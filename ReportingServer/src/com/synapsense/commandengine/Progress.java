package com.synapsense.commandengine;

public interface Progress {
	void showProgress(String message);

	void reportError(Exception e);
}
