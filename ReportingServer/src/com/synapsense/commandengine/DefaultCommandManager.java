package com.synapsense.commandengine;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

import com.synapsense.reporting.utils.CollectionUtils;
import com.synapsense.reporting.utils.algo.ForEach;
import com.synapsense.reporting.utils.algo.UnaryFunctor;

public class DefaultCommandManager implements Serializable, CommandManager {

	private static final long serialVersionUID = -8437369181630826938L;

	private ExecutorService executor;

	private Map<Integer, CommandContext> managedCommands;

	private AtomicInteger ids = new AtomicInteger();

	public DefaultCommandManager(int threadNumber) {
		executor = Executors.newFixedThreadPool(threadNumber);
		managedCommands = CollectionUtils.newLinkedHashMap();
	}

	@Override
	public CommandProxy executeCommand(Command command) {
		final CommandContext context = new CommandContext(command);
		int cId = ids.getAndIncrement();

		managedCommands.put(cId, context);

		executor.execute(new Runnable() {

			@Override
			public void run() {
				context.start();
			}
		});
		return new ManagedProxy(cId);
	}

	@Override
	public Collection<CommandProxy> getManagedCommands() {
		final Collection<CommandProxy> proxies = new ArrayList<CommandProxy>();
		new ForEach<Integer>(managedCommands.keySet()).process(new UnaryFunctor<Integer>() {

			@Override
			public void process(Integer value) {
				proxies.add(new ManagedProxy(value));
			}
		});
		return proxies;
	}

	@Override
	public void clearCommands() {
		for (Iterator<Entry<Integer, CommandContext>> iter = managedCommands.entrySet().iterator(); iter.hasNext();) {
			CommandContext context = iter.next().getValue();
			if (context.removable()) {
				iter.remove();
			}
		}
	}

	@Override
	public void cancelCommand(int id) {
		checkAndGetCOntext(id).cancel();
	}

	@Override
	public CommandStatus getCommandStatus(int id) {
		return checkAndGetCOntext(id).getStatus();
	}

	@Override
	public double getCommandProgress(int id) {
		return checkAndGetCOntext(id).getProgress();
	}

	@Override
	public String getCommandName(int id) {
		return checkAndGetCOntext(id).getCommand().getName();
	}

	@Override
	public String getCommandDescription(int id) {
		return checkAndGetCOntext(id).getCommand().getDescription();
	}

	private CommandContext checkAndGetCOntext(int id) {
		CommandContext context = managedCommands.get(id);
		if (context == null) {
			throw new IllegalArgumentException("No command managed with id " + id);
		}
		return context;
	}

	private class ManagedProxy implements CommandProxy {

		// ESCA-JAVA0096:
		private static final long serialVersionUID = 4071432692959236648L;

		private int commandId;

		private ManagedProxy(int commandId) {
			this.commandId = commandId;
		}

		@Override
		public int getCommandId() {
			return commandId;
		}

		@Override
		public String getCommandName() {
			return DefaultCommandManager.this.getCommandName(commandId);
		}

		@Override
		public String getCommandDescription() {
			return DefaultCommandManager.this.getCommandDescription(commandId);
		}

		@Override
		public void cancel() {
			DefaultCommandManager.this.cancelCommand(commandId);
		}

		@Override
		public double getProgress() {
			return DefaultCommandManager.this.getCommandProgress(commandId);
		}

		@Override
		public CommandStatus getStatus() {
			return DefaultCommandManager.this.getCommandStatus(commandId);
		}
	}
}
