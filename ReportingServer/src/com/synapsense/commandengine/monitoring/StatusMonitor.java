package com.synapsense.commandengine.monitoring;

import com.synapsense.commandengine.CommandStatus;

public interface StatusMonitor {

	double getProgress();

	CommandStatus getStatus();

	void setProgress(double progress);

	void setStatus(CommandStatus status);
}
