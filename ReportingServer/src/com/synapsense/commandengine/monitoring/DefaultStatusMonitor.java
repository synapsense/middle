package com.synapsense.commandengine.monitoring;

import com.synapsense.commandengine.CommandStatus;

public class DefaultStatusMonitor implements StatusMonitor {

	private CommandStatus status;

	private double progress;

	public CommandStatus getStatus() {
		return status;
	}

	public void setStatus(CommandStatus status) {
		this.status = status;
	}

	public double getProgress() {
		return progress;
	}

	public void setProgress(double progress) {
		this.progress = progress;
	}
}
