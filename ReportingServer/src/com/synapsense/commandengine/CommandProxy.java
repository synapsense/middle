package com.synapsense.commandengine;

import java.io.Serializable;

public interface CommandProxy extends Serializable {

	double getProgress();

	CommandStatus getStatus();

	void cancel();

	int getCommandId();

	String getCommandName();

	String getCommandDescription();
}
