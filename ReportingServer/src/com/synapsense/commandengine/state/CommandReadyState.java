package com.synapsense.commandengine.state;

import com.synapsense.commandengine.CommandContext;
import com.synapsense.commandengine.CommandStatus;
import com.synapsense.commandengine.monitoring.StatusMonitor;

public class CommandReadyState extends CommandDefaultState {

	protected CommandReadyState() {
		super("READY");
	}

	@Override
	public void start(final CommandContext context) {
		debugStateMethodCall(this, context, TOKEN_START);
		context.setState(CommandState.RUNNING);
		try {

			context.getCommand().run(new StatusMonitor() {

				@Override
				public void setProgress(double progress) {
					context.setProgress(progress);
				}

				@Override
				public void setStatus(CommandStatus status) {
					context.setStatus(status);
				}

				@Override
				public CommandStatus getStatus() {
					return context.getStatus();
				}

				@Override
				public double getProgress() {
					return context.getProgress();
				}
			});
			context.complete();
		} catch (RuntimeException e) {
			context.setStatus(CommandStatus.failed(e.getMessage()));
			context.fail(e);
		}
	}

	@Override
	public void cancel(CommandContext context) {
		debugStateMethodCall(this, context, TOKEN_CANCEL);
		context.setState(CommandState.CANCELLED);
	}
}
