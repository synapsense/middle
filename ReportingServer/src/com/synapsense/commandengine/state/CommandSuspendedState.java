package com.synapsense.commandengine.state;

import com.synapsense.commandengine.CommandContext;

public class CommandSuspendedState extends CommandDefaultState {

	protected CommandSuspendedState() {
		super("SUSPENDED");
	}

	@Override
	public void fail(CommandContext context) {
		debugStateMethodCall(this, context, TOKEN_FAIL);
		context.setState(CommandState.ERROR);
	}

	@Override
	public void resume(CommandContext context) {
		debugStateMethodCall(this, context, TOKEN_RESUME);
		try {
			context.getCommand().renew();
			context.setState(CommandState.RUNNING);
		} catch (RuntimeException e) {
			context.fail(e);
		}
	}
}
