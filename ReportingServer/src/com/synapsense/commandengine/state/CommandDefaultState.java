package com.synapsense.commandengine.state;

import com.synapsense.commandengine.CommandContext;

public class CommandDefaultState extends CommandState {

	protected CommandDefaultState(String stateName) {
		super(stateName);
	}

	@Override
	public void start(CommandContext context) {
		throwIllegalStateException(this, context, TOKEN_START);
	}

	@Override
	public void cancel(CommandContext context) {
		debugStateMethodCall(this, context, TOKEN_CANCEL);
	}

	@Override
	public void complete(CommandContext context) {
		throwIllegalStateException(this, context, TOKEN_COMPLETE);
	}

	@Override
	public void fail(CommandContext context) {
		throwIllegalStateException(this, context, TOKEN_FAIL);
	}

	@Override
	public void resume(CommandContext context) {
		throwIllegalStateException(this, context, TOKEN_RESUME);
	}

	@Override
	public void suspend(CommandContext context) {
		debugStateMethodCall(this, context, TOKEN_SUSPEND);
	}

	@Override
	public boolean removable() {
		return false;
	}
}
