package com.synapsense.commandengine.state;

import com.synapsense.commandengine.CommandContext;

public class CommandRunningState extends CommandDefaultState {

	protected CommandRunningState() {
		super("RUNNING");
	}

	@Override
	public void cancel(CommandContext context) {
		debugStateMethodCall(this, context, TOKEN_CANCEL);
		context.setState(CommandState.CANCELLING);
		try {
			if (log.isDebugEnabled()) {
				log.debug("Interrupting command " + context.getCommand().getName());
			}
			context.getCommand().interrupt();
		} catch (RuntimeException e) {
			if (log.isDebugEnabled()) {
				log.debug("Command " + context.getCommand().getName() + " failed to interrupt", e);
			}
			context.fail(e);
		}
	}

	@Override
	public void complete(CommandContext context) {
		debugStateMethodCall(this, context, TOKEN_COMPLETE);
		context.setState(CommandState.COMPLETED);
	}

	@Override
	public void fail(CommandContext context) {
		debugStateMethodCall(this, context, TOKEN_FAIL);
		context.setState(CommandState.CANCELLING);
		context.complete();
	}

	@Override
	public void suspend(CommandContext context) {
		debugStateMethodCall(this, context, TOKEN_SUSPEND);
		try {
			if (context.getCommand().pause()) {
				context.setState(CommandState.SUSPENDED);
			}
		} catch (RuntimeException e) {
			context.fail(e);
		}
	}
}
