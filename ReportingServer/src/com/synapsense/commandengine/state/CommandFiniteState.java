package com.synapsense.commandengine.state;

import com.synapsense.commandengine.CommandContext;

public class CommandFiniteState extends CommandDefaultState {

	protected CommandFiniteState(String stateName) {
		super(stateName);
	}

	@Override
	public void start(CommandContext context) {
		debugStateMethodCall(this, context, TOKEN_START);
		context.setState(CommandState.READY);
		context.start();
	}

	@Override
	public boolean removable() {
		return true;
	}
}
