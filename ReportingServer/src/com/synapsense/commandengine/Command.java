package com.synapsense.commandengine;

import com.synapsense.commandengine.monitoring.StatusMonitor;

public interface Command {

	String getName();

	String getDescription();

	void run(StatusMonitor statusMonitor);

	boolean pause();

	void renew();

	void interrupt();

	void rollback();
}
