package com.synapsense.commandengine;

import java.util.Collection;

public interface CommandManager {

	CommandProxy executeCommand(Command command);

	Collection<CommandProxy> getManagedCommands();

	void clearCommands();

	void cancelCommand(int id);

	CommandStatus getCommandStatus(int id);

	double getCommandProgress(int id);

	String getCommandName(int id);

	String getCommandDescription(int id);

}