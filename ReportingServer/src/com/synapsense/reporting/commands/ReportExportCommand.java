package com.synapsense.reporting.commands;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.commandengine.Command;
import com.synapsense.commandengine.CommandStatus;
import com.synapsense.commandengine.monitoring.StatusMonitor;
import com.synapsense.reporting.ReportingException;
import com.synapsense.reporting.dao.ReportDAO;
import com.synapsense.reporting.dto.ReportInfo;
import com.synapsense.reporting.export.ExportFormat;

public class ReportExportCommand implements Command {

	private static final Log log = LogFactory.getLog(ReportExportCommand.class);

	private ReportInfo reportInfo;
	private ExportFormat exportFormat;
	private ReportDAO reportDAO;

	private boolean cancelled;

	public ReportExportCommand(ReportInfo reportInfo, ExportFormat format, ReportDAO reportDAO) {
		this.reportInfo = reportInfo;
		this.exportFormat = format;
		this.reportDAO = reportDAO;
	}

	@Override
	public String getName() {
		return "Export of " + reportInfo.getReportName() + " command";
	}

	@Override
	public String getDescription() {
		return "Export of report '" + reportInfo.getReportName() + "' to " + exportFormat.toString();
	}

	@Override
	public void run(StatusMonitor statusMonitor) {
		try {
			if (log.isDebugEnabled()) {
				log.debug(getName() + "...started");
			}

			if (cancelled) {
				if (log.isDebugEnabled()) {
					log.debug(getName() + "...cancelled");
				}
				statusMonitor.setStatus(CommandStatus.CANCELLED);
				return;
			}

			statusMonitor.setStatus(CommandStatus.inProgress("Exporting report object"));

			reportInfo.addExportedPath(exportFormat, reportDAO.exportReport(reportInfo.getReportId(), exportFormat));

			statusMonitor.setProgress(1.0);

			if (log.isDebugEnabled()) {
				log.debug(getName() + "...completed");
			}

			statusMonitor.setStatus(CommandStatus.completed("Report exported successfully"));
		} catch (ReportingException e) {
			statusMonitor.setStatus(CommandStatus.failed(e.getMessage()));
			log.error("Exporting report '" + reportInfo.getReportName() + "' failed", e);
			throw new ReportingCommandException(getDescription() + "...failed", e);
		}
	}

	@Override
	public void interrupt() {
		if (log.isDebugEnabled()) {
			log.debug(getName() + "...received cancel request");
		}
		cancelled = true;
	}

	@Override
	public void rollback() {
	}

	@Override
	public boolean pause() {
		return false;
	}

	@Override
	public void renew() {
		throw new UnsupportedOperationException();
	}
}
