package com.synapsense.reporting.commands;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.commandengine.Command;
import com.synapsense.commandengine.CommandStatus;
import com.synapsense.commandengine.monitoring.StatusMonitor;
import com.synapsense.reporting.ReportingException;
import com.synapsense.reporting.dao.ReportDAO;
import com.synapsense.reporting.dao.ReportTemplateDAO;
import com.synapsense.reporting.dao.ReportingDAOException;
import com.synapsense.reporting.dto.ReportInfo;
import com.synapsense.reporting.model.Report;
import com.synapsense.reporting.model.ReportGenerationException;
import com.synapsense.reporting.model.ReportGenerationHandler;
import com.synapsense.reporting.model.ReportGenerationListener;
import com.synapsense.reporting.model.ReportTemplate;

public class ReportGenerationCommand implements Command {

	private static final Log log = LogFactory.getLog(ReportGenerationCommand.class);

	private ReportInfo reportInfo;

	private ReportTemplateDAO rtDAO;

	private ReportDAO reportDAO;

	private ReportGenerationHandler handler;

	private ReentrantLock runlock;
	private Condition runCondition;

	private boolean cancelled;

	public ReportGenerationCommand(ReportInfo reportInfo, ReportTemplateDAO rtDAO, ReportDAO reportDAO) {
		this.reportInfo = reportInfo;
		this.rtDAO = rtDAO;
		this.reportDAO = reportDAO;
		this.runlock = new ReentrantLock();
		this.runCondition = runlock.newCondition();
	}

	@Override
	public String getName() {
		return reportInfo.getReportName();
	}

	@Override
	public String getDescription() {
		return "Generation of report '" + reportInfo.getReportName() + "'";
	}

	@Override
	public void run(final StatusMonitor statusMonitor) {
		if (log.isDebugEnabled()) {
			log.debug(getName() + "..started");
		}

		try {
			statusMonitor.setStatus(CommandStatus.inProgress("Compiling report template"));
			ReportTemplate reportTemplate = new ReportTemplate(rtDAO.getReportTemplateDesign(reportInfo
			        .getReportTemplateName()));
			statusMonitor.setProgress(0.3);

			if (checkCancelled(statusMonitor)) {
				return;
			}

			if (log.isDebugEnabled()) {
				log.debug(getName() + "...Creating report generation handler");
			}
			statusMonitor.setStatus(CommandStatus.inProgress("Creating report generation handler"));
			handler = reportTemplate.createReportGenerationHandler(reportInfo);
		} catch (ReportingException e) {
			if (log.isDebugEnabled()) {
				log.error(getName() + "...failed", e);
			}
			statusMonitor.setStatus(CommandStatus.failed("Report generation failed: " + e.getMessage()));
			throw new ReportingCommandException(e.getMessage(), e);
		}

		handler.addListener(new ReportGenerationListener() {

			private void signal() {
				runlock.lock();
				try {
					runCondition.signal();
				} finally {
					runlock.unlock();
				}
			}

			@Override
			public void cancelled() {
				if (log.isDebugEnabled()) {
					log.debug(getName() + "...cancelled");
				}
				statusMonitor.setStatus(CommandStatus.cancelled("Generation cancelled"));
				signal();
			}

			@Override
			public void completed(Report report) {
				try {
					statusMonitor.setProgress(0.6);
					statusMonitor.setStatus(CommandStatus.inProgress("Saving report object"));
					reportDAO.saveReport(reportInfo, report);
					statusMonitor.setProgress(1.0);
					statusMonitor.setStatus(CommandStatus.completed("Report generated successfully"));
					if (log.isDebugEnabled()) {
						log.debug(getName() + "...completed");
					}
				} catch (ReportingDAOException e) {
					if (log.isDebugEnabled()) {
						log.debug(getName() + "...failed", e);
					}
					statusMonitor.setStatus(CommandStatus.failed("Report generation failed: " + e.getMessage()));
				} finally {
					signal();
				}
			}

			@Override
			public void failed(Throwable e) {
				if (log.isDebugEnabled()) {
					log.error(getName() + "...failed", e);
				}
				statusMonitor.setStatus(CommandStatus.failed("Report generation failed: " + e.getMessage()));
				signal();
			}
		});

		statusMonitor.setStatus(CommandStatus.inProgress("Generating report"));
		handler.startGeneration();

		runlock.lock();
		try {
			runCondition.await();
		} catch (InterruptedException e) {
			return;
		} finally {
			runlock.unlock();
		}

		if (statusMonitor.getStatus().equals(CommandStatus.FAILED)) {
			throw new ReportingCommandException(getName() + "..failed");
		}
	}

	@Override
	public void rollback() {
		if (log.isDebugEnabled()) {
			log.debug("Unexecuting command " + getName());
		}
	}

	@Override
	public void interrupt() {
		cancelled = true;
		try {
			handler.cancelGeneration();
		} catch (ReportGenerationException e) {
			throw new ReportingCommandException(e.getMessage(), e);
		}
	}

	@Override
	public boolean pause() {
		return false;
	}

	@Override
	public void renew() {
		throw new UnsupportedOperationException();
	}

	private boolean checkCancelled(StatusMonitor statusMonitor) {
		if (cancelled) {
			statusMonitor.setStatus(CommandStatus.cancelled("Generation cancelled"));
			if (log.isDebugEnabled()) {
				log.debug(getName() + "...cancelled");
			}
		}
		return cancelled;
	}
}
