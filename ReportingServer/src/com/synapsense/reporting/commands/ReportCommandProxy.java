package com.synapsense.reporting.commands;

import com.synapsense.commandengine.CommandProxy;
import com.synapsense.reporting.dto.ReportTaskInfo;

public interface ReportCommandProxy extends CommandProxy {

	ReportTaskInfo getTaskInfo();
}
