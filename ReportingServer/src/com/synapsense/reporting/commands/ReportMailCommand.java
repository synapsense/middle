package com.synapsense.reporting.commands;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.commandengine.Command;
import com.synapsense.commandengine.CommandStatus;
import com.synapsense.commandengine.monitoring.StatusMonitor;
import com.synapsense.reporting.dao.ReportDAO;
import com.synapsense.reporting.dao.ReportingDAOException;
import com.synapsense.reporting.dto.ReportInfo;
import com.synapsense.reporting.export.ExportFormat;
import com.synapsense.reporting.mail.SMTPMailer;
import com.synapsense.reporting.mail.SmtpException;

public class ReportMailCommand implements Command {

	public static final String MESSAGE_SUBJECT = "SynapSense Reporting System Notification";

	public static final String MESSAGE_BODY = "Reports attached";

	private static final Log log = LogFactory.getLog(ReportMailCommand.class);

	private Set<String> destinations;

	private ReportDAO reportDAO;

	private ReportInfo reportInfo;

	private Properties javamailProperties;

	private boolean cancelled = false;

	public ReportMailCommand(ReportDAO reportDAO, Properties mailSettings, Collection<String> destinations,
	        ReportInfo reportInfo) {
		this.reportDAO = reportDAO;
		this.destinations = new LinkedHashSet<String>(destinations);
		this.javamailProperties = new Properties(mailSettings);
		this.reportInfo = reportInfo;
	}

	@Override
	public String getName() {
		return "Sending reports via email";
	}

	@Override
	public String getDescription() {
		return new StringBuilder().append("Sending report ").append(reportInfo.getReportName())
		        .append(" via email to ").append(destinations).toString();
	}

	@Override
	public void run(StatusMonitor statusMonitor) {
		statusMonitor.setStatus(CommandStatus.IN_PROGRESS);
		if (log.isDebugEnabled()) {
			log.debug(getName() + " started...Sending report " + reportInfo.getReportName() + " to " + destinations);
		}

		SMTPMailer mailer = new SMTPMailer(javamailProperties);

		statusMonitor.setProgress(0.1);
		statusMonitor.setStatus(CommandStatus.inProgress("Processing report"));
		List<String> attachments = new ArrayList<String>();

		// Check if the report is already exported to PDF
		String path = reportInfo.getExportedPath(ExportFormat.PDF);
		try {
			if (path == null) {
				path = reportDAO.exportReport(reportInfo.getReportId(), ExportFormat.PDF);
			}
		} catch (ReportingDAOException e) {
			log.error(getName() + " failed", e);
			statusMonitor.setStatus(CommandStatus.failed(e.getMessage()));
			throw new ReportingCommandException(e.getMessage(), e);
		}

		attachments.add(reportDAO.getExportRootPath() + path);

		statusMonitor.setProgress(0.5);
		statusMonitor.setStatus(CommandStatus.inProgress("Sending report"));

		if (cancelled) {
			if (log.isDebugEnabled()) {
				log.debug(getName() + " cancelled");
			}
			statusMonitor.setStatus(CommandStatus.CANCELLED);
			return;
		}

		try {
			mailer.send(MESSAGE_SUBJECT, MESSAGE_BODY, attachments.toArray(new String[attachments.size()]),
			        destinations.toArray(new String[destinations.size()]));

			statusMonitor.setProgress(1.0);
			statusMonitor.setStatus(CommandStatus.completed("Message sent"));
			if (log.isDebugEnabled()) {
				log.debug(getName() + " completed successfully");
			}

		} catch (SmtpException e) {
			statusMonitor.setStatus(CommandStatus.failed(e.getMessage()));
			log.error(getName() + " failed", e);
			throw new ReportingCommandException(e.getMessage(), e);
		}
	}

	@Override
	public void renew() {
		throw new UnsupportedOperationException();
	}

	@Override
	public void rollback() {
	}

	@Override
	public void interrupt() {
		if (log.isDebugEnabled()) {
			log.debug(getName() + "...received cancel request");
		}
		cancelled = true;
	}

	@Override
	public boolean pause() {
		return false;
	}
}
