package com.synapsense.reporting.commands;

import java.util.Collection;
import java.util.Properties;

import com.synapsense.reporting.dao.ReportDAO;
import com.synapsense.reporting.dao.ReportTemplateDAO;
import com.synapsense.reporting.dto.ReportInfo;
import com.synapsense.reporting.export.ExportFormat;

public final class CommandFactory {

	public static ReportGenerationCommand newReportGenerationCommand(ReportInfo reportInfo, ReportTemplateDAO rtDAO,
	        ReportDAO reportDAO) {
		return new ReportGenerationCommand(reportInfo, rtDAO, reportDAO);
	}

	public static ReportExportCommand newReportExportCommand(ReportDAO reportDAO, ReportInfo reportInfo,
	        ExportFormat format) {
		return new ReportExportCommand(reportInfo, format, reportDAO);
	}

	public static ReportMailCommand newReportMailCommand(ReportDAO reportDAO, Properties mailSettings,
	        Collection<String> destinations, ReportInfo reportInfo) {
		return new ReportMailCommand(reportDAO, mailSettings, destinations, reportInfo);
	}

	private CommandFactory() {
	}
}
