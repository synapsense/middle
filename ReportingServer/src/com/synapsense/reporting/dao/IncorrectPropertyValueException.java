package com.synapsense.reporting.dao;

public class IncorrectPropertyValueException extends ReportingDAOException {

	private static final long serialVersionUID = 1613638669058411918L;

	public IncorrectPropertyValueException() {
	}

	public IncorrectPropertyValueException(String message, Throwable cause) {
		super(message, cause);
	}

	public IncorrectPropertyValueException(String message) {
		super(message);
	}

	public IncorrectPropertyValueException(Throwable cause) {
		super(cause);
	}
}
