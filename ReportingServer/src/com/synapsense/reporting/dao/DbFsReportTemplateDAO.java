package com.synapsense.reporting.dao;

import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.reporting.dto.ReportTemplateInfo;
import com.synapsense.reporting.model.ReportParameter;
import com.synapsense.reporting.utils.FileUtils;

public class DbFsReportTemplateDAO implements ReportTemplateDAO {

	private static final long serialVersionUID = -7529708187203190463L;

	private static final Log log = LogFactory.getLog(DbFsReportTemplateDAO.class);

	private ConnectionManager conMan;

	private File fileStorage;

	public DbFsReportTemplateDAO(ConnectionManager conMan, File fileStorage) {
		this.conMan = conMan;
		this.fileStorage = fileStorage;
	}

	@Override
	public void createReportTemplate(ReportTemplateInfo reportTemplateInfo, String reportTemplateDesign)
	        throws ObjectExistsException {

		if (reportTemplateInfo.getReportTemplateName() == null) {
			throw new IllegalArgumentException("Report template name shouldn't be null");
		}

		if (reportTemplateDesign == null) {
			throw new IllegalArgumentException("Report template source shouldn't be null");
		}

		if (log.isDebugEnabled()) {
			log.debug("Report template " + reportTemplateInfo.getReportTemplateName() + " received to persist");
		}

		Connection conn = conMan.getConnection();
		conMan.setAutoCommit(conn, false);

		try {
			createReportTemplate(conn, reportTemplateInfo, reportTemplateDesign);
		} catch (RuntimeException e) {
			log.error(e.getLocalizedMessage(), e);
			conMan.rollback(conn);
			throw e;
		} finally {
			conMan.commit(conn);
			conMan.setAutoCommit(conn, true);
		}
	}

	@Override
	public String getReportTemplateDesign(String reportTemplateName) throws ObjectNotFoundException {

		if (reportTemplateName == null) {
			throw new IllegalArgumentException("Report template name shouldn't be null");
		}

		if (log.isDebugEnabled()) {
			log.debug("Retreiving report template " + reportTemplateName + " design");
		}

		try {
			Connection conn = conMan.getConnection();

			PreparedStatement stmt = conMan.createStatement(conn, "query.get.report.template.design.by.name");
			stmt.setString(1, reportTemplateName);

			ResultSet result = stmt.executeQuery();
			if (!result.next()) {
				throw new ObjectNotFoundException("Report template " + reportTemplateName + " not found");
			}
			return result.getString("DESIGN");
		} catch (SQLException e) {
			log.error(e.getLocalizedMessage(), e);
			throw new ResourceException("Error retreiving report template " + reportTemplateName + " design");
		}
	}

	@Override
	public ReportTemplateInfo getReportTemplateInfo(String reportTemplateName) throws ObjectNotFoundException {

		if (reportTemplateName == null) {
			throw new IllegalArgumentException("Report template name shouldn't be null");
		}

		if (log.isDebugEnabled()) {
			log.debug("Retreiving report template " + reportTemplateName);
		}

		try {
			Connection conn = conMan.getConnection();

			// Creating select statements
			PreparedStatement rtStmt = conMan.createStatement(conn, "query.get.report.template.id.by.name");
			rtStmt.setString(1, reportTemplateName);

			ResultSet resultSet = rtStmt.executeQuery();
			if (!resultSet.next()) {
				if (log.isDebugEnabled()) {
					log.debug("Report template " + reportTemplateName + " not found");
				}
				throw new ObjectNotFoundException("Report template " + reportTemplateName + " not found");
			}

			ReportTemplateInfo rtInfo = new ReportTemplateInfo();
			rtInfo.setReportTemplateName(reportTemplateName);
			rtInfo.setReportTemplateId(resultSet.getInt("ID"));

			if (resultSet.next()) {
				throw new ResourceException("Multiple records returned by query that returns a template by name");
			}

			PreparedStatement paramStmt = conMan.createStatement(conn, "query.get.report.template.parameters");
			paramStmt.setInt(1, rtInfo.getReportTemplateId());
			ResultSet paramSet = paramStmt.executeQuery();
			while (paramSet.next()) {
				try {
					rtInfo.addParameter(new ReportParameter(paramSet.getString("NAME"), Class.forName(paramSet
					        .getString("TYPE"))));
				} catch (ClassNotFoundException e) {
					throw new ResourceException("Unable to cast parameter to type " + paramSet.getString(2), e);
				}
			}

			return rtInfo;
		} catch (SQLException e) {
			log.error(e.getLocalizedMessage(), e);
			throw new ResourceException("Select report template statement execution error", e);
		}
	}

	@Override
	public Collection<String> getReportTemplates() {

		if (log.isDebugEnabled()) {
			log.debug("Retreiving report templates");
		}

		Connection conn = conMan.getConnection();

		try {
			PreparedStatement stmt = conMan.createStatement(conn, "query.get.report.templates.names");
			ResultSet result = stmt.executeQuery();
			Collection<String> templates = new ArrayList<String>();
			while (result.next()) {
				templates.add(result.getString("NAME"));
			}
			return templates;
		} catch (SQLException e) {
			log.error(e.getLocalizedMessage(), e);
			throw new ResourceException("Error fetching report templates", e);
		}
	}

	@Override
	public void removeReportTemplate(String reportTemplateName) throws ObjectNotFoundException {

		if (reportTemplateName == null) {
			throw new IllegalArgumentException("Report template name shouldn't be null");
		}

		Connection conn = conMan.getConnection();

		conMan.setAutoCommit(conn, false);

		if (log.isDebugEnabled()) {
			log.debug("Removing report template " + reportTemplateName);
		}

		try {
			try {
				PreparedStatement stmt = conMan.createStatement(conn, "query.remove.report.template.by.name");
				stmt.setString(1, reportTemplateName);
				int nbDeleted = stmt.executeUpdate();

				if (nbDeleted == 0) {
					throw new ObjectNotFoundException("Report template " + reportTemplateName + " not found");
				}

				if (nbDeleted > 1) {
					throw new ResourceException("Duplicate report template  " + reportTemplateName);
				}
				if (log.isDebugEnabled()) {
					log.debug("Report template " + reportTemplateName + " removed");
				}

			} catch (SQLException e) {
				throw new ResourceException("Report template remove statement execution error", e);
			}

			if (!FileUtils.deleteDirectory(new File(fileStorage, reportTemplateName))) {
				throw new ResourceException("Error removing report template " + reportTemplateName + " folder");
			}

		} catch (RuntimeException e) {
			log.error(e.getLocalizedMessage(), e);
			conMan.rollback(conn);
			throw e;
		} finally {
			conMan.commit(conn);
			conMan.setAutoCommit(conn, true);
		}
	}

	@Override
	public void updateReportTemplate(ReportTemplateInfo reportTemplateInfo, String reportTemplateDesign)
	        throws ObjectNotFoundException, ObjectExistsException {

		if (reportTemplateInfo.getReportTemplateName() == null) {
			throw new IllegalArgumentException("Report template name shouldn't be null");
		}

		if (reportTemplateDesign == null) {
			throw new IllegalArgumentException("Report template design shouldn't be null");
		}

		if (log.isDebugEnabled()) {
			log.debug("Updating report template " + reportTemplateInfo.getReportTemplateId());
		}

		Connection conn = conMan.getConnection();
		conMan.setAutoCommit(conn, false);

		try {
			try {
				PreparedStatement stmt = conMan.createStatement(conn, "query.remove.report.template.by.id");
				stmt.setInt(1, reportTemplateInfo.getReportTemplateId());

				int nbDeleted = stmt.executeUpdate();
				if (nbDeleted == 0) {
					throw new ObjectNotFoundException("Report template with id "
					        + reportTemplateInfo.getReportTemplateId() + " not found");
				}
			} catch (SQLException e) {
				throw new ResourceException("Remove report template query execution error", e);
			}

			try {
				createReportTemplate(conn, reportTemplateInfo, reportTemplateDesign);
			} catch (ObjectExistsException e) {
				conMan.rollback(conn);
				throw e;
			}
		} catch (RuntimeException e) {
			log.error(e.getLocalizedMessage(), e);
			conMan.rollback(conn);
		} finally {
			conMan.commit(conn);
			conMan.setAutoCommit(conn, false);
		}
	}

	private void createReportTemplate(Connection conn, ReportTemplateInfo reportTemplateInfo,
	        String reportTemplateDesign) throws ObjectExistsException {
		try {
			// Create template sql statements
			PreparedStatement selectByNameStmt = conMan.createStatement(conn, "query.count.report.template.by.name");
			selectByNameStmt.setString(1, reportTemplateInfo.getReportTemplateName());

			PreparedStatement insertStmt = conMan.createStatement(conn, "query.insert.report.template");
			insertStmt.setString(1, reportTemplateInfo.getReportTemplateName());
			insertStmt.setString(2, reportTemplateDesign);

			// Check report template uniqueness
			ResultSet result = selectByNameStmt.executeQuery();
			result.next();
			if (result.getInt(1) != 0) {
				throw new ObjectExistsException("Report template with name "
				        + reportTemplateInfo.getReportTemplateName() + " already exists");
			}

			// Insert the template
			insertStmt.executeUpdate();
		} catch (SQLException e) {
			throw new ResourceException("Unable to create report template", e);
		}

		// Fetch just created report template id
		int rtId = 0;
		try {
			PreparedStatement getIdStmt = null;
			getIdStmt = conMan.createStatement(conn, "query.get.report.template.id.by.name");
			getIdStmt.setString(1, reportTemplateInfo.getReportTemplateName());
			ResultSet res = getIdStmt.executeQuery();
			res.next();
			rtId = res.getInt("ID");
		} catch (SQLException e) {
			throw new ResourceException("Error retreiving report template "
			        + reportTemplateInfo.getReportTemplateName() + " id");
		}

		// Fill report template info with newly created id
		reportTemplateInfo.setReportTemplateId(rtId);

		// Persist parameters
		Collection<ReportParameter> parameters = reportTemplateInfo.getParameters();
		if (parameters != null && !parameters.isEmpty()) {

			PreparedStatement insertParamsStmt = null;
			try {
				// Create parameters statement
				insertParamsStmt = conMan.createStatement(conn, "query.insert.report.parameter");
				insertParamsStmt.setInt(3, rtId);

				// Create each parameter
				for (ReportParameter par : parameters) {
					insertParamsStmt.setString(1, par.getName());
					insertParamsStmt.setString(2, par.getType().getName());
					insertParamsStmt.executeUpdate();
				}
			} catch (SQLException e) {
				throw new ResourceException("Unable to persist report template parameters", e);
			}
		}
	}
}
