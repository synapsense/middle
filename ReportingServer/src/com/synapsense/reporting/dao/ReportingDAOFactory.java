package com.synapsense.reporting.dao;

public interface ReportingDAOFactory {

	ReportTemplateDAO newReportTemplateDAO();

	ReportDAO newReportDao();

	void shutdown() throws ReportingDAOException;
}
