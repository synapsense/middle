package com.synapsense.reporting.dao;

import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.reporting.dto.ReportInfo;
import com.synapsense.reporting.dto.ReportTitle;
import com.synapsense.reporting.export.ExportFormat;
import com.synapsense.reporting.model.Report;
import com.synapsense.reporting.model.ReportExportException;
import com.synapsense.reporting.utils.FileUtils;

public class DbFsReportDAO implements ReportDAO {

	private static final long serialVersionUID = 8944315556719387124L;

	private static final Log log = LogFactory.getLog(DbFsReportDAO.class);

	private ConnectionManager connMan;

	private File fileStorage;

	public DbFsReportDAO(ConnectionManager connMan, File fileStorage) {
		this.fileStorage = fileStorage;
		this.connMan = connMan;
	}

	@Override
	public void saveReport(ReportInfo reportInfo, Report reportObject) throws ObjectExistsException,
	        ObjectNotFoundException, IncorrectPropertyValueException {
		if (reportInfo == null) {
			throw new IllegalArgumentException("Report info shouldn't be null");
		}

		if (reportObject == null) {
			throw new IllegalArgumentException("Report object shouldn't be null");
		}

		if (reportInfo.getReportName() == null) {
			throw new IllegalArgumentException("Report name shouldn't be null");
		}

		if (reportInfo.getReportTemplateName() == null) {
			throw new IllegalArgumentException("Report template name shouldn't be null");
		}

		if (log.isDebugEnabled()) {
			log.debug("Saving report object " + reportInfo.getReportName() + " of template "
			        + reportInfo.getReportTemplateName());
		}

		Connection conn = connMan.getConnection();
		connMan.setAutoCommit(conn, false);

		try {
			int rtId = 0;
			PreparedStatement getTemplateIdStmt = connMan.createStatement(conn, "query.get.report.template.id.by.name");
			try {
				getTemplateIdStmt.setString(1, reportInfo.getReportTemplateName());
				ResultSet rtIdSet = getTemplateIdStmt.executeQuery();
				if (!rtIdSet.next()) {
					throw new ObjectNotFoundException("Report template " + reportInfo.getReportTemplateName()
					        + " not found");
				}
				rtId = rtIdSet.getInt("ID");
			} catch (SQLException e) {
				throw new ResourceException("Error retreiving report template id", e);
			}

			long timestamp = new Date().getTime();

			PreparedStatement insertReportStmt = connMan.createStatement(conn, "query.insert.report");
			try {
				insertReportStmt.setString(1, reportInfo.getReportName());
				insertReportStmt.setLong(2, timestamp);
				insertReportStmt.setInt(3, rtId);
				insertReportStmt.setObject(4, reportObject);

				insertReportStmt.executeUpdate();
			} catch (SQLException e) {
				throw new ResourceException("Error inserting report", e);
			}

			// Get newly created report id
			PreparedStatement getIdStmt = connMan.createStatement(conn,
			        "query.get.report.id.by.name.timestamp.template");
			int reportId = 0;
			try {
				getIdStmt.setString(1, reportInfo.getReportName());
				getIdStmt.setLong(2, timestamp);
				getIdStmt.setInt(3, rtId);

				ResultSet idSet = getIdStmt.executeQuery();

				if (!idSet.next()) {
					throw new ResourceException("Just persisted report id not found");
				}
				reportId = idSet.getInt("ID");
			} catch (SQLException e) {
				throw new ResourceException("Error retreiving report id", e);
			}

			// Set report id to dto
			reportInfo.setReportId(reportId);

			Map<String, Object> paramVals = reportInfo.getParametersValues();
			if (!paramVals.isEmpty()) {

				// Persist report parameters
				for (Map.Entry<String, Object> paramEntry : paramVals.entrySet()) {
					String paramName = paramEntry.getKey();
					PreparedStatement getParamStmt = connMan.createStatement(conn,
					        "query.get.paramater.by.name.template");
					int rpId = 0;
					Class<?> rpType = null;
					try {
						getParamStmt.setString(1, paramName);
						getParamStmt.setInt(2, rtId);

						ResultSet rpSet = getParamStmt.executeQuery();

						if (!rpSet.next()) {
							throw new ObjectNotFoundException("Report parameter " + paramName
							        + " not found for template " + reportInfo.getReportTemplateName());
						}

						rpId = rpSet.getInt("ID");
						try {
							rpType = Class.forName(rpSet.getString("TYPE"));
						} catch (ClassNotFoundException e) {
							throw new ResourceException("Error resolving report parameter " + paramName, e);
						}
					} catch (SQLException e) {
						throw new ResourceException("Error retreiving report parameter", e);
					}

					Object value = paramEntry.getValue();
					if (!rpType.isInstance(value)) {
						throw new IncorrectPropertyValueException("Parameter " + paramName + " value is of type "
						        + value.getClass().getName() + " instead of " + rpType.getName()
						        + " specified for report template " + reportInfo.getReportTemplateName());
					}

					PreparedStatement insertParamValStmt = connMan.createStatement(conn,
					        "query.insert.report.parameter.value");
					try {
						insertParamValStmt.setObject(1, value);
						insertParamValStmt.setInt(2, reportId);
						insertParamValStmt.setInt(3, rpId);

						insertParamValStmt.executeUpdate();
					} catch (SQLException e) {
						throw new ResourceException("Error persisting report parameter value", e);
					}
				}
			}
		} catch (RuntimeException e) {
			log.error(e.getLocalizedMessage(), e);
			connMan.rollback(conn);
			throw e;
		} finally {
			connMan.commit(conn);
			connMan.setAutoCommit(conn, true);
		}
	}

	@Override
	public void removeReport(int reportId) throws ObjectNotFoundException {

		if (log.isDebugEnabled()) {
			log.debug("Removeing report " + reportId);
		}

		Connection conn = connMan.getConnection();
		connMan.setAutoCommit(conn, false);
		try {

			String exportPath = getExportFolder(conn, reportId);

			PreparedStatement removeStmt = connMan.createStatement(conn, "query.remove.report.by.id");
			try {
				removeStmt.setInt(1, reportId);

				int nbDeleted = removeStmt.executeUpdate();
				if (nbDeleted == 0) {
					throw new ObjectNotFoundException("Report with id " + reportId + " not found");
				}

			} catch (SQLException e) {
				throw new ResourceException("Error executing remove report query", e);
			}

			if (!FileUtils.deleteDirectory(fileStorage.getAbsolutePath() + exportPath)) {
				throw new ResourceException("Error removing report " + reportId + " export directory");
			}
		} catch (RuntimeException e) {
			log.error(e.getLocalizedMessage(), e);
			connMan.rollback(conn);
			throw e;
		} finally {
			connMan.commit(conn);
			connMan.setAutoCommit(conn, true);
		}
	}

	@Override
	public Collection<ReportTitle> getGeneratedReports(String reportTemplateName) throws ObjectNotFoundException {

		if (reportTemplateName == null) {
			throw new IllegalArgumentException("Report template name shouldn't be null");
		}

		if (log.isDebugEnabled()) {
			log.debug("Retreiving reports of report template " + reportTemplateName);
		}

		Connection conn = connMan.getConnection();

		PreparedStatement stmt = connMan.createStatement(conn, "query.get.reports.by.template.name");

		try {
			stmt.setString(1, reportTemplateName);

			ResultSet reportsSet = stmt.executeQuery();
			Collection<ReportTitle> reports = new ArrayList<ReportTitle>();

			while (reportsSet.next()) {

				ReportTitle report = new ReportTitle(reportTemplateName, reportsSet.getString("NAME"),
				        reportsSet.getLong("TIMESTAMP"));
				int id = reportsSet.getInt("ID");
				report.setReportId(id);

				reports.add(report);
			}
			return reports;
		} catch (SQLException e) {
			log.error(e.getLocalizedMessage(), e);
			throw new ResourceException("Error retreiving reports for report template " + reportTemplateName);
		}
	}

	@Override
	public ReportInfo getReportInfo(int reportId) throws ObjectNotFoundException {
		if (log.isDebugEnabled()) {
			log.debug("Retreiving information for report " + reportId);
		}

		Connection conn = connMan.getConnection();

		PreparedStatement stmt = connMan.createStatement(conn, "query.get.report.template.name.timestamp.by.id");

		try {
			stmt.setInt(1, reportId);

			ResultSet resSet = stmt.executeQuery();

			if (!resSet.next()) {
				throw new ObjectNotFoundException("Report with id " + reportId + " not found");
			}

			ReportInfo reportInfo = new ReportInfo(resSet.getString("TEMPLATE"), resSet.getString("NAME"));
			reportInfo.setTimestamp(resSet.getLong("TIMESTAMP"));
			reportInfo.setReportId(reportId);

			// Fetch parameters
			PreparedStatement parStmt = connMan.createStatement(conn, "query.get.report.parameter.values");
			parStmt.setInt(1, reportId);

			ResultSet paramSet = parStmt.executeQuery();
			while (paramSet.next()) {
				String name = paramSet.getString("NAME");
				String typeStr = paramSet.getString("TYPE");
				Class<?> type = null;
				try {
					type = Class.forName(typeStr);
				} catch (ClassNotFoundException e) {
					throw new ResourceException("Error loading type " + typeStr + " of parameter " + name, e);
				}

				Object value = paramSet.getObject("VALUE");
				if (!type.isInstance(value)) {
					throw new ResourceException("Error casting parameter " + name + " to type " + typeStr);
				}

				reportInfo.addParameterValue(name, value);
			}
			// Fetch exported reports
			PreparedStatement expStmt = connMan.createStatement(conn, "query.get.exported.report.by.report.id");
			expStmt.setInt(1, reportId);
			ResultSet expSet = expStmt.executeQuery();
			while (expSet.next()) {
				String path = expSet.getString("PATH");
				String typeStr = expSet.getString("FORMAT");
				ExportFormat format = ExportFormat.load(typeStr);
				if (format == null) {
					throw new ResourceException("Error loading exported report format " + typeStr);
				}

				reportInfo.addExportedPath(format, path);
			}

			return reportInfo;

		} catch (SQLException e) {
			throw new ResourceException("Error retreiving report by id", e);
		}
	}

	@Override
	public Report getReportObject(int reportId) throws ObjectNotFoundException {

		if (log.isDebugEnabled()) {
			log.debug("Retreiving report object for report " + reportId);
		}

		return getReportObject(connMan.getConnection(), reportId);
	}

	@Override
	public String exportReport(int reportId, ExportFormat format) throws ObjectNotFoundException {

		if (log.isDebugEnabled()) {
			log.debug("Exporting report " + reportId + " to " + format + " format ");
		}

		Connection conn = connMan.getConnection();
		connMan.setAutoCommit(conn, false);

		try {
			String exportPath = getExportFolder(conn, reportId);

			if (!FileUtils.checkOrCreateDirectory(fileStorage.getAbsolutePath() + exportPath)) {
				throw new ResourceException("Error creating export folder");
			}

			String relativePath = exportPath + "/export." + format.getFileExtension();

			String fullPath = fileStorage.getAbsolutePath() + relativePath;

			PreparedStatement stmt = connMan.createStatement(conn, "query.insert.exported.report");
			try {
				stmt.setInt(1, reportId);
				stmt.setString(2, relativePath);
				stmt.setString(3, format.save());

				stmt.executeUpdate();
			} catch (SQLException e) {
				throw new ResourceException("Error persisting exported report", e);
			}

			try {
				getReportObject(conn, reportId).export(format, fullPath);
			} catch (ReportExportException e) {
				throw new ResourceException("Error exporting file to path " + exportPath, e);
			}

			return relativePath;
		} catch (RuntimeException e) {
			log.error(e.getLocalizedMessage(), e);
			connMan.rollback(conn);
			throw e;
		} finally {
			connMan.commit(conn);
			connMan.setAutoCommit(conn, true);
		}
	}

	private String getExportFolder(Connection conn, int reportId) throws ObjectNotFoundException {
		PreparedStatement getRepStmt = connMan.createStatement(conn, "query.get.report.template.name.timestamp.by.id");
		StringBuilder path = new StringBuilder("/");
		try {
			getRepStmt.setInt(1, reportId);

			ResultSet resSet = getRepStmt.executeQuery();

			if (!resSet.next()) {
				throw new ObjectNotFoundException("Report with id " + reportId + " not found");
			}

			path.append(resSet.getString("TEMPLATE")).append("/");
			path.append(resSet.getString("NAME")).append("_").append(resSet.getLong("TIMESTAMP"));
		} catch (SQLException e) {
			throw new ResourceException("Error retreiving report by id", e);
		}
		return path.toString();
	}

	private Report getReportObject(Connection conn, int reportId) throws ObjectNotFoundException {
		PreparedStatement stmt = connMan.createStatement(conn, "query.get.report.object.by.id");
		try {
			stmt.setInt(1, reportId);

			ResultSet rObjectSet = stmt.executeQuery();

			if (!rObjectSet.next()) {
				throw new ObjectNotFoundException("Report with id " + reportId + " not found");
			}
			Object reportObject = rObjectSet.getObject("DATA");
			if (!(reportObject instanceof Report)) {
				throw new ResourceException("Unrecognized object stored for report " + reportId);
			}
			return (Report) reportObject;
		} catch (SQLException e) {
			throw new ResourceException("Error retreiving report object", e);
		}
	}

	@Override
	public String getExportRootPath() {
		return fileStorage.getAbsolutePath();
	}
}
