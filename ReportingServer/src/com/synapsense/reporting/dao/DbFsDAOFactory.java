package com.synapsense.reporting.dao;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.reporting.utils.CollectionUtils;
import com.synapsense.reporting.utils.FileUtils;
import com.synapsense.reporting.utils.SqlFileExecutor;

public class DbFsDAOFactory implements ReportingDAOFactory, ConnectionManager {

	private static final Log log = LogFactory.getLog(DbFsDAOFactory.class);

	private String connString;

	private QueryMap queries = new QueryMap();

	private File fileStorage;

	public DbFsDAOFactory(String driverName, String connString, String initScript, String queryTemplates,
	        String fileStoragePath) throws ReportingDAOException {

		if (log.isDebugEnabled()) {
			log.debug("Creating DB DAO factory");
		}

		this.connString = connString;

		this.fileStorage = new File(fileStoragePath);
		if (!FileUtils.checkOrCreateDirectory(fileStorage)) {
			throw new ReportingDAOException("Unable to resolve storage patth " + fileStoragePath);
		}

		Connection conn = null;
		try {
			if (log.isDebugEnabled()) {
				log.debug("Loading JDBC driver");
			}
			Class.forName(driverName);
			conn = DriverManager.getConnection(connString);
		} catch (SQLException e) {
			log.error(e.getLocalizedMessage(), e);
			throw new ReportingDAOException("Unable to connect to the database", e);
		} catch (ClassNotFoundException e) {
			log.error(e.getLocalizedMessage(), e);
			throw new ReportingDAOException("Unable to load driver class", e);
		}

		if (initScript != null) {

			if (log.isDebugEnabled()) {
				log.debug("Executing database initialization scipt");
			}

			// initialize database
			try {
				SqlFileExecutor executor = new SqlFileExecutor(conn);
				executor.executeScript(initScript);
			} catch (SQLException e) {
				log.error(e.getLocalizedMessage(), e);
				throw new ReportingDAOException("Unable to execute init script", e);
			} catch (IOException e) {
				log.error(e.getLocalizedMessage(), e);
				throw new ReportingDAOException("Unable to read init file", e);
			}
		}

		if (queryTemplates != null) {

			if (log.isDebugEnabled()) {
				log.debug("Loading SQL queries");
			}

			// Load query map
			loadQueryMap(queryTemplates);
		}
	}

	@Override
	public ReportTemplateDAO newReportTemplateDAO() {
		return new DbFsReportTemplateDAO(this, fileStorage);
	}

	@Override
	public ReportDAO newReportDao() {
		return new DbFsReportDAO(this, fileStorage);
	}

	@Override
	public void shutdown() throws ReportingDAOException {
		try {
			Connection conn = DriverManager.getConnection(connString);
			conn.createStatement().execute("SHUTDOWN");
		} catch (SQLException e) {
			throw new ReportingDAOException("Unable to shutdown DAO");
		}
	}

	private void loadQueryMap(String queryTemplates) throws ReportingDAOException {
		try {

			BufferedReader reader = new BufferedReader(new FileReader(queryTemplates));

			String line = null;
			while ((line = reader.readLine()) != null) {

				int delimiter = line.indexOf('=');
				if (delimiter == -1) {
					throw new ReportingDAOException("Incorrect query template file format");
				}
				queries.addQuery(line.substring(0, delimiter), line.substring(delimiter + 1));
			}
			reader.close();
		} catch (IOException e) {
			throw new ReportingDAOException("Unable to read query file");
		}
	}

	static class QueryMap {
		private HashMap<String, String> queries = CollectionUtils.newHashMap();

		private void addQuery(String name, String query) {
			queries.put(name, query);
		}

		public String getQuery(String name) {
			return queries.get(name);
		}
	}

	@Override
	public PreparedStatement createStatement(Connection conn, String queryName) {
		try {
			String insTemplateQuery = queries.getQuery(queryName);
			if (insTemplateQuery == null) {
				log.error("Query " + queryName + " not found in Query repository");
				throw new ResourceException("Query " + queryName + " is not found");
			}
			return conn.prepareStatement(insTemplateQuery);
		} catch (SQLException e) {
			log.error(e.getLocalizedMessage(), e);
			throw new ResourceException("Error creating query " + queryName, e);
		}
	}

	public Connection getConnection() {
		try {
			return DriverManager.getConnection(connString);
		} catch (SQLException e) {
			log.error("Unable to create JDBC connection", e);
			throw new ResourceException("Unable to create JDBC connection", e);
		}
	}

	public void rollback(Connection conn) {
		try {
			conn.rollback();
		} catch (SQLException e) {
			log.error("Unable to rollback a transaction", e);
			throw new ResourceException("Unable to rollback a transaction", e);
		}
	}

	public void commit(Connection conn) {
		try {
			conn.commit();
		} catch (SQLException e) {
			log.error("Unable to commit a transaction", e);
			throw new ResourceException("Unable to commit a transaction", e);
		}
	}

	public void setAutoCommit(Connection conn, boolean autocommit) {
		try {
			conn.setAutoCommit(autocommit);
		} catch (SQLException e) {
			log.error(e.getLocalizedMessage(), e);
			throw new ResourceException("Unable to set connection autocommit mode", e);
		}
	}
}
