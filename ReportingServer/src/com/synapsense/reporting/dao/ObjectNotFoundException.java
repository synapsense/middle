package com.synapsense.reporting.dao;

public class ObjectNotFoundException extends ReportingDAOException {

	private static final long serialVersionUID = 3651618335668494448L;

	public ObjectNotFoundException() {
	}

	public ObjectNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}

	public ObjectNotFoundException(String message) {
		super(message);
	}

	public ObjectNotFoundException(Throwable cause) {
		super(cause);
	}
}
