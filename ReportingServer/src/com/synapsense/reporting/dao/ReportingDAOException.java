package com.synapsense.reporting.dao;

import com.synapsense.reporting.ReportingException;

public class ReportingDAOException extends ReportingException {

	private static final long serialVersionUID = 5151216589208399411L;

	public ReportingDAOException() {
	}

	public ReportingDAOException(String message, Throwable cause) {
		super(message, cause);
	}

	public ReportingDAOException(String message) {
		super(message);
	}

	public ReportingDAOException(Throwable cause) {
		super(cause);
	}
}
