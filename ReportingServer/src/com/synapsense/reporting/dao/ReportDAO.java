package com.synapsense.reporting.dao;

import java.io.Serializable;
import java.util.Collection;

import com.synapsense.reporting.dto.ReportInfo;
import com.synapsense.reporting.dto.ReportTitle;
import com.synapsense.reporting.export.ExportFormat;
import com.synapsense.reporting.model.Report;

public interface ReportDAO extends Serializable {

	Collection<ReportTitle> getGeneratedReports(String reportTemplateName) throws ObjectNotFoundException;

	ReportInfo getReportInfo(int reportId) throws ObjectNotFoundException;

	Report getReportObject(int reportId) throws ObjectNotFoundException;

	void saveReport(ReportInfo reportInfo, Report reportObject) throws ObjectExistsException, ObjectNotFoundException,
	        IncorrectPropertyValueException;

	void removeReport(int reportId) throws ObjectNotFoundException;

	String exportReport(int reportId, ExportFormat format) throws ObjectNotFoundException;

	String getExportRootPath();
}
