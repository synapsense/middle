package com.synapsense.reporting.dao;

public class ObjectExistsException extends ReportingDAOException {

	private static final long serialVersionUID = 2438479941460717158L;

	public ObjectExistsException() {
	}

	public ObjectExistsException(String message, Throwable cause) {
		super(message, cause);
	}

	public ObjectExistsException(String message) {
		super(message);
	}

	public ObjectExistsException(Throwable cause) {
		super(cause);
	}
}
