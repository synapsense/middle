package com.synapsense.reporting.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;

public interface ConnectionManager {

	Connection getConnection();

	void setAutoCommit(Connection conn, boolean autocommit);

	void commit(Connection conn);

	void rollback(Connection conn);

	PreparedStatement createStatement(Connection conn, String query);
}
