package com.synapsense.reporting.dao;

import com.synapsense.reporting.ReportingRuntimeException;

public class DAORuntimeException extends ReportingRuntimeException {

	private static final long serialVersionUID = 1042912078648382832L;

	public DAORuntimeException() {
	}

	public DAORuntimeException(String message) {
		super(message);
	}

	public DAORuntimeException(Throwable cause) {
		super(cause);
	}

	public DAORuntimeException(String message, Throwable cause) {
		super(message, cause);
	}
}
