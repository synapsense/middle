package com.synapsense.reporting.dao;

import java.io.Serializable;
import java.util.Collection;

import com.synapsense.reporting.dto.ReportTemplateInfo;

public interface ReportTemplateDAO extends Serializable {

	void createReportTemplate(ReportTemplateInfo reportTemplateInfo, String reportTemplateDesign)
	        throws ObjectExistsException;

	void updateReportTemplate(ReportTemplateInfo reportTemplateInfo, String reportTemplateDesign)
	        throws ObjectNotFoundException, ObjectExistsException;

	void removeReportTemplate(String reportTemplateName) throws ObjectNotFoundException;

	Collection<String> getReportTemplates();

	String getReportTemplateDesign(String reportTemplateName) throws ObjectNotFoundException;

	ReportTemplateInfo getReportTemplateInfo(String reportTemplateName) throws ObjectNotFoundException;
}
