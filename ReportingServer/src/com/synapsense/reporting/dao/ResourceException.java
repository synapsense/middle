package com.synapsense.reporting.dao;

public class ResourceException extends DAORuntimeException {

	private static final long serialVersionUID = 6340025844623362211L;

	public ResourceException() {
	}

	public ResourceException(String message, Throwable cause) {
		super(message, cause);
	}

	public ResourceException(String message) {
		super(message);
	}

	public ResourceException(Throwable cause) {
		super(cause);
	}
}
