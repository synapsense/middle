package com.synapsense.reporting.app;

import java.io.FileReader;
import java.io.IOException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicReference;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.log4j.PropertyConfigurator;
import org.quartz.JobExecutionException;
import org.quartz.SchedulerException;
import org.quartz.impl.StdSchedulerFactory;

import com.synapsense.commandengine.DefaultCommandManager;
import com.synapsense.reporting.ReportingException;
import com.synapsense.reporting.commands.ReportingCommandManager;
import com.synapsense.reporting.configuration.Configuration;
import com.synapsense.reporting.dao.DbFsDAOFactory;
import com.synapsense.reporting.dao.ReportingDAOFactory;
import com.synapsense.reporting.datasource.ReportingDataSource;
import com.synapsense.reporting.management.JSReportManager;
import com.synapsense.reporting.remote.RemoteReportManager;
import com.synapsense.reporting.remote.RemoteReportManagerImpl;
import com.synapsense.reporting.scheduling.JSSchedulingManager;
import com.synapsense.reporting.scheduling.ScheduledTaskRunner;
import com.synapsense.reporting.dto.ReportTaskInfo;

public class ReportingServer {

	private static Log log = LogFactory.getLog(ReportingServer.class);

	private static AtomicReference<ReportingServer> instance = new AtomicReference<ReportingServer>();

	public static final String REPORT_MANAGER_BINDING = "ReportManager";

	// Static reference is necessary to avoid java.rmi.NoSuchObjectException
	private static RemoteReportManager rm = null;

	private int rmiPort;

	private ScheduledTaskRunner taskRunner;

	private ReportingServer() {
	}

	public static ReportingServer getInstance() {
		if (instance.get() == null) {
			instance.compareAndSet(null, new ReportingServer());
		}
		return instance.get();
	}

	public void start(Properties props) throws ReportingException {

		if (rm != null) {
			throw new IllegalStateException("The server is already started");
		}

		log.info("Starting command executor");
		ReportingCommandManager executor = new ReportingCommandManager();
		executor.setManager(new DefaultCommandManager(Integer.parseInt(props.getProperty("threadpool.tread.number"))));

		log.info("Starting OLAP server");
		ReportingDataSource.getInstance().setConnectionProperties(props);
		ReportingDataSource.getInstance().getMondrianConnection();

		log.info("Starting DAO");
		String storage = Configuration.getProperty("root.dir");
		ReportingDAOFactory daoFactory = new DbFsDAOFactory(Configuration.getProperty("jdbc.driver"),
		        Configuration.getProperty("database.URL"), null /* "./conf/database/init_db.sql" */,
		        "./conf/database/query.templates", storage + "/export");

		log.info("Starting scheduling server");
		JSSchedulingManager scheduler = null;
		try {
			Properties quartzProps = new Properties();
			quartzProps.load(new FileReader("./conf/quartz.properties"));
			scheduler = new JSSchedulingManager();
			scheduler.setScheduler(new StdSchedulerFactory(quartzProps).getScheduler());
			scheduler.start();

		} catch (IOException e) {
			throw new ReportingException("Unable to load scheduler properties", e);
		} catch (SchedulerException e) {
			throw new ReportingException("Unable to start scheduler", e);
		}

		log.info("Starting Report manager");
		final JSReportManager manager = new JSReportManager();
		manager.setCommandExecutor(executor);
		manager.setReportTemplateDAO(daoFactory.newReportTemplateDAO());
		manager.setReportDAO(daoFactory.newReportDao());
		manager.setScheduler(scheduler);
		taskRunner = new ScheduledTaskRunner() {

			@Override
			public void runReportTask(ReportTaskInfo taskInfo) throws JobExecutionException {
				try {
					manager.runReportTask(taskInfo);
				} catch (ReportingException e) {
					throw new JobExecutionException(e.getMessage(), e);
				}
			}
		};

		log.info("Registering remote objects");
		rm = new RemoteReportManagerImpl(manager);
		RemoteReportManager stub = null;
		try {
			stub = (RemoteReportManager) UnicastRemoteObject.exportObject(rm, 0);
			rmiPort = Integer.parseInt(props.getProperty("rmi.port"));
			Registry registry = LocateRegistry.getRegistry(rmiPort);
			registry.rebind(REPORT_MANAGER_BINDING, stub);
		} catch (RemoteException e) {
			throw new ReportingException("Unable to start reporting server", e);
		}
	}

	public ScheduledTaskRunner getTaskRunner() {
		return taskRunner;
	}

	public void stop() throws ReportingException {
		try {
			if (rm == null) {
				throw new IllegalStateException("Server is not started");
			}
			rm = null;
			Registry registry = LocateRegistry.getRegistry(rmiPort);
			try {
				registry.unbind(REPORT_MANAGER_BINDING);
			} catch (NotBoundException e) {
				throw new ReportingException("Unable to stop reporting server", e);
			}
		} catch (RemoteException e) {
			throw new ReportingException("Unable to stop reporting server", e);
		}
	}

	public static void main(String[] args) {

		if (args.length != 1) {
			log.error("Unexpected number of input parameters. Pass one parameter - path to configuration file");
			System.exit(-1);
		}

		PropertyConfigurator.configure("conf/log4j.properties");

		log.info("Loading configuration");
		Configuration.load("conf/reporting.properties");

		log.info("Starting web server");
		WebServer webServer = new WebServer(Configuration.getProperty("root.dir") + "/export",
		        Integer.parseInt(Configuration.getProperty("port")));

		try {
			webServer.startServer();
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			System.exit(-1);
		}

		// Create RMI registry
		try {
			LocateRegistry.createRegistry(Integer.parseInt(Configuration.getProperty("rmi.port")));
		} catch (RemoteException e) {
			log.error(e.getMessage(), e);
			System.exit(-1);
		}

		// Starting reporting server
		ReportingServer reportingServer = ReportingServer.getInstance();
		try {
			reportingServer.start(Configuration.getProperties());
			log.info("Reporting server started");
		} catch (ReportingException e) {
			log.error(e.getMessage(), e.getCause());
			System.exit(-1);
		}
	}
}
