package com.synapsense.reporting.app;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.log4j.PropertyConfigurator;

import com.synapsense.reporting.ReportingRuntimeException;
import com.synapsense.reporting.configuration.Configuration;
import com.synapsense.reporting.utils.SqlFileExecutor;

public class DbScriptRunner {

	private static final Log log = LogFactory.getLog(DbScriptRunner.class);

	public static void main(String[] args) {
		PropertyConfigurator.configure("./conf/log4j.properties");
		// load configuration
		log.info("Database initialization started");
		Configuration.load("./conf/reporting.properties");

		try {
			// load jdbc driver
			try {
				Class.forName(Configuration.getProperty("jdbc.driver"));
			} catch (ClassNotFoundException e) {
				throw new ReportingRuntimeException("Error loading jdbc driver class", e);
			}

			// load connection
			Connection conn = null;
			try {
				conn = DriverManager.getConnection(Configuration.getProperty("database.URL"),
				        Configuration.getProperty("database.user"), Configuration.getProperty("database.password"));
			} catch (SQLException e) {
				throw new ReportingRuntimeException("Error creating jdbc connection", e);
			}

			// Run script
			SqlFileExecutor fileExecutor = new SqlFileExecutor(conn);
			try {
				fileExecutor.executeScript("./conf/database/init_db.sql");
				conn.createStatement().execute("SHUTDOWN");
			} catch (SQLException e) {
				throw new ReportingRuntimeException("Error executing init db script", e);
			} catch (IOException e) {
				throw new ReportingRuntimeException("IO Error", e);
			}
			log.info("Database initialization completed");
			// ESCA-JAVA0266:
			System.out.println("Database initialized successfully");
		} catch (ReportingRuntimeException e) {
			log.error(e.getMessage(), e);
			// ESCA-JAVA0266:
			System.out.println("Database script running error, see log for details");
			System.exit(-1);
		}
	}

	private DbScriptRunner() {
	}
}
