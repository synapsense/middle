package com.synapsense.reporting.app;

import org.mortbay.http.handler.ResourceHandler;
import org.mortbay.jetty.Server;
import org.mortbay.jetty.servlet.ServletHttpContext;

public class WebServer {

	private int port;
	private String srcDir;

	private Server server;

	public WebServer(String srcDir, int port) {
		this.port = port;
		this.srcDir = srcDir;
	}

	public void startServer() throws Exception {
		server = new Server();

		server.addListener(":" + port);

		ResourceHandler handler = new ResourceHandler();

		ServletHttpContext context = (ServletHttpContext) server.getContext("/");
		context.addHandler(handler);
		context.setResourceBase(srcDir);

		server.start();
	}

	public void stop() throws Exception {
		server.stop();
	}
}
