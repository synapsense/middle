package com.synapsense.reporting;

import java.io.Serializable;
import java.util.Collection;

import com.synapsense.reporting.commands.ReportCommandProxy;
import com.synapsense.reporting.dao.ObjectExistsException;
import com.synapsense.reporting.dao.ObjectNotFoundException;
import com.synapsense.reporting.dto.ReportInfo;
import com.synapsense.reporting.dto.ReportTaskInfo;
import com.synapsense.reporting.dto.ReportTemplateInfo;
import com.synapsense.reporting.dto.ReportTitle;
import com.synapsense.reporting.dto.ScheduledTaskInfo;
import com.synapsense.reporting.export.ExportFormat;
import com.synapsense.reporting.model.ReportCompilationException;

public interface ReportManager extends Serializable {

	void addReportTemplate(String reportTemplateDesign) throws ReportCompilationException, ObjectExistsException;

	void updateReportTemplate(String reportTemplateName, String reportTemplateDesign)
	        throws ReportCompilationException, ObjectNotFoundException, ObjectExistsException;

	void removeReportTemplate(String reportTemplateName) throws ReportingException;

	Collection<String> getAvailableReportTemplates() throws ReportingException;

	ReportTemplateInfo getReportTemplateInfo(String reportTemplateName) throws ObjectNotFoundException;

	String getReportTemplateDesign(String reportTemplateName) throws ObjectNotFoundException;

	Collection<ReportTitle> getGeneratedReports(String reportTemplateName) throws ObjectNotFoundException;

	ReportInfo getReportInfo(int reportId) throws ObjectNotFoundException;

	void removeReport(int reportId) throws ObjectNotFoundException;

	ReportCommandProxy runReportGeneration(ReportInfo reportInfo) throws ReportingException;

	ReportCommandProxy exportReport(int reportId, ExportFormat format) throws ReportingException;

	ReportCommandProxy emailReport(int reportId, Collection<String> destinations) throws ReportingException;

	ReportCommandProxy runReportTask(ReportTaskInfo taskInfo) throws ReportingException;

	void scheduleReportTask(ScheduledTaskInfo taskInfo) throws ReportingException;

	Collection<ScheduledTaskInfo> getScheduledReportTasks(String reportTemplateName) throws ReportingException;

	void removeScheduledReportTask(String taskName, String reportTemplateName) throws ReportingException;

	void cancelReportCommand(int commandId) throws ReportingException;

	void clearReportCommands() throws ReportingException;

	Collection<ReportCommandProxy> getReportCommands() throws ReportingException;

	String getServerUrl() throws ReportingException;
}