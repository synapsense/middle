package com.synapsense.reporting.management;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Collection;
import java.util.Set;
import java.util.Map.Entry;

import com.synapsense.commandengine.CompositeCommand;
import com.synapsense.reporting.ReportManager;
import com.synapsense.reporting.ReportingException;
import com.synapsense.reporting.ReportingRuntimeException;
import com.synapsense.reporting.commands.CommandFactory;
import com.synapsense.reporting.commands.ReportCommandProxy;
import com.synapsense.reporting.commands.ReportingCommandManager;
import com.synapsense.reporting.configuration.Configuration;
import com.synapsense.reporting.dao.ObjectExistsException;
import com.synapsense.reporting.dao.ObjectNotFoundException;
import com.synapsense.reporting.dao.ReportDAO;
import com.synapsense.reporting.dao.ReportTemplateDAO;
import com.synapsense.reporting.dto.ReportInfo;
import com.synapsense.reporting.dto.ReportTaskInfo;
import com.synapsense.reporting.dto.ReportTemplateInfo;
import com.synapsense.reporting.dto.ReportTitle;
import com.synapsense.reporting.dto.ScheduledTaskInfo;
import com.synapsense.reporting.export.ExportFormat;
import com.synapsense.reporting.model.ReportCompilationException;
import com.synapsense.reporting.model.ReportParameter;
import com.synapsense.reporting.model.ReportTemplate;
import com.synapsense.reporting.scheduling.SchedulingManager;
import com.synapsense.reporting.utils.algo.ForEach;
import com.synapsense.reporting.utils.algo.UnaryFunctor;

public class JSReportManager implements ReportManager {

	private static final long serialVersionUID = 2522537567570327160L;

	private ReportTemplateDAO rtDAO;

	private ReportDAO reportDAO;

	private ReportingCommandManager executor;

	private SchedulingManager scheduler;

	@Override
	public void addReportTemplate(String reportTemplateDesign) throws ReportCompilationException, ObjectExistsException {
		ReportTemplate reportTemplate = new ReportTemplate(reportTemplateDesign);
		rtDAO.createReportTemplate(new ReportTemplateInfo(reportTemplate.getName(), reportTemplate.getParameters()),
		        reportTemplateDesign);
	}

	@Override
	public void updateReportTemplate(String reportTemplateName, String reportTemplateDesign)
	        throws ReportCompilationException, ObjectNotFoundException, ObjectExistsException {
		ReportTemplate reportTemplate = new ReportTemplate(reportTemplateDesign);
		ReportTemplateInfo rtInfo = rtDAO.getReportTemplateInfo(reportTemplateName);
		rtInfo.clearParameters();
		rtInfo.setReportTemplateName(reportTemplate.getName());
		for (ReportParameter param : reportTemplate.getParameters()) {
			rtInfo.addParameter(param);
		}

		rtDAO.updateReportTemplate(rtInfo, reportTemplateDesign);
	}

	@Override
	public void removeReportTemplate(String reportTemplateName) throws ReportingException {
		rtDAO.removeReportTemplate(reportTemplateName);
		scheduler.removeScheduledTasks(reportTemplateName);
	}

	@Override
	public Collection<String> getAvailableReportTemplates() {
		return rtDAO.getReportTemplates();
	}

	@Override
	public ReportTemplateInfo getReportTemplateInfo(String reportTemplateName) throws ObjectNotFoundException {
		return rtDAO.getReportTemplateInfo(reportTemplateName);
	}

	@Override
	public String getReportTemplateDesign(String reportTemplateName) throws ObjectNotFoundException {
		return rtDAO.getReportTemplateDesign(reportTemplateName);
	}

	@Override
	public Collection<ReportTitle> getGeneratedReports(String reportTemplateName) throws ObjectNotFoundException {
		return reportDAO.getGeneratedReports(reportTemplateName);
	}

	@Override
	public void removeReport(int reportId) throws ObjectNotFoundException {
		reportDAO.removeReport(reportId);
	}

	@Override
	public ReportCommandProxy runReportGeneration(ReportInfo reportInfo) throws ReportingException {
		return runReportTask(ReportTaskInfo.newInstance().addReportGeneration(reportInfo));
	}

	@Override
	public ReportCommandProxy exportReport(int reportId, ExportFormat format) throws ReportingException {
		return runReportTask(ReportTaskInfo.newInstance().addExportTask(reportId, format));
	}

	@Override
	public ReportCommandProxy emailReport(int reportId, Collection<String> destinations) throws ReportingException {
		return runReportTask(ReportTaskInfo.newInstance().addEmailTask(reportId, destinations));
	}

	@Override
	public ReportCommandProxy runReportTask(final ReportTaskInfo taskInfo) throws ReportingException {

		final CompositeCommand complexCommand = new CompositeCommand();

		// Create create/export/send complex command
		new ForEach<ReportInfo>(taskInfo.getReportsGeneration()).process(new UnaryFunctor<ReportInfo>() {
			@Override
			public void process(final ReportInfo reportInfo) {
				// Add report generation command
				complexCommand.addCommand(CommandFactory.newReportGenerationCommand(reportInfo, rtDAO, reportDAO));
				// Add report export commands
				new ForEach<ExportFormat>(taskInfo.getExportFormats()).process(new UnaryFunctor<ExportFormat>() {
					@Override
					public void process(ExportFormat format) {
						complexCommand.addCommand(CommandFactory.newReportExportCommand(reportDAO, reportInfo, format));
					}
				});
				// Add send email command
				if (!taskInfo.getEmailDestinations().isEmpty()) {
					complexCommand.addCommand(CommandFactory.newReportMailCommand(reportDAO,
					        Configuration.getProperties(), taskInfo.getEmailDestinations(), reportInfo));
				}
			}
		});

		// Process separate export commands
		for (Entry<Integer, Set<ExportFormat>> entry : taskInfo.getExportTasks().entrySet()) {
			for (ExportFormat format : entry.getValue()) {
				complexCommand.addCommand(CommandFactory.newReportExportCommand(reportDAO,
				        reportDAO.getReportInfo(entry.getKey()), format));
			}
		}

		// Process separate email tasks
		for (Entry<Integer, Set<String>> entry : taskInfo.getEmailTasks().entrySet()) {
			complexCommand.addCommand(CommandFactory.newReportMailCommand(reportDAO, Configuration.getProperties(),
			        entry.getValue(), reportDAO.getReportInfo(entry.getKey())));
		}

		if (complexCommand.getCommandsSize() == 0) {
			throw new ReportingException("Unable to create empty task");
		}

		if (complexCommand.getCommandsSize() == 1) {
			// No need to create a complex command
			return executor.executeCommand(complexCommand.getCommands().get(0), taskInfo);
		}
		// Finally execute the command
		return executor.executeCommand(complexCommand, taskInfo);
	}

	@Override
	public Collection<ScheduledTaskInfo> getScheduledReportTasks(String reportTemplateName) throws ReportingException {
		return scheduler.getScheduledTasks(reportTemplateName);
	}

	@Override
	public void removeScheduledReportTask(String taskName, String reportTemplateName) throws ReportingException {
		scheduler.removeScheduledTask(taskName, reportTemplateName);
	}

	@Override
	public void scheduleReportTask(ScheduledTaskInfo taskInfo) throws ReportingException {
		scheduler.scheduleReportTask(taskInfo);
	}

	@Override
	public void cancelReportCommand(int commandId) {
		executor.cancelCommand(commandId);
	}

	@Override
	public void clearReportCommands() {
		executor.clearCommands();
	}

	@Override
	public Collection<ReportCommandProxy> getReportCommands() {
		return executor.getManagedCommands();
	}

	@Override
	public String getServerUrl() {
		try {
			return "http://" + InetAddress.getLocalHost().getHostName() + ":" + Configuration.getProperty("port");
		} catch (UnknownHostException e) {
			throw new ReportingRuntimeException(e.getMessage(), e);
		}
	}

	@Override
	public ReportInfo getReportInfo(int reportId) throws ObjectNotFoundException {
		return reportDAO.getReportInfo(reportId);
	}

	public void setReportTemplateDAO(ReportTemplateDAO rtDao) {
		this.rtDAO = rtDao;
	}

	public void setReportDAO(ReportDAO reportDao) {
		this.reportDAO = reportDao;
	}

	public void setCommandExecutor(ReportingCommandManager commandExecutor) {
		this.executor = commandExecutor;
	}

	public void setScheduler(SchedulingManager scheduler) {
		this.scheduler = scheduler;
	}
}
