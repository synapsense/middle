package com.synapsense.reporting.remote;

import java.rmi.RemoteException;

import com.synapsense.commandengine.CommandStatus;
import com.synapsense.reporting.commands.ReportCommandProxy;
import com.synapsense.reporting.dto.ReportTaskInfo;

public class RemoteCommandProxyImpl implements RemoteCommandProxy {

	private ReportCommandProxy proxy;

	@Override
	public String getCommandName() throws RemoteException {
		return proxy.getCommandName();
	}

	public RemoteCommandProxyImpl(ReportCommandProxy proxy) {
		this.proxy = proxy;
	}

	@Override
	public double getProgress() throws RemoteException {
		return proxy.getProgress();
	}

	@Override
	public CommandStatus getStatus() throws RemoteException {
		return proxy.getStatus();
	}

	@Override
	public void cancel() throws RemoteException {
		proxy.cancel();
	}

	@Override
	public ReportTaskInfo getTaskInfo() throws RemoteException {
		return proxy.getTaskInfo();
	}
}
