package com.synapsense.reporting.remote;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Collection;

import com.synapsense.reporting.ReportManager;
import com.synapsense.reporting.ReportingException;
import com.synapsense.reporting.commands.ReportCommandProxy;
import com.synapsense.reporting.dto.ReportInfo;
import com.synapsense.reporting.dto.ReportTaskInfo;
import com.synapsense.reporting.dto.ReportTemplateInfo;
import com.synapsense.reporting.dto.ReportTitle;
import com.synapsense.reporting.dto.ScheduledTaskInfo;
import com.synapsense.reporting.export.ExportFormat;

public class RemoteReportManagerImpl implements RemoteReportManager {

	private ReportManager manager;

	public RemoteReportManagerImpl(ReportManager manager) {
		this.manager = manager;
	}

	@Override
	public RemoteCommandProxy exportReport(int reportId, ExportFormat format) throws ReportingException,
	        RemoteException {
		RemoteCommandProxy proxy = new RemoteCommandProxyImpl(manager.exportReport(reportId, format));
		return (RemoteCommandProxy) UnicastRemoteObject.exportObject(proxy, 0);
	}

	public Collection<String> getAvailableReportTemplates() throws RemoteException, ReportingException {
		return manager.getAvailableReportTemplates();
	}

	@Override
	public Collection<ReportTitle> getGeneratedReports(String reportTemplateName) throws ReportingException,
	        RemoteException {
		return manager.getGeneratedReports(reportTemplateName);
	}

	@Override
	public ReportTemplateInfo getReportTemplateInfo(String reportTemplateName) throws ReportingException,
	        RemoteException {
		return manager.getReportTemplateInfo(reportTemplateName);
	}

	@Override
	public RemoteCommandProxy runReportGeneration(ReportInfo reportInfo) throws ReportingException, RemoteException {
		RemoteCommandProxy proxy = new RemoteCommandProxyImpl(manager.runReportGeneration(reportInfo));
		return (RemoteCommandProxy) UnicastRemoteObject.exportObject(proxy, 0);
	}

	@Override
	public RemoteCommandProxy emailReports(int reportId, Collection<String> destinations) throws ReportingException,
	        RemoteException {
		RemoteCommandProxy proxy = new RemoteCommandProxyImpl(manager.emailReport(reportId, destinations));
		return (RemoteCommandProxy) UnicastRemoteObject.exportObject(proxy, 0);
	}

	@Override
	public RemoteCommandProxy runComplexReportTask(ReportTaskInfo context) throws ReportingException, RemoteException {
		RemoteCommandProxy proxy = new RemoteCommandProxyImpl(manager.runReportTask(context));
		return (RemoteCommandProxy) UnicastRemoteObject.exportObject(proxy, 0);
	}

	@Override
	public void cancelReportCommand(int commandId) throws RemoteException, ReportingException {
		manager.cancelReportCommand(commandId);
	}

	@Override
	public void clearReportCommands() throws RemoteException, ReportingException {
		manager.clearReportCommands();
	}

	@Override
	public Collection<RemoteCommandProxy> getReportCommands() throws RemoteException, ReportingException {
		Collection<RemoteCommandProxy> remoteStubs = new ArrayList<RemoteCommandProxy>();

		Collection<ReportCommandProxy> proxies = manager.getReportCommands();
		for (ReportCommandProxy proxy : proxies) {
			RemoteCommandProxy remoteStub = new RemoteCommandProxyImpl(proxy);
			remoteStubs.add((RemoteCommandProxy) UnicastRemoteObject.exportObject(remoteStub, 0));

		}
		return remoteStubs;
	}

	@Override
	public String getServerUrl() throws ReportingException, RemoteException {
		return manager.getServerUrl();
	}

	@Override
	public void addReportTemplate(String reportTemplateDesign) throws ReportingException, RemoteException {
		manager.addReportTemplate(reportTemplateDesign);
	}

	@Override
	public String getReportTemplateDesign(String reportTemplateName) throws ReportingException, RemoteException {
		return manager.getReportTemplateDesign(reportTemplateName);
	}

	@Override
	public void removeReportTemplate(String reportTemplateName) throws ReportingException, RemoteException {
		manager.removeReportTemplate(reportTemplateName);
	}

	@Override
	public void updateReportTemplate(String reportTemplateName, String reportTemplateDesign) throws ReportingException,
	        RemoteException {
		manager.updateReportTemplate(reportTemplateName, reportTemplateDesign);
	}

	@Override
	public void removeReport(int reportId) throws ReportingException {
		manager.removeReport(reportId);
	}

	@Override
	public Collection<ScheduledTaskInfo> getScheduledReportTasks(String reportTemplateName) throws ReportingException,
	        RemoteException {
		return manager.getScheduledReportTasks(reportTemplateName);
	}

	@Override
	public void removeScheduledReportTask(String taskName, String reportTemplateName) throws ReportingException,
	        RemoteException {
		manager.removeScheduledReportTask(taskName, reportTemplateName);
	}

	@Override
	public void scheduleReportTask(ScheduledTaskInfo taskInfo) throws ReportingException, RemoteException {
		manager.scheduleReportTask(taskInfo);
	}

	@Override
	public ReportInfo getReportInfo(int reportId) throws ReportingException, RemoteException {
		return manager.getReportInfo(reportId);
	}
}
