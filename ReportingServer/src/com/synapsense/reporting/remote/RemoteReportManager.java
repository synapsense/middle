package com.synapsense.reporting.remote;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Collection;

import com.synapsense.reporting.ReportingException;
import com.synapsense.reporting.dto.ReportInfo;
import com.synapsense.reporting.dto.ReportTaskInfo;
import com.synapsense.reporting.dto.ReportTemplateInfo;
import com.synapsense.reporting.dto.ReportTitle;
import com.synapsense.reporting.dto.ScheduledTaskInfo;
import com.synapsense.reporting.export.ExportFormat;

public interface RemoteReportManager extends Remote {

	void addReportTemplate(String reportTemplateDesign) throws ReportingException, RemoteException;

	void updateReportTemplate(String reportTemplateName, String reportTemplateDesign) throws ReportingException,
	        RemoteException;

	void removeReportTemplate(String reportTemplateName) throws ReportingException, RemoteException;

	Collection<String> getAvailableReportTemplates() throws RemoteException, ReportingException;

	ReportTemplateInfo getReportTemplateInfo(String reportTemplateName) throws ReportingException, RemoteException;

	String getReportTemplateDesign(String reportTemplateName) throws ReportingException, RemoteException;

	Collection<ReportTitle> getGeneratedReports(String reportTemplateName) throws ReportingException, RemoteException;

	ReportInfo getReportInfo(int reportId) throws ReportingException, RemoteException;

	void removeReport(int reportId) throws ReportingException, RemoteException;

	RemoteCommandProxy runReportGeneration(ReportInfo reportInfo) throws ReportingException, RemoteException;

	RemoteCommandProxy runComplexReportTask(ReportTaskInfo taskInfo) throws ReportingException, RemoteException;

	RemoteCommandProxy exportReport(int reportId, ExportFormat format) throws ReportingException, RemoteException;

	RemoteCommandProxy emailReports(int reportId, Collection<String> destinations) throws ReportingException,
	        RemoteException;

	void scheduleReportTask(ScheduledTaskInfo taskInfo) throws ReportingException, RemoteException;

	Collection<ScheduledTaskInfo> getScheduledReportTasks(String reportTemplateName) throws ReportingException,
	        RemoteException;

	void removeScheduledReportTask(String taskName, String reportTemplateName) throws ReportingException,
	        RemoteException;

	void cancelReportCommand(int commandId) throws ReportingException, RemoteException;

	void clearReportCommands() throws ReportingException, RemoteException;

	Collection<RemoteCommandProxy> getReportCommands() throws ReportingException, RemoteException;

	String getServerUrl() throws ReportingException, RemoteException;
}
