package com.synapsense.reporting.remote;

import java.rmi.Remote;
import java.rmi.RemoteException;

import com.synapsense.commandengine.CommandStatus;
import com.synapsense.reporting.dto.ReportTaskInfo;

public interface RemoteCommandProxy extends Remote {

	String getCommandName() throws RemoteException;

	CommandStatus getStatus() throws RemoteException;

	double getProgress() throws RemoteException;

	void cancel() throws RemoteException;

	ReportTaskInfo getTaskInfo() throws RemoteException;
}
