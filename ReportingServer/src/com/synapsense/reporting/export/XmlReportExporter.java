package com.synapsense.reporting.export;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;

import com.synapsense.reporting.ReportingRuntimeException;
import com.synapsense.reporting.model.Report;
import com.synapsense.reporting.model.ReportExportException;
import com.synapsense.reporting.model.JSReport;

public class XmlReportExporter implements ReportExporter {

	@Override
	public void export(JSReport report, String outFileName) throws ReportExportException {
		try {
			JasperExportManager.exportReportToXmlFile(report.getJasperPrint(), outFileName, false);
		} catch (JRException e) {
			throw new ReportExportException("Unable to export \"" + report.getJasperPrint().getName()
			        + "\"report to xml", e);
		}
	}

	@Override
	public void export(Report report, String outFilePath) throws ReportExportException {
		throw new ReportingRuntimeException("Dispatching error when exporting report");
	}
}
