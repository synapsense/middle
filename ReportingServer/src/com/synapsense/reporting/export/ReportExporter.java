package com.synapsense.reporting.export;

import com.synapsense.reporting.model.Report;
import com.synapsense.reporting.model.ReportExportException;
import com.synapsense.reporting.model.JSReport;

public interface ReportExporter {

	void export(Report report, String outFilePath) throws ReportExportException;

	void export(JSReport report, String outFilePath) throws ReportExportException;
}
