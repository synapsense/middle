package com.synapsense.reporting.datasource;

import java.util.Properties;
import java.util.concurrent.atomic.AtomicReference;

import mondrian.olap.Connection;
import mondrian.olap.DriverManager;
import mondrian.olap.Util;
import mondrian.olap.Util.PropertyList;
import mondrian.rolap.RolapConnectionProperties;

public class ReportingDataSource {

	private static AtomicReference<ReportingDataSource> ref = new AtomicReference<ReportingDataSource>();

	public static ReportingDataSource getInstance() {
		if (ref.get() == null) {
			ref.compareAndSet(null, new ReportingDataSource());
		}
		return ref.get();
	}

	private PropertyList connProps;

	private ReportingDataSource() {
	}

	public Connection getMondrianConnection() {
		if (connProps == null) {
			return null;
		}
		return DriverManager.getConnection(connProps, null);
	}

	public void setConnectionProperties(Properties props) {
		connProps = new Util.PropertyList();
		connProps.put(RolapConnectionProperties.Provider.toString(), "Mondrian");
		connProps.put(RolapConnectionProperties.Jdbc.toString(),
		        props.getProperty(RolapConnectionProperties.Jdbc.toString()));
		connProps.put(RolapConnectionProperties.JdbcUser.toString(),
		        props.getProperty(RolapConnectionProperties.JdbcUser.toString()));
		connProps.put(RolapConnectionProperties.JdbcPassword.toString(),
		        props.getProperty(RolapConnectionProperties.JdbcPassword.toString()));
		connProps.put(RolapConnectionProperties.JdbcDrivers.toString(),
		        props.getProperty(RolapConnectionProperties.JdbcDrivers.toString()));
		connProps.put(RolapConnectionProperties.Catalog.toString(),
		        props.getProperty(RolapConnectionProperties.Catalog.toString()));
	}
}
