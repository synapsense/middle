package com.synapsense.reporting.datasource;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.synapsense.dto.Alert;
import com.synapsense.dto.AlertFilterBuilder;
import com.synapsense.dto.CollectionTO;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.AlertServiceSystemException;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.exception.PropertyNotFoundException;
import com.synapsense.exception.UnableToConvertPropertyException;

import com.synapsense.service.AlertService;
import com.synapsense.service.Environment;
import com.synapsense.service.UserManagementService;
import com.synapsense.util.CollectionUtils;
import com.synapsense.util.JNDINames;
import com.synapsense.util.ServerLoginModule;

public class AlertDataSource implements JRDataSource {

	private static HashMap<String, String> namesMap = new HashMap<>();
	private static AlertService alertService;
	private static Environment env;
	private static UserManagementService userManagement;
	public static final String ALERT_DESCRIPTION_TYPE_NAME = "ALERT_TYPE";
	public static final String DEFAULT_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
	public static final String DEFAULT_ALERT_STATUS_TYPE = "ALL";
	public static final String DEFAULT_ALERT_PRIORITY_TYPE = "ALL";
	private static InitialContext ctx;
	private static String mPriority;
	
	static {
		namesMap.put("Status", "status");
		namesMap.put("Alert Name", "name");
		namesMap.put("Alert Message", "message");
		namesMap.put("Priority", "priority");
		namesMap.put("Object", "hostTO");
		namesMap.put("Timestamp", "time");
		namesMap.put("Site", "site");
	}
	
	private Iterator<CollectionTO> values;
	
	private CollectionTO current;

	public AlertDataSource() throws Exception {
		values = getData(DEFAULT_ALERT_STATUS_TYPE, new Date(0L), new Date()).iterator();
	}
	
	public AlertDataSource(Date startDate) throws Exception {
		values = getData(DEFAULT_ALERT_STATUS_TYPE, startDate, new Date()).iterator();
	}

	public AlertDataSource(String status, String priority, Date startDate, Date endDate) throws Exception {
		mPriority = priority;
		values = getData(status, startDate, endDate).iterator();
	}


	public AlertDataSource(String startYear, String startMonth, String startDay, String endYear, String endMonth, String endDay) throws Exception {
		values = getData(DEFAULT_ALERT_STATUS_TYPE, setDate(startYear, startMonth, startDay), setDate(endYear, endMonth, endDay)).iterator();
	}

	
	
	@Override
	public Object getFieldValue(JRField field) throws JRException {
		String name = namesMap.get(field.getName());
		if (name != null) {
			//return current;
			ValueTO vTo = current.getSinglePropValue(name);
			if (vTo != null) {
				return vTo.getValue();
			}
		}
		return null;
	}

	@Override
	public boolean next() throws JRException {
		if (values.hasNext()) {
			current = values.next();
			return true;
		}
		return false;
	}
	
	public static Collection<CollectionTO> getAlertsData(Collection<Alert> alerts) 
			throws ObjectNotFoundException, PropertyNotFoundException, UnableToConvertPropertyException{
		Collection<CollectionTO> result = CollectionUtils.newList();
		if (mPriority == null){
			mPriority = DEFAULT_ALERT_PRIORITY_TYPE;
		}
		for (Alert alert : alerts){
			TO<?> hostObj = alert.getHostTO();
			CollectionTO alertData = new CollectionTO(hostObj);
			alertData.addAll(alertToProps(alert));
			if (mPriority.equals(DEFAULT_ALERT_PRIORITY_TYPE) || alertData.getSinglePropValue("priority").getValue().equals(mPriority)){
				result.add(alertData);
			}
		}
		return result;		
	}
	
	public static List<ValueTO> alertToProps(Alert alert) throws ObjectNotFoundException, PropertyNotFoundException, UnableToConvertPropertyException {
		List<ValueTO> values = CollectionUtils.newArrayList(10);
		values.add(new ValueTO("name", alert.getName()));

		TO<?> alertTypeRef = findAlertTypeByName(env, alert.getAlertType());
		if (alertTypeRef == null) {
			throw new AlertServiceSystemException("Alert type  :" + alert.getAlertType() + " does not exist");
		}
		values.add(new ValueTO("type", alertTypeRef));
		String dateFormat = null;
		Map<String, Object> preferences = userManagement.getUserPreferences("admin");

		
		for (Map.Entry<String, Object> preference : preferences.entrySet()){
//			System.out.println(preference.getKey());
			if (preference.getKey().equals("DateFormat")){
				dateFormat = unquoteString(preference.getValue());
//				System.out.println(preference.getValue());
			}
			if (preference.getKey().equals("TimeZone")){
//				System.out.println(preference.getValue());
			}
			
		}
		SimpleDateFormat formatter;
		if (dateFormat != null){
			formatter = new SimpleDateFormat(dateFormat);
		} else {
			formatter = new SimpleDateFormat(DEFAULT_DATE_FORMAT);
		}
		
		String timestamp = formatter.format(new Date(alert.getAlertTime()));
		values.add(new ValueTO("time", timestamp));
		System.out.println(timestamp);
		values.add(new ValueTO("status", alert.getStatus().toString()));
		System.out.println(alert.getFullMessage());
		values.add(new ValueTO("message", alert.getFullMessage()));
		TO<?> hostObj = alert.getHostTO();
		if (!hostObj.equals(TOFactory.EMPTY_TO)){
			Collection<TO<?>> tos = new HashSet<TO<?>>();
			tos.add(hostObj);
			CollectionTO colTo = env.getPropertyValue(tos, new String[] {"name", "location"}).iterator().next(); 
			String objectName = (String) colTo.getSinglePropValue("name").getValue();
			values.add(new ValueTO("hostTO", objectName));
		} else {
			values.add(new ValueTO("hostTO", "System"));
		}
		values.add(new ValueTO("hostTO", TOFactory.EMPTY_TO.equals(hostObj) ? "System" : TOFactory.getInstance().saveTO(hostObj)));
		values.add(new ValueTO("acknowledgement", alert.getAcknowledgement()));
		Date ackTime = alert.getAcknowledgementTime();
		values.add(new ValueTO("acknowledgementTime", ackTime == null ? null : ackTime.getTime()));
		values.add(new ValueTO("user", alert.getUser()));
		values.add(new ValueTO("priority", alertService.getAlertType(alert.getAlertType()).getPriority()));
		//Adding site
		Collection<TO<?>> objDCs = env.getRelatedObjects(hostObj,  "DC", false);
       	if(!objDCs.isEmpty()){
       		TO<?> dc = objDCs.iterator().next();
       		values.add(new ValueTO("site", env.getPropertyValue(dc, "name", String.class)));
       	}
		return values;
	}

	private static String unquoteString(String str) {
		if (str != null && str.length() >= 2 && str.startsWith("\"")
				&& str.endsWith("\"")) {
			return str.substring(1, str.length() - 1);
		}
		return str;
	}
	
	
	public static Date setDate(String year, String month, String day){
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.HOUR_OF_DAY, cal.getActualMinimum(Calendar.HOUR_OF_DAY));
		cal.set(Calendar.MINUTE, cal.getActualMinimum(Calendar.MINUTE));
		cal.set(Calendar.SECOND, cal.getActualMinimum(Calendar.SECOND));
		cal.set(Calendar.MILLISECOND, cal.getActualMinimum(Calendar.MILLISECOND));
		cal.set(Integer.valueOf(year), Integer.valueOf(month) - 1, Integer.valueOf(day));
		System.out.println(cal.getTime().toString());
		return cal.getTime();
	}
	
	/**
	 * Gets all active alerts in system
	 * @return
	 * @throws NamingException
	 * @throws ObjectNotFoundException
	 */
	public static TO<?> findAlertTypeByName(Environment env, String typeName) { 
		ValueTO[] criteria = new ValueTO[] { new ValueTO("name", typeName) };
		Collection<TO<?>> alertDescriptors = env.getObjects(ALERT_DESCRIPTION_TYPE_NAME, criteria);
		if (alertDescriptors.isEmpty()) {
			return null;
		}
		if (alertDescriptors.size() > 1) {
			throw new AlertServiceSystemException("Mupltiple alert types having the same name {" + typeName + "} , "
			        + alertDescriptors);
		}
		
		return alertDescriptors.iterator().next();
	}
	
	private static void initConnection() {
		Properties connectProperties = new Properties();
		String serverUrl = "remote://" + "localhost" + ":4447";
		connectProperties.setProperty(Context.INITIAL_CONTEXT_FACTORY, "org.jboss.naming.remote.client.InitialContextFactory");
		connectProperties.setProperty(Context.PROVIDER_URL, serverUrl);
		connectProperties.setProperty("jboss.naming.client.ejb.context", "true");
		connectProperties.setProperty("jboss.naming.client.connect.options.org.xnio.Options.SASL_POLICY_NOPLAINTEXT", "false");
		connectProperties.setProperty(Context.SECURITY_PRINCIPAL, "admin");
		connectProperties.setProperty(Context.SECURITY_CREDENTIALS, "admin");
		connectProperties.setProperty("SynapEnvRemote", "SynapServer/Environment/remote");
		
		try{
			ctx = new InitialContext(connectProperties);
		} catch (NamingException e) {
			throw new IllegalStateException("Cannot create initial context to connect ES services", e);
		}
		
		userManagement = getProxy(ctx, JNDINames.USER_SERVICE_JNDI_NAME, UserManagementService.class);
		env = getProxy(ctx, JNDINames.ENVIRONMENT, Environment.class);
		alertService = getProxy(ctx, JNDINames.ALERTING_SERVICE_JNDI_NAME, AlertService.class);
//		try {
//			userManagement = ServerLoginModule.getProxy(ctx, JNDINames.USER_SERVICE_JNDI_NAME, UserManagementService.class);
//		} catch (NamingException e) {
//			e.printStackTrace();
//		}
//		env = ServerLoginModule.getProxy(ctx, JNDINames.ENVIRONMENT, Environment.class);
//		alertService = ServerLoginModule.getProxy(ctx, JNDINames.ALERTING_SERVICE_JNDI_NAME, AlertService.class);
	}
	
	protected static <INTERFACE> INTERFACE getProxy(InitialContext ctx, String jndiTarget, Class<INTERFACE> clazz) {
		try {
			return clazz.cast(ServerLoginModule.getProxy(ctx, jndiTarget, clazz));
		} catch (NamingException e) {
			throw new IllegalStateException("Error acquiring reference to " + jndiTarget, e);
		}
	}
	
	public static Collection<CollectionTO> getData(String status, Date startDate, Date endDate) throws Exception {
		initConnection();
		AlertFilterBuilder builder = new AlertFilterBuilder().between(startDate, endDate);
		builder = getAlertFilter(builder, AlertStatusType.getTypeByName(status));
		Collection<Alert> alerts = alertService.getAlerts(builder.build());
		return getAlertsData(alerts);
    }
	
	public static AlertFilterBuilder getAlertFilter(AlertFilterBuilder builder, AlertStatusType type){
		switch(type){
			case ALL:
				return builder;
			case OPENED:
				return builder.opened();
			case ACKNOWLEDGED:
				return builder.acknowledged();
			case DISMISSED:
				return builder.dismissed();
			case RESOLVED:
				return builder.resolved();
			case ALL_ACTIVE:
				return builder.active();
			case ALL_CLOSED:
				return builder.historical();
		}
		return null;
	}
	
	public static void main(String[] args) throws Exception {
		Date start = AlertDataSource.setDate("2012", "8", "1");
		Date end = AlertDataSource.setDate("2012", "9", "1");
		System.out.println(start.toString());
		AlertDataSource dataSource = new AlertDataSource("ALL", "ALL", start, end);
	}
}