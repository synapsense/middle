package com.synapsense.reporting.datasource;

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import com.synapsense.dto.CollectionTO;
import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;
import com.synapsense.service.Environment;
import com.synapsense.service.impl.synapviz.LegendImage;
import com.synapsense.service.impl.synapviz.LiveImage;
import com.synapsense.service.impl.synapviz.LiveImagingService;
import com.synapsense.util.CollectionUtils;
import com.synapsense.util.JNDINames;
import com.synapsense.util.Pair;
import com.synapsense.util.ServerLoginModule;

public class LiveImagingDataSource implements JRDataSource {

	private static HashMap<String, String> namesMap = new HashMap<>();

	static {
		namesMap.put("Image URL", "imageURL");
		namesMap.put("Legend URL", "legendURL");
		namesMap.put("Image TimeStamp", "imageTimeStamp");
		namesMap.put("DC Name", "dcName");
		namesMap.put("Room Name", "roomName");
		namesMap.put("Live Imaging Type", "imagingType");
	}
	
	private Iterator<CollectionTO> values;
	
	private CollectionTO current;
	
	private static LiveImagingService liveImagingService;
	private static Environment env;
	private static final String DEFAULT_HOST = "172.30.201.97";
	private static final String DEFAULT_PORT = "9091";
	private static String mHost = DEFAULT_HOST;
	private static String mPort = DEFAULT_PORT;
	private static final String DC = "n102569 - DC";
	private static final String ROOM = "Room1";
	

	public LiveImagingDataSource() throws Exception {
		Date endDate = new Date();
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(endDate);
		cal.add(Calendar.MINUTE, -30);
		
		Date startDate = cal.getTime();
		values = getData(DC, ROOM, LiveImagingType.TEMP_BOTTOM, startDate, endDate).iterator();
	}
	
	public LiveImagingDataSource(String dcName, String roomName, String liveImagingType) throws Exception {
		Date endDate = new Date();
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(endDate);
		cal.add(Calendar.MINUTE, -30);
		
		Date startDate = cal.getTime();
		values = getData(dcName, roomName, LiveImagingType.getTypeByName(liveImagingType), startDate, endDate).iterator();
	}
	

	public LiveImagingDataSource(String dcName, String roomName, String liveImagingType, String startYear, String startMonth, String startDay, String endYear, String endMonth, String endDay) throws Exception {
		values = getData(dcName, roomName, LiveImagingType.getTypeByName(liveImagingType)
				, AlertDataSource.setDate(startYear, startMonth, startDay)
				, AlertDataSource.setDate(endYear, endMonth, endDay)).iterator();
	}


	public LiveImagingDataSource(String host, String port, String dcName, String roomName, String liveImagingType, String startYear, String startMonth, String startDay, String endYear, String endMonth, String endDay) throws Exception {
		mHost = host;
		mPort = port;
		values = getData(dcName, roomName, LiveImagingType.getTypeByName(liveImagingType)
				, AlertDataSource.setDate(startYear, startMonth, startDay)
				, AlertDataSource.setDate(endYear, endMonth, endDay)).iterator();
	}


	@Override
	public Object getFieldValue(JRField field) throws JRException {
		String name = namesMap.get(field.getName());
		if (name != null) {
			ValueTO vTo = current.getSinglePropValue(name);
			if (vTo != null) {
				return vTo.getValue();
			}
		}	
		return null;
	}

	@Override
	public boolean next() throws JRException {
		if (values.hasNext()) {
			current = values.next();
			return true;
		}
		return false;
	}
	
	private static void initConnection() {
		Properties connectProperties = new Properties();
		String serverUrl = "remote://" + mHost + ":4447";
		connectProperties.setProperty(Context.INITIAL_CONTEXT_FACTORY, "org.jboss.naming.remote.client.InitialContextFactory");
		connectProperties.setProperty(Context.PROVIDER_URL, serverUrl);
		connectProperties.setProperty("jboss.naming.client.ejb.context", "true");
		connectProperties.setProperty("jboss.naming.client.connect.options.org.xnio.Options.SASL_POLICY_NOPLAINTEXT", "false");
		connectProperties.setProperty(Context.SECURITY_PRINCIPAL, "admin");
		connectProperties.setProperty(Context.SECURITY_CREDENTIALS, "admin");
//		connectProperties.setProperty("SynapEnvRemote", "SynapServer/Environment/remote");
		InitialContext ctx;
		try{
			ctx = new InitialContext(connectProperties);
		} catch (NamingException e) {
			throw new IllegalStateException("Cannot create initial context to connect ES services", e);
		}

		env = getProxy(ctx, JNDINames.ENVIRONMENT,
		        Environment.class);
		liveImagingService = getProxy(ctx, JNDINames.LIVE_IMAGING_SERVICE_JNDI_NAME,
		        LiveImagingService.class);
	}
	
	protected static <INTERFACE> INTERFACE getProxy(InitialContext ctx, String jndiTarget, Class<INTERFACE> clazz) {
		try {
			return clazz.cast(ServerLoginModule.getProxy(ctx, jndiTarget, clazz));
		} catch (NamingException e) {
			throw new IllegalStateException("Error acquiring reference to " + jndiTarget, e);
		}
	}
	
	public static Collection<CollectionTO> getData(String dcName, String roomName, LiveImagingType imagingType, Date startDate, Date endDate) throws Exception {
		initConnection();
		
		Collection<LegendImage> legendImages = liveImagingService.getLegendImages(1, imagingType.getDataClassId(), startDate, endDate);
		String legendURL = legendImages.iterator().next().getSiResourcePath();
		TO<?> liveImagingObject = null;
		Collection<TO<?>>objects = env.getObjectsByType("ROOM");
		for (TO<?> object : objects){
			if (env.getPropertyValue(object, "name", String.class).equals(roomName)
					&& env.getPropertyValue(env.getParents(object).iterator().next(), "name", String.class).equals(dcName)){
				liveImagingObject = object;
				break;
			}
		}
		Collection<CollectionTO> resultCollection = CollectionUtils.newList();
		
		Collection<LiveImage> bottomLiveImages =  liveImagingService.getImages(liveImagingObject
				, imagingType.getDataClassId(), imagingType.getLayer(), startDate, endDate);
		for(LiveImage liveImage : bottomLiveImages){
			CollectionTO imageCollection  = new CollectionTO(liveImagingObject);
			imageCollection.addValue(new ValueTO("imageURL", "http://" + mHost + ":" + mPort + "/" + liveImage.getResourceName()));
			imageCollection.addValue(new ValueTO("legendURL", "http://" + mHost + ":" + mPort + "/" + legendURL));
			imageCollection.addValue(new ValueTO("imageTimeStamp", liveImage.getShowingTime()));
			imageCollection.addValue(new ValueTO("dcName", dcName));
			imageCollection.addValue(new ValueTO("roomName", roomName));
			imageCollection.addValue(new ValueTO("imagingType", imagingType.getTypeName()));
			resultCollection.add(imageCollection);
			System.out.println(liveImage.getResourceName() + ": " + liveImage.getShowingTime() + ":" + legendURL);
		} 
		
		return resultCollection;
    }
	
	public static void main(String[] args) throws Exception {
//		TestLiveImagingDataSource dataSource = new TestLiveImagingDataSource(DC, ROOM, "Temp-Middle");
//		TestLiveImagingDataSource dataSource = new TestLiveImagingDataSource("Data Center one", "zn2", "Temp-Middle", "2012", "8", "29", "2012", "8", "29");
		LiveImagingDataSource dataSource = new LiveImagingDataSource("localhost", "9092", "Draft2", "Room1", "Temp-Middle", "2012", "9", "5", "2012", "9", "6");
		System.out.println("Success");
	}
}
