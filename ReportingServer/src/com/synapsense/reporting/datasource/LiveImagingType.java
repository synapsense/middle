package com.synapsense.reporting.datasource;

public enum LiveImagingType {
	/*
	 * Temperature Top
	 */
	TEMP_TOP(200, 131072, "Temp-Top"), 
	TEMP_MIDDLE(200, 16384, "Temp-Middle"),
	TEMP_BOTTOM(200, 2048, "Temp-Bottom"),
	TEMP_SUBFLOOR(200, 256, "Temp-Subfloor"),
	TEMP_CONTROLLED(200, 128, "Temp-Controlled"),
	HUMIDITY(201, 131072, "Humidity"),
	PRESSURE(202, 256, "Pressure"),
	DEW_POINT(998, 131072, "Dew Point");

	private Integer dataClassId;
	private Integer layer;
	private String typeName;

	private LiveImagingType(Integer dataClassId, Integer layer, String typeName) {
		this.dataClassId = dataClassId;
		this.layer = layer;
		this.typeName = typeName;
	}

	public static LiveImagingType getTypeByName(String typeName) {
		for (LiveImagingType type : LiveImagingType.values()) {
			if (type.typeName.equals(typeName))
				return type;
		}
		throw new IllegalArgumentException("Not supported LiveImagingType, type=" + typeName);	
	}
	
	public Integer getDataClassId() {
		return dataClassId;
	}

	public Integer getLayer() {
		return layer;
	}

	public String getTypeName() {
		return typeName;
	}
}
