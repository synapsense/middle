package com.synapsense.reporting.datasource;

public enum AlertStatusType {
	ALL("ALL"), OPENED("OPENED"), ACKNOWLEDGED("ACKNOWLEDGED"), DISMISSED("DISMISSED"), RESOLVED("RESOLVED"), ALL_ACTIVE("ALL_ACTIVE"), ALL_CLOSED("ALL_CLOSED");
	private AlertStatusType(String statusType) {
		this.statusType = statusType;
	}

	private String statusType;
	
	public static AlertStatusType getTypeByName(String typeName) {
		for (AlertStatusType type : AlertStatusType.values()) {
			if (type.statusType.equals(typeName))
				return type;
		}
		throw new IllegalArgumentException("Not supported Alert Status Type, type=" + typeName);	
	}

	public String getStatusType() {
		return statusType;
	}

	public void setStatusType(String statusType) {
		this.statusType = statusType;
	}
}