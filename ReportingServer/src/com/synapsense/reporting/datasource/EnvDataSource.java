package com.synapsense.reporting.datasource;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;
import net.sf.jasperreports.engine.base.JRBaseField;
import net.sf.jasperreports.engine.data.JRCsvDataSource;
import net.sf.jasperreports.engine.design.JRDesignField;
import net.sf.jasperreports.engine.util.JRLoader;

import com.synapsense.dto.CollectionTO;
import com.synapsense.dto.ValueTO;
import com.synapsense.service.Environment;
import com.synapsense.util.ServerLoginModule;

public class EnvDataSource implements JRDataSource {
//public class OLTPDataSource extends JRCsvDataSource {

	private static HashMap<String, String> namesMap = new HashMap<>();
	
	private JRCsvDataSource ds;
	private static Date mStartDate;
	private static Date mEndDate;
	private static Integer mPage = 1;
	static{
		mEndDate = new Date();
		Calendar cal = Calendar.getInstance();
		cal.setTime(mEndDate);
		cal.add(Calendar.DATE, -1);
		mStartDate = cal.getTime();
	}
	
	static {
		namesMap.put("Rack Name", "name");
		namesMap.put("Intake Bottom Avg", "avgcBot");
		namesMap.put("Intake Top Avg", "avgcTop");
		namesMap.put("Intake Bottom Min", "mincBot");
		namesMap.put("Intake Middle Min", "mincMid");
		namesMap.put("Intake Top Min", "mincTop");
		namesMap.put("Intake Middle Avg", "avgcMid");
		namesMap.put("Intake Middle Max", "maxcMid");
		namesMap.put("Intake Top Max", "maxcTop");
		namesMap.put("Intake Bottom Max", "maxcBot");
	}
	
	private Iterator<CollectionTO> values;
	
	private CollectionTO current;
	
	
	public EnvDataSource(Integer page, Date startDate, Date endDate) throws Exception {
		mPage = page;
		mStartDate = startDate;
		mEndDate = endDate;
		values = getData().iterator();
	}
	public EnvDataSource() throws Exception {
//		Properties environment = new Properties();
//		environment.put(Context.INITIAL_CONTEXT_FACTORY, "org.jboss.naming.NamingContextFactory");
//		environment.put(Context.URL_PKG_PREFIXES, "org.jboss.naming.client:org.jnp.interfaces");
//		environment.put(Context.PROVIDER_URL, host);
//
//		try {
//			InitialContext ctx = new InitialContext(environment);
//			ServerLoginModule.init(ctx);
//			ServerLoginModule.login("admin", "admin");
//			env = ServerLoginModule.getProxy(ctx, JNDINames.ENVIRONMENT, Environment.class);
//		} catch (Exception e) {
//			throw new RuntimeException(e);
//		}
		
//		super(inputStream);
		values = getData().iterator();
	}

	@Override
	public Object getFieldValue(JRField field) throws JRException {
		String name = namesMap.get(field.getName());
		if (name != null) {
			ValueTO vTo = current.getSinglePropValue(name);
			if (vTo != null) {
				return vTo.getValue();
			}
		}
		return null;
	}

	@Override
	public boolean next() throws JRException {
		if (values.hasNext()) {
			current = values.next();
			return true;
		}
		return false;
	}
	
	public static Collection<CollectionTO> getData() throws Exception {
		Properties connectProperties = new Properties();
		String serverUrl = "remote://" + "localhost" + ":4447";
		connectProperties.setProperty(Context.INITIAL_CONTEXT_FACTORY, "org.jboss.naming.remote.client.InitialContextFactory");
		connectProperties.setProperty(Context.PROVIDER_URL, serverUrl);
		connectProperties.setProperty("jboss.naming.client.ejb.context", "true");
		connectProperties.setProperty("jboss.naming.client.connect.options.org.xnio.Options.SASL_POLICY_NOPLAINTEXT", "false");
		connectProperties.setProperty(Context.SECURITY_PRINCIPAL, "admin");
		connectProperties.setProperty(Context.SECURITY_CREDENTIALS, "admin");
		connectProperties.setProperty("SynapEnvRemote", "SynapServer/Environment/remote");
		
		
		InitialContext ctx = new InitialContext(connectProperties);
		Environment env = ServerLoginModule.getProxy(ctx,"es-ear/es-core/Environment!com.synapsense.service.Environment",Environment.class);

//		System.out.println(Arrays.asList(env.getObjectTypes()));
		ArrayList<String> aggregates = new ArrayList<>();
		aggregates.add("avg");
		aggregates.add("min");
		aggregates.add("max");
		
		ArrayList<String> propNames = new ArrayList<>();
		propNames.add("cTop");
//		propNames.add("cMid");
//		propNames.add("cBot");
		
//		Date endDate = new Date();
//		
//		Calendar cal = Calendar.getInstance();
//		cal.setTime(endDate);
//		cal.add(Calendar.DATE, -1);
//		
//		Date startDate = cal.getTime();
		
		Collection<CollectionTO> result = null;
//		result = env.getAggregatedData("RACK", aggregates, propNames, mStartDate, mEndDate);
		return result;
    }
	
//	public static void main(String[] args) throws Exception {
//		OLTPDataSource dataSource = new OLTPDataSource("RACK");
//		while (dataSource.next()) {
//			JRDesignField f = new JRDesignField();
//			f.setName("Rack Name");
//			System.out.println(dataSource.getFieldValue(f));
//		}
//    }
}
