package com.synapsense.reporting.datasource;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.synapsense.service.reporting.olap.Member;
import com.synapsense.service.reporting.olap.RangeSet;
import com.synapsense.service.reporting.olap.SimpleMember;
import com.synapsense.service.reporting.olap.custom.Cube;
import com.synapsense.util.CollectionUtils;

public class ReportAxis {

	private List<Member> propertyMembers = CollectionUtils.newList();
	private int axis;
	private String objectIds;
	private String properties;
	private String objectType;
	private Map<String, Set<String>> fields = CollectionUtils.newMap();
	private DateRange dateRange;

	public ReportAxis(int axis, String properties) {
		this.axis = axis;
		this.properties = properties;
	}

	public ReportAxis(int axis, String objectIds, String objectType,
			String properties, int startDay, int startMonth, int startYear,
			int endDay, int endMonth, int endYear) {
		this.axis = axis;
		if (objectIds != null && !objectIds.equals("") && objectType != null
				&& !objectType.equals("")) {
			throw new IllegalArgumentException(
					"Only one objects specification way is acceptable: by type or by ids. Ids: "
							+ objectIds + ", objectType:  " + objectType);
		}
		this.objectIds = objectIds;
		this.objectType = objectType;
		this.properties = properties;
		dateRange = new DateRange(startDay, startMonth, startYear, endDay,
				endMonth, endYear);
	}

	public List<Member> getPropertyMembers() {
		if (propertyMembers.isEmpty()) {
			propertyMembers = new PropertyParser().getProperties();
			if (axis == ReportingTableDataSource.COLUMN_AXIS) {
				Set<String> propertyFields = CollectionUtils.newSet();
				for (Member member : propertyMembers) {
					propertyFields.add(member.getLevel() + "."
							+ member.getKey());
				}
				fields.put(ReportingTableDataSource.COLUMN_DATA_TYPE,
						propertyFields);
			}
		}
		return propertyMembers;
	}

	public String getObjectType() {
		return objectType;
	}

	public void setObjectType(String objectType) {
		this.objectType = objectType;
	}

	public String getObjectIds() {
		return objectIds;
	}

	public String getProperties() {
		return properties;
	}

	public Map<String, Set<String>> getFields() {
		return fields;
	}

	public DateRange getDateRange() {
		return this.dateRange;
	}

	private class PropertyParser {

		private List<Member> propertyTuples = CollectionUtils.newList();;

		private PropertyParser() {
		}

		private void parse() {
			// Parsing properties
			String[] allProperties = properties.split(",");
			for (String property : allProperties) {
				property = property.trim();
			}
			List<String> propertiesList = new ArrayList<String>(
					Arrays.asList(allProperties));

			// Setting columns
			for (String property : propertiesList) {
				String[] propertyParams = property.split("\\.");
				propertyTuples.add(new SimpleMember(Cube.PROPERTY, "Name",
						propertyParams[0] + "." + propertyParams[1] + "."
								+ propertyParams[2]));
			}

		}

		private List<Member> getProperties() {
			if (propertyTuples.isEmpty()) {
				parse();
			}
			return propertyTuples;
		}
	}

	public class DateRange {
		private Integer startDay;
		private Integer startMonth;
		private Integer startYear;
		private Integer endDay;
		private Integer endMonth;
		private Integer endYear;

		private DateRange(int startDay, int startMonth, int startYear,
				int endDay, int endMonth, int endYear) {
			this.startDay = startDay;
			this.startMonth = startMonth;
			this.startYear = startYear;
			this.endDay = endDay;
			this.endMonth = endMonth;
			this.endYear = endYear;
		}

		public RangeSet getRangeSet() {
			Member startMember = new SimpleMember(Cube.TIME, Cube.LEVEL_DAY,
					startDay, new SimpleMember(Cube.TIME, Cube.LEVEL_MONTH,
							startMonth, new SimpleMember(Cube.TIME,
									Cube.LEVEL_YEAR, startYear)));
			Member endMember = new SimpleMember(Cube.TIME, Cube.LEVEL_DAY,
					endDay, new SimpleMember(Cube.TIME, Cube.LEVEL_MONTH,
							endMonth, new SimpleMember(Cube.TIME,
									Cube.LEVEL_YEAR, endYear)));
			RangeSet rangeSet = new RangeSet(Cube.TIME, startMember, endMember);
			return rangeSet;
		}
	}
}