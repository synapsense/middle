package com.synapsense.reporting.datasource;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;
import net.sf.jasperreports.engine.design.JRDesignField;

import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.reporting.datasource.ReportAxis.DateRange;
import com.synapsense.service.reporting.DimensionalQuery;
import com.synapsense.service.reporting.ReportRow;
import com.synapsense.service.reporting.ReportTable;
import com.synapsense.service.reporting.ReportingService;
import com.synapsense.service.reporting.olap.AllMemberSet;
import com.synapsense.service.reporting.olap.AxisSet;
import com.synapsense.service.reporting.olap.CrossJoin;
import com.synapsense.service.reporting.olap.EmptySet;
import com.synapsense.service.reporting.olap.SimpleMember;
import com.synapsense.service.reporting.olap.Tuple;
import com.synapsense.service.reporting.olap.TupleSet;
import com.synapsense.service.reporting.olap.custom.BaseLevel;
import com.synapsense.service.reporting.olap.custom.Cube;
import com.synapsense.util.CollectionUtils;
import com.synapsense.util.JNDINames;
import com.synapsense.util.ServerLoginModule;

public class ReportingTableDataSource implements JRDataSource {

	private final static String OBJECT_TYPE = "ObjectType";
	private final static String ROW_DATA_TYPE = "RowProperties";
	protected final static String COLUMN_DATA_TYPE = "ColumnProperties";
	private final static String DATA_VALUE = "Data Value";
	private final static String START_DAY = "StartDay";
	private final static String START_MONTH = "StartMonth";
	private final static String START_YEAR = "StartYear";
	private final static String END_DAY = "EndDay";
	private final static String END_MONTH = "EndMonth";
	private final static String END_YEAR = "EndYear";
	private final static String OBJECT_TO_LIST = "ObjectTOList";
	protected final static int ROW_AXIS = 0;
	protected final static int COLUMN_AXIS = 1;
	private static InitialContext ctx;

	private static ReportingService reportingService;
	private Iterator<ReportRow> values;
	private ReportRow current;
	private DateRange dateRange;
	private ReportTable reportTable;
	private String objectIds;
	private String properties;
	private String objectType;
	private Map<String, Set<String>> fields = CollectionUtils.newMap();
	private ReportAxis rowAxis;
	private AxisSet timeMembers;
	private AxisSet propertyMembers;
	
	public ReportingTableDataSource(Map<String, Object> parameters) throws Exception {
		this(new ReportAxis(ROW_AXIS
				, (String)parameters.get(OBJECT_TO_LIST)
				, (String)parameters.get(OBJECT_TYPE)
				, (String)parameters.get(ROW_DATA_TYPE)
				, (int)parameters.get(START_DAY)
				, (int)parameters.get(START_MONTH)
				, (int)parameters.get(START_YEAR)
				, (int)parameters.get(END_DAY)
				, (int)parameters.get(END_MONTH)
				, (int)parameters.get(END_YEAR)), (String)parameters.get(COLUMN_DATA_TYPE));
	}

	public ReportingTableDataSource(ReportAxis rowAxis, String columnProperties) {
		this.rowAxis = rowAxis;
		if (rowAxis.getDateRange() != null){
			timeMembers = rowAxis.getDateRange().getRangeSet();
		}
		String objectIds = rowAxis.getObjectIds();
		if (objectIds != null
				&& !objectIds.equals("")) {
			this.objectIds = objectIds;
		}
		String objectType = rowAxis.getObjectType();
		if (objectType != null
				&& !objectType.equals("")) {
			this.objectType = objectType;
		}
		String rowProperties = rowAxis.getProperties();
		if (rowProperties != null 
				&& !rowProperties.equals("")) {
			this.properties = rowProperties;
			this.propertyMembers = new TupleSet(this.rowAxis.getPropertyMembers().toArray(new SimpleMember[0]));
		}
		if (columnProperties != null
				&& !columnProperties.equals("")) {
			this.properties = columnProperties;
			ReportAxis columnAxis = new ReportAxis(COLUMN_AXIS, columnProperties);
			this.propertyMembers = new TupleSet(columnAxis.getPropertyMembers().toArray(new SimpleMember[0]));
		}
		reportTable = getData();
		values = reportTable.iterator();
	}

	@Override
	public Object getFieldValue(JRField field) throws JRException {
		String name = field.getName();
		if (name != null) {
			if (name.equals(DATA_VALUE)) {
				return String.valueOf(current.next());
			} else if (fields.containsKey(COLUMN_DATA_TYPE)) {
				return current.getFieldValue("Name" + "." + name);
			} else {
				return current.getFieldRowValue(name);
			}
		}
		return null;
	}

	@Override
	public boolean next() throws JRException {
		if (values.hasNext()) {
			current = values.next();
			return true;
		}
		return false;
	}
	
	private List<TO<?>> parseObjectIds(String objectIds){
		String[] allObjectIds = objectIds.split(",");
		for (String objectId : allObjectIds) {
			objectId = objectId.trim();
		}
		List<String> objectIdList = new ArrayList<String>(
				Arrays.asList(allObjectIds));
		List<TO<?>> objects = CollectionUtils.newList();
		for (String objectId : objectIdList) {
			objects.add(TOFactory.getInstance().loadTO(objectId));
		}
		return objects;
	}

	private ReportTable getData(){
		initConnection();
		int startRow = 1;
		//TODO need to remove this param
		int endRow = 1000;

		//Parsing object Ids
		AxisSet objectMembers = null;
		if (objectType != null){
			objectMembers = new AllMemberSet(Cube.OBJECT, objectType);
		} 
		if (objectIds != null) {
			List<TO<?>> objects = CollectionUtils.newList();
			List<Tuple> objectTuples = CollectionUtils.newList();
			objects = parseObjectIds(objectIds);
			for (TO<?> object : objects){
				objectTuples.add(new SimpleMember(Cube.OBJECT, object.getTypeName()
						, object));
			}
			objectMembers = new TupleSet(objectTuples.toArray(new SimpleMember[0]));
		}

		AllMemberSet hours = new AllMemberSet(Cube.LEVEL_HOUR, Cube.TIME, new BaseLevel(Cube.TIME, Cube.LEVEL_HOUR));
		AxisSet rows = new CrossJoin(objectMembers, propertyMembers, timeMembers, hours);
		AxisSet slicer = new EmptySet();
		DimensionalQuery query = new DimensionalQuery("avg", null, rows, slicer);
		return reportingService.executeQuery(query, startRow, endRow);
	}
	
	private static void initConnection() {
		Properties connectProperties = new Properties();
		String serverUrl = "remote://" + "localhost" + ":4447";
		connectProperties.setProperty(Context.INITIAL_CONTEXT_FACTORY, "org.jboss.naming.remote.client.InitialContextFactory");
		connectProperties.setProperty(Context.PROVIDER_URL, serverUrl);
		connectProperties.setProperty("jboss.naming.client.ejb.context", "true");
		connectProperties.setProperty("jboss.naming.client.connect.options.org.xnio.Options.SASL_POLICY_NOPLAINTEXT", "false");
		connectProperties.setProperty(Context.SECURITY_PRINCIPAL, "admin");
		connectProperties.setProperty(Context.SECURITY_CREDENTIALS, "admin");
		connectProperties.setProperty("SynapEnvRemote", "SynapServer/Environment/remote");
		
		try{
			ctx = new InitialContext(connectProperties);
		} catch (NamingException e) {
			throw new IllegalStateException("Cannot create initial context to connect ES services", e);
		}
		reportingService = getProxy(ctx, JNDINames.REPORTING_SERVICE_NAME, ReportingService.class);
	}
	
	protected static <INTERFACE> INTERFACE getProxy(InitialContext ctx, String jndiTarget, Class<INTERFACE> clazz) {
		try {
			return clazz.cast(ServerLoginModule.getProxy(ctx, jndiTarget, clazz));
		} catch (NamingException e) {
			throw new IllegalStateException("Error acquiring reference to " + jndiTarget, e);
		}
	}

	public static void main(String[] args) throws Exception {
		ReportingTableDataSource dataSource = new ReportingTableDataSource(new ReportAxis(0, null, "RACK", "RACK.cTop.avg", 25, 1, 2013, 25, 1, 2013), null);
		while (dataSource.next()) {
			JRDesignField f = new JRDesignField();
			f.setName("Object Name");
			System.out.println("Object Name" + dataSource.getFieldValue(f));
			f.setName("Data Type");
			System.out.println("Data Type" + dataSource.getFieldValue(f));
			f.setName("Data Value");
			System.out.println("Data value" + dataSource.getFieldValue(f));
			f.setName("Timestamp");
			System.out.println("Timestamp" + dataSource.getFieldValue(f));
		}
    }
}
