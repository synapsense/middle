package com.synapsense.reporting;

public class ReportingRuntimeException extends RuntimeException {

	private static final long serialVersionUID = 8968939292432548270L;

	public ReportingRuntimeException() {
	}

	public ReportingRuntimeException(String message) {
		super(message);
	}

	public ReportingRuntimeException(Throwable cause) {
		super(cause);
	}

	public ReportingRuntimeException(String message, Throwable cause) {
		super(message, cause);
	}
}
