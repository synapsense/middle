package com.synapsense.reporting.mail;

import com.synapsense.reporting.ReportingException;

public class SmtpException extends ReportingException {

	private static final long serialVersionUID = 8438474555584143684L;

	public SmtpException() {
	}

	public SmtpException(String message) {
		super(message);
	}

	public SmtpException(Throwable cause) {
		super(cause);
	}

	public SmtpException(String message, Throwable cause) {
		super(message, cause);
	}
}
