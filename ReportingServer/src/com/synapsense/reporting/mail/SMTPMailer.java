package com.synapsense.reporting.mail;

import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.NoSuchProviderException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class SMTPMailer {

	private String mailFrom;
	private String user;
	private String password;
	private Properties javamailProperties;

	public SMTPMailer(Properties settings) {
		init(settings);
	}

	public void send(String subject, String body, String[] attachFiles, String recipient) throws SmtpException {
		send(subject, body, attachFiles, new String[] { recipient });
	}

	public void send(String subject, String body, String[] attachFiles, String[] recipients) throws SmtpException {
		Session session = Session.getDefaultInstance(javamailProperties);
		Transport transport = null;
		try {
			transport = session.getTransport();
		} catch (NoSuchProviderException e) {
			throw new SmtpException("Unable to resolve provider", e);
		}

		Message smtpMessage = new MimeMessage(session);
		try {
			smtpMessage.setFrom(new InternetAddress(mailFrom));
			smtpMessage.setSubject(subject);

			// Create the message part
			MimeBodyPart messageBodyPart = new MimeBodyPart();

			// Fill message
			messageBodyPart.setText(body);
			Multipart multipart = new MimeMultipart();
			multipart.addBodyPart(messageBodyPart);

			if (attachFiles != null) {
				// Attachments
				for (String fileAttachment : attachFiles) {
					messageBodyPart = new MimeBodyPart();
					DataSource source = new FileDataSource(fileAttachment);
					messageBodyPart.setDataHandler(new DataHandler(source));
					messageBodyPart.setFileName(fileAttachment.substring(fileAttachment.lastIndexOf('/') + 1));
					multipart.addBodyPart(messageBodyPart);
				}
			}

			// Put parts in message
			smtpMessage.setContent(multipart);

			// Fill recipients
			for (String nextRecipient : recipients) {
				smtpMessage.addRecipient(javax.mail.Message.RecipientType.TO, new InternetAddress(nextRecipient));
			}

			if (user != null) {

				transport.connect(user, password);
			} else {
				transport.connect();
			}
			try {
				transport.sendMessage(smtpMessage, smtpMessage.getRecipients(javax.mail.Message.RecipientType.TO));
			} finally {
				transport.close();
			}
		} catch (AddressException e) {
			throw new SmtpException("Unbale to deliver the message due to address error", e);
		} catch (MessagingException e) {
			throw new SmtpException("Unable to deliver the message", e);
		}
	}

	private void init(Properties p) {
		javamailProperties = new Properties(p);
		mailFrom = javamailProperties.getProperty("mail.from");

		if (javamailProperties.getProperty("mail.smtp.user") != null) {
			user = javamailProperties.getProperty("mail.smtp.user");
			password = javamailProperties.getProperty("mail.smtp.password");
		} else if (javamailProperties.getProperty("mail.smtps.user") != null) {
			user = javamailProperties.getProperty("mail.smtps.user");
			password = javamailProperties.getProperty("mail.smtps.password");
		}
	}
}
