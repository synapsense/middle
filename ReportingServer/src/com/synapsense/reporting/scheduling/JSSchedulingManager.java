package com.synapsense.reporting.scheduling;

import java.io.Serializable;
import java.text.ParseException;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.CronTrigger;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.StatefulJob;

import com.synapsense.reporting.app.ReportingServer;
import com.synapsense.reporting.dto.ScheduledTaskInfo;

public class JSSchedulingManager implements SchedulingManager, Serializable {

	private static final long serialVersionUID = 1042990246256548390L;

	private static final Log log = LogFactory.getLog(JSSchedulingManager.class);

	public static final String SCHEDULED_TASK = "ReportTask";

	public static final String TASK_RUNNER = "TaskRuner";

	private Scheduler scheduler;

	@Override
	public void scheduleReportTask(ScheduledTaskInfo taskInfo) throws SchedulingManagerException {

		if (log.isDebugEnabled()) {
			log.debug("Scheduling task for report template " + taskInfo.getReportTask().getReportTemplateName()
			        + " and cron " + taskInfo.getCron());
		}

		String taskName = taskInfo.getName();
		if (taskName == null) {
			taskName = taskInfo.getReportTask().getReportTemplateName() + "_" + String.valueOf(new Date().getTime());
		}

		taskInfo.setName(taskName);

		JobDetail jobDetail = new JobDetail(taskName, taskInfo.getReportTask().getReportTemplateName(),
		        ReportingJob.class);
		jobDetail.getJobDataMap().put(SCHEDULED_TASK, taskInfo);

		CronTrigger trigger = new CronTrigger(taskName);

		try {
			trigger.setCronExpression(taskInfo.getCron());
		} catch (ParseException e) {
			throw new SchedulingManagerException("Unable to parse cron expression", e);
		}

		try {
			scheduler.scheduleJob(jobDetail, trigger);
		} catch (SchedulerException e) {
			throw new SchedulingManagerException("Unable to schedule job", e);
		}
	}

	@Override
	public Collection<ScheduledTaskInfo> getScheduledTasks(String reportTemlateName) throws SchedulingManagerException {
		List<ScheduledTaskInfo> tasks = new LinkedList<ScheduledTaskInfo>();
		try {
			String[] jobs = scheduler.getJobNames(reportTemlateName);
			for (String job : jobs) {
				tasks.add((ScheduledTaskInfo) scheduler.getJobDetail(job, reportTemlateName).getJobDataMap()
				        .get(SCHEDULED_TASK));
			}
			return tasks;
		} catch (SchedulerException e) {
			throw new SchedulingManagerException("Unable to get scheduled tasks for template " + reportTemlateName);
		}
	}

	@Override
	public void removeScheduledTask(String taskName, String reportTemplateName) throws SchedulingManagerException {
		if (log.isDebugEnabled()) {
			log.debug("Deleting scheduled task for name " + taskName + " template " + reportTemplateName);
		}
		try {
			scheduler.deleteJob(taskName, reportTemplateName);
		} catch (SchedulerException e) {
			throw new SchedulingManagerException("Unable to delete job", e);
		}

	}

	@Override
	public void removeScheduledTasks(String reportTemplateName) throws SchedulingManagerException {
		try {
			String[] tasks = scheduler.getJobNames(reportTemplateName);
			for (String task : tasks) {
				scheduler.deleteJob(task, reportTemplateName);
			}
		} catch (SchedulerException e) {
			throw new SchedulingManagerException("Unable to remove scheduled tasks for template " + reportTemplateName);
		}
	}

	public void start() throws SchedulingManagerException {
		try {
			if (scheduler != null) {
				if (log.isDebugEnabled()) {
					log.debug("Starting scheduling manager");
				}
				scheduler.start();
			}
		} catch (SchedulerException e) {
			log.error(e);
			throw new SchedulingManagerException("Unable to stop scheduling manager", e);
		}
	}

	public void shutdown() throws SchedulingManagerException {
		try {
			if (scheduler != null && scheduler.isStarted()) {
				if (log.isDebugEnabled()) {
					log.debug("Stopping scheduling manager");
				}
				scheduler.shutdown();
			}
		} catch (SchedulerException e) {
			log.error(e);
			throw new SchedulingManagerException("Unable to stop scheduling manager", e);
		}
	}

	public void setScheduler(Scheduler scheduler) {
		this.scheduler = scheduler;
	}

	public static class ReportingJob implements StatefulJob {

		@Override
		public void execute(JobExecutionContext context) throws JobExecutionException {
			ScheduledTaskInfo taskInfo = (ScheduledTaskInfo) context.getJobDetail().getJobDataMap()
			        .get(JSSchedulingManager.SCHEDULED_TASK);
			ScheduledTaskRunner taskRunner = ReportingServer.getInstance().getTaskRunner();
			taskRunner.runReportTask(taskInfo.getReportTask());
		}
	}
}
