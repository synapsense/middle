package com.synapsense.reporting.scheduling;

import com.synapsense.reporting.ReportingException;

public class SchedulingManagerException extends ReportingException {

	private static final long serialVersionUID = -63583250795746386L;

	public SchedulingManagerException() {
	}

	public SchedulingManagerException(String message, Throwable cause) {
		super(message, cause);
	}

	public SchedulingManagerException(String message) {
		super(message);
	}

	public SchedulingManagerException(Throwable cause) {
		super(cause);
	}
}
