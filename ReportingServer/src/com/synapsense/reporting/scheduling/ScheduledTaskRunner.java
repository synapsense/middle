package com.synapsense.reporting.scheduling;

import org.quartz.JobExecutionException;

import com.synapsense.reporting.dto.ReportTaskInfo;

public interface ScheduledTaskRunner {

	void runReportTask(ReportTaskInfo taskInfo) throws JobExecutionException;
}
