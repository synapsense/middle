package com.synapsense.reporting.scheduling;

import java.util.Collection;

import com.synapsense.reporting.dto.ScheduledTaskInfo;

public interface SchedulingManager {

	void scheduleReportTask(ScheduledTaskInfo taskInfo) throws SchedulingManagerException;

	Collection<ScheduledTaskInfo> getScheduledTasks(String reportTemplateName) throws SchedulingManagerException;

	void removeScheduledTask(String taskName, String reportTemplateName) throws SchedulingManagerException;

	void removeScheduledTasks(String reportTemplateName) throws SchedulingManagerException;
}