package com.synapsense.reporting;

public class ReportingException extends Exception {

	private static final long serialVersionUID = -7939499701720801340L;

	public ReportingException() {
	}

	public ReportingException(String message, Throwable cause) {
		super(message, cause);
	}

	public ReportingException(String message) {
		super(message);
	}

	public ReportingException(Throwable cause) {
		super(cause);
	}
}
