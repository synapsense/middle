package com.synapsense.reporting.configuration;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public final class Configuration {

	private static Properties properties;

	public static void load(String path) {
		properties = new Properties();
		try {
			properties.load(new BufferedReader(new FileReader(path)));
		} catch (IOException e) {
			throw new IllegalArgumentException("Unable to read configuration from " + path);
		}
	}

	public static String getProperty(String key) {
		return properties.getProperty(key);
	}

	public static Properties getProperties() {
		return properties;
	}

	private Configuration() {
	}
}
