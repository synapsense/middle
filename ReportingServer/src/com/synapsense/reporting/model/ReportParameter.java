package com.synapsense.reporting.model;

import java.io.Serializable;

public class ReportParameter implements Serializable {

	private static final long serialVersionUID = -4532057219076059950L;

	private String name;

	private Class<?> type;

	public ReportParameter(String paramName, Class<?> paramType) {
		this.name = paramName;
		this.type = paramType;
	}

	public String getName() {
		return name;
	}

	public Class<?> getType() {
		return type;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final ReportParameter other = (ReportParameter) obj;
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return new StringBuilder().append(name).append("-").append(type.getName()).toString();
	}
}
