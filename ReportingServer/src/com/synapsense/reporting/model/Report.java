package com.synapsense.reporting.model;

import java.io.Serializable;

import com.synapsense.reporting.export.ExportFormat;

public interface Report extends Serializable {

	void export(ExportFormat format, String path) throws ReportExportException;
}
