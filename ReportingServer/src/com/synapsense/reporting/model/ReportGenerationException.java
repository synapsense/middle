package com.synapsense.reporting.model;

import com.synapsense.reporting.ReportingException;

public class ReportGenerationException extends ReportingException {

	private static final long serialVersionUID = -7814155184675786304L;

	public ReportGenerationException() {
	}

	public ReportGenerationException(String message, Throwable cause) {
		super(message, cause);
	}

	public ReportGenerationException(String message) {
		super(message);
	}

	public ReportGenerationException(Throwable cause) {
		super(cause);
	}
}
