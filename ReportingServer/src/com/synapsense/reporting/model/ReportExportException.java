package com.synapsense.reporting.model;

import com.synapsense.reporting.ReportingException;

public class ReportExportException extends ReportingException {

	private static final long serialVersionUID = -2698625818887148906L;

	public ReportExportException() {
	}

	public ReportExportException(String message, Throwable cause) {
		super(message, cause);
	}

	public ReportExportException(String message) {
		super(message);
	}

	public ReportExportException(Throwable cause) {
		super(cause);
	}
}
