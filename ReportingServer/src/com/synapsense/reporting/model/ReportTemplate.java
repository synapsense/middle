package com.synapsense.reporting.model;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.fill.AsynchronousFillHandle;
import net.sf.jasperreports.olap.JRMondrianQueryExecuterFactory;

import com.synapsense.reporting.datasource.EnvDataSource;
import com.synapsense.reporting.datasource.ReportingTableDataSource;
import com.synapsense.reporting.datasource.ReportingDataSource;
import com.synapsense.reporting.dto.ReportInfo;
import com.synapsense.reporting.utils.CollectionUtils;

public class ReportTemplate {

	private JasperReport jasperReport;
	
	private LinkedHashMap<String, ReportParameter> parameters = CollectionUtils.newLinkedHashMap();

	public ReportTemplate(String source) throws ReportCompilationException {
		this(new ByteArrayInputStream(source.getBytes()));
	}

	public ReportTemplate(InputStream source) throws ReportCompilationException {
		compile(source);
	}

	public String getName() {
		return jasperReport.getName();
	}

	public ReportParameter getParameter(String paramName) {
		return parameters.get(paramName);
	}

	public Collection<ReportParameter> getParameters() {
		return Collections.unmodifiableCollection(parameters.values());
	}

	public Report generateReport(ReportInfo reportInfo) throws ReportGenerationException {
		try {
			Map<String, Object> params = reportInfo.getParametersValues();
			params.put(JRMondrianQueryExecuterFactory.PARAMETER_MONDRIAN_CONNECTION, ReportingDataSource.getInstance()
			        .getMondrianConnection());
			return new JSReport(JasperFillManager.fillReport(jasperReport, getReportParameters(reportInfo)));
		} catch (JRException e) {
			throw new ReportGenerationException("Unable to generate report " + getName(), e);
		}
	}

	public ReportGenerationHandler createReportGenerationHandler(ReportInfo reportInfo)
	        throws ReportGenerationException {
		Map<String, Object> params = reportInfo.getParametersValues();
		
		JRParameter[] paramsArray = jasperReport.getParameters();
		boolean isMondrianConnectionReport = false;
		for (JRParameter param : paramsArray) {
			if (param.getName().equals(JRMondrianQueryExecuterFactory.PARAMETER_MONDRIAN_CONNECTION)) {
				isMondrianConnectionReport = true;
				break;
			}
		}
		if (!isMondrianConnectionReport) {
			JRDataSource reportingDataSource = null;
			try {
				reportingDataSource = new ReportingTableDataSource(params);
			} catch (Exception e) {
				e.printStackTrace();
			}
			try {
				return new ReportGenerationHandler(AsynchronousFillHandle.createHandle(jasperReport, params, reportingDataSource));
			} catch (JRException e) {
				throw new ReportGenerationException("Unable to create generation handler for report " + getName(), e);
			}
		} else {
			try {
				return new ReportGenerationHandler(AsynchronousFillHandle.createHandle(jasperReport, getReportParameters(reportInfo)));
			} catch (JRException e) {
				throw new ReportGenerationException("Unable to create generation handler for report " + getName(), e);
			}
		}
	}

	private static Map<String, Object> getReportParameters(ReportInfo reportInfo) {
		Map<String, Object> params = reportInfo.getParametersValues();
		params.put(JRMondrianQueryExecuterFactory.PARAMETER_MONDRIAN_CONNECTION, ReportingDataSource.getInstance()
		        .getMondrianConnection());
		return params;
	}

	private void compile(InputStream source) throws ReportCompilationException {
		try {
			jasperReport = JasperCompileManager.compileReport(source);
			JRParameter[] paramsArray = jasperReport.getParameters();
			for (JRParameter param : paramsArray) {
				if (!param.isSystemDefined()) {
					if (!(Serializable.class.isAssignableFrom(param.getValueClass()))) {
						throw new ReportCompilationException("Report template parameter " + param.getName()
						        + " is not serializable");
					}
					parameters.put(param.getName(), new ReportParameter(param.getName(), param.getValueClass()));
				}
			}
		} catch (JRException e) {
			throw new ReportCompilationException("Unable to compile report template ", e);
		}
	}
}
