package com.synapsense.reporting.model;

import net.sf.jasperreports.engine.JasperPrint;

import com.synapsense.reporting.export.ExportFormat;

public class JSReport implements Report {

	private static final long serialVersionUID = -2117697696328046564L;

	private JasperPrint jasperPrint;

	public JSReport(JasperPrint jp) {
		this.jasperPrint = jp;
	}

	@Override
	public void export(ExportFormat format, String path) throws ReportExportException {
		format.getReportExporter().export(this, path);
	}

	public JasperPrint getJasperPrint() {
		return jasperPrint;
	}
}
