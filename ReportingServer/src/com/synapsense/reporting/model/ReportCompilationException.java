package com.synapsense.reporting.model;

import com.synapsense.reporting.ReportingException;

public class ReportCompilationException extends ReportingException {

	private static final long serialVersionUID = -4819671805140141961L;

	public ReportCompilationException() {
	}

	public ReportCompilationException(String message, Throwable cause) {
		super(message, cause);
	}

	public ReportCompilationException(String message) {
		super(message);
	}

	public ReportCompilationException(Throwable cause) {
		super(cause);
	}
}
