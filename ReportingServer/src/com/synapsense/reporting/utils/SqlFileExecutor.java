package com.synapsense.reporting.utils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class SqlFileExecutor {

	private Connection conn;

	public SqlFileExecutor(Connection conn) {
		this.conn = conn;
	}

	public void executeScript(String filePath) throws SQLException, IOException {
		BufferedReader scriptReader = new BufferedReader(new FileReader(filePath));
		String thisLine = null;
		StringBuilder sqlQueryBuilder = new StringBuilder();
		while ((thisLine = scriptReader.readLine()) != null) {

			// Skip comments and empty lines
			if (thisLine.length() > 0 && thisLine.charAt(0) == '-' || thisLine.length() == 0) {
				continue;
			}

			sqlQueryBuilder.append(" ").append(thisLine);

			// If one command complete
			if (sqlQueryBuilder.charAt(sqlQueryBuilder.length() - 1) == ';') {

				Statement stmt = conn.createStatement();

				// Remove semicolon
				sqlQueryBuilder.setLength(sqlQueryBuilder.length() - 1);

				// Execute the statement
				stmt.execute(sqlQueryBuilder.toString());

				sqlQueryBuilder.setLength(0);
			}
		}
	}
}
