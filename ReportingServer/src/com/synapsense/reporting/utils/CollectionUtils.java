package com.synapsense.reporting.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

public final class CollectionUtils {

	public static <U, V> HashMap<U, V> newHashMap() {
		return new HashMap<U, V>();
	}

	public static <U, V> LinkedHashMap<U, V> newLinkedHashMap() {
		return new LinkedHashMap<U, V>();
	}

	public static <U> HashSet<U> newHashSet() {
		return new HashSet<U>();
	}

	public static <U> LinkedHashSet<U> newLinkedHashSet() {
		return new LinkedHashSet<U>();
	}

	public static <U> ArrayList<U> newArrayList() {
		return new ArrayList<U>();
	}

	public static <U, V> Pair<U, V> newPair(U first, V second) {
		return new Pair<U, V>(first, second);
	}

	public static <U, V> void updateSetValuedMap(Map<U, Set<V>> map, U key, V value) {
		Set<V> valueSet = getValueSet(map, key);
		valueSet.add(value);
	}

	public static <U, V> void updateSetValuedMap(Map<U, Set<V>> map, U key, V[] values) {
		Set<V> valueSet = getValueSet(map, key);
		valueSet.addAll(Arrays.asList(values));
	}

	public static <U, V> Collection<V> getAllMapValues(Map<U, Set<V>> map) {
		List<V> list = newArrayList();
		for (Entry<U, Set<V>> entry : map.entrySet()) {
			list.addAll(entry.getValue());
		}
		return list;
	}

	private static <U, V> Set<V> getValueSet(Map<U, Set<V>> map, U key) {
		Set<V> valueSet = map.get(key);
		if (valueSet == null) {
			valueSet = new LinkedHashSet<V>();
			map.put(key, valueSet);
		}
		return valueSet;
	}

	private CollectionUtils() {
	}
}
