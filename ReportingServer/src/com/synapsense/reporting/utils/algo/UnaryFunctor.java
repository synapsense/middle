package com.synapsense.reporting.utils.algo;

public interface UnaryFunctor<T> {
	void process(T value);
}
