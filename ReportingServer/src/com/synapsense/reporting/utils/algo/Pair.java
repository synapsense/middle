package com.synapsense.reporting.utils.algo;

import java.io.Serializable;

public class Pair<T, U> implements Serializable {

	private static final long serialVersionUID = 1946480627003074200L;

	private T first;
	private U second;

	public Pair(T first, U second) {
		this.first = first;
		this.second = second;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((first == null) ? 0 : first.hashCode());
		result = prime * result + ((second == null) ? 0 : second.hashCode());
		return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Pair<T, U> other = (Pair<T, U>) obj; // here we need to suppress
		// warnings :-)
		if (first == null) {
			if (other.first != null) {
				return false;
			}
		} else if (!first.equals(other.first)) {
			// parameters should be
			// performed inside the element
			return false;
		}
		if (second == null) {
			if (other.second != null) {
				return false;
			}
		} else if (!second.equals(other.second)) {
			return false;
		}
		return true;
	}

	public T getFirst() {
		return first;
	}

	public U getSecond() {
		return second;
	}

	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer(32);
		buffer.append("Pair:<first: ");
		buffer.append(first.toString());
		buffer.append("; second: ");
		buffer.append(second.toString());
		buffer.append(">");
		return buffer.toString();
	}
}
