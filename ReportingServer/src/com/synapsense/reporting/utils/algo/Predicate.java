package com.synapsense.reporting.utils.algo;

public interface Predicate<T> {
	boolean evaluate(T value);
}
