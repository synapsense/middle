package com.synapsense.reporting.utils.algo;

public class Finder<T> {
	private Iterable<T> collection = null;
	private Predicate<T> predicate = null;

	public Finder() {

	}

	public Finder(Iterable<T> collection) {
		this.collection = collection;
	}

	public Finder(Iterable<T> collection, Predicate<T> predicate) {
		this.collection = collection;
		this.predicate = predicate;
	}

	public T find(Iterable<T> collection, Predicate<T> predicate) {
		for (T element : collection) {
			if (predicate.evaluate(element)) {
				return element;
			}
		}
		return null;
	}

	public T find(Predicate<T> predicate) {
		if (collection == null) {
			throw new IllegalStateException("collection is not set");
		}
		return find(collection, predicate);
	}

	public T find() {
		if (collection == null) {
			throw new IllegalStateException("collection is not set");
		}
		if (predicate == null) {
			throw new IllegalStateException("predicate is not set");
		}
		return find(collection, predicate);
	}
}
