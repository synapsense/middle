package com.synapsense.reporting.dto;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import com.synapsense.reporting.export.ExportFormat;

public class ReportInfo implements Serializable {

	private static final long serialVersionUID = -1741033545507963994L;

	private int id;

	private String reportTemplateName;

	private String reportName;

	private long timestamp;

	private LinkedHashMap<String, Object> paramVals = new LinkedHashMap<String, Object>();

	private HashMap<ExportFormat, String> exported = new HashMap<ExportFormat, String>();

	public ReportInfo(String reportTemplateName, String reportName) {
		this.reportTemplateName = reportTemplateName;
		this.reportName = reportName;
	}

	public ReportInfo(String reportName) {
		this.reportName = reportName;
	}

	public String getReportName() {
		return reportName;
	}

	public String getReportTemplateName() {
		return reportTemplateName;
	}

	public void addParameterValue(String name, Object value) {
		paramVals.put(name, value);
	}

	public Map<String, Object> getParametersValues() {
		return new LinkedHashMap<String, Object>(paramVals);
	}

	public void addExportedPath(ExportFormat format, String path) {
		exported.put(format, path);
	}

	public String getExportedPath(ExportFormat format) {
		return exported.get(format);
	}

	public Map<ExportFormat, String> getExportedMap() {
		return new HashMap<ExportFormat, String>(exported);
	}

	public int getReportId() {
		return id;
	}

	public void setReportId(int id) {
		this.id = id;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	@Override
	public String toString() {
		return new StringBuilder().append(reportName).toString();
	}
}
