package com.synapsense.reporting.dto;

import java.io.Serializable;

public class ReportTitle implements Serializable {

	private static final long serialVersionUID = 4160535531714792903L;

	private int reportId;

	private String reportTemplateName;

	private String reportName;

	private long timestamp;

	public ReportTitle(String reportTemplateName, String reportName, long timestamp) {
		this.reportTemplateName = reportTemplateName;
		this.reportName = reportName;
		this.timestamp = timestamp;
	}

	public ReportTitle() {
	}

	public String getReportTemplateName() {
		return reportTemplateName;
	}

	public void setReportTemplateName(String reportTemplateName) {
		this.reportTemplateName = reportTemplateName;
	}

	public String getReportName() {
		return reportName;
	}

	public void setReportName(String reportName) {
		this.reportName = reportName;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	public int getReportId() {
		return reportId;
	}

	public void setReportId(int id) {
		this.reportId = id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((reportName == null) ? 0 : reportName.hashCode());
		result = prime * result + ((reportTemplateName == null) ? 0 : reportTemplateName.hashCode());
		result = prime * result + (int) (timestamp ^ (timestamp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final ReportTitle other = (ReportTitle) obj;
		if (reportName == null) {
			if (other.reportName != null) {
				return false;
			}
		} else if (!reportName.equals(other.reportName)) {
			return false;
		}
		if (reportTemplateName == null) {
			if (other.reportTemplateName != null) {
				return false;
			}
		} else if (!reportTemplateName.equals(other.reportTemplateName)) {
			return false;
		}
		if (timestamp != other.timestamp) {
			return false;
		}
		return true;
	}
}
