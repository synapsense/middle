package com.synapsense.reporting.dto;

import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;

import com.synapsense.reporting.model.ReportParameter;
import com.synapsense.reporting.utils.CollectionUtils;

public class ReportTemplateInfo implements Serializable {

	private static final long serialVersionUID = -2827172415900460974L;

	private int reportTemplateId;

	private String reportTemplateName;

	private Set<ReportParameter> parameters;

	public ReportTemplateInfo(String reportTemplateName) {
		this.reportTemplateName = reportTemplateName;
		this.parameters = CollectionUtils.newLinkedHashSet();
	}

	public ReportTemplateInfo(String reportTemplateName, Collection<ReportParameter> parameters) {
		this.reportTemplateName = reportTemplateName;
		this.parameters = new LinkedHashSet<ReportParameter>(parameters);
	}

	public ReportTemplateInfo() {
		this.parameters = CollectionUtils.newLinkedHashSet();
	}

	public int getReportTemplateId() {
		return reportTemplateId;
	}

	public void setReportTemplateId(int reportTemplateId) {
		this.reportTemplateId = reportTemplateId;
	}

	public String getReportTemplateName() {
		return reportTemplateName;
	}

	public void setReportTemplateName(String reportTemplateName) {
		this.reportTemplateName = reportTemplateName;
	}

	public Collection<ReportParameter> getParameters() {
		return Collections.unmodifiableCollection(parameters);
	}

	public void addParameter(ReportParameter parameter) {
		parameters.add(parameter);
	}

	public void removeParameter(ReportParameter parameter) {
		parameters.remove(parameter);
	}

	public void clearParameters() {
		parameters.clear();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((parameters == null) ? 0 : parameters.hashCode());
		result = prime * result + ((reportTemplateName == null) ? 0 : reportTemplateName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final ReportTemplateInfo other = (ReportTemplateInfo) obj;
		if (parameters == null) {
			if (other.parameters != null) {
				return false;
			}
		} else if (!parameters.equals(other.parameters)) {
			return false;
		}
		if (reportTemplateName == null) {
			if (other.reportTemplateName != null) {
				return false;
			}
		} else if (!reportTemplateName.equals(other.reportTemplateName)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return new StringBuilder().append("[").append(reportTemplateName).append("][").append(parameters.toString())
		        .append("]").toString();
	}
}
