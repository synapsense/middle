package com.synapsense.reporting.dto;

import java.io.Serializable;

public class ScheduledTaskInfo implements Serializable {

	private static final long serialVersionUID = 5010976304636848377L;

	private String name;

	private ReportTaskInfo reportTask;

	private String cron;

	public ScheduledTaskInfo(ReportTaskInfo reportTask, String cron) {
		this.reportTask = reportTask;
		this.cron = cron;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ReportTaskInfo getReportTask() {
		return reportTask;
	}

	public void setReportTask(ReportTaskInfo reportTask) {
		this.reportTask = reportTask;
	}

	public String getCron() {
		return cron;
	}

	public void setCron(String cron) {
		this.cron = cron;
	}
}
