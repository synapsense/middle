package com.synapsense.reporting.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import com.synapsense.reporting.export.ExportFormat;
import com.synapsense.reporting.utils.CollectionUtils;

public class ReportTaskInfo implements Serializable {

	private static final long serialVersionUID = 7559852356237691370L;

	private String reportTemplateName = null;

	private ArrayList<ReportInfo> reportGenerationTasks = CollectionUtils.newArrayList();

	// This collections is for a complex task of generation-export-email
	private LinkedHashSet<ExportFormat> exportFormats = CollectionUtils.newLinkedHashSet();

	private LinkedHashSet<String> emailDestinations = CollectionUtils.newLinkedHashSet();

	// This collection is for separate export tasks
	private LinkedHashMap<Integer, Set<ExportFormat>> exportTasks = CollectionUtils.newLinkedHashMap();

	// This collection is for separate email tasks
	private LinkedHashMap<Integer, Set<String>> emailTasks = CollectionUtils.newLinkedHashMap();

	public static ReportTaskInfo newInstance() {
		return new ReportTaskInfo();
	}

	private ReportTaskInfo() {
	}

	public ReportTaskInfo addReportGeneration(ReportInfo reportInfo) {
		if (reportTemplateName == null) {
			reportTemplateName = reportInfo.getReportTemplateName();
		} else if (!reportTemplateName.equals(reportInfo.getReportTemplateName())) {
			throw new IllegalArgumentException("Only on template name allowed per a reporting task");
		}
		reportGenerationTasks.add(reportInfo);
		return this;
	}

	public ReportTaskInfo addExportFormat(ExportFormat format) {
		exportFormats.add(format);
		return this;
	}

	public ReportTaskInfo addExportFormats(Collection<ExportFormat> formats) {
		exportFormats.addAll(formats);
		return this;
	}

	public ReportTaskInfo addMailRecipient(String recipient) {
		emailDestinations.add(recipient);
		return this;
	}

	public ReportTaskInfo addExportTask(int reportId, ExportFormat format) {
		CollectionUtils.updateSetValuedMap(exportTasks, reportId, format);
		return this;
	}

	public ReportTaskInfo addExportTask(int reportId, Collection<ExportFormat> formats) {
		CollectionUtils.updateSetValuedMap(exportTasks, reportId, formats.toArray(new ExportFormat[formats.size()]));
		return this;
	}

	public ReportTaskInfo addEmailTask(int reportId, Collection<String> destinations) {
		CollectionUtils.updateSetValuedMap(emailTasks, reportId, destinations.toArray(new String[destinations.size()]));
		return this;
	}

	public ReportTaskInfo addEmailTask(int reportId, String destination) {
		CollectionUtils.updateSetValuedMap(emailTasks, reportId, destination);
		return this;
	}

	public Collection<ReportInfo> getReportsGeneration() {
		return new ArrayList<ReportInfo>(reportGenerationTasks);
	}

	public Collection<ExportFormat> getExportFormats() {
		return new LinkedHashSet<ExportFormat>(exportFormats);
	}

	public Collection<String> getEmailDestinations() {
		return new LinkedHashSet<String>(emailDestinations);
	}

	public Map<Integer, Set<ExportFormat>> getExportTasks() {
		return new LinkedHashMap<Integer, Set<ExportFormat>>(exportTasks);
	}

	public Map<Integer, Set<String>> getEmailTasks() {
		return new LinkedHashMap<Integer, Set<String>>(emailTasks);
	}

	public String getReportTemplateName() {
		return reportTemplateName;
	}

	public void setReportTemplateName(String reportTemplateName) {
		this.reportTemplateName = reportTemplateName;
	}

	public void clear() {
		reportGenerationTasks.clear();
		exportFormats.clear();
		emailDestinations.clear();
		exportTasks.clear();
		emailTasks.clear();
	}
}
