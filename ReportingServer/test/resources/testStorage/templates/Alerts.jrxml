<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="Alerts" language="groovy" pageWidth="760" pageHeight="595" orientation="Landscape" columnWidth="720" leftMargin="20" rightMargin="20" topMargin="20" bottomMargin="20" uuid="6e83898a-70df-4a86-a40a-703eb2898507">
	<property name="ireport.zoom" value="1.0"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<style name="Title" fontName="Arial" fontSize="26" isBold="true" pdfFontName="Helvetica-Bold"/>
	<style name="SubTitle" forecolor="#666666" fontName="Arial" fontSize="18"/>
	<style name="Column header" forecolor="#666666" fontName="Arial" fontSize="12" isBold="true"/>
	<style name="Detail" fontName="Arial" fontSize="12"/>
	<style name="table">
		<box>
			<pen lineWidth="1.0" lineColor="#000000"/>
		</box>
	</style>
	<style name="table_TH" mode="Opaque" backcolor="#F0F8FF">
		<box>
			<pen lineWidth="0.5" lineColor="#000000"/>
		</box>
	</style>
	<style name="table_CH" mode="Opaque" backcolor="#BFE1FF">
		<box>
			<pen lineWidth="0.5" lineColor="#000000"/>
		</box>
	</style>
	<style name="table_TD" mode="Opaque" backcolor="#FFFFFF">
		<box>
			<pen lineWidth="0.5" lineColor="#000000"/>
		</box>
	</style>
	<style name="table 1">
		<box>
			<pen lineWidth="1.0" lineColor="#000000"/>
		</box>
	</style>
	<style name="table 1_TH" mode="Opaque" backcolor="#F0F8FF">
		<box>
			<pen lineWidth="0.5" lineColor="#000000"/>
		</box>
	</style>
	<style name="table 1_CH" mode="Opaque" backcolor="#BFE1FF">
		<box>
			<pen lineWidth="0.5" lineColor="#000000"/>
		</box>
	</style>
	<style name="table 1_TD" mode="Opaque" backcolor="#FFFFFF">
		<box>
			<pen lineWidth="0.5" lineColor="#000000"/>
		</box>
	</style>
	<style name="Priority" forecolor="#000000" backcolor="#FFFFFF" fill="Solid" scaleImage="FillFrame" pattern="" isItalic="false" isUnderline="false" isStrikeThrough="false">
		<conditionalStyle>
			<conditionExpression><![CDATA[$F{Priority} == "CRITICAL"]]></conditionExpression>
			<style mode="Opaque" forecolor="#000000" backcolor="#FF0000" scaleImage="FillFrame" hAlign="Center" vAlign="Middle" isItalic="false" isUnderline="false" isStrikeThrough="false"/>
		</conditionalStyle>
		<conditionalStyle>
			<conditionExpression><![CDATA[$F{Priority} == "MAJOR"]]></conditionExpression>
			<style mode="Opaque" backcolor="#FFFF00" fill="Solid" scaleImage="FillFrame" hAlign="Center" vAlign="Middle" isItalic="false" isUnderline="false" isStrikeThrough="false"/>
		</conditionalStyle>
	</style>
	<subDataset name="Intake Table" uuid="975d2a89-db86-4532-8d91-d82a25dbe3b3">
		<field name="Status" class="java.lang.String"/>
		<field name="Alert Name" class="java.lang.String"/>
		<field name="Alert Message" class="java.lang.String"/>
		<field name="Priority" class="java.lang.String"/>
		<field name="Object" class="java.lang.String"/>
		<field name="Timestamp" class="java.lang.String"/>
		<field name="Site" class="java.lang.String"/>
		<variable name="Major" class="java.lang.Integer" calculation="Count">
			<variableExpression><![CDATA[($F{Priority}.equals("MAJOR"))?(Integer.valueOf($V{Major}.intValue() + 1)):(null)]]></variableExpression>
			<initialValueExpression><![CDATA[(Integer.valueOf(0))]]></initialValueExpression>
		</variable>
		<variable name="Critical" class="java.lang.Integer" calculation="Count">
			<variableExpression><![CDATA[($F{Priority}.equals("CRITICAL"))?(Integer.valueOf($V{Critical}.intValue())):null]]></variableExpression>
			<initialValueExpression><![CDATA[(Integer.valueOf(0))]]></initialValueExpression>
		</variable>
		<variable name="Total" class="java.lang.Integer" calculation="Count">
			<variableExpression><![CDATA[($F{Priority} != null)?(Integer.valueOf($V{Total}.intValue())):(null)]]></variableExpression>
			<initialValueExpression><![CDATA[(Integer.valueOf(0))]]></initialValueExpression>
		</variable>
		<variable name="Opened" class="java.lang.Integer" calculation="Count">
			<variableExpression><![CDATA[($F{Status}.equals("OPENED"))?(Integer.valueOf($V{Opened}.intValue())):(null)]]></variableExpression>
			<initialValueExpression><![CDATA[(Integer.valueOf(0))]]></initialValueExpression>
		</variable>
		<variable name="Resolved" class="java.lang.Integer" calculation="Count">
			<variableExpression><![CDATA[($F{Status}.equals("RESOLVED"))?(Integer.valueOf($V{Resolved}.intValue())):(null)]]></variableExpression>
			<initialValueExpression><![CDATA[(Integer.valueOf(0))]]></initialValueExpression>
		</variable>
		<variable name="Dismissed" class="java.lang.Integer" calculation="Count">
			<variableExpression><![CDATA[($F{Status}.equals("DISMISSED"))?($V{Dismissed}):(null)]]></variableExpression>
			<initialValueExpression><![CDATA[(Integer.valueOf(0))]]></initialValueExpression>
		</variable>
		<variable name="Acknowledged" class="java.lang.Integer" calculation="Count">
			<variableExpression><![CDATA[($F{Status}.equals("ACKNOWLEDGED"))?($V{Acknowledged}):(null)]]></variableExpression>
			<initialValueExpression><![CDATA[(Integer.valueOf(0))]]></initialValueExpression>
		</variable>
	</subDataset>
	<parameter name="Advanced Report" class="java.lang.Boolean" isForPrompting="false"/>
	<parameter name="Start Date" class="java.util.Date"/>
	<parameter name="End Date" class="java.util.Date"/>
	<parameter name="Status" class="java.lang.String"/>
	<parameter name="Priority" class="java.lang.String"/>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band height="70" splitType="Stretch">
			<staticText>
				<reportElement uuid="7abf160c-87af-43c5-8266-c1ec0484f139" style="Title" x="0" y="15" width="390" height="33"/>
				<textElement verticalAlignment="Middle"/>
				<text><![CDATA[Alerts History]]></text>
			</staticText>
			<image scaleImage="RetainShape">
				<reportElement uuid="06951565-2c30-492d-bd76-c435f03011f8" x="390" y="17" width="383" height="53"/>
				<imageExpression><![CDATA["C:\\webapps\\comconsole\\resources\\skins\\professionalSkin\\icons\\logo.jpg"]]></imageExpression>
			</image>
		</band>
	</title>
	<pageHeader>
		<band splitType="Stretch"/>
	</pageHeader>
	<columnHeader>
		<band height="7" splitType="Stretch"/>
	</columnHeader>
	<detail>
		<band height="30" splitType="Stretch"/>
	</detail>
	<columnFooter>
		<band height="45" splitType="Stretch"/>
	</columnFooter>
	<pageFooter>
		<band height="20" splitType="Stretch">
			<textField>
				<reportElement uuid="737ca1e9-6d7d-4d26-bbac-3c65906e9398" style="Column header" x="433" y="0" width="80" height="20"/>
				<textElement textAlignment="Right">
					<font size="10" isBold="false"/>
				</textElement>
				<textFieldExpression><![CDATA["Page "+$V{PAGE_NUMBER}+" of"]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Report">
				<reportElement uuid="4d5439fe-62f6-4bbd-811a-52b545a371b9" style="Column header" x="513" y="0" width="40" height="20"/>
				<textElement>
					<font size="10" isBold="false"/>
				</textElement>
				<textFieldExpression><![CDATA[" " + $V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<textField pattern="EEEEE dd MMMMM yyyy">
				<reportElement uuid="bd8b3881-58fd-478d-bdf2-6e842c1a4199" style="Column header" x="0" y="0" width="197" height="20"/>
				<textElement>
					<font size="10" isBold="false"/>
				</textElement>
				<textFieldExpression><![CDATA[new java.util.Date()]]></textFieldExpression>
			</textField>
		</band>
	</pageFooter>
	<summary>
		<band height="150" splitType="Stretch">
			<componentElement>
				<reportElement uuid="3b3b8fee-7a60-4c6d-91c5-3f07b06d0a32" key="table 1" style="table 1" stretchType="RelativeToTallestObject" x="0" y="0" width="720" height="50" isRemoveLineWhenBlank="true"/>
				<jr:table xmlns:jr="http://jasperreports.sourceforge.net/jasperreports/components" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports/components http://jasperreports.sourceforge.net/xsd/components.xsd">
					<datasetRun subDataset="Intake Table" uuid="13ad075d-f787-449e-aa49-befe4517e642">
						<dataSourceExpression><![CDATA[new com.synapsense.reporting.datasource.AlertDataSource($P{Status}, $P{Priority}, $P{Start Date}, $P{End Date})]]></dataSourceExpression>
					</datasetRun>
					<jr:column uuid="f2eeb8f4-684c-4529-90d7-3ed1a3eac5c1" width="103">
						<jr:tableFooter height="40" rowSpan="1">
							<textField isStretchWithOverflow="true" isBlankWhenNull="true">
								<reportElement uuid="5e537807-7f4e-402c-8f2a-244dcc77a70a" positionType="Float" stretchType="RelativeToTallestObject" x="0" y="0" width="102" height="40"/>
								<textElement textAlignment="Center" verticalAlignment="Middle">
									<font size="10"/>
									<paragraph tabStopWidth="10"/>
								</textElement>
								<textFieldExpression><![CDATA["OPENED: " + $V{Opened} + " RESOLVED: " + $V{Resolved} + " DISMISSED: " + $V{Dismissed} + " ACKNOWLEDGED: " + $V{Acknowledged}]]></textFieldExpression>
							</textField>
						</jr:tableFooter>
						<jr:columnHeader style="table 1_CH" height="31" rowSpan="1">
							<staticText>
								<reportElement uuid="980e6b59-26b2-401e-ab0f-85f7274c436f" positionType="Float" x="0" y="0" width="102" height="30"/>
								<textElement textAlignment="Center" verticalAlignment="Middle">
									<font size="10" isBold="true"/>
									<paragraph tabStopWidth="10"/>
								</textElement>
								<text><![CDATA[Status]]></text>
							</staticText>
						</jr:columnHeader>
						<jr:detailCell style="table 1_TD" height="21" rowSpan="1">
							<textField isBlankWhenNull="true">
								<reportElement uuid="6ee1268b-37a4-4a60-96fb-914b76da610d" positionType="Float" stretchType="RelativeToTallestObject" x="0" y="0" width="102" height="21"/>
								<textElement textAlignment="Left" verticalAlignment="Middle">
									<font size="10"/>
									<paragraph leftIndent="2" tabStopWidth="10"/>
								</textElement>
								<textFieldExpression><![CDATA[$F{Status}]]></textFieldExpression>
							</textField>
						</jr:detailCell>
					</jr:column>
					<jr:column uuid="2bbcc08a-616f-4b91-8693-3baeca6b8573" width="89">
						<jr:columnHeader style="table 1_CH" height="31" rowSpan="1">
							<staticText>
								<reportElement uuid="9b22dab4-8390-4980-a428-66c96242f521" positionType="Float" x="0" y="0" width="89" height="30"/>
								<textElement textAlignment="Center" verticalAlignment="Middle">
									<font size="10" isBold="true"/>
									<paragraph tabStopWidth="10"/>
								</textElement>
								<text><![CDATA[Alert Name]]></text>
							</staticText>
						</jr:columnHeader>
						<jr:detailCell style="table 1_TD" height="21" rowSpan="1">
							<textField isStretchWithOverflow="true" isBlankWhenNull="true">
								<reportElement uuid="6e6f85a5-f8c2-4c80-800a-7eb744e2beee" positionType="Float" stretchType="RelativeToTallestObject" x="0" y="0" width="89" height="21"/>
								<textElement textAlignment="Left" verticalAlignment="Middle">
									<font size="10"/>
									<paragraph leftIndent="2" tabStopWidth="10"/>
								</textElement>
								<textFieldExpression><![CDATA[$F{Alert Name}]]></textFieldExpression>
							</textField>
						</jr:detailCell>
					</jr:column>
					<jr:column uuid="02bcc846-cc5a-4a7c-aa55-7e4447b6e6be" width="237">
						<jr:columnHeader style="table 1_CH" height="31" rowSpan="1">
							<staticText>
								<reportElement uuid="94c19e4b-bd7d-4e6f-af88-eb91b91e903f" positionType="Float" x="0" y="0" width="237" height="30"/>
								<textElement textAlignment="Center" verticalAlignment="Middle">
									<font size="10" isBold="true"/>
									<paragraph tabStopWidth="10"/>
								</textElement>
								<text><![CDATA[Alert Message]]></text>
							</staticText>
						</jr:columnHeader>
						<jr:detailCell style="table 1_TD" height="21" rowSpan="1">
							<textField isStretchWithOverflow="true" isBlankWhenNull="true">
								<reportElement uuid="8f2bdabd-bc72-42bb-b7eb-3389c0594af1" positionType="Float" stretchType="RelativeToTallestObject" x="0" y="0" width="237" height="21"/>
								<textElement textAlignment="Left" verticalAlignment="Middle">
									<font size="10"/>
									<paragraph leftIndent="2" tabStopWidth="10"/>
								</textElement>
								<textFieldExpression><![CDATA[$F{Alert Message}]]></textFieldExpression>
							</textField>
						</jr:detailCell>
					</jr:column>
					<jr:column uuid="7017b803-d93e-4653-9fb2-5d00c2a79ce2" width="60">
						<jr:tableFooter height="40" rowSpan="1">
							<textField isStretchWithOverflow="true" isBlankWhenNull="true">
								<reportElement uuid="5e537807-7f4e-402c-8f2a-244dcc77a70a" positionType="Float" stretchType="RelativeToTallestObject" x="0" y="0" width="60" height="40"/>
								<textElement textAlignment="Center" verticalAlignment="Middle">
									<paragraph tabStopWidth="10"/>
								</textElement>
								<textFieldExpression><![CDATA["TOTAL: " + $V{Total} + " MAJOR: " + $V{Major} + " CRITICAL: " + $V{Critical}]]></textFieldExpression>
							</textField>
						</jr:tableFooter>
						<jr:columnHeader style="table 1_CH" height="31" rowSpan="1">
							<staticText>
								<reportElement uuid="7a2844ba-8e8c-44d9-9232-16da5c101e7a" positionType="Float" x="0" y="0" width="60" height="30"/>
								<textElement textAlignment="Center" verticalAlignment="Middle">
									<font size="10" isBold="true"/>
									<paragraph tabStopWidth="10"/>
								</textElement>
								<text><![CDATA[Priority]]></text>
							</staticText>
						</jr:columnHeader>
						<jr:detailCell style="table 1_TD" height="21" rowSpan="1">
							<textField isStretchWithOverflow="true" isBlankWhenNull="true">
								<reportElement uuid="62ee8c60-96b8-4933-9f91-35536a571134" style="Priority" positionType="Float" stretchType="RelativeToBandHeight" x="0" y="0" width="60" height="21"/>
								<textElement textAlignment="Left" verticalAlignment="Middle">
									<font size="10"/>
									<paragraph leftIndent="2" tabStopWidth="10"/>
								</textElement>
								<textFieldExpression><![CDATA[$F{Priority}]]></textFieldExpression>
							</textField>
						</jr:detailCell>
					</jr:column>
					<jr:column uuid="64c46aec-56a9-43c2-9354-5464c83491b8" width="82">
						<jr:columnHeader style="table 1_CH" height="31" rowSpan="1">
							<staticText>
								<reportElement uuid="9bf21c32-8766-4fa1-a162-f54207c5a43b" positionType="Float" x="0" y="0" width="82" height="30"/>
								<textElement textAlignment="Center" verticalAlignment="Middle">
									<font size="10" isBold="true"/>
									<paragraph tabStopWidth="10"/>
								</textElement>
								<text><![CDATA[Object]]></text>
							</staticText>
						</jr:columnHeader>
						<jr:detailCell style="table 1_TD" height="21" rowSpan="1">
							<textField isStretchWithOverflow="true" isBlankWhenNull="true">
								<reportElement uuid="90171fd7-db99-431a-8048-b96a6fd74932" positionType="Float" stretchType="RelativeToTallestObject" x="0" y="0" width="82" height="21"/>
								<textElement textAlignment="Left" verticalAlignment="Middle">
									<font size="10"/>
									<paragraph leftIndent="2" tabStopWidth="10"/>
								</textElement>
								<textFieldExpression><![CDATA[$F{Object}]]></textFieldExpression>
							</textField>
						</jr:detailCell>
					</jr:column>
					<jr:column uuid="56973cbc-8cb2-4387-97fe-cb8ce3ae0558" width="93">
						<jr:columnHeader style="table 1_CH" height="31" rowSpan="1">
							<staticText>
								<reportElement uuid="9bf21c32-8766-4fa1-a162-f54207c5a43b" positionType="Float" x="0" y="0" width="93" height="30"/>
								<textElement textAlignment="Center" verticalAlignment="Middle">
									<font size="10" isBold="true"/>
									<paragraph tabStopWidth="10"/>
								</textElement>
								<text><![CDATA[Timestamp]]></text>
							</staticText>
						</jr:columnHeader>
						<jr:detailCell style="table 1_TD" height="21" rowSpan="1">
							<textField isBlankWhenNull="true">
								<reportElement uuid="5e537807-7f4e-402c-8f2a-244dcc77a70a" positionType="Float" stretchType="RelativeToTallestObject" x="0" y="0" width="93" height="21"/>
								<textElement textAlignment="Center" verticalAlignment="Middle">
									<font size="10"/>
									<paragraph tabStopWidth="10"/>
								</textElement>
								<textFieldExpression><![CDATA[$F{Timestamp}]]></textFieldExpression>
							</textField>
						</jr:detailCell>
					</jr:column>
					<jr:column uuid="108f7df5-9ebc-4eb7-a6d5-4f0c6f35e910" width="61">
						<jr:columnHeader style="table 1_CH" height="31" rowSpan="1">
							<staticText>
								<reportElement uuid="a6b228ab-0c3e-42d5-8e1c-2f4eb2b31bb5" positionType="Float" x="0" y="0" width="60" height="30"/>
								<textElement textAlignment="Center" verticalAlignment="Middle">
									<font size="10" isBold="true"/>
									<paragraph tabStopWidth="10"/>
								</textElement>
								<text><![CDATA[Site]]></text>
							</staticText>
						</jr:columnHeader>
						<jr:detailCell style="table 1_TD" height="21" rowSpan="1">
							<textField isStretchWithOverflow="true" isBlankWhenNull="true">
								<reportElement uuid="5e537807-7f4e-402c-8f2a-244dcc77a70a" positionType="Float" stretchType="RelativeToTallestObject" x="0" y="0" width="60" height="21"/>
								<textElement textAlignment="Left" verticalAlignment="Middle">
									<font size="10"/>
									<paragraph leftIndent="2" tabStopWidth="10"/>
								</textElement>
								<textFieldExpression><![CDATA[$F{Site}]]></textFieldExpression>
							</textField>
						</jr:detailCell>
					</jr:column>
				</jr:table>
			</componentElement>
		</band>
	</summary>
</jasperReport>
