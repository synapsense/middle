package com.synapsense.commandengine.test;

import java.lang.reflect.InvocationTargetException;

import junit.framework.Assert;

import org.jmock.Expectations;
import org.jmock.Mockery;
import org.jmock.Sequence;
import org.jmock.api.Action;
import org.jmock.api.Invocation;
import org.jmock.integration.junit4.JMock;
import org.jmock.integration.junit4.JUnit4Mockery;
import org.jmock.internal.NamedSequence;
import org.jmock.lib.action.CustomAction;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.synapsense.commandengine.Command;
import com.synapsense.commandengine.CommandContext;
import com.synapsense.commandengine.monitoring.StatusMonitor;
import com.synapsense.commandengine.state.CommandState;

@RunWith(JMock.class)
public class CommandStateMachineTest {

	private Mockery context;

	@Before
	public void setUp() {
		context = new JUnit4Mockery();
	}

	@Test
	public void testCommandCompletedCancelled() {
		final Command command = context.mock(Command.class);
		final CommandContext commandContext = createCommandStateContext(command);

		final Sequence seq = new NamedSequence("test");
		context.checking(new Expectations() {
			{
				allowing(command).getName();
				will(returnValue("Test Command"));
				oneOf(command).run(with(any(StatusMonitor.class)));
				inSequence(seq);
				will(checkState(CommandState.RUNNING, commandContext));
				oneOf(command).interrupt();
				inSequence(seq);
				will(checkState(CommandState.CANCELLING, commandContext));
				oneOf(command).rollback();
				inSequence(seq);
				will(checkState(CommandState.CANCELLING, commandContext));
			}
		});
		commandContext.cancel();
		Assert.assertEquals("Command canceled, state = CANCELLED", CommandState.CANCELLED, commandContext.getState());

		commandContext.setState(CommandState.READY);
		commandContext.start();
		Assert.assertEquals("Command successfully finished, state = COMPLETED", CommandState.COMPLETED,
		        commandContext.getState());

		commandContext.setState(CommandState.RUNNING);
		commandContext.cancel();
		Assert.assertEquals("Command cancelling, state = CANCELLING", CommandState.CANCELLING,
		        commandContext.getState());

		commandContext.complete();
		Assert.assertEquals("Command canceled, state = CANCELLED", CommandState.CANCELLED, commandContext.getState());

	}

	@Test
	public void testCommandFailed() {
		final Command command = context.mock(Command.class);
		final CommandContext commandContext = createCommandStateContext(command);

		final Sequence seq = new NamedSequence("test");

		context.checking(new Expectations() {
			{
				allowing(command).getName();
				will(returnValue("Test Command"));
				oneOf(command).run(with(any(StatusMonitor.class)));
				inSequence(seq);
				will(doAll(checkState(CommandState.RUNNING, commandContext), throwException(new RuntimeException())));
				oneOf(command).rollback();
				inSequence(seq);
				will(checkState(CommandState.CANCELLING, commandContext));
			}
		});

		commandContext.start();
		Assert.assertEquals("Command failed, state = CANCELLED", CommandState.CANCELLED, commandContext.getState());
	}

	@Test
	public void testErrorStateResult() {
		final Command command = context.mock(Command.class);
		final CommandContext commandContext = createCommandStateContext(command);

		final Sequence seq = new NamedSequence("test");

		context.checking(new Expectations() {
			{
				allowing(command).getName();
				will(returnValue("Test Command"));
				oneOf(command).interrupt();
				inSequence(seq);
				will(doAll(checkState(CommandState.CANCELLING, commandContext), throwException(new RuntimeException())));
				oneOf(command).rollback();
				inSequence(seq);
				will(doAll(checkState(CommandState.CANCELLING, commandContext), throwException(new RuntimeException())));
			}
		});
		commandContext.setState(CommandState.RUNNING);
		commandContext.cancel();
		Assert.assertEquals("Command interruption failed, state = ERROR", CommandState.ERROR, commandContext.getState());

		commandContext.setState(CommandState.CANCELLING);
		commandContext.complete();
		Assert.assertEquals("Command rollback failed, state = ERROR", CommandState.ERROR, commandContext.getState());

	}

	@Test
	public void testRestartCancelledOrCompletedCommand() {
		final Command command = context.mock(Command.class);
		final CommandContext commandContext = createCommandStateContext(command);

		context.checking(new Expectations() {
			{
				allowing(command).getName();
				will(returnValue("Test Command"));
				exactly(3).of(command).run(with(any(StatusMonitor.class)));
				will(checkState(CommandState.RUNNING, commandContext));
				oneOf(command).interrupt();
				will(checkState(CommandState.CANCELLING, commandContext));
				oneOf(command).rollback();
				will(checkState(CommandState.CANCELLING, commandContext));
			}
		});
		commandContext.start();
		commandContext.start();
		Assert.assertEquals("Command successfully finished, state = COMPLETED", CommandState.COMPLETED,
		        commandContext.getState());

		commandContext.setState(CommandState.RUNNING);
		commandContext.cancel();
		commandContext.complete();
		commandContext.start();
		Assert.assertEquals("Command successfully finished, state = COMPLETED", CommandState.COMPLETED,
		        commandContext.getState());

	}

	@Test
	public void testCommandSuspend() {
		final Command command = context.mock(Command.class);

		final CommandContext commandContext = createCommandStateContext(command);

		context.checking(new Expectations() {
			{
				allowing(command).getName();
				will(returnValue("Test Command"));
				oneOf(command).pause();
				will(doAll(checkState(CommandState.RUNNING, commandContext), returnValue(Boolean.TRUE)));
				oneOf(command).renew();
				will(checkState(CommandState.SUSPENDED, commandContext));
			}
		});

		commandContext.setState(CommandState.RUNNING);
		commandContext.suspend();
		commandContext.resume();
		commandContext.complete();

		Assert.assertEquals("Command successfully finished, state = COMPLETED", CommandState.COMPLETED,
		        commandContext.getState());
	}

	@Test
	public void testCommandSuspendFail() {
		final Command command = context.mock(Command.class);

		final CommandContext commandContext = createCommandStateContext(command);

		context.checking(new Expectations() {
			{
				allowing(command).getName();
				will(returnValue("Test Command"));
				oneOf(command).pause();
				will(doAll(checkState(CommandState.RUNNING, commandContext), throwException(new RuntimeException())));
				oneOf(command).rollback();
				will(checkState(CommandState.CANCELLING, commandContext));
			}
		});

		commandContext.setState(CommandState.RUNNING);
		commandContext.suspend();
		Assert.assertEquals("Suspend failed, state = CANCELLED", CommandState.CANCELLED, commandContext.getState());
	}

	@Test
	public void testCommandResumeFail() {
		final Command command = context.mock(Command.class);

		final CommandContext commandContext = createCommandStateContext(command);

		context.checking(new Expectations() {
			{
				allowing(command).getName();
				will(returnValue("Test Command"));
				oneOf(command).pause();
				will(doAll(checkState(CommandState.RUNNING, commandContext), returnValue(Boolean.TRUE)));
				oneOf(command).renew();
				will(doAll(checkState(CommandState.SUSPENDED, commandContext), throwException(new RuntimeException())));
			}
		});

		commandContext.setState(CommandState.RUNNING);
		commandContext.suspend();
		commandContext.resume();
		Assert.assertEquals("Resume failed, state = ERROR", CommandState.ERROR, commandContext.getState());
	}

	@Test
	public void testStatesIllegalTokens() {
		final Command command = context.mock(Command.class);

		final CommandContext commandContext = createCommandStateContext(command);

		context.checking(new Expectations() {
			{
				allowing(command).getName();
				will(returnValue("Test Command"));
			}
		});

		checkIllegalCommandState(commandContext, CommandState.READY, CommandState.TOKEN_COMPLETE);
		checkIllegalCommandState(commandContext, CommandState.READY, CommandState.TOKEN_FAIL);
		checkIllegalCommandState(commandContext, CommandState.READY, CommandState.TOKEN_RESUME);
		// checkIllegalCommandState(commandContext, CommandState.READY,
		// CommandState.TOKEN_SUSPEND);

		checkIllegalCommandState(commandContext, CommandState.RUNNING, CommandState.TOKEN_START);
		checkIllegalCommandState(commandContext, CommandState.RUNNING, CommandState.TOKEN_RESUME);

		// checkIllegalCommandState(commandContext, CommandState.COMPLETED,
		// CommandState.TOKEN_CANCEL);
		checkIllegalCommandState(commandContext, CommandState.COMPLETED, CommandState.TOKEN_COMPLETE);
		checkIllegalCommandState(commandContext, CommandState.COMPLETED, CommandState.TOKEN_FAIL);
		checkIllegalCommandState(commandContext, CommandState.COMPLETED, CommandState.TOKEN_RESUME);
		// checkIllegalCommandState(commandContext, CommandState.COMPLETED,
		// CommandState.TOKEN_SUSPEND);

		// checkIllegalCommandState(commandContext, CommandState.CANCELLED,
		// CommandState.TOKEN_CANCEL);
		checkIllegalCommandState(commandContext, CommandState.CANCELLED, CommandState.TOKEN_COMPLETE);
		checkIllegalCommandState(commandContext, CommandState.CANCELLED, CommandState.TOKEN_FAIL);
		checkIllegalCommandState(commandContext, CommandState.CANCELLED, CommandState.TOKEN_RESUME);
		// checkIllegalCommandState(commandContext, CommandState.CANCELLED,
		// CommandState.TOKEN_SUSPEND);

		// checkIllegalCommandState(commandContext, CommandState.ERROR,
		// CommandState.TOKEN_CANCEL);
		checkIllegalCommandState(commandContext, CommandState.ERROR, CommandState.TOKEN_COMPLETE);
		checkIllegalCommandState(commandContext, CommandState.ERROR, CommandState.TOKEN_FAIL);
		checkIllegalCommandState(commandContext, CommandState.ERROR, CommandState.TOKEN_RESUME);
		// checkIllegalCommandState(commandContext, CommandState.ERROR,
		// CommandState.TOKEN_SUSPEND);
		checkIllegalCommandState(commandContext, CommandState.ERROR, CommandState.TOKEN_START);

		// checkIllegalCommandState(commandContext, CommandState.CANCELLING,
		// CommandState.TOKEN_CANCEL);
		checkIllegalCommandState(commandContext, CommandState.CANCELLING, CommandState.TOKEN_RESUME);
		checkIllegalCommandState(commandContext, CommandState.CANCELLING, CommandState.TOKEN_START);
		// checkIllegalCommandState(commandContext, CommandState.CANCELLING,
		// CommandState.TOKEN_SUSPEND);

		checkIllegalCommandState(commandContext, CommandState.SUSPENDED, CommandState.TOKEN_COMPLETE);
		checkIllegalCommandState(commandContext, CommandState.SUSPENDED, CommandState.TOKEN_START);
		// checkIllegalCommandState(commandContext, CommandState.SUSPENDED,
		// CommandState.TOKEN_SUSPEND);
	}

	private Action checkState(final CommandState state, final CommandContext commandContext) {
		return new CustomAction("State assertion") {

			@Override
			public Object invoke(Invocation invocation) throws Throwable {
				Assert.assertEquals("State Assertion", state, commandContext.getState());
				return null;
			}
		};
	}

	private CommandContext createCommandStateContext(final Command command) {
		context.checking(new Expectations() {
			{
				allowing(command).getName();
				will(returnValue("Test Command"));
			}
		});

		return new CommandContext(command);
	}

	private void checkIllegalCommandState(CommandContext context, CommandState state, String token) {
		try {
			state.getClass().getMethod(token, CommandContext.class).invoke(state, context);
			Assert.fail("Expected illegal state were not thrown from state " + state);
		} catch (InvocationTargetException e) {
			if (e.getCause().getClass() != IllegalStateException.class) {
				Assert.fail(e.getMessage());
			}
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
	}
}
