package com.synapsense.commandengine.test;

import org.junit.Assert;

import com.synapsense.commandengine.CommandProxy;
import com.synapsense.commandengine.CommandStatus;

public class CommandTestUtils {

	private CommandTestUtils() {
	}

	public static boolean waitUntilFinished(CommandProxy proxy) {
		while (true) {
			if (CommandStatus.COMPLETED.equals(proxy.getStatus())) {
				return true;
			} else if (CommandStatus.FAILED.equals(proxy.getStatus())) {
				Assert.fail("Task failed");
				return false;
			}
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				Assert.fail(e.getMessage());
			}
		}
	}
}
