package com.synapsense.commandengine.test;

import static com.synapsense.commandengine.test.CommandTestUtils.waitUntilFinished;

import java.util.Collection;

import org.jmock.Expectations;
import org.jmock.Mockery;
import org.jmock.api.Invocation;
import org.jmock.integration.junit4.JMock;
import org.jmock.integration.junit4.JUnit4Mockery;
import org.jmock.lib.action.CustomAction;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.synapsense.commandengine.Command;
import com.synapsense.commandengine.CommandManager;
import com.synapsense.commandengine.DefaultCommandManager;
import com.synapsense.commandengine.CommandProxy;
import com.synapsense.commandengine.CommandStatus;
import com.synapsense.commandengine.monitoring.StatusMonitor;

@RunWith(JMock.class)
public class CommandManagerTest {

	private Mockery context;

	@Before
	public void setUp() {
		context = new JUnit4Mockery();
	}

	@Test
	public void testCreateCommandManager() {
		final Command command = context.mock(Command.class);
		context.checking(new Expectations() {
			{
				allowing(command).getName();
				will(returnValue("Test command"));
				oneOf(command).run(with(any(StatusMonitor.class)));
			}
		});
		CommandManager commandManager = new DefaultCommandManager(2);
		commandManager.executeCommand(command);
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			Assert.fail(e.getMessage());
		}
	}

	@Test
	public void testListAllManagedTasks() {
		final Command command = context.mock(Command.class);
		context.checking(new Expectations() {
			{
				allowing(command).getName();
				will(returnValue("Test command"));
				allowing(command).run(with(any(StatusMonitor.class)));
			}
		});
		CommandManager commandManager = new DefaultCommandManager(2);
		commandManager.executeCommand(command);

		Collection<CommandProxy> commands = commandManager.getManagedCommands();

		Assert.assertEquals(1, commands.size());
	}

	@Test(timeout = 3000)
	public void testMonitorCommandProxy() {
		final Command command = context.mock(Command.class);
		context.checking(new Expectations() {
			{
				allowing(command).getName();
				will(returnValue("Test command"));
				oneOf(command).run(with(any(StatusMonitor.class)));
				will(new CustomAction("Sleep action") {

					@Override
					public Object invoke(Invocation invocation) throws Throwable {
						Thread.sleep(100);
						((StatusMonitor) invocation.getParameter(0)).setStatus(CommandStatus.COMPLETED);
						return null;
					}
				});
			}
		});
		CommandManager commandManager = new DefaultCommandManager(1);
		CommandProxy proxy = commandManager.executeCommand(command);
		waitUntilFinished(proxy);
	}
}
