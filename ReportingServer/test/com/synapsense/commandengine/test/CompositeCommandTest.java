package com.synapsense.commandengine.test;

import junit.framework.Assert;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jmock.Expectations;
import org.jmock.Mockery;
import org.jmock.Sequence;
import org.jmock.integration.junit4.JMock;
import org.jmock.integration.junit4.JUnit4Mockery;
import org.jmock.internal.NamedSequence;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.synapsense.commandengine.Command;
import com.synapsense.commandengine.CommandStatus;
import com.synapsense.commandengine.CompositeCommand;
import com.synapsense.commandengine.monitoring.DefaultStatusMonitor;
import com.synapsense.commandengine.monitoring.StatusMonitor;

@RunWith(JMock.class)
public class CompositeCommandTest {

	private static final Log log = LogFactory.getLog(CompositeCommandTest.class);

	private Mockery context;

	@Before
	public void setUp() {
		context = new JUnit4Mockery();
	}

	@Test
	public void testCompositeCommandCreation() {
		CompositeCommand compCommand = new CompositeCommand();
		Assert.assertEquals("Command number in a composite command", 0, compCommand.getCommandsSize());
	}

	@Test
	public void testSuccessfulCompositeCommandRun() {
		final Command command1 = context.mock(Command.class, "command1");
		final Command command2 = context.mock(Command.class, "command2");

		context.checking(new Expectations() {
			{
				allowing(command1).getName();
				will(returnValue("Command1"));
				allowing(command2).getName();
				will(returnValue("Command2"));
				oneOf(command1).run(with(any(StatusMonitor.class)));
				oneOf(command2).run(with(any(StatusMonitor.class)));
			}
		});

		CompositeCommand command = new CompositeCommand();
		command.addCommand(command1);
		command.addCommand(command2);
		command.run(new DefaultStatusMonitor() {
			public void setStatus(CommandStatus status) {
				super.setStatus(status);
				log.debug("Composite command status " + status + " - " + status.getStatusMessage());
			}

			public void setProgress(double progress) {
				super.setProgress(progress);
				log.debug("Composite command progress " + progress);
			}
		});
	}

	@Test
	public void testCompositeCommandRollback() {
		final Command command1 = context.mock(Command.class, "command1");
		final Command command2 = context.mock(Command.class, "command2");

		final Sequence sequence = new NamedSequence("RunAndRollbackSequence");
		context.checking(new Expectations() {
			{
				allowing(command1).getName();
				will(returnValue("Command1"));
				allowing(command2).getName();
				will(returnValue("Command2"));
				oneOf(command1).run(with(any(StatusMonitor.class)));
				inSequence(sequence);
				oneOf(command2).run(with(any(StatusMonitor.class)));
				inSequence(sequence);
				oneOf(command2).rollback();
				inSequence(sequence);
				oneOf(command1).rollback();
				inSequence(sequence);
			}
		});

		CompositeCommand command = new CompositeCommand();
		command.addCommand(command1);
		command.addCommand(command2);
		command.run(new DefaultStatusMonitor());
		command.rollback();
	}
}
