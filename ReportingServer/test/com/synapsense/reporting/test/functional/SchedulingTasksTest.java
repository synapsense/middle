package com.synapsense.reporting.test.functional;

import org.junit.Test;

import com.synapsense.reporting.dto.ReportInfo;
import com.synapsense.reporting.dto.ReportTaskInfo;
import com.synapsense.reporting.dto.ScheduledTaskInfo;
import com.synapsense.reporting.remote.RemoteReportManager;

public class SchedulingTasksTest {

	@Test
	public void testAddSchedulingTask() throws Exception {
		RemoteReportManager manager = FacadeManager.getReportManager();
		ScheduledTaskInfo task = new ScheduledTaskInfo(ReportTaskInfo.newInstance().addReportGeneration(
		        new ReportInfo("Test")), "0/1 * * * * ?");

		manager.scheduleReportTask(task);
	}
}
