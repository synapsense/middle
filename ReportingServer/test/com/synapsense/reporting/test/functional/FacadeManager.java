package com.synapsense.reporting.test.functional;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import com.synapsense.reporting.remote.RemoteReportManager;

public class FacadeManager {

	public static RemoteReportManager getReportManager() throws Exception {
		Registry registry = LocateRegistry.getRegistry("localhost", 4325);
		return (RemoteReportManager) registry.lookup("ReportManager");
	}
}
