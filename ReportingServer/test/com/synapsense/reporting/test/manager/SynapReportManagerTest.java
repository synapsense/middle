package com.synapsense.reporting.test.manager;

import static com.synapsense.commandengine.test.CommandTestUtils.waitUntilFinished;
import static com.synapsense.reporting.test.TestUtils.EMPTY_REPORT_DESIGN_PATH;
import static com.synapsense.reporting.test.TestUtils.EMPTY_REPORT_NAME;
import static com.synapsense.reporting.test.TestUtils.checkAndRemoveTestFiles;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import org.jmock.Expectations;
import org.jmock.Mockery;
import org.jmock.integration.junit4.JMock;
import org.jmock.integration.junit4.JUnit4Mockery;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.synapsense.commandengine.DefaultCommandManager;
import com.synapsense.commandengine.CommandProxy;
import com.synapsense.commandengine.test.CommandTestUtils;
import com.synapsense.reporting.ReportingException;
import com.synapsense.reporting.commands.ReportCommandProxy;
import com.synapsense.reporting.commands.ReportingCommandManager;
import com.synapsense.reporting.dao.DbFsDAOFactory;
import com.synapsense.reporting.dao.ObjectNotFoundException;
import com.synapsense.reporting.dao.ReportDAO;
import com.synapsense.reporting.dao.ReportTemplateDAO;
import com.synapsense.reporting.dao.ReportingDAOException;
import com.synapsense.reporting.dao.ReportingDAOFactory;
import com.synapsense.reporting.dto.ReportInfo;
import com.synapsense.reporting.dto.ReportTaskInfo;
import com.synapsense.reporting.dto.ReportTemplateInfo;
import com.synapsense.reporting.dto.ReportTitle;
import com.synapsense.reporting.export.ExportFormat;
import com.synapsense.reporting.management.JSReportManager;
import com.synapsense.reporting.model.ReportCompilationException;
import com.synapsense.reporting.model.ReportTemplate;
import com.synapsense.reporting.model.JSReport;
import com.synapsense.reporting.scheduling.SchedulingManager;
import com.synapsense.reporting.test.TestUtils;

@RunWith(JMock.class)
public class SynapReportManagerTest {

	private Mockery context;
	private JSReportManager manager;

	@Before
	public void setUp() {
		context = new JUnit4Mockery();
		manager = new JSReportManager();
		manager.setCommandExecutor(new ReportingCommandManager(new DefaultCommandManager(2)));
	}

	@Test
	public void testCreateReportTemplate() {
		final String template = TestUtils.readTestFile(EMPTY_REPORT_DESIGN_PATH);
		try {
			ReportTemplate rt = new ReportTemplate(template);
			final ReportTemplateInfo rtInfo = new ReportTemplateInfo(rt.getName(), rt.getParameters());

			final ReportTemplateDAO rtDao = context.mock(ReportTemplateDAO.class);
			manager.setReportTemplateDAO(rtDao);

			try {
				context.checking(new Expectations() {
					{
						oneOf(rtDao).createReportTemplate(rtInfo, template);
					}
				});
			} catch (ReportingDAOException e) {
				e.printStackTrace();
				Assert.fail(e.getMessage());
			}

			manager.addReportTemplate(template);
		} catch (ReportingException e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}

	@Test(expected = ReportCompilationException.class)
	public void testCreateIncorrectReportTemplate() throws ReportingException {
		final String template = TestUtils.readTestFile(EMPTY_REPORT_DESIGN_PATH) + "abracadabra";

		manager.addReportTemplate(template);
	}

	@Test
	public void testUpdateReportTemplate() {
		final String template = TestUtils.readTestFile(EMPTY_REPORT_DESIGN_PATH);

		try {

			ReportTemplate rt = new ReportTemplate(template);
			final ReportTemplateDAO rtDao = context.mock(ReportTemplateDAO.class);
			final ReportTemplateInfo rtIfno = new ReportTemplateInfo(rt.getName(), rt.getParameters());
			manager.setReportTemplateDAO(rtDao);

			context.checking(new Expectations() {
				{
					oneOf(rtDao).createReportTemplate(rtIfno, template);
					oneOf(rtDao).getReportTemplateInfo(rtIfno.getReportTemplateName());
					will(returnValue(rtIfno));
					oneOf(rtDao).updateReportTemplate(rtIfno, template);
				}
			});
			manager.addReportTemplate(template);
			manager.updateReportTemplate(EMPTY_REPORT_NAME, template);
		} catch (ReportingException e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}

	@Test
	public void testRemoveReportTemplate() {
		final ReportTemplateDAO rtDao = context.mock(ReportTemplateDAO.class);
		final SchedulingManager scheduler = context.mock(SchedulingManager.class);
		manager.setReportTemplateDAO(rtDao);
		manager.setScheduler(scheduler);
		try {
			context.checking(new Expectations() {
				{
					oneOf(rtDao).removeReportTemplate(EMPTY_REPORT_NAME);
					oneOf(scheduler).removeScheduledTasks(EMPTY_REPORT_NAME);
				}
			});
			manager.removeReportTemplate(EMPTY_REPORT_NAME);
		} catch (ReportingException e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}

	}

	@Test
	public void testSuccessfullyFetchAllReportTemplates() {
		final Collection<String> reportList = new ArrayList<String>();
		reportList.add("Crac Supply/Return Averages");
		reportList.add("Rack Overcooling");

		final ReportTemplateDAO dao = context.mock(ReportTemplateDAO.class);

		// Creating Mock object for DAO to return specified list of reports
		// template names
		context.checking(new Expectations() {
			{
				oneOf(dao).getReportTemplates();
				will(returnValue(reportList));
			}
		});

		manager.setReportTemplateDAO(dao);

		Collection<String> returnedReportList = manager.getAvailableReportTemplates();

		Assert.assertEquals(reportList, returnedReportList);
	}

	@Test
	public void testSuccessfullFetchReportTemplate() throws ObjectNotFoundException {

		final String template = TestUtils.readTestFile(EMPTY_REPORT_DESIGN_PATH);

		final ReportTemplateInfo rInfo = new ReportTemplateInfo(EMPTY_REPORT_NAME);

		final ReportTemplateDAO dao = context.mock(ReportTemplateDAO.class);

		// Creating Mock object for DAO to return specified list of reports
		// template names
		try {
			context.checking(new Expectations() {
				{
					oneOf(dao).getReportTemplateDesign(EMPTY_REPORT_NAME);
					will(returnValue(template));
					oneOf(dao).getReportTemplateInfo(EMPTY_REPORT_NAME);
					will(returnValue(rInfo));
				}
			});
		} catch (ReportingDAOException e) {
			Assert.fail(e.getMessage());
		}

		manager.setReportTemplateDAO(dao);

		ReportTemplateInfo reportTemplate = manager.getReportTemplateInfo(EMPTY_REPORT_NAME);

		Assert.assertEquals(EMPTY_REPORT_NAME, reportTemplate.getReportTemplateName());

		String design = dao.getReportTemplateDesign(EMPTY_REPORT_NAME);
		Assert.assertEquals(template, design);
	}

	@Test(timeout = 10000)
	public void testSuccessfulReportGeneration() {
		final ReportInfo reportInfo = new ReportInfo(EMPTY_REPORT_NAME, EMPTY_REPORT_NAME);
		final String template = TestUtils.readTestFile(EMPTY_REPORT_DESIGN_PATH);

		try {
			final ReportTemplateDAO rtDao = context.mock(ReportTemplateDAO.class);
			final ReportDAO rDao = context.mock(ReportDAO.class);
			// Creating Mock object for DAO to return specified URI to report
			// template design file
			try {
				context.checking(new Expectations() {
					{
						oneOf(rtDao).getReportTemplateDesign(EMPTY_REPORT_NAME);
						will(returnValue(template));
						oneOf(rDao).saveReport(with(any(ReportInfo.class)), with(any(JSReport.class)));
					}
				});
			} catch (ReportingDAOException e) {
				Assert.fail(e.getMessage());
			}

			manager.setReportTemplateDAO(rtDao);
			manager.setReportDAO(rDao);

			CommandProxy proxy = manager.runReportGeneration(reportInfo);

			if (!CommandTestUtils.waitUntilFinished(proxy)) {
				Assert.fail("Report generation failed by timeout");
			}
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}

	@Test
	public void testSuccessfulyFetchExistingReports() {

		final ReportDAO dao = context.mock(ReportDAO.class);

		final Collection<ReportTitle> existingReports = new ArrayList<ReportTitle>();
		existingReports.add(new ReportTitle(EMPTY_REPORT_NAME, EMPTY_REPORT_NAME, 1));
		try {

			context.checking(new Expectations() {
				{
					oneOf(dao).getGeneratedReports(EMPTY_REPORT_NAME);
					will(returnValue(existingReports));
				}
			});

			manager.setReportDAO(dao);
			Collection<ReportTitle> reports = manager.getGeneratedReports(EMPTY_REPORT_NAME);

			Assert.assertEquals(1, reports.size());

			Assert.assertTrue(reports.contains(new ReportTitle(EMPTY_REPORT_NAME, EMPTY_REPORT_NAME, 1)));
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}

	@Test(timeout = 10000)
	public void testSuccessfulReportExport() throws ReportingException {
		final ReportInfo reportInfo = new ReportInfo(EMPTY_REPORT_NAME, EMPTY_REPORT_NAME);
		reportInfo.setReportId(1);

		final ReportDAO dao = context.mock(ReportDAO.class);
		try {
			context.checking(new Expectations() {
				{
					oneOf(dao).getReportInfo(1);
					will(returnValue(reportInfo));
					oneOf(dao).exportReport(1, ExportFormat.PDF);
					will(returnValue("test"));
				}
			});
		} catch (ReportingDAOException e) {
			Assert.fail(e.getMessage());
		}

		manager.setReportDAO(dao);
		CommandProxy proxy;
		try {
			proxy = manager.exportReport(1, ExportFormat.PDF);
			if (!CommandTestUtils.waitUntilFinished(proxy)) {
				Assert.fail("Report export failed by timeout");
			}
		} catch (ObjectNotFoundException e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}

	@Test(timeout = 10000)
	public void testReportGenerationAndExportTaskTest() {

		try {

			final SchedulingManager scheduler = context.mock(SchedulingManager.class);
			manager.setScheduler(scheduler);
			context.checking(new Expectations() {
				{
					oneOf(scheduler).removeScheduledTasks(EMPTY_REPORT_NAME);
				}
			});

			ReportingDAOFactory daoFactory = new DbFsDAOFactory("org.hsqldb.jdbcDriver", "jdbc:hsqldb:file:"
			        + TestUtils.DB_FOLDER + TestUtils.DB_NAME, "./test/resources/testStorage/database/init_db.sql",
			        "./conf/database/query.templates", TestUtils.EMPTY_REPORT_TMP_PATH);

			manager.setReportTemplateDAO(daoFactory.newReportTemplateDAO());
			manager.setReportDAO(daoFactory.newReportDao());

			manager.addReportTemplate(TestUtils.readTestFile(EMPTY_REPORT_DESIGN_PATH));

			ReportTaskInfo taskContext = ReportTaskInfo.newInstance()
			        .addReportGeneration(new ReportInfo(EMPTY_REPORT_NAME, EMPTY_REPORT_NAME))
			        .addExportFormats(Arrays.asList(ExportFormat.avFormats));

			ReportCommandProxy proxy = manager.runReportTask(taskContext);
			ReportTaskInfo taskInfo = proxy.getTaskInfo();
			Assert.assertEquals(taskContext.getReportTemplateName(), taskInfo.getReportTemplateName());
			waitUntilFinished(proxy);

			manager.removeReportTemplate(EMPTY_REPORT_NAME);
			checkAndRemoveTestFiles(new String[] { TestUtils.EMPTY_REPORT_TMP_PATH });
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}

	@Test(timeout = 3000)
	public void testMonitorManagedCommands() {

		try {
			ReportingDAOFactory daoFactory = new DbFsDAOFactory("org.hsqldb.jdbcDriver", "jdbc:hsqldb:file:"
			        + TestUtils.DB_FOLDER + TestUtils.DB_NAME, "./test/resources/testStorage/database/init_db.sql",
			        "./conf/database/query.templates", TestUtils.EMPTY_REPORT_TMP_PATH);

			manager.setReportTemplateDAO(daoFactory.newReportTemplateDAO());
			manager.setReportDAO(daoFactory.newReportDao());

			manager.addReportTemplate(TestUtils.readTestFile(EMPTY_REPORT_DESIGN_PATH));

			manager.clearReportCommands();
			Assert.assertEquals(0, manager.getReportCommands().size());

			CommandProxy proxy = manager.runReportGeneration(new ReportInfo(EMPTY_REPORT_NAME, EMPTY_REPORT_NAME));
			waitUntilFinished(proxy);

			Collection<ReportCommandProxy> reportCommands = manager.getReportCommands();
			Assert.assertEquals("One managed command", 1, reportCommands.size());

			ReportTaskInfo taskInfo = reportCommands.iterator().next().getTaskInfo();
			Assert.assertEquals(EMPTY_REPORT_NAME, taskInfo.getReportsGeneration().iterator().next()
			        .getReportTemplateName());

			manager.clearReportCommands();
			Assert.assertEquals("Managed commands size", 0, manager.getReportCommands().size());
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}
}
