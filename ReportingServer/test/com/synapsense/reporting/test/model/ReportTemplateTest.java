package com.synapsense.reporting.test.model;

import static com.synapsense.reporting.test.TestUtils.EMPTY_REPORT_DESIGN_PATH;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.concurrent.CountDownLatch;

import org.hamcrest.Description;
import org.jmock.Expectations;
import org.jmock.Mockery;
import org.jmock.api.Action;
import org.jmock.api.Invocation;
import org.jmock.integration.junit4.JMock;
import org.jmock.integration.junit4.JUnit4Mockery;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.synapsense.reporting.dto.ReportInfo;
import com.synapsense.reporting.model.Report;
import com.synapsense.reporting.model.ReportCompilationException;
import com.synapsense.reporting.model.ReportGenerationException;
import com.synapsense.reporting.model.ReportGenerationHandler;
import com.synapsense.reporting.model.ReportGenerationListener;
import com.synapsense.reporting.model.ReportParameter;
import com.synapsense.reporting.model.ReportTemplate;
import com.synapsense.reporting.test.TestUtils;

@RunWith(JMock.class)
public class ReportTemplateTest {

	private Mockery context;

	@Before
	public void setUp() {
		context = new JUnit4Mockery();
	}

	@Test
	public void testSuccessfullEmptyReportTemplateCompilation() {
		try {
			ReportTemplate template = new ReportTemplate(new FileInputStream(EMPTY_REPORT_DESIGN_PATH));
			Assert.assertEquals("EmptyReport", template.getName());
			template = new ReportTemplate(TestUtils.readTestFile(EMPTY_REPORT_DESIGN_PATH));
			Assert.assertEquals("EmptyReport", template.getName());
		} catch (ReportCompilationException e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		} catch (FileNotFoundException e) {
			Assert.fail(e.getMessage());
		}
	}

	@Test
	public void testCorrectReportParametersLoading() {
		try {
			ReportTemplate template = null;
			try {
				template = new ReportTemplate(new FileInputStream(EMPTY_REPORT_DESIGN_PATH));
			} catch (FileNotFoundException e) {
				Assert.fail(e.getMessage());
				return;
			}
			ReportParameter stringParameter = template.getParameter("StringParameter");
			ReportParameter integerParameter = template.getParameter("IntegerParameter");

			Assert.assertEquals(stringParameter.getType(), java.lang.String.class);
			Assert.assertEquals(integerParameter.getType(), java.lang.Integer.class);
		} catch (ReportCompilationException e) {
			Assert.fail(e.getMessage());
		}
	}

	@Test
	public void testSuccessfulEmptyReportGeneration() {
		ReportTemplate template = null;
		try {
			template = new ReportTemplate(new FileInputStream(EMPTY_REPORT_DESIGN_PATH));
		} catch (FileNotFoundException e) {
			Assert.fail(e.getMessage());
			return;
		} catch (ReportCompilationException e) {
			Assert.fail(e.getMessage());
			return;
		}
		try {
			template.generateReport(new ReportInfo("EmptyReportTest"));
		} catch (ReportGenerationException e) {
			Assert.fail(e.getMessage());
		}
	}

	@Test
	public void testAsynchroniousReportGeneration() {
		try {

			final CountDownLatch doneLatch = new CountDownLatch(1);

			ReportTemplate template = new ReportTemplate(new FileInputStream(TestUtils.EMPTY_REPORT_DESIGN_PATH));
			final ReportGenerationListener listener = context.mock(ReportGenerationListener.class);

			context.checking(new Expectations() {
				{
					oneOf(listener).completed(with(any(Report.class)));
					will(doAll(new Action() {

						@Override
						public void describeTo(Description arg0) {
						}

						@Override
						public Object invoke(Invocation invocation) throws Throwable {
							doneLatch.countDown();
							return null;
						}
					}));
				}
			});
			ReportGenerationHandler handler = template.createReportGenerationHandler(new ReportInfo("TestReport"));
			handler.addListener(listener);
			handler.startGeneration();

			doneLatch.await();
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}
}
