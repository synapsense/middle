package com.synapsense.reporting.test.model;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

// ESCA-JAVA0024: Just a suite class
@RunWith(Suite.class)
@SuiteClasses({ ReportTemplateTest.class, ReportTest.class })
public class ReportingModelTestSuite {
}
