package com.synapsense.reporting.test.model;

import static com.synapsense.reporting.test.TestUtils.EMPTY_REPORT_DESIGN_PATH;
import static com.synapsense.reporting.test.TestUtils.EMPTY_REPORT_TMP_PATH;
import static com.synapsense.reporting.test.TestUtils.checkAndRemoveTestFiles;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.synapsense.reporting.ReportingException;
import com.synapsense.reporting.dto.ReportInfo;
import com.synapsense.reporting.export.ExportFormat;
import com.synapsense.reporting.model.Report;
import com.synapsense.reporting.model.ReportTemplate;
import com.synapsense.reporting.utils.FileUtils;

public class ReportTest {

	private Report testedReport;

	@Before
	public void setUp() {
		try {
			ReportTemplate template = new ReportTemplate(new FileInputStream(EMPTY_REPORT_DESIGN_PATH));
			testedReport = template.generateReport(new ReportInfo("EmptyReportTest"));
			FileUtils.checkOrCreateDirectory(EMPTY_REPORT_TMP_PATH);
		} catch (FileNotFoundException e) {
			Assert.fail(e.getMessage());
		} catch (ReportingException e) {
			Assert.fail(e.getMessage());
		}
	}

	@Test
	public void testSuccessfulReportExportToFile() {
		try {

			String exportPath = EMPTY_REPORT_TMP_PATH + "/export";
			testedReport.export(ExportFormat.PDF, exportPath);
			File exportedFile = new File(exportPath);
			Assert.assertTrue(exportedFile.isFile());

			testedReport.export(ExportFormat.HTML, exportPath);
			exportedFile = new File(exportPath);
			Assert.assertTrue(exportedFile.isFile());
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}

	@After
	public void tearDown() {
		checkAndRemoveTestFiles(new String[] { EMPTY_REPORT_TMP_PATH });
	}
}
