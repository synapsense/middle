package com.synapsense.reporting.test.commands;

import static com.synapsense.reporting.test.TestUtils.EMPTY_REPORT_DESIGN_PATH;
import static com.synapsense.reporting.test.TestUtils.EMPTY_REPORT_NAME;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jmock.Expectations;
import org.jmock.Mockery;
import org.jmock.integration.junit4.JMock;
import org.jmock.integration.junit4.JUnit4Mockery;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.synapsense.commandengine.CommandStatus;
import com.synapsense.commandengine.monitoring.DefaultStatusMonitor;
import com.synapsense.commandengine.monitoring.StatusMonitor;
import com.synapsense.reporting.commands.CommandFactory;
import com.synapsense.reporting.commands.ReportGenerationCommand;
import com.synapsense.reporting.dao.ReportDAO;
import com.synapsense.reporting.dao.ReportTemplateDAO;
import com.synapsense.reporting.dao.ReportingDAOException;
import com.synapsense.reporting.dto.ReportInfo;
import com.synapsense.reporting.model.JSReport;
import com.synapsense.reporting.test.TestUtils;

@RunWith(JMock.class)
public class ReportGenerationCommandTest {

	private static final Log log = LogFactory.getLog(ReportGenerationCommandTest.class);

	private Mockery context;

	@Before
	public void setUp() {
		context = new JUnit4Mockery();
	}

	@Test(timeout = 15000)
	public void testSuccessfullyRunReportGenerationCommand() {
		// Creating Mock object for DAO to check if the command saves the result
		// to DAO
		final ReportTemplateDAO rtDao = context.mock(ReportTemplateDAO.class);
		final ReportDAO rDao = context.mock(ReportDAO.class);
		final String reportDesign = TestUtils.readTestFile(EMPTY_REPORT_DESIGN_PATH);
		try {
			context.checking(new Expectations() {
				{
					oneOf(rtDao).getReportTemplateDesign(EMPTY_REPORT_NAME);
					will(returnValue(reportDesign));
					oneOf(rDao).saveReport(with(any(ReportInfo.class)), with(any(JSReport.class)));
				}
			});
		} catch (ReportingDAOException e) {
			Assert.fail(e.getMessage());
		}

		final ReportGenerationCommand command = CommandFactory.newReportGenerationCommand(new ReportInfo(
		        EMPTY_REPORT_NAME, "EmptyReportTest"), rtDao, rDao);

		StatusMonitor monitor = new DefaultStatusMonitor() {
			public void setStatus(CommandStatus status) {
				super.setStatus(status);
				log.debug(command.getName() + " status " + status + " - " + status.getStatusMessage());
			}

			public void setProgress(double progress) {
				super.setProgress(progress);
				log.debug(command.getName() + " progress " + progress);
			}
		};

		command.run(monitor);

		Assert.assertEquals("Status must be COMPLETED", CommandStatus.COMPLETED, monitor.getStatus());
		Assert.assertEquals("Progress must be 1.0", 1.0, monitor.getProgress(), 0.0);
	}
}
