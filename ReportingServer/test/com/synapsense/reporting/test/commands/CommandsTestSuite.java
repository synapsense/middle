package com.synapsense.reporting.test.commands;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ ReportGenerationCommandTest.class, ReportExportCommandTest.class })
public class CommandsTestSuite {
}
