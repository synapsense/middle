package com.synapsense.reporting.test.commands;

import org.jmock.Expectations;
import org.jmock.Mockery;
import org.jmock.integration.junit4.JMock;
import org.jmock.integration.junit4.JUnit4Mockery;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.synapsense.commandengine.monitoring.DefaultStatusMonitor;
import com.synapsense.reporting.commands.CommandFactory;
import com.synapsense.reporting.commands.ReportExportCommand;
import com.synapsense.reporting.dao.ReportDAO;
import com.synapsense.reporting.dao.ReportingDAOException;
import com.synapsense.reporting.dto.ReportInfo;
import com.synapsense.reporting.export.ExportFormat;

@RunWith(JMock.class)
public class ReportExportCommandTest {

	private Mockery context;

	@Before
	public void setUp() {
		context = new JUnit4Mockery();
	}

	@Test
	public void testSuccessfullReportExport() {
		final ReportDAO dao = context.mock(ReportDAO.class);
		try {
			context.checking(new Expectations() {
				{
					oneOf(dao).exportReport(1, ExportFormat.PDF);
				}
			});
		} catch (ReportingDAOException e) {
			Assert.fail(e.getMessage());
		}

		ReportInfo rInfo = new ReportInfo("Test");
		rInfo.setReportId(1);

		ReportExportCommand command = CommandFactory.newReportExportCommand(dao, rInfo, ExportFormat.PDF);
		command.run(new DefaultStatusMonitor());
	}
}
