package com.synapsense.reporting.test.commands;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.jmock.Expectations;
import org.jmock.Mockery;
import org.jmock.integration.junit4.JMock;
import org.jmock.integration.junit4.JUnit4Mockery;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.synapsense.commandengine.CompositeCommand;
import com.synapsense.commandengine.monitoring.DefaultStatusMonitor;
import com.synapsense.reporting.commands.CommandFactory;
import com.synapsense.reporting.dao.ObjectNotFoundException;
import com.synapsense.reporting.dao.ReportDAO;
import com.synapsense.reporting.dto.ReportInfo;
import com.synapsense.reporting.export.ExportFormat;
import com.synapsense.reporting.test.TestUtils;

/**
 * This test shouldn't be used as a part of unit tests suite as it sends mail
 * messages
 */
@RunWith(JMock.class)
public class ReportMailCommandTest {

	private Mockery context;

	@Before
	public void setUp() {
		context = new JUnit4Mockery();
	}

	@Test
	public void testSendReportViaEmail() {

		final ReportDAO dao = context.mock(ReportDAO.class);

		try {
			context.checking(new Expectations() {
				{
					oneOf(dao).exportReport(1, ExportFormat.PDF);
					will(returnValue(TestUtils.EMPTY_REPORT_DESIGN_PATH));
				}

			});
		} catch (ObjectNotFoundException e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}

		Properties props = new Properties();
		props.put("mail.transport.protocol", "smtp");
		props.put("mail.smtp.port", "25");
		props.put("mail.smtp.host", "qmail.merann.ru");
		props.put("mail.from", "prj.synapsense@mera.ru");

		List<String> destinations = new ArrayList<String>();
		destinations.add("dgrudzin@mera.ru");
		destinations.add("dgrudzinskiy@gmail.com");

		CompositeCommand compCommand = new CompositeCommand();

		ReportInfo reportInfo = new ReportInfo("Test");
		reportInfo.setReportId(1);

		compCommand.addCommand(CommandFactory.newReportMailCommand(dao, props, destinations, reportInfo));
		compCommand.run(new DefaultStatusMonitor());

	}
}
