package com.synapsense.reporting.test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import org.junit.Assert;

import com.synapsense.reporting.utils.FileUtils;

public final class TestUtils {

	// Test reports storage
	public static final String STORAGE_PATH = "./test/resources/testStorage";

	public static final String TEST_REPORTS_PATH = STORAGE_PATH + "/fs";

	public static final String STORAGE_TEMPLATES = STORAGE_PATH + "/templates";

	public static final String EMPTY_REPORT_NAME = "EmptyReport";
	public static final String EMPTY_REPORT_ROOT_PATH = STORAGE_TEMPLATES;
	public static final String EMPTY_REPORT_DESIGN_PATH = STORAGE_TEMPLATES + "/" + EMPTY_REPORT_NAME + ".jrxml";
	public static final String EMPTY_REPORT_TMP_PATH = STORAGE_PATH + "/tmp";

	public static final String CRAC_REPORT_NAME = "CracSupplyReturn";
	public static final String CRAC_REPORT_DESIGN_PATH = STORAGE_TEMPLATES + "/" + CRAC_REPORT_NAME + ".jrxml";

	public static final String SUPPLYRETURN_REPORT_NAME = "CracSupplyReturn";
	public static final String RACKOVERCOOLING_REPORT_NAME = "RackOverCooling";
	public static final String TEST_REPORT_NAME = "TestReport";

	public static final String TEMPLATE_EXT = ".jrml";
	public static final String REPORT_EXT = ".jprint";

	public static final String DB_FOLDER = "./test/resources/testStorage/database/";
	public static final String DB_NAME = "testdb";

	public static void checkAndRemoveTestFiles(String[] paths) {
		for (String path : paths) {
			File file = new File(path);
			if (!file.exists()) {
				Assert.fail("File " + path + " not found in test repository");
				return;
			}

			if (file.isDirectory()) {
				if (!FileUtils.deleteDirectory(file)) {
					Assert.fail("Unable to delete directory " + path);
					return;
				}

			} else if (!file.delete()) {
				Assert.fail("Unable to delete file " + path);
			}
		}
	}

	public static String readTestFile(String fileName) {
		StringBuilder builder = new StringBuilder();
		try {
			BufferedReader reader = new BufferedReader(new FileReader(fileName));
			String line = null;
			while ((line = reader.readLine()) != null) {
				builder.append(line);
			}
			reader.close();
			builder.trimToSize();
		} catch (IOException e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
		return builder.toString();
	}

	private TestUtils() {
	}
}
