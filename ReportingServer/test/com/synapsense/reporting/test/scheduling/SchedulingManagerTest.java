package com.synapsense.reporting.test.scheduling;

import org.jmock.Expectations;
import org.jmock.Mockery;
import org.jmock.integration.junit4.JMock;
import org.jmock.integration.junit4.JUnit4Mockery;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.quartz.CronTrigger;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;

import com.synapsense.reporting.dto.ReportInfo;
import com.synapsense.reporting.dto.ReportTaskInfo;
import com.synapsense.reporting.dto.ScheduledTaskInfo;
import com.synapsense.reporting.scheduling.JSSchedulingManager;
import com.synapsense.reporting.scheduling.SchedulingManagerException;

@RunWith(JMock.class)
public class SchedulingManagerTest {

	private Mockery context;

	@Before
	public void setUp() {
		context = new JUnit4Mockery();
	}

	@Test
	public void testCreateSchedulingManager() {
		try {
			JSSchedulingManager manager = new JSSchedulingManager();
			manager.start();
			manager.shutdown();
		} catch (SchedulingManagerException e) {
			Assert.fail(e.getMessage());
			e.printStackTrace();
		}
	}

	@Test
	public void testCreateScheduledTask() throws SchedulingManagerException {

		String cron = "0 0/5 * * * ?";

		final ReportTaskInfo taskInfo = ReportTaskInfo.newInstance()
		        .addReportGeneration(new ReportInfo("Test", "Test")).addMailRecipient("me");

		final ScheduledTaskInfo scTask = new ScheduledTaskInfo(taskInfo, cron);
		final Scheduler scheduler = context.mock(Scheduler.class);

		try {
			context.checking(new Expectations() {
				{
					oneOf(scheduler).scheduleJob(with(any(JobDetail.class)), with(any(CronTrigger.class)));
				}
			});
		} catch (SchedulerException e) {
			e.printStackTrace();
		}

		JSSchedulingManager manager = new JSSchedulingManager();
		manager.setScheduler(scheduler);
		manager.scheduleReportTask(scTask);
	}

	@Test
	public void removeScheduledTask() throws SchedulerException, SchedulingManagerException {
		final Scheduler scheduler = context.mock(Scheduler.class);

		context.checking(new Expectations() {
			{
				oneOf(scheduler).deleteJob("Test", "Test");
			}
		});

		JSSchedulingManager manager = new JSSchedulingManager();
		manager.setScheduler(scheduler);
		manager.removeScheduledTask("Test", "Test");
	}

	@Test
	public void removeTemplateTasks() throws SchedulerException, SchedulingManagerException {
		final Scheduler scheduler = context.mock(Scheduler.class);

		context.checking(new Expectations() {
			{
				oneOf(scheduler).getJobNames("Test");
				will(returnValue(new String[] { "Task1", "Task2" }));
				oneOf(scheduler).deleteJob("Task1", "Test");
				oneOf(scheduler).deleteJob("Task2", "Test");
			}
		});

		JSSchedulingManager manager = new JSSchedulingManager();
		manager.setScheduler(scheduler);
		manager.removeScheduledTasks("Test");
	}

	@Test
	public void getTemplateTasks() throws SchedulerException, SchedulingManagerException {
		final Scheduler scheduler = context.mock(Scheduler.class);

		final JobDetail detail = new JobDetail();
		detail.getJobDataMap().put(JSSchedulingManager.SCHEDULED_TASK,
		        new ScheduledTaskInfo(ReportTaskInfo.newInstance(), ""));

		context.checking(new Expectations() {
			{
				oneOf(scheduler).getJobNames("Test");
				will(returnValue(new String[] { "Task1", "Task2" }));
				oneOf(scheduler).getJobDetail("Task1", "Test");
				will(returnValue(detail));
				oneOf(scheduler).getJobDetail("Task2", "Test");
				will(returnValue(detail));
			}
		});

		JSSchedulingManager manager = new JSSchedulingManager();
		manager.setScheduler(scheduler);
		Assert.assertEquals(2, manager.getScheduledTasks("Test").size());
	}
}
