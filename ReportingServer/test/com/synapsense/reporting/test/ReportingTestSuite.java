package com.synapsense.reporting.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.synapsense.commandengine.test.CommandEngineTestSuite;
import com.synapsense.reporting.test.commands.CommandsTestSuite;
import com.synapsense.reporting.test.dao.DAOTestSuite;
import com.synapsense.reporting.test.manager.SynapReportManagerTest;
import com.synapsense.reporting.test.model.ReportingModelTestSuite;
import com.synapsense.reporting.test.scheduling.SchedulingManagerTest;

@RunWith(Suite.class)
@SuiteClasses({ ReportingModelTestSuite.class, CommandsTestSuite.class, SynapReportManagerTest.class,
        DAOTestSuite.class, CommandEngineTestSuite.class, SchedulingManagerTest.class })
public class ReportingTestSuite {
}
