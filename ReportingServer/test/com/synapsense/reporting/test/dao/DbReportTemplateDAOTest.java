package com.synapsense.reporting.test.dao;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Collection;
import java.util.Iterator;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.synapsense.reporting.dao.DbFsDAOFactory;
import com.synapsense.reporting.dao.ObjectExistsException;
import com.synapsense.reporting.dao.ObjectNotFoundException;
import com.synapsense.reporting.dao.ReportTemplateDAO;
import com.synapsense.reporting.dao.ReportingDAOException;
import com.synapsense.reporting.dao.ReportingDAOFactory;
import com.synapsense.reporting.dto.ReportTemplateInfo;
import com.synapsense.reporting.model.ReportCompilationException;
import com.synapsense.reporting.model.ReportParameter;
import com.synapsense.reporting.model.ReportTemplate;
import com.synapsense.reporting.test.TestUtils;

public class DbReportTemplateDAOTest {

	private static ReportingDAOFactory daoFactory;

	@BeforeClass
	public static void setUp() throws ReportingDAOException {

		daoFactory = new DbFsDAOFactory("org.hsqldb.jdbcDriver", "jdbc:hsqldb:file:" + TestUtils.DB_FOLDER
		        + TestUtils.DB_NAME, "./test/resources/testStorage/database/init_db.sql",
		        "./conf/database/query.templates", TestUtils.STORAGE_PATH);
	}

	@Test
	public void testCreateDAO() throws ReportingDAOException {
		ReportTemplateDAO rtDao = daoFactory.newReportTemplateDAO();
		Assert.assertNotNull("Report template DAO object is null", rtDao);
	}

	@Test(expected = ObjectNotFoundException.class)
	public void testCreateReportTemplate() throws ReportingDAOException {
		ReportTemplateDAO rtDAO = daoFactory.newReportTemplateDAO();
		ReportTemplateInfo reportTemplate = new ReportTemplateInfo(TestUtils.EMPTY_REPORT_NAME);
		rtDAO.createReportTemplate(reportTemplate, TestUtils.readTestFile(TestUtils.EMPTY_REPORT_DESIGN_PATH));
		try {

			Assert.assertFalse("Report template id", reportTemplate.getReportTemplateId() == 0);

			reportTemplate = rtDAO.getReportTemplateInfo(TestUtils.EMPTY_REPORT_NAME);

			Assert.assertNotNull("Report template object is null", reportTemplate);

			Assert.assertEquals("Report template name is incorrect", TestUtils.EMPTY_REPORT_NAME,
			        reportTemplate.getReportTemplateName());

		} catch (ReportingDAOException e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		} finally {
			rtDAO.removeReportTemplate(reportTemplate.getReportTemplateName());
		}

		rtDAO.getReportTemplateInfo(TestUtils.EMPTY_REPORT_NAME);
	}

	@Test(expected = ObjectNotFoundException.class)
	public void testCreateReportTemplateCheckParameters() throws ReportingDAOException, ReportCompilationException,
	        FileNotFoundException {
		ReportTemplateDAO rtDAO = daoFactory.newReportTemplateDAO();
		try {

			ReportTemplate rt = new ReportTemplate(new FileInputStream(TestUtils.EMPTY_REPORT_DESIGN_PATH));

			ReportTemplateInfo rtInfo = new ReportTemplateInfo(TestUtils.EMPTY_REPORT_NAME, rt.getParameters());

			rtDAO.createReportTemplate(rtInfo, TestUtils.readTestFile(TestUtils.EMPTY_REPORT_DESIGN_PATH));

			ReportTemplateInfo reportTemplate = rtDAO.getReportTemplateInfo(TestUtils.EMPTY_REPORT_NAME);

			Assert.assertNotNull("Report template object is null", reportTemplate);

			Assert.assertEquals("Report template name is incorrect", TestUtils.EMPTY_REPORT_NAME,
			        reportTemplate.getReportTemplateName());

			Collection<ReportParameter> parameters = reportTemplate.getParameters();
			Assert.assertEquals("Paramters number", 2, parameters.size());

			Iterator<ReportParameter> iter = parameters.iterator();
			ReportParameter stringParameter = iter.next();
			Assert.assertEquals("String parameter type", String.class, stringParameter.getType());
			Assert.assertEquals("String parameter name", "StringParameter", stringParameter.getName());

			ReportParameter integerParameter = iter.next();
			Assert.assertEquals("Integer parameter type", Integer.class, integerParameter.getType());
			Assert.assertEquals("Integer parameter name", "IntegerParameter", integerParameter.getName());

		} catch (ReportingDAOException e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		} finally {
			rtDAO.removeReportTemplate(TestUtils.EMPTY_REPORT_NAME);
		}

		rtDAO.getReportTemplateInfo(TestUtils.EMPTY_REPORT_NAME);
	}

	@Test(expected = ObjectExistsException.class)
	public void testDuplicateReportTemplate() throws ReportingDAOException {
		ReportTemplateDAO rtDAO = daoFactory.newReportTemplateDAO();

		try {
			rtDAO.createReportTemplate(new ReportTemplateInfo("Test"), "");
			rtDAO.createReportTemplate(new ReportTemplateInfo("Test"), "");
		} finally {
			rtDAO.removeReportTemplate("Test");
		}
	}

	@Test
	public void testGetTemplateDesign() throws ReportingDAOException, ReportCompilationException, FileNotFoundException {
		ReportTemplateDAO rtDAO = daoFactory.newReportTemplateDAO();

		ReportTemplate rt = new ReportTemplate(new FileInputStream(TestUtils.EMPTY_REPORT_DESIGN_PATH));

		String design = TestUtils.readTestFile(TestUtils.EMPTY_REPORT_DESIGN_PATH);

		try {
			rtDAO.createReportTemplate(new ReportTemplateInfo(rt.getName(), rt.getParameters()), design);

			Assert.assertEquals("Report template design by name", design, rtDAO.getReportTemplateDesign(rt.getName()));

		} finally {
			rtDAO.removeReportTemplate(rt.getName());
		}
	}

	@Test
	public void testGetReportTemplates() throws ReportingDAOException {
		ReportTemplateDAO rtDAO = daoFactory.newReportTemplateDAO();

		rtDAO.createReportTemplate(new ReportTemplateInfo("Test1"), "");
		rtDAO.createReportTemplate(new ReportTemplateInfo("Test2"), "");
		rtDAO.createReportTemplate(new ReportTemplateInfo("Test3"), "");

		Collection<String> rts = rtDAO.getReportTemplates();

		Assert.assertEquals("Templates size", 3, rts.size());
		Assert.assertTrue("Templates contains Test1", rts.contains("Test1"));
		Assert.assertTrue("Templates contains Test2", rts.contains("Test2"));
		Assert.assertTrue("Templates contains Test3", rts.contains("Test3"));

		rtDAO.removeReportTemplate("Test1");
		rtDAO.removeReportTemplate("Test2");
		rtDAO.removeReportTemplate("Test3");
	}

	@Test
	public void testUpdateReportTemplate() throws ReportingDAOException {
		ReportTemplateDAO rtDAO = daoFactory.newReportTemplateDAO();

		ReportTemplateInfo rtInfo = new ReportTemplateInfo("Test1");
		rtInfo.addParameter(new ReportParameter("testParam1", String.class));

		rtDAO.createReportTemplate(rtInfo, "design1");

		// change design and parameters
		rtInfo = rtDAO.getReportTemplateInfo("Test1");

		rtInfo.clearParameters();
		ReportParameter param = new ReportParameter("testParam2", Double.class);
		rtInfo.addParameter(param);

		rtDAO.updateReportTemplate(rtInfo, "design2");

		rtInfo = rtDAO.getReportTemplateInfo("Test1");

		Assert.assertEquals("Parameters size", 1, rtInfo.getParameters().size());
		Assert.assertEquals("Parameter", param, rtInfo.getParameters().iterator().next());

		Assert.assertEquals("New design", "design2", rtDAO.getReportTemplateDesign("Test1"));

		// change name
		rtInfo.setReportTemplateName("Test2");
		rtDAO.updateReportTemplate(rtInfo, "design2");

		rtInfo = rtDAO.getReportTemplateInfo("Test2");
		Assert.assertEquals("Parameters size", 1, rtInfo.getParameters().size());
		Assert.assertEquals("Parameter", param, rtInfo.getParameters().iterator().next());

		Assert.assertEquals("New design", "design2", rtDAO.getReportTemplateDesign("Test2"));

		try {
			rtDAO.getReportTemplateInfo("Test1");
			Assert.fail("Report Test1 is stored");
		} catch (ObjectNotFoundException e) {
			// normal execution
		}

		rtDAO.removeReportTemplate("Test2");
	}

	@AfterClass
	public static void tearDownAfterClass() throws ReportingDAOException {
		daoFactory.shutdown();
	}
}
