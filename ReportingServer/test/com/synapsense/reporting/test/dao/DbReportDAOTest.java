package com.synapsense.reporting.test.dao;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Collection;
import java.util.Map;

import net.sf.jasperreports.engine.JasperPrint;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.synapsense.reporting.dao.DbFsDAOFactory;
import com.synapsense.reporting.dao.ReportDAO;
import com.synapsense.reporting.dao.ReportTemplateDAO;
import com.synapsense.reporting.dao.ReportingDAOException;
import com.synapsense.reporting.dao.ReportingDAOFactory;
import com.synapsense.reporting.dto.ReportInfo;
import com.synapsense.reporting.dto.ReportTemplateInfo;
import com.synapsense.reporting.dto.ReportTitle;
import com.synapsense.reporting.export.ExportFormat;
import com.synapsense.reporting.model.Report;
import com.synapsense.reporting.model.ReportCompilationException;
import com.synapsense.reporting.model.ReportGenerationException;
import com.synapsense.reporting.model.ReportParameter;
import com.synapsense.reporting.model.ReportTemplate;
import com.synapsense.reporting.model.JSReport;
import com.synapsense.reporting.test.TestUtils;

public class DbReportDAOTest {

	private static ReportingDAOFactory daoFactory;

	@BeforeClass
	public static void setUpBeforeClass() throws ReportingDAOException {
		daoFactory = new DbFsDAOFactory("org.hsqldb.jdbcDriver", "jdbc:hsqldb:file:" + TestUtils.DB_FOLDER
		        + TestUtils.DB_NAME, "./test/resources/testStorage/database/init_db.sql",
		        "./conf/database/query.templates", TestUtils.STORAGE_PATH);

		ReportTemplateDAO rtDao = daoFactory.newReportTemplateDAO();

		ReportTemplateInfo rtInfo = new ReportTemplateInfo("TestReportTemplate");
		rtInfo.addParameter(new ReportParameter("Param", String.class));
		rtDao.createReportTemplate(rtInfo, "TestDesign");
	}

	@AfterClass
	public static void tearDownAfterClass() throws ReportingDAOException {
		ReportTemplateDAO rtDao = daoFactory.newReportTemplateDAO();
		rtDao.removeReportTemplate("TestReportTemplate");
		daoFactory.shutdown();
	}

	@Test
	public void testCreateReportDao() throws ReportingDAOException {
		ReportDAO rDao = daoFactory.newReportDao();
		Assert.assertNotNull("Report DAO is null", rDao);
	}

	@Test
	public void testSaveReportObject() throws ReportingDAOException {

		ReportDAO rDao = daoFactory.newReportDao();

		JasperPrint jPrint = new JasperPrint();

		jPrint.setName("TestReport");

		ReportInfo rInfo = new ReportInfo("TestReportTemplate", "TestReport");
		rInfo.addParameterValue("Param", "Value");

		rDao.saveReport(rInfo, new JSReport(jPrint));

		Assert.assertFalse("Report id", 0 == rInfo.getReportId());

		Collection<ReportTitle> coll = rDao.getGeneratedReports("TestReportTemplate");

		Assert.assertEquals("Reports size", 1, coll.size());

		ReportTitle title = coll.iterator().next();

		rInfo = rDao.getReportInfo(title.getReportId());
		Assert.assertEquals("Report name", "TestReport", rInfo.getReportName());
		Assert.assertEquals("Report template name", "TestReportTemplate", rInfo.getReportTemplateName());

		Map<String, Object> paramVals = rInfo.getParametersValues();
		Assert.assertEquals("Parameters map size", 1, paramVals.size());

		Object value = paramVals.get("Param");
		Assert.assertNotNull("Parameter value not null", value);

		Assert.assertEquals("Parameter value type", String.class, value.getClass());

		Assert.assertEquals("Value", "Value", (String) value);

		Object reportObject = rDao.getReportObject(rInfo.getReportId());
		Assert.assertNotNull("Report Object null", reportObject);
		Assert.assertEquals("Report object type", JSReport.class, reportObject.getClass());
		Assert.assertEquals("Report object name", "TestReport", ((JSReport) reportObject).getJasperPrint().getName());

		rDao.removeReport(rInfo.getReportId());

		coll = rDao.getGeneratedReports("TestReportTemplate");
		Assert.assertEquals("Reports size", 0, coll.size());
	}

	@Test
	public void testExportReport() throws ReportingDAOException, ReportCompilationException, FileNotFoundException,
	        ReportGenerationException {
		ReportTemplateDAO rtDao = daoFactory.newReportTemplateDAO();
		ReportDAO rDao = daoFactory.newReportDao();

		ReportTemplate template = new ReportTemplate(new FileInputStream(TestUtils.EMPTY_REPORT_DESIGN_PATH));

		rtDao.createReportTemplate(new ReportTemplateInfo(template.getName(), template.getParameters()),
		        TestUtils.readTestFile(TestUtils.EMPTY_REPORT_DESIGN_PATH));

		ReportInfo rInfo = new ReportInfo(template.getName(), "TestReport");
		rInfo.addParameterValue("StringParameter", "StringValue");
		rInfo.addParameterValue("IntegerParameter", 0);

		Report report = template.generateReport(rInfo);

		rDao.saveReport(rInfo, report);

		try {
			String relPath = rDao.exportReport(rInfo.getReportId(), ExportFormat.HTML);
			String path = TestUtils.STORAGE_PATH + "/" + relPath;
			Assert.assertNotNull("Exported path not null", relPath);
			Assert.assertTrue("Exported file exists", new File(path).exists());

			rInfo = rDao.getReportInfo(rDao.getGeneratedReports(template.getName()).iterator().next().getReportId());
			Assert.assertEquals(relPath, rInfo.getExportedPath(ExportFormat.HTML));

			rDao.removeReport(rInfo.getReportId());
			rtDao.removeReportTemplate(template.getName());
			Assert.assertFalse("File does not exist", new File(path).exists());
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}
}
