package com.synapsense.reporting.test.reports;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Properties;

import mondrian.rolap.RolapConnectionProperties;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.synapsense.reporting.ReportingException;
import com.synapsense.reporting.datasource.ReportingDataSource;
import com.synapsense.reporting.dto.ReportInfo;
import com.synapsense.reporting.export.ExportFormat;
import com.synapsense.reporting.model.Report;
import com.synapsense.reporting.model.ReportTemplate;
import com.synapsense.reporting.test.TestUtils;

public class ReportsRunTest {

	@Before
	public void setUp() {
		Properties connProps = new Properties();
		connProps.put(RolapConnectionProperties.Provider.toString(), "Mondrian");
		connProps.put(RolapConnectionProperties.Jdbc.toString(), "jdbc:mysql://localhost/synap_olap");
		connProps.put(RolapConnectionProperties.JdbcUser.toString(), "root");
		connProps.put(RolapConnectionProperties.JdbcPassword.toString(), "1");
		connProps.put(RolapConnectionProperties.JdbcDrivers.toString(), "com.mysql.jdbc.Driver");
		connProps.put(RolapConnectionProperties.Catalog.toString(), "file:./conf/MondrianCube.xml");

		ReportingDataSource.getInstance().setConnectionProperties(connProps);
	}

	@Test
	public void supplyReturnReportGeneration() {

		try {
			ReportTemplate supplyReturnTemplate = null;
			try {
				supplyReturnTemplate = new ReportTemplate(new FileInputStream(TestUtils.STORAGE_TEMPLATES
				        + "/CracSupplyReturn.jrxml"));
			} catch (FileNotFoundException e) {
				Assert.fail(e.getMessage());
				return;
			}
			ReportInfo conf = new ReportInfo("SupplyReturnReport");

			Report report = supplyReturnTemplate.generateReport(conf);

			String exportDir = TestUtils.STORAGE_TEMPLATES + "/CracSupplyReturn/";
			new File(exportDir).mkdirs();
			report.export(ExportFormat.PDF, exportDir + "/CracSupplyReturn.pdf");
			report.export(ExportFormat.HTML, exportDir + "/CracSupplyReturn.html");
		} catch (ReportingException e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}

	@Test
	public void rackOverCoolingReportGeneration() {
		try {
			ReportTemplate rackOverCoolingTemplate = null;
			try {
				rackOverCoolingTemplate = new ReportTemplate(new FileInputStream(TestUtils.STORAGE_TEMPLATES
				        + "/RackOverCooling.jrxml"));
			} catch (FileNotFoundException e) {
				Assert.fail(e.getMessage());
				return;
			}
			ReportInfo config = new ReportInfo("RackOverCoolingAll");
			config.addParameterValue("percent", new Double(50));
			config.addParameterValue("StartYear", new Integer(2009));
			config.addParameterValue("StartMonth", new Integer(11));
			config.addParameterValue("StartDay", new Integer(01));
			config.addParameterValue("EndDay", new Integer(01));
			config.addParameterValue("EndMonth", new Integer(12));
			config.addParameterValue("EndYear", new Integer(2009));

			Report report = rackOverCoolingTemplate.generateReport(config);
			String exportDir = TestUtils.STORAGE_TEMPLATES + "/RackOverCooling/";
			new File(exportDir).mkdirs();
			report.export(ExportFormat.PDF, exportDir + "/RackOverCooling.pdf");
			report.export(ExportFormat.HTML, exportDir + "/RackOverCooling.html");
		} catch (ReportingException e) {
			Assert.fail(e.getMessage());
		}
	}
}
