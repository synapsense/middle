Sub std
	repl "ConfigurationTransport"
End Sub
Sub rmi
	repl "RMITransport"
End Sub
Sub repl(transportName)
	rem dm.conf full path is provided by installer
	c=Session.Property("CustomActionData")
	Set o=CreateObject("Scripting.FileSystemObject")
	Set objReadFile = o.OpenTextFile(c, 1)
	strContents = objReadFile.ReadAll
	objReadFile.Close
	newStr=ReplaceStr("<entry key="+chr(34)+"configTransport"+chr(34)+">.*</entry>",strContents,"<entry key="+chr(34)+"configTransport"+chr(34)+">"+transportName+"</entry>")
	Set objWriteFile=o.CreateTextFile(c+".tmp")
	objWriteFile.Write(newStr)
	objWriteFile.Close
	If (o.FileExists(c)) Then
		o.DeleteFile c,true
	End If
	o.MoveFile c+".tmp",c
End Sub
Function ReplaceStr(patrn, str1, replStr)
  Dim regEx
  Set regEx = New RegExp
  regEx.Pattern = patrn
  regEx.IgnoreCase = True
  ReplaceStr = regEx.Replace(str1, replStr)
End Function

