Set oFSO = CreateObject("Scripting.FileSystemObject")
'startDir = "c:\Program Files\SynapSense\MySQL Server 5.5\data\synap"
startDir = Session.Property("DATA_DIR")

file1Exists = oFSO.FileExists(startDir & "\mysql\db.MYI")
file2Exists = oFSO.FileExists(startDir & "\synap\db.opt")
file3Exists = oFSO.FileExists(startDir & "\synap\objects.ibd")
Dim isUpgrade, isUpgradeText
isUpgrade = "0"
isUpgradeText = "Your new data base will be located here:" & vbNewLine & startDir

If file1Exists And file2Exists And file3Exists Then 
	isUpgrade = "1"
	isUpgradeText = "Your existing data base will be upgraded. Database location:" & vbNewLine & startDir
End If

If isUpgrade = "0" Then
'This is not upgrade, check if selected folder is empty
	If oFSO.FolderExists(startDir) Then
		Set objFolder = oFSO.GetFolder(startDir)
		If Not (objFolder.Files.Count = 0 And objFolder.SubFolders.Count = 0) Then
			isUpgrade = "2"
			MsgBox "No existing DB is found but the target data folder is not empty. Please select another folder."
		End If
	End If
End If
'MsgBox isUpgrade
Session.Property("DB_UPGRADE") = isUpgrade
Session.Property("DB_UPGRADE_TEXT") = isUpgradeText
