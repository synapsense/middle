SET WshShell = CreateObject("WScript.Shell")

dbServiceName = "SynapSense DB"
esServiceName = "synapserver"
'service dependency
WshShell.run "sc config " & chr(34) & esServiceName & chr(34) & " depend= "  & chr(34) & dbServiceName & chr(34), true
'services failure actions
services = Array(dbServiceName, esServiceName, "SynapBACnet", "synapdm", "synapli", "synapmbgw", "synapsnmp")
FOR EACH service IN services
	command = "sc failure " & chr(34) & service & chr(34) & " actions= restart/60000/restart/60000/restart/60000 reset= 86400"
	WshShell.Run command, 0, True
NEXT

