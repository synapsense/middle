Building the installer in the development environment.

NOTE: These instructions assume that you have a working SSENV containing the middle, webapps and deploymentlab repositories.

# One time setup of pre-requisutes
Copy \\svrbld4\e$\prerequisites\\<version> somewhere on your development machine. Then create $SS_SRC_MIDDLE/CommonInstaller/build.properties and specify the location, for example:
prerequisites.src.absolute.path=C:\\work\\advanced-installer\\prerequisites\\7.0

# Build middle
_runin $SS_SRC_MIDDLE mvn -D skipTests install

# Build webapps
_runin $SS_SRC_WEBAPPS/webconsole ant build_standalone_war

# Download MapSense from Jenkins (you'd need a local Latex installation to build it yourself)
ss-get-mapsense

# Make all components available to AdvancedInstaller
_runin $SS_SRC_MIDDLE/CommonInstaller ant prepare

At this point you can just build the installer interactively. However AI is licensed only on Jenkins, so you'll have to share your development drive and access it remotely from Jenkins. It's slow (6 minutes to build the installer) but it works.