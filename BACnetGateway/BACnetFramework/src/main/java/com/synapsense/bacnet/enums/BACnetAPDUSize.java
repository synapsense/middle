package com.synapsense.bacnet.enums;

public enum BACnetAPDUSize {

UP_TO_50(50, 0x00), UP_TO_128(128, 0x01), UP_TO_206(206, 0x02), UP_TO_480(480, 0x03), UP_TO_1024(1024, 0x04), UP_TO_1476(
        1476, 0x05);
final private int byteValue;
final private int intValue;

private BACnetAPDUSize(int intValue, int byteValue) {
	this.intValue = intValue;
	this.byteValue = byteValue;
}

public static BACnetAPDUSize valueOfByLong(long intValue) {
	for (BACnetAPDUSize val : BACnetAPDUSize.values()) {
		if (val.intValue == intValue) {
			return val;
		}
	}
	return UP_TO_1476;
}

public static BACnetAPDUSize valueOf(long byteValue) {
	for (BACnetAPDUSize val : BACnetAPDUSize.values()) {
		if (val.byteValue == byteValue) {
			return val;
		}
	}
	return UP_TO_1476;
}

public int getByteValue() {
	return byteValue;
}

public int getIntValue() {
	return intValue;
}

}
