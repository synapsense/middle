package com.synapsense.bacnet.enums;

public enum ErrorClass {

DEVICE(0), OBJECT(1), PROPERTY(2), RESOURCES(3), SECURITY(4), SERVICES(5), VT(6);
private int code;

ErrorClass(int code) {
	this.code = code;
}

public int getCode() {
	return code;
}

public static ErrorClass getErrorClass(int code) {
	for (ErrorClass enumerated : ErrorClass.values()) {
		if (enumerated.getCode() == code) {
			return enumerated;
		}
	}
	return null;
}

public static ErrorClass getErrorClass(long code) {
	return getErrorClass((int) code);
}

}