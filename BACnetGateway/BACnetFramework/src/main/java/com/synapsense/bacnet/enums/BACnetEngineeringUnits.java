package com.synapsense.bacnet.enums;

public class BACnetEngineeringUnits {

	public static final int VOLTS = 5;
	public static final int AMPERES = 3;
	public static final int NO_UNITS = 95;

	// /////////////////////////////
	// Temperature
	// /////////////////////////////
	public static final int DEGREES_FAHRENHEIT = 64;

	// /////////////////////////////
	// Humidity
	// /////////////////////////////
	public static final int PERCENT_RELATIVE_HUMIDITY = 29;

	// /////////////////////////////
	// Pressure
	// /////////////////////////////
	public static final int INCHES_OF_WATER = 58;

	// /////////////////////////////
	// Energy
	// /////////////////////////////
	public static final int KILOWATT_HOURS = 19;

}
