package com.synapsense.bacnet.objects;

import com.synapsense.bacnet.system.shared.BACnetObjectIdentifier;

/**
 * Helper class for unique COV
 * 
 * @author Alexander Kravin
 * 
 */
public class Triple {
	private final long processId;
	private final BACnetObjectIdentifier objectId;
	private final long propertyId;

	public Triple(long processId, BACnetObjectIdentifier objectId, long propertyId) {
		this.processId = processId;
		this.objectId = objectId;
		this.propertyId = propertyId;
	}

	@Override
	public int hashCode() {
		StringBuilder result = new StringBuilder();
		result.append(processId);
		result.append("-");
		result.append(objectId.toString());
		result.append("-");
		result.append(propertyId);
		return result.toString().hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		boolean result = false;
		if (obj instanceof Triple) {
			if (obj == this) {
				result = true;
			} else {
				Triple temp = (Triple) obj;
				if (temp.objectId.equals(objectId) && temp.processId == processId && temp.propertyId == propertyId) {
					result = true;
				}
			}
		}
		return result;
	}

	public String toString() {
		return "{processId=" + processId + "; objectId=" + objectId + "; propertyId=" + propertyId + "}";
	}
}
