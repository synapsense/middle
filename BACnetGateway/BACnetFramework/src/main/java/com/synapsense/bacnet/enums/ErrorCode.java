package com.synapsense.bacnet.enums;

public enum ErrorCode {
OTHER(0), INCONSISTENT_PARAMETERS(7), MISSING_REQUIRED_PARAMETER(16), READ_ACCESS_DENIED(27), UNKNOWN_OBJECT(31), UNKNOWN_PROPERTY(
        32), WRITE_ACCESS_DENIED(40), INVALID_ARRAY_INDEX(42), COV_SUBSCRIPTION_FAILED(43), NOT_COV_PROPERTY(44), PROPERTY_IS_NOT_AN_ARRAY(
        50);
private int code;

ErrorCode(int code) {
	this.code = code;
}

public int getCode() {
	return code;
}

public static ErrorCode getErrorCode(int code) {
	for (ErrorCode enumerated : ErrorCode.values()) {
		if (enumerated.getCode() == code) {
			return enumerated;
		}
	}
	return null;
}

public static ErrorCode getErrorCode(long code) {
	return getErrorCode((int) code);
}
}
