package com.synapsense.bacnet.system;

/**
 * Holder for BACnetPropertyIdentifier enumerated constants
 * 
 * @author Oleg Stepanov
 * 
 */
public class BACnetPropertyId {
	public static final long ALL = 8;
	public static final long APDU_TIMEOUT = 11;
	public static final long APPLICATION_SOFTWARE_VERSION = 12;
	public static final long COV_INCREMENT = 22;
	public static final long DAYLIGHT_SAVING_STATUS = 24;
	public static final long DESCRIPTION = 28;
	public static final long DEVICE_ADDRESS_BINDING = 30;
	public static final long DEVICE_TYPE = 31;
	public static final long EVENT_STATE = 36;
	public static final long FIRMWARE_REVISION = 44;
	public static final long LIST_OF_SESSION_KEYS = 55;
	public static final long LOCAL_DATE = 56;
	public static final long LOCAL_TIME = 57;
	public static final long LOCATION = 58;
	public static final long MAX_APDU_LENGTH_ACCEPTED = 62;
	public static final long MAX_INFO_FRAMES = 63;
	public static final long MAX_MASTER = 64;
	public static final long MAX_PRES_VALUE = 65;
	public static final long MIN_PRES_VALUE = 69;
	public static final long MODEL_NAME = 70;
	public static final long NUMBER_OF_APDU_RETRIES = 73;
	public static final long OBJECT_IDENTIFIER = 75;
	public static final long OBJECT_LIST = 76;
	public static final long OBJECT_NAME = 77;
	public static final long OBJECT_TYPE = 79;
	public static final long OPTIONAL = 80;
	public static final long OUT_OF_SERVICE = 81;
	public static final long PRESENT_VALUE = 85;
	public static final long PRIORITY_ARRAY = 87;
	public static final long PROTOCOL_OBJECT_TYPES_SUPPORTED = 96;
	public static final long PROTOCOL_SERVICES_SUPPORTED = 97;
	public static final long PROTOCOL_VERSION = 98;
	public static final long RELIABILITY = 103;
	public static final long RELINQUISH_DEFAULT = 104;
	public static final long REQUIRED = 105;
	public static final long RESOLUTION = 106;

	public static final long SEGMENTATION_SUPPORTED = 107;
	public static final long STATUS_FLAGS = 111;
	public static final long SYSTEM_STATUS = 112;
	public static final long UNITS = 117;
	public static final long UPDATE_INTERVAL = 118;
	public static final long UTC_OFFSET = 119;
	public static final long VENDOR_ID = 120;
	public static final long VENDOR_NAME = 121;
	public static final long PROTOCOL_REVISION = 139;
	public static final long ACTIVE_COV_SUBSCRIPTIONS = 152;
	public static final long DATABASE_REVISION = 155;
	public static final long PROFILE_NAME = 168;

	public static final long HOST = 513;
	public static final long PORT = 514;
	public static final long STATUS = 515;
	public static final long CHANNEL = 516;
	public static final long DEFAULT_SAMPLE_INTERVAL = 517;
	public static final long ALERTS_COUNT = 518;

	public static final long LOCATION_X = 519;
	public static final long LOCATION_Y = 520;

	public static final long CONDITIONALERT = 528;

	public static final long NETWORK_ID = 529;
	public static final long NODE_ID = 530;
	public static final long SENSOR_ID = 531;

	public static final long LOGICAL_ID = 532;
	public static final long PHYSICAL_ID = 533;

	public static final long PLATFORM_NAME = 534;
	public static final long APP_VERSION = 535;
	public static final long OS_VERSION = 536;
	public static final long SENSORS_COUNT = 537;

	public static final long RECOMMENDED_MIN = 538;
	public static final long RECOMMENDED_MAX = 539;
	public static final long ALLOWABLE_MIN = 540;
	public static final long ALLOWABLE_MAX = 541;

	public static final long ELAPSED_TIME = 542;

	public static final long LAST_VALUE_TIMESTAMP = 543;
}
