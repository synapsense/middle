package com.synapsense.bacnet.enums;

public class BACnetDeviceStatus {
	public static final int OPERATIONAL = 0;
	public static final int OPERATIONAL_READ_ONLY = 1;
	public static final int DOWNLOAD_REQUIRED = 2;
	public static final int DOWNLOAD_IN_PROGRESS = 3;
	public static final int NON_OPERATIONAL = 4;
	public static final int BACKUP_IN_PROGRESS = 5;
}
