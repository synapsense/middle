package com.synapsense.bacnet.objects;

import java.util.Collection;

import com.synapsense.bacnet.system.BACnetPropertyId;
import com.synapsense.bacnet.system.BACnetPropertyType;
import com.synapsense.bacnet.system.shared.BACnetAddressBinding;
import com.synapsense.bacnet.system.shared.BACnetCOVSubscription;
import com.synapsense.bacnet.system.shared.BACnetObjectIdentifier;
import com.synapsense.bacnet.system.shared.BACnetService;

/**
 * Base interface with required BACnet Device object methods.
 * 
 * @author Oleg Stepanov
 * 
 */
// TODO mark with BACnetProperty
public interface BACnetDevice extends BACnetObject {

	/**
	 * System Status.
	 * 
	 * @return one of BACnetDeviceStatus constants.
	 */
	@BACnetProperty(id = BACnetPropertyId.LOCATION, codec = "CharacterStringCodec")
	String getLocation();

	@BACnetProperty(id = BACnetPropertyId.DESCRIPTION, codec = "CharacterStringCodec")
	String getDescription();

	@BACnetProperty(id = BACnetPropertyId.ACTIVE_COV_SUBSCRIPTIONS, codec = "ActiveCOVSubscriptionsCodec")
	BACnetCOVSubscription[] getActiveCOVSubscriptionsList();

	@BACnetProperty(id = BACnetPropertyId.PROFILE_NAME, codec = "CharacterStringCodec")
	String getProfileName();

	@BACnetProperty(id = BACnetPropertyId.SYSTEM_STATUS, type = BACnetPropertyType.REQUIRED, codec = "EnumCodec", cov = true)
	int getSystemStatus();

	@BACnetProperty(id = BACnetPropertyId.VENDOR_NAME, type = BACnetPropertyType.REQUIRED, codec = "CharacterStringCodec", cov = true)
	String getVendorName();

	@BACnetProperty(id = BACnetPropertyId.VENDOR_ID, type = BACnetPropertyType.REQUIRED, codec = "UIntCodec")
	long getVendorIdentifier();

	@BACnetProperty(id = BACnetPropertyId.MODEL_NAME, type = BACnetPropertyType.REQUIRED, codec = "CharacterStringCodec")
	String getModelName();

	@BACnetProperty(id = BACnetPropertyId.FIRMWARE_REVISION, type = BACnetPropertyType.REQUIRED, codec = "CharacterStringCodec")
	String getFirmwareRevision();

	@BACnetProperty(id = BACnetPropertyId.APPLICATION_SOFTWARE_VERSION, type = BACnetPropertyType.REQUIRED, codec = "CharacterStringCodec")
	String getApplicationSoftwareVersion();

	@BACnetProperty(id = BACnetPropertyId.PROTOCOL_VERSION, type = BACnetPropertyType.REQUIRED, codec = "UIntCodec")
	long getProtocolVersion();

	@BACnetProperty(id = BACnetPropertyId.PROTOCOL_REVISION, type = BACnetPropertyType.REQUIRED, codec = "UIntCodec")
	long getProtocolRevision();

	/**
	 * Returns a list of all services supported by this device.
	 * 
	 * @return array of BACnetService constants
	 */
	@BACnetProperty(id = BACnetPropertyId.PROTOCOL_SERVICES_SUPPORTED, type = BACnetPropertyType.REQUIRED, codec = "ProtocolServicesSupportedCodec")
	BACnetService[] getProtocolServicesSupported();

	/**
	 * Returns a list of all object types supported by this device.
	 * 
	 * @return array of BACnetObjectType constants
	 */
	@BACnetProperty(id = BACnetPropertyId.PROTOCOL_OBJECT_TYPES_SUPPORTED, type = BACnetPropertyType.REQUIRED, codec = "ProtocolObjectTypesSupportedCodec")
	short[] getProtocolObjectTypesSupported();

	@BACnetProperty(id = BACnetPropertyId.OBJECT_LIST, type = BACnetPropertyType.REQUIRED, codec = "ObjectListCodec")
	BACnetObjectIdentifier[] getObjectList();

	@BACnetProperty(id = BACnetPropertyId.DEVICE_ADDRESS_BINDING, type = BACnetPropertyType.REQUIRED, codec = "DeviceAddressBindingCodec")
	BACnetAddressBinding[] getAddressBindings();

	@BACnetProperty(id = BACnetPropertyId.MAX_APDU_LENGTH_ACCEPTED, type = BACnetPropertyType.REQUIRED, codec = "UIntCodec")
	long getMaxAPDULengthAccepted();

	@BACnetProperty(id = BACnetPropertyId.SEGMENTATION_SUPPORTED, type = BACnetPropertyType.REQUIRED, codec = "EnumCodec")
	int getSegmentationSupported();

	@BACnetProperty(id = BACnetPropertyId.APDU_TIMEOUT, type = BACnetPropertyType.REQUIRED, codec = "UIntCodec")
	long getAPDUTimeout();

	@BACnetProperty(id = BACnetPropertyId.NUMBER_OF_APDU_RETRIES, type = BACnetPropertyType.REQUIRED, codec = "UIntCodec")
	long getNumberOfAPDURetries();

	@BACnetProperty(id = BACnetPropertyId.DATABASE_REVISION, type = BACnetPropertyType.REQUIRED, codec = "UIntCodec")
	long getDatabaseRevision();

	// -----
	public Collection<BACnetAddressBinding> getDeviceAddressBindings();

	public Collection<BACnetObject> getObjects();

}
