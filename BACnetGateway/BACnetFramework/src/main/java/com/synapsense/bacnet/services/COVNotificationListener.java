package com.synapsense.bacnet.services;

import com.synapsense.bacnet.system.BACnetPropertyValue;
import com.synapsense.bacnet.system.shared.BACnetObjectIdentifier;

public interface COVNotificationListener {

	void valueChanged(long subscrProcessId, BACnetObjectIdentifier initiatingDeviceId,
	        BACnetObjectIdentifier monitoredObjectId, long timeRemaining, BACnetPropertyValue[] listOfValues);

}
