package com.synapsense.bacnet.enums;

public enum BACnetSegmentation {
SEGMENTED_BOTH(0), SEGMENTED_TRANSMIT(1), SEGMENTED_RECEIVE(2), NO_SEGMENTATION(3);

public final int code;

BACnetSegmentation(int code) {
	this.code = code;
}

}
