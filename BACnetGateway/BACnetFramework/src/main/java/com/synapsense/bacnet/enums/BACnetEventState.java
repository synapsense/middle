/**
 * 
 */
package com.synapsense.bacnet.enums;

public class BACnetEventState {
	public static final int NORMAL = 0;
	public static final int FAULT = 1;
	public static final int OFFNORMAL = 2;
	public static final int HIGH_LIMIT = 3;
	public static final int LOW_LIMIT = 4;
	public static final int LIFE_SAFETY_ALARM = 5;
}
