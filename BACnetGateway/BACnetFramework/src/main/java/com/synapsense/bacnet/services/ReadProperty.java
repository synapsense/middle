package com.synapsense.bacnet.services;

import com.synapsense.bacnet.system.BACnetError;
import com.synapsense.bacnet.system.shared.BACnetObjectIdentifier;

/**
 * Interface of BACnet ReadProperty service.
 * 
 * @author Oleg Stepanov
 * 
 */
public interface ReadProperty {

	/**
	 * Allows to read a value of given property.
	 * 
	 * @param objId
	 *            id of object which property is requested.
	 * @param propId
	 *            id of the property.
	 * @param arrayIndex
	 *            index for indexed properties (optional, null for not
	 *            specified).
	 * @return Packed value of the property.
	 * @throws BACnetError
	 *             if request is failed.
	 */
	byte[] read(BACnetObjectIdentifier objId, long propId, Long arrayIndex) throws BACnetError;

	/**
	 * ReadProperty service result.
	 * 
	 * @author Oleg Stepanov
	 * 
	 */
	/*
	 * public static class Result { public BACnetObjectIdentifier objId; public
	 * long propId; public Long arrayIndex; public Object value; }
	 */
}
