package com.synapsense.bacnet.objects;

import java.beans.IntrospectionException;
import java.util.HashMap;
import java.util.Map;

import com.synapsense.bacnet.enums.ErrorClass;
import com.synapsense.bacnet.enums.ErrorCode;
import com.synapsense.bacnet.services.COVNotificationListener;
import com.synapsense.bacnet.services.ReadProperty;
import com.synapsense.bacnet.services.SubscribeCOVProperty;
import com.synapsense.bacnet.services.WriteProperty;
import com.synapsense.bacnet.system.BACnetError;
import com.synapsense.bacnet.system.COVNotificationTarget;
import com.synapsense.bacnet.system.Device;
import com.synapsense.bacnet.system.shared.BACnetObjectIdentifier;

/**
 * Class that implements Device interface over any user object implementing
 * BACnetDevice interface. Services are implemented as follows: - ReadProperty,
 * WriteProperty and SubscribeCOVP services are implemented by
 * BACnetDeviceWrapper (basing on annotations) - Other service interfaces should
 * be implemented by the user's object. These interfaces are discovered using
 * reflection; all external requests are delegated to them.
 * 
 * @author Oleg Stepanov
 * 
 */
public class BACnetDeviceWrapper implements Device {

	private BACnetObjectWrapper deviceWrapper;
	private Map<BACnetObjectIdentifier, BACnetObjectWrapper> objectWrappers;
	private BACnetDevice wrappee;

	private ReadPropertyService rpService;
	private WritePropertyService wpService;
	private SubscribeCOVPropertyService covService;

	public BACnetDeviceWrapper(BACnetDevice deviceObject) throws IntrospectionException, InvalidPropertyException {
		if (deviceObject == null)
			throw new IllegalArgumentException("Cannot wrapp null deviceObject");

		wrappee = deviceObject;
		deviceWrapper = new BACnetObjectWrapper(wrappee);
		objectWrappers = new HashMap<BACnetObjectIdentifier, BACnetObjectWrapper>();
		rpService = this.new ReadPropertyService();
		wpService = this.new WritePropertyService();
		covService = this.new SubscribeCOVPropertyService();
	}

	public void addBACnetObject(BACnetObject object) throws IntrospectionException, InvalidPropertyException {
		objectWrappers.put(object.getObjectIdentifier(), new BACnetObjectWrapper(object));
	}

	public void removeBACnetObject(BACnetObject object) {
		objectWrappers.remove(object.getObjectIdentifier());
	}

	@Override
	public ReadProperty getReadPropertyService() {
		return rpService;
	}

	@Override
	public WriteProperty getWritePropertyService() {
		return wpService;
	}

	@Override
	public SubscribeCOVProperty getSubscribeCOVPropertyService() {
		return covService;
	}

	@Override
	public Map<Long, BACnetPropertyWrapper> getObjectProperties(BACnetObjectIdentifier objId) throws BACnetError {
		return getWrapper(objId).getPropertyBindings();
	}

	private BACnetObjectWrapper getWrapper(BACnetObjectIdentifier objId) throws BACnetError {
		if (objId.isDeviceId() || objId.equals(deviceWrapper.getWrappee().getObjectIdentifier()))
			return deviceWrapper;
		BACnetObjectWrapper wrapper = objectWrappers.get(objId);
		if (wrapper != null)
			return wrapper;
		throw new BACnetError(ErrorClass.DEVICE, ErrorCode.UNKNOWN_OBJECT);
	}

	public BACnetObjectWrapper getDeviceObjectWrapper() {
		return deviceWrapper;
	}

	public BACnetDevice getWrappee() {
		return wrappee;
	}

	private class ReadPropertyService implements ReadProperty {
		@Override
		public byte[] read(BACnetObjectIdentifier objId, long propId, Long arrayIndex) throws BACnetError {
			return arrayIndex == null ? BACnetDeviceWrapper.this.getWrapper(objId).getProperty(propId).getPackedValue()
			        : BACnetDeviceWrapper.this.getWrapper(objId).getProperty(propId).getPackedValue(arrayIndex);
		}

	}

	private class WritePropertyService implements WriteProperty {
		@Override
		public void write(BACnetObjectIdentifier objId, long propId, Long arrayIndex, byte[] value, byte priority)
		        throws BACnetError {
			if (arrayIndex == null) {
				BACnetDeviceWrapper.this.getWrapper(objId).getProperty(propId).setPackedValue(value);
			} else {
				BACnetDeviceWrapper.this.getWrapper(objId).getProperty(propId).setPackedValue(arrayIndex, value);
			}
		}

	}

	private class SubscribeCOVPropertyService implements SubscribeCOVProperty {
		@Override
		public void subscribe(long subscrProcessId, BACnetObjectIdentifier monitoredObject,
		        COVNotificationListener listener, Long lifeTime, long propId, Float covIncrement) throws BACnetError {

			// find the requested object and property:
			BACnetObjectWrapper objWrapper = getWrapper(monitoredObject);
			BACnetPropertyWrapper propWrapper = objWrapper.getProperty(propId);

			if (lifeTime == null || lifeTime <= 0) {// cancellation request
				propWrapper.cancelSubscription(new Triple(subscrProcessId, monitoredObject, propId));
				return;
			}

			// add subscriber in the list
			propWrapper.subscribe(new COVNotificationTarget(listener, subscrProcessId, lifeTime, covIncrement),
			        BACnetDeviceWrapper.this, objWrapper);
		}
	}
}