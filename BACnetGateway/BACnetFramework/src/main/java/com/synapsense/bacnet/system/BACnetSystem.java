package com.synapsense.bacnet.system;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

import com.synapsense.bacnet.objects.BACnetDevice;
import com.synapsense.bacnet.system.shared.BACnetAddress;

/**
 * Class that represents the whole BACnet System (e.g. networks and devices)
 * reachable from this machine. Local virtual devices are registered through
 * this class as well. Class is a singleton.
 * 
 * @author Oleg Stepanov
 * 
 */
final public class BACnetSystem {

	private static final AtomicReference<BACnetSystem> instance = new AtomicReference<BACnetSystem>();

	public static BACnetSystem getInstance() {
		if (instance.get() == null)
			instance.compareAndSet(null, new BACnetSystem());
		return instance.get();
	}

	private Map<Integer, BACnetNetwork> networks;
	private Collection<BACnetNetwork> roNetworks;

	private BACnetSystem() {
		networks = new HashMap<Integer, BACnetNetwork>();
		roNetworks = Collections.unmodifiableCollection(networks.values());
	}

	public Collection<BACnetNetwork> getNetworks() {
		return roNetworks;
	}

	public BACnetNetwork getNetwork(int netId) {
		return networks.get(netId);
	}

	/**
	 * Creates new network. If network with such id exists it will be returned
	 * instead.
	 * 
	 * @param netId
	 *            id of network to be created.
	 * @return new or existing network.
	 */
	public BACnetNetwork createNetwork(int netId) {
		BACnetNetwork net = networks.get(netId);
		if (net == null) {
			net = new BACnetNetwork(netId);
			networks.put(netId, net);
		}
		return net;
	}

	public void removeNetwork(int netId) {
		networks.remove(netId);
	}

	public void addRoute(int netId1, int netId2, BACnetDevice router) {

	}

	public void setVirtualNet(int netId, BACnetDevice router) {

	}

	/**
	 * Performs device lookup
	 * 
	 * @param address
	 *            device address
	 * @return device with specified address or null.
	 */
	public Device getDevice(BACnetAddress address) {
		BACnetNetwork net = getNetwork(address.networkNumber);
		try {
			return net == null ? null : net.getDevice(address.macAddress);
		} catch (IndexOutOfBoundsException e) {
			// device is not found
			return null;
		}
	}

}
