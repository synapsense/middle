/**
 * 
 */
package com.synapsense.bacnet.enums;

/**
 * @author Alexander Kravin
 * 
 */
public class BACnetReliability {
	public static final long NO_FAULT_DETECTED = 0;
	public static final long NO_SENSOR = 1;
	public static final long OVER_RANGE = 2;
	public static final long UNDER_RANGE = 3;
	public static final long OPEN_LOOP = 4;
	public static final long SHORTED_LOOP = 5;
	public static final long NO_OUTPUT = 6;
	public static final long UNRELIABLE_OTHER = 7;
	public static final long PROCESS_ERROR = 8;
	public static final long MULTI_STATE_FAULT = 9;
	public static final long CONFIGURATION_ERROR = 10;
}
