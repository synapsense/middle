/**
 * 
 */
package com.synapsense.bacnet.system;

/**
 * @author Alexander Kravin
 * 
 */
public class BACnetPropertyType {
	public static final int OPTIONAL = 0;
	public static final int REQUIRED = 1;
}
