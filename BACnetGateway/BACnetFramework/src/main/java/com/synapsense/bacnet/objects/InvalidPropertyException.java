package com.synapsense.bacnet.objects;

/**
 * Exception for operations with BACnet objects properties.
 * 
 * @author Oleg Stepanov
 * 
 */
public class InvalidPropertyException extends Exception {

	private static final long serialVersionUID = 5566331489737282713L;

	public InvalidPropertyException() {
	}

	public InvalidPropertyException(String message) {
		super(message);
	}

	public InvalidPropertyException(Throwable cause) {
		super(cause);
	}

	public InvalidPropertyException(String message, Throwable cause) {
		super(message, cause);
	}

}
