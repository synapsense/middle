package com.synapsense.bacnet.services;

import com.synapsense.bacnet.system.BACnetError;
import com.synapsense.bacnet.system.shared.BACnetObjectIdentifier;

/**
 * Interface of BACnet SubscribeCOVProperty service
 * 
 * @author Oleg Stepanov
 * 
 */
public interface SubscribeCOVProperty {

	/**
	 * Allows to subscribe for value changes of a given property. Both listener
	 * and lifeTime set to null means cancellation request for given subscriber
	 * process Id.<br>
	 * If listner!=null then lifeTime must be present and more than zero.
	 * 
	 * @param subscrProcessId
	 *            subscriber process identifier.
	 * @param monitoredObject
	 *            id of object that contains property to be monitored.
	 * @param listener
	 *            COV callback listener.
	 * @param lifeTime
	 *            number of seconds that may elapse before the subscription can
	 *            be canceled.
	 * @param propID
	 *            id of the property to be monitored.
	 * @param covIncrement
	 *            if property is of type float then notification is sent only
	 *            when change is more than covIncrement
	 * @throws BACnetError
	 *             in case of any errors.
	 */
	void subscribe(long subscrProcessId, BACnetObjectIdentifier monitoredObject, COVNotificationListener listener,
	        Long lifeTime, long propID, Float covIncrement) throws BACnetError;

	/*
	 * void subscribe(long subscrProcessId, BACnetObjectIdentifier
	 * monitoredObject, Boolean confirmed, Long lifeTime, long propID) throws
	 * BACnetError;
	 */
}
