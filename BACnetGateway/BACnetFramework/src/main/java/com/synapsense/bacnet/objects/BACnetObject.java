package com.synapsense.bacnet.objects;

import com.synapsense.bacnet.system.BACnetPropertyId;
import com.synapsense.bacnet.system.BACnetPropertyType;
import com.synapsense.bacnet.system.shared.BACnetObjectIdentifier;

/**
 * Base interface for all BACnet objects.
 * 
 * @author Oleg Stepanov
 * 
 */
public interface BACnetObject {

	/**
	 * Object_Identifier of this object.
	 */
	@BACnetProperty(id = BACnetPropertyId.OBJECT_IDENTIFIER, type = BACnetPropertyType.REQUIRED, codec = "ObjectIDCodec")
	BACnetObjectIdentifier getObjectIdentifier();

	/**
	 * Object_Identifier is not required to be writable by BACnet standard but
	 * the BACnet Framework should be able to assign it since the instance
	 * number is generated. However, if you need to expose this object as
	 * writable to the BACnet devices, simply apply <code>@BACnetProperty</code>
	 * to the implementing method.
	 * 
	 * @param id
	 *            new BACnetObjectIdentifier for this object.
	 */
	void setObjectIdentifier(BACnetObjectIdentifier id);

	/**
	 * Object_Name of this object.
	 */
	@BACnetProperty(id = BACnetPropertyId.OBJECT_NAME, type = BACnetPropertyType.REQUIRED, codec = "CharacterStringCodec", cov = true)
	String getObjectName();

	/**
	 * Object_Type of this object.
	 */
	@BACnetProperty(id = BACnetPropertyId.OBJECT_TYPE, type = BACnetPropertyType.REQUIRED, codec = "EnumCodec")
	short getObjectType();
}
