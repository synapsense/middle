package com.synapsense.bacnet.system;

/**
 * Holder for BACnetPropertyValue data
 * 
 * @author Oleg Stepanov
 * 
 */
public class BACnetPropertyValue {
	public final long propId; // BACnetPropertyId
	public final Long propertyArrayIndex; // used only with array datatypes, if
										  // omitted with an array the entire
										  // array is referenced
	public final byte[] value;// packed value
	public final Short priority;// Unsigned (1..16) OPTIONAL -- used only when
								// property is commandable

	/**
	 * Constructor for convenience.
	 * 
	 * @param propId
	 * @param value
	 */
	public BACnetPropertyValue(long propId, byte[] value) {
		this.propId = propId;
		this.value = value;
		this.propertyArrayIndex = null;
		this.priority = null;
	}

	public BACnetPropertyValue(long propId, byte[] value, Long arrayIndex) {
		this.propId = propId;
		this.value = value;
		this.propertyArrayIndex = arrayIndex;
		this.priority = null;
	}
}
