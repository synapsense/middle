package com.synapsense.bacnet.objects;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Modifier;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.synapsense.bacnet.enums.ErrorClass;
import com.synapsense.bacnet.enums.ErrorCode;
import com.synapsense.bacnet.system.BACnetError;

/**
 * Helper class that simplifies interactions with BACnetObject implementations
 * from BACnet Device side. Class implements ReadProperty, WriteProperty and
 * SubscribeCOVP services using BACnetProperty annotations. Any other services
 * must be implemented explicitly by the object class.
 * 
 * @author Oleg Stepanov
 * 
 */
public class BACnetObjectWrapper {

	private static final Logger log = Logger.getLogger(BACnetObjectWrapper.class);

	private final BACnetObject wrappee;

	private Map<Long, BACnetPropertyWrapper> bindings;
	// keep the second map for name look-up
	private Map<String, BACnetPropertyWrapper> nameBindings;

	/**
	 * Constructor. Wraps any JavaBean class inherited from BACnetObject and
	 * marked with BACnetProperties.<br/>
	 * <b>Note:</b> BACnetProperty annotations redefined on subclasses or
	 * sub-interfaces replace parent's annotations.
	 * 
	 * @param object
	 *            BACnetObject to be wrapped by this instance.
	 * @throws InvalidPropertyException
	 *             if any of object's properties has inconsistent BACnetProperty
	 *             annotations.
	 */
	public BACnetObjectWrapper(BACnetObject object) throws IntrospectionException, InvalidPropertyException {
		if (object == null)
			throw new IllegalArgumentException("Cannot wrap null reference");
		wrappee = object;
		bindings = new HashMap<Long, BACnetPropertyWrapper>();
		if (log.isDebugEnabled())
			log.debug("====== Gathering bindings for " + object.getClass().getName() + " ======");
		processType(object.getClass(), bindings);
		if (log.isDebugEnabled())
			log.debug("====== Gathered bindings for " + object.getClass().getName() + " ======");

		nameBindings = new HashMap<String, BACnetPropertyWrapper>();
		for (BACnetPropertyWrapper wrapper : bindings.values()) {
			nameBindings.put(wrapper.getName(), wrapper);
		}
	}

	/**
	 * Collects <code>BACnetProperty</code> properties for this class and all
	 * its super-classes.
	 * 
	 * @param type
	 *            Class to collect bindings for.
	 * @param bindings
	 *            Map of the bindings.
	 * @throws InvalidPropertyException
	 *             if any of object's properties has inconsistent BACnetProperty
	 *             annotations.
	 */
	private void processType(Class<?> type, Map<Long, BACnetPropertyWrapper> bindings) throws IntrospectionException,
	        InvalidPropertyException {
		// first, call recursively on the super-class
		Class<?> superType = type.getSuperclass();
		if (superType != null && superType != Object.class)
			processType(superType, bindings);
		// process type's interfaces:
		processTypeInterfaces(type, bindings);
		// process properties of the type itself
		if (log.isDebugEnabled())
			log.debug("processing class " + type.getName());
		if (superType != null)
			addPropertyBindings(bindings, Introspector.getBeanInfo(type, superType));
		else
			addPropertyBindings(bindings, Introspector.getBeanInfo(type));
	}

	private void processTypeInterfaces(Class<?> type, Map<Long, BACnetPropertyWrapper> bindings)
	        throws IntrospectionException, InvalidPropertyException {
		// process base interfaces recursively
		for (Class<?> iface : type.getInterfaces()) {
			processTypeInterfaces(iface, bindings);
		}
		// if this type is an interface process it as well
		if (Modifier.isInterface(type.getModifiers())) {
			if (log.isDebugEnabled())
				log.debug("processing interface " + type.getName());
			addPropertyBindings(bindings, Introspector.getBeanInfo(type));
		}
	}

	/**
	 * Adds BACnetProperties to the binding map.
	 * 
	 * @param properties
	 *            the binding map to be modified.
	 * @throws InvalidPropertyException
	 */
	private void addPropertyBindings(Map<Long, BACnetPropertyWrapper> properties, BeanInfo beanInfo)
	        throws InvalidPropertyException {
		// find all properties with BACnetProperty attribute
		for (PropertyDescriptor pdescr : beanInfo.getPropertyDescriptors()) {
			// check if the property is BACnetProperty
			if (!BACnetPropertyWrapper.isBACnetProperty(pdescr))
				continue;// not a BACnetProperty at all

			// create new wrapper
			BACnetPropertyWrapper wrapper = new BACnetPropertyWrapper(pdescr, this);

			// try to find existing property binding
			BACnetPropertyWrapper existingWrapper = properties.get(wrapper.getId());
			if (existingWrapper == null) {
				// add new binding
				properties.put(wrapper.getId(), wrapper);
			} else {
				log.debug("Merging properties " + existingWrapper.getName() + " and " + wrapper.getName());
				existingWrapper.mergeProperty(wrapper);
			}
		}
	}

	public BACnetObject getWrappee() {
		return wrappee;
	}

	public Map<Long, BACnetPropertyWrapper> getPropertyBindings() {
		return Collections.unmodifiableMap(bindings);
	}

	public BACnetPropertyWrapper getProperty(long propId) throws BACnetError {
		BACnetPropertyWrapper pInfo = bindings.get(propId);
		if (pInfo == null) {
			throw new BACnetError(ErrorClass.OBJECT, ErrorCode.UNKNOWN_PROPERTY);
		}
		return pInfo;
	}

}