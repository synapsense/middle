package com.synapsense.bacnet.system;

import com.synapsense.bacnet.services.COVNotificationListener;
import com.synapsense.bacnet.system.shared.BACnetObjectIdentifier;

/**
 * Class incapsulates details of COV notification target
 * 
 * @author Oleg Stepanov
 * 
 */
public class COVNotificationTarget {
	public final long subscriberProcessId;
	public final long expiresAt;// expiration time in seconds
	public final Float covIncremet;
	private final COVNotificationListener subscriber;

	public COVNotificationTarget(COVNotificationListener subscriber, long subscriberProcessId, long duration,
	        Float covIncremet) {
		if (subscriber == null)
			throw new IllegalArgumentException("subscriber cannot be null");
		this.subscriber = subscriber;
		this.subscriberProcessId = subscriberProcessId;
		this.expiresAt = System.currentTimeMillis() / 1000 + duration;
		this.covIncremet = covIncremet;
	}

	public void valueChanged(BACnetObjectIdentifier deviceId, BACnetObjectIdentifier objectId,
	        BACnetPropertyValue[] listOfValues) {
		subscriber.valueChanged(subscriberProcessId, deviceId, objectId, getSecondsRemain(), listOfValues);
	}

	public long getSecondsRemain() {
		return expiresAt - System.currentTimeMillis() / 1000;
	}

	public boolean isExpired() {
		return getSecondsRemain() <= 0;
	}

}
