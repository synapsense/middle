package com.synapsense.bacnet.objects;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.synapsense.bacnet.system.BACnetPropertyType;

/**
 * Annotation for specifying BACnet objects properties. Annotation can be
 * applied to corresponding getter/setter. At runtime, if no setter is found for
 * a given property id, the property is considered read-only (same logic for
 * getter).
 * 
 * @author Oleg Stepanov
 * 
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.FIELD, ElementType.METHOD })
public @interface BACnetProperty {
	/**
	 * Encoder/decoder class that must be used to (de)serialize property value
	 * from/to BACnet encoding. Class must be available for the current thread
	 * classloader.
	 * 
	 * @return Full codec class name.
	 */
	String codec();

	/**
	 * BACnet property id.
	 * 
	 * @return BACnet property id.
	 */
	long id();

	/**
	 * BACnet property type for ReadPropertyMultiple request
	 * 
	 * @return BACnet property type
	 */
	int type() default BACnetPropertyType.OPTIONAL;

	/**
	 * Marks whether this property generates COV notifications.
	 * 
	 * @return
	 */
	boolean cov() default false;
}
