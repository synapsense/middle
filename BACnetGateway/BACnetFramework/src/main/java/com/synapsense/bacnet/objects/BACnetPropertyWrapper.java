package com.synapsense.bacnet.objects;

import java.beans.EventSetDescriptor;
import java.beans.IndexedPropertyChangeEvent;
import java.beans.IndexedPropertyDescriptor;
import java.beans.IntrospectionException;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyDescriptor;
import java.io.ByteArrayOutputStream;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.synapsense.bacnet.enums.ErrorClass;
import com.synapsense.bacnet.enums.ErrorCode;
import com.synapsense.bacnet.network.pdu.apdu.codecs.codecs.Codec;
import com.synapsense.bacnet.network.pdu.apdu.codecs.codecs.CodecsAF;
import com.synapsense.bacnet.network.pdu.apdu.codecs.codecs.primitive.PrimFactory;
import com.synapsense.bacnet.network.pdu.apdu.codecs.exceptions.NoSuitableCodecException;
import com.synapsense.bacnet.network.pdu.apdu.codecs.primitives.BACnetUInt;
import com.synapsense.bacnet.system.BACnetError;
import com.synapsense.bacnet.system.BACnetPropertyType;
import com.synapsense.bacnet.system.BACnetPropertyValue;
import com.synapsense.bacnet.system.COVNotificationTarget;
import com.synapsense.bacnet.system.shared.BACnetCOVSubscription;
import com.synapsense.bacnet.system.shared.BACnetObjectIdentifier;
import com.synapsense.bacnet.system.shared.BACnetObjectPropertyReference;
import com.synapsense.bacnet.system.shared.BACnetRecipient;
import com.synapsense.bacnet.system.shared.BACnetRecipientProcess;

/**
 * Class for storing BACnet property access descriptors such as read/write
 * methods and decoders/encoders.
 * 
 * @author Oleg Stepanov
 * 
 */
public class BACnetPropertyWrapper {
	private static final Logger log = Logger.getLogger(BACnetPropertyWrapper.class);
	private Long id;

	private BACnetObjectWrapper object;
	private PropertyDescriptor propDescr;
	private IndexedPropertyDescriptor indexedDescr;
	private EventSetDescriptor covDescr;
	private BACnetProperty readInfo;
	private BACnetProperty writeInfo;
	private BACnetProperty readIndexedInfo;
	private BACnetProperty writeIndexedInfo;
	private BACnetProperty typeInfo;
	private BACnetProperty covInfo;
	private final Map<Triple, EventDispatcher> dispatchers;
	private final static CodecsAF codecFactory = new PrimFactory();

	/**
	 * Check whether any of property methods is marked with
	 * <code>BACnetProperty</code> annotation.
	 * 
	 * @param descr
	 *            property descriptor to be checked.
	 * @return true if there is at least one method with
	 *         <code>BACnetProperty</code> annotation.
	 */
	public static boolean isBACnetProperty(PropertyDescriptor descr) {
		// check if there either getter, setter or their indexed parts
		// is marked with BACnetProperty annotation:
		if (descr.getReadMethod() != null && descr.getReadMethod().getAnnotation(BACnetProperty.class) != null)
			return true;
		if (descr.getWriteMethod() != null && descr.getWriteMethod().getAnnotation(BACnetProperty.class) != null)
			return true;
		if (descr instanceof IndexedPropertyDescriptor) {
			IndexedPropertyDescriptor indexedDescr = (IndexedPropertyDescriptor) descr;
			if (indexedDescr.getIndexedReadMethod() != null
			        && indexedDescr.getIndexedReadMethod().getAnnotation(BACnetProperty.class) != null)
				return true;
			if (indexedDescr.getIndexedWriteMethod() != null
			        && indexedDescr.getIndexedWriteMethod().getAnnotation(BACnetProperty.class) != null)
				return true;
		}
		return false;
	}

	/**
	 * Creates new property wrapper.
	 * 
	 * @param descr
	 *            descriptor of the property to be wrapped.
	 * @throws IllegalArgumentException
	 *             if descr is null or
	 *             <code>isBACnetProperty(descr)==false</code>
	 * @throws InvalidPropertyException
	 *             if ids of BACnetProperty annotations for property methods are
	 *             different.
	 */
	public BACnetPropertyWrapper(PropertyDescriptor descr, BACnetObjectWrapper object) throws IllegalArgumentException,
	        InvalidPropertyException {
		if (descr == null)
			throw new IllegalArgumentException("Cannot wrap null property descriptor");
		if (object == null)
			throw new IllegalArgumentException("Cannot wrap property of a null object");
		if (!isBACnetProperty(descr))
			throw new IllegalArgumentException("Property " + descr.getName() + " is not a BACnet property");

		dispatchers = new HashMap<Triple, EventDispatcher>();

		this.object = object;
		// propDescr = descr;
		if (descr instanceof IndexedPropertyDescriptor) {
			indexedDescr = (IndexedPropertyDescriptor) descr;
		} else {
			propDescr = descr;
		}

		// get all annotations
		if (propDescr != null) {
			readInfo = checkAnnotationId(descr.getReadMethod());
			if (readInfo != null && log.isDebugEnabled()) {
				log.debug("property " + descr.getName() + " readable: " + id + ", " + readInfo.codec());
			}
			writeInfo = checkAnnotationId(descr.getWriteMethod());
			if (writeInfo != null && log.isDebugEnabled()) {
				log.debug("property " + descr.getName() + " writeable: " + id + ", " + writeInfo.codec());
			}

			typeInfo = checkAnnotationId(descr.getReadMethod());
			if (typeInfo != null) {
				log.debug("property " + descr.getName() + " type: " + id + ", "
				        + (typeInfo.type() == BACnetPropertyType.OPTIONAL ? "OPTIONAL" : "REQUIRED"));
			}

			// either of the above must be not null
			assert (readInfo != null || writeInfo != null);
		}

		if (indexedDescr != null) {
			readIndexedInfo = checkAnnotationId(indexedDescr.getIndexedReadMethod());
			if (readIndexedInfo != null && log.isDebugEnabled()) {
				log.debug("property " + descr.getName() + " read indexed: " + id + ", " + readIndexedInfo.codec());
			}
			writeIndexedInfo = checkAnnotationId(indexedDescr.getIndexedWriteMethod());
			if (writeIndexedInfo != null && log.isDebugEnabled()) {
				log.debug("property " + descr.getName() + " write indexed: " + id + ", " + writeIndexedInfo.codec());
			}
			assert (readIndexedInfo != null || writeIndexedInfo != null);
		}
		// TODO !!!!! events for indexed properties???
		else {
			try {
				EventSetDescriptor esd = getBoundEventSet(descr);
				if (esd != null) {
					// go get the annotation and see if we shall treat
					// this property as COV-able
					/*
					 * covInfo =
					 * esd.getAddListenerMethod().getAnnotation(BACnetProperty
					 * .class); if(covInfo==null) return;//nothing to do - user
					 * don't want it to be COV //check id: if(covInfo.id()!=id)
					 * throw new
					 * InvalidPropertyException("\""+esd.getAddListenerMethod
					 * ().getName()+
					 * "\" has inconsistent BACnet property ids: "+
					 * id+" and "+covInfo.id());
					 */
					covInfo = checkAnnotationId(esd.getAddListenerMethod());
					if (covInfo == null)
						return;// nothing to do - user don't want it to be COV
					covDescr = esd;// store esd

					if (log.isDebugEnabled()) {
						log.debug("property " + descr.getName() + " cov notify: " + id + ", " + covInfo.codec());
					}
				}
			} catch (IntrospectionException e) {
				throw new InvalidPropertyException("Cannot get EventSetHandler on " + descr.getName(), e);
			}
		}

	}

	private BACnetProperty checkAnnotationId(Method method) throws InvalidPropertyException {
		if (method == null)
			return null;// if there is no method - there is no annotation as
						// well
		BACnetProperty ann = method.getAnnotation(BACnetProperty.class);
		if (ann != null) {
			if (id != null) {
				if (ann.id() != id)
					throw new InvalidPropertyException("\"" + method.getName()
					        + "\" has inconsistent BACnet property ids: " + id + " and " + ann.id());
			} else {
				id = ann.id();
			}
			// check if codec exists
			try {
				codecFactory.getCodec(ann.codec());
			} catch (NoSuitableCodecException e) {
				throw new InvalidPropertyException("codec " + ann.codec() + " is not registered", e);
			}
		}
		return ann;
	}

	/**
	 * Method allows to merge <code>BACnetProperty</code> annotations from
	 * overridden property.
	 * 
	 * @param descr
	 * @throws IllegalArgumentException
	 *             if descr is null or
	 *             <code>isBACnetProperty(descr)==false</code>
	 * @throws InvalidPropertyException
	 *             if ids of BACnetProperty annotations for property methods are
	 *             different or property name is not the same as currently
	 *             wrapped property.
	 */
	public void mergeProperty(PropertyDescriptor descr) throws IllegalArgumentException, InvalidPropertyException {
		// check if property names are the same
		if (!descr.getName().equals(this.propDescr.getName()))
			throw new InvalidPropertyException("Cannot merge " + descr.getName() + " and " + propDescr.getName()
			        + " properties");
		// create temporary wrapper (constructor will check property
		// consistency)
		BACnetPropertyWrapper tmpWrapper = new BACnetPropertyWrapper(descr, object);
		mergeProperty(tmpWrapper);
	}

	/**
	 * Method allows to merge <code>BACnetProperty</code> annotations from
	 * overridden property.
	 * 
	 * @param that
	 *            BACnetProperty wrapper of a property to be merged.
	 * @throws IllegalArgumentException
	 *             if descr is null or
	 *             <code>isBACnetProperty(descr)==false</code>
	 * @throws InvalidPropertyException
	 *             if ids of BACnetProperty annotations for property methods are
	 *             different or property name is not the same as currently
	 *             wrapped property.
	 */
	public void mergeProperty(BACnetPropertyWrapper that) throws IllegalArgumentException, InvalidPropertyException {
		// check if property ids are the same
		if (that.id != this.id)
			throw new InvalidPropertyException("Merged property has id " + that.id + "; must be " + id);
		// ok, everything is fine, merge values:
		this.object = that.object;
		// TODO: now annotations are simply overridden but more complicated
		// logic can be employed.
		if (that.indexedDescr != null)
			this.indexedDescr = that.indexedDescr;
		if (that.covDescr != null)
			this.covDescr = that.covDescr;
		if (that.propDescr != null)
			this.propDescr = that.propDescr;
		if (that.readInfo != null)
			this.readInfo = that.readInfo;
		if (that.writeInfo != null)
			this.writeInfo = that.writeInfo;
		if (that.readIndexedInfo != null)
			this.readIndexedInfo = that.readIndexedInfo;
		if (that.writeIndexedInfo != null)
			this.writeIndexedInfo = that.writeIndexedInfo;
		if (that.covInfo != null)
			this.covInfo = that.covInfo;
	}

	/**
	 * Checks if the property is per-property bound. Property is per-property
	 * bound if it has methods<br>
	 * <code>addPListener(PropertyChangeListener l)</code><br>
	 * <code>removePListener(PropertyChangeListener l)</code><br>
	 * where P is the property name.
	 * 
	 * @param descr
	 *            property descriptor
	 * @return an event set for the specified property or null if the property
	 *         is not bound.
	 * @throws IntrospectionException
	 *             if error occurs during EventSetDescriptor creation.
	 */
	public static EventSetDescriptor getBoundEventSet(PropertyDescriptor descr) throws IntrospectionException {
		// construct event handling method names
		StringBuilder builder = new StringBuilder();
		builder.append("add").append(descr.getName()).append("Listener");
		builder.setCharAt(3, Character.toUpperCase(builder.charAt(3)));
		String addMethodName = builder.toString();
		builder.replace(0, 3, "remove");
		String removeMethodName = builder.toString();
		// try to find methods:
		Method propMethod;
		if ((propMethod = descr.getReadMethod()) == null)
			propMethod = descr.getWriteMethod();
		Class<?> clazz = propMethod.getDeclaringClass();
		try {
			if (clazz.getDeclaredMethod(addMethodName, PropertyChangeListener.class) != null
			        && clazz.getDeclaredMethod(removeMethodName, PropertyChangeListener.class) != null) {
				return new EventSetDescriptor(clazz, descr.getName(), PropertyChangeListener.class,
				        changeListenerMethods, addMethodName, removeMethodName);
			}
			return null;
		} catch (NoSuchMethodException e) {
			return null;
		}
	}

	final static String[] changeListenerMethods = new String[] { "propertyChange" };

	public void subscribe(COVNotificationTarget target, BACnetDeviceWrapper device, BACnetObjectWrapper object)
	        throws BACnetError {
		Triple key = new Triple(target.subscriberProcessId, object.getWrappee().getObjectIdentifier(), id);
		EventDispatcher dispatcher = dispatchers.get(key);
		if (dispatcher != null)
			dispatcher.cancelSubscription();
		this.new EventDispatcher(target, device, object);
	}

	public void cancelSubscription(Triple key) {
		EventDispatcher dispatcher = dispatchers.get(key);
		if (dispatcher != null)
			dispatcher.cancelSubscription();
	}

	private Method getActivCOVSubscriptionMethod(PropertyDescriptor descr, String addOrRemove)
	        throws SecurityException, NoSuchMethodException {
		Method propMethod;
		if ((propMethod = descr.getReadMethod()) == null)
			propMethod = descr.getWriteMethod();
		Class<?> clazz = propMethod.getDeclaringClass();

		StringBuilder methodName = new StringBuilder();
		methodName.append(addOrRemove.toLowerCase());
		methodName.append("ActiveCOVSubscription");
		for (Method m : clazz.getMethods()) {
			log.trace(m.getName() + ":" + m.toString());
		}
		return clazz.getMethod(methodName.toString(), BACnetCOVSubscription.class);
	}

	/**
	 * Helper class for COV notification dispatching.
	 * 
	 * @author Oleg Stepanov
	 * 
	 */
	private class EventDispatcher implements PropertyChangeListener {

		private final COVNotificationTarget target;
		private final BACnetDeviceWrapper device;
		private final BACnetObjectWrapper object;
		private final Triple key;
		private final BACnetCOVSubscription subscriber;

		/**
		 * Constructs dispatcher to send notifications on behalf of a given
		 * device and object.
		 * 
		 * @param target
		 *            notification target.
		 * @param device
		 *            device whose id will be used as originator.
		 * @param object
		 *            object whose id will be used as originator.
		 * @throws BACnetError
		 *             if this property is not COV-enabled or subscription is
		 *             failed
		 */
		public EventDispatcher(COVNotificationTarget target, BACnetDeviceWrapper device, BACnetObjectWrapper object)
		        throws BACnetError {
			// first of all, check whether this property supports cov
			if (BACnetPropertyWrapper.this.covDescr == null)
				throw new BACnetError(ErrorClass.PROPERTY, ErrorCode.NOT_COV_PROPERTY);
			// check arguments
			if (target == null)
				throw new IllegalArgumentException("target cannot be null");
			this.target = target;
			if (device == null)
				throw new IllegalArgumentException("device cannot be null");
			this.device = device;
			if (object == null)
				throw new IllegalArgumentException("object cannot be null");
			this.object = object;
			// subscribe for value changes:
			try {
				BACnetPropertyWrapper.this.covDescr.getAddListenerMethod().invoke(object.getWrappee(), this);
			} catch (Exception e) {
				BACnetPropertyWrapper.log.warn("cannot subscribe", e);
				throw new BACnetError(ErrorClass.SERVICES, ErrorCode.COV_SUBSCRIPTION_FAILED);
			}
			// add dispatcher in the map
			key = new Triple(target.subscriberProcessId, object.getWrappee().getObjectIdentifier(),
			        BACnetPropertyWrapper.this.id);

			BACnetPropertyWrapper.this.dispatchers.put(key, this);

			subscriber = getCOVSubscriber();
			// Invoke the ActiveCOVSubscription method
			try {
				getActivCOVSubscriptionMethod(propDescr, "add").invoke(object.getWrappee(), subscriber);
			} catch (Exception e) {
                log.warn(e.getMessage(), e);
			}
		}

		private BACnetCOVSubscription getCOVSubscriber() {
			BACnetCOVSubscription subscriber = new BACnetCOVSubscription();

			BACnetRecipientProcess recipientProcess = new BACnetRecipientProcess();
			BACnetObjectIdentifier deviceId = device.getDeviceObjectWrapper().getWrappee().getObjectIdentifier();
			recipientProcess.setRecipient(new BACnetRecipient(deviceId));

			// int size = device.getWrappee().getDeviceAddressBindings().size();
			// BACnetAddress address =
			// device.getWrappee().getDeviceAddressBindings().toArray(new
			// BACnetAddressBinding[size])[0].deviceAddress;
			// recipientProcess.setRecipient(new BACnetRecipient(address));
			recipientProcess.processIdentifier = target.subscriberProcessId;

			BACnetObjectPropertyReference propRef = new BACnetObjectPropertyReference();
			BACnetObjectIdentifier objectId = object.getWrappee().getObjectIdentifier();
			propRef.setObjectIdentifier(objectId);
			propRef.propertyIdentifier = BACnetPropertyWrapper.this.id.intValue();

			subscriber.setRecipientProcess(recipientProcess);
			subscriber.setMonitoredPropertyReference(propRef);
			subscriber.issueConfirmedNotifications = true;
			subscriber.expiresAt = target.expiresAt;
			subscriber.COVIncrement = target.covIncremet;

			return subscriber;
		}

		@Override
		public void propertyChange(PropertyChangeEvent evt) {
			// Check if we're not expired yet:
			if (target.isExpired()) {
				cancelSubscription();
			} else {
				sendNotification(evt);
			}
		}

		public void cancelSubscription() {
			try {
				// unsubscribe on the object's side
				BACnetPropertyWrapper.this.covDescr.getRemoveListenerMethod().invoke(object.getWrappee(), this);

			} catch (Exception e) {
				BACnetPropertyWrapper.log.warn("cannot remove subscription", e);
			} finally {
				// remove ourselves from the dispatchers map
				BACnetPropertyWrapper.this.dispatchers.remove(key);
			}

			// Invoke the ActiveCOVSubscription method
			try {
				getActivCOVSubscriptionMethod(propDescr, "remove").invoke(object.getWrappee(), subscriber);
			} catch (Exception e) {
				log.warn(e.getMessage(), e);
			}
		}

		private void sendNotification(PropertyChangeEvent evt) {
			// check if changes are out of cov increment interval:
			if (evt.getNewValue() instanceof Float) {
				Float newVal = (Float) evt.getNewValue();
				Float oldVal = (Float) evt.getOldValue();
				if (target.covIncremet != null && Math.abs(newVal - oldVal) < target.covIncremet)
					return;
			}
			Long index = null;
			if (evt instanceof IndexedPropertyChangeEvent) {
				index = (long) ((IndexedPropertyChangeEvent) evt).getIndex();
			}
			try {
				// get codec
				Codec codec = codecFactory.getCodec(getCovEncoder());
				byte[] data = codec.encode(null, evt.getNewValue());

				BACnetPropertyValue val = new BACnetPropertyValue(BACnetPropertyWrapper.this.id, data, index);
				target.valueChanged(device.getDeviceObjectWrapper().getWrappee().getObjectIdentifier(), object
				        .getWrappee().getObjectIdentifier(), new BACnetPropertyValue[] { val });
			} catch (Exception e) {
				log.error("cannot send COV notification", e);
			}
		}

	}

	public byte[] getPackedValue() throws BACnetError {
		Method readMethod = getReadMethod();
		if (readMethod == null) {
			throw new BACnetError(ErrorClass.PROPERTY, ErrorCode.READ_ACCESS_DENIED);
		}
		byte[] res;
		try {

			// read property value from the object:
			Object val = readMethod.invoke(getObjectWrapper().getWrappee());
			// get codec
			Codec codec = codecFactory.getCodec(getEncoder());
			// encode result with codec
			ByteArrayOutputStream stream = new ByteArrayOutputStream();
			stream.write(codec.encode(null, val));

			// new BACnetSequence(3, Actions.SEQUENCE_STOP).encode(stream);
			res = stream.toByteArray();
		} catch (Exception e) {
			if (log.isDebugEnabled())
				log.debug("Cannot read property " + id + " for object "
				        + getObjectWrapper().getWrappee().getObjectIdentifier(), e);
			throw new BACnetError(ErrorClass.PROPERTY, ErrorCode.OTHER);
		}

		return res;
	}

	private byte[] getPacketArrayLength() throws BACnetError {
		Method readMethod = getReadMethod();
		if (readMethod == null) {
			throw new BACnetError(ErrorClass.PROPERTY, ErrorCode.READ_ACCESS_DENIED);
		}
		byte[] res;
		try {
			Object val = readMethod.invoke(getObjectWrapper().getWrappee());
			BACnetUInt codec = new BACnetUInt(null, ((Object[]) val).length);
			ByteArrayOutputStream stream = new ByteArrayOutputStream();
			// new BACnetSequence(3, Actions.SEQUENCE_START).encode(stream);
			codec.encode(stream);
			// new BACnetSequence(3, Actions.SEQUENCE_STOP).encode(stream);
			res = stream.toByteArray();
		} catch (Exception e) {
			if (log.isDebugEnabled())
				log.debug("Cannot read property " + id + " for object "
				        + getObjectWrapper().getWrappee().getObjectIdentifier(), e);
			throw new BACnetError(ErrorClass.PROPERTY, ErrorCode.OTHER);
		}
		return res;
	}

	public byte[] getPackedValue(Long arrayIndex) throws BACnetError {
		if (arrayIndex == null)
			throw new IllegalArgumentException("index cannot be null");

		// array index:
		// 0 - size of the array
		// 1 - value[0]
		// 2 - value[1] etc.
		if (arrayIndex == 0) {
			return getPacketArrayLength();
		}

		Method readMethod = getReadIndexedMethod();
		if (readMethod == null) {
			throw new BACnetError(ErrorClass.PROPERTY, ErrorCode.PROPERTY_IS_NOT_AN_ARRAY);
		}

		// Java indexed properties supports integer indexers only:
		if (arrayIndex > Integer.MAX_VALUE)
			throw new BACnetError(ErrorClass.PROPERTY, ErrorCode.INVALID_ARRAY_INDEX);
		Integer index = arrayIndex.intValue() - 1;
		byte[] res;
		try {
			// read property value from the object:
			Object val = readMethod.invoke(getObjectWrapper().getWrappee(), index);
			Codec codec = codecFactory.getCodec(getIndexedEncoder());

			// encode result with codec
			ByteArrayOutputStream stream = new ByteArrayOutputStream();
			stream.write(codec.encode(null, val));

			// new BACnetSequence(3, Actions.SEQUENCE_STOP).encode(stream);
			res = stream.toByteArray();
		} catch (Exception e) {
			if (log.isDebugEnabled())
				log.debug("Cannot read property " + id + " for object "
				        + getObjectWrapper().getWrappee().getObjectIdentifier(), e);
			throw new BACnetError(ErrorClass.PROPERTY, ErrorCode.OTHER);
		}

		return res;
	}

	public void setPackedValue(byte[] val) throws BACnetError {
		Method writeMethod = getWriteMethod();
		if (writeMethod == null) {
			throw new BACnetError(ErrorClass.PROPERTY, ErrorCode.WRITE_ACCESS_DENIED);
		}
		try {
			// get codec
			Codec codec = codecFactory.getCodec(getDecoder());
			// decode result with codec
			Object newVal = codec.decode(val, null);
			// write property value:
			writeMethod.invoke(getObjectWrapper().getWrappee(), newVal);
		} catch (Exception e) {
			if (log.isDebugEnabled())
				log.debug("Cannot set property " + id + " for object "
				        + getObjectWrapper().getWrappee().getObjectIdentifier(), e);
			throw new BACnetError(ErrorClass.PROPERTY, ErrorCode.OTHER);
		}
	}

	public void setPackedValue(Long arrayIndex, byte[] val) throws BACnetError {
		if (arrayIndex == null)
			throw new IllegalArgumentException("index cannot be null");
		Method writeMethod = getWriteIndexedMethod();
		if (writeMethod == null) {
			throw new BACnetError(ErrorClass.PROPERTY, ErrorCode.PROPERTY_IS_NOT_AN_ARRAY);
		}

		// Java indexed properties supports integer indexers only:
		if (arrayIndex > Integer.MAX_VALUE)
			throw new BACnetError(ErrorClass.PROPERTY, ErrorCode.INVALID_ARRAY_INDEX);
		Integer index = arrayIndex.intValue();
		try {
			// get codec
			Codec codec = codecFactory.getCodec(getIndexedDecoder());
			// decode result with codec
			Object newVal = codec.decode(val, 3);
			// write property value:
			writeMethod.invoke(getObjectWrapper().getWrappee(), index, newVal);
		} catch (Exception e) {
			if (log.isDebugEnabled())
				log.debug("Cannot set property " + id + " for object "
				        + getObjectWrapper().getWrappee().getObjectIdentifier(), e);
			throw new BACnetError(ErrorClass.OBJECT, ErrorCode.OTHER);
		}
	}

	public Long getId() {
		return id;
	}

	public int getType() {
		return typeInfo.type();
	}

	public String getName() {
		if (propDescr != null)
			return propDescr.getName();
		if (indexedDescr != null)
			return indexedDescr.getName();
		throw new IllegalStateException("Either propDescr or indexedDescr haven't be null");
	}

	public BACnetObjectWrapper getObjectWrapper() {
		return object;
	}

	public Method getReadMethod() {
		return propDescr != null ? propDescr.getReadMethod() : null;
	}

	public String getEncoder() {
		return readInfo != null ? readInfo.codec() : null;
	}

	public Method getReadIndexedMethod() {
		return indexedDescr != null ? indexedDescr.getIndexedReadMethod() : null;
	}

	public String getIndexedEncoder() {
		return readIndexedInfo != null ? readIndexedInfo.codec() : null;
	}

	public Method getWriteMethod() {
		return propDescr != null ? propDescr.getWriteMethod() : null;
	}

	public String getDecoder() {
		return writeInfo != null ? writeInfo.codec() : null;
	}

	public Method getWriteIndexedMethod() {
		return indexedDescr != null ? indexedDescr.getIndexedWriteMethod() : null;
	}

	public String getIndexedDecoder() {
		return writeIndexedInfo != null ? writeIndexedInfo.codec() : null;
	}

	public Method getAddListenerMethod() {
		return covDescr != null ? covDescr.getAddListenerMethod() : null;
	}

	public Method getRemoveListenerMethod() {
		return covDescr != null ? covDescr.getRemoveListenerMethod() : null;
	}

	public String getCovEncoder() {
		return covInfo != null ? covInfo.codec() : null;
	}

}
