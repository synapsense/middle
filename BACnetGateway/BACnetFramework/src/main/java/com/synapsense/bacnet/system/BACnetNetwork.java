package com.synapsense.bacnet.system;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import com.synapsense.bacnet.objects.BACnetDeviceWrapper;
import com.synapsense.bacnet.system.shared.BACnetAddress;
import com.synapsense.bacnet.system.shared.BACnetAddressBinding;
import com.synapsense.bacnet.system.shared.BACnetObjectIdentifier;

/**
 * Class represents a virtual BACnet network
 * 
 * @author Oleg Stepanov
 * 
 */
public class BACnetNetwork {
	private final int networkNumber;
	private static final Logger logger = Logger.getLogger(BACnetNetwork.class);

	private Map<byte[], BACnetDeviceWrapper> devices;
	private Collection<Device> roDevices;

	/**
	 * Creates new network with given number.
	 * 
	 * @param networkNumber
	 *            new network number.
	 */
	public BACnetNetwork(int networkNumber) {
		this.networkNumber = networkNumber;
		devices = new HashMap<byte[], BACnetDeviceWrapper>();
		roDevices = Collections.unmodifiableCollection((Collection<? extends Device>) devices.values());
	}

	/**
	 * Locates device by its object identifier.
	 * 
	 * @param deviceId
	 *            object id of a device to be returned.
	 * @return device with given object id
	 * @throws IndexOutOfBoundsException
	 *             if there's no device with such id in this network.
	 */
	public Device getDevice(BACnetObjectIdentifier deviceId) throws IndexOutOfBoundsException {
		for (BACnetDeviceWrapper wrapper : devices.values()) {
			if (wrapper.getWrappee().getObjectIdentifier().equals(deviceId))
				return wrapper;
		}
		throw new IndexOutOfBoundsException("No device with id " + deviceId + " in net " + this.networkNumber);
	}

	/**
	 * Locates device by its mac.
	 * 
	 * @param deviceMac
	 *            mac of a device to be returned.
	 * @return device with given mac
	 * @throws IndexOutOfBoundsException
	 *             if there's no device with such mac in this network.
	 */
	public Device getDevice(byte[] deviceMac) throws IndexOutOfBoundsException {
		// TODO : SUBJECT TO CHANGE!!!
		Device dev = null;
		Set<Map.Entry<byte[], BACnetDeviceWrapper>> entries = devices.entrySet();
		for (Map.Entry<byte[], BACnetDeviceWrapper> entry : entries) {
			if (Arrays.equals(entry.getKey(), deviceMac)) {
				dev = entry.getValue();
				break;
			}
		}
		if (dev == null)
			throw new IndexOutOfBoundsException("no device with mac=" + toHex(deviceMac) + " exists in the network "
			        + networkNumber);
		return dev;
	}

	/**
	 * Returns devices belonging to this network.
	 * 
	 * @return unmodifiable collection of {@link Device}s
	 */
	public Collection<Device> getDevices() {
		return roDevices;
	}

	public static String toHex(byte[] b) {
		StringBuilder result = new StringBuilder();
		if (b != null) {
			for (int i = 0; i < b.length; ++i) {
				if (i == 0)
					result.append(":");
				result.append((Integer.toHexString((b[i] & 0xFF) | 0x100).substring(1, 3).toUpperCase()));
			}
		}
		return result.toString();
	}

	/**
	 * Adds device to the network. New mac is generated for a device and added
	 * as address binding. Device's object identifier is updated with new
	 * instance number as well.
	 * 
	 * @param deviceWrapper
	 *            device to be added.
	 */
	public void addDevice(BACnetDeviceWrapper deviceWrapper) {
		// generate new device id:
		// int deviceId = deviceIdGenerator.getAndIncrement();
		BACnetObjectIdentifier objId = deviceWrapper.getWrappee().getObjectIdentifier();
		int deviceId = objId.getInstanceNumber();
		// create new mac (6 bytes since we emulate ethernet):
		byte[] mac = new byte[] { 0, 0, (byte) (deviceId >> 24), (byte) (deviceId >> 16), (byte) (deviceId >> 8),
		        (byte) deviceId };

		// add device address binding
		BACnetAddress addr = new BACnetAddress(this.getNetworkNumber(), mac);
		BACnetAddressBinding addrBinding = new BACnetAddressBinding(objId, addr);
		deviceWrapper.getWrappee().getDeviceAddressBindings().add(addrBinding);

		logger.debug("addDevice(network=" + getNetworkNumber() + ", deviceId=" + deviceId + ", mac=" + toHex(mac) + ")");
		// put device into internal map as well
		devices.put(mac, deviceWrapper);
	}

	/**
	 * Removes device from network
	 * 
	 * @param deviceWrapper
	 */
	public void removeDevice(BACnetDeviceWrapper deviceWrapper) {
		// find and remove all address bindings with this network
		// LinkedList<BACnetAddressBinding> netBindings = new
		// LinkedList<BACnetAddressBinding>();
		// for(BACnetAddressBinding bnd :
		// deviceWrapper.getWrappee().getDeviceAddressBindings()) {
		// if(bnd.deviceAddress.networkNumber == this.networkNumber)
		// netBindings.add(bnd);
		// }
		// deviceWrapper.getWrappee().getDeviceAddressBindings().removeAll(netBindings);
		// remove from the internal map

		// devices.values().remove(deviceWrapper);
		byte[] mac = deviceWrapper.getWrappee().getAddressBindings()[0].deviceAddress.macAddress;
		Set<Map.Entry<byte[], BACnetDeviceWrapper>> entries = devices.entrySet();
		for (Map.Entry<byte[], BACnetDeviceWrapper> entry : entries) {
			if (Arrays.equals(entry.getKey(), mac)) {
				entries.remove(entry);
				break;
			}
		}

		deviceWrapper.getWrappee().getDeviceAddressBindings().clear();
		devices.remove(mac);
	}

	public void removeAllDevices() {
		for (BACnetDeviceWrapper wrapper : devices.values()) {
			wrapper.getWrappee().getDeviceAddressBindings().clear();
		}
		devices.clear();
	}

	/**
	 * @return number of this network.
	 */
	public int getNetworkNumber() {
		return networkNumber;
	}
}
