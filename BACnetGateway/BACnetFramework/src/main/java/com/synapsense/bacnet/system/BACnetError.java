package com.synapsense.bacnet.system;

import com.synapsense.bacnet.enums.ErrorClass;
import com.synapsense.bacnet.enums.ErrorCode;

/**
 * Exception class that encapsulates BACnet error class and error code.
 * 
 * @author Oleg Stepanov
 * 
 */
public class BACnetError extends Exception {

	private static final long serialVersionUID = -5193315917452044378L;
	private ErrorClass errorClass;
	private ErrorCode errorCode;

	public BACnetError(ErrorClass errorClass, ErrorCode errorCode) {
		this.errorClass = errorClass;
		this.errorCode = errorCode;
	}

	public BACnetError(String message) {
		super(message);
	}

	public BACnetError(Throwable cause) {
		super(cause);
	}

	public BACnetError(String message, Throwable cause) {
		super(message, cause);
	}

	public long getErrorClassNum() {
		return errorClass.getCode();
	}

	public ErrorClass getErrorClass() {
		return errorClass;
	}

	public long getErrorCodeNum() {
		return errorCode.getCode();
	}

	public ErrorCode getErrorCode() {
		return errorCode;
	}
}
