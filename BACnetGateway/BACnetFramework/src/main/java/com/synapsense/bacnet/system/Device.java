package com.synapsense.bacnet.system;

import java.util.Map;

import com.synapsense.bacnet.objects.BACnetPropertyWrapper;
import com.synapsense.bacnet.services.ReadProperty;
import com.synapsense.bacnet.services.SubscribeCOVProperty;
import com.synapsense.bacnet.services.WriteProperty;
import com.synapsense.bacnet.system.shared.BACnetObjectIdentifier;

/**
 * Interface of a virtual BACnet device.
 * 
 * @author Oleg Stepanov
 * 
 */
public interface Device {

	// Confirmed services

	/**
	 * Method allows to request ReadProperty service of this device.
	 * 
	 * @return ReadProperty service or <code>null</code> if such is not
	 *         supported.
	 */
	ReadProperty getReadPropertyService();

	/**
	 * Method allows to request WriteProperty service of this device.
	 * 
	 * @return WriteProperty service or <code>null</code> if such is not
	 *         supported.
	 */
	WriteProperty getWritePropertyService();

	/**
	 * Method allows to request SubscribeCOVProperty service of this device.
	 * 
	 * @return SubscribeCOVProperty service or <code>null</code> if such is not
	 *         supported.
	 */
	SubscribeCOVProperty getSubscribeCOVPropertyService();

	/**
	 * Method allows to request an array of <code>BACnetPropertyWrapper</code>
	 * instances by specified objectId.
	 * 
	 * @param objId
	 *            an object identifier
	 * @return an array of <code>BACnetPropertyWrapper</code> instances
	 */
	Map<Long, BACnetPropertyWrapper> getObjectProperties(BACnetObjectIdentifier objId) throws BACnetError;
}
