package com.synapsense.bacnet.services;

import com.synapsense.bacnet.system.BACnetError;
import com.synapsense.bacnet.system.shared.BACnetObjectIdentifier;

public interface WriteProperty {
	/**
	 * Allows to read a value of given property.
	 * 
	 * @param objId
	 *            id of object which property is to be changed.
	 * @param propId
	 *            id of the property.
	 * @param arrayIndex
	 *            index for indexed properties (optional, null for not
	 *            specified).
	 * @param value
	 *            packed value to be decoded and written.
	 * @param priority
	 *            Request priority
	 * @throws BACnetError
	 *             if request is failed.
	 */
	void write(BACnetObjectIdentifier objId, long propId, Long arrayIndex, byte[] value, byte priority)
	        throws BACnetError;
}
