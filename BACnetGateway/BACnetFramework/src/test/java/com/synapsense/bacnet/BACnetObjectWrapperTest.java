package com.synapsense.bacnet;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.beans.PropertyChangeListener;
import java.util.Map;

import org.junit.BeforeClass;
import org.junit.Test;

import com.synapsense.bacnet.objects.BACnetObject;
import com.synapsense.bacnet.objects.BACnetObjectWrapper;
import com.synapsense.bacnet.objects.BACnetProperty;
import com.synapsense.bacnet.objects.BACnetPropertyWrapper;
import com.synapsense.bacnet.system.shared.BACnetObjectIdentifier;

public class BACnetObjectWrapperTest {

	private static Map<Long, BACnetPropertyWrapper> bindings;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		bindings = new BACnetObjectWrapper(new TestBACnetObject1()).getPropertyBindings();
	}

	@Test
	public void testBindingsFound() {
		assertEquals(5, bindings.entrySet().size());
	}

	@Test
	public void testInheritedBinding() {
		BACnetPropertyWrapper info = bindings.get(75l);
		assertNotNull(info);
		assertEquals("getObjectIdentifier", info.getReadMethod().getName());
	}

	@Test
	public void testCustomBinding() {
		BACnetPropertyWrapper info = bindings.get(5l);
		assertNotNull(info);
		assertEquals("UIntCodec", info.getEncoder());
		assertNull(info.getDecoder());
	}

	@Test
	public void testBindingOverride() {
		BACnetPropertyWrapper info = bindings.get(77l);
		assertNotNull(info);
		assertEquals("CharacterStringCodec", info.getEncoder());
	}

	@Test
	public void testCovBinding() {
		BACnetPropertyWrapper info = bindings.get(77l);
		assertNotNull(info);
		assertNotNull(info.getAddListenerMethod());
		assertNotNull(info.getRemoveListenerMethod());
	}

	public static class TestBACnetObject1 implements BACnetObject {
		// dummy properties
		/**
		 * Object_Identifier of this object.
		 */
		private BACnetObjectIdentifier id;

		@Override
		public BACnetObjectIdentifier getObjectIdentifier() {
			return id;
		}

		@Override
		public void setObjectIdentifier(BACnetObjectIdentifier id) {
			this.id = id;
		}

		/**
		 * Object_Name of this object.
		 */
		private String name;

		@Override
		// must override codec name inherited from BACnetObject interface
		@BACnetProperty(id = 77, codec = "CharacterStringCodec")
		public String getObjectName() {
			return name;
		}

		@Override
		public short getObjectType() {
			return id.getObjectType();
		}

		@BACnetProperty(id = 5, codec = "UIntCodec")
		public long getOtherProperty() {
			return 0;
		}

		// actually, the codes specified is invalid but it's ok for this test
		@BACnetProperty(id = 7, codec = "CharacterStringCodec")
		public String[] getArrayProperty() {
			return null;
		}

		@BACnetProperty(id = 7, codec = "CharacterStringCodec")
		public String getArrayProperty(int index) {
			return "";
		}

		@BACnetProperty(id = 77, codec = "CharacterStringCodec", cov = true)
		public void addObjectNameListener(PropertyChangeListener listener) {
		}

		public void removeObjectNameListener(PropertyChangeListener listener) {
		}

	}
}
