package com.synapsense.bacnet.network.media.IP;

import java.util.HashMap;
import java.util.Map;

import com.synapsense.bacnet.network.jni.Property;
import com.synapsense.bacnet.network.jni.PropertyNotFoundException;

public class DataPacket {

	private Map<Property, Integer> intProps = new HashMap<Property, Integer>();
	private Map<Property, Double> doubleProps = new HashMap<Property, Double>();
	private Map<Property, String> stringProps = new HashMap<Property, String>();
	private Map<Property, Boolean> booleanProps = new HashMap<Property, Boolean>();
	private Map<Property, byte[]> byteArrayProps = new HashMap<Property, byte[]>();
	private Map<Property, short[]> shortArrayProps = new HashMap<Property, short[]>();
	private Map<Property, String[]> stringArrayProps = new HashMap<Property, String[]>();
	private Map<String, Map<Property, ?>> allProps = new HashMap<String, Map<Property, ?>>();

	public DataPacket() {
		allProps.put("int", intProps);
		allProps.put("double", doubleProps);
		allProps.put("string", stringProps);
		allProps.put("boolean", booleanProps);
		allProps.put("byteArray", byteArrayProps);
		allProps.put("shortArray", shortArrayProps);
		allProps.put("stringArray ", stringArrayProps);
	}

	public int readInteger(Property key) throws PropertyNotFoundException {
		return getProperty(key, intProps).intValue();
	}

	public short readShort(Property key) throws PropertyNotFoundException {
		return getProperty(key, intProps).shortValue();
	}

	public byte readByte(Property key) throws PropertyNotFoundException {
		return getProperty(key, intProps).byteValue();
	}

	public double readDouble(Property key) throws PropertyNotFoundException {
		return getProperty(key, doubleProps).doubleValue();
	}

	public String readString(Property key) throws PropertyNotFoundException {
		return getProperty(key, stringProps);
	}

	public boolean readBoolean(Property key) throws PropertyNotFoundException {
		return getProperty(key, booleanProps).booleanValue();
	}

	public byte[] readByteArray(Property key) throws PropertyNotFoundException {
		return getProperty(key, byteArrayProps);
	}

	public String[] readStringArray(Property key) throws PropertyNotFoundException {
		return getProperty(key, stringArrayProps);
	}

	public short[] readShortArray(Property key) throws PropertyNotFoundException {
		return getProperty(key, shortArrayProps);
	}

	public void writeInteger(Property key, int value) {
		intProps.put(key, value);
	}

	public void writeShort(Property key, int value) {
		intProps.put(key, value);
	}

	public void writeByte(Property key, int value) {
		intProps.put(key, value);
	}

	public void writeDouble(Property key, double value) {
		doubleProps.put(key, value);
	}

	public void writeString(Property key, String value) {
		stringProps.put(key, value);
	}

	public void writeBoolean(Property key, boolean value) {
		booleanProps.put(key, value);
	}

	public void writeByteArray(Property key, byte[] value) {
		byteArrayProps.put(key, value);
	}

	public void writeStringArray(Property key, String[] value) {
		stringArrayProps.put(key, value);
	}

	public void writeShortArray(Property key, short[] value) {
		shortArrayProps.put(key, value);
	}

	private <T> T getProperty(Property key, Map<Property, T> props) throws PropertyNotFoundException {
		if (!props.containsKey(key)) {
			throw new PropertyNotFoundException(key);
		}
		return props.get(key);
	}

	public void clear() {
		intProps.clear();
		doubleProps.clear();
		stringProps.clear();
		booleanProps.clear();
		byteArrayProps.clear();
		shortArrayProps.clear();
		stringArrayProps.clear();
		allProps.clear();
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("Properties:\n");
		for (String mapKey : allProps.keySet()) {
			Map<Property, ?> map = allProps.get(mapKey);
			if (!map.isEmpty() && !mapKey.contains("Array")) {
				sb.append(mapKey + ":\n");
				for (Property key : map.keySet()) {
					sb.append("[" + key.name() + "] = [" + map.get(key) + "]");
					sb.append("\n");
				}
			}
		}

		sb.append("Byte Array:\n");
		for (Property key : byteArrayProps.keySet()) {
			sb.append("[" + key.name() + "] = [" + getHexString(byteArrayProps.get(key)).toUpperCase() + "]");
			sb.append("\n");
		}
		return sb.toString();
	}

	public static String getHexString(byte[] b) {
		String result = "";
		for (int i = 0; i < b.length; i++) {
			result += Integer.toString((b[i] & 0xff) + 0x100, 16).substring(1);
		}
		return result;
	}
}
