package com.synapsense.bacnet.network.media.IP.parcer;

public class BVLCCodes {
	// TODO change to enum
	public static final int RESULT = 0x0;
	public static final int WRITE_BROADCAST_DISTRIBUTION_TABLE = 0x1;
	public static final int READ_BROADCAST_DISTRIBUTION_TABLE = 0x2;
	public static final int READ_BROADCAST_DISTRIBUTION_TABLE_ACK = 0x3;
	public static final int FORWARDED_NPDU = 0x4;
	public static final int REGISTER_FOREIGN_DEVICE = 0x5;
	public static final int READ_FOREIGN_DEVICE_TABLE = 0x6;
	public static final int READ_FOREIGN_DEVICE_TABLE_ACK = 0x7;
	public static final int DELETE_FOREIGN_DEVICE_TABLE_ENTRY = 0x8;
	public static final int DISTRIBUTE_BROADCAST_TO_NETWORK = 0x9;
	public static final int ORIGINAL_UNICAST_NPDU = 0x0A;
	public static final int ORIGINAL_BROADCAST_NPDU = 0x0B;

}
