package com.synapsense.bacnet.network.media.IP.parcer;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import com.synapsense.bacnet.network.jni.MalformedPacketException;
import com.synapsense.bacnet.network.jni.Property;
import com.synapsense.bacnet.network.jni.PropertyNotFoundException;
import com.synapsense.bacnet.network.media.IP.DataPacket;
import com.synapsense.bacnet.network.pdu.npdu.NPDUMessageType;

/*
 * 
 *  Implementation of AbstractParcer, which processes parsing 
 *  BACnet-Disconnect-Connection-to-Network NPDU part of packet.
 *  <br>
 *  \code
 * | DNET |
 * |   2  |
 * |   M  |
 *  \endcode
 *
 */
public class DisconnectConnectionToNetworkParcer extends AbstractParcer {

	@Override
	public void read(DataInputStream stream, DataPacket packet) throws MalformedPacketException, IOException {
		logger.debug("Reading BACnet Disconnect-Connection-to-Network...");
		if (stream.available() != 2) {
			throw new MalformedPacketException();
		}
		packet.writeByte(Property.MESSAGE_TYPE, NPDUMessageType.Disconnect_Connection_To_Network.getCode());
		packet.writeShort(Property.NDNET, stream.readUnsignedShort());
	}

	@Override
	public void write(DataOutputStream stream, DataPacket packet) throws IOException, PropertyNotFoundException {
		logger.debug("Writting BACnet Disconnect-Connection-to-Network...");
		stream.writeByte(NPDUMessageType.Disconnect_Connection_To_Network.getCode());
		stream.writeShort(packet.readShort(Property.NDNET));
	}
}
