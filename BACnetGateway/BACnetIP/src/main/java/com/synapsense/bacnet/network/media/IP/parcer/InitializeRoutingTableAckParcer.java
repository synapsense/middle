package com.synapsense.bacnet.network.media.IP.parcer;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import com.synapsense.bacnet.network.jni.MalformedPacketException;
import com.synapsense.bacnet.network.jni.Property;
import com.synapsense.bacnet.network.jni.PropertyNotFoundException;
import com.synapsense.bacnet.network.media.IP.DataPacket;
import com.synapsense.bacnet.network.pdu.npdu.NPDUMessageType;

/*
 *  Implementation of simple_packet, which processes parsing 
 *  BACnet-Initialize-Routing-Table NPDU part of packet.
 *  <br>
 *  \code
 * | PORTS | DNET | PORT ID | PORT INFO LEN | PORT INFO | DNET ...
 * |   1   |   2  |    1    |       1       |    PIL    |   2 
 * |   M   |   O  |    O    |       O       |     O     |   O
 *  \endcode
 *
 */
public class InitializeRoutingTableAckParcer extends RoutingTablesAbstract {

	@Override
	public void read(DataInputStream stream, DataPacket packet) throws MalformedPacketException, IOException {
		logger.debug("Reading BACnet Initialize-Routing-Table-to-Network-Ack...");

		packet.writeByte(Property.MESSAGE_TYPE, NPDUMessageType.Initialize_Routing_Table_Ack.getCode());
		super.read(stream, packet);

	}

	@Override
	public void write(DataOutputStream stream, DataPacket packet) throws IOException, PropertyNotFoundException {
		logger.debug("Writting BACnet Initialize-Routing-Table-to-Network-Ack...");
		stream.writeByte(NPDUMessageType.Initialize_Routing_Table_Ack.getCode());
		super.write(stream, packet);
	}

}
