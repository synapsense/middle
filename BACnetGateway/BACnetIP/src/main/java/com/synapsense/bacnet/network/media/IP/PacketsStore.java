package com.synapsense.bacnet.network.media.IP;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

public class PacketsStore {
	ConcurrentHashMap<Integer, DataPacket> packets;
	AtomicInteger idCounter;

	public PacketsStore() {
		packets = new ConcurrentHashMap<Integer, DataPacket>();
		idCounter = new AtomicInteger(0);
	}

	public Integer createPacket() {
		Integer id = idCounter.getAndIncrement();
		packets.put(id, new DataPacket());
		return id;
	}

	public void removePacket(Integer id) {
		if (packets.containsKey(id)) {
			packets.get(id).clear();
			packets.remove(id);
		}
	}

	public DataPacket getPacket(Integer id) {
		return packets.get(id);
	}

	public void clear() {
		packets.clear();
	}

}
