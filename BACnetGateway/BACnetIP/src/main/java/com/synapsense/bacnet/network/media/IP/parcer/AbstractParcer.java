package com.synapsense.bacnet.network.media.IP.parcer;

import org.apache.log4j.Logger;

public abstract class AbstractParcer implements PacketParcer {
	protected final Logger logger = Logger.getLogger(this.getClass());
	private byte unparcedMessageType;

	public void setUnparcedMessageType(byte type) {
		this.unparcedMessageType = type;
	}

	public byte getUnparcedMessageType() {
		return unparcedMessageType;
	}
}
