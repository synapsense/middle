package com.synapsense.bacnet.network.media.IP.parcer;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import com.synapsense.bacnet.network.jni.MalformedPacketException;
import com.synapsense.bacnet.network.jni.Property;
import com.synapsense.bacnet.network.jni.PropertyNotFoundException;
import com.synapsense.bacnet.network.media.IP.DataPacket;
import com.synapsense.bacnet.network.pdu.apdu.APDUMessageType;

/*
 *  Implementation of AbstractParcer, which processes parsing BACnet-Reject APDU
 *  part of packet.
 *  <br>
 *  \code
 *   7   6   5   4   3   2   1   0
 * |---|---|---|---|---|---|---|---|
 * |    PDU Type   | 0 | 0 | 0 | 0 |
 * |---|---|---|---|---|---|---|---|
 * |      Original Invoke ID       |
 * |---|---|---|---|---|---|---|---|
 * |          Reject Reason        |
 * |---|---|---|---|---|---|---|---|
 *  \endcode
 */
public class RejectParcer extends AbstractParcer {

	@Override
	public void read(DataInputStream stream, DataPacket packet) throws MalformedPacketException, IOException {
		logger.debug("Reading BACnet Reject-PDU...");

		if (stream.available() < 2) {
			throw new MalformedPacketException();
		}
		packet.writeByte(Property.MESSAGE_TYPE, APDUMessageType.reject_PDU.getType());
		packet.writeByte(Property.INVOKE_ID, stream.readUnsignedByte());
		packet.writeByte(Property.REJECT_REASON, stream.readUnsignedByte());
	}

	@Override
	public void write(DataOutputStream stream, DataPacket packet) throws PropertyNotFoundException, IOException {
		logger.debug("Writting BACnet Reject-PDU...");
		int messageType = packet.readByte(Property.MESSAGE_TYPE) << 4;
		stream.writeByte(messageType);
		stream.writeByte(packet.readByte(Property.INVOKE_ID));
		stream.writeByte(packet.readByte(Property.REJECT_REASON));
	}

}
