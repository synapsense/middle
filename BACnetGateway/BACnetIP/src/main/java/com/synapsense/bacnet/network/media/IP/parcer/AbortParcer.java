package com.synapsense.bacnet.network.media.IP.parcer;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import com.synapsense.bacnet.network.jni.MalformedPacketException;
import com.synapsense.bacnet.network.jni.Property;
import com.synapsense.bacnet.network.jni.PropertyNotFoundException;
import com.synapsense.bacnet.network.media.IP.DataPacket;
import com.synapsense.bacnet.network.pdu.apdu.APDUMessageType;

/*
 *  Implementation of AbstractParcer, which processes parsing BACnet-Abort APDU
 *  part of packet.
 *  <br>
 *  \code
 *   7   6   5   4   3   2   1   0
 * |---|---|---|---|---|---|---|---|
 * |    PDU Type   | 0 | 0 | 0 |SRV|
 * |---|---|---|---|---|---|---|---|
 * |      Original Invoke ID       |
 * |---|---|---|---|---|---|---|---|
 * |          Abort Reason         |
 * |---|---|---|---|---|---|---|---|
 *  \endcode
 */
public class AbortParcer extends AbstractParcer {

	@Override
	public void read(DataInputStream stream, DataPacket packet) throws MalformedPacketException, IOException {
		logger.debug("Reading BACnet Abort-PDU...");

		if (stream.available() < 2) {
			throw new MalformedPacketException();
		}
		packet.writeByte(Property.MESSAGE_TYPE, APDUMessageType.abort_PDU.getType());
		packet.writeBoolean(Property.ACK_SERVER, (getUnparcedMessageType() & 1) != 0);
		packet.writeByte(Property.INVOKE_ID, stream.readUnsignedByte());
		packet.writeByte(Property.ABORT_REASON, stream.readUnsignedByte());
	}

	@Override
	public void write(DataOutputStream stream, DataPacket packet) throws PropertyNotFoundException, IOException {
		logger.debug("Writting BACnet Abort-PDU...");
		int messageType = packet.readByte(Property.MESSAGE_TYPE) << 4;
		if (packet.readBoolean(Property.ACK_SERVER)) {
			messageType |= 1;
		}
		stream.writeByte(messageType);
		stream.writeByte(packet.readByte(Property.INVOKE_ID));
		stream.writeByte(packet.readByte(Property.ABORT_REASON));
	}
}
