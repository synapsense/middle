package com.synapsense.bacnet.network.media.IP.parcer;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import com.synapsense.bacnet.network.jni.MalformedPacketException;
import com.synapsense.bacnet.network.jni.Property;
import com.synapsense.bacnet.network.jni.PropertyNotFoundException;
import com.synapsense.bacnet.network.media.IP.DataPacket;
import com.synapsense.bacnet.network.pdu.npdu.NPDUMessageType;

public class WhoIsRouterToNetworkParcer extends AbstractParcer {

	@Override
	public void read(DataInputStream stream, DataPacket packet) throws MalformedPacketException, IOException {
		logger.debug("Reading BACnet Who-Is-Router-to-Network...");
		packet.writeByte(Property.MESSAGE_TYPE, NPDUMessageType.Who_Is_Router_To_Network.getCode());
		if (stream.available() > 0) {
			packet.writeShort(Property.NDNET, stream.readUnsignedShort());
		}
	}

	@Override
	public void write(DataOutputStream stream, DataPacket packet) throws IOException, PropertyNotFoundException {
		logger.debug("Writting BACnet Who-Is-Router-to-Network...");
		stream.writeByte(packet.readByte(Property.MESSAGE_TYPE));
		try {
			stream.writeShort(packet.readShort(Property.NDNET));
		} catch (PropertyNotFoundException e) {
			// do nothing, this property is optional
		}

	}
}
