package com.synapsense.bacnet.network.media.IP;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.net.BindException;
import java.net.DatagramPacket;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.InterfaceAddress;
import java.net.NetworkInterface;
import java.net.SocketAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.Formatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.synapsense.bacnet.network.jni.MalformedPacketException;
import com.synapsense.bacnet.network.jni.NotBVLLMessageException;
import com.synapsense.bacnet.network.jni.Property;
import com.synapsense.bacnet.network.jni.PropertyNotFoundException;
import com.synapsense.bacnet.network.jni.UnableToSendException;
import com.synapsense.bacnet.network.jni.UnableToSetUpInterfaceException;
import com.synapsense.bacnet.network.media.Media;
import com.synapsense.bacnet.network.media.MediaManager;
import com.synapsense.bacnet.network.media.MediaNames;
import com.synapsense.bacnet.network.media.IP.parcer.IPHeader;

public class IP implements Media {
	private Map<Integer, InterfaceAddress> mediaInfo;
	private byte[] MAC;
	private PacketsStore packetsStore;
	private IpPacketReceiver ipReciever;

	static Logger logger = Logger.getLogger(IP.class);
	private int interfaceIndex = 0;
	private int mask = 0;

	public IP() {
		ipReciever = new IpPacketReceiver();
		packetsStore = new PacketsStore();
		try {
			mediaInfo = new HashMap<Integer, InterfaceAddress>();
			Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
			logger.info("Loading media information.");
			int interfaceId = 0;
			while (interfaces.hasMoreElements()) {
				NetworkInterface ni = interfaces.nextElement();
				// filter out virtual, inactive, loopback interfaces
				if (!ni.isVirtual() && !ni.isLoopback() && ni.getHardwareAddress() != null) {

					logger.info("[" + interfaceId + "] Name->" + ni.getName());
					List<InterfaceAddress> ips = ni.getInterfaceAddresses();
					for (InterfaceAddress interfaceAddress : ips) {
						InetAddress ip = interfaceAddress.getAddress();
						if (ip instanceof Inet4Address) {
							String ipString = ip.getHostAddress();
							logger.info("[" + interfaceId + "] IP->[" + ipString + "] ");
							Formatter f = new Formatter();
							byte[] mac = getDestinationMac(ipString);
							for (int i = 0; i < mac.length; i++) {
								f.format("%02X%s", mac[i], (i < mac.length - 1) ? "-" : "");
							}
							// MAC is a byte representation of the ip
							logger.info("[" + interfaceId + "] MAC->" + f.out());
							f.close();
							mediaInfo.put(interfaceId, interfaceAddress);
							interfaceId++;

						}
					}
				} else {
					logger.warn("Wrong Interface " + ni.getDisplayName() + " " + "isUp=" + ni.isUp() + " isVirtual="
					        + ni.isVirtual() + " isLoopBack=" + ni.isLoopback() + " isHardwareAddressNull="
					        + (ni.getHardwareAddress() == null));
					for (InterfaceAddress addr : ni.getInterfaceAddresses()) {
						logger.warn("Filtered address: " + addr.getAddress().getHostAddress());
					}
				}
			}

		} catch (SocketException e) {
			logger.error("Error while getting network interfaces info.");
		}

	}

	public void setPort(int port) {
		ipReciever.setIPPort(port);
	}

	public void setMask(int mask) {
		this.mask = mask;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsense.bacnet.network.media.Media#createPacket()
	 */
	@Override
	public int createPacket() {
		return packetsStore.createPacket();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsense.bacnet.network.media.Media#getActiveEthMac()
	 */
	@Override
	public byte[] getActiveEthMac() {
		return MAC;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsense.bacnet.network.media.Media#getBroadcast()
	 */
	@Override
	public byte[] getBroadcast() {
		// mediaInfo.get(interfaceIndex).getInterfaceAddresses().get(0).getBroadcast();
		// byte[] addr = MAC;
		// int ip = addr[0] & 0xFF;
		// ip <<= 8;
		// ip |= addr[1] & 0xFF;
		// ip <<= 8;
		// ip |= addr[2] & 0xFF;
		// ip <<= 8;
		// ip |= addr[3] & 0xFF;
		// logger.debug("IP address: " + (addr[0] & 0xFF) + "." + (addr[1] &
		// 0xFF) + "." + (addr[2] & 0xFF) + "."
		// + (addr[3] & 0xFF) + " {" + Integer.toBinaryString(ip) + "}");

		// int m = 0xFFFFFFFF;
		// m <<= (32 - mask);
		// logger.debug("Mask: " + ((m >> 24) & 0xFF) + "." + ((m >> 16) & 0xFF)
		// + "." + ((m >> 8) & 0xFF) + "."
		// + (m & 0xFF) + " {" + Integer.toBinaryString(m) + "}");

		// long subnet = ((long) ip & m) & 0xFFFFFFFF;
		// logger.debug("Subnet IP: " + ((subnet >> 24) & 0xFF) + "." + ((subnet
		// >> 16) & 0xFF) + "."
		// + ((subnet >> 8) & 0xFF) + "." + (subnet & 0xFF) + " {" +
		// Long.toBinaryString(subnet) + "}");

		// long b = (subnet | (~m)) & 0xFFFFFFFF;
		// logger.debug("Broadcast: " + ((b >> 24) & 0xFF) + "." + ((b >> 16) &
		// 0xFF) + "." + ((b >> 8) & 0xFF) + "."
		// + (b & 0xFF) + " {" + Long.toBinaryString(b) + "}");

		// byte[] broadcast = { (byte) (b >> 24), (byte) (b >> 16), (byte) (b >>
		// 8), (byte) b, (byte) 0xBA, (byte) 0xC0 };
		InetAddress ia = mediaInfo.get(interfaceIndex).getBroadcast();
		return getDestinationMac(ia.getHostAddress());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsense.bacnet.network.media.Media#getMediaAddress(int)
	 */
	@Override
	public byte[] getMediaAddress(int number) {
		logger.trace("Fetching interface for number " + number);
		InterfaceAddress addr = mediaInfo.get(interfaceIndex);
		if (addr == null) {
			return null;
		} else {
			InetAddress ia = addr.getAddress();
			return getDestinationMac(ia.getHostAddress());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsense.bacnet.network.media.Media#getMediaType()
	 */
	@Override
	public MediaNames getMediaType() {
		return MediaNames.BACnetIP;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.synapsense.bacnet.network.media.Media#isBroadcast(java.lang.String)
	 */
	@Override
	public boolean isBroadcast(byte[] addr) {
		if (addr == null)
			return false;
		int ip = addr[0] & 0xFF;
		ip <<= 8;
		ip |= addr[1] & 0xFF;
		ip <<= 8;
		ip |= addr[2] & 0xFF;
		ip <<= 8;
		ip |= addr[3] & 0xFF;
		// String strIP = (addr[0]&0xFF)+"."+(addr[1]&0xFF)+"."+
		// (addr[2]&0xFF)+"."+(addr[3]&0xFF);
		// //logger.debug("Checking IP address...: "+strIP+" {"+Integer.toBinaryString(ip)+"}");

		int m = 0xFFFFFFFF;
		m <<= (32 - mask);
		// logger.debug("Mask: "+ ((m>>24)&0xFF)+
		// "."+((m>>16)&0xFF)+"."+((m>>8)&0xFF)+"."+(m&0xFF)+" {"+Integer.toBinaryString(m)+"}");

		long subnet = ((long) ip & m) & 0xFFFFFFFF;
		// logger.debug("Subnet IP: "+((subnet>>24)&0xFF)+
		// "."+((subnet>>16)&0xFF)+"."+((subnet>>8)&0xFF)+"."+(subnet&0xFF)+" {"+Long.toBinaryString(subnet)+"}");

		long b = (subnet | (~m)) & 0xFFFFFFFF;
		// logger.debug("Broadcast: "+ ((b>>24)&0xFF)+
		// "."+((b>>16)&0xFF)+"."+((b>>8)&0xFF)+"."+(b&0xFF)+" {"+Long.toBinaryString(b)+"}");
		byte[] broadcast = { (byte) (b >> 24), (byte) (b >> 16), (byte) (b >> 8), (byte) b };

		for (int i = 0; i < broadcast.length; i++) {
			if (broadcast[i] != addr[i]) {
				// logger.debug(strIP + "is NOT a broadcast address");
				return false;
			}
		}
		// logger.debug(strIP + " is BROADCAST address");
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsense.bacnet.network.media.Media#readBoolean(int,
	 * com.synapsense.bacnet.network.jni.Property)
	 */
	@Override
	public boolean readBoolean(int packetID, Property prop) throws PropertyNotFoundException {
		return packetsStore.getPacket(packetID).readBoolean(prop);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsense.bacnet.network.media.Media#readByte(int,
	 * com.synapsense.bacnet.network.jni.Property)
	 */
	@Override
	public byte readByte(int packetID, Property prop) throws PropertyNotFoundException {
		return packetsStore.getPacket(packetID).readByte(prop);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsense.bacnet.network.media.Media#readByteArray(int,
	 * com.synapsense.bacnet.network.jni.Property)
	 */
	@Override
	public byte[] readByteArray(int packetID, Property prop) throws PropertyNotFoundException {
		return packetsStore.getPacket(packetID).readByteArray(prop);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsense.bacnet.network.media.Media#readDST(int)
	 */
	@Override
	public byte[] readDST(int packetID) {
		try {
			return readByteArray(packetID, Property.DST_MAC);
		} catch (PropertyNotFoundException e) {
			return null;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsense.bacnet.network.media.Media#readDone(int)
	 */
	@Override
	public void readDone(int packetID) {
		packetsStore.removePacket(packetID);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsense.bacnet.network.media.Media#readDouble(int,
	 * com.synapsense.bacnet.network.jni.Property)
	 */
	@Override
	public double readDouble(int packetID, Property prop) throws PropertyNotFoundException {
		return packetsStore.getPacket(packetID).readDouble(prop);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsense.bacnet.network.media.Media#readInt(int,
	 * com.synapsense.bacnet.network.jni.Property)
	 */
	@Override
	public int readInt(int packetID, Property prop) throws PropertyNotFoundException {
		return packetsStore.getPacket(packetID).readInteger(prop);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsense.bacnet.network.media.Media#readSRC(int)
	 */
	@Override
	public byte[] readSRC(int packetID) {
		try {
			return readByteArray(packetID, Property.SRC_MAC);
		} catch (PropertyNotFoundException e) {
			return null;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsense.bacnet.network.media.Media#readShort(int,
	 * com.synapsense.bacnet.network.jni.Property)
	 */
	@Override
	public short readShort(int packetID, Property prop) throws PropertyNotFoundException {
		return packetsStore.getPacket(packetID).readShort(prop);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsense.bacnet.network.media.Media#readShortArray(int,
	 * com.synapsense.bacnet.network.jni.Property)
	 */
	@Override
	public short[] readShortArray(int packetID, Property prop) throws PropertyNotFoundException {
		return packetsStore.getPacket(packetID).readShortArray(prop);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsense.bacnet.network.media.Media#readShortArrayAsInt(int,
	 * com.synapsense.bacnet.network.jni.Property)
	 */
	@Override
	public int[] readShortArrayAsInt(int packetID, Property prop) throws PropertyNotFoundException {
		short array[] = readShortArray(packetID, prop);
		int ret[] = new int[array.length];
		for (int i = 0; i < array.length; i++) {
			ret[i] = array[i] & 0xFFFF;
		}
		return ret;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsense.bacnet.network.media.Media#readString(int,
	 * com.synapsense.bacnet.network.jni.Property)
	 */
	@Override
	public String readString(int packetID, Property prop) throws PropertyNotFoundException {
		return packetsStore.getPacket(packetID).readString(prop);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsense.bacnet.network.media.Media#readStringArray(int,
	 * com.synapsense.bacnet.network.jni.Property)
	 */
	@Override
	public String[] readStringArray(int packetID, Property prop) throws PropertyNotFoundException {
		return packetsStore.getPacket(packetID).readStringArray(prop);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsense.bacnet.network.media.Media#setBoolean(int,
	 * com.synapsense.bacnet.network.jni.Property, boolean)
	 */
	@Override
	public void setBoolean(int packetID, Property prop, boolean value) {
		packetsStore.getPacket(packetID).writeBoolean(prop, value);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsense.bacnet.network.media.Media#setByte(int,
	 * com.synapsense.bacnet.network.jni.Property, byte)
	 */
	@Override
	public void setByte(int packetID, Property prop, byte value) {
		packetsStore.getPacket(packetID).writeByte(prop, value);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsense.bacnet.network.media.Media#setByteArray(int,
	 * com.synapsense.bacnet.network.jni.Property, byte[])
	 */
	@Override
	public void setByteArray(int packetID, Property prop, byte[] value) {
		packetsStore.getPacket(packetID).writeByteArray(prop, value);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsense.bacnet.network.media.Media#setDouble(int,
	 * com.synapsense.bacnet.network.jni.Property, double)
	 */
	@Override
	public void setDouble(int packetID, Property prop, double value) {
		packetsStore.getPacket(packetID).writeDouble(prop, value);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsense.bacnet.network.media.Media#setInt(int,
	 * com.synapsense.bacnet.network.jni.Property, int)
	 */
	@Override
	public void setInt(int packetID, Property prop, int value) {
		packetsStore.getPacket(packetID).writeInteger(prop, value);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsense.bacnet.network.media.Media#setShort(int,
	 * com.synapsense.bacnet.network.jni.Property, short)
	 */
	@Override
	public void setShort(int packetID, Property prop, short value) {
		packetsStore.getPacket(packetID).writeShort(prop, value);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsense.bacnet.network.media.Media#setShortArrayAsInt(int,
	 * com.synapsense.bacnet.network.jni.Property, int[])
	 */
	@Override
	public void setShortArrayAsInt(int packetID, Property prop, int[] value) {
		short array[] = new short[value.length];
		for (int i = 0; i < value.length; i++) {
			array[i] = (short) value[i];
		}
		packetsStore.getPacket(packetID).writeShortArray(prop, array);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsense.bacnet.network.media.Media#setString(int,
	 * com.synapsense.bacnet.network.jni.Property, java.lang.String)
	 */
	@Override
	public void setString(int packetID, Property prop, String value) {
		packetsStore.getPacket(packetID).writeString(prop, value);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsense.bacnet.network.media.Media#setStringArray(int,
	 * com.synapsense.bacnet.network.jni.Property, java.lang.String[])
	 */
	@Override
	public void setStringArray(int packetID, Property prop, String[] value) {
		packetsStore.getPacket(packetID).writeStringArray(prop, value);
	}

	public void startCapture(int interfaceIndex, boolean isDebug) throws UnableToSetUpInterfaceException {
		this.interfaceIndex = interfaceIndex;
		MAC = getMediaAddress(interfaceIndex);
		if (MAC == null) {
			throw new UnableToSetUpInterfaceException("Could not start capture from interface with index="
			        + interfaceIndex);
		} else {
			MediaManager.getMediaManager().registerMedia(MAC, this);
			try {
				ipReciever.startCapture(mediaInfo.get(interfaceIndex).getAddress(), isDebug);
			} catch (BindException e) {
				throw new UnableToSetUpInterfaceException(e.getMessage());
			} catch (SocketException e) {
				throw new UnableToSetUpInterfaceException(e.getMessage());
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsense.bacnet.network.media.Media#startRead()
	 */
	@Override
	public int startRead() {
		int packetId = -1;
		while (packetId == -1) {
			try {
				packetId = waitForPacket();
			} catch (MalformedPacketException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NotBVLLMessageException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// upon receiving packet id -1 MPDUHeader will throw
				// PropertyNotFound exception (since we didn't actually receive
				// a packet with id -1) and this will unblock the waiting thread
				return packetId;
			}

		}
		return packetId;
	}

	public int waitForPacket() throws IOException, MalformedPacketException, NotBVLLMessageException {
		DatagramPacket packet = ipReciever.getPacket();
		int packetId = createPacket();
		DataPacket dp = packetsStore.getPacket(packetId);
		SocketAddress src = packet.getSocketAddress();
		if (src instanceof InetSocketAddress) {
			InetSocketAddress fromAddress = (InetSocketAddress) src;
			InetAddress inetAddress = fromAddress.getAddress();
			if (inetAddress.equals(mediaInfo.get(interfaceIndex).getAddress())) {
				logger.debug("Skipping loopback packet");
				return -1;
			}
			byte[] rawIp = inetAddress.getAddress();
			int port = fromAddress.getPort();
			int ipLength = rawIp.length;
			byte[] srcMac = Arrays.copyOf(rawIp, ipLength + 2);
			srcMac[ipLength] = (byte) (port >>> 8);
			srcMac[ipLength + 1] = (byte) port;
			byte[] packetData = Arrays.copyOf(packet.getData(), packet.getLength());
			logger.debug("Captured packet from: [" + inetAddress.getHostAddress() + ":" + port + "]");
			logger.debug("packet data : \n" + Arrays.toString(packetData));
			dp.writeByteArray(Property.SRC_MAC, srcMac);
			dp.writeByteArray(Property.DST_MAC, MAC);
			ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(packetData, 0, packetData.length);
			DataInputStream stream = new DataInputStream(byteArrayInputStream);
			IPHeader.read(stream, dp);
			return packetId;
		} else {
			throw new MalformedPacketException("Source address cannot be determined.");
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsense.bacnet.network.media.Media#sendPacket(int)
	 */
	@Override
	public void sendPacket(int packetID) throws UnableToSendException, PropertyNotFoundException,
	        MalformedPacketException, UnableToSetUpInterfaceException {
		DataPacket packet = packetsStore.getPacket(packetID);
		try {
			byte[] encodedPacket = IPHeader.write(packet);
			ipReciever.sendPacket(encodedPacket, packet);
		} catch (Exception e) {
			throw new UnableToSendException(e.getMessage());
		} finally {
			packetsStore.removePacket(packetID);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsense.bacnet.network.media.Media#writeDST(int,
	 * java.lang.String)
	 */
	@Override
	public void writeDST(int packetID, byte[] DST_ADDR) {
		if (DST_ADDR != null) {
			setByteArray(packetID, Property.DST_MAC, DST_ADDR);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsense.bacnet.network.media.Media#writeSRC(int,
	 * java.lang.String)
	 */
	@Override
	public void writeSRC(int packetID, byte[] SRC_ADDR) {
		if (SRC_ADDR != null) {
			setByteArray(packetID, Property.SRC_MAC, SRC_ADDR);
		}

	}

	@Override
	public void stopCapture() {
		ipReciever.stopCapture();
		packetsStore.clear();
	}

	/**
	 * @return the interfaceIndex
	 */
	public int getInterfaceIndex() {
		return interfaceIndex;
	}

	public DataPacket getPacket(int id) {
		return packetsStore.getPacket(id);
	}

	@Override
	public void setLogConfigPath(String string) {
		// Do nothing
	}

	private static byte[] getDestinationMac(String destination) {
		byte[] result = null;
		String ip = destination;
		if (ip != null) {
			byte[] portBytes = new byte[] { (byte) 0xBA, (byte) 0xC0 };
			if (ip.indexOf(":") != -1) {
				int port = Integer.parseInt(ip.substring(ip.indexOf(":") + 1));
				portBytes[0] = (byte) (port >> 8);
				portBytes[1] = (byte) port;
				ip = ip.substring(0, ip.indexOf(":"));
			}

			String[] splitIp = ip.split("\\.");
			if (splitIp.length != 4) {
				try {
					InetAddress inet = InetAddress.getByName(ip);
					splitIp = inet.getHostAddress().split("\\.");
				} catch (UnknownHostException e) {
				}
			}
			if (splitIp.length == 4) {
				result = new byte[splitIp.length + 2];
				for (int i = 0; i < splitIp.length; i++) {
					result[i] = (byte) Integer.parseInt(splitIp[i]);
				}
				result[result.length - 2] = portBytes[0];
				result[result.length - 1] = portBytes[1];
			}
		}

		return result;
	}

}
