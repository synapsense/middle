package com.synapsense.bacnet.network.media.IP.parcer;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import com.synapsense.bacnet.network.jni.MalformedPacketException;
import com.synapsense.bacnet.network.jni.Property;
import com.synapsense.bacnet.network.jni.PropertyNotFoundException;
import com.synapsense.bacnet.network.media.IP.DataPacket;
import com.synapsense.bacnet.network.pdu.npdu.NPDUMessageType;

/*
 *  Implementation of AbstractParcer, which processes parsing 
 * BACnet-I-Could-Be-Router-to-Network NPDU part of packet.
 *  <br>
 *  \code
 * | DNET | PERF_INDEX |
 * |   2  |      1     |
 * |   M  |      M
 *  \endcode
 *
 *
 */
public class ICouldBeRouterToNetworkParcer extends AbstractParcer {

	@Override
	public void read(DataInputStream stream, DataPacket packet) throws MalformedPacketException, IOException {
		logger.debug("Reading BACnet I-Could-Be-Router-to-Network...");
		if (stream.available() != 3) {
			throw new MalformedPacketException();
		}
		packet.writeByte(Property.MESSAGE_TYPE, NPDUMessageType.I_Could_Be_Router_To_Network.getCode());
		packet.writeShort(Property.NDNET, stream.readUnsignedShort());
		packet.writeByte(Property.PERF_INDEX, stream.readUnsignedByte());
	}

	@Override
	public void write(DataOutputStream stream, DataPacket packet) throws IOException, PropertyNotFoundException {
		logger.debug("Writting BACnet I-Could-Be-Router-to-Network...");
		stream.writeByte(NPDUMessageType.I_Could_Be_Router_To_Network.getCode());
		stream.writeShort(packet.readShort(Property.NDNET));
		stream.writeByte(packet.readShort(Property.PERF_INDEX));
	}

}
