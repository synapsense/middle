package com.synapsense.bacnet.network.media.IP.parcer;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import com.synapsense.bacnet.network.jni.MalformedPacketException;
import com.synapsense.bacnet.network.jni.Property;
import com.synapsense.bacnet.network.jni.PropertyNotFoundException;
import com.synapsense.bacnet.network.media.IP.DataPacket;
import com.synapsense.bacnet.network.pdu.apdu.APDUMessageType;

/*
 *  Implementation of AbstractParcer, which processes parsing BACnet-Error APDU
 *  part of packet.
 *  <br>
 *  \code
 *   7   6   5   4   3   2   1   0
 * |---|---|---|---|---|---|---|---|
 * |    PDU Type   | 0 | 0 | 0 | 0 |
 * |---|---|---|---|---|---|---|---|
 * |      Original Invoke ID       |
 * |---|---|---|---|---|---|---|---|
 * |          Error Choice         |
 * |---|---|---|---|---|---|---|---|
 * |             Error             |
 * |               .               |
 * |               .               |
 * |---|---|---|---|---|---|---|---|
 *  \endcode
 */
public class ErrorParcer extends AbstractParcer {

	@Override
	public void read(DataInputStream stream, DataPacket packet) throws MalformedPacketException, IOException {
		logger.debug("Reading BACnet Error-PDU...");
		if (stream.available() < 2) {
			throw new MalformedPacketException();
		}
		if ((getUnparcedMessageType() & 0x0F) != 0) {
			throw new MalformedPacketException();
		}
		packet.writeByte(Property.MESSAGE_TYPE, APDUMessageType.error_PDU.getType());
		packet.writeByte(Property.INVOKE_ID, stream.readUnsignedByte());
		packet.writeByte(Property.SERV_CHOICE, stream.readUnsignedByte());
		int size = stream.available();
		byte[] payload = new byte[size];
		stream.read(payload, 0, size);
		packet.writeByteArray(Property.PDU_PAYLOAD, payload);
	}

	@Override
	public void write(DataOutputStream stream, DataPacket packet) throws PropertyNotFoundException, IOException {
		logger.debug("Writting BACnet Error-PDU...");

		int messageType = packet.readByte(Property.MESSAGE_TYPE) << 4;

		stream.writeByte(messageType);
		stream.writeByte(packet.readByte(Property.INVOKE_ID));
		stream.writeByte(packet.readByte(Property.SERV_CHOICE));
		stream.write(packet.readByteArray(Property.PDU_PAYLOAD));
	}

}
