package com.synapsense.bacnet.network.media.IP;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.net.SocketException;
import java.util.Arrays;

import org.apache.log4j.Logger;

import com.synapsense.bacnet.network.jni.Property;
import com.synapsense.bacnet.network.jni.PropertyNotFoundException;
import com.synapsense.bacnet.network.jni.UnableToSendException;
import com.synapsense.bacnet.network.jni.UnableToSetUpInterfaceException;

public class IpPacketReceiver {
	protected final Logger logger = Logger.getLogger(this.getClass());

	public static final int DEFAULT_BACNET_PORT = 47808;
	public static final int DEFAULT_APDU_LENGHT = 1476;
	private int port = Integer.parseInt(System.getProperty("com.synapsense.bacnet.port", "47808"));
	private int apduLength = DEFAULT_APDU_LENGHT;
	DatagramSocket socket;
	private boolean started = false;

	public IpPacketReceiver() {

	}

	public void setIPPort(int port) {
		this.port = port;
	}

	public DatagramPacket getPacket() throws IOException {
		DatagramPacket packet = new DatagramPacket(new byte[apduLength], apduLength);
		socket.receive(packet);
		return packet;
	}

	public void startCapture(InetAddress boundAddress, boolean allowOwnSrcMac) throws SocketException {
		if (!started) {
			socket = new DatagramSocket(port, boundAddress);
			socket.setBroadcast(true);
			started = true;
		}
	}

	public void stopCapture() {
		if (started) {
			socket.close();
			started = false;
		}
	}

	public void sendPacket(byte[] packetBuffer, DataPacket dataPacket) throws UnableToSetUpInterfaceException,
	        IOException, UnableToSendException {
		if (socket == null || !socket.isBound()) {
			throw new UnableToSendException("Unable to send a packet, socket is not bond.");
		}
		DatagramPacket datagramPacket = new DatagramPacket(packetBuffer, packetBuffer.length);
		InetAddress inetAddress;
		try {
			inetAddress = InetAddress.getByAddress(Arrays.copyOf(dataPacket.readByteArray(Property.DST_MAC), 4));
		} catch (PropertyNotFoundException e) {
			throw new UnableToSendException("Unable to send packet, Destination is not specified.");
		}
		SocketAddress address = new InetSocketAddress(inetAddress, port);
		datagramPacket.setSocketAddress(address);
		socket.send(datagramPacket);
	}
}
