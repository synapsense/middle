package com.synapsense.bacnet.network.media.IP.parcer;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import com.synapsense.bacnet.network.jni.MalformedPacketException;
import com.synapsense.bacnet.network.jni.Property;
import com.synapsense.bacnet.network.jni.PropertyNotFoundException;
import com.synapsense.bacnet.network.media.IP.DataPacket;
import com.synapsense.bacnet.network.pdu.apdu.APDUMessageType;

/*
 *  Implementation of AbstractParcer, which processes parsing BACnet-Complex-ACK
 *  APDU part of packet.
 *  <br>
 *  \code
 *   7   6   5   4   3   2   1   0
 * |---|---|---|---|---|---|---|---|
 * | PDU Type      |SEG|MOR| 0 | 0 |
 * |---|---|---|---|---|---|---|---|
 * |      Original Invoke ID       |
 * |---|---|---|---|---|---|---|---|
 * |         Sequence Number       | Only present if SEG = 1
 * |---|---|---|---|---|---|---|---|
 * |      Proposed Window Size     | Only present if SEG = 1
 * |---|---|---|---|---|---|---|---|
 * |       Service ACK Choice      |
 * |---|---|---|---|---|---|---|---|
 * |          Service ACK          |
 * |               .               |
 * |               .               |
 * |---|---|---|---|---|---|---|---|
 *  \endcode
 */
public class ComplexAckParcer extends AbstractParcer {

	@Override
	public void read(DataInputStream stream, DataPacket packet) throws MalformedPacketException, IOException {
		logger.debug("Reading BACnet Complex-ACK-PDU...");

		if (stream.available() < 1 || (getUnparcedMessageType() & 0x03) != 0) {
			throw new MalformedPacketException();
		}
		packet.writeByte(Property.MESSAGE_TYPE, APDUMessageType.complexACK_PDU.getType());
		boolean segNumPresented = (getUnparcedMessageType() & 0x08) != 0;

		packet.writeBoolean(Property.IS_SEG_REQUEST, segNumPresented);
		packet.writeBoolean(Property.MORE_SEG_FOLLOW, (getUnparcedMessageType() & 0x04) != 0);
		packet.writeByte(Property.INVOKE_ID, stream.readUnsignedByte());

		if (segNumPresented) {
			if (stream.available() < 2) {
				throw new MalformedPacketException();
			}
			packet.writeByte(Property.SEQUENCE_NUMBER, stream.readUnsignedByte());
			packet.writeByte(Property.WINDOW_SIZE, stream.readUnsignedByte());
		}
		if (stream.available() < 1) {
			throw new MalformedPacketException();
		}

		packet.writeByte(Property.SERV_CHOICE, stream.readUnsignedByte());
		int payloadSize = stream.available();
		byte[] payload = new byte[payloadSize];
		stream.read(payload, 0, payloadSize);
		packet.writeByteArray(Property.PDU_PAYLOAD, payload);
	}

	@Override
	public void write(DataOutputStream stream, DataPacket packet) throws PropertyNotFoundException, IOException {
		logger.debug("Writting BACnet Complex-ACK-PDU...");

		int messageType = packet.readByte(Property.MESSAGE_TYPE) << 4;
		boolean segNumPresent = packet.readBoolean(Property.IS_SEG_REQUEST);
		if (segNumPresent) {
			messageType |= 0x08;
		}
		if (packet.readBoolean(Property.MORE_SEG_FOLLOW)) {
			messageType |= 0x04;
		}
		stream.writeByte(messageType);
		stream.writeByte(packet.readByte(Property.INVOKE_ID));
		if (segNumPresent) {
			stream.writeByte(packet.readByte(Property.SEQUENCE_NUMBER));
			stream.writeByte(packet.readByte(Property.WINDOW_SIZE));
		}
		stream.writeByte(packet.readByte(Property.SERV_CHOICE));
		stream.write(packet.readByteArray(Property.PDU_PAYLOAD));
	}

}
