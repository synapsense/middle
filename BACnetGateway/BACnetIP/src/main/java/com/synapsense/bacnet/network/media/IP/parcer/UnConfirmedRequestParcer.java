package com.synapsense.bacnet.network.media.IP.parcer;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import org.apache.log4j.Logger;

import com.synapsense.bacnet.network.jni.MalformedPacketException;
import com.synapsense.bacnet.network.jni.Property;
import com.synapsense.bacnet.network.jni.PropertyNotFoundException;
import com.synapsense.bacnet.network.media.IP.DataPacket;
import com.synapsense.bacnet.network.pdu.apdu.APDUMessageType;

public class UnConfirmedRequestParcer extends AbstractParcer {
	static Logger logger = Logger.getLogger(UnConfirmedRequestParcer.class);

	@Override
	public void read(DataInputStream stream, DataPacket packet) throws MalformedPacketException, IOException {

		logger.debug("Reading BACnet Unconfirmed-Request-PDU...");
		if (stream.available() < 1) {
			throw new MalformedPacketException();
		}

		if ((getUnparcedMessageType() & 0x0F) != 0) {
			throw new MalformedPacketException();
		}

		packet.writeByte(Property.MESSAGE_TYPE, APDUMessageType.unconfirmed_request_PDU.getType());
		Integer serviceChoice = stream.readUnsignedByte();
		packet.writeByte(Property.SERV_CHOICE, serviceChoice.byteValue());
		int payloadSize = stream.available();
		byte[] payload = new byte[payloadSize];
		stream.read(payload, 0, payloadSize);

		packet.writeByteArray(Property.PDU_PAYLOAD, payload);
	}

	@Override
	public void write(DataOutputStream stream, DataPacket packet) throws PropertyNotFoundException, IOException {
		logger.debug("Writting BACnet Unconfirmed-Request-PDU...");
		byte messageType = packet.readByte(Property.MESSAGE_TYPE);
		messageType <<= 4;
		stream.write(messageType);
		stream.write(packet.readByte(Property.SERV_CHOICE));
		byte[] pduPayload = packet.readByteArray(Property.PDU_PAYLOAD);
		stream.write(pduPayload);

	}
}
