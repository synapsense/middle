package com.synapsense.bacnet.network.media.IP.parcer;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import com.synapsense.bacnet.network.jni.MalformedPacketException;
import com.synapsense.bacnet.network.jni.Property;
import com.synapsense.bacnet.network.jni.PropertyNotFoundException;
import com.synapsense.bacnet.network.media.IP.DataPacket;
import com.synapsense.bacnet.network.pdu.apdu.APDUMessageType;

/*
 * Implementation of simple_packet, which processes parsing BACnet-Segmented-ACK
 *  APDU part of packet.
 *  <br>
 *  \code
 *   7   6   5   4   3   2   1   0
 * |---|---|---|---|---|---|---|---|
 * | PDU Type      | 0 | 0 |NAK|SRV|
 * |---|---|---|---|---|---|---|---|
 * |      Original Invoke ID       |
 * |---|---|---|---|---|---|---|---|
 * |         Sequence Number       |
 * |---|---|---|---|---|---|---|---|
 * |       Actual Window Size      |
 * |---|---|---|---|---|---|---|---|
 *  \endcode
 */
public class SegmentedAckParcer extends AbstractParcer {

	@Override
	public void read(DataInputStream stream, DataPacket packet) throws MalformedPacketException, IOException {
		logger.debug("Reading BACnet Simple-ACK-PDU...");

		if (stream.available() < 3 || (getUnparcedMessageType() & 0x0C) != 0) {
			throw new MalformedPacketException();
		}
		packet.writeByte(Property.MESSAGE_TYPE, APDUMessageType.segmentAck_PDU.getType());
		packet.writeBoolean(Property.ACK_OUT_OF_ORDER, (getUnparcedMessageType() & 0x02) != 0);
		packet.writeBoolean(Property.ACK_SERVER, (getUnparcedMessageType() & 0x01) != 0);

		packet.writeByte(Property.INVOKE_ID, stream.readUnsignedByte());
		packet.writeByte(Property.SEQUENCE_NUMBER, stream.readUnsignedByte());
		packet.writeByte(Property.WINDOW_SIZE, stream.readUnsignedByte());
	}

	@Override
	public void write(DataOutputStream stream, DataPacket packet) throws PropertyNotFoundException, IOException {
		logger.debug("Writting BACnet Complex-ACK-PDU...");

		int messageType = packet.readByte(Property.MESSAGE_TYPE) << 4;

		if (packet.readBoolean(Property.ACK_OUT_OF_ORDER)) {
			messageType |= 0x02;
		}

		if (packet.readBoolean(Property.ACK_SERVER)) {
			messageType |= 0x01;
		}
		stream.writeByte(messageType);

		stream.writeByte(packet.readByte(Property.INVOKE_ID));
		stream.writeByte(packet.readByte(Property.SEQUENCE_NUMBER));
		stream.writeByte(packet.readByte(Property.WINDOW_SIZE));
	}

}
