package com.synapsense.bacnet.network.media.IP.parcer;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import org.apache.log4j.Logger;

import com.synapsense.bacnet.network.jni.MalformedPacketException;
import com.synapsense.bacnet.network.jni.NotBVLLMessageException;
import com.synapsense.bacnet.network.jni.Property;
import com.synapsense.bacnet.network.jni.PropertyNotFoundException;
import com.synapsense.bacnet.network.media.IP.DataPacket;

/*
 BVLL message format
 |DSAP|FUNC|LEN|message
 |  1 |  1 | 2 | ..
 |  M |  M | M | M 
 */
public class IPHeader {

	public static final int MIN_BVLL_MESSAGE_SIZE = 4;
	public static final int BVLL_DSAP = 0x81;

	static Logger logger = Logger.getLogger(IPHeader.class);

	public static void read(DataInputStream stream, DataPacket packet) throws MalformedPacketException,
	        NotBVLLMessageException, IOException {
		int bvllLen;
		logger.debug("Reading IP Header...");
		int bvlc = stream.readUnsignedByte();
		if (bvlc == BVLL_DSAP) {
			logger.debug("BVLC Type = 0x" + Integer.toHexString(bvlc));
			// check if a packet is larger then the minimal packet lengh
			if (stream.available() < MIN_BVLL_MESSAGE_SIZE) {
				throw new NotBVLLMessageException();
			}

			// Get a function code
			Integer func = stream.readUnsignedByte();
			logger.debug("BVLC Function = " + func);

			// Get length of the BVLL message
			short npduLen = stream.readShort();
			logger.debug("BVLL Length = " + npduLen);

			// we've red 4 bytes therefore we need to add them to the packet
			// size
			if (npduLen > stream.available() + 4) {
				throw new MalformedPacketException("Announced packet lenght is bigger than the actual lenght.");
			}

			bvllLen = MIN_BVLL_MESSAGE_SIZE;
			int countBDTEntries = 0;
			switch (func) {
			case BVLCCodes.RESULT:
				/*
				 * Result Code: 2-octets X'0000' Successful completion X'0010
				 * Write-Broadcast-Distribution-Table NAK X'0020'
				 * Read-Broadcast-Distribution-Table NAK X'0030'
				 * Register-Foreign-Device NAK X'0040' Read-Foreign-Device-Table
				 * NAK X'0050' Delete-Foreign-Device-Table-Entry NAK X'0060'
				 * Distribute-Broadcast-To-Network NAK
				 */
				short result = stream.readShort();
				logger.debug("RESULT = " + result);
				bvllLen += 2;
				throw new MalformedPacketException("");
			case BVLCCodes.WRITE_BROADCAST_DISTRIBUTION_TABLE:

			case BVLCCodes.READ_BROADCAST_DISTRIBUTION_TABLE_ACK:
				// List of BDT Entries: N*10-octets
				logger.info("Broadcast distribution table packet is recieved.");
				countBDTEntries = (npduLen - bvllLen) / 10;
				StringBuffer sb = new StringBuffer();
				for (int i = 0; i < countBDTEntries; i++) {
					// BDT Entry = xxx.xxx.xxx.xxx:xxxx
					sb.append("BDT Entry = ");
					sb.append(stream.readShort());
					sb.append(".");
					sb.append(stream.readShort());
					sb.append(".");
					sb.append(stream.readShort());
					sb.append(".");
					sb.append(stream.readShort());
					sb.append(":");
					sb.append(Integer.toHexString(stream.readShort()));
					sb.append(Integer.toHexString(stream.readShort()));
					sb.append("\n");
					sb.append("Distribution Mask = ");
					sb.append(Integer.toHexString(stream.readShort()));
					sb.append(Integer.toHexString(stream.readShort()));
					sb.append(Integer.toHexString(stream.readShort()));
					sb.append(Integer.toHexString(stream.readShort()));
					sb.append("\n");
					bvllLen += 10;
				}
				logger.info(sb.toString());
				throw new MalformedPacketException("");
			case BVLCCodes.READ_BROADCAST_DISTRIBUTION_TABLE:
				// Do nothing
				throw new MalformedPacketException("");
			case BVLCCodes.FORWARDED_NPDU:
				logger.info("Forwarded NPDU packet is recieved.");
				logger.info("B/IP Address of Originating Device = " + stream.readShort() + "." + stream.readShort()
				        + "." + stream.readShort() + "." + stream.readShort() + ":"
				        + Integer.toHexString(stream.readShort()) + Integer.toHexString(stream.readShort()));
				bvllLen += 6;
				throw new MalformedPacketException("");
			case BVLCCodes.REGISTER_FOREIGN_DEVICE:
				logger.info("Register foreign device packet is recieved.");
				logger.info("Time-to-Live(sec) = " + stream.readShort());
				bvllLen += 6;
				throw new MalformedPacketException("");
			case BVLCCodes.READ_FOREIGN_DEVICE_TABLE:
				// Do nothing
				throw new MalformedPacketException("");
			case BVLCCodes.READ_FOREIGN_DEVICE_TABLE_ACK:
				logger.info("Read foreign device table acknowlegment packet is recieved.");
				/*
				 * List of BDT Entries: N*10-octets: 6-octet B/IP address of the
				 * registrant; 2-octet Time-to-Live value supplied at the time
				 * of registration; 2-octet value representing the number of
				 * seconds remaining before the BBMD will purge the registrant's
				 * FDT entry if no re-registration occurs.
				 */
				countBDTEntries = (npduLen - bvllLen) / 10;
				for (int i = 0; i < countBDTEntries; i++) {
					logger.info("B/IP address of the registrant = " + stream.readShort() + "." + stream.readShort()
					        + "." + stream.readShort() + "." + stream.readShort() + ":"
					        + Integer.toHexString(stream.readShort()) + Integer.toHexString(stream.readShort()));
					bvllLen += 6;
					logger.info("Time-to-Live(sec) = " + stream.readShort());
					bvllLen += 2;
					logger.info("Remaining second = " + stream.readShort());
					bvllLen += 2;
				}

				throw new MalformedPacketException("");
			case BVLCCodes.DELETE_FOREIGN_DEVICE_TABLE_ENTRY:
				// FDT Entry: 6-octets
				logger.info("FDT Entry = " + stream.readShort() + "." + stream.readShort() + "." + stream.readShort()
				        + "." + stream.readShort() + ":" + Integer.toHexString(stream.readShort())
				        + Integer.toHexString(stream.readShort()));
				bvllLen += 6;

				throw new MalformedPacketException("");
			case BVLCCodes.DISTRIBUTE_BROADCAST_TO_NETWORK:
				// Do nothing
				throw new MalformedPacketException("");
			case BVLCCodes.ORIGINAL_BROADCAST_NPDU:
			case BVLCCodes.ORIGINAL_UNICAST_NPDU:
				packet.writeByte(Property.BVLC, func.byteValue());
				break;
			default:
				throw new MalformedPacketException("");
			}
			NPDU npdu = new NPDU();
			npdu.read(stream, packet);
			stream.close();
		}
	}

	public static byte[] write(DataPacket packet) throws IOException, PropertyNotFoundException {
		NPDU npdu = new NPDU();
		ByteArrayOutputStream npduOutputStream = new ByteArrayOutputStream();
		DataOutputStream dataOutputStream = new DataOutputStream(npduOutputStream);
		npdu.write(dataOutputStream, packet);
		byte[] npduBytes = npduOutputStream.toByteArray();
		byte[] bvllLen = shortToByteArray((short) (MIN_BVLL_MESSAGE_SIZE + npduBytes.length));
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		// Set BVLC Type 0x81
		outputStream.write(BVLL_DSAP);
		// Set a function code
		outputStream.write(packet.readByte(Property.BVLC));
		// Set bvll length
		outputStream.write(bvllLen);
		// write NPDU part
		outputStream.write(npduBytes);
		outputStream.flush();
		return outputStream.toByteArray();
	}

	public static byte[] shortToByteArray(short value) {
		return new byte[] { (byte) (value >>> 8), (byte) value };
	}
}
