package com.synapsense.bacnet.network.media.IP.parcer;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Formatter;

import org.apache.log4j.Logger;

import com.synapsense.bacnet.network.jni.MalformedPacketException;
import com.synapsense.bacnet.network.jni.Property;
import com.synapsense.bacnet.network.jni.PropertyNotFoundException;
import com.synapsense.bacnet.network.media.IP.DataPacket;
import com.synapsense.bacnet.network.pdu.apdu.APDUMessageType;
import com.synapsense.bacnet.network.pdu.npdu.NPDUMessageType;

public class NPDU {
	static Logger logger = Logger.getLogger(NPDU.class);

	public void read(DataInputStream stream, DataPacket packet) throws MalformedPacketException, IOException {
		logger.debug("Reading BACnet NPCI header...");

		if (stream.available() < 2) {
			throw new MalformedPacketException("");
		}
		// VERSION field should be 0x01
		logger.debug("Version = " + stream.readUnsignedByte());
		int control = stream.readUnsignedByte();

		logger.debug("Control = " + Integer.toBinaryString(control));
		printControl(control);

		// here we parsing control byte:
		// | NLM | 0 | DST_SPEC | 0 | SRC_SPEC | DATA_EXP_REPLY | PRIORITY |
		// | 1 | 1 | 1 | 1 | 1 | 1 | 2 |
		boolean isApdu = ((control & 0x80) == 0);
		packet.writeBoolean(Property.IS_APDU, isApdu);

		boolean hasDaddrs = ((control & 0x20) != 0);
		boolean hasSaddrs = ((control & 0x08) != 0);

		boolean dataExpectingReply = ((control & 0x04) != 0);
		packet.writeBoolean(Property.DATA_EXPECTING_REPLY, dataExpectingReply);

		byte priority = (byte) (control & 0x03);
		packet.writeByte(Property.PRIORITY, priority);

		// now start parsing optional fields

		if (hasDaddrs) {
			if (stream.available() < 3) {
				throw new MalformedPacketException();
			}
			Integer dnet = stream.readUnsignedShort();
			logger.debug("DNET = " + dnet);
			packet.writeShort(Property.DNET, dnet.shortValue());
			Integer dlen = stream.readUnsignedByte();
			logger.debug("DLEN = " + dlen);
			packet.writeByte(Property.DLEN, dlen.byteValue());
			if (dlen > 0) {
				if (stream.available() < dlen) {
					throw new MalformedPacketException();
				}
				byte[] address = new byte[dlen];
				stream.read(address, 0, dlen);
				StringBuffer sb = new StringBuffer();
				sb.append("DADR = ");
				if (logger.isDebugEnabled()) {
					Formatter f = new Formatter();
					for (int i = 0; i < dlen; i++) {
						f.format("%02X%s", address[i], (i < dlen - 1) ? "-" : "");
					}
					sb.append(f.out());
					f.close();
					sb.append("\n");
					logger.debug(sb);
				}
				packet.writeByteArray(Property.DADR, address);
			}
		}

		if (hasSaddrs) {
			if (stream.available() < 3) {
				throw new MalformedPacketException();
			}
			Integer snet = stream.readUnsignedShort();
			logger.debug("SNET = " + snet);
			packet.writeShort(Property.SNET, snet.shortValue());
			Integer slen = stream.readUnsignedByte();
			logger.debug("SLEN = " + slen);
			packet.writeByte(Property.SLEN, slen.byteValue());
			if (slen > 0) {
				if (stream.available() < slen) {
					throw new MalformedPacketException();
				}

				byte[] address = new byte[slen];
				stream.read(address, 0, slen);
				StringBuffer sb = new StringBuffer();
				sb.append("SADR = ");
				if (logger.isDebugEnabled()) {
					Formatter f = new Formatter();
					for (int i = 0; i < slen; i++) {
						f.format("%02X%s", address[i], (i < slen - 1) ? "-" : "");
					}
					sb.append(f.out());
					f.close();
					sb.append("\n");
					logger.debug(sb);
				}

				packet.writeByteArray(Property.SADR, address);
			}

		}

		if (hasDaddrs) {
			if (stream.available() < 1) {
				throw new MalformedPacketException();
			}
			Integer hops = stream.readUnsignedByte();
			packet.writeByte(Property.HOPS, hops.byteValue());
			logger.debug("HOPS = " + hops);
		}

		if (stream.available() < 1) {
			throw new MalformedPacketException();
		}
		int messageType = stream.readUnsignedByte(); // APDU or NPDU message
		                                             // type. APDU
		// message sometimes contains
		// segmentation data
		int resultMessageType = messageType;
		if (!isApdu) // Network layer message. Message Type field is present.
		{
			if (messageType >= 0x80) // Got vendor specific message. Vendor ID
			                         // field is present
			{
				logger.debug("MessageType =0x" + Integer.toHexString(messageType));
				if (stream.available() > 1) {
					throw new MalformedPacketException();
				}
				Integer vendorId = stream.readUnsignedShort();
				packet.writeShort(Property.VENDOR_ID, vendorId.shortValue());
				logger.debug("Vendor ID = " + vendorId);
			}
		} else {
			// here we should select appreciate APDU or NPDU type and parse it
			// for APDU message type is stored in 4 high bits.
			resultMessageType = ((messageType & 0xF0) >> 4);

		}

		logger.debug("End reading BACnet NPCI header.");
		AbstractParcer parcer = getNestedPacket(isApdu, resultMessageType);
		parcer.setUnparcedMessageType((byte) (messageType & 0xFF));
		parcer.read(stream, packet);

	}

	public void write(DataOutputStream stream, DataPacket packet) throws PropertyNotFoundException, IOException {
		logger.debug("Writting BACnet NPCI header...");
		// version
		stream.writeByte(1);
		// Assembling control byte
		byte control = 0;
		boolean isApdu;
		isApdu = packet.readBoolean(Property.IS_APDU);
		if (!isApdu) {
			control |= 0x80;
		}
		// is packet expecting reply
		if (packet.readBoolean(Property.DATA_EXPECTING_REPLY)) {
			control |= 0x04;
		}
		// writing priority into control byte
		control |= (packet.readByte(Property.PRIORITY) & 0x03);
		boolean dstInfoPresent = true;
		byte dLen = 0, sLen = 0, hops = 0;
		short dNet = 0, sNet = 0;
		try {
			dNet = packet.readShort(Property.DNET);
			dLen = packet.readByte(Property.DLEN);
			hops = packet.readByte(Property.HOPS);
			control |= 0x20;

		} catch (PropertyNotFoundException e) {
			dstInfoPresent = false;
		}
		boolean srcInfoPresent = true;
		try {
			sNet = packet.readShort(Property.SNET);
			sLen = packet.readByte(Property.SLEN);
			control |= 0x08;

		} catch (PropertyNotFoundException e) {
			srcInfoPresent = false;
		}
		stream.writeByte(control);
		if (dstInfoPresent) {
			stream.writeShort(dNet);
			stream.writeByte(dLen);
			if (dLen > 0) {
				byte[] dadr = packet.readByteArray(Property.DADR);
				stream.write(dadr);
			}
		}
		if (srcInfoPresent) {
			stream.writeShort(sNet);
			stream.writeByte(sLen);
			if (sLen > 0) {
				byte[] sadr = packet.readByteArray(Property.SADR);
				stream.write(sadr);
			}
		}
		if (dstInfoPresent) {
			stream.writeByte(hops);
		}

		PacketParcer nestedPacket = getNestedPacket(isApdu, packet.readByte(Property.MESSAGE_TYPE));

		nestedPacket.write(stream, packet);
		printControl(control);

	}

	private void printControl(int control) {
		logger.debug("Start parsing control byte...");
		// Bit7
		if ((control & 0x80) == 0) {
			logger.debug("\t0... .... BACnet APDU");
		} else {
			logger.debug("\t1... .... BACnet Network Control Message");
		}
		// Bit6
		logger.debug("\t." + (control & 0x40) + ".. .... Reserved. Shall be zero");

		// Bit5
		if ((control & 0x20) == 0) {
			logger.debug("\t..0. .... DNET,DLEN,DADR, and HopCount are absent");
		} else {
			logger.debug("\t..1. .... DNET,DLEN,DADR, and HopCount are present");
		}

		// Bit4
		logger.debug("\t..." + (control & 0x10) + " .... Reserved. Shall be zero");

		// Bit3
		if ((control & 0x08) == 0) {
			logger.debug("\t.... 0... SNET,SLEN, and SADR are absent");
		} else {
			logger.debug("\t.... 1... SNET,SLEN, and SADR are present");
		}

		// Bit2
		logger.debug("\t.... ." + (control & 0x04) + ".. Data reply expected");

		// Bit1,0
		int priority = (control & 0x03);
		switch (priority) {
		case 0x11:
			logger.debug("\t.... ..11 Life Safety message");
			break;
		case 0x10:
			logger.debug("\t.... ..10 Critical Equipment message");
			break;
		case 0x01:
			logger.debug("\t.... ..01 Urgent message");
			break;
		case 0x00:
			logger.debug("\t.... ..00 Normal message");
			break;
		}
		logger.debug("End parsing control byte.");

	}

	private AbstractParcer getNestedPacket(boolean isApdu, int resultMessageType) {
		if (isApdu) {
			APDUMessageType apduMessageType = APDUMessageType.valueOf((byte) resultMessageType);
			switch (apduMessageType) {
			case confirmed_request_PDU:
				return new ConfirmedRequestParcer();
			case unconfirmed_request_PDU:
				return new UnConfirmedRequestParcer();
			case simpleACK_PDU:
				return new SimpleAckParcer();
			case complexACK_PDU:
				return new ComplexAckParcer();
			case segmentAck_PDU:
				return new SegmentedAckParcer();
			case error_PDU:
				return new ErrorParcer();
			case reject_PDU:
				return new RejectParcer();
			case abort_PDU:
				return new AbortParcer();
			}
		} else {
			NPDUMessageType npduMessageType = NPDUMessageType.valueOf(resultMessageType);
			switch (npduMessageType) {
			case Who_Is_Router_To_Network:
				return new WhoIsRouterToNetworkParcer();
			case I_Am_Router_To_Network:
				return new IAmRouterToNetworkParcer();
			case I_Could_Be_Router_To_Network:
				return new ICouldBeRouterToNetworkParcer();
			case Reject_Message_To_Network:
				return new RejectMessageToNetworkParcer();
			case Router_Busy_To_Network:
				return new RouterBusyToNetworkParcer();
			case Router_Available_To_Network:
				return new RouterAvailableToNetworkParcer();
			case Initialize_Routing_Table:
				return new InitializeRoutingTableParcer();
			case Initialize_Routing_Table_Ack:
				return new InitializeRoutingTableAckParcer();
			case Establish_Connection_To_Network:
				return new EstablishConnectionToNetworkParcer();
			case Disconnect_Connection_To_Network:
				return new DisconnectConnectionToNetworkParcer();
			}
		}

		return null;

	}
}
