package com.synapsense.bacnet.network.media.IP.parcer;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import com.synapsense.bacnet.network.jni.MalformedPacketException;
import com.synapsense.bacnet.network.jni.PropertyNotFoundException;
import com.synapsense.bacnet.network.media.IP.DataPacket;

public interface PacketParcer {

	public void read(DataInputStream stream, DataPacket packet) throws MalformedPacketException, IOException;

	public void write(DataOutputStream stream, DataPacket packet) throws PropertyNotFoundException, IOException;

}