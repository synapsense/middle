package com.synapsense.bacnet.network.media.IP.parcer;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import com.synapsense.bacnet.network.jni.MalformedPacketException;
import com.synapsense.bacnet.network.jni.Property;
import com.synapsense.bacnet.network.jni.PropertyNotFoundException;
import com.synapsense.bacnet.network.media.IP.DataPacket;
import com.synapsense.bacnet.network.pdu.npdu.NPDUMessageType;

/*
 *   Implementation of simple_packet, which processes parsing 
 *  BACnet-Reject-Message-to-Network NPDU part of packet.
 *  <br>
 *  \code
 * | REASON | DNET |
 * |    1   |   2  |
 * |    M   |   M  |
 *  \endcode
 *
 */

public class RejectMessageToNetworkParcer extends AbstractParcer {

	@Override
	public void read(DataInputStream stream, DataPacket packet) throws MalformedPacketException, IOException {
		logger.debug("Reading BACnet Reject-Message-to-Network...");
		if (stream.available() < 3) {
			throw new MalformedPacketException();
		}
		packet.writeByte(Property.MESSAGE_TYPE, NPDUMessageType.Reject_Message_To_Network.getCode());
		packet.writeByte(Property.REJECT_REASON, stream.readUnsignedByte());
		packet.writeShort(Property.NDNET, stream.readUnsignedShort());
	}

	@Override
	public void write(DataOutputStream stream, DataPacket packet) throws IOException, PropertyNotFoundException {
		logger.debug("Writting BACnet Reject-Message-to-Network...");

		stream.writeByte(NPDUMessageType.Reject_Message_To_Network.getCode());
		stream.writeByte(packet.readByte(Property.REJECT_REASON));
		stream.writeShort(packet.readShort(Property.NDNET));
	}

}
