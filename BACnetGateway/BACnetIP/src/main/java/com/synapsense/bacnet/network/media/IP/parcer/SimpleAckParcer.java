package com.synapsense.bacnet.network.media.IP.parcer;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import com.synapsense.bacnet.network.jni.MalformedPacketException;
import com.synapsense.bacnet.network.jni.Property;
import com.synapsense.bacnet.network.jni.PropertyNotFoundException;
import com.synapsense.bacnet.network.media.IP.DataPacket;
import com.synapsense.bacnet.network.pdu.apdu.APDUMessageType;

public class SimpleAckParcer extends AbstractParcer {

	@Override
	public void read(DataInputStream stream, DataPacket packet) throws MalformedPacketException, IOException {
		logger.debug("Reading BACnet Simple-ACK-PDU...");

		if (stream.available() < 2 || (getUnparcedMessageType() & 0x0F) != 0) {
			throw new MalformedPacketException();
		}
		packet.writeByte(Property.MESSAGE_TYPE, APDUMessageType.simpleACK_PDU.getType());
		packet.writeByte(Property.INVOKE_ID, stream.readUnsignedByte());
		packet.writeByte(Property.SERV_CHOICE, stream.readUnsignedByte());

	}

	@Override
	public void write(DataOutputStream stream, DataPacket packet) throws PropertyNotFoundException, IOException {
		logger.debug("Writting BACnet Simple-ACK-PDU...");

		stream.writeByte(packet.readByte(Property.MESSAGE_TYPE) << 4);
		stream.writeByte(packet.readByte(Property.INVOKE_ID));
		stream.writeByte(packet.readByte(Property.SERV_CHOICE));
	}

}
