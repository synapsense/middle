package com.synapsense.bacnet.network.media.IP.parcer;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Formatter;

import com.synapsense.bacnet.network.jni.MalformedPacketException;
import com.synapsense.bacnet.network.jni.Property;
import com.synapsense.bacnet.network.jni.PropertyNotFoundException;
import com.synapsense.bacnet.network.media.IP.DataPacket;

/*
 INITIALIZE_ROUTING_TABLE packet format:
 * | PORTS | DNET | PORT ID | PORT INFO LEN | PORT INFO | DNET ...
 * |   1   |   2  |    1    |       1       |    PIL    |   2 
 * |   M   |   O  |    O    |       O       |     O     |   O
 */
public class RoutingTablesAbstract extends AbstractParcer {
	protected final static int MAX_ROUTE_RECORD_LEN = 266;// maximum length of
	                                                      // routing record:
	                                                      // 5(dnet)+3(port_id)+255(port_info)+2(delimiters)
	                                                      // = 266bytes

	@Override
	public void read(DataInputStream stream, DataPacket packet) throws MalformedPacketException, IOException {
		if (stream.available() < 1) {
			throw new MalformedPacketException();
		}
		int ports = stream.readUnsignedByte();
		Formatter formatter = new Formatter();
		String[] stringArray = new String[ports];
		for (int i = 0; i < ports; i++) {
			int len = parceRoutingTableRecord(stream, formatter);
			if (len > 0) {
				stringArray[i] = formatter.toString();
				formatter.flush();
			} else {
				throw new MalformedPacketException();
			}
		}
		packet.writeStringArray(Property.ROUTING_TABLES, stringArray);
	}

	@Override
	public void write(DataOutputStream stream, DataPacket packet) throws PropertyNotFoundException, IOException {
		String[] routes = packet.readStringArray(Property.ROUTING_TABLES);
		int ports = routes.length;
		stream.writeByte(ports);

		for (int i = 0; i < ports; i++) {

			String[] s = routes[i].split("_");
			if (s.length >= 2) {
				stream.writeShort(Short.valueOf(s[0]));
				stream.writeByte(Byte.valueOf(s[1]));
				if (s.length == 3) {
					String portInfo = s[2];
					stream.writeByte(portInfo.length());
					stream.write(portInfo.getBytes("US-ASCII"));
				}
			}
		}
	}

	private int parceRoutingTableRecord(DataInputStream stream, Formatter formatter) throws IOException {

		if (stream.available() < 4) {
			return 0;
		}
		int connDnet = stream.readUnsignedShort();
		int portId = stream.readUnsignedByte();
		int pinfoLen = stream.readUnsignedByte();

		if (stream.available() < pinfoLen) {
			return 0;
		}
		byte[] pinfo = new byte[pinfoLen];
		stream.read(pinfo);
		formatter.format("%d_%d_%s", connDnet, portId, new String(pinfo, "US-ASCII"));
		return 4 + pinfoLen;
	}
}
