package com.synapsense.bacnet.network.media.IP.parcer;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import com.synapsense.bacnet.network.jni.MalformedPacketException;
import com.synapsense.bacnet.network.jni.Property;
import com.synapsense.bacnet.network.jni.PropertyNotFoundException;
import com.synapsense.bacnet.network.media.IP.DataPacket;
import com.synapsense.bacnet.network.pdu.npdu.NPDUMessageType;

/*
 *  Implementation of AbstractParcer, which processes parsing 
 *  BACnet-Establish-Connection-to-Network NPDU part of packet.
 *  <br>
 *  \code
 * | DNET | TTV |
 * |   2  |  1  |
 * |   M  |  M  |
 *  \endcode
 */
public class EstablishConnectionToNetworkParcer extends AbstractParcer {

	@Override
	public void read(DataInputStream stream, DataPacket packet) throws MalformedPacketException, IOException {
		logger.debug("Reading BACnet Establish-Connection-to-Network...");
		if (stream.available() != 3) {
			throw new MalformedPacketException();
		}
		packet.writeByte(Property.MESSAGE_TYPE, NPDUMessageType.Establish_Connection_To_Network.getCode());
		packet.writeShort(Property.NDNET, stream.readUnsignedShort());
		packet.writeByte(Property.TERMINATION_TIME_VALUE, stream.readUnsignedByte());
	}

	@Override
	public void write(DataOutputStream stream, DataPacket packet) throws IOException, PropertyNotFoundException {
		logger.debug("Writting BACnet Establish-Connection-to-Network...");
		stream.writeByte(NPDUMessageType.Establish_Connection_To_Network.getCode());
		stream.writeShort(packet.readShort(Property.NDNET));
		stream.writeByte(packet.readByte(Property.TERMINATION_TIME_VALUE));
	}

}
