package com.synapsense.bacnet.network.media.IP.parcer;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import com.synapsense.bacnet.network.jni.MalformedPacketException;
import com.synapsense.bacnet.network.jni.Property;
import com.synapsense.bacnet.network.jni.PropertyNotFoundException;
import com.synapsense.bacnet.network.media.IP.DataPacket;
import com.synapsense.bacnet.network.pdu.apdu.APDUMessageType;

/*
 *  Implementation of AbstractParcer, which processes parsing BACnet-Confirmed-Request
 *  APDU part of packet.
 *  <br>
 *  \code
 *   7   6   5   4   3   2   1   0
 * |---|---|---|---|---|---|---|---|
 * | PDU Type      |SEG|MOR| SA| 0 |
 * |---|---|---|---|---|---|---|---|
 * | 0 | Max Segs  |   Max Resp    |
 * |---|---|---|---|---|---|---|---|
 * |            Invoke ID          |
 * |---|---|---|---|---|---|---|---|
 * |         Sequence Number       | Only present if SEG = 1
 * |---|---|---|---|---|---|---|---|
 * |      Proposed Window Size     | Only present if SEG = 1
 * |---|---|---|---|---|---|---|---|
 * |         Service Choice        |
 * |---|---|---|---|---|---|---|---|
 * |         Service Request       |
 * |               .               |
 * |               .               |
 * |---|---|---|---|---|---|---|---|
 *  \endcode
 */
public class ConfirmedRequestParcer extends AbstractParcer {

	@Override
	public void read(DataInputStream stream, DataPacket packet) throws MalformedPacketException, IOException {
		logger.debug("Reading BACnet Confirmed-Request-PDU...");

		boolean segNumPresented;

		if (stream.available() < 1 || (getUnparcedMessageType() & 0x01) != 0) {
			throw new MalformedPacketException();
		}
		packet.writeByte(Property.MESSAGE_TYPE, APDUMessageType.confirmed_request_PDU.getType());
		segNumPresented = (getUnparcedMessageType() & 0x08) != 0;

		packet.writeBoolean(Property.IS_SEG_REQUEST, segNumPresented);
		packet.writeBoolean(Property.MORE_SEG_FOLLOW, (getUnparcedMessageType() & 0x04) != 0);
		packet.writeBoolean(Property.SEG_RESP_ACCEPTED, (getUnparcedMessageType() & 0x02) != 0);

		int segsResp = stream.readUnsignedByte();
		if ((segsResp & 0x80) != 0) {
			throw new MalformedPacketException();
		}

		packet.writeByte(Property.MAX_SEGS, ((segsResp & 0x70) >> 4));
		packet.writeByte(Property.MAX_RESP, segsResp & 0x0F);

		packet.writeByte(Property.INVOKE_ID, stream.readUnsignedByte());

		if (segNumPresented) {
			if (stream.available() < 2) {
				throw new MalformedPacketException();
			}
			packet.writeByte(Property.SEQUENCE_NUMBER, stream.readUnsignedByte());
			packet.writeByte(Property.WINDOW_SIZE, stream.readUnsignedByte());
		}
		if (stream.available() < 1) {
			throw new MalformedPacketException();
		}

		packet.writeByte(Property.SERV_CHOICE, stream.readUnsignedByte());
		int size = stream.available();
		byte[] payload = new byte[size];
		stream.read(payload, 0, size);
		packet.writeByteArray(Property.PDU_PAYLOAD, payload);
	}

	@Override
	public void write(DataOutputStream stream, DataPacket packet) throws PropertyNotFoundException, IOException {
		logger.debug("Writting BACnet Confirmed-Request-PDU...");

		int messageType = packet.readByte(Property.MESSAGE_TYPE) << 4;
		boolean segNumPresent = packet.readBoolean(Property.IS_SEG_REQUEST);
		if (segNumPresent) {
			messageType |= 0x08;
		}
		if (packet.readBoolean(Property.MORE_SEG_FOLLOW)) {
			messageType |= 0x04;
		}
		if (packet.readBoolean(Property.SEG_RESP_ACCEPTED)) {
			messageType |= 0x02;
		}
		stream.writeByte(messageType);
		stream.writeByte(packet.readByte(Property.MAX_RESP) | (packet.readByte(Property.MAX_SEGS) << 4));
		stream.writeByte(packet.readByte(Property.INVOKE_ID));
		if (segNumPresent) {
			stream.writeByte(packet.readByte(Property.SEQUENCE_NUMBER));
			stream.writeByte(packet.readByte(Property.WINDOW_SIZE));
		}
		stream.writeByte(packet.readByte(Property.SERV_CHOICE));
		stream.write(packet.readByteArray(Property.PDU_PAYLOAD));
	}

}
