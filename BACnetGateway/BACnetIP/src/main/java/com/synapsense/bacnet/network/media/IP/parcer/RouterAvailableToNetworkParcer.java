package com.synapsense.bacnet.network.media.IP.parcer;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import com.synapsense.bacnet.network.jni.MalformedPacketException;
import com.synapsense.bacnet.network.jni.Property;
import com.synapsense.bacnet.network.jni.PropertyNotFoundException;
import com.synapsense.bacnet.network.media.IP.DataPacket;
import com.synapsense.bacnet.network.pdu.npdu.NPDUMessageType;

/*
 *  Implementation of AbstractParcer, which processes parsing 
 *  BACnet-Router-Available-to-Network NPDU part of packet.
 *  <br>
 *  \code
 * | DNET | ... | ... 
 * |   2  |  2  |  2
 * |   O  |  O  |  O
 *  \endcode
 * number of DNET fields is specified by payload_len
 */
public class RouterAvailableToNetworkParcer extends AbstractParcer {

	@Override
	public void read(DataInputStream stream, DataPacket packet) throws MalformedPacketException, IOException {
		logger.debug("Reading BACnet Router-Available-to-Network...");
		packet.writeByte(Property.MESSAGE_TYPE, NPDUMessageType.Router_Available_To_Network.getCode());
		if (stream.available() > 0) {
			if ((stream.available() & 1) == 0) {
				int ndnets = stream.available() / 2;
				packet.writeInteger(Property.NDNETS, ndnets);
				short[] dnets = new short[ndnets];
				for (int i = 0; i < ndnets; i++) {
					dnets[i] = (short) stream.readUnsignedShort();
				}
				packet.writeShortArray(Property.NDNETS, dnets);
			} else {
				throw new MalformedPacketException();
			}
		}
	}

	@Override
	public void write(DataOutputStream stream, DataPacket packet) throws IOException, PropertyNotFoundException {
		logger.debug("Writting BACnet Router-Available-to-Network...");
		stream.writeByte(NPDUMessageType.Router_Available_To_Network.getCode());

		short[] dnets = packet.readShortArray(Property.NDNETS);
		for (int i = 0; i < dnets.length; i++) {
			stream.writeShort(dnets[i]);
		}
	}

}
