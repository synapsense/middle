package com.synapsense.bacnet.network.media.IP.parcer;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.Arrays;

import mockit.Expectations;
import mockit.Verifications;

import mockit.integration.junit4.JMockit;
import org.junit.Assert;
import org.junit.Test;

import com.synapsense.bacnet.enums.ErrorClass;
import com.synapsense.bacnet.enums.ErrorCode;
import com.synapsense.bacnet.network.jni.MalformedPacketException;
import com.synapsense.bacnet.network.jni.NotBVLLMessageException;
import com.synapsense.bacnet.network.jni.Property;
import com.synapsense.bacnet.network.jni.PropertyNotFoundException;
import com.synapsense.bacnet.network.jni.UnableToSendException;
import com.synapsense.bacnet.network.jni.UnableToSetUpInterfaceException;
import com.synapsense.bacnet.network.media.IP.DataPacket;
import com.synapsense.bacnet.network.media.IP.HelperUtils;
import com.synapsense.bacnet.network.media.IP.IpPacketReceiver;
import com.synapsense.bacnet.network.pdu.apdu.datatypes.ConfirmedServiceChoice;
import com.synapsense.bacnet.network.pdu.apdu.response.ErrorPDU;
import com.synapsense.bacnet.network.pdu.apdu.svc.responce.ErrorSvc;
import com.synapsense.bacnet.system.BACnetError;
import org.junit.runner.RunWith;

@RunWith(JMockit.class)
public class ErrorParcerTest extends AbstractParcerTest {
	@Test
	public void sendErrorPDUTest() throws Exception {
		final byte[] testData = HelperUtils.toByteArray("810A000D010050210A91019107");
		header.Control = true;
		BACnetError error = new BACnetError(ErrorClass.OBJECT, ErrorCode.INCONSISTENT_PARAMETERS);
		ErrorSvc service = new ErrorSvc(header);
		service.setError(error);
		service.setInvokeID(33);
		ErrorPDU pdu = (ErrorPDU) service.getPacket();
		pdu.setError_choice(ConfirmedServiceChoice.createObject);
		pdu.send();

		new Verifications() {
			{
				byte[] packetBuffer;
				DataPacket dataPacket;
				packetReceiver.sendPacket(packetBuffer = withCapture(), dataPacket = withCapture());
				System.out.println(dataPacket.toString());
				String a = HelperUtils.getHexString(packetBuffer).toUpperCase();
				System.out.println(a);
				System.out.println(Arrays.toString(packetBuffer));
				System.out.println(Arrays.toString(testData));
				Assert.assertArrayEquals(testData, packetBuffer);
			}
		};
	}

	@Test
	public void receiveErrorPDUTest() throws UnableToSetUpInterfaceException, UnableToSendException, IOException,
	        InterruptedException, PropertyNotFoundException, MalformedPacketException {
		// ErorPDU with invoke id = 77, service = ReadProperty, Error Class =
		// communication, Error Code = invalid-data-type
		final byte[] testData = HelperUtils.toByteArray("810A000D0100504D0C91029110");
		new Expectations() {
			{
				packetReceiver.getPacket();
				result = new DatagramPacket(testData, testData.length, new InetSocketAddress(
				        InetAddress.getByAddress(SRC_IP), IpPacketReceiver.DEFAULT_BACNET_PORT));
			}
		};
		packetId = media.startRead();
		header.packetId = packetId;
		ErrorPDU request = new ErrorPDU(header);
		request.receive();
		ErrorSvc svc = new ErrorSvc(header);
		request.receive();
		svc.decodeBuffer(media.readByteArray(packetId, Property.PDU_PAYLOAD));
		Assert.assertEquals(77, request.getInvokeId());
		Assert.assertEquals(ConfirmedServiceChoice.readProperty, request.getError_choice());
		Assert.assertEquals(ErrorClass.PROPERTY, svc.getError().getErrorClass());
		Assert.assertEquals(ErrorCode.MISSING_REQUIRED_PARAMETER, svc.getError().getErrorCode());
	}

	@Test(expected = MalformedPacketException.class)
	public void receiveMaleformedErrorPDUTest() throws UnableToSetUpInterfaceException, UnableToSendException,
	        IOException, InterruptedException, PropertyNotFoundException, MalformedPacketException,
	        NotBVLLMessageException {
		final byte[] testData = HelperUtils.toByteArray("810A00080100504D");
		new Expectations() {
			{
				packetReceiver.getPacket();
				result = new DatagramPacket(testData, testData.length, new InetSocketAddress(
				        InetAddress.getByAddress(SRC_IP), IpPacketReceiver.DEFAULT_BACNET_PORT));
			}
		};
		media.waitForPacket();
	}

	@Test(expected = MalformedPacketException.class)
	public void receiveMaleformedErrorPDU2Test() throws UnableToSetUpInterfaceException, UnableToSendException,
	        IOException, InterruptedException, PropertyNotFoundException, MalformedPacketException,
	        NotBVLLMessageException {
		final byte[] testData = HelperUtils.toByteArray("810A000D0100584D0C91029110");
		new Expectations() {
			{
				packetReceiver.getPacket();
				result = new DatagramPacket(testData, testData.length, new InetSocketAddress(
				        InetAddress.getByAddress(SRC_IP), IpPacketReceiver.DEFAULT_BACNET_PORT));
			}
		};
		media.waitForPacket();
	}

}
