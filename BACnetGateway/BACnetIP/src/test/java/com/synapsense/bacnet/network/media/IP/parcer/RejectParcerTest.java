package com.synapsense.bacnet.network.media.IP.parcer;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.Arrays;

import mockit.Expectations;
import mockit.Verifications;

import mockit.integration.junit4.JMockit;
import org.junit.Assert;
import org.junit.Test;

import com.synapsense.bacnet.network.jni.MalformedPacketException;
import com.synapsense.bacnet.network.jni.NotBVLLMessageException;
import com.synapsense.bacnet.network.jni.PropertyNotFoundException;
import com.synapsense.bacnet.network.jni.UnableToSendException;
import com.synapsense.bacnet.network.jni.UnableToSetUpInterfaceException;
import com.synapsense.bacnet.network.media.IP.DataPacket;
import com.synapsense.bacnet.network.media.IP.HelperUtils;
import com.synapsense.bacnet.network.media.IP.IpPacketReceiver;
import com.synapsense.bacnet.network.pdu.apdu.datatypes.RejectReason;
import com.synapsense.bacnet.network.pdu.apdu.response.RejectPDU;
import org.junit.runner.RunWith;

@RunWith(JMockit.class)
public class RejectParcerTest extends AbstractParcerTest {

	@Test
	public void sendRejectPDU() throws Exception {
		// RejectPDU with reject reason = buffer-overflow and invoke id = 44
		final byte[] testData = HelperUtils.toByteArray("810A00090100602C01");
		header.Control = true;
		RejectPDU pdu = new RejectPDU(header);
		pdu.setRejectReason(RejectReason.buffer_overflow);
		pdu.setId(44);
		pdu.send();

		new Verifications() {
			{
				byte[] packetBuffer;
				DataPacket dataPacket;
				packetReceiver.sendPacket(packetBuffer = withCapture(), dataPacket = withCapture());
				System.out.println(dataPacket.toString());
				String a = HelperUtils.getHexString(packetBuffer).toUpperCase();
				System.out.println(a);
				System.out.println(Arrays.toString(packetBuffer));
				System.out.println(Arrays.toString(testData));
				Assert.assertArrayEquals(testData, packetBuffer);
			}
		};
	}

	@Test
	public void sendRejectPDUWithNoReason() throws Exception {
		// RejectPDU with reject reason = other and invoke id = 44
		final byte[] testData = HelperUtils.toByteArray("810A00090100602C00");
		header.Control = true;
		RejectPDU pdu = new RejectPDU(header);
		pdu.setId(44);
		pdu.send();
		// Since we didn't specify the reject reason, it's supposed to be
		// "other"
		new Verifications() {
			{
				byte[] packetBuffer;
				DataPacket dataPacket;
				packetReceiver.sendPacket(packetBuffer = withCapture(), dataPacket = withCapture());
				System.out.println(dataPacket.toString());
				String a = HelperUtils.getHexString(packetBuffer).toUpperCase();
				System.out.println(a);
				System.out.println(Arrays.toString(packetBuffer));
				System.out.println(Arrays.toString(testData));
				Assert.assertArrayEquals(testData, packetBuffer);
			}
		};
	}

	@Test
	public void receiveRejectPDUTest() throws UnableToSetUpInterfaceException, UnableToSendException, IOException,
	        InterruptedException, PropertyNotFoundException, MalformedPacketException {
		// RejectPDU with reject reason = buffer-overflow and invoke id = 44
		final byte[] testData = HelperUtils.toByteArray("810A00090100602C01");
		new Expectations() {
			{
				packetReceiver.getPacket();
				result = new DatagramPacket(testData, testData.length, new InetSocketAddress(
				        InetAddress.getByAddress(SRC_IP), IpPacketReceiver.DEFAULT_BACNET_PORT));
			}
		};
		packetId = media.startRead();
		header.packetId = packetId;
		header.media = media;
		RejectPDU request = new RejectPDU(header);
		request.receive();
		Assert.assertEquals(44, request.getInvokeId());
		Assert.assertEquals(RejectReason.buffer_overflow, request.getRejectreason());
	}

	@Test(expected = MalformedPacketException.class)
	public void receiveMailformedRejectPDUTest() throws UnableToSetUpInterfaceException, UnableToSendException,
	        IOException, PropertyNotFoundException, MalformedPacketException, NotBVLLMessageException {
		final byte[] testData = HelperUtils.toByteArray("810A0007010060");
		new Expectations() {
			{
				packetReceiver.getPacket();
				result = new DatagramPacket(testData, testData.length, new InetSocketAddress(
				        InetAddress.getByAddress(SRC_IP), IpPacketReceiver.DEFAULT_BACNET_PORT));
			}
		};
		media.waitForPacket();

	}

	@Test(expected = MalformedPacketException.class)
	public void receiveMailformedAbortPDU2Test() throws UnableToSetUpInterfaceException, UnableToSendException,
	        IOException, PropertyNotFoundException, MalformedPacketException, NotBVLLMessageException {
		final byte[] testData = HelperUtils.toByteArray("810A0009010060");
		new Expectations() {
			{
				packetReceiver.getPacket();
				result = new DatagramPacket(testData, testData.length, new InetSocketAddress(
				        InetAddress.getByAddress(SRC_IP), IpPacketReceiver.DEFAULT_BACNET_PORT));
			}
		};
		media.waitForPacket();
	}
}
