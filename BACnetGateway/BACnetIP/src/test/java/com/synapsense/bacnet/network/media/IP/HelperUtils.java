package com.synapsense.bacnet.network.media.IP;

import java.net.InetAddress;
import java.net.UnknownHostException;

import javax.xml.bind.DatatypeConverter;

import org.junit.Ignore;

@Ignore
public class HelperUtils {
	public static byte[] toByteArray(String s) {
		return DatatypeConverter.parseHexBinary(s);
	}

	public static byte[] getDestinationMac(String destination) {
		byte[] result = null;
		String ip = destination;
		if (ip != null) {
			byte[] portBytes = new byte[] { (byte) 0xBA, (byte) 0xC0 };
			if (ip.indexOf(":") != -1) {
				int port = Integer.parseInt(ip.substring(ip.indexOf(":") + 1));
				portBytes[0] = (byte) (port >> 8);
				portBytes[1] = (byte) port;
				ip = ip.substring(0, ip.indexOf(":"));
			}

			String[] splitIp = ip.split("\\.");
			if (splitIp.length != 4) {
				try {
					InetAddress inet = InetAddress.getByName(ip);
					splitIp = inet.getHostAddress().split("\\.");
				} catch (UnknownHostException e) {
				}
			}
			if (splitIp.length == 4) {
				result = new byte[splitIp.length + 2];
				for (int i = 0; i < splitIp.length; i++) {
					result[i] = (byte) Integer.parseInt(splitIp[i]);
				}
				result[result.length - 2] = portBytes[0];
				result[result.length - 1] = portBytes[1];
			}
		}

		return result;
	}

	public static String getHexString(byte[] b) throws Exception {
		String result = "";
		for (int i = 0; i < b.length; i++) {
			result += Integer.toString((b[i] & 0xff) + 0x100, 16).substring(1);
		}
		return result;
	}
}
