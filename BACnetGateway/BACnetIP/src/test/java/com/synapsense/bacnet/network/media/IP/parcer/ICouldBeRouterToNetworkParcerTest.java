package com.synapsense.bacnet.network.media.IP.parcer;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.Arrays;

import mockit.Expectations;
import mockit.Verifications;

import mockit.integration.junit4.JMockit;
import org.junit.Assert;
import org.junit.Test;

import com.synapsense.bacnet.network.jni.MalformedPacketException;
import com.synapsense.bacnet.network.jni.NotBVLLMessageException;
import com.synapsense.bacnet.network.jni.PropertyNotFoundException;
import com.synapsense.bacnet.network.jni.UnableToSendException;
import com.synapsense.bacnet.network.jni.UnableToSetUpInterfaceException;
import com.synapsense.bacnet.network.media.IP.DataPacket;
import com.synapsense.bacnet.network.media.IP.HelperUtils;
import com.synapsense.bacnet.network.media.IP.IpPacketReceiver;
import com.synapsense.bacnet.network.pdu.npdu.response.ICouldBeRouterToNetwork;
import org.junit.runner.RunWith;

@RunWith(JMockit.class)
public class ICouldBeRouterToNetworkParcerTest extends AbstractParcerTest {

	@Test
	public void sendICouldBeRouterToNetworkTest() throws Exception {
		// ICouldBeRouterToNetwork, dnet = 5 and performance index = 23
		final byte[] testData = HelperUtils.toByteArray("810A000A018002000517");
		header.Control = false;
		ICouldBeRouterToNetwork pdu = new ICouldBeRouterToNetwork(header);
		pdu.setNDNET(5);
		pdu.setPERF_INDEX(23);
		pdu.send();

		new Verifications() {
			{
				byte[] packetBuffer;
				DataPacket dataPacket;
				packetReceiver.sendPacket(packetBuffer = withCapture(), dataPacket = withCapture());
				System.out.println(dataPacket.toString());
				String a = HelperUtils.getHexString(packetBuffer).toUpperCase();
				System.out.println(a);
				System.out.println(Arrays.toString(packetBuffer));
				System.out.println(Arrays.toString(testData));
				Assert.assertArrayEquals(testData, packetBuffer);
			}
		};
	}

	@Test
	public void receiveICouldBeRouterToNetworkTest() throws UnableToSetUpInterfaceException, UnableToSendException,
	        IOException, InterruptedException, PropertyNotFoundException, MalformedPacketException {
		// ICouldBeRouterToNetwork, dnet = 5 and performance index = 23
		final byte[] testData = HelperUtils.toByteArray("810A000A018002000517");
		new Expectations() {
			{
				packetReceiver.getPacket();
				result = new DatagramPacket(testData, testData.length, new InetSocketAddress(
				        InetAddress.getByAddress(SRC_IP), IpPacketReceiver.DEFAULT_BACNET_PORT));
			}
		};
		packetId = media.startRead();
		header.packetId = packetId;
		header.media = media;
		ICouldBeRouterToNetwork request = new ICouldBeRouterToNetwork(header);
		request.receive();
		Assert.assertEquals(request.getNDNET(), 5);
		Assert.assertEquals(request.getPERF_INDEX(), 23);
	}

	@Test(expected = MalformedPacketException.class)
	public void receiveICouldBeRouterToNetwork3Test() throws UnableToSetUpInterfaceException, UnableToSendException,
	        IOException, InterruptedException, PropertyNotFoundException, MalformedPacketException,
	        NotBVLLMessageException {
		// ICouldBeRouterToNetwork, dnet = 5 and performance index = 23
		final byte[] testData = HelperUtils.toByteArray("810A00090180020005");
		new Expectations() {
			{
				packetReceiver.getPacket();
				result = new DatagramPacket(testData, testData.length, new InetSocketAddress(
				        InetAddress.getByAddress(SRC_IP), IpPacketReceiver.DEFAULT_BACNET_PORT));
			}
		};
		media.waitForPacket();
	}
}
