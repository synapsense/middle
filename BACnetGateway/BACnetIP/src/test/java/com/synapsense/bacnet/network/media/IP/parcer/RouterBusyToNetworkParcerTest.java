package com.synapsense.bacnet.network.media.IP.parcer;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.Arrays;

import mockit.Expectations;
import mockit.Verifications;

import mockit.integration.junit4.JMockit;
import org.junit.Assert;
import org.junit.Test;

import com.synapsense.bacnet.network.jni.MalformedPacketException;
import com.synapsense.bacnet.network.jni.NotBVLLMessageException;
import com.synapsense.bacnet.network.jni.PropertyNotFoundException;
import com.synapsense.bacnet.network.jni.UnableToSendException;
import com.synapsense.bacnet.network.jni.UnableToSetUpInterfaceException;
import com.synapsense.bacnet.network.media.IP.DataPacket;
import com.synapsense.bacnet.network.media.IP.HelperUtils;
import com.synapsense.bacnet.network.media.IP.IpPacketReceiver;
import com.synapsense.bacnet.network.pdu.npdu.response.RouterBusyToNetwork;
import org.junit.runner.RunWith;

@RunWith(JMockit.class)
public class RouterBusyToNetworkParcerTest extends AbstractParcerTest {
	@Test
	public void sendRouterBusyToNetworkTest() throws Exception {
		// RouterBusyToNetwork, dnet1 = 1, dnet2 = 1, dnet3 = 1, dnet4 = 1
		final byte[] testData = HelperUtils.toByteArray("810B000F0180040001000100010001");
		header.Control = false;
		RouterBusyToNetwork pdu = new RouterBusyToNetwork(header);
		pdu.SetDestinations(new int[] { 1, 1, 1, 1 });
		pdu.send();

		new Verifications() {
			{
				byte[] packetBuffer;
				DataPacket dataPacket;
				packetReceiver.sendPacket(packetBuffer = withCapture(), dataPacket = withCapture());
				System.out.println(dataPacket.toString());
				String a = HelperUtils.getHexString(packetBuffer).toUpperCase();
				System.out.println(a);
				System.out.println(Arrays.toString(packetBuffer));
				System.out.println(Arrays.toString(testData));
				Assert.assertArrayEquals(testData, packetBuffer);
			}
		};
	}

	@Test
	public void sendRouterBusyToNetwork2Test() throws Exception {
		// RouterBusyToNetwork, dnet1 = 55
		final byte[] testData = HelperUtils.toByteArray("810B00090180040037");
		header.Control = false;
		RouterBusyToNetwork pdu = new RouterBusyToNetwork(header);
		pdu.SetDestinations(55);
		pdu.send();

		new Verifications() {
			{
				byte[] packetBuffer;
				DataPacket dataPacket;
				packetReceiver.sendPacket(packetBuffer = withCapture(), dataPacket = withCapture());
				System.out.println(dataPacket.toString());
				String a = HelperUtils.getHexString(packetBuffer).toUpperCase();
				System.out.println(a);
				System.out.println(Arrays.toString(packetBuffer));
				System.out.println(Arrays.toString(testData));
				Assert.assertArrayEquals(testData, packetBuffer);
			}
		};
	}

	@Test(expected = UnableToSendException.class)
	public void sendRouterBusyToNetwork3Test() throws Exception {
		// RouterBusyToNetwork, dnet1 = 1, dnet2 = 1, dnet3 = 1, dnet4 = 1
		final byte[] testData = HelperUtils.toByteArray("810B000F0180040001000100010001");
		header.Control = false;
		RouterBusyToNetwork pdu = new RouterBusyToNetwork(header);
		pdu.send();

		new Verifications() {
			{
				byte[] packetBuffer;
				DataPacket dataPacket;
				packetReceiver.sendPacket(packetBuffer = withCapture(), dataPacket = withCapture());
				System.out.println(dataPacket.toString());
				String a = HelperUtils.getHexString(packetBuffer).toUpperCase();
				System.out.println(a);
				System.out.println(Arrays.toString(packetBuffer));
				System.out.println(Arrays.toString(testData));
				Assert.assertArrayEquals(testData, packetBuffer);
			}
		};
	}

	@Test
	public void receiveRouterBusyToNetworkTest() throws UnableToSetUpInterfaceException, UnableToSendException,
	        IOException, InterruptedException, PropertyNotFoundException, MalformedPacketException {
		// RouterBusyToNetwork, dnet1 = 42
		final byte[] testData = HelperUtils.toByteArray("810B0009018004002A");

		new Expectations() {
			{
				packetReceiver.getPacket();
				result = new DatagramPacket(testData, testData.length, new InetSocketAddress(
				        InetAddress.getByAddress(SRC_IP), IpPacketReceiver.DEFAULT_BACNET_PORT));
			}
		};
		packetId = media.startRead();
		header.packetId = packetId;
		header.media = media;
		RouterBusyToNetwork request = new RouterBusyToNetwork(header);
		request.receive();
		Assert.assertArrayEquals(request.getDestinations(), new int[] { 42 });
	}

	@Test
	public void receiveRouterBusyToNetwork2Test() throws UnableToSetUpInterfaceException, UnableToSendException,
	        IOException, InterruptedException, PropertyNotFoundException, MalformedPacketException {
		// RouterBusyToNetwork, dnet1 = 42, dnet1 = 22
		final byte[] testData = HelperUtils.toByteArray("810B000B018004002A0016");
		new Expectations() {
			{
				packetReceiver.getPacket();
				result = new DatagramPacket(testData, testData.length, new InetSocketAddress(
				        InetAddress.getByAddress(SRC_IP), IpPacketReceiver.DEFAULT_BACNET_PORT));
			}
		};
		packetId = media.startRead();
		header.packetId = packetId;
		header.media = media;
		RouterBusyToNetwork request = new RouterBusyToNetwork(header);
		request.receive();
		Assert.assertArrayEquals(request.getDestinations(), new int[] { 42, 22 });
	}

	@Test(expected = MalformedPacketException.class)
	public void receiveRouterBusyToNetwork3Test() throws UnableToSetUpInterfaceException, UnableToSendException,
	        IOException, InterruptedException, PropertyNotFoundException, MalformedPacketException,
	        NotBVLLMessageException {
		// RouterBusyToNetwork
		final byte[] testData = HelperUtils.toByteArray("810B000A018004002A00");

		new Expectations() {
			{
				packetReceiver.getPacket();
				result = new DatagramPacket(testData, testData.length, new InetSocketAddress(
				        InetAddress.getByAddress(SRC_IP), IpPacketReceiver.DEFAULT_BACNET_PORT));
			}
		};
		media.waitForPacket();
	}

	@Test(expected = MalformedPacketException.class)
	public void receiveRouterBusyToNetwork4Test() throws UnableToSetUpInterfaceException, UnableToSendException,
	        IOException, InterruptedException, PropertyNotFoundException, MalformedPacketException,
	        NotBVLLMessageException {
		// RouterBusyToNetwork
		final byte[] testData = HelperUtils.toByteArray("8109000A01800400");
		new Expectations() {
			{
				packetReceiver.getPacket();
				result = new DatagramPacket(testData, testData.length, new InetSocketAddress(
				        InetAddress.getByAddress(SRC_IP), IpPacketReceiver.DEFAULT_BACNET_PORT));
			}
		};
		media.waitForPacket();
	}

}
