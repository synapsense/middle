package com.synapsense.bacnet.network.media.IP.parcer;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.Arrays;

import mockit.Expectations;
import mockit.Verifications;

import mockit.integration.junit4.JMockit;
import org.junit.Assert;
import org.junit.Test;

import com.synapsense.bacnet.network.jni.MalformedPacketException;
import com.synapsense.bacnet.network.jni.NotBVLLMessageException;
import com.synapsense.bacnet.network.jni.PropertyNotFoundException;
import com.synapsense.bacnet.network.jni.UnableToSendException;
import com.synapsense.bacnet.network.jni.UnableToSetUpInterfaceException;
import com.synapsense.bacnet.network.media.IP.DataPacket;
import com.synapsense.bacnet.network.media.IP.HelperUtils;
import com.synapsense.bacnet.network.media.IP.IpPacketReceiver;
import com.synapsense.bacnet.network.pdu.apdu.datatypes.ConfirmedServiceChoice;
import com.synapsense.bacnet.network.pdu.apdu.response.SimpleACKPDU;
import org.junit.runner.RunWith;

@RunWith(JMockit.class)
public class SimpleAckParcerTest extends AbstractParcerTest {
	@Test
	public void sendSimpleAckTest() throws Exception {
		// SimpleAckPDU with invoke id = 56 and service =
		// ConfirmedCOVNotification
		final byte[] testData = HelperUtils.toByteArray("810A00090100203801");
		header.Control = true;
		SimpleACKPDU pdu = new SimpleACKPDU(header);
		pdu.setService_ACK_choice(ConfirmedServiceChoice.confirmedCOVNotification);
		pdu.setId(56);
		pdu.send();

		new Verifications() {
			{
				byte[] packetBuffer;
				DataPacket dataPacket;
				packetReceiver.sendPacket(packetBuffer = withCapture(), dataPacket = withCapture());
				System.out.println(dataPacket.toString());
				String a = HelperUtils.getHexString(packetBuffer).toUpperCase();
				System.out.println(a);
				System.out.println(Arrays.toString(packetBuffer));
				System.out.println(Arrays.toString(testData));
				Assert.assertArrayEquals(testData, packetBuffer);
			}
		};
	}

	@Test
	public void receiveSimpleAckPDUTest() throws UnableToSetUpInterfaceException, UnableToSendException, IOException,
	        InterruptedException, PropertyNotFoundException, MalformedPacketException {
		// SimpleAckPDU with invoke id = 42 and service = GetAlarmSummary
		final byte[] testData = HelperUtils.toByteArray("810A00090100202A03");
		new Expectations() {
			{
				packetReceiver.getPacket();
				result = new DatagramPacket(testData, testData.length, new InetSocketAddress(InetAddress.getByAddress(SRC_IP), IpPacketReceiver.DEFAULT_BACNET_PORT));
			}
		};
		packetId = media.startRead();
		header.packetId = packetId;
		header.media = media;
		SimpleACKPDU request = new SimpleACKPDU(header);
		request.receive();
		Assert.assertEquals(42, request.getInvokeId());
		Assert.assertEquals(ConfirmedServiceChoice.getAlarmSummary, request.getService_ACK_choice());
	}

	@Test(expected = MalformedPacketException.class)
	public void receiveMailformedSimpleAckPDUTest() throws UnableToSetUpInterfaceException, UnableToSendException,
	        IOException, PropertyNotFoundException, MalformedPacketException, NotBVLLMessageException {
		final byte[] testData = HelperUtils.toByteArray("810A0007010020");
		new Expectations() {
			{
				packetReceiver.getPacket();
				result = new DatagramPacket(testData, testData.length, new InetSocketAddress(
				        InetAddress.getByAddress(SRC_IP), IpPacketReceiver.DEFAULT_BACNET_PORT));
			}
		};
		media.waitForPacket();

	}

	@Test(expected = MalformedPacketException.class)
	public void receiveMailformedSimpleAckPDU2Test() throws UnableToSetUpInterfaceException, UnableToSendException,
	        IOException, PropertyNotFoundException, MalformedPacketException, NotBVLLMessageException {
		final byte[] testData = HelperUtils.toByteArray("810A00090100212A03");
		new Expectations() {
			{
				packetReceiver.getPacket();
				result = new DatagramPacket(testData, testData.length, new InetSocketAddress(InetAddress.getByAddress(SRC_IP), IpPacketReceiver.DEFAULT_BACNET_PORT));
			}
		};
		media.waitForPacket();

	}

	@Test(expected = MalformedPacketException.class)
	public void receiveMailformedSimpleAckPDU3Test() throws UnableToSetUpInterfaceException, UnableToSendException,
	        IOException, PropertyNotFoundException, MalformedPacketException, NotBVLLMessageException {
		final byte[] testData = HelperUtils.toByteArray("810A00090100202A");
		new Expectations() {
			{
				packetReceiver.getPacket();
				result = new DatagramPacket(testData, testData.length, new InetSocketAddress(InetAddress.getByAddress(SRC_IP), IpPacketReceiver.DEFAULT_BACNET_PORT));
			}
		};
		media.waitForPacket();

	}

}
