package com.synapsense.bacnet.network.media.IP.parcer;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.Arrays;

import mockit.Expectations;
import mockit.Verifications;

import mockit.integration.junit4.JMockit;
import org.junit.Assert;
import org.junit.Test;

import com.synapsense.bacnet.network.jni.MalformedPacketException;
import com.synapsense.bacnet.network.jni.NotBVLLMessageException;
import com.synapsense.bacnet.network.jni.PropertyNotFoundException;
import com.synapsense.bacnet.network.jni.UnableToSendException;
import com.synapsense.bacnet.network.jni.UnableToSetUpInterfaceException;
import com.synapsense.bacnet.network.media.IP.DataPacket;
import com.synapsense.bacnet.network.media.IP.HelperUtils;
import com.synapsense.bacnet.network.media.IP.IpPacketReceiver;
import com.synapsense.bacnet.network.pdu.npdu.request.InitializeRoutingTable;
import com.synapsense.bacnet.network.routing.Route;
import com.synapsense.bacnet.network.routing.RoutingService;
import org.junit.runner.RunWith;

@RunWith(JMockit.class)
public class InitializeRoutingTableParcerTest extends AbstractParcerTest {
	@Test
	public void sendInitializeRoutingTableTest() throws Exception {
		// InitializeRoutingTable with one route Dnet = 3, route id = 4 and port
		// info = "portinfo"
		final byte[] testData = HelperUtils.toByteArray("810A00140180060100030408706F7274696E666F");

		Route route = new Route(3, 4, "portinfo", HelperUtils.getDestinationMac("192.168.102.2:47808"));
        RoutingService.dropTable();
		RoutingService.addRoute(route);
		header.Control = false;
		InitializeRoutingTable pdu = new InitializeRoutingTable(header);
		pdu.send();

		new Verifications() {
			{
				byte[] packetBuffer;
				DataPacket dataPacket;
				packetReceiver.sendPacket(packetBuffer = withCapture(), dataPacket = withCapture());
				System.out.println(dataPacket.toString());
				String a = HelperUtils.getHexString(packetBuffer).toUpperCase();
				System.out.println(a);
				System.out.println(Arrays.toString(packetBuffer));
				System.out.println(Arrays.toString(testData));
				Assert.assertArrayEquals(testData, packetBuffer);
			}
		};
	}

	@Test
	public void sendInitializeRoutingTable2Test() throws Exception {
		// InitializeRoutingTable with empty route info
		final byte[] testData = HelperUtils.toByteArray("810A000801800600");
		RoutingService.dropTable();
		header.Control = false;
		InitializeRoutingTable pdu = new InitializeRoutingTable(header);
		pdu.send();

		new Verifications() {
			{
				byte[] packetBuffer;
				DataPacket dataPacket;
				packetReceiver.sendPacket(packetBuffer = withCapture(), dataPacket = withCapture());
				System.out.println(dataPacket.toString());
				String a = HelperUtils.getHexString(packetBuffer).toUpperCase();
				System.out.println(a);
				System.out.println(Arrays.toString(packetBuffer));
				System.out.println(Arrays.toString(testData));
				Assert.assertArrayEquals(testData, packetBuffer);
			}
		};
	}

	@Test
	public void sendInitializeRoutingTable3Test() throws Exception {
		// InitializeRoutingTable with two routes
		// Dnet = 3, route id = 4 and port info = "portinfo"
		// Dnet = 4, route id = 7 and port info = "fff"
		final byte[] testData = HelperUtils.toByteArray("810A001B0180060200030408706F7274696E666F00040703666666");
        RoutingService.dropTable();
		Route route1 = new Route(3, 4, "portinfo", HelperUtils.getDestinationMac("192.168.102.2:47808"));
		Route route2 = new Route(4, 7, "fff", HelperUtils.getDestinationMac("192.168.102.2:47808"));

		RoutingService.addRoute(route1);
		RoutingService.addRoute(route2);
		header.Control = false;
		InitializeRoutingTable pdu = new InitializeRoutingTable(header);
		pdu.send();

		new Verifications() {
			{
				byte[] packetBuffer;
				DataPacket dataPacket;
				packetReceiver.sendPacket(packetBuffer = withCapture(), dataPacket = withCapture());
				System.out.println(dataPacket.toString());
				String a = HelperUtils.getHexString(packetBuffer).toUpperCase();
				System.out.println(a);
				System.out.println(Arrays.toString(packetBuffer));
				System.out.println(Arrays.toString(testData));
				Assert.assertArrayEquals(testData, packetBuffer);
			}
		};
	}

	@Test
	public void receiveInitializeRoutingTableTest() throws UnableToSetUpInterfaceException, UnableToSendException,
	        IOException, InterruptedException, PropertyNotFoundException, MalformedPacketException {
		// InitializeRoutingTable with one route Dnet = 3, route id = 4 and port
		// info = "portinfo"
		final byte[] testData = HelperUtils.toByteArray("810A00140180060100030408706F7274696E666F");
		new Expectations() {
			{
				packetReceiver.getPacket();
				result = new DatagramPacket(testData, testData.length, new InetSocketAddress(
				        InetAddress.getByAddress(SRC_IP), IpPacketReceiver.DEFAULT_BACNET_PORT));
			}
		};
		packetId = media.startRead();
		header.packetId = packetId;
		header.media = media;
		InitializeRoutingTable request = new InitializeRoutingTable(header);
		request.receive();
		Assert.assertArrayEquals(request.getRouteInfo(), new String[] { "3_4_portinfo" });
	}

	@Test
	public void receiveInitializeRoutingTable1Test() throws UnableToSetUpInterfaceException, UnableToSendException,
	        IOException, InterruptedException, PropertyNotFoundException, MalformedPacketException {
		// InitializeRoutingTable with one route Dnet = 3, route id = 4 and port
		// info = "portinfo"
		final byte[] testData = HelperUtils.toByteArray("810A000C0180060100030400");
		new Expectations() {
			{
				packetReceiver.getPacket();
				result = new DatagramPacket(testData, testData.length, new InetSocketAddress(
				        InetAddress.getByAddress(SRC_IP), IpPacketReceiver.DEFAULT_BACNET_PORT));
			}
		};
		packetId = media.startRead();
		header.packetId = packetId;
		header.media = media;
		InitializeRoutingTable request = new InitializeRoutingTable(header);
		request.receive();
		Assert.assertArrayEquals(new String[] { "3_4_" }, request.getRouteInfo());
	}

	@Test
	public void receiveInitializeRoutingTable2Test() throws UnableToSetUpInterfaceException, UnableToSendException,
	        IOException, InterruptedException, PropertyNotFoundException, MalformedPacketException {
		// InitializeRoutingTable with no routes
		final byte[] testData = HelperUtils.toByteArray("810A000801800600");
		new Expectations() {
			{
				packetReceiver.getPacket();
				result = new DatagramPacket(testData, testData.length, new InetSocketAddress(
				        InetAddress.getByAddress(SRC_IP), IpPacketReceiver.DEFAULT_BACNET_PORT));
			}
		};
		packetId = media.startRead();
		header.packetId = packetId;
		header.media = media;
		InitializeRoutingTable request = new InitializeRoutingTable(header);
		request.receive();
		Assert.assertArrayEquals(request.getRouteInfo(), new String[] {});
	}

	@Test(expected = MalformedPacketException.class)
	public void receiveInitializeRoutingTable3Test() throws UnableToSetUpInterfaceException, UnableToSendException,
	        IOException, InterruptedException, PropertyNotFoundException, MalformedPacketException,
	        NotBVLLMessageException {
		// inconsistent port info and port info len
		final byte[] testData = HelperUtils.toByteArray("810A00130180060100030408706F7274696E66");
		new Expectations() {
			{
				packetReceiver.getPacket();
				result = new DatagramPacket(testData, testData.length, new InetSocketAddress(
				        InetAddress.getByAddress(SRC_IP), IpPacketReceiver.DEFAULT_BACNET_PORT));
			}
		};
		media.waitForPacket();
	}

	@Test(expected = MalformedPacketException.class)
	public void receiveInitializeRoutingTable4Test() throws UnableToSetUpInterfaceException, UnableToSendException,
	        IOException, InterruptedException, PropertyNotFoundException, MalformedPacketException,
	        NotBVLLMessageException {
		// short packet
		final byte[] testData = HelperUtils.toByteArray("810A000B01800601000304");
		new Expectations() {
			{
				packetReceiver.getPacket();
				result = new DatagramPacket(testData, testData.length, new InetSocketAddress(
				        InetAddress.getByAddress(SRC_IP), IpPacketReceiver.DEFAULT_BACNET_PORT));
			}
		};
		media.waitForPacket();
	}

	@Test(expected = MalformedPacketException.class)
	public void receiveInitializeRoutingTable5Test() throws UnableToSetUpInterfaceException, UnableToSendException,
	        IOException, InterruptedException, PropertyNotFoundException, MalformedPacketException,
	        NotBVLLMessageException {
		// short packet
		final byte[] testData = HelperUtils.toByteArray("810A0007018006");
		new Expectations() {
			{
				packetReceiver.getPacket();
				result = new DatagramPacket(testData, testData.length, new InetSocketAddress(
				        InetAddress.getByAddress(SRC_IP), IpPacketReceiver.DEFAULT_BACNET_PORT));
			}
		};
		media.waitForPacket();
	}

}
