package com.synapsense.bacnet.network.media.IP.parcer;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.Arrays;

import mockit.Expectations;
import mockit.Verifications;

import mockit.integration.junit4.JMockit;
import org.junit.Assert;
import org.junit.Test;

import com.synapsense.bacnet.network.jni.MalformedPacketException;
import com.synapsense.bacnet.network.jni.NotBVLLMessageException;
import com.synapsense.bacnet.network.jni.PropertyNotFoundException;
import com.synapsense.bacnet.network.jni.UnableToSendException;
import com.synapsense.bacnet.network.jni.UnableToSetUpInterfaceException;
import com.synapsense.bacnet.network.media.IP.DataPacket;
import com.synapsense.bacnet.network.media.IP.HelperUtils;
import com.synapsense.bacnet.network.media.IP.IpPacketReceiver;
import com.synapsense.bacnet.network.pdu.apdu.response.SegmentACKPDU;
import org.junit.runner.RunWith;

@RunWith(JMockit.class)
public class SegmentedAckParcerTest extends AbstractParcerTest {

	@Test
	public void sendSegmentedAckTest() throws Exception {
		// Non negative client SegmentedAckPDU with invoke id = 22, sequence
		// number = 1, actual window size = 4
		final byte[] testData = HelperUtils.toByteArray("810A000A010040160104");
		header.Control = true;
		SegmentACKPDU pdu = new SegmentACKPDU(header);
		pdu.setId(22);
		pdu.setActualWindowSize(4);
		pdu.setNegativeACK(false);
		pdu.setSequenceNumber(1);
		pdu.setServer(false);
		pdu.send();

		new Verifications() {
			{
				byte[] packetBuffer;
				DataPacket dataPacket;
				packetReceiver.sendPacket(packetBuffer = withCapture(), dataPacket = withCapture());
				System.out.println(dataPacket.toString());
				String a = HelperUtils.getHexString(packetBuffer).toUpperCase();
				System.out.println(a);
				System.out.println(Arrays.toString(packetBuffer));
				System.out.println(Arrays.toString(testData));
				Assert.assertArrayEquals(testData, packetBuffer);
			}
		};
	}

	@Test
	public void sendServerSegmentedAckTest() throws Exception {
		// Non negative server SegmentedAckPDU with invoke id = 22, sequence
		// number = 1, actual window size = 4
		final byte[] testData = HelperUtils.toByteArray("810A000A010041160104");
		header.Control = true;
		SegmentACKPDU pdu = new SegmentACKPDU(header);
		pdu.setId(22);
		pdu.setActualWindowSize(4);
		pdu.setNegativeACK(false);
		pdu.setSequenceNumber(1);
		pdu.setServer(true);
		pdu.send();

		new Verifications() {
			{
				byte[] packetBuffer;
				DataPacket dataPacket;
				packetReceiver.sendPacket(packetBuffer = withCapture(), dataPacket = withCapture());
				System.out.println(dataPacket.toString());
				String a = HelperUtils.getHexString(packetBuffer).toUpperCase();
				System.out.println(a);
				System.out.println(Arrays.toString(packetBuffer));
				System.out.println(Arrays.toString(testData));
				Assert.assertArrayEquals(testData, packetBuffer);
			}
		};
	}

	@Test
	public void sendNegativeServerSegmentedAckTest() throws Exception {
		// Negative server SegmentedAckPDU with invoke id = 22, sequence
		// number = 1, actual window size = 4
		final byte[] testData = HelperUtils.toByteArray("810A000A010043160104");
		header.Control = true;
		SegmentACKPDU pdu = new SegmentACKPDU(header);
		pdu.setId(22);
		pdu.setActualWindowSize(4);
		pdu.setNegativeACK(true);
		pdu.setSequenceNumber(1);
		pdu.setServer(true);
		pdu.send();

		new Verifications() {
			{
				byte[] packetBuffer;
				DataPacket dataPacket;
				packetReceiver.sendPacket(packetBuffer = withCapture(), dataPacket = withCapture());
				System.out.println(dataPacket.toString());
				String a = HelperUtils.getHexString(packetBuffer).toUpperCase();
				System.out.println(a);
				System.out.println(Arrays.toString(packetBuffer));
				System.out.println(Arrays.toString(testData));
				Assert.assertArrayEquals(testData, packetBuffer);
			}
		};
	}

	@Test
	public void receiveServerSegmentedAckPDUTest() throws UnableToSetUpInterfaceException, UnableToSendException,
	        IOException, InterruptedException, PropertyNotFoundException, MalformedPacketException {
		// Non negative server SegmentedAckPDU with invoke id = 22, sequence
		// number = 4, actual window size = 5
		final byte[] testData = HelperUtils.toByteArray("810A000A010041160405");
		new Expectations() {
			{
				packetReceiver.getPacket();
				result = new DatagramPacket(testData, testData.length, new InetSocketAddress(
				        InetAddress.getByAddress(SRC_IP), IpPacketReceiver.DEFAULT_BACNET_PORT));
			}
		};
		packetId = media.startRead();
		header.packetId = packetId;
		header.media = media;
		SegmentACKPDU request = new SegmentACKPDU(header);
		request.receive();
		Assert.assertEquals(22, request.getInvokeId());
		Assert.assertEquals(4, request.getSequenceNumber());
		Assert.assertEquals(5, request.getActualWindowSize());
		Assert.assertTrue(request.isServer());
		Assert.assertFalse(request.isNegativeACK());
	}

	@Test
	public void receiveClientSegmentedAckPDUTest() throws UnableToSetUpInterfaceException, UnableToSendException,
	        IOException, InterruptedException, PropertyNotFoundException, MalformedPacketException {
		// Negative Client SegmentedAckPDU with invoke id = 22, sequence
		// number = 4, actual window size = 5
		final byte[] testData = HelperUtils.toByteArray("810A000A010042160405");
		new Expectations() {
			{
				packetReceiver.getPacket();
				result = new DatagramPacket(testData, testData.length, new InetSocketAddress(
				        InetAddress.getByAddress(SRC_IP), IpPacketReceiver.DEFAULT_BACNET_PORT));
			}
		};
		packetId = media.startRead();
		header.packetId = packetId;
		header.media = media;
		SegmentACKPDU request = new SegmentACKPDU(header);
		request.receive();
		Assert.assertEquals(22, request.getInvokeId());
		Assert.assertEquals(4, request.getSequenceNumber());
		Assert.assertEquals(5, request.getActualWindowSize());
		Assert.assertTrue(request.isNegativeACK());
		Assert.assertFalse(request.isServer());
	}

	@Test(expected = MalformedPacketException.class)
	public void receiveMailformedSegmentedAckPDUTest() throws UnableToSetUpInterfaceException, UnableToSendException,
	        IOException, PropertyNotFoundException, MalformedPacketException, NotBVLLMessageException {
		final byte[] testData = HelperUtils.toByteArray("810A000A010045160405");
		new Expectations() {
			{
				packetReceiver.getPacket();
				result = new DatagramPacket(testData, testData.length, new InetSocketAddress(
				        InetAddress.getByAddress(SRC_IP), IpPacketReceiver.DEFAULT_BACNET_PORT));
			}
		};
		media.waitForPacket();

	}

	@Test(expected = MalformedPacketException.class)
	public void receiveMailformedSegmentedAckPDU2Test() throws UnableToSetUpInterfaceException, UnableToSendException,
	        IOException, PropertyNotFoundException, MalformedPacketException, NotBVLLMessageException {
		final byte[] testData = HelperUtils.toByteArray("810A000801004216");
		new Expectations() {
			{
				packetReceiver.getPacket();
				result = new DatagramPacket(testData, testData.length, new InetSocketAddress(
				        InetAddress.getByAddress(SRC_IP), IpPacketReceiver.DEFAULT_BACNET_PORT));
			}
		};
		media.waitForPacket();

	}
}
