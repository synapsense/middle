package com.synapsense.bacnet.network.media.IP.parcer;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.Arrays;

import mockit.Expectations;
import mockit.Verifications;

import mockit.integration.junit4.JMockit;
import org.junit.Assert;
import org.junit.Test;

import com.synapsense.bacnet.network.jni.MalformedPacketException;
import com.synapsense.bacnet.network.jni.NotBVLLMessageException;
import com.synapsense.bacnet.network.jni.PropertyNotFoundException;
import com.synapsense.bacnet.network.jni.UnableToSendException;
import com.synapsense.bacnet.network.jni.UnableToSetUpInterfaceException;
import com.synapsense.bacnet.network.media.IP.DataPacket;
import com.synapsense.bacnet.network.media.IP.HelperUtils;
import com.synapsense.bacnet.network.media.IP.IpPacketReceiver;
import com.synapsense.bacnet.network.pdu.npdu.response.DisconnectConnectionToNetwork;
import org.junit.runner.RunWith;

@RunWith(JMockit.class)
public class DisconnectConnectionToNetworkParcerTest extends AbstractParcerTest {
	@Test
	public void sendDisconnectConnectionToNetworkTest() throws Exception {
		// DisconnectConnectionToNetwork, dnet = 4
		final byte[] testData = HelperUtils.toByteArray("810A00090180090004");
		header.Control = false;
		DisconnectConnectionToNetwork pdu = new DisconnectConnectionToNetwork(header);
		pdu.setDNET(4);
		pdu.send();

		new Verifications() {
			{
				byte[] packetBuffer;
				DataPacket dataPacket;
				packetReceiver.sendPacket(packetBuffer = withCapture(), dataPacket = withCapture());
				System.out.println(dataPacket.toString());
				String a = HelperUtils.getHexString(packetBuffer).toUpperCase();
				System.out.println(a);
				System.out.println(Arrays.toString(packetBuffer));
				System.out.println(Arrays.toString(testData));
				Assert.assertArrayEquals(testData, packetBuffer);
			}
		};
	}

	@Test
	public void receiveDisconnectConnectionToNetworkTest() throws UnableToSetUpInterfaceException,
	        UnableToSendException, IOException, InterruptedException, PropertyNotFoundException,
	        MalformedPacketException {
		// DisconnectConnectionToNetwork, dnet = 4
		final byte[] testData = HelperUtils.toByteArray("810A00090180090004");
		new Expectations() {
			{
				packetReceiver.getPacket();
				result = new DatagramPacket(testData, testData.length, new InetSocketAddress(
				        InetAddress.getByAddress(SRC_IP), IpPacketReceiver.DEFAULT_BACNET_PORT));
			}
		};
		packetId = media.startRead();
		header.packetId = packetId;
		header.media = media;
		DisconnectConnectionToNetwork request = new DisconnectConnectionToNetwork(header);
		request.receive();
		Assert.assertEquals(4, request.getDNET());
	}

	@Test(expected = MalformedPacketException.class)
	public void receiveDisconnectConnectionToNetwork2Test() throws UnableToSetUpInterfaceException,
	        UnableToSendException, IOException, InterruptedException, PropertyNotFoundException,
	        MalformedPacketException, NotBVLLMessageException {
		// Malformed DisconnectConnectionToNetwork
		final byte[] testData = HelperUtils.toByteArray("810A0007018009");
		new Expectations() {
			{
				packetReceiver.getPacket();
				result = new DatagramPacket(testData, testData.length, new InetSocketAddress(
				        InetAddress.getByAddress(SRC_IP), IpPacketReceiver.DEFAULT_BACNET_PORT));
			}
		};
		media.waitForPacket();
	}
}
