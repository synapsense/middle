package com.synapsense.bacnet.network.media.IP.parcer;

import com.synapsense.bacnet.network.jni.BACNetException;
import com.synapsense.bacnet.network.jni.MalformedPacketException;
import com.synapsense.bacnet.network.jni.NotBVLLMessageException;
import com.synapsense.bacnet.network.jni.PropertyNotFoundException;
import com.synapsense.bacnet.network.jni.UnableToSendException;
import com.synapsense.bacnet.network.jni.UnableToSetUpInterfaceException;
import com.synapsense.bacnet.network.media.IP.DataPacket;
import com.synapsense.bacnet.network.media.IP.HelperUtils;
import com.synapsense.bacnet.network.media.IP.IpPacketReceiver;
import com.synapsense.bacnet.network.pdu.npdu.request.EstablishConnectionToNetwork;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.Arrays;
import mockit.Expectations;
import mockit.Verifications;
import mockit.integration.junit4.JMockit;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(JMockit.class)
public class EstablishConnectionToNetworkParcerTest extends AbstractParcerTest {
	@Test
	public void sendEstablishConnectionToNetworkTest() throws Exception {
		// EstablishConnectionToNetwork, dnet = 4, termination time value = 2
		final byte[] testData = HelperUtils.toByteArray("810A000A018008000402");
		header.Control = false;
		EstablishConnectionToNetwork pdu = new EstablishConnectionToNetwork(header);
		pdu.setDNET(4);
		pdu.setTerminationTimeValue(2);
		pdu.send();

		new Verifications() {
			{
				byte[] packetBuffer;
				DataPacket dataPacket;
				packetReceiver.sendPacket(packetBuffer = withCapture(), dataPacket = withCapture());
				System.out.println(dataPacket.toString());
				String a = HelperUtils.getHexString(packetBuffer).toUpperCase();
				System.out.println(a);
				System.out.println(Arrays.toString(packetBuffer));
				System.out.println(Arrays.toString(testData));
				Assert.assertArrayEquals(testData, packetBuffer);
			}
		};
	}

	@Test
	public void receiveEstablishConnectionToNetworkTest() throws UnableToSetUpInterfaceException,
	        UnableToSendException, IOException, InterruptedException, PropertyNotFoundException,
	        MalformedPacketException {
		// EstablishConnectionToNetwork, dnet = 4, termination time value = 2
		final byte[] testData = HelperUtils.toByteArray("810B000A018008000402");
		new Expectations() {
			{
				packetReceiver.getPacket();
				result = new DatagramPacket(testData, testData.length, new InetSocketAddress(
				        InetAddress.getByAddress(SRC_IP), IpPacketReceiver.DEFAULT_BACNET_PORT));
			}
		};
		packetId = media.startRead();
		header.packetId = packetId;
		header.media = media;
		EstablishConnectionToNetwork request = new EstablishConnectionToNetwork(header);
		request.receive();
		Assert.assertEquals(4, request.getDNET());
		Assert.assertEquals(2, request.getTerminationTimeValue());
	}

	@Test(expected = MalformedPacketException.class)
	public void receiveEstablishConnectionToNetwork2Test() throws UnableToSetUpInterfaceException,
	        UnableToSendException, IOException, InterruptedException, PropertyNotFoundException,
	        MalformedPacketException, NotBVLLMessageException {
		// Malformed EstablishConnectionToNetwork
		final byte[] testData = HelperUtils.toByteArray("810B00090180080004");
		new Expectations() {
			{
				packetReceiver.getPacket();
				result = new DatagramPacket(testData, testData.length, new InetSocketAddress(
				        InetAddress.getByAddress(SRC_IP), IpPacketReceiver.DEFAULT_BACNET_PORT));
			}
		};
		media.waitForPacket();
	}
}
