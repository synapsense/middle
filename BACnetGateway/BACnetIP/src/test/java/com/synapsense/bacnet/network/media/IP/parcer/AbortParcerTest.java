package com.synapsense.bacnet.network.media.IP.parcer;

import com.synapsense.bacnet.network.jni.MalformedPacketException;
import com.synapsense.bacnet.network.jni.NotBVLLMessageException;
import com.synapsense.bacnet.network.jni.PropertyNotFoundException;
import com.synapsense.bacnet.network.jni.UnableToSendException;
import com.synapsense.bacnet.network.jni.UnableToSetUpInterfaceException;
import com.synapsense.bacnet.network.media.IP.DataPacket;
import com.synapsense.bacnet.network.media.IP.HelperUtils;
import com.synapsense.bacnet.network.media.IP.IpPacketReceiver;
import com.synapsense.bacnet.network.media.MPDUHeader;
import com.synapsense.bacnet.network.pdu.apdu.datatypes.AbortReason;
import com.synapsense.bacnet.network.pdu.apdu.request.AbortPDU;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.Arrays;
import mockit.Expectations;
import mockit.Verifications;
import mockit.integration.junit4.JMockit;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(JMockit.class)
public class AbortParcerTest extends AbstractParcerTest {

	@Test
	public void sendAbortPDU() throws Exception {
		final byte[] testData = HelperUtils.toByteArray("810A00090100710401");
		header.Control = true;
		AbortPDU abortPDU = new AbortPDU(header);
		abortPDU.setAbortReason(AbortReason.buffer_overflow);
		abortPDU.setId(4);
		abortPDU.setServer(true);
		abortPDU.send();

		new Verifications() {
			{
				byte[] packetBuffer;
				DataPacket dataPacket;
				packetReceiver.sendPacket(packetBuffer = withCapture(), dataPacket = withCapture());
				System.out.println(dataPacket.toString());
				String a = HelperUtils.getHexString(packetBuffer).toUpperCase();
				System.out.println(a);
				System.out.println(Arrays.toString(packetBuffer));
				System.out.println(Arrays.toString(testData));
				Assert.assertArrayEquals(testData, packetBuffer);
			}
		};
	}

	@Test
	public void sendAdortPDUWithNoReason() throws Exception {
		// Client AbortPDU with invoke ID = 4 and abort reason = other
		final byte[] testData = HelperUtils.toByteArray("810A00090100700400");
		header.Control = true;
		AbortPDU abortPDU = new AbortPDU(header);
		abortPDU.setId(4);
		abortPDU.setServer(false);
		abortPDU.send();
		// Since we didn't specify the abort reason, it's supposed to be "other"
		new Verifications() {
			{
				byte[] packetBuffer;
				DataPacket dataPacket;
				packetReceiver.sendPacket(packetBuffer = withCapture(), dataPacket = withCapture());
				System.out.println(dataPacket.toString());
				String a = HelperUtils.getHexString(packetBuffer).toUpperCase();
				System.out.println(a);
				System.out.println(Arrays.toString(packetBuffer));
				System.out.println(Arrays.toString(testData));
				Assert.assertArrayEquals(testData, packetBuffer);
			}
		};
	}

	@Test
	public void receiveAdortPDUTest() throws UnableToSetUpInterfaceException, UnableToSendException, IOException,
	        InterruptedException, PropertyNotFoundException, MalformedPacketException {
		// Client AbortPDU with invoke id = 22 and abort reason = preempted by
		// higher priority task
		final byte[] testData = HelperUtils.toByteArray("810A00090100701603");
		new Expectations() {
			{
				packetReceiver.getPacket();
				result = new DatagramPacket(testData, testData.length, new InetSocketAddress(
				        InetAddress.getByAddress(SRC_IP), IpPacketReceiver.DEFAULT_BACNET_PORT));
			}
		};
		packetId = media.startRead();
		header.packetId = packetId;
		header.media = media;
		AbortPDU request = new AbortPDU(header);
		request.receive();
		Assert.assertEquals(22, request.getInvokeId());
		Assert.assertEquals(AbortReason.preempted_by_higher_priority_task, request.getAbortReason());
		Assert.assertFalse(request.getServer());
	}

	@Test
	public void receiveServerAbortPDUTest() throws UnableToSetUpInterfaceException, UnableToSendException, IOException,
	        InterruptedException, PropertyNotFoundException, MalformedPacketException {
		// Server AbortPDU with invoke id = 22 and abort reason = invalid apdu
		// in this state
		final byte[] testData = HelperUtils.toByteArray("810A00090100711600");
		new Expectations() {
			{
				packetReceiver.getPacket();
				result = new DatagramPacket(testData, testData.length, new InetSocketAddress(
				        InetAddress.getByAddress(SRC_IP), IpPacketReceiver.DEFAULT_BACNET_PORT));
			}
		};
		packetId = media.startRead();
		MPDUHeader header = new MPDUHeader();
		header.packetId = packetId;
		header.media = media;
		AbortPDU request = new AbortPDU(header);
		request.receive();
		Assert.assertEquals(22, request.getInvokeId());
		Assert.assertEquals(AbortReason.other, request.getAbortReason());
		Assert.assertTrue(request.getServer());
	}

	@Test(expected = MalformedPacketException.class)
	public void receiveMailformedAbortPDUTest() throws UnableToSetUpInterfaceException, UnableToSendException,
	        IOException, PropertyNotFoundException, MalformedPacketException, NotBVLLMessageException {
		final byte[] testData = HelperUtils.toByteArray("810A0009010070");
		new Expectations() {
			{
				packetReceiver.getPacket();
				result = new DatagramPacket(testData, testData.length, new InetSocketAddress(
				        InetAddress.getByAddress(SRC_IP), IpPacketReceiver.DEFAULT_BACNET_PORT));
			}
		};
		media.waitForPacket();

	}

	@Test(expected = MalformedPacketException.class)
	public void receiveMailformedAbortPDU2Test() throws UnableToSetUpInterfaceException, UnableToSendException,
	        IOException, PropertyNotFoundException, MalformedPacketException, NotBVLLMessageException {
		final byte[] testData = HelperUtils.toByteArray("810A00090100");
		new Expectations() {
			{
				packetReceiver.getPacket();
				result = new DatagramPacket(testData, testData.length, new InetSocketAddress(
				        InetAddress.getByAddress(SRC_IP), IpPacketReceiver.DEFAULT_BACNET_PORT));
			}
		};
		media.waitForPacket();
	}
}
