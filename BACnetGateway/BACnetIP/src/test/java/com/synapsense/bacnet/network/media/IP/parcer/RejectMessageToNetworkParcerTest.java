package com.synapsense.bacnet.network.media.IP.parcer;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.Arrays;

import mockit.Expectations;
import mockit.Verifications;

import mockit.integration.junit4.JMockit;
import org.junit.Assert;
import org.junit.Test;

import com.synapsense.bacnet.network.jni.MalformedPacketException;
import com.synapsense.bacnet.network.jni.NotBVLLMessageException;
import com.synapsense.bacnet.network.jni.PropertyNotFoundException;
import com.synapsense.bacnet.network.jni.UnableToSendException;
import com.synapsense.bacnet.network.jni.UnableToSetUpInterfaceException;
import com.synapsense.bacnet.network.media.IP.DataPacket;
import com.synapsense.bacnet.network.media.IP.HelperUtils;
import com.synapsense.bacnet.network.media.IP.IpPacketReceiver;
import com.synapsense.bacnet.network.pdu.npdu.response.RejectMessageToNetwork;
import org.junit.runner.RunWith;

@RunWith(JMockit.class)
public class RejectMessageToNetworkParcerTest extends AbstractParcerTest {
	@Test
	public void sendRejectMessageToNetworkTest() throws Exception {
		// RejectMessageToNetwork, dnet = 5 and reject reason = 0
		final byte[] testData = HelperUtils.toByteArray("810A000A018003000005");
		header.Control = false;
		RejectMessageToNetwork pdu = new RejectMessageToNetwork(header);
		pdu.setNDNET(5);
		pdu.setRejectReason(0);
		pdu.send();

		new Verifications() {
			{
				byte[] packetBuffer;
				DataPacket dataPacket;
				packetReceiver.sendPacket(packetBuffer = withCapture(), dataPacket = withCapture());
				System.out.println(dataPacket.toString());
				String a = HelperUtils.getHexString(packetBuffer).toUpperCase();
				System.out.println(a);
				System.out.println(Arrays.toString(packetBuffer));
				System.out.println(Arrays.toString(testData));
				Assert.assertArrayEquals(testData, packetBuffer);
			}
		};
	}

	@Test
	public void receiveRejectMessageToNetworkTest() throws UnableToSetUpInterfaceException, UnableToSendException,
	        IOException, InterruptedException, PropertyNotFoundException, MalformedPacketException {
		// RejectMessageToNetwork, dnet = 5 and reject reason = 0
		final byte[] testData = HelperUtils.toByteArray("810A000A018003000005");
		new Expectations() {
			{
				packetReceiver.getPacket();
				result = new DatagramPacket(testData, testData.length, new InetSocketAddress(
				        InetAddress.getByAddress(SRC_IP), IpPacketReceiver.DEFAULT_BACNET_PORT));
			}
		};
		packetId = media.startRead();
		header.packetId = packetId;
		header.media = media;
		RejectMessageToNetwork request = new RejectMessageToNetwork(header);
		request.receive();
		Assert.assertEquals(request.getNDNET(), 5);
		Assert.assertEquals(request.getRejectReason(), 0);
	}

	@Test(expected = MalformedPacketException.class)
	public void receiveRejectMessageToNetwork3Test() throws UnableToSetUpInterfaceException, UnableToSendException,
	        IOException, InterruptedException, PropertyNotFoundException, MalformedPacketException,
	        NotBVLLMessageException {
		// RejectMessageToNetwork, dnet = 5 and reject reason = 0
		final byte[] testData = HelperUtils.toByteArray("810A00090180030000");
		new Expectations() {
			{
				packetReceiver.getPacket();
				result = new DatagramPacket(testData, testData.length, new InetSocketAddress(
				        InetAddress.getByAddress(SRC_IP), IpPacketReceiver.DEFAULT_BACNET_PORT));
			}
		};
		media.waitForPacket();
	}
}
