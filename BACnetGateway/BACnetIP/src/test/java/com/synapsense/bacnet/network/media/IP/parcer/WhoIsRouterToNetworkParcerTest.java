package com.synapsense.bacnet.network.media.IP.parcer;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.Arrays;

import mockit.Expectations;
import mockit.Verifications;

import mockit.integration.junit4.JMockit;
import org.junit.Assert;
import org.junit.Test;

import com.synapsense.bacnet.network.Constants;
import com.synapsense.bacnet.network.jni.MalformedPacketException;
import com.synapsense.bacnet.network.jni.PropertyNotFoundException;
import com.synapsense.bacnet.network.jni.UnableToSendException;
import com.synapsense.bacnet.network.jni.UnableToSetUpInterfaceException;
import com.synapsense.bacnet.network.media.IP.DataPacket;
import com.synapsense.bacnet.network.media.IP.HelperUtils;
import com.synapsense.bacnet.network.media.IP.IpPacketReceiver;
import com.synapsense.bacnet.network.pdu.npdu.request.WhoIsRouterToNetwork;
import org.junit.runner.RunWith;

@RunWith(JMockit.class)
public class WhoIsRouterToNetworkParcerTest extends AbstractParcerTest {

	@Test
	public void sendWhoIsRouterToNetworkTest() throws Exception {
		// WhoIsRouterToNetwork, dnet = 2
		final byte[] testData = HelperUtils.toByteArray("810A00090180000002");
		header.Control = false;
		WhoIsRouterToNetwork pdu = new WhoIsRouterToNetwork(header);
		pdu.setNdnet(2);
		pdu.send();

		new Verifications() {
			{
				byte[] packetBuffer;
				DataPacket dataPacket;
				packetReceiver.sendPacket(packetBuffer = withCapture(), dataPacket = withCapture());
				System.out.println(dataPacket.toString());
				String a = HelperUtils.getHexString(packetBuffer).toUpperCase();
				System.out.println(a);
				System.out.println(Arrays.toString(packetBuffer));
				System.out.println(Arrays.toString(testData));
				Assert.assertArrayEquals(testData, packetBuffer);
			}
		};
	}

	@Test
	public void sendWhoIsRouterToNetworkNoDNETTest() throws Exception {
		// WhoIsRouterToNetwork pdu without dnet
		final byte[] testData = HelperUtils.toByteArray("810A0007018000");
		header.Control = false;
		WhoIsRouterToNetwork pdu = new WhoIsRouterToNetwork(header);
		pdu.send();

		new Verifications() {
			{
				byte[] packetBuffer;
				DataPacket dataPacket;
				packetReceiver.sendPacket(packetBuffer = withCapture(), dataPacket = withCapture());
				System.out.println(dataPacket.toString());
				String a = HelperUtils.getHexString(packetBuffer).toUpperCase();
				System.out.println(a);
				System.out.println(Arrays.toString(packetBuffer));
				System.out.println(Arrays.toString(testData));
				Assert.assertArrayEquals(testData, packetBuffer);
			}
		};
	}

	@Test
	public void receiveWhoIsRouterToNetworkNoDnetTest() throws UnableToSetUpInterfaceException, UnableToSendException,
	        IOException, InterruptedException, PropertyNotFoundException, MalformedPacketException {
		// WhoIsRouterToNetwork pdu without dnet
		final byte[] testData = HelperUtils.toByteArray("810A0007018000");
		new Expectations() {
			{
				packetReceiver.getPacket();
				result = new DatagramPacket(testData, testData.length, new InetSocketAddress(
				        InetAddress.getByAddress(SRC_IP), IpPacketReceiver.DEFAULT_BACNET_PORT));
			}
		};
		packetId = media.startRead();
		header.packetId = packetId;
		header.media = media;
		WhoIsRouterToNetwork request = new WhoIsRouterToNetwork(header);
		request.receive();
		Assert.assertEquals(Constants.PROPERTY_NOT_SPECIFIED, request.getNdnet());
	}

	@Test
	public void receiveWhoIsRouterToNetworkNoDnet2Test() throws UnableToSetUpInterfaceException, UnableToSendException,
	        IOException, InterruptedException, PropertyNotFoundException, MalformedPacketException {
		// WhoIsRouterToNetwork, dnet = 65
		final byte[] testData = HelperUtils.toByteArray("810A00090180000041");
		new Expectations() {
			{
				packetReceiver.getPacket();
				result = new DatagramPacket(testData, testData.length, new InetSocketAddress(
				        InetAddress.getByAddress(SRC_IP), IpPacketReceiver.DEFAULT_BACNET_PORT));
			}
		};
		packetId = media.startRead();
		header.packetId = packetId;
		header.media = media;
		WhoIsRouterToNetwork request = new WhoIsRouterToNetwork(header);
		request.receive();
		Assert.assertEquals(65, request.getNdnet());
	}

}
