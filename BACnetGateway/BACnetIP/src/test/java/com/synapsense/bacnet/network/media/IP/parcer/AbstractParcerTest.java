package com.synapsense.bacnet.network.media.IP.parcer;

import java.io.ByteArrayOutputStream;
import java.util.Arrays;

import mockit.Mocked;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;

import com.synapsense.bacnet.network.jni.UnableToSetUpInterfaceException;
import com.synapsense.bacnet.network.media.MPDUHeader;
import com.synapsense.bacnet.network.media.IP.HelperUtils;
import com.synapsense.bacnet.network.media.IP.IP;
import com.synapsense.bacnet.network.media.IP.IpPacketReceiver;

@Ignore
public class AbstractParcerTest {
	@Mocked
	IpPacketReceiver packetReceiver;
	static MPDUHeader header;
	static IP media;
	static int packetId;
	static ByteArrayOutputStream testDataStream;
	static final byte[] SRC_IP = Arrays.copyOf(HelperUtils.getDestinationMac("192.168.102.2"), 4);

	@Before
	public void setUp() {
		header = new MPDUHeader();
		header.media = media;
		packetId = media.createPacket();
		header.DST_MAC = HelperUtils.getDestinationMac("192.168.102.1:47808");
		testDataStream = new ByteArrayOutputStream();

	}

	@After
	public void tearDown() {
		media.readDone(packetId);
	}

	@BeforeClass
	static public void setUpClass() throws UnableToSetUpInterfaceException {
		media = new IP();
		media.setMask(24);
	}
}
