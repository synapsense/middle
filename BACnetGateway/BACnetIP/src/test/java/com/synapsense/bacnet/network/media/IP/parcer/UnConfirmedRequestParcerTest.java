package com.synapsense.bacnet.network.media.IP.parcer;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.Arrays;

import mockit.Expectations;
import mockit.Verifications;

import mockit.integration.junit4.JMockit;
import org.junit.Assert;
import org.junit.Test;

import com.synapsense.bacnet.network.jni.MalformedPacketException;
import com.synapsense.bacnet.network.jni.NotBVLLMessageException;
import com.synapsense.bacnet.network.jni.Property;
import com.synapsense.bacnet.network.jni.PropertyNotFoundException;
import com.synapsense.bacnet.network.jni.UnableToSendException;
import com.synapsense.bacnet.network.jni.UnableToSetUpInterfaceException;
import com.synapsense.bacnet.network.media.IP.DataPacket;
import com.synapsense.bacnet.network.media.IP.HelperUtils;
import com.synapsense.bacnet.network.media.IP.IpPacketReceiver;
import com.synapsense.bacnet.network.pdu.apdu.APDUBasePacket;
import com.synapsense.bacnet.network.pdu.apdu.request.UnconfirmedRequestPDU;
import com.synapsense.bacnet.network.pdu.apdu.svc.request.WhoIs;
import org.junit.runner.RunWith;

@RunWith(JMockit.class)
public class UnConfirmedRequestParcerTest extends AbstractParcerTest {

	@Test
	public void sendWhoIsTest() throws Exception {
		// WhoIs without payload
		final byte[] testData = HelperUtils.toByteArray("810A000801001008");
		// Payload in this test is empty
		WhoIs request = new WhoIs(header);
		APDUBasePacket pdu = request.getPacket();
		pdu.send();

		new Verifications() {
			{
				byte[] packetBuffer;
				DataPacket dataPacket;
				packetReceiver.sendPacket(packetBuffer = withCapture(), dataPacket = withCapture());
				System.out.println(dataPacket.toString());
				String a = HelperUtils.getHexString(packetBuffer).toUpperCase();
				System.out.println(a);
				System.out.println(Arrays.toString(packetBuffer));
				System.out.println(Arrays.toString(testData));
				Assert.assertArrayEquals(testData, packetBuffer);
			}
		};
	}

	@Test
	public void sendWhoIsWithPayloadTest() throws Exception {

		// WhoIs without with low limit = 1 and high limit = 122
		final byte[] testData = HelperUtils.toByteArray("810A000C010010080901197A");

		WhoIs request = new WhoIs(header);
		request.setDeviceInstanceRangeLowLimit(1);
		request.setDeviceInstanceRangeHighLimit(122);
		APDUBasePacket pdu = request.getPacket();
		pdu.send();
		new Verifications() {
			{
				byte[] packetBuffer;
				DataPacket dataPacket;
				packetReceiver.sendPacket(packetBuffer = withCapture(), dataPacket = withCapture());
				System.out.println(dataPacket.toString());
				String a = HelperUtils.getHexString(packetBuffer).toUpperCase();
				System.out.println(a);
				System.out.println(Arrays.toString(packetBuffer));
				System.out.println(Arrays.toString(testData));
				Assert.assertArrayEquals(testData, packetBuffer);
			}
		};
	}

	@Test
	public void receiveWhoIsTest() throws UnableToSetUpInterfaceException, UnableToSendException, IOException,
	        InterruptedException, PropertyNotFoundException, MalformedPacketException {
		// Who-Is request with no payload
		final byte[] testData = HelperUtils.toByteArray("810A000801001008");

		new Expectations() {
			{
				packetReceiver.getPacket();
				result = new DatagramPacket(testData, testData.length, new InetSocketAddress(
				        InetAddress.getByAddress(SRC_IP), IpPacketReceiver.DEFAULT_BACNET_PORT));
			}
		};

		packetId = media.startRead();
		Assert.assertArrayEquals(new byte[] {}, media.readByteArray(packetId, Property.PDU_PAYLOAD));
	}

	@Test
	public void receiveWhoIsWithPayloadTest() throws UnableToSetUpInterfaceException, UnableToSendException,
	        IOException, InterruptedException, PropertyNotFoundException, MalformedPacketException {
		// Who-Is request with low limit = 22 and high limit = 77778
		final byte[] testData = HelperUtils.toByteArray("810A000E0100100809161B012FD2");

		new Expectations() {
			{
				packetReceiver.getPacket();
				result = new DatagramPacket(testData, testData.length, new InetSocketAddress(
				        InetAddress.getByAddress(SRC_IP), IpPacketReceiver.DEFAULT_BACNET_PORT));
			}
		};

		packetId = media.startRead();
		header.packetId = packetId;
		UnconfirmedRequestPDU pdu = new UnconfirmedRequestPDU(header);
		pdu.receive();
		WhoIs request = new WhoIs(pdu);
		request.decodeBuffer(media.readByteArray(packetId, Property.PDU_PAYLOAD));
		Assert.assertEquals(22, request.getDeviceInstanceRangeLowLimit());
		Assert.assertEquals(77778, request.getDeviceInstanceRangeHighLimit());
	}

	@Test(expected = MalformedPacketException.class)
	public void receiveMalformedWhoIsTest() throws UnableToSetUpInterfaceException, UnableToSendException, IOException,
	        InterruptedException, PropertyNotFoundException, MalformedPacketException, NotBVLLMessageException {
		// malformed Who-Is request
		final byte[] testData = HelperUtils.toByteArray("810A000E0100100809161B012F");

		new Expectations() {
			{
				packetReceiver.getPacket();
				result = new DatagramPacket(testData, testData.length, new InetSocketAddress(
				        InetAddress.getByAddress(SRC_IP), IpPacketReceiver.DEFAULT_BACNET_PORT));
			}
		};

		media.waitForPacket();
	}

	@Test(expected = MalformedPacketException.class)
	public void receiveMalformedWhoIs2Test() throws UnableToSetUpInterfaceException, UnableToSendException,
	        IOException, InterruptedException, PropertyNotFoundException, MalformedPacketException,
	        NotBVLLMessageException {
		// Malformed Who-Is request
		final byte[] testData = HelperUtils.toByteArray("810A000E0100100809161B");

		new Expectations() {
			{
				packetReceiver.getPacket();
				result = new DatagramPacket(testData, testData.length, new InetSocketAddress(
				        InetAddress.getByAddress(SRC_IP), IpPacketReceiver.DEFAULT_BACNET_PORT));
			}
		};

		media.waitForPacket();
	}

}
