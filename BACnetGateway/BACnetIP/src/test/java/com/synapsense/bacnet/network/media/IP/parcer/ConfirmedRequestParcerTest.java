package com.synapsense.bacnet.network.media.IP.parcer;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.Arrays;

import mockit.Expectations;
import mockit.Verifications;

import mockit.integration.junit4.JMockit;
import org.junit.Assert;
import org.junit.Test;

import com.synapsense.bacnet.enums.BACnetAPDUSize;
import com.synapsense.bacnet.network.jni.MalformedPacketException;
import com.synapsense.bacnet.network.jni.NotBVLLMessageException;
import com.synapsense.bacnet.network.jni.Property;
import com.synapsense.bacnet.network.jni.PropertyNotFoundException;
import com.synapsense.bacnet.network.jni.UnableToSendException;
import com.synapsense.bacnet.network.jni.UnableToSetUpInterfaceException;
import com.synapsense.bacnet.network.media.IP.DataPacket;
import com.synapsense.bacnet.network.media.IP.HelperUtils;
import com.synapsense.bacnet.network.media.IP.IpPacketReceiver;
import com.synapsense.bacnet.network.pdu.apdu.datatypes.ConfirmedServiceChoice;
import com.synapsense.bacnet.network.pdu.apdu.request.ConfirmedRequestPDU;
import com.synapsense.bacnet.network.pdu.apdu.svc.request.ReadProperty;
import com.synapsense.bacnet.system.BACnetPropertyId;
import com.synapsense.bacnet.system.shared.BACnetObjectIdentifier;
import com.synapsense.bacnet.system.shared.BACnetObjectType;
import org.junit.runner.RunWith;

@RunWith(JMockit.class)
public class ConfirmedRequestParcerTest extends AbstractParcerTest {

	@Test
	public void sendNotSegmentedConfirmedRequestTest() throws Exception {
		// Not Segmented ComplexAckPDU with invoke id = 24, segmented = false,
		// maxAPDUAccepted = 1476
		final byte[] testData = HelperUtils.toByteArray("810A001101040205180C0C00000000191C");
		header.Control = true;
		ReadProperty ack = new ReadProperty(header);
		ack.setObjectIdentifier(new BACnetObjectIdentifier(BACnetObjectType.ANALOG_INPUT, 0));
		ack.setPropertyIdentifier(BACnetPropertyId.DESCRIPTION);
		ack.setSegmented_message(false);
		ack.setSegmented_response_accepted(true);
		ack.setMax_APDU_length_accepted(BACnetAPDUSize.UP_TO_1476);
		ack.setMax_segments_accepted(0);

		ack.setInvokeID(24);

		ConfirmedRequestPDU pdu = (ConfirmedRequestPDU) ack.getPacket();

		pdu.send();

		new Verifications() {
			{
				byte[] packetBuffer;
				DataPacket dataPacket;
				packetReceiver.sendPacket(packetBuffer = withCapture(), dataPacket = withCapture());
				System.out.println(dataPacket.toString());
				String a = HelperUtils.getHexString(packetBuffer).toUpperCase();
				System.out.println(a);
				System.out.println(Arrays.toString(packetBuffer));
				System.out.println(Arrays.toString(testData));
				Assert.assertArrayEquals(testData, packetBuffer);
			}
		};
	}

	@Test
	public void sendSegmentedConfirmedRequestTest() throws Exception {
		// Segmented ConfirmedRequest with invoke id = 24, sequence number = 0,
		// proposed window size = 12 with messages to follow, 6 bytes
		// segments. MaxAPDU length = 1476, 1 out of 2 segments
		final byte[] testData = HelperUtils.toByteArray("810A001101040C0518000C0C0C00000000");
		header.Control = true;
		ReadProperty svc = new ReadProperty(header);
		svc.setObjectIdentifier(new BACnetObjectIdentifier(BACnetObjectType.ANALOG_INPUT, 0));
		svc.setPropertyIdentifier(BACnetPropertyId.DESCRIPTION);
		svc.setSegmented_message(true);
		svc.setMore_follows(true);
		svc.setSequence_number(0);
		svc.setProposed_window_size(12);
		svc.setSegmented_response_accepted(false);
		svc.setMax_segments_accepted(0);
		svc.setMax_APDU_length_accepted(BACnetAPDUSize.UP_TO_1476);
		// since stack currently doesn't support segmented messages, we need to
		// put something here and replace with the appropriate segmented data
		// later
		svc.setEncodedBuffer(HelperUtils.toByteArray("00"));
		svc.setInvokeID(24);

		ConfirmedRequestPDU pdu = (ConfirmedRequestPDU) svc.getPacket();
		pdu.setBuffer(HelperUtils.toByteArray("0C00000000"));
		pdu.send();

		new Verifications() {
			{
				byte[] packetBuffer;
				DataPacket dataPacket;
				packetReceiver.sendPacket(packetBuffer = withCapture(), dataPacket = withCapture());
				System.out.println(dataPacket.toString());
				String a = HelperUtils.getHexString(packetBuffer).toUpperCase();
				System.out.println(a);
				System.out.println(Arrays.toString(packetBuffer));
				System.out.println(Arrays.toString(testData));
				Assert.assertArrayEquals(testData, packetBuffer);
			}
		};
	}

	@Test
	public void receiveNotSegmentedConfirmedRequestPDUTest() throws UnableToSetUpInterfaceException,
	        UnableToSendException, IOException, InterruptedException, PropertyNotFoundException,
	        MalformedPacketException {
		// Not Segmented ComplexAckPDU with invoke id = 24, segmented = false,
		// maxAPDUAccepted = 1476
		final byte[] testData = HelperUtils.toByteArray("810A001101040205180C0C00000000191C");
		new Expectations() {
			{
				packetReceiver.getPacket();
				result = new DatagramPacket(testData, testData.length, new InetSocketAddress(
				        InetAddress.getByAddress(SRC_IP), IpPacketReceiver.DEFAULT_BACNET_PORT));
			}
		};
		packetId = media.startRead();
		header.packetId = packetId;
		header.media = media;
		ConfirmedRequestPDU request = new ConfirmedRequestPDU(header);
		request.receive();
		Assert.assertEquals(24, request.getInvokeId());
		Assert.assertFalse(request.isSegmented_message());
		Assert.assertEquals(BACnetAPDUSize.UP_TO_1476, request.getMax_APDU_length_accepted());
		Assert.assertEquals(ConfirmedServiceChoice.readProperty, request.getService_choice());
		Assert.assertTrue(request.isSegmented_response_accepted());
		Assert.assertArrayEquals(HelperUtils.toByteArray("0C00000000191C"), request.getBuffer());
	}

	@Test
	public void receiveSegmentedConfirmedRequestPDUTest() throws UnableToSetUpInterfaceException,
	        UnableToSendException, IOException, InterruptedException, PropertyNotFoundException,
	        MalformedPacketException {
		// Segmented ConfirmedRequest with invoke id = 24, sequence number = 0,
		// proposed window size = 12 with messages to follow, 6 bytes
		// segments. MaxAPDU length = 1476, 1 out of 2 segments
		final byte[] testData = HelperUtils.toByteArray("810A001101040C0518000C0C0C00000000");
		new Expectations() {
			{
				packetReceiver.getPacket();
				result = new DatagramPacket(testData, testData.length, new InetSocketAddress(
				        InetAddress.getByAddress(SRC_IP), IpPacketReceiver.DEFAULT_BACNET_PORT));
			}
		};
		packetId = media.startRead();
		header.packetId = packetId;
		header.media = media;
		ConfirmedRequestPDU request = new ConfirmedRequestPDU(header);
		request.receive();
		Assert.assertEquals(24, request.getInvokeId());
		Assert.assertEquals(0, request.getSequence_number());
		Assert.assertEquals(12, request.getProposed_window_size());
		Assert.assertTrue(request.isSegmented_message());
		Assert.assertTrue(request.isMore_follows());
		Assert.assertFalse(request.isSegmented_response_accepted());
		Assert.assertEquals(BACnetAPDUSize.UP_TO_1476, request.getMax_APDU_length_accepted());
		Assert.assertArrayEquals(HelperUtils.toByteArray("0C00000000"),
		        media.readByteArray(packetId, Property.PDU_PAYLOAD));
	}

	@Test(expected = MalformedPacketException.class)
	public void receiveMalformedSegmentedComplexAckTest() throws UnableToSetUpInterfaceException,
	        UnableToSendException, IOException, InterruptedException, PropertyNotFoundException,
	        MalformedPacketException, NotBVLLMessageException {
		// Malformed request
		final byte[] testData = HelperUtils.toByteArray("810A001101040D0518000C0C0C00000000");

		new Expectations() {
			{
				packetReceiver.getPacket();
				result = new DatagramPacket(testData, testData.length, new InetSocketAddress(
				        InetAddress.getByAddress(SRC_IP), IpPacketReceiver.DEFAULT_BACNET_PORT));
			}
		};

		media.waitForPacket();
	}

	@Test(expected = MalformedPacketException.class)
	public void receiveMalformedSegmentedComplexAck2Test() throws UnableToSetUpInterfaceException,
	        UnableToSendException, IOException, InterruptedException, PropertyNotFoundException,
	        MalformedPacketException, NotBVLLMessageException {
		// Malformed request
		final byte[] testData = HelperUtils.toByteArray("810A001101040C8518000C0C0C00000000");

		new Expectations() {
			{
				packetReceiver.getPacket();
				result = new DatagramPacket(testData, testData.length, new InetSocketAddress(
				        InetAddress.getByAddress(SRC_IP), IpPacketReceiver.DEFAULT_BACNET_PORT));
			}
		};

		media.waitForPacket();
	}

	@Test(expected = MalformedPacketException.class)
	public void receiveMalformedSegmentedComplexAck3Test() throws UnableToSetUpInterfaceException,
	        UnableToSendException, IOException, InterruptedException, PropertyNotFoundException,
	        MalformedPacketException, NotBVLLMessageException {
		// Malformed request
		final byte[] testData = HelperUtils.toByteArray("810A000701040C");

		new Expectations() {
			{
				packetReceiver.getPacket();
				result = new DatagramPacket(testData, testData.length, new InetSocketAddress(
				        InetAddress.getByAddress(SRC_IP), IpPacketReceiver.DEFAULT_BACNET_PORT));
			}
		};

		media.waitForPacket();
	}

	@Test(expected = MalformedPacketException.class)
	public void receiveMalformedSegmentedComplexAck4Test() throws UnableToSetUpInterfaceException,
	        UnableToSendException, IOException, InterruptedException, PropertyNotFoundException,
	        MalformedPacketException, NotBVLLMessageException {
		// Malformed request
		final byte[] testData = HelperUtils.toByteArray("810A000A01040C051800");

		new Expectations() {
			{
				packetReceiver.getPacket();
				result = new DatagramPacket(testData, testData.length, new InetSocketAddress(
				        InetAddress.getByAddress(SRC_IP), IpPacketReceiver.DEFAULT_BACNET_PORT));
			}
		};

		media.waitForPacket();
	}

	@Test(expected = MalformedPacketException.class)
	public void receiveMalformedSegmentedComplexAck5Test() throws UnableToSetUpInterfaceException,
	        UnableToSendException, IOException, InterruptedException, PropertyNotFoundException,
	        MalformedPacketException, NotBVLLMessageException {
		// Malformed request
		final byte[] testData = HelperUtils.toByteArray("810A000B01040C0518000C");

		new Expectations() {
			{
				packetReceiver.getPacket();
				result = new DatagramPacket(testData, testData.length, new InetSocketAddress(
				        InetAddress.getByAddress(SRC_IP), IpPacketReceiver.DEFAULT_BACNET_PORT));
			}
		};

		media.waitForPacket();
	}
}
