package com.synapsense.bacnet.network.media.IP.parcer;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.Arrays;

import mockit.Expectations;
import mockit.Verifications;

import mockit.integration.junit4.JMockit;
import org.junit.Assert;
import org.junit.Test;

import com.synapsense.bacnet.network.jni.MalformedPacketException;
import com.synapsense.bacnet.network.jni.NotBVLLMessageException;
import com.synapsense.bacnet.network.jni.PropertyNotFoundException;
import com.synapsense.bacnet.network.jni.UnableToSendException;
import com.synapsense.bacnet.network.jni.UnableToSetUpInterfaceException;
import com.synapsense.bacnet.network.media.IP.DataPacket;
import com.synapsense.bacnet.network.media.IP.HelperUtils;
import com.synapsense.bacnet.network.media.IP.IpPacketReceiver;
import com.synapsense.bacnet.network.pdu.npdu.response.InitializeRoutingTableAck;
import com.synapsense.bacnet.network.routing.RoutingService;
import org.junit.runner.RunWith;

@RunWith(JMockit.class)
public class InitializeRoutingTableAckParcerTest extends AbstractParcerTest {
	@Test
	public void sendInitializeRoutingTableAckTest() throws Exception {
		// InitializeRoutingTableAck with one route Dnet = 3, route id = 4 and
		// port
		// info = "portinfo"
		final byte[] testData = HelperUtils.toByteArray("810A00140180070100030408706F7274696E666F");

		header.Control = false;
		InitializeRoutingTableAck pdu = new InitializeRoutingTableAck(header);
		pdu.setRouteInfo(new String[] { "3_4_portinfo" });
		pdu.send();

		new Verifications() {
			{
				byte[] packetBuffer;
				DataPacket dataPacket;
				packetReceiver.sendPacket(packetBuffer = withCapture(), dataPacket = withCapture());
				System.out.println(dataPacket.toString());
				String a = HelperUtils.getHexString(packetBuffer).toUpperCase();
				System.out.println(a);
				System.out.println(Arrays.toString(packetBuffer));
				System.out.println(Arrays.toString(testData));
				Assert.assertArrayEquals(testData, packetBuffer);
			}
		};
	}

	@Test
	public void sendInitializeRoutingTableAck2Test() throws Exception {
		// InitializeRoutingTableAck with empty route info
		final byte[] testData = HelperUtils.toByteArray("810A000801800700");
		RoutingService.dropTable();
		header.Control = false;
		InitializeRoutingTableAck pdu = new InitializeRoutingTableAck(header);
		pdu.send();

		new Verifications() {
			{
				byte[] packetBuffer;
				DataPacket dataPacket;
				packetReceiver.sendPacket(packetBuffer = withCapture(), dataPacket = withCapture());
				System.out.println(dataPacket.toString());
				String a = HelperUtils.getHexString(packetBuffer).toUpperCase();
				System.out.println(a);
				System.out.println(Arrays.toString(packetBuffer));
				System.out.println(Arrays.toString(testData));
				Assert.assertArrayEquals(testData, packetBuffer);
			}
		};
	}

	@Test
	public void sendInitializeRoutingTableAck3Test() throws Exception {
		// InitializeRoutingTableAck with two routes
		// Dnet = 3, route id = 4 and port info = "portinfo"
		// Dnet = 4, route id = 7 and port info = "fff"
		final byte[] testData = HelperUtils.toByteArray("810A001B0180070200030408706F7274696E666F00040703666666");

		header.Control = false;
		InitializeRoutingTableAck pdu = new InitializeRoutingTableAck(header);
		pdu.setRouteInfo(new String[] { "3_4_portinfo", "4_7_fff" });
		pdu.send();

		new Verifications() {
			{
				byte[] packetBuffer;
				DataPacket dataPacket;
				packetReceiver.sendPacket(packetBuffer = withCapture(), dataPacket = withCapture());
				System.out.println(dataPacket.toString());
				String a = HelperUtils.getHexString(packetBuffer).toUpperCase();
				System.out.println(a);
				System.out.println(Arrays.toString(packetBuffer));
				System.out.println(Arrays.toString(testData));
				Assert.assertArrayEquals(testData, packetBuffer);
			}
		};
	}

	@Test
	public void receiveInitializeRoutingTableAckTest() throws UnableToSetUpInterfaceException, UnableToSendException,
	        IOException, InterruptedException, PropertyNotFoundException, MalformedPacketException {
		// InitializeRoutingTableAck with one route Dnet = 3, route id = 4 and
		// port
		// info = "portinfo"
		final byte[] testData = HelperUtils.toByteArray("810A00140180070100030408706F7274696E666F");
		new Expectations() {
			{
				packetReceiver.getPacket();
				result = new DatagramPacket(testData, testData.length, new InetSocketAddress(
				        InetAddress.getByAddress(SRC_IP), IpPacketReceiver.DEFAULT_BACNET_PORT));
			}
		};
		packetId = media.startRead();
		header.packetId = packetId;
		header.media = media;
		InitializeRoutingTableAck request = new InitializeRoutingTableAck(header);
		request.receive();
		Assert.assertArrayEquals(new String[] { "3_4_portinfo" }, request.getRouteInfo());
	}

	@Test
	public void receiveInitializeRoutingTableAck1Test() throws UnableToSetUpInterfaceException, UnableToSendException,
	        IOException, InterruptedException, PropertyNotFoundException, MalformedPacketException {
		// InitializeRoutingTableAck with one route Dnet = 3, route id = 4 and
		// port
		// info = "portinfo"
		final byte[] testData = HelperUtils.toByteArray("810A000C0180070100030400");
		new Expectations() {
			{
				packetReceiver.getPacket();
				result = new DatagramPacket(testData, testData.length, new InetSocketAddress(
				        InetAddress.getByAddress(SRC_IP), IpPacketReceiver.DEFAULT_BACNET_PORT));
			}
		};
		packetId = media.startRead();
		header.packetId = packetId;
		header.media = media;
		InitializeRoutingTableAck request = new InitializeRoutingTableAck(header);
		request.receive();
		Assert.assertArrayEquals(new String[] { "3_4_" }, request.getRouteInfo());
	}

	@Test
	public void receiveInitializeRoutingTableAck2Test() throws UnableToSetUpInterfaceException, UnableToSendException,
	        IOException, InterruptedException, PropertyNotFoundException, MalformedPacketException {
		// InitializeRoutingTableAck with no routes
		final byte[] testData = HelperUtils.toByteArray("810A000801800700");
		new Expectations() {
			{
				packetReceiver.getPacket();
				result = new DatagramPacket(testData, testData.length, new InetSocketAddress(
				        InetAddress.getByAddress(SRC_IP), IpPacketReceiver.DEFAULT_BACNET_PORT));
			}
		};
		packetId = media.startRead();
		header.packetId = packetId;
		header.media = media;
		InitializeRoutingTableAck request = new InitializeRoutingTableAck(header);
		request.receive();
		Assert.assertArrayEquals(request.getRouteInfo(), new String[] {});
	}

	@Test(expected = MalformedPacketException.class)
	public void receiveInitializeRoutingTableAck3Test() throws UnableToSetUpInterfaceException, UnableToSendException,
	        IOException, InterruptedException, PropertyNotFoundException, MalformedPacketException,
	        NotBVLLMessageException {
		// inconsistent port info and port info len
		final byte[] testData = HelperUtils.toByteArray("810A00130180070100030408706F7274696E66");
		new Expectations() {
			{
				packetReceiver.getPacket();
				result = new DatagramPacket(testData, testData.length, new InetSocketAddress(
				        InetAddress.getByAddress(SRC_IP), IpPacketReceiver.DEFAULT_BACNET_PORT));
			}
		};
		media.waitForPacket();
	}

	@Test(expected = MalformedPacketException.class)
	public void receiveInitializeRoutingTableAck4Test() throws UnableToSetUpInterfaceException, UnableToSendException,
	        IOException, InterruptedException, PropertyNotFoundException, MalformedPacketException,
	        NotBVLLMessageException {
		// short packet
		final byte[] testData = HelperUtils.toByteArray("810A000B01800701000304");
		new Expectations() {
			{
				packetReceiver.getPacket();
				result = new DatagramPacket(testData, testData.length, new InetSocketAddress(
				        InetAddress.getByAddress(SRC_IP), IpPacketReceiver.DEFAULT_BACNET_PORT));
			}
		};
		media.waitForPacket();
	}

	@Test(expected = MalformedPacketException.class)
	public void receiveInitializeRoutingTableAck5Test() throws UnableToSetUpInterfaceException, UnableToSendException,
	        IOException, InterruptedException, PropertyNotFoundException, MalformedPacketException,
	        NotBVLLMessageException {
		// short packet
		final byte[] testData = HelperUtils.toByteArray("810A0007018007");
		new Expectations() {
			{
				packetReceiver.getPacket();
				result = new DatagramPacket(testData, testData.length, new InetSocketAddress(
				        InetAddress.getByAddress(SRC_IP), IpPacketReceiver.DEFAULT_BACNET_PORT));
			}
		};
		media.waitForPacket();
	}

}
