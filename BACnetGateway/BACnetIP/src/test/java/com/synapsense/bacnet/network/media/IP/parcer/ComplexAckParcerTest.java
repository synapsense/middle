package com.synapsense.bacnet.network.media.IP.parcer;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.Arrays;

import mockit.Expectations;
import mockit.Verifications;

import mockit.integration.junit4.JMockit;
import org.junit.Assert;
import org.junit.Test;

import com.synapsense.bacnet.network.jni.MalformedPacketException;
import com.synapsense.bacnet.network.jni.NotBVLLMessageException;
import com.synapsense.bacnet.network.jni.Property;
import com.synapsense.bacnet.network.jni.PropertyNotFoundException;
import com.synapsense.bacnet.network.jni.UnableToSendException;
import com.synapsense.bacnet.network.jni.UnableToSetUpInterfaceException;
import com.synapsense.bacnet.network.media.IP.DataPacket;
import com.synapsense.bacnet.network.media.IP.HelperUtils;
import com.synapsense.bacnet.network.media.IP.IpPacketReceiver;
import com.synapsense.bacnet.network.pdu.apdu.codecs.codecs.primitive.DoubleCodec;
import com.synapsense.bacnet.network.pdu.apdu.response.ComplexACKPDU;
import com.synapsense.bacnet.network.pdu.apdu.svc.responce.ReadPropertyAck;
import com.synapsense.bacnet.system.BACnetPropertyId;
import com.synapsense.bacnet.system.shared.BACnetObjectIdentifier;
import com.synapsense.bacnet.system.shared.BACnetObjectType;
import org.junit.runner.RunWith;

@RunWith(JMockit.class)
public class ComplexAckParcerTest extends AbstractParcerTest {
	@Test
	public void sendNotSegmentedComplexAckTest() throws Exception {
		// Not Segmented ComplexAckPDU with invoke id = 66
		final byte[] testData = HelperUtils.toByteArray("810A001C010030420C0C0000000019163E550840370000000000003F");
		header.Control = true;
		ReadPropertyAck ack = new ReadPropertyAck(header);
		ack.setObjectIdentifier(new BACnetObjectIdentifier(BACnetObjectType.ANALOG_INPUT, 0));
		ack.setPropertyIdentifier(BACnetPropertyId.COV_INCREMENT);
		DoubleCodec codedc = new DoubleCodec();
		ack.setEncodedBuffer(codedc.encode(null, new Double(23)));
		ack.setSegmented_message(false);

		ack.setInvokeID(66);

		ComplexACKPDU pdu = (ComplexACKPDU) ack.getPacket();

		pdu.send();

		new Verifications() {
			{
				byte[] packetBuffer;
				DataPacket dataPacket;
				packetReceiver.sendPacket(packetBuffer = withCapture(), dataPacket = withCapture());
				System.out.println(dataPacket.toString());
				String a = HelperUtils.getHexString(packetBuffer).toUpperCase();
				System.out.println(a);
				System.out.println(Arrays.toString(packetBuffer));
				System.out.println(Arrays.toString(testData));
				Assert.assertArrayEquals(testData, packetBuffer);
			}
		};
	}

	@Test
	public void sendSegmentedComplexAckTest() throws Exception {
		// Segmented ComplexAckPDU with invoke id = 66, sequence number = 0,
		// proposed window size = 23 with messages to follow, 4 bytes
		// segments. 0 out of 5 segments
		final byte[] testData = HelperUtils.toByteArray("810A000F01003C4200170C0C0C0000");
		header.Control = true;
		ReadPropertyAck ack = new ReadPropertyAck(header);
		ack.setObjectIdentifier(new BACnetObjectIdentifier(BACnetObjectType.ANALOG_INPUT, 0));
		ack.setPropertyIdentifier(BACnetPropertyId.COV_INCREMENT);
		ack.setSegmented_message(true);
		ack.setMore_follows(true);
		ack.setSequence_number(0);
		ack.setProposed_window_size(23);
		// since stack currently doesn't support segmented messages, we need to
		// put something here and replace with the appropriate segmented data
		// later
		ack.setEncodedBuffer(HelperUtils.toByteArray("00"));
		ack.setInvokeID(66);

		ComplexACKPDU pdu = (ComplexACKPDU) ack.getPacket();
		pdu.setBuffer(HelperUtils.toByteArray("0C0C0000"));
		pdu.send();

		new Verifications() {
			{
				byte[] packetBuffer;
				DataPacket dataPacket;
				packetReceiver.sendPacket(packetBuffer = withCapture(), dataPacket = withCapture());
				System.out.println(dataPacket.toString());
				String a = HelperUtils.getHexString(packetBuffer).toUpperCase();
				System.out.println(a);
				System.out.println(Arrays.toString(packetBuffer));
				System.out.println(Arrays.toString(testData));
				Assert.assertArrayEquals(testData, packetBuffer);
			}
		};
	}

	@Test
	public void sendSegmentedComplexAck2Test() throws Exception {
		// Segmented ComplexAckPDU with invoke id = 66, sequence number = 2,
		// proposed window size = 23 with no messages to follow, 4 bytes
		// segments. Last out of 5 segments
		final byte[] testData = HelperUtils.toByteArray("810A000F0100384204170C0000003F");
		header.Control = true;
		ReadPropertyAck ack = new ReadPropertyAck(header);
		ack.setObjectIdentifier(new BACnetObjectIdentifier(BACnetObjectType.ANALOG_INPUT, 0));
		ack.setPropertyIdentifier(BACnetPropertyId.COV_INCREMENT);
		ack.setSegmented_message(true);
		ack.setMore_follows(false);
		ack.setSequence_number(4);
		ack.setProposed_window_size(23);
		// since stack currently doesn't support segmented messages, we need to
		// put something here and replace with the appropriate segmented data
		// later
		ack.setEncodedBuffer(HelperUtils.toByteArray("00"));
		ack.setInvokeID(66);

		ComplexACKPDU pdu = (ComplexACKPDU) ack.getPacket();
		pdu.setBuffer(HelperUtils.toByteArray("0000003F"));
		pdu.send();

		new Verifications() {
			{
				byte[] packetBuffer;
				DataPacket dataPacket;
				packetReceiver.sendPacket(packetBuffer = withCapture(), dataPacket = withCapture());
				System.out.println(dataPacket.toString());
				String a = HelperUtils.getHexString(packetBuffer).toUpperCase();
				System.out.println(a);
				System.out.println(Arrays.toString(packetBuffer));
				System.out.println(Arrays.toString(testData));
				Assert.assertArrayEquals(testData, packetBuffer);
			}
		};
	}

	@Test
	public void receiveNotSegmentedComplexAckPDUTest() throws UnableToSetUpInterfaceException, UnableToSendException,
	        IOException, InterruptedException, PropertyNotFoundException, MalformedPacketException {
		// Not Segmented ComplexAckPDU with invoke id = 66
		final byte[] testData = HelperUtils.toByteArray("810A001C010030420C0C0000000019163E550840370000000000003F");
		new Expectations() {
			{
				packetReceiver.getPacket();
				result = new DatagramPacket(testData, testData.length, new InetSocketAddress(
				        InetAddress.getByAddress(SRC_IP), IpPacketReceiver.DEFAULT_BACNET_PORT));
			}
		};
		packetId = media.startRead();
		header.packetId = packetId;
		header.media = media;
		ComplexACKPDU request = new ComplexACKPDU(header);
		request.receive();
		Assert.assertEquals(66, request.getInvokeId());
		Assert.assertFalse(request.isSegmented_message());
	}

	@Test
	public void receiveSegmentedComplexAckPDUTest() throws UnableToSetUpInterfaceException, UnableToSendException,
	        IOException, InterruptedException, PropertyNotFoundException, MalformedPacketException {
		// Segmented ComplexAckPDU with invoke id = 66, sequence number = 0,
		// proposed window size = 23 with messages to follow, 4 bytes
		// segments. 0 out of 5 segments
		final byte[] testData = HelperUtils.toByteArray("810A000F01003C4200170C0C0C0000");
		new Expectations() {
			{
				packetReceiver.getPacket();
				result = new DatagramPacket(testData, testData.length, new InetSocketAddress(
				        InetAddress.getByAddress(SRC_IP), IpPacketReceiver.DEFAULT_BACNET_PORT));
			}
		};
		packetId = media.startRead();
		header.packetId = packetId;
		header.media = media;
		ComplexACKPDU request = new ComplexACKPDU(header);
		request.receive();
		Assert.assertEquals(66, request.getInvokeId());
		Assert.assertEquals(0, request.getSequence_number());
		Assert.assertEquals(23, request.getProposed_window_size());
		Assert.assertTrue(request.isSegmented_message());
		Assert.assertTrue(request.isMore_follows());
		Assert.assertArrayEquals(HelperUtils.toByteArray("0C0C0000"),
		        media.readByteArray(packetId, Property.PDU_PAYLOAD));

	}

	@Test
	public void receiveSegmentedComplexAckPDU2Test() throws UnableToSetUpInterfaceException, UnableToSendException,
	        IOException, InterruptedException, PropertyNotFoundException, MalformedPacketException {
		// Segmented ComplexAckPDU with invoke id = 66, sequence number = 4,
		// proposed window size = 23 with no messages to follow, 4 bytes
		// segments. Last out of 5 segments
		final byte[] testData = HelperUtils.toByteArray("810A000F0100384204170C0000003F");
		new Expectations() {
			{
				packetReceiver.getPacket();
				result = new DatagramPacket(testData, testData.length, new InetSocketAddress(
				        InetAddress.getByAddress(SRC_IP), IpPacketReceiver.DEFAULT_BACNET_PORT));
			}
		};
		packetId = media.startRead();
		header.packetId = packetId;
		header.media = media;
		ComplexACKPDU request = new ComplexACKPDU(header);
		request.receive();
		Assert.assertEquals(66, request.getInvokeId());
		Assert.assertEquals(4, request.getSequence_number());
		Assert.assertEquals(23, request.getProposed_window_size());
		Assert.assertTrue(request.isSegmented_message());
		Assert.assertFalse(request.isMore_follows());
		Assert.assertArrayEquals(HelperUtils.toByteArray("0000003F"),
		        media.readByteArray(packetId, Property.PDU_PAYLOAD));

	}

	@Test(expected = MalformedPacketException.class)
	public void receiveMalformedSegmentedComplexAckTest() throws UnableToSetUpInterfaceException,
	        UnableToSendException, IOException, InterruptedException, PropertyNotFoundException,
	        MalformedPacketException, NotBVLLMessageException {
		// Malformed request
		final byte[] testData = HelperUtils.toByteArray("810A000F01003D4200170C0C0C0000");

		new Expectations() {
			{
				packetReceiver.getPacket();
				result = new DatagramPacket(testData, testData.length, new InetSocketAddress(
				        InetAddress.getByAddress(SRC_IP), IpPacketReceiver.DEFAULT_BACNET_PORT));
			}
		};

		media.waitForPacket();
	}

	@Test(expected = MalformedPacketException.class)
	public void receiveMalformedSegmentedComplexAck2Test() throws UnableToSetUpInterfaceException,
	        UnableToSendException, IOException, InterruptedException, PropertyNotFoundException,
	        MalformedPacketException, NotBVLLMessageException {
		// Malformed request
		final byte[] testData = HelperUtils.toByteArray("810A000701003C");

		new Expectations() {
			{
				packetReceiver.getPacket();
				result = new DatagramPacket(testData, testData.length, new InetSocketAddress(
				        InetAddress.getByAddress(SRC_IP), IpPacketReceiver.DEFAULT_BACNET_PORT));
			}
		};

		media.waitForPacket();
	}

	@Test(expected = MalformedPacketException.class)
	public void receiveMalformedSegmentedComplexAck3Test() throws UnableToSetUpInterfaceException,
	        UnableToSendException, IOException, InterruptedException, PropertyNotFoundException,
	        MalformedPacketException, NotBVLLMessageException {
		// Malformed request
		final byte[] testData = HelperUtils.toByteArray("810A000901003C4200");

		new Expectations() {
			{
				packetReceiver.getPacket();
				result = new DatagramPacket(testData, testData.length, new InetSocketAddress(
				        InetAddress.getByAddress(SRC_IP), IpPacketReceiver.DEFAULT_BACNET_PORT));
			}
		};

		media.waitForPacket();
	}

	@Test(expected = MalformedPacketException.class)
	public void receiveMalformedSegmentedComplexAck4Test() throws UnableToSetUpInterfaceException,
	        UnableToSendException, IOException, InterruptedException, PropertyNotFoundException,
	        MalformedPacketException, NotBVLLMessageException {
		// Malformed request
		final byte[] testData = HelperUtils.toByteArray("810A000A01003C420017");

		new Expectations() {
			{
				packetReceiver.getPacket();
				result = new DatagramPacket(testData, testData.length, new InetSocketAddress(
				        InetAddress.getByAddress(SRC_IP), IpPacketReceiver.DEFAULT_BACNET_PORT));
			}
		};

		media.waitForPacket();
	}
}
