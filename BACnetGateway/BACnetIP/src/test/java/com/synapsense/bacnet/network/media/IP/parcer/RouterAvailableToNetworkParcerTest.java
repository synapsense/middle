package com.synapsense.bacnet.network.media.IP.parcer;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.Arrays;

import mockit.Expectations;
import mockit.Verifications;

import mockit.integration.junit4.JMockit;
import org.junit.Assert;
import org.junit.Test;

import com.synapsense.bacnet.network.jni.MalformedPacketException;
import com.synapsense.bacnet.network.jni.NotBVLLMessageException;
import com.synapsense.bacnet.network.jni.PropertyNotFoundException;
import com.synapsense.bacnet.network.jni.UnableToSendException;
import com.synapsense.bacnet.network.jni.UnableToSetUpInterfaceException;
import com.synapsense.bacnet.network.media.IP.DataPacket;
import com.synapsense.bacnet.network.media.IP.HelperUtils;
import com.synapsense.bacnet.network.media.IP.IpPacketReceiver;
import com.synapsense.bacnet.network.pdu.npdu.response.RouterAvailableToNetwork;
import org.junit.runner.RunWith;

@RunWith(JMockit.class)
public class RouterAvailableToNetworkParcerTest extends AbstractParcerTest {
	@Test
	public void sendRouterAvailableToNetworkTest() throws Exception {
		// RouterAvailableToNetwork, dnet1 = 1, dnet2 = 1, dnet3 = 1, dnet4 = 1
		final byte[] testData = HelperUtils.toByteArray("810A000F0180050001000100010001");
		header.Control = false;
		RouterAvailableToNetwork pdu = new RouterAvailableToNetwork(header);
		pdu.SetDestinations(new int[] { 1, 1, 1, 1 });
		pdu.send();

		new Verifications() {
			{
				byte[] packetBuffer;
				DataPacket dataPacket;
				packetReceiver.sendPacket(packetBuffer = withCapture(), dataPacket = withCapture());
				System.out.println(dataPacket.toString());
				String a = HelperUtils.getHexString(packetBuffer).toUpperCase();
				System.out.println(a);
				System.out.println(Arrays.toString(packetBuffer));
				System.out.println(Arrays.toString(testData));
				Assert.assertArrayEquals(testData, packetBuffer);
			}
		};
	}

	@Test
	public void sendRouterAvailableToNetwork2Test() throws Exception {
		// RouterAvailableToNetwork, dnet1 = 55
		final byte[] testData = HelperUtils.toByteArray("810A00090180050037");
		header.Control = false;
		RouterAvailableToNetwork pdu = new RouterAvailableToNetwork(header);
		pdu.SetDestinations(55);
		pdu.send();

		new Verifications() {
			{
				byte[] packetBuffer;
				DataPacket dataPacket;
				packetReceiver.sendPacket(packetBuffer = withCapture(), dataPacket = withCapture());
				System.out.println(dataPacket.toString());
				String a = HelperUtils.getHexString(packetBuffer).toUpperCase();
				System.out.println(a);
				System.out.println(Arrays.toString(packetBuffer));
				System.out.println(Arrays.toString(testData));
				Assert.assertArrayEquals(testData, packetBuffer);
			}
		};
	}

	@Test(expected = UnableToSendException.class)
	public void sendRouterAvailableToNetwork3Test() throws Exception {
		// RouterAvailableToNetwork, dnet1 = 1, dnet2 = 1, dnet3 = 1, dnet4 = 1
		final byte[] testData = HelperUtils.toByteArray("810A000F0180050001000100010001");
		header.Control = false;
		RouterAvailableToNetwork pdu = new RouterAvailableToNetwork(header);
		pdu.send();

		new Verifications() {
			{
				byte[] packetBuffer;
				DataPacket dataPacket;
				packetReceiver.sendPacket(packetBuffer = withCapture(), dataPacket = withCapture());
				System.out.println(dataPacket.toString());
				String a = HelperUtils.getHexString(packetBuffer).toUpperCase();
				System.out.println(a);
				System.out.println(Arrays.toString(packetBuffer));
				System.out.println(Arrays.toString(testData));
				Assert.assertArrayEquals(testData, packetBuffer);
			}
		};
	}

	@Test
	public void receiveRouterAvailableToNetworkTest() throws UnableToSetUpInterfaceException, UnableToSendException,
	        IOException, InterruptedException, PropertyNotFoundException, MalformedPacketException {
		// RouterAvailableToNetwork, dnet1 = 42
		final byte[] testData = HelperUtils.toByteArray("810A0009018005002A");

		new Expectations() {
			{
				packetReceiver.getPacket();
				result = new DatagramPacket(testData, testData.length, new InetSocketAddress(
				        InetAddress.getByAddress(SRC_IP), IpPacketReceiver.DEFAULT_BACNET_PORT));
			}
		};
		packetId = media.startRead();
		header.packetId = packetId;
		header.media = media;
		RouterAvailableToNetwork request = new RouterAvailableToNetwork(header);
		request.receive();
		Assert.assertArrayEquals(request.getDestinations(), new int[] { 42 });
	}

	@Test
	public void receiveRouterAvailableToNetwork2Test() throws UnableToSetUpInterfaceException, UnableToSendException,
	        IOException, InterruptedException, PropertyNotFoundException, MalformedPacketException {
		// RouterAvailableToNetwork, dnet1 = 42, dnet1 = 22
		final byte[] testData = HelperUtils.toByteArray("810A000B018005002A0016");
		new Expectations() {
			{
				packetReceiver.getPacket();
				result = new DatagramPacket(testData, testData.length, new InetSocketAddress(
				        InetAddress.getByAddress(SRC_IP), IpPacketReceiver.DEFAULT_BACNET_PORT));
			}
		};
		packetId = media.startRead();
		header.packetId = packetId;
		header.media = media;
		RouterAvailableToNetwork request = new RouterAvailableToNetwork(header);
		request.receive();
		Assert.assertArrayEquals(request.getDestinations(), new int[] { 42, 22 });
	}

	@Test(expected = MalformedPacketException.class)
	public void receiveRouterAvailableToNetwork3Test() throws UnableToSetUpInterfaceException, UnableToSendException,
	        IOException, InterruptedException, PropertyNotFoundException, MalformedPacketException,
	        NotBVLLMessageException {
		// RouterAvailableToNetwork
		final byte[] testData = HelperUtils.toByteArray("810A000A018005002A00");

		new Expectations() {
			{
				packetReceiver.getPacket();
				result = new DatagramPacket(testData, testData.length, new InetSocketAddress(
				        InetAddress.getByAddress(SRC_IP), IpPacketReceiver.DEFAULT_BACNET_PORT));
			}
		};
		media.waitForPacket();
	}

	@Test(expected = MalformedPacketException.class)
	public void receiveRouterAvailableToNetwork4Test() throws UnableToSetUpInterfaceException, UnableToSendException,
	        IOException, InterruptedException, PropertyNotFoundException, MalformedPacketException,
	        NotBVLLMessageException {
		// RouterAvailableToNetwork
		final byte[] testData = HelperUtils.toByteArray("810A000801800500");
		new Expectations() {
			{
				packetReceiver.getPacket();
				result = new DatagramPacket(testData, testData.length, new InetSocketAddress(
				        InetAddress.getByAddress(SRC_IP), IpPacketReceiver.DEFAULT_BACNET_PORT));
			}
		};
		media.waitForPacket();
	}

}
