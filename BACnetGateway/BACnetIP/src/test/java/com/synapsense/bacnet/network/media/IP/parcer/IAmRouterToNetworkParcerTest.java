package com.synapsense.bacnet.network.media.IP.parcer;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.Arrays;

import mockit.Expectations;
import mockit.Verifications;

import mockit.integration.junit4.JMockit;
import org.junit.Assert;
import org.junit.Test;

import com.synapsense.bacnet.network.jni.MalformedPacketException;
import com.synapsense.bacnet.network.jni.NotBVLLMessageException;
import com.synapsense.bacnet.network.jni.PropertyNotFoundException;
import com.synapsense.bacnet.network.jni.UnableToSendException;
import com.synapsense.bacnet.network.jni.UnableToSetUpInterfaceException;
import com.synapsense.bacnet.network.media.IP.DataPacket;
import com.synapsense.bacnet.network.media.IP.HelperUtils;
import com.synapsense.bacnet.network.media.IP.IpPacketReceiver;
import com.synapsense.bacnet.network.pdu.npdu.response.IAmRouterToNetwork;
import org.junit.runner.RunWith;

@RunWith(JMockit.class)
public class IAmRouterToNetworkParcerTest extends AbstractParcerTest {
	@Test
	public void sendIAmRouterToNetworkTest() throws Exception {
		// IAmRouterToNetwork, dnet1 = 1, dnet2 = 1, dnet3 = 1, dnet4 = 1
		final byte[] testData = HelperUtils.toByteArray("810B000F0180010001000100010001");
		header.Control = false;
		IAmRouterToNetwork pdu = new IAmRouterToNetwork(header);
		pdu.SetDestinations(new int[] { 1, 1, 1, 1 });
		pdu.send();

		new Verifications() {
			{
				byte[] packetBuffer;
				DataPacket dataPacket;
				packetReceiver.sendPacket(packetBuffer = withCapture(), dataPacket = withCapture());
				System.out.println(dataPacket.toString());
				String a = HelperUtils.getHexString(packetBuffer).toUpperCase();
				System.out.println(a);
				System.out.println(Arrays.toString(packetBuffer));
				System.out.println(Arrays.toString(testData));
				Assert.assertArrayEquals(testData, packetBuffer);
			}
		};
	}

	@Test
	public void sendIAmRouterToNetwork2Test() throws Exception {
		// IAmRouterToNetwork, dnet1 = 55
		final byte[] testData = HelperUtils.toByteArray("810B00090180010037");
		header.Control = false;
		IAmRouterToNetwork pdu = new IAmRouterToNetwork(header);
		pdu.SetDestinations(55);
		pdu.send();

		new Verifications() {
			{
				byte[] packetBuffer;
				DataPacket dataPacket;
				packetReceiver.sendPacket(packetBuffer = withCapture(), dataPacket = withCapture());
				System.out.println(dataPacket.toString());
				String a = HelperUtils.getHexString(packetBuffer).toUpperCase();
				System.out.println(a);
				System.out.println(Arrays.toString(packetBuffer));
				System.out.println(Arrays.toString(testData));
				Assert.assertArrayEquals(testData, packetBuffer);
			}
		};
	}

	@Test(expected = UnableToSendException.class)
	public void sendIAmRouterToNetwork3Test() throws Exception {
		// IAmRouterToNetwork, dnet1 = 1, dnet2 = 1, dnet3 = 1, dnet4 = 1
		final byte[] testData = HelperUtils.toByteArray("810B000F0180010001000100010001");
		header.Control = false;
		IAmRouterToNetwork pdu = new IAmRouterToNetwork(header);
		pdu.send();

		new Verifications() {
			{
				byte[] packetBuffer;
				DataPacket dataPacket;
				packetReceiver.sendPacket(packetBuffer = withCapture(), dataPacket = withCapture());
				System.out.println(dataPacket.toString());
				String a = HelperUtils.getHexString(packetBuffer).toUpperCase();
				System.out.println(a);
				System.out.println(Arrays.toString(packetBuffer));
				System.out.println(Arrays.toString(testData));
				Assert.assertArrayEquals(testData, packetBuffer);
			}
		};
	}

	@Test
	public void receiveIAmRouterToNetworkTest() throws UnableToSetUpInterfaceException, UnableToSendException,
	        IOException, InterruptedException, PropertyNotFoundException, MalformedPacketException {
		// IAmRouterToNetwork, dnet1 = 1, dnet2 = 1, dnet3 = 1, dnet4 = 1
		final byte[] testData = HelperUtils.toByteArray("810B0009018001002A");
		new Expectations() {
			{
				packetReceiver.getPacket();
				result = new DatagramPacket(testData, testData.length, new InetSocketAddress(
				        InetAddress.getByAddress(SRC_IP), IpPacketReceiver.DEFAULT_BACNET_PORT));
			}
		};
		packetId = media.startRead();
		header.packetId = packetId;
		header.media = media;
		IAmRouterToNetwork request = new IAmRouterToNetwork(header);
		request.receive();
		Assert.assertArrayEquals(request.getDestinations(), new int[] { 42 });
	}

	@Test
	public void receiveIAmRouterToNetwork2Test() throws UnableToSetUpInterfaceException, UnableToSendException,
	        IOException, InterruptedException, PropertyNotFoundException, MalformedPacketException {
		// IAmRouterToNetwork, dnet1 = 1, dnet2 = 1, dnet3 = 1, dnet4 = 1
		final byte[] testData = HelperUtils.toByteArray("810B000B018001002A0016");
		new Expectations() {
			{
				packetReceiver.getPacket();
				result = new DatagramPacket(testData, testData.length, new InetSocketAddress(
				        InetAddress.getByAddress(SRC_IP), IpPacketReceiver.DEFAULT_BACNET_PORT));
			}
		};
		packetId = media.startRead();
		header.packetId = packetId;
		header.media = media;
		IAmRouterToNetwork request = new IAmRouterToNetwork(header);
		request.receive();
		Assert.assertArrayEquals(request.getDestinations(), new int[] { 42, 22 });
	}

	@Test(expected = MalformedPacketException.class)
	public void receiveIAmRouterToNetwork3Test() throws UnableToSetUpInterfaceException, UnableToSendException,
	        IOException, InterruptedException, PropertyNotFoundException, MalformedPacketException,
	        NotBVLLMessageException {
		// IAmRouterToNetwork, dnet1 = 1, dnet2 = 1, dnet3 = 1, dnet4 = 1
		final byte[] testData = HelperUtils.toByteArray("810B000A018001002A00");
		new Expectations() {
			{
				packetReceiver.getPacket();
				result = new DatagramPacket(testData, testData.length, new InetSocketAddress(
				        InetAddress.getByAddress(SRC_IP), IpPacketReceiver.DEFAULT_BACNET_PORT));
			}
		};
		media.waitForPacket();
	}

	@Test(expected = MalformedPacketException.class)
	public void receiveIAmRouterToNetwork4Test() throws UnableToSetUpInterfaceException, UnableToSendException,
	        IOException, InterruptedException, PropertyNotFoundException, MalformedPacketException,
	        NotBVLLMessageException {
		// IAmRouterToNetwork, dnet1 = 1, dnet2 = 1, dnet3 = 1, dnet4 = 1
		final byte[] testData = HelperUtils.toByteArray("8109000A018001");
		new Expectations() {
			{
				packetReceiver.getPacket();
				result = new DatagramPacket(testData, testData.length, new InetSocketAddress(
				        InetAddress.getByAddress(SRC_IP), IpPacketReceiver.DEFAULT_BACNET_PORT));
			}
		};
		media.waitForPacket();
	}

}
