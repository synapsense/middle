/**
 * 
 */
package com.synapsense.bacnet.system.shared;

/**
 * @author Alexander Kravin
 * 
 *         BACnetRecipientProcess ::= SEQUENCE { recipient [0] BACnetRecipient,
 *         processIdentifier [1] Unsigned32 }
 */
public class BACnetRecipientProcess {
	private BACnetRecipient recipient = null;
	public long processIdentifier;

	public BACnetRecipientProcess() {
	}

	/**
	 * @return the recipient
	 */
	public BACnetRecipient getRecipient() {
		return recipient;
	}

	/**
	 * @param recipient
	 *            the recipient to set
	 */
	public void setRecipient(BACnetRecipient recipient) {
		this.recipient = recipient;
	}

	@Override
	public boolean equals(Object obj) {
		boolean result = false;
		if (obj instanceof BACnetRecipientProcess) {
			if (obj == this) {
				result = true;
			} else {
				BACnetRecipientProcess temp = (BACnetRecipientProcess) obj;
				result = Comparator.isEquals(new Object[] { recipient, processIdentifier }, new Object[] {
				        temp.recipient, temp.processIdentifier });
			}
		}
		return result;
	}

	public String toString() {
		return "{recipient=" + recipient + "; processIdentifier=" + processIdentifier + "}";
	}
}
