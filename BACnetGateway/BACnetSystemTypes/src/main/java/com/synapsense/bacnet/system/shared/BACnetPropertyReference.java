/**
 * 
 */
package com.synapsense.bacnet.system.shared;

/**
 * @author Alexander Kravin
 * 
 */
public class BACnetPropertyReference {
	public long propertyIdentifier;
	public Long propertyArrayIndex = null;
}
