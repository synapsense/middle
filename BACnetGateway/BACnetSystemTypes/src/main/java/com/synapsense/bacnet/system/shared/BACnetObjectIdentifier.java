package com.synapsense.bacnet.system.shared;

import com.synapsense.bacnet.system.shared.BACnetObjectType;

public class BACnetObjectIdentifier {
	public final static int DEVICE_ITSELF = 4194303;

	private final int identifier;

	public BACnetObjectIdentifier(int identifier) {
		this.identifier = identifier;
	}

	public BACnetObjectIdentifier(short objType, int instNumber) {
		int id = (objType << 22) | instNumber;
		this.identifier = id;
	}

	public short getObjectType() {
		// the lowest 22 bits are instance number
		return (short) (identifier >> 22);
	}

	public int getInstanceNumber() {
		// mask the highest 10 bits which are object type
		return identifier & 0x3FFFFF;

	}

	public boolean isDeviceId() {
		return isDeviceId(this);
	}

	public static boolean isDeviceId(BACnetObjectIdentifier id) {
		return id.getObjectType() == BACnetObjectType.DEVICE && id.getInstanceNumber() == DEVICE_ITSELF;
	}

	@Override
	public int hashCode() {
		// since we store the only field of type in, let's use it
		return identifier;
		/*
		 * another way to build hashes return new HashCodeBuilder(838751059,
		 * 838751107) .appendSuper(super.hashCode())
		 * .append(this.getObjectType()) .append(this.getInstanceNumber())
		 * .toHashCode();
		 */
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof BACnetObjectIdentifier == false) {
			return false;
		}
		if (this == obj) {
			return true;
		}
		BACnetObjectIdentifier rhs = (BACnetObjectIdentifier) obj;
		return this.identifier == rhs.identifier;
		/*
		 * another way to make comparisons return new EqualsBuilder()
		 * .appendSuper(super.equals(obj)) .append(this.getObjectType(),
		 * rhs.getObjectType()) .append(this.getInstanceNumber(),
		 * rhs.getInstanceNumber()) .isEquals();
		 */
	}

	@Override
	public String toString() {
		return "Type [" + getObjectType() + "] Inst: [" + getInstanceNumber() + "]";
	}
}
