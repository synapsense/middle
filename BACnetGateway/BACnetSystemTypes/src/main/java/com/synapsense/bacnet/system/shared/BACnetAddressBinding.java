package com.synapsense.bacnet.system.shared;

public class BACnetAddressBinding {
	public final BACnetObjectIdentifier deviceObjectIdentifier;
	public final BACnetAddress deviceAddress;

	public BACnetAddressBinding(BACnetObjectIdentifier deviceObjectIdentifier, BACnetAddress deviceAddress) {
		if (deviceObjectIdentifier == null)
			throw new IllegalArgumentException("deviceObjectIdentifier must be not null");
		this.deviceObjectIdentifier = deviceObjectIdentifier;
		if (deviceAddress == null)
			throw new IllegalArgumentException("deviceAddress must be not null");
		this.deviceAddress = deviceAddress;
	}

	public String toString() {
		return "deviceId={" + deviceObjectIdentifier + "}, deviceAddress={" + deviceAddress + "}";
	}
}
