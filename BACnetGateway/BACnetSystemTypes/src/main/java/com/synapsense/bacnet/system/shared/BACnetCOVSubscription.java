/**
 * 
 */
package com.synapsense.bacnet.system.shared;

/**
 * @author Alexander Kravin
 * 
 *         BACnetCOVSubscription ::= SEQUENCE { Recipient [0]
 *         BACnetRecipientProcess, MonitoredPropertyReference [1]
 *         BACnetObjectPropertyReference, IssueConfirmedNotifications [2]
 *         BOOLEAN, TimeRemaining [3] Unsigned, COVIncrement [4] REAL OPTIONAL
 *         -- used only with monitored -- properties with a datatype of REAL }
 * 
 *         BACnetRecipientProcess ::= SEQUENCE { recipient [0] BACnetRecipient,
 *         processIdentifier [1] Unsigned32 }
 * 
 *         BACnetRecipient ::= CHOICE { device [0] BACnetObjectIdentifier,
 *         address [1] BACnetAddress }
 * 
 */
public class BACnetCOVSubscription {
	private BACnetRecipientProcess recipientProcess = null;
	private BACnetObjectPropertyReference monitoredPropertyReference = null;
	public boolean issueConfirmedNotifications = false;
	public long timeRemaining;
	public Float COVIncrement = null;
	public long expiresAt;

	/**
	 * @return the recipientProcess
	 */
	public BACnetRecipientProcess getRecipientProcess() {
		return recipientProcess;
	}

	/**
	 * @param recipientProcess
	 *            the recipientProcess to set
	 */
	public void setRecipientProcess(BACnetRecipientProcess recipientProcess) {
		this.recipientProcess = recipientProcess;
	}

	/**
	 * @return the monitoredPropertyReference
	 */
	public BACnetObjectPropertyReference getMonitoredPropertyReference() {
		return monitoredPropertyReference;
	}

	/**
	 * @param monitoredPropertyReference
	 *            the monitoredPropertyReference to set
	 */
	public void setMonitoredPropertyReference(BACnetObjectPropertyReference monitoredPropertyReference) {
		this.monitoredPropertyReference = monitoredPropertyReference;
	}

	public long getSecondsRemain() {
		return expiresAt - System.currentTimeMillis() / 1000;
	}

	public boolean isExpired() {
		return getSecondsRemain() <= 0;
	}

	@Override
	public boolean equals(Object obj) {
		boolean result = false;
		if (obj instanceof BACnetCOVSubscription) {
			if (obj == this) {
				result = true;
			} else {
				BACnetCOVSubscription temp = (BACnetCOVSubscription) obj;
				result = Comparator.isEquals(new Object[] { recipientProcess, monitoredPropertyReference },
				        new Object[] { temp.recipientProcess, temp.monitoredPropertyReference });
			}
		}
		return result;
	}

	@Override
	public String toString() {
		return "{recipientProcess=" + recipientProcess + "; monitoredPropertyReference=" + monitoredPropertyReference
		        + "; issueConfirmedNotifications=" + issueConfirmedNotifications + "; timeRemaining="
		        + getSecondsRemain() + "; COVIncrement=" + COVIncrement + "}";
	}
}
