package com.synapsense.bacnet.system.shared;

import java.util.GregorianCalendar;

public class BACnetDateValue extends BACnetDateTimeValue {
	public BACnetDateValue(GregorianCalendar dateTime, MonthExtender monthExtender,
	        DayOfMonthExtender dayOfMonthExtender) {
		super(dateTime, monthExtender, dayOfMonthExtender);
	}

	public Object clone() {
		BACnetDateValue value = new BACnetDateValue(this.value, this.monthExtender, this.dayOfMonthExtender);
		return value;
	}
}
