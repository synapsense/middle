/**
 * 
 */
package com.synapsense.bacnet.system.shared;

import java.util.ArrayList;

/**
 * @author Alexander Kravin
 * 
 */
public class ReadAccessResult {
	private BACnetObjectIdentifier objectIdentifier = null;
	private ArrayList<BACnetResult> listOfResults = new ArrayList<BACnetResult>();

	/**
	 * @return the objectIdentifier
	 */
	public BACnetObjectIdentifier getObjectIdentifier() {
		return objectIdentifier;
	}

	/**
	 * @param objectIdentifier
	 *            the objectIdentifier to set
	 */
	public void setObjectIdentifier(BACnetObjectIdentifier objectIdentifier) {
		this.objectIdentifier = objectIdentifier;
	}

	/**
	 * @return the listOfResults
	 */
	public ArrayList<BACnetResult> getListOfResults() {
		return listOfResults;
	}

	public void addResult(BACnetResult listOfResults) {
		this.listOfResults.add(listOfResults);
	}
}
