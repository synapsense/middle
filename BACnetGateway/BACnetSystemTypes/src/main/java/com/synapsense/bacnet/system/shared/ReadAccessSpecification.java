/**
 * 
 */
package com.synapsense.bacnet.system.shared;

import java.util.ArrayList;

/**
 * @author Alexander Kravin
 * 
 *         ReadAccessSpecification ::= SEQUENCE { objectIdentifier [0]
 *         BACnetObjectIdentifier, listOfPropertyReferences [1] SEQUENCE OF
 *         BACnetPropertyReference }
 */
public class ReadAccessSpecification {
	private BACnetObjectIdentifier objectIdentifier = null;
	private ArrayList<BACnetPropertyReference> listOfPropertyReferences = new ArrayList<BACnetPropertyReference>();

	public void addBACnetPropertyReference(BACnetPropertyReference bacnetPropertyReference) {
		listOfPropertyReferences.add(bacnetPropertyReference);
	}

	/**
	 * @return the objectIdentifier
	 */
	public BACnetObjectIdentifier getObjectIdentifier() {
		return objectIdentifier;
	}

	/**
	 * @param objectIdentifier
	 *            the objectIdentifier to set
	 */
	public void setObjectIdentifier(BACnetObjectIdentifier objectIdentifier) {
		this.objectIdentifier = objectIdentifier;
	}

	/**
	 * @return the listOfPropertyReferences
	 */
	public ArrayList<BACnetPropertyReference> getListOfPropertyReferences() {
		return listOfPropertyReferences;
	}
}
