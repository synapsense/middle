/**
 * 
 */
package com.synapsense.bacnet.system.shared;

/**
 * @author Alexander Kravin
 * 
 *         BACnetObjectPropertyReference ::= SEQUENCE { objectIdentifier [0]
 *         BACnetObjectIdentifier, propertyIdentifier [1]
 *         BACnetPropertyIdentifier, propertyArrayIndex [2] Unsigned OPTIONAL --
 *         used only with array datatype -- if omitted with an array the entire
 *         array is referenced }
 * 
 */
public class BACnetObjectPropertyReference {
	private BACnetObjectIdentifier objectIdentifier = null;
	public long propertyIdentifier;
	public Long propertyArrayIndex = null;

	/**
	 * @return the objectIdentifier
	 */
	public BACnetObjectIdentifier getObjectIdentifier() {
		return objectIdentifier;
	}

	/**
	 * @param objectIdentifier
	 *            the objectIdentifier to set
	 */
	public void setObjectIdentifier(BACnetObjectIdentifier objectIdentifier) {
		this.objectIdentifier = objectIdentifier;
	}

	@Override
	public boolean equals(Object obj) {
		boolean result = false;
		if (obj instanceof BACnetObjectPropertyReference) {
			if (obj == this) {
				result = true;
			} else {
				BACnetObjectPropertyReference temp = (BACnetObjectPropertyReference) obj;
				if (temp.objectIdentifier.equals(objectIdentifier) && temp.propertyArrayIndex == propertyArrayIndex
				        && temp.propertyIdentifier == propertyIdentifier) {
					result = true;
				}
			}
		}
		return result;
	}

	public String toString() {
		return "{objectIdentifier=" + objectIdentifier + "; propertyIdentifier=" + propertyIdentifier
		        + "; propertyArrayIndex=" + propertyArrayIndex + "}";
	}
}
