package com.synapsense.bacnet.system.shared;

import java.util.GregorianCalendar;

public class BACnetTimeValue extends BACnetDateTimeValue {
	public BACnetTimeValue(GregorianCalendar dateTime) {
		super(dateTime, MonthExtender.UNSPECIFIED, DayOfMonthExtender.UNSPECIFIED);
	}

	public Object clone() {
		BACnetTimeValue value = new BACnetTimeValue(this.value);
		return value;
	}
}
