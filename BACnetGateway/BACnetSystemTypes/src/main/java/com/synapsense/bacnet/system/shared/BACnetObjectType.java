package com.synapsense.bacnet.system.shared;

/**
 * Predefined BACnet object types according to Clause 21.
 * 
 * @author Oleg Stepanov
 * 
 */
public class BACnetObjectType {
	public static short ACCUMULATOR = 23;
	public static short ANALOG_INPUT = 0;
	public static short ANALOG_OUTPUT = 1;
	public static short ANALOG_VALUE = 2;
	public static short BINARY_INPUT = 3;
	public static short BINARY_OUTPUT = 4;
	public static short BINARY_VALUE = 5;
	// public static int calendar (6),
	// public static int command (7),
	public static short DEVICE = 8;
	// public static int event-enrollment (9),
	// public static int file (10),
	// public static int group (11),
	// public static int life-safety-point (21),
	// public static int life-safety-zone (22),
	// public static int loop (12),
	public static short MULTI_STATE_INPUT = 13;
	public static short MULTI_STATE_OUTPUT = 14;
	public static short MULTI_STATE_VALUE = 19;
	// public static int notification-class (15),
	// public static int program (16),
	// public static int pulse-converter (24),
	// public static int schedule (17),
	// public static int trend-log (20),

	public static short SYNAPSENSE_STORE_OBJECT = 128;
	public static short SYNAPSENSE_DATABASE_OBJECT = 129;
	public static short SYNAPSENSE_WEB_SERVER_OBJECT = 130;
}
