package com.synapsense.bacnet.system.shared;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class BACnetDateTimeValue implements Cloneable {
	protected GregorianCalendar value;

	public enum MonthExtender {
	SIMPLE, ODD, EVEN, UNSPECIFIED;
	}

	public enum DayOfMonthExtender {
	SIMPLE, LAST_DAY, UNSPECIFIED;
	}

	protected DayOfMonthExtender dayOfMonthExtender;
	protected MonthExtender monthExtender;

	public BACnetDateTimeValue(GregorianCalendar dateTime, MonthExtender monthExtender,
	        DayOfMonthExtender dayOfMonthExtender) {
		this.monthExtender = monthExtender;
		this.dayOfMonthExtender = dayOfMonthExtender;

		value = (GregorianCalendar) dateTime.clone();
		if (this.monthExtender != MonthExtender.SIMPLE)
			value.clear(Calendar.MONTH);
		if (this.dayOfMonthExtender != DayOfMonthExtender.SIMPLE)
			value.clear(Calendar.DAY_OF_MONTH);

		if (!value.isSet(Calendar.MONTH) && (this.monthExtender == MonthExtender.SIMPLE)) {
			this.monthExtender = MonthExtender.UNSPECIFIED;
		}
		if (!value.isSet(Calendar.DAY_OF_MONTH) && (this.dayOfMonthExtender == DayOfMonthExtender.SIMPLE)) {
			this.dayOfMonthExtender = DayOfMonthExtender.UNSPECIFIED;
		}
	}

	public GregorianCalendar getDate() {
		return value;
	}

	public MonthExtender getMonthExtender() {
		return monthExtender;
	}

	public DayOfMonthExtender getDayOfMonthExtender() {
		return dayOfMonthExtender;
	}
}