package com.synapsense.bacnet.system.shared;

/**
 * Enum of BACnet services identifiers (see BACnetServicesSupported in Clause
 * 21).
 * 
 * @author Oleg Stepanov
 * 
 */
public enum BACnetService {

// -- Alarm and Event Services
ConfirmedCOVNotification(1), SubscribeCOVProperty(38),

// -- Object Access Services
ReadProperty(12), ReadPropertyMultiple(14), WriteProperty(15),

// -- Unconfirmed Services
I_am(26), I_have(27), UnconfirmedCOVNotification(28), Who_Has(33), Who_Is(34);

public final int serviceCode;

private BACnetService(int serviceCode) {
	this.serviceCode = serviceCode;
}
}
