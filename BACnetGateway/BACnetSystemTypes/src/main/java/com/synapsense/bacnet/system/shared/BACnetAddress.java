package com.synapsense.bacnet.system.shared;

public class BACnetAddress {
	public final int networkNumber;
	public final byte[] macAddress;

	public BACnetAddress(int networkNumber, byte[] macAddress) {
		this.networkNumber = networkNumber;
		if (macAddress == null)
			throw new IllegalArgumentException("macAddress must be not null");
		this.macAddress = new byte[macAddress.length];
		System.arraycopy(macAddress, 0, this.macAddress, 0, macAddress.length);
	}

	@Override
	public boolean equals(Object obj) {
		boolean result = false;
		if (obj instanceof BACnetAddress) {
			if (obj == this) {
				result = true;
			} else {
				BACnetAddress temp = (BACnetAddress) obj;
				result = Comparator.isEquals(new Object[] { networkNumber, macAddress }, new Object[] {
				        temp.networkNumber, temp.macAddress });
			}
		}
		return result;
	}

	public static String toHex(byte[] b) {
		StringBuilder result = new StringBuilder();
		if (b != null) {
			for (int i = 0; i < b.length; ++i) {
				if (i == 0)
					result.append(":");
				result.append((Integer.toHexString((b[i] & 0xFF) | 0x100).substring(1, 3).toUpperCase()));
			}
		}
		return result.toString();
	}

	public String toString() {
		return "{networkNumber=" + networkNumber + "; macAddress=" + toHex(macAddress) + "}";
	}
}
