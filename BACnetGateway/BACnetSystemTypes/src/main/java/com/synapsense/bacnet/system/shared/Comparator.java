package com.synapsense.bacnet.system.shared;

public class Comparator {
	public static boolean isEquals(Object[] args1, Object[] args2) {
		boolean result = false;
		if (args1.length == args2.length) {
			int length = args1.length;
			int sum = 0;
			for (int i = 0; i < length; i++) {
				if (args1[i] == null) {
					if (args2[i] == null) {
						sum++;
					}
				} else if (args1[i].equals(args2[i])) {
					sum++;
				}
			}
			result = length == sum;
		}
		return result;
	}
}
