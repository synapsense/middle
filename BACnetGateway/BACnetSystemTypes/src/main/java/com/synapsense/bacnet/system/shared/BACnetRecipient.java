/**
 * 
 */
package com.synapsense.bacnet.system.shared;

/**
 * @author Alexander Kravin
 * 
 *         BACnetRecipient ::= CHOICE { device [0] BACnetObjectIdentifier,
 *         address [1] BACnetAddress }
 * 
 */
public class BACnetRecipient {
	private BACnetObjectIdentifier objectIdentifier = null;
	private BACnetAddress address = null;

	public BACnetRecipient(BACnetObjectIdentifier objectIdentifier) {
		this.objectIdentifier = objectIdentifier;
	}

	public BACnetRecipient(BACnetAddress address) {
		this.address = address;
	}

	/**
	 * @return the objectIdentifier
	 */
	public BACnetObjectIdentifier getObjectIdentifier() {
		return objectIdentifier;
	}

	/**
	 * @return the address
	 */
	public BACnetAddress getAddress() {
		return address;
	}

	@Override
	public boolean equals(Object obj) {
		boolean result = false;
		if (obj instanceof BACnetRecipient) {
			if (obj == this) {
				result = true;
			} else {
				BACnetRecipient temp = (BACnetRecipient) obj;
				result = Comparator.isEquals(new Object[] { objectIdentifier, address }, new Object[] {
				        temp.objectIdentifier, temp.address });
			}
		}
		return result;
	}

	public String toString() {
		return "{objectIdentifier=" + objectIdentifier + "; address=" + address + "}";
	}
}
