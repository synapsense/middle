/**
 * 
 */
package com.synapsense.bacnet.system.shared;

/**
 * @author Kravin Alexander
 * 
 */
public class BACnetResult {
	public BACnetPropertyReference propertyReference = new BACnetPropertyReference();
	public byte[] propertyValue = null;
	public long propertyAccessErrorClass;
	public long propertyAccessErrorCode;
}
