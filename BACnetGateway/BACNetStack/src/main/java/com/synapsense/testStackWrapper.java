package com.synapsense;

import com.synapsense.bacnet.network.NetworkProcessor;
import com.synapsense.bacnet.network.jni.PropertyNotFoundException;
import com.synapsense.bacnet.network.media.MPDUHeader;

public class testStackWrapper extends Thread {
	private MPDUHeader header;

	private testStackWrapper(MPDUHeader header) {
		super("testStackWrapper");
		this.header = header;

	}

	public static void startCapture(MPDUHeader header) {
		testStackWrapper s = new testStackWrapper(header);
		// s.setDaemon(true);
		s.start();
	}

	public void run() {
		while (true) {
			try {
				NetworkProcessor.getInstance().addPacket(header.receive());
			} catch (PropertyNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}
