package com.synapsense.bacnet.network.pdu.apdu.response;

import com.synapsense.bacnet.network.jni.BACNetException;
import com.synapsense.bacnet.network.jni.Property;
import com.synapsense.bacnet.network.jni.PropertyNotFoundException;
import com.synapsense.bacnet.network.media.MPDUHeader;
import com.synapsense.bacnet.network.media.Media;
import com.synapsense.bacnet.network.pdu.apdu.APDUBasePacket;
import com.synapsense.bacnet.network.pdu.apdu.APDUMessageType;
import com.synapsense.bacnet.network.pdu.apdu.datatypes.ConfirmedServiceChoice;

/**
 * 
 * @author Alexander Borisov </br> BACnet-SimpleACK-PDU ::= SEQUENCE {</br>
 *         pdu-type [0] Unsigned (0..15), -- 2 for this PDU type</br> reserved
 *         [1] Unsigned (0..15), -- must be set to zero</br> invokeID [2]
 *         Unsigned (0..255),</br> service-ACK-choice [3]
 *         BACnetConfirmedServiceChoice</br> -- Context-specific tags 0..3 are
 *         NOT used in header encoding</br> }
 */
public class SimpleACKPDU extends APDUBasePacket {
	ConfirmedServiceChoice service_ACK_choice;// [3]
	                                          // BACnetConfirmedServiceChoice

	public SimpleACKPDU(MPDUHeader header) {
		this.header = header;
		type = APDUMessageType.simpleACK_PDU;
	}

	@Override
	public void receive() throws PropertyNotFoundException {
		Media reader = header.media;
		invokeID = reader.readByte(header.packetId, Property.INVOKE_ID) & 0xFF;
		service_ACK_choice = ConfirmedServiceChoice.valueOf(reader.readByte(header.packetId, Property.SERV_CHOICE));
	}

	@Override
	public void send() throws BACNetException {
		Media reader = header.media;
		header.send();
		reader.setByte(header.packetId, Property.INVOKE_ID, (byte) invokeID);
		reader.setByte(header.packetId, Property.SERV_CHOICE, (byte) service_ACK_choice.getCode());
		reader.setByte(header.packetId, Property.MESSAGE_TYPE, type.getType());
		reader.sendPacket(header.packetId);
	}

	@Override
	public boolean isSegmented() {
		return false;
	}

	/**
	 * @return the service_ACK_choice
	 */
	public ConfirmedServiceChoice getService_ACK_choice() {
		return service_ACK_choice;
	}

	/**
	 * @param service_ACK_choice
	 *            the service_ACK_choice to set
	 */
	public void setService_ACK_choice(ConfirmedServiceChoice service_ACK_choice) {
		this.service_ACK_choice = service_ACK_choice;
	}

}
