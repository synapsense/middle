package com.synapsense.bacnet.network.pdu.apdu.sessions.server.segmented;

import com.synapsense.bacnet.network.pdu.apdu.APDUBasePacket;
import com.synapsense.bacnet.network.pdu.apdu.request.AbortPDU;
import com.synapsense.bacnet.network.pdu.apdu.sessions.AbstractSession;
import com.synapsense.bacnet.network.pdu.apdu.sessions.SessionManager;
import com.synapsense.scheduler.TimeoutTaskUnit;

public class SegmentedSession extends AbstractSession implements TimeoutTaskUnit {

	public SegmentedSession(APDUBasePacket packet) {
		super(packet);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void timeoutAction() {
		SessionManager.getInstance().removeSession(this);
		// TODO Auto-generated method stub

	}

	@Override
	public void Execute() throws Exception {
		AbortPDU abort = new AbortPDU(packet.getHeader());
		abort.send();
		SessionManager.getInstance().removeSession(this);
	}

	@Override
	public void failureAction(Throwable t) {
		SessionManager.getInstance().removeSession(this);
		// TODO Auto-generated method stub

	}

}
