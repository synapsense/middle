package com.synapsense.bacnet.network.pdu.apdu.request;

import com.synapsense.bacnet.network.jni.BACNetException;
import com.synapsense.bacnet.network.jni.Property;
import com.synapsense.bacnet.network.jni.PropertyNotFoundException;
import com.synapsense.bacnet.network.media.MPDUHeader;
import com.synapsense.bacnet.network.media.Media;
import com.synapsense.bacnet.network.pdu.apdu.APDUBasePacket;
import com.synapsense.bacnet.network.pdu.apdu.APDUMessageType;
import com.synapsense.bacnet.network.pdu.apdu.datatypes.UnconfirmedServiceChoice;

/**
 * 
 * @author Alexander Borisov
 * 
 *         BACnet-Unconfirmed-Request-PDU ::= SEQUENCE { pdu-type [0] Unsigned
 *         (0..15), -- 1 for this PDU type reserved [1] Unsigned (0..15), --
 *         must be set to zero service-choice [2]
 *         BACnetUnconfirmedServiceChoice, service-request [3]
 *         BACnet-Unconfirmed-Service-Request -- Context-specific tags 0..3 are
 *         NOT used in header encoding }
 * 
 */
public class UnconfirmedRequestPDU extends APDUBasePacket {

	protected UnconfirmedServiceChoice choise;

	public UnconfirmedRequestPDU(MPDUHeader header) {
		this.header = header;
		type = APDUMessageType.unconfirmed_request_PDU;
	}

	public UnconfirmedServiceChoice getSVCChoise() {
		return choise;
	}

	@Override
	public void receive() throws PropertyNotFoundException {
		Media reader = header.media;
		choise = UnconfirmedServiceChoice.valueOf(reader.readByte(header.packetId, Property.SERV_CHOICE));
		buffer = reader.readByteArray(header.packetId, Property.PDU_PAYLOAD);
	}

	@Override
	public void send() throws BACNetException {
		Media reader = header.media;
		header.send();
		reader.setByte(header.packetId, Property.MESSAGE_TYPE, type.getType());
		reader.setByte(header.packetId, Property.SERV_CHOICE, choise.getType());
		reader.setByteArray(header.packetId, Property.PDU_PAYLOAD, buffer);
		reader.sendPacket(header.packetId);
	}

	@Override
	public boolean isSegmented() {
		return false;
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer(100);
		sb.append("\n");
		sb.append(this.getClass().getName());
		sb.append(header);
		sb.append("choise=");
		sb.append(choise);
		sb.append("\n");
		return sb.toString();
	}

	/**
	 * @param choise
	 *            the choise to set
	 */
	public void setChoise(UnconfirmedServiceChoice choise) {
		this.choise = choise;
	}

}
