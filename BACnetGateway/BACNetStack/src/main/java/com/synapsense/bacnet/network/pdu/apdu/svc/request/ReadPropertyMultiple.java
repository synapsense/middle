package com.synapsense.bacnet.network.pdu.apdu.svc.request;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.synapsense.bacnet.network.media.MPDUHeader;
import com.synapsense.bacnet.network.pdu.apdu.APDUBasePacket;
import com.synapsense.bacnet.network.pdu.apdu.codecs.PrimitivesDecoder;
import com.synapsense.bacnet.network.pdu.apdu.codecs.PrimitivesEncoder;
import com.synapsense.bacnet.network.pdu.apdu.datatypes.ConfirmedServiceChoice;
import com.synapsense.bacnet.network.pdu.apdu.request.ConfirmedRequestPDU;
import com.synapsense.bacnet.network.pdu.apdu.sessions.AbstractSession;
import com.synapsense.bacnet.network.pdu.apdu.sessions.client.unsegmented.ConfirmedSession;
import com.synapsense.bacnet.network.pdu.apdu.svc.SvcBase;
import com.synapsense.bacnet.network.pdu.apdu.svc.SvcException;
import com.synapsense.bacnet.network.pdu.apdu.svc.SvcFactory;
import com.synapsense.bacnet.network.pdu.apdu.svc.responce.ErrorSvc;
import com.synapsense.bacnet.network.pdu.apdu.svc.responce.ReadPropertyMultipleAck;
import com.synapsense.bacnet.network.routing.MalformedHeaderException;
import com.synapsense.bacnet.network.routing.RouteNotFoundException;
import com.synapsense.bacnet.network.routing.RoutingService;
import com.synapsense.bacnet.objects.BACnetPropertyWrapper;
import com.synapsense.bacnet.system.BACnetError;
import com.synapsense.bacnet.system.BACnetPropertyId;
import com.synapsense.bacnet.system.BACnetPropertyType;
import com.synapsense.bacnet.system.BACnetSystem;
import com.synapsense.bacnet.system.Device;
import com.synapsense.bacnet.system.shared.BACnetAddress;
import com.synapsense.bacnet.system.shared.BACnetObjectIdentifier;
import com.synapsense.bacnet.system.shared.BACnetPropertyReference;
import com.synapsense.bacnet.system.shared.BACnetResult;
import com.synapsense.bacnet.system.shared.ReadAccessResult;
import com.synapsense.bacnet.system.shared.ReadAccessSpecification;

/**
 * 
 * @author Kravin Alexander
 *
 */

/**
 * ReadPropertyMultiple-Request ::= SEQUENCE { listOfReadAccessSpecs SEQUENCE OF
 * ReadAccessSpecification }
 * 
 * 
 * ReadAccessSpecification ::= SEQUENCE { objectIdentifier [0]
 * BACnetObjectIdentifier, listOfPropertyReferences [1] SEQUENCE OF
 * BACnetPropertyReference }
 * 
 * BACnetPropertyReference ::= SEQUENCE { propertyIdentifier [0]
 * BACnetPropertyIdentifier, propertyArrayIndex [1] Unsigned OPTIONAL --used
 * only with array datatype -- if omitted with an array the entire array is
 * referenced }
 * 
 */

public class ReadPropertyMultiple extends SvcBase {
	private ArrayList<ReadAccessSpecification> listOfReadAccessSpecs = new ArrayList<ReadAccessSpecification>();
	HashMap<ReadAccessSpecification, Long> propertiesList = new HashMap<ReadAccessSpecification, Long>();

	public ReadPropertyMultiple(MPDUHeader header) {
		super(header);
		cChoise = ConfirmedServiceChoice.writePropertyMultiple;
	}

	public ReadPropertyMultiple(APDUBasePacket packet) {
		super(packet);
	}

	public ReadPropertyMultiple(SvcBase svc) {
		super(svc);
	}

	@Override
	public void decodeBuffer(byte[] buffer) {
		PrimitivesDecoder pd = new PrimitivesDecoder(buffer);
		try {
			while (pd.askForTag() != null) {
				ReadAccessSpecification readAccessSpecification = new ReadAccessSpecification();
				readAccessSpecification.setObjectIdentifier(pd.decodeObjectID(0));
				pd.decodeSequenceStart(1);
				long specialProperty = 0;
				while (!pd.askForSequenceStop(1)) {
					BACnetPropertyReference reference = new BACnetPropertyReference();
					reference.propertyIdentifier = pd.decodeEnum(0);
					if (pd.askForTag().getID() == 1 && !pd.askForSequenceStop(1)) {
						reference.propertyArrayIndex = pd.decodeUInt(1);
					}

					if (reference.propertyIdentifier == BACnetPropertyId.ALL
					        || reference.propertyIdentifier == BACnetPropertyId.OPTIONAL
					        || reference.propertyIdentifier == BACnetPropertyId.REQUIRED) {
						specialProperty = reference.propertyIdentifier;
					}
					readAccessSpecification.addBACnetPropertyReference(reference);
				}
				pd.decodeSequenceStop(1);
				listOfReadAccessSpecs.add(readAccessSpecification);
				if (specialProperty != 0)
					propertiesList.put(readAccessSpecification, specialProperty);
			}
		} catch (IOException e) {
			throw new SvcException("decoded buffer contains illegal data", e);
		}
	}

	private ReadAccessSpecification getSpecialReadAccessSpec(BACnetObjectIdentifier objectId, long propertyId)
	        throws BACnetError {
		ReadAccessSpecification result = new ReadAccessSpecification();
		BACnetAddress addr = new BACnetAddress(header.DNET, header.DADR);
		Map<Long, BACnetPropertyWrapper> wrapProperties = new HashMap<Long, BACnetPropertyWrapper>();
		wrapProperties = BACnetSystem.getInstance().getDevice(addr).getObjectProperties(objectId);
		boolean flag;
		result.setObjectIdentifier(objectId);
		for (BACnetPropertyWrapper prop : wrapProperties.values()) {
			flag = false;
			if (propertyId == BACnetPropertyId.ALL) {
				flag = true;
			} else if (propertyId == BACnetPropertyId.OPTIONAL && prop.getType() == BACnetPropertyType.OPTIONAL) {
				flag = true;
			} else if (propertyId == BACnetPropertyId.REQUIRED && prop.getType() == BACnetPropertyType.REQUIRED) {
				flag = true;
			}
			if (flag) {
				BACnetPropertyReference reference = new BACnetPropertyReference();
				reference.propertyIdentifier = prop.getId();
				result.addBACnetPropertyReference(reference);
			}
		}
		return (result.getListOfPropertyReferences().size() == 0 ? null : result);
	}

	@Override
	public byte[] encodeBuffer() {
		PrimitivesEncoder encoder = new PrimitivesEncoder();
		try {
			for (ReadAccessSpecification spec : listOfReadAccessSpecs) {
				encoder.encodeObjectID(0, spec.getObjectIdentifier());
				encoder.encodeSequenceStart(1);

				for (BACnetPropertyReference ref : spec.getListOfPropertyReferences()) {
					encoder.encodeEnum(0, ref.propertyIdentifier);
					if (ref.propertyArrayIndex != null)
						encoder.encodeUInt(1, ref.propertyArrayIndex);
				}
				encoder.encodeSequenceStop(1);
			}
		} catch (IOException e) {
			logger.warn("Unable to encode buffer", e);
			throw new SvcException("Cannot encode service", e);
		}
		return encoder.getEncodedBuffer();
	}

	@Override
	public SvcBase[] execute() {
		logger.debug("Executing Read property multiple service:");
		// TODO now it only works with non-broadcast messages
		BACnetAddress addr = new BACnetAddress(header.DNET, header.DADR);
		Device device = BACnetSystem.getInstance().getDevice(addr);
		SvcBase response = null;
		byte[] result = null;
		BACnetObjectIdentifier objectId = null;
		try {
			if (device != null) {
				response = SvcFactory.getInstance().getResponseForRequest(this);
				if (propertiesList.size() != 0) {
					// 1 - Delete item from listOfReadAccessSpecs
					// 2 - Add new item into listOfReadAccessSpecs
					ReadAccessSpecification readAccessSpec = null;
					for (ReadAccessSpecification spec : propertiesList.keySet()) {
						listOfReadAccessSpecs.remove(spec);
						readAccessSpec = getSpecialReadAccessSpec(spec.getObjectIdentifier(), propertiesList.get(spec));
						if (readAccessSpec != null) {
							listOfReadAccessSpecs.add(readAccessSpec);
						}
					}
				}
				for (ReadAccessSpecification spec : listOfReadAccessSpecs) {
					objectId = spec.getObjectIdentifier();
					ReadAccessResult readAccessResult = new ReadAccessResult();
					readAccessResult.setObjectIdentifier(objectId);

					for (BACnetPropertyReference ref : spec.getListOfPropertyReferences()) {
						BACnetResult param = new BACnetResult();
						param.propertyReference.propertyIdentifier = ref.propertyIdentifier;
						param.propertyReference.propertyArrayIndex = ref.propertyArrayIndex;
						try {
							result = BACnetSystem.getInstance().getDevice(addr).getReadPropertyService()
							        .read(objectId, ref.propertyIdentifier, ref.propertyArrayIndex);
							param.propertyValue = result;
						} catch (BACnetError e) {
							param.propertyAccessErrorClass = e.getErrorClassNum();
							param.propertyAccessErrorCode = e.getErrorCodeNum();
						}

						readAccessResult.addResult(param);
					}
					((ReadPropertyMultipleAck) response).addReadAccessResults(readAccessResult);
				}
			} else {
				logger.warn("The device with address " + addr + " doesn't exist.");
			}
			if (response == null) {
				response = new ErrorSvc(this);
			}
		} catch (BACnetError e1) {
			response = new ErrorSvc(e1, this);
			result = response.encodeBuffer();
		} catch (Exception e) {
			response = new ErrorSvc(this);
			logger.warn("Abnormal scenario.... Sending error", e);
		} finally {
			response.setEncodedBuffer(result);
			try {
				MPDUHeader respHeader = RoutingService.createResponseHeader(header);
				respHeader.SNET = header.DNET;
				respHeader.SLEN = header.DLEN;
				respHeader.SADR = MPDUHeader.getCopy(header.DADR);
				response.setHeader(respHeader);
			} catch (RouteNotFoundException e) {
				throw new SvcException("Malformed header", e);
			} catch (MalformedHeaderException e) {
				throw new SvcException("Malformed header", e);
			}
		}
		SvcBase res[] = { response };
		return res;
	}

	public AbstractSession createSession() {
		return new ConfirmedSession(this);
	}

	@Override
	public APDUBasePacket getPacket() {
		if (header == null)
			throw new SvcException("Unable to create APDU packet");
		if (header.DST_MAC == null) {
			try {
				header.DST_MAC = MPDUHeader.getCopy(RoutingService.discoverRoute(header.DNET).getDST_MAC());
			} catch (RouteNotFoundException e) {
				throw new SvcException(e.getLocalizedMessage());
			}
		}
		header.Control = true;
		ConfirmedRequestPDU packet = new ConfirmedRequestPDU(header);
		packet.setId(invokeID);
		packet.setService_choice(ConfirmedServiceChoice.readProperty);
		packet.setSegmented_message(this.segmented_message);
		packet.setMore_follows(this.more_follows);
		packet.setSegmented_response_accepted(this.segmented_response_accepted);
		packet.setMax_segments_accepted(this.max_segments_accepted);
		packet.setMax_APDU_length_accepted(getMax_APDU_length_accepted());
		packet.setSequence_number(this.sequence_number);
		packet.setProposed_window_size(this.proposed_window_size);

		if (encodedBuffer == null) {
			encodedBuffer = encodeBuffer();
		}

		packet.setBuffer(encodedBuffer);
		return packet;
	}
}
