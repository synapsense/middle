package com.synapsense.bacnet.network.media;

import java.lang.Thread.State;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.apache.log4j.Logger;

import com.synapsense.bacnet.network.Configuration;
import com.synapsense.bacnet.network.jni.PacketReader;
import com.synapsense.scheduler.SimpleTaskUnit;
import com.synapsense.scheduler.TaskScheduler;

public class PacketMonitor implements SimpleTaskUnit {
	static Logger logger = Logger.getLogger(PacketMonitor.class);
	private ConcurrentMap<Integer, Long> packetQueue;
	private static int timeout;
	private static PacketMonitor instanse;

	static {
		instanse = new PacketMonitor();
		if (Configuration.DEFAULT_SESSION_TIMEOUT <= 0) {
			timeout = 3000;
		} else {
			timeout = Configuration.DEFAULT_SESSION_TIMEOUT * 2;
		}
		TaskScheduler.getInstance().addSvcTask(instanse);
	}

	private PacketMonitor() {
		packetQueue = new ConcurrentHashMap<Integer, Long>();
		logger.debug("Instanse of [PacketMonitor] created");
	}

	public static PacketMonitor getInstanse() {
		return instanse;
	}

	@Override
	public void Execute() throws Exception {
		while (Thread.currentThread().getState() != State.TERMINATED) {
			synchronized (packetQueue) {
				if (packetQueue.isEmpty()) {
					try {
						packetQueue.wait();
					} catch (InterruptedException e) {

					}
					continue;
				}
			}
			for (Integer item : packetQueue.keySet()) {
				try {
					if ((System.currentTimeMillis() - packetQueue.get(item)) > timeout) {
						packetQueue.remove(item);
						PacketReader.getInstance().cancelPacket(item);
						if (logger.isDebugEnabled() == true) {
							logger.debug("Packet with ID=[" + item + "] removed afret timeout");
						}

					}
				} catch (Exception e) {
					logger.debug("Exception occures; it is not error", e);
				}
			}
		}
	}

	public void addPacket(int id) {
		synchronized (packetQueue) {
			if (logger.isDebugEnabled() == true) {
				logger.debug("New packet with ID=[" + id + "] added");
			}
			packetQueue.put(id, System.currentTimeMillis());
			packetQueue.notify();
		}
	}

	public void removePacket(int id) {
		packetQueue.remove(id);
		if (logger.isDebugEnabled() == true) {
			logger.debug("New packet with ID=[" + id + "] removed");
		}

	}

	@Override
	public void failureAction(Throwable t) {
		logger.error("failureAction occured...", t);
	}

}
