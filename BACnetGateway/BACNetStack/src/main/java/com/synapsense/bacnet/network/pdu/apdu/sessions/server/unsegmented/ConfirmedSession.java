package com.synapsense.bacnet.network.pdu.apdu.sessions.server.unsegmented;

import com.synapsense.bacnet.enums.ErrorClass;
import com.synapsense.bacnet.enums.ErrorCode;
import com.synapsense.bacnet.network.media.MPDUHeader;
import com.synapsense.bacnet.network.pdu.apdu.APDUBasePacket;
import com.synapsense.bacnet.network.pdu.apdu.APDUMessageType;
import com.synapsense.bacnet.network.pdu.apdu.datatypes.AbortReason;
import com.synapsense.bacnet.network.pdu.apdu.request.AbortPDU;
import com.synapsense.bacnet.network.pdu.apdu.request.ConfirmedRequestPDU;
import com.synapsense.bacnet.network.pdu.apdu.response.ErrorPDU;
import com.synapsense.bacnet.network.pdu.apdu.sessions.AbstractSession;
import com.synapsense.bacnet.network.pdu.apdu.sessions.SessionManager;
import com.synapsense.bacnet.network.pdu.apdu.sessions.StateConstants;
import com.synapsense.bacnet.network.pdu.apdu.svc.SvcBase;
import com.synapsense.bacnet.network.pdu.apdu.svc.SvcException;
import com.synapsense.bacnet.network.pdu.apdu.svc.SvcFactory;
import com.synapsense.bacnet.network.pdu.apdu.svc.responce.ErrorSvc;
import com.synapsense.bacnet.network.routing.RoutingService;
import com.synapsense.bacnet.system.BACnetError;
import com.synapsense.scheduler.TimeoutTaskUnit;

/**
 * 
 * 
 *
 */

/**
 * @author Alexander Borisov
 * 
 *         </br> <b><i>Confirmed session</i></b></br> </br> <b>IDLE state</b>
 *         </br> </br> <b>ConfirmedBroadcastReceived</b></br> If a
 *         BACnet-Confirmed-Request-PDU whose destination address is a multicast
 *         or broadcast address is received from the network layer, then enter
 *         the IDLE state.</br> </br> <b>ConfirmedUnsegmentedReceived</b></br>
 *         If a BACnet-Confirmed-Request-PDU whose 'segmented-message' parameter
 *         is FALSE is received from the network layer,then send a
 *         CONF_SERV.indication to the local application program, start
 *         RequestTimer; and enter the AWAIT_RESPONSE state.</br> </br>
 *         <b>AbortPDU_Received</b></br> If a BACnet-Abort-PDU whose 'server'
 *         parameter is FALSE is received from the network layer,</br> then
 *         enter the IDLE state.</br> </br> <b>UnexpectedPDU_Received</b></br>
 *         If an unexpected PDU (BACnet-Confirmed-Request-PDU with
 *         'segmented-message' = TRUE and 'sequencenumber' not equal to zero or
 *         BACnet-SegmentACK-PDU with 'server' = FALSE) is received from the
 *         network layer, then issue an N-UNITDATA.request with
 *         'data_expecting_reply' = FALSE to transmit a BACnet-Abort-PDU with
 *         'server' = TRUE; and enter the IDLE state.</br> </br>
 *         <b>AWAIT_RESPONSE state</b></br> </br> <b>SendSimpleACK</b></br> If a
 *         CONF_SERV.response(+) is received from the local application program,
 *         which is to be conveyed via a BACnet-SimpleACK-PDU, then issue an
 *         N-UNITDATA.request with 'data_expecting_reply' = FALSE to transmit a
 *         BACnet-SimpleACK-PDU and enter the IDLE state.</br> </br>
 *         <b>SendUnsegmentedComplexACK</b></br> If a CONF_SERV.response(+) is
 *         received from the local application program, which is to be conveyed
 *         via a BACnet-ComplexACK-PDU, and the length of the APDU is less than
 *         or equal to maximum-transmittable-length as determined according to
 *         5.2.1, then issue an N-UNITDATA.request with 'data_expecting_reply' =
 *         FALSE to transmit a BACnet-ComplexACKPDU with 'segmented-message' =
 *         FALSE and enter the IDLE state.</br> </br>
 *         <b>CannotSendSegmentedComplexACK</b></br> If a CONF_SERV.response(+)
 *         is received from the local application program, which is to be
 *         conveyed via a BACnet-ComplexACK-PDU, and the length of the APDU is
 *         greater than maximum-transmittable-length as determined according to
 *         5.2.1, and either</br>
 * 
 *         a) this device does not support the transmission of segmented
 *         messages or</br> b) the client will not accept a segmented response
 *         (the 'segmented-response-accepted' parameter in BACnet-</br>
 *         ConfirmedRequest-PDU is FALSE), or</br> c) the client's
 *         max-segments-accepted parameter in the BACnet-ConfirmedRequest-PDU is
 *         fewer than required to</br> transmit the total APDU or,</br> d) the
 *         number of segments transmittable by this device is fewer than
 *         required to transmit the total APDU,</br> then issue an
 *         N-UNITDATA.request with 'data_expecting_reply' = FALSE to transmit a
 *         BACnet-Abort-PDU with</br> 'server' = TRUE and 'abort-reason' =
 *         SEGMENTATION_NOT_SUPPORTED for case (a) and (b), or</br>
 *         BUFFER_OVERFLOW for case (c) and (d), and enter the IDLE state.</br>
 *         </br> <b>SendErrorPDU</b></br> If a CONF_SERV.response(-) is received
 *         from the local application program,</br> then issue an
 *         N-UNITDATA.request with 'data_expecting_reply' = FALSE to transmit a
 *         BACnet-Error-PDU and enter the IDLE state.</br> </br>
 *         <b>SendAbort</b></br> If ABORT.request is received from the local
 *         application program, then stop SegmentTimer; issue an
 *         N-UNITDATA.request with 'data_expecting_reply' = FALSE to transmit a
 *         BACnet-Abort-PDU with 'server' = TRUE; and enter the IDLE state.</br>
 *         </br> <b>SendReject</b></br> If REJECT.request is received from the
 *         local application program,</br> then stop SegmentTimer; issue an
 *         N-UNITDATA.request with 'data_expecting_reply' = FALSE to transmit a
 *         BACnet-Reject-PDU; and enter the IDLE state.</br> </br>
 *         <b>AbortPDU_Received</b></br> If a BACnet-Abort-PDU whose 'server'
 *         parameter is FALSE is received from the network layer, then send
 *         ABORT.indication to the local application program; and enter the IDLE
 *         state.</br> </br> <b>DuplicateRequestReceived</b></br> If a
 *         BACnet-Confirmed-Request-PDU whose 'segmented-message' parameter is
 *         FALSE is received from the network layer, then discard the PDU as a
 *         duplicate request, and re-enter the current state.</br> </br>
 *         <b>UnexpectedPDU_Received</b></br> If an unexpected PDU
 *         (BACnet-SegmentACK-PDU whose 'server' parameter is FALSE) is received
 *         from the network layer, then issue an N-UNITDATA.request with
 *         'data_expecting_reply' = FALSE to transmit a BACnet-Abort-PDU
 *         with</br> 'server' = TRUE; send ABORT.indication to the local
 *         application program; and enter the IDLE state.</br> </br>
 *         <b>Timeout</b></br> If RequestTimer becomes greater than Tout, then
 *         issue an N-UNITDATA.request with 'data_expecting_reply' = FALSE to
 *         transmit a BACnet-Abort-PDU with 'server' = TRUE; send
 *         ABORT.indication to the local application program; and enter the IDLE
 *         state.</br> </br>
 * 
 */

public class ConfirmedSession extends AbstractSession implements TimeoutTaskUnit {

	public ConfirmedSession(APDUBasePacket packet) {
		super(packet);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void addMessageToSession(APDUBasePacket packet) {
		if (state != StateConstants.AWAIT_RESPONSE) {
			// obj = null;
			SessionManager.getInstance().removeSession(this);
			return;
		}

		if (packet.getMessageType() != APDUMessageType.abort_PDU) {
		} else {
			MPDUHeader header;
			try {
				header = RoutingService.createResponseHeader(packet.getHeader());
				AbortPDU apdu = new AbortPDU(header, AbortReason.other);
				apdu.setServer(true);
				apdu.setId(packet.getInvokeId());
				apdu.send();
			} catch (Exception e) {
				logger.warn("Exception occures", e);
			}
		}

		// obj = null;
		SessionManager.getInstance().removeSession(this);
	}

	@Override
	public void timeoutAction() {
		MPDUHeader header;
		obj = null;
		try {
			header = RoutingService.createResponseHeader(packet.getHeader());
			AbortPDU apdu = new AbortPDU(header);
			apdu.setServer(true);
			apdu.setId(packet.getInvokeId());
			apdu.setAbortReason(AbortReason.other);
			apdu.send();
		} catch (Exception e) {
			logger.warn("Exception occures", e);
		}
		SessionManager.getInstance().removeSession(this);
	}

	@Override
	public void Execute() throws Exception {
		if (packet.getHeader().media.isBroadcast(packet.getHeader().DADR)) {
			SessionManager.getInstance().removeSession(this);
			return;
		}
		state = StateConstants.AWAIT_RESPONSE;
		SvcFactory sf = SvcFactory.getInstance();
		SvcBase svc = sf.getConfirmedSvc(packet);
		if (svc == null) {
			logger.warn("Unable to create service for [" + packet + "]");
			throw new SvcException("Unable to create service for [" + packet + "]");
		}
		if (logger.isDebugEnabled()) {
			logger.debug("Processing " + svc);
		}
		svc.decodeBuffer(packet.getBuffer());
		svc.setHeader(packet.getHeader());
		SvcBase[] results = svc.execute();
		if (obj == null)
			return;
		for (SvcBase result : results) {
			if (obj == null)
				return;
			result.getPacket().send();
			if (obj == null)
				return;
		}
		SessionManager.getInstance().removeSession(this);
	}

	@Override
	public void failureAction(Throwable t) {
		MPDUHeader header;
		obj = null;
		try {
			header = RoutingService.createResponseHeader(packet.getHeader());
			header.SNET = packet.getHeader().DNET;
			header.SLEN = packet.getHeader().DLEN;
			header.SADR = packet.getHeader().DADR;
			ErrorSvc svc = new ErrorSvc(header);
			if (t != null && t instanceof BACnetError) {
				svc.setError((BACnetError) t);
			} else {
				svc.setError(new BACnetError(ErrorClass.SERVICES, ErrorCode.OTHER));
			}
			ErrorPDU apdu = (ErrorPDU) svc.getPacket();
			apdu.setId(packet.getInvokeId());

			apdu.setError_choice(((ConfirmedRequestPDU) packet).getService_choice());
			apdu.send();
		} catch (Exception e) {
			logger.warn("Exception occures", e);
		}
		SessionManager.getInstance().removeSession(this);
	}
}
