package com.synapsense.bacnet.network.pdu.apdu.sessions.client.segmented;

import com.synapsense.bacnet.network.pdu.apdu.request.AbortPDU;
import com.synapsense.bacnet.network.pdu.apdu.sessions.AbstractSession;
import com.synapsense.bacnet.network.pdu.apdu.svc.SvcBase;
import com.synapsense.scheduler.TimeoutTaskUnit;

public class SegmentedSession extends AbstractSession implements TimeoutTaskUnit {

	public SegmentedSession(SvcBase svc) {
		super(svc);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void timeoutAction() {
		// TODO Auto-generated method stub

	}

	@Override
	public void Execute() throws Exception {
		AbortPDU abort = new AbortPDU(packet.getHeader());
		abort.send();
	}

	@Override
	public void failureAction(Throwable t) {
		// TODO Auto-generated method stub

	}

}
