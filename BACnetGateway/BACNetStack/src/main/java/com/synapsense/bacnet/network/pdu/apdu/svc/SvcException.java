package com.synapsense.bacnet.network.pdu.apdu.svc;

/**
 * 
 * @author Alexander Borisov
 * 
 */
public class SvcException extends RuntimeException {

	public SvcException(String string) {
		super(string);
	}

	public SvcException(String string, Exception e) {
		super(string, e);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -6257629122626478173L;

}
