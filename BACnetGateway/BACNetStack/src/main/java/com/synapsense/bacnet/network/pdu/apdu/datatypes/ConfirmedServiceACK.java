package com.synapsense.bacnet.network.pdu.apdu.datatypes;

public enum ConfirmedServiceACK {
// -- Alarm and Event Services
getAlarmSummary(3), // [3] GetAlarmSummary-ACK,
getEnrollmentSummary(4), // [4] GetEnrollmentSummary-ACK,
getEventInformation(29), // [29] GetEventInformation-ACK,
// -- File Access Services
atomicReadFile(6), // [6] AtomicReadFile-ACK,
atomicWriteFile(7), // [7] AtomicWriteFile-ACK,
// -- Object Access Services
createObject(10), // [10], /*CreateObject-ACK,
readProperty(12), // [12], /*ReadProperty-ACK,
readPropertyConditional(13), // [13] ReadPropertyConditional-ACK,
readPropertyMultiple(14), // [14] ReadPropertyMultiple-ACK,
readRange(26), // [26] ReadRange-ACK,
// -- Remote Device Management Services
confirmedPrivateTransfer(18), // [18] ConfirmedPrivateTransfer-ACK,
// -- Virtual Terminal Services
vtOpen(21), // [21] VT-Open-ACK,
vtData(23), // [23] VT-Data-ACK,
// -- Security Services
authenticate(24);// [24] Authenticate-ACK

private final int Choice;

ConfirmedServiceACK(int Choice) {
	this.Choice = Choice;
}

public int getCode() {
	return Choice;
}

public static ConfirmedServiceACK valueOf(int value) {
	switch (value) {
	case 3:
		return getAlarmSummary;
	case 4:
		return getEnrollmentSummary;
	case 29:
		return getEventInformation;
	case 6:
		return atomicReadFile;
	case 7:
		return atomicWriteFile;
	case 10:
		return createObject;
	case 12:
		return readProperty;
	case 13:
		return readPropertyConditional;
	case 14:
		return readPropertyMultiple;
	case 26:
		return readRange;
	case 18:
		return confirmedPrivateTransfer;
	case 21:
		return vtOpen;
	case 23:
		return vtData;
	case 24:
		return authenticate;
	default:
		throw new IllegalArgumentException("Unable to identify enum type");
	}
}
}
