/**
 * 
 */
package com.synapsense.bacnet.network.jni;

import org.apache.log4j.Logger;

/**
 * @author Alexander Borisov
 * 
 */
public class PacketReader {
	private static Logger logger = Logger.getLogger(PacketReader.class);
	private static PacketReader instance = new PacketReader();

	public static PacketReader getInstance() {
		return instance;
	}

	/**
	 * These methods are native and implements in c++ library
	 * 
	 */

	public native void setLogConfigPath(String value);

	public native int createPacket();

	public native void sendPacket(int packetID) throws UnableToSendException, PropertyNotFoundException,
	        MalformedPacketException, UnableToSetUpInterfaceException;

	public native int startRead();

	public native void readDone(int packetID);

	public native void cancelPacket(int packetID);

	public native String[] getAdaptersInfo();

	public native void startCapture(int number, boolean isDebug) throws UnableToSetUpInterfaceException;

	public native void stopCapture();

	public native int readInt(int packetID, int propCode) throws PropertyNotFoundException;

	public native short readShort(int packetID, int propCode) throws PropertyNotFoundException;

	public native double readDouble(int packetID, int propCode) throws PropertyNotFoundException;

	public native boolean readBool(int packetID, int propCode) throws PropertyNotFoundException;

	public native byte readByte(int packetID, int propCode) throws PropertyNotFoundException;

	public native String readString(int packetID, int propCode) throws PropertyNotFoundException;

	public native void setInt(int packetID, int propCode, int value) throws PropertyNotFoundException;

	public native void setByte(int packetID, int propCode, byte value) throws PropertyNotFoundException;

	public native void setShort(int packetID, int propCode, short value) throws PropertyNotFoundException;

	public native void setString(int packetID, int propCode, String value) throws PropertyNotFoundException;

	public native void setBool(int packetID, int propCode, boolean value) throws PropertyNotFoundException;

	public native void setDouble(int packetID, int propCode, double value) throws PropertyNotFoundException;

	public native int[] readIntArray(int packetID, int propCode) throws PropertyNotFoundException;

	public native double[] readDoubleArray(int packetID, int propCode) throws PropertyNotFoundException;

	public native boolean[] readBoolArray(int packetID, int propCode) throws PropertyNotFoundException;

	public native short[] readShortArray(int packetID, int propCode) throws PropertyNotFoundException;

	public native byte[] readByteArray(int packetID, int propCode) throws PropertyNotFoundException;

	public native String[] readStringArray(int packetID, int propCode) throws PropertyNotFoundException;

	public native void setIntArray(int packetID, int propCode, int[] value) throws PropertyNotFoundException;

	public native void setDoubleArray(int packetID, int propCode, double[] value) throws PropertyNotFoundException;

	public native void setBoolArray(int packetID, int propCode, boolean[] value) throws PropertyNotFoundException;

	public native void setShortArray(int packetID, int propCode, short[] value) throws PropertyNotFoundException;

	public native void setByteArray(int packetID, int propCode, byte[] value) throws PropertyNotFoundException;

	public native void setStringArray(int packetID, int propCode, String[] value) throws PropertyNotFoundException;

}
