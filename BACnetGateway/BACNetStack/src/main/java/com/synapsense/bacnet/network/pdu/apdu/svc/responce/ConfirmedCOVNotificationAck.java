/**
 * 
 */
package com.synapsense.bacnet.network.pdu.apdu.svc.responce;

import com.synapsense.bacnet.network.media.MPDUHeader;
import com.synapsense.bacnet.network.pdu.apdu.APDUBasePacket;
import com.synapsense.bacnet.network.pdu.apdu.datatypes.ConfirmedServiceChoice;
import com.synapsense.bacnet.network.pdu.apdu.response.SimpleACKPDU;
import com.synapsense.bacnet.network.pdu.apdu.svc.SvcBase;
import com.synapsense.bacnet.network.pdu.apdu.svc.SvcException;
import com.synapsense.bacnet.network.routing.RouteNotFoundException;
import com.synapsense.bacnet.network.routing.RoutingService;

/**
 * @author aborisov
 * 
 */
public class ConfirmedCOVNotificationAck extends SvcBase {

	/**
	 * @param svc
	 */
	public ConfirmedCOVNotificationAck(SvcBase svc) {
		super(svc);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param header
	 */
	public ConfirmedCOVNotificationAck(MPDUHeader header) {
		super(header);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param packet
	 */
	public ConfirmedCOVNotificationAck(APDUBasePacket packet) {
		super(packet);
		// TODO Auto-generated constructor stub
	}

	/**
	 * 
	 */
	public ConfirmedCOVNotificationAck() {
		// TODO Auto-generated constructor stub
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.synapsense.bacnet.network.pdu.apdu.svc.SvcBase#decodeBuffer(byte[])
	 */
	@Override
	public void decodeBuffer(byte[] buffer) {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsense.bacnet.network.pdu.apdu.svc.SvcBase#encodeBuffer()
	 */
	@Override
	public byte[] encodeBuffer() {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsense.bacnet.network.pdu.apdu.svc.SvcBase#getPacket()
	 */
	@Override
	public APDUBasePacket getPacket() {
		if (header == null)
			throw new SvcException("Unable to create APDU packet");

		if (header.DST_MAC == null) {
			try {
				header.DST_MAC = MPDUHeader.getCopy(RoutingService.discoverRoute(header.DNET).getDST_MAC());
			} catch (RouteNotFoundException e) {
				throw new SvcException(e.getLocalizedMessage());
			}
		}

		SimpleACKPDU response = new SimpleACKPDU(header);
		response.setId(invokeID);
		response.setService_ACK_choice(ConfirmedServiceChoice.confirmedCOVNotification);
		response.setBuffer(encodedBuffer);
		return response;
	}

}
