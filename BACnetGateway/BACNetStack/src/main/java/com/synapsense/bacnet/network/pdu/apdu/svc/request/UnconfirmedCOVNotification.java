/**
 * 
 */
package com.synapsense.bacnet.network.pdu.apdu.svc.request;

import java.io.IOException;

import com.synapsense.bacnet.network.media.MPDUHeader;
import com.synapsense.bacnet.network.pdu.apdu.APDUBasePacket;
import com.synapsense.bacnet.network.pdu.apdu.codecs.PrimitivesEncoder;
import com.synapsense.bacnet.network.pdu.apdu.datatypes.UnconfirmedServiceChoice;
import com.synapsense.bacnet.network.pdu.apdu.request.UnconfirmedRequestPDU;
import com.synapsense.bacnet.network.pdu.apdu.sessions.AbstractSession;
import com.synapsense.bacnet.network.pdu.apdu.sessions.client.unsegmented.UnconfirmedSession;
import com.synapsense.bacnet.network.pdu.apdu.svc.SvcBase;
import com.synapsense.bacnet.network.pdu.apdu.svc.SvcException;
import com.synapsense.bacnet.network.routing.RouteNotFoundException;
import com.synapsense.bacnet.network.routing.RoutingService;
import com.synapsense.bacnet.system.BACnetPropertyValue;
import com.synapsense.bacnet.system.shared.BACnetObjectIdentifier;

/**
 * @author Alexander Borisov
 * 
 * 
 *         UnconfirmedCOVNotification-Request ::= SEQUENCE {
 *         subscriberProcessIdentifier [0] Unsigned32,
 *         initiatingDeviceIdentifier [1] BACnetObjectIdentifier,
 *         monitoredObjectIdentifier [2] BACnetObjectIdentifier, timeRemaining
 *         [3] Unsigned, listOfValues [4] SEQUENCE OF BACnetPropertyValue }
 */

public class UnconfirmedCOVNotification extends SvcBase {
	private long subscriberProcessIdentifier;
	private BACnetObjectIdentifier initiatingDeviceIdentifier = null;
	private BACnetObjectIdentifier monitoredObjectIdentifier;
	private long timeRemaining;
	private BACnetPropertyValue[] listOfValues;

	/**
	 * @param svc
	 */
	public UnconfirmedCOVNotification(SvcBase svc) {
		super(svc);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param header
	 */
	public UnconfirmedCOVNotification(MPDUHeader header) {
		super(header);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param packet
	 */
	public UnconfirmedCOVNotification(APDUBasePacket packet) {
		super(packet);
		// TODO Auto-generated constructor stub
	}

	/**
	 * 
	 */
	public UnconfirmedCOVNotification() {
		// TODO Auto-generated constructor stub
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.synapsense.bacnet.network.pdu.apdu.svc.SvcBase#decodeBuffer(byte[])
	 */
	@Override
	public void decodeBuffer(byte[] buffer) {
		/*
		 * PrimitivesDecoder pd = new PrimitivesDecoder(buffer); try{
		 * subscriberProcessIdentifier = pd.decodeUInt(0);
		 * initiatingDeviceIdentifier = pd.decodeObjectID(1);
		 * monitoredObjectIdentifier = pd.decodeObjectID(2); timeRemaining =
		 * pd.decodeUInt(3); Vector <BACnetPropertyValue> tmp = new
		 * Vector<BACnetPropertyValue>(); pd.decodeSequenceStart(4); while
		 * (pd.askForSequenceStop(4)){ pd.decodeObjectID(0); BACnetTL tag =
		 * pd.askForTag(); if(tag.getID() == 1) { pd.decodeUInt(1); } else if
		 * (tag.getID() != 2) { throw new Exception(); }
		 * 
		 * tmp.add(new BACnetPropertyValue (pd.decode)) }
		 * 
		 * 
		 * 
		 * } catch (IOException e){
		 * logger.warn("Unable to decode packet buffer"); throw new
		 * SvcException("Unable to decode packet buffer"); }
		 */
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsense.bacnet.network.pdu.apdu.svc.SvcBase#encodeBuffer()
	 */
	@Override
	public byte[] encodeBuffer() {
		/*
		 * UnconfirmedCOVNotification-Request ::= SEQUENCE {
		 * subscriberProcessIdentifier [0] Unsigned32,initiatingDeviceIdentifier
		 * [1] BACnetObjectIdentifier,monitoredObjectIdentifier [2]
		 * BACnetObjectIdentifier,timeRemaining [3] Unsigned,listOfValues [4]
		 * SEQUENCE OF BACnetPropertyValue }
		 */

		PrimitivesEncoder pe = new PrimitivesEncoder();
		try {
			pe.encodeUInt(0, subscriberProcessIdentifier);
			pe.encodeObjectID(1, initiatingDeviceIdentifier);
			pe.encodeObjectID(2, monitoredObjectIdentifier);
			pe.encodeUInt(3, timeRemaining);
			pe.encodeSequenceStart(4);
			for (BACnetPropertyValue pv : listOfValues) {
				pe.encodeUInt(0, pv.propId);
				pe.encodeUInt(1, pv.propertyArrayIndex);
			}
			return pe.getEncodedBuffer();
		} catch (IOException e) {
			logger.warn("Unable to encode buffer", e);
			throw new SvcException("Cannot encode service", e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsense.bacnet.network.pdu.apdu.svc.SvcBase#getPacket()
	 */
	@Override
	public APDUBasePacket getPacket() {
		if (header == null)
			throw new SvcException("Unable to create APDU packet");

		if (header.DST_MAC == null) {
			try {
				header.DST_MAC = MPDUHeader.getCopy(RoutingService.discoverRoute(header.DNET).getDST_MAC());
			} catch (RouteNotFoundException e) {
				throw new SvcException(e.getLocalizedMessage());
			}
		}
		UnconfirmedRequestPDU packet = new UnconfirmedRequestPDU(header);
		packet.setChoise(UnconfirmedServiceChoice.unconfirmedCOVNotification);

		if (encodedBuffer == null) {
			encodedBuffer = encodeBuffer();
		}
		packet.setBuffer(encodedBuffer);
		return packet;
	}

	public AbstractSession createSession() {
		return new UnconfirmedSession(this);
	}

	@Override
	public AbstractSession createSession(int numExpectedPackets) {
		return new UnconfirmedSession(this, numExpectedPackets);
	}

	/**
	 * @return the subscriberProcessIdentifier
	 */
	public long getSubscriberProcessIdentifier() {
		return subscriberProcessIdentifier;
	}

	/**
	 * @param subscriberProcessIdentifier
	 *            the subscriberProcessIdentifier to set
	 */
	public void setSubscriberProcessIdentifier(long subscriberProcessIdentifier) {
		this.subscriberProcessIdentifier = subscriberProcessIdentifier;
	}

	/**
	 * @return the initiatingDeviceIdentifier
	 */
	public BACnetObjectIdentifier getInitiatingDeviceIdentifier() {
		return initiatingDeviceIdentifier;
	}

	/**
	 * @param initiatingDeviceIdentifier
	 *            the initiatingDeviceIdentifier to set
	 */
	public void setInitiatingDeviceIdentifier(BACnetObjectIdentifier initiatingDeviceIdentifier) {
		this.initiatingDeviceIdentifier = initiatingDeviceIdentifier;
	}

	/**
	 * @return the monitoredObjectIdentifier
	 */
	public BACnetObjectIdentifier getMonitoredObjectIdentifier() {
		return monitoredObjectIdentifier;
	}

	/**
	 * @param monitoredObjectIdentifier
	 *            the monitoredObjectIdentifier to set
	 */
	public void setMonitoredObjectIdentifier(BACnetObjectIdentifier monitoredObjectIdentifier) {
		this.monitoredObjectIdentifier = monitoredObjectIdentifier;
	}

	/**
	 * @return the timeRemaining
	 */
	public long getTimeRemaining() {
		return timeRemaining;
	}

	/**
	 * @param timeRemaining
	 *            the timeRemaining to set
	 */
	public void setTimeRemaining(long timeRemaining) {
		this.timeRemaining = timeRemaining;
	}

	/**
	 * @return the listOfValues
	 */
	public BACnetPropertyValue[] getListOfValues() {
		return listOfValues;
	}

	/**
	 * @param listOfValues
	 *            the listOfValues to set
	 */
	public void setListOfValues(BACnetPropertyValue[] listOfValues) {
		this.listOfValues = listOfValues;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsense.bacnet.network.pdu.apdu.svc.SvcBase#execute()
	 */
	@Override
	public SvcBase[] execute() {
		logger.info("UnconfirmedCOVNotification occures");
		return null;
	}
}
