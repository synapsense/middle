/**
 * 
 */
package com.synapsense.bacnet.network.pdu.apdu.request;

import com.synapsense.bacnet.enums.BACnetAPDUSize;
import com.synapsense.bacnet.network.Constants;
import com.synapsense.bacnet.network.jni.BACNetException;
import com.synapsense.bacnet.network.jni.Property;
import com.synapsense.bacnet.network.jni.PropertyNotFoundException;
import com.synapsense.bacnet.network.media.MPDUHeader;
import com.synapsense.bacnet.network.media.Media;
import com.synapsense.bacnet.network.pdu.apdu.APDUBasePacket;
import com.synapsense.bacnet.network.pdu.apdu.APDUMessageType;
import com.synapsense.bacnet.network.pdu.apdu.datatypes.ConfirmedServiceChoice;

/**
 * @author Alexander Borisov </br> BACnet-Confirmed-Request-PDU ::= SEQUENCE
 *         {</br> pdu-type [0] Unsigned (0..15), -- 0 for this PDU type</br>
 *         segmented-message [1] BOOLEAN,</br> more-follows [2] BOOLEAN,</br>
 *         segmented-response-accepted [3] BOOLEAN,</br> reserved [4] Unsigned
 *         (0..3), -- must be set to zero</br> max-segments-accepted [5]
 *         Unsigned (0..7), -- as per 20.1.2.4</br> max-APDU-length-accepted [6]
 *         Unsigned (0..15), -- as per 20.1.2.5</br> invokeID [7] Unsigned
 *         (0..255),</br> sequence-number [8] Unsigned (0..255) OPTIONAL, --
 *         only if segmented msg</br> proposed-window-size [9] Unsigned (1..127)
 *         OPTIONAL, -- only if segmented msg</br> service-choice [10]
 *         BACnetConfirmedServiceChoice,</br> service-request [11]
 *         BACnet-Confirmed-Service-Request OPTIONAL</br> -- Context-specific
 *         tags 0..11 are NOT used in header encoding</br> }</br>
 */
public class ConfirmedRequestPDU extends APDUBasePacket {
	private boolean segmented_message;// [1] BOOLEAN,
	private boolean more_follows;// [2] BOOLEAN,
	private boolean segmented_response_accepted;// [3] BOOLEAN,
	private int max_segments_accepted;// [5] Unsigned (0..7), -- as per 20.1.2.4
	private BACnetAPDUSize max_APDU_length_accepted;// [6] Unsigned (0..15), --
													// as per
	// 20.1.2.5
	private int sequence_number;// [8] Unsigned (0..255) OPTIONAL, -- only if
	                            // segmented
	// msg
	private int proposed_window_size;// [9] Unsigned (1..127) OPTIONAL, -- only
	                                 // if
	// segmented msg
	private ConfirmedServiceChoice service_choice;// [10]

	// BACnetConfirmedServiceChoice,

	public ConfirmedRequestPDU(MPDUHeader header) {
		this.header = header;
		type = APDUMessageType.confirmed_request_PDU;
	}

	public ConfirmedServiceChoice getService_choice() {
		return service_choice;
	}

	public boolean isSegmented_message() {
		return segmented_message;
	}

	public void setSegmented_message(boolean segmented_message) {
		this.segmented_message = segmented_message;
	}

	public boolean isMore_follows() {
		return more_follows;
	}

	public void setMore_follows(boolean more_follows) {
		this.more_follows = more_follows;
	}

	public boolean isSegmented_response_accepted() {
		return segmented_response_accepted;
	}

	public void setSegmented_response_accepted(boolean segmented_response_accepted) {
		this.segmented_response_accepted = segmented_response_accepted;
	}

	public int getMax_segments_accepted() {
		return max_segments_accepted;
	}

	public void setMax_segments_accepted(int max_segments_accepted) {
		this.max_segments_accepted = max_segments_accepted;
	}

	public BACnetAPDUSize getMax_APDU_length_accepted() {
		return max_APDU_length_accepted;
	}

	public void setMax_APDU_length_accepted(BACnetAPDUSize max_APDU_length_accepted) {
		this.max_APDU_length_accepted = max_APDU_length_accepted;
	}

	public int getSequence_number() {
		return sequence_number;
	}

	public void setSequence_number(int sequence_number) {
		this.sequence_number = sequence_number;
	}

	public int getProposed_window_size() {
		return proposed_window_size;
	}

	public void setProposed_window_size(int proposed_window_size) {
		this.proposed_window_size = proposed_window_size;
	}

	// public ConfirmedServiceChoice getService_choice() {
	// return service_choice;
	// }

	public void setService_choice(ConfirmedServiceChoice service_choice) {
		this.service_choice = service_choice;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsense.bacnet.BACNetBaseMessage#recive()
	 */
	@Override
	public void receive() throws PropertyNotFoundException {
		Media reader = header.media;
		invokeID = reader.readByte(header.packetId, Property.INVOKE_ID) & 0xFF;
		segmented_message = reader.readBoolean(header.packetId, Property.IS_SEG_REQUEST);
		segmented_response_accepted = reader.readBoolean(header.packetId, Property.SEG_RESP_ACCEPTED);
		more_follows = reader.readBoolean(header.packetId, Property.MORE_SEG_FOLLOW);
		max_segments_accepted = reader.readByte(header.packetId, Property.MAX_SEGS) & 0xFF;
		max_APDU_length_accepted = BACnetAPDUSize.valueOf(reader.readByte(header.packetId, Property.MAX_RESP) & 0xFF);
		try {
			sequence_number = reader.readByte(header.packetId, Property.SEQUENCE_NUMBER) & 0xFF;
			proposed_window_size = reader.readByte(header.packetId, Property.WINDOW_SIZE) & 0xFF;
		} catch (PropertyNotFoundException e) {
			sequence_number = Constants.PROPERTY_NOT_SPECIFIED;
			proposed_window_size = Constants.PROPERTY_NOT_SPECIFIED;
		}
		service_choice = ConfirmedServiceChoice
		        .valueOf((reader.readByte(header.packetId, Property.SERV_CHOICE) & 0xFF));
		buffer = reader.readByteArray(header.packetId, Property.PDU_PAYLOAD);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsense.bacnet.BACNetBaseMessage#send()
	 */
	@Override
	public void send() throws BACNetException {
		Media reader = header.media;
		header.send();
		reader.setByte(header.packetId, Property.MESSAGE_TYPE, type.getType());
		reader.setBoolean(header.packetId, Property.IS_SEG_REQUEST, segmented_message);
		reader.setBoolean(header.packetId, Property.MORE_SEG_FOLLOW, more_follows);
		reader.setByte(header.packetId, Property.MAX_SEGS, (byte) max_segments_accepted);
		reader.setBoolean(header.packetId, Property.SEG_RESP_ACCEPTED, segmented_response_accepted);
		reader.setByte(header.packetId, Property.MAX_RESP, (byte) max_APDU_length_accepted.getByteValue());
		reader.setByte(header.packetId, Property.SERV_CHOICE, (byte) service_choice.getCode());
		reader.setByteArray(header.packetId, Property.PDU_PAYLOAD, buffer);
		if (sequence_number != Constants.PROPERTY_NOT_SPECIFIED) {
			reader.setByte(header.packetId, Property.SEQUENCE_NUMBER, (byte) sequence_number);
		}

		if (proposed_window_size != Constants.PROPERTY_NOT_SPECIFIED) {
			reader.setByte(header.packetId, Property.WINDOW_SIZE, (byte) proposed_window_size);
		}
		reader.setByte(header.packetId, Property.INVOKE_ID, (byte) invokeID);
		reader.sendPacket(header.packetId);
	}

	@Override
	public boolean isSegmented() {
		return segmented_message;
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer(100);
		sb.append("\n");
		sb.append(this.getClass().getName());
		sb.append(header);
		sb.append("segmented_message = ");
		sb.append(segmented_message);
		sb.append("\n");
		sb.append("invoke ID = ");
		sb.append(invokeID);
		sb.append("\n");

		sb.append("more_follows = ");
		sb.append(more_follows);
		sb.append("\n");

		sb.append("segmented_response_accepted = ");
		sb.append(segmented_response_accepted);
		sb.append("\n");

		sb.append("max_APDU_length_accepted= ");
		sb.append(max_APDU_length_accepted);
		sb.append("\n");

		sb.append("sequence_number= ");
		sb.append(sequence_number);
		sb.append("\n");

		sb.append("proposed_window_size= ");
		sb.append(proposed_window_size);
		sb.append("\n");

		sb.append("service_choice = ");
		sb.append(service_choice);
		sb.append("\n");
		return sb.toString();
	}

}
