package com.synapsense.bacnet.network;

public final class Constants {
	// public static final byte[] BROADCAST_ARRAY =
	// {(byte)0xFF,(byte)0xFF,(byte)0xFF,(byte)0xFF,(byte)0xFF,(byte)0xFF};
	public static final int PROPERTY_NOT_SPECIFIED = Integer.MIN_VALUE;
	public static final int BAC_NET_NET_BROADCAST = 0xFFFF;
	// public static final String BAC_NET_ADDR_BROADCAST = new String
	// (BROADCAST_ARRAY);
}
