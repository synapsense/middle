/**
 * 
 */
package com.synapsense.bacnet.network.routing;

import java.util.Collection;

import org.apache.log4j.Logger;

import com.synapsense.bacnet.network.Configuration;
import com.synapsense.bacnet.network.Constants;
import com.synapsense.bacnet.network.media.MPDUHeader;
import com.synapsense.bacnet.network.media.Media;
import com.synapsense.bacnet.network.media.MediaManager;
import com.synapsense.bacnet.network.pdu.npdu.request.WhoIsRouterToNetwork;

public class RoutingService {
	private static Logger logger = Logger.getLogger(RoutingService.class);

	/**
	 * Creates RoutingService in default configuration
	 */
	private RoutingService() {

	}

	static public Route getRoute(int DNET) throws RouteNotFoundException {
		Route r = RoutingTable.getInstance().getRoute(DNET);
		if (r == null)
			throw new RouteNotFoundException("No route to " + DNET);
		return r;
	}

	static public Route discoverRoute(int DNET) throws RouteNotFoundException {
		Route r = RoutingTable.getInstance().getRoute(DNET);
		if (r != null)
			return r;
		Collection<Media> availiableMedia = MediaManager.getMediaManager().getAllMedia();
		MPDUHeader header = new MPDUHeader();
		for (Media media : availiableMedia) {
			header.media = media;
			header.SRC_MAC = MPDUHeader.getCopy(media.getActiveEthMac());
			header.DST_MAC = media.getBroadcast();
			WhoIsRouterToNetwork packet = new WhoIsRouterToNetwork(header);
			packet.setNdnet(DNET);
			try {
				packet.send();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		synchronized (RoutingTable.getInstance()) {
			try {
				RoutingTable.getInstance().wait(
				        Integer.parseInt(System.getProperty("RoutingService.discoverTimeout", "1000")));
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		Route route = RoutingTable.getInstance().getRoute(DNET);
		if (route == null)
			throw new RouteNotFoundException("Unable to discover route");
		return route;
	}

	static public void addRoute(String data, byte[] mac) {
		addRoute(new Route(data, mac));
	}

	static public void addRoute(Route route) {
		logger.debug("Adding route [" + route.toString() + "]");
		synchronized (RoutingTable.getInstance()) {
			RoutingTable.getInstance().addRoute(route);
			RoutingTable.getInstance().notify();
		}
	}

	static public void updateRoute(String data, byte[] mac) {
		if (logger.isDebugEnabled())
			logger.debug("Updating route [" + data + "]");
		synchronized (RoutingTable.getInstance()) {
			RoutingTable.getInstance().updateRoute(new Route(data, mac));
			RoutingTable.getInstance().notify();
		}
	}

	static public void updateRoute(Route route) {
		if (logger.isDebugEnabled())
			logger.debug("Updating route [" + route + "]");
		synchronized (RoutingTable.getInstance()) {
			RoutingTable.getInstance().updateRoute(route);
			RoutingTable.getInstance().notify();
		}
	}

	static public void removeRoute(int DNET) {
		if (logger.isDebugEnabled())
			logger.debug("Removing route [" + DNET + "]");
		synchronized (RoutingTable.getInstance()) {
			RoutingTable.getInstance().removeRoute(DNET);
			RoutingTable.getInstance().notify();
		}
	}

	static public void dropTable() {
		RoutingTable.getInstance().clear();
	}

	static public String[] getTable() {
		return RoutingTable.getInstance().getTable();
	}

	static public int[] getRoutes() throws RouteNotFoundException {
		return RoutingTable.getInstance().getDNETs();
	}

	static public MPDUHeader getDefaultResponseHeader(MPDUHeader header) {
		logger.debug("Creating default ResponseHeader ");
		MPDUHeader responseHeader = new MPDUHeader();
		responseHeader.media = header.media;

		if (header.media.isBroadcast(header.DST_MAC)) {
			responseHeader.DST_MAC = MPDUHeader.getCopy(header.SRC_MAC);
			responseHeader.SRC_MAC = MPDUHeader.getCopy(header.media.getActiveEthMac());
		} else {
			responseHeader.SRC_MAC = MPDUHeader.getCopy(header.DST_MAC);
			responseHeader.DST_MAC = MPDUHeader.getCopy(header.SRC_MAC);
		}
		responseHeader.priority = header.priority;
		responseHeader.SNET = Configuration.OWN_NET;
		responseHeader.SADR = MPDUHeader.getCopy(Configuration.OWN_ADDR);
		responseHeader.SLEN = responseHeader.SADR.length;
		responseHeader.der = false;
		if (logger.isDebugEnabled())
			logger.debug("Default ResponseHeader " + responseHeader.toString());
		return responseHeader;
	}

	static public MPDUHeader createResponseHeader(MPDUHeader header) throws RouteNotFoundException,
	        MalformedHeaderException {
		logger.debug("Creating ResponseHeader ");
		validateHeader(header);

		MPDUHeader responseHeader = new MPDUHeader();
		responseHeader.media = header.media;

		responseHeader.Control = header.Control;
		responseHeader.BVLC = header.BVLC; // BVLC for BACnet/IP

		if (header.media.isBroadcast(header.DST_MAC)) {
			responseHeader.DST_MAC = header.SRC_MAC;
			responseHeader.SRC_MAC = responseHeader.media.getActiveEthMac();
		} else {
			responseHeader.SRC_MAC = MPDUHeader.getCopy(header.DST_MAC);
			responseHeader.DST_MAC = MPDUHeader.getCopy(header.SRC_MAC);
		}
		responseHeader.priority = header.priority;

		if (logger.isDebugEnabled())
			logger.debug("ResponseHeader " + responseHeader.toString());
		return responseHeader;
	}

	static protected void validateHeader(MPDUHeader header) throws MalformedHeaderException {
		if (header.DNET != Constants.PROPERTY_NOT_SPECIFIED && header.HopCount == Constants.PROPERTY_NOT_SPECIFIED)
			throw new MalformedHeaderException();
	}
}
