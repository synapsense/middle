package com.synapsense.bacnet.network.pdu.apdu.svc.request;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Vector;

import com.synapsense.bacnet.enums.BACnetAPDUSize;
import com.synapsense.bacnet.network.Constants;
import com.synapsense.bacnet.network.media.MPDUHeader;
import com.synapsense.bacnet.network.pdu.apdu.APDUBasePacket;
import com.synapsense.bacnet.network.pdu.apdu.codecs.PrimitivesDecoder;
import com.synapsense.bacnet.network.pdu.apdu.codecs.PrimitivesEncoder;
import com.synapsense.bacnet.network.pdu.apdu.codecs.primitives.BACnetTL;
import com.synapsense.bacnet.network.pdu.apdu.datatypes.UnconfirmedServiceChoice;
import com.synapsense.bacnet.network.pdu.apdu.request.UnconfirmedRequestPDU;
import com.synapsense.bacnet.network.pdu.apdu.sessions.AbstractSession;
import com.synapsense.bacnet.network.pdu.apdu.sessions.client.unsegmented.UnconfirmedSession;
import com.synapsense.bacnet.network.pdu.apdu.svc.SvcBase;
import com.synapsense.bacnet.network.pdu.apdu.svc.SvcException;
import com.synapsense.bacnet.network.pdu.apdu.svc.SvcFactory;
import com.synapsense.bacnet.network.pdu.apdu.svc.responce.IHave;
import com.synapsense.bacnet.network.routing.MalformedHeaderException;
import com.synapsense.bacnet.network.routing.RouteNotFoundException;
import com.synapsense.bacnet.network.routing.RoutingService;
import com.synapsense.bacnet.objects.BACnetDeviceWrapper;
import com.synapsense.bacnet.objects.BACnetObject;
import com.synapsense.bacnet.system.BACnetNetwork;
import com.synapsense.bacnet.system.BACnetSystem;
import com.synapsense.bacnet.system.Device;
import com.synapsense.bacnet.system.shared.BACnetAddressBinding;
import com.synapsense.bacnet.system.shared.BACnetObjectIdentifier;

/**
 * 
 * @author Alexander Borisov
 *
 */

/**
 * </br> <b>Who-Has-Request</b> ::= SEQUENCE { </br> limits SEQUENCE {</br>
 * deviceInstanceRangeLowLimit [0] Unsigned (0..4194303),</br>
 * deviceInstanceRangeHighLimit [1] Unsigned (0..4194303)</br> } OPTIONAL,</br>
 * object CHOICE {</br> objectIdentifier [2] BACnetObjectIdentifier,</br>
 * objectName [3] CharacterString</br> }</br> }</br>
 */
public class WhoHas extends SvcBase {
	protected long deviceInstanceRangeLowLimit = Constants.PROPERTY_NOT_SPECIFIED;
	protected long deviceInstanceRangeHighLimit = Constants.PROPERTY_NOT_SPECIFIED;
	protected BACnetObjectIdentifier objectIdentifier = null;
	protected String objectName = null;

	public WhoHas(MPDUHeader header) {
		super(header);
		uChoise = UnconfirmedServiceChoice.i_Have; // Need to register session
		                                           // for response
	}

	public WhoHas(SvcBase svc) {
		super(svc);
	}

	public WhoHas(APDUBasePacket packet) {
		super(packet);
	}

	@Override
	public void decodeBuffer(byte[] buffer) {
		PrimitivesDecoder pd = new PrimitivesDecoder(buffer);
		try {
			if (pd.askForTag().getID() == 0) {
				deviceInstanceRangeLowLimit = pd.decodeUInt(0);
				deviceInstanceRangeHighLimit = pd.decodeUInt(1);
			}
			BACnetTL tag = pd.askForTag();

			if (tag.getID() == 2) {
				objectIdentifier = pd.decodeObjectID(2);
			} else {
				objectName = pd.decodeCharacterString(3);
			}
		} catch (IOException e) {
			logger.warn("Unable to decode packet buffer", e);
			throw new SvcException("decoded buffer contains illegal data", e);
		}
	}

	@Override
	public byte[] encodeBuffer() {
		PrimitivesEncoder pe = new PrimitivesEncoder();
		try {
			if (deviceInstanceRangeLowLimit != Constants.PROPERTY_NOT_SPECIFIED) {
				pe.encodeUInt(0, deviceInstanceRangeLowLimit);
				pe.encodeUInt(1, deviceInstanceRangeHighLimit);
			}

			if (objectIdentifier != null) {
				pe.encodeObjectID(2, objectIdentifier);
			} else if (objectName != null) {
				pe.encodeCharacterString(3, objectName);
			}
		} catch (IOException e) {
			logger.warn("Unable to encode buffer", e);
			throw new SvcException("Cannot encode service", e);
		}

		return pe.getEncodedBuffer();
	}

	public AbstractSession createSession() {
		return new UnconfirmedSession(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.synapsense.bacnet.network.pdu.apdu.svc.SvcBase#createSession(int)
	 */
	@Override
	public AbstractSession createSession(int numExpectedPackets) {
		return new UnconfirmedSession(this, numExpectedPackets);
	}

	@Override
	public APDUBasePacket getPacket() {
		APDUBasePacket packet;
		if (header == null)
			throw new SvcException("Unable to create APDU packet");
		header.Control = true;
		if (header.DST_MAC == null) {
			try {
				header.DST_MAC = MPDUHeader.getCopy(RoutingService.discoverRoute(header.DNET).getDST_MAC());
			} catch (RouteNotFoundException e) {
				throw new SvcException(e.getLocalizedMessage());
			}
		}

		packet = new UnconfirmedRequestPDU(header);
		if (encodedBuffer == null) {
			encodedBuffer = encodeBuffer();
		}
		packet.setBuffer(encodedBuffer);
		return packet;
	}

	public SvcBase[] execute() {
		if (logger.isDebugEnabled()) {
			logger.debug("Processing WhoIs svc");
		}
		Vector<SvcBase> responses = new Vector<SvcBase>();
		MPDUHeader respHeader;
		try {
			respHeader = RoutingService.createResponseHeader(header);
		} catch (RouteNotFoundException e) {
			logger.warn("Malformed header", e);
			throw new SvcException(e.getLocalizedMessage());
		} catch (MalformedHeaderException e) {
			logger.warn("Malformed header", e);
			throw new SvcException(e.getLocalizedMessage());
		}

		if (header.DNET == Constants.BAC_NET_NET_BROADCAST || header.DNET == Constants.PROPERTY_NOT_SPECIFIED) {
			Collection<BACnetNetwork> networks = BACnetSystem.getInstance().getNetworks();
			for (BACnetNetwork nwk : networks) {
				processNetwork(nwk, responses, respHeader);
			}
		} else {
			BACnetNetwork net = BACnetSystem.getInstance().getNetwork(header.DNET);
			if (net != null) {
				processNetwork(net, responses, respHeader);
			}
		}
		return responses.toArray(new SvcBase[responses.size()]);
	}

	protected void processNetwork(BACnetNetwork network, Vector<SvcBase> responses, MPDUHeader respHeader) {
		Collection<Device> devices = network.getDevices();
		for (Device dev : devices) {
			BACnetDeviceWrapper deviceWrapper = (BACnetDeviceWrapper) dev;

			boolean inRange = false;
			if (deviceInstanceRangeLowLimit == Constants.PROPERTY_NOT_SPECIFIED
			        && deviceInstanceRangeHighLimit == Constants.PROPERTY_NOT_SPECIFIED) {
				inRange = true;
			} else {
				BACnetObjectIdentifier deviceId = deviceWrapper.getWrappee().getObjectIdentifier();
				if (deviceId.getInstanceNumber() >= deviceInstanceRangeLowLimit
				        && deviceId.getInstanceNumber() <= deviceInstanceRangeHighLimit) {
					inRange = true;
				}
			}

			if (inRange) {
				Collection<BACnetObject> objects = deviceWrapper.getWrappee().getObjects();
				List<BACnetObject> resultList = new ArrayList<BACnetObject>();
				for (BACnetObject obj : objects) {
					if (objectIdentifier != null) {
						if (objectIdentifier.equals(obj.getObjectIdentifier())) {
							resultList.add(obj);
							break;
						}
					} else if (objectName != null) {
						if (objectName.equals(obj.getObjectName())) {
							resultList.add(obj);
						}
					}
				}
				for (BACnetObject object : resultList) {
					SvcBase response = SvcFactory.getInstance().getResponseForRequest(this);
					respHeader.SNET = ((BACnetAddressBinding) deviceWrapper.getWrappee().getDeviceAddressBindings()
					        .toArray()[0]).deviceAddress.networkNumber;
					respHeader.SADR = MPDUHeader.getCopy(((BACnetAddressBinding) deviceWrapper.getWrappee()
					        .getDeviceAddressBindings().toArray()[0]).deviceAddress.macAddress);
					respHeader.SLEN = respHeader.SADR.length;

					response.setHeader(respHeader);
					response.setMax_APDU_length_accepted(BACnetAPDUSize.valueOfByLong(deviceWrapper.getWrappee()
					        .getMaxAPDULengthAccepted()));
					((IHave) response).setDeviceIdentifier(deviceWrapper.getWrappee().getObjectIdentifier());
					((IHave) response).setObjectIdentifier(object.getObjectIdentifier());
					((IHave) response).setObjectName((object.getObjectName()));

					responses.add(response);
				}
			}
		}
	}
}
