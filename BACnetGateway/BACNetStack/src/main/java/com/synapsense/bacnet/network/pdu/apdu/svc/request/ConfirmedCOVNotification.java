/**
 * 
 */
package com.synapsense.bacnet.network.pdu.apdu.svc.request;

import java.io.IOException;
import java.util.HashMap;

import com.synapsense.bacnet.network.media.MPDUHeader;
import com.synapsense.bacnet.network.pdu.apdu.APDUBasePacket;
import com.synapsense.bacnet.network.pdu.apdu.codecs.PrimitivesDecoder;
import com.synapsense.bacnet.network.pdu.apdu.codecs.PrimitivesEncoder;
import com.synapsense.bacnet.network.pdu.apdu.codecs.primitives.BACnetTL;
import com.synapsense.bacnet.network.pdu.apdu.datatypes.ConfirmedServiceChoice;
import com.synapsense.bacnet.network.pdu.apdu.request.ConfirmedRequestPDU;
import com.synapsense.bacnet.network.pdu.apdu.sessions.AbstractSession;
import com.synapsense.bacnet.network.pdu.apdu.sessions.client.unsegmented.ConfirmedSession;
import com.synapsense.bacnet.network.pdu.apdu.svc.SvcBase;
import com.synapsense.bacnet.network.pdu.apdu.svc.SvcException;
import com.synapsense.bacnet.network.pdu.apdu.svc.SvcFactory;
import com.synapsense.bacnet.network.routing.MalformedHeaderException;
import com.synapsense.bacnet.network.routing.RouteNotFoundException;
import com.synapsense.bacnet.network.routing.RoutingService;
import com.synapsense.bacnet.services.COVNotificationListener;
import com.synapsense.bacnet.system.BACnetPropertyValue;
import com.synapsense.bacnet.system.shared.BACnetObjectIdentifier;

/**
 * @author Alexander Borisov
 *
 */
/**
 * ConfirmedCOVNotification-Request ::= SEQUENCE { subscriberProcessIdentifier
 * [0] Unsigned32, initiatingDeviceIdentifier [1] BACnetObjectIdentifier,
 * monitoredObjectIdentifier [2] BACnetObjectIdentifier, timeRemaining [3]
 * Unsigned, listOfValues [4] SEQUENCE OF BACnetPropertyValue }
 */
public class ConfirmedCOVNotification extends SvcBase {
	private long subscriberProcessIdentifier;
	private BACnetObjectIdentifier initiatingDeviceIdentifier = null;
	private BACnetObjectIdentifier monitoredObjectIdentifier;
	private long timeRemaining;
	private BACnetPropertyValue[] listOfValues = new BACnetPropertyValue[1];
	private static HashMap<Long, COVNotificationListener> eventsMap = new HashMap<Long, COVNotificationListener>();

	/**
	 * @param svc
	 */
	public ConfirmedCOVNotification(SvcBase svc) {
		super(svc);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the subscriberProcessIdentifier
	 */
	public long getSubscriberProcessIdentifier() {
		return subscriberProcessIdentifier;
	}

	/**
	 * @param subscriberProcessIdentifier
	 *            the subscriberProcessIdentifier to set
	 */
	public void setSubscriberProcessIdentifier(long subscriberProcessIdentifier) {
		this.subscriberProcessIdentifier = subscriberProcessIdentifier;
	}

	/**
	 * @return the initiatingDeviceIdentifier
	 */
	public BACnetObjectIdentifier getInitiatingDeviceIdentifier() {
		return initiatingDeviceIdentifier;
	}

	/**
	 * @param initiatingDeviceIdentifier
	 *            the initiatingDeviceIdentifier to set
	 */
	public void setInitiatingDeviceIdentifier(BACnetObjectIdentifier initiatingDeviceIdentifier) {
		this.initiatingDeviceIdentifier = initiatingDeviceIdentifier;
	}

	/**
	 * @return the monitoredObjectIdentifier
	 */
	public BACnetObjectIdentifier getMonitoredObjectIdentifier() {
		return monitoredObjectIdentifier;
	}

	/**
	 * @param monitoredObjectIdentifier
	 *            the monitoredObjectIdentifier to set
	 */
	public void setMonitoredObjectIdentifier(BACnetObjectIdentifier monitoredObjectIdentifier) {
		this.monitoredObjectIdentifier = monitoredObjectIdentifier;
	}

	/**
	 * @return the timeRemaining
	 */
	public long getTimeRemaining() {
		return timeRemaining;
	}

	/**
	 * @param timeRemaining
	 *            the timeRemaining to set
	 */
	public void setTimeRemaining(long timeRemaining) {
		this.timeRemaining = timeRemaining;
	}

	/**
	 * @return the listOfValues
	 */
	public BACnetPropertyValue[] getListOfValues() {
		return listOfValues;
	}

	/**
	 * @param listOfValues
	 *            the listOfValues to set
	 */
	public void setListOfValues(BACnetPropertyValue[] listOfValues) {
		this.listOfValues = listOfValues;
	}

	/**
	 * @param header
	 */
	public ConfirmedCOVNotification(MPDUHeader header) {
		super(header);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param packet
	 */
	public ConfirmedCOVNotification(APDUBasePacket packet) {
		super(packet);
		// TODO Auto-generated constructor stub
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.synapsense.bacnet.network.pdu.apdu.svc.SvcBase#decodeBuffer(byte[])
	 */
	@Override
	public void decodeBuffer(byte[] buffer) {
		/**
		 * ConfirmedCOVNotification-Request ::= SEQUENCE {
		 * subscriberProcessIdentifier [0] Unsigned32,
		 * initiatingDeviceIdentifier [1] BACnetObjectIdentifier,
		 * monitoredObjectIdentifier [2] BACnetObjectIdentifier, timeRemaining
		 * [3] Unsigned, listOfValues [4] SEQUENCE OF BACnetPropertyValue }
		 */
		PrimitivesDecoder pd = new PrimitivesDecoder(buffer);
		try {
			subscriberProcessIdentifier = pd.decodeUInt(0);
			initiatingDeviceIdentifier = pd.decodeObjectID(1);
			monitoredObjectIdentifier = pd.decodeObjectID(2);
			timeRemaining = pd.decodeUInt(3);
			pd.decodeSequenceStart(4);
			long propID = pd.decodeEnum(0);
			BACnetTL tag = pd.askForTag();
			long arrayIndex = -1;
			if (tag.getID() == 1) {
				arrayIndex = pd.decodeUInt(1);
			}
			pd.decodeSequenceStart(2);
			if (arrayIndex == -1) {
				listOfValues[0] = new BACnetPropertyValue(propID, pd.decodeAny().getBytes());
			} else {
				listOfValues[0] = new BACnetPropertyValue(propID, pd.decodeAny().getBytes(), arrayIndex);
			}
			pd.decodeSequenceStop(2);
			tag = pd.askForTag();
			if (tag.getID() == 3) {
				pd.decodeUInt(3);
			}
			pd.decodeSequenceStop(4);
		} catch (Exception e) {
			logger.warn("Unable to decode packet buffer", e);
			throw new SvcException("Unable to decode packet buffer");
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsense.bacnet.network.pdu.apdu.svc.SvcBase#encodeBuffer()
	 */
	@Override
	public byte[] encodeBuffer() {
		PrimitivesEncoder pe = new PrimitivesEncoder();
		try {
			pe.encodeUInt(0, subscriberProcessIdentifier);
			pe.encodeObjectID(1, initiatingDeviceIdentifier);
			pe.encodeObjectID(2, monitoredObjectIdentifier);
			pe.encodeUInt(3, timeRemaining);
			pe.encodeSequenceStart(4);
			for (BACnetPropertyValue pv : listOfValues) {
				pe.encodeEnum(0, pv.propId);
				if (pv.propertyArrayIndex != null)
					pe.encodeUInt(1, pv.propertyArrayIndex);
				pe.encodeSequenceStart(2);
				pe.appendEncodedBuffer(pv.value);
				pe.encodeSequenceStop(2);
			}
			// Priority not use
			pe.encodeSequenceStop(4);
			return pe.getEncodedBuffer();
		} catch (IOException e) {
			logger.warn("Unable to encode buffer", e);
			throw new SvcException("Cannot encode service", e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsense.bacnet.network.pdu.apdu.svc.SvcBase#getPacket()
	 */
	@Override
	public APDUBasePacket getPacket() {
		if (header == null)
			throw new SvcException("Unable to create APDU packet");
		if (header.DST_MAC == null) {
			try {
				header.DST_MAC = MPDUHeader.getCopy(RoutingService.discoverRoute(header.DNET).getDST_MAC());
			} catch (RouteNotFoundException e) {
				throw new SvcException(e.getLocalizedMessage());
			}
		}
		header.Control = true;
		ConfirmedRequestPDU packet = new ConfirmedRequestPDU(header);
		packet.setId(invokeID);
		packet.setService_choice(ConfirmedServiceChoice.confirmedCOVNotification);
		packet.setSegmented_message(this.segmented_message);
		packet.setMore_follows(this.more_follows);
		packet.setSegmented_response_accepted(this.segmented_response_accepted);
		packet.setMax_segments_accepted(this.max_segments_accepted);
		packet.setMax_APDU_length_accepted(getMax_APDU_length_accepted());
		packet.setSequence_number(this.sequence_number);
		packet.setProposed_window_size(this.proposed_window_size);

		if (encodedBuffer == null) {
			encodedBuffer = encodeBuffer();
		}

		packet.setBuffer(encodedBuffer);
		return packet;
	}

	public AbstractSession createSession() {
		return new ConfirmedSession(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsense.bacnet.network.pdu.apdu.svc.SvcBase#execute()
	 */
	@Override
	public SvcBase[] execute() {
		logger.info("ConfirmedCOVNotification occures");
		logger.info("We just sending ack for this notification");

		if (eventsMap.containsKey(subscriberProcessIdentifier)) {
			eventsMap.get(subscriberProcessIdentifier).valueChanged(subscriberProcessIdentifier,
			        initiatingDeviceIdentifier, monitoredObjectIdentifier, timeRemaining, listOfValues);
		}

		SvcBase response = SvcFactory.getInstance().getResponseForRequest(this);
		try {
			MPDUHeader respHeader = RoutingService.createResponseHeader(header);
			respHeader.SNET = header.DNET;
			respHeader.SLEN = header.DLEN;
			respHeader.SADR = MPDUHeader.getCopy(header.DADR);
			response.setHeader(respHeader);
		} catch (RouteNotFoundException e) {
			throw new SvcException("Malformed header", e);
		} catch (MalformedHeaderException e) {
			throw new SvcException("Malformed header", e);
		}
		SvcBase res[] = { response };
		return res;
	}

	public static void addListener(long subscriberProcessIdentifier, COVNotificationListener listener) {
		if (!eventsMap.containsKey(subscriberProcessIdentifier))
			eventsMap.put(subscriberProcessIdentifier, listener);
	}

	public static void deleteListener(long subscriberProcessIdentifier) {
		eventsMap.remove(subscriberProcessIdentifier);
	}
}
