package com.synapsense.bacnet.network.media.ethernet;

import org.apache.log4j.Logger;

import com.synapsense.bacnet.network.jni.MalformedPacketException;
import com.synapsense.bacnet.network.jni.PacketReader;
import com.synapsense.bacnet.network.jni.Property;
import com.synapsense.bacnet.network.jni.PropertyNotFoundException;
import com.synapsense.bacnet.network.jni.UnableToSendException;
import com.synapsense.bacnet.network.jni.UnableToSetUpInterfaceException;
import com.synapsense.bacnet.network.media.Media;
import com.synapsense.bacnet.network.media.MediaManager;
import com.synapsense.bacnet.network.media.MediaNames;
import com.synapsense.bacnet.network.media.PacketMonitor;

public class Ethernet implements Media {
	private static String PACKET_READER_LIB = "ethernet";
	static protected String[] mediaInfo = PacketReader.getInstance().getAdaptersInfo();
	protected final byte[] MAC;
	static Logger logger = Logger.getLogger(Ethernet.class);
	static PacketReader reader = PacketReader.getInstance();
	private int interfaceIndex = 0;
	static {
		logger.debug("Attempt to load " + PACKET_READER_LIB);
		System.loadLibrary(PACKET_READER_LIB);
		logger.debug("Library loaded successfully");
	}

	public Ethernet() {
		logger.info("loading media information");
		for (int i = 0; i < mediaInfo.length; i++) {
			logger.info("MAC->" + mediaInfo[i]);
		}
		MAC = getMediaAddress(interfaceIndex);
		MediaManager.getMediaManager().registerMedia(MAC, this);
	}

	public Ethernet(Object object) {
		interfaceIndex = (Integer) object;
		logger.info("loading media information");
		for (int i = 0; i < mediaInfo.length; i++) {
			logger.info("MAC->" + mediaInfo[i]);
		}
		MAC = getMediaAddress(interfaceIndex);
		MediaManager.getMediaManager().registerMedia(MAC, this);
	}

	@Override
	public byte[] readDST(int packetID) {
		byte[] result = null;
		try {
			result = readByteArray(packetID, Property.DST_MAC);
		} catch (PropertyNotFoundException e) {
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public byte[] readSRC(int packetID) {
		byte[] result = null;
		try {
			result = readByteArray(packetID, Property.SRC_MAC);
		} catch (PropertyNotFoundException e) {
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public void writeDST(int packetID, byte[] DST_ADDR) {
		if (DST_ADDR != null) {
			try {
				setByteArray(packetID, Property.DST_MAC, DST_ADDR);
			} catch (PropertyNotFoundException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void writeSRC(int packetID, byte[] SRC_ADDR) {
		if (SRC_ADDR != null) {
			try {
				setByteArray(packetID, Property.SRC_MAC, SRC_ADDR);
			} catch (PropertyNotFoundException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public boolean isBroadcast(byte[] b) {
		boolean result = false;
		if (b != null) {
			result = true;
			for (int i = 0; i < b.length; i++) {
				if (b[i] != (byte) 0xFF) {
					result = false;
					break;
				}
			}
		}
		return result;
	}

	@Override
	public byte[] getMediaAddress(int number) {
		if (number > mediaInfo.length)
			return null;
		String mac = null;
		mac = mediaInfo[number];
		int pos = mac.indexOf("->");
		if (pos == -1) {
			return null;
		}
		mac = mac.substring(0, pos);
		byte b[] = new byte[6];
		String sp[] = mac.split(":");
		for (int i = 0; i < sp.length; i++) {
			b[i] = (byte) Integer.parseInt(sp[i], 16);
		}
		return b;
	}

	@Override
	public byte[] getBroadcast() {
		byte b[] = { (byte) 0xff, (byte) 0xff, (byte) 0xff, (byte) 0xff, (byte) 0xff, (byte) 0xff };
		return b;
	}

	@Override
	public byte[] getActiveEthMac() {
		return MAC;
	}

	@Override
	public MediaNames getMediaType() {
		return MediaNames.Ethernet;
	}

	public int readInt(int packetID, Property prop) throws PropertyNotFoundException {
		return reader.readInt(packetID, prop.getCode());
	}

	public byte readByte(int packetID, Property prop) throws PropertyNotFoundException {
		return reader.readByte(packetID, prop.getCode());
	}

	/**
	 * Method is responsible for read property item from network
	 * 
	 * @param prop
	 *            Property name
	 * @return Property value
	 */
	public double readDouble(int packetID, Property prop) throws PropertyNotFoundException {
		return reader.readDouble(packetID, prop.getCode());
	}

	/**
	 * Method is responsible for read property item from network
	 * 
	 * @param prop
	 *            Property name
	 * @return Property value
	 */
	public String readString(int packetID, Property prop) throws PropertyNotFoundException {
		return reader.readString(packetID, prop.getCode());
	}

	/**
	 * Method is responsible for read property item from network
	 * 
	 * @param prop
	 *            Property name
	 * @return Property value
	 */
	public boolean readBoolean(int packetID, Property prop) throws PropertyNotFoundException {
		return reader.readBool(packetID, prop.getCode());
	}

	/**
	 * Method is responsible for read property item from network
	 * 
	 * @param prop
	 *            Property name
	 * @param defaultValue
	 *            default value
	 * @return Property value
	 */

	public short readShort(int packetID, Property prop) throws PropertyNotFoundException {
		return reader.readShort(packetID, prop.getCode());
	}

	public void setBoolean(int packetID, Property prop, boolean value) throws PropertyNotFoundException {
		reader.setBool(packetID, prop.getCode(), value);
	}

	public void setInt(int packetID, Property prop, int value) throws PropertyNotFoundException {
		reader.setInt(packetID, prop.getCode(), value);
	}

	public void setString(int packetID, Property prop, String value) throws PropertyNotFoundException {
		reader.setString(packetID, prop.getCode(), value);
	}

	public void setDouble(int packetID, Property prop, double value) throws PropertyNotFoundException {
		reader.setDouble(packetID, prop.getCode(), value);
	}

	public byte[] readByteArray(int packetID, Property prop) throws PropertyNotFoundException {
		return reader.readByteArray(packetID, prop.getCode());
	}

	public void setByteArray(int packetID, Property prop, byte[] value) throws PropertyNotFoundException {
		reader.setByteArray(packetID, prop.getCode(), value);
	}

	public void setShort(int packetID, Property prop, short value) throws PropertyNotFoundException {
		reader.setShort(packetID, prop.getCode(), value);
	}

	public int[] readShortArrayAsInt(int packetID, Property prop) throws PropertyNotFoundException {
		short array[] = readShortArray(packetID, prop);
		int ret[] = new int[array.length];
		for (int i = 0; i < array.length; i++) {
			ret[i] = array[i] & 0xFFFF;
		}
		return ret;
	}

	public short[] readShortArray(int packetID, Property prop) throws PropertyNotFoundException {
		return reader.readShortArray(packetID, prop.getCode());

	}

	public void setShortArrayAsInt(int packetID, Property prop, int[] value) throws PropertyNotFoundException {
		short array[] = new short[value.length];
		for (int i = 0; i < value.length; i++) {
			array[i] = (short) value[i];
		}
		reader.setShortArray(packetID, prop.getCode(), array);
	}

	public void setByte(int packetID, Property prop, byte value) throws PropertyNotFoundException {
		reader.setByte(packetID, prop.getCode(), value);
	}

	public String[] readStringArray(int packetID, Property prop) throws PropertyNotFoundException {
		return reader.readStringArray(packetID, prop.getCode());
	}

	public void setStringArray(int packetId, Property prop, String[] routeInfo) throws PropertyNotFoundException {
		reader.setStringArray(packetId, prop.getCode(), routeInfo);
	}

	@Override
	public int createPacket() {
		int pacletId = reader.createPacket();
		PacketMonitor.getInstanse().addPacket(pacletId);
		return pacletId;
	}

	@Override
	public void readDone(int packetID) {
		reader.readDone(packetID);
	}

	@Override
	public void sendPacket(int packetID) throws UnableToSendException, PropertyNotFoundException,
	        MalformedPacketException, UnableToSetUpInterfaceException {
		reader.sendPacket(packetID);
		PacketMonitor.getInstanse().removePacket(packetID);
	}

	@Override
	public int startRead() {
		return reader.startRead();
	}

	public void startCapture(int number, boolean isDebug) throws UnableToSetUpInterfaceException {
		reader.startCapture(number, isDebug);
	}

	/**
	 * @return the interfaceIndex
	 */
	public int getInterfaceIndex() {
		return interfaceIndex;
	}

	@Override
	public void stopCapture() {
		reader.stopCapture();
	}

	@Override
	public void setLogConfigPath(String string) {
		reader.setLogConfigPath(string);

	}
}