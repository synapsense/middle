package com.synapsense.bacnet.network;

import java.lang.Thread.State;
import java.util.Collection;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.log4j.Logger;

import com.synapsense.bacnet.network.pdu.PDUBasePacket;
import com.synapsense.bacnet.network.pdu.apdu.APDUBasePacket;
import com.synapsense.bacnet.network.pdu.apdu.sessions.SessionManager;
import com.synapsense.bacnet.network.pdu.npdu.NPDUBasePacket;
import com.synapsense.scheduler.SimpleTaskUnit;
import com.synapsense.scheduler.TaskScheduler;

public class NetworkProcessor implements SimpleTaskUnit {
	private static NetworkProcessor instance;
	LinkedBlockingQueue<PDUBasePacket> incomingQueue;
	Logger logger = Logger.getLogger(NetworkProcessor.class);
	private boolean started = false;

	static {
		instance = new NetworkProcessor();
	}

	public synchronized void start() {
		if (started == false) {
			logger.debug("Try to start NetworkProcessor...");
			started = true;
			TaskScheduler.getInstance().addSvcTask(instance);
		}
		logger.debug("NetworkProcessor was started.");
	}

	public synchronized void stop() {
		if (started) {
			started = false;
			synchronized (incomingQueue) {
				incomingQueue.notify();
			}
		}
	}

	/**
	 * Creates instance of NetworkProcessor
	 */
	private NetworkProcessor() {
		incomingQueue = new LinkedBlockingQueue<PDUBasePacket>();
		logger.debug("NetworkProcessor has been created successfully");
	}

	public static NetworkProcessor getInstance() {
		return instance;
	}

	public void addPacket(PDUBasePacket packet) {
		synchronized (incomingQueue) {
			incomingQueue.add(packet);
			incomingQueue.notify();
		}
		if (logger.isDebugEnabled())
			logger.debug("new packet[" + packet.toString() + "]added to incomingQueue");
	}

	public void addPacket(Collection<PDUBasePacket> packets) {
		synchronized (incomingQueue) {
			for (PDUBasePacket packet : packets) {
				incomingQueue.add(packet);
			}
			incomingQueue.notify();
		}
		if (logger.isDebugEnabled())
			logger.debug("new packets[" + packets.toString() + "]added to incomingQueue");
	}

	@Override
	public void Execute() throws Exception {
		PDUBasePacket message = null;
		while (Thread.currentThread().getState() != State.TERMINATED && started) {
			synchronized (incomingQueue) {
				if (incomingQueue.isEmpty()) {
					try {
						incomingQueue.wait();
					} catch (InterruptedException e) {

					}
					continue;
				}
				message = incomingQueue.poll();
			}
			try {
				if (!message.getHeader().Control) {
					processNetMessage((NPDUBasePacket) message);
					logger.debug("processing NPDU task");
				} else {
					SessionManager.getInstance().createSession((APDUBasePacket) message);
					logger.debug("processing APDU task");
				}
			} catch (Exception e) {
				logger.warn("Exception while processing " + message, e);
			}
		}
		logger.debug("NetworkProcessor was stopped.");
	}

	/**
	 * This function process all packets from network layer
	 * 
	 * @param message
	 */
	protected void processNetMessage(NPDUBasePacket message) {
		TaskScheduler.getInstance().addIncomingTask(message);
	}

	@Override
	public void failureAction(Throwable t) {
	}
}
