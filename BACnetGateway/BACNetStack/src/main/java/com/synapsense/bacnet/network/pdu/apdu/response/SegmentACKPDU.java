package com.synapsense.bacnet.network.pdu.apdu.response;

import com.synapsense.bacnet.network.jni.BACNetException;
import com.synapsense.bacnet.network.jni.Property;
import com.synapsense.bacnet.network.jni.PropertyNotFoundException;
import com.synapsense.bacnet.network.media.MPDUHeader;
import com.synapsense.bacnet.network.media.Media;
import com.synapsense.bacnet.network.pdu.apdu.APDUBasePacket;
import com.synapsense.bacnet.network.pdu.apdu.APDUMessageType;

/**
 * 
 * @author Alexander Borisov </br> The BACnet-SegmentACK-PDU is used to
 *         acknowledge the receipt of one or more PDUs containing portions of a
 *         segmented</br> message. It may also request the next segment or
 *         segments of the segmented message.</br> BACnet-SegmentACK-PDU ::=
 *         SEQUENCE {</br> pdu-type [0] Unsigned (0..15), -- 4 for this PDU
 *         type</br> reserved [1] Unsigned (0..3), -- must be set to zero</br>
 *         negative-ACK [2] BOOLEAN,</br> server [3] BOOLEAN,</br>
 *         original-invokeID [4] Unsigned (0..255),</br> sequence-number [5]
 *         Unsigned (0..255),</br> actual-window-size [6] Unsigned (1..127)</br>
 *         -- Context specific tags 0..6 are NOT used in header encoding</br>
 *         }</br>
 * 
 */
public class SegmentACKPDU extends APDUBasePacket {
	boolean negativeACK;// [2] BOOLEAN,
	boolean server;// [3] BOOLEAN,
	int sequenceNumber;// [5] Unsigned (0..255),
	int actualWindowSize;// [6] Unsigned (1..127)

	public SegmentACKPDU(MPDUHeader header) {
		this.header = header;
		type = APDUMessageType.segmentAck_PDU;
	}

	@Override
	public void receive() throws PropertyNotFoundException {
		Media reader = header.media;

		negativeACK = reader.readBoolean(header.packetId, Property.ACK_OUT_OF_ORDER);
		server = reader.readBoolean(header.packetId, Property.ACK_SERVER);
		sequenceNumber = reader.readByte(header.packetId, Property.SEQUENCE_NUMBER);
		actualWindowSize = reader.readByte(header.packetId, Property.WINDOW_SIZE);
		invokeID = reader.readByte(header.packetId, Property.INVOKE_ID) & 0xFF;
	}

	@Override
	public void send() throws BACNetException {
		Media reader = header.media;
		header.send();
		reader.setByte(header.packetId, Property.MESSAGE_TYPE, type.getType());
		reader.setBoolean(header.packetId, Property.ACK_OUT_OF_ORDER, negativeACK);
		reader.setBoolean(header.packetId, Property.ACK_SERVER, server);
		reader.setByte(header.packetId, Property.SEQUENCE_NUMBER, (byte) sequenceNumber);
		reader.setByte(header.packetId, Property.WINDOW_SIZE, (byte) actualWindowSize);
		reader.setByte(header.packetId, Property.INVOKE_ID, (byte) invokeID);

		reader.sendPacket(header.packetId);

	}

	@Override
	public boolean isSegmented() {
		return false;
	}

	public boolean isNegativeACK() {
		return negativeACK;
	}

	public void setNegativeACK(boolean negativeACK) {
		this.negativeACK = negativeACK;
	}

	public boolean isServer() {
		return server;
	}

	public void setServer(boolean server) {
		this.server = server;
	}

	public int getSequenceNumber() {
		return sequenceNumber;
	}

	public void setSequenceNumber(int sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}

	public int getActualWindowSize() {
		return actualWindowSize;
	}

	public void setActualWindowSize(int actualWindowSize) {
		this.actualWindowSize = actualWindowSize;
	}

}
