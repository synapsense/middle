package com.synapsense.bacnet.network.routing;

public class Route {
	private int ConnectedDNET;
	private int PortID;
	private String PortInfo;
	private byte[] DST_MAC;

	/**
	 * Creates instance of Route
	 * 
	 * @param data
	 *            routing record information
	 */
	public Route(String data, byte[] mac) {
		String s[] = data.split("_");
		switch (s.length) {
		case 2:
			ConnectedDNET = Integer.parseInt(s[0]);
			PortID = Integer.parseInt(s[1]);
			break;
		case 3:
			ConnectedDNET = Integer.parseInt(s[0]);
			PortID = Integer.parseInt(s[1]);
			PortInfo = new String(s[2]);
			break;
		default:
			throw new IllegalArgumentException("Unable to parse routing info");
		}
		DST_MAC = new byte[mac.length];
		System.arraycopy(mac, 0, DST_MAC, 0, mac.length);
	}

	public Route(int dnet, int portID, String portInfo, byte[] mac) {
		this.ConnectedDNET = dnet;
		this.PortID = portID;
		this.PortInfo = portInfo;
		DST_MAC = new byte[mac.length];
		System.arraycopy(mac, 0, DST_MAC, 0, mac.length);
	}

	/**
	 * Creates instance of Route
	 * 
	 * @param data
	 *            routing record information
	 * @return instance of route
	 */
	static public Route parse(String data, byte[] mac) {
		return new Route(data, mac);
	}

	/**
	 * Gets ConnectedDNET
	 * 
	 * @return ConnectedDNET value
	 */
	public int getDNET() {
		return ConnectedDNET;
	}

	/**
	 * Gets PortID
	 * 
	 * @return PortID
	 */
	public int getPort() {
		return PortID;
	}

	/**
	 * Gets PortInfo
	 * 
	 * @return PortInfo
	 */
	public String getPortInfo() {
		return PortInfo;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer(10);
		sb.append("Route:\n");
		sb.append("ConnectedDNET=");
		sb.append(ConnectedDNET);
		sb.append("\n");
		sb.append("PortID=");
		sb.append(PortID);
		sb.append("\n");
		sb.append("PortInfo=");
		sb.append(PortInfo);
		sb.append("\n");
		return sb.toString();

	}

	/**
	 * Creates route info for sending information about route to network
	 * 
	 * @return route information
	 */
	public String toRoute() {
		StringBuffer sb = new StringBuffer(10);
		sb.append(ConnectedDNET);
		sb.append('_');
		sb.append(PortID);
		sb.append('_');
		if (PortInfo != null) {
			sb.append(PortInfo);
		}
		return sb.toString();

	}

	public byte[] getDST_MAC() {
		return DST_MAC;
	}

	public void setDST_MAC(byte[] mac) {
		DST_MAC = new byte[mac.length];
		System.arraycopy(mac, 0, DST_MAC, 0, mac.length);
	}

}
