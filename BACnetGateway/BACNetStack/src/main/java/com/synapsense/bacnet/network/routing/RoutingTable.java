package com.synapsense.bacnet.network.routing;

import java.util.concurrent.ConcurrentHashMap;

/**
 * 
 * @author Alexander Borisov RoutingTable is responsible for storing and
 *         managing routing table
 */
public class RoutingTable {

	private static RoutingTable instance;
	ConcurrentHashMap<Integer, Route> routingTable;

	static {
		instance = new RoutingTable();
	}

	private RoutingTable() {
		routingTable = new ConcurrentHashMap<Integer, Route>();
	}

	/**
	 * Adds route to table
	 * 
	 * @param r
	 *            route
	 */
	public void addRoute(Route r) {
		routingTable.put(r.getDNET(), r);
	}

	/**
	 * Removes route from table
	 * 
	 * @param r
	 *            route
	 */
	public void removeRoute(Route r) {
		routingTable.remove(r.getDNET());
	}

	public void removeRoute(int DNET) {
		routingTable.remove(DNET);
	}

	/**
	 * Update route in table. If route is not exist if will be automatically
	 * added to table
	 * 
	 * @param r
	 *            route
	 */
	public void updateRoute(Route r) {
		if (r.getPort() == 0) {
			removeRoute(r);
		} else if (routingTable.replace(r.getDNET(), r) == null)
			addRoute(r);
	}

	/**
	 * Gets routing table instance
	 * 
	 * @return routing table instance
	 */
	public static RoutingTable getInstance() {
		return instance;
	}

	/**
	 * Clears routing table
	 */
	public void clear() {
		routingTable.clear();
	}

	/**
	 * Gets route to destination
	 * 
	 * @param destination
	 *            network number
	 * @return route
	 */
	public Route getRoute(int destination) {
		return routingTable.get(destination);
	}

	public String[] getTable() {
		String s[] = new String[routingTable.size()];
		int i = 0;
		for (Route r : routingTable.values()) {
			s[i++] = r.toRoute();
		}
		return s;
	}

	public int[] getDNETs() {
		int s[] = new int[routingTable.size()];
		int i = 0;
		for (Route r : routingTable.values()) {
			s[i++] = r.getDNET();
		}
		return s;
	}

	public boolean queryRoute(int dnet) {
		return routingTable.containsKey(dnet);
	}

}
