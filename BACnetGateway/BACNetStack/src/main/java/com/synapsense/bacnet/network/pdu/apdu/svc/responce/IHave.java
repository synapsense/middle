/**
 * 
 */
package com.synapsense.bacnet.network.pdu.apdu.svc.responce;

import java.io.IOException;

import com.synapsense.bacnet.network.media.MPDUHeader;
import com.synapsense.bacnet.network.pdu.apdu.APDUBasePacket;
import com.synapsense.bacnet.network.pdu.apdu.codecs.PrimitivesDecoder;
import com.synapsense.bacnet.network.pdu.apdu.codecs.PrimitivesEncoder;
import com.synapsense.bacnet.network.pdu.apdu.datatypes.UnconfirmedServiceChoice;
import com.synapsense.bacnet.network.pdu.apdu.request.UnconfirmedRequestPDU;
import com.synapsense.bacnet.network.pdu.apdu.svc.SvcBase;
import com.synapsense.bacnet.network.pdu.apdu.svc.SvcException;
import com.synapsense.bacnet.network.routing.RouteNotFoundException;
import com.synapsense.bacnet.network.routing.RoutingService;
import com.synapsense.bacnet.system.shared.BACnetObjectIdentifier;

/**
 * @author Alexander Borisov
 *
 */

/**
 * <b>I-Have-Request</b> ::= SEQUENCE { </br> deviceIdentifier
 * BACnetObjectIdentifier, </br> objectIdentifier BACnetObjectIdentifier, </br>
 * objectName CharacterString </br> } </br>
 */
public class IHave extends SvcBase {
	public IHave(MPDUHeader header) {
		super(header);
		// TODO Auto-generated constructor stub
	}

	public IHave(SvcBase svc) {
		super(svc);
		// TODO Auto-generated constructor stub
	}

	public IHave(APDUBasePacket packet) {
		super(packet);
		// TODO Auto-generated constructor stub
	}

	protected BACnetObjectIdentifier deviceIdentifier; // TODO Add type of
													   // objectIdentifier
	protected BACnetObjectIdentifier objectIdentifier; // TODO Add type of
													   // objectIdentifier
	String objectName;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.synapsense.bacnet.network.pdu.apdu.svc.SvcBase#decodeBuffer(byte[])
	 */
	@Override
	public void decodeBuffer(byte[] buffer) {
		PrimitivesDecoder pd = new PrimitivesDecoder(buffer);
		try {
			deviceIdentifier = pd.decodeObjectID(null);
			objectIdentifier = pd.decodeObjectID(null);
			objectName = pd.decodeCharacterString(null);
		} catch (IOException e) {
			logger.warn("Unable to decode packet buffer", e);
			throw new SvcException("decoded buffer contains illegal data", e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsense.bacnet.network.pdu.apdu.svc.SvcBase#encodeBuffer()
	 */
	@Override
	public byte[] encodeBuffer() {
		PrimitivesEncoder pe = new PrimitivesEncoder();
		try {
			pe.encodeObjectID(null, deviceIdentifier);
			pe.encodeObjectID(null, objectIdentifier);
			pe.encodeCharacterString(null, objectName);
		} catch (IOException e) {
			logger.warn("Unable to encode buffer", e);
			throw new SvcException("Cannot encode service", e);
		}
		return pe.getEncodedBuffer();
	}

	@Override
	public APDUBasePacket getPacket() {
		UnconfirmedRequestPDU packet;
		if (header == null)
			throw new SvcException("Unable to create APDU packet");

		if (header.DST_MAC == null) {
			try {
				header.DST_MAC = MPDUHeader.getCopy(RoutingService.discoverRoute(header.DNET).getDST_MAC());
			} catch (RouteNotFoundException e) {
				throw new SvcException(e.getLocalizedMessage());
			}
		}
		header.Control = true;
		packet = new UnconfirmedRequestPDU(header);
		if (encodedBuffer == null) {
			encodedBuffer = encodeBuffer();
		}
		packet.setBuffer(encodedBuffer);
		packet.setChoise(UnconfirmedServiceChoice.i_Have);
		return packet;
	}

	/**
	 * @return the deviceIdentifier
	 */
	public BACnetObjectIdentifier getDeviceIdentifier() {
		return deviceIdentifier;
	}

	/**
	 * @param deviceIdentifier
	 *            the deviceIdentifier to set
	 */
	public void setDeviceIdentifier(BACnetObjectIdentifier deviceIdentifier) {
		this.deviceIdentifier = deviceIdentifier;
	}

	/**
	 * @return the objectIdentifier
	 */
	public BACnetObjectIdentifier getObjectIdentifier() {
		return objectIdentifier;
	}

	/**
	 * @param objectIdentifier
	 *            the objectIdentifier to set
	 */
	public void setObjectIdentifier(BACnetObjectIdentifier objectIdentifier) {
		this.objectIdentifier = objectIdentifier;
	}

	/**
	 * @return the objectName
	 */
	public String getObjectName() {
		return objectName;
	}

	/**
	 * @param objectName
	 *            the objectName to set
	 */
	public void setObjectName(String objectName) {
		this.objectName = objectName;
	}
}
