package com.synapsense.bacnet.network.media;

import java.util.Collection;
import java.util.HashMap;

public class MediaManager {
	private HashMap<byte[], Media> mediaDev;
	// private HashMap <String, MediaNames> mediaMapper;
	static private MediaManager manager;

	static {
		manager = new MediaManager();
	}

	private MediaManager() {
		mediaDev = new HashMap<byte[], Media>();
		// mediaMapper = new HashMap<String, MediaNames> ();
	}

	public static MediaManager getMediaManager() {
		return manager;
	}

	public void registerMedia(byte[] mac, Media media) {
		mediaDev.put(mac, media);
	}

	// public void registerMediaMapper (String src,MediaNames dst){
	// mediaMapper.put(src, dst);
	// }

	// public Media getMediaByName(String name){
	// return mediaDev.get(name);
	// }

	// public Media getDstMediaBySrc (Media name){
	// MediaNames dst = mediaMapper.get(name.getActiveEthMac());
	// if (dst != null)
	// return mediaDev.get(dst);
	// return null;
	// }

	public Collection<Media> getAllMedia() {
		return mediaDev.values();
	}
}
