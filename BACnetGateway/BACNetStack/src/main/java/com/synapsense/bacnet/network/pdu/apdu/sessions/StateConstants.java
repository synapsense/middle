package com.synapsense.bacnet.network.pdu.apdu.sessions;

public final class StateConstants {
	final static public int IDLE = 100;
	final static public int UnconfirmedReceived = 101;
	final static public int ConfirmedBroadcastReceived = 102;
	final static public int ConfirmedUnsegmentedReceived = 103;
	final static public int ConfirmedSegmentedReceivedNotSupported = 104;
	final static public int ConfirmedSegmentedReceived = 105;
	final static public int AbortPDU_Received = 106;
	final static public int UnexpectedPDU_Received = 107;

	final static public int SEGMENTED_REQUEST = 200;
	final static public int NewSegmentReceived = 201;
	final static public int LastSegmentOfGroupReceived = 202;
	final static public int LastSegmentOfMessageReceived = 203;
	final static public int SegmentReceivedOutOfOrder = 204;
	// AbortPDU_Received,
	// UnexpectedPDU_Received,
	final static public int Timeout = 205;

	final static public int AWAIT_RESPONSE = 300;
	final static public int SendSimpleACK = 301;
	final static public int SendUnsegmentedComplexACK = 302;
	final static public int CannotSendSegmentedComplexACK = 303;
	final static public int SendSegmentedComplexACK = 304;
	final static public int SendErrorPDU = 305;
	final static public int SendAbort = 306;
	final static public int SendReject = 307;
	// AbortPDU_Received,
	final static public int DuplicateRequestReceived = 308;
	final static public int DuplicateSegmentReceived = 309;
	// UnexpectedPDU_Received = 300;
	// Timeout

	final static public int SEGMENTED_RESPONSE = 400;
	final static public int DuplicateACK_Received = 401;
	final static public int NewACK_Received = 402;
	final static public int FinalACK_Received = 403;
	// Timeout,
	final static public int FinalTimeout = 404;
	// AbortPDU_Received,
	// UnexpectedPDU_Received,
	// SendAbort,

}
