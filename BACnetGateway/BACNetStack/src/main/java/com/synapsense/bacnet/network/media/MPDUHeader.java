package com.synapsense.bacnet.network.media;

import java.util.Arrays;

import org.apache.log4j.Logger;

import com.synapsense.bacnet.network.Constants;
import com.synapsense.bacnet.network.jni.Property;
import com.synapsense.bacnet.network.jni.PropertyNotFoundException;
import com.synapsense.bacnet.network.pdu.PDUBasePacket;
import com.synapsense.bacnet.network.pdu.apdu.APDUBasePacket;
import com.synapsense.bacnet.network.pdu.npdu.NPDUBasePacket;

public class MPDUHeader implements Cloneable {
	/**
	 * Media header
	 */
	public byte[] SRC_MAC = null;
	public byte[] DST_MAC = null;

	public int DNET = Constants.PROPERTY_NOT_SPECIFIED;
	public int DLEN = Constants.PROPERTY_NOT_SPECIFIED;
	public byte[] DADR = null;

	public Byte BVLC = null; // BVLC for BACnet/IP

	public int SNET = Constants.PROPERTY_NOT_SPECIFIED;
	public int SLEN = Constants.PROPERTY_NOT_SPECIFIED;
	public byte[] SADR = null;

	public int HopCount = Constants.PROPERTY_NOT_SPECIFIED;

	public int VendorID;
	public int priority = 0x00; // Normal message
	public int packetId;
	public int Version = 1;
	public boolean Control;
	public boolean der;
	public Media media;
	static protected Logger logger = Logger.getLogger(MPDUHeader.class);
	private static final byte BVLC_UNICAST = 0x0A;
	private static final byte BVLC_BROADCAST = 0x0B;

	public static byte[] getCopy(byte[] mac) {
		byte[] result = null;
		if (mac != null) {
			result = new byte[mac.length];
			System.arraycopy(mac, 0, result, 0, mac.length);
		}
		return result;
	}

	public static boolean isEquals(byte[] mass1, byte[] mass2) {
		boolean result = false;
		if (mass1 == null && mass2 == null) {
			result = true;
		} else if (mass1 != null && mass2 != null) {
			result = Arrays.equals(mass1, mass2);
		}
		return result;
	}

	public MPDUHeader(MPDUHeader header) {
		SRC_MAC = getCopy(header.SRC_MAC);
		DST_MAC = getCopy(header.DST_MAC);

		DNET = header.DNET;
		DLEN = header.DLEN;
		DADR = getCopy(header.DADR);

		SNET = header.SNET;
		SLEN = header.SLEN;
		SADR = getCopy(header.SADR);

		HopCount = header.HopCount;

		VendorID = header.VendorID;
		priority = header.priority;
		packetId = header.packetId;
		Version = header.Version;
		Control = header.Control;
		der = header.der;
		media = header.media;

		BVLC = header.BVLC;
	}

	public MPDUHeader() {

	}

	public void send() throws PropertyNotFoundException {
		Media reader = media;
		packetId = reader.createPacket();
		media.writeDST(packetId, DST_MAC);
		media.writeSRC(packetId, SRC_MAC);

		// Set BVLC for BACnet/IP
		if (BVLC == null) {
			BVLC = media.isBroadcast(DST_MAC) ? BVLC_BROADCAST : BVLC_UNICAST;
		}
		reader.setByte(packetId, Property.BVLC, BVLC);

		reader.setBoolean(packetId, Property.IS_APDU, Control);

		// Set destination fields
		if (DNET != Constants.PROPERTY_NOT_SPECIFIED || DLEN != Constants.PROPERTY_NOT_SPECIFIED
		        || HopCount != Constants.PROPERTY_NOT_SPECIFIED) {

			if (DNET == Constants.PROPERTY_NOT_SPECIFIED)
				DNET = 0xFFFF;
			if (HopCount == Constants.PROPERTY_NOT_SPECIFIED)
				HopCount = 255;
			if (DLEN == Constants.PROPERTY_NOT_SPECIFIED)
				DLEN = 0;

			reader.setShort(packetId, Property.DNET, (short) DNET);
			reader.setByte(packetId, Property.HOPS, (byte) HopCount);
			reader.setByte(packetId, Property.DLEN, (byte) DLEN);

			if (DLEN > 0) {
				reader.setByteArray(packetId, Property.DADR, DADR);
			}
		}

		// Set source fields
		if (SNET != Constants.PROPERTY_NOT_SPECIFIED && (SLEN != Constants.PROPERTY_NOT_SPECIFIED || SLEN != 0)
		        && SADR != null) {

			reader.setShort(packetId, Property.SNET, (short) SNET);
			reader.setByte(packetId, Property.SLEN, (byte) SLEN);
			reader.setByteArray(packetId, Property.SADR, SADR);
		}

		reader.setByte(packetId, Property.PRIORITY, (byte) priority);
		reader.setBoolean(packetId, Property.DATA_EXPECTING_REPLY, der);

		logger.debug("Header has been written");
	}

	public PDUBasePacket receive() throws PropertyNotFoundException {
		Media reader = media;
		logger.debug("Reading data from network");
		packetId = reader.startRead();

		der = reader.readBoolean(packetId, Property.DATA_EXPECTING_REPLY);

		SRC_MAC = media.readSRC(packetId);
		DST_MAC = media.readDST(packetId);

		try {
			BVLC = (byte) reader.readByte(packetId, Property.BVLC);
		} catch (PropertyNotFoundException e) {
			logger.debug("Property [BVLC] not specified. Annex J.");
			BVLC = null;
		}

		Control = reader.readBoolean(packetId, Property.IS_APDU);
		logger.debug("[control] = " + Control);
		try {
			DLEN = reader.readByte(packetId, Property.DLEN) & 0xFF;
		} catch (PropertyNotFoundException e) {
			logger.debug("Property [DLEN] not specified");
			DLEN = 0;
		}
		if (DLEN != 0) {
			if (DLEN != 6) {
				// throw exception; data corruption occurs
			}
			DADR = reader.readByteArray(packetId, Property.DADR);
		}

		try {
			SLEN = reader.readByte(packetId, Property.SLEN) & 0xFF;
		} catch (PropertyNotFoundException e) {
			logger.debug("Property [SLEN] not specified");
			SLEN = 0;
		}
		if (SLEN != 0) {
			if (SLEN != 6) {
				// throw exception; data corruption occurs
			}
			SADR = reader.readByteArray(packetId, Property.SADR);
		}

		try {
			DNET = reader.readShort(packetId, Property.DNET) & 0xFFFF;
		} catch (PropertyNotFoundException e) {
			DNET = Constants.PROPERTY_NOT_SPECIFIED;
			logger.debug("property [DNET] not specifyed");
		}
		try {
			SNET = reader.readShort(packetId, Property.SNET) & 0xFFFF;
		} catch (PropertyNotFoundException e) {
			SNET = Constants.PROPERTY_NOT_SPECIFIED;
			logger.debug("property [SNET] not specifyed");
		}
		try {
			HopCount = reader.readByte(packetId, Property.HOPS) & 0xFF;
		} catch (PropertyNotFoundException e) {
			HopCount = Constants.PROPERTY_NOT_SPECIFIED;
			logger.debug("property [HopCount] not specifyed");
		}
		try {
			priority = reader.readByte(packetId, Property.PRIORITY) & 0xFF;
		} catch (PropertyNotFoundException e) {
			priority = Constants.PROPERTY_NOT_SPECIFIED;
			logger.debug("property [priority] not specifyed");
		}
		PDUBasePacket packet = null;
		if (!Control) {
			packet = NPDUBasePacket.getPacket(this);

		} else {
			packet = APDUBasePacket.getPacket(this);
		}
		if (logger.isDebugEnabled()) {
			logger.debug("new packet with ID [" + packetId + "]has been recived from network");
		}
		try {
			packet.receive();
			return packet;
		} finally {
			reader.readDone(packetId);
		}
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer(100);
		sb.append("\n-----");
		sb.append("\nPacket ID=");
		sb.append(packetId);
		sb.append("\n-----");
		sb.append(this.getClass().getName());
		sb.append("-----\n");
		sb.append("Ethernet content\n");
		sb.append("\tSRC_MAC=");
		sb.append(toHex(SRC_MAC));
		sb.append("\n");
		sb.append("\tDST_MAC=");
		sb.append(toHex(DST_MAC));
		sb.append("\n");
		sb.append("-----\n");
		sb.append("\tDER=");
		sb.append(der);
		sb.append("\n");

		sb.append("\tDNET=");
		sb.append(DNET);
		sb.append("\n");
		sb.append("\tDADR=");
		sb.append(toHex(DADR));
		sb.append("\n");

		sb.append("\tSNET=");
		sb.append(SNET);
		sb.append("\n");
		sb.append("\tSADR=");
		sb.append(toHex(SADR));
		sb.append("\n");

		sb.append("\tHopCount=");
		sb.append(HopCount);
		sb.append("\n");

		sb.append("\tVendorID=");

		sb.append(VendorID);
		sb.append("\n");

		sb.append("\tControl=");

		sb.append(Control);
		sb.append("\n");

		sb.append("---------------\n");
		return sb.toString();
	}

	public static String toHex(byte[] b) {
		StringBuilder result = new StringBuilder();
		if (b != null) {
			for (int i = 0; i < b.length; ++i) {
				if (i == 0)
					result.append(":");
				result.append((Integer.toHexString((b[i] & 0xFF) | 0x100).substring(1, 3).toUpperCase()));
			}
		}
		return result.toString();
	}

	protected MPDUHeader clone() {
		MPDUHeader header = new MPDUHeader();
		header.SRC_MAC = getCopy(SRC_MAC);
		header.DST_MAC = getCopy(DST_MAC);

		header.DNET = DNET;
		header.DLEN = DLEN;
		header.DADR = getCopy(DADR);

		header.SNET = SNET;
		header.SLEN = SLEN;
		header.SADR = getCopy(SADR);

		header.HopCount = HopCount;

		header.VendorID = VendorID;
		header.priority = priority;
		header.packetId = packetId;
		header.Version = Version;
		header.Control = Control;
		header.der = der;
		header.media = media;
		header.BVLC = BVLC;
		return header;
	}
}
