/**
 * 
 */
package com.synapsense.bacnet.network.jni;

/**
 * @author Alexander Borisov
 * 
 */
public class PropertyNotFoundException extends BACNetException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5291879680433708738L;

	public PropertyNotFoundException(Property key) {
		super(key.name());
	}

}
