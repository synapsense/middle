/**
 * 
 */
package com.synapsense.bacnet.network.pdu.apdu.svc.responce;

import java.io.IOException;
import java.util.ArrayList;

import com.synapsense.bacnet.network.media.MPDUHeader;
import com.synapsense.bacnet.network.pdu.apdu.APDUBasePacket;
import com.synapsense.bacnet.network.pdu.apdu.codecs.PrimitivesDecoder;
import com.synapsense.bacnet.network.pdu.apdu.codecs.PrimitivesEncoder;
import com.synapsense.bacnet.network.pdu.apdu.datatypes.ConfirmedServiceACK;
import com.synapsense.bacnet.network.pdu.apdu.response.ComplexACKPDU;
import com.synapsense.bacnet.network.pdu.apdu.svc.SvcBase;
import com.synapsense.bacnet.network.pdu.apdu.svc.SvcException;
import com.synapsense.bacnet.network.routing.RouteNotFoundException;
import com.synapsense.bacnet.network.routing.RoutingService;
import com.synapsense.bacnet.system.shared.BACnetResult;
import com.synapsense.bacnet.system.shared.ReadAccessResult;

/**
 * @author Alexander Kravin
 *
 */

/**
 * ReadPropertyMultiple-ACK ::= SEQUENCE { listOfReadAccessResults SEQUENCE OF
 * ReadAccessResult }
 * 
 * 
 * ReadAccessResult ::= SEQUENCE { objectIdentifier [0] BACnetObjectIdentifier,
 * listOfResults [1] SEQUENCE OF SEQUENCE { propertyIdentifier [2]
 * BACnetPropertyIdentifier, propertyArrayIndex [3] Unsigned OPTIONAL, -- used
 * only with array datatype -- if omitted with an array the entire -- array is
 * referenced readResult CHOICE { propertyValue [4] ABSTRACT-SYNTAX.&Type,
 * propertyAccessError [5] Error } } OPTIONAL }
 * 
 */

public class ReadPropertyMultipleAck extends SvcBase {

	private ArrayList<ReadAccessResult> listOfReadAccessResults = new ArrayList<ReadAccessResult>();

	public ReadPropertyMultipleAck(MPDUHeader header) {
		super(header);
	}

	public ReadPropertyMultipleAck(APDUBasePacket packet) {
		super(packet);
	}

	public ReadPropertyMultipleAck(SvcBase svc) {
		super(svc);
	}

	@Override
	public void decodeBuffer(byte[] buffer) {
		PrimitivesDecoder pd = new PrimitivesDecoder(buffer);
		try {
			while (pd.askForTag() != null) {
				ReadAccessResult result = new ReadAccessResult();
				result.setObjectIdentifier(pd.decodeObjectID(0));
				if (pd.askForTag() != null) {
					pd.decodeSequenceStart(1);
					while (pd.askForSequenceStop(1)) {
						BACnetResult param = new BACnetResult();
						param.propertyReference.propertyIdentifier = pd.decodeEnum(2);
						if (pd.askForTag().getID() == 3) {
							param.propertyReference.propertyArrayIndex = pd.decodeUInt(3);
						}
						if (pd.askForTag().getID() == 4) {
							String val = pd.decodeAny();
							param.propertyValue = (val == null ? null : val.getBytes());
						} else if (pd.askForTag().getID() == 5) {
							pd.decodeSequenceStart(5);
							param.propertyAccessErrorClass = pd.decodeEnum(9);
							param.propertyAccessErrorCode = pd.decodeEnum(9);
							pd.decodeSequenceStop(5);
						}
					}
					pd.decodeSequenceStop(1);
				}
				listOfReadAccessResults.add(result);
			}
		} catch (Exception e) {
			logger.warn("Unable to decode packet buffer", e);
			throw new SvcException("decoded buffer contains illegal data", e);
		}
	}

	@Override
	public byte[] encodeBuffer() {
		PrimitivesEncoder encoder = new PrimitivesEncoder();
		try {
			for (ReadAccessResult result : listOfReadAccessResults) {
				encoder.encodeObjectID(0, result.getObjectIdentifier());
				encoder.encodeSequenceStart(1);
				for (BACnetResult prop : result.getListOfResults()) {
					encoder.encodeEnum(2, prop.propertyReference.propertyIdentifier);
					if (prop.propertyReference.propertyArrayIndex != null)
						encoder.encodeUInt(3, prop.propertyReference.propertyArrayIndex);

					if (prop.propertyValue != null) {
						encoder.encodeSequenceStart(4);
						encoder.appendEncodedBuffer(prop.propertyValue);
						encoder.encodeSequenceStop(4);
					} else {
						encoder.encodeSequenceStart(5);
						encoder.encodeEnum(null, prop.propertyAccessErrorClass);
						encoder.encodeEnum(null, prop.propertyAccessErrorCode);
						encoder.encodeSequenceStop(5);
					}
				}
				encoder.encodeSequenceStop(1);
			}
		} catch (IOException e) {
			logger.warn("Unable to encode buffer", e);
			throw new SvcException("Cannot encode service", e);
		}
		return encoder.getEncodedBuffer();
	}

	@Override
	public APDUBasePacket getPacket() {
		if (header == null)
			throw new SvcException("Unable to create APDU packet");
		if (header.DST_MAC == null) {
			try {
				header.DST_MAC = MPDUHeader.getCopy(RoutingService.discoverRoute(header.DNET).getDST_MAC());
			} catch (RouteNotFoundException e) {
				throw new SvcException(e.getLocalizedMessage());
			}
		}

		ComplexACKPDU pdu = new ComplexACKPDU(header);
		pdu.setMore_follows(more_follows);
		pdu.setProposed_window_size(proposed_window_size);
		pdu.setSequence_number(sequence_number);
		pdu.setSegmented_message(segmented_message);
		pdu.setId(invokeID);
		pdu.setService_ACK_choice(ConfirmedServiceACK.readPropertyMultiple);
		pdu.setBuffer(encodeBuffer());
		return pdu;

	}

	/**
	 * @return the listOfReadAccessResults
	 */
	public ArrayList<ReadAccessResult> getListOfReadAccessResults() {
		return listOfReadAccessResults;
	}

	/**
	 * @param listOfReadAccessResults
	 *            the listOfReadAccessResults to set
	 */
	public void addReadAccessResults(ReadAccessResult readAccessResult) {
		listOfReadAccessResults.add(readAccessResult);
	}
}
