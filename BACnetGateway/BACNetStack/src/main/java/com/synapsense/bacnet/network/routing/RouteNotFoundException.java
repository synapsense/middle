package com.synapsense.bacnet.network.routing;

public class RouteNotFoundException extends Exception {

	public RouteNotFoundException(String s) {
		super(s);
	}

	public RouteNotFoundException(int i) {
		super("No route to DNET [" + i + "]");
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 5166514677227434900L;

}
