package com.synapsense.bacnet.network.pdu.apdu;

public enum APDUMessageType {
confirmed_request_PDU((byte) 0), // BACnet-Confirmed-Request-PDU,
unconfirmed_request_PDU((byte) 1), // BACnet-Unconfirmed-Request-PDU,
simpleACK_PDU((byte) 2), // BACnet-SimpleACK-PDU,
complexACK_PDU((byte) 3), // BACnet-ComplexACK-PDU,
segmentAck_PDU((byte) 4), // BACnet-SegmentACK-PDU,
error_PDU((byte) 5), // BACnet-Error-PDU,
reject_PDU((byte) 6), // BACnet-Reject-PDU,
abort_PDU((byte) 7);// BACnet-Abort-PDU

private final byte type;

APDUMessageType(byte type) {
	this.type = type;
}

public byte getType() {
	return type;
}

public static APDUMessageType valueOf(byte b) {
	switch (b) {
	case 0:
		return confirmed_request_PDU;
	case 1:
		return unconfirmed_request_PDU;
	case 2:
		return simpleACK_PDU;
	case 3:
		return complexACK_PDU;
	case 4:
		return segmentAck_PDU;
	case 5:
		return error_PDU;
	case 6:
		return reject_PDU;
	case 7:
		return abort_PDU;
	default:
		throw new IllegalArgumentException("Unable to identify enum type");
	}
}
}
