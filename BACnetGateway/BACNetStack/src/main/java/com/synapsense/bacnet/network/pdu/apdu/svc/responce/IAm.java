/**
 * 
 */
package com.synapsense.bacnet.network.pdu.apdu.svc.responce;

import java.io.IOException;

import com.synapsense.bacnet.enums.BACnetAPDUSize;
import com.synapsense.bacnet.network.media.MPDUHeader;
import com.synapsense.bacnet.network.pdu.apdu.APDUBasePacket;
import com.synapsense.bacnet.network.pdu.apdu.codecs.PrimitivesDecoder;
import com.synapsense.bacnet.network.pdu.apdu.codecs.PrimitivesEncoder;
import com.synapsense.bacnet.network.pdu.apdu.datatypes.UnconfirmedServiceChoice;
import com.synapsense.bacnet.network.pdu.apdu.request.UnconfirmedRequestPDU;
import com.synapsense.bacnet.network.pdu.apdu.svc.SvcBase;
import com.synapsense.bacnet.network.pdu.apdu.svc.SvcException;
import com.synapsense.bacnet.network.routing.RouteNotFoundException;
import com.synapsense.bacnet.network.routing.RoutingService;
import com.synapsense.bacnet.system.shared.BACnetObjectIdentifier;

/**
 * @author Alexander Borisov
 * 
 */

/**
 * </br> <b>I-Am-Request</b> ::= SEQUENCE {</br> iAmDeviceIdentifier
 * BACnetObjectIdentifier,</br> maxAPDULengthAccepted Unsigned,</br>
 * segmentationSupported BACnetSegmentation,</br> vendorID Unsigned</br> }</br>
 */
public class IAm extends SvcBase {

	public IAm(MPDUHeader header) {
		super(header);
		// TODO Auto-generated constructor stub
	}

	public IAm(SvcBase svc) {
		super(svc);
		// TODO Auto-generated constructor stub
	}

	public IAm(APDUBasePacket packet) {
		super(packet);
		// TODO Auto-generated constructor stub
	}

	protected BACnetObjectIdentifier iAmDeviceIdentifier;
	// protected int maxAPDULengthAccepted;
	protected long segmentationSupported;
	protected long vendorID;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.synapsense.bacnet.network.pdu.apdu.svc.SvcBase#decodeBuffer(byte[])
	 */
	@Override
	public void decodeBuffer(byte[] buffer) {
		PrimitivesDecoder pd = new PrimitivesDecoder(buffer);
		try {
			iAmDeviceIdentifier = pd.decodeObjectID(null);
			max_APDU_length_accepted = BACnetAPDUSize.valueOf((int) pd.decodeUInt(null));
			segmentationSupported = pd.decodeEnum(null);
			vendorID = (int) pd.decodeUInt(null);
		} catch (IOException e) {
			logger.warn("Unable to decode packet buffer", e);
			throw new SvcException("decoded buffer contains illegal data", e);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsense.bacnet.network.pdu.apdu.svc.SvcBase#encodeBuffer()
	 */
	@Override
	public byte[] encodeBuffer() {
		byte b[] = null;
		try {
			PrimitivesEncoder pe = new PrimitivesEncoder();
			pe.encodeObjectID(null, iAmDeviceIdentifier);
			pe.encodeUInt(null, max_APDU_length_accepted.getIntValue());
			pe.encodeEnum(null, segmentationSupported);
			pe.encodeUInt(null, vendorID);
			b = pe.getEncodedBuffer();
		} catch (IOException e) {
			throw new SvcException("Cannot encode service", e);
		}
		return b;
	}

	@Override
	public APDUBasePacket getPacket() {
		if (header == null)
			throw new SvcException("Unable to create APDU packet");

		if (header.DST_MAC == null) {
			try {
				header.DST_MAC = MPDUHeader.getCopy(RoutingService.discoverRoute(header.DNET).getDST_MAC());
			} catch (RouteNotFoundException e) {
				throw new SvcException(e.getLocalizedMessage());
			}
		}
		UnconfirmedRequestPDU packet = new UnconfirmedRequestPDU(header);
		packet.setChoise(UnconfirmedServiceChoice.i_Am);

		if (encodedBuffer == null) {
			encodedBuffer = encodeBuffer();
		}
		packet.setBuffer(encodedBuffer);
		return packet;
	}

	/**
	 * @return the iAmDeviceIdentifier
	 */
	public BACnetObjectIdentifier getIAmDeviceIdentifier() {
		return iAmDeviceIdentifier;
	}

	/**
	 * @param amDeviceIdentifier
	 *            the iAmDeviceIdentifier to set
	 */
	public void setIAmDeviceIdentifier(BACnetObjectIdentifier amDeviceIdentifier) {
		iAmDeviceIdentifier = amDeviceIdentifier;
	}

	/**
	 * @return the vendorID
	 */
	public long getVendorID() {
		return vendorID;
	}

	/**
	 * @param vendorID
	 *            the vendorID to set
	 */
	public void setVendorID(long vendorID) {
		this.vendorID = vendorID;
	}

	/**
	 * @return the segmentationSupported
	 */
	public long getSegmentationSupported() {
		return segmentationSupported;
	}

	/**
	 * @param segmentationSupported
	 *            the segmentationSupported to set
	 */
	public void setSegmentationSupported(int segmentationSupported) {
		this.segmentationSupported = segmentationSupported;
	}
}
