package com.synapsense.bacnet.network.pdu;

import org.apache.log4j.Logger;

import com.synapsense.bacnet.network.jni.BACNetException;
import com.synapsense.bacnet.network.jni.PropertyNotFoundException;
import com.synapsense.bacnet.network.jni.UnableToSendException;
import com.synapsense.bacnet.network.media.MPDUHeader;

public abstract class PDUBasePacket {
	protected MPDUHeader header;
	protected Logger logger = Logger.getLogger(this.getClass());

	/**
	 * Gets message header
	 * 
	 * @return message header
	 */
	public MPDUHeader getHeader() {
		return header;
	}

	/**
	 * This method is used for send message to network
	 * 
	 * @throws PropertyNotFoundException
	 *             if unable to set property of message
	 * @throws UnableToSendException
	 *             if unable to send message
	 * @throws BACNetException
	 */
	public abstract void send() throws BACNetException;

	/**
	 * This method is used for receive message from network
	 * 
	 * @throws PropertyNotFoundException
	 *             if unable to read property of message
	 */
	public abstract void receive() throws PropertyNotFoundException;

}
