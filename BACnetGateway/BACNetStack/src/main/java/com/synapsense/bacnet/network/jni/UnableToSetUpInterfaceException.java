package com.synapsense.bacnet.network.jni;

public class UnableToSetUpInterfaceException extends BACNetException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -498404265077580561L;

	public UnableToSetUpInterfaceException(String s) {
		super(s);
	}
}
