package com.synapsense.bacnet.network.pdu.apdu.svc.responce;

import java.io.IOException;

import com.synapsense.bacnet.enums.ErrorClass;
import com.synapsense.bacnet.enums.ErrorCode;
import com.synapsense.bacnet.network.media.MPDUHeader;
import com.synapsense.bacnet.network.pdu.apdu.APDUBasePacket;
import com.synapsense.bacnet.network.pdu.apdu.codecs.PrimitivesDecoder;
import com.synapsense.bacnet.network.pdu.apdu.codecs.PrimitivesEncoder;
import com.synapsense.bacnet.network.pdu.apdu.response.ErrorPDU;
import com.synapsense.bacnet.network.pdu.apdu.svc.SvcBase;
import com.synapsense.bacnet.network.pdu.apdu.svc.SvcException;
import com.synapsense.bacnet.network.routing.RouteNotFoundException;
import com.synapsense.bacnet.network.routing.RoutingService;
import com.synapsense.bacnet.system.BACnetError;

public class ErrorSvc extends SvcBase {
	private BACnetError error;

	public ErrorSvc(MPDUHeader header) {
		super(header);
		// TODO Auto-generated constructor stub
	}

	public ErrorSvc(SvcBase svc) {
		super(svc);
	}

	public ErrorSvc(BACnetError e1, SvcBase svc) {
		this(svc);
		error = e1;
	}

	@Override
	public void decodeBuffer(byte[] buffer) {
		PrimitivesDecoder pd = new PrimitivesDecoder(buffer);
		try {
			error = new BACnetError(ErrorClass.getErrorClass(pd.decodeEnum(null)), ErrorCode.getErrorCode(pd
			        .decodeEnum(null)));
		} catch (IOException e) {
			logger.warn("Unable to decode packet buffer", e);
			throw new SvcException("decoded buffer contains illegal data", e);
		}
	}

	@Override
	public byte[] encodeBuffer() {
		if (error == null)
			return null;
		PrimitivesEncoder pe = new PrimitivesEncoder();
		try {
			pe.encodeEnum(null, error.getErrorClassNum());
			pe.encodeEnum(null, error.getErrorCodeNum());
		} catch (IOException e) {
			throw new SvcException("Cannot encode service", e);
		}

		return pe.getEncodedBuffer();
	}

	@Override
	public APDUBasePacket getPacket() {

		if (header == null)
			throw new SvcException("Unable to create APDU packet");

		if (header.DST_MAC == null) {
			try {
				header.DST_MAC = MPDUHeader.getCopy(RoutingService.discoverRoute(header.DNET).getDST_MAC());
			} catch (RouteNotFoundException e) {
				throw new SvcException(e.getLocalizedMessage());
			}
		}
		ErrorPDU pdu = new ErrorPDU(header);
		pdu.setId(invokeID);
		((ErrorPDU) pdu).setError_choice(cChoise);
		pdu.setBuffer(encodeBuffer());
		return pdu;
	}

	/**
	 * @param error
	 *            the error to set
	 */
	public void setError(BACnetError error) {
		this.error = error;
	}

	public BACnetError getError() {
		return error;
	}

}
