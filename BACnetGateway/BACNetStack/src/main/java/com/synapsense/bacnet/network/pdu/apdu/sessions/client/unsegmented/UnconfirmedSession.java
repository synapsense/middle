package com.synapsense.bacnet.network.pdu.apdu.sessions.client.unsegmented;

import com.synapsense.bacnet.network.pdu.apdu.APDUBasePacket;
import com.synapsense.bacnet.network.pdu.apdu.sessions.AbstractSession;
import com.synapsense.bacnet.network.pdu.apdu.sessions.SessionManager;
import com.synapsense.bacnet.network.pdu.apdu.sessions.SessionManager.SessionId;
import com.synapsense.bacnet.network.pdu.apdu.svc.SvcBase;
import com.synapsense.bacnet.network.pdu.apdu.svc.SvcFactory;
import com.synapsense.scheduler.TaskScheduler;
import com.synapsense.scheduler.TimeoutTaskUnit;

/**
 * 
 * @author Alexander Borisov
 * 
 *         <i>State Machine for Requesting BACnet User (client)</i> </br> </br>
 *         <b>SendUnconfirmed</b></br> If UNCONF_SERV.request is received from
 *         the local application program,</br> then issue an N-UNITDATA.request
 *         with 'data_expecting_reply' = FALSE </br> to transmit a
 *         BACnet-Unconfirmed-Request-PDU,</br> and enter the IDLE state.</br>
 *         </br>
 */

public class UnconfirmedSession extends AbstractSession implements TimeoutTaskUnit {
	private int numExpectedPackets = Integer.parseInt(System.getProperty(
	        "Client.UnconfirmedSession.numExpectedPackets", "1"));

	public UnconfirmedSession(SvcBase svc) {
		super(svc);
		byte b = service.getUChoise().getType();
		id = new SessionId(service.getHeader().DST_MAC, service.getHeader().SRC_MAC, (int) b, true,
		        service.getHeader().media);
		SessionManager.getInstance().addSession(this);
		TaskScheduler.getInstance().addOutgoingTask(this, timeout);
		if (logger.isDebugEnabled())
			logger.debug("New session " + this.toString() + "created");
	}

	public UnconfirmedSession(SvcBase svc, int numExpectedPackets) {
		super(svc);
		byte b = service.getUChoise().getType();
		id = new SessionId(service.getHeader().DST_MAC, service.getHeader().SRC_MAC, (int) b, true,
		        service.getHeader().media);
		SessionManager.getInstance().addSession(this);
		TaskScheduler.getInstance().addOutgoingTask(this, timeout);
		if (logger.isDebugEnabled())
			logger.debug("New session " + this.toString() + "created");
		this.numExpectedPackets = numExpectedPackets;
	}

	@Override
	public void addMessageToSession(APDUBasePacket packet) {
		// TODO Need to add response processing for this session.
		synchronized (inQueue) {
			inQueue.add(packet);
			inQueue.notify();
		}

	}

	@Override
	public void timeoutAction() {
		SessionManager.getInstance().removeSession(this);
		obj = null;
		synchronized (inQueue) {
			inQueue.notify();
		}
	}

	@Override
	public void Execute() throws Exception {
		APDUBasePacket req = service.getPacket();
		// TODO need to add rooting information to packet
		req.send();
		// TODO: Wait for response
		APDUBasePacket response = null;
		do {
			synchronized (inQueue) {
				if (inQueue.isEmpty()) {
					try {
						inQueue.wait();
					} catch (InterruptedException e) {

					}
					continue;
				}
				response = inQueue.poll();

				SvcBase svc = SvcFactory.getInstance().getUnConfirmedSvc(response);
				svc.decodeBuffer(response.getBuffer());
				svc.setHeader(response.getHeader());
				service.addResult(svc);
				numExpectedPackets--;
			}
			// TODO add response here;
		} while (obj != null && numExpectedPackets > 0);
		service.resultAvailiable();
	}

	@Override
	public void failureAction(Throwable t) {
		logger.warn("Session error", t);
		if (packet != null) {
			logger.warn("Packet details: " + packet.toString());
		}
		SessionManager.getInstance().removeSession(this);
		service.resultAvailiable();
		synchronized (inQueue) {
			inQueue.notify();
		}
	}

}
