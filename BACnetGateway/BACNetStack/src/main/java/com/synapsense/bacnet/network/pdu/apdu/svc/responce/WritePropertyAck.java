/**
 * 
 */
package com.synapsense.bacnet.network.pdu.apdu.svc.responce;

import com.synapsense.bacnet.network.media.MPDUHeader;
import com.synapsense.bacnet.network.pdu.apdu.APDUBasePacket;
import com.synapsense.bacnet.network.pdu.apdu.datatypes.ConfirmedServiceChoice;
import com.synapsense.bacnet.network.pdu.apdu.response.SimpleACKPDU;
import com.synapsense.bacnet.network.pdu.apdu.sessions.AbstractSession;
import com.synapsense.bacnet.network.pdu.apdu.sessions.client.unsegmented.ConfirmedSession;
import com.synapsense.bacnet.network.pdu.apdu.svc.SvcBase;
import com.synapsense.bacnet.network.pdu.apdu.svc.SvcException;
import com.synapsense.bacnet.network.routing.RouteNotFoundException;
import com.synapsense.bacnet.network.routing.RoutingService;

/**
 * @author Alexander Borisov
 * 
 *         WritePropertyMultiple-Request ::= SEQUENCE {
 *         listOfwriteAccessSpecifications SEQUENCE OF WriteAccessSpecification
 *         }
 * 
 *         WriteAccessSpecification ::= SEQUENCE { objectIdentifier [0]
 *         BACnetObjectIdentifier, listOfProperties [1] SEQUENCE OF
 *         BACnetPropertyValue }
 * 
 *         BACnetPropertyValue ::= SEQUENCE { propertyIdentifier [0]
 *         BACnetPropertyIdentifier, propertyArrayIndex [1] Unsigned OPTIONAL,
 *         -- used only with array datatypes -- if omitted with an array the
 *         entire array is referenced --value [2] ABSTRACT-SYNTAX.&Type priority
 *         [3] Unsigned (1..16) OPTIONAL -- used only when property is
 *         commandable }
 * 
 */
public class WritePropertyAck extends SvcBase {

	/**
	 * @param svc
	 */
	public WritePropertyAck(SvcBase svc) {
		super(svc);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param header
	 */
	public WritePropertyAck(MPDUHeader header) {
		super(header);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param packet
	 */
	public WritePropertyAck(APDUBasePacket packet) {
		super(packet);
		// TODO Auto-generated constructor stub
	}

	/**
	 * 
	 */
	public WritePropertyAck() {
		// TODO Auto-generated constructor stub
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.synapsense.bacnet.network.pdu.apdu.svc.SvcBase#decodeBuffer(byte[])
	 */
	@Override
	public void decodeBuffer(byte[] buffer) {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsense.bacnet.network.pdu.apdu.svc.SvcBase#encodeBuffer()
	 */
	@Override
	public byte[] encodeBuffer() {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsense.bacnet.network.pdu.apdu.svc.SvcBase#getPacket()
	 */
	@Override
	public APDUBasePacket getPacket() {
		if (header == null)
			throw new SvcException("Unable to create APDU packet");
		if (header.DST_MAC == null) {
			try {
				header.DST_MAC = MPDUHeader.getCopy(RoutingService.discoverRoute(header.DNET).getDST_MAC());
			} catch (RouteNotFoundException e) {
				throw new SvcException(e.getLocalizedMessage());
			}
		}

		SimpleACKPDU response = new SimpleACKPDU(header);
		response.setId(invokeID);
		response.setService_ACK_choice(ConfirmedServiceChoice.writeProperty);
		response.setBuffer(encodedBuffer);
		return response;
	}

	public AbstractSession createSession() {
		return new ConfirmedSession(this);
	}
}
