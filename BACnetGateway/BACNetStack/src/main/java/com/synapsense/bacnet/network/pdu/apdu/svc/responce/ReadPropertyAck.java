package com.synapsense.bacnet.network.pdu.apdu.svc.responce;

import java.io.IOException;

import com.synapsense.bacnet.network.media.MPDUHeader;
import com.synapsense.bacnet.network.pdu.apdu.APDUBasePacket;
import com.synapsense.bacnet.network.pdu.apdu.codecs.PrimitivesDecoder;
import com.synapsense.bacnet.network.pdu.apdu.codecs.PrimitivesEncoder;
import com.synapsense.bacnet.network.pdu.apdu.codecs.primitives.BACnetTL;
import com.synapsense.bacnet.network.pdu.apdu.datatypes.ConfirmedServiceACK;
import com.synapsense.bacnet.network.pdu.apdu.response.ComplexACKPDU;
import com.synapsense.bacnet.network.pdu.apdu.svc.SvcBase;
import com.synapsense.bacnet.network.pdu.apdu.svc.SvcException;
import com.synapsense.bacnet.network.routing.RouteNotFoundException;
import com.synapsense.bacnet.network.routing.RoutingService;
import com.synapsense.bacnet.system.shared.BACnetObjectIdentifier;

/**
 * 
 * @author Alexander Borisov
 * 
 *         ReadProperty-ACK ::= SEQUENCE { objectIdentifier [0]
 *         BACnetObjectIdentifier, propertyIdentifier [1]
 *         BACnetPropertyIdentifier, propertyArrayIndex [2] Unsigned OPTIONAL,
 *         --used only with array datatype -- if omitted with an array the
 *         entire array is referenced propertyValue [3] ABSTRACT-SYNTAX.&Type }
 * 
 */
public class ReadPropertyAck extends SvcBase {

	public ReadPropertyAck(MPDUHeader header) {
		super(header);
		// TODO Auto-generated constructor stub
	}

	public ReadPropertyAck(SvcBase svc) {
		super(svc);
		// TODO Auto-generated constructor stub
	}

	public ReadPropertyAck(APDUBasePacket packet) {
		super(packet);
		// TODO Auto-generated constructor stub
	}

	private BACnetObjectIdentifier objectIdentifier;
	private long propertyIdentifier;
	private Long propertyArrayIndex; // null if propertyArrayIndex [2] Unsigned
									 // OPTIONAL is omitted
	private String propertyValue;

	@Override
	public void decodeBuffer(byte[] buffer) {
		PrimitivesDecoder pd = new PrimitivesDecoder(buffer);
		propertyArrayIndex = null;
		try {
			objectIdentifier = pd.decodeObjectID(0);
			propertyIdentifier = pd.decodeEnum(1);
			BACnetTL tag = pd.askForTag();
			if (tag.getID() == 2) {
				propertyArrayIndex = pd.decodeUInt(2);
			}
			pd.decodeSequenceStart(3);
			propertyValue = pd.decodeAny();
			pd.decodeSequenceStop(3);
		} catch (Exception e) {
			logger.warn("Unable to decode packet buffer", e);
			throw new SvcException("decoded buffer contains illegal data", e);
		}
	}

	@Override
	public byte[] encodeBuffer() {
		PrimitivesEncoder encoder = new PrimitivesEncoder();
		try {
			encoder.encodeObjectID(0, objectIdentifier);
			encoder.encodeUInt(1, propertyIdentifier);
			if (propertyArrayIndex != null)
				encoder.encodeUInt(2, propertyArrayIndex);
			encoder.encodeSequenceStart(3);
			encoder.appendEncodedBuffer(encodedBuffer);
			encoder.encodeSequenceStop(3);
		} catch (IOException e) {
			logger.warn("Unable to encode buffer", e);
			throw new SvcException("Cannot encode service", e);
		}
		return encoder.getEncodedBuffer();
	}

	@Override
	public APDUBasePacket getPacket() {
		if (header == null)
			throw new SvcException("Unable to create APDU packet");
		if (header.DST_MAC == null) {
			try {
				header.DST_MAC = MPDUHeader.getCopy(RoutingService.discoverRoute(header.DNET).getDST_MAC());
			} catch (RouteNotFoundException e) {
				throw new SvcException(e.getLocalizedMessage());
			}
		}

		ComplexACKPDU pdu = new ComplexACKPDU(header);
		pdu.setMore_follows(more_follows);
		pdu.setProposed_window_size(proposed_window_size);
		pdu.setSequence_number(sequence_number);
		pdu.setSegmented_message(segmented_message);
		pdu.setId(invokeID);
		pdu.setService_ACK_choice(ConfirmedServiceACK.readProperty);
		pdu.setBuffer(encodeBuffer());
		return pdu;
	}

	public BACnetObjectIdentifier getObjectIdentifier() {
		return objectIdentifier;
	}

	public void setObjectIdentifier(BACnetObjectIdentifier objectIdentifier) {
		this.objectIdentifier = objectIdentifier;
	}

	public long getPropertyIdentifier() {
		return propertyIdentifier;
	}

	public void setPropertyIdentifier(long propertyIdentifier) {
		this.propertyIdentifier = propertyIdentifier;
	}

	public Long getPropertyArrayIndex() {
		return propertyArrayIndex;
	}

	public void setPropertyArrayIndex(Long propertyArrayIndex) {
		this.propertyArrayIndex = propertyArrayIndex;
	}

	/**
	 * @return the propertyValue
	 */
	public String getPropertyValue() {
		return propertyValue;
	}
}
