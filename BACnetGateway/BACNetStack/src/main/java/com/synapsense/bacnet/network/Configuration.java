/**
 * 
 */
package com.synapsense.bacnet.network;

/**
 * @author Alexander Borisov
 * 
 */
public final class Configuration {
	public static int OWN_NET = 1;
	public static byte[] OWN_ADDR;
	public static int DEFAULT_SESSION_TIMEOUT = 10000; // see 12.11.27
}
