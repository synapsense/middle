package com.synapsense.bacnet.network.jni;


public enum Property {
SRC_MAC(0), // bytes[6]
DST_MAC(1), // bytes[6]
PAYLOAD_LEN(2), // uint16_t
// NCPI
IS_APDU(3), // bool
PRIORITY(4), // uint_8_t (see enum priority_types)
DATA_EXPECTING_REPLY(5), // bool
DNET(6), // uint16_t
DLEN(7), // uint8_t
DADR(8), // bytes[DLEN], absent if DLEN == 0
SNET(9), // uint16_t
SLEN(10), // uint8_t
SADR(11), // bytes[SLEN], absent if SLEN == 0
HOPS(12), // uint8_t
MESSAGE_TYPE(13), // uint8_t (see enum netlayer_message_types)
VENDOR_ID(14), // uint16_t

// NPDU
NDNET(15), // uint16_t
NDNETS(16), // array (uint16_t)
PERF_INDEX(17), // uint8_t
REJECT_REASON(18), // uint8_t (see enum reject_reasons)
ROUTING_TABLES(19), // array (string) !!!SUBJECT TO CHANGE!!!
// format:
// <DNET>_<PORT-ID>_<PORT_INFO>
TERMINATION_TIME_VALUE(20), // uint8_t

// APDU
IS_SEG_REQUEST(21), // bool
MORE_SEG_FOLLOW(22), // bool
SEG_RESP_ACCEPTED(23), // bool
MAX_SEGS(24), // uint8_t (see enum max_segs_accepted)
MAX_RESP(25), // uint8_t (see enum max_resp_len)
INVOKE_ID(26), // uint8_t
SEQUENCE_NUMBER(27), // uint8_t
WINDOW_SIZE(28), // uint8_t
SERV_CHOICE(29), // uint8_t
PDU_PAYLOAD(30), // bytes[]
ACK_OUT_OF_ORDER(31), // bool
ACK_SERVER(32), // bool
ABORT_REASON(33), // uint8_t (see enum abort_reasons)

// BVLL
BVLC(34); // uint8_t

private final int code;

Property(int code) {
	this.code = code;
}

public int getCode() {
	return code;
}

public static String getNameByCode(int code) {
	for (Property prop : Property.values()) {
		if (prop.getCode() == code) {
			return prop.name();
		}
	}
	return "UNKNOWN_PROPERTY_CODE";
}

}
