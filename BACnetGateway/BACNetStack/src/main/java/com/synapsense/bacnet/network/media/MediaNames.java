package com.synapsense.bacnet.network.media;

public enum MediaNames {
Ethernet("com.synapsense.bacnet.network.media.ethernet.Ethernet"), ARCNET(null), MSTP(null), LonTalk(null), BACnetIP(
        "com.synapsense.bacnet.network.media.IP.IP");

private String mediaClassName;

private MediaNames(String cls) {
	this.mediaClassName = cls;
}

/**
 * Creates new Media instance, which is listening on given interface index
 * 
 * @param interfaceIndex
 *            - interface index
 * @return
 */
public Media newMedia(Object object) throws InstantiationException {
	try {
		Class<?> clazz = Class.forName(mediaClassName);
		return (Media) clazz.getConstructor(new Class[] { Object.class }).newInstance(object);
	} catch (Exception e) {
		throw new InstantiationException("Cannot instantiate media");
	}
}
}
