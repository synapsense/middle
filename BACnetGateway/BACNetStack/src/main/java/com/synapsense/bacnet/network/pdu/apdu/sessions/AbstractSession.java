/**
 * 
 */
package com.synapsense.bacnet.network.pdu.apdu.sessions;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.log4j.Logger;

import com.synapsense.bacnet.network.Configuration;
import com.synapsense.bacnet.network.pdu.apdu.APDUBasePacket;
import com.synapsense.bacnet.network.pdu.apdu.sessions.SessionManager.SessionId;
import com.synapsense.bacnet.network.pdu.apdu.sessions.server.segmented.SegmentedSession;
import com.synapsense.bacnet.network.pdu.apdu.sessions.server.unsegmented.ConfirmedSession;
import com.synapsense.bacnet.network.pdu.apdu.sessions.server.unsegmented.UnconfirmedSession;
import com.synapsense.bacnet.network.pdu.apdu.svc.SvcBase;
import com.synapsense.scheduler.TimeoutTaskUnit;

/**
 * This class represents base session - clients and servers
 * 
 * @author Alexander Borisov
 * 
 */
public abstract class AbstractSession implements TimeoutTaskUnit {
	protected SessionId id;
	protected APDUBasePacket packet;
	protected SvcBase service;
	volatile protected Object obj;
	protected Logger logger = Logger.getLogger(this.getClass());
	protected int state;
	protected int timeout;
	protected LinkedBlockingQueue<APDUBasePacket> inQueue;
	private static AtomicInteger invocationID = new AtomicInteger(0);

	/**
	 * Creates session by invokeID identifier This constructor is used by a
	 * server
	 * 
	 * @param packet
	 *            PDU packet
	 */
	public AbstractSession(APDUBasePacket packet) {
		this.packet = packet;
		this.service = null;
		timeout = Configuration.DEFAULT_SESSION_TIMEOUT;
		obj = new Object();
		state = StateConstants.IDLE;
		inQueue = null;
		if (logger.isDebugEnabled())
			logger.debug("New session " + this.toString() + "created");
	}

	/**
	 * This constructor is used by a client
	 */
	public AbstractSession(SvcBase service) {
		this.packet = null;

		this.service = service;
		timeout = Configuration.DEFAULT_SESSION_TIMEOUT;
		obj = new Object();
		state = StateConstants.IDLE;
		inQueue = new LinkedBlockingQueue<APDUBasePacket>();
	}

	/**
	 * Session ID
	 * 
	 * @return ID of session
	 */
	public SessionId getId() {
		return id;
	}

	/**
	 * Session timeout
	 * 
	 * @return timeout of session
	 */
	public int getTimeout() {
		return timeout;
	}

	/**
	 * Sets session ID
	 * 
	 * @param id2
	 *            ID of session
	 */
	public void setId(SessionId id2) {
		this.id = id2;
	}

	/**
	 * Returns current PDU packet
	 * 
	 * @return current PDU packet
	 */
	public APDUBasePacket getPacket() {
		return packet;
	}

	/**
	 * Sets PDU packet
	 * 
	 * @param packet
	 */
	public void setPacket(APDUBasePacket packet) {
		this.packet = packet;
	}

	public void addMessageToSession(APDUBasePacket packet) {
		throw new UnsupportedOperationException("Unknown session");
	}

	/**
	 * Gets/Creates session according to message type
	 * 
	 * @param packet
	 *            incoming packet
	 * @return session
	 */
	public static AbstractSession createServerSession(APDUBasePacket packet) {
		switch (packet.getMessageType()) {
		case confirmed_request_PDU:
			if (packet.isSegmented() == false) {
				return new ConfirmedSession(packet);
			} else {
				return new SegmentedSession(packet);
			}
		case unconfirmed_request_PDU:
			return new UnconfirmedSession(packet);
		default:
			return null;
		}
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer(100);
		sb.append("\nSession ID = ");
		sb.append(id);
		sb.append("\n");

		sb.append("timeout = ");
		sb.append(timeout);
		sb.append("mS\n");

		sb.append("Exception = ");
		sb.append("\n");
		return sb.toString();
	}

	protected static int generateID() {
		return invocationID.getAndIncrement() & 0xFF;

	}
}
