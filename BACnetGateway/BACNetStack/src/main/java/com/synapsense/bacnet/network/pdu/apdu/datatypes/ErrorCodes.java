package com.synapsense.bacnet.network.pdu.apdu.datatypes;

public enum ErrorCodes {
other(127), acknowledgeAlarm(0), confirmedCOVNotification(1), confirmedEventNotification(2), getAlarmSummary(3), getEnrollmentSummary(
        4), getEventInformation(29), subscribeCOV(5), subscribeCOVProperty(28), lifeSafetyOperation(27), atomicReadFile(
        6), atomicWriteFile(7), addListElement(8), removeListElement(9), createObject(10), deleteObject(11), readProperty(
        12), readPropertyConditional(13), readPropertyMultiple(14), readRange(26), writeProperty(15), writePropertyMultiple(
        16), deviceCommunicationControl(17), confirmedPrivateTransfer(18), confirmedTextMessage(19), reinitializeDevice(
        20), vtOpen(21), vtClose(22), vtData(23), authenticate(24), requestKey(25);
private final int error;

ErrorCodes(int error) {
	this.error = error;
}

public int getCode() {
	return error;
}
}
