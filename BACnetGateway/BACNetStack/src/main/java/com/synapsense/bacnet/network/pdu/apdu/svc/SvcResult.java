/**
 * 
 */
package com.synapsense.bacnet.network.pdu.apdu.svc;

import java.util.Vector;

/**
 * @author Alexander Borisov
 * 
 */
public class SvcResult extends Vector<SvcBase> {
	private static final long serialVersionUID = 8160436502437955581L;
}
