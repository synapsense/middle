/**
 * 
 */
package com.synapsense.bacnet.network.pdu.apdu;

import com.synapsense.bacnet.network.jni.Property;
import com.synapsense.bacnet.network.jni.PropertyNotFoundException;
import com.synapsense.bacnet.network.media.MPDUHeader;
import com.synapsense.bacnet.network.pdu.PDUBasePacket;
import com.synapsense.bacnet.network.pdu.apdu.request.AbortPDU;
import com.synapsense.bacnet.network.pdu.apdu.request.ConfirmedRequestPDU;
import com.synapsense.bacnet.network.pdu.apdu.request.UnconfirmedRequestPDU;
import com.synapsense.bacnet.network.pdu.apdu.response.ComplexACKPDU;
import com.synapsense.bacnet.network.pdu.apdu.response.ErrorPDU;
import com.synapsense.bacnet.network.pdu.apdu.response.RejectPDU;
import com.synapsense.bacnet.network.pdu.apdu.response.SegmentACKPDU;
import com.synapsense.bacnet.network.pdu.apdu.response.SimpleACKPDU;

/**
 * @author Alexander Borisov
 * 
 */
abstract public class APDUBasePacket extends PDUBasePacket {
	protected int invokeID;// [7] Unsigned (0..255),
	protected byte[] buffer; // APDU service request
	protected APDUMessageType type;

	public int getInvokeId() {
		return invokeID;
	}

	public byte[] getBuffer() {
		return buffer;
	}

	public void setBuffer(byte buffer[]) {
		this.buffer = buffer;
	}

	public void setId(int invokeID) {
		this.invokeID = invokeID;
	}

	public APDUMessageType getMessageType() {
		return type;
	}

	/**
	 * Checks for segmented message
	 * 
	 * @return true if message is segmented otherwise return false
	 */
	public boolean isSegmented() {
		return false;
	}

	/**
	 * Creates packet according to network data
	 * 
	 * @param header
	 *            NPCI header of BACNet packet
	 * @return APDU packet
	 * @throws PropertyNotFoundException
	 *             if unable to find property value
	 */
	public static PDUBasePacket getPacket(MPDUHeader header) throws PropertyNotFoundException {
		APDUMessageType type = APDUMessageType.valueOf(header.media.readByte(header.packetId, Property.MESSAGE_TYPE));
		switch (type) {
		case confirmed_request_PDU:
			return new ConfirmedRequestPDU(header);
		case unconfirmed_request_PDU:
			return new UnconfirmedRequestPDU(header);
		case simpleACK_PDU:
			return new SimpleACKPDU(header);
		case complexACK_PDU:
			return new ComplexACKPDU(header);
		case segmentAck_PDU:
			return new SegmentACKPDU(header);
		case error_PDU:
			return new ErrorPDU(header);
		case reject_PDU:
			return new RejectPDU(header);
		case abort_PDU:
			return new AbortPDU(header);
		default:
			return null;
		}
	}
}
