package com.synapsense.bacnet.network.pdu.npdu;

public enum NPDUMessageType {
/**
 * X'00': Who-Is-Router-To-Network message
 */
Who_Is_Router_To_Network(0x00),
/**
 * X'01': I-Am-Router-To-Network message
 */
I_Am_Router_To_Network(0x01),
/**
 * X'02': I-Could-Be-Router-To-Network message
 */
I_Could_Be_Router_To_Network(0x02),
/**
 * X'03': Reject-Message-To-Network message
 */
Reject_Message_To_Network(0x03),
/**
 * X'04': Router-Busy-To-Network message
 */
Router_Busy_To_Network(0x04),
/**
 * X'05': Router-Available-To-Network message
 */
Router_Available_To_Network(0x05),
/**
 * X'06': Initialize-Routing-Table message
 */
Initialize_Routing_Table(0x06),
/**
 * X'07': Initialize-Routing-Table-Ack message
 */
Initialize_Routing_Table_Ack(0x07),
/**
 * X'08': Establish-Connection-To-Network message
 */
Establish_Connection_To_Network(0x08),
/**
 * X'09': Disconnect-Connection-To-Network message
 */
Disconnect_Connection_To_Network(0x09), Net_Other(0xff);
private final int type;

NPDUMessageType(int type) {
	this.type = type;
}

public int getCode() {
	return type;

}

/**
 * Returns the enum constant of this type with the specified code
 * 
 * @param code
 *            int code
 * @return message type
 */
static public NPDUMessageType valueOf(int code) {
	switch (code) {
	case 0:
		return Who_Is_Router_To_Network;
	case 1:
		return I_Am_Router_To_Network;
	case 2:
		return I_Could_Be_Router_To_Network;
	case 3:
		return Reject_Message_To_Network;
	case 4:
		return Router_Busy_To_Network;
	case 5:
		return Router_Available_To_Network;
	case 6:
		return Initialize_Routing_Table;
	case 7:
		return Initialize_Routing_Table_Ack;
	case 8:
		return Establish_Connection_To_Network;
	case 9:
		return Disconnect_Connection_To_Network;
	default:
		return Net_Other;
	}
}
}
