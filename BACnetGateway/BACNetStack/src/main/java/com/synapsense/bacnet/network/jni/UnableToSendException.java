package com.synapsense.bacnet.network.jni;

public class UnableToSendException extends BACNetException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 104028853694284639L;

	public UnableToSendException(String s) {
		super(s);
	}
}
