/**
 * 
 */
package com.synapsense.bacnet.network.pdu.npdu.response;

import com.synapsense.bacnet.network.jni.BACNetException;
import com.synapsense.bacnet.network.jni.Property;
import com.synapsense.bacnet.network.jni.PropertyNotFoundException;
import com.synapsense.bacnet.network.media.MPDUHeader;
import com.synapsense.bacnet.network.pdu.npdu.NPDUBasePacket;
import com.synapsense.bacnet.network.pdu.npdu.NPDUMessageType;
import com.synapsense.bacnet.network.routing.RoutingService;
import com.synapsense.scheduler.SimpleTaskUnit;

/**
 * @author Alexander Borisov This message is indicated by a Message Type of
 *         X'04' optionally followed by a list of 2-octet network numbers. It
 *         shall always be transmitted with a broadcast MAC address appropriate
 *         to the network on which it is broadcast. Router-Busy-To-Network is
 *         used by a router to curtail the receipt of messages for specific
 *         DNETs or all DNETs. See Figure 6-9.
 */
public class RouterBusyToNetwork extends NPDUBasePacket implements SimpleTaskUnit {
	protected int[] DNET_LIST;

	public RouterBusyToNetwork(MPDUHeader header) {
		this.header = header;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsense.scheduler.SimpleTaskUnit#doTask()
	 */
	@Override
	public void Execute() throws Exception {
		for (int i = 0; i < DNET_LIST.length; i++) {
			RoutingService.removeRoute(DNET_LIST[i]);
		}
	}

	@Override
	public void receive() throws PropertyNotFoundException {
		DNET_LIST = header.media.readShortArrayAsInt(header.packetId, Property.NDNETS);
	}

	@Override
	public void send() throws BACNetException {
		header.DST_MAC = header.media.getBroadcast();
		header.send();
		if (logger.isDebugEnabled())
			logger.debug("sending " + this.toString());
		header.media.setByte(header.packetId, Property.MESSAGE_TYPE,
		        (byte) NPDUMessageType.Router_Busy_To_Network.getCode());
		if (DNET_LIST != null) {
			header.media.setShortArrayAsInt(header.packetId, Property.NDNETS, DNET_LIST);
		}
		header.media.sendPacket(header.packetId);
	}

	@Override
	public void failureAction(Throwable t) {
	}

	/**
	 * Sets destination list
	 * 
	 * @param DNET_LIST
	 *            list of destinations
	 */
	public void SetDestinations(int[] DNET_LIST) {
		this.DNET_LIST = DNET_LIST;
	}

	/**
	 * Sets destination
	 * 
	 * @param DNET
	 *            destination
	 */
	public void SetDestinations(int DNET) {
		this.DNET_LIST = new int[1];
		DNET_LIST[0] = DNET;
	}

	public int[] getDestinations() {
		return DNET_LIST;
	}
}
