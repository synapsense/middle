package com.synapsense.bacnet.network.pdu.apdu.datatypes;

public enum AbortReason {
other(0), buffer_overflow(1), invalid_apdu_in_this_state(2), preempted_by_higher_priority_task(3), segmentation_not_supported(
        4);

private final int reason;

AbortReason(int reason) {
	this.reason = reason;
}

public int getCode() {
	return reason;
}

public static AbortReason valueOf(int value) {
	switch (value) {
	case 0:
		return other;
	case 1:
		return buffer_overflow;
	case 2:
		return invalid_apdu_in_this_state;
	case 3:
		return preempted_by_higher_priority_task;
	case 4:
		return segmentation_not_supported;
	default:
		throw new IllegalArgumentException("Unable to identify enum type");
	}
}

}
