package com.synapsense.bacnet.network.pdu.apdu.sessions.client.unsegmented;

import com.synapsense.bacnet.network.pdu.apdu.APDUBasePacket;
import com.synapsense.bacnet.network.pdu.apdu.sessions.AbstractSession;
import com.synapsense.bacnet.network.pdu.apdu.sessions.SessionManager;
import com.synapsense.bacnet.network.pdu.apdu.sessions.SessionManager.SessionId;
import com.synapsense.bacnet.network.pdu.apdu.svc.SvcBase;
import com.synapsense.bacnet.network.pdu.apdu.svc.SvcFactory;
import com.synapsense.bacnet.network.pdu.apdu.svc.responce.ErrorSvc;
import com.synapsense.scheduler.TaskScheduler;
import com.synapsense.scheduler.TimeoutTaskUnit;

/**
 * 
 * 
 *
 */

/**
 * @author Alexander Borisov
 * 
 *         </br> <b><i>Confirmed session</i></b></br> </br> <b>IDLE state</b>
 *         </br> </br> <b>ConfirmedBroadcastReceived</b></br> If a
 *         BACnet-Confirmed-Request-PDU whose destination address is a multicast
 *         or broadcast address is received from the network layer, then enter
 *         the IDLE state.</br> </br> <b>ConfirmedUnsegmentedReceived</b></br>
 *         If a BACnet-Confirmed-Request-PDU whose 'segmented-message' parameter
 *         is FALSE is received from the network layer,then send a
 *         CONF_SERV.indication to the local application program, start
 *         RequestTimer; and enter the AWAIT_RESPONSE state.</br> </br>
 *         <b>AbortPDU_Received</b></br> If a BACnet-Abort-PDU whose 'server'
 *         parameter is FALSE is received from the network layer,</br> then
 *         enter the IDLE state.</br> </br> <b>UnexpectedPDU_Received</b></br>
 *         If an unexpected PDU (BACnet-Confirmed-Request-PDU with
 *         'segmented-message' = TRUE and 'sequencenumber' not equal to zero or
 *         BACnet-SegmentACK-PDU with 'server' = FALSE) is received from the
 *         network layer, then issue an N-UNITDATA.request with
 *         'data_expecting_reply' = FALSE to transmit a BACnet-Abort-PDU with
 *         'server' = TRUE; and enter the IDLE state.</br> </br>
 *         <b>AWAIT_RESPONSE state</b></br> </br> <b>SendSimpleACK</b></br> If a
 *         CONF_SERV.response(+) is received from the local application program,
 *         which is to be conveyed via a BACnet-SimpleACK-PDU, then issue an
 *         N-UNITDATA.request with 'data_expecting_reply' = FALSE to transmit a
 *         BACnet-SimpleACK-PDU and enter the IDLE state.</br> </br>
 *         <b>SendUnsegmentedComplexACK</b></br> If a CONF_SERV.response(+) is
 *         received from the local application program, which is to be conveyed
 *         via a BACnet-ComplexACK-PDU, and the length of the APDU is less than
 *         or equal to maximum-transmittable-length as determined according to
 *         5.2.1, then issue an N-UNITDATA.request with 'data_expecting_reply' =
 *         FALSE to transmit a BACnet-ComplexACKPDU with 'segmented-message' =
 *         FALSE and enter the IDLE state.</br> </br>
 *         <b>CannotSendSegmentedComplexACK</b></br> If a CONF_SERV.response(+)
 *         is received from the local application program, which is to be
 *         conveyed via a BACnet-ComplexACK-PDU, and the length of the APDU is
 *         greater than maximum-transmittable-length as determined according to
 *         5.2.1, and either</br>
 * 
 *         a) this device does not support the transmission of segmented
 *         messages or</br> b) the client will not accept a segmented response
 *         (the 'segmented-response-accepted' parameter in BACnet-</br>
 *         ConfirmedRequest-PDU is FALSE), or</br> c) the client's
 *         max-segments-accepted parameter in the BACnet-ConfirmedRequest-PDU is
 *         fewer than required to</br> transmit the total APDU or,</br> d) the
 *         number of segments transmittable by this device is fewer than
 *         required to transmit the total APDU,</br> then issue an
 *         N-UNITDATA.request with 'data_expecting_reply' = FALSE to transmit a
 *         BACnet-Abort-PDU with</br> 'server' = TRUE and 'abort-reason' =
 *         SEGMENTATION_NOT_SUPPORTED for case (a) and (b), or</br>
 *         BUFFER_OVERFLOW for case (c) and (d), and enter the IDLE state.</br>
 *         </br> <b>SendErrorPDU</b></br> If a CONF_SERV.response(-) is received
 *         from the local application program,</br> then issue an
 *         N-UNITDATA.request with 'data_expecting_reply' = FALSE to transmit a
 *         BACnet-Error-PDU and enter the IDLE state.</br> </br>
 *         <b>SendAbort</b></br> If ABORT.request is received from the local
 *         application program, then stop SegmentTimer; issue an
 *         N-UNITDATA.request with 'data_expecting_reply' = FALSE to transmit a
 *         BACnet-Abort-PDU with 'server' = TRUE; and enter the IDLE state.</br>
 *         </br> <b>SendReject</b></br> If REJECT.request is received from the
 *         local application program,</br> then stop SegmentTimer; issue an
 *         N-UNITDATA.request with 'data_expecting_reply' = FALSE to transmit a
 *         BACnet-Reject-PDU; and enter the IDLE state.</br> </br>
 *         <b>AbortPDU_Received</b></br> If a BACnet-Abort-PDU whose 'server'
 *         parameter is FALSE is received from the network layer, then send
 *         ABORT.indication to the local application program; and enter the IDLE
 *         state.</br> </br> <b>DuplicateRequestReceived</b></br> If a
 *         BACnet-Confirmed-Request-PDU whose 'segmented-message' parameter is
 *         FALSE is received from the network layer, then discard the PDU as a
 *         duplicate request, and re-enter the current state.</br> </br>
 *         <b>UnexpectedPDU_Received</b></br> If an unexpected PDU
 *         (BACnet-SegmentACK-PDU whose 'server' parameter is FALSE) is received
 *         from the network layer, then issue an N-UNITDATA.request with
 *         'data_expecting_reply' = FALSE to transmit a BACnet-Abort-PDU
 *         with</br> 'server' = TRUE; send ABORT.indication to the local
 *         application program; and enter the IDLE state.</br> </br>
 *         <b>Timeout</b></br> If RequestTimer becomes greater than Tout, then
 *         issue an N-UNITDATA.request with 'data_expecting_reply' = FALSE to
 *         transmit a BACnet-Abort-PDU with 'server' = TRUE; send
 *         ABORT.indication to the local application program; and enter the IDLE
 *         state.</br> </br>
 * 
 */

public class ConfirmedSession extends AbstractSession implements TimeoutTaskUnit {

	public ConfirmedSession(SvcBase svc) {
		super(svc);
		int invId = generateID();
		id = new SessionId(svc.getHeader().DST_MAC, svc.getHeader().SRC_MAC, invId, false, svc.getHeader().media);
		SessionManager.getInstance().addSession(this);
		TaskScheduler.getInstance().addOutgoingTask(this, timeout);
		if (logger.isDebugEnabled())
			logger.debug("New session " + this.toString() + "created");
	}

	@Override
	public void addMessageToSession(APDUBasePacket packet) {
		// TODO Need to add response processing for this session.
		synchronized (inQueue) {
			inQueue.add(packet);
			inQueue.notify();
		}
	}

	@Override
	public void timeoutAction() {
		SessionManager.getInstance().removeSession(this);
		obj = null;
		synchronized (inQueue) {
			inQueue.notify();
		}
	}

	@Override
	public void Execute() throws Exception {
		APDUBasePacket req = service.getPacket();
		// TODO need to add rooting information to packet

		req.setId(this.getId().getId());
		req.send();
		// TODO: Wait for response
		APDUBasePacket response = null;

		do {
			synchronized (inQueue) {
				if (inQueue.isEmpty()) {
					try {
						inQueue.wait();
					} catch (InterruptedException e) {

					}
					continue;
				}
				response = inQueue.poll();

				switch (response.getMessageType()) {
				case confirmed_request_PDU: {
					SvcBase svc = SvcFactory.getInstance().getConfirmedSvc(response);
					svc.decodeBuffer(response.getBuffer());
					svc.setHeader(response.getHeader());
					service.addResult(svc);
				}
					break;

				case simpleACK_PDU: {
					SvcBase svc = SvcFactory.getInstance().getConfirmedSvcSimple(response);
					svc.setHeader(response.getHeader());
					service.addResult(svc);
				}
					break;

				case complexACK_PDU: {
					SvcBase svc = SvcFactory.getInstance().getConfirmedACK(response);
					svc.decodeBuffer(response.getBuffer());
					svc.setHeader(response.getHeader());
					service.addResult(svc);
				}
					break;
				case error_PDU: {
					ErrorSvc svc = new ErrorSvc(response.getHeader());
					svc.decodeBuffer(response.getBuffer());
					service.addResult(svc);
				}
					break;

				default:
					throw new IllegalStateException("Unexpected APDU recieved\n" + response.toString());
				}
				service.resultAvailiable();
				SessionManager.getInstance().removeSession(this);
				return;
			}
		} while (obj != null);
		service.resultAvailiable();

	}

	@Override
	public void failureAction(Throwable t) {
		logger.warn("Session error", t);
		if (packet != null) {
			logger.warn("Packet details: " + packet.toString());
		}
		SessionManager.getInstance().removeSession(this);
		service.resultAvailiable();
		synchronized (inQueue) {
			inQueue.notify();
		}
	}

}
