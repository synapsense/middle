package com.synapsense.bacnet.network.pdu.apdu.svc;

import org.apache.log4j.Logger;

import com.synapsense.bacnet.enums.BACnetAPDUSize;
import com.synapsense.bacnet.network.media.MPDUHeader;
import com.synapsense.bacnet.network.pdu.apdu.APDUBasePacket;
import com.synapsense.bacnet.network.pdu.apdu.datatypes.ConfirmedServiceChoice;
import com.synapsense.bacnet.network.pdu.apdu.datatypes.UnconfirmedServiceChoice;
import com.synapsense.bacnet.network.pdu.apdu.request.ConfirmedRequestPDU;
import com.synapsense.bacnet.network.pdu.apdu.request.UnconfirmedRequestPDU;
import com.synapsense.bacnet.network.pdu.apdu.response.ComplexACKPDU;
import com.synapsense.bacnet.network.pdu.apdu.sessions.AbstractSession;
import com.synapsense.bacnet.system.BACnetError;

/**
 * This class is wrapper for all services
 * 
 * @author Aexander Borisov
 * 
 */
abstract public class SvcBase {
	protected MPDUHeader header;
	protected Logger logger = Logger.getLogger(this.getClass());
	protected byte[] encodedBuffer;
	protected SvcResult result = new SvcResult();

	protected boolean segmented_message;// [1] BOOLEAN,
	protected boolean more_follows;// [2] BOOLEAN,
	protected boolean segmented_response_accepted;// [3] BOOLEAN,
	protected int max_segments_accepted;// [5] Unsigned (0..7), -- as per
	                                    // 20.1.2.4
	protected BACnetAPDUSize max_APDU_length_accepted;// [6] Unsigned (0..15),
	                                                  // -- as per
	// 20.1.2.5
	protected int sequence_number;// [8] Unsigned (0..255) OPTIONAL, -- only if
	                              // segmented msg
	protected int proposed_window_size;// [9] Unsigned (1..127) OPTIONAL, --
	                                   // only if
	protected int invokeID;
	protected ConfirmedServiceChoice cChoise = null;
	protected UnconfirmedServiceChoice uChoise = null;

	/**
	 * Creates new copy of service
	 * 
	 * @param svc
	 *            original service
	 */
	public SvcBase(SvcBase svc) {
		this.segmented_message = svc.segmented_message;
		this.more_follows = svc.more_follows;// [2] BOOLEAN,
		this.segmented_response_accepted = svc.segmented_response_accepted;// [3]
		                                                                   // BOOLEAN,
		this.max_segments_accepted = svc.max_segments_accepted;// [5] Unsigned
		                                                       // (0..7), -- as
		                                                       // per 20.1.2.4
		this.max_APDU_length_accepted = svc.max_APDU_length_accepted;// [6]
		                                                             // Unsigned
		                                                             // (0..15),
		                                                             // -- as
		                                                             // per
		                                                             // 20.1.2.5
		this.sequence_number = svc.sequence_number;// [8] Unsigned (0..255)
		                                           // OPTIONAL, -- only if
		                                           // segmented msg
		this.proposed_window_size = svc.proposed_window_size;// [9] Unsigned
		                                                     // (1..127)
		                                                     // OPTIONAL, --
		                                                     // only if
		this.invokeID = svc.invokeID;
		this.cChoise = svc.cChoise;
		this.uChoise = svc.uChoise;
	}

	public SvcBase(MPDUHeader header) {
		this.header = new MPDUHeader(header);
	}

	/**
	 * Creates new copy of service
	 * 
	 * @param packet
	 *            packet
	 */
	public SvcBase(APDUBasePacket packet) {
		this.segmented_message = packet.isSegmented();
		this.invokeID = packet.getInvokeId();

		if (packet instanceof ConfirmedRequestPDU) {
			this.segmented_response_accepted = ((ConfirmedRequestPDU) packet).isSegmented_response_accepted();
			this.max_segments_accepted = ((ConfirmedRequestPDU) packet).getMax_segments_accepted();
			this.max_APDU_length_accepted = ((ConfirmedRequestPDU) packet).getMax_APDU_length_accepted();
			this.sequence_number = ((ConfirmedRequestPDU) packet).getSequence_number();
			this.proposed_window_size = ((ConfirmedRequestPDU) packet).getProposed_window_size();
			this.more_follows = ((ConfirmedRequestPDU) packet).isMore_follows();
			this.cChoise = ((ConfirmedRequestPDU) packet).getService_choice();
		}
		if (packet instanceof ComplexACKPDU) {
			this.sequence_number = ((ComplexACKPDU) packet).getSequence_number();
			this.proposed_window_size = ((ComplexACKPDU) packet).getProposed_window_size();
			this.more_follows = ((ComplexACKPDU) packet).isMore_follows();
		}
		if (packet instanceof UnconfirmedRequestPDU) {
			this.uChoise = ((UnconfirmedRequestPDU) packet).getSVCChoise();
		}
	}

	protected SvcBase() {

	}

	public void addResult(SvcBase item) {
		result.add(item);
	}

	public SvcResult getResult() {
		synchronized (result) {
			try {
				result.wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return result;
	}

	public void resultAvailiable() {
		synchronized (result) {
			result.notify();
		}
	}

	public void setHeader(MPDUHeader header) {
		this.header = new MPDUHeader(header);
	}

	public MPDUHeader getHeader() {
		return header;
	}

	/**
	 * Execution of service This function should be redefined in your service
	 * 
	 * @return result of execution
	 */
	public SvcBase[] execute() {
		throw new SvcException("Service is not supported");
	}

	public byte[] getEncodedBuffer() {
		return encodedBuffer;
	}

	public void setEncodedBuffer(byte[] buffer) {
		if (buffer == null)
			return;
		encodedBuffer = new byte[buffer.length];
		System.arraycopy(buffer, 0, encodedBuffer, 0, buffer.length);
	}

	/**
	 * This method is used to decode service data buffer
	 * 
	 * @param buffer
	 *            byte buffer
	 * @throws BACnetError
	 */
	abstract public void decodeBuffer(byte[] buffer) throws BACnetError;

	/**
	 * This method is used to encode result of execution service
	 * 
	 * @return encoded buffer
	 */
	abstract public byte[] encodeBuffer();

	public AbstractSession createSession() {
		throw new IllegalArgumentException("This method is not supported for this svc type");
	}

	public AbstractSession createSession(int numExpectedPackets) {
		throw new IllegalArgumentException("This method is not supported for this svc type");
	}

	abstract public APDUBasePacket getPacket();

	/**
	 * @return the segmented_message
	 */
	public boolean isSegmented_message() {
		return segmented_message;
	}

	/**
	 * @param segmented_message
	 *            the segmented_message to set
	 */
	public void setSegmented_message(boolean segmented_message) {
		this.segmented_message = segmented_message;
	}

	/**
	 * @return the more_follows
	 */
	public boolean isMore_follows() {
		return more_follows;
	}

	/**
	 * @param more_follows
	 *            the more_follows to set
	 */
	public void setMore_follows(boolean more_follows) {
		this.more_follows = more_follows;
	}

	/**
	 * @return the segmented_response_accepted
	 */
	public boolean isSegmented_response_accepted() {
		return segmented_response_accepted;
	}

	/**
	 * @param segmented_response_accepted
	 *            the segmented_response_accepted to set
	 */
	public void setSegmented_response_accepted(boolean segmented_response_accepted) {
		this.segmented_response_accepted = segmented_response_accepted;
	}

	/**
	 * @return the max_segments_accepted
	 */
	public int getMax_segments_accepted() {
		return max_segments_accepted;
	}

	/**
	 * @param max_segments_accepted
	 *            the max_segments_accepted to set
	 */
	public void setMax_segments_accepted(int max_segments_accepted) {
		this.max_segments_accepted = max_segments_accepted;
	}

	/**
	 * @return the max_APDU_length_accepted
	 */
	public BACnetAPDUSize getMax_APDU_length_accepted() {
		return max_APDU_length_accepted;
	}

	/**
	 * @param max_APDU_length_accepted
	 *            the max_APDU_length_accepted to set
	 */
	public void setMax_APDU_length_accepted(BACnetAPDUSize max_APDU_length_accepted) {
		this.max_APDU_length_accepted = max_APDU_length_accepted;
	}

	/**
	 * @return the sequence_number
	 */
	public int getSequence_number() {
		return sequence_number;
	}

	/**
	 * @param sequence_number
	 *            the sequence_number to set
	 */
	public void setSequence_number(int sequence_number) {
		this.sequence_number = sequence_number;
	}

	/**
	 * @return the proposed_window_size
	 */
	public int getProposed_window_size() {
		return proposed_window_size;
	}

	/**
	 * @param proposed_window_size
	 *            the proposed_window_size to set
	 */
	public void setProposed_window_size(int proposed_window_size) {
		this.proposed_window_size = proposed_window_size;
	}

	/**
	 * @return the invokeID
	 */
	public int getInvokeID() {
		return invokeID;
	}

	/**
	 * @param invokeID
	 *            the invokeID to set
	 */
	public void setInvokeID(int invokeID) {
		this.invokeID = invokeID;
	}

	public ConfirmedServiceChoice getCChoise() {
		return cChoise;
	}

	public UnconfirmedServiceChoice getUChoise() {
		return uChoise;
	}

}
