package com.synapsense.bacnet.network.pdu.apdu.response;

import com.synapsense.bacnet.network.jni.BACNetException;
import com.synapsense.bacnet.network.jni.Property;
import com.synapsense.bacnet.network.jni.PropertyNotFoundException;
import com.synapsense.bacnet.network.media.MPDUHeader;
import com.synapsense.bacnet.network.media.Media;
import com.synapsense.bacnet.network.pdu.apdu.APDUBasePacket;
import com.synapsense.bacnet.network.pdu.apdu.APDUMessageType;
import com.synapsense.bacnet.network.pdu.apdu.datatypes.ConfirmedServiceChoice;

/**
 * 
 * @author Alexander Borisov </br> The PDU fields have the following values:
 *         </br> PDU Type = 5 (BACnet-Error-PDU)</br> Original Invoke ID =
 *         (0..255)</br> Error Choice = BACnetConfirmedServiceChoice</br> Error
 *         = Variable Encoding per 20.2.</br> </br>
 */
public class ErrorPDU extends APDUBasePacket {
	ConfirmedServiceChoice error_choice; // [3] BACnetConfirmedServiceChoice,

	public ErrorPDU(MPDUHeader header) {
		this.header = header;
		type = APDUMessageType.error_PDU;
		buffer = null;
	}

	public ErrorPDU(MPDUHeader header, ConfirmedServiceChoice err) {
		this.header = header;
		type = APDUMessageType.error_PDU;
		error_choice = err;
		buffer = null;
	}

	@Override
	public void receive() throws PropertyNotFoundException {
		Media reader = header.media;
		invokeID = reader.readByte(header.packetId, Property.INVOKE_ID) & 0xFF;
		error_choice = ConfirmedServiceChoice.valueOf(reader.readByte(header.packetId, Property.SERV_CHOICE));
		buffer = reader.readByteArray(header.packetId, Property.PDU_PAYLOAD);
	}

	@Override
	public void send() throws BACNetException {
		Media reader = header.media;
		header.send();
		reader.setByte(header.packetId, Property.SERV_CHOICE, (byte) error_choice.getCode());
		if (buffer == null) {
			buffer = new byte[0];
		}
		reader.setByteArray(header.packetId, Property.PDU_PAYLOAD, buffer);
		reader.setByte(header.packetId, Property.MESSAGE_TYPE, type.getType());
		reader.setByte(header.packetId, Property.INVOKE_ID, (byte) invokeID);
		reader.sendPacket(header.packetId);

	}

	@Override
	public boolean isSegmented() {
		return false;
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer(100);
		sb.append("\n");
		sb.append(this.getClass().getName());
		sb.append(header);
		sb.append("invoke ID = ");
		sb.append(invokeID);
		sb.append("\n");

		sb.append("error_choice = ");
		sb.append(error_choice);
		sb.append("\n");

		return sb.toString();
	}

	/**
	 * @return the error_choice
	 */
	public ConfirmedServiceChoice getError_choice() {
		return error_choice;
	}

	/**
	 * @param error_choice
	 *            the error_choice to set
	 */
	public void setError_choice(ConfirmedServiceChoice error_choice) {
		this.error_choice = error_choice;
	}
}
