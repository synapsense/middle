package com.synapsense.bacnet.network.jni;

public class MalformedPacketException extends BACNetException {

	public MalformedPacketException(String s) {
		super(s);
	}

	public MalformedPacketException() {
		super("");
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 8980461404186416011L;

}
