package com.synapsense.bacnet.network.pdu.apdu.datatypes;

public enum RejectReason {
other(0), buffer_overflow(1), inconsistent_parameters(2), invalid_parameter_data_type(3), invalid_tag(4), missing_required_parameter(
        5), parameter_out_of_range(6), too_many_arguments(7), undefined_enumeration(8), unrecognized_service(9);

private final int reason;

RejectReason(int reason) {
	this.reason = reason;
}

public int getCode() {
	return reason;
}

public static RejectReason valueOf(int value) {
	switch (value) {
	case 0:
		return other;
	case 1:
		return buffer_overflow;
	case 2:
		return inconsistent_parameters;
	case 3:
		return invalid_parameter_data_type;
	case 4:
		return invalid_tag;
	case 5:
		return missing_required_parameter;
	case 6:
		return parameter_out_of_range;
	case 7:
		return too_many_arguments;
	case 8:
		return undefined_enumeration;
	case 9:
		return unrecognized_service;
	default:
		throw new IllegalArgumentException("Unable to identify enum type");

	}

}
}
