package com.synapsense.bacnet.network.pdu.apdu.request;

import com.synapsense.bacnet.network.jni.BACNetException;
import com.synapsense.bacnet.network.jni.Property;
import com.synapsense.bacnet.network.jni.PropertyNotFoundException;
import com.synapsense.bacnet.network.media.MPDUHeader;
import com.synapsense.bacnet.network.media.Media;
import com.synapsense.bacnet.network.pdu.apdu.APDUBasePacket;
import com.synapsense.bacnet.network.pdu.apdu.APDUMessageType;
import com.synapsense.bacnet.network.pdu.apdu.datatypes.AbortReason;

/**
 * 
 * @author Alexander Borisov </br> The BACnet-Abort-PDU is used to terminate a
 *         transaction between two peers.</br> BACnet-Abort-PDU ::= SEQUENCE
 *         {</br> pdu-type [0] Unsigned (0..15), -- 7 for this PDU type</br>
 *         reserved [1] Unsigned (0..7), -- must be set to zero</br> server [2]
 *         BOOLEAN,</br> original-invokeID [3] Unsigned (0..255),</br>
 *         abort-reason [4] BACnetAbortReason</br> -- Context specific tags 0..4
 *         are NOT used in header encoding</br> }
 */
public class AbortPDU extends APDUBasePacket {
	boolean server;// [2] BOOLEAN,
	AbortReason abort_reason = AbortReason.other;// [4] BACnetAbortReason

	public AbortPDU(MPDUHeader header) {
		this.header = header;
		type = APDUMessageType.abort_PDU;
	}

	public AbortPDU(MPDUHeader header, AbortReason reason) {
		this.header = header;
		type = APDUMessageType.abort_PDU;
		this.abort_reason = reason;
	}

	/**
	 * Sets server flag
	 * 
	 * @param server
	 *            server flag
	 */
	public void setServer(boolean server) {
		this.server = server;
	}

	/**
	 * Gets server flag
	 * 
	 * @return server flag
	 */
	public boolean getServer() {
		return server;
	}

	/**
	 * Sets abort reason
	 * 
	 * @param reason
	 *            abort reason
	 */
	public void setAbortReason(AbortReason reason) {
		this.abort_reason = reason;
	}

	public AbortReason getAbortReason() {
		return abort_reason;
	}

	@Override
	public void receive() throws PropertyNotFoundException {
		Media reader = header.media;
		server = reader.readBoolean(header.packetId, Property.ACK_SERVER);
		abort_reason = AbortReason.valueOf(reader.readByte(header.packetId, Property.ABORT_REASON));
		invokeID = reader.readByte(header.packetId, Property.INVOKE_ID) & 0xFF;
	}

	@Override
	public void send() throws BACNetException {
		Media reader = header.media;
		header.send();
		reader.setByte(header.packetId, Property.INVOKE_ID, (byte) invokeID);
		reader.setByte(header.packetId, Property.MESSAGE_TYPE, type.getType());
		reader.setBoolean(header.packetId, Property.ACK_SERVER, server);
		if (abort_reason != null)
			reader.setByte(header.packetId, Property.ABORT_REASON, (byte) abort_reason.getCode());
		reader.sendPacket(header.packetId);
	}

	@Override
	public boolean isSegmented() {
		return false;
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer(100);
		sb.append("\n");
		sb.append(this.getClass().getName());
		sb.append(header);
		sb.append("server = ");
		sb.append(server);
		sb.append("\n");
		sb.append("invoke ID = ");
		sb.append(invokeID);
		sb.append("\n");

		sb.append("abort_reason=");
		sb.append(abort_reason);
		sb.append("\n");

		return sb.toString();
	}

}
