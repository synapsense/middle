package com.synapsense.bacnet.network.pdu.apdu.datatypes;

public enum ConfirmedServiceRequest {
// -- Alarm and Event Services
acknowledgeAlarm, // [0] AcknowledgeAlarm-Request,
confirmedCOVNotification, // [1] ConfirmedCOVNotification-Request,
confirmedEventNotification, // [2] ConfirmedEventNotification-Request,
// -- getAlarmSummary conveys no parameters
getEnrollmentSummary, // [4] GetEnrollmentSummary-Request,
getEventInformation, // [29] GetEventInformation-Request,
subscribeCOV, // [5] SubscribeCOV-Request,
subscribeCOVProperty, // [28] SubscribeCOVProperty-Request,
lifeSafetyOperation, // [27] LifeSafetyOperation-Request,
// -- File Access Services
atomicReadFile, // [6] AtomicReadFile-Request,
atomicWriteFile, // [7] AtomicWriteFile-Request,
// -- Object Access Services
addListElement, // [8] AddListElement-Request,
removeListElement, // [9] RemoveListElement-Request,
createObject, // [10] CreateObject-Request,
deleteObject, // [11] DeleteObject-Request,
readProperty, // [12] ReadProperty-Request,
readPropertyConditional, // [13] ReadPropertyConditional-Request,
readPropertyMultiple, // [14] ReadPropertyMultiple-Request,
readRange, // [26] ReadRange-Request,
writeProperty, // [15] WriteProperty-Request,
writePropertyMultiple, // [16] WritePropertyMultiple-Request,
// -- Remote Device Management Services
deviceCommunicationControl, // [17] DeviceCommunicationControl-Request,
confirmedPrivateTransfer, // [18] ConfirmedPrivateTransfer-Request,
confirmedTextMessage, // [19] ConfirmedTextMessage-Request,
reinitializeDevice, // [20] ReinitializeDevice-Request,
// -- Virtual Terminal Services
vtOpen, // [21] VT-Open-Request,
vtClose, // [22] VT-Close-Request,
vtData, // [23] VT-Data-Request,
// -- Security Services
authenticate, // [24] Authenticate-Request,
requestKey // [25] RequestKey-Request
}
