package com.synapsense.bacnet.network.pdu.apdu.svc.request;

import java.io.IOException;
import java.util.Collection;
import java.util.Vector;

import com.synapsense.bacnet.enums.BACnetAPDUSize;
import com.synapsense.bacnet.network.Constants;
import com.synapsense.bacnet.network.media.MPDUHeader;
import com.synapsense.bacnet.network.pdu.apdu.APDUBasePacket;
import com.synapsense.bacnet.network.pdu.apdu.codecs.PrimitivesDecoder;
import com.synapsense.bacnet.network.pdu.apdu.codecs.PrimitivesEncoder;
import com.synapsense.bacnet.network.pdu.apdu.codecs.primitives.BACnetTL;
import com.synapsense.bacnet.network.pdu.apdu.datatypes.UnconfirmedServiceChoice;
import com.synapsense.bacnet.network.pdu.apdu.request.UnconfirmedRequestPDU;
import com.synapsense.bacnet.network.pdu.apdu.sessions.AbstractSession;
import com.synapsense.bacnet.network.pdu.apdu.sessions.client.unsegmented.UnconfirmedSession;
import com.synapsense.bacnet.network.pdu.apdu.svc.SvcBase;
import com.synapsense.bacnet.network.pdu.apdu.svc.SvcException;
import com.synapsense.bacnet.network.pdu.apdu.svc.SvcFactory;
import com.synapsense.bacnet.network.pdu.apdu.svc.responce.IAm;
import com.synapsense.bacnet.network.routing.MalformedHeaderException;
import com.synapsense.bacnet.network.routing.RouteNotFoundException;
import com.synapsense.bacnet.network.routing.RoutingService;
import com.synapsense.bacnet.objects.BACnetDeviceWrapper;
import com.synapsense.bacnet.system.BACnetNetwork;
import com.synapsense.bacnet.system.BACnetSystem;
import com.synapsense.bacnet.system.Device;
import com.synapsense.bacnet.system.shared.BACnetAddressBinding;

/**
 * 
 * @author Alexander Borisov
 *
 */

/**
 * </br> <b>Who-Is-Request</b> ::= SEQUENCE {</br> deviceInstanceRangeLowLimit
 * [0] Unsigned (0..4194303) OPTIONAL, -- must be used as a pair, see 16.10</br>
 * deviceInstanceRangeHighLimit [1] Unsigned (0..4194303) OPTIONAL -- must be
 * used as a pair, see 16.10</br> }</br>
 */
public class WhoIs extends SvcBase {
	public WhoIs(MPDUHeader header) {
		super(header);
		uChoise = UnconfirmedServiceChoice.i_Am; // Need to register session for
		                                         // response
	}

	public WhoIs(SvcBase svc) {
		super(svc);
	}

	public WhoIs(APDUBasePacket packet) {
		super(packet);
	}

	protected int deviceInstanceRangeLowLimit = Constants.PROPERTY_NOT_SPECIFIED;
	protected int deviceInstanceRangeHighLimit = Constants.PROPERTY_NOT_SPECIFIED;

	@Override
	public void decodeBuffer(byte[] buffer) {
		PrimitivesDecoder pd = new PrimitivesDecoder(buffer);
		try {
			deviceInstanceRangeLowLimit = Constants.PROPERTY_NOT_SPECIFIED;
			deviceInstanceRangeHighLimit = Constants.PROPERTY_NOT_SPECIFIED;

			BACnetTL btl = pd.askForTag();
			if (btl == null)
				return;
			if (btl.getID() != 0)
				throw new SvcException("Unable to decode data");

			deviceInstanceRangeLowLimit = (int) pd.decodeUInt(0);
			deviceInstanceRangeHighLimit = (int) pd.decodeUInt(1);
		} catch (java.io.IOException e) {
			logger.warn("Unable to decode packet buffer", e);
			throw new SvcException("decoded buffer contains illegal data", e);
		}
	}

	@Override
	public byte[] encodeBuffer() {
		PrimitivesEncoder pe = new PrimitivesEncoder();
		try {
			if (deviceInstanceRangeLowLimit != Constants.PROPERTY_NOT_SPECIFIED
			        && deviceInstanceRangeHighLimit != Constants.PROPERTY_NOT_SPECIFIED) {
				pe.encodeUInt(0, deviceInstanceRangeLowLimit);
				pe.encodeUInt(1, deviceInstanceRangeHighLimit);
			}
		} catch (IOException e) {
			logger.warn("Unable to encode buffer", e);
			throw new SvcException("Cannot encode service", e);
		}
		return pe.getEncodedBuffer();
	}

	@Override
	public SvcBase[] execute() {
		if (logger.isDebugEnabled()) {
			logger.debug("Processing WhoIs svc");
		}
		Vector<SvcBase> responses = new Vector<SvcBase>();
		MPDUHeader respHeader;
		try {
			respHeader = RoutingService.createResponseHeader(header);
		} catch (RouteNotFoundException e) {
			logger.warn("Malformed header", e);
			throw new SvcException(e.getLocalizedMessage());
		} catch (MalformedHeaderException e) {
			logger.warn("Malformed header", e);
			throw new SvcException(e.getLocalizedMessage());
		}

		if (header.DNET == Constants.BAC_NET_NET_BROADCAST || header.DNET == Constants.PROPERTY_NOT_SPECIFIED) {
			Collection<BACnetNetwork> networks = BACnetSystem.getInstance().getNetworks();
			for (BACnetNetwork nwk : networks) {
				processNetwork(nwk, responses, respHeader);
			}
		} else {
			BACnetNetwork net = BACnetSystem.getInstance().getNetwork(header.DNET);
			if (net != null) {
				processNetwork(net, responses, respHeader);
			}
		}

		SvcBase[] result = new SvcBase[responses.size()];
		responses.copyInto(result);
		return result;
	}

	public AbstractSession createSession() {
		return new UnconfirmedSession(this);
	}

	@Override
	public AbstractSession createSession(int numExpectedPackets) {
		return new UnconfirmedSession(this, numExpectedPackets);
	}

	@Override
	public APDUBasePacket getPacket() {
		if (header == null)
			throw new SvcException("Unable to create APDU packet");
		header.Control = true;
		if (header.DST_MAC == null) {
			try {
				header.DST_MAC = MPDUHeader.getCopy(RoutingService.discoverRoute(header.DNET).getDST_MAC());
			} catch (RouteNotFoundException e) {
				throw new SvcException(e.getLocalizedMessage());
			}
		}

		// TODO add routing for populating header; it should be made for
		// requests because only request can be sent by stack
		UnconfirmedRequestPDU packet = new UnconfirmedRequestPDU(header);
		if (encodedBuffer == null) {
			encodedBuffer = encodeBuffer();
		}
		packet.setBuffer(encodedBuffer);
		packet.setChoise(UnconfirmedServiceChoice.who_Is);
		return packet;
	}

	protected void processNetwork(BACnetNetwork network, Vector<SvcBase> responses, MPDUHeader respHeader) {
		Collection<Device> devs = network.getDevices();
		for (Device dev_ : devs) {
			BACnetDeviceWrapper dev = (BACnetDeviceWrapper) dev_;
			if (deviceInstanceRangeLowLimit == Constants.PROPERTY_NOT_SPECIFIED
			        && deviceInstanceRangeHighLimit == Constants.PROPERTY_NOT_SPECIFIED) {
				SvcBase response = SvcFactory.getInstance().getResponseForRequest(this);

				respHeader.SNET = ((BACnetAddressBinding) dev.getWrappee().getDeviceAddressBindings().toArray()[0]).deviceAddress.networkNumber;
				respHeader.SADR = MPDUHeader.getCopy(((BACnetAddressBinding) dev.getWrappee()
				        .getDeviceAddressBindings().toArray()[0]).deviceAddress.macAddress);
				respHeader.SLEN = respHeader.SADR.length;

				response.setHeader(respHeader);
				response.setMax_APDU_length_accepted(BACnetAPDUSize.valueOfByLong(dev.getWrappee()
				        .getMaxAPDULengthAccepted()));
				((IAm) response).setSegmentationSupported((int) dev.getWrappee().getSegmentationSupported());
				((IAm) response).setVendorID(dev.getWrappee().getVendorIdentifier());

				((IAm) response).setIAmDeviceIdentifier(dev.getWrappee().getObjectIdentifier());

				responses.add(response);
			} else {
				for (BACnetAddressBinding bab : dev.getWrappee().getDeviceAddressBindings()) {
					if (bab.deviceObjectIdentifier.getInstanceNumber() >= deviceInstanceRangeLowLimit
					        && bab.deviceObjectIdentifier.getInstanceNumber() <= deviceInstanceRangeHighLimit) {
						SvcBase response = SvcFactory.getInstance().getResponseForRequest(this);
						respHeader.SNET = ((BACnetAddressBinding) dev.getWrappee().getDeviceAddressBindings().toArray()[0]).deviceAddress.networkNumber;
						respHeader.SADR = MPDUHeader.getCopy(((BACnetAddressBinding) dev.getWrappee()
						        .getDeviceAddressBindings().toArray()[0]).deviceAddress.macAddress);
						respHeader.SLEN = respHeader.SADR.length;
						response.setHeader(respHeader);
						response.setMax_APDU_length_accepted(BACnetAPDUSize.valueOfByLong(dev.getWrappee()
						        .getMaxAPDULengthAccepted()));
						((IAm) response).setSegmentationSupported((int) dev.getWrappee().getSegmentationSupported());
						((IAm) response).setVendorID(dev.getWrappee().getVendorIdentifier());
						((IAm) response).setIAmDeviceIdentifier(dev.getWrappee().getObjectIdentifier());
						responses.add(response);
					}
				}
			}

		}

	}

	public int getDeviceInstanceRangeLowLimit() {
		return deviceInstanceRangeLowLimit;
	}

	public void setDeviceInstanceRangeLowLimit(int deviceInstanceRangeLowLimit) {
		this.deviceInstanceRangeLowLimit = deviceInstanceRangeLowLimit;
	}

	public int getDeviceInstanceRangeHighLimit() {
		return deviceInstanceRangeHighLimit;
	}

	public void setDeviceInstanceRangeHighLimit(int deviceInstanceRangeHighLimit) {
		this.deviceInstanceRangeHighLimit = deviceInstanceRangeHighLimit;
	}
}
