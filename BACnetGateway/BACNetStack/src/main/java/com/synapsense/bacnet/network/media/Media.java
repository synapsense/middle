package com.synapsense.bacnet.network.media;

import com.synapsense.bacnet.network.jni.MalformedPacketException;
import com.synapsense.bacnet.network.jni.Property;
import com.synapsense.bacnet.network.jni.PropertyNotFoundException;
import com.synapsense.bacnet.network.jni.UnableToSendException;
import com.synapsense.bacnet.network.jni.UnableToSetUpInterfaceException;

public interface Media {

	/**
	 * Reads source address from media
	 * 
	 * @param packetID
	 *            id of packet
	 * @return source address
	 */
	public byte[] readSRC(int packetID);

	/**
	 * Reads destination address from media
	 * 
	 * @param packetID
	 *            id of packet
	 * @return destination address
	 */
	public byte[] readDST(int packetID);

	/**
	 * Writes source address to media
	 * 
	 * @param packetID
	 *            id of packet
	 * @param SRC_ADDR
	 *            source address
	 */
	public void writeSRC(int packetID, byte[] SRC_ADDR);

	/**
	 * Writes destination address to media
	 * 
	 * @param packetID
	 *            id of packet
	 * @param DST_ADDR
	 *            source address
	 */
	public void writeDST(int packetID, byte[] DST_ADDR);

	/**
	 * Checks destination address for broadcast mark
	 * 
	 * @param s
	 *            MAC address
	 * @return true if s is Broadcast
	 */
	public boolean isBroadcast(byte[] s);

	/**
	 * Gets broadcast address for media type
	 * 
	 * @return broadcast address if media
	 */
	public byte[] getBroadcast();

	/**
	 * Gets Media address for device with number
	 * 
	 * @param number
	 *            number of device
	 * @return Media address
	 */
	public byte[] getMediaAddress(int number);

	/**
	 * Gets active network interface MAC
	 * 
	 * @return
	 */
	public byte[] getActiveEthMac();

	public MediaNames getMediaType();

	/**
	 * Packet reader interface
	 */

	public int readInt(int packetID, Property prop) throws PropertyNotFoundException;

	public byte readByte(int packetID, Property prop) throws PropertyNotFoundException;

	/**
	 * Method is responsible for read property item from network
	 * 
	 * @param prop
	 *            Property name
	 * @return Property value
	 */
	public double readDouble(int packetID, Property prop) throws PropertyNotFoundException;

	/**
	 * Method is responsible for read property item from network
	 * 
	 * @param prop
	 *            Property name
	 * @return Property value
	 */
	public String readString(int packetID, Property prop) throws PropertyNotFoundException;

	/**
	 * Method is responsible for read property item from network
	 * 
	 * @param prop
	 *            Property name
	 * @return Property value
	 */
	public boolean readBoolean(int packetID, Property prop) throws PropertyNotFoundException;

	public short readShort(int packetID, Property prop) throws PropertyNotFoundException;

	public void setBoolean(int packetID, Property prop, boolean value) throws PropertyNotFoundException;

	public void setInt(int packetID, Property prop, int value) throws PropertyNotFoundException;

	public void setString(int packetID, Property prop, String value) throws PropertyNotFoundException;

	public void setDouble(int packetID, Property prop, double value) throws PropertyNotFoundException;

	public byte[] readByteArray(int packetID, Property prop) throws PropertyNotFoundException;

	public void setByteArray(int packetID, Property prop, byte[] value) throws PropertyNotFoundException;

	public void setShort(int packetID, Property prop, short value) throws PropertyNotFoundException;

	public int[] readShortArrayAsInt(int packetID, Property prop) throws PropertyNotFoundException;

	public short[] readShortArray(int packetID, Property prop) throws PropertyNotFoundException;

	public void setShortArrayAsInt(int packetID, Property prop, int[] value) throws PropertyNotFoundException;

	public void setByte(int packetID, Property prop, byte value) throws PropertyNotFoundException;

	public String[] readStringArray(int packetID, Property prop) throws PropertyNotFoundException;

	public void setStringArray(int packetId, Property prop, String[] routeInfo) throws PropertyNotFoundException;

	public int createPacket();

	public void sendPacket(int packetID) throws UnableToSendException, PropertyNotFoundException,
	        MalformedPacketException, UnableToSetUpInterfaceException;

	public int startRead();

	public void readDone(int packetID);

	public void startCapture(int number, boolean isDebug) throws UnableToSetUpInterfaceException;

	public void stopCapture();

	public void setLogConfigPath(String string);
}
