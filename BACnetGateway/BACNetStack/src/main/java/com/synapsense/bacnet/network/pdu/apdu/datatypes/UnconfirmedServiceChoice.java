package com.synapsense.bacnet.network.pdu.apdu.datatypes;

/**
 * 
 * @author Alexander Borisov BACnetUnconfirmedServiceChoice ::= ENUMERATED {
 *         i-Am (0), i-Have (1), unconfirmedCOVNotification (2),
 *         unconfirmedEventNotification (3), unconfirmedPrivateTransfer (4),
 *         unconfirmedTextMessage (5), timeSynchronization (6), who-Has (7),
 *         who-Is (8), utcTimeSynchronization (9) } -- Other services to be
 *         added as they are defined. All choice values in this production are
 *         reserved for definition by -- ASHRAE. Proprietary extensions are made
 *         by using the UnconfirmedPrivateTransfer service.
 */
public enum UnconfirmedServiceChoice {
i_Am((byte) 0), i_Have((byte) 1), unconfirmedCOVNotification((byte) 2), unconfirmedEventNotification((byte) 3), unconfirmedPrivateTransfer(
        (byte) 4), unconfirmedTextMessage((byte) 5), timeSynchronization((byte) 6), who_Has((byte) 7), who_Is((byte) 8), utcTimeSynchronization(
        (byte) 9);

private final byte type;

public byte getType() {
	return type;
}

UnconfirmedServiceChoice(byte type) {
	this.type = type;
}

public static UnconfirmedServiceChoice valueOf(byte b) {
	switch (b) {
	case 0:
		return i_Am;
	case 1:
		return i_Have;
	case 2:
		return unconfirmedCOVNotification;
	case 3:
		return unconfirmedEventNotification;
	case 4:
		return unconfirmedPrivateTransfer;
	case 5:
		return unconfirmedTextMessage;
	case 6:
		return timeSynchronization;
	case 7:
		return who_Has;
	case 8:
		return who_Is;
	case 9:
		return utcTimeSynchronization;
	default:
		throw new IllegalArgumentException("Unable to identify enum type");
	}
}

}
