/**
 * 
 */
package com.synapsense.bacnet.network.pdu.npdu.request;

import com.synapsense.bacnet.network.Configuration;
import com.synapsense.bacnet.network.jni.BACNetException;
import com.synapsense.bacnet.network.jni.Property;
import com.synapsense.bacnet.network.jni.PropertyNotFoundException;
import com.synapsense.bacnet.network.media.MPDUHeader;
import com.synapsense.bacnet.network.pdu.npdu.NPDUBasePacket;
import com.synapsense.bacnet.network.pdu.npdu.NPDUMessageType;
import com.synapsense.bacnet.network.pdu.npdu.response.RejectMessageToNetwork;
import com.synapsense.bacnet.network.routing.RoutingService;
import com.synapsense.scheduler.SimpleTaskUnit;

/**
 * @author Alexander Borisov This massage is used to instruct a half-router to
 *         establish a new PTP connection that creates a path to the indicated
 *         This message is indicated by a Message Type of X'08'. The complete
 *         format of the NPDU is shown in Figure octet network number indicates
 *         the DNET that should be connected to by this half-router. The 1-octet
 *         "TerminationValue" specifies the time, in seconds, that the
 *         connection shall remain established in the absence of NPDUs
 *         connection. A value of 0 indicates that the connection should be
 *         considered to be permanent. See 6.7.1.4.
 */
public class EstablishConnectionToNetwork extends NPDUBasePacket implements SimpleTaskUnit {
	private int DNET;
	private int TerminationTimeValue;

	public EstablishConnectionToNetwork(MPDUHeader header) {
		this.header = header;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsense.scheduler.SimpleTaskUnit#doTask()
	 */
	@Override
	public void Execute() throws Exception {
		throw new UnsupportedOperationException("Method is not supported");
	}

	@Override
	public void receive() throws PropertyNotFoundException {
		DNET = header.media.readShort(header.packetId, Property.NDNET) & 0xFFFF;
		TerminationTimeValue = header.media.readByte(header.packetId, Property.TERMINATION_TIME_VALUE) & 0xFF;
	}

	@Override
	public void send() throws BACNetException {
		header.send();
		logger.debug("sending " + this.toString());

		header.media.setByte(header.packetId, Property.MESSAGE_TYPE,
		        (byte) NPDUMessageType.Establish_Connection_To_Network.getCode());
		header.media.setShort(header.packetId, Property.NDNET, (short) DNET);
		header.media.setByte(header.packetId, Property.TERMINATION_TIME_VALUE, (byte) TerminationTimeValue);
		header.media.sendPacket(header.packetId);
	}

	@Override
	public void failureAction(Throwable t) {
		try {
			new RejectMessageToNetwork(RoutingService.getDefaultResponseHeader(header), 0, Configuration.OWN_NET)
			        .send();
		} catch (Exception e) {
			logger.warn("Unable to send message", e);
		}
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer(10);
		sb.append(this.getClass().getName());
		sb.append(header);
		sb.append("\tDNET=");
		sb.append(DNET);
		sb.append("\n");
		sb.append("\tTerminationTimeValue=");
		sb.append(TerminationTimeValue);
		sb.append("\n");
		return sb.toString();
	}

	public int getDNET() {
		return DNET;
	}

	public void setDNET(int dNET) {
		DNET = dNET;
	}

	public int getTerminationTimeValue() {
		return TerminationTimeValue;
	}

	public void setTerminationTimeValue(int terminationTimeValue) {
		TerminationTimeValue = terminationTimeValue;
	}

}
