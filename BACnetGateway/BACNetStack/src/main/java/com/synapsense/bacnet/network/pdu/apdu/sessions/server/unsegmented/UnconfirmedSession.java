package com.synapsense.bacnet.network.pdu.apdu.sessions.server.unsegmented;

import com.synapsense.bacnet.network.pdu.apdu.APDUBasePacket;
import com.synapsense.bacnet.network.pdu.apdu.sessions.AbstractSession;
import com.synapsense.bacnet.network.pdu.apdu.sessions.SessionManager;
import com.synapsense.bacnet.network.pdu.apdu.svc.SvcBase;
import com.synapsense.bacnet.network.pdu.apdu.svc.SvcFactory;
import com.synapsense.scheduler.TimeoutTaskUnit;

/**
 * 
 * @author Alexander Borisov
 * 
 *         IDLE UnconfirmedReceived If a BACnet-Unconfirmed-Request-PDU is
 *         received from the network layer, then send an UNCONF_SERV.indication
 *         to the local application program, and enter the IDLE state.
 * 
 *         AbortPDU_Received If a BACnet-Abort-PDU whose 'server' parameter is
 *         FALSE is received from the network layer, then enter the IDLE state.
 * 
 *         UnexpectedPDU_Received If an unexpected PDU
 *         (BACnet-Confirmed-Request-PDU with 'segmented-message' = TRUE and
 *         'sequencenumber' not equal to zero or BACnet-SegmentACK-PDU with
 *         'server' = FALSE) is received from the network layer, then issue an
 *         N-UNITDATA.request with 'data_expecting_reply' = FALSE to transmit a
 *         BACnet-Abort-PDU with 'server' = TRUE; and enter the IDLE state.
 * 
 */

public class UnconfirmedSession extends AbstractSession implements TimeoutTaskUnit {

	public UnconfirmedSession(APDUBasePacket packet) {
		super(packet);
		//timeout = Integer.parseInt(System.getProperty("UnconfirmedSession.timeout", "-1"));
		// TODO Auto-generated constructor stub
	}

	@Override
	public void addMessageToSession(APDUBasePacket packet) {
	}

	@Override
	public void timeoutAction() {
		obj = null;
		logger.warn("Timeout for session\n" + this.toString());
		SessionManager.getInstance().removeSession(this);
	}

	@Override
	public void Execute() throws Exception {
		SvcFactory sf = SvcFactory.getInstance();
		SvcBase svc = sf.getUnConfirmedSvc(packet);
		if (logger.isDebugEnabled()) {
			logger.debug("Processing " + svc);
		}
		svc.decodeBuffer(packet.getBuffer());
		svc.setHeader(packet.getHeader());
		try {
			SvcBase results[];
			results = svc.execute();
			if (obj == null)
				return;
			if (results != null) {
				for (SvcBase result : results) {
					if (obj == null)
						return;
					result.getPacket().send();
					if (obj == null)
						return;
				}
			} else {
				logger.info("No reply received from the application layer, skipping.");
			}
		} catch (Exception e) {
			logger.warn("Svc execution error", e);
		}
		SessionManager.getInstance().removeSession(this);
	}

	@Override
	public void failureAction(Throwable t) {
        logger.warn("Session error", t);
        if (packet != null) {
            logger.warn("Packet details: " + packet.toString());
        }
		SessionManager.getInstance().removeSession(this);
	}

}
