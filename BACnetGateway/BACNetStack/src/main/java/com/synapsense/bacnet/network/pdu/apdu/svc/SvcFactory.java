package com.synapsense.bacnet.network.pdu.apdu.svc;

import java.lang.reflect.InvocationTargetException;
import java.util.Hashtable;

import org.apache.log4j.Logger;

import com.synapsense.bacnet.network.pdu.apdu.APDUBasePacket;
import com.synapsense.bacnet.network.pdu.apdu.datatypes.ConfirmedServiceACK;
import com.synapsense.bacnet.network.pdu.apdu.datatypes.ConfirmedServiceChoice;
import com.synapsense.bacnet.network.pdu.apdu.datatypes.UnconfirmedServiceChoice;
import com.synapsense.bacnet.network.pdu.apdu.request.ConfirmedRequestPDU;
import com.synapsense.bacnet.network.pdu.apdu.request.UnconfirmedRequestPDU;
import com.synapsense.bacnet.network.pdu.apdu.response.ComplexACKPDU;
import com.synapsense.bacnet.network.pdu.apdu.response.SimpleACKPDU;
import com.synapsense.bacnet.network.pdu.apdu.svc.request.ConfirmedCOVNotification;
import com.synapsense.bacnet.network.pdu.apdu.svc.request.ReadProperty;
import com.synapsense.bacnet.network.pdu.apdu.svc.request.ReadPropertyMultiple;
import com.synapsense.bacnet.network.pdu.apdu.svc.request.SubscribeCOVProperty;
import com.synapsense.bacnet.network.pdu.apdu.svc.request.TimeSynchronization;
import com.synapsense.bacnet.network.pdu.apdu.svc.request.UTCTimeSynchronization;
import com.synapsense.bacnet.network.pdu.apdu.svc.request.UnconfirmedCOVNotification;
import com.synapsense.bacnet.network.pdu.apdu.svc.request.WhoHas;
import com.synapsense.bacnet.network.pdu.apdu.svc.request.WhoIs;
import com.synapsense.bacnet.network.pdu.apdu.svc.request.WriteProperty;
import com.synapsense.bacnet.network.pdu.apdu.svc.responce.ConfirmedCOVNotificationAck;
import com.synapsense.bacnet.network.pdu.apdu.svc.responce.IAm;
import com.synapsense.bacnet.network.pdu.apdu.svc.responce.IHave;
import com.synapsense.bacnet.network.pdu.apdu.svc.responce.ReadPropertyAck;
import com.synapsense.bacnet.network.pdu.apdu.svc.responce.ReadPropertyMultipleAck;
import com.synapsense.bacnet.network.pdu.apdu.svc.responce.SubscribeCOVPropertyAck;
import com.synapsense.bacnet.network.pdu.apdu.svc.responce.WritePropertyAck;

/**
 * This class is a service storage for all services which is supported by
 * default our stack
 * 
 * @author Alexander Borisov
 * 
 */
public final class SvcFactory {
	/**
	 * Mapping choice -> confirmed Service Mapping choice -> unconfirmed Service
	 * Mapping choice -> ACK
	 */
	protected Hashtable<ConfirmedServiceChoice, Class<? extends SvcBase>> confirmedSvc;
	protected Hashtable<UnconfirmedServiceChoice, Class<? extends SvcBase>> unconfirmedSvc;
	protected Hashtable<ConfirmedServiceACK, Class<? extends SvcBase>> confirmedSvcAck;
	protected Hashtable<ConfirmedServiceChoice, Class<? extends SvcBase>> confirmedSvcSimpleAck;

	/**
	 * Mapping request - response model
	 */
	protected Hashtable<Class<? extends SvcBase>, Class<? extends SvcBase>> svcPair;

	private static Logger logger = Logger.getLogger(SvcFactory.class);
	private static SvcFactory instance;

	/**
	 * Creates
	 */
	private SvcFactory() {
		confirmedSvc = new Hashtable<ConfirmedServiceChoice, Class<? extends SvcBase>>();
		unconfirmedSvc = new Hashtable<UnconfirmedServiceChoice, Class<? extends SvcBase>>();
		confirmedSvcAck = new Hashtable<ConfirmedServiceACK, Class<? extends SvcBase>>();
		confirmedSvcSimpleAck = new Hashtable<ConfirmedServiceChoice, Class<? extends SvcBase>>();
		svcPair = new Hashtable<Class<? extends SvcBase>, Class<? extends SvcBase>>();
		init();
	}

	static {
		instance = new SvcFactory();
	}

	protected void init() {
		// Confirmed sessions
		registerConfirmedSVC(ConfirmedServiceChoice.readProperty, ReadProperty.class);
		registerConfirmedSVC(ConfirmedServiceChoice.readPropertyMultiple, ReadPropertyMultiple.class);
		registerConfirmedSVC(ConfirmedServiceChoice.writeProperty, WriteProperty.class);
		registerConfirmedSVC(ConfirmedServiceChoice.subscribeCOVProperty, SubscribeCOVProperty.class);
		registerConfirmedSVC(ConfirmedServiceChoice.confirmedCOVNotification, ConfirmedCOVNotification.class);

		registerConfirmedSVCSimple(ConfirmedServiceChoice.writeProperty, WritePropertyAck.class);
		registerConfirmedSVCSimple(ConfirmedServiceChoice.subscribeCOVProperty, SubscribeCOVPropertyAck.class);

		// Unconfirmed sessions
		registerUnConfirmedSVC(UnconfirmedServiceChoice.i_Am, IAm.class);
		registerUnConfirmedSVC(UnconfirmedServiceChoice.i_Have, IHave.class);
		registerUnConfirmedSVC(UnconfirmedServiceChoice.who_Has, WhoHas.class);
		registerUnConfirmedSVC(UnconfirmedServiceChoice.who_Is, WhoIs.class);
		registerUnConfirmedSVC(UnconfirmedServiceChoice.timeSynchronization, TimeSynchronization.class);
		registerUnConfirmedSVC(UnconfirmedServiceChoice.utcTimeSynchronization, UTCTimeSynchronization.class);
		registerUnConfirmedSVC(UnconfirmedServiceChoice.unconfirmedCOVNotification, UnconfirmedCOVNotification.class);

		// Register ACK for svc
		registerConfirmedACKforSVC(ConfirmedServiceACK.readProperty, ReadPropertyAck.class);
		registerConfirmedACKforSVC(ConfirmedServiceACK.readPropertyMultiple, ReadPropertyMultipleAck.class);

		// request -> response svc model
		registerResponseForRequest(WhoHas.class, IHave.class);
		registerResponseForRequest(WhoIs.class, IAm.class);
		registerResponseForRequest(ReadProperty.class, ReadPropertyAck.class);
		registerResponseForRequest(ReadPropertyMultiple.class, ReadPropertyMultipleAck.class);
		registerResponseForRequest(WriteProperty.class, WritePropertyAck.class);
		registerResponseForRequest(SubscribeCOVProperty.class, SubscribeCOVPropertyAck.class);
		registerResponseForRequest(ConfirmedCOVNotification.class, ConfirmedCOVNotificationAck.class);
	}

	/**
	 * Gets instance of svc factory
	 * 
	 * @return instance of svc factory
	 */
	public static SvcFactory getInstance() {
		return instance;
	}

	/**
	 * Registers confirmed service to factory
	 * 
	 * @param choise
	 *            confirmed service from ConfirmedServiceChoice enum
	 * @param svc
	 *            confirmed service
	 */
	public void registerConfirmedSVC(ConfirmedServiceChoice choise, Class<? extends SvcBase> svc) {
		if (logger.isDebugEnabled()) {
			logger.debug("Registering [" + svc + "] for [" + choise + "]");
		}
		confirmedSvc.put(choise, svc);
	}

	public void registerConfirmedSVCSimple(ConfirmedServiceChoice choise, Class<? extends SvcBase> svc) {
		if (logger.isDebugEnabled()) {
			logger.debug("Registering [" + svc + "] for [" + choise + "]");
		}
		confirmedSvcSimpleAck.put(choise, svc);
	}

	public void registerConfirmedACKforSVC(ConfirmedServiceACK choise, Class<? extends SvcBase> svc) {
		if (logger.isDebugEnabled()) {
			logger.debug("Registering [" + svc + "] for [" + choise + "]");
		}
		confirmedSvcAck.put(choise, svc);
	}

	public void registerResponseForRequest(Class<? extends SvcBase> request, Class<? extends SvcBase> response) {
		if (logger.isDebugEnabled()) {
			logger.debug("Registering [" + response + "] for [" + request + "]");
		}
		svcPair.put(request, response);
	}

	/**
	 * Registers unconfirmed service to factory
	 * 
	 * @param choise
	 *            unconfirmed service from UnconfirmedServiceChoice enum
	 * @param svc
	 *            unconfirmed service
	 */
	public void registerUnConfirmedSVC(UnconfirmedServiceChoice choise, Class<? extends SvcBase> svc) {
		if (logger.isDebugEnabled()) {
			logger.debug("Registering [" + svc + "] for [" + choise + "]");
		}
		unconfirmedSvc.put(choise, svc);
	}

	/**
	 * Gets service by ConfirmedServiceChoice identifier
	 * 
	 * @param key
	 *            ConfirmedServiceChoice enum
	 * @return confirmed service of null if there are no service for selected
	 *         code
	 */
	public SvcBase getConfirmedSvc(APDUBasePacket packet) {
		Class<? extends SvcBase> c = confirmedSvc.get(((ConfirmedRequestPDU) packet).getService_choice());
		if (c == null)
			return null;
		SvcBase svc = null;
		try {
			svc = c.getConstructor(new Class[] { APDUBasePacket.class }).newInstance(packet);
		} catch (IllegalArgumentException e) {
			logger.warn("Unable to create instance for [" + packet.getClass() + "]", e);
		} catch (SecurityException e) {
			logger.warn("Unable to create instance for [" + packet.getClass() + "]", e);
		} catch (InstantiationException e) {
			logger.warn("Unable to create instance for [" + packet.getClass() + "]", e);
		} catch (IllegalAccessException e) {
			logger.warn("Unable to create instance for [" + packet.getClass() + "]", e);
		} catch (InvocationTargetException e) {
			logger.warn("Unable to create instance for [" + packet.getClass() + "]", e);
		} catch (NoSuchMethodException e) {
			logger.warn("Unable to create instance for [" + packet.getClass() + "]", e);
		}
		return svc;
	}

	public SvcBase getConfirmedSvcSimple(APDUBasePacket packet) {
		Class<? extends SvcBase> c = confirmedSvcSimpleAck.get(((SimpleACKPDU) packet).getService_ACK_choice());
		if (c == null)
			return null;
		SvcBase svc = null;
		try {
			svc = c.getConstructor(new Class[] { APDUBasePacket.class }).newInstance(packet);
		} catch (IllegalArgumentException e) {
			logger.warn("Unable to create instance for [" + packet.getClass() + "]", e);
		} catch (SecurityException e) {
			logger.warn("Unable to create instance for [" + packet.getClass() + "]", e);
		} catch (InstantiationException e) {
			logger.warn("Unable to create instance for [" + packet.getClass() + "]", e);
		} catch (IllegalAccessException e) {
			logger.warn("Unable to create instance for [" + packet.getClass() + "]", e);
		} catch (InvocationTargetException e) {
			logger.warn("Unable to create instance for [" + packet.getClass() + "]", e);
		} catch (NoSuchMethodException e) {
			logger.warn("Unable to create instance for [" + packet.getClass() + "]", e);
		}
		return svc;
	}

	/**
	 * Gets service by UnconfirmedServiceChoice identifier
	 * 
	 * @param packet
	 *            UnconfirmedServiceChoice enum
	 * @return confirmed service of null if there are no service for selected
	 *         code
	 */
	public SvcBase getUnConfirmedSvc(APDUBasePacket packet) {
		Class<? extends SvcBase> c = unconfirmedSvc.get(((UnconfirmedRequestPDU) packet).getSVCChoise());
		if (c == null)
			return null;
		SvcBase svc = null;
		try {
			svc = c.getConstructor(new Class[] { APDUBasePacket.class }).newInstance(packet);
		} catch (IllegalArgumentException e) {
			logger.warn("Unable to create instance for [" + packet.getClass() + "]", e);
		} catch (SecurityException e) {
			logger.warn("Unable to create instance for [" + packet.getClass() + "]", e);
		} catch (InstantiationException e) {
			logger.warn("Unable to create instance for [" + packet.getClass() + "]", e);
		} catch (IllegalAccessException e) {
			logger.warn("Unable to create instance for [" + packet.getClass() + "]", e);
		} catch (InvocationTargetException e) {
			logger.warn("Unable to create instance for [" + packet.getClass() + "]", e);
		} catch (NoSuchMethodException e) {
			logger.warn("Unable to create instance for [" + packet.getClass() + "]", e);
		}
		return svc;
	}

	/**
	 * Gets service by ConfirmedServiceACK identifier
	 * 
	 * @param key
	 *            ConfirmedServiceACK enum
	 * @return confirmed service of null if there are no service for selected
	 *         code
	 */
	public SvcBase getConfirmedACK(APDUBasePacket packet) {
		Class<? extends SvcBase> c = confirmedSvcAck.get(((ComplexACKPDU) packet).getService_ACK_choice());
		if (c == null)
			return null;
		SvcBase svc = null;
		try {
			svc = c.getConstructor(new Class[] { APDUBasePacket.class }).newInstance(packet);
		} catch (IllegalArgumentException e) {
			logger.warn("Unable to create instance for [" + packet.getClass() + "]", e);
		} catch (SecurityException e) {
			logger.warn("Unable to create instance for [" + packet.getClass() + "]", e);
		} catch (InstantiationException e) {
			logger.warn("Unable to create instance for [" + packet.getClass() + "]", e);
		} catch (IllegalAccessException e) {
			logger.warn("Unable to create instance for [" + packet.getClass() + "]", e);
		} catch (InvocationTargetException e) {
			logger.warn("Unable to create instance for [" + packet.getClass() + "]", e);
		} catch (NoSuchMethodException e) {
			logger.warn("Unable to create instance for [" + packet.getClass() + "]", e);
		}
		return svc;
	}

	public SvcBase getResponseForRequest(SvcBase request) {
		if (logger.isDebugEnabled()) {
			logger.debug("Getting response for [" + request.getClass() + "]");
		}
		Class<? extends SvcBase> c = svcPair.get(request.getClass());

		if (logger.isDebugEnabled()) {
			logger.debug("Result is  [" + c + "]");
		}
		if (c == null)
			return null;
		SvcBase svc = null;
		try {
			svc = c.getConstructor(new Class[] { SvcBase.class }).newInstance(request);
		} catch (IllegalArgumentException e) {
			logger.warn("Unable to create instance for [" + request.getClass() + "]", e);
		} catch (SecurityException e) {
			logger.warn("Unable to create instance for [" + request.getClass() + "]", e);
		} catch (InstantiationException e) {
			logger.warn("Unable to create instance for [" + request.getClass() + "]", e);
		} catch (IllegalAccessException e) {
			logger.warn("Unable to create instance for [" + request.getClass() + "]", e);
		} catch (InvocationTargetException e) {
			logger.warn("Unable to create instance for [" + request.getClass() + "]", e);
		} catch (NoSuchMethodException e) {
			logger.warn("Unable to create instance for [" + request.getClass() + "]", e);
		}
		return svc;
	}

}
