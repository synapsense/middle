package com.synapsense.bacnet.network.pdu.apdu.response;

import com.synapsense.bacnet.network.Constants;
import com.synapsense.bacnet.network.jni.BACNetException;
import com.synapsense.bacnet.network.jni.Property;
import com.synapsense.bacnet.network.jni.PropertyNotFoundException;
import com.synapsense.bacnet.network.media.MPDUHeader;
import com.synapsense.bacnet.network.media.Media;
import com.synapsense.bacnet.network.pdu.apdu.APDUBasePacket;
import com.synapsense.bacnet.network.pdu.apdu.APDUMessageType;
import com.synapsense.bacnet.network.pdu.apdu.datatypes.ConfirmedServiceACK;

/**
 * 
 * @author Alexander Borisov </br> BACnet-ComplexACK-PDU ::= SEQUENCE {</br>
 *         pdu-type [0] Unsigned (0..15), -- 3 for this PDU type</br>
 *         segmented-message [1] BOOLEAN,</br> more-follows [2] BOOLEAN,</br>
 *         reserved [3] Unsigned (0..3), -- must be set to zero</br> invokeID
 *         [4] Unsigned (0..255),</br> sequence-number [5] Unsigned (0..255)
 *         OPTIONAL, --only if segment</br> proposed-window-size [6] Unsigned
 *         (1..127) OPTIONAL, -- only if segment</br> service-ACK-choice [7]
 *         BACnetConfirmedServiceChoice,</br> service-ACK [8]
 *         BACnet-Confirmed-Service-ACK</br> -- Context-specific tags 0..8 are
 *         NOT used in header encoding}</br>
 */
public class ComplexACKPDU extends APDUBasePacket {
	boolean segmented_message;// [1] BOOLEAN,
	boolean more_follows;// [2] BOOLEAN,
	int sequence_number;// [5] Unsigned (0..255) OPTIONAL, --only if segment
	int proposed_window_size;// [6] Unsigned (1..127) OPTIONAL, -- only if
	                         // segment
	ConfirmedServiceACK service_ACK_choice;// [7]
	                                       // BACnetConfirmedServiceChoice,
	                                       // ConfirmedServiceACK service_ACK;//
										   // [8] BACnet-Confirmed-Service-ACK

	public ComplexACKPDU(MPDUHeader header) {
		this.header = header;
		type = APDUMessageType.complexACK_PDU;
	}

	public boolean isSegmented_message() {
		return segmented_message;
	}

	public boolean isMore_follows() {
		return more_follows;
	}

	public int getSequence_number() {
		return sequence_number;
	}

	public int getProposed_window_size() {
		return proposed_window_size;
	}

	public ConfirmedServiceACK getService_ACK_choice() {
		return service_ACK_choice;
	}

	public void setSegmented_message(boolean segmented_message) {
		this.segmented_message = segmented_message;
	}

	public void setMore_follows(boolean more_follows) {
		this.more_follows = more_follows;
	}

	public void setSequence_number(int sequence_number) {
		this.sequence_number = sequence_number;
	}

	public void setProposed_window_size(int proposed_window_size) {
		this.proposed_window_size = proposed_window_size;
	}

	public void setService_ACK_choice(ConfirmedServiceACK service_ACK_choice) {
		this.service_ACK_choice = service_ACK_choice;
	}

	@Override
	public void receive() throws PropertyNotFoundException {
		Media reader = header.media;
		invokeID = reader.readByte(header.packetId, Property.INVOKE_ID) & 0xFF;
		segmented_message = reader.readBoolean(header.packetId, Property.IS_SEG_REQUEST);
		more_follows = reader.readBoolean(header.packetId, Property.MORE_SEG_FOLLOW);
		if (segmented_message == true) {
			sequence_number = reader.readByte(header.packetId, Property.SEQUENCE_NUMBER);
			proposed_window_size = reader.readByte(header.packetId, Property.WINDOW_SIZE);
		} else {
			sequence_number = Constants.PROPERTY_NOT_SPECIFIED;
			proposed_window_size = Constants.PROPERTY_NOT_SPECIFIED;
		}
		service_ACK_choice = ConfirmedServiceACK.valueOf(reader.readByte(header.packetId, Property.SERV_CHOICE) & 0xFF);
		buffer = reader.readByteArray(header.packetId, Property.PDU_PAYLOAD);
	}

	@Override
	public void send() throws BACNetException {
		Media reader = header.media;
		header.send();
		reader.setBoolean(header.packetId, Property.IS_SEG_REQUEST, segmented_message);
		reader.setBoolean(header.packetId, Property.MORE_SEG_FOLLOW, more_follows);
		if (segmented_message == true) {
			reader.setByte(header.packetId, Property.SEQUENCE_NUMBER, (byte) sequence_number);
			reader.setByte(header.packetId, Property.WINDOW_SIZE, (byte) proposed_window_size);
		}
		reader.setByte(header.packetId, Property.MESSAGE_TYPE, type.getType());
		reader.setByte(header.packetId, Property.SERV_CHOICE, (byte) service_ACK_choice.getCode());
		reader.setByteArray(header.packetId, Property.PDU_PAYLOAD, buffer);
		reader.setByte(header.packetId, Property.INVOKE_ID, (byte) invokeID);
		reader.sendPacket(header.packetId);
	}

	@Override
	public boolean isSegmented() {
		return segmented_message;
	}

}
