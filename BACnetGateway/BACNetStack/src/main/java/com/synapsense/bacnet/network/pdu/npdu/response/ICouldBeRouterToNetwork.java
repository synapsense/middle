/**
 * 
 */
package com.synapsense.bacnet.network.pdu.npdu.response;

import com.synapsense.bacnet.network.Configuration;
import com.synapsense.bacnet.network.jni.BACNetException;
import com.synapsense.bacnet.network.jni.Property;
import com.synapsense.bacnet.network.jni.PropertyNotFoundException;
import com.synapsense.bacnet.network.media.MPDUHeader;
import com.synapsense.bacnet.network.pdu.npdu.NPDUBasePacket;
import com.synapsense.bacnet.network.pdu.npdu.NPDUMessageType;
import com.synapsense.bacnet.network.routing.RoutingService;
import com.synapsense.scheduler.SimpleTaskUnit;

/**
 * @author Alexander Borisov This message is used to respond to a
 *         Who-Is-Router-To-Network message containing a specific 2-octet
 *         network number when the responding half-router has the capability of
 *         establishing a PTP connection that can be used to reach the desired
 *         network but this PTP connection is not currently established.
 */
public class ICouldBeRouterToNetwork extends NPDUBasePacket implements SimpleTaskUnit {
	protected int NDNET;
	protected int PERF_INDEX;

	/**
	 * Creates ICouldBeRouterToNetwork with header
	 * 
	 * @param header
	 *            header
	 */
	public ICouldBeRouterToNetwork(MPDUHeader header) {
		this.header = header;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsense.scheduler.SimpleTaskUnit#doTask()
	 */
	@Override
	public void Execute() throws Exception {
		throw new UnsupportedOperationException("Method is not supported");
	}

	@Override
	public void receive() throws PropertyNotFoundException {
		NDNET = header.media.readShort(header.packetId, Property.NDNET) & 0xFFFF;
		PERF_INDEX = header.media.readByte(header.packetId, Property.PERF_INDEX) & 0xFF;
	}

	@Override
	public void send() throws BACNetException {
		header.send();
		logger.debug("sending " + this.toString());
		header.media.setByte(header.packetId, Property.MESSAGE_TYPE,
		        (byte) NPDUMessageType.I_Could_Be_Router_To_Network.getCode());
		header.media.setShort(header.packetId, Property.NDNET, (short) NDNET);
		header.media.setByte(header.packetId, Property.PERF_INDEX, (byte) PERF_INDEX);
		header.media.sendPacket(header.packetId);
	}

	@Override
	public void failureAction(Throwable t) {
		try {
			new RejectMessageToNetwork(RoutingService.getDefaultResponseHeader(header), 0, Configuration.OWN_NET)
			        .send();
		} catch (Exception e) {
			logger.warn("Unable to send message", e);
		}
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer(10);
		sb.append(header);
		sb.append(this.getClass().getName());
		sb.append("\n");
		sb.append("\tNDNET=");
		sb.append(NDNET);
		sb.append("\n");
		sb.append("\tPERF_INDEX=");
		sb.append(PERF_INDEX);
		sb.append("\n");
		return sb.toString();
	}

	public int getNDNET() {
		return NDNET;
	}

	public void setNDNET(int nDNET) {
		NDNET = nDNET;
	}

	public int getPERF_INDEX() {
		return PERF_INDEX;
	}

	public void setPERF_INDEX(int pERF_INDEX) {
		PERF_INDEX = pERF_INDEX;
	}

}
