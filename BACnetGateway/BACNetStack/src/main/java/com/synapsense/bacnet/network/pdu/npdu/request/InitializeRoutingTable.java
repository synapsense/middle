/**
 * 
 */
package com.synapsense.bacnet.network.pdu.npdu.request;

import com.synapsense.bacnet.network.Configuration;
import com.synapsense.bacnet.network.jni.BACNetException;
import com.synapsense.bacnet.network.jni.Property;
import com.synapsense.bacnet.network.jni.PropertyNotFoundException;
import com.synapsense.bacnet.network.jni.UnableToSendException;
import com.synapsense.bacnet.network.media.MPDUHeader;
import com.synapsense.bacnet.network.media.Media;
import com.synapsense.bacnet.network.pdu.npdu.NPDUBasePacket;
import com.synapsense.bacnet.network.pdu.npdu.NPDUMessageType;
import com.synapsense.bacnet.network.pdu.npdu.response.InitializeRoutingTableAck;
import com.synapsense.bacnet.network.pdu.npdu.response.RejectMessageToNetwork;
import com.synapsense.bacnet.network.routing.RoutingService;
import com.synapsense.scheduler.SimpleTaskUnit;

/**
 * @author Alexander Borisov This message is indicated by a Message Type of
 *         X'06'. It is used to initialize the routing table of a router or to
 *         query the contents of the current routing table. The format of the
 *         data portion of the Initialize-Routing-Table message is shown in
 *         Figure 6-11. The Number of Ports field of this NPDU indicates how
 *         many port mappings are being provided in this NPDU. This field
 *         permits routing tables to be incrementally updated as the network
 *         changes. Valid entries in this field are 0-255. Following this field
 *         are sets of data indicating the DNET directly connected to this port
 *         or accessible through a dial-up PTP connection, Port ID, Port Info
 *         Length, and, in the case Port Info Length is nonzero, Port Info. If
 *         an Initialize-Routing-Table message is sent with the Number of Ports
 *         equal to zero, the responding device shall return its complete
 *         routing table in an Initialize-Routing-Table -Ack message without
 *         updating its routing table. If the Port ID field has a value of zero,
 *         then all table entries for the specified DNET shall be purged from
 *         the table. If the Port ID field has a non-zero value, then the
 *         routing information for this DNET shall either replace any previous
 *         entry for this DNET in the routing table or, if no such entry exists,
 *         be appended to the routing table. The Port Info Length is an unsigned
 *         integer indicating the length of the Port Info field. The Port Info
 *         field, if present, shall contain an octet string. A typical use would
 *         be to convey modem control and dial information for accessing a
 *         remote network via a dial-up PTP connection.
 */
public class InitializeRoutingTable extends NPDUBasePacket implements SimpleTaskUnit {
	private String[] routeInfo;

	/**
	 * Creates InitializeRoutingTable message
	 * 
	 * @param header
	 *            header
	 */
	public InitializeRoutingTable(MPDUHeader header) {
		this.header = header;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsense.scheduler.SimpleTaskUnit#doTask()
	 */
	@Override
	public void Execute() throws Exception {
		NPDUBasePacket ack = null;
		MPDUHeader h = RoutingService.createResponseHeader(header);
		try {
			if (routeInfo != null && routeInfo.length != 0) {
				for (int i = 0; i < routeInfo.length; i++) {
					RoutingService.updateRoute(routeInfo[i], header.SRC_MAC);
				}
				ack = new InitializeRoutingTableAck(h);
			} else {
				ack = new InitializeRoutingTableAck(h, RoutingService.getTable());
			}
			ack.send();
		} catch (UnableToSendException e) {
			new RejectMessageToNetwork(h, 2, Configuration.OWN_NET).send();
		}
	}

	@Override
	public void receive() throws PropertyNotFoundException {
		routeInfo = header.media.readStringArray(header.packetId, Property.ROUTING_TABLES);
	}

	@Override
	public void send() throws BACNetException {
		Media reader = header.media;
		header.send();
		if (logger.isDebugEnabled())
			logger.debug("sending " + this.toString());
		reader.setByte(header.packetId, Property.MESSAGE_TYPE,
		        (byte) NPDUMessageType.Initialize_Routing_Table.getCode());
		reader.setStringArray(header.packetId, Property.ROUTING_TABLES, RoutingService.getTable());
		reader.sendPacket(header.packetId);
	}

	@Override
	public void failureAction(Throwable t) {
		try {
			new RejectMessageToNetwork(RoutingService.getDefaultResponseHeader(header), 2, Configuration.OWN_NET)
			        .send();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer(10);
		sb.append(this.getClass().getName());
		sb.append(header);
		if (routeInfo != null && routeInfo.length != 0) {
			sb.append("\trouteInfo=");
			for (int i = 0; i < routeInfo.length; i++) {
				sb.append(routeInfo[i]);
				sb.append("\n");
			}
		}
		return sb.toString();
	}

	public String[] getRouteInfo() {
		return routeInfo;
	}

	public void setRouteInfo(String[] routeInfo) {
		this.routeInfo = routeInfo;
	}

}
