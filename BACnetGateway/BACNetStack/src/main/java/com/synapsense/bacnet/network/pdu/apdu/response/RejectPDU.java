package com.synapsense.bacnet.network.pdu.apdu.response;

import com.synapsense.bacnet.network.jni.BACNetException;
import com.synapsense.bacnet.network.jni.Property;
import com.synapsense.bacnet.network.jni.PropertyNotFoundException;
import com.synapsense.bacnet.network.media.MPDUHeader;
import com.synapsense.bacnet.network.media.Media;
import com.synapsense.bacnet.network.pdu.apdu.APDUBasePacket;
import com.synapsense.bacnet.network.pdu.apdu.APDUMessageType;
import com.synapsense.bacnet.network.pdu.apdu.datatypes.RejectReason;

/**
 * 
 * @author Alexander Borisov </br> The BACnet-Reject-PDU is used to reject a
 *         received confirmed request PDU based on syntactical flaws or other
 *         protocol errors that prevent the PDU from being interpreted or the
 *         requested service from being provided. Only confirmed request PDUs
 *         may be rejected (see 18.8).</br> BACnet-Reject-PDU ::= SEQUENCE
 *         {</br> pdu-type [0] Unsigned (0..15), -- 6 for this PDU type</br>
 *         reserved [1] Unsigned (0..15), -- must be set to zero</br>
 *         original-invokeID [2] Unsigned (0..255),</br> reject reason [3]
 *         BACnetRejectReason</br> -- Context specific tags 0..3 are NOT used in
 *         header encoding</br> }</br>
 * 
 */
public class RejectPDU extends APDUBasePacket {
	RejectReason rejectreason = RejectReason.other;// [3] BACnetRejectReason

	public RejectPDU(MPDUHeader header) {
		this.header = header;
		type = APDUMessageType.reject_PDU;
	}

	@Override
	public void receive() throws PropertyNotFoundException {
		Media reader = header.media;
		invokeID = reader.readByte(header.packetId, Property.INVOKE_ID) & 0xFF;
		rejectreason = RejectReason.valueOf(reader.readByte(header.packetId, Property.REJECT_REASON) & 0xFF);
	}

	@Override
	public void send() throws BACNetException {
		Media reader = header.media;
		header.send();
		reader.setByte(header.packetId, Property.INVOKE_ID, (byte) invokeID);
		reader.setByte(header.packetId, Property.REJECT_REASON, (byte) rejectreason.getCode());
		reader.setByte(header.packetId, Property.MESSAGE_TYPE, type.getType());
		reader.sendPacket(header.packetId);
	}

	public void setRejectReason(RejectReason rejectReason) {
		this.rejectreason = rejectReason;

	}

	public RejectReason getRejectreason() {
		return rejectreason;
	}

	@Override
	public boolean isSegmented() {
		return false;
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer(100);
		sb.append("\n");
		sb.append(this.getClass().getName());
		sb.append(header);
		sb.append("invoke ID = ");
		sb.append(invokeID);
		sb.append("\n");

		sb.append("rejectreason = ");
		sb.append(rejectreason);
		sb.append("\n");

		return sb.toString();
	}

}
