package com.synapsense.bacnet.network.jni;

public class BACNetException extends Exception {

	/**
     * 
     */
	private static final long serialVersionUID = -7946944788463879487L;

	public BACNetException(String s) {
		super(s);
	}

}
