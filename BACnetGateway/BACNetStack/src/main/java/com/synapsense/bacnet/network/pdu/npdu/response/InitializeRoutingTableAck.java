/**
 * 
 */
package com.synapsense.bacnet.network.pdu.npdu.response;

import com.synapsense.bacnet.network.jni.BACNetException;
import com.synapsense.bacnet.network.jni.Property;
import com.synapsense.bacnet.network.jni.PropertyNotFoundException;
import com.synapsense.bacnet.network.media.MPDUHeader;
import com.synapsense.bacnet.network.media.Media;
import com.synapsense.bacnet.network.pdu.npdu.NPDUBasePacket;
import com.synapsense.bacnet.network.pdu.npdu.NPDUMessageType;
import com.synapsense.scheduler.SimpleTaskUnit;

/**
 * @author Alexander Borisov This message is indicated by a Message Type of
 *         X'07'. It is used to indicate that the routing table of a router has
 *         been changed or the table has been queried through the receipt of an
 *         Initialize-Routing-Table message with the Number of Ports field set
 *         equal to zero. The data portion of this message, returned only in
 *         response to a routing table query, conveys the routing table
 *         information, and it has the same format as the data portion of an
 *         Initialize-Routing-Table message.
 */
public class InitializeRoutingTableAck extends NPDUBasePacket implements SimpleTaskUnit {

	private String[] routeInfo;

	/**
	 * Creates InitializeRoutingTableAck packet with header
	 * 
	 * @param h
	 *            header
	 */
	public InitializeRoutingTableAck(MPDUHeader h) {
		this.header = h;
		routeInfo = new String[0];
	}

	/**
	 * Creates InitializeRoutingTableAck packet with header and routing
	 * information
	 * 
	 * @param h
	 *            header
	 * @param routeInfo
	 *            routing information
	 */
	public InitializeRoutingTableAck(MPDUHeader h, String[] routeInfo) {
		this.header = h;
		this.routeInfo = routeInfo;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsense.scheduler.SimpleTaskUnit#doTask()
	 */
	@Override
	public void Execute() throws Exception {
	}

	@Override
	public void failureAction(Throwable t) {
		// TODO Auto-generated method stub

	}

	@Override
	public void receive() throws PropertyNotFoundException {
		routeInfo = header.media.readStringArray(header.packetId, Property.ROUTING_TABLES);
	}

	@Override
	public void send() throws BACNetException {
		Media reader = header.media;
		header.send();
		if (logger.isDebugEnabled())
			logger.debug("sending " + this.toString());
		reader.setByte(header.packetId, Property.MESSAGE_TYPE,
		        (byte) NPDUMessageType.Initialize_Routing_Table_Ack.getCode());
		reader.setStringArray(header.packetId, Property.ROUTING_TABLES, routeInfo);
		reader.sendPacket(header.packetId);
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer(10);
		sb.append(this.getClass().getName());
		sb.append(header);
		if (routeInfo != null && routeInfo.length != 0) {
			sb.append("\trouteInfo=");
			for (int i = 0; i < routeInfo.length; i++) {
				sb.append(routeInfo[i]);
				sb.append("\n");
			}
		}
		return sb.toString();
	}

	public String[] getRouteInfo() {
		return routeInfo;
	}

	public void setRouteInfo(String[] routeInfo) {
		this.routeInfo = routeInfo;
	}

}
