package com.synapsense.bacnet.network.pdu.apdu.svc.request;

import java.io.IOException;

import com.synapsense.bacnet.network.media.MPDUHeader;
import com.synapsense.bacnet.network.pdu.apdu.APDUBasePacket;
import com.synapsense.bacnet.network.pdu.apdu.codecs.PrimitivesDecoder;
import com.synapsense.bacnet.network.pdu.apdu.codecs.PrimitivesEncoder;
import com.synapsense.bacnet.network.pdu.apdu.codecs.exceptions.NotResolvedPayloadException;
import com.synapsense.bacnet.network.pdu.apdu.codecs.primitives.BACnetTL;
import com.synapsense.bacnet.network.pdu.apdu.datatypes.ConfirmedServiceChoice;
import com.synapsense.bacnet.network.pdu.apdu.request.ConfirmedRequestPDU;
import com.synapsense.bacnet.network.pdu.apdu.sessions.AbstractSession;
import com.synapsense.bacnet.network.pdu.apdu.sessions.client.unsegmented.ConfirmedSession;
import com.synapsense.bacnet.network.pdu.apdu.svc.SvcBase;
import com.synapsense.bacnet.network.pdu.apdu.svc.SvcException;
import com.synapsense.bacnet.network.pdu.apdu.svc.SvcFactory;
import com.synapsense.bacnet.network.pdu.apdu.svc.responce.ErrorSvc;
import com.synapsense.bacnet.network.routing.MalformedHeaderException;
import com.synapsense.bacnet.network.routing.RouteNotFoundException;
import com.synapsense.bacnet.network.routing.RoutingService;
import com.synapsense.bacnet.system.BACnetError;
import com.synapsense.bacnet.system.BACnetSystem;
import com.synapsense.bacnet.system.Device;
import com.synapsense.bacnet.system.shared.BACnetAddress;
import com.synapsense.bacnet.system.shared.BACnetObjectIdentifier;

/**
 * 
 * @author Alexander Borisov
 *
 */

/**
 * WriteProperty-Request ::= SEQUENCE { objectIdentifier [0]
 * BACnetObjectIdentifier, propertyIdentifier [1] BACnetPropertyIdentifier,
 * propertyArrayIndex [2] Unsigned OPTIONAL, --used only with array datatype --
 * if omitted with an array the entire -- array is referenced --propertyValue
 * [3] ABSTRACT-SYNTAX.&Type, priority [4] Unsigned8 (1..16) OPTIONAL --used
 * only when property is commandable }
 */
public class WriteProperty extends SvcBase {
	protected BACnetObjectIdentifier objectIdentifier;
	protected long propertyIdentifier;
	protected Long propertyArrayIndex; // null if propertyArrayIndex [2]
	                                   // Unsigned OPTIONAL is omitted
	protected byte[] propertyValue;
	protected byte priority;

	public WriteProperty(MPDUHeader header) {
		super(header);
		cChoise = ConfirmedServiceChoice.writeProperty;
	}

	public WriteProperty(APDUBasePacket packet) {
		super(packet);
	}

	public WriteProperty(SvcBase svc) {
		super(svc);
	}

	@Override
	public void decodeBuffer(byte[] buffer) {
		PrimitivesDecoder pd = new PrimitivesDecoder(buffer);
		propertyArrayIndex = null;
		try {
			objectIdentifier = pd.decodeObjectID(0);
			propertyIdentifier = pd.decodeEnum(1);
			BACnetTL tag = pd.askForTag();
			if (tag == null)
				throw new SvcException("decoded buffer contains illegal data");
			switch (tag.getID()) {
			case 2: {
				propertyArrayIndex = pd.decodeUInt(2);
				pd.decodeSequenceStart(3);
				propertyValue = pd.decodeAny().getBytes();
				pd.decodeSequenceStop(3);
				break;
			}
			case 3: {
				pd.decodeSequenceStart(3);
				propertyValue = pd.getTaggedRaw();
				pd.decodeSequenceStop(3);
				break;
			}
			default:
				throw new SvcException("decoded buffer contains illegal data");
			}

			tag = pd.askForTag();
			if (tag != null && tag.getID() == 4)
				priority = (byte) pd.decodeUInt(4);
		} catch (IOException e) {
			throw new SvcException("decoded buffer contains illegal data", e);
		} catch (NotResolvedPayloadException e) {
			throw new SvcException("decoded buffer contains illegal data", e);
		}
	}

	@Override
	public byte[] encodeBuffer() {
		PrimitivesEncoder encoder = new PrimitivesEncoder();
		try {
			encoder.encodeObjectID(0, objectIdentifier);
			encoder.encodeEnum(1, propertyIdentifier);
			if (propertyArrayIndex != null)
				encoder.encodeUInt(2, propertyArrayIndex);
			encoder.appendEncodedBuffer(propertyValue);
		} catch (IOException e) {
			logger.warn("Unable to encode buffer", e);
			throw new SvcException("Cannot encode service", e);
		}
		return encoder.getEncodedBuffer();
	}

	@Override
	public SvcBase[] execute() {
		logger.debug("Executing Read property service:");
		logger.debug("PropID:" + propertyIdentifier);
		logger.debug("ObjID:" + objectIdentifier);
		// TODO now it only works with non-broadcast messages
		BACnetAddress addr = new BACnetAddress(header.DNET, header.DADR);
		SvcBase response = null;
		try {
			Device device = BACnetSystem.getInstance().getDevice(addr);
			if (device != null) {
				com.synapsense.bacnet.services.WriteProperty writeService = device.getWritePropertyService();
				if (writeService != null) {
					writeService.write(objectIdentifier, propertyIdentifier, propertyArrayIndex, propertyValue,
					        priority);
					response = SvcFactory.getInstance().getResponseForRequest(this);
					logger.info("Request for write property processed... Result: ok");
				} else {
					logger.warn("The device " + addr + " isn't supported Write operation for PropId:"
					        + propertyIdentifier + ".");
				}
			} else {
				logger.warn("The device with address " + addr + " doesn't exist.");
			}
			if (response == null) {
				response = new ErrorSvc(this);
			}
		} catch (BACnetError e1) {
			response = new ErrorSvc(e1, this);
			response.encodeBuffer();
		} catch (Exception e) {
			response = new ErrorSvc(this);
			logger.warn("Abnormal scenario.... Sending error", e);
		} finally {
			try {
				MPDUHeader respHeader = RoutingService.createResponseHeader(header);
				respHeader.SNET = header.DNET;
				respHeader.SLEN = header.DLEN;
				respHeader.SADR = MPDUHeader.getCopy(header.DADR);
				response.setHeader(respHeader);
			} catch (RouteNotFoundException e) {
				throw new SvcException("Malformed header", e);
			} catch (MalformedHeaderException e) {
				throw new SvcException("Malformed header", e);
			}
		}
		SvcBase res[] = { response };
		return res;
	}

	public AbstractSession createSession() {
		return new ConfirmedSession(this);
	}

	public BACnetObjectIdentifier getObjectIdentifier() {
		return objectIdentifier;
	}

	public void setObjectIdentifier(BACnetObjectIdentifier objectIdentifier) {
		this.objectIdentifier = objectIdentifier;
	}

	public long getPropertyIdentifier() {
		return propertyIdentifier;
	}

	public void setPropertyIdentifier(long propertyIdentifier) {
		this.propertyIdentifier = propertyIdentifier;
	}

	public Long getPropertyArrayIndex() {
		return propertyArrayIndex;
	}

	public void setPropertyArrayIndex(Long propertyArrayIndex) {
		this.propertyArrayIndex = propertyArrayIndex;
	}

	@Override
	public APDUBasePacket getPacket() {
		if (header == null)
			throw new SvcException("Unable to create APDU packet");
		if (header.DST_MAC == null) {
			try {
				header.DST_MAC = MPDUHeader.getCopy(RoutingService.discoverRoute(header.DNET).getDST_MAC());
			} catch (RouteNotFoundException e) {
				throw new SvcException(e.getLocalizedMessage());
			}
		}
		header.Control = true;
		ConfirmedRequestPDU packet = new ConfirmedRequestPDU(header);
		packet.setId(invokeID);
		packet.setService_choice(ConfirmedServiceChoice.writeProperty);
		packet.setSegmented_message(this.segmented_message);
		packet.setMore_follows(this.more_follows);
		packet.setSegmented_response_accepted(this.segmented_response_accepted);
		packet.setMax_segments_accepted(this.max_segments_accepted);
		packet.setMax_APDU_length_accepted(getMax_APDU_length_accepted());
		packet.setSequence_number(this.sequence_number);
		packet.setProposed_window_size(this.proposed_window_size);

		if (encodedBuffer == null) {
			encodedBuffer = encodeBuffer();
		}
		packet.setBuffer(encodedBuffer);
		return packet;
	}

	public void setPropertyValue(byte[] propertyValue) {
		this.propertyValue = propertyValue.clone();
	}
}
