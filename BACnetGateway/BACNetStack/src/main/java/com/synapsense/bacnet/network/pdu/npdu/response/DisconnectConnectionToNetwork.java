/**
 * 
 */
package com.synapsense.bacnet.network.pdu.npdu.response;

import com.synapsense.bacnet.network.Configuration;
import com.synapsense.bacnet.network.jni.BACNetException;
import com.synapsense.bacnet.network.jni.Property;
import com.synapsense.bacnet.network.jni.PropertyNotFoundException;
import com.synapsense.bacnet.network.media.MPDUHeader;
import com.synapsense.bacnet.network.pdu.npdu.NPDUBasePacket;
import com.synapsense.bacnet.network.pdu.npdu.NPDUMessageType;
import com.synapsense.bacnet.network.routing.RoutingService;
import com.synapsense.scheduler.SimpleTaskUnit;

/**
 * @author Alexander Borisov This message is indicated by a Message Type of
 *         X'09' followed by a 2-octet network number. This message is used to
 *         instruct a router to disconnect an established PTP connection.
 */
public class DisconnectConnectionToNetwork extends NPDUBasePacket implements SimpleTaskUnit {
	private int DNET;

	/**
	 * Create DisconnectConnectionToNetwork instance with header
	 * 
	 * @param header
	 *            header of DisconnectConnectionToNetwork message
	 */
	public DisconnectConnectionToNetwork(MPDUHeader header) {
		this.header = header;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsense.scheduler.SimpleTaskUnit#doTask()
	 */
	@Override
	public void Execute() throws Exception {
		throw new UnsupportedOperationException("Method is not supported");
	}

	@Override
	public void receive() throws PropertyNotFoundException {
		DNET = header.media.readShort(header.packetId, Property.NDNET) & 0xFFFF;

	}

	@Override
	public void send() throws BACNetException {
		header.send();
		logger.debug("sending " + this.toString());

		header.media.setByte(header.packetId, Property.MESSAGE_TYPE,
		        (byte) NPDUMessageType.Disconnect_Connection_To_Network.getCode());
		header.media.setShort(header.packetId, Property.NDNET, (short) DNET);
		header.media.sendPacket(header.packetId);
	}

	@Override
	public void failureAction(Throwable t) {
		try {
			new RejectMessageToNetwork(RoutingService.getDefaultResponseHeader(header), 0, Configuration.OWN_NET)
			        .send();
		} catch (Exception e) {
			logger.warn("Unable to send message", e);
		}
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer(10);
		sb.append(header);
		sb.append(this.getClass().getName());
		sb.append("\n");
		sb.append("\tDNET=");
		sb.append(DNET);
		return sb.toString();
	}

	public int getDNET() {
		return DNET;
	}

	public void setDNET(int dNET) {
		DNET = dNET;
	}

}
