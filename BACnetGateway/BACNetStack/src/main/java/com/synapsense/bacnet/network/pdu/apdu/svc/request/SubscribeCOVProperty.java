/**
 * 
 */
package com.synapsense.bacnet.network.pdu.apdu.svc.request;

import java.io.IOException;

import com.synapsense.bacnet.enums.ErrorClass;
import com.synapsense.bacnet.enums.ErrorCode;
import com.synapsense.bacnet.network.media.MPDUHeader;
import com.synapsense.bacnet.network.pdu.apdu.APDUBasePacket;
import com.synapsense.bacnet.network.pdu.apdu.codecs.PrimitivesDecoder;
import com.synapsense.bacnet.network.pdu.apdu.codecs.PrimitivesEncoder;
import com.synapsense.bacnet.network.pdu.apdu.codecs.primitives.BACnetTL;
import com.synapsense.bacnet.network.pdu.apdu.datatypes.ConfirmedServiceChoice;
import com.synapsense.bacnet.network.pdu.apdu.request.ConfirmedRequestPDU;
import com.synapsense.bacnet.network.pdu.apdu.sessions.AbstractSession;
import com.synapsense.bacnet.network.pdu.apdu.sessions.client.unsegmented.ConfirmedSession;
import com.synapsense.bacnet.network.pdu.apdu.svc.SvcBase;
import com.synapsense.bacnet.network.pdu.apdu.svc.SvcException;
import com.synapsense.bacnet.network.pdu.apdu.svc.SvcFactory;
import com.synapsense.bacnet.network.pdu.apdu.svc.responce.ErrorSvc;
import com.synapsense.bacnet.network.routing.MalformedHeaderException;
import com.synapsense.bacnet.network.routing.RouteNotFoundException;
import com.synapsense.bacnet.network.routing.RoutingService;
import com.synapsense.bacnet.services.COVNotificationListener;
import com.synapsense.bacnet.system.BACnetError;
import com.synapsense.bacnet.system.BACnetNetwork;
import com.synapsense.bacnet.system.BACnetPropertyValue;
import com.synapsense.bacnet.system.BACnetSystem;
import com.synapsense.bacnet.system.Device;
import com.synapsense.bacnet.system.shared.BACnetAddress;
import com.synapsense.bacnet.system.shared.BACnetObjectIdentifier;
import com.synapsense.bacnet.system.shared.BACnetPropertyReference;

/**
 * @author Alexander Borisov
 *
 */

/**
 * SubscribeCOVProperty-Request ::= SEQUENCE { subscriberProcessIdentifier [0]
 * Unsigned32, monitoredObjectIdentifier [1] BACnetObjectIdentifier,
 * issueConfirmedNotifications [2] BOOLEAN OPTIONAL, lifetime [3] Unsigned
 * OPTIONAL, monitoredPropertyIdentifier [4] BACnetPropertyReference,
 * covIncrement [5] REAL OPTIONAL }
 * 
 */
public class SubscribeCOVProperty extends SvcBase {
	private long subscriberProcessIdentifier;
	private BACnetObjectIdentifier monitoredObjectIdentifier = null;
	private Boolean issueConfirmedNotifications = null;
	private Long lifetime = null;
	private BACnetPropertyReference monitoredPropertyIdentifier = new BACnetPropertyReference();
	private Float covIncrement = null;

	public SubscribeCOVProperty(MPDUHeader header) {
		super(header);
		cChoise = ConfirmedServiceChoice.subscribeCOVProperty;
	}

	public SubscribeCOVProperty(APDUBasePacket packet) {
		super(packet);
	}

	public SubscribeCOVProperty(SvcBase svc) {
		super(svc);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.synapsense.bacnet.network.pdu.apdu.svc.SvcBase#decodeBuffer(byte[])
	 */
	@Override
	public void decodeBuffer(byte[] buffer) throws BACnetError {
		PrimitivesDecoder pd = new PrimitivesDecoder(buffer);
		try {
			subscriberProcessIdentifier = pd.decodeUInt(0);
			monitoredObjectIdentifier = pd.decodeObjectID(1);
			BACnetTL tag = pd.askForTag();
			switch (tag.getID()) {
			case 2:
				issueConfirmedNotifications = pd.decodeBoolean(2);
				tag = pd.askForTag();
				if (tag.getID() == 3) {
					lifetime = pd.decodeUInt(3);
					try {
						pd.decodeSequenceStart(4);
						monitoredPropertyIdentifier.propertyIdentifier = pd.decodeEnum(0);
						if (pd.askForTag() != null && pd.askForTag().getID() == 1)
							monitoredPropertyIdentifier.propertyArrayIndex = pd.decodeUInt(1);
						pd.decodeSequenceStop(4);
					} catch (IOException e) {
						// buffer reader've thrown the exception since the
						// monitoredPropertyIdentifier
						// parameter is missing from the packet and it's
						// impossible to find sequence start tag. Return
						// MISSING_REQUIRED_PARAMETER error.
						logger.warn("Unable to decode packet buffer, missing 'monitoredPropertyIdentifier' parameter.");
						throw new BACnetError(ErrorClass.SERVICES, ErrorCode.MISSING_REQUIRED_PARAMETER);
					}
				} else {
					logger.warn("Unable to decode packet buffer, missing 'lifetime' parameter.");
					throw new BACnetError(ErrorClass.SERVICES, ErrorCode.MISSING_REQUIRED_PARAMETER);
				}
				break;
			case 3:
				// we've got lifetime tag but not Issue Confirmed Notification
				// tag. They both should be present.
				logger.warn("Unable to decode packet buffer, missing 'Issue Confirmed Notification' parameter.");
				throw new BACnetError(ErrorClass.SERVICES, ErrorCode.MISSING_REQUIRED_PARAMETER);
			case 4:
				pd.decodeSequenceStart(4);
				monitoredPropertyIdentifier.propertyIdentifier = pd.decodeEnum(0);
				if (pd.askForTag() != null && pd.askForTag().getID() == 1)
					monitoredPropertyIdentifier.propertyArrayIndex = pd.decodeUInt(1);
				pd.decodeSequenceStop(4);
				break;
			default:
				logger.warn("Unable to decode packet buffer");
				throw new SvcException("Unable to decode packet buffer");
			}

			tag = pd.askForTag();
			if (tag != null) {
				if (tag.getID() == 5) {
					covIncrement = pd.decodeReal(5);
				}
			}
		} catch (IOException e) {
			logger.warn("Unable to decode packet buffer", e);
			throw new BACnetError(ErrorClass.SERVICES, ErrorCode.INCONSISTENT_PARAMETERS);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsense.bacnet.network.pdu.apdu.svc.SvcBase#encodeBuffer()
	 */
	@Override
	public byte[] encodeBuffer() {
		PrimitivesEncoder pe = new PrimitivesEncoder();
		try {
			pe.encodeUInt(0, subscriberProcessIdentifier);
			pe.encodeObjectID(1, monitoredObjectIdentifier);
			if (issueConfirmedNotifications != null) {
				pe.encodeBoolean(2, issueConfirmedNotifications);
			}
			if (lifetime != null) {
				pe.encodeUInt(3, lifetime);
			}
			pe.encodeSequenceStart(4);
			pe.encodeEnum(0, monitoredPropertyIdentifier.propertyIdentifier);
			if (monitoredPropertyIdentifier.propertyArrayIndex != null)
				pe.encodeUInt(1, monitoredPropertyIdentifier.propertyArrayIndex);
			pe.encodeSequenceStop(4);
			if (covIncrement != null) {
				pe.encodeReal(5, covIncrement);
			}
			return pe.getEncodedBuffer();
		} catch (IOException e) {
			logger.warn("Unable to encode buffer", e);
			throw new SvcException("Cannot encode service", e);
		}
	}

	@Override
	public APDUBasePacket getPacket() {
		if (header == null)
			throw new SvcException("Unable to create APDU packet");
		if (header.DST_MAC == null) {
			try {
				header.DST_MAC = MPDUHeader.getCopy(RoutingService.discoverRoute(header.DNET).getDST_MAC());
			} catch (RouteNotFoundException e) {
				throw new SvcException(e.getLocalizedMessage());
			}
		}
		header.Control = true;
		ConfirmedRequestPDU packet = new ConfirmedRequestPDU(header);
		packet.setId(invokeID);
		packet.setService_choice(ConfirmedServiceChoice.subscribeCOVProperty);
		packet.setSegmented_message(this.segmented_message);
		packet.setMore_follows(this.more_follows);
		packet.setSegmented_response_accepted(this.segmented_response_accepted);
		packet.setMax_segments_accepted(this.max_segments_accepted);
		packet.setMax_APDU_length_accepted(getMax_APDU_length_accepted());
		packet.setSequence_number(this.sequence_number);
		packet.setProposed_window_size(this.proposed_window_size);

		if (encodedBuffer == null) {
			encodedBuffer = encodeBuffer();
		}

		packet.setBuffer(encodedBuffer);
		return packet;
	}

	/**
	 * @return the subscriberProcessIdentifier
	 */
	public long getSubscriberProcessIdentifier() {
		return subscriberProcessIdentifier;
	}

	/**
	 * @param subscriberProcessIdentifier
	 *            the subscriberProcessIdentifier to set
	 */
	public void setSubscriberProcessIdentifier(int subscriberProcessIdentifier) {
		this.subscriberProcessIdentifier = subscriberProcessIdentifier;
	}

	/**
	 * @return the monitoredObjectIdentifier
	 */
	public BACnetObjectIdentifier getMonitoredObjectIdentifier() {
		return monitoredObjectIdentifier;
	}

	/**
	 * @param monitoredObjectIdentifier
	 *            the monitoredObjectIdentifier to set
	 */
	public void setMonitoredObjectIdentifier(BACnetObjectIdentifier monitoredObjectIdentifier) {
		this.monitoredObjectIdentifier = monitoredObjectIdentifier;
	}

	/**
	 * @return the issueConfirmedNotifications
	 */
	public boolean isIssueConfirmedNotifications() {
		return issueConfirmedNotifications;
	}

	/**
	 * @param issueConfirmedNotifications
	 *            the issueConfirmedNotifications to set
	 */
	public void setIssueConfirmedNotifications(boolean issueConfirmedNotifications) {
		this.issueConfirmedNotifications = issueConfirmedNotifications;
	}

	/**
	 * @return the lifetime
	 */

	/**
	 * @return the covIncrement
	 */
	public double getCovIncrement() {
		return covIncrement;
	}

	/**
	 * @param covIncrement
	 *            the covIncrement to set
	 */

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsense.bacnet.network.pdu.apdu.svc.SvcBase#execute()
	 */

	public SvcBase[] execute() {
		if (logger.isDebugEnabled()) {
			logger.debug("Executing SubscribeCOVProperty  service:");
			logger.debug("header.DNET=" + header.DNET);
			logger.debug("monitoredObjectIdentifier=" + monitoredObjectIdentifier);
			logger.debug("subscriberProcessIdentifier" + subscriberProcessIdentifier);
			logger.debug("lifetime" + lifetime);
			logger.debug("monitoredPropertyIdentifier" + monitoredPropertyIdentifier);
		}

		SvcBase response = null;
		try {
			BACnetNetwork network = BACnetSystem.getInstance().getNetwork(header.DNET);
			if (network != null) {
				Device device = network.getDevice(header.DADR);
				if (device != null) {
					com.synapsense.bacnet.services.SubscribeCOVProperty subscribeService = device
					        .getSubscribeCOVPropertyService();
					if (subscribeService != null) {
						subscribeService.subscribe(subscriberProcessIdentifier, monitoredObjectIdentifier,
						        new COVNotificationListenerImpl(),// TODO
						                                          // implement
						                                          // this class
						        lifetime, monitoredPropertyIdentifier.propertyIdentifier, covIncrement);
						response = SvcFactory.getInstance().getResponseForRequest(this);
					} else {
						logger.warn("The device with address " + new BACnetAddress(header.DNET, header.DADR)
						        + " is not supported COV for PropId: " + monitoredPropertyIdentifier.propertyIdentifier
						        + ".");
					}
				} else {
					logger.warn("The device with address " + new BACnetAddress(header.DNET, header.DADR)
					        + " doesn't exist.");
				}
			} else {
				logger.warn("The network with number [" + header.DNET + "] doesn't exist.");
			}
			if (response == null) {
				response = new ErrorSvc(this);
			}
		} catch (BACnetError e) {
			response = new ErrorSvc(e, this);
			// result = response.encodeBuffer();
			response.encodeBuffer();
		} catch (Exception e) {
			response = new ErrorSvc(this);
			logger.warn("Abnormal scenario.... Sending error", e);
		} finally {
			try {
				MPDUHeader respHeader = RoutingService.createResponseHeader(header);
				respHeader.SNET = header.DNET;
				respHeader.SLEN = header.DLEN;
				respHeader.SADR = MPDUHeader.getCopy(header.DADR);
				response.setHeader(respHeader);
			} catch (RouteNotFoundException e) {
				throw new SvcException("Malformed header", e);
			} catch (MalformedHeaderException e) {
				throw new SvcException("Malformed header", e);
			}
		}

		SvcBase res[] = { response };
		return res;
	}

	/**
	 * @return the issueConfirmedNotifications
	 */
	public Boolean getIssueConfirmedNotifications() {
		return issueConfirmedNotifications;
	}

	/**
	 * @param issueConfirmedNotifications
	 *            the issueConfirmedNotifications to set
	 */
	public void setIssueConfirmedNotifications(Boolean issueConfirmedNotifications) {
		this.issueConfirmedNotifications = issueConfirmedNotifications;
	}

	/**
	 * @return the lifetime
	 */
	public Long getLifetime() {
		return lifetime;
	}

	/**
	 * @param lifetime
	 *            the lifetime to set
	 */
	public void setLifetime(Long lifetime) {
		this.lifetime = lifetime;
	}

	/**
	 * @param subscriberProcessIdentifier
	 *            the subscriberProcessIdentifier to set
	 */
	public void setSubscriberProcessIdentifier(long subscriberProcessIdentifier) {
		this.subscriberProcessIdentifier = subscriberProcessIdentifier;
	}

	/**
	 * @param covIncrement
	 *            the covIncrement to set
	 */
	public void setCovIncrement(Float covIncrement) {
		this.covIncrement = covIncrement;
	}

	/**
	 * @return the monitoredPropertyIdentifier
	 */
	public BACnetPropertyReference getMonitoredPropertyIdentifier() {
		return monitoredPropertyIdentifier;
	}

	/**
	 * @param monitoredPropertyIdentifier
	 *            the monitoredPropertyIdentifier to set
	 */
	public void setMonitoredPropertyIdentifier(BACnetPropertyReference monitoredPropertyIdentifier) {
		this.monitoredPropertyIdentifier = monitoredPropertyIdentifier;
	}

	protected class COVNotificationListenerImpl implements COVNotificationListener {
		private MPDUHeader respHeader;

		public COVNotificationListenerImpl() {
			try {
				if (logger.isDebugEnabled()) {
					logger.debug("Creating new instanse for COVNotificationListenerImpl");
				}
				respHeader = RoutingService.createResponseHeader(header);
			} catch (Exception e) {
				throw new SvcException("Unable to create listener", e);
			}
			if (logger.isDebugEnabled()) {
				logger.debug("New instanse for COVNotificationListenerImpl created");
			}
		}

		@Override
		public void valueChanged(long subscrProcessId, BACnetObjectIdentifier initiatingDeviceId,
		        BACnetObjectIdentifier monitoredObjectId, long timeRemaining, BACnetPropertyValue[] listOfValues) {
			if (logger.isInfoEnabled()) {
				logger.info("valueChanged call");
				logger.info("subscrProcessId=" + subscrProcessId);
				logger.info("initiatingDeviceId=" + initiatingDeviceId);
				logger.info("monitoredObjectId=" + monitoredObjectId);
				logger.info("timeRemaining=" + timeRemaining);
				logger.info("listOfValues=" + listOfValues);
			}
			SvcBase respSvc = null;
			if (issueConfirmedNotifications == true) {
				if (logger.isInfoEnabled()) {
					logger.info("Sending ConfirmedNotifications to target");
				}
				respSvc = new ConfirmedCOVNotification(respHeader);
				ConfirmedCOVNotification tmp = (ConfirmedCOVNotification) respSvc;
				tmp.setSubscriberProcessIdentifier(subscrProcessId);
				tmp.setInitiatingDeviceIdentifier(initiatingDeviceId);
				tmp.setMonitoredObjectIdentifier(monitoredObjectId);
				tmp.setTimeRemaining(timeRemaining);
				tmp.setListOfValues(listOfValues);
			} else {
				if (logger.isInfoEnabled()) {
					logger.info("Sending UnConfirmedNotifications to target");
				}
				respSvc = new UnconfirmedCOVNotification(respHeader);
				UnconfirmedCOVNotification tmp = (UnconfirmedCOVNotification) respSvc;
				tmp.setSubscriberProcessIdentifier(subscrProcessId);
				tmp.setInitiatingDeviceIdentifier(initiatingDeviceId);
				tmp.setMonitoredObjectIdentifier(monitoredObjectId);
				tmp.setTimeRemaining(timeRemaining);
				tmp.setListOfValues(listOfValues);
			}
			respSvc.createSession();
			// TODO Add something for confirmed COV.....
		}
	}

	public AbstractSession createSession() {
		return new ConfirmedSession(this);
	}
}
