package com.synapsense.bacnet.network.pdu.apdu.sessions;

import java.util.Arrays;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import com.synapsense.bacnet.network.media.Media;
import com.synapsense.bacnet.network.pdu.apdu.APDUBasePacket;
import com.synapsense.bacnet.network.pdu.apdu.request.UnconfirmedRequestPDU;
import com.synapsense.scheduler.TaskScheduler;

public final class SessionManager {
	ConcurrentHashMap<SessionId, AbstractSession> data;
	Logger logger = Logger.getLogger(SessionManager.class);
	private static SessionManager manager;

	static {
		manager = new SessionManager();
	}

	/**
	 * Creates SessionManager
	 */
	private SessionManager() {
		data = new ConcurrentHashMap<SessionId, AbstractSession>();
	}

	public static SessionManager getInstance() {
		return manager;
	}

	/**
	 * Creates/Update session information for APDU packet If session is not
	 * exist new session will be created otherwise instance of session will
	 * returned
	 * 
	 * @param packet
	 *            APDU packet
	 */
	public void createSession(APDUBasePacket packet) {
		SessionId id;
		if (packet instanceof UnconfirmedRequestPDU) {
			// for unconfirmed requests we don't have invokeID
			id = new SessionId(packet.getHeader().SRC_MAC, packet.getHeader().DST_MAC,
			        (int) ((UnconfirmedRequestPDU) packet).getSVCChoise().getType(), true, packet.getHeader().media);
		} else {
			id = new SessionId(packet.getHeader().SRC_MAC, packet.getHeader().DST_MAC, packet.getInvokeId(), false,
			        packet.getHeader().media);
		}

		AbstractSession session = null;
		logger.debug("Trying to find session with ID: " + id.toString());
		session = data.get(id);
		if (session != null) {
			session.addMessageToSession(packet);
			if (logger.isDebugEnabled())
				logger.debug("Session with ID [" + id.toString() + "] has been modifyed");
		} else {
			session = AbstractSession.createServerSession(packet);
			if (session == null) {
				logger.warn("Unexpected APDU message recieved (will be ignored) [" + packet.toString() + "]");
				return;
			}
			session.setId(id);
			data.put(id, session);
			logger.debug("Created and added session with ID: " + id.toString());
			logger.debug("Queue size [" + data.size() + "]");
			TaskScheduler.getInstance().addIncomingTask(session, session.getTimeout());

		}
	}

	public void removeSession(AbstractSession s) {
		data.remove(s.getId());
		if (logger.isDebugEnabled()) {
			logger.debug("Removing session [" + s.toString() + "]");
			logger.debug("Queue size [" + data.size() + "]");
		}
	}

	public void addSession(AbstractSession s) {
		data.put(s.getId(), s);
		if (logger.isDebugEnabled()) {
			logger.debug("Registering new session [" + s.toString() + "]");
			logger.debug("Queue size [" + data.size() + "]");
		}
	}

	public static class SessionId {
		private Media media;
		// SRC_MAC for server and DST_MAC for client
		private byte[] addr1;
		// SRC_MAC for client and DST_MAC for server
		private byte[] addr2;
		private Integer id;
		private boolean isUnconfirmed;

		public SessionId(byte[] addr1, byte[] addr2, Integer id, boolean isUnconfirmed, Media media) {
			this.media = media;
			this.addr1 = addr1;
			this.addr2 = addr2;
			this.id = id;
			this.isUnconfirmed = isUnconfirmed;
		}

		public byte[] getAddr1() {
			return addr1;
		}

		public byte[] getAddr2() {
			return addr2;
		}

		public Integer getId() {
			return id;
		}

		public boolean isUnconfirmed() {
			return isUnconfirmed;
		}

		public void setUnconfirmed(boolean isUnconfirmed) {
			this.isUnconfirmed = isUnconfirmed;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			SessionId other = (SessionId) obj;

			if (id == null) {
				if (other.id != null)
					return false;
			} else {
				if (!id.equals(other.getId())) {
					return false;
				}
			}
			if (!media.isBroadcast(addr1) && !media.isBroadcast(other.addr1) && !Arrays.equals(addr1, other.addr1))
				return false;
			if (!media.isBroadcast(addr2) && !media.isBroadcast(other.addr2) && !Arrays.equals(addr2, other.addr2))
				return false;
			return true;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			if (isUnconfirmed) {
				result = prime * result + Arrays.hashCode(media.getBroadcast());
			} else {
				if (addr1 != null) {
					result = prime * result + Arrays.hashCode(addr1);
				}
				if (addr2 != null) {
					result = prime * result + Arrays.hashCode(addr2);
				}
			}
			result = prime * result + ((id == null) ? 0 : id.hashCode());
			return result;
		}

		@Override
		public String toString() {
			StringBuffer sb = new StringBuffer();
			sb.append("ADDR1 [");
			sb.append(toHex(addr1));
			sb.append("] ADDR2 [");
			sb.append(toHex(addr2));
			sb.append("] ID [");
			sb.append(id);
			sb.append("] isUnconfirmed [");
			sb.append(isUnconfirmed);
			sb.append("]");

			return sb.toString();
		}

		public static String toHex(byte[] b) {
			String hexmac = "";
			if (b == null)
				return hexmac;
			for (int i = 0; i < b.length; ++i) {
				if (!hexmac.isEmpty())
					hexmac = hexmac + ":";
				hexmac = hexmac + (Integer.toHexString((b[i] & 0xFF) | 0x100).substring(1, 3).toUpperCase());
			}
			return hexmac;
		}
	}
}
