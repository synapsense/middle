/**
 * 
 */
package com.synapsense.bacnet.network.pdu.npdu.response;

import com.synapsense.bacnet.network.Configuration;
import com.synapsense.bacnet.network.jni.BACNetException;
import com.synapsense.bacnet.network.jni.Property;
import com.synapsense.bacnet.network.jni.PropertyNotFoundException;
import com.synapsense.bacnet.network.media.MPDUHeader;
import com.synapsense.bacnet.network.pdu.npdu.NPDUBasePacket;
import com.synapsense.bacnet.network.pdu.npdu.NPDUMessageType;
import com.synapsense.bacnet.network.routing.Route;
import com.synapsense.bacnet.network.routing.RoutingService;
import com.synapsense.scheduler.SimpleTaskUnit;

/**
 * @author Alexander Borisov This message is indicated by a Message Type of
 *         X'05' optionally followed by a list of 2-octet network numbers. It
 *         shall always be transmitted with a broadcast MAC address.
 *         Router-Available-To-Network is used by a router to enable or reenable
 *         the receipt of messages for a specific list of DNETs or all DNETs.
 */

public class RouterAvailableToNetwork extends NPDUBasePacket implements SimpleTaskUnit {
	protected int[] DNET_LIST;

	public RouterAvailableToNetwork(MPDUHeader header) {
		this.header = header;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsense.scheduler.SimpleTaskUnit#doTask()
	 */
	@Override
	public void Execute() throws Exception {
		for (int i = 0; i < DNET_LIST.length; i++) {
			RoutingService.updateRoute(new Route(DNET_LIST[i], 1, this.getClass().getName(), header.SRC_MAC));
		}
	}

	@Override
	public void receive() throws PropertyNotFoundException {
		DNET_LIST = header.media.readShortArrayAsInt(header.packetId, Property.NDNETS);

	}

	@Override
	public void send() throws BACNetException {
		header.send();
		if (DNET_LIST != null) {
			header.media.setShortArrayAsInt(header.packetId, Property.NDNETS, DNET_LIST);
		}
		header.media.setByte(header.packetId, Property.MESSAGE_TYPE,
		        (byte) NPDUMessageType.Router_Available_To_Network.getCode());
		header.media.sendPacket(header.packetId);
	}

	@Override
	public void failureAction(Throwable t) {
		try {
			new RejectMessageToNetwork(RoutingService.getDefaultResponseHeader(header), 0, Configuration.OWN_NET)
			        .send();
		} catch (Exception e) {
			logger.warn("Unable to send message", e);
		}
	}

	public String toString() {
		StringBuffer sb = new StringBuffer(10);
		sb.append(header);
		sb.append(this.getClass().getName());
		sb.append("\n");
		if (DNET_LIST != null) {
			sb.append("\tNumber of networks = ");
			sb.append(DNET_LIST.length);
			sb.append("\n");
			for (int i = 0; i < DNET_LIST.length; i++) {
				sb.append("\tNet [");
				sb.append(i);
				sb.append("] =");
				sb.append(DNET_LIST[i]);
				sb.append("\n");
			}
		}
		return sb.toString();
	}

	/**
	 * Sets destination list
	 * 
	 * @param DNET_LIST
	 *            list of destinations
	 */
	public void SetDestinations(int[] DNET_LIST) {
		this.DNET_LIST = DNET_LIST;
	}

	/**
	 * Sets destination
	 * 
	 * @param DNET
	 *            destination
	 */
	public void SetDestinations(int DNET) {
		this.DNET_LIST = new int[1];
		DNET_LIST[0] = DNET;
	}

	public int[] getDestinations() {
		return DNET_LIST;
	}
}
