package com.synapsense.bacnet.network.pdu.apdu.datatypes;

public enum ConfirmedServiceChoice {
acknowledgeAlarm(0), confirmedCOVNotification(1), confirmedEventNotification(2), getAlarmSummary(3), getEnrollmentSummary(
        4), getEventInformation(29), subscribeCOV(5), subscribeCOVProperty(28), lifeSafetyOperation(27), atomicReadFile(
        6), atomicWriteFile(7), addListElement(8), removeListElement(9), createObject(10), deleteObject(11), readProperty(
        12), readPropertyConditional(13), readPropertyMultiple(14), readRange(26), writeProperty(15), writePropertyMultiple(
        16), deviceCommunicationControl(17), confirmedPrivateTransfer(18), confirmedTextMessage(19), reinitializeDevice(
        20), vtOpen(21), vtClose(22), vtData(23), authenticate(24), requestKey(25);

private final int Choice;

ConfirmedServiceChoice(int Choice) {
	this.Choice = Choice;
}

public int getCode() {
	return Choice;
}

public static ConfirmedServiceChoice valueOf(int value) {
	switch (value) {
	case 0:
		return acknowledgeAlarm;
	case 1:
		return confirmedCOVNotification;
	case 2:
		return confirmedEventNotification;
	case 3:
		return getAlarmSummary;
	case 4:
		return getEnrollmentSummary;
	case 29:
		return getEventInformation;
	case 5:
		return subscribeCOV;
	case 28:
		return subscribeCOVProperty;
	case 27:
		return lifeSafetyOperation;
	case 6:
		return atomicReadFile;
	case 7:
		return atomicWriteFile;
	case 8:
		return addListElement;
	case 9:
		return removeListElement;
	case 10:
		return createObject;
	case 11:
		return deleteObject;
	case 12:
		return readProperty;
	case 13:
		return readPropertyConditional;
	case 14:
		return readPropertyMultiple;
	case 26:
		return readRange;
	case 15:
		return writeProperty;
	case 16:
		return writePropertyMultiple;
	case 17:
		return deviceCommunicationControl;
	case 18:
		return confirmedPrivateTransfer;
	case 19:
		return confirmedTextMessage;
	case 20:
		return reinitializeDevice;
	case 21:
		return vtOpen;
	case 22:
		return vtClose;
	case 23:
		return vtData;
	case 24:
		return authenticate;
	case 25:
		return requestKey;
	default:
		throw new IllegalArgumentException("Unable to identify enum type");
	}
}
}
