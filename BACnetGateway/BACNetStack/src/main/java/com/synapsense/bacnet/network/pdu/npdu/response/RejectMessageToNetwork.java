/**
 * 
 */
package com.synapsense.bacnet.network.pdu.npdu.response;

import com.synapsense.bacnet.network.jni.BACNetException;
import com.synapsense.bacnet.network.jni.Property;
import com.synapsense.bacnet.network.jni.PropertyNotFoundException;
import com.synapsense.bacnet.network.media.MPDUHeader;
import com.synapsense.bacnet.network.media.Media;
import com.synapsense.bacnet.network.pdu.npdu.NPDUBasePacket;
import com.synapsense.bacnet.network.pdu.npdu.NPDUMessageType;
import com.synapsense.scheduler.SimpleTaskUnit;

/**
 * @author Alexander Borisov This message is indicated by a Message Type of
 *         X'03' followed by an octet indicating the reason for the rejection
 *         and a 2- octet network number (see Figure 6-7). It is directed to the
 *         node that originated the message being rejected, as indicated by the
 *         source address information in that message. The rejection reason
 *         octet shall contain an unsigned integer with one of the following
 *         values: 0: Other error. 1: The router is not directly connected to
 *         DNET and cannot find a router to DNET on any directly connected
 *         network using Who-Is-Router-To-Network messages. 2: The router is
 *         busy and unable to accept messages for the specified DNET at the
 *         present time. 3: It is an unknown network layer message type. 4: The
 *         message is too long to be routed to this DNET.
 */

public class RejectMessageToNetwork extends NPDUBasePacket implements SimpleTaskUnit {
	protected int NDNET;
	protected int RejectReason;

	/**
	 * Creates RejectMessageToNetwork packet with header
	 * 
	 * @param header
	 *            header
	 */
	public RejectMessageToNetwork(MPDUHeader header) {
		this.header = header;
	}

	/**
	 * Creates RejectMessageToNetwork packet
	 * 
	 * @param header
	 *            header
	 * @param reason
	 *            reject reason
	 * @param NDNET
	 *            dnet
	 */
	public RejectMessageToNetwork(MPDUHeader header, int reason, int NDNET) {
		this.header = header;
		RejectReason = reason;
		this.NDNET = NDNET;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsense.scheduler.SimpleTaskUnit#doTask()
	 */
	@Override
	public void Execute() throws Exception {
	}

	@Override
	public void receive() throws PropertyNotFoundException {
		NDNET = header.media.readShort(header.packetId, Property.NDNET) & 0xFFFF;
		RejectReason = header.media.readByte(header.packetId, Property.REJECT_REASON) & 0xFF;
	}

	@Override
	public void send() throws BACNetException {
		header.send();
		if (logger.isDebugEnabled())
			logger.debug("sending " + this.toString());
		Media reader = header.media;
		reader.setByte(header.packetId, Property.MESSAGE_TYPE,
		        (byte) NPDUMessageType.Reject_Message_To_Network.getCode());
		reader.setShort(header.packetId, Property.NDNET, (short) NDNET);
		reader.setByte(header.packetId, Property.REJECT_REASON, (byte) RejectReason);
		reader.sendPacket(header.packetId);
	}

	@Override
	public void failureAction(Throwable t) {
		// TODO Auto-generated method stub

	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer(10);
		sb.append(header);
		sb.append(this.getClass().getName());
		sb.append("\n");
		sb.append("\tNDNET=");
		sb.append(NDNET);
		sb.append("\n");
		sb.append("\tRejectReason=");
		sb.append(RejectReason);
		sb.append("\n");
		return sb.toString();
	}

	public int getNDNET() {
		return NDNET;
	}

	public void setNDNET(int nDNET) {
		NDNET = nDNET;
	}

	public int getRejectReason() {
		return RejectReason;
	}

	public void setRejectReason(int rejectReason) {
		RejectReason = rejectReason;
	}

}
