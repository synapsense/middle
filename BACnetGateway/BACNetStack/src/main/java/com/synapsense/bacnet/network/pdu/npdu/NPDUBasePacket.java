package com.synapsense.bacnet.network.pdu.npdu;

import com.synapsense.bacnet.network.jni.Property;
import com.synapsense.bacnet.network.jni.PropertyNotFoundException;
import com.synapsense.bacnet.network.media.MPDUHeader;
import com.synapsense.bacnet.network.pdu.PDUBasePacket;
import com.synapsense.bacnet.network.pdu.npdu.request.EstablishConnectionToNetwork;
import com.synapsense.bacnet.network.pdu.npdu.request.InitializeRoutingTable;
import com.synapsense.bacnet.network.pdu.npdu.request.WhoIsRouterToNetwork;
import com.synapsense.bacnet.network.pdu.npdu.response.DisconnectConnectionToNetwork;
import com.synapsense.bacnet.network.pdu.npdu.response.IAmRouterToNetwork;
import com.synapsense.bacnet.network.pdu.npdu.response.ICouldBeRouterToNetwork;
import com.synapsense.bacnet.network.pdu.npdu.response.InitializeRoutingTableAck;
import com.synapsense.bacnet.network.pdu.npdu.response.RejectMessageToNetwork;
import com.synapsense.bacnet.network.pdu.npdu.response.RouterAvailableToNetwork;
import com.synapsense.bacnet.network.pdu.npdu.response.RouterBusyToNetwork;
import com.synapsense.scheduler.SimpleTaskUnit;

public abstract class NPDUBasePacket extends PDUBasePacket implements SimpleTaskUnit {

	/**
	 * Creates instance of {@code}NPDUBasePacket
	 * 
	 * @param header
	 *            header of packet
	 * @return instance of {@code}NPDUBasePacket
	 * @throws PropertyNotFoundException
	 *             if some property is missing
	 */
	public static NPDUBasePacket getPacket(MPDUHeader header) throws PropertyNotFoundException {
		NPDUMessageType m = NPDUMessageType.valueOf(header.media.readByte(header.packetId, Property.MESSAGE_TYPE));

		switch (m) {
		case Who_Is_Router_To_Network: {
			return new WhoIsRouterToNetwork(header);
		}
		case I_Am_Router_To_Network: {
			return new IAmRouterToNetwork(header);
		}
		case I_Could_Be_Router_To_Network: {
			return new ICouldBeRouterToNetwork(header);
		}
		case Reject_Message_To_Network: {
			return new RejectMessageToNetwork(header);
		}
		case Router_Busy_To_Network: {
			return new RouterBusyToNetwork(header);
		}
		case Router_Available_To_Network: {
			return new RouterAvailableToNetwork(header);
		}
		case Initialize_Routing_Table: {
			return new InitializeRoutingTable(header);
		}
		case Initialize_Routing_Table_Ack: {
			return new InitializeRoutingTableAck(header);
		}

		case Establish_Connection_To_Network: {
			return new EstablishConnectionToNetwork(header);
		}
		case Disconnect_Connection_To_Network: {
			return new DisconnectConnectionToNetwork(header);
		}
		default: {
			return null;
		}
		}
	}
}
