package com.synapsense;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import com.synapsense.bacnet.network.jni.UnableToSetUpInterfaceException;
import com.synapsense.bacnet.network.media.MPDUHeader;
import com.synapsense.bacnet.network.media.Media;
import com.synapsense.bacnet.network.media.MediaNames;
import com.synapsense.bacnet.network.pdu.apdu.svc.SvcResult;
import com.synapsense.bacnet.network.pdu.apdu.svc.request.WhoIs;

public class test {

	private static final Logger logger = Logger.getLogger(test.class);

	public static String getMediaAddress(String mac) {
		byte b[] = new byte[6];
		String sp[] = mac.split(":");
		for (int i = 0; i < sp.length; i++) {
			b[i] = (byte) Integer.parseInt(sp[i]);
		}
		b[4] = (byte) 0xBA;
		b[5] = (byte) 0xC0;
		return new String(b);
	}

	public static String getBroadcast(String address, int mask) {

		byte[] addr = new byte[4];
		String s[] = address.split(":");
		for (int i = 0; i < s.length; i++) {
			addr[i] = Byte.parseByte(s[i]);
		}

		int ip = addr[0] & 0xFF;
		ip <<= 8;
		ip |= addr[1] & 0xFF;
		ip <<= 8;
		ip |= addr[2] & 0xFF;
		ip <<= 8;
		ip |= addr[3] & 0xFF;
		logger.debug("IP address: " + (addr[0] & 0xFF) + "." + (addr[1] & 0xFF) + "." + (addr[2] & 0xFF) + "."
		        + (addr[3] & 0xFF) + " {" + Integer.toBinaryString(ip) + "}");

		int m = 0xFFFFFFFF;
		m = (m << (32 - mask)) & 0xFFFFFFFF;
		logger.debug("Mask: " + ((m >> 24) & 0xFF) + "." + ((m >> 16) & 0xFF) + "." + ((m >> 8) & 0xFF) + "."
		        + (m & 0xFF) + " {" + Integer.toBinaryString(m) + "}");

		long subnet = ((long) ip & m) & 0xFFFFFFFF;
		logger.debug("Subnet IP: " + ((subnet >> 24) & 0xFF) + "." + ((subnet >> 16) & 0xFF) + "."
		        + ((subnet >> 8) & 0xFF) + "." + (subnet & 0xFF) + " {" + Long.toBinaryString(subnet) + "}");

		long b = (subnet | (~m)) & 0xFFFFFFFF;
		logger.debug("Broadcast: " + ((b >> 24) & 0xFF) + "." + ((b >> 16) & 0xFF) + "." + ((b >> 8) & 0xFF) + "."
		        + (b & 0xFF) + " {" + Long.toBinaryString(b) + "}");

		byte[] broadcast = { (byte) (b >> 24), (byte) (b >> 16), (byte) (b >> 8), (byte) b, (byte) 0xBA, (byte) 0xC0 };
		return new String(broadcast);

	}

	/**
	 * @param args
	 * @throws InstantiationException
	 */
	public static void main(String[] args) throws InstantiationException {

		// System.out.println ("Hello="+"hello".hashCode());
		// System.out.println ("Hello="+"helllo".hashCode());
		// System.exit(-1);
		// RoutingService.addRoute("2_0_main");
		// Ethernet media = new Ethernet();
		// media.startCapture(1,Boolean.parseBoolean(System.getProperty("PacketReader.debug","false")));
		// MPDUHeader header = new MPDUHeader();
		// header.media = media;
		//
		// Configuration.OWN_ADDR = header.media.getActiveEthMac();

		PropertyConfigurator.configure("C:/git/master/SynapControl/BacNetPlugin/lib/log4j.properties");
		// test t = new test();
		// String b = t.getBroadcast("10:1:1:35", 24);
		// //

		// PacketReader.getInstance().setLogConfigPath("C:/git/master/SynapControl/BacNetPlugin/conf/bnparser.log.conf");

		Media media = MediaNames.BACnetIP.newMedia(1);
		try {
			media.startCapture(0, false);
		} catch (UnableToSetUpInterfaceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		MPDUHeader header = new MPDUHeader();
		header.media = media;

		testStackWrapper.startCapture(header);

		// 1 - Who-Is
		header.SRC_MAC = media.getActiveEthMac();
		header.DST_MAC = media.getBroadcast(); // getMediaAddress("192:168:103:4");

		WhoIs request = new WhoIs(header);
		request.createSession();
		SvcResult results = request.getResult();

		// WhoIsRouterToNetwork request = new WhoIsRouterToNetwork(header);
		// IAmRouterToNetwork request = new IAmRouterToNetwork(header);
		// request.SetDestinations(3);
		// request.setNdnet(3);

		// try {
		// request.send();
		// } catch (Exception e) {
		// // TODO Auto-generated catch block
		// }

		int a = 1;

		// while (true) {
		// try {
		// NetworkProcessor.getInstance().addPacket(header.receive());
		// } catch (PropertyNotFoundException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		// }
	}

}
