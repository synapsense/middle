package com.synapsense.scheduler;

public interface TimeoutTaskUnit extends SimpleTaskUnit {
	/**
	 * Calls if task timeout occurs
	 */
	public void timeoutAction();
}
