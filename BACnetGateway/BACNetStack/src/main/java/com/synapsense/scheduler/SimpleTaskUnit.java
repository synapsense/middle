package com.synapsense.scheduler;

/**
 * This interface should be implements in class which goods for TaskSheduller
 * 
 * @author Alexander Borisov
 * 
 */

public interface SimpleTaskUnit {
	/**
	 * Execute task in {@code}TaskScheduler
	 * 
	 * @throws Exception
	 *             if any errors occurs
	 */
	public void Execute() throws Exception;

	/**
	 * If task execution fails this method will be called
	 */
	public void failureAction(Throwable t);
}
