package com.synapsense.scheduler;

public interface ComplexTaskUnit extends TimeoutTaskUnit {
	/**
	 * Calls for stops timer
	 */
	public void stopTimer();
}
