package com.synapsense.scheduler;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import org.apache.log4j.Logger;

/**
 * This class provides simultaneous execution of tasks
 * 
 * @author Alexander Borisov
 * 
 */
public class TaskScheduler {
	/**
	 * instance of TaskScheduler
	 */
	static private TaskScheduler instance;
	protected Logger logger = Logger.getLogger(TaskScheduler.class);
	final static int INCOMING_POOL_SIZE = Integer.parseInt(System.getProperty("TaskScheduler.incomingpool", "10"));
	final static int OUTGOING_POOL_SIZE = Integer.parseInt(System.getProperty("TaskScheduler.outgoingpool", "10"));
	final static int SVC_POOL_SIZE = Integer.parseInt(System.getProperty("TaskScheduler.svcpool", "5"));

	ExecutorService incomingPool;
	ExecutorService outgoingPool;
	ExecutorService svcPool;

	/**
	 * initial pool creation
	 */
	static {
		instance = new TaskScheduler();
	}

	/**
	 * Creates pool for threads
	 */
	private TaskScheduler() {
		incomingPool = Executors.newFixedThreadPool(INCOMING_POOL_SIZE);
		outgoingPool = Executors.newFixedThreadPool(OUTGOING_POOL_SIZE);
		svcPool = Executors.newFixedThreadPool(SVC_POOL_SIZE);
		logger.debug("Pools have been created successully");
	}

	/**
	 * Gets instance of TaskScheduler
	 * 
	 * @return instance of TaskScheduler
	 */
	public static TaskScheduler getInstance() {
		return instance;
	}

	/**
	 * Add and run task to scheduler
	 * 
	 * @param unit
	 *            class which implements SimpleTaskUnit interface
	 */
	public void addIncomingTask(SimpleTaskUnit unit) {
		incomingPool.execute(new RunnableTask(unit));
		// if (logger.isDebugEnabled())
		// logger.debug("task [" + unit.toString() +
		// "] has been added to TaskScheduler");

	}

	/**
	 * Add and run task to scheduler
	 * 
	 * @param unit
	 *            class which implements TimeoutTaskUnit interface
	 * @param duration
	 *            task duration -1 if infinite duration
	 */
	public void addIncomingTask(TimeoutTaskUnit unit, int duration) {
		incomingPool.execute(new RunnableTimeoutTask(unit, duration));
		// if (logger.isDebugEnabled())
		// logger.debug("task [" + unit.toString() +
		// "] has been added to TaskScheduler");

	}

	/**
	 * Add and run task to scheduler
	 * 
	 * @param unit
	 *            class which implements SimpleTaskUnit interface
	 */
	public void addOutgoingTask(SimpleTaskUnit unit) {
		outgoingPool.execute(new RunnableTask(unit));
		logger.debug("TASK [" + unit.toString() + "] has been added to TaskScheduler");

	}

	/**
	 * Add and run task to scheduler
	 * 
	 * @param unit
	 *            class which implements TimeoutTaskUnit interface
	 * @param duration
	 *            task duration -1 if infinite duration
	 */
	public void addOutgoingTask(final TimeoutTaskUnit unit, final int duration) {
		outgoingPool.execute(new RunnableTimeoutTask(unit, duration));
		logger.debug("TASK [" + unit.toString() + "] has been added to TaskScheduler, ["
		        + Thread.currentThread().getName() + "]");

	}

	/**
	 * Add and run task to scheduler
	 * 
	 * @param unit
	 *            class which implements SimpleTaskUnit interface
	 */
	public void addSvcTask(SimpleTaskUnit unit) {
		svcPool.execute(new RunnableTask(unit));
		// if (logger.isDebugEnabled())
		// logger.debug("task [" + unit.toString() +
		// "] has been added to TaskScheduler");

	}

	/**
	 * Add and run task to scheduler
	 * 
	 * @param unit
	 *            class which implements TimeoutTaskUnit interface
	 * @param duration
	 *            task duration -1 if infinite duration
	 */
	public void addSvcTask(TimeoutTaskUnit unit, int duration) {
		svcPool.execute(new RunnableTimeoutTask(unit, duration));
		// if (logger.isDebugEnabled())
		// logger.debug("task [" + unit.toString() +
		// "] has been added to TaskScheduler");

	}

	/** Invokes shutdownNow() on all executors */
	public void shutdownNow() {
		incomingPool.shutdownNow();
		outgoingPool.shutdownNow();
		svcPool.shutdownNow();
	}
}

/**
 * Class responsible for background task execution
 * 
 * @author Alexander Borisov
 * 
 */
class RunnableTask implements Runnable {
	private SimpleTaskUnit task;
	static Logger logger = Logger.getLogger(RunnableTask.class);

	/**
	 * Creates instance of task
	 * 
	 * @param unit
	 */
	public RunnableTask(SimpleTaskUnit unit) {
		this.task = unit;
		logger.debug("new instance of TaskThread created");
	}

	@Override
	public void run() {
		try {
			task.Execute();
			logger.debug("task has been finished");
		} catch (Exception t) {
			try {
				logger.warn("doTask() failure, executing failure action", t);
				task.failureAction(t);
			} catch (Throwable t1) {
				logger.warn("failureAction() failure, ", t1);
			}
		}
	}
}

/**
 * 
 * @author Alexander Borisov This class is responsible for task execution with
 *         timeout
 */
class RunnableTimeoutTask implements Runnable {
	private static ExecutorService timeoutExecutor = Executors.newFixedThreadPool(TaskScheduler.INCOMING_POOL_SIZE
	        + TaskScheduler.OUTGOING_POOL_SIZE);
	private final TimeoutTaskUnit unit;
	private final int duration;
	static Logger logger = Logger.getLogger(RunnableTimeoutTask.class);

	/**
	 * Default constructor for TimeoutTaskThread
	 * 
	 * @param unit
	 *            class which implements TimeoutTaskUnit interface
	 * @param duration
	 *            task duration </br> -1 if task have infinite time
	 */
	public RunnableTimeoutTask(TimeoutTaskUnit unit, int duration) {
		this.unit = unit;
		this.duration = duration;
	}

	@Override
	public void run() {
		FutureTask<Boolean> timeoutTask = new FutureTask<Boolean>(new Callable<Boolean>() {

			@Override
			public Boolean call() throws Exception {
				if (duration != -1) {
					unit.Execute();
					logger.debug("Execution of task has been finished");
					return true;
				}
				return false;
			}
		});
		timeoutExecutor.execute(timeoutTask);

		try {
			timeoutTask.get(duration, TimeUnit.MILLISECONDS);
		} catch (InterruptedException e) {
			logger.debug("Execution of task is failure", e);
			unit.failureAction(e);
		} catch (ExecutionException e) {
			logger.debug("Execution of task is failure", e);
			unit.failureAction(e);
		} catch (TimeoutException e) {
			logger.debug("Execution of task timedout", e);
			unit.timeoutAction();
		}

	}

}