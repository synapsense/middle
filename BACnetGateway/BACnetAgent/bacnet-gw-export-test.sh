#!/usr/bin/env bash

# This scripts automates the execution of bacnet-gw-export with a variety of parameters.
#
# How to run:
#  - If you don't run from SSENV, make sure that JAVA_HOME corresponds to the version for this branch.
#  - Do "mvn install" in this directory.
#  - Start ES.
#  - Start BG or make sure that ../../runtime/BACnet GW/conf contains instances.map.xml.
#  - Run this script.

# NOTE: Tests using specific object IDs are disabled by default because they need to be customized for your environment.
# Obtain the IDs from MapSense or from then output of this test and then enter them below:

#DC_ID=1220
#ZONE_ID=1223
#RACK_ID=1286
#CRAC_ID=1811
#PRESSURE_ID=1467
#VERTICALTEMPERATURE_ID=1507
#WSNNODE_ID=1282
#WSNSENSOR_ID=1274

RACK_OBJECTS="DC ZONE RACK RACK.cTop WSNNODE WSNSENSOR"
RACK_INSTANCE_OBJECTS="DC:$DC_ID ZONE:$ZONE_ID RACK:$RACK_ID RACK:$RACK_ID.cTop WSNNODE:$WSNNODE_ID WSNSENSOR:$WSNSENSOR_ID"
RACK_TEMPLATE='$DC.name$[DC:$DC.id$]/$ZONE.name$[ZONE:$ZONE.id$]/$RACK.name$[RACK:$RACK.id$]/$WSNNODE.name$[WSNNODE:$WSNNODE.id$]/$WSNSENSOR.name$[WSNSENSOR:$WSNSENSOR.id$] ==> Device:$DEVICE.id$/Sensor:$OBJECT.id$'

CRAC_OBJECTS="CRAC CRAC.returnT"
CRAC_INSTANCE_OBJECTS="CRAC:$CRAC_ID CRAC:$CRAC_ID.returnT"
CRAC_TEMPLATE='$DC.name$[DC:$DC.id$]/$ZONE.name$[ZONE:$ZONE.id$]/$CRAC.name$[CRAC:$CRAC.id$]/$WSNNODE.name$[WSNNODE:$WSNNODE.id$]/$WSNSENSOR.name$[WSNSENSOR:$WSNSENSOR.id$] ==> Device:$DEVICE.id$/Sensor:$OBJECT.id$'

PRESSURE_OBJECTS="PRESSURE PRESSURE.pressure"
PRESSURE_INSTANCE_OBJECTS="PRESSURE:$PRESSURE_ID PRESSURE:$PRESSURE_ID.pressure"
PRESSURE_TEMPLATE='$DC.name$[DC:$DC.id$]/$ZONE.name$[ZONE:$ZONE.id$]/$PRESSURE.name$[PRESSURE:$PRESSURE.id$]/$WSNNODE.name$[WSNNODE:$WSNNODE.id$]/$WSNSENSOR.name$[WSNSENSOR:$WSNSENSOR.id$] ==> Device:$DEVICE.id$/Sensor:$OBJECT.id$'

VERTICALTEMPERATURE_OBJECTS="VERTICALTEMPERATURE VERTICALTEMPERATURE.top"
VERTICALTEMPERATURE_INSTANCE_OBJECTS="VERTICALTEMPERATURE:$VERTICALTEMPERATURE_ID VERTICALTEMPERATURE:$VERTICALTEMPERATURE_ID.top"
VERTICALTEMPERATURE_TEMPLATE='$DC.name$[DC:$DC.id$]/$ZONE.name$[ZONE:$ZONE.id$]/$VERTICALTEMPERATURE.name$[VERTICALTEMPERATURE:$VERTICALTEMPERATURE.id$]/$WSNNODE.name$[WSNNODE:$WSNNODE.id$]/$WSNSENSOR.name$[WSNSENSOR:$WSNSENSOR.id$] ==> Device:$DEVICE.id$/Sensor:$OBJECT.id$'


BACNET_GW_EXPORT="$(realpath "../../runtime/BACnet GW/bin/bacnet-gw-export")"

function invoke-export {
	local OBJECTS="$1"
	local TEMPLATE="$2"
	for object in $OBJECTS; do
		echo
		echo "TESTING $object"
		echo
		"$BACNET_GW_EXPORT" -object "$object" -template "$TEMPLATE"
	done
}

# Fail fast
set -e

invoke-export "$RACK_OBJECTS" "$RACK_TEMPLATE"
invoke-export "$CRAC_OBJECTS" "$CRAC_TEMPLATE"
invoke-export "$PRESSURE_OBJECTS" "$PRESSURE_TEMPLATE"
invoke-export "$VERTICALTEMPERATURE_OBJECTS" "$VERTICALTEMPERATURE_TEMPLATE"

[[ -n $RACK_ID ]] && invoke-export "$RACK_INSTANCE_OBJECTS" "$RACK_TEMPLATE"
[[ -n $CRAC_ID ]] && invoke-export "$CRAC_INSTANCE_OBJECTS" "$CRAC_TEMPLATE"
[[ -n $PRESSURE_ID ]] && invoke-export "$PRESSURE_INSTANCE_OBJECTS" "$PRESSURE_TEMPLATE"
[[ -n $VERTICALTEMPERATURE_ID ]] && invoke-export "$VERTICALTEMPERATURE_INSTANCE_OBJECTS" "$VERTICALTEMPERATURE_TEMPLATE"

