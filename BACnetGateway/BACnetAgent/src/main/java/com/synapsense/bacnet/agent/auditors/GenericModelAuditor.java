package com.synapsense.bacnet.agent.auditors;

import java.beans.IntrospectionException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import com.synapsense.bacnet.agent.BACnetGWConfig;
import com.synapsense.bacnet.agent.wrappers.devices.CustomDevice;
import com.synapsense.bacnet.agent.wrappers.objects.CustomInput;
import com.synapsense.bacnet.objects.InvalidPropertyException;
import com.synapsense.bacnet.system.BACnetNetwork;
import com.synapsense.bacnet.system.BACnetSystem;
import com.synapsense.bacnetgw.config.BacnetDevice;
import com.synapsense.bacnetgw.config.BacnetNetwork;
import com.synapsense.bacnetgw.config.BacnetObject;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.exception.PropertyNotFoundException;
import com.synapsense.exception.UnableToConvertPropertyException;
import com.synapsense.service.Environment;

public class GenericModelAuditor {

	static final Logger logger = Logger.getLogger(GenericModelAuditor.class);
	private BacnetNetwork mappedObjects;
	private BACnetGWConfig config;
	private Map<TO<?>, BacnetDevice> devices = new HashMap<TO<?>, BacnetDevice>();
	private Map<TO<?>, Set<String>> devicesToProps = new HashMap<TO<?>, Set<String>>();

	public GenericModelAuditor(BACnetGWConfig config, BacnetNetwork mapping) {
		mappedObjects = mapping;
		this.config = config;
	}

	public void initModel(Environment env) throws IntrospectionException,
			InvalidPropertyException {
		int ownNetId = config.getStackConfig().getOwnNet();
		BACnetNetwork net = BACnetSystem.getInstance().getNetwork(ownNetId);
		if (net == null) {
			throw new IllegalArgumentException(
					"Unable to find BACnetGateway network with id [" + ownNetId
							+ "], can not initialize generic model.");
		}

		// first get all the info from the configuration
		for (BacnetDevice device : mappedObjects.getBacnetDevice()) {
			String strTO = device.getTo();
			try {
				TO<?> to = TOFactory.getInstance().loadTO(strTO);
				devices.put(to, device);
				for (BacnetObject obj : device.getBacnetObject()) {
					Set<String> tos = devicesToProps.get(to);
					if (tos == null) {
						tos = new HashSet<String>();
						devicesToProps.put(to, tos);
					}
					tos.add(obj.getProperty());
				}
			} catch (IllegalArgumentException e) {
				logger.warn("Unable to parce TO [" + strTO
						+ "]. The object won't be accessable.");
			}
		}
		// create devices and objects accordingly
		Set<TO<?>> devicesTOs = devices.keySet();
		for (TO<?> to : devicesTOs) {
			Map<String, Object> deviceProperties = new HashMap<String, Object>();
			String deviceName;
			try {
				deviceName = env.getPropertyValue(to,
						CustomDevice.DeviceFld.name.toString(), String.class);
			} catch (ObjectNotFoundException e1) {
				// Object doesn't exist, it has to be skipped
				continue;
			} catch (PropertyNotFoundException e1) {
				logger.debug("Unable to find 'name' propery for object "
						+ to.toString());
				deviceName = to.toString();
			} catch (UnableToConvertPropertyException e1) {
				logger.debug("Unable to read 'name' propery for object "
						+ to.toString());
				deviceName = to.toString();
			}

			deviceProperties.put(CustomDevice.DeviceFld.name.toString(),
					deviceName);
			deviceProperties.put(CustomDevice.DeviceFld.desc.toString(),
					"Device for object [" + to + "]");
			deviceProperties.put(CustomDevice.DeviceFld.location.toString(),
					"Environment Server");
			BacnetDevice mappedDevice = devices.get(to);
			if (mappedDevice != null) {
				try {
					Integer id = Integer.parseInt(mappedDevice.getId());
					CustomDevice device = new CustomDevice(config,
							deviceProperties, id);
					Set<String> props = devicesToProps.get(to);
					int inputId = 0;
					for (String prop : props) {
						CustomInput input = new CustomInput(inputId, to, prop);
						device.addInput(input);
						inputId++;
					}
					net.addDevice(device.getWrapper());
				} catch (NumberFormatException e) {
					continue;
				}
			}

		}
	}
}
