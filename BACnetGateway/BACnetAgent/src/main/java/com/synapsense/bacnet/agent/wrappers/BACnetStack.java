package com.synapsense.bacnet.agent.wrappers;

import com.synapsense.bacnet.agent.BACnetGWConfig;
import com.synapsense.bacnet.agent.BACnetGWConfig.StackConfig;
import com.synapsense.bacnet.agent.NetworkInterface;
import com.synapsense.bacnet.network.Configuration;
import com.synapsense.bacnet.network.NetworkProcessor;
import com.synapsense.bacnet.network.jni.UnableToSetUpInterfaceException;
import com.synapsense.bacnet.network.media.MPDUHeader;
import com.synapsense.bacnet.network.media.Media;
import com.synapsense.bacnet.network.routing.Route;
import com.synapsense.bacnet.network.routing.RoutingService;
import com.synapsense.scheduler.SimpleTaskUnit;
import java.lang.Thread.State;
import java.util.Collection;
import org.apache.log4j.Logger;

/**
 * This class is responsible for listening and processing incoming BACnet
 * packets
 */
public class BACnetStack implements SimpleTaskUnit {
	private final static Logger logger = Logger.getLogger(BACnetStack.class);
	private StackConfig config = null;
	private Collection<NetworkInterface> networkInterfaces;
	private Collection<Media> media = null;

	/**
	 * Initializes BACnet stack and Routing Service
	 * 
	 * @param config
	 */
	public void init(BACnetGWConfig config) {
		this.config = config.getStackConfig();
		networkInterfaces = this.config.getNetworkInterfaces();
		media = this.config.getSupportedMedia();
		Configuration.OWN_NET = this.config.getOwnNet();
		Configuration.DEFAULT_SESSION_TIMEOUT = config.getAdpuTimeout();
	}

	/**
	 * Starts the capturing
	 */
	public void Execute() {
		try {
			if ((config == null) || (media == null)) {
				throw new IllegalStateException("run() must be invoked only after init()");
			}

			int interfaceIndex = -1;
			int mask = -1;

			for (NetworkInterface net : networkInterfaces) {
				Media ip = null;
				try {
					interfaceIndex = net.getInterfaceIndex();
					mask = net.getIpMask();
					ip = net.getMedia();
					ip.startCapture(interfaceIndex, net.isAllowCaptureOwnMAC());
					Configuration.OWN_ADDR = ip.getActiveEthMac();
					if (Configuration.OWN_ADDR != null) {
						RoutingService.addRoute(new Route(Configuration.OWN_NET, 0, "", Configuration.OWN_ADDR));
						logger.info("Connected to IP: {interfaceIndex=" + interfaceIndex + "; mask=" + mask + "}");
					} else {
						throw new UnableToSetUpInterfaceException("Could not get own address");
					}
				} catch (UnableToSetUpInterfaceException e) {
					media.remove(ip);
					logger.error("Unable to connect to IP: {interfaceIndex=" + interfaceIndex + "; mask=" + mask + "}",e);
				}
			}

			if (!media.isEmpty()) {
				logger.debug("Starting the capturing the IP media...");
				NetworkProcessor.getInstance().start();
				// [BC 09/2015] How silly is this test?
				while (Thread.currentThread().getState() != State.TERMINATED) {
					try {
						for (Media ip : media) {
							MPDUHeader header = new MPDUHeader();
							header.media = ip;
							// The thread blocks here on no activity. Graceful shutdown is not possible.
							NetworkProcessor.getInstance().addPacket(header.receive());
						}
					} catch (Exception e) {
						logger.warn("Unknown packet", e);
					}
				}
			} else {
				throw new IllegalArgumentException("Unable to connect to any of configured interfaces. Cannot initialize BACnet GW");
			}
		} catch (Exception e) {
			logger.fatal("BACNet GW encountered a fatal exception and will exit.", e);
			System.exit(1);
		}
	}

	@Override
	public void failureAction(Throwable t) {
		// The exceptions are caught in Execute(), so this will never get called
		logger.info("Unexpected exception.", t);
	}
}
