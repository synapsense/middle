package com.synapsense.bacnet.agent.auditors;

import com.synapsense.bacnet.agent.model.BACnetGWModel;
import com.synapsense.dto.ChangedValueTO;
import com.synapsense.dto.DTO;
import com.synapsense.service.nsvc.events.ObjectCreatedEvent;
import com.synapsense.service.nsvc.events.ObjectCreatedEventProcessor;
import com.synapsense.service.nsvc.events.ObjectDeletedEvent;
import com.synapsense.service.nsvc.events.ObjectDeletedEventProcessor;
import com.synapsense.service.nsvc.events.PropertiesChangedEvent;
import com.synapsense.service.nsvc.events.PropertiesChangedEventProcessor;
import com.synapsense.service.nsvc.events.RelationRemovedEvent;
import com.synapsense.service.nsvc.events.RelationRemovedEventProcessor;
import com.synapsense.service.nsvc.events.RelationSetEvent;
import com.synapsense.service.nsvc.events.RelationSetEventProcessor;
import java.util.Map;

class NodeEventProcessor implements ObjectCreatedEventProcessor, ObjectDeletedEventProcessor, RelationSetEventProcessor,
		RelationRemovedEventProcessor, PropertiesChangedEventProcessor {

	private final BACnetGWModel model;

	NodeEventProcessor(BACnetGWModel model) {
		this.model = model;
	}

	@Override
	public ObjectCreatedEvent process(ObjectCreatedEvent oce) {
		Map<String, Object> properties = DTO.toMap(oce.getValues());
		model.addNode(oce.getObjectTO(), properties);
		return null;
	}

	@Override
	public ObjectDeletedEvent process(ObjectDeletedEvent ode) {
		model.removeNode(ode.getObjectTO());
		return null;
	}

	@Override
	public RelationSetEvent process(RelationSetEvent rse) {
		model.addNodeToNetwork(rse.getParentTO(), rse.getChildTO());
		return null;
	}

	@Override
	public RelationRemovedEvent process(RelationRemovedEvent rre) {
		model.removeNodeFromNetwork(rre.getParentTO(), rre.getChildTO());
		return null;
	}

	@Override
	public PropertiesChangedEvent process(PropertiesChangedEvent pce) {
		for (ChangedValueTO changeValueTO : pce.getChangedValues()) {
			model.updateNodeProperty(pce.getObjectTO(), changeValueTO);
		}
		return null;
	}
}