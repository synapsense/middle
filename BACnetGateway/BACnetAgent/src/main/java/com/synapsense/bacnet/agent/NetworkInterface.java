package com.synapsense.bacnet.agent;

import com.synapsense.bacnet.network.media.IP.IP;

public class NetworkInterface {

	private IP media;
	private int interfaceIndex = 0;
	private boolean allowCaptureOwnMAC = false;
	private int ipMask = 24;
	private int bacnetIPport = 47808;

	public NetworkInterface() {
	}

	public IP getMedia() {
		if (media == null) {
			media = new IP();
			media.setMask(ipMask);
			media.setPort(bacnetIPport);
		}
		return media;
	}

	public int getInterfaceIndex() {
		return interfaceIndex;
	}

	public void setInterfaceIndex(int interfaceIndex) {
		this.interfaceIndex = interfaceIndex;
	}

	public boolean isAllowCaptureOwnMAC() {
		return allowCaptureOwnMAC;
	}

	public void setAllowCaptureOwnMAC(boolean allowCaptureOwnMAC) {
		this.allowCaptureOwnMAC = allowCaptureOwnMAC;
	}

	public int getIpMask() {
		return ipMask;
	}

	public void setIPMask(int ipMask) {
		this.ipMask = ipMask;
	}

	public void setBacnetIPport(int bacnetIPport) {
		this.bacnetIPport = bacnetIPport;
	}

}
