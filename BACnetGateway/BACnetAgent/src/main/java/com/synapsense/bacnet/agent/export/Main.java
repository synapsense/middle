package com.synapsense.bacnet.agent.export;

import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.service.Environment;
import com.synapsense.util.ServerLoginModule;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.Properties;
import javax.naming.InitialContext;

public class Main {

	private static void printHelp() {
		//noinspection StringBufferReplaceableByString
		StringBuilder builder = new StringBuilder();
		builder.append("Usage: bacnet-gw-export -object <object> -template <template>\n\n");
		builder.append("where <options> includes:\n\n");
		builder.append("<object>   \tThe following values are possible: TYPE, TYPE.property,\n");
		builder.append("           \tTYPE:id,TYPE:id.property.\n");
		builder.append("           \tWhere TYPE is one of ES types, for example: 'DC','ZONE',\n");
		builder.append("           \t'NETWORK','RACK','CRAC','PRESSURE','NODE' and so on.\n\n");
		builder.append("           \t'property' is a property name of TYPE object which has a\n");
		builder.append("           \treference to a sensor.\n\n");
		builder.append("           \t'id' is unique identifier of TYPE object.\n\n");
		builder.append("<template> \tString template may contain a variables and optional text.\n");
		builder.append("           \tVariables must be in $...$ to be interpreted as variables.\n");
		builder.append("           \tEach variable has 2 predefined properties: 'name' and 'id'.\n");
		builder.append("           \tThere are 2 predefined BACNET variables. These are:\n");
		builder.append("           \t'DEVICE' and 'OBJECT'.\n");
		builder.append("           \tAlso it is possible to use any TYPE object in the capacity of\n");
		builder.append("           \tvariable.\n");
		builder.append("           \tFor example: $DC.id$, $ZONE.name$, $DEVICE.id$, $DEVICE.name$,\n");
		builder.append("           \t$OBJECT.id$, $OBJECT.name$, $NETWORK.name$ and so on.\n\n");

		builder.append("Usage example:\n\n");
		builder.append("            \tbacnet-gw-export -object RACK.cTop -template\n");
		builder.append("            \tDevice:$DEVICE.id$/host:47808/Analog-Input:$OBJECT.id$\n\n");
		builder.append("            \tOutput may be following:\n\n");
		builder.append("            \tDevice:77010/host:47808/Analog-Input:1\n");
		builder.append("            \tDevice:77023/host:47808/Analog-Input:5\n");
		builder.append("            \t...");

		System.out.println(builder.toString());
	}

	public static void main(String[] args) throws Exception {

		String selection = null, template = null;
		if (args.length == 4) {
			for (int i = 0; i < args.length; i++) {
				if (args[i].equals("-object")) { // In code I use the more appropriate name "selection"
					selection = args[i + 1];
				}
				if (args[i].equals("-template")) {
					template = args[i + 1];
				}
			}
		} else {
			printHelp();
			System.exit(1);
		}

		if (selection == null || template == null) {
			System.out.println("'-object' or '-template' argument is not specified.");
			printHelp();
			System.exit(1);
		}

		execute(selection, template);
	}

	private static void execute(String selection, String template) {
		try {
			Environment env = connect();
			Writer writer = new OutputStreamWriter(System.out);

			try {
				if (selection.contains(":")) { // is an instance
					if (selection.contains(".")) { // property specified
						String[] splitStr = selection.split("\\.");
						TO<?> object = TOFactory.getInstance().loadTO(splitStr[0].toUpperCase());
						String property = splitStr[1];
						BacnetGatewayExport.exportObjectProperty(env, writer, template, object, property);
					} else {
						TO<?> object = TOFactory.getInstance().loadTO(selection.toUpperCase());
						BacnetGatewayExport.exportObject(env, writer, template, object);
					}
				} else if (selection.contains(".")) { // property specified
					String[] splitStr = selection.split("\\.");
					String type = splitStr[0].toUpperCase();
					String property = splitStr[1];
					BacnetGatewayExport.exportProperty(env, writer, template, type, property);
				} else {
					String type = selection.toUpperCase();
					BacnetGatewayExport.exportType(env, writer, template, type);
				}
			} finally {
				// Flush manually, OutputStreamWriter is buffering internally
				writer.flush();
			}
		} catch (IllegalStateException e) {
			System.err.println("Failed to connect to ES. Export cannot proceed.");
			e.printStackTrace();
			System.exit(1);
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
	}

	/** Encapsulates the internal details of connecting to the Environment EJB on the ES server */
	private static Environment connect() throws Exception {
		Properties properties = new Properties();
		properties.put("java.naming.factory.url.pkgs", "org.jboss.ejb.client.naming");
		return ServerLoginModule.getService(new InitialContext(properties), "Environment", Environment.class);
	}
}
