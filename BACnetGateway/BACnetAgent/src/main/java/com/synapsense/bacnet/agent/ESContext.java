package com.synapsense.bacnet.agent;

import java.io.InputStream;
import java.util.Properties;

import javax.naming.InitialContext;

import com.synapsense.bacnet.agent.utils.ESTypes;
import org.apache.log4j.Logger;

import com.synapsense.service.AlertService;
import com.synapsense.service.Environment;
import com.synapsense.service.impl.audit.AuditorSvc;
import com.synapsense.service.nsvc.NotificationService;
import com.synapsense.transport.Proxy;
import com.synapsense.transport.TransportManager;
import com.synapsense.util.ServerLoginModule;

/**
 * Singleton class which realizes access to Environment API.
 * Try to connect to ES
 */
public class ESContext {

	private static final Logger log =Logger.getLogger(ESContext.class);

	private static final String SynapEnvConfFile = "escontext.properties";

	private static ESContext synapEnv = new ESContext();

	private Properties connectProperties = null;

	private Environment env = null;
	private AlertService alertService = null;
	private AuditorSvc auditorSvc = null;
	private Proxy<NotificationService> nsvcProxy;

	public static boolean testConnect() {
		log.info("Trying to connect to ES...");
		return synapEnv.connect();
	}

	private ESContext() {
		connectProperties = new Properties();
		try (InputStream jbossEjbIs = ESContext.class.getClassLoader().getResourceAsStream(SynapEnvConfFile)) {
            connectProperties.load(jbossEjbIs);
		} catch (Exception e) {
			log.fatal("Cannot load \"" + SynapEnvConfFile + "\" file", e);
			throw new RuntimeException("Cannot load \"" + SynapEnvConfFile + "\" file");
		}
	}

	public static Environment getEnv() {
		login();
		return synapEnv.env;
	}

	public static AlertService getAS() {
		login();
		return synapEnv.alertService;
	}

	public static AuditorSvc getAuditorSvc(){
		login();
		return synapEnv.auditorSvc;
	}
	public static NotificationService getNsvc() {
		return synapEnv.nsvcProxy.getService();
	}

	public static Proxy<NotificationService> getNsvcProxy() {
		return synapEnv.nsvcProxy;
	}

	private static void login() {
		synapEnv.connect();
	}

	private boolean connect() {
		try {
			if (nsvcProxy == null) {
				nsvcProxy = TransportManager.getProxy(connectProperties, NotificationService.class);
			}
			InitialContext ctx = new InitialContext(connectProperties);
			env = ServerLoginModule.getService(ctx,	"Environment", Environment.class);
            env.getObjectsByType(ESTypes.NETWORK);
			alertService = ServerLoginModule.getService(ctx, "AlertService", AlertService.class);
		} catch (Exception e) {
			log.error("Unable to connect to ES.", e);
			return false;
		}
		return true;
	}
}
