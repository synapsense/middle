package com.synapsense.bacnet.agent.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import org.apache.log4j.Logger;

import java.util.Map;
import com.synapsense.bacnet.agent.ESContext;
import com.synapsense.dto.CollectionTO;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.dto.ValueTO;

/**
 * Utility class
 */
public class ESUtils {
	private static final Logger logger = Logger.getLogger(ESUtils.class);

	/**
	 * @param to
	 * @param propNames
	 * @return a Map with property names and their values
	 */
	public static Map<String, Object> extractProperties(TO<?> to, String[] propNames) {
		Map<String, Object> result = new HashMap<String, Object>();
		Collection<TO<?>> objIds = new ArrayList<TO<?>>(1);
		objIds.add(to);
		Collection<CollectionTO> allObjectsAndProperties = ESContext.getEnv().getPropertyValue(objIds, propNames);
		for (CollectionTO someObject : allObjectsAndProperties) {
			Collection<ValueTO> values = someObject.getPropValues();
			if (values != null) {
				for (ValueTO value : values) {
					result.put(value.getPropertyName(), value.getValue());
				}
			} else {
				logger.warn("Cannot init '" + saveTO(to) + "' object. There are no properties for it.");
			}
		}
		return result;
	}

	/**
	 * @param to
	 * @return a String representation of <code>to</code> object
	 */
	public static String saveTO(TO<?> to) {
		return TOFactory.getInstance().saveTO(to);
	}

	/**
	 * Converts a string into <code>TO<?></code> type
	 * 
	 * @param to
	 * @return a <code>TO<?></code> type instance
	 */
	public static TO<?> loadTO(String to) {
		return TOFactory.getInstance().loadTO(to);
	}
}
