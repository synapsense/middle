package com.synapsense.bacnet.agent.wrappers;

import com.synapsense.bacnet.objects.BACnetObject;
import com.synapsense.bacnet.system.shared.BACnetCOVSubscription;
import com.synapsense.bacnet.system.shared.BACnetObjectIdentifier;

/**
 * BACnetObject wrapper
 */
public abstract class SynapObject implements BACnetObject {
	protected BACnetObjectIdentifier id;
	// [BC 09/2015] Even before my refactoring, parent was set only for sensors.
	private SynapDevice parent;

	@Override
	public BACnetObjectIdentifier getObjectIdentifier() {
		return id;
	}

	@Override
	public void setObjectIdentifier(BACnetObjectIdentifier id) {
		this.id = id;
	}

	protected SynapDevice getParent() {
		return parent;
	}

	protected void setParent(SynapDevice parent) {
		this.parent = parent;
	}

	public void addActiveCOVSubscription(BACnetCOVSubscription covSubscription) {
		parent.addActiveCOVSubscription(covSubscription);
	}

	public void removeActiveCOVSubscription(BACnetCOVSubscription covSubscription) {
		parent.removeActiveCOVSubscription(covSubscription);
	}
}
