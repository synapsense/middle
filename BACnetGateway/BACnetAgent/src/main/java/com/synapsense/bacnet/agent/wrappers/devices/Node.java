package com.synapsense.bacnet.agent.wrappers.devices;

import com.synapsense.bacnet.agent.BACnetGWConfig;
import com.synapsense.bacnet.agent.ESContext;
import com.synapsense.bacnet.agent.utils.TypeConvertor;
import com.synapsense.bacnet.agent.wrappers.SynapDevice;
import com.synapsense.bacnet.agent.wrappers.objects.Sensor;
import com.synapsense.bacnet.enums.BACnetDeviceStatus;
import com.synapsense.bacnet.objects.BACnetDeviceWrapper;
import com.synapsense.bacnet.objects.BACnetProperty;
import com.synapsense.bacnet.objects.InvalidPropertyException;
import com.synapsense.bacnet.system.BACnetPropertyId;
import com.synapsense.bacnet.system.shared.BACnetObjectIdentifier;
import com.synapsense.dto.Alert;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.dto.ValueTO;
import java.beans.IntrospectionException;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import org.apache.log4j.Logger;

public class Node extends SynapDevice {

	private final Map<String, Object> properties;
	private final PropertyChangeSupport pcs;
	private final BACnetDeviceWrapper wrapper;
	private final TO<?> to;

	private final static int PROP_COUNT = NodeFld.values().length;

	private static HashMap<String, Object[]> covProperties = new HashMap<String, Object[]>();

	private static final Logger log = Logger.getLogger(Node.class);

	static {
		covProperties.put(NodeFld.status.toString(), new Object[]{"status", Long.class});
	}

	public Node(BACnetGWConfig config, TO<?> node, Map<String, Object> nodeProperties, int bacnetId) {
		super(config);
		try {
			to = node;
			wrapper = new BACnetDeviceWrapper(this);
			pcs = new PropertyChangeSupport(this);
			properties = new HashMap<String, Object>(nodeProperties);
			id = new BACnetObjectIdentifier(getObjectType(), bacnetId);
		} catch (IntrospectionException | InvalidPropertyException e) {
			// From this level up these exceptions are bugs (unchecked exception)
			throw new RuntimeException(e);
		}
	}

	public void attachSensor(Sensor sensor) {
		attachObject(sensor);
	}

	public void detachSensor(Sensor sensor) {
		detachObject(sensor);
	}

	@Override
	public int getSystemStatus() {
		return BACnetDeviceStatus.OPERATIONAL;
	}

	@Override
	public String getObjectName() {
		return (String) properties.get(NodeFld.name.toString());
	}

	@BACnetProperty(id = BACnetPropertyId.OBJECT_NAME, codec = "CharacterStringCodec")
	public void setObjectName(String nodeName) throws Exception {
		ESContext.getEnv().setPropertyValue(to, NodeFld.name.toString(), nodeName);
	}

	@BACnetProperty(id = BACnetPropertyId.LOGICAL_ID, codec = "UIntCodec")
	public long getLogicalId() {
		return (Integer) properties.get(NodeFld.id.toString());
	}

	@BACnetProperty(id = BACnetPropertyId.PHYSICAL_ID, codec = "UIntCodec")
	public long getPhysicalId() {
		return (Long) properties.get(NodeFld.mac.toString());
	}

	@BACnetProperty(id = BACnetPropertyId.PLATFORM_NAME, codec = "CharacterStringCodec")
	public String getPlatformName() {
		return (String) properties.get(NodeFld.platformName.toString());
	}

	// @BACnetProperty(id = BACnetPropertyId.NETWORK_ID, codec = "UIntCodec")
	// public long getNetworkId() throws Exception {
	// return (Integer)properties.get(NodeFld.networkId.toString());
	// }

	@BACnetProperty(id = BACnetPropertyId.SENSORS_COUNT, codec = "UIntCodec")
	public long getSensorsCount() throws Exception {
		Collection<TO<?>> sensors = ESContext.getEnv().getChildren(to);
		return sensors.size();
	}

	@BACnetProperty(id = BACnetPropertyId.ALERTS_COUNT, codec = "UIntCodec")
	public long getAlertsCount() {
		Collection<Alert> alerts = ESContext.getAS().getActiveAlerts(to, true);
		return alerts == null ? 0 : alerts.size();
	}

	@BACnetProperty(id = BACnetPropertyId.DEFAULT_SAMPLE_INTERVAL, codec = "CharacterStringCodec")
	public String getSamplingInterval() {
		return (String) properties.get(NodeFld.period.toString());
	}

	@BACnetProperty(id = BACnetPropertyId.DEFAULT_SAMPLE_INTERVAL, codec = "CharacterStringCodec")
	public void setSamplingInterval(String nodeSamplingInterval) throws Exception {
		ESContext.getEnv().setPropertyValue(to, NodeFld.period.toString(), nodeSamplingInterval);
	}

	@BACnetProperty(id = BACnetPropertyId.LOCATION_Y, codec = "RealCodec")
	public float getLocationY() {
		Double value = (Double) properties.get(NodeFld.y.toString());
		return value == null ? Float.MIN_VALUE : value.floatValue();
	}

	@BACnetProperty(id = BACnetPropertyId.LOCATION_Y, codec = "RealCodec")
	public void setLocationY(float nodeLocationY) throws Exception {
		ESContext.getEnv().setPropertyValue(to, NodeFld.y.toString(), Float.valueOf(nodeLocationY).doubleValue());
	}

	@BACnetProperty(id = BACnetPropertyId.LOCATION_X, codec = "RealCodec")
	public float getLocationX() {
		Double value = (Double) properties.get(NodeFld.x.toString());
		return value == null ? Float.MIN_VALUE : value.floatValue();
	}

	@BACnetProperty(id = BACnetPropertyId.LOCATION_X, codec = "RealCodec")
	public void setLocationX(float nodeLocationX) throws Exception {
		ESContext.getEnv().setPropertyValue(to, NodeFld.x.toString(), Float.valueOf(nodeLocationX).doubleValue());
	}

	@Override
	public String getDescription() {
		return (String) properties.get(NodeFld.desc.toString());
	}

	@BACnetProperty(id = BACnetPropertyId.DESCRIPTION, codec = "CharacterStringCodec")
	public void setDescription(String nodeDescription) throws Exception {
		ESContext.getEnv().setPropertyValue(to, NodeFld.desc.toString(), nodeDescription);
	}

	@Override
	public String getLocation() {
		return (String) properties.get(NodeFld.location.toString());
	}

	@BACnetProperty(id = BACnetPropertyId.LOCATION, codec = "CharacterStringCodec")
	public void setLocation(String nodeLocation) throws Exception {
		ESContext.getEnv().setPropertyValue(to, NodeFld.location.toString(), nodeLocation);
	}

	@BACnetProperty(id = BACnetPropertyId.OS_VERSION, codec = "CharacterStringCodec")
	public String getOSVersion() {
		return (String) properties.get(NodeFld.nodeOSVersion.toString());
	}

	@BACnetProperty(id = BACnetPropertyId.APP_VERSION, codec = "CharacterStringCodec")
	public String getAppVersion() {
		return (String) properties.get(NodeFld.nodeAppVersion.toString());
	}

	@BACnetProperty(id = BACnetPropertyId.STATUS, codec = "UIntCodec")
	public long getStatus() {
		Integer value = (Integer) properties.get(NodeFld.status.toString());
		return value == null ? Integer.MIN_VALUE : value.intValue();
	}

	@BACnetProperty(id = BACnetPropertyId.STATUS, codec = "UIntCodec", cov = true)
	public void addStatusListener(PropertyChangeListener listener) {
		pcs.addPropertyChangeListener((String) covProperties.get(NodeFld.status.toString())[0], listener);
	}

	public void removeStatusListener(PropertyChangeListener listener) {
		pcs.removePropertyChangeListener((String) covProperties.get(NodeFld.status.toString())[0], listener);
	}

	public void addPropertyValue(String property, Object value) {
		properties.put(property, value);
	}

	public TO<?> getTO() {
		return to;
	}

	private void processUpdate(String property, Object value) {
		if (covProperties.get(property) != null) {
			String covProperty = (String) covProperties.get(property)[0];
			if (pcs.getPropertyChangeListeners(covProperty).length != 0) {
				Object prev = properties.get(property);
				log.trace("'" + TOFactory.getInstance().saveTO(to) + "' object: old value='" + prev + "'; new value='"
						+ value + "'");
				if (!value.equals(prev)) {
					log.trace("firePropertyChange('" + covProperty + "', '" + prev + "', '" + value + "')");
					Class<?> target = (Class<?>) covProperties.get(property)[1];
					if (TypeConvertor.isConverted(value, target)) {
						pcs.firePropertyChange(covProperty, TypeConvertor.convert(prev, target),
								TypeConvertor.convert(value, target));
					} else {
						pcs.firePropertyChange(covProperty, prev, value);
					}
				}
			}
		}
		properties.put(property, value);
	}

	public void updateProperty(ValueTO valueTo) {
		processUpdate(valueTo.getPropertyName(), valueTo.getValue());
	}

	public BACnetDeviceWrapper getWrapper() {
		return wrapper;
	}

	public static String[] getPropertyNames() {
		String[] propNames = new String[PROP_COUNT];
		int i = 0;
		for (NodeFld s : NodeFld.values()) {
			propNames[i] = s.toString();
			i++;
		}
		return propNames;
	}

	public enum NodeFld {
		id, mac, name, platformId, platformName, desc, location, nodeAppVersion, nodeOSVersion, period, x, y, status, alerts;
	}

	// /////////////////////////////////////////////////////////////////////////////////////////////
	// Only for testing
	// /////////////////////////////////////////////////////////////////////////////////////////////
	// @BACnetProperty(id = BACnetPropertyId.STATUS, codec =
	// "CharacterStringCodec")
	// public void setStatus(String status) throws Exception {
	// Boolean res = (Boolean)properties.get(NodeFld.status.toString());
	// properties.put(NodeFld.status.toString(), Boolean.valueOf(!res));
	// updateProperty(NodeFld.status.toString());
	// }
}
