package com.synapsense.bacnet.agent.interfaces;

import com.synapsense.bacnet.objects.BACnetProperty;
import com.synapsense.bacnet.system.BACnetPropertyId;
import com.synapsense.bacnet.system.shared.BACnetCOVSubscription;
import java.beans.PropertyChangeListener;

public interface StatusObject {

	@BACnetProperty(id = BACnetPropertyId.STATUS, codec = "CharacterStringCodec")
	String getStatus();

	// @BACnetProperty(id = BACnetPropertyId.STATUS, codec =
	// "CharacterStringCodec")
	// public void setStatus(String status);

	void setOldStatus(String status);

	String getOldStatus();

	void addActiveCOVSubscription(BACnetCOVSubscription covSubscription);

	void removeActiveCOVSubscription(BACnetCOVSubscription covSubscription);

	@BACnetProperty(id = BACnetPropertyId.STATUS, codec = "CharacterStringCodec", cov = true)
	void addStatusListener(PropertyChangeListener listener);

	void removeStatusListener(PropertyChangeListener listener);
}
