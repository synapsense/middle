package com.synapsense.bacnet.agent.wrappers.objects;

import java.util.HashMap;
import java.util.Map;

import com.synapsense.bacnet.agent.ESContext;
import com.synapsense.bacnet.agent.wrappers.SynapObject;
import com.synapsense.bacnet.enums.BACnetEventState;
import com.synapsense.bacnet.objects.BACnetProperty;
import com.synapsense.bacnet.system.BACnetPropertyId;
import com.synapsense.bacnet.system.BACnetPropertyType;
import com.synapsense.bacnet.system.shared.BACnetObjectIdentifier;
import com.synapsense.bacnet.system.shared.BACnetObjectType;
import com.synapsense.dto.TO;

public class CustomInput extends SynapObject {

	private final Map<String, Object> properties;
	private final TO<?> to;
	private String propertyName;

	private enum InputFld {
		name, status, type, lastValue;
	}

	public CustomInput(int id, TO<?> to, String propertyName) {
		this.to = to;

		this.properties = new HashMap<String, Object>();
		this.properties.put(InputFld.name.toString(), to.toString() + "/"
				+ propertyName);
		this.id = new BACnetObjectIdentifier(getObjectType(), id);
		this.propertyName = propertyName;
	}

	@Override
	public String getObjectName() {
		return (String) properties.get(InputFld.name.toString());
	}

	@Override
	public short getObjectType() {
		return BACnetObjectType.ANALOG_INPUT;
	}

	@BACnetProperty(id = BACnetPropertyId.EVENT_STATE, type = BACnetPropertyType.REQUIRED, codec = "EnumCodec")
	public int getEventState() {
		return BACnetEventState.NORMAL;
	}

	@BACnetProperty(id = BACnetPropertyId.PRESENT_VALUE, type = BACnetPropertyType.REQUIRED, codec = "RealCodec")
	public float getPresentValue() throws Exception {
		Float lastValue = Float.MIN_VALUE;
		Double value = ESContext.getEnv().getPropertyValue(to, propertyName,
				Double.class);
		if (value != null)
			lastValue = value.floatValue();
		return lastValue;
	}

}
