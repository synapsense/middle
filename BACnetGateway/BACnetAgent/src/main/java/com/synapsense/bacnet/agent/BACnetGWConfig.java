package com.synapsense.bacnet.agent;

import java.util.ArrayList;
import java.util.Collection;

import com.synapsense.bacnet.network.media.Media;

/**
 * BACnetGW configuration class
 */
public class BACnetGWConfig {

	public static class StackConfig {

		private int ownNet;
		private int wsnStartNet;
		private Collection<NetworkInterface> networkInterfaces;
		private Collection<Media> media;

		public StackConfig() {
			networkInterfaces = new ArrayList<NetworkInterface>();
			media = new ArrayList<Media>();
		}

		public Collection<Media> getSupportedMedia() {
			for (NetworkInterface net : networkInterfaces) {
				media.add(net.getMedia());
			}
			return media;
		}

		public int getOwnNet() {
			return ownNet;
		}

		public void setOwnNet(int ownNet) {
			this.ownNet = ownNet;
		}

		public int getWsnStartNet() {
			return wsnStartNet;
		}

		public void setWsnStartNet(int externalNet) {
			this.wsnStartNet = externalNet;
		}

		public Collection<NetworkInterface> getNetworkInterfaces() {
			return networkInterfaces;
		}

		public void setNetworkInterfaces(Collection<NetworkInterface> networkInterfaces) {
			this.networkInterfaces = networkInterfaces;
		}

	}

	private StackConfig stackConfig = new StackConfig();
	private String synapSystemConfigFile;
	private int adpuTimeout = 10000;
	private String applicationSoftVersion;
	private String description;
	private String firmwareRevision;
	private int maxAPDULenAccepted;
	private String commonModelName;
	private long numberOfADPURetries;
	private long protocolRevision;
	private long protocolVersion;
	private long vendorIdentifier;
	private String vendorName;
	private int updateInterval;
	private int rmiPort = 1100;
	private int initialDeviceId = 77000;
	private int reconnectInterval = 60;

	public int getReconnectInterval() {
		return reconnectInterval;
	}

	public void setReconnectInterval(int reconnectInterval) {
		this.reconnectInterval = reconnectInterval;
	}

	public int getInitialDeviceId() {
		return initialDeviceId;
	}

	public void setInitialDeviceId(int initialDeviceId) {
		this.initialDeviceId = initialDeviceId;
	}

	public StackConfig getStackConfig() {
		return stackConfig;
	}

	public void setStackConfig(StackConfig stackConfig) {
		this.stackConfig = stackConfig;
	}

	public String getSynapSystemConfigFile() {
		return synapSystemConfigFile;
	}

	public void setSynapSystemConfigFile(String synapSystemConfigFile) {
		this.synapSystemConfigFile = synapSystemConfigFile;
	}

	public int getAdpuTimeout() {
		return adpuTimeout;
	}

	public void setAdpuTimeout(int adpuTimeout) {
		this.adpuTimeout = adpuTimeout;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getApplicationSoftVersion() {
		return applicationSoftVersion;
	}

	public void setApplicationSoftVersion(String applicationSoftVersion) {
		this.applicationSoftVersion = applicationSoftVersion;
	}

	public String getFirmwareRevision() {
		return firmwareRevision;
	}

	public void setFirmwareRevision(String firmwareRevision) {
		this.firmwareRevision = firmwareRevision;
	}

	public int getMaxAPDULenAccepted() {
		return maxAPDULenAccepted;
	}

	public void setMaxAPDULenAccepted(long maxAPDULenAccepted) {
		this.maxAPDULenAccepted = (int) maxAPDULenAccepted;
	}

	public String getCommonModelName() {
		return commonModelName;
	}

	public void setCommonModelName(String commonModelName) {
		this.commonModelName = commonModelName;
	}

	public long getNumberOfADPURetries() {
		return numberOfADPURetries;
	}

	public void setNumberOfADPURetries(long numberOfADPURetries) {
		this.numberOfADPURetries = numberOfADPURetries;
	}

	public long getProtocolRevision() {
		return protocolRevision;
	}

	public void setProtocolRevision(long protocolRevision) {
		this.protocolRevision = protocolRevision;
	}

	public long getProtocolVersion() {
		return protocolVersion;
	}

	public void setProtocolVersion(long protocolVersion) {
		this.protocolVersion = protocolVersion;
	}

	public long getVendorIdentifier() {
		return vendorIdentifier;
	}

	public void setVendorIdentifier(long vendorIdentifier) {
		this.vendorIdentifier = vendorIdentifier;
	}

	public String getVendorName() {
		return vendorName;
	}

	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}

	/**
	 * @return the cacheUpdateInterval
	 */
	public int getUpdateInterval() {
		return updateInterval;
	}

	/**
	 * @param cacheUpdateInterval
	 *            the cacheUpdateInterval to set
	 */
	public void setUpdateInterval(int updateInterval) {
		this.updateInterval = updateInterval;
	}

	public int getRmiPort() {
		return rmiPort;
	}

	public void setRmiPort(int rmiPort) {
		this.rmiPort = rmiPort;
	}
}
