package com.synapsense.bacnet.agent.wrappers.devices;

import com.synapsense.bacnet.agent.BACnetGWConfig;
import com.synapsense.bacnet.agent.utils.TypeConvertor;
import com.synapsense.bacnet.agent.wrappers.SynapDevice;
import com.synapsense.bacnet.agent.wrappers.devices.Node.NodeFld;
import com.synapsense.bacnet.agent.wrappers.objects.CustomInput;
import com.synapsense.bacnet.enums.BACnetDeviceStatus;
import com.synapsense.bacnet.objects.BACnetDeviceWrapper;
import com.synapsense.bacnet.objects.BACnetProperty;
import com.synapsense.bacnet.objects.InvalidPropertyException;
import com.synapsense.bacnet.system.BACnetPropertyId;
import com.synapsense.bacnet.system.BACnetPropertyType;
import com.synapsense.bacnet.system.shared.BACnetObjectIdentifier;
import com.synapsense.dto.ValueTO;
import java.beans.IntrospectionException;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.HashMap;
import java.util.Map;
import org.apache.log4j.Logger;

public class CustomDevice extends SynapDevice {
	private static final Logger log = Logger.getLogger(CustomDevice.class);
	private final Map<String, Object> properties;
	private final PropertyChangeSupport pcs;
	private final BACnetDeviceWrapper wrapper;

	private static HashMap<String, Object[]> covProperties = new HashMap<String, Object[]>();
	static {
		covProperties.put(NodeFld.status.toString(), new Object[] { "status", Long.class });
	}

	public enum DeviceFld {
	id, name, desc, location, status, lastValue;
	}

	public CustomDevice(BACnetGWConfig config, Map<String, Object> nodeProperties, Integer deviceId)
	        throws IntrospectionException, InvalidPropertyException {
		super(config);
		wrapper = new BACnetDeviceWrapper(this);
		pcs = new PropertyChangeSupport(this);
		properties = new HashMap<String, Object>(nodeProperties);
		id = new BACnetObjectIdentifier(getObjectType(), deviceId);
	}

	@Override
	public int getSystemStatus() {
		return BACnetDeviceStatus.OPERATIONAL;
	}

	@Override
	public String getObjectName() {
		return (String) properties.get(DeviceFld.name.toString());
	}

	@Override
	public String getDescription() {
		return (String) properties.get(DeviceFld.desc.toString());
	}

	@Override
	public String getLocation() {
		return (String) properties.get(DeviceFld.location.toString());
	}

	public void removeStatusListener(PropertyChangeListener listener) {
		pcs.removePropertyChangeListener((String) covProperties.get(NodeFld.status.toString())[0], listener);
	}

	public void addPropertyValue(String property, Object value) {
		properties.put(property, value);
	}

	private void processUpdate(String property, Object value) {
		if (covProperties.get(property) != null) {
			String covProperty = (String) covProperties.get(property)[0];
			if (pcs.getPropertyChangeListeners(covProperty).length != 0) {
				Object prev = properties.get(property);
				// log.trace("'" + TOFactory.getInstance().saveTO(to) +
				// "' object: old value='" + prev + "'; new value='"
				// + value + "'");
				if (!value.equals(prev)) {
					log.trace("firePropertyChange('" + covProperty + "', '" + prev + "', '" + value + "')");
					Class<?> target = (Class<?>) covProperties.get(property)[1];
					if (TypeConvertor.isConverted(value, target)) {
						pcs.firePropertyChange(covProperty, TypeConvertor.convert(prev, target),
						        TypeConvertor.convert(value, target));
					} else {
						pcs.firePropertyChange(covProperty, prev, value);
					}
				}
			}
		}
		properties.put(property, value);
	}

	public void updateProperty(ValueTO valueTo) {
		processUpdate(valueTo.getPropertyName(), valueTo.getValue());
	}

	public BACnetDeviceWrapper getWrapper() {
		return wrapper;
	}

	@BACnetProperty(id = BACnetPropertyId.PRESENT_VALUE, type = BACnetPropertyType.OPTIONAL, codec = "RealCodec")
	public float getPresentValue() {
		Float lastValue = Float.MIN_VALUE;
		Double value = (Double) properties.get(DeviceFld.lastValue.toString());
		if (value != null)
			lastValue = value.floatValue();
		return lastValue;
	}

	public void addInput(CustomInput input) {
		try {
			getWrapper().addBACnetObject(input);
			attachObject(input);
		} catch (Exception e) {
			log.warn("Unable to register property [" + input.getObjectIdentifier().toString() + "] for device ["
			        + getObjectIdentifier().toString() + "]");
		}

	}
}