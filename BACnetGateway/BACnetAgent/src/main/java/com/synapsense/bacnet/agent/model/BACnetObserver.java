package com.synapsense.bacnet.agent.model;

import org.apache.log4j.Logger;

import com.synapsense.bacnet.agent.wrappers.devices.Network;
import com.synapsense.bacnet.agent.wrappers.devices.Node;
import com.synapsense.bacnet.agent.wrappers.objects.Sensor;
import com.synapsense.bacnet.network.Configuration;
import com.synapsense.bacnet.network.routing.Route;
import com.synapsense.bacnet.network.routing.RoutingService;
import com.synapsense.bacnet.objects.BACnetDeviceWrapper;

/**
 * Observer class for BACnet network
 */
public class BACnetObserver implements BACnetGWModelListener {
	private static final Logger logger = Logger.getLogger(BACnetObserver.class);

	/**
	 * Adds a <code>Network</code> object into BACnet network as a new device
	 */
	@Override
	public void addNetwork(Network network) {
		try {
			network.getBacnetNetwork().addDevice(new BACnetDeviceWrapper(network));
			RoutingService.addRoute(new Route(network.getBacnetNetwork().getNetworkNumber(), 0, "",
			        Configuration.OWN_ADDR));
		} catch (Exception e) {
			logger.warn("It mustn't happen", e);
		}
	}

	/**
	 * Adds a <code>Node</code> object into BACnet network as a new device
	 */
	@Override
	public void addNode(Network network, Node node) {
		try {
			network.getBacnetNetwork().addDevice(node.getWrapper());
		} catch (Exception e) {
			logger.warn("It mustn't happen", e);
		}
	}

	/**
	 * Removes a <code>Network</code> object from BACnet network
	 */
	@Override
	public void removeNetwork(Network network) {
		network.getBacnetNetwork().removeAllDevices();
		RoutingService.removeRoute(network.getBacnetNetwork().getNetworkNumber());
	}

	/**
	 * Removes a <code>Node</code> object from BACnet network
	 */
	@Override
	public void removeNode(Network network, Node node) {
		network.getBacnetNetwork().removeDevice(node.getWrapper());
	}

	/**
	 * Adds a <code>Sensor</code> object into BACnet Node device as a new BACnet
	 * object
	 */
	@Override
	public void addSensor(Node node, Sensor sensor) {
		try {
			node.getWrapper().addBACnetObject(sensor);
			node.attachSensor(sensor);
		} catch (Exception e) {
			logger.warn("It mustn't happen", e);
		}
	}

	/**
	 * Removes a <code>Sensor</code> object from BACnet Node device
	 */
	@Override
	public void removeSensor(Node node, Sensor sensor) {
		node.getWrapper().removeBACnetObject(sensor);
		node.detachSensor(sensor);
	}
}
