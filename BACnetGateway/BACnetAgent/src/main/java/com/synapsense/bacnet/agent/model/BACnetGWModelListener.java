package com.synapsense.bacnet.agent.model;

import com.synapsense.bacnet.agent.wrappers.devices.Network;
import com.synapsense.bacnet.agent.wrappers.devices.Node;
import com.synapsense.bacnet.agent.wrappers.objects.Sensor;

public interface BACnetGWModelListener {
	public void addNetwork(Network network);

	public void addNode(Network network, Node node);

	public void addSensor(Node node, Sensor sensor);

	public void removeNetwork(Network network);

	public void removeNode(Network network, Node node);

	public void removeSensor(Node node, Sensor sensor);
}
