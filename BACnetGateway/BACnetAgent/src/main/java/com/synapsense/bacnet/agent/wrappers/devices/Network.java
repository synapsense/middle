package com.synapsense.bacnet.agent.wrappers.devices;

import com.synapsense.bacnet.agent.BACnetGWConfig;
import com.synapsense.bacnet.agent.ESContext;
import com.synapsense.bacnet.agent.utils.ESUtils;
import com.synapsense.bacnet.agent.utils.TypeConvertor;
import com.synapsense.bacnet.agent.wrappers.SynapDevice;
import com.synapsense.bacnet.enums.BACnetDeviceStatus;
import com.synapsense.bacnet.objects.BACnetProperty;
import com.synapsense.bacnet.system.BACnetNetwork;
import com.synapsense.bacnet.system.BACnetPropertyId;
import com.synapsense.bacnet.system.BACnetSystem;
import com.synapsense.bacnet.system.shared.BACnetObjectIdentifier;
import com.synapsense.dto.Alert;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.dto.ValueTO;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import org.apache.log4j.Logger;

public class Network extends SynapDevice {

	private final Map<String, Object> properties;
	private final PropertyChangeSupport pcs;
	private final BACnetNetwork bacnetNetwork;
	private final TO<?> to;

	private final static int PROP_COUNT = NetFld.values().length;

	private static Map<String, Object[]> covProperties = new HashMap<String, Object[]>();

	private static final Logger log = Logger.getLogger(Network.class);

	static {
		covProperties.put(NetFld.name.toString(), new Object[]{"objectName", String.class});
	}

	public Network(BACnetGWConfig config, TO<?> network, int bacnetId) {
		super(config);
		to = network;
		pcs = new PropertyChangeSupport(this);

		properties = ESUtils.extractProperties(to, getPropertyNames());
		id = new BACnetObjectIdentifier(getObjectType(), bacnetId);

		bacnetNetwork = BACnetSystem.getInstance().createNetwork(id.getInstanceNumber());
	}

	public void attachNode(Node node) {
		super.attachObject(node);
	}

	public void detachNode(Node node) {
		super.detachObject(node);
	}

	@Override
	public BACnetObjectIdentifier[] getObjectList() {
		BACnetObjectIdentifier[] objs = {getObjectIdentifier()};
		return objs;
	}

	@Override
	public short[] getProtocolObjectTypesSupported() {
		short[] types = {getObjectType()};
		return types;
	}

	@Override
	public int getSystemStatus() {
		return BACnetDeviceStatus.OPERATIONAL;
	}

	@Override
	public String getObjectName() {
		return (String) properties.get(NetFld.name.toString());
	}

	@BACnetProperty(id = BACnetPropertyId.OBJECT_NAME, codec = "CharacterStringCodec")
	public void setObjectName(String objectName) throws Exception {
		ESContext.getEnv().setPropertyValue(to, NetFld.name.toString(), objectName);
	}

	@BACnetProperty(id = BACnetPropertyId.ALERTS_COUNT, codec = "UIntCodec")
	public long getAlertsCount() {
		Collection<Alert> alerts = ESContext.getAS().getActiveAlerts(to, true);
		return alerts == null ? 0 : alerts.size();
	}

	@Override
	public String getDescription() {
		return (String) properties.get(NetFld.desc.toString());
	}

	@BACnetProperty(id = BACnetPropertyId.DESCRIPTION, codec = "CharacterStringCodec")
	public void setDescription(String networkDescription) throws Exception {
		ESContext.getEnv().setPropertyValue(to, NetFld.desc.toString(), networkDescription);
	}

	@Override
	public String getLocation() {
		return "localhost";
	}

	@BACnetProperty(id = BACnetPropertyId.STATUS, codec = "CharacterStringCodec")
	public String getStatus() {
		return "Network is available";
	}

	@BACnetProperty(id = BACnetPropertyId.STATUS, codec = "CharacterStringCodec", cov = true)
	public void addStatusListener(PropertyChangeListener listener) {
		pcs.addPropertyChangeListener("status", listener);
	}

	public void removeStatusListener(PropertyChangeListener listener) {
		pcs.removePropertyChangeListener("status", listener);
	}

	@BACnetProperty(id = BACnetPropertyId.OBJECT_NAME, codec = "CharacterStringCodec", cov = true)
	public void addObjectNameListener(PropertyChangeListener listener) {
		pcs.addPropertyChangeListener((String) covProperties.get(NetFld.name.toString())[0], listener);
	}

	public void removeObjectNameListener(PropertyChangeListener listener) {
		pcs.removePropertyChangeListener((String) covProperties.get(NetFld.name.toString())[0], listener);
	}

	public TO<?> getTO() {
		return to;
	}

	public void addPropertyValue(String property, Object value) {
		properties.put(property, value);
	}

	private void processUpdate(String property, Object value) {
		if (covProperties.get(property) != null) {
			String covProperty = (String) covProperties.get(property)[0];
			if (pcs.getPropertyChangeListeners(covProperty).length != 0) {
				Object prev = properties.get(property);
				log.trace("'" + TOFactory.getInstance().saveTO(to) + "' object: old value='" + prev + "'; new value='"
						+ value + "'");
				if (!value.equals(prev)) {
					log.trace("firePropertyChange('" + covProperty + "', '" + prev + "', '" + value + "')");
					Class<?> target = (Class<?>) covProperties.get(property)[1];
					if (TypeConvertor.isConverted(value, target)) {
						pcs.firePropertyChange(covProperty, TypeConvertor.convert(prev, target),
								TypeConvertor.convert(value, target));
					} else {
						pcs.firePropertyChange(covProperty, prev, value);
					}
				}
			}
		}
		properties.put(property, value);
	}

	public void updateProperty(ValueTO valueTo) {
		processUpdate(valueTo.getPropertyName(), valueTo.getValue());
	}

	public BACnetNetwork getBacnetNetwork() {
		return bacnetNetwork;
	}

	public static String[] getPropertyNames() {
		String[] propNames = new String[PROP_COUNT];
		int i = 0;
		for (NetFld s : NetFld.values()) {
			propNames[i] = s.toString();
			i++;
		}
		return propNames;
	}

	private enum NetFld {
		name, desc, alerts;
	}
}
