package com.synapsense.bacnet.agent.wrappers.objects;

import com.synapsense.bacnet.agent.ESContext;
import com.synapsense.bacnet.agent.utils.TypeConvertor;
import com.synapsense.bacnet.agent.wrappers.SynapObject;
import com.synapsense.bacnet.agent.wrappers.devices.Node;
import com.synapsense.bacnet.enums.BACnetEngineeringUnits;
import com.synapsense.bacnet.enums.BACnetEventState;
import com.synapsense.bacnet.enums.BACnetReliability;
import com.synapsense.bacnet.objects.BACnetProperty;
import com.synapsense.bacnet.system.BACnetPropertyId;
import com.synapsense.bacnet.system.BACnetPropertyType;
import com.synapsense.bacnet.system.shared.BACnetCOVSubscription;
import com.synapsense.bacnet.system.shared.BACnetObjectIdentifier;
import com.synapsense.bacnet.system.shared.BACnetObjectType;
import com.synapsense.bacnet.system.shared.BACnetTimeValue;
import com.synapsense.dto.Alert;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.dto.ValueTO;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;
import org.apache.log4j.Logger;

public class Sensor extends SynapObject {

	private final Map<String, Object> properties;
	private final PropertyChangeSupport pcs;
	private final TO<?> to;

	private float covIncrement;
	private Integer hashCode;

	private final static int PROP_COUNT = SensorFld.values().length;

	private static Map<String, Object[]> covProperties = new HashMap<String, Object[]>();

	private final static Logger logger = Logger.getLogger(Sensor.class);

	static {
		covProperties.put(SensorFld.lastValue.toString(), new Object[] { "presentValue", Float.class });
		covProperties.put(SensorFld.status.toString(), new Object[] { "status", String.class });

		// /////////////////////////////
		// Only for testing
		// ////////////////////////////
		// covProperties.put(SensorFld.sensorDescription.toString(), new
		// Object[] {"description", String.class});
	}

	public Sensor(TO<?> sensor, Map<String, Object> sensorProperties) {
		to = sensor;
		properties = new HashMap<String, Object>(sensorProperties);
		pcs = new PropertyChangeSupport(this);

		int channel = (Integer) properties.get(SensorFld.channel.toString());
		id = new BACnetObjectIdentifier(getObjectType(), channel);
	}

	public void setParent(Node node) {
		super.setParent(node);
	}

	public Node getParent() {
		return (Node) super.getParent();
	}

	public static String[] getPropertyNames() {
		String[] propNames = new String[PROP_COUNT];
		int i = 0;
		for (SensorFld s : SensorFld.values()) {
			propNames[i] = s.toString();
			i++;
		}
		return propNames;
	}

	@Override
	public void addActiveCOVSubscription(BACnetCOVSubscription covSubscription) {
		super.addActiveCOVSubscription(covSubscription);
		if (covSubscription.getMonitoredPropertyReference().propertyIdentifier == BACnetPropertyId.PRESENT_VALUE) {
			covIncrement = covSubscription.COVIncrement == null ? 0 : covSubscription.COVIncrement;
		}
	}

	@Override
	public int hashCode() {
		if (hashCode == null)
			hashCode = TOFactory.getInstance().saveTO(to).hashCode();
		return hashCode;
	}

	@Override
	public boolean equals(Object obj) {
		if ((obj != null) && (obj instanceof Sensor)) {
			return hashCode() == ((Sensor) obj).hashCode();
		}
		return false;
	}

	@BACnetProperty(id = BACnetPropertyId.DEVICE_TYPE, codec = "CharacterStringCodec")
	public String getDeviceType() {
		return "Device";
	}

	@BACnetProperty(id = BACnetPropertyId.RELIABILITY, codec = "EnumCodec")
	public long getReliability() {
		return BACnetReliability.NO_FAULT_DETECTED;
	}

	@BACnetProperty(id = BACnetPropertyId.UPDATE_INTERVAL, codec = "UIntCodec")
	public long getUpdateInterval() {
		return 10;
	}

	@BACnetProperty(id = BACnetPropertyId.COV_INCREMENT, codec = "RealCodec")
	public float getCOVIncrement() {
		return covIncrement;
	}

	@BACnetProperty(id = BACnetPropertyId.MIN_PRES_VALUE, codec = "RealCodec")
	public float getMinPresentValue() {
		Float aMin = Float.MIN_VALUE;
		Double value = (Double) properties.get(SensorFld.min.toString());
		if (value != null)
			aMin = value.floatValue();
		return aMin;
	}

	@BACnetProperty(id = BACnetPropertyId.MIN_PRES_VALUE, codec = "RealCodec")
	public void setMinPresentValue(float minPresentValue) throws Exception {
		ESContext.getEnv().setPropertyValue(to, SensorFld.min.toString(), new Double(minPresentValue));
	}

	@BACnetProperty(id = BACnetPropertyId.MAX_PRES_VALUE, codec = "RealCodec")
	public float getMaxPresentValue() {
		Float aMax = Float.MIN_VALUE;
		Double value = (Double) properties.get(SensorFld.max.toString());
		if (value != null)
			aMax = value.floatValue();
		return aMax;
	}

	@BACnetProperty(id = BACnetPropertyId.MAX_PRES_VALUE, codec = "RealCodec")
	public void setMaxPresentValue(float maxPresentValue) throws Exception {
		ESContext.getEnv().setPropertyValue(to, SensorFld.max.toString(), new Double(maxPresentValue));
	}

	@BACnetProperty(id = BACnetPropertyId.ALLOWABLE_MIN, codec = "RealCodec")
	public float getAllowableMin() {
		Float aMin = Float.MIN_VALUE;
		Double value = (Double) properties.get(SensorFld.aMin.toString());
		if (value != null)
			aMin = value.floatValue();
		return aMin;
	}

	@BACnetProperty(id = BACnetPropertyId.ALLOWABLE_MIN, codec = "RealCodec")
	public void setAllowableMin(float minValue) throws Exception {
		ESContext.getEnv().setPropertyValue(to, SensorFld.aMin.toString(), new Double(minValue));
	}

	@BACnetProperty(id = BACnetPropertyId.ALLOWABLE_MAX, codec = "RealCodec")
	public float getAllowableMax() {
		Float aMax = Float.MIN_VALUE;
		Double value = (Double) properties.get(SensorFld.aMax.toString());
		if (value != null)
			aMax = value.floatValue();
		return aMax;
	}

	@BACnetProperty(id = BACnetPropertyId.ALLOWABLE_MAX, codec = "RealCodec")
	public void setAllowableMax(float maxValue) throws Exception {
		ESContext.getEnv().setPropertyValue(to, SensorFld.aMax.toString(), new Double(maxValue));
	}

	@BACnetProperty(id = BACnetPropertyId.RESOLUTION, codec = "RealCodec")
	public float getResolution() {
		return 0;
	}

	@BACnetProperty(id = BACnetPropertyId.STATUS_FLAGS, type = BACnetPropertyType.REQUIRED, codec = "BitStringCodec")
	public boolean[] getStatusFlags() {
		return new boolean[] { false, false, false, false };
	}

	@BACnetProperty(id = BACnetPropertyId.EVENT_STATE, type = BACnetPropertyType.REQUIRED, codec = "EnumCodec")
	public int getEventState() {
		return BACnetEventState.NORMAL;
	}

	@BACnetProperty(id = BACnetPropertyId.OUT_OF_SERVICE, type = BACnetPropertyType.REQUIRED, codec = "BooleanCodec")
	public boolean getOutOfService() {
		return false;
	}

	@BACnetProperty(id = BACnetPropertyId.CHANNEL, codec = "UIntCodec")
	public long getNodeChannel() {
		return (Integer) properties.get(SensorFld.channel.toString());
	}

	@BACnetProperty(id = BACnetPropertyId.RECOMMENDED_MIN, codec = "RealCodec")
	public float getRecommendedMin() {
		Float rMin = Float.MIN_VALUE;
		Double value = (Double) properties.get(SensorFld.rMin.toString());
		if (value != null)
			rMin = value.floatValue();
		return rMin;
	}

	@BACnetProperty(id = BACnetPropertyId.RECOMMENDED_MIN, codec = "RealCodec")
	public void setRecommendedMin(float sensorRecommendedMin) throws Exception {
		ESContext.getEnv().setPropertyValue(to, SensorFld.rMin.toString(), new Double(sensorRecommendedMin));
	}

	@BACnetProperty(id = BACnetPropertyId.RECOMMENDED_MAX, codec = "RealCodec")
	public float getRecommendedMax() {
		Float rMax = Float.MIN_VALUE;
		Double value = (Double) properties.get(SensorFld.rMax.toString());
		if (value != null)
			rMax = value.floatValue();
		return rMax;
	}

	@BACnetProperty(id = BACnetPropertyId.RECOMMENDED_MAX, codec = "RealCodec")
	public void setRecommendedMax(float sensorRecommendedMax) throws Exception {
		ESContext.getEnv().setPropertyValue(to, SensorFld.rMax.toString(), new Double(sensorRecommendedMax));
	}

	@Override
	public String getObjectName() {
		return (String) properties.get(SensorFld.name.toString());
	}

	@Override
	public short getObjectType() {
		return BACnetObjectType.ANALOG_INPUT;
	}

	@BACnetProperty(id = BACnetPropertyId.PRESENT_VALUE, type = BACnetPropertyType.REQUIRED, codec = "RealCodec")
	public float getPresentValue() {
		Float lastValue = Float.MIN_VALUE;
		Double value = (Double) properties.get(SensorFld.lastValue.toString());
		if (value != null)
			lastValue = value.floatValue();
		return lastValue;
	}

	@BACnetProperty(id = BACnetPropertyId.DESCRIPTION, codec = "CharacterStringCodec")
	public void setDescription(String sensorDescription) throws Exception {
		ESContext.getEnv().setPropertyValue(to, SensorFld.desc.toString(), sensorDescription);
	}

	@BACnetProperty(id = BACnetPropertyId.DESCRIPTION, codec = "CharacterStringCodec")
	public String getDescription() {
		return (String) properties.get(SensorFld.desc.toString());
	}

	@BACnetProperty(id = BACnetPropertyId.UNITS, codec = "EnumCodec")
	public int getUnits() {
		int classId = (Integer) properties.get(SensorFld.dataclass.toString());
		switch (classId) {
		case 200: // units = "Temperature (F)";
			return BACnetEngineeringUnits.DEGREES_FAHRENHEIT;
		case 201: // units = "Humidity (";
			return BACnetEngineeringUnits.PERCENT_RELATIVE_HUMIDITY;
		case 202: // units = "Pressure (inH2O)";
			return BACnetEngineeringUnits.INCHES_OF_WATER;
		case 203: // units = "Purity";
			break;
		case 204: // units = "Particle Count";
			break;
		case 205: // units = "Voltage (V)";
			return BACnetEngineeringUnits.VOLTS;
		case 206: // units = "Current (A)";
			return BACnetEngineeringUnits.AMPERES;
		case 207: // units = "Energy (kWh)";
			return BACnetEngineeringUnits.KILOWATT_HOURS;
		case 208: // units = "pH";
			break;
		case 209: // units = "Liquid Flow (GPM)";
			break;
		case 210: // units = "Battery Strength (V)";
			return BACnetEngineeringUnits.VOLTS;
		case 211: // units = "Door Status";
			break;
		case 212: // units = "Air Flow (SCCM)";
			break;
		case 213: // units = "Liquid Presence";
			break;
		}
		return BACnetEngineeringUnits.NO_UNITS;
	}

	@BACnetProperty(id = BACnetPropertyId.PROFILE_NAME, codec = "CharacterStringCodec")
	public String getProfileName() {
		return "266-";
	}

	@BACnetProperty(id = BACnetPropertyId.LOCATION, codec = "CharacterStringCodec")
	public String getLocation() {
		return "Property is not defined";
	}

	@BACnetProperty(id = BACnetPropertyId.STATUS, codec = "CharacterStringCodec")
	public String getStatus() {
		return ((Integer) properties.get(SensorFld.status.toString())).toString();
	}

	@BACnetProperty(id = BACnetPropertyId.ALERTS_COUNT, codec = "UIntCodec")
	public long getAlertsCount() {
		Collection<Alert> alerts = ESContext.getAS().getActiveAlerts(to, true);
		return alerts == null ? 0 : alerts.size();
	}

	@BACnetProperty(id = BACnetPropertyId.LAST_VALUE_TIMESTAMP, codec = "TimeCodec")
	public BACnetTimeValue getTimeStamp() throws Exception {
		GregorianCalendar result = new GregorianCalendar();
		long time = ESContext.getEnv().getPropertyTimestamp(to, SensorFld.lastValue.toString());
		Date timestamp = new Date(time);
		result.setTime(timestamp);
		return new BACnetTimeValue(result);
	}

	@BACnetProperty(id = BACnetPropertyId.ELAPSED_TIME, codec = "UIntCodec")
	public long getElapsedTime() throws Exception {
		long time = ESContext.getEnv().getPropertyTimestamp(to, SensorFld.lastValue.toString());
		return (System.currentTimeMillis() - time) / 1000;
	}

	@BACnetProperty(id = BACnetPropertyId.PRESENT_VALUE, codec = "RealCodec", cov = true)
	public void addPresentValueListener(PropertyChangeListener listener) {
		pcs.addPropertyChangeListener((String) covProperties.get(SensorFld.lastValue.toString())[0], listener);
	}

	public void removePresentValueListener(PropertyChangeListener listener) {
		pcs.removePropertyChangeListener((String) covProperties.get(SensorFld.lastValue.toString())[0], listener);
	}

	@BACnetProperty(id = BACnetPropertyId.STATUS, codec = "CharacterStringCodec", cov = true)
	public void addStatusListener(PropertyChangeListener listener) {
		pcs.addPropertyChangeListener((String) covProperties.get(SensorFld.status.toString())[0], listener);
	}

	public void removeStatusListener(PropertyChangeListener listener) {
		pcs.removePropertyChangeListener((String) covProperties.get(SensorFld.status.toString())[0], listener);
	}

	public void addPropertyValue(String property, Object value) {
		properties.put(property, value);
	}

	public TO<?> getTO() {
		return to;
	}

	private void processUpdate(String property, Object value) {
		if (covProperties.get(property) != null) {
			String covProperty = (String) covProperties.get(property)[0];
			if (pcs.getPropertyChangeListeners(covProperty).length != 0) {
				Object prev = properties.get(property);
				logger.trace("'" + TOFactory.getInstance().saveTO(to) + "' object: old value='" + prev
				        + "'; new value='" + value + "'");
				if (!value.equals(prev)) {
					logger.trace("firePropertyChange('" + covProperty + "', '" + prev + "', '" + value + "')");
					Class<?> target = (Class<?>) covProperties.get(property)[1];
					if (TypeConvertor.isConverted(value, target)) {
						pcs.firePropertyChange(covProperty, TypeConvertor.convert(prev, target),
						        TypeConvertor.convert(value, target));
					} else {
						pcs.firePropertyChange(covProperty, prev, value);
					}
				}
			}
		}
		properties.put(property, value);
	}

	public void updateProperty(ValueTO valueTo) {
		processUpdate(valueTo.getPropertyName(), valueTo.getValue());
	}

	public void addProperties(Map<String, Object> sensorProperties) {
		properties.putAll(properties);
	}

	// /////////////////////////////////////////////////////////////////////////////////////////////
	// Only for testing
	// /////////////////////////////////////////////////////////////////////////////////////////////
	// @BACnetProperty(id = BACnetPropertyId.PRESENT_VALUE, type =
	// BACnetPropertyType.REQUIRED, codec = "RealCodec")
	// public void setPresentValue(float pres) throws Exception {
	// Double res = (Double)properties.get(SensorFld.lastValue.toString());
	// properties.put(SensorFld.lastValue.toString(), Double.valueOf(res+10));
	// updateProperty(SensorFld.lastValue.toString());
	// }
	//
	// @BACnetProperty(id = BACnetPropertyId.STATUS, codec =
	// "CharacterStringCodec")
	// public void setStatus(String status) throws Exception {
	// Integer res = (Integer)properties.get(SensorFld.status.toString());
	// properties.put(SensorFld.status.toString(), Integer.valueOf(res+10));
	// updateProperty(SensorFld.status.toString());
	// }

	// @BACnetProperty(id = BACnetPropertyId.DESCRIPTION, codec =
	// "CharacterStringCodec", cov = true)
	// public void addDescriptionListener(PropertyChangeListener listener) {
	// pcs.addPropertyChangeListener((String)covProperties.get(SensorFld.sensorDescription.toString())[0],listener);
	// }
	// public void removeDescriptionListener(PropertyChangeListener listener) {
	// pcs.removePropertyChangeListener((String)covProperties.get(SensorFld.sensorDescription.toString())[0],listener);
	// }

	private enum SensorFld {
		name, desc, channel, status, min, max, dataclass, aMin, aMax, rMin, rMax, type, lastValue, dataTimestamp, alerts;
	}
}
