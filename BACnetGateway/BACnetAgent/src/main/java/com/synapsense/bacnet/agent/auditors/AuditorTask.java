package com.synapsense.bacnet.agent.auditors;

import com.panduit.sz.util.concurrent.PExecutors;
import com.synapsense.bacnet.agent.BACnetGWConfig;
import com.synapsense.bacnet.agent.ESContext;
import com.synapsense.bacnetgw.config.BacnetNetwork;
import com.synapsense.scheduler.SimpleTaskUnit;
import com.synapsense.service.Environment;
import com.synapsense.service.nsvc.NotificationService;
import com.synapsense.transport.Proxy;
import com.synapsense.transport.Proxy.LifecycleListener;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.apache.log4j.Logger;

/**
 * This class is responsible for initializing BACnetGW model and registering BACnet GW into NotificationService
 */
public class AuditorTask implements SimpleTaskUnit, LifecycleListener {

	private BACnetGWConfig config;
	private volatile boolean connected;
	private Environment env;
	private NotificationService nsvc;
	private Proxy<NotificationService> nsProxy;
	private WSNModelAuditor wsnModelAuditor;
	private GenericModelAuditor genericModelAuditor;

	static final Logger logger = Logger.getLogger(AuditorTask.class);

	public AuditorTask(BACnetGWConfig config, BacnetNetwork mapping) throws IOException {
		this.config = config;
		wsnModelAuditor = new WSNModelAuditor(config);
		if (mapping != null) {
			genericModelAuditor = new GenericModelAuditor(config, mapping);
		}
	}

	public WSNModelAuditor getWsnModelAuditor() {
		return wsnModelAuditor;
	}

	/**
	 * Tries to establish a connection to ES and initializes BACnetGW external networks from data of ES
	 */
	@Override
	public void Execute() {
		try {
			while (!Thread.interrupted()) {
				if (!connected) {
					logger.info("Connecting to ES and refreshing BACnet model ...");
					connect();
					wsnModelAuditor.initModel(getEnv());
					if (genericModelAuditor != null) {
						genericModelAuditor.initModel(getEnv());
					}
					wsnModelAuditor.startEventProcessing(nsvc);
					connected = true;
					logger.info("BACnet model refreshed successfully.");
				}
				Thread.sleep(1000);
			}
		} catch (InterruptedException e) {
			// This is normal termination
			logger.info("AuditorTask reconnection thread terminated.");
		} catch (Exception e) {
			logger.fatal("BACNet GW encountered a fatal exception and will exit.", e);
			System.exit(1);
		}
	}

	@Override
	public void failureAction(Throwable t) {
		// The exceptions are caught in Execute(), so this will never get called
		logger.info("Unexpected exception.", t); }

	@Override
	public void disconnected(Proxy<?> arg0) {
		if (connected) {
			logger.info("Notified that connection to ES was lost.", new Exception("Stacktrace showing where the disconnect notification came from."));
			connected = false;
		}

	}

	@Override
	public void connected(Proxy<?> arg0) {
	}

	private Environment getEnv() {
		if (env == null) {
			env = ESContext.getEnv();
		}
		return env;
	}

	/** Establishes a connection with ES and registers BACnetGW into NotificationService */
	private void connect() throws InterruptedException {
		while (!ESContext.testConnect()) {
			logger.info("Next attempt to establish connection will be in " + config.getReconnectInterval() + " sec.");
			Thread.sleep(config.getReconnectInterval() * 1000);
		}
		updateNsvcRegistration();

	}

	/** Updates a connection to NotificationService */
	private void updateNsvcRegistration() {
		if (nsProxy == null) {
			nsProxy = ESContext.getNsvcProxy();
			nsProxy.addListener(this);
			nsvc = nsProxy.getService();
		}
	}

	/** Invoked during shutdown */
	public void stop() {
		try {
			wsnModelAuditor.stopEventProcessing(nsvc);
			// Disconnect from ES, but spend no more than 1 sec. ES might not be running.
			PExecutors.executeVWithTimeout(nsProxy::release, 1, TimeUnit.SECONDS);
			// TODO When restoring support for GenericModelAuditor, stop it here.
		} catch (InterruptedException e) {
			// We need to handle the exception, but the shutdown hook wouldn't be interrupted.
			Thread.currentThread().interrupt();
		}
		logger.info("AuditorTask stopped.");
	}
}
