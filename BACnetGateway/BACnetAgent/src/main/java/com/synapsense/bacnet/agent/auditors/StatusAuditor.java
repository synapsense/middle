package com.synapsense.bacnet.agent.auditors;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.log4j.Logger;

import com.synapsense.bacnet.agent.BACnetGWConfig;
import com.synapsense.bacnet.agent.interfaces.StatusObject;

/**
 * This class is responsible for updating <code>status</code> properties of
 * Database and WebServer objects
 */
public class StatusAuditor extends TimerTask {
	private static StatusAuditor instance = new StatusAuditor();
	private static final Logger log = Logger.getLogger(StatusAuditor.class);
	private ArrayList<StatusObject> objects = new ArrayList<StatusObject>();
	private Timer timer = null;
	private int period;

	private StatusAuditor() {
		period = 1000;
	}

	/**
	 * Sets an updating period
	 * 
	 * @param conf
	 *            BACnet GW configuration
	 */
	public static void init(BACnetGWConfig conf) {
		instance.period = conf.getUpdateInterval() < 1 ? 1000 : conf.getUpdateInterval() * 1000;
	}

	/**
	 * Adds objects to audit
	 * 
	 * @param object
	 *            an object to audit
	 */
	public static void addObject(StatusObject object) {
		if (!instance.objects.contains(object)) {
			synchronized (instance.objects) {
				instance.objects.add(object);
				if (instance.timer == null)
					instance.start();
			}
		}
	}

	/**
	 * Removes objects
	 * 
	 * @param object
	 *            an object to audit
	 */
	public static void removeObject(StatusObject object) {
		if (instance.objects.contains(object)) {
			synchronized (instance.objects) {
				instance.objects.remove(object);
				if (instance.objects.size() == 0)
					instance.stop();
			}
		}
	}

	@Override
	public void run() {
		log.debug("Starting update...");
		synchronized (objects) {
			for (StatusObject object : objects) {
				try {
					String oldStatus = object.getOldStatus();
					String curStatus = object.getStatus();

					if (!curStatus.equals(oldStatus)) {
						object.setOldStatus(curStatus);
					}
				} catch (Exception e) {
					log.warn(e.getMessage(), e);
				}
			}
			log.debug("Next update will be in " + period / 1000 + " seconds. Size=" + objects.size());
		}
	}

	/**
	 * Stops Status auditor
	 */
	private void stop() {
		log.info("Stoping...");
		if (timer != null) {
			timer.cancel();
		}
		timer = null;
	}

	/**
	 * Starts Status auditor
	 */
	private void start() {
		log.info("Starting auditor...");
		timer = new Timer(true);
		timer.schedule(instance, 0, period);
	}
}
