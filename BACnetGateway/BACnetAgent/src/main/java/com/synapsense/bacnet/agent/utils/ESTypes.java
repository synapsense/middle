package com.synapsense.bacnet.agent.utils;

public class ESTypes {
	public static String DATACENTER = "WSNDC";
	public static String NETWORK = "WSNNETWORK";
	public static String NODE = "WSNNODE";
	public static String SENSOR = "WSNSENSOR";
}
