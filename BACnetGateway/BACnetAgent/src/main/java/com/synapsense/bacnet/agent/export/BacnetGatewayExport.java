package com.synapsense.bacnet.agent.export;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;
import com.synapsense.bacnet.agent.BACnetGWMapper;
import com.synapsense.bacnet.agent.services.XMLMappingImpl;
import com.synapsense.dto.ObjectType;
import com.synapsense.dto.PropertyDescr;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.EnvException;
import com.synapsense.service.Environment;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.lang3.text.StrSubstitutor;

public class BacnetGatewayExport {
	private final Environment env;
	private final String template;
	private final Map<String, Integer> bacnetMapping; // TYPE:ES_ID -> BACNET_ID
	private final ListMultimap<String, String> toProperties; // TYPE -> TO properties
	// variables for template interpolation
	private final HashMap<String, Object> templateContext; // TYPE.id -> id, TYPE.name -> name

	private final static String SENSOR = "WSNSENSOR";
	private final static String NODE = "WSNNODE";
	private final static String ALERT = "ALERT";

	private BacnetGatewayExport(Environment env, String template) throws Exception {
		this.env = env;
		// Append \n so we have one object per line
		this.template = template + "\n";

		bacnetMapping = loadBacnetMapping();
		toProperties = loadToProperties(env);
		templateContext = new HashMap<>();
	}

	private static Map<String, Integer> loadBacnetMapping() throws IOException {
		XMLMappingImpl mapping = BACnetGWMapper.loadMapping();
		if (mapping == null) {
			throw new RuntimeException("Mapping file does not exist. Export cannot proceed.");
		}
		return mapping.getMapping();
	}

	private static ListMultimap<String, String> loadToProperties(Environment env) {
		ListMultimap<String, String> properties = ArrayListMultimap.create();
		String[] types = env.getObjectTypes();
		for (String type : types) {
			ObjectType objType = env.getObjectType(type);
			for (PropertyDescr descr : objType.getPropertyDescriptors()) {
				if (descr.getTypeName().equals(TO.class.getName())) {
					properties.put(type, descr.getName());
				}
			}
		}
		return properties;
	}

	private void exportType(Writer writer, String type) throws EnvException, IOException {
		Collection<TO<?>> objects;
		objects = env.getObjectsByType(type);
		for (TO<?> object : objects) {
			exportObject(writer, object);
		}
	}

	private void exportProperty(Writer writer, String type, String property) throws EnvException, IOException {
		Collection<TO<?>> objects = env.getObjectsByType(type);
		for (TO<?> object : objects) {
			exportObjectProperty(writer, object, property);
		}
	}

	private void exportObject(Writer writer, TO<?> object) throws EnvException, IOException {
		Collection<TO<?>> sensors = new ArrayList<>();
		Collection<TO<?>> nodes;
		String type = object.getTypeName();
		if (type.equalsIgnoreCase(SENSOR)) {
			sensors.add(object);
			nodes = env.getParents(object, NODE);
			for (TO<?> node : nodes) {
				render(writer, node, object);
			}
		} else if (type.equalsIgnoreCase(NODE)) {
			sensors = env.getChildren(object, SENSOR);
			for (TO<?> sensor : sensors) {
				render(writer, object, sensor);
			}
		} else {
			// Check children
			nodes = env.getChildren(object, NODE);
			if (nodes.size() == 0) {
				Collection<TO<?>> children = env.getChildren(object);
				for (TO<?> child : children) {
					exportObject(writer, child);
				}
			} else {
				for (TO<?> node : nodes) {
					sensors = env.getChildren(node, SENSOR);
					for (TO<?> sensor : sensors) {
						render(writer, node, sensor);
					}
				}
			}
		}
	}

	private void exportObjectProperty(Writer writer, TO<?> object, String property) throws EnvException, IOException {
		TO<?> sensor = env.getPropertyValue(object, property, TO.class);
		if (sensor != null && sensor.getTypeName().equalsIgnoreCase(SENSOR)) {
			Collection<TO<?>> nodes = env.getParents(sensor, NODE);
			for (TO<?> node : nodes) {
				render(writer, node, sensor);
			}
		}
	}

	private void render(Writer writer, TO<?> node, TO<?> sensor) throws EnvException, IOException {
		putAttributesForTO(sensor);
		// node is a DEVICE
		Integer id = bacnetMapping.get(TOFactory.getInstance().saveTO(node));
		String name = env.getPropertyValue(node, "name", String.class);
		templateContext.put("DEVICE.id", id);
		templateContext.put("DEVICE.name", name);

		// sensor is a OBJECT
		id = env.getPropertyValue(sensor, "channel", Integer.class);
		name = env.getPropertyValue(sensor, "name", String.class);
		templateContext.put("OBJECT.id", id);
		templateContext.put("OBJECT.name", name);

		writer.append(new StrSubstitutor(templateContext, "$", "$").replace(template));
	}

	/** Recursively put in stContext all attributes related to the given TO */
	private void putAttributesForTO(TO<?> to) throws EnvException {
		String name = env.getPropertyValue(to, "name", String.class);
		templateContext.put(to.getTypeName() + ".id", to.getID());
		templateContext.put(to.getTypeName() + ".name", name);
		Collection<TO<?>> parents = env.getParents(to);
		for (TO<?> parent : parents) {
			putAttributesForTO(parent);
		}

		if (to.getTypeName().equalsIgnoreCase(SENSOR)) {
			for (String type : toProperties.keySet()) {
				if (type.equalsIgnoreCase(ALERT))
					continue;

				for (String property : toProperties.get(type)) {
					parents = env.getObjects(type, new ValueTO[]{new ValueTO(property, to)});
					for (TO<?> parent : parents) {
						putAttributesForTO(parent);
					}
				}
			}
		}
	}

	public static void exportType(Environment env, Writer writer, String template, String type) throws Exception {
		new BacnetGatewayExport(env, template).exportType(writer, type);
	}

	public static void exportProperty(Environment env, Writer writer, String template, String type, String property) throws Exception {
		new BacnetGatewayExport(env, template).exportProperty(writer, type, property);
	}

	public static void exportObject(Environment env, Writer writer, String template, TO<?> object) throws Exception {
		new BacnetGatewayExport(env, template).exportObject(writer, object);
	}

	public static void exportObjectProperty(Environment env, Writer writer, String template, TO<?> object, String property) throws Exception {
		new BacnetGatewayExport(env, template).exportObjectProperty(writer, object, property);
	}
}
