package com.synapsense.bacnet.agent.model;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.LinkedHashMultimap;
import com.google.common.collect.SetMultimap;
import com.synapsense.bacnet.agent.BACnetGWConfig;
import com.synapsense.bacnet.agent.BACnetGWMapper;
import com.synapsense.bacnet.agent.utils.ESUtils;
import com.synapsense.bacnet.agent.wrappers.devices.Network;
import com.synapsense.bacnet.agent.wrappers.devices.Node;
import com.synapsense.bacnet.agent.wrappers.objects.Sensor;
import com.synapsense.dto.ChangedValueTO;
import com.synapsense.dto.TO;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Maintains a NETWORK -> NODE -> SENSOR model for BACnet.
 *
 * <p>NOTE: This class is not safe for concurrent use. WSNModelAuditor is responsible  with serializing access to the model.</p>
 */
public class BACnetGWModel {

	private final BACnetGWConfig config;
	private final BACnetGWMapper mapper;

	private final Map<String, Network> networks;
	private final Map<String, Node> nodes;
	private final Map<String, Sensor> sensors;

	// NETWORK <-> NODE with O(1) for all operations
	private final SetMultimap<String, String> networkToNodes;
	private final Map<String, String> nodeToNetwork;

	// NODE <-> SENSOR with O(1) for all operations
	private final SetMultimap<String, String> nodeToSensors;
	private final Map<String, String> sensorToNode;
	

	private final List<BACnetGWModelListener> listeners;

	private static final Logger logger = Logger.getLogger(BACnetGWModel.class);

	public BACnetGWModel(BACnetGWConfig config, BACnetGWMapper mapper) {
		this.config = config;
		this.mapper = mapper;
		// We use linked hash maps for predictable iteration order. It's not a requirement, but it's nice and makes unit testing easier.
		networks = new LinkedHashMap<>();
		nodes = new LinkedHashMap<>();
		sensors = new LinkedHashMap<>();
		networkToNodes = LinkedHashMultimap.create();
		nodeToNetwork = new LinkedHashMap<>();
		nodeToSensors = LinkedHashMultimap.create();
		sensorToNode = new LinkedHashMap<>();
		listeners = new ArrayList<>();
	}

	public void addListener(BACnetGWModelListener listener) {
		if (!listeners.contains(listener))
			listeners.add(listener);
	}

	/** builds a String key for a TO using the badly named ESUtil.saveTO(). Null TOs are not allowed. */
	private String key(TO<?> to) {
		return ESUtils.saveTO(checkNotNull(to));
	}

	public void addNetwork(TO<?> networkTO) {
		String networkKey = key(networkTO);

		// The network may already exist when refreshing the model.
		if (networks.containsKey(networkKey)) {
			return;
		}

		assert !networkToNodes.containsKey(networkKey) : networkKey;

		Network network = new Network(config, networkTO, mapper.getMapping().getNetworkId(networkKey));
		networks.put(networkKey, network);

		if (logger.isDebugEnabled()) {
			logger.debug("\tNetwork " + networkKey + " created.");
		}

		for (BACnetGWModelListener listener: listeners) {
			listener.addNetwork(network);
		}
	}

	public void updateNetworkProperty(TO<?> networkTO, ChangedValueTO changedValueTO) {
		String networkKey = key(networkTO);
		checkNotNull(changedValueTO);

		if (logger.isTraceEnabled()) {
			logger.trace("Updating " + networkKey + " with " + changedValueTO + ".");
		}

		Network network = networks.get(networkKey);
		if (network == null) {
			logger.warn("Failed updating " + networkKey + " with " + changedValueTO + ". The network could not be found.");
			return;
		}
		network.updateProperty(changedValueTO);
	}

	public void removeNetwork(TO<?> networkTO) {
		removeNetwork(key(networkTO));
	}

	private void removeNetwork(String networkKey) {
		// Cascade remove the children nodes (the collection is cloned to avoid ConcurrentModificationException)
		for (String nodeKey: ImmutableList.copyOf(networkToNodes.get(networkKey))) {
			removeNode(nodeKey);
		}

		assert !networkToNodes.containsKey(networkKey) : networkKey;

		// Remove the network
		Network network = networks.remove(networkKey);
		if (network == null) {
			logger.warn("Network " + networkKey + " not found when removing.");
			return;
		}

		if (logger.isDebugEnabled()) {
			logger.debug("\tNetwork " + networkKey + " removed.");
		}

		// Invoke listeners
		for (BACnetGWModelListener listener: listeners) {
			listener.removeNetwork(network);
		}
	}

	public void addNode(TO<?> nodeTO, Map<String, Object> nodeProperties) {
		String nodeKey = key(nodeTO);
		checkNotNull(nodeProperties);

		// The node may already exist when refreshing the model.
		if (nodes.containsKey(nodeKey)) {
			return;
		}

		assert !nodeToNetwork.containsKey(nodeKey) : nodeKey;
		assert !nodeToSensors.containsKey(nodeKey) : nodeKey;

		Node node = new Node(config, nodeTO, nodeProperties, mapper.getMapping().getId(nodeKey));
		nodes.put(nodeKey, node);

		if (logger.isDebugEnabled()) {
			logger.debug("\t\tNode " + nodeKey + " created.");
		}
	}

	public void addNodeToNetwork(TO<?> networkTO, TO<?> nodeTO) {
		String networkKey = key(networkTO);
		String nodeKey = key(nodeTO);

		// The link may already exist when refreshing the model
		if (networkToNodes.containsEntry(networkKey, nodeKey)) {
			return;
		}

		// The network and node must have been added in advance. This is not enforced by the API.
		Network network = networks.get(networkKey);
		if (network == null) {
			logger.warn("Network " + networkKey + " not found when adding node " + nodeKey + ".");
			return;
		}
		Node node = nodes.get(nodeKey);
		if (node == null) {
			logger.warn("Node " + nodeKey + " not found when adding to network " + networkKey + ".");
			return;
		}

		// Remove from old network if necessary
		String oldNetworkKey = nodeToNetwork.get(nodeKey);
		if (oldNetworkKey != null) {
			assert !oldNetworkKey.equals(networkKey) : networkKey + " " + nodeKey;
			removeNodeFromNetwork(oldNetworkKey, nodeKey);
		}

		assert !nodeToNetwork.containsKey(nodeKey) : nodeKey;

		// Add link in both directions
		networkToNodes.put(networkKey, nodeKey);
		nodeToNetwork.put(nodeKey, networkKey);

		if (logger.isDebugEnabled()) {
			logger.debug("\t\t\tNode " + nodeKey + " added to network " + networkKey + ".");
		}

		// Invoke the listeners
		for (BACnetGWModelListener listener : listeners) {
			listener.addNode(network, node);
		}
	}

	public void updateNodeProperty(TO<?> nodeTO, ChangedValueTO changedValueTO) {
		String nodeKey = key(nodeTO);
		checkNotNull(changedValueTO);

		if (logger.isTraceEnabled()) {
			logger.trace("Updating " + nodeKey + " with " + changedValueTO + ".");
		}

		Node node = nodes.get(nodeKey);
		if (node == null) {
			logger.warn("Failed updating " + nodeKey + " with " + changedValueTO + ". The node could not be found.");
			return;
		}
		node.updateProperty(changedValueTO);
	}

	public void removeNodeFromNetwork(TO<?> networkTO, TO<?> nodeTO) {
		removeNodeFromNetwork(key(networkTO), key(nodeTO));
	}

	private void removeNodeFromNetwork(String networkKey, String nodeKey) {
		// The link may already have been removed through a cascaded operation
		if (!networkToNodes.containsEntry(networkKey, nodeKey)) {
			return;
		}

		assert nodeToNetwork.containsKey(nodeKey) : networkKey + " " + nodeKey;
		assert nodeToNetwork.get(nodeKey).equals(networkKey) : networkKey + " " + nodeKey;

		// Remove link in both directions
		networkToNodes.remove(networkKey, nodeKey);
		nodeToNetwork.remove(nodeKey);

		if (logger.isDebugEnabled()) {
			logger.debug("\t\t\tNode " + nodeKey + " removed from network " + networkKey + ".");
		}

		// Lookup the network and the node. They must exist.
		Network network = networks.get(networkKey);
		Node node = nodes.get(nodeKey);

		assert network != null : networkKey + " " + nodeKey;
		assert node != null : networkKey + " " + nodeKey;

		// Invoke listeners
		for (BACnetGWModelListener listener : listeners) {
			listener.removeNode(network, node);
		}
	}

	public void removeNode(TO<?> nodeTO) {
		removeNode(key(nodeTO));
	}
	
	private void removeNode(String nodeKey) {
		// Cascade remove the children sensors (the collection is cloned to avoid ConcurrentModificationException)
		for (String sensorKey : ImmutableList.copyOf(nodeToSensors.get(nodeKey))) {
			removeSensor(sensorKey);
		}

		assert !nodeToSensors.containsKey(nodeKey) : nodeKey;

		// Remove link to network. It typically exists, but the API does not enforce it.
		String networkKey = nodeToNetwork.get(nodeKey);
		if (networkKey != null) {
			removeNodeFromNetwork(networkKey, nodeKey);
		}

		assert !nodeToNetwork.containsKey(nodeKey);

		// Remove the node. It may be absent after a cascaded remove from the network.
		Node node = nodes.remove(nodeKey);

		if (node != null && logger.isDebugEnabled()) {
			logger.debug("\t\tNode " + nodeKey + " removed.");
		}
	}

	public void addSensor(TO<?> sensorTO, Map<String, Object> sensorProperties) {
		String sensorKey = key(sensorTO);
		checkNotNull(sensorProperties);

		// The node may already exist when refreshing the model.
		if (sensors.containsKey(sensorKey)) {
			return;
		}

		assert !sensorToNode.containsKey(sensorKey) : sensorKey;

		Sensor sensor = new Sensor(sensorTO, sensorProperties);
		sensors.put(sensorKey, sensor);

		if (logger.isDebugEnabled()) {
			logger.debug("\t\t\t\tSensor " + sensorKey + " created.");
		}
	}

	public void addSensorToNode(TO<?> nodeTO, TO<?> sensorTO) {
		String nodeKey = key(nodeTO);
		String sensorKey = key(sensorTO);

		// The link may already exist when refreshing the model
		if (nodeToSensors.containsEntry(nodeKey, sensorKey)) {
			return;
		}

		// The SS data model does not allow moving sensors. The sensor can't be linked to another node.
		if (sensorToNode.containsKey(sensorKey)) {
			logger.warn("Failed adding sensor " + sensorKey + " to node " + nodeKey + ". It's already linked to node " + sensorToNode.get(sensorKey) + ".");
			return;
		}

		// The node and sensor must have been added in advance. This is not enforced by the API.
		Node node = nodes.get(nodeKey);
		if (node == null) {
			logger.warn("Node " + nodeKey + " not found when adding sensor " + sensorKey + ".");
			return;
		}
		Sensor sensor = sensors.get(sensorKey);
		if (sensor == null) {
			logger.warn("Sensor " + sensorKey + " not found when adding to node " + nodeKey + ".");
			return;
		}

		// Preserved from the original implementation
		sensor.setParent(node);

		// Add link in both directions
		nodeToSensors.put(nodeKey, sensorKey);
		sensorToNode.put(sensorKey, nodeKey);

		if (logger.isDebugEnabled()) {
			logger.debug("\t\t\t\t\tSensor " + sensorKey + " added to node " + nodeKey + ".");
		}

		// Invoke the listeners
		for (BACnetGWModelListener listener : listeners) {
			listener.addSensor(node, sensor);
		}
	}

	public void updateSensorProperty(TO<?> sensorTO, ChangedValueTO changedValueTO) {
		String sensorKey = key(sensorTO);
		checkNotNull(changedValueTO);

		if (logger.isTraceEnabled()) {
			logger.trace("Updating " + sensorKey + " with " + changedValueTO + ".");
		}

		Sensor sensor = sensors.get(sensorKey);
		if (sensor == null) {
			logger.warn("Failed updating " + sensorKey + " with " + changedValueTO + ". The sensor could not be found.");
			return;
		}
		sensor.updateProperty(changedValueTO);
	}

	public void removeSensorFromNode(TO<?> nodeTO, TO<?> sensorTO) {
		removeSensorFromNode(key(nodeTO), key(sensorTO));
	}

	private void removeSensorFromNode(String nodeKey, String sensorKey) {
		// The link may already have been removed through a cascaded operation
		if (!nodeToSensors.containsEntry(nodeKey, sensorKey)) {
			return;
		}

		assert sensorToNode.containsKey(sensorKey) : nodeKey + " " + sensorKey;
		assert sensorToNode.get(sensorKey).equals(nodeKey) : nodeKey + " " + sensorKey;

		// Remove link in both directions
		nodeToSensors.remove(nodeKey, sensorKey);
		sensorToNode.remove(sensorKey);

		if (logger.isDebugEnabled()) {
			logger.debug("\t\t\t\t\tSensor " + sensorKey + " removed from node " + nodeKey + ".");
		}

		// Lookup the node and the sensor. They must exist.
		Node node = nodes.get(nodeKey);
		Sensor sensor = sensors.get(sensorKey);

		assert node != null : nodeKey + " " + sensorKey;
		assert sensor != null : nodeKey + " " + sensorKey;

		// Invoke listeners
		for (BACnetGWModelListener listener : listeners) {
			listener.removeSensor(node, sensor);
		}
	}

	public void removeSensor(TO<?> sensorTO) {
		removeSensor(key(sensorTO));
	}

	private void removeSensor(String sensorKey) {
		// Remove link to node. It typically exists, but the API does not enforce it.
		String nodeKey = sensorToNode.get(sensorKey);
		if (nodeKey != null) {
			removeSensorFromNode(nodeKey, sensorKey);
		}

		assert !sensorToNode.containsKey(sensorKey) : sensorKey;

		// Remove the node. It may be absent after a cascaded remove from the network.
		Sensor sensor = sensors.remove(sensorKey);
		if (sensor != null && logger.isDebugEnabled()) {
			logger.debug("\t\t\t\tSensor " + sensorKey + " removed.");
		}
	}
	
	public void clear() {
		for (String networkKey : ImmutableList.copyOf(networks.keySet())) {
			removeNetwork(networkKey);
		}
		// Remove nodes not linked to a network (the API allows that)
		for (String nodeKey : ImmutableList.copyOf(nodes.keySet())) {
			removeNode(nodeKey);
		}
		// Remove sensors not linked to a node (the API allows that)
		for (String sensorKey : ImmutableList.copyOf(sensors.keySet())) {
			removeSensor(sensorKey);
		}
		logger.info("BACnet model cleared.");
	}
}
