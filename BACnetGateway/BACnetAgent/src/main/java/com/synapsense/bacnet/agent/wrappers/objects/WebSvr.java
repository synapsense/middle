package com.synapsense.bacnet.agent.wrappers.objects;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Map;

import com.synapsense.bacnet.agent.ESContext;
import com.synapsense.bacnet.agent.auditors.StatusAuditor;
import com.synapsense.bacnet.agent.interfaces.StatusObject;
import com.synapsense.bacnet.agent.wrappers.SynapObject;
import com.synapsense.bacnet.objects.BACnetProperty;
import com.synapsense.bacnet.system.BACnetPropertyId;
import com.synapsense.bacnet.system.shared.BACnetObjectIdentifier;
import com.synapsense.bacnet.system.shared.BACnetObjectType;
import com.synapsense.service.impl.audit.Auditor;
import com.synapsense.service.impl.audit.AuditorSvc;

public class WebSvr extends SynapObject implements StatusObject {
	private final static String objectName = "SynapSense web server host";
	private PropertyChangeSupport pcs;
	private String status;
	private String host = "Property is not defined";
	private int port = 0;
	private boolean isInit = false;

	public WebSvr() {
		pcs = new PropertyChangeSupport(this);
		setObjectIdentifier(new BACnetObjectIdentifier(getObjectType(), 3));
	}

	@Override
	public String getObjectName() {
		return objectName;
	}

	@Override
	public short getObjectType() {
		return BACnetObjectType.SYNAPSENSE_WEB_SERVER_OBJECT;
	}

	/**
	 * @return the location
	 */
	@BACnetProperty(id = BACnetPropertyId.LOCATION, codec = "CharacterStringCodec")
	public String getLocation() {
		return "Property is not defined";
	}

	/**
	 * @return the description
	 */
	@BACnetProperty(id = BACnetPropertyId.DESCRIPTION, codec = "CharacterStringCodec")
	public String getDescription() {
		return "Property is not defined";
	}

	/**
	 * @return the hostName
	 */
	@BACnetProperty(id = BACnetPropertyId.HOST, codec = "CharacterStringCodec")
	public String getHostName() {
		if (!isInit) {
			init();
			isInit = true;
		}
		return host;
	}

	/**
	 * @return the hostPort
	 */
	@BACnetProperty(id = BACnetPropertyId.PORT, codec = "UIntCodec")
	public long getHostPort() {
		if (!isInit) {
			init();
			isInit = true;
		}
		return port;
	}

	private void init() {
		String config = getAuditor().getConfiguration();
		if (config != null && !config.isEmpty()) {
			config = config.substring(config.indexOf("//") + 2);
			config = config.substring(0, config.indexOf("/"));
			String[] split = config.split(":");
			host = split[0];
			if (split.length > 1)
				port = Integer.parseInt(split[1]);
		}
	}

	private Auditor getAuditor() {
		Auditor res = null;
		AuditorSvc ause = ESContext.getAuditorSvc();
		for (Map.Entry<String, Auditor> entry : ause.getAuditors().entrySet()) {
			String auditorName = entry.getKey();
			if (auditorName.endsWith("WebServerAuditor")) {
				res = entry.getValue();
				break;
			}
		}
		return res;
	}

	@Override
	public String getStatus() {
		String result = getAuditor().getLastStatus();
		return result != null ? result : "Cannot get WEB server status";
	}

	@Override
	public String getOldStatus() {
		return status;
	}

	@Override
	public void setOldStatus(String status) {
		pcs.firePropertyChange("status", this.status, status);
		this.status = status;
	}

	@Override
	public void addStatusListener(PropertyChangeListener listener) {
		status = getStatus();
		pcs.addPropertyChangeListener("status", listener);
		StatusAuditor.addObject(this);
	}

	@Override
	public void removeStatusListener(PropertyChangeListener listener) {
		pcs.removePropertyChangeListener("status", listener);
		StatusAuditor.removeObject(this);
	}

	// @Override
	// public void setStatus(String status) {
	// pcs.firePropertyChange("status", this.status, status);
	// this.status = status;
	// }
}
