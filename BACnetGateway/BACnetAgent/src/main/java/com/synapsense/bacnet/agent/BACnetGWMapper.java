package com.synapsense.bacnet.agent;

import com.panduit.sz.util.io.PFiles;
import com.synapsense.bacnet.agent.services.XMLMappingImpl;
import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.log4j.Logger;

/** Class that associates SS ids with BACnet ids. The mapping is persisted to conf/instances.map.xml */
public class BACnetGWMapper {

	private XMLMappingImpl mapping;

	private static final Path MAPPING_FILE = Paths.get("conf", "instances.map.xml");
	private static final Path MAPPING_BACKUP_DIR = Paths.get("conf", "backup");

	private static final Logger logger = Logger.getLogger(BACnetGWMapper.class);

	public BACnetGWMapper(int initialDeviceId, int initialNetworkId) throws IOException {
		mapping = loadMapping();
		if (mapping != null) {
			logger.info("Loaded mapping from " + MAPPING_FILE.toAbsolutePath());
		} else {
			mapping = new XMLMappingImpl(initialDeviceId, initialNetworkId);
			logger.info("Created empty mapping.");
		}
	}

	/**
	 * loads the mapping from instances.map.xml. Returns null if the file does not exist. This static method is intended for read-only
	 * access to the mapping.
	 */
	public static XMLMappingImpl loadMapping() throws IOException {
		return Files.exists(MAPPING_FILE) ? (XMLMappingImpl) decodeXml(MAPPING_FILE) : null;
	}

	@SuppressWarnings("unchecked")
	private static Object decodeXml(Path xmlFile) throws IOException {
		try (XMLDecoder decoder = new XMLDecoder(Files.newInputStream(xmlFile))) {
			return decoder.readObject();
		}
	}

	/** Saves mapping information into file */
	public void saveMapping() throws IOException {
		if (!mapping.isDirty()) return;

		// Reset changed flag
		mapping.setChanged(false);

		// Backup existing mapping if it exists
		if (Files.exists(MAPPING_FILE)) {
			Path mappingBackupFile = newBackupFile();
			logger.info("Backing up previous mapping to " + mappingBackupFile.toAbsolutePath());
			PFiles.moveFileToFile(MAPPING_FILE, mappingBackupFile);

		}

		// Save new mapping
		logger.info("Saving mapping to " + MAPPING_FILE.toAbsolutePath());
		encodeXml(MAPPING_FILE, mapping);
	}

	private static Path newBackupFile() {
		String mappingFileName = MAPPING_FILE.getFileName().toString();
		String timestampedSuffix = new SimpleDateFormat(".yyyy-MM-dd-HH-mm-ss.'xml'").format(new Date());
		return MAPPING_BACKUP_DIR.resolve(mappingFileName.replace(".xml", timestampedSuffix));
	}

	private static void encodeXml(Path xmlFile, Object object) throws IOException {
		try (XMLEncoder encoder = new XMLEncoder(Files.newOutputStream(xmlFile))) {
			encoder.writeObject(object);
		}
	}

	public XMLMappingImpl getMapping() {
		return mapping;
	}
}
