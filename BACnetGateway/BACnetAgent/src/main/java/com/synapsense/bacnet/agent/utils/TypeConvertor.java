package com.synapsense.bacnet.agent.utils;

import java.lang.reflect.Method;
import java.util.HashMap;

/**
 * Utility class for converting simple types
 */
public class TypeConvertor {

	private static HashMap<Class<?>, Object> supportedTypes = new HashMap<Class<?>, Object>();

	static {
		supportedTypes.put(String.class, String.valueOf(1));
		supportedTypes.put(Long.class, Long.valueOf(1));
		supportedTypes.put(Integer.class, Integer.valueOf(1));
		supportedTypes.put(Float.class, Float.valueOf(1));
		supportedTypes.put(Double.class, Double.valueOf(1));
		supportedTypes.put(Boolean.class, Boolean.valueOf(true));
	}

	private TypeConvertor() {
	}

	/**
	 * Returns TRUE if Value can be converted into Target type
	 * 
	 * @param value
	 *            Value to convert
	 * @param target
	 *            Class of target type
	 * @return TRUE if Value can be converted into Target type, otherwise FALSE
	 */
	public static boolean isConverted(Object value, Class<?> target) {
		return supportedTypes.containsKey(target) && supportedTypes.containsKey(value.getClass());
	}

	/**
	 * Converts a value into the target type
	 * 
	 * @param <T>
	 *            Target type
	 * @param value
	 *            Value to convert
	 * @param target
	 *            Class of target type
	 * @return the converted value
	 */
	@SuppressWarnings("unchecked")
	public static <T> T convert(Object value, Class<T> target) {
		if (value == null)
			return (T) value;
		T res = null;
		try {
			if (target == String.class) {
				res = (T) value.toString();
			} else {
				Method valueOf = target.getMethod("valueOf", String.class);
				res = (T) valueOf.invoke(supportedTypes.get(target), value.toString());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return res;
	}
}
