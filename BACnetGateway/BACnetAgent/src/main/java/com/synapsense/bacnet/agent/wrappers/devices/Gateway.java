package com.synapsense.bacnet.agent.wrappers.devices;

import com.synapsense.bacnet.agent.BACnetGWConfig;
import com.synapsense.bacnet.agent.wrappers.SynapDevice;
import com.synapsense.bacnet.agent.wrappers.objects.DatabaseSvr;
import com.synapsense.bacnet.agent.wrappers.objects.WebSvr;
import com.synapsense.bacnet.enums.BACnetDeviceStatus;
import com.synapsense.bacnet.system.shared.BACnetObjectIdentifier;
import org.apache.log4j.Logger;

public class Gateway extends SynapDevice {

	Logger logger = Logger.getLogger(Gateway.class);
	private String description = null;
	private final static String objectName = "SynapSense BACnet gateway";

	public Gateway(BACnetGWConfig config, int bacnetId) {
		super(config);
		description = config.getDescription();

		setObjectIdentifier(new BACnetObjectIdentifier(getObjectType(), bacnetId));

		DatabaseSvr dbWrapper = new DatabaseSvr();
		WebSvr webWrapper = new WebSvr();

		// Attach objects to the gateway
		attachObject(dbWrapper);
		attachObject(webWrapper);
	}

	@Override
	public String getObjectName() {
		return objectName;
	}

	@Override
	public int getSystemStatus() {
		return BACnetDeviceStatus.OPERATIONAL;
	}

	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public String getLocation() {
		return "localhost";
	}
}
