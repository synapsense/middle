package com.synapsense.bacnet.agent.auditors;

import com.synapsense.bacnet.agent.model.BACnetGWModel;
import com.synapsense.dto.ChangedValueTO;
import com.synapsense.service.nsvc.events.ObjectCreatedEvent;
import com.synapsense.service.nsvc.events.ObjectCreatedEventProcessor;
import com.synapsense.service.nsvc.events.ObjectDeletedEvent;
import com.synapsense.service.nsvc.events.ObjectDeletedEventProcessor;
import com.synapsense.service.nsvc.events.PropertiesChangedEvent;
import com.synapsense.service.nsvc.events.PropertiesChangedEventProcessor;

class NetworkEventProcessor implements ObjectCreatedEventProcessor, ObjectDeletedEventProcessor, PropertiesChangedEventProcessor {

	private final BACnetGWModel model;

	NetworkEventProcessor(BACnetGWModel model) {
		this.model = model;
	}

	@Override
	public ObjectCreatedEvent process(ObjectCreatedEvent oce) {
		this.model.addNetwork(oce.getObjectTO());
		return null;
	}

	@Override
	public ObjectDeletedEvent process(ObjectDeletedEvent ode) {
		this.model.removeNetwork(ode.getObjectTO());
		return null;
	}

	@Override
	public PropertiesChangedEvent process(PropertiesChangedEvent pce) {
		for (ChangedValueTO value : pce.getChangedValues()) {
			this.model.updateNetworkProperty(pce.getObjectTO(), value);
		}
		return null;
	}
}