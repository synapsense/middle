package com.synapsense.bacnet.agent.services;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * maps from SS objects to BACnet ids for WSNNODE and WSNNETWORK. The mapping is persistent (saved to file).
 *
 * <p>This class is not thread-safe. Since 6.8 it's used only from the "ES Event Processor" thread, so it doesn't need to be
 * thread-safe.</p>
 *
 * <p>NOTE: This class cannot be renamed without breaking compatibility with exiting mapping files. Also, fields cannot be renamed or
 * removed</p>
 */
public class XMLMappingImpl {

	private Map<String, Integer> mapping;
	private Map<String, Integer> networkMapping;
	private int currentId;
	private int currentNetworkId;
	private boolean changed;
	private long changedTimestamp;

	private static final long serialVersionUID = -6744771536482789530L;

	public XMLMappingImpl(int initialId, int initialNetworkId) {
		currentId = initialId - 1;
		currentNetworkId = initialNetworkId - 1;
		mapping = new LinkedHashMap<>();
		networkMapping = new LinkedHashMap<>();
	}

	/** required by XML serialization (poor choice). Do not use in code. */
	@SuppressWarnings("unused")
	@Deprecated
	public XMLMappingImpl() {
		mapping = new LinkedHashMap<>();
		networkMapping = new LinkedHashMap<>();
	}

	/** returns true if changed more than 5 sec ago */
	public boolean isDirty() {
		return changed && System.currentTimeMillis() - changedTimestamp > 5000;
	}

	public void setChanged(boolean changed) {
		this.changed = changed;
		if (changed) {
			changedTimestamp = System.currentTimeMillis();
		}
	}

	public int getId(String to) {
		Integer id = mapping.get(to);
		if (id == null) {
			id = ++currentId;
			mapping.put(to, id);
			setChanged(true);
		}
		return id;
	}

	public int getNetworkId(String to) {
		Integer id = networkMapping.get(to);
		if (id == null) {
			id = ++currentNetworkId;
			networkMapping.put(to, id);
			setChanged(true);
		}
		return id;
	}

	/** required by XML serialization (poorChoice). It can't be wrapped in unmodifiableMap() */
	public Map<String, Integer> getMapping() throws RemoteException {
		return mapping;
	}

	/** required by XML serialization (poor choice). Do not use in code. */
	@SuppressWarnings("unused")
	@Deprecated
	public void setMapping(Map<String, Integer> mapping) {
		this.mapping = mapping;
	}

	/** required by XML serialization (poor choice). Do not use in code. */
	@SuppressWarnings("unused")
	@Deprecated
	public Map<String, Integer> getNetworkMapping() {
		return networkMapping;
	}

	/** required by XML serialization (poor choice). Do not use in code. */
	@SuppressWarnings("unused")
	@Deprecated
	public void setNetworkMapping(Map<String, Integer> networkMapping) {
		this.networkMapping = networkMapping;
	}

	/** required by XML serialization (poor choice). Do not use in code. */
	@SuppressWarnings("unused")
	@Deprecated
	public int getCurrentId() {
		return currentId;
	}

	/** required by XML serialization (poor choice). Do not use in code. */
	@SuppressWarnings("unused")
	@Deprecated
	public void setCurrentId(int currentId) {
		this.currentId = currentId;
	}

	/** required by XML serialization (poor choice). Do not use in code. */
	@SuppressWarnings("unused")
	@Deprecated
	public int getCurrentNetworkId() {
		return currentNetworkId;
	}

	/** required by XML serialization (poor choice). Do not use in code. */
	@SuppressWarnings("unused")
	@Deprecated
	public void setCurrentNetworkId(int currentNetworkId) {
		this.currentNetworkId = currentNetworkId;
	}
}
