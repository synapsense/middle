package com.synapsense.bacnet.agent.auditors;

import com.synapsense.bacnet.agent.model.BACnetGWModel;
import com.synapsense.dto.ChangedValueTO;
import com.synapsense.dto.DTO;
import com.synapsense.service.nsvc.events.ObjectCreatedEvent;
import com.synapsense.service.nsvc.events.ObjectCreatedEventProcessor;
import com.synapsense.service.nsvc.events.ObjectDeletedEvent;
import com.synapsense.service.nsvc.events.ObjectDeletedEventProcessor;
import com.synapsense.service.nsvc.events.PropertiesChangedEvent;
import com.synapsense.service.nsvc.events.PropertiesChangedEventProcessor;
import com.synapsense.service.nsvc.events.RelationRemovedEvent;
import com.synapsense.service.nsvc.events.RelationRemovedEventProcessor;
import com.synapsense.service.nsvc.events.RelationSetEvent;
import com.synapsense.service.nsvc.events.RelationSetEventProcessor;

class SensorEventProcessor implements ObjectCreatedEventProcessor, ObjectDeletedEventProcessor, RelationSetEventProcessor,
		RelationRemovedEventProcessor, PropertiesChangedEventProcessor {

	private final BACnetGWModel model;

    SensorEventProcessor(BACnetGWModel model) {
    	this.model = model;
    }

	@Override
	public ObjectCreatedEvent process(ObjectCreatedEvent oce) {
		model.addSensor(oce.getObjectTO(), DTO.toMap(oce.getValues()));
		return null;
	}

	@Override
	public ObjectDeletedEvent process(ObjectDeletedEvent ode) {
		model.removeSensor(ode.getObjectTO());
		return null;
	}

	@Override
	public RelationSetEvent process(RelationSetEvent rse) {
		model.addSensorToNode(rse.getParentTO(), rse.getChildTO());
		return null;
	}

	@Override
	public RelationRemovedEvent process(RelationRemovedEvent rre) {
		model.removeSensorFromNode(rre.getParentTO(), rre.getChildTO());
		return null;
	}

	@Override
	public PropertiesChangedEvent process(PropertiesChangedEvent pce) {
		for (ChangedValueTO changeValueTO: pce.getChangedValues()) {
			model.updateSensorProperty(pce.getObjectTO(), changeValueTO);
		}
		return null;
	}
}