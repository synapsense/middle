package com.synapsense.bacnet.agent;

import com.synapsense.bacnet.agent.auditors.AuditorTask;
import com.synapsense.bacnet.agent.auditors.StatusAuditor;
import com.synapsense.bacnet.agent.wrappers.BACnetStack;
import com.synapsense.bacnet.agent.wrappers.devices.Gateway;
import com.synapsense.bacnet.objects.BACnetDeviceWrapper;
import com.synapsense.bacnet.system.BACnetNetwork;
import com.synapsense.bacnet.system.BACnetSystem;
import com.synapsense.bacnetgw.config.BacnetNetwork;
import com.synapsense.scheduler.TaskScheduler;
import java.beans.XMLDecoder;
import java.io.InputStream;
import java.util.concurrent.CountDownLatch;
import org.apache.log4j.Logger;

/** BACnet Gateway entry point */
public class BACnetGW {

	private BACnetStack stackThread;
	private AuditorTask auditorTaskThread;

	private final static String GW_CONFIG = "bacnetgw.xml";
	private final static String MAPPING_FILE = "bacnetMappingCfg.xml";

	private final static Logger logger = Logger.getLogger(BACnetGW.class);

	private BACnetGW() {
	}

	private void init() throws Exception {
		logger.info("Loading the configuration.");
		XMLDecoder decoder = new XMLDecoder(BACnetGW.class.getClassLoader().getResourceAsStream(GW_CONFIG));
		BACnetGWConfig config = (BACnetGWConfig) decoder.readObject();
		decoder.close();

		// [BC 10/15] This is the custom mapping that we don't currently support
		BacnetNetwork mapping = readConfig(MAPPING_FILE);
		auditorTaskThread = new AuditorTask(config, mapping);
		initBACnetGW(config);
		initBACnetStack(config);
	}

	private void initBACnetGW(BACnetGWConfig config) throws Exception {
		StatusAuditor.init(config);
		BACnetNetwork own = BACnetSystem.getInstance().createNetwork(config.getStackConfig().getOwnNet());
		int bacnetId = auditorTaskThread.getWsnModelAuditor().getMapper().getMapping().getId("BACnetGW");
		own.addDevice(new BACnetDeviceWrapper(new Gateway(config, bacnetId)));
	}

	private void initBACnetStack(BACnetGWConfig config) {
		stackThread = new BACnetStack();
		stackThread.init(config);
	}

	private static BacnetNetwork readConfig(String fileName) {
		BacnetNetwork conf = null;

		InputStream is = BACnetGW.class.getClassLoader().getResourceAsStream(fileName);
		if (is == null) {
			logger.info("No BACNet mapping file found in " + fileName);
		} else {
			try {
				XMLDecoder decoder = new XMLDecoder(is);
				conf = (BacnetNetwork) decoder.readObject();
				decoder.close();
			} catch (Exception e) {
				logger.info("BACNet mapping file " + fileName + " cannot be parsed.", e);
			}
		}
		return conf;
	}

	private void start() {
		// TODO [BC 09/2015] Don't use SynapSoft task wrappers for these services.
		TaskScheduler.getInstance().addSvcTask(stackThread);
		TaskScheduler.getInstance().addSvcTask(auditorTaskThread);
	}

	private void shutdown() {
		try {
			logger.info("Stopping BACnet GW ...");

			// Just interrupt the tasks, we can't wait on stackTread because it does blocking I/O.
			TaskScheduler.getInstance().shutdownNow();

			auditorTaskThread.stop();
			logger.info("BACnet GW stopped.");
		} catch (Exception e) {
			logger.warn("BACnet GW encountered an error while stopping.", e);
		}
	}

	public static void main(String[] args) throws InterruptedException {
		try {
			logger.info("Starting BACnet GW ...");

			final BACnetGW agent = new BACnetGW();
			agent.init();
			agent.start();
			Runtime.getRuntime().addShutdownHook(new Thread(agent::shutdown, "BG Shutdown"));

			logger.info("BACnet GW started.");
		} catch (Exception e) {
			logger.fatal("BACNet GW encountered a fatal exception and will exit.", e);
			System.exit(1);
		}

		//Keep the VM alive
		new CountDownLatch(1).await();
	}

	/** Called by AI when the Windows service is stopped. Just delegate to System.exit(). */
	@SuppressWarnings("unused")
	public static void stop() {
		System.exit(0);
	}
}
