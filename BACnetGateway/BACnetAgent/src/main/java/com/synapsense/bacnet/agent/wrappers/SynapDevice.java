package com.synapsense.bacnet.agent.wrappers;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.TimeZone;

import org.apache.log4j.Logger;

import com.synapsense.bacnet.agent.BACnetGWConfig;
import com.synapsense.bacnet.enums.BACnetSegmentation;
import com.synapsense.bacnet.objects.BACnetDevice;
import com.synapsense.bacnet.objects.BACnetObject;
import com.synapsense.bacnet.objects.BACnetProperty;
import com.synapsense.bacnet.system.BACnetPropertyId;
import com.synapsense.bacnet.system.shared.BACnetAddressBinding;
import com.synapsense.bacnet.system.shared.BACnetCOVSubscription;
import com.synapsense.bacnet.system.shared.BACnetDateTimeValue.DayOfMonthExtender;
import com.synapsense.bacnet.system.shared.BACnetDateTimeValue.MonthExtender;
import com.synapsense.bacnet.system.shared.BACnetDateValue;
import com.synapsense.bacnet.system.shared.BACnetObjectIdentifier;
import com.synapsense.bacnet.system.shared.BACnetObjectType;
import com.synapsense.bacnet.system.shared.BACnetService;
import com.synapsense.bacnet.system.shared.BACnetTimeValue;

public abstract class SynapDevice implements BACnetDevice {
	private final static Logger logger = Logger.getLogger(SynapDevice.class);
	protected BACnetObjectIdentifier id;
	Collection<BACnetAddressBinding> addressBindings;
	private BACnetGWConfig config;
	private java.util.List<BACnetObject> objects;
	private Collection<BACnetObject> objectsUmf;
	private List<BACnetCOVSubscription> covSubscriptions;

	protected SynapDevice(BACnetGWConfig config) {
		this.config = config;
		addressBindings = new ArrayList<BACnetAddressBinding>();
		objects = new ArrayList<BACnetObject>();
		covSubscriptions = new ArrayList<BACnetCOVSubscription>();
		objectsUmf = Collections.unmodifiableList(objects);
		attachObject(this);
	}

	/**
	 * Adds an object to device
	 * 
	 * @param obj
	 */
	protected void attachObject(BACnetObject obj) {
		objects.add(obj);
	}

	/**
	 * Removes an object from device
	 * 
	 * @param obj
	 */
	protected void detachObject(BACnetObject obj) {
		objects.remove(obj);
	}

	@Override
	public BACnetObjectIdentifier[] getObjectList() {
		BACnetObjectIdentifier[] ids = new BACnetObjectIdentifier[objects.size()];
		int i = 0;
		for (BACnetObject obj : objects) {
			ids[i++] = obj.getObjectIdentifier();
		}
		return ids;
	}

	public void addActiveCOVSubscription(BACnetCOVSubscription covSubscription) {
		covSubscriptions.add(covSubscription);
		logger.debug("addActiveCOVSubscription=" + covSubscription);
		logger.debug("covSubscriptions SIZE=" + covSubscriptions.size());
	}

	public void removeActiveCOVSubscription(BACnetCOVSubscription covSubscription) {
		covSubscriptions.remove(covSubscription);
		logger.debug("removeActiveCOVSubscription=" + covSubscription);
		logger.debug("covSubscriptions SIZE=" + covSubscriptions.size());
	}

	@Override
	public BACnetService[] getProtocolServicesSupported() {
		BACnetService[] svcs = { BACnetService.Who_Is, BACnetService.I_am, BACnetService.Who_Has, BACnetService.I_have,
		        BACnetService.ReadProperty, BACnetService.ReadPropertyMultiple, BACnetService.WriteProperty,
		        BACnetService.SubscribeCOVProperty, BACnetService.ConfirmedCOVNotification,
		        BACnetService.UnconfirmedCOVNotification };
		return svcs;
	}

	@Override
	public BACnetCOVSubscription[] getActiveCOVSubscriptionsList() {
		ArrayList<BACnetCOVSubscription> toAdd = new ArrayList<BACnetCOVSubscription>();
		for (BACnetCOVSubscription curCOV : covSubscriptions) {
			long timeRemaining = curCOV.getSecondsRemain();
			if (timeRemaining > 0) {
				curCOV.timeRemaining = timeRemaining;
				toAdd.add(curCOV);
			}
		}

		BACnetCOVSubscription[] covList = new BACnetCOVSubscription[toAdd.size()];
		int i = 0;
		for (BACnetCOVSubscription curCOV : toAdd) {
			covList[i++] = curCOV;
		}

		return covList;
	}

	@Override
	public short[] getProtocolObjectTypesSupported() {
		short[] types = new short[objects.size()];
		int i = 0;
		for (BACnetObject obj : objects) {
			types[i++] = obj.getObjectType();
		}
		return types;
	}

	@Override
	public long getAPDUTimeout() {
		return config.getAdpuTimeout();
	}

	@Override
	public String getApplicationSoftwareVersion() {
		return config.getApplicationSoftVersion();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsense.bacnet.objects.BACnetDevice#getDatabaseRevision()
	 * This property, of type Unsigned, is a logical revision number for the
	 * device's database. It is incremented when an object is created, an object
	 * is deleted, an object's name is changed, an object's Object_Identifier
	 * property is changed, or a restore is performed.
	 */
	@Override
	public long getDatabaseRevision() {
		return 1;
	}

	@Override
	public String getFirmwareRevision() {
		return config.getFirmwareRevision();
	}

	@Override
	public long getMaxAPDULengthAccepted() {
		return config.getMaxAPDULenAccepted();
	}

	@Override
	public String getModelName() {
		return config.getCommonModelName();
	}

	@Override
	public long getNumberOfAPDURetries() {
		return config.getNumberOfADPURetries();
	}

	@Override
	public long getProtocolRevision() {
		return config.getProtocolRevision();
	}

	@Override
	public long getProtocolVersion() {
		return config.getProtocolVersion();
	}

	@Override
	public int getSegmentationSupported() {
		return BACnetSegmentation.NO_SEGMENTATION.code;
	}

	@Override
	public long getVendorIdentifier() {
		return config.getVendorIdentifier();
	}

	@Override
	public String getVendorName() {
		return config.getVendorName();
	}

	@Override
	public BACnetObjectIdentifier getObjectIdentifier() {
		return id;
	}

	@Override
	public short getObjectType() {
		return BACnetObjectType.DEVICE;
	}

	@Override
	public void setObjectIdentifier(BACnetObjectIdentifier id) {
		logger.debug("Device got BACnetID=" + id.toString());
		this.id = id;
	}

	@Override
	public Collection<BACnetAddressBinding> getDeviceAddressBindings() {
		return addressBindings;
	}

	@Override
	public Collection<BACnetObject> getObjects() {
		return objectsUmf;
	}

	@BACnetProperty(id = BACnetPropertyId.OBJECT_LIST, codec = "ObjectIDCodec")
	public BACnetObjectIdentifier getObjID(int index) {
		return objects.get(index).getObjectIdentifier();
	}

	@BACnetProperty(id = BACnetPropertyId.LOCAL_TIME, codec = "TimeCodec")
	public BACnetTimeValue getLocalTime() {
		return new BACnetTimeValue(new GregorianCalendar());
	}

	@BACnetProperty(id = BACnetPropertyId.LOCAL_DATE, codec = "DateCodec")
	public BACnetDateValue getLocalDate() {
		return new BACnetDateValue(new GregorianCalendar(), MonthExtender.SIMPLE, DayOfMonthExtender.SIMPLE);
	}

	@BACnetProperty(id = BACnetPropertyId.UTC_OFFSET, codec = "SIntCodec")
	public int getUTCOffset() {
		return TimeZone.getDefault().getOffset(Calendar.ZONE_OFFSET) / (1000 * 60);
	}

	@BACnetProperty(id = BACnetPropertyId.DAYLIGHT_SAVING_STATUS, codec = "BooleanCodec")
	public boolean getDaylightSavingStatus() {
		return TimeZone.getDefault().useDaylightTime();
	}

	@Override
	public String getProfileName() {
		return "266-";
	}

	@Override
	public BACnetAddressBinding[] getAddressBindings() {
		BACnetAddressBinding[] listOfAddressBindings = new BACnetAddressBinding[addressBindings.size()];
		int i = 0;
		for (BACnetAddressBinding bindAddress : addressBindings) {
			listOfAddressBindings[i++] = bindAddress;
		}
		return listOfAddressBindings;
	}
}
