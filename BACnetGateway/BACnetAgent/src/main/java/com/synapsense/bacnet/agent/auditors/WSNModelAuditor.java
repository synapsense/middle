package com.synapsense.bacnet.agent.auditors;

import com.panduit.sz.util.concurrent.PExecutors;
import com.synapsense.bacnet.agent.BACnetGWConfig;
import com.synapsense.bacnet.agent.ESContext;
import com.synapsense.bacnet.agent.model.BACnetGWModel;
import com.synapsense.bacnet.agent.model.BACnetObserver;
import com.synapsense.bacnet.agent.BACnetGWMapper;
import com.synapsense.bacnet.agent.utils.ESTypes;
import com.synapsense.bacnet.agent.utils.ESUtils;
import com.synapsense.bacnet.agent.wrappers.devices.Network;
import com.synapsense.bacnet.agent.wrappers.devices.Node;
import com.synapsense.bacnet.agent.wrappers.objects.Sensor;
import com.synapsense.dto.CollectionTO;
import com.synapsense.dto.DTO;
import com.synapsense.dto.TO;
import com.synapsense.service.Environment;
import com.synapsense.service.nsvc.EventProcessor;
import com.synapsense.service.nsvc.NotificationService;
import com.synapsense.service.nsvc.events.AsyncProcessor;
import com.synapsense.service.nsvc.events.DispatchingProcessor;
import com.synapsense.service.nsvc.filters.ObjectCreatedEventFilter;
import com.synapsense.service.nsvc.filters.ObjectDeletedEventFilter;
import com.synapsense.service.nsvc.filters.PropertiesChangedEventFilter;
import com.synapsense.service.nsvc.filters.RelationRemovedEventFilter;
import com.synapsense.service.nsvc.filters.RelationSetEventFilter;
import com.synapsense.service.nsvc.filters.TOMatcher;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import org.apache.log4j.Logger;

/** Builds a BACnet model from the DB and then keeps it up to date based on ES object model events. */
public class WSNModelAuditor {

	private final BACnetGWMapper mapper;
	private final BACnetGWModel model;
	private final Map<EventProcessor, EventProcessor> processors;
	// Single-threaded executor for handling ES events. This avoids the need of synchronization for model changes.
	private final ScheduledExecutorService eventExecutor;

	private static final String EVENT_EXECUTOR_NAME = "ES Event Processor";

	private static final Logger logger = Logger.getLogger(WSNModelAuditor.class);

	public WSNModelAuditor(BACnetGWConfig config) throws IOException {
		mapper = new BACnetGWMapper(config.getInitialDeviceId(), config.getStackConfig().getWsnStartNet());
		model = new BACnetGWModel(config, mapper);
		// This inconspicuous listener is how the BACnet stack gets notified of changes to the model
		model.addListener(new BACnetObserver());
		this.eventExecutor = PExecutors.newSingleThreadScheduledExecutor(EVENT_EXECUTOR_NAME);
		this.processors = createProcessors(
				new NetworkEventProcessor(model),
				new NodeEventProcessor(model),
				new SensorEventProcessor(model));
	}

	public BACnetGWMapper getMapper() {
		return mapper;
	}

	/**
	 * Creates external BACnet networks from data of ES
	 *
	 * <p>This is called each time BG reconnects to ES so we can't assume the model is empty (as the MERA implementation did).</p>
	 */
	public void initModel(final Environment env) throws ExecutionException, InterruptedException {
		// Access to the model is serialized through the eventExecutor
		PExecutors.executeV(eventExecutor, () -> initModelInternal(env));
	}

	private void initModelInternal(Environment env) throws Exception {
		logger.info("Initialization of BACnet system...");

		model.clear();

		Collection<TO<?>> networks = env.getObjectsByType(ESTypes.NETWORK);
		for (TO<?> network : networks) {
			logger.info("Loading " + ESUtils.saveTO(network) + ".");
			model.addNetwork(network);

			Collection<TO<?>> nodes = env.getChildren(network, ESTypes.NODE);
			Collection<CollectionTO> nodeObjects = ESContext.getEnv().getPropertyValue(nodes, Node.getPropertyNames());
			for (CollectionTO nodeObject : nodeObjects) {
				TO<?> node = nodeObject.getObjId();
				model.addNode(node, DTO.toMap(nodeObject.getPropValues()));
				model.addNodeToNetwork(network, node);

				Collection<TO<?>> sensors = env.getChildren(node, ESTypes.SENSOR);
				Collection<CollectionTO> sensorObjects = env.getPropertyValue(sensors, Sensor.getPropertyNames());
				for (CollectionTO sensorObject : sensorObjects) {
					TO<?> sensor = sensorObject.getObjId();
					model.addSensor(sensor, DTO.toMap(sensorObject.getPropValues()));
					model.addSensorToNode(node, sensor);
				}
			}
		}
		mapper.saveMapping();

		logger.info("BACnet system initialized.");
	}

	public void startEventProcessing(NotificationService nsvc) {
		// Register event processors
		logger.info("Registering event processors...");
		for (Entry<EventProcessor, EventProcessor> entry : processors.entrySet()) {
			// [BC 09/2015] We don't de-register here.
			//  - First time it's not required.
			//  - On re-connect the ES would not have the registration and fail anyway.
			nsvc.registerProcessor(entry.getKey(), entry.getValue());
		}

		// Save mappings periodically if changed
		eventExecutor.scheduleWithFixedDelay(new Runnable() {
			@Override
			public void run() {
				saveMappings();
			}
		}, 10, 10, TimeUnit.SECONDS);
	}

	private void saveMappings() {
		try {
			mapper.saveMapping();
		} catch (Exception e) {
			logger.error("Failed to save mappings.", e);
		}
	}

	public void stopEventProcessing(NotificationService nsvc) throws InterruptedException {
		// [BC 09/2015] It looks like proxy.realease() was enough to clean up.
//		for (EventProcessor processor : processors.keySet()) {
//			nsvc.deregisterProcessor(processor);
//		}

		// Stop eventExecutor
		if (!PExecutors.stopImmediately(eventExecutor)) {
			logger.warn("Failed to stop " + EVENT_EXECUTOR_NAME + "executor.");
		}

	}

	/** Returns a Map with processors and their filters */
	private Map<EventProcessor, EventProcessor> createProcessors(
			NetworkEventProcessor networkEP, NodeEventProcessor nodeEP, SensorEventProcessor sensorEP) {

		Map<EventProcessor, EventProcessor> map = new HashMap<>();
		TOMatcher networkMatcher = new TOMatcher(ESTypes.NETWORK);
		TOMatcher nodeMatcher = new TOMatcher(ESTypes.NODE);
		TOMatcher sensorMatcher = new TOMatcher(ESTypes.SENSOR);

		DispatchingProcessor networkDispatcher = new DispatchingProcessor();
		DispatchingProcessor nodeDispatcher = new DispatchingProcessor();
		DispatchingProcessor sensorDispatcher = new DispatchingProcessor();
		DispatchingProcessor networkFilter = new DispatchingProcessor();
		DispatchingProcessor nodeFilter = new DispatchingProcessor();
		DispatchingProcessor sensorFilter = new DispatchingProcessor();


		// For networks
		networkDispatcher.putOcep(networkEP);
		networkDispatcher.putOdep(networkEP);
		networkDispatcher.putPcep(networkEP);
		networkFilter.putOcep(new ObjectCreatedEventFilter(networkMatcher));
		networkFilter.putOdep(new ObjectDeletedEventFilter(networkMatcher));
		networkFilter.putPcep(new PropertiesChangedEventFilter(networkMatcher,
				Network.getPropertyNames()));
		// For nodes
		nodeDispatcher.putOcep(nodeEP);
		nodeDispatcher.putOdep(nodeEP);
		nodeDispatcher.putRsep(nodeEP);
		nodeDispatcher.putRrep(nodeEP);
		nodeDispatcher.putPcep(nodeEP);
		nodeFilter.putOcep(new ObjectCreatedEventFilter(nodeMatcher));
		nodeFilter.putOdep(new ObjectDeletedEventFilter(nodeMatcher));
		nodeFilter.putRsep(new RelationSetEventFilter(networkMatcher,
				nodeMatcher));
		nodeFilter.putRrep(new RelationRemovedEventFilter(networkMatcher,
				nodeMatcher));
		nodeFilter.putPcep(new PropertiesChangedEventFilter(nodeMatcher, Node
				.getPropertyNames()));
		// For sensors
		sensorDispatcher.putOcep(sensorEP);
		sensorDispatcher.putOdep(sensorEP);
		sensorDispatcher.putRsep(sensorEP);
		sensorDispatcher.putRrep(sensorEP);
		sensorDispatcher.putPcep(sensorEP);

		sensorFilter.putOcep(new ObjectCreatedEventFilter(sensorMatcher));
		sensorFilter.putOdep(new ObjectDeletedEventFilter(sensorMatcher));
		sensorFilter.putRsep(new RelationSetEventFilter(nodeMatcher,
				sensorMatcher));
		sensorFilter.putRrep(new RelationRemovedEventFilter(nodeMatcher,
				sensorMatcher));
		sensorFilter.putPcep(new PropertiesChangedEventFilter(sensorMatcher,
				Sensor.getPropertyNames()));

		map.put(new AsyncProcessor(networkDispatcher, eventExecutor), networkFilter);
		map.put(new AsyncProcessor(nodeDispatcher, eventExecutor), nodeFilter);
		map.put(new AsyncProcessor(sensorDispatcher, eventExecutor), sensorFilter);
		return map;
	}

}
