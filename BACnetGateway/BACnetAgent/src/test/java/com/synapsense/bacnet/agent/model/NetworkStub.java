package com.synapsense.bacnet.agent.model;

import com.synapsense.bacnet.agent.BACnetGWConfig;
import com.synapsense.bacnet.agent.wrappers.devices.Network;
import com.synapsense.dto.TO;
import mockit.Deencapsulation;
import mockit.Mock;
import mockit.MockUp;

/** Stub for Network that just passes along the TO. */
public final class NetworkStub extends MockUp<Network> {

	@Mock
	public void $init(BACnetGWConfig config, TO<Integer> to, int bacnetId) {
		// We only care about the TO
		Deencapsulation.setField(getMockInstance(), "to", to);
	}

	/** readable alternative to instantiating NetworkStub. Instantiating the MockUp activates mocking of the associated Network class. */
	public static void activate() {
		new NetworkStub();
	}
}

