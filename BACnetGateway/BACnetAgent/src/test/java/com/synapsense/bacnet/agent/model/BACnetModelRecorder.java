package com.synapsense.bacnet.agent.model;

import com.google.common.base.Joiner;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.ArrayList;
import java.util.List;
import mockit.Deencapsulation;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Records BACnetModelListener events as a list of strings.
 *
 * <p>Note: We take advantage that all SynapDevice classes have a getTO() method. Life could have bee easier if getTO had been in the base
 * class.</p>
 */
public class BACnetModelRecorder implements InvocationHandler {

	private final List<String> actualEvents = new ArrayList<>();

	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		List<String> stringArgs = new ArrayList<>();
		for (Object arg : args) {
			stringArgs.add(Deencapsulation.invoke(arg, "getTO").toString());
		}
		actualEvents.add(method.getName() + '(' + Joiner.on(",").join(stringArgs) + ")");
		return null;
	}

	public BACnetGWModelListener getListener() {
		return (BACnetGWModelListener) Proxy.newProxyInstance(getClass().getClassLoader(), new Class[]{BACnetGWModelListener.class}, this);
	}

	public void verifyEvents(String... expectedEvents) {
		assertThat(actualEvents).containsExactly(expectedEvents);
	}
}
