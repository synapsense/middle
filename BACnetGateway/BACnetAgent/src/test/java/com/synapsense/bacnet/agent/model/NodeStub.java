package com.synapsense.bacnet.agent.model;

import com.synapsense.bacnet.agent.BACnetGWConfig;
import com.synapsense.bacnet.agent.wrappers.devices.Node;
import com.synapsense.dto.TO;
import java.util.Map;
import mockit.Deencapsulation;
import mockit.Mock;
import mockit.MockUp;

/** Stub for Node that just passes along the TO. */
public final class NodeStub extends MockUp<Node> {

	@Mock
	public void $init(BACnetGWConfig config, TO<?> to, Map<String, Object> nodeProperties, int bacnetId) {
		// We only care about the TO
		Deencapsulation.setField(getMockInstance(), "to", to);
	}

	/** readable alternative to instantiating NodeStub. Instantiating the MockUp activates mocking of the associated Node class. */
	public static void activate() {
		new NodeStub();
	}
}

