package com.synapsense.bacnet.agent.model;

import com.synapsense.bacnet.agent.wrappers.objects.Sensor;
import com.synapsense.dto.TO;
import java.util.Map;
import mockit.Deencapsulation;
import mockit.Mock;
import mockit.MockUp;

/** Stub for Sensor that just passes along the TO. */
public final class SensorStub extends MockUp<Sensor> {

	@Mock
	public void $init(TO<?> to, Map<String, Object> sensorProperties) {
		// We only care about the TO
		Deencapsulation.setField(getMockInstance(), "to", to);
	}

	/** readable alternative to instantiating SensorStub. Instantiating the MockUp activates mocking of the associated Sensor class. */
	public static void activate() {
		new SensorStub();
	}
}

