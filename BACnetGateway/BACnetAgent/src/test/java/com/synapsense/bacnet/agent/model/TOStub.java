package com.synapsense.bacnet.agent.model;

import com.synapsense.dto.TO;

/** Stub for TO with a predictable toString() implementation. */
public class TOStub implements TO<Integer> {

	private final String type;
	private final Integer id;

	public TOStub(String type, Integer id) {
		this.type = type;
		this.id = id;
	}

	@Override
	public String toString() {
		return type + ":" + id;
	}

	@Override
	public Integer getID() {
		return id;
	}

	@Override
	public String getTypeName() {
		return type;
	}

	@Override
	public Integer getEsID() {
		return null;
	}
}
