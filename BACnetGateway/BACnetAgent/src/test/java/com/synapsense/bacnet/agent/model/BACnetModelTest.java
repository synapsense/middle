package com.synapsense.bacnet.agent.model;

import com.synapsense.bacnet.agent.BACnetGWMapper;
import com.synapsense.bacnet.agent.wrappers.devices.Network;
import com.synapsense.bacnet.agent.wrappers.devices.Node;
import com.synapsense.bacnet.agent.wrappers.objects.Sensor;
import com.synapsense.dto.ChangedValueTO;
import com.synapsense.dto.TO;
import java.util.Collections;
import mockit.Mocked;
import mockit.Verifications;
import mockit.integration.junit4.JMockit;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(JMockit.class)
public class BACnetModelTest {

	BACnetGWModel model;
	BACnetModelRecorder recorder;

	@Mocked
	BACnetGWMapper mapper;

	// Constants for object type (shortened)
	public static final String NETWORK = "NETWORK";
	public static final String NODE = "NODE";
	public static final String SENSOR = "SENSOR";

	@Before
	public void setUp() {
		// Stub device classes to disable BACnet stack initialization
		// See http://jmockit.org/tutorial/Faking.html#mocks
		NetworkStub.activate();
		NodeStub.activate();
		SensorStub.activate();

		// Create a model and register a listener that records all events as a list of strings
		// NOTE: I initially tried to use JMockit verifications but it was verbose and complex.
		model = new BACnetGWModel(null, mapper);
		recorder = new BACnetModelRecorder();
		model.addListener(recorder.getListener());
	}

	@Test(expected = NullPointerException.class)
	public void nullTO() {
		model.addNetwork(null);
	}

	@Test(expected = NullPointerException.class)
	public void nullProperties() {
		model.addNode(new TOStub(NODE, 1), null);
	}

	@Test
	public void idealSequence() {

		addNetwork(1);
		addNode(11);
		addNodeToNetwork(1, 11);
		addSensor(111);
		addSensorToNode(11, 111);
		removeSensorFromNode(11, 111);
		removeSensor(111);
		removeNodeFromNetwork(1, 11);
		removeNode(11);
		removeNetwork(1);
		// (1) To assert that the objects had been removed, we add them one more time.
		addNetwork(1);
		addNode(11);
		addNodeToNetwork(1, 11);
		addSensor(111);
		addSensorToNode(11, 111);

		recorder.verifyEvents(
				"addNetwork(NETWORK:1)",
				"addNode(NETWORK:1,NODE:11)",
				"addSensor(NODE:11,SENSOR:111)",
				"removeSensor(NODE:11,SENSOR:111)",
				"removeNode(NETWORK:1,NODE:11)",
				"removeNetwork(NETWORK:1)",
				// (1)
				"addNetwork(NETWORK:1)",
				"addNode(NETWORK:1,NODE:11)",
				"addSensor(NODE:11,SENSOR:111)"
		);
	}

	@Test
	public void idealSequenceWithMultipleObjects() {

		addNetwork(1);
		addNode(11);
		addNodeToNetwork(1, 11);
		addSensor(111);
		addSensorToNode(11, 111);
		addNetwork(2);
		addNode(22);
		addNodeToNetwork(2, 22);
		addSensor(222);
		addSensorToNode(22, 222);
		removeSensorFromNode(22, 222);
		removeSensor(222);
		removeNodeFromNetwork(2, 22);
		removeNode(22);
		removeNetwork(2);
		removeSensorFromNode(11, 111);
		removeSensor(111);
		removeNodeFromNetwork(1, 11);
		removeNode(11);
		removeNetwork(1);

		recorder.verifyEvents(
				"addNetwork(NETWORK:1)",
				"addNode(NETWORK:1,NODE:11)",
				"addSensor(NODE:11,SENSOR:111)",
				"addNetwork(NETWORK:2)",
				"addNode(NETWORK:2,NODE:22)",
				"addSensor(NODE:22,SENSOR:222)",
				"removeSensor(NODE:22,SENSOR:222)",
				"removeNode(NETWORK:2,NODE:22)",
				"removeNetwork(NETWORK:2)",
				"removeSensor(NODE:11,SENSOR:111)",
				"removeNode(NETWORK:1,NODE:11)",
				"removeNetwork(NETWORK:1)"
		);
	}

	@Test
	public void observedSequence() {

		addNetwork(1);
		addSensor(111); // sensor created before node
		addNode(11);
		addNodeToNetwork(1, 11);
		addSensorToNode(11, 111);
		removeNode(11); // no removeNodeFromNetwork()
		removeNetwork(1);
		removeSensor(111); // no removeSensorFromNode(), not before removeNode()
		// (1) To assert that the objects had been removed, we add them one more time.
		addNetwork(1);
		addNode(11);
		addNodeToNetwork(1, 11);
		addSensor(111);
		addSensorToNode(11, 111);

		// We assert that the order is the same as in the ideal sequence
		recorder.verifyEvents(
				"addNetwork(NETWORK:1)",
				"addNode(NETWORK:1,NODE:11)",
				"addSensor(NODE:11,SENSOR:111)",
				"removeSensor(NODE:11,SENSOR:111)",
				"removeNode(NETWORK:1,NODE:11)",
				"removeNetwork(NETWORK:1)",
				// (1)
				"addNetwork(NETWORK:1)",
				"addNode(NETWORK:1,NODE:11)",
				"addSensor(NODE:11,SENSOR:111)"
		);
	}

	@Test
	public void observedSequenceWithMultipleObjects() {

		addNetwork(1);
		addSensor(111); // sensor created before node
		addNode(11);
		addNodeToNetwork(1, 11);
		addSensorToNode(11, 111);
		addNetwork(2);
		addSensor(222); // sensor created before node
		addNode(22);
		addNodeToNetwork(2, 22);
		addSensorToNode(22, 222);
		removeNode(22); // no removeNodeFromNetwork()
		removeNetwork(2);
		removeSensor(222); // no removeSensorFromNode(), not before removeNode()
		removeNode(11); // no removeNodeFromNetwork()
		removeNetwork(1);
		removeSensor(111); // no removeSensorFromNode(), not before removeNode()
		// (1) To assert that the objects had been removed, we add them one more time.
		addNetwork(1);
		addNode(11);
		addNodeToNetwork(1, 11);
		addSensor(111);
		addSensorToNode(11, 111);
		addNetwork(2);
		addNode(22);
		addNodeToNetwork(2, 22);
		addSensor(222);
		addSensorToNode(22, 222);

		// We assert that the order is the same as in the ideal sequence
		recorder.verifyEvents(
				"addNetwork(NETWORK:1)",
				"addNode(NETWORK:1,NODE:11)",
				"addSensor(NODE:11,SENSOR:111)",
				"addNetwork(NETWORK:2)",
				"addNode(NETWORK:2,NODE:22)",
				"addSensor(NODE:22,SENSOR:222)",
				"removeSensor(NODE:22,SENSOR:222)",
				"removeNode(NETWORK:2,NODE:22)",
				"removeNetwork(NETWORK:2)",
				"removeSensor(NODE:11,SENSOR:111)",
				"removeNode(NETWORK:1,NODE:11)",
				"removeNetwork(NETWORK:1)",
				// (1)
				"addNetwork(NETWORK:1)",
				"addNode(NETWORK:1,NODE:11)",
				"addSensor(NODE:11,SENSOR:111)",
				"addNetwork(NETWORK:2)",
				"addNode(NETWORK:2,NODE:22)",
				"addSensor(NODE:22,SENSOR:222)"
		);
	}

	@Test
	public void twoNetworksFourNodesEightSensors() {

		addNetwork(1);
		addNetwork(2);
		addNode(11);
		addNode(12);
		addNode(21);
		addNode(22);
		addSensor(111);
		addSensor(112);
		addSensor(121);
		addSensor(122);
		addSensor(211);
		addSensor(212);
		addSensor(221);
		addSensor(222);
		addNodeToNetwork(1, 11);
		addNodeToNetwork(1, 12);
		addNodeToNetwork(2, 21);
		addNodeToNetwork(2, 22);
		addSensorToNode(11, 111);
		addSensorToNode(11, 112);
		addSensorToNode(12, 121);
		addSensorToNode(12, 122);
		addSensorToNode(21, 211);
		addSensorToNode(21, 212);
		addSensorToNode(22, 221);
		addSensorToNode(22, 222);
		removeNetwork(1);
		removeSensor(222);
		removeNode(22);
		removeNetwork(2);


		// We assert that the order is the same as in the ideal sequence
		recorder.verifyEvents(
				"addNetwork(NETWORK:1)",
				"addNetwork(NETWORK:2)",
				"addNode(NETWORK:1,NODE:11)",
				"addNode(NETWORK:1,NODE:12)",
				"addNode(NETWORK:2,NODE:21)",
				"addNode(NETWORK:2,NODE:22)",
				"addSensor(NODE:11,SENSOR:111)",
				"addSensor(NODE:11,SENSOR:112)",
				"addSensor(NODE:12,SENSOR:121)",
				"addSensor(NODE:12,SENSOR:122)",
				"addSensor(NODE:21,SENSOR:211)",
				"addSensor(NODE:21,SENSOR:212)",
				"addSensor(NODE:22,SENSOR:221)",
				"addSensor(NODE:22,SENSOR:222)",
				"removeSensor(NODE:11,SENSOR:111)",
				"removeSensor(NODE:11,SENSOR:112)",
				"removeNode(NETWORK:1,NODE:11)",
				"removeSensor(NODE:12,SENSOR:121)",
				"removeSensor(NODE:12,SENSOR:122)",
				"removeNode(NETWORK:1,NODE:12)",
				"removeNetwork(NETWORK:1)",
				"removeSensor(NODE:22,SENSOR:222)",
				"removeSensor(NODE:22,SENSOR:221)",
				"removeNode(NETWORK:2,NODE:22)",
				"removeSensor(NODE:21,SENSOR:211)",
				"removeSensor(NODE:21,SENSOR:212)",
				"removeNode(NETWORK:2,NODE:21)",
				"removeNetwork(NETWORK:2)"
		);
	}

	@Test
	public void changeNetworkOfNode() {

		addNetwork(1);
		addNode(11);
		addNodeToNetwork(1, 11);
		// Create a 2nd network and move the node to it
		addNetwork(2);
		addNodeToNetwork(2, 11); // add comes before remove
		removeNodeFromNetwork(1, 11);

		recorder.verifyEvents(
				"addNetwork(NETWORK:1)",
				"addNode(NETWORK:1,NODE:11)",
				"addNetwork(NETWORK:2)",
				"removeNode(NETWORK:1,NODE:11)", // we fix the add/remove order internally
				"addNode(NETWORK:2,NODE:11)"
		);
	}

	@Test
	public void changeNodeOfSensor() {

		addNetwork(1);
		addNode(11);
		addNodeToNetwork(1, 11);
		addSensor(111);
		addSensorToNode(11, 111);
		// Create a 2nd node and move the sensor to it
		addNode(12);
		addNodeToNetwork(1, 12);
		addSensorToNode(12, 111);
		removeSensorFromNode(11, 111);

		// The sensor cannot be moved
		recorder.verifyEvents(
				"addNetwork(NETWORK:1)",
				"addNode(NETWORK:1,NODE:11)",
				"addSensor(NODE:11,SENSOR:111)",
				"addNode(NETWORK:1,NODE:12)",
				"removeSensor(NODE:11,SENSOR:111)"
		);
	}

	@Test
	public void ignoreDuplicates() {

		addNetwork(1);
		addNetwork(1);
		addNode(11);
		addNode(11);
		addNodeToNetwork(1, 11);
		addNodeToNetwork(1, 11);
		addSensor(111);
		addSensor(111);
		addSensorToNode(11, 111);
		addSensorToNode(11, 111);
		removeSensorFromNode(11, 111);
		removeSensorFromNode(11, 111);
		removeSensor(111);
		removeSensor(111);
		removeNodeFromNetwork(1, 11);
		removeNodeFromNetwork(1, 11);
		removeNode(11);
		removeNode(11);
		removeNetwork(1);
		removeNetwork(1);
		// (1) To assert that the objects had been removed, we add them one more time.
		addNetwork(1);
		addNode(11);
		addNodeToNetwork(1, 11);
		addSensor(111);
		addSensorToNode(11, 111);

		recorder.verifyEvents(
				"addNetwork(NETWORK:1)",
				"addNode(NETWORK:1,NODE:11)",
				"addSensor(NODE:11,SENSOR:111)",
				"removeSensor(NODE:11,SENSOR:111)",
				"removeNode(NETWORK:1,NODE:11)",
				"removeNetwork(NETWORK:1)",
				// (1)
				"addNetwork(NETWORK:1)",
				"addNode(NETWORK:1,NODE:11)",
				"addSensor(NODE:11,SENSOR:111)"
		);
	}

	@Test
	public void cascadeDeleteNodes() {

		addNetwork(1);
		addNode(11);
		addNodeToNetwork(1, 11);
		addNode(12);
		addNodeToNetwork(1, 12);
		removeNetwork(1);

		recorder.verifyEvents(
				"addNetwork(NETWORK:1)",
				"addNode(NETWORK:1,NODE:11)",
				"addNode(NETWORK:1,NODE:12)",
				"removeNode(NETWORK:1,NODE:11)",
				"removeNode(NETWORK:1,NODE:12)",
				"removeNetwork(NETWORK:1)"
		);
	}

	@Test
	public void addNodeWithoutNetwork() {

		addNetwork(1);
		addNode(11);
		addNodeToNetwork(1, 12);
		addNodeToNetwork(2, 11);

		recorder.verifyEvents(
				"addNetwork(NETWORK:1)"
		);
	}

	@Test
	public void removeNodeWithWrongNetwork() {

		addNetwork(1);
		addNode(11);
		addNodeToNetwork(1, 11);
		removeNodeFromNetwork(2, 11);
		removeNodeFromNetwork(1, 12);

		recorder.verifyEvents(
				"addNetwork(NETWORK:1)",
				"addNode(NETWORK:1,NODE:11)"
		);
	}

	@Test
	public void addSensorWithoutNode() {

		addNetwork(1);
		addNode(11);
		addNodeToNetwork(1, 11);
		addSensor(111);
		addSensorToNode(11, 112);
		addSensorToNode(12, 111);
		recorder.verifyEvents(
				"addNetwork(NETWORK:1)",
				"addNode(NETWORK:1,NODE:11)"
		);
	}

	@Test
	public void removeSensorWithoutNode() {

		addNetwork(1);
		addNode(11);
		addNodeToNetwork(1, 11);
		addSensor(111);
		addSensorToNode(11, 111);
		removeSensorFromNode(11, 112);
		removeSensorFromNode(12, 111);
		recorder.verifyEvents(
				"addNetwork(NETWORK:1)",
				"addNode(NETWORK:1,NODE:11)",
				"addSensor(NODE:11,SENSOR:111)"
		);
	}

	@Test
	public void cascadeDeleteSensors() {

		addNetwork(1);
		addNode(11);
		addSensor(111);
		addNodeToNetwork(1, 11);
		addSensorToNode(11, 111);
		addSensor(112);
		addSensorToNode(11, 112);
		removeNode(11);
		removeNetwork(1);

		recorder.verifyEvents(
				"addNetwork(NETWORK:1)",
				"addNode(NETWORK:1,NODE:11)",
				"addSensor(NODE:11,SENSOR:111)",
				"addSensor(NODE:11,SENSOR:112)",
				"removeSensor(NODE:11,SENSOR:111)",
				"removeSensor(NODE:11,SENSOR:112)",
				"removeNode(NETWORK:1,NODE:11)",
				"removeNetwork(NETWORK:1)"
		);
	}

	@Test
	public void clear() {

		addNetwork(1);
		addNetwork(2);
		addNode(11);
		addNode(12);
		addNode(21);
		addNode(22);
		addNode(33); // node not linked to network
		addSensor(111);
		addSensor(112);
		addSensor(121);
		addSensor(122);
		addSensor(211);
		addSensor(212);
		addSensor(221);
		addSensor(222);
		addSensor(333); // sensor not linked to node
		addNodeToNetwork(1, 11);
		addNodeToNetwork(1, 12);
		addNodeToNetwork(2, 21);
		addNodeToNetwork(2, 22);
		addSensorToNode(11, 111);
		addSensorToNode(11, 112);
		addSensorToNode(12, 121);
		addSensorToNode(12, 122);
		addSensorToNode(21, 211);
		addSensorToNode(21, 212);
		addSensorToNode(22, 221);
		addSensorToNode(22, 222);
		model.clear();

		// We assert that the order is the same as in the ideal sequence
		recorder.verifyEvents(
				"addNetwork(NETWORK:1)",
				"addNetwork(NETWORK:2)",
				"addNode(NETWORK:1,NODE:11)",
				"addNode(NETWORK:1,NODE:12)",
				"addNode(NETWORK:2,NODE:21)",
				"addNode(NETWORK:2,NODE:22)",
				"addSensor(NODE:11,SENSOR:111)",
				"addSensor(NODE:11,SENSOR:112)",
				"addSensor(NODE:12,SENSOR:121)",
				"addSensor(NODE:12,SENSOR:122)",
				"addSensor(NODE:21,SENSOR:211)",
				"addSensor(NODE:21,SENSOR:212)",
				"addSensor(NODE:22,SENSOR:221)",
				"addSensor(NODE:22,SENSOR:222)",
				"removeSensor(NODE:11,SENSOR:111)",
				"removeSensor(NODE:11,SENSOR:112)",
				"removeNode(NETWORK:1,NODE:11)",
				"removeSensor(NODE:12,SENSOR:121)",
				"removeSensor(NODE:12,SENSOR:122)",
				"removeNode(NETWORK:1,NODE:12)",
				"removeNetwork(NETWORK:1)",
				"removeSensor(NODE:21,SENSOR:211)",
				"removeSensor(NODE:21,SENSOR:212)",
				"removeNode(NETWORK:2,NODE:21)",
				"removeSensor(NODE:22,SENSOR:221)",
				"removeSensor(NODE:22,SENSOR:222)",
				"removeNode(NETWORK:2,NODE:22)",
				"removeNetwork(NETWORK:2)"
		);
	}

	@Test
	public void updateNetworkProperty(@Mocked final Network network) {

		final TO networkTO = new TOStub(NETWORK, 1);
		final ChangedValueTO networkChangeTO = new ChangedValueTO("foo", "bar");

		final TO invalidNetworkTO = new TOStub(NETWORK, 2);
		final ChangedValueTO invalidNetworkChangeTO = new ChangedValueTO("invalidFoo", "invalidBar");

		model.addNetwork(networkTO);
		model.updateNetworkProperty(networkTO, networkChangeTO);
		// We skip adding invalidNetworkTO 
		model.updateNetworkProperty(invalidNetworkTO, invalidNetworkChangeTO);

		new Verifications() {{
			// updateProperty() invoked for the valid network
			network.updateProperty(networkChangeTO);

			// updateProperty() not invoked for the invalid network
			network.updateProperty(invalidNetworkChangeTO);
			times = 0;
		}};
	}

	@Test
	public void updateNodeProperty(@Mocked final Node node) {

		final TO nodeTO = new TOStub(NODE, 1);
		final ChangedValueTO nodeChangeTO = new ChangedValueTO("foo", "bar");

		final TO invalidNodeTO = new TOStub(NODE, 2);
		final ChangedValueTO invalidNodeChangeTO = new ChangedValueTO("invalidFoo", "invalidBar");

		model.addNode(nodeTO, Collections.<String, Object>emptyMap());
		model.updateNodeProperty(nodeTO, nodeChangeTO);
		// We skip adding invalidNodeTO 
		model.updateNodeProperty(invalidNodeTO, invalidNodeChangeTO);

		new Verifications() {{
			// updateProperty() invoked for the valid node
			node.updateProperty(nodeChangeTO);

			// updateProperty() not invoked for the invalid node
			node.updateProperty(invalidNodeChangeTO);
			times = 0;
		}};
	}

	@Test
	public void updateSensorProperty(@Mocked final Sensor sensor) {

		final TO sensorTO = new TOStub(SENSOR, 1);
		final ChangedValueTO sensorChangeTO = new ChangedValueTO("foo", "bar");

		final TO invalidSensorTO = new TOStub(SENSOR, 2);
		final ChangedValueTO invalidSensorChangeTO = new ChangedValueTO("invalidFoo", "invalidBar");

		model.addSensor(sensorTO, Collections.<String, Object>emptyMap());
		model.updateSensorProperty(sensorTO, sensorChangeTO);
		// We skip adding invalidSensorTO 
		model.updateSensorProperty(invalidSensorTO, invalidSensorChangeTO);

		new Verifications() {{
			// updateProperty() invoked for the valid sensor
			sensor.updateProperty(sensorChangeTO);

			// updateProperty() not invoked for the invalid sensor
			sensor.updateProperty(invalidSensorChangeTO);
			times = 0;
		}};
	}

	private void addNetwork(int networkId) {
		model.addNetwork(new TOStub(NETWORK, networkId));
	}

	private void removeNetwork(int networkId) {
		model.removeNetwork(new TOStub(NETWORK, networkId));
	}

	private void addNode(int nodeId) {
		model.addNode(new TOStub(NODE, nodeId), Collections.<String, Object>emptyMap());
	}

	private void addNodeToNetwork(int networkId, int nodeId) {
		model.addNodeToNetwork(new TOStub(NETWORK, networkId), new TOStub(NODE, nodeId));
	}

	private void removeNodeFromNetwork(int networkId, int nodeId) {
		model.removeNodeFromNetwork(new TOStub(NETWORK, networkId), new TOStub(NODE, nodeId));
	}

	private void removeNode(int nodeId) {
		model.removeNode(new TOStub(NODE, nodeId));
	}

	private void addSensor(int sensorId) {
		model.addSensor(new TOStub(SENSOR, sensorId), Collections.<String, Object>emptyMap());
	}

	private void addSensorToNode(int nodeId, int sensorId) {
		model.addSensorToNode(new TOStub(NODE, nodeId), new TOStub(SENSOR, sensorId));
	}

	private void removeSensorFromNode(int nodeId, int sensorId) {
		model.removeSensorFromNode(new TOStub(NODE, nodeId), new TOStub(SENSOR, sensorId));
	}

	private void removeSensor(int sensorId) {
		model.removeSensor(new TOStub(SENSOR, sensorId));
	}
}
