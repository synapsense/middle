/**
 * 
 */
package com.synapsense.bacnet.network.pdu.apdu.codecs.codecs.complex;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import com.synapsense.bacnet.network.pdu.apdu.codecs.PrimitivesDecoder;
import com.synapsense.bacnet.network.pdu.apdu.codecs.PrimitivesEncoder;
import com.synapsense.bacnet.network.pdu.apdu.codecs.codecs.Codec;
import com.synapsense.bacnet.network.pdu.apdu.codecs.exceptions.DecodingException;
import com.synapsense.bacnet.network.pdu.apdu.codecs.exceptions.EncodingException;
import com.synapsense.bacnet.system.shared.BACnetAddress;
import com.synapsense.bacnet.system.shared.BACnetAddressBinding;
import com.synapsense.bacnet.system.shared.BACnetObjectIdentifier;

/**
 * @author Kravin Alexander
 * 
 *         Device_Address_Binding is a list of BACnetAddressBinding
 * 
 *         BACnetAddressBinding ::= SEQUENCE { deviceObjectIdentifier
 *         BACnetObjectIdentifier, deviceAddress BACnetAddress }
 * 
 *         BACnetAddress ::= SEQUENCE { network-number Unsigned16, -- A value of
 *         0 indicates the local network mac-address OCTET STRING -- A string of
 *         length 0 indicates a broadcast }
 * 
 * 
 */
public class DeviceAddressBindingCodec extends Codec {

	@Override
	protected Object decode(ByteArrayInputStream in, Integer expected_id) throws DecodingException {
		PrimitivesDecoder decoder = new PrimitivesDecoder(in);
		Collection<BACnetAddressBinding> listOfAddressBinding = new ArrayList<BACnetAddressBinding>();

		try {
			while (decoder.askForTag() != null) {

				BACnetObjectIdentifier objectId = decoder.decodeObjectID(null);
				int networkNumber = (int) decoder.decodeUInt(null);
				byte[] mac = decoder.decodeOctetString(null);

				BACnetAddress address = new BACnetAddress(networkNumber, mac);
				listOfAddressBinding.add(new BACnetAddressBinding(objectId, address));
			}
		} catch (IOException e) {
			throw new DecodingException(e);
		}

		return listOfAddressBinding.toArray();
	}

	@Override
	protected void encode(ByteArrayOutputStream out, Integer id, Object payload) throws EncodingException {
		BACnetAddressBinding[] listOfAddressBinding = (BACnetAddressBinding[]) payload;

		PrimitivesEncoder encoder = new PrimitivesEncoder();

		try {
			for (BACnetAddressBinding bindAddress : listOfAddressBinding) {
				encoder.encodeObjectID(null, bindAddress.deviceObjectIdentifier);
				encoder.encodeUInt(null, bindAddress.deviceAddress.networkNumber);
				encoder.encodeOctetString(null, bindAddress.deviceAddress.macAddress);
			}

			out.write(encoder.getEncodedBuffer());
		} catch (IOException e) {
			throw new EncodingException(e);
		}
	}

}
