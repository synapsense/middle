/**
 * 
 */
package com.synapsense.bacnet.network.pdu.apdu.codecs.codecs.complex;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import com.synapsense.bacnet.network.pdu.apdu.codecs.PrimitivesDecoder;
import com.synapsense.bacnet.network.pdu.apdu.codecs.PrimitivesEncoder;
import com.synapsense.bacnet.network.pdu.apdu.codecs.codecs.Codec;
import com.synapsense.bacnet.network.pdu.apdu.codecs.exceptions.DecodingException;
import com.synapsense.bacnet.network.pdu.apdu.codecs.exceptions.EncodingException;
import com.synapsense.bacnet.system.shared.BACnetAddress;
import com.synapsense.bacnet.system.shared.BACnetCOVSubscription;
import com.synapsense.bacnet.system.shared.BACnetObjectPropertyReference;
import com.synapsense.bacnet.system.shared.BACnetRecipient;
import com.synapsense.bacnet.system.shared.BACnetRecipientProcess;

/**
 * @author Alexander Kravin
 * 
 *         Active_COV_Subscriptions is a list of BACnetCOVSubscription
 * 
 *         BACnetCOVSubscription ::= SEQUENCE { Recipient [0]
 *         BACnetRecipientProcess, MonitoredPropertyReference [1]
 *         BACnetObjectPropertyReference, IssueConfirmedNotifications [2]
 *         BOOLEAN, TimeRemaining [3] Unsigned, COVIncrement [4] REAL OPTIONAL
 *         -- used only with monitored -- properties with a datatype of REAL }
 * 
 *         BACnetRecipientProcess ::= SEQUENCE { recipient [0] BACnetRecipient,
 *         processIdentifier [1] Unsigned32 }
 * 
 *         BACnetRecipient ::= CHOICE { device [0] BACnetObjectIdentifier,
 *         address [1] BACnetAddress }
 * 
 *         BACnetAddress ::= SEQUENCE { network-number Unsigned16, -- A value of
 *         0 indicates the local network mac-address OCTET STRING -- A string of
 *         length 0 indicates a broadcast }
 * 
 *         BACnetObjectPropertyReference ::= SEQUENCE { objectIdentifier [0]
 *         BACnetObjectIdentifier, propertyIdentifier [1]
 *         BACnetPropertyIdentifier, propertyArrayIndex [2] Unsigned OPTIONAL --
 *         used only with array datatype -- if omitted with an array the entire
 *         array is referenced }
 * 
 */
public class ActiveCOVSubscriptionsCodec extends Codec {

	@Override
	protected Object decode(ByteArrayInputStream in, Integer expected_id) throws DecodingException {
		PrimitivesDecoder decoder = new PrimitivesDecoder(in);
		Collection<BACnetCOVSubscription> listOfSubscriptions = new ArrayList<BACnetCOVSubscription>();
		try {
			while (decoder.askForTag() != null) {
				// [0] - BACnetRecipientProcess
				BACnetRecipientProcess recipientProcess = new BACnetRecipientProcess();
				BACnetRecipient recipient;

				decoder.decodeSequenceStart(0);
				decoder.decodeSequenceStart(0);
				if (decoder.askForTag().getID() == 0) {
					recipient = new BACnetRecipient(decoder.decodeObjectID(0));
				} else {
					decoder.decodeSequenceStart(1);
					int networkNumber = (int) decoder.decodeUInt(null);
					byte[] mac = decoder.decodeOctetString(null);
					recipient = new BACnetRecipient(new BACnetAddress(networkNumber, mac));
					decoder.decodeSequenceStop(1);
				}
				recipientProcess.setRecipient(recipient);
				recipientProcess.processIdentifier = decoder.decodeUInt(1);

				// [1] - BACnetObjectPropertyReference
				BACnetObjectPropertyReference objectPropRef = new BACnetObjectPropertyReference();
				decoder.decodeSequenceStart(1);
				objectPropRef.setObjectIdentifier(decoder.decodeObjectID(0));
				objectPropRef.propertyIdentifier = decoder.decodeEnum(1);
				if (decoder.askForTag().getID() == 2) {
					objectPropRef.propertyArrayIndex = decoder.decodeUInt(2);
				}
				decoder.decodeSequenceStop(1);

				BACnetCOVSubscription covSubscription = new BACnetCOVSubscription();
				covSubscription.setRecipientProcess(recipientProcess);
				covSubscription.setMonitoredPropertyReference(objectPropRef);

				// [2] - IssueConfirmedNotifications
				covSubscription.issueConfirmedNotifications = decoder.decodeBoolean(2);

				// [3] - TimeRemaining
				covSubscription.timeRemaining = decoder.decodeUInt(3);

				// [4] - COVIncrement
				if (decoder.askForTag() != null && decoder.askForTag().getID() == 4) {
					covSubscription.COVIncrement = decoder.decodeReal(4);
				}

				// add result into the list
				listOfSubscriptions.add(covSubscription);
			}

		} catch (IOException e) {
			throw new DecodingException(e);
		}

		return listOfSubscriptions.toArray();
	}

	@Override
	protected void encode(ByteArrayOutputStream out, Integer id, Object payload) throws EncodingException {
		BACnetCOVSubscription[] listOfSubscriptions = (BACnetCOVSubscription[]) payload;

		PrimitivesEncoder encoder = new PrimitivesEncoder();

		try {
			for (BACnetCOVSubscription subscription : listOfSubscriptions) {

				// [0] - BACnetRecipientProcess
				encoder.encodeSequenceStart(0);
				BACnetRecipientProcess recipientProcess = subscription.getRecipientProcess();
				BACnetRecipient recipient = recipientProcess.getRecipient();
				encoder.encodeSequenceStart(0);
				if (recipient.getObjectIdentifier() != null) {
					encoder.encodeObjectID(0, recipient.getObjectIdentifier());
				} else {
					encoder.encodeSequenceStart(1);
					encoder.encodeUInt(null, recipient.getAddress().networkNumber);
					encoder.encodeOctetString(null, recipient.getAddress().macAddress);
					encoder.encodeSequenceStop(1);
				}
				encoder.encodeSequenceStop(0);

				encoder.encodeUInt(1, recipientProcess.processIdentifier);
				encoder.encodeSequenceStop(0);

				// [1] - BACnetObjectPropertyReference
				BACnetObjectPropertyReference objectPropRef = subscription.getMonitoredPropertyReference();
				encoder.encodeSequenceStart(1);
				encoder.encodeObjectID(0, objectPropRef.getObjectIdentifier());
				encoder.encodeEnum(1, objectPropRef.propertyIdentifier);
				if (objectPropRef.propertyArrayIndex != null) {
					encoder.encodeUInt(2, objectPropRef.propertyArrayIndex);
				}
				encoder.encodeSequenceStop(1);

				// [2] - IssueConfirmedNotifications
				encoder.encodeBoolean(2, subscription.issueConfirmedNotifications);

				// [3] - TimeRemaining
				encoder.encodeUInt(3, subscription.timeRemaining);

				// [4] - COVIncrement
				if (subscription.COVIncrement != null) {
					encoder.encodeReal(4, subscription.COVIncrement);
				}
			}
			out.write(encoder.getEncodedBuffer());
		} catch (IOException e) {
			throw new EncodingException(e);
		}

	}

}
