package com.synapsense.bacnet.network.pdu.apdu.codecs.codecs;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

import com.synapsense.bacnet.network.pdu.apdu.codecs.exceptions.DecodingException;
import com.synapsense.bacnet.network.pdu.apdu.codecs.exceptions.EncodingException;

public abstract class Codec {
	protected abstract void encode(ByteArrayOutputStream out, Integer id, Object payload) throws EncodingException;

	public void encode(EncodeContext context, Integer id, Object payload) throws EncodingException {
		encode(context.getStream(), id, payload);
	}

	public byte[] encode(Integer id, Object payload) throws EncodingException {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		encode(out, id, payload);
		return out.toByteArray();
	}

	protected abstract Object decode(ByteArrayInputStream in, Integer expected_id) throws DecodingException;

	public Object decode(DecodeContext context, Integer expected_id) throws DecodingException {
		return decode(context.getStream(), expected_id);
	}

	public Object decode(byte[] buffer, Integer expected_id) throws DecodingException {
		ByteArrayInputStream in = new ByteArrayInputStream(buffer);
		return decode(in, expected_id);
	}
}
