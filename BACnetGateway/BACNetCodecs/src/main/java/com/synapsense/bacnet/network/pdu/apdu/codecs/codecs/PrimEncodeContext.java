package com.synapsense.bacnet.network.pdu.apdu.codecs.codecs;

import com.synapsense.bacnet.network.pdu.apdu.codecs.codecs.primitive.PrimFactory;

public class PrimEncodeContext extends EncodeContext {
	private PrimFactory codecs_factory;

	public PrimEncodeContext() {
		super();
		codecs_factory = new PrimFactory();
	}

	@Override
	public CodecsAF getCodecsFactory() {
		return codecs_factory;
	}

}
