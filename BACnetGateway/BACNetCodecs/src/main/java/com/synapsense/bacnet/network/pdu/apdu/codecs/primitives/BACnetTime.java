package com.synapsense.bacnet.network.pdu.apdu.codecs.primitives;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.GregorianCalendar;

import com.synapsense.bacnet.system.shared.BACnetTimeValue;

/**
 * The encoding of a time value shall be primitive, with four contents octets.
 * Time values shall be encoded in the contents octets as four binary integers.
 * The first contents octet shall represent the hour, in the 24-hour system (1
 * P.M. = D'13'); the second octet shall represent the minute of the hour; the
 * third octet shall represent the second of the minute; and the fourth octet
 * shall represent the fractional part of the second in hundredths of a second.
 * A value of X'FF' = D'255' in any of the four octets shall indicate that the
 * corresponding value is unspecified. If all four octets = X'FF', the
 * corresponding time may be interpreted as "any" or "don't care."
 * 
 * @author anechaev
 * 
 */
public class BACnetTime extends BACnetInteger {
	private final static ValueType type = ValueType.TIME;

	private final static int UNSPECIFIED_ENCODE = 0xFF;

	private BACnetTimeValue time;

	public BACnetTime(Integer id, BACnetTimeValue payload) {
		time = (BACnetTimeValue) payload.clone();
		encode_payload();
		InitializeContent(id, this.payload, type);
	}

	public BACnetTime(ByteArrayInputStream stream, Integer expected_id) throws IOException {
		super(stream, expected_id, type);
		decode_payload();
	}

	@Override
	protected void calc_most_sign_byte() {
		most_sign_byte = 4;
	}

	public BACnetTimeValue getPayload() {
		return time;
	}

	private void encode_payload() {
		payload = 0;
		GregorianCalendar value = time.getDate();

		if (value.isSet(Calendar.HOUR_OF_DAY)) {
			payload |= 0xFF & value.get(Calendar.HOUR_OF_DAY);
		} else {
			payload |= UNSPECIFIED_ENCODE;
		}
		payload <<= 8;

		if (value.isSet(Calendar.MINUTE)) {
			payload |= 0xFF & value.get(Calendar.MINUTE);
		} else {
			payload |= UNSPECIFIED_ENCODE;
		}
		payload <<= 8;

		if (value.isSet(Calendar.SECOND)) {
			payload |= 0xFF & value.get(Calendar.SECOND);
		} else {
			payload |= UNSPECIFIED_ENCODE;
		}
		payload <<= 8;

		if (value.isSet(Calendar.MILLISECOND)) {
			payload |= 0xFF & (value.get(Calendar.MILLISECOND) / 10);
		} else {
			payload |= UNSPECIFIED_ENCODE;
		}
	}

	private void decode_payload() {
		int cur_payload;

		GregorianCalendar value = new GregorianCalendar();
		value.clear();

		cur_payload = (int) (payload & 0xFF000000) >> 24;
		if (cur_payload != UNSPECIFIED_ENCODE)
			value.set(Calendar.HOUR_OF_DAY, cur_payload);

		cur_payload = (int) (payload & 0xFF0000) >> 16;
		if (cur_payload != UNSPECIFIED_ENCODE)
			value.set(Calendar.MINUTE, cur_payload);

		cur_payload = (int) (payload & 0xFF00) >> 8;
		if (cur_payload != UNSPECIFIED_ENCODE)
			value.set(Calendar.SECOND, cur_payload);

		cur_payload = (int) (payload & 0xFF00) >> 8;
		if (cur_payload != UNSPECIFIED_ENCODE)
			value.set(Calendar.MILLISECOND, cur_payload * 10);
	}
}
