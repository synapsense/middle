/**
 * 
 */
package com.synapsense.bacnet.network.pdu.apdu.codecs.primitives;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * OCTET STRING primitive; The encoding of an octet string value shall be
 * primitive. The encoding shall contain zero, one, or more contents octets
 * equal in value to the octets in the data value, in the order in which they
 * appear in the data value, and with the most significant bit of an octet of
 * the data value aligned with the most significant bit of an octet of the
 * contents octets. (20.2.8)
 * 
 * @author anechaev
 * 
 */
public class BACnetOctetString extends BACnetTLV {
	private final static ValueType type = ValueType.OCTET_STRING;
	private byte[] payload;

	/**
	 * Creates new OCTET STRING from byte[].
	 * 
	 * @param id
	 *            - null for APPLICATION class tags, not null tag id for CONTEXT
	 *            SPECIFIC tags
	 * @param content
	 *            - byte[] payload
	 */
	public BACnetOctetString(Integer id, byte[] payload) {
		super();
		createTag(id, type, payload.length);
		this.payload = payload.clone();
	}

	public BACnetOctetString(ByteArrayInputStream stream, Integer expected_id) throws IOException {
		super(stream, expected_id, type);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.synapsense.bacnet.APDUparser.codecs.BACnetEncodeable#encode(byte[],
	 * int)
	 */
	@Override
	public void encode(ByteArrayOutputStream stream) throws IOException {
		tag.encode(stream);
		stream.write(payload);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.synapsense.bacnet.APDUparser.codecs.BACnetEncodeable#decode(byte[],
	 * int)
	 */
	@Override
	protected void decode(ByteArrayInputStream stream) throws IOException {
		payload = new byte[(int) tag.getPayloadSize()];
		read(stream, payload);
	}

	@Override
	public int getEncodedSize() {
		return tag.getEncodedSize() + payload.length;
	}

	public byte[] getPayload() {
		return payload.clone();
	}
}
