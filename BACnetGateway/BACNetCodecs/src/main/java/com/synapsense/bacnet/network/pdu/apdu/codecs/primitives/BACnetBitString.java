package com.synapsense.bacnet.network.pdu.apdu.codecs.primitives;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class BACnetBitString extends BACnetTLV {
	private final static ValueType type = ValueType.BIT_STRING;

	private boolean[] payload;
	private byte[] payload_encoded;

	public BACnetBitString(Integer id, boolean[] payload) {
		super();
		this.payload = payload.clone();
		encode_payload();
		createTag(id, type, payload_encoded.length + 1);
	}

	public BACnetBitString(ByteArrayInputStream stream, Integer expected_id) throws IOException {
		super(stream, expected_id, type);
	}

	private void encode_payload() {
		int cur_byte = 0;

		if (payload.length == 0) {
			payload_encoded = new byte[0];
			return;
		}

		payload_encoded = new byte[(payload.length >> 3) + (((payload.length & 0x7) != 0) ? 1 : 0)];

		for (int bit = 0; bit < payload.length; bit++) {
			if (((bit & 0x7) == 0) && (bit > 0)) {
				payload_encoded[(bit >> 3) - 1] = (byte) cur_byte;
				cur_byte = 0;
			}
			cur_byte <<= 1;
			if (payload[bit])
				cur_byte |= 1;
		}
		if ((payload.length & 0x07) != 0)
			cur_byte <<= 8 - (payload.length & 0x07);
		payload_encoded[payload_encoded.length - 1] = (byte) cur_byte;
	}

	@Override
	protected void decode(ByteArrayInputStream stream) throws IOException {
		int octets = (int) tag.getPayloadSize() - 1;
		int unused_bits = stream.read();

		payload = new boolean[octets * 8 - unused_bits];
		if (octets == 0) {
			payload_encoded = new byte[0];
			return;
		}
		payload_encoded = new byte[octets];
		int octet = 0;
		for (int bit = 0; bit < payload.length; bit++) {
			if ((bit & 0x7) == 0) {
				octet = stream.read();
				payload_encoded[bit >> 3] = (byte) octet;
			}
			payload[bit] = ((octet & 0x80) != 0);
			octet <<= 1;
		}
	}

	@Override
	public void encode(ByteArrayOutputStream stream) throws IOException {
		tag.encode(stream);

		if ((payload.length & 0x7) != 0) {
			stream.write(8 - (payload.length & 0x7));
		} else {
			stream.write(0);
		}
		stream.write(payload_encoded);
	}

	@Override
	public int getEncodedSize() {
		return tag.getEncodedSize() + payload_encoded.length + 1;
	}

	public boolean[] getPayload() {
		return payload.clone();
	}
}
