package com.synapsense.bacnet.network.pdu.apdu.codecs.codecs.primitive;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

import com.synapsense.bacnet.network.pdu.apdu.codecs.codecs.Codec;
import com.synapsense.bacnet.network.pdu.apdu.codecs.codecs.CodecSupports;
import com.synapsense.bacnet.network.pdu.apdu.codecs.exceptions.DecodingException;
import com.synapsense.bacnet.network.pdu.apdu.codecs.exceptions.EncodingException;
import com.synapsense.bacnet.network.pdu.apdu.codecs.primitives.BACnetSequence;

@CodecSupports(payloadClass = BACnetSequence.Actions.class, codecName = "SequenceCodec")
public class SequenceCodec extends Codec {

	@Override
	protected Object decode(ByteArrayInputStream in, Integer expected_id) throws DecodingException {
		try {
			BACnetSequence a = new BACnetSequence(in, expected_id);
			return a.getPayload();
		} catch (Exception e) {
			throw new DecodingException(e);
		}
	}

	@Override
	protected void encode(ByteArrayOutputStream out, Integer id, Object payload) throws EncodingException {
		try {
			BACnetSequence a = new BACnetSequence(id, (BACnetSequence.Actions) payload);
			a.encode(out);
		} catch (Exception e) {
			throw new EncodingException(e);
		}
	}
}
