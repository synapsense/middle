package com.synapsense.bacnet.network.pdu.apdu.codecs.exceptions;

public class CodecsException extends Exception {
	public CodecsException(String reason) {
		super(reason);
	}

	public CodecsException(Throwable reason) {
		super(reason);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -8532126631521229673L;

}
