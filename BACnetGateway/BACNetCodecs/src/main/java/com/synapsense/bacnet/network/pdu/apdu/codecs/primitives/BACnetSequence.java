package com.synapsense.bacnet.network.pdu.apdu.codecs.primitives;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class BACnetSequence extends BACnetPrimitive {
	public enum Actions {
	SEQUENCE_START(0x0E), SEQUENCE_STOP(0x0F);

	private int code;

	public int getCode() {
		return code;
	}

	Actions(int code) {
		this.code = code;
	}
	}

	private Actions action;

	private BACnetT tag;
	protected final static String malformed_buffer_error = "buffer does not contain value expected";

	public BACnetSequence(int id, Actions action) {
		this.action = action;
		tag = new BACnetT(TagClass.CONTEXT_SPECIFIC, id);
	}

	public BACnetSequence(ByteArrayInputStream stream, Integer expected_id) throws IOException {
		tag = new BACnetT(stream);
		if (!tag.checkTag(expected_id, null))
			throw new IllegalArgumentException(malformed_buffer_error);
		decode(stream);
	}

	@Override
	protected void decode(ByteArrayInputStream stream) throws IOException {
		int sequence = tag.getFirstTagOctet() & 0x0F;

		if (sequence == Actions.SEQUENCE_START.getCode()) {
			action = Actions.SEQUENCE_START;
		} else if (sequence == Actions.SEQUENCE_STOP.getCode()) {
			action = Actions.SEQUENCE_STOP;
		} else
			throw new IllegalArgumentException(malformed_buffer_error);
	}

	@Override
	public void encode(ByteArrayOutputStream stream) throws IOException {
		byte[] buffer = new byte[getEncodedSize()];
		tag.encode(buffer, 0);
		buffer[0] = (byte) ((0xF0 & buffer[0]) | action.getCode());
		stream.write(buffer);
	}

	@Override
	public int getEncodedSize() {
		return tag.getEncodedSize();
	}

	public Actions getPayload() {
		return action;
	}
}
