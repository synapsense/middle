package com.synapsense.bacnet.network.pdu.apdu.codecs.primitives;

import java.io.ByteArrayInputStream;
import java.io.IOException;

abstract class BACnetUnsigned extends BACnetInteger {
	public BACnetUnsigned(Integer id, long payload, ValueType type) {
		super(id, payload, type);
	}

	public BACnetUnsigned(ByteArrayInputStream stream, Integer expected_id, ValueType type) throws IOException {
		super(stream, expected_id, type);
	}

	@Override
	protected void calc_most_sign_byte() {
		most_sign_byte = 4;
		long mask = 0xFF000000L;
		while ((most_sign_byte > 1) && ((payload & mask) == 0)) {
			most_sign_byte--;
			mask >>>= 8;
		}
	}
}