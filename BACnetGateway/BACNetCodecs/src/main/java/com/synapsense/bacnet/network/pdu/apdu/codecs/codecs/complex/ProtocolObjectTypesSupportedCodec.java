package com.synapsense.bacnet.network.pdu.apdu.codecs.codecs.complex;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import com.synapsense.bacnet.network.pdu.apdu.codecs.PrimitivesDecoder;
import com.synapsense.bacnet.network.pdu.apdu.codecs.PrimitivesEncoder;
import com.synapsense.bacnet.network.pdu.apdu.codecs.codecs.Codec;
import com.synapsense.bacnet.network.pdu.apdu.codecs.exceptions.DecodingException;
import com.synapsense.bacnet.network.pdu.apdu.codecs.exceptions.EncodingException;

public class ProtocolObjectTypesSupportedCodec extends Codec {

	@Override
	protected Object decode(ByteArrayInputStream in, Integer expected_id) throws DecodingException {
		try {
			PrimitivesDecoder decoder = new PrimitivesDecoder(in);

			// decoder.decodeSequenceStart(expected_id);

			boolean[] result = decoder.decodeBitString(null);
			Collection<Short> objectTypes = new ArrayList<Short>();

			for (int i = 0; i < result.length; i++) {
				if (result[i]) {
					objectTypes.add((short) i);
				}
			}

			// decoder.decodeSequenceStop(expected_id);
			return objectTypes.toArray();
		} catch (IOException e) {
			throw new DecodingException(e);
		}
	}

	@Override
	protected void encode(ByteArrayOutputStream out, Integer id, Object payload) throws EncodingException {
		short[] objectTypes = ((short[]) payload).clone();
		Arrays.sort(objectTypes); // TODO: change for finding maximum element
								  // only
		int size = 0;
		for (int i = 0; i < objectTypes.length; i++) {
			if (objectTypes[i] < 0)
				throw new IllegalArgumentException("payload contains illegal object type:" + objectTypes[i]);
			if (objectTypes[i] <= 127)
				size++;
		}
		short[] standardTypes = Arrays.copyOf(objectTypes, size);

		boolean[] result = new boolean[standardTypes[standardTypes.length - 1] + 1];

		for (int i = 0; i < standardTypes.length; i++) {
			result[standardTypes[i]] = true;
		}

		PrimitivesEncoder encoder = new PrimitivesEncoder();
		try {
			// encoder.encodeSequenceStart(id);
			encoder.encodeBitString(null, result);
			// encoder.encodeSequenceStop(id);
			out.write(encoder.getEncodedBuffer());
		} catch (IOException e) {
			throw new EncodingException(e);
		}
	}
}
