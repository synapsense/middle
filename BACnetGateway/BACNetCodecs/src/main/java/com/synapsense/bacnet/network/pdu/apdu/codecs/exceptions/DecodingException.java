package com.synapsense.bacnet.network.pdu.apdu.codecs.exceptions;

public class DecodingException extends CodecsException {
	public DecodingException(String reason) {
		super(reason);
	}

	public DecodingException(Throwable reason) {
		super(reason);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 5913433057977188198L;
}
