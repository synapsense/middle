package com.synapsense.bacnet.network.pdu.apdu.codecs.primitives;

import java.io.ByteArrayInputStream;
import java.io.IOException;

/**
 * UNSIGNED INT primitive The encoding of an unsigned integer value shall be
 * primitive, with at least one contents octet. Unsigned integers shall be
 * encoded in the contents octet(s) as binary numbers in the range 0 to (28*L -
 * 1) where L is the number of octets used to encode the value and L is at least
 * one. Values encoded into more than one octet shall be conveyed with the most
 * significant octet first. All unsigned integers shall be encoded in the
 * smallest number of octets possible. That is, the first octet of any
 * multi-octet encoded value shall not be X'00'. (20.2.4)
 * 
 * @author anechaev
 * 
 */
public class BACnetUInt extends BACnetUnsigned {
	private final static ValueType type = ValueType.UINT;

	public BACnetUInt(Integer id, long payload) {
		super(id, payload, type);
	}

	public BACnetUInt(ByteArrayInputStream stream, Integer expected_id) throws IOException {
		super(stream, expected_id, type);
	}

	public long getPayload() {
		return payload;
	}
}
