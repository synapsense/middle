package com.synapsense.bacnet.network.pdu.apdu.codecs.exceptions;

public class NotResolvedPayloadException extends Exception {

	public NotResolvedPayloadException(String reason) {
		super(reason);
	}

	public NotResolvedPayloadException(Throwable reason) {
		super(reason);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -8352929554540447081L;

}
