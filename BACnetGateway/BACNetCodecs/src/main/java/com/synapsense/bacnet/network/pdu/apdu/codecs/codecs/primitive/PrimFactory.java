package com.synapsense.bacnet.network.pdu.apdu.codecs.codecs.primitive;

import java.util.HashMap;

import com.synapsense.bacnet.network.pdu.apdu.codecs.codecs.Codec;
import com.synapsense.bacnet.network.pdu.apdu.codecs.codecs.CodecsAF;
import com.synapsense.bacnet.network.pdu.apdu.codecs.exceptions.CannotRegisterCodecException;
import com.synapsense.bacnet.network.pdu.apdu.codecs.exceptions.NoSuitableCodecException;

public class PrimFactory extends CodecsAF {
	private HashMap<Class<?>, Codec> codecsByTypes;
	private HashMap<String, Codec> codecsByNames;
	private final static String PRIM_PACKAGE_NAME = "com.synapsense.bacnet.network.pdu.apdu.codecs.codecs.primitive.";
	private final static String COMPLEX_PACKAGE_NAME = "com.synapsense.bacnet.network.pdu.apdu.codecs.codecs.complex.";

	public PrimFactory() {
		codecsByTypes = new HashMap<Class<?>, Codec>();
		codecsByNames = new HashMap<String, Codec>();
		try {
			registerCodec(OctetStringCodec.class);
			registerCodec(UIntCodec.class);
			registerCodec(BooleanCodec.class);
			registerCodec(CharacterStringCodec.class);
			registerCodec(DoubleCodec.class);
			registerCodec(ObjectIDCodec.class);
			registerCodec(RealCodec.class);
			registerCodec(SequenceCodec.class);
			registerCodec(SIntCodec.class);
			registerCodec(DateCodec.class);
			registerCodec(TimeCodec.class);
			registerCodec(EnumCodec.class);
			registerCodec(BitStringCodec.class);
			registerCodec(NullCodec.class);

			registerCodec("OctetStringCodec", PRIM_PACKAGE_NAME + "OctetStringCodec");
			registerCodec("UIntCodec", PRIM_PACKAGE_NAME + "UIntCodec");
			registerCodec("BooleanCodec", PRIM_PACKAGE_NAME + "BooleanCodec");
			registerCodec("CharacterStringCodec", PRIM_PACKAGE_NAME + "CharacterStringCodec");
			registerCodec("DoubleCodec", PRIM_PACKAGE_NAME + "DoubleCodec");
			registerCodec("ObjectIDCodec", PRIM_PACKAGE_NAME + "ObjectIDCodec");
			registerCodec("RealCodec", PRIM_PACKAGE_NAME + "RealCodec");
			registerCodec("SequenceCodec", PRIM_PACKAGE_NAME + "SequenceCodec");
			registerCodec("SIntCodec", PRIM_PACKAGE_NAME + "SIntCodec");
			registerCodec("DateCodec", PRIM_PACKAGE_NAME + "DateCodec");
			registerCodec("TimeCodec", PRIM_PACKAGE_NAME + "TimeCodec");
			registerCodec("EnumCodec", PRIM_PACKAGE_NAME + "EnumCodec");
			registerCodec("BitStringCodec", PRIM_PACKAGE_NAME + "BitStringCodec");
			registerCodec("NullCodec", PRIM_PACKAGE_NAME + "NullCodec");

			registerCodec("ObjectListCodec", COMPLEX_PACKAGE_NAME + "ObjectListCodec");
			registerCodec("ProtocolObjectTypesSupportedCodec", COMPLEX_PACKAGE_NAME
			        + "ProtocolObjectTypesSupportedCodec");
			registerCodec("ProtocolServicesSupportedCodec", COMPLEX_PACKAGE_NAME + "ProtocolServicesSupportedCodec");
			registerCodec("ActiveCOVSubscriptionsCodec", COMPLEX_PACKAGE_NAME + "ActiveCOVSubscriptionsCodec");
			registerCodec("DeviceAddressBindingCodec", COMPLEX_PACKAGE_NAME + "DeviceAddressBindingCodec");

		} catch (CannotRegisterCodecException e) {
			e.printStackTrace();
		}
	}

	@Override
	public Codec getCodec(Class<?> payload) throws NoSuitableCodecException {
		Codec codec = codecsByTypes.get(payload);
		if (codec == null)
			throw new NoSuitableCodecException(payload.getCanonicalName());

		return codec;
	}

	@Override
	public void registerCodec(Class<? extends Codec> codec) throws CannotRegisterCodecException {
		try {
			registerCodec(extractPayloadClass(codec), codec.newInstance());
		} catch (InstantiationException e) {
			throw new CannotRegisterCodecException(e);
		} catch (IllegalAccessException e) {
			throw new CannotRegisterCodecException(e);
		}
	}

	private void registerCodec(Class<?> payload, Codec codec) throws CannotRegisterCodecException {
		if (codecsByTypes.put(payload, codec) != null)
			throw new CannotRegisterCodecException("Codec " + codec.getClass().getCanonicalName()
			        + " has been already registered");
	}

	@Override
	public Codec getCodec(String codecName) throws NoSuitableCodecException {
		Codec codec = codecsByNames.get(codecName);
		if (codec == null)
			throw new NoSuitableCodecException(codecName);

		return codec;
	}

	@Override
	public void registerCodec(String codecName, String codecClassName) throws CannotRegisterCodecException {
		try {
			Codec codec = (Codec) Class.forName(codecClassName).newInstance();
			if (codecsByNames.put(codecName, codec) != null)
				throw new CannotRegisterCodecException("Codec named " + codecName + " has been already registered");
		} catch (Exception e) {
			throw new CannotRegisterCodecException(e);
		}
	}
}
