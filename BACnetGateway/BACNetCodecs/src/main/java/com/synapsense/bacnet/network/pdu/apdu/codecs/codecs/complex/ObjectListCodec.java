package com.synapsense.bacnet.network.pdu.apdu.codecs.codecs.complex;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import com.synapsense.bacnet.network.pdu.apdu.codecs.PrimitivesDecoder;
import com.synapsense.bacnet.network.pdu.apdu.codecs.PrimitivesEncoder;
import com.synapsense.bacnet.network.pdu.apdu.codecs.codecs.Codec;
import com.synapsense.bacnet.network.pdu.apdu.codecs.exceptions.DecodingException;
import com.synapsense.bacnet.network.pdu.apdu.codecs.exceptions.EncodingException;
import com.synapsense.bacnet.system.shared.BACnetObjectIdentifier;

public class ObjectListCodec extends Codec {

	@Override
	protected Object decode(ByteArrayInputStream in, Integer expected_id) throws DecodingException {
		try {
			PrimitivesDecoder decoder = new PrimitivesDecoder(in);
			// decoder.decodeSequenceStart(expected_id);

			Collection<BACnetObjectIdentifier> objectList = new ArrayList<BACnetObjectIdentifier>();
			while (!decoder.askForSequenceStop(expected_id)) {
				objectList.add(decoder.decodeObjectID(null));
			}
			// decoder.decodeSequenceStop(expected_id);

			return objectList.toArray();
		} catch (IOException e) {
			throw new DecodingException(e);
		}
	}

	@Override
	protected void encode(ByteArrayOutputStream out, Integer id, Object payload) throws EncodingException {
		BACnetObjectIdentifier[] objectList = (BACnetObjectIdentifier[]) payload;
		PrimitivesEncoder encoder = new PrimitivesEncoder();

		try {
			// encoder.encodeSequenceStart(id);
			for (BACnetObjectIdentifier it : objectList) {
				encoder.encodeObjectID(null, it);
			}
			// encoder.encodeSequenceStop(id);
			out.write(encoder.getEncodedBuffer());
		} catch (IOException e) {
			throw new EncodingException(e);
		}
	}
}
