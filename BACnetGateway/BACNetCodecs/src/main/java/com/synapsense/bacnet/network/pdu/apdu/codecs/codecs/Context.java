package com.synapsense.bacnet.network.pdu.apdu.codecs.codecs;

public interface Context {
	public CodecsAF getCodecsFactory();
}
