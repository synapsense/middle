package com.synapsense.bacnet.network.pdu.apdu.codecs.primitives;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * BOOLEAN primitive Application-tagged Boolean values shall be encoded within a
 * single octet by setting the length/value/type field to B'000' if the value to
 * be encoded is FALSE or B'001' if the value to be encoded is TRUE.
 * Context-tagged Boolean primitive data shall contain one contents octet. The
 * value of this octet shall be B'00000000' if the value to be encoded is FALSE
 * or B'00000001' if the value to be encoded is TRUE. (20.2.3)
 * 
 * @author anechaev
 * 
 */
public class BACnetBoolean extends BACnetTLV {
	private final static ValueType type = ValueType.BOOLEAN;

	private boolean payload;

	/**
	 * Creates a new BOOLEAN primitive
	 * 
	 * @param id
	 *            - null for APPLICATION class tags, not null tag id for CONTEXT
	 *            SPECIFIC tags
	 * @param payload
	 *            - boolean
	 */
	public BACnetBoolean(Integer id, boolean payload) {
		if (id == null) {
			createTag(id, type, (payload) ? 1 : 0);
		} else {
			createTag(id, type, 1);
		}
		this.payload = payload;
	}

	public BACnetBoolean(ByteArrayInputStream stream, Integer expected_id) throws IOException {
		super(stream, expected_id, type);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.synapsense.bacnet.APDUparser.codecs.BACnetEncodeable#encode(byte[],
	 * int)
	 */
	@Override
	public void encode(ByteArrayOutputStream stream) throws IOException {
		tag.encode(stream);

		if (tag.getTagClass() == TagClass.CONTEXT_SPECIFIC) {
			stream.write((payload ? 1 : 0));
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.synapsense.bacnet.APDUparser.codecs.BACnetEncodeable#decode(byte[],
	 * int)
	 */
	@Override
	protected void decode(ByteArrayInputStream stream) throws IOException {
		if (tag.getTagClass() == TagClass.APPLICATION) {
			switch ((int) tag.getPayloadSize()) {
			case 0:
				payload = false;
				break;
			case 1:
				payload = true;
				break;
			default:
				throw new IllegalArgumentException(malformed_buffer_error);
			}
		} else {
			if (tag.getPayloadSize() != 1)
				throw new IllegalArgumentException(malformed_buffer_error);
			int value = read(stream);
			switch (value) {
			case 0:
				payload = false;
				break;
			case 1:
				payload = true;
				break;
			default:
				throw new IllegalArgumentException(malformed_buffer_error);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.synapsense.bacnet.APDUparser.codecs.BACnetEncodeable#getEncodedSize()
	 */
	@Override
	public int getEncodedSize() {
		int size = tag.getEncodedSize();
		if (tag.getTagClass() == TagClass.CONTEXT_SPECIFIC)
			size++;
		return size;
	}

	public boolean getPayload() {
		return payload;
	}
}
