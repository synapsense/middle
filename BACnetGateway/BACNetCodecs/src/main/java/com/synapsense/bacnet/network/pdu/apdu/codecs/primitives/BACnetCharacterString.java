package com.synapsense.bacnet.network.pdu.apdu.codecs.primitives;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;

/**
 * CHARACTER STRING primitive The encoding of a character string value shall be
 * primitive. The encoding shall contain an initial contents octet, and zero,
 * one, or more additional contents octets equal in value to the octets in the
 * data value, in the order in which they appear in the data value, i.e., most
 * significant octet first, and with the most significant bit of an octet of the
 * data value aligned with the most significant bit of an octet of the contents
 * octets. The initial octet shall specify the character set with the following
 * encoding: X'00' ANSI X3.4 X'01' IBM™/Microsoft™ DBCS X'02' JIS C 6226
 * X'03' ISO 10646 (UCS-4) X'04' ISO 10646 (UCS-2) X'05' ISO 8859-1 Other values
 * of the initial octet are reserved by ASHRAE.
 * 
 * In the case of IBM/Microsoft DBCS (X'01'), the initial octet shall be
 * followed by two additional octets whose value shall represent an unsigned
 * integer, with the most significant octet first, that shall indicate the Code
 * Page to be presumed for the characters that follow.
 * 
 * In the case of ISO 10646 UCS-2 (X'04') and UCS4 (X'03'), each character of
 * the string shall be represented by two or four octets, respectively. The
 * octet order for UCS-2 shall be Row-Cell. The octet order for UCS-4 shall be
 * Group-Plane-Row-Cell.
 * 
 * @author anechaev
 * 
 */
public class BACnetCharacterString extends BACnetTLV {
	private final static ValueType type = ValueType.CHARACTER_STRING;

	// private final static String SUPPORTED_CHARSET = "ISO-8859-1";
	// private final static int SUPPORTED_CHARSET_BACNET_ID = 5; //ISO-8859-1
	private final static String SUPPORTED_CHARSET = "ISO-8859-1";
	private final static int SUPPORTED_CHARSET_BACNET_ID = 0; // ISO-8859-1
	private String payload;
	private byte[] payload_encoded;

	public BACnetCharacterString(Integer id, String payload) {
		super();
		payload = (payload == null ? "" : payload);
		this.payload = payload;
		payload_encoded = payload.getBytes(Charset.forName(SUPPORTED_CHARSET));
		// TODO Check for zero-length strings!!! must they be encoded in payload
		// or not

		createTag(id, type, payload_encoded.length + 1); // add additional
		                                                 // encoding type id

	}

	public BACnetCharacterString(ByteArrayInputStream stream, Integer expected_id) throws IOException {
		super(stream, expected_id, type);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.synapsense.bacnet.APDUparser.codecs.BACnetEncodeable#decode(byte[],
	 * int)
	 */
	@Override
	protected void decode(ByteArrayInputStream stream) throws IOException {
		if (tag.getPayloadSize() == 0) {
			payload = new String("");
			payload_encoded = new byte[0];
			return;
		}
		if (read(stream) != SUPPORTED_CHARSET_BACNET_ID)
			throw new IllegalArgumentException("String encoding format is not supported");

		payload_encoded = new byte[(int) tag.getPayloadSize() - 1]; // -
		                                                            // encoding
		                                                            // type

		if (payload_encoded.length == 0) {
			payload = "";
		} else {
			read(stream, payload_encoded);
			// payload = new String(payload_encoded,
			// Charset.forName(SUPPORTED_CHARSET));
			payload = new String(payload_encoded);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.synapsense.bacnet.APDUparser.codecs.BACnetEncodeable#encode(byte[],
	 * int)
	 */
	@Override
	public void encode(ByteArrayOutputStream stream) throws IOException {
		tag.encode(stream);
		stream.write(SUPPORTED_CHARSET_BACNET_ID);
		if (payload_encoded.length != 0) {
			stream.write(payload_encoded);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.synapsense.bacnet.APDUparser.codecs.BACnetEncodeable#getEncodedSize()
	 */
	@Override
	public int getEncodedSize() {
		return (int) (tag.getEncodedSize() + tag.getPayloadSize());
	}

	public String getPayload() {
		return payload;
	}
}
