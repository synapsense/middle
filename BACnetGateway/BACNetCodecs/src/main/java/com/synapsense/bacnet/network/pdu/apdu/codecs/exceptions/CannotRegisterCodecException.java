package com.synapsense.bacnet.network.pdu.apdu.codecs.exceptions;

public class CannotRegisterCodecException extends CodecsException {
	public CannotRegisterCodecException(String reason) {
		super(reason);
	}

	public CannotRegisterCodecException(Throwable reason) {
		super(reason);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -3772760587358564418L;
}
