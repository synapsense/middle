package com.synapsense.bacnet.network.pdu.apdu.codecs.primitives;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.GregorianCalendar;

import com.synapsense.bacnet.system.shared.BACnetDateValue;

/**
 * The encoding of a date value shall be primitive, with four contents octets.
 * Date values shall be encoded in the contents octets as four binary integers.
 * The first contents octet shall represent the year minus 1900; the second
 * octet shall represent the month, with January = 1; the third octet shall
 * represent the day of the month; and the fourth octet shall represent the day
 * of the week, with Monday = 1. A value of X'FF' = D'255' in any of the four
 * octets shall indicate that the corresponding value is unspecified. If all
 * four octets = X'FF', the corresponding date may be interpreted as "any" or
 * "don't care."
 * 
 * @author anechaev
 * 
 */
public class BACnetDate extends BACnetInteger {
	private final static ValueType type = ValueType.DATE;

	private final static int ODD_MONTH_ENCODE = 13;
	private final static int EVEN_MONTH_ENCODE = 14;
	private final static int LAST_DAY_OF_MONTH_ENCODE = 32;
	private final static int UNSPECIFIED_ENCODE = 0xFF;

	private BACnetDateValue date;

	public BACnetDate(Integer id, BACnetDateValue payload) {
		date = (BACnetDateValue) payload.clone();
		encode_payload();
		InitializeContent(id, this.payload, type);
	}

	public BACnetDate(ByteArrayInputStream stream, Integer expected_id) throws IOException {
		super(stream, expected_id, type);
		decode_payload();
	}

	@Override
	protected void calc_most_sign_byte() {
		most_sign_byte = 4;
	}

	public BACnetDateValue getPayload() {
		return date;
	}

	private void encode_payload() {
		payload = 0;
		GregorianCalendar value = date.getDate();

		if (value.isSet(Calendar.YEAR)) {
			payload |= 0xFF & (value.get(Calendar.YEAR) - 1900);
		} else {
			payload = UNSPECIFIED_ENCODE;
		}
		payload <<= 8;

		switch (date.getMonthExtender()) {
		case SIMPLE:
			payload |= 0xFF & (value.get(Calendar.MONTH) + 1);
			break;
		case EVEN:
			payload |= EVEN_MONTH_ENCODE;
			break;
		case ODD:
			payload |= ODD_MONTH_ENCODE;
			break;
		case UNSPECIFIED:
			payload |= UNSPECIFIED_ENCODE;
			break;
		}
		payload <<= 8;

		switch (date.getDayOfMonthExtender()) {
		case SIMPLE:
			payload |= 0xFF & value.get(Calendar.DAY_OF_MONTH);
			break;
		case LAST_DAY:
			payload |= LAST_DAY_OF_MONTH_ENCODE;
			break;
		case UNSPECIFIED:
			payload |= UNSPECIFIED_ENCODE;
		}
		payload <<= 8;

		if (value.isSet(Calendar.DAY_OF_WEEK)) {
			switch (value.get(Calendar.DAY_OF_WEEK)) {
			case Calendar.MONDAY:
				payload |= 1;
				break;
			case Calendar.TUESDAY:
				payload |= 2;
				break;
			case Calendar.WEDNESDAY:
				payload |= 3;
				break;
			case Calendar.THURSDAY:
				payload |= 4;
				break;
			case Calendar.FRIDAY:
				payload |= 5;
				break;
			case Calendar.SATURDAY:
				payload |= 6;
				break;
			case Calendar.SUNDAY:
				payload |= 7;
				break;
			}
		} else {
			payload |= UNSPECIFIED_ENCODE;
		}
	}

	private void decode_payload() {
		int cur_payload;
		BACnetDateValue.MonthExtender monthExtender;
		BACnetDateValue.DayOfMonthExtender dayOfMonthExtender;

		GregorianCalendar value = new GregorianCalendar();
		value.clear();

		cur_payload = (int) (payload & 0xFF000000) >> 24;
		switch (cur_payload) {
		case UNSPECIFIED_ENCODE:
			break;
		default:
			value.set(Calendar.YEAR, cur_payload + 1900);
			break;
		}

		cur_payload = (int) (payload & 0xFF0000) >> 16;
		switch (cur_payload) {
		case ODD_MONTH_ENCODE:
			monthExtender = BACnetDateValue.MonthExtender.ODD;
			break;
		case EVEN_MONTH_ENCODE:
			monthExtender = BACnetDateValue.MonthExtender.EVEN;
			break;
		case UNSPECIFIED_ENCODE:
			monthExtender = BACnetDateValue.MonthExtender.UNSPECIFIED;
			break;
		default:
			monthExtender = BACnetDateValue.MonthExtender.SIMPLE;
			value.set(Calendar.MONTH, cur_payload);
		}

		cur_payload = (int) (payload & 0xFF00) >> 8;
		switch (cur_payload) {
		case LAST_DAY_OF_MONTH_ENCODE:
			dayOfMonthExtender = BACnetDateValue.DayOfMonthExtender.LAST_DAY;
			break;
		case UNSPECIFIED_ENCODE:
			dayOfMonthExtender = BACnetDateValue.DayOfMonthExtender.UNSPECIFIED;
			break;
		default:
			dayOfMonthExtender = BACnetDateValue.DayOfMonthExtender.SIMPLE;
			value.set(Calendar.DAY_OF_MONTH, cur_payload);
			break;
		}

		switch ((int) (payload & 0xFF)) {
		case 1:
			value.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
			break;
		case 2:
			value.set(Calendar.DAY_OF_WEEK, Calendar.TUESDAY);
			break;
		case 3:
			value.set(Calendar.DAY_OF_WEEK, Calendar.WEDNESDAY);
			break;
		case 4:
			value.set(Calendar.DAY_OF_WEEK, Calendar.THURSDAY);
			break;
		case 5:
			value.set(Calendar.DAY_OF_WEEK, Calendar.FRIDAY);
			break;
		case 6:
			value.set(Calendar.DAY_OF_WEEK, Calendar.SATURDAY);
			break;
		case 7:
			value.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
			break;
		case UNSPECIFIED_ENCODE:
			break;
		default:
			throw new IllegalArgumentException(malformed_buffer_error);
		}

		date = new BACnetDateValue(value, monthExtender, dayOfMonthExtender);
	}
}
