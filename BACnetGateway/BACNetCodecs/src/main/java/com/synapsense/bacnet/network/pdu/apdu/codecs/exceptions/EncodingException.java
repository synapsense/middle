package com.synapsense.bacnet.network.pdu.apdu.codecs.exceptions;

public class EncodingException extends CodecsException {

	public EncodingException(String reason) {
		super(reason);
	}

	public EncodingException(Throwable reason) {
		super(reason);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -7700632953771371699L;

}
