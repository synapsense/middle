package com.synapsense.bacnet.network.pdu.apdu.codecs.primitives;

public enum TagClass {
APPLICATION, CONTEXT_SPECIFIC
}
