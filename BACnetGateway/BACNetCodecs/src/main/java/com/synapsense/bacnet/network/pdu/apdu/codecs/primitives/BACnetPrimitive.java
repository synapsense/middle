package com.synapsense.bacnet.network.pdu.apdu.codecs.primitives;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * Parent interface for every BACnet's APDU primitive It is not a interface
 * because we must implement PROTECTED method
 * 
 * @author anechaev
 * 
 */
public abstract class BACnetPrimitive {
	/**
	 * @return encoded size of a primitive in octets
	 */
	public abstract int getEncodedSize();

	/**
	 * encodes primitive to destination buffer. throws
	 * ArrayIndexOutOfBoundException or...
	 * 
	 * @param stream
	 *            - byte stream to serialize to
	 */
	public abstract void encode(ByteArrayOutputStream stream) throws IOException;

	/**
	 * decodes primitive from source stream. throws
	 * ArrayIndexOutOfBoundException or...
	 * 
	 * @param stream
	 *            - source stream to deserialize from
	 */
	protected abstract void decode(ByteArrayInputStream stream) throws IOException;

	protected int read(ByteArrayInputStream stream) throws IOException {
		int a = stream.read();
		if (a == -1)
			throw new IOException("Attempt to read after the end of input stream");
		return 0xFF & a;
	}

	protected void read(ByteArrayInputStream stream, byte[] result) throws IOException {
		if (stream.read(result) != result.length)
			throw new IOException("Attempt to read after the end of input stream");
	}
}
