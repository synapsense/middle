package com.synapsense.bacnet.network.pdu.apdu.codecs.primitives;

import java.io.ByteArrayInputStream;
import java.io.IOException;

/**
 * The encoding of a double precision real number value shall be primitive with
 * eight contents octets. Double precision real numbers shall be encoded using
 * the method specified in ANSI/IEEE Standard 754-1985, "IEEE Standard for
 * Binary Floating-Point Arithmetic." This standard should be consulted for
 * details. The multi-octet value shall be conveyed most significant (sign and
 * exponent) octet first. For the case of double precision real numbers, the
 * encoding format is: Bit Number: 63 62 ... 52 51 ... 0
 * |---|---|...|...|---|---|...|...|---| | s | e | f |
 * |---|---|...|...|---|---|...|...|---| Field Width: 1 <-----11 -----><----- 52
 * -----> where the numbers indicate the field widths in bits. Non-zero values
 * shall be represented by the equation v = (-1)s2e-1023(1*f) where the symbol
 * "*" signifies the binary point. Zero shall be indicated by setting s, e, and
 * f to zero.
 * 
 * @author anechaev
 * 
 */
public class BACnetDouble extends BACnetInteger {
	private final static ValueType type = ValueType.DOUBLE;

	public BACnetDouble(Integer id, double payload) {
		super(id, Double.doubleToLongBits(payload), type);
	}

	public BACnetDouble(ByteArrayInputStream stream, Integer expected_id) throws IOException {
		super(stream, expected_id, type);
	}

	@Override
	protected void calc_most_sign_byte() {
		most_sign_byte = 8;
	}

	public double getPayload() {
		return Double.longBitsToDouble(payload);
	}
}
