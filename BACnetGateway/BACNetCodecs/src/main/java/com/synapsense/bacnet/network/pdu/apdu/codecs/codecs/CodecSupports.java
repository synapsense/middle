package com.synapsense.bacnet.network.pdu.apdu.codecs.codecs;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface CodecSupports {
	public final static String DEFAULT_CODEC_NAME = "";

	Class<?> payloadClass();

	String codecName() default DEFAULT_CODEC_NAME;
}
