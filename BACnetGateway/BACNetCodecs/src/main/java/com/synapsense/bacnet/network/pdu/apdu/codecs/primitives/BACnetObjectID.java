package com.synapsense.bacnet.network.pdu.apdu.codecs.primitives;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import com.synapsense.bacnet.system.shared.BACnetObjectIdentifier;

/**
 * A BACnet Object Identifier value shall consist of two components: (1) A
 * 10-bit object type, representing the BACnetObjectType of the object, with bit
 * 9 the most significant bit and bit 0 the least significant. For objects
 * defined in this standard, the value for this field shall be determined by the
 * BACnetObjectType enumeration in Clause 21. (2) A 22-bit object instance
 * number, with bit 21 the most significant bit and bit 0 the least significant.
 * Bit Number: 31 ... 22 21 ... 0 |---|---|---|---|---|...|---|---| | Obj Type |
 * Instance Number | |---|---|---|---|---|...|---|---| Field Width: <----- 10
 * -----> <----- 22 -----> The encoding of an object identifier value shall be
 * primitive, with four contents octets as follows: Bits 9 through 2 of the
 * object type shall be encoded in bits 7 through 0 of the first contents octet.
 * Bits 1 through 0 of the object type shall be encoded in bits 7 through 6 of
 * the second contents octet. Bits 21 through 16 of the object instance shall be
 * encoded in bits 5 through 0 of the second contents octet. Bits 15 through 8
 * of the object instance shall be encoded in bits 7 through 0 of the third
 * contents octet. Bits 7 through 0 of the object instance shall be encoded in
 * bits 7 through 0 of the fourth contents octet.
 * 
 * @author anechaev
 * 
 */
public class BACnetObjectID extends BACnetInteger {
	private final static ValueType type = ValueType.BACNET_OID;

	public BACnetObjectID(Integer id, BACnetObjectIdentifier payload) {
		super(id, (payload.getObjectType() << 22) | payload.getInstanceNumber(), type);
	}

	public BACnetObjectID(Integer id, short objType, int instNumber) {
		super(id, (objType << 22) | instNumber, type);
	}

	public BACnetObjectID(ByteArrayInputStream stream, Integer expected_id) throws IOException {
		super(stream, expected_id, type);
	}

	@Override
	protected void calc_most_sign_byte() {
		most_sign_byte = 4;
	}

	public BACnetObjectIdentifier getPayload() {
		return new BACnetObjectIdentifier((int) payload);
	}
}
