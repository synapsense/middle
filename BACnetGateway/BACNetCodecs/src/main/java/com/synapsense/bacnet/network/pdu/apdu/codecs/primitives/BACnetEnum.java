package com.synapsense.bacnet.network.pdu.apdu.codecs.primitives;

import java.io.ByteArrayInputStream;
import java.io.IOException;

/**
 * The encoding of an enumerated value shall be primitive, with at least one
 * contents octet. Enumerated values shall be encoded in the contents octet(s)
 * as binary numbers in the range 0 to (28*L - 1) where L is the number of
 * octets used to encode the value and L is at least one. Values encoded into
 * more than one octet shall be conveyed most significant octet first. All
 * enumerated values shall be encoded in the smallest number of octets possible.
 * That is, the first octet of any multi-octet encoded value shall not be X'00'.
 * 
 * @author anechaev
 * 
 */
public class BACnetEnum extends BACnetUnsigned {
	private final static ValueType type = ValueType.ENUM;

	public BACnetEnum(Integer id, long payload) {
		super(id, payload, type);
	}

	public BACnetEnum(ByteArrayInputStream stream, Integer expected_id) throws IOException {
		super(stream, expected_id, type);
	}

	public long getPayload() {
		return payload;
	}
}
