package com.synapsense.bacnet.network.pdu.apdu.codecs;

import java.io.IOException;
import com.synapsense.bacnet.network.pdu.apdu.codecs.codecs.PrimDecodeContext;
import com.synapsense.bacnet.network.pdu.apdu.codecs.codecs.PrimEncodeContext;
import com.synapsense.bacnet.network.pdu.apdu.codecs.codecs.complex.ObjectListCodec;
import com.synapsense.bacnet.network.pdu.apdu.codecs.exceptions.CodecsException;
import com.synapsense.bacnet.network.pdu.apdu.codecs.exceptions.DecodingException;
import com.synapsense.bacnet.network.pdu.apdu.codecs.exceptions.EncodingException;
import com.synapsense.bacnet.system.shared.BACnetObjectIdentifier;
import com.synapsense.bacnet.system.shared.BACnetObjectType;

public class PrimitivesExample {

	/**
	 * @param args
	 */

	public static void main(String[] args) {
		BACnetObjectIdentifier list[] = new BACnetObjectIdentifier[3];
		list[0] = new BACnetObjectIdentifier(BACnetObjectType.DEVICE, 1);
		list[1] = new BACnetObjectIdentifier(BACnetObjectType.ANALOG_INPUT, 2);
		list[2] = new BACnetObjectIdentifier(BACnetObjectType.ANALOG_INPUT, 3);
		ObjectListCodec codec = new ObjectListCodec();
		byte[] result = null;
		try {
			result = codec.encode(3, list);

		} catch (EncodingException e) {
			e.printStackTrace();
		}

		try {
			Object a = codec.decode(result, 3);
		} catch (DecodingException e) {
			e.printStackTrace();
		}

		System.out.println("started 1...");
		long started = System.currentTimeMillis();
		try {
			for (int i = 0; i < 100000; i++) {
				PrimitivesEncoder encoder = new PrimitivesEncoder();
				encoder.encodeUInt(0, 1);
				encoder.encodeUInt(1, 2);
				encoder.encodeUInt(2, 3);
				PrimitivesDecoder decoder = new PrimitivesDecoder(encoder.getEncodedBuffer());
				decoder.decodeUInt(0);
				decoder.decodeUInt(1);
				decoder.decodeUInt(2);
			}
		} catch (IOException e) {
		}
		long stopped = System.currentTimeMillis();
		System.out.println("stopped 1...");
		System.out.println("It took " + (stopped - started) + " millis to compute");

		System.out.println("started 2...");
		started = System.currentTimeMillis();
		for (int i = 0; i < 100000; i++) {
			PrimEncodeContext ctx = new PrimEncodeContext();
			try {
				ctx.encode(0, new Long(1));
				ctx.encode(1, new Long(2));
				ctx.encode(2, new Long(3));
				PrimDecodeContext ctxd = new PrimDecodeContext(ctx.getContextPayload());
				ctxd.decode(0, Long.class);
				ctxd.decode(1, Long.class);
				ctxd.decode(2, Long.class);
			} catch (CodecsException e) {
				System.out.println(e.toString());
			}
		}
		stopped = System.currentTimeMillis();
		System.out.println("stopped 2...");
		System.out.println("It took " + (stopped - started) + " millis to compute");
		System.exit(0);
	}
}
