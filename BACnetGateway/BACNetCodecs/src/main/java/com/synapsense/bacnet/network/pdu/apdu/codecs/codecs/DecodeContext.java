package com.synapsense.bacnet.network.pdu.apdu.codecs.codecs;

import java.io.ByteArrayInputStream;
import com.synapsense.bacnet.network.pdu.apdu.codecs.exceptions.DecodingException;
import com.synapsense.bacnet.network.pdu.apdu.codecs.exceptions.NoSuitableCodecException;

public abstract class DecodeContext implements Context {
	private ByteArrayInputStream stream;

	public DecodeContext(byte[] buffer) {
		stream = new ByteArrayInputStream(buffer);
	}

	public Object decode(Integer expected_id, Class<?> payload_type) throws NoSuitableCodecException, DecodingException {
		return getCodecsFactory().getCodec(payload_type).decode(this, expected_id);
	}

	public Object decode(Integer expected_id, String codecName) throws NoSuitableCodecException, DecodingException {
		return getCodecsFactory().getCodec(codecName).decode(this, expected_id);
	}

	public Object tryDecode(Integer expected_id, Class<?> payload_type) throws NoSuitableCodecException {
		return tryDecode(expected_id, getCodecsFactory().getCodec(payload_type));
	}

	public Object tryDecode(Integer expected_id, String codecName) throws NoSuitableCodecException {
		return tryDecode(expected_id, getCodecsFactory().getCodec(codecName));
	}

	private Object tryDecode(Integer expected_id, Codec codec) {
		Object result = null;
		stream.mark(1);
		try {
			result = codec.decode(this, expected_id);
		} catch (Exception e) {
			stream.reset();
		}
		return result;
	}

	public ByteArrayInputStream getStream() {
		return stream;
	}
}
