package com.synapsense.bacnet.network.pdu.apdu.codecs.codecs;

import java.io.ByteArrayOutputStream;

import com.synapsense.bacnet.network.pdu.apdu.codecs.exceptions.EncodingException;
import com.synapsense.bacnet.network.pdu.apdu.codecs.exceptions.NoSuitableCodecException;

public abstract class EncodeContext implements Context {
	private ByteArrayOutputStream stream;

	public EncodeContext() {
		stream = new ByteArrayOutputStream();
	}

	public byte[] getContextPayload() {
		return stream.toByteArray();
	}

	public void encode(Integer id, Object payload) throws NoSuitableCodecException, EncodingException {
		Codec a = getCodecsFactory().getCodec(payload.getClass());
		a.encode(this, id, payload);
	}

	public void encode(Integer id, Object payload, String codecName) throws NoSuitableCodecException, EncodingException {
		getCodecsFactory().getCodec(codecName).encode(this, id, payload);
	}

	public ByteArrayOutputStream getStream() {
		return stream;
	}
}
