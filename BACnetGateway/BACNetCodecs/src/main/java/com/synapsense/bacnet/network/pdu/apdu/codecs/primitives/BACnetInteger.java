package com.synapsense.bacnet.network.pdu.apdu.codecs.primitives;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

abstract class BACnetInteger extends BACnetTLV {
	protected long payload;
	protected int most_sign_byte;

	protected BACnetInteger(Integer id, long payload, ValueType type) {
		super();
		InitializeContent(id, payload, type);
	}

	/**
	 * This constructor is used in child's constructor with direct call to
	 * delayed initialization procedure InitializeContent() followed. Do not
	 * forget to call this function in child class constructor, which uses
	 * super().
	 */
	protected BACnetInteger() {
		super();
	}

	protected void InitializeContent(Integer id, long payload, ValueType type) {
		this.payload = payload;
		calc_most_sign_byte();
		createTag(id, type, most_sign_byte);
	}

	protected BACnetInteger(ByteArrayInputStream stream, Integer expected_id, ValueType type) throws IOException {
		super(stream, expected_id, type);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.synapsense.bacnet.APDUparser.codecs.BACnetEncodeable#encode(byte[],
	 * int)
	 */
	@Override
	public void encode(ByteArrayOutputStream stream) throws IOException {
		tag.encode(stream);
		long mask = 0xFF << 8 * (most_sign_byte - 1);
		for (int i = (most_sign_byte - 1); i >= 0; i--) {
			stream.write((int) ((payload & mask) >> 8 * i));
			mask >>= 8;
		}
	}

	@Override
	public int getEncodedSize() {
		return tag.getEncodedSize() + (int) tag.getPayloadSize();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.synapsense.bacnet.APDUparser.codecs.BACnetEncodeable#decode(byte[],
	 * int)
	 */
	@Override
	protected void decode(ByteArrayInputStream stream) throws IOException {
		most_sign_byte = (int) tag.getPayloadSize();
		payload = 0;
		for (int i = 0; i < most_sign_byte; i++) {
			payload <<= 8;
			payload |= read(stream);
		}
	}

	protected abstract void calc_most_sign_byte();
}
