package com.synapsense.bacnet.network.pdu.apdu.codecs.primitives;

public enum ValueType {
NULL(0), BOOLEAN(1), UINT(2), SINT(3), REAL(4), DOUBLE(5), OCTET_STRING(6), CHARACTER_STRING(7), BIT_STRING(8), ENUM(9), DATE(
        10), TIME(11), BACNET_OID(12);
private final int code;

ValueType(int code) {
	this.code = code;
}

public static ValueType valueOf(int code) {
	switch (code) {
	case 0:
		return NULL;
	case 1:
		return BOOLEAN;
	case 2:
		return UINT;
	case 3:
		return SINT;
	case 4:
		return REAL;
	case 5:
		return DOUBLE;
	case 6:
		return OCTET_STRING;
	case 7:
		return CHARACTER_STRING;
	case 8:
		return BIT_STRING;
	case 9:
		return ENUM;
	case 10:
		return DATE;
	case 11:
		return TIME;
	case 12:
		return BACNET_OID;

	default:
		throw new IllegalArgumentException("cannot instantiate ValueType enum element mapped to " + code);
	}
}

public int getCode() {
	return code;
}
}
