package com.synapsense.bacnet.network.pdu.apdu.codecs.primitives;

import java.io.ByteArrayInputStream;
import java.io.IOException;

public class BACnetSInt extends BACnetInteger {
	private final static ValueType type = ValueType.SINT;

	public BACnetSInt(Integer id, int payload) {
		super(id, payload & 0xFFFFFFFFl, type);
	}

	public BACnetSInt(ByteArrayInputStream stream, Integer expected_id) throws IOException {
		super(stream, expected_id, type);
	}

	@Override
	protected void calc_most_sign_byte() {
		long temp = payload;
		most_sign_byte = 4;
		final long mask = 0xFF000000L;
		if ((temp & 0x80000000l) == 0x00000000l) { // positive numbers
			while ((most_sign_byte > 1) && ((temp & mask) == 0)) {
				most_sign_byte--;
				temp <<= 8;
			}
			if ((temp & 0x80000000l) != 0)
				most_sign_byte++; // first meaning octet seems to be negative,
								  // so we must add additional zero octet
		} else {
			while ((most_sign_byte > 1) && ((temp & mask) == mask)) {
				most_sign_byte--;
				temp <<= 8;
			}
			if ((temp & 0x80000000l) == 0)
				most_sign_byte++; // first meaning octet seems to be positive,
								  // so we must add additional 0xFF octet
		}
	}

	@Override
	protected void decode(ByteArrayInputStream stream) throws IOException {
		most_sign_byte = (int) tag.getPayloadSize();
		byte first = (byte) read(stream);
		payload = ((first >= 0) ? 0 : 0xFFFFFF00) | (0xFF & first);
		for (int i = 0; i < most_sign_byte - 1; i++) {
			payload <<= 8;
			payload |= read(stream);
		}
	}

	public int getPayload() {
		return (int) payload;
	}
}
