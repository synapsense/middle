/**
 * 
 */
package com.synapsense.bacnet.network.pdu.apdu.codecs.codecs.primitive;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

import com.synapsense.bacnet.network.pdu.apdu.codecs.codecs.Codec;
import com.synapsense.bacnet.network.pdu.apdu.codecs.codecs.CodecSupports;
import com.synapsense.bacnet.network.pdu.apdu.codecs.exceptions.DecodingException;
import com.synapsense.bacnet.network.pdu.apdu.codecs.exceptions.EncodingException;
import com.synapsense.bacnet.network.pdu.apdu.codecs.primitives.BACnetNull;

@CodecSupports(payloadClass = Object.class, codecName = "NullCodec")
public class NullCodec extends Codec {

	@Override
	protected Object decode(ByteArrayInputStream in, Integer expected_id) throws DecodingException {
		return null;
	}

	@Override
	protected void encode(ByteArrayOutputStream out, Integer id, Object payload) throws EncodingException {
		try {
			BACnetNull a = new BACnetNull(id);
			a.encode(out);
		} catch (Exception e) {
			throw new EncodingException(e);
		}
	}

}
