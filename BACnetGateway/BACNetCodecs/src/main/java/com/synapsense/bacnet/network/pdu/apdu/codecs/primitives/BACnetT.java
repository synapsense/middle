package com.synapsense.bacnet.network.pdu.apdu.codecs.primitives;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class BACnetT extends BACnetPrimitive {
	private TagClass tag_class;
	private byte firstTagOctet;
	private int tag_id;
	private int encoded_size;

	public void checkContent() {
		if ((tag_id >= 255) || (tag_id < 0))
			throw new IllegalArgumentException("tag id must be in range [0..254]");
		;
		if ((tag_id > 15) && (tag_class == TagClass.APPLICATION))
			throw new IllegalArgumentException("tag id for Application class tag must be in range [0..15]");
	}

	public BACnetT(TagClass tag_class, int tag_id) {
		this.tag_class = tag_class;
		this.tag_id = tag_id;

		checkContent();

		encoded_size = 1;

		if (tag_id > 15)
			encoded_size = 2;
	}

	public BACnetT(ByteArrayInputStream stream) throws IOException {
		decode(stream);
	}

	@Override
	protected void decode(ByteArrayInputStream stream) throws IOException {
		int value = read(stream);
		firstTagOctet = (byte) value;
		encoded_size = 1;

		if ((value & 0x08) != 0) {
			tag_class = TagClass.CONTEXT_SPECIFIC;
		} else {
			tag_class = TagClass.APPLICATION;
		}

		tag_id = (value & 0xF0) >>> 4;

		if ((tag_id == 15) && (tag_class.equals(TagClass.CONTEXT_SPECIFIC))) {
			encoded_size++;
			// if(buffer_payload_size < encoded_size) throw new
			// IllegalArgumentException("buffer is too small");
			tag_id = read(stream);
			// if(tag_id == 255) throw new
			// IllegalArgumentException("wrong tag id read from buffer");
		}
	}

	@Override
	public void encode(ByteArrayOutputStream stream) {
		throw new IllegalArgumentException("BACnetT cannot be serialized to byte stream");
	}

	public void encode(byte[] buffer, int offset) {
		if (tag_id < 15) {
			buffer[offset] = (byte) (tag_id << 4);
		} else {
			buffer[offset] = (byte) 0xF0;
			buffer[offset + 1] = (byte) (tag_id & 0xFF);
		}

		if (tag_class.equals(TagClass.CONTEXT_SPECIFIC))
			buffer[offset] = (byte) (buffer[offset] | 0x08);
	}

	@Override
	public int getEncodedSize() {
		return encoded_size;
	}

	public boolean equals(BACnetT other) {
		return ((this.tag_class == other.tag_class) && (this.tag_id == other.tag_id));
	}

	public int hashCode() {
		return tag_class.hashCode() * 100 + tag_id;
	}

	/**
	 * check such constraint, applicable for each primitive: CONTEXT_SPECIFIC
	 * tags must have tag id, but APPLICATION tags - mustn't;
	 * 
	 * @param tag_class
	 *            - TagClass.APPLICATION or TagClass.CONTEXT_SPECIFIC
	 * @param id
	 *            - null, if tag id is not set, tag id - otherwise
	 * @return check result :-)
	 */
	public boolean checkTag(Integer expected_id, ValueType expected_type) {
		if ((tag_class == TagClass.APPLICATION) ^ (expected_id == null))
			return false;
		if ((tag_class == TagClass.APPLICATION) && (expected_type.getCode() != tag_id))
			return false;
		if ((tag_class == TagClass.CONTEXT_SPECIFIC) && (expected_id != tag_id))
			return false;
		return true;
	}

	public TagClass getTagClass() {
		return tag_class;
	}

	public int getID() {
		return tag_id;
	}

	public byte getFirstTagOctet() {
		return firstTagOctet;
	}
}
