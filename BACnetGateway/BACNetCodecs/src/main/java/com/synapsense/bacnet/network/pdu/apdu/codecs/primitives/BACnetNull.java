package com.synapsense.bacnet.network.pdu.apdu.codecs.primitives;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * NULL primitive; The encoding of a Null value shall be primitive, with no
 * contents octet. (20.2.3) Example: Application-tagged null value ASN.1 = NULL
 * Application Tag = Null (Tag Number = 0) Encoded Tag = X'00'
 * 
 * @author anechaev
 * 
 */
public class BACnetNull extends BACnetTLV {
	private final static ValueType type = ValueType.NULL;

	/**
	 * Creates a new BACnet NULL primitive
	 * 
	 * @param id
	 *            - null for APPLICATION class tags, not null tag id for CONTEXT
	 *            SPECIFIC tags
	 */
	public BACnetNull(Integer id) {
		createTag(id, type, 0);
	}

	public BACnetNull(ByteArrayInputStream stream, Integer expected_id) throws IOException {
		super(stream, expected_id, type);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.synapsense.bacnet.APDUparser.codecs.BACnetEncodeable#encode(byte[],
	 * int)
	 */
	@Override
	public void encode(ByteArrayOutputStream stream) throws IOException {
		tag.encode(stream);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.synapsense.bacnet.APDUparser.codecs.BACnetEncodeable#decode(byte[],
	 * int)
	 */
	@Override
	protected void decode(ByteArrayInputStream stream) throws IOException {
		// DOES NOTHING FOR NULLS
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.synapsense.bacnet.APDUparser.codecs.BACnetEncodeable#getEncodedSize()
	 */
	@Override
	public int getEncodedSize() {
		return tag.getEncodedSize();
	}
}
