package com.synapsense.bacnet.network.pdu.apdu.codecs.codecs;

import com.synapsense.bacnet.network.pdu.apdu.codecs.exceptions.CannotRegisterCodecException;
import com.synapsense.bacnet.network.pdu.apdu.codecs.exceptions.NoSuitableCodecException;

public abstract class CodecsAF {

	protected final static Class<?> extractPayloadClass(Class<? extends Codec> codec)
	        throws CannotRegisterCodecException {
		CodecSupports annot = codec.getAnnotation(CodecSupports.class);
		if (annot == null)
			throw new CannotRegisterCodecException("No annotation for codec " + codec.getCanonicalName());
		return annot.payloadClass();
	}

	protected final static String extractCodecName(Class<? extends Codec> codec) {
		CodecSupports annot = codec.getAnnotation(CodecSupports.class);
		if (annot == null)
			return null;

		if (annot.codecName() == CodecSupports.DEFAULT_CODEC_NAME)
			return null;

		return annot.codecName();
	}

	public abstract Codec getCodec(Class<?> payload) throws NoSuitableCodecException;

	public abstract Codec getCodec(String codecName) throws NoSuitableCodecException;

	public abstract void registerCodec(Class<? extends Codec> codec) throws CannotRegisterCodecException;

	public abstract void registerCodec(String codecName, String codecClassName) throws CannotRegisterCodecException;
}
