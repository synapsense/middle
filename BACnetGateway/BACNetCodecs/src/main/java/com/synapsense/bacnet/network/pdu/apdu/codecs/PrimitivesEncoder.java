package com.synapsense.bacnet.network.pdu.apdu.codecs;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import com.synapsense.bacnet.network.pdu.apdu.codecs.primitives.BACnetBitString;
import com.synapsense.bacnet.network.pdu.apdu.codecs.primitives.BACnetBoolean;
import com.synapsense.bacnet.network.pdu.apdu.codecs.primitives.BACnetCharacterString;
import com.synapsense.bacnet.network.pdu.apdu.codecs.primitives.BACnetDate;
import com.synapsense.bacnet.network.pdu.apdu.codecs.primitives.BACnetDouble;
import com.synapsense.bacnet.network.pdu.apdu.codecs.primitives.BACnetEnum;
import com.synapsense.bacnet.network.pdu.apdu.codecs.primitives.BACnetNull;
import com.synapsense.bacnet.network.pdu.apdu.codecs.primitives.BACnetObjectID;
import com.synapsense.bacnet.network.pdu.apdu.codecs.primitives.BACnetOctetString;
import com.synapsense.bacnet.network.pdu.apdu.codecs.primitives.BACnetReal;
import com.synapsense.bacnet.network.pdu.apdu.codecs.primitives.BACnetSInt;
import com.synapsense.bacnet.network.pdu.apdu.codecs.primitives.BACnetSequence;
import com.synapsense.bacnet.network.pdu.apdu.codecs.primitives.BACnetTime;
import com.synapsense.bacnet.network.pdu.apdu.codecs.primitives.BACnetUInt;
import com.synapsense.bacnet.system.shared.BACnetDateValue;
import com.synapsense.bacnet.system.shared.BACnetObjectIdentifier;
import com.synapsense.bacnet.system.shared.BACnetTimeValue;

public class PrimitivesEncoder {
	private ByteArrayOutputStream buffer;

	public PrimitivesEncoder() {
		buffer = new ByteArrayOutputStream(1024);
	}

	public byte[] getEncodedBuffer() {
		return buffer.toByteArray();
	}

	public void encodeNull(Integer id) throws IOException {
		BACnetNull value = new BACnetNull(id);
		value.encode(buffer);
	}

	public void encodeBoolean(Integer id, boolean value) throws IOException {
		BACnetBoolean a = new BACnetBoolean(id, value);
		a.encode(buffer);
	}

	public void encodeOctetString(Integer id, byte[] value) throws IOException {
		BACnetOctetString a = new BACnetOctetString(id, value);
		a.encode(buffer);
	}

	public void encodeUInt(Integer id, long value) throws IOException {
		BACnetUInt a = new BACnetUInt(id, value);
		a.encode(buffer);
	}

	public void encodeSInt(Integer id, int value) throws IOException {
		BACnetSInt a = new BACnetSInt(id, value);
		a.encode(buffer);
	}

	public void encodeCharacterString(Integer id, String value) throws IOException {
		BACnetCharacterString a = new BACnetCharacterString(id, value);
		a.encode(buffer);
	}

	public void encodeObjectID(Integer id, BACnetObjectIdentifier value) throws IOException {
		BACnetObjectID a = new BACnetObjectID(id, value);
		a.encode(buffer);
	}

	public void encodeObjectID(Integer id, short objType, int instNumber) throws IOException {
		BACnetObjectID a = new BACnetObjectID(id, objType, instNumber);
		a.encode(buffer);
	}

	public void encodeReal(Integer id, float value) throws IOException {
		BACnetReal a = new BACnetReal(id, value);
		a.encode(buffer);
	}

	public void encodeReal(Integer id, double value) throws IOException {
		BACnetDouble a = new BACnetDouble(id, value);
		a.encode(buffer);
	}

	public void encodeSequenceStart(Integer id) throws IOException {
		BACnetSequence a = new BACnetSequence(id, BACnetSequence.Actions.SEQUENCE_START);
		a.encode(buffer);
	}

	public void encodeSequenceStop(Integer id) throws IOException {
		BACnetSequence a = new BACnetSequence(id, BACnetSequence.Actions.SEQUENCE_STOP);
		a.encode(buffer);
	}

	public void encodeDate(Integer id, BACnetDateValue value) throws IOException {
		BACnetDate a = new BACnetDate(id, value);
		a.encode(buffer);
	}

	public void encodeEnum(Integer id, long value) throws IOException {
		BACnetEnum a = new BACnetEnum(id, value);
		a.encode(buffer);
	}

	public void encodeBitString(Integer id, boolean[] value) throws IOException {
		BACnetBitString a = new BACnetBitString(id, value);
		a.encode(buffer);
	}

	public void encodeTime(Integer id, BACnetTimeValue value) throws IOException {
		BACnetTime a = new BACnetTime(id, value);
		a.encode(buffer);
	}

	public void appendEncodedBuffer(byte[] value) throws IOException {
		this.buffer.write(value);
	}
}
