package com.synapsense.bacnet.network.pdu.apdu.codecs.primitives;

import java.io.ByteArrayInputStream;
import java.io.IOException;

/**
 * The common description of BACnet's tagged APDU primitive
 * 
 * @author anechaev
 * 
 */
public abstract class BACnetTLV extends BACnetPrimitive {
	protected BACnetTL tag;
	protected final static String malformed_buffer_error = "buffer does not contain value expected";

	protected BACnetTLV(ByteArrayInputStream stream, Integer expected_id, ValueType type) throws IOException {
		tag = new BACnetTL(stream);
		if (!tag.checkTag(expected_id, type))
			throw new IllegalArgumentException(malformed_buffer_error);
		decode(stream);
	}

	protected BACnetTLV() {

	}

	protected void createTag(Integer id, ValueType type, long payload_size) {
		if (id == null) {
			tag = new BACnetTL(TagClass.APPLICATION, type.getCode(), payload_size);
		} else {
			tag = new BACnetTL(TagClass.CONTEXT_SPECIFIC, id, payload_size);
		}
	}
}
