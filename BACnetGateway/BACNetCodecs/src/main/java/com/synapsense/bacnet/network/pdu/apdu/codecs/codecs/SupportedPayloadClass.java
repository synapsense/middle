package com.synapsense.bacnet.network.pdu.apdu.codecs.codecs;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface SupportedPayloadClass {
	Class<?> payloadClass();
}
