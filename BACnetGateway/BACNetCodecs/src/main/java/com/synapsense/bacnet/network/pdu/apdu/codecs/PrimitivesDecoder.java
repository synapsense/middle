package com.synapsense.bacnet.network.pdu.apdu.codecs;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import com.synapsense.bacnet.network.pdu.apdu.codecs.exceptions.NotResolvedPayloadException;
import com.synapsense.bacnet.network.pdu.apdu.codecs.primitives.BACnetBitString;
import com.synapsense.bacnet.network.pdu.apdu.codecs.primitives.BACnetBoolean;
import com.synapsense.bacnet.network.pdu.apdu.codecs.primitives.BACnetCharacterString;
import com.synapsense.bacnet.network.pdu.apdu.codecs.primitives.BACnetDate;
import com.synapsense.bacnet.network.pdu.apdu.codecs.primitives.BACnetDouble;
import com.synapsense.bacnet.network.pdu.apdu.codecs.primitives.BACnetEnum;
import com.synapsense.bacnet.network.pdu.apdu.codecs.primitives.BACnetNull;
import com.synapsense.bacnet.network.pdu.apdu.codecs.primitives.BACnetObjectID;
import com.synapsense.bacnet.network.pdu.apdu.codecs.primitives.BACnetOctetString;
import com.synapsense.bacnet.network.pdu.apdu.codecs.primitives.BACnetReal;
import com.synapsense.bacnet.network.pdu.apdu.codecs.primitives.BACnetSInt;
import com.synapsense.bacnet.network.pdu.apdu.codecs.primitives.BACnetSequence;
import com.synapsense.bacnet.network.pdu.apdu.codecs.primitives.BACnetTL;
import com.synapsense.bacnet.network.pdu.apdu.codecs.primitives.BACnetTime;
import com.synapsense.bacnet.network.pdu.apdu.codecs.primitives.BACnetUInt;
import com.synapsense.bacnet.network.pdu.apdu.codecs.primitives.TagClass;
import com.synapsense.bacnet.network.pdu.apdu.codecs.primitives.ValueType;
import com.synapsense.bacnet.system.shared.BACnetDateValue;
import com.synapsense.bacnet.system.shared.BACnetObjectIdentifier;
import com.synapsense.bacnet.system.shared.BACnetTimeValue;

public class PrimitivesDecoder {
	private ByteArrayInputStream buffer;

	public PrimitivesDecoder(byte[] buffer) {
		this.buffer = new ByteArrayInputStream(buffer);
	}

	public PrimitivesDecoder(ByteArrayInputStream buffer) {
		this.buffer = buffer;
	}

	public BACnetTL askForTag() {
		BACnetTL tag = null;
		buffer.mark(10);
		try {
			tag = new BACnetTL(buffer);
		} catch (Exception e) {
		} finally {
			buffer.reset();
		}
		return tag;
	}

	public void decodeNull(Integer expected_id) throws IOException {
		new BACnetNull(buffer, expected_id);
	}

	public boolean decodeBoolean(Integer expected_id) throws IOException {
		BACnetBoolean value = new BACnetBoolean(buffer, expected_id);
		return value.getPayload();
	}

	public byte[] decodeOctetString(Integer expected_id) throws IOException {
		BACnetOctetString value = new BACnetOctetString(buffer, expected_id);
		return value.getPayload();
	}

	public long decodeUInt(Integer expected_id) throws IOException {
		BACnetUInt value = new BACnetUInt(buffer, expected_id);
		return value.getPayload();
	}

	public int decodeSInt(Integer expected_id) throws IOException {
		BACnetSInt value = new BACnetSInt(buffer, expected_id);
		return value.getPayload();
	}

	public String decodeCharacterString(Integer expected_id) throws IOException {
		BACnetCharacterString value = new BACnetCharacterString(buffer, expected_id);
		return value.getPayload();
	}

	public BACnetObjectIdentifier decodeObjectID(Integer expected_id) throws IOException {
		BACnetObjectID value = new BACnetObjectID(buffer, expected_id);
		return value.getPayload();
	}

	public float decodeReal(Integer expected_id) throws IOException {
		BACnetReal value = new BACnetReal(buffer, expected_id);
		return value.getPayload();
	}

	public double decodeDouble(Integer expected_id) throws IOException {
		BACnetDouble value = new BACnetDouble(buffer, expected_id);
		return value.getPayload();
	}

	public void decodeSequenceStart(Integer expected_id) throws IOException {
		BACnetSequence value = new BACnetSequence(buffer, expected_id);
		if (value.getPayload() != BACnetSequence.Actions.SEQUENCE_START)
			throw new IllegalArgumentException("sequence start tag not found");
	}

	public void decodeSequenceStop(Integer expected_id) throws IOException {
		BACnetSequence value = new BACnetSequence(buffer, expected_id);
		if (value.getPayload() != BACnetSequence.Actions.SEQUENCE_STOP)
			throw new IllegalArgumentException("sequence stop tag not found");
	}

	public BACnetDateValue decodeDate(Integer expected_id) throws IOException {
		BACnetDate value = new BACnetDate(buffer, expected_id);
		return value.getPayload();
	}

	public BACnetTimeValue decodeTime(Integer expected_id) throws IOException {
		BACnetTime value = new BACnetTime(buffer, expected_id);
		return value.getPayload();
	}

	public long decodeEnum(Integer expected_id) throws IOException {
		BACnetEnum value = new BACnetEnum(buffer, expected_id);
		return value.getPayload();
	}

	public boolean[] decodeBitString(Integer expected_id) throws IOException {
		BACnetBitString value = new BACnetBitString(buffer, expected_id);
		return value.getPayload();
	}

	public byte[] getTaggedRaw() throws IOException {
		BACnetTL tag = askForTag();
		int toRead = (int) (tag.getEncodedSize() + tag.getPayloadSize() + 1);
		byte[] result = new byte[toRead];
		int actuallyRead = 0;
		while (toRead > 0) {
			int read = buffer.read(result, actuallyRead, toRead);
			System.out.println("read: " + read);
			toRead -= read;
			actuallyRead += read;
		}
		return result;
	}

	/**
	 * Tries to decode payload as application-specific String, Unsigned or Real
	 * 
	 * @return converted to string payload otherwise
	 * @throws IOException
	 *             if payload is invalid (wrong encoding)
	 * @throws NotResolvedPayloadException
	 *             if payload contains data with unsupported type or if there is
	 *             no data in payload
	 */

	public String decodeAny() throws IOException, NotResolvedPayloadException {
		BACnetTL tag = askForTag();

		if (tag == null)
			return null;
		if (tag.getTagClass() != TagClass.APPLICATION)
			throw new NotResolvedPayloadException("");

		buffer.mark(1000);
		try {
			switch (ValueType.valueOf(tag.getID())) {
			case CHARACTER_STRING:
				return decodeCharacterString(null);
			case UINT:
				return new Long(decodeUInt(null)).toString();
			case SINT:
				return new Integer(decodeSInt(null)).toString();
			case REAL:
				return new Float(decodeReal(null)).toString();
			case DOUBLE:
				return new Double(decodeDouble(null)).toString();
			case BOOLEAN:
				return new Boolean(decodeBoolean(null)).toString();
			case ENUM:
				return new Long(decodeEnum(null)).toString();
			default:
				buffer.reset();
				throw new NotResolvedPayloadException("");
			}
		} catch (IOException e) {
			buffer.reset();
			throw e;
		} catch (Exception e) {
			buffer.reset();
			throw new NotResolvedPayloadException("");
		}
	}

	public boolean askForSequenceStop(Integer expected_id) throws IOException {
		buffer.mark(1000);
		try {
			BACnetSequence value = new BACnetSequence(buffer, expected_id);
			buffer.reset();
			if (value.getPayload() == BACnetSequence.Actions.SEQUENCE_STOP)
				return true;
		} catch (Exception e) {
		}
		buffer.reset();
		return false;
	}
}
