package com.synapsense.bacnet.network.pdu.apdu.codecs.primitives;

import java.io.ByteArrayInputStream;
import java.io.IOException;

/**
 * The encoding of a real number value shall be primitive, with four contents
 * octets. Real numbers shall be encoded using the method specified in ANSI/IEEE
 * Standard 754-1985, "IEEE Standard for Binary Floating-Point Arithmetic." This
 * standard should be consulted for details. The multi-octet value shall be
 * conveyed with the most significant (sign and exponent) octet first. For the
 * case of single precision real numbers, the encoding format is: Bit Number: 31
 * 30 ... 23 22 ... 0 |---|---|...|...|---|---|...|...|---| | s | e | f |
 * |---|---|...|...|---|---|...|...|---| Field Width: 1 <----- 8 ------><-----
 * 23 ----> where the numbers indicate the field widths in bits. Non-zero values
 * shall be represented by the equation v = (-1)s2e-127(1*f) where the symbol
 * "*" signifies the binary point. Zero shall be indicated by setting s, e, and
 * f to zero.
 * 
 * @author anechaev
 * 
 */
public class BACnetReal extends BACnetInteger {
	private final static ValueType type = ValueType.REAL;

	public BACnetReal(Integer id, float payload) {
		super(id, Float.floatToIntBits(payload) & 0xFFFFFFFFl, type);
	}

	public BACnetReal(ByteArrayInputStream stream, Integer expected_id) throws IOException {
		super(stream, expected_id, type);
	}

	@Override
	protected void calc_most_sign_byte() {
		most_sign_byte = 4;
	}

	public float getPayload() {
		return Float.intBitsToFloat((int) payload);
	}
}
