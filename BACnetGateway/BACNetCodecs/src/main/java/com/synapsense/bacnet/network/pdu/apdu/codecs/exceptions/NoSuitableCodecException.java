package com.synapsense.bacnet.network.pdu.apdu.codecs.exceptions;

public class NoSuitableCodecException extends CodecsException {

	public NoSuitableCodecException(String reason) {
		super(reason);
	}

	public NoSuitableCodecException(Throwable reason) {
		super(reason);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 5213133879951553005L;

}
