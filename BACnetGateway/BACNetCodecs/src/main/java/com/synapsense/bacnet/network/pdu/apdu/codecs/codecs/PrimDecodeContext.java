package com.synapsense.bacnet.network.pdu.apdu.codecs.codecs;

import com.synapsense.bacnet.network.pdu.apdu.codecs.codecs.primitive.PrimFactory;

public class PrimDecodeContext extends DecodeContext {
	private PrimFactory codecs_factory;

	public PrimDecodeContext(byte[] buffer) {
		super(buffer);
		codecs_factory = new PrimFactory();
	}

	@Override
	public CodecsAF getCodecsFactory() {
		return codecs_factory;
	}

}
