package com.synapsense.bacnet.network.pdu.apdu.codecs.primitives;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class BACnetTL extends BACnetPrimitive {
	BACnetT tag;
	private long payload_size;
	private int encoded_size;

	private void checkContent() {
		tag.checkContent();
		if ((payload_size > 0xFFFFFFFFl) || (payload_size < 0))
			throw new IllegalArgumentException("payload size exceeds limits [0..2^32-1]");
	}

	public BACnetTL(TagClass tag_class, int tag_id, long payload_size) {
		tag = new BACnetT(tag_class, tag_id);
		this.payload_size = payload_size;

		checkContent();

		encoded_size = 1;

		if (payload_size > 65535) {
			encoded_size += 5;
		} else if (payload_size > 253) {
			encoded_size += 3;
		} else if (payload_size > 4)
			encoded_size++;
	}

	public BACnetTL(ByteArrayInputStream stream) throws IOException {
		tag = new BACnetT(stream);
		decode(stream);
		checkContent();
	}

	private void encode(byte[] buffer, int offset) {
		long temp_payload_size = payload_size;
		int actual_offset = tag.getEncodedSize();
		tag.encode(buffer, offset);

		if (payload_size < 5) {
			buffer[offset] = (byte) (buffer[offset] | payload_size);
		} else if (payload_size < 0xFE) {
			buffer[offset] = (byte) (buffer[offset] | 0x05);
			buffer[actual_offset] = (byte) payload_size;
		} else {
			buffer[offset] = (byte) (buffer[offset] | 0x05);
			if (payload_size < 65536) {
				buffer[actual_offset] = (byte) 0xFE;
				for (int i = 2; i > 0; i--) {
					buffer[actual_offset + i] = (byte) (temp_payload_size & 0xFF);
					temp_payload_size >>>= 8;
				}
			} else {
				buffer[actual_offset] = (byte) 0xFF;
				for (int i = 4; i > 0; i--) {
					buffer[actual_offset + i] = (byte) (temp_payload_size & 0xFF);
					temp_payload_size >>>= 8;
				}
			}
		}
	}

	@Override
	public void encode(ByteArrayOutputStream stream) throws IOException {
		byte[] buffer = new byte[getEncodedSize()];
		tag.encode(buffer, 0);
		encode(buffer, 0);
		stream.write(buffer);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.synapsense.bacnet.APDUparser.codecs.BACnetEncodeable#decode(byte[],
	 * int)
	 */
	@Override
	protected void decode(ByteArrayInputStream stream) throws IOException {
		payload_size = tag.getFirstTagOctet() & 0x07;

		if (payload_size == 5) {
			encoded_size++;
			// if(buffer_payload_size < encoded_size) throw new
			// IllegalArgumentException("buffer is too small");
			payload_size = read(stream);

			switch ((int) payload_size) {
			case 0xFE:
				encoded_size += 2;
				// if(buffer_payload_size < encoded_size) throw new
				// IllegalArgumentException("buffer is too small");
				payload_size = (read(stream) << 8);
				payload_size |= read(stream);
				break;
			case 0xFF:
				encoded_size += 4;
				// if(buffer_payload_size < encoded_size) throw new
				// IllegalArgumentException("buffer is too small");
				payload_size = (read(stream) << 24);
				payload_size |= (read(stream) << 16);
				payload_size |= (read(stream) << 8);
				payload_size |= read(stream);
				payload_size &= 0xFFFFFFFFl;
				break;
			}
		}
	}

	public boolean equals(BACnetTL other) {
		return (tag.equals(other.tag) && (this.payload_size == other.payload_size));
	}

	public boolean checkTag(Integer expected_id, ValueType expected_type) {
		return tag.checkTag(expected_id, expected_type);
	}

	public TagClass getTagClass() {
		return tag.getTagClass();
	}

	public int getID() {
		return tag.getID();
	}

	public long getPayloadSize() {
		return payload_size;
	}

	public int getEncodedSize() {
		return encoded_size + tag.getEncodedSize() - 1;
	}
}
