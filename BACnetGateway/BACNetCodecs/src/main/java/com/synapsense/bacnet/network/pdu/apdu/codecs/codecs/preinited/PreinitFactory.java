package com.synapsense.bacnet.network.pdu.apdu.codecs.codecs.preinited;

import java.util.HashMap;

import com.synapsense.bacnet.network.pdu.apdu.codecs.codecs.Codec;
import com.synapsense.bacnet.network.pdu.apdu.codecs.codecs.CodecsAF;
import com.synapsense.bacnet.network.pdu.apdu.codecs.exceptions.CannotRegisterCodecException;
import com.synapsense.bacnet.network.pdu.apdu.codecs.exceptions.NoSuitableCodecException;

public class PreinitFactory extends CodecsAF {
	private HashMap<Class<?>, Codec> supported_codecs;

	public PreinitFactory() {
		supported_codecs = new HashMap<Class<?>, Codec>();
		try {
			registerCodec(OctetStringCodec.class);
		} catch (CannotRegisterCodecException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public Codec getCodec(Class<?> payload) throws NoSuitableCodecException {
		Codec codec = supported_codecs.get(payload);
		return codec;
	}

	@Override
	public void registerCodec(Class<? extends Codec> codec) throws CannotRegisterCodecException {
		// TODO Auto-generated method stub

	}

	@Override
	public Codec getCodec(String codecName) throws NoSuitableCodecException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void registerCodec(String codecName, String codecClassName) throws CannotRegisterCodecException {
		// TODO Auto-generated method stub

	}

}
