package com.synapsense.bacnet.network.pdu.apdu.codecs.codecs.complex;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import com.synapsense.bacnet.network.pdu.apdu.codecs.PrimitivesDecoder;
import com.synapsense.bacnet.network.pdu.apdu.codecs.PrimitivesEncoder;
import com.synapsense.bacnet.network.pdu.apdu.codecs.codecs.Codec;
import com.synapsense.bacnet.network.pdu.apdu.codecs.exceptions.DecodingException;
import com.synapsense.bacnet.network.pdu.apdu.codecs.exceptions.EncodingException;
import com.synapsense.bacnet.system.shared.BACnetService;

/**
 * This class is responsible for coding the ProtocolServicesSupported property.
 * Internally, this property is stored as array of BACnetService objects. For
 * BACnet, this property seems to be a constant-length bit string.
 * 
 * @author anechaev
 * 
 */
public class ProtocolServicesSupportedCodec extends Codec {
	private final static int MAX_SERVICES_SUPPORTED = 40;

	@Override
	protected Object decode(ByteArrayInputStream in, Integer expected_id) throws DecodingException {
		try {
			PrimitivesDecoder decoder = new PrimitivesDecoder(in);

			// decoder.decodeSequenceStart(expected_id);

			boolean[] result = decoder.decodeBitString(null);
			Collection<BACnetService> services = new ArrayList<BACnetService>();

			for (short i = 0; i < result.length; i++) {
				if (result[i]) {
					if (i == BACnetService.ReadProperty.serviceCode) {
						services.add(BACnetService.ReadProperty);
					} else if (i == BACnetService.WriteProperty.serviceCode) {
						services.add(BACnetService.WriteProperty);
					}
				}
			}

			// decoder.decodeSequenceStop(expected_id);
			return services.toArray();
		} catch (IOException e) {
			throw new DecodingException(e);
		}
	}

	@Override
	protected void encode(ByteArrayOutputStream out, Integer id, Object payload) throws EncodingException {
		boolean[] result = new boolean[MAX_SERVICES_SUPPORTED];
		BACnetService[] services = (BACnetService[]) payload;

		for (int i = 0; i < services.length; i++) {
			if ((services[i].serviceCode < 0) || (services[i].serviceCode >= MAX_SERVICES_SUPPORTED)) {
				throw new IllegalArgumentException("payload contains wrong service:" + services[i]);

			}
			result[services[i].serviceCode] = true;
		}

		PrimitivesEncoder encoder = new PrimitivesEncoder();

		try {
			// encoder.encodeSequenceStart(id);
			encoder.encodeBitString(null, result);
			// encoder.encodeSequenceStop(id);
			out.write(encoder.getEncodedBuffer());
		} catch (IOException e) {
			throw new EncodingException(e);
		}
	}
}
