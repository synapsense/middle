package com.synapsense.bacnet.network.pdu.apdu.codecs.tests;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import com.synapsense.bacnet.network.pdu.apdu.codecs.primitives.BACnetObjectID;
import com.synapsense.bacnet.system.shared.BACnetObjectType;

import junit.framework.TestCase;

public class BACnetObjectIDTest extends TestCase {
	public void test20_2_14() throws IOException {
		BACnetObjectID value = new BACnetObjectID(null, (short) BACnetObjectType.BINARY_INPUT, 15);
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		value.encode(out);
		byte[] buffer = out.toByteArray();
		assertEquals("20.2.14 claims that buffer must be encoded in 5 octets", buffer.length, 5);
		assertEquals("20.2.14 claims that buffer[0] == 0xc4", 0xFF & buffer[0], 0xc4);
		assertEquals("20.2.14 claims that buffer[1] == 0x00", 0xFF & buffer[1], 0x00);
		assertEquals("20.2.14 claims that buffer[2] == 0xc0", 0xFF & buffer[2], 0xc0);
		assertEquals("20.2.14 claims that buffer[3] == 0x00", 0xFF & buffer[3], 0x00);
		assertEquals("20.2.14 claims that buffer[4] == 0x0F", 0xFF & buffer[4], 0x0F);
	}

	public void test20_2_15() throws IOException {
		BACnetObjectID value = new BACnetObjectID(4, (short) BACnetObjectType.BINARY_INPUT, 15);
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		value.encode(out);
		byte[] buffer = out.toByteArray();
		assertEquals("20.2.14 claims that buffer must be encoded in 5 octets", buffer.length, 5);
		assertEquals("20.2.14 claims that buffer[0] == 0x4c", 0xFF & buffer[0], 0x4c);
		assertEquals("20.2.14 claims that buffer[1] == 0x00", 0xFF & buffer[1], 0x00);
		assertEquals("20.2.14 claims that buffer[2] == 0xc0", 0xFF & buffer[2], 0xc0);
		assertEquals("20.2.14 claims that buffer[3] == 0x00", 0xFF & buffer[3], 0x00);
		assertEquals("20.2.14 claims that buffer[4] == 0x0f", 0xFF & buffer[4], 0x0f);
	}
}
