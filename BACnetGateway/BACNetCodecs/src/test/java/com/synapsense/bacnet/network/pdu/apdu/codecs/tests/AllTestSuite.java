package com.synapsense.bacnet.network.pdu.apdu.codecs.tests;

import junit.framework.Test;
import junit.framework.TestSuite;

public class AllTestSuite {

	public static Test suite() {
		TestSuite suite = new TestSuite("Test for com.synapsense.bacnet.network.pdu.apdu.codecs.tests");
		// $JUnit-BEGIN$
		// suite.addTestSuite(BACnetNullTest.class);
		// suite.addTestSuite(BACnetBooleanTest.class);
		suite.addTestSuite(BACnetOctetStringTest.class);
		suite.addTestSuite(BACnetUIntTest.class);
		suite.addTestSuite(BACnetCharacterStringTest.class);
		suite.addTestSuite(BACnetTagTest.class);
		suite.addTestSuite(BACnetObjectIDTest.class);
		suite.addTestSuite(BACnetSIntTest.class);
		suite.addTestSuite(BACnetRealTest.class);
		suite.addTestSuite(BACnetDoubleTest.class);
		suite.addTestSuite(BACnetSequenceTest.class);
		suite.addTestSuite(BACnetDateTest.class);
		suite.addTestSuite(BACnetTimeTest.class);
		suite.addTestSuite(BACnetEnumTest.class);
		suite.addTestSuite(BACnetBitStringTest.class);
		suite.addTestSuite(ObjectListCodecTest.class);
		suite.addTestSuite(ProtocolServicesSupportedCodecTest.class);
		suite.addTestSuite(ProtocolObjectTypesSupportedCodecTest.class);
		suite.addTestSuite(PrimitivesDecodeAny.class);
		// $JUnit-END$
		return suite;
	}

}
