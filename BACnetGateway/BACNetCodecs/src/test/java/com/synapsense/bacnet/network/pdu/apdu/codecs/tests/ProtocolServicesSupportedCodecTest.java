package com.synapsense.bacnet.network.pdu.apdu.codecs.tests;

import com.synapsense.bacnet.network.pdu.apdu.codecs.codecs.complex.ProtocolServicesSupportedCodec;
import com.synapsense.bacnet.network.pdu.apdu.codecs.exceptions.DecodingException;
import com.synapsense.bacnet.network.pdu.apdu.codecs.exceptions.EncodingException;
import com.synapsense.bacnet.system.shared.BACnetService;

import junit.framework.TestCase;

public class ProtocolServicesSupportedCodecTest extends TestCase {
	public void testAllServices() {
		BACnetService[] services = { BACnetService.ReadProperty, BACnetService.WriteProperty };
		byte[] result = null;
		ProtocolServicesSupportedCodec codec = new ProtocolServicesSupportedCodec();
		try {
			result = codec.encode(3, services);
		} catch (EncodingException e) {
			e.printStackTrace();
			fail(e.toString());
		}
		try {
			Object a = codec.decode(result, 3);
			assertEquals(services[0].serviceCode, ((BACnetService) ((Object[]) a)[0]).serviceCode);
			assertEquals(services[1].serviceCode, ((BACnetService) ((Object[]) a)[1]).serviceCode);
		} catch (DecodingException e) {
			e.printStackTrace();
			fail(e.toString());
		}
	}
}
