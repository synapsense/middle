package com.synapsense.bacnet.network.pdu.apdu.codecs.tests;

import java.util.Arrays;

import com.synapsense.bacnet.network.pdu.apdu.codecs.codecs.complex.ProtocolObjectTypesSupportedCodec;
import com.synapsense.bacnet.network.pdu.apdu.codecs.exceptions.DecodingException;
import com.synapsense.bacnet.network.pdu.apdu.codecs.exceptions.EncodingException;
import com.synapsense.bacnet.system.shared.BACnetObjectType;

import junit.framework.TestCase;

public class ProtocolObjectTypesSupportedCodecTest extends TestCase {
	public void testCreate3Objects() {
		short[] objectTypes = { BACnetObjectType.DEVICE, BACnetObjectType.ACCUMULATOR, BACnetObjectType.ANALOG_INPUT };
		Arrays.sort(objectTypes);
		byte[] result = null;
		ProtocolObjectTypesSupportedCodec codec = new ProtocolObjectTypesSupportedCodec();
		try {
			result = codec.encode(3, objectTypes);
		} catch (EncodingException e) {
			e.printStackTrace();
			fail(e.toString());
		}
		try {
			Object a = codec.decode(result, 3);
			assertEquals(new Short(objectTypes[0]), (Short) ((Object[]) a)[0]);
			assertEquals(new Short(objectTypes[1]), (Short) ((Object[]) a)[1]);
			assertEquals(new Short(objectTypes[2]), (Short) ((Object[]) a)[2]);
		} catch (DecodingException e) {
			e.printStackTrace();
			fail(e.toString());
		}
	}
}
