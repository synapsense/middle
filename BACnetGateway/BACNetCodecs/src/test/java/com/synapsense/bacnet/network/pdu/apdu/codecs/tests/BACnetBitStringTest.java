package com.synapsense.bacnet.network.pdu.apdu.codecs.tests;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Arrays;

import com.synapsense.bacnet.network.pdu.apdu.codecs.primitives.BACnetBitString;

import junit.framework.TestCase;

public class BACnetBitStringTest extends TestCase {
	private void performAction(boolean[] payload) throws IOException {
		BACnetBitString from = new BACnetBitString(null, payload);
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		from.encode(out);
		ByteArrayInputStream in = new ByteArrayInputStream(out.toByteArray());
		BACnetBitString to = new BACnetBitString(in, null);
		boolean[] result = to.getPayload();
		assertTrue(Arrays.equals(payload, result));
	}

	public void testZeroBits() throws IOException {
		performAction(new boolean[0]);
	}

	public void test1set() throws IOException {
		boolean[] payload = { true };
		performAction(payload);
	}

	public void test1unset() throws IOException {
		boolean[] payload = { false };
		performAction(payload);
	}

	public void test2bits() throws IOException {
		boolean[] payload = { true, false };
		performAction(payload);
	}

	public void test7bits() throws IOException {
		boolean[] payload = { true, false, true, false, true, false, true };
		performAction(payload);
	}

	public void test8bits() throws IOException {
		boolean[] payload = { true, false, true, false, true, false, true, false };
		performAction(payload);
	}

	public void test9bits() throws IOException {
		boolean[] payload = { true, false, true, false, true, false, true, false, true };
		performAction(payload);
	}

	public void test12bits() throws IOException {
		boolean[] payload = { true, false, true, false, true, false, true, false, true, false, true, false };
		performAction(payload);
	}

	public void test15bits() throws IOException {
		boolean[] payload = { true, false, true, false, true, false, true, false, true, false, true, false, true,
		        false, true };
		performAction(payload);
	}

	public void test16bits() throws IOException {
		boolean[] payload = { true, false, true, false, true, false, true, false, true, false, true, false, true,
		        false, true, false };
		performAction(payload);
	}

	public void test17bits() throws IOException {
		boolean[] payload = { true, false, true, false, true, false, true, false, true, false, true, false, true,
		        false, true, false, true };
		performAction(payload);
	}

	public void test20_2_10() throws IOException {
		boolean[] payload = { true, false, true, false, true };

		BACnetBitString a = new BACnetBitString(null, payload);
		assertEquals("20.2.10 claims that such value must be encoded in 3 bytes", a.getEncodedSize(), 3);
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		a.encode(out);
		byte[] buffer = out.toByteArray();
		assertEquals("buffer[0] == 0x82", 0xFF & buffer[0], 0x82);
		assertEquals("buffer[1] == 0x03", 0xFF & buffer[1], 0x03);
		assertEquals("buffer[2] == 0xA8", 0xFF & buffer[2], 0xA8);
	}

	public void test20_2_15() throws IOException {
		boolean[] payload = { true, false, true, false, true };

		BACnetBitString a = new BACnetBitString(0, payload);
		assertEquals("20.2.10 claims that such value must be encoded in 3 bytes", a.getEncodedSize(), 3);
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		a.encode(out);
		byte[] buffer = out.toByteArray();
		assertEquals("buffer[0] == 0x0A", 0xFF & buffer[0], 0x0A);
		assertEquals("buffer[1] == 0x03", 0xFF & buffer[1], 0x03);
		assertEquals("buffer[2] == 0xA8", 0xFF & buffer[2], 0xA8);
	}
}
