package com.synapsense.bacnet.network.pdu.apdu.codecs.tests;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import com.synapsense.bacnet.network.pdu.apdu.codecs.primitives.BACnetUInt;

import junit.framework.TestCase;

public class BACnetUIntTest extends TestCase {
	private void PayloadTesting(long payload) throws IOException {
		BACnetUInt a1 = new BACnetUInt(1, payload);
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		a1.encode(out);
		ByteArrayInputStream in = new ByteArrayInputStream(out.toByteArray());
		BACnetUInt a2 = new BACnetUInt(in, 1);
		assertEquals(a1.getPayload(), a2.getPayload());
	}

	public void testZeroPayload() throws IOException {
		PayloadTesting(0);
	}

	public void testPayload1() throws IOException {
		PayloadTesting(1);
	}

	public void testPayload127() throws IOException {
		PayloadTesting(127);
	}

	public void testPayload256() throws IOException {
		PayloadTesting(256);
	}

	public void testPayload1000() throws IOException {
		PayloadTesting(1000);
	}

	public void testPayload3456() throws IOException {
		PayloadTesting(3456);
	}

	public void testPayload65535() throws IOException {
		PayloadTesting(65535);
	}

	public void testPayload65536() throws IOException {
		PayloadTesting(65536);
	}

	public void testPayload1000000() throws IOException {
		PayloadTesting(1000000);
	}

	public void testPayloadIntMax() throws IOException {
		PayloadTesting(0xFFFFFFFFl);
	}

	public void testBACnet20_2_4() throws IOException {
		BACnetUInt a = new BACnetUInt(null, 72);
		assertEquals("20.2.4 claims that such value must be encoded in 2 bytes", a.getEncodedSize(), 2);
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		a.encode(out);
		byte[] buffer = out.toByteArray();
		assertEquals("buffer[0] == 0x21", 0xFF & buffer[0], 0x21);
		assertEquals("buffer[1] == 0x48", 0xFF & buffer[1], 0x48);
	}

	public void testBACnet20_2_15_for_UINT() throws IOException {
		BACnetUInt a = new BACnetUInt(0, 256);
		assertEquals("20.2.15 claims that such value must be encoded in 3 bytes", a.getEncodedSize(), 3);
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		a.encode(out);
		byte[] buffer = out.toByteArray();
		assertEquals("buffer[0] == 0x0A", 0xFF & buffer[0], 0x0A);
		assertEquals("buffer[1] == 0x01", 0xFF & buffer[1], 0x01);
		assertEquals("buffer[2] == 0x00", 0xFF & buffer[2], 0x00);
	}
}
