package com.synapsense.bacnet.network.pdu.apdu.codecs.tests;

import com.synapsense.bacnet.network.pdu.apdu.codecs.primitives.BACnetTL;
import com.synapsense.bacnet.network.pdu.apdu.codecs.primitives.TagClass;

import junit.framework.*;

public class BACnetTagTest extends TestCase {
	public void testNegativeTagID() {
		try {
			new BACnetTL(TagClass.APPLICATION, -1, 1);
			fail("No exception for negative tag id raised");
		} catch (IllegalArgumentException e) {
		}
	}

	public void testNegativePayload() {
		try {
			new BACnetTL(TagClass.APPLICATION, 1, -1);
			fail("No exception for negative payload size raised");
		} catch (IllegalArgumentException e) {
		}
	}

	public void testZeroPayload() {
		try {
			new BACnetTL(TagClass.APPLICATION, 1, 0);
		} catch (Exception e) {
			fail("Zero payload is not a mistake. It is used in NULL and APPL BOOLEAN encodings");
		}
	}

	public void testMaximumPayload() {
		new BACnetTL(TagClass.APPLICATION, 1, 0xFFFFFFFFL);
	}

	public void testMaximumOverPayload() {
		try {
			new BACnetTL(TagClass.APPLICATION, 1, 0x100000000L);
			fail("No exception for overflow payload size raised");
		} catch (IllegalArgumentException e) {
		}
	}

	public void testTagClassApplicationAndTagID() {
		try {
			new BACnetTL(TagClass.APPLICATION, 16, 1);
			fail("Application tag ids must be less than 16");
		} catch (IllegalArgumentException e) {
		}

	}

	public void testTagClassContextAndTagID() {
		try {
			new BACnetTL(TagClass.CONTEXT_SPECIFIC, 16, 1);
		} catch (IllegalArgumentException e) {
			fail("Context tag may be greater than 16");
		}

	}

	public void testTagIDOversized() {
		try {
			new BACnetTL(TagClass.CONTEXT_SPECIFIC, 255, 1);
			fail("Application tag ids must be less than 16");
		} catch (IllegalArgumentException e) {
		}

	}

	public void testTagLittleIDBigPayload() {
		new BACnetTL(TagClass.CONTEXT_SPECIFIC, 1, 300);
	}

	public void testEncodedSizes() {
		BACnetTL tag;
		tag = new BACnetTL(TagClass.CONTEXT_SPECIFIC, 0, 0);
		assertEquals("ID(0), Payload(0), encoded_size == 1", tag.getEncodedSize(), 1);

		tag = new BACnetTL(TagClass.CONTEXT_SPECIFIC, 16, 0);
		assertEquals("ID(16), Payload(0), encoded_size == 2", tag.getEncodedSize(), 2);

		tag = new BACnetTL(TagClass.CONTEXT_SPECIFIC, 1, 4);
		assertEquals("ID(1), Payload(4), encoded_size == 1", tag.getEncodedSize(), 1);

		tag = new BACnetTL(TagClass.CONTEXT_SPECIFIC, 1, 5);
		assertEquals("ID(1), Payload(5), encoded_size == 2", tag.getEncodedSize(), 2);

		tag = new BACnetTL(TagClass.CONTEXT_SPECIFIC, 16, 5);
		assertEquals("ID(16), Payload(5), encoded_size == 3", tag.getEncodedSize(), 3);

		tag = new BACnetTL(TagClass.CONTEXT_SPECIFIC, 1, 253);
		assertEquals("ID(1), Payload(253), encoded_size == 1", tag.getEncodedSize(), 2);

		tag = new BACnetTL(TagClass.CONTEXT_SPECIFIC, 1, 254);
		assertEquals("ID(1), Payload(254), encoded_size == 4", tag.getEncodedSize(), 4);

		tag = new BACnetTL(TagClass.CONTEXT_SPECIFIC, 1, 255);
		assertEquals("ID(1), Payload(255), encoded_size == 4", tag.getEncodedSize(), 4);

		tag = new BACnetTL(TagClass.CONTEXT_SPECIFIC, 1, 65535);
		assertEquals("ID(1), Payload(65535), encoded_size == 4", tag.getEncodedSize(), 4);

		tag = new BACnetTL(TagClass.CONTEXT_SPECIFIC, 1, 65536);
		assertEquals("ID(1), Payload(65536), encoded_size == 6", tag.getEncodedSize(), 6);

		tag = new BACnetTL(TagClass.CONTEXT_SPECIFIC, 1, 0xFFFFFFFFL);
		assertEquals("ID(1), Payload(0xFFFFFFFF), encoded_size == 6", tag.getEncodedSize(), 6);

		tag = new BACnetTL(TagClass.CONTEXT_SPECIFIC, 254, 0xFFFFFFFFL);
		assertEquals("ID(254), Payload(0xFFFFFFFF), encoded_size == 7", tag.getEncodedSize(), 7);
	}
}
