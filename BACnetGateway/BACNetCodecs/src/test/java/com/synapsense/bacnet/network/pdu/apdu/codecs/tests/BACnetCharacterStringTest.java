package com.synapsense.bacnet.network.pdu.apdu.codecs.tests;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import junit.framework.TestCase;

import com.synapsense.bacnet.network.pdu.apdu.codecs.primitives.BACnetCharacterString;

public class BACnetCharacterStringTest extends TestCase {

	private void stringTester(String encodeable) throws IOException {
		BACnetCharacterString a = new BACnetCharacterString(1, encodeable);
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		a.encode(out);
		ByteArrayInputStream in = new ByteArrayInputStream(out.toByteArray());
		BACnetCharacterString b = new BACnetCharacterString(in, 1);
		assertTrue(a.getPayload().equals(b.getPayload()));
	}

	public void testZeroString() throws IOException {
		stringTester("");
	}

	public void test1byteString() throws IOException {
		stringTester("1");
	}

	public void testMultibyteString() throws IOException {
		stringTester("Hello world! 12345678900987654321");
	}

}
