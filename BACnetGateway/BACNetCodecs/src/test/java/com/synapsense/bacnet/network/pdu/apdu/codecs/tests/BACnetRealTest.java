package com.synapsense.bacnet.network.pdu.apdu.codecs.tests;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import com.synapsense.bacnet.network.pdu.apdu.codecs.primitives.BACnetReal;
import junit.framework.TestCase;

public class BACnetRealTest extends TestCase {
	private void PerformTest(float payload) throws IOException {
		BACnetReal value = new BACnetReal(1, payload);
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		value.encode(out);
		ByteArrayInputStream in = new ByteArrayInputStream(out.toByteArray());
		BACnetReal value2 = new BACnetReal(in, 1);
		assertEquals(value.getPayload(), value2.getPayload()); // ??? is it
															   // suitable for
															   // Reals
	}

	public void test0() throws IOException {
		PerformTest(0);
	}

	public void test1() throws IOException {
		PerformTest(1);
	}

	public void test_1() throws IOException {
		PerformTest(-1);
	}

	public void test1_1() throws IOException {
		PerformTest((float) 1.1);
	}

	public void test1e10() throws IOException {
		PerformTest((float) 1e10);
	}

	public void test1e_10() throws IOException {
		PerformTest((float) 1e-10);
	}

	public void testNan() throws IOException {
		PerformTest(Float.NaN);
	}

	public void test_1e10() throws IOException {
		PerformTest((float) -1e10);
	}

	public void test1123456_789() throws IOException {
		PerformTest((float) 123456.789);
	}

	public void test20_2_6() throws IOException {
		BACnetReal a = new BACnetReal(null, (float) 72.0);
		assertEquals("20.2.4 claims that such value must be encoded in 5 bytes", a.getEncodedSize(), 5);
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		a.encode(out);
		byte[] buffer = out.toByteArray();
		assertEquals("buffer[0] == 0x44", 0xFF & buffer[0], 0x44);
		assertEquals("buffer[1] == 0x42", 0xFF & buffer[1], 0x42);
		assertEquals("buffer[2] == 0x90", 0xFF & buffer[2], 0x90);
		assertEquals("buffer[3] == 0x00", 0xFF & buffer[3], 0x00);
		assertEquals("buffer[4] == 0x00", 0xFF & buffer[4], 0x00);
	}

	public void test20_2_15() throws IOException {
		BACnetReal a = new BACnetReal(0, (float) -33.3);
		assertEquals("20.2.15 claims that such value must be encoded in 5 bytes", a.getEncodedSize(), 5);
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		a.encode(out);
		byte[] buffer = out.toByteArray();
		assertEquals("buffer[0] == 0x0c", 0xFF & buffer[0], 0x0c);
		assertEquals("buffer[1] == 0xc2", 0xFF & buffer[1], 0xc2);
		assertEquals("buffer[2] == 0x05", 0xFF & buffer[2], 0x05);
		assertEquals("buffer[3] == 0x33", 0xFF & buffer[3], 0x33);
		assertEquals("buffer[4] == 0x33", 0xFF & buffer[4], 0x33);
	}

}
