package com.synapsense.bacnet.network.pdu.apdu.codecs.tests;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import com.synapsense.bacnet.network.pdu.apdu.codecs.primitives.BACnetDouble;

import junit.framework.TestCase;

public class BACnetDoubleTest extends TestCase {
	private void PerformTest(double payload) throws IOException {
		BACnetDouble value = new BACnetDouble(1, payload);
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		value.encode(out);
		ByteArrayInputStream in = new ByteArrayInputStream(out.toByteArray());
		BACnetDouble value2 = new BACnetDouble(in, 1);
		assertEquals(value.getPayload(), value2.getPayload()); // ??? is it
															   // suitable for
															   // Reals
	}

	public void test0() throws IOException {
		PerformTest(0);
	}

	public void test1() throws IOException {
		PerformTest(1);
	}

	public void test_1() throws IOException {
		PerformTest(-1);
	}

	public void test1_1() throws IOException {
		PerformTest((float) 1.1);
	}

	public void test1e10() throws IOException {
		PerformTest((float) 1e10);
	}

	public void test1e_10() throws IOException {
		PerformTest((float) 1e-10);
	}

	public void testNan() throws IOException {
		PerformTest(Float.NaN);
	}

	public void test_1e10() throws IOException {
		PerformTest((float) -1e10);
	}

	public void test1123456_789() throws IOException {
		PerformTest((float) 123456.789);
	}

	public void test20_2_7() throws IOException {
		BACnetDouble a = new BACnetDouble(null, 72.0);
		assertEquals("20.2.7 claims that such value must be encoded in 10 bytes", a.getEncodedSize(), 10);
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		a.encode(out);
		byte[] buffer = out.toByteArray();
		assertEquals("buffer[0] == 0x55", 0xFF & buffer[0], 0x55);
		assertEquals("buffer[1] == 0x08", 0xFF & buffer[1], 0x08);
		assertEquals("buffer[2] == 0x40", 0xFF & buffer[2], 0x40);
		assertEquals("buffer[3] == 0x52", 0xFF & buffer[3], 0x52);
		assertEquals("buffer[4] == 0x00", 0xFF & buffer[4], 0x00);
		assertEquals("buffer[5] == 0x00", 0xFF & buffer[5], 0x00);
		assertEquals("buffer[5] == 0x00", 0xFF & buffer[6], 0x00);
		assertEquals("buffer[5] == 0x00", 0xFF & buffer[7], 0x00);
	}

	public void test20_2_15() throws IOException {
		BACnetDouble a = new BACnetDouble(1, -33.3);
		assertEquals("20.2.7 claims that such value must be encoded in 10 bytes", a.getEncodedSize(), 10);
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		a.encode(out);
		byte[] buffer = out.toByteArray();
		assertEquals("buffer[0] == 0x1d", 0xFF & buffer[0], 0x1d);
		assertEquals("buffer[1] == 0x08", 0xFF & buffer[1], 0x08);
		assertEquals("buffer[2] == 0xc0", 0xFF & buffer[2], 0xc0);
		assertEquals("buffer[3] == 0x40", 0xFF & buffer[3], 0x40);
		assertEquals("buffer[4] == 0xa6", 0xFF & buffer[4], 0xa6);
		assertEquals("buffer[5] == 0x66", 0xFF & buffer[5], 0x66);
		assertEquals("buffer[5] == 0x66", 0xFF & buffer[6], 0x66);
		assertEquals("buffer[5] == 0x66", 0xFF & buffer[7], 0x66);
	}
}
