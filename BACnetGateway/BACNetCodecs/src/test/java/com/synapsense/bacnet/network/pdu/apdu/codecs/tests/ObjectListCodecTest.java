package com.synapsense.bacnet.network.pdu.apdu.codecs.tests;

import com.synapsense.bacnet.network.pdu.apdu.codecs.codecs.complex.ObjectListCodec;
import com.synapsense.bacnet.network.pdu.apdu.codecs.exceptions.DecodingException;
import com.synapsense.bacnet.network.pdu.apdu.codecs.exceptions.EncodingException;
import com.synapsense.bacnet.system.shared.BACnetObjectIdentifier;
import com.synapsense.bacnet.system.shared.BACnetObjectType;

import junit.framework.TestCase;

public class ObjectListCodecTest extends TestCase {
	public void testCreate3Objects() {
		BACnetObjectIdentifier list[] = new BACnetObjectIdentifier[3];
		list[0] = new BACnetObjectIdentifier(BACnetObjectType.DEVICE, 1);
		list[1] = new BACnetObjectIdentifier(BACnetObjectType.ANALOG_INPUT, 2);
		list[2] = new BACnetObjectIdentifier(BACnetObjectType.ANALOG_INPUT, 3);
		ObjectListCodec codec = new ObjectListCodec();
		byte[] result = null;
		try {
			result = codec.encode(3, list);

		} catch (EncodingException e) {
			e.printStackTrace();
			fail(e.toString());
		}

		try {
			Object a = codec.decode(result, 3);
			assertEquals(list[0].getInstanceNumber(), ((BACnetObjectIdentifier) ((Object[]) a)[0]).getInstanceNumber());
			assertEquals(list[0].getObjectType(), ((BACnetObjectIdentifier) ((Object[]) a)[0]).getObjectType());
			assertEquals(list[1].getInstanceNumber(), ((BACnetObjectIdentifier) ((Object[]) a)[1]).getInstanceNumber());
			assertEquals(list[1].getObjectType(), ((BACnetObjectIdentifier) ((Object[]) a)[1]).getObjectType());
			assertEquals(list[2].getInstanceNumber(), ((BACnetObjectIdentifier) ((Object[]) a)[2]).getInstanceNumber());
			assertEquals(list[2].getObjectType(), ((BACnetObjectIdentifier) ((Object[]) a)[2]).getObjectType());
		} catch (DecodingException e) {
			e.printStackTrace();
			//fail(e.toString());
		}
	}

	public void test0Objects() {
		BACnetObjectIdentifier list[] = new BACnetObjectIdentifier[0];
		ObjectListCodec codec = new ObjectListCodec();
		byte[] result = null;
		try {
			result = codec.encode(3, list);

		} catch (EncodingException e) {
			e.printStackTrace();
			fail(e.toString());
		}
		try {
			Object a = codec.decode(result, 3);
			assertEquals(((Object[]) a).length, 0);
		} catch (DecodingException e) {
			e.printStackTrace();
			//fail(e.toString());
		}
	}
}
