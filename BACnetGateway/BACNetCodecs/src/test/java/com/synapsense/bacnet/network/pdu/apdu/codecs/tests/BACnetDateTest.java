package com.synapsense.bacnet.network.pdu.apdu.codecs.tests;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.GregorianCalendar;

import junit.framework.TestCase;
import com.synapsense.bacnet.network.pdu.apdu.codecs.primitives.BACnetDate;
import com.synapsense.bacnet.system.shared.BACnetDateValue;

public class BACnetDateTest extends TestCase {

	public void test20_2_12() throws IOException {
		GregorianCalendar calendar = new GregorianCalendar(1991, Calendar.JANUARY, 24, 17, 35, 45);
		calendar.set(Calendar.MILLISECOND, 170);
		BACnetDate value = new BACnetDate(null, new BACnetDateValue(calendar, BACnetDateValue.MonthExtender.SIMPLE,
		        BACnetDateValue.DayOfMonthExtender.SIMPLE));
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		value.encode(out);
		byte[] buffer = out.toByteArray();
		assertEquals("20.2.12 claims that buffer must be encoded in 5 octets", buffer.length, 5);
		assertEquals("20.2.12 claims that buffer[0] == 0xA4", 0xFF & buffer[0], 0xA4);
		assertEquals("20.2.12 claims that buffer[1] == 0x5B", 0xFF & buffer[1], 0x5B);
		assertEquals("20.2.12 claims that buffer[2] == 0x01", 0xFF & buffer[2], 0x01);
		assertEquals("20.2.12 claims that buffer[3] == 0x18", 0xFF & buffer[3], 0x18);
		assertEquals("20.2.12 claims that buffer[0] == 0x04", 0xFF & buffer[4], 0x04);
	}

	public void test20_2_15() throws IOException {
		GregorianCalendar calendar = new GregorianCalendar(1991, Calendar.JANUARY, 24, 17, 35, 45);
		calendar.set(Calendar.MILLISECOND, 170);
		BACnetDate value = new BACnetDate(9, new BACnetDateValue(calendar, BACnetDateValue.MonthExtender.SIMPLE,
		        BACnetDateValue.DayOfMonthExtender.SIMPLE));
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		value.encode(out);
		byte[] buffer = out.toByteArray();
		assertEquals("20.2.15 claims that buffer must be encoded in 5 octets", buffer.length, 5);
		assertEquals("20.2.15 claims that buffer[0] == 0x9C", 0xFF & buffer[0], 0x9C);
		assertEquals("20.2.15 claims that buffer[1] == 0x5B", 0xFF & buffer[1], 0x5B);
		assertEquals("20.2.15 claims that buffer[2] == 0x01", 0xFF & buffer[2], 0x01);
		assertEquals("20.2.15 claims that buffer[3] == 0x18", 0xFF & buffer[3], 0x18);
		assertEquals("20.2.15 claims that buffer[0] == 0x04", 0xFF & buffer[4], 0x04);
	}
}
