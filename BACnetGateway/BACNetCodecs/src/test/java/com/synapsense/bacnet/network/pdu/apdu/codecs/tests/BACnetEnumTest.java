package com.synapsense.bacnet.network.pdu.apdu.codecs.tests;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import com.synapsense.bacnet.network.pdu.apdu.codecs.primitives.BACnetEnum;
import junit.framework.TestCase;

public class BACnetEnumTest extends TestCase {
	public void test20_2_11() throws IOException {
		BACnetEnum value = new BACnetEnum(null, (byte) 0);
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		value.encode(out);
		byte[] buffer = out.toByteArray();
		assertEquals("20.2.11 claims that buffer must be encoded in 2 octets", buffer.length, 2);
		assertEquals("20.2.11 claims that buffer[0] == 0x91", 0xFF & buffer[0], 0x91);
		assertEquals("20.2.11 claims that buffer[1] == 0x00", 0xFF & buffer[1], 0x00);
	}

	public void test20_2_15() throws IOException {
		BACnetEnum value = new BACnetEnum(9, (byte) 0);
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		value.encode(out);
		byte[] buffer = out.toByteArray();
		assertEquals("20.2.15 claims that buffer must be encoded in 2 octets", buffer.length, 2);
		assertEquals("20.2.15 claims that buffer[0] == 0x91", 0xFF & buffer[0], 0x99);
		assertEquals("20.2.15 claims that buffer[1] == 0x00", 0xFF & buffer[1], 0x00);
	}
}
