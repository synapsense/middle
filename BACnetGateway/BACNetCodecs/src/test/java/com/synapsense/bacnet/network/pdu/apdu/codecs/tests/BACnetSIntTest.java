package com.synapsense.bacnet.network.pdu.apdu.codecs.tests;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import com.synapsense.bacnet.network.pdu.apdu.codecs.primitives.BACnetSInt;
import junit.framework.TestCase;

public class BACnetSIntTest extends TestCase {
	private void PayloadTesting(int payload) throws IOException {
		BACnetSInt a1 = new BACnetSInt(1, payload);
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		a1.encode(out);
		ByteArrayInputStream in = new ByteArrayInputStream(out.toByteArray());
		BACnetSInt a2 = new BACnetSInt(in, 1);
		assertEquals(a1.getPayload(), a2.getPayload());
	}

	public void testZeroPayload() throws IOException {
		PayloadTesting(0);
	}

	public void testPayload1() throws IOException {
		PayloadTesting(1);
	}

	public void testPayload127() throws IOException {
		PayloadTesting(127);
	}

	public void testPayload255() throws IOException {
		PayloadTesting(255);
	}

	public void testPayload256() throws IOException {
		PayloadTesting(256);
	}

	public void testPayload1000() throws IOException {
		PayloadTesting(1000);
	}

	public void testPayload3456() throws IOException {
		PayloadTesting(3456);
	}

	public void testPayload65535() throws IOException {
		PayloadTesting(65535);
	}

	public void testPayload65536() throws IOException {
		PayloadTesting(65536);
	}

	public void testPayload1000000() throws IOException {
		PayloadTesting(1000000);
	}

	public void testPayload0x7FFFFFFF() throws IOException {
		PayloadTesting(0x7FFFFFFF);
	}

	public void testPayload_1() throws IOException {
		PayloadTesting(-1);
	}

	public void testPayload_2() throws IOException {
		PayloadTesting(-2);
	}

	public void testPayload_1000() throws IOException {
		PayloadTesting(-1000);
	}

	public void testPayload_256() throws IOException {
		PayloadTesting(-256);
	}

	public void testPayload_987654() throws IOException {
		PayloadTesting(-987654);
	}

	public void testPayload_0x80000000() throws IOException {
		PayloadTesting(0x80000000);
	}

	public void testBACnet20_2_5() throws IOException {
		BACnetSInt a = new BACnetSInt(null, 72);
		assertEquals("20.2.5 claims that such value must be encoded in 2 bytes", a.getEncodedSize(), 2);
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		a.encode(out);
		byte[] buffer = out.toByteArray();
		assertEquals("buffer[0] == 0x31", 0xFF & buffer[0], 0x31);
		assertEquals("buffer[1] == 0x48", 0xFF & buffer[1], 0x48);
	}

	public void testBACnet20_2_15_for_SINT() throws IOException {
		BACnetSInt a = new BACnetSInt(5, -72);
		assertEquals("20.2.15 claims that such value must be encoded in 2 bytes", a.getEncodedSize(), 2);
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		a.encode(out);
		byte[] buffer = out.toByteArray();
		assertEquals("buffer[0] == 0x59", 0xFF & buffer[0], 0x59);
		assertEquals("buffer[1] == 0xb8", 0xFF & buffer[1], 0xb8);
	}
}
