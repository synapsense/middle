package com.synapsense.bacnet.network.pdu.apdu.codecs.tests;

import java.io.IOException;

import com.synapsense.bacnet.network.pdu.apdu.codecs.PrimitivesDecoder;
import com.synapsense.bacnet.network.pdu.apdu.codecs.PrimitivesEncoder;
import com.synapsense.bacnet.network.pdu.apdu.codecs.exceptions.NotResolvedPayloadException;

import junit.extensions.TestDecorator;
import junit.framework.TestCase;

public class PrimitivesDecodeAny extends TestCase {
	public void testOnlyAnyPayload() {
		final String payload = "test";
		PrimitivesEncoder enc = new PrimitivesEncoder();
		try {
			enc.encodeCharacterString(null, payload);
		} catch (IOException e) {
			// never happen (in theory)
			e.printStackTrace();
			fail();
		}
		PrimitivesDecoder dc = new PrimitivesDecoder(enc.getEncodedBuffer());
		try {
			assertTrue(payload.equals(dc.decodeAny()));
		} catch (IOException e) {
			fail("not expected exception - NotResolvedPayloadException");
		} catch (NotResolvedPayloadException e) {
			fail("not expected exception - NotResolvedPayloadException");
		}
	}

	public void testAsFirst() {
		final String payload = "test";
		PrimitivesEncoder enc = new PrimitivesEncoder();
		try {
			enc.encodeCharacterString(null, payload);
			enc.encodeCharacterString(null, "some");

		} catch (IOException e) {
			// never happen (in theory)
			e.printStackTrace();
			fail();
		}
		PrimitivesDecoder dc = new PrimitivesDecoder(enc.getEncodedBuffer());
		try {
			assertTrue(payload.equals(dc.decodeAny()));
		} catch (IOException e) {
			fail("not expected exception - NotResolvedPayloadException");
		} catch (NotResolvedPayloadException e) {
			fail("not expected exception - NotResolvedPayloadException");
		}
	}

	public void testInMiddle() {
		final String payload = "test";
		final String payload2 = "test";
		PrimitivesEncoder enc = new PrimitivesEncoder();
		try {
			enc.encodeCharacterString(null, "really");
			enc.encodeCharacterString(null, payload);
			enc.encodeCharacterString(null, payload2);

		} catch (IOException e) {
			// never happen (in theory)
			e.printStackTrace();
			fail();
		}
		PrimitivesDecoder dc = new PrimitivesDecoder(enc.getEncodedBuffer());
		try {
			dc.decodeCharacterString(null);
			assertTrue(payload.equals(dc.decodeAny()));
			assertTrue(payload2.equals(dc.decodeCharacterString(null)));
		} catch (IOException e) {
			fail("not expected exception - NotResolvedPayloadException");
		} catch (NotResolvedPayloadException e) {
			fail("not expected exception - NotResolvedPayloadException");
		}
	}

	public void testUInt() {
		final int payload = 1234;
		PrimitivesEncoder enc = new PrimitivesEncoder();
		try {
			enc.encodeUInt(null, payload);
		} catch (IOException e) {
			// never happen (in theory)
			e.printStackTrace();
			fail();
		}
		PrimitivesDecoder dc = new PrimitivesDecoder(enc.getEncodedBuffer());
		try {
			assertTrue(new Integer(payload).toString().equals(dc.decodeAny()));
		} catch (IOException e) {
			fail("not expected exception - NotResolvedPayloadException");
		} catch (NotResolvedPayloadException e) {
			fail("not expected exception - NotResolvedPayloadException");
		}
	}

	public void testNotResolved() {
		PrimitivesEncoder enc = new PrimitivesEncoder();
		try {
			enc.encodeBitString(null, new boolean[] { true, false });
		} catch (IOException e) {
			e.printStackTrace();
			fail();
		}
	}

	public void testDecodeRaw() {
		PrimitivesEncoder enc = new PrimitivesEncoder();
		try {
			enc.encodeCharacterString(null, "1");
			enc.encodeCharacterString(null, "qwertyuiop");
			enc.encodeCharacterString(null, "2");
			PrimitivesDecoder dec = new PrimitivesDecoder(enc.getEncodedBuffer());
			dec.decodeCharacterString(null);
			byte[] encoded = dec.getTaggedRaw();
			dec.decodeCharacterString(null);
			PrimitivesDecoder dec2 = new PrimitivesDecoder(encoded);
			assertTrue(dec2.decodeCharacterString(null).equals("qwertyuiop"));
		} catch (IOException e) {
			fail();
		}
	}

	public static void main(String[] a) {
		new PrimitivesDecodeAny().testDecodeRaw();
	}
}
