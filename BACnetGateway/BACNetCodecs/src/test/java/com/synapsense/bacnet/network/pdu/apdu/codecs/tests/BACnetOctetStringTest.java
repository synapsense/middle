package com.synapsense.bacnet.network.pdu.apdu.codecs.tests;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import com.synapsense.bacnet.network.pdu.apdu.codecs.primitives.BACnetOctetString;

import junit.framework.*;

public class BACnetOctetStringTest extends TestCase {
	public void test20_2_8() throws IOException {
		byte[] payload = { 0x12, 0x34, (byte) 0xFF };
		BACnetOctetString a = new BACnetOctetString(null, payload);
		assertEquals("according 20.2.8  encoded len == 4", a.getEncodedSize(), 4);

		ByteArrayOutputStream out = new ByteArrayOutputStream();
		a.encode(out);
		byte[] buffer = out.toByteArray();
		assertEquals("according 20.2.8  buffer[0] == 0x63", 0xFF & buffer[0], 0x63);
		assertEquals("according 20.2.8  buffer[1] == 0x12", 0xFF & buffer[1], 0x12);
		assertEquals("according 20.2.8  buffer[2] == 0x34", 0xFF & buffer[2], 0x34);
		assertEquals("according 20.2.8  buffer[3] == 0xff", 0xFF & buffer[3], 0xFF);
	}

	public void test20_2_15() throws IOException {
		byte[] payload = { 0x43, 0x21 };
		BACnetOctetString a = new BACnetOctetString(1, payload);
		assertEquals("according 20.2.15  encoded len == 3", a.getEncodedSize(), 3);

		ByteArrayOutputStream out = new ByteArrayOutputStream();
		a.encode(out);
		byte[] buffer = out.toByteArray();
		assertEquals("according 20.2.15  buffer[0] == 0x1A", 0xFF & buffer[0], 0x1A);
		assertEquals("according 20.2.15  buffer[1] == 0x43", 0xFF & buffer[1], 0x43);
		assertEquals("according 20.2.15  buffer[2] == 0x21", 0xFF & buffer[2], 0x21);
	}

}
