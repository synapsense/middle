package com.synapsense.bacnet.network.pdu.apdu.codecs.tests;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import com.synapsense.bacnet.network.pdu.apdu.codecs.primitives.BACnetSequence;

import junit.framework.TestCase;

public class BACnetSequenceTest extends TestCase {
	public void test20_2_16_Start() throws IOException {
		BACnetSequence a = new BACnetSequence(0, BACnetSequence.Actions.SEQUENCE_START);
		assertEquals("20.2.16 claims that such value must be encoded in 1 bytes", a.getEncodedSize(), 1);
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		a.encode(out);
		byte[] buffer = out.toByteArray();
		assertEquals("buffer[0] == 0x0E", 0xFF & buffer[0], 0x0E);
		ByteArrayInputStream in = new ByteArrayInputStream(buffer);
		BACnetSequence b = new BACnetSequence(in, 0);
		assertEquals("encoded.payload == decoded.payload", a.getPayload(), b.getPayload());
	}

	public void testBigID_Start() throws IOException {
		BACnetSequence a = new BACnetSequence(201, BACnetSequence.Actions.SEQUENCE_START);
		assertEquals("such value must be encoded in 2 bytes", a.getEncodedSize(), 2);
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		a.encode(out);
		byte[] buffer = out.toByteArray();
		assertEquals("buffer[0] == 0xFE", 0xFF & buffer[0], 0xFE);
		assertEquals("buffer[1] == 0xc9", 0xFF & buffer[1], 0xc9);
		ByteArrayInputStream in = new ByteArrayInputStream(buffer);
		BACnetSequence b = new BACnetSequence(in, 201);
		assertEquals("encoded.payload == decoded.payload", a.getPayload(), b.getPayload());
	}

	public void test20_2_16_Stop() throws IOException {
		BACnetSequence a = new BACnetSequence(0, BACnetSequence.Actions.SEQUENCE_STOP);
		assertEquals("20.2.16 claims that such value must be encoded in 1 bytes", a.getEncodedSize(), 1);
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		a.encode(out);
		byte[] buffer = out.toByteArray();
		assertEquals("buffer[0] == 0x0F", 0xFF & buffer[0], 0x0F);
		ByteArrayInputStream in = new ByteArrayInputStream(buffer);
		BACnetSequence b = new BACnetSequence(in, 0);
		assertEquals("encoded.payload == decoded.payload", a.getPayload(), b.getPayload());
	}

	public void testBigID_Stop() throws IOException {
		BACnetSequence a = new BACnetSequence(201, BACnetSequence.Actions.SEQUENCE_STOP);
		assertEquals("such value must be encoded in 2 bytes", a.getEncodedSize(), 2);
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		a.encode(out);
		byte[] buffer = out.toByteArray();
		assertEquals("buffer[0] == 0xFF", 0xFF & buffer[0], 0xFF);
		assertEquals("buffer[1] == 0xc9", 0xFF & buffer[1], 0xc9);
		ByteArrayInputStream in = new ByteArrayInputStream(buffer);
		BACnetSequence b = new BACnetSequence(in, 201);
		assertEquals("encoded.payload == decoded.payload", a.getPayload(), b.getPayload());
	}
}
