package com.synapsense.bacnet.network.pdu.apdu.codecs.tests;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.GregorianCalendar;

import com.synapsense.bacnet.network.pdu.apdu.codecs.primitives.BACnetTime;
import com.synapsense.bacnet.system.shared.BACnetTimeValue;

import junit.framework.TestCase;

public class BACnetTimeTest extends TestCase {
	public void test20_2_13() throws IOException {
		GregorianCalendar calendar = new GregorianCalendar(1991, Calendar.JANUARY, 24, 17, 35, 45);
		calendar.set(Calendar.MILLISECOND, 170);
		BACnetTime value = new BACnetTime(null, new BACnetTimeValue(calendar));
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		value.encode(out);
		byte[] buffer = out.toByteArray();
		assertEquals("20.2.13 claims that buffer must be encoded in 5 octets", buffer.length, 5);
		assertEquals("20.2.13 claims that buffer[0] == 0xB4", 0xFF & buffer[0], 0xB4);
		assertEquals("20.2.13 claims that buffer[1] == 0x11", 0xFF & buffer[1], 0x11);
		assertEquals("20.2.13 claims that buffer[2] == 0x23", 0xFF & buffer[2], 0x23);
		assertEquals("20.2.13 claims that buffer[3] == 0x2D", 0xFF & buffer[3], 0x2D);
		assertEquals("20.2.13 claims that buffer[0] == 0x11", 0xFF & buffer[4], 0x11);
	}

	public void test20_2_15() throws IOException {
		GregorianCalendar calendar = new GregorianCalendar(1991, Calendar.JANUARY, 24, 17, 35, 45);
		calendar.set(Calendar.MILLISECOND, 170);
		BACnetTime value = new BACnetTime(4, new BACnetTimeValue(calendar));
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		value.encode(out);
		byte[] buffer = out.toByteArray();
		assertEquals("20.2.15 claims that buffer must be encoded in 5 octets", buffer.length, 5);
		assertEquals("20.2.15 claims that buffer[0] == 0x4C", 0xFF & buffer[0], 0x4C);
		assertEquals("20.2.13 claims that buffer[1] == 0x11", 0xFF & buffer[1], 0x11);
		assertEquals("20.2.13 claims that buffer[2] == 0x23", 0xFF & buffer[2], 0x23);
		assertEquals("20.2.13 claims that buffer[3] == 0x2D", 0xFF & buffer[3], 0x2D);
		assertEquals("20.2.13 claims that buffer[0] == 0x11", 0xFF & buffer[4], 0x11);
	}
}
