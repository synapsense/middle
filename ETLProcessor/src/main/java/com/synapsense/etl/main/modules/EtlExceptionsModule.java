package com.synapsense.etl.main.modules;

import com.google.inject.AbstractModule;
import com.google.inject.name.Names;
import com.synapsense.dto.AlertType;
import com.synapsense.etl.EtlApplication;
import com.synapsense.etl.configuration.ConfigSource;
import com.synapsense.etl.configuration.Configuration;
import com.synapsense.etl.configuration.FlatFileConfigSource;
import com.synapsense.etl.main.providers.AlertServiceProvider;
import com.synapsense.service.AlertService;

public class EtlExceptionsModule extends AbstractModule {

	@Override
	protected void configure() {
		ConfigSource mainApp = new FlatFileConfigSource("internal", "conf/internal.properties");
		Configuration p = mainApp.getConfiguration();

		String typeName = p.getOption("alert.type");
		String priority = p.getOption("alert.priority");
		String descr = p.getOption("alert.description");
		String header = p.getOption("alert.header");
		String body = p.getOption("alert.body");

		AlertType at = new AlertType(typeName, typeName, descr, priority, header, body, true, true, true);

		// StopEtLHandler init
		bind(EtlApplication.class).annotatedWith(Names.named("MainApp")).to(EtlApplication.class);

		// RaiseAlertHandler init
		bind(AlertService.class).annotatedWith(Names.named("AlertService")).toProvider(AlertServiceProvider.class);
		bind(AlertType.class).annotatedWith(Names.named("EtlAlertType")).toInstance(at);

	}

}
