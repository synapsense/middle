package com.synapsense.etl.main.providers;

import com.google.inject.Provider;
import com.synapsense.etl.core.EtlTaskGenerator;

public class EtlTaskGeneratorProvider implements Provider<EtlTaskGenerator> {

	private EtlTaskGenerator taskGen;

	public EtlTaskGeneratorProvider(EtlTaskGenerator taskGen) {
		set(taskGen);
	}

	public synchronized void set(EtlTaskGenerator taskGen) {
		this.taskGen = taskGen;
	}

	@Override
	public synchronized EtlTaskGenerator get() {
		return taskGen;
	}

}
