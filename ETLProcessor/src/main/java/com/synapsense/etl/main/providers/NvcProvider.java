package com.synapsense.etl.main.providers;

import java.util.Properties;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.synapsense.service.nsvc.NotificationService;
import com.synapsense.transport.InterfaceNotSupportedException;
import com.synapsense.transport.NotConnectedException;
import com.synapsense.transport.Proxy;
import com.synapsense.transport.TransportManager;

public final class NvcProvider extends ESAnyServiceProvider<NotificationService> {

	@Inject
	NvcProvider(@Named("ES_HOST") String host, @Named("ES_PORT") int port, @Named("ES_LOGIN") String login,
	        @Named("ES_PASSWORD") String password) {
		super(host, port, login, password);
	}

	@Override
	protected Class<NotificationService> getServiceClass() {
		return NotificationService.class;
	}

	@Override
	protected String getServiceName() {
		return "NotificationService";
	}

	@Override
	protected NotificationService getProxy(InitialContext ctx) {
		try {
			Properties p = (Properties) ctx.getEnvironment();
			Proxy<NotificationService> proxy = TransportManager.getProxy(p, NotificationService.class);
			proxy.addListener(new NsvcLifeCycleListener());
			return proxy.getService();
		} catch (InterfaceNotSupportedException e) {
			throw new IllegalStateException("Error acquiring reference to " + getServiceName(), e);
		} catch (NamingException e) {
			throw new IllegalStateException("Error acquiring reference to " + getServiceName(), e);
		}
	}

	private class NsvcLifeCycleListener implements Proxy.LifecycleListener {

		@Override
		public void disconnected(Proxy<?> proxy) {
			try {
				proxy.connect();
			} catch (NotConnectedException e) {
				// log.warn("Unable to connect to Notification service", e);
			}
		}

		@Override
		public void connected(Proxy<?> proxy) {
		}
	}

}
