package com.synapsense.etl.main;

import java.util.Arrays;

import org.apache.log4j.Logger;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.synapsense.etl.EtlApplication;
import com.synapsense.etl.StdOutErrLog;
import com.synapsense.etl.exceptions.Exceptions;
import com.synapsense.etl.exceptions.OnErrorHandler;
import com.synapsense.etl.exceptions.RaiseAlertHandler;
import com.synapsense.etl.main.modules.EtlApplicationModule;
import com.synapsense.etl.main.modules.EtlConfigurationModule;
import com.synapsense.etl.main.modules.EtlExceptionsModule;

/**
 * Application context
 * 
 * @author shabanov
 * 
 * 
 */
public class EtlMain {
	private static Logger logger = Logger.getLogger(EtlApplication.class);

	private static EtlApplication app;
	private static Object lock = new Object();

	static {
		StdOutErrLog.tieSystemOutAndErrToLog();
	}

	public static void main(String[] args) {

		logger.info("Running ETL service");

		Injector injector = Guice.createInjector(new EtlConfigurationModule(), new EtlExceptionsModule(),
		        new EtlApplicationModule());

		logger.info("Registering ETL error handlers");

		OnErrorHandler raiseAlert = injector.getInstance(RaiseAlertHandler.class);
		// report all exceptions to alert service
		Exceptions.addAction(".*Exception", Arrays.asList(raiseAlert));

		app = injector.getInstance(EtlApplication.class);

		app.start();
		logger.info("ETL service was started");
		try {
			synchronized (lock) {
				lock.wait();
			}
		} catch (InterruptedException e) {
			// This exception is by design and not logged
		}
	}

	public static void stop() {
		logger.info("Stopping ETL service");
		app.close();
		logger.info("ETL service was stopped");

		synchronized (lock) {
			lock.notifyAll();
		}

		System.exit(0);
	}
}
