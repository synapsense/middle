package com.synapsense.etl.main.providers;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.synapsense.service.Environment;

public final class EnvProvider extends ESAnyServiceProvider<Environment> {

	@Inject
	EnvProvider(@Named("ES_HOST") String host, @Named("ES_PORT") int port, @Named("ES_LOGIN") String login,
	        @Named("ES_PASSWORD") String password) {
		super(host, port, login, password);
	}

	@Override
	protected Class<Environment> getServiceClass() {
		return Environment.class;
	}

	@Override
	protected String getServiceName() {
		return "Environment";
	}

}
