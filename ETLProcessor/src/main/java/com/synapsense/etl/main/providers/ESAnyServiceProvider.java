package com.synapsense.etl.main.providers;

import com.google.inject.Provider;
import com.synapsense.util.ServerLoginModule;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.util.Properties;

public abstract class ESAnyServiceProvider<T> implements Provider<T> {
    private InitialContext ctx;

    private String user;
    private String password;
    private Properties environment;

    protected ESAnyServiceProvider(String host, int port, String login, String password) {
        this.user = login;
        this.password = password;

        environment = new Properties();
        environment.put(Context.INITIAL_CONTEXT_FACTORY, "org.jboss.naming.remote.client.InitialContextFactory");
        environment.put(Context.PROVIDER_URL, "remote://" + host + ":" + port);
        environment.put("jboss.naming.client.ejb.context", "true");
        environment.put("jboss.naming.client.connect.options.org.xnio.Options.SASL_POLICY_NOPLAINTEXT", "false");
        environment.put("com.synapsense.transport", "com.synapsense.transport.rmi.AsyncRmiClientTransport");
        environment.put("com.synapsense.nsvc.transport.rmi.AsyncRmiClientTransport.RmiRegistryHost", host);

        environment.put(Context.SECURITY_PRINCIPAL, login);
        environment.put(Context.SECURITY_CREDENTIALS, password);
    }

    @Override
    public T get() {
        try {
            ctx = new InitialContext(environment);
        } catch (NamingException e) {
            throw new IllegalStateException("Cannot create initial context to connect ES services", e);
        }

        T service = getProxy(ctx);

        return service;
    }

    protected abstract Class<T> getServiceClass();

    protected abstract String getServiceName();

    protected T getProxy(InitialContext ctx) {
        try {
            final StringBuilder jndiName = new StringBuilder();
            jndiName.append("es-ear/").append("es-core").append("/").append("/").append(getServiceName()).append('!').append(getServiceClass().getName());
            T service = ServerLoginModule.getProxy(ctx, jndiName.toString(), getServiceClass());
            return service;
        } catch (NamingException e) {
            throw new IllegalStateException("Error acquiring reference to " + getServiceName(), e);
        }

    }

}
