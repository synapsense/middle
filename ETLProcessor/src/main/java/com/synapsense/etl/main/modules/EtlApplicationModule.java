package com.synapsense.etl.main.modules;

import com.google.inject.AbstractModule;
import com.google.inject.name.Names;
import com.synapsense.etl.configuration.AbstractConfigSource;
import com.synapsense.etl.configuration.ConfigurationProvider;
import com.synapsense.etl.configuration.ConfigurationProviderImpl;
import com.synapsense.etl.configuration.FlatFileConfigSource;
import com.synapsense.etl.main.providers.EnvProvider;
import com.synapsense.etl.main.providers.NvcProvider;
import com.synapsense.etl.main.providers.SchedulerServiceProvider;
import com.synapsense.etl.scheduler.EtlScheduler;
import com.synapsense.etl.scheduler.SchedulerService;
import com.synapsense.service.Environment;
import com.synapsense.service.nsvc.NotificationService;

public class EtlApplicationModule extends AbstractModule {

	@Override
	protected void configure() {
		AbstractConfigSource etlDefault = new FlatFileConfigSource("etl", "conf/etl.properties");

		// ESConfigSource
		bind(NotificationService.class).annotatedWith(Names.named("ES notifier")).toProvider(NvcProvider.class);
		bind(Environment.class).annotatedWith(Names.named("Environment")).toProvider(EnvProvider.class);
		bind(AbstractConfigSource.class).annotatedWith(Names.named("ETL default configuration")).toInstance(etlDefault);

		// EtlScheduler
		bind(org.quartz.Scheduler.class).toProvider(SchedulerServiceProvider.class);

		// ETL Application
		bind(ConfigurationProvider.class).annotatedWith(Names.named("Application configuration")).to(
		        ConfigurationProviderImpl.class);

		bind(SchedulerService.class).to(EtlScheduler.class);
	}
}
