package com.synapsense.etl.main.providers;

import com.google.inject.Provider;
import com.synapsense.etl.core.EtlLog;
import com.synapsense.etl.entities.EtlSessionLog;

public class EtlLogProvider implements Provider<EtlLog> {

	private EtlLog log;

	public EtlLogProvider(EtlLog log) {
		setLog(log);
	}

	public synchronized void setLog(EtlLog log) {
		this.log = new SilentDbLog(log);
	}

	@Override
	public synchronized EtlLog get() {
		return log;
	}

	public static class SilentDbLog implements EtlLog {

		private EtlLog log;

		public SilentDbLog(EtlLog log) {
			this.log = log;
		}

		public EtlSessionLog getLastImported() {
			return log.getLastImported();
		}

		public EtlSessionLog getFirstImported() {
			return log.getFirstImported();
		}

		public int getOverallObjectsNumber() {
			return log.getOverallObjectsNumber();
		}

		public void log(EtlSessionLog scriptLog) {
		}

		public void checkTargetDb() {
			log.checkTargetDb();
		}

		public void checkSourceDb() {
			log.checkSourceDb();
		}
	}

}
