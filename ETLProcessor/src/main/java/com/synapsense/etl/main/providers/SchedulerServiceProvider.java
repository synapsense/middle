package com.synapsense.etl.main.providers;

import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.impl.StdSchedulerFactory;

import com.google.inject.Provider;

public class SchedulerServiceProvider implements Provider<Scheduler> {

	private StdSchedulerFactory factory;

	private Scheduler scheduler;

	public SchedulerServiceProvider() {
		try {
			factory = new StdSchedulerFactory("conf/quartz.properties");
			scheduler = factory.getScheduler();
			scheduler.start();
		} catch (SchedulerException e) {
			throw new IllegalStateException("Scheduler service cannot be initialized", e);
		}
	}

	@Override
	public Scheduler get() {
		return scheduler;
	}

}
