package com.synapsense.etl.main.modules;

import com.google.inject.AbstractModule;
import com.google.inject.multibindings.Multibinder;
import com.google.inject.name.Names;
import com.synapsense.etl.configuration.ConfigSource;
import com.synapsense.etl.configuration.Configuration;
import com.synapsense.etl.configuration.ESConfigSource;
import com.synapsense.etl.configuration.FlatFileConfigSource;

public class EtlConfigurationModule extends AbstractModule {

	@Override
	protected void configure() {
		ConfigSource mainApp = new FlatFileConfigSource("internal", "conf/internal.properties");
		Configuration p = mainApp.getConfiguration();

		// pass ES login info to all components require this info
		bind(String.class).annotatedWith(Names.named("ES_HOST")).toInstance(p.getOption("ES_HOST"));
		bind(String.class).annotatedWith(Names.named("ES_PORT")).toInstance(p.getOption("ES_PORT"));
		bind(String.class).annotatedWith(Names.named("ES_LOGIN")).toInstance(p.getOption("ES_LOGIN"));
		bind(String.class).annotatedWith(Names.named("ES_PASSWORD")).toInstance(p.getOption("ES_PASSWORD"));

		Multibinder<ConfigSource> multibinder = Multibinder.newSetBinder(binder(), ConfigSource.class);
		multibinder.addBinding().to(ESConfigSource.class);
		multibinder.addBinding().toInstance(mainApp);

	}

}
