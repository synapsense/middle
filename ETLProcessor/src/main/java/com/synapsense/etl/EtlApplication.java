package com.synapsense.etl;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import com.synapsense.etl.configuration.*;
import com.synapsense.etl.core.*;
import com.synapsense.etl.exceptions.Exceptions;
import com.synapsense.etl.main.providers.EtlLogProvider;
import com.synapsense.etl.main.providers.EtlTaskGeneratorProvider;
import com.synapsense.etl.scheduler.SchedulerService;
import com.synapsense.etl.vendor.EtlUrlResolver;
import com.synapsense.util.LocalizationUtils;
import org.apache.log4j.Logger;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Singleton
public class EtlApplication {
	private static Logger logger = Logger.getLogger(EtlApplication.class);

	private static final String CURRENT_ETL = "Current ETL";
	private static final String BACKGROUND_ETL = "Background ETL";

	private ConfigurationProvider configProvider;
	private SchedulerService scheduler;

	private CommandManager<Script> etlManager;

	private EtlLogProvider etlLog;
	private EtlTaskGeneratorProvider etlTaskGen;

	@Inject
	public EtlApplication(@Named("Application configuration") ConfigurationProvider configProvider,
	        SchedulerService scheduler) {
		this.configProvider = configProvider;
		this.scheduler = scheduler;
	}

	public void start() {
		logger.info("Starting ETL application...");
		logger.info("Configuration sources installed : " + configProvider.listConfigurations());

		Configuration appProperties = null;
		try {
			appProperties = configProvider.getConfiguration("internal");
		} catch (ConfigurationException e) {
			logger.error("Cannot start ETL processor without internal configuration", e);
			close();
			System.exit(1);
		}

		final Configuration appFinalRef = appProperties;

		final CommandManager<Command> configLoadManager = new EtlConfigurationManger();
		configLoadManager.add(new Command() {
			@Override
			public int priority() {
				return 0;
			}

			@Override
			public void execute() {
				try {
					logger.info("Loading etl configuration...");
					Configuration etlProperties = configProvider.getConfiguration("etl");
					LoadEtlConfigurationAction loadConfigAction = new LoadEtlConfigurationAction(appFinalRef
					        .merge(etlProperties));
					loadConfigAction.load();
					configProvider.enableAutoReload("etl", loadConfigAction);
					configLoadManager.shutdown();
					logger.info("Etl configuration is successfully loaded");
				} catch (Exception e) {
					logger.warn("Cannot load configuration,  will try again in 1 minute" , e);
					configLoadManager.add(new Sleep(60 * 1000));
					configLoadManager.add(this);
				}
			}
		});

		startInAnotherThread(configLoadManager, "ETL-config loader");

		logger.info("ETL application was started");
	}

	public void close() {
		logger.info("Closing ETL application...");
		if (etlManager != null)
			etlManager.shutdown();
		if (scheduler != null)
			scheduler.shutdown();
	}

	private void startInAnotherThread(Runnable thread, String threadName) {
		Thread newThread = new Thread(thread, threadName);
		newThread.setDaemon(true);
		newThread.start();
	}

	private CommandManager<Script> loadEtlManager(EtlLogProvider etlLog) {
		final CommandManager<Script> etlManager = new EtlManager(etlLog);
		etlManager.add(new Loop(etlManager, new Sleep(5000)));
		return etlManager;
	}

	private class LoadEtlConfigurationAction implements ReloadCallback {
		private volatile boolean loaded = false;
		private DateFormat etlLoadFromFormat = new SimpleDateFormat("yyyy-MM-dd");
		private DateFormat etlOnDemandFormat = new SimpleDateFormat("yyyy-MM-dd H:m:s");

		private Configuration loadedConfiguration;

		public LoadEtlConfigurationAction(Configuration loadedConfiguration) {
			this.loadedConfiguration = loadedConfiguration;
		}

		@Override
		public synchronized void reload(Configuration configuration) {
			logger.info("Reloading ETL process configuration...");
			if (!loaded) {
				logger.info("Configuration is not loaded yet. Trying to load again...");
				loadedConfiguration.merge(configuration);
				load();
				return;
			}

			if (configuration.equals(loadedConfiguration)) {
				logger.info("Configuration hasn't been changed , nothing to reload : " + configuration);
				return;
			}

			String sourceUrl = configuration.getOption("source.url");
			String oldSourceUrl = loadedConfiguration.getOption("source.url");
			String targetUrl = configuration.getOption("target.url");
			String oldTargetUrl = loadedConfiguration.getOption("target.url");
			String sourceLogin = configuration.getOption("source.user");
			String oldSourceLogin = loadedConfiguration.getOption("source.user");
			String targetLogin = configuration.getOption("target.user");
			String oldTargetLogin = loadedConfiguration.getOption("target.user");
			String sourcePassw = configuration.getOption("source.password");
			String oldSourcePassw = loadedConfiguration.getOption("source.password");
			String targetPassw = configuration.getOption("target.password");
			String oldTargetPassw = loadedConfiguration.getOption("target.password");

			EtlUrlResolver sourceUrlResolver = EtlUrlResolver.getInstance(sourceUrl, sourceLogin, sourcePassw);

			EtlUrlResolver targetUrlResolver = EtlUrlResolver.getInstance(targetUrl, targetLogin, targetPassw);

			if (!sourceUrl.equals(oldSourceUrl) || !targetUrl.equals(oldTargetUrl)
			        || !sourceLogin.equals(oldSourceLogin) || !targetLogin.equals(oldTargetLogin)
			        || !sourcePassw.equals(oldSourcePassw) || !targetPassw.equals(oldTargetPassw)) {
				EntityManagerFactory sourceFactory = Persistence.createEntityManagerFactory("etl_source",
				        sourceUrlResolver.getJPAOptions());
				EntityManagerFactory targetFactory = Persistence.createEntityManagerFactory("etl_target",
				        targetUrlResolver.getJPAOptions());
				EtlApplication.this.etlLog.setLog(new DbEtlLog(sourceFactory, targetFactory));
			}

			String dataDir = loadedConfiguration.getOption("data.dir");
			String dataDirShared = loadedConfiguration.getOption("data.dir.shared");

			logger.info("Updating current ETL process schedule...");
			String schedule = configuration.getOption("etl_schedule");
			String oldSchedule = loadedConfiguration.getOption("etl_schedule");
			if (!oldSchedule.equals(schedule)) {
				// scheduler.updateSchedule("Current ETL", schedule);
				scheduler.scheduleTask(CURRENT_ETL, new CurrentEtlProcess(etlTaskGen, etlManager,
				        EtlApplication.this.etlLog, targetUrlResolver.getBufferFile(dataDir, dataDirShared)), schedule);
				logger.info("Current ETL schedule is updated from : " + oldSchedule + " to : " + schedule);
			}

			String oldEtlOnDemandStart = loadedConfiguration.getOption("ondemand.load_from");
			String oldEtlOnDemandEnd = loadedConfiguration.getOption("ondemand.load_to");
			String oldEtlOnDemandWhenToStart = loadedConfiguration.getOption("ondemand.start_at");

			String etlOnDemandStart = configuration.getOption("ondemand.load_from");
			String etlOnDemandEnd = configuration.getOption("ondemand.load_to");
			String etlOnDemandWhenToStart = configuration.getOption("ondemand.start_at");

			if (!oldEtlOnDemandStart.equals(etlOnDemandStart) || !oldEtlOnDemandEnd.equals(etlOnDemandEnd)
			        || !oldEtlOnDemandWhenToStart.equals(etlOnDemandWhenToStart)) {

				try {
					Date etlOnDemandStartDate = etlOnDemandFormat.parse(etlOnDemandStart);
					Date etlOnDemandEndDate = etlOnDemandFormat.parse(etlOnDemandEnd);
					Date etlOnDemandStartTime = new Date();
					if (etlOnDemandWhenToStart != null && !etlOnDemandWhenToStart.isEmpty()) {
						etlOnDemandStartTime = etlOnDemandFormat.parse(etlOnDemandWhenToStart);
					}

					if (etlOnDemandEndDate.after(etlOnDemandStartTime)) {
						logger.warn("On-demand ETL cannot be scheduled. ETL task start time {" + etlOnDemandStartTime
						        + "} should stand behind the end of ETL interval  {" + etlOnDemandEndDate + "}");
					} else {
						scheduler.scheduleTask("On-demand ETL", new CustomEtlProcess(etlTaskGen, etlManager, etlLog,
						        targetUrlResolver.getBufferFile(dataDir, dataDirShared), etlOnDemandStartDate,
						        etlOnDemandEndDate), etlOnDemandStartTime);
						logger.info("On-demand ETL is scheduled at : " + Utils.formatDate(etlOnDemandStartTime));
					}
				} catch (ParseException e) {
					logger.warn("Cannot reload on-demand etl task due to ", e);
				}
			}

			updateLoadedConfig(configuration);

			EtlApplication.this.etlTaskGen.set(new EtlTaskGenerator(loadedConfiguration));
		}

		public synchronized void load() {
			if (loaded) {
				logger.debug("Etl configuration is already loaded");
				return;
			}

			String sourceUrl = loadedConfiguration.getOption("source.url");
			if (sourceUrl == null) {
				Exceptions.throwUncheckedException(new ConfigurationException("source.url property is not defined",
				        LocalizationUtils.getLocalizedString("alert_message_source_url_is_undefined")));
			}

			String sourceLogin = loadedConfiguration.getOption("source.user");
			if (sourceLogin == null) {
				Exceptions.throwUncheckedException(new ConfigurationException("source.user property is not defined",
				        LocalizationUtils.getLocalizedString("alert_message_source_user_is_undefined")));
			}

			String sourcePassw = loadedConfiguration.getOption("source.password");

			String targetUrl = loadedConfiguration.getOption("target.url");
			if (targetUrl == null) {
				Exceptions.throwUncheckedException(new ConfigurationException("target.url property is not defined",
				        LocalizationUtils.getLocalizedString("alert_message_target_url_is_undefined")));
			}

			String targetLogin = loadedConfiguration.getOption("target.user");
			if (targetLogin == null) {
				Exceptions.throwUncheckedException(new ConfigurationException("target.user property is not defined",
				        LocalizationUtils.getLocalizedString("alert_message_target_user_is_undefined")));
			}

			String targetPassw = loadedConfiguration.getOption("target.password");

			// load etl log
			EtlUrlResolver sourceUrlResolver = EtlUrlResolver.getInstance(sourceUrl, sourceLogin, sourcePassw);
			EntityManagerFactory sourceFactory = Persistence.createEntityManagerFactory("etl_source",
			        sourceUrlResolver.getJPAOptions());

			// load etl log
			EtlUrlResolver targetUrlResolver = EtlUrlResolver.getInstance(targetUrl, targetLogin, targetPassw);

			EntityManagerFactory targetFactory = Persistence.createEntityManagerFactory("etl_target",
			        targetUrlResolver.getJPAOptions());

			EtlApplication.this.etlLog = new EtlLogProvider(new DbEtlLog(sourceFactory, targetFactory));

			loadedConfiguration.merge(new PropertyBasedConfiguration(targetUrlResolver.getEtlOptions()));
			EtlApplication.this.etlTaskGen = new EtlTaskGeneratorProvider(new EtlTaskGenerator(loadedConfiguration));

			etlManager = loadEtlManager(EtlApplication.this.etlLog);

			String dataDir = loadedConfiguration.getOption("data.dir");
			String dataDirShared = loadedConfiguration.getOption("data.dir.shared");

			// scheduling current etl process
			String currentEtlSchedule = loadedConfiguration.getOption("etl_schedule");
			scheduler.scheduleTask(CURRENT_ETL, new CurrentEtlProcess(etlTaskGen, etlManager,
			        EtlApplication.this.etlLog, targetUrlResolver.getBufferFile(dataDir, dataDirShared)),
			        currentEtlSchedule);
			logger.info("New current ETL schedule is " + currentEtlSchedule);

			// TODO : may be depreciated since introduced concept of custom etl
			// process
			// scheduling background etl process
			String chunk = loadedConfiguration.getOption("chunk");
			if (!Utils.validateChunkExpr(chunk)) {
				logger.warn("Not valid chunk option specified : " + chunk + ". default '1 hr' will be used");
				chunk = "1 hr";
			}

			String backgroundEtlSchedule = loadedConfiguration.getOption("background_etl_schedule");
			String loadDataFrom = loadedConfiguration.getOption("load_data_from");
			try {
				Date loadFrom = etlLoadFromFormat.parse(loadDataFrom);

				scheduler.scheduleTask(BACKGROUND_ETL, new BackgroundEtlProcess(etlTaskGen, etlManager, etlLog, chunk,
				        loadFrom), backgroundEtlSchedule);
				logger.info("New background ETL schedule is " + backgroundEtlSchedule);

			} catch (ParseException e) {
				logger.warn(
				        "Background ETL process is not started. Correct 'load_data_from' parameter. Unsupported date format for : "
				                + loadDataFrom);
			}

			String etlOnDemandStart = loadedConfiguration.getOption("ondemand.load_from");
			String etlOnDemandEnd = loadedConfiguration.getOption("ondemand.load_to");
			String etlOnDemandWhenToStart = loadedConfiguration.getOption("ondemand.start_at");
			// skip if no on-demand parameters passed
			if (etlOnDemandStart == null || etlOnDemandEnd == null) {
				logger.info("ETL on-demand process is not configured");
			} else {
				try {

					Date etlOnDemandStartDate = etlOnDemandFormat.parse(etlOnDemandStart);
					Date etlOnDemandEndDate = etlOnDemandFormat.parse(etlOnDemandEnd);

					Date etlOnDemandStartTime = new Date();
					if (etlOnDemandWhenToStart != null && !etlOnDemandWhenToStart.isEmpty()) {
						etlOnDemandStartTime = etlOnDemandFormat.parse(etlOnDemandWhenToStart);
					}
					// check if current time is still a high boundary for task
					// interval
					if (etlOnDemandEndDate.after(etlOnDemandStartTime)) {
						logger.warn("On-demand ETL cannot be scheduled. ETL task start time {" + etlOnDemandStartTime
						        + "} should stand behind end of ETL interval {" + etlOnDemandEndDate + "}");
					} else {
						scheduler.scheduleTask("On-demand ETL", new CustomEtlProcess(etlTaskGen, etlManager, etlLog,
						        targetUrlResolver.getBufferFile(dataDir, dataDirShared), etlOnDemandStartDate,
						        etlOnDemandEndDate), etlOnDemandStartTime);
						logger.info("On-demand ETL is scheduled at : " + Utils.formatDate(etlOnDemandStartTime));
					}
				} catch (ParseException e) {
					logger.warn(
					        "On-demand ETL process is not started. Correct 'ondemand.load_from','ondemand.load_to','ondemand.start_at' parameters. Unsupported date format for : "
					                + etlOnDemandStart + "," + etlOnDemandEnd + "," + etlOnDemandWhenToStart);
				}

			}

			startInAnotherThread(new Runnable() {
				@Override
				public void run() {
					etlManager.run();
				}
			}, "ETL-manager");

			logger.info("ETL configuration was successfully loaded");
			loaded = true;
		}

		/**
		 * Merge options from <code>config</code> parameter to loaded options
		 * 
		 * @param config
		 *            New options to merge
		 */
		private void updateLoadedConfig(Configuration config) {
			this.loadedConfiguration.merge(config);
		}

	}

}
