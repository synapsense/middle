package com.synapsense.etl;

public class Options {
	// internal
	public static String JNDI_URL = "JNDI_URL";
	public static String ES_LOGIN = "ES_LOGIN";
	public static String ES_PASSWORD = "ES_PASSWORD";

	public static String autorefresh = "autorefresh";
	public static String DATA_DIR = "data.dir";
	public static String DATA_DIR_SHARED = "data.dir.shared";
	public static String SCRIPT_DIR = "script.dir";
	public static String script_dimensions = "script.dimensions";
	public static String script_export_facts = "script.export.facts";
	public static String script_import_facts = "script.import.facts";
	public static String BACKGROUND_ETL_CHUNK = "chunk";

	public static String background_etl_schedule = "background_etl_schedule";
	public static String etl_max_single_interval = "etl.max_single_interval";
}
