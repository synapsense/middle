package com.synapsense.etl.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "METAINF")
public class MetaInf {

	@Id
	@Column(name = "etl_version")
	private Integer etlVersion;

	@Column(name = "etl_start")
	private Date etlRunDate;

	MetaInf() {
	}

	public MetaInf(Integer etlVersion, Date etlRunDate) {
		this.etlVersion = etlVersion;
		this.etlRunDate = etlRunDate;
	}

	public Integer getEtlVersion() {
		return etlVersion;
	}

	public void setEtlVersion(Integer etlVersion) {
		this.etlVersion = etlVersion;
	}

	public Date getEtlRunDate() {
		return etlRunDate;
	}

	public void setEtlRunDate(Date etlRunDate) {
		this.etlRunDate = etlRunDate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((etlRunDate == null) ? 0 : etlRunDate.hashCode());
		result = prime * result + ((etlVersion == null) ? 0 : etlVersion.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MetaInf other = (MetaInf) obj;
		if (etlRunDate == null) {
			if (other.etlRunDate != null)
				return false;
		} else if (!etlRunDate.equals(other.etlRunDate))
			return false;
		if (etlVersion == null) {
			if (other.etlVersion != null)
				return false;
		} else if (!etlVersion.equals(other.etlVersion))
			return false;
		return true;
	}

}
