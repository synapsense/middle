package com.synapsense.etl.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "ETL_LOG")
public class EtlSessionLog {
	public static EtlSessionLog NOTHING = new EtlSessionLog() {
		public String toString() {
			return "Empty ETL session";
		}
	};

	@Id
	@GenericGenerator(name = "generator", strategy = "native")
	@GeneratedValue(generator = "generator")
	private Integer id;

	@Column(name = "start_date")
	private Date startDate;

	@Column(name = "end_date")
	private Date endDate;

	@Column(name = "start_obj")
	private Integer startObject;

	@Column(name = "end_obj")
	private Integer endObject;

	public Integer getId() {
		return id;
	}

	EtlSessionLog() {
	}

	public EtlSessionLog(Date startDate, Date endDate, Integer starObject, Integer endObject) {
		this.startDate = startDate;
		this.endDate = endDate;
		this.startObject = starObject;
		this.endObject = endObject;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date start) {
		this.startDate = start;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date end) {
		this.endDate = end;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((endDate == null) ? 0 : endDate.hashCode());
		result = prime * result + ((endObject == null) ? 0 : endObject.hashCode());
		result = prime * result + ((startObject == null) ? 0 : startObject.hashCode());
		result = prime * result + ((startDate == null) ? 0 : startDate.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EtlSessionLog other = (EtlSessionLog) obj;
		if (endDate == null) {
			if (other.endDate != null)
				return false;
		} else if (!endDate.equals(other.endDate))
			return false;
		if (endObject == null) {
			if (other.endObject != null)
				return false;
		} else if (!endObject.equals(other.endObject))
			return false;
		if (startObject == null) {
			if (other.startObject != null)
				return false;
		} else if (!startObject.equals(other.startObject))
			return false;
		if (startDate == null) {
			if (other.startDate != null)
				return false;
		} else if (!startDate.equals(other.startDate))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "session interval [" + startDate + "," + endDate + "]; object range [" + startObject + ".." + endObject
		        + "]";
	}

	public Integer getStartObject() {
		return startObject;
	}

	public void setStartObject(Integer starObject) {
		this.startObject = starObject;
	}

	public Integer getEndObject() {
		return endObject;
	}

	public void setEndObject(Integer endObject) {
		this.endObject = endObject;
	}

}
