package com.synapsense.etl.script;

import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class TimeDimGenerator {

	public List<TimeRec> generate(Date start, Date end) {
		Calendar c = Calendar.getInstance();
		c.setTime(end);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
		int minute = c.get(Calendar.MINUTE);
		c.set(Calendar.MINUTE, minute / 5 * 5);
		// determine end of time interval
		Date lEnd = c.getTime();

		c.setTime(start);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);

		minute = c.get(Calendar.MINUTE);
		c.set(Calendar.MINUTE, minute / 5 * 5);

		List<TimeRec> result = new LinkedList<TimeDimGenerator.TimeRec>();
		while (lEnd.after(c.getTime())) {
			c.add(Calendar.MINUTE, 5);
			TimeRec rec = new TimeRec(c.getTime(), c.get(Calendar.YEAR), c.get(Calendar.MONTH) + 1,
			        c.get(Calendar.DAY_OF_MONTH), c.get(Calendar.HOUR_OF_DAY), c.get(Calendar.MINUTE));
			result.add(rec);
		}

		return result;
	}

	public static class TimeRec {
		public Date date;
		public int year;
		public int month;
		public int day;
		public int hour;
		public int minute;

		public TimeRec(Date date, int year, int month, int day, int hour, int minute) {
			this.date = date;
			this.year = year;
			this.month = month;
			this.day = day;
			this.hour = hour;
			this.minute = minute;
		}

		@Override
		public String toString() {
			return date.toString();
		}
	}
}
