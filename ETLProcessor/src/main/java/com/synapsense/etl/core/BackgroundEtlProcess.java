package com.synapsense.etl.core;

import java.util.Date;
import java.util.List;

import com.google.inject.Provider;
import com.synapsense.etl.entities.EtlSessionLog;

@Deprecated
public class BackgroundEtlProcess extends EtlProcess {

	@SuppressWarnings("unused")
	private String chunkSize;

	@SuppressWarnings("unused")
	private Date loadFrom;

	public BackgroundEtlProcess(Provider<EtlTaskGenerator> taskGen, CommandManager<Script> etlManager,
	        Provider<EtlLog> etlLog, String chunkSize, Date loadFrom) {
		super(taskGen, etlManager, etlLog, "background.csv", "background.csv");
		this.chunkSize = chunkSize;
		this.loadFrom = loadFrom;
	}

	@Override
	protected List<EtlSessionLog> provideDateInterval(int objectsNum, EtlSessionLog firstExport,
	        EtlSessionLog lastExport) {
		throw new UnsupportedOperationException("Deprecated");
	}

	/*
	 * @Override protected Pair<Date, Date> provideDateInterval(final Date
	 * firstExportDate, final Date lastExportDate) { Date end = firstExportDate;
	 * Date start = Utils.dateSub(end, chunkSize);
	 * 
	 * if (loadFrom.after(start)) { start = loadFrom; }
	 * 
	 * if (start.equals(end) || start.after(end)) {
	 * Exceptions.throwUncheckedException(new EtlProcessingException(
	 * "Wrong interval to ETL from date : " + start + " to " + end +
	 * ".Background ETL will be shutdown", true)); }
	 * 
	 * return Pair.pair(start, end); }
	 */
}