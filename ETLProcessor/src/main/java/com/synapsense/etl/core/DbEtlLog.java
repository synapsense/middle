package com.synapsense.etl.core;

import com.synapsense.etl.entities.EtlSessionLog;
import com.synapsense.etl.exceptions.Exceptions;
import com.synapsense.util.LocalizationUtils;
import org.apache.log4j.Logger;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;

public class DbEtlLog implements EtlLog {
    private static Logger logger = Logger.getLogger(EtlLog.class);

    private EntityManagerFactory emfSource;
    private EntityManagerFactory emfTarget;

    public DbEtlLog(EntityManagerFactory emfSource, EntityManagerFactory emfTarget) {
        this.emfSource = emfSource;
        this.emfTarget = emfTarget;
    }

    @Override
    public int getOverallObjectsNumber() {
        EntityManager em = emfSource.createEntityManager();
        try {
            Integer number = (Integer) em.createNativeQuery("select max(o.id) from objects as o").getSingleResult();
            return number == null ? 0 : number;
        } finally {
            em.close();
        }
    }

    @Override
    public void log(EtlSessionLog scriptLog) {
        logger.debug("Logging ETL session : " + scriptLog);
        EntityManager em = emfTarget.createEntityManager();
        try {
            em.getTransaction().begin();
            em.persist(scriptLog);
            em.getTransaction().commit();
        } finally {
            em.close();
        }
        logger.debug("ETL session has been successfully logged, " + scriptLog);
    }

    @Override
    public void checkTargetDb() {
        logger.debug("Checking target database availability...");
        EntityManager em = emfTarget.createEntityManager();
        try {
            em.createQuery("select count(item) from EtlSessionLog as item").getSingleResult();
        } catch (Exception e) {
            Exceptions.throwUncheckedException(new DatabaseAccessException(e.getMessage(), LocalizationUtils.getLocalizedString(
                    "alert_message_target_database_is_inaccessible", e.getMessage())));
        } finally {
            em.close();
        }
        logger.debug("Target database is available now");
    }

    @Override
    public void checkSourceDb() {
        logger.debug("Checking source database availability...");
        EntityManager em = emfSource.createEntityManager();
        try {
            em.createNativeQuery("select 1").getSingleResult();
        } catch (Exception e) {
            Exceptions.throwUncheckedException(new DatabaseAccessException(e.getMessage(), LocalizationUtils.getLocalizedString(
                    "alert_message_source_database_is_inaccessible", e.getMessage())));
        } finally {
            em.close();
        }
        logger.debug("Source database is available now");
    }

    @Override
    public EtlSessionLog getLastImported() {
        EntityManager em = emfTarget.createEntityManager();
        try {
            EtlSessionLog record = (EtlSessionLog) em
                    .createQuery(
                            "select item from EtlSessionLog as item where"
                                    + " item.endDate = (select max(item2.endDate) from EtlSessionLog as item2)"
                                    + " order by item.endObject desc").setMaxResults(1).getSingleResult();
            return record;
        } catch (NoResultException e) {
            return EtlSessionLog.NOTHING;
        } finally {
            em.close();
        }
    }

    @Override
    public EtlSessionLog getFirstImported() {
        EntityManager em = emfTarget.createEntityManager();
        try {
            EtlSessionLog record = (EtlSessionLog) em
                    .createQuery(
                            "select item from EtlSessionLog as item where"
                                    + " item.startDate = (select min(item2.startDate) from EtlSessionLog as item2)"
                                    + " order by item.startObject").setMaxResults(1).getSingleResult();
            return record;
        } catch (NoResultException e) {
            return EtlSessionLog.NOTHING;
        } finally {
            em.close();
        }
    }

}
