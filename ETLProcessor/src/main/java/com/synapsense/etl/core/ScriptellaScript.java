package com.synapsense.etl.core;

import java.net.URL;
import java.util.Map;

import org.apache.log4j.Logger;

import scriptella.execution.EtlExecutor;
import scriptella.execution.EtlExecutorException;
import scriptella.execution.ExecutionStatistics;
import scriptella.interactive.ProgressIndicator;

import com.synapsense.etl.entities.EtlSessionLog;
import com.synapsense.etl.exceptions.Exceptions;

public class ScriptellaScript extends BaseScript {

	private static Logger logger = Logger.getLogger(ScriptellaScript.class);

	public ScriptellaScript(URL url, Map<String, Object> properties) {
		super(url, properties);
	}

	@Override
	public EtlSessionLog run() {
		URL url = getURL();
		logger.trace("Looking up script url at : " + url);
		EtlExecutor etlExec = EtlExecutor.newExecutor(url, getProperties());
		logger.trace("Etl executor is ready to extract data");
		try {
			logger.trace("Extracting data...");
			ExecutionStatistics stat = etlExec.execute(new Progress());
			logger.trace("Etl executor reports : " + stat.toString());
		} catch (EtlExecutorException e) {
			Exceptions.throwUncheckedException(new EtlProcessingException(e.getMessage()));
		}

		return null;
	}

	@Override
	public String toString() {
		return "Basic scriptella script , located at " + getURL();
	}

	private static class Progress implements ProgressIndicator {

		@Override
		public void showProgress(double progress, String message) {
			logger.trace(message);
		}

	}

}
