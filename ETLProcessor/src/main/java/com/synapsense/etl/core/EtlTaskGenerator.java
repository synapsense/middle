package com.synapsense.etl.core;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import scriptella.util.IOUtils;

import com.synapsense.etl.configuration.Configuration;
import com.synapsense.etl.configuration.ConfigurationException;
import com.synapsense.etl.entities.EtlSessionLog;
import com.synapsense.etl.exceptions.Exceptions;
import com.synapsense.util.LocalizationUtils;

public class EtlTaskGenerator {
	private Configuration configuration;

	private URL dimensionsUrl;
	private URL exportUrl;
	private URL importUrl;

	private int maxSingleEtlInterval;

	public EtlTaskGenerator(Configuration configuration) {
		this.configuration = configuration;
		String dir = configuration.getOption("script.dir");
		try {
			dimensionsUrl = IOUtils.toUrl(new File(dir + "/" + configuration.getOption("script.dimensions")));
			exportUrl = IOUtils.toUrl(new File(dir + "/" + configuration.getOption("script.export.facts")));
			importUrl = IOUtils.toUrl(new File(dir + "/" + configuration.getOption("script.import.facts")));
		} catch (MalformedURLException e) {
			Exceptions.throwUncheckedException(new ConfigurationException("ETL scripts are not found." + e.getMessage(), LocalizationUtils.getLocalizedString(
			        "alert_message_not_resolved_urls_of_etl_scripts", e.getMessage())));
		}

		try {
			maxSingleEtlInterval = Integer.parseInt(configuration.getOption("etl.max_single_interval"));
		} catch (NumberFormatException e) {
			maxSingleEtlInterval = 5;
		}

	}

	public Script generate(final EtlSessionLog chunk, String bufferFileExport, String bufferFileImport, int chunkSize) {
		// preparing properties
		Map<String, Object> basic = new HashMap<String, Object>();
		// direct reflection of config file to script properties
		basic.putAll(configuration.listOptions());

		basic.put("file.tmp_fact_export", bufferFileExport);
		basic.put("file.tmp_fact_import", bufferFileImport);

		BaseScript dimScript = new ScriptellaScript(dimensionsUrl, basic);

		final List<Script> etlSequence = new LinkedList<Script>();
		etlSequence.add(dimScript);

		// determine interval in days
		long days = (-1 * (chunk.getStartDate().getTime() - chunk.getEndDate().getTime())) / (1000 * 60 * 60 * 24);

		// put given interval into properties
		basic.put("etl.startdate", chunk.getStartDate());
		basic.put("etl.enddate", chunk.getEndDate());

		// optimization , loop constant 5 can be exported to config
		if (days > 5) {
			Calendar c = Calendar.getInstance();
			c.setTime(chunk.getStartDate());
			// first interval will start from given start date
			// extract smaller intervals
			for (int j = maxSingleEtlInterval; j < (days + maxSingleEtlInterval); j += maxSingleEtlInterval) {
				// calculating end of next chunk
				c.add(Calendar.DAY_OF_MONTH, (int) (j > days ? (days - (j - maxSingleEtlInterval)) : j));
				// replace end of the interval
				basic.put("etl.enddate", c.getTime());
				etlSequence.addAll(chunkanize(basic, chunk.getStartObject(), chunk.getEndObject(), chunkSize));
				// move start of next chunk forward
				c.add(Calendar.SECOND, 1);
				// replace start of next interval
				basic.put("etl.startdate", c.getTime());
			}
		} else {
			// just one etl chunk should be generated
			etlSequence.addAll(chunkanize(basic, chunk.getStartObject(), chunk.getEndObject(), chunkSize));
		}

		// formula is one dimension script + date interval divided to
		// MAX_ETL_CHUNK_IN_DAYS * every object chunk of size=chunkSize * 2
		// (export and import scripts always go together)
		if (etlSequence.size() != 1 + (days / maxSingleEtlInterval + 1)
		        * (((chunk.getEndObject() - chunk.getStartObject()) / chunkSize) + 1) * 2) {
			throw new java.lang.IllegalStateException(
			        "Error in etl optimization algo , wrong scripts number generated (" + etlSequence.size() + ")");
		}

		return new EtlSessionScript(etlSequence, chunk);

	}

	private List<Script> chunkanize(final Map<String, Object> basic, int startObject, int endObject, int chunkSize) {
		final List<Script> etlSequence = new LinkedList<Script>();

		for (int i = startObject; i <= endObject; i += chunkSize) {
			Map<String, Object> nextChunkProps = new HashMap<String, Object>(basic);
			nextChunkProps.put("etl.startobject", i);
			int nextRangeStartNum = (i + chunkSize - 1);
			nextChunkProps.put("etl.endobject", nextRangeStartNum > endObject ? endObject : nextRangeStartNum);

			BaseScript etlExport = new ScriptellaScript(exportUrl, nextChunkProps);
			etlSequence.add(etlExport);

			BaseScript etlImport = new ScriptellaScript(importUrl, nextChunkProps);
			etlSequence.add(etlImport);
		}

		return etlSequence;
	}

	public static class EtlSessionScript implements Script {

		private List<Script> etlSequence;
		private EtlSessionLog chunk;

		public EtlSessionScript(List<Script> etlSequence, EtlSessionLog chunk) {
			this.etlSequence = etlSequence;
			this.chunk = chunk;
		}

		@Override
		public EtlSessionLog run() {
			for (Script script : etlSequence) {
				script.run();
			}
			return chunk;
		}

		@Override
		public String toString() {
			return "ETL session, " + chunk.toString();
		}
	}

}