package com.synapsense.etl.core;

import com.synapsense.etl.EtlApplicationException;

public class DatabaseAccessException extends EtlApplicationException {

	private static final long serialVersionUID = 3158872575295813893L;

	public DatabaseAccessException(String message, String macroizedMessage) {
		super(message, macroizedMessage);
	}

}
