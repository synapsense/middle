package com.synapsense.etl.core;

import com.synapsense.etl.entities.EtlSessionLog;

public interface Script {
	EtlSessionLog run();
}
