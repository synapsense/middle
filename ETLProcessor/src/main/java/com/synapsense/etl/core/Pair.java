package com.synapsense.etl.core;

public class Pair<F, S> {
	public F getFirst() {
		return f;
	}

	public S getSecond() {
		return s;
	}

	private Pair(F f, S s) {
		this.f = f;
		this.s = s;
	}

	public static <F, S> Pair<F, S> pair(F f, S s) {
		return new Pair<F, S>(f, s);
	}

	F f;
	S s;
}
