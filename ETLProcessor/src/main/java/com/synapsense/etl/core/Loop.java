package com.synapsense.etl.core;

import com.synapsense.etl.entities.EtlSessionLog;

public class Loop implements Command, Script {

	private CommandManager<Command> manager;
	private CommandManager<Script> scriptManager;

	private Command command;
	private Script script;

	public Loop(CommandManager<Command> manager, Command task) {
		this.manager = manager;
		this.command = task;
	}

	public Loop(CommandManager<Script> manager, Script task) {
		this.scriptManager = manager;
		this.script = task;
	}

	@Override
	public EtlSessionLog run() {
		script.run();
		scriptManager.add(this);
		return EtlSessionLog.NOTHING;
	}

	@Override
	public void execute() {
		command.execute();
		manager.add(this);
	}

	@Override
	public int priority() {
		return 0;
	}

	@Override
	public String toString() {
		return "Loop";
	}

}
