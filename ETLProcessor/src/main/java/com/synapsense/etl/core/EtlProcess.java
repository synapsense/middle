package com.synapsense.etl.core;

import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import com.google.inject.Provider;
import com.synapsense.etl.entities.EtlSessionLog;
import com.synapsense.etl.exceptions.Exceptions;
import com.synapsense.etl.scheduler.ScheduleTask;
import com.synapsense.util.LocalizationUtils;

public abstract class EtlProcess implements ScheduleTask {

	private static Logger logger = Logger.getLogger(EtlProcess.class);

	protected static final int objectsPerScript = 50;

	private String bufferFileExport;
	private String bufferFileImport;

	private Provider<EtlTaskGenerator> taskGen;
	private CommandManager<Script> etlManager;
	private Provider<EtlLog> etlLog;

	protected EtlProcess(Provider<EtlTaskGenerator> taskGen, CommandManager<Script> etlManager,
	        Provider<EtlLog> etlLog, String bufferFileExport, String bufferFileImport) {
		this.taskGen = taskGen;
		this.etlManager = etlManager;
		this.etlLog = etlLog;
		this.bufferFileExport = bufferFileExport;
		this.bufferFileImport = bufferFileImport;
	}

	public void run() {
		logger.info("Starting ETL process...");
		try {
			logger.info("Checking databases availability...");
			getLog().checkSourceDb();
			getLog().checkTargetDb();
		} catch (DatabaseAccessException e) {
			// exit if some of databases are inaccessible
			logger.warn("Cannot access database instance...", e);
			return;
		}

		int objectsNum = getLog().getOverallObjectsNumber();
		logger.trace("Overall objects in ES = " + objectsNum);

		EtlSessionLog recStart = getLog().getFirstImported();
		if (recStart.equals(EtlSessionLog.NOTHING)) {
			recStart = new EtlSessionLog(new Date(System.currentTimeMillis() - 1000), new Date(), 1, objectsNum);
		}

		EtlSessionLog recEnd = getLog().getLastImported();
		if (recEnd.equals(EtlSessionLog.NOTHING)) {
			recEnd = new EtlSessionLog(new Date(System.currentTimeMillis() - 1000), new Date(), 1, objectsNum);
		}

		if (recStart.getStartDate().after(recEnd.getEndDate())) {
			Exceptions.throwUncheckedException(new EtlProcessingException("Cannot execute ETL task. Seems that etl logs are corrupted", LocalizationUtils
			        .getLocalizedString("alert_message_can_not_execute_etl_task")));
			return;
		}

		// proposing date interval to etl, delegates to
		// descendants(#template_method)
		logger.trace("Getting etl chunks to perform...");
		List<EtlSessionLog> chunks = provideDateInterval(objectsNum, recStart, recEnd);

		if (chunks.isEmpty()) {
			logger.debug("Etl process returned no ETL chunks to perform");
			return;
		}

		logger.trace("Performing each chunk...");
		for (EtlSessionLog nextChunk : chunks) {
			Date start = nextChunk.getStartDate();
			Date end = nextChunk.getEndDate();

			logger.trace("Checking etl date interval which is proposed to be loaded...");
			if (start.equals(end) || start.after(end)) {
				Exceptions.throwUncheckedException(new EtlProcessingException(this.getClass()
				        + " reports: iterval to extract is broken", LocalizationUtils.getLocalizedString(
				        "alert_message_wrong_interval_to_etl_from_date", this.getClass().getName(),
				        Utils.formatDate(start), Utils.formatDate(end))));
			}

			logger.debug("Generating ETL script : " + nextChunk);
			Script etlIt = getTaskGen().generate(nextChunk, this.bufferFileExport, this.bufferFileImport,
			        objectsPerScript);

			logger.debug("Queuing ETL process to extract data from " + Utils.formatDate(start) + " to "
			        + Utils.formatDate(end));
			getEtlManager().add(etlIt);
		}
	}

	/**
	 * Delegates descendant to determine interval of extracted data
	 * 
	 * @param objectsNum
	 *            total objects registered in system
	 * @param firstExport
	 *            left end of etl timeline
	 * @param lastExport
	 *            right end of etl timeline
	 * @return List of etl chunks to extract or empty collection if there is no
	 *         appropriate chunks
	 * @throws EtlProcessingException
	 *             if error occurs during execution
	 */
	protected abstract List<EtlSessionLog> provideDateInterval(final int objectsNum, final EtlSessionLog firstExport,
	        final EtlSessionLog lastExport);

	protected EtlTaskGenerator getTaskGen() {
		return taskGen.get();
	}

	protected CommandManager<Script> getEtlManager() {
		return etlManager;
	}

	private EtlLog getLog() {
		return this.etlLog.get();
	}
}
