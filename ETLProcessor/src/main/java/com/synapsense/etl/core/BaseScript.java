package com.synapsense.etl.core;

import java.net.URL;
import java.util.Map;

import com.synapsense.etl.entities.EtlSessionLog;

abstract class BaseScript implements Script {

	private URL url;
	private Map<String, Object> properties;

	protected BaseScript(URL url, Map<String, Object> properties) {
		this.url = url;
		this.properties = properties;
	}

	public abstract EtlSessionLog run();

	@Override
	public String toString() {
		return "EtlScript [url=" + url + ", properties=" + properties + "]";
	}

	protected URL getURL() {
		return this.url;
	}

	protected Map<String, Object> getProperties() {
		return this.properties;
	}

}
