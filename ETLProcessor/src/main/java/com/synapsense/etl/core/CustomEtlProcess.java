package com.synapsense.etl.core;

import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import com.google.inject.Provider;
import com.synapsense.etl.entities.EtlSessionLog;
import com.synapsense.etl.exceptions.Exceptions;
import com.synapsense.etl.vendor.BufferFile;
import com.synapsense.util.LocalizationUtils;

/**
 * Allows to ETL single portion of data.
 * 
 * @author shabanov
 * 
 */
public class CustomEtlProcess extends EtlProcess {

	private Date startEtlFrom;
	private Date endEtlAt;

	public CustomEtlProcess(Provider<EtlTaskGenerator> taskGen, CommandManager<Script> etlManager,
	        Provider<EtlLog> etlLog, BufferFile bufferFile, Date start, Date end) {
		super(taskGen, etlManager, etlLog, bufferFile.localPath("custom.csv"), bufferFile.relativePath("custom.csv"));
		if (start.after(end)) {
			throw new IllegalArgumentException(
			        "Supplied interval is invalid , interval start date is after interval end date");
		}
		this.startEtlFrom = start;
		this.endEtlAt = end;

	}

	@Override
	protected List<EtlSessionLog> provideDateInterval(int objectsNum, EtlSessionLog firstExport,
	        EtlSessionLog lastExport) {
		Date etlStartedAt = firstExport.getStartDate();
		Date etlFinishedAt = lastExport.getEndDate();

		List<EtlSessionLog> chunksToEtl = new LinkedList<EtlSessionLog>();

		// shift etl interval inside already exported interval in case of equal
		// boundaries
		if (endEtlAt.equals(etlFinishedAt)) {
			endEtlAt = rollSeconds(endEtlAt, -1);
		}

		if (startEtlFrom.equals(etlStartedAt)) {
			startEtlFrom = rollSeconds(startEtlFrom, 1);
		}

		// ajusting date intervals to avoid one second ovelapping
		if (endEtlAt.equals(etlStartedAt)) {
			endEtlAt = rollSeconds(endEtlAt, -1);
		}

		if (startEtlFrom.equals(etlFinishedAt)) {
			startEtlFrom = rollSeconds(startEtlFrom, 1);
		}

		// --*---*--eb---ee--->
		if (startEtlFrom.before(etlStartedAt) && endEtlAt.before(etlStartedAt)) {
			chunksToEtl.add(new EtlSessionLog(startEtlFrom, endEtlAt, 1, objectsNum));
			return chunksToEtl;
		}

		// ---eb---ee----*---*-->
		if (startEtlFrom.after(etlFinishedAt) && endEtlAt.after(etlFinishedAt)) {
			chunksToEtl.add(new EtlSessionLog(startEtlFrom, endEtlAt, 1, objectsNum));
			return chunksToEtl;
		}

		// --*--eb----*---ee--->
		if (startEtlFrom.before(etlStartedAt) && endEtlAt.after(etlStartedAt) && endEtlAt.before(etlFinishedAt)) {
			chunksToEtl.add(new EtlSessionLog(startEtlFrom, endEtlAt, 1, objectsNum));
			return chunksToEtl;
		}

		// ---eb--*---*--ee--->
		if (startEtlFrom.after(etlStartedAt) && startEtlFrom.before(etlFinishedAt) && endEtlAt.after(etlStartedAt)
		        && endEtlAt.before(etlFinishedAt)) {
			Exceptions.throwUncheckedException(new EtlProcessingException("Interval [" + startEtlFrom.toString() + ", "
			        + endEtlAt.toString() + "] has already been extracted", LocalizationUtils.getLocalizedString(
			        "alert_message_wrong_interval_to_etl_from_date_it_is_already_extracted", startEtlFrom.toString(),
			        endEtlAt.toString())));
		}

		// ---eb--*--ee--*-->
		if (startEtlFrom.after(etlStartedAt) && startEtlFrom.before(etlFinishedAt) && endEtlAt.after(etlFinishedAt)) {
			chunksToEtl.add(new EtlSessionLog(etlFinishedAt, endEtlAt, 1, objectsNum));
			return chunksToEtl;
		}

		// --*--eb----ee---*--> special case , need two intervals to etl
		if (startEtlFrom.before(etlStartedAt) && endEtlAt.after(etlFinishedAt)) {
			chunksToEtl.add(new EtlSessionLog(startEtlFrom, etlStartedAt, 1, objectsNum));
			chunksToEtl.add(new EtlSessionLog(etlFinishedAt, endEtlAt, 1, objectsNum));
			return chunksToEtl;
		}

		return chunksToEtl;
	}

	private Date rollSeconds(Date date, int seconds) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.roll(Calendar.SECOND, seconds);
		return c.getTime();
	}

}
