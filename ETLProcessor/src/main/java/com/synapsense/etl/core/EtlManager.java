package com.synapsense.etl.core;

import org.apache.log4j.Logger;

import com.google.inject.Provider;
import com.synapsense.etl.entities.EtlSessionLog;

public class EtlManager extends CommandManager<Script> {
	private static Logger logger = Logger.getLogger(EtlManager.class);

	private Provider<EtlLog> etlLog;

	public EtlManager(Provider<EtlLog> etlLog) {
		this.etlLog = etlLog;
	}

	@Override
	protected void executeNext(Script script) {
		logger.trace("Executing next etl script... " + script.toString());
		try {
			EtlSessionLog scriptLog = script.run();
			if (scriptLog != null && !EtlSessionLog.NOTHING.equals(scriptLog)) {
				logger.debug("Script " + script + " was executed");
				etlLog.get().log(scriptLog);
			}
		} catch (EtlProcessingException e) {
			logger.warn("Cannot execute etl script :" + script.toString(), e);
		}

	}
}
