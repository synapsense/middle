package com.synapsense.etl.core;

import java.util.Comparator;
import java.util.concurrent.PriorityBlockingQueue;

public class EtlConfigurationManger extends CommandManager<Command> {

	public EtlConfigurationManger() {
		super(new PriorityBlockingQueue<Command>(100, new Comparator<Command>() {
			@Override
			public int compare(Command o1, Command o2) {
				if (o1.priority() > o2.priority())
					return 1;
				if (o1.priority() < o2.priority())
					return -1;
				return 0;
			}
		}));
	}

	@Override
	protected void executeNext(Command elem) {
		elem.execute();
	}
}
