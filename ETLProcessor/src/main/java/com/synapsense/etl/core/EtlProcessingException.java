package com.synapsense.etl.core;

import com.synapsense.etl.EtlApplicationException;

public class EtlProcessingException extends EtlApplicationException {

	private static final long serialVersionUID = 299819938697852091L;

	private boolean closeEtlProcess;

	public EtlProcessingException(String message) {
		this(message, message);
	}
	
	public EtlProcessingException(String message,String macroizedMessage) {
		this(message, macroizedMessage, false);
	}

	public EtlProcessingException(String message, String macroizedMessage, boolean closeEtlProcess) {
		super(message, macroizedMessage);
		this.closeEtlProcess = closeEtlProcess;
	}

	public boolean isFinished() {
		return closeEtlProcess;
	}

}
