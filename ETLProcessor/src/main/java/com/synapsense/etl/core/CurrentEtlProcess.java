package com.synapsense.etl.core;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;

import com.google.inject.Provider;
import com.synapsense.etl.entities.EtlSessionLog;
import com.synapsense.etl.exceptions.Exceptions;
import com.synapsense.etl.vendor.BufferFile;
import com.synapsense.util.LocalizationUtils;

public class CurrentEtlProcess extends EtlProcess {

	private static Logger logger = Logger.getLogger(CurrentEtlProcess.class);

	public CurrentEtlProcess(Provider<EtlTaskGenerator> taskGen, CommandManager<Script> etlManager,
	        Provider<EtlLog> etlLog, BufferFile bufferFile) {
		super(taskGen, etlManager, etlLog, bufferFile.localPath("current.csv"), bufferFile.relativePath("current.csv"));
	}

	@Override
	protected List<EtlSessionLog> provideDateInterval(final int objectsNum, final EtlSessionLog firstExport,
	        final EtlSessionLog lastExport) {
		// FIXME hack +1000: how to determine the last export date in case of
		// first etl run?
		logger.trace("Using current date time as upper etl limit...");
		Date current = new Date(System.currentTimeMillis() + 1000);
		Date lastExportStart = lastExport.getStartDate();
		Date lastExportEnd = lastExport.getEndDate();

		if (current.before(lastExportEnd)) {
			Exceptions.throwUncheckedException(new EtlProcessingException(
			        "Current ETL process error. Interval is wrong [" + lastExportEnd.toString() + ", "
			                + Utils.formatDate(current)
			                + ". Perhaps it was the system time rolled back on host machine", LocalizationUtils
			                .getLocalizedString("alert_message_interval_to_etl_is_wrong", lastExportEnd.toString(),
			                        Utils.formatDate(current))));
		}

		List<EtlSessionLog> chunksToEtl = new LinkedList<EtlSessionLog>();
		if (objectsNum > lastExport.getEndObject()) {
			// lost chunk
			chunksToEtl.add(new EtlSessionLog(lastExportStart, lastExportEnd, lastExport.getEndObject(), objectsNum));
		}
		// main task chunk
		chunksToEtl.add(new EtlSessionLog(lastExportEnd, current, 1, objectsNum));

		return chunksToEtl;
	}

}
