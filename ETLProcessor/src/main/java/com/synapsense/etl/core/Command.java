package com.synapsense.etl.core;

public interface Command {
	void execute();

	int priority();
}
