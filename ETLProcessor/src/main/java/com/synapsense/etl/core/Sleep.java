package com.synapsense.etl.core;

import com.synapsense.etl.entities.EtlSessionLog;

public class Sleep implements Command, Script {
	private long time;

	public Sleep(long time) {
		this.time = time;
	}

	@Override
	public EtlSessionLog run() {
		try {
			Thread.sleep(this.time);
		} catch (InterruptedException e) {
			return EtlSessionLog.NOTHING;
		}
		return EtlSessionLog.NOTHING;
	}

	@Override
	public void execute() {
		run();
	}

	@Override
	public int priority() {
		return 0;
	}

	@Override
	public String toString() {
		return "Sleep [" + time + "] ms";
	}
}
