package com.synapsense.etl.core;

import com.synapsense.etl.entities.EtlSessionLog;

public interface EtlLog {

	EtlSessionLog getLastImported();

	EtlSessionLog getFirstImported();

	int getOverallObjectsNumber();

	void log(EtlSessionLog scriptLog);

	void checkTargetDb();

	void checkSourceDb();

}
