package com.synapsense.etl.core;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.StringTokenizer;

public class Utils {

	private static DateFormat DATE_FORMAT = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.FULL);

	public static boolean validateChunkExpr(String chunkExpr) {
		try {
			dateSub(new Date(), chunkExpr);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public static Date dateSub(Date current, String chunkExpr) {
		StringTokenizer st = new StringTokenizer(chunkExpr);
		Integer numberOfUnits = null;
		try {
			numberOfUnits = Integer.parseInt(st.nextToken());
		} catch (NumberFormatException e) {
			throw new IllegalArgumentException("Supplied 'chunkExp' is invalid. use <integer (day|hr|min)");
		}

		String timeUnit = st.nextToken();
		if (!("day".equals(timeUnit) || "hr".equals(timeUnit) || "min".equals(timeUnit))) {
			throw new IllegalArgumentException("Supplied 'chunkExp' is invalid. use <integer (day|hr|min)");
		}

		int timePartField = 0;

		if ("day".equals(timeUnit)) {
			timePartField = Calendar.DAY_OF_MONTH;
		}
		if ("hr".equals(timeUnit)) {
			timePartField = Calendar.HOUR_OF_DAY;
		}
		if ("min".equals(timeUnit)) {
			timePartField = Calendar.MINUTE;
		}

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(current);
		calendar.add(timePartField, -numberOfUnits);
		return calendar.getTime();

	}

	public static synchronized String formatDate(Date date) {
		return DATE_FORMAT.format(date);
	}

}
