package com.synapsense.etl.core;

import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

public abstract class CommandManager<T> implements Runnable {

	private volatile boolean isRunning = false;
	private Queue<T> queue;

	protected CommandManager() {
		this(new LinkedBlockingQueue<T>());
	}

	protected CommandManager(Queue<T> queue) {
		this.queue = queue;
	}

	public void shutdown() {
		isRunning = false;
	}

	public void add(T elem) {
		queue.add(elem);
	}

	@Override
	public void run() {
		isRunning = true;
		while (queue.size() != 0 && isRunning) {
			T elem = queue.poll();
			executeNext(elem);
		}
	}

	protected abstract void executeNext(T elem);

}
