package com.synapsense.etl.exceptions;

import java.util.Collection;
import java.util.Date;

import org.apache.log4j.Logger;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.name.Named;
import com.synapsense.dto.Alert;
import com.synapsense.dto.AlertType;
import com.synapsense.etl.EtlApplicationException;
import com.synapsense.etl.configuration.ConfigurationException;
import com.synapsense.exception.AlertTypeAlreadyExistsException;
import com.synapsense.service.AlertService;
import com.synapsense.util.LocalizationUtils;

public class RaiseAlertHandler implements OnErrorHandler {
	private static Logger logger = Logger.getLogger(RaiseAlertHandler.class);

	private Provider<AlertService> alertServiceProvider;
	private AlertType alertType;

	@Inject
	public RaiseAlertHandler(@Named("AlertService") Provider<AlertService> alertService,
	        @Named("EtlAlertType") AlertType alertType) {
		this.alertServiceProvider = alertService;
		this.alertType = alertType;
	}

	@Override
	public void onError(EtlApplicationException exception) {
		AlertService alertService = null;
		try {
			alertService = getAlertServiceProxy();
		} catch (Exception e) {
			logger.warn("Alert service is temporary unavailable. Cannot register alert in ES", e);
			logger.warn("Warning ETL application :", exception);
			return;
		}

		tryCreateAlertType(alertService, alertType);

		Alert alert = new Alert(alertType.getName(), alertType.getName(), new Date(), exception.getMacroizedMessage());

		logger.warn("Raising new alert because of ", exception);
		alertService.raiseAlert(alert);
	}

	private AlertService getAlertServiceProxy() {
		return this.alertServiceProvider.get();
	}

	private void tryCreateAlertType(AlertService alertService, AlertType alertType) {
		Collection<AlertType> types = alertService.getAlertTypes(new String[] { alertType.getName() });
		if (types.isEmpty()) {
			try {
				logger.info("Registering new alert type : " + alertType);
				alertService.createAlertType(alertType);
			} catch (AlertTypeAlreadyExistsException e) {
				Exceptions.throwUncheckedException(new ConfigurationException(e.getMessage(), LocalizationUtils.getLocalizedString(
				        "alert_message_cannot_create_alert_type_for_etl_processor", e.getMessage())));
			}
		}
	}

}
