package com.synapsense.etl.exceptions;

import com.synapsense.etl.EtlApplicationException;

public interface OnErrorHandler {
	void onError(EtlApplicationException exception);
}
