package com.synapsense.etl.exceptions;

import org.apache.log4j.Logger;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.synapsense.etl.EtlApplication;
import com.synapsense.etl.EtlApplicationException;

public class StopEtlHandler implements OnErrorHandler {
	private static Logger logger = Logger.getLogger(StopEtlHandler.class);

	private EtlApplication application;

	@Inject
	public StopEtlHandler(@Named("MainApp") EtlApplication application) {
		this.application = application;
	}

	@Override
	public void onError(EtlApplicationException e) {
		logger.warn("Closing ETL application because ", e);
		application.close();
	}

}
