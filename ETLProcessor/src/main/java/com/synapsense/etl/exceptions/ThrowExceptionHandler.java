package com.synapsense.etl.exceptions;

import com.synapsense.etl.EtlApplicationException;

public class ThrowExceptionHandler implements OnErrorHandler {

	@Override
	public void onError(EtlApplicationException e) {
		throw e;
	}

}
