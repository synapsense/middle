package com.synapsense.etl.exceptions;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import com.google.inject.Singleton;
import com.synapsense.etl.EtlApplicationException;

@Singleton
public class Exceptions {
	private static volatile Exceptions instance;

	private Map<Pattern, List<OnErrorHandler>> typeConditions;

	private Exceptions() {
		this.typeConditions = new HashMap<Pattern, List<OnErrorHandler>>();
	}

	public static EtlApplicationException throwUncheckedException(EtlApplicationException e)
	        throws EtlApplicationException {
		List<OnErrorHandler> callbacksToCall = new LinkedList<OnErrorHandler>();
		for (Map.Entry<Pattern, List<OnErrorHandler>> entry : getInstance().typeConditions.entrySet()) {
			if (entry.getKey().matcher(e.getClass().getName()).matches()) {
				callbacksToCall.addAll(entry.getValue());
			}
		}

		for (OnErrorHandler callback : callbacksToCall) {
			// TODO drawback of chaining - onerror may throw another exception
			callback.onError(e);
		}

		throw e;
	}

	public static void addAction(String regex, List<OnErrorHandler> handlers) {
		getInstance().typeConditions.put(Pattern.compile(regex), handlers);
	}

	private static Exceptions getInstance() {
		if (instance == null) {
			synchronized (Exceptions.class) {
				if (instance == null)
					instance = new Exceptions();
			}
		}
		return instance;
	}

}
