package com.synapsense.etl.scheduler;

import java.util.Date;

public interface SchedulerService {
	void scheduleTask(String taskId, ScheduleTask task, Date startTime);

	void scheduleTask(String taskId, ScheduleTask task, String schedule);

	void removeTask(String taskId);

	void shutdown();

	void updateSchedule(String taskId, String newSchedule);
}
