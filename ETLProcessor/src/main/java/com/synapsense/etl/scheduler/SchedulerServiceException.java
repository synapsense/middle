package com.synapsense.etl.scheduler;

import com.synapsense.etl.EtlApplicationException;

public class SchedulerServiceException extends EtlApplicationException {

	private static final long serialVersionUID = -5655158312228143140L;

	public SchedulerServiceException(String message) {
		super(message, message);
	}

}
