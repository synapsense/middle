package com.synapsense.etl.scheduler;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.synapsense.etl.core.EtlProcessingException;

public class QuartzTaskProxy implements Job {

	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		ScheduleTask task = (ScheduleTask) arg0.getJobDetail().getJobDataMap().get("task");
		try {
			task.run();
		} catch (EtlProcessingException e) {
			JobExecutionException thw = new JobExecutionException(e.getMessage());
			if (e.isFinished()) {
				thw.setUnscheduleAllTriggers(true);
			}
			throw thw;
		}
	}
}
