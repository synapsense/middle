package com.synapsense.etl.scheduler;

public interface ScheduleTask {
	void run();
}
