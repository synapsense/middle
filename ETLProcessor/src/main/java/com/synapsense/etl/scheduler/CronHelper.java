package com.synapsense.etl.scheduler;

import org.quartz.CronExpression;

public final class CronHelper {
	public static final boolean validate(String schedule) {
		return CronExpression.isValidExpression(schedule);
	}

	public static final String everyNSeconds(int sec) {
		return "0/" + sec + " * * * * ?";
	}
}
