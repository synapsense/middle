package com.synapsense.etl.scheduler;

import java.text.ParseException;
import java.util.Date;

import org.apache.log4j.Logger;
import org.quartz.CronTrigger;
import org.quartz.JobDetail;
import org.quartz.SchedulerException;
import org.quartz.SimpleTrigger;
import org.quartz.Trigger;

import com.google.inject.Inject;
import com.synapsense.etl.exceptions.Exceptions;
import com.synapsense.util.LocalizationUtils;

public class EtlScheduler implements SchedulerService {

	private static Logger logger = Logger.getLogger(SchedulerService.class);

	private static final String ETL_GROUP_NAME = "ETL";

	private org.quartz.Scheduler schedulerService;

	@Inject
	public EtlScheduler(org.quartz.Scheduler schedulerService) {
		this.schedulerService = schedulerService;
	}

	@Override
	public void scheduleTask(final String taskId, final ScheduleTask task, final String schedule) {
		removeTask(taskId);

		JobDetail jd = new JobDetail(taskId, ETL_GROUP_NAME, QuartzTaskProxy.class);
		jd.getJobDataMap().put("task", task);

		Trigger trigger = createCronTrigger(taskId, schedule);
		scheduleTask(jd, trigger);
		logger.info("Successfully scheduled task : " + taskId + ", cron expression " + schedule);
	}

	@Override
	public void shutdown() {
		try {
			schedulerService.shutdown(true);
		} catch (SchedulerException e) {
			Exceptions.throwUncheckedException(new SchedulerServiceException(e.getMessage()));
		}
	}

	@Override
	public void updateSchedule(String taskId, String newSchedule) {
		Trigger trigger = createCronTrigger(taskId, newSchedule);
		try {
			schedulerService.rescheduleJob(taskId, "ETL", trigger);
		} catch (SchedulerException e) {
			Exceptions.throwUncheckedException(new SchedulerServiceException(e.getMessage()));
		}
	}

	@Override
	public void removeTask(String taskId) {
		try {
			schedulerService.deleteJob(taskId, ETL_GROUP_NAME);
		} catch (SchedulerException e) {
			logger.debug("Cannot remove task with id : " + taskId + " due to ", e);
		}
	}

	@Override
	public void scheduleTask(String taskId, ScheduleTask task, Date startTime) {
		JobDetail jd = new JobDetail(taskId, ETL_GROUP_NAME, QuartzTaskProxy.class);
		jd.getJobDataMap().put("task", task);
		Trigger trigger = createSingleRunTrigger(taskId, startTime);
		scheduleTask(jd, trigger);
		logger.info("Successfully scheduled no repeat task : " + taskId + ", will start at " + startTime);
	}

	private void scheduleTask(JobDetail jd, Trigger trigger) {
		try {
			schedulerService.scheduleJob(jd, trigger);
		} catch (SchedulerException e) {
			Exceptions.throwUncheckedException(new SchedulerServiceException(e.getMessage()));
		}
	}

	private Trigger createSingleRunTrigger(String triggerName, Date startTime) {
		Trigger trigger = new SimpleTrigger(triggerName, ETL_GROUP_NAME, startTime);
		return trigger;
	}

	private Trigger createCronTrigger(String triggerName, String schedule) {
		try {
			Trigger trigger = new CronTrigger(triggerName, ETL_GROUP_NAME, schedule);
			trigger.setJobGroup(ETL_GROUP_NAME);
			trigger.setJobName(triggerName);
			trigger.setStartTime(new Date());
			return trigger;
		} catch (ParseException e) {
			Exceptions.throwUncheckedException(new SchedulerServiceException(LocalizationUtils.getLocalizedString(
			        "alert_message_error_in_cron_expression", schedule, e.getMessage())));
		}
		return null;
	}
}
