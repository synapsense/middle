package com.synapsense.etl;

public class EtlConstants {
	public static final String CONFIGURATION_DATA_TYPE_NAME = "CONFIGURATION_DATA";
	public static final String CONFIGURATION_DATA_CONFIG_PROPERTY = "config";
	public static final String CONFIGURATION_DATA_NAME_PROPERTY = "name";
}
