package com.synapsense.etl.vendor;

import java.util.HashMap;
import java.util.Map;

import com.synapsense.etl.configuration.ConfigurationException;
import com.synapsense.etl.exceptions.Exceptions;
import com.synapsense.util.LocalizationUtils;

public abstract class EtlUrlResolver {

	private String url;
	private String login;
	private String passw;

	private Map<String, String> jpaOptions = new HashMap<>();
	private Map<String, String> etlOptions = new HashMap<>();

	protected EtlUrlResolver(String url, String login, String passw) {
		this.url = url;
		this.login = login;
		this.passw = passw;

		parseUrl();
	}

	public static EtlUrlResolver getInstance(String url, String login, String passw) {
		if (url.contains("mysql")) {
			return new EtlMySql(url, login, passw);
		} else if (url.contains("sqlserver")) {
			return new EtlSqlServer(url, login, passw);
		} else {
			throw Exceptions.throwUncheckedException(new ConfigurationException("Unsupported target database vendor. Check url " + url, LocalizationUtils.getLocalizedString(
			        "alert_message_unsupported_target_database_vendor", url)));
		}
	}

	public Map<String, String> getJPAOptions() {
		return jpaOptions;
	}

	public Map<String, String> getEtlOptions() {
		return etlOptions;
	}

	public abstract BufferFile getBufferFile(String dataDir, String dataDirSharedAlias);

	protected abstract Map<String, String> provideJPASpecifics();

	protected abstract Map<String, String> provideETLSpecifics();

	private void parseUrl() {
		jpaOptions.put("hibernate.connection.username", login);
		jpaOptions.put("hibernate.connection.password", passw);
		jpaOptions.put("hibernate.connection.url", url);
		jpaOptions.putAll(provideJPASpecifics());
		etlOptions.putAll(provideETLSpecifics());
	}

}
