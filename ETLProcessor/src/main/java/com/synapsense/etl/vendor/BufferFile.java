package com.synapsense.etl.vendor;

import java.io.File;

public class BufferFile {

	private String dataPathLocal;
	private String dataPathRemote;

	public BufferFile() {
		this("", "");
	}

	public BufferFile(String dataPathLocal, String dataPathRemote) {
		this.dataPathLocal = dataPathLocal.isEmpty() ? null : dataPathLocal;
		this.dataPathRemote = dataPathRemote;
	}

	public String localPath(String fileName) {
		return (new File(dataPathLocal, fileName).getAbsolutePath().replace("\\", "\\\\"));
	}

	public String relativePath(String fileName) {
		if (dataPathRemote.isEmpty())
			return localPath(fileName);
		return dataPathRemote + "\\" + fileName;
	}
}
