package com.synapsense.etl.vendor;

import java.util.HashMap;
import java.util.Map;

public class EtlMySql extends EtlUrlResolver {

	protected EtlMySql(String url, String login, String passw) {
		super(url, login, passw);
	}

	@Override
	public BufferFile getBufferFile(String dataDir, String dataDirSharedAlias) {
		return new BufferFile(dataDir, "");
	}

	@Override
	protected Map<String, String> provideJPASpecifics() {
		Map<String, String> options = new HashMap<String, String>();
		options.put("hibernate.dialect", "org.hibernate.dialect.MySQLDialect");
		options.put("hibernate.connection.driver_class", "com.mysql.jdbc.Driver");
		return options;
	}

	@Override
	protected Map<String, String> provideETLSpecifics() {
		Map<String, String> options = new HashMap<String, String>();
		options.put("target.driver", "com.mysql.jdbc.Driver");
		return options;
	}

}
