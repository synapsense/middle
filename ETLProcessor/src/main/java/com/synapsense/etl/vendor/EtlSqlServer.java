package com.synapsense.etl.vendor;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;

public class EtlSqlServer extends EtlUrlResolver {

	protected EtlSqlServer(String url, String login, String passw) {
		super(url, login, passw);
	}

	@Override
	public BufferFile getBufferFile(String dataDir, String dataDirSharedAlias) {
		return new BufferFile(dataDir, generateUNC(getLocalMachineName(), dataDirSharedAlias));
	}

	@Override
	protected Map<String, String> provideJPASpecifics() {
		Map<String, String> options = new HashMap<String, String>();
		options.put("hibernate.dialect", "org.hibernate.dialect.SQLServerDialect");
		options.put("hibernate.connection.driver_class", "com.microsoft.sqlserver.jdbc.SQLServerDriver");

		return options;
	}

	@Override
	protected Map<String, String> provideETLSpecifics() {
		Map<String, String> options = new HashMap<String, String>();
		options.put("target.driver", "com.microsoft.sqlserver.jdbc.SQLServerDriver");
		return options;
	}

	/*
	 * private boolean isTargetHostLocal(String targetHostName) { String localIp
	 * = getLocalMachineIP(); String localName = getLocalMachineName();
	 * 
	 * return targetHostName.equals(localIp) || targetHostName.equals(localName)
	 * || targetHostName.equals(LOCALHOST) ||
	 * targetHostName.equals(LOCALHOST_ADDRESS); }
	 */

	private String getLocalMachineName() {
		InetAddress localAddr = localhost();
		return localAddr.getHostName();
	}

	/*
	 * private String getLocalMachineIP() { InetAddress localAddr = localhost();
	 * return localAddr.getHostAddress(); }
	 */

	private String generateUNC(String hostName, String relativePath) {
		return "\\\\" + hostName + "\\" + relativePath;
	}

	private InetAddress localhost() {
		try {
			return InetAddress.getLocalHost();
		} catch (UnknownHostException e) {
			throw new IllegalStateException("Error trying to determine local host name", e);
		}
	}

}
