package com.synapsense.etl.configuration;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

import com.synapsense.etl.exceptions.Exceptions;
import com.synapsense.util.LocalizationUtils;

public abstract class AbstractConfigSource implements ConfigSource {

	private String name;
	private Configuration cachedConfig;
	private volatile boolean autoreloadEnabled = false;
	private List<ReloadCallback> callbacks = new LinkedList<ReloadCallback>();

	public AbstractConfigSource(String fileName) {
		this.name = fileName;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public synchronized Configuration getConfiguration() {
		String configContent = readConfiguration();
		Configuration updatedConfiguration = prepareFromString(configContent);
		cacheConfiguration(updatedConfiguration);
		return cachedConfig;
	}

	public synchronized void addSourceChangedListener(ReloadCallback callback) {
		this.callbacks.add(callback);

		if (!autoreloadEnabled) {
			enableAutoReload();
			autoreloadEnabled = true;
		}

	}

	@Override
	public String toString() {
		return "Configuration source: name=" + name + ";file=" + getUrl();
	}

	protected abstract String readConfiguration();

	protected abstract void enableAutoReload() throws ConfigurationException;

	protected synchronized void reload(String config) {
		Configuration configuration = prepareFromString(config);

		if (cachedConfig.equals(configuration)) {
			return;
		}

		cacheConfiguration(configuration);

		for (ReloadCallback callback : this.callbacks) {
			callback.reload(this.cachedConfig);
		}
	}

	private void cacheConfiguration(Configuration configuration) {
		if (cachedConfig == null || !this.cachedConfig.equals(configuration)) {
			this.cachedConfig = configuration;
		}
	}

	private Configuration prepareFromString(String config) {
		if (config == null)
			return new NulConfiguration();

		Properties properties = new Properties();
		InputStream is = new ByteArrayInputStream(config.getBytes());
		try {
			properties.load(is);
			return new PropertyBasedConfiguration(properties);
		} catch (IOException e) {
			Exceptions.throwUncheckedException(new ConfigurationException(e.getMessage(), LocalizationUtils.getLocalizedString(
			        "alert_message_error_parsing_configuration_source", e.getMessage(), getUrl())));
		}
		return new NulConfiguration();
	}

}
