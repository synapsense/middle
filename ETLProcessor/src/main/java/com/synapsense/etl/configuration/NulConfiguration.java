package com.synapsense.etl.configuration;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class NulConfiguration implements Configuration {

	private Date timestamp = new Date(0L);

	@Override
	public Map<String, Object> listOptions() {
		return new HashMap<String, Object>();
	}

	@Override
	public String getOption(String oprionName) {
		return "";
	}

	@Override
	public Configuration merge(Configuration configuration) {
		return this;
	}

	@Override
	public String toString() {
		return ">NulConfiguration";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((timestamp == null) ? 0 : timestamp.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		return true;
	}

}
