package com.synapsense.etl.configuration;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.log4j.Logger;

import com.synapsense.etl.exceptions.Exceptions;
import com.synapsense.util.LocalizationUtils;

public class FlatFileConfigSource extends AbstractConfigSource {

	private static Logger logger = Logger.getLogger(FlatFileConfigSource.class);

	private Timer timer;

	private String fileName;
	
	public FlatFileConfigSource(String name, String fileName) {
		super(name);
		this.fileName = fileName;
	}

	protected String readConfiguration() {
		try {
			StringBuilder bld = new StringBuilder();
			BufferedReader reader = new BufferedReader(new FileReader(new File(getUrl())));
			try {
				String data;
				while ((data = reader.readLine()) != null) {
					if (bld.length() > 0)
						bld.append("\n");
					bld.append(data);
				}
				return bld.toString();
			} finally {
				reader.close();
			}
		} catch (IOException e) {
			Exceptions.throwUncheckedException(new ConfigurationException(e.getMessage(), LocalizationUtils.getLocalizedString(
			        "alert_message_error_accessing_configuration_source", e.getMessage(), getUrl())));
		}
		return null;
	}

	@Override
	protected void enableAutoReload() {
		synchronized (this) {
			if (timer != null) {
				timer.cancel();
			}
			timer = new Timer(true);
			timer.schedule(new TimerTask() {
				@Override
				public void run() {
					try {
						reload(readConfiguration());
					} catch (Exception e) {
						logger.warn("Error occured during reload of configuration", e);
					}
				}
			}, new Date(), 30 * 1000);
		}
	}

	@Override
    public String getUrl() {
	    return fileName;
    }

}
