package com.synapsense.etl.configuration;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import com.google.inject.Inject;

public class ConfigurationProviderImpl implements ConfigurationProvider {

	private static Logger logger = Logger.getLogger(ConfigurationProvider.class);

	private Map<String, Configuration> preloadedConfigs;

	private Map<String, ConfigSource> configSourcesMap;

	@Inject
	public ConfigurationProviderImpl(Set<ConfigSource> configSources) {
		this.preloadedConfigs = new HashMap<String, Configuration>();
		this.configSourcesMap = new HashMap<String, ConfigSource>();
		for (ConfigSource source : configSources) {
			configSourcesMap.put(source.getName(), source);
		}
	}

	public void enableAutoReload(String config, ReloadCallback callback) {
		logger.debug("Enabling auto reload of " + config + " configuration");
		ConfigSource source = this.configSourcesMap.get(config);
		source.addSourceChangedListener(callback);
	}

	@Override
	public Configuration getConfiguration(String name) {
		synchronized (this) {
			ConfigSource source = configSourcesMap.get(name);
			if (source != null) {
				Configuration config = source.getConfiguration();
				preloadedConfigs.put(name, config);
				return config;
			}
		}
		return new NulConfiguration();

	}

	@Override
	public List<String> listConfigurations() {
		return new LinkedList<String>() {
			private static final long serialVersionUID = 5084661765448435477L;

			{
				for (ConfigSource source : configSourcesMap.values()) {
					add(source.getName());
				}
			}
		};
	}

}
