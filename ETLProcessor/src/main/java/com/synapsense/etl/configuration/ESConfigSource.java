package com.synapsense.etl.configuration;

import java.util.Collection;

import org.apache.log4j.Logger;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.name.Named;
import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;
import com.synapsense.etl.EtlConstants;
import com.synapsense.etl.exceptions.Exceptions;
import com.synapsense.exception.EnvException;
import com.synapsense.service.Environment;
import com.synapsense.service.nsvc.NotificationService;
import com.synapsense.service.nsvc.events.DispatchingProcessor;
import com.synapsense.service.nsvc.filters.PropertiesChangedEventFilter;
import com.synapsense.service.nsvc.filters.TOMatcher;
import com.synapsense.util.LocalizationUtils;

public class ESConfigSource extends AbstractConfigSource {

	private static Logger logger = Logger.getLogger(ESConfigSource.class);

	private volatile boolean isInitialized = false;
	private TO<?> etlConfigHostObject;

	private DispatchingProcessor processor = new DispatchingProcessor();
	private DispatchingProcessor filter = new DispatchingProcessor();

	private Provider<Environment> environmentService;
	private Provider<NotificationService> notificationService;
	private AbstractConfigSource initialConfig;

	@Inject
	public ESConfigSource(@Named("ES notifier") Provider<NotificationService> notificator,
	        @Named("Environment") Provider<Environment> environment,
	        @Named("ETL default configuration") AbstractConfigSource initialConfig) {
		super("etl");
		this.notificationService = notificator;
		this.environmentService = environment;
		this.initialConfig = initialConfig;
	}

	protected synchronized String readConfiguration() {
		Environment env = null;

		try {
			env = environmentService.get();
		} catch (Exception e) {
			Exceptions.throwUncheckedException(new ConfigurationException(e.getMessage(), LocalizationUtils.getLocalizedString(
			        "alert_message_environment_is_inaccessible", getUrl())));
		}

		if (!isInitialized) {
			return init(env);
		}

		String fileContent = null;
		try {
			fileContent = env.getPropertyValue(etlConfigHostObject, EtlConstants.CONFIGURATION_DATA_CONFIG_PROPERTY, String.class);
		} catch (EnvException e) {
			Exceptions.throwUncheckedException(new ConfigurationException(e.getMessage(), LocalizationUtils.getLocalizedString(
			        "alert_message_error_accessing_configuration_source", e.getMessage(), getUrl())));
		}

		return fileContent;
	}

	private synchronized String init(Environment env) {
		String configAsString = initialConfig.readConfiguration();
		if (env == null) {
			return configAsString;
		}

		ValueTO name = new ValueTO(EtlConstants.CONFIGURATION_DATA_NAME_PROPERTY, "ETL");

		Collection<TO<?>> etlConfig = env.getObjects(EtlConstants.CONFIGURATION_DATA_TYPE_NAME, new ValueTO[] { name });

		if (etlConfig.isEmpty()) {
			try {
				ValueTO config = new ValueTO(EtlConstants.CONFIGURATION_DATA_CONFIG_PROPERTY, configAsString);
				ValueTO[] createObjectParameters = new ValueTO[] { name, config };
				this.etlConfigHostObject = env.createObject(EtlConstants.CONFIGURATION_DATA_TYPE_NAME, createObjectParameters);
			} catch (EnvException e) {
				logger.warn("Cannot create ETL configuration object in ES", e);
			}
		} else {
			this.etlConfigHostObject = etlConfig.iterator().next();
			try {
				String config = env.getPropertyValue(this.etlConfigHostObject, EtlConstants.CONFIGURATION_DATA_CONFIG_PROPERTY, String.class);
				if (config == null || config.isEmpty()) {
					try {
						env.setPropertyValue(this.etlConfigHostObject, EtlConstants.CONFIGURATION_DATA_CONFIG_PROPERTY, configAsString);
					} catch (EnvException e) {
						logger.warn("Cannot update ETL configuration in ES", e);
					}
				} else {
					configAsString = config;
				}
			} catch (EnvException e) {
				logger.warn("Cannot get ETL configuration from ES. Local file configuration will be used instead", e);
			}

		}

		this.isInitialized = true;

		return configAsString;
	}

	@Override
	protected void enableAutoReload() {
		try {
			NotificationService nvc = notificationService.get();
			// construct processor
			processor.putPcep(new ConfigUpdateListener(this));
			filter.putPcep(new PropertiesChangedEventFilter(new TOMatcher(this.etlConfigHostObject)));
			nvc.registerProcessor(processor, filter);
		} catch (Exception e) {
			Exceptions.throwUncheckedException(new ConfigurationException(e.getMessage(), LocalizationUtils
			        .getLocalizedString("alert_message_notification_service_error")));
		}
	}

	@Override
    public String getUrl() {
	    return etlConfigHostObject + "\\" + "config" ;
    }

}
