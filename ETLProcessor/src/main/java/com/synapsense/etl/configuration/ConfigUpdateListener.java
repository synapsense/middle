package com.synapsense.etl.configuration;

import com.synapsense.dto.ChangedValueTO;
import com.synapsense.service.nsvc.events.PropertiesChangedEvent;
import com.synapsense.service.nsvc.events.PropertiesChangedEventProcessor;

public class ConfigUpdateListener implements PropertiesChangedEventProcessor {

	private transient AbstractConfigSource configSource;

	public ConfigUpdateListener(AbstractConfigSource configSource) {
		this.configSource = configSource;
	}

	@Override
	public PropertiesChangedEvent process(PropertiesChangedEvent e) {
		for (ChangedValueTO chval : e.getChangedValues()) {
			if (chval.getPropertyName().equals("config")) {
				configSource.reload((String) chval.getValue());
				break;
			}
		}
		return e;
	}

}
