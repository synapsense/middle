package com.synapsense.etl.configuration;

import java.util.Map;

public interface Configuration {
	static Configuration NUL = new NulConfiguration();

	Map<String, Object> listOptions();

	String getOption(String oprionName);

	Configuration merge(Configuration configuration);
}
