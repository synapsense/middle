package com.synapsense.etl.configuration;

import java.util.List;

public interface ConfigurationProvider {

	void enableAutoReload(String configName, ReloadCallback callback);

	Configuration getConfiguration(String name);

	List<String> listConfigurations();

}