package com.synapsense.etl.configuration;

public interface ConfigSource {
	String getName();

	String getUrl();

	Configuration getConfiguration() throws ConfigurationException;

	void addSourceChangedListener(ReloadCallback callback);
}
