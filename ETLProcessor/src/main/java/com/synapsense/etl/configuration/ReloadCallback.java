package com.synapsense.etl.configuration;

public interface ReloadCallback {
	void reload(Configuration configuration);
}
