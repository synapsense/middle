package com.synapsense.etl.configuration;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class PropertyBasedConfiguration implements Configuration {

	private final Properties properties;

	public PropertyBasedConfiguration(Properties properties) {
		this.properties = properties;
	}

	public PropertyBasedConfiguration(final Map<String, String> properties) {
		this(new Properties() {
			private static final long serialVersionUID = -9202587413234490884L;
			{
				putAll(properties);
			}
		});
	}

	@Override
	public Map<String, Object> listOptions() {
		return propertiesToMap(properties);
	}

	@Override
	public String getOption(String oprionName) {
		return properties.getProperty(oprionName);
	}

	@Override
	public String toString() {
		return properties.toString();
	}

	private Map<String, Object> propertiesToMap(Properties properties) {
		Map<String, Object> map = new HashMap<String, Object>(properties.size());
		for (Map.Entry<Object, Object> entry : properties.entrySet()) {
			map.put((String) entry.getKey(), entry.getValue());
		}
		return map;
	}

	@Override
	public Configuration merge(Configuration configuration) {
		properties.putAll(configuration.listOptions());
		return this;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((properties == null) ? 0 : properties.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PropertyBasedConfiguration other = (PropertyBasedConfiguration) obj;
		if (properties == null) {
			if (other.properties != null)
				return false;
		} else if (!properties.equals(other.properties))
			return false;
		return true;
	}

}
