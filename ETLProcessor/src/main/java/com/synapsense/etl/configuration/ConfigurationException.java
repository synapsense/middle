package com.synapsense.etl.configuration;

import com.synapsense.etl.EtlApplicationException;

public class ConfigurationException extends EtlApplicationException {

	private static final long serialVersionUID = 4418293714555043082L;

	private String configurationSource;

	public ConfigurationException(String message, String macroizedMessage) {
		super(message, macroizedMessage);
	}

	public ConfigurationException(String message, String macroizedMessage, String configurationSource) {
		this(message, macroizedMessage);
		this.configurationSource = configurationSource;
	}

	public String getConfigurationSource() {
		return configurationSource;
	}

}
