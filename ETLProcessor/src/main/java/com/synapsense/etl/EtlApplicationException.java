package com.synapsense.etl;

public class EtlApplicationException extends RuntimeException {

	private static final long serialVersionUID = -4152221725157725812L;

	private String macroizedMessage;
	
	public EtlApplicationException(String message, String macroizedMessage) {
		super(message);
		this.macroizedMessage = macroizedMessage;
	}
	
	public String getMacroizedMessage() {
    	return macroizedMessage;
    }

}
