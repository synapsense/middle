
CREATE DATABASE synap_olap
GO

USE synap_olap

IF EXISTS (SELECT name 
	   FROM   sysobjects 
	   WHERE  name = N'MEASURES_FACT' 
	   AND    type = 'U')
    DROP TABLE MEASURES_FACT

CREATE TABLE MEASURES_FACT (
  id int IDENTITY(1,1) PRIMARY KEY,
  object_id_FK int DEFAULT '0',
  property_id_FK int DEFAULT '0',
  time_id_FK int DEFAULT '0',
  value decimal(15,3) NOT NULL
)


IF EXISTS (SELECT name 
	   FROM   sysobjects
	   WHERE  name = N'OBJECTS_DIM' 
	   AND    type = 'U')
    DROP TABLE OBJECTS_DIM


CREATE TABLE OBJECTS_DIM (
  id int PRIMARY KEY,
  name varchar(300) NOT NULL,
  type varchar(300) NOT NULL
)


IF EXISTS (SELECT name 
	   FROM   sysobjects 
	   WHERE  name = N'PROPERTIES_DIM' 
	   AND    type = 'U')
    DROP TABLE PROPERTIES_DIM


CREATE TABLE PROPERTIES_DIM (
  id int PRIMARY KEY,
  name varchar(300) NOT NULL
)


IF EXISTS (SELECT name 
	   FROM   sysobjects 
	   WHERE  name = N'TIME_DIM' 
	   AND    type = 'U')
    DROP TABLE TIME_DIM


CREATE TABLE TIME_DIM (
  id int IDENTITY(1,1) PRIMARY KEY,
  sampled_timestamp datetime,
  minute smallint  NOT NULL,
  hour smallint  NOT NULL,
  day smallint  NOT NULL,
  month smallint  NOT NULL,
  year smallint  NOT NULL
)


IF EXISTS (SELECT name 
	   FROM   sysobjects 
	   WHERE  name = N'TMP_FACT' 
	   AND    type = 'U')
    DROP TABLE TMP_FACT



CREATE TABLE TMP_FACT (
  object_id int DEFAULT '0',
  property_id int DEFAULT '0',
  timestamp datetime,
  value decimal(15,3)
)



IF EXISTS (SELECT name 
	   FROM   sysobjects 
	   WHERE  name = N'TMP_PROPERTIES' 
	   AND    type = 'U')
    DROP TABLE TMP_PROPERTIES


CREATE TABLE TMP_PROPERTIES (
  id int NOT NULL,
  name varchar(300) NOT NULL,
)


IF EXISTS (SELECT name 
	   FROM   sysobjects 
	   WHERE  name = N'TMP_OBJECTS' 
	   AND    type = 'U')
    DROP TABLE TMP_OBJECTS


CREATE TABLE TMP_OBJECTS (
  id int NOT NULL,
  name varchar(300) NOT NULL,
  type varchar(300) NOT NULL
)


IF EXISTS (SELECT name 
	   FROM   sysobjects 
	   WHERE  name = N'METAINF' 
	   AND    type = 'U')
    DROP TABLE METAINF

CREATE TABLE METAINF (
  etl_version int PRIMARY KEY,
  etl_rundate datetime
)

IF EXISTS (SELECT name 
	   FROM   sysobjects 
	   WHERE  name = N'ETL_LOG' 
	   AND    type = 'U')
    DROP TABLE ETL_LOG

CREATE TABLE ETL_LOG (
  id int IDENTITY(1,1) PRIMARY KEY,
  start_date datetime NOT NULL,
  end_date  datetime NOT NULL,
  start_obj int NOT NULL,
  end_obj int NOT NULL
)

ALTER TABLE MEASURES_FACT ADD
  CONSTRAINT fact_refers_property_fk FOREIGN KEY (property_id_FK)
    REFERENCES PROPERTIES_DIM(id)


ALTER TABLE MEASURES_FACT ADD
  CONSTRAINT fact_refers_time_fk FOREIGN KEY (time_id_FK)
    REFERENCES TIME_DIM(id)


ALTER TABLE MEASURES_FACT ADD
  CONSTRAINT fact_refers_object_fk FOREIGN KEY (object_id_FK)
    REFERENCES OBJECTS_DIM(id)


-- indexing tables 

CREATE INDEX start_date_idx ON ETL_LOG (start_date)
CREATE INDEX end_date_idx ON  ETL_LOG (end_date)

CREATE INDEX object_id_idx ON  MEASURES_FACT ( object_id_FK )
CREATE INDEX property_id_idx ON  MEASURES_FACT ( property_id_FK )
CREATE INDEX time_id_idx ON  MEASURES_FACT (time_id_FK)

CREATE INDEX name_idx ON  OBJECTS_DIM (name)
CREATE INDEX type_idx ON  OBJECTS_DIM (type)

CREATE INDEX name_idx ON  PROPERTIES_DIM (name)

CREATE INDEX minute_idx ON  TIME_DIM ( minute )
CREATE INDEX hour_idx ON  TIME_DIM ( hour )
CREATE INDEX day_idx ON  TIME_DIM (day)
CREATE INDEX month_idx ON  TIME_DIM (month)
CREATE INDEX year_idx ON  TIME_DIM (year)

CREATE UNIQUE INDEX date_uniq ON  TIME_DIM (sampled_timestamp)