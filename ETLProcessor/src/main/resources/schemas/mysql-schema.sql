-- =============================================================================
-- Diagram Name: reportingDB
-- Created on: 03.02.2010 22:01:57
-- Diagram Version: 
-- =============================================================================
DROP DATABASE IF EXISTS `synap_olap`;

CREATE DATABASE IF NOT EXISTS `synap_olap`;

USE `synap_olap`;

SET FOREIGN_KEY_CHECKS=0;

-- Drop table MEASURES_FACT
DROP TABLE IF EXISTS `MEASURES_FACT`;

CREATE TABLE `MEASURES_FACT` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `object_id_FK` int(11) DEFAULT '0',
  `property_id_FK` int(11) DEFAULT '0',
  `time_id_FK` int(11) DEFAULT '0',
  `value` double(15,3) NOT NULL,
  PRIMARY KEY(`id`),
  INDEX `object_id_idx`(`object_id_FK`),
  INDEX `property_id_idx`(`property_id_FK`),
  INDEX `time_id_idx`(`time_id_FK`)
)
ENGINE=INNODB
ROW_FORMAT=default;

-- Drop table OBJECTS_DIM
DROP TABLE IF EXISTS `OBJECTS_DIM`;

CREATE TABLE `OBJECTS_DIM` (
  `id` int(11) NOT NULL,
  `name` varchar(300) NOT NULL,
  `type` varchar(300) NOT NULL,
  PRIMARY KEY(`id`),
  INDEX `name_idx`(`name`(20)),
  INDEX `type_idx`(`type`(20))
)
ENGINE=INNODB;

-- Drop table PROPERTIES_DIM
DROP TABLE IF EXISTS `PROPERTIES_DIM`;

CREATE TABLE `PROPERTIES_DIM` (
  `id` int(11) NOT NULL,
  `name` varchar(300) NOT NULL,
  PRIMARY KEY(`id`),
  INDEX `name_idx`(`name`(20))
)
ENGINE=INNODB
ROW_FORMAT=default;

-- Drop table TIME_DIM
DROP TABLE IF EXISTS `TIME_DIM`;

CREATE TABLE `TIME_DIM` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sampled_timestamp` datetime,
  `minute` smallint(6) UNSIGNED NOT NULL,
  `hour` smallint(6) UNSIGNED NOT NULL,
  `day` smallint(6) UNSIGNED NOT NULL,
  `month` smallint(6) UNSIGNED NOT NULL,
  `year` smallint(6) UNSIGNED NOT NULL,
  PRIMARY KEY(`id`),
  INDEX `minute_ind`(`minute`),
  INDEX `hour_ind`(`hour`),
  INDEX `day_ind`(`day`),
  INDEX `month_ind`(`month`),
  INDEX `year_ind`(`year`),
  UNIQUE INDEX `date_uniq`(`sampled_timestamp`)
)
ENGINE=INNODB
ROW_FORMAT=default;

-- Drop table TMP_FACT
DROP TABLE IF EXISTS `TMP_FACT`;

CREATE TABLE `TMP_FACT` (
  `object_id` int(11) DEFAULT '0',
  `property_id` int(11) DEFAULT '0',
  `timestamp` datetime,
  `value` double(15,3)
)
ENGINE=MYISAM
ROW_FORMAT=default;

-- Drop table TMP_PROPERTIES
DROP TABLE IF EXISTS `TMP_PROPERTIES`;

CREATE TABLE `TMP_PROPERTIES` (
  `id` int(11) NOT NULL,
  `name` varchar(300) NOT NULL,
  PRIMARY KEY(`id`)
)
ENGINE=MYISAM
ROW_FORMAT=default;

-- Drop table TMP_OBJECTS
DROP TABLE IF EXISTS `TMP_OBJECTS`;

CREATE TABLE `TMP_OBJECTS` (
  `id` int(11) NOT NULL,
  `name` varchar(300) NOT NULL,
  `type` varchar(300) NOT NULL,
  PRIMARY KEY(`id`)
)
ENGINE=MYISAM
ROW_FORMAT=default;


DROP TABLE IF EXISTS `METAINF`;

CREATE TABLE `METAINF` (
  `etl_version` int(11),
  `etl_rundate` datetime,
  PRIMARY KEY(`etl_version`)
)
ENGINE=INNODB
ROW_FORMAT=default;

-- Drop table TMP_PROPERTIES
DROP TABLE IF EXISTS `ETL_LOG`;

CREATE TABLE `ETL_LOG` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `start_date` datetime NOT NULL,
  `end_date`  datetime NOT NULL,
  `start_obj` int NOT NULL,
  `end_obj`  int NOT NULL,
  PRIMARY KEY(`id`),
  INDEX `start_date_ind`(`start_date`),
  INDEX `end_date_ind`(`end_date`)
)
ENGINE=INNODB
ROW_FORMAT=default;


ALTER TABLE `MEASURES_FACT` ADD
  CONSTRAINT  FOREIGN KEY (`property_id_FK`)
    REFERENCES `PROPERTIES_DIM`(`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION;

ALTER TABLE `MEASURES_FACT` ADD
  CONSTRAINT  FOREIGN KEY (`time_id_FK`)
    REFERENCES `TIME_DIM`(`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION;

ALTER TABLE `MEASURES_FACT` ADD
  CONSTRAINT  FOREIGN KEY (`object_id_FK`)
    REFERENCES `OBJECTS_DIM`(`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION;

SET FOREIGN_KEY_CHECKS=1;



