package unit;

import java.io.File;
import java.util.Date;

import org.jmock.Expectations;
import org.jmock.Mockery;
import org.jmock.Sequence;
import org.jmock.integration.junit4.JUnit4Mockery;
import org.jmock.lib.legacy.ClassImposteriser;
import org.junit.Test;

import com.synapsense.etl.core.CommandManager;
import com.synapsense.etl.core.CurrentEtlProcess;
import com.synapsense.etl.core.EtlLog;
import com.synapsense.etl.core.EtlManager;
import com.synapsense.etl.core.EtlTaskGenerator;
import com.synapsense.etl.core.Script;
import com.synapsense.etl.entities.EtlSessionLog;
import com.synapsense.etl.main.providers.EtlLogProvider;
import com.synapsense.etl.main.providers.EtlTaskGeneratorProvider;
import com.synapsense.etl.scheduler.ScheduleTask;
import com.synapsense.etl.vendor.BufferFile;

public class CurrentEtlProcessTest {
	private static Mockery context = new JUnit4Mockery() {
		{
			setImposteriser(ClassImposteriser.INSTANCE);
		}
	};

	private static final EtlTaskGenerator taskGen = context.mock(EtlTaskGenerator.class);
	private static final EtlTaskGeneratorProvider taskGenProvider = context.mock(EtlTaskGeneratorProvider.class);

	private static final CommandManager<Script> etlManager = context.mock(EtlManager.class);
	private static final EtlLog etlLog = context.mock(EtlLog.class);
	private static final EtlLogProvider etlLogProvider = context.mock(EtlLogProvider.class);
	private static final EtlSessionLog etlLogSession = context.mock(EtlSessionLog.class);
	private static final Script etlScript = context.mock(Script.class);

	@Test
	public void testCurrentEtlExecuting() {
		final Date sessionStart = new Date(System.currentTimeMillis() - 1000);
		final Date sessionEnd = new Date();

		ScheduleTask task = new CurrentEtlProcess(taskGenProvider, etlManager, etlLogProvider, new BufferFile("", ""));

		final Sequence currentEtlProcess = context.sequence("currentEtlProcess");
		context.checking(new Expectations() {
			{
				exactly(1).of(etlLogProvider).get();
				inSequence(currentEtlProcess);
				will(returnValue(etlLog));

				exactly(1).of(etlLog).checkSourceDb();
				inSequence(currentEtlProcess);

				exactly(1).of(etlLogProvider).get();
				inSequence(currentEtlProcess);
				will(returnValue(etlLog));

				exactly(1).of(etlLog).checkTargetDb();
				inSequence(currentEtlProcess);

				exactly(1).of(etlLogProvider).get();
				inSequence(currentEtlProcess);
				will(returnValue(etlLog));

				exactly(1).of(etlLog).getOverallObjectsNumber();
				inSequence(currentEtlProcess);
				will(returnValue(1000));

				exactly(1).of(etlLogProvider).get();
				inSequence(currentEtlProcess);
				will(returnValue(etlLog));

				exactly(1).of(etlLog).getFirstImported();
				inSequence(currentEtlProcess);
				will(returnValue(etlLogSession));

				exactly(1).of(etlLogProvider).get();
				inSequence(currentEtlProcess);
				will(returnValue(etlLog));

				exactly(1).of(etlLog).getLastImported();
				inSequence(currentEtlProcess);
				will(returnValue(etlLogSession));

				exactly(1).of(etlLogSession).getStartDate();
				inSequence(currentEtlProcess);
				will(returnValue(sessionStart));

				exactly(1).of(etlLogSession).getEndDate();
				inSequence(currentEtlProcess);
				will(returnValue(sessionEnd));

				exactly(1).of(etlLogSession).getStartDate();
				inSequence(currentEtlProcess);
				will(returnValue(sessionStart));

				exactly(1).of(etlLogSession).getEndDate();
				inSequence(currentEtlProcess);
				will(returnValue(sessionEnd));

				exactly(1).of(etlLogSession).getEndObject();
				inSequence(currentEtlProcess);
				will(returnValue(1000));

				exactly(1).of(taskGenProvider).get();
				inSequence(currentEtlProcess);
				will(returnValue(taskGen));

				String bufferFile = new File("current.csv").getAbsolutePath().replace("\\", "\\\\");

				exactly(1).of(taskGen).generate(with(any(EtlSessionLog.class)), with(bufferFile), with(bufferFile),
				        with(50));
				inSequence(currentEtlProcess);
				will(returnValue(etlScript));

				exactly(1).of(etlManager).add(etlScript);
				inSequence(currentEtlProcess);
			}
		});

		task.run();

	}

	@Test
	public void testCurrentEtlLostChunk() {
		final Date sessionStart = new Date(System.currentTimeMillis() - 1000);
		final Date sessionEnd = new Date();

		ScheduleTask task = new CurrentEtlProcess(taskGenProvider, etlManager, etlLogProvider, new BufferFile("", ""));

		final int objectsNum = 1000;

		final Sequence currentEtlProcess = context.sequence("currentEtlProcess");
		context.checking(new Expectations() {
			{
				exactly(1).of(etlLogProvider).get();
				inSequence(currentEtlProcess);
				will(returnValue(etlLog));

				exactly(1).of(etlLog).checkSourceDb();
				inSequence(currentEtlProcess);

				exactly(1).of(etlLogProvider).get();
				inSequence(currentEtlProcess);
				will(returnValue(etlLog));

				exactly(1).of(etlLog).checkTargetDb();
				inSequence(currentEtlProcess);

				exactly(1).of(etlLogProvider).get();
				inSequence(currentEtlProcess);
				will(returnValue(etlLog));

				exactly(1).of(etlLog).getOverallObjectsNumber();
				inSequence(currentEtlProcess);
				will(returnValue(objectsNum));

				exactly(1).of(etlLogProvider).get();
				inSequence(currentEtlProcess);
				will(returnValue(etlLog));

				exactly(1).of(etlLog).getFirstImported();
				inSequence(currentEtlProcess);
				will(returnValue(etlLogSession));

				exactly(1).of(etlLogProvider).get();
				inSequence(currentEtlProcess);
				will(returnValue(etlLog));

				exactly(1).of(etlLog).getLastImported();
				inSequence(currentEtlProcess);
				will(returnValue(etlLogSession));

				exactly(1).of(etlLogSession).getStartDate();
				inSequence(currentEtlProcess);
				will(returnValue(sessionStart));

				exactly(1).of(etlLogSession).getEndDate();
				inSequence(currentEtlProcess);
				will(returnValue(sessionEnd));

				exactly(1).of(etlLogSession).getStartDate();
				inSequence(currentEtlProcess);
				will(returnValue(sessionStart));

				exactly(1).of(etlLogSession).getEndDate();
				inSequence(currentEtlProcess);
				will(returnValue(sessionEnd));

				exactly(1).of(etlLogSession).getEndObject();
				inSequence(currentEtlProcess);
				will(returnValue(900));

				exactly(1).of(etlLogSession).getEndObject();
				inSequence(currentEtlProcess);
				will(returnValue(900));

				exactly(1).of(taskGenProvider).get();
				inSequence(currentEtlProcess);
				will(returnValue(taskGen));

				String bufferFile = new File("current.csv").getAbsolutePath().replace("\\", "\\\\");
				exactly(1).of(taskGen).generate(with(any(EtlSessionLog.class)), with(bufferFile), with(bufferFile),
				        with(50));
				inSequence(currentEtlProcess);
				will(returnValue(etlScript));

				exactly(1).of(etlManager).add(etlScript);
				inSequence(currentEtlProcess);

				exactly(1).of(taskGenProvider).get();
				inSequence(currentEtlProcess);
				will(returnValue(taskGen));

				exactly(1).of(taskGen).generate(with(any(EtlSessionLog.class)), with(bufferFile), with(bufferFile),
				        with(50));
				inSequence(currentEtlProcess);
				will(returnValue(etlScript));

				exactly(1).of(etlManager).add(etlScript);
				inSequence(currentEtlProcess);

			}
		});

		task.run();

	}
}
