package unit;

import static org.junit.Assert.assertEquals;

import java.util.Calendar;
import java.util.Date;

import org.junit.Test;

import com.synapsense.etl.core.Utils;

public class UtilsTest {

	@Test(expected = IllegalArgumentException.class)
	public void testRollDateIllegalTimeUnit() {
		Date d = new Date();
		Utils.dateSub(d, "1 dd");
	}

	@Test(expected = IllegalArgumentException.class)
	public void testRollDateIllegalNumberOfTimeUnits() {
		Date d = new Date();
		Utils.dateSub(d, "asd day");
	}

	@Test
	public void testRollDateSubstractDays() {
		Calendar c = Calendar.getInstance();
		Date d = new Date();
		c.setTime(d);
		c.add(Calendar.DAY_OF_MONTH, -37);

		Date calculated = Utils.dateSub(d, "37 day");
		assertEquals(c.getTime(), calculated);
	}
}
