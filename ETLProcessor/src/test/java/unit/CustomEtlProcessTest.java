package unit;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.jmock.Expectations;
import org.jmock.Mockery;
import org.jmock.Sequence;
import org.jmock.integration.junit4.JUnit4Mockery;
import org.jmock.lib.legacy.ClassImposteriser;
import org.junit.Test;

import com.synapsense.etl.core.CommandManager;
import com.synapsense.etl.core.CustomEtlProcess;
import com.synapsense.etl.core.EtlLog;
import com.synapsense.etl.core.EtlManager;
import com.synapsense.etl.core.EtlProcessingException;
import com.synapsense.etl.core.EtlTaskGenerator;
import com.synapsense.etl.core.Script;
import com.synapsense.etl.entities.EtlSessionLog;
import com.synapsense.etl.main.providers.EtlLogProvider;
import com.synapsense.etl.main.providers.EtlTaskGeneratorProvider;
import com.synapsense.etl.scheduler.ScheduleTask;
import com.synapsense.etl.vendor.BufferFile;

public class CustomEtlProcessTest {
	private static Mockery context = new JUnit4Mockery() {
		{
			setImposteriser(ClassImposteriser.INSTANCE);
		}
	};

	private static final EtlTaskGenerator taskGen = context.mock(EtlTaskGenerator.class);
	private static final EtlTaskGeneratorProvider taskGenProvider = context.mock(EtlTaskGeneratorProvider.class);

	private static final CommandManager<Script> etlManager = context.mock(EtlManager.class);
	private static final EtlLog etlLog = context.mock(EtlLog.class);
	private static final EtlLogProvider etlLogProvider = context.mock(EtlLogProvider.class);
	private static final Script script = context.mock(Script.class);
	private static final EtlSessionLog etlLogSession = context.mock(EtlSessionLog.class);

	@Test
	public void testBackgroundEtlDateIntervalCase1() throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		final Date start = sdf.parse("2010-08-02");
		final Date end = sdf.parse("2010-08-03");

		final Date firstSuccessfulExport = sdf.parse("2010-08-04");
		final Date lastSuccessfulExport = sdf.parse("2010-08-05");

		ScheduleTask task = createEtlProcessInstance(start, end);

		addChecking(firstSuccessfulExport, lastSuccessfulExport, start, end, start, end);

		task.run();
	}

	@Test
	public void testBackgroundEtlDateIntervalCase2() throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		final Date start = sdf.parse("2010-08-02");
		final Date end = sdf.parse("2010-08-04");

		final Date firstSuccessfulExport = sdf.parse("2010-08-03");
		final Date lastSuccessfulExport = sdf.parse("2010-08-05");

		ScheduleTask task = createEtlProcessInstance(start, end);

		addChecking(firstSuccessfulExport, lastSuccessfulExport, start, end, start, firstSuccessfulExport);

		task.run();
	}

	@Test
	public void testBackgroundEtlDateIntervalCase3() throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		final Date start = sdf.parse("2010-08-02");
		final Date end = sdf.parse("2010-08-04");

		final Date firstSuccessfulExport = sdf.parse("2010-08-01");
		final Date lastSuccessfulExport = sdf.parse("2010-08-03");

		ScheduleTask task = createEtlProcessInstance(start, end);

		addChecking(firstSuccessfulExport, lastSuccessfulExport, start, end, lastSuccessfulExport, end);

		task.run();
	}

	@Test
	public void testBackgroundEtlDateIntervalCase4() throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		final Date start = sdf.parse("2010-08-04");
		final Date end = sdf.parse("2010-08-05");

		final Date firstSuccessfulExport = sdf.parse("2010-08-01");
		final Date lastSuccessfulExport = sdf.parse("2010-08-03");

		ScheduleTask task = createEtlProcessInstance(start, end);

		addChecking(firstSuccessfulExport, lastSuccessfulExport, start, end, start, end);

		task.run();
	}

	@Test(expected = EtlProcessingException.class)
	public void testBackgroundEtlDateIntervalCase5() throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		final Date start = sdf.parse("2010-08-02");
		final Date end = sdf.parse("2010-08-04");

		final Date firstSuccessfulExport = sdf.parse("2010-08-01");
		final Date lastSuccessfulExport = sdf.parse("2010-08-05");

		ScheduleTask task = createEtlProcessInstance(start, end);

		addChecking(firstSuccessfulExport, lastSuccessfulExport, start, end, start, end);

		task.run();
	}

	private CustomEtlProcess createEtlProcessInstance(final Date start, final Date end) {
		return new CustomEtlProcess(taskGenProvider, etlManager, etlLogProvider, new BufferFile(), start, end);
	}

	@Test
	/**
	 * there is no algorithm yet to load multiple intervals by one etl process 
	 */
	public void testBackgroundEtlDateIntervalCase6() throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		final Date start = sdf.parse("2010-08-01");
		final Date end = sdf.parse("2010-08-05");

		final Date firstSuccessfulExport = sdf.parse("2010-08-02");
		final Date lastSuccessfulExport = sdf.parse("2010-08-04");

		ScheduleTask task = createEtlProcessInstance(start, end);

		addChecking(firstSuccessfulExport, lastSuccessfulExport, start, end, start, end);

		task.run();
	}

	private void addChecking(final Date etlStart, final Date etlEnd, final Date start, final Date end,
	        final Date shouldStartAt, final Date shouldEndAt) {
		final Sequence etlProcessSeq = context.sequence("etlProcess");
		context.checking(new Expectations() {
			{
				exactly(1).of(etlLogProvider).get();
				will(returnValue(etlLog));
				inSequence(etlProcessSeq);

				exactly(1).of(etlLog).checkSourceDb();
				inSequence(etlProcessSeq);

				exactly(1).of(etlLogProvider).get();
				will(returnValue(etlLog));
				inSequence(etlProcessSeq);

				exactly(1).of(etlLog).checkTargetDb();
				inSequence(etlProcessSeq);

				exactly(1).of(etlLogProvider).get();
				will(returnValue(etlLog));
				inSequence(etlProcessSeq);

				exactly(1).of(etlLog).getOverallObjectsNumber();
				will(returnValue(1000));
				inSequence(etlProcessSeq);

				exactly(1).of(etlLogProvider).get();
				will(returnValue(etlLog));
				inSequence(etlProcessSeq);

				exactly(1).of(etlLog).getFirstImported();
				inSequence(etlProcessSeq);
				will(returnValue(etlLogSession));

				exactly(1).of(etlLogProvider).get();
				inSequence(etlProcessSeq);
				will(returnValue(etlLog));

				exactly(1).of(etlLog).getLastImported();
				inSequence(etlProcessSeq);
				will(returnValue(etlLogSession));

				exactly(1).of(etlLogSession).getStartDate();
				inSequence(etlProcessSeq);
				will(returnValue(etlStart));

				exactly(1).of(etlLogSession).getEndDate();
				inSequence(etlProcessSeq);
				will(returnValue(etlEnd));

				exactly(1).of(etlLogSession).getStartDate();
				inSequence(etlProcessSeq);
				will(returnValue(etlStart));

				exactly(1).of(etlLogSession).getEndDate();
				inSequence(etlProcessSeq);
				will(returnValue(etlEnd));

				exactly(1).of(taskGenProvider).get();
				inSequence(etlProcessSeq);
				will(returnValue(taskGen));

				// FIXME : new api
				exactly(1).of(taskGen).generate(with(any(EtlSessionLog.class)),
				        with((new File("custom.csv").getAbsolutePath().replace("\\", "\\\\"))),
				        with((new File("custom.csv").getAbsolutePath().replace("\\", "\\\\"))), with(50));
				inSequence(etlProcessSeq);
				will(returnValue(script));

				exactly(1).of(etlManager).add(script);
				inSequence(etlProcessSeq);
			}
		});
	}
}
