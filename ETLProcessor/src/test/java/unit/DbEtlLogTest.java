package unit;

import com.synapsense.etl.core.DbEtlLog;
import com.synapsense.etl.core.EtlLog;
import org.jmock.Expectations;
import org.jmock.Mockery;
import org.jmock.Sequence;
import org.jmock.integration.junit4.JUnit4Mockery;
import org.jmock.lib.legacy.ClassImposteriser;
import org.junit.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;

public class DbEtlLogTest {

	private static Mockery context = new JUnit4Mockery() {
		{
			setImposteriser(ClassImposteriser.INSTANCE);
		}
	};
	private static final EntityManagerFactory factorySource = context.mock(EntityManagerFactory.class, "emf source");
	private static final EntityManagerFactory factoryTarget = context.mock(EntityManagerFactory.class, "emf target");

	private static final EntityManager em = context.mock(EntityManager.class);
	private static final Query query = context.mock(Query.class);

	//@Test
	public void testGetOverallObjectsNum() {

		EtlLog etlLog = new DbEtlLog(factorySource, factoryTarget);

		final Sequence objectsNum = context.sequence("gettingObjectsNum");
		context.checking(new Expectations() {
			{
				exactly(1).of(factorySource).createEntityManager();
				inSequence(objectsNum);
				will(returnValue(em));
				exactly(1).of(em).createNativeQuery("select max(o.id) from objects as o");
				inSequence(objectsNum);
				will(returnValue(query));

				exactly(1).of(query).getSingleResult();
				inSequence(objectsNum);
				will(returnValue(1));
			}
		});

		etlLog.getOverallObjectsNumber();
	}

}
