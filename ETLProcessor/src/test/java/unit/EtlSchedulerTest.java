package unit;

import org.jmock.Expectations;
import org.jmock.Mockery;
import org.jmock.integration.junit4.JUnit4Mockery;
import org.junit.Test;
import org.quartz.JobDetail;
import org.quartz.SchedulerException;
import org.quartz.Trigger;

import com.synapsense.etl.scheduler.CronHelper;
import com.synapsense.etl.scheduler.EtlScheduler;
import com.synapsense.etl.scheduler.ScheduleTask;
import com.synapsense.etl.scheduler.SchedulerService;

public class EtlSchedulerTest {
	private static Mockery context = new JUnit4Mockery();

	@Test
	public void testScheduleTaskSuccess() throws SchedulerException {
		final ScheduleTask task = context.mock(ScheduleTask.class);
		final org.quartz.Scheduler quartz = context.mock(org.quartz.Scheduler.class);
		String cronExpr = CronHelper.everyNSeconds(5);

		SchedulerService scheduler = new EtlScheduler(quartz);

		context.checking(new Expectations() {
			{
				oneOf(quartz).deleteJob(with("taskName"), with("ETL"));
				oneOf(quartz).scheduleJob(with(any(JobDetail.class)), with(any(Trigger.class)));
			}
		});

		scheduler.scheduleTask("taskName", task, cronExpr);
	}
}
