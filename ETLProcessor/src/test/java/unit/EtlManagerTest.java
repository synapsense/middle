package unit;

import java.util.Date;

import org.jmock.Expectations;
import org.jmock.Mockery;
import org.jmock.integration.junit4.JUnit4Mockery;
import org.jmock.lib.legacy.ClassImposteriser;
import org.junit.Test;

import com.synapsense.etl.core.CommandManager;
import com.synapsense.etl.core.EtlLog;
import com.synapsense.etl.core.EtlManager;
import com.synapsense.etl.core.Loop;
import com.synapsense.etl.core.Script;
import com.synapsense.etl.entities.EtlSessionLog;
import com.synapsense.etl.main.providers.EtlLogProvider;

public class EtlManagerTest {
	private static Mockery context = new JUnit4Mockery() {
		{
			setImposteriser(ClassImposteriser.INSTANCE);
		}
	};

	static final Script script = context.mock(Script.class);

	@Test
	public void testLoop() {
		final CommandManager<Script> manager = context.mock(EtlManager.class);

		final Loop loop = new Loop(manager, script);

		context.checking(new Expectations() {
			{
				exactly(1).of(script).run();
				exactly(1).of(manager).add(loop);
			}
		});

		loop.run();
	}

	@Test
	public void testEtlManagerProcessing() {
		final EtlLog etlLog = context.mock(EtlLog.class);
		final EtlLogProvider etlLogProvider = context.mock(EtlLogProvider.class);

		final CommandManager<Script> manager = new EtlManager(etlLogProvider);

		final EtlSessionLog logRec = new EtlSessionLog(new Date(), new Date(), 1, 1000);
		context.checking(new Expectations() {
			{
				exactly(1).of(script).run();
				will(returnValue(logRec));
				exactly(1).of(etlLogProvider).get();
				will(returnValue(etlLog));
				exactly(1).of(etlLog).log(with(any(EtlSessionLog.class)));
			}
		});

		manager.add(script);
		manager.run();

	}
}
