package unit;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.net.UnknownHostException;

import org.junit.Test;

import com.synapsense.etl.vendor.EtlUrlResolver;

public class EtlUrlResolverTest {

	@Test
	public void testUrlTextLocalHostnameAndPort() throws Exception {
		EtlUrlResolver urlRes = EtlUrlResolver.getInstance("jdbc:mysql://localhost/synap_olap", "r", "p");
		assertEquals(new File("custom.csv").getAbsolutePath().replace("\\", "\\\\"), urlRes
		        .getBufferFile("", "etldata").relativePath("custom.csv"));
	}

	@Test
	public void testUrlTextLocalHostnameAndPortWithLocalDirectoryOption() throws Exception {
		EtlUrlResolver urlRes = EtlUrlResolver.getInstance("jdbc:mysql://localhost/synap_olap", "r", "p");
		assertEquals(new File("data", "custom.csv").getAbsolutePath().replace("\\", "\\\\"),
		        urlRes.getBufferFile("data", "etldata").relativePath("custom.csv"));
	}

	@Test
	public void testUrlTextLocalHostnameOnly() throws Exception {
		EtlUrlResolver urlRes = EtlUrlResolver.getInstance("jdbc:mysql://localhost/synap_olap", "r", "p");
		assertEquals(new File("custom.csv").getAbsolutePath().replace("\\", "\\\\"), urlRes
		        .getBufferFile("", "etldata").relativePath("custom.csv"));
	}

	@Test
	public void testUrl_127_0_0_1_IPOnly() throws Exception {
		EtlUrlResolver urlRes = EtlUrlResolver.getInstance("jdbc:mysql://127.0.0.1/synap_olap;selectMethod=cursor",
		        "r", "p");
		assertEquals(new File("custom.csv").getAbsolutePath().replace("\\", "\\\\"), urlRes
		        .getBufferFile("", "etldata").relativePath("custom.csv"));
	}

	@Test
	public void testUrlLocalHostAssignedIPOnly() throws Exception {
		EtlUrlResolver urlRes = EtlUrlResolver.getInstance(
		        "jdbc:mysql://192.168.200.118/synap_olap;selectMethod=cursor", "r", "p");
		assertEquals(new File("custom.csv").getAbsolutePath().replace("\\", "\\\\"), urlRes
		        .getBufferFile("", "etldata").relativePath("custom.csv"));
	}

	@Test
	public void testUrl_127_0_0_1_IPAndPort() throws Exception {
		EtlUrlResolver urlRes = EtlUrlResolver.getInstance(
		        "jdbc:mysql://127.0.0.1:1000/synap_olap;selectMethod=cursor", "r", "p");
		assertEquals(new File("custom.csv").getAbsolutePath().replace("\\", "\\\\"), urlRes
		        .getBufferFile("", "etldata").relativePath("custom.csv"));
	}

	@Test
	public void testUrlTextRemoteHostnameOnly() throws Exception {
		EtlUrlResolver urlRes = EtlUrlResolver.getInstance(
		        "jdbc:sqlserver://machine:1000/synap_olap;selectMethod=cursor", "r", "p");
		assertEquals(getFileRelativePath("etldata\\custom.csv"),
		        urlRes.getBufferFile("", "etldata").relativePath("custom.csv"));
	}

	@Test
	public void testUrlIpRemoteHostOnly() throws Exception {
		EtlUrlResolver urlRes = EtlUrlResolver.getInstance(
		        "jdbc:sqlserver://192.168.200.80/synap_olap;selectMethod=cursor", "r", "p");
		assertEquals(getFileRelativePath("etldata\\custom.csv"),
		        urlRes.getBufferFile("", "etldata").relativePath("custom.csv"));
	}

	@Test
	public void testUrlIpRemoteHostAndPort() throws Exception {
		EtlUrlResolver urlRes = EtlUrlResolver.getInstance(
		        "jdbc:mysql://192.168.200.80:1000/synap_olap;selectMethod=cursor", "r", "p");
		assertEquals(new File("custom.csv").getAbsolutePath().replace("\\", "\\\\"), urlRes
		        .getBufferFile("", "etldata").relativePath("custom.csv"));
	}

	@Test
	public void testUrlResolveAsRemote_127_0_0_1_IPAndPort() throws Exception {
		EtlUrlResolver urlRes = EtlUrlResolver.getInstance(
		        "jdbc:mysql://127.0.0.1:1000/synap_olap;selectMethod=cursor", "r", "p");
		assertEquals(new File("custom.csv").getAbsolutePath().replace("\\", "\\\\"), urlRes
		        .getBufferFile("", "etldata").relativePath("custom.csv"));
	}

	public static void main(String[] args) throws MalformedURLException {
		URL u = new URL("file", "localhost", "/etldata/custom.csv");
		System.out.println(u.toString());
	}

	private String getFileRelativePath(String filePath) {
		return "\\\\" + getLocalMachineName() + "\\" + filePath;
	}

	private InetAddress localhost() {
		try {
			return InetAddress.getLocalHost();
		} catch (UnknownHostException e) {
			throw new IllegalStateException("Error trying to determine local host name", e);
		}
	}

	private String getLocalMachineName() {
		InetAddress localAddr = localhost();
		return localAddr.getHostName();
	}
}
