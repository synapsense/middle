package func;

import com.synapsense.etl.core.DbEtlLog;
import com.synapsense.etl.core.EtlLog;
import com.synapsense.etl.entities.EtlSessionLog;
import org.dbunit.JdbcDatabaseTester;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.Date;

public class DbEtlLogSqlServerTest {
	private static JdbcDatabaseTester databaseTester;

	public static final String DB_DRIVER = "net.sourceforge.jtds.jdbc.Driver";
	public static final String DB_CONNECTION = "jdbc:jtds:sqlserver://localhost:1435/synap_olap;selectMethod=cursor";
	public static final String DB_USER = "sa";
	public static final String DB_PASSWORD = "root";

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		databaseTester = new JdbcDatabaseTester(DB_DRIVER, DB_CONNECTION, DB_USER, DB_PASSWORD);
	}

	@AfterClass
	public static void teardownAfterClass() throws Exception {
		IDataSet databaseDataSet = getConnection().createDataSet(new String[] { "ETL_LOG" });
		DatabaseOperation.TRUNCATE_TABLE.execute(getConnection(), databaseDataSet);
	}

	@Test
	public void testLog() {
		EntityManagerFactory f1 = Persistence.createEntityManagerFactory("etl_source");
		EntityManagerFactory f2 = Persistence.createEntityManagerFactory("etl_target_sqlserver");
		EtlLog log = new DbEtlLog(f1, f2);
		log.log(new EtlSessionLog(new Date(), new Date(), 1, 1000));

	}

	protected static IDatabaseConnection getConnection() throws Exception {
		return databaseTester.getConnection();
	}
}
