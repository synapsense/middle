package func.standalone;

import java.util.Arrays;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.synapsense.etl.EtlApplication;
import com.synapsense.etl.exceptions.Exceptions;
import com.synapsense.etl.exceptions.OnErrorHandler;
import com.synapsense.etl.exceptions.ThrowExceptionHandler;

public class ETLSinglePeriod {
	public static void main(String[] args) {
		Injector injector = Guice.createInjector(new EtlApplicationModule());

		Exceptions.addAction(".*Exception", Arrays.<OnErrorHandler>asList(new ThrowExceptionHandler()));

		EtlApplication app = injector.getInstance(EtlApplication.class);

		app.start();
	}
}
