package func.standalone;

import mock.FakeAlertServiceProvider;
import mock.FakeEnvProvider;
import mock.FakeNvcProvider;

import com.google.inject.AbstractModule;
import com.google.inject.multibindings.Multibinder;
import com.google.inject.name.Names;
import com.synapsense.dto.AlertType;
import com.synapsense.etl.EtlApplication;
import com.synapsense.etl.configuration.AbstractConfigSource;
import com.synapsense.etl.configuration.ConfigSource;
import com.synapsense.etl.configuration.Configuration;
import com.synapsense.etl.configuration.ConfigurationProvider;
import com.synapsense.etl.configuration.ConfigurationProviderImpl;
import com.synapsense.etl.configuration.FlatFileConfigSource;
import com.synapsense.etl.main.providers.SchedulerServiceProvider;
import com.synapsense.etl.scheduler.EtlScheduler;
import com.synapsense.etl.scheduler.SchedulerService;
import com.synapsense.service.AlertService;
import com.synapsense.service.Environment;
import com.synapsense.service.nsvc.NotificationService;

public class EtlApplicationModule extends AbstractModule {

	@Override
	protected void configure() {
		AbstractConfigSource etlProperties = new FlatFileConfigSource("etl", "test/func/standalone/etl.properties");
		
		AbstractConfigSource mainApp = new FlatFileConfigSource("internal", "conf/internal.properties");
		Configuration p = mainApp.getConfiguration();

		// pass ES login info to all components require this info
		bind(String.class).annotatedWith(Names.named("JNDI_URL")).toInstance(p.getOption("JNDI_URL"));
		bind(String.class).annotatedWith(Names.named("ES_LOGIN")).toInstance(p.getOption("ES_LOGIN"));
		bind(String.class).annotatedWith(Names.named("ES_PASSWORD")).toInstance(p.getOption("ES_PASSWORD"));

		Multibinder<ConfigSource> multibinder = Multibinder.newSetBinder(binder(), ConfigSource.class);
		multibinder.addBinding().toInstance(etlProperties);
		multibinder.addBinding().toInstance(mainApp);
		
		// --- exceptions
		String typeName = p.getOption("alert.type");
		String priority = p.getOption("alert.priority");
		String descr = p.getOption("alert.description");
		String header = p.getOption("alert.header");
		String body = p.getOption("alert.body");

		AlertType at = new AlertType(typeName, typeName, descr, priority, header, body, true, true, true);

		// StopEtLHandler init
		bind(EtlApplication.class).annotatedWith(Names.named("MainApp")).to(EtlApplication.class);

		// RaiseAlertHandler init
		bind(AlertService.class).annotatedWith(Names.named("AlertService")).toProvider(FakeAlertServiceProvider.class);
		bind(AlertType.class).annotatedWith(Names.named("EtlAlertType")).toInstance(at);
		
		// --- configuration 
		// ESConfigSource
		bind(NotificationService.class).annotatedWith(Names.named("ES notifier")).toProvider(FakeNvcProvider.class);
		bind(Environment.class).annotatedWith(Names.named("Environment")).toProvider(FakeEnvProvider.class);
		bind(AbstractConfigSource.class).annotatedWith(Names.named("ETL default configuration")).toInstance(mainApp);

		// EtlScheduler
		bind(org.quartz.Scheduler.class).toProvider(SchedulerServiceProvider.class);

		// ETL Application
		bind(ConfigurationProvider.class).annotatedWith(Names.named("Application configuration")).to(
		        ConfigurationProviderImpl.class);

		bind(SchedulerService.class).to(EtlScheduler.class);
	}
}
