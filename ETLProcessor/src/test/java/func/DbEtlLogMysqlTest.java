package func;

import com.synapsense.etl.core.DbEtlLog;
import com.synapsense.etl.core.EtlLog;
import com.synapsense.etl.entities.EtlSessionLog;
import org.dbunit.JdbcDatabaseTester;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.Calendar;
import java.util.Date;

import static org.junit.Assert.assertEquals;

public class DbEtlLogMysqlTest {
	private static JdbcDatabaseTester databaseTester;

	public static final String DB_DRIVER = "com.mysql.jdbc.Driver";
	public static final String DB_CONNECTION = "jdbc:mysql://localhost/synap_olap";
	public static final String DB_USER = "root";
	public static final String DB_PASSWORD = "dbuser";

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		databaseTester = new JdbcDatabaseTester(DB_DRIVER, DB_CONNECTION, DB_USER, DB_PASSWORD);
	}

	@AfterClass
	public static void teardownAfterClass() throws Exception {
		IDataSet databaseDataSet = getConnection().createDataSet(new String[] { "ETL_LOG" });
		DatabaseOperation.TRUNCATE_TABLE.execute(getConnection(), databaseDataSet);
	}

	@Test
	public void testGetOverallObjectsNum() {
		EntityManagerFactory f1 = Persistence.createEntityManagerFactory("etl_source");
		EntityManagerFactory f2 = Persistence.createEntityManagerFactory("etl_target");
		EtlLog log = new DbEtlLog(f1, f2);
		int num = log.getOverallObjectsNumber();
		assertEquals(0, num);
	}

	@Test
	public void testLog() {
		EntityManagerFactory f1 = Persistence.createEntityManagerFactory("etl_source");
		EntityManagerFactory f2 = Persistence.createEntityManagerFactory("etl_target");
		EtlLog log = new DbEtlLog(f1, f2);
		log.log(new EtlSessionLog(new Date(), new Date(), 1, 1000));

	}

	@Test
	public void testLastSuccessfulExportLog() throws Exception {
		DatabaseOperation.INSERT.execute(getConnection(),
		        new FlatXmlDataSet(getClass().getResourceAsStream("LastEtlSessionDataSet.xml")));

		EntityManagerFactory f1 = Persistence.createEntityManagerFactory("etl_source");
		EntityManagerFactory f2 = Persistence.createEntityManagerFactory("etl_target");
		EtlLog log = new DbEtlLog(f1, f2);
		EtlSessionLog record = log.getLastImported();

		Calendar c = Calendar.getInstance();
		c.set(Calendar.MILLISECOND, 0);
		c.set(2010, 9, 1, 12, 0, 0);
		assertEquals(c.getTime(), record.getEndDate());

		assertEquals(Integer.valueOf(1), record.getStartObject());
		assertEquals(Integer.valueOf(100), record.getEndObject());
	}

	@Test
	public void testFirstSuccessfulExportLog() throws Exception {
		DatabaseOperation.INSERT.execute(getConnection(),
		        new FlatXmlDataSet(getClass().getResourceAsStream("FirstEtlSessionDataSet.xml")));

		EntityManagerFactory f1 = Persistence.createEntityManagerFactory("etl_source");
		EntityManagerFactory f2 = Persistence.createEntityManagerFactory("etl_target");
		EtlLog log = new DbEtlLog(f1, f2);
		EtlSessionLog record = log.getFirstImported();

		Calendar c = Calendar.getInstance();
		c.set(Calendar.MILLISECOND, 0);
		c.set(2010, 8, 30, 12, 0, 0);
		assertEquals(c.getTime(), record.getStartDate());

		assertEquals(Integer.valueOf(1), record.getStartObject());
		assertEquals(Integer.valueOf(50), record.getEndObject());
	}

	@Test
	public void testFirstSuccessfulExportLogEmptyTable() throws Exception {

		EntityManagerFactory f1 = Persistence.createEntityManagerFactory("etl_source");
		EntityManagerFactory f2 = Persistence.createEntityManagerFactory("etl_target");
		EtlLog log = new DbEtlLog(f1, f2);
		EtlSessionLog record = log.getFirstImported();

		Calendar c = Calendar.getInstance();
		c.set(Calendar.MILLISECOND, 0);
		c.set(2010, 8, 30, 12, 0, 0);
		assertEquals(c.getTime(), record.getStartDate());

		assertEquals(Integer.valueOf(0), record.getStartObject());
		assertEquals(Integer.valueOf(0), record.getEndObject());
	}

	protected static IDatabaseConnection getConnection() throws Exception {
		return databaseTester.getConnection();
	}
}
