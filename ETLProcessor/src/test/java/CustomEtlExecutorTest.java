import java.io.File;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Map;

import scriptella.execution.EtlExecutor;
import scriptella.execution.EtlExecutorException;
import scriptella.interactive.ProgressIndicator;
import scriptella.util.IOUtils;

public class CustomEtlExecutorTest {
	public static void executeEtl(String name, Map<String, Object> p) throws MalformedURLException,
	        EtlExecutorException {

		EtlExecutor exec;
		exec = EtlExecutor.newExecutor(IOUtils.toUrl(new File(name)), p);
		exec.execute(new ProgressIndicator() {
			@Override
			public void showProgress(double progress, String message) {
			}
		});
	}

	public static void main(String[] args) throws MalformedURLException, EtlExecutorException {
		Map<String, Object> p = new HashMap<String, Object>();
		long start = System.currentTimeMillis();
		for (int i = 1; i < 1500; i += 50) {
			p.put("file.tmp", "export_" + i + "_" + (i + 50));
			p.put("etl.startobject", i);
			p.put("etl.endobject", i + 50);
			p.put("etl.startdate", "2010-09-08 00:00:00");
			p.put("etl.enddate", "2010-09-10 14:00:00");
			executeEtl("test//export.etl.xml", p);
		}

		System.out.println(System.currentTimeMillis() - start);
		// executeEtl("test//import.etl.xml");
	}
}
