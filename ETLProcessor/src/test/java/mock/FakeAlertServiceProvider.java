package mock;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

import org.apache.log4j.Logger;

import com.google.inject.Provider;
import com.synapsense.service.AlertService;

public class FakeAlertServiceProvider implements Provider<AlertService> {

	private static Logger logger = Logger.getLogger(AlertService.class);

	@Override
	public AlertService get() {
		class InvocationListener implements InvocationHandler {
			@Override
			public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
				if ("raiseAlert".equals(method.getName())) {
					logger.warn("Alert is raised up : " + args[0]);
				}

				return null;
			}
		}

		AlertService alertServiceProxy = (AlertService) Proxy.newProxyInstance(AlertService.class.getClassLoader(),
		        new Class[] { AlertService.class }, new InvocationListener());
		return alertServiceProxy;

	}

}
