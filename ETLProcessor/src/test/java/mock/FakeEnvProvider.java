package mock;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

import com.google.inject.Provider;
import com.synapsense.service.Environment;

public class FakeEnvProvider implements Provider<Environment> {

	@Override
	public Environment get() {
		Class<?> proxyClass = Proxy
		        .getProxyClass(Environment.class.getClassLoader(), new Class[] { Environment.class });
		Environment f = null;
		try {
			f = (Environment) proxyClass.getConstructor(new Class[] { InvocationHandler.class }).newInstance(
			        new Object[] { new Ih() });
		} catch (Exception e) {
			throw new IllegalStateException(e);
		}

		return f;
	}

	private static class Ih implements InvocationHandler {

		@Override
		public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
			if (method.getName().equals("createObject")) {

			}

			if (method.getName().equals("getPropertyValue")) {

			}
			return null;
		}

	}
}
