package mock;

import com.google.inject.Provider;
import com.synapsense.service.nsvc.EventProcessor;
import com.synapsense.service.nsvc.NotificationService;

public class FakeNvcProvider implements Provider<NotificationService> {

	@Override
	public NotificationService get() {
		return new NotificationService() {

			@Override
			public void registerProcessor(EventProcessor processor, EventProcessor filter) {
			}

			@Override
			public void deregisterProcessor(EventProcessor processor) {

			}
		};
	}
}
