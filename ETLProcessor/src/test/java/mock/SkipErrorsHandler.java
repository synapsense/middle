package mock;

import org.apache.log4j.Logger;

import com.synapsense.etl.EtlApplicationException;
import com.synapsense.etl.exceptions.OnErrorHandler;

public class SkipErrorsHandler implements OnErrorHandler {
	private static Logger logger = Logger.getLogger(SkipErrorsHandler.class);
	
	@Override
    public void onError(EtlApplicationException exception) {
		logger.warn("Skipping error : "  + exception.getMessage());
    }

}
