import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.security.auth.login.LoginException;

import com.synapsense.dto.ObjectType;
import com.synapsense.dto.PropertyDescr;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.exception.EnvException;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.exception.PropertyNotFoundException;
import com.synapsense.exception.UnableToConvertPropertyException;
import com.synapsense.service.Environment;
import com.synapsense.util.ServerLoginModule;

public class EnvironmentHelper {
	private static InitialContext ctx;
	private static Environment service;

	static {
		Properties environment = new Properties();
		environment.put(Context.INITIAL_CONTEXT_FACTORY, "org.jboss.naming.NamingContextFactory");
		environment.put(Context.URL_PKG_PREFIXES, "org.jboss.naming.client:org.jnp.interfaces");
		environment.put(Context.PROVIDER_URL, "jnp://localhost:1099");
		try {
			ctx = new InitialContext(environment);
		} catch (NamingException e) {
			throw new IllegalStateException("Cannot create initial context to connect ES services");
		}

		try {
			service = ServerLoginModule.getProxy(ctx, "SynapServer/Environment/remote", Environment.class);
		} catch (NamingException e) {
			throw new IllegalStateException("Error acquiring reference to ", e);
		}

		try {
			ServerLoginModule.login("admin", "admin");
		} catch (LoginException | NamingException e) {
			throw new IllegalStateException("Wrong credentials");
		}

	}

	public static void set(TO<?> id, String name, Object val) {
		try {
			service.setPropertyValue(id, name, val);
		} catch (ObjectNotFoundException e) {
			e.printStackTrace();
		} catch (PropertyNotFoundException e) {
			e.printStackTrace();
		} catch (UnableToConvertPropertyException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {

		try {
			StringBuilder bld = new StringBuilder();
			BufferedReader reader = new BufferedReader(new FileReader(new File("config/etl.properties")));
			try {
				String data;
				while ((data = reader.readLine()) != null) {
					if (bld.length() > 0)
						bld.append("\n");
					bld.append(data);
				}
				EnvironmentHelper.set(TOFactory.getInstance().loadTO("CONFIGURATION_DATA:5"), "config", bld.toString());
			} finally {
				reader.close();
			}
		} catch (IOException e) {
		}

		try {
			seed();
		} catch (EnvException e) {
			e.printStackTrace();
		}

	}

	public static void seed() throws EnvException {
		ObjectType ot = service.getObjectType("newtype");
		if (ot == null) {
			ot = service.createObjectType("newtype");
			ot.getPropertyDescriptors().add(new PropertyDescr("lastValue", Double.class.getName(), true));
			service.updateObjectType(ot);
		}

		TO to = service.createObject("newtype");

		for (int i = 0; i < 100; i++) {
			set(to, "lastValue", i + 0.1);
		}
	}

}
