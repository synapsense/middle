'==================
' Main Script Body
'==================
	
	Dim FSO, FolderName, FileName 
	Dim WshShell

	Dim strComputer       
	Dim strNamespace     
	Dim objWMIService    
	
	Set FSO = WScript.CreateObject("Scripting.FileSystemObject")
	Set WshShell = WScript.CreateObject("WScript.Shell")

	strComputer = "."
	strNamespace = "root\cimv2"
	Set objWMIService = GetObject("WinMgmts:{impersonationLevel=impersonate}!\\" & strComputer & "\root\cimv2")

	'==================
	' Checking for MySQL installation directory
	'==================

	WScript.Echo "Checking for MySQL"

	Dim processArch	
	processArch = WshShell.RegRead("HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\Session Manager\Environment\PROCESSOR_ARCHITECTURE")
	'On Error Resume Next
	if processArch = "AMD64" Then
		FolderName = WshShell.RegRead("HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\MySQL AB\MySQL Server 5.5\Location")
	else 
		FolderName = WshShell.RegRead("HKEY_LOCAL_MACHINE\SOFTWARE\MySQL AB\MySQL Server 5.5\Location")
	end if 

	if  Not(FSO.FolderExists(FolderName))  Then
  		WScript.Echo "SynapSense Archive Error", "Unable to find MySQL on this computer. Please contact SynapSense Support at support@synapsense.com"
		WScript.Quit(1)
	end if
	
	FileName = FolderName + "my.ini"
	
	if  Not(FSO.FileExists(FileName))  Then
  		WScript.Echo "SynapSense Archive Error", "Unable to find my.ini file at location specified in registry. Please contact SynapSense Support at support@synapsense.com"
		WScript.Quit(1)
	end if


	'* =========================
	'* Stopping MySQL
	'* =========================
	
	WScript.Echo "Stopping MySQL"

	Dim mysqlsvc 
	Set mysqlsvc = objWMIService.Get("Win32_Service.Name='MySQL'")
	if not(mysqlsvc.State = "Stopped") Then
		mysqlsvc.StopService()
	End If
	
	Dim colService		
	Dim objService 
	Dim stoped
	stoped = false

	Do while not(stoped)
	WScript.Sleep 1000
	Set colServices = objWMIService.ExecQuery("SELECT * FROM Win32_Service WHERE State = 'Stopped' and Name='MySQL'")
	For Each objService In colServices
	 stoped = true
	Next
	Loop 


	'* =========================
	'* Editing my.ini
	'* =========================
	
	Dim F, TextStream
	Const paramsNum=14
	Const ForReading = 1, ForAppending=8, TristateUseDefault = -2
	Const section = "mysqld",section2 = "mysql" 
	
	Dim key(15), value(15), testVal(15)
	key(0) = "innodb_flush_log_at_trx_commit"
	value(0)="2"
	key(1)="event_scheduler"
	value(1)="on"
	key(2)="innodb_file_per_table"
	 value(2)="1"
	
	'DataBase optimisation configration parameters
	key(3)="query_cache_size"
	value(3)="0"
	key(4)="innodb_additional_mem_pool_size"
	value(4)="12M"
	key(5)="innodb_log_buffer_size"
	value(5)="10M"
	
	key(6)="tmp_table_size"
	value(6)="16M"
	key(7)="max_heap_table_size"
	value(7)="16M"
	key(8)="read_buffer_size"
	value(8)="2M"
	key(9)="sort_buffer_size"
	value(9)="8M"
	key(10)="character-set-server"
	value(10)="utf8"
	key(11)="max_connections"
	value(11)="30"
	key(12)="max_allowed_packet"
	value(12)="16M"
	key(13)="innodb_buffer_pool_size"
	
	'* =========================
	'* Definition of innodb_buffer_pool_size (depends on RAM size)
	'* =========================
	Dim objComputer, colComputer
	Set colComputer = objWMIService.ExecQuery ("Select * from Win32_ComputerSystem")
	For Each objComputer in colComputer
   		intRamMB = int((objComputer.TotalPhysicalMemory) /1048576)
	Next
	
	value(13)=int((intRamMB) /4)	
	if ((processArch = "x86") and (value(13)>1500)) Then	
		value(13)= "1500"
	end if
	

	key(14)="innodb_log_file_size"
	value(14)=int(value(13)/4) & "M"
	value(13)=value(13) & "M"
	
	WScript.Echo "Enabling Event Scheduler"
	WriteINIString section, key(0), value(0), FileName
	WriteINIString section, "# Enable Event scheduler for auto-archive", " ", FileName
	WriteINIString section, key(1), value(1), FileName
	WScript.Echo "Setting Individual File Tables"
	WriteINIString section, "# Enable individual files for tables", " ", FileName 
	WriteINIString section, key(2), value(2), FileName
	For i=3 to paramsNum
	   WriteINIString section, key(i), value(i), FileName
	Next	
	
	'* adding dafault-character-set to the mysql client config
	key(15)="default-character-set"
	value(15)="utf8"

	WriteINIString section2, key(15), value(15), FileName

	'* =========================
	'* Checking my.ini (Read the value back to make sure it is correct)
	'* =========================
	
	WScript.Echo "Checking MySQL Config"	
	
	For i=0 to paramsNum
	   testVal(i) = GetINIString(section, key(i), " ", FileName)
	Next	
	
	For i=0 to paramsNum
		if not(testVal(i)=value(i)) Then
	  		WScript.Echo "SynapSense Archive Error", "Unable to write to my.ini file. Please contact SynapSense Support at support@synapsense.com"
			WScript.Quit(1)
		Exit for
		End If
	Next	

	testval(15) = GetINIString(section2, key(15), " ", FileName)
	if not(testVal(15)=value(15)) Then
		WScript.Echo "SynapSense Archive Error", "Unable to write to my.ini file. Please contact SynapSense Support at support@synapsense.com"
		WScript.Quit(1)
	End If
	
	'* =========================
	'* Renaming MySQL log files
	'* =========================

	Dim PathList
	PathList = GetINIString(section, "innodb_log_group_home_dir",  " ", FileName)
	if Len(PathList)<2 Then
		PathList = GetINIString(section, "datadir", " ", FileName)
	End If
	PathList = Replace(PathList,"/","\")
	PathList = Replace(PathList,Chr(34),"")
	Set F = FSO.GetFolder(PathList)
	Set Files = F.Files	
	
	intHighNumber = 10000
	intLowNumber = 1
	Randomize
    	intNumber = Int((intHighNumber - intLowNumber + 1) * Rnd + intLowNumber)

	
	For Each File In Files
  	  if (InStr(File.Name, "ib_logfile") and (Len(File.Name)<16))  Then
     	     FSO.MoveFile PathList & File.Name, PathList & File.Name & "_backup" & intNumber
	  end If
	Next

	'* =========================
	'* Starting MySQL
	'* =========================

	WScript.Echo "Checking MySQL Service. This may take up to 10 minutes..."

	Set mysqlsvc = objWMIService.Get("Win32_Service.Name='MySQL'")
	if not(mysqlsvc.State = "Started") Then
		mysqlsvc.StartService()
	End If
	
	Dim started
	started = false
	iter=0

	Do Until (iter>600) or (started)
		WScript.Sleep 1000
		Set colServices = objWMIService.ExecQuery("SELECT * FROM Win32_Service WHERE State = 'Running' and Name='MySQL' and Status='OK'")

		For Each objService In colServices
			started = true
	Next
		iter=iter+1
	Loop 
		
	mySqlPid = 0
	Do Until mySqlPid>0
		WScript.Sleep 1000
		if iter>600 Then
	  		WScript.Echo "Unable to start MySQL service. Please contact SynapSense Support at support@synapsense.com"
			WScript.Quit(1)
		End If
		mySqlPid = WshShell.run ("cmd /c " & FolderName & "/bin/mysql.exe", 0, true)
		iter=iter+1
	Loop

	WScript.Echo "MySQL configuration complete"


'* =========================
'* End Main Script Body
'* =========================


'* =========================
'* Function GetINIString(Section, KeyName, Default, FileName)
'* sub WriteINIString(Section, KeyName, Value, FileName)
'* =========================
	
Sub WriteINIString(Section, KeyName, Value, FileName)
  Dim INIContents, PosSection, PosEndSection
  
  'Get contents of the INI file As a string
  INIContents = GetFile(FileName)

  'Find section
  PosSection = InStr(1, INIContents, "[" & Section & "]", vbTextCompare)
  If PosSection>0 Then
    'Section exists. Find end of section
    PosEndSection = InStr(PosSection, INIContents, vbCrLf & "[")
    '?Is this last section?
    If PosEndSection = 0 Then PosEndSection = Len(INIContents)+1
    
    'Separate section contents
    Dim OldsContents, NewsContents, Line
    Dim sKeyName, Found
    OldsContents = Mid(INIContents, PosSection, PosEndSection - PosSection)
    OldsContents = split(OldsContents, vbCrLf)

    'Temp variable To find a Key
    sKeyName = LCase(KeyName & "=")

    'Enumerate section lines
    For Each Line In OldsContents
      If LCase(Left(Line, Len(sKeyName))) = sKeyName Then
        Line = KeyName & "=" & Value
        Found = True
      End If
      NewsContents = NewsContents & Line & vbCrLf
    Next

    If isempty(Found) Then
      'key Not found - add it at the end of section
      NewsContents = NewsContents & KeyName & "=" & Value
    Else
      'remove last vbCrLf - the vbCrLf is at PosEndSection
      NewsContents = Left(NewsContents, Len(NewsContents) - 2)
    End If

    'Combine pre-section, new section And post-section data.
    INIContents = Left(INIContents, PosSection-1) & _
      NewsContents & Mid(INIContents, PosEndSection)
  else'if PosSection>0 Then
    'Section Not found. Add section data at the end of file contents.
    If Right(INIContents, 2) <> vbCrLf And Len(INIContents)>0 Then 
      INIContents = INIContents & vbCrLf 
    End If
    INIContents = INIContents & "[" & Section & "]" & vbCrLf & _
      KeyName & "=" & Value
  end if'if PosSection>0 Then
  WriteFile FileName, INIContents
End Sub

Function GetINIString(Section, KeyName, Default, FileName)
  Dim INIContents, PosSection, PosEndSection, sContents, Value, Found
  
  'Get contents of the INI file As a string
  INIContents = GetFile(FileName)

  'Find section
  PosSection = InStr(1, INIContents, "[" & Section & "]", vbTextCompare)
  If PosSection>0 Then
    'Section exists. Find end of section
    PosEndSection = InStr(PosSection, INIContents, vbCrLf & "[")
    '?Is this last section?
    If PosEndSection = 0 Then PosEndSection = Len(INIContents)+1
    
    'Separate section contents
    sContents = Mid(INIContents, PosSection, PosEndSection - PosSection)

    If InStr(1, sContents, vbCrLf & KeyName & "=", vbTextCompare)>0 Then
      Found = True
      'Separate value of a key.
      Value = SeparateField(sContents, vbCrLf & KeyName & "=", vbCrLf)
    End If
  End If
  If isempty(Found) Then Value = Default
  GetINIString = Value
End Function

'Separates one field between sStart And sEnd
Function SeparateField(ByVal sFrom, ByVal sStart, ByVal sEnd)
  Dim PosB: PosB = InStr(1, sFrom, sStart, 1)
  If PosB > 0 Then
    PosB = PosB + Len(sStart)
    Dim PosE: PosE = InStr(PosB, sFrom, sEnd, 1)
    If PosE = 0 Then PosE = InStr(PosB, sFrom, vbCrLf, 1)
    If PosE = 0 Then PosE = Len(sFrom) + 1
    SeparateField = Mid(sFrom, PosB, PosE - PosB)
  End If
End Function


'File functions
Function GetFile(ByVal FileName)
  Dim FS: Set FS = CreateObject("Scripting.FileSystemObject")
  'Go To windows folder If full path Not specified.
  If InStr(FileName, ":\") = 0 And Left (FileName,2)<>"\\" Then 
    FileName = FS.GetSpecialFolder(0) & "\" & FileName
  End If
  On Error Resume Next

  GetFile = FS.OpenTextFile(FileName).ReadAll
End Function

Function WriteFile(ByVal FileName, ByVal Contents)
  
  Dim FS: Set FS = CreateObject("Scripting.FileSystemObject")
  'On Error Resume Next

  'Go To windows folder If full path Not specified.
  If InStr(FileName, ":\") = 0 And Left (FileName,2)<>"\\" Then 
    FileName = FS.GetSpecialFolder(0) & "\" & FileName
  End If

  Dim OutStream: Set OutStream = FS.OpenTextFile(FileName, 2, True)
  OutStream.Write Contents
  OutStream.Close 	
End Function
