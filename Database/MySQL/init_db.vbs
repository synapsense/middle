Dim mysqlPath, rootPassword
CustomActionArray = Split(Session.Property("CustomActionData"), ";", 2)
mysqlPath = CustomActionArray(0)
rootPassword = CustomActionArray(1)
mysqlScriptsPath = mysqlPath & "scripts\"
rootUser="root"
scripts = Array("create_db.sql", "seed_db.sql", "partition_historical_values.sql")

SET WshShell = CreateObject("WScript.Shell")
FOR EACH script IN scripts
	command = chr(34) & mysqlPath & "bin\mysql.exe" & chr(34) & " --user=" & rootUser & " -p" & rootPassword & " -e " & chr(34) & "source " & mysqlScriptsPath & script & chr(34)
	WshShell.Run command, 0, True
NEXT

