use synap;

UPDATE tbl_user_preferences_assoc t, tbl_user_preferences p SET t.preference_value='"en_US"' where p.preference_name = "SavedLanguage" and p.preference_id=t.preference_id and t.preference_value='"En"';
UPDATE tbl_user_preferences_assoc t, tbl_user_preferences p SET t.preference_value='"ru_RU"' where p.preference_name = "SavedLanguage" and p.preference_id=t.preference_id and t.preference_value='"Ru"';