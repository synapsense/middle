ALTER table `tbl_network_statbase` modify `network_id` INT(11) UNSIGNED;
ALTER table `tbl_network_statbase` modify `logical_id` INT(11) UNSIGNED;
ALTER table `tbl_network_statnode` modify `network_id` INT(11) UNSIGNED;
ALTER table `tbl_network_statnode` modify `logical_id` INT(11) UNSIGNED;