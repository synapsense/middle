use `synap`;

delimiter //

ALTER EVENT `archive_data`
DO
BEGIN
-- default schema veriable
DECLARE SCHEMA_NAME VARCHAR(10) default 'synap';
-- stores the name of the partition with lower range
DECLARE lastPartitionName VARCHAR(3);
-- the time point -  determines the sensor data that has to be archived
DECLARE oldDataTimePoint DATE;
-- the time point - upper border limit of the new partiotion range(in days)
DECLARE newDataTimePoint INT;

DECLARE maxRangePartitionName VARCHAR(3);
DECLARE maxRangePartitionDataTimePoint DATE;


CREATE TABLE IF NOT EXISTS `ARCHIVE_SENSOR_DATA` (
  `sensor_id` int(11) COMMENT 'The data request id that triggered this collection',
  `sensor_node_data` double(15,6) COMMENT 'Actual value of the measurement captured by the sensor node sensor.',
  `data_time_stamp` datetime COMMENT 'The timestamp when the data was collected by the sensor node'
)
ENGINE=ARCHIVE;

CREATE TABLE IF NOT EXISTS `ARCHIVE_HISTORICAL_VALUES` (
  `value_id_fk` int(11) NOT NULL,
  `timestamp` datetime NOT NULL,
  `valuei` int(11),
  `valued` double(15,3),
  `valuel` bigint(20),
  `valuev` varchar(21833)
)
ENGINE=ARCHIVE;

-- sensor data
-- retrieves date and partition name of the partition with last ordinal pos (means the partition with lower range border )
-- implements cycle buffer of partition names
call GET_PARTITION_INFO(SCHEMA_NAME,'tbl_sensor_data',oldDataTimePoint,lastPartitionName);

-- moves online sensor data from the last partiotion(6 months old data) to the archive
INSERT INTO ARCHIVE_SENSOR_DATA ( `sensor_id`, `sensor_node_data`, `data_time_stamp`)
(select `sensor_id`, `sensor_node_data`, `data_time_stamp`
   from `tbl_sensor_data`
   where `data_time_stamp` < oldDataTimePoint);

call DROP_PARTITION(SCHEMA_NAME,'tbl_sensor_data',lastPartitionName);

-- retrieves the upper range border of the new partition
set newDataTimePoint = GET_PARTITION_UPPER_RANGE_BORDER(SCHEMA_NAME,'tbl_sensor_data');

-- retrieves info about partition with max range border
call GET_MAX_RANGE_PARTITION_INFO(SCHEMA_NAME,'tbl_sensor_data',maxRangePartitionDataTimePoint,maxRangePartitionName);

call CREATE_PARTITION(SCHEMA_NAME,'tbl_sensor_data',lastPartitionName,newDataTimePoint,maxRangePartitionName,TO_DAYS(maxRangePartitionDataTimePoint));

-- historical values
-- retrieves date and partition name of the partition with last ordinal pos (means the partition with lower range border )
-- implements cycle buffer of partition names
call GET_PARTITION_INFO(SCHEMA_NAME,'historical_values',oldDataTimePoint,lastPartitionName);

-- moves online sensor data from the last partiotion(6 months old data) to the archive
INSERT INTO ARCHIVE_HISTORICAL_VALUES (`value_id_fk`, `timestamp`,`valuei`,`valued`,`valuel`,`valuev`)
(select `value_id_fk`, `timestamp`,`valuei`,`valued`,`valuel`,`valuev`   from `historical_values`
   where `timestamp` < oldDataTimePoint);

call DROP_PARTITION(SCHEMA_NAME,'historical_values',lastPartitionName);

-- retrieves the upper range border of the new partition
set newDataTimePoint = GET_PARTITION_UPPER_RANGE_BORDER(SCHEMA_NAME,'historical_values');

-- retrieves info about partition with max range border
call GET_MAX_RANGE_PARTITION_INFO(SCHEMA_NAME,'historical_values',maxRangePartitionDataTimePoint,maxRangePartitionName);

call CREATE_PARTITION(SCHEMA_NAME,'historical_values',lastPartitionName,newDataTimePoint,maxRangePartitionName,TO_DAYS(maxRangePartitionDataTimePoint));

END//
delimiter ;

alter table ARCHIVE_HISTORICAL_VALUES drop column id; 
alter table ARCHIVE_HISTORICAL_VALUES modify valuev varchar(21833);

