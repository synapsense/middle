-- Script is used for creating new properties for Alert Priority, which is needed for Audio-Visual notification performer
-- This script is consisted in migrate_alert_priority script. Don't use this script if you execute migrate_alert_priority.
use synap;
DELIMITER |
CREATE PROCEDURE insert_int_value(
  in_object_id int, in_property_id int, in_timestamp long, valuei int)
  BEGIN
    DECLARE valueId   int;

    SET valueId =
          insert_object_value(
            in_object_id, in_property_id, in_timestamp);

    INSERT INTO valuei(
                  value_id_fk, value)
    VALUES (valueId, valuei);
  END |
DELIMITER ;

DELIMITER | 
CREATE FUNCTION insert_object_value(
  in_object_id int, in_property_id int, in_timestamp long)
  RETURNS INTEGER
  BEGIN
    set @id=0;
    SELECT value_id into @id 
        from obj_values
        where object_id_fk=in_object_id and attr_id_fk=in_property_id limit 1;
        
    IF @id!=0
    THEN 
        RETURN @id;
    END IF;
  
    INSERT INTO obj_values(
                  object_id_fk, attr_id_fk, TIMESTAMP)
    VALUES (in_object_id, in_property_id, in_timestamp);

    RETURN last_insert_id();
  END|
DELIMITER ; 

DELIMITER |
CREATE PROCEDURE insert_long_value(
  in_object_id int, in_property_id int, in_timestamp long, valuel long)
  BEGIN
    DECLARE valueId   int;

    SET valueId =
          insert_object_value(
            in_object_id, in_property_id, in_timestamp);

    INSERT INTO valuel(
                  value_id_fk, value)
    VALUES (valueId, valuel);
  END|
DELIMITER ;

select objtype_id into @objTypeId from `object_types` where name = 'ALERT_PRIORITY' limit 1;

delimiter |
create procedure update_priority()
    begin
	
        declare objId int;
        declare done int;
        declare cur_priority cursor for select id from objects where object_type_id_fk=@objTypeId and static = 0;
					
        DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

		 

        insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('redSignal', @objTypeId, 1);
        set @redSignalId=last_insert_id();
        insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('greenSignal', @objTypeId, 1);
        set @greenSignalId=last_insert_id();
        insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('yellowSignal', @objTypeId, 1);
        set @yellowSignalId=last_insert_id();
        insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('audioSignal', @objTypeId, 1);
        set @audioSignalId=last_insert_id();
        insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('lightDuration', @objTypeId, 7);
        set @lightDurationId=last_insert_id();
        insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('audioDuration', @objTypeId, 7);
        set @audioDurationId=last_insert_id();

        open cur_priority;

        main:
        loop
            fetch cur_priority
                into objId;
            IF done
            THEN
                LEAVE main;
            END IF;    
            select timestamp into @currentTime  from `obj_values` where object_id_fk = objId limit 1;
            call insert_int_value(objId, @redSignalId, @currentTime, 0);
            call insert_int_value(objId, @greenSignalId, @currentTime, 0);
            call insert_int_value(objId, @yellowSignalId, @currentTime, 0);
            call insert_int_value(objId, @audioSignalId, @currentTime, 0);
            call insert_long_value(objId, @lightDurationId, @currentTime, 0);
            call insert_long_value(objId, @audioDurationId, @currentTime, 0);
                   

        end loop;
			
        CLOSE cur_priority;
	end;|
delimiter ;

call update_priority();

-- renew ids 
SET @max_object_id := IFNULL((SELECT max(`id`)+1 FROM `objects`),1);
SET @max_value_id := IFNULL((SELECT max(`value_id`)+1 FROM `obj_values`),1);
SET @max_valued_id := IFNULL((SELECT max(`id`)+1 FROM `valued`),1);
SET @max_valuel_id := IFNULL((SELECT max(`id`)+1 FROM `valuel`),1);
SET @max_valuei_id := IFNULL((SELECT max(`id`)+1 FROM `valuei`),1);
SET @max_valuev_id := IFNULL((SELECT max(`id`)+1 FROM `valuev`),1);
SET @max_valuelink_id := IFNULL((SELECT max(`id`)+1 FROM `value_obj_link`),1);
SET @max_valueplink_id := IFNULL((SELECT max(`id`)+1 FROM `value_property_link`),1);
SET @max_tag_value_id := IFNULL((SELECT max(`value_id`)+1 FROM `tag_values`),1);
SET @max_group_value_id := IFNULL((SELECT max(`group_id`)+1 FROM `historical_values`),1);

delete from uid_table;
INSERT INTO `uid_table` (`object_id`,`value_id`,`valued_id`,`valuev_id`,`valuei_id`,`valuel_id`,`valuelink_id`,`valueplink_id`, `tag_value_id`, `group_id`) values (@max_object_id,@max_value_id,@max_valued_id,@max_valuev_id,@max_valuei_id,@max_valuel_id,@max_valuelink_id,@max_valueplink_id,@max_tag_value_id,@max_group_value_id);

drop procedure if exists update_priority;
drop procedure if exists insert_int_value;
drop function if exists insert_object_value;
drop procedure if exists insert_long_value;
