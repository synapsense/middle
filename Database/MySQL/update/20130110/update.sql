use synap;

update valuev as vv set vv.value = CASE 
-- protects from double update 
WHEN vv.value regexp 'isEveryonePermitted' THEN vv.value
WHEN vv.value is null THEN ''
WHEN vv.value = '' THEN ''
WHEN vv.value = '__everyone' THEN '{isEveryonePermitted: true}' 
ELSE concat('{isEveryonePermitted: false, users: [', concat('"',REPLACE(vv.value, ',', '","'),'"'), ']}')
END
where vv.value_id_fk in 
(select value_id from obj_values as ov, objects as o, object_types as ot, attributes as a 
where o.id = ov.object_id_fk 
and ov.attr_id_fk = a.attr_id
and o.object_type_id_fk = ot.objtype_id 
and a.object_type_id_fk = ot.objtype_id 
and a.name = 'permissions' 
and ot.name in ('DC','ZONE')
);