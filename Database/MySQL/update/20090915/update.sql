use `synap`;
-- Drop Event clear_exceeded_data
DROP EVENT IF EXISTS `clear_expired_data`;


DELIMITER |
CREATE EVENT `clear_expired_data`
  ON SCHEDULE EVERY '1' MONTH STARTS '2009-10-01 00:01:00'
  ENABLE
DO
BEGIN
delete from tbl_nht_info
where report_timestamp <= DATE_SUB(CURDATE(),INTERVAL 1 MONTH);
END; |
DELIMITER ;