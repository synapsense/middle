Run update.bat database upgrade script to upgrade Application Server database schema 
to the latest version. 

Command line options:
    update.bat <username> <password> <es_version_to_update>


Examples:
// Run upgrade script using root/dbuser database username and password. 
// Starting version of the Application Server database schema is 5.3
update.bat root dbuser 5.3

// Run upgrade script using root/dbuser database username and password. 
// Starting version of the Application Server database schema is 5.0.0
update.bat root dbuser  5.0.0

// You may use output redirection like this to capture output into the log file
update.bat root dbuser 5.0.4 1>logfile.log 2>&1


Notes:
    All database update scripts are required for the successful upgrade process. If some of the files are missing the upgrade process will fail.

    Partitions start date is obtained from MySQL database using INFORMATION_SCHEMA. If current database schema has no partitioned tables,
the current date will be used as start date for the first partition.





