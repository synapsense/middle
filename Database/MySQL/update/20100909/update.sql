-- predefined type CONFIGURATION_DATA to avoid EJBAccessException cause createObjectType is accessible only by users with 'admin' role
start transaction;

INSERT INTO  `object_types` (`name`) values ('CONFIGURATION_DATA');
INSERT INTO  `objects` (`object_type_id_fk`,`name`,`static`) values ((select objtype_id from `object_types` where name = 'CONFIGURATION_DATA'),'CONFIGURATION_DATA',1);
INSERT INTO  `attributes` (`object_type_id_fk`,`name`,`attr_type_id_fk`) values ((select objtype_id from `object_types` where name = 'CONFIGURATION_DATA'),'name',3);
INSERT INTO  `attributes` (`object_type_id_fk`,`name`,`attr_type_id_fk`) values ((select objtype_id from `object_types` where name = 'CONFIGURATION_DATA'),'config',3);

commit;