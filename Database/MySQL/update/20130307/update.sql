-- Bug#8306
-- The script creates a brand new reporting object model

use synap;

-- create auxiliary functions & procedures
DROP FUNCTION IF EXISTS insert_object_value;
DELIMITER | 
CREATE FUNCTION insert_object_value(
  in_object_id int, in_property_id int, in_timestamp long)
  RETURNS INTEGER
  BEGIN
    set @id=0;
    SELECT value_id into @id 
        from obj_values
        where object_id_fk=in_object_id and attr_id_fk=in_property_id limit 1;
        
    IF @id!=0
    THEN 
        RETURN @id;
    END IF;

    INSERT INTO obj_values(
                  object_id_fk, attr_id_fk, TIMESTAMP)
    VALUES (in_object_id, in_property_id, in_timestamp);

    RETURN last_insert_id();
  END|
DELIMITER ; 

DROP PROCEDURE IF EXISTS insert_varchar_value;

DELIMITER |
CREATE PROCEDURE insert_varchar_value(
  in_object_id int, in_property_id int, in_timestamp long, valuev varchar(20000))
  BEGIN
    DECLARE valueId   INT;

    SET valueId =
          insert_object_value(
            in_object_id, in_property_id, in_timestamp);

    INSERT INTO valuev(
                  value_id_fk, VALUE)
    VALUES (valueId, valuev);
  END|

DELIMITER ;

DROP PROCEDURE IF EXISTS insert_double_value;

DELIMITER |

CREATE PROCEDURE insert_double_value(
  in_object_id int, in_property_id int, in_timestamp long, valued double) 
  BEGIN
    DECLARE valueId   int;

    SET valueId =
          insert_object_value(
            in_object_id, in_property_id, in_timestamp);

    INSERT INTO valued(
                  value_id_fk, value)
    VALUES (valueId, valued);

  END|

DELIMITER ;

DROP PROCEDURE IF EXISTS insert_int_value;

DELIMITER |
CREATE PROCEDURE insert_int_value(
  in_object_id int, in_property_id int, in_timestamp long, valuei int)
  BEGIN
    DECLARE valueId   int;

    SET valueId =
          insert_object_value(
            in_object_id, in_property_id, in_timestamp);

    INSERT INTO valuei(
                  value_id_fk, value)
    VALUES (valueId, valuei);
  END|
DELIMITER ;

DROP PROCEDURE IF EXISTS insert_long_value;

DELIMITER |
CREATE PROCEDURE insert_long_value(
  in_object_id int, in_property_id int, in_timestamp long, valuel long)
  BEGIN
    DECLARE valueId   int;

    SET valueId =
          insert_object_value(
            in_object_id, in_property_id, in_timestamp);

    INSERT INTO valuel(
                  value_id_fk, value)
    VALUES (valueId, valuel);
  END|
DELIMITER ;

DROP PROCEDURE IF EXISTS insert_link_value;

DELIMITER |
CREATE PROCEDURE insert_link_value(
  in_object_id int, in_property_id int, in_timestamp long, value_link int)
  BEGIN
    DECLARE valueId   int;
    DECLARE typeName  varchar(300);

    SET valueId =
          insert_object_value(
            in_object_id, in_property_id, in_timestamp);

    CALL add_link_value(valueId, in_timestamp, value_link);
  END|
DELIMITER ;

DROP PROCEDURE IF EXISTS add_link_value;

DELIMITER |
CREATE PROCEDURE add_link_value(
  valueId int, in_timestamp long, value_link int)
  BEGIN
    DECLARE typeName  varchar(300);
            
    IF (value_link IS NULL) THEN
      INSERT INTO value_obj_link(
                  value_id_fk, object_id,object_type)
      VALUES (valueId, NULL, NULL);
    ELSE
      SELECT ot.NAME INTO typeName FROM objects o , object_types ot WHERE o.object_type_id_fk = ot.objtype_id
      AND o.id = value_link;

      INSERT INTO value_obj_link(value_id_fk, object_id, object_type)
      VALUES (valueId, value_link, typeName);

    END IF;
    
  END|
DELIMITER ;

DROP PROCEDURE IF EXISTS insert_binarydata_value;

DELIMITER |
CREATE PROCEDURE insert_binarydata_value(
  in_object_id int, in_property_id int, in_timestamp long, value_binary mediumblob)
  BEGIN
    DECLARE valueId   INT;

    SET valueId =
          insert_object_value(
            in_object_id, in_property_id, in_timestamp);

    INSERT INTO value_binary(
                  value_id_fk, VALUE)
    VALUES (valueId, value_binary);
  END|
 DELIMITER ;

set @currentTime = UNIX_TIMESTAMP() * 1000;




-- create type 'REPORT_TEMPLATE'
INSERT INTO object_types(name) VALUES ('REPORT_TEMPLATE');
    
SET @lastTypeId := last_insert_id();
    
INSERT INTO objects(name, object_type_id_fk, static) VALUES ('REPORT_TEMPLATE', @lastTypeId, 1);
        
insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('name', @lastTypeId, 3);
set @namePropertyId = last_insert_id(); 
insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('design', @lastTypeId, 8);
set @designPropertyId = last_insert_id(); 
insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('owner', @lastTypeId, 3);
set @ownerPropertyId = last_insert_id(); 
insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('private', @lastTypeId, 1);
set @privatePropertyId = last_insert_id(); 
insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('system', @lastTypeId, 1);
set @systemPropertyId = last_insert_id(); 
insert into attributes(name, object_type_id_fk, attr_type_id_fk, collection) values ('parameters', @lastTypeId, 5, 1);
set @parametersPropertyId = last_insert_id(); 

-- Tabular Custom Query
INSERT INTO `objects` (`object_type_id_fk`,`name`,`static`) VALUES (@lastTypeId,null,0);
set @tableTemplateId = last_insert_id();
CALL insert_varchar_value(@tableTemplateId, @namePropertyId, @currentTime, 'Tabular Custom Query');
CALL insert_binarydata_value(@tableTemplateId, @designPropertyId, @currentTime, BINARY '<?xml version="1.0" encoding="UTF-8"?>\r\n<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="Tabular Custom Query" language="groovy" pageWidth="842" pageHeight="595" orientation="Landscape" columnWidth="802" leftMargin="20" rightMargin="20" topMargin="20" bottomMargin="20" uuid="6e83898a-70df-4a86-a40a-703eb2898507">\r\n	<property name="ireport.zoom" value="1.0"/>\r\n	<property name="ireport.x" value="0"/>\r\n	<property name="ireport.y" value="0"/>\r\n	<style name="Title" fontName="Arial" fontSize="26" isBold="true" pdfFontName="Helvetica-Bold"/>\r\n	<style name="SubTitle" forecolor="#666666" fontName="Arial" fontSize="18"/>\r\n	<style name="Column header" forecolor="#666666" fontName="Arial" fontSize="12" isBold="true"/>\r\n	<style name="Detail" fontName="Arial" fontSize="12"/>\r\n	<style name="table">\r\n		<box>\r\n			<pen lineWidth="1.0" lineColor="#000000"/>\r\n		</box>\r\n	</style>\r\n	<style name="table_TH" mode="Opaque" backcolor="#F0F8FF">\r\n		<box>\r\n			<pen lineWidth="0.5" lineColor="#000000"/>\r\n		</box>\r\n	</style>\r\n	<style name="table_CH" mode="Opaque" backcolor="#BFE1FF">\r\n		<box>\r\n			<pen lineWidth="0.5" lineColor="#000000"/>\r\n		</box>\r\n	</style>\r\n	<style name="table_TD" mode="Opaque" backcolor="#FFFFFF">\r\n		<box>\r\n			<pen lineWidth="0.5" lineColor="#000000"/>\r\n		</box>\r\n	</style>\r\n	<style name="table 1" hAlign="Center" vAlign="Top" fontName="Arial" isItalic="true" isUnderline="true" isStrikeThrough="true">\r\n		<pen lineColor="#004574"/>\r\n		<box>\r\n			<pen lineWidth="1.0" lineColor="#004574"/>\r\n			<topPen lineWidth="1.0" lineColor="#004574"/>\r\n			<leftPen lineWidth="1.0" lineColor="#004574"/>\r\n			<bottomPen lineWidth="1.0" lineColor="#004574"/>\r\n			<rightPen lineWidth="1.0" lineColor="#004574"/>\r\n		</box>\r\n	</style>\r\n	<style name="table 1_TH" mode="Opaque" backcolor="#F0F8FF">\r\n		<box>\r\n			<pen lineWidth="0.5" lineColor="#004574"/>\r\n			<topPen lineColor="#004574"/>\r\n			<leftPen lineColor="#004574"/>\r\n			<bottomPen lineColor="#004574"/>\r\n			<rightPen lineColor="#004574"/>\r\n		</box>\r\n	</style>\r\n	<style name="table 1_CH" mode="Opaque" backcolor="#E6E6E6">\r\n		<box>\r\n			<pen lineWidth="0.5" lineColor="#004574"/>\r\n			<topPen lineColor="#004574"/>\r\n			<leftPen lineColor="#004574"/>\r\n			<bottomPen lineColor="#004574"/>\r\n			<rightPen lineColor="#004574"/>\r\n		</box>\r\n	</style>\r\n	<style name="table 1_TD" mode="Opaque" backcolor="#FFFFFF" hAlign="Left" pattern="">\r\n		<box>\r\n			<pen lineWidth="0.5" lineColor="#004574"/>\r\n			<topPen lineColor="#004574"/>\r\n			<leftPen lineColor="#004574"/>\r\n			<bottomPen lineColor="#004574"/>\r\n			<rightPen lineColor="#004574"/>\r\n		</box>\r\n		<conditionalStyle>\r\n			<conditionExpression><![CDATA[new Boolean($V{REPORT_COUNT}.intValue()%2==0)]]></conditionExpression>\r\n			<style backcolor="#F3F6F8"/>\r\n		</conditionalStyle>\r\n	</style>\r\n	<subDataset name="Intake Table" uuid="975d2a89-db86-4532-8d91-d82a25dbe3b3">\r\n		<field name="Object Name" class="java.lang.String"/>\r\n		<field name="Data Type" class="java.lang.String"/>\r\n		<field name="Data Value" class="java.lang.String"/>\r\n		<field name="Timestamp" class="java.lang.String"/>\r\n	</subDataset>\r\n	<parameter name="Custom Query ID" class="java.lang.String">\r\n        <parameterDescription><![CDATA[ {"uiComponentType": "CustomQuerySelector"}]]></parameterDescription>\r\n	</parameter>\r\n	<parameter name="Report Title" class="java.lang.String"/>\r\n	<parameter name="Report Period (hours)" class="java.lang.Long"/>\r\n	<title>\r\n		<band height="30" splitType="Stretch">\r\n			<textField pattern="" isBlankWhenNull="true">\r\n				<reportElement uuid="7abf160c-87af-43c5-8266-c1ec0484f139" style="Title" mode="Opaque" x="154" y="0" width="648" height="26" forecolor="#004574" backcolor="#E6E6E6"/>\r\n				<textElement textAlignment="Center" verticalAlignment="Middle">\r\n					<font size="16"/>\r\n				</textElement>\r\n				<textFieldExpression><![CDATA[$P{Report Title}]]></textFieldExpression>\r\n			</textField>\r\n			<image scaleImage="RetainShape" vAlign="Middle" isUsingCache="true">\r\n				<reportElement uuid="d5eceb83-3d12-45ed-a919-4182696b7965" mode="Opaque" x="0" y="0" width="154" height="26" forecolor="#E6E6E6" backcolor="#E6E6E6"/>\r\n				<imageExpression><![CDATA["http://localhost:8080/synapsoft/resources/images/slogo_g.png"]]></imageExpression>\r\n			</image>\r\n			<rectangle>\r\n				<reportElement uuid="1d301550-0f1e-4454-a939-6f214f98c4ec" x="0" y="26" width="801" height="4" forecolor="#004574" backcolor="#004574"/>\r\n			</rectangle>\r\n		</band>\r\n	</title>\r\n	<detail>\r\n		<band height="495">\r\n			<componentElement>\r\n				<reportElement uuid="3b3b8fee-7a60-4c6d-91c5-3f07b06d0a32" key="table 1" style="table 1" stretchType="RelativeToTallestObject" isPrintRepeatedValues="false" mode="Transparent" x="25" y="8" width="755" height="50" isRemoveLineWhenBlank="true" backcolor="#E6E6E6"/>\r\n				<jr:table xmlns:jr="http://jasperreports.sourceforge.net/jasperreports/components" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports/components http://jasperreports.sourceforge.net/xsd/components.xsd">\r\n					<datasetRun subDataset="Intake Table" uuid="13ad075d-f787-449e-aa49-befe4517e642">\r\n						<dataSourceExpression><![CDATA[new com.synapsense.service.reporting.datasource.TabularCustomQueryDataSource(com.synapsense.dto.TOFactory.getInstance().loadTO($P{Custom Query ID}), new java.util.Date(System.currentTimeMillis() - $P{Report Period (hours)} * 60 * 60* 1000), new java.util.Date())]]></dataSourceExpression>\r\n					</datasetRun>\r\n					<jr:column width="200" uuid="f2eeb8f4-684c-4529-90d7-3ed1a3eac5c1">\r\n						<jr:columnHeader style="table 1_CH" height="31" rowSpan="1">\r\n							<staticText>\r\n								<reportElement uuid="980e6b59-26b2-401e-ab0f-85f7274c436f" positionType="Float" x="10" y="0" width="180" height="30" forecolor="#004574"/>\r\n								<textElement textAlignment="Center" verticalAlignment="Middle">\r\n									<font fontName="Arial" size="12" isBold="true"/>\r\n									<paragraph tabStopWidth="10"/>\r\n								</textElement>\r\n								<text><![CDATA[Object Name]]></text>\r\n							</staticText>\r\n						</jr:columnHeader>\r\n						<jr:detailCell style="table 1_TD" height="21" rowSpan="1">\r\n							<textField isBlankWhenNull="true">\r\n								<reportElement uuid="6ee1268b-37a4-4a60-96fb-914b76da610d" positionType="Float" stretchType="RelativeToTallestObject" x="0" y="0" width="200" height="21"/>\r\n								<textElement textAlignment="Left" verticalAlignment="Middle">\r\n									<font fontName="Arial"/>\r\n									<paragraph leftIndent="2" tabStopWidth="10"/>\r\n								</textElement>\r\n								<textFieldExpression><![CDATA[$F{Object Name}]]></textFieldExpression>\r\n							</textField>\r\n						</jr:detailCell>\r\n					</jr:column>\r\n					<jr:column width="200" uuid="2bbcc08a-616f-4b91-8693-3baeca6b8573">\r\n						<jr:columnHeader style="table 1_CH" height="31" rowSpan="1">\r\n							<staticText>\r\n								<reportElement uuid="9b22dab4-8390-4980-a428-66c96242f521" positionType="Float" x="10" y="0" width="180" height="30" forecolor="#004574"/>\r\n								<textElement textAlignment="Center" verticalAlignment="Middle">\r\n									<font fontName="Arial" size="12" isBold="true"/>\r\n									<paragraph tabStopWidth="10"/>\r\n								</textElement>\r\n								<text><![CDATA[Data Type]]></text>\r\n							</staticText>\r\n						</jr:columnHeader>\r\n						<jr:detailCell style="table 1_TD" height="21" rowSpan="1">\r\n							<textField isStretchWithOverflow="true" isBlankWhenNull="true">\r\n								<reportElement uuid="6e6f85a5-f8c2-4c80-800a-7eb744e2beee" positionType="Float" stretchType="RelativeToTallestObject" x="10" y="0" width="180" height="21"/>\r\n								<textElement textAlignment="Center" verticalAlignment="Middle">\r\n									<font fontName="Arial" size="10"/>\r\n									<paragraph leftIndent="2" tabStopWidth="10"/>\r\n								</textElement>\r\n								<textFieldExpression><![CDATA[$F{Data Type}]]></textFieldExpression>\r\n							</textField>\r\n						</jr:detailCell>\r\n					</jr:column>\r\n					<jr:column width="150" uuid="02bcc846-cc5a-4a7c-aa55-7e4447b6e6be">\r\n						<jr:columnHeader style="table 1_CH" height="31" rowSpan="1">\r\n							<staticText>\r\n								<reportElement uuid="94c19e4b-bd7d-4e6f-af88-eb91b91e903f" positionType="Float" x="10" y="0" width="130" height="30" forecolor="#004574"/>\r\n								<textElement textAlignment="Center" verticalAlignment="Middle">\r\n									<font fontName="Arial" size="12" isBold="true"/>\r\n									<paragraph tabStopWidth="10"/>\r\n								</textElement>\r\n								<text><![CDATA[Data Value]]></text>\r\n							</staticText>\r\n						</jr:columnHeader>\r\n						<jr:detailCell style="table 1_TD" height="21" rowSpan="1">\r\n							<textField isStretchWithOverflow="true" isBlankWhenNull="true">\r\n								<reportElement uuid="8f2bdabd-bc72-42bb-b7eb-3389c0594af1" positionType="Float" stretchType="RelativeToTallestObject" x="10" y="0" width="130" height="21"/>\r\n								<textElement textAlignment="Right" verticalAlignment="Middle">\r\n									<font fontName="Arial" size="10"/>\r\n									<paragraph leftIndent="2" tabStopWidth="10"/>\r\n								</textElement>\r\n								<textFieldExpression><![CDATA[$F{Data Value}]]></textFieldExpression>\r\n							</textField>\r\n						</jr:detailCell>\r\n					</jr:column>\r\n					<jr:column width="200" uuid="56973cbc-8cb2-4387-97fe-cb8ce3ae0558">\r\n						<jr:columnHeader style="table 1_CH" height="31" rowSpan="1">\r\n							<staticText>\r\n								<reportElement uuid="9bf21c32-8766-4fa1-a162-f54207c5a43b" positionType="Float" x="10" y="0" width="180" height="30" forecolor="#004574"/>\r\n								<textElement textAlignment="Center" verticalAlignment="Middle">\r\n									<font fontName="Arial" size="12" isBold="true"/>\r\n									<paragraph tabStopWidth="10"/>\r\n								</textElement>\r\n								<text><![CDATA[Timestamp]]></text>\r\n							</staticText>\r\n						</jr:columnHeader>\r\n						<jr:detailCell style="table 1_TD" height="21" rowSpan="1">\r\n							<textField isBlankWhenNull="true">\r\n								<reportElement uuid="5e537807-7f4e-402c-8f2a-244dcc77a70a" positionType="Float" stretchType="RelativeToTallestObject" x="10" y="0" width="180" height="21"/>\r\n								<textElement textAlignment="Center" verticalAlignment="Middle">\r\n									<font fontName="Arial" size="10"/>\r\n									<paragraph tabStopWidth="10"/>\r\n								</textElement>\r\n								<textFieldExpression><![CDATA[$F{Timestamp}]]></textFieldExpression>\r\n							</textField>\r\n						</jr:detailCell>\r\n					</jr:column>\r\n				</jr:table>\r\n			</componentElement>\r\n		</band>\r\n	</detail>\r\n	<pageFooter>\r\n		<band height="26">\r\n			<rectangle>\r\n				<reportElement uuid="1a757c5f-6f4f-48cc-acca-a1d945284927" x="0" y="4" width="801" height="4" forecolor="#004574" backcolor="#004574"/>\r\n			</rectangle>\r\n			<textField>\r\n				<reportElement uuid="226504f0-51cb-425d-b2c0-d2942e74f769" x="705" y="7" width="80" height="15"/>\r\n				<textElement textAlignment="Right" verticalAlignment="Middle">\r\n					<font size="8"/>\r\n				</textElement>\r\n				<textFieldExpression><![CDATA["Page "+$V{PAGE_NUMBER}+" of"]]></textFieldExpression>\r\n			</textField>\r\n			<textField evaluationTime="Report">\r\n				<reportElement uuid="5e43c5f2-6ab3-4cb4-a0a4-4192afd8ab6e" x="786" y="7" width="14" height="15"/>\r\n				<textElement textAlignment="Right" verticalAlignment="Middle">\r\n					<font size="8"/>\r\n				</textElement>\r\n				<textFieldExpression><![CDATA[" " + $V{PAGE_NUMBER}]]></textFieldExpression>\r\n			</textField>\r\n			<textField pattern="MM/dd/yyyy HH.mm.ss">\r\n				<reportElement uuid="77d93b44-b5f4-4919-afca-a73ffcca8098" x="6" y="7" width="100" height="15"/>\r\n				<textElement verticalAlignment="Middle">\r\n					<font size="8"/>\r\n				</textElement>\r\n				<textFieldExpression><![CDATA[new java.util.Date()]]></textFieldExpression>\r\n			</textField>\r\n		</band>\r\n	</pageFooter>\r\n</jasperReport>\r\n');
CALL insert_varchar_value(@tableTemplateId, @ownerPropertyId, @currentTime, 'synapsense');
CALL insert_int_value(@tableTemplateId, @privatePropertyId, @currentTime, 0);
CALL insert_int_value(@tableTemplateId, @systemPropertyId, @currentTime, 1);
set @tableParametersId = insert_object_value(@tableTemplateId, @parametersPropertyId, @currentTime);


-- Chart Custom Query
INSERT INTO `objects` (`object_type_id_fk`,`name`,`static`) VALUES (@lastTypeId,null,0);
set @chartTemplateId = last_insert_id();
CALL insert_varchar_value(@chartTemplateId, @namePropertyId, @currentTime, 'Chart Custom Query');
CALL insert_binarydata_value(@chartTemplateId, @designPropertyId, @currentTime, BINARY '<?xml version="1.0" encoding="UTF-8"?>\r\n<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="Chart Custom Query" language="groovy" pageWidth="842" pageHeight="595" orientation="Landscape" columnWidth="802" leftMargin="20" rightMargin="20" topMargin="20" bottomMargin="20" scriptletClass="com.synapsense.service.reporting.scriplets.CustomQueryChartScriptlet" uuid="6e83898a-70df-4a86-a40a-703eb2898507">\r\n	<property name="ireport.zoom" value="8.0"/>\r\n	<property name="ireport.x" value="5040"/>\r\n	<property name="ireport.y" value="4211"/>\r\n	<style name="Title" fontName="Arial" fontSize="26" isBold="true" pdfFontName="Helvetica-Bold"/>\r\n	<style name="SubTitle" forecolor="#666666" fontName="Arial" fontSize="18"/>\r\n	<style name="Column header" forecolor="#666666" fontName="Arial" fontSize="12" isBold="true" pdfFontName="Helvetica-Bold"/>\r\n	<style name="Detail" fontName="Arial" fontSize="12"/>\r\n	<style name="table">\r\n		<box>\r\n			<pen lineWidth="1.0" lineColor="#000000"/>\r\n		</box>\r\n	</style>\r\n	<style name="table_TH" mode="Opaque" backcolor="#F0F8FF">\r\n		<box>\r\n			<pen lineWidth="0.5" lineColor="#000000"/>\r\n		</box>\r\n	</style>\r\n	<style name="table_CH" mode="Opaque" backcolor="#BFE1FF">\r\n		<box>\r\n			<pen lineWidth="0.5" lineColor="#000000"/>\r\n		</box>\r\n	</style>\r\n	<style name="table_TD" mode="Opaque" backcolor="#FFFFFF">\r\n		<box>\r\n			<pen lineWidth="0.5" lineColor="#000000"/>\r\n		</box>\r\n	</style>\r\n	<style name="table 1">\r\n		<box>\r\n			<pen lineWidth="1.0" lineColor="#000000"/>\r\n		</box>\r\n	</style>\r\n	<style name="table 1_TH" mode="Opaque" backcolor="#F0F8FF">\r\n		<box>\r\n			<pen lineWidth="0.5" lineColor="#000000"/>\r\n		</box>\r\n	</style>\r\n	<style name="table 1_CH" mode="Opaque" backcolor="#BFE1FF">\r\n		<box>\r\n			<pen lineWidth="0.5" lineColor="#000000"/>\r\n		</box>\r\n	</style>\r\n	<style name="table 1_TD" mode="Opaque" backcolor="#FFFFFF">\r\n		<box>\r\n			<pen lineWidth="0.5" lineColor="#000000"/>\r\n		</box>\r\n	</style>\r\n	<parameter name="Custom Query ID" class="java.lang.String">\r\n		<parameterDescription><![CDATA[{"uiComponentType": "CustomQuerySelector"}]]></parameterDescription>\r\n	</parameter>\r\n	<parameter name="Report Title" class="java.lang.String"/>\r\n	<parameter name="Report Period (hours)" class="java.lang.Integer"/>\r\n	<variable name="Chart" class="net.sf.jasperreports.engine.JRRenderable" incrementType="Report" calculation="System"/>\r\n	<title>\r\n		<band height="30" splitType="Stretch">\r\n			<textField pattern="">\r\n				<reportElement uuid="7abf160c-87af-43c5-8266-c1ec0484f139" style="Title" mode="Opaque" x="154" y="0" width="648" height="26" forecolor="#004574" backcolor="#E6E6E6"/>\r\n				<textElement textAlignment="Center" verticalAlignment="Middle">\r\n					<font size="16"/>\r\n				</textElement>\r\n				<textFieldExpression><![CDATA[$P{Report Title}]]></textFieldExpression>\r\n			</textField>\r\n			<image scaleImage="RetainShape" vAlign="Middle" isUsingCache="true">\r\n				<reportElement uuid="b62513d5-b730-4fa1-ada0-7a585460ebf7" mode="Opaque" x="0" y="0" width="154" height="26" forecolor="#E6E6E6" backcolor="#E6E6E6"/>\r\n				<imageExpression><![CDATA["http://localhost:8080/synapsoft/resources/images/slogo_g.png"]]></imageExpression>\r\n			</image>\r\n			<rectangle>\r\n				<reportElement uuid="15ba970b-b491-4adb-b37d-330a53e7e49e" mode="Opaque" x="0" y="26" width="801" height="4" forecolor="#004574" backcolor="#004574"/>\r\n			</rectangle>\r\n		</band>\r\n	</title>\r\n	<detail>\r\n		<band height="499">\r\n			<image scaleImage="Clip" hAlign="Center" vAlign="Middle" hyperlinkType="Reference">\r\n				<reportElement uuid="d4eaf274-eec9-48fa-b781-21c886b79738" x="20" y="8" width="790" height="477"/>\r\n				<imageExpression><![CDATA[$V{Chart}]]></imageExpression>\r\n				<hyperlinkReferenceExpression><![CDATA["http://www.jfree.org/jfreechart"]]></hyperlinkReferenceExpression>\r\n			</image>\r\n		</band>\r\n	</detail>\r\n	<pageFooter>\r\n		<band height="22">\r\n			<rectangle>\r\n				<reportElement uuid="33bebb29-2881-4175-b788-03b70f088f46" x="0" y="0" width="801" height="4" forecolor="#004574" backcolor="#004574"/>\r\n			</rectangle>\r\n			<textField pattern="MM/dd/yyyy HH.mm.ss">\r\n				<reportElement uuid="dd488bd5-ce01-4883-8c41-7d1e6086a107" x="6" y="7" width="100" height="15" forecolor="#000000"/>\r\n				<textElement textAlignment="Left" verticalAlignment="Middle">\r\n					<font size="8"/>\r\n				</textElement>\r\n				<textFieldExpression><![CDATA[new java.util.Date()]]></textFieldExpression>\r\n			</textField>\r\n			<textField>\r\n				<reportElement uuid="fda9e4a2-8a8b-4cd1-b8ee-97fd91570aa8" x="705" y="7" width="80" height="15"/>\r\n				<textElement textAlignment="Right" verticalAlignment="Middle">\r\n					<font size="8"/>\r\n				</textElement>\r\n				<textFieldExpression><![CDATA["Page "+$V{PAGE_NUMBER}+" of"]]></textFieldExpression>\r\n			</textField>\r\n			<textField evaluationTime="Report">\r\n				<reportElement uuid="f4a4ee45-43a0-4ebf-9c98-8b86319aa459" x="786" y="7" width="14" height="15"/>\r\n				<textElement textAlignment="Right" verticalAlignment="Middle">\r\n					<font size="8"/>\r\n				</textElement>\r\n				<textFieldExpression><![CDATA[" " + $V{PAGE_NUMBER}]]></textFieldExpression>\r\n			</textField>\r\n		</band>\r\n	</pageFooter>\r\n</jasperReport>\r\n');
CALL insert_varchar_value(@chartTemplateId, @ownerPropertyId, @currentTime, 'synapsense');
CALL insert_int_value(@chartTemplateId, @privatePropertyId, @currentTime, 0);
CALL insert_int_value(@chartTemplateId, @systemPropertyId, @currentTime, 1);
set @chartParametersId = insert_object_value(@chartTemplateId, @parametersPropertyId, @currentTime);

-- create type 'REPORT'
INSERT INTO object_types(name) VALUES ('REPORT_TASK');

SET @lastTypeId := last_insert_id();

INSERT INTO objects(name, object_type_id_fk, static) VALUES ('REPORT_TASK', @lastTypeId, 1);

insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('name', @lastTypeId, 3);
insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('templateId', @lastTypeId, 1);
insert into attributes(name, object_type_id_fk, attr_type_id_fk, collection) values ('parameterValues', @lastTypeId, 5, 1);
insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('cron', @lastTypeId, 3);
insert into attributes(name, object_type_id_fk, attr_type_id_fk, collection) values ('users', @lastTypeId, 3, 1);
insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('status', @lastTypeId, 1);
insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('owner', @lastTypeId, 3);
insert into attributes(name, object_type_id_fk, attr_type_id_fk, collection) values ('formats', @lastTypeId, 3, 1);


-- create type 'REPORT'
INSERT INTO object_types(name) VALUES ('REPORT');

SET @lastTypeId := last_insert_id();

INSERT INTO objects(name, object_type_id_fk, static) VALUES ('REPORT', @lastTypeId, 1);

insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('taskId', @lastTypeId, 1);
insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('timestamp', @lastTypeId, 7);
insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('dataPath', @lastTypeId, 3);
insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('userName', @lastTypeId, 3);

-- create type 'REPORT_PARAMETER'
INSERT INTO object_types(name) VALUES ('REPORT_PARAMETER');
    
SET @lastTypeId := last_insert_id();
    
INSERT INTO objects(name, object_type_id_fk, static) VALUES ('REPORT_PARAMETER', @lastTypeId, 1);
        
insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('name', @lastTypeId, 3);
set @namePropertyId = last_insert_id(); 
insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('type', @lastTypeId, 3);
set @typePropertyId = last_insert_id(); 
insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('templateId', @lastTypeId, 1);
set @templatePropertyId = last_insert_id(); 
insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('orderId', @lastTypeId, 1);
set @orderPropertyId = last_insert_id(); 
insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('description', @lastTypeId, 3);
set @descriptionPropertyId = last_insert_id(); 

-- Tabular CustomQuery Parameter
INSERT INTO `objects` (`object_type_id_fk`,`name`,`static`) VALUES (@lastTypeId,null,0);
set @paramId = last_insert_id();
CALL insert_varchar_value(@paramId, @namePropertyId, @currentTime, 'Custom Query ID');
CALL insert_varchar_value(@paramId, @typePropertyId, @currentTime, 'java.lang.String');
CALL insert_int_value(@paramId, @templatePropertyId, @currentTime, @tableTemplateId);
CALL add_link_value(@tableParametersId, @currentTime, @paramId);
CALL insert_int_value(@paramId, @orderPropertyId, @currentTime, 0);
CALL insert_varchar_value(@paramId, @descriptionPropertyId, @currentTime, '{"uiComponentType": "CustomQuerySelector"}');

-- Tabular Title Parameter
INSERT INTO `objects` (`object_type_id_fk`,`name`,`static`) VALUES (@lastTypeId,null,0);
set @paramId = last_insert_id();
CALL insert_varchar_value(@paramId, @namePropertyId, @currentTime, 'Report Title');
CALL insert_varchar_value(@paramId, @typePropertyId, @currentTime, 'java.lang.String');
CALL insert_int_value(@paramId, @templatePropertyId, @currentTime, @tableTemplateId);
CALL add_link_value(@tableParametersId, @currentTime, @paramId);
CALL insert_int_value(@paramId, @orderPropertyId, @currentTime, 1);

-- Tabular IntervalInHours Parameter
INSERT INTO `objects` (`object_type_id_fk`,`name`,`static`) VALUES (@lastTypeId,null,0);
set @paramId = last_insert_id();
CALL insert_varchar_value(@paramId, @namePropertyId, @currentTime, 'Report Period (hours)');
CALL insert_varchar_value(@paramId, @typePropertyId, @currentTime, 'java.lang.Long');
CALL insert_int_value(@paramId, @templatePropertyId, @currentTime, @tableTemplateId);
CALL add_link_value(@tableParametersId, @currentTime, @paramId);
CALL insert_int_value(@paramId, @orderPropertyId, @currentTime, 2);

-- Chart CustomQuery Parameter
INSERT INTO `objects` (`object_type_id_fk`,`name`,`static`) VALUES (@lastTypeId,null,0);
set @paramId = last_insert_id();
CALL insert_varchar_value(@paramId, @namePropertyId, @currentTime, 'Custom Query ID');
CALL insert_varchar_value(@paramId, @typePropertyId, @currentTime, 'java.lang.String');
CALL insert_int_value(@paramId, @templatePropertyId, @currentTime, @chartTemplateId);
CALL add_link_value(@chartParametersId, @currentTime, @paramId);
CALL insert_int_value(@paramId, @orderPropertyId, @currentTime, 0);
CALL insert_varchar_value(@paramId, @descriptionPropertyId, @currentTime, '{"uiComponentType": "CustomQuerySelector"}');

-- Chart Title Parameter
INSERT INTO `objects` (`object_type_id_fk`,`name`,`static`) VALUES (@lastTypeId,null,0);
set @paramId = last_insert_id();
CALL insert_varchar_value(@paramId, @namePropertyId, @currentTime, 'Report Title');
CALL insert_varchar_value(@paramId, @typePropertyId, @currentTime, 'java.lang.String');
CALL insert_int_value(@paramId, @templatePropertyId, @currentTime, @chartTemplateId);
CALL add_link_value(@chartParametersId, @currentTime, @paramId);
CALL insert_int_value(@paramId, @orderPropertyId, @currentTime, 1);

-- Chart IntervalInHours Parameter
INSERT INTO `objects` (`object_type_id_fk`,`name`,`static`) VALUES (@lastTypeId,null,0);
set @paramId = last_insert_id();
CALL insert_varchar_value(@paramId, @namePropertyId, @currentTime, 'Report Period (hours)');
CALL insert_varchar_value(@paramId, @typePropertyId, @currentTime, 'java.lang.Integer');
CALL insert_int_value(@paramId, @templatePropertyId, @currentTime, @chartTemplateId);
CALL add_link_value(@chartParametersId, @currentTime, @paramId);
CALL insert_int_value(@paramId, @orderPropertyId, @currentTime, 2);

-- create type 'REPORT_PARAMETER_VALUE'
INSERT INTO object_types(name) VALUES ('REPORT_PARAMETER_VALUE');
    
SET @lastTypeId := last_insert_id();
    
INSERT INTO objects(name, object_type_id_fk, static) VALUES ('REPORT_PARAMETER_VALUE', @lastTypeId, 1);
        
insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('value', @lastTypeId, 3);
insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('reportTaskId', @lastTypeId, 1);
insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('parameterId', @lastTypeId, 1);
insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('integerValue', @lastTypeId, 1);
insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('dateValue', @lastTypeId, 4);
insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('toValue', @lastTypeId, 5);
insert into attributes(name, object_type_id_fk, attr_type_id_fk, collection) values ('setTOValue', @lastTypeId, 5, 1);
insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('longValue', @lastTypeId, 7);

-- create type 'EXPORTED_REPORT'
INSERT INTO object_types(name) VALUES ('EXPORTED_REPORT');
    
SET @lastTypeId := last_insert_id();
    
INSERT INTO objects(name, object_type_id_fk, static) VALUES ('EXPORTED_REPORT', @lastTypeId, 1);
        
insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('reportId', @lastTypeId, 1);
insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('format', @lastTypeId, 3);
insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('exportedPath', @lastTypeId, 3);


-- =============================================================================
-- drop auxiliary functions & procedures
DROP FUNCTION IF EXISTS insert_object_value;
DROP PROCEDURE IF EXISTS insert_varchar_value;
DROP PROCEDURE IF EXISTS insert_double_value;
DROP PROCEDURE IF EXISTS insert_int_value;
DROP PROCEDURE IF EXISTS insert_long_value;
DROP PROCEDURE IF EXISTS insert_link_value;
DROP PROCEDURE IF EXISTS add_link_value;
DROP PROCEDURE IF EXISTS insert_binarydata_value;
