﻿use synap;
-- Drop table TBL_SENSOR_DATA
DROP TABLE IF EXISTS `TBL_SENSOR_DATA`;

-- ------------------------------------------------------------
-- Description:
-- This table stores a single piece of data collected at a sensor node.
-- ------------------------------------------------------------

CREATE TABLE `TBL_SENSOR_DATA` (
  `sensor_id` int(11) COMMENT 'The data request id that triggered this collection',
  `data_time_stamp` datetime COMMENT 'The point of time when the data was collected by the sensor node',
  `sensor_node_data` double(15,6) COMMENT 'Actual value of the measurement captured by the sensor node sensor.',
  INDEX `idx_sensor_id_date_time`(`sensor_id`, `data_time_stamp`)
)
ENGINE=INNODB
COMMENT = 'Stores a single piece of data collected at a sensor node.  '
CHARACTER SET utf8 ;

-- partitions table tbl_sensor_data
ALTER TABLE tbl_sensor_data PARTITION BY
range(TO_DAYS(`data_time_stamp`)) (
  partition p1 values LESS THAN (to_days('2009-02-01')),
  partition p2 values LESS THAN (to_days('2009-03-01')),
  partition p3 values LESS THAN (to_days('2009-04-01')),
  partition p4 values LESS THAN (to_days('2009-05-01')),
  partition p5 values LESS THAN (to_days('2009-06-01')),
  partition p6 values LESS THAN (to_days('2009-07-01'))
);

-- enables archivation event
-- can be executed even if db has some working data

DROP EVENT IF EXISTS `archive_data`;

delimiter //
-- starts before the last partition will be filled out (~86M records= 10000 sensors x 5 min x 1 month)
-- expected execution time is 1.5-2 hours
CREATE EVENT `archive_data`
ON SCHEDULE EVERY 1 MONTH
-- starts just before 'LESS THAN' value of the last partition
STARTS TIMESTAMP(DATE('2009-06-28'),'01:00')
DO
BEGIN
-- default schema veriable
DECLARE SCHEMA_NAME VARCHAR(10) default 'synap';
-- stores the name of the partition with lower range
DECLARE lastPartitionName VARCHAR(2);
-- the time point -  determines the sensor data that has to be archived
DECLARE oldDataTimePoint date;
-- the time point - upper border limit of the new partiotion range(in days)
DECLARE newDataTimePoint int;

CREATE TABLE IF NOT EXISTS `ARCHIVE_SENSOR_DATA` (
  `sensor_id` int(11) COMMENT 'The data request id that triggered this collection',
  `data_time_stamp` datetime COMMENT 'The timestamp when the data was collected by the sensor node',
  `sensor_node_data` double(15,6) COMMENT 'Actual value of the measurement captured by the sensor node sensor.'
)
ENGINE=ARCHIVE;

-- retrieves date and partition name of the partition with last ordinal pos (means the partition with lower range border )
-- implements cycle buffer of partition names
SELECT from_days(p.partition_description) ,p.partition_name
into oldDataTimePoint,lastPartitionName
FROM information_schema.PARTITIONS P
where p.table_schema = SCHEMA_NAME
and p.table_name = 'tbl_sensor_data'
and p.partition_ordinal_position =
(select min(P_sub.partition_ordinal_position)
  from information_schema.PARTITIONS P_sub where P_sub.table_schema = SCHEMA_NAME
  and P_sub.table_name = 'tbl_sensor_data'
);

-- moves online sensor data from the last partiotion(6 months old data) to the archive
INSERT INTO ARCHIVE_SENSOR_DATA ( `sensor_id`, `sensor_node_data`, `data_time_stamp`)
(select `sensor_id`, `sensor_node_data`, `data_time_stamp`
   from `tbl_sensor_data`
   where `data_time_stamp` < oldDataTimePoint);


-- dynamic instruction to drop partition with the specified name (last partiotion is no more necessary)
set @preparedStmtDrop = concat('ALTER TABLE ',SCHEMA_NAME,'.tbl_sensor_data drop partition ',lastPartitionName);
PREPARE drop_last_partition from @preparedStmtDrop;
EXECUTE drop_last_partition;
DEALLOCATE PREPARE drop_last_partition;

-- retrieves the upper range border of the new partition
--   1.gets partition range upper border
--   2.add 1 month interval to it
--   3.converts the date back to days count
SELECT to_days(date_add(from_days(p.partition_description), INTERVAL 1 month))
into newDataTimePoint
FROM information_schema.PARTITIONS P
where p.table_schema = SCHEMA_NAME
and p.table_name = 'tbl_sensor_data'
and p.partition_ordinal_position =
(select max(P_sub.partition_ordinal_position)
  from information_schema.PARTITIONS P_sub where P_sub.table_schema = SCHEMA_NAME
  and P_sub.table_name = 'tbl_sensor_data'
);

-- dynamic instruction to create the just deleted partition with the new range borders
set @preparedStmtCreate = concat('ALTER TABLE ',SCHEMA_NAME,'.tbl_sensor_data add partition(partition ',lastPartitionName, ' values LESS THAN (',newDataTimePoint,'))');
PREPARE create_new_partition from @preparedStmtCreate;
EXECUTE create_new_partition;
DEALLOCATE PREPARE create_new_partition;

END//
delimiter ;
