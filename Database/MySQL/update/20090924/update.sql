use `synap`;

update `TBL_DATA_CLASS` set data_class_name="LDETECT" where data_class_id=213;
update `TBL_DATA_CLASS` set data_class_description="Liquid Detector" where data_class_id=213;

update `TBL_SENSOR_TYPE` set sensor_name="Liquid Presence Detector" where sensor_data_class_id=213;

alter table tbl_user_preferences_assoc modify column preference_value varchar(1000);