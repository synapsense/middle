use `synap`;

ALTER TABLE `tbl_alert_priority` ADD COLUMN `repeat_count` INTEGER UNSIGNED NOT NULL DEFAULT 3 AFTER `range_end`;
