use 'synap';
ALTER TABLE TBL_SENSOR ADD COLUMN scale_min DOUBLE(15,6) AFTER delta;
ALTER TABLE TBL_SENSOR ADD COLUMN scale_max DOUBLE(15,6) AFTER scale_min;

UPDATE TBL_SENSOR SET scale_min = min, scale_max = max;
