use `synap`;

alter table valued modify column `value` DOUBLE(15,6);
alter table tbl_visualization_legend_point modify column `position` DOUBLE(15,6);
alter table tbl_visualization_legend_point ADD UNIQUE INDEX `legend_position_idx`(`legend`, `position`);
