-- Bug#8016
-- The script introduces a new email template 'Short Email with LiveImaging'

use synap;

START TRANSACTION;

select objtype_id into @template_type_id from object_types where name = 'ALERT_MESSAGE_TEMPLATE';

select attr_id into @template_name_property_id from object_types ot, attributes a where ot.objtype_id = a.object_type_id_fk and ot.name = 'ALERT_MESSAGE_TEMPLATE' and a.name = 'name';
select attr_id into @template_header_property_id from object_types ot, attributes a where ot.objtype_id = a.object_type_id_fk and ot.name = 'ALERT_MESSAGE_TEMPLATE' and a.name = 'header';
select attr_id into @template_body_property_id from object_types ot, attributes a where ot.objtype_id = a.object_type_id_fk and ot.name = 'ALERT_MESSAGE_TEMPLATE' and a.name = 'body';
select attr_id into @template_descr_property_id from object_types ot, attributes a where ot.objtype_id = a.object_type_id_fk and ot.name = 'ALERT_MESSAGE_TEMPLATE' and a.name = 'description';
select attr_id into @template_messagetype_property_id from object_types ot, attributes a where ot.objtype_id = a.object_type_id_fk and ot.name = 'ALERT_MESSAGE_TEMPLATE' and a.name = 'messagetype';
select attr_id into @template_type_property_id from object_types ot, attributes a where ot.objtype_id = a.object_type_id_fk and ot.name = 'ALERT_MESSAGE_TEMPLATE' and a.name = 'type';


-- SMTP with Live Image attachment
INSERT INTO `objects` (`object_type_id_fk`,`name`,`static`) VALUES (@template_type_id,null,0);
set @template_id = last_insert_id();

INSERT INTO `obj_values` (`object_id_fk`,`attr_id_fk`, `timestamp`) VALUES (@template_id,@template_name_property_id,unix_timestamp()*1000);
set @last_objvalues_id = last_insert_id();
INSERT INTO `valuev` (`value`,`value_id_fk`) VALUES ("Short Email with LiveImaging", @last_objvalues_id);

INSERT INTO `obj_values` (`object_id_fk`,`attr_id_fk`, `timestamp`) VALUES (@template_id,@template_header_property_id,unix_timestamp()*1000);
set @last_objvalues_id = last_insert_id();
INSERT INTO `valuev` (`value`,`value_id_fk`) VALUES ("Tier $MSG_TIER$ System  Alert - $parent(DC).name$ \\ $name$  \\ $ALERT_NAME$ @ $TRIGGER_DATA_VALUE$", @last_objvalues_id);

INSERT INTO `obj_values` (`object_id_fk`,`attr_id_fk`, `timestamp`) VALUES (@template_id,@template_body_property_id,unix_timestamp()*1000);
set @last_objvalues_id = last_insert_id();
INSERT INTO `valuev` (`value`,`value_id_fk`) VALUES ("Tier $MSG_TIER$ System  Alert - $parent(DC).name$ \\ $parent(ZONE).name$ \\ $name$ \n\nAlert: $ALERT_NAME$\nTime: $TIMESTAMP$\nTrigger Value: $TRIGGER_DATA_VALUE$\nCurrent Value: $CURRENT_DATA_VALUE$ $DC_LIVE_IMAGE$" ,@last_objvalues_id);

INSERT INTO `obj_values` (`object_id_fk`,`attr_id_fk`, `timestamp`) VALUES (@template_id,@template_descr_property_id,unix_timestamp()*1000);
set @last_objvalues_id = last_insert_id();
INSERT INTO `valuev` (`value`,`value_id_fk`) VALUES ("A short email alert notification with LiveImaging image included",@last_objvalues_id);

INSERT INTO `obj_values` (`object_id_fk`,`attr_id_fk`, `timestamp`) VALUES (@template_id,@template_messagetype_property_id,unix_timestamp()*1000);
set @last_objvalues_id = last_insert_id();
INSERT INTO `valuev` (`value`,`value_id_fk`) VALUES ("1",@last_objvalues_id);

INSERT INTO `obj_values` (`object_id_fk`,`attr_id_fk`, `timestamp`) VALUES (@template_id,@template_type_property_id,unix_timestamp()*1000);
set @last_objvalues_id = last_insert_id();
INSERT INTO `valuei` (`value`,`value_id_fk`) VALUES (1,@last_objvalues_id);

COMMIT;