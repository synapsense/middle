use `synap`;

update `TBL_SENSOR_TYPE` set sensor_capability="Senses electrical current" where type_id=30;
update `TBL_SENSOR_TYPE` set sensor_capability="Senses cumulative energy per sampling interval" where type_id=20;
update `TBL_SENSOR_TYPE` set sensor_name="Energy meter" where type_id=20;
update `TBL_SENSOR_TYPE` set sensor_capability="Senses cumulative energy per sampling interval" where type_id=32;
update `TBL_SENSOR_TYPE` set sensor_name="Energy meter" where type_id=32;
update `TBL_DATA_CLASS` set data_class_name="ENERGY" where data_class_id=207;
update `TBL_DATA_CLASS` set data_class_description="Energy" where data_class_id=207;
update `TBL_UNITS` set unit_name="Energy" where unit_id=106;
update `TBL_UNITS` set unit_symbol="kWh" where unit_id=106;
update `TBL_UNITS` set unit_description="Energy measurement" where unit_id=106;

drop user synapsense;
grant select, insert, update, delete on Synap.* to 'dbuser'@'%' identified by 'dbu$er';