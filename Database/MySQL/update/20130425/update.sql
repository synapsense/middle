﻿-- Bug#9041
-- The script introduces a new object type 'GLOBAL_INFO' with one property 'jsonStr' and creates one object of this type.
-- New type's aim is to prevent regenerating images to the date prior to the 6.5 install date

use synap;

DROP FUNCTION IF EXISTS insert_object_value;
DELIMITER |
CREATE FUNCTION insert_object_value(
  in_object_id int, in_property_id int, in_timestamp long)
  RETURNS INTEGER
  BEGIN
    set @id=0;
    SELECT value_id into @id 
        from obj_values
        where object_id_fk=in_object_id and attr_id_fk=in_property_id limit 1;

    IF @id!=0
    THEN 
        RETURN @id;
    END IF;

    INSERT INTO obj_values(
                  object_id_fk, attr_id_fk, TIMESTAMP)
    VALUES (in_object_id, in_property_id, in_timestamp);

    RETURN last_insert_id();
  END|
DELIMITER ;

DROP PROCEDURE IF EXISTS insert_varchar_value;

DELIMITER |
CREATE PROCEDURE insert_varchar_value(
  in_object_id int, in_property_id int, in_timestamp long, valuev varchar(20000))
  BEGIN
    DECLARE valueId   INT;

    SET valueId =
          insert_object_value(
            in_object_id, in_property_id, in_timestamp);

    INSERT INTO valuev(
                  value_id_fk, VALUE)
    VALUES (valueId, valuev);
  END|

DELIMITER ;

set @currentTime = UNIX_TIMESTAMP() * 1000;

-- Create GLOBAL_INFO type
INSERT INTO  `object_types` (`name`) VALUES ('GLOBAL_INFO');
set @globalInfoTypeId = last_insert_id();
INSERT INTO  `objects` (`object_type_id_fk`,`name`,`static`) VALUES (@globalInfoTypeId,'GLOBAL_INFO',1);
INSERT INTO  `attributes` (`object_type_id_fk`,`name`,`attr_type_id_fk`) VALUES (@globalInfoTypeId,'jsonStr',3);
set @globalInfoJsonStrPropertyId = last_insert_id();

-- Create GLOBAL_INFO object
INSERT INTO `objects` (`object_type_id_fk`,`name`,`static`) VALUES (@globalInfoTypeId,null,0);
set @globalInfoObjectId = last_insert_id();
CALL insert_varchar_value(@globalInfoObjectId, @globalInfoJsonStrPropertyId, @currentTime, CONCAT('{"install_date_6_5":',@currentTime, '}'));


DROP FUNCTION IF EXISTS insert_object_value;
DROP PROCEDURE IF EXISTS insert_varchar_value;