-- dre rools source code update. double replace - first for property , second for DRE getter method
update tbl_rule_types
set source = replace(replace(source,'ratSP','setpoint'),'getRatSP','getSetpoint')
where type = 2 
and source regexp 'ratSP';

update tbl_rule_types
set source = replace(replace(source,'valveControl','coolingControl'),'getValveControl','getCoolingControl')
where type = 2 
and source regexp 'valveControl';

update tbl_rule_types
set source = replace(replace(source,'valvePosition','capacity'),'getValvePosition','getCapacity')
where type = 2 
and source regexp 'valvePosition';

update tbl_rule_types
set source = replace(replace(source,'fanspeed','setpoint'),'getFanspeed','getSetpoint')
where type = 2 
and source regexp 'fanspeed';

update tbl_rule_types
set source = replace(replace(source,'controltype','resource'),'getControltype','getResource')
where type = 2 
and source regexp 'controltype';

-- custom query updates, separate queries with regexp to avoid unwanted changes
update object_types ot , objects o, attributes a, obj_values ov, valuev v
set v.value = replace(v.value,'controltype','resource')
where ot.objtype_id = o.object_type_id_fk
and ot.objtype_id = a.object_type_id_fk
and ov.object_id_fk = o.id
and ov.attr_id_fk = a.attr_id
and ov.value_id = v.value_id_fk
and ot.name = 'CUSTOMQUERY'
and a.name = 'associatedSensors'
and v.value regexp 'controltype';

update object_types ot , objects o, attributes a, obj_values ov, valuev v
set v.value = replace(v.value,'fanspeed','setpoint')
where ot.objtype_id = o.object_type_id_fk
and ot.objtype_id = a.object_type_id_fk
and ov.object_id_fk = o.id
and ov.attr_id_fk = a.attr_id
and ov.value_id = v.value_id_fk
and ot.name = 'CUSTOMQUERY'
and a.name = 'associatedSensors'
and v.value regexp 'fanspeed';

update object_types ot , objects o, attributes a, obj_values ov, valuev v
set v.value = replace(v.value,'valvePosition','capacity')
where ot.objtype_id = o.object_type_id_fk
and ot.objtype_id = a.object_type_id_fk
and ov.object_id_fk = o.id
and ov.attr_id_fk = a.attr_id
and ov.value_id = v.value_id_fk
and ot.name = 'CUSTOMQUERY'
and a.name = 'associatedSensors'
and v.value regexp 'valvePosition';


update object_types ot , objects o, attributes a, obj_values ov, valuev v
set v.value = replace(v.value,'valveControl','coolingControl')
where ot.objtype_id = o.object_type_id_fk
and ot.objtype_id = a.object_type_id_fk
and ov.object_id_fk = o.id
and ov.attr_id_fk = a.attr_id
and ov.value_id = v.value_id_fk
and ot.name = 'CUSTOMQUERY'
and a.name = 'associatedSensors'
and v.value regexp 'valveControl';


update object_types ot , objects o, attributes a, obj_values ov, valuev v
set v.value = replace(v.value,'ratSP','setpoint')
where ot.objtype_id = o.object_type_id_fk
and ot.objtype_id = a.object_type_id_fk
and ov.object_id_fk = o.id
and ov.attr_id_fk = a.attr_id
and ov.value_id = v.value_id_fk
and ot.name = 'CUSTOMQUERY'
and a.name = 'associatedSensors'
and v.value regexp 'ratSP';
