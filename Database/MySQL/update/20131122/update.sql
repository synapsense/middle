-- Bug#9578
-- The script adds a new 'taskName' property for 'Report' object type

use synap;

select obt.objtype_id into @obj_type_id from object_types obt where obt.name = "REPORT";
insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('taskName', @obj_type_id, 3);
