use `synap`;

CREATE TABLE IF NOT EXISTS `value_binary` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `value` mediumblob,
  `value_id_FK` int(11) NOT NULL,
  PRIMARY KEY(`id`),
  INDEX `FK_value_id`(`value_id_FK`)
)
ENGINE=INNODB
COMMENT = 'Stores binary prop.values';

ALTER TABLE `value_binary` ADD
  CONSTRAINT `root_value_reference` FOREIGN KEY (`value_id_FK`)
    REFERENCES `OBJ_VALUES`(`value_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION;

INSERT INTO `attributes_types` (`attr_type_id`,`java_type`) VALUES (8,"com.synapsense.dto.BinaryData");