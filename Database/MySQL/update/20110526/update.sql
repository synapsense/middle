use `synap`;

alter table `tbl_alert` drop column `version`;
-- update status from 1 to 0(active), from 0(historical) ot 2 (dismissed)
update tbl_alert set status = 2 where status = 0;
update tbl_alert set status = 0 where status = 1;

create table `tbl_alert_type_template` (
  `alert_type_id_fk` int(11) not null,
  `transport_type_id_fk` int(11) not null,
  `object_id_fk` int(11),
  primary key(`alert_type_id_fk`, `transport_type_id_fk`)
)
engine=innodb;

create table `tbl_transport_type` (
  `transport_type_id` int(11) not null auto_increment,
  `name` varchar(128) not null,
  primary key(`transport_type_id`),
  unique index `name_uniq`(`name`)
)
engine=innodb;

-- insert into transport type
INSERT INTO `tbl_transport_type` (`transport_type_id`,`name`) VALUES (1,"SMTP");
INSERT INTO `tbl_transport_type` (`transport_type_id`,`name`) VALUES (2,"SMS");
INSERT INTO `tbl_transport_type` (`transport_type_id`,`name`) VALUES (3,"SNMP");
INSERT INTO `tbl_transport_type` (`transport_type_id`,`name`) VALUES (4,"CONSOLE");

-- new table for alert priority
create table `tbl_alert_priority_temp` (
  `alert_priority_id` int(11) not null,
  `name` varchar(128) not null,
  `dismiss_interval` bigint(20) not null,
  primary key(`alert_priority_id`)
)
engine=innodb;

-- new table to store tiers
create table `tbl_alert_priority_tier` (
  `priority_tier_id` int(11) not null auto_increment,
  `alert_priority_id_fk` int(11) not null,
  `name` varchar(128) not null,
  `interval` bigint(20) unsigned,
  `repeat_limit` int(11) unsigned,
  `send_esc` smallint(1) default '0',
  `send_ack` smallint(1) default '0',
  `send_resolve` smallint(1) default '0',
  primary key(`priority_tier_id`),
  unique index `priority_tier_name_uniq`(`alert_priority_id_fk`, `name`)
)
engine=innodb;

-- insert priorities
INSERT INTO `tbl_alert_priority_temp` (`alert_priority_id`,`name`,`dismiss_interval`) VALUES (1,"CRITICAL",600000);
 INSERT INTO `tbl_alert_priority_tier` (`priority_tier_id`,`alert_priority_id_fk`,`name`,`interval`,`repeat_limit`) VALUES(1,1,"TIER1",10*60*1000,2);
 INSERT INTO `tbl_alert_priority_tier` (`priority_tier_id`,`alert_priority_id_fk`,`name`,`interval`,`repeat_limit`) VALUES(2,1,"TIER2",10*60*1000,2);
 INSERT INTO `tbl_alert_priority_tier` (`priority_tier_id`,`alert_priority_id_fk`,`name`,`interval`,`repeat_limit`) VALUES(3,1,"TIER3",10*60*1000,2);

INSERT INTO `tbl_alert_priority_temp` (`alert_priority_id`,`name`,`dismiss_interval`) VALUES (2,"MAJOR",1200000);
 INSERT INTO `tbl_alert_priority_tier` (`priority_tier_id`,`alert_priority_id_fk`,`name`,`interval`,`repeat_limit`) VALUES(4,2,"TIER1",15*60*1000,3);
 INSERT INTO `tbl_alert_priority_tier` (`priority_tier_id`,`alert_priority_id_fk`,`name`,`interval`,`repeat_limit`) VALUES(5,2,"TIER2",15*60*1000,3);
 INSERT INTO `tbl_alert_priority_tier` (`priority_tier_id`,`alert_priority_id_fk`,`name`,`interval`,`repeat_limit`) VALUES(6,2,"TIER3",15*60*1000,3);

INSERT INTO `tbl_alert_priority_temp` (`alert_priority_id`,`name`,`dismiss_interval`) VALUES (3,"MINOR",2592000000);
 INSERT INTO `tbl_alert_priority_tier` (`priority_tier_id`,`alert_priority_id_fk`,`name`,`interval`,`repeat_limit`) VALUES(7,3,"TIER1",30*60*1000,3);
 INSERT INTO `tbl_alert_priority_tier` (`priority_tier_id`,`alert_priority_id_fk`,`name`,`interval`,`repeat_limit`) VALUES(8,3,"TIER2",30*60*1000,3);
 INSERT INTO `tbl_alert_priority_tier` (`priority_tier_id`,`alert_priority_id_fk`,`name`,`interval`,`repeat_limit`) VALUES(9,3,"TIER3",30*60*1000,3);

INSERT INTO `tbl_alert_priority_temp` (`alert_priority_id`,`name`,`dismiss_interval`) VALUES (4,"INFORMATIONAL",2592000000);
INSERT INTO `tbl_alert_priority_tier` (`priority_tier_id`,`alert_priority_id_fk`,`name`,`interval`,`repeat_limit`) VALUES(10,4,"TIER1",60*60*1000,3);

alter table tbl_alert_type drop column version;
alter table tbl_alert_type add column alert_priority_id_fk int(11) not null default 0 after alert_type_id;
alter table tbl_alert_type add column auto_ack smallint(1) unsigned not null default 0 after auto_dismiss;

-- update priority rerefences
-- high -> critical
update tbl_alert_type as at , tbl_alert_priority as ap 
set at.alert_priority_id_fk = 1 
where at.priority between ap.range_begin and ap.range_end and ap.range_begin >=0 and ap.range_end <= 32;
-- middle -> major
update tbl_alert_type as at , tbl_alert_priority as ap 
set at.alert_priority_id_fk = 2 
where at.priority between ap.range_begin and ap.range_end and ap.range_begin >=33 and ap.range_end <= 67;
-- low -> minor
update tbl_alert_type as at , tbl_alert_priority as ap 
set at.alert_priority_id_fk = 3
where at.priority between ap.range_begin and ap.range_end and ap.range_begin >=68 and ap.range_end <= 100;

alter table tbl_alert_type drop column priority;

-- Drop table TBL_ALERT_PRIORITY
drop table if exists `tbl_alert_priority`;
alter table tbl_alert_priority_temp rename to tbl_alert_priority;

create table `tbl_destinations_temp` (
  `destination_id` int(11) not null auto_increment,
  `destination` varchar(100),
  `user_id_fk` int(11) default null,
  primary key(`destination_id`),
  unique index `destination_uniq`(`destination`(20))
)
engine=innodb;

-- moving destinations to temp
insert into tbl_destinations_temp (destination) select distinct(dest_name) from tbl_destinations;

create table `tbl_alert_type_destination_assoc` (
  `alert_type_id_fk` int(11) not null,
  `transport_type_id_fk` int(11) not null,
  `priority_tier_id_fk` int(11) not null,
  `dest_id_fk` int(11) not null,
  primary key(`alert_type_id_fk`, `transport_type_id_fk`, `priority_tier_id_fk`, `dest_id_fk`)
)
engine=innodb
comment = 'alert type to destination assoc';

-- moving SMTP destination references using tier1 as default, because tier is a new concept which is missing in previous release ver
insert into tbl_alert_type_destination_assoc (alert_type_id_fk,transport_type_id_fk,priority_tier_id_fk,dest_id_fk)
select assoc.alert_type_id_fk, 1, apt.priority_tier_id, dt.destination_id 
from tbl_alert_type_action_dest_assoc assoc, tbl_alert_type at , tbl_destinations_temp dt, tbl_destinations d, tbl_users u,tbl_alert_priority ap, tbl_alert_priority_tier apt 
where assoc.alert_type_id_fk = at.alert_type_id 
and at.alert_priority_id_fk = ap.alert_priority_id
and ap.alert_priority_id = apt.alert_priority_id_fk
and apt.name = 'TIER1'
and assoc.dest_id_fk = d.dest_id
and d.dest_name = dt.destination
and dt.destination = u.primary_email
and assoc.performer_id_fk = 2;

-- update destinations which relate to users making them visible for console
update tbl_destinations_temp d , tbl_users u 
set d.user_id_fk = u.user_id, d.destination = u.user_name
where d.destination = u.primary_email;

-- moving SNMP destination references using tier1 as default, because tier is a new concept which is missing in previous release ver
insert into tbl_alert_type_destination_assoc (alert_type_id_fk,transport_type_id_fk,priority_tier_id_fk,dest_id_fk)
select assoc.alert_type_id_fk, 2, apt.priority_tier_id, dt.destination_id 
from tbl_alert_type_action_dest_assoc assoc, tbl_alert_type at , tbl_destinations_temp dt, tbl_destinations d, tbl_alert_priority ap, tbl_alert_priority_tier apt 
where assoc.alert_type_id_fk = at.alert_type_id 
and at.alert_priority_id_fk = ap.alert_priority_id
and ap.alert_priority_id = apt.alert_priority_id_fk
and apt.name = 'TIER1'
and assoc.dest_id_fk = d.dest_id
and d.dest_name = dt.destination
and assoc.performer_id_fk = 5;

-- get rid of unused destinations
delete from tbl_destinations_temp where destination_id not in (select dest_id_fk from tbl_alert_type_destination_assoc);

-- cleaning
drop table if exists `tbl_alert_type_action_dest_assoc`;
drop table if exists `tbl_performers`;
drop table if exists `tbl_destinations`;
alter table tbl_destinations_temp rename to tbl_destinations;

-- Needs to prepare message templates infrastructure before initiating alert types
-- seeding default templates for all type of messages
INSERT INTO `object_types` (`name`, `wsn`) VALUES("ALERT_MESSAGE_TEMPLATE", 1);
SET @amt_type_id = last_insert_id();

INSERT INTO `objects` (`object_type_id_fk`,`name`,`static`) VALUES (@amt_type_id,"ALERT_MESSAGE_TEMPLATE",1);
INSERT INTO `attributes` (`name`, `object_type_id_FK`, `attr_type_id_FK`) VALUES ("name",@amt_type_id,3);
SET @name_attr_id = last_insert_id();

INSERT INTO `attributes` (`name`, `object_type_id_FK`, `attr_type_id_FK`) VALUES ("header",@amt_type_id,3);
SET @header_attr_id = last_insert_id();

INSERT INTO `attributes` (`name`, `object_type_id_FK`, `attr_type_id_FK`) VALUES ("body",@amt_type_id,3);
SET @body_attr_id = last_insert_id();

INSERT INTO `attributes` (`name`, `object_type_id_FK`, `attr_type_id_FK`) VALUES ("description",@amt_type_id,3);
SET @descr_attr_id = last_insert_id();

INSERT INTO `attributes` (`name`, `object_type_id_FK`, `attr_type_id_FK`) VALUES ("messagetype",@amt_type_id,3);
SET @mtype_attr_id = last_insert_id();

INSERT INTO `attributes` (`name`, `object_type_id_FK`, `attr_type_id_FK`) VALUES ("type",@amt_type_id,1);
SET @template_type_attr_id = last_insert_id();


-- predefined Short SMS, code=2
INSERT INTO `objects` (`object_type_id_fk`,`name`,`static`) VALUES (@amt_type_id,null,0);
SET @sms_template_object_id = last_insert_id();

INSERT INTO `obj_values` (`object_id_fk`,`attr_id_fk`, `timestamp`) VALUES (@sms_template_object_id,@name_attr_id,unix_timestamp()*1000);
SET @name_value_id = last_insert_id();
INSERT INTO `valuev` (`value`,`value_id_fk`) VALUES ("Short SMS",@name_value_id);

INSERT INTO `obj_values` (`object_id_fk`,`attr_id_fk`, `timestamp`) VALUES (@sms_template_object_id,@header_attr_id,unix_timestamp()*1000);
SET @header_value_id = last_insert_id();
INSERT INTO `valuev` (`value`,`value_id_fk`) VALUES ("",@header_value_id);

INSERT INTO `obj_values` (`object_id_fk`,`attr_id_fk`, `timestamp`) VALUES (@sms_template_object_id,@body_attr_id,unix_timestamp()*1000);
SET @body_value_id = last_insert_id();
INSERT INTO `valuev` (`value`,`value_id_fk`) VALUES ("$parent(DC).name$ - $name$ - $ALERT_NAME$\n\nCurrent Value: $CURRENT_DATA_VALUE$\n\nTime Triggered: $TIMESTAMP$",@body_value_id);

INSERT INTO `obj_values` (`object_id_fk`,`attr_id_fk`, `timestamp`) VALUES (@sms_template_object_id,@descr_attr_id,unix_timestamp()*1000);
SET @descr_value_id = last_insert_id();
INSERT INTO `valuev` (`value`,`value_id_fk`) VALUES ("A short SMS alert notification",@descr_value_id);

INSERT INTO `obj_values` (`object_id_fk`,`attr_id_fk`, `timestamp`) VALUES (@sms_template_object_id,@mtype_attr_id,unix_timestamp()*1000);
SET @mt_value_id = last_insert_id();
INSERT INTO `valuev` (`value`,`value_id_fk`) VALUES ("2",@mt_value_id);

INSERT INTO `obj_values` (`object_id_fk`,`attr_id_fk`, `timestamp`) VALUES (@sms_template_object_id,@template_type_attr_id,unix_timestamp()*1000);
SET @tt_value_id = last_insert_id();
INSERT INTO `valuei` (`value`,`value_id_fk`) VALUES (1,@tt_value_id);

-- predefined Long SMS, code=2
INSERT INTO `objects` (`object_type_id_fk`,`name`,`static`) VALUES (@amt_type_id,null,0);
SET @sms_long_template_object_id = last_insert_id();

INSERT INTO `obj_values` (`object_id_fk`,`attr_id_fk`, `timestamp`) VALUES (@sms_long_template_object_id,@name_attr_id,unix_timestamp()*1000);
SET @name_value_id = last_insert_id();
INSERT INTO `valuev` (`value`,`value_id_fk`) VALUES ("Long SMS",@name_value_id);

INSERT INTO `obj_values` (`object_id_fk`,`attr_id_fk`, `timestamp`) VALUES (@sms_long_template_object_id,@header_attr_id,unix_timestamp()*1000);
SET @header_value_id = last_insert_id();
INSERT INTO `valuev` (`value`,`value_id_fk`) VALUES ("",@header_value_id);

INSERT INTO `obj_values` (`object_id_fk`,`attr_id_fk`, `timestamp`) VALUES (@sms_long_template_object_id,@body_attr_id,unix_timestamp()*1000);
SET @body_value_id = last_insert_id();
INSERT INTO `valuev` (`value`,`value_id_fk`) VALUES ("Tier $MSG_TIER$ System  Alert - $parent(DC).name$ \\ $name$\n\nAlert: $ALERT_NAME$\nTrigger Value: $TRIGGER_DATA_VALUE$\nCurrent Value: $CURRENT_DATA_VALUE$",@body_value_id);

INSERT INTO `obj_values` (`object_id_fk`,`attr_id_fk`, `timestamp`) VALUES (@sms_long_template_object_id,@descr_attr_id,unix_timestamp()*1000);
SET @descr_value_id = last_insert_id();
INSERT INTO `valuev` (`value`,`value_id_fk`) VALUES ("A long SMS alert notification",@descr_value_id);

INSERT INTO `obj_values` (`object_id_fk`,`attr_id_fk`, `timestamp`) VALUES (@sms_long_template_object_id,@mtype_attr_id,unix_timestamp()*1000);
SET @mt_value_id = last_insert_id();
INSERT INTO `valuev` (`value`,`value_id_fk`) VALUES ("2",@mt_value_id);

INSERT INTO `obj_values` (`object_id_fk`,`attr_id_fk`, `timestamp`) VALUES (@sms_long_template_object_id,@template_type_attr_id,unix_timestamp()*1000);
SET @tt_value_id = last_insert_id();
INSERT INTO `valuei` (`value`,`value_id_fk`) VALUES (1,@tt_value_id);

-- predefined Short email, code = 1
INSERT INTO `objects` (`object_type_id_fk`,`name`,`static`) VALUES (@amt_type_id,null,0);
SET @template_object_id = last_insert_id();

INSERT INTO `obj_values` (`object_id_fk`,`attr_id_fk`, `timestamp`) VALUES (@template_object_id,@name_attr_id,unix_timestamp()*1000);
SET @name_value_id = last_insert_id();
INSERT INTO `valuev` (`value`,`value_id_fk`) VALUES ("Short Email",@name_value_id);

INSERT INTO `obj_values` (`object_id_fk`,`attr_id_fk`, `timestamp`) VALUES (@template_object_id,@header_attr_id,unix_timestamp()*1000);
SET @header_value_id = last_insert_id();
INSERT INTO `valuev` (`value`,`value_id_fk`) VALUES ("Tier $MSG_TIER$ System  Alert - $parent(DC).name$ \\ $name$ \\ $ALERT_NAME$ @ $TRIGGER_DATA_VALUE$",@header_value_id);

INSERT INTO `obj_values` (`object_id_fk`,`attr_id_fk`, `timestamp`) VALUES (@template_object_id,@body_attr_id,unix_timestamp()*1000);
SET @body_value_id = last_insert_id();
INSERT INTO `valuev` (`value`,`value_id_fk`) VALUES ("Tier $MSG_TIER$ System  Alert - $parent(DC).name$ \\ $parent(ZONE).name$ \\ $name$\n\nAlert: $ALERT_NAME$\nTime: $TIMESTAMP$\nTrigger Value: $TRIGGER_DATA_VALUE$\nCurrent Value: $CURRENT_DATA_VALUE$",@body_value_id);

INSERT INTO `obj_values` (`object_id_fk`,`attr_id_fk`, `timestamp`) VALUES (@template_object_id,@descr_attr_id,unix_timestamp()*1000);
SET @descr_value_id = last_insert_id();
INSERT INTO `valuev` (`value`,`value_id_fk`) VALUES ("A short email alert notification",@descr_value_id);

INSERT INTO `obj_values` (`object_id_fk`,`attr_id_fk`, `timestamp`) VALUES (@template_object_id,@mtype_attr_id,unix_timestamp()*1000);
SET @mt_value_id = last_insert_id();
INSERT INTO `valuev` (`value`,`value_id_fk`) VALUES ("1",@mt_value_id);

INSERT INTO `obj_values` (`object_id_fk`,`attr_id_fk`, `timestamp`) VALUES (@template_object_id,@template_type_attr_id,unix_timestamp()*1000);
SET @tt_value_id = last_insert_id();
INSERT INTO `valuei` (`value`,`value_id_fk`) VALUES (1,@tt_value_id);

-- predefined trap template, code = 3
INSERT INTO `objects` (`object_type_id_fk`,`name`,`static`) VALUES (@amt_type_id,null,0);
SET @snmp_template_object_id = last_insert_id();

INSERT INTO `obj_values` (`object_id_fk`,`attr_id_fk`, `timestamp`) VALUES (@snmp_template_object_id,@name_attr_id,unix_timestamp()*1000);
SET @name_value_id = last_insert_id();
INSERT INTO `valuev` (`value`,`value_id_fk`) VALUES ("SNMP trap",@name_value_id);

INSERT INTO `obj_values` (`object_id_fk`,`attr_id_fk`, `timestamp`) VALUES (@snmp_template_object_id,@header_attr_id,unix_timestamp()*1000);
SET @header_value_id = last_insert_id();
INSERT INTO `valuev` (`value`,`value_id_fk`) VALUES ("",@header_value_id);

INSERT INTO `obj_values` (`object_id_fk`,`attr_id_fk`, `timestamp`) VALUES (@snmp_template_object_id,@body_attr_id,unix_timestamp()*1000);
SET @body_value_id = last_insert_id();
INSERT INTO `valuev` (`value`,`value_id_fk`) VALUES ("Tier $MSG_TIER$ System  Alert - $parent(DC).name$ \\ $name$ \\ $ALERT_NAME$ @ $TRIGGER_DATA_VALUE$\n\nCurrent Value: $CURRENT_DATA_VALUE$",@body_value_id);

INSERT INTO `obj_values` (`object_id_fk`,`attr_id_fk`, `timestamp`) VALUES (@snmp_template_object_id,@descr_attr_id,unix_timestamp()*1000);
SET @descr_value_id = last_insert_id();
INSERT INTO `valuev` (`value`,`value_id_fk`) VALUES ("A short SNMP Trap notification",@descr_value_id);

INSERT INTO `obj_values` (`object_id_fk`,`attr_id_fk`, `timestamp`) VALUES (@snmp_template_object_id,@mtype_attr_id,unix_timestamp()*1000);
SET @mt_value_id = last_insert_id();
INSERT INTO `valuev` (`value`,`value_id_fk`) VALUES ("3",@mt_value_id);

INSERT INTO `obj_values` (`object_id_fk`,`attr_id_fk`, `timestamp`) VALUES (@snmp_template_object_id,@template_type_attr_id,unix_timestamp()*1000);
SET @tt_value_id = last_insert_id();
INSERT INTO `valuei` (`value`,`value_id_fk`) VALUES (1,@tt_value_id);


-- predefined console template, code = 4
INSERT INTO `objects` (`object_type_id_fk`,`name`,`static`) VALUES (@amt_type_id,null,0);
SET @console_template_object_id = last_insert_id();

INSERT INTO `obj_values` (`object_id_fk`,`attr_id_fk`, `timestamp`) VALUES (@console_template_object_id,@name_attr_id,unix_timestamp()*1000);
SET @name_value_id = last_insert_id();
INSERT INTO `valuev` (`value`,`value_id_fk`) VALUES ("Console Notification",@name_value_id);

INSERT INTO `obj_values` (`object_id_fk`,`attr_id_fk`, `timestamp`) VALUES (@console_template_object_id,@header_attr_id,unix_timestamp()*1000);
SET @header_value_id = last_insert_id();
INSERT INTO `valuev` (`value`,`value_id_fk`) VALUES ("",@header_value_id);

INSERT INTO `obj_values` (`object_id_fk`,`attr_id_fk`, `timestamp`) VALUES (@console_template_object_id,@body_attr_id,unix_timestamp()*1000);
SET @body_value_id = last_insert_id();
INSERT INTO `valuev` (`value`,`value_id_fk`) VALUES ("Trigger Value: $TRIGGER_DATA_VALUE$\nCurrent Value: $CURRENT_DATA_VALUE$",@body_value_id);

INSERT INTO `obj_values` (`object_id_fk`,`attr_id_fk`, `timestamp`) VALUES (@console_template_object_id,@descr_attr_id,unix_timestamp()*1000);
SET @descr_value_id = last_insert_id();
INSERT INTO `valuev` (`value`,`value_id_fk`) VALUES ("A notification used in Console display of alert details",@descr_value_id);

INSERT INTO `obj_values` (`object_id_fk`,`attr_id_fk`, `timestamp`) VALUES (@console_template_object_id,@mtype_attr_id,unix_timestamp()*1000);
SET @mt_value_id = last_insert_id();
INSERT INTO `valuev` (`value`,`value_id_fk`) VALUES ("4",@mt_value_id);

INSERT INTO `obj_values` (`object_id_fk`,`attr_id_fk`, `timestamp`) VALUES (@console_template_object_id,@template_type_attr_id,unix_timestamp()*1000);
SET @tt_value_id = last_insert_id();
INSERT INTO `valuei` (`value`,`value_id_fk`) VALUES (0,@tt_value_id);


-- predefined console template for system alerts, code = 4
INSERT INTO `objects` (`object_type_id_fk`,`name`,`static`) VALUES (@amt_type_id,null,0);
SET @console_template_object_id = last_insert_id();

INSERT INTO `obj_values` (`object_id_fk`,`attr_id_fk`, `timestamp`) VALUES (@console_template_object_id,@name_attr_id,unix_timestamp()*1000);
SET @name_value_id = last_insert_id();
INSERT INTO `valuev` (`value`,`value_id_fk`) VALUES ("Console System Notification",@name_value_id);

INSERT INTO `obj_values` (`object_id_fk`,`attr_id_fk`, `timestamp`) VALUES (@console_template_object_id,@header_attr_id,unix_timestamp()*1000);
SET @header_value_id = last_insert_id();
INSERT INTO `valuev` (`value`,`value_id_fk`) VALUES ("",@header_value_id);

INSERT INTO `obj_values` (`object_id_fk`,`attr_id_fk`, `timestamp`) VALUES (@console_template_object_id,@body_attr_id,unix_timestamp()*1000);
SET @body_value_id = last_insert_id();
INSERT INTO `valuev` (`value`,`value_id_fk`) VALUES ("$message$",@body_value_id);

INSERT INTO `obj_values` (`object_id_fk`,`attr_id_fk`, `timestamp`) VALUES (@console_template_object_id,@descr_attr_id,unix_timestamp()*1000);
SET @descr_value_id = last_insert_id();
INSERT INTO `valuev` (`value`,`value_id_fk`) VALUES ("A notification used in Console display of system alert details",@descr_value_id);

INSERT INTO `obj_values` (`object_id_fk`,`attr_id_fk`, `timestamp`) VALUES (@console_template_object_id,@mtype_attr_id,unix_timestamp()*1000);
SET @mt_value_id = last_insert_id();
INSERT INTO `valuev` (`value`,`value_id_fk`) VALUES ("4",@mt_value_id);

INSERT INTO `obj_values` (`object_id_fk`,`attr_id_fk`, `timestamp`) VALUES (@console_template_object_id,@template_type_attr_id,unix_timestamp()*1000);
SET @tt_value_id = last_insert_id();
INSERT INTO `valuei` (`value`,`value_id_fk`) VALUES (1,@tt_value_id);

-- math error alert system rule type
INSERT INTO `tbl_alert_type` (`alert_priority_id_fk`,`name`,`description`,`header`,`body`,`is_active`,`auto_dismiss`) VALUES(1,"Math Error Alert","This alert is triggered when a math error is detected in a CRE Rule","Math Error in Rule","$message$",1,1);


DELIMITER $$

DROP PROCEDURE IF EXISTS `move_temlates` $$
CREATE PROCEDURE  `move_temlates`(in_console_id INT, in_short_sms_id INT, in_snmp_trap_id INT)
BEGIN
    DECLARE done                            INT DEFAULT 0;

    DECLARE cv_alert_type_id         INT;

    DECLARE currentTime              LONG;

    DECLARE
      cur_alert_types CURSOR FOR
        SELECT alert_type_id FROM temp_system_alert_types;

    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

    SET currentTime = UNIX_TIMESTAMP() * 1000;
	
    create temporary table temp_system_alert_types (alert_type_id int primary key);
    -- do this because we do not know how to distinguish system alert from others
    insert into temp_system_alert_types select alert_type_id from tbl_alert_type where name = 'WSN_CONFIGURATION';
    insert into temp_system_alert_types select alert_type_id from tbl_alert_type where name = 'Unbalanced Phases Alert';
    insert into temp_system_alert_types select alert_type_id from tbl_alert_type where name = 'UNKNOWN';
    insert into temp_system_alert_types select alert_type_id from tbl_alert_type where name = 'Sensor requires service';
    insert into temp_system_alert_types select alert_type_id from tbl_alert_type where name = 'Sensor disconnected';
    insert into temp_system_alert_types select alert_type_id from tbl_alert_type where name = 'Out of recommended range';
    insert into temp_system_alert_types select alert_type_id from tbl_alert_type where name = 'Out of allowable range';
    insert into temp_system_alert_types select alert_type_id from tbl_alert_type where name = 'Node not reporting';
    insert into temp_system_alert_types select alert_type_id from tbl_alert_type where name = 'Maximum Threshold Alert';
    insert into temp_system_alert_types select alert_type_id from tbl_alert_type where name = 'Low battery';
    insert into temp_system_alert_types select alert_type_id from tbl_alert_type where name = 'Communication error';
    insert into temp_system_alert_types select alert_type_id from tbl_alert_type where name = 'Circular dependency in CRE rule';
    insert into temp_system_alert_types select alert_type_id from tbl_alert_type where name = 'Circuit Power above threshold';
    insert into temp_system_alert_types select alert_type_id from tbl_alert_type where name = 'Circuit Power Factor below threshold';
    insert into temp_system_alert_types select alert_type_id from tbl_alert_type where name = 'Circuit Current above threshold';
    insert into temp_system_alert_types select alert_type_id from tbl_alert_type where name = 'Audit Alert';
    insert into temp_system_alert_types select alert_type_id from tbl_alert_type where name = 'Math Error Alert';

   OPEN cur_alert_types;

   main:
    LOOP
      FETCH cur_alert_types
      INTO cv_alert_type_id;

      IF done
      THEN
        LEAVE main;
      END IF;

       -- update body text as in seed	
       update tbl_alert_type set body = '$message$' where alert_type_id = cv_alert_type_id;

       INSERT INTO `tbl_alert_type_template` (`alert_type_id_fk`,`transport_type_id_fk`,`object_id_fk`) values (cv_alert_type_id,4,in_console_id);
	  
    END LOOP;

    CLOSE cur_alert_types; 
  
END $$
DELIMITER ;

CALL move_temlates(@console_template_object_id,-1, -1);

DROP PROCEDURE IF EXISTS `move_temlates`;

-- initital ids to start 
-- should be updated each time in addition to new INSERT INTO clause
SET @max_object_id := IFNULL((SELECT max(`id`)+1 FROM `objects`),1);
SET @max_value_id := IFNULL((SELECT max(`value_id`)+1 FROM `obj_values`),1);
SET @max_valued_id := IFNULL((SELECT max(`id`)+1 FROM `valued`),1);
SET @max_valuel_id := IFNULL((SELECT max(`id`)+1 FROM `valuel`),1);
SET @max_valuei_id := IFNULL((SELECT max(`id`)+1 FROM `valuei`),1);
SET @max_valuev_id := IFNULL((SELECT max(`id`)+1 FROM `valuev`),1);
SET @max_valuelink_id := IFNULL((SELECT max(`id`)+1 FROM `value_obj_link`),1);
SET @max_valueplink_id := IFNULL((SELECT max(`id`)+1 FROM `value_property_link`),1);

delete from uid_table;
INSERT INTO `uid_table` (`object_id`,`value_id`,`valued_id`,`valuev_id`,`valuei_id`,`valuel_id`,`valuelink_id`,`valueplink_id`) values (@max_object_id,@max_value_id,@max_valued_id,@max_valuev_id,@max_valuei_id,@max_valuel_id,@max_valuelink_id,@max_valueplink_id);

ALTER TABLE `TBL_ALERT_TYPE` ADD
  CONSTRAINT `alert_type_has_priority` FOREIGN KEY (`alert_priority_id_FK`)
    REFERENCES `TBL_ALERT_PRIORITY`(`alert_priority_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION;
    
ALTER TABLE `TBL_ALERT_TYPE_DESTINATION_ASSOC` ADD
  CONSTRAINT `Ref_84` FOREIGN KEY (`transport_type_id_FK`)
    REFERENCES `TBL_TRANSPORT_TYPE`(`transport_type_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION;
    
ALTER TABLE `TBL_ALERT_TYPE_DESTINATION_ASSOC` ADD
  CONSTRAINT `Ref_85` FOREIGN KEY (`alert_type_id_FK`)
    REFERENCES `TBL_ALERT_TYPE`(`alert_type_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION;

ALTER TABLE `TBL_ALERT_TYPE_DESTINATION_ASSOC` ADD
  CONSTRAINT `Ref_54` FOREIGN KEY (`priority_tier_id_FK`)
    REFERENCES `TBL_ALERT_PRIORITY_TIER`(`priority_tier_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION;

ALTER TABLE `TBL_ALERT_TYPE_DESTINATION_ASSOC` ADD
  CONSTRAINT `Ref_63` FOREIGN KEY (`dest_id_FK`)
    REFERENCES `tbl_destinations`(`destination_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION;

    ALTER TABLE `TBL_ALERT_PRIORITY_TIER` ADD
  CONSTRAINT `tier_belongs_to_priority` FOREIGN KEY (`alert_priority_id_FK`)
    REFERENCES `TBL_ALERT_PRIORITY`(`alert_priority_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION;

ALTER TABLE `TBL_ALERT_TYPE_TEMPLATE` ADD
  CONSTRAINT `template_relates_to_transport_type` FOREIGN KEY (`transport_type_id_FK`)
    REFERENCES `TBL_TRANSPORT_TYPE`(`transport_type_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION;

ALTER TABLE `TBL_ALERT_TYPE_TEMPLATE` ADD
  CONSTRAINT `template_belongs_to_alert_type` FOREIGN KEY (`alert_type_id_FK`)
    REFERENCES `TBL_ALERT_TYPE`(`alert_type_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION;

ALTER TABLE `TBL_ALERT_TYPE_TEMPLATE` ADD
  CONSTRAINT `template_stores_in_object` FOREIGN KEY (`object_id_FK`)
    REFERENCES `OBJECTS`(`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION;

ALTER TABLE `tbl_destinations` ADD
  CONSTRAINT `destination_refers_user` FOREIGN KEY (`user_id_fk`)
    REFERENCES `TBL_USERS`(`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION;

DELETE FROM `tbl_rule_types` WHERE `name`='Autodismiss alerts';