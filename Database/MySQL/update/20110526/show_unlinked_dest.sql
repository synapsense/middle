# shows all destinations highlighting those destinations which do not belong to any user
select at.name 'alert', d.dest_name 'email', ifnull(u.user_name,'<?>') 'user'
from 
(tbl_destinations as d
    join tbl_alert_type_action_dest_assoc  on dest_id = dest_id_fk
    join tbl_alert_type as at on alert_type_id_fk = alert_type_id 
    join tbl_performers as p on performer_id_fk = performer_id
)
left join tbl_users as u on dest_name = primary_email 
