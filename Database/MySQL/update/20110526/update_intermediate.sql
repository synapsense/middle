use synap;

DELIMITER $$

DROP PROCEDURE IF EXISTS `move_temlates` $$
CREATE PROCEDURE  `move_temlates`(in_console_id INT, in_short_sms_id INT, in_snmp_trap_id INT)
BEGIN
    DECLARE done                            INT DEFAULT 0;

    DECLARE cv_alert_type_id         INT;

    DECLARE currentTime                 LONG;

    DECLARE
      cur_alert_types CURSOR FOR
        SELECT alert_type_id FROM temp_system_alert_types;

    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

    SET currentTime = UNIX_TIMESTAMP() * 1000;
	
    create temporary table temp_system_alert_types (alert_type_id int primary key);
    -- do this because we do not know how to distinguish system alert from others
    insert into temp_system_alert_types select alert_type_id from tbl_alert_type where name = 'WSN_CONFIGURATION';
    insert into temp_system_alert_types select alert_type_id from tbl_alert_type where name = 'Unbalanced Phases Alert';
    insert into temp_system_alert_types select alert_type_id from tbl_alert_type where name = 'UNKNOWN';
    insert into temp_system_alert_types select alert_type_id from tbl_alert_type where name = 'Sensor requires service';
    insert into temp_system_alert_types select alert_type_id from tbl_alert_type where name = 'Sensor disconnected';
    insert into temp_system_alert_types select alert_type_id from tbl_alert_type where name = 'Out of recommended range';
    insert into temp_system_alert_types select alert_type_id from tbl_alert_type where name = 'Out of allowable range';
    insert into temp_system_alert_types select alert_type_id from tbl_alert_type where name = 'Node not reporting';
    insert into temp_system_alert_types select alert_type_id from tbl_alert_type where name = 'Maximum Threshold Alert';
    insert into temp_system_alert_types select alert_type_id from tbl_alert_type where name = 'Low battery';
    insert into temp_system_alert_types select alert_type_id from tbl_alert_type where name = 'Communication error';
    insert into temp_system_alert_types select alert_type_id from tbl_alert_type where name = 'Circular dependency in CRE rule';
    insert into temp_system_alert_types select alert_type_id from tbl_alert_type where name = 'Circuit Power above threshold';
    insert into temp_system_alert_types select alert_type_id from tbl_alert_type where name = 'Circuit Power Factor below threshold';
    insert into temp_system_alert_types select alert_type_id from tbl_alert_type where name = 'Circuit Current above threshold';
    insert into temp_system_alert_types select alert_type_id from tbl_alert_type where name = 'Audit Alert';

   OPEN cur_alert_types;

   main:
    LOOP
      FETCH cur_alert_types
      INTO cv_alert_type_id;

      IF done
      THEN
        LEAVE main;
      END IF;

       -- update body text as in seed	
       update tbl_alert_type set body = '$message$' where alert_type_id = cv_alert_type_id;
       -- cleanup old templates links if exist (this is done here because only system alert types eligible for such update)
       delete from valuev where value_id_fk in (select value_id from obj_values where object_id_fk in (select  object_id_fk from tbl_alert_type_template where alert_type_id_fk = cv_alert_type_id and transport_type_id_fk in (1)));
       delete from obj_values where object_id_fk in (select  object_id_fk from tbl_alert_type_template where alert_type_id_fk = cv_alert_type_id and transport_type_id_fk in (1) );
       delete obj, at from objects as obj , tbl_alert_type_template as at where at.alert_type_id_fk = cv_alert_type_id and at.transport_type_id_fk in (1) and at.object_id_fk = obj.id;
       delete from tbl_alert_type_template where alert_type_id_fk = cv_alert_type_id;

       INSERT INTO `tbl_alert_type_template` (`alert_type_id_fk`,`transport_type_id_fk`,`object_id_fk`) values (cv_alert_type_id,4,in_console_id);
	  
    END LOOP;

    CLOSE cur_alert_types; 
  
END $$
DELIMITER ;

START TRANSACTION;

SET @amt_type_id = (select objtype_id from object_types where name = 'ALERT_MESSAGE_TEMPLATE');
SET @name_attr_id = (select attr_id from attributes where name = 'name' and object_type_id_fk = @amt_type_id);
SET @header_attr_id = (select attr_id from attributes where name = 'header' and object_type_id_fk = @amt_type_id);
SET @body_attr_id = (select attr_id from attributes where name = 'body' and object_type_id_fk = @amt_type_id);
SET @descr_attr_id = (select attr_id from attributes where name = 'description' and object_type_id_fk = @amt_type_id);
SET @mtype_attr_id = (select attr_id from attributes where name = 'messagetype' and object_type_id_fk = @amt_type_id);


-- predefined console template for system alerts, code = 4
INSERT INTO `objects` (`object_type_id_fk`,`name`,`static`) VALUES (@amt_type_id,null,0);
SET @console_template_object_id = last_insert_id();

INSERT INTO `obj_values` (`object_id_fk`,`attr_id_fk`, `timestamp`) VALUES (@console_template_object_id,@name_attr_id,unix_timestamp()*1000);
SET @name_value_id = last_insert_id();
INSERT INTO `valuev` (`value`,`value_id_fk`) VALUES ("Console System Notification",@name_value_id);

INSERT INTO `obj_values` (`object_id_fk`,`attr_id_fk`, `timestamp`) VALUES (@console_template_object_id,@header_attr_id,unix_timestamp()*1000);
SET @header_value_id = last_insert_id();
INSERT INTO `valuev` (`value`,`value_id_fk`) VALUES ("",@header_value_id);

INSERT INTO `obj_values` (`object_id_fk`,`attr_id_fk`, `timestamp`) VALUES (@console_template_object_id,@body_attr_id,unix_timestamp()*1000);
SET @body_value_id = last_insert_id();
INSERT INTO `valuev` (`value`,`value_id_fk`) VALUES ("$message$",@body_value_id);

INSERT INTO `obj_values` (`object_id_fk`,`attr_id_fk`, `timestamp`) VALUES (@console_template_object_id,@descr_attr_id,unix_timestamp()*1000);
SET @descr_value_id = last_insert_id();
INSERT INTO `valuev` (`value`,`value_id_fk`) VALUES ("A notification used in Console display of system alert details",@descr_value_id);

INSERT INTO `obj_values` (`object_id_fk`,`attr_id_fk`, `timestamp`) VALUES (@console_template_object_id,@mtype_attr_id,unix_timestamp()*1000);
SET @mt_value_id = last_insert_id();
INSERT INTO `valuev` (`value`,`value_id_fk`) VALUES ("4",@mt_value_id);

CALL move_temlates(@console_template_object_id,-1,-1);

COMMIT;

DROP PROCEDURE IF EXISTS `move_temlates`;