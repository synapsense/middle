use `synap`;

drop table if exists tbl_neighbor_list;
drop table if exists tbl_instances;
drop table if exists tbl_db_version;
drop table if exists tbl_custom_queries;
drop table if exists tbl_configuration;
drop table if exists inheritance;

-- Drop Event clear_expired_data
drop event if exists `clear_expired_data`;

delimiter |
create event `clear_expired_data`
  on schedule every '1' month starts '2009-10-01 00:01:00'
  enable
do
begin
  delete from tbl_nht_info
  where report_timestamp <= date_sub(curdate(),interval 1 month);
  delete from tbl_network_statnode
  where nwk_timestamp <= date_sub(curdate(),interval 1 month);
  delete from tbl_activity_log where `timestamp` <= date_sub(curdate(),interval 1 month);
  delete from tbl_battery_capacity where battery_capacity_time_stamp <= date_sub(curdate(),interval 1 month);
  delete from tbl_network_statbase where nwk_timestamp <= date_sub(curdate(),interval 1 month);
 end; |
delimiter ;

ALTER table `tbl_sensor_node` modify `visualization_location_x` DOUBLE(15,6) COMMENT 'x location in visualization app';
ALTER table `tbl_sensor_node` modify `visualization_location_y` DOUBLE(15,6) COMMENT 'y location in visualization app';
ALTER table `tbl_sensor` modify `visual_loc_x` DOUBLE(15,6);
ALTER table `tbl_sensor` modify `visual_loc_y` DOUBLE(15,6);
