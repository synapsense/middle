use synap;

-- converting properties x,y from int to double
-- updates existing values to doubles
START TRANSACTION;

-- copy values to table of doubles
insert into valued (value, value_id_fk) 
select vi.value, ov.value_id 
from valuei vi, obj_values ov, object_types ot, objects o, attributes a
where vi.value_id_fk = ov.value_id 
and ov.object_id_fk = o.id
and ov.attr_id_fk = a.attr_id 
and ot.objtype_id = o.object_type_id_fk
and ot.objtype_id = a.object_type_id_fk
and ot.name = 'ION_WIRELESS'
and a.name in ('x', 'y');

-- clean up integers
delete vi from valuei vi, obj_values ov, object_types ot, objects o, attributes a
where vi.value_id_fk = ov.value_id 
and ov.object_id_fk = o.id
and ov.attr_id_fk = a.attr_id 
and ot.objtype_id = o.object_type_id_fk
and ot.objtype_id = a.object_type_id_fk
and ot.name = 'ION_WIRELESS'
and a.name in ('x', 'y');

-- update metadata - int to double 
update attributes a, object_types ot set a.attr_type_id_fk = 2 
where ot.objtype_id = a.object_type_id_fk
and ot.name = 'ION_WIRELESS'
and a.name in ('x', 'y');


COMMIT;



