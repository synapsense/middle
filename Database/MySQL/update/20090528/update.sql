use `synap`;
ALTER TABLE `tbl_visualization_images` ADD INDEX `zone_dataclass_layer_time_idx`(`zone`, `dataclass`, `layer`, `showingtime`);
ALTER TABLE `tbl_visualization_images` ADD INDEX `server_time_idx`(`server`, `showingtime`);
ALTER TABLE `tbl_visualization_images` DROP INDEX `Visual -> Servers`;




