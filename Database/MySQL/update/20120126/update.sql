use synap;

INSERT INTO `tbl_visualization_legend` (`id`, `server`, `dataclass`, `captures`, `last_updated`) VALUES (4,1,998,5,0);

-- dewpoint
INSERT INTO `tbl_visualization_legend_point` (`legend`, `position`, `red`, `green`, `blue`) VALUES (4,25, 230,111,0);
INSERT INTO `tbl_visualization_legend_point` (`legend`, `position`, `red`, `green`, `blue`) VALUES (4,35, 128,206,71);
INSERT INTO `tbl_visualization_legend_point` (`legend`, `position`, `red`, `green`, `blue`) VALUES (4,39, 100,255,0);
INSERT INTO `tbl_visualization_legend_point` (`legend`, `position`, `red`, `green`, `blue`) VALUES (4,43, 50,255,0);
INSERT INTO `tbl_visualization_legend_point` (`legend`, `position`, `red`, `green`, `blue`) VALUES (4,46, 0,255,0);
INSERT INTO `tbl_visualization_legend_point` (`legend`, `position`, `red`, `green`, `blue`) VALUES (4,51, 173,255,241);
INSERT INTO `tbl_visualization_legend_point` (`legend`, `position`, `red`, `green`, `blue`) VALUES (4,58, 51,160,255);
INSERT INTO `tbl_visualization_legend_point` (`legend`, `position`, `red`, `green`, `blue`) VALUES (4,62, 51,51,255);
INSERT INTO `tbl_visualization_legend_point` (`legend`, `position`, `red`, `green`, `blue`) VALUES (4,70, 255,0,0);