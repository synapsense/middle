use synap;

-- auxilary function

DROP FUNCTION IF EXISTS insert_object_value;
DELIMITER |
CREATE FUNCTION insert_object_value(
  in_object_id int, in_property_id int, in_timestamp long)
  RETURNS INTEGER
  BEGIN
    set @id=0;
    SELECT value_id into @id
        from obj_values
        where object_id_fk=in_object_id and attr_id_fk=in_property_id limit 1;

    IF @id!=0
    THEN
        RETURN @id;
    END IF;

    INSERT INTO obj_values(
                  object_id_fk, attr_id_fk, TIMESTAMP)
    VALUES (in_object_id, in_property_id, in_timestamp);

    RETURN last_insert_id();
  END|
DELIMITER ;

drop procedure if exists alert_update;
delimiter |
create procedure alert_update()
begin
    
    declare done                                    int default 0;

    declare alert_type_obj_id                       int;
    declare alert_type_obj_id_timestamp             long;
    declare alert_hist_id                           int;

    declare 
            cur_alert_hist_id cursor for 
                select `vi`.`value`, `ov`.`timestamp`, `o`.`id` from object_types as ot 
                    join objects as o on ot.objtype_id=o.object_type_id_fk 
                    join attributes as attr on attr.object_type_id_fk = ot.objtype_id
                    left join obj_values as ov on o.id = ov.object_id_fk and attr.attr_id = ov.attr_id_fk
                    left join valuei as vi on ov.value_id=vi.value_id_fk 
                    where ot.name = 'ALERT_HISTORY' 
                        and o.static = 0 
                        and attr.name = 'type';

    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

    -- add attributes to the ALERT_HISTORY type

    select objtype_id into @alert_hist_type_id from object_types where name = 'ALERT_HISTORY' limit 1;
    
    INSERT INTO `attributes`(`name`,`mapped_name`,`historical`,`object_type_id_fk`,`attr_type_id_fk`,`collection`,`static`,`version`) 
    VALUES ('typeName', null,1,@alert_hist_type_id,3,0,0,0);
   
    set @alert_type_name_attr_id := last_insert_id();
   
    INSERT INTO `attributes`(`name`,`mapped_name`,`historical`,`object_type_id_fk`,`attr_type_id_fk`,`collection`,`static`,`version`) 
    VALUES ('typeDescr', null,1,@alert_hist_type_id,3,0,0,0);
   
    set @alert_type_descr_attr_id := last_insert_id();
    
    INSERT INTO `attributes`(`name`,`mapped_name`,`historical`,`object_type_id_fk`,`attr_type_id_fk`,`collection`,`static`,`version`) 
    VALUES ('priority', null,1,@alert_hist_type_id,3,0,0,0);
   
    set @alert_type_prior_attr_id := last_insert_id();

    -- set new attributes to objects
    
    open cur_alert_hist_id;
    
    main:loop
    
        fetch cur_alert_hist_id into alert_type_obj_id, alert_type_obj_id_timestamp, alert_hist_id;
            
        if done
        then
            close cur_alert_hist_id;
            leave main;
        end if;
    
        -- select alert type name
        select vv.value into @alert_type_name from objects as o
            join object_types as ot on ot.objtype_id=o.object_type_id_fk
            join attributes as attr on ot.objtype_id=attr.object_type_id_fk
            left join obj_values as ov on o.id = ov.object_id_fk and attr.attr_id = ov.attr_id_fk 
            left join valuev as vv on ov.value_id=vv.value_id_fk 
            where o.id=alert_type_obj_id 
                and attr.name ='name';
         
        -- select alert type description 
        select vv.value into @alert_type_descr from objects as o
            join object_types as ot on ot.objtype_id=o.object_type_id_fk
            join attributes as attr on ot.objtype_id=attr.object_type_id_fk
            left join obj_values as ov on o.id = ov.object_id_fk and attr.attr_id = ov.attr_id_fk 
            left join valuev as vv on ov.value_id=vv.value_id_fk 
            where o.id=alert_type_obj_id 
                and attr.name ='description';
        
        -- select alert priority obj id
        select vol.object_id into @priority_obj_id from objects as o
            join object_types as ot on ot.objtype_id=o.object_type_id_fk
            join attributes as attr on ot.objtype_id=attr.object_type_id_fk
            left join obj_values as ov on o.id = ov.object_id_fk and attr.attr_id = ov.attr_id_fk 
            left join value_obj_link as vol on ov.value_id=vol.value_id_fk 
            where o.id=alert_type_obj_id
                and attr.name ='priority' limit 1;
         
        -- select priority name
        select vv.value into @priority_name from objects as o
            join object_types as ot on ot.objtype_id=o.object_type_id_fk
            join attributes as attr on ot.objtype_id=attr.object_type_id_fk
            left join obj_values as ov on o.id = ov.object_id_fk and attr.attr_id = ov.attr_id_fk 
            left join valuev as vv on ov.value_id=vv.value_id_fk 
            where o.id=@priority_obj_id 
                and attr.name ='name' limit 1;
        
		if alert_type_obj_id_timestamp is not null then
			set @name_valueId := insert_object_value(alert_hist_id,@alert_type_name_attr_id, alert_type_obj_id_timestamp);
			if @alert_type_name is not null then
				INSERT INTO valuev(value_id_fk, VALUE) VALUES (@name_valueId, @alert_type_name);
			else
				INSERT INTO valuev(value_id_fk, VALUE) VALUES (@name_valueId, 'UNKNOWN');
			end if;
			
			set @descr_valueId = insert_object_value(alert_hist_id,@alert_type_descr_attr_id, alert_type_obj_id_timestamp);
			if @alert_type_descr is not null then
				if char_length(@alert_type_descr) = 0 then
					set @alert_type_descr = null; 
				end if;
				INSERT INTO valuev(value_id_fk, VALUE) VALUES (@descr_valueId, @alert_type_descr);
			else
				INSERT INTO valuev(value_id_fk, VALUE) VALUES (@descr_valueId, 'Generic alert, any alert with non-existing type is saved as UNKNOWN');
			end if;
			
			set @priority_valueId := insert_object_value(alert_hist_id,@alert_type_prior_attr_id, alert_type_obj_id_timestamp);
			if @priority_name is not null then
				 INSERT INTO valuev(value_id_fk, VALUE) VALUES (@priority_valueId, @priority_name);
			else
				 INSERT INTO valuev(value_id_fk, VALUE) VALUES (@priority_valueId, 'MAJOR');
			end if;
		end if;
        
        -- set new attributes to historical values of current object
    
        history:begin
        
            declare history_done                int default 0;
            
            declare hv_timestamp                            datetime;
            declare hv_valuei                               int;
            declare hv_group_id                             int;
        
            declare 
                cur_alert_hist_history cursor for 
                    select `hv`.`timestamp`, `valuei`, `group_id` from object_types as ot 
                        join objects as o on ot.objtype_id=o.object_type_id_fk 
                        join attributes as attr on attr.object_type_id_fk = ot.objtype_id
                        left join obj_values as ov on o.id = ov.object_id_fk and attr.attr_id = ov.attr_id_fk
                        left join historical_values as hv on ov.value_id = hv.value_id_fk
                        where ot.name = 'ALERT_HISTORY' 
                            and o.static = 0 
                            and attr.name = 'type' 
                            and o.id=alert_hist_id;
        
            DECLARE CONTINUE HANDLER FOR NOT FOUND SET history_done = 1;
            
            open cur_alert_hist_history;
            
            loop2: loop 
        
                fetch cur_alert_hist_history into hv_timestamp, hv_valuei, hv_group_id;
        
                if history_done
                then
                    close cur_alert_hist_history;
                    leave history;
                end if;
                
            -- select alert type name
            select vv.value into @alert_type_name from objects as o
                join object_types as ot on ot.objtype_id=o.object_type_id_fk
                join attributes as attr on ot.objtype_id=attr.object_type_id_fk
                left join obj_values as ov on o.id = ov.object_id_fk and attr.attr_id = ov.attr_id_fk 
                left join valuev as vv on ov.value_id=vv.value_id_fk 
                where o.id=hv_valuei 
                    and attr.name ='name';
                
            -- select alert type description
            select vv.value into @alert_type_descr from objects as o
                join object_types as ot on ot.objtype_id=o.object_type_id_fk
                join attributes as attr on ot.objtype_id=attr.object_type_id_fk
                left join obj_values as ov on o.id = ov.object_id_fk and attr.attr_id = ov.attr_id_fk 
                left join valuev as vv on ov.value_id=vv.value_id_fk 
                where o.id=hv_valuei
                    and attr.name ='description';
                    
            -- select alert priority obj id
            select vol.object_id into @priority_obj_id from objects as o
                join object_types as ot on ot.objtype_id=o.object_type_id_fk
                join attributes as attr on ot.objtype_id=attr.object_type_id_fk
                left join obj_values as ov on o.id = ov.object_id_fk and attr.attr_id = ov.attr_id_fk 
                left join value_obj_link as vol on ov.value_id=vol.value_id_fk 
                where o.id=hv_valuei
                    and attr.name ='priority' limit 1;
         
            -- select priority name
            select vv.value into @priority_name from objects as o
                join object_types as ot on ot.objtype_id=o.object_type_id_fk
                join attributes as attr on ot.objtype_id=attr.object_type_id_fk
                left join obj_values as ov on o.id = ov.object_id_fk and attr.attr_id = ov.attr_id_fk 
                left join valuev as vv on ov.value_id=vv.value_id_fk 
                where o.id=@priority_obj_id 
                    and attr.name ='name' limit 1;

            if hv_timestamp is not null then
             
                if @alert_type_name is not null then
                    insert into historical_values(value_id_fk, timestamp, valuev, group_id)
                    values(@name_valueId, hv_timestamp, @alert_type_name, hv_group_id);
                else
                    insert into historical_values(value_id_fk, timestamp, valuev, group_id)
                    values(@name_valueId, hv_timestamp, 'UNKNOWN', hv_group_id);
                end if;
                
                if @alert_type_descr is not null then
                    if char_length(@alert_type_descr) != 0 then
                        insert into historical_values(value_id_fk, timestamp, valuev, group_id)
                        values(@descr_valueId, hv_timestamp, @alert_type_descr, hv_group_id);
                    end if;
                else
                    insert into historical_values(value_id_fk, timestamp, valuev, group_id)
                    values(@descr_valueId, hv_timestamp, 'Generic alert, any alert with non-existing type is saved as UNKNOWN', hv_group_id);
                end if;
                
                if @priority_name is not null then
                    insert into historical_values(value_id_fk, timestamp, valuev, group_id)
                    values(@priority_valueId, hv_timestamp, @priority_name, hv_group_id);
                else
                    insert into historical_values(value_id_fk, timestamp, valuev, group_id)
                    values(@priority_valueId, hv_timestamp, 'MAJOR', hv_group_id);
                end if;
            end if;
            
            end loop loop2;
        
        end history;

    end loop main;
    
    -- delete the 'type' attribute from the ALERT_HISTORY type
    
    DELETE FROM `synap`.`attributes`
    WHERE name = 'type' and object_type_id_fk =(select objtype_id from object_types where name = 'ALERT_HISTORY');
            
end;|

delimiter ;

-- execute procedure

start transaction;
  call alert_update();
        
-- renew ids after update
SET @max_object_id := IFNULL((SELECT max(`id`)+1 FROM `objects`),1);
SET @max_value_id := IFNULL((SELECT max(`value_id`)+1 FROM `obj_values`),1);
SET @max_valued_id := IFNULL((SELECT max(`id`)+1 FROM `valued`),1);
SET @max_valuel_id := IFNULL((SELECT max(`id`)+1 FROM `valuel`),1);
SET @max_valuei_id := IFNULL((SELECT max(`id`)+1 FROM `valuei`),1);
SET @max_valuev_id := IFNULL((SELECT max(`id`)+1 FROM `valuev`),1);
SET @max_valuelink_id := IFNULL((SELECT max(`id`)+1 FROM `value_obj_link`),1);
SET @max_valueplink_id := IFNULL((SELECT max(`id`)+1 FROM `value_property_link`),1);
SET @max_tag_value_id := IFNULL((SELECT max(`value_id`)+1 FROM `tag_values`),1);
SET @max_group_value_id := IFNULL((SELECT max(`group_id`)+1 FROM `historical_values`),1);


delete from uid_table;
INSERT INTO `uid_table` (`object_id`,`value_id`,`valued_id`,`valuev_id`,`valuei_id`,`valuel_id`,`valuelink_id`,`valueplink_id`, `tag_value_id`, `group_id`) values (@max_object_id,@max_value_id,@max_valued_id,@max_valuev_id,@max_valuei_id,@max_valuel_id,@max_valuelink_id,@max_valueplink_id,@max_tag_value_id,@max_group_value_id);

commit;

drop procedure if exists alert_update;
DROP FUNCTION IF EXISTS insert_object_value;
