use `synap`;

set autocommit = 0;

start transaction;

-- moving DC image files into database 
INSERT INTO value_binary (value_id_fk, value)
select ov.value_id, load_file(vv.value) from valuev vv , obj_values ov, object_types ot , attributes a
where ot.objtype_id = a.object_type_id_fk
and ov.attr_id_fk = a.attr_id
and vv.value_id_fk = ov.value_id
and ot.name = 'DC'
and a.name = 'image';

delete vv from valuev vv , obj_values ov, object_types ot , attributes a
where ot.objtype_id = a.object_type_id_fk
and ov.attr_id_fk = a.attr_id
and vv.value_id_fk = ov.value_id
and ot.name = 'DC'
and a.name = 'image';

-- moving ZONE image files into database
INSERT INTO value_binary (value_id_fk, value)
select ov.value_id, load_file(vv.value) from valuev vv , obj_values ov, object_types ot , attributes a
where ot.objtype_id = a.object_type_id_fk
and ov.attr_id_fk = a.attr_id
and vv.value_id_fk = ov.value_id
and ot.name in('ZONE')
and a.name = 'map';

delete vv from valuev vv , obj_values ov, object_types ot , attributes a
where ot.objtype_id = a.object_type_id_fk
and ov.attr_id_fk = a.attr_id
and vv.value_id_fk = ov.value_id
and ot.name in('ZONE','SET')
and a.name = 'map';

delete ov from obj_values ov , attributes a , object_types ot
where ot.objtype_id = a.object_type_id_fk
and ov.attr_id_fk = a.attr_id
and ot.name = 'SET'
and a.name = 'map';

delete from tbl_rule_property_src_assoc where attr_id_fk = 
(select a.attr_id from attributes a, object_types ot
where ot.objtype_id = a.object_type_id_fk
and ot.name = 'SET'
and a.name = 'map'
);

delete from tbl_rule_property_dest_assoc where attr_id_fk = 
(select a.attr_id from attributes a, object_types ot
where ot.objtype_id = a.object_type_id_fk
and ot.name = 'SET'
and a.name = 'map'
);

delete a from attributes a, object_types ot
where ot.objtype_id = a.object_type_id_fk
and ot.name = 'SET'
and a.name = 'map';

update attributes a , object_types ot set a.attr_type_id_fk = 8
where ot.objtype_id = a.object_type_id_fk
and ot.name = 'DC'
and a.name = 'image';

update attributes a , object_types ot set a.attr_type_id_fk = 8
where ot.objtype_id = a.object_type_id_fk
and ot.name = 'ZONE'
and a.name = 'map';


commit;

