use synap;

drop procedure if exists updateArchiveEventStartTime;

delimiter |
create procedure updateArchiveEventStartTime()
begin
-- select datetime when event should starts
    SELECT distinct timestamp(DATE(from_days(p.partition_description+1)),'01:00') into @lastPartDateTime 
    FROM information_schema.PARTITIONS P 
    where p.table_schema = 'synap' 
    and p.table_name ='historical_values' 
    and p.partition_ordinal_position = 
    (select distinct(min(P_sub1.partition_ordinal_position)) 
    from (select distinct p_sub.partition_ordinal_position 
            from information_schema.PARTITIONS P_sub 
            where P_sub.table_schema = 'synap' and P_sub.table_name = 'historical_values' 
            order by partition_ordinal_position desc limit 2) as p_sub1) limit 1;

-- select event's original start datetime     
    select starts into @eventStartDateTime 
            from INFORMATION_SCHEMA.EVENTS 
            where EVENT_SCHEMA='synap' and EVENT_NAME='archive_data'; 

-- compare datetimes. if not equals update event's start datetime
    if @eventStartDateTime!=@lastPartDateTime then 
        ALTER EVENT `archive_data`
        ON SCHEDULE EVERY 1 DAY
        STARTS @lastPartDateTime;
    end if;
end|
delimiter ;

call updateArchiveEventStartTime();
drop procedure if exists updateArchiveEventStartTime;
