-- find 'version' property of ROOT object and update it.
-- version number should be changed to the public ES version before each GA
-- at the point when MapSense team is ready for it.
SET @type_id := (SELECT `objtype_id` FROM `object_types` WHERE `name`="ROOT");
SET @attr_id := (SELECT `attr_id` FROM `attributes` WHERE `object_type_id_fk`=@type_id AND `name`="version");
SET @obj_id := (SELECT `id` FROM `objects` WHERE `object_type_id_fk` = @type_id AND `static`=0);
SET @val_id := (SELECT `value_id` FROM `obj_values` WHERE `attr_id_fk`=@attr_id AND `object_id_fk`=@obj_id);
start transaction;
UPDATE `obj_values` SET `timestamp` = unix_timestamp()*1000 WHERE `value_id`=@val_id;
UPDATE `valuev` SET `value`="7.0" WHERE `value_id_fk`=@val_id;
commit;
