﻿-- configuration of rule types and instances - status checker which calculates status of all objects
use synap;

DROP PROCEDURE IF EXISTS createStatusCheckerRule;

delimiter//

CREATE PROCEDURE createStatusCheckerRule()
BEGIN
   DECLARE ruleTypeID INT;
   DECLARE ruleExistingFlag INT;


   select count(1) into ruleExistingFlag from `tbl_rule_types` where name = "com.synapsense.rulesengine.core.script.metric.StatusChecker";

  if (ruleExistingFlag=0) then
     insert into `tbl_rule_types` (`name`,`source_type`,`source`,`dsl`,`precompiled`,`type`) values ("com.synapsense.rulesengine.core.script.metric.StatusChecker","Java",null,null,1,1);

     select rule_type_id INTO ruleTypeID from `tbl_rule_types` where rule_type_id = max(rule_type_id);

     insert into `tbl_rule_instances` (`name`,`propagating`,`rule_type_id_fk`) values ("Rule.StatusChecker",0,ruleTypeID);
  end if;

END//
delimiter ;

call createStatusCheckerRule;

DROP PROCEDURE createStatusCheckerRule;
