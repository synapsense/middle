use `synap`;

DROP PROCEDURE IF EXISTS `GET_PARTITION_INFO`;

delimiter //
CREATE PROCEDURE GET_PARTITION_INFO(IN schemaName VARCHAR(50), IN tableName VARCHAR(50), OUT timepoint DATE, OUT partitionName VARCHAR(3))
begin
DECLARE lastPartitionName VARCHAR(3);

DECLARE oldDateTimepoint DATE;

SELECT distinct from_days(p.partition_description) ,p.partition_name
into timepoint,partitionName
FROM information_schema.PARTITIONS P
where p.table_schema = schemaName
and p.table_name =tableName
and p.partition_ordinal_position =
(select distinct(min(P_sub.partition_ordinal_position))
  from information_schema.PARTITIONS P_sub where P_sub.table_schema = schemaName
  and P_sub.table_name =tableName
);

end//
delimiter ;


DROP PROCEDURE IF EXISTS `GET_MAX_RANGE_PARTITION_INFO`;

delimiter //
CREATE PROCEDURE GET_MAX_RANGE_PARTITION_INFO(IN schemaName VARCHAR(50), IN tableName VARCHAR(50), OUT timepoint DATE, OUT partitionName VARCHAR(3))
begin
DECLARE lastPartitionName VARCHAR(3);

DECLARE oldDateTimepoint DATE;

SELECT distinct from_days(p.partition_description) ,p.partition_name
into timepoint,partitionName
FROM information_schema.PARTITIONS P
where p.table_schema = schemaName
and p.table_name =tableName
and p.partition_ordinal_position =
(select distinct(MAX(P_sub.partition_ordinal_position))
  from information_schema.PARTITIONS P_sub where P_sub.table_schema = schemaName
  and P_sub.table_name =tableName
);

end//
delimiter ;


DROP FUNCTION IF EXISTS `GET_PARTITION_UPPER_RANGE_BORDER`;

delimiter //
CREATE FUNCTION `GET_PARTITION_UPPER_RANGE_BORDER` (schemaName VARCHAR(50),tableName VARCHAR(50))  RETURNS INT
begin
DECLARE result INT;

-- retrieves the upper range border of the new partition
--   1.gets partition range upper border
--   2.add 1 month interval to it
--   3.converts the date back to days count
/*SELECT distinct to_days(date_add(from_days(p.partition_description), INTERVAL 1 month))
into result
FROM information_schema.PARTITIONS P
where p.table_schema = schemaName
and p.table_name =tableName
and p.partition_ordinal_position =
(select distinct(max(P_sub.partition_ordinal_position))
  from information_schema.PARTITIONS P_sub where P_sub.table_schema = schemaName
  and P_sub.table_name =tableName
);*/

SELECT distinct to_days(date_add(from_days(p.partition_description), INTERVAL 1 month))
into result
FROM information_schema.PARTITIONS P
where p.table_schema = schemaName
and p.table_name =tableName
and p.partition_ordinal_position =
(select distinct(min(P_sub1.partition_ordinal_position))
  from (select distinct p_sub.partition_ordinal_position from
  information_schema.PARTITIONS P_sub where P_sub.table_schema = schemaName
  and P_sub.table_name = tableName order by partition_ordinal_position desc limit 2) as p_sub1
);

return result;
end//
delimiter ;

DROP PROCEDURE IF EXISTS `CREATE_PARTITION`;

delimiter //
CREATE PROCEDURE CREATE_PARTITION(IN schemaName VARCHAR(50), IN  tableName VARCHAR(50), IN partitionName VARCHAR(50),IN timepoint INT,IN maxRangePartitionName VARCHAR(50),IN maxRangeTimepoint INT)
begin
-- dynamic instruction to create new partition by reorginizing the partiotion with max range border into 2
set @preparedStmtCreate = concat('ALTER TABLE ',schemaName,'.',tableName,' REORGANIZE partition ' , maxRangePartitionName, ' INTO (partition ',partitionName, ' values LESS THAN (',timepoint,'), partition ',maxRangePartitionName,' values LESS THAN(',maxRangeTimepoint,'));');

PREPARE create_new_partition from @preparedStmtCreate;
EXECUTE create_new_partition;
DEALLOCATE PREPARE create_new_partition;
end//
delimiter ;

DROP PROCEDURE IF EXISTS `DROP_PARTITION`;

delimiter //
CREATE PROCEDURE DROP_PARTITION(IN schemaName VARCHAR(50), IN tableName VARCHAR(50), IN partitionName VARCHAR(50)) 
begin
-- dynamic instruction to drop partition with the specified name (last partiotion is no more necessary)
set @preparedStmtDrop = concat('ALTER TABLE ',schemaName,'.',tableName,' drop partition ',partitionName);
PREPARE drop_last_partition from @preparedStmtDrop;
EXECUTE drop_last_partition;
DEALLOCATE PREPARE drop_last_partition;
end//

delimiter ;

delimiter //

-- enables archivation event
-- can be executed even if db has some working data
ALTER EVENT `archive_data` DO
BEGIN
-- default schema veriable
DECLARE SCHEMA_NAME VARCHAR(10) default 'synap';
-- stores the name of the partition with lower range
DECLARE lastPartitionName VARCHAR(3);
-- the time point -  determines the sensor data that has to be archived
DECLARE oldDataTimePoint DATE;
-- the time point - upper border limit of the new partiotion range(in days)
DECLARE newDataTimePoint INT;

DECLARE maxRangePartitionName VARCHAR(3);
DECLARE maxRangePartitionDataTimePoint DATE;


CREATE TABLE IF NOT EXISTS `ARCHIVE_SENSOR_DATA` (
  `sensor_id` int(11) COMMENT 'The data request id that triggered this collection',
  `sensor_node_data` double(15,6) COMMENT 'Actual value of the measurement captured by the sensor node sensor.',
  `data_time_stamp` datetime COMMENT 'The timestamp when the data was collected by the sensor node'
)
ENGINE=ARCHIVE;

CREATE TABLE IF NOT EXISTS `ARCHIVE_HISTORICAL_VALUES` (
  `value_id_fk` int(11) NOT NULL,
  `timestamp` datetime NOT NULL,
  `valuei` int(11),
  `valued` double(15,3),
  `valuel` bigint(20),
  `valuev` varchar(2000)
)
ENGINE=ARCHIVE;

-- sensor data
-- retrieves date and partition name of the partition with last ordinal pos (means the partition with lower range border )
-- implements cycle buffer of partition names
call GET_PARTITION_INFO(SCHEMA_NAME,'tbl_sensor_data',oldDataTimePoint,lastPartitionName);

-- moves online sensor data from the last partiotion(6 months old data) to the archive
INSERT INTO ARCHIVE_SENSOR_DATA ( `sensor_id`, `sensor_node_data`, `data_time_stamp`)
(select `sensor_id`, `sensor_node_data`, `data_time_stamp`
   from `tbl_sensor_data`
   where `data_time_stamp` < oldDataTimePoint);

call DROP_PARTITION(SCHEMA_NAME,'tbl_sensor_data',lastPartitionName);

-- retrieves the upper range border of the new partition
set newDataTimePoint = GET_PARTITION_UPPER_RANGE_BORDER(SCHEMA_NAME,'tbl_sensor_data');

-- retrieves info about partition with max range border
call GET_MAX_RANGE_PARTITION_INFO(SCHEMA_NAME,'tbl_sensor_data',maxRangePartitionDataTimePoint,maxRangePartitionName);

call CREATE_PARTITION(SCHEMA_NAME,'tbl_sensor_data',lastPartitionName,newDataTimePoint,maxRangePartitionName,TO_DAYS(maxRangePartitionDataTimePoint));

-- historical values
-- retrieves date and partition name of the partition with last ordinal pos (means the partition with lower range border )
-- implements cycle buffer of partition names
call GET_PARTITION_INFO(SCHEMA_NAME,'historical_values',oldDataTimePoint,lastPartitionName);

-- moves online sensor data from the last partiotion(6 months old data) to the archive
INSERT INTO ARCHIVE_HISTORICAL_VALUES (`value_id_fk`, `timestamp`,`valuei`,`valued`,`valuel`,`valuev`)
(select `value_id_fk`, `timestamp`,`valuei`,`valued`,`valuel`,`valuev`   from `historical_values`
   where `timestamp` < oldDataTimePoint);

call DROP_PARTITION(SCHEMA_NAME,'historical_values',lastPartitionName);

-- retrieves the upper range border of the new partition
set newDataTimePoint = GET_PARTITION_UPPER_RANGE_BORDER(SCHEMA_NAME,'historical_values');

-- retrieves info about partition with max range border
call GET_MAX_RANGE_PARTITION_INFO(SCHEMA_NAME,'historical_values',maxRangePartitionDataTimePoint,maxRangePartitionName);

call CREATE_PARTITION(SCHEMA_NAME,'historical_values',lastPartitionName,newDataTimePoint,maxRangePartitionName,TO_DAYS(maxRangePartitionDataTimePoint));

END//
delimiter ;