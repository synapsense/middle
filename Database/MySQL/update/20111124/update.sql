USE synap;



DROP PROCEDURE IF EXISTS tmp_custom_query_cleaner;    




DELIMITER $$



    



CREATE PROCEDURE tmp_custom_query_cleaner(propertyName VARCHAR(255))



    



    BEGIN



    DECLARE startIdx INT;



    



    DECLARE valId INT;



    DECLARE linesValId INT;



    DECLARE newAssociatedSensors TEXT;



    DECLARE newLines TEXT;



        



    DECLARE subVal TEXT;



    DECLARE previousDelimIdx INT;



    DECLARE nextDelimIdx INT;



    DECLARE objectID INT;



    



    DECLARE id INT;



    DECLARE substrBeforeOccurrence TEXT;



    DECLARE ordinalNumOfOccurence INT;







    DECLARE done INT;



     



     DECLARE



          cur_associated CURSOR FOR select val_id, object_id, val



            FROM all_objects_props_values



            where



            propname = 'associatedSensors'



            and val regexp propertyName;







    DECLARE



          cur_lines CURSOR FOR select val_id, val



            FROM all_objects_props_values



            where



            propname = 'lines'



            and object_id = id;



            



    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;



    



    open cur_associated;



    



    main_loop:



        LOOP



        



          FETCH cur_associated INTO valId, objectID, newAssociatedSensors;



          IF done



          THEN



            LEAVE main_loop;



          END IF;               



          -- bound parameter of cur_lines



          SET id = objectID;           



          OPEN cur_lines;



             FETCH cur_lines INTO linesValId, newLines;



          CLOSE cur_lines;



          IF done



          THEN



             SET newLines = '';



          END IF;



            



          overcooling_loop: LOOP



            SET startIdx = locate(propertyName, newAssociatedSensors);



            IF (startIdx = 0) THEN 



                LEAVE overcooling_loop;



            END IF;    



            SET subVal = substr(newAssociatedSensors,1, startIdx);



            SET nextDelimIdx = locate(',', newAssociatedSensors,startIdx);

            

            SET previousDelimIdx = length(subVal) - locate(',', reverse(subVal));  -- locate can return 0


          -- if overcooling elem is first


            IF previousDelimIdx = length(subVal) THEN 


            SET ordinalNumOfOccurence = 1;

            SET previousDelimIdx = 0;

            SET nextDelimIdx = nextDelimIdx + 1;


            ELSE 


            SET substrBeforeOccurrence = substring(newAssociatedSensors,1,previousDelimIdx);


            SET ordinalNumOfOccurence = LENGTH(substrBeforeOccurrence)-LENGTH(REPLACE(substrBeforeOccurrence, ',', '')) + 2;


            END IF;
    

            -- if overcooling is only value in associatedSensors


            IF nextDelimIdx=1 and previousDelimIdx = 0 THEN 



                SET newAssociatedSensors = '';



                SET newLines = '';    



                LEAVE overcooling_loop;



            END IF;           
    

            -- cut middle 



            SET newAssociatedSensors = concat(substring(newAssociatedSensors,1,previousDelimIdx),substring(newAssociatedSensors,nextDelimIdx));



            -- lines        



            -- overcooling occurence index in array



            SET subVal = substring_index(newLines,'],',ordinalNumOfOccurence);

 

            IF length(subVal) = length(newLines) THEN 

            SET previousDelimIdx = length(subVal) - locate('[',reverse(subVal)) - 1;

            SET nextDelimIdx = length(subVal);

           
           ELSE             
            

            SET previousDelimIdx = length(subVal) - locate('[',reverse(subVal));


            SET nextDelimIdx = length(subVal)+3;



            END IF;

            SET newLines = concat(substring(newLines,1,previousDelimIdx),substring(newLines,nextDelimIdx));                                    



        END LOOP overcooling_loop;



     



     -- update of associatedSensors



        update valuev set value = newAssociatedSensors where value_id_fk = valId;



        update valuev set value = newLines where value_id_fk = linesValId;



            



    END LOOP main_loop;



          



    close cur_associated;



    



    END$$



    



DELIMITER ;



CALL tmp_custom_query_cleaner('overcoolingThreshold');
CALL tmp_custom_query_cleaner('minDelta');
CALL tmp_custom_query_cleaner('medDelta');
CALL tmp_custom_query_cleaner('maxDelta');
CALL tmp_custom_query_cleaner('minRecorded');
CALL tmp_custom_query_cleaner('medRecorded');
CALL tmp_custom_query_cleaner('maxRecorded');
CALL tmp_custom_query_cleaner('minTarget');
CALL tmp_custom_query_cleaner('medTarget');
CALL tmp_custom_query_cleaner('maxTarget');



DROP PROCEDURE IF EXISTS tmp_custom_query_cleaner;    
