use synap;

select attr_id into @htop_id from attributes a, object_types ot where a.object_type_id_fk = ot.objtype_id and ot.name = 'RACK' and a.name = 'hTop';

insert into tbl_rule_property_src_assoc (object_id_fk, attr_id_fk, rule_id_fk, binding, link_type) 
select ri.object_id_fk, 
@htop_id,
ri.rule_id, 
'hTop', 
null 
from tbl_rule_types rt, tbl_rule_instances ri 
where rt.rule_type_id = ri.rule_type_id_fk 
and rt.name = 'com.synapsense.rulesengine.core.script.metric.RackBPA'
and not exists (
select 1 from tbl_rule_property_src_assoc src 
where src.rule_id_fk = ri.rule_id 
and src.object_id_fk = ri.object_id_fk 
and src.attr_id_fk = @htop_id
);