use `synap`;

CREATE TABLE IF NOT EXISTS `TBL_VISUALIZATION_LEGEND` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `server` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `dataclass` int(11) NOT NULL,
  `captures` int(11) NOT NULL,
  `last_updated` bigint(20) NOT NULL,
  PRIMARY KEY(`id`)
)
ENGINE=INNODB;

CREATE TABLE IF NOT EXISTS `TBL_VISUALIZATION_LEGEND_POINT` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `legend` int(11) NOT NULL DEFAULT '0',
  `position` double(15,3),
  `red` int(11),
  `green` int(11),
  `blue` int(11),
  PRIMARY KEY(`id`)
)
ENGINE=INNODB;

CREATE TABLE IF NOT EXISTS `TBL_VISUALIZATION_LEGEND_IMAGE` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `legend` int(11) NOT NULL DEFAULT '0',
  `us_resource_path` varchar(300) NOT NULL,
  `si_resource_path` varchar(300) NOT NULL,
  `show_time` timestamp NOT NULL,
  PRIMARY KEY(`id`)
)
ENGINE=INNODB;


ALTER TABLE `TBL_VISUALIZATION_LEGEND` ADD
  CONSTRAINT `legend_belongs_to_server` FOREIGN KEY (`server`)
    REFERENCES `TBL_VISUALIZATION_SERVERS`(`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION;

ALTER TABLE `TBL_VISUALIZATION_LEGEND_POINT` ADD
  CONSTRAINT `legendpoint_belongsto_legend` FOREIGN KEY (`legend`)
    REFERENCES `TBL_VISUALIZATION_LEGEND`(`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION;

ALTER TABLE `TBL_VISUALIZATION_LEGEND_IMAGE` ADD
  CONSTRAINT `legendimage_belongsto_legend` FOREIGN KEY (`legend`)
    REFERENCES `TBL_VISUALIZATION_LEGEND`(`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION;


-- liveimaging legend configuration
INSERT INTO `tbl_visualization_legend` (`id`, `server`, `dataclass`, `captures`, `last_updated`) values (1,1,200,5,0);
INSERT INTO `tbl_visualization_legend` (`id`, `server`, `dataclass`, `captures`, `last_updated`) values (2,1,201,5,0);
INSERT INTO `tbl_visualization_legend` (`id`, `server`, `dataclass`, `captures`, `last_updated`) values (3,1,202,5,0);

-- temperature
INSERT INTO `tbl_visualization_legend_point` (`id`, `legend`, `position`, `red`, `green`, `blue`) values (1,1,50,0,0,255);
INSERT INTO `tbl_visualization_legend_point` (`id`, `legend`, `position`, `red`, `green`, `blue`) values (2,1,57,0,150,255);
INSERT INTO `tbl_visualization_legend_point` (`id`, `legend`, `position`, `red`, `green`, `blue`) values (3,1,64.5,0,255,100);
INSERT INTO `tbl_visualization_legend_point` (`id`, `legend`, `position`, `red`, `green`, `blue`) values (4,1,66.5,0,255,50);
INSERT INTO `tbl_visualization_legend_point` (`id`, `legend`, `position`, `red`, `green`, `blue`) values (5,1,72.5,0,255,0);
INSERT INTO `tbl_visualization_legend_point` (`id`, `legend`, `position`, `red`, `green`, `blue`) values (6,1,76.5,50,255,0);
INSERT INTO `tbl_visualization_legend_point` (`id`, `legend`, `position`, `red`, `green`, `blue`) values (7,1,80.6,100,255,0);
INSERT INTO `tbl_visualization_legend_point` (`id`, `legend`, `position`, `red`, `green`, `blue`) values (8,1,90,230,210,0);
INSERT INTO `tbl_visualization_legend_point` (`id`, `legend`, `position`, `red`, `green`, `blue`) values (9,1,100,230,80,0);
INSERT INTO `tbl_visualization_legend_point` (`id`, `legend`, `position`, `red`, `green`, `blue`) values (10,1,110,255,25,0);
INSERT INTO `tbl_visualization_legend_point` (`id`, `legend`, `position`, `red`, `green`, `blue`) values (11,1,120,255,0,0);

-- humidity
INSERT INTO `tbl_visualization_legend_point` (`id`, `legend`, `position`, `red`, `green`, `blue`) values (12,2,20,230,111,0);
INSERT INTO `tbl_visualization_legend_point` (`id`, `legend`, `position`, `red`, `green`, `blue`) values (13,2,30,128,206,71);
INSERT INTO `tbl_visualization_legend_point` (`id`, `legend`, `position`, `red`, `green`, `blue`) values (14,2,35,100,255,0);
INSERT INTO `tbl_visualization_legend_point` (`id`, `legend`, `position`, `red`, `green`, `blue`) values (15,2,40,50,255,0);
INSERT INTO `tbl_visualization_legend_point` (`id`, `legend`, `position`, `red`, `green`, `blue`) values (16,2,45,0,255,0);
INSERT INTO `tbl_visualization_legend_point` (`id`, `legend`, `position`, `red`, `green`, `blue`) values (17,2,70,51,160,255);
INSERT INTO `tbl_visualization_legend_point` (`id`, `legend`, `position`, `red`, `green`, `blue`) values (18,2,80,51,51,255);

-- pressure
INSERT INTO `tbl_visualization_legend_point` (`id`, `legend`, `position`, `red`, `green`, `blue`) values (19,3,0,0,0,0);
INSERT INTO `tbl_visualization_legend_point` (`id`, `legend`, `position`, `red`, `green`, `blue`) values (20,3,0.08,0,255,0);
INSERT INTO `tbl_visualization_legend_point` (`id`, `legend`, `position`, `red`, `green`, `blue`) values (21,3,0.15,0,0,255);
