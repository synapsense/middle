﻿-- Bug#8861
-- The script adds a new property 'applicationType' to DASHBOARD object type if not exists
-- and sets it to 'WebConsole' for all existing DASHBOADs 

use synap;

DROP PROCEDURE IF EXISTS createAppTypeAttributeProc;
delimiter //

CREATE PROCEDURE createAppTypeAttributeProc()
BEGIN
   DECLARE attrTypeId INT DEFAULT NULL;
   DECLARE new_attr_id INT DEFAULT NULL;
   DECLARE new_value_id INT DEFAULT NULL;
   DECLARE obj_id INT DEFAULT NULL;
   DECLARE dashTypeId INT DEFAULT NULL;
   DECLARE done INT DEFAULT 0;

   -- declare cursor for "DASHBOARD" objects
   DECLARE curs CURSOR FOR select o.id from objects o, object_types ot where o.object_type_id_fk=ot.objtype_id and ot.name="DASHBOARD" and o.static = 0;
   DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

   select objtype_id INTO dashTypeId from object_types where name="DASHBOARD";
   select attr_type_id INTO attrTypeId from attributes_types where java_type="java.lang.String";

   IF dashTypeId IS NOT NULL THEN
       -- create "applicationType" attribute for "DASHBOARD"
       INSERT INTO attributes (name,historical, object_type_id_FK, attr_type_id_FK, collection, static) VALUES ('applicationType',0, dashTypeId, attrTypeId, 0, 0);
       -- get "applicationType" attribute Id
       select attr_id INTO new_attr_id from attributes where name="applicationType" and object_type_id_FK = dashTypeId;
   END IF;

   OPEN curs;
   main:
        loop
            fetch curs INTO obj_id;

            IF done
            THEN
                LEAVE main;
            END IF;

            INSERT INTO obj_values(timestamp, object_id_FK, attr_id_FK, version) VALUES (0, obj_id, new_attr_id, 0);

            select value_id INTO new_value_id from obj_values where object_id_FK = obj_id and attr_id_FK = new_attr_id;
            INSERT INTO valuev(value, value_id_FK) VALUES ("WebConsole", new_value_id);

        end loop;
    CLOSE curs;

END//
delimiter ;

call createAppTypeAttributeProc();

DROP PROCEDURE createAppTypeAttributeProc;