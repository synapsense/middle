use `synap`;

CREATE TABLE `TBL_NHT_INFO` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `num_neigh` int(11),
  `report_timestamp` datetime,
  `nids` varchar(10),
  `rssi` tinyblob,
  `state` tinyblob,
  `hop` tinyblob,
  `hoptm` tinyblob,
  PRIMARY KEY(`id`),
  INDEX `num_neigh_timestamp_idx`(`num_neigh`, `report_timestamp`)
)
ENGINE=INNODB;