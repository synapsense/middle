use synap;

alter table historical_values rename historical_values_old;

create table `historical_values` (
  `value_id_fk` int(11) not null,
  `timestamp` datetime not null,
  `valuei` int(11),
  `valued` double,
  `valuel` bigint(20),
  `valuev` varchar(21833),
  index `idx_value_id_timestamp`(`value_id_fk`, `timestamp`)
)
engine=innodb;

DROP PROCEDURE IF EXISTS migrate_historical_data;

DELIMITER |

CREATE PROCEDURE migrate_historical_data ()
BEGIN
 DECLARE done                       INT DEFAULT 0;     
 
 DECLARE cv_sensor_id               INT; 
 DECLARE cv_objvalue_id             INT;
 
 DECLARE v_date_start               DATE;
 DECLARE v_date_end                 DATE;
 
 DECLARE v_affected_rows            INT;
 DECLARE v_exec_time                TIME;
 DECLARE v_start                    TIME;
 DECLARE v_started_at               DATETIME;

 
 declare cur_sensors cursor for select temp_sensors.old_id , obj_values.value_id  
 from temp_sensors, obj_values
 where 
    obj_values.object_id_fk = temp_sensors.new_id
    and obj_values.attr_id_fk = (select attr_id
                                 from attributes, object_types
                                 where attributes.object_type_id_fk = object_types.objtype_id
                                 and   object_types.name = 'WSNSENSOR'
                                 and   attributes.name = 'lastValue')
  order by temp_sensors.old_id;

 DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

 -- creates data transfer log to save date interval at checkpoints
-- process = 1 for sensor data transsfer, and 2 - for historical_values to reorganized table transfer
 create table if not exists temp_historical_transfer_log (id int auto_increment, process int, started datetime, run_date date, exec_time_sec time, rows_affected int, primary key (id));
 
 -- determine date interval to load
 select run_date ,DATE_SUB(run_date,interval 1 day) into v_date_end , v_date_start from temp_historical_transfer_log 
 where id = (select max(id) from temp_historical_transfer_log); 
 if done then 
    set v_date_start  =  CURDATE();
    set v_date_end    =  DATE_ADD(CURDATE(), INTERVAL 1 day);
 end if;

 set v_started_at = NOW();
 set v_start = CURTIME();
 set v_affected_rows = 0;
 
 SET FOREIGN_KEY_CHECKS=0;
  
 -- loops all sensors
 OPEN cur_sensors;
 BEGIN
    DECLARE EXIT HANDLER FOR NOT FOUND BEGIN END;  
    LOOP
       FETCH cur_sensors INTO cv_sensor_id,cv_objvalue_id;

       insert into historical_values(value_id_fk, timestamp, valued)
         select cv_objvalue_id, sd.data_time_stamp, sd.sensor_node_data
          from tbl_sensor_data sd
          where sd.sensor_id = cv_sensor_id and sd.data_time_stamp between v_date_start and v_date_end;
      
       set v_affected_rows = v_affected_rows + ROW_COUNT();
             
     END LOOP;
 END;
 CLOSE cur_sensors;

 SET FOREIGN_KEY_CHECKS=1;

 insert into temp_historical_transfer_log(process, started,run_date,exec_time_sec,rows_affected) values (1,v_started_at,v_date_start,SUBTIME(CURTIME(),v_start),v_affected_rows);

 insert into historical_values(value_id_fk, timestamp, valued, valuev, valuel, valuei) 
  select value_id_fk, timestamp, valued, valuev, valuel, valuei from historical_values_old where timestamp between v_date_start and v_date_end;

 insert into temp_historical_transfer_log(process, started,run_date,exec_time_sec,rows_affected) values (2,v_started_at,v_date_start,SUBTIME(CURTIME(),v_start),ROW_COUNT());

end; |

delimiter ;

-- Drop Event
drop event if exists `migrate_historical_data_event`;

delimiter |
create event `migrate_historical_data_event`
  on schedule every '20' minute starts NOW() ends NOW() + interval 30 day
  enable
do
begin
  -- ? when to stop this ? 
  -- calculation : 
  --  7K senssor 18 months = 1Billion records
 call migrate_historical_data();
-- how to delete tables tbl_sensor_data , historical_values_old?
-- additional update script will be required to remove old tables  
end; |
delimiter ;

-- new double , floating point 
alter table valued modify value double;
alter table tbl_visualization_legend_point modify position double;
alter table archive_historical_values modify valued DOUBLE;

