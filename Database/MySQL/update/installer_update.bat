@echo off
set CURRENT_VERSION=7.0
set logfile="%APPDATA%\SynapSense_%CURRENT_VERSION%_db_upgrade.log"
del %logfile%
set "log=call :log "
%log% "*************************************"

%log% "Reading input arguments..."
set mysqldir=%1
%log% "MySQL folder=%mysqldir%"
set mysqluser=%2
%log% "MySQL user=%mysqluser%"
set password=%3
%log% "password=%password%"

set db_schema=synap 
set mysqlbin=%mysqldir%bin

rem run mysql_upgrade

pushd %mysqlbin%
	%log% "Upgrading database..."
	mysql_upgrade.exe -p%password% >>%logfile%
	%log% "Stopping database..."
	net stop "SynapSense DB"
	%log% "Starting database..."
	net start "SynapSense DB"
popd

call:get_es_version es_ver
if %es_ver% EQU undefined  (
	%log% "The selected ES data base version cannot be determined. The installation will be cancelled."
	call:msg "The selected ES data base version cannot be determined. The installation will be cancelled."
	exit 1
)
if %es_ver% EQU %CURRENT_VERSION% (
	%log% "Your existing ES data base version %es_ver% is up to date."
	rem call:msg "Your existing ES data base version %es_ver% is up to date."
	exit 0
 )
if %es_ver% EQU 6.8.1 (
	%log% "Your existing ES data base version %es_ver% will be updated to %CURRENT_VERSION%."
	call:run_update 7.0
	call:run_version_update
	exit 0
)
if %es_ver% EQU 6.8 (
	%log% "Your existing ES data base version %es_ver% will be updated to %CURRENT_VERSION%."
	call:run_update_681
	call:run_update 7.0
	call:run_version_update
	exit 0
 )
if %es_ver% EQU 6.7 (
	%log% "Your existing ES data base version %es_ver% will be updated to %CURRENT_VERSION%."
	call:run_update 6.8
	call:run_update_681
	call:run_update 7.0
	call:run_version_update
	exit 0
 )
if %es_ver% EQU 6.6 (
	%log% "Your existing ES data base version %es_ver% will be updated to %CURRENT_VERSION%."
	call:run_update 6.7
	call:run_update 6.8
	call:run_update_681
	call:run_update 7.0
	call:run_version_update
	exit 0
 )
if %es_ver% EQU 6.5 (
	%log% "Your existing ES data base version %es_ver% will be updated to %CURRENT_VERSION%." 
    rem [BC 01/2015] According to the log, 6.5.txt was introduced in 6.6 and should have been called 6.6.txt
	call:run_update 6.5
	call:run_update 6.7
	call:run_update 6.8
	call:run_update_681
	call:run_update 7.0
	call:run_version_update
	exit 0
 )

if %es_ver% EQU 6.4 (
	%log% "Your existing ES data base version %es_ver% will be updated to %CURRENT_VERSION%." 
	call:run_update 6.4
	call:run_update 6.5
	call:run_update 6.7
	call:run_update 6.8
	call:run_update_681
	call:run_update 7.0
	call:run_version_update
	exit 0
 ) 
 
else (
	%log% "Customer database currently at an unsupported version: %es_ver%. Please update the database to 6.4, then rerun the installer."
	call:msg "Customer database currently at an unsupported version: %es_ver%. Please update the database to 6.4, then rerun the installer."
	exit 1
 )
 
:run_update
pushd %mysqldir%
for /f %%a in (scripts\update\%~1.txt) do ( 
     if not exist scripts\update\%%a ( 
     %log% "%%a file is missing. Database upgrade failed!"
	 call:msg "%%a file is missing. Database upgrade failed!"
     exit 1
    )
 )
type nul > blank
set has_error="0"
for /f %%a in (scripts\update\%~1.txt) do ( 
	%log% "*************************************"
	%log% "Executing upgrade script %%a"	
	
    bin\mysql.exe --force --user=%mysqluser% -p%password% %db_schema%< scripts\update\%%a  1>>%logfile% 2>error_output
	
	fc error_output blank > nul
	if errorlevel 1 (
		set has_error="1"
		type error_output >> %logfile%
	)
 )
del blank
del error_output

if %has_error% EQU "1" (
	%log% "There were errors during upgrade scripts execution. See log for details."
	call:msg "There were errors during upgrade scripts execution. See log for details."
)
popd
exit /b 
 
:run_update_681
rem Remove CSV file from previous run (otherwise the CSV export would fail)
pushd %mysqldir%
if exist data\synap\historical_values.csv (
  del data\synap\historical_values.csv
)
popd

rem Now run the update scripts
call:run_update 6.8.1

rem Reclaim disk space
pushd %mysqldir%
if exist data\synap\historical_values.csv (
  del data\synap\historical_values.csv
)
popd

exit /b

:run_version_update
pushd %mysqldir%
rem Call ES version update script
%log% "Updating ES version..."
bin\mysql.exe --user=%mysqluser% -p%password% %db_schema%< scripts\update\update_es_version.sql 2>>%logfile% 1>&2

rem Call uid_table(id counters storage) update script
%log% "Updating uid_table..."
bin\mysql.exe --user=%mysqluser% -p%password% %db_schema%< scripts\update\update_uid.sql 2>>%logfile% 1>&2

rem Removing temp file
if exist date.txt ( 
  del date.txt
)
%log% "Done running update scripts."
popd
exit /b 

rem function to get current ES version from database ROOT.version
:get_es_version
	%log% "Getting current ES version from database..."
	setlocal enableextensions 
	set "CURRENT_ES_VER=undefined"
	pushd %mysqlbin%
	%log% "Connecting to database..."
    mysql.exe --user=%mysqluser% -p%password% -e"SELECT 1" 2>>%logfile% 1>nul
	for /f "tokens=*" %%a in (
		'mysql.exe --user=%mysqluser% -p%password% %db_schema% -e"select v.value as '' from object_types ot, objects o, attributes a, obj_values ov, valuev v where ot.objtype_id=o.object_type_id_fk and ov.object_id_fk = o.id and ov.attr_id_fk = a.attr_id and ov.value_id = v.value_id_fk and ot.name = 'ROOT' and a.name = 'version' and o.static = 0"'
	) do ( 
		set "CURRENT_ES_VER=%%a"
	) 
	(endlocal
		set "%~1=%CURRENT_ES_VER%"
	)	
	popd
exit /b

:msg
	>usermessage.vbs echo MsgBox("%~1")
	wscript.exe usermessage.vbs
	del usermessage.vbs
exit /b

:log
echo %~1 
echo %date% %time% - %~1 >>%logfile%
EXIT /B 0
