@echo off
rem DATABASE validation before starting migrate procedure
echo Validating database consistency...

set mysqluser=%1
set password=%2
set db_schema=%3 

SET DB_ERROR_MESSAGE=Error executing mysql command line

mysql -u%mysqluser% -p%password% %db_schema% -e "delete r1 from tbl_nwk_route r1 ,tbl_nwk_route r2 where r1.route_id <> r2.route_id and r1.node_physical_id = r2.node_physical_id" 1> nul 2>&1
if ERRORLEVEL 1 goto mysql_error

mysql -u%mysqluser% -p%password% %db_schema% -e "select node_physical_id as mac, count(node_physical_id) number from tbl_sensor_node group by 1 having count(node_physical_id) > 1" > duplicate_nodes.txt
if ERRORLEVEL 1 goto mysql_error

mysql -u%mysqluser% -p%password% %db_schema% -e "select val as mac , count(val) as number from all_objects_props_values WHERE type_name ='GATEWAY' AND propname = 'uniqueId' group by 1 having count(val)>1" > duplicate_gateways.txt
if ERRORLEVEL 1 goto mysql_error

mysql -u%mysqluser% -p%password% %db_schema% -e "SELECT gtw.val as mac , concat(gtw.type_name,':',gtw.object_id) as gateway_oid , concat('NODE:',node.node_logical_id) as node_oid from all_objects_props_values gtw, tbl_sensor_node node WHERE gtw.type_name ='GATEWAY' AND gtw.propname = 'uniqueId' and node.node_physical_id = gtw.val" > shared_macs.txt
if ERRORLEVEL 1 goto mysql_error


for %%I in (duplicate_nodes.txt,duplicate_gateways.txt,shared_macs.txt) do (
 SET ERROR_STATUS=%%~zI
)
if not %ERROR_STATUS%==0 goto error_printing

goto proved

:error_printing
echo [Duplicate NODES]
type duplicate_nodes.txt

echo.

echo [Duplicate GATEWAYS]
type duplicate_gateways.txt

echo.

echo [Shared macs]
type shared_macs.txt

goto uniqueness_error

:mysql_error
echo MySQL error : %DB_ERROR_MESSAGE%
exit /B 1

:uniqueness_error
echo.
echo ***Database validation failed:
 echo Duplicate mac ids are found in database
 echo Some NODEs, GATAEWAYs share the same macid
 echo Check additional information listed above and correct object model before continue migration
 echo  Section [Duplicate NODES] contains enumeration of NODEs with the same mac id
 echo  Section [Duplicate GATEWAYS] contains enumeration of GATEWAYS with the same mac id
 echo  Section [Shared macs] contains enumeration of GATEWAYS and NODES which have shared mac ids
exit /B 2

:proved
echo Database is valid
rem echo Deleting temporary files
del shared_macs.txt
del duplicate_gateways.txt
del duplicate_nodes.txt
exit /B 0