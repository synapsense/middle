set mysqluser=%1
set password=%2
set db_schema=%3 

call validate %mysqluser% %password% %db_schema%
if ERRORLEVEL 1 exit /B %ERRORLEVEL%

rem Save database dump to rollback changes at any time
echo Saving database dump...
rem mysqldump -u%mysqluser% -p%password% %db_schema% > synap.dump

echo Creating auxilary database objects...
mysql -u%mysqluser% -p%password% %db_schema% < migrate_drop.sql
mysql -u%mysqluser% -p%password% %db_schema% < migrate_utils.sql
mysql -u%mysqluser% -p%password% %db_schema% < migrate_networks.sql
mysql -u%mysqluser% -p%password% %db_schema% < migrate_nodes.sql
mysql -u%mysqluser% -p%password% %db_schema% < migrate_sensors.sql
mysql -u%mysqluser% -p%password% %db_schema% < migrate_gateways.sql
mysql -u%mysqluser% -p%password% %db_schema% < migrate_battery_data.sql
mysql -u%mysqluser% -p%password% %db_schema% < migrate_statistic.sql
mysql -u%mysqluser% -p%password% %db_schema% < migrate_alerts.sql
mysql -u%mysqluser% -p%password% %db_schema% < migrate_rules.sql
mysql -u%mysqluser% -p%password% %db_schema% < migrate_schemachanges.sql

rem script which actually does all migration tasks, changes database state
echo Executing migration procedure...
mysql -u%mysqluser% -p%password% %db_schema% < migrate_all_run.sql
if ERRORLEVEL 1 goto error

goto success

:error
echo Error removing wsn. Fix problems and try again
set err_code=1
goto exit

:success 
echo WSN migration was successfully done
set err_code=0
goto exit

:exit
rem in any case aux database objects should be removed even if they weren't created
echo Cleaning up auxilary database objects...
mysql -u%mysqluser% -p%password% %db_schema% < migrate_drop.sql

echo Exiting WSN migration procedure
exit /B %err_code%
