DROP PROCEDURE IF EXISTS migrate_sensors;

DELIMITER | 
CREATE PROCEDURE migrate_sensors(in_newname VARCHAR(50))
  BEGIN
    DECLARE done                     INT DEFAULT 0;

    DECLARE lastTypeId               INT;
    DECLARE lastObjectId             INT;

    DECLARE cp_topology_id           INT;

    DECLARE namePropertyId           INT;
    DECLARE descPropertyId           INT;
    DECLARE channelPropertyId        INT;
    DECLARE statusPropertyId         INT;
    DECLARE minPropertyId            INT;
    DECLARE maxPropertyId            INT;
    DECLARE aminPropertyId           INT;
    DECLARE amaxPropertyId           INT;
    DECLARE rminPropertyId           INT;
    DECLARE rmaxPropertyId           INT;
    DECLARE xPropertyId              INT;
    DECLARE yPropertyId              INT;
    DECLARE zPropertyId              INT;
    DECLARE subSamplesPropertyId     INT;
    DECLARE deltaPropertyId          INT;
    DECLARE scaleMinPropertyId       INT;
    DECLARE scaleMaxPropertyId       INT;
    DECLARE typePropertyId           INT;
    DECLARE lastValuePropertyId      INT;
    DECLARE dataclassPropertyId      INT;
    DECLARE enabledPropertyId        INT;
    DECLARE smartSendPropertyId      INT;
    DECLARE modePropertyId           INT;
    DECLARE intervalPropertyId       INT;
 
    DECLARE lastObjectValueId        INT;
    DECLARE currentTime              LONG;

    DECLARE cv_old_id                INT;
    DECLARE cv_name                  VARCHAR(300);
    DECLARE cv_desc                  VARCHAR(300);
    DECLARE cv_channel               INT;
    DECLARE cv_status                INT;
    DECLARE cv_min                   DOUBLE;
    DECLARE cv_max                   DOUBLE;
    DECLARE cv_amin                  DOUBLE;
    DECLARE cv_amax                  DOUBLE;
    DECLARE cv_rmin                  DOUBLE;
    DECLARE cv_rmax                  DOUBLE;
    DECLARE cv_x                     DOUBLE;
    DECLARE cv_y                     DOUBLE;
    DECLARE cv_z                     DOUBLE;
    DECLARE cv_subsamples            INT;
    DECLARE cv_delta                 INT;
    DECLARE cv_scalemin              DOUBLE;
    DECLARE cv_scalemax              DOUBLE;
    DECLARE cv_type                  INTEGER;
    DECLARE cv_lastvalue             DOUBLE;
    DECLARE cv_lastvalue_timestamp   LONG;
    DECLARE cv_dataclass_id          INT;
    DECLARE cv_enabled               INT;
    DECLARE cv_smart_send_threshold  DOUBLE;
    DECLARE cv_mode                  VARCHAR(300);
    DECLARE cv_interval              INT;
 
    DECLARE cv_topology_id           INT;
    DECLARE cv_parent_id             INT;

    DECLARE
      cur_sensors CURSOR FOR
        SELECT sensor.sensor_id, sensor.name, dc.data_class_description, sensor.channel, sensor.status,
               ifnull(sensor.min,dc.data_class_minimum_value), ifnull(sensor.max,dc.data_class_maximum_value), 
               sensor.a_min, sensor.a_max, sensor.r_min, sensor.r_max, 
               sensor.visual_loc_x, sensor.visual_loc_y, sensor.visual_layer, sensor.subsamples,
               sensor.delta, sensor.scale_min, sensor.scale_max, sensor.type_id_fk, rdata.sensor_node_data,
               UNIX_TIMESTAMP(rdata.data_time_stamp) * 1000, topology.id, st.sensor_data_class_id,
               if (node.core_platform_id_fk=15 && sensor.channel>=3 && ((sensor.delta/100) & 0x8000) = 0 , 0 , 1) enabled,
               if (node.core_platform_id_fk in (11,18) && sensor.delta is not null,sensor.delta/100,0) smart_send_threshold,
               if (node.core_platform_id_fk=15 ,
                   if (sensor.channel in (6,8),
                     if ( (delta / 100) & 0x7fff = 0,'edge_detect','state_change_count'),
                   'averaging')
               ,'instant') mode,
               if (node.core_platform_id_fk=15 && sensor.channel in (6,8) , (delta / 100) & 0x7fff, 0) sinterval
        FROM tbl_sensor sensor
             JOIN tbl_sensor_type as st 
               ON (st.type_id=sensor.type_id_fk)
             JOIN tbl_data_class as dc 
               ON (st.sensor_data_class_id=dc.data_class_id)
             JOIN tbl_sensor_network_topology topology
               ON (sensor.sensor_id = topology.sensor_id_fk)
             JOIN tbl_sensor_node node 
               ON (topology.node_logical_id_fk = node.node_logical_id)
             LEFT OUTER JOIN tbl_realtime_data as rdata
               ON (sensor.sensor_id = rdata.sensor_id);
             
    DECLARE
      cur_sensor_relations CURSOR FOR
        SELECT object_id_fk
        FROM tbl_generic_wsn_assoc
        WHERE topology_id_fk = cp_topology_id;

    DECLARE
      cur_sensor_wsn_relations CURSOR FOR
        SELECT node_logical_id_fk
        FROM tbl_sensor_network_topology
        WHERE id = cp_topology_id;

    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

    INSERT INTO object_types(name)
    VALUES (in_newname);

    SET lastTypeId := last_insert_id();

    INSERT INTO objects(
                  name, object_type_id_fk, static)
    VALUES (in_newname, lastTypeId, 1);

    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('name', lastTypeId, 3);

    set namePropertyId = last_insert_id();

    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('desc', lastTypeId, 3);

    set descPropertyId = last_insert_id();

    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('channel', lastTypeId, 1);

    set channelPropertyId = last_insert_id();

    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('status', lastTypeId, 1);

    set statusPropertyId = last_insert_id();

    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('min', lastTypeId, 2);

    set minPropertyId = last_insert_id();

    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('max', lastTypeId, 2);

    set maxPropertyId = last_insert_id();

    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('aMin', lastTypeId, 2);

    set aminPropertyId = last_insert_id();

    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('aMax', lastTypeId, 2);

    set amaxPropertyId = last_insert_id();

    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('rMin', lastTypeId, 2);

    set rminPropertyId = last_insert_id();

    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('rMax', lastTypeId, 2);

    set rmaxPropertyId = last_insert_id();

    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('x', lastTypeId, 2);

    set xPropertyId = last_insert_id();

    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('y', lastTypeId, 2);

    set yPropertyId = last_insert_id();

    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('z', lastTypeId, 1);

    set zPropertyId = last_insert_id();

    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('subSamples', lastTypeId, 1);

    set subSamplesPropertyId = last_insert_id();

    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('delta', lastTypeId, 1);

    set deltaPropertyId = last_insert_id();

    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('scaleMin', lastTypeId, 2);

    set scaleMinPropertyId = last_insert_id();

    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('scaleMax', lastTypeId, 2);

    set scaleMaxPropertyId = last_insert_id();

    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('type', lastTypeId, 1);

    set typePropertyId = last_insert_id();

    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk, historical)
    values ('lastValue', lastTypeId, 2, 1);

    set lastValuePropertyId = last_insert_id();

    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('dataclass', lastTypeId, 1);

    set dataclassPropertyId = last_insert_id();
    
    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('enabled', lastTypeId, 1);

    set enabledPropertyId = last_insert_id();
    
    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('smartSendThreshold', lastTypeId, 2);

    set smartSendPropertyId = last_insert_id();
    
    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('mode', lastTypeId, 3);

    set modePropertyId = last_insert_id();
    
    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('interval', lastTypeId, 1);

    set intervalPropertyId = last_insert_id();      

    SET currentTime = UNIX_TIMESTAMP() * 1000;

    OPEN cur_sensors;

   main:
    LOOP
      FETCH cur_sensors
      INTO cv_old_id, cv_name, cv_desc, cv_channel, cv_status,
           cv_min, cv_max, cv_amin, cv_amax, cv_rmin,
           cv_rmax, cv_x, cv_y, cv_z, cv_subsamples,
           cv_delta, cv_scalemin, cv_scalemax, cv_type, cv_lastvalue,
           cv_lastvalue_timestamp, cv_topology_id, cv_dataclass_id, cv_enabled,
           cv_smart_send_threshold, cv_mode , cv_interval;

      IF done
      THEN
        LEAVE main;
      END IF;

      INSERT INTO objects(object_type_id_fk)
      VALUES (lastTypeId);

      SET lastObjectId = last_insert_id();

      INSERT INTO TEMP_SENSORS
      VALUES (cv_old_id, lastObjectId, cv_topology_id);

      CALL insert_varchar_value(
             lastObjectId, namePropertyId, currentTime, cv_name);
      CALL insert_varchar_value(
             lastObjectId, descPropertyId, currentTime, cv_desc);
      CALL insert_int_value(
             lastObjectId, channelPropertyId, currentTime, cv_channel);
      CALL insert_int_value(
             lastObjectId, statusPropertyId, currentTime, cv_status);      
      CALL insert_double_value(
             lastObjectId, minPropertyId, currentTime, cv_min);
      CALL insert_double_value(
             lastObjectId, maxPropertyId, currentTime, cv_max);
      CALL insert_double_value(
             lastObjectId, aminPropertyId, currentTime, cv_amin);
      CALL insert_double_value(
             lastObjectId, amaxPropertyId, currentTime, cv_amax);
      CALL insert_double_value(
             lastObjectId, rminPropertyId, currentTime, cv_rmin);
      CALL insert_double_value(
             lastObjectId, rmaxPropertyId, currentTime, cv_rmax);
      CALL insert_double_value(
             lastObjectId, xPropertyId, currentTime, cv_x);
      CALL insert_double_value(
             lastObjectId, yPropertyId, currentTime, cv_y);
      CALL insert_int_value(
             lastObjectId, zPropertyId, currentTime, cv_z);
      CALL insert_int_value(
             lastObjectId, subSamplesPropertyId, currentTime, cv_subsamples);
      CALL insert_int_value(
             lastObjectId, deltaPropertyId, currentTime, cv_delta);
      CALL insert_double_value(
             lastObjectId, scaleMinPropertyId, currentTime, cv_scalemin);
      CALL insert_double_value(
             lastObjectId, scaleMaxPropertyId, currentTime, cv_scalemax);
      CALL insert_double_value(
             lastObjectId, lastValuePropertyId, cv_lastvalue_timestamp, cv_lastvalue);
      CALL insert_int_value(
             lastObjectId, dataclassPropertyId, currentTime, cv_dataclass_id);               
      call insert_int_value(
             lastObjectId, typePropertyId, currentTime, cv_type);
      call insert_int_value(
             lastObjectId, enabledPropertyId, currentTime, cv_enabled);
      call insert_double_value(
             lastObjectId, smartSendPropertyId, currentTime, cv_smart_send_threshold);
      call insert_varchar_value(
             lastObjectId, modePropertyId, currentTime, cv_mode);
      call insert_int_value(
             lastObjectId, intervalPropertyId, currentTime, cv_interval);
             
      IF cv_topology_id IS NOT NULL
      THEN
        set cp_topology_id = cv_topology_id;

        OPEN cur_sensor_relations;

       -- moving generic relations
       subloop:
        LOOP
          FETCH cur_sensor_relations INTO cv_parent_id;

          IF done
          THEN
            set done = 0;
            LEAVE subloop;
          END IF;

          -- new relation
          INSERT INTO object_parents(
                        parent_id_fk, object_id_fk)
          VALUES (cv_parent_id, lastObjectId);
        END LOOP;

        CLOSE cur_sensor_relations;

        OPEN cur_sensor_wsn_relations;

       -- moving wsn relations
       subloop_wsn:
        LOOP
          FETCH cur_sensor_wsn_relations INTO cv_parent_id;

          IF done
          THEN
            set done = 0;
            LEAVE subloop_wsn;
          END IF;

          set cv_parent_id =
                get_wsn_newid(
                  'NODE', cv_parent_id);

          -- relation between new sensor and new node
          INSERT INTO object_parents(
                        parent_id_fk, object_id_fk)
          VALUES (cv_parent_id, lastObjectId);
        END LOOP;

        CLOSE cur_sensor_wsn_relations;
      END IF;
    END LOOP;

    CLOSE cur_sensors;
    
  update value_obj_link l , temp_sensors temp
  set l.object_id = temp.new_id, l.object_type = in_newname
  where l.object_id = temp.old_id
  and l.object_type = 'SENSOR';
  
  END|
DELIMITER ;