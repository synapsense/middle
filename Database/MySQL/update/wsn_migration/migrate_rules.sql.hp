DROP PROCEDURE IF EXISTS migrate_rules;

DELIMITER |

CREATE PROCEDURE migrate_rules(in in_es_version varchar(50))
MAIN:
BEGIN
 DECLARE done                       INT DEFAULT 0;  
 DECLARE cv_rule_id                 INT;
 DECLARE cv_source                  MEDIUMTEXT;
 DECLARE cv_old_link                VARCHAR(30);
 DECLARE cv_new_link                VARCHAR(30);
  
 DECLARE v_temp_source              MEDIUMTEXT;
 DECLARE v_object_link              VARCHAR(20);
 DECLARE v_object_id                INT;
 DECLARE v_instr_index              INT;
 
 DECLARE cur_dre_rules_with_links CURSOR FOR select rule_type_id,source  from tbl_rule_types where type = 2 and source regexp '(SENSOR|NODE)[:][0-9]+';

 DECLARE cur_parsed_dre_rules CURSOR FOR
 SELECT temp_rule.rule_id, concat('WSNNODE:',temp_rule.object_id) old_link, concat('WSNNODE:' ,node.new_id) new_link
 FROM temp_rule_types temp_rule JOIN temp_nodes node ON (temp_rule.object_id = node.old_id)
 AND temp_rule.object_type = 0
 UNION ALL
  SELECT rule_id,concat('WSNSENSOR:',temp_rule.object_id) old_link, concat('WSNSENSOR:', sensor.new_id) new_link
 FROM temp_rule_types temp_rule JOIN temp_sensors sensor ON (temp_rule.object_id = sensor.old_id)
 AND temp_rule.object_type = 1;
 

 DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;
 
  -- updates predefined rules
    UPDATE `tbl_rule_types` SET `source`='package ConditionalAlerts;\n\nimport com.synapsense.dre.domainobjects.*;\nimport com.synapsense.dre.service.DRulesEngineService;\nimport com.synapsense.dre.service.Trigger;\n\nglobal java.util.concurrent.ConcurrentHashMap triggers;\n\nrule \"Sensor disconnected\" \n	 when\n		$obj0 : WSNSENSOR( $prop0 : lastValue== -3000 )\n	 then\n	 Trigger t = new Trigger(\"Sensor disconnected\");\n     t.addObject(0,$obj0);\n     if(triggers.putIfAbsent(t.getCompositeId(), t) == null){ \n     insert(t);\n     DRulesEngineService.sendAlert(\"Sensor disconnected\", \"Sensor disconnected\", \"B/S[name:\" +  ((WSNNODE)DRulesEngineService.getParent($obj0)).getName() + \"]. SENSOR[name:\" + $obj0.getName() + \"].\",  null, $obj0.getNativeTO(), t, 0);\n     }\nend\nrule \"anti_Sensor disconnected\" \n	 when\n	    $t : Trigger( ruleId == \"Sensor disconnected\" )\n	    $obj0 : WSNSENSOR(eval ( serializedTO  == $t.getObject(0)), $prop0 : lastValue != -3000 )\n\n	 then\nTrigger t = (Trigger)triggers.remove($t.getCompositeId());\nif(t != null) retract(t);\nend' WHERE `name` = 'Sensor disconnected';
    UPDATE `tbl_rule_types` SET `source`='package ConditionalAlerts;\n\nimport com.synapsense.dre.domainobjects.*;\nimport com.synapsense.dre.service.DRulesEngineService;\nimport com.synapsense.dre.service.Trigger;\n\nglobal java.util.concurrent.ConcurrentHashMap triggers;\n\nrule \"Sensor requires service\"\n    when\n        $obj0 : WSNSENSOR( $prop0 : lastValue == -2000)\n    then\n        Trigger t = new Trigger(\"Sensor requires service\");\n        t.addObject(0,$obj0);\n        if(triggers.putIfAbsent(t.getCompositeId(), t) == null){ \n        insert(t);\n        DRulesEngineService.sendAlert(\"Sensor requires service\", \"Sensor requires service\", \"B/S[name:\" +  ((WSNNODE)DRulesEngineService.getParent($obj0)).getName()+ \"]. SENSOR[name:\" + $obj0.getName() + \"].\",  null, $obj0.getNativeTO(), t, 0);\n        }\nend\nrule \"anti_Sensor requires service\" \n	 when\n		$t : Trigger( ruleId == \"Sensor requires service\" )\n	    $obj0 : WSNSENSOR(eval ( serializedTO  == $t.getObject(0)), $prop0 : lastValue != -2000 )\n\n	 then\nTrigger t = (Trigger)triggers.remove($t.getCompositeId());\nif(t != null) retract(t);\nend' WHERE `name` = 'Sensor requires service';
    UPDATE `tbl_rule_types` SET `source`='package ConditionalAlerts;\n\nimport com.synapsense.dre.domainobjects.*;\nimport com.synapsense.dre.service.DRulesEngineService;\nimport com.synapsense.dre.service.Trigger;\n\nglobal java.util.concurrent.ConcurrentHashMap triggers;\n\nrule \"Base station not reporting\"\n    when\n        ($obj0:WSNNODE(status == 0))\n\n    then\n     Trigger t = new Trigger(\"Base station not reporting\");\n     t.addObject(0,$obj0);\n     if(triggers.putIfAbsent(t.getCompositeId(), t) == null){ \n     insert(t);\n     String l = $obj0.getLocation();\n     String loc = (l == null || \"\".equals(l)) ? \"\" : (\", location:\" + l);\n     DRulesEngineService.sendAlert(\"Base station not reporting\", \"Base station not reporting\", \"B/S[name:\" + $obj0.getName() + loc + \"].\",  null, $obj0.getNativeTO(), t, 0);\n	}\nend\nrule \"anti_Base station not reporting\" \n	 when\n		$t : Trigger( ruleId == \"Base station not reporting\" )\n	    $obj0 : WSNNODE(eval ( serializedTO  == $t.getObject(0)), status == 1 )\n\n	 then\nTrigger t = (Trigger)triggers.remove($t.getCompositeId());\nif(t != null) retract(t);\nend' WHERE `name` = 'Base station not reporting';    

  -- seeding auxilary table 
   OPEN cur_dre_rules_with_links;
   rule_loop:
   LOOP 
     FETCH cur_dre_rules_with_links INTO cv_rule_id, cv_source;       
      IF done
      THEN      
        LEAVE rule_loop;
      END IF;
     
     set v_temp_source = cv_source;
     while instr(cv_source,'"NODE:')>0 do 
      -- takes first occurence of NODE link string 
      set cv_source = substring(cv_source,instr(cv_source,'"NODE:')+5);
      set v_object_link = substring(cv_source,instr(cv_source,':'),10);      
      set v_object_id = convert(substring(v_object_link,instr(v_object_link,':')+1, instr(v_object_link,'"')-instr(v_object_link,':')-1),UNSIGNED);
      insert into temp_rule_types (rule_id , object_id , object_type) values (cv_rule_id,v_object_id,0);      
     end while;
     
     set cv_source = v_temp_source;
     while instr(cv_source,'"SENSOR:')>0 do 
      -- takes first occurence of SENSOR link string 
      set cv_source = substring(cv_source,instr(cv_source,'"SENSOR:')+7);
      set v_object_link = substring(cv_source,instr(cv_source,':'),10);      
      set v_object_id = convert(substring(v_object_link,instr(v_object_link,':')+1, instr(v_object_link,'"')-instr(v_object_link,':')-1),UNSIGNED);
      insert into temp_rule_types (rule_id , object_id , object_type) values (cv_rule_id,v_object_id,1);      
     end while;
      
   END LOOP;
   
   CLOSE cur_dre_rules_with_links;
   
   -- rename node,sensor type to new names
   update tbl_rule_types set source =  replace(replace(source,'NODE','WSNNODE'),'SENSOR','WSNSENSOR')
   -- excludes CRE rules
   where type = 2
   -- excludes predefined     
   and rule_type_id > 9;
   
   update tbl_rule_types set source =  replace(source,'GATEWAY','WSNGATEWAY')
   -- excludes CRE rules
   where type = 2
   -- excludes predefined     
   and rule_type_id > 9;
   
   
   set done = 0;
   
   -- replaces old node,sensor ids by new ones
   OPEN cur_parsed_dre_rules;
   parsed_rule_loop:
   LOOP 
     FETCH cur_parsed_dre_rules INTO cv_rule_id, cv_old_link, cv_new_link;       
      IF done
      THEN
        LEAVE parsed_rule_loop;
      END IF;
      
      update tbl_rule_types 
      set source = replace(source,cv_old_link,cv_new_link) 
      where rule_type_id = cv_rule_id;
      
   END LOOP;
   CLOSE cur_parsed_dre_rules;     

  
   -- updates of property names
   update tbl_rule_types set source =  replace(source,'"AMin"','"aMin"')
   -- excludes CRE rules
   where type = 2;
   
   update tbl_rule_types set source =  replace(source,'"AMax"','"aMax"')
   -- excludes CRE rules
   where type = 2;
   
   update tbl_rule_types set source =  replace(source,'"RMin"','"rMin"')
   -- excludes CRE rules
   where type = 2;
   
   update tbl_rule_types set source =  replace(source,'"RMax"','"rMax"')
   -- excludes CRE rules
   where type = 2;
     
   update tbl_rule_types set source =  replace(source,'type.dataClass.dataClassId','dataclass')
   -- excludes CRE rules
   where type = 2;
   
   -- consider more safe way to replace old style reference NODE<-SENSOR
   update tbl_rule_types set source =  replace(source,'nodeId == $obj0.nodeLogicalId','eval($obj0.equals(DRulesEngineService.getParent($sens0)))')
   -- excludes CRE rules
   where type = 2;
      
END MAIN|

DELIMITER ;