DROP PROCEDURE IF EXISTS migrate_battery_data;

DELIMITER |

CREATE PROCEDURE migrate_battery_data()
BEGIN

INSERT INTO historical_values(value_id_fk, timestamp, valued)
 SELECT obj_values.value_id, bc. battery_capacity_time_stamp , bc.battery_capacity
  FROM temp_nodes, obj_values , tbl_battery_capacity bc, attributes, object_types
WHERE
 obj_values.object_id_fk = temp_nodes.new_id
 AND obj_values.attr_id_fk = attributes.attr_id
 AND bc.node_logical_id_fk = temp_nodes.old_id
 AND attributes.object_type_id_fk = object_types.objtype_id
 AND object_types.NAME = 'WSNNODE'
 AND attributes.NAME  = 'batteryCapacity';

END|

DELIMITER ;