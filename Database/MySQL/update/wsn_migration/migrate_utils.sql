
DROP FUNCTION IF EXISTS insert_object_value;
DELIMITER | 
CREATE FUNCTION insert_object_value(
  in_object_id int, in_property_id int, in_timestamp long)
  RETURNS INTEGER
  BEGIN
    INSERT INTO obj_values(
                  object_id_fk, attr_id_fk, TIMESTAMP)
    VALUES (in_object_id, in_property_id, in_timestamp);

    RETURN last_insert_id();
  END|
DELIMITER ; 

DROP PROCEDURE IF EXISTS insert_varchar_value;

DELIMITER |
CREATE PROCEDURE insert_varchar_value(
  in_object_id int, in_property_id int, in_timestamp long, valuev varchar(20000))
  BEGIN
    DECLARE valueId   INT;

    SET valueId =
          insert_object_value(
            in_object_id, in_property_id, in_timestamp);

    INSERT INTO valuev(
                  value_id_fk, VALUE)
    VALUES (valueId, valuev);
  END|

DELIMITER ;

DROP PROCEDURE IF EXISTS insert_double_value;

DELIMITER |

CREATE PROCEDURE insert_double_value(
  in_object_id int, in_property_id int, in_timestamp long, valued double) 
  BEGIN
    DECLARE valueId   int;

    SET valueId =
          insert_object_value(
            in_object_id, in_property_id, in_timestamp);

    INSERT INTO valued(
                  value_id_fk, value)
    VALUES (valueId, valued);

  END|

DELIMITER ;

DROP PROCEDURE IF EXISTS insert_int_value;

DELIMITER |
CREATE PROCEDURE insert_int_value(
  in_object_id int, in_property_id int, in_timestamp long, valuei int)
  BEGIN
    DECLARE valueId   int;

    SET valueId =
          insert_object_value(
            in_object_id, in_property_id, in_timestamp);

    INSERT INTO valuei(
                  value_id_fk, value)
    VALUES (valueId, valuei);
  END|
DELIMITER ;

DROP PROCEDURE IF EXISTS insert_long_value;

DELIMITER |
CREATE PROCEDURE insert_long_value(
  in_object_id int, in_property_id int, in_timestamp long, valuel long)
  BEGIN
    DECLARE valueId   int;

    SET valueId =
          insert_object_value(
            in_object_id, in_property_id, in_timestamp);

    INSERT INTO valuel(
                  value_id_fk, value)
    VALUES (valueId, valuel);
  END|
DELIMITER ;

DROP PROCEDURE IF EXISTS insert_link_value;

DELIMITER |
CREATE PROCEDURE insert_link_value(
  in_object_id int, in_property_id int, in_timestamp long, value_link int)
  BEGIN
    DECLARE valueId   int;
    DECLARE typeName  varchar(300);
    
    SET valueId =
          insert_object_value(
            in_object_id, in_property_id, in_timestamp);
            
    IF (value_link IS NULL) THEN
      INSERT INTO value_obj_link(
                  value_id_fk, object_id,object_type)
      VALUES (valueId, NULL, NULL);
    ELSE
      SELECT ot.NAME INTO typeName FROM objects o , object_types ot WHERE o.object_type_id_fk = ot.objtype_id
      AND o.id = value_link;

      INSERT INTO value_obj_link(value_id_fk, object_id, object_type)
      VALUES (valueId, value_link, typeName);

    END IF;
    
  END|
DELIMITER ;


DROP FUNCTION IF EXISTS get_wsn_newid;

DELIMITER |
CREATE FUNCTION get_wsn_newid(
  in_wsn_type VARCHAR(300), in_old_id int)
  RETURNS INTEGER
  BEGIN
    DECLARE v_new_id   int;

    case in_wsn_type
      when 'NETWORK'
      then
        select new_id
        into v_new_id
        from TEMP_NETWORKS
        where old_id = in_old_id;
      when 'NODE'
      then
        select new_id
        into v_new_id
        from TEMP_NODES
        where old_id = in_old_id;
      when 'PLATFORM'
      then
        select new_id
        into v_new_id
        from TEMP_PLATFORMS
        where old_id = in_old_id;
      when 'SENSORTYPE'
      then
        select new_id
        into v_new_id
        from TEMP_SENSORTYPES
        where old_id = in_old_id;
      else
        set v_new_id = -1;
    end case;

    return v_new_id;
  END|
DELIMITER ;