DROP TABLE IF EXISTS TEMP_NETWORKS;
DROP TABLE IF EXISTS TEMP_NODES;
DROP TABLE IF EXISTS TEMP_SENSORS;
DROP TABLE IF EXISTS temp_historical_transfer_log;
DROP TABLE IF EXISTS temp_rule_types;

CREATE TABLE TEMP_NETWORKS(old_id int, new_id int,topology_id int, PRIMARY KEY(old_id, new_id));
CREATE TABLE TEMP_NODES(old_id int, new_id int, topology_id int, neighbor int, PRIMARY KEY(old_id, new_id));
CREATE TABLE TEMP_SENSORS(old_id int, new_id int, topology_id int, PRIMARY KEY(old_id, new_id));      
CREATE TABLE temp_rule_types(id int auto_increment , rule_id int , object_id int , object_type int, PRIMARY KEY(id));

set autocommit = 0;

-- next actions are executing in one transaction
START TRANSACTION;
-- run order is important because of links between objects
call migrate_networks('WSNNETWORK');
call migrate_nodes('WSNNODE');
call migrate_sensors('WSNSENSOR');
call migrate_gateways();

call migrate_battery_data();
call migrate_statistic();
call migrate_alerts();
call migrate_rules('ss');

call migrate_schemachanges();
COMMIT; -- so you can be sure that all previous calls are done or be cancelled

SET FOREIGN_KEY_CHECKS=0;

-- next instructions cannot be done in a single transaction because of implicit commits in mysql
DROP TABLE IF EXISTS tbl_generic_wsn_assoc;
DROP TABLE IF EXISTS tbl_alert_to_topology_map;
DROP TABLE IF EXISTS tbl_sensor_network_topology;
DROP TABLE IF EXISTS tbl_sensor_network;
DROP TABLE IF EXISTS tbl_battery_capacity;
DROP TABLE IF EXISTS tbl_sensor_node;
DROP TABLE IF EXISTS tbl_core_platform;
DROP TABLE IF EXISTS tbl_sensor;
DROP TABLE IF EXISTS tbl_sensor_type;
DROP TABLE IF EXISTS tbl_data_class;
DROP TABLE IF EXISTS tbl_units;
DROP TABLE IF EXISTS tbl_realtime_data;

DROP TABLE IF EXISTS tbl_nwk_route;
DROP TABLE IF EXISTS `ARCHIVE_SENSOR_DATA`;

SET FOREIGN_KEY_CHECKS=1;

-- moved from migrate_schemachanges() because of implicit commit 
    
alter table tbl_network_statbase drop column `network_id`;

alter table tbl_network_statnode drop column `network_id`;

-- drops-creates nht table because it was broken before
drop table if exists `tbl_nht_info`;
 
create table `tbl_nht_info` (
  `id` int(11) not null auto_increment,
  `node_id` int(11),
  `num_neigh` int(11),
  `report_timestamp` datetime,
  `nids` binary(20),
  `rssi` tinyblob,
  `state` tinyblob,
  `hop` tinyblob,
  `hoptm` tinyblob,
  primary key(`id`),
  index `idx_node_id_timestamp`(`node_id`, `report_timestamp`)
)
engine=innodb;

-- Drop View all_objects
drop view if exists `all_objects`;

create view `all_objects` as
select o.id  as id, t.name as name , 1 as generic
from objects o , object_types t
where o.object_type_id_fk= t.objtype_id;

-- Drop Event clear_expired_data
drop event if exists `clear_expired_data`;

delimiter |
create event `clear_expired_data`
  on schedule every '1' month starts '2009-10-01 00:01:00'
  enable
do
begin
  delete from tbl_nht_info where report_timestamp <= date_sub(curdate(),interval 1 month);
  delete from tbl_network_statnode where nwk_timestamp <= date_sub(curdate(),interval 3 month);
  delete from tbl_activity_log where `timestamp` <= date_sub(curdate(),interval 3 month);
  delete from tbl_network_statbase where nwk_timestamp <= date_sub(curdate(),interval 3 month);
end; |
delimiter ; 


