DROP PROCEDURE IF EXISTS migrate_units;

DELIMITER | 
CREATE PROCEDURE migrate_units(in_newname varchar(50))
  BEGIN
    DECLARE done                INT DEFAULT 0;

    DECLARE lastTypeId          INT;
    DECLARE lastObjectId        INT;
    DECLARE namePropertyId      INT;
    DECLARE descPropertyId      INT;
    DECLARE symbolPropertyId    INT;
    DECLARE lastObjectValueId   INT;
    DECLARE idPropertyId        INT;
    
    DECLARE currentTime         LONG;

    DECLARE cv_old_id           VARCHAR(300);
    DECLARE cv_name             VARCHAR(300);
    DECLARE cv_description      VARCHAR(300);
    DECLARE cv_symbol           VARCHAR(300);

    DECLARE
      cur_units CURSOR FOR
        SELECT unit_id, unit_name, unit_symbol, unit_description FROM tbl_units;

    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

    INSERT INTO object_types(name)
    VALUES (in_newname);

    SET lastTypeId := last_insert_id();

    INSERT INTO objects(
                  name, object_type_id_fk, static)
    VALUES (in_newname, lastTypeId, 1);

    INSERT INTO attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    VALUES ('unitName', lastTypeId, 3);

    SET namePropertyId := last_insert_id();

    INSERT INTO attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    VALUES ('unitSymbol', lastTypeId, 3);

    SET symbolPropertyId := last_insert_id();

    INSERT INTO attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    VALUES ('unitDescription', lastTypeId, 3);

    SET descPropertyId := last_insert_id();
    
    INSERT INTO attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    VALUES ('id', lastTypeId, 1);

    SET idPropertyId := last_insert_id();

    SET currentTime = UNIX_TIMESTAMP() * 1000;

    create table TEMP_UNITS(
      old_id int, new_id int, primary key(old_id, new_id));

    OPEN cur_units;

   main:
    LOOP
      FETCH cur_units
      INTO cv_old_id, cv_name, cv_description, cv_symbol;

      IF done
      THEN
        LEAVE main;
      END IF;

      INSERT INTO objects(object_type_id_fk)
      VALUES (lastTypeId);

      SET lastObjectId = last_insert_id();

      insert into TEMP_UNITS
      values (cv_old_id, lastObjectId);

      call insert_varchar_value(
             lastObjectId, namePropertyId, currentTime, cv_name);
      call insert_varchar_value(
             lastObjectId, descPropertyId, currentTime, cv_description);
      call insert_varchar_value(
             lastObjectId, symbolPropertyId, currentTime, cv_symbol);
      call insert_int_value(
             lastObjectId, idPropertyId, currentTime, cv_old_id);
    END LOOP;

    CLOSE cur_units;
  END|

DELIMITER ;