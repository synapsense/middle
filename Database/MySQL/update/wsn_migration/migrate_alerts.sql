DROP PROCEDURE IF EXISTS migrate_alerts;

DELIMITER |
CREATE PROCEDURE migrate_alerts()
BEGIN

    -- replace ids
    insert into tbl_alert_to_objects_map(alert_id_fk,object_id_fk)
       select t.alert_id_fk, net.new_id from temp_networks net join tbl_alert_to_topology_map t on (net.topology_id = t.topology_id_fk);

    insert into tbl_alert_to_objects_map(alert_id_fk,object_id_fk)
       select t.alert_id_fk, node.new_id from temp_nodes node join tbl_alert_to_topology_map t on (node.topology_id = t.topology_id_fk);

    insert into tbl_alert_to_objects_map(alert_id_fk,object_id_fk)
       select t.alert_id_fk, sensor.new_id from temp_sensors sensor join tbl_alert_to_topology_map t on (sensor.topology_id = t.topology_id_fk);

END|
DELIMITER ;