DROP PROCEDURE IF EXISTS migrate_gateways;

DELIMITER | 
CREATE PROCEDURE migrate_gateways()
MAIN_BLOCK:BEGIN
    DECLARE done                     INT DEFAULT 0;

    DECLARE lastTypeId               INT;
    DECLARE lastObjectId             INT;
    
    DECLARE addressPropertyId        INT;
    DECLARE protocolPropertyId       INT;
    DECLARE securityModePropertyId   INT;
    DECLARE keyPropertyId            INT;
    
    DECLARE cp_address               VARCHAR(300);

    DECLARE cv_net_id                INT;
    DECLARE cv_net_comport           VARCHAR(300);

    DECLARE cv_gateway_id            INT;
    DECLARE cv_gateway_address       VARCHAR(300);
    DECLARE cv_ip                    VARCHAR(300);
    DECLARE cv_protocol              VARCHAR(300);
    DECLARE cv_sec_mode              VARCHAR(300);
    DECLARE cv_sec_key               VARCHAR(300);
    
    DECLARE currentTime              LONG;

    DECLARE
      cur_networks CURSOR FOR SELECT oldnet.new_id , comport FROM tbl_sensor_network net , temp_networks oldnet     
      where 
        net.network_id = oldnet.old_id
        and instr(net.comport,cp_address) > 0;

    DECLARE
      cur_gateways CURSOR FOR SELECT object_id, val, 
      if( instr(val,';')>0, substring(val,instr(val,':')+1,instr(val,';')-(instr(val,':')+1)) , substring(val,instr(val,':')+1)) ip,
      concat(substring(val,1,instr(val,':')-1),'_RG2') protocol,
      if (instr(val,'[') > 0 , 'AES-CFB-PSK','none') sec_mode,
      substring(val,instr(val,'['),instr(val,']')) sec_key
      FROM all_objects_props_values
        WHERE type_name IN('GATEWAY')
        AND propname IN ('address');

    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;
  
    select objtype_id into lastTypeId from object_types where name = 'GATEWAY';
    if done then 
	leave MAIN_BLOCK;
    end if;

    select attr_id into addressPropertyId from attributes where name = 'address' and object_type_id_fk = lastTypeId;
	    
    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('protocol', lastTypeId, 3);
    
    set protocolPropertyId = last_insert_id();
    
    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('securityMode', lastTypeId, 3);

    set securityModePropertyId = last_insert_id();
 
    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('key', lastTypeId, 3);

    set keyPropertyId = last_insert_id();       
 
    SET currentTime = UNIX_TIMESTAMP() * 1000;
     
    OPEN cur_gateways;

    main:
    LOOP
        FETCH cur_gateways INTO cv_gateway_id, cv_gateway_address, cv_ip, cv_protocol, cv_sec_mode, cv_sec_key;
         IF done
          THEN
            set done = 0;
            LEAVE main;
         END IF;
         
        call insert_varchar_value(
             cv_gateway_id, protocolPropertyId, currentTime, cv_protocol);
        call insert_varchar_value(
             cv_gateway_id, securityModePropertyId, currentTime, cv_sec_mode);
        call insert_varchar_value(
             cv_gateway_id, keyPropertyId, currentTime, cv_sec_key);             
        
        -- updates 'address' property by only IP
        update valuev vv , obj_values ov 
        set vv.value = cv_ip 
        where vv.value_id_fk = ov.value_id
        and ov.attr_id_fk = addressPropertyId
        and ov.object_id_fk = cv_gateway_id;
        
        -- updates cursor parameter to use it in 'cur_networks' cursor
        set cp_address = cv_gateway_address;

    OPEN cur_networks;
      subloop:
        LOOP
          FETCH cur_networks into cv_net_id, cv_net_comport;

          IF done
          THEN
            set done = 0;
            LEAVE subloop;
          END IF;

          if (cv_net_id is null) then
            iterate subloop;
          end if;
          -- new relation 
          INSERT INTO object_parents(
                        parent_id_fk, object_id_fk)
          VALUES (cv_net_id, cv_gateway_id);

        END LOOP;

    CLOSE cur_networks;
    END LOOP;

  CLOSE cur_gateways;

  -- then we need to unlink gateways from DCs
  -- exactly removes all links between DCs and GATEWAYs where DC is parent and GATEWAY is its child
  delete p 
  from object_parents p 
  join objects o on (o.id = p.object_id_fk) 
  join objects o2 on (o2.id = p.parent_id_fk)
  join object_types tchild on (o.object_type_id_fk = tchild.objtype_id) 
  join object_types tparent on (o2.object_type_id_fk = tparent.objtype_id)
  where tchild.name = 'GATEWAY'
  and tparent.name = 'DC';
  
END|
DELIMITER ;