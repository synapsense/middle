DROP PROCEDURE IF EXISTS migrate_gateways;
DROP PROCEDURE IF EXISTS migrate_networks;
DROP PROCEDURE IF EXISTS migrate_nodes;
DROP PROCEDURE IF EXISTS migrate_sensors;
DROP PROCEDURE IF EXISTS migrate_rules;
DROP PROCEDURE IF EXISTS migrate_alerts;
DROP PROCEDURE IF EXISTS migrate_schemachanges;
DROP PROCEDURE IF EXISTS migrate_battery_data;
DROP PROCEDURE IF EXISTS migrate_statistic;
DROP FUNCTION IF EXISTS insert_object_value;
DROP PROCEDURE IF EXISTS insert_varchar_value;
DROP PROCEDURE IF EXISTS insert_double_value;
DROP PROCEDURE IF EXISTS insert_int_value;
DROP PROCEDURE IF EXISTS insert_long_value;
DROP PROCEDURE IF EXISTS insert_link_value;
DROP FUNCTION IF EXISTS get_wsn_newid;

DROP TABLE IF EXISTS temp_rule_types;