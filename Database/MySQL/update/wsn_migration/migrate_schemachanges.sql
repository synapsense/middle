DROP PROCEDURE IF EXISTS migrate_schemachanges;

DELIMITER |

CREATE PROCEDURE migrate_schemachanges()
BEGIN
  
-- GATEWAYS restructure  
-- moving values from "integer store" to "double store"
insert into valued (value_id_fk,value)
select value_id_fk, vi.value from valuei vi , obj_values ov
where
ov.value_id = vi.value_id_fk
and attr_id_fk in (select a.attr_id from attributes a, object_types ot
where a.object_type_id_fk = ot.objtype_id and ot.name in ('GATEWAY','CRAC','RACK','PRESSURE') and a.name in('x','y'));

-- clearing integer store of unnecessary values            
delete vi from valuei vi , obj_values ov
where
ov.value_id = vi.value_id_fk
and attr_id_fk in (select a.attr_id from attributes a, object_types ot
where a.object_type_id_fk = ot.objtype_id and ot.name in ('GATEWAY','CRAC','RACK','PRESSURE') and a.name in('x','y'));

-- removing all GATEWAY.networkId values
delete vi,ov from valuei vi , obj_values ov
where
ov.value_id = vi.value_id_fk
and attr_id_fk in (select a.attr_id from attributes a, object_types ot
where a.object_type_id_fk = ot.objtype_id and ot.name in ('GATEWAY') and a.name in('networkId'));

-- removing networkId attribute
delete a from attributes a join object_types ot
on (a.object_type_id_fk = ot.objtype_id)
where ot.name in ('GATEWAY') and a.name in('networkId');

-- changes x , y type to double
update attributes a, object_types ot 
set a.attr_type_id_fk = 2
where a.object_type_id_fk = ot.objtype_id
and ot.name in ('GATEWAY','CRAC','RACK','PRESSURE')
and a.name in ('x','y');

-- renaming uniqueId property to mac
update attributes a, object_types ot 
set a.name = 'mac'
where a.object_type_id_fk = ot.objtype_id
and ot.name in ('GATEWAY')
and a.name in ('uniqueId');

-- gateway renaming
update object_types set name = 'WSNGATEWAY' where name = 'GATEWAY';

-- gateway links update
update value_obj_link vol
set vol.object_type = 'WSNGATEWAY'
where vol.object_type = 'GATEWAY';

--  CALCED_SENSOR type structural update
update attributes a , object_types t 
set a.name = 'aMin'
where a.object_type_id_fk = t.objtype_id 
and t.name = 'CALCED_SENSOR'
and a.name = 'AMin';

update attributes a , object_types t 
set a.name = 'aMax'
where a.object_type_id_fk = t.objtype_id 
and t.name = 'CALCED_SENSOR'
and a.name = 'AMax';

update attributes a , object_types t 
set a.name = 'rMin'
where a.object_type_id_fk = t.objtype_id 
and t.name = 'CALCED_SENSOR'
and a.name = 'RMin';

update attributes a , object_types t 
set a.name = 'rMax'
where a.object_type_id_fk = t.objtype_id 
and t.name = 'CALCED_SENSOR'
and a.name = 'RMax';

update attributes a , object_types t 
set a.name = 'min'
where a.object_type_id_fk = t.objtype_id 
and t.name = 'CALCED_SENSOR'
and a.name = 'sensorDisplayMin';

update attributes a , object_types t 
set a.name = 'max'
where a.object_type_id_fk = t.objtype_id 
and t.name = 'CALCED_SENSOR'
and a.name = 'sensorDisplayMax';

--  SANDOVALSENSOR  type structural update
update attributes a , object_types t 
set a.name = 'aMin'
where a.object_type_id_fk = t.objtype_id 
and t.name = 'SANDOVALSENSOR'
and a.name = 'AMin';

update attributes a , object_types t 
set a.name = 'aMax'
where a.object_type_id_fk = t.objtype_id 
and t.name = 'SANDOVALSENSOR'
and a.name = 'AMax';

update attributes a , object_types t 
set a.name = 'rMin'
where a.object_type_id_fk = t.objtype_id 
and t.name = 'SANDOVALSENSOR'
and a.name = 'RMin';

update attributes a , object_types t 
set a.name = 'rMax'
where a.object_type_id_fk = t.objtype_id 
and t.name = 'SANDOVALSENSOR'
and a.name = 'RMax';

update attributes a , object_types t 
set a.name = 'min'
where a.object_type_id_fk = t.objtype_id 
and t.name = 'SANDOVALSENSOR'
and a.name = 'sensorDisplayMin';

update attributes a , object_types t 
set a.name = 'max'
where a.object_type_id_fk = t.objtype_id 
and t.name = 'SANDOVALSENSOR'
and a.name = 'sensorDisplayMax';

update attributes a , object_types t 
set a.name = 'image'
where a.object_type_id_fk = t.objtype_id 
and t.name = 'ZONE'
and a.name = 'map';

-- adding wsnUpgrade property (not historical, BinaryData) to store .csv file into the object model 
insert into attributes (name, historical, object_type_id_FK, attr_type_id_FK)
  values ("wsnUpgrade", 0, (select objtype_id from object_types where name = "ROOT"), 8);

-- creating value_id 
insert into obj_values (timestamp, object_id_FK, attr_id_FK) values (unix_timestamp() * 1000, (select id from objects where object_type_id_FK = (select object_type_id_FK FROM objects where name = "ROOT") order by id desc limit 1), last_insert_id());

-- Allow group_concat to work with long strings
set session group_concat_max_len = 15000000;

-- convert mapping tables into csv blob and insert it into the data model
insert into value_binary (value_id_fk, value) values (last_insert_id(), 
  (select group_concat(map separator '\n') from
    (select concat("NETWORK:", t.old_id, ", WSNNETWORK:", t.new_id) as map  from temp_networks t union all
     select concat("NODE:", t1.old_id, ", WSNNODE:", t1.new_id) as map from temp_nodes t1 union all
     select concat("SENSOR:", t2.old_id, ", WSNSENSOR:", t2.new_id) as map from temp_sensors t2
    ) as mapping
  )
);

-- set variable to its default
set session group_concat_max_len = 1024;

END|

DELIMITER ;