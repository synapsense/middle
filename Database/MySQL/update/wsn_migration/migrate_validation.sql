DROP PROCEDURE IF EXISTS migrate_validation;

DELIMITER | 

CREATE PROCEDURE migrate_validation(out out_message varchar(300))
  MAIN:
  BEGIN
    DECLARE temp_int1 INT;
    DECLARE temp_int2 INT;
    
    -- sets up all is ok status 
    set out_message = 'OK';   
    
    
    select count(*) into temp_int1 from tbl_sensor_network;    
    select count(*) into temp_int2 from objects o join object_types ot on (o.object_type_id_fk=ot.objtype_id)
    where ot.name = 'WSNNETWORK' and o.static=0;
    if temp_int1 <> temp_int2 then 
      SET out_message = concat('Problem migrating networks. was ', temp_int1,' networks', ', moved ',temp_int1);
      leave main;
    end if;    
    
    select count(*) into temp_int1 from tbl_sensor_node;    
    select count(*) into temp_int2 from objects o join object_types ot on (o.object_type_id_fk=ot.objtype_id)
    where ot.name = 'WSNNODE' and o.static=0;
    if temp_int1 <> temp_int2 then 
      SET out_message = concat('Problem migrating nodes. was ', temp_int1,' nodes', ', moved ',temp_int1);
      leave main;
    end if;
    
    select count(*) into temp_int1 from tbl_sensor;    
    select count(*) into temp_int2 from objects o join object_types ot on (o.object_type_id_fk=ot.objtype_id)
    where ot.name = 'WSNSENSOR' and o.static=0;
    if temp_int1 <> temp_int2 then 
      SET out_message = concat('Problem migrating sensors. was ', temp_int1,' sensors', ', moved ',temp_int1);
      leave main;
    end if;
    
   select count(*) into temp_int1 FROM tbl_nwk_route;   
   select count(*) into temp_int2 FROM all_objects_props_values
    where type_name = 'WSNNODE'
    and propname in ('route')
    and val is not null;
    
   if temp_int1 <> temp_int2 then 
      SET out_message = concat('Problem migrating routes. was ', temp_int1,' routes',', moved ',temp_int2);
      leave main;
   end if;
    
   SELECT count(*) into temp_int1 FROM all_objects_props_values
   where propname in('x','y') and instr(val,'.') = 0;
   
   if temp_int1 > 0 then 
      SET out_message = concat('Not all x,y properties are doubles');
      leave main;
   end if;
    
   SELECT count(*) into temp_int1 from attributes a, object_types ot 
   where a.object_type_id_fk = ot.objtype_id
   and ot.name in ('GATEWAY')
   and a.name in ('uniqueId','networkId');
   if temp_int1 > 0 then 
      SET out_message = concat('GATEWAY still has uniqueId or networkId property');
      leave main;
   end if;
    
   SELECT count(*) into temp_int1 from attributes a, object_types ot 
   where a.object_type_id_fk = ot.objtype_id
   and ot.name in ('GATEWAY')
   and a.name in ('mac');
   if temp_int1 = 0 then 
      SET out_message = concat('GATEWAY has no property mac');
      leave main;
   end if;

  END MAIN|
  
DELIMITER ;   