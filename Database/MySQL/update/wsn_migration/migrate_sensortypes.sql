DROP PROCEDURE IF EXISTS migrate_sensortypes;

DELIMITER | 
CREATE PROCEDURE migrate_sensortypes(in_newname VARCHAR(50))
  BEGIN
    DECLARE done                     INT DEFAULT 0;

    DECLARE lastTypeId               INT;
    DECLARE lastObjectId             INT;

    DECLARE idPropertyId             INT;
    DECLARE namePropertyId           INT;
    DECLARE capabilityPropertyId     INT;
    DECLARE manufacturerPropertyId   INT;
    DECLARE dataClassPropertyId      INT;

    DECLARE lastObjectValueId        INT;
    DECLARE currentTime              LONG;

    DECLARE cv_old_id                INT;
    DECLARE cv_name                  VARCHAR(300);
    DECLARE cv_capability            VARCHAR(300);
    DECLARE cv_manufacturer          VARCHAR(300);
    DECLARE cv_dataclass             INT;

    DECLARE
      cur_sensortypes CURSOR FOR
        SELECT type_id, sensor_name, sensor_capability, sensor_manufacturer, sensor_data_class_id FROM tbl_sensor_type;

    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

    INSERT INTO object_types(name)
    VALUES (in_newname);

    SET lastTypeId := last_insert_id();

    INSERT INTO objects(
                  name, object_type_id_fk, static)
    VALUES (in_newname, lastTypeId, 1);

    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('name', lastTypeId, 3);

    set namePropertyId = last_insert_id();

    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('capability', lastTypeId, 3);

    set capabilityPropertyId = last_insert_id();

    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('manufacturer', lastTypeId, 3);

    set manufacturerPropertyId = last_insert_id();

    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('dataClass', lastTypeId, 5);

    set dataClassPropertyId = last_insert_id();

    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('id', lastTypeId, 1);

    set idPropertyId = last_insert_id();
    

    create table TEMP_SENSORTYPES(
      old_id int, new_id int, primary key(old_id, new_id));

    SET currentTime = UNIX_TIMESTAMP() * 1000;

    OPEN cur_sensortypes;

   main:
    LOOP
      FETCH cur_sensortypes
      INTO cv_old_id, cv_name, cv_capability, cv_manufacturer, cv_dataclass;

      IF done
      THEN
        LEAVE main;
      END IF;

      INSERT INTO objects(object_type_id_fk)
      VALUES (lastTypeId);

      SET lastObjectId = last_insert_id();

      insert into TEMP_SENSORTYPES
      values (cv_old_id, lastObjectId);

      call insert_varchar_value(
             lastObjectId, namePropertyId, currentTime, cv_name);
      call insert_varchar_value(
             lastObjectId, capabilityPropertyId, currentTime, cv_capability);
      call insert_varchar_value(
             lastObjectId, manufacturerPropertyId, currentTime, cv_manufacturer);
      call insert_int_value(
             lastObjectId, idPropertyId, currentTime, cv_old_id);

      select new_id
      into cv_dataclass
      from TEMP_DATACLASSES
      where old_id = cv_dataclass;

      call insert_link_value(
             lastObjectId, dataClassPropertyId, currentTime, cv_dataclass);
    END LOOP;

    CLOSE cur_sensortypes;
  END|
DELIMITER ;