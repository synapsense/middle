DROP PROCEDURE IF EXISTS migrate_dataclasses;

DELIMITER | 
CREATE PROCEDURE migrate_dataclasses(in_newname VARCHAR(50))
  BEGIN
    DECLARE done                          INT DEFAULT 0;

    DECLARE lastTypeId                    INT;
    DECLARE lastObjectId                  INT;
    DECLARE namePropertyId                INT;
    DECLARE descPropertyId                INT;
    DECLARE scalarComplexFlagPropertyId   INT;
    DECLARE minValuePropertyId            INT;
    DECLARE maxValuePropertyId            INT;
    DECLARE presisionPropertyId           INT;
    DECLARE aminPropertyId                INT;
    DECLARE amaxPropertyId                INT;
    DECLARE rminPropertyId                INT;
    DECLARE rmaxPropertyId                INT;
    DECLARE unitPropertyId                INT;
    DECLARE idPropertyId                  INT;

    DECLARE lastObjectValueId             INT;
    DECLARE currentTime                   LONG;

    DECLARE cv_old_id                     INT;
    DECLARE cv_name                       VARCHAR(300);
    DECLARE cv_description                VARCHAR(300);
    DECLARE cv_scalarComplexFlag          INT;
    DECLARE cv_minvalue                   DOUBLE;
    DECLARE cv_maxvalue                   DOUBLE;
    DECLARE cv_presision                  INT;
    DECLARE cv_unit                       INT;
    DECLARE cv_amin                       DOUBLE;
    DECLARE cv_amax                       DOUBLE;
    DECLARE cv_rmin                       DOUBLE;
    DECLARE cv_rmax                       DOUBLE;

    DECLARE
      cur_dataclasses CURSOR FOR
        SELECT data_class_id, data_class_name, data_class_description, is_scalar_or_complex_flg, data_class_minimum_value,
               data_class_maximum_value, data_class_precision, unit_id, data_class_a_min, data_class_a_max,
               data_class_r_min, data_class_r_max
        FROM tbl_data_class;

    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

    INSERT INTO object_types(name)
    VALUES (in_newname);

    SET lastTypeId := last_insert_id();

    INSERT INTO objects(
                  name, object_type_id_fk, static)
    VALUES (in_newname, lastTypeId, 1);

    INSERT INTO attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    VALUES ('dataClassName', lastTypeId, 3);

    SET namePropertyId := last_insert_id();

    INSERT INTO attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    VALUES ('dataClassDescription', lastTypeId, 3);

    SET descPropertyId := last_insert_id();

    INSERT INTO attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    VALUES ('isScalarOrComplexFlg', lastTypeId, 1);

    SET scalarComplexFlagPropertyId := last_insert_id();

    INSERT INTO attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    VALUES ('dataClassMinimumValue', lastTypeId, 2);

    SET minValuePropertyId := last_insert_id();

    INSERT INTO attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    VALUES ('dataClassMaximumValue', lastTypeId, 2);

    SET maxValuePropertyId := last_insert_id();

    INSERT INTO attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    VALUES ('dataClassPrecision', lastTypeId, 2);

    SET presisionPropertyId := last_insert_id();

    INSERT INTO attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    VALUES ('dataClassAMin', lastTypeId, 2);

    SET aminPropertyId := last_insert_id();

    INSERT INTO attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    VALUES ('dataClassAMax', lastTypeId, 2);

    SET amaxPropertyId := last_insert_id();

    INSERT INTO attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    VALUES ('dataClassRMin', lastTypeId, 2);

    SET rminPropertyId := last_insert_id();

    INSERT INTO attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    VALUES ('dataClassRMax', lastTypeId, 2);

    SET rmaxPropertyId := last_insert_id();

    /*INSERT INTO attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    VALUES ('unit', lastTypeId, 5);

    SET unitPropertyId := last_insert_id();*/

    INSERT INTO attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    VALUES ('id', lastTypeId, 1);

    SET idPropertyId := last_insert_id();
    
    create table TEMP_DATACLASSES(
      old_id int, new_id int, primary key(old_id, new_id));

    SET currentTime = UNIX_TIMESTAMP() * 1000;

    OPEN cur_dataclasses;

   main:
    LOOP
      FETCH cur_dataclasses
      INTO cv_old_id, cv_name, cv_description, cv_scalarComplexFlag, cv_minvalue,
           cv_maxvalue, cv_presision, cv_unit, cv_amin, cv_amax,
           cv_rmin, cv_rmax;

      IF done
      THEN
        LEAVE main;
      END IF;

      INSERT INTO objects(object_type_id_fk)
      VALUES (lastTypeId);

      SET lastObjectId = last_insert_id();

      insert into TEMP_DATACLASSES
      values (cv_old_id, lastObjectId);

      call insert_varchar_value(
             lastObjectId, namePropertyId, currentTime, cv_name);
      call insert_varchar_value(
             lastObjectId, descPropertyId, currentTime, cv_description);
      call insert_int_value(
             lastObjectId, scalarComplexFlagPropertyId, currentTime, cv_scalarComplexFlag);
      call insert_double_value(
             lastObjectId, minValuePropertyId, currentTime, cv_minvalue);
      call insert_double_value(
             lastObjectId, maxValuePropertyId, currentTime, cv_maxvalue);
      call insert_int_value(
             lastObjectId, presisionPropertyId, currentTime, cv_presision);
      call insert_double_value(
             lastObjectId, aminPropertyId, currentTime, cv_amin);
      call insert_double_value(
             lastObjectId, amaxPropertyId, currentTime, cv_amax);
      call insert_double_value(
             lastObjectId, rminPropertyId, currentTime, cv_rmin);
      call insert_double_value(
             lastObjectId, rmaxPropertyId, currentTime, cv_rmax);
      call insert_int_value(
             lastObjectId, idPropertyId, currentTime, cv_old_id);
             
     /* select new_id
      into cv_unit
      from temp_units
      where old_id = cv_unit;

      call insert_link_value(
             lastObjectId, unitPropertyId, currentTime, cv_unit);*/
             
    END LOOP;

    CLOSE cur_dataclasses;
  END|
DELIMITER ;