DROP PROCEDURE IF EXISTS migrate_nodes;

DELIMITER | 
CREATE PROCEDURE migrate_nodes(in_newname VARCHAR(50))
  BEGIN
    DECLARE done                        INT DEFAULT 0;

    DECLARE lastTypeId                  INT;
    DECLARE lastObjectId                INT;

    DECLARE cp_topology_id              INT;

    DECLARE namePropertyId              INT;
    DECLARE descPropertyId              INT;
    DECLARE periodPropertyId            INT;
    DECLARE macPropertyId               INT;
    DECLARE locationPropertyId          INT;
    DECLARE xPropertyId                 INT;
    DECLARE yPropertyId                 INT;
    DECLARE platformIdPropertyId        INT;
    DECLARE platformNamePropertyId      INT;
    DECLARE appPropertyId               INT;
    DECLARE osPropertyId                INT;
    DECLARE nwkPropertyId               INT;
    DECLARE capacityPropertyId          INT;
    DECLARE remainingDaysPropertyId     INT;
    DECLARE idPropertyId                INT;
    DECLARE statusPropertyId            INT;
    DECLARE routePropertyId             INT;
    DECLARE batteryOperatedPropertyId   INT;
    DECLARE programPropertyId           INT;

    DECLARE lastObjectValueId           INT;
    DECLARE currentTime                 LONG;

    DECLARE cv_old_id                   INT;
    DECLARE cv_name                     VARCHAR(300);
    DECLARE cv_desc                     VARCHAR(300);
    DECLARE cv_period                   VARCHAR(300);
    DECLARE cv_mac                      LONG;
    DECLARE cv_location                 VARCHAR(300);
    DECLARE cv_x                        DOUBLE;
    DECLARE cv_y                        DOUBLE;
    DECLARE cv_platform_id              INT;
    DECLARE cv_platform_name            VARCHAR(300);
    DECLARE cv_app                      VARCHAR(300);
    DECLARE cv_os                       VARCHAR(300);
    DECLARE cv_nwk                      VARCHAR(300);
    DECLARE cv_capacity                 DOUBLE;
    DECLARE cv_capacity_timestamp       LONG;
    DECLARE cv_remaining_days           INT;
    DECLARE cv_status                   INT;
    DECLARE cv_route                    INT;

    DECLARE cv_topology_id              INT;
    DECLARE cv_parent_id           INT;

    DECLARE
      cur_nodes CURSOR FOR
        SELECT node.node_logical_id,
               node.node_name,
               node.node_description,
               concat(
                 node.node_sample_frequency,
                 ' ',
                 (CASE node.sample_frequency_units
                    WHEN 0 THEN 'sec'
                    WHEN 1 THEN 'min'
                    WHEN 2 THEN 'hr'
                  END)),
               node.node_physical_id,
               node.node_location,
               node.visualization_location_x,
               node.visualization_location_y,
               node.core_platform_id_fk,
               cp.name,
               concat(
                 node.node_app_version, ' ', node.node_app_revision),
               concat(
                 node.node_os_version, ' ', node.node_os_revision),
               concat(
                 node.node_nwk_version, ' ', node.node_nwk_revision),
               node.current_battery_capacity,
               unix_timestamp(node.current_battery_capacity_timestamp) * 1000,
               node.remaining_days,
               topology.id,
               node2.node_logical_id
        FROM tbl_sensor_node node
             JOIN tbl_core_platform cp
               ON (node.core_platform_id_fk = cp.id)
             LEFT OUTER JOIN tbl_nwk_route route
               ON (node.node_physical_id = route.node_physical_id)
             LEFT OUTER JOIN tbl_sensor_node node2
               ON (node2.node_physical_id = route.neighbor_physical_id)
             LEFT OUTER JOIN tbl_sensor_network_topology topology
               ON (node.node_logical_id = topology.node_logical_id_fk)
        WHERE topology.sensor_id_fk IS NULL;

    DECLARE
      cur_node_relations CURSOR FOR
        SELECT object_id_fk
        FROM tbl_generic_wsn_assoc
        WHERE topology_id_fk = cp_topology_id;

    DECLARE
      cur_node_wsn_relations CURSOR FOR
        SELECT network_id_fk
        FROM tbl_sensor_network_topology
        WHERE id = cp_topology_id;

    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

    INSERT INTO object_types(name)
    VALUES (in_newname);

    SET lastTypeId := last_insert_id();

    INSERT INTO objects(
                  name, object_type_id_fk, static)
    VALUES (in_newname, lastTypeId, 1);

    INSERT INTO attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    VALUES ('id', lastTypeId, 1);

    SET idPropertyId = last_insert_id();

    INSERT INTO attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    VALUES ('name', lastTypeId, 3);

    SET namePropertyId = last_insert_id();

    INSERT INTO attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    VALUES ('desc', lastTypeId, 3);

    SET descPropertyId = last_insert_id();

    INSERT INTO attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    VALUES ('period', lastTypeId, 3);

    SET periodPropertyId = last_insert_id();

    INSERT INTO attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    VALUES ('mac', lastTypeId, 7);

    SET macPropertyId = last_insert_id();

    INSERT INTO attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    VALUES ('location', lastTypeId, 3);

    SET locationPropertyId = last_insert_id();

    INSERT INTO attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    VALUES ('x', lastTypeId, 2);

    SET xPropertyId = last_insert_id();

    INSERT INTO attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    VALUES ('y', lastTypeId, 2);

    SET yPropertyId = last_insert_id();

    INSERT INTO attributes(
                  name, object_type_id_fk, attr_type_id_fk, historical)
    VALUES ('batteryCapacity', lastTypeId, 2, 1);

    SET capacityPropertyId = last_insert_id();

    INSERT INTO attributes(
                  name, object_type_id_fk, attr_type_id_fk, historical)
    VALUES ('remainingDays', lastTypeId, 1, 1);

    SET remainingDaysPropertyId = last_insert_id();

    INSERT INTO attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    VALUES ('platformId', lastTypeId, 1);

    SET platformIdPropertyId = last_insert_id();

    INSERT INTO attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    VALUES ('platformName', lastTypeId, 3);

    SET platformNamePropertyId = last_insert_id();

    INSERT INTO attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    VALUES ('nodeAppVersion', lastTypeId, 3);

    SET appPropertyId = last_insert_id();

    INSERT INTO attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    VALUES ('nodeOSVersion', lastTypeId, 3);

    SET osPropertyId = last_insert_id();

    INSERT INTO attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    VALUES ('nodeNwkVersion', lastTypeId, 3);

    SET nwkPropertyId = last_insert_id();

    INSERT INTO attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    VALUES ('status', lastTypeId, 1);

    SET statusPropertyId = last_insert_id();

    INSERT INTO attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    VALUES ('route', lastTypeId, 5);

    SET routePropertyId = last_insert_id();

    INSERT INTO attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    VALUES ('batteryOperated', lastTypeId, 1);

    SET batteryOperatedPropertyId = last_insert_id();

    INSERT INTO attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    VALUES ('program', lastTypeId, 3);

    SET programPropertyId = last_insert_id();   

    SET currentTime = UNIX_TIMESTAMP() * 1000;

    OPEN cur_nodes;

   main:
    LOOP
      FETCH cur_nodes
      INTO cv_old_id, cv_name, cv_desc, cv_period, cv_mac,
           cv_location, cv_x, cv_y, cv_platform_id, cv_platform_name,
           cv_app, cv_os, cv_nwk, cv_capacity, cv_capacity_timestamp,
           cv_remaining_days, cv_topology_id, cv_route;

      IF done
      THEN
        LEAVE main;
      END IF;

      INSERT INTO objects(object_type_id_fk)
      VALUES (lastTypeId);

      SET lastObjectId = last_insert_id();

      INSERT INTO TEMP_NODES
      VALUES (cv_old_id, lastObjectId, cv_topology_id, cv_route);

      CALL insert_int_value(
             lastObjectId, idPropertyId, currentTime, NULL);
      CALL insert_varchar_value(
             lastObjectId, namePropertyId, currentTime, cv_name);
      CALL insert_varchar_value(
             lastObjectId, descPropertyId, currentTime, cv_desc);
      CALL insert_varchar_value(
             lastObjectId, periodPropertyId, currentTime, cv_period);
      CALL insert_long_value(
             lastObjectId, macPropertyId, currentTime, cv_mac);
      CALL insert_varchar_value(
             lastObjectId, locationPropertyId, currentTime, cv_location);
      CALL insert_double_value(
             lastObjectId, xPropertyId, currentTime, cv_x);
      CALL insert_double_value(
             lastObjectId, yPropertyId, currentTime, cv_y);
      CALL insert_varchar_value(
             lastObjectId, appPropertyId, currentTime, cv_app);
      CALL insert_varchar_value(
             lastObjectId, osPropertyId, currentTime, cv_os);
      CALL insert_varchar_value(
             lastObjectId, nwkPropertyId, currentTime, cv_nwk);
      CALL insert_int_value(
             lastObjectId, remainingDaysPropertyId, currentTime, cv_remaining_days);
      CALL insert_double_value(
             lastObjectId, capacityPropertyId, cv_capacity_timestamp, cv_capacity);
      CALL insert_int_value(
             lastObjectId, statusPropertyId, currentTime, 0);
      call insert_int_value(
             lastObjectId, platformIdPropertyId, currentTime, cv_platform_id);
      call insert_varchar_value(
             lastObjectId, platformNamePropertyId, currentTime, cv_platform_name);
      -- by default WSNNODE is battery operated
      call insert_int_value(
             lastObjectId, batteryOperatedPropertyId, currentTime, 1);
      call insert_link_value(
             lastObjectId, routePropertyId, currentTime, null);

      IF cv_topology_id IS NOT NULL
      THEN
        set cp_topology_id = cv_topology_id;

        OPEN cur_node_relations;

       -- moving generic relations
       subloop:
        LOOP
          FETCH cur_node_relations INTO cv_parent_id;

          IF done
          THEN
            set done = 0;
            LEAVE subloop;
          END IF;

          -- new relation
          INSERT INTO object_parents(
                        parent_id_fk, object_id_fk)
          VALUES (cv_parent_id, lastObjectId);
        END LOOP;

        CLOSE cur_node_relations;

        OPEN cur_node_wsn_relations;

       -- moving wsn relations
       subloop_wsn:
        LOOP
          FETCH cur_node_wsn_relations INTO cv_parent_id;

          IF done
          THEN
            set done = 0;
            LEAVE subloop_wsn;
          END IF;

          set cv_parent_id =
                get_wsn_newid(
                  'NETWORK', cv_parent_id);

          -- relation between new node and new network
          INSERT INTO object_parents(
                        parent_id_fk, object_id_fk)
          VALUES (cv_parent_id, lastObjectId);
        END LOOP;

        CLOSE cur_node_wsn_relations;
      END IF;
    END LOOP;

    CLOSE cur_nodes;

    update value_obj_link l,
           temp_nodes temp
    set l.object_id = temp.new_id, l.object_type = in_newname
    where l.object_id = temp.old_id
    and   l.object_type = 'NODE';

    -- sets up battery operated to false for nodes which have no battery power source
    update valuei vi,
           obj_values ov
    set vi.value = 0
    where vi.value_id_fk = ov.value_id
    and   ov.object_id_fk in (select temp_node.new_id
                              from tbl_sensor_node n join temp_nodes temp_node on (n.node_logical_id = temp_node.old_id)
                              where exists
                                      (select 1
                                       from tbl_sensor s, tbl_sensor_network_topology t
                                       where t.node_logical_id_fk = n.node_logical_id
                                       and   t.sensor_id_fk = s.sensor_id
                                       and   s.type_id_fk in (6, 8, 12, 16, 19, 22, 26, 28, 30, 31)
                                       and   t.sensor_id_fk is not null))
    and   ov.attr_id_fk = batteryOperatedPropertyId;

    -- sets up remainingsdays to -1 for non battery operated nodes
    update valuei vi,
           obj_values ov
    set vi.value = -1
    where vi.value_id_fk = ov.value_id
    and ov.object_id_fk in (select temp_node.new_id
                              from tbl_sensor_node n join temp_nodes temp_node on (n.node_logical_id = temp_node.old_id)
                              where exists
                                      (select 1
                                       from tbl_sensor s, tbl_sensor_network_topology t
                                       where t.node_logical_id_fk = n.node_logical_id
                                       and   t.sensor_id_fk = s.sensor_id
                                       and   s.type_id_fk in (6, 8, 12, 16, 19, 22, 26, 28, 30, 31)
                                       and   t.sensor_id_fk is not null))
    and ov.attr_id_fk = remainingDaysPropertyId;

    -- sets up node's reference to routing device
 UPDATE value_obj_link vol,
       obj_values ov,
       tbl_nwk_route route,
       (SELECT node.node_physical_id AS id,
               'WSNNODE' AS type_name,
               tn.new_id AS oid
          FROM tbl_sensor_node node, temp_nodes tn
         WHERE node.node_logical_id = tn.old_id) allnodes,
       (SELECT node.node_physical_id AS id,
               'WSNNODE' AS type_name,
               tn.new_id AS oid
          FROM tbl_sensor_node node, temp_nodes tn
         WHERE node.node_logical_id = tn.old_id
        UNION ALL
        SELECT vl.value AS id,
               ot.name AS type_name,
               o.id AS oid
          FROM objects o
               JOIN object_types ot
                  ON (o.object_type_id_fk = ot.objtype_id)
               JOIN attributes a
                  ON (a.object_type_id_fk = ot.objtype_id)
               JOIN obj_values ov
                  ON (o.id = ov.object_id_fk AND ov.attr_id_fk = a.attr_id)
               JOIN valuel vl
                  ON (ov.value_id = vl.value_id_fk)
         WHERE ot.name = 'GATEWAY' AND a.name = 'uniqueId') AS allwsn
   SET vol.object_id = allwsn.oid, vol.object_type = allwsn.type_name
 WHERE     route.node_physical_id = allnodes.id
       AND allwsn.id = route.neighbor_physical_id
       AND ov.value_id = vol.value_id_fk
       AND ov.object_id_fk = allnodes.oid
       AND ov.attr_id_fk = routePropertyId;
      
  END|
DELIMITER ;