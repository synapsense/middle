DROP PROCEDURE IF EXISTS migrate_platforms;

DELIMITER | 
CREATE PROCEDURE migrate_platforms(in_newname VARCHAR(50))
  BEGIN
    DECLARE done                    INT DEFAULT 0;

    DECLARE lastTypeId              INT;
    DECLARE lastObjectId            INT;
    DECLARE namePropertyId          INT;
    DECLARE descPropertyId          INT;
    DECLARE iconPropertyId          INT;
    DECLARE sensePointsPropertyId   INT;
    DECLARE idPropertyId            INT;
        
    DECLARE lastObjectValueId       INT;
    
    DECLARE currentTime             LONG;

    DECLARE cv_old_id               INT;
    DECLARE cv_name                 VARCHAR(300);
    DECLARE cv_description          VARCHAR(300);
    DECLARE cv_icon                 VARCHAR(300);
    DECLARE cv_sensepoints          INT;

    DECLARE
      cur_platforms CURSOR FOR
        SELECT id, name, description, icon, sensepoints FROM tbl_core_platform;

    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

    INSERT INTO object_types(name)
    VALUES (in_newname);

    SET lastTypeId := last_insert_id();

    INSERT INTO objects(
                  name, object_type_id_fk, static)
    VALUES (in_newname, lastTypeId, 1);

    INSERT INTO attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    VALUES ('name', lastTypeId, 3);

    SET namePropertyId := last_insert_id();

    INSERT INTO attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    VALUES ('description', lastTypeId, 3);

    SET descPropertyId := last_insert_id();

    INSERT INTO attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    VALUES ('icon', lastTypeId, 3);

    SET iconPropertyId := last_insert_id();

    INSERT INTO attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    VALUES ('sensepoints', lastTypeId, 1);

    SET sensePointsPropertyId := last_insert_id();

    INSERT INTO attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    VALUES ('id', lastTypeId, 1);

    SET idPropertyId := last_insert_id();
    
    create table TEMP_PLATFORMS(
      old_id int, new_id int, primary key(old_id, new_id));

    SET currentTime = UNIX_TIMESTAMP() * 1000;

    OPEN cur_platforms;

   main:
    LOOP
      FETCH cur_platforms
      INTO cv_old_id, cv_name, cv_description, cv_icon, cv_sensepoints;

      IF done
      THEN
        LEAVE main;
      END IF;

      INSERT INTO objects(object_type_id_fk)
      VALUES (lastTypeId);

      SET lastObjectId = last_insert_id();

      insert into TEMP_PLATFORMS
      values (cv_old_id, lastObjectId);

      call insert_varchar_value(
             lastObjectId, namePropertyId, currentTime, cv_name);
      call insert_varchar_value(
             lastObjectId, descPropertyId, currentTime, cv_description);
      call insert_varchar_value(
             lastObjectId, iconPropertyId, currentTime, cv_icon);
      call insert_int_value(
             lastObjectId, sensePointsPropertyId, currentTime, cv_sensepoints);
      call insert_int_value(
             lastObjectId, idPropertyId, currentTime, cv_old_id);             
             
    END LOOP;

    CLOSE cur_platforms;
  END| 
DELIMITER ;