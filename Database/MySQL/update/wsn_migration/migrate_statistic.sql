DROP PROCEDURE IF EXISTS migrate_statistic;

DELIMITER |

CREATE PROCEDURE migrate_statistic()
BEGIN
    
  update tbl_network_statbase stat, temp_nodes node
  set stat.logical_id = node.new_id
  where stat.logical_id = node.old_id;
  
  update tbl_network_statnode stat, temp_nodes node
  set stat.logical_id = node.new_id
  where stat.logical_id = node.old_id;

END|

DELIMITER ;