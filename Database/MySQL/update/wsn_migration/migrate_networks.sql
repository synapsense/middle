DROP PROCEDURE IF EXISTS migrate_networks;

DELIMITER | 
CREATE PROCEDURE migrate_networks(in_newname VARCHAR(50))
  BEGIN
    DECLARE done                INT DEFAULT 0;

    DECLARE lastTypeId          INT;
    DECLARE lastObjectId        INT;

    DECLARE cp_topology_id      INT;

    DECLARE namePropertyId      INT;
    DECLARE descPropertyId      INT;
    DECLARE panIdPropertyId     INT;
    DECLARE versionPropertyId   INT;

    DECLARE lastObjectValueId   INT;
    DECLARE currentTime         LONG;

    DECLARE cv_old_id           INT;
    DECLARE cv_name             VARCHAR(300);
    DECLARE cv_desc             VARCHAR(300);
    DECLARE cv_panid            VARCHAR(300);
    DECLARE cv_version          VARCHAR(300);
    DECLARE cv_topology_id      INT;
    DECLARE cv_parent_id        INT;

    DECLARE
      cur_networks CURSOR FOR
        SELECT net.network_id, net.network_name, net.network_description, net.pan_id, net.network_version,
               topology.id
        FROM   tbl_sensor_network net
             left outer join
               tbl_sensor_network_topology topology
             on (net.network_id = topology.network_id_fk)
        WHERE topology.sensor_id_fk IS NULL
        AND   topology.node_logical_id_fk IS NULL;

    DECLARE
      cur_networks_relations CURSOR FOR
        SELECT object_id_fk
        FROM tbl_generic_wsn_assoc
        where topology_id_fk = cp_topology_id;

    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

    INSERT INTO object_types(name)
    VALUES (in_newname);

    SET lastTypeId := last_insert_id();

    INSERT INTO objects(
                  name, object_type_id_fk, static)
    VALUES (in_newname, lastTypeId, 1);

    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('name', lastTypeId, 3);

    set namePropertyId = last_insert_id();

    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('desc', lastTypeId, 3);

    set descPropertyId = last_insert_id();

    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('panId', lastTypeId, 1);

    set panIdPropertyId = last_insert_id();

    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('version', lastTypeId, 3);

    set versionPropertyId = last_insert_id();
  
    SET currentTime = UNIX_TIMESTAMP() * 1000;

    OPEN cur_networks;

   main:
    LOOP
      FETCH cur_networks
      INTO cv_old_id, cv_name, cv_desc, cv_panid, cv_version, cv_topology_id;

      IF done
      THEN
        LEAVE main;
      END IF;

      INSERT INTO objects(object_type_id_fk)
      VALUES (lastTypeId);

      SET lastObjectId = last_insert_id();

      insert into TEMP_NETWORKS
      values (cv_old_id, lastObjectId, cv_topology_id);

      call insert_varchar_value(
             lastObjectId, namePropertyId, currentTime, cv_name);
      call insert_varchar_value(
             lastObjectId, descPropertyId, currentTime, cv_desc);
      call insert_int_value(
             lastObjectId, panIdPropertyId, currentTime, cv_panid);
      call insert_varchar_value(
             lastObjectId, versionPropertyId, currentTime, cv_version);

      IF cv_topology_id IS NOT NULL
      THEN
        set cp_topology_id = cv_topology_id;

        OPEN cur_networks_relations;

       subloop:
        LOOP
          FETCH cur_networks_relations INTO cv_parent_id;

          IF done
          THEN
            set done = 0;
            LEAVE subloop;
          END IF;

          -- new relation
          insert into object_parents(
                        parent_id_fk, object_id_fk)
          values (cv_parent_id, lastObjectId);
        END LOOP;

        CLOSE cur_networks_relations;
      END IF;
    END LOOP;

    CLOSE cur_networks;
    
  update value_obj_link l , temp_networks temp
  set l.object_id = temp.new_id , l.object_type = in_newname
  where l.object_id = temp.old_id
  and l.object_type = 'NETWORK';
  
  END|
DELIMITER ;