use synap;

select value_id_fk into @value_id from valuev where value = 'Circular dependency in CRE rule' limit 1;
select object_id_fk into @object_id from obj_values where value_id = @value_id limit 1;
select object_type_id_fk into @object_type from objects where id = @object_id limit 1;

select attr_id into @attr from attributes where object_type_id_fk = @object_type and name = 'name' limit 1;
update valuev set value = 'Critical system alert' where value_id_fk = (select value_id from obj_values where object_id_fk = @object_id and attr_id_fk = @attr);

select attr_id into @attr from attributes where object_type_id_fk = @object_type and name = 'displayName' limit 1;
update valuev set value = 'Critical system alert' where value_id_fk = (select value_id from obj_values where object_id_fk = @object_id and attr_id_fk = @attr);

select attr_id into @attr from attributes where object_type_id_fk = @object_type and name = 'description' limit 1;
update valuev set value = 'This alert is triggered when a critical error preventing a system component from functioning has occurred' where value_id_fk = (select value_id from obj_values where object_id_fk = @object_id and attr_id_fk = @attr);
