use `synap`;
ALTER TABLE `tbl_alert` ADD COLUMN `user_id_fk` INT AFTER `alert_type_id_fk`;
ALTER TABLE `tbl_alert` ADD COLUMN `acknowledgement` VARCHAR(300) AFTER `user_id_fk`;
ALTER TABLE `tbl_alert` ADD COLUMN `ack_time` datetime AFTER `acknowledgement`;

ALTER TABLE `tbl_alert` ADD CONSTRAINT `alert_dismissed_by` FOREIGN KEY `alert_dismissed_by`(`user_id_fk`)
    REFERENCES `tbl_users` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION;