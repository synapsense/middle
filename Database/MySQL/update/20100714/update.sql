use `synap`;

DELIMITER $$

# 'CRAC_CRAC:1__supplyT_0'->'CRAC:1->supplyT'
# 'CRAC_$_CRAC:1__supplyT_0'->'CRAC:1->supplyT'
# 'SENSOR:2'->'SENSOR:2->lastValue'
DROP FUNCTION IF EXISTS `synap`.`convertProperty` $$
CREATE FUNCTION `synap`.`convertProperty` (str VARCHAR(300)) RETURNS VARCHAR(300)
BEGIN
    DECLARE id VARCHAR(300);
    DECLARE propName VARCHAR(300);
    DECLARE tmp VARCHAR(300);
    SET tmp=SUBSTRING_INDEX(str, "__", 1); #should be like "CRAC_CRAC:1" or "CRAC_$_CRAC:1"
    #Now try to split by "_$_"
    SET id= SUBSTRING_INDEX(tmp, "_$_", -1);
    IF id=tmp THEN
      #try to split by "_"
      SET id= SUBSTRING_INDEX(tmp, "_", -1);
    END IF;
    SET propName=SUBSTRING_INDEX(SUBSTRING_INDEX(str, "__", -1),"_",1) ;
    IF propName=id THEN
      SET propName="lastValue" ;
    END IF;
    RETURN CONCAT(id, "->", propName);
END $$

DROP FUNCTION IF EXISTS `synap`.`convertCustomQuery` $$
CREATE FUNCTION `synap`.`convertCustomQuery` (str VARCHAR(21833)) RETURNS VARCHAR(21833)
BEGIN
    DECLARE left_token VARCHAR(300);
    DECLARE result VARCHAR(21833) DEFAULT "";
    DECLARE delim VARCHAR(1) DEFAULT ",";

    #split by "," in the loop
    ins_loop: LOOP
      SET left_token=SUBSTRING_INDEX(str, delim, 1);
      SET result=CONCAT(result,synap.convertProperty(left_token),",");
      IF length(left_token)=length(str)  THEN
        LEAVE ins_loop;
      END  IF;

      SET str=REPLACE(str, CONCAT(left_token, delim), "");
    END LOOP;
    RETURN SUBSTRING(result, 1, LENGTH(result)-1);
END $$

# 'CRAC_CRAC:1__supplyT_0[1,5];CRAC_CRAC:2__supplyT_0[1,6]'->'[[1,5],[1,6]]'
DROP FUNCTION IF EXISTS `synap`.`convertLineDescrs` $$
CREATE FUNCTION `synap`.`convertLineDescrs` (str VARCHAR(21833)) RETURNS VARCHAR(21833)
BEGIN
    DECLARE left_token VARCHAR(300);
    DECLARE result VARCHAR(21833) DEFAULT "";
    DECLARE delim VARCHAR(1) DEFAULT ";";

    #split by ";" in the loop
    ins_loop: LOOP
      SET left_token=SUBSTRING_INDEX(str, delim, 1);
      SET result=CONCAT(result,CONCAT("[",SUBSTRING_INDEX(left_token, "[", -1)),",");
      IF length(left_token)=length(str)  THEN
        LEAVE ins_loop;
      END  IF;

      SET str=REPLACE(str, CONCAT(left_token, delim), "");
    END LOOP;
    RETURN CONCAT("[",SUBSTRING(result, 1, LENGTH(result)-1),"]");
END $$

DELIMITER ;

UPDATE valuev,obj_values,attributes,object_types SET valuev.value=convertCustomQuery(valuev.value) where value_id_fk=obj_values.value_id and obj_values.attr_id_fk=attributes.attr_id and attributes.name="associatedSensors" and attributes.object_type_id_fk=object_types.objtype_id and object_types.name="CUSTOMQUERY";
UPDATE valuev,obj_values,attributes,object_types SET valuev.value=convertLineDescrs(valuev.value) where value_id_fk=obj_values.value_id and obj_values.attr_id_fk=attributes.attr_id and attributes.name="lines" and attributes.object_type_id_fk=object_types.objtype_id and object_types.name="CUSTOMQUERY";

DROP FUNCTION IF EXISTS `synap`.`convertProperty`;
DROP FUNCTION IF EXISTS `synap`.`convertCustomQuery`;
DROP FUNCTION IF EXISTS `synap`.`convertLineDescrs`;
