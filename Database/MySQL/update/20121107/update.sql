use synap;

-- create type 'GEO'
INSERT INTO object_types(name) VALUES ('GEO');

SET @lastTypeId := last_insert_id();

INSERT INTO objects(name, object_type_id_fk, static) VALUES ('GEO', @lastTypeId, 1);
	
insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('name', @lastTypeId, 3);
insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('dcsData', @lastTypeId, 3);
insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('map', @lastTypeId, 3);
insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('x', @lastTypeId, 2);
insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('y', @lastTypeId, 2);
insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('status', @lastTypeId, 3);
insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('image', @lastTypeId, 8);
insert into attributes(name, historical, object_type_id_fk, attr_type_id_fk) values ('minPue', 1, @lastTypeId, 2);
insert into attributes(name, historical, object_type_id_fk, attr_type_id_fk) values ('maxPue', 1, @lastTypeId, 2);
insert into attributes(name, historical, object_type_id_fk, attr_type_id_fk) values ('avgPue', 1, @lastTypeId, 2);
insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('minPueDC', @lastTypeId, 3);
insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('minPueDCName', @lastTypeId, 3);
insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('maxPueDC', @lastTypeId, 3);
insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('maxPueDCName', @lastTypeId, 3);
insert into attributes(name, historical, object_type_id_fk, attr_type_id_fk) values ('maxItPower', 1, @lastTypeId, 2);
insert into attributes(name, historical, object_type_id_fk, attr_type_id_fk) values ('minItPower', 1, @lastTypeId, 2);
insert into attributes(name, historical, object_type_id_fk, attr_type_id_fk) values ('minCoolingPower', 1, @lastTypeId, 2);
insert into attributes(name, historical, object_type_id_fk, attr_type_id_fk) values ('maxCoolingPower', 1, @lastTypeId, 2);
insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('minCoolingPowerDC', @lastTypeId, 3);
insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('minCoolingPowerDCName', @lastTypeId, 3);
insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('maxCoolingPowerDC', @lastTypeId, 3);
insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('maxCoolingPowerDCName', @lastTypeId, 3);
insert into attributes(name, historical, object_type_id_fk, attr_type_id_fk) values ('avgIntakeTemp', 1, @lastTypeId, 2);
insert into attributes(name, historical, object_type_id_fk, attr_type_id_fk) values ('minCracDeltaT', 1, @lastTypeId, 2);
insert into attributes(name, historical, object_type_id_fk, attr_type_id_fk) values ('maxCracDeltaT', 1, @lastTypeId, 2);
insert into attributes(name, historical, object_type_id_fk, attr_type_id_fk) values ('avgCracDeltaT', 1, @lastTypeId, 2);
insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('numDCs', @lastTypeId, 1);
