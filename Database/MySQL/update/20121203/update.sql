use synap;

DROP FUNCTION IF EXISTS insert_object_value;
DELIMITER | 
CREATE FUNCTION insert_object_value(
  in_object_id int, in_property_id int, in_timestamp long)
  RETURNS INTEGER
  BEGIN
    set @id=0;
    SELECT value_id into @id 
        from obj_values
        where object_id_fk=in_object_id and attr_id_fk=in_property_id limit 1;
        
    IF @id!=0
    THEN 
        RETURN @id;
    END IF;
  
    INSERT INTO obj_values(
                  object_id_fk, attr_id_fk, TIMESTAMP)
    VALUES (in_object_id, in_property_id, in_timestamp);

    RETURN last_insert_id();
  END|
DELIMITER ; 

DROP PROCEDURE IF EXISTS insert_varchar_value;

DELIMITER |
CREATE PROCEDURE insert_varchar_value(
  in_object_id int, in_property_id int, in_timestamp long, valuev varchar(20000))
  BEGIN
    DECLARE valueId   INT;

    SET valueId =
          insert_object_value(
            in_object_id, in_property_id, in_timestamp);

    INSERT INTO valuev(
                  value_id_fk, VALUE)
    VALUES (valueId, valuev);
  END|

DELIMITER ;

DROP PROCEDURE IF EXISTS insert_double_value;

DELIMITER |

CREATE PROCEDURE insert_double_value(
  in_object_id int, in_property_id int, in_timestamp long, valued double) 
  BEGIN
    DECLARE valueId   int;

    SET valueId =
          insert_object_value(
            in_object_id, in_property_id, in_timestamp);

    INSERT INTO valued(
                  value_id_fk, value)
    VALUES (valueId, valued);

  END|

DELIMITER ;

DROP PROCEDURE IF EXISTS insert_int_value;

DELIMITER |
CREATE PROCEDURE insert_int_value(
  in_object_id int, in_property_id int, in_timestamp long, valuei int)
  BEGIN
    DECLARE valueId   int;

    SET valueId =
          insert_object_value(
            in_object_id, in_property_id, in_timestamp);

    INSERT INTO valuei(
                  value_id_fk, value)
    VALUES (valueId, valuei);
  END|
DELIMITER ;

DROP PROCEDURE IF EXISTS insert_long_value;

DELIMITER |
CREATE PROCEDURE insert_long_value(
  in_object_id int, in_property_id int, in_timestamp long, valuel long)
  BEGIN
    DECLARE valueId   int;

    SET valueId =
          insert_object_value(
            in_object_id, in_property_id, in_timestamp);

    INSERT INTO valuel(
                  value_id_fk, value)
    VALUES (valueId, valuel);
  END|
DELIMITER ;

DROP PROCEDURE IF EXISTS insert_link_value;

DELIMITER |
CREATE PROCEDURE insert_link_value(
  in_object_id int, in_property_id int, in_timestamp long, value_link int)
  BEGIN
    DECLARE valueId   int;
    DECLARE typeName  varchar(300);
    
    SET valueId =
          insert_object_value(
            in_object_id, in_property_id, in_timestamp);
            
    IF (value_link IS NULL) THEN
      INSERT INTO value_obj_link(
                  value_id_fk, object_id,object_type)
      VALUES (valueId, NULL, NULL);
    ELSE
      SELECT ot.NAME INTO typeName FROM objects o , object_types ot WHERE o.object_type_id_fk = ot.objtype_id
      AND o.id = value_link;

      INSERT INTO value_obj_link(value_id_fk, object_id, object_type)
      VALUES (valueId, value_link, typeName);

    END IF;
    
  END|
DELIMITER ;
DROP PROCEDURE IF EXISTS update_alert_types;
delimiter |
create procedure update_alert_types( TemplateNTLog int, templatesPropertyId int)
    begin
        declare objId int;
        declare done int;
        declare currentTime long;
        declare cur_alert_type cursor for select id from objects o, object_types ot where o.object_type_id_fk=ot.objtype_id and ot.name = 'ALERT_TYPE' and o.static = 0;
		
        DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;
		
		select UNIX_TIMESTAMP()*1000 into currentTime;
		
        open cur_alert_type;
	
        main:
        loop
            fetch cur_alert_type
                into objId;
            IF done
            THEN
                LEAVE main;
            END IF;    
            
            
			CALL insert_link_value(
             objId, templatesPropertyId, currentTime, TemplateNTLog);


        end loop;
			
        CLOSE cur_alert_type;
	end;|
delimiter ;
DROP PROCEDURE IF EXISTS update_priority;
delimiter |
create procedure update_priority(priorityTypeId int)
    begin
	
        declare objId int;
        declare done int;
		declare ntLog int;
		declare currentTime long;
        declare cur_priority cursor for select id from objects where object_type_id_fk=priorityTypeId and static = 0;
					
        DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

		select UNIX_TIMESTAMP()*1000 into currentTime;

        insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('ntLog', priorityTypeId, 1);
        set ntLog=last_insert_id();

        open cur_priority;

        main:
        loop
            fetch cur_priority
                into objId;
            IF done
            THEN
                LEAVE main;
            END IF;    
            call insert_int_value(objId, ntLog, currentTime, 0);

        end loop;
			
        CLOSE cur_priority;
	end;|
delimiter ;

start transaction;

select objtype_id into @objTypeId from `object_types` where name = 'ALERT_MESSAGE_TEMPLATE' limit 1;
select attr_id into @name_attr from attributes where object_type_id_fk = @objTypeId and name = 'name' limit 1;
select attr_id into @header_attr from attributes where object_type_id_fk = @objTypeId and name = 'header' limit 1;
select attr_id into @body_attr from attributes where object_type_id_fk = @objTypeId and name = 'body' limit 1;
select attr_id into @description_attr from attributes where object_type_id_fk = @objTypeId and name = 'description' limit 1;
select attr_id into @messagetype_attr from attributes where object_type_id_fk = @objTypeId and name = 'messagetype' limit 1;
select attr_id into @type_attr from attributes where object_type_id_fk = @objTypeId and name = 'type' limit 1;
select UNIX_TIMESTAMP()*1000 into @currentTime;

 INSERT INTO objects(object_type_id_fk)
        VALUES (@objTypeId);
        
        SET @TemplateNTLog1 = last_insert_id();
        
        CALL insert_varchar_value(
             @TemplateNTLog1, @name_attr, @currentTime, 'Windows Event Log System Notification');
		CALL insert_varchar_value(
             @TemplateNTLog1, @header_attr, @currentTime, '');
		CALL insert_varchar_value(
             @TemplateNTLog1, @body_attr, @currentTime, 'Alert Name: $ALERT_NAME$\nAlert Message: $message$');
		CALL insert_varchar_value(
             @TemplateNTLog1, @description_attr, @currentTime, 'A notification used in Windows Event Log display of system alert details');
		CALL insert_varchar_value(
             @TemplateNTLog1, @messagetype_attr, @currentTime, '6');
		CALL insert_int_value(
             @TemplateNTLog1, @type_attr, @currentTime, 1);
			 
 INSERT INTO objects(object_type_id_fk)
        VALUES (@objTypeId);
        
        SET @TemplateNTLog2 = last_insert_id();
        
        CALL insert_varchar_value(
             @TemplateNTLog2, @name_attr, @currentTime, 'Windows Event Log Notification');
		CALL insert_varchar_value(
             @TemplateNTLog2, @header_attr, @currentTime, '');
		CALL insert_varchar_value(
             @TemplateNTLog2, @body_attr, @currentTime, 'Alert Name: $ALERT_NAME$\nTrigger Value: $TRIGGER_DATA_VALUE$\nCurrent Value: $CURRENT_DATA_VALUE$');
		CALL insert_varchar_value(
             @TemplateNTLog2, @description_attr, @currentTime, 'A notification used in Windows Event Log display of alert details');
		CALL insert_varchar_value(
             @TemplateNTLog2, @messagetype_attr, @currentTime, '6');
		CALL insert_int_value(
             @TemplateNTLog2, @type_attr, @currentTime, 0);
	
	select objtype_id into @alertTypeId from `object_types` where name = 'ALERT_TYPE' limit 1;
	select attr_id into @templatesPropertyId from attributes where object_type_id_fk = @alertTypeId and name = 'templates' limit 1;
	call update_alert_types(@TemplateNTLog1, @templatesPropertyId);
    select objtype_id into @priorityTypeId from `object_types` where name = 'ALERT_PRIORITY' limit 1;
    call update_priority(@priorityTypeId);


commit;

DROP PROCEDURE IF EXISTS update_priority;
DROP PROCEDURE IF EXISTS update_alert_types;
DROP FUNCTION IF EXISTS insert_object_value;
DROP PROCEDURE IF EXISTS insert_varchar_value;
DROP PROCEDURE IF EXISTS insert_double_value;
DROP PROCEDURE IF EXISTS insert_int_value;
DROP PROCEDURE IF EXISTS insert_long_value;
DROP PROCEDURE IF EXISTS insert_link_value;

