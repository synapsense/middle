use `synap`;
CREATE TABLE `TAGS` (
  `tag_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `attr_id_FK` int(11) NOT NULL DEFAULT '0',
  `tag_type_id_FK` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY(`tag_id`),
  index `attr_id_index`(`attr_id_FK`),
  index `tag_type_index`(`tag_type_id_FK`),
  unique index `name_tag_uniq`(`name`, `attr_id_FK`)
)
ENGINE=INNODB
COMMENT = 'Tags of object''s properties ';

CREATE TABLE `TAG_VALUES` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT,
  `tag_id_FK` int(11) NOT NULL DEFAULT '0',
  `value` varchar(21840),
  `object_id_FK` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY(`value_id`),
  unique index `tag_uniq`(`tag_id_FK`, `object_id_FK`)
)
ENGINE=INNODB
COMMENT = 'Tag values table';

ALTER TABLE `TAGS` ADD 
  CONSTRAINT `tag has type` FOREIGN KEY (`tag_type_id_FK`)
    REFERENCES `ATTRIBUTES_TYPES`(`attr_type_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION;
    
ALTER TABLE `TAGS` ADD 
  CONSTRAINT `parent property reference` FOREIGN KEY (`attr_id_FK`)
    REFERENCES `ATTRIBUTES`(`attr_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION;

ALTER TABLE `TAG_VALUES` ADD 
  CONSTRAINT `parent object reference` FOREIGN KEY (`object_id_FK`)
    REFERENCES `OBJECTS`(`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION;

ALTER TABLE `TAG_VALUES` ADD 
  CONSTRAINT `tag descriptor reference` FOREIGN KEY (`tag_id_FK`)
    REFERENCES `TAGS`(`tag_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION;

