-- Bug#0000 commit#56c34117f65ef3c681d123c8efa5b73a1fef7ab0
-- The script removes obsolete LiveImaging schema from ES database

use synap;
DROP TABLE IF EXISTS `tbl_visualization_images`;
DROP TABLE IF EXISTS `tbl_visualization_legend_image`;
DROP TABLE IF EXISTS `tbl_visualization_legend_point`;
DROP TABLE IF EXISTS `tbl_visualization_legend`;
DROP TABLE IF EXISTS `tbl_visualization_servers`;