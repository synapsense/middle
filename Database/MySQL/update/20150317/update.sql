use synap;

-- [10035] - Change Battery Status Report to support new battery life formula

-- NOTE: This script has no effect if the report cannot be found.
-- NOTE: This script can be run multiple times without issues.

-- Determine the id of the report object
select object_id into @object_id from all_objects_props_values where type_name = 'TABULAR_TEMPLATE' and propname = 'name' and val = 'Battery Status Report';

-- Determine the id of the jsonString value
select val_id into @val_id from all_objects_props_values where object_id = @object_id && propname = 'jsonString';

-- Update the timestamp of the jsonString value
update obj_values set timestamp = unix_timestamp() * 1000 where value_id = @val_id;

-- Update the jsonString value
update valuev set value = '{
    "templateLinkedView": null,
    "templateName"      : "Battery Status Report",
    "sensorProperties"  : ["lastValue"],
    "siblingObjTypes"   : [],
    "columnTemplates"   : [
        {
            "propertyRelationType": "This",
            "title"               : "columnname/battery_status",
            "maxColumnCount"      : 10,
            "displayMode"         : "SimpleStatusIcon",
            "columnWidth"         : 90,
            "propClass"           : "classjava.lang.Integer",
            "propertyName"        : "batteryStatus",
            "propertyObjectType"  : "WSNNODE"
        },
        {
            "propertyRelationType": "This",
            "title"               : "columnname/node_name",
            "maxColumnCount"      : 10,
            "displayMode"         : "Simple",
            "columnWidth"         : 112,
            "propClass"           : "classjava.lang.String",
            "propertyName"        : "name",
            "propertyObjectType"  : "WSNNODE"
        },
        {
            "propertyRelationType": "This",
            "title"               : "object_property_wsnnode_location",
            "maxColumnCount"      : 10,
            "displayMode"         : "Simple",
            "columnWidth"         : 93,
            "propClass"           : "classjava.lang.String",
            "propertyName"        : "location",
            "propertyObjectType"  : "WSNNODE"
        },
        {
            "propertyRelationType": "Parent",
            "title"               : "columnname/zone",
            "maxColumnCount"      : 10,
            "displayMode"         : "Simple",
            "columnWidth"         : 125,
            "propClass"           : "classjava.lang.String",
            "propertyName"        : "name",
            "propertyObjectType"  : "ZONE"
        }
    ],
    "objType"           : "WSNNODE",
    "templateMenuItems" : [
        {"menuItemAlias": "Enable"},
        {"menuItemAlias": "Disable"},
        {"menuItemAlias": "DefineAlert"},
        {"menuItemAlias": "GoToDataAnalysis"},
        {"menuItemAlias": "SendData"},
        {"menuItemAlias": "Identify"},
        {"menuItemAlias": "Reset"},
        {"menuItemAlias": "ChangeCapacity"},
        {"menuItemAlias": "ShowNodeOnFloorplan"}
    ]
}' where value_id_fk = @val_id;


-- Delete any customizations/preferences saved for this report.
select preference_id into @preference_id from tbl_user_preferences where tbl_user_preferences.preference_name = 'Template_Battery Status Report';

-- Delete all user preferences related to Battery Status report.
delete from tbl_user_preferences_assoc where tbl_user_preferences_assoc.preference_id =  @preference_id;
delete from tbl_user_preferences where tbl_user_preferences.preference_id =  @preference_id;
