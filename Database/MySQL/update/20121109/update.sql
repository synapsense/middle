use synap;

-- create procedure for update users

drop procedure if exists migrate_users;

delimiter |

create procedure migrate_users()
begin
  declare done                                    int default 0;
  declare user_id                                 int(11);
  declare user_role                               varchar(300);

  declare
          cur_users cursor for
              select `u`.`user_id`, `r`.`role` 
                from tbl_users u 
                    join roles r 
                    on r.role_id = u.role_id_fk;

  DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;
  
  open cur_users;
  
  main:loop
        
    fetch cur_users into user_id, user_role;

    if done
    then
        leave main;
    end if;
    
	-- Administrators-1 Power-2 Standard-3
		if user_role = 'admin' then
			insert into user_groups(`usr_id`,`grp_id`) values (user_id,1);
		end if;
		if user_role = 'engineer' then
			insert into user_groups(`usr_id`,`grp_id`) values (user_id,2);
		end if;
		if user_role = 'user' then
			insert into user_groups(`usr_id`,`grp_id`) values (user_id,3);
		end if;

  end loop main;
	
  close cur_users;
  
end;|

delimiter ;

delimiter $$

-- drop all user group related tables, for safety 
DROP TABLE IF EXISTS `group_privileges`$$
DROP TABLE IF EXISTS `privileges_parents`$$
DROP TABLE IF EXISTS `privileges`$$
DROP TABLE IF EXISTS `user_groups`$$
DROP TABLE IF EXISTS `groups`$$

-- create user group related tables

CREATE TABLE `groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `description` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8$$


CREATE TABLE `privileges` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `description` varchar(300) DEFAULT NULL,
  `system` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8$$

CREATE TABLE `privileges_parents` (
  `privilege_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  PRIMARY KEY (`privilege_id`,`parent_id`),
  KEY `parent_id_fk_idx` (`parent_id`),
  CONSTRAINT `privilege_id_fk` FOREIGN KEY (`privilege_id`) REFERENCES `privileges` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `parent_id_fk` FOREIGN KEY (`parent_id`) REFERENCES `privileges` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8$$

CREATE TABLE `group_privileges` (
  `prv_id` int(11) NOT NULL,
  `grp_id` int(11) NOT NULL,
  PRIMARY KEY (`prv_id`,`grp_id`),
  KEY `fk_prv_id` (`prv_id`),
  KEY `fk1_grp_id` (`grp_id`),
  CONSTRAINT `fk1_grp_id` FOREIGN KEY (`grp_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_prv_id` FOREIGN KEY (`prv_id`) REFERENCES `privileges` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8$$

CREATE TABLE `user_groups` (
  `usr_id` int(11) NOT NULL,
  `grp_id` int(11) NOT NULL,
  PRIMARY KEY (`usr_id`,`grp_id`),
  KEY `fk_usr_id` (`usr_id`),
  KEY `fk_grp_id` (`grp_id`),
  CONSTRAINT `fk_grp_id` FOREIGN KEY (`grp_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_usr_id` FOREIGN KEY (`usr_id`) REFERENCES `tbl_users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8$$

delimiter ;

-- create groups
insert into groups (id,name) values (1,'Administrators');
insert into groups (id,name) values (2,'Power');
insert into groups (id,name) values (3,'Standard');

-- create privileges
insert into privileges (id, name, system) values (1,'root_permission',1);
insert into privileges (id, name, system) values (2,'get_data',1);
insert into privileges (id, name, system) values (3,'edit_data',1);
insert into privileges (id, name, system) values (4,'edit_meta_data',1);
insert into privileges (id, name, system) values (5,'get_user_information',1);
insert into privileges (id, name, system) values (6,'get_user_data',1);
insert into privileges (id, name, system) values (7,'edit_user_data',1);
insert into privileges (id, name, system) values (8,'edit_user_preferences',1);
insert into privileges (id, name, system) values (9,'read_file',1);
insert into privileges (id, name, system) values (10,'write_file',1);

insert into privileges (id, name, description, system) values (11,'ACCESS_PERMISSIONS', 'Access Permissons',6);
insert into privileges (id, name, description, system) values (12,'ADD_REMOVE_USER_PERMISSIONS', 'Add/Remove User Permissions',6);
insert into privileges (id, name, description, system) values (13,'VIEW_DATA_CENTER_PROPERTIES', 'View Data Center Properties',6);
insert into privileges (id, name, description, system) values (14,'EDIT_DATA_CENTER_PROPERTIES_GENERAL', 'Edit Data Center Properties General',6);
insert into privileges (id, name, description, system) values (15,'EDIT_DATA_CENTER_PROPERTIES_CONTACTS', 'Edit Data Center Properties Contacts',6);
insert into privileges (id, name, description, system) values (16,'EDIT_DATA_CENTER_PROPERTIES_UTILITY_COSTS', 'Edit Data Center Properties Utility Costs',6);
insert into privileges (id, name, description, system) values (17,'ACCESS_DASHBOARDS', 'Access Dashboards',6);
insert into privileges (id, name, description, system) values (18,'CREATE_EDIT_DELETE_OWN_PRIVATE_DASHBOARDS', 'Create/edit/delete own private dashboards',6);
insert into privileges (id, name, description, system) values (19,'CREATE_EDIT_DELETE_OWN_PUBLIC_DASHBOARDS', 'Create/edit/delete own public dashboards',6);
insert into privileges (id, name, description, system) values (20,'CREATE_EDIT_DELETE_ALL_DASHBOARDS', 'Create/edit/delete all dashboards',6);
insert into privileges (id, name, description, system) values (21,'ACCESS_DATA_ANALYSIS', 'Access Data Analysis',6);
insert into privileges (id, name, description, system) values (22,'PRINT_EXPORT_QUERY_RESULTS', 'Print/export query results',6);
insert into privileges (id, name, description, system) values (23,'PRINT_EXPORT_CHARTS', 'Print/export charts',6);
insert into privileges (id, name, description, system) values (24,'ADD_ANNOTATIONS', 'Add Annotations',6);
insert into privileges (id, name, description, system) values (25,'ADD_SYSTEM_ANNOTATIONS', 'Add System Annotations',6);
insert into privileges (id, name, description, system) values (26,'ACCESS_CUSTOM_QUERIES', 'Access Custom Queries',6);
insert into privileges (id, name, description, system) values (27,'CREATE_EDIT_DELETE_OWN_PRIVATE_QUERIES', 'Create/edit/delete own private custom queries',6);
insert into privileges (id, name, description, system) values (28,'CREATE_EDIT_DELETE_OWN_PUBLIC_QUERIES', 'Create/edit/delete own public custom queries',6);
insert into privileges (id, name, description, system) values (29,'CREATE_EDIT_DELETE_ALL_QUERIES', 'Create/edit/delete all custom queries',6);
insert into privileges (id, name, description, system) values (30,'ACCESS_FIND_AVAILABLE_CAPACITY', 'Access Find Available Capacity',6);
insert into privileges (id, name, description, system) values (31,'ACCESS_CARBON_EMISSIONS_SUMMARY', 'Access Carbon Emissions Summary',6);
insert into privileges (id, name, description, system) values (32,'EDIT_CARBON_EMISSIONS_TARGET_VALUES', 'Edit Carbon Emissions Target Values',6);
insert into privileges (id, name, description, system) values (33,'PRINT_CARBON_EMISSIONS_SUMMARY', 'Print Carbon Emissions Summary',6);
insert into privileges (id, name, description, system) values (34,'ACCESS_NETWORK_STATISTICS', 'Access Network Statistics',6);
insert into privileges (id, name, description, system) values (35,'PRINT_EXPORT_NETWORK_STATISTICS', 'Print/Export Network Statistics',6);
insert into privileges (id, name, description, system) values (36,'ACCESS_ALERT_DEFINITIONS', 'Access Alert Definitions',6);
insert into privileges (id, name, description, system) values (37,'CREATE_EDIT_ALERT_DEFINITIONS', 'Create/edit Alert Definitions',6);
insert into privileges (id, name, description, system) values (38,'ENABLE_DISABLE_ALERT_DEFINITIONS', 'Enable/Disable Alert Definitions',6);
insert into privileges (id, name, description, system) values (39,'DELETE_ALERT_DEFINITIONS', 'Delete Alert Definitions',6);
insert into privileges (id, name, description, system) values (40,'ACCESS_NOTIFICATION_TEMPLATES', 'Access Notification Templates',6);
insert into privileges (id, name, description, system) values (41,'CREATE_EDIT_NOTIFICATION_TEMPLATES', 'Create/edit Notification Templates',6);
insert into privileges (id, name, description, system) values (42,'DELETE_NOTIFICATION_TEMPLATES', 'Delete Notification Templates',6);
insert into privileges (id, name, description, system) values (43,'ACCESS_ALL_ALERTS', 'Access All Alerts',6);
insert into privileges (id, name, description, system) values (44,'ACKNOWLEDGE_AND_RESOLVE_ALERTS', 'Acknowledge and Resolve Alerts',6);
insert into privileges (id, name, description, system) values (45,'DISMISS_ALERTS', 'Dismiss Alerts',6);
insert into privileges (id, name, description, system) values (46,'ACCESS_DELETE_ALL_ALERTS', 'Access Delete All Alerts',6);
insert into privileges (id, name, description, system) values (47,'ACCESS_MANAGE_USERS', 'Access Manage Users',6);
insert into privileges (id, name, description, system) values (48,'CREATE_EDIT_USERS', 'Create/edit users',6);
insert into privileges (id, name, description, system) values (49,'DELETE_USERS', 'Delete users',6);
insert into privileges (id, name, description, system) values (50,'ACCESS_MANAGE_ASSET_GROUPS', 'Access Manage Asset Groups',6);
insert into privileges (id, name, description, system) values (51,'CREATE_EDIT_ASSET_GROUPS', 'Create/edit asset groups',6);
insert into privileges (id, name, description, system) values (52,'DELETE_GROUPS', 'Delete groups',6);
insert into privileges (id, name, description, system) values (55,'ACCESS_CONFIGURATION', 'Access Configuration',6);
insert into privileges (id, name, description, system) values (56,'EDIT_CONFIGURATION_SETTINGS', 'Edit Configuration Settings',6);
insert into privileges (id, name, description, system) values (57,'ACCESS_RECONCILE_ASSET_NAMES', 'Access Reconcile Asset Names',6);
insert into privileges (id, name, description, system) values (58,'ACCESS_SYSTEM_ACTIVITY_LOG', 'Access System Activity Log',6);
insert into privileges (id, name, description, system) values (59,'PRINT_EXPORT_SYSTEM_ACTIVITY_LOG', 'Print/export System Activity Log',6);
insert into privileges (id, name, description, system) values (60,'ACCESS_SYSTEM_CONFIGURATION_REPORT', 'Access System Configuration Report',6);
insert into privileges (id, name, description, system) values (61,'PRINT_EXPORT_SYSTEM_CONFIGURATION_REPORT', 'Print/export System Configuration Report',6);
insert into privileges (id, name, description, system) values (62,'ACCESS_MODBUS_POINTS_LIST', 'Access Modbus Points List',6);
insert into privileges (id, name, description, system) values (64,'GENERATE_DIAGNOSTIC_SUPPORT_PACKAGE', 'Generate Diagnostic Support Package',6);
insert into privileges (id, name, description, system) values (65,'ACCESS_PUE_SUMMARY', 'Access PUE Summary',6);
insert into privileges (id, name, description, system) values (66,'ACCESS_PUE_DETAILS', 'Access PUE Details',6);
insert into privileges (id, name, description, system) values (67,'ACCESS_FLOORPLAN', 'Access Floorplan',6);
insert into privileges (id, name, description, system) values (68,'ACCESS_WSN_NETWORK_FLOORPLAN', 'Access WSN Network floorplan',6);
insert into privileges (id, name, description, system) values (69,'SEND_WSN_COMMANDS_TO_NODES', 'Send WSN commands to nodes',6);
insert into privileges (id, name, description, system) values (70,'ACCESS_ACTIVE_CONTROL_FLOORPLAN', 'Access active control performance data',6);
insert into privileges (id, name, description, system) values (72,'EDIT_ACTIVE_CONTROL_ALGORITHM_PROPERTIES', 'Edit active control operational parameters',6);
insert into privileges (id, name, description, system) values (73,'EDIT_ACTIVE_CONTROL_TARGET_VALUES', 'Edit active control target values',6);
insert into privileges (id, name, description, system) values (76,'ACCESS_ENVIRONMENTALS_FLOORPLAN', 'Access Environmentals floorplan',6);
insert into privileges (id, name, description, system) values (77,'ENABLE_DISABLE_OBJECTS', 'Enable/Disable Objects',6);
insert into privileges (id, name, description, system) values (78,'ACCESS_POWERIMAGING_FLOORPLAN', 'Access PowerImaging floorplan',6);
insert into privileges (id, name, description, system) values (79,'EDIT_POWERIMAGING_PARAMETERS', 'Edit PowerImaging parameters',6);
insert into privileges (id, name, description, system) values (80,'ENABLE_LIVEIMAGING_LAYER', 'Enable LiveImaging layer',6);
insert into privileges (id, name, description, system) values (81,'ACCESS_LIVEIMAGING_ADVANCED', 'Access LiveImaging Advanced',6);
insert into privileges (id, name, description, system) values (82,'PRINT_EXPORT_LIVEIMAGING_IMAGES', 'Print/Export LiveImaging images',6);
insert into privileges (id, name, description, system) values (83,'ACCESS_PREDEFINED_METRICS', 'Access Pre-Defined Metrics',6);
insert into privileges (id, name, description, system) values (84,'ACCESS_CUSTOM_METRICS', 'Access Custom Metrics',6);
insert into privileges (id, name, description, system) values (85,'ACCESS_PDU_VIEW', 'Access PDU view',6);
insert into privileges (id, name, description, system) values (86,'ACCESS_BCMS_VIEW', 'Access BCMS view',6);
insert into privileges (id, name, description, system) values (87,'EDIT_BCMS_CIRCUIT_NAMES', 'Edit BCMS circuit names',6);
insert into privileges (id, name, description, system) values (88,'ACCESS_DATA_VIEWS', 'Access Data Views',6);
insert into privileges (id, name, description, system) values (89,'CREATE_EDIT_DELETE_OWN_PRIVATE_TEMPLATES', 'Create/edit/delete own private templates',6);
insert into privileges (id, name, description, system) values (90,'CREATE_EDIT_DELETE_OWN_PUBLIC_TEMPLATES', 'Create/edit/delete own public templates',6);
insert into privileges (id, name, description, system) values (91,'CREATE_EDIT_DELETE_ALL_TEMPLATES', 'Create/edit/delete all templates',6);
insert into privileges (id, name, description, system) values (92,'RESTORE_DEFAULT_TEMPLATES', 'Restore default templates',6);
insert into privileges (id, name, description, system) values (93,'PRINT_EXPORT_DATA_VIEWS', 'Print/export data views',6);
insert into privileges (id, name, description, system) values (94,'ACCESS_WEBCONSOLE', 'Access webconsole',6);
insert into privileges (id, name, description, system) values (97,'ACCESS_VIEW_ES', 'Access View Environment Servers',4);
insert into privileges (id, name, description, system) values (100,'EXPORT_FROM_MAPSENSE', 'Allow export from MapSense',6);
insert into privileges (id, name, description, system) values (101,'MANAGE_NODES', 'Manage Nodes',6);

-- create privilage parent relations
INSERT INTO privileges_parents(`privilege_id`,`parent_id`) VALUES(3,11);
INSERT INTO privileges_parents(`privilege_id`,`parent_id`) VALUES(7,12);
INSERT INTO privileges_parents(`privilege_id`,`parent_id`) VALUES(3,14);
INSERT INTO privileges_parents(`privilege_id`,`parent_id`) VALUES(3,15);
INSERT INTO privileges_parents(`privilege_id`,`parent_id`) VALUES(3,16);
INSERT INTO privileges_parents(`privilege_id`,`parent_id`) VALUES(3,18);
INSERT INTO privileges_parents(`privilege_id`,`parent_id`) VALUES(3,19);
INSERT INTO privileges_parents(`privilege_id`,`parent_id`) VALUES(3,20);
INSERT INTO privileges_parents(`privilege_id`,`parent_id`) VALUES(3,27);
INSERT INTO privileges_parents(`privilege_id`,`parent_id`) VALUES(4,27);
INSERT INTO privileges_parents(`privilege_id`,`parent_id`) VALUES(3,28);
INSERT INTO privileges_parents(`privilege_id`,`parent_id`) VALUES(4,28);
INSERT INTO privileges_parents(`privilege_id`,`parent_id`) VALUES(3,29);
INSERT INTO privileges_parents(`privilege_id`,`parent_id`) VALUES(3,32);
INSERT INTO privileges_parents(`privilege_id`,`parent_id`) VALUES(3,37);
INSERT INTO privileges_parents(`privilege_id`,`parent_id`) VALUES(3,38);
INSERT INTO privileges_parents(`privilege_id`,`parent_id`) VALUES(3,39);
INSERT INTO privileges_parents(`privilege_id`,`parent_id`) VALUES(3,41);
INSERT INTO privileges_parents(`privilege_id`,`parent_id`) VALUES(3,42);
INSERT INTO privileges_parents(`privilege_id`,`parent_id`) VALUES(3,44);
INSERT INTO privileges_parents(`privilege_id`,`parent_id`) VALUES(3,45);
INSERT INTO privileges_parents(`privilege_id`,`parent_id`) VALUES(3,46);
INSERT INTO privileges_parents(`privilege_id`,`parent_id`) VALUES(3,47);
INSERT INTO privileges_parents(`privilege_id`,`parent_id`) VALUES(7,47);
INSERT INTO privileges_parents(`privilege_id`,`parent_id`) VALUES(7,48);
INSERT INTO privileges_parents(`privilege_id`,`parent_id`) VALUES(7,49);
INSERT INTO privileges_parents(`privilege_id`,`parent_id`) VALUES(3,51);
INSERT INTO privileges_parents(`privilege_id`,`parent_id`) VALUES(4,51);
INSERT INTO privileges_parents(`privilege_id`,`parent_id`) VALUES(3,52);
INSERT INTO privileges_parents(`privilege_id`,`parent_id`) VALUES(3,53);
INSERT INTO privileges_parents(`privilege_id`,`parent_id`) VALUES(3,54);
INSERT INTO privileges_parents(`privilege_id`,`parent_id`) VALUES(3,56);
INSERT INTO privileges_parents(`privilege_id`,`parent_id`) VALUES(10,56);
INSERT INTO privileges_parents(`privilege_id`,`parent_id`) VALUES(3,57);
INSERT INTO privileges_parents(`privilege_id`,`parent_id`) VALUES(10,63);
INSERT INTO privileges_parents(`privilege_id`,`parent_id`) VALUES(3,71);
INSERT INTO privileges_parents(`privilege_id`,`parent_id`) VALUES(3,72);
INSERT INTO privileges_parents(`privilege_id`,`parent_id`) VALUES(3,73);
INSERT INTO privileges_parents(`privilege_id`,`parent_id`) VALUES(3,77);
INSERT INTO privileges_parents(`privilege_id`,`parent_id`) VALUES(3,79);
INSERT INTO privileges_parents(`privilege_id`,`parent_id`) VALUES(3,87);
INSERT INTO privileges_parents(`privilege_id`,`parent_id`) VALUES(3,89);
INSERT INTO privileges_parents(`privilege_id`,`parent_id`) VALUES(3,90);
INSERT INTO privileges_parents(`privilege_id`,`parent_id`) VALUES(3,91);
INSERT INTO privileges_parents(`privilege_id`,`parent_id`) VALUES(3,92);
INSERT INTO privileges_parents(`privilege_id`,`parent_id`) VALUES(2,94);
INSERT INTO privileges_parents(`privilege_id`,`parent_id`) VALUES(5,94);
INSERT INTO privileges_parents(`privilege_id`,`parent_id`) VALUES(6,94);
INSERT INTO privileges_parents(`privilege_id`,`parent_id`) VALUES(8,94);
INSERT INTO privileges_parents(`privilege_id`,`parent_id`) VALUES(9,94);
INSERT INTO privileges_parents(`privilege_id`,`parent_id`) VALUES(2,95);
INSERT INTO privileges_parents(`privilege_id`,`parent_id`) VALUES(5,95);
INSERT INTO privileges_parents(`privilege_id`,`parent_id`) VALUES(6,95);
INSERT INTO privileges_parents(`privilege_id`,`parent_id`) VALUES(8,95);
INSERT INTO privileges_parents(`privilege_id`,`parent_id`) VALUES(3,96);
INSERT INTO privileges_parents(`privilege_id`,`parent_id`) VALUES(3,98);
INSERT INTO privileges_parents(`privilege_id`,`parent_id`) VALUES(2,100);
INSERT INTO privileges_parents(`privilege_id`,`parent_id`) VALUES(3,100);
INSERT INTO privileges_parents(`privilege_id`,`parent_id`) VALUES(4,100);
INSERT INTO privileges_parents(`privilege_id`,`parent_id`) VALUES(3,101);
INSERT INTO privileges_parents(`privilege_id`,`parent_id`) VALUES(3,102);

-- create group privileges relations
insert into group_privileges(`prv_id`,`grp_id`) values (1,1);
insert into group_privileges(`prv_id`,`grp_id`) values (13,2);
insert into group_privileges(`prv_id`,`grp_id`) values (13,3);
insert into group_privileges(`prv_id`,`grp_id`) values (14,2);
insert into group_privileges(`prv_id`,`grp_id`) values (15,2);
insert into group_privileges(`prv_id`,`grp_id`) values (17,2);
insert into group_privileges(`prv_id`,`grp_id`) values (17,3);
insert into group_privileges(`prv_id`,`grp_id`) values (18,2);
insert into group_privileges(`prv_id`,`grp_id`) values (18,3);
insert into group_privileges(`prv_id`,`grp_id`) values (19,2);
insert into group_privileges(`prv_id`,`grp_id`) values (21,2);
insert into group_privileges(`prv_id`,`grp_id`) values (21,3);
insert into group_privileges(`prv_id`,`grp_id`) values (22,2);
insert into group_privileges(`prv_id`,`grp_id`) values (22,3);
insert into group_privileges(`prv_id`,`grp_id`) values (23,2);
insert into group_privileges(`prv_id`,`grp_id`) values (23,3);
insert into group_privileges(`prv_id`,`grp_id`) values (24,2);
insert into group_privileges(`prv_id`,`grp_id`) values (25,2);
insert into group_privileges(`prv_id`,`grp_id`) values (26,2);
insert into group_privileges(`prv_id`,`grp_id`) values (26,3);
insert into group_privileges(`prv_id`,`grp_id`) values (27,2);
insert into group_privileges(`prv_id`,`grp_id`) values (27,3);
insert into group_privileges(`prv_id`,`grp_id`) values (28,2);
insert into group_privileges(`prv_id`,`grp_id`) values (30,2);
insert into group_privileges(`prv_id`,`grp_id`) values (30,3);
insert into group_privileges(`prv_id`,`grp_id`) values (31,2);
insert into group_privileges(`prv_id`,`grp_id`) values (31,3);
insert into group_privileges(`prv_id`,`grp_id`) values (32,2);
insert into group_privileges(`prv_id`,`grp_id`) values (33,2);
insert into group_privileges(`prv_id`,`grp_id`) values (33,3);
insert into group_privileges(`prv_id`,`grp_id`) values (34,2);
insert into group_privileges(`prv_id`,`grp_id`) values (34,3);
insert into group_privileges(`prv_id`,`grp_id`) values (35,2);
insert into group_privileges(`prv_id`,`grp_id`) values (35,3);
insert into group_privileges(`prv_id`,`grp_id`) values (36,2);
insert into group_privileges(`prv_id`,`grp_id`) values (37,2);
insert into group_privileges(`prv_id`,`grp_id`) values (38,2);
insert into group_privileges(`prv_id`,`grp_id`) values (43,2);
insert into group_privileges(`prv_id`,`grp_id`) values (43,3);
insert into group_privileges(`prv_id`,`grp_id`) values (44,2);
insert into group_privileges(`prv_id`,`grp_id`) values (44,3);
insert into group_privileges(`prv_id`,`grp_id`) values (45,2);
insert into group_privileges(`prv_id`,`grp_id`) values (45,3);
insert into group_privileges(`prv_id`,`grp_id`) values (64,2);
insert into group_privileges(`prv_id`,`grp_id`) values (64,3);
insert into group_privileges(`prv_id`,`grp_id`) values (65,2);
insert into group_privileges(`prv_id`,`grp_id`) values (65,3);
insert into group_privileges(`prv_id`,`grp_id`) values (67,2);
insert into group_privileges(`prv_id`,`grp_id`) values (67,3);
insert into group_privileges(`prv_id`,`grp_id`) values (70,2);
insert into group_privileges(`prv_id`,`grp_id`) values (70,3);
insert into group_privileges(`prv_id`,`grp_id`) values (71,2);
insert into group_privileges(`prv_id`,`grp_id`) values (72,2);
insert into group_privileges(`prv_id`,`grp_id`) values (73,2);
insert into group_privileges(`prv_id`,`grp_id`) values (74,2);
insert into group_privileges(`prv_id`,`grp_id`) values (75,2);
insert into group_privileges(`prv_id`,`grp_id`) values (76,2);
insert into group_privileges(`prv_id`,`grp_id`) values (76,3);
insert into group_privileges(`prv_id`,`grp_id`) values (77,2);
insert into group_privileges(`prv_id`,`grp_id`) values (78,2);
insert into group_privileges(`prv_id`,`grp_id`) values (78,3);
insert into group_privileges(`prv_id`,`grp_id`) values (80,2);
insert into group_privileges(`prv_id`,`grp_id`) values (80,3);
insert into group_privileges(`prv_id`,`grp_id`) values (81,2);
insert into group_privileges(`prv_id`,`grp_id`) values (81,3);
insert into group_privileges(`prv_id`,`grp_id`) values (82,2);
insert into group_privileges(`prv_id`,`grp_id`) values (82,3);
insert into group_privileges(`prv_id`,`grp_id`) values (83,2);
insert into group_privileges(`prv_id`,`grp_id`) values (83,3);
insert into group_privileges(`prv_id`,`grp_id`) values (84,2);
insert into group_privileges(`prv_id`,`grp_id`) values (84,3);
insert into group_privileges(`prv_id`,`grp_id`) values (85,2);
insert into group_privileges(`prv_id`,`grp_id`) values (85,3);
insert into group_privileges(`prv_id`,`grp_id`) values (86,2);
insert into group_privileges(`prv_id`,`grp_id`) values (86,3);
insert into group_privileges(`prv_id`,`grp_id`) values (87,2);
insert into group_privileges(`prv_id`,`grp_id`) values (88,2);
insert into group_privileges(`prv_id`,`grp_id`) values (88,3);
insert into group_privileges(`prv_id`,`grp_id`) values (89,2);
insert into group_privileges(`prv_id`,`grp_id`) values (89,3);
insert into group_privileges(`prv_id`,`grp_id`) values (90,2);
insert into group_privileges(`prv_id`,`grp_id`) values (93,2);
insert into group_privileges(`prv_id`,`grp_id`) values (93,3);
insert into group_privileges(`prv_id`,`grp_id`) values (94,2);
insert into group_privileges(`prv_id`,`grp_id`) values (94,3);
insert into group_privileges(`prv_id`,`grp_id`) values (95,2);
insert into group_privileges(`prv_id`,`grp_id`) values (95,3);
insert into group_privileges(`prv_id`,`grp_id`) values (101,2);
insert into group_privileges(`prv_id`,`grp_id`) values (102,2);

-- create user group relations
call migrate_users();
drop procedure if exists migrate_users;

-- delete column role from user table
ALTER TABLE `synap`.`tbl_users` DROP FOREIGN KEY `Ref_81` ;
ALTER TABLE `synap`.`tbl_users` DROP COLUMN `role_id_fk`, DROP INDEX `Ref_81` ;

-- drop role table
drop table if exists `roles`;