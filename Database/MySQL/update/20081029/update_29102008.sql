-- the update of db schema objects.
-- the property 'name' is not moved to proper structure (tables : attributes , obj_value , valuev) it must be done by migration tool.
-- use synap schema
USE `synap`;
SET AUTOCOMMIT=0;
SET FOREIGN_KEY_CHECKS=0;

DROP INDEX idx_name ON `objects`;
-- new non unique index on name field
CREATE INDEX idx_name ON `objects` (`name`);

-- remove old unique index
-- drop FK on objects
alter table `objects` DROP FOREIGN KEY db_prototype_new_db_prototype_Ref_01;
-- remove old unique index
DROP INDEX type_name_uniq ON `objects`;

alter table `objects` ADD CONSTRAINT FK_object_type FOREIGN KEY  idx_fk_object_type (`object_type_id_fk`) references `object_types`(`objtype_id`);

ALTER TABLE `objects` MODIFY COLUMN `name` VARCHAR(128) CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT 'Unique object name in boundaries of type';

-- destinations must be unique
truncate table `tbl_destinations`;
DROP INDEX idx_name_uniq ON `tbl_destinations`;
CREATE UNIQUE INDEX idx_name_uniq ON `tbl_destinations` (`dest_name`);

-- not necessary table
DROP TABLE IF EXISTS `tbl_performer_destination_assoc`;
-- not necessary table
DROP TABLE IF EXISTS `tbl_performer_alert_type_assoc`;

-- new table for ternary association alert type - action - destination ()
-- drops old if already exists
DROP TABLE IF EXISTS `TBL_ALERT_TYPE_ACTION_DEST_ASSOC`;

-- create new table
CREATE TABLE `TBL_ALERT_TYPE_ACTION_DEST_ASSOC` (
  `alert_type_id_FK` int(11) NOT NULL DEFAULT '0',
  `performer_id_FK` int(11) NOT NULL DEFAULT '0',
  `dest_id_FK` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY(`alert_type_id_FK`, `performer_id_FK`, `dest_id_FK`)
)
ENGINE=INNODB
COMMENT = 'Alert type - action - destination assoc';


-- FKs on TBL_ALERT_TYPE_ACTION_DEST_ASSOC table
ALTER TABLE `TBL_ALERT_TYPE_ACTION_DEST_ASSOC` ADD
  CONSTRAINT `Ref_83` FOREIGN KEY (`dest_id_FK`)
    REFERENCES `TBL_DESTINATIONS`(`dest_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION;

ALTER TABLE `TBL_ALERT_TYPE_ACTION_DEST_ASSOC` ADD
  CONSTRAINT `Ref_84` FOREIGN KEY (`performer_id_FK`)
    REFERENCES `TBL_PERFORMERS`(`performer_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION;

ALTER TABLE `TBL_ALERT_TYPE_ACTION_DEST_ASSOC` ADD
  CONSTRAINT `Ref_85` FOREIGN KEY (`alert_type_id_FK`)
    REFERENCES `TBL_ALERT_TYPE`(`alert_type_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION;

SET FOREIGN_KEY_CHECKS=1;