To update your existing database, please, do following (do it only one time):
 
1) Create dump of your database (just in case J )

2) Execute �update_29102008.sql� script 

Important note: execute this script from �MySQL query browser� (do NOT use usual command line), since some minor errors could be here (we couldn�t quickly get rid of it). It is needed just to skip errors and continue (usual command line breaks in this case and doesn�t reach end of script).
 
Following scripts should not contain errors; you can execute it from any place:

3) Execute �createProcedure.sql� script (it will create necessary procedure)

4) Execute �copyNameProperties.sql� script (it will create property �name� for each object and appropriately fill it).
