
use synap;

DROP PROCEDURE IF EXISTS copyObjectNames;
delimiter //

CREATE PROCEDURE copyObjectNames()
BEGIN
   DECLARE num INT DEFAULT 5;
   DECLARE isCreated INT DEFAULT 5;
   DECLARE attrTypeId INT DEFAULT 5;
   DECLARE new_attr_id INT DEFAULT 5;
   DECLARE new_value_id INT DEFAULT 5;
   DECLARE obj_id INT DEFAULT 5;
   DECLARE obj_type_id INT DEFAULT 5;
   DECLARE obj_name VARCHAR(128);
   DECLARE done INT default 0;

   DECLARE curs CURSOR FOR select id, name, object_type_id_FK from objects;
   DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;

   select count(*) INTO num from objects;
   select attr_type_id INTO attrTypeId from attributes_types where java_type="java.lang.String";

        OPEN curs;
        REPEAT FETCH curs INTO obj_id, obj_name, obj_type_id;
                IF  num > 0 THEN

                   -- if is not created, insert
                   select count(*) INTO isCreated from attributes where name="name" and object_type_id_FK = obj_type_id;
                   IF isCreated <= 0 THEN
                      INSERT INTO attributes (name,historical, object_type_id_FK, attr_type_id_FK, collection, static) VALUES ('name',0, obj_type_id, attrTypeId, 0, 0);
                   END IF;

                   select attr_id INTO new_attr_id from attributes where name="name" and object_type_id_FK = obj_type_id;
                   INSERT INTO obj_values(timestamp, object_id_FK, attr_id_FK, version) VALUES (0, obj_id, new_attr_id, 0);

                   select value_id INTO new_value_id from obj_values where object_id_FK = obj_id and attr_id_FK = new_attr_id;
                   INSERT INTO valuev(value, value_id_FK) VALUES (obj_name, new_value_id);

                   SET num = num - 1;
               END IF;
         UNTIL done END REPEAT;
	 CLOSE curs;

END//
delimiter ;