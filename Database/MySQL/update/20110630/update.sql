use `synap`;

drop function if exists tmp_number_format_remover;

DELIMITER $$

CREATE FUNCTION tmp_number_format_remover(in_source TEXT) RETURNS text

    NO SQL

BEGIN

	DECLARE result TEXT;
	DECLARE temp_source TEXT;
	DECLARE temp_next_char varchar(1);
	DECLARE idx_begin INT default 0;
	DECLARE idx_loop INT default 0;
	DECLARE idx_temp INT default 1; 
	
	SET idx_begin = LOCATE('numberFormatter.format(',in_source) + 23;

  if idx_begin <=23 then 
    return in_source;
  end if;

  set result = in_source;

  WHILE (idx_begin > 23) DO

 	 SET temp_source = SUBSTRING(result,idx_begin);

	  WHILE (idx_temp<>0 and idx_loop < LENGTH(temp_source)) DO

		SET temp_next_char = SUBSTRING(temp_source,idx_loop,1);

   		IF temp_next_char = '(' THEN

   			SET idx_temp = idx_temp+1;

   		ELSEIF 	temp_next_char = ')' THEN 

   			SET idx_temp = idx_temp-1;

   		END IF;

   		SET idx_loop = idx_loop + 1;

     	END WHILE;

	SET temp_source = INSERT(temp_source,idx_loop-1,1,'');

 	SET result = INSERT(result,idx_begin-23,LENGTH(result),temp_source);

  SET idx_begin = LOCATE('numberFormatter.format(',result) + 23;

  END WHILE;

 RETURN result;

END$$

DELIMITER ;

update tbl_rule_types 

set source = tmp_number_format_remover(replace(source,'global java.text.NumberFormat numberFormatter;',''))

where source regexp 'numberFormatter';

drop function if exists tmp_number_format_remover;

