use synap;

-- [10017] Some DRE rules fail compilation and are discarded
update tbl_rule_types set source = replace(source, '$obj0', 'obj0') where source_type = 'drools';