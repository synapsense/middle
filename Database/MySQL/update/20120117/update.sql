use synap;

DROP procedure IF EXISTS drop_constraint;

delimiter |
create procedure drop_constraint( in_table varchar(128), in_column varchar(128))
    begin
    declare const_name varchar(128);
       
       SELECT  
         c.constraint_name into const_name 
          FROM information_schema.table_constraints AS c 
          INNER JOIN information_schema.key_column_usage AS u 
          USING( constraint_schema, constraint_name ) 
          WHERE c.constraint_type = 'FOREIGN KEY' 
            and c.table_schema='synap' 
            and u.table_name=in_table 
            and u.column_name=in_column 
          ORDER BY u.table_schema,u.table_name,u.column_name;
        
        if const_name is not null then
            SET @sql_text = concat('ALTER TABLE `',in_table,'` DROP FOREIGN KEY `',const_name,'`');
            PREPARE stmt FROM @sql_text;
            EXECUTE stmt;
            DEALLOCATE PREPARE stmt;
        end if;
    end|
delimiter ;

SET FOREIGN_KEY_CHECKS=0;

call drop_constraint('OBJECTS','object_type_id_fk');
call drop_constraint('OBJ_VALUES','object_id_fk');
call drop_constraint('OBJ_VALUES','attr_id_fk');
call drop_constraint('valueD','value_id_fk');
call drop_constraint('valueI','value_id_fk');
call drop_constraint('valueV','value_id_fk');
call drop_constraint('valueDt','value_id_fk');
call drop_constraint('VALUE_OBJ_LINK','value_id_fk');
call drop_constraint('valueL','value_id_fk');
call drop_constraint('ATTRIBUTES','object_type_id_fk');
call drop_constraint('OBJECT_PARENTS','object_id_fk');
call drop_constraint('OBJECT_PARENTS','parent_id_fk');
call drop_constraint('VALUE_PROPERTY_LINK','value_id_fk');
call drop_constraint('value_binary','value_id_fk');
call drop_constraint('tags','attr_id_fk');
call drop_constraint('tag_values','tag_id_fk');
call drop_constraint('tag_values','object_id_fk');

ALTER TABLE `OBJECTS` ADD
  CONSTRAINT `object has type` FOREIGN KEY (`object_type_id_FK`)
    REFERENCES `OBJECT_TYPES`(`objtype_id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION;

ALTER TABLE `OBJ_VALUES` ADD
  CONSTRAINT `Ref_30` FOREIGN KEY (`object_id_FK`)
    REFERENCES `OBJECTS`(`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION;

ALTER TABLE `OBJ_VALUES` ADD
  CONSTRAINT `Ref_33` FOREIGN KEY (`attr_id_FK`)
    REFERENCES `ATTRIBUTES`(`attr_id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION;

ALTER TABLE `valueD` ADD
  CONSTRAINT `Ref_59` FOREIGN KEY (`value_id_FK`)
    REFERENCES `OBJ_VALUES`(`value_id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION;

ALTER TABLE `valueI` ADD
  CONSTRAINT `Ref_68` FOREIGN KEY (`value_id_FK`)
    REFERENCES `OBJ_VALUES`(`value_id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION;

ALTER TABLE `valueV` ADD
  CONSTRAINT `Ref_70` FOREIGN KEY (`value_id_FK`)
    REFERENCES `OBJ_VALUES`(`value_id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION;

ALTER TABLE `valueDt` ADD
  CONSTRAINT `Ref_69` FOREIGN KEY (`value_id_FK`)
    REFERENCES `OBJ_VALUES`(`value_id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION;

ALTER TABLE `VALUE_OBJ_LINK` ADD
  CONSTRAINT `Ref_56` FOREIGN KEY (`value_id_FK`)
    REFERENCES `OBJ_VALUES`(`value_id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION;

ALTER TABLE `valueL` ADD
  CONSTRAINT `Ref_49` FOREIGN KEY (`value_id_FK`)
    REFERENCES `OBJ_VALUES`(`value_id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION;

ALTER TABLE `ATTRIBUTES` ADD
  CONSTRAINT `attribute has type` FOREIGN KEY (`object_type_id_FK`)
    REFERENCES `OBJECT_TYPES`(`objtype_id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION;

ALTER TABLE `OBJECT_PARENTS` ADD
  CONSTRAINT `Ref_48` FOREIGN KEY (`object_id_FK`)
    REFERENCES `OBJECTS`(`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION;

ALTER TABLE `OBJECT_PARENTS` ADD
  CONSTRAINT `Ref_58` FOREIGN KEY (`parent_id_fk`)
    REFERENCES `OBJECTS`(`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION;

ALTER TABLE `VALUE_PROPERTY_LINK` ADD
  CONSTRAINT `Ref_57` FOREIGN KEY (`value_id_FK`)
    REFERENCES `OBJ_VALUES`(`value_id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION;

ALTER TABLE `value_binary` ADD
  CONSTRAINT `root_value_reference` FOREIGN KEY (`value_id_FK`)
    REFERENCES `OBJ_VALUES`(`value_id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION;

ALTER TABLE `tags` ADD
  CONSTRAINT `tag_descriptor_relates_to_attribute` FOREIGN KEY (`attr_id_FK`)
    REFERENCES `ATTRIBUTES`(`attr_id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION;

ALTER TABLE `tag_values` ADD
  CONSTRAINT `tag_has_descriptor` FOREIGN KEY (`tag_id_FK`)
    REFERENCES `tags`(`tag_id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION;

ALTER TABLE `tag_values` ADD
  CONSTRAINT `tag_relates_to_object` FOREIGN KEY (`object_id_FK`)
    REFERENCES `OBJECTS`(`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION;

SET FOREIGN_KEY_CHECKS=1;
DROP procedure IF EXISTS drop_constraint;