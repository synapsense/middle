use synap;

update tbl_sensor_node set node_sample_frequency = null where node_sample_frequency < 0 or sample_frequency_units is null;
update tbl_sensor_node set sample_frequency_units = null where sample_frequency_units not in (1,2,3) or node_sample_frequency is null;