use synap;

drop procedure if exists update_alert_type_ref;
delimiter |
-- change alert type reference value and hostTO reference value of alert history from value_obj_link to valuei
create procedure update_alert_type_ref()
begin
    
    declare done                                    int default 0;

    declare alert_hist_obj_id                       int;

    declare 
            cur_alert_hist_id cursor for 
                select `o`.`id` from object_types as ot 
                    join objects as o on ot.objtype_id=o.object_type_id_fk 
                    where ot.name = 'ALERT_HISTORY' 
                        and o.static = 0 ;

    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;
    
    open cur_alert_hist_id;
    
    main:loop
    
        fetch cur_alert_hist_id into alert_hist_obj_id;
            
        if done
        then
            close cur_alert_hist_id;
            leave main;
        end if;
        
        select `ov`.`value_id`, `vi`.`value` into @type_val_id, @type_val from object_types as ot 
                    join objects as o on ot.objtype_id=o.object_type_id_fk 
                    join attributes as attr on attr.object_type_id_fk = ot.objtype_id
                    left join obj_values as ov on o.id = ov.object_id_fk and attr.attr_id = ov.attr_id_fk
                    left join valuei as vi on ov.value_id=vi.value_id_fk 
                    where ot.name = 'ALERT_HISTORY' 
                        and o.static = 0 
                        and attr.name = 'type' 
                        and o.id = alert_hist_obj_id;
                        
        select `ov`.`value_id`, `vi`.`value` into @hostTO_val_id, @hostTO_val from object_types as ot 
                    join objects as o on ot.objtype_id=o.object_type_id_fk 
                    join attributes as attr on attr.object_type_id_fk = ot.objtype_id
                    left join obj_values as ov on o.id = ov.object_id_fk and attr.attr_id = ov.attr_id_fk
                    left join valuei as vi on ov.value_id=vi.value_id_fk 
                    where ot.name = 'ALERT_HISTORY' 
                        and o.static = 0 
                        and attr.name = 'hostTO' 
                        and o.id = alert_hist_obj_id;
        
        if @type_val_id is not null then
            if @type_val is null then
                select object_id into @alert_type_obj_id from value_obj_link where value_id_fk = @type_val_id;
                delete from value_obj_link where value_id_fk = @type_val_id;
                INSERT INTO valuei(value_id_fk, VALUE) VALUES (@type_val_id, @alert_type_obj_id);
            end if;
        end if;
            
        if @hostTO_val_id is not null then 
            if @hostTO_val is null then
                select object_id into @alert_hostTO_obj_id from value_obj_link where value_id_fk = @hostTO_val_id;
                delete from value_obj_link where value_id_fk = @hostTO_val_id;
                INSERT INTO valuei(value_id_fk, VALUE) VALUES (@hostTO_val_id, @alert_hostTO_obj_id);
            end if;
        end if;
    
    end loop main;
            
end;|

delimiter ;

-- execute procedure

start transaction;
  call update_alert_type_ref();
        
-- renew ids after update
SET @max_object_id := IFNULL((SELECT max(`id`)+1 FROM `objects`),1);
SET @max_value_id := IFNULL((SELECT max(`value_id`)+1 FROM `obj_values`),1);
SET @max_valued_id := IFNULL((SELECT max(`id`)+1 FROM `valued`),1);
SET @max_valuel_id := IFNULL((SELECT max(`id`)+1 FROM `valuel`),1);
SET @max_valuei_id := IFNULL((SELECT max(`id`)+1 FROM `valuei`),1);
SET @max_valuev_id := IFNULL((SELECT max(`id`)+1 FROM `valuev`),1);
SET @max_valuelink_id := IFNULL((SELECT max(`id`)+1 FROM `value_obj_link`),1);
SET @max_valueplink_id := IFNULL((SELECT max(`id`)+1 FROM `value_property_link`),1);
SET @max_tag_value_id := IFNULL((SELECT max(`value_id`)+1 FROM `tag_values`),1);
SET @max_group_value_id := IFNULL((SELECT max(`group_id`)+1 FROM `historical_values`),1);


delete from uid_table;
INSERT INTO `uid_table` (`object_id`,`value_id`,`valued_id`,`valuev_id`,`valuei_id`,`valuel_id`,`valuelink_id`,`valueplink_id`, `tag_value_id`, `group_id`) values (@max_object_id,@max_value_id,@max_valued_id,@max_valuev_id,@max_valuei_id,@max_valuel_id,@max_valuelink_id,@max_valueplink_id,@max_tag_value_id,@max_group_value_id);

commit;

drop procedure if exists update_alert_type_ref;