use `synap`;
-- Drop View all_objects_props_values
DROP VIEW IF EXISTS `all_objects_props_values`;

CREATE VIEW `all_objects_props_values` AS
select a.name as propname ,o.name as object_name ,ot.name as type_name, v.value_id as val_id , o.id as object_id, ot.objtype_id as type_id,
case
when vv.value is not null then vv.value
when vi.value is not null then vi.value
when vd.value is not null then trim(both '0' from cast(vd.value as char))
when vdt.value is not null then vdt.value
when vl.value is not null then vl.value
when vol.object_id is not null then concat(vol.object_type,':',vol.object_id)
when vpl.object_id is not null then concat(vpl.object_type,':',vpl.object_id,':',vpl.property_name)
else null
end as val
from
objects o inner join object_types ot on (ot.objtype_id = o.object_type_id_fk )
left outer join obj_values v on (o.id = v.object_id_fk)
left outer  join attributes a on (a.attr_id = v.attr_id_fk)
left outer join  valuev vv on (v.value_id = vv.value_id_fk)
left outer join  valuei vi on (v.value_id = vi.value_id_fk)
left outer join  valued vd on (v.value_id = vd.value_id_fk)
left outer join  valuedt vdt on (v.value_id = vdt.value_id_fk)
left outer join  valuel vl on (v.value_id = vl.value_id_fk)
left outer join  value_obj_link vol on (v.value_id = vol.value_id_fk)
left outer join  value_property_link vpl on (v.value_id = vpl.value_id_fk)
where
o.static = 0;