SELECT distinct DATE_FORMAT(DATE_SUB(from_days(p.partition_description), INTERVAL 1 day),'%m/%e/%Y') start_date
FROM information_schema.PARTITIONS P
where p.table_schema = 'synap'
and p.table_name = 'tbl_sensor_data'
and p.partition_ordinal_position =
(select distinct(min(P_sub.partition_ordinal_position))
  from information_schema.PARTITIONS P_sub where P_sub.table_schema = 'synap'
  and P_sub.table_name = 'tbl_sensor_data'
);
