@echo off

set mysqluser=%1

set password=%2

set db_schema=%3

set err_code=0

if not exist historical_event.sql (
	echo historical_event.sql file is missing
	set err_code=1
	goto exit
)

rem option -d says to mysqldump tool to retrieve only create table statement with no data
mysqldump -u%mysqluser% -p%password% %db_schema% historical_values -d > historical.sql
if ERRORLEVEL 1 ( 
	echo Failed to create historical_values table dump
	goto error
)

 
if not exist historical.sql (
	echo historical.sql dump file is missing
        set err_code=1
	goto exit
)

mysql -u%mysqluser% -p%password% %db_schema% -e "alter table historical_values rename historical_values_mars;"
if ERRORLEVEL 1 ( 
	echo Failed to rename table historical_values 
	goto error
)

mysql -u%mysqluser% -p%password% %db_schema% < historical.sql
if ERRORLEVEL 1 ( 
	echo Failed to create new table historical_values
	goto error
)

mysql -u%mysqluser% -p%password% %db_schema% < historical_event.sql
if ERRORLEVEL 1 ( 
	echo Failed to execute historical_event.sql
	goto error
)


goto success

:error
echo Failed to alter historical_values. Fix problems and try again
set err_code=1
goto exit

:success
echo Historical_values table update is done
set err_code=0

:exit
del historical.sql
exit /B %err_code%