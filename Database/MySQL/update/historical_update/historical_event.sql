use synap;

-- create auxiliary procedure for safety
DROP PROCEDURE IF EXISTS `DROP_PARTITION`;

delimiter |
CREATE PROCEDURE DROP_PARTITION(IN schemaName VARCHAR(50), IN tableName VARCHAR(50), IN partitionName VARCHAR(50)) 
begin
-- dynamic instruction to drop partition with the specified name (last partiotion is no more necessary)
set @preparedStmtDrop = concat('ALTER TABLE ',schemaName,'.',tableName,' drop partition ',partitionName);
PREPARE drop_last_partition from @preparedStmtDrop;
EXECUTE drop_last_partition;
DEALLOCATE PREPARE drop_last_partition;
end|

delimiter ;

drop table if exists `temp_counter`;
     
-- create counter
create table `temp_counter` (
    `id` int(11) NOT NULL ,
    `count` int(11) DEFAULT '0',
    PRIMARY KEY (`id`));

-- add group_id field to historical_values table
ALTER TABLE `historical_values` 
    ADD COLUMN `group_id` INT(11) DEFAULT NULL  AFTER `valuev` , 
    CHANGE COLUMN `valuev` `valuev` VARCHAR(21822) DEFAULT NULL;

-- copy historical data for current day
INSERT INTO `historical_values` 
        select *, NULL
            from `historical_values_mars` 
            where timestamp >= DATE(NOW()) and timestamp < DATE_ADD(DATE(NOW()), INTERVAL 1 DAY);

-- set partition descr for current day
set @last_partition_descr := TO_DAYS(DATE(NOW())+1);

-- activePartitions number counts partitions which already have data , it should be >= 1
-- getting partition of current day
select partition_name, PARTITION_ORDINAL_POSITION into @pname , @activePartitionsNumber
        from INFORMATION_SCHEMA.PARTITIONS 
        where TABLE_NAME='historical_values_mars' 
            and TABLE_SCHEMA='synap' 
            and partition_description=@last_partition_descr limit 1;

-- create procedure for check partition of the current day
-- if we found it: drop it and set counter to the next day
-- else looking for first existed partition and set counter to the day of this partition
drop procedure if exists get_last_partition;
delimiter |
create procedure get_last_partition()
begin
    if @pname is not null 
    then
        call DROP_PARTITION('synap', 'historical_values_mars', @pname);
        insert into `temp_counter` value (1, @last_partition_descr - 1);
    else
        while @pname is null do
            set @last_partition_descr := @last_partition_descr - 1;

            select partition_name, PARTITION_ORDINAL_POSITION into @pname , @activePartitionsNumber
            from INFORMATION_SCHEMA.PARTITIONS 
                where TABLE_NAME='historical_values_mars' 
                and TABLE_SCHEMA='synap' 
                and partition_description=@last_partition_descr limit 1;
        end while;
        set @activePartitionsNumber := @activePartitionsNumber + 1;
        insert into `temp_counter` value (1, @last_partition_descr);
    end if;

end|
delimiter ;

call get_last_partition();
drop procedure if exists get_last_partition;

drop event if exists change_historical_tbl_event;

set @start_time=timestamp(DATE(NOW()+ interval 1 HOUR),TIME_FORMAT(TIME(NOW()+ interval 1 HOUR),'%H:12:00'));

delimiter |

create event change_historical_tbl_event
    on schedule every '30' minute 
        starts @start_time+interval 30 minute 
        ends @start_time + interval 30*(@activePartitionsNumber-1)+1 minute
    enable
do begin
main1:begin
        
    select `count` into @days from `temp_counter` limit 1;
    
    select partition_name, PARTITION_ORDINAL_POSITION into @pname, @ordinalPosition 
        from INFORMATION_SCHEMA.PARTITIONS 
        where TABLE_NAME='historical_values_mars' 
            and TABLE_SCHEMA='synap' 
            and partition_description=@days limit 1;
        
    set @days:=@days-1;
    
    IF @ordinalPosition>1 THEN
    
        INSERT INTO `historical_values` 
            select *, NULL
                from `historical_values_mars` 
                 where timestamp >= FROM_DAYS(@days) and timestamp < DATE_ADD(FROM_DAYS(@days), INTERVAL 1 DAY);
        
    ELSEIF @ordinalPosition=1 THEN
    
        INSERT INTO `historical_values` 
                select *, NULL
                    from `historical_values_mars` 
                     where timestamp < DATE_ADD(FROM_DAYS(@days), INTERVAL 1 DAY);
    ELSE
    
        LEAVE main1;
    
    END IF;
            
    call DROP_PARTITION('synap', 'historical_values_mars', @pname);
   
    end main1;
    
    update `temp_counter`
        set `count`=@days
        where id=1;
        
    
end|

delimiter ;

select ends into @ends_date from INFORMATION_SCHEMA.EVENTS 
        where EVENT_SCHEMA='synap' and EVENT_NAME='change_historical_tbl_event' limit 1;

drop event if exists drop_historical_values_mars;

delimiter |

create event drop_historical_values_mars
    on schedule at @ends_date + interval 12 hour
    enable
    do
        begin
            drop table if exists `historical_values_mars`; 
            drop table if exists `temp_counter`;
        end;|
        
delimiter ;