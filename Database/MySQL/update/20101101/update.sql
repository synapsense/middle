delete from tbl_rule_instance_constants
where rule_id_fk in
  (select rule_id from tbl_rule_instances where rule_type_id_fk in
    (select rule_type_id from tbl_rule_types where source_type ='Groovy')
  );

delete from tbl_rule_property_dest_assoc
where rule_id_fk in
  (select rule_id from tbl_rule_instances where rule_type_id_fk in
    (select rule_type_id from tbl_rule_types where source_type ='Groovy')
  );

delete from tbl_rule_property_src_assoc
where rule_id_fk in
  (select rule_id from tbl_rule_instances where rule_type_id_fk in
    (select rule_type_id from tbl_rule_types where source_type ='Groovy')
  );


delete ri from tbl_rule_instances ri , tbl_rule_types rt
where ri.rule_type_id_fk = rt.rule_type_id
and  rt.source_type ='Groovy';

delete from tbl_alert_to_rules_map where
rule_type_id_fk in (select rule_type_id from tbl_rule_types where source_type ='Groovy');

-- Remove unnecessary links from attributes table
ALTER TABLE `synap`.`attributes` DROP FOREIGN KEY `Ref_71`;
ALTER TABLE `attributes` DROP COLUMN `rule_type_id_fk`;


delete from tbl_rule_types where source_type ='Groovy';
