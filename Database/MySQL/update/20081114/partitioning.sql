-- enables partitioning feature
-- can be executed even if db has some working data
use `synap`;
-- partitions table tbl_sensor_data
ALTER TABLE tbl_sensor_data PARTITION BY HASH(DAYOFMONTH(data_time_stamp)) PARTITIONS 31;
-- add here the more partition definitions