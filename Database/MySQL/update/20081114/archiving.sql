-- enables archivation event
-- can be executed even if db has some working data
use `synap`;
DROP EVENT IF EXISTS `archive_data`;
delimiter //
CREATE EVENT `archive_data`
ON SCHEDULE EVERY 1 DAY
STARTS TIMESTAMP(DATE(DATE_ADD(NOW(),INTERVAL 1 DAY)),'01:00')
DO

BEGIN

CREATE TABLE IF NOT EXISTS `ARCHIVE_SENSOR_DATA` (
  `sensor_id` int(11) COMMENT 'The data request id that triggered this collection',
  `sensor_node_data` double(15,6) COMMENT 'Actual value of the measurement captured by the sensor node sensor.',
  `data_time_stamp` timestamp(14) COMMENT 'The timestamp when the data was collected by the sensor node'
)
ENGINE=ARCHIVE;

INSERT INTO ARCHIVE_SENSOR_DATA ( `sensor_id`, `sensor_node_data`, `data_time_stamp`)
(select `sensor_id`, `sensor_node_data`, `data_time_stamp`
   from `tbl_sensor_data` 
   where `data_time_stamp` < DATE_SUB(NOW(),INTERVAL 30 DAY)
);

DELETE FROM `tbl_sensor_data` where `data_time_stamp` < DATE_SUB(NOW(),INTERVAL 30 DAY);
END//
delimiter ;
