use `synap`;

ALTER TABLE `tbl_sensor_data` DROP COLUMN `id`;

ALTER TABLE `tbl_sensor_data` DROP FOREIGN KEY `Sensordata_Sensor`;

ALTER TABLE `tbl_sensor_data`  ADD INDEX `idx_data_time_stamp` (`data_time_stamp`);

