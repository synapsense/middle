This update enables partitioning&archiving feature.
It is only for existing db schemas.
If you need to create new db schema please use the init_db.bat at database/deployment directory.

To update your existing db schema please run 'update.bat' file in current directory.

