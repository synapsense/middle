use synap;

ALTER TABLE tbl_sensor ADD COLUMN `firmwaretype` INTEGER COMMENT 'Sensor\'s firmware type : 0(ordinal), 1(smartsend), 2(averaging)' AFTER `visual_layer`,
 ADD COLUMN `subsamples` INTEGER COMMENT 'Number of subsamples for smartsend sensor' AFTER `firmwaretype`,
 ADD COLUMN `delta` INTEGER COMMENT 'Percentage' AFTER `subsamples`;
