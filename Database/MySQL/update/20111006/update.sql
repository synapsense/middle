use `synap`;

ALTER TABLE `tbl_alert_type` ADD COLUMN `auto_dismiss_allowed` SMALLINT(1) NOT NULL DEFAULT '1'  AFTER `auto_ack` ;


UPDATE `tbl_alert_type` SET `auto_dismiss_allowed` = 0 
	WHERE `name` IN ("UNKNOWN","Low battery","WSN_CONFIGURATION","Active Control Communication Alert",
			"Active Control Critical Alert","Active Control Overcooling Alert","Active Control Undercooling Alert","Active Control Sensors Not Reporting Alert","Active Control CRAH or CRAC is Offline Alert");
