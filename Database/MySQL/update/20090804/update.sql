use synap;
INSERT INTO `TBL_DATA_CLASS` (`data_class_id`,`data_class_name`,`data_class_description`,`is_scalar_or_complex_flg`,`data_class_minimum_value`,`data_class_maximum_value`,`data_class_precision`,`unit_id`) VALUES (214,'ESTATE','Equipment State',0,-0.1,1.1,0,999);
INSERT INTO `TBL_DATA_CLASS` (`data_class_id`,`data_class_name`,`data_class_description`,`is_scalar_or_complex_flg`,`data_class_minimum_value`,`data_class_maximum_value`,`data_class_precision`,`unit_id`) VALUES (999,'CLSLESS','Classless data - never displayed directly',0,-0.1,1.1,0,999);

INSERT INTO `TBL_SENSOR_TYPE` (`type_id`,`sensor_data_class_id`,`sensor_capability`, `sensor_name`) VALUES (34,999,"Senses Generic 4-20mA Inputs", "Milliamps");
INSERT INTO `TBL_SENSOR_TYPE` (`type_id`,`sensor_data_class_id`,`sensor_capability`, `sensor_name`) VALUES (35,999,"Senses Generic Contactors", "State");
INSERT INTO `TBL_SENSOR_TYPE` (`type_id`,`sensor_data_class_id`,`sensor_capability`, `sensor_name`) VALUES (100000,214,"Senses On/Off State of Equipment", "Equipment On/Off Status");
 