USE `Synap`;

DROP TABLE IF EXISTS `TBL_NWK_ROUTE`;

CREATE TABLE `TBL_NWK_ROUTE` (
  `route_id` int(11) NOT NULL AUTO_INCREMENT,
  `network_id` int(11) NOT NULL,
  `node_physical_id` bigint(20) UNSIGNED NOT NULL,
  `neighbor_physical_id` bigint(20) UNSIGNED NOT NULL,
  PRIMARY KEY(`route_id`)
)
ENGINE=INNODB
CHARACTER SET utf8 ;