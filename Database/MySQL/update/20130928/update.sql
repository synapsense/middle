-- Historical data retention policy change
-- 24 months online data instead of only 18 months 
use synap;

DROP PROCEDURE IF EXISTS `GET_PARTITION_NUMBER`;

delimiter //
CREATE PROCEDURE GET_PARTITION_NUMBER(IN schemaName VARCHAR(50), IN tableName VARCHAR(50), OUT partitionsNum INT)
begin

SELECT count(*) into partitionsNum
FROM information_schema.PARTITIONS p
where p.table_schema = schemaName
and p.table_name = tableName;

end//
delimiter ;


delimiter //

alter event `archive_data` 
DO
BEGIN
-- default schema veriable
DECLARE SCHEMA_NAME VARCHAR(10) default 'synap';

-- means 24 months
DECLARE ONLINE_PARTITIONS_THRESHOLD INT default 720;

-- stores the number of partitions
DECLARE partitionsNum INT;

-- stores the name of the partition with lower range
DECLARE lastPartitionName VARCHAR(10);
-- the time point -  determines the sensor data that has to be archived
DECLARE oldDataTimePoint DATE;
-- the time point - upper border limit of the new partiotion range(in days)
DECLARE newDataTimePoint INT;

DECLARE maxRangePartitionName VARCHAR(10);
DECLARE maxRangePartitionDataTimePoint DATE;

-- historical values
-- retrieves date and partition name of the partition with last ordinal pos (means the partition with lower range border )
-- implements cycle buffer of partition names
call GET_PARTITION_INFO(SCHEMA_NAME,'historical_values',oldDataTimePoint,lastPartitionName);

call GET_PARTITION_NUMBER(SCHEMA_NAME, 'historical_values', partitionsNum);

-- remove old partition only if there is 2 years data online
IF  partitionsNum >= ONLINE_PARTITIONS_THRESHOLD THEN
  call DROP_PARTITION(SCHEMA_NAME,'historical_values',lastPartitionName);
ELSE
  -- avoid name collision. partition name should be unique
  set lastPartitionName = concat('p',partitionsNum+1);  
END IF;

-- retrieves the upper range border of the new partition
set newDataTimePoint = GET_PARTITION_UPPER_RANGE_BORDER(SCHEMA_NAME,'historical_values');

-- retrieves info about partition with max range border
call GET_MAX_RANGE_PARTITION_INFO(SCHEMA_NAME,'historical_values',maxRangePartitionDataTimePoint,maxRangePartitionName);

call CREATE_PARTITION(SCHEMA_NAME,'historical_values',lastPartitionName,newDataTimePoint,maxRangePartitionName,TO_DAYS(maxRangePartitionDataTimePoint));

END//
delimiter ;

drop table if exists `archive_historical_values`;
