use `synap`;
-- why 21833? because default database encoding is utf8 which requires 3 bytes per character.
-- and max myql row size is 65K, that is why 21833 is used (it is max value which is possible to store in historical_values table. 
ALTER TABLE `valuev` MODIFY COLUMN `value` VARCHAR(21833) DEFAULT NULL;
ALTER TABLE `historical_values` MODIFY COLUMN `valuev` VARCHAR(21833) DEFAULT NULL;