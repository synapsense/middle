drop procedure if exists migrate_alert_priority_tier;

delimiter |

create procedure migrate_alert_priority_tier()
begin
    declare done                    int default 0;
    
    declare lastTypeId              int;
    declare namePropertyId          int;
    declare intervalPropertyId      int;
    declare notifAttemptsPropertyId int;
    declare sendEscPropertyId       int;
    declare sendAckPropertyId       int;
    declare sendResolvedPropertyId  int;
    declare currentTime             long;
    declare lastObjectId            int;
    
    declare apt_id                  int;
    declare apt_name                varchar(300);
    declare apt_interval            long;
    declare apt_notifAttempts       int;
    declare apt_sendEsc             int;
    declare apt_sendAck             int;
    declare apt_sendResolve         int;
    
    declare 
        cur_tier cursor for
            select `priority_tier_id`, `name`, `interval`, `repeat_limit`, `send_esc`, `send_ack`, `send_resolve` 
                from tbl_alert_priority_tier;
                
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;
    
    INSERT INTO object_types(name)
        VALUES ('ALERT_PRIORITY_TIER');
    
    SET lastTypeId := last_insert_id();
    
    INSERT INTO objects(
                  name, object_type_id_fk, static)
        VALUES ('ALERT_PRIORITY_TIER', lastTypeId, 1);
        
    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('name', lastTypeId, 3);

    set namePropertyId = last_insert_id();
    
    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('interval', lastTypeId, 7);
    
    set intervalPropertyId = last_insert_id();
    
    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('notificationAttempts', lastTypeId, 1);
    
    set notifAttemptsPropertyId = last_insert_id();
    
    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('sendEscalation', lastTypeId, 1);
    
    set sendEscPropertyId = last_insert_id();
    
    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('sendAck', lastTypeId, 1);
    
    set sendAckPropertyId = last_insert_id();
    
    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('sendResolve', lastTypeId, 1);
    
    set sendResolvedPropertyId = last_insert_id();
    
    set currentTime = UNIX_TIMESTAMP() * 1000;
    
    open cur_tier;
    
   main:
    loop
        fetch cur_tier
            into apt_id, apt_name, apt_interval, apt_notifAttempts, 
                 apt_sendEsc, apt_sendAck, apt_sendResolve;
        IF done
        THEN
            LEAVE main;
        END IF;    
        
        INSERT INTO objects(object_type_id_fk)
        VALUES (lastTypeId);
        
        SET lastObjectId = last_insert_id();
        
        insert into temp_tier_map values(apt_id,lastObjectId);
        
        CALL insert_varchar_value(
             lastObjectId, namePropertyId, currentTime, apt_name);
        CALL insert_long_value(
             lastObjectId, intervalPropertyId, currentTime, apt_interval);
        CALL insert_int_value(
             lastObjectId, notifAttemptsPropertyId, currentTime, apt_notifAttempts);
        CALL insert_int_value(
             lastObjectId, sendEscPropertyId, currentTime, apt_sendEsc);
        CALL insert_int_value(
             lastObjectId, sendAckPropertyId, currentTime, apt_sendAck);
        CALL insert_int_value(
             lastObjectId, sendResolvedPropertyId, currentTime, apt_sendResolve);
    
    end loop;
    
    CLOSE cur_tier;
        
end;|

delimiter ;