drop procedure if exists migrate_alert_destination;

delimiter |

create procedure migrate_alert_destination()
begin
    declare done                   int default 0;
    
    declare lastTypeId              int;
    declare destinationPropertyId   int;
    declare messageTypePropertyId   int;
    declare alertTypePropertyId     int;
    declare tierPropertyId          int;
    declare currentTime             long;
    declare lastObjectId            int;
    
    declare ad_id                  int;
    declare ad_destination         varchar(300);
    declare ad_msgType             int;
    declare ad_alertType           int;
    declare ad_tier                varchar(300);
    
    declare 
        cur_alert_dest cursor for
            select `destination`, `transport_type_id_fk`, 
                    `new_alert_type`, `name`
                from tbl_destinations as td
                join tbl_alert_type_destination_assoc as atda 
                    on td.destination_id=atda.dest_id_fk 
                join tbl_alert_priority_tier as atpt
                    on atpt.priority_tier_id=atda.priority_tier_id_fk
                join temp_alert_type_map as tatm
                on tatm.old_alert_type=atda.alert_type_id_fk;
                
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;
    
    INSERT INTO object_types(name)
        VALUES ('ALERT_DESTINATION');
    
    SET lastTypeId := last_insert_id();
    
    INSERT INTO objects(
                  name, object_type_id_fk, static)
        VALUES ('ALERT_DESTINATION', lastTypeId, 1);
        
    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('destination', lastTypeId, 3);

    set destinationPropertyId = last_insert_id();
    
    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('messagetype', lastTypeId, 1);

    set messageTypePropertyId = last_insert_id();
    
    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('alertType', lastTypeId, 5);
    
    set alertTypePropertyId = last_insert_id();
    
    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('tier', lastTypeId, 3);
    
    set tierPropertyId = last_insert_id();
    
    set currentTime = UNIX_TIMESTAMP() * 1000;
    
    open cur_alert_dest;
    
   main:
    loop
        fetch cur_alert_dest
            into ad_destination, ad_msgType, ad_alertType, ad_tier;
            
        IF done
        THEN
            LEAVE main;
        END IF;    
        
        INSERT INTO objects(object_type_id_fk)
        VALUES (lastTypeId);
        
        SET lastObjectId = last_insert_id();
        
        CALL insert_varchar_value(
             lastObjectId, destinationPropertyId, currentTime, ad_destination);
        CALL insert_int_value(
             lastObjectId,messageTypePropertyId,currentTime,ad_msgType);
        CALL insert_link_value(
             lastObjectId, alertTypePropertyId, currentTime, ad_alertType);
        CALL insert_varchar_value(
             lastObjectId, tierPropertyId, currentTime, ad_tier);
    
    end loop;
    
    CLOSE cur_alert_dest;
        
end;|

delimiter ;