
DROP FUNCTION IF EXISTS insert_object_value;
DELIMITER | 
CREATE FUNCTION insert_object_value(
  in_object_id int, in_property_id int, in_timestamp long)
  RETURNS INTEGER
  BEGIN
    set @id=0;
    SELECT value_id into @id 
        from obj_values
        where object_id_fk=in_object_id and attr_id_fk=in_property_id limit 1;
        
    IF @id!=0
    THEN 
        RETURN @id;
    END IF;
  
    INSERT INTO obj_values(
                  object_id_fk, attr_id_fk, TIMESTAMP)
    VALUES (in_object_id, in_property_id, in_timestamp);

    RETURN last_insert_id();
  END|
DELIMITER ; 

DROP PROCEDURE IF EXISTS insert_varchar_value;

DELIMITER |
CREATE PROCEDURE insert_varchar_value(
  in_object_id int, in_property_id int, in_timestamp long, valuev varchar(20000))
  BEGIN
    DECLARE valueId   INT;

    SET valueId =
          insert_object_value(
            in_object_id, in_property_id, in_timestamp);

    INSERT INTO valuev(
                  value_id_fk, VALUE)
    VALUES (valueId, valuev);
  END|

DELIMITER ;

DROP PROCEDURE IF EXISTS insert_double_value;

DELIMITER |

CREATE PROCEDURE insert_double_value(
  in_object_id int, in_property_id int, in_timestamp long, valued double) 
  BEGIN
    DECLARE valueId   int;

    SET valueId =
          insert_object_value(
            in_object_id, in_property_id, in_timestamp);

    INSERT INTO valued(
                  value_id_fk, value)
    VALUES (valueId, valued);

  END|

DELIMITER ;

DROP PROCEDURE IF EXISTS insert_int_value;

DELIMITER |
CREATE PROCEDURE insert_int_value(
  in_object_id int, in_property_id int, in_timestamp long, valuei int)
  BEGIN
    DECLARE valueId   int;

    SET valueId =
          insert_object_value(
            in_object_id, in_property_id, in_timestamp);

    INSERT INTO valuei(
                  value_id_fk, value)
    VALUES (valueId, valuei);
  END|
DELIMITER ;

DROP PROCEDURE IF EXISTS insert_long_value;

DELIMITER |
CREATE PROCEDURE insert_long_value(
  in_object_id int, in_property_id int, in_timestamp long, valuel long)
  BEGIN
    DECLARE valueId   int;

    SET valueId =
          insert_object_value(
            in_object_id, in_property_id, in_timestamp);

    INSERT INTO valuel(
                  value_id_fk, value)
    VALUES (valueId, valuel);
  END|
DELIMITER ;

DROP PROCEDURE IF EXISTS insert_link_value;

DELIMITER |
CREATE PROCEDURE insert_link_value(
  in_object_id int, in_property_id int, in_timestamp long, value_link int)
  BEGIN
    DECLARE valueId   int;
    DECLARE typeName  varchar(300);
    
    SET valueId =
          insert_object_value(
            in_object_id, in_property_id, in_timestamp);
            
    IF (value_link IS NULL) THEN
      INSERT INTO value_obj_link(
                  value_id_fk, object_id,object_type)
      VALUES (valueId, NULL, NULL);
    ELSE
      SELECT ot.NAME INTO typeName FROM objects o , object_types ot WHERE o.object_type_id_fk = ot.objtype_id
      AND o.id = value_link;

      INSERT INTO value_obj_link(value_id_fk, object_id, object_type)
      VALUES (valueId, value_link, typeName);

    END IF;
    
  END|
DELIMITER ;



drop table if exists temp_priority_map;

create table `temp_priority_map`(
    `old_priority` int(11),
    `new_priority` int(11)
);

drop table if exists temp_tier_map;

create table `temp_tier_map`(
    `old_tier` int(11),
    `new_tier` int(11)
);

drop table if exists temp_alert_type_map;

create table `temp_alert_type_map`(
    `old_alert_type` int(11),
    `new_alert_type` int(11)
);

drop table if exists temp_alert_map;

create table `temp_alert_map`(
    `old_alert_id` int(11),
    `new_alert_id` int(11)
);

-- add new column for storing historical group id sequence 
ALTER TABLE `uid_table` 
    ADD COLUMN `group_id` INT(11) NOT NULL AFTER `tag_value_id`;

start transaction;
    call migrate_alert_priority_tier();
    call migrate_alert_priority();
    call migrate_alert_type();
    call migrate_alert();
    call migrate_alert_destination();
    call migrate_alert_history();
        
-- renew ids after main update
SET @max_object_id := IFNULL((SELECT max(`id`)+1 FROM `objects`),1);
SET @max_value_id := IFNULL((SELECT max(`value_id`)+1 FROM `obj_values`),1);
SET @max_valued_id := IFNULL((SELECT max(`id`)+1 FROM `valued`),1);
SET @max_valuel_id := IFNULL((SELECT max(`id`)+1 FROM `valuel`),1);
SET @max_valuei_id := IFNULL((SELECT max(`id`)+1 FROM `valuei`),1);
SET @max_valuev_id := IFNULL((SELECT max(`id`)+1 FROM `valuev`),1);
SET @max_valuelink_id := IFNULL((SELECT max(`id`)+1 FROM `value_obj_link`),1);
SET @max_valueplink_id := IFNULL((SELECT max(`id`)+1 FROM `value_property_link`),1);
SET @max_tag_value_id := IFNULL((SELECT max(`value_id`)+1 FROM `tag_values`),1);
SET @max_group_value_id := IFNULL((SELECT max(`group_id`)+1 FROM `historical_values`),1);


delete from uid_table;
INSERT INTO `uid_table` (`object_id`,`value_id`,`valued_id`,`valuev_id`,`valuei_id`,`valuel_id`,`valuelink_id`,`valueplink_id`, `tag_value_id`, `group_id`) values (@max_object_id,@max_value_id,@max_valued_id,@max_valuev_id,@max_valuei_id,@max_valuel_id,@max_valuelink_id,@max_valueplink_id,@max_tag_value_id,@max_group_value_id);

commit;

drop table if exists temp_priority_map;
drop table if exists temp_tier_map;
drop table if exists temp_alert_type_map;
drop table if exists temp_alert_map;

DROP FUNCTION IF EXISTS insert_object_value;
DROP PROCEDURE IF EXISTS insert_varchar_value;
DROP PROCEDURE IF EXISTS insert_double_value;
DROP PROCEDURE IF EXISTS insert_int_value;
DROP PROCEDURE IF EXISTS insert_long_value;
DROP PROCEDURE IF EXISTS insert_link_value;

drop procedure if exists migrate_alert_priority_tier;
drop procedure if exists migrate_alert_priority;
drop procedure if exists migrate_alert_type;
drop procedure if exists migrate_alert;
drop procedure if exists migrate_alert_destination;
drop procedure if exists migrate_alert_history;

SET FOREIGN_KEY_CHECKS=0;
drop table if exists tbl_alert;
drop table if exists tbl_alert_priority;
drop table if exists tbl_alert_priority_tier;
drop table if exists tbl_alert_to_objects_map;
drop table if exists tbl_alert_to_rules_map;
drop table if exists tbl_alert_type;
drop table if exists tbl_alert_type_destination_assoc;
drop table if exists tbl_alert_type_template;
drop table if exists tbl_destinations;
drop table if exists tbl_transport_type;
SET FOREIGN_KEY_CHECKS=1;
