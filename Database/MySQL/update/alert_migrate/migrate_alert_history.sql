drop procedure if exists migrate_alert_history;

delimiter |

create procedure migrate_alert_history()
begin
    declare done                    int default 0;
    
    declare lastTypeId              int;
    declare alertIdPropertyId       int;
    declare namePropertyId          int;
    declare typePropertyId          int;
    declare timePropertyId          int;
    declare statusPropertyId        int;
    declare messagePropertyId       int;
    declare fullMsgPropertyId       int;
    declare hostTOPropertyId        int;
    declare hostTypePropertyId      int;
    declare ackPropertyId           int;
    declare ackTimePropertyId       int;
    declare userPropertyId          int;
    declare currentTime             long;
    declare lastObjectId            int;
    
    
    declare ah_id                  int;
    declare ah_alert_id            int;
    declare ah_name                varchar(300);
    declare ah_type                int;
    declare ah_time                long;
    declare ah_status              int;
    declare ah_message             text;
    declare ah_fullMessage         text;
    declare ah_hostTO              int;
    declare ah_hostType            varchar(300);
    declare ah_ack                 varchar(300);
    declare ah_ackTime             long;
    declare ah_ackTime_orig        long;
    declare ah_user                varchar(300);
    
    
    declare 
        cur_alert_hist cursor for
            select alert_map.`new_alert_id`, `ta`.`name`, `ta`.`time`, `ta`.`status`,`ta`.`origin_message` , 
                    `ta`.`message`, `ta`.`acknowledgement`, unix_timestamp(`ta`.`ack_time`)*1000,
                    `ta`.`user`,`tat`.`new_alert_type`, `atom`.`object_id_fk` ,`ot`.`name`
                from tbl_alert as ta
                left join temp_alert_map as alert_map on ta.alert_id = alert_map.old_alert_id
                left join temp_alert_type_map as tat 
                    on ta.alert_type_id_fk=tat.old_alert_type 
                left join tbl_alert_to_objects_map as atom
                    on atom.alert_id_fk=ta.alert_id
                left join objects as obj
                    on obj.id=atom.object_id_fk
                left join object_types as ot
                    on ot.objtype_id=obj.object_type_id_fk
                where status>1 
                order by time;
                
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;
    
    INSERT INTO object_types(name)
        VALUES ('ALERT_HISTORY');
    
    SET lastTypeId := last_insert_id();
    
    INSERT INTO objects(
                  name, object_type_id_fk, static)
        VALUES ('ALERT_HISTORY', lastTypeId, 1);
        
    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk, historical)
    values ('alertId', lastTypeId, 1, 1);
    
    set alertIdPropertyId := last_insert_id();
     
    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk, historical)
    values ('name', lastTypeId, 3, 1);

    set namePropertyId := last_insert_id();
    
    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk, historical)
    values ('type', lastTypeId, 1, 1);

    set typePropertyId = last_insert_id();
    
    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk, historical)
    values ('time', lastTypeId, 7, 1);
    
    set timePropertyId = last_insert_id();
    
    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk, historical)
    values ('status', lastTypeId, 1, 1);
    
    set statusPropertyId = last_insert_id();
    
    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk, historical)
    values ('message', lastTypeId, 3, 1);
    
    set messagePropertyId = last_insert_id();
    
    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk, historical)
    values ('fullMessage', lastTypeId, 3, 1);
    
    set fullMsgPropertyId = last_insert_id();
    
    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk, historical)
    values ('hostTO', lastTypeId, 1, 1);
    
    set hostTOPropertyId = last_insert_id();
    
    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk, historical)
    values ('hostType', lastTypeId, 3, 1);
    
    set hostTypePropertyId = last_insert_id();
    
    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk, historical)
    values ('acknowledgement', lastTypeId, 3, 1);
    
    set ackPropertyId = last_insert_id();
    
    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk, historical)
    values ('acknowledgementTime', lastTypeId, 7, 1);
    
    set ackTimePropertyId = last_insert_id();
    
    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk, historical)
    values ('user', lastTypeId, 3, 1);
    
    set userPropertyId = last_insert_id();
    
    set currentTime = UNIX_TIMESTAMP() * 1000;
    
    select  alert_map.`new_alert_id`, `ta`.`name`, `ta`.`time`, `ta`.`status`,`ta`.`origin_message` , 
                    `ta`.`message`, `ta`.`acknowledgement`, unix_timestamp(`ta`.`ack_time`)*1000,
                    `ta`.`user`,`tat`.`new_alert_type`, `atom`.`object_id_fk` ,`ot`.`name`
        into ah_alert_id, ah_name, ah_time, ah_status, 
                 ah_message, ah_fullMessage, ah_ack, 
                 ah_ackTime, ah_user, ah_type, ah_hostTO, ah_hostType 
        from tbl_alert as ta
        		left join temp_alert_map as alert_map 
        		 	on ta.alert_id = alert_map.old_alert_id
                left join temp_alert_type_map as tat 
                    on ta.alert_type_id_fk=tat.old_alert_type 
                left join tbl_alert_to_objects_map as atom
                    on atom.alert_id_fk=ta.alert_id
                left join objects as obj
                    on obj.id=atom.object_id_fk
                left join object_types as ot
                    on ot.objtype_id=obj.object_type_id_fk
                where status>1 
                order by time desc limit 1;
                
        INSERT INTO objects(object_type_id_fk)
        VALUES (lastTypeId);
        
        SET lastObjectId = last_insert_id();
    
       	CALL insert_int_value(
             lastObjectId, alertIdPropertyId, currentTime, ah_alert_id);
        set @alert_id_value_id := insert_object_value(
             lastObjectId, alertIdPropertyId, currentTime);
             
        CALL insert_varchar_value(
             lastObjectId, namePropertyId, currentTime, ah_name);
        set @name_value_id := insert_object_value(
             lastObjectId, namePropertyId, currentTime);
             
        CALL insert_link_value(
             lastObjectId,typePropertyId,currentTime,ah_type);
        set @type_value_id := insert_object_value(
             lastObjectId, typePropertyId, currentTime);
             
        CALL insert_long_value(
             lastObjectId, timePropertyId, currentTime, ah_time);
        set @time_value_id := insert_object_value(
             lastObjectId, timePropertyId, currentTime);
             
        CALL insert_int_value(
             lastObjectId, statusPropertyId, currentTime, ah_status);
        set @status_value_id := insert_object_value(
             lastObjectId, statusPropertyId, currentTime);
            
        CALL insert_varchar_value(
             lastObjectId, messagePropertyId, currentTime, ah_message);
        set @message_value_id := insert_object_value(
             lastObjectId, messagePropertyId, currentTime);
          
        CALL insert_varchar_value(
             lastObjectId, fullMsgPropertyId, currentTime, ah_fullMessage);
        set @fullMsg_value_id := insert_object_value(
             lastObjectId, fullMsgPropertyId, currentTime);
        
        CALL insert_link_value(
            lastObjectId, hostTOPropertyId, currentTime, ah_hostTO);
        set @hostTO_value_id := insert_object_value(
            lastObjectId, hostTOPropertyId, currentTime);
          
        CALL insert_varchar_value(
             lastObjectId, hostTypePropertyId, currentTime, ah_hostType);
        set @hostType_value_id := insert_object_value(
             lastObjectId, hostTypePropertyId, currentTime);
           
        CALL insert_varchar_value(
             lastObjectId, ackPropertyId, currentTime, ah_ack);
        set @ack_value_id := insert_object_value(
             lastObjectId, ackPropertyId, currentTime);
             
        CALL insert_long_value(
             lastObjectId, ackTimePropertyId, currentTime, ah_ackTime);
        set @ackTime_value_id := insert_object_value(
             lastObjectId, ackTimePropertyId, currentTime);
        
        CALL insert_varchar_value(
             lastObjectId, userPropertyId, currentTime, ah_user);
        set @user_value_id := insert_object_value(
             lastObjectId, userPropertyId, currentTime);
    
    set @group_id:=0;
    
    open cur_alert_hist;
    
   main:
    loop
        fetch cur_alert_hist
            into ah_alert_id, ah_name, ah_time, ah_status, 
                 ah_message, ah_fullMessage, ah_ack, 
                 ah_ackTime, ah_user, ah_type, ah_hostTO, ah_hostType;
            
-- add check for ack_time(if NULL use current timestamp)(Bug#7510)          
        if ah_ackTime is null then
           set ah_ackTime := UNIX_TIMESTAMP()*1000;
        end if;
        
        set ah_ackTime_orig:=ah_ackTime;
        set ah_ackTime:=ah_ackTime / 1000;
        
        IF done
        THEN
            LEAVE main;
        END IF; 
        
        set @group_id=@group_id+1;
        
        insert into historical_values(value_id_fk, timestamp, valuei, group_id) 
            values(@alert_id_value_id, FROM_UNIXTIME(ah_ackTime), ah_alert_id,@group_id);
            
        insert into historical_values(value_id_fk, timestamp, valuev, group_id) 
            values(@name_value_id, FROM_UNIXTIME(ah_ackTime), ah_name,@group_id);
            
        insert into historical_values(value_id_fk, timestamp, valuei, group_id) 
            values(@type_value_id, FROM_UNIXTIME(ah_ackTime), ah_type,@group_id);
            
        insert into historical_values(value_id_fk, timestamp, valuel, group_id) 
            values(@time_value_id, FROM_UNIXTIME(ah_ackTime), ah_time,@group_id);
            
        insert into historical_values(value_id_fk, timestamp, valuei, group_id) 
            values(@status_value_id, FROM_UNIXTIME(ah_ackTime), ah_status,@group_id);
            
        insert into historical_values(value_id_fk, timestamp, valuev, group_id) 
            values(@message_value_id, FROM_UNIXTIME(ah_ackTime), ah_message,@group_id);
           
        if ah_fullMessage is not null then
        insert into historical_values(value_id_fk, timestamp, valuev, group_id) 
            values(@fullMsg_value_id, FROM_UNIXTIME(ah_ackTime), ah_fullMessage,@group_id);
        end if;
        
        if ah_hostTO is not null then
        insert into historical_values(value_id_fk, timestamp, valuei, group_id) 
            values(@hostTO_value_id, FROM_UNIXTIME(ah_ackTime), ah_hostTO,@group_id);
        end if;
        
        if ah_hostType is not null then
        insert into historical_values(value_id_fk, timestamp, valuev, group_id) 
            values(@hostType_value_id, FROM_UNIXTIME(ah_ackTime), ah_hostType,@group_id);
        end if;
         
-- check acknowledgement(put value if not NULL)(Bug#7510)
        if ah_ack is not null then
        insert into historical_values(value_id_fk, timestamp, valuev, group_id) 
            values(@ack_value_id, FROM_UNIXTIME(ah_ackTime), ah_ack,@group_id);
        end if;
        
        insert into historical_values(value_id_fk, timestamp, valuel, group_id) 
            values(@ackTime_value_id, FROM_UNIXTIME(ah_ackTime), ah_ackTime_orig,@group_id);
        
        if ah_user is not null then
        insert into historical_values(value_id_fk, timestamp, valuev, group_id) 
            values(@user_value_id, FROM_UNIXTIME(ah_ackTime), ah_user,@group_id);
        end if;
    
    end loop;
    
    CLOSE cur_alert_hist;
        
end;|

delimiter ;