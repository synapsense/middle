drop procedure if exists migrate_alert_type;

delimiter |

create procedure migrate_alert_type()
begin
    declare done                    int default 0;
    
    declare lastTypeId              int;
    declare namePropertyId          int;
    declare displayNamePropertyId   int;
    declare descrPropertyId         int;
    declare activePropertyId        int;
    declare autoAckPropertyId       int;
    declare autoDismissPropertyId   int;
    declare autoDismissAllowedPropertyId int;
    declare priorityPropertyId      int;
    declare templatesPropertyId     int;
    declare currentTime             long;
    declare lastObjectId            int;
    
    declare at_id                   int;
    declare at_name                 varchar(300);
    declare at_displayName          varchar(300);
    declare at_description          varchar(300);
    declare at_active               int;
    declare at_autoAck              int;
    declare at_autoDismiss          int;
    declare at_autoDismissAllowed   int;
     
    declare at_priority             int;
    
    declare at_new_type_id          int;
    declare at_template             int;

    declare 
        cur_type cursor for
            select `alert_type_id`, `name`, 
            		  CASE substring(`name`,1,13)
   					  WHEN 'ConsoleAlert_' THEN substring(`name`,14)
    				  ELSE `name` 
    				  END, 
    				`description`, `is_active`, `auto_ack`, `auto_dismiss`, `auto_dismiss_allowed`, `new_priority` 
                from tbl_alert_type as tat 
                join temp_priority_map as tp 
                on tat.alert_priority_id_fk=tp.old_priority;
                
    declare
        cur_templates cursor for
            select `object_id_fk`, `new_alert_type` 
            from tbl_alert_type_template as tatt
            join temp_alert_type_map as tat
            on tat.old_alert_type=tatt.alert_type_id_fk;
                
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;
    
    INSERT INTO object_types(name)
        VALUES ('ALERT_TYPE');
    
    SET lastTypeId := last_insert_id();
    
    INSERT INTO objects(
                  name, object_type_id_fk, static)
        VALUES ('ALERT_TYPE', lastTypeId, 1);
        
    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('name', lastTypeId, 3);

    set namePropertyId = last_insert_id();

    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('displayName', lastTypeId, 3);

    set displayNamePropertyId = last_insert_id();
    
    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('description', lastTypeId, 3);

    set descrPropertyId = last_insert_id();
    
    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('active', lastTypeId, 1);
    
    set activePropertyId = last_insert_id();
    
    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('autoAck', lastTypeId, 1);
    
    set autoAckPropertyId = last_insert_id();
    
    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('autoDismiss', lastTypeId, 1);
    
    set autoDismissPropertyId = last_insert_id();
    
    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('autoDismissAllowed', lastTypeId, 1);
    
    set autoDismissAllowedPropertyId = last_insert_id();
    
    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('priority', lastTypeId, 5);
    
    set priorityPropertyId = last_insert_id();
    
    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk,historical,collection)
    values ('templates', lastTypeId, 5,0,1);
    
    set templatesPropertyId = last_insert_id();
    
    set currentTime = UNIX_TIMESTAMP() * 1000;
    
    open cur_type;
    
   main:
    loop
        fetch cur_type
            into at_id, at_name, at_displayName , at_description, at_active, 
                 at_autoAck, at_autoDismiss, at_autoDismissAllowed, at_priority;
        IF done
        THEN
            LEAVE main;
        END IF;    
        
        INSERT INTO objects(object_type_id_fk)
        VALUES (lastTypeId);
        
        SET lastObjectId = last_insert_id();
        
        insert into temp_alert_type_map values(at_id, lastObjectId);
        
        CALL insert_varchar_value(
             lastObjectId, namePropertyId, currentTime, at_name);
        CALL insert_varchar_value(
             lastObjectId, displayNamePropertyId, currentTime, at_displayName);
        CALL insert_varchar_value(
             lastObjectId, descrPropertyId, currentTime, at_description);
        CALL insert_int_value(
             lastObjectId, activePropertyId, currentTime, at_active);
        CALL insert_int_value(
             lastObjectId, autoAckPropertyId, currentTime, at_autoAck);
        CALL insert_int_value(
             lastObjectId, autoDismissPropertyId, currentTime, at_autoDismiss);
        CALL insert_int_value(
             lastObjectId, autoDismissAllowedPropertyId, currentTime, at_autoDismissAllowed);
        CALL insert_link_value(
             lastObjectId, priorityPropertyId, currentTime, at_priority);
             
    end loop;
    
    CLOSE cur_type;
    
    set done:=0;
        
    open cur_templates;
    
   templates:
    loop
        fetch cur_templates
            into at_template, at_new_type_id;
        IF done
        THEN
            LEAVE templates;
        END IF;    
        
        CALL insert_link_value(
             at_new_type_id, templatesPropertyId, currentTime, at_template);
             
    end loop;
    
    CLOSE cur_templates;
    
end;|

delimiter ;