drop procedure if exists migrate_alert;

delimiter |

create procedure migrate_alert()
begin
    declare done                    int default 0;
    
    declare lastTypeId              int;
    declare namePropertyId          int;
    declare typePropertyId          int;
    declare timePropertyId          int;
    declare statusPropertyId        int;
    declare messagePropertyId       int;
    declare fullMsgPropertyId       int;
    declare hostTOPropertyId        int;
    declare ackPropertyId           int;
    declare ackTimePropertyId       int;
    declare userPropertyId          int;
    declare currentTime             long;
    declare lastObjectId            int;
    
    declare a_id                  int;
    declare a_name                varchar(300);
    declare a_type                int;
    declare a_time                long;
    declare a_status              int;
    declare a_message             text;
    declare a_fullMessage         text;
    declare a_hostTO              int;
    declare a_ack                 varchar(300);
    declare a_ackTime             long;
    declare a_user                varchar(300);
    
    
    declare 
        cur_alert cursor for
            select `alert_id`, `name`, `time`, `status`, `origin_message`, 
                    `message`, `acknowledgement`, `ack_time`,
                    `user`,`new_alert_type`, `object_id_fk` 
                from tbl_alert as ta
                left join temp_alert_type_map as tat 
                    on ta.alert_type_id_fk=tat.old_alert_type 
                left join tbl_alert_to_objects_map as atom
                    on atom.alert_id_fk=ta.alert_id;
                
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;
    
    INSERT INTO object_types(name)
        VALUES ('ALERT');
    
    SET lastTypeId := last_insert_id();
    
    INSERT INTO objects(
                  name, object_type_id_fk, static)
        VALUES ('ALERT', lastTypeId, 1);
        
    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('name', lastTypeId, 3);

    set namePropertyId = last_insert_id();
    
    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('type', lastTypeId, 5);

    set typePropertyId = last_insert_id();
    
    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('time', lastTypeId, 7);
    
    set timePropertyId = last_insert_id();
    
    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('status', lastTypeId, 1);
    
    set statusPropertyId = last_insert_id();
    
    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('message', lastTypeId, 3);
    
    set messagePropertyId = last_insert_id();
    
    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('fullMessage', lastTypeId, 3);
    
    set fullMsgPropertyId = last_insert_id();
    
    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('hostTO', lastTypeId, 5);
    
    set hostTOPropertyId = last_insert_id();
    
    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('acknowledgement', lastTypeId, 3);
    
    set ackPropertyId = last_insert_id();
    
    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('acknowledgementTime', lastTypeId, 7);
    
    set ackTimePropertyId = last_insert_id();
    
    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('user', lastTypeId, 3);
    
    set userPropertyId = last_insert_id();
    
    set currentTime = UNIX_TIMESTAMP() * 1000;
    
    open cur_alert;
    
   main:
    loop
        fetch cur_alert
            into a_id, a_name, a_time, a_status, 
                 a_message, a_fullMessage, a_ack, a_ackTime, a_user, a_type, a_hostTO;
        IF done
        THEN
            LEAVE main;
        END IF;    
        
        INSERT INTO objects(object_type_id_fk)
        VALUES (lastTypeId);
        
        SET lastObjectId = last_insert_id();

 		insert into temp_alert_map values(a_id, lastObjectId);
        
        
  
		if a_status<2 then
        CALL insert_varchar_value(
             lastObjectId, namePropertyId, currentTime, a_name);
        CALL insert_link_value(
             lastObjectId,typePropertyId,currentTime,a_type);
        CALL insert_long_value(
             lastObjectId, timePropertyId, currentTime, a_time);
        CALL insert_int_value(
             lastObjectId, statusPropertyId, currentTime, a_status);
        CALL insert_varchar_value(
             lastObjectId, messagePropertyId, currentTime, a_message);
        CALL insert_varchar_value(
             lastObjectId, fullMsgPropertyId, currentTime, a_fullMessage);
        CALL insert_link_value(
             lastObjectId, hostTOPropertyId, currentTime, a_hostTO);
        CALL insert_varchar_value(
             lastObjectId, ackPropertyId, currentTime, a_ack);
        CALL insert_long_value(
             lastObjectId, ackTimePropertyId, currentTime, UNIX_TIMESTAMP(a_ackTime)*1000);
        CALL insert_varchar_value(
             lastObjectId, userPropertyId, currentTime, a_user);
		else 
        delete from objects where id = lastObjectId;
    end if;
	
    end loop;
    
    CLOSE cur_alert;
        
end;|

delimiter ;