@echo off

set mysqluser=%1
set password=%2
set db_schema=%3 

mysql -u%mysqluser% -p%password% %db_schema% < migrate_alert_priority_tier.sql
mysql -u%mysqluser% -p%password% %db_schema% < migrate_alert_priority.sql
mysql -u%mysqluser% -p%password% %db_schema% < migrate_alert_type.sql
mysql -u%mysqluser% -p%password% %db_schema% < migrate_alert.sql
mysql -u%mysqluser% -p%password% %db_schema% < migrate_alert_destination.sql
mysql -u%mysqluser% -p%password% %db_schema% < migrate_alert_history.sql

echo Executing migration procedure...
mysql -u%mysqluser% -p%password% %db_schema% < migrate_run_all.sql
if ERRORLEVEL 1 goto error

goto success

:error
echo Error removing alert. Fix problems and try again
set err_code=1
goto exit

:success 
echo Alert migration is done
set err_code=0
goto exit

:exit
echo Exiting alert migration procedure
exit /B %err_code%
