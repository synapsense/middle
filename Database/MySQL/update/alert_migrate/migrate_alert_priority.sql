drop procedure if exists migrate_alert_priority;

delimiter |

create procedure migrate_alert_priority()
begin
    declare done                       int default 0;
    
    declare lastTypeId                 int;
    declare namePropertyId             int;
    declare dismissIntervalPropertyId  int;  
    declare tiersPropertyId            int;
	declare redSignalPropertyId		   int;
	declare yellowSignalPropertyId     int;
	declare greenSignalPropertyId	   int;
	declare audioSignalPropertyId	   int;
	declare lightDurationPropertyId    int;
	declare audioDurationPropertyId    int;
    declare currentTime                long;
    declare lastObjectId               int;
    
    declare ap_id                      int;
    declare ap_name                    varchar(300);
    declare ap_dismissInterval         long;
    
    declare new_tier_id                int;
    declare new_priority_id            int;
    
     declare 
        cur_priority cursor for
            select * from tbl_alert_priority;
            
    declare 
        cur_tiers cursor for
            select new_tier, new_priority 
                from temp_tier_map as tt
                    join tbl_alert_priority_tier as apt
                      on (tt.old_tier=apt.priority_tier_id)
                    join temp_priority_map as tp
                      on (tp.old_priority=apt.alert_priority_id_fk);
            
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

    INSERT INTO object_types(name)
        VALUES ('ALERT_PRIORITY');
    
    SET lastTypeId := last_insert_id();
    
    INSERT INTO objects(
                  name, object_type_id_fk, static)
        VALUES ('ALERT_PRIORITY', lastTypeId, 1);
        
    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('name', lastTypeId, 3);

    set namePropertyId = last_insert_id();
    
    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('dismissInterval', lastTypeId, 7);
    
    set dismissIntervalPropertyId = last_insert_id();
    
    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk,historical,collection)
    values ('tiers', lastTypeId, 5,0,1);
    
    set tiersPropertyId = last_insert_id();
	
	insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('redSignal', lastTypeId, 1);
	
	set redSignalPropertyId=last_insert_id();
	
	insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('greenSignal', lastTypeId, 1);
	
	set greenSignalPropertyId=last_insert_id();
	
	insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('yellowSignal', lastTypeId, 1);
	
	set yellowSignalPropertyId=last_insert_id();
	
	insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('audioSignal', lastTypeId, 1);
	
	set audioSignalPropertyId=last_insert_id();
	
	insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('lightDuration', lastTypeId, 7);
	
	set lightDurationPropertyId=last_insert_id();
	
	insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('audioDuration', lastTypeId, 7);
	
	set audioDurationPropertyId=last_insert_id();
    
     set currentTime = UNIX_TIMESTAMP() * 1000;
    
    open cur_priority;
    
   main:
    loop
        fetch cur_priority
            into ap_id, ap_name, ap_dismissInterval;
        IF done
        THEN
            LEAVE main;
        END IF;    
        
        INSERT INTO objects(object_type_id_fk)
        VALUES (lastTypeId);
        
        SET lastObjectId = last_insert_id();
        
        insert into temp_priority_map values (ap_id,lastObjectId);
        
        CALL insert_varchar_value(
             lastObjectId, namePropertyId, currentTime, ap_name);
        CALL insert_long_value(
             lastObjectId, dismissIntervalPropertyId, currentTime, ap_dismissInterval);
			 
		CALL insert_int_value(lastObjectId, redSignalPropertyId, currentTime, 0);
        CALL insert_int_value(lastObjectId, greenSignalPropertyId, currentTime, 0);
        CALL insert_int_value(lastObjectId, yellowSignalPropertyId, currentTime, 0);
        CALL insert_int_value(lastObjectId, audioSignalPropertyId, currentTime, 0);
        CALL insert_long_value(lastObjectId, audioDurationPropertyId, currentTime, 0);
        CALL insert_long_value(lastObjectId, lightDurationPropertyId, currentTime, 0);

    end loop;
    
    CLOSE cur_priority;
    
    set done:=0;
    
    open cur_tiers;
    
   tier:
    loop
        fetch cur_tiers
            into new_tier_id,new_priority_id;
        IF done
        THEN
            LEAVE tier;
        END IF;    
        
        call insert_link_value(new_priority_id,tiersPropertyId,currentTime,new_tier_id);

    end loop;
    
    CLOSE cur_tiers;
end;|

delimiter ;