use `synap`;

DROP PROCEDURE IF EXISTS `STORE_AND_UPDATE_ALERTS_USER_INFO`;
delimiter //
CREATE PROCEDURE STORE_AND_UPDATE_ALERTS_USER_INFO () 
BEGIN

DECLARE alert_id_var INT;
DECLARE user_var VARCHAR(300);

DECLARE done INT DEFAULT 0;
DECLARE cur CURSOR FOR SELECT `TBL_ALERT`.`alert_id`, CONCAT_WS(' ',`TBL_USERS`.`f_name`, `TBL_USERS`.`l_name`)   from `TBL_ALERT` left join `TBL_USERS` on `TBL_ALERT`.`user_id_fk` = `TBL_USERS`.`user_id` where `TBL_ALERT`.`status` = 0;
DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

ALTER TABLE `TBL_ALERT` ADD `user` varchar(300) AFTER `alert_type_id_fk`;

OPEN cur;

REPEAT FETCH cur INTO alert_id_var, user_var;
    UPDATE `TBL_ALERT` SET `user` = user_var WHERE `alert_id` = alert_id_var;
UNTIL done END REPEAT;
CLOSE cur;

ALTER TABLE `TBL_ALERT` DROP FOREIGN KEY `alert_dismissed_by`; 
ALTER TABLE `TBL_ALERT` DROP `user_id_fk`;
END//
delimiter ;

CALL STORE_AND_UPDATE_ALERTS_USER_INFO();
DROP PROCEDURE IF EXISTS `STORE_AND_UPDATE_ALERTS_USER_INFO`;