START TRANSACTION;
-- renew ids 
SET @max_object_id := IFNULL((SELECT max(`id`)+1 FROM `objects`),1);
SET @max_tag_value_id := IFNULL((SELECT max(`value_id`)+1 FROM `tag_values`),1);
SET @max_value_id := IFNULL((SELECT max(`value_id`)+1 FROM `obj_values`),1);
SET @max_valued_id := IFNULL((SELECT max(`id`)+1 FROM `valued`),1);
SET @max_valuel_id := IFNULL((SELECT max(`id`)+1 FROM `valuel`),1);
SET @max_valuei_id := IFNULL((SELECT max(`id`)+1 FROM `valuei`),1);
SET @max_valuev_id := IFNULL((SELECT max(`id`)+1 FROM `valuev`),1);
SET @max_valuelink_id := IFNULL((SELECT max(`id`)+1 FROM `value_obj_link`),1);
SET @max_valueplink_id := IFNULL((SELECT max(`id`)+1 FROM `value_property_link`),1);
-- uncomment next line when group_id counter is affected in one of the update script for release
-- SET @max_group_value_id := IFNULL((SELECT max(`group_id`)+1 FROM `historical_values`),1);
UPDATE `uid_table`
	SET
	object_id = @max_object_id,
	tag_value_id = @max_tag_value_id,
	value_id = @max_value_id,
	valued_id = @max_valued_id,
	valuev_id = @max_valuev_id,
	valuei_id = @max_valuei_id,
	valuel_id = @max_valuel_id,
	valuelink_id = @max_valuelink_id,
	valueplink_id = @max_valueplink_id
	-- uncomment next line when group_id counter is affected in one of the update script for release
	-- , group_id = @max_group_value_id
WHERE 1 = 1;

COMMIT;
