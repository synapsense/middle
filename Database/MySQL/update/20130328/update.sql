-- Bug#8892
-- The script adds a new property 'modelVersion' to ROOT object type

use synap;

-- new property of ROOT type
select objtype_id into @rootTypeId from object_types where name = 'Root';	
insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('modelVersion', @rootTypeId, 3);
