
DROP PROCEDURE IF EXISTS `GET_PARTITION_NUMBER`;

delimiter //
CREATE PROCEDURE GET_PARTITION_NUMBER(IN schemaName VARCHAR(50), IN tableName VARCHAR(50), OUT partitionsNum INT)
begin

SELECT count(*) into partitionsNum
FROM information_schema.PARTITIONS p
where p.table_schema = schemaName
and p.table_name = tableName;

end//
delimiter ;

DROP PROCEDURE IF EXISTS `GET_PARTITION_INFO`;

delimiter //
CREATE PROCEDURE GET_PARTITION_INFO(IN schemaName VARCHAR(50), IN tableName VARCHAR(50), OUT timepoint DATE, OUT partitionName VARCHAR(10))
begin
DECLARE lastPartitionName VARCHAR(10);

DECLARE oldDateTimepoint DATE;

SELECT distinct from_days(p.partition_description) ,p.partition_name
into timepoint,partitionName
FROM information_schema.PARTITIONS p
where p.table_schema = schemaName
and p.table_name =tableName
and p.partition_ordinal_position =
(select distinct(min(P_sub.partition_ordinal_position))
  from information_schema.PARTITIONS P_sub where P_sub.table_schema = schemaName
  and P_sub.table_name =tableName
);

end//
delimiter ;


DROP PROCEDURE IF EXISTS `GET_MAX_RANGE_PARTITION_INFO`;

delimiter //
CREATE PROCEDURE GET_MAX_RANGE_PARTITION_INFO(IN schemaName VARCHAR(50), IN tableName VARCHAR(50), OUT timepoint DATE, OUT partitionName VARCHAR(10))
begin
DECLARE lastPartitionName VARCHAR(10);

DECLARE oldDateTimepoint DATE;

SELECT distinct from_days(p.partition_description) ,p.partition_name
into timepoint,partitionName
FROM information_schema.PARTITIONS P
where p.table_schema = schemaName
and p.table_name =tableName
and p.partition_ordinal_position =
(select distinct(MAX(P_sub.partition_ordinal_position))
  from information_schema.PARTITIONS P_sub where P_sub.table_schema = schemaName
  and P_sub.table_name =tableName
);

end//
delimiter ;


DROP FUNCTION IF EXISTS `GET_PARTITION_UPPER_RANGE_BORDER`;

delimiter //
CREATE FUNCTION `GET_PARTITION_UPPER_RANGE_BORDER` (schemaName VARCHAR(50),tableName VARCHAR(50))  RETURNS INT
begin
DECLARE result INT;

-- retrieves the upper range border of the new partition
--   1.gets partition range upper border
--   2.add 1 month interval to it
--   3.converts the date back to days count

SELECT distinct to_days(date_add(from_days(p.partition_description), INTERVAL 1 day))
into result
FROM information_schema.PARTITIONS P
where p.table_schema = schemaName
and p.table_name =tableName
and p.partition_ordinal_position =
(select distinct(min(P_sub1.partition_ordinal_position))
  from (select distinct p_sub.partition_ordinal_position from
  information_schema.PARTITIONS P_sub where P_sub.table_schema = schemaName
  and P_sub.table_name = tableName order by partition_ordinal_position desc limit 2) as p_sub1
);

return result;
end//
delimiter ;

DROP PROCEDURE IF EXISTS `CREATE_PARTITION`;

delimiter //
CREATE PROCEDURE CREATE_PARTITION(IN schemaName VARCHAR(50), IN  tableName VARCHAR(50), IN partitionName VARCHAR(50),IN timepoint INT,IN maxRangePartitionName VARCHAR(50),IN maxRangeTimepoint INT)
begin
-- dynamic instruction to create new partition by reorginizing the partiotion with max range border into 2
set @preparedStmtCreate = concat('ALTER TABLE ',schemaName,'.',tableName,' REORGANIZE partition ' , maxRangePartitionName, ' INTO (partition ',partitionName, ' values LESS THAN (',timepoint,'), partition ',maxRangePartitionName,' values LESS THAN(',maxRangeTimepoint,'));');

PREPARE create_new_partition from @preparedStmtCreate;
EXECUTE create_new_partition;
DEALLOCATE PREPARE create_new_partition;
end//
delimiter ;

DROP PROCEDURE IF EXISTS `DROP_PARTITION`;

delimiter //
CREATE PROCEDURE DROP_PARTITION(IN schemaName VARCHAR(50), IN tableName VARCHAR(50), IN partitionName VARCHAR(50)) 
begin
-- dynamic instruction to drop partition with the specified name (last partiotion is no more necessary)
set @preparedStmtDrop = concat('ALTER TABLE ',schemaName,'.',tableName,' drop partition ',partitionName);
PREPARE drop_last_partition from @preparedStmtDrop;
EXECUTE drop_last_partition;
DEALLOCATE PREPARE drop_last_partition;
end//

delimiter ;

-- enables archivation event
-- can be executed even if db has some working data
DROP EVENT IF EXISTS `archive_data`;

delimiter //
-- starts before the last partition will be filled out (~86M records= 10000 sensors x 5 min x 1 month)
-- expected execution time is 1.5-2 hours
CREATE EVENT `archive_data`
ON SCHEDULE EVERY 1 DAY
-- starts just before 'LESS THAN' value of the last partition
STARTS TIMESTAMP(DATE(%event_start),'01:00')
DO
BEGIN
-- default schema veriable
DECLARE SCHEMA_NAME VARCHAR(10) default 'synap';

-- means 24 months
DECLARE ONLINE_PARTITIONS_THRESHOLD INT default 720;

-- stores the number of partitions
DECLARE partitionsNum INT;

-- stores the name of the partition with lower range
DECLARE lastPartitionName VARCHAR(10);
-- the time point -  determines the sensor data that has to be archived
DECLARE oldDataTimePoint DATE;
-- the time point - upper border limit of the new partiotion range(in days)
DECLARE newDataTimePoint INT;

DECLARE maxRangePartitionName VARCHAR(10);
DECLARE maxRangePartitionDataTimePoint DATE;

-- historical values
-- retrieves date and partition name of the partition with last ordinal pos (means the partition with lower range border )
-- implements cycle buffer of partition names
call GET_PARTITION_INFO(SCHEMA_NAME,'historical_values',oldDataTimePoint,lastPartitionName);

call GET_PARTITION_NUMBER(SCHEMA_NAME, 'historical_values', partitionsNum);

-- remove old partition only if there is 2 years data online
IF  partitionsNum >= ONLINE_PARTITIONS_THRESHOLD THEN
  call DROP_PARTITION(SCHEMA_NAME,'historical_values',lastPartitionName);
ELSE
  -- avoid name collision. partition name should be unique
  set lastPartitionName = concat('p',partitionsNum+1);  
END IF;

-- retrieves the upper range border of the new partition
set newDataTimePoint = GET_PARTITION_UPPER_RANGE_BORDER(SCHEMA_NAME,'historical_values');

-- retrieves info about partition with max range border
call GET_MAX_RANGE_PARTITION_INFO(SCHEMA_NAME,'historical_values',maxRangePartitionDataTimePoint,maxRangePartitionName);

call CREATE_PARTITION(SCHEMA_NAME,'historical_values',lastPartitionName,newDataTimePoint,maxRangePartitionName,TO_DAYS(maxRangePartitionDataTimePoint));

END//

delimiter ;

