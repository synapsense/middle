use synap;

DROP PROCEDURE IF EXISTS insert_link_value;

DELIMITER |
CREATE PROCEDURE insert_link_value(
  in_object_id int, in_property_id int, in_timestamp long, value_link int)
  BEGIN
    DECLARE valueId   int;
    DECLARE typeName  varchar(300);
    
    SET valueId =
          insert_object_value(
            in_object_id, in_property_id, in_timestamp);
    
    CALL add_link_value(valueId, in_timestamp, value_link);
  END|
DELIMITER ;

DROP PROCEDURE IF EXISTS add_link_value;

DELIMITER |
CREATE PROCEDURE add_link_value(
  valueId int, in_timestamp long, value_link int)
  BEGIN
    DECLARE typeName  varchar(300);
            
    IF (value_link IS NULL) THEN
      INSERT INTO value_obj_link(
                  value_id_fk, object_id,object_type)
      VALUES (valueId, NULL, NULL);
    ELSE
      SELECT ot.NAME INTO typeName FROM objects o , object_types ot WHERE o.object_type_id_fk = ot.objtype_id
      AND o.id = value_link;

      INSERT INTO value_obj_link(value_id_fk, object_id, object_type)
      VALUES (valueId, value_link, typeName);

    END IF;
    
  END|
DELIMITER ;

DROP FUNCTION IF EXISTS insert_object_value;
DELIMITER | 
CREATE FUNCTION insert_object_value(
  in_object_id int, in_property_id int, in_timestamp long)
  RETURNS INTEGER
  BEGIN
    set @id=0;
    SELECT value_id into @id 
        from obj_values
        where object_id_fk=in_object_id and attr_id_fk=in_property_id limit 1;
        
    IF @id!=0
    THEN 
        RETURN @id;
    END IF;
  
    INSERT INTO obj_values(
                  object_id_fk, attr_id_fk, TIMESTAMP)
    VALUES (in_object_id, in_property_id, in_timestamp);

    RETURN last_insert_id();
  END|
DELIMITER ;  


DROP PROCEDURE IF EXISTS insert_varchar_value;
DELIMITER |
CREATE PROCEDURE insert_varchar_value(
  in_object_id int, in_property_id int, in_timestamp long, valuev varchar(20000))
  BEGIN
    DECLARE valueId   INT;

    SET valueId =
          insert_object_value(
            in_object_id, in_property_id, in_timestamp);

    INSERT INTO valuev(
                  value_id_fk, VALUE)
    VALUES (valueId, valuev);
  END|

DELIMITER ;


DROP PROCEDURE IF EXISTS insert_int_value;
DELIMITER |
CREATE PROCEDURE insert_int_value(
  in_object_id int, in_property_id int, in_timestamp long, valuei int)
  BEGIN
    DECLARE valueId   int;

    SET valueId =
          insert_object_value(
            in_object_id, in_property_id, in_timestamp);

    INSERT INTO valuei(
                  value_id_fk, value)
    VALUES (valueId, valuei);
  END|
DELIMITER ;




DROP PROCEDURE IF EXISTS transfer_privileges;
DROP TEMPORARY TABLE IF EXISTS migration_privileges;
Delimiter //
Create procedure transfer_privileges() 
Begin
	
	Declare prev_id INT(11);
	Declare name,description,tag Varchar(512);
	Declare system tinyint(1);
	Declare sortOrder smallInt(6) unsigned;
	Declare done INT DEFAULT 0;
	Declare cur_migr Cursor FOR SELECT * FROM privileges;
	Declare Continue Handler FOR NOT FOUND SET done = 1;
			
		
		open cur_migr;
label: LOOP
		Fetch cur_migr INTO prev_id,name,description,system,tag,sortOrder;
		if done then LEAVE label; END IF;
		
		Insert objects(object_type_id_fk) values(@type_id_user_privilege); 
		Set @last_id=last_insert_id();
		Insert into migration_privileges(id_new,id_prev) Values(@last_id,prev_id);
		
		Call insert_varchar_value(@last_id, @userPrivilegeNameAttr, @currentTime, name);
		IF description IS NOT NULL THEN
			Call insert_varchar_value(@last_id, @userPrivilegeDescriptionAttr, @currentTime, description);
		END IF;
		Call insert_int_value(@last_id, @userPrivilegeSystemAttr, @currentTime, system);
		IF description IS NOT NULL THEN
			Call insert_varchar_value(@last_id, @userPrivilegeTagAttr, @currentTime, tag);
		END IF;
		IF description IS NOT NULL THEN
			Call insert_int_value(@last_id, @userPrivilegeSortOrderAttr, @currentTime, sortOrder);
		END IF;		
		
END LOOP;
		close cur_migr;


		Relations:Begin
				Declare child_id,parent_id INT(11);
				Declare done2 INT DEFAULT 0;
				Declare cur_parents Cursor FOR SELECT * from privileges_parents;
				Declare Continue Handler FOR NOT FOUND SET done2 = 1;

				open cur_parents;
				set_relations: LOOP
					Fetch cur_parents INTO child_id,parent_id;
					if done2 then LEAVE set_relations; END IF;
					Select id_new into @child_id from migration_privileges where id_prev=child_id;
					Select id_new into @parent_id from migration_privileges where id_prev=parent_id;
					Insert into object_parents Values(@child_id,@parent_id);
				END LOOP;	
				close cur_parents;
		End Relations;


End //
Delimiter ;


DROP PROCEDURE IF EXISTS transfer_groups;
DROP TEMPORARY TABLE IF EXISTS migration_groups;
Delimiter //
Create procedure transfer_groups() 
Begin
	
	Declare prev_id INT(11);
	Declare statistics TEXT;
	Declare name VARCHAR(64);
	Declare description VARCHAR(256);
	Declare done INT DEFAULT 0;
	Declare cur_migr Cursor for select * from groups;
	Declare Continue Handler for NOT FOUND SET done = 1;
	
		
		open cur_migr;
label: LOOP
		
		Fetch cur_migr into prev_id,name,description,statistics;
		if done then LEAVE label; END IF;
		Insert objects(object_type_id_fk) values(@type_id_user_group); 
		Set @last_id = last_insert_id();
		Insert into migration_groups(id_new,id_prev) Values(@last_id,prev_id);

		Call insert_varchar_value(@last_id, @userGroupNameAttr, @currentTime, name);
		IF description IS NOT NULL THEN
			Call insert_varchar_value(@last_id, @userGroupDescriptionAttr, @currentTime, description);
		END IF;
		IF statistics IS NOT NULL THEN
			Call insert_varchar_value(@last_id, @userGroupStatisticsAttr, @currentTime, statistics);
		END IF;
		
		
END LOOP;
		close cur_migr;

		Relations:Begin
				Declare prv_id,grp_id INT(11);
				Declare done2 INT DEFAULT 0;
				Declare cur_parents Cursor FOR SELECT * from group_privileges;
				Declare Continue Handler FOR NOT FOUND SET done2 = 1;

				open cur_parents;
				set_relations: LOOP
					Fetch cur_parents INTO prv_id, grp_id;
					if done2 then LEAVE set_relations; END IF;
					Select id_new into @prv_id from migration_privileges where id_prev=prv_id;
					Select id_new into @grp_id from migration_groups where id_prev=grp_id;
					Insert into object_parents Values(@prv_id,@grp_id);
				END LOOP;	
				close cur_parents;
		End Relations;


End //
Delimiter ;


DROP PROCEDURE IF EXISTS transfer_preferences;
DROP TEMPORARY TABLE IF EXISTS migration_preferences;
Delimiter //
Create procedure transfer_preferences() 
Begin
	
	Declare prev_id INT(11);
	Declare name, value VARCHAR(1024);
	Declare done INT DEFAULT 0;
	Declare cur_migr Cursor for select up.preference_id,preference_name,preference_value from tbl_user_preferences up join tbl_user_preferences_assoc upa on(up.preference_id=upa.preference_id);
	Declare Continue Handler FOR NOT FOUND SET done = 1;
	
		
		
		open cur_migr;
label: LOOP
		Fetch cur_migr into prev_id,name,value;
		if done then LEAVE label; END IF;
		
		Insert objects(object_type_id_fk) values(@type_id_user_preference); 
		Set @last_id=last_insert_id();
		Insert into migration_preferences(id_new,id_prev) Values(@last_id,prev_id);
		
		
		Call insert_varchar_value(@last_id, @userPreferenceNameAttr, @currentTime, name);
		Call insert_varchar_value(@last_id, @userPreferenceValueAttr, @currentTime, value);
		
END LOOP;
		close cur_migr;


End //
Delimiter ;



DROP PROCEDURE IF EXISTS transfer_users;
DROP TEMPORARY TABLE IF EXISTS migration_users;
Delimiter //
Create procedure transfer_users() 
Begin
	
	Declare prev_id, group_id, preference_id, user_id INT(11);
	Declare userName,userPassword,userPasswordHint,userPasswordHintAnswer,FName,LName,middleInitial,title VARCHAR(256);
	Declare primaryPhonenumber, secondaryPhonenumber,primaryEmail,secondaryEmail, smsAddress, description VARCHAR(256);
	Declare done INT DEFAULT 0;
	Declare cur_migr Cursor FOR SELECT * FROM tbl_users;
	Declare Continue Handler FOR NOT FOUND SET done = 1;

		
		open cur_migr;
label: LOOP
		Fetch cur_migr into prev_id,userName,userPassword,userPasswordHint,userPasswordHintAnswer,FName,LName,middleInitial,title,primaryPhonenumber, secondaryPhonenumber,primaryEmail,secondaryEmail, smsAddress, description;
		if done then LEAVE label; END IF;
		
		
		INSERT objects(object_type_id_fk) Values(@type_id_user);
		Set @last_id=last_insert_id();
		INSERT INTO migration_users(id_new,id_prev) Values (@last_id,prev_id);

		Call insert_varchar_value(@last_id, @userNameAttr, @currentTime, userName);
		Call insert_varchar_value(@last_id, @userPasswordAttr, @currentTime, userPassword);
		Call insert_varchar_value(@last_id, @userPasswordHintAttr, @currentTime, userPasswordHint);
		Call insert_varchar_value(@last_id, @userPasswordHintAnswerAttr, @currentTime, userPasswordHintAnswer);
		Call insert_varchar_value(@last_id, @userFNameAttr, @currentTime, FName);
		Call insert_varchar_value(@last_id, @userLNameAttr, @currentTime, LName);
		Call insert_varchar_value(@last_id, @userMiddleInitialAttr, @currentTime, middleInitial);
		Call insert_varchar_value(@last_id, @userTitleAttr, @currentTime, userName);
		Call insert_varchar_value(@last_id, @userPrimaryPhoneNumberAttr, @currentTime, primaryPhonenumber);	
		Call insert_varchar_value(@last_id, @userSecondaryPhoneNumberAttr, @currentTime, secondaryPhonenumber);
		Call insert_varchar_value(@last_id, @userPrimaryEmailAttr, @currentTime, primaryEmail);	
		Call insert_varchar_value(@last_id, @userSecondaryEmailAttr, @currentTime, secondaryEmail);
		Call insert_varchar_value(@last_id, @userSmsAddressAttr, @currentTime, smsAddress);
		Call insert_varchar_value(@last_id, @userDescriptionAttr, @currentTime, description);


END LOOP;
		close cur_migr;



		Preference_Relations:Begin
				Declare done2 INT DEFAULT 0;
				Declare cur_parents Cursor FOR SELECT tupa.user_id,tupa.preference_id from tbl_user_preferences_assoc tupa;
				Declare Continue Handler FOR NOT FOUND SET done2 = 1;

				open cur_parents;
				set_relations: LOOP
					Fetch cur_parents INTO user_id,preference_id;
					if done2 then LEAVE set_relations; END IF;
					Select id_new into @user_id from migration_users where id_prev=user_id;
					Select id_new into @preference_id from migration_preferences where id_prev=preference_id;

					Call insert_link_value(@user_id,@userPreferencesAttr,@currentTime,@preference_id);
				END LOOP;	
				close cur_parents;
		End Preference_Relations;
		Group_Relations:Begin
				Declare done3 INT DEFAULT 0;
				Declare cur_parents Cursor FOR SELECT * from user_groups;
				Declare Continue Handler FOR NOT FOUND SET done3 = 1;

				open cur_parents;
				set_relations: LOOP
					Fetch cur_parents INTO user_id,group_id;
					if done3 then LEAVE set_relations; END IF;
					Select id_new into @user_id from migration_users where id_prev=user_id;
					Select id_new into @group_id from migration_groups where id_prev=group_id;
					Call insert_link_value(@user_id,@userGroupsAttr,@currentTime,@group_id);
				END LOOP;	
				close cur_parents;
		End Group_Relations;

End //
Delimiter ;

DROP PROCEDURE IF EXISTS update_customqueries_userID;
Delimiter //
Create procedure update_customqueries_userID() 
Begin
	
	Declare value_id,user_id INT(11);
	Declare done INT DEFAULT 0;
	Declare cur_migr Cursor FOR SELECT ov.value_id,vi.value FROM synap.objects ob 
									   join object_types ot on(ob.object_type_id_fk=ot.objtype_id)
									   join obj_values ov on(ob.id=ov.object_id_fk) 
									   join attributes atr on(attr_id_fk=attr_id) 
									   join valuei vi on(value_id_fk=ov.value_id) 
									   where ot.name='CUSTOMQUERY' and atr.name='userId';
	Declare Continue Handler FOR NOT FOUND SET done = 1;
	open cur_migr;

loop_update: LOOP
		Fetch cur_migr into value_id,user_id;
		IF done then LEAVE loop_update; END IF;
		Select id_new into @new_id from migration_users where id_prev=user_id; 
		Update valuei Set value=@new_id where value_id_fk=value_id;
END LOOP;
	close cur_migr;
END; //
Delimiter ;




Set @currentTime=UNIX_TIMESTAMP()*1000;


Create Temporary Table migration_users(id_new INT(11), id_prev INT(11) Primary Key);
Create Temporary table migration_groups(id_new INT(11), id_prev INT(11) Primary Key);
Create Temporary table migration_preferences(id_new INT(11), id_prev INT(11) Primary Key);
Create Temporary table migration_privileges(id_new INT(11), id_prev INT(11) Primary Key);

insert object_types( name) Values('USER_PRIVILEGE');
Set @type_id_user_privilege=last_insert_id();
insert attributes(name,object_type_id_fk,attr_type_id_fk) Values('name',@type_id_user_privilege,3);
Set @userPrivilegeNameAttr=last_insert_id();
insert attributes(name,object_type_id_fk,attr_type_id_fk) Values('description',@type_id_user_privilege,3);
Set @userPrivilegeDescriptionAttr=last_insert_id();
insert attributes(name,object_type_id_fk,attr_type_id_fk) Values('system',@type_id_user_privilege,1);
Set @userPrivilegeSystemAttr=last_insert_id();
insert attributes(name,object_type_id_fk,attr_type_id_fk) Values('tag',@type_id_user_privilege,3);
Set @userPrivilegeTagAttr=last_insert_id();
insert attributes(name,object_type_id_fk,attr_type_id_fk) Values('sortOrder',@type_id_user_privilege,1);
Set @userPrivilegeSortOrderAttr=last_insert_id();



insert object_types( name) Values('USER_GROUP');
Set @type_id_user_group=last_insert_id();
insert attributes(name,object_type_id_fk,attr_type_id_fk) Values('name',@type_id_user_group,3);
Set @userGroupNameAttr=last_insert_id();
insert attributes(name,object_type_id_fk,attr_type_id_fk) Values('description',@type_id_user_group,3);
Set @userGroupDescriptionAttr=last_insert_id();
insert attributes(name,object_type_id_fk,attr_type_id_fk) Values('statistics',@type_id_user_group,3);
Set @userGroupStatisticsAttr=last_insert_id();


insert object_types( name) Values('USER_PREFERENCE');
Set @type_id_user_preference=last_insert_id();
insert attributes(name,object_type_id_fk,attr_type_id_fk) Values('name',@type_id_user_preference,3);
Set @userPreferenceNameAttr=last_insert_id();
insert attributes(name,object_type_id_fk,attr_type_id_fk) Values('value',@type_id_user_preference,3);
Set @userPreferenceValueAttr=last_insert_id();


insert object_types( name) Values('USER');
Set @type_id_user=last_insert_id();
insert attributes(name,object_type_id_fk,attr_type_id_fk) Values('userName',@type_id_user,3);
Set @userNameAttr=last_insert_id();
insert attributes(name,object_type_id_fk,attr_type_id_fk) Values('userPassword',@type_id_user,3);
Set @userPasswordAttr=last_insert_id();
insert attributes(name,object_type_id_fk,attr_type_id_fk) Values('userPasswordHint',@type_id_user,3);
Set @userPasswordHintAttr=last_insert_id();
insert attributes(name,object_type_id_fk,attr_type_id_fk) Values('userPasswordHintAnswer',@type_id_user,3);
Set @userPasswordHintAnswerAttr=last_insert_id();
insert attributes(name,object_type_id_fk,attr_type_id_fk) Values('FName',@type_id_user,3);
Set @userFNameAttr=last_insert_id();
insert attributes(name,object_type_id_fk,attr_type_id_fk) Values('LName',@type_id_user,3);
Set @userLNameAttr=last_insert_id();
insert attributes(name,object_type_id_fk,attr_type_id_fk) Values('middleInitial',@type_id_user,3);
Set @userMiddleInitialAttr=last_insert_id();
insert attributes(name,object_type_id_fk,attr_type_id_fk) Values('title',@type_id_user,3);
Set @userTitleAttr=last_insert_id();
insert attributes(name,object_type_id_fk,attr_type_id_fk) Values('primaryPhonenumber',@type_id_user,3);
Set @userPrimaryPhoneNumberAttr=last_insert_id();
insert attributes(name,object_type_id_fk,attr_type_id_fk) Values('secondaryPhonenumber',@type_id_user,3);
Set @userSecondaryPhoneNumberAttr=last_insert_id();
insert attributes(name,object_type_id_fk,attr_type_id_fk) Values('primaryEmail',@type_id_user,3);
Set @userPrimaryEmailAttr=last_insert_id();
insert attributes(name,object_type_id_fk,attr_type_id_fk) Values('secondaryEmail',@type_id_user,3);
Set @userSecondaryEmailAttr=last_insert_id();
insert attributes(name,object_type_id_fk,attr_type_id_fk) Values('smsAddress',@type_id_user,3);
Set @userSmsAddressAttr=last_insert_id();
insert attributes(name,object_type_id_fk,attr_type_id_fk) Values('description',@type_id_user,3);
Set @userDescriptionAttr=last_insert_id();
insert into attributes(name, object_type_id_fk, attr_type_id_fk,historical,collection) values ('groups', @type_id_user, 5,0,1);
Set @userGroupsAttr=last_insert_id();
insert into attributes(name, object_type_id_fk, attr_type_id_fk,historical,collection) values ('preferences', @type_id_user, 5,0,1);
Set @userPreferencesAttr=last_insert_id();

Call transfer_privileges();
Call transfer_groups();
Call transfer_preferences();
Call transfer_users();
CALL update_customqueries_userID();

-- initital ids to start 
-- should be updated each time in addition to new INSERT INTO clause
SET @max_object_id := IFNULL((SELECT max(`id`)+1 FROM `objects`),1);
SET @max_value_id := IFNULL((SELECT max(`value_id`)+1 FROM `obj_values`),1);
SET @max_valued_id := IFNULL((SELECT max(`id`)+1 FROM `valued`),1);
SET @max_valuel_id := IFNULL((SELECT max(`id`)+1 FROM `valuel`),1);
SET @max_valuei_id := IFNULL((SELECT max(`id`)+1 FROM `valuei`),1);
SET @max_valuev_id := IFNULL((SELECT max(`id`)+1 FROM `valuev`),1);
SET @max_valuelink_id := IFNULL((SELECT max(`id`)+1 FROM `value_obj_link`),1);
SET @max_valueplink_id := IFNULL((SELECT max(`id`)+1 FROM `value_property_link`),1);
SET @max_tag_value_id := IFNULL((SELECT max(`value_id`)+1 FROM `tag_values`),1);
SET @max_group_value_id := 1;

delete from uid_table;
INSERT INTO  `uid_table` (`object_id`,`value_id`,`valued_id`,`valuev_id`,`valuei_id`,`valuel_id`,`valuelink_id`,`valueplink_id`,`tag_value_id`,`group_id`) values (@max_object_id,@max_value_id,@max_valued_id,@max_valuev_id,@max_valuei_id,@max_valuel_id,@max_valuelink_id,@max_valueplink_id,@max_tag_value_id,@max_group_value_id);



DROP PROCEDURE IF EXISTS transfer_privileges;
DROP PROCEDURE IF EXISTS transfer_groups;
DROP PROCEDURE IF EXISTS transfer_preferences;
DROP PROCEDURE IF EXISTS transfer_users;
DROP PROCEDURE IF EXISTS update_customqueries_userID; 

DROP PROCEDURE IF EXISTS insert_link_value;
DROP PROCEDURE IF EXISTS add_link_value;
DROP FUNCTION IF EXISTS insert_object_value;
DROP PROCEDURE IF EXISTS insert_varchar_value;
DROP PROCEDURE IF EXISTS insert_int_value;

DROP TABLE IF EXISTS group_privileges;
DROP TABLE IF EXISTS user_groups;
DROP TABLE IF EXISTS tbl_user_preferences_assoc;
DROP TABLE IF EXISTS privileges_parents;
DROP TABLE IF EXISTS tbl_users;
DROP TABLE IF EXISTS tbl_user_preferences;
DROP TABLE IF EXISTS groups;
DROP TABLE IF EXISTS privileges; 


