-- Bug# 9057
-- Change NHT info table structure. Since this table stores debug information only, 
-- we don't need to preserve and convert data.

use synap;

-- Drop table TBL_NHT_INFO
drop table if exists `tbl_nht_info`;

create table `tbl_nht_info` (
  `id` int(11) not null auto_increment,
  `node_id` int(11),
  `node_id_hex` varchar(10),
  `num_neigh`  int(11),
  `report_timestamp` datetime,
  `neigh_id_hex` varchar(10),
  `rssi` varchar(20),
  `state` int(11),
  `hop` int(11),
  `hoptm` int(11),
  primary key(`id`)
)
engine=innodb;