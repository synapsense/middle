use synap;

-- [10036] Change Battery Life calculations and alerts to support the Bongo node
-- * Update description for low battery alert
-- * Add new alert for critical battery alert
-- NOTE: This script can be run multiple times without duplicating data or failing.

-- Update description for low battery alert
select object_id into @alert_type_id from all_objects_props_values where type_name = 'ALERT_TYPE' and propname = 'name' and val = 'Low battery';
select val_id into @val_id from all_objects_props_values where object_id = @alert_type_id && propname = 'description';
update valuev set value = 'Triggers when battery status becomes low' where value_id_fk = @val_id;

-- Define convenience functions and procedures
DROP FUNCTION IF EXISTS insert_object_value;
DELIMITER |
CREATE FUNCTION insert_object_value(in_object_id int, in_property_id int, in_timestamp long)
  RETURNS INTEGER
  BEGIN
    set @id=0;
    SELECT value_id into @id from obj_values where object_id_fk=in_object_id and attr_id_fk=in_property_id limit 1;
    IF @id!=0 THEN RETURN @id; END IF;
    INSERT INTO obj_values(object_id_fk, attr_id_fk, TIMESTAMP) VALUES (in_object_id, in_property_id, in_timestamp);
    RETURN last_insert_id();
  END|
DELIMITER ;

DROP PROCEDURE IF EXISTS insert_varchar_value;
DELIMITER |
CREATE PROCEDURE insert_varchar_value(in_object_id int, in_property_id int, in_timestamp long, valuev varchar(20000))
  BEGIN
    DECLARE valueId INT;
    SET valueId = insert_object_value(in_object_id, in_property_id, in_timestamp);
    INSERT INTO valuev(value_id_fk, VALUE) VALUES (valueId, valuev);
  END|
DELIMITER ;

DROP PROCEDURE IF EXISTS insert_int_value;
DELIMITER |
CREATE PROCEDURE insert_int_value(in_object_id int, in_property_id int, in_timestamp long, valuei int)
  BEGIN
    DECLARE valueId int;
    SET valueId = insert_object_value(in_object_id, in_property_id, in_timestamp);
    INSERT INTO valuei(value_id_fk, value) VALUES (valueId, valuei);
  END|
DELIMITER ;

DROP PROCEDURE IF EXISTS insert_link_value;
DELIMITER |
CREATE PROCEDURE insert_link_value(in_object_id int, in_property_id int, in_timestamp long, value_link int)
  BEGIN
    DECLARE valueId int;
    DECLARE typeName varchar(300);
    SET valueId = insert_object_value(in_object_id, in_property_id, in_timestamp);
    CALL add_link_value(valueId, in_timestamp, value_link);
  END|
DELIMITER ;

DROP PROCEDURE IF EXISTS add_link_value;
DELIMITER |
CREATE PROCEDURE add_link_value(valueId int, in_timestamp long, value_link int)
  BEGIN
    DECLARE typeName  varchar(300);
    IF (value_link IS NULL) THEN
      INSERT INTO value_obj_link(value_id_fk, object_id,object_type) VALUES (valueId, NULL, NULL);
    ELSE
      SELECT ot.NAME INTO typeName FROM objects o , object_types ot WHERE o.object_type_id_fk = ot.objtype_id AND o.id = value_link;
      INSERT INTO value_obj_link(value_id_fk, object_id, object_type) VALUES (valueId, value_link, typeName);
    END IF;
  END|
DELIMITER ;

-- Add alert for critical battery alert
drop procedure if exists SafeInsertCriticalBatteryAlert;
delimiter |
create procedure SafeInsertCriticalBatteryAlert()
  begin
    if (select object_id from all_objects_props_values where type_name = 'ALERT_TYPE' and propname = 'name' and val = 'Critical battery') is null then
      -- Find common parameters
      select objtype_id into @alertTypeId from object_types where name = 'ALERT_TYPE';
      set @currentTime = UNIX_TIMESTAMP() * 1000;
      select attr_id into @templatesPropertyId from attributes where object_type_id_fk = @alertTypeId and name = 'templates';
      -- Add the alert
      insert into objects(object_type_id_fk) values (@alertTypeId);
      set @alertObjId = last_insert_id();
      -- Set various properties
      call insert_varchar_value(@alertObjId, (select attr_id from attributes where object_type_id_fk = @alertTypeId and name = 'name'), @currentTime, 'Critical battery');
      call insert_varchar_value(@alertObjId, (select attr_id from attributes where object_type_id_fk = @alertTypeId and name = 'displayName'), @currentTime, 'Critical battery');
      call insert_varchar_value(@alertObjId, (select attr_id from attributes where object_type_id_fk = @alertTypeId and name = 'description'), @currentTime, 'Triggers when battery status becomes critical');
      call insert_int_value(@alertObjId, (select attr_id from attributes where object_type_id_fk = @alertTypeId and name = 'active'), @currentTime, 1);
      call insert_int_value(@alertObjId, (select attr_id from attributes where object_type_id_fk = @alertTypeId and name = 'autoAck'), @currentTime, 0);
      call insert_int_value(@alertObjId, (select attr_id from attributes where object_type_id_fk = @alertTypeId and name = 'autoDismiss'), @currentTime, 0);
      call insert_int_value(@alertObjId, (select attr_id from attributes where object_type_id_fk = @alertTypeId and name = 'autoDismissAllowed'), @currentTime, 0);
      call insert_link_value(@alertObjId, (select attr_id from attributes where object_type_id_fk = @alertTypeId and name = 'priority'), @currentTime, (select object_id from all_objects_props_values where type_name = 'ALERT_PRIORITY' and val = 'CRITICAL'));
      call insert_link_value(@alertObjId, @templatesPropertyId, @currentTime, (select object_id from all_objects_props_values where type_name = 'ALERT_MESSAGE_TEMPLATE' and val = 'Console System Notification'));
      call insert_link_value(@alertObjId, @templatesPropertyId, @currentTime, (select object_id from all_objects_props_values where type_name = 'ALERT_MESSAGE_TEMPLATE' and val = 'Windows Event Log System Notification'));
    end if;
  end|
delimiter ;

call SafeInsertCriticalBatteryAlert();

-- Clean up
drop procedure if exists SafeInsertCriticalBatteryAlert;
drop procedure if exists insert_varchar_value;
drop procedure if exists insert_int_value;
drop procedure if exists insert_link_value;
drop procedure if exists add_link_value;
drop function if exists insert_object_value;
