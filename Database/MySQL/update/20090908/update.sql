use `synap`;

ALTER TABLE `tbl_alert_type` ADD COLUMN `auto_dismiss` SMALLINT UNSIGNED NOT NULL DEFAULT 0 COMMENT 'auto dismiss flag' AFTER `is_active`;