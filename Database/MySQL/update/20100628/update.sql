use `synap`;

UPDATE tbl_user_preferences_assoc SET preference_value=CONCAT("\"",preference_value,"\"") WHERE preference_id IN ( SELECT preference_id FROM tbl_user_preferences WHERE preference_name NOT IN ("LiveDataRefreshRate","SessionExpirationTimeout","DecimalFormat"));
