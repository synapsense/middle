-- Bug#8750
-- The script adds a new table for storing historical binary values

use synap;

-- Drop table HISTORICAL_BINARY_VALUES
drop table if exists `historical_binary_values`;

create table `historical_binary_values` (
 `value_id_fk` int(11) not null,
 `timestamp` datetime not null,
 `valueb` mediumblob,
 index `idx_value_id_timestamp_binary`(`value_id_fk`, `timestamp`)
)
engine=innodb;
