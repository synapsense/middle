use `synap`;

-- Drop table tag_values
drop table if exists `uid_table`;

create table `uid_table` (
	`object_id` int(11) not null,
	`value_id` int(11) not null,
	`valued_id` int(11) not null,
	`valuev_id` int(11) not null,
	`valuei_id` int(11) not null,
	`valuel_id` int(11) not null,
	`valuelink_id` int(11) not null,
	`valueplink_id` int(11) not null
)
engine=innodb
comment = 'Stores certain tables ids used by key generation algorithm';

SET @max_object_id := IFNULL((SELECT max(`id`)+1 FROM `objects`),1);
SET @max_value_id := IFNULL((SELECT max(`value_id`)+1 FROM `obj_values`),1);
SET @max_valued_id := IFNULL((SELECT max(`id`)+1 FROM `valued`),1);
SET @max_valuel_id := IFNULL((SELECT max(`id`)+1 FROM `valuel`),1);
SET @max_valuei_id := IFNULL((SELECT max(`id`)+1 FROM `valuei`),1);
SET @max_valuev_id := IFNULL((SELECT max(`id`)+1 FROM `valuev`),1);
SET @max_valuelink_id := IFNULL((SELECT max(`id`)+1 FROM `value_obj_link`),1);
SET @max_valueplink_id := IFNULL((SELECT max(`id`)+1 FROM `value_property_link`),1);

INSERT INTO  `uid_table` (`object_id`,`value_id`,`valued_id`,`valuev_id`,`valuei_id`,`valuel_id`,`valuelink_id`,`valueplink_id`) values (@max_object_id,@max_value_id,@max_valued_id,@max_valuev_id,@max_valuei_id,@max_valuel_id,@max_valuelink_id,@max_valueplink_id);
