//max number of partitions per table 
MAX_PARTITIONS_NUMBER = 1024

//the last partition range border
LAST_PARTITION_DATE = inParentheses("2100-01-01");

// utility functions
function usage() { 
   return "cscript <script_name> <partition_number:integer> <template_file_name:string> <output_file:string>";
}

function readFileContent(fileName) { 
try { 
   objFSO =WScript.CreateObject("Scripting.FileSystemObject");
   objFile = objFSO.OpenTextFile(fileName, 1);
   //reads its content
   templateContent = objFile.ReadAll();
} finally { 
   objFile.Close();
}
return templateContent;
} 

function writeFileContent(fineName,content) {
try { 
   //opens the specified file
   objFSO =WScript.CreateObject("Scripting.FileSystemObject");
   //saves file with the new content
   objFile = objFSO.CreateTextFile(fineName, 2);
   objFile.WriteLine(sql);
} finally { 
   objFile.Close();
}
}

function EOL(source) { 
  return source + "\n";
}

function getDay(date) { 
  return date.getDate();	
}

function getMonth(date) { 
   return date.getMonth()+1;
}

function getYear(date) { 
   return date.getYear();
}

// domain specific functions

function inParentheses(expression) { 
   return "'" + expression + "'";
}

function clueDateFormatedString(year , month , day){ 
 return inParentheses(year + "-" + month + "-" + day);
}

// builds an array of dates by the specified date - dt 
// returns array of size = size
function getDatesArray(dt,size,shift) {  
  cDate = new Date(dt);
  cDate.setDate(cDate.getDate()+shift);
  partitionRanges = new Array();
  for (var i=1;i<size;i++)  { 
    cYear  = getYear(cDate);
    cMonth = getMonth(cDate);	
    cDay = getDay(cDate);
    partitionRanges[i]= clueDateFormatedString(cYear,cMonth,cDay);
    cDate.setDate(cDate.getDate()+1);
  }

  partitionRanges[size]= LAST_PARTITION_DATE;

  return partitionRanges;
} //end function getDatesArray(dt,size) 

function getEventStartDate(date,partitionsNum) {
     localDate = new Date(date);
     localDate.setDate(getDay(localDate)+partitionsNum);
     return clueDateFormatedString(getYear(localDate),getMonth(localDate),localDate.getDate());
}

function alterTablePartitionByRange(tableName,columnName,partitionRanges) { 
  alertTableClause = EOL("ALTER TABLE \`" + tableName + "\` PARTITION BY RANGE(TO_DAYS(\`" + columnName + "\`)) ( ");
  
  for (var i=1;i<partitionRanges.length;i++)  {
       alertTableClause = EOL(alertTableClause); 
       alertTableClause += "partition p" + i + " values LESS THAN (to_days(" +partitionRanges[i] + "))";
       if (i != partitionRanges.length-1) { alertTableClause += ","};
  }; 

  alertTableClause = EOL(alertTableClause);
  alertTableClause += ");";

return EOL(alertTableClause);

}

function USE(dbName) { 
   return  "USE " + dbName + ";";
}

//main// 

tooManyPartitionsError = new Error ("Maximum number of partition per table is 1024. Correct input argument!");

//current date (script executing time)
currentDate = new Date();

//number of partitions to generate
partitionsNumber = parseInt(WSH.Arguments(0));
if (isNaN(partitionsNumber)) { 
  throw new Error("Illegal partitionsNumber argument , it should be an integer");
}

if ( (partitionsNumber) > 1024 ) { 
  throw tooManyPartitionsError;
}

shiftDays = parseInt(WSH.Arguments(1));
if (isNaN(shiftDays)) { 
  throw new Error("Illegal shiftDays argument , it should be an integer");
}

//gets partitions ranges as an array of formatted dates
partitionRanges = getDatesArray(currentDate,partitionsNumber,shiftDays);

//firstly go inside synap
sql = USE("synap");
sql = EOL(sql);
sql += alterTablePartitionByRange("historical_values","timestamp",partitionRanges);
sql = EOL(sql);

strInputTemplateFileName = WSH.Arguments(2);
sql += readFileContent(strInputTemplateFileName);

//event first start date , formatted, consider days shifted
eventStartDate = getEventStartDate(currentDate,partitionsNumber+shiftDays-2);

//replacing it in template
sql = sql.replace( new RegExp("%event_start"  ,"g"), eventStartDate);

//write generated file
strOutputFileName = WSH.Arguments(3);
writeFileContent(strOutputFileName,sql);



