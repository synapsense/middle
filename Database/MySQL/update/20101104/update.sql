-- this update refactors CRE database structure by removing unnecessary many-to-many association between objects and rules and removing unnecessary tables


-- update TBL_RULE_INSTANCES by adding 2 new columns 

ALTER TABLE `TBL_RULE_INSTANCES` ADD 
   COLUMN `object_id_FK` int(11) NOT NULL DEFAULT '0';

ALTER TABLE `TBL_RULE_INSTANCES` ADD 
   COLUMN `attr_id_FK` int(11) NOT NULL DEFAULT '0';

ALTER TABLE `TBL_RULE_INSTANCES` DROP 
   COLUMN `propagating`;


-- create and call a procedure that copies the object and attribute keys from TBL_RULE_PROPERTY_DEST_ASSOC to TBL_RULE_INSTANCES

DROP PROCEDURE IF EXISTS update_rule_instances;

DELIMITER |
CREATE PROCEDURE update_rule_instances()
BEGIN
  DECLARE done INT DEFAULT 0;
  DECLARE ruleId, objectId, attrId INT;
  DECLARE destCur CURSOR FOR SELECT rule_id_fk, object_id_fk, attr_id_FK FROM TBL_RULE_PROPERTY_DEST_ASSOC;
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

  OPEN destCur;

  main_loop: LOOP
    FETCH destCur INTO ruleId, objectId, attrId;
    IF done THEN
      LEAVE main_loop;
    END IF;
    UPDATE TBL_RULE_INSTANCES r SET r.object_id_FK = objectId, r.attr_id_FK = attrId WHERE r.rule_id = ruleId; 
  END LOOP;
 
  CLOSE destCur;
  
  -- delete all the records that didn't have destination properties (deleted rules)
  DELETE FROM TBL_RULE_PROPERTY_SRC_ASSOC WHERE rule_id_FK IN (SELECT rule_id FROM  TBL_RULE_INSTANCES WHERE object_id_FK = 0);
  DELETE FROM TBL_RULE_INSTANCES WHERE object_id_FK = 0;

END; 

SET autocommit = 0;
CALL update_rule_instances();
COMMIT;


-- turn on foreign key constraints and the new index

ALTER TABLE `TBL_RULE_INSTANCES` ADD
   CONSTRAINT `Ref_55` FOREIGN KEY (`object_id_FK`)
    REFERENCES `OBJECTS`(`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION;

ALTER TABLE `TBL_RULE_INSTANCES` ADD
  CONSTRAINT `Ref_60` FOREIGN KEY (`attr_id_FK`)
    REFERENCES `ATTRIBUTES`(`attr_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION;

ALTER TABLE `TBL_RULE_INSTANCES` ADD 
  UNIQUE INDEX `object_attr_uniq`(`object_id_FK`, `attr_id_FK`);

-- drop unnecessary tables and the procedure

DROP TABLE TBL_RULE_PROPERTY_DEST_ASSOC;
DROP TABLE TBL_RULE_INSTANCE_CONSTANTS;
DROP PROCEDURE update_rule_instances;
