use `synap`;

-- Drop Event clear_expired_data
drop event if exists `clear_expired_data`;

delimiter |
create event `clear_expired_data`
  on schedule every '1' month starts '2009-10-01 00:01:00'
  enable
do
begin
  delete from tbl_nht_info
   where report_timestamp <= date_sub(curdate(),interval 1 month);
  delete from tbl_network_statnode
   where nwk_timestamp <= date_sub(curdate(),interval 3 month);
  delete from tbl_activity_log where `timestamp` <= date_sub(curdate(),interval 1 month);
  delete from tbl_battery_capacity where battery_capacity_time_stamp <= date_sub(curdate(),interval 1 month);
  delete from tbl_network_statbase where nwk_timestamp <= date_sub(curdate(),interval 3 month);
 end; |
delimiter ;
