use synap;

START TRANSACTION;
update tbl_visualization_legend_point set position = 0.028 where id = 18;
update tbl_visualization_legend_point set position = 0.03 where id = 19;
update tbl_visualization_legend_point set position = 0.04 where id = 20;
COMMIT;