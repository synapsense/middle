use synap;

select objtype_id into @alertTypeId from object_types where name = 'ALERT' limit 1;

insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('lastNTierName', @alertTypeId, 3);
    
    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('lastAttempt', @alertTypeId, 1);