use `synap`;

insert into `TBL_ALERT_TYPE` (`name`,`description`,`priority`,`header`,`body`,`is_active`) values ("Communication error","Triggers when one or more data points can not be acquired",0,"Communication error","$message$",1);
