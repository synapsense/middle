use synap;

SET FOREIGN_KEY_CHECKS=0;

-- -----------------------------------------------------
-- Table `privileges`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `privileges` ;

CREATE  TABLE IF NOT EXISTS `privileges` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(50) NOT NULL ,
  `description` VARCHAR(300) NULL ,
  `system` TINYINT(1)  NOT NULL DEFAULT 0 ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `name_UNIQUE` (`name` ASC) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `groups`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `groups` ;

CREATE  TABLE IF NOT EXISTS `groups` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(50) NOT NULL ,
  `description` VARCHAR(300) NULL ,
  UNIQUE INDEX `name_UNIQUE` (`name` ASC) ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `user_groups`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `user_groups` ;

CREATE  TABLE IF NOT EXISTS `user_groups` (
  `usr_id` INT(11) NOT NULL ,
  `grp_id` INT(11) NOT NULL ,
  PRIMARY KEY (`usr_id`, `grp_id`) ,
  INDEX `fk_usr_id` (`usr_id` ASC) ,
  INDEX `fk_grp_id` (`grp_id` ASC) ,
  CONSTRAINT `fk_usr_id`
    FOREIGN KEY (`usr_id` )
    REFERENCES `tbl_users` (`user_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_grp_id`
    FOREIGN KEY (`grp_id` )
    REFERENCES `groups` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `group_privileges`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `group_privileges` ;

CREATE  TABLE IF NOT EXISTS `group_privileges` (
  `prv_id` INT(11) NOT NULL ,
  `grp_id` INT(11) NOT NULL ,
  PRIMARY KEY (`prv_id`, `grp_id`) ,
  INDEX `fk_prv_id` (`prv_id` ASC) ,
  INDEX `fk1_grp_id` (`grp_id` ASC) ,
  CONSTRAINT `fk_prv_id`
    FOREIGN KEY (`prv_id` )
    REFERENCES `privileges` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk1_grp_id`
    FOREIGN KEY (`grp_id` )
    REFERENCES `groups` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

SET FOREIGN_KEY_CHECKS=1;
