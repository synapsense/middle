use synap;

update valuei set value = 0 
where value_id_fk = 
(select ov.value_id from object_types ot, attributes a, obj_values ov 
where a.object_type_id_fk = ot.objtype_id
and ov.attr_id_fk  = a.attr_id
and a.name = 'notificationAttempts'
and ot.name = 'ALERT_PRIORITY_TIER'
and ov.object_id_fk = 
(select vol.object_id from objects o, object_types ot, attributes a, obj_values ov, value_obj_link vol
where o.object_type_id_fk = ot.objtype_id
and a.object_type_id_fk = ot.objtype_id
and ov.object_id_fk = o.id
and ov.attr_id_fk  = a.attr_id
and ov.value_id = vol.value_id_fk
and a.name = 'tiers'
and ot.name = 'ALERT_PRIORITY'
and o.id = (select o.id from objects o, object_types ot, attributes a, obj_values ov, valuev vv
where o.object_type_id_fk = ot.objtype_id
and a.object_type_id_fk = ot.objtype_id
and ov.object_id_fk = o.id
and ov.attr_id_fk  = a.attr_id
and ov.value_id = vv.value_id_fk
and a.name = 'name'
and ot.name = 'ALERT_PRIORITY'
and vv.value = 'INFORMATIONAL')))