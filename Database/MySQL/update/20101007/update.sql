-- add 'version' property to the ROOT type and set it to the version 6.0 for this update
SET @type_id := (SELECT `objtype_id` FROM `object_types` WHERE `name`="ROOT");
SET @obj_id := (SELECT `id` FROM `objects` WHERE `object_type_id_fk` = @type_id AND `static`=0);
start transaction;
INSERT INTO `attributes` (`name`, `object_type_id_fk`, `attr_type_id_fk`) VALUES ("version",@type_id,3);
INSERT INTO `obj_values` (`object_id_fk`,`attr_id_fk`, `timestamp`) VALUES (@obj_id,last_insert_id(),unix_timestamp()*1000);
INSERT INTO `valuev` (`value`,`value_id_fk`) VALUES ("6.0",last_insert_id());
commit;
