-- Bug#9140
-- The script adds new privileges for Reporting actions

use synap;

insert into privileges (id, name, description, system, tag, sort_order) values (97,'SCHEDULE_CUSTOM_QUERIES', 'Schedule Custom Queries',2, 'REPORT_MANAGEMENT', 3801);
insert into privileges (id, name, description, system, tag, sort_order) values (98,'MANAGE_REPORT_TEMPLATES', 'Manage Report Templates',2, 'REPORT_MANAGEMENT', 3802);
insert into privileges (id, name, description, system, tag, sort_order) values (99,'VIEW_REPORTS', 'View Reports',2, 'REPORT_MANAGEMENT', 3803);
insert into privileges (id, name, description, system, tag, sort_order) values (100,'DELETE_REPORTS', 'Delete Reports',2, 'REPORT_MANAGEMENT', 3804);

INSERT INTO privileges_parents(`privilege_id`,`parent_id`) VALUES(3,97);
INSERT INTO privileges_parents(`privilege_id`,`parent_id`) VALUES(5,97);
INSERT INTO privileges_parents(`privilege_id`,`parent_id`) VALUES(6,97);
INSERT INTO privileges_parents(`privilege_id`,`parent_id`) VALUES(10,97);
INSERT INTO privileges_parents(`privilege_id`,`parent_id`) VALUES(3,98);
INSERT INTO privileges_parents(`privilege_id`,`parent_id`) VALUES(5,98);
INSERT INTO privileges_parents(`privilege_id`,`parent_id`) VALUES(6,98);
INSERT INTO privileges_parents(`privilege_id`,`parent_id`) VALUES(10,98);
INSERT INTO privileges_parents(`privilege_id`,`parent_id`) VALUES(9,99);
INSERT INTO privileges_parents(`privilege_id`,`parent_id`) VALUES(3,100);
INSERT INTO privileges_parents(`privilege_id`,`parent_id`) VALUES(10,100);

insert into group_privileges(`prv_id`,`grp_id`) values (99,2);
insert into group_privileges(`prv_id`,`grp_id`) values (99,3);
