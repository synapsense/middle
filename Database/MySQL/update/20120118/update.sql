use synap;

drop procedure if exists migrate_historical_data;
drop table if exists tbl_sensor_data;
drop table if exists historical_values_old;
drop table if exists temp_historical_transfer_log;