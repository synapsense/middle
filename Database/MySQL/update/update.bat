@echo off
echo ********************************************
echo Updating database schema(%date%, %time%)...

echo Reading input arguments...
set mysqluser=%1
echo MySQL user=%mysqluser%
set password=%2
echo password=%password%
set scripts_path=.
set source_es_version=%3
echo ES version=%source_es_version%
set db_schema=synap 

echo Getting current ES version from database...
set es_ver=
call:get_es_version es_ver

rem check that current ES version is not speficied. 
rem This can be true for ES releases prior to 6.0
if %es_ver% EQU undefined  (
	echo Current ES version is undefined
	rem In this case input parameter with ES version must be specified
	if not defined source_es_version (
		echo.Cannot update database. Please specify source ES version parameter upon next run
		goto exit
	)
) else (
  	 echo Current ES version is %es_ver%	
  	 rem In this case both ES versions should be equal
	 if defined source_es_version (
		if "%es_ver%" NEQ "%source_es_version%"  (
			echo.Cannot update database. Mismatching ES versions: Current [%es_ver%], specified [%source_es_version%]
			goto exit
		)
	) else (
 		set source_es_version=%es_ver%
	)			
)

rem in any case mysql.ini should be updated
echo Checking/updating database configuration...
 cscript ../db_config.vbs
  if ERRORLEVEL 1 goto exit
echo Historical data archiving is enabled
 
if %source_es_version%==5.0.0 goto update_1
if %source_es_version%==5.0.1 goto update_1 
if %source_es_version%==5.0.2 goto update_1 
if %source_es_version%==5.0.3 goto update_2
if %source_es_version%==5.0.4 goto update_2
if %source_es_version%==5.1 goto update_3
if %source_es_version%==5.2 goto update_4
if %source_es_version%==5.3 goto update_5
if %source_es_version%==6.0 goto update_6
if %source_es_version%==6.1 goto update_7
if %source_es_version%==6.1.0 goto update_7
if %source_es_version%==6.1.1 goto update_8
if %source_es_version%==6.2 goto update_9
if %source_es_version%==6.3 goto update_10
if %source_es_version%==6.4 goto update_11

rem if version is not supported                                                                                                                        
echo Nothing to update since %source_es_version% is the latest ES version
echo or it is not supported
echo Please use one from the list : 5.0.0, 5.0.1, 5.0.2, 5.0.3, 5.0.4, 5.1, 5.2, 5.3, 6.0, 6.1, 6.1.0, 6.1.1, 6.2, 6.3, 6.4

goto exit

rem from 5.0 to latest 5.0.3
:update_1
 echo Updating database to the latest 5.0.x version
 for /f %%a in (5.0.0.txt) do ( 
     if not exist %scripts_path%/%%a ( 
     echo %scripts_path%/%%a file is missing. Database update failed! 
     goto exit
    )
 )

 for /f %%a in (5.0.0.txt) do ( 
     mysql --user=%mysqluser% -p%password% < %scripts_path%/%%a 
 )

echo Database was updated to the latest 5.0.x version...

rem Then from 5.0.3 to 5.1
:update_2
 echo Updating database from 5.0.x to 5.1

 for /f %%a in (5.0.3.txt) do ( 
     if not exist %scripts_path%/%%a ( 
     echo %scripts_path%/%%a file is missing. Database update failed! 
     goto exit
    )
 )
 
 for /f %%a in (5.0.3.txt) do ( 
     mysql --user=%mysqluser% -p%password% %db_schema%< %scripts_path%/%%a 
 )

echo Generating appropriate partitioning schema...
rem the first argument is number of partitions ,then number of subpartitions, then input template and the last one is sql script to enable concrete partitioning schema
rem don't forget that  ("number of partitions" * "number of subpartitions")<=1024

rem getting start date of the first partition
if exist date.txt ( del date.txt )

mysql --user=%mysqluser% -p%password% < select_start_date.sql | findstr "\/2009$" > date.txt

rem defining new variable with gotten date as value
set /p start_date= <date.txt

rem empty file checking for a case when database has not partitioned table tbl_sensor_data
for /f %%a in (date.txt) do (goto do_partitions)
rem if date.txt is empty                                           
echo There are no partitions in the current database schema. Current date will be used as first partition start date! 

:do_partitions
cscript createPartitioningSchema.js 18 50 partitioning_template.sql partitioning_schema.sql %start_date% 

echo Enabling partitioning ...
mysql --user=%mysqluser% -p%password% < partitioning_schema.sql 
echo Partitioning enabled
echo Database was updated to 5.1 version... 

rem Then from 5.1 to 5.2
:update_3
 echo Updating database from 5.1 to 5.2

 for /f %%a in (5.1.txt) do ( 
     if not exist %scripts_path%/%%a ( 
     echo %scripts_path%/%%a file is missing. Database update failed! 
     goto exit
    )
 )
 
 for /f %%a in (5.1.txt) do ( 
     mysql --user=%mysqluser% -p%password% %db_schema%< %scripts_path%/%%a 
 )

echo Database was updated to 5.2 version 

rem Then from 5.2 to 5.3
:update_4
 echo Updating database from 5.2 to 5.3

 for /f %%a in (5.2.txt) do ( 
     if not exist %scripts_path%/%%a ( 
     echo %scripts_path%/%%a file is missing. Database update failed! 
     goto exit
    )
 )
 
 for /f %%a in (5.2.txt) do ( 
     mysql --force --user=%mysqluser% -p%password% %db_schema%< %scripts_path%/%%a 
 )

echo Database was updated to 5.3 version 

rem Then from 5.3 to 6.0.0
:update_5
 echo Updating database from 5.3 to 6.0.0
 rem set this to indicate that historical_values has been updated

 for /f %%a in (5.3.txt) do ( 
     if not exist %scripts_path%/%%a ( 
     echo %scripts_path%/%%a file is missing. Database update failed! 
     goto exit
    )
 )
 
pushd %scripts_path%\wsn_migration\
call migrate %mysqluser% %password% %db_schema%
 if errorlevel 1 (
    echo Can not execute WSN migration. Database update failed!
    goto exit
 )
 popd 

 for /f %%a in (5.3.txt) do ( 
     mysql --force --user=%mysqluser% -p%password% %db_schema%< %scripts_path%/%%a 
 )

echo Re-partitioning historical_values table ...

set days_to_shift=-1
call:get_numofdays_since_install days_to_shift

echo Building partitioning script
cscript createPartitioningSchema.js 540 %days_to_shift% partitioning_template.sql "%scripts_path%/partitioning_schema.sql" > nul
echo Partitioning schema file is created and placed into %scripts_path% directory
if not exist %scripts_path%/partitioning_schema.sql (
    echo Failed to create partitioning upgrade script. Database update failed!
    goto exit
)

echo Setting new partitioning on historical_values table
mysql --user=%mysqluser% -p%password% %db_schema% < "%scripts_path%/partitioning_schema.sql"

echo Database was updated to 6.0.0 version 

rem Then from 6.0 to 6.1
:update_6
 echo Updating database from 6.0 to 6.1

 for /f %%a in (6.0.txt) do ( 
     if not exist %scripts_path%/%%a ( 
     echo %scripts_path%/%%a file is missing. Database update failed! 
     goto exit
    )
 )
 
 for /f %%a in (6.0.txt) do ( 
     mysql --force --user=%mysqluser% -p%password% %db_schema%< %scripts_path%/%%a 
 )

echo Database was updated to 6.1 version 

rem Then from 6.1 to 6.1.1
:update_7
 echo Updating database from 6.1 to 6.1.1

 for /f %%a in (6.1.0.txt) do ( 
     if not exist %scripts_path%/%%a ( 
     echo %scripts_path%/%%a file is missing. Database update failed! 
     goto exit
    )
 )

 for /f %%a in (6.1.0.txt) do ( 
     mysql --force --user=%mysqluser% -p%password% %db_schema%< %scripts_path%/%%a 
 )


rem Then from 6.1.1 to 6.2
:update_8
 echo Updating database from 6.1.1 to 6.2

 for /f %%a in (6.1.1.txt) do ( 
     if not exist %scripts_path%/%%a ( 
     echo %scripts_path%/%%a file is missing. Database update failed! 
     goto exit
    )
 )

pushd %scripts_path%\historical_update\

setlocal enableextensions 
for /f "tokens=*" %%a in ( 
'mysql -uroot -pdbuser synap -e"select 1 from information_schema.events where event_name='migrate_historical_data_event'"'
) do ( 
set updateFrom53=%%a 
) 

if defined updateFrom53 (
    rem just update table struct
	mysql --user=%mysqluser% -p%password% %db_schema% -e"ALTER TABLE `historical_values`  ADD COLUMN `group_id` INT(11) DEFAULT NULL  AFTER `valuev` , CHANGE COLUMN `valuev` `valuev` VARCHAR(21822) DEFAULT NULL;"
) else (
    rem table update + extra steps - scheduling historical_values copy event
	call hist %mysqluser% %password% %db_schema%
)

if errorlevel 1 (
		echo Failed to execute historical_values migration scripts. Database update failed!
		goto exit
)
endlocal 

popd

pushd %scripts_path%\alert_migrate\
call migrate %mysqluser% %password% %db_schema%
 if errorlevel 1 (
    echo Failed to execute Alert schema migration scripts. Database update failed!
    goto exit
 )
popd
 
for /f %%a in (6.1.1.txt) do ( 
     mysql --force --user=%mysqluser% -p%password% %db_schema%< %scripts_path%/%%a 
)

echo Database was updated to 6.2 version 

rem Then from 6.2 to 6.3
:update_9
 echo Updating database from 6.2 to 6.3
  
 for /f %%a in (6.2.txt) do ( 
     if not exist %scripts_path%/%%a ( 
     echo %scripts_path%/%%a file is missing. Database update failed! 
     goto exit
    )
 )
 
 for /f %%a in (6.2.txt) do ( 
     mysql --force --user=%mysqluser% -p%password% %db_schema%< %scripts_path%/%%a 
 )

rem Then from 6.3 to 6.4
:update_10
 echo Updating database from 6.3 to 6.4
 
 for /f %%a in (6.3.txt) do ( 
     if not exist %scripts_path%/%%a ( 
     echo %scripts_path%/%%a file is missing. Database update failed! 
     goto exit
    )
 )
 
 for /f %%a in (6.3.txt) do ( 
     mysql --force --user=%mysqluser% -p%password% %db_schema%< %scripts_path%/%%a 
 )
 
rem Then from 6.4 to 6.5
:update_11
echo Updating database from 6.4 to 6.5
 
 for /f %%a in (6.4.txt) do ( 
     if not exist %scripts_path%/%%a ( 
     echo %scripts_path%/%%a file is missing. Database update failed! 
     goto exit
    )
 )
 
 for /f %%a in (6.4.txt) do ( 
     mysql --force --user=%mysqluser% -p%password% %db_schema%< %scripts_path%/%%a 
 )
 
rem Call ES version update script
mysql --user=%mysqluser% -p%password% %db_schema%< %scripts_path%/update_es_version.sql

rem Call uid_table(id counters storage) update script
mysql --user=%mysqluser% -p%password% %db_schema%< %scripts_path%/update_uid.sql

echo Database was updated to 6.5 version 

echo The system database update was completed successfully 
rem Removing temp file
if exist date.txt ( 
  del date.txt
)

:exit
echo Exiting(%date%, %time%)... 
pause
exit

rem function to get current ES version from database ROOT.version
:get_es_version
setlocal enableextensions 
set "CURRENT_ES_VER=undefined"
for /f "tokens=*" %%a in ( 
'mysql --user=%mysqluser% -p%password% %db_schema% -e"select v.value as '' from object_types ot, objects o, attributes a, obj_values ov, valuev v where ot.objtype_id=o.object_type_id_fk and ov.object_id_fk = o.id and ov.attr_id_fk = a.attr_id and ov.value_id = v.value_id_fk and ot.name = 'ROOT' and a.name = 'version' and o.static = 0"'
) do ( 
set "CURRENT_ES_VER=%%a"
) 

(endlocal
 set "%~1=%CURRENT_ES_VER%"
)
goto:eof

rem function to get number of days since historical_values table partitioning install
:get_numofdays_since_install
setlocal enableextensions 
set "NUM=-1"
for /f "tokens=*" %%a in ( 
'mysql -uroot -pdbuser synap -e"SELECT case when DATEDIFF(from_days(p.partition_description),now()) >= 0 then -1 when DATEDIFF(from_days(p.partition_description), now()) <= -540 then -540 else DATEDIFF(from_days(p.partition_description), now()) end as '' FROM information_schema.PARTITIONS P where p.table_schema = 'synap' and p.table_name = 'historical_values' order by p.partition_ordinal_position limit 1"'
) do ( 
set "NUM=%%a"
) 

(endlocal
 set "%~1=%NUM%"
)
goto:eof