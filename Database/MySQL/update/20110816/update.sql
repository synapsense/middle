use `synap`;

ALTER TABLE `tbl_activity_log` DROP FOREIGN KEY `activity_record_refers_object` ;

ALTER TABLE `tbl_activity_log` 
  ADD CONSTRAINT `activity_record_refers_object`
  FOREIGN KEY (`object_id_fk` )
  REFERENCES `objects` (`id` )
  ON DELETE SET NULL
  ON UPDATE NO ACTION;