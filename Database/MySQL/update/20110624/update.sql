use `synap`;

alter table `uid_table` add column `tag_value_id` INT(11)  not null after `valueplink_id`;
SET @max_tag_value_id := IFNULL((SELECT max(`value_id`)+1 FROM `tag_values`),1);

update `uid_table` set tag_value_id = @max_tag_value_id;
