ALTER TABLE `synap`.`tbl_sensor_node` MODIFY COLUMN `current_battery_capacity` DOUBLE(15,3) DEFAULT NULL COMMENT 'currnet_battery_capacity',
MODIFY COLUMN `remaining_days` INTEGER UNSIGNED DEFAULT NULL;
