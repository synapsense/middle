truncate table historical_values;
drop index idx_value_id_timestamp on historical_values;
alter table historical_values modify group_id int(11) not null default 0;
create unique index idx_value_id_timestamp on historical_values (value_id_fk, timestamp, group_id);

