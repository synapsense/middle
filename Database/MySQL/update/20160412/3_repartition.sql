
-- Clean up old / renamed stored procedures
DROP PROCEDURE IF EXISTS `GET_PARTITION_NUMBER`;
DROP PROCEDURE IF EXISTS `GET_PARTITION_INFO`;

alter table historical_values remove partitioning;

set @today = to_days(now());
set @recreateHistoricalPartitionsStmt = concat(
        'alter table `historical_values` partition by range(to_days(`timestamp`)) (',
        'partition p1 values less than (', @today - 718, '),',
        'partition p2 values less than (', @today - 717, '),',
        'partition p3 values less than (', @today - 716, '),',
        'partition p4 values less than (', @today - 715, '),',
        'partition p5 values less than (', @today - 714, '),',
        'partition p6 values less than (', @today - 713, '),',
        'partition p7 values less than (', @today - 712, '),',
        'partition p8 values less than (', @today - 711, '),',
        'partition p9 values less than (', @today - 710, '),',
        'partition p10 values less than (', @today - 709, '),',
        'partition p11 values less than (', @today - 708, '),',
        'partition p12 values less than (', @today - 707, '),',
        'partition p13 values less than (', @today - 706, '),',
        'partition p14 values less than (', @today - 705, '),',
        'partition p15 values less than (', @today - 704, '),',
        'partition p16 values less than (', @today - 703, '),',
        'partition p17 values less than (', @today - 702, '),',
        'partition p18 values less than (', @today - 701, '),',
        'partition p19 values less than (', @today - 700, '),',
        'partition p20 values less than (', @today - 699, '),',
        'partition p21 values less than (', @today - 698, '),',
        'partition p22 values less than (', @today - 697, '),',
        'partition p23 values less than (', @today - 696, '),',
        'partition p24 values less than (', @today - 695, '),',
        'partition p25 values less than (', @today - 694, '),',
        'partition p26 values less than (', @today - 693, '),',
        'partition p27 values less than (', @today - 692, '),',
        'partition p28 values less than (', @today - 691, '),',
        'partition p29 values less than (', @today - 690, '),',
        'partition p30 values less than (', @today - 689, '),',
        'partition p31 values less than (', @today - 688, '),',
        'partition p32 values less than (', @today - 687, '),',
        'partition p33 values less than (', @today - 686, '),',
        'partition p34 values less than (', @today - 685, '),',
        'partition p35 values less than (', @today - 684, '),',
        'partition p36 values less than (', @today - 683, '),',
        'partition p37 values less than (', @today - 682, '),',
        'partition p38 values less than (', @today - 681, '),',
        'partition p39 values less than (', @today - 680, '),',
        'partition p40 values less than (', @today - 679, '),',
        'partition p41 values less than (', @today - 678, '),',
        'partition p42 values less than (', @today - 677, '),',
        'partition p43 values less than (', @today - 676, '),',
        'partition p44 values less than (', @today - 675, '),',
        'partition p45 values less than (', @today - 674, '),',
        'partition p46 values less than (', @today - 673, '),',
        'partition p47 values less than (', @today - 672, '),',
        'partition p48 values less than (', @today - 671, '),',
        'partition p49 values less than (', @today - 670, '),',
        'partition p50 values less than (', @today - 669, '),',
        'partition p51 values less than (', @today - 668, '),',
        'partition p52 values less than (', @today - 667, '),',
        'partition p53 values less than (', @today - 666, '),',
        'partition p54 values less than (', @today - 665, '),',
        'partition p55 values less than (', @today - 664, '),',
        'partition p56 values less than (', @today - 663, '),',
        'partition p57 values less than (', @today - 662, '),',
        'partition p58 values less than (', @today - 661, '),',
        'partition p59 values less than (', @today - 660, '),',
        'partition p60 values less than (', @today - 659, '),',
        'partition p61 values less than (', @today - 658, '),',
        'partition p62 values less than (', @today - 657, '),',
        'partition p63 values less than (', @today - 656, '),',
        'partition p64 values less than (', @today - 655, '),',
        'partition p65 values less than (', @today - 654, '),',
        'partition p66 values less than (', @today - 653, '),',
        'partition p67 values less than (', @today - 652, '),',
        'partition p68 values less than (', @today - 651, '),',
        'partition p69 values less than (', @today - 650, '),',
        'partition p70 values less than (', @today - 649, '),',
        'partition p71 values less than (', @today - 648, '),',
        'partition p72 values less than (', @today - 647, '),',
        'partition p73 values less than (', @today - 646, '),',
        'partition p74 values less than (', @today - 645, '),',
        'partition p75 values less than (', @today - 644, '),',
        'partition p76 values less than (', @today - 643, '),',
        'partition p77 values less than (', @today - 642, '),',
        'partition p78 values less than (', @today - 641, '),',
        'partition p79 values less than (', @today - 640, '),',
        'partition p80 values less than (', @today - 639, '),',
        'partition p81 values less than (', @today - 638, '),',
        'partition p82 values less than (', @today - 637, '),',
        'partition p83 values less than (', @today - 636, '),',
        'partition p84 values less than (', @today - 635, '),',
        'partition p85 values less than (', @today - 634, '),',
        'partition p86 values less than (', @today - 633, '),',
        'partition p87 values less than (', @today - 632, '),',
        'partition p88 values less than (', @today - 631, '),',
        'partition p89 values less than (', @today - 630, '),',
        'partition p90 values less than (', @today - 629, '),',
        'partition p91 values less than (', @today - 628, '),',
        'partition p92 values less than (', @today - 627, '),',
        'partition p93 values less than (', @today - 626, '),',
        'partition p94 values less than (', @today - 625, '),',
        'partition p95 values less than (', @today - 624, '),',
        'partition p96 values less than (', @today - 623, '),',
        'partition p97 values less than (', @today - 622, '),',
        'partition p98 values less than (', @today - 621, '),',
        'partition p99 values less than (', @today - 620, '),',
        'partition p100 values less than (', @today - 619, '),',
        'partition p101 values less than (', @today - 618, '),',
        'partition p102 values less than (', @today - 617, '),',
        'partition p103 values less than (', @today - 616, '),',
        'partition p104 values less than (', @today - 615, '),',
        'partition p105 values less than (', @today - 614, '),',
        'partition p106 values less than (', @today - 613, '),',
        'partition p107 values less than (', @today - 612, '),',
        'partition p108 values less than (', @today - 611, '),',
        'partition p109 values less than (', @today - 610, '),',
        'partition p110 values less than (', @today - 609, '),',
        'partition p111 values less than (', @today - 608, '),',
        'partition p112 values less than (', @today - 607, '),',
        'partition p113 values less than (', @today - 606, '),',
        'partition p114 values less than (', @today - 605, '),',
        'partition p115 values less than (', @today - 604, '),',
        'partition p116 values less than (', @today - 603, '),',
        'partition p117 values less than (', @today - 602, '),',
        'partition p118 values less than (', @today - 601, '),',
        'partition p119 values less than (', @today - 600, '),',
        'partition p120 values less than (', @today - 599, '),',
        'partition p121 values less than (', @today - 598, '),',
        'partition p122 values less than (', @today - 597, '),',
        'partition p123 values less than (', @today - 596, '),',
        'partition p124 values less than (', @today - 595, '),',
        'partition p125 values less than (', @today - 594, '),',
        'partition p126 values less than (', @today - 593, '),',
        'partition p127 values less than (', @today - 592, '),',
        'partition p128 values less than (', @today - 591, '),',
        'partition p129 values less than (', @today - 590, '),',
        'partition p130 values less than (', @today - 589, '),',
        'partition p131 values less than (', @today - 588, '),',
        'partition p132 values less than (', @today - 587, '),',
        'partition p133 values less than (', @today - 586, '),',
        'partition p134 values less than (', @today - 585, '),',
        'partition p135 values less than (', @today - 584, '),',
        'partition p136 values less than (', @today - 583, '),',
        'partition p137 values less than (', @today - 582, '),',
        'partition p138 values less than (', @today - 581, '),',
        'partition p139 values less than (', @today - 580, '),',
        'partition p140 values less than (', @today - 579, '),',
        'partition p141 values less than (', @today - 578, '),',
        'partition p142 values less than (', @today - 577, '),',
        'partition p143 values less than (', @today - 576, '),',
        'partition p144 values less than (', @today - 575, '),',
        'partition p145 values less than (', @today - 574, '),',
        'partition p146 values less than (', @today - 573, '),',
        'partition p147 values less than (', @today - 572, '),',
        'partition p148 values less than (', @today - 571, '),',
        'partition p149 values less than (', @today - 570, '),',
        'partition p150 values less than (', @today - 569, '),',
        'partition p151 values less than (', @today - 568, '),',
        'partition p152 values less than (', @today - 567, '),',
        'partition p153 values less than (', @today - 566, '),',
        'partition p154 values less than (', @today - 565, '),',
        'partition p155 values less than (', @today - 564, '),',
        'partition p156 values less than (', @today - 563, '),',
        'partition p157 values less than (', @today - 562, '),',
        'partition p158 values less than (', @today - 561, '),',
        'partition p159 values less than (', @today - 560, '),',
        'partition p160 values less than (', @today - 559, '),',
        'partition p161 values less than (', @today - 558, '),',
        'partition p162 values less than (', @today - 557, '),',
        'partition p163 values less than (', @today - 556, '),',
        'partition p164 values less than (', @today - 555, '),',
        'partition p165 values less than (', @today - 554, '),',
        'partition p166 values less than (', @today - 553, '),',
        'partition p167 values less than (', @today - 552, '),',
        'partition p168 values less than (', @today - 551, '),',
        'partition p169 values less than (', @today - 550, '),',
        'partition p170 values less than (', @today - 549, '),',
        'partition p171 values less than (', @today - 548, '),',
        'partition p172 values less than (', @today - 547, '),',
        'partition p173 values less than (', @today - 546, '),',
        'partition p174 values less than (', @today - 545, '),',
        'partition p175 values less than (', @today - 544, '),',
        'partition p176 values less than (', @today - 543, '),',
        'partition p177 values less than (', @today - 542, '),',
        'partition p178 values less than (', @today - 541, '),',
        'partition p179 values less than (', @today - 540, '),',
        'partition p180 values less than (', @today - 539, '),',
        'partition p181 values less than (', @today - 538, '),',
        'partition p182 values less than (', @today - 537, '),',
        'partition p183 values less than (', @today - 536, '),',
        'partition p184 values less than (', @today - 535, '),',
        'partition p185 values less than (', @today - 534, '),',
        'partition p186 values less than (', @today - 533, '),',
        'partition p187 values less than (', @today - 532, '),',
        'partition p188 values less than (', @today - 531, '),',
        'partition p189 values less than (', @today - 530, '),',
        'partition p190 values less than (', @today - 529, '),',
        'partition p191 values less than (', @today - 528, '),',
        'partition p192 values less than (', @today - 527, '),',
        'partition p193 values less than (', @today - 526, '),',
        'partition p194 values less than (', @today - 525, '),',
        'partition p195 values less than (', @today - 524, '),',
        'partition p196 values less than (', @today - 523, '),',
        'partition p197 values less than (', @today - 522, '),',
        'partition p198 values less than (', @today - 521, '),',
        'partition p199 values less than (', @today - 520, '),',
        'partition p200 values less than (', @today - 519, '),',
        'partition p201 values less than (', @today - 518, '),',
        'partition p202 values less than (', @today - 517, '),',
        'partition p203 values less than (', @today - 516, '),',
        'partition p204 values less than (', @today - 515, '),',
        'partition p205 values less than (', @today - 514, '),',
        'partition p206 values less than (', @today - 513, '),',
        'partition p207 values less than (', @today - 512, '),',
        'partition p208 values less than (', @today - 511, '),',
        'partition p209 values less than (', @today - 510, '),',
        'partition p210 values less than (', @today - 509, '),',
        'partition p211 values less than (', @today - 508, '),',
        'partition p212 values less than (', @today - 507, '),',
        'partition p213 values less than (', @today - 506, '),',
        'partition p214 values less than (', @today - 505, '),',
        'partition p215 values less than (', @today - 504, '),',
        'partition p216 values less than (', @today - 503, '),',
        'partition p217 values less than (', @today - 502, '),',
        'partition p218 values less than (', @today - 501, '),',
        'partition p219 values less than (', @today - 500, '),',
        'partition p220 values less than (', @today - 499, '),',
        'partition p221 values less than (', @today - 498, '),',
        'partition p222 values less than (', @today - 497, '),',
        'partition p223 values less than (', @today - 496, '),',
        'partition p224 values less than (', @today - 495, '),',
        'partition p225 values less than (', @today - 494, '),',
        'partition p226 values less than (', @today - 493, '),',
        'partition p227 values less than (', @today - 492, '),',
        'partition p228 values less than (', @today - 491, '),',
        'partition p229 values less than (', @today - 490, '),',
        'partition p230 values less than (', @today - 489, '),',
        'partition p231 values less than (', @today - 488, '),',
        'partition p232 values less than (', @today - 487, '),',
        'partition p233 values less than (', @today - 486, '),',
        'partition p234 values less than (', @today - 485, '),',
        'partition p235 values less than (', @today - 484, '),',
        'partition p236 values less than (', @today - 483, '),',
        'partition p237 values less than (', @today - 482, '),',
        'partition p238 values less than (', @today - 481, '),',
        'partition p239 values less than (', @today - 480, '),',
        'partition p240 values less than (', @today - 479, '),',
        'partition p241 values less than (', @today - 478, '),',
        'partition p242 values less than (', @today - 477, '),',
        'partition p243 values less than (', @today - 476, '),',
        'partition p244 values less than (', @today - 475, '),',
        'partition p245 values less than (', @today - 474, '),',
        'partition p246 values less than (', @today - 473, '),',
        'partition p247 values less than (', @today - 472, '),',
        'partition p248 values less than (', @today - 471, '),',
        'partition p249 values less than (', @today - 470, '),',
        'partition p250 values less than (', @today - 469, '),',
        'partition p251 values less than (', @today - 468, '),',
        'partition p252 values less than (', @today - 467, '),',
        'partition p253 values less than (', @today - 466, '),',
        'partition p254 values less than (', @today - 465, '),',
        'partition p255 values less than (', @today - 464, '),',
        'partition p256 values less than (', @today - 463, '),',
        'partition p257 values less than (', @today - 462, '),',
        'partition p258 values less than (', @today - 461, '),',
        'partition p259 values less than (', @today - 460, '),',
        'partition p260 values less than (', @today - 459, '),',
        'partition p261 values less than (', @today - 458, '),',
        'partition p262 values less than (', @today - 457, '),',
        'partition p263 values less than (', @today - 456, '),',
        'partition p264 values less than (', @today - 455, '),',
        'partition p265 values less than (', @today - 454, '),',
        'partition p266 values less than (', @today - 453, '),',
        'partition p267 values less than (', @today - 452, '),',
        'partition p268 values less than (', @today - 451, '),',
        'partition p269 values less than (', @today - 450, '),',
        'partition p270 values less than (', @today - 449, '),',
        'partition p271 values less than (', @today - 448, '),',
        'partition p272 values less than (', @today - 447, '),',
        'partition p273 values less than (', @today - 446, '),',
        'partition p274 values less than (', @today - 445, '),',
        'partition p275 values less than (', @today - 444, '),',
        'partition p276 values less than (', @today - 443, '),',
        'partition p277 values less than (', @today - 442, '),',
        'partition p278 values less than (', @today - 441, '),',
        'partition p279 values less than (', @today - 440, '),',
        'partition p280 values less than (', @today - 439, '),',
        'partition p281 values less than (', @today - 438, '),',
        'partition p282 values less than (', @today - 437, '),',
        'partition p283 values less than (', @today - 436, '),',
        'partition p284 values less than (', @today - 435, '),',
        'partition p285 values less than (', @today - 434, '),',
        'partition p286 values less than (', @today - 433, '),',
        'partition p287 values less than (', @today - 432, '),',
        'partition p288 values less than (', @today - 431, '),',
        'partition p289 values less than (', @today - 430, '),',
        'partition p290 values less than (', @today - 429, '),',
        'partition p291 values less than (', @today - 428, '),',
        'partition p292 values less than (', @today - 427, '),',
        'partition p293 values less than (', @today - 426, '),',
        'partition p294 values less than (', @today - 425, '),',
        'partition p295 values less than (', @today - 424, '),',
        'partition p296 values less than (', @today - 423, '),',
        'partition p297 values less than (', @today - 422, '),',
        'partition p298 values less than (', @today - 421, '),',
        'partition p299 values less than (', @today - 420, '),',
        'partition p300 values less than (', @today - 419, '),',
        'partition p301 values less than (', @today - 418, '),',
        'partition p302 values less than (', @today - 417, '),',
        'partition p303 values less than (', @today - 416, '),',
        'partition p304 values less than (', @today - 415, '),',
        'partition p305 values less than (', @today - 414, '),',
        'partition p306 values less than (', @today - 413, '),',
        'partition p307 values less than (', @today - 412, '),',
        'partition p308 values less than (', @today - 411, '),',
        'partition p309 values less than (', @today - 410, '),',
        'partition p310 values less than (', @today - 409, '),',
        'partition p311 values less than (', @today - 408, '),',
        'partition p312 values less than (', @today - 407, '),',
        'partition p313 values less than (', @today - 406, '),',
        'partition p314 values less than (', @today - 405, '),',
        'partition p315 values less than (', @today - 404, '),',
        'partition p316 values less than (', @today - 403, '),',
        'partition p317 values less than (', @today - 402, '),',
        'partition p318 values less than (', @today - 401, '),',
        'partition p319 values less than (', @today - 400, '),',
        'partition p320 values less than (', @today - 399, '),',
        'partition p321 values less than (', @today - 398, '),',
        'partition p322 values less than (', @today - 397, '),',
        'partition p323 values less than (', @today - 396, '),',
        'partition p324 values less than (', @today - 395, '),',
        'partition p325 values less than (', @today - 394, '),',
        'partition p326 values less than (', @today - 393, '),',
        'partition p327 values less than (', @today - 392, '),',
        'partition p328 values less than (', @today - 391, '),',
        'partition p329 values less than (', @today - 390, '),',
        'partition p330 values less than (', @today - 389, '),',
        'partition p331 values less than (', @today - 388, '),',
        'partition p332 values less than (', @today - 387, '),',
        'partition p333 values less than (', @today - 386, '),',
        'partition p334 values less than (', @today - 385, '),',
        'partition p335 values less than (', @today - 384, '),',
        'partition p336 values less than (', @today - 383, '),',
        'partition p337 values less than (', @today - 382, '),',
        'partition p338 values less than (', @today - 381, '),',
        'partition p339 values less than (', @today - 380, '),',
        'partition p340 values less than (', @today - 379, '),',
        'partition p341 values less than (', @today - 378, '),',
        'partition p342 values less than (', @today - 377, '),',
        'partition p343 values less than (', @today - 376, '),',
        'partition p344 values less than (', @today - 375, '),',
        'partition p345 values less than (', @today - 374, '),',
        'partition p346 values less than (', @today - 373, '),',
        'partition p347 values less than (', @today - 372, '),',
        'partition p348 values less than (', @today - 371, '),',
        'partition p349 values less than (', @today - 370, '),',
        'partition p350 values less than (', @today - 369, '),',
        'partition p351 values less than (', @today - 368, '),',
        'partition p352 values less than (', @today - 367, '),',
        'partition p353 values less than (', @today - 366, '),',
        'partition p354 values less than (', @today - 365, '),',
        'partition p355 values less than (', @today - 364, '),',
        'partition p356 values less than (', @today - 363, '),',
        'partition p357 values less than (', @today - 362, '),',
        'partition p358 values less than (', @today - 361, '),',
        'partition p359 values less than (', @today - 360, '),',
        'partition p360 values less than (', @today - 359, '),',
        'partition p361 values less than (', @today - 358, '),',
        'partition p362 values less than (', @today - 357, '),',
        'partition p363 values less than (', @today - 356, '),',
        'partition p364 values less than (', @today - 355, '),',
        'partition p365 values less than (', @today - 354, '),',
        'partition p366 values less than (', @today - 353, '),',
        'partition p367 values less than (', @today - 352, '),',
        'partition p368 values less than (', @today - 351, '),',
        'partition p369 values less than (', @today - 350, '),',
        'partition p370 values less than (', @today - 349, '),',
        'partition p371 values less than (', @today - 348, '),',
        'partition p372 values less than (', @today - 347, '),',
        'partition p373 values less than (', @today - 346, '),',
        'partition p374 values less than (', @today - 345, '),',
        'partition p375 values less than (', @today - 344, '),',
        'partition p376 values less than (', @today - 343, '),',
        'partition p377 values less than (', @today - 342, '),',
        'partition p378 values less than (', @today - 341, '),',
        'partition p379 values less than (', @today - 340, '),',
        'partition p380 values less than (', @today - 339, '),',
        'partition p381 values less than (', @today - 338, '),',
        'partition p382 values less than (', @today - 337, '),',
        'partition p383 values less than (', @today - 336, '),',
        'partition p384 values less than (', @today - 335, '),',
        'partition p385 values less than (', @today - 334, '),',
        'partition p386 values less than (', @today - 333, '),',
        'partition p387 values less than (', @today - 332, '),',
        'partition p388 values less than (', @today - 331, '),',
        'partition p389 values less than (', @today - 330, '),',
        'partition p390 values less than (', @today - 329, '),',
        'partition p391 values less than (', @today - 328, '),',
        'partition p392 values less than (', @today - 327, '),',
        'partition p393 values less than (', @today - 326, '),',
        'partition p394 values less than (', @today - 325, '),',
        'partition p395 values less than (', @today - 324, '),',
        'partition p396 values less than (', @today - 323, '),',
        'partition p397 values less than (', @today - 322, '),',
        'partition p398 values less than (', @today - 321, '),',
        'partition p399 values less than (', @today - 320, '),',
        'partition p400 values less than (', @today - 319, '),',
        'partition p401 values less than (', @today - 318, '),',
        'partition p402 values less than (', @today - 317, '),',
        'partition p403 values less than (', @today - 316, '),',
        'partition p404 values less than (', @today - 315, '),',
        'partition p405 values less than (', @today - 314, '),',
        'partition p406 values less than (', @today - 313, '),',
        'partition p407 values less than (', @today - 312, '),',
        'partition p408 values less than (', @today - 311, '),',
        'partition p409 values less than (', @today - 310, '),',
        'partition p410 values less than (', @today - 309, '),',
        'partition p411 values less than (', @today - 308, '),',
        'partition p412 values less than (', @today - 307, '),',
        'partition p413 values less than (', @today - 306, '),',
        'partition p414 values less than (', @today - 305, '),',
        'partition p415 values less than (', @today - 304, '),',
        'partition p416 values less than (', @today - 303, '),',
        'partition p417 values less than (', @today - 302, '),',
        'partition p418 values less than (', @today - 301, '),',
        'partition p419 values less than (', @today - 300, '),',
        'partition p420 values less than (', @today - 299, '),',
        'partition p421 values less than (', @today - 298, '),',
        'partition p422 values less than (', @today - 297, '),',
        'partition p423 values less than (', @today - 296, '),',
        'partition p424 values less than (', @today - 295, '),',
        'partition p425 values less than (', @today - 294, '),',
        'partition p426 values less than (', @today - 293, '),',
        'partition p427 values less than (', @today - 292, '),',
        'partition p428 values less than (', @today - 291, '),',
        'partition p429 values less than (', @today - 290, '),',
        'partition p430 values less than (', @today - 289, '),',
        'partition p431 values less than (', @today - 288, '),',
        'partition p432 values less than (', @today - 287, '),',
        'partition p433 values less than (', @today - 286, '),',
        'partition p434 values less than (', @today - 285, '),',
        'partition p435 values less than (', @today - 284, '),',
        'partition p436 values less than (', @today - 283, '),',
        'partition p437 values less than (', @today - 282, '),',
        'partition p438 values less than (', @today - 281, '),',
        'partition p439 values less than (', @today - 280, '),',
        'partition p440 values less than (', @today - 279, '),',
        'partition p441 values less than (', @today - 278, '),',
        'partition p442 values less than (', @today - 277, '),',
        'partition p443 values less than (', @today - 276, '),',
        'partition p444 values less than (', @today - 275, '),',
        'partition p445 values less than (', @today - 274, '),',
        'partition p446 values less than (', @today - 273, '),',
        'partition p447 values less than (', @today - 272, '),',
        'partition p448 values less than (', @today - 271, '),',
        'partition p449 values less than (', @today - 270, '),',
        'partition p450 values less than (', @today - 269, '),',
        'partition p451 values less than (', @today - 268, '),',
        'partition p452 values less than (', @today - 267, '),',
        'partition p453 values less than (', @today - 266, '),',
        'partition p454 values less than (', @today - 265, '),',
        'partition p455 values less than (', @today - 264, '),',
        'partition p456 values less than (', @today - 263, '),',
        'partition p457 values less than (', @today - 262, '),',
        'partition p458 values less than (', @today - 261, '),',
        'partition p459 values less than (', @today - 260, '),',
        'partition p460 values less than (', @today - 259, '),',
        'partition p461 values less than (', @today - 258, '),',
        'partition p462 values less than (', @today - 257, '),',
        'partition p463 values less than (', @today - 256, '),',
        'partition p464 values less than (', @today - 255, '),',
        'partition p465 values less than (', @today - 254, '),',
        'partition p466 values less than (', @today - 253, '),',
        'partition p467 values less than (', @today - 252, '),',
        'partition p468 values less than (', @today - 251, '),',
        'partition p469 values less than (', @today - 250, '),',
        'partition p470 values less than (', @today - 249, '),',
        'partition p471 values less than (', @today - 248, '),',
        'partition p472 values less than (', @today - 247, '),',
        'partition p473 values less than (', @today - 246, '),',
        'partition p474 values less than (', @today - 245, '),',
        'partition p475 values less than (', @today - 244, '),',
        'partition p476 values less than (', @today - 243, '),',
        'partition p477 values less than (', @today - 242, '),',
        'partition p478 values less than (', @today - 241, '),',
        'partition p479 values less than (', @today - 240, '),',
        'partition p480 values less than (', @today - 239, '),',
        'partition p481 values less than (', @today - 238, '),',
        'partition p482 values less than (', @today - 237, '),',
        'partition p483 values less than (', @today - 236, '),',
        'partition p484 values less than (', @today - 235, '),',
        'partition p485 values less than (', @today - 234, '),',
        'partition p486 values less than (', @today - 233, '),',
        'partition p487 values less than (', @today - 232, '),',
        'partition p488 values less than (', @today - 231, '),',
        'partition p489 values less than (', @today - 230, '),',
        'partition p490 values less than (', @today - 229, '),',
        'partition p491 values less than (', @today - 228, '),',
        'partition p492 values less than (', @today - 227, '),',
        'partition p493 values less than (', @today - 226, '),',
        'partition p494 values less than (', @today - 225, '),',
        'partition p495 values less than (', @today - 224, '),',
        'partition p496 values less than (', @today - 223, '),',
        'partition p497 values less than (', @today - 222, '),',
        'partition p498 values less than (', @today - 221, '),',
        'partition p499 values less than (', @today - 220, '),',
        'partition p500 values less than (', @today - 219, '),',
        'partition p501 values less than (', @today - 218, '),',
        'partition p502 values less than (', @today - 217, '),',
        'partition p503 values less than (', @today - 216, '),',
        'partition p504 values less than (', @today - 215, '),',
        'partition p505 values less than (', @today - 214, '),',
        'partition p506 values less than (', @today - 213, '),',
        'partition p507 values less than (', @today - 212, '),',
        'partition p508 values less than (', @today - 211, '),',
        'partition p509 values less than (', @today - 210, '),',
        'partition p510 values less than (', @today - 209, '),',
        'partition p511 values less than (', @today - 208, '),',
        'partition p512 values less than (', @today - 207, '),',
        'partition p513 values less than (', @today - 206, '),',
        'partition p514 values less than (', @today - 205, '),',
        'partition p515 values less than (', @today - 204, '),',
        'partition p516 values less than (', @today - 203, '),',
        'partition p517 values less than (', @today - 202, '),',
        'partition p518 values less than (', @today - 201, '),',
        'partition p519 values less than (', @today - 200, '),',
        'partition p520 values less than (', @today - 199, '),',
        'partition p521 values less than (', @today - 198, '),',
        'partition p522 values less than (', @today - 197, '),',
        'partition p523 values less than (', @today - 196, '),',
        'partition p524 values less than (', @today - 195, '),',
        'partition p525 values less than (', @today - 194, '),',
        'partition p526 values less than (', @today - 193, '),',
        'partition p527 values less than (', @today - 192, '),',
        'partition p528 values less than (', @today - 191, '),',
        'partition p529 values less than (', @today - 190, '),',
        'partition p530 values less than (', @today - 189, '),',
        'partition p531 values less than (', @today - 188, '),',
        'partition p532 values less than (', @today - 187, '),',
        'partition p533 values less than (', @today - 186, '),',
        'partition p534 values less than (', @today - 185, '),',
        'partition p535 values less than (', @today - 184, '),',
        'partition p536 values less than (', @today - 183, '),',
        'partition p537 values less than (', @today - 182, '),',
        'partition p538 values less than (', @today - 181, '),',
        'partition p539 values less than (', @today - 180, '),',
        'partition p540 values less than (', @today - 179, '),',
        'partition p541 values less than (', @today - 178, '),',
        'partition p542 values less than (', @today - 177, '),',
        'partition p543 values less than (', @today - 176, '),',
        'partition p544 values less than (', @today - 175, '),',
        'partition p545 values less than (', @today - 174, '),',
        'partition p546 values less than (', @today - 173, '),',
        'partition p547 values less than (', @today - 172, '),',
        'partition p548 values less than (', @today - 171, '),',
        'partition p549 values less than (', @today - 170, '),',
        'partition p550 values less than (', @today - 169, '),',
        'partition p551 values less than (', @today - 168, '),',
        'partition p552 values less than (', @today - 167, '),',
        'partition p553 values less than (', @today - 166, '),',
        'partition p554 values less than (', @today - 165, '),',
        'partition p555 values less than (', @today - 164, '),',
        'partition p556 values less than (', @today - 163, '),',
        'partition p557 values less than (', @today - 162, '),',
        'partition p558 values less than (', @today - 161, '),',
        'partition p559 values less than (', @today - 160, '),',
        'partition p560 values less than (', @today - 159, '),',
        'partition p561 values less than (', @today - 158, '),',
        'partition p562 values less than (', @today - 157, '),',
        'partition p563 values less than (', @today - 156, '),',
        'partition p564 values less than (', @today - 155, '),',
        'partition p565 values less than (', @today - 154, '),',
        'partition p566 values less than (', @today - 153, '),',
        'partition p567 values less than (', @today - 152, '),',
        'partition p568 values less than (', @today - 151, '),',
        'partition p569 values less than (', @today - 150, '),',
        'partition p570 values less than (', @today - 149, '),',
        'partition p571 values less than (', @today - 148, '),',
        'partition p572 values less than (', @today - 147, '),',
        'partition p573 values less than (', @today - 146, '),',
        'partition p574 values less than (', @today - 145, '),',
        'partition p575 values less than (', @today - 144, '),',
        'partition p576 values less than (', @today - 143, '),',
        'partition p577 values less than (', @today - 142, '),',
        'partition p578 values less than (', @today - 141, '),',
        'partition p579 values less than (', @today - 140, '),',
        'partition p580 values less than (', @today - 139, '),',
        'partition p581 values less than (', @today - 138, '),',
        'partition p582 values less than (', @today - 137, '),',
        'partition p583 values less than (', @today - 136, '),',
        'partition p584 values less than (', @today - 135, '),',
        'partition p585 values less than (', @today - 134, '),',
        'partition p586 values less than (', @today - 133, '),',
        'partition p587 values less than (', @today - 132, '),',
        'partition p588 values less than (', @today - 131, '),',
        'partition p589 values less than (', @today - 130, '),',
        'partition p590 values less than (', @today - 129, '),',
        'partition p591 values less than (', @today - 128, '),',
        'partition p592 values less than (', @today - 127, '),',
        'partition p593 values less than (', @today - 126, '),',
        'partition p594 values less than (', @today - 125, '),',
        'partition p595 values less than (', @today - 124, '),',
        'partition p596 values less than (', @today - 123, '),',
        'partition p597 values less than (', @today - 122, '),',
        'partition p598 values less than (', @today - 121, '),',
        'partition p599 values less than (', @today - 120, '),',
        'partition p600 values less than (', @today - 119, '),',
        'partition p601 values less than (', @today - 118, '),',
        'partition p602 values less than (', @today - 117, '),',
        'partition p603 values less than (', @today - 116, '),',
        'partition p604 values less than (', @today - 115, '),',
        'partition p605 values less than (', @today - 114, '),',
        'partition p606 values less than (', @today - 113, '),',
        'partition p607 values less than (', @today - 112, '),',
        'partition p608 values less than (', @today - 111, '),',
        'partition p609 values less than (', @today - 110, '),',
        'partition p610 values less than (', @today - 109, '),',
        'partition p611 values less than (', @today - 108, '),',
        'partition p612 values less than (', @today - 107, '),',
        'partition p613 values less than (', @today - 106, '),',
        'partition p614 values less than (', @today - 105, '),',
        'partition p615 values less than (', @today - 104, '),',
        'partition p616 values less than (', @today - 103, '),',
        'partition p617 values less than (', @today - 102, '),',
        'partition p618 values less than (', @today - 101, '),',
        'partition p619 values less than (', @today - 100, '),',
        'partition p620 values less than (', @today - 99, '),',
        'partition p621 values less than (', @today - 98, '),',
        'partition p622 values less than (', @today - 97, '),',
        'partition p623 values less than (', @today - 96, '),',
        'partition p624 values less than (', @today - 95, '),',
        'partition p625 values less than (', @today - 94, '),',
        'partition p626 values less than (', @today - 93, '),',
        'partition p627 values less than (', @today - 92, '),',
        'partition p628 values less than (', @today - 91, '),',
        'partition p629 values less than (', @today - 90, '),',
        'partition p630 values less than (', @today - 89, '),',
        'partition p631 values less than (', @today - 88, '),',
        'partition p632 values less than (', @today - 87, '),',
        'partition p633 values less than (', @today - 86, '),',
        'partition p634 values less than (', @today - 85, '),',
        'partition p635 values less than (', @today - 84, '),',
        'partition p636 values less than (', @today - 83, '),',
        'partition p637 values less than (', @today - 82, '),',
        'partition p638 values less than (', @today - 81, '),',
        'partition p639 values less than (', @today - 80, '),',
        'partition p640 values less than (', @today - 79, '),',
        'partition p641 values less than (', @today - 78, '),',
        'partition p642 values less than (', @today - 77, '),',
        'partition p643 values less than (', @today - 76, '),',
        'partition p644 values less than (', @today - 75, '),',
        'partition p645 values less than (', @today - 74, '),',
        'partition p646 values less than (', @today - 73, '),',
        'partition p647 values less than (', @today - 72, '),',
        'partition p648 values less than (', @today - 71, '),',
        'partition p649 values less than (', @today - 70, '),',
        'partition p650 values less than (', @today - 69, '),',
        'partition p651 values less than (', @today - 68, '),',
        'partition p652 values less than (', @today - 67, '),',
        'partition p653 values less than (', @today - 66, '),',
        'partition p654 values less than (', @today - 65, '),',
        'partition p655 values less than (', @today - 64, '),',
        'partition p656 values less than (', @today - 63, '),',
        'partition p657 values less than (', @today - 62, '),',
        'partition p658 values less than (', @today - 61, '),',
        'partition p659 values less than (', @today - 60, '),',
        'partition p660 values less than (', @today - 59, '),',
        'partition p661 values less than (', @today - 58, '),',
        'partition p662 values less than (', @today - 57, '),',
        'partition p663 values less than (', @today - 56, '),',
        'partition p664 values less than (', @today - 55, '),',
        'partition p665 values less than (', @today - 54, '),',
        'partition p666 values less than (', @today - 53, '),',
        'partition p667 values less than (', @today - 52, '),',
        'partition p668 values less than (', @today - 51, '),',
        'partition p669 values less than (', @today - 50, '),',
        'partition p670 values less than (', @today - 49, '),',
        'partition p671 values less than (', @today - 48, '),',
        'partition p672 values less than (', @today - 47, '),',
        'partition p673 values less than (', @today - 46, '),',
        'partition p674 values less than (', @today - 45, '),',
        'partition p675 values less than (', @today - 44, '),',
        'partition p676 values less than (', @today - 43, '),',
        'partition p677 values less than (', @today - 42, '),',
        'partition p678 values less than (', @today - 41, '),',
        'partition p679 values less than (', @today - 40, '),',
        'partition p680 values less than (', @today - 39, '),',
        'partition p681 values less than (', @today - 38, '),',
        'partition p682 values less than (', @today - 37, '),',
        'partition p683 values less than (', @today - 36, '),',
        'partition p684 values less than (', @today - 35, '),',
        'partition p685 values less than (', @today - 34, '),',
        'partition p686 values less than (', @today - 33, '),',
        'partition p687 values less than (', @today - 32, '),',
        'partition p688 values less than (', @today - 31, '),',
        'partition p689 values less than (', @today - 30, '),',
        'partition p690 values less than (', @today - 29, '),',
        'partition p691 values less than (', @today - 28, '),',
        'partition p692 values less than (', @today - 27, '),',
        'partition p693 values less than (', @today - 26, '),',
        'partition p694 values less than (', @today - 25, '),',
        'partition p695 values less than (', @today - 24, '),',
        'partition p696 values less than (', @today - 23, '),',
        'partition p697 values less than (', @today - 22, '),',
        'partition p698 values less than (', @today - 21, '),',
        'partition p699 values less than (', @today - 20, '),',
        'partition p700 values less than (', @today - 19, '),',
        'partition p701 values less than (', @today - 18, '),',
        'partition p702 values less than (', @today - 17, '),',
        'partition p703 values less than (', @today - 16, '),',
        'partition p704 values less than (', @today - 15, '),',
        'partition p705 values less than (', @today - 14, '),',
        'partition p706 values less than (', @today - 13, '),',
        'partition p707 values less than (', @today - 12, '),',
        'partition p708 values less than (', @today - 11, '),',
        'partition p709 values less than (', @today - 10, '),',
        'partition p710 values less than (', @today - 9, '),',
        'partition p711 values less than (', @today - 8, '),',
        'partition p712 values less than (', @today - 7, '),',
        'partition p713 values less than (', @today - 6, '),',
        'partition p714 values less than (', @today - 5, '),',
        'partition p715 values less than (', @today - 4, '),',
        'partition p716 values less than (', @today - 3, '),',
        'partition p717 values less than (', @today - 2, '),',
        'partition p718 values less than (', @today - 1, '),',
        'partition p719 values less than (', @today, '),',
        'partition p720 values less than (', @today + 1, '),',
        'partition p721 values less than (', @today + 2, '),',
        'partition pMax values less than (to_days("2100-01-01")));');

prepare recreateHistoricalPartitions from @recreateHistoricalPartitionsStmt;
execute recreateHistoricalPartitions;
deallocate prepare recreateHistoricalPartitions;

-- Extracted from partition_historical_values.sql
-- Modified to create but disable the rotate partitions event
-- The event will be enabled in the last step
DROP PROCEDURE IF EXISTS `GET_NUM_PARTITIONS`;

delimiter //
CREATE PROCEDURE GET_NUM_PARTITIONS(
  IN schemaName VARCHAR(50),
  IN tableName VARCHAR(50),
  OUT numPartitions INTEGER)
begin
  SELECT count(*) into numPartitions
  FROM information_schema.PARTITIONS p
  where p.table_schema = schemaName
  and p.table_name = tableName;
end//
delimiter ;

DROP PROCEDURE IF EXISTS `GET_OLDEST_PARTITION_INFO`;

delimiter //
CREATE PROCEDURE GET_OLDEST_PARTITION_INFO(
  IN schemaName VARCHAR(50),
  IN tableName VARCHAR(50),
  OUT timepoint INTEGER,
  OUT partitionName VARCHAR(10))
begin
  SELECT distinct p.partition_description, p.partition_name
  into timepoint, partitionName
  FROM information_schema.PARTITIONS p
  where p.table_schema = schemaName
  and p.table_name = tableName
  and p.partition_ordinal_position =
    (select distinct(min(P_sub.partition_ordinal_position))
      from information_schema.PARTITIONS P_sub where P_sub.table_schema = schemaName
      and P_sub.table_name = tableName
    );
end//
delimiter ;


DROP PROCEDURE IF EXISTS `GET_MAX_RANGE_PARTITION_INFO`;

delimiter //
CREATE PROCEDURE GET_MAX_RANGE_PARTITION_INFO(
  IN schemaName VARCHAR(50),
  IN tableName VARCHAR(50),
  OUT timepoint INTEGER,
  OUT partitionName VARCHAR(10))
begin
  SELECT distinct p.partition_description, p.partition_name
  into timepoint, partitionName
  FROM information_schema.PARTITIONS p
  where p.table_schema = schemaName
  and p.table_name = tableName
  and p.partition_ordinal_position =
    (select distinct(MAX(P_sub.partition_ordinal_position))
      from information_schema.PARTITIONS P_sub where P_sub.table_schema = schemaName
      and P_sub.table_name = tableName
    );
end//
delimiter ;


DROP FUNCTION IF EXISTS `GET_PARTITION_UPPER_RANGE_BORDER`;

delimiter //
CREATE FUNCTION GET_PARTITION_UPPER_RANGE_BORDER(
  schemaName VARCHAR(50),
  tableName VARCHAR(50)
)  RETURNS INTEGER
begin
  DECLARE result INTEGER;

  -- retrieves the upper range border of the newest partition
  -- before the MAX_RANGE partition (ie, next to last partition)
  SELECT distinct p.partition_description
  into result
  FROM information_schema.PARTITIONS p
  where p.table_schema = schemaName
  and p.table_name = tableName
  and p.partition_ordinal_position =
  (select distinct(min(P_sub1.partition_ordinal_position))
    from (select distinct P_sub.partition_ordinal_position from
    information_schema.PARTITIONS P_sub where P_sub.table_schema = schemaName
    and P_sub.table_name = tableName order by partition_ordinal_position desc limit 2) as P_sub1
  );

  return result;
end//
delimiter ;

DROP PROCEDURE IF EXISTS `CREATE_PARTITION`;

delimiter //
CREATE PROCEDURE CREATE_PARTITION(
  IN schemaName VARCHAR(50),
  IN tableName VARCHAR(50),
  IN partitionName VARCHAR(50),
  IN timepoint INT,
  IN maxRangePartitionName VARCHAR(50),
  IN maxRangeTimepoint INT)
begin
  -- dynamic instruction to create new partition by reorginizing the partiotion with max range border into 2
  set @preparedStmtCreate = concat(
    'ALTER TABLE ', schemaName,'.',tableName,' REORGANIZE partition ',
    maxRangePartitionName, ' INTO (',
      ' partition ', partitionName, ' values LESS THAN (',timepoint,'),',
      ' partition ', maxRangePartitionName,' values LESS THAN(',maxRangeTimepoint,'));');

  PREPARE create_new_partition from @preparedStmtCreate;
  EXECUTE create_new_partition;
  DEALLOCATE PREPARE create_new_partition;
end//
delimiter ;

DROP PROCEDURE IF EXISTS `DROP_PARTITION`;

delimiter //
CREATE PROCEDURE DROP_PARTITION(
  IN schemaName VARCHAR(50),
  IN tableName VARCHAR(50),
  IN partitionName VARCHAR(50))
begin
  -- dynamic instruction to drop partition with the specified name (last partiotion is no more necessary)
  set @preparedStmtDrop = concat('ALTER TABLE ',schemaName,'.',tableName,' drop partition ',partitionName);
  PREPARE drop_last_partition from @preparedStmtDrop;
  EXECUTE drop_last_partition;
  DEALLOCATE PREPARE drop_last_partition;
end//
delimiter ;

DROP PROCEDURE IF EXISTS `ROTATE_PARTITIONS`;

delimiter //
CREATE PROCEDURE ROTATE_PARTITIONS(
  IN TABLE_NAME VARCHAR(50),
  IN MAX_ONLINE_PARTITIONS INTEGER)
BEGIN
  -- stores the number of existing partitions
  DECLARE numPartitions INTEGER;

  -- stores the name of the partition with lowest range (oldest data)
  DECLARE oldestPartitionName VARCHAR(10);
  -- the time point - determines the sensor data that has to be archived
  DECLARE oldestPartitionDayThreshold INTEGER;
  -- the time point - upper border limit of the new partition range(in days)
  DECLARE upperRangePartitionDayThreshold INTEGER;

  DECLARE maxRangePartitionName VARCHAR(10);
  DECLARE maxRangePartitionDayThreshold INTEGER;

  DECLARE tomorrowPartitionName VARCHAR(10);
  DECLARE tomorrowPartitionDayThreshold INTEGER;

  -- historical values
  -- retrieves date and partition name of the partition with last ordinal pos
  -- (means the partition with lower range border)
  -- implements cycle buffer of partition names
  call GET_OLDEST_PARTITION_INFO(DATABASE(),TABLE_NAME,
    oldestPartitionDayThreshold,
    oldestPartitionName
  );

  -- gets the total number of existing partitions
  call GET_NUM_PARTITIONS(DATABASE(), TABLE_NAME,
    numPartitions
  );

  -- retrieve the upper range border of the next-to-last partition,
  -- the one just before the MAX_RANGE partition.  It should equal to
  -- to_days(today() + 1 day)
  set upperRangePartitionDayThreshold = GET_PARTITION_UPPER_RANGE_BORDER(
    DATABASE(),TABLE_NAME);

  -- this partition represents tomorrow's data, tomorrow is the day less than
  -- the day after tomorrow
  set tomorrowPartitionDayThreshold = to_days(date_add(now(), interval 2 day));

  IF upperRangePartitionDayThreshold < tomorrowPartitionDayThreshold THEN
    -- Only drop a partition if one will be created.
    -- Otherwise, the partition names can get out of sync with the count.
    -- Only a real problem in artificial situations, or ones created by upgrade
    -- scripts, date changes, etc.  Better safe than sorry.
    IF numPartitions >= MAX_ONLINE_PARTITIONS THEN
      -- remove old partition only if there is 2 years data online
      call DROP_PARTITION(DATABASE(),TABLE_NAME,oldestPartitionName);
      -- re-use the old partition for new data
      set tomorrowPartitionName = oldestPartitionName;
    ELSE
      -- avoid name collision. partition name should be unique assuming low
      -- numbered partitions were not manually removed and maxRange partition
      -- is called 'pMax', not numeric
      set tomorrowPartitionName = concat('p', numPartitions);
    END IF;
    -- retrieves info about partition with max range border
    call GET_MAX_RANGE_PARTITION_INFO(DATABASE(),TABLE_NAME,
      maxRangePartitionDayThreshold,
      maxRangePartitionName);

    -- re-use the "oldest" partition for today's data
    call CREATE_PARTITION(DATABASE(),TABLE_NAME,
      tomorrowPartitionName,
      tomorrowPartitionDayThreshold,
      maxRangePartitionName,
      maxRangePartitionDayThreshold
    );
  END IF;
END//
delimiter ;


DROP EVENT IF EXISTS `ROTATE_HISTORICAL_VALUE_PARTITIONS_EVENT`;

delimiter //
CREATE EVENT `ROTATE_HISTORICAL_VALUE_PARTITIONS_EVENT`
-- start at 1AM today, run daily
ON SCHEDULE EVERY 1 DAY
STARTS TIMESTAMP(DATE(NOW()), '01:00')
DISABLE
DO
BEGIN
  DECLARE TABLE_NAME VARCHAR(20) default 'historical_values';
  -- 720 = 24 months * 30 days + 2 (1 for tomorrow, 1 for pMax)
  DECLARE MAX_ONLINE_PARTITIONS INTEGER default 722;

  CALL ROTATE_PARTITIONS(TABLE_NAME, MAX_ONLINE_PARTITIONS);
END//
delimiter ;

