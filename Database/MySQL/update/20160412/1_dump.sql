-- Bug 24896: Unique index on historical values
-- Bug 25094: Partitions not rotating correctly

-- Remove the old partitioning event.
-- This will be recreated differently in a later step.
drop event if exists `archive_data`;

-- Only save the last 720 days of data
-- Save null group_id as '0' since table is modified to not allow NULL
select value_id_fk, timestamp, valuei, valued, valuel, valuev, ifnull(group_id, 0) from historical_values
where to_days(timestamp) > to_days(now()) - 720
into outfile 'historical_values.csv'
fields terminated by ',';

