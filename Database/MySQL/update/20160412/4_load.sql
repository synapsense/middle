load data infile 'historical_values.csv' replace
into table historical_values
fields terminated by ','
(value_id_fk, timestamp, valuei, valued, valuel, valuev, group_id);

ALTER EVENT `ROTATE_HISTORICAL_VALUE_PARTITIONS_EVENT` ENABLE;

