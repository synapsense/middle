mysql -uroot -pdbuser synap -e "CREATE TABLE innodb_lock_monitor(a int) ENGINE=INNODB;"

mkdir innodb
:loop
set hour=%TIME:~0,2%
set minute=%TIME:~3,2%
set second=%TIME:~6,2%
mysql -uroot -pdbuser -e "show processlist\G" > innodb/innodb%hour%_%minute%_%second%.txt
mysql -uroot -pdbuser -e "show engine innodb status\G" >> innodb/innodb%hour%_%minute%_%second%.txt
call :sleep 10
goto loop

:sleep
ping -n %1 127.0.0.1 > NUL 2>&1
