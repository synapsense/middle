Print innodb status logs to files
Each 2 seconds generates new file with related name

Strongly recommended to run it before reproducing ES bugs 
and to attach new log files to bugzilla marked as something like "innodb logs"

