SET FOREIGN_KEY_CHECKS=0;

ALTER TABLE historical_values DISABLE KEYS;
ALTER TABLE tbl_sensor_data DISABLE KEYS;

  load data infile 'c:/share/saveload/tbl_sensor_data.csv' into table tbl_sensor_data;
  load data infile 'c:/share/saveload/historical_values.csv' into table historical_values;

ALTER TABLE tbl_sensor_data ENABLE KEYS;
ALTER TABLE historical_values ENABLE KEYS;


SET FOREIGN_KEY_CHECKS=1;

