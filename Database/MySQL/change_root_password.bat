@echo off

rem Enable Command Extensions
setlocal enableextensions

rem Read the password as the first argument
set PASSWORD=%1
if not defined PASSWORD (
    echo Missing password argument
    goto error
)

rem Detect MYSQL_HOME relative to the script
set "MYSQL_HOME=%~dp0.."
set "MYSQLADMIN=%MYSQL_HOME%\bin\mysqladmin.exe"

echo Press Ctrl-C to interrupt and cancel the installation.
echo Waiting for the DB to become available ...
:status_loop
"%MYSQLADMIN%" --user root status >nul 2>&1
if ERRORLEVEL 1 (
    echo The DB is not yet available. Retrying after 5 seconds ...
    rem Simulate a 5 sec sleep with a ping (the only portable way to sleep in Windows)
    ping -n 5 localhost >nul 2>&1
    goto status_loop
)

rem Finally, change the root passowrd
echo Changing the root password ...
"%MYSQLADMIN%" --user root password %PASSWORD%
if ERRORLEVEL 1 (
    echo Failed to change the root password. The installation will fail.
    goto error
)

rem We're done
goto:eof

:error
rem Pause so the user can see the failure
pause
exit /b 1
