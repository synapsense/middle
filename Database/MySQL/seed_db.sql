-- init db schema by erasing all TABLEs and put into it init data
USE `synap`;


-- =============================================================================
-- Create dbuser db user and grant appropriate permissions
-- =============================================================================
GRANT select, insert, update, delete on synap.* to 'dbuser'@'%' IDENTIFIED BY 'dbu$er';
GRANT FILE on *.* to 'dbuser'@'%' IDENTIFIED BY 'dbu$er';

SET FOREIGN_KEY_CHECKS=0;

-- =============================================================================
-- create auxiliary functions & procedures
DROP FUNCTION IF EXISTS insert_object_value;
DELIMITER |
CREATE FUNCTION insert_object_value(
  in_object_id int, in_property_id int, in_timestamp long)
  RETURNS INTEGER
  BEGIN
    set @id=0;
    SELECT value_id into @id
        from obj_values
        where object_id_fk=in_object_id and attr_id_fk=in_property_id limit 1;

    IF @id!=0
    THEN
        RETURN @id;
    END IF;

    INSERT INTO obj_values(
                  object_id_fk, attr_id_fk, TIMESTAMP)
    VALUES (in_object_id, in_property_id, in_timestamp);

    RETURN last_insert_id();
  END|
DELIMITER ;

DROP PROCEDURE IF EXISTS insert_binarydata_value;

DELIMITER |
CREATE PROCEDURE insert_binarydata_value(
  in_object_id int, in_property_id int, in_timestamp long, value_binary mediumblob)
  BEGIN
    DECLARE valueId   INT;

    SET valueId =
          insert_object_value(
            in_object_id, in_property_id, in_timestamp);

    INSERT INTO value_binary(
                  value_id_fk, VALUE)
    VALUES (valueId, value_binary);
  END|

DELIMITER ;

DROP PROCEDURE IF EXISTS insert_varchar_value;

DELIMITER |
CREATE PROCEDURE insert_varchar_value(
  in_object_id int, in_property_id int, in_timestamp long, valuev varchar(20000))
  BEGIN
    DECLARE valueId   INT;

    SET valueId =
          insert_object_value(
            in_object_id, in_property_id, in_timestamp);

    INSERT INTO valuev(
                  value_id_fk, VALUE)
    VALUES (valueId, valuev);
  END|

DELIMITER ;

DROP PROCEDURE IF EXISTS insert_double_value;

DELIMITER |

CREATE PROCEDURE insert_double_value(
  in_object_id int, in_property_id int, in_timestamp long, valued double)
  BEGIN
    DECLARE valueId   int;

    SET valueId =
          insert_object_value(
            in_object_id, in_property_id, in_timestamp);

    INSERT INTO valued(
                  value_id_fk, value)
    VALUES (valueId, valued);

  END|

DELIMITER ;

DROP PROCEDURE IF EXISTS insert_int_value;

DELIMITER |
CREATE PROCEDURE insert_int_value(
  in_object_id int, in_property_id int, in_timestamp long, valuei int)
  BEGIN
    DECLARE valueId   int;

    SET valueId =
          insert_object_value(
            in_object_id, in_property_id, in_timestamp);

    INSERT INTO valuei(
                  value_id_fk, value)
    VALUES (valueId, valuei);
  END|
DELIMITER ;

DROP PROCEDURE IF EXISTS insert_long_value;

DELIMITER |
CREATE PROCEDURE insert_long_value(
  in_object_id int, in_property_id int, in_timestamp long, valuel long)
  BEGIN
    DECLARE valueId   int;

    SET valueId =
          insert_object_value(
            in_object_id, in_property_id, in_timestamp);

    INSERT INTO valuel(
                  value_id_fk, value)
    VALUES (valueId, valuel);
  END|
DELIMITER ;

DROP PROCEDURE IF EXISTS insert_link_value;

DELIMITER |
CREATE PROCEDURE insert_link_value(
  in_object_id int, in_property_id int, in_timestamp long, value_link int)
  BEGIN
    DECLARE valueId   int;
    DECLARE typeName  varchar(300);

    SET valueId =
          insert_object_value(
            in_object_id, in_property_id, in_timestamp);

    CALL add_link_value(valueId, in_timestamp, value_link);
  END|
DELIMITER ;

DROP PROCEDURE IF EXISTS add_link_value;

DELIMITER |
CREATE PROCEDURE add_link_value(
  valueId int, in_timestamp long, value_link int)
  BEGIN
    DECLARE typeName  varchar(300);

    IF (value_link IS NULL) THEN
      INSERT INTO value_obj_link(
                  value_id_fk, object_id,object_type)
      VALUES (valueId, NULL, NULL);
    ELSE
      SELECT ot.NAME INTO typeName FROM objects o , object_types ot WHERE o.object_type_id_fk = ot.objtype_id
      AND o.id = value_link;

      INSERT INTO value_obj_link(value_id_fk, object_id, object_type)
      VALUES (valueId, value_link, typeName);

    END IF;

  END|
DELIMITER ;

-- =============================================================================
-- erase wsn tables
TRUNCATE TABLE `tbl_nht_info`;
TRUNCATE TABLE `tbl_network_statbase`;
TRUNCATE TABLE `tbl_network_statbase`;

-- =============================================================================
-- erase tables related  to object model
TRUNCATE TABLE `valuei`;
TRUNCATE TABLE `valuel`;
TRUNCATE TABLE `valued`;
TRUNCATE TABLE `valuedt`;
TRUNCATE TABLE `valuev`;
TRUNCATE TABLE `value_binary`;
TRUNCATE TABLE `value_obj_link`;
TRUNCATE TABLE `value_property_link`;
TRUNCATE TABLE `obj_values`;
TRUNCATE TABLE `historical_values`;
TRUNCATE TABLE `attributes`;
TRUNCATE TABLE `attributes_types`;
TRUNCATE TABLE `object_parents`;
TRUNCATE TABLE `objects`;
TRUNCATE TABLE `object_types`;
TRUNCATE TABLE `tags`;
TRUNCATE TABLE `tag_values`;
TRUNCATE TABLE `uid_table`;
-- =============================================================================
-- erase tables related  to RE
TRUNCATE TABLE `tbl_rule_instances`;
TRUNCATE TABLE `tbl_rule_types`;
TRUNCATE TABLE `tbl_rule_property_src_assoc`;

-- =============================================================================
-- erase activity log
TRUNCATE TABLE `tbl_activity_log`;

SET FOREIGN_KEY_CHECKS=1;

-- =============================================================================
-- Initialize predefined GENERIC object model

-- supported property types
INSERT INTO `attributes_types` (`attr_type_id`,`java_type`) VALUES (1,"java.lang.Integer");
INSERT INTO `attributes_types` (`attr_type_id`,`java_type`) VALUES (2,"java.lang.Double");
INSERT INTO `attributes_types` (`attr_type_id`,`java_type`) VALUES (3,"java.lang.String");
INSERT INTO `attributes_types` (`attr_type_id`,`java_type`) VALUES (4,"java.util.Date");
INSERT INTO `attributes_types` (`attr_type_id`,`java_type`) VALUES (5,"com.synapsense.dto.TO");
INSERT INTO `attributes_types` (`attr_type_id`,`java_type`) VALUES (6,"com.synapsense.service.impl.dao.to.PropertyTO");
INSERT INTO `attributes_types` (`attr_type_id`,`java_type`) VALUES (7,"java.lang.Long");
INSERT INTO `attributes_types` (`attr_type_id`,`java_type`) VALUES (8,"com.synapsense.dto.BinaryData");

set @currentTime = UNIX_TIMESTAMP() * 1000;

-- predefined type CUSTOMQUERY to avoid EJBAccessException cause createObjectType is accessible only by users with 'admin' role
INSERT INTO  `object_types` (`name`) VALUES ('CUSTOMQUERY');
set @customQueryTypeId = last_insert_id();
INSERT INTO  `objects` (`object_type_id_fk`,`name`,`static`) VALUES (@customQueryTypeId,'CUSTOMQUERY',1);
INSERT INTO  `attributes` (`object_type_id_fk`,`name`,`attr_type_id_fk`) VALUES (@customQueryTypeId,'name',3);
INSERT INTO  `attributes` (`object_type_id_fk`,`name`,`attr_type_id_fk`) VALUES (@customQueryTypeId,'associatedSensors',3);
INSERT INTO  `attributes` (`object_type_id_fk`,`name`,`attr_type_id_fk`) VALUES (@customQueryTypeId,'privateCheck',1);
INSERT INTO  `attributes` (`object_type_id_fk`,`name`,`attr_type_id_fk`) VALUES (@customQueryTypeId,'queryDesc',3);
INSERT INTO  `attributes` (`object_type_id_fk`,`name`,`attr_type_id_fk`) VALUES (@customQueryTypeId,'userId',1);
INSERT INTO  `attributes` (`object_type_id_fk`,`name`,`attr_type_id_fk`) VALUES (@customQueryTypeId,'lines',3);
INSERT INTO  `attributes` (`object_type_id_fk`,`name`,`attr_type_id_fk`) VALUES (@customQueryTypeId,'chartDescr',3);

-- predefined type CONFIGURATION_DATA to avoid EJBAccessException cause createObjectType is accessible only by users with 'admin' role
INSERT INTO  `object_types` (`name`) VALUES ('CONFIGURATION_DATA');
set @confDataTypeId = last_insert_id();
INSERT INTO  `objects` (`object_type_id_fk`,`name`,`static`) VALUES (@confDataTypeId,'CONFIGURATION_DATA',1);
INSERT INTO  `attributes` (`object_type_id_fk`,`name`,`attr_type_id_fk`) VALUES (@confDataTypeId,'name',3);
INSERT INTO  `attributes` (`object_type_id_fk`,`name`,`attr_type_id_fk`) VALUES (@confDataTypeId,'config',3);

-- Create ROOT type and one object with the version string
INSERT INTO `object_types` (`name`) VALUES("ROOT");
set @rootTypeId = last_insert_id();
INSERT INTO  `objects` (`object_type_id_fk`,`name`,`static`) VALUES (@rootTypeId,'Root',1);
INSERT INTO `attributes` (`name`, `object_type_id_fk`, `attr_type_id_fk`) VALUES ("name",@rootTypeId,3);
set @rootNamePropertyId = last_insert_id();
INSERT INTO `attributes` (`name`, `object_type_id_fk`, `attr_type_id_fk`) VALUES ("version",@rootTypeId,3);
set @rootVersionPropertyId = last_insert_id();
INSERT INTO `attributes` (`name`, `object_type_id_fk`, `attr_type_id_fk`) VALUES ("modelVersion",@rootTypeId,3);
-- Create root object
INSERT INTO `objects` (`object_type_id_fk`,`name`,`static`) VALUES (@rootTypeId,null,0);
set @rootObjectId = last_insert_id();
CALL insert_varchar_value(@rootObjectId, @rootNamePropertyId, @currentTime, 'Root');
CALL insert_varchar_value(@rootObjectId, @rootVersionPropertyId, @currentTime, '7.0');

-- Need to prepare message templates infrastructure before initiating alert types
-- seeding default templates for all type of messages
INSERT INTO `object_types` (`name`) VALUES("ALERT_MESSAGE_TEMPLATE");
set @messageTemplateTypeId = last_insert_id();
INSERT INTO `objects` (`object_type_id_fk`,`name`,`static`) VALUES (@messageTemplateTypeId,"ALERT_MESSAGE_TEMPLATE",1);

INSERT INTO `attributes` (`name`, `object_type_id_fk`, `attr_type_id_fk`) VALUES ("name",@messageTemplateTypeId,3);
set @templateNamePropertyId = last_insert_id();
INSERT INTO `attributes` (`name`, `object_type_id_fk`, `attr_type_id_fk`) VALUES ("header",@messageTemplateTypeId,3);
set @templateHeaderPropertyId = last_insert_id();
INSERT INTO `attributes` (`name`, `object_type_id_fk`, `attr_type_id_fk`) VALUES ("body",@messageTemplateTypeId,3);
set @templateBodyPropertyId = last_insert_id();
INSERT INTO `attributes` (`name`, `object_type_id_fk`, `attr_type_id_fk`) VALUES ("description",@messageTemplateTypeId,3);
set @templateDescrPropertyId = last_insert_id();
INSERT INTO `attributes` (`name`, `object_type_id_fk`, `attr_type_id_fk`) VALUES ("messagetype",@messageTemplateTypeId,3);
set @templateMessageTypePropertyId = last_insert_id();
INSERT INTO `attributes` (`name`, `object_type_id_fk`, `attr_type_id_fk`) VALUES ("type",@messageTemplateTypeId,1);
set @templateTypePropertyId = last_insert_id();

-- Short SMS code=2
INSERT INTO `objects` (`object_type_id_fk`,`name`,`static`) VALUES (@messageTemplateTypeId,null,0);
set @templateId = last_insert_id();
CALL insert_varchar_value(@templateId, @templateNamePropertyId, @currentTime, 'Short SMS');
CALL insert_varchar_value(@templateId, @templateHeaderPropertyId, @currentTime, '');
CALL insert_varchar_value(@templateId, @templateBodyPropertyId, @currentTime, '$parent(DC).name$ - $name$ - $ALERT_NAME$\n\nCurrent value: $CURRENT_DATA_VALUE$\n\nTime Triggered: $TIMESTAMP$');
CALL insert_varchar_value(@templateId, @templateDescrPropertyId, @currentTime, 'A short SMS alert notification');
CALL insert_varchar_value(@templateId, @templateMessageTypePropertyId, @currentTime, '2');
CALL insert_int_value(@templateId, @templateTypePropertyId, @currentTime, 1);

-- Short email code = 1
INSERT INTO `objects` (`object_type_id_fk`,`name`,`static`) VALUES (@messageTemplateTypeId,null,0);
set @templateId = last_insert_id();
CALL insert_varchar_value(@templateId, @templateNamePropertyId, @currentTime, 'Short Email');
CALL insert_varchar_value(@templateId, @templateHeaderPropertyId, @currentTime, 'Tier $MSG_TIER$ System  Alert - $parent(DC).name$ \\ $name$  \\ $ALERT_NAME$ @ $TRIGGER_DATA_VALUE$');
CALL insert_varchar_value(@templateId, @templateBodyPropertyId, @currentTime, 'Tier $MSG_TIER$ System  Alert - $parent(DC).name$ \\ $parent(ROOM).name$ \\ $name$ \n\nAlert: $ALERT_NAME$\nTime: $TIMESTAMP$\nValue that triggered alert: $TRIGGER_DATA_VALUE$\nCurrent value: $CURRENT_DATA_VALUE$');
CALL insert_varchar_value(@templateId, @templateDescrPropertyId, @currentTime, 'A short email alert notification');
CALL insert_varchar_value(@templateId, @templateMessageTypePropertyId, @currentTime, '1');
CALL insert_int_value(@templateId, @templateTypePropertyId, @currentTime, 1);

-- Trap code = 3
INSERT INTO `objects` (`object_type_id_fk`,`name`,`static`) VALUES (@messageTemplateTypeId,null,0);
set @templateId = last_insert_id();
CALL insert_varchar_value(@templateId, @templateNamePropertyId, @currentTime, 'SNMP trap');
CALL insert_varchar_value(@templateId, @templateHeaderPropertyId, @currentTime, '');
CALL insert_varchar_value(@templateId, @templateBodyPropertyId, @currentTime, 'Tier $MSG_TIER$ System  Alert - $parent(DC).name$ \\ $name$ \\ $ALERT_NAME$ @ $TRIGGER_DATA_VALUE$\n\nCurrent value: $CURRENT_DATA_VALUE$');
CALL insert_varchar_value(@templateId, @templateDescrPropertyId, @currentTime, 'A short SNMP Trap notification');
CALL insert_varchar_value(@templateId, @templateMessageTypePropertyId, @currentTime, '3');
CALL insert_int_value(@templateId, @templateTypePropertyId, @currentTime, 1);

-- Long SMS code=2
INSERT INTO `objects` (`object_type_id_fk`,`name`,`static`) VALUES (@messageTemplateTypeId,null,0);
set @templateId = last_insert_id();
CALL insert_varchar_value(@templateId, @templateNamePropertyId, @currentTime, 'Long SMS');
CALL insert_varchar_value(@templateId, @templateHeaderPropertyId, @currentTime, '');
CALL insert_varchar_value(@templateId, @templateBodyPropertyId, @currentTime, 'Tier $MSG_TIER$ System  Alert - $parent(DC).name$ \\ $name$ \n\nAlert: $ALERT_NAME$\nValue that triggered alert: $TRIGGER_DATA_VALUE$\nCurrent value: $CURRENT_DATA_VALUE$');
CALL insert_varchar_value(@templateId, @templateDescrPropertyId, @currentTime, 'A long SMS alert notification');
CALL insert_varchar_value(@templateId, @templateMessageTypePropertyId, @currentTime, '2');
CALL insert_int_value(@templateId, @templateTypePropertyId, @currentTime, 1);

-- Console view default, code = 4
INSERT INTO `objects` (`object_type_id_fk`,`name`,`static`) VALUES (@messageTemplateTypeId,null,0);
set @templateId = last_insert_id();
CALL insert_varchar_value(@templateId, @templateNamePropertyId, @currentTime, 'Console Notification');
CALL insert_varchar_value(@templateId, @templateHeaderPropertyId, @currentTime, '');
CALL insert_varchar_value(@templateId, @templateBodyPropertyId, @currentTime, 'Value that triggered alert: $TRIGGER_DATA_VALUE$\nCurrent value: $CURRENT_DATA_VALUE$');
CALL insert_varchar_value(@templateId, @templateDescrPropertyId, @currentTime, 'A notification used in Console display of alert details');
CALL insert_varchar_value(@templateId, @templateMessageTypePropertyId, @currentTime, '4');
CALL insert_int_value(@templateId, @templateTypePropertyId, @currentTime, 0);

-- Console view default for system alerts, code = 4
INSERT INTO `objects` (`object_type_id_fk`,`name`,`static`) VALUES (@messageTemplateTypeId,null,0);
set @csnTemplateId = last_insert_id();
CALL insert_varchar_value(@csnTemplateId, @templateNamePropertyId, @currentTime, 'Console System Notification');
CALL insert_varchar_value(@csnTemplateId, @templateHeaderPropertyId, @currentTime, '');
CALL insert_varchar_value(@csnTemplateId, @templateBodyPropertyId, @currentTime, '$message$');
CALL insert_varchar_value(@csnTemplateId, @templateDescrPropertyId, @currentTime, 'A notification used in Console display of system alert details');
CALL insert_varchar_value(@csnTemplateId, @templateMessageTypePropertyId, @currentTime, '4');
CALL insert_int_value(@csnTemplateId, @templateTypePropertyId, @currentTime, 1);

-- NT log view default for system alerts, code = 6
INSERT INTO `objects` (`object_type_id_fk`,`name`,`static`) VALUES (@messageTemplateTypeId,null,0);
set @welsnTemplateId = last_insert_id();
CALL insert_varchar_value(@welsnTemplateId, @templateNamePropertyId, @currentTime, 'Windows Event Log System Notification');
CALL insert_varchar_value(@welsnTemplateId, @templateHeaderPropertyId, @currentTime, '');
CALL insert_varchar_value(@welsnTemplateId, @templateBodyPropertyId, @currentTime, 'Alert Name: $ALERT_NAME$\nAlert Message: $message$');
CALL insert_varchar_value(@welsnTemplateId, @templateDescrPropertyId, @currentTime, 'A notification used in Windows Event Log display of system alert details');
CALL insert_varchar_value(@welsnTemplateId, @templateMessageTypePropertyId, @currentTime, '6');
CALL insert_int_value(@welsnTemplateId, @templateTypePropertyId, @currentTime, 1);

-- NT log default, code = 6
INSERT INTO `objects` (`object_type_id_fk`,`name`,`static`) VALUES (@messageTemplateTypeId,null,0);
set @templateId = last_insert_id();
CALL insert_varchar_value(@templateId, @templateNamePropertyId, @currentTime, 'Windows Event Log Notification');
CALL insert_varchar_value(@templateId, @templateHeaderPropertyId, @currentTime, '');
CALL insert_varchar_value(@templateId, @templateBodyPropertyId, @currentTime, 'Alert Name: $ALERT_NAME$\nValue that triggered alert: $TRIGGER_DATA_VALUE$\nCurrent value: $CURRENT_DATA_VALUE$');
CALL insert_varchar_value(@templateId, @templateDescrPropertyId, @currentTime, 'A notification used in Windows Event Log display of alert details');
CALL insert_varchar_value(@templateId, @templateMessageTypePropertyId, @currentTime, '6');
CALL insert_int_value(@templateId, @templateTypePropertyId, @currentTime, 0);

-- SMTP with Live Image attachment
INSERT INTO `objects` (`object_type_id_fk`,`name`,`static`) VALUES (@messageTemplateTypeId,null,0);
set @templateId = last_insert_id();
CALL insert_varchar_value(@templateId, @templateNamePropertyId, @currentTime, 'Short Email with LiveImaging');
CALL insert_varchar_value(@templateId, @templateHeaderPropertyId, @currentTime, 'Tier $MSG_TIER$ System  Alert - $parent(DC).name$ \\ $name$  \\ $ALERT_NAME$ @ $TRIGGER_DATA_VALUE$');
CALL insert_varchar_value(@templateId, @templateBodyPropertyId, @currentTime, 'Tier $MSG_TIER$ System  Alert - $parent(DC).name$ \\ $parent(ROOM).name$ \\ $name$ \n\nAlert: $ALERT_NAME$\nTime: $TIMESTAMP$\nValue that triggered alert: $TRIGGER_DATA_VALUE$\nCurrent value: $CURRENT_DATA_VALUE$ $DC_LIVE_IMAGE$');
CALL insert_varchar_value(@templateId, @templateDescrPropertyId, @currentTime, 'A short email alert notification with LiveImaging image included');
CALL insert_varchar_value(@templateId, @templateMessageTypePropertyId, @currentTime, '1');
CALL insert_int_value(@templateId, @templateTypePropertyId, @currentTime, 1);

-- create type 'ALERT_PRIORITY_TIER'
    INSERT INTO object_types(name)
        VALUES ('ALERT_PRIORITY_TIER');

    SET @lastTypeId := last_insert_id();

    INSERT INTO objects(
                  name, object_type_id_fk, static)
        VALUES ('ALERT_PRIORITY_TIER', @lastTypeId, 1);

    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('name', @lastTypeId, 3);

    set @namePropertyId = last_insert_id();

    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('interval', @lastTypeId, 7);

    set @intervalPropertyId = last_insert_id();

    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('notificationAttempts', @lastTypeId, 1);

    set @notifAttemptsPropertyId = last_insert_id();

    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('sendEscalation', @lastTypeId, 1);

    set @sendEscPropertyId = last_insert_id();

    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('sendAck', @lastTypeId, 1);

    set @sendAckPropertyId = last_insert_id();

    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('sendResolve', @lastTypeId, 1);

    set @sendResolvedPropertyId = last_insert_id();

-- create objects 'ALERT_PRIORITY_TIER'
    INSERT INTO objects(object_type_id_fk)
        VALUES (@lastTypeId);

        SET @TierId1 = last_insert_id();

        CALL insert_varchar_value(
             @TierId1, @namePropertyId, @currentTime, 'TIER1');
        CALL insert_long_value(
             @TierId1, @intervalPropertyId, @currentTime, 10*60*1000);
        CALL insert_int_value(
             @TierId1, @notifAttemptsPropertyId, @currentTime, 2);
        CALL insert_int_value(
             @TierId1, @sendEscPropertyId, @currentTime, 0);
        CALL insert_int_value(
             @TierId1, @sendAckPropertyId, @currentTime, 0);
        CALL insert_int_value(
             @TierId1, @sendResolvedPropertyId, @currentTime, 0);

    INSERT INTO objects(object_type_id_fk)
        VALUES (@lastTypeId);

        SET @TierId2 = last_insert_id();

        CALL insert_varchar_value(
             @TierId2, @namePropertyId, @currentTime, 'TIER2');
        CALL insert_long_value(
             @TierId2, @intervalPropertyId, @currentTime, 10*60*1000);
        CALL insert_int_value(
             @TierId2, @notifAttemptsPropertyId, @currentTime, 2);
        CALL insert_int_value(
             @TierId2, @sendEscPropertyId, @currentTime, 0);
        CALL insert_int_value(
             @TierId2, @sendAckPropertyId, @currentTime, 0);
        CALL insert_int_value(
             @TierId2, @sendResolvedPropertyId, @currentTime, 0);

    INSERT INTO objects(object_type_id_fk)
        VALUES (@lastTypeId);

        SET @TierId3 = last_insert_id();

        CALL insert_varchar_value(
             @TierId3, @namePropertyId, @currentTime, 'TIER3');
        CALL insert_long_value(
             @TierId3, @intervalPropertyId, @currentTime, 10*60*1000);
        CALL insert_int_value(
             @TierId3, @notifAttemptsPropertyId, @currentTime, 2);
        CALL insert_int_value(
             @TierId3, @sendEscPropertyId, @currentTime, 0);
        CALL insert_int_value(
             @TierId3, @sendAckPropertyId, @currentTime, 0);
        CALL insert_int_value(
             @TierId3, @sendResolvedPropertyId, @currentTime, 0);

   INSERT INTO objects(object_type_id_fk)
        VALUES (@lastTypeId);

        SET @TierId4 = last_insert_id();

        CALL insert_varchar_value(
             @TierId4, @namePropertyId, @currentTime, 'TIER1');
        CALL insert_long_value(
             @TierId4, @intervalPropertyId, @currentTime, 15*60*1000);
        CALL insert_int_value(
             @TierId4, @notifAttemptsPropertyId, @currentTime, 3);
        CALL insert_int_value(
             @TierId4, @sendEscPropertyId, @currentTime, 0);
        CALL insert_int_value(
             @TierId4, @sendAckPropertyId, @currentTime, 0);
        CALL insert_int_value(
             @TierId4, @sendResolvedPropertyId, @currentTime, 0);

    INSERT INTO objects(object_type_id_fk)
        VALUES (@lastTypeId);

        SET @TierId5 = last_insert_id();

        CALL insert_varchar_value(
             @TierId5, @namePropertyId, @currentTime, 'TIER2');
        CALL insert_long_value(
             @TierId5, @intervalPropertyId, @currentTime, 15*60*1000);
        CALL insert_int_value(
             @TierId5, @notifAttemptsPropertyId, @currentTime, 3);
        CALL insert_int_value(
             @TierId5, @sendEscPropertyId, @currentTime, 0);
        CALL insert_int_value(
             @TierId5, @sendAckPropertyId, @currentTime, 0);
        CALL insert_int_value(
             @TierId5, @sendResolvedPropertyId, @currentTime, 0);

    INSERT INTO objects(object_type_id_fk)
        VALUES (@lastTypeId);

        SET @TierId6 = last_insert_id();

        CALL insert_varchar_value(
             @TierId6, @namePropertyId, @currentTime, 'TIER3');
        CALL insert_long_value(
             @TierId6, @intervalPropertyId, @currentTime, 15*60*1000);
        CALL insert_int_value(
             @TierId6, @notifAttemptsPropertyId, @currentTime, 3);
        CALL insert_int_value(
             @TierId6, @sendEscPropertyId, @currentTime, 0);
        CALL insert_int_value(
             @TierId6, @sendAckPropertyId, @currentTime, 0);
        CALL insert_int_value(
             @TierId6, @sendResolvedPropertyId, @currentTime, 0);

    INSERT INTO objects(object_type_id_fk)
        VALUES (@lastTypeId);

        SET @TierId7 = last_insert_id();

        CALL insert_varchar_value(
             @TierId7, @namePropertyId, @currentTime, 'TIER1');
        CALL insert_long_value(
             @TierId7, @intervalPropertyId, @currentTime, 30*60*1000);
        CALL insert_int_value(
             @TierId7, @notifAttemptsPropertyId, @currentTime, 3);
        CALL insert_int_value(
             @TierId7, @sendEscPropertyId, @currentTime, 0);
        CALL insert_int_value(
             @TierId7, @sendAckPropertyId, @currentTime, 0);
        CALL insert_int_value(
             @TierId7, @sendResolvedPropertyId, @currentTime, 0);

    INSERT INTO objects(object_type_id_fk)
        VALUES (@lastTypeId);

        SET @TierId8 = last_insert_id();

        CALL insert_varchar_value(
             @TierId8, @namePropertyId, @currentTime, 'TIER2');
        CALL insert_long_value(
             @TierId8, @intervalPropertyId, @currentTime, 30*60*1000);
        CALL insert_int_value(
             @TierId8, @notifAttemptsPropertyId, @currentTime, 3);
        CALL insert_int_value(
             @TierId8, @sendEscPropertyId, @currentTime, 0);
        CALL insert_int_value(
             @TierId8, @sendAckPropertyId, @currentTime, 0);
        CALL insert_int_value(
             @TierId8, @sendResolvedPropertyId, @currentTime, 0);

    INSERT INTO objects(object_type_id_fk)
        VALUES (@lastTypeId);

        SET @TierId9 = last_insert_id();

        CALL insert_varchar_value(
             @TierId9, @namePropertyId, @currentTime, 'TIER3');
        CALL insert_long_value(
             @TierId9, @intervalPropertyId, @currentTime, 30*60*1000);
        CALL insert_int_value(
             @TierId9, @notifAttemptsPropertyId, @currentTime, 3);
        CALL insert_int_value(
             @TierId9, @sendEscPropertyId, @currentTime, 0);
        CALL insert_int_value(
             @TierId9, @sendAckPropertyId, @currentTime, 0);
        CALL insert_int_value(
             @TierId9, @sendResolvedPropertyId, @currentTime, 0);

    INSERT INTO objects(object_type_id_fk)
        VALUES (@lastTypeId);

        SET @TierId10 = last_insert_id();

        CALL insert_varchar_value(
             @TierId10, @namePropertyId, @currentTime, 'TIER1');
        CALL insert_long_value(
             @TierId10, @intervalPropertyId, @currentTime, 60*60*1000);
        CALL insert_int_value(
             @TierId10, @notifAttemptsPropertyId, @currentTime, 0);
        CALL insert_int_value(
             @TierId10, @sendEscPropertyId, @currentTime, 0);
        CALL insert_int_value(
             @TierId10, @sendAckPropertyId, @currentTime, 0);
        CALL insert_int_value(
             @TierId10, @sendResolvedPropertyId, @currentTime, 0);

-- create type 'ALERT_PRIORITY'
    INSERT INTO object_types(name)
        VALUES ('ALERT_PRIORITY');

    SET @lastTypeId := last_insert_id();

    INSERT INTO objects(
                  name, object_type_id_fk, static)
        VALUES ('ALERT_PRIORITY', @lastTypeId, 1);

    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('name', @lastTypeId, 3);

    set @namePropertyId = last_insert_id();

    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('dismissInterval', @lastTypeId, 7);

    set @dismissIntervalPropertyId = last_insert_id();

    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk,historical,collection)
    values ('tiers', @lastTypeId, 5,0,1);

    set @tiersPropertyId = last_insert_id();

    -- add new attributes for audio_visual notification

    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('redSignal', @lastTypeId, 1);

	 set @redSignalId = last_insert_id();

    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('greenSignal', @lastTypeId, 1);

	 set @greenSignalId = last_insert_id();

    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('yellowSignal', @lastTypeId, 1);

	 set @yellowSignalId = last_insert_id();

    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('audioSignal', @lastTypeId, 1);

	 set @audioSignalId = last_insert_id();

    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('lightDuration', @lastTypeId, 7);

	 set @lightDurationId = last_insert_id();

    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('audioDuration', @lastTypeId, 7);

	 set @audioDurationId = last_insert_id();

	 insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('ntLog', @lastTypeId, 1);

	 set @ntLog = last_insert_id();

    set @currentTime = UNIX_TIMESTAMP() * 1000;

-- create objects 'ALERT_PRIORITY'
    INSERT INTO objects(object_type_id_fk)
        VALUES (@lastTypeId);

        SET @alertPriorityId1 = last_insert_id();

        CALL insert_varchar_value(
             @alertPriorityId1, @namePropertyId, @currentTime, 'CRITICAL');
        CALL insert_long_value(
             @alertPriorityId1, @dismissIntervalPropertyId, @currentTime, 600000);

        call insert_link_value(@alertPriorityId1,@tiersPropertyId,@currentTime,@TierId1);
        call insert_link_value(@alertPriorityId1,@tiersPropertyId,@currentTime,@TierId2);
        call insert_link_value(@alertPriorityId1,@tiersPropertyId,@currentTime,@TierId3);

		call insert_int_value(@alertPriorityId1, @redSignalId, @currentTime, 0);
		call insert_int_value(@alertPriorityId1, @greenSignalId, @currentTime, 0);
		call insert_int_value(@alertPriorityId1, @yellowSignalId, @currentTime, 0);
		call insert_int_value(@alertPriorityId1, @audioSignalId, @currentTime, 0);
		call insert_long_value(@alertPriorityId1, @lightDurationId, @currentTime, 0);
		call insert_long_value(@alertPriorityId1, @audioDurationId, @currentTime, 0);
		call insert_int_value(@alertPriorityId1, @ntLog, @currentTime, 0);

    INSERT INTO objects(object_type_id_fk)
        VALUES (@lastTypeId);

        SET @alertPriorityId2 = last_insert_id();

        CALL insert_varchar_value(
             @alertPriorityId2, @namePropertyId, @currentTime, 'MAJOR');
        CALL insert_long_value(
             @alertPriorityId2, @dismissIntervalPropertyId, @currentTime, 1200000);

        call insert_link_value(@alertPriorityId2,@tiersPropertyId,@currentTime,@TierId4);
        call insert_link_value(@alertPriorityId2,@tiersPropertyId,@currentTime,@TierId5);
        call insert_link_value(@alertPriorityId2,@tiersPropertyId,@currentTime,@TierId6);

		call insert_int_value(@alertPriorityId2, @redSignalId, @currentTime, 0);
		call insert_int_value(@alertPriorityId2, @greenSignalId, @currentTime, 0);
		call insert_int_value(@alertPriorityId2, @yellowSignalId, @currentTime, 0);
		call insert_int_value(@alertPriorityId2, @audioSignalId, @currentTime, 0);
		call insert_long_value(@alertPriorityId2, @lightDurationId, @currentTime, 0);
		call insert_long_value(@alertPriorityId2, @audioDurationId, @currentTime, 0);
		call insert_int_value(@alertPriorityId2, @ntLog, @currentTime, 0);

    INSERT INTO objects(object_type_id_fk)
        VALUES (@lastTypeId);

        SET @alertPriorityId3 = last_insert_id();

        CALL insert_varchar_value(
             @alertPriorityId3, @namePropertyId, @currentTime, 'MINOR');
        CALL insert_long_value(
             @alertPriorityId3, @dismissIntervalPropertyId, @currentTime, 2592000000);

        call insert_link_value(@alertPriorityId3,@tiersPropertyId,@currentTime,@TierId7);
        call insert_link_value(@alertPriorityId3,@tiersPropertyId,@currentTime,@TierId8);
        call insert_link_value(@alertPriorityId3,@tiersPropertyId,@currentTime,@TierId9);

		call insert_int_value(@alertPriorityId3, @redSignalId, @currentTime, 0);
		call insert_int_value(@alertPriorityId3, @greenSignalId, @currentTime, 0);
		call insert_int_value(@alertPriorityId3, @yellowSignalId, @currentTime, 0);
		call insert_int_value(@alertPriorityId3, @audioSignalId, @currentTime, 0);
		call insert_long_value(@alertPriorityId3, @lightDurationId, @currentTime, 0);
		call insert_long_value(@alertPriorityId3, @audioDurationId, @currentTime, 0);
		call insert_int_value(@alertPriorityId3, @ntLog, @currentTime, 0);

    INSERT INTO objects(object_type_id_fk)
        VALUES (@lastTypeId);

        SET @alertPriorityId4 = last_insert_id();

        CALL insert_varchar_value(
             @alertPriorityId4, @namePropertyId, @currentTime, 'INFORMATIONAL');
        CALL insert_long_value(
             @alertPriorityId4, @dismissIntervalPropertyId, @currentTime, 2592000000);

        call insert_link_value(@alertPriorityId4,@tiersPropertyId,@currentTime,@TierId10);

		call insert_int_value(@alertPriorityId4, @redSignalId, @currentTime, 0);
		call insert_int_value(@alertPriorityId4, @greenSignalId, @currentTime, 0);
		call insert_int_value(@alertPriorityId4, @yellowSignalId, @currentTime, 0);
		call insert_int_value(@alertPriorityId4, @audioSignalId, @currentTime, 0);
		call insert_long_value(@alertPriorityId4, @lightDurationId, @currentTime, 0);
		call insert_long_value(@alertPriorityId4, @audioDurationId, @currentTime, 0);
		call insert_int_value(@alertPriorityId4, @ntLog, @currentTime, 0);

-- create type 'ALERT_TYPE'
    INSERT INTO object_types(name)
        VALUES ('ALERT_TYPE');

    SET @lastTypeId := last_insert_id();

    INSERT INTO objects(
                  name, object_type_id_fk, static)
        VALUES ('ALERT_TYPE', @lastTypeId, 1);

    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('name', @lastTypeId, 3);

    set @namePropertyId = last_insert_id();

    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('displayName', @lastTypeId, 3);

    set @displayNamePropertyId = last_insert_id();

    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('description', @lastTypeId, 3);

    set @descrPropertyId = last_insert_id();

    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('active', @lastTypeId, 1);

    set @activePropertyId = last_insert_id();

    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('autoAck', @lastTypeId, 1);

    set @autoAckPropertyId = last_insert_id();

    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('autoDismiss', @lastTypeId, 1);

    set @autoDismissPropertyId = last_insert_id();

    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('autoDismissAllowed', @lastTypeId, 1);

    set @autoDismissAllowedPropertyId = last_insert_id();

    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('priority', @lastTypeId, 5);

    set @priorityPropertyId = last_insert_id();

    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk,historical,collection)
    values ('templates', @lastTypeId, 5,0,1);

    set @templatesPropertyId = last_insert_id();

    set @currentTime = UNIX_TIMESTAMP() * 1000;

-- create objects 'ALERT_TYPE'
    INSERT INTO objects(object_type_id_fk)
        VALUES (@lastTypeId);

        SET @alertTypeId1 = last_insert_id();

        CALL insert_varchar_value(
             @alertTypeId1, @namePropertyId, @currentTime, 'UNKNOWN');
        CALL insert_varchar_value(
             @alertTypeId1, @displayNamePropertyId, @currentTime, 'UNKNOWN');
        CALL insert_varchar_value(
             @alertTypeId1, @descrPropertyId, @currentTime, 'Generic alert, any alert with non-existing type is saved as UNKNOWN');
        CALL insert_int_value(
             @alertTypeId1, @activePropertyId, @currentTime, 1);
        CALL insert_int_value(
             @alertTypeId1, @autoAckPropertyId, @currentTime, 0);
        CALL insert_int_value(
             @alertTypeId1, @autoDismissPropertyId, @currentTime, 0);
        CALL insert_int_value(
             @alertTypeId1, @autoDismissAllowedPropertyId, @currentTime, 0);
        CALL insert_link_value(
             @alertTypeId1, @priorityPropertyId, @currentTime, @alertPriorityId2);
        CALL insert_link_value(
             @alertTypeId1, @templatesPropertyId, @currentTime, @csnTemplateId);
		CALL insert_link_value(
             @alertTypeId1, @templatesPropertyId, @currentTime, @welsnTemplateId);

    INSERT INTO objects(object_type_id_fk)
        VALUES (@lastTypeId);

        SET @alertTypeId2 = last_insert_id();

        CALL insert_varchar_value(
             @alertTypeId2, @namePropertyId, @currentTime, 'WSN_CONFIGURATION');
        CALL insert_varchar_value(
             @alertTypeId2, @displayNamePropertyId, @currentTime, 'WSN_CONFIGURATION');
        CALL insert_varchar_value(
             @alertTypeId2, @descrPropertyId, @currentTime, 'Alert about wrong Wireless Sensor Network configuration');
        CALL insert_int_value(
             @alertTypeId2, @activePropertyId, @currentTime, 1);
        CALL insert_int_value(
             @alertTypeId2, @autoAckPropertyId, @currentTime, 0);
        CALL insert_int_value(
             @alertTypeId2, @autoDismissPropertyId, @currentTime, 0);
        CALL insert_int_value(
             @alertTypeId2, @autoDismissAllowedPropertyId, @currentTime, 0);
        CALL insert_link_value(
             @alertTypeId2, @priorityPropertyId, @currentTime, @alertPriorityId2);
        CALL insert_link_value(
             @alertTypeId2, @templatesPropertyId, @currentTime, @csnTemplateId);
		CALL insert_link_value(
             @alertTypeId2, @templatesPropertyId, @currentTime, @welsnTemplateId);


    INSERT INTO objects(object_type_id_fk)
        VALUES (@lastTypeId);

        SET @alertTypeId3 = last_insert_id();

        CALL insert_varchar_value(
             @alertTypeId3, @namePropertyId, @currentTime, 'Audit Alert');
        CALL insert_varchar_value(
             @alertTypeId3, @displayNamePropertyId, @currentTime, 'Audit Alert');
        CALL insert_varchar_value(
             @alertTypeId3, @descrPropertyId, @currentTime, 'This alert is triggered when communication between any two system components is lost.');
        CALL insert_int_value(
             @alertTypeId3, @activePropertyId, @currentTime, 1);
        CALL insert_int_value(
             @alertTypeId3, @autoAckPropertyId, @currentTime, 0);
        CALL insert_int_value(
             @alertTypeId3, @autoDismissPropertyId, @currentTime, 1);
        CALL insert_int_value(
             @alertTypeId3, @autoDismissAllowedPropertyId, @currentTime, 1);
        CALL insert_link_value(
             @alertTypeId3, @priorityPropertyId, @currentTime, @alertPriorityId2);
        CALL insert_link_value(
             @alertTypeId3, @templatesPropertyId, @currentTime, @csnTemplateId);
        CALL insert_link_value(
             @alertTypeId3, @templatesPropertyId, @currentTime, @welsnTemplateId);

    INSERT INTO objects(object_type_id_fk)
        VALUES (@lastTypeId);

        SET @alertTypeId4 = last_insert_id();

        CALL insert_varchar_value(
             @alertTypeId4, @namePropertyId, @currentTime, 'Sensor disconnected');
        CALL insert_varchar_value(
             @alertTypeId4, @displayNamePropertyId, @currentTime, 'Sensor disconnected');
        CALL insert_varchar_value(
             @alertTypeId4, @descrPropertyId, @currentTime, 'Triggers when sensor is disconnected from the node');
        CALL insert_int_value(
             @alertTypeId4, @activePropertyId, @currentTime, 0);
        CALL insert_int_value(
             @alertTypeId4, @autoAckPropertyId, @currentTime, 0);
        CALL insert_int_value(
             @alertTypeId4, @autoDismissPropertyId, @currentTime, 1);
        CALL insert_int_value(
             @alertTypeId4, @autoDismissAllowedPropertyId, @currentTime, 1);
        CALL insert_link_value(
             @alertTypeId4, @priorityPropertyId, @currentTime, @alertPriorityId2);
        CALL insert_link_value(
             @alertTypeId4, @templatesPropertyId, @currentTime, @csnTemplateId);
		CALL insert_link_value(
             @alertTypeId4, @templatesPropertyId, @currentTime, @welsnTemplateId);

    INSERT INTO objects(object_type_id_fk)
        VALUES (@lastTypeId);

        SET @alertTypeId5 = last_insert_id();

        CALL insert_varchar_value(
             @alertTypeId5, @namePropertyId, @currentTime, 'Sensor requires service');
        CALL insert_varchar_value(
             @alertTypeId5, @displayNamePropertyId, @currentTime, 'Sensor requires service');
        CALL insert_varchar_value(
             @alertTypeId5, @descrPropertyId, @currentTime, 'Triggers when sensor requires service');
        CALL insert_int_value(
             @alertTypeId5, @activePropertyId, @currentTime, 0);
        CALL insert_int_value(
             @alertTypeId5, @autoAckPropertyId, @currentTime, 0);
        CALL insert_int_value(
             @alertTypeId5, @autoDismissPropertyId, @currentTime, 1);
        CALL insert_int_value(
             @alertTypeId5, @autoDismissAllowedPropertyId, @currentTime, 1);
        CALL insert_link_value(
             @alertTypeId5, @priorityPropertyId, @currentTime, @alertPriorityId2);
        CALL insert_link_value(
             @alertTypeId5, @templatesPropertyId, @currentTime, @csnTemplateId);
		CALL insert_link_value(
             @alertTypeId5, @templatesPropertyId, @currentTime, @welsnTemplateId);

    INSERT INTO objects(object_type_id_fk)
        VALUES (@lastTypeId);

        SET @alertTypeId6 = last_insert_id();

        CALL insert_varchar_value(
             @alertTypeId6, @namePropertyId, @currentTime, 'Node not reporting');
        CALL insert_varchar_value(
             @alertTypeId6, @displayNamePropertyId, @currentTime, 'Node not reporting');
        CALL insert_varchar_value(
             @alertTypeId6, @descrPropertyId, @currentTime, "Triggers when node doesn't send data");
        CALL insert_int_value(
             @alertTypeId6, @activePropertyId, @currentTime, 0);
        CALL insert_int_value(
             @alertTypeId6, @autoAckPropertyId, @currentTime, 0);
        CALL insert_int_value(
             @alertTypeId6, @autoDismissPropertyId, @currentTime, 1);
        CALL insert_int_value(
             @alertTypeId6, @autoDismissAllowedPropertyId, @currentTime, 1);
        CALL insert_link_value(
             @alertTypeId6, @priorityPropertyId, @currentTime, @alertPriorityId2);
        CALL insert_link_value(
             @alertTypeId6, @templatesPropertyId, @currentTime, @csnTemplateId);
		CALL insert_link_value(
             @alertTypeId6, @templatesPropertyId, @currentTime, @welsnTemplateId);

    INSERT INTO objects(object_type_id_fk)
        VALUES (@lastTypeId);

        SET @alertTypeId7 = last_insert_id();

        CALL insert_varchar_value(
             @alertTypeId7, @namePropertyId, @currentTime, 'Out of recommended range');
        CALL insert_varchar_value(
             @alertTypeId7, @displayNamePropertyId, @currentTime, 'Out of recommended range');
        CALL insert_varchar_value(
             @alertTypeId7, @descrPropertyId, @currentTime, 'Triggers when rack cold aisle temperature goes out of configured recommended range');
        CALL insert_int_value(
             @alertTypeId7, @activePropertyId, @currentTime, 0);
        CALL insert_int_value(
             @alertTypeId7, @autoAckPropertyId, @currentTime, 0);
        CALL insert_int_value(
             @alertTypeId7, @autoDismissPropertyId, @currentTime, 1);
        CALL insert_int_value(
             @alertTypeId7, @autoDismissAllowedPropertyId, @currentTime, 1);
        CALL insert_link_value(
             @alertTypeId7, @priorityPropertyId, @currentTime, @alertPriorityId2);
        CALL insert_link_value(
             @alertTypeId7, @templatesPropertyId, @currentTime, @csnTemplateId);
		CALL insert_link_value(
             @alertTypeId7, @templatesPropertyId, @currentTime, @welsnTemplateId);

    INSERT INTO objects(object_type_id_fk)
        VALUES (@lastTypeId);

        SET @alertTypeId8 = last_insert_id();

        CALL insert_varchar_value(
             @alertTypeId8, @namePropertyId, @currentTime, 'Out of allowable range');
        CALL insert_varchar_value(
             @alertTypeId8, @displayNamePropertyId, @currentTime, 'Out of allowable range');
        CALL insert_varchar_value(
             @alertTypeId8, @descrPropertyId, @currentTime, 'Triggers when rack cold aisle temperature goes out of configured allowable range');
        CALL insert_int_value(
             @alertTypeId8, @activePropertyId, @currentTime, 1);
        CALL insert_int_value(
             @alertTypeId8, @autoAckPropertyId, @currentTime, 0);
        CALL insert_int_value(
             @alertTypeId8, @autoDismissPropertyId, @currentTime, 1);
        CALL insert_int_value(
             @alertTypeId8, @autoDismissAllowedPropertyId, @currentTime, 1);
        CALL insert_link_value(
             @alertTypeId8, @priorityPropertyId, @currentTime, @alertPriorityId2);
        CALL insert_link_value(
             @alertTypeId8, @templatesPropertyId, @currentTime, @csnTemplateId);
		CALL insert_link_value(
             @alertTypeId8, @templatesPropertyId, @currentTime, @welsnTemplateId);

    INSERT INTO objects(object_type_id_fk)
        VALUES (@lastTypeId);

        SET @alertTypeId9 = last_insert_id();

        CALL insert_varchar_value(
             @alertTypeId9, @namePropertyId, @currentTime, 'Low battery');
        CALL insert_varchar_value(
             @alertTypeId9, @displayNamePropertyId, @currentTime, 'Low battery');
        CALL insert_varchar_value(
             @alertTypeId9, @descrPropertyId, @currentTime, 'Triggers when battery status becomes low');
        CALL insert_int_value(
             @alertTypeId9, @activePropertyId, @currentTime, 1);
        CALL insert_int_value(
             @alertTypeId9, @autoAckPropertyId, @currentTime, 0);
        CALL insert_int_value(
             @alertTypeId9, @autoDismissPropertyId, @currentTime, 0);
        CALL insert_int_value(
             @alertTypeId9, @autoDismissAllowedPropertyId, @currentTime, 0);
        CALL insert_link_value(
             @alertTypeId9, @priorityPropertyId, @currentTime, @alertPriorityId2);
        CALL insert_link_value(
             @alertTypeId9, @templatesPropertyId, @currentTime, @csnTemplateId);
		CALL insert_link_value(
             @alertTypeId9, @templatesPropertyId, @currentTime, @welsnTemplateId);

    INSERT INTO objects(object_type_id_fk)
        VALUES (@lastTypeId);

        SET @alertTypeId10 = last_insert_id();

        CALL insert_varchar_value(
             @alertTypeId10, @namePropertyId, @currentTime, 'Circuit Current above threshold');
        CALL insert_varchar_value(
             @alertTypeId10, @displayNamePropertyId, @currentTime, 'Circuit Current above threshold');
        CALL insert_varchar_value(
             @alertTypeId10, @descrPropertyId, @currentTime, 'Circuit Current above threshold');
        CALL insert_int_value(
             @alertTypeId10, @activePropertyId, @currentTime, 0);
        CALL insert_int_value(
             @alertTypeId10, @autoAckPropertyId, @currentTime, 0);
        CALL insert_int_value(
             @alertTypeId10, @autoDismissPropertyId, @currentTime, 1);
        CALL insert_int_value(
             @alertTypeId10, @autoDismissAllowedPropertyId, @currentTime, 1);
        CALL insert_link_value(
             @alertTypeId10, @priorityPropertyId, @currentTime, @alertPriorityId3);
        CALL insert_link_value(
             @alertTypeId10, @templatesPropertyId, @currentTime, @csnTemplateId);
		CALL insert_link_value(
             @alertTypeId10, @templatesPropertyId, @currentTime, @welsnTemplateId);

    INSERT INTO objects(object_type_id_fk)
        VALUES (@lastTypeId);

        SET @alertTypeId11 = last_insert_id();

        CALL insert_varchar_value(
             @alertTypeId11, @namePropertyId, @currentTime, 'Circuit Power above threshold');
        CALL insert_varchar_value(
             @alertTypeId11, @displayNamePropertyId, @currentTime, 'Circuit Power above threshold');
        CALL insert_varchar_value(
             @alertTypeId11, @descrPropertyId, @currentTime, 'Circuit Power above threshold');
        CALL insert_int_value(
             @alertTypeId11, @activePropertyId, @currentTime, 0);
        CALL insert_int_value(
             @alertTypeId11, @autoAckPropertyId, @currentTime, 0);
        CALL insert_int_value(
             @alertTypeId11, @autoDismissPropertyId, @currentTime, 1);
        CALL insert_int_value(
             @alertTypeId11, @autoDismissAllowedPropertyId, @currentTime, 1);
        CALL insert_link_value(
             @alertTypeId11, @priorityPropertyId, @currentTime, @alertPriorityId3);
        CALL insert_link_value(
             @alertTypeId11, @templatesPropertyId, @currentTime, @csnTemplateId);
		CALL insert_link_value(
             @alertTypeId11, @templatesPropertyId, @currentTime, @welsnTemplateId);

    INSERT INTO objects(object_type_id_fk)
        VALUES (@lastTypeId);

        SET @alertTypeId12 = last_insert_id();

        CALL insert_varchar_value(
             @alertTypeId12, @namePropertyId, @currentTime, 'Circuit Power Factor below threshold');
        CALL insert_varchar_value(
             @alertTypeId12, @displayNamePropertyId, @currentTime, 'Circuit Power Factor below threshold');
        CALL insert_varchar_value(
             @alertTypeId12, @descrPropertyId, @currentTime, 'Circuit Power Factor below threshold');
        CALL insert_int_value(
             @alertTypeId12, @activePropertyId, @currentTime, 0);
        CALL insert_int_value(
             @alertTypeId12, @autoAckPropertyId, @currentTime, 0);
        CALL insert_int_value(
             @alertTypeId12, @autoDismissPropertyId, @currentTime, 1);
        CALL insert_int_value(
             @alertTypeId12, @autoDismissAllowedPropertyId, @currentTime, 1);
        CALL insert_link_value(
             @alertTypeId12, @priorityPropertyId, @currentTime, @alertPriorityId3);
        CALL insert_link_value(
             @alertTypeId12, @templatesPropertyId, @currentTime, @csnTemplateId);
		CALL insert_link_value(
             @alertTypeId12, @templatesPropertyId, @currentTime, @welsnTemplateId);

    INSERT INTO objects(object_type_id_fk)
        VALUES (@lastTypeId);

        SET @alertTypeId13 = last_insert_id();

        CALL insert_varchar_value(
             @alertTypeId13, @namePropertyId, @currentTime, 'Communication error');
        CALL insert_varchar_value(
             @alertTypeId13, @displayNamePropertyId, @currentTime, 'Communication error');
        CALL insert_varchar_value(
             @alertTypeId13, @descrPropertyId, @currentTime, 'Triggers when one or more data points can not be acquired');
        CALL insert_int_value(
             @alertTypeId13, @activePropertyId, @currentTime, 1);
        CALL insert_int_value(
             @alertTypeId13, @autoAckPropertyId, @currentTime, 0);
        CALL insert_int_value(
             @alertTypeId13, @autoDismissPropertyId, @currentTime, 1);
        CALL insert_int_value(
             @alertTypeId13, @autoDismissAllowedPropertyId, @currentTime, 1);
        CALL insert_link_value(
             @alertTypeId13, @priorityPropertyId, @currentTime, @alertPriorityId1);
        CALL insert_link_value(
             @alertTypeId13, @templatesPropertyId, @currentTime, @csnTemplateId);
		CALL insert_link_value(
             @alertTypeId13, @templatesPropertyId, @currentTime, @welsnTemplateId);

    INSERT INTO objects(object_type_id_fk)
        VALUES (@lastTypeId);

        SET @alertTypeId14 = last_insert_id();

        CALL insert_varchar_value(
             @alertTypeId14, @namePropertyId, @currentTime, 'Critical system alert');
        CALL insert_varchar_value(
             @alertTypeId14, @displayNamePropertyId, @currentTime, 'Critical system alert');
        CALL insert_varchar_value(
             @alertTypeId14, @descrPropertyId, @currentTime, 'This alert is triggered when a critical error preventing a system component from functioning has occurred');
        CALL insert_int_value(
             @alertTypeId14, @activePropertyId, @currentTime, 1);
        CALL insert_int_value(
             @alertTypeId14, @autoAckPropertyId, @currentTime, 0);
        CALL insert_int_value(
             @alertTypeId14, @autoDismissPropertyId, @currentTime, 1);
        CALL insert_int_value(
             @alertTypeId14, @autoDismissAllowedPropertyId, @currentTime, 1);
        CALL insert_link_value(
             @alertTypeId14, @priorityPropertyId, @currentTime, @alertPriorityId1);
        CALL insert_link_value(
             @alertTypeId14, @templatesPropertyId, @currentTime, @csnTemplateId);
		CALL insert_link_value(
             @alertTypeId14, @templatesPropertyId, @currentTime, @welsnTemplateId);

    INSERT INTO objects(object_type_id_fk)
        VALUES (@lastTypeId);

        SET @alertTypeId15 = last_insert_id();

        CALL insert_varchar_value(
             @alertTypeId15, @namePropertyId, @currentTime, 'Maximum Threshold Alert');
        CALL insert_varchar_value(
             @alertTypeId15, @displayNamePropertyId, @currentTime, 'Maximum Threshold Alert');
        CALL insert_varchar_value(
             @alertTypeId15, @descrPropertyId, @currentTime, 'This alert is triggered when average current for a given RPDU reaches or exceeds the maximum threshold configured for that RPDU');
        CALL insert_int_value(
             @alertTypeId15, @activePropertyId, @currentTime, 0);
        CALL insert_int_value(
             @alertTypeId15, @autoAckPropertyId, @currentTime, 0);
        CALL insert_int_value(
             @alertTypeId15, @autoDismissPropertyId, @currentTime, 1);
        CALL insert_int_value(
             @alertTypeId15, @autoDismissAllowedPropertyId, @currentTime, 1);
        CALL insert_link_value(
             @alertTypeId15, @priorityPropertyId, @currentTime, @alertPriorityId1);
        CALL insert_link_value(
             @alertTypeId15, @templatesPropertyId, @currentTime, @csnTemplateId);
		CALL insert_link_value(
             @alertTypeId15, @templatesPropertyId, @currentTime, @welsnTemplateId);

    INSERT INTO objects(object_type_id_fk)
        VALUES (@lastTypeId);

        SET @alertTypeId16 = last_insert_id();

        CALL insert_varchar_value(
             @alertTypeId16, @namePropertyId, @currentTime, 'Unbalanced Phases Alert');
        CALL insert_varchar_value(
             @alertTypeId16, @displayNamePropertyId, @currentTime, 'Unbalanced Phases Alert');
        CALL insert_varchar_value(
             @alertTypeId16, @descrPropertyId, @currentTime, 'This alert is triggered when load on one or more phases in a given RPDU exceeds the other Phase(s) by more than an allowable variation');
        CALL insert_int_value(
             @alertTypeId16, @activePropertyId, @currentTime, 0);
        CALL insert_int_value(
             @alertTypeId16, @autoAckPropertyId, @currentTime, 0);
        CALL insert_int_value(
             @alertTypeId16, @autoDismissPropertyId, @currentTime, 1);
        CALL insert_int_value(
             @alertTypeId16, @autoDismissAllowedPropertyId, @currentTime, 1);
        CALL insert_link_value(
             @alertTypeId16, @priorityPropertyId, @currentTime, @alertPriorityId1);
        CALL insert_link_value(
             @alertTypeId16, @templatesPropertyId, @currentTime, @csnTemplateId);
		CALL insert_link_value(
             @alertTypeId16, @templatesPropertyId, @currentTime, @welsnTemplateId);

    INSERT INTO objects(object_type_id_fk)
        VALUES (@lastTypeId);

        SET @alertTypeId17 = last_insert_id();

        CALL insert_varchar_value(
             @alertTypeId17, @namePropertyId, @currentTime, 'Math Error Alert');
        CALL insert_varchar_value(
             @alertTypeId17, @displayNamePropertyId, @currentTime, 'Math Error Alert');
        CALL insert_varchar_value(
             @alertTypeId17, @descrPropertyId, @currentTime, 'This alert is triggered when a math error is detected in a CRE Rule');
        CALL insert_int_value(
             @alertTypeId17, @activePropertyId, @currentTime, 1);
        CALL insert_int_value(
             @alertTypeId17, @autoAckPropertyId, @currentTime, 0);
        CALL insert_int_value(
             @alertTypeId17, @autoDismissPropertyId, @currentTime, 1);
        CALL insert_int_value(
             @alertTypeId17, @autoDismissAllowedPropertyId, @currentTime, 1);
        CALL insert_link_value(
             @alertTypeId17, @priorityPropertyId, @currentTime, @alertPriorityId1);
        CALL insert_link_value(
             @alertTypeId17, @templatesPropertyId, @currentTime, @csnTemplateId);
		CALL insert_link_value(
             @alertTypeId17, @templatesPropertyId, @currentTime, @welsnTemplateId);

    INSERT INTO objects(object_type_id_fk)
        VALUES (@lastTypeId);

        SET @alertTypeId18 = last_insert_id();

        CALL insert_varchar_value(
            @alertTypeId18, @namePropertyId, @currentTime, 'Critical battery');
        CALL insert_varchar_value(
            @alertTypeId18, @displayNamePropertyId, @currentTime, 'Critical battery');
        CALL insert_varchar_value(
            @alertTypeId18, @descrPropertyId, @currentTime, 'Triggers when battery status becomes critical');
        CALL insert_int_value(
            @alertTypeId18, @activePropertyId, @currentTime, 1);
        CALL insert_int_value(
            @alertTypeId18, @autoAckPropertyId, @currentTime, 0);
        CALL insert_int_value(
            @alertTypeId18, @autoDismissPropertyId, @currentTime, 0);
        CALL insert_int_value(
            @alertTypeId18, @autoDismissAllowedPropertyId, @currentTime, 0);
        CALL insert_link_value(
            @alertTypeId18, @priorityPropertyId, @currentTime, @alertPriorityId1);
        CALL insert_link_value(
            @alertTypeId18, @templatesPropertyId, @currentTime, @csnTemplateId);
        CALL insert_link_value(
            @alertTypeId18, @templatesPropertyId, @currentTime, @welsnTemplateId);

-- create type 'ALERT'
    INSERT INTO object_types(name)
        VALUES ('ALERT');

    SET @lastTypeId := last_insert_id();

    INSERT INTO objects(
                  name, object_type_id_fk, static)
        VALUES ('ALERT', @lastTypeId, 1);

    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('name', @lastTypeId, 3);

    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('type', @lastTypeId, 5);

    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('time', @lastTypeId, 7);

    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('status', @lastTypeId, 1);

    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('message', @lastTypeId, 3);

    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('fullMessage', @lastTypeId, 3);

    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('hostTO', @lastTypeId, 5);

    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('acknowledgement', @lastTypeId, 3);

    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('acknowledgementTime', @lastTypeId, 7);

    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('user', @lastTypeId, 3);

    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('lastNTierName', @lastTypeId, 3);

    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('lastAttempt', @lastTypeId, 1);

-- create type 'ALERT_DESTINATION'
    INSERT INTO object_types(name)
        VALUES ('ALERT_DESTINATION');

    SET @lastTypeId := last_insert_id();

    INSERT INTO objects(
                  name, object_type_id_fk, static)
        VALUES ('ALERT_DESTINATION', @lastTypeId, 1);

    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('destination', @lastTypeId, 3);

    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('messagetype', @lastTypeId, 1);

    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('alertType', @lastTypeId, 5);

    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk)
    values ('tier', @lastTypeId, 3);

-- create type 'ALERT_HISTORY'
    INSERT INTO object_types(name)
        VALUES ('ALERT_HISTORY');

    SET @lastTypeId := last_insert_id();

    INSERT INTO objects(
                  name, object_type_id_fk, static)
        VALUES ('ALERT_HISTORY', @lastTypeId, 1);

    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk, historical)
    values ('alertId', @lastTypeId, 1, 1);

    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk, historical)
    values ('name', @lastTypeId, 3, 1);

    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk, historical)
    values ('typeName', @lastTypeId, 3, 1);

    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk, historical)
    values ('typeDescr', @lastTypeId, 3, 1);

    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk, historical)
    values ('priority', @lastTypeId, 3, 1);

    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk, historical)
    values ('time', @lastTypeId, 7, 1);

    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk, historical)
    values ('status', @lastTypeId, 1, 1);

    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk, historical)
    values ('message', @lastTypeId, 3, 1);

    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk, historical)
    values ('fullMessage', @lastTypeId, 3, 1);

    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk, historical)
    values ('hostTO', @lastTypeId, 1, 1);

    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk, historical)
    values ('hostType', @lastTypeId, 3, 1);

    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk, historical)
    values ('acknowledgement', @lastTypeId, 3, 1);

    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk, historical)
    values ('acknowledgementTime', @lastTypeId, 7, 1);

    insert into attributes(
                  name, object_type_id_fk, attr_type_id_fk, historical)
    values ('user', @lastTypeId, 3, 1);

-- create type 'REPORT_TEMPLATE'
INSERT INTO object_types(name) VALUES ('REPORT_TEMPLATE');

SET @lastTypeId := last_insert_id();

INSERT INTO objects(name, object_type_id_fk, static) VALUES ('REPORT_TEMPLATE', @lastTypeId, 1);

insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('name', @lastTypeId, 3);
set @namePropertyId = last_insert_id();
insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('design', @lastTypeId, 8);
set @designPropertyId = last_insert_id();
insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('owner', @lastTypeId, 3);
set @ownerPropertyId = last_insert_id();
insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('private', @lastTypeId, 1);
set @privatePropertyId = last_insert_id();
insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('system', @lastTypeId, 1);
set @systemPropertyId = last_insert_id();
insert into attributes(name, object_type_id_fk, attr_type_id_fk, collection) values ('parameters', @lastTypeId, 5, 1);
set @parametersPropertyId = last_insert_id();

-- Tabular Custom Query
-- Data should be kept in sync with middle\es\es-core\src\main\resources\com\synapsense\impl\reporting\templates\Tabular Custom Query.jrxml
INSERT INTO `objects` (`object_type_id_fk`,`name`,`static`) VALUES (@lastTypeId,null,0);
set @tableTemplateId = last_insert_id();
CALL insert_varchar_value(@tableTemplateId, @namePropertyId, @currentTime, 'Tabular Custom Query');
CALL insert_binarydata_value(@tableTemplateId, @designPropertyId, @currentTime, BINARY '<?xml version="1.0" encoding="UTF-8"?>\r\n<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="Tabular Custom Query" language="groovy" pageWidth="842" pageHeight="595" orientation="Landscape" columnWidth="802" leftMargin="20" rightMargin="20" topMargin="20" bottomMargin="20" uuid="6e83898a-70df-4a86-a40a-703eb2898507">\r\n	<property name="ireport.zoom" value="1.0"/>\r\n	<property name="ireport.x" value="0"/>\r\n	<property name="ireport.y" value="0"/>\r\n	<style name="Title" fontName="Arial" fontSize="26" isBold="true" pdfFontName="Helvetica-Bold"/>\r\n	<style name="SubTitle" forecolor="#666666" fontName="Arial" fontSize="18"/>\r\n	<style name="Column header" forecolor="#666666" fontName="Arial" fontSize="12" isBold="true"/>\r\n	<style name="Detail" fontName="Arial" fontSize="12"/>\r\n	<style name="table">\r\n		<box>\r\n			<pen lineWidth="1.0" lineColor="#000000"/>\r\n		</box>\r\n	</style>\r\n	<style name="table_TH" mode="Opaque" backcolor="#F0F8FF">\r\n		<box>\r\n			<pen lineWidth="0.5" lineColor="#000000"/>\r\n		</box>\r\n	</style>\r\n	<style name="table_CH" mode="Opaque" backcolor="#BFE1FF">\r\n		<box>\r\n			<pen lineWidth="0.5" lineColor="#000000"/>\r\n		</box>\r\n	</style>\r\n	<style name="table_TD" mode="Opaque" backcolor="#FFFFFF">\r\n		<box>\r\n			<pen lineWidth="0.5" lineColor="#000000"/>\r\n		</box>\r\n	</style>\r\n	<style name="table 1" hAlign="Center" vAlign="Top" fontName="Arial" isItalic="true" isUnderline="true" isStrikeThrough="true">\r\n		<pen lineColor="#004574"/>\r\n		<box>\r\n			<pen lineWidth="1.0" lineColor="#004574"/>\r\n			<topPen lineWidth="1.0" lineColor="#004574"/>\r\n			<leftPen lineWidth="1.0" lineColor="#004574"/>\r\n			<bottomPen lineWidth="1.0" lineColor="#004574"/>\r\n			<rightPen lineWidth="1.0" lineColor="#004574"/>\r\n		</box>\r\n	</style>\r\n	<style name="table 1_TH" mode="Opaque" backcolor="#F0F8FF">\r\n		<box>\r\n			<pen lineWidth="0.5" lineColor="#004574"/>\r\n			<topPen lineColor="#004574"/>\r\n			<leftPen lineColor="#004574"/>\r\n			<bottomPen lineColor="#004574"/>\r\n			<rightPen lineColor="#004574"/>\r\n		</box>\r\n	</style>\r\n	<style name="table 1_CH" mode="Opaque" backcolor="#E6E6E6">\r\n		<box>\r\n			<pen lineWidth="0.5" lineColor="#004574"/>\r\n			<topPen lineColor="#004574"/>\r\n			<leftPen lineColor="#004574"/>\r\n			<bottomPen lineColor="#004574"/>\r\n			<rightPen lineColor="#004574"/>\r\n		</box>\r\n	</style>\r\n	<style name="table 1_TD" mode="Opaque" backcolor="#FFFFFF" hAlign="Left" pattern="">\r\n		<box>\r\n			<pen lineWidth="0.5" lineColor="#004574"/>\r\n			<topPen lineColor="#004574"/>\r\n			<leftPen lineColor="#004574"/>\r\n			<bottomPen lineColor="#004574"/>\r\n			<rightPen lineColor="#004574"/>\r\n		</box>\r\n		<conditionalStyle>\r\n			<conditionExpression><![CDATA[new Boolean($V{REPORT_COUNT}.intValue()%2==0)]]></conditionExpression>\r\n			<style backcolor="#F3F6F8"/>\r\n		</conditionalStyle>\r\n	</style>\r\n	<subDataset name="Intake Table" uuid="975d2a89-db86-4532-8d91-d82a25dbe3b3">\r\n		<field name="Object Name" class="java.lang.String"/>\r\n		<field name="Data Type" class="java.lang.String"/>\r\n		<field name="Data Value" class="java.lang.String"/>\r\n		<field name="Timestamp" class="java.lang.String"/>\r\n	</subDataset>\r\n	<parameter name="Custom Query ID" class="java.lang.String">\r\n        <parameterDescription><![CDATA[ {"uiComponentType": "CustomQuerySelector"}]]></parameterDescription>\r\n	</parameter>\r\n	<parameter name="Report Title" class="java.lang.String"/>\r\n	<parameter name="Report Period (hours)" class="java.lang.Long"/>\r\n	<title>\r\n		<band height="30" splitType="Stretch">\r\n			<textField pattern="" isBlankWhenNull="true">\r\n				<reportElement uuid="7abf160c-87af-43c5-8266-c1ec0484f139" style="Title" mode="Opaque" x="154" y="0" width="648" height="26" forecolor="#004574" backcolor="#E6E6E6"/>\r\n				<textElement textAlignment="Center" verticalAlignment="Middle">\r\n					<font size="16"/>\r\n				</textElement>\r\n				<textFieldExpression><![CDATA[$P{Report Title}]]></textFieldExpression>\r\n			</textField>\r\n			<image scaleImage="RetainShape" vAlign="Middle" isUsingCache="true">\r\n				<reportElement uuid="d5eceb83-3d12-45ed-a919-4182696b7965" mode="Opaque" x="0" y="0" width="154" height="26" forecolor="#E6E6E6" backcolor="#E6E6E6"/>\r\n				<imageExpression><![CDATA["http://localhost:8080/synapsoft/resources/images/slogo_g.png"]]></imageExpression>\r\n			</image>\r\n			<rectangle>\r\n				<reportElement uuid="1d301550-0f1e-4454-a939-6f214f98c4ec" x="0" y="26" width="801" height="4" forecolor="#004574" backcolor="#004574"/>\r\n			</rectangle>\r\n		</band>\r\n	</title>\r\n	<detail>\r\n		<band height="495">\r\n			<componentElement>\r\n				<reportElement uuid="3b3b8fee-7a60-4c6d-91c5-3f07b06d0a32" key="table 1" style="table 1" stretchType="RelativeToTallestObject" isPrintRepeatedValues="false" mode="Transparent" x="25" y="8" width="755" height="50" isRemoveLineWhenBlank="true" backcolor="#E6E6E6"/>\r\n				<jr:table xmlns:jr="http://jasperreports.sourceforge.net/jasperreports/components" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports/components http://jasperreports.sourceforge.net/xsd/components.xsd">\r\n					<datasetRun subDataset="Intake Table" uuid="13ad075d-f787-449e-aa49-befe4517e642">\r\n						<dataSourceExpression><![CDATA[new com.synapsense.service.reporting.datasource.TabularCustomQueryDataSource(com.synapsense.dto.TOFactory.getInstance().loadTO($P{Custom Query ID}), new java.util.Date(System.currentTimeMillis() - $P{Report Period (hours)} * 60 * 60* 1000), new java.util.Date())]]></dataSourceExpression>\r\n					</datasetRun>\r\n					<jr:column width="200" uuid="f2eeb8f4-684c-4529-90d7-3ed1a3eac5c1">\r\n						<jr:columnHeader style="table 1_CH" height="31" rowSpan="1">\r\n							<staticText>\r\n								<reportElement uuid="980e6b59-26b2-401e-ab0f-85f7274c436f" positionType="Float" x="10" y="0" width="180" height="30" forecolor="#004574"/>\r\n								<textElement textAlignment="Center" verticalAlignment="Middle">\r\n									<font fontName="Arial" size="12" isBold="true"/>\r\n									<paragraph tabStopWidth="10"/>\r\n								</textElement>\r\n								<text><![CDATA[Object Name]]></text>\r\n							</staticText>\r\n						</jr:columnHeader>\r\n						<jr:detailCell style="table 1_TD" height="21" rowSpan="1">\r\n							<textField isBlankWhenNull="true">\r\n								<reportElement uuid="6ee1268b-37a4-4a60-96fb-914b76da610d" positionType="Float" stretchType="RelativeToTallestObject" x="0" y="0" width="200" height="21"/>\r\n								<textElement textAlignment="Left" verticalAlignment="Middle">\r\n									<font fontName="Arial"/>\r\n									<paragraph leftIndent="2" tabStopWidth="10"/>\r\n								</textElement>\r\n								<textFieldExpression><![CDATA[$F{Object Name}]]></textFieldExpression>\r\n							</textField>\r\n						</jr:detailCell>\r\n					</jr:column>\r\n					<jr:column width="200" uuid="2bbcc08a-616f-4b91-8693-3baeca6b8573">\r\n						<jr:columnHeader style="table 1_CH" height="31" rowSpan="1">\r\n							<staticText>\r\n								<reportElement uuid="9b22dab4-8390-4980-a428-66c96242f521" positionType="Float" x="10" y="0" width="180" height="30" forecolor="#004574"/>\r\n								<textElement textAlignment="Center" verticalAlignment="Middle">\r\n									<font fontName="Arial" size="12" isBold="true"/>\r\n									<paragraph tabStopWidth="10"/>\r\n								</textElement>\r\n								<text><![CDATA[Data Type]]></text>\r\n							</staticText>\r\n						</jr:columnHeader>\r\n						<jr:detailCell style="table 1_TD" height="21" rowSpan="1">\r\n							<textField isStretchWithOverflow="true" isBlankWhenNull="true">\r\n								<reportElement uuid="6e6f85a5-f8c2-4c80-800a-7eb744e2beee" positionType="Float" stretchType="RelativeToTallestObject" x="10" y="0" width="180" height="21"/>\r\n								<textElement textAlignment="Center" verticalAlignment="Middle">\r\n									<font fontName="Arial" size="10"/>\r\n									<paragraph leftIndent="2" tabStopWidth="10"/>\r\n								</textElement>\r\n								<textFieldExpression><![CDATA[$F{Data Type}]]></textFieldExpression>\r\n							</textField>\r\n						</jr:detailCell>\r\n					</jr:column>\r\n					<jr:column width="150" uuid="02bcc846-cc5a-4a7c-aa55-7e4447b6e6be">\r\n						<jr:columnHeader style="table 1_CH" height="31" rowSpan="1">\r\n							<staticText>\r\n								<reportElement uuid="94c19e4b-bd7d-4e6f-af88-eb91b91e903f" positionType="Float" x="10" y="0" width="130" height="30" forecolor="#004574"/>\r\n								<textElement textAlignment="Center" verticalAlignment="Middle">\r\n									<font fontName="Arial" size="12" isBold="true"/>\r\n									<paragraph tabStopWidth="10"/>\r\n								</textElement>\r\n								<text><![CDATA[Data Value]]></text>\r\n							</staticText>\r\n						</jr:columnHeader>\r\n						<jr:detailCell style="table 1_TD" height="21" rowSpan="1">\r\n							<textField isStretchWithOverflow="true" isBlankWhenNull="true">\r\n								<reportElement uuid="8f2bdabd-bc72-42bb-b7eb-3389c0594af1" positionType="Float" stretchType="RelativeToTallestObject" x="10" y="0" width="130" height="21"/>\r\n								<textElement textAlignment="Right" verticalAlignment="Middle">\r\n									<font fontName="Arial" size="10"/>\r\n									<paragraph leftIndent="2" tabStopWidth="10"/>\r\n								</textElement>\r\n								<textFieldExpression><![CDATA[$F{Data Value}]]></textFieldExpression>\r\n							</textField>\r\n						</jr:detailCell>\r\n					</jr:column>\r\n					<jr:column width="200" uuid="56973cbc-8cb2-4387-97fe-cb8ce3ae0558">\r\n						<jr:columnHeader style="table 1_CH" height="31" rowSpan="1">\r\n							<staticText>\r\n								<reportElement uuid="9bf21c32-8766-4fa1-a162-f54207c5a43b" positionType="Float" x="10" y="0" width="180" height="30" forecolor="#004574"/>\r\n								<textElement textAlignment="Center" verticalAlignment="Middle">\r\n									<font fontName="Arial" size="12" isBold="true"/>\r\n									<paragraph tabStopWidth="10"/>\r\n								</textElement>\r\n								<text><![CDATA[Timestamp]]></text>\r\n							</staticText>\r\n						</jr:columnHeader>\r\n						<jr:detailCell style="table 1_TD" height="21" rowSpan="1">\r\n							<textField isBlankWhenNull="true">\r\n								<reportElement uuid="5e537807-7f4e-402c-8f2a-244dcc77a70a" positionType="Float" stretchType="RelativeToTallestObject" x="10" y="0" width="180" height="21"/>\r\n								<textElement textAlignment="Center" verticalAlignment="Middle">\r\n									<font fontName="Arial" size="10"/>\r\n									<paragraph tabStopWidth="10"/>\r\n								</textElement>\r\n								<textFieldExpression><![CDATA[$F{Timestamp}]]></textFieldExpression>\r\n							</textField>\r\n						</jr:detailCell>\r\n					</jr:column>\r\n				</jr:table>\r\n			</componentElement>\r\n		</band>\r\n	</detail>\r\n	<pageFooter>\r\n		<band height="26">\r\n			<rectangle>\r\n				<reportElement uuid="1a757c5f-6f4f-48cc-acca-a1d945284927" x="0" y="4" width="801" height="4" forecolor="#004574" backcolor="#004574"/>\r\n			</rectangle>\r\n			<textField>\r\n				<reportElement uuid="226504f0-51cb-425d-b2c0-d2942e74f769" x="705" y="7" width="80" height="15"/>\r\n				<textElement textAlignment="Right" verticalAlignment="Middle">\r\n					<font size="8"/>\r\n				</textElement>\r\n				<textFieldExpression><![CDATA["Page "+$V{PAGE_NUMBER}+" of"]]></textFieldExpression>\r\n			</textField>\r\n			<textField evaluationTime="Report">\r\n				<reportElement uuid="5e43c5f2-6ab3-4cb4-a0a4-4192afd8ab6e" x="786" y="7" width="14" height="15"/>\r\n				<textElement textAlignment="Right" verticalAlignment="Middle">\r\n					<font size="8"/>\r\n				</textElement>\r\n				<textFieldExpression><![CDATA[" " + $V{PAGE_NUMBER}]]></textFieldExpression>\r\n			</textField>\r\n			<textField pattern="MM/dd/yyyy HH.mm.ss">\r\n				<reportElement uuid="77d93b44-b5f4-4919-afca-a73ffcca8098" x="6" y="7" width="100" height="15"/>\r\n				<textElement verticalAlignment="Middle">\r\n					<font size="8"/>\r\n				</textElement>\r\n				<textFieldExpression><![CDATA[new java.util.Date()]]></textFieldExpression>\r\n			</textField>\r\n		</band>\r\n	</pageFooter>\r\n</jasperReport>\r\n');
CALL insert_varchar_value(@tableTemplateId, @ownerPropertyId, @currentTime, 'synapsense');
CALL insert_int_value(@tableTemplateId, @privatePropertyId, @currentTime, 0);
CALL insert_int_value(@tableTemplateId, @systemPropertyId, @currentTime, 1);
set @tableParametersId = insert_object_value(@tableTemplateId, @parametersPropertyId, @currentTime);


-- Chart Custom Query
-- Data should be kept in sync with middle\es\es-core\src\main\resources\com\synapsense\impl\reporting\templates\Chart Custom Query.jrxml
INSERT INTO `objects` (`object_type_id_fk`,`name`,`static`) VALUES (@lastTypeId,null,0);
set @chartTemplateId = last_insert_id();
CALL insert_varchar_value(@chartTemplateId, @namePropertyId, @currentTime, 'Chart Custom Query');
CALL insert_binarydata_value(@chartTemplateId, @designPropertyId, @currentTime, BINARY '<?xml version="1.0" encoding="UTF-8"?>\r\n<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="Chart Custom Query" language="groovy" pageWidth="842" pageHeight="595" orientation="Landscape" columnWidth="802" leftMargin="20" rightMargin="20" topMargin="20" bottomMargin="20" uuid="6e83898a-70df-4a86-a40a-703eb2898507">\r\n	<property name="ireport.zoom" value="8.0"/>\r\n	<property name="ireport.x" value="5040"/>\r\n	<property name="ireport.y" value="4211"/>\r\n	<style name="Title" fontName="Arial" fontSize="26" isBold="true" pdfFontName="Helvetica-Bold"/>\r\n	<style name="SubTitle" forecolor="#666666" fontName="Arial" fontSize="18"/>\r\n	<style name="Column header" forecolor="#666666" fontName="Arial" fontSize="12" isBold="true" pdfFontName="Helvetica-Bold"/>\r\n	<style name="Detail" fontName="Arial" fontSize="12"/>\r\n	<style name="table">\r\n		<box>\r\n			<pen lineWidth="1.0" lineColor="#000000"/>\r\n		</box>\r\n	</style>\r\n	<style name="table_TH" mode="Opaque" backcolor="#F0F8FF">\r\n		<box>\r\n			<pen lineWidth="0.5" lineColor="#000000"/>\r\n		</box>\r\n	</style>\r\n	<style name="table_CH" mode="Opaque" backcolor="#BFE1FF">\r\n		<box>\r\n			<pen lineWidth="0.5" lineColor="#000000"/>\r\n		</box>\r\n	</style>\r\n	<style name="table_TD" mode="Opaque" backcolor="#FFFFFF">\r\n		<box>\r\n			<pen lineWidth="0.5" lineColor="#000000"/>\r\n		</box>\r\n	</style>\r\n	<style name="table 1">\r\n		<box>\r\n			<pen lineWidth="1.0" lineColor="#000000"/>\r\n		</box>\r\n	</style>\r\n	<style name="table 1_TH" mode="Opaque" backcolor="#F0F8FF">\r\n		<box>\r\n			<pen lineWidth="0.5" lineColor="#000000"/>\r\n		</box>\r\n	</style>\r\n	<style name="table 1_CH" mode="Opaque" backcolor="#BFE1FF">\r\n		<box>\r\n			<pen lineWidth="0.5" lineColor="#000000"/>\r\n		</box>\r\n	</style>\r\n	<style name="table 1_TD" mode="Opaque" backcolor="#FFFFFF">\r\n		<box>\r\n			<pen lineWidth="0.5" lineColor="#000000"/>\r\n		</box>\r\n	</style>\r\n	<parameter name="Custom Query ID" class="java.lang.String">\r\n		<parameterDescription><![CDATA[{"uiComponentType": "CustomQuerySelector"}]]></parameterDescription>\r\n	</parameter>\r\n	<parameter name="Report Title" class="java.lang.String"/>\r\n	<parameter name="Report Period (hours)" class="java.lang.Integer"/>\r\n	<variable name="Chart" class="net.sf.jasperreports.engine.JRRenderable" incrementType="Report" calculation="System"/>\r\n	<title>\r\n		<band height="30" splitType="Stretch">\r\n			<textField pattern="">\r\n				<reportElement uuid="7abf160c-87af-43c5-8266-c1ec0484f139" style="Title" mode="Opaque" x="154" y="0" width="648" height="26" forecolor="#004574" backcolor="#E6E6E6"/>\r\n				<textElement textAlignment="Center" verticalAlignment="Middle">\r\n					<font size="16"/>\r\n				</textElement>\r\n				<textFieldExpression><![CDATA[$P{Report Title}]]></textFieldExpression>\r\n			</textField>\r\n			<image scaleImage="RetainShape" vAlign="Middle" isUsingCache="true">\r\n				<reportElement uuid="b62513d5-b730-4fa1-ada0-7a585460ebf7" mode="Opaque" x="0" y="0" width="154" height="26" forecolor="#E6E6E6" backcolor="#E6E6E6"/>\r\n				<imageExpression><![CDATA["http://localhost:8080/synapsoft/resources/images/slogo_g.png"]]></imageExpression>\r\n			</image>\r\n			<rectangle>\r\n				<reportElement uuid="15ba970b-b491-4adb-b37d-330a53e7e49e" mode="Opaque" x="0" y="26" width="801" height="4" forecolor="#004574" backcolor="#004574"/>\r\n			</rectangle>\r\n		</band>\r\n	</title>\r\n	<detail>\r\n		<band height="499">\r\n			<image scaleImage="Clip" hAlign="Center" vAlign="Middle" hyperlinkType="Reference">\r\n				<reportElement uuid="d4eaf274-eec9-48fa-b781-21c886b79738" x="20" y="8" width="790" height="477"/>\r\n				<imageExpression><![CDATA[$V{Chart}]]></imageExpression>\r\n				<hyperlinkReferenceExpression><![CDATA["http://www.jfree.org/jfreechart"]]></hyperlinkReferenceExpression>\r\n			</image>\r\n		</band>\r\n	</detail>\r\n	<pageFooter>\r\n		<band height="22">\r\n			<rectangle>\r\n				<reportElement uuid="33bebb29-2881-4175-b788-03b70f088f46" x="0" y="0" width="801" height="4" forecolor="#004574" backcolor="#004574"/>\r\n			</rectangle>\r\n			<textField pattern="MM/dd/yyyy HH.mm.ss">\r\n				<reportElement uuid="dd488bd5-ce01-4883-8c41-7d1e6086a107" x="6" y="7" width="100" height="15" forecolor="#000000"/>\r\n				<textElement textAlignment="Left" verticalAlignment="Middle">\r\n					<font size="8"/>\r\n				</textElement>\r\n				<textFieldExpression><![CDATA[new java.util.Date()]]></textFieldExpression>\r\n			</textField>\r\n			<textField>\r\n				<reportElement uuid="fda9e4a2-8a8b-4cd1-b8ee-97fd91570aa8" x="705" y="7" width="80" height="15"/>\r\n				<textElement textAlignment="Right" verticalAlignment="Middle">\r\n					<font size="8"/>\r\n				</textElement>\r\n				<textFieldExpression><![CDATA["Page "+$V{PAGE_NUMBER}+" of"]]></textFieldExpression>\r\n			</textField>\r\n			<textField evaluationTime="Report">\r\n				<reportElement uuid="f4a4ee45-43a0-4ebf-9c98-8b86319aa459" x="786" y="7" width="14" height="15"/>\r\n				<textElement textAlignment="Right" verticalAlignment="Middle">\r\n					<font size="8"/>\r\n				</textElement>\r\n				<textFieldExpression><![CDATA[" " + $V{PAGE_NUMBER}]]></textFieldExpression>\r\n			</textField>\r\n		</band>\r\n	</pageFooter>\r\n</jasperReport>\r\n');
CALL insert_varchar_value(@chartTemplateId, @ownerPropertyId, @currentTime, 'synapsense');
CALL insert_int_value(@chartTemplateId, @privatePropertyId, @currentTime, 0);
CALL insert_int_value(@chartTemplateId, @systemPropertyId, @currentTime, 1);
set @chartParametersId = insert_object_value(@chartTemplateId, @parametersPropertyId, @currentTime);

-- create type 'REPORT_TASK'
INSERT INTO object_types(name) VALUES ('REPORT_TASK');

SET @lastTypeId := last_insert_id();

INSERT INTO objects(name, object_type_id_fk, static) VALUES ('REPORT_TASK', @lastTypeId, 1);

insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('name', @lastTypeId, 3);
insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('templateId', @lastTypeId, 1);
insert into attributes(name, object_type_id_fk, attr_type_id_fk, collection) values ('parameterValues', @lastTypeId, 5, 1);
insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('cron', @lastTypeId, 3);
insert into attributes(name, object_type_id_fk, attr_type_id_fk, collection) values ('users', @lastTypeId, 3, 1);
insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('status', @lastTypeId, 1);
insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('owner', @lastTypeId, 3);
insert into attributes(name, object_type_id_fk, attr_type_id_fk, collection) values ('formats', @lastTypeId, 3, 1);

-- create type 'REPORT'
INSERT INTO object_types(name) VALUES ('REPORT');

SET @lastTypeId := last_insert_id();

INSERT INTO objects(name, object_type_id_fk, static) VALUES ('REPORT', @lastTypeId, 1);

insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('taskId', @lastTypeId, 1);
insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('timestamp', @lastTypeId, 7);
insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('dataPath', @lastTypeId, 3);
insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('userName', @lastTypeId, 3);
insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('taskName', @lastTypeId, 3);

-- create type 'REPORT_PARAMETER'
INSERT INTO object_types(name) VALUES ('REPORT_PARAMETER');

SET @lastTypeId := last_insert_id();

INSERT INTO objects(name, object_type_id_fk, static) VALUES ('REPORT_PARAMETER', @lastTypeId, 1);

insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('name', @lastTypeId, 3);
set @namePropertyId = last_insert_id();
insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('type', @lastTypeId, 3);
set @typePropertyId = last_insert_id();
insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('templateId', @lastTypeId, 1);
set @templatePropertyId = last_insert_id();
insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('orderId', @lastTypeId, 1);
set @orderPropertyId = last_insert_id();
insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('description', @lastTypeId, 3);
set @descriptionPropertyId = last_insert_id();

-- Tabular CustomQuery Parameter
INSERT INTO `objects` (`object_type_id_fk`,`name`,`static`) VALUES (@lastTypeId,null,0);
set @paramId = last_insert_id();
CALL insert_varchar_value(@paramId, @namePropertyId, @currentTime, 'Custom Query ID');
CALL insert_varchar_value(@paramId, @typePropertyId, @currentTime, 'java.lang.String');
CALL insert_int_value(@paramId, @templatePropertyId, @currentTime, @tableTemplateId);
CALL add_link_value(@tableParametersId, @currentTime, @paramId);
CALL insert_int_value(@paramId, @orderPropertyId, @currentTime, 0);
CALL insert_varchar_value(@paramId, @descriptionPropertyId, @currentTime, '{"uiComponentType": "CustomQuerySelector"}');

-- Tabular Title Parameter
INSERT INTO `objects` (`object_type_id_fk`,`name`,`static`) VALUES (@lastTypeId,null,0);
set @paramId = last_insert_id();
CALL insert_varchar_value(@paramId, @namePropertyId, @currentTime, 'Report Title');
CALL insert_varchar_value(@paramId, @typePropertyId, @currentTime, 'java.lang.String');
CALL insert_int_value(@paramId, @templatePropertyId, @currentTime, @tableTemplateId);
CALL add_link_value(@tableParametersId, @currentTime, @paramId);
CALL insert_int_value(@paramId, @orderPropertyId, @currentTime, 1);

-- Tabular IntervalInHours Parameter
INSERT INTO `objects` (`object_type_id_fk`,`name`,`static`) VALUES (@lastTypeId,null,0);
set @paramId = last_insert_id();
CALL insert_varchar_value(@paramId, @namePropertyId, @currentTime, 'Report Period (hours)');
CALL insert_varchar_value(@paramId, @typePropertyId, @currentTime, 'java.lang.Long');
CALL insert_int_value(@paramId, @templatePropertyId, @currentTime, @tableTemplateId);
CALL add_link_value(@tableParametersId, @currentTime, @paramId);
CALL insert_int_value(@paramId, @orderPropertyId, @currentTime, 2);

-- Chart CustomQuery Parameter
INSERT INTO `objects` (`object_type_id_fk`,`name`,`static`) VALUES (@lastTypeId,null,0);
set @paramId = last_insert_id();
CALL insert_varchar_value(@paramId, @namePropertyId, @currentTime, 'Custom Query ID');
CALL insert_varchar_value(@paramId, @typePropertyId, @currentTime, 'java.lang.String');
CALL insert_int_value(@paramId, @templatePropertyId, @currentTime, @chartTemplateId);
CALL add_link_value(@chartParametersId, @currentTime, @paramId);
CALL insert_int_value(@paramId, @orderPropertyId, @currentTime, 0);
CALL insert_varchar_value(@paramId, @descriptionPropertyId, @currentTime, '{"uiComponentType": "CustomQuerySelector"}');

-- Chart Title Parameter
INSERT INTO `objects` (`object_type_id_fk`,`name`,`static`) VALUES (@lastTypeId,null,0);
set @paramId = last_insert_id();
CALL insert_varchar_value(@paramId, @namePropertyId, @currentTime, 'Report Title');
CALL insert_varchar_value(@paramId, @typePropertyId, @currentTime, 'java.lang.String');
CALL insert_int_value(@paramId, @templatePropertyId, @currentTime, @chartTemplateId);
CALL add_link_value(@chartParametersId, @currentTime, @paramId);
CALL insert_int_value(@paramId, @orderPropertyId, @currentTime, 1);

-- Chart IntervalInHours Parameter
INSERT INTO `objects` (`object_type_id_fk`,`name`,`static`) VALUES (@lastTypeId,null,0);
set @paramId = last_insert_id();
CALL insert_varchar_value(@paramId, @namePropertyId, @currentTime, 'Report Period (hours)');
CALL insert_varchar_value(@paramId, @typePropertyId, @currentTime, 'java.lang.Integer');
CALL insert_int_value(@paramId, @templatePropertyId, @currentTime, @chartTemplateId);
CALL add_link_value(@chartParametersId, @currentTime, @paramId);
CALL insert_int_value(@paramId, @orderPropertyId, @currentTime, 2);

-- create type 'REPORT_PARAMETER_VALUE'
INSERT INTO object_types(name) VALUES ('REPORT_PARAMETER_VALUE');

SET @lastTypeId := last_insert_id();

INSERT INTO objects(name, object_type_id_fk, static) VALUES ('REPORT_PARAMETER_VALUE', @lastTypeId, 1);

insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('value', @lastTypeId, 3);
insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('reportTaskId', @lastTypeId, 1);
insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('parameterId', @lastTypeId, 1);
insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('integerValue', @lastTypeId, 1);
insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('dateValue', @lastTypeId, 4);
insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('toValue', @lastTypeId, 5);
insert into attributes(name, object_type_id_fk, attr_type_id_fk, collection) values ('setTOValue', @lastTypeId, 5, 1);
insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('longValue', @lastTypeId, 7);

-- create type 'EXPORTED_REPORT'
INSERT INTO object_types(name) VALUES ('EXPORTED_REPORT');

SET @lastTypeId := last_insert_id();

INSERT INTO objects(name, object_type_id_fk, static) VALUES ('EXPORTED_REPORT', @lastTypeId, 1);

insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('reportId', @lastTypeId, 1);
insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('format', @lastTypeId, 3);
insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('exportedPath', @lastTypeId, 3);

-- create type 'USER_GROUP'
INSERT INTO object_types(name) VALUES ('USER_GROUP');

SET @groupTypeId := last_insert_id();

INSERT INTO objects(name, object_type_id_fk, static) VALUES ('USER_GROUP', @groupTypeId, 1);

insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('name', @groupTypeId, 3);
SET @groupNameId := last_insert_id();
insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('description', @groupTypeId, 3);
SET @groupDescrId := last_insert_id();
insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('statistics', @groupTypeId, 3);
SET @groupStatId := last_insert_id();

-- create type 'USER_PRIVILEGE'
INSERT INTO object_types(name) VALUES ('USER_PRIVILEGE');

SET @privTypeId := last_insert_id();

INSERT INTO objects(name, object_type_id_fk, static) VALUES ('USER_PRIVILEGE', @privTypeId, 1);

insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('name', @privTypeId, 3);
SET @privNameId := last_insert_id();
insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('description', @privTypeId, 3);
SET @privDescrId := last_insert_id();
insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('system', @privTypeId, 1);
SET @privSysId := last_insert_id();
insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('tag', @privTypeId, 3);
SET @privTagId := last_insert_id();
insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('sortOrder', @privTypeId, 1);
SET @privSortId := last_insert_id();

-- ==================================================================================
-- =    Create objects 'USER_GROUP', 'USER_PRIVILEGE' and relations between them.   =
-- =    This block of code can be generetad with middle\Tools\PermissionsCSVParser  =
-- ==================================================================================

-- create objects 'USER_GROUP'

INSERT INTO objects(object_type_id_fk) VALUES (@groupTypeId);
SET @Administrators_GroupId := last_insert_id();
CALL insert_varchar_value(@Administrators_GroupId, @groupNameId, @currentTime, 'Administrators');

INSERT INTO objects(object_type_id_fk) VALUES (@groupTypeId);
SET @Power_GroupId := last_insert_id();
CALL insert_varchar_value(@Power_GroupId, @groupNameId, @currentTime, 'Power');

INSERT INTO objects(object_type_id_fk) VALUES (@groupTypeId);
SET @Standard_GroupId := last_insert_id();
CALL insert_varchar_value(@Standard_GroupId, @groupNameId, @currentTime, 'Standard');

-- create objects 'USER_PRIVILEGE' (system)

INSERT INTO objects(object_type_id_fk) VALUES (@privTypeId);
SET @root_permission_PrivId := last_insert_id();
CALL insert_varchar_value(@root_permission_PrivId, @privNameId, @currentTime, 'root_permission');
CALL insert_int_value(@root_permission_PrivId, @privSysId, @currentTime, 1);

INSERT INTO objects(object_type_id_fk) VALUES (@privTypeId);
SET @get_data_PrivId := last_insert_id();
CALL insert_varchar_value(@get_data_PrivId, @privNameId, @currentTime, 'get_data');
CALL insert_int_value(@get_data_PrivId, @privSysId, @currentTime, 1);

INSERT INTO objects(object_type_id_fk) VALUES (@privTypeId);
SET @edit_data_PrivId := last_insert_id();
CALL insert_varchar_value(@edit_data_PrivId, @privNameId, @currentTime, 'edit_data');
CALL insert_int_value(@edit_data_PrivId, @privSysId, @currentTime, 1);

INSERT INTO objects(object_type_id_fk) VALUES (@privTypeId);
SET @edit_meta_data_PrivId := last_insert_id();
CALL insert_varchar_value(@edit_meta_data_PrivId, @privNameId, @currentTime, 'edit_meta_data');
CALL insert_int_value(@edit_meta_data_PrivId, @privSysId, @currentTime, 1);

INSERT INTO objects(object_type_id_fk) VALUES (@privTypeId);
SET @get_user_information_PrivId := last_insert_id();
CALL insert_varchar_value(@get_user_information_PrivId, @privNameId, @currentTime, 'get_user_information');
CALL insert_int_value(@get_user_information_PrivId, @privSysId, @currentTime, 1);

INSERT INTO objects(object_type_id_fk) VALUES (@privTypeId);
SET @get_user_data_PrivId := last_insert_id();
CALL insert_varchar_value(@get_user_data_PrivId, @privNameId, @currentTime, 'get_user_data');
CALL insert_int_value(@get_user_data_PrivId, @privSysId, @currentTime, 1);

INSERT INTO objects(object_type_id_fk) VALUES (@privTypeId);
SET @edit_user_data_PrivId := last_insert_id();
CALL insert_varchar_value(@edit_user_data_PrivId, @privNameId, @currentTime, 'edit_user_data');
CALL insert_int_value(@edit_user_data_PrivId, @privSysId, @currentTime, 1);

INSERT INTO objects(object_type_id_fk) VALUES (@privTypeId);
SET @edit_user_preferences_PrivId := last_insert_id();
CALL insert_varchar_value(@edit_user_preferences_PrivId, @privNameId, @currentTime, 'edit_user_preferences');
CALL insert_int_value(@edit_user_preferences_PrivId, @privSysId, @currentTime, 1);

INSERT INTO objects(object_type_id_fk) VALUES (@privTypeId);
SET @read_file_PrivId := last_insert_id();
CALL insert_varchar_value(@read_file_PrivId, @privNameId, @currentTime, 'read_file');
CALL insert_int_value(@read_file_PrivId, @privSysId, @currentTime, 1);

INSERT INTO objects(object_type_id_fk) VALUES (@privTypeId);
SET @write_file_PrivId := last_insert_id();
CALL insert_varchar_value(@write_file_PrivId, @privNameId, @currentTime, 'write_file');
CALL insert_int_value(@write_file_PrivId, @privSysId, @currentTime, 1);

-- create objects 'USER_PRIVILEGE'

INSERT INTO objects(object_type_id_fk) VALUES (@privTypeId);
SET @ACCESS_PERMISSIONS_PrivId := last_insert_id();
CALL insert_varchar_value(@ACCESS_PERMISSIONS_PrivId, @privNameId, @currentTime, 'ACCESS_PERMISSIONS');
CALL insert_varchar_value(@ACCESS_PERMISSIONS_PrivId, @privDescrId, @currentTime, 'Access Site Permissons');
CALL insert_int_value(@ACCESS_PERMISSIONS_PrivId, @privSysId, @currentTime, 6);
CALL insert_varchar_value(@ACCESS_PERMISSIONS_PrivId, @privTagId, @currentTime, 'SYSTEM_CONFIGURATION');
CALL insert_int_value(@ACCESS_PERMISSIONS_PrivId, @privSortId, @currentTime, 100);

INSERT INTO objects(object_type_id_fk) VALUES (@privTypeId);
SET @ADD_REMOVE_USER_PERMISSIONS_PrivId := last_insert_id();
CALL insert_varchar_value(@ADD_REMOVE_USER_PERMISSIONS_PrivId, @privNameId, @currentTime, 'ADD_REMOVE_USER_PERMISSIONS');
CALL insert_varchar_value(@ADD_REMOVE_USER_PERMISSIONS_PrivId, @privDescrId, @currentTime, 'Edit Site Permissions');
CALL insert_int_value(@ADD_REMOVE_USER_PERMISSIONS_PrivId, @privSysId, @currentTime, 6);
CALL insert_varchar_value(@ADD_REMOVE_USER_PERMISSIONS_PrivId, @privTagId, @currentTime, 'SYSTEM_CONFIGURATION');
CALL insert_int_value(@ADD_REMOVE_USER_PERMISSIONS_PrivId, @privSortId, @currentTime, 200);

INSERT INTO objects(object_type_id_fk) VALUES (@privTypeId);
SET @VIEW_DATA_CENTER_PROPERTIES_PrivId := last_insert_id();
CALL insert_varchar_value(@VIEW_DATA_CENTER_PROPERTIES_PrivId, @privNameId, @currentTime, 'VIEW_DATA_CENTER_PROPERTIES');
CALL insert_varchar_value(@VIEW_DATA_CENTER_PROPERTIES_PrivId, @privDescrId, @currentTime, 'Access Center Properties');
CALL insert_int_value(@VIEW_DATA_CENTER_PROPERTIES_PrivId, @privSysId, @currentTime, 6);
CALL insert_varchar_value(@VIEW_DATA_CENTER_PROPERTIES_PrivId, @privTagId, @currentTime, 'SYSTEM_CONFIGURATION');
CALL insert_int_value(@VIEW_DATA_CENTER_PROPERTIES_PrivId, @privSortId, @currentTime, 300);

INSERT INTO objects(object_type_id_fk) VALUES (@privTypeId);
SET @EDIT_DATA_CENTER_PROPERTIES_GENERAL_PrivId := last_insert_id();
CALL insert_varchar_value(@EDIT_DATA_CENTER_PROPERTIES_GENERAL_PrivId, @privNameId, @currentTime, 'EDIT_DATA_CENTER_PROPERTIES_GENERAL');
CALL insert_varchar_value(@EDIT_DATA_CENTER_PROPERTIES_GENERAL_PrivId, @privDescrId, @currentTime, 'Edit Data Center Properties');
CALL insert_int_value(@EDIT_DATA_CENTER_PROPERTIES_GENERAL_PrivId, @privSysId, @currentTime, 6);
CALL insert_varchar_value(@EDIT_DATA_CENTER_PROPERTIES_GENERAL_PrivId, @privTagId, @currentTime, 'SYSTEM_CONFIGURATION');
CALL insert_int_value(@EDIT_DATA_CENTER_PROPERTIES_GENERAL_PrivId, @privSortId, @currentTime, 400);

INSERT INTO objects(object_type_id_fk) VALUES (@privTypeId);
SET @ACCESS_MANAGE_USERS_PrivId := last_insert_id();
CALL insert_varchar_value(@ACCESS_MANAGE_USERS_PrivId, @privNameId, @currentTime, 'ACCESS_MANAGE_USERS');
CALL insert_varchar_value(@ACCESS_MANAGE_USERS_PrivId, @privDescrId, @currentTime, 'Access Users');
CALL insert_int_value(@ACCESS_MANAGE_USERS_PrivId, @privSysId, @currentTime, 6);
CALL insert_varchar_value(@ACCESS_MANAGE_USERS_PrivId, @privTagId, @currentTime, 'SYSTEM_CONFIGURATION');
CALL insert_int_value(@ACCESS_MANAGE_USERS_PrivId, @privSortId, @currentTime, 500);

INSERT INTO objects(object_type_id_fk) VALUES (@privTypeId);
SET @CREATE_EDIT_USERS_PrivId := last_insert_id();
CALL insert_varchar_value(@CREATE_EDIT_USERS_PrivId, @privNameId, @currentTime, 'CREATE_EDIT_USERS');
CALL insert_varchar_value(@CREATE_EDIT_USERS_PrivId, @privDescrId, @currentTime, 'Configure Users');
CALL insert_int_value(@CREATE_EDIT_USERS_PrivId, @privSysId, @currentTime, 6);
CALL insert_varchar_value(@CREATE_EDIT_USERS_PrivId, @privTagId, @currentTime, 'SYSTEM_CONFIGURATION');
CALL insert_int_value(@CREATE_EDIT_USERS_PrivId, @privSortId, @currentTime, 600);

INSERT INTO objects(object_type_id_fk) VALUES (@privTypeId);
SET @ACCESS_USER_GROUPS_PrivId := last_insert_id();
CALL insert_varchar_value(@ACCESS_USER_GROUPS_PrivId, @privNameId, @currentTime, 'ACCESS_USER_GROUPS');
CALL insert_varchar_value(@ACCESS_USER_GROUPS_PrivId, @privDescrId, @currentTime, 'Access User Groups');
CALL insert_int_value(@ACCESS_USER_GROUPS_PrivId, @privSysId, @currentTime, 6);
CALL insert_varchar_value(@ACCESS_USER_GROUPS_PrivId, @privTagId, @currentTime, 'SYSTEM_CONFIGURATION');
CALL insert_int_value(@ACCESS_USER_GROUPS_PrivId, @privSortId, @currentTime, 700);

INSERT INTO objects(object_type_id_fk) VALUES (@privTypeId);
SET @ACCESS_MANAGE_ASSET_GROUPS_PrivId := last_insert_id();
CALL insert_varchar_value(@ACCESS_MANAGE_ASSET_GROUPS_PrivId, @privNameId, @currentTime, 'ACCESS_MANAGE_ASSET_GROUPS');
CALL insert_varchar_value(@ACCESS_MANAGE_ASSET_GROUPS_PrivId, @privDescrId, @currentTime, 'Access  Asset Groups');
CALL insert_int_value(@ACCESS_MANAGE_ASSET_GROUPS_PrivId, @privSysId, @currentTime, 6);
CALL insert_varchar_value(@ACCESS_MANAGE_ASSET_GROUPS_PrivId, @privTagId, @currentTime, 'SYSTEM_CONFIGURATION');
CALL insert_int_value(@ACCESS_MANAGE_ASSET_GROUPS_PrivId, @privSortId, @currentTime, 800);

INSERT INTO objects(object_type_id_fk) VALUES (@privTypeId);
SET @CREATE_EDIT_ASSET_GROUPS_PrivId := last_insert_id();
CALL insert_varchar_value(@CREATE_EDIT_ASSET_GROUPS_PrivId, @privNameId, @currentTime, 'CREATE_EDIT_ASSET_GROUPS');
CALL insert_varchar_value(@CREATE_EDIT_ASSET_GROUPS_PrivId, @privDescrId, @currentTime, 'Configure Asset groups');
CALL insert_int_value(@CREATE_EDIT_ASSET_GROUPS_PrivId, @privSysId, @currentTime, 6);
CALL insert_varchar_value(@CREATE_EDIT_ASSET_GROUPS_PrivId, @privTagId, @currentTime, 'SYSTEM_CONFIGURATION');
CALL insert_int_value(@CREATE_EDIT_ASSET_GROUPS_PrivId, @privSortId, @currentTime, 900);

INSERT INTO objects(object_type_id_fk) VALUES (@privTypeId);
SET @ACCESS_CONFIGURATION_PrivId := last_insert_id();
CALL insert_varchar_value(@ACCESS_CONFIGURATION_PrivId, @privNameId, @currentTime, 'ACCESS_CONFIGURATION');
CALL insert_varchar_value(@ACCESS_CONFIGURATION_PrivId, @privDescrId, @currentTime, 'Access Configuration');
CALL insert_int_value(@ACCESS_CONFIGURATION_PrivId, @privSysId, @currentTime, 6);
CALL insert_varchar_value(@ACCESS_CONFIGURATION_PrivId, @privTagId, @currentTime, 'SYSTEM_CONFIGURATION');
CALL insert_int_value(@ACCESS_CONFIGURATION_PrivId, @privSortId, @currentTime, 1200);

INSERT INTO objects(object_type_id_fk) VALUES (@privTypeId);
SET @EDIT_CONFIGURATION_SETTINGS_PrivId := last_insert_id();
CALL insert_varchar_value(@EDIT_CONFIGURATION_SETTINGS_PrivId, @privNameId, @currentTime, 'EDIT_CONFIGURATION_SETTINGS');
CALL insert_varchar_value(@EDIT_CONFIGURATION_SETTINGS_PrivId, @privDescrId, @currentTime, 'Edit Configuration Settings');
CALL insert_int_value(@EDIT_CONFIGURATION_SETTINGS_PrivId, @privSysId, @currentTime, 6);
CALL insert_varchar_value(@EDIT_CONFIGURATION_SETTINGS_PrivId, @privTagId, @currentTime, 'SYSTEM_CONFIGURATION');
CALL insert_int_value(@EDIT_CONFIGURATION_SETTINGS_PrivId, @privSortId, @currentTime, 1300);

INSERT INTO objects(object_type_id_fk) VALUES (@privTypeId);
SET @ACCESS_RECONCILE_ASSET_NAMES_PrivId := last_insert_id();
CALL insert_varchar_value(@ACCESS_RECONCILE_ASSET_NAMES_PrivId, @privNameId, @currentTime, 'ACCESS_RECONCILE_ASSET_NAMES');
CALL insert_varchar_value(@ACCESS_RECONCILE_ASSET_NAMES_PrivId, @privDescrId, @currentTime, 'Access Reconcile Asset Names');
CALL insert_int_value(@ACCESS_RECONCILE_ASSET_NAMES_PrivId, @privSysId, @currentTime, 6);
CALL insert_varchar_value(@ACCESS_RECONCILE_ASSET_NAMES_PrivId, @privTagId, @currentTime, 'SYSTEM_CONFIGURATION');
CALL insert_int_value(@ACCESS_RECONCILE_ASSET_NAMES_PrivId, @privSortId, @currentTime, 1400);

INSERT INTO objects(object_type_id_fk) VALUES (@privTypeId);
SET @ACCESS_SYSTEM_ACTIVITY_LOG_PrivId := last_insert_id();
CALL insert_varchar_value(@ACCESS_SYSTEM_ACTIVITY_LOG_PrivId, @privNameId, @currentTime, 'ACCESS_SYSTEM_ACTIVITY_LOG');
CALL insert_varchar_value(@ACCESS_SYSTEM_ACTIVITY_LOG_PrivId, @privDescrId, @currentTime, 'Access System Activity Log');
CALL insert_int_value(@ACCESS_SYSTEM_ACTIVITY_LOG_PrivId, @privSysId, @currentTime, 6);
CALL insert_varchar_value(@ACCESS_SYSTEM_ACTIVITY_LOG_PrivId, @privTagId, @currentTime, 'SYSTEM_CONFIGURATION');
CALL insert_int_value(@ACCESS_SYSTEM_ACTIVITY_LOG_PrivId, @privSortId, @currentTime, 1500);

INSERT INTO objects(object_type_id_fk) VALUES (@privTypeId);
SET @PRINT_EXPORT_SYSTEM_ACTIVITY_LOG_PrivId := last_insert_id();
CALL insert_varchar_value(@PRINT_EXPORT_SYSTEM_ACTIVITY_LOG_PrivId, @privNameId, @currentTime, 'PRINT_EXPORT_SYSTEM_ACTIVITY_LOG');
CALL insert_varchar_value(@PRINT_EXPORT_SYSTEM_ACTIVITY_LOG_PrivId, @privDescrId, @currentTime, 'Print/Export System Activity Log');
CALL insert_int_value(@PRINT_EXPORT_SYSTEM_ACTIVITY_LOG_PrivId, @privSysId, @currentTime, 6);
CALL insert_varchar_value(@PRINT_EXPORT_SYSTEM_ACTIVITY_LOG_PrivId, @privTagId, @currentTime, 'SYSTEM_CONFIGURATION');
CALL insert_int_value(@PRINT_EXPORT_SYSTEM_ACTIVITY_LOG_PrivId, @privSortId, @currentTime, 1600);

INSERT INTO objects(object_type_id_fk) VALUES (@privTypeId);
SET @ACCESS_SYSTEM_CONFIGURATION_REPORT_PrivId := last_insert_id();
CALL insert_varchar_value(@ACCESS_SYSTEM_CONFIGURATION_REPORT_PrivId, @privNameId, @currentTime, 'ACCESS_SYSTEM_CONFIGURATION_REPORT');
CALL insert_varchar_value(@ACCESS_SYSTEM_CONFIGURATION_REPORT_PrivId, @privDescrId, @currentTime, 'Access System Configuration Report');
CALL insert_int_value(@ACCESS_SYSTEM_CONFIGURATION_REPORT_PrivId, @privSysId, @currentTime, 6);
CALL insert_varchar_value(@ACCESS_SYSTEM_CONFIGURATION_REPORT_PrivId, @privTagId, @currentTime, 'SYSTEM_CONFIGURATION');
CALL insert_int_value(@ACCESS_SYSTEM_CONFIGURATION_REPORT_PrivId, @privSortId, @currentTime, 1700);

INSERT INTO objects(object_type_id_fk) VALUES (@privTypeId);
SET @PRINT_EXPORT_SYSTEM_CONFIGURATION_REPORT_PrivId := last_insert_id();
CALL insert_varchar_value(@PRINT_EXPORT_SYSTEM_CONFIGURATION_REPORT_PrivId, @privNameId, @currentTime, 'PRINT_EXPORT_SYSTEM_CONFIGURATION_REPORT');
CALL insert_varchar_value(@PRINT_EXPORT_SYSTEM_CONFIGURATION_REPORT_PrivId, @privDescrId, @currentTime, 'Print/Export System Configuration Report');
CALL insert_int_value(@PRINT_EXPORT_SYSTEM_CONFIGURATION_REPORT_PrivId, @privSysId, @currentTime, 6);
CALL insert_varchar_value(@PRINT_EXPORT_SYSTEM_CONFIGURATION_REPORT_PrivId, @privTagId, @currentTime, 'SYSTEM_CONFIGURATION');
CALL insert_int_value(@PRINT_EXPORT_SYSTEM_CONFIGURATION_REPORT_PrivId, @privSortId, @currentTime, 1800);

INSERT INTO objects(object_type_id_fk) VALUES (@privTypeId);
SET @ACCESS_MODBUS_POINTS_LIST_PrivId := last_insert_id();
CALL insert_varchar_value(@ACCESS_MODBUS_POINTS_LIST_PrivId, @privNameId, @currentTime, 'ACCESS_MODBUS_POINTS_LIST');
CALL insert_varchar_value(@ACCESS_MODBUS_POINTS_LIST_PrivId, @privDescrId, @currentTime, 'Access Modbus Points List');
CALL insert_int_value(@ACCESS_MODBUS_POINTS_LIST_PrivId, @privSysId, @currentTime, 6);
CALL insert_varchar_value(@ACCESS_MODBUS_POINTS_LIST_PrivId, @privTagId, @currentTime, 'SYSTEM_CONFIGURATION');
CALL insert_int_value(@ACCESS_MODBUS_POINTS_LIST_PrivId, @privSortId, @currentTime, 1900);

INSERT INTO objects(object_type_id_fk) VALUES (@privTypeId);
SET @GENERATE_DIAGNOSTIC_SUPPORT_PACKAGE_PrivId := last_insert_id();
CALL insert_varchar_value(@GENERATE_DIAGNOSTIC_SUPPORT_PACKAGE_PrivId, @privNameId, @currentTime, 'GENERATE_DIAGNOSTIC_SUPPORT_PACKAGE');
CALL insert_varchar_value(@GENERATE_DIAGNOSTIC_SUPPORT_PACKAGE_PrivId, @privDescrId, @currentTime, 'Generate Diagnostic Support Package');
CALL insert_int_value(@GENERATE_DIAGNOSTIC_SUPPORT_PACKAGE_PrivId, @privSysId, @currentTime, 6);
CALL insert_varchar_value(@GENERATE_DIAGNOSTIC_SUPPORT_PACKAGE_PrivId, @privTagId, @currentTime, 'SYSTEM_CONFIGURATION');
CALL insert_int_value(@GENERATE_DIAGNOSTIC_SUPPORT_PACKAGE_PrivId, @privSortId, @currentTime, 2200);

INSERT INTO objects(object_type_id_fk) VALUES (@privTypeId);
SET @EXPORT_FROM_MAPSENSE_PrivId := last_insert_id();
CALL insert_varchar_value(@EXPORT_FROM_MAPSENSE_PrivId, @privNameId, @currentTime, 'EXPORT_FROM_MAPSENSE');
CALL insert_varchar_value(@EXPORT_FROM_MAPSENSE_PrivId, @privDescrId, @currentTime, 'Allow export from MapSense');
CALL insert_int_value(@EXPORT_FROM_MAPSENSE_PrivId, @privSysId, @currentTime, 6);
CALL insert_varchar_value(@EXPORT_FROM_MAPSENSE_PrivId, @privTagId, @currentTime, 'SYSTEM_CONFIGURATION');
CALL insert_int_value(@EXPORT_FROM_MAPSENSE_PrivId, @privSortId, @currentTime, 2300);

INSERT INTO objects(object_type_id_fk) VALUES (@privTypeId);
SET @ACCESS_DATA_ANALYSIS_PrivId := last_insert_id();
CALL insert_varchar_value(@ACCESS_DATA_ANALYSIS_PrivId, @privNameId, @currentTime, 'ACCESS_DATA_ANALYSIS');
CALL insert_varchar_value(@ACCESS_DATA_ANALYSIS_PrivId, @privDescrId, @currentTime, 'Access Data Analysis');
CALL insert_int_value(@ACCESS_DATA_ANALYSIS_PrivId, @privSysId, @currentTime, 6);
CALL insert_varchar_value(@ACCESS_DATA_ANALYSIS_PrivId, @privTagId, @currentTime, 'REPORT_MANAGEMENT');
CALL insert_int_value(@ACCESS_DATA_ANALYSIS_PrivId, @privSortId, @currentTime, 2400);

INSERT INTO objects(object_type_id_fk) VALUES (@privTypeId);
SET @PRINT_EXPORT_QUERY_RESULTS_PrivId := last_insert_id();
CALL insert_varchar_value(@PRINT_EXPORT_QUERY_RESULTS_PrivId, @privNameId, @currentTime, 'PRINT_EXPORT_QUERY_RESULTS');
CALL insert_varchar_value(@PRINT_EXPORT_QUERY_RESULTS_PrivId, @privDescrId, @currentTime, 'Print/export query results');
CALL insert_int_value(@PRINT_EXPORT_QUERY_RESULTS_PrivId, @privSysId, @currentTime, 6);
CALL insert_varchar_value(@PRINT_EXPORT_QUERY_RESULTS_PrivId, @privTagId, @currentTime, 'REPORT_MANAGEMENT');
CALL insert_int_value(@PRINT_EXPORT_QUERY_RESULTS_PrivId, @privSortId, @currentTime, 2500);

INSERT INTO objects(object_type_id_fk) VALUES (@privTypeId);
SET @PRINT_EXPORT_CHARTS_PrivId := last_insert_id();
CALL insert_varchar_value(@PRINT_EXPORT_CHARTS_PrivId, @privNameId, @currentTime, 'PRINT_EXPORT_CHARTS');
CALL insert_varchar_value(@PRINT_EXPORT_CHARTS_PrivId, @privDescrId, @currentTime, 'Print/export charts');
CALL insert_int_value(@PRINT_EXPORT_CHARTS_PrivId, @privSysId, @currentTime, 6);
CALL insert_varchar_value(@PRINT_EXPORT_CHARTS_PrivId, @privTagId, @currentTime, 'REPORT_MANAGEMENT');
CALL insert_int_value(@PRINT_EXPORT_CHARTS_PrivId, @privSortId, @currentTime, 2600);

INSERT INTO objects(object_type_id_fk) VALUES (@privTypeId);
SET @ADD_ANNOTATIONS_PrivId := last_insert_id();
CALL insert_varchar_value(@ADD_ANNOTATIONS_PrivId, @privNameId, @currentTime, 'ADD_ANNOTATIONS');
CALL insert_varchar_value(@ADD_ANNOTATIONS_PrivId, @privDescrId, @currentTime, 'Add Annotations');
CALL insert_int_value(@ADD_ANNOTATIONS_PrivId, @privSysId, @currentTime, 6);
CALL insert_varchar_value(@ADD_ANNOTATIONS_PrivId, @privTagId, @currentTime, 'REPORT_MANAGEMENT');
CALL insert_int_value(@ADD_ANNOTATIONS_PrivId, @privSortId, @currentTime, 2700);

INSERT INTO objects(object_type_id_fk) VALUES (@privTypeId);
SET @ADD_SYSTEM_ANNOTATIONS_PrivId := last_insert_id();
CALL insert_varchar_value(@ADD_SYSTEM_ANNOTATIONS_PrivId, @privNameId, @currentTime, 'ADD_SYSTEM_ANNOTATIONS');
CALL insert_varchar_value(@ADD_SYSTEM_ANNOTATIONS_PrivId, @privDescrId, @currentTime, 'Add System Annotations');
CALL insert_int_value(@ADD_SYSTEM_ANNOTATIONS_PrivId, @privSysId, @currentTime, 6);
CALL insert_varchar_value(@ADD_SYSTEM_ANNOTATIONS_PrivId, @privTagId, @currentTime, 'REPORT_MANAGEMENT');
CALL insert_int_value(@ADD_SYSTEM_ANNOTATIONS_PrivId, @privSortId, @currentTime, 2800);

INSERT INTO objects(object_type_id_fk) VALUES (@privTypeId);
SET @ACCESS_CUSTOM_QUERIES_PrivId := last_insert_id();
CALL insert_varchar_value(@ACCESS_CUSTOM_QUERIES_PrivId, @privNameId, @currentTime, 'ACCESS_CUSTOM_QUERIES');
CALL insert_varchar_value(@ACCESS_CUSTOM_QUERIES_PrivId, @privDescrId, @currentTime, 'Access Custom Queries');
CALL insert_int_value(@ACCESS_CUSTOM_QUERIES_PrivId, @privSysId, @currentTime, 6);
CALL insert_varchar_value(@ACCESS_CUSTOM_QUERIES_PrivId, @privTagId, @currentTime, 'REPORT_MANAGEMENT');
CALL insert_int_value(@ACCESS_CUSTOM_QUERIES_PrivId, @privSortId, @currentTime, 2900);

INSERT INTO objects(object_type_id_fk) VALUES (@privTypeId);
SET @CREATE_EDIT_DELETE_OWN_PRIVATE_QUERIES_PrivId := last_insert_id();
CALL insert_varchar_value(@CREATE_EDIT_DELETE_OWN_PRIVATE_QUERIES_PrivId, @privNameId, @currentTime, 'CREATE_EDIT_DELETE_OWN_PRIVATE_QUERIES');
CALL insert_varchar_value(@CREATE_EDIT_DELETE_OWN_PRIVATE_QUERIES_PrivId, @privDescrId, @currentTime, 'Configure own private custom queries');
CALL insert_int_value(@CREATE_EDIT_DELETE_OWN_PRIVATE_QUERIES_PrivId, @privSysId, @currentTime, 6);
CALL insert_varchar_value(@CREATE_EDIT_DELETE_OWN_PRIVATE_QUERIES_PrivId, @privTagId, @currentTime, 'REPORT_MANAGEMENT');
CALL insert_int_value(@CREATE_EDIT_DELETE_OWN_PRIVATE_QUERIES_PrivId, @privSortId, @currentTime, 3000);

INSERT INTO objects(object_type_id_fk) VALUES (@privTypeId);
SET @CREATE_EDIT_DELETE_OWN_PUBLIC_QUERIES_PrivId := last_insert_id();
CALL insert_varchar_value(@CREATE_EDIT_DELETE_OWN_PUBLIC_QUERIES_PrivId, @privNameId, @currentTime, 'CREATE_EDIT_DELETE_OWN_PUBLIC_QUERIES');
CALL insert_varchar_value(@CREATE_EDIT_DELETE_OWN_PUBLIC_QUERIES_PrivId, @privDescrId, @currentTime, 'Configure own public custom queries');
CALL insert_int_value(@CREATE_EDIT_DELETE_OWN_PUBLIC_QUERIES_PrivId, @privSysId, @currentTime, 6);
CALL insert_varchar_value(@CREATE_EDIT_DELETE_OWN_PUBLIC_QUERIES_PrivId, @privTagId, @currentTime, 'REPORT_MANAGEMENT');
CALL insert_int_value(@CREATE_EDIT_DELETE_OWN_PUBLIC_QUERIES_PrivId, @privSortId, @currentTime, 3100);

INSERT INTO objects(object_type_id_fk) VALUES (@privTypeId);
SET @CREATE_EDIT_DELETE_ALL_QUERIES_PrivId := last_insert_id();
CALL insert_varchar_value(@CREATE_EDIT_DELETE_ALL_QUERIES_PrivId, @privNameId, @currentTime, 'CREATE_EDIT_DELETE_ALL_QUERIES');
CALL insert_varchar_value(@CREATE_EDIT_DELETE_ALL_QUERIES_PrivId, @privDescrId, @currentTime, 'Configure all custom queries');
CALL insert_int_value(@CREATE_EDIT_DELETE_ALL_QUERIES_PrivId, @privSysId, @currentTime, 6);
CALL insert_varchar_value(@CREATE_EDIT_DELETE_ALL_QUERIES_PrivId, @privTagId, @currentTime, 'REPORT_MANAGEMENT');
CALL insert_int_value(@CREATE_EDIT_DELETE_ALL_QUERIES_PrivId, @privSortId, @currentTime, 3200);

INSERT INTO objects(object_type_id_fk) VALUES (@privTypeId);
SET @ACCESS_FIND_AVAILABLE_CAPACITY_PrivId := last_insert_id();
CALL insert_varchar_value(@ACCESS_FIND_AVAILABLE_CAPACITY_PrivId, @privNameId, @currentTime, 'ACCESS_FIND_AVAILABLE_CAPACITY');
CALL insert_varchar_value(@ACCESS_FIND_AVAILABLE_CAPACITY_PrivId, @privDescrId, @currentTime, 'Access Find Available Capacity');
CALL insert_int_value(@ACCESS_FIND_AVAILABLE_CAPACITY_PrivId, @privSysId, @currentTime, 6);
CALL insert_varchar_value(@ACCESS_FIND_AVAILABLE_CAPACITY_PrivId, @privTagId, @currentTime, 'REPORT_MANAGEMENT');
CALL insert_int_value(@ACCESS_FIND_AVAILABLE_CAPACITY_PrivId, @privSortId, @currentTime, 3300);

INSERT INTO objects(object_type_id_fk) VALUES (@privTypeId);
SET @ACCESS_CARBON_EMISSIONS_SUMMARY_PrivId := last_insert_id();
CALL insert_varchar_value(@ACCESS_CARBON_EMISSIONS_SUMMARY_PrivId, @privNameId, @currentTime, 'ACCESS_CARBON_EMISSIONS_SUMMARY');
CALL insert_varchar_value(@ACCESS_CARBON_EMISSIONS_SUMMARY_PrivId, @privDescrId, @currentTime, 'Access Carbon Emissions Summary');
CALL insert_int_value(@ACCESS_CARBON_EMISSIONS_SUMMARY_PrivId, @privSysId, @currentTime, 6);
CALL insert_varchar_value(@ACCESS_CARBON_EMISSIONS_SUMMARY_PrivId, @privTagId, @currentTime, 'REPORT_MANAGEMENT');
CALL insert_int_value(@ACCESS_CARBON_EMISSIONS_SUMMARY_PrivId, @privSortId, @currentTime, 3400);

INSERT INTO objects(object_type_id_fk) VALUES (@privTypeId);
SET @EDIT_CARBON_EMISSIONS_TARGET_VALUES_PrivId := last_insert_id();
CALL insert_varchar_value(@EDIT_CARBON_EMISSIONS_TARGET_VALUES_PrivId, @privNameId, @currentTime, 'EDIT_CARBON_EMISSIONS_TARGET_VALUES');
CALL insert_varchar_value(@EDIT_CARBON_EMISSIONS_TARGET_VALUES_PrivId, @privDescrId, @currentTime, 'Edit Carbon Emissions Target Values');
CALL insert_int_value(@EDIT_CARBON_EMISSIONS_TARGET_VALUES_PrivId, @privSysId, @currentTime, 6);
CALL insert_varchar_value(@EDIT_CARBON_EMISSIONS_TARGET_VALUES_PrivId, @privTagId, @currentTime, 'REPORT_MANAGEMENT');
CALL insert_int_value(@EDIT_CARBON_EMISSIONS_TARGET_VALUES_PrivId, @privSortId, @currentTime, 3500);

INSERT INTO objects(object_type_id_fk) VALUES (@privTypeId);
SET @PRINT_CARBON_EMISSIONS_SUMMARY_PrivId := last_insert_id();
CALL insert_varchar_value(@PRINT_CARBON_EMISSIONS_SUMMARY_PrivId, @privNameId, @currentTime, 'PRINT_CARBON_EMISSIONS_SUMMARY');
CALL insert_varchar_value(@PRINT_CARBON_EMISSIONS_SUMMARY_PrivId, @privDescrId, @currentTime, 'Print Carbon Emissions Summary');
CALL insert_int_value(@PRINT_CARBON_EMISSIONS_SUMMARY_PrivId, @privSysId, @currentTime, 6);
CALL insert_varchar_value(@PRINT_CARBON_EMISSIONS_SUMMARY_PrivId, @privTagId, @currentTime, 'REPORT_MANAGEMENT');
CALL insert_int_value(@PRINT_CARBON_EMISSIONS_SUMMARY_PrivId, @privSortId, @currentTime, 3600);

INSERT INTO objects(object_type_id_fk) VALUES (@privTypeId);
SET @ACCESS_NETWORK_STATISTICS_PrivId := last_insert_id();
CALL insert_varchar_value(@ACCESS_NETWORK_STATISTICS_PrivId, @privNameId, @currentTime, 'ACCESS_NETWORK_STATISTICS');
CALL insert_varchar_value(@ACCESS_NETWORK_STATISTICS_PrivId, @privDescrId, @currentTime, 'Access Network Statistics');
CALL insert_int_value(@ACCESS_NETWORK_STATISTICS_PrivId, @privSysId, @currentTime, 6);
CALL insert_varchar_value(@ACCESS_NETWORK_STATISTICS_PrivId, @privTagId, @currentTime, 'REPORT_MANAGEMENT');
CALL insert_int_value(@ACCESS_NETWORK_STATISTICS_PrivId, @privSortId, @currentTime, 3700);

INSERT INTO objects(object_type_id_fk) VALUES (@privTypeId);
SET @PRINT_EXPORT_NETWORK_STATISTICS_PrivId := last_insert_id();
CALL insert_varchar_value(@PRINT_EXPORT_NETWORK_STATISTICS_PrivId, @privNameId, @currentTime, 'PRINT_EXPORT_NETWORK_STATISTICS');
CALL insert_varchar_value(@PRINT_EXPORT_NETWORK_STATISTICS_PrivId, @privDescrId, @currentTime, 'Print/Export Network Statistics');
CALL insert_int_value(@PRINT_EXPORT_NETWORK_STATISTICS_PrivId, @privSysId, @currentTime, 6);
CALL insert_varchar_value(@PRINT_EXPORT_NETWORK_STATISTICS_PrivId, @privTagId, @currentTime, 'REPORT_MANAGEMENT');
CALL insert_int_value(@PRINT_EXPORT_NETWORK_STATISTICS_PrivId, @privSortId, @currentTime, 3800);

INSERT INTO objects(object_type_id_fk) VALUES (@privTypeId);
SET @ACCESS_ALERT_DEFINITIONS_PrivId := last_insert_id();
CALL insert_varchar_value(@ACCESS_ALERT_DEFINITIONS_PrivId, @privNameId, @currentTime, 'ACCESS_ALERT_DEFINITIONS');
CALL insert_varchar_value(@ACCESS_ALERT_DEFINITIONS_PrivId, @privDescrId, @currentTime, 'Access Alert Definitions');
CALL insert_int_value(@ACCESS_ALERT_DEFINITIONS_PrivId, @privSysId, @currentTime, 6);
CALL insert_varchar_value(@ACCESS_ALERT_DEFINITIONS_PrivId, @privTagId, @currentTime, 'ALERT_MANAGEMENT');
CALL insert_int_value(@ACCESS_ALERT_DEFINITIONS_PrivId, @privSortId, @currentTime, 3900);

INSERT INTO objects(object_type_id_fk) VALUES (@privTypeId);
SET @CREATE_EDIT_ALERT_DEFINITIONS_PrivId := last_insert_id();
CALL insert_varchar_value(@CREATE_EDIT_ALERT_DEFINITIONS_PrivId, @privNameId, @currentTime, 'CREATE_EDIT_ALERT_DEFINITIONS');
CALL insert_varchar_value(@CREATE_EDIT_ALERT_DEFINITIONS_PrivId, @privDescrId, @currentTime, 'Configure Alert Definitions');
CALL insert_int_value(@CREATE_EDIT_ALERT_DEFINITIONS_PrivId, @privSysId, @currentTime, 6);
CALL insert_varchar_value(@CREATE_EDIT_ALERT_DEFINITIONS_PrivId, @privTagId, @currentTime, 'ALERT_MANAGEMENT');
CALL insert_int_value(@CREATE_EDIT_ALERT_DEFINITIONS_PrivId, @privSortId, @currentTime, 4000);

INSERT INTO objects(object_type_id_fk) VALUES (@privTypeId);
SET @ENABLE_DISABLE_ALERT_DEFINITIONS_PrivId := last_insert_id();
CALL insert_varchar_value(@ENABLE_DISABLE_ALERT_DEFINITIONS_PrivId, @privNameId, @currentTime, 'ENABLE_DISABLE_ALERT_DEFINITIONS');
CALL insert_varchar_value(@ENABLE_DISABLE_ALERT_DEFINITIONS_PrivId, @privDescrId, @currentTime, 'Enable/Disable Alert Definitions');
CALL insert_int_value(@ENABLE_DISABLE_ALERT_DEFINITIONS_PrivId, @privSysId, @currentTime, 6);
CALL insert_varchar_value(@ENABLE_DISABLE_ALERT_DEFINITIONS_PrivId, @privTagId, @currentTime, 'ALERT_MANAGEMENT');
CALL insert_int_value(@ENABLE_DISABLE_ALERT_DEFINITIONS_PrivId, @privSortId, @currentTime, 4100);

INSERT INTO objects(object_type_id_fk) VALUES (@privTypeId);
SET @DELETE_ALERT_DEFINITIONS_PrivId := last_insert_id();
CALL insert_varchar_value(@DELETE_ALERT_DEFINITIONS_PrivId, @privNameId, @currentTime, 'DELETE_ALERT_DEFINITIONS');
CALL insert_varchar_value(@DELETE_ALERT_DEFINITIONS_PrivId, @privDescrId, @currentTime, 'Delete Alert Definitions');
CALL insert_int_value(@DELETE_ALERT_DEFINITIONS_PrivId, @privSysId, @currentTime, 6);
CALL insert_varchar_value(@DELETE_ALERT_DEFINITIONS_PrivId, @privTagId, @currentTime, 'ALERT_MANAGEMENT');
CALL insert_int_value(@DELETE_ALERT_DEFINITIONS_PrivId, @privSortId, @currentTime, 4200);

INSERT INTO objects(object_type_id_fk) VALUES (@privTypeId);
SET @ACCESS_NOTIFICATION_TEMPLATES_PrivId := last_insert_id();
CALL insert_varchar_value(@ACCESS_NOTIFICATION_TEMPLATES_PrivId, @privNameId, @currentTime, 'ACCESS_NOTIFICATION_TEMPLATES');
CALL insert_varchar_value(@ACCESS_NOTIFICATION_TEMPLATES_PrivId, @privDescrId, @currentTime, 'Access Notification Templates');
CALL insert_int_value(@ACCESS_NOTIFICATION_TEMPLATES_PrivId, @privSysId, @currentTime, 6);
CALL insert_varchar_value(@ACCESS_NOTIFICATION_TEMPLATES_PrivId, @privTagId, @currentTime, 'ALERT_MANAGEMENT');
CALL insert_int_value(@ACCESS_NOTIFICATION_TEMPLATES_PrivId, @privSortId, @currentTime, 4300);

INSERT INTO objects(object_type_id_fk) VALUES (@privTypeId);
SET @CREATE_EDIT_NOTIFICATION_TEMPLATES_PrivId := last_insert_id();
CALL insert_varchar_value(@CREATE_EDIT_NOTIFICATION_TEMPLATES_PrivId, @privNameId, @currentTime, 'CREATE_EDIT_NOTIFICATION_TEMPLATES');
CALL insert_varchar_value(@CREATE_EDIT_NOTIFICATION_TEMPLATES_PrivId, @privDescrId, @currentTime, 'Configure Notification Templates');
CALL insert_int_value(@CREATE_EDIT_NOTIFICATION_TEMPLATES_PrivId, @privSysId, @currentTime, 6);
CALL insert_varchar_value(@CREATE_EDIT_NOTIFICATION_TEMPLATES_PrivId, @privTagId, @currentTime, 'ALERT_MANAGEMENT');
CALL insert_int_value(@CREATE_EDIT_NOTIFICATION_TEMPLATES_PrivId, @privSortId, @currentTime, 4400);

INSERT INTO objects(object_type_id_fk) VALUES (@privTypeId);
SET @ACCESS_ALL_ALERTS_PrivId := last_insert_id();
CALL insert_varchar_value(@ACCESS_ALL_ALERTS_PrivId, @privNameId, @currentTime, 'ACCESS_ALL_ALERTS');
CALL insert_varchar_value(@ACCESS_ALL_ALERTS_PrivId, @privDescrId, @currentTime, 'Access All Alerts');
CALL insert_int_value(@ACCESS_ALL_ALERTS_PrivId, @privSysId, @currentTime, 6);
CALL insert_varchar_value(@ACCESS_ALL_ALERTS_PrivId, @privTagId, @currentTime, 'ALERT_MANAGEMENT');
CALL insert_int_value(@ACCESS_ALL_ALERTS_PrivId, @privSortId, @currentTime, 4500);

INSERT INTO objects(object_type_id_fk) VALUES (@privTypeId);
SET @ACKNOWLEDGE_AND_RESOLVE_ALERTS_PrivId := last_insert_id();
CALL insert_varchar_value(@ACKNOWLEDGE_AND_RESOLVE_ALERTS_PrivId, @privNameId, @currentTime, 'ACKNOWLEDGE_AND_RESOLVE_ALERTS');
CALL insert_varchar_value(@ACKNOWLEDGE_AND_RESOLVE_ALERTS_PrivId, @privDescrId, @currentTime, 'Acknowledge and Resolve Alerts');
CALL insert_int_value(@ACKNOWLEDGE_AND_RESOLVE_ALERTS_PrivId, @privSysId, @currentTime, 6);
CALL insert_varchar_value(@ACKNOWLEDGE_AND_RESOLVE_ALERTS_PrivId, @privTagId, @currentTime, 'ALERT_MANAGEMENT');
CALL insert_int_value(@ACKNOWLEDGE_AND_RESOLVE_ALERTS_PrivId, @privSortId, @currentTime, 4600);

INSERT INTO objects(object_type_id_fk) VALUES (@privTypeId);
SET @DISMISS_ALERTS_PrivId := last_insert_id();
CALL insert_varchar_value(@DISMISS_ALERTS_PrivId, @privNameId, @currentTime, 'DISMISS_ALERTS');
CALL insert_varchar_value(@DISMISS_ALERTS_PrivId, @privDescrId, @currentTime, 'Dismiss Alerts');
CALL insert_int_value(@DISMISS_ALERTS_PrivId, @privSysId, @currentTime, 6);
CALL insert_varchar_value(@DISMISS_ALERTS_PrivId, @privTagId, @currentTime, 'ALERT_MANAGEMENT');
CALL insert_int_value(@DISMISS_ALERTS_PrivId, @privSortId, @currentTime, 4700);

INSERT INTO objects(object_type_id_fk) VALUES (@privTypeId);
SET @ACCESS_DELETE_ALL_ALERTS_PrivId := last_insert_id();
CALL insert_varchar_value(@ACCESS_DELETE_ALL_ALERTS_PrivId, @privNameId, @currentTime, 'ACCESS_DELETE_ALL_ALERTS');
CALL insert_varchar_value(@ACCESS_DELETE_ALL_ALERTS_PrivId, @privDescrId, @currentTime, 'Access Delete All Alerts');
CALL insert_int_value(@ACCESS_DELETE_ALL_ALERTS_PrivId, @privSysId, @currentTime, 6);
CALL insert_varchar_value(@ACCESS_DELETE_ALL_ALERTS_PrivId, @privTagId, @currentTime, 'ALERT_MANAGEMENT');
CALL insert_int_value(@ACCESS_DELETE_ALL_ALERTS_PrivId, @privSortId, @currentTime, 4800);

INSERT INTO objects(object_type_id_fk) VALUES (@privTypeId);
SET @ACCESS_DASHBOARDS_PrivId := last_insert_id();
CALL insert_varchar_value(@ACCESS_DASHBOARDS_PrivId, @privNameId, @currentTime, 'ACCESS_DASHBOARDS');
CALL insert_varchar_value(@ACCESS_DASHBOARDS_PrivId, @privDescrId, @currentTime, 'Access Dashboards');
CALL insert_int_value(@ACCESS_DASHBOARDS_PrivId, @privSysId, @currentTime, 6);
CALL insert_varchar_value(@ACCESS_DASHBOARDS_PrivId, @privTagId, @currentTime, 'DASHBOARDS');
CALL insert_int_value(@ACCESS_DASHBOARDS_PrivId, @privSortId, @currentTime, 4900);

INSERT INTO objects(object_type_id_fk) VALUES (@privTypeId);
SET @CREATE_EDIT_DELETE_OWN_PRIVATE_DASHBOARDS_PrivId := last_insert_id();
CALL insert_varchar_value(@CREATE_EDIT_DELETE_OWN_PRIVATE_DASHBOARDS_PrivId, @privNameId, @currentTime, 'CREATE_EDIT_DELETE_OWN_PRIVATE_DASHBOARDS');
CALL insert_varchar_value(@CREATE_EDIT_DELETE_OWN_PRIVATE_DASHBOARDS_PrivId, @privDescrId, @currentTime, 'Configure own private dashboards');
CALL insert_int_value(@CREATE_EDIT_DELETE_OWN_PRIVATE_DASHBOARDS_PrivId, @privSysId, @currentTime, 6);
CALL insert_varchar_value(@CREATE_EDIT_DELETE_OWN_PRIVATE_DASHBOARDS_PrivId, @privTagId, @currentTime, 'DASHBOARDS');
CALL insert_int_value(@CREATE_EDIT_DELETE_OWN_PRIVATE_DASHBOARDS_PrivId, @privSortId, @currentTime, 5000);

INSERT INTO objects(object_type_id_fk) VALUES (@privTypeId);
SET @CREATE_EDIT_DELETE_OWN_PUBLIC_DASHBOARDS_PrivId := last_insert_id();
CALL insert_varchar_value(@CREATE_EDIT_DELETE_OWN_PUBLIC_DASHBOARDS_PrivId, @privNameId, @currentTime, 'CREATE_EDIT_DELETE_OWN_PUBLIC_DASHBOARDS');
CALL insert_varchar_value(@CREATE_EDIT_DELETE_OWN_PUBLIC_DASHBOARDS_PrivId, @privDescrId, @currentTime, 'Configure own public dashboards');
CALL insert_int_value(@CREATE_EDIT_DELETE_OWN_PUBLIC_DASHBOARDS_PrivId, @privSysId, @currentTime, 6);
CALL insert_varchar_value(@CREATE_EDIT_DELETE_OWN_PUBLIC_DASHBOARDS_PrivId, @privTagId, @currentTime, 'DASHBOARDS');
CALL insert_int_value(@CREATE_EDIT_DELETE_OWN_PUBLIC_DASHBOARDS_PrivId, @privSortId, @currentTime, 5100);

INSERT INTO objects(object_type_id_fk) VALUES (@privTypeId);
SET @CREATE_EDIT_DELETE_ALL_DASHBOARDS_PrivId := last_insert_id();
CALL insert_varchar_value(@CREATE_EDIT_DELETE_ALL_DASHBOARDS_PrivId, @privNameId, @currentTime, 'CREATE_EDIT_DELETE_ALL_DASHBOARDS');
CALL insert_varchar_value(@CREATE_EDIT_DELETE_ALL_DASHBOARDS_PrivId, @privDescrId, @currentTime, 'Configure all dashboards');
CALL insert_int_value(@CREATE_EDIT_DELETE_ALL_DASHBOARDS_PrivId, @privSysId, @currentTime, 6);
CALL insert_varchar_value(@CREATE_EDIT_DELETE_ALL_DASHBOARDS_PrivId, @privTagId, @currentTime, 'DASHBOARDS');
CALL insert_int_value(@CREATE_EDIT_DELETE_ALL_DASHBOARDS_PrivId, @privSortId, @currentTime, 5200);

INSERT INTO objects(object_type_id_fk) VALUES (@privTypeId);
SET @ACCESS_FLOORPLAN_PrivId := last_insert_id();
CALL insert_varchar_value(@ACCESS_FLOORPLAN_PrivId, @privNameId, @currentTime, 'ACCESS_FLOORPLAN');
CALL insert_varchar_value(@ACCESS_FLOORPLAN_PrivId, @privDescrId, @currentTime, 'Access Floorplan');
CALL insert_int_value(@ACCESS_FLOORPLAN_PrivId, @privSysId, @currentTime, 6);
CALL insert_varchar_value(@ACCESS_FLOORPLAN_PrivId, @privTagId, @currentTime, 'FLOORPLAN');
CALL insert_int_value(@ACCESS_FLOORPLAN_PrivId, @privSortId, @currentTime, 5300);

INSERT INTO objects(object_type_id_fk) VALUES (@privTypeId);
SET @ACCESS_WSN_NETWORK_FLOORPLAN_PrivId := last_insert_id();
CALL insert_varchar_value(@ACCESS_WSN_NETWORK_FLOORPLAN_PrivId, @privNameId, @currentTime, 'ACCESS_WSN_NETWORK_FLOORPLAN');
CALL insert_varchar_value(@ACCESS_WSN_NETWORK_FLOORPLAN_PrivId, @privDescrId, @currentTime, 'Access WSN Network floorplan');
CALL insert_int_value(@ACCESS_WSN_NETWORK_FLOORPLAN_PrivId, @privSysId, @currentTime, 6);
CALL insert_varchar_value(@ACCESS_WSN_NETWORK_FLOORPLAN_PrivId, @privTagId, @currentTime, 'FLOORPLAN');
CALL insert_int_value(@ACCESS_WSN_NETWORK_FLOORPLAN_PrivId, @privSortId, @currentTime, 5400);

INSERT INTO objects(object_type_id_fk) VALUES (@privTypeId);
SET @SEND_WSN_COMMANDS_TO_NODES_PrivId := last_insert_id();
CALL insert_varchar_value(@SEND_WSN_COMMANDS_TO_NODES_PrivId, @privNameId, @currentTime, 'SEND_WSN_COMMANDS_TO_NODES');
CALL insert_varchar_value(@SEND_WSN_COMMANDS_TO_NODES_PrivId, @privDescrId, @currentTime, 'Send WSN commands to nodes');
CALL insert_int_value(@SEND_WSN_COMMANDS_TO_NODES_PrivId, @privSysId, @currentTime, 6);
CALL insert_varchar_value(@SEND_WSN_COMMANDS_TO_NODES_PrivId, @privTagId, @currentTime, 'FLOORPLAN');
CALL insert_int_value(@SEND_WSN_COMMANDS_TO_NODES_PrivId, @privSortId, @currentTime, 5500);

INSERT INTO objects(object_type_id_fk) VALUES (@privTypeId);
SET @ACCESS_ACTIVE_CONTROL_DATA_PrivId := last_insert_id();
CALL insert_varchar_value(@ACCESS_ACTIVE_CONTROL_DATA_PrivId, @privNameId, @currentTime, 'ACCESS_ACTIVE_CONTROL_DATA');
CALL insert_varchar_value(@ACCESS_ACTIVE_CONTROL_DATA_PrivId, @privDescrId, @currentTime, 'Access active control performance data');
CALL insert_int_value(@ACCESS_ACTIVE_CONTROL_DATA_PrivId, @privSysId, @currentTime, 6);
CALL insert_varchar_value(@ACCESS_ACTIVE_CONTROL_DATA_PrivId, @privTagId, @currentTime, 'FLOORPLAN');
CALL insert_int_value(@ACCESS_ACTIVE_CONTROL_DATA_PrivId, @privSortId, @currentTime, 5600);

INSERT INTO objects(object_type_id_fk) VALUES (@privTypeId);
SET @EDIT_ACTIVE_CONTROL_OPERATIONAL_PARAMS_PrivId := last_insert_id();
CALL insert_varchar_value(@EDIT_ACTIVE_CONTROL_OPERATIONAL_PARAMS_PrivId, @privNameId, @currentTime, 'EDIT_ACTIVE_CONTROL_OPERATIONAL_PARAMS');
CALL insert_varchar_value(@EDIT_ACTIVE_CONTROL_OPERATIONAL_PARAMS_PrivId, @privDescrId, @currentTime, 'Edit active control operational parameters');
CALL insert_int_value(@EDIT_ACTIVE_CONTROL_OPERATIONAL_PARAMS_PrivId, @privSysId, @currentTime, 6);
CALL insert_varchar_value(@EDIT_ACTIVE_CONTROL_OPERATIONAL_PARAMS_PrivId, @privTagId, @currentTime, 'FLOORPLAN');
CALL insert_int_value(@EDIT_ACTIVE_CONTROL_OPERATIONAL_PARAMS_PrivId, @privSortId, @currentTime, 5800);

INSERT INTO objects(object_type_id_fk) VALUES (@privTypeId);
SET @EDIT_ACTIVE_CONTROL_TARGET_VALUES_PrivId := last_insert_id();
CALL insert_varchar_value(@EDIT_ACTIVE_CONTROL_TARGET_VALUES_PrivId, @privNameId, @currentTime, 'EDIT_ACTIVE_CONTROL_TARGET_VALUES');
CALL insert_varchar_value(@EDIT_ACTIVE_CONTROL_TARGET_VALUES_PrivId, @privDescrId, @currentTime, 'Edit active control target values');
CALL insert_int_value(@EDIT_ACTIVE_CONTROL_TARGET_VALUES_PrivId, @privSysId, @currentTime, 6);
CALL insert_varchar_value(@EDIT_ACTIVE_CONTROL_TARGET_VALUES_PrivId, @privTagId, @currentTime, 'FLOORPLAN');
CALL insert_int_value(@EDIT_ACTIVE_CONTROL_TARGET_VALUES_PrivId, @privSortId, @currentTime, 5900);

INSERT INTO objects(object_type_id_fk) VALUES (@privTypeId);
SET @ACCESS_ENVIRONMENTALS_FLOORPLAN_PrivId := last_insert_id();
CALL insert_varchar_value(@ACCESS_ENVIRONMENTALS_FLOORPLAN_PrivId, @privNameId, @currentTime, 'ACCESS_ENVIRONMENTALS_FLOORPLAN');
CALL insert_varchar_value(@ACCESS_ENVIRONMENTALS_FLOORPLAN_PrivId, @privDescrId, @currentTime, 'Access Environmentals floorplan');
CALL insert_int_value(@ACCESS_ENVIRONMENTALS_FLOORPLAN_PrivId, @privSysId, @currentTime, 6);
CALL insert_varchar_value(@ACCESS_ENVIRONMENTALS_FLOORPLAN_PrivId, @privTagId, @currentTime, 'FLOORPLAN');
CALL insert_int_value(@ACCESS_ENVIRONMENTALS_FLOORPLAN_PrivId, @privSortId, @currentTime, 6300);

INSERT INTO objects(object_type_id_fk) VALUES (@privTypeId);
SET @MANAGE_NODES_PrivId := last_insert_id();
CALL insert_varchar_value(@MANAGE_NODES_PrivId, @privNameId, @currentTime, 'MANAGE_NODES');
CALL insert_varchar_value(@MANAGE_NODES_PrivId, @privDescrId, @currentTime, 'Manage Nodes');
CALL insert_int_value(@MANAGE_NODES_PrivId, @privSysId, @currentTime, 6);
CALL insert_varchar_value(@MANAGE_NODES_PrivId, @privTagId, @currentTime, 'FLOORPLAN');
CALL insert_int_value(@MANAGE_NODES_PrivId, @privSortId, @currentTime, 6400);

INSERT INTO objects(object_type_id_fk) VALUES (@privTypeId);
SET @ENABLE_DISABLE_OBJECTS_PrivId := last_insert_id();
CALL insert_varchar_value(@ENABLE_DISABLE_OBJECTS_PrivId, @privNameId, @currentTime, 'ENABLE_DISABLE_OBJECTS');
CALL insert_varchar_value(@ENABLE_DISABLE_OBJECTS_PrivId, @privDescrId, @currentTime, 'Enable/Disable Objects');
CALL insert_int_value(@ENABLE_DISABLE_OBJECTS_PrivId, @privSysId, @currentTime, 6);
CALL insert_varchar_value(@ENABLE_DISABLE_OBJECTS_PrivId, @privTagId, @currentTime, 'FLOORPLAN');
CALL insert_int_value(@ENABLE_DISABLE_OBJECTS_PrivId, @privSortId, @currentTime, 6500);

INSERT INTO objects(object_type_id_fk) VALUES (@privTypeId);
SET @ACCESS_POWERIMAGING_FLOORPLAN_PrivId := last_insert_id();
CALL insert_varchar_value(@ACCESS_POWERIMAGING_FLOORPLAN_PrivId, @privNameId, @currentTime, 'ACCESS_POWERIMAGING_FLOORPLAN');
CALL insert_varchar_value(@ACCESS_POWERIMAGING_FLOORPLAN_PrivId, @privDescrId, @currentTime, 'Access PowerImaging floorplan');
CALL insert_int_value(@ACCESS_POWERIMAGING_FLOORPLAN_PrivId, @privSysId, @currentTime, 6);
CALL insert_varchar_value(@ACCESS_POWERIMAGING_FLOORPLAN_PrivId, @privTagId, @currentTime, 'FLOORPLAN');
CALL insert_int_value(@ACCESS_POWERIMAGING_FLOORPLAN_PrivId, @privSortId, @currentTime, 6600);

INSERT INTO objects(object_type_id_fk) VALUES (@privTypeId);
SET @EDIT_POWERIMAGING_PARAMETERS_PrivId := last_insert_id();
CALL insert_varchar_value(@EDIT_POWERIMAGING_PARAMETERS_PrivId, @privNameId, @currentTime, 'EDIT_POWERIMAGING_PARAMETERS');
CALL insert_varchar_value(@EDIT_POWERIMAGING_PARAMETERS_PrivId, @privDescrId, @currentTime, 'Edit PowerImaging parameters');
CALL insert_int_value(@EDIT_POWERIMAGING_PARAMETERS_PrivId, @privSysId, @currentTime, 6);
CALL insert_varchar_value(@EDIT_POWERIMAGING_PARAMETERS_PrivId, @privTagId, @currentTime, 'FLOORPLAN');
CALL insert_int_value(@EDIT_POWERIMAGING_PARAMETERS_PrivId, @privSortId, @currentTime, 6700);

INSERT INTO objects(object_type_id_fk) VALUES (@privTypeId);
SET @ENABLE_LIVEIMAGING_LAYER_PrivId := last_insert_id();
CALL insert_varchar_value(@ENABLE_LIVEIMAGING_LAYER_PrivId, @privNameId, @currentTime, 'ENABLE_LIVEIMAGING_LAYER');
CALL insert_varchar_value(@ENABLE_LIVEIMAGING_LAYER_PrivId, @privDescrId, @currentTime, 'Enable LiveImaging layer');
CALL insert_int_value(@ENABLE_LIVEIMAGING_LAYER_PrivId, @privSysId, @currentTime, 6);
CALL insert_varchar_value(@ENABLE_LIVEIMAGING_LAYER_PrivId, @privTagId, @currentTime, 'FLOORPLAN');
CALL insert_int_value(@ENABLE_LIVEIMAGING_LAYER_PrivId, @privSortId, @currentTime, 6800);

INSERT INTO objects(object_type_id_fk) VALUES (@privTypeId);
SET @ACCESS_LIVEIMAGING_ADVANCED_PrivId := last_insert_id();
CALL insert_varchar_value(@ACCESS_LIVEIMAGING_ADVANCED_PrivId, @privNameId, @currentTime, 'ACCESS_LIVEIMAGING_ADVANCED');
CALL insert_varchar_value(@ACCESS_LIVEIMAGING_ADVANCED_PrivId, @privDescrId, @currentTime, 'Access LiveImaging Advanced');
CALL insert_int_value(@ACCESS_LIVEIMAGING_ADVANCED_PrivId, @privSysId, @currentTime, 6);
CALL insert_varchar_value(@ACCESS_LIVEIMAGING_ADVANCED_PrivId, @privTagId, @currentTime, 'FLOORPLAN');
CALL insert_int_value(@ACCESS_LIVEIMAGING_ADVANCED_PrivId, @privSortId, @currentTime, 6900);

INSERT INTO objects(object_type_id_fk) VALUES (@privTypeId);
SET @PRINT_EXPORT_LIVEIMAGING_IMAGES_PrivId := last_insert_id();
CALL insert_varchar_value(@PRINT_EXPORT_LIVEIMAGING_IMAGES_PrivId, @privNameId, @currentTime, 'PRINT_EXPORT_LIVEIMAGING_IMAGES');
CALL insert_varchar_value(@PRINT_EXPORT_LIVEIMAGING_IMAGES_PrivId, @privDescrId, @currentTime, 'Print/Export LiveImaging images');
CALL insert_int_value(@PRINT_EXPORT_LIVEIMAGING_IMAGES_PrivId, @privSysId, @currentTime, 6);
CALL insert_varchar_value(@PRINT_EXPORT_LIVEIMAGING_IMAGES_PrivId, @privTagId, @currentTime, 'FLOORPLAN');
CALL insert_int_value(@PRINT_EXPORT_LIVEIMAGING_IMAGES_PrivId, @privSortId, @currentTime, 7000);

INSERT INTO objects(object_type_id_fk) VALUES (@privTypeId);
SET @ACCESS_PUE_SUMMARY_PrivId := last_insert_id();
CALL insert_varchar_value(@ACCESS_PUE_SUMMARY_PrivId, @privNameId, @currentTime, 'ACCESS_PUE_SUMMARY');
CALL insert_varchar_value(@ACCESS_PUE_SUMMARY_PrivId, @privDescrId, @currentTime, 'Access PUE Summary');
CALL insert_int_value(@ACCESS_PUE_SUMMARY_PrivId, @privSysId, @currentTime, 6);
CALL insert_varchar_value(@ACCESS_PUE_SUMMARY_PrivId, @privTagId, @currentTime, 'DATA_MONITORING');
CALL insert_int_value(@ACCESS_PUE_SUMMARY_PrivId, @privSortId, @currentTime, 7100);

INSERT INTO objects(object_type_id_fk) VALUES (@privTypeId);
SET @ACCESS_PREDEFINED_METRICS_PrivId := last_insert_id();
CALL insert_varchar_value(@ACCESS_PREDEFINED_METRICS_PrivId, @privNameId, @currentTime, 'ACCESS_PREDEFINED_METRICS');
CALL insert_varchar_value(@ACCESS_PREDEFINED_METRICS_PrivId, @privDescrId, @currentTime, 'Access Pre-Defined Metrics');
CALL insert_int_value(@ACCESS_PREDEFINED_METRICS_PrivId, @privSysId, @currentTime, 6);
CALL insert_varchar_value(@ACCESS_PREDEFINED_METRICS_PrivId, @privTagId, @currentTime, 'DATA_MONITORING');
CALL insert_int_value(@ACCESS_PREDEFINED_METRICS_PrivId, @privSortId, @currentTime, 7200);

INSERT INTO objects(object_type_id_fk) VALUES (@privTypeId);
SET @ACCESS_CUSTOM_METRICS_PrivId := last_insert_id();
CALL insert_varchar_value(@ACCESS_CUSTOM_METRICS_PrivId, @privNameId, @currentTime, 'ACCESS_CUSTOM_METRICS');
CALL insert_varchar_value(@ACCESS_CUSTOM_METRICS_PrivId, @privDescrId, @currentTime, 'Access Custom Metrics');
CALL insert_int_value(@ACCESS_CUSTOM_METRICS_PrivId, @privSysId, @currentTime, 6);
CALL insert_varchar_value(@ACCESS_CUSTOM_METRICS_PrivId, @privTagId, @currentTime, 'DATA_MONITORING');
CALL insert_int_value(@ACCESS_CUSTOM_METRICS_PrivId, @privSortId, @currentTime, 7300);

INSERT INTO objects(object_type_id_fk) VALUES (@privTypeId);
SET @ACCESS_PDU_VIEW_PrivId := last_insert_id();
CALL insert_varchar_value(@ACCESS_PDU_VIEW_PrivId, @privNameId, @currentTime, 'ACCESS_PDU_VIEW');
CALL insert_varchar_value(@ACCESS_PDU_VIEW_PrivId, @privDescrId, @currentTime, 'Access PDU view');
CALL insert_int_value(@ACCESS_PDU_VIEW_PrivId, @privSysId, @currentTime, 6);
CALL insert_varchar_value(@ACCESS_PDU_VIEW_PrivId, @privTagId, @currentTime, 'DATA_MONITORING');
CALL insert_int_value(@ACCESS_PDU_VIEW_PrivId, @privSortId, @currentTime, 7400);

INSERT INTO objects(object_type_id_fk) VALUES (@privTypeId);
SET @ACCESS_BCMS_VIEW_PrivId := last_insert_id();
CALL insert_varchar_value(@ACCESS_BCMS_VIEW_PrivId, @privNameId, @currentTime, 'ACCESS_BCMS_VIEW');
CALL insert_varchar_value(@ACCESS_BCMS_VIEW_PrivId, @privDescrId, @currentTime, 'Access BCMS view');
CALL insert_int_value(@ACCESS_BCMS_VIEW_PrivId, @privSysId, @currentTime, 6);
CALL insert_varchar_value(@ACCESS_BCMS_VIEW_PrivId, @privTagId, @currentTime, 'DATA_MONITORING');
CALL insert_int_value(@ACCESS_BCMS_VIEW_PrivId, @privSortId, @currentTime, 7500);

INSERT INTO objects(object_type_id_fk) VALUES (@privTypeId);
SET @EDIT_BCMS_CIRCUIT_NAMES_PrivId := last_insert_id();
CALL insert_varchar_value(@EDIT_BCMS_CIRCUIT_NAMES_PrivId, @privNameId, @currentTime, 'EDIT_BCMS_CIRCUIT_NAMES');
CALL insert_varchar_value(@EDIT_BCMS_CIRCUIT_NAMES_PrivId, @privDescrId, @currentTime, 'Edit BCMS circuit names');
CALL insert_int_value(@EDIT_BCMS_CIRCUIT_NAMES_PrivId, @privSysId, @currentTime, 6);
CALL insert_varchar_value(@EDIT_BCMS_CIRCUIT_NAMES_PrivId, @privTagId, @currentTime, 'DATA_MONITORING');
CALL insert_int_value(@EDIT_BCMS_CIRCUIT_NAMES_PrivId, @privSortId, @currentTime, 7600);

INSERT INTO objects(object_type_id_fk) VALUES (@privTypeId);
SET @ACCESS_DATA_VIEWS_PrivId := last_insert_id();
CALL insert_varchar_value(@ACCESS_DATA_VIEWS_PrivId, @privNameId, @currentTime, 'ACCESS_DATA_VIEWS');
CALL insert_varchar_value(@ACCESS_DATA_VIEWS_PrivId, @privDescrId, @currentTime, 'Access Data Views');
CALL insert_int_value(@ACCESS_DATA_VIEWS_PrivId, @privSysId, @currentTime, 6);
CALL insert_varchar_value(@ACCESS_DATA_VIEWS_PrivId, @privTagId, @currentTime, 'DATA_MONITORING');
CALL insert_int_value(@ACCESS_DATA_VIEWS_PrivId, @privSortId, @currentTime, 7700);

INSERT INTO objects(object_type_id_fk) VALUES (@privTypeId);
SET @CREATE_EDIT_DELETE_OWN_PRIVATE_TEMPLATES_PrivId := last_insert_id();
CALL insert_varchar_value(@CREATE_EDIT_DELETE_OWN_PRIVATE_TEMPLATES_PrivId, @privNameId, @currentTime, 'CREATE_EDIT_DELETE_OWN_PRIVATE_TEMPLATES');
CALL insert_varchar_value(@CREATE_EDIT_DELETE_OWN_PRIVATE_TEMPLATES_PrivId, @privDescrId, @currentTime, 'Configure own private templates');
CALL insert_int_value(@CREATE_EDIT_DELETE_OWN_PRIVATE_TEMPLATES_PrivId, @privSysId, @currentTime, 6);
CALL insert_varchar_value(@CREATE_EDIT_DELETE_OWN_PRIVATE_TEMPLATES_PrivId, @privTagId, @currentTime, 'DATA_MONITORING');
CALL insert_int_value(@CREATE_EDIT_DELETE_OWN_PRIVATE_TEMPLATES_PrivId, @privSortId, @currentTime, 7800);

INSERT INTO objects(object_type_id_fk) VALUES (@privTypeId);
SET @CREATE_EDIT_DELETE_OWN_PUBLIC_TEMPLATES_PrivId := last_insert_id();
CALL insert_varchar_value(@CREATE_EDIT_DELETE_OWN_PUBLIC_TEMPLATES_PrivId, @privNameId, @currentTime, 'CREATE_EDIT_DELETE_OWN_PUBLIC_TEMPLATES');
CALL insert_varchar_value(@CREATE_EDIT_DELETE_OWN_PUBLIC_TEMPLATES_PrivId, @privDescrId, @currentTime, 'Configure own public templates');
CALL insert_int_value(@CREATE_EDIT_DELETE_OWN_PUBLIC_TEMPLATES_PrivId, @privSysId, @currentTime, 6);
CALL insert_varchar_value(@CREATE_EDIT_DELETE_OWN_PUBLIC_TEMPLATES_PrivId, @privTagId, @currentTime, 'DATA_MONITORING');
CALL insert_int_value(@CREATE_EDIT_DELETE_OWN_PUBLIC_TEMPLATES_PrivId, @privSortId, @currentTime, 7900);

INSERT INTO objects(object_type_id_fk) VALUES (@privTypeId);
SET @CREATE_EDIT_DELETE_ALL_TEMPLATES_PrivId := last_insert_id();
CALL insert_varchar_value(@CREATE_EDIT_DELETE_ALL_TEMPLATES_PrivId, @privNameId, @currentTime, 'CREATE_EDIT_DELETE_ALL_TEMPLATES');
CALL insert_varchar_value(@CREATE_EDIT_DELETE_ALL_TEMPLATES_PrivId, @privDescrId, @currentTime, 'Configure all templates');
CALL insert_int_value(@CREATE_EDIT_DELETE_ALL_TEMPLATES_PrivId, @privSysId, @currentTime, 6);
CALL insert_varchar_value(@CREATE_EDIT_DELETE_ALL_TEMPLATES_PrivId, @privTagId, @currentTime, 'DATA_MONITORING');
CALL insert_int_value(@CREATE_EDIT_DELETE_ALL_TEMPLATES_PrivId, @privSortId, @currentTime, 8000);

INSERT INTO objects(object_type_id_fk) VALUES (@privTypeId);
SET @RESTORE_DEFAULT_TEMPLATES_PrivId := last_insert_id();
CALL insert_varchar_value(@RESTORE_DEFAULT_TEMPLATES_PrivId, @privNameId, @currentTime, 'RESTORE_DEFAULT_TEMPLATES');
CALL insert_varchar_value(@RESTORE_DEFAULT_TEMPLATES_PrivId, @privDescrId, @currentTime, 'Restore default templates');
CALL insert_int_value(@RESTORE_DEFAULT_TEMPLATES_PrivId, @privSysId, @currentTime, 6);
CALL insert_varchar_value(@RESTORE_DEFAULT_TEMPLATES_PrivId, @privTagId, @currentTime, 'DATA_MONITORING');
CALL insert_int_value(@RESTORE_DEFAULT_TEMPLATES_PrivId, @privSortId, @currentTime, 8100);

INSERT INTO objects(object_type_id_fk) VALUES (@privTypeId);
SET @PRINT_EXPORT_DATA_VIEWS_PrivId := last_insert_id();
CALL insert_varchar_value(@PRINT_EXPORT_DATA_VIEWS_PrivId, @privNameId, @currentTime, 'PRINT_EXPORT_DATA_VIEWS');
CALL insert_varchar_value(@PRINT_EXPORT_DATA_VIEWS_PrivId, @privDescrId, @currentTime, 'Print/export data views');
CALL insert_int_value(@PRINT_EXPORT_DATA_VIEWS_PrivId, @privSysId, @currentTime, 6);
CALL insert_varchar_value(@PRINT_EXPORT_DATA_VIEWS_PrivId, @privTagId, @currentTime, 'DATA_MONITORING');
CALL insert_int_value(@PRINT_EXPORT_DATA_VIEWS_PrivId, @privSortId, @currentTime, 8200);

INSERT INTO objects(object_type_id_fk) VALUES (@privTypeId);
SET @ACCESS_WEBCONSOLE_PrivId := last_insert_id();
CALL insert_varchar_value(@ACCESS_WEBCONSOLE_PrivId, @privNameId, @currentTime, 'ACCESS_WEBCONSOLE');
CALL insert_varchar_value(@ACCESS_WEBCONSOLE_PrivId, @privDescrId, @currentTime, 'Access Web Console');
CALL insert_int_value(@ACCESS_WEBCONSOLE_PrivId, @privSysId, @currentTime, 6);
CALL insert_varchar_value(@ACCESS_WEBCONSOLE_PrivId, @privTagId, @currentTime, 'SYSTEM_CONFIGURATION');
CALL insert_int_value(@ACCESS_WEBCONSOLE_PrivId, @privSortId, @currentTime, 50);

INSERT INTO objects(object_type_id_fk) VALUES (@privTypeId);
SET @SCHEDULE_CUSTOM_QUERIES_PrivId := last_insert_id();
CALL insert_varchar_value(@SCHEDULE_CUSTOM_QUERIES_PrivId, @privNameId, @currentTime, 'SCHEDULE_CUSTOM_QUERIES');
CALL insert_varchar_value(@SCHEDULE_CUSTOM_QUERIES_PrivId, @privDescrId, @currentTime, 'Schedule Custom Queries');
CALL insert_int_value(@SCHEDULE_CUSTOM_QUERIES_PrivId, @privSysId, @currentTime, 2);
CALL insert_varchar_value(@SCHEDULE_CUSTOM_QUERIES_PrivId, @privTagId, @currentTime, 'REPORT_MANAGEMENT');
CALL insert_int_value(@SCHEDULE_CUSTOM_QUERIES_PrivId, @privSortId, @currentTime, 3801);

INSERT INTO objects(object_type_id_fk) VALUES (@privTypeId);
SET @MANAGE_REPORT_TEMPLATES_PrivId := last_insert_id();
CALL insert_varchar_value(@MANAGE_REPORT_TEMPLATES_PrivId, @privNameId, @currentTime, 'MANAGE_REPORT_TEMPLATES');
CALL insert_varchar_value(@MANAGE_REPORT_TEMPLATES_PrivId, @privDescrId, @currentTime, 'Manage Report Templates');
CALL insert_int_value(@MANAGE_REPORT_TEMPLATES_PrivId, @privSysId, @currentTime, 2);
CALL insert_varchar_value(@MANAGE_REPORT_TEMPLATES_PrivId, @privTagId, @currentTime, 'REPORT_MANAGEMENT');
CALL insert_int_value(@MANAGE_REPORT_TEMPLATES_PrivId, @privSortId, @currentTime, 3802);

INSERT INTO objects(object_type_id_fk) VALUES (@privTypeId);
SET @VIEW_REPORTS_PrivId := last_insert_id();
CALL insert_varchar_value(@VIEW_REPORTS_PrivId, @privNameId, @currentTime, 'VIEW_REPORTS');
CALL insert_varchar_value(@VIEW_REPORTS_PrivId, @privDescrId, @currentTime, 'View Reports');
CALL insert_int_value(@VIEW_REPORTS_PrivId, @privSysId, @currentTime, 2);
CALL insert_varchar_value(@VIEW_REPORTS_PrivId, @privTagId, @currentTime, 'REPORT_MANAGEMENT');
CALL insert_int_value(@VIEW_REPORTS_PrivId, @privSortId, @currentTime, 3803);

INSERT INTO objects(object_type_id_fk) VALUES (@privTypeId);
SET @DELETE_REPORTS_PrivId := last_insert_id();
CALL insert_varchar_value(@DELETE_REPORTS_PrivId, @privNameId, @currentTime, 'DELETE_REPORTS');
CALL insert_varchar_value(@DELETE_REPORTS_PrivId, @privDescrId, @currentTime, 'Delete Reports');
CALL insert_int_value(@DELETE_REPORTS_PrivId, @privSysId, @currentTime, 2);
CALL insert_varchar_value(@DELETE_REPORTS_PrivId, @privTagId, @currentTime, 'REPORT_MANAGEMENT');
CALL insert_int_value(@DELETE_REPORTS_PrivId, @privSortId, @currentTime, 3804);

-- create relations system privilege - privilege

INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@edit_data_PrivId, @ACCESS_PERMISSIONS_PrivId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@edit_user_data_PrivId, @ADD_REMOVE_USER_PERMISSIONS_PrivId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@edit_data_PrivId, @EDIT_DATA_CENTER_PROPERTIES_GENERAL_PrivId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@edit_data_PrivId, @ACCESS_MANAGE_USERS_PrivId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@edit_user_data_PrivId, @ACCESS_MANAGE_USERS_PrivId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@edit_user_data_PrivId, @CREATE_EDIT_USERS_PrivId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@edit_data_PrivId, @ACCESS_USER_GROUPS_PrivId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@edit_user_data_PrivId, @ACCESS_USER_GROUPS_PrivId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@edit_data_PrivId, @CREATE_EDIT_ASSET_GROUPS_PrivId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@edit_meta_data_PrivId, @CREATE_EDIT_ASSET_GROUPS_PrivId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@edit_data_PrivId, @EDIT_CONFIGURATION_SETTINGS_PrivId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@write_file_PrivId, @EDIT_CONFIGURATION_SETTINGS_PrivId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@edit_data_PrivId, @ACCESS_RECONCILE_ASSET_NAMES_PrivId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@get_data_PrivId, @EXPORT_FROM_MAPSENSE_PrivId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@edit_data_PrivId, @EXPORT_FROM_MAPSENSE_PrivId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@edit_meta_data_PrivId, @EXPORT_FROM_MAPSENSE_PrivId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@edit_data_PrivId, @CREATE_EDIT_DELETE_OWN_PRIVATE_QUERIES_PrivId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@edit_meta_data_PrivId, @CREATE_EDIT_DELETE_OWN_PRIVATE_QUERIES_PrivId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@edit_data_PrivId, @CREATE_EDIT_DELETE_OWN_PUBLIC_QUERIES_PrivId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@edit_meta_data_PrivId, @CREATE_EDIT_DELETE_OWN_PUBLIC_QUERIES_PrivId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@edit_data_PrivId, @CREATE_EDIT_DELETE_ALL_QUERIES_PrivId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@edit_data_PrivId, @EDIT_CARBON_EMISSIONS_TARGET_VALUES_PrivId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@edit_data_PrivId, @CREATE_EDIT_ALERT_DEFINITIONS_PrivId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@edit_data_PrivId, @ENABLE_DISABLE_ALERT_DEFINITIONS_PrivId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@edit_data_PrivId, @DELETE_ALERT_DEFINITIONS_PrivId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@edit_data_PrivId, @CREATE_EDIT_NOTIFICATION_TEMPLATES_PrivId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@edit_data_PrivId, @ACKNOWLEDGE_AND_RESOLVE_ALERTS_PrivId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@edit_data_PrivId, @DISMISS_ALERTS_PrivId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@edit_data_PrivId, @ACCESS_DELETE_ALL_ALERTS_PrivId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@edit_data_PrivId, @CREATE_EDIT_DELETE_OWN_PRIVATE_DASHBOARDS_PrivId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@edit_data_PrivId, @CREATE_EDIT_DELETE_OWN_PUBLIC_DASHBOARDS_PrivId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@edit_data_PrivId, @CREATE_EDIT_DELETE_ALL_DASHBOARDS_PrivId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@edit_data_PrivId, @SEND_WSN_COMMANDS_TO_NODES_PrivId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@edit_data_PrivId, @EDIT_ACTIVE_CONTROL_OPERATIONAL_PARAMS_PrivId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@edit_data_PrivId, @EDIT_ACTIVE_CONTROL_TARGET_VALUES_PrivId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@edit_data_PrivId, @MANAGE_NODES_PrivId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@edit_data_PrivId, @ENABLE_DISABLE_OBJECTS_PrivId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@edit_data_PrivId, @EDIT_POWERIMAGING_PARAMETERS_PrivId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@edit_data_PrivId, @EDIT_BCMS_CIRCUIT_NAMES_PrivId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@edit_data_PrivId, @CREATE_EDIT_DELETE_OWN_PRIVATE_TEMPLATES_PrivId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@edit_data_PrivId, @CREATE_EDIT_DELETE_OWN_PUBLIC_TEMPLATES_PrivId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@edit_data_PrivId, @CREATE_EDIT_DELETE_ALL_TEMPLATES_PrivId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@edit_data_PrivId, @RESTORE_DEFAULT_TEMPLATES_PrivId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@get_data_PrivId, @ACCESS_WEBCONSOLE_PrivId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@get_user_information_PrivId, @ACCESS_WEBCONSOLE_PrivId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@get_user_data_PrivId, @ACCESS_WEBCONSOLE_PrivId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@edit_user_preferences_PrivId, @ACCESS_WEBCONSOLE_PrivId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@read_file_PrivId, @ACCESS_WEBCONSOLE_PrivId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@edit_data_PrivId, @SCHEDULE_CUSTOM_QUERIES_PrivId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@get_user_information_PrivId, @SCHEDULE_CUSTOM_QUERIES_PrivId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@get_user_data_PrivId, @SCHEDULE_CUSTOM_QUERIES_PrivId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@write_file_PrivId, @SCHEDULE_CUSTOM_QUERIES_PrivId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@edit_data_PrivId, @MANAGE_REPORT_TEMPLATES_PrivId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@get_user_information_PrivId, @MANAGE_REPORT_TEMPLATES_PrivId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@get_user_data_PrivId, @MANAGE_REPORT_TEMPLATES_PrivId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@write_file_PrivId, @MANAGE_REPORT_TEMPLATES_PrivId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@read_file_PrivId, @VIEW_REPORTS_PrivId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@edit_data_PrivId, @DELETE_REPORTS_PrivId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@write_file_PrivId, @DELETE_REPORTS_PrivId);

-- create relations privilege - group

INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@root_permission_PrivId, @Administrators_GroupId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@VIEW_DATA_CENTER_PROPERTIES_PrivId, @Power_GroupId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@VIEW_DATA_CENTER_PROPERTIES_PrivId, @Standard_GroupId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@EDIT_DATA_CENTER_PROPERTIES_GENERAL_PrivId, @Power_GroupId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@GENERATE_DIAGNOSTIC_SUPPORT_PACKAGE_PrivId, @Power_GroupId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@GENERATE_DIAGNOSTIC_SUPPORT_PACKAGE_PrivId, @Standard_GroupId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@ACCESS_DATA_ANALYSIS_PrivId, @Power_GroupId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@ACCESS_DATA_ANALYSIS_PrivId, @Standard_GroupId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@PRINT_EXPORT_QUERY_RESULTS_PrivId, @Power_GroupId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@PRINT_EXPORT_QUERY_RESULTS_PrivId, @Standard_GroupId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@PRINT_EXPORT_CHARTS_PrivId, @Power_GroupId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@PRINT_EXPORT_CHARTS_PrivId, @Standard_GroupId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@ADD_ANNOTATIONS_PrivId, @Power_GroupId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@ADD_SYSTEM_ANNOTATIONS_PrivId, @Power_GroupId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@ACCESS_CUSTOM_QUERIES_PrivId, @Power_GroupId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@ACCESS_CUSTOM_QUERIES_PrivId, @Standard_GroupId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@CREATE_EDIT_DELETE_OWN_PRIVATE_QUERIES_PrivId, @Power_GroupId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@CREATE_EDIT_DELETE_OWN_PRIVATE_QUERIES_PrivId, @Standard_GroupId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@CREATE_EDIT_DELETE_OWN_PUBLIC_QUERIES_PrivId, @Power_GroupId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@ACCESS_FIND_AVAILABLE_CAPACITY_PrivId, @Power_GroupId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@ACCESS_FIND_AVAILABLE_CAPACITY_PrivId, @Standard_GroupId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@ACCESS_CARBON_EMISSIONS_SUMMARY_PrivId, @Power_GroupId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@ACCESS_CARBON_EMISSIONS_SUMMARY_PrivId, @Standard_GroupId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@EDIT_CARBON_EMISSIONS_TARGET_VALUES_PrivId, @Power_GroupId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@PRINT_CARBON_EMISSIONS_SUMMARY_PrivId, @Power_GroupId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@PRINT_CARBON_EMISSIONS_SUMMARY_PrivId, @Standard_GroupId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@ACCESS_NETWORK_STATISTICS_PrivId, @Power_GroupId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@ACCESS_NETWORK_STATISTICS_PrivId, @Standard_GroupId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@PRINT_EXPORT_NETWORK_STATISTICS_PrivId, @Power_GroupId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@PRINT_EXPORT_NETWORK_STATISTICS_PrivId, @Standard_GroupId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@ACCESS_ALERT_DEFINITIONS_PrivId, @Power_GroupId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@CREATE_EDIT_ALERT_DEFINITIONS_PrivId, @Power_GroupId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@ENABLE_DISABLE_ALERT_DEFINITIONS_PrivId, @Power_GroupId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@ACCESS_ALL_ALERTS_PrivId, @Power_GroupId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@ACCESS_ALL_ALERTS_PrivId, @Standard_GroupId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@ACKNOWLEDGE_AND_RESOLVE_ALERTS_PrivId, @Power_GroupId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@ACKNOWLEDGE_AND_RESOLVE_ALERTS_PrivId, @Standard_GroupId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@DISMISS_ALERTS_PrivId, @Power_GroupId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@DISMISS_ALERTS_PrivId, @Standard_GroupId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@ACCESS_DASHBOARDS_PrivId, @Power_GroupId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@ACCESS_DASHBOARDS_PrivId, @Standard_GroupId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@CREATE_EDIT_DELETE_OWN_PRIVATE_DASHBOARDS_PrivId, @Power_GroupId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@CREATE_EDIT_DELETE_OWN_PRIVATE_DASHBOARDS_PrivId, @Standard_GroupId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@CREATE_EDIT_DELETE_OWN_PUBLIC_DASHBOARDS_PrivId, @Power_GroupId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@ACCESS_FLOORPLAN_PrivId, @Power_GroupId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@ACCESS_FLOORPLAN_PrivId, @Standard_GroupId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@ACCESS_ACTIVE_CONTROL_DATA_PrivId, @Power_GroupId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@ACCESS_ACTIVE_CONTROL_DATA_PrivId, @Standard_GroupId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@EDIT_ACTIVE_CONTROL_OPERATIONAL_PARAMS_PrivId, @Power_GroupId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@EDIT_ACTIVE_CONTROL_TARGET_VALUES_PrivId, @Power_GroupId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@ACCESS_ENVIRONMENTALS_FLOORPLAN_PrivId, @Power_GroupId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@ACCESS_ENVIRONMENTALS_FLOORPLAN_PrivId, @Standard_GroupId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@MANAGE_NODES_PrivId, @Power_GroupId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@ENABLE_DISABLE_OBJECTS_PrivId, @Power_GroupId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@ACCESS_POWERIMAGING_FLOORPLAN_PrivId, @Power_GroupId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@ACCESS_POWERIMAGING_FLOORPLAN_PrivId, @Standard_GroupId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@ENABLE_LIVEIMAGING_LAYER_PrivId, @Power_GroupId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@ENABLE_LIVEIMAGING_LAYER_PrivId, @Standard_GroupId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@ACCESS_LIVEIMAGING_ADVANCED_PrivId, @Power_GroupId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@ACCESS_LIVEIMAGING_ADVANCED_PrivId, @Standard_GroupId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@PRINT_EXPORT_LIVEIMAGING_IMAGES_PrivId, @Power_GroupId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@PRINT_EXPORT_LIVEIMAGING_IMAGES_PrivId, @Standard_GroupId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@ACCESS_PUE_SUMMARY_PrivId, @Power_GroupId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@ACCESS_PUE_SUMMARY_PrivId, @Standard_GroupId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@ACCESS_PREDEFINED_METRICS_PrivId, @Power_GroupId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@ACCESS_PREDEFINED_METRICS_PrivId, @Standard_GroupId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@ACCESS_CUSTOM_METRICS_PrivId, @Power_GroupId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@ACCESS_CUSTOM_METRICS_PrivId, @Standard_GroupId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@ACCESS_PDU_VIEW_PrivId, @Power_GroupId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@ACCESS_PDU_VIEW_PrivId, @Standard_GroupId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@ACCESS_BCMS_VIEW_PrivId, @Power_GroupId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@ACCESS_BCMS_VIEW_PrivId, @Standard_GroupId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@EDIT_BCMS_CIRCUIT_NAMES_PrivId, @Power_GroupId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@ACCESS_DATA_VIEWS_PrivId, @Power_GroupId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@ACCESS_DATA_VIEWS_PrivId, @Standard_GroupId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@CREATE_EDIT_DELETE_OWN_PRIVATE_TEMPLATES_PrivId, @Power_GroupId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@CREATE_EDIT_DELETE_OWN_PRIVATE_TEMPLATES_PrivId, @Standard_GroupId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@CREATE_EDIT_DELETE_OWN_PUBLIC_TEMPLATES_PrivId, @Power_GroupId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@PRINT_EXPORT_DATA_VIEWS_PrivId, @Power_GroupId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@PRINT_EXPORT_DATA_VIEWS_PrivId, @Standard_GroupId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@ACCESS_WEBCONSOLE_PrivId, @Power_GroupId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@ACCESS_WEBCONSOLE_PrivId, @Standard_GroupId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@VIEW_REPORTS_PrivId, @Power_GroupId);
INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@VIEW_REPORTS_PrivId, @Standard_GroupId);

-- ==================================================================================

-- create type 'USER'
INSERT INTO object_types(name) VALUES ('USER');

SET @userTypeId := last_insert_id();

INSERT INTO objects(name, object_type_id_fk, static) VALUES ('USER', @userTypeId, 1);

insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('userName', @userTypeId, 3);
SET @userNameId := last_insert_id();
insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('userPassword', @userTypeId, 3);
SET @userPassId := last_insert_id();
insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('userPasswordHint', @userTypeId, 3);
SET @userPassHintId := last_insert_id();
insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('userPasswordHintAnswer', @userTypeId, 3);
SET @userPassHistAnswerId := last_insert_id();
insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('FName', @userTypeId, 3);
SET @userFNameId := last_insert_id();
insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('LName', @userTypeId, 3);
SET @userLNameId := last_insert_id();
insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('middleInitial', @userTypeId, 3);
SET @userMIId := last_insert_id();
insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('title', @userTypeId, 3);
SET @userTitleId := last_insert_id();
insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('primaryPhonenumber', @userTypeId, 3);
SET @userPPhoneId := last_insert_id();
insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('secondaryPhonenumber', @userTypeId, 3);
SET @userSPhoneId := last_insert_id();
insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('primaryEmail', @userTypeId, 3);
SET @userPEmailId := last_insert_id();
insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('secondaryEmail', @userTypeId, 3);
SET @userSEmailId := last_insert_id();
insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('smsAddress', @userTypeId, 3);
SET @userSMSId := last_insert_id();
insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('description', @userTypeId, 3);
SET @userDescrId := last_insert_id();
insert into attributes(name, object_type_id_fk, attr_type_id_fk,historical,collection) values ('groups', @userTypeId, 5,0,1);
SET @userGroupsId := last_insert_id();
insert into attributes(name, object_type_id_fk, attr_type_id_fk,historical,collection) values ('preferences', @userTypeId, 5,0,1);
SET @userPrefsId := last_insert_id();

-- create objects 'USER'
INSERT INTO objects(object_type_id_fk) VALUES (@userTypeId);
SET @userObjectId := last_insert_id();
CALL insert_varchar_value(@userObjectId, @userNameId, @currentTime, 'synapsense');
CALL insert_varchar_value(@userObjectId, @userPassId, @currentTime, SHA1('synap$ense'));
CALL insert_varchar_value(@userObjectId, @userFNameId, @currentTime, 'Synapsense');
CALL insert_varchar_value(@userObjectId, @userLNameId, @currentTime, 'Synapsense');
CALL insert_varchar_value(@userObjectId, @userTitleId, @currentTime, 'Synapsense');
CALL insert_varchar_value(@userObjectId, @userDescrId, @currentTime, 'Synapsense');
CALL insert_link_value(@userObjectId, @userGroupsId, @currentTime, @Standard_GroupId);
set @userPreferences = insert_object_value(@userObjectId, @userPrefsId, @currentTime);

INSERT INTO objects(object_type_id_fk) VALUES (@userTypeId);
SET @userObjectId := last_insert_id();
CALL insert_varchar_value(@userObjectId, @userNameId, @currentTime, 'admin');
CALL insert_varchar_value(@userObjectId, @userPassId, @currentTime, SHA1('admin'));
CALL insert_varchar_value(@userObjectId, @userFNameId, @currentTime, 'Admin');
CALL insert_varchar_value(@userObjectId, @userLNameId, @currentTime, 'Admin');
CALL insert_varchar_value(@userObjectId, @userTitleId, @currentTime, 'Admin');
CALL insert_varchar_value(@userObjectId, @userDescrId, @currentTime, 'Admin');
CALL insert_link_value(@userObjectId, @userGroupsId, @currentTime, @Administrators_GroupId);
set @userPreferences = insert_object_value(@userObjectId, @userPrefsId, @currentTime);

-- create type 'USER_PREFERENCE'
INSERT INTO object_types(name) VALUES ('USER_PREFERENCE');

SET @lastTypeId := last_insert_id();

INSERT INTO objects(name, object_type_id_fk, static) VALUES ('USER_PREFERENCE', @lastTypeId, 1);

insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('name', @lastTypeId, 3);
insert into attributes(name, object_type_id_fk, attr_type_id_fk) values ('value', @lastTypeId, 3);

-- initital ids to start
-- should be updated each time in addition to new INSERT INTO clause
SET @max_object_id := IFNULL((SELECT max(`id`)+1 FROM `objects`),1);
SET @max_value_id := IFNULL((SELECT max(`value_id`)+1 FROM `obj_values`),1);
SET @max_valued_id := IFNULL((SELECT max(`id`)+1 FROM `valued`),1);
SET @max_valuel_id := IFNULL((SELECT max(`id`)+1 FROM `valuel`),1);
SET @max_valuei_id := IFNULL((SELECT max(`id`)+1 FROM `valuei`),1);
SET @max_valuev_id := IFNULL((SELECT max(`id`)+1 FROM `valuev`),1);
SET @max_valuelink_id := IFNULL((SELECT max(`id`)+1 FROM `value_obj_link`),1);
SET @max_valueplink_id := IFNULL((SELECT max(`id`)+1 FROM `value_property_link`),1);
SET @max_tag_value_id := IFNULL((SELECT max(`value_id`)+1 FROM `tag_values`),1);
SET @max_group_value_id := 1;

INSERT INTO  `uid_table` (`object_id`,`value_id`,`valued_id`,`valuev_id`,`valuei_id`,`valuel_id`,`valuelink_id`,`valueplink_id`,`tag_value_id`,`group_id`) values (@max_object_id,@max_value_id,@max_valued_id,@max_valuev_id,@max_valuei_id,@max_valuel_id,@max_valuelink_id,@max_valueplink_id,@max_tag_value_id,@max_group_value_id);


-- =============================================================================
-- Rules
-- rule types init

INSERT INTO `tbl_rule_types` (`name`, `source_type`, `source`, `dsl`, `precompiled`, `type`, `rule_scheduler_config`, `version`) VALUES ('Sensor disconnected','drools','package ConditionalAlerts;\r\n\r\nimport com.synapsense.dre.domainobjects.*;\r\nimport com.synapsense.dre.service.DRulesEngineService;\r\nimport com.synapsense.dre.service.Trigger;\r\nimport com.synapsense.util.LocalizationUtils;\r\n\r\nglobal java.util.concurrent.ConcurrentHashMap triggers;\r\n\r\nrule \"Sensor disconnected\" \r\n    when\r\n        obj0 : WSNSENSOR( $prop0 : lastValue== -3000 )\r\n    then\r\n        Trigger t = new Trigger(\"Sensor disconnected\");\r\n        t.addObject(0,obj0);\r\n        if(triggers.putIfAbsent(t.getCompositeId(), t) == null){ \r\n            insert(t);\r\n            String nodeInfo = \"\";\r\n            WSNNODE parent = ((WSNNODE)DRulesEngineService.getParent(obj0, \"WSNNODE\"));\r\n            if(parent != null){\r\n                nodeInfo = LocalizationUtils.getLocalizedString(\"alert_message_node_info\", new String[] {parent.getName()});\r\n            }\r\n            DRulesEngineService.sendAlert(\"Sensor disconnected\", \"Sensor disconnected\", nodeInfo + LocalizationUtils.getLocalizedString(\"alert_message_sensor_info\", new String[] {obj0.getName()}) ,  null, obj0.getNativeTO(), t, 0);\r\n        }\r\nend\r\nrule \"anti_Sensor disconnected\" \r\n    when\r\n        $t : Trigger( ruleId == \"Sensor disconnected\" )\r\n        obj0 : WSNSENSOR(eval ( serializedTO  == $t.getObject(0)), $prop0 : lastValue != -3000 )\r\n    then\r\n        Trigger t = (Trigger)triggers.remove($t.getCompositeId());\r\n        if(t != null) retract(t);\r\nend',NULL,0,2,'0 0/5 * * * ?',0);
INSERT INTO `tbl_rule_types` (`name`, `source_type`, `source`, `dsl`, `precompiled`, `type`, `rule_scheduler_config`, `version`) VALUES ('Sensor requires service','drools','package ConditionalAlerts;\r\n\r\nimport com.synapsense.dre.domainobjects.*;\r\nimport com.synapsense.dre.service.DRulesEngineService;\r\nimport com.synapsense.dre.service.Trigger;\r\nimport com.synapsense.util.LocalizationUtils;\r\n\r\nglobal java.util.concurrent.ConcurrentHashMap triggers;\r\n\r\nrule \"Sensor requires service\"\r\n    when\r\n        obj0 : WSNSENSOR( $prop0 : lastValue == -2000)\r\n    then\r\n        Trigger t = new Trigger(\"Sensor requires service\");\r\n        t.addObject(0,obj0);\r\n        if(triggers.putIfAbsent(t.getCompositeId(), t) == null){ \r\n            insert(t);\r\n            String nodeInfo = \"\";\r\n            WSNNODE parent = ((WSNNODE)DRulesEngineService.getParent(obj0, \"WSNNODE\"));\r\n            if(parent != null){\r\n                nodeInfo = LocalizationUtils.getLocalizedString(\"alert_message_node_info\", new String[] {parent.getName()}); \r\n            }\r\n            DRulesEngineService.sendAlert(\"Sensor requires service\", \"Sensor requires service\", nodeInfo + LocalizationUtils.getLocalizedString(\"alert_message_sensor_info\", new String[] {obj0.getName()}), null, obj0.getNativeTO(), t, 0);\r\n        }\r\nend\r\nrule \"anti_Sensor requires service\" \r\n    when\r\n        $t : Trigger( ruleId == \"Sensor requires service\" )\r\n        obj0 : WSNSENSOR(eval ( serializedTO  == $t.getObject(0)), $prop0 : lastValue != -2000 )\r\n    then\r\n        Trigger t = (Trigger)triggers.remove($t.getCompositeId());\r\n        if(t != null) retract(t);\r\nend',NULL,0,2,'0 0/5 * * * ?',0);
INSERT INTO `tbl_rule_types` (`name`, `source_type`, `source`, `dsl`, `precompiled`, `type`, `rule_scheduler_config`, `version`) VALUES ('Node not reporting','drools','package ConditionalAlerts;\r\n\r\nimport com.synapsense.dre.domainobjects.*;\r\nimport com.synapsense.dre.service.DRulesEngineService;\r\nimport com.synapsense.dre.service.Trigger;\r\nimport com.synapsense.util.LocalizationUtils;\r\n\r\nglobal java.util.concurrent.ConcurrentHashMap triggers;\r\n\r\nrule \"Node not reporting\"\r\n    when\r\n        (obj0:WSNNODE(status == 0))\r\n    then\r\n        Trigger t = new Trigger(\"Node not reporting\");\r\n        t.addObject(0,obj0);\r\n        if(triggers.putIfAbsent(t.getCompositeId(), t) == null){ \r\n            insert(t);\r\n            DRulesEngineService.sendAlert(\"Node not reporting\", \"Node not reporting\", LocalizationUtils.getLocalizedString(\"alert_message_node_info\", new String[] {obj0.getName()}),  null, obj0.getNativeTO(), t, 0);\r\n        }\r\nend\r\nrule \"anti_Node not reporting\" \r\n    when\r\n        $t : Trigger( ruleId == \"Node not reporting\" )\r\n        obj0 : WSNNODE(eval ( serializedTO  == $t.getObject(0)), status == 1 )\r\n    then\r\n        Trigger t = (Trigger)triggers.remove($t.getCompositeId());\r\n        if(t != null) retract(t);\r\nend',NULL,0,2,'0 0/5 * * * ?',0);
INSERT INTO `tbl_rule_types` (`name`, `source_type`, `source`, `dsl`, `precompiled`, `type`, `rule_scheduler_config`, `version`) VALUES ('Out of recommended range','drools','package ConditionalAlerts;\r\n\r\nimport com.synapsense.dre.domainobjects.*;\r\nimport com.synapsense.dre.service.DRulesEngineService;\r\nimport com.synapsense.dre.service.Trigger;\r\nimport com.synapsense.util.LocalizationUtils;\r\n\r\nglobal java.util.concurrent.ConcurrentHashMap triggers;\r\n\r\nrule \"Out of recommended range\"\r\n    when\r\n        obj0:RACK() && eval(\r\n                              (DRulesEngineService.readDouble(obj0.getCTop(), \"lastValue\") > DRulesEngineService.readDouble(obj0.getCTop(), \"rMax\"))  ||\r\n                              (DRulesEngineService.readDouble(obj0.getCMid(), \"lastValue\") > DRulesEngineService.readDouble(obj0.getCMid(), \"rMax\"))  ||\r\n                              (DRulesEngineService.readDouble(obj0.getCBot(), \"lastValue\") >  DRulesEngineService.readDouble(obj0.getCBot(), \"rMax\")) ||\r\n                              ((DRulesEngineService.readDouble(obj0.getCBot(), \"lastValue\") < DRulesEngineService.readDouble(obj0.getCBot(), \"rMin\"))\r\n                                && (DRulesEngineService.readDouble(obj0.getCBot(), \"lastValue\") > -1000.0)) ||\r\n                              ((DRulesEngineService.readDouble(obj0.getCMid(), \"lastValue\") < DRulesEngineService.readDouble(obj0.getCMid(), \"rMin\"))\r\n                                && (DRulesEngineService.readDouble(obj0.getCMid(), \"lastValue\") > -1000.0)) ||\r\n                              ((DRulesEngineService.readDouble(obj0.getCTop(), \"lastValue\") < DRulesEngineService.readDouble(obj0.getCTop(), \"rMin\"))\r\n                                && (DRulesEngineService.readDouble(obj0.getCTop(), \"lastValue\") > -1000.0))\r\n                             )\r\n    then\r\n        RACK rack = obj0;\r\n        Trigger t = new Trigger(\"Out of recommended range\");\r\n        t.addObject(0,obj0);\r\n        if(triggers.putIfAbsent(t.getCompositeId(), t) == null){ \r\n        insert(t);\r\n        DRulesEngineService.sendAlert(\"Out of recommended range\", \"Out of recommended range\",\r\n                                      LocalizationUtils.getLocalizedString(\"alert_message_out_of_recommended_range\", new String[] {\r\n                                          rack.getName() ,\r\n                                          \"[$data(\" + rack.getNativeTO() + \".cTop.lastValue,\" + DRulesEngineService.readDouble(rack.getCTop(), \"lastValue\") + \",200)$, $data(\" + rack.getNativeTO() + \".cMid.lastValue,\" + DRulesEngineService.readDouble(rack.getCMid(), \"lastValue\") + \",200)$, $data(\" + rack.getNativeTO() + \".cBot.lastValue,\" + DRulesEngineService.readDouble(rack.getCBot(), \"lastValue\") + \",200)$]\"\r\n                                      }),\r\n                                      null, rack.getNativeTO(), t, 0);\r\n       }\r\nend\r\nrule \"anti_Out of recommended range\" \r\n	 when\r\n		$t : Trigger( ruleId == \"Out of recommended range\" )\r\n		not(obj0:RACK(eval ( serializedTO  == $t.getObject(0))) && eval(\r\n                              (DRulesEngineService.readDouble(obj0.getCTop(), \"lastValue\") > DRulesEngineService.readDouble(obj0.getCTop(), \"rMax\"))  ||\r\n                              (DRulesEngineService.readDouble(obj0.getCMid(), \"lastValue\") > DRulesEngineService.readDouble(obj0.getCMid(), \"rMax\"))  ||\r\n                              (DRulesEngineService.readDouble(obj0.getCBot(), \"lastValue\") >  DRulesEngineService.readDouble(obj0.getCBot(), \"rMax\")) ||\r\n                              ((DRulesEngineService.readDouble(obj0.getCBot(), \"lastValue\") < DRulesEngineService.readDouble(obj0.getCBot(), \"rMin\"))\r\n                                && (DRulesEngineService.readDouble(obj0.getCBot(), \"lastValue\") > -1000.0)) ||\r\n                              ((DRulesEngineService.readDouble(obj0.getCMid(), \"lastValue\") < DRulesEngineService.readDouble(obj0.getCMid(), \"rMin\"))\r\n                                && (DRulesEngineService.readDouble(obj0.getCMid(), \"lastValue\") > -1000.0)) ||\r\n                              ((DRulesEngineService.readDouble(obj0.getCTop(), \"lastValue\") < DRulesEngineService.readDouble(obj0.getCTop(), \"rMin\"))\r\n                                && (DRulesEngineService.readDouble(obj0.getCTop(), \"lastValue\") > -1000.0))\r\n                             )\r\n)\r\n	 then\r\nTrigger t = (Trigger)triggers.remove($t.getCompositeId());\r\nif(t != null) retract(t);\r\nend',NULL,0,2,'0 0/5 * * * ?',0);
INSERT INTO `tbl_rule_types` (`name`, `source_type`, `source`, `dsl`, `precompiled`, `type`, `rule_scheduler_config`, `version`) VALUES ('Out of allowable range','drools','package ConditionalAlerts;\r\n\r\nimport com.synapsense.dre.domainobjects.*;\r\nimport com.synapsense.dre.service.DRulesEngineService;\r\nimport com.synapsense.dre.service.Trigger;\r\nimport com.synapsense.util.LocalizationUtils;\r\n\r\nglobal java.util.concurrent.ConcurrentHashMap triggers;\r\n\r\nrule \"Out of allowable range\"\r\n    when\r\n        obj0:RACK() && eval(\r\n                              (DRulesEngineService.readDouble(obj0.getCTop(), \"lastValue\") > DRulesEngineService.readDouble(obj0.getCTop(), \"aMax\"))  ||\r\n                              (DRulesEngineService.readDouble(obj0.getCMid(), \"lastValue\") > DRulesEngineService.readDouble(obj0.getCMid(), \"aMax\"))  ||\r\n                              (DRulesEngineService.readDouble(obj0.getCBot(), \"lastValue\") >  DRulesEngineService.readDouble(obj0.getCBot(), \"aMax\")) ||\r\n                              ((DRulesEngineService.readDouble(obj0.getCBot(), \"lastValue\") < DRulesEngineService.readDouble(obj0.getCBot(), \"aMin\"))\r\n                                && (DRulesEngineService.readDouble(obj0.getCBot(), \"lastValue\") > -1000.0)) ||\r\n                              ((DRulesEngineService.readDouble(obj0.getCMid(), \"lastValue\") < DRulesEngineService.readDouble(obj0.getCMid(), \"aMin\"))\r\n                                && (DRulesEngineService.readDouble(obj0.getCMid(), \"lastValue\") > -1000.0)) ||\r\n                              ((DRulesEngineService.readDouble(obj0.getCTop(), \"lastValue\") < DRulesEngineService.readDouble(obj0.getCTop(), \"aMin\"))\r\n                                && (DRulesEngineService.readDouble(obj0.getCTop(), \"lastValue\") > -1000.0))\r\n                             )\r\n    then\r\n        RACK rack = obj0;\r\n        Trigger t = new Trigger(\"Out of allowable range\");\r\n        t.addObject(0,obj0);\r\n        if(triggers.putIfAbsent(t.getCompositeId(), t) == null){ \r\n        insert(t);\r\n        DRulesEngineService.sendAlert(\"Out of allowable range\", \"Out of allowable range\",\r\n                                      LocalizationUtils.getLocalizedString(\"alert_message_out_of_allowable_range\", new String[] {\r\n                                          rack.getName() ,\r\n                                          \"[$data(\" + rack.getNativeTO() + \".cTop.lastValue,\" + DRulesEngineService.readDouble(rack.getCTop(), \"lastValue\") + \",200)$, $data(\" + rack.getNativeTO() + \".cMid.lastValue,\" + DRulesEngineService.readDouble(rack.getCMid(), \"lastValue\") + \",200)$, $data(\" + rack.getNativeTO() + \".cBot.lastValue,\" + DRulesEngineService.readDouble(rack.getCBot(), \"lastValue\") + \",200)$]\"\r\n                                      }),\r\n                                      null, rack.getNativeTO(), t, 0);\r\n        }\r\n        \r\nend\r\nrule \"anti_Out of allowable range\" \r\n	 when\r\n		$t : Trigger( ruleId == \"Out of allowable range\" )\r\n		not(obj0:RACK(eval ( serializedTO  == $t.getObject(0))) && eval(\r\n                              (DRulesEngineService.readDouble(obj0.getCTop(), \"lastValue\") > DRulesEngineService.readDouble(obj0.getCTop(), \"aMax\"))  ||\r\n                              (DRulesEngineService.readDouble(obj0.getCMid(), \"lastValue\") > DRulesEngineService.readDouble(obj0.getCMid(), \"aMax\"))  ||\r\n                              (DRulesEngineService.readDouble(obj0.getCBot(), \"lastValue\") >  DRulesEngineService.readDouble(obj0.getCBot(), \"aMax\")) ||\r\n                              ((DRulesEngineService.readDouble(obj0.getCBot(), \"lastValue\") < DRulesEngineService.readDouble(obj0.getCBot(), \"aMin\"))\r\n                                && (DRulesEngineService.readDouble(obj0.getCBot(), \"lastValue\") > -1000.0)) ||\r\n                              ((DRulesEngineService.readDouble(obj0.getCMid(), \"lastValue\") < DRulesEngineService.readDouble(obj0.getCMid(), \"aMin\"))\r\n                                && (DRulesEngineService.readDouble(obj0.getCMid(), \"lastValue\") > -1000.0)) ||\r\n                              ((DRulesEngineService.readDouble(obj0.getCTop(), \"lastValue\") < DRulesEngineService.readDouble(obj0.getCTop(), \"aMin\"))\r\n                                && (DRulesEngineService.readDouble(obj0.getCTop(), \"lastValue\") > -1000.0))\r\n                             )\r\n)\r\n	 then\r\nTrigger t = (Trigger)triggers.remove($t.getCompositeId());\r\nif(t != null) retract(t);\r\nend',NULL,0,2,'0 0/5 * * * ?',0);
INSERT INTO `tbl_rule_types` (`name`, `source_type`, `source`, `dsl`, `precompiled`, `type`, `rule_scheduler_config`, `version`) VALUES ('Circuit Current above threshold','drools','package ConditionalAlerts;\r\n\r\nimport com.synapsense.dre.domainobjects.*;\r\nimport com.synapsense.dre.service.DRulesEngineService;\r\nimport com.synapsense.dre.service.Trigger;\r\nimport com.synapsense.util.LocalizationUtils;\r\n\r\nglobal java.util.concurrent.ConcurrentHashMap triggers;\r\n\r\nrule \"Circuit Current above threshold\"\r\n    when\r\n        obj0:CIRCUIT() && eval((DRulesEngineService.readDouble(obj0.getCurrent(), \"lastValue\")/obj0.getMaxCurrent() * 100) > obj0.getMidCurrent())\r\n    then      \r\n        Trigger t = new Trigger(\"Circuit Current above threshold\");\r\n        t.addObject(0,obj0);\r\n        if(triggers.putIfAbsent(t.getCompositeId(), t) == null){ \r\n        insert(t);\r\n        DRulesEngineService.sendAlert(\"Circuit Current above threshold\", \"Circuit Current above threshold\",  LocalizationUtils.getLocalizedString(\"alert_message_circuit_current_above_threshold\", new String[] {\r\n                        obj0.getName(),\r\n                        \"$data(\" +  obj0.getNativeTO() + \".current.lastValue,\" + DRulesEngineService.readDouble(obj0.getCurrent(), \"lastValue\") + \",206)$\"\r\n                    }) , null, obj0.getNativeTO(), t, 0);\r\n        }\r\nend\r\nrule \"anti_Circuit Current above threshold\" \r\n	 when\r\n		$t : Trigger( ruleId == \"Circuit Current above threshold\" )\r\n		obj0:CIRCUIT(eval ( serializedTO  == $t.getObject(0)))\r\n		not (eval((DRulesEngineService.readDouble(obj0.getCurrent(), \"lastValue\")/obj0.getMaxCurrent() * 100) > obj0.getMidCurrent()))\r\n		\r\n	 then\r\nTrigger t = (Trigger)triggers.remove($t.getCompositeId());\r\nif(t != null) retract(t);\r\nend',NULL,0,2,'0 0/5 * * * ?',0);
INSERT INTO `tbl_rule_types` (`name`, `source_type`, `source`, `dsl`, `precompiled`, `type`, `rule_scheduler_config`, `version`) VALUES ('Circuit Power above threshold','drools','package ConditionalAlerts;\r\n\r\nimport com.synapsense.dre.domainobjects.*;\r\nimport com.synapsense.dre.service.DRulesEngineService;\r\nimport com.synapsense.dre.service.Trigger;\r\nimport com.synapsense.util.LocalizationUtils;\r\n\r\nglobal java.util.concurrent.ConcurrentHashMap triggers;\r\n\r\nrule \"Circuit Power above threshold\"\r\n    when\r\n        obj0:CIRCUIT() && eval((DRulesEngineService.readDouble(obj0.getPower(), \"lastValue\")/obj0.getMaxPower() * 100) > obj0.getMidPower())\r\n    then      \r\n        Trigger t = new Trigger(\"Circuit Power above threshold\");\r\n        t.addObject(0,obj0);\r\n        if(triggers.putIfAbsent(t.getCompositeId(), t) == null){ \r\n        insert(t);\r\n        DRulesEngineService.sendAlert(\"Circuit Power above threshold\", \"Circuit Power above threshold\",  LocalizationUtils.getLocalizedString(\"alert_message_circuit_power_above_threshold\", new String[] {\r\n                        obj0.getName(),\r\n                        \"$data(\" +  obj0.getNativeTO() + \".power.lastValue,\" + DRulesEngineService.readDouble(obj0.getPower(), \"lastValue\") + \",207)$\"\r\n                    }) , null, obj0.getNativeTO(), t, 0);\r\n        }\r\nend\r\nrule \"anti_Circuit Power above threshold\" \r\n	 when\r\n		$t : Trigger( ruleId == \"Circuit Power above threshold\" )\r\n		obj0:CIRCUIT(eval ( serializedTO  == $t.getObject(0)))\r\n		not (eval((DRulesEngineService.readDouble(obj0.getPower(), \"lastValue\")/obj0.getMaxPower() * 100) > obj0.getMidPower()))\r\n		\r\n	 then\r\nTrigger t = (Trigger)triggers.remove($t.getCompositeId());\r\nif(t != null) retract(t);\r\nend',NULL,0,2,'0 0/5 * * * ?',0);
INSERT INTO `tbl_rule_types` (`name`, `source_type`, `source`, `dsl`, `precompiled`, `type`, `rule_scheduler_config`, `version`) VALUES ('Circuit Power Factor below threshold','drools','package ConditionalAlerts;\r\n\r\nimport com.synapsense.dre.domainobjects.*;\r\nimport com.synapsense.dre.service.DRulesEngineService;\r\nimport com.synapsense.dre.service.Trigger;\r\nimport com.synapsense.util.LocalizationUtils;\r\n\r\nglobal java.util.concurrent.ConcurrentHashMap triggers;\r\n\r\nrule \"Circuit Power Factor below threshold\"\r\n    when\r\n        obj0:CIRCUIT() && eval((DRulesEngineService.readDouble(obj0.getPowerFactor(), \"lastValue\")/obj0.getMaxPowerFactor() * 100) < obj0.getMidPowerFactor())\r\n    then      \r\n        Trigger t = new Trigger(\"Circuit Power Factor below threshold\");\r\n        t.addObject(0,obj0);\r\n        if(triggers.putIfAbsent(t.getCompositeId(), t) == null){ \r\n        insert(t);\r\n        DRulesEngineService.sendAlert(\"Circuit Power Factor below threshold\", \"Circuit Power Factor below threshold\",  LocalizationUtils.getLocalizedString(\"alert_message_circuit_power_factor_below_threshold\", new String[] {\r\n                        obj0.getName(),\r\n                        \"$data(\" +  obj0.getNativeTO() + \".powerFactor.lastValue,\" + DRulesEngineService.readDouble(obj0.getPowerFactor(), \"lastValue\") + \",powerfactor)$\"\r\n                    }) , null, obj0.getNativeTO(), t, 0);\r\n        }\r\nend\r\nrule \"anti_Circuit Power Factor below threshold\" \r\n	 when\r\n		$t : Trigger( ruleId == \"Circuit Power Factor below threshold\" )\r\n		obj0:CIRCUIT(eval ( serializedTO  == $t.getObject(0)))\r\n		not (eval((DRulesEngineService.readDouble(obj0.getPowerFactor(), \"lastValue\")/obj0.getMaxPowerFactor() * 100) < obj0.getMidPowerFactor()))\r\n		\r\n	 then\r\nTrigger t = (Trigger)triggers.remove($t.getCompositeId());\r\nif(t != null) retract(t);\r\nend',NULL,0,2,'0 0/5 * * * ?',0);
INSERT INTO `tbl_rule_types` (`name`, `source_type`, `source`, `dsl`, `precompiled`, `type`, `rule_scheduler_config`, `version`) VALUES ('Maximum Threshold Alert','drools','package ConditionalAlerts;\n\n\n\nimport com.synapsense.dre.domainobjects.*;\n\nimport com.synapsense.dre.service.DRulesEngineService;\n\nimport com.synapsense.dre.service.Trigger;\n\nimport com.synapsense.util.LocalizationUtils;\n\n\n\nglobal java.util.concurrent.ConcurrentHashMap triggers;\n\n\n\nrule \"Maximum Threshold Alert\" \n\n when\n\n obj0 : PHASE() && eval ( obj0.getAvgCurrent() != null && obj0.getMaxCurrentThreshold() != null && obj0.getAvgCurrent() >= obj0.getMaxCurrentThreshold() )\n\n then\n\n    Trigger t = new Trigger(\"Maximum Threshold Alert\");\n\n    t.addObject(0,obj0);\n\n    if(triggers.putIfAbsent(t.getCompositeId(), t) == null) { \n\n    insert(t);\n\n    RPDU parent = ((RPDU)DRulesEngineService.getParent(obj0, \"RPDU\"));\n\n    String powerRackInfo = \"\";\n\n    if(parent != null){\n\n      powerRackInfo = \"Rack: \" + parent.getName() + \" \";\n\n    }\n\n    DRulesEngineService.sendAlert(\"Maximum Threshold Alert\", \"Maximum Threshold Alert\", \n\n        LocalizationUtils.getLocalizedString(\"alert_message_maximum_threshold_alert\", \n\n            new String[] {\n\n                powerRackInfo + \" Phase \" + obj0.getName(),\n\n                \"$data(\" + obj0.getNativeTO() + \".avgCurrent,\" + obj0.getAvgCurrent() + \",206)$\",\n                \"$data(\" + obj0.getNativeTO() + \".maxCurrentThreshold,\" + obj0.getMaxCurrentThreshold() + \",206)$\"\n\n                }), \n\n            null, obj0.getNativeTO(), t, 0);\n\n    }\n\n end\n\nrule \"anti_Maximum Threshold Alert\" \n\n when\n\n $t : Trigger( ruleId == \"Maximum Threshold Alert\" )\n\n                obj0 : PHASE(eval ( serializedTO == $t.getObject(0)))\n\n          not ( eval( obj0.getAvgCurrent() != null && obj0.getMaxCurrentThreshold() != null && obj0.getAvgCurrent() >= obj0.getMaxCurrentThreshold() ) )\n\n then\n\n    Trigger t = (Trigger)triggers.remove($t.getCompositeId());\n\n    if(t != null) retract(t);\n\n end\n',NULL,'0','2','0 0/5 * * * ?','0');
INSERT INTO `tbl_rule_types` (`name`, `source_type`, `source`, `dsl`, `precompiled`, `type`, `rule_scheduler_config`, `version`) VALUES ('Unbalanced Phases Alert','drools','package ConditionalAlerts;\r\n\r\nimport com.synapsense.dre.domainobjects.*;\r\nimport com.synapsense.dre.service.DRulesEngineService;\r\nimport com.synapsense.dre.service.Trigger;\r\nimport com.synapsense.util.LocalizationUtils;\r\n\r\nglobal java.util.concurrent.ConcurrentHashMap triggers;\r\n\r\n\r\n\r\nrule \"Unbalanced Phases Alert\" \r\n when\r\n obj0 : RPDU( phaseOutOfBalance != 0 )\r\n\r\n then\r\n    Trigger t = new Trigger(\"Unbalanced Phases Alert\");\r\n    t.addObject(0,obj0);\r\n    if(triggers.putIfAbsent(t.getCompositeId(), t) == null){ \r\n    insert(t);\r\n    String powerRackInfo = \"\";\r\n    POWER_RACK parent = ((POWER_RACK)DRulesEngineService.getParent(obj0, \"POWER_RACK\"));\r\n    if(parent != null){\r\n        powerRackInfo =  LocalizationUtils.getLocalizedString(\"alert_message_rack_info\", new String[] {parent.getName()});\r\n    }\r\n    DRulesEngineService.sendAlert(\"Unbalanced Phases Alert\", \"Unbalanced Phases Alert\", powerRackInfo +  LocalizationUtils.getLocalizedString(\"alert_message_unbalanced_phases\", new String[] { obj0.getName()}), null, obj0.getNativeTO(), t, 0);\r\n    }\r\nend\r\nrule \"anti_Unbalanced Phases Alert\" \r\n when\r\n $t : Trigger( ruleId == \"Unbalanced Phases Alert\" )\r\n         obj0 : RPDU(eval ( serializedTO == $t.getObject(0)), phaseOutOfBalance == 0 )\r\n\r\n then\r\nTrigger t = (Trigger)triggers.remove($t.getCompositeId());\r\nif(t != null) retract(t);\r\nend\r\n\r\n',NULL,0,2,'0 0/5 * * * ?',0);

-- =============================================================================
-- drop auxiliary functions & procedures
DROP FUNCTION IF EXISTS insert_object_value;
DROP PROCEDURE IF EXISTS insert_varchar_value;
DROP PROCEDURE IF EXISTS insert_double_value;
DROP PROCEDURE IF EXISTS insert_int_value;
DROP PROCEDURE IF EXISTS insert_long_value;
DROP PROCEDURE IF EXISTS insert_link_value;
DROP PROCEDURE IF EXISTS add_link_value;
DROP PROCEDURE IF EXISTS insert_binarydata_value;
