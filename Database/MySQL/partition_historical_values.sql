
use `synap`;

set @createPartitionStmt = concat(
  "alter table historical_values partition by range(to_days(`timestamp`)) (",
  " partition p1 values less than (", to_days(now()) + 1, "),",
  " partition p2 values less than (", to_days(now()) + 2, "),",
  " partition pMax values less than (", to_days("2100-01-01"), "));");
prepare createPartition from @createPartitionStmt;
execute createPartition;
deallocate prepare createPartition;

DROP PROCEDURE IF EXISTS `GET_NUM_PARTITIONS`;

delimiter //
CREATE PROCEDURE GET_NUM_PARTITIONS(
  IN schemaName VARCHAR(50),
  IN tableName VARCHAR(50),
  OUT numPartitions INTEGER)
begin
  SELECT count(*) into numPartitions
  FROM information_schema.PARTITIONS p
  where p.table_schema = schemaName
  and p.table_name = tableName;
end//
delimiter ;

DROP PROCEDURE IF EXISTS `GET_OLDEST_PARTITION_INFO`;

delimiter //
CREATE PROCEDURE GET_OLDEST_PARTITION_INFO(
  IN schemaName VARCHAR(50),
  IN tableName VARCHAR(50),
  OUT timepoint INTEGER,
  OUT partitionName VARCHAR(10))
begin
  SELECT distinct p.partition_description, p.partition_name
  into timepoint, partitionName
  FROM information_schema.PARTITIONS p
  where p.table_schema = schemaName
  and p.table_name = tableName
  and p.partition_ordinal_position =
    (select distinct(min(P_sub.partition_ordinal_position))
      from information_schema.PARTITIONS P_sub where P_sub.table_schema = schemaName
      and P_sub.table_name = tableName
    );
end//
delimiter ;


DROP PROCEDURE IF EXISTS `GET_MAX_RANGE_PARTITION_INFO`;

delimiter //
CREATE PROCEDURE GET_MAX_RANGE_PARTITION_INFO(
  IN schemaName VARCHAR(50),
  IN tableName VARCHAR(50),
  OUT timepoint INTEGER,
  OUT partitionName VARCHAR(10))
begin
  SELECT distinct p.partition_description, p.partition_name
  into timepoint, partitionName
  FROM information_schema.PARTITIONS p
  where p.table_schema = schemaName
  and p.table_name = tableName
  and p.partition_ordinal_position =
    (select distinct(MAX(P_sub.partition_ordinal_position))
      from information_schema.PARTITIONS P_sub where P_sub.table_schema = schemaName
      and P_sub.table_name = tableName
    );
end//
delimiter ;


DROP FUNCTION IF EXISTS `GET_PARTITION_UPPER_RANGE_BORDER`;

delimiter //
CREATE FUNCTION GET_PARTITION_UPPER_RANGE_BORDER(
  schemaName VARCHAR(50),
  tableName VARCHAR(50)
)  RETURNS INTEGER
begin
  DECLARE result INTEGER;

  -- retrieves the upper range border of the newest partition
  -- before the MAX_RANGE partition (ie, next to last partition)
  SELECT distinct p.partition_description
  into result
  FROM information_schema.PARTITIONS p
  where p.table_schema = schemaName
  and p.table_name = tableName
  and p.partition_ordinal_position =
  (select distinct(min(P_sub1.partition_ordinal_position))
    from (select distinct P_sub.partition_ordinal_position from
    information_schema.PARTITIONS P_sub where P_sub.table_schema = schemaName
    and P_sub.table_name = tableName order by partition_ordinal_position desc limit 2) as P_sub1
  );

  return result;
end//
delimiter ;

DROP PROCEDURE IF EXISTS `CREATE_PARTITION`;

delimiter //
CREATE PROCEDURE CREATE_PARTITION(
  IN schemaName VARCHAR(50),
  IN tableName VARCHAR(50),
  IN partitionName VARCHAR(50),
  IN timepoint INT,
  IN maxRangePartitionName VARCHAR(50),
  IN maxRangeTimepoint INT)
begin
  -- dynamic instruction to create new partition by reorginizing the partiotion with max range border into 2
  set @preparedStmtCreate = concat(
    'ALTER TABLE ', schemaName,'.',tableName,' REORGANIZE partition ',
    maxRangePartitionName, ' INTO (',
      ' partition ', partitionName, ' values LESS THAN (',timepoint,'),',
      ' partition ', maxRangePartitionName,' values LESS THAN(',maxRangeTimepoint,'));');

  PREPARE create_new_partition from @preparedStmtCreate;
  EXECUTE create_new_partition;
  DEALLOCATE PREPARE create_new_partition;
end//
delimiter ;

DROP PROCEDURE IF EXISTS `DROP_PARTITION`;

delimiter //
CREATE PROCEDURE DROP_PARTITION(
  IN schemaName VARCHAR(50),
  IN tableName VARCHAR(50),
  IN partitionName VARCHAR(50))
begin
  -- dynamic instruction to drop partition with the specified name (last partiotion is no more necessary)
  set @preparedStmtDrop = concat('ALTER TABLE ',schemaName,'.',tableName,' drop partition ',partitionName);
  PREPARE drop_last_partition from @preparedStmtDrop;
  EXECUTE drop_last_partition;
  DEALLOCATE PREPARE drop_last_partition;
end//
delimiter ;

DROP PROCEDURE IF EXISTS `ROTATE_PARTITIONS`;

delimiter //
CREATE PROCEDURE ROTATE_PARTITIONS(
  IN TABLE_NAME VARCHAR(50),
  IN MAX_ONLINE_PARTITIONS INTEGER)
BEGIN
  -- stores the number of existing partitions
  DECLARE numPartitions INTEGER;

  -- stores the name of the partition with lowest range (oldest data)
  DECLARE oldestPartitionName VARCHAR(10);
  -- the time point - determines the sensor data that has to be archived
  DECLARE oldestPartitionDayThreshold INTEGER;
  -- the time point - upper border limit of the new partition range(in days)
  DECLARE upperRangePartitionDayThreshold INTEGER;

  DECLARE maxRangePartitionName VARCHAR(10);
  DECLARE maxRangePartitionDayThreshold INTEGER;

  DECLARE tomorrowPartitionName VARCHAR(10);
  DECLARE tomorrowPartitionDayThreshold INTEGER;

  -- historical values
  -- retrieves date and partition name of the partition with last ordinal pos
  -- (means the partition with lower range border)
  -- implements cycle buffer of partition names
  call GET_OLDEST_PARTITION_INFO(DATABASE(),TABLE_NAME,
    oldestPartitionDayThreshold,
    oldestPartitionName
  );

  -- gets the total number of existing partitions
  call GET_NUM_PARTITIONS(DATABASE(), TABLE_NAME,
    numPartitions
  );

  -- retrieve the upper range border of the next-to-last partition,
  -- the one just before the MAX_RANGE partition.  It should equal to
  -- to_days(today() + 1 day)
  set upperRangePartitionDayThreshold = GET_PARTITION_UPPER_RANGE_BORDER(
    DATABASE(),TABLE_NAME);

  -- this partition represents tomorrow's data, tomorrow is the day less than
  -- the day after tomorrow
  set tomorrowPartitionDayThreshold = to_days(date_add(now(), interval 2 day));

  IF upperRangePartitionDayThreshold < tomorrowPartitionDayThreshold THEN
    -- Only drop a partition if one will be created.
    -- Otherwise, the partition names can get out of sync with the count.
    -- Only a real problem in artificial situations, or ones created by upgrade
    -- scripts, date changes, etc.  Better safe than sorry.
    IF numPartitions >= MAX_ONLINE_PARTITIONS THEN
      -- remove old partition only if there is 2 years data online
      call DROP_PARTITION(DATABASE(),TABLE_NAME,oldestPartitionName);
      -- re-use the old partition for new data
      set tomorrowPartitionName = oldestPartitionName;
    ELSE
      -- avoid name collision. partition name should be unique assuming low
      -- numbered partitions were not manually removed and maxRange partition
      -- is called 'pMax', not numeric
      set tomorrowPartitionName = concat('p', numPartitions);
    END IF;
    -- retrieves info about partition with max range border
    call GET_MAX_RANGE_PARTITION_INFO(DATABASE(),TABLE_NAME,
      maxRangePartitionDayThreshold,
      maxRangePartitionName);

    -- re-use the "oldest" partition for today's data
    call CREATE_PARTITION(DATABASE(),TABLE_NAME,
      tomorrowPartitionName,
      tomorrowPartitionDayThreshold,
      maxRangePartitionName,
      maxRangePartitionDayThreshold
    );
  END IF;
END//
delimiter ;


DROP EVENT IF EXISTS `ROTATE_HISTORICAL_VALUE_PARTITIONS_EVENT`;

delimiter //
CREATE EVENT `ROTATE_HISTORICAL_VALUE_PARTITIONS_EVENT`
-- start at 1AM today, run daily
ON SCHEDULE EVERY 1 DAY
STARTS TIMESTAMP(DATE(NOW()), '01:00')
DO
BEGIN
  DECLARE TABLE_NAME VARCHAR(20) default 'historical_values';
  -- 720 = 24 months * 30 days + 2 (1 for tomorrow, 1 for pMax)
  DECLARE MAX_ONLINE_PARTITIONS INTEGER default 722;

  CALL ROTATE_PARTITIONS(TABLE_NAME, MAX_ONLINE_PARTITIONS);
END//
delimiter ;

