-- =============================================================================
-- Diagram Name: SynapDB Design
-- Created on: 27.05.2011 10:00:23
-- Diagram Version: 1.1801
-- =============================================================================
drop database if exists `synap`;

-- ------------------------------------------------------------
-- description:
-- the purpose of this database is to provide a central location where configuration data and captured data for the synapsense condition monitoring system is persisted.
-- ------------------------------------------------------------

-- ------------------------------------------------------------
-- annotation:
-- synapdb
-- ------------------------------------------------------------



create database if not exists `synap`
character set utf8;

use `synap`;

SET FOREIGN_KEY_CHECKS=0;

-- Drop table tbl_activity_log
drop table if exists `tbl_activity_log`;

create table `tbl_activity_log` (
  `activity_id` int(11) auto_increment,
  `activity_desc` varchar(300),
  `activity_module` varchar(50),
  `timestamp` timestamp not null,
  `username` varchar(50) not null,
  `activity_action_type` varchar(50),
  `object_id_fk` int(11),
  primary key(`activity_id`),
  index `idx_action_time`(`activity_action_type`(20), `timestamp`),
  index `idx_module_time`(`activity_module`(20), `timestamp`),
  index `idx_username_time`(`username`(20), `timestamp`),
  index `idx_timestamp`(`timestamp`)
)
engine=innodb
character set utf8 ;

-- Drop table TBL_RULE_INSTANCES
drop table if exists `tbl_rule_instances`;

create table `tbl_rule_instances` (
  `rule_id` int(11) not null auto_increment,
  `name` varchar(128) not null,
  `version` int(11) default '0',
  `rule_type_id_fk` int(11) not null default '0',
  `object_id_fk` int(11) not null default '0',
  `attr_id_fk` int(11) not null default '0',
  primary key(`rule_id`),
  unique index `type_name_uniq`(`name`, `rule_type_id_fk`),
  unique index `object_attr_uniq`(`object_id_fk`, `attr_id_fk`)
)
engine=innodb
comment = 'rule instances';

-- Drop table objects
drop table if exists `objects`;

create table `objects` (
  `id` int(11) not null auto_increment,
  `object_type_id_fk` int(11) not null default '0' comment 'object type reference',
  `name` varchar(128) comment 'object name',
  `description` varchar(300) comment 'short object description',
  `instance_config` varchar(2000),
  `static` smallint(1) not null default '0',
  `version` int(11) default '0',
  primary key(`id`),
  index `idx_name`(`name`),
  index `fk_object_type`(`object_type_id_fk`)
)
engine=innodb
comment = 'objects instances';

-- Drop table obj_values
drop table if exists `obj_values`;

create table `obj_values` (
  `value_id` int(11) not null auto_increment,
  `timestamp` bigint(20),
  `object_id_fk` int(11) not null default '0',
  `attr_id_fk` int(11) not null default '0',
  `version` int(11) default '0',
  primary key(`value_id`),
  unique index `fk_object_attribute_uniq`(`object_id_fk`, `attr_id_fk`)
)
engine=innodb
comment = 'main value table';

-- Drop table object_types
drop table if exists `object_types`;

create table `object_types` (
  `objtype_id` int(11) not null auto_increment comment 'id',
  `name` varchar(128) not null comment 'unique object type name',
  `wsn` int(11) not null default '1' comment 'generic or wsn type',
  `type_config` varchar(2000),
  `version` int(11) default '0',
  primary key(`objtype_id`),
  unique index `name_uniq`(`name`)
)
engine=innodb
comment = 'object types';

-- Drop table valueD
drop table if exists `valued`;

create table `valued` (
  `id` int(11) not null auto_increment,
  `value` double,
  `value_id_fk` int(11) not null,
  primary key(`id`),
  index `fk_value_id`(`value_id_fk`)
)
engine=innodb
comment = 'stores double prop.values';

-- Drop table valueI
drop table if exists `valuei`;

create table `valuei` (
  `id` int(11) not null auto_increment,
  `value` int(11),
  `value_id_fk` int(11) not null,
  primary key(`id`),
  index `fk_value_id`(`value_id_fk`)
)
engine=innodb
comment = 'stores int prop.values';

-- Drop table valueV
drop table if exists `valuev`;

create table `valuev` (
  `id` int(11) not null auto_increment,
  `value` varchar(21833),
  `value_id_fk` int(11) not null,
  primary key(`id`),
  index `fk_value_id`(`value_id_fk`)
)
engine=innodb
comment = 'stores varchar prop.values';

-- Drop table attributes_types
drop table if exists `attributes_types`;

create table `attributes_types` (
  `attr_type_id` int(11) not null auto_increment,
  `java_type` varchar(50),
  `version` int(11),
  primary key(`attr_type_id`)
)
engine=innodb
comment = 'all prop.types';

-- Drop table valueDt
drop table if exists `valuedt`;

create table `valuedt` (
  `id` int(11) not null auto_increment,
  `value` timestamp,
  `value_id_fk` int(11) not null,
  primary key(`id`),
  index `fk_value_id`(`value_id_fk`)
)
engine=innodb
comment = 'stores date prop.values';

-- Drop table TBL_RULE_PROPERTY_SRC_ASSOC
drop table if exists `tbl_rule_property_src_assoc`;

create table `tbl_rule_property_src_assoc` (
  `object_id_fk` int(11) not null default '0',
  `attr_id_fk` int(11) not null default '0',
  `rule_id_fk` int(11) not null default '0',
  `binding` varchar(50),
  `link_type` smallint(6),
  primary key(`object_id_fk`, `attr_id_fk`, `rule_id_fk`)
)
engine=innodb
comment = 'assoc of rule with source objects';

-- Drop table VALUE_OBJ_LINK
drop table if exists `value_obj_link`;

create table `value_obj_link` (
  `id` int(11) not null auto_increment,
  `object_id` int(11),
  `object_type` varchar(50),
  `value_id_fk` int(11) not null,
  primary key(`id`),
  unique index `value_uniq`(`object_id`, `object_type`, `value_id_fk`)
)
engine=innodb
comment = 'stores links to objects';

-- Drop table valueL
drop table if exists `valuel`;

create table `valuel` (
  `id` int(11) not null auto_increment,
  `value` bigint(20),
  `value_id_fk` int(11) not null,
  primary key(`id`),
  index `fk_value_id`(`value_id_fk`)
)
engine=innodb
comment = 'stores long  prop.values';

-- Drop table HISTORICAL_VALUES
drop table if exists `historical_values`;

create table `historical_values` (
  `value_id_fk` int(11) not null,
  `timestamp` datetime not null,
  `valuei` int(11),
  `valued` double,
  `valuel` bigint(20),
  `valuev` varchar(21822),
  `group_id` int(11) not null default 0,
  unique index `idx_value_id_timestamp`(`value_id_fk`, `timestamp`, `group_id`)
)
engine=innodb;

-- Drop table HISTORICAL_BINARY_VALUES
drop table if exists `historical_binary_values`;

create table `historical_binary_values` (
  `value_id_fk` int(11) not null,
  `timestamp` datetime not null,
  `valueb` mediumblob,
  index `idx_value_id_timestamp_binary`(`value_id_fk`, `timestamp`)
)
engine=innodb;

-- Drop table attributes
drop table if exists `attributes`;

create table `attributes` (
  `attr_id` int(11) not null auto_increment,
  `name` varchar(128) not null,
  `mapped_name` varchar(250),
  `historical` smallint(1) default '0',
  `object_type_id_fk` int(11) not null default '0',
  `attr_type_id_fk` int(11) not null default '0',
  `collection` smallint(1) default '0',
  `static` smallint(1) not null default '0',
  `version` int(11) default '0',
  primary key(`attr_id`),
  unique index `name_type_uniq`(`name`, `object_type_id_fk`),
  index `fk_object_type_id`(`object_type_id_fk`),
  index `fk_attribute_type_id`(`attr_type_id_fk`)
)
engine=innodb
comment = 'object''s properties';

-- Drop table object_parents
drop table if exists `object_parents`;

create table `object_parents` (
  `object_id_fk` int(11) not null default '0',
  `parent_id_fk` int(11) not null default '0',
  primary key(`object_id_fk`, `parent_id_fk`)
)
engine=innodb
comment = 'parent-child many-2-many
first level ancestors';

-- Drop table VALUE_PROPERTY_LINK
drop table if exists `value_property_link`;

create table `value_property_link` (
  `id` int(11) not null auto_increment,
  `object_id` int(11),
  `object_type` varchar(50),
  `property_name` varchar(50),
  `value_id_fk` int(11) not null,
  primary key(`id`),
  index `fk_value_id`(`value_id_fk`),
  unique index `value_uniq`(`object_id`, `object_type`, `property_name`, `value_id_fk`)
)
engine=innodb
comment = 'stores links to properties of objects';

-- Drop table tbl_rule_types
drop table if exists `tbl_rule_types`;

create table `tbl_rule_types` (
  `rule_type_id` int(11) not null auto_increment,
  `name` varchar(128) not null,
  `source_type` varchar(50) not null,
  `source` mediumtext,
  `dsl` varchar(100),
  `precompiled` smallint(1) default '0',
  `type` smallint(2),
  `rule_scheduler_config` varchar(15),
  `version` int(11) default '0',
  `behavior` int(1) not null default '0',
  primary key(`rule_type_id`),
  unique index `name_uniq`(`name`, `type`)
)
engine=innodb
comment = 'rule templates';

-- Drop table tbl_network_statnode
drop table if exists `tbl_network_statnode`;

create table `tbl_network_statnode` (
  `id` int(11) not null auto_increment,
  `logical_id` int(11) unsigned,
  `nwk_timestamp` timestamp not null default current_timestamp,
  `send_fails_full_queue` smallint(6) unsigned,
  `send_fails_no_path` smallint(6) unsigned,
  `number_one_hop_retransmission` smallint(6) unsigned,
  `forwarding_packets_dropped` smallint(6) unsigned,
  `parent_fail_count` smallint(6) unsigned,
  `broadcast_retransmission` smallint(6) unsigned,
  `packets_from_base_station` smallint(6) unsigned,
  `packets_to_base_station` smallint(6) unsigned,
  `packets_forwarded` smallint(6) unsigned,
  `packets_sent` smallint(6) unsigned,
  `radio_on_time` int(11) unsigned comment 'radio on time in ms',
  `free_slot_count` smallint(6) unsigned,
  `wrong_slot_count` smallint(6) unsigned,
  `total_slot_count` smallint(6) unsigned,
  `packets_rcvd_count` smallint(6) unsigned,
  `crc_fail_packet_count` smallint(6) unsigned,
  `time_syncs_lost_drift_detection` smallint(6) unsigned,
  `time_syncs_lost_timeout` smallint(6) unsigned,
  `node_latency` int(11),
  `hop_count` int(11),
  `router_state` int(11),
  `parent_state` int(11),
  `noise_thresh` int(11),
  `ch_1` int(11) not null default '0',
  `ch_2` int(11) not null default '0',
  `ch_3` int(11) not null default '0',
  `ch_4` int(11) not null default '0',
  `ch_5` int(11) not null default '0',
  `ch_6` int(11) not null default '0',
  `ch_7` int(11) not null default '0',
  `ch_8` int(11) not null default '0',
  `ch_9` int(11) not null default '0',
  `ch_10` int(11) not null default '0',
  `ch_11` int(11) not null default '0',
  `ch_12` int(11) not null default '0',
  `ch_13` int(11) not null default '0',
  `ch_14` int(11) not null default '0',
  `ch_15` int(11) not null default '0',
  `ch_16` int(11) not null default '0',
  primary key(`id`),
  index `logical_id_timestamp_idx`(`logical_id`, `nwk_timestamp`)
)
engine=innodb;

-- Drop table tbl_network_statbase
drop table if exists `tbl_network_statbase`;

create table `tbl_network_statbase` (
  `id` int(11) not null auto_increment,
  `logical_id` int(11) unsigned,
  `nwk_timestamp` timestamp not null default current_timestamp,
  `packets_from_nodes` smallint(6) unsigned,
  `duplicate_packets` smallint(6) unsigned,
  `out_of_order_packets` smallint(6) unsigned,
  `packets_sent` smallint(6) unsigned,
  `free_slot_count` smallint(6) unsigned,
  `wrong_slot_count` smallint(6) unsigned,
  `total_slot_count` smallint(6) unsigned,
  `packets_rcvd_count` smallint(6) unsigned,
  `crc_fail_packet_count` smallint(6) unsigned,
  `time_syncs_lost_drift_detection` smallint(6) unsigned,
  `time_syncs_lost_timeout` smallint(6) unsigned,
  `noise_thresh` int(11),
  `ch_1` int(11) not null default '0',
  `ch_2` int(11) not null default '0',
  `ch_3` int(11) not null default '0',
  `ch_4` int(11) not null default '0',
  `ch_5` int(11) not null default '0',
  `ch_6` int(11) not null default '0',
  `ch_7` int(11) not null default '0',
  `ch_8` int(11) not null default '0',
  `ch_9` int(11) not null default '0',
  `ch_10` int(11) not null default '0',
  `ch_11` int(11) not null default '0',
  `ch_12` int(11) not null default '0',
  `ch_13` int(11) not null default '0',
  `ch_14` int(11) not null default '0',
  `ch_15` int(11) not null default '0',
  `ch_16` int(11) not null default '0',
  primary key(`id`)
)
engine=innodb;

-- Drop table TBL_NHT_INFO
drop table if exists `tbl_nht_info`;

create table `tbl_nht_info` (
  `id` int(11) not null auto_increment,
  `node_id` int(11),
  `node_id_hex` varchar(10),
  `num_neigh`  int(11),
  `report_timestamp` datetime,
  `neigh_id_hex` varchar(10),
  `rssi` varchar(20),
  `state` int(11),
  `hop` int(11),
  `hoptm` int(11),
  primary key(`id`)
)
engine=innodb;

-- Drop table value_binary
drop table if exists `value_binary`;

create table `value_binary` (
  `id` int(11) not null auto_increment,
  `value` mediumblob,
  `value_id_fk` int(11) not null,
  primary key(`id`),
  index `fk_value_id`(`value_id_fk`)
)
engine=innodb
comment = 'stores any binary prop.values';

-- Drop table tags
drop table if exists `tags`;

create table `tags` (
  `tag_id` int(11) not null auto_increment,
  `name` varchar(128) not null,
  `attr_id_fk` int(11) not null default '0',
  `tag_type_id_fk` int(11) not null default '0',
  primary key(`tag_id`),
  unique index `name_tag_uniq`(`name`, `attr_id_fk`),
  index `attr_id_index`(`attr_id_fk`),
  index `tag_type_index`(`tag_type_id_fk`)
)
engine=innodb
comment = 'tags of object''s properties ';

-- Drop table tag_values
drop table if exists `tag_values`;

create table `tag_values` (
  `value_id` int(11) not null auto_increment,
  `tag_id_fk` int(11) not null default '0',
  `value` varchar(21840),
  `object_id_fk` int(11) not null default '0',
  primary key(`value_id`),
  unique index `tag_uniq`(`tag_id_fk`, `object_id_fk`)
)
engine=innodb
comment = 'tag values table';

-- Drop table uid_table
drop table if exists `uid_table`;

create table `uid_table` (
  `object_id` int(11) not null,
  `value_id` int(11) not null,
  `valued_id` int(11) not null,
  `valuev_id` int(11) not null,
  `valuei_id` int(11) not null,
  `valuel_id` int(11) not null,
  `valuelink_id` int(11) not null,
  `valueplink_id` int(11) not null,
  `tag_value_id` int(11) not null,
  `group_id` int(11) not null
)
engine=innodb
comment = 'stores certain tables primary key
last generated ids used by key generation algorithm';

ALTER TABLE `tbl_activity_log` ADD
  CONSTRAINT `activity_record_refers_object` FOREIGN KEY (`object_id_fk`)
    REFERENCES `objects`(`id`)
    ON DELETE SET NULL
    ON UPDATE NO ACTION;

ALTER TABLE `tbl_rule_instances` ADD
  CONSTRAINT `Ref_65` FOREIGN KEY (`rule_type_id_fk`)
    REFERENCES `tbl_rule_types`(`rule_type_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION;

ALTER TABLE `tbl_rule_instances` ADD
  CONSTRAINT `Ref_55` FOREIGN KEY (`object_id_fk`)
    REFERENCES `objects`(`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION;

ALTER TABLE `tbl_rule_instances` ADD
  CONSTRAINT `Ref_60` FOREIGN KEY (`attr_id_fk`)
    REFERENCES `attributes`(`attr_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION;

ALTER TABLE `objects` ADD
  CONSTRAINT `object has type` FOREIGN KEY (`object_type_id_fk`)
    REFERENCES `object_types`(`objtype_id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION;

ALTER TABLE `obj_values` ADD
  CONSTRAINT `Ref_30` FOREIGN KEY (`object_id_fk`)
    REFERENCES `objects`(`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION;

ALTER TABLE `obj_values` ADD
  CONSTRAINT `Ref_33` FOREIGN KEY (`attr_id_fk`)
    REFERENCES `attributes`(`attr_id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION;

ALTER TABLE `valued` ADD
  CONSTRAINT `Ref_59` FOREIGN KEY (`value_id_fk`)
    REFERENCES `obj_values`(`value_id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION;

ALTER TABLE `valuei` ADD
  CONSTRAINT `Ref_68` FOREIGN KEY (`value_id_fk`)
    REFERENCES `obj_values`(`value_id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION;

ALTER TABLE `valuev` ADD
  CONSTRAINT `Ref_70` FOREIGN KEY (`value_id_fk`)
    REFERENCES `obj_values`(`value_id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION;

ALTER TABLE `valuedt` ADD
  CONSTRAINT `Ref_69` FOREIGN KEY (`value_id_fk`)
    REFERENCES `obj_values`(`value_id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION;

ALTER TABLE `tbl_rule_property_src_assoc` ADD
  CONSTRAINT `Ref_51` FOREIGN KEY (`object_id_fk`)
    REFERENCES `objects`(`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION;

ALTER TABLE `tbl_rule_property_src_assoc` ADD
  CONSTRAINT `Ref_53` FOREIGN KEY (`rule_id_fk`)
    REFERENCES `tbl_rule_instances`(`rule_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION;

ALTER TABLE `tbl_rule_property_src_assoc` ADD
  CONSTRAINT `Ref_52` FOREIGN KEY (`attr_id_fk`)
    REFERENCES `attributes`(`attr_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION;

ALTER TABLE `value_obj_link` ADD
  CONSTRAINT `Ref_56` FOREIGN KEY (`value_id_fk`)
    REFERENCES `obj_values`(`value_id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION;

ALTER TABLE `valuel` ADD
  CONSTRAINT `Ref_49` FOREIGN KEY (`value_id_fk`)
    REFERENCES `obj_values`(`value_id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION;

ALTER TABLE `attributes` ADD
  CONSTRAINT `Ref_39` FOREIGN KEY (`attr_type_id_fk`)
    REFERENCES `attributes_types`(`attr_type_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION;

ALTER TABLE `attributes` ADD
  CONSTRAINT `attribute has type` FOREIGN KEY (`object_type_id_fk`)
    REFERENCES `object_types`(`objtype_id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION;

ALTER TABLE `object_parents` ADD
  CONSTRAINT `Ref_48` FOREIGN KEY (`object_id_fk`)
    REFERENCES `objects`(`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION;

ALTER TABLE `object_parents` ADD
  CONSTRAINT `Ref_58` FOREIGN KEY (`parent_id_fk`)
    REFERENCES `objects`(`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION;

ALTER TABLE `value_property_link` ADD
  CONSTRAINT `Ref_57` FOREIGN KEY (`value_id_fk`)
    REFERENCES `obj_values`(`value_id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION;

ALTER TABLE `value_binary` ADD
  CONSTRAINT `root_value_reference` FOREIGN KEY (`value_id_fk`)
    REFERENCES `obj_values`(`value_id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION;

ALTER TABLE `tags` ADD
  CONSTRAINT `tag_descriptor_relates_to_attribute` FOREIGN KEY (`attr_id_fk`)
    REFERENCES `attributes`(`attr_id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION;

ALTER TABLE `tags` ADD
  CONSTRAINT `tag_descriptor_has_type` FOREIGN KEY (`tag_type_id_fk`)
    REFERENCES `attributes_types`(`attr_type_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION;

ALTER TABLE `tag_values` ADD
  CONSTRAINT `tag_has_descriptor` FOREIGN KEY (`tag_id_fk`)
    REFERENCES `tags`(`tag_id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION;

ALTER TABLE `tag_values` ADD
  CONSTRAINT `tag_relates_to_object` FOREIGN KEY (`object_id_fk`)
    REFERENCES `objects`(`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION;

SET FOREIGN_KEY_CHECKS=1;

-- Drop View all_objects_props_values
drop view if exists `all_objects_props_values`;

create view `all_objects_props_values` as
select a.name as propname ,o.name as object_name ,ot.name as type_name, v.value_id as val_id , o.id as object_id, ot.objtype_id as type_id,
case
when vv.value is not null then vv.value
when vi.value is not null then vi.value
when vd.value is not null then trim(both '0' from cast(vd.value as char))
when vdt.value is not null then vdt.value
when vl.value is not null then vl.value
when vol.object_id is not null then concat(vol.object_type,':',vol.object_id)
when vpl.object_id is not null then concat(vpl.object_type,':',vpl.object_id,':',vpl.property_name)
else null
end as val
from
objects o inner join object_types ot on (ot.objtype_id = o.object_type_id_fk )
left outer join obj_values v on (o.id = v.object_id_fk)
left outer  join attributes a on (a.attr_id = v.attr_id_fk)
left outer join  valuev vv on (v.value_id = vv.value_id_fk)
left outer join  valuei vi on (v.value_id = vi.value_id_fk)
left outer join  valued vd on (v.value_id = vd.value_id_fk)
left outer join  valuedt vdt on (v.value_id = vdt.value_id_fk)
left outer join  valuel vl on (v.value_id = vl.value_id_fk)
left outer join  value_obj_link vol on (v.value_id = vol.value_id_fk)
left outer join  value_property_link vpl on (v.value_id = vpl.value_id_fk)
where
o.static = 0
;

-- Drop View all_objects
drop view if exists `all_objects`;

create view `all_objects` as
select o.id  as id, t.name as name , 1 as generic
from objects o , object_types t
where o.object_type_id_fk= t.objtype_id

;


-- Drop Event clear_expired_data
drop event if exists `clear_expired_data`;


delimiter |
create event `clear_expired_data`
  on schedule every '1' month starts '2009-10-01 00:01:00'
  enable
do
begin
  delete from tbl_nht_info where report_timestamp <= date_sub(curdate(),interval 1 month);
  delete from tbl_network_statnode where nwk_timestamp <= date_sub(curdate(),interval 3 month);
  delete from tbl_activity_log where `timestamp` <= date_sub(curdate(),interval 18 month);
  delete from tbl_network_statbase where nwk_timestamp <= date_sub(curdate(),interval 3 month);
end; |
delimiter ;

