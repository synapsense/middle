@echo off

cd /D "%~d0""%~p0"

set silent=%1

if "%silent%" == "Yes" goto start

SET /P ANSWER=[You are about to initialize the system database.  Type 'Yes' to continue.]
if not "%ANSWER%" == "Yes" goto done

:start
SET password=dbuser

echo Checking if default password is enabled...
:check_password
  mysql --user=root -p%password% -e "SELECT VERSION();"
  if ERRORLEVEL 1 (
    set /P password=Enter password:
    goto check_password
  )

if defined SS_DEV_MODE (
  rem [BC 12/2014] We use a pre-configured my.ini during development
  echo Skiping db_config.vbs in dev mode ...
) else (
  echo Enabling historical data archiving ...
    SET ERROR_MESSAGE=Failed to enable archiving!
    cscript db_config.vbs
    if ERRORLEVEL 1 goto error
  echo Historical data archiving is enabled
)

echo Creating database schema ...
  SET ERROR_MESSAGE=File create_db.sql does not exist!
  if not exist create_db.sql goto error

  SET ERROR_MESSAGE=Error occurred while creating database schema!
  mysql --user=root -p%password% < create_db.sql
  if ERRORLEVEL 1 goto error
echo Database schema is created

echo Seeding database ...
  SET ERROR_MESSAGE=File seed_db.sql does not exist!
  if not exist seed_db.sql goto error

  SET ERROR_MESSAGE=Failed to load default configuration data!
  mysql --user=root -p%password% < seed_db.sql
  if ERRORLEVEL 1 goto error
echo Database is seeded

echo Enabling partitioning ...
  SET ERROR_MESSAGE=File partition_historical_values.sql does not exist!
  if not exist partition_historical_values.sql goto error

  SET ERROR_MESSAGE=Error occurred during partitioning script execution!
  mysql --user=root -p%password% < partition_historical_values.sql
  if ERRORLEVEL 1 goto error
echo Partitioning is enabled.

echo The system database initialization was completed successfully
if not "%silent%" == "Yes" pause
exit /B

:error
echo ...
echo The system database initialization was not completed due to errors.
echo Details: %ERROR_MESSAGE%
pause
exit /B

:done
