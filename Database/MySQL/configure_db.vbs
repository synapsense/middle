Function Configure	
	Dim FSO, MysqlHomeDir, MyIniFileName, IsDbUpgrade
	Dim processArch

	' For use out of installer runtime
	' MysqlHomeDir = WshShell.CurrentDirectory
	CustomActionArray = Split(Session.Property("CustomActionData"), ";", 3)
	MysqlHomeDir = NormalizePath(CustomActionArray(0))
	MysqlDataDir = NormalizePath(CustomActionArray(1))
	IsDbUpgrade = CustomActionArray(2)
	MyIniFileName = MysqlHomeDir & "my.ini"

	'* =========================
	'* Prepare datadir
	'* =========================

	If IsDbUpgrade = "0" Then
		Set fso = CreateObject("Scripting.FileSystemObject")
		fso.CopyFolder MysqlHomeDir & "default_data\\mysql", MysqlDataDir, TRUE 
		fso.CopyFolder MysqlHomeDir & "default_data\\performance_schema", MysqlDataDir, TRUE 
	End If

	'* =========================
	'* Definition of innodb_buffer_pool_size and innodb_log_file_size(depends on RAM size)
	'* =========================
	Set objWMIService = GetObject("WinMgmts:{impersonationLevel=impersonate}!\\.\root\cimv2")

	Dim objComputer, colComputer
	Set colComputer = objWMIService.ExecQuery ("Select * from Win32_ComputerSystem")
	For Each objComputer in colComputer
		totalRAM = int((objComputer.TotalPhysicalMemory)/(1024*1024))
	Next

	bufferPoolSize=int((totalRAM) /4)
	bufferPoolSizeMg = bufferPoolSize & "M"
	WriteINIString "mysqld", "innodb_buffer_pool_size", bufferPoolSizeMg, MyIniFileName

	logFileSize=int(bufferPoolSize/4)
	logFileSizeMg = logFileSize & "M"
	WriteINIString "mysqld", "innodb_log_file_size", logFileSizeMg, MyIniFileName
End Function

Function Rollback
	Dim FSO, MysqlHomeDir, MyIniFileName, IsDbUpgrade

	' For use out of installer runtime
	' MysqlHomeDir = WshShell.CurrentDirectory
	CustomActionArray = Split(Session.Property("CustomActionData"), ";", 3)
	MysqlHomeDir = CustomActionArray(0)
	MysqlDataDir = CustomActionArray(1)
	IsDbUpgrade = CustomActionArray(2)
	'* =========================
	'* Remove datadir
	'* =========================

	If IsDbUpgrade = "0" Then
		Set fso = CreateObject("Scripting.FileSystemObject")
		fso.DeleteFolder MysqlDataDir & "\mysql", TRUE 
		fso.DeleteFolder MysqlDataDir & "\performance_schema", TRUE 
		fso.DeleteFile MysqlDataDir & "\*", TRUE 
	End If

End Function
Function QuoteAndEscape(ByVal Str)
	escapedStr = Replace(Str, "\", "\\")
	QuoteAndEscape = chr(34) & escapedStr & chr(34)
End Function

Function NormalizePath(ByVal Str)
	If Right(Str, 1) = "\" Then
		NormalizePath = Str
	Else 
		NormalizePath = Str & "\"
	End If
End Function

Sub WriteINIString(Section, KeyName, Value, FileName)
  Dim INIContents, PosSection, PosEndSection
  
  'Get contents of the INI file As a string
  INIContents = ReadFile(FileName)

  'Find section
  PosSection = InStr(1, INIContents, "[" & Section & "]", vbTextCompare)
  If PosSection>0 Then
    'Section exists. Find end of section
    PosEndSection = InStr(PosSection, INIContents, vbCrLf & "[")
    '?Is this last section?
    If PosEndSection = 0 Then PosEndSection = Len(INIContents)+1
    
    'Separate section contents
    Dim OldsContents, NewsContents, Line
    Dim sKeyName, Found
    OldsContents = Mid(INIContents, PosSection, PosEndSection - PosSection)
    OldsContents = split(OldsContents, vbCrLf)

    'Temp variable To find a Key
    sKeyName = LCase(KeyName & "=")

    'Enumerate section lines
    For Each Line In OldsContents
      If LCase(Left(Line, Len(sKeyName))) = sKeyName Then
        Line = KeyName & "=" & Value
        Found = True
      End If
      NewsContents = NewsContents & Line & vbCrLf
    Next

    If isempty(Found) Then
      'key Not found - add it at the end of section
      NewsContents = NewsContents & KeyName & "=" & Value
    Else
      'remove last vbCrLf - the vbCrLf is at PosEndSection
      NewsContents = Left(NewsContents, Len(NewsContents) - 2)
    End If

    'Combine pre-section, new section And post-section data.
    INIContents = Left(INIContents, PosSection-1) & _
      NewsContents & Mid(INIContents, PosEndSection)
  else'if PosSection>0 Then
    'Section Not found. Add section data at the end of file contents.
    If Right(INIContents, 2) <> vbCrLf And Len(INIContents)>0 Then 
      INIContents = INIContents & vbCrLf 
    End If
    INIContents = INIContents & "[" & Section & "]" & vbCrLf & _
      KeyName & "=" & Value
  end if'if PosSection>0 Then
  WriteFile FileName, INIContents
End Sub

Sub UnzipData(pathToZipFile, extractTo)
    Set fso = CreateObject("Scripting.FileSystemObject")
	' Check again if the target folder already contains MySQL data files in it
	If Not fso.FileExists(extractTo & "\mysql\db.MYI") then
		If Not fso.FolderExists(extractTo) then fso.CreateFolder(extractTo)
		set objShell = CreateObject("Shell.Application")
		set filesInzip = objShell.NameSpace(pathToZipFile).items()
		objShell.NameSpace(extractTo).CopyHere(filesInzip)
		Set objShell = Nothing
	End If	
	Set fso = Nothing
End Sub 'UnzipData

'File functions
Function ReadFile(ByVal FileName)
  Dim FS: Set FS = CreateObject("Scripting.FileSystemObject")
  'Go To windows folder If full path Not specified.
  If InStr(FileName, ":\") = 0 And Left (FileName,2)<>"\\" Then 
    FileName = FS.GetSpecialFolder(0) & "\" & FileName
  End If
  On Error Resume Next

  ReadFile = FS.OpenTextFile(FileName).ReadAll
End Function

Function WriteFile(ByVal FileName, ByVal Contents)
  
  Dim FS: Set FS = CreateObject("Scripting.FileSystemObject")
  'On Error Resume Next

  'Go To windows folder If full path Not specified.
  If InStr(FileName, ":\") = 0 And Left (FileName,2)<>"\\" Then 
    FileName = FS.GetSpecialFolder(0) & "\" & FileName
  End If

  Dim OutStream: Set OutStream = FS.OpenTextFile(FileName, 2, True)
  OutStream.Write Contents
  OutStream.Close 	
End Function