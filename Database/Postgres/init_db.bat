@ECHO off

SET /P ANSWER=[You are about to initialize the system database.Would you like to continue?(y/n)]
if not "%ANSWER%" == "y" goto exit

if exist "%APPDATA%\postgresql\pgpass.conf" goto check_database

@echo To do all init process automatically ,
@echo you should put file pgpass.conf to "%APPDATA%/postgresql" directory manually before running script 
@echo File should contain string "<host:port:database:user:password>"

SET /P ANSWER=[Do you want to create password file manually and run db init script later?(y/n)]
if not "%ANSWER%" == "n" goto exit

@echo Used PostgreSQL user should have CREATE DATABASE privilege!
SET /P ANSWER=[Enter connection string ("<host>:<port>:*:<user>")]
SET /P PASSWORD=[Enter password]
SET ANSWER=%ANSWER%:%PASSWORD%

mkdir "%APPDATA%\postgresql"
echo %ANSWER%>"%APPDATA%\postgresql\pgpass.conf"

:check_database
psql -U postgres -l 2>&1 | findstr /I synap > nul
if errorlevel == 1 goto create_database else goto remove_database

:remove_database
SET /P ANSWER=[Database "synap" already exists!Overwrite?(y/n)] 
if not "%ANSWER%" == "y" goto exit

@echo JIC stopping scheduler service if exists and started
net stop pgAgent

@echo Removing database schema
dropdb  -U postgres synap > nul
rem @IF errorlevel 1 goto db_error

:create_database
@echo Creating new clear database schema
createdb -U postgres synap > nul
@IF errorlevel 1  goto db_error

@echo Creating plpgsql language in synap schema
createlang -U postgres -d synap plpgsql
@IF errorlevel 1  goto db_error

@echo Event scheduler setup
call init_scheduler.bat postgres %PASSWORD%

psql -U postgres -d synap -f create_db.sql 

psql -U postgres -d synap -f seed_db.sql

goto exit



:db_error
echo PostgreSQL returns error
goto exit

:db_exists
echo Database synap alredy exists! 


goto exit


:exit
@echo Exiting...
exit