rem Create pgagent schema tables
psql -U postgres -d synap -f pgagent.sql

rem Install scheduler tool as windows service
"c:\Program Files\PostgreSQL\8.3\bin\pgAgent" REMOVE pgAgent
"c:\Program Files\PostgreSQL\8.3\bin\pgAgent" INSTALL pgAgent -u %1 -p %2 hostaddr=127.0.0.1 dbname=synap user=%1 password=%2

rem start scheduler service 
net start pgAgent

