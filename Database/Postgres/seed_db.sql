-- =============================================================================
-- Create syanpsense db user and grant appropriate permissions
-- =============================================================================
-- grant select, insert, update, delete on Synap.* to 'synapsense'@'%' identified by 'synap$ense';

-- Uncomment the line below to enable SSL security
-- grant select, insert, update, delete on SynapDB.* to 'synapsense'@'%' identified by 'synap$ense' REQUIRE SSL;

-- =============================================================================
-- erase net stat
TRUNCATE TABLE TBL_NHT_INFO CASCADE;
TRUNCATE TABLE TBL_NETWORK_STATBASE CASCADE;
TRUNCATE TABLE TBL_NETWORK_STATNODE CASCADE;

-- =============================================================================
-- erase generic part of db
TRUNCATE TABLE valuei CASCADE;
TRUNCATE TABLE valuel CASCADE;
TRUNCATE TABLE valued CASCADE;
TRUNCATE TABLE valuedt CASCADE;
TRUNCATE TABLE valuev CASCADE;
TRUNCATE TABLE value_binary CASCADE;
TRUNCATE TABLE VALUE_OBJ_LINK CASCADE;
TRUNCATE TABLE VALUE_PROPERTY_LINK CASCADE;
TRUNCATE TABLE obj_values CASCADE;
TRUNCATE TABLE historical_values CASCADE;
TRUNCATE TABLE attributes CASCADE;
TRUNCATE TABLE attributes_types CASCADE;
TRUNCATE TABLE object_parents CASCADE;
TRUNCATE TABLE objects CASCADE;
TRUNCATE TABLE object_types CASCADE;

-- =============================================================================
-- erase tables related  to RE
TRUNCATE TABLE TBL_RULE_INSTANCES CASCADE;
TRUNCATE TABLE TBL_RULE_TYPES CASCADE;
TRUNCATE TABLE tbl_rule_property_dest_assoc CASCADE;
TRUNCATE TABLE tbl_rule_property_src_assoc CASCADE;
TRUNCATE TABLE tbl_rule_instance_constants CASCADE;

-- =============================================================================
-- performers and dest
TRUNCATE TABLE TBL_ALERT_TYPE_ACTION_DEST_ASSOC CASCADE;
TRUNCATE TABLE TBL_PERFORMERS CASCADE;
TRUNCATE TABLE tbl_destinations CASCADE;

-- =============================================================================
-- erase alerts
TRUNCATE TABLE TBL_ALERT_TYPE CASCADE;
TRUNCATE TABLE TBL_ALERT_PRIORITY CASCADE;
TRUNCATE TABLE TBL_ALERT CASCADE;
TRUNCATE TABLE TBL_ALERT_TO_OBJECTS_MAP CASCADE;
TRUNCATE TABLE TBL_ALERT_TO_TOPOLOGY_MAP CASCADE;
TRUNCATE TABLE TBL_ALERT_TO_RULES_MAP CASCADE;

-- =============================================================================
-- erase users
TRUNCATE TABLE TBL_USERS CASCADE;
TRUNCATE TABLE Roles CASCADE;
TRUNCATE TABLE TBL_USER_PREFERENCES CASCADE;
TRUNCATE TABLE TBL_USER_PREFERENCES_ASSOC CASCADE;

-- =============================================================================
-- erase users
TRUNCATE TABLE TBL_VISUALIZATION_IMAGES CASCADE;
TRUNCATE TABLE TBL_VISUALIZATION_SERVERS CASCADE;
TRUNCATE TABLE tbl_visualization_legend_point CASCADE;
TRUNCATE TABLE tbl_visualization_legend CASCADE;
truncate table TBL_VISUALIZATION_LEGEND_IMAGE CASCADE;

-- =============================================================================
-- erase activity log
truncate table tbl_activity_log CASCADE;

-- =============================================================================
-- Add rows to the user table
INSERT INTO Roles VALUES (1,'admin','Administrator');
INSERT INTO Roles VALUES (2,'user','User');
INSERT INTO Roles VALUES (3,'engineer','Engineer');
select setval('role_id_seq',3);
INSERT INTO TBL_USERS (user_id,user_name, user_password,user_password_hint,user_password_hint_answer,role_id_fk,f_name, l_name,middle_initial,title,primary_phonenumber,secondary_phonenumber,primary_email,secondary_email,description) VALUES (1,'admin',SHA1('admin'),'', '', 1,'Admin','Admin', '', 'Administrator','','','','', 'Administrator');
INSERT INTO TBL_USERS (user_id,user_name, user_password,user_password_hint,user_password_hint_answer,role_id_fk,f_name, l_name,middle_initial,title,primary_phonenumber,secondary_phonenumber,primary_email,secondary_email,description) VALUES (2,'synapsense',SHA1('synap$ense'),'', '', 2,'Synapsense','Synapsense', '', 'Synapsense','','','','', 'Synapsense');
select setval('user_id_seq',2);

-- =============================================================================
-- supported performers
-- INSERT INTO TBL_DESTINATIONS (dest_id,dest_name,version) values (1,'shabanov@mera.ru',0);
-- =============================================================================
-- supported performers
INSERT INTO TBL_PERFORMERS (performer_id,performer_name) values (1,'DBSpooler');
INSERT INTO TBL_PERFORMERS (performer_id,performer_name) values (2,'SMTPMailer');
INSERT INTO TBL_PERFORMERS (performer_id,performer_name) values (3,'AgentMessenger');
INSERT INTO TBL_PERFORMERS (performer_id,performer_name) values (4,'JMXNotificationSender');
INSERT INTO TBL_PERFORMERS (performer_id,performer_name) values (5,'SNMPTrapPerformer');
select setval('performer_id_seq',5);

-- =============================================================================
-- alerts type init
INSERT INTO TBL_ALERT_PRIORITY (alert_priority_id,name,interval,range_begin,range_end) values (1,'LOW',60*60*1000,68,99);
INSERT INTO TBL_ALERT_PRIORITY (alert_priority_id,name,interval,range_begin,range_end) values (2,'MIDDLE',30*60*1000,33,67);
INSERT INTO TBL_ALERT_PRIORITY (alert_priority_id,name,interval,range_begin,range_end) values (3,'HIGH',15*60*1000,0,32);
select setval('alert_priority_id_seq',3);

-- =============================================================================
-- alerts type init
INSERT INTO TBL_ALERT_TYPE (alert_type_id,name,description,priority,header,body,is_active) values (1,'UNKNOWN','Generic alert, any alert with non-existing type is saved as UNKNOWN',33,'UNKNOWN ALERT TYPE','Errors: $message$',true);
INSERT INTO TBL_ALERT_TYPE (alert_type_id,name,description,priority,header,body,is_active) values (2,'NODE_STATUS','Alert that is generated when event from node has arrived',68,'UNKNOWN ALERT TYPE','Node status : $message$',false);
INSERT INTO TBL_ALERT_TYPE (alert_type_id,name,description,priority,header,body,is_active) values (3,'WSN_CONFIGURATION','Alert about wrong Wireless Sensor Network configuration',0,'WSN_CONFIGURATION','$name$: $message$',true);
INSERT INTO TBL_ALERT_TYPE (alert_type_id,name,description,priority,header,body,is_active) values (4,'DAL_ERROR','All DAL errors',1,'DAL','DAL error : $message$',true);
INSERT INTO TBL_ALERT_TYPE (alert_type_id,name,description,priority,header,body,is_active) values (5,'Audit Alert','This alert is triggered when communication between any two system components is lost.',0,'System Communication Failure','Audit alert: $message$',true);
INSERT INTO TBL_ALERT_TYPE (alert_type_id,name,description,priority,header,body,is_active) values (6,'Sensor disconnected','Triggers when sensor is disconnected from the node',33,'Sensor disconnected','$message$',false);
INSERT INTO TBL_ALERT_TYPE (alert_type_id,name,description,priority,header,body,is_active) values (7,'Sensor requires service','Triggers when sensor requires service',33,'Sensor requires service','$message$',false);
INSERT INTO TBL_ALERT_TYPE (alert_type_id,name,description,priority,header,body,is_active) values (8,'Node not reporting','Triggers when node doesn''t send data',33,'Node not reporting','$message$',false);
INSERT INTO TBL_ALERT_TYPE (alert_type_id,name,description,priority,header,body,is_active) values (9,'Out of recommended range','Triggers when rack cold aisle temperature goes out of configured recommended range',33,'Out of recommended range','$message$',false);
INSERT INTO TBL_ALERT_TYPE (alert_type_id,name,description,priority,header,body,is_active) values (10,'Out of allowable range','Triggers when rack cold aisle temperature goes out of configured allowable range',0,'Out of allowable range','$message$',true);
INSERT INTO TBL_ALERT_TYPE (alert_type_id,name,description,priority,header,body,is_active) values (11,'Low battery','Triggers when node remaining days forecast is below 100 days',0,'Low battery alert','$message$',true);
INSERT INTO TBL_ALERT_TYPE (alert_type_id,name,description,priority,header,body,is_active) VALUES (12,'Circuit Current above threshold','Circuit Current above threshold',68,'Circuit Current above threshold','$message$',false);
INSERT INTO TBL_ALERT_TYPE (alert_type_id,name,description,priority,header,body,is_active) VALUES (13,'Circuit Power above threshold','Circuit Power above threshold',68,'Circuit Power above threshold','$message$',false);
INSERT INTO TBL_ALERT_TYPE (alert_type_id,name,description,priority,header,body,is_active) VALUES (14,'Circuit Power Factor below threshold','Circuit Power Factor below threshold',68,'Circuit Power Factor below threshold','$message$',false);
INSERT INTO TBL_ALERT_TYPE (alert_type_id,name,description,priority,header,body,is_active) values (15,'Communication error','Triggers when one or more data points can not be acquired',0,'Communication error','$message$',true);
select setval('alert_type_id_seq',15);

-- =============================================================================
-- rule types init
INSERT INTO tbl_rule_types(rule_type_id,name,source_type,source,dsl,precompiled,type,rule_scheduler_config) VALUES (1,'Sensor disconnected','drools',E'package ConditionalAlerts;\n\nimport com.synapsense.dre.domainobjects.*;\nimport com.synapsense.dre.service.DRulesEngineService;\nimport com.synapsense.dre.service.Trigger;\n\nglobal java.util.concurrent.ConcurrentHashMap triggers;\n\nrule \"Sensor disconnected\" \n	 when\n		$obj0 : SENSOR( $prop0 : lastValue== -3000 )\n		$obj1 : NODE( nodeLogicalId == $obj0.nodeId )\n\n	 then\n	 Trigger t = new Trigger(\"Sensor disconnected\");\n     t.addObject(0,$obj0);\n     t.addObject(1,$obj1);\n     if(triggers.putIfAbsent(t.getCompositeId(), t) == null){ \n     insert(t);\n     DRulesEngineService.sendAlert(\"Sensor disconnected\", \"Sensor disconnected\", \"NODE[name:\" +  $obj1.getName() + \"]. SENSOR[name:\" + $obj0.getName() + \"].\",  null, $obj0.getNativeTO(), t, 0);\n     }\nend\nrule \"anti_Sensor disconnected\" \n	 when\n		$t : Trigger( ruleId == \"Sensor disconnected\" )\n	    $obj0 : SENSOR(eval ( serializedTO  == $t.getObject(0)), $prop0 : lastValue != -3000 )\n	    $obj1 : NODE( eval ( serializedTO  == $t.getObject(1)))\n\n	 then\nTrigger t = (Trigger)triggers.remove($t.getCompositeId());\nif(t != null) retract(t);\nend',NULL,false,2,'0 0/5 * * * ?');
INSERT INTO tbl_rule_types(rule_type_id,name,source_type,source,dsl,precompiled,type,rule_scheduler_config) VALUES (2,'Sensor requires service','drools',E'package ConditionalAlerts;\n\nimport com.synapsense.dre.domainobjects.*;\nimport com.synapsense.dre.service.DRulesEngineService;\nimport com.synapsense.dre.service.Trigger;\n\nglobal java.util.concurrent.ConcurrentHashMap triggers;\n\nrule \"Sensor requires service\"\n    when\n        $obj0 : SENSOR( $prop0 : lastValue == -2000)\n        $obj1 : NODE( nodeLogicalId == $obj0.nodeId)\n    then\n        Trigger t = new Trigger(\"Sensor requires service\");\n        t.addObject(0,$obj0);\n        t.addObject(1,$obj1);\n        if(triggers.putIfAbsent(t.getCompositeId(), t) == null){ \n        insert(t);\n        DRulesEngineService.sendAlert(\"Sensor requires service\", \"Sensor requires service\", \"NODE[name:\" +  $obj1.getName() + \"]. SENSOR[name:\" + $obj0.getName() + \"].\",  null, $obj0.getNativeTO(), t, 0);\n        }\nend\nrule \"anti_Sensor requires service\" \n	 when\n		$t : Trigger( ruleId == \"Sensor requires service\" )\n	    $obj0 : SENSOR(eval ( serializedTO  == $t.getObject(0)), $prop0 : lastValue != -2000 )\n	    $obj1 : NODE( eval ( serializedTO  == $t.getObject(1))\n )\n\n	 then\nTrigger t = (Trigger)triggers.remove($t.getCompositeId());\nif(t != null) retract(t);\nend',NULL,false,2,'0 0/5 * * * ?');
INSERT INTO tbl_rule_types(rule_type_id,name,source_type,source,dsl,precompiled,type,rule_scheduler_config) VALUES (3,'Node not reporting','drools',E'package ConditionalAlerts;\n\nimport com.synapsense.dre.domainobjects.*;\nimport com.synapsense.dre.service.DRulesEngineService;\nimport com.synapsense.dre.service.Trigger;\n\nglobal java.util.concurrent.ConcurrentHashMap triggers;\n\nrule \"Node not reporting\"\n    when\n        ($obj0:NODE(status == false))\n\n    then\n     Trigger t = new Trigger(\"Node not reporting\");\n     t.addObject(0,$obj0);\n     if(triggers.putIfAbsent(t.getCompositeId(), t) == null){ \n     insert(t);\n     String l = $obj0.getLocation();\n     String loc = (l == null || \"\".equals(l)) ? \"\" : (\", location:\" + l);\n     DRulesEngineService.sendAlert(\"Node not reporting\", \"Node not reporting\", \"NODE[name:\" + $obj0.getName() + loc + \"].\",  null, $obj0.getNativeTO(), t, 0);\n	}\nend\nrule \"anti_Node not reporting\" \n	 when\n		$t : Trigger( ruleId == \"Node not reporting\" )\n	    $obj0 : NODE(eval ( serializedTO  == $t.getObject(0)), status == true )\n\n	 then\nTrigger t = (Trigger)triggers.remove($t.getCompositeId());\nif(t != null) retract(t);\nend',NULL,false,2,'0 0/5 * * * ?');
INSERT INTO tbl_rule_types(rule_type_id,name,source_type,source,dsl,precompiled,type,rule_scheduler_config) VALUES (4,'Out of recommended range','drools',E'package ConditionalAlerts;\n\nimport com.synapsense.dre.domainobjects.*;\nimport com.synapsense.dre.service.DRulesEngineService;\nimport com.synapsense.dre.service.Trigger;\n\nglobal java.util.concurrent.ConcurrentHashMap triggers;\nglobal java.text.NumberFormat numberFormatter;\n\nrule \"Out of recommended range\"\n    when\n        $obj0:RACK() && eval(\n                              (DRulesEngineService.readDouble($obj0.getCTop(), \"lastValue\") > DRulesEngineService.readDouble($obj0.getCTop(), \"RMax\"))  ||\n                              (DRulesEngineService.readDouble($obj0.getCMid(), \"lastValue\") > DRulesEngineService.readDouble($obj0.getCMid(), \"RMax\"))  ||\n                              (DRulesEngineService.readDouble($obj0.getCBot(), \"lastValue\") >  DRulesEngineService.readDouble($obj0.getCBot(), \"RMax\")) ||\n                              ((DRulesEngineService.readDouble($obj0.getCBot(), \"lastValue\") < DRulesEngineService.readDouble($obj0.getCBot(), \"RMin\"))\n                                && (DRulesEngineService.readDouble($obj0.getCBot(), \"lastValue\") > -1000.0)) ||\n                              ((DRulesEngineService.readDouble($obj0.getCMid(), \"lastValue\") < DRulesEngineService.readDouble($obj0.getCMid(), \"RMin\"))\n                                && (DRulesEngineService.readDouble($obj0.getCMid(), \"lastValue\") > -1000.0)) ||\n                              ((DRulesEngineService.readDouble($obj0.getCTop(), \"lastValue\") < DRulesEngineService.readDouble($obj0.getCTop(), \"RMin\"))\n                                && (DRulesEngineService.readDouble($obj0.getCTop(), \"lastValue\") > -1000.0))\n                             )\n    then\n	    RACK rack = $obj0;\n        Trigger t = new Trigger(\"Out of recommended range\");\n        t.addObject(0,$obj0);\n        if(triggers.putIfAbsent(t.getCompositeId(), t) == null){ \n        insert(t);\n        DRulesEngineService.sendAlert(\"Out of recommended range\", \"Out of recommended range\",\n                                      rack.getName()  + \" intake temp is Out of recommended range.\" + \n                                      \"RACK[cTop:\" + numberFormatter.format(DRulesEngineService.readDouble(rack.getCTop(), \"lastValue\")) + \"F, cMid:\" + numberFormatter.format(DRulesEngineService.readDouble(rack.getCMid(), \"lastValue\")) + \"F, cBot:\" + numberFormatter.format(DRulesEngineService.readDouble(rack.getCBot(), \"lastValue\")) + \"F].\",\n                                      null, rack.getNativeTO(), t, 0);\n       }\nend\nrule \"anti_Out of recommended range\" \n	 when\n		$t : Trigger( ruleId == \"Out of recommended range\" )\n		not($obj0:RACK(eval ( serializedTO  == $t.getObject(0))) && eval(\n                              (DRulesEngineService.readDouble($obj0.getCTop(), \"lastValue\") > DRulesEngineService.readDouble($obj0.getCTop(), \"RMax\"))  ||\n                              (DRulesEngineService.readDouble($obj0.getCMid(), \"lastValue\") > DRulesEngineService.readDouble($obj0.getCMid(), \"RMax\"))  ||\n                              (DRulesEngineService.readDouble($obj0.getCBot(), \"lastValue\") >  DRulesEngineService.readDouble($obj0.getCBot(), \"RMax\")) ||\n                              ((DRulesEngineService.readDouble($obj0.getCBot(), \"lastValue\") < DRulesEngineService.readDouble($obj0.getCBot(), \"RMin\"))\n                                && (DRulesEngineService.readDouble($obj0.getCBot(), \"lastValue\") > -1000.0)) ||\n                              ((DRulesEngineService.readDouble($obj0.getCMid(), \"lastValue\") < DRulesEngineService.readDouble($obj0.getCMid(), \"RMin\"))\n                                && (DRulesEngineService.readDouble($obj0.getCMid(), \"lastValue\") > -1000.0)) ||\n                              ((DRulesEngineService.readDouble($obj0.getCTop(), \"lastValue\") < DRulesEngineService.readDouble($obj0.getCTop(), \"RMin\"))\n                                && (DRulesEngineService.readDouble($obj0.getCTop(), \"lastValue\") > -1000.0))\n                             )\n)\n	 then\nTrigger t = (Trigger)triggers.remove($t.getCompositeId());\nif(t != null) retract(t);\nend',NULL,false,2,'0 0/5 * * * ?');
INSERT INTO tbl_rule_types(rule_type_id,name,source_type,source,dsl,precompiled,type,rule_scheduler_config) VALUES (5,'Out of allowable range','drools',E'package ConditionalAlerts;\n\nimport com.synapsense.dre.domainobjects.*;\nimport com.synapsense.dre.service.DRulesEngineService;\nimport com.synapsense.dre.service.Trigger;\n\nglobal java.util.concurrent.ConcurrentHashMap triggers;\nglobal java.text.NumberFormat numberFormatter;\n\nrule \"Out of allowable range\"\n    when\n        $obj0:RACK() && eval(\n                              (DRulesEngineService.readDouble($obj0.getCTop(), \"lastValue\") > DRulesEngineService.readDouble($obj0.getCTop(), \"AMax\"))  ||\n                              (DRulesEngineService.readDouble($obj0.getCMid(), \"lastValue\") > DRulesEngineService.readDouble($obj0.getCMid(), \"AMax\"))  ||\n                              (DRulesEngineService.readDouble($obj0.getCBot(), \"lastValue\") >  DRulesEngineService.readDouble($obj0.getCBot(), \"AMax\")) ||\n                              ((DRulesEngineService.readDouble($obj0.getCBot(), \"lastValue\") < DRulesEngineService.readDouble($obj0.getCBot(), \"AMin\"))\n                                && (DRulesEngineService.readDouble($obj0.getCBot(), \"lastValue\") > -1000.0)) ||\n                              ((DRulesEngineService.readDouble($obj0.getCMid(), \"lastValue\") < DRulesEngineService.readDouble($obj0.getCMid(), \"AMin\"))\n                                && (DRulesEngineService.readDouble($obj0.getCMid(), \"lastValue\") > -1000.0)) ||\n                              ((DRulesEngineService.readDouble($obj0.getCTop(), \"lastValue\") < DRulesEngineService.readDouble($obj0.getCTop(), \"AMin\"))\n                                && (DRulesEngineService.readDouble($obj0.getCTop(), \"lastValue\") > -1000.0))\n                             )\n    then\n        RACK rack = $obj0;\n        Trigger t = new Trigger(\"Out of allowable range\");\n        t.addObject(0,$obj0);\n        if(triggers.putIfAbsent(t.getCompositeId(), t) == null){ \n        insert(t);\n        DRulesEngineService.sendAlert(\"Out of allowable range\", \"Out of allowable range\",\n                                      rack.getName()  + \" intake temp is Out of allowable range.\" + \n                                      \"RACK[cTop:\" + numberFormatter.format(DRulesEngineService.readDouble(rack.getCTop(), \"lastValue\")) + \"F, cMid:\" + numberFormatter.format(DRulesEngineService.readDouble(rack.getCMid(), \"lastValue\")) + \"F, cBot:\" + numberFormatter.format(DRulesEngineService.readDouble(rack.getCBot(), \"lastValue\")) + \"F].\",\n                                      null, rack.getNativeTO(), t, 0);\n        }\n        \nend\nrule \"anti_Out of allowable range\" \n	 when\n		$t : Trigger( ruleId == \"Out of allowable range\" )\n		not($obj0:RACK(eval ( serializedTO  == $t.getObject(0))) && eval(\n                              (DRulesEngineService.readDouble($obj0.getCTop(), \"lastValue\") > DRulesEngineService.readDouble($obj0.getCTop(), \"AMax\"))  ||\n                              (DRulesEngineService.readDouble($obj0.getCMid(), \"lastValue\") > DRulesEngineService.readDouble($obj0.getCMid(), \"AMax\"))  ||\n                              (DRulesEngineService.readDouble($obj0.getCBot(), \"lastValue\") >  DRulesEngineService.readDouble($obj0.getCBot(), \"AMax\")) ||\n                              ((DRulesEngineService.readDouble($obj0.getCBot(), \"lastValue\") < DRulesEngineService.readDouble($obj0.getCBot(), \"AMin\"))\n                                && (DRulesEngineService.readDouble($obj0.getCBot(), \"lastValue\") > -1000.0)) ||\n                              ((DRulesEngineService.readDouble($obj0.getCMid(), \"lastValue\") < DRulesEngineService.readDouble($obj0.getCMid(), \"AMin\"))\n                                && (DRulesEngineService.readDouble($obj0.getCMid(), \"lastValue\") > -1000.0)) ||\n                              ((DRulesEngineService.readDouble($obj0.getCTop(), \"lastValue\") < DRulesEngineService.readDouble($obj0.getCTop(), \"AMin\"))\n                                && (DRulesEngineService.readDouble($obj0.getCTop(), \"lastValue\") > -1000.0))\n                             )\n)\n	 then\nTrigger t = (Trigger)triggers.remove($t.getCompositeId());\nif(t != null) retract(t);\nend',NULL,false,2,'0 0/5 * * * ?');
INSERT INTO tbl_rule_types(rule_type_id,name,source_type,source,dsl,precompiled,type,rule_scheduler_config) VALUES (6,'Circuit Current above threshold','drools',E'package ConditionalAlerts;\n\nimport com.synapsense.dre.domainobjects.*;\nimport com.synapsense.dre.service.DRulesEngineService;\nimport com.synapsense.dre.service.Trigger;\n\nglobal java.util.concurrent.ConcurrentHashMap triggers;\nglobal java.text.NumberFormat numberFormatter;\n\nrule \"Circuit Current above threshold\"\n    when\n        $obj0:CIRCUIT() && eval((DRulesEngineService.readDouble($obj0.getCurrent(), \"lastValue\")/$obj0.getMaxCurrent() * 100) > $obj0.getMidCurrent())\n    then      \n        Trigger t = new Trigger(\"Circuit Current above threshold\");\n        t.addObject(0,$obj0);\n        if(triggers.putIfAbsent(t.getCompositeId(), t) == null){ \n        insert(t);\n        DRulesEngineService.sendAlert(\"Circuit Current above threshold\", \"Circuit Current above threshold\", $obj0.getName()  + \" current above threshold.\", null, $obj0.getNativeTO(), t, 0);\n        }\nend\nrule \"anti_Circuit Current above threshold\" \n	 when\n		$t : Trigger( ruleId == \"Circuit Current above threshold\" )\n		$obj0:CIRCUIT(eval ( serializedTO  == $t.getObject(0)))\n		not (eval((DRulesEngineService.readDouble($obj0.getCurrent(), \"lastValue\")/$obj0.getMaxCurrent() * 100) > $obj0.getMidCurrent()))\n		\n	 then\nTrigger t = (Trigger)triggers.remove($t.getCompositeId());\nif(t != null) retract(t);\nend',NULL,false,2,'0 0/5 * * * ?');
INSERT INTO tbl_rule_types(rule_type_id,name,source_type,source,dsl,precompiled,type,rule_scheduler_config) VALUES (7,'Circuit Power above threshold','drools',E'package ConditionalAlerts;\n\nimport com.synapsense.dre.domainobjects.*;\nimport com.synapsense.dre.service.DRulesEngineService;\nimport com.synapsense.dre.service.Trigger;\n\nglobal java.util.concurrent.ConcurrentHashMap triggers;\nglobal java.text.NumberFormat numberFormatter;\n\nrule \"Circuit Power above threshold\"\n    when\n        $obj0:CIRCUIT() && eval((DRulesEngineService.readDouble($obj0.getPower(), \"lastValue\")/$obj0.getMaxPower() * 100) > $obj0.getMidPower())\n    then      \n        Trigger t = new Trigger(\"Circuit Power above threshold\");\n        t.addObject(0,$obj0);\n        if(triggers.putIfAbsent(t.getCompositeId(), t) == null){ \n        insert(t);\n        DRulesEngineService.sendAlert(\"Circuit Power above threshold\", \"Circuit Power above threshold\", $obj0.getName()  + \" power above threshold.\", null, $obj0.getNativeTO(), t, 0);\n        }\nend\nrule \"anti_Circuit Power above threshold\" \n	 when\n		$t : Trigger( ruleId == \"Circuit Power above threshold\" )\n		$obj0:CIRCUIT(eval ( serializedTO  == $t.getObject(0)))\n		not (eval((DRulesEngineService.readDouble($obj0.getPower(), \"lastValue\")/$obj0.getMaxPower() * 100) > $obj0.getMidPower()))\n		\n	 then\nTrigger t = (Trigger)triggers.remove($t.getCompositeId());\nif(t != null) retract(t);\nend',NULL,false,2,'0 0/5 * * * ?');
INSERT INTO tbl_rule_types(rule_type_id,name,source_type,source,dsl,precompiled,type,rule_scheduler_config) VALUES (8,'Circuit Power Factor below threshold','drools',E'package ConditionalAlerts;\n\nimport com.synapsense.dre.domainobjects.*;\nimport com.synapsense.dre.service.DRulesEngineService;\nimport com.synapsense.dre.service.Trigger;\n\nglobal java.util.concurrent.ConcurrentHashMap triggers;\nglobal java.text.NumberFormat numberFormatter;\n\nrule \"Circuit Power Factor below threshold\"\n    when\n        $obj0:CIRCUIT() && eval((DRulesEngineService.readDouble($obj0.getPowerFactor(), \"lastValue\")/$obj0.getMaxPowerFactor() * 100) < $obj0.getMidPowerFactor())\n    then      \n        Trigger t = new Trigger(\"Circuit Power Factor below threshold\");\n        t.addObject(0,$obj0);\n        if(triggers.putIfAbsent(t.getCompositeId(), t) == null){ \n        insert(t);\n        DRulesEngineService.sendAlert(\"Circuit Power Factor below threshold\", \"Circuit Power Factor below threshold\", $obj0.getName()  + \" power factor below threshold.\", null, $obj0.getNativeTO(), t, 0);\n        }\nend\nrule \"anti_Circuit Power Factor below threshold\" \n	 when\n		$t : Trigger( ruleId == \"Circuit Power Factor below threshold\" )\n		$obj0:CIRCUIT(eval ( serializedTO  == $t.getObject(0)))\n		not (eval((DRulesEngineService.readDouble($obj0.getPowerFactor(), \"lastValue\")/$obj0.getMaxPowerFactor() * 100) < $obj0.getMidPowerFactor()))\n		\n	 then\nTrigger t = (Trigger)triggers.remove($t.getCompositeId());\nif(t != null) retract(t);\nend',NULL,false,2,'0 0/5 * * * ?');
INSERT INTO tbl_rule_types(rule_type_id,name,source_type,source,dsl,precompiled,type,rule_scheduler_config) VALUES (9,'Autodismiss alerts','drools',E'package ConditionalAlerts\r\n\r\nimport com.synapsense.dre.domainobjects.*;\r\nimport com.synapsense.dre.service.DRulesEngineService.DREHelper;\r\n\r\nglobal DREHelper helper;\r\n\r\nrule \"Autodismiss low priority alerts\"\r\n    when\r\n        $obj : ALERT(status == 1) && eval(helper.isAutoDismissed($obj.getNativeTO(), 68, 99))\r\n    then\r\n     helper.scheduleAlertDismission($obj.getNativeTO(), \"Automatically cleared\", 2592000000L);        \r\nend\r\n\r\nrule \"Autodismiss medium priority alerts\"\r\n    when\r\n        $obj : ALERT(status == 1) && eval(helper.isAutoDismissed($obj.getNativeTO(), 33, 67))\r\n    then\r\n     helper.scheduleAlertDismission($obj.getNativeTO(), \"Automatically cleared\", 1200000L);        \r\nend\r\n\r\nrule \"Autodismiss high priority alerts\"\r\n\r\n    when\r\n        $obj : ALERT(status == 1) && eval(helper.isAutoDismissed($obj.getNativeTO(), 0, 32))\r\n    then\r\n     helper.scheduleAlertDismission($obj.getNativeTO(), \"Automatically cleared\", 600000L);        \r\nend',NULL,false,2,'0 0/5 * * * ?');
select setval('rule_type_id_seq',9);

-- =============================================================================
-- supported property types
INSERT INTO attributes_types (attr_type_id,java_type) VALUES (1,'java.lang.Integer');
INSERT INTO attributes_types (attr_type_id,java_type) VALUES (2,'java.lang.Double');
INSERT INTO attributes_types (attr_type_id,java_type) VALUES (3,'java.lang.String');
INSERT INTO attributes_types (attr_type_id,java_type) VALUES (4,'java.util.Date');
INSERT INTO attributes_types (attr_type_id,java_type) VALUES (5,'com.synapsense.dto.TO');
INSERT INTO attributes_types (attr_type_id,java_type) VALUES (6,'com.synapsense.service.impl.dao.to.PropertyTO');
INSERT INTO attributes_types (attr_type_id,java_type) VALUES (7,'java.lang.Long');
INSERT INTO attributes_types (attr_type_id,java_type) VALUES (8,'com.synapsense.dto.BinaryData');
select setval('attr_type_id_seq',8);

-- =============================================================================
-- predefined type CUSTOMQUERY to avoid EJBAccessException cause createObjectType is accessible only by users with 'admin' role
INSERT INTO  object_types (objtype_id,name) values (1,'CUSTOMQUERY');
select setval('objtype_id_seq',1);

INSERT INTO  objects (id,object_type_id_fk,name,static) values (1,1,'CUSTOMQUERY',true);
select setval('object_id_seq',1);

INSERT INTO  attributes (attr_id,object_type_id_fk,name,attr_type_id_fk) values (1,1,'name',3);
INSERT INTO  attributes (attr_id,object_type_id_fk,name,attr_type_id_fk) values (2,1,'associatedSensors',3);
INSERT INTO  attributes (attr_id,object_type_id_fk,name,attr_type_id_fk) values (3,1,'privateCheck',1);
INSERT INTO  attributes (attr_id,object_type_id_fk,name,attr_type_id_fk) values (4,1,'queryDesc',3);
INSERT INTO  attributes (attr_id,object_type_id_fk,name,attr_type_id_fk) values (5,1,'userId',1);
INSERT INTO  attributes (attr_id,object_type_id_fk,name,attr_type_id_fk) values (6,1,'lines',3);
INSERT INTO  attributes (attr_id,object_type_id_fk,name,attr_type_id_fk) values (7,1,'chartDescr',3);
select setval('attr_id_seq',7);

-- =============================================================================
-- add viz servers
INSERT INTO TBL_VISUALIZATION_SERVERS(id,url) VALUES(1,'http://localhost:9091/');
select setval('vis_server_id_seq',1);

-- liveimaging legend configuration
INSERT INTO tbl_visualization_legend (id, server, dataclass, captures, last_updated) values (1,1,200,5,0);
INSERT INTO tbl_visualization_legend (id, server, dataclass, captures, last_updated) values (2,1,201,5,0);
INSERT INTO tbl_visualization_legend (id, server, dataclass, captures, last_updated) values (3,1,202,5,0);
select setval('vis_legend_id_seq',3);

-- temperature
INSERT INTO tbl_visualization_legend_point (id, legend, position, red, green, blue) values (1,1,50,0,0,255);
INSERT INTO tbl_visualization_legend_point (id, legend, position, red, green, blue) values (2,1,57,0,150,255);
INSERT INTO tbl_visualization_legend_point (id, legend, position, red, green, blue) values (3,1,64.5,0,255,100);
INSERT INTO tbl_visualization_legend_point (id, legend, position, red, green, blue) values (4,1,66.5,0,255,50);
INSERT INTO tbl_visualization_legend_point (id, legend, position, red, green, blue) values (5,1,72.5,0,255,0);
INSERT INTO tbl_visualization_legend_point (id, legend, position, red, green, blue) values (6,1,76.5,50,255,0);
INSERT INTO tbl_visualization_legend_point (id, legend, position, red, green, blue) values (7,1,80.6,100,255,0);
INSERT INTO tbl_visualization_legend_point (id, legend, position, red, green, blue) values (8,1,90,230,210,0);
INSERT INTO tbl_visualization_legend_point (id, legend, position, red, green, blue) values (9,1,100,230,80,0);
INSERT INTO tbl_visualization_legend_point (id, legend, position, red, green, blue) values (10,1,110,255,25,0);
INSERT INTO tbl_visualization_legend_point (id, legend, position, red, green, blue) values (11,1,120,255,0,0);

-- humidity
INSERT INTO tbl_visualization_legend_point (id, legend, position, red, green, blue) values (12,2,20,230,111,0);
INSERT INTO tbl_visualization_legend_point (id, legend, position, red, green, blue) values (13,2,30,128,206,71);
INSERT INTO tbl_visualization_legend_point (id, legend, position, red, green, blue) values (14,2,35,100,255,0);
INSERT INTO tbl_visualization_legend_point (id, legend, position, red, green, blue) values (15,2,40,50,255,0);
INSERT INTO tbl_visualization_legend_point (id, legend, position, red, green, blue) values (16,2,45,0,255,0);
INSERT INTO tbl_visualization_legend_point (id, legend, position, red, green, blue) values (17,2,70,51,160,255);
INSERT INTO tbl_visualization_legend_point (id, legend, position, red, green, blue) values (18,2,80,51,51,255);

-- pressure
INSERT INTO tbl_visualization_legend_point (id, legend, position, red, green, blue) values (19,3,0,0,0,0);
INSERT INTO tbl_visualization_legend_point (id, legend, position, red, green, blue) values (20,3,0.08,0,255,0);
INSERT INTO tbl_visualization_legend_point (id, legend, position, red, green, blue) values (21,3,0.15,0,0,255);
select setval('vis_legend_point_id_seq',21);
