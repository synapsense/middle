#!/bin/bash

#script for realigning files from ApplicationServer into es before merging into jboss7

git mv ApplicationServer es
cd es
git mv SynapEnvAPI es-api
cd es-api
git rm -rf .settings
git rm -rf generated/
git rm -rf .classpath .project 
mkdir -p src/main/java src/main/resources
git mv src/com src/main/java/
git rm -rf src/META-INF/
mkdir -p src/test/java
git mv test/com src/test/java
rm -rf test/
git mv schemas/ src/main/resources/
cd ..


git mv SynapEnvCore es-core
cd es-core/
git rm -rf .settings/
mkdir -p src/main/java src/main/resources
git mv src/com src/main/java
mkdir -p ../es-api/src/main/java/com/synapsense/transport/rmi
git mv src/main/java/com/synapsense/impl/transport/rmi/AsyncRmiClientTransport.java ../es-api/src/main/java/com/synapsense/transport/rmi
git mv src/main/java/com/synapsense/impl/transport/rmi/AsyncRmiNotificationClient.java ../es-api/src/main/java/com/synapsense/transport/rmi
git mv src/main/java/com/synapsense/impl/transport/rmi/AsyncRmiNotificationService.java ../es-api/src/main/java/com/synapsense/transport/rmi
git mv src/main/java/com/synapsense/impl/transport/rmi/Constants.java ../es-api/src/main/java/com/synapsense/transport/rmi
mkdir -p src/main/java/com/synapsense/impl/performer/snmp
git mv snmp-synap/com/synapsense/performer/snmp/SynapSNMPPerformer.java src/main/java/com/synapsense/impl/performer/snmp
git rm -rf generated lib snmp-hp snmp-synap .classpath .project
git rm -rf src/main/java/com/synapsense/security
git rm -f src/META-INF/MANIFEST.MF 
git mv src/META-INF/ejb-jar.xml src/META-INF/ejb-jar_porting.xml 
git mv src/META-INF/jboss.xml src/META-INF/jboss_porting.xml 
git rm -f src/META-INF/services/com.synapsense.transport.ClientTransport
git mv src/META-INF/ src/main/resources/
git mv src/synap.properties src/main/resources/
git mv schemas/ src/main/resources/
mkdir -p src/test/java src/test/resources
git mv test/com/ src/test/java/
git mv test/log4j.properties src/test/resources/
rm -rf test/
cd ..


git mv HibernateDAL es-dal
cd es-dal
git rm -rf .settings lib .classpath .project 
mkdir -p src/main/java src/main/resources
mkdir -p src/main/resources/com/synapsense/dal/generic/entities
git mv src/com/synapsense/dal/generic/entities/*.xml src/main/resources/com/synapsense/dal/generic/entities/
git mv src/com/ src/main/java/
git mv -f src/META-INF/ejb-jar.xml src/META-INF/ejb-jar_porting.xml 
git mv -f src/META-INF/jboss.xml src/META-INF/jboss_porting.xml 
git rm -f src/META-INF/MANIFEST.MF 
git mv src/META-INF/ src/main/resources/
mkdir -p src/test/java src/test/resources
git mv test/com/ src/test/java/
git mv -f test/log4j.properties src/test/resources
rm -rf test/
cd ..


git mv DCMetrics/ es-domain
cd es-domain/
git rm -rf .settings lib .classpath .project 
mkdir -p src/main/java src/main/resources
git mv src/com/ src/main/java/
git rm -rf src/META-INF/
mkdir -p src/test/java
git mv test/com/ src/test/java/
rm -rf test/
cd ..


git mv DRulesEngine es-dre
cd es-dre/
git rm -rf .settings lib .classpath .project 
mkdir -p src/main/java src/main/resources src/test/java
git mv src/com/ src/main/java/
git mv src/InterfacesMapping.properties src/main/resources/
mkdir src/main/resources/META-INF
git mv ejbModule/META-INF/ejb-jar.xml src/main/resources/META-INF/ejb-jar_porting.xml
git mv ejbModule/META-INF/jboss.xml src/main/resources/META-INF/jboss_porting.xml
git rm -rf ejbModule/
git mv test/com src/test/java
git rm -rf test/
cd ..


git mv SynapServer es-ear
cd es-ear/
git rm -rf .settings .project 
cd JBoss
mkdir -p standalone/configuration
cp -r server/default/conf/i18n standalone/configuration
cp -r server/default/conf/snmp standalone/configuration
cp server/default/conf/audit.xml.ss standalone/configuration/audit.xml
cp server/default/conf/dbupdater.xml standalone/configuration
cp server/default/conf/mapper.xsd standalone/configuration
cp server/default/conf/mapping.xml standalone/configuration
cp server/default/conf/support-info.xml standalone/configuration
cp server/default/conf/tasks.xml standalone/configuration
cp server/default/conf/unitresolvers.xml standalone/configuration
cp server/default/conf/unitsystems.xml standalone/configuration
git add standalone
