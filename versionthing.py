import re
import os
import sys
import string

check_ext = [re.compile('.*\.java'),
             re.compile('.*\.groovy'),
             re.compile('.*\.html'),
             re.compile('.*\.aip'),
             ]

token_replace = { 'version' : [re.compile('\$SYNAPVER\$'), re.compile('5\.0\.0')],
                  'revision' : [re.compile('\$SYNAPREV\$')],
                  }



def get_revision():
    ver = -1
    svn_cmd = None
    try:
        svn_cmd = os.popen("git rev-parse HEAD")
        line = svn_cmd.readline()
        line = string.strip(line)
        ver = line[0:10]
    except:
        svn_cmd = os.popen("c:/cygwin/bin/git rev-parse HEAD")
        line = svn_cmd.readline()
        line = string.strip(line)
        ver = line[0:10]

    if svn_cmd is not None:
        svn_cmd.close()
    if int(ver,16) == -1:
        print "Could not get last revision. Check that git-rev-parse HEAD works"
        sys.exit(-1)
    return ver

def get_tag(match = None):
    cmdtxt = "git describe"
    if match is not None:
        cmdtxt += " --match=\"" + match + "\"" # Horrible shell escape

    svn_cmd = None
    try:
        svn_cmd = os.popen(cmdtxt)
        line = svn_cmd.readline()
        line = string.strip(line)
        ver = line
    except:
        svn_cmd = os.popen("c:/cygwin/bin/" + cmdtxt)
        line = svn_cmd.readline()
        line = string.strip(line)
        ver = line
    return line

def file_replace(filename, symbols):
    print "File",filename
    contents = open(filename, 'r').readlines() # Suck in the whole file
    newcontents = []
    r = contents.pop()
    while r:
        for symbol in symbols:
            if symbol in token_replace:
                tokens = token_replace[symbol]
                for token in tokens:
                    r = token.sub(symbols[symbol], r)
        # Add the regexped string back to the end of the output list
        newcontents.append(r)
        # Get the next element.
        if len(contents) > 0:
            r = contents.pop()
        else:
            break
    # Invert the output (append vs pop)
    newcontents.reverse()
    # Write the file back out
    outf = open(filename, 'w')
    for line in newcontents:
        outf.write(line)
    outf.close()



def file_tree_replace(symbols):
    for root, dirs, files in os.walk('.'):
        for name in files:
            for ext in check_ext:
                if ext.match(name, 1):
                    file_replace(os.path.join(root, name), symbols)

def main():
    revision = 0
    tag = "NO TAG"

    revision = get_revision()
    if len(sys.argv) > 1:
        tag = get_tag(sys.argv[1]) # pass in the match string
    else:
        tag = get_tag()

    rev = ""
    (branch, ver) = tag.split('/')

    # This may not always succeed
    try:
        (ver, rev) = ver.split('_')
    except:
        rev = revision

    v = ver.split('-')
    ver = v[0]
    try:
        if len(v) > 1:
            rev = rev + '_' + '-'.join(v[1:])
    except:
        pass

    print "Going to search & replace for: Version",ver,"Rev",rev
    file_tree_replace({'version': ver, 'revision': rev})


if __name__ == '__main__':
    main()
