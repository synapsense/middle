package com.synapsense.snmpagent;

import java.beans.XMLDecoder;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.synapsense.config.XmlConfigUtils;
import org.apache.log4j.Logger;
import org.snmp4j.agent.MOAccess;
import org.snmp4j.agent.ManagedObject;
import org.snmp4j.agent.mo.DefaultMOFactory;
import org.snmp4j.agent.mo.MOAccessImpl;
import org.snmp4j.agent.mo.MOTable;
import org.snmp4j.agent.mo.MOTableIndex;
import org.snmp4j.agent.mo.MOTableSubIndex;
import org.snmp4j.smi.Gauge32;
import org.snmp4j.smi.Integer32;
import org.snmp4j.smi.OID;
import org.snmp4j.smi.OctetString;
import org.snmp4j.smi.SMIConstants;
import org.snmp4j.smi.Variable;

import com.synapsense.mib.Field;
import com.synapsense.mib.MIB;
import com.synapsense.mib.Notify;
import com.synapsense.mib.NotifyMessage;
import com.synapsense.mib.Scalar;
import com.synapsense.mib.Table;
import com.synapsense.snmpagent.engine.SynapDSStorage;
import com.synapsense.snmpagent.engine.SynapMutableMOTableModel;
import com.synapsense.snmpagent.tables.AbstractScl;
import com.synapsense.snmpagent.tables.AbstractTbl;
import com.synapsense.util.SynapAgentException;
import com.synapsense.util.SynapSNMPFatalException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

public class MIBManager {
    public static final String TYPE_INTEGER = "INTEGER";
    public static final String TYPE_STRING = "STRING";
    public static final String TYPE_TIMESTAMP = "TIMESTAMP";
    public static final String TYPE_LONG = "LONG";

    public static final String ACCESS_READ_ONLY = "READONLY";
    public static final String ACCESS_READ_WRITE = "READWRITE";
    private final static Logger logger = Logger.getLogger(MIBManager.class);
    private final static String mibFile = "mib.xml";
    private static MIBManager mm = null;
    private Map<String, OID> tableBaseOIDs = new ConcurrentHashMap<String, OID>();
    protected static XmlConfigUtils confBuilder;

    static {
        try {
            confBuilder = new XmlConfigUtils("lib", com.synapsense.mib.MIB.class);
        } catch (JAXBException e) {
            throw new IllegalStateException("Cannot read MIB file", e);
        }
    }

    private MIB mib;

    private MIBManager() {
        loadMIB();
    }

    /**
     * Returns class instance
     */
    public static MIBManager getInstance() {
        if (mm == null) {
            mm = new MIBManager();
        }
        return mm;
    }

    /**
     * Loads MIB Configuration file
     */
    public void loadMIB() {
        try {
            logger.info("Loading the configuaration.");
            JAXBContext context = JAXBContext.newInstance(MIB.class);
            Unmarshaller unmarshaller = context.createUnmarshaller();
            InputStream is = MIBManager.class.getClassLoader().getResourceAsStream(mibFile);
            mib = (MIB) unmarshaller.unmarshal(is);
            is.close();

        } catch (Exception e) {
            logger.fatal(e.getLocalizedMessage(), e);
            throw new SynapSNMPFatalException(e);
        }
    }

    /**
     * Returns access by string
     *
     * @param access access of described OID
     */
    protected MOAccess getAccess(String access) {
        if (access.equalsIgnoreCase(ACCESS_READ_ONLY))
            return MOAccessImpl.ACCESS_READ_ONLY;
        else if (access.equalsIgnoreCase(ACCESS_READ_WRITE))
            return MOAccessImpl.ACCESS_READ_WRITE;
        else
            return MOAccessImpl.ACCESS_READ_ONLY;
    }

    /**
     * Creates sub-indexes for MO table
     *
     * @param tableNode node in MIB config document which describes a table
     */
    protected List<MOTableSubIndex> getIndexes(Table tableNode) {
        ArrayList<MOTableSubIndex> alist = new ArrayList<MOTableSubIndex>(2);
        List<Field> nl = tableNode.getField();
        for (Field field : nl) {
            boolean value_index = field.isIndex();
            String value_type = field.getType().value();
            String oid = field.getAddOID();
            if (value_index) {
                alist.add(new MOTableSubIndex(new OID(tableNode.getBaseOID() + oid), getSyntax(value_type), 1, 1));
            }
        }
        return alist;
    }

    /**
     * Returns syntax by type
     *
     * @param type type of described OID
     */
    protected int getSyntax(String type) {
        if (type.equalsIgnoreCase(TYPE_INTEGER))
            return SMIConstants.SYNTAX_INTEGER;
        else if (type.equalsIgnoreCase(TYPE_STRING))
            return SMIConstants.SYNTAX_OCTET_STRING;
        else if (type.equalsIgnoreCase(TYPE_TIMESTAMP))
            return SMIConstants.SYNTAX_TIMETICKS;
        else if (type.equalsIgnoreCase(TYPE_LONG))
            return SMIConstants.SYNTAX_OCTET_STRING;
        else
            return SMIConstants.SYNTAX_OBJECT_IDENTIFIER;
    }

    /**
     * Creates columns for MO table
     *
     * @param tableOID
     * @param tableNode node in MIB config document which describes a table
     * @return list of columns of MO Table
     * @throws SynapAgentException
     */
    protected List<SynapMOColumn> getColumns(OID tableOID, Table tableNode) throws SynapAgentException {
        List<SynapMOColumn> alist = new ArrayList<SynapMOColumn>();
        List<Field> nl = tableNode.getField();
        for (Field column : nl) {
            String columnName = column.getName();
            String value_type = column.getType().value();
            String value_access = column.getAccess().value();
            String propOID = tableOID + column.getAddOID();
            String[] idxs = propOID.split("\\.");

            int j = Integer.parseInt(idxs[idxs.length - 1]);
            // index,syntax,access, name
            SynapMOColumn col = new SynapMOColumn(j, getSyntax(value_type), getAccess(value_access), columnName);
            alist.add(col);
        }
        return alist;
    }

    /**
     * Creates MO Table from current node
     *
     * @param node node in MIB config document which describes a table
     */
    protected ManagedObject createTableFromNode(Table node) throws SynapAgentException {
        OID tableOID = null;
        String tableName = null;
        tableOID = new OID(node.getBaseOID());
        tableName = node.getName();
        tableBaseOIDs.put(tableName, tableOID);
        List<MOTableSubIndex> indexList = getIndexes(node);
        MOTableIndex indexDef = null;
        if (!indexList.isEmpty()) {
            MOTableSubIndex[] indexes = new MOTableSubIndex[indexList.size()];
            for (int i = 0; i < indexList.size(); i++) {
                indexes[i] = indexList.get(i);
            }
            indexDef = new MOTableIndex(indexes, false);
        }
        SynapMOColumn[] columns = null;
        List<SynapMOColumn> columnsList = null;
        MOTable tbl = null;
        try {
            columnsList = getColumns(tableOID, node);
            if (!columnsList.isEmpty()) {
                columns = columnsList.toArray(new SynapMOColumn[0]);
            }
            int columnNum = columnsList.size();
            Map<String, Integer> columnNameToIdx = new HashMap<String, Integer>();
            Map<String, Integer> columnTypes = new HashMap<String, Integer>();
            Map<Integer, Integer> columnIdToIdx = new HashMap<Integer, Integer>();
            for (int i = 0; i < columnNum; i++) {
                SynapMOColumn column = columnsList.get(i);
                String columnName = column.getColumnName();
                columnNameToIdx.put(columnName, i);
                columnTypes.put(columnName, column.getSyntax());
                columnIdToIdx.put(column.getColumnID(), i);
            }
            logger.info("Creating table with name=" + tableName);
            SynapMutableMOTableModel tableModel = new SynapMutableMOTableModel(tableName, columnNameToIdx, columnTypes);
            String claName = node.getClazz();
            Class<?> cla = Class.forName(claName);
            @SuppressWarnings("rawtypes")
            Class[] pt = {String.class, OID.class, SynapMutableMOTableModel.class, Map.class};
            AbstractTbl object = (AbstractTbl) cla.getConstructor(pt).newInstance(tableName, tableOID, tableModel,
                    columnIdToIdx);
            String[] propNames = columnTypes.keySet().toArray(new String[columnTypes.keySet().size()]);
            SynapDSStorage.getInstance().addTable(tableName, object, propNames);
            tbl = DefaultMOFactory.getInstance().createTable(tableOID, indexDef, columns, tableModel);
            tbl.addMOChangeListener(object);
        } catch (Exception e) {
            throw new SynapAgentException(e);
        }
        return tbl;
    }

    /**
     * Creates MO Scalar from current node
     *
     * @param sc node in MIB config document which describes a scalar
     * @return created new MO Scalar object
     * @throws SynapAgentException if something wrong
     */
    protected ManagedObject createScalarFromNode(Scalar sc) throws SynapAgentException {
        Field node = sc.getField();
        OID scalarOID = null;
        scalarOID = new OID(sc.getBaseOID() + node.getAddOID());
        String str = node.getType().value();
        int syntax = getSyntax(str);
        String defVal = node.getDefval();
        logger.info("Creating scalar with name=" + node.getName());
        Variable variable;
        if (defVal == null) {
            if (SMIConstants.SYNTAX_OCTET_STRING == syntax) {
                defVal = "";
            } else {
                defVal = "0";
            }
        }
        switch (syntax) {
            case SMIConstants.SYNTAX_OCTET_STRING:
                variable = new OctetString(defVal);
                break;
            case SMIConstants.SYNTAX_GAUGE32:
                variable = new Gauge32(Long.parseLong(defVal));
                break;
            case SMIConstants.SYNTAX_INTEGER32:
                variable = new Integer32(Integer.parseInt(defVal));
                break;
            default:
                variable = new Integer32(Integer.parseInt(defVal));
        }
        try {
            String claName = sc.getClazz();
            Class<?> cla = Class.forName(claName);
            @SuppressWarnings("rawtypes")
            Class[] pt = {String.class, Variable.class};
            AbstractScl object = (AbstractScl) cla.getConstructor(pt).newInstance(node.getName(), variable);
            SynapDSStorage.getInstance().addScalar(object, object);
        } catch (Exception e) {
            throw new SynapAgentException(e);
        }
        return DefaultMOFactory.getInstance().createScalar(scalarOID, MOAccessImpl.ACCESS_READ_ONLY, variable);
    }

    /**
     * Creates and returns all managed objects which are described in MIB config
     * file
     *
     * @throws SynapAgentException
     */
    public List<ManagedObject> getMOList() {
        List<ManagedObject> alist = new ArrayList<ManagedObject>();
        List<Table> node = mib.getTable();
        ManagedObject mo = null;
        for (Table table : node) {
            try {
                mo = createTableFromNode(table);
            } catch (Throwable e) {
                logger.error(e.getLocalizedMessage());
                logger.debug("", e);
            }
            if (mo != null)
                alist.add(mo);
        }
        List<Scalar> scs = mib.getScalar();
        for (Scalar table : scs) {
            try {
                mo = createScalarFromNode(table);
            } catch (Throwable e) {
                logger.error(e.getLocalizedMessage());
                logger.debug("", e);
            }
            if (mo != null)
                alist.add(mo);
        }
        SynapDSStorage.getInstance().registerProcessors();
        return alist;
    }

    /**
     * Searches for Notify Events in MIB xml document
     *
     * @return Hashmap of supported Notify Events as key and OIDs as value
     */
    public HashMap<String, OID> getEvents() {
        HashMap<String, OID> events = new HashMap<String, OID>();
        List<Notify> n = mib.getNotify();
        for (Notify notify : n) {
            String eventName = null;
            String eventOID = null;
            eventName = notify.getName();
            eventOID = notify.getOID();
            if ((eventName != null) && (!"".equals(eventName)) && (eventOID != null) && (!"".equals(eventOID))) {
                if (!events.containsKey(eventName)) {
                    events.put(eventName, new OID(eventOID));
                } else
                    logger.warn("Duplicate Notify Event " + eventName);
            }
        }
        return events;
    }

    /**
     * Searches for SynapSenseEventMessage OID in MIB xml document
     *
     * @return SynapSenseEventMessage OID
     */
    public OID getEventMessageOID() {
        NotifyMessage n = mib.getNotifyMessage();
        String messageOID = n.getOID();
        return new OID(messageOID);
    }

    public OID getRootOID() {
        return new OID(mib.getRootOid().getOID());
    }

    public OID getTableBaseOID(String tableName) {
        return tableBaseOIDs.get(tableName);
    }

}
