package com.synapsense.snmpagent;

import java.io.File;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.snmp4j.TransportMapping;
import org.snmp4j.agent.BaseAgent;
import org.snmp4j.agent.CommandProcessor;
import org.snmp4j.agent.DuplicateRegistrationException;
import org.snmp4j.agent.ManagedObject;
import org.snmp4j.agent.mo.snmp.RowStatus;
import org.snmp4j.agent.mo.snmp.SnmpCommunityMIB;
import org.snmp4j.agent.mo.snmp.SnmpNotificationMIB;
import org.snmp4j.agent.mo.snmp.SnmpTargetMIB;
import org.snmp4j.agent.mo.snmp.StorageType;
import org.snmp4j.agent.mo.snmp.TransportDomains;
import org.snmp4j.agent.mo.snmp.VacmMIB;
import org.snmp4j.mp.MPv3;
import org.snmp4j.mp.MessageProcessingModel;
import org.snmp4j.mp.SnmpConstants;
import org.snmp4j.security.SecurityLevel;
import org.snmp4j.security.SecurityModel;
import org.snmp4j.security.USM;
import org.snmp4j.security.UsmUser;
import org.snmp4j.smi.Address;
import org.snmp4j.smi.Integer32;
import org.snmp4j.smi.OID;
import org.snmp4j.smi.OctetString;
import org.snmp4j.smi.TcpAddress;
import org.snmp4j.smi.TransportIpAddress;
import org.snmp4j.smi.UdpAddress;
import org.snmp4j.smi.Variable;
import org.snmp4j.smi.VariableBinding;
import org.snmp4j.transport.DefaultTcpTransportMapping;
import org.snmp4j.transport.DefaultUdpTransportMapping;
import org.snmp4j.util.ThreadPool;
import com.synapsense.util.SynapAgentException;
import com.synapsense.util.SynapExceptionCreator;
import com.synapsense.util.SynapExceptionHandler;

/**
 * The <code>SynapSNMPAgent</code> is a SNMP agent implementation based on the
 * SNMP4J-Agent framework. The <code>SynapSNMPAgent</code> extends the
 * <code>BaseAgent</code> It implements SynapSense MIB
 * <p>
 * 
 * @author Maxim Bogatkin, Vladimir Rodionov
 * @version 2.0
 */
public class SynapSNMPBaseAgent extends BaseAgent implements SynapSNMPAgent, SynapExceptionCreator {
	private List<ManagedObject> list;

	protected void sendColdStartNotification() {
		for (int i = 0; i < notificationTargets.size(); i++) {
			if (this.notificationTargets.get(i).getAddress() instanceof TcpAddress) {
				InetSocketAddress sa = new InetSocketAddress(notificationTargets.get(i).getAddress().getInetAddress(),
				        notificationTargets.get(i).getAddress().getPort());
				Socket s = new Socket();
				try {
					s.connect(sa);
					s.close();
					Address ipa = targets.getTargetAddress(new OctetString("notification" + i));
					if (ipa == null) {
						targets.addTargetAddress(new OctetString("notification" + i),
						        TransportDomains.transportDomainTcpIpv4, new OctetString(notificationTargets.get(i)
						                .getAddress().getValue()), notificationTargets.get(i).getTimeout(),
						        notificationTargets.get(i).getRetriesCount(), new OctetString("notify"),
						        new OctetString("v2c"), StorageType.permanent);
					}
				} catch (IOException e) {
					logger.warn("Target " + sa.getHostName() + ":" + sa.getPort() + " is not available.");
					targets.removeTargetAddress(new OctetString("notification" + i));
				}
			}
		}
		super.sendColdStartNotification();
	}

	private static final Logger logger = LogManager.getLogger(SynapSNMPBaseAgent.class);

	/**
	 * Returns class logger
	 */
	public Logger getLogger() {
		return logger;
	}

	public static final String defConfigurationFile = "conf/SNMPConf.xml";

	private static final String SynapBootCounterFile = "conf/SynapSNMPBaseAgentBC.cfg";
	private static final String SynapAgentConfigFile = "conf/SynapSNMPBaseAgentCONFIG.cfg";
	private static final String RequestPoolName = "RequestPool";

	private List<SNMPNotificationTarget> notificationTargets = null;
	private List<TransportIpAddress> transportInterfaces = null;
	private SnmpTargetMIB targets = null;
	/*---------------------------- v2c ----------------------------*/

	private boolean v2c_supported = false;
	private OctetString communityStringPublic = null;
	private OctetString communityStringPrivate = null;

	/*---------------------------- v3 ----------------------------*/
	private boolean v3_supported = false;
	private OctetString securityName = null;
	private OctetString contextName = null;
	private int securityLevel = SecurityLevel.NOAUTH_NOPRIV;
	private OID authProtocol = null;
	private OctetString authPassword = null;
	private OID privProtocol = null;
	private OctetString privPassword = null;

	protected HashMap<String, OID> events;

	protected OID oidSynapsenseEventMessage = null;

	/**
	 * Creates <code>SynapSNMPAgent</code> object
	 * 
	 * @param configuration
	 *            configuration object
	 */
	public SynapSNMPBaseAgent(SynapSNMPAgentCO configuration) {
		super(new File(SynapBootCounterFile), new File(SynapAgentConfigFile), new CommandProcessor(new OctetString(
		        MPv3.createLocalEngineID())));
		notificationTargets = new ArrayList<SNMPNotificationTarget>();
		transportInterfaces = new ArrayList<TransportIpAddress>();

		/* read conf */
		v2c_supported = configuration.v2c_supported();
		if (v2c_supported) {
			communityStringPublic = configuration.getCommunityPublicString();
			communityStringPrivate = configuration.getCommunityPrivateString();
		}

		v3_supported = configuration.v3_supported();
		if (v3_supported) {
			securityName = configuration.getSecurityName();
			contextName = configuration.getContextName();
			securityLevel = configuration.getSecurityLevel();
			if (securityLevel > SecurityLevel.NOAUTH_NOPRIV) {
				authProtocol = configuration.getAuthenticationOID();
				authPassword = configuration.getAuthenticationPassword();
			}
			if (securityLevel == SecurityLevel.AUTH_PRIV) {
				privProtocol = configuration.getPrivacyOID();
				privPassword = configuration.getPrivacyPassword();
			}
		}

		configuration.getNotificationTargets(notificationTargets);
		configuration.getNetworkInterfaces(transportInterfaces);

	}

	/**
	 * Initializes SNMP Agent
	 * 
	 * @throws SocketException
	 * 
	 */
	public void init() {
		this.getAgent().setWorkerPool(ThreadPool.create(RequestPoolName, 9));
		try {
			super.init();
			if (v2c_supported) {
				server.addContext(communityStringPublic);
			}
			if (v3_supported) {
				server.addContext(contextName);
			}
		} catch (IOException e) {
			logger.fatal(e);
			new SynapExceptionHandler().handleException(
			        SynapAgentException.getException(SynapAgentException.CRITICAL_SNMP_AGENT_NOT_INITIALIZED, e), this);
		}
		addShutdownHook();
		finishInit();
		events = MIBManager.getInstance().getEvents();
		oidSynapsenseEventMessage = MIBManager.getInstance().getEventMessageOID();
		sendColdStartNotification();
	}

	/**
	 * Register MIB tables from XML file at the agent's server.
	 */
	protected void registerManagedObjects() {
		list = MIBManager.getInstance().getMOList();
		if (list.size() == 0) {
			logger.warn("List of managed object is empty. Agent can not manage objects in MIB");
		}
		ManagedObject vmo = server.getManagedObject(new OID(".1.3.6.1.2.1.1.2.0"), null);
		server.unregister(vmo, null);
		for (int i = 0; i < list.size(); i++) {
			String moDesc = (list.get(i)).toString();
			try {
				server.register(list.get(i), null);
			} catch (DuplicateRegistrationException e) {
				new SynapExceptionHandler().handleException(SynapAgentException.getException(
				        SynapAgentException.ERROR_MIB_TABLE_TRING_REGISTERED_TWICE, new String[] { moDesc }, e), this);
			}
		}
	}

	/**
	 * Adds community to security name mappings needed for SNMPv1 and SNMPv2c.
	 * 
	 * @param communityMIB
	 *            the SnmpCommunityMIB holding coexistence configuration for
	 *            community based security models.
	 */
	protected void addCommunities(SnmpCommunityMIB communityMIB) {
		if (v2c_supported) {
			Variable[] com2sec = new Variable[] { new OctetString(communityStringPublic), // community
																						  // name
			        new OctetString(communityStringPublic), // security name
			        getAgent().getContextEngineID(), // local engine ID
			        new OctetString(communityStringPublic), // default context
															// name
			        new OctetString(), // transport tag
			        new Integer32(StorageType.permanent), // storage type
			        new Integer32(RowStatus.active) // row status
			};
			communityMIB.getSnmpCommunityEntry().addRow(communityMIB.getSnmpCommunityEntry().createRow(
			        new OctetString("public2public").toSubIndex(true), com2sec));

			Variable[] com2sec2 = new Variable[] { new OctetString(communityStringPrivate), // community
																							// name
			        new OctetString(communityStringPrivate), // security name
			        getAgent().getContextEngineID(), // local engine ID
			        new OctetString(communityStringPublic), // default context
															// name
			        new OctetString(), // transport tag
			        new Integer32(StorageType.permanent), // storage type
			        new Integer32(RowStatus.active) // row status
			};
			communityMIB.getSnmpCommunityEntry().addRow(communityMIB.getSnmpCommunityEntry().createRow(
			        new OctetString("private2private").toSubIndex(true), com2sec2));
		}
	}

	/**
	 * Adds notification targets and filters.
	 * 
	 * @param targetMIB
	 *            the SnmpTargetMIB holding the target configuration.
	 * @param notificationMIB
	 *            the SnmpNotificationMIB holding the notification (filter)
	 *            configuration.
	 */
	protected void addNotificationTargets(SnmpTargetMIB targetMIB, SnmpNotificationMIB notificationMIB) {
		targetMIB.addDefaultTDomains();
		if (notificationTargets.size() == 0) {
			logger.warn("Notification list is empty. Agent will not send any notifications");
		}
		OID td = null;
		for (int i = 0; i < notificationTargets.size(); i++) {
			SNMPNotificationTarget target = notificationTargets.get(i);
			if (this.notificationTargets.get(i).getAddress() instanceof TcpAddress) {
				td = TransportDomains.transportDomainTcpIpv4;
			} else if (this.notificationTargets.get(i).getAddress() instanceof UdpAddress) {
				td = TransportDomains.transportDomainUdpIpv4;
			}
			if (target.getSNMPVersion() == SnmpConstants.version2c) {
				if (!v2c_supported) {
					logger.error("v2c is disabled but target " + notificationTargets.get(i).getAddress()
					        + " have snmpversion: v2c");
					continue;
				}

				targetMIB.addTargetAddress(new OctetString("notification" + i), td, new OctetString(notificationTargets
				        .get(i).getAddress().getValue()), notificationTargets.get(i).getTimeout(), notificationTargets
				        .get(i).getRetriesCount(), new OctetString("notify"), new OctetString("v2c"),
				        StorageType.permanent);
				targetMIB.addTargetParams(new OctetString("v2c"), MessageProcessingModel.MPv2c,
				        SecurityModel.SECURITY_MODEL_SNMPv2c, communityStringPublic, securityLevel,
				        StorageType.permanent);
			} else if (target.getSNMPVersion() == SnmpConstants.version3) {
				if (!v3_supported) {
					logger.error("v3 is disabled but target " + notificationTargets.get(i).getAddress()
					        + " have snmpversion: v3");
					continue;
				}

				targetMIB.addTargetAddress(new OctetString("notification" + i), td, new OctetString(notificationTargets
				        .get(i).getAddress().getValue()), notificationTargets.get(i).getTimeout(), notificationTargets
				        .get(i).getRetriesCount(), new OctetString("notify"), new OctetString("v3"),
				        StorageType.permanent);
				targetMIB.addTargetParams(new OctetString("v3"), MessageProcessingModel.MPv3,
				        SecurityModel.SECURITY_MODEL_USM, securityName, securityLevel, StorageType.permanent);

			}
		}
		notificationMIB.addNotifyEntry(new OctetString("default"), new OctetString("notify"),
		        SnmpNotificationMIB.SnmpNotifyTypeEnum.trap, StorageType.permanent);
		targets = targetMIB;
	}

	/**
	 * Adds all the necessary users to the USM.
	 * 
	 * @param usm
	 *            the USM instance used by this agent.
	 */
	protected void addUsmUser(USM usm) {
		if (v3_supported) {
			UsmUser user = new UsmUser(securityName, authProtocol, authPassword, privProtocol, privPassword);
			usm.addUser(user.getSecurityName(), usm.getLocalEngineID(), user);
		}
	}

	/**
	 * Adds VACM configuration.
	 * 
	 * @param vacm
	 *            the VacmMIB holding the agent's view configuration.
	 */
	protected void addViews(VacmMIB vacm) {
		if (v2c_supported) {
			vacm.addGroup(SecurityModel.SECURITY_MODEL_SNMPv1, this.communityStringPublic,
			        new OctetString("v1v2group"), StorageType.permanent);
			vacm.addGroup(SecurityModel.SECURITY_MODEL_SNMPv2c, this.communityStringPublic,
			        new OctetString("v1v2group"), StorageType.permanent);
			vacm.addGroup(SecurityModel.SECURITY_MODEL_SNMPv2c, this.communityStringPrivate, new OctetString(
			        "v1v2group_w"), StorageType.permanent);

			vacm.addAccess(new OctetString("v1v2group"), new OctetString(), SecurityModel.SECURITY_MODEL_ANY,
			        SecurityLevel.NOAUTH_NOPRIV, VacmMIB.VACM_MATCH_PREFIX, new OctetString("fullReadView"),
			        new OctetString(), new OctetString("fullNotifyView"), StorageType.permanent);

			vacm.addAccess(new OctetString("v1v2group_w"), new OctetString(), SecurityModel.SECURITY_MODEL_ANY,
			        SecurityLevel.NOAUTH_NOPRIV, VacmMIB.VACM_MATCH_PREFIX, new OctetString("fullReadView"),
			        new OctetString("fullWriteView"), new OctetString("fullNotifyView"), StorageType.permanent);

			vacm.addViewTreeFamily(new OctetString("fullReadView"), new OID("1.3"), new OctetString(),
			        VacmMIB.vacmViewIncluded, StorageType.permanent);
			vacm.addViewTreeFamily(new OctetString("fullWriteView"), new OID("1.3"), new OctetString(),
			        VacmMIB.vacmViewIncluded, StorageType.permanent);
			vacm.addViewTreeFamily(new OctetString("fullNotifyView"), new OID("1.3"), new OctetString(),
			        VacmMIB.vacmViewIncluded, StorageType.permanent);
		}

		if (v3_supported) {
			vacm.addGroup(SecurityModel.SECURITY_MODEL_USM, securityName, new OctetString("v3ourgroup"),
			        StorageType.permanent);
			vacm.addAccess(new OctetString("v3ourgroup"), new OctetString(), SecurityModel.SECURITY_MODEL_USM,
			        securityLevel, VacmMIB.VACM_MATCH_EXACT, new OctetString("v3readview"), new OctetString(
			                "v3writeview"), new OctetString("v3notifyview"), StorageType.permanent);
			vacm.addAccess(new OctetString("v3ourgroup"), contextName, SecurityModel.SECURITY_MODEL_USM, securityLevel,
			        VacmMIB.VACM_MATCH_EXACT, new OctetString("v3readview"), new OctetString("v3writeview"),
			        new OctetString("v3notifyview"), StorageType.permanent);
			vacm.addViewTreeFamily(new OctetString("v3readview"), new OID("1.3"), new OctetString(),
			        VacmMIB.vacmViewIncluded, StorageType.permanent);
			vacm.addViewTreeFamily(new OctetString("v3writeview"), new OID("1.3"), new OctetString(),
			        VacmMIB.vacmViewIncluded, StorageType.permanent);
			vacm.addViewTreeFamily(new OctetString("v3notifyview"), new OID("1.3"), new OctetString(), // 1.3.6.1.4.1.29078.1
			        VacmMIB.vacmViewIncluded, StorageType.permanent);
		}

	}

	/**
	 * Unregister additional managed objects from the agent's server.
	 */
	protected void unregisterManagedObjects() {
		for (int i = 0; i < list.size(); i++) {
			server.unregister((ManagedObject) list.get(i), null);
		}
	}

	/**
	 * Initializes the transport mappings (ports) to be used by the agent.
	 * 
	 * @throws IOException
	 */
	protected void initTransportMappings() throws IOException {
		transportMappings = new TransportMapping[transportInterfaces.size()];
		for (int i = 0; i < transportInterfaces.size(); i++) {
			TransportIpAddress address = transportInterfaces.get(i);
			if (address instanceof UdpAddress) {
				transportMappings[i] = new DefaultUdpTransportMapping((UdpAddress) address);
			} else if (address instanceof TcpAddress) {
				transportMappings[i] = new DefaultTcpTransportMapping((TcpAddress) address);
			}
		}
	}

	/**
	 * Sends notification to all configured target NMS systems
	 * 
	 * @param commandName
	 *            the received command type.
	 * @param commandParameter
	 *            the command parameter or null if not exist.
	 */
	public void sendNotify(String commandName, String commandParameter) {
		for (int i = 0; i < notificationTargets.size(); i++) {
			if (this.notificationTargets.get(i).getAddress() instanceof TcpAddress) {
				InetSocketAddress sa = new InetSocketAddress(notificationTargets.get(i).getAddress().getInetAddress(),
				        notificationTargets.get(i).getAddress().getPort());
				Socket s = new Socket();
				try {
					s.connect(sa);
					s.close();
					Address ipa = targets.getTargetAddress(new OctetString("notification" + i));
					if (ipa == null) {
						targets.addTargetAddress(new OctetString("notification" + i),
						        TransportDomains.transportDomainTcpIpv4, new OctetString(notificationTargets.get(i)
						                .getAddress().getValue()), notificationTargets.get(i).getTimeout(),
						        notificationTargets.get(i).getRetriesCount(), new OctetString("notify"),
						        new OctetString("v2c"), StorageType.permanent);
					}
				} catch (IOException e) {
					logger.warn("Target " + sa.getHostName() + ":" + sa.getPort() + " is not available.");
					targets.removeTargetAddress(new OctetString("notification" + i));
				}
			}
		}
		if ((commandName != null)) {
			OID no = events.get("snmpagentEvent");// new
												  // OID("1.3.6.1.4.1.29078.1.1.1.1.1.1");
			//
			if ((commandParameter != null) && (oidSynapsenseEventMessage != null)) {
				VariableBinding vbs = new VariableBinding(oidSynapsenseEventMessage, new OctetString(commandParameter));
				if (v3_supported) {
					this.getNotificationOriginator().notify(this.contextName, no, new VariableBinding[] { vbs });
				} else {
					if (v2c_supported) {
						this.getNotificationOriginator().notify(this.communityStringPublic, no,
						        new VariableBinding[] { vbs });
					}
				}

			} else {
				if (v3_supported) {
					this.getNotificationOriginator().notify(this.contextName, no, new VariableBinding[] {});

				} else {
					if (v2c_supported) {
						this.getNotificationOriginator().notify(this.communityStringPublic, no,
						        new VariableBinding[] {});
					}
				}
			}
		} else
			logger.warn("Unsupported Notify event");
	}

	@Override
	public void addTableIndexes(String tableName, List<String> indexes) {
		// TODO Auto-generated method stub

	}

	@Override
	public List<String> getTableIndexes(String tableName) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ManagedObject> getMOList() {
		return list;
	}

}