package com.synapsense.snmpagent;

import java.io.*;
import java.net.InetSocketAddress;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;

import org.snmp4j.agent.*;
import org.snmp4j.agent.agentx.AgentX;
import org.snmp4j.agent.agentx.AgentXMessageDispatcherImpl;
import org.snmp4j.agent.agentx.AgentXProtocol;
import org.snmp4j.agent.agentx.AgentXSession;
import org.snmp4j.agent.agentx.subagent.AgentXSubagent;
import org.snmp4j.agent.agentx.subagent.RegistrationCallback;
import org.snmp4j.agent.mo.MOTable;
import org.snmp4j.agent.mo.MOTableRow;
import org.snmp4j.agent.mo.snmp.SNMPv2MIB.SysUpTimeImpl;
import org.snmp4j.agent.mo.snmp4j.Snmp4jConfigMib;
import org.snmp4j.agent.mo.snmp4j.Snmp4jLogMib;

import org.apache.log4j.Logger;
import org.apache.log4j.LogManager;

import org.snmp4j.mp.*;
import org.snmp4j.smi.*;
import org.snmp4j.transport.*;
import org.snmp4j.util.ThreadPool;

import com.synapsense.util.SynapAgentException;
import com.synapsense.util.SynapExceptionCreator;
import com.synapsense.util.SynapExceptionHandler;

/**
 * The <code>SynapSNMPAgent</code> is a SNMP agent implementation based on the
 * SNMP4J-Agent framework. The <code>SynapSNMPAgent</code> extends the
 * <code>BaseAgent</code> It implements SynapSense MIB
 * <p>
 * 
 * @author Maxim Bogatkin, Vladimir Rodionov
 * @version 2.0
 */
public class SynapSNMPSubAgent extends AgentXSubagent implements SynapSNMPAgent, SynapExceptionCreator, Runnable,
        TransportStateListener {
	private List<ManagedObject> list;
	private String agentStatus = "";
	private String synapstoreStatus = "";
	private String databaseStatus = "";
	private String webserverStatus = "";
	private String synapvizserverStatus = "";
	private String webserverURL = "";
	private String synapvizserverURL = "";

	private static final Logger logger = LogManager.getLogger(SynapSNMPSubAgent.class);

	/**
	 * Returns class logger
	 */
	public Logger getLogger() {
		return logger;
	}

	private static final OctetString SUBAGENT_NAME = new OctetString("SynapSense AgentX4J agent");

	public static final String defConfigurationFile = "conf/SNMPConf.xml";

	private static final String RequestPoolName = "RequestPool";

	private Map<String, List<String>> tableIndexes = null;
	protected HashMap<String, OID> events;

	protected OID oidSynapsenseEventMessage = null;

	private MOServer server = null;
	private Snmp4jConfigMib snmp4jConfigMib;
	private Snmp4jLogMib snmp4jLogMib;
	private AgentXSession session;
	private int sessionID = 0;

	private Address localHost = null;
	private Address masterHost = null;

	private SysUpTimeImpl sessionContextUpTime = new SysUpTimeImpl();

	/**
	 * Creates <code>SynapSNMPAgent</code> object
	 * 
	 * @param configuration
	 *            configuration object
	 */
	public SynapSNMPSubAgent(SynapSNMPAgentCO configuration) {
		super(new AgentX(new AgentXMessageDispatcherImpl()), new OID(), SUBAGENT_NAME);
		server = new DefaultMOServer();
		server.addContext(new OctetString());
		setThreadPool(ThreadPool.create(RequestPoolName, 9));
		addMOServer(server);

		List<TransportIpAddress> transportInterfaces = new ArrayList<TransportIpAddress>();
		configuration.getNetworkInterfaces(transportInterfaces);
		if (transportInterfaces.size() > 0) {
			TransportIpAddress addr2 = transportInterfaces.get(0);
			if (addr2 != null) {
				localHost = new TcpAddress(addr2.getInetAddress(), addr2.getPort());
			}
		}

		InetSocketAddress addr = configuration.getMasterHost();
		if (addr != null) {
			masterHost = new TcpAddress(addr.getAddress(), addr.getPort());
		}
		tableIndexes = new HashMap<String, List<String>>(10);

	}

	protected void register() {
		list = MIBManager.getInstance().getMOList();
		if (list.size() == 0) {
			logger.warn("List of managed object is empty. Agent can not manage objects in MIB");
		}
		for (int i = 0; i < list.size(); i++) {
			String moDesc = (list.get(i)).toString();
			try {
				server.register(list.get(i), null);
			} catch (DuplicateRegistrationException e) {
				new SynapExceptionHandler().handleException(SynapAgentException.getException(
				        SynapAgentException.ERROR_MIB_TABLE_TRING_REGISTERED_TWICE, new String[] { moDesc }, e), this);
			}
		}
	}

	protected void unregisterSessionDependent() {
		if (session != null) {
			OctetString sessionContext = getSessionContext(session.getSessionID());
			server.removeContext(sessionContext);
			if (snmp4jConfigMib != null) {
				snmp4jConfigMib.unregisterMOs(server, sessionContext);
			}
			if (snmp4jLogMib != null) {
				snmp4jLogMib.unregisterMOs(server, sessionContext);
			}
		}
	}

	protected void registerSessionDependent() throws DuplicateRegistrationException {
		OctetString sessionContext = getSessionContext(session.getSessionID());
		server.addContext(sessionContext);
		snmp4jConfigMib = new Snmp4jConfigMib(sessionContextUpTime);
		snmp4jConfigMib.registerMOs(server, sessionContext);
		snmp4jLogMib = new Snmp4jLogMib();
		snmp4jLogMib.registerMOs(server, sessionContext);
	}

	private static OctetString getSessionContext(int sessionID) {
		return new OctetString("session=" + sessionID);
	}

	public void run() {
		try {
			Runtime.getRuntime().addShutdownHook(new AgentShutdown());
			register();
			unregisterSessionDependent();
			session = new AgentXSession(++sessionID);
			int status = this.connect(masterHost, localHost, session);
			if (status == AgentXProtocol.AGENTX_SUCCESS) {
				addAgentCaps(session, new OctetString(), MIBManager.getInstance().getRootOID(), new OctetString(
				        SUBAGENT_NAME));
				registerSessionDependent();
				List<?> failed = registerRegions(session, new OctetString());
				if (failed.size() > 0) {
					logger.warn("Failed to register to following objects at master:" + failed);
				}
				TimeTicks upTime = new TimeTicks();
				registerRegions(session, getSessionContext(session.getSessionID()), upTime);
				sessionContextUpTime.setValue(upTime);
				setPingDelay(30);
				notify(null, SnmpConstants.warmStart, new VariableBinding[] { new VariableBinding(
				        SnmpConstants.sysDescr, new OctetString(SUBAGENT_NAME)) });
				((ConnectionOrientedTransportMapping) session.getPeer().getTransport()).addTransportStateListener(this);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	// [BC 07/2015] Deprecated method copied from snmp4j-agentx-1.3.2
	public List registerRegions(AgentXSession session, OctetString context) {
		return this.registerRegions(session, context, null);
	}

	// [BC 07/2015] Deprecated method copied from snmp4j-agentx-1.3.2
	public List registerRegions(AgentXSession session, OctetString context, TimeTicks sysUpTime) {
		final LinkedList failures = new LinkedList();
		registerRegions(session, context, sysUpTime, new RegistrationCallback() {
			public void registrationEvent(OctetString context, ManagedObject mo, int result) {
				if(result != 0) {
					//noinspection unchecked
					failures.add(mo);
				}

			}

			public boolean tableRegistrationEvent(OctetString context, MOTable table, MOTableRow row, boolean indexAllocation, int result, int retryCount) {
				if(result != 0) {
					//noinspection unchecked
					failures.add(row);
				}

				return false;
			}

			public void unregistrationEvent(OctetString context, ManagedObject mo, int status) {
			}

			public void tableUnregistrationEvent(OctetString context, MOTable mo, MOTableRow row, boolean indexAllocation, int status) {
			}
		});
		return failures;
	}

	public void connectionStateChanged(final TransportStateEvent change) {
		if (change.getNewState() == TransportStateEvent.STATE_DISCONNECTED_REMOTELY) {
			// remove session dependent registrations
			// try to reconnect
			Thread t = new Thread(new Runnable() {
				public void run() {
					Address addr = change.getPeerAddress();
					// try to reconnect ten times if we have been disconnected
					// remotely
					for (int i = 0; i < 10; i++) {
						try {
							Thread.sleep(5000);
							if (connect(addr, localHost, session) == AgentXProtocol.AGENTX_SUCCESS) {
								// if connected register our MIB objects
								try {
									registerSessionDependent();
								} catch (DuplicateRegistrationException ex1) {
									ex1.printStackTrace();
								}
								registerRegions(session, new OctetString());
								server.addContext(getSessionContext(session.getSessionID()));
								TimeTicks upTime = new TimeTicks();
								registerRegions(session, getSessionContext(session.getSessionID()), upTime);
								sessionContextUpTime.setValue(upTime);
								break;
							}
						} catch (IOException ex) {
							ex.printStackTrace();
						} catch (InterruptedException ex) {
							break;
						}
					}
				}
			});
			t.start();
		}
	}

	public synchronized List<String> getTableIndexes(String tableName) {
		if (!tableIndexes.containsKey(tableName)) {
			return new ArrayList<String>();
		} else {
			return tableIndexes.get(tableName);
		}
	}

	/**
	 * The <code>AgentShutdown</code> is being executed when the agent is about
	 * to be shut down (e.g. terminated by SIGTERM/Ctrl-C). Its purpose is to
	 * send an AgentX Close PDU to the master agent to gracefully unregister the
	 * current session.
	 * 
	 * @author Frank Fock
	 * @version 1.0
	 */
	class AgentShutdown extends Thread {
		public void run() {
			try {
				close(session, AgentXProtocol.REASON_SHUTDOWN);
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
	}

	public String getStatus() {
		String res = "OK";

		return res;
	}

	@Override
	public void addTableIndexes(String tableName, List<String> indexes) {
		// TODO Auto-generated method stub

	}

	public String getAgentStatus() {
		return agentStatus;
	}

	public String getSynapstoreStatus() {
		return synapstoreStatus;
	}

	public String getDatabaseStatus() {
		return databaseStatus;
	}

	public String getWebserverStatus() {
		return webserverStatus;
	}

	public String getWebserverURL() {
		return webserverURL;
	}

	public String getSynapvizserverURL() {
		return synapvizserverURL;
	}

	public String getSynapvizserverStatus() {
		return synapvizserverStatus;
	}

	@Override
	public List<ManagedObject> getMOList() {
		return list;
	}
}
