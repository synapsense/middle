/**
 * 
 */
package com.synapsense.snmpagent;

import java.beans.XMLDecoder;
import java.io.*;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.util.List;

import com.synapsense.mib.MIB;
import org.apache.log4j.Logger;
import org.snmp4j.mp.SnmpConstants;
import org.snmp4j.security.AuthMD5;
import org.snmp4j.security.AuthSHA;
import org.snmp4j.security.PrivAES128;
import org.snmp4j.security.PrivAES192;
import org.snmp4j.security.PrivAES256;
import org.snmp4j.security.PrivDES;
import org.snmp4j.security.SecurityLevel;
import org.snmp4j.smi.OctetString;
import org.snmp4j.smi.TcpAddress;
import org.snmp4j.smi.TransportIpAddress;
import org.snmp4j.smi.UdpAddress;
import org.snmp4j.smi.OID;

import com.synapsense.snmpagent.SNMPNotificationTarget;
import com.synapsense.util.SynapSNMPFatalException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

/**
 * The <code>SynapSNMPAgentCO</code> is a configuration storage for a
 * <code>SynapSNMPAgent</code> object
 * 
 * @author Maxim Bogatkin
 * @version 2.0
 */
public class SynapSNMPAgentCO {
	private final static Logger logger = Logger.getLogger(SynapSNMPAgentCO.class);

	private final static String CONFIG_FILE = "SNMPConf.xml";
	private InetAddress[] hosts = null;
	private int[] ports = null;
	private String[] protocols = null;

	private boolean enabled_v2c = false;
	private String communityPublic = null;
	private String communityPrivate = null;

	private boolean enabled_v3 = false;
	private String securityName = null;
	private String contextName = null;
	private boolean authentication_enabled = false;
	private String auth_protocol = null;
	private String auth_password = null;
	private boolean privacy_enabled = false;
	private String priv_protocol = null;
	private String priv_password = null;

	private InetAddress[] target_hosts = null;
	private int[] target_ports = null;
	private String[] target_protocols = null;
	private String[] target_versions = null;
	private int[] target_timeouts = null;
	private int[] target_retries = null;
	private int[] target_pingIntervals = null;

	private boolean agentXprotocolEnabled = false;
	private boolean isMaster = true;
	private InetAddress masterHostName = null;
	private int masterHostPort = -1;

	public SynapSNMPAgentCO() {

		SNMPAgent configuration = null;

		try {
			logger.info("Loading the configuaration.");
            JAXBContext context=   JAXBContext.newInstance(SNMPAgent.class);
            Unmarshaller unmarshaller  =context.createUnmarshaller();
            InputStream is = MIBManager.class.getClassLoader().getResourceAsStream(CONFIG_FILE);
            configuration = (SNMPAgent) unmarshaller.unmarshal(is);
            is.close();
		} catch (Exception e) {
			logger.error("Configuration file " + CONFIG_FILE + " is not found! Stopping");
			System.exit(1);
		}
		setValues(configuration);

	}

	private void setValues(SNMPAgent root) {
		SNMPConfiguration snmpConf = root.getSNMPConfiguration();
		List<NetworkInterface> netIf = snmpConf.networkInterfaces.getNetworkInterface();
		hosts = new InetAddress[netIf.size()];
		ports = new int[netIf.size()];
		protocols = new String[netIf.size()];
		int i = 0;
		for (NetworkInterface networkInterface : netIf) {
			try {
				hosts[i] = InetAddress.getByName(networkInterface.getHostname());
				ports[i] = networkInterface.getHostport();
				protocols[i] = networkInterface.getTransportProtocol().value();
			} catch (UnknownHostException e) {
				logger.fatal(e.getLocalizedMessage(), e);
				throw new SynapSNMPFatalException(e);
			}
			i++;
		}

		V2C v2c = snmpConf.getV2C();
		enabled_v2c = v2c.isEnabled();
		communityPublic = v2c.getCommunityStringPublic();
		communityPrivate = v2c.getCommunityStringPrivate();

		V3 v3 = snmpConf.getV3();
		enabled_v3 = v3.isEnabled();
		securityName = v3.getSecurityName();
		contextName = v3.getContextName();
		Authentication au = v3.getAuthentication();
		authentication_enabled = au.isEnabled();
		auth_protocol = au.getProtocol().value();
		auth_password = au.getPassword();
		Privacy pr = v3.getPrivacy();
		privacy_enabled = pr.isEnabled();
		priv_protocol = pr.getProtocol().value();
		priv_password = pr.getPassword();
		NotificationTargets nts = snmpConf.getNotificationTargets();
		List<NotificationTarget> nt = nts.getNotificationTarget();
		int cnt = nt.size();
		target_hosts = new InetAddress[cnt];
		target_ports = new int[cnt];
		target_protocols = new String[cnt];
		target_versions = new String[cnt];
		target_timeouts = new int[cnt];
		target_retries = new int[cnt];
		target_pingIntervals = new int[cnt];
		i = 0;
		for (NotificationTarget notificationTarget : nt) {
			try {
				target_hosts[i] = InetAddress.getByName(notificationTarget.getHostname());
			} catch (UnknownHostException e) {
				logger.warn(e.getLocalizedMessage(), e);
			}
			target_ports[i] = notificationTarget.getHostport();
			target_protocols[i] = protocols[0];
			target_versions[i] = notificationTarget.getSNMPversion().value();
			target_timeouts[i] = notificationTarget.getTimeout();
			target_retries[i] = notificationTarget.getRetriesCount();
			target_pingIntervals[i] = notificationTarget.getPingInterval();
			++i;
		}
		AgentXProtocol axp = snmpConf.getAgentXProtocol();
		agentXprotocolEnabled = axp.isEnabled();
		isMaster = "MASTER".equalsIgnoreCase(axp.getMode().value());
		try {
			masterHostName = InetAddress.getByName(axp.getMasterHostname());
		} catch (UnknownHostException e) {
			logger.warn(e.getLocalizedMessage(), e);
		}
		masterHostPort = axp.getMasterHostport();
	}

	/**
	 * Gets network interfaces
	 * 
	 * @param list
	 *            list to fill
	 */
	public void getNetworkInterfaces(List<TransportIpAddress> list) {
		for (int i = 0; i < hosts.length; i++) {
			TransportIpAddress addr = null;
			if ("UDP".equalsIgnoreCase(protocols[i])) {
				addr = new UdpAddress(hosts[i], ports[i]);
			} else if ("TCP".equalsIgnoreCase(protocols[i])) {
				addr = new TcpAddress(hosts[i], ports[i]);
			}
			if (addr != null) {
				list.add(addr);
			}
		}
	}

	/**
	 * Gets SNMP notification targets
	 * 
	 * @param list
	 *            list to fill
	 */
	public void getNotificationTargets(List<SNMPNotificationTarget> list) {
		for (int i = 0; i < target_hosts.length; i++) {
			TransportIpAddress addr = null;
			try {
				if ("UDP".equalsIgnoreCase(target_protocols[i])) {
					addr = new UdpAddress(target_hosts[i], target_ports[i]);
				} else if ("TCP".equalsIgnoreCase(target_protocols[i])) {
					addr = new TcpAddress(target_hosts[i], target_ports[i]);
				}
			} catch (Exception e) {
				// TODO: raise & handle synapagent exception
			}

			int version = -1;
			if ("v2c".equalsIgnoreCase(target_versions[i])) {
				version = SnmpConstants.version2c;
			} else if ("v3".equalsIgnoreCase(target_versions[i])) {
				version = SnmpConstants.version3;
			}
			list.add(new SNMPNotificationTarget(addr, target_pingIntervals[i], version, target_timeouts[i],
			        target_retries[i]));
		}
	}

	/**
	 * @return SNMP v2c public community string
	 */
	public OctetString getCommunityPublicString() {
		return new OctetString(communityPublic);
	}

	/**
	 * @return SNMP v2c private community string
	 */
	public OctetString getCommunityPrivateString() {
		return new OctetString(communityPrivate);
	}

	/**
	 * @return SNMP v3 security name
	 */
	public OctetString getSecurityName() {
		return new OctetString(securityName);
	}

	/**
	 * @return SNMP v3 context name
	 */
	public OctetString getContextName() {
		return new OctetString(contextName);
	}

	/**
	 * @return if SNMP v2c will be supported
	 */
	public boolean v2c_supported() {
		return enabled_v2c;
	}

	/**
	 * @return if SNMP v3 will be supported
	 */
	public boolean v3_supported() {
		return enabled_v3;
	}

	/**
	 * @return security level of the SNMP v3 implementation
	 */
	public int getSecurityLevel() {
		if (!enabled_v3 || !authentication_enabled) {
			return SecurityLevel.NOAUTH_NOPRIV;
		} else {
			if (privacy_enabled) {
				return SecurityLevel.AUTH_PRIV;
			} else {
				return SecurityLevel.AUTH_NOPRIV;
			}
		}
	}

	/**
	 * @return authentication protocol OID
	 */
	public OID getAuthenticationOID() {
		if ("MD5".equalsIgnoreCase(auth_protocol)) {
			return AuthMD5.ID;
		} else if ("SHA".equalsIgnoreCase(auth_protocol)) {
			return AuthSHA.ID;
		} else
			return null;
	}

	/**
	 * @return password for the authentication protocol
	 */
	public OctetString getAuthenticationPassword() {
		return new OctetString(auth_password);
	}

	/**
	 * @return privacy protocol OID
	 */
	public OID getPrivacyOID() {
		if ("DES".equalsIgnoreCase(priv_protocol)) {
			return PrivDES.ID;
		} else if ("AES".equalsIgnoreCase(priv_protocol) || "AES128".equalsIgnoreCase(priv_protocol)) {
			return PrivAES128.ID;
		} else if ("AES192".equalsIgnoreCase(priv_protocol)) {
			return PrivAES192.ID;
		} else if ("AES256".equalsIgnoreCase(priv_protocol)) {
			return PrivAES256.ID;
		} else
			return null;
	}

	/**
	 * @return password for the privacy protocol
	 */
	public OctetString getPrivacyPassword() {
		return new OctetString(priv_password);
	}

	/**
	 * @return if agent should support AgentX protocol (master/subagent) mode
	 */
	public boolean isXProtocolEnabled() {
		return agentXprotocolEnabled;
	}

	/**
	 * @return if agent will be work in master mode or subagent mode
	 */
	public boolean isMasterMode() {
		return isMaster;
	}

	/**
	 * @return <code>InetSocketAddress</code> of the master agent host
	 */
	public InetSocketAddress getMasterHost() {
		if ((masterHostName == null) || (masterHostPort == -1)) {
			return null;
		} else {
			return new InetSocketAddress(masterHostName, masterHostPort);
		}
	}
}
