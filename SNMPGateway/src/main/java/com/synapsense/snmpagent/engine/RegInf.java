package com.synapsense.snmpagent.engine;

import com.synapsense.service.nsvc.EventProcessor;

public class RegInf {
private EventProcessor dispatcher;
	private  EventProcessor filter;

    public RegInf(EventProcessor dispatcher, EventProcessor filter) {
        this.dispatcher = dispatcher;
        this.filter = filter;
    }

    public EventProcessor getDispatcher() {
        return dispatcher;
    }

    public void setDispatcher(EventProcessor dispatcher) {
        this.dispatcher = dispatcher;
    }

    public EventProcessor getFilter() {
        return filter;
    }

    public void setFilter(EventProcessor filter) {
        this.filter = filter;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RegInf regInf = (RegInf) o;

        if (!dispatcher.equals(regInf.dispatcher)) return false;
        if (!filter.equals(regInf.filter)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = dispatcher.hashCode();
        result = 31 * result + filter.hashCode();
        return result;
    }
}