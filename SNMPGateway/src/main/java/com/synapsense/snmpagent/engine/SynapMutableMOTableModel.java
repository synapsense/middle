package com.synapsense.snmpagent.engine;

import java.util.Map;

import org.apache.log4j.Logger;
import org.snmp4j.agent.mo.DefaultMOFactory;
import org.snmp4j.agent.mo.DefaultMOMutableTableModel;
import org.snmp4j.agent.mo.MOMutableTableRow;
import org.snmp4j.agent.mo.MOTableRow;
import org.snmp4j.smi.Gauge32;
import org.snmp4j.smi.Integer32;
import org.snmp4j.smi.OID;
import org.snmp4j.smi.OctetString;
import org.snmp4j.smi.SMIConstants;
import org.snmp4j.smi.TimeTicks;
import org.snmp4j.smi.Variable;

public class SynapMutableMOTableModel extends DefaultMOMutableTableModel {
	private final static Logger logger = Logger.getLogger(SynapMutableMOTableModel.class);
	private Map<String, Integer> columnNameToIdx;
	private Map<String, Integer> columnTypes;
	private String tableName;

	public SynapMutableMOTableModel(String tableName, Map<String, Integer> columnNameToIdx,
	        Map<String, Integer> columnTypes) {
		this.tableName = tableName;
		this.columnNameToIdx = columnNameToIdx;
		this.columnTypes = columnTypes;
	}

	public void setValueByColName(OID rowId, String columnName, Object data) {
		MOMutableTableRow tr = (MOMutableTableRow) getRow(rowId);
		if (tr != null) {
			Integer columnIdx = columnNameToIdx.get(columnName);
			if (!columnTypes.containsKey(columnName)) {
				logger.warn("Unable to set value [" + data + "]. Column for name [" + columnName
				        + "] is not found in table [" + tableName + "].");
				return;
			}
			tr.setValue(columnIdx, convData(data, columnTypes.get(columnName)));
		}
	}

	public void addRow(int[] rowId, Map<String, Object> rowData) {
		Variable[] values = new Variable[columnNameToIdx.size()];
		for (String columnName : columnNameToIdx.keySet()) {
			Object fieldData = rowData.get(columnName);
			if (fieldData == null) {
				if (!rowData.containsKey(columnName)) {
					if (logger.isDebugEnabled()) {
						logger.debug("No data for the column [" + tableName + "." + columnName
						        + "] is provided. Field will be skipped.");
					}
				}
				// fill the field with default value
				values[columnNameToIdx.get(columnName)] = getDefaultVariable(columnTypes.get(columnName));
			} else {
				values[columnNameToIdx.get(columnName)] = convData(rowData.get(columnName), columnTypes.get(columnName));
			}
		}
		MOTableRow row = DefaultMOFactory.getInstance().createRow(new OID(rowId), values);
		addRow(row);
	}

	/**
	 * Returns column name by it's index or empty string if column with the
	 * provided index is not found.
	 * 
	 * @param idx
	 *            Column index.
	 * @return Column name;
	 * 
	 */
	public String getColumnNameByIdx(Integer idx) {
		String columnName = "";
		if (idx != null) {
			for (String currentColName : columnNameToIdx.keySet()) {
				if (idx.equals(columnNameToIdx.get(currentColName))) {
					columnName = currentColName;
				}
			}
		}
		return columnName;
	}

	protected static Variable convData(Object objData, int syntax) {
		switch (syntax) {
		case SMIConstants.SYNTAX_OCTET_STRING:
			try {
				return new OctetString(toASCII(objData.toString()));
			} catch (Exception e) {
				return new OctetString("");
			}
		case SMIConstants.SYNTAX_GAUGE32:
			try {
				return new Gauge32((Long) objData);
			} catch (Exception e) {
				return new Gauge32(0L);
			}
		case SMIConstants.SYNTAX_INTEGER32:
			try {
				return new Integer32((Integer) objData);
			} catch (Exception e) {
				return new Gauge32(0L);
			}
		case SMIConstants.SYNTAX_TIMETICKS:
			try {
				return new TimeTicks((Integer) objData);
			} catch (Exception e) {
				return new TimeTicks(0);
			}
		default:
			try {
				return new Integer32((Integer) objData);
			} catch (Exception e) {
				return new Integer32(0);
			}
		}
	}

	protected static Variable getDefaultVariable(int syntax) {
		switch (syntax) {
		case SMIConstants.SYNTAX_OCTET_STRING:
			return new OctetString("");
		case SMIConstants.SYNTAX_GAUGE32:
			return new Gauge32(0L);
		case SMIConstants.SYNTAX_INTEGER32:
			return new Gauge32(0L);
		case SMIConstants.SYNTAX_TIMETICKS:
			return new TimeTicks(0);
		default:
			return new Integer32(0);
		}
	}

	private static String toASCII(String source) {
		byte[] sourceBytes = source.getBytes();
		StringBuffer buf = new StringBuffer(sourceBytes.length);
		for (int i = 0; i < sourceBytes.length; i++) {
			if ((Character.isISOControl((char) sourceBytes[i])) || ((sourceBytes[i] & 0xFF) < 0x80)) {
				buf.append((char) sourceBytes[i]);
			}
			else{
				buf.append(" ");
			}
		}
		return buf.toString();
	}
}
