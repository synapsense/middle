package com.synapsense.snmpagent.engine;

import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.log4j.Logger;
import com.synapsense.service.nsvc.EventProcessor;
import com.synapsense.service.nsvc.events.DispatchingProcessor;
import com.synapsense.service.nsvc.filters.ObjectCreatedEventFilter;
import com.synapsense.service.nsvc.filters.ObjectDeletedEventFilter;
import com.synapsense.service.nsvc.filters.PropertiesChangedEventFilter;
import com.synapsense.service.nsvc.filters.RelationRemovedEventFilter;
import com.synapsense.service.nsvc.filters.RelationSetEventFilter;
import com.synapsense.service.nsvc.filters.TOMatcher;
import com.synapsense.snmpagent.tables.AbstractScl;
import com.synapsense.snmpagent.tables.AbstractTbl;
import com.synapsense.snmpagent.tables.DataSource;
import com.synapsense.util.SynapEnv;

public class SynapDSStorage {

	private static final Logger logger = Logger.getLogger(SynapDSStorage.class);
	protected final static long updateInterval = 60000;
	private Map<Object, DataSource> ds = new HashMap<Object, DataSource>();
	private Map<DataSource, RegInf> tableDisp = new HashMap<DataSource, RegInf>();
	private Timer updateTimer;
	private static SynapDSStorage instance = new SynapDSStorage();

	private SynapDSStorage() {
		updateTimer = new Timer("DataSource update timer", true);
		updateTimer.schedule(new TimerTask() {

			@Override
			public void run() {
				for (DataSource item : ds.values()) {
					try {
						item.refresh();
						if(logger.isDebugEnabled()) {
							logger.debug("Refreshed "+item.getClass().getSimpleName() +" table`");
						}
					} catch (Throwable e) {
						logger.warn("Error updating table " + item.toString() + " by timer");
						if (logger.isDebugEnabled()) {
							logger.debug("Details: ", e);
						}
					}
				}
			}
		}, updateInterval, updateInterval);
	}

	public static SynapDSStorage getInstance() {
		return instance;
	}

	public void addTable(Object o, AbstractTbl t, String[] propList) {
		ds.put(o, t);
		if (t.isUseNSVC()) {
			registerTable(o, t, propList);
		}
	}

	public void addScalar(Object o, AbstractScl t) {
		ds.put(o, t);
	}

	private void registerTable(Object o, AbstractTbl t, String[] propList) {
		DispatchingProcessor dispatcher = new DispatchingProcessor();
		DispatchingProcessor filter = new DispatchingProcessor(true);
		TOMatcher matcher = new TOMatcher(o.toString());
		TOMatcher parent = new TOMatcher(".*");

		dispatcher.putOcep(t);
		dispatcher.putOdep(t);
		dispatcher.putPcep(t);
		dispatcher.putRsep(t);
		dispatcher.putRrep(t);

		filter.putOcep(new ObjectCreatedEventFilter(matcher));
		filter.putOdep(new ObjectDeletedEventFilter(matcher));
		filter.putPcep(new PropertiesChangedEventFilter(matcher, propList));
		filter.putRsep(new RelationSetEventFilter(parent, matcher));
		filter.putRrep(new RelationRemovedEventFilter(parent, matcher));

		RegInf ri = new RegInf(dispatcher, filter);
		tableDisp.put(t, ri);
	}

	public void registerProcessors() {
        for(DataSource ds : tableDisp.keySet()){
            RegInf processorToRegister = tableDisp.get(ds);
            logger.info("Registering event processor for table [" + ds.toString() + "]");
		    SynapEnv.getNsvc().registerProcessor(processorToRegister.getDispatcher(), processorToRegister.getFilter());
        }
	}

	public void reregisterDataSources() {
		for (DataSource dataSource : ds.values()) {
			try {
				logger.info("Reregistering datasource [" + dataSource.toString() + "]");
				dataSource.reregister();
			} catch (Exception e) {
				logger.warn("Unable to reregister datasource " + dataSource.toString()
				        + ". It will be skipped.", e);
			}
		}

	}

}
