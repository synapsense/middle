/**
 * 
 */
package com.synapsense.snmpagent;

import org.apache.log4j.Logger;
import org.apache.log4j.LogManager;

import org.snmp4j.smi.TransportIpAddress;

/**
 * The <code>SNMPNotificationTarget</code> represents a NMS in the system
 * 
 * @author Maxim Bogatkin
 * @version 2.0
 */
public class SNMPNotificationTarget {

	private static final Logger logger = LogManager.getLogger(SNMPNotificationTarget.class);

	/**
	 * Returns class logger
	 */
	public Logger getLogger() {
		return logger;
	}

	private TransportIpAddress address = null;
	private int snmpVersion = -1;
	private int timeout = -1;
	private int retriesCount = -1;

	private String AVAILABLE_NAME = "notificationTargetAvailableEvent";
	private String NOT_AVAILABLE_NAME = "notificationTargetNotAvailableEvent";

	/**
	 * Creates a <code>SNMPNotificationTarget</code> instance
	 * 
	 * @param address
	 *            address of the NMS
	 * @param pingInterval
	 *            ping interval for auditor
	 * @param snmpVersion
	 *            SNMP protocol version
	 * @param timeout
	 *            SNMP protocol timeout
	 * @param retriesCount
	 *            SNMP protocol retries count
	 */
	public SNMPNotificationTarget(TransportIpAddress address, int pingInterval, int snmpVersion, int timeout,
	        int retriesCount) {
		this.address = address;
		this.snmpVersion = snmpVersion;
		this.timeout = timeout;
		this.retriesCount = timeout;
	}

	/**
	 * @return <code>TransportIpAddress</code> of this target
	 */
	public TransportIpAddress getAddress() {
		return address;
	}

	/**
	 * @return SNMP version of this target
	 */
	public int getSNMPVersion() {
		return snmpVersion;
	}

	/**
	 * @return timeout for this target
	 */
	public int getTimeout() {
		return timeout;
	}

	/**
	 * @return retries count for this target
	 */
	public int getRetriesCount() {
		return retriesCount;
	}

	protected final String getAvailableCommandName() {
		return AVAILABLE_NAME;
	}

	protected final String getUnAvailableCommandName() {
		return NOT_AVAILABLE_NAME;
	}

	protected final String getAvailableCommandParameter() {
		return "" + address + " is available";
	}

	protected final String getUnAvailableCommandParameter() {
		return "" + address + " is not available";
	}

	protected boolean canRepair() {
		return false;
	}

	protected String getPingableName() {
		return "Notification target "
		        + (address == null ? "null" : address.getInetAddress().getHostAddress() + ":" + address.getPort());
	}

	protected boolean probe() {
		try {
			return address.getInetAddress().isReachable(timeout);
		} catch (Exception e) {
			return false;
		}

	}

}
