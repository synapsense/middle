package com.synapsense.snmpagent;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class SynapSNMP {
	public static SynapsenseAgent agent = null;
	private static Object lock = new Object();
	private static boolean stop = false;
	private static final Logger logger = LogManager.getLogger(SynapSNMP.class);

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		agent = new SynapsenseAgent();
		agent.startApps();
		agent.setDaemon(true);
		agent.start();
		logger.info("The agent has been started.");
		synchronized (lock) {
			while (!stop) {
				try {
					lock.wait();
				} catch (InterruptedException e) {
					logger.warn(e.getLocalizedMessage(), e);
				}
			}
		}
	}

	public static void stop() {
		agent.stopAgent();
		stop = true;
		synchronized (lock) {
			lock.notifyAll();
		}
		System.exit(0);
	}

}
