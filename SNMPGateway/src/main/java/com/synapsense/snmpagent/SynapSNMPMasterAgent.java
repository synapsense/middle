package com.synapsense.snmpagent;

import java.io.*;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import org.snmp4j.agent.*;
import org.snmp4j.agent.agentx.master.AgentXMasterAgent;
import org.snmp4j.agent.mo.snmp.*;

import org.apache.log4j.Logger;
import org.apache.log4j.LogManager;

import org.snmp4j.mp.*;
import org.snmp4j.security.*;
import org.snmp4j.smi.*;
import org.snmp4j.transport.*;
import org.snmp4j.util.ThreadPool;

import com.synapsense.util.SynapAgentException;
import com.synapsense.util.SynapExceptionCreator;
import com.synapsense.util.SynapExceptionHandler;

/**
 * The <code>SynapSNMPAgent</code> is a SNMP agent implementation based on the
 * SNMP4J-Agent framework. The <code>SynapSNMPAgent</code> extends the
 * <code>BaseAgent</code> It implements SynapSense MIB
 * <p>
 * 
 * @author Maxim Bogatkin, Vladimir Rodionov
 * @version 2.0
 */
public class SynapSNMPMasterAgent extends AgentXMasterAgent implements SynapSNMPAgent, SynapExceptionCreator {
	private List<ManagedObject> list;
	private String agentStatus = "";
	private String synapstoreStatus = "";
	private String databaseStatus = "";
	private String webserverStatus = "";
	private String synapvizserverStatus = "";
	private String webserverURL = "";
	private String synapvizserverURL = "";

	protected void sendColdStartNotification() {
		for (int i = 0; i < notificationTargets.size(); i++) {
			if (this.notificationTargets.get(i).getAddress() instanceof TcpAddress) {
				InetSocketAddress sa = new InetSocketAddress(notificationTargets.get(i).getAddress().getInetAddress(),
				        notificationTargets.get(i).getAddress().getPort());
				Socket s = new Socket();
				try {
					s.connect(sa);
					Address ipa = targets.getTargetAddress(new OctetString("notification" + i));
					if (ipa == null) {
						targets.addTargetAddress(new OctetString("notification" + i),
						        TransportDomains.transportDomainTcpIpv4, new OctetString(notificationTargets.get(i)
						                .getAddress().getValue()), notificationTargets.get(i).getTimeout(),
						        notificationTargets.get(i).getRetriesCount(), new OctetString("notify"),
						        new OctetString("v2c"), StorageType.permanent);
					}
				} catch (IOException e) {
					logger.warn("Target " + sa.getHostName() + ":" + sa.getPort() + " is not available.");
					targets.removeTargetAddress(new OctetString("notification" + i));
				}
			}
		}
		// super.sendColdStartNotification();
	}

	private static final Logger logger = LogManager.getLogger(SynapSNMPMasterAgent.class);

	/**
	 * Returns class logger
	 */
	public Logger getLogger() {
		return logger;
	}

	public static final String defConfigurationFile = "conf/SNMPConf.xml";

	private static final String SynapBootCounterFile = "conf/SynapSNMPMasterAgentBC.cfg";
	private static final String SynapAgentConfigFile = "conf/SynapSNMPMasterAgentCONFIG.cfg";
	private static final String RequestPoolName = "RequestPool";

	private List<SNMPNotificationTarget> notificationTargets = null;
	private List<TransportIpAddress> transportInterfaces = null;
	private SnmpTargetMIB targets = null;

	/*---------------------------- v2c ----------------------------*/

	private boolean v2c_supported = false;
	private OctetString communityStringPublic = null;
	private OctetString communityStringPrivate = null;

	/*---------------------------- v3 ----------------------------*/
	private boolean v3_supported = false;
	private OctetString securityName = null;
	private OctetString contextName = null;
	private int securityLevel = SecurityLevel.NOAUTH_NOPRIV;
	private OID authProtocol = null;
	private OctetString authPassword = null;
	private OID privProtocol = null;
	private OctetString privPassword = null;
	protected HashMap<String, OID> events;

	protected OID oidSynapsenseEventMessage = null;

	/**
	 * Creates <code>SynapSNMPAgent</code> object
	 * 
	 * @param configuration
	 *            configuration object
	 */
	public SynapSNMPMasterAgent(SynapSNMPAgentCO configuration) {
		super(new File(SynapBootCounterFile), new File(SynapAgentConfigFile));
		notificationTargets = new ArrayList<SNMPNotificationTarget>();
		transportInterfaces = new ArrayList<TransportIpAddress>();

		/* read conf */
		v2c_supported = configuration.v2c_supported();
		if (v2c_supported) {
			communityStringPublic = configuration.getCommunityPublicString();
			communityStringPrivate = configuration.getCommunityPrivateString();
		}

		v3_supported = configuration.v3_supported();
		if (v3_supported) {
			securityName = configuration.getSecurityName();
			contextName = configuration.getContextName();
			securityLevel = configuration.getSecurityLevel();
			if (securityLevel > SecurityLevel.NOAUTH_NOPRIV) {
				authProtocol = configuration.getAuthenticationOID();
				authPassword = configuration.getAuthenticationPassword();
			}
			if (securityLevel == SecurityLevel.AUTH_PRIV) {
				privProtocol = configuration.getPrivacyOID();
				privPassword = configuration.getPrivacyPassword();
			}
		}

		configuration.getNotificationTargets(notificationTargets);
		configuration.getNetworkInterfaces(transportInterfaces);
	}

	/**
	 * Initializes SNMP Agent
	 * 
	 */
	public void init() {
		super.initialize();
		this.getCommandProcessor().setWorkerPool(ThreadPool.create(RequestPoolName, 9));
		setLocalhostSubagentsOnly(false);
		if (v2c_supported) {
			agent.getServer(null).addContext(communityStringPublic);
		}
		if (v3_supported) {
			agent.getServer(null).addContext(contextName);
		}
		registerShutdownHook();

		events = MIBManager.getInstance().getEvents();
		oidSynapsenseEventMessage = MIBManager.getInstance().getEventMessageOID();
		sendColdStartNotification();
	}

	/**
	 * Register MIB tables from XML file at the agent's server.
	 */
	protected void registerManagedObjects() {
		if (list.size() == 0) {
			logger.warn("List of managed object is empty. Agent can not manage objects in MIB");
		}
		for (int i = 0; i < list.size(); i++) {
			String moDesc = (list.get(i)).toString();
			try {
				agent.getServer(null).register(list.get(i), null);
			} catch (DuplicateRegistrationException e) {
				new SynapExceptionHandler().handleException(SynapAgentException.getException(
				        SynapAgentException.ERROR_MIB_TABLE_TRING_REGISTERED_TWICE, new String[] { moDesc }, e), this);
			}
		}
	}

	/**
	 * Adds community to security name mappings needed for SNMPv1 and SNMPv2c.
	 * 
	 * @param communityMIB
	 *            the SnmpCommunityMIB holding coexistence configuration for
	 *            community based security models.
	 */
	protected void addCommunities(SnmpCommunityMIB communityMIB) {
		if (v2c_supported) {
			Variable[] com2sec = new Variable[] { new OctetString(communityStringPublic), // community
																						  // name
			        new OctetString("public"), // security name
			        agent.getContextEngineID(), // local engine ID
			        new OctetString(communityStringPublic), // default context
															// name
			        new OctetString(), // transport tag
			        new Integer32(StorageType.permanent), // storage type
			        new Integer32(RowStatus.active) // row status
			};
			communityMIB.getSnmpCommunityEntry().addRow(communityMIB.getSnmpCommunityEntry().createRow(
			        new OctetString("public2public").toSubIndex(true), com2sec));

			Variable[] com2sec2 = new Variable[] { new OctetString(communityStringPrivate), // community
																							// name
			        new OctetString("private"), // security name
			        agent.getContextEngineID(), // local engine ID
			        new OctetString(communityStringPublic), // default context
															// name
			        new OctetString(), // transport tag
			        new Integer32(StorageType.permanent), // storage type
			        new Integer32(RowStatus.active) // row status
			};
			communityMIB.getSnmpCommunityEntry().addRow(communityMIB.getSnmpCommunityEntry().createRow(
			        new OctetString("private2private").toSubIndex(true), com2sec2));
		}
	}

	/**
	 * Adds notification targets and filters.
	 * 
	 * @param targetMIB
	 *            the SnmpTargetMIB holding the target configuration.
	 * @param notificationMIB
	 *            the SnmpNotificationMIB holding the notification (filter)
	 *            configuration.
	 */
	protected void addNotificationTargets(SnmpTargetMIB targetMIB, SnmpNotificationMIB notificationMIB) {
		targetMIB.addDefaultTDomains();
		if (notificationTargets.size() == 0) {
			logger.warn("Notification list is empty. Agent will not send any notifications");
		}
		OID td = null;
		for (int i = 0; i < notificationTargets.size(); i++) {
			SNMPNotificationTarget target = notificationTargets.get(i);
			if (this.notificationTargets.get(i).getAddress() instanceof TcpAddress) {
				td = TransportDomains.transportDomainTcpIpv4;
			} else if (this.notificationTargets.get(i).getAddress() instanceof UdpAddress) {
				td = TransportDomains.transportDomainUdpIpv4;
			}
			if (target.getSNMPVersion() == SnmpConstants.version2c) {
				if (!v2c_supported)
					logger.error("v2c is disabled but target " + notificationTargets.get(i).getAddress()
					        + " have snmpversion: v2c");

				targetMIB.addTargetAddress(new OctetString("notification"), td, new OctetString(notificationTargets
				        .get(i).getAddress().getValue()), notificationTargets.get(i).getTimeout(), notificationTargets
				        .get(i).getRetriesCount(), new OctetString("notify"), new OctetString("v2c"),
				        StorageType.permanent);
				targetMIB.addTargetParams(new OctetString("v2c"), MessageProcessingModel.MPv2c,
				        SecurityModel.SECURITY_MODEL_SNMPv2c, communityStringPublic, securityLevel,
				        StorageType.permanent);
			} else if (target.getSNMPVersion() == SnmpConstants.version3) {
				if (!v3_supported)
					logger.error("v3 is disabled but target " + notificationTargets.get(i).getAddress()
					        + " have snmpversion: v3");

				targetMIB.addTargetAddress(new OctetString("notification"), td, new OctetString(notificationTargets
				        .get(i).getAddress().getValue()), notificationTargets.get(i).getTimeout(), notificationTargets
				        .get(i).getRetriesCount(), new OctetString("notify"), new OctetString("v3"),
				        StorageType.permanent);
				targetMIB.addTargetParams(new OctetString("v3"), MessageProcessingModel.MPv3,
				        SecurityModel.SECURITY_MODEL_USM, securityName, securityLevel, StorageType.permanent);
			}
		}
		notificationMIB.addNotifyEntry(new OctetString("default"), new OctetString("notify"),
		        SnmpNotificationMIB.SnmpNotifyTypeEnum.trap, StorageType.permanent);
		targets = targetMIB;
	}

	/**
	 * Adds all the necessary users to the USM.
	 * 
	 * @param usm
	 *            the USM instance used by this agent.
	 */
	protected void addUsmUser(USM usm) {
		if (v3_supported) {
			UsmUser user = new UsmUser(securityName, authProtocol, authPassword, privProtocol, privPassword);
			usm.addUser(user.getSecurityName(), usm.getLocalEngineID(), user);
		}
	}

	/**
	 * Adds VACM configuration.
	 * 
	 * @param vacm
	 *            the VacmMIB holding the agent's view configuration.
	 */
	protected void addViews(VacmMIB vacm) {
		if (v2c_supported) {
			vacm.addGroup(SecurityModel.SECURITY_MODEL_SNMPv1, this.communityStringPublic,
			        new OctetString("v1v2group"), StorageType.permanent);
			vacm.addGroup(SecurityModel.SECURITY_MODEL_SNMPv2c, this.communityStringPublic,
			        new OctetString("v1v2group"), StorageType.permanent);
			vacm.addGroup(SecurityModel.SECURITY_MODEL_SNMPv2c, this.communityStringPrivate, new OctetString(
			        "v1v2group_w"), StorageType.permanent);

			vacm.addAccess(new OctetString("v1v2group"), new OctetString(), SecurityModel.SECURITY_MODEL_ANY,
			        SecurityLevel.NOAUTH_NOPRIV, VacmMIB.VACM_MATCH_PREFIX, new OctetString("fullReadView"),
			        new OctetString(), new OctetString("fullNotifyView"), StorageType.permanent);
			vacm.addAccess(new OctetString("v1v2group_w"), new OctetString(), SecurityModel.SECURITY_MODEL_ANY,
			        SecurityLevel.NOAUTH_NOPRIV, VacmMIB.VACM_MATCH_PREFIX, new OctetString("fullReadView"),
			        new OctetString("fullWriteView"), new OctetString("fullNotifyView"), StorageType.permanent);

			vacm.addViewTreeFamily(new OctetString("fullReadView"), new OID("1.3"), new OctetString(),
			        VacmMIB.vacmViewIncluded, StorageType.permanent);
			vacm.addViewTreeFamily(new OctetString("fullWriteView"), new OID("1.3"), new OctetString(),
			        VacmMIB.vacmViewIncluded, StorageType.permanent);
			vacm.addViewTreeFamily(new OctetString("fullNotifyView"), new OID("1.3"), new OctetString(),
			        VacmMIB.vacmViewIncluded, StorageType.permanent);
		}

		if (v3_supported) {
			vacm.addGroup(SecurityModel.SECURITY_MODEL_USM, securityName, new OctetString("v3ourgroup"),
			        StorageType.permanent);
			vacm.addAccess(new OctetString("v3ourgroup"), contextName, SecurityModel.SECURITY_MODEL_USM, securityLevel,
			        VacmMIB.VACM_MATCH_EXACT, new OctetString("v3readview"), new OctetString("v3writeview"),
			        new OctetString("v3notifyview"), StorageType.permanent);
			vacm.addViewTreeFamily(new OctetString("v3readview"), new OID("1.3"), new OctetString(),
			        VacmMIB.vacmViewIncluded, StorageType.permanent);
			vacm.addViewTreeFamily(new OctetString("v3writeview"), new OID("1.3"), new OctetString(),
			        VacmMIB.vacmViewIncluded, StorageType.permanent);
			vacm.addViewTreeFamily(new OctetString("v3notifyview"), new OID("1.3"), new OctetString(), // 1.3.6.1.4.1.29078.1
			        VacmMIB.vacmViewIncluded, StorageType.permanent);
		}

	}

	/**
	 * Unregister additional managed objects from the agent's server.
	 */
	protected void unregisterManagedObjects() {
		list = MIBManager.getInstance().getMOList();
		for (int i = 0; i < list.size(); i++) {
			agent.getServer(null).unregister((ManagedObject) list.get(i), null);
		}
	}

	/**
	 * Initializes the transport mappings (ports) to be used by the agent.
	 * 
	 * @throws IOException
	 */
	protected void initTransportMappings() throws IOException {
		for (int i = 0; i < transportInterfaces.size(); i++) {
			TransportIpAddress address = transportInterfaces.get(i);
			if (address instanceof UdpAddress) {
				addAgentXTransportMapping(new DefaultUdpTransportMapping((UdpAddress) address));
			} else if (address instanceof TcpAddress) {
				addAgentXTransportMapping(new DefaultTcpTransportMapping((TcpAddress) address));
			}
		}
	}

	public String getStatus() {
		String res = "OK";

		return res;
	}

	@Override
	public void addTableIndexes(String tableName, List<String> indexes) {
		// TODO Auto-generated method stub

	}

	@Override
	public List<String> getTableIndexes(String tableName) {
		// TODO Auto-generated method stub
		return null;
	}

	public String getAgentStatus() {
		return agentStatus;
	}

	public String getSynapstoreStatus() {
		return synapstoreStatus;
	}

	public String getDatabaseStatus() {
		return databaseStatus;
	}

	public String getWebserverStatus() {
		return webserverStatus;
	}

	public String getWebserverURL() {
		return webserverURL;
	}

	public String getSynapvizserverURL() {
		return synapvizserverURL;
	}

	public String getSynapvizserverStatus() {
		return synapvizserverStatus;
	}

	@Override
	public List<ManagedObject> getMOList() {
		return list;
	}
}
