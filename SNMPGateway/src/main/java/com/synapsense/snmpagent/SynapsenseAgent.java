/**
 * 
 */
package com.synapsense.snmpagent;

import com.synapsense.util.SynapAgentException;
import com.synapsense.util.SynapExceptionCreator;
import com.synapsense.util.SynapExceptionHandler;
import org.apache.log4j.Logger;
import org.apache.log4j.LogManager;

/**
 * @author mbogat
 * 
 */
public class SynapsenseAgent extends Thread implements SynapExceptionCreator {

	private static final Logger logger = LogManager.getLogger(SynapsenseAgent.class);
	private static Object lock = new Object();

	private SynapSNMPAgent agent = null;

	/**
	 * Returns class logger
	 */
	public Logger getLogger() {
		return logger;
	}

	/**
	 * Creates a <code>SynapsenseAgent</code> instance
	 */
	SynapsenseAgent() {

	}

	public void startApps() {

		Runtime.getRuntime().addShutdownHook(new Thread() {
			public void run() {
				logger.info("good bye");
			}
		});

		try {
			loadResources();
			initSynapSNMPAgent();
			if (agent instanceof SynapSNMPMasterAgent) {
				((SynapSNMPMasterAgent) agent).init();
			} else if (agent instanceof SynapSNMPBaseAgent) {
				((SynapSNMPBaseAgent) agent).init();
			}

		} catch (SynapAgentException e) {
			new SynapExceptionHandler().handleException(e, this);
		}
	}

	private void initSynapSNMPAgent() throws SynapAgentException {
		SynapSNMPAgentCO conf = new SynapSNMPAgentCO();
		if (!conf.isXProtocolEnabled()) {
			agent = new SynapSNMPBaseAgent(conf);
		} else if (conf.isMasterMode()) {
			agent = new SynapSNMPMasterAgent(conf);
		} else {
			agent = new SynapSNMPSubAgent(conf);
		}
	}

	private void loadResources() throws SynapAgentException {
		MIBManager.getInstance().loadMIB();
	}

	private boolean stop = false;

	public void stopAgent() {
		stop = true;
		synchronized (lock) {
			lock.notifyAll();
		}
	}

	public void run() {
		((Runnable) agent).run();
		synchronized (lock) {
			while (!stop) {
				try {
					lock.wait();
				} catch (InterruptedException e) {

				}
			}
		}
	}

	public SynapSNMPAgent getAgent() {
		return agent;
	}
}
