package com.synapsense.snmpagent;

import org.snmp4j.agent.MOAccess;
import org.snmp4j.agent.mo.MOMutableColumn;
import org.snmp4j.log.Log4jLogFactory;
import org.snmp4j.log.LogFactory;

/**
 * Column which has a methods to access it's name.
 * 
 * @author Dmitry Malchikov
 */
public class SynapMOColumn extends MOMutableColumn {
	static {
		LogFactory.setLogFactory(new Log4jLogFactory());
	}

	private final String columnName;

	public SynapMOColumn(int columnID, int syntax, MOAccess access, String name) {
		super(columnID, syntax, access);
		columnName = name;
	}

	/**
	 * @return column name
	 */
	public String getColumnName() {
		return columnName;
	}

}
