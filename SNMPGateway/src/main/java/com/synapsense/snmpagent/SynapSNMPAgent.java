package com.synapsense.snmpagent;

import java.util.List;

import org.snmp4j.agent.ManagedObject;

/**
 * @author mbogat
 * 
 */
public interface SynapSNMPAgent {

	/**
	 * @return status of the agent
	 */
	public void addTableIndexes(String tableName, List<String> indexes);

	public List<String> getTableIndexes(String tableName);

	public List<ManagedObject> getMOList();
}
