package com.synapsense.snmpagent.tables;

import java.util.Map;

import org.apache.log4j.Logger;
import org.snmp4j.smi.Variable;

import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.service.impl.audit.Auditor;
import com.synapsense.service.impl.audit.AuditorSvc;
import com.synapsense.util.SynapAgentException;
import com.synapsense.util.SynapEnv;

public class AuditorScl extends AbstractScl {
	private static final Logger logger = Logger.getLogger(AuditorScl.class);

	public AuditorScl(String name, Variable variable) throws ObjectNotFoundException {
		super("AuditorScl-" + name, variable);
	}

	@Override
	protected void fillTable() {
		AuditorSvc ause = SynapEnv.getAuditorSvc();
		if (ause == null) {
			return;
		}
		String st = "Undefined";
		String[] lname = name.split("-");
		try {
			Map<String, Auditor> auds = ause.getAuditors();
			Auditor au = auds.get(lname[1]);

			if (au != null) {
				st = au.getLastStatus();
			}
		} catch (Exception e) {
			logger.error("failed to update auditor [" + lname + "], setting status to unknown.");
		}
		AgentStatusScl.setStatus(lname[1], st);
		variable.setValue(st);
	}

	@Override
	public void refresh() throws SynapAgentException {
		fillTable();
	}

}
