package com.synapsense.snmpagent.tables;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.snmp4j.smi.OID;

import com.synapsense.dto.TO;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.service.DRuleTypeService;
import com.synapsense.snmpagent.engine.SynapMutableMOTableModel;
import com.synapsense.util.SynapAgentException;
import com.synapsense.util.SynapEnv;

public class AlertCondTbl extends AbstractTbl {
	public AlertCondTbl(String name, OID baseOID, SynapMutableMOTableModel tableModel,
	        Map<Integer, Integer> columnIdToIdx) {
		super(name, baseOID, tableModel, columnIdToIdx, false);
	}

	private final static Logger logger = Logger.getLogger(AlertCondTbl.class);

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	protected void fillTable() {
		DRuleTypeService dRuleTypeDAO = SynapEnv.getDRuleTypeDAO();
		if (dRuleTypeDAO == null)
			return;
		Collection<TO<Integer>> dRuleTypes = dRuleTypeDAO.getAllRuleTypes();
		if (dRuleTypes != null) {
			Map<String, Object> newTableRow;
			for (TO<?> to : dRuleTypes) {
				newTableRow = new HashMap<String, Object>();
				Integer objId = (Integer) to.getID();
				newTableRow.put("alertconditionID", objId);
				try {
					newTableRow.put("alertconditionName", dRuleTypeDAO.getName(objId));
				} catch (ObjectNotFoundException e) {
					logger.error(e.getLocalizedMessage());
				}
				try {
					newTableRow.put("alertconditionExpression", dRuleTypeDAO.getSource(objId));
				} catch (ObjectNotFoundException e) {
					logger.error(e.getLocalizedMessage());
				}
				// TODO find out what data put in this column
				newTableRow.put("alertconditionDescription", "");
				tableModel.addRow(new int[] { objId }, newTableRow);
			}
		}
	}

	@Override
	public void refresh() throws SynapAgentException {
		tableModel.clear();
		fillTable();
	}
}
