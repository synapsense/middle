package com.synapsense.snmpagent.tables;

import com.synapsense.util.SynapAgentException;

public interface DataSource {

	public void refresh() throws SynapAgentException;

	public void reregister() throws SynapAgentException;

}
