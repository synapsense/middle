package com.synapsense.snmpagent.tables;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.snmp4j.agent.mo.MOTableRow;
import org.snmp4j.smi.OID;

import com.synapsense.dto.ChangedValueTO;
import com.synapsense.dto.CollectionTO;
import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.service.Environment;
import com.synapsense.service.nsvc.events.PropertiesChangedEvent;
import com.synapsense.snmpagent.engine.SynapMutableMOTableModel;
import com.synapsense.util.SynapAgentException;
import com.synapsense.util.SynapEnv;

public class SensorTbl extends AbstractTbl {
	private final static Logger logger = Logger.getLogger(SensorTbl.class);
	private final static String DTS = "dataTimestamp";
	private final static String LVI = "lastValueInt";
	private final static String PARENTTYPE = "WSNNODE";
	private static ArrayList<String> dblValues = new ArrayList<String>();
	static {
		dblValues.add("rMin");
		dblValues.add("rMax");
		dblValues.add("aMin");
		dblValues.add("aMax");
		dblValues.add("min");
		dblValues.add("max");
	}
	private final static String[] propNames = { "channel", "type", "name", "rMin", "rMax", "aMin", "aMax", "min",
	        "max", "lastValue", "dataTimestamp", "numAlerts", "lastValueInt" };

	public SensorTbl(String name, OID baseOID, SynapMutableMOTableModel tableModel, Map<Integer, Integer> columnIdToIdx) {
		super(name, baseOID, tableModel, columnIdToIdx, true);
		parentType = PARENTTYPE;
	}

	@Override
	public PropertiesChangedEvent process(PropertiesChangedEvent arg0) {
		super.process(arg0);

		for (ChangedValueTO changedValueTO : arg0.getChangedValues()) {
			if ("lastValue".equals(changedValueTO.getPropertyName())) {
				Integer rowId = (Integer) arg0.getObjectTO().getID();
				int[] indexLower = new int[] { rowId, 0 };
				int[] indexUpper = new int[] { rowId, OID.MAX_SUBID_VALUE };
				@SuppressWarnings("unchecked")
				List<MOTableRow> rows = tableModel.getRows(new OID(indexLower), new OID(indexUpper));
				if (!rows.isEmpty() && rows.size() == 1) {
					OID oid = rows.get(0).getIndex();
					tableModel.setValueByColName(oid, DTS, new Date(changedValueTO.getTimeStamp()).toString());

					Double lv = (Double) changedValueTO.getValue();
					// TODO fix conversion
					Integer intLastVal = (new Double(lv * 100.0)).intValue()
					        + (((new Double(lv * 1000.0)).intValue() % 10) > 5 ? 1 : 0);
					tableModel.setValueByColName(oid, LVI, intLastVal);
				}
			}

		}
		return null;
	};

	@Override
	protected void fillTable() {
		Environment es = SynapEnv.getEnv();
		if (es == null) {
			return;
		}
		Collection<TO<?>> tableRows = es.getObjectsByType(name);
		if (tableRows == null || tableRows.size() == 0) {
			logger.warn(name + " is empty");
		} else {
			Collection<CollectionTO> allObjectsAndProperties = es.getPropertyValue(tableRows, propNames);
			Map<String, Object> newTableRow;
			for (CollectionTO someObject : allObjectsAndProperties) {

				List<ValueTO> allProperties = someObject.getPropValues();
				if (allProperties == null) {
					logger.error("Object " + name + "(" + someObject.getObjId() + ") hasn't properties.");
				} else {
					TO<?> to = someObject.getObjId();
					Integer objId = (Integer) to.getID();
					Integer parentId = 0;
					Collection<TO<?>> parents;
					try {
						parents = es.getParents(to, PARENTTYPE);

						if (parents.iterator().hasNext()) {
							parentId = (Integer) parents.iterator().next().getID();
						}
					} catch (ObjectNotFoundException e1) {
						logger.warn("Can't find object's parent: " + to);
					}
					newTableRow = new HashMap<String, Object>();
					newTableRow.put(ID1, objId);
					newTableRow.put(ID2, parentId);

					for (ValueTO valueTO : allProperties) {
						valueTO.getPropertyName();
						newTableRow.put(valueTO.getPropertyName(), valueTO.getValue());

						if ("lastValue".equals(valueTO.getPropertyName())) {
							newTableRow.put(DTS, new Date(valueTO.getTimeStamp()).toString());

							Double lv = (Double) valueTO.getValue();
							// TODO fix conversion
							Integer intLastVal = (new Double(lv * 100.0)).intValue()
							        + (((new Double(lv * 1000.0)).intValue() % 10) > 5 ? 1 : 0);
							newTableRow.put(LVI, intLastVal);

						}
					}
					tableModel.addRow(new int[] { objId, parentId }, newTableRow);
				}

			}
		}
	}

	@Override
	public void setValue(Object id1, Object id2, Object value) throws SynapAgentException {
		Object dVal;
		if (dblValues.contains(id2)) {
			dVal = Double.parseDouble((String) value);
		} else {
			dVal = value;
		}
		super.setValue(id1, id2, dVal);
	}

	@Override
	public void refresh() throws SynapAgentException {
		// do nothing
	}

	@Override
	public void reregister() throws SynapAgentException {
		tableModel.clear();
		fillTable();
	}
}
