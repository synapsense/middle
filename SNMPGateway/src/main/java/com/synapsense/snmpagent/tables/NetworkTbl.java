package com.synapsense.snmpagent.tables;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.snmp4j.smi.OID;

import com.synapsense.dto.CollectionTO;
import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.service.Environment;
import com.synapsense.snmpagent.engine.SynapMutableMOTableModel;
import com.synapsense.util.SynapAgentException;
import com.synapsense.util.SynapEnv;

public class NetworkTbl extends AbstractTbl {
	public NetworkTbl(String name, OID baseOID, SynapMutableMOTableModel tableModel, Map<Integer, Integer> columnIdToIdx) {
		super(name, baseOID, tableModel, columnIdToIdx, true);
	}

	private final static String[] propNames = { "name", "desc", "numAlerts", "version", "panId" };
	private final static Logger logger = Logger.getLogger(NetworkTbl.class);
	private final static String NODE_TYPE_NAME = "WSNNODE";
	private final static String NODESCOUNT = "networkNodesCount";
	private final static String RESETCOMAND = "networkResetCommand";

	@Override
	protected void fillTable() {
		Environment es = SynapEnv.getEnv();
		if (es == null) {
			return;
		}
		Collection<TO<?>> tableRows = es.getObjectsByType(name);
		if (tableRows == null || tableRows.size() == 0) {
			logger.warn(name + " is empty");
		} else {
			Collection<CollectionTO> allObjectsAndProperties = es.getPropertyValue(tableRows, propNames);
			Map<String, Object> newTableRow;
			for (CollectionTO someObject : allObjectsAndProperties) {
				if (someObject != null) {
					TO<?> to = someObject.getObjId();
					List<ValueTO> allProperties = someObject.getPropValues();
					Integer nodesCount = 0;
					Collection<TO<?>> coTO;
					try {
						coTO = es.getChildren(to, NODE_TYPE_NAME);
						nodesCount = coTO.size();
					} catch (ObjectNotFoundException e) {
						logger.error("Unable to find WSNNETWORK object " + to + " while getting it's child nodes.");
					}

					if (allProperties == null) {
						logger.error("Object " + name + "(" + to + ") has no properties.");
					} else {

						Integer objId = (Integer) to.getID();
						newTableRow = new HashMap<String, Object>();
						newTableRow.put(ID1, objId);
						newTableRow.put(NODESCOUNT, nodesCount);
						for (ValueTO field : allProperties) {
							String propertyName = field.getPropertyName();
							newTableRow.put(propertyName, field.getValue());
						}

						newTableRow.put(RESETCOMAND, new Integer(0));

						tableModel.addRow(new int[] { objId }, newTableRow);
					}
				}
			}
		}
	}

	@Override
	public void refresh() throws SynapAgentException {
		Environment es = SynapEnv.getEnv();
		if (es == null) {
			return;
		}

		Collection<TO<?>> tableRows = es.getObjectsByType(name);
		if (tableRows == null || tableRows.size() == 0) {
			logger.warn(name + " is empty");
		} else {
			for (TO<?> to : tableRows) {
				Integer nodesCount = 0;
				Collection<TO<?>> coTO;
				try {
					coTO = es.getChildren(to, NODE_TYPE_NAME);
					nodesCount = coTO.size();
				} catch (ObjectNotFoundException e) {
					logger.error("Unable to find WSNNETWORK object " + to + " while getting it's child nodes.");
				}
				tableModel.setValueByColName(new OID(new int[] { (Integer) to.getID() }), NODESCOUNT, nodesCount);
			}
		}
	}

	@Override
	public void reregister() throws SynapAgentException {
		tableModel.clear();
		fillTable();
	}

}
