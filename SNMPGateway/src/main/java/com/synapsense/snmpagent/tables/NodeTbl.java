package com.synapsense.snmpagent.tables;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.snmp4j.smi.OID;

import com.synapsense.dto.CollectionTO;
import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.service.Environment;
import com.synapsense.snmpagent.engine.SynapMutableMOTableModel;
import com.synapsense.util.SynapAgentException;
import com.synapsense.util.SynapEnv;

public class NodeTbl extends AbstractTbl {

	private final static Logger logger = Logger.getLogger(NodeTbl.class);
	private final static String[] propNames = { "mac", "name", "location", "desc", "nodeAppVersion", "nodeNwkVersion",
	        "nodeOSVersion", "period", "numAlerts", "x", "y" };
	private final static Set<String> COMMANDS = new HashSet<String>();
	static {
		COMMANDS.add("reset");
		COMMANDS.add("identify");
		COMMANDS.add("wakeup");
		COMMANDS.add("nodeDeleteCommand");
	}

	private final static String SENSORCOUNT = "nodeSensorCount";
	private final static String PARENTTYPE = "WSNNETWORK";
	private final static String SENSOR_TYPE_NAME = "WSNSENSOR";

	private static ArrayList<String> dblValues = new ArrayList<String>();
	static {
		dblValues.add("x");
		dblValues.add("y");
	}

	public NodeTbl(String name, OID baseOID, SynapMutableMOTableModel tableModel, Map<Integer, Integer> columnIdToIdx) {
		super(name, baseOID, tableModel, columnIdToIdx, true);
		parentType = PARENTTYPE;
	}

	@Override
	protected void fillTable() {
		Environment es = SynapEnv.getEnv();
		if (es == null) {
			return;
		}
		Collection<TO<?>> tableRows = es.getObjectsByType(name);
		if (tableRows == null || tableRows.size() == 0) {
			logger.warn(name + " is empty");
		} else {
			Collection<CollectionTO> allObjectsAndProperties = es.getPropertyValue(tableRows, propNames);
			Map<String, Object> newTableRow;
			for (CollectionTO someObject : allObjectsAndProperties) {
				if (someObject != null) {
					List<ValueTO> allProperties = someObject.getPropValues();
					if (allProperties == null) {
						logger.error("Object " + name + "(" + someObject.getObjId() + ") hasn't properties.");
					} else {
						TO<?> to = someObject.getObjId();
						Integer objId = (Integer) to.getID();
						Integer parentId = 0;
						Collection<TO<?>> parents;
						try {
							parents = es.getParents(to, PARENTTYPE);

							if (parents.iterator().hasNext()) {
								parentId = (Integer) parents.iterator().next().getID();
							}
						} catch (ObjectNotFoundException e1) {
							logger.warn("Can't find object's parent: " + to);
						}
						newTableRow = new HashMap<String, Object>();
						newTableRow.put(ID1, objId);
						newTableRow.put(ID2, parentId);
						Integer sensorsCount = 0;

						Collection<TO<?>> coTO;
						try {
							coTO = es.getChildren(to, SENSOR_TYPE_NAME);
							sensorsCount = coTO.size();
						} catch (ObjectNotFoundException e) {
							logger.error("Unable to find WSNNODE object " + to + " while getting it's child nodes.");
						}
						newTableRow.put(SENSORCOUNT, sensorsCount);
						for (ValueTO valueTO : allProperties) {
							String propertyName = valueTO.getPropertyName();
							newTableRow.put(propertyName, valueTO.getValue());
						}
						for (String command : COMMANDS) {
							newTableRow.put(command, new Integer(0));
						}

						tableModel.addRow(new int[] { objId, parentId }, newTableRow);
					}
				}
			}
		}

	}

	@Override
	public void refresh() throws SynapAgentException {
		Environment es = SynapEnv.getEnv();
		if (es == null) {
			return;
		}
		Collection<TO<?>> tableRows = es.getObjectsByType(name);
		if (tableRows == null || tableRows.size() == 0) {
			logger.warn(name + " is empty");
		} else {
			for (TO<?> to : tableRows) {
				Integer sensorCount = 0;
				Collection<TO<?>> coTO;
				try {
					coTO = es.getChildren(to, SENSOR_TYPE_NAME);
					sensorCount = coTO.size();
				} catch (ObjectNotFoundException e) {
					logger.error("Unable to find WSNNODE object " + to + " while getting it's child nodes.");
				}
				tableModel.setValueByColName(new OID(new int[] { (Integer) to.getID() }), SENSORCOUNT, sensorCount);
			}
		}

	}

	@Override
	public void setValue(Object id1, Object id2, Object value) throws SynapAgentException {
		Object dVal;
		if (dblValues.contains(id2)) {
			dVal = Double.parseDouble((String) value);
		} else {
			dVal = value;
		}
		super.setValue(id1, id2, dVal);
	}

	@Override
	public void reregister() throws SynapAgentException {
		tableModel.clear();
		fillTable();
	}

}
