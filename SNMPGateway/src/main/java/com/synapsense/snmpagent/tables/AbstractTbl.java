package com.synapsense.snmpagent.tables;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.snmp4j.agent.mo.DefaultMOFactory;
import org.snmp4j.agent.mo.MOChangeEvent;
import org.snmp4j.agent.mo.MOChangeListener;
import org.snmp4j.agent.mo.MOMutableTableModel;
import org.snmp4j.agent.mo.MOMutableTableRow;
import org.snmp4j.agent.mo.MOTableRow;
import org.snmp4j.smi.OID;
import org.snmp4j.smi.Variable;

import com.synapsense.dto.ChangedValueTO;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.dto.ValueTO;
import com.synapsense.service.nsvc.events.ObjectCreatedEvent;
import com.synapsense.service.nsvc.events.ObjectCreatedEventProcessor;
import com.synapsense.service.nsvc.events.ObjectDeletedEvent;
import com.synapsense.service.nsvc.events.ObjectDeletedEventProcessor;
import com.synapsense.service.nsvc.events.PropertiesChangedEvent;
import com.synapsense.service.nsvc.events.PropertiesChangedEventProcessor;
import com.synapsense.service.nsvc.events.RelationRemovedEvent;
import com.synapsense.service.nsvc.events.RelationRemovedEventProcessor;
import com.synapsense.service.nsvc.events.RelationSetEvent;
import com.synapsense.service.nsvc.events.RelationSetEventProcessor;
import com.synapsense.snmpagent.engine.SynapMutableMOTableModel;
import com.synapsense.util.SynapAgentException;
import com.synapsense.util.SynapEnv;

public abstract class AbstractTbl implements DataSource, ObjectCreatedEventProcessor, ObjectDeletedEventProcessor,
        PropertiesChangedEventProcessor, RelationSetEventProcessor, RelationRemovedEventProcessor, MOChangeListener {
	private final static Logger logger = Logger.getLogger(AbstractTbl.class);

	protected final static String ID1 = "ID1";
	protected final static String ID2 = "ID2";
	protected String name;
	protected String parentType;
	protected OID baseOID;
	protected SynapMutableMOTableModel tableModel;
	protected Map<Integer, Integer> columnIdToIdx;
	protected boolean useNSVC = false;

	public AbstractTbl(final String name, OID baseOID, SynapMutableMOTableModel tableModel,
	        Map<Integer, Integer> columnIdToIdx, boolean reg) {
		this.name = name;
		this.baseOID = baseOID;
		this.tableModel = tableModel;
		this.useNSVC = reg;
		this.columnIdToIdx = columnIdToIdx;
	}

	public void setValue(Object id1, Object id2, Object value) throws SynapAgentException {
		TO<?> totot = TOFactory.getInstance().loadTO(name + ":" + id1.toString());
		try {
			SynapEnv.getEnv().setPropertyValue(totot, id2.toString(), value);
		} catch (Exception e) {
			throw new SynapAgentException(e);
		}
	}

	@Override
	public ObjectCreatedEvent process(ObjectCreatedEvent arg0) {
		int[] index;
		Integer objId = (Integer) arg0.getObjectTO().getID();
		if (parentType != null && !parentType.isEmpty()) {
			index = new int[] { objId, 0 };
		} else {
			index = new int[] { objId };
		}
		Map<String, Object> rowData = new HashMap<String, Object>();
		// first let's add object id to the column ID1
		rowData.put(ID1, objId);
		ValueTO[] valueTOs = arg0.getValues();
		// now add the rest of the properties to the columns with corresponding
		// names
		for (ValueTO valueTO : valueTOs) {
			rowData.put(valueTO.getPropertyName(), valueTO.getValue());
		}
		// add a new row to the table
		tableModel.addRow(index, rowData);
		return null;
	}

	@Override
	public ObjectDeletedEvent process(ObjectDeletedEvent arg0) {
		int[] index;
		Integer rowId = (Integer) arg0.getObjectTO().getID();
		if (parentType != null && !parentType.isEmpty()) {
			index = new int[] { rowId, 0 };
		} else {
			index = new int[] { rowId };
		}
		@SuppressWarnings("unchecked")
		List<MOTableRow> rowsToDelete = tableModel.getRows(new OID(index), null);
		for (MOTableRow row : rowsToDelete) {
			tableModel.removeRow(row.getIndex());
		}
		return null;
	}

	@Override
	public PropertiesChangedEvent process(PropertiesChangedEvent arg0) {
		Integer rowId = (Integer) arg0.getObjectTO().getID();

		OID oid;
		if (parentType != null && !parentType.isEmpty()) {

			int[] indexLower = new int[] { rowId, 0 };
			int[] indexUpper = new int[] { rowId, OID.MAX_SUBID_VALUE };
			@SuppressWarnings("unchecked")
			List<MOTableRow> rows = tableModel.getRows(new OID(indexLower), new OID(indexUpper));
			if (!rows.isEmpty() && rows.size() == 1) {
				oid = rows.get(0).getIndex();
			} else {
				logger.warn("Unable to find row for Object [" + arg0.getObjectTO()
				        + "] during PropertiesChangedEvent handling. Properties for this object will not be updated.");
				return null;
			}

		} else {
			int[] index = new int[] { rowId };
			oid = new OID(index);
		}
		ChangedValueTO[] values = arg0.getChangedValues();
		for (ChangedValueTO changedValueTO : values) {
			if (logger.isDebugEnabled()) {
				logger.debug("Value of the property [" + changedValueTO.getPropertyName() + "] for object with TO ["
				        + arg0.getObjectTO() + "] is changed from " + changedValueTO.getOldValue() + " to "
				        + changedValueTO.getValue());
			}
			tableModel.setValueByColName(oid, changedValueTO.getPropertyName(), changedValueTO.getValue());
		}
		return null;
	}

	@Override
	public RelationRemovedEvent process(RelationRemovedEvent arg0) {
		if (parentType != null && !parentType.isEmpty() && parentType.equals(arg0.getParentTO().getTypeName())) {
			Integer rowId = (Integer) arg0.getChildTO().getID();
			Integer parentId = (Integer) arg0.getParentTO().getID();
			OID idx = new OID(new int[] { rowId, parentId });
			// first update the row data
			tableModel.setValueByColName(idx, ID2, 0);
			MOMutableTableRow row = (MOMutableTableRow) tableModel.getRow(idx);
			if (row != null) {
				// now replace a row with the new one with updated data and new
				// index
				tableModel.removeRow(row.getIndex());
				Variable[] values = new Variable[row.size()];
				for (int i = 0; i < row.size(); i++) {
					values[i] = row.getValue(i);
				}
				tableModel.addRow(DefaultMOFactory.getInstance().createRow(new OID(new int[] { rowId, 0 }), values));
			}
		}
		return null;
	}

	@Override
	public RelationSetEvent process(RelationSetEvent arg0) {
		if (parentType != null && !parentType.isEmpty() && parentType.equals(arg0.getParentTO().getTypeName())) {
			Integer rowId = (Integer) arg0.getChildTO().getID();
			Integer parentId = (Integer) arg0.getParentTO().getID();
			OID idx = new OID(new int[] { rowId, 0 });
			// first update the row data
			tableModel.setValueByColName(idx, ID2, parentId);
			MOMutableTableRow row = (MOMutableTableRow) tableModel.getRow(idx);
			if (row != null) {
				// now replace a row with the new one with updated data and new
				// index
				tableModel.removeRow(row.getIndex());
				Variable[] values = new Variable[row.size()];
				for (int i = 0; i < row.size(); i++) {
					values[i] = row.getValue(i);
				}
				tableModel.addRow(DefaultMOFactory.getInstance().createRow(new OID(new int[] { rowId, parentId }),
				        values));
			}
		}
		return null;
	}

	/**
	 * By default just refresh table. For the tables, which use Notification
	 * Service we need to do more since some notifications might be missed.
	 */
	@Override
	public void reregister() throws SynapAgentException {
		refresh();
	}

	protected abstract void fillTable();

	public MOMutableTableModel getTableModel() {
		return tableModel;
	}

	public boolean isUseNSVC() {
		return useNSVC;
	}

	@Override
	public String toString() {
		return name;
	}

	@Override
	public void beforePrepareMOChange(MOChangeEvent changeEvent) {
	}

	@Override
	public void afterPrepareMOChange(MOChangeEvent changeEvent) {
	}

	@Override
	public void beforeMOChange(MOChangeEvent changeEvent) {
	}

	@Override
	public void afterMOChange(MOChangeEvent changeEvent) {
		String newValue = changeEvent.getNewValue().toString();
		int objId;
		int columnId;
		OID eventOID = changeEvent.getOID();
		int[] idArray = eventOID.toIntArray();
		int length = idArray.length;
		if (parentType == null || parentType.isEmpty()) {
			objId = idArray[length - 1];
			columnId = idArray[length - 2];
		} else {
			objId = idArray[length - 2];
			columnId = idArray[length - 3];
		}
		int columnIdx = columnIdToIdx.get(columnId);

		String propName = tableModel.getColumnNameByIdx(columnIdx);
		if (!propName.isEmpty()) {
			try {
				setValue(objId, propName, newValue);
			} catch (SynapAgentException e) {
				logger.error("Unable to set value to ES for object with id " + objId + ", property " + propName
				        + ", and value " + newValue);
			}
		}
	}
}
