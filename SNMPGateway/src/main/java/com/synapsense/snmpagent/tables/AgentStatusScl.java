package com.synapsense.snmpagent.tables;

import java.util.HashMap;
import java.util.Map.Entry;

import org.snmp4j.smi.Variable;

import com.synapsense.util.SynapAgentException;

public class AgentStatusScl extends AbstractScl {

	public AgentStatusScl(String name, Variable variable) {
		super(name, variable);
	}

	private static HashMap<String, String> statuses = new HashMap<String, String>();

	@Override
	protected void fillTable() {
		StringBuffer sb = new StringBuffer();
		StringBuffer tsb = new StringBuffer();

		for (Entry<String, String> elt : statuses.entrySet()) {
			sb.append(elt.getValue());
			sb.append("; ");
			tsb.append("OK");
			tsb.append("; ");
		}
		if (tsb.toString().equalsIgnoreCase(sb.toString())) {
			variable.setValue("OK");
		} else {
			variable.setValue(sb.toString());
		}
	}

	public static void setStatus(String k, String v) {
		statuses.put(k, v);
	}

	@Override
	public void refresh() throws SynapAgentException {
		fillTable();
	}
}
