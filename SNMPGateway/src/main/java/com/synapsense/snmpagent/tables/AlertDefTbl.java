package com.synapsense.snmpagent.tables;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.snmp4j.smi.OID;

import com.synapsense.dto.Alert;
import com.synapsense.dto.AlertType;
import com.synapsense.dto.MessageTemplate;
import com.synapsense.dto.MessageType;
import com.synapsense.dto.TO;
import com.synapsense.service.AlertService;
import com.synapsense.service.Environment;
import com.synapsense.snmpagent.engine.SynapMutableMOTableModel;
import com.synapsense.util.SynapAgentException;
import com.synapsense.util.SynapEnv;

public class AlertDefTbl extends AbstractTbl {

	public AlertDefTbl(String name, OID baseOID, SynapMutableMOTableModel tableModel,
	        Map<Integer, Integer> columnIdToIdx) {
		super(name, baseOID, tableModel, columnIdToIdx, false);
	}

	@Override
	protected void fillTable() {
		Environment es = SynapEnv.getEnv();
		AlertService as = SynapEnv.getAS();
		if (es == null || as == null)
			return;
		Collection<TO<?>> nets = es.getObjectsByType("WSNNETWORK");
		if (nets != null && !nets.isEmpty()) {
			Collection<Alert> activeAlerts = as.getActiveAlerts(nets, true);
			HashMap<String, Integer> typeToCount = new HashMap<String, Integer>();
			HashMap<String, String> typeToList = new HashMap<String, String>();
			for (Alert alert : activeAlerts) {
				String type = alert.getAlertType();
				Integer c = 1;
				if (typeToCount.containsKey(type)) {
					c = typeToCount.get(type);
					++c;
				}
				typeToCount.put(type, c);
				String l = alert.getName();
				if (typeToList.containsKey(type)) {
					l = typeToList.get(type);
					l += "; " + alert.getName();
				}
				typeToList.put(type, l);
			}
			Collection<AlertType> alTypes = as.getAllAlertTypes();
			int i = 1;
			Map<String, Object> newTableRow;
			for (AlertType alertType : alTypes) {
				newTableRow = new HashMap<String, Object>();
				newTableRow.put("alertdefinitionID", i);
				newTableRow.put("alertName", alertType.getName());
				newTableRow.put("alertDescription", alertType.getDescription());
				newTableRow.put("alertPriority", alertType.getPriority().toString());

				MessageTemplate emailTemplate = alertType.getMessageTemplate(MessageType.SMTP);
				String header = null;
				String body = null;
				if (emailTemplate != null) {
					header = emailTemplate.getHeader(es);
					body = emailTemplate.getBody(es);
				}
				newTableRow.put("alertMsgHeader", header);
				newTableRow.put("alertMsgBody", body);

				Integer co = typeToCount.get(alertType.getName());
				if (co == null) {
					co = 0;
				}
				newTableRow.put("alertActiveCount", co);
				String li = typeToList.get(alertType.getName());
				if (li == null) {
					li = "";
				}
				newTableRow.put("alertActiveList", li);
				tableModel.addRow(new int[] { i }, newTableRow);
				i++;
			}
		}
	}

	@Override
	public void refresh() throws SynapAgentException {
		tableModel.clear();
		fillTable();
	}
}
