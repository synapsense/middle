package com.synapsense.snmpagent.tables;

import org.apache.log4j.Logger;
import org.snmp4j.smi.Variable;

import com.synapsense.util.SynapAgentException;

public class ConstantScl extends AbstractScl {
	private final static Logger logger = Logger.getLogger(ConstantScl.class);

	public ConstantScl(String name, Variable variable) {
		super(name, variable);
		logger.info(name + "=:=" + variable.toString());
	}

	@Override
	protected void fillTable() {
	}

	@Override
	public void refresh() throws SynapAgentException {
	}

}
