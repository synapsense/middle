package com.synapsense.snmpagent.tables;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import com.synapsense.dto.TO;
import org.apache.log4j.Logger;
import org.snmp4j.smi.OID;

import com.synapsense.service.UserManagementService;
import com.synapsense.snmpagent.engine.SynapMutableMOTableModel;
import com.synapsense.util.SynapAgentException;
import com.synapsense.util.SynapEnv;

public class RecipientTbl extends AbstractTbl {

	private final static Logger logger = Logger.getLogger(RecipientTbl.class);

	public RecipientTbl(String name, OID baseOID, SynapMutableMOTableModel tableModel,
	        Map<Integer, Integer> columnIdToIdx) {
		super(name, baseOID, tableModel, columnIdToIdx, false);
	}

	@Override
	protected void fillTable() {
		UserManagementService userManSvc = SynapEnv.getUMS();
		if (userManSvc == null) {
			return;
		}
		Collection<TO<?>> ruleTypes;
		try {
			ruleTypes = userManSvc.getAllUsers();
		} catch (Exception e) {
			logger.debug("Unable to get data for the Recipients table. Insufficient permissions.");
			return;
		}
		if (ruleTypes != null) {
			Map<String, Object> newTableRow;
			for (TO<?> to : ruleTypes) {
				Integer objId = (Integer) to.getID();
				Map<String, Object> userProps = userManSvc.getUserInformation(to);
				newTableRow = new HashMap<String, Object>();
				newTableRow.put("recipientPersonID", objId);
				newTableRow.put("recipientFirstName", userProps.get(userManSvc.USER_FIRST_NAME));
				newTableRow.put("recipientMiddleName", userProps.get(userManSvc.USER_MIDDLE_INITIAL));
				newTableRow.put("recipientLastName", userProps.get(userManSvc.USER_LAST_NAME));
				newTableRow.put("recipientTitle", userProps.get(userManSvc.USER_TITLE));
				newTableRow.put("recipientEmail1", userProps.get(userManSvc.USER_PRIMARY_EMAIL));
				newTableRow.put("recipientEmail2", userProps.get(userManSvc.USER_SECONDARY_EMAIL));
				newTableRow.put("recipientSmsAddress", userProps.get(userManSvc.USER_SMS_ADDRESS));
				tableModel.addRow(new int[] { objId }, newTableRow);
			}
		}
	}

	@Override
	public void refresh() throws SynapAgentException {
		tableModel.clear();
		fillTable();
	}

}
