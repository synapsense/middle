package com.synapsense.snmpagent.tables;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.snmp4j.smi.OID;

import com.synapsense.dto.Alert;
import com.synapsense.dto.AlertFilter;
import com.synapsense.dto.AlertFilterBuilder;
import com.synapsense.dto.AlertType;
import com.synapsense.dto.TO;
import com.synapsense.service.AlertService;
import com.synapsense.service.Environment;
import com.synapsense.snmpagent.engine.SynapMutableMOTableModel;
import com.synapsense.util.SynapAgentException;
import com.synapsense.util.SynapEnv;

public class AlertTbl extends AbstractTbl {
	public AlertTbl(String name, OID baseOID, SynapMutableMOTableModel tableModel, Map<Integer, Integer> columnIdToIdx) {
		super(name, baseOID, tableModel, columnIdToIdx, false);
	}

	@Override
	protected void fillTable() {
		Environment es = SynapEnv.getEnv();
		AlertService as = SynapEnv.getAS();
		if (es == null || as == null)
			return;
		Collection<TO<?>> nets = es.getObjectsByType("WSNNETWORK");
		if (nets != null && !nets.isEmpty()) {
			final AlertFilter all = new AlertFilterBuilder().active().historical().system().onHosts(nets).propagate(true).build();
			Collection<Alert> alerts = as.getAlerts(all);
			Map<String, Object> newTableRow;
			for (Alert al : alerts) {
				Integer objId = al.getAlertId();
				newTableRow = new HashMap<String, Object>();
				newTableRow.put("alertID", objId);

				String[] atNames = { al.getAlertType() };
				Collection<AlertType> alTypes = as.getAlertTypes(atNames);
				for (AlertType alertType : alTypes) {
					newTableRow.put("alertAlertDefinitionsName", alertType.getName());
					break;
				}
				newTableRow.put("alertNodeID", al.getHostTO().getID());
				newTableRow.put("alertDataTimestamp", new Date(al.getAlertTime()).toString());
				newTableRow.put("alertSensorNodeData", al.getFullMessage());
				// TruthValue is defined in RFC 2579 with true = 1 and false = 2
				// if alert is active, it is not dismissed (2), it is dismissed
				// otherwise (1)
				newTableRow.put("alertDismissedFlag", al.getStatus().isActive() ? 2 : 1);
				tableModel.addRow(new int[] { objId }, newTableRow);
			}
		}
	}

	@Override
	public void setValue(Object id1, Object id2, Object value) throws SynapAgentException {
		if ("alertDismissedFlag".equalsIgnoreCase(id2.toString())) {
			AlertService as = SynapEnv.getAS();
			try {
				as.dismissAlert((Integer) id1, "dismissed through snmp agent");
			} catch (Exception e) {
				throw new SynapAgentException(e);
			}
		}
	}

	@Override
	public void refresh() throws SynapAgentException {
		tableModel.clear();
		fillTable();
	}

}
