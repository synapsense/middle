package com.synapsense.snmpagent.tables;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.snmp4j.smi.OID;

import com.synapsense.dto.CollectionTO;
import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.service.Environment;
import com.synapsense.snmpagent.engine.SynapMutableMOTableModel;
import com.synapsense.util.SynapAgentException;
import com.synapsense.util.SynapEnv;

public class RouteTbl extends AbstractTbl {
	private final static Logger logger = Logger.getLogger(RouteTbl.class);
	private final static String OBJTYPE = "WSNNODE";
	private final static String NETWORK_ID = "networkId";
	private final static String NEIGHBOR_PHYS_ID = "neighborPhysId";
	private final static String PROPNAME1 = "route";
	private final static String PROPNAME2 = "mac";
	private final static String PARENTTYPE = "WSNNETWORK";

	public RouteTbl(String name, OID baseOID, SynapMutableMOTableModel tableModel, Map<Integer, Integer> columnIdToIdx)
	        throws ObjectNotFoundException {
		super(name, baseOID, tableModel, columnIdToIdx, false);
	}

	@Override
	protected void fillTable() {
		Environment es = SynapEnv.getEnv();
		if (es == null) {
			return;
		}
		Collection<TO<?>> tableRows = es.getObjectsByType(OBJTYPE);
		if (tableRows == null || tableRows.size() == 0) {
			logger.warn(name + " is empty");
		} else {
			Collection<CollectionTO> properties = es.getPropertyValue(tableRows, new String[] { PROPNAME1, PROPNAME2 });
			int k = 1;
			Map<String, Object> newTableRow;
			for (CollectionTO someObject : properties) {
				List<ValueTO> allProperties = someObject.getPropValues();
				if (allProperties == null) {
					logger.error("Object " + name + "(" + someObject.getObjId() + ") has no properties.");
				} else {
					Integer netId = 0;

					Collection<TO<?>> parents;
					try {
						parents = es.getParents(someObject.getObjId(), PARENTTYPE);

						if (parents.iterator().hasNext()) {
							netId = (Integer) parents.iterator().next().getID();
						}
					} catch (ObjectNotFoundException e1) {
						logger.warn("Can't find object's parent: " + someObject.getObjId());
					}
					newTableRow = new HashMap<String, Object>();
					newTableRow.put(ID1, k);
					newTableRow.put(NETWORK_ID, netId);

					for (ValueTO valueTO : allProperties) {
						if (PROPNAME1.equals(valueTO.getPropertyName())) {
							Object value = "";
							List<TO<?>> nn = new ArrayList<TO<?>>();
							TO<?> to = (TO<?>) valueTO.getValue();
							if (to != null) {
								nn.add(to);
								Collection<CollectionTO> props = es.getPropertyValue(nn, new String[] { PROPNAME2 });
								if (props.size() == 1) {
									CollectionTO cto = props.iterator().next();
									List<ValueTO> lpvto = cto.getPropValue(PROPNAME2);
									if (lpvto != null) {
										if (cto.getPropValue(PROPNAME2).size() == 1) {
											value = cto.getPropValue(PROPNAME2).get(0).getValue();
										}
									}
								}
							}
							newTableRow.put(NEIGHBOR_PHYS_ID, value);
						} else {
							newTableRow.put(PROPNAME2, valueTO.getValue());

						}

					}
					tableModel.addRow(new int[] { k }, newTableRow);
				}
				k++;
			}
		}
	}

	@Override
	public void refresh() throws SynapAgentException {
		tableModel.clear();
		fillTable();
	}

}
