package com.synapsense.snmpagent.tables;

import org.apache.log4j.Logger;
import org.snmp4j.smi.AssignableFromString;
import org.snmp4j.smi.Variable;

public abstract class AbstractScl implements DataSource {
	private final static Logger logger = Logger.getLogger(AbstractScl.class);
	protected String name;
	protected AssignableFromString variable;

	public AbstractScl(final String name, Variable variable) {
		this.name = name;
		this.variable = (AssignableFromString) variable;
	}

	public void reregister() {
		try {
			fillTable();
		} catch (Throwable e) {
			logger.error("Cannot update scalar '" + name + "'");
		}
	}

	protected abstract void fillTable();

	@Override
	public String toString() {
		return name;
	}
}
