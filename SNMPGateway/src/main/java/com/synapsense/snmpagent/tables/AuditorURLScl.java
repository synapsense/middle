package com.synapsense.snmpagent.tables;

import java.util.Map;

import org.snmp4j.smi.Variable;

import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.service.impl.audit.Auditor;
import com.synapsense.service.impl.audit.AuditorSvc;
import com.synapsense.util.SynapAgentException;
import com.synapsense.util.SynapEnv;

public class AuditorURLScl extends AbstractScl {

	public AuditorURLScl(String name, Variable variable) throws ObjectNotFoundException {
		super("AuditorURLScl-" + name, variable);
	}

	@Override
	protected void fillTable() {
		AuditorSvc ause = SynapEnv.getAuditorSvc();
		if (ause == null) {
			return;
		}
		Map<String, Auditor> auds = ause.getAuditors();
		String[] lname = name.split("-");
		Auditor au = auds.get(lname[1]);
		String st = "Undefined";
		if (au != null) {
			st = au.getConfiguration();
		}
		variable.setValue(st);
	}

	@Override
	public void refresh() throws SynapAgentException {
		fillTable();
	}

}
