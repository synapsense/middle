package com.synapsense.util;

public class SynapSNMPFatalException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6716732886207183224L;

	public SynapSNMPFatalException() {
		super();
	}

	public SynapSNMPFatalException(String message, Throwable cause) {
		super(message, cause);
	}

	public SynapSNMPFatalException(String message) {
		super(message);
	}

	public SynapSNMPFatalException(Throwable cause) {
		super(cause);
	}

}
