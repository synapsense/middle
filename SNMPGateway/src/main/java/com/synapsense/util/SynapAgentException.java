package com.synapsense.util;

import java.util.HashMap;

/**
 * The <code>SynapAgentException</code> is a unified exception which should be
 * used wherever where it is possible. This class loads text of error messages
 * from file. Therefore SynapsenseAgent may be multilingval. Also it has error
 * code which allows to define which error is critical and which is not.
 * <p>
 * 
 * @author Maxim Bogatkin
 * @version 2.0
 */
public class SynapAgentException extends Exception {

	/**
	 * Auto generated Serial Version UID
	 */
	private static final long serialVersionUID = -7412818536417406423L;
	private final static String PARAMETER_STRING = "PARAM";
	public final static int SNMP_SVR_CRITICAL = 1;
	public final static int SNMP_SVR_ERROR = 2;
	public final static int SNMP_SVR_WARNING = 3;

	private static HashMap<Integer, String> errorMap = new HashMap<Integer, String>();

	/**
	 * Creates error text by error code. If message should contain parameter the
	 * list of their is provided also
	 * 
	 * @param errorCode
	 *            code of error message
	 * @param params
	 *            array of parameters
	 * 
	 */
	private static String getText(int errorCode, String[] params) {
		if (errorMap.containsKey(errorCode)) {
			String text = errorMap.get(errorCode);
			if (params != null) {
				for (int i = 0; i < params.length; i++) {

					text = text.replaceFirst(PARAMETER_STRING, "" + params[i]);
				}
			}
			return text;
		} else {
			// logger.warn("Error with code=" + errorCode + " does not exist");
			StringBuffer text = new StringBuffer("Null description");
			if (params != null) {
				text.append("(");
				for (int i = 0; i < params.length; i++) {
					if (i == params.length - 1) {
						text.append("param" + i + "=" + params[i] + "");
					} else {
						text.append("param" + i + "=" + params[i] + ", ");
					}
				}
				text.append(")");
			}
			return text.toString();
		}
	}

	/**
	 * Returns error text by error code. If message should contain parameter the
	 * list of their is provided also
	 * 
	 * @param errorCode
	 *            code of error message
	 * @param params
	 *            array of parameters
	 * 
	 */
	public final static SynapAgentException getException(int errorCode, String[] params, Exception cause) {
		SynapAgentException ex = new SynapAgentException(errorCode, getText(errorCode, params)
		        + ((cause != null) ? " caused by: " + cause.getMessage() : ""), cause);
		return ex;
	}

	/**
	 * Returns error text by error code without params.
	 * 
	 * @param errorCode
	 *            code of error message
	 * 
	 */
	public final static SynapAgentException getException(int errorCode, Exception cause) {
		return getException(errorCode, null, cause);
	}

	protected int errorCode;
	protected String errorText;
	protected int severity;

	/**
	 * Returns error code
	 * 
	 * @return error code
	 * 
	 */
	public int getErrorCode() {
		return errorCode;
	}

	/**
	 * Returns error text
	 * 
	 * @return error text
	 * 
	 */
	public String getErrorText() {
		return errorText;
	}

	/**
	 * Returns severity by error code
	 * 
	 * @return severity by error code
	 */
	public int getSeverity() {
		return severity;
	}

	/**
	 * Creates <code>SynapAgentException</code> object
	 * 
	 * @param errorCode
	 *            Error code
	 * @param errorText
	 *            Error text
	 * @param e
	 *            Nested error object
	 */
	private SynapAgentException(int errorCode, String errorText, Exception e) {
		super("Code " + errorCode + ", " + errorText, e);
		this.errorCode = errorCode;
		this.errorText = errorText;
		if (errorCode < 100)
			severity = SNMP_SVR_CRITICAL;
		else if (errorCode < 300)
			severity = SNMP_SVR_ERROR;
		else
			severity = SNMP_SVR_WARNING;
	}

	public SynapAgentException(Exception e) {
		super(e);
	}

	/* Critical exceptions */
	public final static int CRITICAL_CONFIG_FILE_NOT_FOUND = 1;
	public final static int CRITICAL_CONFIG_FILE_NOT_READABLE = 2;
	public final static int CRITICAL_CONFIG_FILE_IS_EMPTY = 3;
	public final static int CRITICAL_CONFIG_FILE_NOT_PROCESSED = 4;
	public final static int CRITICAL_SNMP_AGENT_NOT_INITIALIZED = 5;
	public final static int CRITICAL_MIBXML_FILE_NOT_FOUND = 6;
	public final static int CRITICAL_MIBXML_FILE_NOT_READABLE = 7;

	/* Module error exceptions */
	public final static int ERROR_ERR_MESSAGE_FILE_NOT_FOUND = 101;
	public final static int ERROR_DB_DRIVER_NOT_INITIALIZED = 102;
	public final static int ERROR_DB_CANNOT_CONNECT = 103;
	public final static int ERROR_DB_CANNOT_EXECUTE_QUERY = 104;
	public final static int ERROR_DB_CANNOT_READ_DB_DATA = 105;
	public final static int ERROR_MIB_CANNOT_CREATE_TABLE_FROM_NODE = 106;
	public final static int ERROR_MIB_TABLE_NOT_UPDATED = 107;
	public final static int ERROR_MIB_TABLE_TRING_REGISTERED_TWICE = 108;
	public final static int ERROR_SYS_CANNOT_GET_CONNECTION_FROM_SYSTEM = 109;
	public final static int ERROR_DB_CONNECTION_BROKEN = 110;
	public final static int ERROR_RES_CANNOT_LOAD_RESOURCE = 111;
	public final static int ERROR_ERR_MESSAGE_FILE_NOT_READABLE = 112;
	public final static int ERROR_MIB_CANNOT_CREATE_SCALAR_FROM_NODE = 113;
	public final static int ERROR_SSTORE_CANNOT_CREATE_SENDER = 114;
	public final static int ERROR_SSTORE_CANNOT_SEND_MESS_TO_GATEWAY = 115;

	public final static int ERROR_CONF_SYSTEM_ID_NOT_DEFINED = 201;
	public final static int ERROR_CONF_DB_NOT_FOUND = 202;
	public final static int ERROR_CONF_SYNSTORE_NOT_FOUND = 203;

	public final static int ERROR_CONF_DB_SERVER_NAME_NULL_OR_MISSING = 204;
	public final static int ERROR_CONF_DB_DRIVER_NAME_NULL_OR_MISSING = 204;
	public final static int ERROR_CONF_DB_HOST_NAME_NULL_OR_MISSING = 204;
	public final static int ERROR_CONF_DB_PORT_NULL_OR_MISSING = 204;
	public final static int ERROR_CONF_DB_NAME_NULL_OR_MISSING = 204;
	public final static int ERROR_CONF_DB_USERNAME_NULL_OR_MISSING = 204;
	public final static int ERROR_CONF_DB_USERPASS_NULL_OR_MISSING = 208;

	public final static int ERROR_CONF_DB_HOST_INCORRECT = 204;
	public final static int ERROR_CONF_DB_PORT_INCORRECT = 205;
	public final static int ERROR_CONF_DB_NAME_INCORRECT = 206;
	public final static int ERROR_CONF_DB_USERNAME_INCORRECT = 207;
	public final static int ERROR_CONF_DB_USERPASS_INCORRECT = 208;
	public final static int ERROR_CONF_DB_PARAM_NOT_FOUND = 209;
	public final static int ERROR_CONF_DB_PARAM_INCORRECT = 210;
	public final static int ERROR_CONF_SYNSTORE_HOST_INCORRECT = 211;
	public final static int ERROR_CONF_SYNSTORE_PORT_INCORRECT = 212;
	public final static int ERROR_CONF_AGENT_SNMP_HOST_INCORRECT = 213;
	public final static int ERROR_CONF_AGENT_SNMP_PORT_INCORRECT = 214;
	public final static int ERROR_CONF_AGENT_LISTENER_HOST_INCORRECT = 215;
	public final static int ERROR_CONF_AGENT_LISTENER_PORT_INCORRECT = 216;
	public final static int ERROR_CONF_MANAGER_ID_NOT_DEFINED = 217;
	public final static int ERROR_CONF_MANAGER_HOST_INCORRECT = 218;
	public final static int ERROR_CONF_MANAGER_PORT_INCORRECT = 219;
	public final static int ERROR_CONF_SYNSOFT_FILE_INCORRECT = 220;
	public final static int ERROR_CONF_SYNSOFT_PROTOCOL_INCORRECT = 221;
	public final static int ERROR_CONF_SYNSOFT_URL_INCORRECT = 222;
	public final static int ERROR_CONF_SYNSOFT_HOST_INCORRECT = 223;
	public final static int ERROR_CONF_SYNSOFT_PORT_INCORRECT = 224;
	public final static int ERROR_CONF_SYNSOFT_PING_INTERVAL_INCORRECT = 225;
	public final static int ERROR_CONF_SYNSOFT_NOT_FOUND = 226;
	public final static int ERROR_CONF_DB_PING_INTERVAL_INCORRECT = 227;
	public final static int ERROR_CONF_SYNSTORE_PING_INTERVAL_INCORRECT = 228;

	public final static int ERROR_CONF_SYSTEM_ID_DUPLICATED = 229;
	public final static int ERROR_CONF_DB_HOST_NOT_FOUND = 230;
	public final static int ERROR_CONF_DB_PORT_NOT_FOUND = 231;
	public final static int ERROR_CONF_DB_NAME_NOT_FOUND = 232;
	public final static int ERROR_CONF_DB_USERNAME_NOT_FOUND = 233;
	public final static int ERROR_CONF_DB_USERPASS_NOT_FOUND = 234;
	public final static int ERROR_CONF_SYNSTORE_HOST_NOT_FOUND = 235;
	public final static int ERROR_CONF_SYNSTORE_PORT_NOT_FOUND = 236;
	public final static int ERROR_CONF_SYNSOFT_HOST_NOT_FOUND = 237;
	public final static int ERROR_CONF_SYNSOFT_PORT_NOT_FOUND = 238;
	public final static int ERROR_CONF_SYNSOFT_PROTOCOL_NOT_FOUND = 239;
	public final static int ERROR_CONF_SYNSOFT_FILE_NOT_FOUND = 240;
	public final static int ERROR_CONF_AGENT_SNMP_HOST_NOT_FOUND = 241;
	public final static int ERROR_CONF_AGENT_SNMP_PORT_NOT_FOUND = 242;
	public final static int ERROR_CONF_AGENT_LISTENER_HOST_NOT_FOUND = 243;
	public final static int ERROR_CONF_AGENT_LISTENER_PORT_NOT_FOUND = 244;
	public final static int ERROR_CONF_AGENT_COMMUNITY_PUBLIC_NOT_FOUND = 245;
	public final static int ERROR_CONF_AGENT_COMMUNITY_PRIVATE_NOT_FOUND = 246;
	public final static int ERROR_CONF_AGENT_MIBXML_FILE_NOT_FOUND = 247;
	public final static int ERROR_CONF_AGENT_DB_DRIVER_NOT_FOUND = 248;
	public final static int ERROR_CONF_AGENT_DB_PREFIX_NOT_FOUND = 249;
	public final static int ERROR_CONF_MANAGER_HOST_NOT_FOUND = 250;
	public final static int ERROR_CONF_MANAGER_PORT_NOT_FOUND = 251;

	/* Other exceptions */

	public final static int WARNING_SET_IP_HOST_INCORRECT = 301;
	public final static int WARNING_SET_IP_PORT_INCORRECT = 302;
	public final static int WARNING_SET_DB_NAME_INCORRECT = 303;
	public final static int WARNING_SET_DB_USERNAME_INCORRECT = 304;
	public final static int WARNING_SET_DB_USERPASS_INCORRECT = 305;
	public final static int WARNING_SET_DB_PARAM_INCORRECT = 306;
	public final static int WARNING_SET_DB_SQL_DRIVER_INCORRECT = 307;
	public final static int WARNING_SET_DB_JDBC_PREFIX_INCORRECT = 308;

	public final static int WARNING_XML_CANNOT_LOAD_XML_FILE = 401;
	public final static int WARNING_XML_CANNOT_CREATE_PARSER = 402;
	public final static int WARNING_XML_CANNOT_PARSE_XML = 403;
	public final static int WARNING_XML_CANNOT_FIND_ATTR_IN_NODE = 404;
	public final static int WARNING_XML_CANNOT_FIND_NODE_IN_DOC_BY_OID = 405;
	public final static int WARNING_XML_CANNOT_FIND_NODE_IN_DOC_BY_NAME = 406;
	public final static int WARNING_XML_CANNOT_LOAD_XML_STREAM = 407;
	public final static int WARNING_CONF_PARSING_WRONG_VALUE = 408;

	public final static int WARNING_MIB_CANNOT_SET_COLUMN_TYPE = 501;
	public final static int WARNING_MIB_COLUMN_LIST_IS_EMPTY = 502;
	public final static int WARNING_MIB_UPDATE_PERIOD_NOT_FOUND = 503;
	public final static int WARNING_MIB_CANNOT_CONSTRUCT_SQL_QUERY = 504;
	public final static int WARNING_MIB_NOTIFY_EVENT_NOT_CORRECT = 505;
	public final static int WARNING_MIB_WRONG_TABLE_OID_OR_SQL_QUERY = 506;
	public final static int WARNING_MIB_CANNOT_FIND_STORED_SQL_QUERY = 507;

	public final static int WARNING_COMMON_INT_TO_STR_ERR = 601;

	//
	/**
	 * Converts error to string
	 * 
	 * @return Exception text as string
	 */
	public String getExceptionText() {
		return "Code " + this.errorCode + ", " + this.errorText;
	}
}
