package com.synapsense.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.naming.InitialContext;
import javax.security.auth.login.LoginContext;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.synapsense.service.AlertService;
import com.synapsense.service.DRuleTypeService;
import com.synapsense.service.Environment;
import com.synapsense.service.RuleTypeService;
import com.synapsense.service.UserManagementService;
import com.synapsense.service.impl.audit.AuditorSvc;
import com.synapsense.service.nsvc.NotificationService;
import com.synapsense.snmpagent.engine.SynapDSStorage;
import com.synapsense.transport.Proxy;
import com.synapsense.transport.TransportManager;
import com.synapsense.transport.rmi.Constants;

/**
 * Singleton class which realizes access to Environment API.
 * 
 * @author Dmitry Malchikov
 */
public class SynapEnv implements Proxy.LifecycleListener {

	private static SynapEnv synapEnv = null;

	private Environment env = null;
	private AlertService alse = null;
	@SuppressWarnings("rawtypes")
	private DRuleTypeService drtodao = null;
	@SuppressWarnings("rawtypes")
	private RuleTypeService rtodao = null;
	private UserManagementService ums = null;
	private AuditorSvc asvc = null;
	private NotificationService nsvc = null;
	private Proxy<NotificationService> nsProxy = null;
	private static final String SynapEnvConfFile = "SynapEnv.properties";

	private static final Logger logger = LogManager.getLogger(SynapEnv.class);
	protected static LoginContext lc;
	private static Properties environment = new Properties();

	private SynapEnv() {
		try {
            InputStream synapEnvIs = SynapEnv.class.getClassLoader().getResourceAsStream(SynapEnvConfFile);
            InputStream jbossEjbIs = SynapEnv.class.getClassLoader().getResourceAsStream("jboss-ejb-client.properties");
			environment.load(synapEnvIs);
            environment.load(jbossEjbIs);
            synapEnvIs.close();
            jbossEjbIs.close();
		} catch (IOException e1) {
			logger.error(e1.getLocalizedMessage(), e1);
		}

		nsProxy = TransportManager.getProxy(environment, NotificationService.class);
		nsvc = nsProxy.getService();
		nsProxy.addListener(this);
	}

	private void init() {
		try {
			InitialContext ctx = new InitialContext(environment);
			logger.info("Looking for Environment");

			asvc = ServerLoginModule.getService(ctx, "AuditorSvcImpl", AuditorSvc.class);
			env = ServerLoginModule.getService(ctx, "Environment", Environment.class);

			alse = ServerLoginModule.getService(ctx, "AlertService", AlertService.class);
			drtodao = ServerLoginModule.getService(ctx, "es-dal", "DRuleTypeObjectDAO", DRuleTypeService.class);
			rtodao = ServerLoginModule.getService(ctx, "es-dal", "RuleTypeObjectDAO", RuleTypeService.class);
			ums = ServerLoginModule.getService(ctx, "UserManagementService", UserManagementService.class);
			nsvc = nsProxy.getService();

		} catch (Throwable t) {
			// catch and log everything so the calling thread won't be
			// blocked
			if (logger.isDebugEnabled()) {
				logger.error("Cannot connect to ES", t);
			} else {
				logger.error("Cannot connect to ES: " + t.getLocalizedMessage());
			}
		}
	}

	@Override
	public void connected(Proxy<?> proxy) {
		logger.info("Connection to the ES is [restored], reinitializing tables...");
		init();
		SynapDSStorage.getInstance().reregisterDataSources();
	}

	@Override
	public void disconnected(Proxy<?> proxy) {
		try {
			logger.warn("Connection to the ES is [lost], trying to restore...");
			proxy.connect();
		} catch (Exception e) {
			// do nothing or log it. if reconnection fails
			// this listener will be invoked again when
			// connection check period elapses.
		}

	}

	/**
	 * Return Environment instance
	 * 
	 * @return Environment instance
	 */
	public static synchronized Environment getEnv() {
		if (synapEnv == null) {
			synapEnv = new SynapEnv();
		}
		return synapEnv.env;
	}

	public static synchronized AlertService getAS() {
		if (synapEnv == null) {
			synapEnv = new SynapEnv();
		}
		return synapEnv.alse;
	}

	public static synchronized DRuleTypeService<?> getDRuleTypeDAO() {
		if (synapEnv == null) {
			synapEnv = new SynapEnv();
		}
		return synapEnv.drtodao;
	}

	public static synchronized RuleTypeService<?> getRuleTypeDAO() {
		if (synapEnv == null) {
			synapEnv = new SynapEnv();
		}
		return synapEnv.rtodao;
	}

	public static synchronized UserManagementService getUMS() {
		if (synapEnv == null) {
			synapEnv = new SynapEnv();
		}
		return synapEnv.ums;
	}

	public static synchronized AuditorSvc getAuditorSvc() {
		if (synapEnv == null) {
			synapEnv = new SynapEnv();
		}
		return synapEnv.asvc;
	}

	public static synchronized NotificationService getNsvc() {
		if (synapEnv == null) {
			synapEnv = new SynapEnv();
		}
		return synapEnv.nsvc;
	}

	public static Proxy<NotificationService> getNsProxy() {
		if (synapEnv == null) {
			synapEnv = new SynapEnv();
		}
		return synapEnv.nsProxy;
	}

	public static SynapEnv getInstance() {
		return synapEnv;
	}
}