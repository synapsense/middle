package com.synapsense.util;

import org.apache.log4j.Logger;

/**
 * The <code>SynapExceptionCreator</code> is interface for all classes which
 * produce any logs throw Log4J library
 * 
 * @author Maxim Bogatkin
 * @version 2.0
 */
public interface SynapExceptionCreator {

	/**
	 * Returns class logger
	 */
	public Logger getLogger();

}
