package com.synapsense.util;

/**
 * The <code>SynapExceptionHandler</code> is a class which handle all SynapSense
 * agent exceptions
 * 
 * @author Maxim Bogatkin
 * @version 2.0
 */
public class SynapExceptionHandler {

	public final static String AGENT_EVENT = "snmpagentEvent";

	/**
	 * Handles an exception and make the following actions related on exception
	 * severity <code>SNMP_SVR_CRITICAL</code> - exit
	 * <code>SNMP_SVR_ERROR</code> - send notification to NMSes
	 * <code>SNMP_SVR_WARNING</code> - nothing
	 * 
	 * All exception are stored in log
	 * 
	 * @param e
	 *            Exception
	 * @param creator
	 *            Exception initiator
	 */
	public void handleException(SynapAgentException e, SynapExceptionCreator creator) {
		switch (e.getSeverity()) {
		case SynapAgentException.SNMP_SVR_CRITICAL:
			if ((creator != null) && (creator.getLogger() != null)) {
				creator.getLogger().fatal(e.getErrorText(), e);
			}
			System.exit(e.getErrorCode());
			break;
		case SynapAgentException.SNMP_SVR_ERROR:
			if ((creator != null) && (creator.getLogger() != null)) {
				creator.getLogger().error(e.getErrorText(), e);
			}
			break;
		case SynapAgentException.SNMP_SVR_WARNING:
			if ((creator != null) && (creator.getLogger() != null)) {
				creator.getLogger().error(e.getErrorText(), e);
			}
			break;
		}

	}
}
