package com.panduit.sz.api.ss.assets;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.panduit.sz.api.shared.AbstractNamedEntity;
import com.panduit.sz.api.util.SmartZoneValueStyle;
import java.util.List;
import org.immutables.value.Value;

/**
 * SmartZone location, represented as a tree. Each location contains a list of children locations.
 *
 * <p>NOTE: It's the responsibility of the developer to ensure that the data structure contains no cycles.</p>
 */
@Value.Immutable
@SmartZoneValueStyle
@JsonSerialize
public abstract class AbstractLocation extends AbstractNamedEntity {

    /** The level in the tree */
    public abstract LocationLevel getLocationLevel();

    /** The children locations */
    public abstract List<Location> getChildren();

    /**
     * Flag whether the floorplan has already been associated with a MapSense project. MapSense would refuse to create a project based on a
     * floorplan that is already instrumented. The default value is {@code false}.
     */
    @Value.Default
    public boolean isWsnInstrumented() {
        return false;
    }

    /** Returns true if this floorplan corresponds to {@code LocationLevel.FLOOR} */
    @JsonIgnore
    public boolean isFloorplan() {
        return getLocationLevel() == LocationLevel.FLOOR;
    }

    /** Creates a non-floorplan location, an internal node in the location tree. */
    public static Location of(Integer id, String name, LocationLevel locationLevel, Location... children) {
        return Location.builder()
                .id(id)
                .name(name)
                .locationLevel(locationLevel)
                .addChildren(children)
                .build();
    }

    /** Creates a floorplan location, usually a leaf in the location tree. */
    public static Location of(Integer id, String name, boolean isWsnInstrumented) {
        return Location.builder()
                .id(id)
                .name(name)
                .locationLevel(LocationLevel.FLOOR)
                .wsnInstrumented(isWsnInstrumented)
                .build();
    }
}
