package com.panduit.sz.api.ss.assets;

import com.google.common.collect.Maps;
import com.panduit.sz.shared.SmartZoneNonTransientException;
import java.util.EnumSet;
import java.util.Map;
import java.util.Optional;

/**
 * Enumeration for location levels, with support for mapping to/from database IDs.
 *
 * <p>NOTE: The {@code id()} and {@code valueOf(Integer)} methods match the existing enum convention for {@code name()} and
 * {@code valueOf(String)}.</p>
 */
@SuppressWarnings("unused")
public enum LocationLevel {

    COUNTRY(1), STATE(2), CITY(3), COMPANY(4), STREET(5), BUILDING(6), FLOOR(7), ROOM(8), TECHNICIAN(99), GROUP(100), WORLD(1000), OTHER(-1);

    private final Integer id;

    private static final Map<Integer, LocationLevel> valuesById = Maps.uniqueIndex(EnumSet.allOf(LocationLevel.class), LocationLevel::id);

    LocationLevel(Integer id) {
        this.id = id;
    }

    public Integer id() {
        return id;
    }

    public static LocationLevel valueOf(Integer id) {
        return Optional.ofNullable(valuesById.get(id)).orElse(LocationLevel.OTHER);
    }
}
