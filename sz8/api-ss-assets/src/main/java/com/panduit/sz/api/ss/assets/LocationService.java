package com.panduit.sz.api.ss.assets;

import java.util.List;

/** Service for exposing the SmartZone location tree resource to MapSense */
public interface LocationService {

    /**
     * Returns the SmartZone location tree as a list of top-level locations. If there are no locations, an empty list is returned. Children
     * of a node are sorted alphabetically by name.</p>
     */
    List<Location> getLocationTree();
}
