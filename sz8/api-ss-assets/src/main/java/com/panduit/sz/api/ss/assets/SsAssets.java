package com.panduit.sz.api.ss.assets;

import com.google.common.base.Verify;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Constants for the REST URIs, to be shared by client and server.
 *
 * <p>Created by SDUH on 11/23/2015.</p>
 */
public class SsAssets {

    public static final String LOCATIONS = "/api/ss/locations";
    public static final String LOCATIONS_PATH = LOCATIONS;

    public static String locationsPath() {
        return LOCATIONS_PATH;
    }

    public static final String FLOORPLANS = "/api/ss/floorplans";
    public static final String FLOORPLAN_ID = "floorplanId";
    public static final String FLOORPLAN = "{" + FLOORPLAN_ID + "}";
    public static final String FLOORPLAN_PATH = FLOORPLANS + "/" + FLOORPLAN;

    public static String floorplanPath(int floorplanId) {
        return expand(FLOORPLAN_PATH, floorplanId);
    }

    public static final String INSTRUMENTATION = FLOORPLAN + "/instrumentation";
    public static final String INSTRUMENTATION_PATH = FLOORPLANS + "/" + INSTRUMENTATION;

    public static String instrumentationPath(int floorplanId) {
        return expand(INSTRUMENTATION_PATH, floorplanId);
    }

    private static final Pattern PATH_VARIABLE_PATTERN = Pattern.compile("\\{\\w+\\}");

    /**
     * Expands a template like {@code /api/ss/floorplans/{floorplanId}} using the given variables. The variables must match exactly the
     * placeholders in the template.
     *
     * <p>TODO Move to a utility class once we have multiple URI classes.</p>
     */
    static String expand(String template, Object... vars) {
        Matcher matcher = PATH_VARIABLE_PATTERN.matcher(template);
        StringBuffer buffer = new StringBuffer();
        int i = 0;
        while (matcher.find()) {
            Verify.verify(i < vars.length, "Variables %s are too few for template %s.", Arrays.toString(vars), template);
            matcher.appendReplacement(buffer, String.valueOf(vars[i++]));
        }
        Verify.verify(i == vars.length, "Variables %s are too many for template %s.", Arrays.toString(vars), template);
        matcher.appendTail(buffer);
        return buffer.toString();
    }
}
