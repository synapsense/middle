package com.panduit.sz.api.ss.assets;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.panduit.sz.api.util.SmartZoneValueStyle;
import org.immutables.value.Value;

/**
 * SmartZone container corresponding to a SS rack
 *
 * <p>Impl Note: Take into account the customized {@code equals()} when adding/removing properties.</p>
 */
@Value.Immutable
@SmartZoneValueStyle
@JsonSerialize
public abstract class AbstractRack extends AbstractContainer {

    /**
     * Customized {@code equals()}. We want to compare floating-point properties with an {@code epsilon}.
     */
    @Override
    public boolean equals(Object another) {
        return super.equals(another) && another instanceof AbstractRack;
    }
}
