package com.panduit.sz.api.ss.assets;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.panduit.sz.api.shared.AbstractNamedEntity;
import com.panduit.sz.api.shared.ByteArray;
import com.panduit.sz.api.util.SmartZoneValueStyle;
import com.panduit.sz.util.math.PNumbers;
import java.util.List;
import java.util.Optional;
import org.immutables.value.Value;

/**
 * A floorplan with a background image and containers such as racks and crahs
 *
 * <p>Impl Note: Take into account the customized {@code equals()} when adding/removing properties.</p>
 */
@Value.Immutable
@SmartZoneValueStyle
@JsonSerialize
public abstract class AbstractFloorplan extends AbstractNamedEntity {

    public static final double SIZE_EPSILON = 0.01; // 1/100th of an inch

    /**
     * The background image for this floorplan.
     *
     * <p>NOTE: The pixel dimensions of the image combined with the {@code width} and {@code height} properties give the scale of the
     * background in terms of pixels/inch. Floorplans coming from SmartZone may have different scale for width and height, but MapSense will
     * adjust the aspect ratio of the image and the coordinates of all containers so that the scale is the same for both dimensions and the
     * containers appear to be in the same place.</p>
     *
     * <p>NOTE: We use our own {@code ByteArray} because Immutables does not properly support optional arrays, see <a
     * href="https://github.com/immutables/immutables/issues/193">https://github.com/immutables/immutables/issues/193</a>.</p>
     */
    public abstract Optional<ByteArray> getBackground();

    /** The width of floorplan, in inches. It corresponds to the x axis. */
    public abstract double getWidth();

    /** The height of the floorplan, in inches. It corresponds to the y axis. */
    public abstract double getHeight();

    /** The racks owned by this floorplan */
    public abstract List<Rack> getRacks();

    /** The crahs owned by this floorplan */
    public abstract List<Crah> getCrahs();

    /** The vertical temps owned by this floorplan */
    public abstract List<VerticalTemp> getVerticalTemps();

    /** The generic temps owned by this floorplan */
    public abstract List<GenericTemp> getGenericTemps();

    /** The pressures owned by this floorplan */
    public abstract List<Pressure> getPressures();

    /** The file path of the configuration file when this floorplan was last synced / linked */
    public abstract Optional<String> getLastSyncFilePath();

    /**
     * Disallow {@code hashCode()}. We want to compare floating-point properties with an {@code epsilon} and this makes it impossible to
     * define a consistent {@code hashCode()}.
     */
    @Override
    public int hashCode() {
        throw new UnsupportedOperationException("Objects with arbitrary precision fields cannot have a meaningful hashCode() without rounding.");
    }

    /**
     * Customized {@code equals()}. We want to compare floating-point properties with an {@code epsilon}.
     */
    @Override
    public boolean equals(Object another) {
        return this == another || another instanceof AbstractFloorplan && equalsFloorplan((AbstractFloorplan) another);
    }

    private boolean equalsFloorplan(AbstractFloorplan another) {
        return getBackground().equals(another.getBackground())
                && PNumbers.almostEqual(getWidth(), another.getWidth(), SIZE_EPSILON)
                && PNumbers.almostEqual(getHeight(), another.getHeight(), SIZE_EPSILON)
                && getRacks().equals(another.getRacks())
                && getCrahs().equals(another.getCrahs())
                && getVerticalTemps().equals(another.getVerticalTemps())
                && getGenericTemps().equals(another.getGenericTemps())
                && getPressures().equals(another.getPressures())
                && getName().equals(another.getName())
                && getId().equals(another.getId())
                && getLastSyncFilePath().equals(another.getLastSyncFilePath());
    }

    /** Compares the {@code (width, height)} of this object to other values using an {@code epsilon} */
    public boolean sizeEquals(double anotherWidth, double anotherHeight) {
        return PNumbers.almostEqual(getWidth(), anotherWidth, SIZE_EPSILON)
                && PNumbers.almostEqual(getHeight(), anotherHeight, SIZE_EPSILON);
    }
}
