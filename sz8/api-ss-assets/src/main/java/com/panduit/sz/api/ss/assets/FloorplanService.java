package com.panduit.sz.api.ss.assets;

import com.panduit.sz.api.shared.IdNameTuple;
import com.panduit.sz.shared.SmartZoneNonTransientException;
import java.util.List;

/** Service for exposing the SmartZone floorplan resource to MapSense */
public interface FloorplanService {

    /**
     * Returns the floorplan with the given ID
     *
     * @throws SmartZoneNonTransientException if the floorplan does not exist
     */
    Floorplan getFloorplan(Integer floorplanId);

    /**
     * Creates a floorplan from an offline MS project under an existing empty location. IDs are discarded, SZ generates new ones.
     *
     * @throws SmartZoneNonTransientException if the parent location does not exist or any of the entities are not new (persisted to the
     *                                        DB)
     */
    Integer createFloorplan(Integer parentLocationId, Floorplan floorplan);

    /**
     * Updates an existing floorplan (MS as advanced SZ editor)
     *
     * <p>It returns (id, name) tuples for the new containers that were assigned a SZ ID. It's up to MS to ensure that the names are
     * unique.</p>
     *
     * @throws SmartZoneNonTransientException if the floorplan does not exist
     */
    List<IdNameTuple> updateFloorplan(Floorplan floorplan);

    /**
     * Marks this floorplan as instrumented by MS. This is used as a synchronization mechanism to ensure that only one MS project is
     * associated to a SZ location.
     *
     * @return true if the floorplan was not previously instrumented.
     * @throws SmartZoneNonTransientException if the floorplan does not exist
     */
    boolean instrumentFloorplan(Integer floorplanId, String filePath);

    /**
     * Marks this floorplan as not instrumented by MS.
     *
     * @return true if the floorplan was previously instrumented.
     * @throws SmartZoneNonTransientException if the floorplan does not exist
     */
    boolean uninstrumentFloorplan(Integer floorplanId);
}
