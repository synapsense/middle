package com.panduit.sz.api.ss.assets;

import com.panduit.sz.api.shared.AbstractNamedEntity;
import com.panduit.sz.api.shared.Point2D;
import com.panduit.sz.util.math.PNumbers;

/**
 * A container on a floorplan, like rack, crah, a.s.o.
 *
 * <p>Impl Note: Take into account the customized {@code equals()} when adding/removing properties.</p>
 */
public abstract class AbstractContainer extends AbstractNamedEntity {

    public static final double SIZE_EPSILON = 0.01; // 1/100th of an inch
    public static final double ROTATION_EPSILON = 0.001; // 1/1000th of a degree

    /** The (x,y) coordinates of the middle of the container, in inches. It's relative to the top-left corner of the floorplan. */
    public abstract Point2D getLocation();

    /** The width of the container, in inches. It corresponds to the y axis. */
    public abstract double getWidth();

    /** The depth of the container, in inches. It corresponds to the x axis. */
    public abstract double getDepth();

    /** The geometric (counter-clockwise) rotation of the container, in degrees. */
    public abstract double getRotation();

    /**
     * Disallow {@code hashCode()}. We want to compare floating-point properties with an {@code epsilon} and this makes it impossible
     * to define a consistent {@code hashCode()}.
     */
    @Override
    public int hashCode() {
        throw new UnsupportedOperationException("Objects with arbitrary precision fields cannot have a meaningful hashCode() without rounding.");
    }

    /**
     * Customized {@code equals()}. We want to compare floating-point properties with an {@code epsilon}.
     */
    @Override
    public boolean equals(Object another) {
        return this == another || another instanceof AbstractContainer && equalsContainer((AbstractContainer) another);
    }

    private boolean equalsContainer(AbstractContainer another) {
        return getLocation().equals(another.getLocation())
                && PNumbers.almostEqual(getWidth(), another.getWidth(), SIZE_EPSILON)
                && PNumbers.almostEqual(getDepth(), another.getDepth(), SIZE_EPSILON)
                && PNumbers.almostEqual(getRotation(), another.getRotation(), ROTATION_EPSILON)
                && getName().equals(another.getName())
                && getId().equals(another.getId());
    }

    /** Compares the {@code (width, depth)} of this object to other values using an {@code epsilon} */
    public boolean sizeEquals(double anotherWidth, double anotherDepth) {
        return PNumbers.almostEqual(getWidth(), anotherWidth, SIZE_EPSILON)
                && PNumbers.almostEqual(getDepth(), anotherDepth, SIZE_EPSILON);
    }

    /** Compares the {@code rotation} of this object to another value using an {@code epsilon} */
    public boolean rotationEquals(double anotherRotation) {
        return PNumbers.almostEqual(getRotation(), anotherRotation, ROTATION_EPSILON);
    }
}
