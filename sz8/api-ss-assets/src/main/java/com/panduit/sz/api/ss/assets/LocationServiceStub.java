package com.panduit.sz.api.ss.assets;

import java.util.List;

/** LocationService implementation that returns a predefined tree */
public class LocationServiceStub implements LocationService {
    private final List<Location> locations;

    /**
     * Creates a new instance of LocationServiceStub
     *
     * @param locationTree the predefined list of locations to be returned by getLocationTree
     */
    public LocationServiceStub(List<Location> locationTree) {
        this.locations = locationTree;
    }

    @Override
    public List<Location> getLocationTree() {
        return locations;
    }
}
