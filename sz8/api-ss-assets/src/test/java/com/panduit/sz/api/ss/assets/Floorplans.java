package com.panduit.sz.api.ss.assets;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import java.util.List;
import java.util.Set;

/** static utility methods for working with {@code Floorplan}. It's used just by unit tests for now. */
public class Floorplans {

    public static List<AbstractContainer> containers(Floorplan floorplan) {
        return ImmutableList.<AbstractContainer>builder()
                .addAll(floorplan.getRacks())
                .addAll(floorplan.getCrahs())
                .addAll(floorplan.getVerticalTemps())
                .addAll(floorplan.getGenericTemps())
                .addAll(floorplan.getPressures())
                .build();
    }

    public static Set<String> containerNames(Floorplan floorplan) {
        return ImmutableSet.<String>builder()
                .addAll(containerNames(floorplan.getRacks()))
                .addAll(containerNames(floorplan.getCrahs()))
                .addAll(containerNames(floorplan.getVerticalTemps()))
                .addAll(containerNames(floorplan.getGenericTemps()))
                .addAll(containerNames(floorplan.getPressures()))
                .build();
    }

    private static List<String> containerNames(List<? extends AbstractContainer> containers) {
        return Lists.transform(containers, AbstractContainer::getName);
    }
}
