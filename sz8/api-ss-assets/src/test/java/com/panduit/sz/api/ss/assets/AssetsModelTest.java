package com.panduit.sz.api.ss.assets;

import com.panduit.sz.api.shared.ByteArray;
import com.panduit.sz.api.shared.Point2D;
import com.panduit.sz.api.util.JacksonTestService;
import com.panduit.sz.api.util.SmartZoneApiTest;
import com.panduit.sz.shared.SmartZoneNonTransientException;
import java.io.IOException;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Tests JSON serialization for the assets data model. This should catch accidental changes to the classes that would break the wire
 * format.
 */
public class AssetsModelTest extends SmartZoneApiTest {

    @Autowired
    private JacksonTestService jacksonTestService;

    @Test(expected = IllegalStateException.class)
    public void whenMissingRequiredPropertyThenThrowException() {
        Rack.builder().id(1).build();
    }

    @Test
    public void floorplan() throws IOException {
        jacksonTestService.testToJsonAndBack(
                Floorplan.builder()
                        .id(1)
                        .name("my floorplan")
                        .background(ByteArray.of("my background".getBytes())) // Funny, right?
                        .width(4)
                        .height(5)
                        .lastSyncFilePath("Test.dlz")
                        .addRacks(
                                Rack.builder()
                                        .id(6)
                                        .name("my rack 1")
                                        .location(Point2D.of(7, 8))
                                        .width(9)
                                        .depth(10)
                                        .rotation(11.12)
                                        .build(),
                                Rack.builder()
                                        .id(13)
                                        .name("my rack 2")
                                        .location(Point2D.of(14, 15))
                                        .width(16)
                                        .depth(17)
                                        .rotation(18.19)
                                        .build())
                        .addCrahs(
                                Crah.builder()
                                        .id(20)
                                        .name("my crah 1")
                                        .location(Point2D.of(21, 22))
                                        .width(23)
                                        .depth(24)
                                        .rotation(25.26)
                                        .build())
                        .build(),
                "{\"background\":{\"bytes\":\"bXkgYmFja2dyb3VuZA==\"},\"width\":4.0,\"height\":5.0,\"lastSyncFilePath\":\"Test.dlz\",\"racks\":[{\"location\":{\"x\":7.0,\"y\":8.0},\"width\":9.0,\"depth\":10.0,\"rotation\":11.12,\"name\":\"my rack 1\",\"id\":6},{\"location\":{\"x\":14.0,\"y\":15.0},\"width\":16.0,\"depth\":17.0,\"rotation\":18.19,\"name\":\"my rack 2\",\"id\":13}],\"crahs\":[{\"location\":{\"x\":21.0,\"y\":22.0},\"width\":23.0,\"depth\":24.0,\"rotation\":25.26,\"name\":\"my crah 1\",\"id\":20}],\"verticalTemps\":[],\"genericTemps\":[],\"pressures\":[],\"name\":\"my floorplan\",\"id\":1}");
    }

    @Test
    public void locationTreeWithEmptyRoot() throws IOException {
        jacksonTestService.testToJsonAndBack(
                Location.of(1, "root", LocationLevel.COUNTRY),
                "{\"id\":1,\"name\":\"root\",\"locationLevel\":\"COUNTRY\",\"children\":[],\"wsnInstrumented\":false}"
        );
    }


    @Test
    public void locationTreeWithMixedChildren() throws IOException {
        jacksonTestService.testToJsonAndBack(
                Location.of(1, "root", LocationLevel.COUNTRY,
                        Location.of(2, "location", LocationLevel.BUILDING),
                        Location.of(3, "floorplan", true)),
                "{\"locationLevel\":\"COUNTRY\",\"children\":[{\"locationLevel\":\"BUILDING\",\"children\":[],\"wsnInstrumented\":false,\"name\":\"location\",\"id\":2},{\"locationLevel\":\"FLOOR\",\"children\":[],\"wsnInstrumented\":true,\"name\":\"floorplan\",\"id\":3}],\"wsnInstrumented\":false,\"name\":\"root\",\"id\":1}"
        );
    }

    @Test
    public void realisticLocationTree() throws IOException {
        int id = 1;
        //noinspection UnusedAssignment
        jacksonTestService.testToJsonAndBack(
                Location.of(id++, "world", LocationLevel.COUNTRY,
                        Location.of(id++, "us", LocationLevel.COUNTRY,
                                Location.of(id++, "folsom", LocationLevel.CITY,
                                        Location.of(id++, "folsom floor 1", true),
                                        Location.of(id++, "folsom floor 2", true),
                                        Location.of(id++, "folsom floor 3", false),
                                        Location.of(id++, "folsom floor 4", false)),
                                Location.of(id++, "chicago", LocationLevel.CITY)),
                        Location.of(id++, "mexico", LocationLevel.COUNTRY,
                                Location.of(id++, "cancun", LocationLevel.CITY,
                                        Location.of(id++, "cancun floor 1", true)))),
                "{\"locationLevel\":\"COUNTRY\",\"children\":[{\"locationLevel\":\"COUNTRY\",\"children\":[{\"locationLevel\":\"CITY\",\"children\":[{\"locationLevel\":\"FLOOR\",\"children\":[],\"wsnInstrumented\":true,\"name\":\"folsom floor 1\",\"id\":4},{\"locationLevel\":\"FLOOR\",\"children\":[],\"wsnInstrumented\":true,\"name\":\"folsom floor 2\",\"id\":5},{\"locationLevel\":\"FLOOR\",\"children\":[],\"wsnInstrumented\":false,\"name\":\"folsom floor 3\",\"id\":6},{\"locationLevel\":\"FLOOR\",\"children\":[],\"wsnInstrumented\":false,\"name\":\"folsom floor 4\",\"id\":7}],\"wsnInstrumented\":false,\"name\":\"folsom\",\"id\":3},{\"locationLevel\":\"CITY\",\"children\":[],\"wsnInstrumented\":false,\"name\":\"chicago\",\"id\":8}],\"wsnInstrumented\":false,\"name\":\"us\",\"id\":2},{\"locationLevel\":\"COUNTRY\",\"children\":[{\"locationLevel\":\"CITY\",\"children\":[{\"locationLevel\":\"FLOOR\",\"children\":[],\"wsnInstrumented\":true,\"name\":\"cancun floor 1\",\"id\":11}],\"wsnInstrumented\":false,\"name\":\"cancun\",\"id\":10}],\"wsnInstrumented\":false,\"name\":\"mexico\",\"id\":9}],\"wsnInstrumented\":false,\"name\":\"world\",\"id\":1}"
        );
    }

    @Test
    public void locationLevelFromInteger() {
        assertThat(LocationLevel.valueOf(LocationLevel.BUILDING.id())).isSameAs(LocationLevel.BUILDING);
    }

    @Test
    public void locationLevelFromString() {
        assertThat(LocationLevel.valueOf(LocationLevel.BUILDING.name())).isSameAs(LocationLevel.BUILDING);
    }

    @Test(expected = IllegalArgumentException.class)
    public void whenLocationLevelNameIsInvalidThenExceptionIsThrown() {
        LocationLevel.valueOf("INVALID_LOCATION");
    }

    @Test(expected = IllegalArgumentException.class)
    public void enumValueOfIsCaseSensitive() {
        LocationLevel.valueOf("background");
    }

    @Test
    public void floorplanSizeComparison() {
        double width = 10_000;
        double height = 5_000;
        Floorplan floorplan = Floorplan.builder()
                .id(1)
                .name("test floorplan")
                .width(width)
                .height(height)
                .build();

        double e = Floorplan.SIZE_EPSILON;
        assertThat(floorplan.sizeEquals(width + .5 * e, height - .5 * e)).isTrue();
        assertThat(floorplan.sizeEquals(width + 1.5 * e, height)).isFalse();
        assertThat(floorplan.sizeEquals(width, height - 1.5 * e)).isFalse();
    }

    @Test
    public void containerSizeAndRotationComparison() {
        double width = 29.53;
        double depth = 47.24;
        double rotation = 0;
        Rack rack = Rack.builder()
                .id(1)
                .name("test rack")
                .width(width)
                .depth(depth)
                .rotation(rotation)
                .location(Point2D.of(0, 0))
                .build();

        double se = Rack.SIZE_EPSILON;
        assertThat(rack.sizeEquals(width + .5 * se, depth - .5 * se)).isTrue();
        assertThat(rack.sizeEquals(width + 1.5 * se, depth)).isFalse();
        assertThat(rack.sizeEquals(width, depth - 1.5 * se)).isFalse();

        double re = Rack.ROTATION_EPSILON;
        assertThat(rack.rotationEquals(rotation + .5 * re)).isTrue();
        assertThat(rack.rotationEquals(rotation + 1.5 * re)).isFalse();
    }

}
