package com.panduit.sz.api.ss.assets;

import com.google.common.collect.ImmutableList;
import com.panduit.sz.api.shared.ByteArray;
import com.panduit.sz.api.shared.IdNameTuple;
import com.panduit.sz.api.shared.Point2D;
import com.panduit.sz.client.shared.SmartZoneRestClient;
import com.panduit.sz.client.shared.SslHelper;
import com.panduit.sz.shared.SmartZoneNonTransientException;
import com.panduit.sz.util.io.PResources;
import java.io.IOException;
import java.util.List;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.core.ParameterizedTypeReference;

import static java.util.stream.Collectors.toList;
import static org.junit.Assert.assertTrue;

/**
 * Exercises the {@code ss/floorplans} endpoint against SZ running on localhost.
 *
 * <p>It uses a top-level {@code FLOOR} location called {@code FloorplansExampleClient} that must be manually created in SZ.</p>
 */
public class FloorplansExampleClient {

    private SmartZoneRestClient szClient;

    private static final String HOST = "localhost";
    private static final int PORT = 8443;
    private static final String USER = "admin";
    private static final String PASSWORD = "Panduit12";

    private static final String LOCATION_NAME = FloorplansExampleClient.class.getSimpleName();
    private static final String BACKGROUND_RESOURCE = "gray-background.png";

    private static final ParameterizedTypeReference<ImmutableList<Location>> LOCATIONS_TYPE = new ParameterizedTypeReference<ImmutableList<Location>>() {};
    private static final ParameterizedTypeReference<ImmutableList<IdNameTuple>> ID_NAME_TUPLES_TYPE = new ParameterizedTypeReference<ImmutableList<IdNameTuple>>() {};

    @BeforeClass
    public static void classSetUp() throws IOException {
        SslHelper.configure();
//        System.setProperty("sz.client.httpProxy", "localhost:8888"); // Temporarily enable proxy for development
    }

    @Before
    public void setUp() {
        szClient = new SmartZoneRestClient(HOST, PORT, USER, PASSWORD);
    }

    @Test
    public void exerciseUpdateFloorplan() {
        // Lookup the ID of the test location
        Integer floorplanId = getTestLocationId();

        // Initialize the floorplan
        updateFloorplan(initialFloorplan(floorplanId));

        // Update the floorplan, covering insert/update/delete
        updateFloorplan(modifiedFloorplan(getFloorplan(floorplanId)));
    }

    private Integer getTestLocationId() {
        return getLocationTree().stream()
                .filter(x -> x.getName().equals(LOCATION_NAME))
                .filter(x -> x.getLocationLevel() == LocationLevel.FLOOR)
                .findFirst()
                .orElseThrow(() -> new SmartZoneNonTransientException(
                        "Top-level location " + LOCATION_NAME + " was not found in SZ. Please create it manually and make sure the level is FLOOR."))
                .getId();
    }

    private List<Location> getLocationTree() {
        return szClient.getForObject(SsAssets.locationsPath(), LOCATIONS_TYPE);
    }

    private Floorplan getFloorplan(Integer floorplanId) {
        return szClient.getForObject(SsAssets.floorplanPath(floorplanId), Floorplan.class);
    }

    private List<IdNameTuple> updateFloorplan(Floorplan floorplan) {
        return szClient.putForObject(SsAssets.floorplanPath(floorplan.getId()), floorplan, ID_NAME_TUPLES_TYPE);
    }

    private Floorplan initialFloorplan(Integer floorplanId) {
        return Floorplan.builder()
                .id(floorplanId)
                .name(LOCATION_NAME)
                .background(ByteArray.of(PResources.readBytes(BACKGROUND_RESOURCE)))
                .width(400)
                .height(300)
                .addRacks(
                        Rack.builder()
                                .name("rack 1")
                                .location(Point2D.of(100, 100))
                                .width(0)
                                .depth(0)
                                .rotation(0)
                                .build(),
                        Rack.builder()
                                .name("rack 2")
                                .location(Point2D.of(200, 200))
                                .width(0)
                                .depth(0)
                                .rotation(90)
                                .build())
                .build();
    }

    private Floorplan modifiedFloorplan(Floorplan initialFloorplan) {
        return Floorplan.builder().from(initialFloorplan)
                .racks(initialFloorplan.getRacks().stream()
                        .map(this::modifyRack)
                        .collect(toList()))
                .build();
    }

    private Rack modifyRack(Rack rack) {
        switch (rack.getName()) {
            case "rack 1": // update name and position
                return Rack.builder().from(rack)
                        .name(rack.getName() + " modified")
                        .location(Point2D.of(200, 100))
                        .build();
            case "rack 2": // replace with new rack
                return Rack.builder()
                        .name("rack 3")
                        .location(Point2D.of(100, 200))
                        .width(0)
                        .depth(0)
                        .rotation(180)
                        .build();
            default:
                throw new IllegalArgumentException("Unexpected rack: " + rack);
        }
    }

    @Test
    public void exerciseInstrumentFloorplan() {
        // Lookup the ID of the test location
        Integer floorplanId = getTestLocationId();

        assertTrue(instrumentFloorplan(floorplanId));
        assertTrue(uninstrumentFloorplan(floorplanId));
    }

    private boolean instrumentFloorplan(Integer floorplanId) {
        return szClient.putForObject(SsAssets.instrumentationPath(floorplanId) , Boolean.class);
    }

    private boolean uninstrumentFloorplan(Integer floorplanId) {
        return szClient.deleteForObject(SsAssets.instrumentationPath(floorplanId) , Boolean.class);
    }
}
