package com.panduit.sz.api.ss.assets;

import com.google.common.base.VerifyException;
import org.junit.Test;

import static com.panduit.sz.api.ss.assets.SsAssets.expand;
import static org.assertj.core.api.Assertions.assertThat;

public class SsAssetsTest {

    @Test
    public void expandNone() {
        assertThat(expand("foo")).isEqualTo("foo");
    }

    @Test
    public void expandOne() {
        assertThat(expand("foo/{id}", 1)).isEqualTo("foo/1");
        assertThat(expand("{id}/foo", 1)).isEqualTo("1/foo");
        assertThat(expand("foo/{id}/bar", 1)).isEqualTo("foo/1/bar");
    }

    @Test
    public void expandTwo() {
        assertThat(expand("foo/{id}/bar/{id2}", 1, 2)).isEqualTo("foo/1/bar/2");
        assertThat(expand("{id}/foo/{id2}/bar", 1, 2)).isEqualTo("1/foo/2/bar");
        assertThat(expand("foo/{id}/bar/{id2}/foobar", 1, 2)).isEqualTo("foo/1/bar/2/foobar");
    }

    @Test(expected = VerifyException.class)
    public void expandTooManyArgs() {
        expand("foo/{id}/bar", 1, 2);
    }

    @Test(expected = VerifyException.class)
    public void expandTooFewArgs() {
        expand("foo/{id}/bar");
    }
}