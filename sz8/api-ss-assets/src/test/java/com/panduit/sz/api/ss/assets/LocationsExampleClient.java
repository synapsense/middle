package com.panduit.sz.api.ss.assets;

import com.panduit.sz.api.shared.ByteArray;
import com.panduit.sz.api.shared.Point2D;
import com.panduit.sz.client.shared.SmartZoneRestClient;
import com.panduit.sz.client.shared.SslHelper;
import com.panduit.sz.shared.AuthenticationException;
import com.panduit.sz.shared.NotFoundException;
import com.panduit.sz.shared.SmartZoneTransientException;
import java.io.IOException;
import java.util.List;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.core.ParameterizedTypeReference;

/** Exercises the {@code ss/locations} endpoint against SZ running on localhost */
public class LocationsExampleClient {

    private static final String HOST = "localhost";
    private static final int PORT = 8443;
    private static final String USER = "admin";
    private static final String PASSWORD = "Panduit12";

    private static final String INVALID_USER = "invalid-user";
    private static final String INVALID_PASSWORD = "invalid-password";
    private static final String INVALID_LOCATIONS = "/api/ss/invalid-locations";
    private static final String INVALID_HOST = "invalid-host";
    private static final int INVALID_PORT = 55555;

    private static final ParameterizedTypeReference<List> LOCATIONS_TYPE = new ParameterizedTypeReference<List>() {};

    @BeforeClass
    public static void beforeClass() throws IOException {
        SslHelper.configure();
//        System.setProperty("sz.client.httpProxy", "localhost:8888"); // Temporarily enable proxy for development
    }

    @Test(expected = SmartZoneTransientException.class)
    public void invalidHost() {
        new SmartZoneRestClient(INVALID_HOST, PORT, USER, PASSWORD).getForObject(SsAssets.locationsPath(), LOCATIONS_TYPE);
    }

    @Test(expected = SmartZoneTransientException.class)
    public void invalidPort() {
        new SmartZoneRestClient(HOST, INVALID_PORT, USER, PASSWORD).getForObject(SsAssets.locationsPath(), LOCATIONS_TYPE);
    }

    @Test(expected = AuthenticationException.class)
    public void invalidUser() {
        new SmartZoneRestClient(HOST, PORT, INVALID_USER, PASSWORD).getForObject(SsAssets.locationsPath(), LOCATIONS_TYPE);
    }

    @Test(expected = AuthenticationException.class)
    public void invalidPassword() {
        new SmartZoneRestClient(HOST, PORT, USER, INVALID_PASSWORD).getForObject(SsAssets.locationsPath(), LOCATIONS_TYPE);
    }

    // Note that invalid resources are handled by Tomcat and the response is HTML
    @Test(expected = NotFoundException.class)
    public void invalidResource() {
        new SmartZoneRestClient(HOST, PORT, USER, PASSWORD).getForObject(INVALID_LOCATIONS, LOCATIONS_TYPE);
    }

    @Test(expected = NotFoundException.class)
    public void notFound() {
        new SmartZoneRestClient(HOST, PORT, USER, PASSWORD).getForObject(SsAssets.floorplanPath(-1), Floorplan.class);
    }

    @Test
    public void getLocations() {
        SmartZoneRestClient client = new SmartZoneRestClient(HOST, PORT, USER, PASSWORD);
        System.out.println(client.getForObject(SsAssets.locationsPath(), LOCATIONS_TYPE));
        // The second request should not re-authenticate
        System.out.println(client.getForObject(SsAssets.locationsPath(), LOCATIONS_TYPE));
    }

    @Test
    public void updateFloorplan() {
        new SmartZoneRestClient(HOST, PORT, USER, PASSWORD).put(SsAssets.floorplanPath(1), simpleFloorplan());
    }

    private Floorplan simpleFloorplan() {
        return Floorplan.builder()
                .id(1)
                .name("my floorplan")
                .background(ByteArray.of("my background".getBytes()))
                .width(2)
                .height(3)
                .addRacks(
                        Rack.builder()
                                .id(4)
                                .name("my rack")
                                .location(Point2D.of(5, 6))
                                .width(7)
                                .depth(8)
                                .rotation(9.10)
                                .build())
                .build();
    }
}
