package com.panduit.sz.api.shared;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.panduit.sz.api.util.SmartZoneValueStyle;
import com.panduit.sz.util.math.PNumbers;
import org.immutables.value.Value;

/**
 * Point in the 2-dimensional space, defined by {@code x} and {@code y}. The unit of measure is assumed to be inches.
 */
@Value.Immutable
@SmartZoneValueStyle
@JsonSerialize
public abstract class AbstractPoint2D {

    public static final double EPSILON = 0.01; // 1/100th of an inch

    @Value.Parameter
    public abstract double getX();

    @Value.Parameter
    public abstract double getY();

    /**
     * Disallow {@code hashCode()}. We want to compare floating-point properties with an {@code epsilon} and this makes it impossible to
     * define a consistent {@code hashCode()}.
     */
    @Override
    public int hashCode() {
        throw new UnsupportedOperationException("Objects with arbitrary precision fields cannot have a meaningful hashCode() without rounding.");
    }

    /**
     * Customized {@code equals()}. We want to compare floating-point properties with an {@code epsilon}.
     */
    @Override
    public boolean equals(Object another) {
        return this == another || another instanceof AbstractPoint2D && equalsPoint2D((AbstractPoint2D) another);
    }

    private boolean equalsPoint2D(AbstractPoint2D another) {
        return PNumbers.almostEqual(getX(), another.getX(), EPSILON)
                && PNumbers.almostEqual(getY(), another.getY(), EPSILON);
    }
}
