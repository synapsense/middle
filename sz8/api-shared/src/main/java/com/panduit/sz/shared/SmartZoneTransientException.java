package com.panduit.sz.shared;

/** Generic exception indicating a failure that MIGHT NOT re-occur if the operation is retried. */
@SuppressWarnings("unused")
public class SmartZoneTransientException extends SmartZoneRuntimeException {

    public SmartZoneTransientException() {
    }

    public SmartZoneTransientException(String message) {
        super(message);
    }

    public SmartZoneTransientException(String message, Throwable cause) {
        super(message, cause);
    }

    public SmartZoneTransientException(Throwable cause) {
        super(cause);
    }

    public SmartZoneTransientException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
