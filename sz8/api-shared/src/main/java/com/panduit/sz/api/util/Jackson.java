package com.panduit.sz.api.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.guava.GuavaModule;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;

/** Static utility methods to instantiate various configurations of {@link ObjectMapper}. */
public class Jackson {

    private Jackson() {}

    /** Creates an {@code ObjectMapper} configured for the needs of the SmartZone project */
    public static ObjectMapper newDefaultObjectMapper() {
        return new ObjectMapper()
                .registerModules(
                        new Jdk8Module(),
                        new GuavaModule())
                .setPropertyNamingStrategy(new SmartZonePropertyNamingStrategy());
    }
}
