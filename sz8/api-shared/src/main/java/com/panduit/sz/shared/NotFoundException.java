package com.panduit.sz.shared;

/**
 * Used to represent condition of not finding assets such as floorplans etc.
 *
 * Created by SDUH on 12/22/2015.
 */
public class NotFoundException extends SmartZoneNonTransientException {

    public NotFoundException() {
        super();
    }

    public NotFoundException(String message) {
        super(message);
    }

    public NotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public NotFoundException(Throwable cause) {
        super(cause);
    }

    public NotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
