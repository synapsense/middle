package com.panduit.sz.shared;

/** Generic exception indicating a failure that WOULD STILL occur if the operation is retried. */
@SuppressWarnings("unused")
public class SmartZoneNonTransientException extends SmartZoneRuntimeException {

    public SmartZoneNonTransientException() {
    }

    public SmartZoneNonTransientException(String message) {
        super(message);
    }

    public SmartZoneNonTransientException(String message, Throwable cause) {
        super(message, cause);
    }

    public SmartZoneNonTransientException(Throwable cause) {
        super(cause);
    }

    public SmartZoneNonTransientException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
