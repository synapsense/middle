package com.panduit.sz.api.shared;

/** Abstract base class for objects that have a name, like floorplans and racks. */
public abstract class AbstractNamedEntity extends AbstractEntity {

    public abstract String getName();
}
