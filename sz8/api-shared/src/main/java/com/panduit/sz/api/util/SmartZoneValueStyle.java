package com.panduit.sz.api.util;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import org.immutables.value.Value;

/** Meta-annotation capturing global configuration for Immutables */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.CLASS)
@Value.Style(
        // Detect 'is' and 'has' prefixes in accessor methods in addition to 'get'.
        // This must be used in conjunction with SmartZonePropertyNamingStrategy to ensure that Jackson uses the same convention.
        get = {"get*", "is*", "has*"},

        // No prefix or suffix for generated immutable type (generate Foo instead of ImmutableFoo)
        typeImmutable = "*"
)
public @interface SmartZoneValueStyle {
}
