package com.panduit.sz.shared;

/**
 * Base class for all SmartZone exceptions.
 *
 * <p>It is an unchecked exception for the following reasons:</p>
 *
 * <ul>
 *
 * <li>Avoid catching/wrapping/rethrowing exceptions when the method signature does not match.</li>
 *
 * <li>Avoid catching exceptions declared in an interface but not thrown by the implementation class.</li>
 *
 * <li>The functional interfaces from Java 8 do not support checked exceptions.</li>
 *
 * </ul>
 *
 * <p> NOTE: [BC] In principle I like checked exceptions, but in practice the pain of using them outweighs the benefits.</p>
 */
public class SmartZoneRuntimeException extends RuntimeException {

    public SmartZoneRuntimeException() {
    }

    public SmartZoneRuntimeException(String message) {
        super(message);
    }

    public SmartZoneRuntimeException(String message, Throwable cause) {
        super(message, cause);
    }

    public SmartZoneRuntimeException(Throwable cause) {
        super(cause);
    }

    public SmartZoneRuntimeException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
