package com.panduit.sz.api.shared;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.immutables.value.Value;

/** Abstract base class for objects that have a numeric identifier, typically entities persisted to the DB with a surrogate ID. */
public abstract class AbstractEntity {

    /** The ID of the entity, with a default of 0. By convention ID = 0 is reserved for not persisted entities. */
    @Value.Default
    public Integer getId() {
        return 0;
    }

    /**
     * Returns true if the entity is new (not yet persisted to the DB).
     *
     * <p>NOTE: It's annotated with {@code @JsonIgnore} because it's not part of the state of the object.</p>
     */
    @JsonIgnore
    public boolean isNew() {
        return getId() == 0;
    }
}
