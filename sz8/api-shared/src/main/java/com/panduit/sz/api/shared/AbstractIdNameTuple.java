package com.panduit.sz.api.shared;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.panduit.sz.api.util.SmartZoneValueStyle;
import org.immutables.value.Value;

/** Tuple consisting of a numeric ID and a name */
@Value.Immutable
@SmartZoneValueStyle
@JsonSerialize
public abstract class AbstractIdNameTuple {

    @Value.Parameter
    public abstract Integer getId();

    @Value.Parameter
    public abstract String getName();
}
