package com.panduit.sz.api.util;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.cfg.MapperConfig;
import com.fasterxml.jackson.databind.introspect.AnnotatedMethod;

/**
 * Jackson PropertyNamingStrategy, customized to accept 'has' prefixes in accessor methods in addition to 'get' and 'is'. This is used in
 * conjunction with SmartZoneValueStyle so that Jackson and Immutables detect properties in a consistent manner.
 */
public class SmartZonePropertyNamingStrategy extends PropertyNamingStrategy {
    @Override
    public String nameForGetterMethod(MapperConfig<?> config, AnnotatedMethod method, String defaultName) {
        return defaultName.length() > 3 && defaultName.startsWith("has") && Character.isUpperCase(defaultName.charAt(3)) ?
                defaultName.substring(3, 4).toLowerCase() + defaultName.substring(4) : defaultName;
    }
}
