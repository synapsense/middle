package com.panduit.sz.api.shared;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.panduit.sz.api.util.SmartZoneValueStyle;
import org.immutables.value.Value;

/** Wrapper for byte[] that implements equality by value */
@Value.Immutable
@SmartZoneValueStyle
@JsonSerialize
public abstract class AbstractByteArray {

    @Value.Parameter
    public abstract byte[] getBytes();
}
