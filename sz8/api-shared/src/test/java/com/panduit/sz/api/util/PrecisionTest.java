package com.panduit.sz.api.util;

import com.panduit.sz.api.shared.Point2D;
import org.junit.Test;

import static com.panduit.sz.api.shared.AbstractPoint2D.EPSILON;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class PrecisionTest {

    private static final double X = 1.2345;
    private static final double Y = 1.6789;

    @Test
    public void sameValue() {
        assertEquals(Point2D.of(X, Y), Point2D.of(X, Y));
    }

    @Test
    public void almostSameValue() {
        assertEquals(Point2D.of(X, Y), Point2D.of(X + 0.5 * EPSILON, Y - 0.5 * EPSILON));
    }

    @Test
    public void differentValue() {
        assertNotEquals(Point2D.of(X, Y), Point2D.of(X + 1.5 * EPSILON, Y));
        assertNotEquals(Point2D.of(X, Y), Point2D.of(X, Y - 1.5 * EPSILON));
    }

    @Test
    public void conversion() {
        assertEquals(Point2D.of(X, Y), Point2D.of(X / 3 * 7 / 7 * 3, Y / 5 * 9 / 9 * 5));
    }

    @Test
    public void floatPrecision() {
        assertEquals(Point2D.of(1_000 + X, Y), Point2D.of((float) (1_000 + X), Y));
        assertEquals(Point2D.of(10_000 + X, Y), Point2D.of((float) (10_000 + X), Y));
        assertEquals(Point2D.of(100_000 + X, Y), Point2D.of((float) (100_000 + X), Y));
        assertNotEquals(Point2D.of(1_000_000 + X, Y), Point2D.of((float) (1_000_000 + X), Y));
    }

    @Test(expected = UnsupportedOperationException.class)
    public void hashCodeShouldFail() {
        //noinspection ResultOfMethodCallIgnored
        Point2D.of(X, Y).hashCode();
    }
}
