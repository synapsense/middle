package com.panduit.sz.api.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/** Spring configuration for the SZ8/SS integration APIs tests. */
@Configuration
public class SmartZoneApiTestConfig {

    @Bean
    public ObjectMapper jackson() {
        return Jackson.newDefaultObjectMapper();
    }

    @Bean
    public JacksonTestService jacksonTestService(ObjectMapper jackson) {
        return new JacksonTestService(jackson);
    }
}
