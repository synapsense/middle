package com.panduit.sz.api.util;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class SmartZonePropertyNamingStrategyTest {

    private SmartZonePropertyNamingStrategy namingStrategy = new SmartZonePropertyNamingStrategy();

    @Test
    public void whenTheNameDoesNotMatchThenItIsReturnedUnchanged() {
        assertThat(namingStrategy.nameForGetterMethod(null, null, "fo")).isEqualTo("fo");
        assertThat(namingStrategy.nameForGetterMethod(null, null, "foo")).isEqualTo("foo");
        assertThat(namingStrategy.nameForGetterMethod(null, null, "fooB")).isEqualTo("fooB");
        assertThat(namingStrategy.nameForGetterMethod(null, null, "fooBar")).isEqualTo("fooBar");
    }

    @Test
    public void whenTheNameIsTooShortThenItIsReturnedUnchanged() {
        assertThat(namingStrategy.nameForGetterMethod(null, null, "")).isEqualTo("");
        assertThat(namingStrategy.nameForGetterMethod(null, null, "h")).isEqualTo("h");
        assertThat(namingStrategy.nameForGetterMethod(null, null, "ha")).isEqualTo("ha");
        assertThat(namingStrategy.nameForGetterMethod(null, null, "has")).isEqualTo("has");
    }

    @Test
    public void whenTheNameIsNotProperlyCapitalizedThenItIsReturnedUnchanged() {
        assertThat(namingStrategy.nameForGetterMethod(null, null, "hastily")).isEqualTo("hastily");
    }

    @Test
    public void whenTheNameMatchesThenTheHasPrefixIsRemoved() {
        assertThat(namingStrategy.nameForGetterMethod(null, null, "hasX")).isEqualTo("x");
        assertThat(namingStrategy.nameForGetterMethod(null, null, "hasFun")).isEqualTo("fun");
    }
}