package com.panduit.sz.api.util;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.exc.UnrecognizedPropertyException;
import com.panduit.sz.api.shared.ByteArray;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import org.immutables.value.Value;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.assertj.core.api.Assertions.assertThat;

public class JsonSerializationTest extends SmartZoneApiTest {

    @Autowired
    private ObjectMapper jackson;

    @Autowired
    private JacksonTestService jacksonTestService;

    @Test
    public void dependencyInjection() throws Exception {
        assertThat(jackson).isNotNull();
    }

    @Test(expected = JsonMappingException.class)
    public void whenPropertyAbsentInJsonThenDeserializationThrowsException() throws IOException {
        jackson.readValue("{\"x\":1,\"z\":3}", Foo.class);
    }

    @Test
    public void whenOptionalPropertyAbsentInJsonThenDeserializationIsSuccessful() throws IOException {
        assertThat(jackson.readValue("{\"x\":1,\"y\":2}", Foo.class))
                .isEqualTo(Foo.builder().x(1).y(2).build());
    }

    @Test(expected = UnrecognizedPropertyException.class)
    public void whenJsonHasExtraPropertyThenDeserializationThrowsException() throws IOException {
        jackson.readValue("{\"x\":1,\"y\":2,\"z\":3,\"extra\":4}", Foo.class);
    }

    @Test
    public void whenJsonHasIgnoredExtraPropertyThenDeserializationIsSuccessful() throws IOException {
        assertThat(jackson.readValue("{\"x\":1,\"y\":2,\"z\":3,\"ignoredExtra\":4}", Foo.class))
                .isEqualTo(Foo.builder().x(1).y(2).z(3).build());
    }

    @Value.Immutable
    @SmartZoneValueStyle
    // extra fields declared as ignored don't break deserialization
    @JsonIgnoreProperties("ignoredExtra")
    @JsonSerialize
    static abstract class AbstractFoo {
        public abstract Integer getX();
        public abstract Integer getY();
        public abstract Optional<Integer> getZ(); // optional fields can be absent in the JSON
        public abstract List<String> getItems(); // collection properties default to empty when absent in the JSON
    }

    @Test
    public void valueWithIsGetter() throws IOException {
        jacksonTestService.testToJsonAndBack(
                ValueWithIsGetter.of(true),
                "{\"happy\":true}");
    }

    @Value.Immutable
    @SmartZoneValueStyle
    @JsonSerialize
    static abstract class AbstractValueWithIsGetter {
        @Value.Parameter
        public abstract boolean isHappy();
    }

    @Test
    public void valueWithHasGetter() throws IOException {
        jacksonTestService.testToJsonAndBack(
                ValueWithHasGetter.of(true),
                "{\"fun\":true}");
    }

    @Value.Immutable
    @SmartZoneValueStyle
    @JsonSerialize
    static abstract class AbstractValueWithHasGetter {
        @Value.Parameter
        public abstract boolean hasFun();
    }

    @Test
    public void requiredEnum() throws IOException {
        jacksonTestService.testToJsonAndBack(
                ValueWithEnum.of(Numbers.ONE),
                "{\"number\":\"ONE\"}");
    }

    enum Numbers {ONE, TWO}

    @Value.Immutable
    @SmartZoneValueStyle
    @JsonSerialize
    static abstract class AbstractValueWithEnum {
        @Value.Parameter
        public abstract Numbers getNumber();
    }

    @Test
    public void optionalEnum() throws IOException {
        jacksonTestService.testToJsonAndBack(
                ValueWithOptionalEnum.of(Optional.of(Numbers.TWO)),
                "{\"number\":\"TWO\"}");

        jacksonTestService.testToJsonAndBack(
                ValueWithOptionalEnum.of(Optional.empty()),
                "{\"number\":null}");
    }

    @Value.Immutable
    @SmartZoneValueStyle
    @JsonSerialize
    static abstract class AbstractValueWithOptionalEnum {
        @Value.Parameter
        public abstract Optional<Numbers> getNumber();
    }

    @Test
    public void byteArray() throws IOException {
        jacksonTestService.testToJsonAndBack(ValueWithByteArray.of(ByteArray.of("123".getBytes())), "{\"image\":{\"bytes\":\"MTIz\"}}");
    }

    @Value.Immutable
    @SmartZoneValueStyle
    @JsonSerialize
    static abstract class AbstractValueWithByteArray {
        @Value.Parameter
        public abstract ByteArray getImage();
    }

    @Test
    public void optionalByteArray() throws IOException {
        jacksonTestService.testToJsonAndBack(ValueWithOptionalByteArray.of(Optional.of(ByteArray.of("123".getBytes()))), "{\"image\":{\"bytes\":\"MTIz\"}}");
        jacksonTestService.testToJsonAndBack(ValueWithOptionalByteArray.of(Optional.empty()), "{\"image\":null}");
    }

    @Value.Immutable
    @SmartZoneValueStyle
    @JsonSerialize
    static abstract class AbstractValueWithOptionalByteArray {
        @Value.Parameter
        public abstract Optional<ByteArray> getImage();
    }
}
