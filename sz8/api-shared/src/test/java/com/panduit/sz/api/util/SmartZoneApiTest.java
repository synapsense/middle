package com.panduit.sz.api.util;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

/**
 * Base class for Spring tests exercising the SZ8/SS integration APIs.
 *
 * <p>NOTE: All tests extending this class will inherit the annotations below.</p>
 */

// Make this a Spring test so we can isolate instantiation of test dependencies.
@RunWith(SpringJUnit4ClassRunner.class)

// Specify the class responsible to instantiate the Spring beans (a.k.a configuration).
@ContextConfiguration(classes = SmartZoneApiTestConfig.class)

// Configure a minimal set of test execution listeners, some default ones are not on the path. See META-INF/spring.factories.
@TestExecutionListeners(listeners = {DependencyInjectionTestExecutionListener.class})
public abstract class SmartZoneApiTest {
}
