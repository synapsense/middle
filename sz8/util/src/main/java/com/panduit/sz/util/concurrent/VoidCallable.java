package com.panduit.sz.util.concurrent;

/** Similar to a Callable, but call() returns void. This makes it more lambda-friendly. */
public interface VoidCallable {
    void call() throws Exception;
}
