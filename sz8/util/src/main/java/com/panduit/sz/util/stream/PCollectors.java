package com.panduit.sz.util.stream;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.stream.Collector;
import java.util.stream.Collector.Characteristics;

/** More implementations of {@link Collector}, adding to those from the standard library. */
public class PCollectors {

    private PCollectors() {
    }

    /**
     * Returns a {@code Collector} that accumulates the input elements into a new {@code ListMultimap}. The key is obtained by applying the
     * {@code keyMapper} function to the input elements.
     *
     * @param <T> the type of the input elements
     * @param <K> the output type of the key mapping function
     * @return a {@code Collector} which collects all the input elements into a {@code ListMultimap}
     */
    public static <T, K> Collector<T, ?, ListMultimap<K, T>> toListMultimap(Function<? super T, ? extends K> keyMapper) {
        BiConsumer<ListMultimap<K, T>, T> accumulator = (result, item) -> result.put(keyMapper.apply(item), item);
        BinaryOperator<ListMultimap<K, T>> combiner = (left, right) -> {
            left.putAll(right);
            return left;
        };
        return Collector.of(ArrayListMultimap::create, accumulator, combiner, Characteristics.IDENTITY_FINISH);
    }
}
