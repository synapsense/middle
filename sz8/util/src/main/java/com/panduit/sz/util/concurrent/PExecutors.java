package com.panduit.sz.util.concurrent;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/** Utility methods for working with executors */
public abstract class PExecutors {

    private static final String THREAD_NAME_FORMAT = "%s %d"; // NAME NUMBER

    /** Creates a single-threaded {@code ScheduledExecutor} with a custom name */
    public static ScheduledExecutorService newSingleThreadScheduledExecutor(String name) {
        return Executors.newSingleThreadScheduledExecutor(new NamedThreadFactory(name));
    }

    /**
     * Stops the executors by invoking {@code shutdownNow()} and then waiting 3 sec for all of them to terminate. Returns true if all
     * executors terminated before the timeout.
     */
    public static boolean stopImmediately(ExecutorService... executors) throws InterruptedException {
        for (ExecutorService executor : executors) {
            executor.shutdownNow();
        }
        long endTime = System.currentTimeMillis() + 3000;
        for (ExecutorService executor : executors) {
            long remainingWait = endTime - System.currentTimeMillis();
            if (endTime <= 0) {
                return false;
            }
            executor.awaitTermination(remainingWait, TimeUnit.MILLISECONDS);
        }
        return true;
    }

    /** Submits the {@code voidCallable} to the {@code executor} and waits until it completes */
    public static void executeV(ExecutorService executor, VoidCallable voidCallable) throws ExecutionException, InterruptedException {
        executor.submit(callable(voidCallable)).get();
    }

    /**
     * Invokes the {@code voidCallable} in a separate thread and waits for it to complete, after which returns true. If the timeout expires,
     * the thread is interrupted and returns false.
     */
    public static boolean executeVWithTimeout(VoidCallable voidCallable, long timeout, TimeUnit timeUnit) throws InterruptedException {
        ExecutorService executor = newSingleThreadScheduledExecutor("executeWithTimeout()");
        executor.submit(callable(voidCallable));
        executor.shutdown();
        if (executor.awaitTermination(timeout, timeUnit)) {
            return true;
        }
        executor.shutdownNow();
        return false;
    }

    /**
     * Executes the given {@code voidCallable} asynchronously in a new executor and returns the corresponding {@code Future}. This method
     * returns immediately.
     */
    public static Future<Void> asyncV(VoidCallable voidCallable) {
        return async(callable(voidCallable));
    }

    /**
     * Executes the given {@code callable} asynchronously in a new executor and returns the corresponding {@code Future}. This method
     * returns immediately.
     */
    private static <V> Future<V> async(Callable<V> callable) {
        ExecutorService executor = newSingleThreadScheduledExecutor("async()");
        Future<V> result = executor.submit(callable);
        executor.shutdown();
        return result;
    }

    /** Wraps a {@code VoidCallable} into a {@code Callable} */
    private static Callable<Void> callable(final VoidCallable voidCallable) {
        return () -> {
            voidCallable.call();
            return null;
        };
    }

    /** {@code ThreadFactory} with a customizable name */
    private static class NamedThreadFactory implements ThreadFactory {

        private final String name;
        private final AtomicInteger number;

        public NamedThreadFactory(String name) {
            this.name = name;
            this.number = new AtomicInteger(1);
        }

        public Thread newThread(Runnable runnable) {
            return new Thread(runnable, String.format(THREAD_NAME_FORMAT, name, number.getAndIncrement()));
        }
    }
}
