package com.panduit.sz.util.function;

/**
 * Variant of {@link Block} that returns void.
 */
public interface VoidBlock {
    void call();
}
