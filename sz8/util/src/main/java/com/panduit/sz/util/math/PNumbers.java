package com.panduit.sz.util.math;

/** Utility methods for working with numbers */
public class PNumbers {

    /**
     * Returns {@code true} if the two values differ by at most epsilon.
     *
     * <p>This method is useful when comparing values that may have rounding errors after some computation.</p>
     */
    public static boolean almostEqual(double v1, double v2, double epsilon) {
        return Double.compare(v1, v2) == 0 || Math.abs(v1 - v2) <= epsilon;
    }
}
