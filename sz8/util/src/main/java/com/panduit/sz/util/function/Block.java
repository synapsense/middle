package com.panduit.sz.util.function;

/**
 * Similar to {@code Callable}, but only throws unchecked exceptions.
 *
 * <p>It is intended to be used with "execute around" methods. Example: {@code transaction(() -> doStuff())}.</p>
 */
public interface Block<V> {
    V call();

    static Block fromVoidBlock(VoidBlock voidBlock) {
        return () -> {
            voidBlock.call();
            return null;
        };
    }
}
