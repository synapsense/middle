package com.panduit.sz.util.io;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

/**
 * Provides utility methods for working with files, building on top of {@code java.nio.file.Files}.
 *
 * <p>As the number of methods grows we may want to switch to the more powerful commons-io library.</p>
 */
public class PFiles {

    private PFiles() {
    }

    /**
     * Copies a resource to a file using the loader of the given class.
     *
     * <p>The resource can be relative to the class such as {@code foo.properties} or absolute such as {@code
     * /com/panduit/foo.properties}.</p>
     *
     * <p>If the target file already exists then it is silently overwritten.</p>
     *
     * <p>Checked exceptions are wrapped in {@code RuntimeException}.</p>
     */
    public static void copyResource(String resource, Path target, Class<?> loadingClass) {
        try {
            URL resourceUrl = loadingClass.getResource(resource);
            if (resourceUrl == null) {
                throw new IllegalArgumentException("Resource not found: " + resource);
            }
            // Converting URL to Path does not work, see http://stackoverflow.com/questions/22605666/java-access-files-in-jar-causes-java-nio-file-filesystemnotfoundexception
            //      Files.copy(Paths.get(resourceUrl.toURI()), target, StandardCopyOption.REPLACE_EXISTING);
            // We'll just use the stream (less convenient, but functional).
            try (InputStream in = resourceUrl.openStream()) {
                Files.copy(in, target, StandardCopyOption.REPLACE_EXISTING);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /** Moves source file to target file. Parent directories are created as needed. Target is replaced if existing. */
    public static void moveFileToFile(Path source, Path target) throws IOException {
        Path targetParent = target.getParent();
        if (!Files.exists(targetParent)) {
            Files.createDirectories(targetParent);
        }
        Files.move(source, target, REPLACE_EXISTING);
    }

    /** Creates a file in the temp directory that will be deleted when the VM terminates. */
    public static Path createAutoDeleteTempFile(String prefix, String suffix) throws IOException {
        File result = File.createTempFile(prefix, suffix);
        result.deleteOnExit();
        return result.toPath();
    }
}
