package com.panduit.sz.util.io;

import com.google.common.io.Resources;
import java.io.IOException;

/**
 * Provides utility methods for working with classpath resources.
 */
public class PResources {

    private PResources() {
    }

    /** Reads the resource with the given name as a byte array */
    public static byte[] readBytes(String resourceName) {
        try {
            return Resources.toByteArray(Resources.getResource(resourceName));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
