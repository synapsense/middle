package com.panduit.sz.util.io;

import java.io.IOException;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class PFilesTest {

    @Test
    public void testCopyRelativeResource() throws IOException {
        testCopyResource("test-resource.txt");
    }

    @Test
    public void testCopyAbsoluteResource() throws IOException {
        testCopyResource("/com/panduit/sz/util/io/test-resource.txt");
    }

    @Test
    public void testCopyResourceWithOverwrite() throws IOException {
        Files.write(Paths.get("test-resource-copy.txt"), Collections.singletonList("Goodbye, World!"));
        testCopyResource("test-resource.txt");
    }

    @Test(expected = RuntimeException.class)
    public void testInvalidResource() throws IOException {
        testCopyResource("invalid-test-resource.txt");
    }

    private static void testCopyResource(String resource) throws IOException {
        Path actualFile = Paths.get("test-resource-copy.txt");
        try {
            PFiles.copyResource(resource, actualFile, PFilesTest.class);
            assertThat(actualFile).hasContent("Hello, World!");
        } finally {
            Files.deleteIfExists(actualFile);
        }
    }

    /**
     * Makes sure that we can extract the resource from a jar. The initial implementation used the more convenient {@link Files#copy(Path,
     * Path, CopyOption...)} which <a href="http://stackoverflow.com/questions/22605666/java-access-files-in-jar-causes-java-nio-file-filesystemnotfoundexception">
     * doesn't work</a> when the resource is in a jar.
     *
     * <p>Here we don't care about asserting the content, we just want to confirm that the resource can be accessed.</p>
     */
    @Test
    public void testCopyResourceFromJar() throws IOException {
        Path actualFile = Paths.get("Test.class");
        try {
            PFiles.copyResource("/org/junit/Test.class", actualFile, PFilesTest.class);
        } finally {
            Files.deleteIfExists(actualFile);
        }
    }
}