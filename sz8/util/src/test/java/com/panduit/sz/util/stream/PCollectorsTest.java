package com.panduit.sz.util.stream;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableListMultimap;
import org.junit.Test;

import static com.panduit.sz.util.stream.PCollectors.toListMultimap;
import static org.assertj.guava.api.Assertions.assertThat;

public class PCollectorsTest {

    @Test
    public void multimapCollector() {
        assertThat(ImmutableList.of("Mickey", "Minnie", "Goofy").stream().collect(toListMultimap(String::length)))
                .hasSameEntriesAs(ImmutableListMultimap.of(6, "Mickey", 6, "Minnie", 5, "Goofy"));
    }

    @Test
    public void parallelMultimapCollector() {
        assertThat(ImmutableList.of("Mickey", "Minnie", "Goofy").parallelStream().collect(toListMultimap(String::length)))
                .hasSameEntriesAs(ImmutableListMultimap.of(6, "Mickey", 6, "Minnie", 5, "Goofy"));
    }
}