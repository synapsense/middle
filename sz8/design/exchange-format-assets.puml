@startuml

interface LocationService {
	' Returns the root of the location tree
	getLocationTree() : Location
}
LocationService --> Location

interface FloorplanService {
	getFloorplan(floorplanId : Integer) : Floorplan

	' For importing offline MS project into SZ
	' SZ automatically assumes a call to instrumentFloorplan()
	' Returns floorplanId
	createFloorplan(parentLocationId : Integer, floorplan : Floorplan) : Integer

	' Updates the floorplan in the DB and returns the SZ IDs of any newly generated containers.
	' MS uses the returned names to correlate the IDs back to objects. It's up to MS to enforce unique names.
	updateFloorplan(floorplan : Floorplan) : List<IdNameTuple>

	' Marks the floorplan as instrumented for WSN.
	' Returns false if the floorplan is already instrumented. MS should call getFloorplan() next.
	instrumentFloorplan(floorplanId : Integer) : Boolean

	' Marks the floorplan as not instrumented by MS.
	' Returns true if the floorplan transitioned from instrumented to uninstrumented.
	uninstrumentFloorplan(floorplanId : Integer) : Boolean
}
FloorplanService --> Floorplan

abstract class Entity {
	id : Integer
}

abstract class NamedEntity extends Entity {
	name : String
}
class Location extends NamedEntity {
	locationLevel : LocationLevel
	' Internal nodes can have children
	children : List<Location>
	' Extra attributes of floorplan locations
	isWsnInstrumented : Boolean
}

class Floorplan extends NamedEntity {
	background : byte[]
	width : double
	height : double
	racks : List<Rack>
	crahs : List<Crah>
	verticalTemps : List<VerticalTemp>
	genericTemps : List<GenericTemp>
	pressures : List<Pressure>
}
Floorplan o-- Rack
Floorplan o-- Crah
Floorplan o-- VerticalTemp
Floorplan o-- GenericTemp
Floorplan o-- Pressure

class Container  extends NamedEntity {
	location : Point2D
	width : double
	depth : double
	rotation : double
}

class Rack extends Container
class Crah extends Container
class VerticalTemp extends Container
class GenericTemp extends Container
class Pressure extends Container

class Point2D {
	x : double
	y : double
}

class IdNameTuple {
    id: Integer
    name: String
}

enum LocationLevel {
	COUNTRY
	STATE
	CITY
	COMPANY
	STREET
	BUILDING
	FLOOR
	ROOM
	TECHNICIAN
	GROUP
}

@enduml