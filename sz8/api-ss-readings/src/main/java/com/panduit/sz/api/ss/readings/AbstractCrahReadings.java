package com.panduit.sz.api.ss.readings;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.panduit.sz.api.shared.AbstractEntity;
import com.panduit.sz.api.util.SmartZoneValueStyle;
import java.util.Optional;
import org.immutables.value.Value;

/** Sensor samples for a CRAH asset */
@Value.Immutable
@SmartZoneValueStyle
@JsonSerialize
public abstract class AbstractCrahReadings extends AbstractEntity {

    /** A sample of the supply air temperature this CRAH is generating */
    public abstract Optional<Reading> getSupplyTemp();

    /** A sample of return air temperature this CRAH is pulling from the room */
    public abstract Optional<Reading> getReturnTemp();

    /** A sample of the dewpoint of the supply air this CRAH is generating */
    public abstract Optional<Reading> getSupplyDp();

    /** A sample of the dewpoint of the return air this CRAH is pulling from the room */
    public abstract Optional<Reading> getReturnDp();

    /** A metric describing the Air Loss Ratio of this CRAH unit */
    public abstract Optional<Reading> getAlr();
}

