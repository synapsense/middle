package com.panduit.sz.api.ss.readings;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.panduit.sz.api.shared.AbstractEntity;
import com.panduit.sz.api.util.SmartZoneValueStyle;
import java.util.Optional;
import org.immutables.value.Value;

/** Sensor samples for a free-standing 3-sensor vertical monitoring device */
@Value.Immutable
@SmartZoneValueStyle
@JsonSerialize
public abstract class AbstractVerticalTempReadings extends AbstractEntity {

    /** A sample of the "top" temperature, about 42U off the floor */
    public abstract Optional<Reading> getTopTemperature();

    /** A sample of the "middle" temperature, about 21U off the floor */
    public abstract Optional<Reading> getMiddleTemperature();

    /** A sample of the "bottom" temperature, about 1U off the floor */
    public abstract Optional<Reading> getBottomTemperature();
}
