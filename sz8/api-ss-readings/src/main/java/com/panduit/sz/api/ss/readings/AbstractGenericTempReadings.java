package com.panduit.sz.api.ss.readings;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.panduit.sz.api.shared.AbstractEntity;
import com.panduit.sz.api.util.SmartZoneValueStyle;
import java.util.Optional;
import org.immutables.value.Value;

/** Sensor samples for a generic temperature sensor */
@Value.Immutable
@SmartZoneValueStyle
@JsonSerialize
public abstract class AbstractGenericTempReadings extends AbstractEntity {

    /** A sample of temperature */
    public abstract Optional<Reading> getTemperature();
}
