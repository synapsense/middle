package com.panduit.sz.api.ss.readings;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.panduit.sz.api.shared.AbstractEntity;
import com.panduit.sz.api.util.SmartZoneValueStyle;
import java.util.Optional;
import org.immutables.value.Value;

/** Sensor samples for a Rack asset */
@Value.Immutable
@SmartZoneValueStyle
@JsonSerialize
public abstract class AbstractRackReadings extends AbstractEntity {

    /** A sample of the cold (intake) top temp */
    public abstract Optional<Reading> getCTopTemp();

    /** A sample of the cold (intake) middle temp */
    public abstract Optional<Reading> getCMidTemp();

    /** A sample of the cold (intake) bottom temp */
    public abstract Optional<Reading> getCBotTemp();

    /** A sample of hot (exhaust) top temp */
    public abstract Optional<Reading> getHTopTemp();

    /** A sample of hot (exhaust) middle temp */
    public abstract Optional<Reading> getHMidTemp();

    /** A sample of hot (exhaust) bottom temp */
    public abstract Optional<Reading> getHBotTemp();

    /** A sample of the supply air cooling this rack */
    public abstract Optional<Reading> getRefTemp();

    /** A sample of the relative humidity, usually at the cold (intake) top */
    public abstract Optional<Reading> getRh();

    /** A metric describing the volume of recirculated air */
    public abstract Optional<Reading> getRa();

    /** A metric describing the rate of change of the cold top temp */
    public abstract Optional<Reading> getCtROfChange();
}
