package com.panduit.sz.api.ss.readings;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.panduit.sz.api.shared.AbstractEntity;
import com.panduit.sz.api.util.SmartZoneValueStyle;
import java.util.Optional;
import org.immutables.value.Value;

/** Sensor samples for a subfloor differential pressure device */
@Value.Immutable
@SmartZoneValueStyle
@JsonSerialize
public abstract class AbstractPressureReadings extends AbstractEntity {

    /** A sample of the subfloor differential pressure */
    public abstract Optional<Reading> getPressure();
}
