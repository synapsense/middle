package com.panduit.sz.api.ss.readings;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.panduit.sz.api.util.SmartZoneValueStyle;
import java.time.Instant;
import org.immutables.value.Value;

/** A single sensor sample taken at a specific instant */
@Value.Immutable
@SmartZoneValueStyle
@JsonSerialize
public abstract class AbstractReading {
    public abstract Double getValue();
    public abstract Instant getInstant();
}
