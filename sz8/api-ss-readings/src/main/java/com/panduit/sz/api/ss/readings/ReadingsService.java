package com.panduit.sz.api.ss.readings;

import java.util.List;

/** Service to retrieve environmental readings from the ES, and deliver
 * it in a way that can be integrated into a SmartZone readings model */
public interface ReadingsService {

    /** Get a list of readings for each floorplan on an environmental server */
    List<FloorplanData> getFloorplanData();
}
