package com.panduit.sz.api.ss.readings;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.panduit.sz.api.shared.AbstractEntity;
import com.panduit.sz.api.util.SmartZoneValueStyle;
import java.util.List;
import org.immutables.value.Value;

/** A collection of sensor samples for all instrumented assets on a floorplan */
@Value.Immutable
@SmartZoneValueStyle
@JsonSerialize
public abstract class AbstractFloorplanData extends AbstractEntity {
    public abstract List<RackReadings> getRackReadings();
    public abstract List<CrahReadings> getCrahReadings();
    public abstract List<VerticalTempReadings> getVerticalTempReadings();
    public abstract List<GenericTempReadings> getGenericTempReadings();
    public abstract List<PressureReadings> getPressureReadings();
}