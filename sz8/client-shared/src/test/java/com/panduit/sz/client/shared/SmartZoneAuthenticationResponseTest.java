package com.panduit.sz.client.shared;

import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.panduit.sz.api.util.Jackson;
import java.io.IOException;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class SmartZoneAuthenticationResponseTest {

    private final ObjectMapper jackson = Jackson.newDefaultObjectMapper();

    @Test
    public void whenExtraPropertiesTheDeserializationIsOk() throws IOException {
        assertThat(jackson.readValue(
                "{\"success\":false,\"errorCode\":1,\"errorMessages\":[\"Bad password\"], \"ignoredExtraProperty\":\"ignored\"}",
                SmartZoneAuthenticationResponse.class)
        ).isEqualTo(SmartZoneAuthenticationResponse.builder()
                .success(false)
                .errorCode(1)
                .addErrorMessages("Bad password")
                .build()
        );
    }

    @Test(expected = JsonMappingException.class)
    public void whenMissingPropertiesTheDeserializationFails() throws IOException {
        assertThat(jackson.readValue(
                "{\"errorCode\":1,\"errorMessages\":[\"Bad password\"], \"ignoredExtraProperty\":\"ignored\"}",
                SmartZoneAuthenticationResponse.class)
        ).isEqualTo(SmartZoneAuthenticationResponse.builder()
                .success(false)
                .errorCode(1)
                .addErrorMessages("Bad password")
                .build()
        );
    }
}