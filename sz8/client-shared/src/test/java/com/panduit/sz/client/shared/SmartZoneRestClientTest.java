package com.panduit.sz.client.shared;

import com.panduit.sz.shared.AuthenticationException;
import com.panduit.sz.shared.NotFoundException;
import com.panduit.sz.shared.SmartZoneNonTransientException;
import org.junit.Before;
import org.junit.Test;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.client.MockRestServiceServer;

import static com.panduit.sz.client.shared.SmartZoneRestClient.AUTHENTICATE_FORM_PASSWORD;
import static com.panduit.sz.client.shared.SmartZoneRestClient.AUTHENTICATE_FORM_USERNAME;
import static com.panduit.sz.client.shared.SmartZoneRestClient.AUTHENTICATE_PATH;
import static com.panduit.sz.client.shared.SmartZoneRestClient.CONTEXT_PATH;
import static com.panduit.sz.client.shared.SmartZoneRestClient.JSESSIONID;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.content;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withBadRequest;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withServerError;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withStatus;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withUnauthorizedRequest;

public class SmartZoneRestClientTest {

    private SmartZoneRestClient smartZoneRestClient;
    private MockRestServiceServer mockRestServiceServer;

    private static final String HOST = "localhost";
    private static final int PORT = 8443;
    private static final String USERNAME = "admin";
    private static final String PASSWORD = "secret";
    private static final String TEST_RESOURCE = "/test";
    private static final ParameterizedTypeReference<Boolean> BOOLEAN_TYPE = new ParameterizedTypeReference<Boolean>() {};

    @Before
    public void setUp() {
        smartZoneRestClient = new SmartZoneRestClient(HOST, PORT, USERNAME, PASSWORD);
        mockRestServiceServer = MockRestServiceServer.createServer(smartZoneRestClient.getRestTemplate());
    }

    @Test
    public void getForObject() throws Exception {
        expectAuthentication();
        expectGetForObject(TEST_RESOURCE, "true");
        expectGetForObject(TEST_RESOURCE, "false");

        assertTrue(smartZoneRestClient.getForObject(TEST_RESOURCE, Boolean.class));
        assertFalse(smartZoneRestClient.getForObject(TEST_RESOURCE, Boolean.class));

        verify();
    }

    @Test
    public void getForObjectWithParameterizedType() {
        expectAuthentication();
        expectGetForObject(TEST_RESOURCE, "true");

        assertTrue(smartZoneRestClient.getForObject(TEST_RESOURCE, BOOLEAN_TYPE));

        verify();
    }

    @Test
    public void put() {
        expectAuthentication();
        expectPut(TEST_RESOURCE, "true");

        smartZoneRestClient.put(TEST_RESOURCE, "true");

        verify();
    }

    @Test
    public void putForObjectWithoutContent() {
        expectAuthentication();
        expectPutForObjectWithoutContent(TEST_RESOURCE, "true");

        assertTrue(smartZoneRestClient.putForObject(TEST_RESOURCE, Boolean.class));

        verify();
    }

    @Test
    public void putForObject() {
        expectAuthentication();
        expectPutForObject(TEST_RESOURCE, "true", "false");

        assertFalse(smartZoneRestClient.putForObject(TEST_RESOURCE, "true", BOOLEAN_TYPE));

        verify();
    }

    @Test
    public void deleteForObject() {
        expectAuthentication();
        expectDeleteForObject(TEST_RESOURCE, "true");

        assertTrue(smartZoneRestClient.deleteForObject(TEST_RESOURCE, Boolean.class));

        verify();
    }

    @Test(expected = SmartZoneNonTransientException.class)
    public void unexpectedStatusCodeOnExistingSession() {
        expectAuthentication();
        expectGetForObject(TEST_RESOURCE, "true");
        // Now simulate an unexpected status code
        mockRestServiceServer.expect(requestTo(uri(TEST_RESOURCE))).andRespond(withStatus(HttpStatus.CONTINUE));

        assertTrue(smartZoneRestClient.getForObject(TEST_RESOURCE, Boolean.class));
        smartZoneRestClient.getForObject(TEST_RESOURCE, Boolean.class);
    }

    @Test(expected = SmartZoneNonTransientException.class)
    public void unexpectedClientExceptionOnExistingSession() {
        expectAuthentication();
        expectGetForObject(TEST_RESOURCE, "true");
        // Now simulate a client exception
        mockRestServiceServer.expect(requestTo(uri(TEST_RESOURCE))).andRespond(withBadRequest());

        assertTrue(smartZoneRestClient.getForObject(TEST_RESOURCE, Boolean.class));
        smartZoneRestClient.getForObject(TEST_RESOURCE, Boolean.class);
    }

    @Test(expected = SmartZoneNonTransientException.class)
    public void unexpectedStatusCodeOnNewSession() {
        expectAuthentication();
        // Now simulate an unexpected status code
        mockRestServiceServer.expect(requestTo(uri(TEST_RESOURCE))).andRespond(withStatus(HttpStatus.CONTINUE));

        smartZoneRestClient.getForObject(TEST_RESOURCE, Boolean.class);
    }

    @Test(expected = NotFoundException.class)
    public void notFound() {
        expectAuthentication();
        // Now simulate NOT_FOUND
        mockRestServiceServer.expect(requestTo(uri(TEST_RESOURCE))).andRespond(withStatus(HttpStatus.NOT_FOUND));

        smartZoneRestClient.getForObject(TEST_RESOURCE, Boolean.class);
    }

    @Test
    public void reAuthentication() {
        expectAuthentication();
        expectGetForObject(TEST_RESOURCE, "true");
        // Now simulate a server restart and return 401
        mockRestServiceServer.expect(requestTo(uri(TEST_RESOURCE))).andRespond(withUnauthorizedRequest());
        // The client should re-authenticate
        expectAuthentication();
        // And then we're back to normal
        expectGetForObject(TEST_RESOURCE, "false");

        assertTrue(smartZoneRestClient.getForObject(TEST_RESOURCE, Boolean.class));
        assertFalse(smartZoneRestClient.getForObject(TEST_RESOURCE, Boolean.class));

        verify();
    }

    @Test(expected = SmartZoneNonTransientException.class)
    public void unexpectedStatusCodeForAuthentication() {
        mockRestServiceServer
                .expect(requestTo(uri(AUTHENTICATE_PATH)))
                .andExpect(method(HttpMethod.POST))
                .andExpect(content()
                        .string(String.format("%s=%s&%s=%s", AUTHENTICATE_FORM_USERNAME, USERNAME, AUTHENTICATE_FORM_PASSWORD, PASSWORD)))
                .andRespond(withStatus(HttpStatus.CREATED));

        smartZoneRestClient.getForObject(TEST_RESOURCE, Boolean.class);
    }

    @Test(expected = AuthenticationException.class)
    public void authenticationFailure() {
        mockRestServiceServer
                .expect(requestTo(uri(AUTHENTICATE_PATH)))
                .andExpect(method(HttpMethod.POST))
                .andExpect(content()
                        .string(String.format("%s=%s&%s=%s", AUTHENTICATE_FORM_USERNAME, USERNAME, AUTHENTICATE_FORM_PASSWORD, PASSWORD)))
                .andRespond(withSuccess("{\"success\":false,\"errorCode\":5,\"errorMessages\":[\"Simulated Error\"]}", MediaType.APPLICATION_JSON));

        smartZoneRestClient.getForObject(TEST_RESOURCE, Boolean.class);
    }

    @Test(expected = SmartZoneNonTransientException.class)
    public void unexpectedServerErrorForAuthentication() {
        mockRestServiceServer
                .expect(requestTo(uri(AUTHENTICATE_PATH)))
                .andExpect(method(HttpMethod.POST))
                .andExpect(content()
                        .string(String.format("%s=%s&%s=%s", AUTHENTICATE_FORM_USERNAME, USERNAME, AUTHENTICATE_FORM_PASSWORD, PASSWORD)))
                .andRespond(withServerError());

        smartZoneRestClient.getForObject(TEST_RESOURCE, Boolean.class);
    }

    private void expectAuthentication() {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add( HttpHeaders.SET_COOKIE, JSESSIONID + "=1234");

        mockRestServiceServer
                .expect(requestTo(uri(AUTHENTICATE_PATH)))
                .andExpect(method(HttpMethod.POST))
                .andExpect(content()
                        .string(String.format("%s=%s&%s=%s", AUTHENTICATE_FORM_USERNAME, USERNAME, AUTHENTICATE_FORM_PASSWORD, PASSWORD)))
                .andRespond(withSuccess("{\"success\":true,\"errorCode\":0,\"errorMessages\":[]}", MediaType.APPLICATION_JSON)
                        .headers(httpHeaders));
    }

    private void expectGetForObject(String resource, String response) {
        mockRestServiceServer
                .expect(requestTo(uri(resource)))
                .andExpect(method(HttpMethod.GET))
                .andRespond(withSuccess(response, MediaType.APPLICATION_JSON));
    }

    private void expectPut(String resource, String content) {
        mockRestServiceServer
                .expect(requestTo(uri(resource)))
                .andExpect(method(HttpMethod.PUT))
                .andExpect(content().string(content))
                .andRespond(withSuccess());
    }

    private void expectPutForObjectWithoutContent(String resource, String response) {
        mockRestServiceServer
                .expect(requestTo(uri(resource)))
                .andExpect(method(HttpMethod.PUT))
                .andRespond(withSuccess(response, MediaType.APPLICATION_JSON));
    }

    private void expectPutForObject(String resource, String content, String response) {
        mockRestServiceServer
                .expect(requestTo(uri(resource)))
                .andExpect(method(HttpMethod.PUT))
                .andExpect(content().string(content))
                .andRespond(withSuccess(response, MediaType.APPLICATION_JSON));
    }

    private void expectDeleteForObject(String resource, String response) {
        mockRestServiceServer
                .expect(requestTo(uri(resource)))
                .andExpect(method(HttpMethod.DELETE))
                .andRespond(withSuccess(response, MediaType.APPLICATION_JSON));
    }

    private String uri(String resource) {
        return String.format("https://%s:%s/%s%s", HOST, PORT, CONTEXT_PATH, resource);
    }

    private void verify() {
        mockRestServiceServer.verify();
    }
}