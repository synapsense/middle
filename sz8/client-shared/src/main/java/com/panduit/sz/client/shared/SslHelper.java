package com.panduit.sz.client.shared;

import com.panduit.sz.util.io.PFiles;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import javax.net.ssl.HttpsURLConnection;

/** Utilities for working with self-signed certificates */
public class SslHelper {

    private static final String TRUSTSTORE = "smartzone.truststore";
    private static final String TRUSTSTORE_PASSWORD = "acmnms";

    private SslHelper() {
    }

    /** Performs necessary configuration to connect to SmartZone components that use self-signed certificates. */
    public static void configure() {
        try {
            System.out.println("!!! CAUTION !!! HTTPS is used for encryption, but the configuration is vulnerable to MITM attacks. Revise before releasing 8.0!");
            SslHelper.enableSmartZoneTruststore(PFiles.createAutoDeleteTempFile("smartzone-", ".truststore"));
            SslHelper.disableHostnameValidation();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Configures the JVM to trust the truststore containing SmartZone self-signed certificates.
     *
     * @param truststorePath the bundled truststore will be saved as this file. Make sure it refers to a location that the application can
     *                       write to.
     */
    private static void enableSmartZoneTruststore(Path truststorePath) {
        // Extract the bundled truststore
        PFiles.copyResource(TRUSTSTORE, truststorePath, SslHelper.class);
        // Configure SSL to use the truststore
//        System.out.println("SmartZone truststore: " + truststorePath);
        System.setProperty("javax.net.ssl.trustStore", truststorePath.toString());
        System.setProperty("javax.net.ssl.trustStorePassword", TRUSTSTORE_PASSWORD);
    }

    /**
     * The SZ self-signed certificate doesn't match the hostname, so we must disable SSL hostname validation (and open up to MITM attacks).
     *
     * <p>The SZ certificate has {@code CN=localhost:8443}. Even localhost wouldn't match because the port is now considered part of the
     * hostname.</p>
     *
     * <p>NOTE: This configuration is only for the JDK implementation of SSL, it does not cover 3rd-party libraries like
     * commons-httpclient.</p>
     */
    private static void disableHostnameValidation() {
        HttpsURLConnection.setDefaultHostnameVerifier((hostname, sslSession) -> true);
    }

}
