package com.panduit.sz.client.shared;

import com.google.common.base.Verify;
import com.google.common.collect.ImmutableSet;
import com.panduit.sz.api.util.Jackson;
import com.panduit.sz.shared.AuthenticationException;
import com.panduit.sz.shared.NotFoundException;
import com.panduit.sz.shared.SmartZoneNonTransientException;
import com.panduit.sz.shared.SmartZoneRuntimeException;
import com.panduit.sz.shared.SmartZoneTransientException;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URI;
import java.util.Collections;
import java.util.ListIterator;
import java.util.Optional;
import java.util.Set;
import java.util.function.Supplier;
import javax.net.ssl.SSLHandshakeException;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import static org.springframework.http.HttpStatus.NO_CONTENT;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.HttpStatus.UNAUTHORIZED;

/** Wrapper around {@code RestTemplate} that works with SmartZone authentication */
public class SmartZoneRestClient {

    private final String host;
    private final int port;
    private final String username;
    private final String password;

    private final RestTemplate restTemplate;

    // The cookie for the session with the SmartZone Server. It's null when a session hasn't been established or has expired.
    private String jsessionid;

    // Hard-coded context path of the SmartZone rest.war webapp. I don't want to expose it until there's a real need for customization.
    public static final String CONTEXT_PATH = "rest";
    public static final String AUTHENTICATE_PATH = "/api/authenticate";
    public static final String AUTHENTICATE_FORM_USERNAME = "username";
    public static final String AUTHENTICATE_FORM_PASSWORD = "password";

    // The cookie for the tomcat session
    public static final String JSESSIONID = "JSESSIONID";

    // System property for RestTemplate proxy
    public static final String HTTP_PROXY = "sz.client.httpProxy";

    private final Set<HttpStatus> VALID_GET_STATUSES = ImmutableSet.of(OK);
    private final Set<HttpStatus> VALID_PUT_STATUSES = ImmutableSet.of(OK, NO_CONTENT);
    private final Set<HttpStatus> VALID_DELETE_STATUSES = ImmutableSet.of(OK, NO_CONTENT);

    public SmartZoneRestClient(String host, int port, String username, String password) {
        this.host = host;
        this.port = port;
        this.username = username;
        this.password = password;

        this.restTemplate = createRestTemplate();
    }

    /** Expose the underlying {@code RestTemplate} for MS unit tests */
    public RestTemplate getRestTemplate() {
        return restTemplate;
    }

    /**
     * Retrieve a representation by doing a GET on the URL. The response is converted and returned.
     *
     * @param resourcePath a resource path, relative to the context path of the webapp. Ex: {@code /api/ss/floorplans/21}. Note that
     *                     variables are already expanded.
     * @param responseType the type of the return value
     * @return the converted object
     */
    public <T> T getForObject(String resourcePath, Class<T> responseType) {
        return exchangeWithAuthentication(VALID_GET_STATUSES, () ->
                restTemplate.exchange(uri(resourcePath), HttpMethod.GET, authenticatedRequest(), responseType)
        );
    }

    /**
     * Retrieve a representation by doing a GET on the URL. The response is converted and returned.
     *
     * @param resourcePath a resource path, relative to the context path of the webapp. Ex: {@code /api/ss/floorplans/21}. Note that
     *                     variables are already expanded.
     * @param responseType the type of the return value
     * @return the converted object
     */
    public <T> T getForObject(String resourcePath, ParameterizedTypeReference<T> responseType) {
        return exchangeWithAuthentication(VALID_GET_STATUSES, () ->
                restTemplate.exchange(uri(resourcePath), HttpMethod.GET, authenticatedRequest(), responseType)
        );
    }

    public void put(String resourcePath, Object resourceContent) {
        exchangeWithAuthentication(VALID_PUT_STATUSES, () ->
                restTemplate.exchange(uri(resourcePath), HttpMethod.PUT, authenticatedRequest(resourceContent), Void.class)
        );
    }

    /** PUT without content */
    public <T> T putForObject(String resourcePath, Class<T> responseType) {
        return exchangeWithAuthentication(VALID_PUT_STATUSES, () ->
                restTemplate.exchange(uri(resourcePath), HttpMethod.PUT, authenticatedRequest(), responseType)
        );
    }

    public <T> T putForObject(String resourcePath, Object resourceContent, ParameterizedTypeReference<T> responseType) {
        return exchangeWithAuthentication(VALID_PUT_STATUSES, () ->
                restTemplate.exchange(uri(resourcePath), HttpMethod.PUT, authenticatedRequest(resourceContent), responseType)
        );
    }

    public <T> T deleteForObject(String resourcePath, Class<T> responseType) {
        return exchangeWithAuthentication(VALID_DELETE_STATUSES, () ->
                restTemplate.exchange(uri(resourcePath), HttpMethod.DELETE,authenticatedRequest(), responseType)
        );
    }

    private <T> T exchangeWithAuthentication(Set<HttpStatus> validStatuses, Supplier<ResponseEntity<T>> block) {
        try {
            // If we have a session, use it and hope it's not expired.
            if (jsessionid != null) {
                try {
                    ResponseEntity<T> responseEntity = block.get();
                    if (!validStatuses.contains(responseEntity.getStatusCode())) {
                        throw mapResponseEntityToException(responseEntity);
                    }
                    return responseEntity.getBody();
                } catch (HttpClientErrorException e) {
                    if (e.getStatusCode() == UNAUTHORIZED) {
                        // The session expired, we'll retry.
                        jsessionid = null;
                    } else {
                        throw e;
                    }
                }
            }
            // Authenticate and try with the new session
            authenticate();
            ResponseEntity<T> responseEntity = block.get();
            if (!validStatuses.contains(responseEntity.getStatusCode())) {
                throw mapResponseEntityToException(responseEntity);
            }
            return responseEntity.getBody();
        } catch (RestClientException e) {
            throw mapRestClientException(e);
        }
    }

    private <T> SmartZoneRuntimeException mapResponseEntityToException(ResponseEntity<T> responseEntity) {
        return new SmartZoneNonTransientException("Unexpected API response: " + responseEntity.getStatusCode());
    }

    /** authenticates against the SmartZone server and stores JSESSIONID cookie in sessionId */
    private void authenticate() {
        try {
            // This is a form post, so RestTemplate requires us to use MultiValueMap
            MultiValueMap<String, String> formData = new LinkedMultiValueMap<>();
            formData.add(AUTHENTICATE_FORM_USERNAME, username);
            formData.add(AUTHENTICATE_FORM_PASSWORD, password);

            // We can't use the more convenient postForObject() because we need to retrieve the JSESSIONID cookie.
            // TODO: Use UriConstants.AUTH_ENDPOINT when available in the jar.
            ResponseEntity<SmartZoneAuthenticationResponse> response = restTemplate.postForEntity(
                    uri(AUTHENTICATE_PATH), formData, SmartZoneAuthenticationResponse.class);

            // SZ always sends code 200 and not appropriate 4** codes, even when user/pwd is bad
            if (response.getStatusCode() != OK) {
                throw new SmartZoneNonTransientException("Unexpected authentication error: " + response.getStatusCode());
            }

            SmartZoneAuthenticationResponse responseBody = response.getBody();
            if (!responseBody.isSuccess()) {
                throw new AuthenticationException("Authentication failure: " + responseBody.getErrorMessages());
            }

            // It's all good, we can retrieve the cookie
            jsessionid = Verify.verifyNotNull(response.getHeaders().get(HttpHeaders.SET_COOKIE), "Successful authentication response without Set-Cookie.")
                    .stream()
                    .filter(x -> x.contains(JSESSIONID))
                    .findFirst()
                    .orElseThrow(() -> new SmartZoneNonTransientException("Missing JSESSIONID cookie after successful authentication"));
        } catch (RestClientException e) {
            throw mapRestClientException(e);
        }
    }

    /** creates an HttpEntity with the JSESSIONID cookie */
    private HttpEntity<String> authenticatedRequest() {
        HttpHeaders headers = new HttpHeaders();
        headers.set(HttpHeaders.COOKIE, jsessionid);
        return new HttpEntity<>(headers);
    }

    /** creates an HttpEntity with the JSESSIONID cookie and the given content */
    private <T> HttpEntity<T> authenticatedRequest(T content) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.set(HttpHeaders.COOKIE, jsessionid);
        return new HttpEntity<>(content, headers);
    }

    /**
     * Builds a full URI
     *
     * @param resourcePath a resource path, relative to the context path of the webapp. Ex: {@code /api/ss/floorplans/21}. Note that
     *                     variables are already expanded.
     * @return the full URI. Ex: {@code https://localhost:8443/rest/api/ss/floorplans/21}
     */
    private URI uri(String resourcePath) {
        return UriComponentsBuilder.newInstance()
                .scheme("https")
                .host(host)
                .port(port)
                .path(CONTEXT_PATH)
                .path(resourcePath)
                .build()
                .encode()
                .toUri();
    }

    /** Creates a new {@code RestTemplate} where the Jackson message converter is replaced with our customized one */
    private static RestTemplate createRestTemplate() {
        RestTemplate result = new RestTemplate();

        // RequestFactory
        SimpleClientHttpRequestFactory requestFactory = new SimpleClientHttpRequestFactory();
        Optional<Proxy> proxy = createProxy();
        if (proxy.isPresent()) {
            requestFactory.setProxy(proxy.get());
        }
        result.setRequestFactory(requestFactory);

        // MessageConverters
        ListIterator<HttpMessageConverter<?>> iter = result.getMessageConverters().listIterator();
        while (iter.hasNext()) {
            if (iter.next() instanceof MappingJackson2HttpMessageConverter) {
                iter.set(new MappingJackson2HttpMessageConverter(Jackson.newDefaultObjectMapper()));
            }
        }

        return result;
    }

    /** Creates an HTTP proxy for RestTemplate based on the {@code sz.client.httpProxy} system property. The format is {@code host:port} */
    private static Optional<Proxy> createProxy() {
        try {
            String proxyString = System.getProperty(HTTP_PROXY);
            if (proxyString == null) {
                return Optional.empty();
            }
            String[] hostPort = proxyString.split(":");
            if (hostPort.length != 2) {
                throw new SmartZoneNonTransientException("Invalid proxy configuration, expected host:port but was " + proxyString);
            }
            return Optional.of(new Proxy(Proxy.Type.HTTP, new InetSocketAddress(hostPort[0], Integer.parseInt(hostPort[1]))));
        } catch (NumberFormatException e) {
            throw new SmartZoneNonTransientException("Invalid proxy port. " + e.getMessage());
        }
    }

    /**
     * Translates RestClientException into SmartZoneRuntimeException.
     *
     * <p>IMPL NOTE: The matching server translation is in {@code SmartZoneExceptionHandler}.</p>
     */
    private static SmartZoneRuntimeException mapRestClientException(RestClientException e) {
        if (e.getCause() instanceof IOException && ! (e.getCause() instanceof SSLHandshakeException)) {
            // SSLHandshakeException indicates a problem with the server certificate. That's not transient (even though it's  an I/O exception)
            return new SmartZoneTransientException("Unable to connect to SmartZone. ", e);
        }
        else if (e instanceof HttpStatusCodeException) {
            HttpStatusCodeException httpStatusCodeException = (HttpStatusCodeException) e;
            switch (httpStatusCodeException.getStatusCode()) {
                case NOT_FOUND:
                    return new NotFoundException(httpStatusCodeException.getResponseBodyAsString());
                default:
                    return new SmartZoneNonTransientException("SmartZone API error.", e);
            }

        } else {
            return new SmartZoneNonTransientException("Unexpected SmartZone API error.", e);
        }
    }
}
