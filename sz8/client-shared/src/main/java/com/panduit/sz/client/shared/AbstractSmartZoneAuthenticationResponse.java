package com.panduit.sz.client.shared;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.panduit.sz.api.util.SmartZoneValueStyle;
import java.util.List;
import org.immutables.value.Value;

/**
 * Model for the SmartZone authentication response, containing the minimal information needed by an API client.
 *
 * <p>NOTE: Extra properties are ignored because of {@code @JsonIgnoreProperties} and missing properties are detected by the class generated
 * by Immutables. Quite a powerful combination.</p>
 */
@Value.Immutable
@SmartZoneValueStyle
@JsonSerialize
@JsonIgnoreProperties(ignoreUnknown = true)
public abstract class AbstractSmartZoneAuthenticationResponse {

    public abstract boolean isSuccess();
    public abstract int getErrorCode();
    public abstract List<String> getErrorMessages();
}
