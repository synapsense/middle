package com.panduit.sz.client.ss.readings;

import com.panduit.sz.api.ss.readings.FloorplanData;
import com.panduit.sz.api.ss.readings.RackReadings;
import com.panduit.sz.api.ss.readings.ReadingsService;
import com.panduit.sz.shared.SmartZoneNonTransientException;
import com.synapsense.dto.CollectionTO;
import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

public class ReadingsServiceImpl implements ReadingsService {

    // ES object types
    public static final String FLOORPLAN = "DC";
    public static final String RACK = "RACK";
    public static final String CRAC = "CRAC";
    public static final String PRESSURE = "PRESSURE";

    // Rack property names
    public static final String C_TOP = "cTop";
    public static final String C_MID = "cMid";
    public static final String C_BOT = "cBot";
    public static final String H_TOP = "hTop";
    public static final String H_MID = "hMid";
    public static final String H_BOT = "hBot";
    public static final String REF = "ref";
    public static final String RA = "ra";
    public static final String RH = "rh";
    public static final String CTROFCHANGE = "cTROfChange";

    public static final String SZ_ID = "smartZoneId";

    private EsClient es;

    public ReadingsServiceImpl(String host) {
        es = new EsClient(host);
    }

    @Override
    public List<FloorplanData> getFloorplanData() {
        try {
            Collection<CollectionTO> floorplanOs = es.objectsByType(FLOORPLAN);
            List<FloorplanData> floorplanData = new LinkedList<>();

            for (CollectionTO floorplanO: floorplanOs) {
                ValueTO szidValue = floorplanO.getSinglePropValue(SZ_ID);
                if (szidValue != null && szidValue.getValue() != null && (Integer)szidValue.getValue() != 0) {
                    floorplanData.add(
                            floorplanData(floorplanO)
                    );
                }
            }
            return floorplanData;
        } catch (Exception e) {
            throw new SmartZoneNonTransientException("Error communicating with Environment Server", e);
        }
    }

    protected FloorplanData floorplanData(CollectionTO floorplanObject) throws Exception {
        return FloorplanData.builder()
                .id(EsClient.intValue(floorplanObject, SZ_ID))
                .rackReadings(rackReadings(floorplanObject.getObjId()))
                .build();
    }

    protected List<RackReadings> rackReadings(TO<?> floorplanId) throws Exception {
        Collection<CollectionTO> rackObjects = es.floorplanObjectsByType(floorplanId, RACK);
        List<RackReadings> rackReadings = new LinkedList<>();
        for (CollectionTO rackO: rackObjects) {
            if (EsClient.intValue(rackO, SZ_ID) != null) {
                rackReadings.add(
                        RackReadings.builder()
                                .id(EsClient.intValue(rackO, SZ_ID))
                                .cTopTemp(es.getSensorReading(rackO, C_TOP))
                                .cMidTemp(es.getSensorReading(rackO, C_MID))
                                .cBotTemp(es.getSensorReading(rackO, C_BOT))
                                .hTopTemp(es.getSensorReading(rackO, H_TOP))
                                .hMidTemp(es.getSensorReading(rackO, H_MID))
                                .hBotTemp(es.getSensorReading(rackO, H_BOT))
                                .refTemp(es.getSensorReading(rackO, REF))
                                .rh(es.getSensorReading(rackO, RH))
                                .ra(es.getCalculatedReading(rackO, RA))
                                .ctROfChange(es.getCalculatedReading(rackO, CTROFCHANGE))
                                .build()
                );
            }
        }
        return rackReadings;
    }
}
