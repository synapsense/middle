package com.panduit.sz.client.ss.readings;

import com.panduit.sz.api.ss.readings.Reading;
import com.panduit.sz.shared.SmartZoneRuntimeException;
import com.synapsense.dto.CollectionTO;
import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;
import com.synapsense.service.Environment;
import com.synapsense.util.EJBClientConfigurator;
import com.synapsense.util.ServerLoginModule;
import java.time.Instant;
import java.util.Collection;
import java.util.Collections;
import java.util.Optional;
import java.util.Properties;
import javax.naming.InitialContext;
import javax.naming.NamingException;

public class EsClient {

    // Properties
    public static final String LAST_VALUE = "lastValue";

    public Environment env;

    public EsClient(String host) {
        this.env = connect(host);
    }

    public Collection<CollectionTO> objectsByType(String typeName) {
        Collection<TO<?>> objectsByType = env.getObjectsByType(typeName);
        if(!objectsByType.isEmpty()) {
            return env.getAllPropertiesValues(objectsByType);
        }
        return Collections.emptyList();
    }

    public Collection<CollectionTO> floorplanObjectsByType(TO<?> floorplanId, String typeName) {
        Collection<TO<?>> relatedObjects = env.getRelatedObjects(floorplanId, typeName, true);
        if(!relatedObjects.isEmpty()) {
            return env.getAllPropertiesValues(relatedObjects);
        }
        return Collections.emptyList();
    }

    public static Integer intValue(CollectionTO cto, String propertyName) {
        return (Integer) cto.getSinglePropValue(propertyName).getValue();
    }

    public Optional<Reading> getSensorReading(CollectionTO object, String property) throws Exception {
        ValueTO sensor = object.getSinglePropValue(property);
        if(sensor != null && sensor.getValue() != null) {
            Collection<ValueTO> sensorProperties = env.getAllPropertiesValues((TO<?>) sensor.getValue());
            for (ValueTO v : sensorProperties) {
                if (v.getPropertyName().equals(LAST_VALUE)) {
                    if (isErrorCode((double) v.getValue())) {
                        return Optional.empty();
                    }
                    return Optional.of(Reading.builder()
                            .value((double) v.getValue())
                            .instant(Instant.ofEpochMilli(v.getTimeStamp()))
                            .build());
                }
            }
        }
        return Optional.empty();
    }

    public Optional<Reading> getCalculatedReading(CollectionTO object, String property) {
        ValueTO calcReading = object.getSinglePropValue(property);
        if(calcReading != null && calcReading.getValue() != null && !isErrorCode((double) calcReading.getValue())) {
            return Optional.of(Reading.builder()
                    .value((double) calcReading.getValue())
                    .instant(Instant.ofEpochMilli(calcReading.getTimeStamp()))
                    .build());
        } else {
            return Optional.empty();
        }
    }

    private boolean isErrorCode(double value) {
        return value == -2000 || value == -3000 || value == -5000;
    }

    /** Encapsulates the internal details of connecting to the Environment EJB on the ES server */
    private static Environment connect(String host) {
        EJBClientConfigurator.setupEJBClientContextSelector(host, "admin", "admin");
        Properties properties = new Properties();
        properties.put("java.naming.factory.url.pkgs", "org.jboss.ejb.client.naming");
        try {
            return ServerLoginModule.getService(new InitialContext(properties), "Environment", Environment.class);
        } catch (NamingException e) {
            throw new SmartZoneRuntimeException("Failed to connect to Environment Server",e);
        }
    }
}
