package com.panduit.sz.client.ss.readings;

import com.panduit.sz.api.ss.readings.FloorplanData;
import com.panduit.sz.api.ss.readings.ReadingsService;
import java.util.List;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * Created by roan on 2/15/2016.
 */
public class ReadingsServiceExampleClient {

    private static ReadingsService rs;

    @BeforeClass
    public static void setup() throws Exception {
        rs = new ReadingsServiceImpl("localhost");
    }

    @Test
    public void getData() {
        List<FloorplanData> data = rs.getFloorplanData();
        System.out.println(data.toString());
    }


}
