package com.panduit.sz.client.ss.readings;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Preconditions;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;
import com.synapsense.dto.CollectionTO;
import com.synapsense.dto.EnvObjectBundle;
import com.synapsense.dto.EnvObjectHistory;
import com.synapsense.dto.ObjectType;
import com.synapsense.dto.ObjectTypeMatchMode;
import com.synapsense.dto.PropertyDescr;
import com.synapsense.dto.Relation;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOBundle;
import com.synapsense.dto.Tag;
import com.synapsense.dto.TagDescriptor;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.EnvException;
import com.synapsense.exception.IllegalInputParameterException;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.exception.PropertyNotFoundException;
import com.synapsense.exception.ServerConfigurationException;
import com.synapsense.exception.TagNotFoundException;
import com.synapsense.exception.UnableToConvertPropertyException;
import com.synapsense.exception.UnableToConvertTagException;
import com.synapsense.exception.UnsupportedRelationException;
import com.synapsense.search.QueryDescription;
import com.synapsense.service.Environment;
import com.synapsense.service.impl.dao.to.AbstractTO;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class FakeEnvironment implements Environment{
    private static final String DATACENTER = "DC";
    private static final String RACK = "RACK";
    public static final String SZ_ID = "smartZoneId";

    private final ListMultimap<String, TO<?>> objectsByType = ArrayListMultimap.create();
    private final ListMultimap<TO<?>, TO<?>> objectRelationships = ArrayListMultimap.create();
    private final Map<TO<?>, CollectionTO> objectProperties = new HashMap<>();
    private final Set<TO<?>> danglingPtrs = new HashSet<>();

    public FakeEnvironment(String configPath) {
        try(InputStream is = FakeEnvironment.class.getResourceAsStream(configPath)){
            parseEnvironmentConfig(is);
        } catch (IOException e) {
            e.printStackTrace();
            throw new IllegalArgumentException("Error parsing resource", e);
        }
    }

    public FakeEnvironment(JsonNode rootNode) {
        JsonNode environmentData = rootNode.path("environmentData");
        parseObjectsByType(environmentData);
    }

    private void parseEnvironmentConfig(InputStream is) throws IOException {
        ObjectMapper m = new ObjectMapper();
        JsonNode rootNode = m.readTree(is);
        JsonNode environmentData = rootNode.path("environmentData");
        parseObjectsByType(environmentData);
    }

    private void parseObjectsByType(JsonNode environmentData) {
        for(JsonNode dc : environmentData){
            TO<?> dcObject = new MockTO(dc.path("id").asText(), DATACENTER);
            if (!dc.path("properties").path("szId").isNull()) {
                this.objectsByType.put(DATACENTER, dcObject);
                storeObjectSzId(dcObject, dc);
                for (JsonNode child : dc.path("children")) {
                    if (!child.path("properties").path("szId").isNull()
                            && RACK.equals(child.path("type").asText())) {
                        TO<?> rackObject = new MockTO(child.path("id").asText(), RACK);
                        this.objectsByType.put(RACK, rackObject);
                        storeObjectSzId(rackObject, child);
                        this.objectRelationships.put(dcObject, rackObject);
                        storeRackProperties(rackObject, child);
                    }
                }
            }
        }
    }

    private void storeObjectSzId(TO<?> object, JsonNode node) {
        ValueTO dcSzId = new ValueTO(SZ_ID, node.path("properties").path("szId").asInt());
        List<ValueTO> dcProps = new ArrayList<>(Collections.singletonList(dcSzId));
        CollectionTO dcPropMap = new CollectionTO(object, dcProps);
        this.objectProperties.put(object, dcPropMap);
    }

    private void storeRackProperties(TO<?> rackObject, JsonNode child) {
        Iterator<Map.Entry<String, JsonNode>> properties = child.path("properties").fields();
        while (properties.hasNext()){
            Map.Entry<String, JsonNode> propEntry = properties.next();
            switch (propEntry.getKey()) {
                case "cTop" :
                case "cMid" :
                case "cBot" :
                case "hTop" :
                case "hMid" :
                case "hBot" :
                case "ref" :
                case "rh" :
                    if(!propEntry.getValue().isNull()) {
                        TO<?> sensorObject = storeSensor(propEntry.getKey(), rackObject);
                        storeReading(propEntry, sensorObject, "lastValue");
                    }
                    break;
                case "ra" :
                case "cTROfChange" :
                    storeReading(propEntry, rackObject, propEntry.getKey());
                    break;
                default :
                    break;
            }
        }
    }

    private TO<?> storeSensor(String sensor, TO<?> rackObject) {
        TO<?> sensorObject = new MockTO(rackObject.getID() + sensor, "SENSOR");
        ValueTO property = new ValueTO(sensor, sensorObject);
        storeProperty(rackObject, property);
        return sensorObject;
    }

    private void storeReading(Map.Entry<String, JsonNode> propEntry, TO<?> sensorObject, String propertyName) {
        JsonNode propNode = propEntry.getValue();
        if (propNode.isTextual()  && "ObjectNotFoundException".equals(propNode.asText())) {
            danglingPtrs.add(sensorObject);
        } else {
            JsonNode lastValue = propNode.path("lastValue");
            JsonNode readingNode = lastValue.path("value");
            final Double reading;
            if(!readingNode.isNull()){
                reading = readingNode.asDouble();
            } else {
                reading = null;
            }
            long timestamp = lastValue.path("timestamp").asLong();
            ValueTO property = new ValueTO(propertyName, reading, timestamp);
            storeProperty(sensorObject, property);
        }
    }

    private void storeProperty(TO<?> object, ValueTO property) {
        if(this.objectProperties.containsKey(object)){
            this.objectProperties.get(object).addValue(property);
        } else {
            ArrayList<ValueTO> propList = new ArrayList<>(Collections.singletonList(property));
            CollectionTO propMap = new CollectionTO(object, propList);
            this.objectProperties.put(object, propMap);
        }
    }

    /*
    ================
    MOCKED METHODS
    ================
     */

    @Override
     public Collection<TO<?>> getObjectsByType(String typeName) {
        return this.objectsByType.get(typeName);
    }

    @Override
    public Collection<ValueTO> getAllPropertiesValues(TO<?> objId) throws ObjectNotFoundException {
        if (danglingPtrs.contains(objId)) {
            throw new ObjectNotFoundException("I'm sorry Dave, I'm afraid I can't do that.");
        }
        return this.objectProperties.get(objId).getPropValues();
    }

    @Override
    public Collection<CollectionTO> getAllPropertiesValues(Collection<TO<?>> objIds) {
        if (objIds == null || objIds.isEmpty()) {
            throw new IllegalInputParameterException("value must neither be null nor empty.");
        }
        return this.objectProperties.entrySet()
                .stream()
                .filter(entry -> objIds.contains(entry.getKey()))
                .map(Map.Entry::getValue)
                .collect(Collectors.toList());
    }

    @Override
    public Collection<TO<?>> getRelatedObjects(TO<?> objId, String objectType, boolean isChild) {
        Preconditions.checkArgument(isChild, "This mocked class in can only handle parent-child relationships");
        return this.objectRelationships.get(objId).stream()
                .filter(child -> objectType.equals(child.getTypeName()))
                .collect(Collectors.toList());
    }

    /*
    ===============
    MOCKED CLASSES
    ===============
     */

    private static class MockTO extends AbstractTO<String> implements TO<String> {
        public MockTO(String id, String typeName){
            super(id, typeName);
        }
    }

    /*
    ================
    UNIMPLEMENTED METHODS
    ================
     */

    @Override
    public ObjectType createObjectType(String name) throws EnvException {
        return null;
    }

    @Override
    public ObjectType[] getObjectTypes(String[] names) {
        return new ObjectType[0];
    }

    @Override
    public ObjectType getObjectType(String name) {
        return null;
    }

    @Override
    public String[] getObjectTypes() {
        return new String[0];
    }

    @Override
    public void updateObjectType(ObjectType objType) throws EnvException {

    }

    @Override
    public void deleteObjectType(String typeName) throws EnvException {

    }

    @Override
    public TO<?> createObject(String typeName) throws EnvException {
        return null;
    }

    @Override
    public TO<?> createObject(String typeName, ValueTO[] propertyValues) throws EnvException {
        return null;
    }

    @Override
    public TOBundle createObjects(EnvObjectBundle objectsToCreate) throws EnvException {
        return null;
    }

    @Override
    public Collection<TO<?>> getObjectsByType(String typeName, ObjectTypeMatchMode matchMode) {
        return null;
    }

    @Override
    public Collection<TO<?>> getObjects(String typeName, ValueTO[] propertyValues) {
        return null;
    }

    @Override
    public Collection<TO<?>> getObjects(QueryDescription queryDescr) {
        return null;
    }

    @Override
    public Collection<TO<?>> getObjects(String typeName, ObjectTypeMatchMode matchMode, ValueTO[] propertyValues) {
        return null;
    }

    @Override
    public void deleteObject(TO<?> objId) throws ObjectNotFoundException {

    }

    @Override
    public void deleteObject(TO<?> objId, int level) throws ObjectNotFoundException {

    }

    @Override
    public void setAllPropertiesValues(TO<?> objId, ValueTO[] values) throws ObjectNotFoundException, PropertyNotFoundException, UnableToConvertPropertyException {

    }

    @Override
    public void setAllPropertiesValues(TO<?> objId, Collection<ValueTO> values, Collection<Tag> tags) throws EnvException {

    }

    @Override
    public <RETURN_TYPE> RETURN_TYPE getPropertyValue(TO<?> objId, String propName, Class<RETURN_TYPE> clazz) throws ObjectNotFoundException, PropertyNotFoundException, UnableToConvertPropertyException {
        return null;
    }

    @Override
    public <REQUESTED_TYPE> void setPropertyValue(TO<?> objId, String propName, REQUESTED_TYPE value) throws ObjectNotFoundException, PropertyNotFoundException, UnableToConvertPropertyException {

    }

    @Override
    public <REQUESTED_TYPE> void addPropertyValue(TO<?> objId, String propName, REQUESTED_TYPE value) throws ObjectNotFoundException, PropertyNotFoundException, UnableToConvertPropertyException {

    }

    @Override
    public <REQUESTED_TYPE> void removePropertyValue(TO<?> objId, String propName, REQUESTED_TYPE value) throws ObjectNotFoundException, PropertyNotFoundException, UnableToConvertPropertyException {

    }

    @Override
    public <RETURN_TYPE> Collection<ValueTO> getHistory(TO<?> objId, String propName, Class<RETURN_TYPE> clazz) throws ObjectNotFoundException, UnableToConvertPropertyException, PropertyNotFoundException {
        return null;
    }

    @Override
    public <RETURN_TYPE> Collection<ValueTO> getHistory(TO<?> objId, String propName, int size, int start, Class<RETURN_TYPE> clazz) throws ObjectNotFoundException, PropertyNotFoundException, UnableToConvertPropertyException {
        return null;
    }

    @Override
    public <RETURN_TYPE> Collection<ValueTO> getHistory(TO<?> objId, String propName, Date start, Date end, Class<RETURN_TYPE> clazz) throws ObjectNotFoundException, PropertyNotFoundException, UnableToConvertPropertyException {
        return null;
    }

    @Override
    public EnvObjectHistory getHistory(TO<?> objId, Collection<String> propNames, Date start, Date end) throws ObjectNotFoundException, PropertyNotFoundException {
        return null;
    }

    @Override
    public <RETURN_TYPE> Collection<ValueTO> getHistory(TO<?> objId, String propName, Date start, int numberPoints, Class<RETURN_TYPE> clazz) throws ObjectNotFoundException, PropertyNotFoundException, UnableToConvertPropertyException {
        return null;
    }

    @Override
    public Integer getHistorySize(TO<?> objId, String propName) throws ObjectNotFoundException, PropertyNotFoundException {
        return null;
    }

    @Override
    public void setRelation(TO<?> parentId, TO<?> childId) throws ObjectNotFoundException, UnsupportedRelationException {

    }

    @Override
    public void setRelation(List<Relation> relations) throws ObjectNotFoundException, UnsupportedRelationException {

    }

    @Override
    public void removeRelation(TO<?> parentId, TO<?> childId) throws ObjectNotFoundException {

    }

    @Override
    public void removeRelation(List<Relation> relations) throws ObjectNotFoundException {

    }

    @Override
    public Collection<TO<?>> getParents(TO<?> objId, String typeName) throws ObjectNotFoundException {
        return null;
    }

    @Override
    public Collection<TO<?>> getParents(TO<?> objId) throws ObjectNotFoundException {
        return null;
    }

    @Override
    public Collection<TO<?>> getChildren(TO<?> objId) throws ObjectNotFoundException {
        return null;
    }

    @Override
    public Collection<TO<?>> getChildren(TO<?> objId, String typeName) throws ObjectNotFoundException {
        return null;
    }

    @Override
    public Collection<CollectionTO> getChildren(Collection<TO<?>> objIds, String typeName) {
        return null;
    }

    @Override
    public Collection<CollectionTO> getChildren(Collection<TO<?>> objIds) {
        return null;
    }

    @Override
    public long getPropertyTimestamp(TO<?> objId, String propName) throws ObjectNotFoundException, PropertyNotFoundException {
        return 0;
    }

    @Override
    public PropertyDescr getPropertyDescriptor(String typeName, String propName) throws ObjectNotFoundException, PropertyNotFoundException {
        return null;
    }

    @Override
    public Collection<CollectionTO> getPropertyValue(Collection<TO<?>> objIds, String[] propNames) {
        return null;
    }

    @Override
    public <RETURN_TYPE> RETURN_TYPE getPropertyValue(String typeName, String propName, Class<RETURN_TYPE> clazz) throws ObjectNotFoundException, PropertyNotFoundException, UnableToConvertPropertyException {
        return null;
    }

    @Override
    public <REQUESTED_TYPE> void setPropertyValue(String typeName, String propName, REQUESTED_TYPE value) throws ObjectNotFoundException, PropertyNotFoundException, UnableToConvertPropertyException {

    }

    @Override
    public void configurationComplete() throws ServerConfigurationException {

    }

    @Override
    public void configurationStart() throws ServerConfigurationException {

    }

    @Override
    public void setAllPropertiesValues(TO<?> objId, ValueTO[] values, boolean persist) throws ObjectNotFoundException, PropertyNotFoundException, UnableToConvertPropertyException {

    }

    @Override
    public <REQUESTED_TYPE> void setPropertyValue(TO<?> objId, String propName, REQUESTED_TYPE value, boolean persist) throws PropertyNotFoundException, ObjectNotFoundException, UnableToConvertPropertyException {

    }

    @Override
    public boolean exists(TO<?> objId) throws IllegalInputParameterException {
        return false;
    }

    @Override
    public TagDescriptor getTagDescriptor(String typeName, String propName, String tagName) throws ObjectNotFoundException, PropertyNotFoundException, TagNotFoundException {
        return null;
    }

    @Override
    public <RETURN_TYPE> RETURN_TYPE getTagValue(TO<?> objId, String propName, String tagName, Class<RETURN_TYPE> clazz) throws ObjectNotFoundException, PropertyNotFoundException, TagNotFoundException, UnableToConvertTagException {
        return null;
    }

    @Override
    public Collection<Tag> getAllTags(TO<?> objId, String propName) throws ObjectNotFoundException, PropertyNotFoundException {
        return null;
    }

    @Override
    public Collection<Tag> getAllTags(TO<?> objId) throws ObjectNotFoundException {
        return null;
    }

    @Override
    public Map<TO<?>, Collection<Tag>> getAllTags(String typeName) {
        return null;
    }

    @Override
    public Map<TO<?>, Map<String, Collection<Tag>>> getTags(Collection<TO<?>> objIds, List<String> propertyNames, List<String> tagNames) {
        return null;
    }

    @Override
    public void setTagValue(TO<?> objId, String propName, String tagName, Object value) throws PropertyNotFoundException, ObjectNotFoundException, TagNotFoundException, UnableToConvertTagException {

    }

    @Override
    public void setAllTagsValues(TO<?> objId, Collection<Tag> tagsToSet) throws PropertyNotFoundException, ObjectNotFoundException, TagNotFoundException, UnableToConvertTagException {

    }
}
