package com.panduit.sz.client.ss.readings;

import com.synapsense.service.Environment;
import mockit.Mock;
import mockit.MockUp;

/**
 * Created by roan on 3/4/2016.
 */
public class MockEsClient extends MockUp<EsClient> {
    private final Environment env;

    public MockEsClient(Environment env) {
        this.env = env;
    }

    @Mock
    Environment connect(String host) {
        return this.env;
    }
}
