package com.panduit.sz.client.ss.readings;

import com.panduit.sz.api.ss.readings.FloorplanData;
import com.panduit.sz.api.ss.readings.RackReadings;
import com.panduit.sz.api.ss.readings.Reading;
import com.panduit.sz.api.ss.readings.ReadingsService;
import com.panduit.sz.shared.SmartZoneNonTransientException;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import mockit.integration.junit4.JMockit;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

@RunWith(JMockit.class)
public class ReadingsServiceImplTest {

    /*
    ==============
    TESTS
    ==============
     */
    @Test
    public void test_getFloorPlanData_oneRackHasAllSensors() throws Exception{
        //setup
        new MockEsClient(new FakeEnvironment("SimpleFloorplan.json"));
        ReadingsService rs = new ReadingsServiceImpl("host");

        //execute
        RackReadings rack = getFirstRackReadings(rs);

        //validate
        assertValidSensor(rack.getCTopTemp());
        assertValidSensor(rack.getCMidTemp());
        assertValidSensor(rack.getCBotTemp());
        assertValidSensor(rack.getHTopTemp());
        assertValidSensor(rack.getHMidTemp());
        assertValidSensor(rack.getHBotTemp());
        assertValidSensor(rack.getRefTemp());
        assertValidSensor(rack.getRa());
        assertValidSensor(rack.getRh());
        assertValidSensor(rack.getCtROfChange());
    }

    @Test
    public void test_getFloorPlanData_floorPlanNotInSz() throws Exception {
        //setup
        new MockEsClient(new FakeEnvironment("NonSzFloorplan.json"));
        ReadingsService rs = new ReadingsServiceImpl("host");

        //execute
        List<FloorplanData> data = rs.getFloorplanData();

        //validate
        assert data.isEmpty();
    }

    @Test
    public void test_getFloorPlanData_rackNotInSz() throws Exception {
        //setup
        new MockEsClient(new FakeEnvironment("NonSzRack.json"));
        ReadingsService rs = new ReadingsServiceImpl("host");

        //execute
        List<FloorplanData> data = rs.getFloorplanData();
        FloorplanData floor = data.iterator().next();

        //validate
        assert floor.getRackReadings().isEmpty();
    }

    @Test
    public void test_getFloorPlanData_sensorError2000() throws Exception {
        //setup
        new MockEsClient(new FakeEnvironment("SensorError2000.json"));
        ReadingsService rs = new ReadingsServiceImpl("host");

        //execute
       RackReadings rack = getFirstRackReadings(rs);

        //validate
        assertEmptySensorData(rack.getCTopTemp());
    }

    @Test
    public void test_getFloorPlanData_sensorError3000() throws Exception {
        //setup
        new MockEsClient(new FakeEnvironment("SensorError3000.json"));
        ReadingsService rs = new ReadingsServiceImpl("host");

        //execute
        RackReadings rack = getFirstRackReadings(rs);

        //validate
        assertEmptySensorData(rack.getCTopTemp());
    }

    @Test
    public void test_getFloorPlanData_sensorError5000() throws Exception {
        //setup
        new MockEsClient(new FakeEnvironment("SensorError5000.json"));
        ReadingsService rs = new ReadingsServiceImpl("host");

        //execute
        RackReadings rack = getFirstRackReadings(rs);

        //validate
        assertEmptySensorData(rack.getCTopTemp());
    }

    @Test
    public void test_getFloorPlanData_raErrorCode() throws Exception {
        //setup
        new MockEsClient(new FakeEnvironment("raErrorCode.json"));
        ReadingsService rs = new ReadingsServiceImpl("host");

        //execute
        RackReadings rack = getFirstRackReadings(rs);

        //validate
        assertEmptySensorData(rack.getRa());
    }

    @Test
    public void test_getFloorPlanData_ctrROfChangeErrorCode() throws Exception {
        //setup
        new MockEsClient(new FakeEnvironment("ctROfChangeErrorCode.json"));
        ReadingsService rs = new ReadingsServiceImpl("host");

        //execute
        RackReadings rack = getFirstRackReadings(rs);

        //validate
        assertEmptySensorData(rack.getCtROfChange());
    }


    @Test
    public void test_getFloorPlanData_oneEmptySensorPtr() throws Exception {
        //setup
        new MockEsClient(new FakeEnvironment("EmptySensorPtr.json"));
        ReadingsService rs = new ReadingsServiceImpl("host");

        //execute
        RackReadings rack = getFirstRackReadings(rs);

        //validate
        assertEmptySensorData(rack.getCTopTemp());

    }

    @Test(expected = SmartZoneNonTransientException.class)
    public void test_getFloorPlanData_danglingSensorPtr() {
        //setup
        ReadingsService rs = null;
        try {
            new MockEsClient(new FakeEnvironment("DanglingSensorPtr.json"));
            rs = new ReadingsServiceImpl("host");
        } catch (Exception e) {
            assert false;
        }

        //execute & validate
        rs.getFloorplanData();
    }

    @Test
    public void test_getFloorPlanData_nullCalculatedValue() throws Exception {
        //setup
        new MockEsClient(new FakeEnvironment("NullCalculatedValue.json"));
        ReadingsService rs = new ReadingsServiceImpl("host");

        //execute
        RackReadings rack = getFirstRackReadings(rs);

        //validate
        assertEmptySensorData(rack.getCtROfChange());

    }

    /*
    =============
    UTILITIES
    =============
     */

    private RackReadings getFirstRackReadings(ReadingsService rs) {
        Collection<FloorplanData> data = rs.getFloorplanData();
        FloorplanData floor = data.iterator().next();
        return floor.getRackReadings().iterator().next();
    }

    private void assertValidSensor(Optional<Reading> sensor) {
        assertThat(sensor).hasValueSatisfying(reading -> {
            assertNotNull(reading.getValue());
            assertNotNull(reading.getInstant());});
    }

    private void assertEmptySensorData(Optional<Reading> reading) {
        assert !reading.isPresent();
    }

}