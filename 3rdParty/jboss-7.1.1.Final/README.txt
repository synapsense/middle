JBoss 7.1.1 patches are maintained on github in a synapsense branch:
    https://github.com/bcalmac/jboss-as/tree/synapsense

JBoss build procedure:
    1. Java 7 is required (updated JAVA_HOME and PATH)
    2. mvn -D skipTests install
    3. copy jboss-as-server and jboss-as-ejb3 jars to es/es-ear/JBoss/modules

JBoss 7.1.1 comes with HornetQ 2.2.13. The HornetQ patches are at:
    https://github.com/bcalmac/hornetq/tree/synapsense

HornetQ build procedure:
    1. Java 6 is required (updated JAVA_HOME and PATH)
    2. build.bat
    3. copy hornetq-jms jar to es/es-ear/JBoss/modules.
    3.1 add version to the name of the jar (the build does not take care of that)

See the commit log for details about the included patches.