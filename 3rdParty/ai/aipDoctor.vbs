'==========================================================================
'
' VBScript Source File
'
' NAME: 
'
' AUTHOR: Alexahder Kravin
' DATE  : 14.09.2009
'
' COMMENT: 
'
'==========================================================================
On Error Resume Next
Dim fso
Dim index

Const ForReading=1
Const ForWriting=2
index = 1

Set fso = CreateObject("Scripting.FileSystemObject")

Set args = WScript.Arguments
If args.Count > 0 Then
	If fso.FileExists(args(0)) Then
		processFile args(0)
		WScript.Echo args(0) & " file was cured. " & (index-1) & " infections were found."
	End If
Else
	WScript.Echo "Something goes wrong. Sorry :)"
	WScript.Quit 1
End If

Function GetPath(filePath)
	GetPath = Left(filePath, InStrRev(filePath, "\"))
End Function

Function GetFileName(filePath)
	GetFileName = Right(filePath, Len(filePath) - InStrRev(filePath, "\"))
End Function

Sub processFile(filePath)
	Dim fileForWrite
	Dim fileForRead

	path = GetPath(filePath)
	fileName = GetFileName(filePath)

	Set fileForRead=fso.OpenTextFile(filePath,ForReading)

	newPath = path & fileName & ".cured"
	If fso.FileExists(newPath) = True Then
		Set fileForWrite = fso.OpenTextFile(newPath,ForWriting)
	Else
		Set fileForWrite = fso.CreateTextFile(newPath,TRUE)
	End If

	expr1 = "Sequence=""[^""]*"
	Do While fileForRead.AtEndOfStream<>True
		strEntry=fileForRead.ReadLine
		
		If InStr(strEntry, "Sequence=""") <> 0 Then
			result = processString(strEntry, expr1, "Sequence=""" & index)
			fileForWrite.WriteLine result
			index = index + 1
		Else
			fileForWrite.WriteLine strEntry
		End If
	Loop

	fileForRead.Close
	fileForWrite.Close
End Sub

Function processString(txt, expr1, expr2)
	Dim oReg
	Set oReg = New RegExp
	oReg.Global = True
	oReg.IgnoreCase = False

	oReg.Pattern = expr1
	processString = oReg.Replace(txt, expr2)
End Function
