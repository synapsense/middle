/**
 * 
 */
package org.drools.util;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import org.drools.common.InternalFactHandle;
import org.drools.reteoo.ReteTuple;
import org.drools.reteoo.TupleMemory;

public class TupleHashTable extends AbstractHashTable
    implements
    TupleMemory,
    Externalizable {
    public TupleHashTable() {
        this( 16,
              0.75f );
    }

    public TupleHashTable(final int capacity,
                          final float loadFactor) {
        super( capacity,
               loadFactor );
    }

    public void readExternal(ObjectInput in) throws IOException,
                                            ClassNotFoundException {
        super.readExternal( in );
        // since tuples does no vary hashcode, this should not need
        // to rebuild on deserialization
        //        resize( table.length,
        //                true );
    }

    public void writeExternal(ObjectOutput out) throws IOException {
        super.writeExternal( out );
    }

    protected void updateHashCode(Entry entry) {
        // nothing to do
    }

    public Iterator iterator(final InternalFactHandle handle) {
        return iterator();
    }

    public void add(final ReteTuple tuple) {
    	
        final int hashCode = tuple.hashCode();
        int index = indexOf( hashCode,
                                   this.table.length );

        while(table[index] != null) {
        	index++;
        	if(index == this.table.length) {
        		resize( 2 * this.table.length,
                        false );
        		add(tuple);
        		return;
        	}        	
        }
        tuple.setNext( this.table[index] );
        this.table[index] = tuple;                
        
        if ( this.size++ >= this.threshold ) {
            resize( 2 * this.table.length,
                    false );
        }
    }

    public ReteTuple get(final ReteTuple tuple) {
        final int hashCode = tuple.hashCode();
        int index = indexOf( hashCode,
                                   this.table.length );
        for(; index < this.table.length; index ++) {
        	final ReteTuple current = (ReteTuple) this.table[index];
        	if(current == null) continue;
        	if ( hashCode == current.hashCode() && tuple.equals( current ) ) {
                return current;
            }
        }
        return null;
    }

    //*************************
    protected void resize(int newCapacity, boolean recalculateHashCode) {
        final Entry[] oldTable = this.table;
        final int oldCapacity = oldTable.length;
        if ( oldCapacity == AbstractHashTable.MAX_CAPACITY ) {
            this.threshold = Integer.MAX_VALUE;
            return;
        }

        final Entry[] newTable = new Entry[newCapacity];

        for ( int i = 0; i < this.table.length; i++ ) {
            Entry entry = this.table[i];
            if ( entry == null ) {
                continue;
            }
            this.table[i] = null;
            Entry next = null;
            while ( entry != null ) {
                next = entry.getNext();
                
                if( recalculateHashCode ) {
                    updateHashCode( entry );
                }
                int index = indexOf( entry.hashCode(),
                                           newTable.length );
                while(newTable[index] != null) {
                	index++;
                	if(index == newTable.length) {
                		resize(2 * newCapacity, recalculateHashCode);
                		return;
                	}
                }
                entry.setNext( newTable[index] );
                newTable[index] = entry;

                entry = next;
            }
        }

        this.table = newTable;
        this.threshold = (int) (newCapacity * this.loadFactor);

    }
    //*************************
    
    public ReteTuple remove(final ReteTuple tuple) {
        final int hashCode = tuple.hashCode();
        int index = indexOf( hashCode,
                                   this.table.length );
        
        for(; index < this.table.length; index++) {
        	final ReteTuple current = (ReteTuple) this.table[index];
        	if(current == null) continue;
        	if ( hashCode == current.hashCode() && tuple.equals( current ) ) {
        		this.table[index] = null;
        		this.size--;
        		return current;
        	}
        }
        return null;       
    }

    public Entry getBucket(final Object object) {
        final int hashCode = object.hashCode();
        int index = indexOf( hashCode,
                                   this.table.length );
        for(; index < this.table.length; index++) {
        	Entry current = this.table[index];
        	if(current == null) continue;
        	if ( hashCode == current.hashCode() && object.equals( current ) ) {
        		return current;
        	}
        }
        return null;
    }

    public boolean contains(final ReteTuple tuple) {
        return (get( tuple ) != null);
    }

    public boolean isIndexed() {
        return false;
    }
}