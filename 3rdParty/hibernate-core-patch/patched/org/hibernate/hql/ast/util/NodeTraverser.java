package org.hibernate.hql.ast.util;

import antlr.collections.AST;

/**
 * A visitor for traversing an AST tree.
 *
 * @author Steve Ebersole
 */
public class NodeTraverser {
	public static interface VisitationStrategy {
		public void visit(AST node);
	}

	private final VisitationStrategy strategy;

	public NodeTraverser(VisitationStrategy strategy) {
		this.strategy = strategy;
	}

	/**
	 * Traverse the AST tree depth first.
	 * <p/>
	 * Note that the AST passed in is not visited itself.  Visitation starts
	 * with its children.
	 *
	 * @param ast
	 */
	public void traverseDepthFirst(AST ast) {
		if (ast == null) {
			throw new IllegalArgumentException("node to traverse cannot be null!");
		}
		visitDepthChildren(ast);
	}

	private void visitDepthChildren(AST ast) {
		// if ( ast == null ) {
		// return;
		// }
		AST[] children = new AST[ast.getNumberOfChildren()];
		int counter = 0;
		{
			// filling children array
			AST child = ast.getFirstChild();
			while (child != null) {
				children[counter++] = child;
				child = child.getNextSibling();
			}
		}

		// check
		if (counter != children.length)
			throw new IllegalArgumentException("Number of children for " + ast
					+ " is not equal to actual number of them");

		for (int i = 0; i < children.length; i++) {
			AST child = children[i];
			strategy.visit(child);
			visitDepthChildren(child);
		}
		// visitDepthFirst( ast.getFirstChild() );
		// visitDepthFirst( ast.getNextSibling() );
	}

}
