package com.synapsense.liveimaging.web;

import java.net.URI;

import org.springframework.web.client.RestTemplate;

public class WebTest {

	public static void main(String[] args) throws Exception {
		ResourceServer rs = new ResourceServer(".", 8080);
		try {
			rs.startServer();

			RestTemplate restTemplate = new RestTemplate();
			byte[] re = restTemplate.getForObject(new URI("http://localhost:8080/generate/1"), byte[].class);

			System.out.println(re);
		} finally {
			rs.stop();
		}
	}
}
