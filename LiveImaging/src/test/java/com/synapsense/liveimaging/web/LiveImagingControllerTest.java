package com.synapsense.liveimaging.web;

import java.util.Date;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.web.servlet.HandlerAdapter;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.annotation.AnnotationMethodHandlerAdapter;

import com.synapsense.liveimaging.conf.LiConfigFactory;
import com.synapsense.liveimaging.es.EsConfigurationLoader;
import com.synapsense.liveimaging.job.HistoricalImageGenerator;
import com.synapsense.liveimaging.job.RealTimeImageGenerator;

public class LiveImagingControllerTest {

	private static RealTimeImageGenerator rtGenerator;
	private static ResourceServer rs;
	private static ApplicationContext ctx;
	private static HistoricalImageGenerator hsGenerator;
	private MockHttpServletRequest mockRequest;

	@BeforeClass
	public static void setUpClass() {
		try {
			LiConfigFactory.setConfigText(EsConfigurationLoader.readConfigFile());

			ctx = new ClassPathXmlApplicationContext("liveimaging-spring.xml");
			rtGenerator = ctx.getBean(RealTimeImageGenerator.class);
			hsGenerator = ctx.getBean(HistoricalImageGenerator.class);
			rs = ctx.getBean(ResourceServer.class);
			rs.startServer();
			rtGenerator.start();
			hsGenerator.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@AfterClass
	public static void tearDownClass() throws Exception {
		rs.stop();
		hsGenerator.stop();
		rtGenerator.stop();
	}

	@Before
	public void setUp() {

	}

	@After
	public void tearDown() {

	}

	//@Test
	public void testGenerateImages() throws Exception {
		new Date();
		mockRequest = new MockHttpServletRequest("POST", "/generateImages");
		mockRequest.setParameter("start", Long.toString(new Date().getTime() - 200000));
		mockRequest.setParameter("end", Long.toString(new Date().getTime() - 10000));
		LiveImagingController controller = ctx.getBean(LiveImagingController.class);
		HandlerAdapter handlerAdapter = new AnnotationMethodHandlerAdapter();
		ModelAndView result = handlerAdapter.handle(mockRequest, new MockHttpServletResponse(), controller);
		Thread.currentThread().wait(60000);
		System.out.println(result.toString());
	}

	// @Test
	// public void testSetLiveImaging() {
	// fail("Not yet implemented");
	// }
	//
	// @Test
	// public void testSetOutputDirectory() {
	// fail("Not yet implemented");
	// }
	//
	// @Test
	// public void testGetLatestImage() {
	// fail("Not yet implemented");
	// }
	//
	// @Test
	// public void testGetImageStringIntegerIntegerLong() {
	// fail("Not yet implemented");
	// }
	//
	// @Test
	// public void testGetImageStringIntegerIntegerLongLong() {
	// fail("Not yet implemented");
	// }
	//
	// @Test
	// public void testGetImages() {
	// fail("Not yet implemented");
	// }
	//
	// @Test
	// public void testGetMovie() {
	// fail("Not yet implemented");
	// }
	//
	// @Test
	// public void testUpdateLegend() {
	// fail("Not yet implemented");
	// }
	//
	// @Test
	// public void testGetCurrentLegendImage() {
	// fail("Not yet implemented");
	// }
	//
	// @Test
	// public void testGetLegends() {
	// fail("Not yet implemented");
	// }
	//
	// @Test
	// public void testIsServiceAvailable() {
	// fail("Not yet implemented");
	// }
	//
	// @Test
	// public void testHasImages() {
	// fail("Not yet implemented");
	// }

}
