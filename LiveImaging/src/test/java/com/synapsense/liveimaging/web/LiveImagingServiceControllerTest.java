package com.synapsense.liveimaging.web;

import com.synapsense.dto.TO;
import com.synapsense.liveimaging.web.smartzone.LiveImageType;
import com.synapsense.liveimaging.web.smartzone.LiveImagingServiceController;
import com.synapsense.service.Environment;
import org.hornetq.utils.json.JSONArray;
import org.hornetq.utils.json.JSONObject;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.web.servlet.HandlerAdapter;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.annotation.AnnotationMethodHandlerAdapter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class LiveImagingServiceControllerTest {

	private static String liBaseUrl = "http://win7-ci-test:9091";

	@BeforeClass
	public static void setUpClass() {
	}

	@AfterClass
	public static void tearDownClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() {
	}

    private JSONArray callLI(String urlText) throws Exception {
        URL url = new URL(urlText);
        URLConnection connection = url.openConnection();

        String decodedString;
        String jsonString = null;
        BufferedReader in = new BufferedReader(
                new InputStreamReader(
                        connection.getInputStream()));
        while ((decodedString = in.readLine()) != null) {
            jsonString = decodedString;
        }
        in.close();

        if (!jsonString.startsWith("[")) {
            jsonString = "[" + jsonString + "]";
        }

        JSONArray jsonArray = new JSONArray(jsonString);
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject json = jsonArray.getJSONObject(i);
            assertNotNull(json);
            assertNotNull(json.get("instant"));
            assertNotNull(json.get("url"));
            assertNotNull(json.get("legendUrl"));
            assertNotNull(json.get("legendUrlSi"));
        }

        return jsonArray;
    }

    // Commenting all these tests out because it requires ES and LI to be running on the
    // test machine, and is dependent on the ES objects that are in the database.

	 //@Test
	 public void testGetLatestImage() throws Exception {
         // http://localhost:9091/floorplans/{id}/images/latest?imageType=
         // TEMP_TOP | TEMP_MIDDLE | TEMP_BOTTOM | TEMP_SUBFLOOR | TEMP_CONTROLLED |
         // HUMIDITY | PRESSURE | DEWPOINT
		 Integer floorplanId = 17756;
         StringBuilder sb = new StringBuilder();
         sb.append(liBaseUrl);
         sb.append("/floorplans/");
         sb.append(floorplanId.toString());
         sb.append("/images/latest?imageType=");
         sb.append(LiveImageType.TEMP_TOP.toString());

         JSONArray ja = this.callLI(sb.toString());
         assertEquals(1, ja.length());
	 }

    //@Test
    public void testGetImage() throws Exception {
        // http://localhost:9091/floorplans/{id}/images?imageType={LiveImageType name}
        //                                             &date={milliseconds since epoch}
        Integer floorplanId = 17756;
        StringBuilder sb = new StringBuilder();
        sb.append(liBaseUrl);
        sb.append("/floorplans/");
        sb.append(floorplanId.toString());
        sb.append("/images?imageType=");
        sb.append(LiveImageType.TEMP_TOP.toString());
        sb.append("&date=");
        sb.append(new Date().getTime());

        JSONArray ja = this.callLI(sb.toString());
        assertEquals(1, ja.length());
    }

    //@Test
    public void testGetImages() throws Exception {
        // http://localhost:9091/floorplans/{id}/images?imageType={LiveImageType name}
        //                                             &start={milliseconds since epoch}
        //                                             &end={milliseconds since epoch}
        //                                             &maxImages={maximum number of images to return}
        Integer floorplanId = 17756;
        Date now = new Date();
        StringBuilder sb = new StringBuilder();
        sb.append(liBaseUrl);
        sb.append("/floorplans/");
        sb.append(floorplanId.toString());
        sb.append("/images?imageType=");
        sb.append(LiveImageType.TEMP_TOP.toString());
        sb.append("&start=");
        sb.append("0");
        sb.append("&end=");
        sb.append(now.getTime());
        sb.append("&maxImages=");
        sb.append("5");

        JSONArray ja = this.callLI(sb.toString());
        assertEquals(5, ja.length());
    }

    //@Test
    public void testGetMovie() throws Exception {
        // http://localhost:9091/floorplans/{id}/images/movie?imageType={LiveImageType name}
        //                                                   &start={milliseconds since epoch}
        //                                                   &end={milliseconds since epoch}
        //                                                   &maxImages={maximum number of images to return}
        //                                                   &unitSystem={NATIVE | SI}
        Integer floorplanId = 17756;
        Date now = new Date();
        StringBuilder sb = new StringBuilder();
        sb.append(liBaseUrl);
        sb.append("/floorplans/");
        sb.append(floorplanId.toString());
        sb.append("/images/movie?imageType=");
        sb.append(LiveImageType.TEMP_TOP.toString());
        sb.append("&start=");
        sb.append(0);
        sb.append("&end=");
        sb.append(now.getTime());
        sb.append("&maxImages=");
        sb.append("5");
        sb.append("&unitSystem=");
        sb.append("NATIVE");

        URL url = new URL(sb.toString());
        URLConnection connection = url.openConnection();

        InputStream is = connection.getInputStream();
        assertNotNull(is);
    }
}
