package com.synapsense.liveimaging.conf;

import java.io.*;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import junit.framework.Assert;

import org.junit.BeforeClass;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.synapsense.liveimaging.LiveImagingApp;
import com.synapsense.liveimaging.es.EsConfigurationLoader;


public class ConfigurationConverterTest {

	private static final String OLD_CONFIG = "src" + LiConstants.SEPARATOR
			+ "test" + LiConstants.SEPARATOR + "config" + LiConstants.SEPARATOR
			+ "old_conf.xml";
	private static String localConfig;
	private static String oldConfig;
	private static EsConfigurationLoader confLoader;
	
	@BeforeClass
	public static void setUpClass() {
		try {
			localConfig = EsConfigurationLoader.readConfigFile();
			StringBuilder sb = new StringBuilder();
			File confFile = new File(OLD_CONFIG);
			InputStream is = new FileInputStream(confFile);
			BufferedReader reader = new BufferedReader(new InputStreamReader(is));
			try {
				String strLine;
				while ((strLine = reader.readLine()) != null) {
					sb.append(strLine);
				}
			} finally {
				reader.close();
			}
			oldConfig = sb.toString();
			confLoader = new EsConfigurationLoader(new LiveImagingApp());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	//@Test
	public void test() {
		Assert.assertFalse(localConfig.equals(oldConfig));
		try {
			String updatedConf = confLoader.updateConfig(oldConfig);
			//String updatedConf = oldConfig;
			Document docUpdated = null, docLocal = null;

			try {
				docUpdated = DocumentBuilderFactory
						.newInstance()
						.newDocumentBuilder()
						.parse(new ByteArrayInputStream(updatedConf.getBytes()));

				docLocal = DocumentBuilderFactory
						.newInstance()
						.newDocumentBuilder()
						.parse(new ByteArrayInputStream(localConfig.getBytes()));
				
				NodeList lList = docLocal.getDocumentElement().getChildNodes();
				NodeList updList = docUpdated.getDocumentElement().getChildNodes();
				verifyNodes(lList, updList);
				verifyNodes(updList, lList);
			} catch (SAXException | ParserConfigurationException e) {
				e.printStackTrace();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void verifyNodes(NodeList base, NodeList whatCompare) {
		for (int i = 0; i < base.getLength(); ++i) {
			Node n = base.item(i);
			if(n.getNodeType() != Node.COMMENT_NODE && n.getNodeType() != Node.TEXT_NODE) {
				boolean found = false;
				for (int j = 0; j < whatCompare.getLength(); ++j) {
					if (whatCompare.item(j).getNodeType() != Node.COMMENT_NODE
							&& whatCompare.item(j).getNodeType() != Node.TEXT_NODE) {
						found |= whatCompare.item(j).isEqualNode(n);
						if (found)
							break;
					}
				}
				Assert.assertTrue("Not equals config for " + i + "  node " + n.getNodeName(), found);
			}
		}
	}
}
