package com.synapsense.liveimaging.job;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import junit.framework.Assert;

import org.jmock.Expectations;
import org.jmock.integration.junit4.JUnitRuleMockery;
import org.junit.Rule;
import org.junit.Test;

import com.synapsense.liveimaging.conf.LiConfig.RealtimeGenerator.KeepPeriod;
import com.synapsense.liveimaging.job.ImageRipper;
import com.synapsense.liveimaging.job.Util;
import com.synapsense.liveimaging.storage.ImageDataStorageReader;
import com.synapsense.liveimaging.storage.ImageDataStorageWriter;
import com.synapsense.liveimaging.storage.LiveImageVO;

public class ImageRipperTest {

	@Rule
	public final JUnitRuleMockery context = new JUnitRuleMockery();

	@SuppressWarnings("unchecked")
	//@Test
	public void imagesDeletionFirstTime() {

		final Date currDate = new Date();

		final ImageDataStorageReader reader = context.mock(ImageDataStorageReader.class);
		final ImageDataStorageWriter writer = context.mock(ImageDataStorageWriter.class);

		ArrayList<KeepPeriod> keepPeriods = new ArrayList<KeepPeriod>();
		KeepPeriod keepPeriod = new KeepPeriod();
		keepPeriod.setPeriod(30);
		keepPeriod.setRate(1);
		keepPeriods.add(keepPeriod);
		keepPeriod = new KeepPeriod();
		keepPeriod.setPeriod(60);
		keepPeriod.setRate(2);
		keepPeriods.add(keepPeriod);
		keepPeriod = new KeepPeriod();
		keepPeriod.setPeriod(120);
		keepPeriod.setRate(3);
		keepPeriods.add(keepPeriod);

		final List<LiveImageVO> secPeriodImages = createImages(Util.subtractDays(currDate, 60),
		        Util.subtractDays(currDate, 30));
		final List<LiveImageVO> thPeriodImages = createImages(Util.subtractDays(currDate, 120),
		        Util.subtractDays(currDate, 60));

		try {
			context.checking(new Expectations() {
				{
					oneOf(reader).getImages(Util.subtractDays(currDate, 60), Util.subtractDays(currDate, 30));
					will(returnValue(secPeriodImages));
					oneOf(reader).getImages(Util.subtractDays(currDate, 120), Util.subtractDays(currDate, 60));
					will(returnValue(thPeriodImages));
					exactly(2).of(writer).deleteImages(with(any(Collection.class)));
					oneOf(writer).deleteImages(Util.subtractDays(currDate, 120));
				}
			});
		} catch (Exception e) {
			Assert.fail(e.getMessage());
			e.printStackTrace();
		}

		File expireLog = new File("./output/expire.log");
		expireLog.delete();
		ImageRipper ripper = new ImageRipper(reader, writer, keepPeriods);
		ripper.ripImages(currDate);
		expireLog.delete();
	}

	@SuppressWarnings("unchecked")
	//@Test
	public void incrementalImageDelete() throws IOException {
		final Date currDate = new Date();

		final Date prevDate = Util.subtractDays(currDate, 1);

		final ImageDataStorageReader reader = context.mock(ImageDataStorageReader.class);
		final ImageDataStorageWriter writer = context.mock(ImageDataStorageWriter.class);

		ArrayList<KeepPeriod> keepPeriods = new ArrayList<KeepPeriod>();
		KeepPeriod keepPeriod = new KeepPeriod();
		keepPeriod.setPeriod(30);
		keepPeriod.setRate(1);
		keepPeriods.add(keepPeriod);
		keepPeriod = new KeepPeriod();
		keepPeriod.setPeriod(60);
		keepPeriod.setRate(2);
		keepPeriods.add(keepPeriod);
		keepPeriod = new KeepPeriod();
		keepPeriod.setPeriod(120);
		keepPeriod.setRate(3);
		keepPeriods.add(keepPeriod);

		final List<LiveImageVO> secPeriodImages = createImages(Util.subtractDays(currDate, 60),
		        Util.subtractDays(currDate, 30));
		final List<LiveImageVO> thPeriodImages = createImages(Util.subtractDays(currDate, 120),
		        Util.subtractDays(currDate, 60));

		try {
			context.checking(new Expectations() {
				{
					oneOf(reader).getImages(Util.subtractDays(currDate, 31), Util.subtractDays(currDate, 30));
					will(returnValue(secPeriodImages));
					oneOf(reader).getImages(Util.subtractDays(currDate, 61), Util.subtractDays(currDate, 60));
					will(returnValue(thPeriodImages));
					exactly(2).of(writer).deleteImages(with(any(Collection.class)));
					oneOf(writer).deleteImages(Util.subtractDays(currDate, 120));
				}
			});
		} catch (Exception e) {
			Assert.fail(e.getMessage());
			e.printStackTrace();
		}

		File expireLog = new File("./expire.log");
		if (!expireLog.exists()) {
			expireLog.createNewFile();
		}

		FileWriter exWriter = new FileWriter(expireLog);
		exWriter.write(String.valueOf(prevDate.getTime()));
		exWriter.close();

		ImageRipper ripper = new ImageRipper(reader, writer, keepPeriods);
		ripper.ripImages(currDate);

		expireLog.delete();
	}

	//@Test
	public void correctRippingOfPeriods() {
		final Date currDate = new Date();

		final ImageDataStorageReader reader = context.mock(ImageDataStorageReader.class);
		final ImageDataStorageWriter writer = context.mock(ImageDataStorageWriter.class);

		ArrayList<KeepPeriod> keepPeriods = new ArrayList<KeepPeriod>();
		KeepPeriod keepPeriod = new KeepPeriod();
		keepPeriod.setPeriod(1);
		keepPeriod.setRate(1);
		keepPeriods.add(keepPeriod);
		keepPeriod = new KeepPeriod();
		keepPeriod.setPeriod(2);
		keepPeriod.setRate(2);
		keepPeriods.add(keepPeriod);

		final List<LiveImageVO> secPeriodImages = createImages(Util.subtractDays(currDate, 2),
		        Util.subtractDays(currDate, 1));

		final List<LiveImageVO> secToDelete = createImages(Util.subtractDays(currDate, 2),
		        Util.subtractDays(currDate, 1), 2, ImageRipper.SAMPLE_INTERVAL);

		try {
			context.checking(new Expectations() {
				{
					oneOf(reader).getImages(Util.subtractDays(currDate, 2), Util.subtractDays(currDate, 1));
					will(returnValue(secPeriodImages));
					oneOf(writer).deleteImages(with(secToDelete));
					oneOf(writer).deleteImages(Util.subtractDays(currDate, 2));
				}
			});
		} catch (Exception e) {
			Assert.fail(e.getMessage());
			e.printStackTrace();
		}

		File expireLog = new File("./output/expire.log");
		expireLog.delete();
		ImageRipper ripper = new ImageRipper(reader, writer, keepPeriods);
		ripper.ripImages(currDate);
		expireLog.delete();
	}

	//@Test
	public void correctPeriodPickingOldExpireLog() throws Exception {
		final Date currDate = new Date();

		final ImageDataStorageReader reader = context.mock(ImageDataStorageReader.class);
		final ImageDataStorageWriter writer = context.mock(ImageDataStorageWriter.class);

		ArrayList<KeepPeriod> keepPeriods = new ArrayList<KeepPeriod>();
		KeepPeriod keepPeriod = new KeepPeriod();
		keepPeriod.setPeriod(30);
		keepPeriod.setRate(1);
		keepPeriods.add(keepPeriod);

		try {
			context.checking(new Expectations() {
				{
					oneOf(writer).deleteImages(Util.subtractDays(currDate, 30));
				}
			});
		} catch (Exception e) {
			Assert.fail(e.getMessage());
			e.printStackTrace();
		}

		// write very old expire timestamp
		File expireLog = new File("./expire.log");
		BufferedWriter expireWrtier = new BufferedWriter(new FileWriter(expireLog));
		expireWrtier.write(String.valueOf(Util.subtractDays(currDate, 120).getTime()));
		expireWrtier.close();

		ImageRipper ripper = new ImageRipper(reader, writer, keepPeriods);
		ripper.ripImages(currDate);
		expireLog.delete();
	}

	private List<LiveImageVO> createImages(Date startDate, Date endDate) {
		return createImages(startDate, endDate, 1, 0);
	}

	private List<LiveImageVO> createImages(Date startDate, Date endDate, int rate, long offset) {
		LinkedList<LiveImageVO> images = new LinkedList<LiveImageVO>();
		Date imageDate = new Date(startDate.getTime() + offset);
		while (imageDate.before(endDate)) {
			images.add(new LiveImageVO("image1-910-200-128", null, imageDate, "png"));
			imageDate = new Date(imageDate.getTime() + rate * ImageRipper.SAMPLE_INTERVAL);
		}
		return images;
	}
}