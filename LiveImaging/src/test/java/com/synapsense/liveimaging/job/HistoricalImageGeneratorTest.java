package com.synapsense.liveimaging.job;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import org.jmock.Expectations;
import org.jmock.Mockery;
import org.jmock.lib.legacy.ClassImposteriser;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.exception.PropertyNotFoundException;
import com.synapsense.exception.UnableToConvertPropertyException;
import com.synapsense.liveimaging.conf.LiConfigFactory;
import com.synapsense.liveimaging.es.ESContext;
import com.synapsense.liveimaging.es.EsConfigurationLoader;
import com.synapsense.liveimaging.web.ImageType;
import com.synapsense.service.Environment;

public class HistoricalImageGeneratorTest {
	private static Mockery context = new Mockery() {
		{
			setImposteriser(ClassImposteriser.INSTANCE);
		}
	};

	private static ClassPathXmlApplicationContext ctx;
	private static HistoricalImageGenerator hsGenerator;
	private static ESContext esContext = context.mock(ESContext.class);
	private static Environment environment;

	@BeforeClass
	public static void setUpClass() throws IOException {

		// environment = new HistoricalEnvironmentMock();
		try {
			LiConfigFactory.setConfigText(EsConfigurationLoader.readConfigFile());

			ctx = new ClassPathXmlApplicationContext("liveimaging-spring.xml");
			hsGenerator = ctx.getBean(HistoricalImageGenerator.class);
			// replace ESContext with mock
			// EsDataSource dataSource = ctx.getBean(EsDataSource.class);
			// dataSource.setEsContext(esContext);
			hsGenerator.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@AfterClass
	public static void tearDownClass() throws Exception {
		hsGenerator.stop();
	}

	//@Test
	public void test() throws ObjectNotFoundException, PropertyNotFoundException, UnableToConvertPropertyException {
		context.checking(new Expectations() {
			{
				// allowing(esContext).testConnect();
				// will(returnValue(true));

				allowing(esContext).getEnv();
				will(returnValue(environment));
			}
		});
		// HistoricalData.csv contains data from Feb 25 22:15:30 to Feb 26
		// 13:05:31
		SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd");

		try {
			Date date1 = ft.parse("2013-02-25");
			System.out.println(date1);
			Date date2 = ft.parse("2013-02-26");
			System.out.println(date2);
			Collection<TO<?>> objIds = new ArrayList<TO<?>>();
			objIds.add(TOFactory.getInstance().loadTO("DC:710"));
			Collection<ImageType> imageTypes = new ArrayList<ImageType>();
			imageTypes.add(new ImageType(200, 131072));
			//hsGenerator.addTask(objIds, imageTypes, 300000l, ft.parse("2013-02-25"), ft.parse("2013-02-26"));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		context.assertIsSatisfied();
	}
}
