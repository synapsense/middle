package com.synapsense.liveimaging.test;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.imageio.ImageIO;

import com.synapsense.liveimaging.data.ColorMap;
import com.synapsense.liveimaging.data.ColorPoint;
import com.synapsense.liveimaging.draw.Area;
import com.synapsense.liveimaging.draw.DrawLiveImageException;
import com.synapsense.liveimaging.draw.LiveImageBuilder;
import com.synapsense.liveimaging.draw.PolygonArea;
import com.synapsense.liveimaging.draw.RectangleArea;
import com.synapsense.liveimaging.draw.ValuePoint;
import com.synapsense.liveimaging.interpolation.engine.LiInterpolationEngine;
import com.synapsense.liveimaging.interpolation.methods.BiLinear;

public class ArbitraryPolygonTest {

	public static void main(String[] args) throws DrawLiveImageException, IOException {

		List<ColorPoint> map = new ArrayList<ColorPoint>();
		map.add(new ColorPoint(50, 0, 0, 255));
		map.add(new ColorPoint(57, 0, 150, 255));
		map.add(new ColorPoint(64.5, 0, 255, 100));
		map.add(new ColorPoint(66.5, 0, 255, 50));
		map.add(new ColorPoint(72.5, 0, 255, 0));
		map.add(new ColorPoint(76.5, 50, 255, 0));
		map.add(new ColorPoint(80.6, 100, 255, 0));
		map.add(new ColorPoint(90, 230, 210, 0));
		map.add(new ColorPoint(100, 230, 80, 0));
		map.add(new ColorPoint(110, 255, 25, 0));
		map.add(new ColorPoint(120, 255, 0, 0));

		// LiInterpolationEngine.setGpuCheck(false);
		// LiInterpolationEngine.addCpuInterpolator(100, CachedOrdKriging.class,
		// new HashMap<String, Integer>());
		Map<String, Integer> props = new HashMap<String, Integer>();
		props.put("gridSize", 40);
		LiInterpolationEngine engine = new LiInterpolationEngine(null);
		engine.addCpuInterpolator(1000, BiLinear.class, props);

		Area area = new RectangleArea(0, 0, 900, 700);
		PolygonArea polygon = new PolygonArea(area);
		float[] xp = new float[] { 100.0f, 200.0f, 400.0f, 500.0f, 0.0f };
		float[] yp = new float[] { 100.0f, 300.0f, 100.0f, 600.0f, 500.0f };
		for (int i = 0; i < xp.length; i++) {
			polygon.addAngle(xp[i], yp[i]);
		}
		area.nest(polygon);

		PolygonArea polygon2 = new PolygonArea(polygon);
		polygon2.addAngle(200, 400);
		polygon2.addAngle(300, 400);
		polygon2.addAngle(300, 500);

		polygon.nest(polygon2);

		area.addSampleValue(new ValuePoint(205, 385, 40));
		area.addSampleValue(new ValuePoint(250, 430, 90));
		area.addSampleValue(new ValuePoint(295, 475, 120));
		
		
		area.addSampleValue(new ValuePoint(405, 345, 50));
		area.addSampleValue(new ValuePoint(350, 430, 90));
		area.addSampleValue(new ValuePoint(495, 465, 120));

		
		area.addSampleValue(new ValuePoint(455, 115, 50));
		area.addSampleValue(new ValuePoint(750, 30, 90));
		area.addSampleValue(new ValuePoint(125, 665, 120));
		
		area.addSampleValue(new ValuePoint(426, 515, 50));
		area.addSampleValue(new ValuePoint(750, 530, 90));
		area.addSampleValue(new ValuePoint(825, 665, 40));
		/*Random rand = new Random();
		for (int i = 0; i < 120; i++) {
			double val = 50 + rand.nextInt(70);

			int x = 0;
			int y = 0;
			//do {
			x = rand.nextInt(900);
			y = rand.nextInt(700);
			//} while (polygon.inArea(x, y));
			area.addSampleValue(new ValuePoint(x, y, val));
		}*/

		LiveImageBuilder builder = new LiveImageBuilder();
		builder.setColorMap(new ColorMap("test", map));
		builder.setInterpolationFactory(engine.getInterpolationFactory());
		builder.setBackGroundImage(new BufferedImage(901, 701, BufferedImage.TYPE_INT_ARGB));
		long start = System.currentTimeMillis();
		area.draw(builder);
		long end = System.currentTimeMillis();
		System.out.println("Interpolated within " + (end - start) + " millis." );
		ImageIO.write(builder.getImage(), "png", new File("./image1.png"));
	}
}
