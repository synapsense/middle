package com.synapsense.liveimaging.test;

import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.liveimaging.data.DataSource;
import com.synapsense.liveimaging.data.DataSourceException;
import com.synapsense.liveimaging.data.PayLoad;
import com.synapsense.liveimaging.data.PayLoadCollection;
import com.synapsense.liveimaging.web.ImageType;

import java.util.Collection;
import java.util.Date;

/**
 * Created by synapsoft on 2/11/2016.
 */
public class SmartZoneTestEsDataSource implements DataSource {
    @Override
    public void load(PayLoadCollection payloads, Date showTime) throws DataSourceException {
    }

    @Override
    public void load(PayLoadCollection payloads,
                     Date startTime,
                     Date endTime,
                     Collection<TO<?>> objIds,
                     Collection<ImageType> imageTypes,
                     long interval) throws DataSourceException {

    }

    @Override
    public PayLoad load(TO<?> loadTO, Integer dataclass, Integer layer) throws DataSourceException {
        return null;
    }

    @Override
    public PayLoad loadLite(TO<?> loadTO, Integer dataclass, Integer layer)
            throws DataSourceException {
        return null;
    }

    @Override
    public TO<?> getESObjectForSmartzoneId(String floorplan) {
        return TOFactory.getInstance().loadTO("DC", Integer.valueOf(floorplan));
    }
}
