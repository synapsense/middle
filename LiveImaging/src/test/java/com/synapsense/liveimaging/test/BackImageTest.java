package com.synapsense.liveimaging.test;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;

import com.synapsense.dto.BinaryData;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.liveimaging.conf.LiConstants;
import com.synapsense.liveimaging.data.ColorPoint;
import com.synapsense.liveimaging.draw.ImageUtil;
import com.synapsense.liveimaging.es.ESContext;
import com.synapsense.liveimaging.interpolation.engine.LiInterpolationEngine;
import com.synapsense.liveimaging.interpolation.methods.CachedBiLinear;
import com.synapsense.liveimaging.interpolation.methods.OrdKriging;
import com.synapsense.service.Environment;

public class BackImageTest {

	public static void main(String[] args) throws Exception {
		Environment env = new ESContext().getEnv();

		List<ColorPoint> map = new ArrayList<ColorPoint>();
		map.add(new ColorPoint(50, 0, 0, 255));
		map.add(new ColorPoint(57, 0, 150, 255));
		map.add(new ColorPoint(64.5, 0, 255, 100));
		map.add(new ColorPoint(66.5, 0, 255, 50));
		map.add(new ColorPoint(72.5, 0, 255, 0));
		map.add(new ColorPoint(76.5, 50, 255, 0));
		map.add(new ColorPoint(80.6, 100, 255, 0));
		map.add(new ColorPoint(90, 230, 210, 0));
		map.add(new ColorPoint(100, 230, 80, 0));
		map.add(new ColorPoint(110, 255, 25, 0));
		map.add(new ColorPoint(120, 255, 0, 0));

		// LiInterpolationEngine.setGpuCheck(false);
		LiInterpolationEngine engine = new LiInterpolationEngine(null);
		engine.addCpuInterpolator(100, OrdKriging.class, new HashMap<String, Integer>());
		Map<String, Integer> props = new HashMap<String, Integer>();
		props.put("gridSize", 40);
		engine.addCpuInterpolator(1000, CachedBiLinear.class, props);

		Collection<TO<?>> rooms = env.getObjectsByType(LiConstants.ROOM);
		for (TO<?> room : rooms) {
			BinaryData bin = env.getPropertyValue(room, "map", BinaryData.class);

			BufferedImage img = ImageIO.read(new ByteArrayInputStream(bin.getValue()));
			ImageIO.write(img, "png", new File("./output/" + TOFactory.getInstance().saveTO(room).replaceAll(":", "-")
			        + ".png"));

			long start = System.currentTimeMillis();
			BufferedImage copy = ImageUtil.copyImage(img);
			System.out.println("Image copy time: " + (System.currentTimeMillis() - start));

			ImageIO.write(copy, "png", new File("./output/" + TOFactory.getInstance().saveTO(room).replaceAll(":", "-")
			        + "-copy.png"));
		}

	}
}
