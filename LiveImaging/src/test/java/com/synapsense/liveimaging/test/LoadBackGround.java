package com.synapsense.liveimaging.test;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.util.Collection;
import java.util.Properties;

import javax.imageio.ImageIO;
import javax.naming.Context;
import javax.naming.InitialContext;

import com.synapsense.dto.BinaryData;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.liveimaging.conf.LiConstants;
import com.synapsense.service.Environment;
import com.synapsense.util.JNDINames;
import com.synapsense.util.ServerLoginModule;

public class LoadBackGround {

	public static void main(String[] args) throws Exception {

		Environment env = getEnv();
		Collection<TO<?>> rooms = env.getObjectsByType(LiConstants.ROOM);
		for (TO<?> room : rooms) {
			BinaryData bin = env.getPropertyValue(room, "map", BinaryData.class);

			BufferedImage img = ImageIO.read(new ByteArrayInputStream(bin.getValue()));
			ImageIO.write(img, "png", new File("./" + TOFactory.getInstance().saveTO(room) + ".png"));
		}
	}

	private static Environment getEnv() throws Exception {
		java.util.Properties environment = new Properties();
		environment.put(Context.INITIAL_CONTEXT_FACTORY, "org.jnp.interfaces.NamingContextFactory");
		environment.put(Context.URL_PKG_PREFIXES, "org.jboss.naming:org.jnp.interfaces");
		environment.put(Context.PROVIDER_URL, "jnp://192.168.200.80:1099");
		InitialContext context = new InitialContext(environment);
		Environment env = ServerLoginModule.getProxy(context, JNDINames.ENVIRONMENT, Environment.class);
		ServerLoginModule.login("admin", "admin");
		return env;
	}
}
