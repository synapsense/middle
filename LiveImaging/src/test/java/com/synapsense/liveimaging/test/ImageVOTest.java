package com.synapsense.liveimaging.test;

import com.synapsense.liveimaging.storage.ImageVO;
import junit.framework.Assert;
import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;

public class ImageVOTest {

	@Test
	public void loadImageVofromString() throws ParseException {
		ImageVO li = ImageVO.load("image-6-200-256-2011-04-03-00_03_00.jpg");
		Assert.assertNotNull("Li not null", li);
		Assert.assertEquals("Date",
							Instant.ofEpochMilli(new SimpleDateFormat("yyyy-MM-dd-HH_mm_ss").parse("2011-04-03-00_03_00").getTime()),
							li.getInstant());
	}

	@Test(expected=IllegalArgumentException.class)
	public void loadImageVofromStringPng() throws ParseException {
		ImageVO li = ImageVO.load("image-----2011-05-03-00_03_00.png");
		Assert.assertNotNull("Li not null", li);
        Assert.assertEquals("Date",
                            Instant.ofEpochMilli(new SimpleDateFormat("yyyy-MM-dd-HH_mm_ss").parse("2011-05-03-00_03_00").getTime()),
                            li.getInstant());
	}

	@Test
	public void loadStringWithIncorrectFormat() {
		ImageVO li = ImageVO.load("image-6-200-256.2011-04-03-00_03_00.gif");
		Assert.assertNull("Li null", li);
	}

	@Test
	public void loadStringWithDotInsteadOfdash() {
		ImageVO li = ImageVO.load("image-6-200-256.2011-04-03-00_03_00.png");
		Assert.assertNull("Li null", li);
	}
}
