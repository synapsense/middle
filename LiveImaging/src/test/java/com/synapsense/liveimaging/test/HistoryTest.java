package com.synapsense.liveimaging.test;

import java.util.Collection;
import java.util.Date;

import org.junit.Test;

import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.dto.ValueTO;
import com.synapsense.liveimaging.es.ESContext;
import com.synapsense.service.Environment;

public class HistoryTest {
	//@Test
	public void main() throws Exception {
		ESContext esContext = new ESContext();
		Environment env = esContext.getEnv();
		HistoricalEnvironmentMock mock = new HistoricalEnvironmentMock();
		mock.getHistory(null, null, null, null, null);
		Collection<TO<?>> sensors = env.getObjectsByType("WSNSENSOR");

		System.out.println("Sensors size: " + sensors.size());

		long start = System.currentTimeMillis();
		Collection<ValueTO> hist = env.getHistory(TOFactory.getInstance().loadTO("WSNSENSOR:192"), "lastValue",
		        Double.class);
		System.out.println(new Date(hist.iterator().next().getTimeStamp()).toString());
		long minDate = Long.MAX_VALUE, maxDate = Long.MIN_VALUE;
		for (ValueTO valueTO : hist) {
			long valueToTimestamp = valueTO.getTimeStamp();
			if (minDate > valueToTimestamp) {
				minDate = valueToTimestamp;
			}
			if (maxDate < valueToTimestamp) {
				maxDate = valueToTimestamp;
			}
		}
		System.out.println("Retrieved " + hist.size() + " historical values " + (System.currentTimeMillis() - start));
		System.out.println("Min Date:[" + new Date(minDate).toString() + "], Max Date: [" + new Date(maxDate) + "]");
		esContext.disconnect();
	}
}
