package com.synapsense.liveimaging.test;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import junit.framework.Assert;

import org.junit.Test;

import com.synapsense.liveimaging.storage.LiveImageVO;
import com.synapsense.liveimaging.storage.LiveImageVO.ImageType;

public class LiveImageVOTest {

	@Test
	public void loadLiveImageVofromString() throws ParseException {
		LiveImageVO li = LiveImageVO.load("image-6-200-256-2011-04-03-00_03_00.jpg");
		Assert.assertNotNull("Li not null", li);
		Assert.assertEquals("Id", "image-6-200-256", li.getImageId());
		Assert.assertEquals("Date", new SimpleDateFormat("yyyy-MM-dd-HH_mm_ss").parse("2011-04-03-00_03_00"),
		        li.getShowTime());
		Assert.assertEquals("Type", ImageType.JPG, li.getImageType());
	}

	@Test(expected=IllegalArgumentException.class)
	public void loadLiveImageVofromStringPng() throws ParseException {
		LiveImageVO li = LiveImageVO.load("image-----2011-05-03-00_03_00.png");
		Assert.assertNotNull("Li not null", li);
		Assert.assertEquals("Id", "image----", li.getImageId());
		Assert.assertEquals("Date", new SimpleDateFormat("yyyy-MM-dd-HH_mm_ss").parse("2011-05-03-00_03_00"),
		        li.getShowTime());
		Assert.assertEquals("Type", ImageType.PNG, li.getImageType());
	}

	@Test
	public void loadStringWithIncorrectFormat() {
		LiveImageVO li = LiveImageVO.load("image-6-200-256.2011-04-03-00_03_00.gif");
		Assert.assertNull("Li null", li);
	}

	@Test
	public void loadStringWithDotInsteadOfdash() {
		LiveImageVO li = LiveImageVO.load("image-6-200-256.2011-04-03-00_03_00.png");
		Assert.assertNull("Li null", li);
	}
}
