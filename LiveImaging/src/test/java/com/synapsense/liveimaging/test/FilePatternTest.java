package com.synapsense.liveimaging.test;

import static org.junit.Assert.*;

import java.util.regex.Pattern;

import org.junit.Test;

public class FilePatternTest {
	private static Pattern resourcePattern = Pattern
	        .compile("^.+\\-\\d{4}\\-\\d{2}\\-\\d{2}\\-\\d{2}_\\d{2}_\\d{2}\\.(png|jpg)$");
	@Test
	public void test1() {
		assertTrue(resourcePattern.matcher("image-145-200-128-2013-02-25-21_15_00.png").matches());
	}
	@Test
	public void test2() {
		assertTrue(resourcePattern.matcher("hist-image-145-200-128-2013-02-25-21_15_00.png").matches());
	}

}
