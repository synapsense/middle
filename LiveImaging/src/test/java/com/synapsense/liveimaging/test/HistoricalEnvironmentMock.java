package com.synapsense.liveimaging.test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.exception.PropertyNotFoundException;
import com.synapsense.exception.UnableToConvertPropertyException;
import com.synapsense.liveimaging.AbstractEnvironmentMock;
import com.synapsense.liveimaging.conf.ConfigurationException;
import com.synapsense.liveimaging.es.EsConfigurationLoader;

public class HistoricalEnvironmentMock extends AbstractEnvironmentMock {
	private Map<TO<?>, List<ValueTO>> historicalValues = new HashMap<>();

	public HistoricalEnvironmentMock() throws IOException {
		InputStream is = EsConfigurationLoader.class.getClassLoader().getResourceAsStream("HistoricalData.csv");
		if (is == null) {
			throw new ConfigurationException("Data file [HistoricalData.csv] is not found in the classpath.");
		}
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		try {
			String strLine;
			TOFactory factory = TOFactory.getInstance();
			// Data is ordered by objectId, propertyName, timestamp
			int previousObjectId = -1;
			while ((strLine = reader.readLine()) != null) {
				String[] row = strLine.split(";");
				int objectId = Integer.parseInt(row[0]);
				TO<?> objectTO = factory.loadTO("WSNSENSOR", objectId);
				List<ValueTO> valueTOs;

				if (objectId != previousObjectId) {
					valueTOs = new ArrayList<>();
					historicalValues.put(objectTO, valueTOs);
					previousObjectId = objectId;
				} else {
					valueTOs = historicalValues.get(objectTO);
				}
				valueTOs.add(new ValueTO(row[1], Double.parseDouble(row[3]), Long.parseLong(row[2]) * 1000));
			}
		} finally {
			reader.close();
		}
	}

	@Override
	public <RETURN_TYPE> Collection<ValueTO> getHistory(TO<?> objId, String propName, Date start, Date end,
	        Class<RETURN_TYPE> clazz) throws ObjectNotFoundException, PropertyNotFoundException,
	        UnableToConvertPropertyException {
		List<ValueTO> valueTOs = historicalValues.get(TOFactory.getInstance().loadTO("WSNSENSOR:178"));
		for (ValueTO value : valueTOs) {
			System.out.println(new Date(value.getTimeStamp()).toString());
		}
		return null;
	}

}
