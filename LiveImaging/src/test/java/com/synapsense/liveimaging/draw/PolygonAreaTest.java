package com.synapsense.liveimaging.draw;

import org.junit.Assert;
import org.junit.Test;

import com.synapsense.liveimaging.draw.PolygonArea;
import com.synapsense.liveimaging.draw.RectangleArea;

public class PolygonAreaTest {

	@Test
	public void pointInArea() {
		PolygonArea area = new PolygonArea();
		area.addAngle(0, 0);
		area.addAngle(1, 3);
		area.addAngle(4, 4);

		Assert.assertTrue(area.inArea(0, 0));
		Assert.assertTrue(area.inArea(1, 2));
		Assert.assertTrue(area.inArea(2, 2));
		Assert.assertTrue(area.inArea(1, 2));
		Assert.assertFalse(area.inArea(10, 2));
	}

	@Test
	public void edgePointInArea() {
		PolygonArea poly = new PolygonArea();
		poly.addAngle(1, 5);
		poly.addAngle(1, 7);
		poly.addAngle(2, 7);
		poly.addAngle(2, 5);

		Assert.assertTrue(poly.inArea(1, 5));
		Assert.assertTrue(poly.inArea(2, 6));
		Assert.assertTrue(poly.inArea(1, 6));
	}

	@Test
	public void polyInPoly() {
		PolygonArea poly1 = new PolygonArea();
		poly1.addAngle(1, 1);
		poly1.addAngle(1, 2);
		poly1.addAngle(5, 6);
		poly1.addAngle(10, 10);

		PolygonArea poly2 = new PolygonArea();
		poly2.addAngle(1, 1);
		poly2.addAngle(1, 3);
		poly2.addAngle(1, 11);
		poly2.addAngle(15, 15);
		poly2.addAngle(15, 0);

		Assert.assertTrue(poly1.inscribed(poly2));
		Assert.assertFalse(poly2.inscribed(poly1));
	}

	@Test
	public void polyIntersects() {
		PolygonArea poly1 = new PolygonArea();
		poly1.addAngle(1, 1);
		poly1.addAngle(1, 2);
		poly1.addAngle(5, 6);
		poly1.addAngle(10, 10);

		PolygonArea poly2 = new PolygonArea();
		poly2.addAngle(1, 1);
		poly2.addAngle(2, 3);
		poly2.addAngle(1, 11);
		poly2.addAngle(15, 15);
		poly2.addAngle(15, 0);

		Assert.assertFalse(poly1.inscribed(poly2));
		Assert.assertFalse(poly2.inscribed(poly1));

	}

	@Test
	public void polyDontIntersects() {
		PolygonArea poly1 = new PolygonArea();
		poly1.addAngle(1, 1);
		poly1.addAngle(1, 2);
		poly1.addAngle(5, 6);
		poly1.addAngle(10, 10);

		PolygonArea poly2 = new PolygonArea();
		poly2.addAngle(11, 12);
		poly2.addAngle(15, 20);
		poly2.addAngle(23, 30);

		Assert.assertFalse(poly1.inscribed(poly2));
		Assert.assertFalse(poly2.inscribed(poly1));
	}

	@Test
	public void PolyInscribedInItself() {
		PolygonArea poly1 = new PolygonArea();
		poly1.addAngle(1, 1);
		poly1.addAngle(1, 2);
		poly1.addAngle(5, 6);
		poly1.addAngle(10, 10);

		Assert.assertTrue(poly1.inscribed(poly1));
	}

	@Test
	public void polygonInRectangle() {
		PolygonArea poly = new PolygonArea();
		poly.addAngle(1, 5);
		poly.addAngle(1, 7);
		poly.addAngle(2, 7);

		RectangleArea rect = new RectangleArea(0, 0, 8, 9);

		Assert.assertTrue(poly.inscribed(rect));
		Assert.assertFalse(rect.inscribed(poly));
	}

	@Test
	public void polyIntersectsRectangle() {
		PolygonArea poly1 = new PolygonArea();
		poly1.addAngle(1, 1);
		poly1.addAngle(1, 2);
		poly1.addAngle(5, 6);
		poly1.addAngle(10, 10);

		RectangleArea rect = new RectangleArea(3, 4, 11, 11);
		Assert.assertFalse(poly1.inscribed(rect));
		Assert.assertFalse(rect.inscribed(poly1));
	}

	@Test
	public void polyDontRectangle() {
		PolygonArea poly1 = new PolygonArea();
		poly1.addAngle(1, 1);
		poly1.addAngle(1, 2);
		poly1.addAngle(5, 6);
		poly1.addAngle(10, 10);

		RectangleArea rect = new RectangleArea(11, 11, 20, 100);
		Assert.assertFalse(poly1.inscribed(rect));
		Assert.assertFalse(rect.inscribed(poly1));
	}

	@Test
	public void calculateArea() {
		PolygonArea poly = new PolygonArea();
		poly.addAngle(1, 1);
		poly.addAngle(1, 3);
		poly.addAngle(4, 3);
		poly.addAngle(4, 1);

		Assert.assertEquals(2 * 3, poly.calcArea(), 0.001);
	}

	@Test
	public void calculateTriangleArea() {
		PolygonArea poly = new PolygonArea();
		poly.addAngle(1, 1);
		poly.addAngle(4, 3);
		poly.addAngle(4, 1);

		Assert.assertEquals(3, poly.calcArea(), 0.001);
	}
}
