package com.synapsense.liveimaging.draw;

import java.util.Collection;

import org.junit.Assert;
import org.junit.Test;

import com.synapsense.liveimaging.draw.Area;
import com.synapsense.liveimaging.draw.AreaProcessor;
import com.synapsense.liveimaging.draw.PolygonArea;
import com.synapsense.liveimaging.draw.RectangleArea;

public class AreaManagerTest {

	@Test
	public void oneAreaForest() {
		AreaProcessor m = new AreaProcessor();
		RectangleArea area = new RectangleArea(0, 0, 20, 30);
		m.addArea(area);

		Collection<Area> forest = m.getAreasForest();
		Assert.assertEquals(1, forest.size());
	}

	@Test
	public void oneInscribedArea() {
		AreaProcessor m = new AreaProcessor();
		RectangleArea area1 = new RectangleArea(0, 0, 20, 30);
		RectangleArea area2 = new RectangleArea(1, 1, 10, 10);
		m.addArea(area1);
		m.addArea(area2);

		Collection<Area> forest = m.getAreasForest();
		Assert.assertEquals(1, forest.size());
		Assert.assertTrue(area1 == forest.iterator().next());
	}

	@Test
	public void twoIntersectedAreas() {
		AreaProcessor m = new AreaProcessor();
		RectangleArea area1 = new RectangleArea(0, 0, 20, 30);
		PolygonArea area2 = new PolygonArea();
		area2.addAngle(0, 0);
		area2.addAngle(10, 10);
		area2.addAngle(40, 0);

		m.addArea(area1);
		m.addArea(area2);
		Collection<Area> forest = m.getAreasForest();
		Assert.assertEquals(2, forest.size());
	}

	@Test
	public void twoIntersectedWithChildren() {
		AreaProcessor m = new AreaProcessor();
		RectangleArea area1 = new RectangleArea(0, 0, 20, 30);
		PolygonArea area2 = new PolygonArea();
		area2.addAngle(0, 0);
		area2.addAngle(10, 10);
		area2.addAngle(40, 0);
		m.addArea(area1);
		m.addArea(area2);

		m.addArea(new RectangleArea(1, 1, 20, 10));
		m.addArea(new RectangleArea(25, 0, 30, 1));
		Collection<Area> forest = m.getAreasForest();
		Assert.assertEquals(2, forest.size());
	}

	@Test
	public void nest3Levels() {
		AreaProcessor m = new AreaProcessor();
		RectangleArea area1 = new RectangleArea(0, 0, 50, 50);
		RectangleArea area2 = new RectangleArea(10, 10, 40, 30);
		PolygonArea area3 = new PolygonArea();
		area3.addAngle(10, 15);
		area3.addAngle(15, 20);
		area3.addAngle(30, 15);

		m.addArea(area1);
		m.addArea(area2);
		m.addArea(area3);

		Collection<Area> forest = m.getAreasForest();
		Assert.assertEquals(1, forest.size());
		Assert.assertTrue(area1 == forest.iterator().next());
	}
}
