package com.synapsense.liveimaging.draw;

import org.junit.Assert;
import org.junit.Test;

import com.synapsense.liveimaging.draw.Segment;

public class SegmentTest {

	@Test
	public void segmentsIntersect() {
		Segment seg1 = new Segment(0, 0, 10, 20);
		Segment seg2 = new Segment(10, 10, 5, 20);

		Assert.assertTrue(seg1.intersects(seg2));
		Assert.assertTrue(seg2.intersects(seg1));
	}

	@Test
	public void segmentsDontIntersect() {
		Segment seg1 = new Segment(20, 10, 50, 50);
		Segment seg2 = new Segment(0, 0, 1, 1);

		Assert.assertFalse(seg1.intersects(seg2));
		Assert.assertFalse(seg2.intersects(seg1));
	}

	@Test
	public void segmentsParallel() {
		Segment seg1 = new Segment(1.5f, 2.5f, 46, 15);
		Segment seg2 = new Segment(3, 5, 92, 30);

		Assert.assertFalse(seg1.intersects(seg2));
		Assert.assertFalse(seg2.intersects(seg1));
	}

	@Test
	public void segmentsOverlayed() {
		Segment seg1 = new Segment(10, 10, 20, 20);
		Segment seg2 = new Segment(10, 10, 40, 40);

		Assert.assertFalse(seg1.intersects(seg2));
		Assert.assertFalse(seg2.intersects(seg1));
	}

	@Test
	public void segmentsTouch() {
		Segment seg1 = new Segment(2, 1, 4, 3);
		Segment seg2 = new Segment(4, 3, 3, 0);

		Assert.assertFalse(seg1.intersects(seg2));
		Assert.assertFalse(seg2.intersects(seg1));
	}

	@Test
	public void pointBelongs() {
		Segment s = new Segment(3, 4, 1, 2);

		Assert.assertTrue(s.contains(1, 2));
		Assert.assertTrue(s.contains(3, 4));
		Assert.assertTrue(s.contains(2, 3));
	}

	@Test
	public void pointDoesntBelong() {
		Segment s = new Segment(1, 2, 3, 4);

		Assert.assertFalse(s.contains(0, 0));
	}

	@Test
	public void doesntBelongOnLine() {
		Segment s = new Segment(1, 1, 1, 2);

		Assert.assertFalse(s.contains(1, 8));
	}

	@Test
	public void calcLength() {
		Segment s = new Segment(1, 10, 1, 2);

		Assert.assertEquals(8.0, s.getLength(), 0.001);
	}
}
