package com.synapsense.liveimaging.draw;

import org.junit.Test;

import junit.framework.Assert;

import com.synapsense.liveimaging.draw.PolygonArea;
import com.synapsense.liveimaging.draw.RectangleArea;

public class RectangleAreaTest {

	@Test
	public void rectangleAreasInscribed() {
		RectangleArea rect1 = new RectangleArea(0, 0, 150, 150);
		RectangleArea rect2 = new RectangleArea(1, 15, 30, 100);

		Assert.assertTrue(rect2.inscribed(rect1));
		Assert.assertFalse(rect1.inscribed(rect2));
	}

	@Test
	public void rectangleAreasInscribedTouched() {
		RectangleArea rect1 = new RectangleArea(0, 0, 150, 150);
		RectangleArea rect2 = new RectangleArea(1, 0, 30, 150);

		Assert.assertTrue(rect2.inscribed(rect1));
		Assert.assertFalse(rect1.inscribed(rect2));
	}

	@Test
	public void rectangleAreasIntersect() {
		RectangleArea rect1 = new RectangleArea(0, 0, 150, 150);
		RectangleArea rect2 = new RectangleArea(30, 40, 200, 1000);

		Assert.assertFalse(rect2.inscribed(rect1));
		Assert.assertFalse(rect1.inscribed(rect2));
	}

	@Test
	public void rectangleAreasDontIntersect() {
		RectangleArea rect1 = new RectangleArea(0, 0, 150, 150);
		RectangleArea rect2 = new RectangleArea(300, 20, 500, 1000);

		Assert.assertFalse(rect2.inscribed(rect1));
		Assert.assertFalse(rect1.inscribed(rect2));
	}

	@Test
	public void rectInscribedItself() {
		RectangleArea rect = new RectangleArea(0, 0, 150, 150);

		Assert.assertTrue(rect.inscribed(rect));
	}

	@Test
	public void pointInArea() {
		RectangleArea rect = new RectangleArea(0, 0, 150, 150);

		Assert.assertTrue(rect.inArea(150, 20));
		Assert.assertTrue(rect.inArea(100, 100));
		Assert.assertTrue(rect.inArea(0, 150));
		Assert.assertFalse(rect.inArea(1000, 150));
	}

	@Test
	public void rectangleInPolygon() {
		RectangleArea rect = new RectangleArea(1, 5, 4, 6);
		PolygonArea poly = new PolygonArea();
		poly.addAngle(1, 5);
		poly.addAngle(1, 7);
		poly.addAngle(10, 20);
		poly.addAngle(7, 2);

		Assert.assertTrue(poly.inArea(1, 5));
		Assert.assertTrue(rect.inscribed(poly));
	}

	@Test
	public void calculateRectArea() {
		RectangleArea rect = new RectangleArea(0, 0, 10, 20);

		Assert.assertEquals(200, rect.calcArea(), 0.001);
	}
}
