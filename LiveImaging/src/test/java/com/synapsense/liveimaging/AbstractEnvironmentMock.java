package com.synapsense.liveimaging;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.synapsense.dto.CollectionTO;
import com.synapsense.dto.EnvObjectBundle;
import com.synapsense.dto.EnvObjectHistory;
import com.synapsense.dto.ObjectType;
import com.synapsense.dto.ObjectTypeMatchMode;
import com.synapsense.dto.PropertyDescr;
import com.synapsense.dto.Relation;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOBundle;
import com.synapsense.dto.Tag;
import com.synapsense.dto.TagDescriptor;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.EnvException;
import com.synapsense.exception.IllegalInputParameterException;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.exception.PropertyNotFoundException;
import com.synapsense.exception.ServerConfigurationException;
import com.synapsense.exception.TagNotFoundException;
import com.synapsense.exception.UnableToConvertPropertyException;
import com.synapsense.exception.UnableToConvertTagException;
import com.synapsense.exception.UnsupportedRelationException;
import com.synapsense.search.QueryDescription;
import com.synapsense.service.Environment;

public abstract class AbstractEnvironmentMock implements Environment {

	@Override
	public ObjectType createObjectType(String name) throws EnvException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ObjectType[] getObjectTypes(String[] names) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ObjectType getObjectType(String name) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String[] getObjectTypes() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void updateObjectType(ObjectType objType) throws EnvException {
		// TODO Auto-generated method stub

	}

	@Override
	public void deleteObjectType(String typeName) throws EnvException {
		// TODO Auto-generated method stub

	}

	@Override
	public TO<?> createObject(String typeName) throws EnvException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TO<?> createObject(String typeName, ValueTO[] propertyValues) throws EnvException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TOBundle createObjects(EnvObjectBundle objectsToCreate) throws EnvException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Collection<TO<?>> getObjectsByType(String typeName) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Collection<TO<?>> getObjectsByType(String typeName, ObjectTypeMatchMode matchMode) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Collection<TO<?>> getObjects(String typeName, ValueTO[] propertyValues) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Collection<TO<?>> getObjects(QueryDescription queryDescr) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Collection<TO<?>> getObjects(String typeName, ObjectTypeMatchMode matchMode, ValueTO[] propertyValues) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deleteObject(TO<?> objId) throws ObjectNotFoundException {
		// TODO Auto-generated method stub

	}

	@Override
	public void deleteObject(TO<?> objId, int level) throws ObjectNotFoundException {
		// TODO Auto-generated method stub

	}

	@Override
	public Collection<ValueTO> getAllPropertiesValues(TO<?> objId) throws ObjectNotFoundException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setAllPropertiesValues(TO<?> objId, ValueTO[] values) throws ObjectNotFoundException,
	        PropertyNotFoundException, UnableToConvertPropertyException {
		// TODO Auto-generated method stub

	}

	@Override
	public void setAllPropertiesValues(TO<?> objId, Collection<ValueTO> values, Collection<Tag> tags)
	        throws EnvException {
		// TODO Auto-generated method stub

	}

	@Override
	public <RETURN_TYPE> RETURN_TYPE getPropertyValue(TO<?> objId, String propName, Class<RETURN_TYPE> clazz)
	        throws ObjectNotFoundException, PropertyNotFoundException, UnableToConvertPropertyException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <REQUESTED_TYPE> void setPropertyValue(TO<?> objId, String propName, REQUESTED_TYPE value)
	        throws ObjectNotFoundException, PropertyNotFoundException, UnableToConvertPropertyException {
		// TODO Auto-generated method stub

	}

	@Override
	public <REQUESTED_TYPE> void addPropertyValue(TO<?> objId, String propName, REQUESTED_TYPE value)
	        throws ObjectNotFoundException, PropertyNotFoundException, UnableToConvertPropertyException {
		// TODO Auto-generated method stub

	}

	@Override
	public <REQUESTED_TYPE> void removePropertyValue(TO<?> objId, String propName, REQUESTED_TYPE value)
	        throws ObjectNotFoundException, PropertyNotFoundException, UnableToConvertPropertyException {
		// TODO Auto-generated method stub

	}

	@Override
	public <RETURN_TYPE> Collection<ValueTO> getHistory(TO<?> objId, String propName, Class<RETURN_TYPE> clazz)
	        throws ObjectNotFoundException, UnableToConvertPropertyException, PropertyNotFoundException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <RETURN_TYPE> Collection<ValueTO> getHistory(TO<?> objId, String propName, int size, int start,
	        Class<RETURN_TYPE> clazz) throws ObjectNotFoundException, PropertyNotFoundException,
	        UnableToConvertPropertyException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <RETURN_TYPE> Collection<ValueTO> getHistory(TO<?> objId, String propName, Date start, Date end,
	        Class<RETURN_TYPE> clazz) throws ObjectNotFoundException, PropertyNotFoundException,
	        UnableToConvertPropertyException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public EnvObjectHistory getHistory(TO<?> objId, Collection<String> propNames, Date start, Date end)
	        throws ObjectNotFoundException, PropertyNotFoundException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <RETURN_TYPE> Collection<ValueTO> getHistory(TO<?> objId, String propName, Date start, int numberPoints,
	        Class<RETURN_TYPE> clazz) throws ObjectNotFoundException, PropertyNotFoundException,
	        UnableToConvertPropertyException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer getHistorySize(TO<?> objId, String propName) throws ObjectNotFoundException,
	        PropertyNotFoundException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setRelation(TO<?> parentId, TO<?> childId) throws ObjectNotFoundException, UnsupportedRelationException {
		// TODO Auto-generated method stub

	}

	@Override
	public void setRelation(List<Relation> relations) throws ObjectNotFoundException, UnsupportedRelationException {
		// TODO Auto-generated method stub

	}

	@Override
	public void removeRelation(TO<?> parentId, TO<?> childId) throws ObjectNotFoundException {
		// TODO Auto-generated method stub

	}

	@Override
	public void removeRelation(List<Relation> relations) throws ObjectNotFoundException {
		// TODO Auto-generated method stub

	}

	@Override
	public Collection<TO<?>> getParents(TO<?> objId, String typeName) throws ObjectNotFoundException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Collection<TO<?>> getParents(TO<?> objId) throws ObjectNotFoundException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Collection<TO<?>> getChildren(TO<?> objId) throws ObjectNotFoundException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Collection<TO<?>> getChildren(TO<?> objId, String typeName) throws ObjectNotFoundException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Collection<CollectionTO> getChildren(Collection<TO<?>> objIds, String typeName) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Collection<CollectionTO> getChildren(Collection<TO<?>> objIds) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getPropertyTimestamp(TO<?> objId, String propName) throws ObjectNotFoundException,
	        PropertyNotFoundException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public PropertyDescr getPropertyDescriptor(String typeName, String propName) throws ObjectNotFoundException,
	        PropertyNotFoundException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Collection<CollectionTO> getPropertyValue(Collection<TO<?>> objIds, String[] propNames) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Collection<CollectionTO> getAllPropertiesValues(Collection<TO<?>> objIds) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Collection<TO<?>> getRelatedObjects(TO<?> objId, String objectType, boolean isChild) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <RETURN_TYPE> RETURN_TYPE getPropertyValue(String typeName, String propName, Class<RETURN_TYPE> clazz)
	        throws ObjectNotFoundException, PropertyNotFoundException, UnableToConvertPropertyException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <REQUESTED_TYPE> void setPropertyValue(String typeName, String propName, REQUESTED_TYPE value)
	        throws ObjectNotFoundException, PropertyNotFoundException, UnableToConvertPropertyException {
		// TODO Auto-generated method stub

	}

	@Override
	public void configurationComplete() throws ServerConfigurationException {
		// TODO Auto-generated method stub

	}

	@Override
	public void configurationStart() throws ServerConfigurationException {
		// TODO Auto-generated method stub

	}

	@Override
	public void setAllPropertiesValues(TO<?> objId, ValueTO[] values, boolean persist) throws ObjectNotFoundException,
	        PropertyNotFoundException, UnableToConvertPropertyException {
		// TODO Auto-generated method stub

	}

	@Override
	public <REQUESTED_TYPE> void setPropertyValue(TO<?> objId, String propName, REQUESTED_TYPE value, boolean persist)
	        throws PropertyNotFoundException, ObjectNotFoundException, UnableToConvertPropertyException {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean exists(TO<?> objId) throws IllegalInputParameterException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public TagDescriptor getTagDescriptor(String typeName, String propName, String tagName)
	        throws ObjectNotFoundException, PropertyNotFoundException, TagNotFoundException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <RETURN_TYPE> RETURN_TYPE getTagValue(TO<?> objId, String propName, String tagName, Class<RETURN_TYPE> clazz)
	        throws ObjectNotFoundException, PropertyNotFoundException, TagNotFoundException,
	        UnableToConvertTagException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Collection<Tag> getAllTags(TO<?> objId, String propName) throws ObjectNotFoundException,
	        PropertyNotFoundException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Collection<Tag> getAllTags(TO<?> objId) throws ObjectNotFoundException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map<TO<?>, Collection<Tag>> getAllTags(String typeName) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map<TO<?>, Map<String, Collection<Tag>>> getTags(Collection<TO<?>> objIds, List<String> propertyNames,
	        List<String> tagNames) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setTagValue(TO<?> objId, String propName, String tagName, Object value)
	        throws PropertyNotFoundException, ObjectNotFoundException, TagNotFoundException,
	        UnableToConvertTagException {
		// TODO Auto-generated method stub

	}

	@Override
	public void setAllTagsValues(TO<?> objId, Collection<Tag> tagsToSet) throws PropertyNotFoundException,
	        ObjectNotFoundException, TagNotFoundException, UnableToConvertTagException {
		// TODO Auto-generated method stub

	}

}
