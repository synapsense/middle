package com.synapsense.liveimaging.interpolation.engine;

import java.util.HashMap;

import org.junit.Assert;
import org.junit.Test;

import com.synapsense.liveimaging.draw.EValuePoint;
import com.synapsense.liveimaging.draw.RectangleArea;
import com.synapsense.liveimaging.interpolation.Interpolator;
import com.synapsense.liveimaging.interpolation.engine.CachedInterpolationFactory;
import com.synapsense.liveimaging.interpolation.engine.LiInterpolationEngine;
import com.synapsense.liveimaging.interpolation.methods.CachedOrdKriging;

public class CacheInterpolationFactoryTest {

	@Test
	public void getInterpolatorForTheSameArea() {
		LiInterpolationEngine engine = new LiInterpolationEngine(null);
	
		engine.addCpuInterpolator(10000, CachedOrdKriging.class, new HashMap<String, Integer>());
		CachedInterpolationFactory factory = engine.getCachedInterpolationFactory();
		RectangleArea area = new RectangleArea(0.0f, 100.0f, 100.0f, 300.0f);
		EValuePoint p = new EValuePoint(0.0f, 22f, 50f);
		area.addSampleValue(p);
		Interpolator interp = factory.getInterpolator(area);
		p.setValue(100);
		area.addSampleValue(p);
		Assert.assertTrue(interp == factory.getInterpolator(area));
	}
	public static void main(String[] args) {
		new CacheInterpolationFactoryTest().getInterpolatorForTheSameArea();
	}
}
