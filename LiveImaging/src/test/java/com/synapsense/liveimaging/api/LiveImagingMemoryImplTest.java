package com.synapsense.liveimaging.api;

import com.synapsense.dto.TOFactory;
import com.synapsense.liveimaging.conf.LiConstants;
import com.synapsense.liveimaging.legend.ColorManager;
import com.synapsense.liveimaging.storage.LegendImageVO;
import com.synapsense.liveimaging.storage.LiveImageVO;
import com.synapsense.liveimaging.storage.MemoryDataStorage;
import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Collection;
import java.util.Date;

public class LiveImagingMemoryImplTest {

	private MemoryDataStorage storage;

	private LiveImaging api;

	@Before
	public void inti() {
		ColorManager colorMan = null;

		storage = new MemoryDataStorage("./testDir");

		LegendImageVO legend1 = new LegendImageVO("legends" + LiConstants.SEPARATOR + "legend1", 200, new Date(1),
		        null, null);
		LegendImageVO legend2 = new LegendImageVO("legends" + LiConstants.SEPARATOR + "legend2", 201, new Date(1),
		        null, null);
		LegendImageVO legend3 = new LegendImageVO("legends" + LiConstants.SEPARATOR + "legend3", 202, new Date(1),
		        null, null);
		LegendImageVO legend4 = new LegendImageVO("legends" + LiConstants.SEPARATOR + "legend4", 998, new Date(1),
		        null, null);

		LegendImageVO legend5 = new LegendImageVO("legends" + LiConstants.SEPARATOR + "legend5", 200, new Date(10),
		        null, null);
		LegendImageVO legend6 = new LegendImageVO("legends" + LiConstants.SEPARATOR + "legend6", 202, new Date(11),
		        null, null);

		storage.saveLegend(legend1);
		storage.saveLegend(legend2);
		storage.saveLegend(legend3);
		storage.saveLegend(legend4);
		storage.saveLegend(legend5);
		storage.saveLegend(legend6);

		LiveImageVO i1 = new LiveImageVO(LiConstants.DC + LiConstants.SEPARATOR + "6" + LiConstants.SEPARATOR + "256"
		        + LiConstants.SEPARATOR + "image-6-200-256", null, new Date(1), "PNG");
		LiveImageVO i2 = new LiveImageVO(LiConstants.DC + LiConstants.SEPARATOR + "6" + LiConstants.SEPARATOR + "256"
		        + LiConstants.SEPARATOR + "image-6-200-256", null, new Date(2), "PNG");
		LiveImageVO i3 = new LiveImageVO(LiConstants.DC + LiConstants.SEPARATOR + "6" + LiConstants.SEPARATOR + "256"
		        + LiConstants.SEPARATOR + "image-6-200-256", null, new Date(3), "PNG");
		LiveImageVO i4 = new LiveImageVO(LiConstants.DC + LiConstants.SEPARATOR + "6" + LiConstants.SEPARATOR + "256"
		        + LiConstants.SEPARATOR + "image-6-200-256", null, new Date(4), "PNG");

		LiveImageVO i5 = new LiveImageVO(LiConstants.DC + LiConstants.SEPARATOR + "6" + LiConstants.SEPARATOR + "128"
		        + LiConstants.SEPARATOR + "image-6-200-128", null, new Date(5), "PNG");
		LiveImageVO i6 = new LiveImageVO(LiConstants.DC + LiConstants.SEPARATOR + "6" + LiConstants.SEPARATOR + "128"
		        + LiConstants.SEPARATOR + "image-6-200-128", null, new Date(6), "PNG");
		LiveImageVO i7 = new LiveImageVO(LiConstants.DC + LiConstants.SEPARATOR + "6" + LiConstants.SEPARATOR + "256"
		        + LiConstants.SEPARATOR + "image-6-202-256", null, new Date(7), "PNG");
		LiveImageVO i8 = new LiveImageVO(LiConstants.DC + LiConstants.SEPARATOR + "6" + LiConstants.SEPARATOR + "256"
		        + LiConstants.SEPARATOR + "image-6-202-256", null, new Date(8), "PNG");

		LiveImageVO i9 = new LiveImageVO(LiConstants.ROOM + LiConstants.SEPARATOR + "7" + LiConstants.SEPARATOR + "256"
		        + LiConstants.SEPARATOR + "image-7-200-256", null, new Date(9), "PNG");
		LiveImageVO i10 = new LiveImageVO(LiConstants.ROOM + LiConstants.SEPARATOR + "7" + LiConstants.SEPARATOR + "256"
		        + LiConstants.SEPARATOR + "image-7-200-256", null, new Date(10), "PNG");
		LiveImageVO i11 = new LiveImageVO(LiConstants.ROOM + LiConstants.SEPARATOR + "7" + LiConstants.SEPARATOR + "128"
		        + LiConstants.SEPARATOR + "image-7-200-128", null, new Date(11), "PNG");
		LiveImageVO i12 = new LiveImageVO(LiConstants.ROOM + LiConstants.SEPARATOR + "7" + LiConstants.SEPARATOR + "128"
		        + LiConstants.SEPARATOR + "image-7-200-128", null, new Date(12), "PNG");

		LiveImageVO i13 = new LiveImageVO(LiConstants.ROOM + LiConstants.SEPARATOR + "8" + LiConstants.SEPARATOR + "256"
		        + LiConstants.SEPARATOR + "image-8-200-256", null, new Date(13), "PNG");
		LiveImageVO i14 = new LiveImageVO(LiConstants.ROOM + LiConstants.SEPARATOR + "8" + LiConstants.SEPARATOR + "256"
		        + LiConstants.SEPARATOR + "image-8-998-256", null, new Date(14), "PNG");
		LiveImageVO i15 = new LiveImageVO(LiConstants.ROOM + LiConstants.SEPARATOR + "8" + LiConstants.SEPARATOR + "128"
		        + LiConstants.SEPARATOR + "image-8-200-128", null, new Date(15), "PNG");
		LiveImageVO i16 = new LiveImageVO(LiConstants.ROOM + LiConstants.SEPARATOR + "8" + LiConstants.SEPARATOR + "128"
		        + LiConstants.SEPARATOR + "image-8-200-128", null, new Date(16), "PNG");

		storage.saveLiveImage(i1);
		storage.saveLiveImage(i2);
		storage.saveLiveImage(i3);
		storage.saveLiveImage(i4);
		storage.saveLiveImage(i5);
		storage.saveLiveImage(i6);
		storage.saveLiveImage(i7);
		storage.saveLiveImage(i8);
		storage.saveLiveImage(i9);
		storage.saveLiveImage(i10);
		storage.saveLiveImage(i11);
		storage.saveLiveImage(i12);
		storage.saveLiveImage(i13);
		storage.saveLiveImage(i14);
		storage.saveLiveImage(i15);
		storage.saveLiveImage(i16);

		api = new LiveImagingMemoryImpl(storage, colorMan, null, null);
	}

	@Test
	public void getLatestImageTest() {
		LiveImageVO image = api.getLatestImage(TOFactory.getInstance().loadTO(LiConstants.DC, 6), 200, 256);
		Assert.assertEquals(new Date(4), image.getShowTime());

		LiveImageVO notExistedImage = api.getLatestImage(TOFactory.getInstance().loadTO(LiConstants.DC, 234), 200, 1);
		Assert.assertNull(notExistedImage);
	}

	@Test
	public void getImageTest() {
		LiveImageVO image = api.getImage(TOFactory.getInstance().loadTO(LiConstants.ROOM, 8), 200, 128, new Date(15));
		Assert.assertEquals(new Date(15), image.getShowTime());

		image = api.getImage(TOFactory.getInstance().loadTO(LiConstants.ROOM, 8), 200, 128, new Date(89));
		Assert.assertEquals(new Date(16), image.getShowTime());
	}

	@Test
	public void getImageByDates() {
		LiveImageVO image = api.getImage(TOFactory.getInstance().loadTO(LiConstants.DC, 6), 200, 256, new Date(1), new Date(20));
		Assert.assertEquals(new Date(4), image.getShowTime());

		image = api.getImage(TOFactory.getInstance().loadTO(LiConstants.DC, 6), 200, 256, new Date(1), new Date(3));
		Assert.assertEquals(new Date(3), image.getShowTime());

		image = api.getImage(TOFactory.getInstance().loadTO(LiConstants.DC, 6), 200, 256, new Date(100), new Date(300));
		Assert.assertNull(image);

		image = api.getImage(TOFactory.getInstance().loadTO(LiConstants.DC, 634), 200, 256, new Date(100), new Date(300));
		Assert.assertNull(image);
	}

	@Test
	public void getImagesTest() {
		Collection<LiveImageVO> images = api.getImages(TOFactory.getInstance().loadTO(LiConstants.DC, 6), 200, 256, new Date(1),
		        new Date(20), 120);
		Assert.assertEquals(4, images.size());

		images = api.getImages(TOFactory.getInstance().loadTO(LiConstants.DC, 6), 200, 256, new Date(1), new Date(3), 120);
		Assert.assertEquals(3, images.size());

		images = api.getImages(TOFactory.getInstance().loadTO(LiConstants.DC, 6), 200, 256, new Date(100), new Date(300), 120);
		Assert.assertEquals(0, images.size());

		images = api.getImages(TOFactory.getInstance().loadTO(LiConstants.DC, 66), 200, 256, new Date(1), new Date(3), 120);
		Assert.assertEquals(0, images.size());
	}

	@Test
	public void getCurrentLegendImageTest() {
		LegendImageVO legend = api.getCurrentLegendImage(202);
		Assert.assertEquals(new Date(11), legend.getShowTime());

		legend = api.getCurrentLegendImage(998);
		Assert.assertEquals(new Date(1), legend.getShowTime());

		legend = api.getCurrentLegendImage(9983);
		Assert.assertNull(legend);
	}

	@Test
	public void hasImagesTest() {
		Assert.assertTrue(api.hasImages(TOFactory.getInstance().loadTO(LiConstants.ROOM, 7)));
		Assert.assertFalse(api.hasImages(TOFactory.getInstance().loadTO(LiConstants.ROOM, 78)));
	}

}
