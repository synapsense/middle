package com.synapsense.liveimaging.api;

import com.synapsense.liveimaging.api.smartzone.LiveImagingService;
import com.synapsense.liveimaging.api.smartzone.LiveImagingServiceImpl;
import com.synapsense.liveimaging.conf.LiConstants;
import com.synapsense.liveimaging.data.DataSource;
import com.synapsense.liveimaging.storage.LegendImageVO;
import com.synapsense.liveimaging.storage.LiveImageVO;
import com.synapsense.liveimaging.storage.MemoryDataStorage;
import com.synapsense.liveimaging.test.SmartZoneTestEsDataSource;
import com.synapsense.liveimaging.web.smartzone.LiveImageType;
import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;
import org.threeten.extra.Interval;

import java.io.IOException;
import java.time.Instant;
import java.util.Collection;
import java.util.Date;

public class LiveImagingServiceImplTest {

    private LiveImagingService szApi;

    @Before
	public void inti() {
        MemoryDataStorage storage = new MemoryDataStorage("./testDir");

		LegendImageVO legend1 = new LegendImageVO("legends" + LiConstants.SEPARATOR + "legend1", 200, new Date(1),
		        null, null);
		LegendImageVO legend2 = new LegendImageVO("legends" + LiConstants.SEPARATOR + "legend2", 201, new Date(1),
		        null, null);
		LegendImageVO legend3 = new LegendImageVO("legends" + LiConstants.SEPARATOR + "legend3", 202, new Date(1),
		        null, null);
		LegendImageVO legend4 = new LegendImageVO("legends" + LiConstants.SEPARATOR + "legend4", 998, new Date(1),
		        null, null);

		LegendImageVO legend5 = new LegendImageVO("legends" + LiConstants.SEPARATOR + "legend5", 200, new Date(10),
		        null, null);
		LegendImageVO legend6 = new LegendImageVO("legends" + LiConstants.SEPARATOR + "legend6", 202, new Date(11),
		        null, null);

		storage.saveLegend(legend1);
		storage.saveLegend(legend2);
		storage.saveLegend(legend3);
		storage.saveLegend(legend4);
		storage.saveLegend(legend5);
		storage.saveLegend(legend6);

		LiveImageVO i1 = new LiveImageVO(LiConstants.DC + LiConstants.SEPARATOR + "6" + LiConstants.SEPARATOR + "256"
		        + LiConstants.SEPARATOR + "image-6-200-256", null, new Date(1), "PNG");
		LiveImageVO i2 = new LiveImageVO(LiConstants.DC + LiConstants.SEPARATOR + "6" + LiConstants.SEPARATOR + "256"
		        + LiConstants.SEPARATOR + "image-6-200-256", null, new Date(2), "PNG");
		LiveImageVO i3 = new LiveImageVO(LiConstants.DC + LiConstants.SEPARATOR + "6" + LiConstants.SEPARATOR + "256"
		        + LiConstants.SEPARATOR + "image-6-200-256", null, new Date(3), "PNG");
		LiveImageVO i4 = new LiveImageVO(LiConstants.DC + LiConstants.SEPARATOR + "6" + LiConstants.SEPARATOR + "256"
		        + LiConstants.SEPARATOR + "image-6-200-256", null, new Date(4), "PNG");

		LiveImageVO i5 = new LiveImageVO(LiConstants.DC + LiConstants.SEPARATOR + "6" + LiConstants.SEPARATOR + "128"
		        + LiConstants.SEPARATOR + "image-6-200-128", null, new Date(5), "PNG");
		LiveImageVO i6 = new LiveImageVO(LiConstants.DC + LiConstants.SEPARATOR + "6" + LiConstants.SEPARATOR + "128"
		        + LiConstants.SEPARATOR + "image-6-200-128", null, new Date(6), "PNG");
		LiveImageVO i7 = new LiveImageVO(LiConstants.DC + LiConstants.SEPARATOR + "6" + LiConstants.SEPARATOR + "256"
		        + LiConstants.SEPARATOR + "image-6-202-256", null, new Date(7), "PNG");
		LiveImageVO i8 = new LiveImageVO(LiConstants.DC + LiConstants.SEPARATOR + "6" + LiConstants.SEPARATOR + "256"
		        + LiConstants.SEPARATOR + "image-6-202-256", null, new Date(8), "PNG");

		storage.saveLiveImage(i1);
		storage.saveLiveImage(i2);
		storage.saveLiveImage(i3);
		storage.saveLiveImage(i4);
		storage.saveLiveImage(i5);
		storage.saveLiveImage(i6);
		storage.saveLiveImage(i7);
		storage.saveLiveImage(i8);

        DataSource dataSource = new SmartZoneTestEsDataSource();
		szApi = new LiveImagingServiceImpl(storage);
        ((LiveImagingServiceImpl)szApi).setEsDataSource(dataSource);
	}

    @Test
	public void getLatestImageTest() throws IOException {
		LiveImageVO image = szApi.getLatestImage("6", LiveImageType.TEMP_SUBFLOOR);
        Assert.assertNotNull(image);
        Assert.assertEquals(Instant.ofEpochMilli((new Date(4)).getTime()), Instant.ofEpochMilli(image.getShowTime().getTime()));

        LiveImageVO notExistedImage = szApi.getLatestImage("234", LiveImageType.TEMP_SUBFLOOR);
		Assert.assertNull(notExistedImage);
	}

	@Test
	public void getImageTest() throws IOException {
        Instant desiredInstant = Instant.ofEpochMilli((new Date(7)).getTime());
        LiveImageVO image = szApi.getImage("6", LiveImageType.PRESSURE, desiredInstant);
        Assert.assertNotNull(image);
        Assert.assertEquals(desiredInstant, Instant.ofEpochMilli(image.getShowTime().getTime()));

        desiredInstant = Instant.ofEpochMilli((new Date(89)).getTime());
		image = szApi.getImage("6", LiveImageType.PRESSURE, desiredInstant);
        Assert.assertEquals(Instant.ofEpochMilli((new Date(8)).getTime()), Instant.ofEpochMilli(image.getShowTime().getTime()));
	}

	@Test
	public void getImagesTest() throws IOException {
        Instant start = Instant.ofEpochMilli((new Date(1)).getTime());
        Instant end = Instant.ofEpochMilli((new Date(3)).getTime());
        Interval interval = Interval.of(start, end);
        Collection<LiveImageVO> images = szApi.getImages("6", LiveImageType.TEMP_SUBFLOOR, interval, 120);
		Assert.assertEquals(3, images.size());

        start = Instant.ofEpochMilli((new Date(100)).getTime());
        end = Instant.ofEpochMilli((new Date(300)).getTime());
        interval = Interval.of(start, end);
        images = szApi.getImages("6", LiveImageType.TEMP_SUBFLOOR, interval, 120);
		Assert.assertEquals(0, images.size());

        start = Instant.ofEpochMilli((new Date(1)).getTime());
        end = Instant.ofEpochMilli((new Date(3)).getTime());
        interval = Interval.of(start, end);
        images = szApi.getImages("66", LiveImageType.TEMP_SUBFLOOR, interval, 120);
		Assert.assertEquals(0, images.size());
	}

}
