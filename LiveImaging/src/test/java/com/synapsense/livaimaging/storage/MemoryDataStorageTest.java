package com.synapsense.livaimaging.storage;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import com.synapsense.liveimaging.conf.LiConstants;
import com.synapsense.liveimaging.storage.LegendImageVO;
import com.synapsense.liveimaging.storage.LiveImageVO;
import com.synapsense.liveimaging.storage.MemoryDataStorage;

public class MemoryDataStorageTest {

	private MemoryDataStorage storage;

	@Before
	public void init() {
		storage = new MemoryDataStorage("./testDir");

		LegendImageVO legend1 = new LegendImageVO("legends" + LiConstants.SEPARATOR + "legend1", 200, new Date(1),
		        null, null);
		LegendImageVO legend2 = new LegendImageVO("legends" + LiConstants.SEPARATOR + "legend2", 201, new Date(1),
		        null, null);
		LegendImageVO legend3 = new LegendImageVO("legends" + LiConstants.SEPARATOR + "legend3", 202, new Date(1),
		        null, null);
		LegendImageVO legend4 = new LegendImageVO("legends" + LiConstants.SEPARATOR + "legend4", 998, new Date(1),
		        null, null);

		LegendImageVO legend5 = new LegendImageVO("legends" + LiConstants.SEPARATOR + "legend5", 200, new Date(10),
		        null, null);
		LegendImageVO legend6 = new LegendImageVO("legends" + LiConstants.SEPARATOR + "legend6", 202, new Date(11),
		        null, null);

		storage.saveLegend(legend1);
		storage.saveLegend(legend2);
		storage.saveLegend(legend3);
		storage.saveLegend(legend4);
		storage.saveLegend(legend5);
		storage.saveLegend(legend6);

		LiveImageVO i1 = new LiveImageVO(LiConstants.DC + LiConstants.SEPARATOR + "6" + LiConstants.SEPARATOR + "256"
		        + LiConstants.SEPARATOR + "image-6-200-256", null, new Date(1), "PNG");
		LiveImageVO i2 = new LiveImageVO(LiConstants.DC + LiConstants.SEPARATOR + "6" + LiConstants.SEPARATOR + "256"
		        + LiConstants.SEPARATOR + "image-6-200-256", null, new Date(2), "PNG");
		LiveImageVO i3 = new LiveImageVO(LiConstants.DC + LiConstants.SEPARATOR + "6" + LiConstants.SEPARATOR + "256"
		        + LiConstants.SEPARATOR + "image-6-200-256", null, new Date(3), "PNG");
		LiveImageVO i4 = new LiveImageVO(LiConstants.DC + LiConstants.SEPARATOR + "6" + LiConstants.SEPARATOR + "256"
		        + LiConstants.SEPARATOR + "image-6-200-256", null, new Date(4), "PNG");

		LiveImageVO i5 = new LiveImageVO(LiConstants.DC + LiConstants.SEPARATOR + "6" + LiConstants.SEPARATOR + "128"
		        + LiConstants.SEPARATOR + "image-6-200-128", null, new Date(5), "PNG");
		LiveImageVO i6 = new LiveImageVO(LiConstants.DC + LiConstants.SEPARATOR + "6" + LiConstants.SEPARATOR + "128"
		        + LiConstants.SEPARATOR + "image-6-200-128", null, new Date(6), "PNG");
		LiveImageVO i7 = new LiveImageVO(LiConstants.DC + LiConstants.SEPARATOR + "6" + LiConstants.SEPARATOR + "256"
		        + LiConstants.SEPARATOR + "image-6-202-256", null, new Date(7), "PNG");
		LiveImageVO i8 = new LiveImageVO(LiConstants.DC + LiConstants.SEPARATOR + "6" + LiConstants.SEPARATOR + "256"
		        + LiConstants.SEPARATOR + "image-6-202-256", null, new Date(8), "PNG");

		LiveImageVO i9 = new LiveImageVO(LiConstants.ROOM + LiConstants.SEPARATOR + "7" + LiConstants.SEPARATOR + "256"
		        + LiConstants.SEPARATOR + "image-7-200-256", null, new Date(9), "PNG");
		LiveImageVO i10 = new LiveImageVO(LiConstants.ROOM + LiConstants.SEPARATOR + "7" + LiConstants.SEPARATOR + "256"
		        + LiConstants.SEPARATOR + "image-7-200-256", null, new Date(10), "PNG");
		LiveImageVO i11 = new LiveImageVO(LiConstants.ROOM + LiConstants.SEPARATOR + "7" + LiConstants.SEPARATOR + "128"
		        + LiConstants.SEPARATOR + "image-7-200-128", null, new Date(11), "PNG");
		LiveImageVO i12 = new LiveImageVO(LiConstants.ROOM + LiConstants.SEPARATOR + "7" + LiConstants.SEPARATOR + "128"
		        + LiConstants.SEPARATOR + "image-7-200-128", null, new Date(12), "PNG");

		LiveImageVO i13 = new LiveImageVO(LiConstants.ROOM + LiConstants.SEPARATOR + "8" + LiConstants.SEPARATOR + "256"
		        + LiConstants.SEPARATOR + "image-8-200-256", null, new Date(13), "PNG");
		LiveImageVO i14 = new LiveImageVO(LiConstants.ROOM + LiConstants.SEPARATOR + "8" + LiConstants.SEPARATOR + "256"
		        + LiConstants.SEPARATOR + "image-8-998-256", null, new Date(14), "PNG");
		LiveImageVO i15 = new LiveImageVO(LiConstants.ROOM + LiConstants.SEPARATOR + "8" + LiConstants.SEPARATOR + "128"
		        + LiConstants.SEPARATOR + "image-8-200-128", null, new Date(15), "PNG");
		LiveImageVO i16 = new LiveImageVO(LiConstants.ROOM + LiConstants.SEPARATOR + "8" + LiConstants.SEPARATOR + "128"
		        + LiConstants.SEPARATOR + "image-8-200-128", null, new Date(16), "PNG");

		storage.saveLiveImage(i1);
		storage.saveLiveImage(i2);
		storage.saveLiveImage(i3);
		storage.saveLiveImage(i4);
		storage.saveLiveImage(i5);
		storage.saveLiveImage(i6);
		storage.saveLiveImage(i7);
		storage.saveLiveImage(i8);
		storage.saveLiveImage(i9);
		storage.saveLiveImage(i10);
		storage.saveLiveImage(i11);
		storage.saveLiveImage(i12);
		storage.saveLiveImage(i13);
		storage.saveLiveImage(i14);
		storage.saveLiveImage(i15);
		storage.saveLiveImage(i16);
	}

	@Test
	public void getAvailableImagesTest() {
		Set<LiveImageVO> dcImages = storage.getAvailableImages(LiConstants.DC, 6, 200, 256);
		Assert.assertEquals(4, dcImages.size());

		SortedSet<LiveImageVO> roomImages = storage.getAvailableImages(LiConstants.ROOM, 8, 200, 256);
		Assert.assertEquals(1, roomImages.size());

		LiveImageVO imageVO = roomImages.last();

		Assert.assertEquals(new Date(13), imageVO.getShowTime());
		Assert.assertEquals(new Date(10), imageVO.getLegend().getShowTime());
	}

	@Test
	public void getAvailableLegendsTest() {
		SortedSet<LegendImageVO> legends = storage.getAvailableLegends(200);
		Assert.assertEquals(2, legends.size());

		LegendImageVO legend = legends.last();
		Assert.assertEquals(new Date(10), legend.getShowTime());
	}

	@Test
	public void deleteImages() {
		Assert.assertTrue(storage.hasImage(LiConstants.ROOM, 8));

		Collection<LiveImageVO> images = new ArrayList<LiveImageVO>();

		images.add(new LiveImageVO(LiConstants.ROOM + LiConstants.SEPARATOR + "8" + LiConstants.SEPARATOR + "256"
		        + LiConstants.SEPARATOR + "image-8-200-256", null, new Date(13), "PNG"));
		images.add(new LiveImageVO(LiConstants.ROOM + LiConstants.SEPARATOR + "8" + LiConstants.SEPARATOR + "256"
		        + LiConstants.SEPARATOR + "image-8-998-256", null, new Date(14), "PNG"));
		images.add(new LiveImageVO(LiConstants.ROOM + LiConstants.SEPARATOR + "8" + LiConstants.SEPARATOR + "128"
		        + LiConstants.SEPARATOR + "image-8-200-128", null, new Date(15), "PNG"));
		images.add(new LiveImageVO(LiConstants.ROOM + LiConstants.SEPARATOR + "8" + LiConstants.SEPARATOR + "128"
		        + LiConstants.SEPARATOR + "image-8-200-128", null, new Date(16), "PNG"));

		storage.deleteImages(images);

		Assert.assertFalse(storage.hasImage(LiConstants.ROOM, 8));

	}

	@Test
	public void deleteImagesByDate() {
		Assert.assertTrue(storage.hasImage(LiConstants.DC, 6));
		Assert.assertEquals(2, storage.getAvailableImages(LiConstants.ROOM, 7, 200, 128).size());

		storage.deleteImages(new Date(12));

		Assert.assertFalse(storage.hasImage(LiConstants.DC, 6));

		Assert.assertEquals(1, storage.getAvailableImages(LiConstants.ROOM, 7, 200, 128).size());
	}

	@Test
	public void getimages() {
		List<LiveImageVO> images = storage.getImages(new Date(1), new Date(4));
		Assert.assertEquals(4, images.size());
		LiveImageVO image = images.get(0);
		Assert.assertEquals(200, image.getDataclass());

		images = storage.getImages(new Date(10), new Date(11));
		Assert.assertEquals(2, images.size());
	}

}
