package com.synapsense.liveimaging;

import com.synapsense.exception.EnvException;
import com.synapsense.liveimaging.es.EsConfigurationLoader;

public class ESConfigurationReset {

	public static void main(String[] args) throws EnvException {
		new EsConfigurationLoader(null).resetConfigInEs();
		System.out.println("Done");
		System.exit(0);
	}
}
