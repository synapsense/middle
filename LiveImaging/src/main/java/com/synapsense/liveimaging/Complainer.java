package com.synapsense.liveimaging;

/**
 * Interface to be used when something is going wrong and the user needs to be
 * notified about it
 * 
 * @author Dmitry Grudzinskiy
 * 
 */
public interface Complainer {

	/**
	 * LiveImaging can't use the port specified in the configuration
	 * 
	 * @param port
	 *            port number which LiveImaging cannot use
	 */
	void complainPort(int port);

	/**
	 * LiveImaging can't use the output directory specified in the configuration
	 * 
	 * @param dir
	 *            path to the directory
	 */
	void complainDir(String dir);

	/**
	 * Complain about general problem happened while generating images
	 *
	 */
	void complainGeneral();

	/**
	 * Complain about general problem happened while generating images
	 *
	 */
	void complainGeneral(String msg);
}
