package com.synapsense.liveimaging;

import com.synapsense.exception.EnvException;
import com.synapsense.liveimaging.es.EsConfigurationLoader;

public class EsConfigurationRemover {

	public static void main(String[] args) throws EnvException {
		new EsConfigurationLoader(null).deleteConfigFromEs();
		System.out.println("Done");
		System.exit(0);
	}
}
