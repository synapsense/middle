package com.synapsense.liveimaging;

import java.util.concurrent.CountDownLatch;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/** Entry point of the LiveImaging process */
public class LIServiceLauncher {

	private static final LiveImagingApp app = new LiveImagingApp();

	private static final Log log = LogFactory.getLog(LIServiceLauncher.class);

	public static void main(String[] args) throws InterruptedException {
		try {
			app.startLiveImaging();
		} catch (InterruptedException e) {
			log.info("LiveImaging was interrupted while retrying to start.");
			return;
		} catch (Exception e) {
			log.fatal("LiveImaging failed to start.", e);
			System.exit(1);
		}

		Runtime.getRuntime().addShutdownHook(new Thread(LIServiceLauncher::shutdown, "LI Shutdown"));

		//Keep the VM alive
		new CountDownLatch(1).await();
	}

	private static void shutdown() {
		try {
			app.stopLiveImaging();
		} catch (Exception e) {
			log.info("Failed to stop LiveImaging.", e);
		}
	}

	/** Called by AI when the Windows service is stopped. Just delegate to System.exit(). */
	public static void stop() {
		System.exit(0);
	}
}
