package com.synapsense.liveimaging.job;

import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public final class Util {

	private static final int MAX_PERIOD_DAYS = 15;
	public static Date subtractDays(Date date, int days) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.DAY_OF_MONTH, -days);
		return calendar.getTime();
	}

	/**Splits interval into subintervals with <code>Util.MAX_PERIOD_DAYS</code> length.
	 * 
	 * 
	 * @param start
	 * @param end
	 * @return List of dates in reverse sequence, contains at least two elements.
	 */
	public static List<Date> splitInterval(Date start, Date end) {
		LinkedList<Date> dates = new LinkedList<Date>();
		Date d = new Date(end.getTime());
		dates.add((Date) d.clone());
		while (d.after(start)) {
			d = subtractDays(d, MAX_PERIOD_DAYS);
			if(d.after(start)) {
				dates.add((Date) d.clone());
			}
		}
		dates.add(start);
		return dates;
	}

	private Util() {
	}
}
