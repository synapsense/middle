package com.synapsense.liveimaging.job;

import java.awt.image.BufferedImage;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.liveimaging.Complainer;
import com.synapsense.liveimaging.data.PayLoad;
import com.synapsense.liveimaging.draw.DrawLiveImageException;
import com.synapsense.liveimaging.draw.LiveImageBuilder;
import com.synapsense.liveimaging.interpolation.InterpolationFactory;
import com.synapsense.liveimaging.storage.ImageDataStorageWriter;
import com.synapsense.liveimaging.storage.LiveImageVO;
import com.synapsense.liveimaging.storage.StorageException;

public class Worker {

	protected static final Log log = LogFactory.getLog(Worker.class);

	private InterpolationFactory factory;
	private ImageDataStorageWriter storage;
	private boolean historical = false;

	// PNG or JPG
	private String imageType;
	protected Complainer complainer;

	public BufferedImage generateImage(final PayLoad payload) {
		String imageId = payload.getImageId();
		log.info("Generating new image " + imageId + " number of points " + payload.getNbPoints());
		LiveImageBuilder builder = new LiveImageBuilder();
		builder.setBackGroundImage(payload.getBackground());
		builder.setInterpolationFactory(factory);
		builder.setColorMap(payload.getColorMap());
		try {
			long start = System.currentTimeMillis();
			payload.getImage().draw(builder);
			if (log.isDebugEnabled()) {
				log.debug("Image " + imageId + " generation time " + (System.currentTimeMillis() - start));
			}
			log.info("Image " + imageId + " has been generated");
		} catch (DrawLiveImageException e) {
			String msg = "Error generating image " + imageId;
			complainer.complainGeneral(msg);
			log.error(msg, e);
		}

		return builder.getImage();
	}

	public void saveImage(String imageId, BufferedImage image, Date showTime) {
		try {
			long saveStart = System.currentTimeMillis();
			storage.saveLiveImage(new LiveImageVO(imageId, image, showTime, imageType, historical));
			if (log.isDebugEnabled()) {
				log.info("Image " + imageId + " save time - " + (System.currentTimeMillis() - saveStart));
			}
			log.info("Image " + imageId + " has been saved");
		} catch (StorageException e) {
			if (Thread.currentThread().isInterrupted()){
				log.warn("Unable to save image. "+Thread.currentThread().getName()+" has been interrupted");
				return;
			}
			String msg = "Error saving image " + imageId;
			complainer.complainGeneral(msg);
			log.error(msg, e);
		}
	}

	public void setInterpolationFactory(InterpolationFactory factory) {
		this.factory = factory;
	}

	public void setStorage(ImageDataStorageWriter storage) {
		this.storage = storage;
	}

	public void setImageType(String imageType) {
		this.imageType = imageType;
	}

	public void setComplainer(Complainer complainer) {
		this.complainer = complainer;
	}

	public void setHistorical(boolean historical) {
		this.historical = historical;
	}
}
