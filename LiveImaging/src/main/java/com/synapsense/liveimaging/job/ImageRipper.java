package com.synapsense.liveimaging.job;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

import com.synapsense.liveimaging.Complainer;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.EnvException;
import com.synapsense.liveimaging.conf.LiConfig.RealtimeGenerator.KeepPeriod;
import com.synapsense.liveimaging.conf.LiConstants;
import com.synapsense.liveimaging.es.ESContext;
import com.synapsense.liveimaging.es.EsConfigurationLoader;
import com.synapsense.liveimaging.storage.ImageDataStorageReader;
import com.synapsense.liveimaging.storage.ImageDataStorageWriter;
import com.synapsense.liveimaging.storage.LiveImageVO;
import com.synapsense.liveimaging.storage.StorageException;

public class ImageRipper {

	private static final Log log = LogFactory.getLog(ImageRipper.class);

	public static final long SAMPLE_INTERVAL = 5 * 60 * 1000;
	public static final String OBJECT_NAME = "LiImageRipper";

	private ESContext esContext;
	private ImageDataStorageReader reader;
	private ImageDataStorageWriter writer;
	private Collection<KeepPeriod> keepPeriods;

	protected Complainer complainer;
	public void setComplainer(Complainer complainer) {
		this.complainer = complainer;
	}

	private static HashMap<String, List<LiveImageVO>> sortImages(List<LiveImageVO> images) {

		// Sort by date
		Collections.sort(images, new Comparator<LiveImageVO>() {
			@Override
			public int compare(LiveImageVO o1, LiveImageVO o2) {
				if (o1.getShowTime().before(o2.getShowTime())) {
					return -1;
				}
				if (o1.getShowTime().after(o2.getShowTime())) {
					return 1;
				}
				return 0;
			}
		});

		// Sort them by image ids
		HashMap<String, List<LiveImageVO>> imageMap = new HashMap<String, List<LiveImageVO>>();
		for (LiveImageVO image : images) {
			List<LiveImageVO> list = imageMap.get(image.getImageId());
			if (list == null) {
				list = new ArrayList<LiveImageVO>();
				imageMap.put(image.getImageId(), list);
			}
			list.add(image);
		}
		return imageMap;
	}

	public ImageRipper(ImageDataStorageReader reader, ImageDataStorageWriter writer, Collection<KeepPeriod> keepPeriods) {
		this.reader = reader;
		this.writer = writer;
		this.keepPeriods = keepPeriods;
	}

	public void ripImages(Date currentDate) {
		long lastRunTime = getLastRunTime();

		Date endDate = new Date(currentDate.getTime());
		for (KeepPeriod keepPeriod : keepPeriods) {
			Date startDate = Util.subtractDays(currentDate, keepPeriod.getPeriod());
			if (keepPeriod.getRate() > 1) {
				if (startDate.after(new Date(lastRunTime))) {
					// First run, process the whole period
					processPeriod(startDate, endDate, keepPeriod.getRate());
				} else {
					// Process only last changes
					processPeriod(new Date(endDate.getTime() - (currentDate.getTime() - lastRunTime)), endDate,
					        keepPeriod.getRate());
				}
			}
			endDate = startDate;
		}

		// Delete all the images older that the last period
		try {
			log.info("Deleting images were made before " + endDate);
			writer.deleteImages(endDate);
		} catch (StorageException e) {
			log.warn("Unable to delete images older than " + endDate, e);
		}
		updateLastRunTime(currentDate.getTime());
	}

	public void ripAllImages() {
		long currentTime = System.currentTimeMillis();
		Date endDate = new Date(currentTime);
		for (KeepPeriod keepPeriod : keepPeriods) {
			Date startDate = Util.subtractDays(endDate, keepPeriod.getPeriod());
			if (keepPeriod.getRate() > 1) {
				processPeriod(startDate, endDate, keepPeriod.getRate());
			}
			endDate = startDate;
		}
		try {
			writer.deleteImages(endDate);
		} catch (StorageException e) {
			String msg = "Unable to delete images older than " + endDate;
			complainer.complainGeneral(msg);
			log.warn(msg, e);
		}
		updateLastRunTime(currentTime);
	}

	private void processPeriod(Date startDate, Date endDate, int rate) {
		// The minimum process period is rate * sample interval
		// In case the period since last image archiving is less than minimum
		// process period we need to update period start date, otherwise we may
		// leave some images that should be deleted
		Date rateBeginDate = new Date(endDate.getTime() - rate * SAMPLE_INTERVAL);
		if (rateBeginDate.before(startDate)) {
			startDate = rateBeginDate;
		}

		log.info("Archiving images for dates " + startDate + " - " + endDate + " with rate " + rate);
		// When interval is too big process operation may takes a lot of time 
		//and fails with transaction timeout or OOM error.
		// Split interval to operate with limited size of data. 
		List<Date> dates = Util.splitInterval(startDate, endDate);
		Iterator<Date> iter = dates.iterator();
		endDate = iter.next();
		while (iter.hasNext()) {
			Date start = iter.next();
			log.info("Archiving images: process interval " + start + " - " + endDate + " with rate " + rate);
			HashMap<String, List<LiveImageVO>> imageMap = sortImages(reader.getImages(start, endDate));

			List<LiveImageVO> toDelete = new LinkedList<LiveImageVO>();

			for (Entry<String, List<LiveImageVO>> entry : imageMap.entrySet()) {
				List<LiveImageVO> images = entry.getValue();
	
				long chunkStart = start.getTime();
	
				int i = 0;
				while (chunkStart <= endDate.getTime()) {
					long chunkEnd = chunkStart + rate * SAMPLE_INTERVAL;
	
					// Delete all the images before the start period
					while (i < images.size() && chunkStart > images.get(i).getShowTime().getTime()) {
						toDelete.add(images.get(i++));
					}
	
					// Save one image per period
					if (i < images.size() && images.get(i).getShowTime().getTime() < chunkEnd) {
						i++;
					}
	
					chunkStart = chunkEnd;
				}
			}
			try {
				writer.deleteImages(toDelete);
			} catch (StorageException e) {
				String msg = "Unable to delete images for period " + start + " - " + endDate;
				complainer.complainGeneral(msg);
				log.warn(msg, e);
			}
			endDate = start;
		}
	}

	private long getLastRunTime() {
        Collection<TO<?>> configs = esContext.getEnv().getObjects(EsConfigurationLoader.OBJECT_TYPE,
		        new ValueTO[] { new ValueTO(EsConfigurationLoader.NAME_PROPERTY, OBJECT_NAME) });
        if(configs.isEmpty()) {
			log.warn("Empty expire transaction log.");
			return 0;
        } else if(configs.size() > 1) {
			log.warn("Multipe expire transaction log have been found in ES");
			return 0;
        } else {
        	TO<?> lastRunTO = configs.iterator().next();
        	try {
        		return Long.valueOf(esContext.getEnv().getPropertyValue(lastRunTO, EsConfigurationLoader.CONFIG_PROPERTY, String.class));
			} catch (EnvException e) {
				log.error("Unable to get expire timestamp...", e);
				return 0;
			}
        }
	}

	private void updateLastRunTime(long lastRunTime) {
        Collection<TO<?>> configs = esContext.getEnv().getObjects(EsConfigurationLoader.OBJECT_TYPE,
		        new ValueTO[] { new ValueTO(EsConfigurationLoader.NAME_PROPERTY, OBJECT_NAME) });
        if(configs.isEmpty()) {
			log.warn("Empty expire transaction log.");
			try {
				esContext.getEnv().createObject(EsConfigurationLoader.OBJECT_TYPE, new ValueTO[] {
						new ValueTO(EsConfigurationLoader.NAME_PROPERTY, OBJECT_NAME),
				        new ValueTO(EsConfigurationLoader.CONFIG_PROPERTY, String.valueOf(lastRunTime)) });
			} catch (EnvException e) {
				log.error("Unable to save expire timestamp...", e);
			}
        } else if(configs.size() > 1) {
			log.warn("Multipe expire transaction log have been found in ES");
        } else {
        	TO<?> lastRunTO = configs.iterator().next();
        	try {
        		esContext.getEnv().setPropertyValue(lastRunTO, EsConfigurationLoader.CONFIG_PROPERTY,
        				String.valueOf(lastRunTime));
			} catch (EnvException e) {
				log.error("Unable to get expire timestamp...", e);
			}
        }
	}

	public void setEsContext(ESContext esContext) {
		this.esContext = esContext;
	}
}
