package com.synapsense.liveimaging.job;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;

public class ExpireJob implements StatefulJob {

	private static final Log log = LogFactory.getLog(ExpireJob.class);

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		long start = System.currentTimeMillis();
		ImageRipper ripper = (ImageRipper) context.getJobDetail().getJobDataMap()
		        .get(RealTimeImageGenerator.IMAGE_RIPPER);
		ripper.ripImages(context.getFireTime());
		log.info("Expire job completed in " + (System.currentTimeMillis() - start) + " mls");
	}

}
