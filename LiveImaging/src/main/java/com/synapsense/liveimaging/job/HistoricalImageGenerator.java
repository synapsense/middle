package com.synapsense.liveimaging.job;

import java.text.ParseException;
import java.util.Collection;
import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobDetail;
import org.quartz.SchedulerException;
import org.quartz.impl.JobDetailImpl;
import org.quartz.impl.triggers.CronTriggerImpl;

import com.synapsense.dto.TO;
import com.synapsense.liveimaging.conf.LiConstants;
import com.synapsense.liveimaging.data.DataSourceException;
import com.synapsense.liveimaging.web.ImageType;

public class HistoricalImageGenerator extends AbstractImageGenerator {

	private static final Log log = LogFactory.getLog(HistoricalImageGenerator.class);
	private String histExpireCron;
	public static final String HISTORICAL_IMAGE_RIPPER = "historical_ripper";
	private static final String JOB_GROUP = "HistoricalImageGeneration";
	private ScheduleService scheduleService;
	protected String outputDir = LiConstants.OUTPUT_FOLDER;
	private HistoricalImageRipper histImageRipper;

	private AtomicReference<GenerationDetails> payloadReference;
	private final Lock lock = new ReentrantLock();
	private Condition nonEmpty;
	private ExecutorService payloadLoader = Executors.newSingleThreadExecutor(new ThreadFactory() {
		private final AtomicInteger threadNumber = new AtomicInteger(1);

		@Override
		public Thread newThread(Runnable r) {
			Thread t = new Thread(r, "pool-payload-loader-" + threadNumber.getAndIncrement());
			t.setUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
				@Override
				public void uncaughtException(Thread t, Throwable e) {
					log.warn("Unexpected error occurred. Thread " + t.getName() + " is stopped", e);
				}
			});
			return t;
		}
	});

	public HistoricalImageGenerator() {
		this.historical = true;
	}

	@Override
	public void start() throws SchedulerException, ParseException {
		// Loading of payloads in a separate thread for quick response
		initPayloadLoader();

		Runtime rt = Runtime.getRuntime();
		// Half of the processors are used by RealTimeImageGenerator. Starting
		// n/4 workers.
		int avail = Math.max(1, rt.availableProcessors() / 4);
		if (avail > LiConstants.WORKER_LIMIT)
			avail = LiConstants.WORKER_LIMIT;
		log.info("WORKER: Available processors: " + rt.availableProcessors() + ", usable processors: "
		        + (int) (rt.availableProcessors() / 4) + ", worker limit: " + LiConstants.WORKER_LIMIT + ", Starting "
		        + avail + " workers ");
		startWorkers(avail, "History");
		scheduleJobs();
	}

	@Override
	public void stop() {
		stopWorkers();
		payloadLoader.shutdownNow();
		try {
			while (!payloadLoader.awaitTermination(5, TimeUnit.SECONDS)) {
				log.info("Waiting for payload loader to be stopped...");
			}
		} catch (InterruptedException e) {
			log.warn("Awaiting payload loader termination has been interrupted", e);
			Thread.currentThread().interrupt(); // restore status
			return;
		}
	}

	public boolean hasJob() {
		if (workerQueue.isEmpty()) {
			if (payloadReference.get() == null) {
				return false;
			}
		}
		return true;
	}

	public boolean addTask(Collection<TO<?>> objIds, Collection<ImageType> imageTypes, long interval, Date start,
	        Date end) {
		if (workerQueue.isEmpty()) {
			if (payloadReference.get() == null) {
				if (lock.tryLock()) {
					if (payloadReference.get() == null) {
						try {
							payloadReference.set(new GenerationDetails(new PayLoadPipe(), objIds, imageTypes, interval,
							        start, end));
							nonEmpty.signalAll();
							return true;
						} finally {
							lock.unlock();
						}
					}

				}
			}
		}
		return false;
	}

	public void setHistExpireCron(String histExpireCron) {
		this.histExpireCron = histExpireCron;
	}

	public void setOutputDir(String outputDir) {
		this.outputDir = outputDir;
	}

	public void setScheduleService(ScheduleService scheduleService) {
		this.scheduleService = scheduleService;
	}

	public void setHistImageRipper(HistoricalImageRipper histImageRipper) {
		this.histImageRipper = histImageRipper;
	}

	private void scheduleJobs() throws SchedulerException, ParseException {
		// Create historical image expiration job
		JobDetail historicalExpireJob = new JobDetailImpl("HistoricalImageExpireJob", JOB_GROUP, HistoricalExpireJob.class);
		historicalExpireJob.getJobDataMap().put(HISTORICAL_IMAGE_RIPPER, histImageRipper);
		scheduleService.scheduleJob(historicalExpireJob, new CronTriggerImpl("HistoricalImageExpireTrigger", JOB_GROUP,
		        histExpireCron));
		log.info("Historical Image archiving is set on schedule " + histExpireCron);
	}

	private void initPayloadLoader() {
		payloadReference = new AtomicReference<GenerationDetails>();

		nonEmpty = lock.newCondition();

		payloadLoader.execute(new Runnable() {

			@Override
			public void run() {
				while (true) {
					lock.lock();
					try {
						if (payloadReference.get() == null) {
							try {
								log.debug("Awaiting a new historical images generation task...");
								nonEmpty.await();
							} catch (InterruptedException e) {
								log.debug(Thread.currentThread().getName() + " has been interrupted");
								Thread.currentThread().interrupt();
								return;
							}
						}
						GenerationDetails genDetails = payloadReference.get();

						log.debug("Loading historical payloads...");
						try {
							dataSource.load(genDetails.payloadCollection, genDetails.start, genDetails.end,
							        genDetails.objIds, genDetails.imageTypes, genDetails.interval);
						} catch (DataSourceException e) {
							String msg = "Failed to load payloads due to - " + e.getLocalizedMessage();
							complainer.complainGeneral(msg);
							log.warn(msg, e);
						}

					} finally {
						payloadReference.set(null);
						lock.unlock();
					}

				}

			}
		});
	}

	private class GenerationDetails {
		private PayLoadPipe payloadCollection;
		private Collection<TO<?>> objIds;
		private Collection<ImageType> imageTypes;
		private long interval;
		private Date start;
		private Date end;

		private GenerationDetails(PayLoadPipe payloadCollection, Collection<TO<?>> objIds,
		        Collection<ImageType> imageTypes, long interval, Date start, Date end) {
			this.payloadCollection = payloadCollection;
			this.objIds = objIds;
			this.imageTypes = imageTypes;
			this.interval = interval;
			this.start = start;
			this.end = end;
		}
	}

}
