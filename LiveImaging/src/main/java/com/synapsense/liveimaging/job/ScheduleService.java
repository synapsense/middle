package com.synapsense.liveimaging.job;

import java.util.Properties;

import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;

public class ScheduleService {
	private Scheduler scheduler;

	public ScheduleService(String threadCount) throws SchedulerException {
		// Create and start scheduler
		Properties props = new Properties();
		props.put("org.quartz.threadPool.class", "org.quartz.simpl.SimpleThreadPool");
		props.put("org.quartz.threadPool.threadCount", threadCount);
		SchedulerFactory schFactory = new StdSchedulerFactory(props);
		scheduler = schFactory.getScheduler();
	}

	public void stop() throws SchedulerException {
		// Kill scheduler
		scheduler.shutdown(true);
	}

	public void start() throws SchedulerException {
		scheduler.start();
	}

	public void scheduleJob(JobDetail jobDetail, CronTrigger trigger) throws SchedulerException {
		scheduler.scheduleJob(jobDetail, trigger);
	}

}
