package com.synapsense.liveimaging.job;

import java.awt.image.BufferedImage;
import java.text.ParseException;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobDetail;
import org.quartz.SchedulerException;
import org.quartz.impl.JobDetailImpl;
import org.quartz.impl.triggers.CronTriggerImpl;

import com.synapsense.dto.TO;
import com.synapsense.liveimaging.conf.LiConstants;
import com.synapsense.liveimaging.data.DataSourceException;
import com.synapsense.liveimaging.data.PayLoad;

public class RealTimeImageGenerator extends AbstractImageGenerator {

	public static final String PAY_LOADER = "payloader";
	public static final String IMAGE_RIPPER = "ripper";
	public static final String IMAGE_GENERATION_JOB = "ImageGenerationJob";

	private final static Log log = LogFactory.getLog(RealTimeImageGenerator.class);

	private static final String JOB_GROUP = "RealTimeImageGeneration";

	private String drawCron;
	private String expireCron;

	private ScheduleService scheduleService;

	protected String outputDir = LiConstants.OUTPUT_FOLDER;

	private ImageRipper imageRipper;

	static interface PayLoader {

		void addNewPayload(Date genTime);
	}

	@Override
	public void start() throws Exception {

		// Start image generation threads
		startWorkers();

		// Schedule data loading and old images deletion
		scheduleJobs();
	}

	@Override
	public void stop() {
		// Kill working threads
		stopWorkers();
	}

	public void setDrawCron(String drawCron) {
		this.drawCron = drawCron;
	}

	public void setExpireCron(String expireCron) {
		this.expireCron = expireCron;
	}

	public void setOutputDir(String outputDir) {
		this.outputDir = outputDir;
	}

	public void setScheduleService(ScheduleService scheduleService) {
		this.scheduleService = scheduleService;
	}

	public void setImageRipper(ImageRipper imageRipper) {
		this.imageRipper = imageRipper;
	}

	private void startWorkers() {
		Runtime rt = Runtime.getRuntime();
		// Start n/2 workers
		int avail = Math.max(1, rt.availableProcessors() / 2);
		if (avail > LiConstants.WORKER_LIMIT)
			avail = LiConstants.WORKER_LIMIT;

		int workers = avail;

		String workersNumProperty = System.getProperty("com.synapsense.liveimaging.workers", Integer.toString(avail));
		try {
			workers = Integer.parseInt(workersNumProperty);
		} catch (NumberFormatException e) {
			log.warn("System property \"com.synapsense.liveimaging.workers\" should be integer");
		}

		log.info("WORKER: Available processors: " + rt.availableProcessors() + ", usable processors: " + avail
		        + ", worker limit: " + LiConstants.WORKER_LIMIT + ", Starting " + workers + " workers ");
		startWorkers(workers, "Runtime");
	}

	private void scheduleJobs() throws SchedulerException, ParseException {
		// Create image generation job
		JobDetail generationJob = new JobDetailImpl(IMAGE_GENERATION_JOB, JOB_GROUP, PushJob.class);
		generationJob.getJobDataMap().put(PAY_LOADER, new PayLoader() {
			@Override
			public void addNewPayload(Date genTime) {
				try {
					dataSource.load(new PayLoadPipe(), genTime);
				} catch (DataSourceException e) {
					log.error(e.getMessage(), e);
				}
			}
		});
		scheduleService.scheduleJob(generationJob, new CronTriggerImpl("ImageGenerationTrigger", JOB_GROUP, drawCron));
		log.info("Image generation is set on schedule " + drawCron);

		// Create image expiration job
		JobDetail expireJob = new JobDetailImpl("ImageExpireJob", JOB_GROUP, ExpireJob.class);
		expireJob.getJobDataMap().put(IMAGE_RIPPER, imageRipper);
		scheduleService.scheduleJob(expireJob, new CronTriggerImpl("ImageExpireTrigger", JOB_GROUP, expireCron));
		log.info("Image archiving is set on schedule " + expireCron);
	}

	public BufferedImage generateLatestImage(TO<?> loadTO, Integer dataclass, Integer layer, boolean lite) {
		Worker worker = new Worker();
		worker.setInterpolationFactory(nonCachingInterpolationFactory);
		worker.setStorage(writer);
		worker.setImageType(imageFormat);
		worker.setComplainer(complainer);

		PayLoad payload = lite ? dataSource.loadLite(loadTO, dataclass, layer) : dataSource.load(loadTO, dataclass,
		        layer);

		return worker.generateImage(payload);
	}

	public void forceRunImageRipper() {
		log.info("Running archiving for all images");
		imageRipper.ripAllImages();
	}

	public void forceRunImageGeneration() {
		log.info("Running image generation");
		dataSource.load(new PayLoadPipe(), new Date());
	}

}
