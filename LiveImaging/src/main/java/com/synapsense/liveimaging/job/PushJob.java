package com.synapsense.liveimaging.job;

import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;

import com.synapsense.liveimaging.job.RealTimeImageGenerator.PayLoader;

public class PushJob implements StatefulJob {

	public static final long DEFAULT_PERIOD = 1000 * 60 * 60;
	private static final Log log = LogFactory.getLog(PushJob.class);

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		long startTime = System.currentTimeMillis();
		Date firetime = context.getFireTime();

		PayLoader payLoader = (PayLoader) context.getJobDetail().getJobDataMap().get(RealTimeImageGenerator.PAY_LOADER);
		payLoader.addNewPayload(firetime);
		log.info("Added new payload - " + (System.currentTimeMillis() - startTime));
	}


}
