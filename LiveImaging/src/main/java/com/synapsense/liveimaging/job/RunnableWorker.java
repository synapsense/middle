package com.synapsense.liveimaging.job;

import java.awt.image.BufferedImage;
import java.util.concurrent.LinkedBlockingQueue;

import com.synapsense.liveimaging.data.PayLoad;

public class RunnableWorker extends Worker implements Runnable {
	protected LinkedBlockingQueue<PayLoad> queue;

	@Override
	public void run() {
		while (true) {
			PayLoad payload = null;
			try {
				payload = queue.take();
			} catch (InterruptedException e) {
				log.info(Thread.currentThread().getName() + " has been interrupted");
				Thread.currentThread().interrupt();
				return;
			}
			processPayload(payload);
		}
	}

	public void setQueue(LinkedBlockingQueue<PayLoad> queue) {
		this.queue = queue;
	}

	protected void processPayload(PayLoad payload) {
		try {
			long start = System.currentTimeMillis();
			BufferedImage generatedImage = generateImage(payload);
			saveImage(payload.getImageId(), generatedImage, payload.getShowTime());
			log.info("Image " + payload.getImageId() + " full time - " + (System.currentTimeMillis() - start));
		} catch (Throwable e) {
			String msg = "Runtime exception occurred while generating image for area " + payload.getImage();
			complainer.complainGeneral(msg);
			log.error(msg, e);
		}
	}
}
