package com.synapsense.liveimaging.job;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.liveimaging.conf.LiConstants;
import com.synapsense.liveimaging.storage.ImageDataStorageReader;
import com.synapsense.liveimaging.storage.ImageDataStorageWriter;
import com.synapsense.liveimaging.storage.LiveImageVO;

public class HistoricalImageRipper {
	private static final Log log = LogFactory.getLog(HistoricalImageRipper.class);
	private ImageDataStorageReader reader;
	private ImageDataStorageWriter writer;
	private String output = LiConstants.OUTPUT_FOLDER;
	// 24 hours
	private long keepPeriod = 1000 * 60 * 60 * 24;

	public HistoricalImageRipper(ImageDataStorageReader reader, ImageDataStorageWriter writer, String output,
	        int histKeepPeriod) {
		this.reader = reader;
		this.writer = writer;
		this.output = output;
		this.keepPeriod = 1000 * 60 * 60 * histKeepPeriod;
	}

	public void ripImages() {
		List<LiveImageVO> historicalImages = reader.getAllHistoricalImages();
		Date cutOffDate = new Date(System.currentTimeMillis() - keepPeriod);
		log.info("Deliting Historical Images, created before " + cutOffDate);
		List<LiveImageVO> imagesToDelete = new ArrayList<LiveImageVO>();
		for (LiveImageVO image : historicalImages) {
			Date creationDate = image.getCreationDate();
			if (creationDate.before(cutOffDate)) {
				imagesToDelete.add(image);
			}
		}
		// updateLog(imagesToDelete);
		writer.deleteImages(imagesToDelete);
	}

	@SuppressWarnings("unused")
    private void updateLog(List<LiveImageVO> imagesToDelete) {
		File expireLog = new File(output + "/histexpire.log");
		if (!expireLog.exists()) {
			try {
				expireLog.createNewFile();
			} catch (IOException e) {
				log.error("Unable to create expire log file...", e);
				return;
			}
		}
		StringBuffer sb = new StringBuffer();
		for (LiveImageVO image : imagesToDelete) {
			sb.append(image.getImageId());
			sb.append("\n");
		}
		try {
			FileWriter logWriter = new FileWriter(expireLog);

			logWriter.write(sb.toString());
			logWriter.close();
		} catch (IOException e) {
			log.error("Unable to write to expire log file...", e);
		}
	}

}
