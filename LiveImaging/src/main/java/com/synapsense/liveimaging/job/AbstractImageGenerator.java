package com.synapsense.liveimaging.job;

import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.liveimaging.Complainer;
import com.synapsense.liveimaging.data.DataSource;
import com.synapsense.liveimaging.data.PayLoad;
import com.synapsense.liveimaging.data.PayLoadCollection;
import com.synapsense.liveimaging.interpolation.InterpolationFactory;
import com.synapsense.liveimaging.storage.ImageDataStorageWriter;

public abstract class AbstractImageGenerator {

	private static final Log log = LogFactory.getLog(AbstractImageGenerator.class);

	protected DataSource dataSource;
	protected ImageDataStorageWriter writer;
	protected InterpolationFactory interpolationFactory;
	protected InterpolationFactory nonCachingInterpolationFactory;

	protected String imageFormat;
	protected Complainer complainer;
	protected boolean historical = false;

	// Executor service for image generation threads
	private ExecutorService executorService;
	protected LinkedBlockingQueue<PayLoad> workerQueue = new LinkedBlockingQueue<PayLoad>();

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	public void setImageWriter(ImageDataStorageWriter imageDataStorage) {
		this.writer = imageDataStorage;
	}

	public void setInterpolationFactory(InterpolationFactory interpolationFactory) {
		this.interpolationFactory = interpolationFactory;
	}

	public void setNonCachingInterpolationFactory(InterpolationFactory interpolationFactory) {
		this.nonCachingInterpolationFactory = interpolationFactory;
	}

	public void setImageFormat(String imageFormat) {
		this.imageFormat = imageFormat;
	}

	public void setComplainer(Complainer complainer) {
		this.complainer = complainer;
	}

	public abstract void start() throws Exception;

	public abstract void stop();

	protected void startWorkers(int workersNumber, final String namePrefix) {
		executorService = Executors.newFixedThreadPool(workersNumber, new ThreadFactory() {
			private final AtomicInteger poolNumber = new AtomicInteger(1);

			@Override
			public Thread newThread(Runnable r) {
				Thread thread = new Thread(Thread.currentThread().getThreadGroup(), r, namePrefix + "Worker-"
				        + poolNumber.getAndIncrement());
				thread.setDaemon(true);
				return thread;
			}
		});

		for (int i = 1; i <= workersNumber; i++) {
			RunnableWorker worker = new RunnableWorker();
			worker.setInterpolationFactory(interpolationFactory);
			worker.setQueue(workerQueue);
			worker.setStorage(writer);
			worker.setImageType(imageFormat);
			worker.setComplainer(complainer);
			worker.setHistorical(historical);
			executorService.execute(worker);
		}
	}

	protected void stopWorkers() {
		executorService.shutdownNow();
		try {
			while (!executorService.awaitTermination(5, TimeUnit.SECONDS)) {
				log.info("Waiting for all workers to be stopped...");
			}
		} catch (InterruptedException e) {
			log.warn("Awaiting workers termination has been interrupted", e);
			Thread.currentThread().interrupt(); // restore status
			return;
		}
	}

	protected class PayLoadPipe implements PayLoadCollection {

		@Override
		public boolean isEmpty() {
			return workerQueue.isEmpty();
		}

		@Override
		public int size() {
			return workerQueue.size();
		}

		@Override
		public void addPayLoad(PayLoad payload) {
			try {
				workerQueue.put(payload);
			} catch (InterruptedException e) {
				log.debug("Adding a new historical generation payload to the pipe has been interrupted");
				Thread.currentThread().interrupt();
			}
		}
	}
}
