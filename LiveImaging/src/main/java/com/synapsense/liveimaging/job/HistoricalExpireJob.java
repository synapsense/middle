package com.synapsense.liveimaging.job;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;

public class HistoricalExpireJob implements StatefulJob {

	private static final Log log = LogFactory.getLog(ExpireJob.class);

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		long start = System.currentTimeMillis();
		HistoricalImageRipper ripper = (HistoricalImageRipper) context.getJobDetail().getJobDataMap()
		        .get(HistoricalImageGenerator.HISTORICAL_IMAGE_RIPPER);
		ripper.ripImages();
		log.info("Historical Expire job completed in " + (System.currentTimeMillis() - start) + " mls");
	}

}