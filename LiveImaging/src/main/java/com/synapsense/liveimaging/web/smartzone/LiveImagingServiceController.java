package com.synapsense.liveimaging.web.smartzone;

import com.synapsense.liveimaging.api.smartzone.LiveImagingService;
import com.synapsense.liveimaging.storage.ImageVO;
import com.synapsense.liveimaging.storage.LiveImageVO;
import com.synapsense.liveimaging.web.MovieStream;
import com.synapsense.liveimaging.web.NotFoundException;
import com.synapsense.liveimaging.web.Result;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.threeten.extra.Interval;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;

@Controller
@RequestMapping(value = "/floorplans/{id}")
public class LiveImagingServiceController {

    private static final Log log = LogFactory.getLog(LiveImagingServiceController.class);

    @Autowired
    private LiveImagingService szApi;

    @RequestMapping(value = "/images", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public Result hasImages(@PathVariable
                            final String id) {
        return new Result(szApi.hasImages(id));
    }

    @RequestMapping(value = "/images",
                    method = RequestMethod.GET,
                    produces = "application/json",
                    params = {"imageType", "date"})
    @ResponseBody
    public ImageVO getImage(
            @PathVariable
            final String id,
            @RequestParam(value = "imageType")
            final LiveImageType imageType,
            @RequestParam(value = "date")
            final Long date,
            HttpServletRequest request) throws IOException {

        log.debug("getImage( id=" + id + ", imageType=" + imageType.toString() +
                  ", date=" + date.toString() + " )");

        final LiveImageVO li = szApi.getImage(id, imageType, Instant.ofEpochMilli(date));
        return createImage(request, li);
    }

    @RequestMapping(value = "/images",
                    method = RequestMethod.GET,
                    produces = "application/json",
                    params = {"imageType", "start", "end", "maxImages"})
    @ResponseBody
    public Collection<ImageVO> getImages(
            @PathVariable
            final String id,
            @RequestParam(value = "imageType")
            final LiveImageType imageType,
            @RequestParam(value = "start")
            final Long start,
            @RequestParam(value = "end")
            final Long end,
            @RequestParam(value = "maxImages", defaultValue = "120")
            final Integer maxImages,
            HttpServletRequest request)
            throws IOException {

        log.debug("getImages( id=" + id + ", imageType=" + imageType.toString() +
                  ", start=" + start.toString() + ", end=" + end.toString() +
                  ", maxImages=" + maxImages.toString() + " )");

        final Instant startAt = Instant.ofEpochMilli(start);
        final Instant endAt = Instant.ofEpochMilli(end);
        final Interval interval = Interval.of(startAt, endAt);
        final Collection<LiveImageVO> liveImages = szApi.getImages(id, imageType, interval, maxImages);
        final List<ImageVO> images = new ArrayList<>(liveImages.size());
        for (final LiveImageVO liveImage : liveImages) {
            images.add(createImage(request, liveImage));
        }

        return images;
    }

    @RequestMapping(value = "/images/latest",
                    method = RequestMethod.GET,
                    produces = "application/json",
                    params = {"imageType"})
    @ResponseBody
    public ImageVO getLatestImage(
            @PathVariable
            final String id,
            @RequestParam(value = "imageType")
            final LiveImageType imageType,
            HttpServletRequest request) throws IOException {

        log.debug("getImage( id=" + id + ", imageType=" + imageType.toString() + " )");

        final LiveImageVO li = szApi.getLatestImage(id, imageType);
        return createImage(request, li);
    }

    @RequestMapping(value = "/images/movie",
                    method = RequestMethod.GET,
                    produces = "application/octet-stream",
                    params = {"imageType", "start", "end", "maxImages", "unitSystem"})
    public void getMovie(
            @PathVariable
            final String id,
            @RequestParam(value = "imageType")
            final LiveImageType imageType,
            @RequestParam(value = "start")
            final Long start,
            @RequestParam(value = "end")
            final Long end,
            @RequestParam(value = "maxImages", defaultValue = "120")
            final Integer maxImages,
            @RequestParam(value = "unitSystem", defaultValue = "NATIVE")
            final UnitsSystem unitSystem,
            final Locale locale,
            @RequestHeader(value = "User-Agent")
            final String userAgent,
            final HttpServletResponse response) throws IOException {

        log.debug("getMovie( id=" + id + ", imageType=" + imageType.toString() +
                  ", start=" + start.toString() + ", end=" + end.toString() +
                  ", maxImages=" + maxImages.toString() + ", unitSystem=" + unitSystem.toString() + " )");

        final Instant startAt = Instant.ofEpochMilli(start);
        final Instant endAt = Instant.ofEpochMilli(end);
        final Interval interval = Interval.of(startAt, endAt);
        final MovieStream outputStream = new MovieStream(userAgent, response);

        szApi.getMovie(id,
                       imageType,
                       interval,
                       maxImages,
                       unitSystem,
                       locale,
                       outputStream);
    }

    public void setLiveImaging(final LiveImagingService szApi) {
        this.szApi = szApi;
    }

    private static URL getBaseUrl(HttpServletRequest request) throws MalformedURLException {
        return new URL(request.isSecure() ? "https" : "http",
                       request.getServerName(),
                       request.getServerPort(),
                       "");
    }

    private static ImageVO createImage(HttpServletRequest request, LiveImageVO li) throws MalformedURLException {
        if (li == null) {
            throw new NotFoundException();
        }

        URL baseURL = getBaseUrl(request);
        return new ImageVO(baseURL, li);
    }
}
