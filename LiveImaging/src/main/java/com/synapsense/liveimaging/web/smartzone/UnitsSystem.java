package com.synapsense.liveimaging.web.smartzone;

/**
 * Created by WFI on 2/4/2016.
 */
public enum UnitsSystem {
    NATIVE("Server"),
    SI("SI");

    private final String esTypeName;

    UnitsSystem(final String esTypeName) {
        this.esTypeName = esTypeName;
    }

    public String getESTypeName() {
        return esTypeName;
    }
}

