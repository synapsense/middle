package com.synapsense.liveimaging.web;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ImageType {

	private int dataclass;

	private int layer;

	public ImageType() {

	}

	public ImageType(int dataclass, int layer) {
		this.dataclass = dataclass;
		this.layer = layer;
	}

	public int getLayer() {
		return layer;
	}

	public void setLayer(int layer) {
		this.layer = layer;
	}

	public int getDataclass() {
		return dataclass;
	}

	public void setDataclass(int dataclass) {
		this.dataclass = dataclass;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + dataclass;
		result = prime * result + layer;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ImageType other = (ImageType) obj;
		if (dataclass != other.dataclass)
			return false;
		if (layer != other.layer)
			return false;
		return true;
	}

}
