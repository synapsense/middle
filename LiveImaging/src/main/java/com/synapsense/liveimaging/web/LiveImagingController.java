package com.synapsense.liveimaging.web;

import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.liveimaging.api.LiveImaging;
import com.synapsense.liveimaging.data.ColorMap;
import com.synapsense.liveimaging.data.ColorMaps;
import com.synapsense.liveimaging.job.RealTimeImageGenerator;
import com.synapsense.liveimaging.storage.LegendImageVO;
import com.synapsense.liveimaging.storage.LiveImageVO;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

@Controller
public class LiveImagingController {

	@Autowired
	private LiveImaging api;
	@Autowired
	private RealTimeImageGenerator realTimeImageGenerator;

	public void setLiveImaging(LiveImaging api) {
		this.api = api;
	}

	@RequestMapping(value = "/getLatestImage", method = RequestMethod.GET, produces = "image/png")
	@ResponseBody
	public byte[] getLatestImage(@RequestParam(value = "objId") String objId,
	        @RequestParam(value = "dataclass") Integer dataclass, @RequestParam(value = "layer") Integer layer,
	        HttpServletResponse response) {
		LiveImageVO image = api.getLatestImage(TOFactory.getInstance().loadTO(objId), dataclass, layer);
		if (image != null) {
			try {
				response.setHeader("Timestamp", String.valueOf(image.getShowTime().getTime()));
				return api.loadImage(image.getResourceName());
			} catch (IOException e) {
				throw new InternalServerException();
			}
		} else {
			throw new NotFoundException();
		}
	}

	@RequestMapping(value = "/generateLatestImage", method = RequestMethod.GET, produces = "image/png")
	@ResponseBody
	public byte[] generateLatestImage(@RequestParam(value = "objId") String objId,
	        @RequestParam(value = "dataclass") Integer dataclass, @RequestParam(value = "layer") Integer layer,
	        HttpServletResponse response) {
		try {
			return api.generateLatestImage(TOFactory.getInstance().loadTO(objId), dataclass, layer);
		} catch (IOException e) {
			throw new InternalServerException();
		}
	}

	@RequestMapping(value = "/generateLatestLiteImage", method = RequestMethod.GET, produces = "image/png")
	@ResponseBody
	public byte[] generateLatestLiteImage(@RequestParam(value = "objId") String objId,
	        @RequestParam(value = "dataclass") Integer dataclass, @RequestParam(value = "layer") Integer layer,
	        HttpServletResponse response) {
		try {
			return api.generateLatestLiteImage(TOFactory.getInstance().loadTO(objId), dataclass, layer);
		} catch (IOException e) {
			throw new InternalServerException();
		}
	}

	@RequestMapping(value = "/getImage", method = RequestMethod.GET, produces = { "image/png", "image/jpg" }, params = { "date" })
	@ResponseBody
	public byte[] getImage(@RequestParam(value = "objId") String objId,
	        @RequestParam(value = "dataclass") Integer dataclass, @RequestParam(value = "layer") Integer layer,
	        @RequestParam(value = "date") Long date, HttpServletResponse response) {
		LiveImageVO image = api.getImage(TOFactory.getInstance().loadTO(objId), dataclass, layer, new Date(date));
		if (image != null) {
			try {
				response.setHeader("Timestamp", String.valueOf(image.getShowTime().getTime()));
				return api.loadImage(image.getResourceName());
			} catch (IOException e) {
				throw new InternalServerException();
			}
		} else {
			throw new NotFoundException();
		}

	}

	@RequestMapping(value = "/getImage", method = RequestMethod.GET, produces = "application/json", params = { "start",
	        "end" })
	@ResponseBody
	public LiveImageVO getImage(@RequestParam(value = "objId") String objId,
	        @RequestParam(value = "dataclass") Integer dataclass, @RequestParam(value = "layer") Integer layer,
	        @RequestParam(value = "start") Long start, @RequestParam(value = "end") Long end) {
		LiveImageVO image = api.getImage(TOFactory.getInstance().loadTO(objId), dataclass, layer, new Date(start),
		        new Date(end));
		return image;
	}

	@RequestMapping(value = "/getImages", method = RequestMethod.GET, produces = "application/json", params = {
	        "dataclass", "layer" })
	@ResponseBody
	public Collection<LiveImageVO> getImages(@RequestParam(value = "objId") String objId,
	        @RequestParam(value = "dataclass") Integer dataclass, @RequestParam(value = "layer") Integer layer,
	        @RequestParam(value = "start") Long start, @RequestParam(value = "end") Long end,
			@RequestParam(value = "maxImages", defaultValue = "120") Integer maxImages) {
		Collection<LiveImageVO> images = api.getImages(TOFactory.getInstance().loadTO(objId), dataclass, layer,
				new Date(start), new Date(end), maxImages);
		return images;
	}

	@RequestMapping(value = "/getMovie", method = RequestMethod.GET, produces = "application/octet-stream")
	public void getMovie(@RequestParam(value = "objId") String objId,
	        @RequestParam(value = "dataclass") Integer dataclass, @RequestParam(value = "layer") Integer layer,
			@RequestParam(value = "start") Long start, @RequestParam(value = "end") Long end,
  		    @RequestParam(value = "maxImages", defaultValue = "120") Integer maxImages,
	        @RequestParam(value = "unitSystem", defaultValue = "Server") String unitSystem, Locale locale,
	        @RequestHeader(value = "User-Agent") String userAgent, HttpServletResponse response) throws IOException {

		MovieStream outputStream = new MovieStream(userAgent, response);
		api.getMovie(TOFactory.getInstance().loadTO(objId), dataclass, layer, new Date(start), new Date(end),
		        unitSystem, locale, outputStream, maxImages);

	}

	@RequestMapping(value = "/updateLegend", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public Result updateLegend(@RequestBody ColorMaps colormaps) {
		try {
			api.updateLegend(colormaps);
			return new Result(true);
		} catch (Exception e) {
			return new Result(false);
		}
	}

	@RequestMapping(value = "/getCurrentLegendImage", method = RequestMethod.GET, produces = "image/png")
	@ResponseBody
	public byte[] getCurrentLegendImage(@RequestParam(value = "dataclass") Integer dataclass,
	        @RequestParam(value = "unitSystem", defaultValue = "Server") String unitSystem) {
		LegendImageVO legend = api.getCurrentLegendImage(dataclass);
		if (legend != null) {
			try {
				if (unitSystem.equalsIgnoreCase("SI")) {
					return api.loadImage(legend.getSiResourcePath());
				} else {
					return api.loadImage(legend.getResorcePath());
				}
			} catch (IOException e) {
				throw new InternalServerException();
			}
		} else {
			throw new NotFoundException();
		}
	}

	@RequestMapping(value = "/getLegends", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public Collection<ColorMap> getLegends() {
		Collection<ColorMap> colormaps = api.getLegends();
		if (colormaps != null) {
			return colormaps;
		} else {
			throw new InternalServerException();
		}

	}

	@RequestMapping(value = "/isServiceAvailable", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public Result isServiceAvailable() {
		return new Result(api.isServiceAvailable());
	}

	@RequestMapping(value = "/hasImages", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public Result hasImages(@RequestParam(value = "objId") String objId) {
		return new Result(api.hasImages(TOFactory.getInstance().loadTO(objId)));
	}

	@RequestMapping(value = "/generateImages", method = RequestMethod.POST)
	@ResponseBody
	public Result generateImages(@RequestParam(value = "objId") Collection<String> rawObjIds,
	        @RequestParam(value = "imageTypes") String imageTypes, @RequestParam(value = "interval") Long interval,
	        @RequestParam(value = "start") Long start, @RequestParam(value = "end") Long end) {
		ObjectMapper mapper = new ObjectMapper();
		Collection<ImageType> types;
		Collection<TO<?>> objIds = new HashSet<TO<?>>();
		try {
			types = mapper.readValue(imageTypes, new TypeReference<Set<ImageType>>() {
			});
			TOFactory factory = TOFactory.getInstance();
			for (String objId : rawObjIds) {
				objIds.add(factory.loadTO(objId));
			}

		} catch (IOException e) {
			throw new BadRequestException();
		}
		return new Result(api.generateImages(objIds, types, interval, new Date(start), new Date(end)));
	}

	@RequestMapping(value = "/isHistoricalGeneratingRunning", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public Result isHistoricalGeneratingRunning() {
		return new Result(api.isHistoricalGeneratingRunning());
	}

	@RequestMapping(value = "/getImages", method = RequestMethod.GET, produces = "application/json", params = { "imageTypes" })
	@ResponseBody
	public Collection<LiveImageVO> getImages(@RequestParam(value = "objId") String objId,
	        @RequestParam(value = "imageTypes") String imageTypes, @RequestParam(value = "date") Long date) {
		ObjectMapper mapper = new ObjectMapper();
		Collection<LiveImageVO> images = new ArrayList<LiveImageVO>();
		try {
			Set<ImageType> types = mapper.readValue(imageTypes, new TypeReference<Set<ImageType>>() {
			});
			for (ImageType type : types) {
				LiveImageVO image = api.getImage(TOFactory.getInstance().loadTO(objId), type.getDataclass(),
				        type.getLayer(), new Date(date));
				if (image != null) {
					images.add(image);
				}
			}
		} catch (IOException e) {
			throw new BadRequestException();
		}
		return images;
	}

	@RequestMapping(value = "/runImageRipper", method = RequestMethod.GET)
	@ResponseBody
	public Result runImageRipper() {
		api.runImageRipper();
		return new Result(true);
	}

	@RequestMapping(value = "/runImageGeneration", method = RequestMethod.GET)
	@ResponseBody
	public void runImageGeneration() {
		realTimeImageGenerator.forceRunImageGeneration();
	}

}
