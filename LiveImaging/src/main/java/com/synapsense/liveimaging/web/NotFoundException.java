package com.synapsense.liveimaging.web;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class NotFoundException extends RuntimeException {

	/**
     * 
     */
	private static final long serialVersionUID = -2582653968950471115L;

}
