package com.synapsense.liveimaging.web;

import com.synapsense.liveimaging.conf.LiConstants;
import com.synapsense.liveimaging.legend.LegendConstants;
import com.synapsense.liveimaging.storage.LiveImageVO;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.monte.media.Format;
import org.monte.media.FormatKeys;
import org.monte.media.avi.AVIWriter;
import org.monte.media.math.Rational;
import org.mortbay.util.UrlEncoded;

import javax.imageio.ImageIO;
import javax.imageio.stream.ImageOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collection;

import static org.monte.media.FormatKeys.EncodingKey;
import static org.monte.media.FormatKeys.FrameRateKey;
import static org.monte.media.FormatKeys.MediaTypeKey;
import static org.monte.media.VideoFormatKeys.*;

public class MovieStream {

	private static Log log = LogFactory.getLog(MovieStream.class);

	private String name;
	private ImageOutputStream stream;
	private HttpServletResponse response;
	private OutputStream os;
	private String userAgent;

	public MovieStream(String useragent, HttpServletResponse writeTo) throws IOException {
		this.response = writeTo;
		this.os = writeTo.getOutputStream();
		this.stream = ImageIO.createImageOutputStream(os);
		this.userAgent = useragent;

	}

	public void outputAVI(String name,
						  String legendLabel,
						  String unitSystem,
						  Collection<LiveImageVO> images,
						  String imageDir) throws IOException {
		AVIWriter out = null;
		Format format = null;

		try {
			out = new AVIWriter(this.getImageOutputStream());
			format = new Format(EncodingKey, ENCODING_AVI_MJPG, DepthKey, 24);
		} catch (Exception e) {
			log.warn("Could not generate video file", e);
			throw e;
		}

		BufferedImage logo = null;
		try {
			logo = ImageIO.read(this.getClass().getClassLoader().getResource(LiConstants.LOGO_RESOURCE));
		} catch (IOException e) {
			log.warn("Video file could not be generated. Logo is not loaded", e);
			throw e;
		}

		this.setName(name.replace(" ", "_"));

		// start video generation
		final int X_GAP = 5;
		final int Y_GAP = 15;
		final int MIN_WIDTH = 350;

		int trackNumber = 0;
		boolean isFirstFrame = true;
		int imageWidth = -1;
		int imageHeight = -1;
		int logoHeight = logo.getHeight();
		int videoWidth = -1;
		int videoHeight = -1;
		log.debug("Start generation of movie");
		for (LiveImageVO image : images) {
			try {

				BufferedImage bufImg = ImageIO.read(new File(imageDir + LiConstants.SEPARATOR
																	 + image.getResourceName()));

				if (isFirstFrame) {
					imageWidth = bufImg.getWidth();
					imageHeight = bufImg.getHeight();

					videoWidth = imageWidth < MIN_WIDTH ? MIN_WIDTH : imageWidth;
					videoHeight = 3 * Y_GAP + logoHeight + imageHeight + LegendConstants.LEGEND_HEIGHT;
					format = format.prepend(MediaTypeKey, FormatKeys.MediaType.VIDEO,//
											FrameRateKey, new Rational(1, 1), WidthKey, videoWidth, HeightKey, videoHeight);
					trackNumber = out.addTrack(format);
					isFirstFrame = false;

				}

				BufferedImage frame = new BufferedImage(videoWidth, videoHeight, BufferedImage.TYPE_INT_RGB);
				Graphics2D g = (Graphics2D) frame.getGraphics();
				g.setColor(Color.white);
				g.fillRect(0, 0, videoWidth, videoHeight);
				g.setFont(new Font("Arial", Font.PLAIN, 11));
				g.setColor(Color.BLACK);
				FontMetrics fm = g.getFontMetrics();

				// draw logo
				g.drawImage(logo, X_GAP, Y_GAP, Color.white, null);

				// draw live image
				if (bufImg.getHeight() != imageHeight || bufImg.getWidth() != imageWidth) {
					g.drawImage(bufImg.getScaledInstance(imageWidth, imageHeight, Image.SCALE_SMOOTH),
								(videoWidth - imageWidth) / 2, logoHeight + Y_GAP, Color.WHITE, null);
				} else {
					g.drawImage(bufImg, (videoWidth - imageWidth) / 2, logoHeight + Y_GAP, Color.WHITE, null);
				}

				// draw legend
				BufferedImage legendImage;
				if (unitSystem.equals("SI")) {
					legendImage = ImageIO.read(new File(imageDir + LiConstants.SEPARATOR
																+ image.getLegend().getSiResourcePath()));
				} else {
					legendImage = ImageIO.read(new File(imageDir + LiConstants.SEPARATOR
																+ image.getLegend().getResorcePath()));
				}
				g.drawString(legendLabel, X_GAP, 2 * Y_GAP + logoHeight + imageHeight + legendImage.getHeight() / 2);
				g.drawImage(legendImage, 2 * X_GAP + fm.stringWidth(legendLabel), 2 * Y_GAP + logoHeight + imageHeight,
							Color.WHITE, null);

				// draw name and time
				DateFormat dateformat = new SimpleDateFormat("dd MMM yyy HH:mm:ss");
				String timestamp = dateformat.format(image.getShowTime());

				int nameWidth = fm.stringWidth(name);
				int timestampWidth = fm.stringWidth(timestamp);
				// align
				int legendWidth = legendImage.getWidth() + fm.stringWidth(legendLabel);
				int infoLocation = Y_GAP;
				if ((legendWidth + nameWidth) < videoWidth || (legendWidth + timestampWidth) < videoWidth) {
					infoLocation = (videoHeight - Y_GAP - legendImage.getHeight()) + g.getFont().getSize();
				}
				if (nameWidth >= timestampWidth) {
					g.drawString(name, videoWidth - 2 * X_GAP - nameWidth, infoLocation);
					g.drawString(timestamp, videoWidth - 2 * X_GAP - nameWidth, infoLocation + g.getFont().getSize());
				} else {
					g.drawString(name, videoWidth - 2 * X_GAP - timestampWidth, infoLocation);
					g.drawString(timestamp, videoWidth - 2 * X_GAP - timestampWidth, infoLocation
							+ g.getFont().getSize());
				}

				g.dispose();
				out.write(trackNumber, frame, 1);
			} catch (IOException e) {
				log.warn("Could not add frame into video file. Frame skipped", e);
				throw e;
			}

		}
		log.debug("Generation of movie completed");
		try {
			out.finish();
			out.close();
		} catch (IOException e) {
			log.warn("Could not close video stream", e);
			throw e;
		}

		return;
	}

	public String getName() {
		return name;
	}

	public ImageOutputStream getImageOutputStream() {
		return stream;
	}

	public OutputStream getStream() throws IOException {
		return response.getOutputStream();
	}

	public void setName(String name) {
		this.name = name;
		updateHttpHeaders();
	}

	private void updateHttpHeaders() {
		String movieName = UrlEncoded.encodeString(name, "UTF-8");
		String disHeader;
		if (userAgent.toLowerCase().contains("firefox")) {
			disHeader = "Attachment; Filename*=\"utf-8'" + movieName + ".avi\"";
		} else {
			disHeader = "Attachment; Filename=\"" + movieName + ".avi\"";
		}
		response.setHeader("Content-Disposition", disHeader);
		response.addHeader("Pragma", "no-cache");
		response.addHeader("Cache-Control", "no-cache, no-store, max-age=0");
		response.addDateHeader("Expires", 1L);
	}

}
