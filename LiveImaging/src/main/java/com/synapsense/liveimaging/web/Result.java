package com.synapsense.liveimaging.web;

public class Result {

	private boolean result;

	public boolean isResult() {
		return result;
	}

	public void setResult(boolean result) {
		this.result = result;
	}

	public Result(boolean result) {
		this.result = result;
	}
}
