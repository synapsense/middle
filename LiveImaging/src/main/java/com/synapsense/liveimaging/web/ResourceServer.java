package com.synapsense.liveimaging.web;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.BindException;
import java.nio.file.Files;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.mortbay.jetty.Server;
import org.mortbay.jetty.handler.ContextHandler;
import org.mortbay.jetty.servlet.ServletHandler;
import org.mortbay.jetty.servlet.ServletHolder;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.web.context.support.XmlWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import com.synapsense.liveimaging.Complainer;
import com.synapsense.liveimaging.conf.LiConstants;

public class ResourceServer implements ApplicationContextAware {
	private static final String CROSSDOMAIN_LOCATION = "crossdomain.xml";
	private static final Log log = LogFactory.getLog(ResourceServer.class);

	private String outputDir = LiConstants.OUTPUT_FOLDER;
	private int port;
	private ApplicationContext applicationContext;
	private Server server;
	private Complainer complainer;

	public ResourceServer(String outputDir, int port) {
		super();
		this.port = port;
		this.outputDir = outputDir;
	}

	public void startServer() {
		log.info("Starting resource server on port " + port + ", directory " + outputDir);
		server = new Server(port);

		final ContextHandler context = new ContextHandler();
		context.setContextPath("/");
		context.setResourceBase(outputDir);
		server.setHandler(context);
		createCrossdomain();
		// configure child WebApplicationContext
		XmlWebApplicationContext webApplicationContext = new XmlWebApplicationContext();
		webApplicationContext.setParent(applicationContext);
		webApplicationContext.setConfigLocation("classpath:servlet-context.xml");

		final DispatcherServlet dispatcherServlet = new DispatcherServlet(webApplicationContext);

		final ServletHandler servletHandler = new ServletHandler();
		servletHandler.addServletWithMapping(new ServletHolder(dispatcherServlet), "/*");
		context.addHandler(servletHandler);

		try {
			server.start();
		} catch (BindException e) {
			complainer.complainPort(getPort());
			log.error("Unable to start resource server", e);
			// We still want LiveImaging to start even without the web
			// server
			// throw new RuntimeException(e);
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}

	}

	public void stop() throws Exception {
		server.stop();
		server.destroy();
	}

	public int getPort() {
		return port;
	}

	public void setComplainer(Complainer complainer) {
		this.complainer = complainer;
	}

	private void createCrossdomain() {
		File crossdomain = new File(outputDir, "crossdomain.xml");
		if (!crossdomain.exists()) {
			log.debug("crossdomain.xml doesn't exist.");
			try (InputStream is = getClass().getClassLoader().getResourceAsStream(CROSSDOMAIN_LOCATION)) {
				Files.copy(is, crossdomain.toPath());
			} catch (IOException e) {
				log.warn("Could not put crossdomain.xml into output folder", e);
			}
		}
	}

	public static void main(String[] args) throws Exception {
		ResourceServer rs = new ResourceServer(".", 8080);
		rs.startServer();
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
	}
}
