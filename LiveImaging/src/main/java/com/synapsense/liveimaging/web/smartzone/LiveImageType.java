package com.synapsense.liveimaging.web.smartzone;

/**
 * Created by WFI on 2/4/2016.
 */
public enum LiveImageType {
    TEMP_TOP(200, 131072),
    TEMP_MIDDLE(200, 16384),
    TEMP_BOTTOM(200, 2048),
    TEMP_SUBFLOOR(200, 256),
    TEMP_CONTROLLED(200, 128),
    HUMIDITY(201, 131072),
    PRESSURE(202, 256),
    DEW_POINT(998, 131072);

    private final Integer dataClass;
    private final Integer layer;

    LiveImageType(final Integer dataClass, final Integer layer) {
        this.dataClass = dataClass;
        this.layer = layer;
    }

    public Integer getDataClass() {
        return dataClass;
    }

    public Integer getLayer() {
        return layer;
    }
}
