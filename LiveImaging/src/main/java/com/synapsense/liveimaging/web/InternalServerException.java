package com.synapsense.liveimaging.web;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
public class InternalServerException extends RuntimeException {

	/**
     * 
     */
    private static final long serialVersionUID = 6915651916523186699L;

}
