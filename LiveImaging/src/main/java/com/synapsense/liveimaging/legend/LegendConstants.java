package com.synapsense.liveimaging.legend;

public interface LegendConstants {

	String LEGEND_TEMPERATURE = "200";
	String LEGEND_HUMIDITY = "201";
	String LEGEND_PRESSURE = "202";
	String LEGEND_DEWPOINT = "998";

	String[] LEGEND_DATACLASSES = new String[] { LEGEND_TEMPERATURE, LEGEND_HUMIDITY, LEGEND_PRESSURE, LEGEND_DEWPOINT };

	double REC_TEMPERATURE_MAX = 80.6;
	double ALL_TEMPERATURE_MAX = 90;
	double REC_TEMPERATURE_MIN = 64.4;
	double ALL_TEMPERATURE_MIN = 59;

	Double[] TEMPERATURE_THR = new Double[] { ALL_TEMPERATURE_MIN, REC_TEMPERATURE_MIN, REC_TEMPERATURE_MAX,
	        ALL_TEMPERATURE_MAX };

	double REC_HUMIDITY_MAX = 55;
	double ALL_HUMIDITY_MAX = 80;
	double REC_HUMIDITY_MIN = 40;
	double ALL_HUMIDITY_MIN = 20;

	Double[] HUMIDITY_THR = new Double[] { ALL_HUMIDITY_MIN, REC_HUMIDITY_MIN, REC_HUMIDITY_MAX, ALL_HUMIDITY_MAX };

	int LEGEND_WIDTH = 250;
	int LEGEND_HEIGHT = 24;
	int LEGEND_OFFSET = 25;
	int VALUES_WIDTH = LEGEND_WIDTH - 2 * LEGEND_OFFSET;
	int COLORED_HEIGHT = (LEGEND_HEIGHT / 2 - 2);

}
