package com.synapsense.liveimaging.legend;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.liveimaging.data.ColorMap;
import com.synapsense.liveimaging.data.Converter;
import com.synapsense.liveimaging.storage.ImageDataStorageWriter;
import com.synapsense.liveimaging.storage.LegendImageVO;

public class LegendGenerator {

	private static final Log log = LogFactory.getLog(LegendGenerator.class);

	private static String LEGEND_FOLDER = "legends";

	private ImageDataStorageWriter cmpWriter;

	public LegendGenerator(ImageDataStorageWriter writer) {
		this.cmpWriter = writer;
	}

	public void generateLegendImage(ColorMap colorMap, Date genTime) throws Exception {
		log.info("Generating legend images for dataclass: " + colorMap.getType());
		BufferedImage usImage = paintLegend(colorMap, genTime);
		BufferedImage siImage = usImage;
		if (colorMap.getType().equals(LegendConstants.LEGEND_TEMPERATURE)
		        || colorMap.getType().equals(LegendConstants.LEGEND_DEWPOINT)) {
			ColorMap siTempr = new ColorMap(colorMap, new Converter() {

				@Override
				public String getName() {
					return "si";
				}

				@Override
				public double convert(double source) {
					return (source - 32) * 5 / 9;
				}
			});
			siImage = paintLegend(siTempr, genTime);
		} else if (colorMap.getType().equals(LegendConstants.LEGEND_PRESSURE)) {
			ColorMap siPressure = new ColorMap(colorMap, new Converter() {

				@Override
				public String getName() {
					return "si";
				}

				@Override
				public double convert(double source) {
					return source * 249.089;
				}
			});
			siImage = paintLegend(siPressure, genTime);
		}
		LegendImageVO legend = new LegendImageVO(getLegendImagePath(colorMap.getType(), genTime),
		        Integer.parseInt(colorMap.getType()), genTime, usImage, siImage);
		cmpWriter.saveLegend(legend);
	}

	private BufferedImage paintLegend(ColorMap c, Date date) throws IOException {
		BufferedImage bi = new BufferedImage(LegendConstants.LEGEND_WIDTH, LegendConstants.LEGEND_HEIGHT,
		        BufferedImage.TYPE_INT_ARGB);

		Graphics2D g = (Graphics2D) bi.getGraphics();

		// Legend and max
		double min = c.getMinPoint();
		double max = c.getMaxPoint();

		g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_LCD_HRGB);

		// Transparent color
		g.setColor(new Color(0x00FFFFFF & Color.white.getRGB(), true));

		g.fill(new Rectangle2D.Double(0, LegendConstants.COLORED_HEIGHT + 1, LegendConstants.LEGEND_WIDTH,
		        LegendConstants.LEGEND_HEIGHT));

		for (int x = 0; x < LegendConstants.LEGEND_WIDTH; x++) {
			// Draw correspondingly colored line
			Color color = c.getColorAt(scaleLegendPoint(min, max, x));
			g.setPaint(color);
			Line2D line = new Line2D.Double(x, 0, x, LegendConstants.COLORED_HEIGHT);
			g.draw(line);
		}

		// Draw recommended thresholds
		g.setColor(Color.black);
		for (double thr : c.getAshrae()) {
			int x = revScaleLegendPoint(min, max, thr);
			g.draw(new Line2D.Double(x, 0, x, LegendConstants.COLORED_HEIGHT));
		}

		// Draw legend values. For temperature it is necessary to create 2
		// pictures
		int step = LegendConstants.VALUES_WIDTH / c.getCaptures();
		DecimalFormat zFormatter = (new DecimalFormat("0.##"));
		DecimalFormat cFormatter = (new DecimalFormat("0"));
		g.setFont(new Font("Lucida", Font.BOLD, 9));
		for (int x = LegendConstants.LEGEND_OFFSET; x <= LegendConstants.LEGEND_OFFSET + LegendConstants.VALUES_WIDTH; x += step) {
			g.draw(new Line2D.Double(x, LegendConstants.COLORED_HEIGHT + 1, x, LegendConstants.LEGEND_HEIGHT - 4));
			double pos = scaleLegendPoint(min, max, x);
			String toDraw = max < 10 ? zFormatter.format(pos) : cFormatter.format(pos);
			g.drawString(toDraw, x + 2, LegendConstants.LEGEND_HEIGHT - 4);
		}

		return bi;
	}

	private String getLegendImagePath(String type, Date date) {
		DateFormat formatter = new SimpleDateFormat("MM.dd.yyyy_hh.mm");
		return LEGEND_FOLDER + "/legend_" + type + "_" + formatter.format(date) + ".png";
	}

	private double scaleLegendPoint(double min, double max, int x) {
		return (max - min) * ((double) (x - LegendConstants.LEGEND_OFFSET) / (double) LegendConstants.VALUES_WIDTH)
		        + min;
	}

	private int revScaleLegendPoint(double min, double max, double x) {
		return new Double(LegendConstants.VALUES_WIDTH * ((x - min) / (max - min)) + LegendConstants.LEGEND_OFFSET)
		        .intValue();
	}
}
