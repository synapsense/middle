package com.synapsense.liveimaging.legend;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URLDecoder;
import java.nio.channels.FileChannel;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.liveimaging.conf.LiConstants;
import com.synapsense.liveimaging.data.ColorMap;
import com.synapsense.liveimaging.data.ColorMaps;

public class ColorManager {

	private static final Log log = LogFactory.getLog(ColorManager.class);

	private LegendGenerator legendMan;

	private static String COLOR_MAP_FILE_NAME = "colormap.xml";
	private static String BACK_UP_FILE_NAME = "colormap.backup";
	private static String SCHEMA_FILE_NAME = "legend-config.xsd";
	private File color_file;

	private ColorMaps colormaps;

	private Marshaller marshaller;
	private Unmarshaller unmarshller;

	public ColorManager(LegendGenerator manager) throws Exception {
		this.legendMan = manager;
		validateXml(COLOR_MAP_FILE_NAME);
		color_file = new File(URLDecoder.decode(getClass().getClassLoader().getResource(COLOR_MAP_FILE_NAME).getFile(),
		        "UTF-8"));
		JAXBContext jc = JAXBContext.newInstance(ColorMaps.class);
		marshaller = jc.createMarshaller();
		unmarshller = jc.createUnmarshaller();
		loadColorMap();
		generateLegends();
	}

	public ColorMap getColorMap(String name) {
		return colormaps.getColorMap(name);
	}

	public Collection<ColorMap> getColorMap() {
		return colormaps.getColormaps();
	}

	private void loadColorMap() throws JAXBException {
		log.info("Loading of color model");
		colormaps = (ColorMaps) unmarshller.unmarshal(color_file);
	}

	public void updateColorModel(ColorMaps colorModel) throws Exception {
		log.info("Updating of color model");
		createBackUpFile();
		synchronized (colormaps) {
			try {
				colormaps.merge(colorModel);
				saveColorModel();
				generateLegends(colorModel);
			} catch (IllegalArgumentException e) {
				throw e;
			} catch (Exception e) {
				try {
					loadBackUpFile();
					colormaps = (ColorMaps) unmarshller.unmarshal(color_file);
					generateLegends();
				} catch (Exception e1) {
					throw new Exception(
					        "Fatal Error. Could not update colors model. Could not restore from previous model", e);
				}
				throw new Exception("Could not update colors model. Restored previous", e);
			}
		}
		log.info("Color model successfully updated");

	}

	private void validateXml(String fileName) throws Exception {
		SchemaFactory schemaFactory = SchemaFactory.newInstance(javax.xml.XMLConstants.W3C_XML_SCHEMA_NS_URI);
		Schema schema = schemaFactory.newSchema(ColorManager.class.getClassLoader().getResource(SCHEMA_FILE_NAME));
		InputStream is = ColorManager.class.getClassLoader().getResource(fileName).openStream();
		try {
			schema.newValidator().validate(new StreamSource(is));
		} finally {
			is.close();
		}

	}

	private void generateLegends() throws Exception {
		generateLegends(colormaps);
	}

	private void generateLegends(ColorMaps colormaps) throws Exception {
		List<ColorMap> maps = colormaps.getColormaps();
		for (ColorMap map : maps) {
			generateLegend(map);
		}
	}

	private void generateLegend(ColorMap model) throws Exception {
		legendMan.generateLegendImage(model, new Date());
	}

	private void saveColorModel() throws JAXBException {
		marshaller.marshal(colormaps, color_file);
	}

	private void loadBackUpFile() throws Exception {
		File backup = new File(color_file.getParent() + LiConstants.SEPARATOR + BACK_UP_FILE_NAME);
		copyFile(backup, color_file);
	}

	private void createBackUpFile() throws Exception {
		File backup = new File(color_file.getParentFile(), BACK_UP_FILE_NAME);
		backup.createNewFile();
		copyFile(color_file, backup);

	}

	private void copyFile(File from, File to) throws Exception {
		FileInputStream fis = new FileInputStream(from);
		FileOutputStream fos = new FileOutputStream(to);
		try {
			FileChannel fileChanInput = fis.getChannel();
			FileChannel fileChanOutput = fos.getChannel();
			try {
				fileChanInput.transferTo(0, fileChanInput.size(), fileChanOutput);
			} finally {
				fileChanInput.close();
				fileChanOutput.close();
			}
		} finally {
			fis.close();
			fos.close();
		}
	}
}
