package com.synapsense.liveimaging.es;

import java.util.Properties;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.service.AlertService;
import com.synapsense.service.Environment;
import com.synapsense.service.LocalizationService;
import com.synapsense.service.UnitSystemsService;
import com.synapsense.service.nsvc.NotificationService;
import com.synapsense.transport.Proxy;
import com.synapsense.transport.TransportManager;
import com.synapsense.util.ServerLoginModule;

public class ESContext {

	private static final Log log = LogFactory.getLog(ESContext.class);

	private static final String SynapEnvConfFile = "escontext.properties";

	private Properties connectProperties = null;

	private Environment env = null;
	private InitialContext ctx = null;
	private AlertService alertService = null;
	private Proxy<NotificationService> nsvcProxy;
	private UnitSystemsService unitSystemsService = null;
	private LocalizationService localizationService = null;

	public ESContext() {
		connectProperties = new Properties();
		try {
			connectProperties.load(ESContext.class.getClassLoader().getResourceAsStream(SynapEnvConfFile));
		} catch (Exception e) {
			log.fatal("Cannot load \"" + SynapEnvConfFile + "\" file", e);
			throw new RuntimeException("Cannot load \"" + SynapEnvConfFile + "\" file");
		}
	}

	public Environment getEnv() {
		return env;
	}

	public AlertService getAlertService() {
		return alertService;
	}

	public NotificationService getNsvc() {
		return nsvcProxy.getService();
	}

	public Proxy<NotificationService> getNsvcProxy() {
		return nsvcProxy;
	}

	public UnitSystemsService getUnitSystemsService() {
		return unitSystemsService;
	}

	public LocalizationService getLocalizationService() {
		return localizationService;
	}

	public synchronized boolean connect() {
		if (ctx == null) {
			try {
				ctx = new InitialContext(connectProperties);
			} catch (NamingException e) {
				log.error(e.getLocalizedMessage(), e);
				return false;
			}
		}

		if (nsvcProxy == null) {
			nsvcProxy = TransportManager.getProxy(connectProperties, NotificationService.class);
		}

		try {
			if (env == null) {
				env = ServerLoginModule.getService(ctx, "Environment", Environment.class);
			}

			if (alertService == null) {
				alertService = ServerLoginModule.getService(ctx, "AlertService", AlertService.class);
			}

			if (unitSystemsService == null) {
				unitSystemsService = ServerLoginModule.getService(ctx, "UnitSystemsServiceImpl",
				        UnitSystemsService.class);
			}
			if (localizationService == null) {
				localizationService = ServerLoginModule.getService(ctx, "LocalizationService",
				        LocalizationService.class);
			}
		} catch (NamingException e) {
			log.error(e.getLocalizedMessage(), e);
			disconnect();
			return false;
		}

		return true;
	}

	public synchronized void disconnect() {
		log.info("Disconnecting from ES...");
		try {
			if (ctx != null) {
				try {
					ctx.close();
				} catch (NamingException e) {
					log.warn("Failed to close initial context due to ", e);
				}
			}

			if (nsvcProxy != null && nsvcProxy.isConnected()) {
				nsvcProxy.release();
			}

		} finally {
			alertService = null;
			env = null;
			unitSystemsService = null;
			localizationService = null;
			ctx = null;
			nsvcProxy = null;
		}
	}
}
