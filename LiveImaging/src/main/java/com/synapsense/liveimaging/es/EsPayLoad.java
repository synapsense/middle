package com.synapsense.liveimaging.es;

import java.awt.image.BufferedImage;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import com.synapsense.dto.TO;
import com.synapsense.liveimaging.data.ColorMap;
import com.synapsense.liveimaging.data.PayLoad;
import com.synapsense.liveimaging.draw.Area;
import com.synapsense.liveimaging.draw.EValuePoint;
import com.synapsense.liveimaging.draw.LiveImage;
import com.synapsense.liveimaging.draw.Point2F;
import com.synapsense.liveimaging.draw.RectangleArea;
import com.synapsense.liveimaging.draw.ValuePoint;

class EsPayLoad implements PayLoad {

	private RectangleArea imageArea;

	private BufferedImage background;

	private String imageId;

	private Date showTime;

	private String colMapType;

	private ColorMap colorMap;

	private HashSet<Point2F> uniquePoints = new HashSet<Point2F>();

	private HashMap<TO<?>, ValuePoint> sensePoints = new HashMap<TO<?>, ValuePoint>();

	private double x;
	private double y;
	private double regionWidth;
	private double regionHeight;

	public EsPayLoad(double x, double y, double regionWidth, double regionHeight, String imageId, String colorMapType,
	        BufferedImage background) {
		this.x = x;
		this.y = y;
		this.regionWidth = regionWidth;
		this.regionHeight = regionHeight;
		this.imageId = imageId;
		this.background = background;
        this.colMapType = colorMapType;
		imageArea = new RectangleArea(0.0f, 0.0f, background.getWidth() - 1, background.getHeight() - 1);
	}

	public EsPayLoad(EsPayLoad other, Date newShowTime) {
		this.imageArea = new RectangleArea(other.imageArea);
		this.background = other.background;
		this.imageId = other.imageId;
		this.showTime = newShowTime;
		this.colMapType = other.colMapType;
		this.colorMap = other.colorMap;
		this.x = other.x;
		this.y = other.y;
		this.regionWidth = other.regionWidth;
		this.regionHeight = other.regionHeight;
		this.uniquePoints.addAll(other.uniquePoints);
		this.sensePoints.putAll(other.sensePoints);
	}

    @Override
	public String getImageId() {
		return imageId;
	}

	@Override
	public LiveImage getImage() {
		return imageArea;
	}

	@Override
	public BufferedImage getBackground() {
		return background;
	}

	@Override
	public ColorMap getColorMap() {
		return colorMap;
	}

	@Override
	public int getNbPoints() {
		return uniquePoints.size();
	}

    @Override
    public boolean addPoint(TO<?> sensor, double x, double y, float value) {
        EValuePoint point = new EValuePoint((float) ((x - this.getX())
                * this.getWidth() / this.getRegionWidth()),
                (float) ((y - this.getY()) * this.getHeight() / this
                        .getRegionHeight()), value);

        return addPoint(sensor, point);
    }

    public void nest(Area area) {
		imageArea.nest(area);
	}

	private boolean addPoint(TO<?> sensor, EValuePoint valuePoint) {
		if (!uniquePoints.add(valuePoint.getCoord())) {
			return false;
		}
		if (imageArea.inArea(valuePoint.getX(), valuePoint.getY())) {
			sensePoints.put(sensor, valuePoint);
			imageArea.addSampleValue(valuePoint);
		} else {
			EsDataSource.log.warn("Sensor " + sensor + " coordinates are out of bounds, skipping...");
		}
		return true;
	}

	// Doesn't create new point object
	public boolean updatePointValue(TO<?> sensor, double value) {
		ValuePoint valuePoint = sensePoints.get(sensor);
		if (valuePoint == null) {
			return false;
		}
		if (EsDataSource.log.isTraceEnabled()) {
			EsDataSource.log.trace("Updating point " + valuePoint + ": new value - " + value);
		}
		valuePoint.setValue((float)value);
		return true;
	}

	public int getWidth() {
		return background.getWidth();
	}

	public int getHeight() {
		return background.getHeight();
	}

	public String getColMapType() {
		return colMapType;
	}

	public void setColorMap(ColorMap colorMap) {
		this.colorMap = colorMap;
	}

	public Date getShowTime() {
		return showTime;
	}

	public void setShowTime(Date showTime) {
		this.showTime = showTime;
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	public double getRegionHeight() {
		return regionHeight;
	}

	public double getRegionWidth() {
		return regionWidth;
	}

}