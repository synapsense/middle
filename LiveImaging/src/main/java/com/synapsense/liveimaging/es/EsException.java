package com.synapsense.liveimaging.es;

public class EsException extends Exception {

	private static final long serialVersionUID = 8127641490644315818L;

	public EsException() {
	}

	public EsException(String arg0) {
		super(arg0);
	}

	public EsException(Throwable arg0) {
		super(arg0);
	}

	public EsException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}
}
