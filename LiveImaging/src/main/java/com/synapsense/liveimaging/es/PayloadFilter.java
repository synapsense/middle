package com.synapsense.liveimaging.es;

import java.util.Collection;
import java.util.HashSet;

import com.synapsense.dto.TO;
import com.synapsense.liveimaging.conf.LiConstants;
import com.synapsense.liveimaging.web.ImageType;

public interface PayloadFilter {

	boolean accept(TO<?> objId, int dataclass, int layers);
	
	boolean accept(TO<?> dcTO, TO<?> roomTO);
}

class HistPayloadFilter implements PayloadFilter {

	private Collection<TO<?>> objIds;

	private Collection<ImageType> imageTypes;

	public HistPayloadFilter(Collection<TO<?>> objIds, Collection<ImageType> imageTypes) {
		this.objIds = objIds;
		this.imageTypes = imageTypes;
	}

	public boolean accept(TO<?> objId, int dataclass, int layer) {
		if (objIds.contains(objId) && imageTypes.contains(new ImageType(dataclass, layer))) {
			return true;
		}
		return false;
	}

	@Override
	public boolean accept(TO<?> dcTO, TO<?> roomTO) {
		if (objIds.contains(dcTO) || objIds.contains(roomTO)) {
			return true;
		}
		return false;
	}
}

class RtPayloadFilter implements PayloadFilter {

	private HashSet<String> levels;

	public RtPayloadFilter(HashSet<String> levels) {
		this.levels = levels;
	}

	@Override
	public boolean accept(TO<?> objId, int dataclass, int layers) {
		if (objId.getTypeName().equalsIgnoreCase(LiConstants.DC)) {
			return levels.contains(LiConstants.DC);
		} else if (objId.getTypeName().equalsIgnoreCase(LiConstants.ROOM)) {
			return levels.contains(LiConstants.ROOM);
		}
		return false;
	}

	@Override
	public boolean accept(TO<?> dcTO, TO<?> roomTO) {
		return true;
	}

}
