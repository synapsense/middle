package com.synapsense.liveimaging.es;

import java.io.*;
import java.net.InetAddress;
import java.util.Collection;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.synapsense.dto.CollectionTO;
import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.EnvException;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.liveimaging.LiveImagingApp;
import com.synapsense.liveimaging.conf.ConfigurationException;
import com.synapsense.service.Environment;
import com.synapsense.service.nsvc.events.DispatchingProcessor;
import com.synapsense.service.nsvc.events.PropertiesChangedEvent;
import com.synapsense.service.nsvc.events.PropertiesChangedEventProcessor;
import com.synapsense.service.nsvc.filters.PropertiesChangedEventFilter;
import com.synapsense.service.nsvc.filters.TOMatcher;

public class EsConfigurationLoader {

	public static final String OBJECT_NAME = "LiveImaging";
	public static final String OBJECT_TYPE = "CONFIGURATION_DATA";
	public static final String NAME_PROPERTY = "name";
	public static final String CONFIG_PROPERTY = "config";
	public static final String CONF_FILE = "conf.xml";

	private static final Log log = LogFactory.getLog(EsConfigurationLoader.class);
	private static final String SCHEMA_FILE_NAME = "li-config.xsd";

	private DispatchingProcessor processor;

	// EsConfigLoader has it's own es context since it's started before the rest
	// of the application
	private ESContext esContext;

	public EsConfigurationLoader(LiveImagingApp app) {
		esContext = new ESContext();
		esContext.connect();
		processor = new DispatchingProcessor().putPcep(new LiConfigurationChangedEventProcessor(app));
	}

	public String getConfig() throws ConfigurationException {
		Environment env = esContext.getEnv();

		Collection<TO<?>> configs = env.getObjects(OBJECT_TYPE,
		        new ValueTO[] { new ValueTO(NAME_PROPERTY, OBJECT_NAME) });
		TO<?> objId = null;
		String config = null;

		if (configs.isEmpty()) {
			log.info("LiveImaging configuration doesn't exist on ES yet, uploading default configuration from file...");
			// The first time, loading default configuration
			try {
				config = readConfigFile();
				config = setHostName(config);
			} catch (IOException e) {
				throw new ConfigurationException("Unable to read default configuration file", e);
			}

			try {
				objId = env.createObject(OBJECT_TYPE, new ValueTO[] { new ValueTO(NAME_PROPERTY, OBJECT_NAME),
				        new ValueTO(CONFIG_PROPERTY, config) });
			} catch (EnvException e) {
				throw new ConfigurationException("Unable to upload default configuration file to ES", e);
			}
		} else if (configs.size() > 1) {
			log.warn("Multiple LiveImaging configuration objects have been found in ES, will use the latest one");
			long latestTimestamp = 0;
			Collection<CollectionTO> allConfigs = env.getPropertyValue(configs, new String[] { CONFIG_PROPERTY });
			for (CollectionTO conf : allConfigs) {
				ValueTO confVTO = conf.getSinglePropValue(CONFIG_PROPERTY);
				if (confVTO.getTimeStamp() > latestTimestamp) {
					objId = conf.getObjId();
					config = (String) confVTO.getValue();
					latestTimestamp = confVTO.getTimeStamp();
				}
			}
			// delete the others
			for (TO<?> other : configs) {
				if (!other.equals(objId)) {
					try {
						env.deleteObject(other);
					} catch (ObjectNotFoundException e) {
						log.error("Unable to delete configuration file from ES", e);
					}
				}
			}
		} else {
			log.info("Found single config file on ES");
			objId = configs.iterator().next();
			try {
				config = env.getPropertyValue(objId, CONFIG_PROPERTY, String.class);
			} catch (EnvException e) {
				throw new ConfigurationException("Unable to get configuration from ES");
			}
		}

		registerNsvcProcessor(objId);

		try {
			if (config == null) {
				config = env.getPropertyValue(objId, CONFIG_PROPERTY, String.class);
			}
			try {
				validateConfig(config);
			} catch (SAXException e) {
				try {
					String updLiConfigText = updateConfig(config);
					if (!updLiConfigText.equals(config)) {
						config = updLiConfigText;
						saveESConfig(config);
						return config;
					}
				} catch (IOException e1) {
					throw new ConfigurationException("Unable to update configuration on ES.", e1);
				}
			} catch (IOException e) {
				throw new ConfigurationException("Unable to validate configuration.", e);
			}
			return config;

		} catch (EnvException e) {
			throw new ConfigurationException("Unable to fetch configuration from ES...", e);
		}
	}

	private void validateConfig(String liConfigText) throws SAXException, IOException {
		SchemaFactory schemaFactory = SchemaFactory.newInstance(javax.xml.XMLConstants.W3C_XML_SCHEMA_NS_URI);
		Schema schema = schemaFactory.newSchema(EsConfigurationLoader.class.getClassLoader().getResource(
		        SCHEMA_FILE_NAME));
		schema.newValidator().validate(new StreamSource(new StringReader(liConfigText)));
	}

	public String updateConfig(String configText) throws IOException {
		Document doc = parseConfig(configText);

		if (doc != null) {
			String xmlns = doc.getDocumentElement().getAttribute("xmlns:li");
			if (xmlns.indexOf("synapviz") != -1) {
				doc.getDocumentElement().setAttribute("xmlns:li",
				        doc.getDocumentElement().getAttribute("xmlns:li").replaceAll("synapviz", "liveimaging"));

				Element histGen = doc.createElement("li:historical-generator");

				Element histExpTrigger = doc.createElement("li:hist-expire-trigger");
				histExpTrigger.setTextContent("0 0 0/24 * * ?");
				histGen.appendChild(histExpTrigger);

				Element histKeepPeriod = doc.createElement("li:hist-keep-period");
				histKeepPeriod.setTextContent("24");
				histGen.appendChild(histKeepPeriod);

				doc.getDocumentElement().appendChild(histGen);

				NodeList obsoleteNodes = doc.getElementsByTagName("li:interpolator");
				while (obsoleteNodes.getLength() != 0) {
					for (int i = 0; i < obsoleteNodes.getLength(); i++) {
						doc.getDocumentElement().removeChild(obsoleteNodes.item(i));
					}
					obsoleteNodes = doc.getElementsByTagName("li:interpolator");
				}
			}
			setHostName(doc, InetAddress.getLocalHost().getHostName());
			configText = convertDocument(doc);
		}
		return configText;
	}

	private String setHostName(String config) throws IOException {
		Document doc = parseConfig(config);

		if (doc != null) {
			setHostName(doc, InetAddress.getLocalHost().getHostName());
			config = convertDocument(doc);
		}

		return config;
	}

	private Document setHostName(Document doc, String hostName) throws IOException {

		NodeList hostNameNode = doc.getElementsByTagName("li:webserver-ip");
		if (hostNameNode.getLength() != 1)
			log.error("Unexpected number of 'webserver-ip' tag in config: " + hostNameNode.getLength());

		if (hostNameNode.item(0).getTextContent().equals("localhost")) {
			hostNameNode.item(0).setTextContent(hostName);
		}
		return doc;
	}

	private Document parseConfig(String config) {
		Document doc = null;

		try {
			doc = DocumentBuilderFactory.newInstance().newDocumentBuilder()
			        .parse(new ByteArrayInputStream(config.getBytes()));
		} catch (Exception e) {
			log.error("Unable to convert config due to ", e);
		}
		return doc;
	}

	private String convertDocument(Document doc) throws IOException {
		String config = null;
		Transformer transf;
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		try {
			transf = TransformerFactory.newInstance().newTransformer();
			Source src = new DOMSource(doc);
			Result dest = new StreamResult(os);
			transf.transform(src, dest);
			config = os.toString();

			// remove trailing whitespaces and empty lines
			config = config.replaceAll("(?m)^[ \t]*\r?\n", "");
		} catch (TransformerException e) {
			log.error("Unable to convert modified config ", e);
		} finally {
			os.close();
		}
		return config;
	}

	private void saveESConfig(String config) {
		Environment env = esContext.getEnv();
		Collection<TO<?>> configs = env.getObjects(OBJECT_TYPE,
		        new ValueTO[] { new ValueTO(NAME_PROPERTY, OBJECT_NAME) });
		if (configs.isEmpty()) {
			try {
				configs.add(env.createObject(OBJECT_TYPE, new ValueTO[] { new ValueTO(NAME_PROPERTY, OBJECT_NAME),
				        new ValueTO(CONFIG_PROPERTY, config) }));
			} catch (EnvException e) {
				throw new ConfigurationException("Unable to save updated configuration file to ES", e);
			}
		} else {
			if (configs.size() > 1) {
				throw new ConfigurationException("Multiple LiveImaging configuration objects have been found in ES");
			}
			TO<?> objId = configs.iterator().next();

			try {
				env.setPropertyValue(objId, CONFIG_PROPERTY, config);
			} catch (EnvException e) {
				throw new ConfigurationException("Unable to save updated configuration file to ES", e);
			}
		}
	}

	public void deleteConfigFromEs() throws EnvException {
		Environment env = esContext.getEnv();
		Collection<TO<?>> configs = env.getObjects(OBJECT_TYPE,
		        new ValueTO[] { new ValueTO(NAME_PROPERTY, OBJECT_NAME) });
		for (TO<?> config : configs) {
			env.deleteObject(config);
		}
	}

	public void resetConfigInEs() throws EnvException {
		Environment env = esContext.getEnv();
		Collection<TO<?>> configs = env.getObjects(OBJECT_TYPE,
		        new ValueTO[] { new ValueTO(NAME_PROPERTY, OBJECT_NAME) });

		if (!configs.isEmpty()) {
			TO<?> objId = configs.iterator().next();
			env.setPropertyValue(objId, CONFIG_PROPERTY, env.getPropertyValue(objId, CONFIG_PROPERTY, Object.class));
		}
	}

	public static String readConfigFile() throws IOException {
		StringBuilder sb = new StringBuilder();
		InputStream is = EsConfigurationLoader.class.getClassLoader().getResourceAsStream(CONF_FILE);
		if (is == null) {
			throw new ConfigurationException("Config file [" + CONF_FILE + "] is not found in the classpath.");
		}
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		try {
			String strLine;
			while ((strLine = reader.readLine()) != null) {
				sb.append(strLine);
			}
		} finally {
			reader.close();
		}
		return sb.toString();
	}

	private void registerNsvcProcessor(TO<?> objId) {
		esContext.getNsvc().registerProcessor(
		        processor,
		        new DispatchingProcessor().putPcep(new PropertiesChangedEventFilter(new TOMatcher(objId),
		                new String[] { CONFIG_PROPERTY })));
	}

	public void setEsContext(ESContext esContext) {
		this.esContext = esContext;
	}
}

class LiConfigurationChangedEventProcessor implements PropertiesChangedEventProcessor {

	private static final Log log = LogFactory.getLog(EsConfigurationLoader.class);

	private LiveImagingApp app;

	public LiConfigurationChangedEventProcessor(LiveImagingApp app) {
		this.app = app;
	}

	@Override
	public PropertiesChangedEvent process(PropertiesChangedEvent e) {
		// In case LI configuration has changed I just restart the application
		log.info("LiveImaging configuration has been updated, reconfiguring application ...");
		app.stopLiveImaging();
		try {
			app.startLiveImaging();
		} catch (InterruptedException e2) {
			log.info("Interrupted while starting LiveImaging.");
		} catch (Exception e2) {
			log.error("LiveImaging failed to start.", e2);
		}
		log.info("LiveImaging configuration has been processed.");
		return null;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((app == null) ? 0 : app.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LiConfigurationChangedEventProcessor other = (LiConfigurationChangedEventProcessor) obj;
		if (app == null) {
			if (other.app != null)
				return false;
		} else if (!app.equals(other.app))
			return false;
		return true;
	}
}
