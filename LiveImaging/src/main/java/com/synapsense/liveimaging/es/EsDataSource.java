package com.synapsense.liveimaging.es;

import com.google.common.base.Function;
import com.panduit.sz.util.concurrent.PExecutors;
import com.synapsense.dto.BinaryData;
import com.synapsense.dto.ChangedValueTO;
import com.synapsense.dto.CollectionTO;
import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.EnvException;
import com.synapsense.liveimaging.Complainer;
import com.synapsense.liveimaging.conf.LiConstants;
import com.synapsense.liveimaging.data.DataSource;
import com.synapsense.liveimaging.data.DataSourceException;
import com.synapsense.liveimaging.data.EsPropertyLoader;
import com.synapsense.liveimaging.data.PayLoad;
import com.synapsense.liveimaging.data.PayLoadCollection;
import com.synapsense.liveimaging.draw.Area;
import com.synapsense.liveimaging.draw.ImageUtil;
import com.synapsense.liveimaging.legend.ColorManager;
import com.synapsense.liveimaging.web.ImageType;
import com.synapsense.service.Environment;
import com.synapsense.service.impl.dao.to.PropertyTO;
import com.synapsense.service.nsvc.EventProcessor;
import com.synapsense.service.nsvc.NotificationService;
import com.synapsense.service.nsvc.events.ConfigurationCompleteEvent;
import com.synapsense.service.nsvc.events.ConfigurationCompleteEventProcessor;
import com.synapsense.service.nsvc.events.DispatchingProcessor;
import com.synapsense.service.nsvc.events.PropertiesChangedEvent;
import com.synapsense.service.nsvc.events.PropertiesChangedEventProcessor;
import com.synapsense.service.nsvc.events.ReferencedPropertyChangedEvent;
import com.synapsense.service.nsvc.events.ReferencedPropertyChangedEventProcessor;
import com.synapsense.service.nsvc.filters.EventTypeFilter;
import com.synapsense.service.nsvc.filters.PropertiesChangedEventFilter;
import com.synapsense.service.nsvc.filters.ReferencedPropertyChangedEventFilter;
import com.synapsense.service.nsvc.filters.TOMatcher;
import com.synapsense.transport.NotConnectedException;
import com.synapsense.transport.Proxy;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;

public class EsDataSource implements DataSource, EsPropertyLoader {
	private static final long THRESHOLD = 1000 * 60 * 60 * 12; // 12 hrs

	static final Log log = LogFactory.getLog(EsDataSource.class);

	enum ImageSize {
	PX900(900, 600), PX1800(1800, 1200);

	private final int width;
	private final int height;

	private ImageSize(int width, int height) {
		this.width = width;
		this.height = height;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	}

	private HashSet<String> levels = new HashSet<String>();

	private ColorManager colorManager;

	protected Complainer complainer;
	public void setComplainer(Complainer complainer) {
		this.complainer = complainer;
	}

	private ESContext esContext;

	private static String[] roomPropNames = new String[] { "x", "y", "width", "height" };

	String[] sensorPropNames = new String[] { "x", "y", "dataclass", "z", "lastValue", "status" };

	// Payloads for runtime processing
	private final Map<String, PayLoad> rtPayloads = Collections.synchronizedMap(new HashMap<String, PayLoad>());

	// Payloads for lite image generation on demand
	private final Map<String, PayLoad> rtLitePayloads = Collections.synchronizedMap(new HashMap<String, PayLoad>());

	// temp cache for historical values
	// TODO : it is reused on each historical generation request
	// thanks God such requests are executed in a single thread!
	private Map<TO<?>, HashMap<String, TreeMap<Date, Object>>> histCache = new HashMap<>();

	private HashSet<TO<?>> sensorsWithCOINs = new HashSet<TO<?>>();

	// cache to hold mapping from SmartZone Id to ES Object
	private Map<Integer, TO<?>> smartZoneIdToESObj = new HashMap<Integer, TO<?>>();

	private int drawThreshold = 2;

	private String separator = LiConstants.SEPARATOR;

	private boolean firstCall = true;

	private Invalidatable toInvalidate;

	private volatile boolean isValid = false;

	private NsvcLifeCycleListener nsvcLifeCycleListener = new NsvcLifeCycleListener();

	private class NsvcLifeCycleListener implements Proxy.LifecycleListener {

		@Override
		public void disconnected(Proxy<?> proxy) {
			if (log.isDebugEnabled()) {
				log.debug("Disconnected from Notification service, invalidating data source");
			}
			invalidate();
			log.warn("Disconnected from Notification service, reconnecting...");
			try {
				proxy.connect();
			} catch (NotConnectedException e) {
				log.warn("Unable to connect to Notification service", e);
			}
		}

		@Override
		public void connected(Proxy<?> proxy) {
			// As we still might have lost some notifications before got
			// reconnected
			invalidate();
			log.info("Connection to Notification service has been established");
		}

	}

	// scale image functions
	private static final class ImageScaleFunction implements Function<BinaryData, BufferedImage> {
		static ImageScaleFunction SCALE_TO_LITE_SIZE = new ImageScaleFunction(ImageSize.PX900);
		static ImageScaleFunction SCALE_NORMAL_SIZE = new ImageScaleFunction(ImageSize.PX1800);

		static ImageScaleFunction choose(ImageSize size) {
			switch (size) {
			case PX1800:
				return SCALE_NORMAL_SIZE;
			case PX900:
				return SCALE_TO_LITE_SIZE;
			default:
				return SCALE_NORMAL_SIZE;
			}
		}

		@Override
		public BufferedImage apply(BinaryData input) {
			try {
				return scaleBackgroundImage(input, imageSize == ImageSize.PX900);
			} catch (IOException e) {
				throw new DataSourceException("Failed to re-scale image to size : " + imageSize, e);
			}
		}

		private ImageScaleFunction(ImageSize imgSize) {
			this.imageSize = imgSize;
		}

		private final ImageSize imageSize;

		/** Returns null if the image is null or corrupted */
		private BufferedImage scaleBackgroundImage(BinaryData data, boolean lite) throws IOException {
			if (data == null) return null; // Handle missing image
			BufferedImage image = ImageIO.read(new ByteArrayInputStream(data.getValue()));
			// Handle corrupted image
			if (image == null) {
				logCorruptedImage(data);
				return null;
			}

			int srcW = image.getWidth(null);
			int srcH = image.getHeight(null);
			int targHeight = srcH;
			int targWidth = srcW;

			if (srcW > ImageSize.PX900.getWidth() || srcH > ImageSize.PX900.getHeight()) {
				targHeight = ImageSize.PX900.getHeight();
				targWidth = ImageSize.PX900.getWidth();
				if ((srcW > ImageSize.PX1800.getWidth() || srcH > ImageSize.PX1800.getHeight()) && !lite) {
					targHeight = ImageSize.PX1800.getHeight();
					targWidth = ImageSize.PX1800.getWidth();
				}
				if (srcW > srcH) {
					targHeight = targWidth * srcH / srcW;
				} else {
					targWidth = targHeight * srcW / srcH;
				}
			}

			return ImageUtil.scaleImage(image, targWidth, targHeight);
		}

		private void logCorruptedImage(BinaryData data) {
			try {
				Path file = Paths.get("logs", "corrupted-images", new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss-SSS.'dat'").format(new Date()));
				Files.createDirectories(file.getParent());
				Files.write(file, data.getValue());
				log.info("Corrupted image written to disk for troubleshooting: " + file);
			} catch (IOException e) {
				log.warn("Failed to write corrupted image to disk for troubleshooting.", e);
			}
		}
	};

	@Override
	public void load(PayLoadCollection payloadCollection, Date showTime) throws DataSourceException {
		if (!payloadCollection.isEmpty()) {
			String msg = "LiveImaging cannot manage to generate all images. There is at least " + payloadCollection.size()
					+ " images to be generated from the last generation job";
			complainer.complainGeneral(msg);
			log.warn(msg);
		}

		reloadPayloadsCacheIfNeeded();

		synchronized (rtPayloads) {
			for (PayLoad payload : rtPayloads.values()) {
				if (payload.getNbPoints() < drawThreshold)
					continue;

				payload.setColorMap(colorManager.getColorMap(payload.getColMapType()));
				payload.setShowTime(showTime);
				payloadCollection.addPayLoad(payload);
			}
		}
	}

	@Override
	public void load(PayLoadCollection payloadCollection, Date startDate, Date endDate, Collection<TO<?>> objIds,
	        Collection<ImageType> imageTypes, long period) throws DataSourceException {
		List<Date> cronDates = new LinkedList<Date>();
		long genTime = startDate.getTime();
		long endTime = endDate.getTime();
		while (genTime < endTime) {
			cronDates.add(new Date(genTime));
			genTime += period;
		}
		cronDates.add(new Date(endTime));
		int dates = cronDates.size();
		// Iterating over time and add payloads to the pipe
		ListIterator<Date> iterator = cronDates.listIterator(dates);

		// Reverse iterating for load of history once at the first iteration.
		while (iterator.hasPrevious()) {
			Date showTime = iterator.previous();

			log.info("Loading payloads for date " + showTime);
			Map<String, PayLoad> payloads = loadPayLoads(true, new HistPayloadFilter(objIds, imageTypes), showTime)
			        .first();

			for (PayLoad payload : payloads.values()) {
				payload.setShowTime(showTime);
				payload.setColorMap(colorManager.getColorMap(payload.getColMapType()));
				payloadCollection.addPayLoad(payload);
			}

		}
		histCache.clear();
		log.info("Payloads for the period " + startDate + " - " + endDate + " have been loaded");
	}

	@Override
	public PayLoad load(TO<?> loadTO, Integer dataclass, Integer layer) throws DataSourceException {
		return getCachedPayloadTuple(loadTO, dataclass, layer, false);
	}

	@Override
	public PayLoad loadLite(TO<?> loadTO, Integer dataclass, Integer layer) throws DataSourceException {
		return getCachedPayloadTuple(loadTO, dataclass, layer, true);
	}

	public TO<?> getESObjectForSmartzoneId(String floorplan) {
		return this.smartZoneIdToESObj.get(Integer.valueOf(floorplan));
	}

	public void setLevels(Collection<String> levels) {
		this.levels.addAll(levels);
	}

	public void setColorManager(ColorManager colorManager) {
		this.colorManager = colorManager;
	}

	public static interface Invalidatable {
		void invalidate();
	}

	public void setToInvalidate(Invalidatable toInvalidate) {
		this.toInvalidate = toInvalidate;
	}

	private void initializeEsConnection() {
		if (!firstCall)
			return;

		synchronized (EsDataSource.class) {
			if (firstCall) {
				firstCall = false;
				// register processors
				registerProcessors();
			}
		}
	}

	private PayLoad getCachedPayloadTuple(TO<?> loadTO, Integer dataclass, Integer layer, boolean lite) {
		reloadPayloadsCacheIfNeeded();

		Map<String, PayLoad> targetMap = lite ? rtLitePayloads : rtPayloads;

		PayLoad cachedPayload = targetMap.get(EsUtil.loadLiveImageId(loadTO, dataclass, layer, separator, false));
		if (cachedPayload == null) {
			throw new DataSourceException("Unable to find payload in cache");
		}

		EsPayLoad payloadInCache = new EsPayLoad((EsPayLoad) cachedPayload, new Date());
		payloadInCache.setColorMap(colorManager.getColorMap(cachedPayload.getColMapType()));

		return payloadInCache;
	}

	private void reloadPayloadsCacheIfNeeded() {
		if (isValid())
			return;

		log.info("Payloads cache is empty. Loading payloads from ES...");
		synchronized (rtPayloads) {
			if (isValid())
				return;

			rtPayloads.clear();
			rtLitePayloads.clear();

			Tuple payloads = loadPayLoads(false, new RtPayloadFilter(levels), null);

			rtPayloads.putAll(payloads.first());
			rtLitePayloads.putAll(payloads.second());

			isValid = true;
		}

		log.info("Payloads have been reloaded");
	}

	private boolean isValid() {
		return isValid;
	}

	// invalidate cache of payloads only
	private void invalidate() {
		if (toInvalidate != null) {
			toInvalidate.invalidate();
		}
		isValid = false;
	}

	class Tuple {
		private Map<String, PayLoad> f;
		private Map<String, PayLoad> s;

		public Tuple(Map<String, PayLoad> payloads, Map<String, PayLoad> litePayloads) {
			this.f = payloads;
			this.s = litePayloads;
		}

		Map<String, PayLoad> first() {
			return f;
		}

		Map<String, PayLoad> second() {
			return s;
		}

	}

	/**
	 * Loads payloads from ES
	 * 
	 * @param historical
	 *            Use history when appropriate flag
	 * @param filter
	 *            How to filter payloads
	 * @param when
	 * @return Tuple of payloads to be generated
	 * 
	 * @throws DataSourceException
	 */
	private Tuple loadPayLoads(boolean historical, PayloadFilter filter, final Date when) throws DataSourceException {
		// TODO: This method should be reviewed and simplified
		final Environment env = esContext.getEnv();

		Map<String, PayLoad> litePayloads = new HashMap<>();
		Map<String, PayLoad> payloads = new HashMap<>();

		Map<CaId, Collection<Area>> areaMap = new CaESLoader(env, this, when).load();

		Collection<TO<?>> dcs = env.getObjectsByType(LiConstants.DC);
		try {
			for (final TO<?> dc : dcs) {
				BufferedImage dcBackground = getBackgroundImage(dc, env, when, ImageSize.PX1800);
				if (dcBackground == null) {
					String msg = "Skipping " + dc + " because of missing/corrupted background image.";
					complainer.complainGeneral(msg);
					log.warn(msg);
					continue;
				}
				BufferedImage dcLiteBackground = null;
				if (!historical) {
					dcLiteBackground = getBackgroundImage(dc, env, null, ImageSize.PX900);
					if (dcLiteBackground == null) {
						String msg = "Skipping " + dc + " because of missing/corrupted background image.";
						complainer.complainGeneral(msg);
						log.warn(msg);
						continue;
					}
				}

				Double dcWidth = getProperty(dc, "width", env, when, Double.class);
				Double dcHeight = getProperty(dc, "height", env, when, Double.class);

				Integer floorplanId = getProperty(dc, "smartZoneId", env, when, Integer.class);
				if (floorplanId != null) {
					smartZoneIdToESObj.put(floorplanId, dc);
				}

				/*
				 * HACX TIME! Once COINs stop being standalone, mapsense will
				 * just tweak the layer value of the sensor being pointed at by
				 * a coin to make it also be in the control layer. Until then,
				 * we simulate that here.
				 */
				Collection<TO<?>> coins = env.getRelatedObjects(dc, LiConstants.COIN, true);
				if (!coins.isEmpty()) {
					Collection<CollectionTO> coinSensors = env.getPropertyValue(coins, new String[] { "sensor" });
					for (CollectionTO s : coinSensors) {
						sensorsWithCOINs.add((TO<?>) s.getSinglePropValue("sensor").getValue());
					}
				}

				Collection<TO<?>> rooms = env.getRelatedObjects(dc, LiConstants.ROOM, true);
				if (rooms == null || rooms.isEmpty()) {
					continue;
				}
				Collection<CollectionTO> result = null;
				if (historical) {
					result = new ArrayList<>(rooms.size());
					for (TO<?> room : rooms) {
						if (!filter.accept(dc, room))
							continue;

						List<String> properties = Arrays.asList(roomPropNames);
						CollectionTO c = new CollectionTO(room, properties.size());
						for (String p : properties) {
							Double res = getProperty(room, p, env, when, Double.class);
							c.addValue(new ValueTO(p, res));
						}
						result.add(c);
					}
				} else {
					result = env.getPropertyValue(rooms, roomPropNames);
				}
				for (CollectionTO res : result) {
					final TO<?> room = res.getObjId();
					BufferedImage roomBackground = getBackgroundImage(room, env, when, ImageSize.PX1800);
					if (roomBackground == null) {
						String msg = "Skipping " + room + " because of missing/corrupted background image.";
						complainer.complainGeneral(msg);
						log.warn(msg);
						continue;
					}
					BufferedImage roomLiteBackground = null;
					if (!historical) {
						roomLiteBackground = getBackgroundImage(room, env, null, ImageSize.PX900);
						if (roomLiteBackground == null) {
							String msg = "Skipping " + room + " because of missing/corrupted background image.";
							complainer.complainGeneral(msg);
							log.warn(msg);
							continue;
						}
					}

					ValueTO pVal = res.getSinglePropValue("x");
					double roomX = (pVal.getValue() != null) ? (Double) pVal.getValue() : 0.0D;
					pVal = res.getSinglePropValue("y");
					double roomY = (pVal.getValue() != null) ? (Double) pVal.getValue() : 0.0D;
					pVal = res.getSinglePropValue("width");
					double width = (pVal.getValue() != null) ? (Double) pVal.getValue() : 0.0D;
					pVal = res.getSinglePropValue("height");
					double height = (pVal.getValue() != null) ? (Double) pVal.getValue() : 0.0D;

					Collection<TO<?>> sensors = env.getRelatedObjects(room, LiConstants.WSNSENSOR, true);
					Collection<TO<?>> displaypoints = env.getRelatedObjects(room, LiConstants.DISPLAYPOINT, true);
					Collection<CollectionTO> sensorValues = new LinkedList<>();

					// need to call getPropertyValue separately for each object type
					if (!sensors.isEmpty()) {
						if (when != null) {
							// Historical properties
							sensorValues.addAll(loadHistoricalProperties(sensors, env, when));
						} else {
							sensorValues.addAll(env.getPropertyValue(sensors, sensorPropNames));
						}
					}
					if (!displaypoints.isEmpty()) {
						if (when != null) {
							// Historical properties
							sensorValues.addAll(loadHistoricalProperties(displaypoints, env, when));
						} else {
							sensorValues.addAll(env.getPropertyValue(displaypoints, sensorPropNames));
						}
					}
					for (CollectionTO pvArray : sensorValues) {
						TO<?> sensor = pvArray.getObjId();

						pVal = pvArray.getSinglePropValue("z");
						int layerMask = (pVal.getValue() != null) ? (Integer) pVal.getValue() : 0;

						if (layerMask == 0) {
							// sensor is not part of live imaging layer,
							// skipping...
							continue;
						}

						pVal = pvArray.getSinglePropValue("status");
						int status = (pVal.getValue() != null) ? (Integer) pVal.getValue() : 0;

						pVal = pvArray.getSinglePropValue("lastValue");
						double value;
						Object contents = pVal.getValue();
						if (contents instanceof PropertyTO) {
							Object oval = env.getPropertyValue(((PropertyTO) contents).getObjId(),
							        ((PropertyTO) contents).getPropertyName(), Object.class);
							Double val = null;

							if (oval instanceof Double) {
								val = (Double) oval;
							} else if (oval instanceof TO) {
								val = env.getPropertyValue(((TO<?>) oval), "lastValue", Double.class);
							} else {
								if (oval != null) {
									log.error(sensor + ".lastValue should be double");
								}
								continue;
							}
							value = (val != null) ? val : -3000.0D;
						} else if (contents instanceof Double) {
							value = (pVal.getValue() != null) ? (Double) pVal.getValue() : -3000.0D;
						} else if (contents != null) {
							log.error(sensor + ".lastValue should be double");
							continue;
						} else
							continue;

						if (!historical) {
							if (status != 1) {
								log.warn("Sensor " + sensor + " status is not 1, skipping...");
								continue;
							}
							if (value < -1000.0) {
								log.warn("No recent value for " + sensor + ", skipping...");
								continue;
							}
						}

						pVal = pvArray.getSinglePropValue("x");
						double sensorX = (pVal.getValue() != null) ? (Double) pVal.getValue() : 0.0D;

						pVal = pvArray.getSinglePropValue("y");
						double sensorY = (pVal.getValue() != null) ? (Double) pVal.getValue() : 0.0D;

						pVal = pvArray.getSinglePropValue("dataclass");
						int dataClassId = (Integer) pVal.getValue();

						// a sensor may participate in several layers

						if (sensorsWithCOINs.contains(sensor)) {
							// add control layer
							layerMask = layerMask | LiConstants.CONTROLLAYER;
						}

						Integer[] layers = EsUtil.parseLayerMask(layerMask);
						for (int layer : layers) {

							float fValue = (float) value;

							Collection<Area> cas = areaMap.get(new CaId(dc, layer));
							if (filter.accept(room, dataClassId, layer)) {

								String imageId = EsUtil
								        .loadLiveImageId(room, dataClassId, layer, separator, historical);

								PayLoad payload = payloads.get(imageId);
								if (payload == null) {
									payload = makeNewPayload(roomBackground, roomX, roomY, width, height, dataClassId,
									        imageId, cas);
									payloads.put(imageId, payload);
								}

								boolean bothPayloadsUpdated = payload.addPoint(sensor, sensorX, sensorY, fValue);

								PayLoad payloadLite = litePayloads.get(imageId);
								if (!historical && payloadLite == null) {
									payloadLite = makeNewPayload(roomLiteBackground, roomX, roomY, width, height,
									        dataClassId, imageId, cas);
									litePayloads.put(imageId, payloadLite);
								}

								if (!historical)
									bothPayloadsUpdated &= payloadLite.addPoint(sensor, sensorX, sensorY, fValue);

								if (!bothPayloadsUpdated) {
									log.warn("Sensor "
									        + sensor
									        + " coordinates are duplicating. It won't be considered while generating images.");
								}
							}

							if (filter.accept(dc, dataClassId, layer)) {

								String imageId = EsUtil.loadLiveImageId(dc, dataClassId, layer, separator, historical);

								PayLoad payload = payloads.get(imageId);
								if (payload == null) {
									payload = makeNewPayload(dcBackground, 0, 0, dcWidth, dcHeight, dataClassId,
									        imageId, cas);
									payloads.put(imageId, payload);
								}

								boolean bothPayloadsUpdated = payload.addPoint(sensor, sensorX, sensorY, fValue);

								PayLoad payloadLite = litePayloads.get(imageId);
								if (!historical && payloadLite == null) {
									payloadLite = makeNewPayload(dcLiteBackground, 0, 0, dcWidth, dcHeight,
									        dataClassId, imageId, cas);
									litePayloads.put(imageId, payloadLite);
								}

								if (!historical)
									bothPayloadsUpdated &= payloadLite.addPoint(sensor, sensorX, sensorY, fValue);

								if (!bothPayloadsUpdated) {
									log.warn("Sensor "
									        + sensor
									        + " coordinates are duplicating. It won't be considered while generating images.");
								}
							}
						}
					}
				}
			}

			log.info("smartZoneIdToESObj = " + smartZoneIdToESObj);
			return new Tuple(payloads, litePayloads);
		} catch (EnvException e) {
			throw new DataSourceException("ES related error happened", e);
		}
	}

	private PayLoad makeNewPayload(BufferedImage background, double x, double y, double width, double height,
	        int dataClassId, String imageId, Collection<Area> cas) {
		EsPayLoad payLoad = new EsPayLoad(x, y, width, height, imageId, String.valueOf(dataClassId), background);
		// contained area
		if (cas != null) {
			for (Area ca : cas) {
				payLoad.nest(ca.affline((float) payLoad.getX(), (float) payLoad.getY(),
				        (float) (payLoad.getWidth() / payLoad.getRegionWidth()),
				        (float) (payLoad.getHeight() / payLoad.getRegionHeight())));
			}
		}

		return payLoad;
	}

	private <T> LinkedList<ValueTO> loadHistoricalValues(Environment env, TO<?> sensor, String propertyName,
	        Date startDate, Date endDate, Class<T> clazz) {
		try {
			LinkedList<ValueTO> values = new LinkedList<>();
			if (LiConstants.WSNSENSOR.equals(sensor.getTypeName()) || !"lastValue".equals(propertyName)) {
				values.add(getProperty(sensor, propertyName, env, startDate, endDate, clazz));
			} else if (LiConstants.DISPLAYPOINT.equals(sensor.getTypeName())) {
				PropertyTO propertyTO = env.getPropertyValue(sensor, "lastValue", PropertyTO.class);
				ValueTO value = getProperty(propertyTO.getObjId(), propertyTO.getPropertyName(), env, startDate,
				        endDate, clazz);
				value.setPropertyName(propertyName);
				values.add(value);
			}

			return values;
		} catch (EnvException e) {
			throw new DataSourceException("Unable to get history ", e);
		}
	}

	private Collection<CollectionTO> loadHistoricalProperties(Collection<TO<?>> TOs, Environment env, Date when)
	        throws EnvException {

		Collection<CollectionTO> res = new LinkedList<CollectionTO>();
		for (TO<?> to : TOs) {
			CollectionTO cTO = new CollectionTO(to);
			cTO.addAll(loadHistoricalValues(env, to, "lastValue", new Date(when.getTime() - THRESHOLD), when,
			        Double.class));
			cTO.addAll(loadHistoricalValues(env, to, "x", new Date(0), when, Double.class));
			cTO.addAll(loadHistoricalValues(env, to, "y", new Date(0), when, Double.class));
			cTO.addAll(loadHistoricalValues(env, to, "z", new Date(0), when, Integer.class));
			cTO.addAll(loadHistoricalValues(env, to, "dataclass", new Date(0), when, Integer.class));
			cTO.addAll(loadHistoricalValues(env, to, "status", new Date(when.getTime() - THRESHOLD), when,
			        Integer.class));

			res.add(cTO);
		}
		return res;
	}

	private void updateRtPointValue(TO<?> sensor, double newVal) {
		if (!isValid()) {
			// Payloads haven't been created yet
			if (log.isTraceEnabled()) {
				log.trace("Received point update request " + sensor + " lastValue = " + newVal
				        + " but payloads haven't been loaded yet, nothing to update");
			}
			return;
		}
		// traversing should be in sync block
		synchronized (rtPayloads) {
			if (!isValid()) {
				if (log.isTraceEnabled()) {
					log.trace("Received point update request " + sensor + " lastValue = " + newVal
					        + " but payloads haven't been loaded yet, nothing to update");
				}
				return;
			}
			for (PayLoad payload : rtPayloads.values()) {
				payload.updatePointValue(sensor, (float) newVal);
			}
			for (PayLoad payload : rtLitePayloads.values()) {
				payload.updatePointValue(sensor, (float) newVal);
			}
		}
	}

	private class SensorPropertiesEventProcessor implements PropertiesChangedEventProcessor {

		@Override
		public PropertiesChangedEvent process(PropertiesChangedEvent e) {
			for (ChangedValueTO chVal : e.getChangedValues()) {
				if (chVal.getPropertyName().equals("lastValue")) {
					if (chVal.getOldValue() == null || (Double) chVal.getOldValue() < -2000.0) {
						if (log.isTraceEnabled()) {
							log.trace("Sensor " + e.getObjectTO() + " became alive. Invalidating data source.");
						}
						// Sensor started reporting
						invalidate();
					} else {
						if (log.isTraceEnabled()) {
							log.trace("Sensor " + e.getObjectTO()
							        + " lastValue updated. Looking for image samples to update.");
						}
						updateRtPointValue(e.getObjectTO(), (Double) chVal.getValue());
					}
				} else if (chVal.getPropertyName().equals("status") || chVal.getPropertyName().equals("z")) {
					if (log.isTraceEnabled()) {
						log.trace("Sensor " + e.getObjectTO() + " " + chVal.getPropertyName()
						        + " changed. Invalidating data source.");
					}
					// It is easier and faster just to reload all the next
					// time
					invalidate();
				}
			}
			return e;
		}
	}

	private class DisplaypointEventProcessor implements ReferencedPropertyChangedEventProcessor {

		@Override
		public ReferencedPropertyChangedEvent process(ReferencedPropertyChangedEvent event) {
			Double newValue = (Double) event.getReferencedVal().getValue();
			if (newValue == null) {
				if (log.isTraceEnabled()) {
					log.trace("Unrecognized value recieved for displaypoint");
				}
				return event;
			}

			updateRtPointValue(event.getReferencingObj(), newValue);

			if (log.isTraceEnabled()) {
				log.trace("Update value for " + event.getReferencingObj() + " to " + newValue);
			}

			return event;
		}
	}

	private class ESConfigurationProcessor implements ConfigurationCompleteEventProcessor {

		@Override
		public ConfigurationCompleteEvent process(ConfigurationCompleteEvent e) {
			log.info("ES has been reconfigured. Invalidating data source.");
			invalidate();
			return e;
		}
	}

	private HashMap<EventProcessor, EventProcessor> createEventProcessors() {
		HashMap<EventProcessor, EventProcessor> prfilMap = new HashMap<EventProcessor, EventProcessor>();

		// Sensor properties
		DispatchingProcessor propsProcessor = new DispatchingProcessor();
		propsProcessor.putPcep(new SensorPropertiesEventProcessor());
		DispatchingProcessor propsFilter = new DispatchingProcessor().putPcep(new PropertiesChangedEventFilter(
		        new TOMatcher(LiConstants.WSNSENSOR), new String[] { "lastValue", "status", "z" }));
		prfilMap.put(propsProcessor, propsFilter);

		// displaypoints
		DispatchingProcessor displayPointsProcessor = new DispatchingProcessor();
		displayPointsProcessor.putRpcep(new DisplaypointEventProcessor());
		DispatchingProcessor dpFilter = new DispatchingProcessor().putRpcep(new ReferencedPropertyChangedEventFilter(
		        new TOMatcher(LiConstants.DISPLAYPOINT), "lastValue"));
		prfilMap.put(displayPointsProcessor, dpFilter);

		// Configuration complete
		DispatchingProcessor confProcessor = new DispatchingProcessor();
		confProcessor.putCcep(new ESConfigurationProcessor());
		prfilMap.put(confProcessor, new EventTypeFilter(ConfigurationCompleteEvent.class));

		return prfilMap;
	}

	private void registerProcessors() {
		log.info("Registering update listeners on ES server...");
		// For connection management
		Proxy<NotificationService> proxy = esContext.getNsvcProxy();
		proxy.addListener(nsvcLifeCycleListener);

		// Register processors
		HashMap<EventProcessor, EventProcessor> processors = createEventProcessors();
		NotificationService nsvc = esContext.getNsvc();
		for (Map.Entry<EventProcessor, EventProcessor> entry : processors.entrySet()) {
			nsvc.registerProcessor(entry.getKey(), entry.getValue());
		}
	}

	public void setEsContext(ESContext esContext) {
		this.esContext = esContext;
	}

	private <T> ValueTO getProperty(TO<?> objId, String propName, Environment env, Date threshold, Date when,
	        Class<T> clazz) throws EnvException {
		long timestamp = env.getPropertyTimestamp(objId, propName);
		if (when != null) {
			ValueTO data = (ValueTO) getFromCache(objId, propName, when);
			if (data != null)
				return data;

			if (when.getTime() < timestamp) {
				// Need historical object

				Collection<ValueTO> hist = env.getHistory(objId, propName, threshold, when, clazz);
				if (hist != null && hist.size() > 0) {
					TreeMap<Date, Object> map = new TreeMap<Date, Object>();
					for (ValueTO v : hist) {
						map.put(new Date(v.getTimeStamp()), v);
					}
					try {
						data = (ValueTO) map.lastEntry().getValue();
					} catch (ClassCastException e) {
						log.error("Unable convert " + map.lastEntry() + " to ValueTO. " + objId + "::" + propName);
					}
					// put all history map to cache for this property
					saveToCache(objId, propName, map);
					return data;
				} else {
					log.debug("Unable to get new property '" + propName + "' for " + objId + " before " + when);
				}
			} else {
				data = new ValueTO(propName, env.getPropertyValue(objId, propName, clazz), timestamp);
				saveToCache(objId, propName, new Date(timestamp), data);
				return data;
			}
		}
		return new ValueTO(propName, env.getPropertyValue(objId, propName, clazz), timestamp);
	}

	@Override
	public <T> T getProperty(TO<?> objId, String propName, Environment env, Date when, Class<T> clazz)
	        throws EnvException {

		if (when != null) {
			T data = (T) getFromCache(objId, propName, when);
			if (data != null)
				return data;

			long timestamp = env.getPropertyTimestamp(objId, propName);
			if (when.getTime() < timestamp) {
				// Need historical object

				Collection<ValueTO> hist = env.getHistory(objId, propName, new Date(0), when, clazz);
				if (hist != null && hist.size() > 0) {
					TreeMap<Date, Object> map = new TreeMap<Date, Object>();
					for (ValueTO v : hist) {
						map.put(new Date(v.getTimeStamp()), v.getValue());
					}
					data = (T) map.lastEntry().getValue();
					// put all history map to cache for this property
					saveToCache(objId, propName, map);
					return data;
				} else {
					log.debug("Unable to get new property '" + propName + "' for " + objId + " before " + when);
				}
			} else {
				data = env.getPropertyValue(objId, propName, clazz);
				saveToCache(objId, propName, new Date(timestamp), data);
				return (T) data;
			}
		}
		return (T) env.getPropertyValue(objId, propName, clazz);
	}

	private BufferedImage getBackgroundImage(TO<?> objId, Environment env, Date when, ImageSize size)
	        throws EnvException {
		final String imageProperty = "image";
		final ImageScaleFunction scaleFunction = ImageScaleFunction.choose(size);

		if (when != null) {
			BufferedImage data1 = (BufferedImage) getFromCache(objId, imageProperty, when);
			if (data1 != null)
				return data1;

			long timestamp = env.getPropertyTimestamp(objId, imageProperty);
			if (when.getTime() < timestamp) {
				// Need historical object
				// [BC 08/2015] Why do we load from the beginning? load() is called with a specific range of dates.
				Collection<ValueTO> hist = env.getHistory(objId, imageProperty, new Date(0), when, BinaryData.class);
				if (hist != null && hist.size() > 0) {
					TreeMap<Date, Object> map = new TreeMap<Date, Object>();
					for (ValueTO v : hist) {
						BufferedImage data2 = scaleFunction.apply((BinaryData) v.getValue());
						if (data2 == null) continue;
						map.put(new Date(v.getTimeStamp()), data2);
					}
					if (map.isEmpty()) return null; // This can happen when all images are missing/corrupted
					BufferedImage data3 = (BufferedImage) map.lastEntry().getValue();
					// put all history map to cache for this property
					saveToCache(objId, imageProperty, map);
					return data3;
				} else {
					log.info("No history for property '" + imageProperty + "' of " + objId + " before " + when + ". Using the current value.");
				}
			}
			BufferedImage data4 = scaleFunction.apply(env.getPropertyValue(objId, imageProperty, BinaryData.class));
			if (data4 == null) return null;
			saveToCache(objId, imageProperty, new Date(timestamp), data4);
			return data4;
		}
		return scaleFunction.apply(env.getPropertyValue(objId, imageProperty, BinaryData.class));

	}

	private Object getFromCache(TO<?> objId, String propName, Date when) {
		HashMap<String, TreeMap<Date, Object>> cacheElement = histCache.get(objId);
		if (cacheElement != null) {
			TreeMap<Date, Object> propertyCache = cacheElement.get(propName);
			if (propertyCache != null) {
				// Cache already loaded, let's use values
				Map.Entry<Date, Object> entry = propertyCache.floorEntry(when);
				if (entry != null && entry.getValue() != null) {
					return entry.getValue();
				}
			}
		}
		return null;
	}

	private void saveToCache(TO<?> objId, String propName, Date when, Object data) {
		if (histCache.containsKey(objId)) {
			HashMap<String, TreeMap<Date, Object>> propCache = histCache.get(objId);
			if (propCache != null) {
				TreeMap<Date, Object> dateCache = propCache.get(propName);
				if (dateCache != null) {
					dateCache.put(when, data);
				} else {
					dateCache = new TreeMap<Date, Object>();
					dateCache.put(when, data);
					propCache.put(propName, dateCache);
				}
			} else {
				// Cache of properties doesn't exist yet, create it.
				propCache = new HashMap<String, TreeMap<Date, Object>>();
				TreeMap<Date, Object> dateCache = new TreeMap<Date, Object>();
				dateCache.put(when, data);
				propCache.put(propName, dateCache);
				histCache.put(objId, propCache);
			}
		} else {
			// Init cache for TO
			HashMap<String, TreeMap<Date, Object>> propertyCache = new HashMap<String, TreeMap<Date, Object>>();
			TreeMap<Date, Object> dateCache = new TreeMap<Date, Object>();
			dateCache.put(when, data);
			propertyCache.put(propName, dateCache);
			histCache.put(objId, propertyCache);
		}
	}

	private void saveToCache(TO<?> objId, String propName, TreeMap<Date, Object> dateCacheToSave) {
		if (histCache.containsKey(objId)) {
			HashMap<String, TreeMap<Date, Object>> propCache = histCache.get(objId);
			if (propCache != null) {
				TreeMap<Date, Object> dateCache = propCache.get(propName);
				if (dateCache != null) {
					dateCache.putAll(dateCacheToSave);
				} else {
					propCache.put(propName, dateCacheToSave);
				}
			} else {
				// Cache of properties doesn't exist yet, create it.
				propCache = new HashMap<String, TreeMap<Date, Object>>();
				propCache.put(propName, dateCacheToSave);
				histCache.put(objId, propCache);
			}
		} else {
			// Init cache for TO
			HashMap<String, TreeMap<Date, Object>> propertyCache = new HashMap<String, TreeMap<Date, Object>>();
			propertyCache.put(propName, dateCacheToSave);
			histCache.put(objId, propertyCache);
		}
	}

	void init() {
		initializeEsConnection();
		reloadPayloadsCacheIfNeeded();
	}

	void destroy() {
		final Proxy<NotificationService> proxy = esContext.getNsvcProxy();
		if (proxy != null) {
			// Disconnect from ES, but spend no more than 1 sec. ES might not be running.
			try {
				PExecutors.executeVWithTimeout(proxy::release, 1, TimeUnit.SECONDS);
			} catch (InterruptedException e) {
				Thread.currentThread().interrupt();
			}
		}
	}
}
