package com.synapsense.liveimaging.es;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import com.synapsense.exception.PropertyNotFoundException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.dto.TO;
import com.synapsense.exception.EnvException;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.liveimaging.conf.LiConstants;
import com.synapsense.liveimaging.data.EsPropertyLoader;
import com.synapsense.liveimaging.draw.Area;
import com.synapsense.liveimaging.draw.AreaProcessor;
import com.synapsense.liveimaging.draw.Point2F;
import com.synapsense.liveimaging.draw.PolygonArea;
import com.synapsense.liveimaging.draw.RectangleArea;
import com.synapsense.service.Environment;

public class CaESLoader implements CaLoader {

	private static final Log log = LogFactory.getLog(CaESLoader.class);

	private Environment env;
	private EsPropertyLoader propLoader;
	private Date when;

	CaESLoader(Environment env, EsPropertyLoader propLoader, Date when) {
		this.env = env;
		this.propLoader = propLoader;
		this.when = when;
	}

	@Override
	public Map<CaId, Collection<Area>> load(){
		Map<CaId, Collection<Area>> res = new HashMap<>();
		Collection<TO<?>> dcs = env.getObjectsByType(LiConstants.DC);
		for (TO<?> dc : dcs) {
			HashMap<Integer, AreaProcessor> layerProcMap = new HashMap<>();

			// Rectangles
			Collection<TO<?>> cas = env.getRelatedObjects(dc, "contained_rectangle", true);

			for (TO<?> caTo : cas) {
				try {
					addRectangles(caTo, env, layerProcMap);
				} catch (EnvException e) {
					log.warn("Some properties of rectangle area  " + caTo + " cannot be read", e);
				}
			}

			// Arbitrary polygons and rooms
			cas = env.getRelatedObjects(dc, "contained_poly", true);
			cas.addAll(env.getRelatedObjects(dc, "room", true));

			for (TO<?> poly : cas) {
				try {
					addPolygons(poly, env, layerProcMap);
				} catch (EnvException e) {
					log.warn("Some properties of polygon area " + poly + " cannot be read.", e);
				}
			}

			for (Entry<Integer, AreaProcessor> entry : layerProcMap.entrySet()) {
				res.put(new CaId(dc, entry.getKey()), entry.getValue().getAreasForest());
			}
		}
		return res;
	}

	private void addRectangles(TO<?> rect, Environment env, HashMap<Integer, AreaProcessor> procMap) throws EnvException {
		Double xCenter = propLoader.getProperty(rect, "x", env, when, Double.class);
		Double yCenter = propLoader.getProperty(rect, "y", env, when, Double.class);
		Double width = propLoader.getProperty(rect, "width", env, when, Double.class);
		Double depth = propLoader.getProperty(rect, "depth", env, when, Double.class);

		Double x1 = xCenter - depth / 2;
		Double y1 = yCenter - width / 2;
		Double x2 = xCenter + depth / 2;
		Double y2 = yCenter + width / 2;

		Integer[] layers = EsUtil.parseLayerMask(propLoader.getProperty(rect, "visualLayer", env, when, Integer.class));
		for (Integer layer : layers) {
			RectangleArea rectArea = new RectangleArea(x1.floatValue(), y1.floatValue(), x2.floatValue(), y2.floatValue());
			setParameters(rect, rectArea, env);
			addAreaToProcess(layer, rectArea, procMap);
		}
	}

	private void addAreaToProcess(int layer, Area area, HashMap<Integer, AreaProcessor> procMap) {
		AreaProcessor proc = procMap.get(layer);
		if (proc == null) {
			proc = new AreaProcessor();
			procMap.put(layer, proc);
		}
		proc.addArea(area);
	}

	private void addPolygons(TO<?> poly, Environment env, HashMap<Integer, AreaProcessor> procMap) throws EnvException {
		ArrayList<Point2F> points = new ArrayList<>();
		String[] pointArr = propLoader.getProperty(poly, "points", env, when, String.class).split(";");
		for (String cStr : pointArr) {
			String[] xy = cStr.split(",");
			points.add(new Point2F(Float.valueOf(xy[0]), Float.valueOf(xy[1])));
		}

		Integer[] layers = EsUtil.parseLayerMask(propLoader.getProperty(poly, "visualLayer", env, when, Integer.class));
		for (Integer layer : layers) {
			PolygonArea area = new PolygonArea();
			setParameters(poly, area, env);
			area.addAllAngles(points);
			addAreaToProcess(layer, area, procMap);
		}
	}

	private void setParameters(TO<?> caTo, Area area, Environment env) throws EnvException {
		Integer val;
		try {
			val = env.getPropertyValue(caTo, "alpha", Integer.class);
			if (val != null)
				area.setAlpha(val);
		} catch (PropertyNotFoundException e) {
			log.info("'alpha' property not found for " + caTo + ". Will assume defaults.");
		}
		try {
			val = env.getPropertyValue(caTo, "color", Integer.class);
			if (val != null)
				area.setColor(val);
		} catch (PropertyNotFoundException e) {
			log.info("'color' property not found for " + caTo + ". Will assume defaults.");
		}
		try {
			val = env.getPropertyValue(caTo, "minPoints", Integer.class);
			if (val != null)
				area.setMinPoints(val);
		} catch (PropertyNotFoundException e) {
			log.info("'minPoints' property not found for " + caTo + ". Will assume defaults.");
		}
	}
}
