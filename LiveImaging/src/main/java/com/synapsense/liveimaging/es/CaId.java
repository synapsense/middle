package com.synapsense.liveimaging.es;

import com.synapsense.dto.TO;

public class CaId {

	private TO<?> objId;

	private int layer;

	public CaId(TO<?> objId, int layer) {
		super();
		this.objId = objId;
		this.layer = layer;
	}

	public TO<?> getObjId() {
		return objId;
	}

	public void setObjId(TO<?> objId) {
		this.objId = objId;
	}

	public int getLayer() {
		return layer;
	}

	public void setLayer(int layer) {
		this.layer = layer;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + layer;
		result = prime * result + ((objId == null) ? 0 : objId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final CaId other = (CaId) obj;
		if (layer != other.layer)
			return false;
		if (objId == null) {
			if (other.objId != null)
				return false;
		} else if (!objId.equals(other.objId))
			return false;
		return true;
	}
}
