package com.synapsense.liveimaging.es;

import com.synapsense.dto.TO;
import com.synapsense.liveimaging.conf.LiConstants;
import com.synapsense.service.Environment;
import com.synapsense.service.LocalizationService;
import com.synapsense.service.UnitSystemsService;
import com.synapsense.util.unitconverter.Dimension;
import com.synapsense.util.unitconverter.UnitSystem;
import com.synapsense.util.unitconverter.UnitSystems;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.LinkedList;
import java.util.Locale;
import java.util.Map;

public final class EsUtil {
	private static Log log = LogFactory.getLog(EsUtil.class);

	public static String loadLiveImageId(TO<?> objId, int dataClass, int layer, String separator, boolean historical) {
		StringBuilder builder = new StringBuilder();
		builder.append(objId.getTypeName()).append(separator).append(objId.getID()).append(separator).append(layer)
		        .append(separator);
		if (historical) {
			builder.append(LiConstants.HIST_IMAGE_PREFIX);
		}
		builder.append("image-").append(objId.getID()).append("-").append(dataClass).append("-").append(layer);
		return builder.toString();

	}

	// TODO: layer 1 should work!

	public static Integer[] parseLayerMask(int mask) {
		LinkedList<Integer> layers = new LinkedList<Integer>();
		int layer = 1;
		for (int i = 1; i <= 32; i++) {
			layer *= 2;
			if ((layer & mask) != 0) {
				layers.add(layer);
			}
		}
		return layers.toArray(new Integer[layers.size()]);
	}

	public static Dimension getDimension(final ESContext esContext, Integer dataclass, String unitSystem) {
		UnitSystemsService unitSystemsService = esContext.getUnitSystemsService();
		Dimension dimension = null;
		UnitSystems unitSystems = unitSystemsService.getUnitSystems();
		try {
			UnitSystem us = unitSystems.getUnitSystem(unitSystem);
			dimension = us.getDimension(String.valueOf(dataclass));
		} catch (Exception e) {
			Map<String, Dimension> us = unitSystems.getSimpleDimensions();
			dimension = us.get(String.valueOf(dataclass));
		}

		return dimension;
	}

	public static String getDimensionName(final ESContext esContext, Dimension dimension, Integer dataclass, Integer layer, Locale locale) {
		String dimName = null;

		if (dimension != null) {
			String tmp = null;
			if (dataclass != 200) {
				tmp = dimension.getLongUnits().replace("/", "_");
			} else {
				// Finding of layer. Only for temperature. Workaround for
				// bug 9232
				switch (layer) {
					case 131072:
						tmp = "dimension_temp_top";
						break;
					case 16384:
						tmp = "dimension_temp_middle";
						break;
					case 2048:
						tmp = "dimension_temp_bottom";
						break;
					case 256:
						tmp = "dimension_subfloor_temp";
						break;
					case 128:
						tmp = "dimension_controlled_temp";
						break;
					default:
						tmp = "dimension_temperature";
						break;
				}

			}

			dimName = localizeString(esContext, tmp, locale);
			if (dimName == null || dimName.isEmpty()) {
				dimName = tmp;
			}
		}

		return dimName;
	}

	public static String getDimensionUnits(final ESContext esContext, Dimension dimension, Locale locale) {
		String dimUnit = null;
		LocalizationService localizationService = esContext.getLocalizationService();
		if (locale == null) {
			locale = localizationService.getDefaultLocale();
		}

		if (dimension != null) {
			String tmp = dimension.getUnits().replace("/", "_");
			dimUnit = localizeString(esContext, tmp, locale);
			if (dimUnit == null || dimUnit.isEmpty()) {
				dimUnit = tmp;
			}
		}

		return dimUnit;
	}

	public static String getLocalizedName(final ESContext esContext, TO<?> objId, Locale locale) {
		String name = null;
		Environment env = esContext.getEnv();
		LocalizationService localizationService = esContext.getLocalizationService();
		if (locale == null) {
			locale = localizationService.getDefaultLocale();
		}

		try {
			name = env.getPropertyValue(objId, "name", String.class);
			String localeName = localizationService.getTranslation(name, locale);
			if (localeName != null) {
				name = localeName;
			}
		} catch (Exception e) {
			log.warn("Could not get object name", e);
		}

		return name;
	}

	public static String localizeString(final ESContext esContext, String key, Locale locale) {
		String str = null;
		LocalizationService localizationService = esContext.getLocalizationService();
		if (locale != null) {
			str = localizationService.getString(key, locale);
		}

		if (str == null) {
			log.debug("Could not find translation of [" + key + "]. Locale: " + locale
							  + ". Default locale will be used");
			str = localizationService.getString(key);
			if (str == null) {
				log.warn("Could not find translation of [" + key + "] for default locale");
				str = "";
			}
		}

		return str;
	}

	private EsUtil() {
	}
}
