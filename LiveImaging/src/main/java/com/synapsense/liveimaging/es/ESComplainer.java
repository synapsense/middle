package com.synapsense.liveimaging.es;

import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.dto.Alert;
import com.synapsense.liveimaging.Complainer;
import com.synapsense.util.LocalizationUtils;

public class ESComplainer implements Complainer {

	private static Log log = LogFactory.getLog(ESComplainer.class);

	public static final String ALERT_TYPE_CRITICAL = "Critical system alert";
	public static final String ALERT_NAME_PORT_BUSY = "LiveImaging port is already in use";
	public static final String ALERT_MSG_PORT_BUSY = "alert_msg_port_busy";
	public static final String ALERT_NAME_DIR_ACCESS = "LiveImaging output directory is not accessible";
	public static final String ALERT_MSG_DIR_ACCESS = "alert_msg_dir_access";
	public static final String ALERT_NAME_GENERAL = "LiveImaging error";
	public static final String ALERT_MSG_GENERAL = "alert_msg_li_general";

	private ESContext esContext;

	@Override
	public void complainPort(int port) {
		log.info("Complaining about bad port " + port + " to ES");
		throwAlert(ALERT_NAME_PORT_BUSY,
		        LocalizationUtils.getLocalizedString(ALERT_MSG_PORT_BUSY, String.valueOf(port)));
	}

	@Override
	public void complainDir(String dir) {
		log.info("Complaining about bad output directoty " + dir + " to ES");
		throwAlert(ALERT_NAME_DIR_ACCESS,
				LocalizationUtils.getLocalizedString(ALERT_MSG_DIR_ACCESS, String.valueOf(dir)));
	}

	@Override
	public void complainGeneral() {
		this.complainGeneral(null);
	}

	@Override
	public void complainGeneral(String msg) {
		log.info("Throwing general complaint  to ES");
		if (msg == null || msg.isEmpty()) {
			msg = LocalizationUtils.getLocalizedString(ALERT_MSG_GENERAL);
		}
		throwAlert(ALERT_NAME_GENERAL, msg);
	}

	private void throwAlert(String name, String message) {
		try {
			esContext.getAlertService().raiseAlert(new Alert(name, ALERT_TYPE_CRITICAL, new Date(), message));
		} catch (Exception e) {
			log.warn("Unable to complain ", e);
		}
	}

	public void setEsContext(ESContext esContext) {
		this.esContext = esContext;
	}
}
