package com.synapsense.liveimaging.es;

import java.util.Collection;
import java.util.Map;

import com.synapsense.liveimaging.data.DataSourceException;
import com.synapsense.liveimaging.draw.Area;

public interface CaLoader {

	Map<CaId, Collection<Area>> load() throws DataSourceException;
}
