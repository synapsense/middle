package com.synapsense.liveimaging.es;

import java.util.HashMap;
import java.util.Map;

import com.synapsense.dto.TO;
import com.synapsense.liveimaging.draw.ValuePoint;

public class EsHistoricalSensors {
	private long timestamp;
	private Map<TO<?>, ValuePoint> valuePoints;

	public EsHistoricalSensors(long timestamp, Map<TO<?>, ValuePoint> valuePoints) {
		super();
		this.timestamp = timestamp;
		this.valuePoints = new HashMap<TO<?>, ValuePoint>(valuePoints);
	}

	public EsHistoricalSensors() {
		timestamp = 0;
		valuePoints = new HashMap<TO<?>, ValuePoint>();
	}

	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	public Map<TO<?>, ValuePoint> getValuePoints() {
		return valuePoints;
	}

	public void setValuePoints(Map<TO<?>, ValuePoint> valuePoints) {
		this.valuePoints = valuePoints;
	}
}
