package com.synapsense.liveimaging.data;

import com.synapsense.liveimaging.LiveImagingException;

public class DataSourceException extends LiveImagingException {

	private static final long serialVersionUID = -1763084938740513579L;

	public DataSourceException() {
		super();
	}

	public DataSourceException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public DataSourceException(String arg0) {
		super(arg0);
	}

	public DataSourceException(Throwable arg0) {
		super(arg0);
	}
}
