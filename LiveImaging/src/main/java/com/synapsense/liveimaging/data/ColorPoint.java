package com.synapsense.liveimaging.data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonAutoDetect.Visibility;

@JsonAutoDetect(fieldVisibility = Visibility.NONE)
@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name = "point")
public class ColorPoint implements Comparable<ColorPoint> {

	@JsonProperty("value")
	@XmlAttribute(name = "value", required = true)
	private Double position;
	@JsonProperty("r")
	@XmlAttribute(name = "r", required = true)
	private Integer red;
	@JsonProperty("g")
	@XmlAttribute(name = "g", required = true)
	private Integer green;
	@JsonProperty("b")
	@XmlAttribute(name = "b", required = true)
	private Integer blue;

	public ColorPoint(double pos, int r, int g, int b) {
		position = pos;
		red = r;
		green = g;
		blue = b;
	}

	public ColorPoint(ColorPoint p, Converter converter) {
		this(converter.convert(p.position), p.red, p.getGreen(), p.blue);
	}

	public ColorPoint() {
	}

	public int compareTo(ColorPoint arg0) {
		ColorPoint p = (ColorPoint) arg0;
		if (position == p.position)
			return 0;
		if (position < p.position)
			return -1;
		if (position > p.position)
			return 1;
		return 0;
	}

	public Double getPosition() {
		return position;
	}

	public void setPosition(Double position) {
		this.position = position;
	}

	public Integer getRed() {
		return red;
	}

	public void setRed(Integer red) {
		this.red = red;
	}

	public Integer getGreen() {
		return green;
	}

	public void setGreen(Integer green) {
		this.green = green;
	}

	public Integer getBlue() {
		return blue;
	}

	public void setBlue(Integer blue) {
		this.blue = blue;
	}

	public boolean equals(ColorPoint rhs) {
		return position == rhs.position;
	}
}
