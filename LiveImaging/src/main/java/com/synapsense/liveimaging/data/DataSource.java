package com.synapsense.liveimaging.data;

import java.util.Collection;
import java.util.Date;

import com.synapsense.dto.TO;
import com.synapsense.liveimaging.es.PayloadFilter;
import com.synapsense.liveimaging.web.ImageType;

public interface DataSource {

	void load(PayLoadCollection payloads, Date showTime) throws DataSourceException;

	void load(PayLoadCollection payloads, Date startTime, Date endTime,Collection <TO<?>> objIds,Collection<ImageType> imageTypes, long interval) throws DataSourceException;

	PayLoad load(TO<?> loadTO, Integer dataclass, Integer layer) throws DataSourceException;
	
	PayLoad loadLite(TO<?> loadTO, Integer dataclass, Integer layer) throws DataSourceException;

	TO<?> getESObjectForSmartzoneId(String floorplan);
}
