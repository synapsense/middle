package com.synapsense.liveimaging.data;

import java.util.Date;

import com.synapsense.dto.TO;
import com.synapsense.exception.EnvException;
import com.synapsense.service.Environment;

public interface EsPropertyLoader {

	public <T> T getProperty(TO<?> objId, String propName, Environment env, Date when, Class<T> clazz)
	        throws EnvException;
}
