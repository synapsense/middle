package com.synapsense.liveimaging.data;

import java.awt.image.BufferedImage;
import java.util.Date;

import com.synapsense.dto.TO;
import com.synapsense.liveimaging.draw.LiveImage;

public interface PayLoad {

	String getImageId();

	Date getShowTime();

	LiveImage getImage();

	BufferedImage getBackground();

	ColorMap getColorMap();

	int getNbPoints();

    boolean addPoint(TO<?> sensor, double sensorX, double sensorY, float fValue);

    String getColMapType();

    boolean updatePointValue(TO<?> sensor, double newVal);

    void setColorMap(ColorMap colorMap);

    void setShowTime(Date showTime);
}
