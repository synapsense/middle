package com.synapsense.liveimaging.data;

public interface PayLoadCollection {
	/**
	 * Returns true if collection has payloads in work
	 * 
	 * @return
	 */
	boolean isEmpty();

    int size();

	void addPayLoad(PayLoad payload);
}
