package com.synapsense.liveimaging.data;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name = "colormaps")
public class ColorMaps {

	@XmlElement(name = "colormap", required = true)
	private List<ColorMap> colormaps;

	public List<ColorMap> getColormaps() {
		return colormaps;
	}

	public void setColormaps(List<ColorMap> colormaps) {
		this.colormaps = colormaps;
	}

	public ColorMap getColorMap(String type) {
		for (ColorMap cm : colormaps) {
			if (cm.getType().equals(type)) {
				return cm;
			}
		}
		return null;
	}

	private boolean isValid(ColorMaps colormaps) {
		List<ColorMap> colormapList = colormaps.getColormaps();
		if (!(colormapList == null || colormapList.isEmpty())) {
			for (ColorMap colormap : colormapList) {
				if (!(colormap.getCaptures() == null || colormap.getType() == null)) {
					List<ColorPoint> points = colormap.getPoints();
					if (!(points == null || points.isEmpty())) {
						for (ColorPoint point : points) {
							if (point.getPosition() == null || point.getRed() == null || point.getGreen() == null
							        || point.getBlue() == null) {
								return false;
							}
						}
					} else {
						return false;
					}
				} else {
					return false;
				}
			}
		} else {
			return false;
		}
		return true;
	}

	public void merge(ColorMaps newColorMaps) {
		if (isValid(newColorMaps)) {
			List<ColorMap> newMap = newColorMaps.getColormaps();
			for (ColorMap map : newMap) {
				ColorMap m = getColorMap(map.getType());
				if (m != null) {
					colormaps.remove(m);
					colormaps.add(map);
				} else {
					colormaps.add(map);
				}
			}
		} else {
			throw new IllegalArgumentException("Color model has incorrect format");
		}

	}

}