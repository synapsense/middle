package com.synapsense.liveimaging.data;

public interface Converter {

	public String getName();

	public double convert(double source);
}
