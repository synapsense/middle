package com.synapsense.liveimaging.data;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonAutoDetect.Visibility;
import org.codehaus.jackson.annotate.JsonProperty;

import com.synapsense.liveimaging.legend.LegendConstants;

@JsonAutoDetect(fieldVisibility = Visibility.NONE, setterVisibility = Visibility.NONE, getterVisibility = Visibility.NONE)
@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name = "colormap")
public final class ColorMap {

	@JsonProperty("dataclassId")
	@XmlAttribute(name = "dataclassId", required = true)
	private String type;

	@JsonProperty("captures")
	@XmlAttribute(name = "captures", required = true)
	private Integer captures;

	private List<ColorPoint> points;
	private List<Double> ashrae;

	public ColorMap() {

	}

	public ColorMap(String type) {
		this(type, 5, new ArrayList<ColorPoint>(), new ArrayList<Double>());
	}

	public ColorMap(String type, List<ColorPoint> points) {
		this(type, 5, points, new ArrayList<Double>());
	}

	public ColorMap(String type, int captures, List<ColorPoint> points) {
		this(type, captures, points, new ArrayList<Double>());
	}

	public ColorMap(String type, int captures, List<ColorPoint> points, List<Double> ashrae) {
		this.type = type;
		this.captures = captures;
		this.ashrae = ashrae;
		setPoints(new ArrayList<ColorPoint>(points));
	}

	public ColorMap(ColorMap cmap, Converter converter) {
		this.type = converter.getName() + cmap.type;
		this.captures = cmap.captures;
		List<ColorPoint> points = new ArrayList<ColorPoint>();
		for (ColorPoint p : cmap.points) {
			points.add(new ColorPoint(p, converter));
		}
		setPoints(points);
		ashrae = new ArrayList<Double>();
		for (double ash : cmap.getAshrae()) {
			ashrae.add(converter.convert(ash));
		}
	}

	@JsonProperty("point")
	@XmlElement(name = "point", required = true)
	public void setPoints(List<ColorPoint> points) {
		Collections.sort(points);
		this.points = points;
	}

	@JsonProperty("point")
	public List<ColorPoint> getPoints() {
		return points;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Integer getCaptures() {
		return captures;
	}

	public void setCaptures(Integer captures) {
		this.captures = captures;
	}

	public void setAshrae(List<Double> ashrae) {
		this.ashrae = ashrae;
	}

	public List<Double> getAshrae() {
		if (type.equals(LegendConstants.LEGEND_TEMPERATURE)) {
			return Arrays.asList(LegendConstants.TEMPERATURE_THR);
		} else if (type.equals(LegendConstants.LEGEND_HUMIDITY)) {
			return Arrays.asList(LegendConstants.HUMIDITY_THR);
		}
		return new ArrayList<Double>();
	}

	public double getMaxPoint() {
		return points.get(points.size() - 1).getPosition();
	}

	public double getMinPoint() {
		return points.get(0).getPosition();
	}

	public void addPoint(double pos, int r, int g, int b) {
		ColorPoint p = new ColorPoint();
		p.setPosition(pos);
		p.setRed(r);
		p.setGreen(g);
		p.setBlue(b);
		points.add(p);
		Collections.sort(points);
	}

	public Color getColorBetween(ColorPoint lp, ColorPoint p, double distance) {
		int rdiff = (int) ((p.getRed() - lp.getRed()) * distance);
		int gdiff = (int) ((p.getGreen() - lp.getGreen()) * distance);
		int bdiff = (int) ((p.getBlue() - lp.getBlue()) * distance);
		Color c = new Color(lp.getRed() + rdiff, lp.getGreen() + gdiff, lp.getBlue() + bdiff);
		return c;
	}

	public Color getColorAt(double pos) {
		ColorPoint lp = points.get(0);
		ColorPoint rp = points.get(points.size() - 1);

		if (pos <= lp.getPosition()) {
			return new Color(lp.getRed(), lp.getGreen(), lp.getBlue());
		}

		if (pos >= rp.getPosition()) {
			return new Color(rp.getRed(), rp.getGreen(), rp.getBlue());
		}

		int min = 0;
		int max = points.size() - 1;
		int mid = 0;
		ColorPoint midPoint;
		do {
			mid = (max + min) / 2;
			midPoint = points.get(mid);
			if (midPoint.getPosition() < pos) {
				min = mid;
			} else {
				max = mid;
			}
		} while (min < max - 1);

		if (midPoint.getPosition() == pos) {
			return new Color(midPoint.getRed(), midPoint.getGreen(), midPoint.getBlue());
		}

		ColorPoint lPoint = points.get(min);
		ColorPoint rPoint = points.get(max);

		return getColorBetween(lPoint, rPoint,
		        (pos - lPoint.getPosition()) / (rPoint.getPosition() - lPoint.getPosition()));
	}
}
