package com.synapsense.liveimaging.interpolation;

/**
 * 
 * @author Dmitry Grudzinskiy
 * 
 */
public interface Interpolator {

	float[] interpolate(float[] x, float[] y);

	float[] interpolate(float[] x, float[] y, Samples samples);

	boolean isCacheable();
}
