package com.synapsense.liveimaging.interpolation.methods.util;

import java.util.Random;

/**
 * LU Decomposition.
 * <P>
 * For an m-by-n matrix A with m >= n, the LU decomposition is an m-by-n unit
 * lower triangular matrix L, an n-by-n upper triangular matrix U, and a
 * permutation vector piv of length m so that A(piv,:) = L*U. If m < n, then L
 * is m-by-m and U is m-by-n.
 * <P>
 * The LU decompostion with pivoting always exists, even if the matrix is
 * singular, so the constructor will never fail. The primary use of the LU
 * decomposition is in the solution of square systems of simultaneous linear
 * equations. This will fail if isNonsingular() returns false.
 */

public class LUDecomposition implements java.io.Serializable {

	/*
	 * ------------------------ Class variables ------------------------
	 */

	/**
	 * 
	 */
	private static final long serialVersionUID = 8331772077474650838L;

	/**
	 * Array for internal storage of decomposition.
	 * 
	 * @serial internal array storage.
	 */
	private float[][] LU;

	/**
	 * Row and column dimensions, and pivot sign.
	 * 
	 * @serial column dimension.
	 * @serial row dimension.
	 * @serial pivot sign.
	 */
	private int m, n, pivsign;

	/**
	 * Internal storage of pivot vector.
	 * 
	 * @serial pivot vector.
	 */
	private int[] piv;

	/*
	 * ------------------------ Constructor ------------------------
	 */

	/**
	 * LU Decomposition
	 * 
	 * @param A
	 *            Rectangular matrix
	 * @return Structure to access L, U and piv.
	 */

	public LUDecomposition(Matrix A) {

		// Use a "left-looking", dot-product, Crout/Doolittle algorithm.

		LU = A.getArrayCopy();
		m = A.getRowDimension();
		n = A.getColumnDimension();
		piv = new int[m];
		for (int i = 0; i < m; i++) {
			piv[i] = i;
		}
		pivsign = 1;
		float[] LUrowi;
		float[] LUcolj = new float[m];

		// Outer loop.

		for (int j = 0; j < n; j++) {

			// Make a copy of the j-th column to localize references.

			for (int i = 0; i < m; i++) {
				LUcolj[i] = LU[i][j];
			}

			// Apply previous transformations.

			for (int i = 0; i < m; i++) {
				LUrowi = LU[i];

				// Most of the time is spent in the following dot product.

				int kmax = Math.min(i, j);
				float s = 0.0f;
				for (int k = 0; k < kmax; k++) {
					s += LUrowi[k] * LUcolj[k];
				}

				LUrowi[j] = LUcolj[i] -= s;
			}

			// Find pivot and exchange if necessary.

			int p = j;
			for (int i = j + 1; i < m; i++) {
				if (Math.abs(LUcolj[i]) > Math.abs(LUcolj[p])) {
					p = i;
				}
			}
			if (p != j) {
				for (int k = 0; k < n; k++) {
					float t = LU[p][k];
					LU[p][k] = LU[j][k];
					LU[j][k] = t;
				}
				int k = piv[p];
				piv[p] = piv[j];
				piv[j] = k;
				pivsign = -pivsign;
			}

			// Compute multipliers.

			if (j < m & LU[j][j] != 0.0) {
				for (int i = j + 1; i < m; i++) {
					LU[i][j] /= LU[j][j];
				}
			}
		}
	}

	/*
	 * ------------------------ Temporary, experimental code.
	 * ------------------------ *\
	 * 
	 * \** LU Decomposition, computed by Gaussian elimination. <P> This
	 * constructor computes L and U with the "daxpy"-based elimination algorithm
	 * used in LINPACK and MATLAB. In Java, we suspect the dot-product, Crout
	 * algorithm will be faster. We have temporarily included this constructor
	 * until timing experiments confirm this suspicion. <P> @param A Rectangular
	 * matrix @param linpackflag Use Gaussian elimination. Actual value ignored.
	 * 
	 * @return Structure to access L, U and piv. \
	 * 
	 * public LUDecomposition (Matrix A, int linpackflag) { // Initialize. LU =
	 * A.getArrayCopy(); m = A.getRowDimension(); n = A.getColumnDimension();
	 * piv = new int[m]; for (int i = 0; i < m; i++) { piv[i] = i; } pivsign =
	 * 1; // Main loop. for (int k = 0; k < n; k++) { // Find pivot. int p = k;
	 * for (int i = k+1; i < m; i++) { if (Math.abs(LU[i][k]) >
	 * Math.abs(LU[p][k])) { p = i; } } // Exchange if necessary. if (p != k) {
	 * for (int j = 0; j < n; j++) { double t = LU[p][j]; LU[p][j] = LU[k][j];
	 * LU[k][j] = t; } int t = piv[p]; piv[p] = piv[k]; piv[k] = t; pivsign =
	 * -pivsign; } // Compute multipliers and eliminate k-th column. if
	 * (LU[k][k] != 0.0) { for (int i = k+1; i < m; i++) { LU[i][k] /= LU[k][k];
	 * for (int j = k+1; j < n; j++) { LU[i][j] -= LU[i][k]*LU[k][j]; } } } } }
	 * \* ------------------------ End of temporary code.
	 * ------------------------
	 */

	/*
	 * ------------------------ Public Methods ------------------------
	 */

	/**
	 * Is the matrix nonsingular?
	 * 
	 * @return true if U, and hence A, is nonsingular.
	 */

	public boolean isNonsingular() {
		for (int j = 0; j < n; j++) {
			if (LU[j][j] == 0)
				return false;
		}
		return true;
	}

	/**
	 * Return lower triangular factor
	 * 
	 * @return L
	 */

	public Matrix getL() {
		Matrix X = new Matrix(m, n);
		float[][] L = X.getArray();
		for (int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				if (i > j) {
					L[i][j] = LU[i][j];
				} else if (i == j) {
					L[i][j] = 1.0f;
				} else {
					L[i][j] = 0.0f;
				}
			}
		}
		return X;
	}

	/**
	 * Return upper triangular factor
	 * 
	 * @return U
	 */

	public Matrix getU() {
		Matrix X = new Matrix(n, n);
		float[][] U = X.getArray();
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				if (i <= j) {
					U[i][j] = LU[i][j];
				} else {
					U[i][j] = 0.0f;
				}
			}
		}
		return X;
	}

	/**
	 * Return pivot permutation vector
	 * 
	 * @return piv
	 */

	public int[] getPivot() {
		int[] p = new int[m];
		for (int i = 0; i < m; i++) {
			p[i] = piv[i];
		}
		return p;
	}

	/**
	 * Return pivot permutation vector as a one-dimensional double array
	 * 
	 * @return (double) piv
	 */

	public double[] getDoublePivot() {
		double[] vals = new double[m];
		for (int i = 0; i < m; i++) {
			vals[i] = (double) piv[i];
		}
		return vals;
	}

	/**
	 * Determinant
	 * 
	 * @return det(A)
	 * @exception IllegalArgumentException
	 *                Matrix must be square
	 */

	public double det() {
		if (m != n) {
			throw new IllegalArgumentException("Matrix must be square.");
		}
		double d = (double) pivsign;
		for (int j = 0; j < n; j++) {
			d *= LU[j][j];
		}
		return d;
	}

	/**
	 * Solve A*X = B
	 * 
	 * @param B
	 *            A Matrix with as many rows as A and any number of columns.
	 * @return X so that L*U*X = B(piv,:)
	 * @exception IllegalArgumentException
	 *                Matrix row dimensions must agree.
	 * @exception RuntimeException
	 *                Matrix is singular.
	 */

	public Matrix solve(Matrix B) {
		if (B.getRowDimension() != m) {
			throw new IllegalArgumentException("Matrix row dimensions must agree.");
		}
		if (!this.isNonsingular()) {
			throw new RuntimeException("Matrix is singular.");
		}

		// Copy right hand side with pivoting
		int nx = B.getColumnDimension();
		Matrix Xmat = B.getMatrix(piv, 0, nx - 1);
		float[][] X = Xmat.getArray();

		// Solve L*Y = B(piv,:)
		for (int k = 0; k < n; k++) {
			for (int i = k + 1; i < n; i++) {
				for (int j = 0; j < nx; j++) {
					X[i][j] -= X[k][j] * LU[i][k];
				}
			}
		}
		// Solve U*X = Y;
		for (int k = n - 1; k >= 0; k--) {
			for (int j = 0; j < nx; j++) {
				X[k][j] /= LU[k][k];
			}
			for (int i = 0; i < k; i++) {
				for (int j = 0; j < nx; j++) {
					X[i][j] -= X[k][j] * LU[i][k];
				}
			}
		}
		return Xmat;
	}

	public float[] solve(float[] b) {

		float bPiv[] = new float[n];

		// pivots
		for (int i = 0; i < n; i++) {
			bPiv[i] = b[piv[i]];
		}

		// Solve Ly = b
		float[] y = new float[n];
		y[0] = bPiv[0];
		for (int k = 1; k < n; k++) {
			y[k] = bPiv[k] - vectorMult(y, 0, k - 1, k);
		}

		// Solve U*x = y;
		float[] x = new float[n];
		x[n - 1] = y[n - 1] / LU[n - 1][n - 1];
		for (int k = n - 2; k >= 0; k--) {
			x[k] = (y[k] - vectorMult(x, k + 1, n - 1, k)) / LU[k][k];
		}
		return x;
	}

	private float vectorMult(float[] a, int st, int end, int row) {
		// A tip for server JVM optimization
		float res = 0;
		for (int i = st; i < end - 2; i += 4) {
			res += a[i] * LU[row][i] + a[i + 1] * LU[row][i + 1] + a[i + 2] * LU[row][i + 2] + a[i + 3]
			        * LU[row][i + 3] /*
									 * + a[i + 4] * LU[row][i + 4] + a[i + 5] *
									 * LU[row][i + 5] + a[i + 6] LU[row][i + 6]
									 * + a[i + 7] * LU[row][i + 7]
									 */;
		}
		for (int i = end - (end - st + 1) % 4 + 1; i <= end; i++)
			res += a[i] * LU[row][i];
		return res;
	}

	public static void main(String[] args) {
		Random rand = new Random();
		int n = 100;
		float x[] = new float[n];
		float y[] = new float[n];
		for (int i = 0; i < n; i++) {
			x[i] = rand.nextInt(10000);
			y[i] = rand.nextInt(10000);
		}

		Matrix m = new Matrix(n, n);
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				m.set(i, j, (float) Math.sqrt((x[i] - x[j]) * (x[i] - x[j]) + (y[i] - y[j]) * (y[i] - y[j])));
			}
		}

		long start = System.currentTimeMillis();
		LUDecomposition lu = new LUDecomposition(m);
		System.out.println("Decomposition time: " + (System.currentTimeMillis() - start));

		int pNb = 135450;
		float[] xp = new float[pNb];
		float[] yp = new float[pNb];
		for (int j = 0; j < pNb; j++) {
			xp[j] = rand.nextInt(10000);
			yp[j] = rand.nextInt(10000);
		}

		start = System.currentTimeMillis();

		for (int j = 0; j < pNb; j++) {
			float[] b = new float[n];
			for (int i = 0; i < n; i++) {
				b[i] = (float) Math.sqrt((x[i] - xp[j]) * (x[i] - xp[j]) + (y[i] - yp[j]) * (y[i] - yp[j]));
			}
			lu.solve(b);
		}
		System.out.println("Solve time: " + (System.currentTimeMillis() - start));
	}
}
