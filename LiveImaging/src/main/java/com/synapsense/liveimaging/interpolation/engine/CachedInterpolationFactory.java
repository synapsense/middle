package com.synapsense.liveimaging.interpolation.engine;

import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.liveimaging.es.EsDataSource.Invalidatable;
import com.synapsense.liveimaging.interpolation.InterpolationException;
import com.synapsense.liveimaging.interpolation.InterpolationFactory;
import com.synapsense.liveimaging.interpolation.Interpolator;
import com.synapsense.liveimaging.interpolation.Samples;

public class CachedInterpolationFactory implements InterpolationFactory, Invalidatable {

	private InterpolationFactory factory;

	private ConcurrentHashMap<Samples, Interpolator> cache = new ConcurrentHashMap<Samples, Interpolator>();

	private Log log = LogFactory.getLog(CachedInterpolationFactory.class);

	public CachedInterpolationFactory(InterpolationFactory factory) {
		this.factory = factory;
	}

	@Override
	public Interpolator getInterpolator(Samples samples) throws InterpolationException {
		Interpolator interp = cache.get(samples);
		if (interp == null) {
			interp = factory.getInterpolator(samples);
			if (interp.isCacheable()) {
				cache.put(samples, interp);
			}
			if (log.isDebugEnabled()) {
				log.debug("Returning new interpolator of type " + interp.getClass().getSimpleName());
			}
		} else {
			if (log.isDebugEnabled()) {
				log.debug("Returning cached interpolator of type " + interp.getClass().getSimpleName());
			}
		}
		return interp;
	}

	@Override
    public void invalidate() {
		cache.clear();
    }
}
