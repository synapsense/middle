package com.synapsense.liveimaging.interpolation.methods;

import java.util.HashMap;

import com.synapsense.liveimaging.draw.Point2F;
import com.synapsense.liveimaging.interpolation.Samples;

public class CachedOrdKriging extends OrdKriging {

	private HashMap<Point2F, float[]> eqResMap = new HashMap<Point2F, float[]>();

	public CachedOrdKriging(Samples samples) {
		super(samples);
	}

	public CachedOrdKriging() {
	}

	@Override
	public float[] interpolate(float[] x, float[] y, Samples samples) {
		float[] res = new float[x.length];
		for (int i = 0; i < x.length; i++) {
			res[i] = sumResult(solveEquations(x[i], y[i], samples), samples);
		}
		rewind();
		return res;
	}

	@Override
	public boolean isCacheable() {
		return true;
	}

	@Override
	protected float[] solveEquations(float x, float y, Samples samples) {
		Point2F point = new Point2F(x, y);
		float[] eqRes = eqResMap.get(point);
		if (eqRes == null) {
			initSamples(samples);
			eqRes = super.solveEquations(x, y, samples);
			eqResMap.put(point, eqRes);
		}
		return eqRes;
	}
}
