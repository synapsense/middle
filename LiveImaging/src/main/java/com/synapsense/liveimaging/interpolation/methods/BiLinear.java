package com.synapsense.liveimaging.interpolation.methods;

import com.synapsense.liveimaging.draw.Point2F;
import com.synapsense.liveimaging.draw.Point2I;
import com.synapsense.liveimaging.interpolation.Interpolator;
import com.synapsense.liveimaging.interpolation.Samples;
import com.synapsense.liveimaging.interpolation.methods.util.NeighborBinarySearcher;

public class BiLinear implements Interpolator {

	// grid points
	private float[] xGrid;
	private float[] yGrid;

	// grid points for interpolation
	private float[] xInter;
	private float[] yInter;

	private NeighborBinarySearcher xSearcher;
	private NeighborBinarySearcher ySearcher;

	private int gridSize = 40;

	private Interpolator gridBuilder;

	public BiLinear() {
		gridBuilder = createGridBuilder();
	}

	@Override
	public float[] interpolate(float[] x, float[] y, Samples samples) {
		buildGrid(samples);
		return interpolate(x, y, gridBuilder.interpolate(xInter, yInter, samples));
	}

	@Override
	public float[] interpolate(float[] x, float[] y) {
		return interpolate(x, y, gridBuilder.interpolate(xInter, yInter));
	}

	private float[] interpolate(float[] x, float[] y, float[] gridVals) {
		float[] result = new float[x.length];
		for (int i = 0; i < x.length; i++) {
			result[i] = interpolatePoint(x[i], y[i], gridVals);
		}
		return result;
	}

	@Override
	public boolean isCacheable() {
		return false;
	}

	public void setGridSize(int gridSize) {
		this.gridSize = gridSize;
	}

	protected Interpolator createGridBuilder() {
		return new OrdKriging();
	}

	private float interpolatePoint(float x, float y, float[] gridVals) {
		// Search x coordinates of the square
		Point2I searchRes = xSearcher.search(x);
		int i1 = searchRes.getX();
		int i2 = searchRes.getY();

		// Search y coordinates of the square
		searchRes = ySearcher.search(y);
		int j1 = searchRes.getX();
		int j2 = searchRes.getY();

		// Calculate function value
		float denominator = (xGrid[i2] - xGrid[i1]) * (yGrid[j2] - yGrid[j1]);
		float q11 = gridVals[i1 * xGrid.length + j1] * (xGrid[i2] - x) * (yGrid[j2] - y);
		float q12 = gridVals[i1 * xGrid.length + j2] * (xGrid[i2] - x) * (y - yGrid[j1]);
		float q21 = gridVals[i2 * xGrid.length + j1] * (x - xGrid[i1]) * (yGrid[j2] - y);
		float q22 = gridVals[i2 * xGrid.length + j2] * (x - xGrid[i1]) * (y - yGrid[j1]);

		return (q11 + q12 + q21 + q22) / denominator;
	}

	private void buildGrid(Samples samples) {
		if (xGrid != null) {
			return;
		}
		xGrid = new float[gridSize + 1];
		yGrid = new float[gridSize + 1];

		Point2F leftBound = samples.getLeftBound();
		Point2F rightBound = samples.getRightBound();

		xSearcher = buildGridOnDimension(xGrid, leftBound.getX(), rightBound.getX());
		ySearcher = buildGridOnDimension(yGrid, leftBound.getY(), rightBound.getY());

		xInter = new float[(gridSize + 1) * (gridSize + 1)];
		yInter = new float[(gridSize + 1) * (gridSize + 1)];
		for (int i = 0; i < xGrid.length; i++) {
			for (int j = 0; j < yGrid.length; j++) {
				xInter[i * xGrid.length + j] = xGrid[i];
				yInter[i * xGrid.length + j] = yGrid[j];
			}
		}
	}

	private NeighborBinarySearcher buildGridOnDimension(float[] grid, float x1, float x2) {
		float step = (x2 - x1) / gridSize;
		int i = 0;
		for (float x = x1; x <= x2; x += step) {
			grid[i++] = x;
		}
		return new NeighborBinarySearcher(grid);
	}
}
