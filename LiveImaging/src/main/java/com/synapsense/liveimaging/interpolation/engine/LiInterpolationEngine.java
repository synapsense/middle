package com.synapsense.liveimaging.interpolation.engine;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.sun.jna.Native;
import com.synapsense.liveimaging.interpolation.InterpolationFactory;
import com.synapsense.liveimaging.interpolation.InterpolatorConf;
import com.synapsense.liveimaging.interpolation.methods.CachedBiLinear;
import com.synapsense.liveimaging.interpolation.methods.NaturalNeighbor;
import com.synapsense.liveimaging.interpolation.methods.GpuOrdKriging.CublasLibrary;
import com.synapsense.liveimaging.interpolation.methods.GpuOrdKriging.CulaLibrary;

public final class LiInterpolationEngine {

	Map<Integer, Class<?>> interpolators = new TreeMap<Integer, Class<?>>();
	Map<Integer, Map<String, Integer>> allproperties = new HashMap<Integer, Map<String, Integer>>();

	static int gpuThreshold = 100;
	static int gpuGridSize = 60;
	static CulaLibrary culaLib;
	static CublasLibrary cublasLib;

	private static final Log log = LogFactory.getLog(LiInterpolationEngine.class);

	private static boolean gpuCheck = true;

	public LiInterpolationEngine(Collection<InterpolatorConf> interpConf) {
		if (interpConf != null) {
			for (InterpolatorConf inter : interpConf) {
				Map<String, Integer> props = new HashMap<String, Integer>();
				props.putAll(inter.getProperties());
				try {
					addCpuInterpolator(inter.getLimit(), Class.forName(inter.getClazz()), props);
				} catch (ClassNotFoundException e) {
					log.fatal("Specified interpolator class " + inter.getClazz()
					        + " not found in the class path, exiting...");
					throw new RuntimeException(e);
				}
			}
		}
	}

	public InterpolationFactory getInterpolationFactory() {
		if (!gpuCheck) {
			return new CpuInterpolationFactory(interpolators, allproperties);
		}

		// Load CULA library
		try {
			culaLib = (CulaLibrary) Native.loadLibrary("cula", CulaLibrary.class);
		} catch (UnsatisfiedLinkError e) {
			log.info("Cula library is not found, all the calculations will be performed on CPU");
			return new CpuInterpolationFactory(interpolators, allproperties);
		}

		// Init CULA
		int s = culaLib.culaInitialize();
		if (s != 0) {
			log.info("Unable to initialize CULA library, all the calculations will be performed on CPU");
			return new CpuInterpolationFactory(interpolators, allproperties);
		}

		// Load cublas
		try {
			cublasLib = (CublasLibrary) Native.loadLibrary("cublas64_31_9", CublasLibrary.class);
		} catch (UnsatisfiedLinkError e) {
			log.info("Cublas library is not found, all the calculations will be performed on CPU");
			return new CpuInterpolationFactory(interpolators, allproperties);
		}

		// Init cublas
		s = cublasLib.cublasInit();
		if (s != 0) {
			log.info("Unable to initialize CUBLAS library, all the calculations will  be performed on CPU");
			return new CpuInterpolationFactory(interpolators, allproperties);
		}

		log.info("GPU libraries loaded successfully");
		return new GpuInterpolationFactory();
	}

	public void addCpuInterpolator(int pointLimit, Class<?> clazz, Map<String, Integer> properties) {
		interpolators.put(pointLimit, clazz);
		allproperties.put(pointLimit, properties);
	}

	public static void setGpuThreshold(int gpuThreshold) {
		LiInterpolationEngine.gpuThreshold = gpuThreshold;
	}

	public static void setGpuGridSize(int gpuGridSize) {
		LiInterpolationEngine.gpuGridSize = gpuGridSize;
	}

	public static void setGpuCheck(boolean gpuCheck) {
		LiInterpolationEngine.gpuCheck = gpuCheck;
	}

	public void loadDefaultInterpolators() {
		Map<String, Integer> props = new HashMap<String, Integer>();
		props.put("gridSize", 150);
		addCpuInterpolator(100, CachedBiLinear.class, props);
		props = new HashMap<String, Integer>();
		props.put("gridSize", 60);
		addCpuInterpolator(300, CachedBiLinear.class, props);
		props = new HashMap<String, Integer>();
		props.put("gridSize", 40);
		addCpuInterpolator(2000, CachedBiLinear.class, props);
		addCpuInterpolator(100000, NaturalNeighbor.class, props);
	}

	public CachedInterpolationFactory getCachedInterpolationFactory() {
		return new CachedInterpolationFactory(getInterpolationFactory());

	}
}
