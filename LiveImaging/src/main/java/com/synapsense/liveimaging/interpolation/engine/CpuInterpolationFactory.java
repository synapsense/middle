package com.synapsense.liveimaging.interpolation.engine;

import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import com.synapsense.liveimaging.interpolation.InterpolationException;
import com.synapsense.liveimaging.interpolation.InterpolationFactory;
import com.synapsense.liveimaging.interpolation.Interpolator;
import com.synapsense.liveimaging.interpolation.Samples;

public class CpuInterpolationFactory implements InterpolationFactory {
	private Map<Integer, Class<?>> interpolators = new TreeMap<Integer, Class<?>>();
	private Map<Integer, Map<String, Integer>> allproperties = new HashMap<Integer, Map<String, Integer>>();

	public CpuInterpolationFactory(Map<Integer, Class<?>> interpolators,
	        Map<Integer, Map<String, Integer>> allproperties) {
		this.interpolators.putAll(interpolators);
		this.allproperties.putAll(allproperties);
	}

	public Interpolator getInterpolator(Samples samples) throws InterpolationException {

		for (Map.Entry<Integer, Class<?>> interpEntry : interpolators.entrySet()) {
			if (samples.size() <= interpEntry.getKey()) {
				try {
					Interpolator interp = (Interpolator) interpEntry.getValue().newInstance();
					Map<String, Integer> props = allproperties.get(interpEntry.getKey());
					if (props != null) {
						for (Map.Entry<String, Integer> propEntry : props.entrySet()) {
							BeanInfo beanInfo = Introspector.getBeanInfo(interpEntry.getValue());
							boolean propertyFound = false;
							for (PropertyDescriptor descr : beanInfo.getPropertyDescriptors()) {
								if (descr.getName().equals(propEntry.getKey())) {
									Method method = descr.getWriteMethod();
									if (method != null) {
										method.invoke(interp, propEntry.getValue());
										propertyFound = true;
									}
								}
							}
							if (!propertyFound) {
								throw new InterpolationException("Setter method not found for property");
							}
						}
					}
					return interp;
				} catch (Exception e) {
					throw new InterpolationException("Unable to instantiate interpolator - " + e.getMessage(), e);
				}
			}
		}
		throw new InterpolationException("No interpolator found for " + samples.size()
		        + " number of points. Check configuration");
	}
}
