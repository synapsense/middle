package com.synapsense.liveimaging.interpolation.methods;

import com.synapsense.liveimaging.interpolation.Interpolator;

public class CachedBiLinear extends BiLinear {

	public CachedBiLinear() {
		super();
	}

	@Override
	public boolean isCacheable() {
		return true;
	}

	@Override
	protected Interpolator createGridBuilder() {
		return new CachedOrdKriging();
	}
}
