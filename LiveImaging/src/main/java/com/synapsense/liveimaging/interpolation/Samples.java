package com.synapsense.liveimaging.interpolation;

import com.synapsense.liveimaging.draw.Point2F;
import com.synapsense.liveimaging.draw.ValuePoint;

public interface Samples extends Iterable<ValuePoint> {

	ValuePoint get(int index);

	Point2F getLeftBound();

	Point2F getRightBound();

	int size();
}