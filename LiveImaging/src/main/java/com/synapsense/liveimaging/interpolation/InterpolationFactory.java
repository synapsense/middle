package com.synapsense.liveimaging.interpolation;

public interface InterpolationFactory {

	Interpolator getInterpolator(Samples samples) throws InterpolationException;
}
