package com.synapsense.liveimaging.interpolation.methods;

import com.synapsense.liveimaging.draw.ValuePoint;
import com.synapsense.liveimaging.interpolation.Interpolator;
import com.synapsense.liveimaging.interpolation.Samples;

import edu.mines.jtk.interp.SibsonInterpolator2;
import edu.mines.jtk.interp.SibsonInterpolator2.Method;

public class NaturalNeighbor implements Interpolator {

	private SibsonInterpolator2 interp;

	public NaturalNeighbor(Samples samples) {
		loadSibson(samples);
	}

	public NaturalNeighbor() {
	}

	@Override
	public float[] interpolate(float[] x, float[] y) {
		float[] ret = new float[x.length];
		for (int i = 0; i < ret.length; i++) {
			ret[i] = interp.interpolate(x[i], y[i]);
		}
		return ret;
	}

	@Override
	public float[] interpolate(float[] x, float[] y, Samples samples) {
		loadSibson(samples);
		return interpolate(x, y);
	}

	@Override
	public boolean isCacheable() {
		return false;
	}

	private void loadSibson(Samples samples) {
		float[] x = new float[samples.size()];
		float[] y = new float[samples.size()];
		float[] f = new float[samples.size()];
		int i = 0;
		for (ValuePoint sample : samples) {
			x[i] = floatValue(sample.getX());
			y[i] = floatValue(sample.getY());
			f[i++] = floatValue(sample.getValue());
		}
		interp = new SibsonInterpolator2(Method.WATSON_SAMBRIDGE, f, x, y);
		interp.setBounds(floatValue(samples.getLeftBound().getX()), floatValue(samples.getRightBound().getX()),
		        floatValue(samples.getLeftBound().getY()), floatValue(samples.getRightBound().getY()));
	}

	private static float floatValue(double val) {
		return new Double(val).floatValue();
	}
}
