package com.synapsense.liveimaging.interpolation.methods;

import com.synapsense.liveimaging.draw.ValuePoint;
import com.synapsense.liveimaging.interpolation.Interpolator;
import com.synapsense.liveimaging.interpolation.Samples;
import com.synapsense.liveimaging.interpolation.methods.util.LUDecomposition;
import com.synapsense.liveimaging.interpolation.methods.util.Matrix;

public class OrdKriging implements Interpolator {

	private LUDecomposition lu;
	private Samples samples;

	public OrdKriging(Samples samples) {
		initSamples(samples);
	}

	public OrdKriging() {
	}

	@Override
	public boolean isCacheable() {
		return false;
	}

	@Override
	public float[] interpolate(float[] x, float[] y, Samples samples) {
		initSamples(samples);
		return interpolate(x, y);
	}

	@Override
	public float[] interpolate(float[] x, float[] y) {
		float[] ret = new float[x.length];
		for (int i = 0; i < ret.length; i++) {
			ret[i] = sumResult(solveEquations(x[i], y[i], this.samples), this.samples);
		}
		return ret;
	}

	protected float[] solveEquations(float x, float y, Samples samples) {
		float[] dist = new float[samples.size() + 1];
		int i = 0;
		for (ValuePoint point : samples) {
			dist[i++] = point.getDistance(x, y);
		}
		dist[samples.size()] = 1;
		return lu.solve(dist);
	}

	protected float sumResult(float[] eqRes, Samples samples) {
		float z = 0;
		int i = 0;
		for (ValuePoint point : samples) {
			z += eqRes[i++] * point.getValue();
		}
		return z;
	}

	private void luFactorization() {
		int size = samples.size();
		Matrix matrix = new Matrix(size + 1, size + 1);
		for (int j = 0; j < size + 1; j++) {
			for (int i = 0; i < size + 1; i++) {

				// Check for ordinary kriging border condition, set to right
				// values
				if ((i == size) || (j == size)) {
					matrix.set(i, j, 1);

					if ((i == size) && (j == size)) {
						matrix.set(i, j, 0);
					}
					continue;
				}
				// Set normally
				matrix.set(i, j, samples.get(i).getDistance(samples.get(j)));
			}
		}
		lu = matrix.lu();
	}

	protected void rewind() {
		lu = null;
	}

	protected void initSamples(Samples samples) {
		if (lu == null) {
			this.samples = samples;
			luFactorization();
		}
	}
}
