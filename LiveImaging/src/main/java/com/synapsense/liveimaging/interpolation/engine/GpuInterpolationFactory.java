package com.synapsense.liveimaging.interpolation.engine;

import static com.synapsense.liveimaging.interpolation.engine.LiInterpolationEngine.cublasLib;
import static com.synapsense.liveimaging.interpolation.engine.LiInterpolationEngine.culaLib;
import static com.synapsense.liveimaging.interpolation.engine.LiInterpolationEngine.gpuGridSize;
import static com.synapsense.liveimaging.interpolation.engine.LiInterpolationEngine.gpuThreshold;

import com.synapsense.liveimaging.interpolation.InterpolationFactory;
import com.synapsense.liveimaging.interpolation.Interpolator;
import com.synapsense.liveimaging.interpolation.Samples;
import com.synapsense.liveimaging.interpolation.methods.BiLinear;
import com.synapsense.liveimaging.interpolation.methods.GpuOrdKriging;

public class GpuInterpolationFactory implements InterpolationFactory {

	@Override
	public Interpolator getInterpolator(Samples samples) {
		final GpuOrdKriging gpuMain = new GpuOrdKriging(samples, cublasLib, culaLib);
		if (samples.size() < gpuThreshold) {
			return gpuMain;
		}

		BiLinear interpolator = new BiLinear() {

			@Override
			protected Interpolator createGridBuilder() {
				return gpuMain;
			}
		};
		interpolator.setGridSize(gpuGridSize);
		return interpolator;
	}
}