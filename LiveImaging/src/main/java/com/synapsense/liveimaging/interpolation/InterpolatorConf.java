package com.synapsense.liveimaging.interpolation;

import java.util.HashMap;
import java.util.Map;

public class InterpolatorConf {

	protected Map<String, Integer> properties = new HashMap<String, Integer>();
	protected String name;
	protected String clazz;
	protected Integer limit;

	public Map<String, Integer> getProperties() {

		return this.properties;
	}

	public String getName() {
		return name;
	}

	public void setName(String value) {
		this.name = value;
	}

	public String getClazz() {
		return clazz;
	}

	public void setClazz(String value) {
		this.clazz = value;
	}

	public Integer getLimit() {
		return limit;
	}

	public void setLimit(Integer value) {
		this.limit = value;
	}

	public void setProperties(Map<String, Integer> properties) {
		this.properties = properties;
	}
}