package com.synapsense.liveimaging.interpolation.methods;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.sun.jna.Library;
import com.sun.jna.Pointer;
import com.synapsense.liveimaging.interpolation.Interpolator;
import com.synapsense.liveimaging.interpolation.Samples;

public class GpuOrdKriging implements Interpolator {

	private static final Log log = LogFactory.getLog(GpuOrdKriging.class);

	private float[] luMatr;
	private int[] pivot;
	private int size;

	private CublasLibrary cublasLib;
	private CulaLibrary culaLib;

	private Samples samples;

	public static interface CulaLibrary extends Library {

		int culaInitialize();

		void culaShutdown();

		int culaSgetrf(int m, int n, float[] lu, int lda, int[] p);

		int culaSgetrs(char trans, int n, int nhrs, float[] a, int lda, int[] ipiv, float[] b);

		String culaGetStatusString(int s);

		int culaGetErrorInfo();
	}

	public static interface CublasLibrary extends Library {

		int cublasInit();

		int cublasShutdown();

		int cublasAlloc(int n, int elemSize, Pointer[] devicePtr);

		int cublasFree(Pointer devicePtr);

		int cublasSetVector(int n, int elemSize, float[] x, int incx, Pointer y, int incy);

		int cublasGetVector(int n, int elemSize, Pointer x, int incx, float[] y, int incy);

		int cublasStrsm(char side, char uplo, char transa, char diag, int m, int n, float alpha, Pointer A, int lda,
		        Pointer B, int ldb);
	}

	public GpuOrdKriging(Samples samples, CublasLibrary cublasLib, CulaLibrary culaLib) {
		this.cublasLib = cublasLib;
		this.culaLib = culaLib;
		this.samples = samples;
		luFactorization();
	}

	@Override
	public float[] interpolate(float[] x, float[] y, Samples samples) {
		this.samples = samples;
		luFactorization();
		return interpolate(x, y);
	}

	@Override
	public float[] interpolate(float[] x, float[] y) {
		float[] ret = new float[x.length];
		float[] dist = new float[(x.length) * (size + 1)];
		for (int j = 0; j < x.length; j++) {
			ret[j] = 0F;
			int i = 0;
			float[] dPiv = new float[size + 1];
			for (i = 0; i < size; i++) {
				dPiv[i] = (float) samples.get(i).getDistance(x[j], y[j]);
			}
			dPiv[size] = 1;
			for (int h = 0; h < size + 1; h++) {
				dist[j * (size + 1) + h] = dPiv[pivot[h]];
			}
		}

		int floatSize = Float.SIZE / Byte.SIZE;

		Pointer[] mA = new Pointer[1];
		Pointer[] mDist = new Pointer[1];
		cublasLib.cublasAlloc((size + 1) * (size + 1), floatSize, mA);
		cublasLib.cublasAlloc((size + 1) * x.length, floatSize, mDist);

		cublasLib.cublasSetVector((size + 1) * (size + 1), floatSize, luMatr, 1, mA[0], 1);
		cublasLib.cublasSetVector((size + 1) * (x.length), floatSize, dist, 1, mDist[0], 1);

		cublasLib.cublasStrsm('l', 'l', 'n', 'u', size + 1, x.length, 1.0f, mA[0], size + 1, mDist[0], size + 1);
		cublasLib.cublasStrsm('l', 'u', 'n', 'n', size + 1, x.length, 1.0f, mA[0], size + 1, mDist[0], size + 1);

		cublasLib.cublasGetVector((size + 1) * (x.length), floatSize, mDist[0], 1, dist, 1);

		cublasLib.cublasFree(mA[0]);
		cublasLib.cublasFree(mDist[0]);

		for (int i = 0; i < ret.length; i++) {
			for (int j = 0; j < size; j++) {
				ret[i] += samples.get(j).getValue() * dist[(size + 1) * i + j];
			}
		}
		return ret;
	}

	@Override
	public boolean isCacheable() {
		return false;
	}

	private void luFactorization() {
		luMatr = new float[(size + 1) * (size + 1)];
		for (int j = 0; j < size + 1; j++) {
			for (int i = 0; i < size + 1; i++) {

				// Check for ordinary kriging border condition, set to right
				// values
				if ((i == size) || (j == size)) {
					luMatr[i * (size + 1) + j] = 1;

					if ((i == size) && (j == size)) {
						luMatr[i * (size + 1) + j] = 0;
					}
					continue;
				}
				// Set normally
				luMatr[i * (size + 1) + j] = new Double(samples.get(i).getDistance(samples.get(j))).floatValue();
			}
		}
		int[] p = new int[size + 1];

		int s = culaLib.culaSgetrf(size + 1, size + 1, luMatr, size + 1, p);
		if (s != 0) {
			log.error("LU CUDA error " + culaLib.culaGetErrorInfo());
		}

		pivot = new int[size + 1];
		for (int i = 0; i < size + 1; i++) {
			pivot[i] = i;
		}

		for (int i = 0; i < size + 1; i++) {
			if (i != p[i] - 1) {
				int temp = pivot[i];
				pivot[i] = pivot[p[i] - 1];
				pivot[p[i] - 1] = temp;
			}
		}
	}
}
