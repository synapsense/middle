package com.synapsense.liveimaging.interpolation;

import com.synapsense.liveimaging.LiveImagingException;

public class InterpolationException extends LiveImagingException {

	private static final long serialVersionUID = -4320547116121986611L;

	public InterpolationException() {
		super();
	}

	public InterpolationException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public InterpolationException(String arg0) {
		super(arg0);
	}

	public InterpolationException(Throwable arg0) {
		super(arg0);
	}
}
