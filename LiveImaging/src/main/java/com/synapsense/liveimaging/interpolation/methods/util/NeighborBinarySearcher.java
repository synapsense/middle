package com.synapsense.liveimaging.interpolation.methods.util;

import com.synapsense.liveimaging.draw.Point2I;

public class NeighborBinarySearcher {

	private float[] values;

	public NeighborBinarySearcher(float[] values) {
		this.values = values;
	}

	public float[] getValues() {
		return values;
	}

	public Point2I search(float val) {
		int left = 0;
		int right = values.length - 1;
		int mid = 0;
		do {
			mid = (left + right) / 2;
			if (values[mid] < val) {
				left = mid;
			} else {
				right = mid;
			}
		} while (left < right - 1);
		return new Point2I(left, right);
	}
}
