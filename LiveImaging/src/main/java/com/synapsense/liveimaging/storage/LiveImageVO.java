package com.synapsense.liveimaging.storage;

import com.synapsense.liveimaging.conf.LiConstants;
import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonAutoDetect.Visibility;
import org.codehaus.jackson.annotate.JsonProperty;

import java.awt.image.BufferedImage;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Pattern;

@JsonAutoDetect(fieldVisibility = Visibility.NONE, getterVisibility = Visibility.NONE, setterVisibility = Visibility.NONE)
public final class LiveImageVO {

	private static Pattern resourcePattern = Pattern
	        .compile("^.+\\-\\d{4}\\-\\d{2}\\-\\d{2}\\-\\d{2}_\\d{2}_\\d{2}\\.(png|jpg)$");

	private static Pattern imageIdPattern = Pattern.compile(".+\\-\\d+\\-\\d+\\-\\d+");

	private static String DATE_FORMAT = "yyyy-MM-dd-HH_mm_ss";

	// [8127] Since SimpleDateFormat is not thread safe it's necessary to make
	// it thread local
	private static final ThreadLocal<SimpleDateFormat> FORMATTER = new ThreadLocal<SimpleDateFormat>() {
		protected SimpleDateFormat initialValue() {
			return new SimpleDateFormat(DATE_FORMAT);
		}
	};

	private String imageId;

	private BufferedImage image;

	@JsonProperty("timestamp")
	private Date showTime;

	@JsonProperty("layer")
	private int layer;

	@JsonProperty("dataclass")
	private int dataclass;

	private ImageType imageType;

	@JsonProperty("legend")
	private LegendImageVO legend;

	private boolean historical = false;

	private Date creationDate;

	public static LiveImageVO load(String resourceName) {
		// Check if the resource name is in correct format
		if (!resourcePattern.matcher(resourceName).matches()) {
			return null;
		}
		int dotPos = resourceName.lastIndexOf('.');
		boolean hist = resourceName.contains(LiConstants.HIST_IMAGE_PREFIX);
		try {
			return new LiveImageVO(resourceName.substring(0, dotPos - 20), null, FORMATTER.get().parse(
			        resourceName.substring(dotPos - 19, dotPos)), resourceName.substring(dotPos + 1), hist);
		} catch (ParseException e) {
			// Should never occur since the string matches the pattern
			throw new IllegalArgumentException("Resource name" + resourceName + " is in incorrect format ", e);
		}
	}

	public LiveImageVO(String imageId, BufferedImage image, Date showTime, String type) {
		this.imageId = imageId;
		this.image = image;
		this.showTime = showTime;
		if (type != null) {
			this.imageType = ImageType.valueOf(type.toUpperCase());
		}
		if (!imageIdPattern.matcher(imageId).matches()) {
			throw new IllegalArgumentException("Image id[" + imageId + "] is in incorrect format");
		}
		String[] partName = imageId.split("-");
		layer = Integer.parseInt(partName[3]);
		dataclass = Integer.parseInt(partName[2]);

	}

	public LiveImageVO(String imageId, BufferedImage image, Date showTime, String type, boolean historical) {
		this(imageId, image, showTime, type);
		this.historical = historical;
	}

	// Only for JSON responses. Flash doesn't understand default system
	// separator
	@JsonProperty("url")
	private String getResourcePathAsURL() {
		String url = getResourceName();
		return url.replace(LiConstants.SEPARATOR, "/");
	}

	public String getResourceName() {
		return new StringBuilder(imageId).append("-").append(FORMATTER.get().format(showTime)).append(".")
		        .append(imageType).toString();

	}

	public Date getShowTime() {
		return showTime;
	}

	public BufferedImage getImage() {
		return image;
	}

	public void setImage(BufferedImage image) {
		this.image = image;
	}

	public String getImageId() {
		return imageId;
	}

	public ImageType getImageType() {
		return imageType;
	}

	public int getLayer() {
		return layer;
	}

	public void setLayer(int layer) {
		this.layer = layer;
	}

	public int getDataclass() {
		return dataclass;
	}

	public void setDataclass(int dataclass) {
		this.dataclass = dataclass;
	}

	public void setCreationDate(Date date) {
		this.creationDate = date;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public static enum ImageType {
	PNG, JPG;

	@Override
	public String toString() {
		return name().toLowerCase();
	}
	}

	public boolean isHistorical() {
		return historical;
	}

	@Override
	public String toString() {
		return "LiveImageVO [imageId=" + imageId + ", showTime=" + showTime + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((imageId == null) ? 0 : imageId.hashCode());
		result = prime * result + ((showTime == null) ? 0 : showTime.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LiveImageVO other = (LiveImageVO) obj;
		if (imageId == null) {
			if (other.imageId != null)
				return false;
		} else if (!imageId.equals(other.imageId))
			return false;
		if (showTime == null) {
			if (other.showTime != null)
				return false;
		} else if (!showTime.equals(other.showTime))
			return false;
		if (this.isHistorical() != other.isHistorical()) {
			return false;
		}
		return true;
	}

	public LegendImageVO getLegend() {
		return legend;
	}

	public void setLegend(LegendImageVO legend) {
		this.legend = legend;
	}

}
