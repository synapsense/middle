package com.synapsense.liveimaging.storage;

import java.util.Date;
import java.util.List;

public interface ImageDataStorageReader {

	List<LiveImageVO> getImages(Date startDate, Date endDate);

	List<LiveImageVO> getAllHistoricalImages();
}
