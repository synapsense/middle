package com.synapsense.liveimaging.storage;

import com.synapsense.liveimaging.conf.LiConstants;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonProperty;

import java.net.MalformedURLException;
import java.net.URL;
import java.time.Instant;

/**
 * Created by WFI on 2/10/2016.
 */
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.NONE,
                getterVisibility = JsonAutoDetect.Visibility.NONE,
                setterVisibility = JsonAutoDetect.Visibility.NONE)
public class ImageVO {
    private final LiveImageVO liveImage;
    private final URL baseURL;

    private static final Log log = LogFactory.getLog(ImageVO.class);

    public static ImageVO load(String resourceName) {
        LiveImageVO li = LiveImageVO.load(resourceName);
        if (li == null) {
            return null;
        }
        URL baseURL = null;
        try {
            baseURL = new URL("http", "test.com", 1234, "");
        } catch (MalformedURLException e) {
        }
        return new ImageVO(baseURL, li);
    }

    public ImageVO(final URL baseURL, final LiveImageVO liveImage) {
        this.liveImage = liveImage;
        this.baseURL = baseURL;
    }

    @JsonProperty("instant")
    public Instant getInstant() {
        return Instant.ofEpochMilli(liveImage.getShowTime().getTime());
    }

    @JsonProperty("timestamp")
    public Long getTimeStamp() {
        return liveImage.getShowTime().getTime();
    }

    @JsonProperty("legendUrl")
    public URL getLegendUrl() throws MalformedURLException {
        return new URL(baseURL, liveImage.getLegend().getResorcePath().replace(LiConstants.SEPARATOR, "/"));
    }

    @JsonProperty("legendUrlSi")
    public URL getLegendUrlSi() throws MalformedURLException {
        return new URL(baseURL, liveImage.getLegend().getSiResourcePath().replace(LiConstants.SEPARATOR, "/"));
    }

    @JsonProperty("url")
    public URL getUrl() throws MalformedURLException {
        return new URL(baseURL, liveImage.getResourceName().replace(LiConstants.SEPARATOR, "/"));
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("ImageVO [instant=" + getInstant().toString());

        try {
            sb.append(", url=" + getUrl().toString());
            sb.append(", legendUrl=" + getLegendUrl().toString());
            sb.append(", legendUrlSi=" + getLegendUrlSi().toString());
        } catch (MalformedURLException e) {
            log.debug("MalformedURLException: " + e.toString());
        }

        sb.append("]");
        return sb.toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((liveImage == null) ? 0 : liveImage.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }

        ImageVO other = (ImageVO) obj;
        if (liveImage == null) {
            if (other.liveImage != null) {
                return false;
            }
        } else if (!liveImage.equals(other.liveImage)) {
            return false;
        }

        return true;
    }

}
