package com.synapsense.liveimaging.storage;

import com.synapsense.liveimaging.LiveImagingException;

public class StorageException extends LiveImagingException {

	private static final long serialVersionUID = 6127025026189487333L;

	public StorageException() {
	}

	public StorageException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public StorageException(String arg0) {
		super(arg0);
	}

	public StorageException(Throwable arg0) {
		super(arg0);
	}
}
