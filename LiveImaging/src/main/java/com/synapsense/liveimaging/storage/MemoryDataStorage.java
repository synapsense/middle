package com.synapsense.liveimaging.storage;

import com.synapsense.dto.TO;
import com.synapsense.liveimaging.conf.LiConstants;
import com.synapsense.liveimaging.legend.LegendConstants;
import com.synapsense.liveimaging.web.smartzone.LiveImageType;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.threeten.extra.Interval;

import java.io.File;
import java.io.FilenameFilter;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.concurrent.ConcurrentSkipListSet;

public class MemoryDataStorage implements ImageDataStorageWriter, ImageDataStorageReader {
	private String outputDir = LiConstants.OUTPUT_FOLDER;
	private String SEPARATOR = LiConstants.SEPARATOR;

	private Map<String, Map<Integer, SortedSet<LiveImageVO>>> imageStorage;
	private Map<Integer, SortedSet<LegendImageVO>> legendStorage;

	private Comparator<LiveImageVO> imageComparator;
	private Comparator<LegendImageVO> legendComparator;

	private static final Log log = LogFactory.getLog(MemoryDataStorage.class);

	public MemoryDataStorage(String outputDir) {
		this.outputDir = outputDir;
		initStorage();
		//we should load legends before loading of images
		loadLegends();
		loadImages();

	}

	private void initStorage() {
		imageStorage = new HashMap<String, Map<Integer, SortedSet<LiveImageVO>>>();
		imageComparator = new Comparator<LiveImageVO>() {

			@Override
			public int compare(LiveImageVO o1, LiveImageVO o2) {
				if (o1.getShowTime().before(o2.getShowTime())) {
					return -1;
				} else if (o1.getShowTime().after(o2.getShowTime())) {
					return 1;
				} else {
					return 0;
				}
			}

		};

		legendStorage = new HashMap<Integer, SortedSet<LegendImageVO>>();
		legendComparator = new Comparator<LegendImageVO>() {

			@Override
			public int compare(LegendImageVO o1, LegendImageVO o2) {
				if (o1.getShowTime().before(o2.getShowTime())) {
					return -1;
				} else if (o1.getShowTime().after(o2.getShowTime())) {
					return 1;
				} else {
					return 0;
				}
			}

		};
	}

	public void loadImages() {
		log.info("Loading available images");

		File root = new File(outputDir);
		File[] area = root.listFiles(new FilenameFilter() {

			@Override
			public boolean accept(File dir, String name) {
				if (name.equalsIgnoreCase(LiConstants.DC) || name.equalsIgnoreCase(LiConstants.ROOM)) {
					return true;
				} else {
					return false;
				}
			}

		});
		if (area != null) {
			for (int i = 0; i < area.length; i++) {
				File[] ids = area[i].listFiles();
				for (int k = 0; k < ids.length; k++) {
					File[] layers = ids[k].listFiles();

					for (int n = 0; n < layers.length; n++) {
						Map<Integer, SortedSet<LiveImageVO>> dataclasses = new HashMap<Integer, SortedSet<LiveImageVO>>();
						File[] images = layers[n].listFiles();

						for (int j = 0; j < images.length; j++) {
							File imageFile = images[j];
							LiveImageVO liveimage = LiveImageVO.load(buildKey(area[i].getName(), ids[k].getName(),
							        layers[n].getName()) + SEPARATOR + imageFile.getName());
							if(liveimage != null) {
								liveimage.setCreationDate(new Date(imageFile.lastModified()));
								Integer dataclass = liveimage.getDataclass();
								// put links to legend into liveimage
								liveimage.setLegend(findLegend(dataclass, liveimage.getShowTime()));
								// and put image to collection
								if (dataclasses.containsKey(dataclass)) {
									dataclasses.get(dataclass).add(liveimage);
								} else {
									SortedSet<LiveImageVO> imageSet = new ConcurrentSkipListSet<LiveImageVO>(
									        imageComparator);
									imageSet.add(liveimage);
									dataclasses.put(dataclass, imageSet);
								}
							}

						}
						imageStorage.put(buildKey(area[i].getName(), ids[k].getName(), layers[n].getName()),
						        dataclasses);
					}
				}

			}
		}
	}

	public void loadLegends() {
		log.info("Loading available legends");
		for (int i = 0; i < LegendConstants.LEGEND_DATACLASSES.length; i++) {
			legendStorage.put(Integer.valueOf(LegendConstants.LEGEND_DATACLASSES[i]),
			        loadLegendsByDataClass(LegendConstants.LEGEND_DATACLASSES[i]));
		}
	}

	private SortedSet<LegendImageVO> loadLegendsByDataClass(final String dataclass) {
		SortedSet<LegendImageVO> legends = new ConcurrentSkipListSet<LegendImageVO>(legendComparator);

		File root = new File(outputDir + SEPARATOR + LiConstants.LEGEND_FOLDER);

		File[] legendFiles = root.listFiles(new FilenameFilter() {

			@Override
			public boolean accept(File dir, String name) {
				if (name.contains("legend_" + dataclass)) {
					return true;
				} else {
					return false;
				}
			}

		});
		if (legendFiles != null) {
			for (int i = 0; i < legendFiles.length; i++) {
				LegendImageVO legend = LegendImageVO.load(LiConstants.LEGEND_FOLDER + SEPARATOR
				        + legendFiles[i].getName());
				legends.add(legend);
			}
		}

		return legends;
	}

	public SortedSet<LiveImageVO> getAvailableImages(final String area, final Integer id, final Integer dataclass, final Integer layer) {
		final String key = buildKey(area, String.valueOf(id), String.valueOf(layer));
		if (imageStorage.containsKey(key)) {
			return imageStorage.get(key).get(dataclass);
		}
		return null;
	}

	public LiveImageVO getImage(TO<?> to, Integer dataclass, Integer layer, Date date) {
		SortedSet<LiveImageVO> images = getAvailableImages(to.getTypeName(), (Integer) to.getID(), dataclass, layer);
		if (images != null) {
			LiveImageVO previousImage = null;
			for (LiveImageVO image : images) {
				if (date.equals(image.getShowTime())) {
					return image;
				}
				if (image.getShowTime().before(date)) {
					previousImage = image;
				} else {
					return previousImage;
				}

			}
			return previousImage;
		}
		log.debug("Image [obj: " + to.toString() + "; layer: " + layer + "; dataclass: " + dataclass + "; showingTime: "
						  + date.toString() + "] does not exist ");
		return null;
	}

	public LiveImageVO getImage(final TO<?> to, final LiveImageType imageType, final Instant instant) {
		return getImage(to, imageType.getDataClass(), imageType.getLayer(), Date.from(instant));
	}

	public LiveImageVO getLatestImage(final TO<?> to, final Integer dataclass, final Integer layer) {
		final SortedSet<LiveImageVO> images = getAvailableImages(to.getTypeName(), (Integer) to.getID(), dataclass, layer);
		if (images != null && !images.isEmpty()) {
			return images.last();
		}

		log.debug("Image [obj: " + to.toString() + "; layer: " + layer + "; dataclass: " + dataclass + "] does not exist ");
		return null;
	}

	public LiveImageVO getLatestImage(final TO<?> to, final LiveImageType imageType) {
		return getLatestImage(to, imageType.getDataClass(), imageType.getLayer());
	}

	public List<LiveImageVO> getImages(final TO<?> to, final Integer dataclass, final Integer layer, final Date start, final Date end) {
		final List<LiveImageVO> images = new ArrayList<LiveImageVO>();
		final SortedSet<LiveImageVO> availableImages = getAvailableImages(to.getTypeName(), (Integer) to.getID(), dataclass, layer);
		if (availableImages != null) {
			for (final LiveImageVO image : availableImages) {
				final Date showDate = image.getShowTime();
				if (showDate.after(end)) {
					break;
				}
				if ((showDate.after(start) || showDate.equals(start)) && (showDate.before(end) || showDate.equals(end))) {
					images.add(image);
				}
			}
		}

		if (log.isDebugEnabled() && images.isEmpty()) {
			log.debug("Could not find images [obj: " + to.toString() + " ;dataclass: " + dataclass + "; layer:" + layer
							  + "start: " + start + "; end" + end + "]");
		}

		return images;
	}

	public List<LiveImageVO> getImages(final TO<?> to, final LiveImageType imageType, final Interval interval) {
		return getImages(to, imageType.getDataClass(), imageType.getLayer(), Date.from(interval.getStart()), Date.from(interval.getEnd()));
	}

	public List<LiveImageVO> getImages(final TO<?> to, final LiveImageType imageType, final Interval interval, final Integer maxImages) {
		List<LiveImageVO> images = getImages(to, imageType, interval);
		return filterImages(images, maxImages);
	}

	public static List<LiveImageVO> filterImages(final List<LiveImageVO> images, final Integer maxImages) {
		if (images == null) {
			return images;
		}

		float totalImgNum = images.size();
		float imgNumAfterFilter = maxImages > 0 ? maxImages : 120;
		float gap = (totalImgNum - 1) / (imgNumAfterFilter - 1);

		if (gap < 1f) {
			return images;
		} else {
			List<LiveImageVO> res = new ArrayList<LiveImageVO>();
			for (float k = 0; k < images.size(); k += gap) {
				res.add(images.get(Math.round(k)));
			}
			return res;
		}
	}


	private String extractMapKey(String imageId) {
		return imageId.substring(0, imageId.lastIndexOf(SEPARATOR));
	}

	private String buildKey(String area, String id, String layer) {
		StringBuilder builder = new StringBuilder();
		builder.append(area);
		builder.append(SEPARATOR);
		builder.append(id);
		builder.append(SEPARATOR);
		builder.append(layer);
		return builder.toString();
	}

	@Override
	public void saveLiveImage(LiveImageVO liveImage) throws StorageException {
		// I don't want to store images. Links is enough
		liveImage.setImage(null);
		// and set last legend
		liveImage.setLegend(legendStorage.get(liveImage.getDataclass()).last());
		Map<Integer, SortedSet<LiveImageVO>> dataclasses;
		if (imageStorage.containsKey(extractMapKey(liveImage.getImageId()))) {
			dataclasses = imageStorage.get(extractMapKey(liveImage.getImageId()));
		} else {
			dataclasses = new HashMap<Integer, SortedSet<LiveImageVO>>();
			imageStorage.put(extractMapKey(liveImage.getImageId()), dataclasses);
		}

		if (dataclasses.containsKey(liveImage.getDataclass())) {
			dataclasses.get(liveImage.getDataclass()).add(liveImage);
		} else {
			SortedSet<LiveImageVO> images = new ConcurrentSkipListSet<LiveImageVO>(imageComparator);
			images.add(liveImage);
			dataclasses.put(liveImage.getDataclass(), images);
		}

	}

	@Override
	public void deleteImages(Date before) throws StorageException {
		log.debug("Deleting images generated before: " + before);
		Collection<Map<Integer, SortedSet<LiveImageVO>>> allImages = imageStorage.values();
		// Iterator is used for safety removing
		Iterator<Map<Integer, SortedSet<LiveImageVO>>> allImagesIt = allImages.iterator();
		while (allImagesIt.hasNext()) {
			Map<Integer, SortedSet<LiveImageVO>> layerImages = allImagesIt.next();
			Collection<SortedSet<LiveImageVO>> dataclassImages = layerImages.values();
			Iterator<SortedSet<LiveImageVO>> datalassImagesIt = dataclassImages.iterator();
			while (datalassImagesIt.hasNext()) {
				SortedSet<LiveImageVO> images = datalassImagesIt.next();
				for (LiveImageVO image : images) {
					if (image.getShowTime().before(before)) {
						images.remove(image);
					}
				}
				// if images for dataclass do not exist, empty collection will
				// be removed
				if (images.isEmpty()) {
					datalassImagesIt.remove();
				}
			}
			// do the same for layer
			if (layerImages.isEmpty()) {
				allImagesIt.remove();
			}
		}

	}

	@Override
	public void deleteImages(Collection<LiveImageVO> images) throws StorageException {
		for (LiveImageVO image : images) {
			deleteImage(image);
		}

	}

	private void deleteImage(LiveImageVO image) {
		Map<Integer, SortedSet<LiveImageVO>> dataclassesImages = imageStorage.get(extractMapKey(image.getImageId()));
		SortedSet<LiveImageVO> images = dataclassesImages.get(image.getDataclass());
		if (images != null) {
			if (!images.remove(image)) {
				log.debug("Could not delete image[" + image + "]. Image is not found");
			}
		} else {
			log.debug("Could not delete image[" + image + "]. Image is not found");
		}
		if (images.isEmpty()) {
			dataclassesImages.remove(image.getDataclass());
			if (dataclassesImages.isEmpty()) {
				imageStorage.remove(extractMapKey(image.getImageId()));
			}
		}
	}

	@Override
	public void saveLegend(LegendImageVO legendImage) throws StorageException {
		// I don't want to store images. Links is enough
		legendImage.setSiImage(null);
		legendImage.setUsImage(null);
		Integer dataclass = legendImage.getType();
		SortedSet<LegendImageVO> legends = legendStorage.get(dataclass);
		if (legends != null) {
			legends.add(legendImage);
		} else {
			legends = new ConcurrentSkipListSet<LegendImageVO>(legendComparator);
			legends.add(legendImage);
			legendStorage.put(dataclass, legends);
		}

	}

	public SortedSet<LegendImageVO> getAvailableLegends(Integer dataclass) {
		return legendStorage.get(dataclass);
	}

	public boolean hasImage(String area, Integer id) {
		Set<String> imageKeys = imageStorage.keySet();
		for (String key : imageKeys) {
			if (key.contains(area + SEPARATOR + id + SEPARATOR)) {
				return true;
			}
		}
		return false;
	}

	public String getOutputDir() {
		return outputDir;
	}

	@Override
	public List<LiveImageVO> getImages(Date startDate, Date endDate) {
		List<LiveImageVO> images = new ArrayList<LiveImageVO>();
		Collection<Map<Integer, SortedSet<LiveImageVO>>> allImages = imageStorage.values();
		for (Map<Integer, SortedSet<LiveImageVO>> imagesSortedByDataclass : allImages) {
			Collection<SortedSet<LiveImageVO>> imagesForDataclass = imagesSortedByDataclass.values();
			for (SortedSet<LiveImageVO> imagesSet : imagesForDataclass) {
				for (LiveImageVO img : imagesSet) {
					if ((img.getShowTime().after(startDate) || img.getShowTime().equals(startDate))
					        & (img.getShowTime().before(endDate) || img.getShowTime().equals(endDate))) {
						images.add(img);
					}
				}
			}
		}
		return images;
	}

	public List<LiveImageVO> getAllHistoricalImages() {
		List<LiveImageVO> images = new ArrayList<LiveImageVO>();
		Collection<Map<Integer, SortedSet<LiveImageVO>>> allImages = imageStorage.values();
		for (Map<Integer, SortedSet<LiveImageVO>> imagesSortedByDataclass : allImages) {
			Collection<SortedSet<LiveImageVO>> imagesForDataclass = imagesSortedByDataclass.values();
			for (SortedSet<LiveImageVO> imagesSet : imagesForDataclass) {
				for (LiveImageVO img : imagesSet) {
					if (img.isHistorical()) {
						images.add(img);
					}
				}
			}

		}
		return images;
	}

	private LegendImageVO findLegend(Integer dataclass, Date date) {
		SortedSet<LegendImageVO> legends = legendStorage.get(dataclass);
		LegendImageVO l = legends.last();
		for (LegendImageVO legend : legends) {
			if (legend.getShowTime().before(date)) {
				l = legend;
			} else {
				return l;
			}
		}
		return l;
	}

}
