package com.synapsense.liveimaging.storage;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedList;
import java.util.UUID;

import javax.imageio.ImageIO;

import com.synapsense.liveimaging.Complainer;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.liveimaging.conf.LiConstants;
import com.synapsense.liveimaging.draw.ImageUtil;
import com.synapsense.liveimaging.storage.LiveImageVO.ImageType;

public class FileSystemStorage implements ImageDataStorageWriter {

	private static final Log log = LogFactory.getLog(FileSystemStorage.class);
	
	private String SEPARATOR = LiConstants.SEPARATOR;
	
	private String LEGEND_FOLDER = LiConstants.LEGEND_FOLDER;

	private String outputDir = LiConstants.OUTPUT_FOLDER;

	protected Complainer complainer;
	public void setComplainer(Complainer complainer) {
		this.complainer = complainer;
	}

	@Override
	public void saveLiveImage(LiveImageVO liveImage) throws StorageException {
		String srcFilePath = getLiveImagePath(liveImage.getResourceName());

		// Create directories if necessary
		mkDirs(srcFilePath);

		BufferedImage toDraw = liveImage.getImage();

		// Bug 5635 - Inconsistent view of jpg images
		if (liveImage.getImageType() == ImageType.JPG) {
			// Unfortunately in this case we need to be sure that the image has
			// only 3 channels
			// otherwise browsers will consider the image as CMYK and add
			toDraw = ImageUtil.copyImageToRGB(toDraw);
		}

		// Save generated image as a file
		File outFile = new File(srcFilePath);
		try {
			if (!outFile.exists()) {
				outFile.createNewFile();
			}
			RandomAccessFile file = new RandomAccessFile(outFile, "rw");
			FileChannel channel = file.getChannel();
			ByteArrayOutputStream bas = new ByteArrayOutputStream();
			ImageIO.write(toDraw, liveImage.getImageType().toString(), bas);
			byte[] data = bas.toByteArray();
			ByteBuffer buffer = ByteBuffer.wrap(data);
			channel.write(buffer);
			channel.close();
			file.close();
			liveImage.setCreationDate(new Date());
		} catch (IOException e) {
			String msg = "Cannot save image file " + outFile.getName();
			complainer.complainGeneral(msg);
			throw new StorageException(msg, e);
		}
	}

	@Override
	public void deleteImages(Date before) throws StorageException {
		File root = new File(outputDir);
		File[] dirs = root.listFiles(new FileFilter() {
			@Override
			public boolean accept(File pathname) {
				return !pathname.getName().equals(LEGEND_FOLDER)
						&& pathname.isDirectory();
			}
		});

		if (dirs == null) {
			// No files created yet
			return;
		}

		for (File dir : dirs) {
			Collection<File> toDelete = recursiveFindOldFiles(dir, before);
			for (File file : toDelete) {
				file.delete();
				if (log.isDebugEnabled()) {
					log.debug("Image file " + file.getName()
							+ " has been deleted");
				}
			}
		}
	}

	@Override
	public void deleteImages(Collection<LiveImageVO> images)
			throws StorageException {
		for (LiveImageVO image : images) {
			File imageFile = new File(getLiveImagePath(image.getResourceName()));
			if (!imageFile.delete()) {
				log.warn("Unable to delete image file "
						+ imageFile.getAbsolutePath() + ", does it exist? ");
			} else {
				if (log.isDebugEnabled()) {
					log.debug("Image file " + imageFile.getName()
							+ " has been deleted");
				}
			}
		}
	}

	// FIXME: Not yet used. Will be used after refactoring of legends generation
	@Override
	public void saveLegend(LegendImageVO legendImage) throws StorageException {
		String usPath = getLegendResourceName(
				String.valueOf(legendImage.getType()),
				legendImage.getShowTime());
		String siPath;
		try {
			saveLegendImage(usPath, legendImage.getUsImage());
		} catch (IOException e) {
			throw new StorageException("Cannot save legend file " + usPath, e);
		}
		if (legendImage.getSiImage() == null) {
			siPath = usPath;
		} else {
			siPath = getLegendResourceName(
					"si" + String.valueOf(legendImage.getType()),
					legendImage.getShowTime());
			try {
				saveLegendImage(siPath, legendImage.getSiImage());
			} catch (IOException e) {
				// necessary to delete us legend
				new File(getLegendPath(usPath)).delete();
				throw new StorageException("Cannot save legend file " + siPath,
						e);
			}
		}
		// LiveImagingService liService = ESContext.getLiService();
		// liService.addLegendImage(new LegendImage(1, legendImage.getType(),
		// usPath, siPath, legendImage.getShowTime()));
	}

	public void setOutputDir(String outputDir) throws StorageException {
		this.outputDir = outputDir;

		File out = new File(outputDir);
		out.mkdirs();

		if (!out.canWrite()) {
			throw new StorageException("Won't be able to save images to "
					+ outputDir);
		}

		// This is the easiest way to check if we can actually create and modify
		// files inside this folder
		File file = new File(out, "OutputAccessTest."
				+ UUID.randomUUID().toString());
		try {
			file.createNewFile();
			file.delete();
		} catch (IOException e) {
			String msg = "Won't be able to save images to " + outputDir;
			complainer.complainGeneral(msg);
			throw new StorageException(msg, e);
		}
	}

	public String getOutputDir() {
	    return outputDir;
    }
	
	private Collection<File> recursiveFindOldFiles(File root, Date date) {
		LinkedList<File> oldFiles = new LinkedList<File>();
		File[] children = root.listFiles();
		for (File file : children) {
			if (file.isFile()) {
				LiveImageVO li = LiveImageVO.load(file.getName());
				if (li != null && li.getShowTime().before(date)) {
					oldFiles.add(file);
				}
			} else {
				oldFiles.addAll(recursiveFindOldFiles(file, date));
			}
		}
		return oldFiles;
	}

	private void saveLegendImage(String resourceName, BufferedImage image)
			throws IOException {
		String path = getLegendPath(resourceName);
		mkDirs(path);
		ImageIO.write(image, ImageType.PNG.toString(), new File(path));
	}

	private String getLegendPath(String resourceName) {
		return new StringBuilder(outputDir).append(SEPARATOR)
				.append(resourceName).toString();
	}

	private String getLegendResourceName(String type, Date date) {
		DateFormat formatter = new SimpleDateFormat("MM.dd.yyyy_hh.mm");
		return new StringBuilder(LEGEND_FOLDER).append(SEPARATOR)
				.append("legend_").append(type).append("_")
				.append(formatter.format(date)).append(".")
				.append(ImageType.PNG).toString();
	}

	private String getLiveImagePath(String resourceName) {
		return new StringBuilder(outputDir).append(SEPARATOR)
				.append(resourceName).toString();
	}

	private void mkDirs(String path) {
		new File(path.substring(0, path.lastIndexOf(SEPARATOR))).mkdirs();
	}
}
