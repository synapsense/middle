package com.synapsense.liveimaging.storage;

import java.util.Collection;
import java.util.Date;
import java.util.List;

public class CompositeDataStorageWriter implements ImageDataStorageWriter {

	private List<ImageDataStorageWriter> storages;

	public void setStorages(List<ImageDataStorageWriter> storages) {
		this.storages = storages;
	}

	@Override
	public void saveLiveImage(final LiveImageVO liveImage) throws StorageException {
		new StorageLoop().loop(new Iteration() {
			public void perform(ImageDataStorageWriter storage) throws StorageException {
				storage.saveLiveImage(liveImage);
			}
		});
	}

	@Override
	public void deleteImages(final Date before) throws StorageException {
		new StorageLoop().loop(new Iteration() {
			public void perform(ImageDataStorageWriter storage) throws StorageException {
				storage.deleteImages(before);
			}
		});
	}

	@Override
	public void saveLegend(final LegendImageVO legendImage) throws StorageException {
		new StorageLoop().loop(new Iteration() {
			public void perform(ImageDataStorageWriter storage) throws StorageException {
				storage.saveLegend(legendImage);
			}
		});
	}

	@Override
	public void deleteImages(final Collection<LiveImageVO> images) throws StorageException {
		new StorageLoop().loop(new Iteration() {
			public void perform(ImageDataStorageWriter storage) throws StorageException {
				storage.deleteImages(images);
			}
		});
	}

	private interface Iteration {
		void perform(ImageDataStorageWriter storage) throws StorageException;
	}

	private class StorageLoop {

		public void loop(Iteration action) throws StorageException {
			for (ImageDataStorageWriter storage : storages) {
				action.perform(storage);
			}
		}
	}
}
