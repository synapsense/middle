package com.synapsense.liveimaging.storage;

import java.util.Collection;
import java.util.Date;

public interface ImageDataStorageWriter {

	void saveLiveImage(LiveImageVO liveImage) throws StorageException;

	void deleteImages(Date before) throws StorageException;

	void deleteImages(Collection<LiveImageVO> images) throws StorageException;

	void saveLegend(LegendImageVO legendImage) throws StorageException;
}