package com.synapsense.liveimaging.storage;

import java.awt.image.BufferedImage;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Pattern;

import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonAutoDetect.Visibility;

import com.synapsense.liveimaging.conf.LiConstants;

@JsonAutoDetect(fieldVisibility = Visibility.NONE, getterVisibility = Visibility.NONE, setterVisibility = Visibility.NONE)
public class LegendImageVO {

	private int type;

	private String resourcePath;

	private Date showTime;

	private BufferedImage defImage;

	private BufferedImage siImage;

	private static String DATE_FORMAT = "MM.dd.yyyy_hh.mm";

	private static Pattern resourcePattern = Pattern
	        .compile("^.+\\_\\d{3}\\_\\d{2}\\.\\d{2}\\.\\d{4}\\_\\d{2}\\.\\d{2}\\.(png|jpg)$");

	private static final ThreadLocal<SimpleDateFormat> FORMATTER = new ThreadLocal<SimpleDateFormat>() {
		protected SimpleDateFormat initialValue() {
			return new SimpleDateFormat(DATE_FORMAT);
		}
	};

	public static LegendImageVO load(String resourceName) {
		// Check if the resource name is in correct format
		if (!resourcePattern.matcher(resourceName).matches()) {
			return null;
		}
		int dotPos = resourceName.lastIndexOf('.');
		try {
			Date time = FORMATTER.get().parse(resourceName.substring(dotPos - 16, dotPos));
			Integer type = Integer.valueOf(resourceName.split("_")[1]);
			return new LegendImageVO(resourceName, type, time, null, null);
		} catch (ParseException e) {
			// Should never occur since the string matches the pattern
			throw new IllegalArgumentException("Resource name" + resourceName + " is in incorrect format ", e);
		}
	}

	public LegendImageVO(String resourcePath, int type, Date showTime, BufferedImage defImage, BufferedImage siImage) {
		super();
		this.setResorcePath(resourcePath);
		this.type = type;
		this.showTime = showTime;
		this.defImage = defImage;
		this.siImage = siImage;
	}

	public Date getShowTime() {
		return showTime;
	}

	public void setShowTime(Date showTime) {
		this.showTime = showTime;
	}

	public BufferedImage getUsImage() {
		return defImage;
	}

	public BufferedImage getSiImage() {
		return siImage;
	}

	public void setUsImage(BufferedImage image) {
		this.defImage = image;
	}

	public void setSiImage(BufferedImage image) {
		this.siImage = image;
	}

	public int getType() {
		return type;
	}

	public String getResorcePath() {
		return resourcePath;
	}

	public String getSiResourcePath() {
		return resourcePath.replaceFirst("_", "_si");
	}

	public void setResorcePath(String resorcePath) {
		this.resourcePath = resorcePath;
	}

	//Only for JSON responses. Flash doesn't understand default system separator
	@JsonProperty("url")
	private String getResourcePathAsURL() {
		return resourcePath.replace(LiConstants.SEPARATOR, "/");
	}

	@JsonProperty("si_url")
	private String getSiResourcePathAsURL() {
		return resourcePath.replaceFirst("_", "_si").replace(LiConstants.SEPARATOR, "/");
	}
}
