package com.synapsense.liveimaging.api;

import com.synapsense.dto.TO;
import com.synapsense.liveimaging.data.ColorMap;
import com.synapsense.liveimaging.data.ColorMaps;
import com.synapsense.liveimaging.storage.LegendImageVO;
import com.synapsense.liveimaging.storage.LiveImageVO;
import com.synapsense.liveimaging.web.ImageType;
import com.synapsense.liveimaging.web.MovieStream;

import java.io.IOException;
import java.util.Collection;
import java.util.Date;
import java.util.Locale;

public interface LiveImaging {

	public LiveImageVO getLatestImage(TO<?> objId, int dataclass, int layer);

	public LiveImageVO getImage(TO<?> objId, int dataclass, int layer, Date date);

	public LiveImageVO getImage(TO<?> objId, int dataclass, int layer, Date start, Date end);

	public Collection<LiveImageVO> getImages(TO<?> objId, int dataclass, int layer, Date start, Date end, int maxImages);

	public void getMovie(TO<?> objId, int dataclass, int layer, Date start, Date end, String unitSystem,
	        Locale locale, MovieStream writeTo, int maxImages) throws IOException;

	public LegendImageVO getCurrentLegendImage(int dataclass);

	public Collection<ColorMap> getLegends();

	public void updateLegend(ColorMaps newLegend) throws Exception;

	public boolean generateImages(Collection<TO<?>> objects, Collection<ImageType> imageTypes, long interval,
	        Date start, Date end);

	public boolean isHistoricalGeneratingRunning();

	public byte[] generateLatestImage(TO<?> loadTO, Integer dataclass, Integer layer) throws IOException;

	public byte[] generateLatestLiteImage(TO<?> loadTO, Integer dataclass, Integer layer) throws IOException;

	public boolean hasImages(TO<?> objId);

	public boolean isServiceAvailable();

	public void runImageRipper();

	public byte[] loadImage(String resourcePath) throws IOException;

}
