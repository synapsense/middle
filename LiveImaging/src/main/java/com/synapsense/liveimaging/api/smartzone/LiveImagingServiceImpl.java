package com.synapsense.liveimaging.api.smartzone;

import com.synapsense.dto.TO;
import com.synapsense.liveimaging.data.DataSource;
import com.synapsense.liveimaging.es.ESContext;
import com.synapsense.liveimaging.es.EsUtil;
import com.synapsense.liveimaging.storage.LegendImageVO;
import com.synapsense.liveimaging.storage.LiveImageVO;
import com.synapsense.liveimaging.storage.MemoryDataStorage;
import com.synapsense.liveimaging.web.MovieStream;
import com.synapsense.liveimaging.web.NotFoundException;
import com.synapsense.liveimaging.web.smartzone.LiveImageType;
import com.synapsense.liveimaging.web.smartzone.UnitsSystem;
import com.synapsense.util.unitconverter.Dimension;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.threeten.extra.Interval;

import java.io.IOException;
import java.time.Instant;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.SortedSet;

public class LiveImagingServiceImpl implements LiveImagingService {

    private static final Log log = LogFactory.getLog(LiveImagingServiceImpl.class);
    private final MemoryDataStorage storage;
    private DataSource esDataSource;

    private ESContext esContext;

    public LiveImagingServiceImpl(final MemoryDataStorage storage) {
        this.storage = storage;
    }

    @Override
    public LiveImageVO getImage(final String id, final LiveImageType imageType, final Instant instant) {
        return storage.getImage(getDCForFloorplan(id), imageType, instant);
    }

    @Override
    public Collection<LiveImageVO> getImages(final String id,
                                             final LiveImageType imageType,
                                             final Interval interval,
                                             final Integer maxImages) {
        List<LiveImageVO> images = storage.getImages(getDCForFloorplan(id), imageType, interval);
        return MemoryDataStorage.filterImages(images, maxImages);
    }

    @Override
    public LiveImageVO getLatestImage(final String id, final LiveImageType imageType) {
        return storage.getLatestImage(getDCForFloorplan(id), imageType);
    }

    @Override
    public void getMovie(final String id,
                                final LiveImageType imageType,
                                final Interval interval,
                                final Integer maxImages,
                                final UnitsSystem unitSystem,
                                final Locale locale,
                                final MovieStream stream) throws IOException {
        final TO<?> to = getDCForFloorplan(id);
        Collection<LiveImageVO> availableImages;
        try {
            availableImages = getImages(id, imageType, interval, maxImages);
            if (availableImages == null || availableImages.isEmpty()) {
                throw new IllegalArgumentException("Images [id: " + id + " ;imageType: " + imageType + "; interval:"
                                                           + interval + "] don't exist");
            }
            SortedSet<LegendImageVO> availableLegends = storage.getAvailableLegends(imageType.getDataClass());
            if (availableLegends == null || availableLegends.isEmpty()) {
                throw new IllegalArgumentException("Legends for dataclass: " + imageType.getDataClass() + "don't exist");
            }
        } catch (IllegalArgumentException e) {
            log.warn(e.getMessage(), e);
            return;
        }

        String legendLabel = EsUtil.localizeString(esContext, "text_map_legend", locale);
        Dimension dimension = EsUtil.getDimension(esContext, imageType.getDataClass(), unitSystem.getESTypeName());
        String dimName = EsUtil.getDimensionName(esContext, dimension, imageType.getDataClass(), imageType.getLayer(), locale);
        String dimUnit = EsUtil.getDimensionUnits(esContext, dimension, locale);
        String name = EsUtil.getLocalizedName(esContext, to, locale);
        name += " " + dimName + "(" + dimUnit + ")";

        stream.outputAVI(name, legendLabel, unitSystem.getESTypeName(), availableImages, storage.getOutputDir());
    }

    @Override
    public boolean hasImages(final String id) {
        TO<?> objId;
        try {
            objId = getDCForFloorplan(id);
        } catch (NotFoundException e) {
            return false;
        }

        return storage.hasImage(objId.getTypeName(), (Integer) objId.getID());
    }

    public void setEsDataSource(final DataSource esDataSource) {
        this.esDataSource = esDataSource;
    }

    public void setEsContext(ESContext esContext) {
        this.esContext = esContext;
    }

    private TO<?> getDCForFloorplan(final String id) {
        final TO<?> to = esDataSource.getESObjectForSmartzoneId(id);
        if (to == null) {
            log.debug("No Images for [id: " + id);
            throw new NotFoundException();
        }

        return to;
    }

}
