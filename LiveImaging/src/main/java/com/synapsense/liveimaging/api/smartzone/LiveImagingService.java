package com.synapsense.liveimaging.api.smartzone;

import com.synapsense.liveimaging.storage.ImageVO;
import com.synapsense.liveimaging.storage.LiveImageVO;
import com.synapsense.liveimaging.web.BadRequestException;
import com.synapsense.liveimaging.web.MovieStream;
import com.synapsense.liveimaging.web.NotFoundException;
import com.synapsense.liveimaging.web.smartzone.LiveImageType;
import com.synapsense.liveimaging.web.smartzone.UnitsSystem;
import org.threeten.extra.Interval;

import java.io.IOException;
import java.net.URL;
import java.time.Instant;
import java.util.Collection;
import java.util.Locale;

public interface LiveImagingService {

    LiveImageVO getImage(final String id,
                         final LiveImageType imageType,
                         final Instant instant)
            throws NotFoundException, BadRequestException, IOException;

    Collection<LiveImageVO> getImages(final String id,
                                      final LiveImageType imageType,
                                      final Interval interval,
                                      final Integer maxImages)
            throws NotFoundException, BadRequestException, IOException;

    LiveImageVO getLatestImage(final String id,
                               final LiveImageType imageType)
            throws NotFoundException, BadRequestException, IOException;

    void getMovie(final String id,
                  final LiveImageType imageType,
                  final Interval interval,
                  final Integer maxImages,
                  final UnitsSystem unitSystem,
                  final Locale locale,
                  final MovieStream stream)
            throws NotFoundException, BadRequestException, IOException;


    boolean hasImages(final String id);

}
