package com.synapsense.liveimaging.api;

import com.synapsense.dto.TO;
import com.synapsense.liveimaging.conf.LiConstants;
import com.synapsense.liveimaging.data.ColorMap;
import com.synapsense.liveimaging.data.ColorMaps;
import com.synapsense.liveimaging.es.ESContext;
import com.synapsense.liveimaging.es.EsUtil;
import com.synapsense.liveimaging.job.HistoricalImageGenerator;
import com.synapsense.liveimaging.job.RealTimeImageGenerator;
import com.synapsense.liveimaging.legend.ColorManager;
import com.synapsense.liveimaging.storage.LegendImageVO;
import com.synapsense.liveimaging.storage.LiveImageVO;
import com.synapsense.liveimaging.storage.MemoryDataStorage;
import com.synapsense.liveimaging.web.ImageType;
import com.synapsense.liveimaging.web.MovieStream;
import com.synapsense.util.unitconverter.Dimension;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.SortedSet;

public class LiveImagingMemoryImpl implements LiveImaging {

	private final Log logger = LogFactory.getLog(LiveImagingMemoryImpl.class);
	private MemoryDataStorage storage;
	private HistoricalImageGenerator historicalImageGenerator;
	private RealTimeImageGenerator realTimeImageGenerator;
	private ESContext esContext;
	private ColorManager colorMan;


	private static Log log = LogFactory.getLog(LiveImagingMemoryImpl.class);

	public LiveImagingMemoryImpl(MemoryDataStorage storage, ColorManager colorMan,
	        HistoricalImageGenerator historicalImageGenerator, RealTimeImageGenerator realTimeImageGenerator) {
		this.storage = storage;
		this.colorMan = colorMan;
		this.historicalImageGenerator = historicalImageGenerator;
		this.realTimeImageGenerator = realTimeImageGenerator;
	}

	@Override
	public LiveImageVO getLatestImage(TO<?> objId, int dataclass, int layer) {
		return storage.getLatestImage(objId, dataclass, layer);
	}

	@Override
	public LiveImageVO getImage(TO<?> objId, int dataclass, int layer, Date date) {
		return storage.getImage(objId, dataclass, layer, date);
	}

	@Override
	public void getMovie(TO<?> objId, int dataclass, int layer, Date start, Date end, String unitSystem,
	        Locale locale, MovieStream stream, int maxImages) throws IOException {
		Collection<LiveImageVO> availableImages = null;
		try {
			availableImages = getImages(objId, dataclass, layer, start, end, maxImages);
			if (availableImages == null || availableImages.isEmpty()) {
				throw new IllegalArgumentException("Images [objId: " + objId + " ;dataclass: " + dataclass + "; layer:"
				        + layer + "] don't exist");
			}
			SortedSet<LegendImageVO> availableLegends = storage.getAvailableLegends(dataclass);
			if (availableLegends == null || availableLegends.isEmpty()) {
				throw new IllegalArgumentException("Legends for dataclass: " + dataclass + "don't exist");
			}
		} catch (IllegalArgumentException e) {
			log.warn(e.getMessage(), e);
			return;
		}

		String legendLabel = EsUtil.localizeString(esContext, "text_map_legend", locale);
		Dimension dimension = EsUtil.getDimension(esContext, dataclass, unitSystem);
		String dimName = EsUtil.getDimensionName(esContext, dimension, dataclass, layer, locale);
		String dimUnit = EsUtil.getDimensionUnits(esContext, dimension, locale);
		String name = EsUtil.getLocalizedName(esContext, objId, locale);
		name += " " + dimName + "(" + dimUnit + ")";
		stream.setName(name.replace(" ", "_"));

		stream.outputAVI(name, legendLabel, unitSystem, availableImages, storage.getOutputDir());
	}

	@Override
	public LegendImageVO getCurrentLegendImage(int dataclass) {
		SortedSet<LegendImageVO> legends = storage.getAvailableLegends(dataclass);
		if (legends != null && !legends.isEmpty()) {
			return legends.last();
		}
		log.debug("Legend [dataclass: " + dataclass + "] does not exist");
		return null;
	}

	@Override
	public boolean isServiceAvailable() {
		return true;
	}

	@Override
	public byte[] generateLatestImage(TO<?> loadTO, Integer dataclass, Integer layer) throws IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		BufferedImage image = realTimeImageGenerator.generateLatestImage(loadTO, dataclass, layer, false);
		ImageIO.write(image, "png", baos);
		return baos.toByteArray();
	}

	@Override
	public byte[] generateLatestLiteImage(TO<?> loadTO, Integer dataclass, Integer layer) throws IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		BufferedImage image = realTimeImageGenerator.generateLatestImage(loadTO, dataclass, layer, true);
		ImageIO.write(image, "png", baos);
		return baos.toByteArray();
	}

	@Override
	public void updateLegend(ColorMaps newLegend) throws Exception {
		try {
			colorMan.updateColorModel(newLegend);
		} catch (IllegalArgumentException e) {
			log.warn("Could not update legend. New colors model has incorrect format");
			throw e;
		} catch (Exception e) {
			log.warn("Could not update legend", e);
			throw e;
		}

	}

	@Override
	public Collection<ColorMap> getLegends() {
		return colorMan.getColorMap();
	}

	@Override
	public boolean hasImages(TO<?> objId) {
		return storage.hasImage(objId.getTypeName(), (Integer) objId.getID());
	}

	@Override
	public boolean generateImages(Collection<TO<?>> objIds, Collection<ImageType> imageTypes, long interval,
	        Date start, Date end) {
		boolean result = historicalImageGenerator.addTask(objIds, imageTypes, interval, start, end);
		if (result)
			logger.info("Generating images for dates [" + start.toString() + " - " + end.toString() + "]");
		else
			logger.info("Allowed only one historical image generation job at a time. New task is rejected");
		return result;
	}

	@Override
	public boolean isHistoricalGeneratingRunning() {
		return historicalImageGenerator.hasJob();
	}

	@Override
	public LiveImageVO getImage(TO<?> objId, int dataclass, int layer, Date start, Date end) {
		List<LiveImageVO> availableImages = storage.getImages(objId, dataclass, layer, start, end);
		if (availableImages != null && !availableImages.isEmpty()) {
			return availableImages.get(availableImages.size() - 1);
		} else {
			log.debug("Images [objId: " + objId + " ;dataclass: " + dataclass + "; layer:" + layer + "] don't exist");
			return null;
		}

	}

	@Override
	public Collection<LiveImageVO> getImages(TO<?> objId, int dataclass, int layer, Date start, Date end, int maxImages) {
		List<LiveImageVO> images = storage.getImages(objId, dataclass, layer, start, end);
		return MemoryDataStorage.filterImages(images, maxImages);
	}

	@Override
	public byte[] loadImage(String resourcePath) throws IOException {
		Path path = Paths.get(storage.getOutputDir() + LiConstants.SEPARATOR + resourcePath);
		byte[] data = Files.readAllBytes(path);
		return data;
	}

	public void setEsContext(ESContext esContext) {
		this.esContext = esContext;
	}

	public void setRealTimeImageGenerator(RealTimeImageGenerator realTimeImageGenerator) {
		this.realTimeImageGenerator = realTimeImageGenerator;
	}

	@Override
	public void runImageRipper() {
		realTimeImageGenerator.forceRunImageRipper();

	}

}
