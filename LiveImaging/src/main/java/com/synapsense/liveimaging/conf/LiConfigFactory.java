package com.synapsense.liveimaging.conf;

import java.io.StringReader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.springframework.beans.factory.FactoryBean;


public class LiConfigFactory implements FactoryBean<LiConfig> {
	
	private static String configText = null;

	// This factory is used by spring to get LiConfig object. The object is
	// deserialized either from file or from the ES.
	@Override
	public LiConfig getObject() throws Exception {
		if (configText == null) {
			throw new ConfigurationException("Config file text is not initialized. Bean creation cannot be continued.");
		}
		try {
			JAXBContext jc = JAXBContext.newInstance(LiConfig.class);
			Unmarshaller u = jc.createUnmarshaller();

			return (LiConfig) u.unmarshal(new StringReader(configText));
		} catch (JAXBException e) {
			throw new ConfigurationException("Unable to parse LiveImaging configuration", e);
		}
	}

	@Override
	public Class<?> getObjectType() {
		return LiConfig.class;
	}

	@Override
	public boolean isSingleton() {
		return true;
	}

	public static void setConfigText(String configText) {
		LiConfigFactory.configText = configText;
	}

}
