package com.synapsense.liveimaging.conf;

import com.synapsense.liveimaging.LiveImagingException;

public class ConfigurationException extends LiveImagingException {

	private static final long serialVersionUID = -7603024198597194208L;

	public ConfigurationException() {
		super();
	}

	public ConfigurationException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public ConfigurationException(String arg0) {
		super(arg0);
	}

	public ConfigurationException(Throwable arg0) {
		super(arg0);
	}
}
