package com.synapsense.liveimaging.conf;

public class LiConstants {
	public static final String SEPARATOR = System.getProperty("file.separator");
	public static final String LEGEND_FOLDER = "legends";
	public static final String OUTPUT_FOLDER = "./output";
	public static final String LOGO_RESOURCE="logo/li_logo.png";
	public static final String HIST_IMAGE_PREFIX = "hist";
	
	public static final String ROOM = "ROOM";
	public static final String DC = "DC";
	public static final String COIN = "CONTROLPOINT_SINGLECOMPARE";
    public static final String WSNSENSOR = "WSNSENSOR";
    public static final String DISPLAYPOINT = "DISPLAYPOINT";

	public static final Integer CONTROLLAYER = 128;
	
	public static final int WORKER_LIMIT = 32;
}
