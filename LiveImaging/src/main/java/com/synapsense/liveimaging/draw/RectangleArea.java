package com.synapsense.liveimaging.draw;

import java.util.ArrayList;
import java.util.Iterator;

public class RectangleArea extends Area {

	private float x1;
	private float y1;
	private float x2;
	private float y2;

	private ArrayList<Segment> edges = new ArrayList<Segment>(4);

	public RectangleArea(float x1, float y1, float x2, float y2) {
		this(null, x1, y1, x2, y2);
	}

	public RectangleArea(Area parent, float x1, float y1, float x2, float y2) {
		super(null);
		if (x2 <= x1 || y2 <= y1) {
			throw new IllegalArgumentException("Incorrect angles: X2 should be > X1, Y2 should be > Y1");
		}
		this.x1 = x1;
		this.y1 = y1;
		this.x2 = x2;
		this.y2 = y2;

		edges.add(new Segment(x1, y1, x2, y1));
		edges.add(new Segment(x1, y1, x1, y2));
		edges.add(new Segment(x2, y1, x2, y2));
		edges.add(new Segment(x1, y2, x2, y2));
	}

	public RectangleArea(RectangleArea other) {
		this(null, other.x1, other.y1, other.x2, other.y2);
		Iterator<ValuePoint> i = other.iterator();
		while (i.hasNext()) {
			this.addSampleValue(i.next());
		}
		for(Area nestedArea : other.getNestedAreas()){
			this.nest(nestedArea);
		}
	}

	private RectangleArea(Area parent, Point2F p1, Point2F p2) {
		this(p1.getX(), p1.getY(), p2.getX(), p2.getY());
	}

	@Override
	public boolean inArea(float x, float y) {
		return x >= x1 && x <= x2 && y >= y1 && y <= y2;
	}

	@Override
	public float calcArea() {
		return edges.get(0).getLength() * edges.get(1).getLength();
	}

	@Override
	public Point2F getLeftBound() {
		return new Point2F(x1, y1);
	}

	@Override
	public Point2F getRightBound() {
		return new Point2F(x2, y2);
	}

	@Override
	protected Area afflineCopy(Area parent, float x0, float y0, float sX, float sY) {
		return new RectangleArea(parent, new Point2F(x1, y1).scale(x0, y0, sX, sY), new Point2F(x2, y2).scale(x0, y0,
		        sX, sY));
	}

	public RectangleArea subArea(float x, float y, float width, float height) {
		if (x1 >= x + width || y1 >= y + height || x2 <= x || y2 <= y) {
			// no intersection
			return null;
		}

		float cX1 = x1 < x ? x : x1;
		float cY1 = y1 < y ? y : y1;

		float cX2 = x2 > x + width ? x + width : x2;
		float cY2 = y2 > y + height ? y + height : y2;

		return new RectangleArea(cX1, cY1, cX2, cY2);
	}

	@Override
	protected Iterable<Segment> getEdges() {
		return edges;
	}
}
