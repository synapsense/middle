package com.synapsense.liveimaging.draw;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.liveimaging.data.ColorMap;
import com.synapsense.liveimaging.draw.Loop2D.Loop2DBody;
import com.synapsense.liveimaging.interpolation.InterpolationException;
import com.synapsense.liveimaging.interpolation.InterpolationFactory;
import com.synapsense.liveimaging.interpolation.Interpolator;
import com.synapsense.liveimaging.interpolation.Samples;

public class LiveImageBuilder {

	private static final Log log = LogFactory.getLog(LiveImageBuilder.class);

	private InterpolationFactory interpolationFactory;

	private ColorMap colorMap;

	private int drawThreshold;

	private int solidColor;

	private int alpha;

	private BufferedImage background;

	private BufferedImage image;

	void drawArea(Samples samples, final PointFilter filter) throws DrawLiveImageException {
		if (drawThreshold == 0) {
			// no image
			return;
		}
		if (colorMap == null || interpolationFactory == null || background == null) {
			throw new IllegalStateException(
			        "Color map, background image and interpolation factory must be set to produce image");
		}

		if (image == null) {
			image = new BufferedImage(background.getWidth(), background.getHeight(), background.getType());
		}

		if (log.isDebugEnabled()) {
			log.debug("Image height=" + image.getHeight() + " width=" + image.getWidth());
		}

		Graphics2D graphics = image.createGraphics();
		// Draw image
		final Point2I leftBound = samples.getLeftBound().toIntPoint();
		final Point2I rightBound = samples.getRightBound().toIntPoint();

		AreaLoop areaLoop = new AreaLoop(leftBound, rightBound, filter);
		graphics.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, alpha * 1.0f / 255f));

		if (samples.size() < drawThreshold) {
			final BufferedImage solImage = new BufferedImage(image.getWidth(), image.getHeight(),
			        BufferedImage.TYPE_INT_ARGB);
			// Solid fill
			areaLoop.loop(new Loop2DBody() {
				public void run(int x, int y) {
					solImage.setRGB(x, y, solidColor);
				}
			});
			graphics.drawImage(solImage, 0, 0, null);
		} else {
			Interpolator interpolator;
			try {
				interpolator = interpolationFactory.getInterpolator(samples);
				if (log.isDebugEnabled()) {
					log.debug("Loaded interpolator " + interpolator.getClass().getSimpleName());
				}
			} catch (InterpolationException e) {
				log.error(e.getLocalizedMessage(), e);
				throw new DrawLiveImageException("Image drawing error: " + e.getMessage(), e);
			}

			// Collect points to interpolate and draw
			int xElements = (rightBound.getX() - leftBound.getX() + 1);
			int yElements = (rightBound.getY() - leftBound.getY() + 1);

			yElements = xElements *= yElements;

			final float[] xDraw = new float[xElements];
			final float[] yDraw = new float[yElements];
			final int[] counter = new int [1];
			counter[0] = 0;
			long start = System.currentTimeMillis();
			areaLoop.loop(new Loop2DBody() {
				public void run(int x, int y) {
					xDraw[counter[0]] = x;
					yDraw[counter[0]] = y;
					++counter[0];
				}
			});
			if (log.isDebugEnabled()) {
				log.debug("Image height=" + image.getHeight() + " width=" + image.getWidth() + " points filter time "
				        + (System.currentTimeMillis() - start));
			}
			float[] xDrawCut = null;
			float[] yDrawCut;
			if(counter[0] < xElements) {
				xDrawCut = new float[counter[0]];
				System.arraycopy(xDraw, 0, xDrawCut, 0, counter[0]);
			} else {
				xDrawCut = xDraw;
			}

			if(counter[0] < yElements) {
				yDrawCut = new float[counter[0]];
				System.arraycopy(yDraw, 0, yDrawCut, 0, counter[0]);
			} else {
				yDrawCut = yDraw;
			}

			start = System.currentTimeMillis();

			float[] result = interpolator.interpolate(xDraw, yDraw, samples);
			if (log.isDebugEnabled()) {
				log.debug("Image samples size " + samples.size() + " interpolation time "
				        + (System.currentTimeMillis() - start));
			}

			start = System.currentTimeMillis();
			for (int i = 0; i < result.length; i++) {
				image.setRGB( (int) xDraw[i], (int) yDraw[i],
				        (alpha << 24) | colorMap.getColorAt(result[i]).getRGB());
			}
			if (log.isDebugEnabled()) {
				log.debug("Image height=" + image.getHeight() + " width=" + image.getWidth() + " RGB set time "
				        + (System.currentTimeMillis() - start));
			}
		}

		graphics.drawImage(background, null, 0, 0);

		// Draw sensor points
		graphics.setPaint(Color.black);
		for (ValuePoint sample : samples) {
			Rectangle2D rect = new Rectangle2D.Double(sample.getX() - 1.5, sample.getY() - 1.5, 3, 3);
			graphics.draw(rect);
		}

		graphics.dispose();
	}

	public BufferedImage getImage() {
		return image;
	}

	public void setBackGroundImage(BufferedImage background) {
		this.background = background;
	}

	public void setInterpolationFactory(InterpolationFactory interpolationFactory) {
		this.interpolationFactory = interpolationFactory;
	}

	public void setColorMap(ColorMap colorMap) {
		this.colorMap = colorMap;
	}

	public void setDrawThreshold(int drawThreshold) {
		this.drawThreshold = drawThreshold;
	}

	public void setSolidColor(int solidColor) {
		this.solidColor = solidColor;
	}

	public void setAlpha(int alpha) {
		this.alpha = alpha;
	}

	private final class AreaLoop implements Loop2D {

		private int xStart;
		private int xEnd;
		private int yStart;
		private int yEnd;

		private PointFilter filter;

		AreaLoop(Point2I leftBound, Point2I rightBound, PointFilter filter) {
			this.xStart = leftBound.getX();
			this.xEnd = rightBound.getX();
			this.yStart = leftBound.getY();
			this.yEnd = rightBound.getY();
			this.filter = filter;
		}

		public void loop(Loop2DBody body) {
			for (int x = xStart; x <= xEnd; x++) {
				for (int y = yStart; y <= yEnd; y++) {
					if (filter.drawPoint(x, y) && ((background.getRGB(x, y) & 0xFF000000) >>> 24 < 255)) {
						body.run(x, y);
					}
				}
			}
		}
	}
}
