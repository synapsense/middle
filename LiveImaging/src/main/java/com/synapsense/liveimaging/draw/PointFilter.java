package com.synapsense.liveimaging.draw;

interface PointFilter {
	boolean drawPoint(float x, float y);
}
