package com.synapsense.liveimaging.draw;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;

import com.synapsense.liveimaging.interpolation.Samples;

public abstract class Area implements LiveImage, Samples {

	private Area parent;

	private ArrayList<Area> nestedAreas = new ArrayList<Area>();

	private LinkedList<ValuePoint> samples = new LinkedList<ValuePoint>();

	// Draw parameters
	private int minPoints = 2;
	private int color = 0xC0C0C0;
	private int alpha = 0xFF;

	public Area(Area parent) {
		this.parent = parent;
	}

	@Override
	public void draw(LiveImageBuilder imageBuilder) throws DrawLiveImageException {
		for (Area child : nestedAreas) {
			child.draw(imageBuilder);
		}

		imageBuilder.setAlpha(alpha);
		imageBuilder.setDrawThreshold(minPoints);
		imageBuilder.setSolidColor(color);
		imageBuilder.drawArea(this, new PointFilter() {

			@Override
			public boolean drawPoint(float x, float y) {

				// inside the bounds of this area
				if (!inArea(x, y)) {
					return false;
				}

				// inside all the parent areas
				Area p = parent;
				while (p != null) {
					if (!p.inArea(x, y)) {
						return false;
					}
					p = p.parent;
				}

				// out of all the children
				for (Area area : nestedAreas) {
					if (area.inArea(x, y)) {
						return false;
					}
				}
				return true;
			}
		});
	}

	@Override
	public Iterator<ValuePoint> iterator() {
		return samples.iterator();
	}

	@Override
	public ValuePoint get(int index) {
		return samples.get(index);
	}

	@Override
	public int size() {
		return samples.size();
	}

	public void nest(Area area) {
		nestedAreas.add(area);
		area.parent = this;
	}

	public void addSampleValue(ValuePoint p) {
		for (Area area : nestedAreas) {
			if (area.inArea(p.getX(), p.getY())) {
				area.addSampleValue(p);
				return;
			}
		}
		samples.add(p);
	}
	public void removePoint(Point2F coord) {
		for (Area area : nestedAreas) {
			if (area.inArea(coord.getX(), coord.getY())) {
				area.removePoint(coord);
				return;
			}
		}
		for (int i = 0; i < samples.size(); i++) {
			if (samples.get(i).getCoord().equals(coord)) {
				samples.remove(i);
			}
		}
    }
	public void addSampleValue(float x, float y, float value) {
		addSampleValue(new ValuePoint(x, y, value));
	}

	public boolean updateSampleValue(ValuePoint p) {
		for (Area area : nestedAreas) {
			if (area.inArea(p.getX(), p.getY())) {
				return area.updateSampleValue(p);
			}
		}
		for (int i = 0; i < samples.size(); i++) {
			if (samples.get(i).getCoord().equals(p.getCoord())) {
				samples.set(i, p);
				return true;
			}
		}
		return false;
	}

	public Area affline(float x0, float y0, float sX, float sY) {
		return affline(null, x0, y0, sX, sY);
	}

	public boolean inArea(float x1, float y1, float x2, float y2) {
		if (!inArea(x1, y1) || !inArea(x2, y2)) {
			return false;
		}
		Segment seg = new Segment(x1, y1, x2, y2);
		for (Segment edge : getEdges()) {
			if (seg.intersects(edge)) {
				return false;
			}
		}
		return true;
	}

	public boolean inscribed(Area other) {
		for (Segment edge : getEdges()) {
			if (!other.inArea(edge.getStart().getX(), edge.getStart().getY(), edge.getEnd().getX(), edge.getEnd()
			        .getY())) {
				return false;
			}
		}
		return true;
	}

	public abstract boolean inArea(float x, float y);

	public abstract float calcArea();

	public void setMinPoints(int minPoints) {
		this.minPoints = minPoints;
	}

	public void setColor(int color) {
		this.color = color;
	}

	public void setAlpha(int alpha) {
		this.alpha = alpha;
	}
	public ArrayList<Area> getNestedAreas() {
	    return nestedAreas;
    }

	protected Area affline(Area parent, float x0, float y0, float sX, float sY) {
		Area afflineCopy = afflineCopy(parent, x0, y0, sX, sY);
		afflineCopy.setAlpha(alpha);
		afflineCopy.setColor(color);
		afflineCopy.setMinPoints(minPoints);
		for (Area child : nestedAreas) {
			afflineCopy.nest(child.affline(afflineCopy, x0, y0, sX, sY));
		}
		return afflineCopy;
	}

	protected abstract Iterable<Segment> getEdges();

	protected abstract Area afflineCopy(Area parent, float x0, float y0, float sX, float sY);
}
