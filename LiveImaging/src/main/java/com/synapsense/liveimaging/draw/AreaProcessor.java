package com.synapsense.liveimaging.draw;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;

public class AreaProcessor {

	private ArrayList<Area> areas = new ArrayList<Area>();

	public void addArea(Area area) {
		areas.add(area);
	}

	public Collection<Area> getAreasForest() {
		ArrayList<Area> forest = new ArrayList<Area>();
		if (areas.isEmpty()) {
			return forest;
		}
		Collections.sort(areas, new Comparator<Area>() {
			@Override
			public int compare(Area o1, Area o2) {
				return Double.compare(o1.calcArea(), o2.calcArea());
			}
		});

		for (int i = 0; i < areas.size(); i++) {
			if (!findParent(i)) {
				forest.add(areas.get(i));
			}
		}
		return forest;
	}

	public Collection<Area> listAreas() {
		return Collections.unmodifiableCollection(areas);
	}

	public void clear() {
		areas.clear();
	}

	private boolean findParent(int i) {
		if (i == areas.size() - 1) {
			return false;
		}
		for (int j = i + 1; j < areas.size(); j++) {
			if (areas.get(i).inscribed(areas.get(j))) {
				areas.get(j).nest(areas.get(i));
				return true;
			}
		}
		return false;
	}
}
