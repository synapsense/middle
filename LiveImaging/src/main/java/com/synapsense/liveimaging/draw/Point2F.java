package com.synapsense.liveimaging.draw;

public final class Point2F {

	private float x;
	private float y;

	// object is immutable, I can cash the hashCode
	private int hashCode;

	public Point2F(float x, float y) {
		this.x = x;
		this.y = y;
		this.hashCode = hash();
	}

	public float getDistance(Point2F other) {
		return getDistance(other.x, other.y);
	}

	public float getDistance(float x, float y) {
		return (float) Math.sqrt((x - this.x) * (x - this.x) + (y - this.y) * (y - this.y));
	}

	public Point2F scale(float x0, float y0, float sX, float sY) {
		return new Point2F((x - x0) * sX, (y - y0) * sY);
	}

	public Point2I toIntPoint() {
		return new Point2I(new Float(Math.floor(x)).intValue(), new Float(Math.floor(y)).intValue());
	}

	public float getX() {
		return x;
	}

	public float getY() {
		return y;
	}

	@Override
	public String toString() {
		return "[x=" + x + ", y=" + y + "]";
	}

	@Override
	public int hashCode() {
		return this.hashCode;
	}

	public int hash() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Float.floatToIntBits(x);
		result = prime * result + Float.floatToIntBits(y);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Point2F other = (Point2F) obj;
		if (Float.floatToIntBits(x) != Float.floatToIntBits(other.x))
			return false;
		if (Float.floatToIntBits(y) != Float.floatToIntBits(other.y))
			return false;
		return true;
	}
}
