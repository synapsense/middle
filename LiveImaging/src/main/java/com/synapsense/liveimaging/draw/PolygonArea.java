package com.synapsense.liveimaging.draw;

import java.util.ArrayList;

public class PolygonArea extends Area {

	// Bounds
	private float x1 = Float.MAX_VALUE;
	private float y1 = Float.MAX_VALUE;
	private float x2 = -x1;
	private float y2 = -y1;

	private ArrayList<Point2F> points = new ArrayList<Point2F>();

	private ArrayList<Segment> edges = null;

	public PolygonArea(Area parent) {
		super(parent);
	}

	public PolygonArea() {
		this(null);
	}

	public void addAngle(float x, float y) {
		addAngle(new Point2F(x, y));
	}

	public void addAngle(Point2F point) {
		x1 = point.getX() < x1 ? point.getX() : x1;
		x2 = point.getX() > x2 ? point.getX() : x2;
		y1 = point.getY() < y1 ? point.getY() : y1;
		y2 = point.getY() > y2 ? point.getY() : y2;
		points.add(point);
		edges = null;
	}

	public void addAllAngles(Iterable<Point2F> angles) {
		for (Point2F angle : angles) {
			addAngle(angle);
		}
	}

	@Override
	public boolean inArea(float x, float y) {
		for (Segment edge : getEdges()) {
			if (edge.contains(x, y)) {
				return true;
			}
		}
		int nbIntersections = 0;
		for (int i = 0, j = points.size() - 1; i < points.size(); j = i++) {
			if ((((points.get(i).getY() <= y) && (y < points.get(j).getY())) || ((points.get(j).getY() <= y) && (y < points
			        .get(i).getY())))
			        && (x > (points.get(j).getX() - points.get(i).getX()) * (y - points.get(i).getY())
			                / (points.get(j).getY() - points.get(i).getY()) + points.get(i).getX())) {
				nbIntersections++;
			}
		}
		return nbIntersections % 2 != 0;
	}

	@Override
	public float calcArea() {
		double area = 0.0;
		for (Segment seg : getEdges()) {
			// trapezoid
			area += (seg.getStart().getX() - seg.getEnd().getX()) * (seg.getStart().getY() + seg.getEnd().getY()) / 2;
		}
		return (float) Math.abs(area);
	}

	@Override
	public Point2F getLeftBound() {
		return new Point2F(x1, y1);
	}

	@Override
	public Point2F getRightBound() {
		return new Point2F(x2, y2);
	}

	@Override
	protected Area afflineCopy(Area parent, float x0, float y0, float sX, float sY) {
		PolygonArea copy = new PolygonArea(parent);
		for (Point2F point : points) {
			copy.addAngle(point.scale(x0, y0, sX, sY));
		}
		return copy;
	}

	@Override
	protected Iterable<Segment> getEdges() {
		if (edges == null) {
			edges = new ArrayList<Segment>(points.size());
			for (int i = 0, j = points.size() - 1; i < points.size(); j = i++) {
				edges.add(new Segment(points.get(j), points.get(i)));
			}
		}
		return edges;
	}
}
