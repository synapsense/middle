package com.synapsense.liveimaging.draw;

public class Segment {

	private Point2F start;
	private Point2F end;

	// Line equation coeffs
	private float a;
	private float b;
	private float d;

	public Segment(Point2F start, Point2F end) {
		this.start = start;
		this.end = end;

		a = -end.getY() + start.getY();
		b = +end.getX() - start.getX();
		d = -(a * start.getX() + b * start.getY());
	}

	public Segment(float x1, float y1, float x2, float y2) {
		this(new Point2F(x1, y1), new Point2F(x2, y2));
	}

	protected Point2F getStart() {
		return start;
	}

	protected Point2F getEnd() {
		return end;
	}

	public float getLength() {
		return (float) Math.sqrt((end.getX() - start.getX()) * (end.getX() - start.getX()) + ((end.getY() - start.getY()) * (end.getY() - start.getY())));
	}

	public boolean intersects(Segment other) {
		double seg1Line2Start = other.a * start.getX() + other.b * start.getY() + other.d;
		double seg1Line2End = other.a * end.getX() + other.b * end.getY() + other.d;

		double seg2Line1Start = a * other.start.getX() + b * other.start.getY() + d;
		double seg2Line1End = a * other.end.getX() + b * other.end.getY() + d;

		return seg1Line2Start * seg1Line2End < 0 && seg2Line1Start * seg2Line1End < 0;
	}

    public boolean contains(float x, float y) {
        return belong(x, y);
    }

    private boolean belong(float x, float y) {
        return (a * x + b * y + d == 0) && inBounds(x, start.getX(), end.getX())
                && inBounds(y, start.getY(), end.getY());
    }

	private boolean inBounds(float x, float x1, float x2) {
		return (x >= x1) && (x <= x2) || (x >= x2 && x <= x1);
	}
}
