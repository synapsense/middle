package com.synapsense.liveimaging.draw;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;

public final class ImageUtil {

	public static BufferedImage scaleImage(BufferedImage image, int width, int height) {
		BufferedImage scaledImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		Graphics2D graphics2D = scaledImage.createGraphics();
		Image scaledBgMap = image.getScaledInstance(width, height, Image.SCALE_SMOOTH);
		graphics2D.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_LCD_HRGB);
		graphics2D.drawImage(scaledBgMap, 0, 0, null);
		return scaledImage;
	}

	public static BufferedImage copyImage(BufferedImage image) {
		ColorModel cm = image.getColorModel();
		return new BufferedImage(cm, image.copyData(null), cm.isAlphaPremultiplied(), null);
	}

	public static BufferedImage copyImageToRGB(Image image) {
		BufferedImage result = new BufferedImage(image.getWidth(null), image.getHeight(null),
		        BufferedImage.TYPE_INT_RGB);
		result.createGraphics().drawImage(image, 0, 0, null);
		return result;
	}

	private ImageUtil() {
	}
}
