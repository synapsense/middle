package com.synapsense.liveimaging.draw;

public interface Loop2D {
	void loop(Loop2DBody body);

	public static interface Loop2DBody {
		void run(int x, int y);
	}
}
