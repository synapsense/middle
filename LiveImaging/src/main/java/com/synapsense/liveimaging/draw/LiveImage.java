package com.synapsense.liveimaging.draw;

public interface LiveImage {
	void draw(LiveImageBuilder imageBuilder) throws DrawLiveImageException;
}
