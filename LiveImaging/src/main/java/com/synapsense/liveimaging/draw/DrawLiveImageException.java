package com.synapsense.liveimaging.draw;

public class DrawLiveImageException extends Exception {

	private static final long serialVersionUID = -2691949440505447108L;

	public DrawLiveImageException() {
	}

	public DrawLiveImageException(String arg0) {
		super(arg0);
	}

	public DrawLiveImageException(Throwable arg0) {
		super(arg0);
	}

	public DrawLiveImageException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}
}
