package com.synapsense.liveimaging.draw;

public class ValuePoint {

	private Point2F coord;

	protected float value;

	public ValuePoint(float x, float y, float value) {
		coord = new Point2F(x, y);
		this.value = value;
	}

	public ValuePoint(float x, float y) {
		this(x, y, 0);
	}

	public float getDistance(ValuePoint other) {
		return coord.getDistance(other.coord);
	}

	public float getDistance(float x, float y) {
		return coord.getDistance(x, y);
	}

	public float getX() {
		return coord.getX();
	}

	public float getY() {
		return coord.getY();
	}

	public Point2F getCoord() {
		return coord;
	}

	public float getValue() {
		return value;
	}

	@Override
	public String toString() {
		return "[coord=" + coord + ", value=" + value + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((coord == null) ? 0 : coord.hashCode());
		long temp;
		temp = Double.doubleToLongBits(value);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ValuePoint other = (ValuePoint) obj;
		if (coord == null) {
			if (other.coord != null)
				return false;
		} else if (!coord.equals(other.coord))
			return false;
		if (Double.doubleToLongBits(value) != Double.doubleToLongBits(other.value))
			return false;
		return true;
	}

	public void setValue(float value2) {
		this.value = value2;

	}
}
