package com.synapsense.liveimaging.draw;

public class EValuePoint extends ValuePoint {

	public EValuePoint(float x, float y, float value) {
		super(x, y, value);
	}

	public void setValue(float value) {
		this.value = value;
	}
}
