package com.synapsense.liveimaging;

import com.synapsense.liveimaging.conf.ConfigurationException;
import com.synapsense.liveimaging.conf.LiConfigFactory;
import com.synapsense.liveimaging.es.EsConfigurationLoader;
import java.util.concurrent.locks.ReentrantLock;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class LiveImagingApp {

	private enum State {STOPPED, SLEEPING, RUNNING}

	private final EsConfigurationLoader configLoader;
	private final ReentrantLock lock;

	private ClassPathXmlApplicationContext ctx;
	private State state;
	private Thread sleepingThread;

	private static final long SLEEP_PERIOD = 15 * 1000;

	private static final Log log = LogFactory.getLog(LiveImagingApp.class);

	public LiveImagingApp() {
		configLoader = new EsConfigurationLoader(this);
		lock = new ReentrantLock();
		state = State.STOPPED;
	}

	public void startLiveImaging() throws ConfigurationException, InterruptedException {
		lock.lock();
		try {
			if (state != State.STOPPED) {
				log.warn("Ignoring start request while " + state);
				return;
			}
			log.info("Starting LiveImaging application ...");
			// First, load config from ES
			while (true) {
				try {
					LiConfigFactory.setConfigText(configLoader.getConfig());
					break;
				} catch (ConfigurationException e) {
					throw e;
				} catch (Exception e) {
					log.warn("Error loading configuration, sleeping for a while ...", e);
					sleepWithoutLock();
				}
			}
			// Now we can initialize spring and try to start all the services.
			ctx = new ClassPathXmlApplicationContext("liveimaging-spring.xml");
			ctx.start();
			log.info("LiveImaging application has been started.");
			state = State.RUNNING;
		} finally {
			lock.unlock();
		}
	}

	/** Sleep without holding the lock */
	private void sleepWithoutLock() throws InterruptedException {
		state = State.SLEEPING;
		// Expose the current thread so that the stopping thread can interrupt.
		sleepingThread = Thread.currentThread();
		// Release the lock while sleeping so the stopping thread can get in.
		lock.unlock();
		try {
			Thread.sleep(SLEEP_PERIOD);
		} finally {
			// Re-acquire the lock after the sleep
			lock.lock();
		}
		state = State.STOPPED;
		sleepingThread = null;
		// Deal with the race condition of being interrupted after Thread.sleep() returns
		if (Thread.interrupted()) {
			throw new InterruptedException();
		}
	}

	public void stopLiveImaging() {
		lock.lock();
		try {
			if (state == State.STOPPED) {
				log.warn("Ignoring stop request while " + state);
				return;
			}
			log.info("Stopping LiveImaging ...");
			if (state == State.SLEEPING) {
				log.info("LiveImaging is sleeping between retries in another thread and will be interrupted.");
				sleepingThread.interrupt();
			}
			// Stop the Spring container. The context could be null if we're still trying to connect to ES.
			if (ctx != null) {
				ctx.close();
				ctx = null;
			}
			LiConfigFactory.setConfigText(null);
			log.info("LiveImaging stopped.");
			state = State.STOPPED;
		} catch (Exception e) {
			log.error("LiveImaging encountered an error while stopping.", e);
		} finally {
			lock.unlock();
		}
	}
}
