package com.synapsense.liveimaging;

public class LiveImagingException extends RuntimeException {

	private static final long serialVersionUID = -9157715437355440244L;

	public LiveImagingException() {
		super();
	}

	public LiveImagingException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public LiveImagingException(String arg0) {
		super(arg0);
	}

	public LiveImagingException(Throwable arg0) {
		super(arg0);
	}
}
