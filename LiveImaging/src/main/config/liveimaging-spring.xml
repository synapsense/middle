<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:mvc="http://www.springframework.org/schema/mvc"
       xmlns:context="http://www.springframework.org/schema/context"
       xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd
		http://www.springframework.org/schema/context http://www.springframework.org/schema/context/spring-context.xsd">

    <!-- Factory produced beans -->
    <bean id="config" class="com.synapsense.liveimaging.conf.LiConfigFactory"/>

    <bean id="cachedInterpolationFactory" factory-bean="liInterpolationEngine"
          factory-method="getCachedInterpolationFactory"/>

    <!-- Configure LI Interpolation engine with appropriate interpolators -->
    <bean
            class="com.synapsense.liveimaging.interpolation.engine.LiInterpolationEngine"
            id="liInterpolationEngine">
        <constructor-arg>
            <list>
                <bean class="com.synapsense.liveimaging.interpolation.InterpolatorConf">
                    <property name="name" value="Bilinear15"/>
                    <property name="clazz"
                              value="com.synapsense.liveimaging.interpolation.methods.BiLinear"/>
                    <property name="limit" value="15"/>
                    <property name="properties">
                        <map>
                            <entry key="gridSize" value="150"/>
                        </map>
                    </property>
                </bean>
                <bean class="com.synapsense.liveimaging.interpolation.InterpolatorConf">
                    <property name="name" value="Bilinear150"/>
                    <property name="clazz"
                              value="com.synapsense.liveimaging.interpolation.methods.CachedBiLinear"/>
                    <property name="limit" value="100"/>
                    <property name="properties">
                        <map>
                            <entry key="gridSize" value="150"/>
                        </map>
                    </property>
                </bean>
                <bean class="com.synapsense.liveimaging.interpolation.InterpolatorConf">
                    <property name="name" value="Bilinear60"/>
                    <property name="clazz"
                              value="com.synapsense.liveimaging.interpolation.methods.CachedBiLinear"/>
                    <property name="limit" value="300"/>
                    <property name="properties">
                        <map>
                            <entry key="gridSize" value="60"/>
                        </map>
                    </property>
                </bean>
                <bean class="com.synapsense.liveimaging.interpolation.InterpolatorConf">
                    <property name="name" value="Bilinear40"/>
                    <property name="clazz"
                              value="com.synapsense.liveimaging.interpolation.methods.CachedBiLinear"/>
                    <property name="limit" value="2000"/>
                    <property name="properties">
                        <map>
                            <entry key="gridSize" value="40"/>
                        </map>
                    </property>
                </bean>
                <bean class="com.synapsense.liveimaging.interpolation.InterpolatorConf">
                    <property name="name" value="Bilinear60"/>
                    <property name="clazz"
                              value="com.synapsense.liveimaging.interpolation.methods.NaturalNeighbor"/>
                    <property name="limit" value="10000"/>
                </bean>
            </list>
        </constructor-arg>
    </bean>

    <!-- Image Ripper -->
    <bean class="com.synapsense.liveimaging.job.ImageRipper"
          id="rtImageRipper">
        <constructor-arg ref="memoryDataStorage"/>
        <constructor-arg ref="compositeDataStorageWriter"/>
        <constructor-arg value="#{config.realtimeGenerator.keepPeriod}"/>
        <property name="esContext" ref="esContext"/>
        <property name="complainer" ref="complainer"/>
    </bean>

    <!-- Real Time Image Generator -->
    <bean class="com.synapsense.liveimaging.job.RealTimeImageGenerator"
          id="realTimeImageGenerator" init-method="start" destroy-method="stop">
        <property name="drawCron" value="#{config.realtimeGenerator.drawTrigger}"/>
        <property name="expireCron" value="#{config.realtimeGenerator.expireTrigger}"/>
        <property name="imageFormat" value="#{config.imageFormat}"/>
        <property name="outputDir" value="#{config.outputDir}"/>
        <property name="scheduleService" ref="scheduleService"/>
        <property name="dataSource" ref="esDataSource"/>
        <property name="imageWriter" ref="compositeDataStorageWriter"/>
        <property name="complainer" ref="complainer"/>
        <property name="interpolationFactory" ref="rtCachedInterpolationFactory"/>
        <property name="nonCachingInterpolationFactory">
            <bean factory-bean="liInterpolationEngine" factory-method="getInterpolationFactory"/>
        </property>
        <property name="imageRipper" ref="rtImageRipper"/>
    </bean>

    <!-- Historical Image Generator -->
    <bean class="com.synapsense.liveimaging.job.HistoricalImageRipper"
          id="histImageRipper">
        <constructor-arg ref="memoryDataStorage"/>
        <constructor-arg ref="compositeDataStorageWriter"/>
        <constructor-arg value="#{config.outputDir}"/>
        <constructor-arg value="#{config.historicalGenerator.histKeepPeriod}"/>
    </bean>
    <bean class="com.synapsense.liveimaging.job.HistoricalImageGenerator"
          id="historicalImageGenerator" init-method="start" destroy-method="stop">
        <property name="histExpireCron"
                  value="#{config.historicalGenerator.histExpireTrigger}"/>
        <property name="imageFormat" value="#{config.imageFormat}"/>
        <property name="outputDir" value="#{config.outputDir}"/>
        <property name="scheduleService" ref="scheduleService"/>
        <property name="dataSource" ref="esDataSource"/>
        <property name="imageWriter" ref="compositeDataStorageWriter"/>
        <property name="complainer" ref="complainer"/>
        <property name="histImageRipper" ref="histImageRipper"/>
        <!-- Historical Image generator has to use it's own not cached interpolator -->
        <property name="interpolationFactory">
            <bean factory-bean="liInterpolationEngine" factory-method="getInterpolationFactory"/>
        </property>
    </bean>

    <bean
            class="com.synapsense.liveimaging.interpolation.engine.CachedInterpolationFactory"
            id="rtCachedInterpolationFactory" factory-bean="liInterpolationEngine"
            factory-method="getCachedInterpolationFactory"/>

    <bean id="scheduleService" class="com.synapsense.liveimaging.job.ScheduleService"
          init-method="start" destroy-method="stop">
        <constructor-arg value="3"/>
    </bean>

    <bean id="compositeDataStorageWriter"
          class="com.synapsense.liveimaging.storage.CompositeDataStorageWriter">
        <property name="storages">
            <list>
                <ref bean="fileSystemStorage"/>
                <ref bean="memoryDataStorage"/>
            </list>
        </property>
    </bean>

    <!-- Data storage writers -->
    <bean id="fileSystemStorage" class="com.synapsense.liveimaging.storage.FileSystemStorage">
        <property name="outputDir" value="#{config.outputDir}"/>
        <property name="complainer" ref="complainer"/>
    </bean>
    <bean id="memoryDataStorage" class="com.synapsense.liveimaging.storage.MemoryDataStorage">
        <constructor-arg name="outputDir" value="#{config.outputDir}"/>
    </bean>
    <bean class="com.synapsense.liveimaging.es.ESComplainer" id="complainer">
        <property name="esContext" ref="esContext"></property>
    </bean>

    <bean class="com.synapsense.liveimaging.api.LiveImagingMemoryImpl"
          id="liveImagingAPI">
        <constructor-arg ref="memoryDataStorage"/>
        <constructor-arg ref="colorManager"/>
        <constructor-arg ref="historicalImageGenerator"/>
        <constructor-arg ref="realTimeImageGenerator"/>
        <property name="esContext" ref="esContext"/>
    </bean>

    <bean class="com.synapsense.liveimaging.api.smartzone.LiveImagingServiceImpl"
          id="liveImagingSZAPI">
        <constructor-arg ref="memoryDataStorage"/>
        <property name="esContext" ref="esContext"/>
        <property name="esDataSource" ref="esDataSource"/>
    </bean>

    <!-- Resource server -->
    <bean class="com.synapsense.liveimaging.web.ResourceServer" id="resourceServer" init-method="startServer"
          destroy-method="stop">
        <constructor-arg value="#{config.webserverPort}"/>
        <constructor-arg name="outputDir" value="#{config.outputDir}"/>
        <property name="complainer" ref="complainer"></property>
    </bean>

    <bean class="com.synapsense.liveimaging.legend.ColorManager" id="colorManager">
        <constructor-arg>
            <bean class="com.synapsense.liveimaging.legend.LegendGenerator">
                <constructor-arg>
                    <bean
                            class="com.synapsense.liveimaging.storage.CompositeDataStorageWriter">
                        <property name="storages">
                            <list>
                                <bean class="com.synapsense.liveimaging.storage.FileSystemStorage">
                                    <property name="outputDir" value="#{config.outputDir}"/>
                                </bean>
                                <ref bean="memoryDataStorage"/>
                            </list>
                        </property>
                    </bean>
                </constructor-arg>
            </bean>
        </constructor-arg>
    </bean>

    <bean class="com.synapsense.liveimaging.es.EsDataSource" id="esDataSource" init-method="init"
          destroy-method="destroy">
        <property name="levels" value="#{config.generationLevel}"/>
        <property name="colorManager" ref="colorManager"/>
        <property name="complainer" ref="complainer"/>
        <property name="esContext" ref="esContext"/>
        <property name="toInvalidate" ref="rtCachedInterpolationFactory"></property>
    </bean>

    <bean class="com.synapsense.liveimaging.es.ESContext" id="esContext" init-method="connect"
          destroy-method="disconnect"/>

    <!-- this is how proper quartz integration could look like -->
    <!-- bean name="pushJob" class="org.springframework.scheduling.quartz.JobDetailBean">
        <property name="jobClass" value="com.synapsense.liveimaging.job.PushJob"/>
    </bean>

    <bean name="expireJob" class="org.springframework.scheduling.quartz.JobDetailBean">
        <property name="jobClass" value="com.synapsense.liveimaging.job.ImageRipper"/>
    </bean>

    <bean name="historicalExpireJob" class="org.springframework.scheduling.quartz.JobDetailBean">
        <property name="jobClass" value="com.synapsense.liveimaging.job.HistoricalImageRipper"/>
    </bean>

    <bean id="pushJobTrigger" class="org.springframework.scheduling.quartz.CronTriggerBean">
        <property name="jobDetail" ref="pushJob"/>
        <property name="cronExpression" value="#{config.realtimeGenerator.drawTrigger}"/>
    </bean>

    <bean id="expireJobTrigger" class="org.springframework.scheduling.quartz.CronTriggerBean">
        <property name="jobDetail" ref="expireJob"/>
        <property name="cronExpression" value="#{config.realtimeGenerator.expireTrigger}"/>
    </bean>

    <bean id="historicalExpireJobTrigger" class="org.springframework.scheduling.quartz.CronTriggerBean">
        <property name="jobDetail" ref="historicalExpireJob"/>
        <property name="cronExpression"
                  value="#{config.historicalGenerator.histExpireTrigger}"/>
    </bean>

    <bean class="org.springframework.scheduling.quartz.SchedulerFactoryBean">
        <property name="org.quartz.threadPool.class" value="org.quartz.simpl.SimpleThreadPool"/>
        <property name="org.quartz.threadPool.threadCount" value="3"/>
        <property name="triggers">
            <list>
                <ref bean="pushJobTrigger"/>
                <ref bean="expireJobTrigger"/>
                <ref bean="historicalExpireJobTrigger"/>
            </list>
        </property>
    </bean -->


</beans>
