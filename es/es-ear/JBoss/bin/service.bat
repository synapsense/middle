@echo off
REM JBoss, the OpenSource webOS
REM
REM Distributable under LGPL license.
REM See terms of license at gnu.org.
REM
REM -------------------------------------------------------------------------
REM JBoss Service Script for Windows
REM -------------------------------------------------------------------------

setlocal

set "SVCNAME=@SVCNAME@"
set "SVCDISP=@SVCNAME@"
set "SVCDESC=SynapSense Environment Server"

set "DIRNAME=%~dp0%"
set "JBOSSSVC=%DIRNAME%jbosssvc.exe"
set "START_LOG=%DIRNAME%..\standalone\log\service-start.log"
set "STOP_LOG=%DIRNAME%..\standalone\log\service-stop.log"
set "NOPAUSE=Y"


REM [BC 07/2015] Comment out -Xrs.
REM Customizing JAVA_OPTS here disables the customization from standalone.conf.bat.
REM On Java 8 / Windows 7 the service is not terminated on a logoff event.
REM set JAVA_OPTS=-Xrs

REM Figure out the running mode

if /I "%1" == "install"   goto cmdInstall
if /I "%1" == "uninstall" goto cmdUninstall
if /I "%1" == "start"     goto cmdStart
if /I "%1" == "stop"      goto cmdStop
if /I "%1" == "restart"   goto cmdRestart
if /I "%1" == "signal"    goto cmdSignal
echo Usage: service install^|uninstall^|start^|stop^|restart^|signal
goto cmdEnd

REM jbosssvc retun values
REM ERR_RET_USAGE           1
REM ERR_RET_VERSION         2
REM ERR_RET_INSTALL         3
REM ERR_RET_REMOVE          4
REM ERR_RET_PARAMS          5
REM ERR_RET_MODE            6

:errExplain
if errorlevel 1 echo Invalid command line parameters
if errorlevel 2 echo Failed installing %SVCDISP%
if errorlevel 4 echo Failed removing %SVCDISP%
if errorlevel 6 echo Unknown service mode for %SVCDISP%
goto cmdEnd

:cmdInstall
rem The service is configured for manual startup (intended for development)
rem The -w option to specify the working directory does not work.
rem jbossvc.exe gets confused when associateing arguments with options
"%JBOSSSVC%" -idcm %SVCNAME% "%SVCDISP%" "%SVCDESC%" service.bat
if not errorlevel 0 goto errExplain
echo Service %SVCDISP% installed
goto cmdEnd

:cmdUninstall
"%JBOSSSVC%" -u %SVCNAME%
if not errorlevel 0 goto errExplain
echo Service %SVCDISP% removed
goto cmdEnd

:cmdStart
REM Executed on service start
del .r.lock 2>&1 | findstr /C:"being used" > nul
if not errorlevel 1 (
  echo Could not continue. Locking file already in use.
  goto cmdEnd
)
echo Y > .r.lock
rem Create the log directory
mkdir "%DIRNAME%..\standalone\log"
"%JBOSSSVC%" -p 1 "Starting %SVCDISP%" > "%START_LOG%"
call standalone.bat -b 0.0.0.0 < .r.lock >> "%START_LOG%" 2>&1
"%JBOSSSVC%" -p 1 "Shutdown %SVCDISP% service" >> "%START_LOG%"
del .r.lock
goto cmdEnd

:cmdStop
REM Executed on service stop
echo Y > .s.lock
%JBOSSSVC% -p 1 "Shutting down %SVCDISP%" > "%STOP_LOG%".log
call jboss-cli.bat --connect command=:shutdown < .s.lock >> "%STOP_LOG%" 2>&1
%JBOSSSVC% -p 1 "Shutdown %SVCDISP% service" >> "%STOP_LOG%"
del .s.lock
goto cmdEnd

:cmdRestart
REM Executed manually from command line
REM Note: We can only stop and start
echo Y > .s.lock
%JBOSSSVC% -p 1 "Shutting down %SVCDISP%" >> shutdown.log
del .s.lock
:waitRun
REM Delete lock file
del .r.lock > nul 2>&1
REM Wait one second if lock file exist
%JBOSSSVC% -s 1
if exist ".r.lock" goto waitRun
echo Y > .r.lock
%JBOSSSVC% -p 1 "Restarting %SVCDISP%" >> run.log
call standalone.bat < .r.lock >> run.log 2>&1
%JBOSSSVC% -p 1 "Shutdown %SVCDISP% service" >> run.log
del .r.lock
goto cmdEnd

:cmdSignal
REM Send signal to the service.
REM Requires jbosssch.dll to be loaded in JVM
@if not ""%2"" == """" goto execSignal
echo Missing signal parameter.
echo Usage: service signal [0...9]
goto cmdEnd
:execSignal
%JBOSSSVC% -k%2 %SVCNAME%
goto cmdEnd

:cmdEnd
