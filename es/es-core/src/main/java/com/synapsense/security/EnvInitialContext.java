package com.synapsense.security;

import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 * @author : shabanov
 */
public class EnvInitialContext extends InitialContext implements AutoCloseable {
    public EnvInitialContext() throws NamingException {
        super();
    }
}
