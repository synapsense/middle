package com.synapsense.security;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.security.Principal;
import java.security.acl.Group;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.security.auth.Subject;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.login.LoginException;
import javax.security.auth.spi.LoginModule;

import com.synapsense.dto.TO;
import com.synapsense.util.CollectionUtils;
import org.apache.log4j.Logger;
import org.jboss.security.SimplePrincipal;

import com.synapsense.exception.UserManagementException;
import com.synapsense.service.UserManagementService;

/**
 * This module does not perform any validation per se but merely updates user
 * information stored in SynapSense DB with data from an already authenticated
 * Subject.<br>
 * In addition to that it performs two more steps:
 * <ol>
 * <li>If any subject's role has matching group in ES then it will add that
 * group privileges (recursively) to the subject's roles. Effectively, this step
 * maps ES groups/privileges to JBoss/EJB roles</li>
 * <li>If any attribute mappings are specified and corresponding attributes were
 * provided by previous login modules then it will use them to create/update
 * user fields (first/last name, email, title etc)</li>
 * </ol>
 * Module options:
 * <ul>
 * <li><i>userAttributes</i> A string in form userField=attrName[,...] where
 * userField is User class setter name and attrName is an attribute name.<br>
 * For example, fName=givenName,title=title will take values of attributes
 * 'givenName' and 'title' and pass them to User.setFName() and User.setTitle()
 * methods. <b>Optional; default is none (null string)</b></li>
 * <li><i>attributesPrincipal</i> Name of a Group principal to take attribute
 * values from. <b>Optional; default is "Attributes"</b></li>
 * </ul>
 * Note: this module works via ES API; you need to add RunAsLoginModule after
 * (after, not before) it.
 * 
 * @see LdapExtLoginModule
 * 
 * @author Oleg Stepanov
 * 
 */
public class UpdateSynapUserLoginModule implements LoginModule {

	private static final Logger log = Logger.getLogger(UpdateSynapUserLoginModule.class);

	private static final String UMS_LOCAL_JNDI_NAME = "java:global/es-ear/es-core/UserManagementService";
	private static final String ATTR_MAPPING_OPT = "userAttributes";
	private static final String ATTRIBUTES_PRINCIPAL_OPT = "attributesPrincipal";

	private Subject subject;// a subject being authenticated

	private Map<String, String> attrMapping = new HashMap<>();
	private String attributesPrincipalName = "Attributes";

	@Override
	public void initialize(Subject subject, CallbackHandler callbackHandler, Map<String, ?> sharedState,
	        Map<String, ?> options) {
		this.subject = subject;

		String tmp = (String) options.get(ATTRIBUTES_PRINCIPAL_OPT);
		if (tmp != null && !tmp.isEmpty()) {
			attributesPrincipalName = tmp;
		}
		log.debug("Attributes principal name set to " + attributesPrincipalName);

		String userAttributes = (String) options.get(ATTR_MAPPING_OPT);
		if (userAttributes != null && !userAttributes.isEmpty()) {
			if (log.isDebugEnabled())
				log.debug("Parsing attributes mapping string: " + userAttributes);

			String[] pairs = userAttributes.split(",");
			for (String pair : pairs) {
				String[] sides = pair.split("=");
				if (sides.length != 2) {
					log.warn("Unrecognized mapping pair \"" + pair + "\" in option " + ATTR_MAPPING_OPT);
					continue;
				}

				if (log.isDebugEnabled())
					log.debug("Found user attribute mapping: " + sides[0] + "=" + sides[1]);
				attrMapping.put(sides[0], sides[1]);
			}
		}
	}

	@Override
	public boolean login() throws LoginException {
		// we cannot do anything meaningful during login phase because
		// other modules have not populated Subject with principals yet
		// see commit() method
		return true;
	}

	@Override
	public boolean commit() throws LoginException {
		// assume that previous login modules have already
		// added principals to the subject.

		// use the first non-group principal to get user name
		Principal principal = null;
		for (Principal p : subject.getPrincipals()) {
			if (p instanceof Group == false) {
				principal = p;
				break;
			}
		}
		if (principal == null) {
			throw new LoginException("No user principal created by previous login modules");
		}

		String userName = principal.getName();

		InitialContext ctx = null;
		try {

			ctx = new InitialContext();
			UserManagementService ums = (UserManagementService) ctx.lookup(UMS_LOCAL_JNDI_NAME);

			// let's see if previous modules have found any roles with matching
			// groups in ES
			Group subjectRoles = getGroup(subject, "Roles");
			Collection<String> esGroups = filterEsUserGroups(subjectRoles, ums);

			TO<?> user = ums.getUser(userName);
			if (user == null) {
				// if user logs in for the first time and has no matching ES
				// groups then we don't create an account for him
				if (esGroups.isEmpty())
					throw new LoginException("User " + userName + " has no matching ES roles");

				// else
				user = ums.createUser(userName, "new_pass", null);
				// just make sure it won't match SHA1 for any password
				ums.setRawPassword(user, "LDAP");
			}

			// check if there are any updates in user info
			Map<String, Object> updates = updateUserFromSubject(subject, attributesPrincipalName, attrMapping);
			if (!updates.isEmpty())
				ums.updateUser(user, updates);

			updateUserGroups(user, subjectRoles, esGroups, ums);

		} catch (Exception e) {
			throw (LoginException) new LoginException("Cannot update user info for user " + userName).initCause(e);
		} finally {
			try {
				if (ctx != null)
					ctx.close();
			} catch (NamingException e) {
				log.warn("Cannot close InitialContext", e);
			}
		}
		return true;
	}

	private static Collection<String> filterEsUserGroups(Group group, UserManagementService ums)
	        throws UserManagementException {
		if (group == null) {
			return Collections.emptyList();
		}

		// get all ES groups
		Collection<TO<?>> allEsGroups = ums.getAllGroups();

		// filter those which are assigned to the subject
		Collection<String> subjectRoles = getRoleNames(group);
		Collection<String> esGroups = new LinkedList<>();
		for (TO<?> g : allEsGroups) {
			String name = (String) ums.getProperty(g, ums.GROUP_NAME);
			if (subjectRoles.contains(name)) {
				esGroups.add((String) ums.getProperty(g, ums.GROUP_NAME));
			}
		}
		
		return esGroups;
	}
	
	static private void updateUserGroups(TO<?> user, Group roles, Collection<String> newEsGroups,
	        UserManagementService ums) throws UserManagementException {

		// update user groups
		if (log.isDebugEnabled()) {
			log.debug("Updating ES groups for user " + user.toString() + "; new groups: " + newEsGroups);
		}
		ums.setUserGroups(user, newEsGroups);

		// 2. Add ES privileges as subject roles for JBoss authorization
		// subsystem
		Set<String> privileges = combineWithChildren(ums.getUserPrivilegeIds(user), ums);
		if (log.isDebugEnabled()) {
			StringBuilder sb = new StringBuilder();
			sb.append("Adding roles for user " + user.toString() + ":\r\n");
			for (String privilege : privileges) {
				sb.append(privilege + "\r\n");
			}
			log.debug(sb.toString());
		}
		for (String privilege : privileges) {
			roles.addMember(new SimplePrincipal(privilege));
		}
	}

	private static Group getGroup(Subject subject, String name) {
		for (Group g : subject.getPrincipals(Group.class)) {
			if (name.equals(g.getName())) {
				return g;
			}
		}
		return null;
	}

	private static Collection<String> getRoleNames(Group group) {
		Collection<String> names = new LinkedList<>();
		Enumeration<? extends Principal> roles = group.members();
		while (roles.hasMoreElements()) {
			names.add(roles.nextElement().getName());
		}
		return names;
	}

	/**
	 * Updates field values with given mappings from subject.
	 * 
	 * @return updated user object if any mapping were found; null otherwise
	 */
	private static Map<String, Object> updateUserFromSubject(Subject subject, String attrGroupName,
	        Map<String, String> mapping) {
		if (mapping.isEmpty()) {
			log.debug("No attribute mapping specified");
			return null;
		}

		Group attrGroup = getGroup(subject, attrGroupName);
		if (attrGroup == null) {
			log.warn("Group \"" + attrGroupName + "\" not found in subject; attribute mapping will not be applied");
			return null;
		}

		// get attributes from Subject into convenient map
		Map<String, String> attributes = new HashMap<>();
		Enumeration<? extends Principal> subGroups = attrGroup.members();
		while (subGroups.hasMoreElements()) {
			Principal attr = subGroups.nextElement();
			if (attr instanceof Group) {
				Enumeration<? extends Principal> values = ((Group) attr).members();
				if (values.hasMoreElements()) {
					attributes.put(attr.getName(), values.nextElement().getName());
					if (log.isDebugEnabled() && values.hasMoreElements()) {
						log.debug("More than one value present for " + attr.getName() + "; the first one used");
					}
				}
			}
		}

		boolean isDirty = false;

		Map<String, Object> props = CollectionUtils.newMap();
		for(Map.Entry<String, String> pair : mapping.entrySet()) {
			props.put(pair.getKey(), attributes.get(pair.getValue()));
			isDirty = true;
		}

		return props;
	}

	private static Set<String> combineWithChildren(Collection<String> parentRoles, UserManagementService ums)
	        throws UserManagementException {
		Set<String> result = new HashSet<>();
		for(String parentRole : parentRoles) {
			Collection<String> children = ums.getChildrenPrivilegeIds(parentRole);
			result.addAll(combineWithChildren(children, ums));
			result.add(parentRole);
		}
		return result;
	}

	@Override
	public boolean abort() throws LoginException {
		return true;
	}

	@Override
	public boolean logout() throws LoginException {
		// in fact, here we may want to remove from subject those roles
		// that we added in commit() but so far I don't see where it
		// may cause any problems
		return true;
	}

}
