package com.synapsense.security;

import com.synapsense.dto.TO;
import com.synapsense.exception.UserManagementException;
import com.synapsense.service.UserManagementService;
import com.synapsense.util.CollectionUtils;
import org.jboss.security.RunAsIdentity;
import org.jboss.security.SecurityContextAssociation;
import org.jboss.security.SecurityContextFactory;
import org.jboss.security.identity.RoleGroup;

import javax.naming.NamingException;
import java.security.Principal;
import java.util.Set;

/**
 * The class must extend RunAsIdentity to be lawful citizen of Jboss Security
 * subsystem.
 * 
 * An idea is to have ES based "run as" identity implementation. One can treat
 * it as an adapter of UserManagementService to Jboss Security
 * 
 * @author : shabanov
 */
public class RunAsEsUser extends RunAsIdentity {

	public static interface Action<V, E extends Exception> {
		V run() throws E;
	}

	// no security check for internal use
	private static final String UMS_LOCAL_JNDI_NAME = "java:global/es-ear/es-core/LocalUserManagementService";
	private static final String DEFAULT_SECURITY_DOMAIN = "SynapServer";

	private RunAsIdentity jbossDelegeate;

	public static boolean isPrincipalExist(String principal) {
		try (EnvInitialContext ctx = new EnvInitialContext()){
			UserManagementService ums = (UserManagementService) ctx.lookup(UMS_LOCAL_JNDI_NAME);
			return ums.getUser(principal) != null;
		} catch (NamingException e) {
			throw new IllegalStateException("Failed to lookup user management service", e);
        } 
	}
	
	public static <V, E extends Exception> V execute(final String principal, Action<V, E> action) throws E {
		if (principal == null || principal.isEmpty())
			return action.run();
        // prevent to create RunAs for anonymous ID  - it must have at least one role specified.
        if (principal.equals("anonymous"))
            return action.run();


        boolean callerHasContext = SecurityContextAssociation.getSecurityContext() != null;
		try {
			if (!callerHasContext) {
				try {
					SecurityContextAssociation.setSecurityContext(SecurityContextFactory
					        .createSecurityContext(DEFAULT_SECURITY_DOMAIN));
				} catch (Exception e) {
					throw new IllegalStateException("Failed to create security context for domain : "
					        + DEFAULT_SECURITY_DOMAIN, e);
				}
			}
			SecurityContextAssociation.pushRunAsIdentity(new RunAsEsUser(principal));
			return action.run();
		} finally {
            if (callerHasContext) {
                SecurityContextAssociation.popRunAsIdentity();
            } else {
                SecurityContextAssociation.clearSecurityContext();
            }
		}
	}

	private RunAsEsUser(String userName) {
        // we won't use "this" object
		super("ACCESS_WEB_CONSOLE", userName);

        String mainRole = null;
        Set<String> runAsRoles = CollectionUtils.newSet();
        // build roles list
		try (EnvInitialContext ctx = new EnvInitialContext()){
			UserManagementService ums = (UserManagementService) ctx.lookup(UMS_LOCAL_JNDI_NAME);
			TO<?> user = ums.getUser(userName);
			for (String parentRole : ums.getUserPrivilegeIds(user)) {
                if (mainRole==null) {
                    mainRole = parentRole;
                } else  {
                    runAsRoles.add(parentRole);
                }
				for (String childRole : ums.getChildrenPrivilegeIds(parentRole)) {
					runAsRoles.add(childRole);
				}
			}
		} catch (NamingException e) {
			throw new IllegalStateException("Failed to lookup user management service", e);
        } catch (UserManagementException e) {
            throw new IllegalStateException("Failed to use user management service", e);
        }

        if (mainRole == null) {
             throw new IllegalStateException("User " + userName + " should have at least one role assigned");
        }
        this.jbossDelegeate = new RunAsIdentity(mainRole, userName, runAsRoles);
	}

	// we should override all methods
	// all calls are delegated to jbossDelegeate

	@Override
	public Set<Principal> getRunAsRoles() {
		return jbossDelegeate.getRunAsRoles();
	}

	@Override
	public RoleGroup getRunAsRolesAsRoleGroup() {
		return jbossDelegeate.getRunAsRolesAsRoleGroup();
	}

	@Override
	public synchronized Set<Principal> getPrincipalsSet() {
		return jbossDelegeate.getPrincipalsSet();
	}

	@Override
	public boolean doesUserHaveRole(Principal role) {
		return jbossDelegeate.doesUserHaveRole(role);
	}

	@Override
	public boolean doesUserHaveRole(Set<Principal> methodRoles) {
		return jbossDelegeate.doesUserHaveRole(methodRoles);
	}

	@Override
	public synchronized Object clone() throws CloneNotSupportedException {
		return jbossDelegeate.clone();
	}

	@Override
	public String toString() {
		return jbossDelegeate.toString();
	}
}
