package com.synapsense.security;

import java.security.acl.Group;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.InitialLdapContext;
import javax.security.auth.Subject;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.login.LoginException;

import org.apache.log4j.Logger;
import org.jboss.security.SimpleGroup;
import org.jboss.security.SimplePrincipal;

/**
 * A slightly modified version of
 * org.jboss.security.auth.spi.LdapExtLoginModule.<br>
 * Allows using not only fixed credentials for LDAP binding/searching but those
 * of an active Subject, too. Optionally can pull additional attributes from
 * user's LDAP record and inject them into Subject.<br>
 * Modified module options:
 * <ul>
 * <li><i>bindDN, bindCredential</i> These now support macro expansion.
 * Supported macros are {$username} and {$password}. For example, if you need
 * not fixed but current user credentials and some specific domain to be used
 * for LDAP binding then set <i>bindDN=mydomain\{$username}</i> and
 * <i>bindCredential={$password}</i></li>
 * </ul>
 * New module options:
 * <ul>
 * <li><i>getAttributes</i> A comma-separated list of attributes to pull from
 * user record. The record will be located with the same <i>baseCtxDn</i> and
 * <i>baseFilter</i> used for authentication. <b>Optional; default is none (null
 * string)</b></li>
 * <li><i>attributesPrincipal</i> Name of a Group principal to put the
 * attributes into. <b>Optional; default is "Attributes"</b></li>
 * </ul>
 * Other module options are exactly the same as for
 * {@link org.jboss.security.auth.spi.LdapExtLoginModule}
 * 
 * @see UpdateSynapUserLoginModule
 * 
 * @author Oleg Stepanov
 * 
 */
public class LdapExtLoginModule extends org.jboss.security.auth.spi.LdapExtLoginModule {

	private static final String BIND_DN = "bindDN";

	private static final String BIND_CREDENTIAL = "bindCredential";

	private static final String ATTRIBUTES_PRINCIPAL_OPT = "attributesPrincipal";
	private static final String GET_ATTRIBUTES_OPT = "getAttributes";

	private static String USER_MACRO = "username";
	private static String PASSWD_MACRO = "password";

	private Group attrGroup = new SimpleGroup("Attributes");
	private String[] getAttributes;


	@Override
	@SuppressWarnings("rawtypes")
	public void initialize(Subject subject, CallbackHandler callbackHandler, Map sharedState, Map options) {
		super.initialize(subject, callbackHandler, sharedState, options);

		String tmp = (String) options.get(ATTRIBUTES_PRINCIPAL_OPT);
		if (tmp != null && !tmp.isEmpty())
			attrGroup = new SimpleGroup(tmp);

		tmp = (String) options.get(GET_ATTRIBUTES_OPT);
		if (tmp != null && !tmp.isEmpty())
			getAttributes = tmp.split(",");

	}

	@Override
	@SuppressWarnings("unchecked")
	protected boolean validatePassword(String inputPassword, String expectedPassword) {
		// swap the original unmodifiable map with temporary one
		Map<?, ?> oldOptions = options;
		options = new HashMap<Object, Object>(oldOptions);

		// replace macros in bindDN and bindCredentials options
		Map<String, String> values = new HashMap<String, String>(2);
		values.put(USER_MACRO, getUsername());
		values.put(PASSWD_MACRO, inputPassword);

		String tmpString = (String) options.get(BIND_DN);
		if (tmpString != null) {
			options.put(BIND_DN, MacroReplacer.replace(tmpString, values));
		}

		tmpString = (String) options.get(BIND_CREDENTIAL);
		if (tmpString != null) {
			options.put(BIND_CREDENTIAL, MacroReplacer.replace(tmpString, values));
		}

		try {
			// let the base class do its job
			return super.validatePassword(inputPassword, expectedPassword);
		} finally {
			// restore original options map (just in case, you know)
			options = oldOptions;
		}
	}

	@Override
	protected void rolesSearch(InitialLdapContext ctx, SearchControls constraints, String user, String userDN,
	        int recursionMax, int nesting) throws NamingException {
		super.rolesSearch(ctx, constraints, user, userDN, recursionMax, nesting);

		if (getAttributes != null && getAttributes.length > 0) {
			SearchControls attrConstraints = new SearchControls();
			attrConstraints.setSearchScope(SearchControls.SUBTREE_SCOPE);
			attrConstraints.setTimeLimit(searchTimeLimit);
			attrConstraints.setReturningAttributes(getAttributes);

			NamingEnumeration<SearchResult> results = ctx.search(baseDN, baseFilter, new Object[] { user },
			        attrConstraints);

			try {
				if (!results.hasMore()) {
					log.warn("Cannot retrieve attributes " + Arrays.toString(getAttributes) + " for user " + user);
					return;
				}

				Attributes attributes = results.next().getAttributes();
				for (String attrName : getAttributes) {
					Attribute attr = attributes.get(attrName);

					if (attr == null || attr.size() == 0) {
						log.warn("User " + user + ": retrieved no values for attribute " + attrName);

						continue;
					}

					Group holder = new SimpleGroup(attrName);
					attrGroup.addMember(holder);

					for (int i = 0; i < attr.size(); i++) {
						Object value = attr.get(i);
						if (value != null)
							holder.addMember(new SimplePrincipal(value.toString()));
					}
				}
			} finally {
				results.close();
			}
		}
	}

	@Override
	public boolean commit() throws LoginException {
		// we assume here that only one LdapExtLoginModule will
		// authenticate the Subject and populate its attributes group.
		// otherwise we'd need to check if attributes group is already
		// present in the Subject.
		subject.getPrincipals().add(attrGroup);
		return super.commit();
	}

	@Override
	public boolean logout() throws LoginException {
		subject.getPrincipals().remove(attrGroup);
		return super.logout();
	}
}

class MacroReplacer {

	private static Logger log = Logger.getLogger(LdapExtLoginModule.class);

	private static final Pattern macroPattern = Pattern.compile("\\{\\$(\\p{Alnum}+)\\}");

	public static String replace(String inString, Map<String, String> values) {
		Matcher m = macroPattern.matcher(inString);
		StringBuffer sb = new StringBuffer();
		while (m.find()) {
			String value = values.get(m.group(1));
			if (value != null) {
				if (log.isDebugEnabled()) {
					// don't log replacement value since it could be a password!
					log.debug("Macro " + m.group() + " found; replaced");
				}
				m.appendReplacement(sb, Matcher.quoteReplacement(value));
			} else {
				log.warn("Macro " + m.group() + " found but no replacement value has been provided; left as is");
				m.appendReplacement(sb, "$0");
			}
		}
		m.appendTail(sb);
		return sb.toString();
	}

}