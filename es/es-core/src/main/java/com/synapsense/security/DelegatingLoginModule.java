package com.synapsense.security;

import java.util.Map;

import javax.security.auth.Subject;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.login.LoginContext;
import javax.security.auth.login.LoginException;
import javax.security.auth.spi.LoginModule;

import org.apache.log4j.Logger;

/**
 * The module allows one to delegate a login step in one LoginContext to the
 * whole another LoginContext. It can be useful if several login modules with
 * different flags (required, sufficient, optional etc.) should be treated as
 * one logical login module with its own flag.<br>
 * Note that JBoss 7 has its own mechanism for JAAS-to-Realm and Realm-to-JAAS
 * delegation which can be used to achieve the same results (although with more
 * verbose configuration).<br>
 * Module options:
 * <ul>
 * <li><i>loginContextName</i> Name of LoginContext this module instance should
 * delegate to</li>
 * </ul>
 * 
 * @author Oleg Stepanov
 * 
 */
public class DelegatingLoginModule implements LoginModule {

	private static final Logger log = Logger.getLogger(DelegatingLoginModule.class);

	private static final String CONTEXT_NAME_OPT = "loginContextName";

	private String loginContextName;
	private LoginContext loginContext;

	@Override
	public void initialize(Subject subject, CallbackHandler callbackHandler, Map<String, ?> sharedState,
	        Map<String, ?> options) {

		loginContextName = (String) options.get(CONTEXT_NAME_OPT);
		if (loginContextName == null) {
			log.warn(CONTEXT_NAME_OPT + " option is not set; any login attempt will fail");
			return;
		}

		try {
			if (subject != null && callbackHandler != null) {
				loginContext = new LoginContext(loginContextName, subject, callbackHandler);
			} else if (subject != null) {
				loginContext = new LoginContext(loginContextName, subject);
			} else {
				loginContext = new LoginContext(loginContextName, callbackHandler);
			}
		} catch (Exception e) {
			log.warn("Cannot create child login context with configuration " + loginContextName);
		}
	}

	@Override
	public boolean login() throws LoginException {
		if (loginContext == null) {
			throw new LoginException("Child login context with configuration " + loginContextName
			        + " has not been created");
		}
		
		loginContext.login();

		return true;
	}

	@Override
	public boolean commit() throws LoginException {
		return true;
	}

	@Override
	public boolean abort() throws LoginException {
		if (loginContext != null)
			loginContext.logout();
		return true;
	}

	@Override
	public boolean logout() throws LoginException {
		if (loginContext != null)
			loginContext.logout();
		return true;
	}

}
