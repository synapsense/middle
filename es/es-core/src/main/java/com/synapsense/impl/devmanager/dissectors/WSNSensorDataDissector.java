package com.synapsense.impl.devmanager.dissectors;

import com.synapsense.dto.TO;
import com.synapsense.plugin.messages.GenericMessage;
import com.synapsense.plugin.messages.GenericMessage.GenericMessageTypes;
import com.synapsense.plugin.messages.WSNSensorDataBuilder.FieldConstants;
import com.synapsense.service.impl.messages.base.SensorData;
import com.synapsense.util.algo.Pair;
import com.synapsense.util.algo.UnaryFunctor;

public class WSNSensorDataDissector implements MessageDissector {
	private final UnaryFunctor<Pair<TO<?>, SensorData>> functor;

	public WSNSensorDataDissector(UnaryFunctor<Pair<TO<?>, SensorData>> functor) {
		this.functor = functor;
	}

	@Override
	public boolean tasteMessage(GenericMessage msg) {
		return msg.getType() == GenericMessageTypes.WSN_SENSOR_DATA;
	}

	@Override
	public void process(TO<?> objID, GenericMessage msg) {
		if (msg == null)
			throw new IllegalArgumentException("msg cannot be null");
		if (!tasteMessage(msg))
			throw new IllegalArgumentException("Incompatible message type");
		try {
			SensorData data = new SensorData((Integer) msg.get(FieldConstants.SENSOR_ID.name()),
			        (Double) msg.get(FieldConstants.DATA.name()), (Long) msg.get(FieldConstants.TIMESTAMP.name()),
			        (Integer) msg.get(FieldConstants.NETWORK_ID.name()), (Integer) msg.get(FieldConstants.NODE_PLATFORM
			                .name()), (Integer) msg.get(FieldConstants.TYPE_ID.name()));
			functor.process(new Pair<TO<?>, SensorData>(objID, data));
		} catch (Exception e) {
			throw new IllegalArgumentException("Wrong data is stored in message", e);
		}
	}

}
