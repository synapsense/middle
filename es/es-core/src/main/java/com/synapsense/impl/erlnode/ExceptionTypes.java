package com.synapsense.impl.erlnode;
/**
 * 
 * This enumeration contains all the exception types, which are available to
 * return to the controlling module.
 * 
 * @author stikhon
 * 
 */
public enum ExceptionTypes {
	NO_OBJECT_TYPE("NoObjectType", "Object type is not found."),
	ILLEGAL_ARGUMENTS("IllegalArguments", "Tuple has invalid format."),
	NAMING("Naming", ""),
	LOGIN("Login", "Log in cannot be performed."),
	OBJECT_ALREADY_EXISTS("ObjectAlreadyExists", "Same object already exists."),
	PROPERTY_NOT_FOUND("PropertyNotFound", "Property is not found."),
	BAD_TYPE("BadType", "Bad type."),
	OBJECT_NOT_FOUND("ObjectNotFound", "Object not found."), 
	UNSUPPORTED_RELATION("UnsupportedRelation","Unsupported relation."), 
	UNABLE_TO_CONVERT_PROPERTY("UnableToConvertProperty","Unable to convert property."),
	ILLEGAL_INPUT_PARAMETER("IllegalInputParameter","The typename is invalid."), 
	ALERT_SVC_SYSTEM("AlertSvcSystem","Error occurred while rising an alert."), 
	NO_SUCH_MODULE("NoSuchModule","No such module"), 
	NO_SUCH_ACTIVITY("NoSuchActivity","No such activity"),
	THROWABLE("Throwable", ""),
	CANNOT_CONNECT("CannotConnectException", "Can not get connection to server");
	
	private String exceptionType;
	private String defaultMessage;

	ExceptionTypes(final String exceptionType, final String defaultMessage) {
		this.exceptionType = exceptionType;
		this.defaultMessage = defaultMessage;
	}

	public String getExceptionType() {
		return exceptionType;
	}

	public String getDefaultMessage() {
		return defaultMessage;
	}

}
