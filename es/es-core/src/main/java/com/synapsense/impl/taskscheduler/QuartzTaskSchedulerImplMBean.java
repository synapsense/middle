package com.synapsense.impl.taskscheduler;

import com.synapsense.service.ServiceManagement;
import org.quartz.Job;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public interface QuartzTaskSchedulerImplMBean extends ServiceManagement {
	/**
	 * Removes task from the schedule.
	 * 
	 *
     * @param taskName
     *            name of the task to be removed.
     * @throws TaskSchedulerException
	 */
	boolean removeTask(String taskName) throws TaskSchedulerException;

	/**
	 * Removes task from the schedule<br>
	 * Uses only given task group
	 * 
	 * @param group
	 *            Task group name which has task <tt>taskName</tt>
	 * @param taskName
	 *            name of the task to be removed
	 * @throws TaskSchedulerException
	 */
    boolean removeTask(String group, String taskName) throws TaskSchedulerException;


    /**
     * Removes whole group of tasks from the scheduler<br>
     *
     * @param group
     *            Task group name which has task <tt>taskName</tt>
     * @throws TaskSchedulerException
     */
    void removeGroup(String group) throws TaskSchedulerException;

	/**
	 * Schedule new EJB bean as a task for execution.<br>
	 * New job will be put into <tt>DEFAULT</tt> group.
	 * 
	 * @param taskName
	 *            task name
	 * @param taskParams
	 *            cron-formatted expression with task schedule
	 * @param jndiName
	 *            EJB bean JNDI name
	 * @param className
	 *            bean interface name
	 * @param methodName
	 *            bean method to be invoked.
	 * @param params
	 *            parameters for the method to be invoked. Map must contain
	 *            parameter types as keys and arguments as values. Parameters
	 *            will be passed in the same order as they appear in a map so
	 *            you'd probably want to use LinkedHashMap here.
	 * @throws TaskSchedulerException
	 */
	void addEjbTask(String taskName, String taskParams, String jndiName, String className, String methodName,
	        LinkedHashMap<Class<?>, Object> params) throws TaskSchedulerException;

    void addEjbTask(String taskName, String group, String taskParams, String jndiName, String className, String methodName,
                    LinkedHashMap<Class<?>, Object> params) throws TaskSchedulerException;

	/**
	 * Schedule new Quartz job for execution.
	 * 
	 * @param taskName
	 *            task name
	 * @param taskParams
	 *            cron-formatted expression with task schedule
	 * @param clazz
	 *            class implementing quartz Job interface.
	 * @throws TaskSchedulerException
	 */
	void addPlainTask(String taskName, String taskParams, Class<? extends Job> clazz) throws TaskSchedulerException;

	/**
	 * Checks that task with given <tt>taskName</tt> already exists
	 * 
	 * @param taskName
	 *            Name of task to check
	 * @return true - in a case of scheduled task , false - if there is no task
	 *         with such <tt>taskName</tt> in scheduler storage
	 */
	boolean isTaskAlreadyScheduled(String group, String taskName);

	/**
	 * Returns already scheduled tasks as a string
	 * 
	 * @return String filled by all tasks' names
	 */
	String showScheduledTasks();

    Collection<String> getScheduledTasksByGroup(String group) throws TaskSchedulerException;

	/**
	 * Returns ejb job arguments if any
	 * 
	 * @param taskName
	 *            Ejb task name
	 * @param group
	 *            Task's group name
     *
	 * @return Map of arguments assigned to ejb task
	 */
	Map<Class<?>, Object> getEjbTaskParams(String taskName, String group) throws TaskSchedulerException;

    boolean isCronValid(String cron);
}
