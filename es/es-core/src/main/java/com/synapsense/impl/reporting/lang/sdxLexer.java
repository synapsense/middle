// $ANTLR 3.4 D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g 2013-01-17 11:44:24

package com.synapsense.impl.reporting.lang;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked"})
public class sdxLexer extends Lexer {
    public static final int EOF=-1;
    public static final int AMP_QUOTED_ID=4;
    public static final int ASTERISK=5;
    public static final int AXIS=6;
    public static final int COLON=7;
    public static final int COLUMNS=8;
    public static final int COMMA=9;
    public static final int CONCAT=10;
    public static final int DOT=11;
    public static final int EMPTY=12;
    public static final int EQ=13;
    public static final int FUN=14;
    public static final int GE=15;
    public static final int GT=16;
    public static final int ID=17;
    public static final int LBRACE=18;
    public static final int LE=19;
    public static final int LPAREN=20;
    public static final int LT=21;
    public static final int MEMBERS=22;
    public static final int MINUS=23;
    public static final int NE=24;
    public static final int NON=25;
    public static final int NUMBER=26;
    public static final int ON=27;
    public static final int PLUS=28;
    public static final int QUOTE=29;
    public static final int QUOTED_ID=30;
    public static final int RBRACE=31;
    public static final int ROWS=32;
    public static final int RPAREN=33;
    public static final int SELECT=34;
    public static final int SEMICOLON=35;
    public static final int SOLIDUS=36;
    public static final int STRING=37;
    public static final int WHERE=38;
    public static final int WS=39;

      @Override
      public void reportError(RecognitionException e) {
        throw new RuntimeException("I quit!\n" + e, e); 
      }


    // delegates
    // delegators
    public Lexer[] getDelegates() {
        return new Lexer[] {};
    }

    public sdxLexer() {} 
    public sdxLexer(CharStream input) {
        this(input, new RecognizerSharedState());
    }
    public sdxLexer(CharStream input, RecognizerSharedState state) {
        super(input,state);
    }
    public String getGrammarFileName() { return "D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g"; }

    // $ANTLR start "ASTERISK"
    public final void mASTERISK() throws RecognitionException {
        try {
            int _type = ASTERISK;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:17:10: ( '*' )
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:17:12: '*'
            {
            match('*'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "ASTERISK"

    // $ANTLR start "AXIS"
    public final void mAXIS() throws RecognitionException {
        try {
            int _type = AXIS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:18:6: ( 'AXIS' )
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:18:8: 'AXIS'
            {
            match("AXIS"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "AXIS"

    // $ANTLR start "COLUMNS"
    public final void mCOLUMNS() throws RecognitionException {
        try {
            int _type = COLUMNS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:19:9: ( 'COLUMNS' )
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:19:11: 'COLUMNS'
            {
            match("COLUMNS"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "COLUMNS"

    // $ANTLR start "EMPTY"
    public final void mEMPTY() throws RecognitionException {
        try {
            int _type = EMPTY;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:20:7: ( 'EMPTY' )
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:20:9: 'EMPTY'
            {
            match("EMPTY"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "EMPTY"

    // $ANTLR start "MEMBERS"
    public final void mMEMBERS() throws RecognitionException {
        try {
            int _type = MEMBERS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:21:9: ( 'members' )
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:21:11: 'members'
            {
            match("members"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "MEMBERS"

    // $ANTLR start "NON"
    public final void mNON() throws RecognitionException {
        try {
            int _type = NON;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:22:5: ( 'NON' )
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:22:7: 'NON'
            {
            match("NON"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "NON"

    // $ANTLR start "ON"
    public final void mON() throws RecognitionException {
        try {
            int _type = ON;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:23:4: ( 'ON' )
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:23:6: 'ON'
            {
            match("ON"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "ON"

    // $ANTLR start "PLUS"
    public final void mPLUS() throws RecognitionException {
        try {
            int _type = PLUS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:24:6: ( '+' )
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:24:8: '+'
            {
            match('+'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "PLUS"

    // $ANTLR start "ROWS"
    public final void mROWS() throws RecognitionException {
        try {
            int _type = ROWS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:25:6: ( 'ROWS' )
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:25:8: 'ROWS'
            {
            match("ROWS"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "ROWS"

    // $ANTLR start "SELECT"
    public final void mSELECT() throws RecognitionException {
        try {
            int _type = SELECT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:26:8: ( 'SELECT' )
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:26:10: 'SELECT'
            {
            match("SELECT"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "SELECT"

    // $ANTLR start "WHERE"
    public final void mWHERE() throws RecognitionException {
        try {
            int _type = WHERE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:27:7: ( 'WHERE' )
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:27:9: 'WHERE'
            {
            match("WHERE"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "WHERE"

    // $ANTLR start "QUOTE"
    public final void mQUOTE() throws RecognitionException {
        try {
            int _type = QUOTE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:179:9: ( '\\'' )
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:179:11: '\\''
            {
            match('\''); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "QUOTE"

    // $ANTLR start "COLON"
    public final void mCOLON() throws RecognitionException {
        try {
            int _type = COLON;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:181:9: ( ':' )
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:181:11: ':'
            {
            match(':'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "COLON"

    // $ANTLR start "SEMICOLON"
    public final void mSEMICOLON() throws RecognitionException {
        try {
            int _type = SEMICOLON;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:182:12: ( ';' )
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:182:14: ';'
            {
            match(';'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "SEMICOLON"

    // $ANTLR start "COMMA"
    public final void mCOMMA() throws RecognitionException {
        try {
            int _type = COMMA;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:183:9: ( ',' )
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:183:11: ','
            {
            match(','); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "COMMA"

    // $ANTLR start "CONCAT"
    public final void mCONCAT() throws RecognitionException {
        try {
            int _type = CONCAT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:184:10: ( '||' )
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:184:12: '||'
            {
            match("||"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "CONCAT"

    // $ANTLR start "DOT"
    public final void mDOT() throws RecognitionException {
        try {
            int _type = DOT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:185:7: ( '.' )
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:185:9: '.'
            {
            match('.'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "DOT"

    // $ANTLR start "EQ"
    public final void mEQ() throws RecognitionException {
        try {
            int _type = EQ;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:186:7: ( '=' )
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:186:9: '='
            {
            match('='); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "EQ"

    // $ANTLR start "GE"
    public final void mGE() throws RecognitionException {
        try {
            int _type = GE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:187:7: ( '>=' )
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:187:9: '>='
            {
            match(">="); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "GE"

    // $ANTLR start "GT"
    public final void mGT() throws RecognitionException {
        try {
            int _type = GT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:188:7: ( '>' )
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:188:9: '>'
            {
            match('>'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "GT"

    // $ANTLR start "LBRACE"
    public final void mLBRACE() throws RecognitionException {
        try {
            int _type = LBRACE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:189:10: ( '{' )
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:189:12: '{'
            {
            match('{'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "LBRACE"

    // $ANTLR start "LE"
    public final void mLE() throws RecognitionException {
        try {
            int _type = LE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:190:7: ( '<=' )
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:190:9: '<='
            {
            match("<="); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "LE"

    // $ANTLR start "LPAREN"
    public final void mLPAREN() throws RecognitionException {
        try {
            int _type = LPAREN;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:191:10: ( '(' )
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:191:12: '('
            {
            match('('); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "LPAREN"

    // $ANTLR start "LT"
    public final void mLT() throws RecognitionException {
        try {
            int _type = LT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:192:7: ( '<' )
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:192:9: '<'
            {
            match('<'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "LT"

    // $ANTLR start "MINUS"
    public final void mMINUS() throws RecognitionException {
        try {
            int _type = MINUS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:193:9: ( '-' )
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:193:11: '-'
            {
            match('-'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "MINUS"

    // $ANTLR start "NE"
    public final void mNE() throws RecognitionException {
        try {
            int _type = NE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:194:7: ( '<>' )
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:194:9: '<>'
            {
            match("<>"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "NE"

    // $ANTLR start "RBRACE"
    public final void mRBRACE() throws RecognitionException {
        try {
            int _type = RBRACE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:196:10: ( '}' )
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:196:12: '}'
            {
            match('}'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "RBRACE"

    // $ANTLR start "RPAREN"
    public final void mRPAREN() throws RecognitionException {
        try {
            int _type = RPAREN;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:197:10: ( ')' )
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:197:12: ')'
            {
            match(')'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "RPAREN"

    // $ANTLR start "SOLIDUS"
    public final void mSOLIDUS() throws RecognitionException {
        try {
            int _type = SOLIDUS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:198:10: ( '/' )
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:198:12: '/'
            {
            match('/'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "SOLIDUS"

    // $ANTLR start "FUN"
    public final void mFUN() throws RecognitionException {
        try {
            int _type = FUN;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:200:6: ( 'AVG' | 'MAX' | 'MIN' | 'COUNT' | 'SUM' )
            int alt1=5;
            switch ( input.LA(1) ) {
            case 'A':
                {
                alt1=1;
                }
                break;
            case 'M':
                {
                int LA1_2 = input.LA(2);

                if ( (LA1_2=='A') ) {
                    alt1=2;
                }
                else if ( (LA1_2=='I') ) {
                    alt1=3;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 1, 2, input);

                    throw nvae;

                }
                }
                break;
            case 'C':
                {
                alt1=4;
                }
                break;
            case 'S':
                {
                alt1=5;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;

            }

            switch (alt1) {
                case 1 :
                    // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:200:8: 'AVG'
                    {
                    match("AVG"); 



                    }
                    break;
                case 2 :
                    // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:200:16: 'MAX'
                    {
                    match("MAX"); 



                    }
                    break;
                case 3 :
                    // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:200:24: 'MIN'
                    {
                    match("MIN"); 



                    }
                    break;
                case 4 :
                    // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:200:32: 'COUNT'
                    {
                    match("COUNT"); 



                    }
                    break;
                case 5 :
                    // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:200:42: 'SUM'
                    {
                    match("SUM"); 



                    }
                    break;

            }
            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "FUN"

    // $ANTLR start "NUMBER"
    public final void mNUMBER() throws RecognitionException {
        try {
            int _type = NUMBER;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:202:10: ( ( '0' .. '9' )+ )
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:202:12: ( '0' .. '9' )+
            {
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:202:12: ( '0' .. '9' )+
            int cnt2=0;
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( ((LA2_0 >= '0' && LA2_0 <= '9')) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:
            	    {
            	    if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
            	        input.consume();
            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt2 >= 1 ) break loop2;
                        EarlyExitException eee =
                            new EarlyExitException(2, input);
                        throw eee;
                }
                cnt2++;
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "NUMBER"

    // $ANTLR start "ID"
    public final void mID() throws RecognitionException {
        try {
            int _type = ID;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:204:5: ( ( 'a' .. 'z' | 'A' .. 'Z' | '_' | '$' ) ( 'a' .. 'z' | 'A' .. 'Z' | '_' | '0' .. '9' | '$' )* )
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:204:8: ( 'a' .. 'z' | 'A' .. 'Z' | '_' | '$' ) ( 'a' .. 'z' | 'A' .. 'Z' | '_' | '0' .. '9' | '$' )*
            {
            if ( input.LA(1)=='$'||(input.LA(1) >= 'A' && input.LA(1) <= 'Z')||input.LA(1)=='_'||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:204:36: ( 'a' .. 'z' | 'A' .. 'Z' | '_' | '0' .. '9' | '$' )*
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( (LA3_0=='$'||(LA3_0 >= '0' && LA3_0 <= '9')||(LA3_0 >= 'A' && LA3_0 <= 'Z')||LA3_0=='_'||(LA3_0 >= 'a' && LA3_0 <= 'z')) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:
            	    {
            	    if ( input.LA(1)=='$'||(input.LA(1) >= '0' && input.LA(1) <= '9')||(input.LA(1) >= 'A' && input.LA(1) <= 'Z')||input.LA(1)=='_'||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
            	        input.consume();
            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "ID"

    // $ANTLR start "AMP_QUOTED_ID"
    public final void mAMP_QUOTED_ID() throws RecognitionException {
        try {
            int _type = AMP_QUOTED_ID;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:207:14: ( '[&' ( ID ( ( ' ' | '\\t' )+ ID )* | NUMBER ) ']' )
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:207:16: '[&' ( ID ( ( ' ' | '\\t' )+ ID )* | NUMBER ) ']'
            {
            match("[&"); 



            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:207:21: ( ID ( ( ' ' | '\\t' )+ ID )* | NUMBER )
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0=='$'||(LA6_0 >= 'A' && LA6_0 <= 'Z')||LA6_0=='_'||(LA6_0 >= 'a' && LA6_0 <= 'z')) ) {
                alt6=1;
            }
            else if ( ((LA6_0 >= '0' && LA6_0 <= '9')) ) {
                alt6=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 6, 0, input);

                throw nvae;

            }
            switch (alt6) {
                case 1 :
                    // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:207:22: ID ( ( ' ' | '\\t' )+ ID )*
                    {
                    mID(); 


                    // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:207:25: ( ( ' ' | '\\t' )+ ID )*
                    loop5:
                    do {
                        int alt5=2;
                        int LA5_0 = input.LA(1);

                        if ( (LA5_0=='\t'||LA5_0==' ') ) {
                            alt5=1;
                        }


                        switch (alt5) {
                    	case 1 :
                    	    // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:207:26: ( ' ' | '\\t' )+ ID
                    	    {
                    	    // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:207:26: ( ' ' | '\\t' )+
                    	    int cnt4=0;
                    	    loop4:
                    	    do {
                    	        int alt4=2;
                    	        int LA4_0 = input.LA(1);

                    	        if ( (LA4_0=='\t'||LA4_0==' ') ) {
                    	            alt4=1;
                    	        }


                    	        switch (alt4) {
                    	    	case 1 :
                    	    	    // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:
                    	    	    {
                    	    	    if ( input.LA(1)=='\t'||input.LA(1)==' ' ) {
                    	    	        input.consume();
                    	    	    }
                    	    	    else {
                    	    	        MismatchedSetException mse = new MismatchedSetException(null,input);
                    	    	        recover(mse);
                    	    	        throw mse;
                    	    	    }


                    	    	    }
                    	    	    break;

                    	    	default :
                    	    	    if ( cnt4 >= 1 ) break loop4;
                    	                EarlyExitException eee =
                    	                    new EarlyExitException(4, input);
                    	                throw eee;
                    	        }
                    	        cnt4++;
                    	    } while (true);


                    	    mID(); 


                    	    }
                    	    break;

                    	default :
                    	    break loop5;
                        }
                    } while (true);


                    }
                    break;
                case 2 :
                    // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:207:47: NUMBER
                    {
                    mNUMBER(); 


                    }
                    break;

            }


            match(']'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "AMP_QUOTED_ID"

    // $ANTLR start "QUOTED_ID"
    public final void mQUOTED_ID() throws RecognitionException {
        try {
            int _type = QUOTED_ID;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:210:10: ( ( '[' ( ID ( ( ' ' | '\\t' )+ ID )* | NUMBER ) ']' ) )
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:210:12: ( '[' ( ID ( ( ' ' | '\\t' )+ ID )* | NUMBER ) ']' )
            {
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:210:12: ( '[' ( ID ( ( ' ' | '\\t' )+ ID )* | NUMBER ) ']' )
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:210:13: '[' ( ID ( ( ' ' | '\\t' )+ ID )* | NUMBER ) ']'
            {
            match('['); 

            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:210:17: ( ID ( ( ' ' | '\\t' )+ ID )* | NUMBER )
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0=='$'||(LA9_0 >= 'A' && LA9_0 <= 'Z')||LA9_0=='_'||(LA9_0 >= 'a' && LA9_0 <= 'z')) ) {
                alt9=1;
            }
            else if ( ((LA9_0 >= '0' && LA9_0 <= '9')) ) {
                alt9=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 9, 0, input);

                throw nvae;

            }
            switch (alt9) {
                case 1 :
                    // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:210:18: ID ( ( ' ' | '\\t' )+ ID )*
                    {
                    mID(); 


                    // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:210:21: ( ( ' ' | '\\t' )+ ID )*
                    loop8:
                    do {
                        int alt8=2;
                        int LA8_0 = input.LA(1);

                        if ( (LA8_0=='\t'||LA8_0==' ') ) {
                            alt8=1;
                        }


                        switch (alt8) {
                    	case 1 :
                    	    // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:210:22: ( ' ' | '\\t' )+ ID
                    	    {
                    	    // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:210:22: ( ' ' | '\\t' )+
                    	    int cnt7=0;
                    	    loop7:
                    	    do {
                    	        int alt7=2;
                    	        int LA7_0 = input.LA(1);

                    	        if ( (LA7_0=='\t'||LA7_0==' ') ) {
                    	            alt7=1;
                    	        }


                    	        switch (alt7) {
                    	    	case 1 :
                    	    	    // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:
                    	    	    {
                    	    	    if ( input.LA(1)=='\t'||input.LA(1)==' ' ) {
                    	    	        input.consume();
                    	    	    }
                    	    	    else {
                    	    	        MismatchedSetException mse = new MismatchedSetException(null,input);
                    	    	        recover(mse);
                    	    	        throw mse;
                    	    	    }


                    	    	    }
                    	    	    break;

                    	    	default :
                    	    	    if ( cnt7 >= 1 ) break loop7;
                    	                EarlyExitException eee =
                    	                    new EarlyExitException(7, input);
                    	                throw eee;
                    	        }
                    	        cnt7++;
                    	    } while (true);


                    	    mID(); 


                    	    }
                    	    break;

                    	default :
                    	    break loop8;
                        }
                    } while (true);


                    }
                    break;
                case 2 :
                    // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:210:43: NUMBER
                    {
                    mNUMBER(); 


                    }
                    break;

            }


            match(']'); 

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "QUOTED_ID"

    // $ANTLR start "STRING"
    public final void mSTRING() throws RecognitionException {
        try {
            int _type = STRING;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:213:9: ( '\"' (~ '\"' )* '\"' | '\\'' (~ '\\'' )* '\\'' )
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( (LA12_0=='\"') ) {
                alt12=1;
            }
            else if ( (LA12_0=='\'') ) {
                alt12=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 12, 0, input);

                throw nvae;

            }
            switch (alt12) {
                case 1 :
                    // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:213:11: '\"' (~ '\"' )* '\"'
                    {
                    match('\"'); 

                    // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:213:15: (~ '\"' )*
                    loop10:
                    do {
                        int alt10=2;
                        int LA10_0 = input.LA(1);

                        if ( ((LA10_0 >= '\u0000' && LA10_0 <= '!')||(LA10_0 >= '#' && LA10_0 <= '\uFFFF')) ) {
                            alt10=1;
                        }


                        switch (alt10) {
                    	case 1 :
                    	    // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:
                    	    {
                    	    if ( (input.LA(1) >= '\u0000' && input.LA(1) <= '!')||(input.LA(1) >= '#' && input.LA(1) <= '\uFFFF') ) {
                    	        input.consume();
                    	    }
                    	    else {
                    	        MismatchedSetException mse = new MismatchedSetException(null,input);
                    	        recover(mse);
                    	        throw mse;
                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop10;
                        }
                    } while (true);


                    match('\"'); 

                    }
                    break;
                case 2 :
                    // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:214:5: '\\'' (~ '\\'' )* '\\''
                    {
                    match('\''); 

                    // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:214:10: (~ '\\'' )*
                    loop11:
                    do {
                        int alt11=2;
                        int LA11_0 = input.LA(1);

                        if ( ((LA11_0 >= '\u0000' && LA11_0 <= '&')||(LA11_0 >= '(' && LA11_0 <= '\uFFFF')) ) {
                            alt11=1;
                        }


                        switch (alt11) {
                    	case 1 :
                    	    // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:
                    	    {
                    	    if ( (input.LA(1) >= '\u0000' && input.LA(1) <= '&')||(input.LA(1) >= '(' && input.LA(1) <= '\uFFFF') ) {
                    	        input.consume();
                    	    }
                    	    else {
                    	        MismatchedSetException mse = new MismatchedSetException(null,input);
                    	        recover(mse);
                    	        throw mse;
                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop11;
                        }
                    } while (true);


                    match('\''); 

                    }
                    break;

            }
            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "STRING"

    // $ANTLR start "WS"
    public final void mWS() throws RecognitionException {
        try {
            int _type = WS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:217:5: ( ( ' ' | '\\t' | '\\r' | '\\f' | '\\n' )+ )
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:217:9: ( ' ' | '\\t' | '\\r' | '\\f' | '\\n' )+
            {
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:217:9: ( ' ' | '\\t' | '\\r' | '\\f' | '\\n' )+
            int cnt13=0;
            loop13:
            do {
                int alt13=2;
                int LA13_0 = input.LA(1);

                if ( ((LA13_0 >= '\t' && LA13_0 <= '\n')||(LA13_0 >= '\f' && LA13_0 <= '\r')||LA13_0==' ') ) {
                    alt13=1;
                }


                switch (alt13) {
            	case 1 :
            	    // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:
            	    {
            	    if ( (input.LA(1) >= '\t' && input.LA(1) <= '\n')||(input.LA(1) >= '\f' && input.LA(1) <= '\r')||input.LA(1)==' ' ) {
            	        input.consume();
            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt13 >= 1 ) break loop13;
                        EarlyExitException eee =
                            new EarlyExitException(13, input);
                        throw eee;
                }
                cnt13++;
            } while (true);


             _channel=HIDDEN; 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "WS"

    public void mTokens() throws RecognitionException {
        // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:1:8: ( ASTERISK | AXIS | COLUMNS | EMPTY | MEMBERS | NON | ON | PLUS | ROWS | SELECT | WHERE | QUOTE | COLON | SEMICOLON | COMMA | CONCAT | DOT | EQ | GE | GT | LBRACE | LE | LPAREN | LT | MINUS | NE | RBRACE | RPAREN | SOLIDUS | FUN | NUMBER | ID | AMP_QUOTED_ID | QUOTED_ID | STRING | WS )
        int alt14=36;
        alt14 = dfa14.predict(input);
        switch (alt14) {
            case 1 :
                // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:1:10: ASTERISK
                {
                mASTERISK(); 


                }
                break;
            case 2 :
                // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:1:19: AXIS
                {
                mAXIS(); 


                }
                break;
            case 3 :
                // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:1:24: COLUMNS
                {
                mCOLUMNS(); 


                }
                break;
            case 4 :
                // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:1:32: EMPTY
                {
                mEMPTY(); 


                }
                break;
            case 5 :
                // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:1:38: MEMBERS
                {
                mMEMBERS(); 


                }
                break;
            case 6 :
                // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:1:46: NON
                {
                mNON(); 


                }
                break;
            case 7 :
                // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:1:50: ON
                {
                mON(); 


                }
                break;
            case 8 :
                // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:1:53: PLUS
                {
                mPLUS(); 


                }
                break;
            case 9 :
                // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:1:58: ROWS
                {
                mROWS(); 


                }
                break;
            case 10 :
                // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:1:63: SELECT
                {
                mSELECT(); 


                }
                break;
            case 11 :
                // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:1:70: WHERE
                {
                mWHERE(); 


                }
                break;
            case 12 :
                // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:1:76: QUOTE
                {
                mQUOTE(); 


                }
                break;
            case 13 :
                // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:1:82: COLON
                {
                mCOLON(); 


                }
                break;
            case 14 :
                // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:1:88: SEMICOLON
                {
                mSEMICOLON(); 


                }
                break;
            case 15 :
                // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:1:98: COMMA
                {
                mCOMMA(); 


                }
                break;
            case 16 :
                // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:1:104: CONCAT
                {
                mCONCAT(); 


                }
                break;
            case 17 :
                // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:1:111: DOT
                {
                mDOT(); 


                }
                break;
            case 18 :
                // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:1:115: EQ
                {
                mEQ(); 


                }
                break;
            case 19 :
                // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:1:118: GE
                {
                mGE(); 


                }
                break;
            case 20 :
                // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:1:121: GT
                {
                mGT(); 


                }
                break;
            case 21 :
                // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:1:124: LBRACE
                {
                mLBRACE(); 


                }
                break;
            case 22 :
                // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:1:131: LE
                {
                mLE(); 


                }
                break;
            case 23 :
                // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:1:134: LPAREN
                {
                mLPAREN(); 


                }
                break;
            case 24 :
                // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:1:141: LT
                {
                mLT(); 


                }
                break;
            case 25 :
                // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:1:144: MINUS
                {
                mMINUS(); 


                }
                break;
            case 26 :
                // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:1:150: NE
                {
                mNE(); 


                }
                break;
            case 27 :
                // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:1:153: RBRACE
                {
                mRBRACE(); 


                }
                break;
            case 28 :
                // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:1:160: RPAREN
                {
                mRPAREN(); 


                }
                break;
            case 29 :
                // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:1:167: SOLIDUS
                {
                mSOLIDUS(); 


                }
                break;
            case 30 :
                // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:1:175: FUN
                {
                mFUN(); 


                }
                break;
            case 31 :
                // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:1:179: NUMBER
                {
                mNUMBER(); 


                }
                break;
            case 32 :
                // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:1:186: ID
                {
                mID(); 


                }
                break;
            case 33 :
                // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:1:189: AMP_QUOTED_ID
                {
                mAMP_QUOTED_ID(); 


                }
                break;
            case 34 :
                // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:1:203: QUOTED_ID
                {
                mQUOTED_ID(); 


                }
                break;
            case 35 :
                // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:1:213: STRING
                {
                mSTRING(); 


                }
                break;
            case 36 :
                // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:1:220: WS
                {
                mWS(); 


                }
                break;

        }

    }


    protected DFA14 dfa14 = new DFA14(this);
    static final String DFA14_eotS =
        "\2\uffff\6\35\1\uffff\3\35\1\54\6\uffff\1\56\1\uffff\1\61\5\uffff"+
        "\1\35\5\uffff\6\35\1\75\4\35\6\uffff\2\35\2\uffff\1\35\1\105\4\35"+
        "\1\112\1\uffff\2\35\1\105\1\35\2\105\1\116\1\uffff\4\35\1\uffff"+
        "\1\123\2\35\1\uffff\1\35\1\105\1\127\1\35\1\uffff\1\35\1\132\1\35"+
        "\1\uffff\1\35\1\135\1\uffff\1\136\1\137\3\uffff";
    static final String DFA14_eofS =
        "\140\uffff";
    static final String DFA14_minS =
        "\1\11\1\uffff\1\126\1\117\1\115\1\145\1\117\1\116\1\uffff\1\117"+
        "\1\105\1\110\1\0\6\uffff\1\75\1\uffff\1\75\5\uffff\1\101\2\uffff"+
        "\1\44\2\uffff\1\111\1\107\1\114\1\120\1\155\1\116\1\44\1\127\1\114"+
        "\1\115\1\105\6\uffff\1\130\1\116\2\uffff\1\123\1\44\1\125\1\116"+
        "\1\124\1\142\1\44\1\uffff\1\123\1\105\1\44\1\122\3\44\1\uffff\1"+
        "\115\1\124\1\131\1\145\1\uffff\1\44\1\103\1\105\1\uffff\1\116\2"+
        "\44\1\162\1\uffff\1\124\1\44\1\123\1\uffff\1\163\1\44\1\uffff\2"+
        "\44\3\uffff";
    static final String DFA14_maxS =
        "\1\175\1\uffff\1\130\1\117\1\115\1\145\1\117\1\116\1\uffff\1\117"+
        "\1\125\1\110\1\uffff\6\uffff\1\75\1\uffff\1\76\5\uffff\1\111\2\uffff"+
        "\1\172\2\uffff\1\111\1\107\1\125\1\120\1\155\1\116\1\172\1\127\1"+
        "\114\1\115\1\105\6\uffff\1\130\1\116\2\uffff\1\123\1\172\1\125\1"+
        "\116\1\124\1\142\1\172\1\uffff\1\123\1\105\1\172\1\122\3\172\1\uffff"+
        "\1\115\1\124\1\131\1\145\1\uffff\1\172\1\103\1\105\1\uffff\1\116"+
        "\2\172\1\162\1\uffff\1\124\1\172\1\123\1\uffff\1\163\1\172\1\uffff"+
        "\2\172\3\uffff";
    static final String DFA14_acceptS =
        "\1\uffff\1\1\6\uffff\1\10\4\uffff\1\15\1\16\1\17\1\20\1\21\1\22"+
        "\1\uffff\1\25\1\uffff\1\27\1\31\1\33\1\34\1\35\1\uffff\1\37\1\40"+
        "\1\uffff\1\43\1\44\13\uffff\1\14\1\23\1\24\1\26\1\32\1\30\2\uffff"+
        "\1\41\1\42\7\uffff\1\7\7\uffff\1\36\4\uffff\1\6\3\uffff\1\2\4\uffff"+
        "\1\11\3\uffff\1\4\2\uffff\1\13\2\uffff\1\12\1\3\1\5";
    static final String DFA14_specialS =
        "\14\uffff\1\0\123\uffff}>";
    static final String[] DFA14_transitionS = {
            "\2\40\1\uffff\2\40\22\uffff\1\40\1\uffff\1\37\1\uffff\1\35\2"+
            "\uffff\1\14\1\26\1\31\1\1\1\10\1\17\1\27\1\21\1\32\12\34\1\15"+
            "\1\16\1\25\1\22\1\23\2\uffff\1\2\1\35\1\3\1\35\1\4\7\35\1\33"+
            "\1\6\1\7\2\35\1\11\1\12\3\35\1\13\3\35\1\36\3\uffff\1\35\1\uffff"+
            "\14\35\1\5\15\35\1\24\1\20\1\30",
            "",
            "\1\42\1\uffff\1\41",
            "\1\43",
            "\1\44",
            "\1\45",
            "\1\46",
            "\1\47",
            "",
            "\1\50",
            "\1\51\17\uffff\1\52",
            "\1\53",
            "\0\37",
            "",
            "",
            "",
            "",
            "",
            "",
            "\1\55",
            "",
            "\1\57\1\60",
            "",
            "",
            "",
            "",
            "",
            "\1\62\7\uffff\1\63",
            "",
            "",
            "\1\65\1\uffff\1\64\11\uffff\12\65\7\uffff\32\65\4\uffff\1\65"+
            "\1\uffff\32\65",
            "",
            "",
            "\1\66",
            "\1\67",
            "\1\70\10\uffff\1\71",
            "\1\72",
            "\1\73",
            "\1\74",
            "\1\35\13\uffff\12\35\7\uffff\32\35\4\uffff\1\35\1\uffff\32"+
            "\35",
            "\1\76",
            "\1\77",
            "\1\100",
            "\1\101",
            "",
            "",
            "",
            "",
            "",
            "",
            "\1\102",
            "\1\103",
            "",
            "",
            "\1\104",
            "\1\35\13\uffff\12\35\7\uffff\32\35\4\uffff\1\35\1\uffff\32"+
            "\35",
            "\1\106",
            "\1\107",
            "\1\110",
            "\1\111",
            "\1\35\13\uffff\12\35\7\uffff\32\35\4\uffff\1\35\1\uffff\32"+
            "\35",
            "",
            "\1\113",
            "\1\114",
            "\1\35\13\uffff\12\35\7\uffff\32\35\4\uffff\1\35\1\uffff\32"+
            "\35",
            "\1\115",
            "\1\35\13\uffff\12\35\7\uffff\32\35\4\uffff\1\35\1\uffff\32"+
            "\35",
            "\1\35\13\uffff\12\35\7\uffff\32\35\4\uffff\1\35\1\uffff\32"+
            "\35",
            "\1\35\13\uffff\12\35\7\uffff\32\35\4\uffff\1\35\1\uffff\32"+
            "\35",
            "",
            "\1\117",
            "\1\120",
            "\1\121",
            "\1\122",
            "",
            "\1\35\13\uffff\12\35\7\uffff\32\35\4\uffff\1\35\1\uffff\32"+
            "\35",
            "\1\124",
            "\1\125",
            "",
            "\1\126",
            "\1\35\13\uffff\12\35\7\uffff\32\35\4\uffff\1\35\1\uffff\32"+
            "\35",
            "\1\35\13\uffff\12\35\7\uffff\32\35\4\uffff\1\35\1\uffff\32"+
            "\35",
            "\1\130",
            "",
            "\1\131",
            "\1\35\13\uffff\12\35\7\uffff\32\35\4\uffff\1\35\1\uffff\32"+
            "\35",
            "\1\133",
            "",
            "\1\134",
            "\1\35\13\uffff\12\35\7\uffff\32\35\4\uffff\1\35\1\uffff\32"+
            "\35",
            "",
            "\1\35\13\uffff\12\35\7\uffff\32\35\4\uffff\1\35\1\uffff\32"+
            "\35",
            "\1\35\13\uffff\12\35\7\uffff\32\35\4\uffff\1\35\1\uffff\32"+
            "\35",
            "",
            "",
            ""
    };

    static final short[] DFA14_eot = DFA.unpackEncodedString(DFA14_eotS);
    static final short[] DFA14_eof = DFA.unpackEncodedString(DFA14_eofS);
    static final char[] DFA14_min = DFA.unpackEncodedStringToUnsignedChars(DFA14_minS);
    static final char[] DFA14_max = DFA.unpackEncodedStringToUnsignedChars(DFA14_maxS);
    static final short[] DFA14_accept = DFA.unpackEncodedString(DFA14_acceptS);
    static final short[] DFA14_special = DFA.unpackEncodedString(DFA14_specialS);
    static final short[][] DFA14_transition;

    static {
        int numStates = DFA14_transitionS.length;
        DFA14_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA14_transition[i] = DFA.unpackEncodedString(DFA14_transitionS[i]);
        }
    }

    class DFA14 extends DFA {

        public DFA14(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 14;
            this.eot = DFA14_eot;
            this.eof = DFA14_eof;
            this.min = DFA14_min;
            this.max = DFA14_max;
            this.accept = DFA14_accept;
            this.special = DFA14_special;
            this.transition = DFA14_transition;
        }
        public String getDescription() {
            return "1:1: Tokens : ( ASTERISK | AXIS | COLUMNS | EMPTY | MEMBERS | NON | ON | PLUS | ROWS | SELECT | WHERE | QUOTE | COLON | SEMICOLON | COMMA | CONCAT | DOT | EQ | GE | GT | LBRACE | LE | LPAREN | LT | MINUS | NE | RBRACE | RPAREN | SOLIDUS | FUN | NUMBER | ID | AMP_QUOTED_ID | QUOTED_ID | STRING | WS );";
        }
        public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
            IntStream input = _input;
        	int _s = s;
            switch ( s ) {
                    case 0 : 
                        int LA14_12 = input.LA(1);

                        s = -1;
                        if ( ((LA14_12 >= '\u0000' && LA14_12 <= '\uFFFF')) ) {s = 31;}

                        else s = 44;

                        if ( s>=0 ) return s;
                        break;
            }
            NoViableAltException nvae =
                new NoViableAltException(getDescription(), 14, _s, input);
            error(nvae);
            throw nvae;
        }

    }
 

}