package com.synapsense.impl.alerting.macros;

import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TimestampMacro extends AbstractPatternMacro {

	private static final Pattern pattern = Pattern.compile("\\$(?i)timestamp\\$");

	@Override
	public String expandIn(String input, MacroContext ctx) {
		if (input == null)
			return null;
		if (ctx.alert == null)
			throw new IllegalArgumentException("MacroContext.alert must be not null");
		if (ctx.usrCtx == null)
			throw new IllegalArgumentException("MacroContext.userCtx must be not null");
		return super.expandIn(input, ctx);
	}

	@Override
	protected String expand(Matcher m, MacroContext ctx) {
		Long timestamp = ctx.alert.getAlertTime();
		if (timestamp == null)
			return Macro.DEFAULT_EXPANSION;
		return Matcher.quoteReplacement(ctx.usrCtx.getDateFormat().format(new Date(timestamp)));
	}

	@Override
	protected Pattern getPattern() {
		return pattern;
	}

}
