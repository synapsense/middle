package com.synapsense.impl.reporting.tables;

import com.google.common.collect.LinkedListMultimap;
import com.google.common.collect.ListMultimap;
import com.synapsense.dto.PropertyDescr;
import com.synapsense.dto.TO;
import com.synapsense.impl.reporting.engine.ReportUserInfo;
import com.synapsense.service.reporting.Formula;
import com.synapsense.service.reporting.Formula.TransformType;
import com.synapsense.service.reporting.SortingProperty;
import com.synapsense.service.reporting.dto.CellTO;
import com.synapsense.service.reporting.dto.RowTO;
import com.synapsense.service.reporting.dto.TableTO;
import com.synapsense.service.reporting.olap.Filter;
import com.synapsense.service.reporting.olap.custom.Cube;
import com.synapsense.service.reporting.olap.custom.EnvironmentDataSource;
import com.synapsense.util.CollectionUtils;

import java.util.*;

import static com.synapsense.service.reporting.Formula.TransformType.*;

public class ReportTableTransformer {
	
	private static final int ROW = 0;
	private static final int COLUMN = 1;
	private static final String MAX = "max";
	private static final String MIN = "min";
	private static final String AVG = "avg";
	public static final int ROW_HEADER = 0;
	public static final int COLUMN_HEADER = 1;
	public static final int SLICER_AXIS_NUMBER = 2;
	protected final static String TIMESTAMP = "Timestamp";

	private TableTO table;
    private ListMultimap<TO<?>, RowTO> groups = LinkedListMultimap.create();
	
	public static Map<TransformType, TableTransformer> transformerMap = CollectionUtils.newMap();
	
	static {
		transformerMap.put(ORIGINAL, new OriginalTableTransformer());
		transformerMap.put(RAW, new RawTableTransformer());
		transformerMap.put(COLUMN_10, new VerticalCutTableTransformer());
		transformerMap.put(ROW_1, new HorizontalCutTableTransformer());
		transformerMap.put(SORT, new SortTableTransformer());
        transformerMap.put(FILTER, new FilterTableTransformer());
        transformerMap.put(GROUP, new GroupTableTransformer());
	}

    public static class FilterTableTransformer implements TableTransformer{

        @Override
        public TableTO getTable(TableTO table, Formula formula, EnvironmentDataSource dataSource, ReportUserInfo reportUserInfo, ListMultimap<TO<?>, RowTO> groups) {

            List<RowTO> tableRows = CollectionUtils.newArrayList(table.getHeight());
            for (RowTO row : table) {
                TO<?> object = (TO<?>) row.getFieldValue("name", Cube.DIM_OBJECT);
                List<TO<?>> objects = CollectionUtils.newList();
                objects.add(object);
                boolean accepted = false;
                for (Filter filter : formula.getFilters().getFilters()){
                    accepted = accepted || accept(row, filter);
                }
                if (accepted) {
                    tableRows.add(row);
                }
            }
            return composeTable(table, tableRows);
            
        }

        private boolean accept(RowTO row, Filter filter) {
        	String fullPropertyName = filter.getProperties().iterator().next();
            Object value = row.getFieldValue(fullPropertyName, Cube.DIM_PROPERTY);
            HashMap<String, Object> map = new HashMap<>();
            map.put(fullPropertyName, value);
            return filter.accept(map);
        }

    }

    public static class GroupTableTransformer implements TableTransformer{

        @Override
        public TableTO getTable(TableTO table, Formula formula, EnvironmentDataSource dataSource, ReportUserInfo reportUserInfo, ListMultimap<TO<?>, RowTO> groups) {

            List<RowTO> tableRows = CollectionUtils.newArrayList(table.getHeight());

            for (RowTO row : table) {
                TO<?> object = (TO<?>) row.getFieldValue("name", Cube.DIM_OBJECT);
                TO<?> parent;
              	parent = dataSource.getRelatedObjects(object, formula.getGroupObjectType(), false).iterator().next();

                groups.put(parent, row);

            }

            for (Collection group : groups.asMap().values()) {
                tableRows.addAll(group);
            }
            return composeTable(table, tableRows);

        }

    }

	public static class SortTableTransformer implements TableTransformer{

		@Override
		public TableTO getTable(TableTO table, Formula formula, EnvironmentDataSource dataSource, ReportUserInfo reportUserInfo, ListMultimap<TO<?>, RowTO> groups) {

            List<RowTO> tableRows = CollectionUtils.newArrayList(table.getHeight());
            //sorting by groups
            if (!groups.isEmpty()) {
                for (Collection<RowTO> group : groups.asMap().values()) {
                    List<RowTO> groupRows = CollectionUtils.newArrayList(table.getHeight());
                    for (RowTO row : group) {
                        preSortRow(row, formula, dataSource);
                        groupRows.add(row);
                    }
                    Collections.sort(groupRows);
                    tableRows.addAll(groupRows);
                }
            } else {
                for (RowTO row : table) {
                    preSortRow(row, formula, dataSource);
                    tableRows.add(row);
                }
                Collections.sort(tableRows);
            }


            return composeTable(table, tableRows);
		}

        private void preSortRow(RowTO row, Formula formula, EnvironmentDataSource dataSource) {
            TO<?> object = (TO<?>)row.getFieldValue("name", Cube.DIM_OBJECT);
            List<TO<?>> objects = CollectionUtils.newList();
            objects.add(object);

            List<SortingProperty> originalSortingProperties = CollectionUtils.newList(formula.getSortingProperties());
            List<SortingProperty> resultSortingProperties = CollectionUtils.newList();
            for (SortingProperty sortingProperty : originalSortingProperties) {
                String[] propertyArray = sortingProperty.getPropertyName().split("\\.");
                Comparable value = null;
                if (propertyArray.length == 2) {
                    value = (Comparable) this.getValue(objects, propertyArray[1], dataSource);
                } else if(propertyArray.length == 1) {
                    value = (Comparable) this.getValue(objects, propertyArray[0], dataSource);
                } else {
                    value = (Comparable) row.getFieldValue(sortingProperty.getPropertyName(), Cube.DIM_PROPERTY);
                }
                resultSortingProperties.add(new SortingProperty(sortingProperty).setValue(value));
            }
            row.setSortingPropertyValues(resultSortingProperties);
        }
		
		private Object getValue(List<TO<?>> objects, String propertyName, EnvironmentDataSource dataSource) {

            TO<?> valueObject = null;
			String valuePropertyName = null;
			PropertyDescr propDesc = dataSource.getPropertyDescriptor(objects.iterator().next().getTypeName(), propertyName);
			if (propDesc != null && "com.synapsense.dto.TO".equals(propDesc.getTypeName())) {
                valueObject = (TO<?>) dataSource.getPropertyValue(objects, new String[]{propertyName})
                        .iterator().next().getSinglePropValue(propertyName).getValue();
				valuePropertyName = "lastValue";
			} else {
				valueObject = objects.iterator().next();
				valuePropertyName = propertyName;
			}
            // link can be null
            if (valueObject == null) return null;
            List<TO<?>> valueObjects = CollectionUtils.newList();
            valueObjects.add(valueObject);
            Object value = dataSource.getPropertyValue(valueObjects, new String[]{valuePropertyName})
                    .iterator().next().getSinglePropValue(valuePropertyName).getValue();
            return value;
        }
		
	}

	
	public static class OriginalTableTransformer implements TableTransformer{

		@Override
		public TableTO getTable(TableTO table, Formula formula, EnvironmentDataSource dataSource, ReportUserInfo reportUserInfo, ListMultimap<TO<?>, RowTO> groups) {
			return table;
		}
		
	}
	
	public static class RawTableTransformer implements TableTransformer{

		@Override
		public TableTO getTable(TableTO table, Formula formula, EnvironmentDataSource dataSource, ReportUserInfo reportUserInfo, ListMultimap<TO<?>, RowTO> groups) {
			List<RowTO> topRows = CollectionUtils.newList();
			for (RowTO reportRow : table) {
				List<Long> checkedList = CollectionUtils.newList();
				for (Object object : List.class.cast(reportRow.getFieldValue(TIMESTAMP, Cube.DIM_TIME))) {
			    	checkedList.add(Long.class.cast(object));
			    }
			    if (checkedList.isEmpty()) {
			    	continue;
			    }
			    topRows.add(reportRow.getRow());
			}
			return composeTable(table, topRows);
		}

	}
	
	public static class HorizontalCutTableTransformer implements TableTransformer{

		@Override
		public TableTO getTable(TableTO table, Formula formula, EnvironmentDataSource dataSource, ReportUserInfo reportUserInfo, ListMultimap<TO<?>, RowTO> groups) {
			int size = ROW_1.getSize();
            List<RowTO> rows = CollectionUtils.newList();
            int rowIndex = 0;
            for (RowTO reportRow : table) {
                if(rowIndex++ < size) {
                    rows.add(reportRow);
                } else {
                    break;
                }
            }
            composeTable(table, rows);
			return table; 
		}
		
	}
		
	public static class VerticalCutTableTransformer implements TableTransformer{

		@Override
		public TableTO getTable(TableTO table, Formula formula, EnvironmentDataSource dataSource, ReportUserInfo reportUserInfo, ListMultimap<TO<?>, RowTO> groups) {
			List<RowTO> topRows = CollectionUtils.newList();
			for (RowTO reportRow : table) {
				List<CellTO> cells = CollectionUtils.newList();
				for (CellTO cell : reportRow) {
					cells.add(cell);
				}
				List<CellTO> rowTopCells = getTop(cells, formula);
				//Need to add empty value cell for getting timestamp for row
				if (!cells.isEmpty() && rowTopCells.isEmpty()) {
					rowTopCells.add(cells.iterator().next());
				}
				topRows.add(new RowTO(rowTopCells));
			}
			return composeTable(table, topRows);
		}
		
		private List<CellTO> getTop(List<CellTO> rowCells, Formula formula) {
			int size = COLUMN_10.getSize();
			String aggregationType = formula.getAggregationType();
			List<CellTO> topElements = CollectionUtils.newList();
			PriorityQueue<CellTO> heap = new PriorityQueue<CellTO>(size);
			for (CellTO item : rowCells) {
				if (item.getValue() == null) {
					continue;
				}
				if (heap.size() < size
						|| (aggregationType.equals(MIN)
						? (Double.compare(item.getValue().doubleValue(), heap.peek().getValue().doubleValue()) <= 0)
								: (Double.compare(item.getValue().doubleValue(), heap.peek().getValue().doubleValue()) >= 0)
								)) {
					if (heap.size() == size) {
						heap.remove(heap.peek());
					}
					heap.offer(item);
				}
			}
			while(!heap.isEmpty()) {
				topElements.add(heap.poll());
			}
			if (!aggregationType.equals(MIN)) {
				Collections.reverse(topElements);
			}
			return topElements;
		}

	}
	
	private static interface TableTransformer {
		TableTO getTable(TableTO table, Formula formula, EnvironmentDataSource ds, ReportUserInfo reportUserInfo, ListMultimap<TO<?>, RowTO> groups);
	}
	
	public ReportTableTransformer(TableTO table) {
		this.table = table;
	}
	
	public TableTO transform(Formula formula, EnvironmentDataSource ds, ReportUserInfo reportUserInfo) {
        TableTO resultTable = this.table;
        for (TransformType transformType : formula.getTransformTypes()) {
            resultTable = transformerMap.get(transformType).getTable(resultTable, formula, ds, reportUserInfo, groups);
        }
        return resultTable;
	}

	private static TableTO composeTable (TableTO table, List<RowTO> topRows) {
		Map<Integer, List<String>> headerMap = CollectionUtils.newMap();
		headerMap.put(ROW_HEADER, table.getHeader(ROW_HEADER));
		headerMap.put(COLUMN_HEADER, table.getHeader(COLUMN_HEADER));
		headerMap.put(SLICER_AXIS_NUMBER, table.getHeader(SLICER_AXIS_NUMBER));
		TableTO rawTable = new TableTO(topRows, table.getUserId(), headerMap, topRows.size());
		
		return rawTable;
	}
}
