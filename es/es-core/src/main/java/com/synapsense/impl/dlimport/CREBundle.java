package com.synapsense.impl.dlimport;

import java.util.Map;

import org.apache.log4j.Logger;

import com.synapsense.dto.CRERule;
import com.synapsense.dto.TO;
import com.synapsense.service.RulesEngine;

/**
 * @author Gabriel Helman
 * @since Mars 
 * Date: 5/9/11 
 * Time: 6:12 PM Represents a CRE Rule Instance to be
 *        created at a later time in the export process
 */
public class CREBundle {
	private final static Logger log = Logger.getLogger(CREBundle.class);
	private String ruleTypeName;
	private String destinationPropertyName;
	private Map<String, String> bindings;
	private String dlid;

	public CREBundle(String ruleTypeName, String dlid, String destinationPropertyName, Map<String, String> bindings) {
		this.ruleTypeName = ruleTypeName;
		this.destinationPropertyName = destinationPropertyName;
		this.bindings = bindings;
		this.dlid = dlid;
	}

	public String getRuleTypeName() {
		return ruleTypeName;
	}

	public void setRuleTypeName(String ruleTypeName) {
		this.ruleTypeName = ruleTypeName;
	}

	public String getDestinationPropertyName() {
		return destinationPropertyName;
	}

	public void setDestinationPropertyName(String destinationPropertyName) {
		this.destinationPropertyName = destinationPropertyName;
	}

	public Map<String, String> getBindings() {
		return bindings;
	}

	public void setBindings(Map<String, String> bindings) {
		this.bindings = bindings;
	}

	public boolean hasBindings() {
		return bindings != null && bindings.size() > 0;
	}

	public String getDlid() {
		return dlid;
	}

	public void setDlid(String dlid) {
		this.dlid = dlid;
	}

	public String getRuleInstanceName(TO<?> destination) {
		StringBuilder instanceName = new StringBuilder();
		instanceName.append(destination.getID().toString());
		instanceName.append(destinationPropertyName);
		return instanceName.toString();
	}

	/**
	 * Convert this bundle into an actual CRERule, ready for use.
	 * 
	 * @param destination
	 * @return
	 */
	public CRERule makeRule(TO<?> destination, RulesEngine re) {

		// instance name format is <TO#><propertyName>
		// for example, "42ultimateAnswer" or "13lastValue"
		// StringBuilder instanceName = new StringBuilder();
		// instanceName.append( destination.getID().toString() );
		// instanceName.append( destinationPropertyName );

		String instanceName = this.getRuleInstanceName(destination);

		log.trace("Constructing a CRERule named '" + instanceName.toString() + "' of type '" + this.getRuleTypeName() + "'" );

		TO<?> ruleTypeID = re.getRuleTypeId(this.getRuleTypeName());

		CRERule rule = new CRERule(instanceName.toString(), ruleTypeID, destination, destinationPropertyName);

		for (Map.Entry<String, String> e : this.getBindings().entrySet()) {
			log.trace("adding binding object prop=" + e.getKey() + "  boundvar= " + e.getValue());
			rule.addSrcProperty(destination, e.getKey(), e.getValue());
		}

		return rule;

	}
}
