
package com.synapsense.impl.erlnode.commands;

import java.util.Map;

import com.ericsson.otp.erlang.OtpErlangBinary;
import com.ericsson.otp.erlang.OtpErlangObject;
import com.ericsson.otp.erlang.OtpErlangTuple;
import com.synapsense.dto.ObjectType;
import com.synapsense.dto.TO;
import com.synapsense.exception.IllegalInputParameterException;
import com.synapsense.impl.erlnode.ExceptionTypes;
import com.synapsense.impl.erlnode.utils.ToConverter;
import com.synapsense.impl.erlnode.utils.ValueTOConverter;

import com.synapsense.impl.erlnode.utils.StringConverter;

public class SetPropertyValueCommand extends Command {

	public SetPropertyValueCommand(Map<String, Object> state) {
		super(state);
	}

	public OtpErlangObject do_command(OtpErlangTuple command) throws Exception {
		OtpErlangObject toTuple = command.elementAt(1);
		OtpErlangObject propertyNameTuple = command.elementAt(2);
		OtpErlangObject valueTuple = command.elementAt(3);
		if (toTuple == null || propertyNameTuple == null
				|| !(toTuple instanceof OtpErlangTuple)
				|| !(propertyNameTuple instanceof OtpErlangBinary)
				|| valueTuple == null) {
			return makeException(ExceptionTypes.ILLEGAL_ARGUMENTS, null);
		}

		TO<?> objId = ToConverter.parseTo((OtpErlangTuple) toTuple);
		String propertyName = StringConverter.utf8BinaryToString(command.elementAt(2));
		ObjectType ot = getEnv().getObjectType(objId.getTypeName());
		if(ot== null){
			throw new IllegalInputParameterException("Cannot find object type [" + objId.getTypeName() + " ].");
		}
		try {
			String wantType = ValueTOConverter.lookupPropertyType(ot, propertyName);
			getEnv().setPropertyValue(objId, propertyName, ValueTOConverter.valueObject(valueTuple, wantType));
		} catch (IllegalArgumentException e) {
			return makeException(ExceptionTypes.NO_OBJECT_TYPE, valueTuple
					.toString());
		} catch (com.synapsense.exception.ObjectNotFoundException e) {
			return makeException(ExceptionTypes.OBJECT_NOT_FOUND, e
					.getMessage());
		} catch (com.synapsense.exception.PropertyNotFoundException e) {
			return makeException(ExceptionTypes.PROPERTY_NOT_FOUND, e
					.getMessage());
		}

		return makeOkResult(null);
	}
}
