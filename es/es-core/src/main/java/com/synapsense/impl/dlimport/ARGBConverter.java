package com.synapsense.impl.dlimport;

import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * Helper class to convert ARGB image types.
 * 
 */
public class ARGBConverter {

	/**
	 * Converts a BufferedImage to TYPE_INT_ARGB, insuring that we know what
	 * type the backing array is.
	 * 
	 * @param source
	 *            any BufferedImage
	 * @return a BufferedImage containing the same image as source, but stored
	 *         as TYPE_INT_ARGB.
	 */
	public static BufferedImage convert(BufferedImage source) {
		BufferedImage result = new BufferedImage(source.getWidth(null), source.getHeight(null),
		        BufferedImage.TYPE_INT_ARGB);
		final Graphics2D g2 = result.createGraphics();
		g2.drawImage(source, 0, 0, null);
		g2.dispose();
		return result;
	}

}
