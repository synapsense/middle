package com.synapsense.impl.alerting.mapping;

import java.util.Collection;
import java.util.List;

import com.synapsense.dto.Alert;
import com.synapsense.dto.AlertPriority;
import com.synapsense.dto.AlertType;
import com.synapsense.dto.PriorityTier;
import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;
import com.synapsense.service.Environment;
import com.synapsense.util.CollectionUtils.Functor;

public final class Converters {

	public static Functor<List<ValueTO>, Alert> alertToPropertiesConverter(final Environment env) {
		return new AlertToProperties(env);
	}

	public static Functor<List<ValueTO>, AlertType> alertTypeToPropertiesConverter(final Environment env) {
		return new AlertTypeToProperties(env);
	}

	public static Functor<List<ValueTO>, AlertPriority> alertPriorityToPropertiesConverter(final Environment env) {
		return new AlertPriorityToProperties(env);
	}

	public static Functor<AlertPriority, Collection<ValueTO>> propertiesToAlertPriorityConverter(final Environment env) {
		return new PropertiesToAlertPriority(env);
	}

	public static Functor<Alert, Collection<ValueTO>> propertiesToAlertConverter(final TO<?> alertRef,
	        final Environment env) {
		return new PropertiesToAlert(env,alertRef);
	}

	public static Functor<PriorityTier, Collection<ValueTO>> propertiesToTierConverter() {
		return new PropertiesToTier();
	}
	
	public static Functor<Collection<ValueTO>, PriorityTier> tierToPropertiesConverter() {
		return new TierToProperties();
	}
	

	public static Functor<AlertType, Collection<ValueTO>> propertiesToAlertTypeConverter(final Environment env) {
		return new PropertiesToAlertType(env);
	}

	private Converters() {
	}
}
