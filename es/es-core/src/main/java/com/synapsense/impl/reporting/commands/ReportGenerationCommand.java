package com.synapsense.impl.reporting.commands;

import com.synapsense.exception.ObjectExistsException;
import com.synapsense.impl.reporting.commandengine.Command;
import com.synapsense.impl.reporting.commandengine.monitoring.StatusMonitor;
import com.synapsense.impl.reporting.dao.ReportDAO;
import com.synapsense.impl.reporting.dao.ReportTemplateDAO;
import com.synapsense.impl.reporting.model.*;
import com.synapsense.service.reporting.dto.CommandStatus;
import com.synapsense.service.reporting.dto.ReportInfo;
import com.synapsense.service.reporting.dto.ReportTaskInfo;
import com.synapsense.service.reporting.exception.DAOReportingException;
import com.synapsense.service.reporting.exception.GenerationReportingException;
import com.synapsense.service.reporting.management.ReportingException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class ReportGenerationCommand implements Command {

	private static final Log log = LogFactory.getLog(ReportGenerationCommand.class);

	private ReportTaskInfo reportTask;
    private ReportInfo reportInstance;

	private ReportTemplateDAO rtDAO;

	private ReportDAO reportDAO;

	private ReportGenerationHandler handler;

	private ReentrantLock runlock;
	private Condition runCondition;

	private boolean cancelled;

	public ReportGenerationCommand(ReportTaskInfo reportTask, ReportInfo reportInfo, ReportTemplateDAO rtDAO, ReportDAO reportDAO) {
        this.reportInstance = reportInfo;
		this.reportTask = reportTask;
		this.rtDAO = rtDAO;
		this.reportDAO = reportDAO;
		this.runlock = new ReentrantLock();
		this.runCondition = runlock.newCondition();
	}

	@Override
	public String getName() {
		return "Report " + reportTask.getName();
	}

	@Override
	public String getDescription() {
		return "Generation of report '" + reportTask.getName() + "'";
	}

	@Override
	public ReportInfo getReportInfo() {
		return this.reportInstance;
	}
	
	@Override
	public void run(final StatusMonitor statusMonitor) {
		if (log.isDebugEnabled()) {
			log.debug(getName() + "..started");
		}

		try {
			// Make sure any relative parameters are updated, such as "Last Month" date ranges.
			reportTask.refreshParameters();

			statusMonitor.setStatus(CommandStatus.inProgress("Compiling report template"));
			ReportTemplate reportTemplate = new ReportTemplate(rtDAO.getReportTemplateDesign(reportTask.getReportTemplateName()));
			statusMonitor.setProgress(0.3);

			if (checkCancelled(statusMonitor)) {
				return;
			}

			if (log.isDebugEnabled()) {
				log.debug(getName() + "...Creating report generation handler");
			}
			statusMonitor.setStatus(CommandStatus.inProgress("Creating report generation handler"));
            // TODO : what if no param values. silent
			handler = reportTemplate.createReportGenerationHandler(reportTask.getParametersValues());
		} catch (ReportingException e) {
			if (log.isDebugEnabled()) {
				log.error(getName() + "...failed", e);
			}
			statusMonitor.setStatus(CommandStatus.failed("Report generation failed: " + e.getMessage()));
			throw new ReportingCommandException(e.getMessage(), e);
		}

		handler.addListener(new ReportGenerationListener() {

			private void signal() {
				runlock.lock();
				try {
					runCondition.signal();
				} finally {
					runlock.unlock();
				}
			}

			@Override
			public void cancelled() {
				if (log.isDebugEnabled()) {
					log.debug(getName() + "...cancelled");
				}
				statusMonitor.setStatus(CommandStatus.cancelled("Generation cancelled"));
				signal();
			}

			@Override
			public void completed(Report report) {
				try {
					statusMonitor.setProgress(0.6);
					statusMonitor.setStatus(CommandStatus.inProgress("Saving report object"));
					reportInstance.setReportTaskId(reportTask.getTaskId());
					reportDAO.saveReport(reportInstance, report, reportTask.getReportTemplateName());
					statusMonitor.setProgress(1.0);
					statusMonitor.setStatus(CommandStatus.completed("Report generated successfully"));
					if (log.isDebugEnabled()) {
						log.debug(getName() + "...completed");
					}
				} catch (ObjectExistsException | DAOReportingException e) {
					log.warn(getName() + "...failed", e);
					statusMonitor.setStatus(CommandStatus.failed("Report generation failed: " + e.getMessage()));
				} finally {
					signal();
				}
			}

			@Override
			public void failed(Throwable e) {
				log.warn("Failed to generate report " + getName(), e);
				statusMonitor.setStatus(CommandStatus.failed("Report generation failed: " + e.getMessage()));
				signal();
			}
		});

		statusMonitor.setStatus(CommandStatus.inProgress("Generating report"));
		handler.startGeneration();

//		runlock.lock();
//		try {
//			runCondition.await();
//		} catch (InterruptedException e) {
//			return;
//		} finally {
//			runlock.unlock();
//		}

		if (statusMonitor.getStatus().equals(CommandStatus.FAILED)) {
			String message = statusMonitor.getStatus().getStatusMessage();
			if (message == null) {
				message = getName() + " generation failed.";
			}
			throw new ReportingCommandException(message);
		}
	}

	@Override
	public void rollback() {
		if (log.isDebugEnabled()) {
			log.debug("Undo command " + getName());
		}
	}

	@Override
	public void interrupt() {
		cancelled = true;
		try {
			handler.cancelGeneration();
		} catch (GenerationReportingException e) {
			throw new ReportingCommandException(e.getMessage(), e);
		}
	}

	@Override
	public boolean pause() {
		return false;
	}

	@Override
	public void renew() {
		throw new UnsupportedOperationException();
	}

	private boolean checkCancelled(StatusMonitor statusMonitor) {
		if (cancelled) {
			statusMonitor.setStatus(CommandStatus.cancelled("Generation cancelled"));
			if (log.isDebugEnabled()) {
				log.debug(getName() + "...cancelled");
			}
		}
		return cancelled;
	}
}
