
package com.synapsense.impl.erlnode.commands;

import java.util.Collection;
import java.util.Map;

import com.ericsson.otp.erlang.OtpErlangList;
import com.ericsson.otp.erlang.OtpErlangObject;
import com.ericsson.otp.erlang.OtpErlangTuple;
import com.synapsense.impl.erlnode.ExceptionTypes;
import com.synapsense.impl.erlnode.utils.CollectionToConverter;
import com.synapsense.impl.erlnode.utils.ToConverter;
import com.synapsense.dto.TO;
import com.synapsense.exception.ObjectNotFoundException;

/**
 * @author stikhon
 * 
 */
public class GetChildrenCommand1 extends Command {

	public GetChildrenCommand1(Map<String, Object> state) {
		super(state);
	}

	/*
	 * %%       Objects = list(to()) | to()
	 */
	@Override
	public OtpErlangObject do_command(OtpErlangTuple command) throws Exception {
		OtpErlangObject result = null;

		OtpErlangObject toTuple = command.elementAt(1);
		if (toTuple == null
				|| !(toTuple instanceof OtpErlangTuple || toTuple instanceof OtpErlangList)) {
			//An empty list represented by "" will cause an exception
			result = makeException(ExceptionTypes.ILLEGAL_ARGUMENTS, null);
		} else if (toTuple instanceof OtpErlangTuple) {
			TO<?> to = ToConverter.parseTo((OtpErlangTuple) toTuple);
			try {
				result = makeOkResult(ToConverter.serializeToList(getEnv()
						.getChildren(to)));
			} catch (ObjectNotFoundException e) {
				result = makeException(ExceptionTypes.OBJECT_NOT_FOUND, e
						.getMessage());
			}
		} else if (toTuple instanceof OtpErlangList) {
			Collection<TO<?>> toList = ToConverter
					.parseToList((OtpErlangList) toTuple);
			result = makeOkResult(CollectionToConverter
					.serializeCollectionToList(getEnv().getChildren(toList)));
		}

		return result;
	}

}
