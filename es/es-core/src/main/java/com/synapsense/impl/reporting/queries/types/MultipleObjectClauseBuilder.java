package com.synapsense.impl.reporting.queries.types;

import java.util.List;
import java.util.Map;

import com.synapsense.impl.reporting.queries.TableProcessor.QueryClause;
import com.synapsense.impl.reporting.queries.types.ReportingQueryTypeFactory.EmptyQueryTypeClauseBuilder;
import com.synapsense.impl.reporting.utils.ReportingConstants.QUERY.SYMBOL;
import com.synapsense.service.reporting.Cell;
import com.synapsense.util.CollectionUtils;

public class MultipleObjectClauseBuilder implements QueryTypeClauseBuilder {

    private static final Map<QueryClause, QueryTypeClauseBuilder> clauseBuilders = CollectionUtils.newMap();


    static {
        clauseBuilders.put(QueryClause.GROUP_BY,
                new GroupByMultipleObjectClauseBuilder());
        clauseBuilders.put(QueryClause.SELECT,
                new EmptyQueryTypeClauseBuilder());
        clauseBuilders.put(QueryClause.WHERE,
                new EmptyQueryTypeClauseBuilder());    }


    public QueryTypeClauseBuilder getClauseBuilder(QueryClause queryClause) {
        QueryTypeClauseBuilder clauseBuilder = clauseBuilders.get(queryClause);
        return clauseBuilder;
    }

    @Override
    public String build(QueryClause queryClause, List<Cell> valueIdCells) {

        return getClauseBuilder(queryClause).build(queryClause, valueIdCells);

    }

    private static class GroupByMultipleObjectClauseBuilder implements QueryTypeClauseBuilder {

        @Override
        public String build(QueryClause queryClause, List<Cell> valueIdCells) {

            StringBuilder clauseBuilder = new StringBuilder();

            for (Cell cell : valueIdCells ) {
            	clauseBuilder.append("value_id_fk in (:cellValueIds")
						.append(valueIdCells.indexOf(cell))
						.append(SYMBOL.CLOSE_BRACKET)
						.append(SYMBOL.COMMA)
						.append(SYMBOL.SPACE);
			}
            return clauseBuilder.toString();
        }
    }

}
