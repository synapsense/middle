package com.synapsense.impl.alerting.macros;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

public class MacrosCollection implements Macro {

	private Collection<Macro> macros;

    public MacrosCollection(Macro...m) {
        this(Arrays.asList(m));
    }

	public MacrosCollection(Collection<Macro> macros) {
		this.macros = macros;
	}

	@Override
	public String expandIn(String input, MacroContext ctx) {
		for (Macro macro : macros) {
			input = macro.expandIn(input, ctx);
		}
		return input;
	}

}
