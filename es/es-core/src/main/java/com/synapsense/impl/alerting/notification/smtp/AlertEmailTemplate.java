package com.synapsense.impl.alerting.notification.smtp;

import com.synapsense.impl.alerting.notification.Provider;
import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.Template;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.nio.file.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import static java.nio.file.StandardWatchEventKinds.*;

/**
 * @author : shabanov
 */
public class AlertEmailTemplate implements Provider<Template> {
	private final static Logger logger = Logger.getLogger(AlertEmailTemplate.class);

	public static final String DEFAULT_TEMPLATE = "<html>\n" + "<body>\n" + "<h1>${message}</h1>\n"
	        + "<#if attachments?size != 0>\n" + "<table class=\"pretty\">\n" + "<tr>\n"
	        + "<#list attachments as attachment>\n" + "<td>${attachment.description}" + "</#list>\n" + "</tr>\n"
	        + "<tr>\n" + "<#list attachments as attachment>" + "<td>${attachment.img}" + "</#list>\n" + "</tr>\n"
	        + "</table>\n" + "</#if>\n" + "</body>\n" + "</html>\n";

	private Configuration cfg;

	private File templateFile;

	private Template template;

	private final Lock templateModifyLock = new ReentrantLock(true);

	private ExecutorService watchThread = Executors.newSingleThreadExecutor();

	public AlertEmailTemplate(File template, boolean reloadIfChanged) {
        // TODO : redesign exceptional conditions : file not found, etc...
		this.templateFile = template;

		this.cfg = new Configuration();
		try {
			cfg.setDirectoryForTemplateLoading(new File(templateFile.getParent()));
			cfg.setObjectWrapper(new DefaultObjectWrapper());
			// do not override template in runtime
			cfg.setWhitespaceStripping(false);
		} catch (IOException e) {
			throw new IllegalStateException("Failed to init template engine", e);
		}

		try {
			loadTemplate(cfg);
		} catch (IOException e) {
			logger.warn("Failed to load template from file", e);
			try {
				loadDefaultTemplate(cfg);
			} catch (IOException e1) {
				throw new IllegalStateException("Failed to load default email template", e);
			}
		}

		if (reloadIfChanged) {
			try {
				watchChanges();
			} catch (Exception e) {
				throw new IllegalStateException("Failed to start watching template file", e);
			}
		}
	}

	@Override
	public Template get() {
		templateModifyLock.lock();
		try {
			return template;
		} finally {
			templateModifyLock.unlock();
		}
	}

	public synchronized void watchChanges() {
		if (logger.isDebugEnabled()) {
			logger.debug("Starting email " + "template file watcher...");
		}

		final WatchService watcher;
		try {
			watcher = FileSystems.getDefault().newWatchService();
			Paths.get(templateFile.getParentFile().toURI()).register(watcher, ENTRY_MODIFY, ENTRY_DELETE, ENTRY_CREATE);
		} catch (IOException e) {
			throw new IllegalStateException("Failed to start watching template file", e);
		}

		watchThread.execute(new Runnable() {
			@Override
			public void run() {
				while (true) {
					WatchKey key = null;
					if (logger.isTraceEnabled()) {
						logger.trace("Waiting for event from FS...");
					}

					if (Thread.currentThread().isInterrupted())
						break;

					try {
						key = watcher.take();
					} catch (InterruptedException e) {
						return;
					}
					if (logger.isTraceEnabled()) {
						logger.trace("Got new event " + key);
					}

					for (WatchEvent event : key.pollEvents()) {
						WatchEvent.Kind kind = event.kind();
						if (kind == OVERFLOW) {
							continue;
						}

						WatchEvent<Path> ev = event;
						Path name = ev.context();
						if (!name.equals(Paths.get(templateFile.getName()))) {
							continue;
						}

						if (kind == ENTRY_MODIFY || kind == ENTRY_CREATE) {
							if (logger.isTraceEnabled()) {
								logger.trace("MODIFY event received on file : " + name);
							}
							// print out event
							try {
								loadTemplate(cfg);
							} catch (IOException e) {
								logger.warn("Failed to reload template. Previous one will be used", e);
							}
						} else if (kind == ENTRY_DELETE) {
							if (logger.isDebugEnabled()) {
								logger.debug("Template has been deleted. Loading default one...");
							}
							try {
								loadDefaultTemplate(cfg);
							} catch (IOException e) {
								logger.warn("Failed to reload default template", e);
							}
						}

					}
					boolean valid = key.reset();
					if (!valid) {
						logger.warn("Watch key is no more valid!" + key);
						break;
					}
				}
			}
		});
	}

	public synchronized void stopWatchChanges() {
		watchThread.shutdownNow();
		try {
			if (!watchThread.awaitTermination(5, TimeUnit.SECONDS)) {
				throw new IllegalStateException(
				        "Failed to wait service termination in specified interval. watchThread did not respond to interruption properly");
			}
		} catch (InterruptedException e) {
			logger.warn("Failed to stop watching template file", e);
		}
		watchThread = Executors.newSingleThreadExecutor();
	}

	private void loadDefaultTemplate(Configuration cfg) throws IOException {
		templateModifyLock.lock();
		try {
			template = new Template(templateFile.getName(), new StringReader(DEFAULT_TEMPLATE), cfg);
		} finally {
			templateModifyLock.unlock();
		}

		if (logger.isDebugEnabled()) {
			if (logger.isDebugEnabled()) {
				logger.debug("Default email template has been loaded:" + template.toString());
			}
		}
	}

	private void loadTemplate(Configuration cfg) throws IOException {
		templateModifyLock.lock();
		try {
			template = cfg.getTemplate(templateFile.getName());
		} finally {
			templateModifyLock.unlock();
		}
		if (logger.isDebugEnabled()) {
			logger.debug("Email template has been loaded from file:" + template.toString());
		}
	}

}
