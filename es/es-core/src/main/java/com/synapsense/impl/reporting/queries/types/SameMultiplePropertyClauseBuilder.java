package com.synapsense.impl.reporting.queries.types;

import java.util.List;
import java.util.Map;
import com.synapsense.impl.reporting.queries.TableProcessor.QueryClause;
import com.synapsense.impl.reporting.queries.types.ReportingQueryTypeFactory.EmptyQueryTypeClauseBuilder;

import com.synapsense.impl.reporting.utils.ReportingConstants.QUERY.CLAUSE_PART;
import com.synapsense.impl.reporting.utils.ReportingConstants.QUERY.SYMBOL;
import com.synapsense.impl.reporting.utils.ReportingConstants.QUERY.WORD;
import com.synapsense.service.reporting.Cell;
import com.synapsense.util.CollectionUtils;

public class SameMultiplePropertyClauseBuilder implements
		QueryTypeClauseBuilder {

    private static final Map<QueryClause, QueryTypeClauseBuilder> clauseBuilders = CollectionUtils.newMap();

    static {
        clauseBuilders.put(QueryClause.GROUP_BY,
                new GroupBySameMultiplePropertyClauseBuilder());
        clauseBuilders.put(QueryClause.SELECT,
                new SelectSameMultiplePropertyClauseBuilder());
        clauseBuilders.put(QueryClause.WHERE,
                new WhereSameMultiplePropertyClauseBuilder());    }


    public QueryTypeClauseBuilder getClauseBuilder(QueryClause queryClause) {
        QueryTypeClauseBuilder clauseBuilder = clauseBuilders.get(queryClause);
        return clauseBuilder;
    }

    @Override
    public String build(QueryClause queryClause, List<Cell> valueIdCells) {

        return getClauseBuilder(queryClause).build(queryClause, valueIdCells);

    }
    
    private static class GroupBySameMultiplePropertyClauseBuilder implements QueryTypeClauseBuilder {

        @Override
        public String build(QueryClause queryClause, List<Cell> valueIdCells) {

            StringBuilder clauseBuilder = new StringBuilder();

            clauseBuilder.append("objects_valueids.object_id")
				.append(SYMBOL.COMMA)
				.append(SYMBOL.SPACE);
            return clauseBuilder.toString();
            
        }
    }
    
    private static class WhereSameMultiplePropertyClauseBuilder implements QueryTypeClauseBuilder {

        @Override
        public String build(QueryClause queryClause, List<Cell> valueIdCells) {

            StringBuilder clauseBuilder = new StringBuilder();

            clauseBuilder.append(CLAUSE_PART.OBJECTS_WHERE_JOIN_BY_VALUEID)
				.append(SYMBOL.SPACE)
				.append(WORD.AND);
            return clauseBuilder.toString();
            
        }
    }
    
    private static class SelectSameMultiplePropertyClauseBuilder implements QueryTypeClauseBuilder {

        @Override
        public String build(QueryClause queryClause, List<Cell> valueIdCells) {
            StringBuilder clauseBuilder = new StringBuilder();
            clauseBuilder.append("objects_valueids.object_id, ");
            return clauseBuilder.toString();
            
        }
    }

    
}
