package com.synapsense.impl.performer.audio;

import java.net.SocketException;
import java.util.Random;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import javax.naming.NamingException;

import org.apache.log4j.Logger;
import org.opennms.protocols.snmp.SnmpHandler;
import org.opennms.protocols.snmp.SnmpInt32;
import org.opennms.protocols.snmp.SnmpParameters;
import org.opennms.protocols.snmp.SnmpPduPacket;
import org.opennms.protocols.snmp.SnmpPduRequest;
import org.opennms.protocols.snmp.SnmpPeer;
import org.opennms.protocols.snmp.SnmpSMI;
import org.opennms.protocols.snmp.SnmpSession;
import org.opennms.protocols.snmp.SnmpSyntax;
import org.opennms.protocols.snmp.SnmpVarBind;

import com.synapsense.dto.Alert;
import com.synapsense.dto.AlertPriority;
import com.synapsense.dto.AlertStatus;
import com.synapsense.dto.Message;
import com.synapsense.impl.performer.snmp.SNMPPerformerException;
import com.synapsense.impl.performer.snmp.conf.ConnectionInfoProvider;
import com.synapsense.impl.performer.snmp.conf.ConnectionInfoProvider.ConnectionInfo;
import com.synapsense.impl.performer.snmp.conf.SNMPConfigurationException;
import com.synapsense.service.AlertService;

public class SignalSender implements SnmpHandler {

	private final static Logger logger = Logger.getLogger(SignalSender.class);
	private final static int VERSION = SnmpSMI.SNMPV1;

	private final String destination;
	private SnmpSession session;
	private final ConnectionInfo connInfo;
	private AlertService alertService;

	private volatile boolean activeSignalTower = false;
	private boolean activeSound = false;
	private boolean activeLight = false;

	private int activeAlertId;
	private String activeAlertPriority;

	private static Random random = new Random();
	private StatusChangeListener statusListener;

	private ScheduledExecutorService executorService;
	private TurnOffAudioTask audioTask;
	private ScheduledFuture<?> audioScheduledFuture;
	private TurnOffLightTask lightTask;
	private ScheduledFuture<?> lightScheduledFuture;

	public SignalSender(String destination, AlertService alertService, ScheduledExecutorService executorService)
	        throws SNMPConfigurationException {

		this.alertService = alertService;
		this.destination = destination;
		this.executorService = executorService;

		connInfo = ConnectionInfoProvider.getConnectionInfo(destination, "AV");

		SnmpPeer peer = new SnmpPeer(connInfo.getRemAddr(), connInfo.getRemPort(), connInfo.getLocAddr(),
		        connInfo.getLocPort());

		SnmpParameters params = peer.getParameters();
		params.setReadCommunity(connInfo.getReadCommunity());
		params.setVersion(VERSION);
		params.setWriteCommunity(connInfo.getWriteCommunity());
		peer.setParameters(params);

		statusListener = StatusChangeListener.getInstance();

		try {
			session = new SnmpSession(peer);
		} catch (SocketException e) {
			throw new SNMPConfigurationException("Cannot create SNMP session for Signal Tower,", e);
		}

		session.setDefaultHandler(this);
		logger.info("Sender to tower " + destination + " creation done.");
	}

	public void send(Message metadata) throws SNMPPerformerException {

		// if tower is turning off
		if (!activeSignalTower) {
			turnOnTheSignalTower(metadata);

		} else {
			// if second notification
			if (activeAlertId == metadata.getAlertID()) {
				if (lightScheduledFuture != null) {
					if (!lightScheduledFuture.isDone())
						lightScheduledFuture.cancel(true);
				}
				if (audioScheduledFuture != null) {
					if (!audioScheduledFuture.isDone())
						audioScheduledFuture.cancel(true);
				}
				turnOnTheSignalTower(metadata);
			} else {
				// if priority is higher
				if (comparePriority(metadata.getPriority())) {
					if (lightScheduledFuture != null) {
						if (!lightScheduledFuture.isDone())
							lightScheduledFuture.cancel(true);
					}
					if (audioScheduledFuture != null) {
						if (!audioScheduledFuture.isDone())
							audioScheduledFuture.cancel(true);
					}
					turnOnTheSignalTower(metadata);

				}
			}

		}
	}

	private void turnOnTheSignalTower(Message metadata) throws SNMPPerformerException {
		if (isOpenAlertStatus(metadata)) {
			lightTask = new TurnOffLightTask(this);
			audioTask = new TurnOffAudioTask(this);
			AlertPriority priority;
			AlertSwitchObject object = new AlertSwitchObject(metadata.getAlertID(), this, destination);
			try {
				priority = getAlertPriority(metadata);
				statusListener.unsubscribe(destination);
				session.send(createTurnOnRequest(priority));
				activeSignalTower = true;
				activeAlertId = metadata.getAlertID();
				activeAlertPriority = metadata.getPriority();
				if ((priority.isAudioSignal())) {
					activeSound = true;
					if (priority.getAudioDuration() != 0) {
						audioScheduledFuture = executorService.schedule(audioTask, priority.getAudioDuration(),
						        TimeUnit.SECONDS);
					} else {
						object.setAudioIntervention(true);
					}
				}
				if ((priority.isGreenSignal() || priority.isRedSignal() || priority.isYellowSignal())) {
					activeLight = true;
					if (priority.getLightDuration() != 0) {
						lightScheduledFuture = executorService.schedule(lightTask, priority.getLightDuration(),
						        TimeUnit.SECONDS);
					} else {
						object.setLightIntervention(true);
					}
				}
				setInactive();
				if (object.isIntervention()) {
					statusListener.subscribe(object);
				}
			} catch (NamingException e) {
				throw new SNMPPerformerException("Priority [" + metadata.getPriority() + "] doesn't exist");
			}
		}

	}

	private boolean isOpenAlertStatus(Message metadata) {
		Alert alert = alertService.getAlert(metadata.getAlertID());
		AlertStatus status = alert.getStatus();
		if (status.ordinal() == 0) {
			return true;
		} else {
			return false;
		}
	}

	private AlertPriority getAlertPriority(Message metadata) throws NamingException {
		AlertPriority priority = alertService.getAlertPriority(metadata.getPriority());
		return priority;
	}

	private boolean comparePriority(String priority) {
		if (priority.equals(SignalTowerConstants.ALERT_PRIORITY_INFORMATIONAL)) {
			return false;
		}
		if (priority.equals(SignalTowerConstants.ALERT_PRIORITY_MINOR)) {
			if (activeAlertPriority.equals(SignalTowerConstants.ALERT_PRIORITY_INFORMATIONAL)) {
				return true;
			} else {
				return false;
			}
		}
		if (priority.equals(SignalTowerConstants.ALERT_PRIORITY_MAJOR)) {
			if (activeAlertPriority.equals(SignalTowerConstants.ALERT_PRIORITY_INFORMATIONAL)
			        || activeAlertPriority.equals(SignalTowerConstants.ALERT_PRIORITY_MINOR)) {
				return true;
			} else {
				return false;
			}
		}
		if (priority.equals(SignalTowerConstants.ALERT_PRIORITY_CRITICAL)) {
			if (!activeAlertPriority.equals(SignalTowerConstants.ALERT_PRIORITY_CRITICAL)) {
				return true;
			} else {
				return false;
			}
		}
		return false;
	}

	private void setInactiveAudio() {
		activeSound = false;
		setInactive();
	}

	private void setInactiveLight() {
		activeLight = false;
		setInactive();
	}

	private void setInactive() {
		if ((!activeSound && !activeLight)) {
			activeSignalTower = false;
			activeAlertId = -1;
			activeAlertPriority = "";
		}
	}

	private SnmpPduRequest createTurnOnRequest(AlertPriority priority) {
		SnmpPduRequest request = new SnmpPduRequest();
		request.setCommand(SnmpPduPacket.SET);
		request.addVarBindAt(0, new SnmpVarBind(SignalTowerConstants.RED_LED_OID, new SnmpInt32(
		        priority.isRedSignal() ? 1 : 0)));
		request.addVarBindAt(1,
		        new SnmpVarBind(SignalTowerConstants.YELLOW_LED_OID, new SnmpInt32(priority.isYellowSignal() ? 1 : 0)));
		request.addVarBindAt(2,
		        new SnmpVarBind(SignalTowerConstants.GREEN_LED_OID, new SnmpInt32(priority.isGreenSignal() ? 1 : 0)));
		request.addVarBindAt(3, new SnmpVarBind(SignalTowerConstants.SIGNAL_OID, new SnmpInt32(
		        priority.isAudioSignal() ? 1 : 0)));
		request.setRequestId(random.nextInt());
		return request;
	}

	protected synchronized void sendTurnOffAudioRequest() {
		SnmpPduRequest request = new SnmpPduRequest();
		request.setCommand(SnmpPduPacket.SET);
		request.addVarBind(new SnmpVarBind(SignalTowerConstants.SIGNAL_OID, new SnmpInt32(0)));
		request.setRequestId(random.nextInt());
		session.send(request);
		setInactiveAudio();
	}

	protected synchronized void sendTurnOffLightRequest() {
		SnmpPduRequest request = new SnmpPduRequest();
		request.setCommand(SnmpPduPacket.SET);
		request.addVarBind(new SnmpVarBind(SignalTowerConstants.RED_LED_OID, new SnmpInt32(0)));
		request.addVarBind(new SnmpVarBind(SignalTowerConstants.GREEN_LED_OID, new SnmpInt32(0)));
		request.addVarBind(new SnmpVarBind(SignalTowerConstants.YELLOW_LED_OID, new SnmpInt32(0)));
		request.setRequestId(random.nextInt());
		session.send(request);
		setInactiveLight();
	}

	public void close() {
		logger.info("Closing the Signal Tower Sender to " + destination);
		session.close();
		session = null;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((destination == null) ? 0 : destination.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final SignalSender other = (SignalSender) obj;
		if (destination == null) {
			if (other.destination != null)
				return false;
		} else if (!destination.equals(other.destination))
			return false;
		return true;
	}

	@Override
	public void snmpInternalError(SnmpSession arg0, int arg1, SnmpSyntax arg2) {
		logger.warn("Got snmp internal error " + arg2 + " from tower: " + destination);
	}

	@Override
	public void snmpReceivedPdu(SnmpSession arg0, int arg1, SnmpPduPacket arg2) {
		logger.info("Got SNMP PDU " + arg2 + " from tower: " + destination);
	}

	@Override
	public void snmpTimeoutError(SnmpSession arg0, SnmpSyntax arg1) {
		logger.warn("Could not send response to tower: " + destination);
	}

}
