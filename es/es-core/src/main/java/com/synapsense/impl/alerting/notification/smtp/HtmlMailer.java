package com.synapsense.impl.alerting.notification.smtp;

import com.synapsense.dto.Message;
import com.synapsense.impl.alerting.notification.Provider;
import com.synapsense.util.CollectionUtils;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.apache.log4j.Logger;

import javax.mail.Session;
import java.io.IOException;
import java.io.StringWriter;
import java.util.Collections;
import java.util.Map;

/**
 * Html email sender. Prepares email in html format, perform attachments if any.
 * 
 * @author : shabanov
 */
public class HtmlMailer extends SMTPMailer {

	private final static Logger logger = Logger.getLogger(HtmlMailer.class);

	private final Provider<Template> templateProvider;

	public HtmlMailer(Provider<Session> sessionPrv, Provider<Template> templateProvider) {
		super(sessionPrv);
		this.templateProvider = templateProvider;
	}

	@Override
	protected Email generateEmail(Message message) throws EmailException {
		final HtmlEmail email = new HtmlEmail();

	    Map<String, Object> media = message.getMediaData();

        Map<String, Object> dataModel = CollectionUtils.newMap();

        Attachments attachments = (Attachments) media.get("attachments");
        if (attachments!=null) {
            dataModel.putAll(attachments.putIntoEmail(email));
        } else {
            dataModel.put("attachments", Collections.emptyList());
        }
            
        String messageContent = message.getContent();
        dataModel.put("message", messageContent);

		StringWriter out = new StringWriter();
        try {
            templateProvider.get().process(dataModel, out);
        } catch (TemplateException | IOException e) {
            throw new EmailException("Failed to process template", e);
        }

        email.setHtmlMsg(out.toString());

		return email;
	}
}
