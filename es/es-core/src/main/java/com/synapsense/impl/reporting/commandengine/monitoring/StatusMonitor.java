package com.synapsense.impl.reporting.commandengine.monitoring;

import com.synapsense.service.reporting.dto.CommandStatus;

public interface StatusMonitor {

	double getProgress();

	CommandStatus getStatus();

	void setProgress(double progress);

	void setStatus(CommandStatus status);
}
