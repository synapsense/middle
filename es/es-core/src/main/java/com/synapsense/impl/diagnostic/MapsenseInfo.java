package com.synapsense.impl.diagnostic;

import java.io.File;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Map.Entry;

import org.apache.log4j.Logger;

import com.synapsense.impl.diagnostic.logfinder.utils.DirHelper;
import com.synapsense.impl.utils.ConfigurationDataUtils;

public class MapsenseInfo {
	private static final Logger LOG = Logger.getLogger(MapsenseInfo.class);
	private static final String DL_FILE_EXTENSION_SUCCESS = "success";
	private static final String MAPSENSE_CONF_DATA_NAME = "MapSense";
	private static final String MAPSENSE_WORKING_DIR_PROPERTY_NAME = "MapSense_Home";
	private static final String MAPSENSE_JOURNAL_FOLDER = "journal";
	private static final String MAPSENSE_LOGS_FOLDER = "logs";

	public static String getMapsenseWorkingDir() {
		String mapsenseStorageHome = null;
		try {
			String configStr = ConfigurationDataUtils.getConfigurationString(MAPSENSE_CONF_DATA_NAME);
			if (configStr != null && !configStr.isEmpty()) {
				Properties props = new Properties();
				props.load(new StringReader(configStr));
				mapsenseStorageHome = props.getProperty(MAPSENSE_WORKING_DIR_PROPERTY_NAME);
			}
		} catch (Exception e) {
			LOG.error(e.getLocalizedMessage(), e);
		}
		return mapsenseStorageHome;
	}

	public static String getMapsenseLogsDir() {
		String mapsenseLogDir = null;
		String mapsenseStorageHome = getMapsenseWorkingDir();
		if (mapsenseStorageHome != null) {
			mapsenseLogDir = mapsenseStorageHome + File.separatorChar + MAPSENSE_LOGS_FOLDER;
		}
		return mapsenseLogDir;
	}

	public static String getMapsenseJournalDir() {
		String mapsenseJournalDir = null;
		String mapsenseStorageHome = getMapsenseWorkingDir();
		if (mapsenseStorageHome != null) {
			mapsenseJournalDir = mapsenseStorageHome + File.separatorChar + MAPSENSE_JOURNAL_FOLDER;
		}
		return mapsenseJournalDir;
	}

	public static List<String> getMapsenseLatestDlFiles() {
		List<String> latestDlFiles = new ArrayList<String>();
		String mapsenseJournalDir = getMapsenseJournalDir();
		LOG.info("Mapsense Journal dir: " + mapsenseJournalDir);

		if (mapsenseJournalDir != null) {
			Map<String, List<String>> filesMap = new HashMap<String, List<String>>();
			String[] dlFiles = DirHelper.getFileListing(mapsenseJournalDir, DL_FILE_EXTENSION_SUCCESS);

			if (dlFiles != null) {
				for (String fileName : dlFiles) {
					String shortFileName = getShortDlFileName(fileName);
					if (!filesMap.containsKey(shortFileName)) {
						filesMap.put(shortFileName, new ArrayList<String>());
					}
					filesMap.get(shortFileName).add(mapsenseJournalDir + File.separatorChar + fileName);
				}
			}

			for (Entry<String, List<String>> entry : filesMap.entrySet()) {
				List<String> fullFileNames = entry.getValue();
				if (!fullFileNames.isEmpty()) {
					File latestFile = new File(fullFileNames.get(0));
					for (int i = 1; i < fullFileNames.size(); i++) {
						File currentFile = new File(fullFileNames.get(i));
						if (latestFile.lastModified() < currentFile.lastModified()) {
							latestFile = currentFile;
						}
					}
					latestDlFiles.add(latestFile.getAbsolutePath());
				}
			}
		}

		return latestDlFiles;
	}

	private static String getShortDlFileName(String fileName) {
		String shortFileName = fileName;
		int numberOfPoints = 2;
		for (int i = 0; i < numberOfPoints; i++) {
			int indx = shortFileName.lastIndexOf('.');
			if (indx != -1) {
				shortFileName = shortFileName.substring(0, indx);
			} else {
				break;
			}
		}

		return shortFileName;
	}
}
