
package com.synapsense.impl.erlnode.commands;

import java.util.Map;

import com.ericsson.otp.erlang.OtpErlangBinary;
import com.ericsson.otp.erlang.OtpErlangObject;
import com.ericsson.otp.erlang.OtpErlangTuple;
import com.synapsense.impl.erlnode.ExceptionTypes;
import com.synapsense.impl.erlnode.utils.ToConverter;
import com.synapsense.exception.IllegalInputParameterException;

import com.synapsense.impl.erlnode.utils.StringConverter;

/**
 * @author gsmith
 * 
 */
public class GetObjectsByTypeCommand extends Command {

	public GetObjectsByTypeCommand(Map<String, Object> state) {
		super(state);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see Command#do_command(com.ericsson.otp.erlang.OtpErlangTuple)
	 */
	@Override
	public OtpErlangObject do_command(OtpErlangTuple command) throws Exception {
		OtpErlangObject result = null;

		OtpErlangObject typeName = command.elementAt(1);
		if (typeName == null || !(typeName instanceof OtpErlangBinary)) {
			return makeException(ExceptionTypes.ILLEGAL_ARGUMENTS, null);
		}

		String typeNameValue = StringConverter.utf8BinaryToString(typeName);
		try {
			result = makeOkResult(ToConverter.serializeToList(getEnv()
				.getObjectsByType(typeNameValue)));
		} catch (IllegalInputParameterException e) {
			result = makeException(ExceptionTypes.ILLEGAL_INPUT_PARAMETER, e.getMessage());
		}

		return result;
	}

}
