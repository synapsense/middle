package com.synapsense.impl.alerting.macros;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AlertIdMacro extends AbstractPatternMacro {

	private static final Pattern pattern = Pattern.compile("\\$(?i)alert_id\\$");

	@Override
	public String expandIn(String input, MacroContext ctx) {
		if (input == null)
			return null;
		if (ctx.alert == null)
			throw new IllegalArgumentException("MacroContext.alert must be not null");
		return super.expandIn(input, ctx);
	}

	@Override
	protected String expand(Matcher m, MacroContext ctx) {
		return ctx.alert.getAlertId().toString();
	}

	@Override
	protected Pattern getPattern() {
		return pattern;
	}

}
