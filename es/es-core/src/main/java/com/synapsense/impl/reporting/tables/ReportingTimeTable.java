package com.synapsense.impl.reporting.tables;

/**
 * The Interface <code>ReportingTimeTable</code> describes behavior for time reporting tables.
 */
public interface ReportingTimeTable extends ReportingTable {

	/**
	 * Gets the clause.
	 *
	 * @return the clause
	 */
	@Override
	String get();

}