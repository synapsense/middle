package com.synapsense.impl.reporting.commandengine.state;

public class CommandRemovableState extends CommandDefaultState {

	protected CommandRemovableState(String stateName) {
		super(stateName);
	}

	@Override
	public boolean removable() {
		return true;
	}
}
