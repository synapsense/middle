package com.synapsense.impl.alerting;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import com.synapsense.dto.Alert;
import com.synapsense.dto.AlertStatus;
import com.synapsense.dto.CollectionTO;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.impl.alerting.mapping.Converters;
import com.synapsense.service.Environment;
import com.synapsense.util.CollectionUtils;

class AlertStore implements Cache<Alert, TO<?>> {
	private Environment datasource;
	private Map<Integer, TO<?>> alertKey;
	private Map<TO<?>, Integer> toKey;
	private ReadWriteLock lock = new ReentrantReadWriteLock();

	public AlertStore(Environment datasource) {
		this.datasource = datasource;
		this.alertKey = new HashMap<Integer, TO<?>>(0);
		this.toKey = new HashMap<TO<?>, Integer>(0);
		fillAlertsMaps();
	}

	@Override
	public void put(Alert alert, TO<?> alertId) {
		int hash = calculateHash(alert);
		Lock l = lockOnWrite();
		try {
			alertKey.put(hash, alertId);
			toKey.put(alertId, hash);
		} finally {
			l.unlock();
		}
	}

	@Override
	public TO<?> get(Alert key) {
		int hash = calculateHash(key);
		Lock l = lockOnRead();
		try {
			final TO<?> alertOpened = alertKey.get(hash);
			return alertOpened;
		} finally {
			l.unlock();
		}
	}

	@Override
	public boolean remove(TO<?> value) {
		Lock l = lockOnWrite();
		try {
			Integer key = toKey.remove(value);
			if (key == null)
				return false;
			alertKey.remove(key);
			return true;
		} finally {
			l.unlock();
		}
	}

	public void clear() {
		Lock l = lockOnWrite();
		try {
			alertKey.clear();
			toKey.clear();
		} finally {
			l.unlock();
		}
	}

	private Lock lockOnRead() {
		Lock rwl = lock.readLock();
		rwl.lock();
		return rwl;
	}

	private Lock lockOnWrite() {
		Lock rwl = lock.writeLock();
		rwl.lock();
		return rwl;
	}

	private int calculateHash(Alert alert) {
		// done as separate function but may need refactoring ???
		final int prime = 31;
		int result = 1;
		result = prime * result + alert.getAlertType().hashCode();
		// if host TO is EMPTY , message is used in hashCode
		if (TOFactory.EMPTY_TO.equals(alert.getHostTO())) {
			result = prime * result + ((alert.getMessage() == null) ? 0 : alert.getMessage().hashCode());
		} else {
			// else , the hostTO is used in hashCode
			result = prime * result + alert.getHostTO().hashCode();
		}
		return result;
	}

	private void fillAlertsMaps() {
		Collection<TO<?>> alerts = datasource.getObjectsByType(AlertConstants.TYPE.ALERT_TYPE_NAME);
		Collection<CollectionTO> alertsProperties = datasource.getAllPropertiesValues(alerts);

		for (CollectionTO alertProps : alertsProperties) {
			TO<?> alertRef = alertProps.getObjId();
			Alert dto = CollectionUtils.map(alertProps.getPropValues(), Converters.propertiesToAlertConverter(alertRef, datasource));
			if (dto.getStatus() == AlertStatus.OPENED || dto.getStatus() == AlertStatus.ACKNOWLEDGED) {
				put(dto, alertProps.getObjId());
			}
		}
	}
}
