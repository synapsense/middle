
package com.synapsense.impl.erlnode.commands;

import java.util.Map;

import com.ericsson.otp.erlang.OtpErlangBoolean;
import com.ericsson.otp.erlang.OtpErlangObject;
import com.ericsson.otp.erlang.OtpErlangTuple;
import com.synapsense.impl.erlnode.ExceptionTypes;
import com.synapsense.impl.erlnode.utils.ToConverter;
import com.synapsense.dto.TO;
import com.synapsense.exception.IllegalInputParameterException;

/**
 * @author stikhon
 * 
 */
public class ExistsCommand extends Command {

	public ExistsCommand(Map<String, Object> state) {
		super(state);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see Command#do_command(com.ericsson.otp.erlang.OtpErlangTuple)
	 */
	@Override
	public OtpErlangObject do_command(OtpErlangTuple command) throws Exception {

		OtpErlangObject to = command.elementAt(1);
		if (to == null || !(to instanceof OtpErlangTuple)) {
			return makeException(ExceptionTypes.ILLEGAL_ARGUMENTS, null);
		}
		TO<?> toToCheck = ToConverter.parseTo((OtpErlangTuple) to);
		try {
			OtpErlangBoolean exists = new OtpErlangBoolean((getEnv()
					.exists(toToCheck)));
			return makeOkResult(exists);
		} catch (IllegalInputParameterException e) {
			return makeException(ExceptionTypes.ILLEGAL_ARGUMENTS, e
					.getMessage());
		}

	}

}
