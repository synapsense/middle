package com.synapsense.impl.alerting.macros;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.service.Environment;
import com.synapsense.util.Pair;
import com.synapsense.util.unitconverter.UnitSystem;

public class DimensionResolveMacro extends AbstractDataValueMacro {

	private static final Pattern TO_REF_PATTERN = Pattern.compile("(\\w+:\\d+)\\.");
	private static final Pattern PROP_REF_PATTERN = Pattern.compile("\\.(\\w+)");

	@Override
	public String expandIn(String input, MacroContext ctx) {
		if (input == null)
			return null;
		if (ctx.convEnv == null)
			throw new IllegalArgumentException("MacroContext.convEnv must be not null");
		return super.expandIn(input, ctx);
	}

	@Override
	protected String expand(Matcher m, MacroContext ctx) {
		if (m.group(3) != null) // dimension is already specified, nothing to do
			return "$0";

		String dimName = null;
		// try to find the dimension
		try {
			Pair<TO<?>, String> derefTO = dereferenceTO(m.group(1), ctx.convEnv.getProxiedEnvironment());
			UnitSystem baseSystem = ctx.convEnv.getBaseSystem(derefTO.getFirst(), derefTO.getSecond());
			dimName = ctx.convEnv.getDimension(derefTO.getFirst(), derefTO.getSecond(), baseSystem.getName()).getName();
		} catch (Exception e) {
			// TODO log it
			// logger.debug("Cannot find dimension for "+m.group(1), e);
		}

		if (dimName != null) {
			return "\\$data($1,$2," + Matcher.quoteReplacement(dimName) + ")\\$";
		} else {
			return "$0";
		}
	}

	private static Pair<TO<?>, String> dereferenceTO(String toPointer, Environment env) throws Exception {
		Matcher toRefMatcher = TO_REF_PATTERN.matcher(toPointer);
		TO<?> curTo = null;
		if (toRefMatcher.find()) {
			Matcher propNameMatcher = PROP_REF_PATTERN.matcher(toPointer);
			curTo = TOFactory.getInstance().loadTO(toRefMatcher.group(1));
			for (int searchPos = toRefMatcher.end() - 1; propNameMatcher.find(searchPos); searchPos = propNameMatcher
			        .end()) {
				if (!propNameMatcher.hitEnd()) {
					curTo = env.getPropertyValue(curTo, propNameMatcher.group(1), TO.class);
				} else {
					return new Pair<TO<?>, String>(curTo, propNameMatcher.group(1));
				}
			}
		}
		throw new IllegalArgumentException("Input string does not contain valid TO: " + toPointer);
	}

}
