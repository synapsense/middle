package com.synapsense.impl.erlnode.filters;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.ericsson.otp.erlang.OtpErlangList;
import com.ericsson.otp.erlang.OtpErlangObject;
import com.ericsson.otp.erlang.OtpErlangTuple;
import com.synapsense.impl.erlnode.commands.RegisterProcessorCommand;
import com.synapsense.impl.erlnode.utils.CollectionToConverter;
import com.synapsense.service.nsvc.Event;
import com.synapsense.service.nsvc.EventProcessor;
import com.synapsense.service.nsvc.events.ConfigurationCompleteEvent;
import com.synapsense.service.nsvc.events.ConfigurationStartedEvent;
import com.synapsense.service.nsvc.filters.EventTypeFilter;

public class EventTypeFilterParser implements FilterDefParser {

	private static final Logger log = Logger.getLogger(OrFilterParser.class);

	private static final Map<String, Class<? extends Event>> classes = new HashMap<String, Class<? extends Event>>();
	static {
		classes.put("configuration_started_event", ConfigurationStartedEvent.class);
		classes.put("configuration_complete_event", ConfigurationCompleteEvent.class);
	}

	@Override
	public EventProcessor parse(OtpErlangTuple filterDef) {
		OtpErlangObject tmp = filterDef.elementAt(1);
		if (tmp == null || RegisterProcessorCommand.undefined.equals(tmp)) {
			throw new IllegalArgumentException("The second parameter of event_type_filter must be a list of classes");
		}
		OtpErlangList classDefinitions = CollectionToConverter.checkList(tmp);
		if (classDefinitions == null) {
			throw new IllegalArgumentException(
			        "The second parameter of event_type_filter must be a list of classes but was"
			                + filterDef.elementAt(1).getClass().getName());
		}

		List<Class<? extends Event>> types = new ArrayList<Class<? extends Event>>();

		for (OtpErlangObject subFilterDef : classDefinitions.elements()) {
			types.add(classes.get(subFilterDef.toString()));
		}

		log.debug("returning EventTypeFilter");
		return new EventTypeFilter(types);
	}
}
