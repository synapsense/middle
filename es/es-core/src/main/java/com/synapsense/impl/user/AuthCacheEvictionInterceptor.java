package com.synapsense.impl.user;

import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;
import javax.management.MBeanServerConnection;
import javax.management.ObjectName;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;

import org.apache.log4j.Logger;

public class AuthCacheEvictionInterceptor {

	private final static Logger log = Logger.getLogger(AuthCacheEvictionInterceptor.class);

	@AroundInvoke
	public Object execute(InvocationContext icx) throws Exception {
		Object result = icx.proceed();
		evictUsersFromCache();
		return result;
	}

	private void evictUsersFromCache() {
		String urlString = System.getProperty("jmx.service.url", "service:jmx:remoting-jmx://localhost:9999");
		JMXServiceURL serviceURL;
		try {
			serviceURL = new JMXServiceURL(urlString);
			JMXConnector jmxConnector = JMXConnectorFactory.connect(serviceURL, null);
			MBeanServerConnection connection = jmxConnector.getMBeanServerConnection();

			ObjectName SynapServer = new ObjectName("jboss.as:subsystem=security,security-domain=SynapServer");
			connection.invoke(SynapServer, "flushCache", null, null);

			jmxConnector.close();
		} catch (Exception e) {
			log.warn("Can't flush the authentication cache", e);
		}
	}
}
