package com.synapsense.impl.reporting.queries.types;

import com.synapsense.impl.reporting.queries.TableProcessor.*;
import com.synapsense.service.reporting.Cell;

import java.util.List;

public interface QueryTypeClauseBuilder {

    String build(QueryClause queryClause, List<Cell> valueIdCells);

}
