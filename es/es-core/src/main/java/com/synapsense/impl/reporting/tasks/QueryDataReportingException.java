package com.synapsense.impl.reporting.tasks;

import javax.ejb.ApplicationException;

import com.synapsense.impl.reporting.dao.DAOReportingRuntimeException;

@ApplicationException(rollback = false)
public class QueryDataReportingException extends DAOReportingRuntimeException {

	private static final long serialVersionUID = 8747290048154526670L;

	public QueryDataReportingException() {
	}

	public QueryDataReportingException(String message, Throwable cause) {
		super(message, cause);
	}

	public QueryDataReportingException(String message) {
		super(message);
	}

	public QueryDataReportingException(Throwable cause) {
		super(cause);
	}
}
