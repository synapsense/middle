package com.synapsense.impl.taskscheduler.tasks;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import com.synapsense.dto.PropertyDescr;
import com.synapsense.dto.TO;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.exception.PropertyNotFoundException;
import com.synapsense.exception.UnableToConvertPropertyException;
import com.synapsense.impl.utils.CommonUtils;
import com.synapsense.service.Environment;

/**
 * Class implements status checker behavior. It takes all objects of the
 * specified type and tries to check their status. At least one inactive child
 * object means that parent is active.
 * 
 */
public class ChildrenStatusChecker implements StatusChecker {
	private final static Logger logger = Logger.getLogger(ChildrenStatusChecker.class);
	public static final int GATEWAY_STATUS_ALIVE = 1;
	public static final int GATEWAY_STATUS_SYNC = 4;

	private Environment env;
	private String objType;

	private static Map<String, Boolean> typesMap = new HashMap<String, Boolean>();

	public ChildrenStatusChecker(String objType) {
		this.objType = objType;
	}

	@Override
	public void check(Environment env) {
		this.env = env;
		Collection<TO<?>> objects = env.getObjectsByType(objType);
		for (TO<?> to : objects) {
			try {
				int status = getChildrenStatus(to);

				if (CommonUtils.nullSafeInt(env.getPropertyValue(to, "status", Integer.class)) != status) {
					env.setPropertyValue(to, "status", status);
				}

			} catch (Exception e) {
				logger.error(e.getLocalizedMessage(), e);
			}
		}
	}

	private int getChildrenStatus(TO<?> to) throws ObjectNotFoundException {
		int status = 1;
		Collection<TO<?>> children = env.getChildren(to);

		for (TO<?> child : children) {
			String childType = child.getTypeName();
			if (hasStatusProperty(childType)) {
				try {
					status = CommonUtils.nullSafeInt(env.getPropertyValue(child, "status", Integer.class));
				} catch (PropertyNotFoundException e) {
					logger.error(e.getMessage());
				} catch (UnableToConvertPropertyException e) {
					logger.error(e.getMessage());
				}

				if ((status == 0)
				        || (childType.equals("WSNGATEWAY") && !(((status & GATEWAY_STATUS_ALIVE) > 0) && (status & GATEWAY_STATUS_SYNC) > 0))) {
					return 0;
				}
			}
		}

		return 1;
	}

	private boolean hasStatusProperty(String type) {

		Boolean res = typesMap.get(type);
		if (res != null) {
			return res;
		}

		boolean result = false;
		Set<PropertyDescr> descrs = env.getObjectType(type).getPropertyDescriptors();
		for (PropertyDescr descr : descrs) {
			if ("status".equals(descr.getName())) {
				result = true;
				break;
			}
		}

		typesMap.put(type, result);
		return result;
	}
}
