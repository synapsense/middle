package com.synapsense.impl.alerting.notification;

import java.util.regex.Pattern;

import com.synapsense.service.UserManagementService;

public class Destinations {
	private static final Pattern rfc2822 = Pattern
	        .compile("^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$");

	public static Destination newDestination(final String destAsString, final UserManagementService userService) {
		if (destAsString == null) {
			throw new IllegalArgumentException();
		}

		if (destAsString.isEmpty()) {
			throw new IllegalArgumentException();
		}

		if (rfc2822.matcher(destAsString).matches()) {
			return new PlainDestination(destAsString);
		}

		if (destAsString.contains(":")) {
			return new PlainDestination(destAsString);
		}

		return new UserDestination(destAsString, userService);
	}

	private Destinations() {
	}
}