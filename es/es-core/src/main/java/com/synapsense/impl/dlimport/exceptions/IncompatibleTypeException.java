package com.synapsense.impl.dlimport.exceptions;

public class IncompatibleTypeException extends RuntimeException {
	private static final long serialVersionUID = 1811553379165967090L;

	public IncompatibleTypeException(String _msg) {
		super(_msg);
	}
}
