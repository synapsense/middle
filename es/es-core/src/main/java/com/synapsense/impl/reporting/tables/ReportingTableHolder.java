package com.synapsense.impl.reporting.tables;

import java.util.Map;
import java.util.Set;

import com.synapsense.service.reporting.olap.Hierarchy;
import com.synapsense.service.reporting.olap.custom.Cube;
import com.synapsense.util.CollectionUtils;

public class ReportingTableHolder {
	
	
	Map<ReportingHelperTableType, ReportingTable> reportingTables;
	
	
	
	public ReportingTableHolder() {
		this.reportingTables = CollectionUtils.newMap();
		initTables();
	}

	private void initTables() {
		
		reportingTables.put(ReportingHelperTableType.LEVEL_VALUES_TIME_TABLE, new LevelValuesTimeTable());
		reportingTables.put(ReportingHelperTableType.MEMBERS_TIME_TABLE, new MembersTimeTable());
		reportingTables.put(ReportingHelperTableType.RANGE_MEMBER_TIME_TABLE, new RangeMemberTimeTable());
		reportingTables.put(ReportingHelperTableType.RANGES_TIME_TABLE, new RangesTimeTable());
		reportingTables.put(ReportingHelperTableType.OBJECT_TYPE_TABLE, new ObjectTypeTable());
		reportingTables.put(ReportingHelperTableType.PROPERTY_TYPE_TABLE, new PropertyTypeTable());
				
	}

	public ReportingTable put(ReportingHelperTableType tableType, ReportingTable table) {
		return reportingTables.put(tableType, table);
	}	
	
	public ReportingTable get(ReportingHelperTableType tableType) {
		return reportingTables.get(tableType);
	}
	
	public static enum ReportingHelperTableType {
		LEVEL_VALUES_TIME_TABLE("LevelValuesTimeTable", Cube.TIME)
		, MEMBERS_TIME_TABLE("MembersTimeTable", Cube.TIME)
		, RANGE_MEMBER_TIME_TABLE("RangeMemberTimeTable", Cube.TIME)
		, RANGES_TIME_TABLE("RangesTimeTable", Cube.TIME)
		, OBJECT_TYPE_TABLE("ObjectTypeTable", Cube.OBJECT)
		, PROPERTY_TYPE_TABLE("PropertyTypeTable", Cube.PROPERTY)
		, VALUE_ID_ATTRIBUTES_TABLE("ValueIdAttributesTable", Cube.PROPERTY), 
		SLICER_OBJECT_TYPE_TABLE("SlicerObjectTypeTable", Cube.OBJECT);
		
		private ReportingHelperTableType(String name, Hierarchy hierarchy) {
			this.name = name;
			this.hierarchy = hierarchy;
		}

		private String name;
		private Hierarchy hierarchy;
		
		public static Set<ReportingHelperTableType> getTypesByHierarchy(Hierarchy hierarchy) {
			Set<ReportingHelperTableType> hierarchyTables = CollectionUtils.newSet();
			for (ReportingHelperTableType type : ReportingHelperTableType.values()) {
				if (type.getHierarchy().equals(hierarchy))
					hierarchyTables.add(type);
			}
		return hierarchyTables;
	}
		
		public String getName() {
			return name;
		}

		public Hierarchy getHierarchy() {
			return hierarchy;
		}		
		
	}

	public Set<ReportingTable> getReportingTimeTables() {
		Set<ReportingTable> result = CollectionUtils.newSet();
		for (ReportingHelperTableType type : ReportingHelperTableType.getTypesByHierarchy(Cube.TIME)) {
			result.add(get(type));
		}
		return result;
	}

}
