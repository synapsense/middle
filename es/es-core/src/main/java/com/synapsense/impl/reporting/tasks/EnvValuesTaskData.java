package com.synapsense.impl.reporting.tasks;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;
import com.synapsense.impl.reporting.queries.types.ReportingQueryType;
import com.synapsense.service.reporting.Cell;
import com.synapsense.util.CollectionUtils;
import com.synapsense.util.unitconverter.UnitConverter;

public class EnvValuesTaskData {
	
	private String envDataQuery;	
	
	private List<Cell> valueIdCells;

	private List<UnitConverter> unitConverters;	

	private Map<String, Set<Cell>> formulaCellsMap;
	
	private List<Cell> cells;

	private ListMultimap<Integer, Integer> cellObjectMap;

	private ListMultimap<String, Integer> cellPropertyMap;
	
	private ReportingQueryType queryType;

	public String getEnvDataQuery() {
		return envDataQuery;
	}

	public List<Cell> getValueIdCells() {
		return valueIdCells;
	}

	public List<UnitConverter> getUnitConverters() {
		return Collections.unmodifiableList(unitConverters);
	}

	public Map<String, Set<Cell>> getFormulaCellsMap() {
		return Collections.unmodifiableMap(formulaCellsMap);
	}

	public List<Cell> getDataSet() {
		return cells;
	}
	
	public ListMultimap<Integer, Integer> getCellObjectMap() {
		return this.cellObjectMap;
	}
	
	public ListMultimap<String, Integer> getCellPropertyMap() {
		return cellPropertyMap;
	}
	
	public ReportingQueryType getQueryType() {
		return queryType;
	}

	private EnvValuesTaskData(EnvValuesTaskDataBuilder builder) {
		this.envDataQuery = builder.getEnvDataQuery();
		this.valueIdCells = builder.getValueIdCells();
		this.unitConverters = builder.getUnitConverters();
		this.formulaCellsMap = builder.getFormulaCellsMap();
		this.cells = builder.getCells();
		this.cellObjectMap = builder.getCellObjectMap();
		this.cellPropertyMap = builder.getCellPropertyMap();
		this.queryType = builder.getQueryType();
	}
	
	public static class EnvValuesTaskDataBuilder {

		private String envDataQuery = "";	
		
		private List<Cell> valueIdCells = CollectionUtils.newList();

		private List<UnitConverter> unitConverters = CollectionUtils.newList();	

		private Map<String, Set<Cell>> formulaCellsMap = CollectionUtils.newMap();
		
		private List<Cell> cells = CollectionUtils.newList();

		private ListMultimap<Integer, Integer> cellObjectMap = ArrayListMultimap.create();
		
		private ListMultimap<String, Integer> cellPropertyMap = ArrayListMultimap.create();
		
		private ReportingQueryType queryType;

        public EnvValuesTaskDataBuilder() {
        }
        
        public EnvValuesTaskDataBuilder(EnvValuesTaskData envValuesTaskData) {
    		this.envDataQuery = envValuesTaskData.getEnvDataQuery();
    		this.valueIdCells = envValuesTaskData.getValueIdCells();
    		this.unitConverters = envValuesTaskData.getUnitConverters();
    		this.formulaCellsMap = envValuesTaskData.getFormulaCellsMap();
    		this.cells = envValuesTaskData.getDataSet();
    		this.cellObjectMap = envValuesTaskData.getCellObjectMap();
    		this.cellPropertyMap = envValuesTaskData.getCellPropertyMap();
    		this.queryType = envValuesTaskData.getQueryType();
        }
        
        public EnvValuesTaskData build() {
            return new EnvValuesTaskData(this);
        }
        
        public EnvValuesTaskDataBuilder envDataQuery(String envDataQuery) {
        	this.envDataQuery = envDataQuery;
        	return this;
        }

        public EnvValuesTaskDataBuilder valueIdCells(List<Cell> valueIdCells) {
        	this.valueIdCells.addAll(valueIdCells);
        	return this;
        }
        
        public EnvValuesTaskDataBuilder unitConverters(List<UnitConverter> unitConverters) {
        	this.unitConverters.addAll(unitConverters);
        	return this;
        }

        public EnvValuesTaskDataBuilder formulaCellsMap(Map<String, Set<Cell>> formulaCellsMap) {
        	this.formulaCellsMap.putAll(formulaCellsMap);
        	return this;
        }
        
        public EnvValuesTaskDataBuilder cells(List<Cell> cells) {
        	this.cells.addAll(cells);
        	return this;
        }
        
        public EnvValuesTaskDataBuilder cellObjectMap(ListMultimap<Integer, Integer> cellObjectMap) {
        	this.cellObjectMap.putAll(cellObjectMap);
        	return this;
        }
        
        public EnvValuesTaskDataBuilder cellPropertyMap(ListMultimap<String, Integer> cellPropertyMap) {
        	this.cellPropertyMap.putAll(cellPropertyMap);
        	return this;
        }
        
		public EnvValuesTaskDataBuilder queryType(ReportingQueryType queryType) {
			this.queryType = queryType;
			return this;
		}
		
		String getEnvDataQuery() {
			return envDataQuery;
		}

		List<Cell> getValueIdCells() {
			return valueIdCells;
		}

		List<UnitConverter> getUnitConverters() {
			return unitConverters;
		}

		Map<String, Set<Cell>> getFormulaCellsMap() {
			return formulaCellsMap;
		}

		List<Cell> getCells() {
			return cells;
		}

		ListMultimap<Integer, Integer> getCellObjectMap() {
			return cellObjectMap;
		}

		ListMultimap<String, Integer> getCellPropertyMap() {
			return cellPropertyMap;
		}

		ReportingQueryType getQueryType() {
			return queryType;
		}

	}

}
