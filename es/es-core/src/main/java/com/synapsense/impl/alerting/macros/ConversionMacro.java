package com.synapsense.impl.alerting.macros;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import com.synapsense.util.unitconverter.ConverterException;
import com.synapsense.util.unitconverter.Dimension;
import com.synapsense.util.unitconverter.UnitConverter;

public class ConversionMacro extends AbstractPatternMacro {

	private static final Logger log = Logger.getLogger(ConversionMacro.class);

	private static final Pattern pattern = Pattern
	        .compile("\\$(?i)conv\\((-?\\d+(?:\\.\\d+)?)\\,((?:\\w+\\s*){1,})\\)\\$");

	static final String OUT_OF_RANGE_NUMBER_REPLACEMENT = "x";

	public static void main(String[] args) {
		String inputString = "$conv(23.5,my dim)$";
		Matcher m = pattern.matcher(inputString);
		if (m.find()) {
			System.out.println(m.group(1));
			System.out.println(m.group(2));
		} else {
			System.out.println("pattern doesn't match");
		}

	}

	@Override
	public String expandIn(String input, MacroContext ctx) {
		if (input == null)
			return null;
		if (ctx.usrCtx == null)
			throw new IllegalArgumentException("MacroContext.userCtx must be not null");
		if (ctx.unitSystems == null)
			throw new IllegalArgumentException("MacroContext.unitSystems must be not null");
		return super.expandIn(input, ctx);
	}

	@Override
	protected String expand(Matcher m, MacroContext ctx) {
		String replacementToReturn = m.group(1);
		String dimensionName = m.group(2);
		try {
			final double toDouble = Double.parseDouble(replacementToReturn);
			if (Double.isNaN(toDouble) || toDouble <= -1000)
				return DEFAULT_EXPANSION;
			try {
				final String targetUnitSystem = ctx.usrCtx.getUnitSystem();
				// lazy getter because this call is expensive
				if (!"Server".equals(targetUnitSystem)) {
					final UnitConverter conv = ctx.unitSystems.getUnitConverter("Server", targetUnitSystem,
					        dimensionName);
					replacementToReturn = ctx.usrCtx.getDataFormat().format(conv.convert(toDouble));
				} else {
					replacementToReturn = ctx.usrCtx.getDataFormat().format(toDouble);
				}

				Dimension dimension = ctx.unitSystems.getSimpleDimensions().get(dimensionName);
				if (dimension == null) {
					dimension = ctx.unitSystems.getUnitSystem(targetUnitSystem).getDimension(dimensionName);
				}

				if (dimension != null) {
					String units = dimension.getUnits();
					try {
						units = (units.isEmpty()) ? units : ctx.localizationService.getString(dimension.getUnits(),
                                ctx.usrCtx.getLocale());
					} catch (NullPointerException e) {
						log.info("NPE breakpoint");
						throw e;
					}

					if (units != null && !units.isEmpty()) {
						replacementToReturn += " " + units;
					}
				}
			} catch (final ConverterException e) {
				log.warn("Macro " + m.group() + " contains invalid dimension expression \"" + m.group(2) + "\"", e);
			}
		} catch (final NumberFormatException e) {
			log.warn("Macro " + m.group() + ": only doubles are supported for conversion but was "
			        + replacementToReturn, e);
		}

		return Matcher.quoteReplacement(replacementToReturn);
	}

	@Override
	protected Pattern getPattern() {
		return pattern;
	}

}
