package com.synapsense.impl.erlnode.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.ericsson.otp.erlang.OtpErlangAtom;
import com.ericsson.otp.erlang.OtpErlangBinary;
import com.ericsson.otp.erlang.OtpErlangDouble;
import com.ericsson.otp.erlang.OtpErlangInt;
import com.ericsson.otp.erlang.OtpErlangList;
import com.ericsson.otp.erlang.OtpErlangLong;
import com.ericsson.otp.erlang.OtpErlangObject;
import com.ericsson.otp.erlang.OtpErlangRangeException;
import com.ericsson.otp.erlang.OtpErlangTuple;

import com.synapsense.dto.PropertyDescr;
import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;
import com.synapsense.dto.ObjectType;
import com.synapsense.exception.IllegalInputParameterException;

import com.synapsense.service.Environment;

import com.synapsense.impl.erlnode.utils.StringConverter;

/**
 * @author stikhon
 * 
 */
public class ValueTOConverter {

	public final static String VALUE_TO_ATOM_ID = "value_to";
	public final static OtpErlangAtom nullAtom = new OtpErlangAtom("null");

	public synchronized static String lookupPropertyType(ObjectType objType, String propertyName) {

		PropertyDescr descr = objType.getPropertyDescriptor(propertyName);
		if (descr == null) {
			throw new IllegalInputParameterException("Cannot find property [" + propertyName + "] in object type ["
			        + objType.getName() + " ].");
		}
		return descr.getTypeName();
	}

	public static OtpErlangTuple serializeValueTO(ValueTO valueTO) {
		if (valueTO == null) {
			throw new IllegalArgumentException();
		}

		OtpErlangObject valueObj = null;
		OtpErlangTuple result;

		Object value = valueTO.getValue();
		if (value instanceof Integer) {
			valueObj = new OtpErlangInt(((Integer) value).intValue());
		} else if (value instanceof Long) {
			valueObj = new OtpErlangLong(((Long) value).longValue());
		} else if (value instanceof Double) {
			valueObj = new OtpErlangDouble(((Double) value).doubleValue());
		} else if (value instanceof String) {
			valueObj = StringConverter.stringToUtf8Binary((String) value);
		} else if (value instanceof TO<?>) {
			valueObj = ToConverter.serializeTo((TO<?>) value);
		} else if (value instanceof Collection) {
			ArrayList<OtpErlangTuple> toList = new ArrayList<OtpErlangTuple>();
			for (Object link : (Collection<?>) value) {
				if (!(link instanceof TO<?>)) {
					throw new IllegalArgumentException("I support only collection of TOs!");
				}
				toList.add(ToConverter.serializeTo((TO<?>) link));
			}
			valueObj = new OtpErlangList(toList.toArray(new OtpErlangTuple[toList.size()]));
		}
		if (valueObj != null) {
			result = new OtpErlangTuple(
			        new OtpErlangObject[] { new OtpErlangAtom(VALUE_TO_ATOM_ID),
			                StringConverter.stringToUtf8Binary(valueTO.getPropertyName()), valueObj,
			                new OtpErlangLong(valueTO.getTimeStamp()) });
		} else {
			result = new OtpErlangTuple(new OtpErlangObject[] { new OtpErlangAtom(VALUE_TO_ATOM_ID),
			        StringConverter.stringToUtf8Binary(valueTO.getPropertyName()), nullAtom,
			        new OtpErlangLong(valueTO.getTimeStamp()) });
		}

		return result;

	}

	public static ValueTO parseValueTO(ObjectType ot, OtpErlangTuple tuple) throws OtpErlangRangeException {
		if (tuple == null || tuple.arity() < 3 || !(VALUE_TO_ATOM_ID.equals(tuple.elementAt(0).toString()))
		        || !(tuple.elementAt(1) instanceof OtpErlangBinary)) {
			throw new IllegalArgumentException();
		}

		String propertyName = StringConverter.utf8BinaryToString(tuple.elementAt(1));

		long timestamp = 0L;
		if (tuple.elementAt(3) instanceof OtpErlangLong) {
			timestamp = ((OtpErlangLong) tuple.elementAt(3)).longValue();
		}
		String wantType = lookupPropertyType(ot, propertyName);
		OtpErlangObject value = tuple.elementAt(2);
		return new ValueTO(propertyName, valueObject(value, wantType), timestamp);
	}

	public static Object valueObject(OtpErlangObject value, String wantType) {
		try {
			if (value instanceof OtpErlangBinary && wantType.equals("java.lang.String"))
				return StringConverter.utf8BinaryToString(value);
			else if (value instanceof OtpErlangInt && wantType.equals("java.lang.Integer"))
				return ((OtpErlangInt) value).intValue();
			else if (value instanceof OtpErlangInt && wantType.equals("java.lang.Long"))
				return ((OtpErlangInt) value).longValue();
			else if (value instanceof OtpErlangLong && wantType.equals("java.lang.Integer"))
				return ((OtpErlangLong) value).intValue();
			else if (value instanceof OtpErlangLong && wantType.equals("java.lang.Long"))
				return ((OtpErlangLong) value).longValue();
			else if (value instanceof OtpErlangDouble && wantType.equals("java.lang.Double"))
				return ((OtpErlangDouble) value).doubleValue();
			else if (value instanceof OtpErlangDouble && wantType.equals("java.lang.Float"))
				return ((OtpErlangDouble) value).floatValue();
			else if (value instanceof OtpErlangAtom && value.equals(nullAtom))
				return null;
			else if (value instanceof OtpErlangTuple && wantType.equals(TO.class.getName())
			        && ((OtpErlangTuple) value).arity() == 2
			        && ((OtpErlangTuple) value).elementAt(0).equals(ToConverter.toAtom))
				return ToConverter.parseTo((OtpErlangTuple) value);
			else
				throw new IllegalArgumentException();
		} catch (OtpErlangRangeException e) {
			throw new IllegalArgumentException();
		}
	}

	public static OtpErlangList serializeValueTOList(Collection<ValueTO> list) {
		if (list == null) {
			throw new IllegalArgumentException();
		}
		List<OtpErlangTuple> objectsList = new ArrayList<OtpErlangTuple>();
		for (ValueTO valueTO : list) {
			objectsList.add(serializeValueTO(valueTO));
		}
		return new OtpErlangList(objectsList.toArray(new OtpErlangObject[objectsList.size()]));

	}

	public static Collection<ValueTO> parseValueTOList(Environment env, String objType, OtpErlangList list) {
		if (list == null) {
			throw new IllegalArgumentException();
		}
		int size = list.arity();
		Collection<ValueTO> valueTOs = new ArrayList<ValueTO>(size);
		ObjectType ot = env.getObjectType(objType);
		if (ot == null) {
			throw new IllegalInputParameterException("Cannot find object type [" + objType + " ].");
		}
		for (int i = 0; i < size; i++) {
			try {
				valueTOs.add(parseValueTO(ot, (OtpErlangTuple) list.elementAt(i)));
			} catch (OtpErlangRangeException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return valueTOs;
	}
}
