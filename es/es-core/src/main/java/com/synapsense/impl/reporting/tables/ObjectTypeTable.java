package com.synapsense.impl.reporting.tables;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.synapsense.impl.reporting.queries.AxisProcessor.ReportingTypeTable;
import com.synapsense.service.reporting.olap.Member;
import com.synapsense.service.reporting.olap.Tuple;
import com.synapsense.service.reporting.olap.TupleSet;
import com.synapsense.service.reporting.olap.custom.Level;
import com.synapsense.util.CollectionUtils;

/**
 * The <code>ObjectTypeTable</code> class contains Object Types Map
 * 
 * @author Grigory Ryabov
 */
public class ObjectTypeTable implements ReportingTypeTable{

	private static final long serialVersionUID = 8587508754719401068L;
	
	Map<String, List<Tuple>> objectTypesTable;
	
	public ObjectTypeTable() {
		objectTypesTable = new LinkedHashMap<String, List<Tuple>>();
	}	
	
	@SuppressWarnings("unchecked")
	@Override
	public Map<String, TupleSet> get() {
		
		Map<String, TupleSet> result = CollectionUtils.newMap();
		for (Entry<String, List<Tuple>> entry : objectTypesTable.entrySet()){
			List<Tuple> values = entry.getValue();
			TupleSet set = new TupleSet(values.toArray(new Tuple[values.size()]));
			result.put(entry.getKey(), set);
		}
		return result;
		
	}
	
	public void add(Member object) {
		
		String objectType = object.getLevel();
		if (objectTypesTable.containsKey(objectType)){
			objectTypesTable.get(objectType).add(object);
		} else {
			List<Tuple> objects = CollectionUtils.newList();
			objects.add(object);
			objectTypesTable.put(objectType, objects);
		}
		
	}

	public int size() {
		return objectTypesTable.size();
	}

	@Override
	public boolean isEmpty() {
		return size() == 0 ? true : false;
	}
	
	@Override
	public ObjectTypeTable getTable() {
		
		ObjectTypeTable copy = new ObjectTypeTable();
		for (Entry<String, List<Tuple>> entry : objectTypesTable.entrySet()) {
			copy.put(entry.getKey(), entry.getValue());
		}
		return copy;
		
	}
	
	private void put(String key, List<Tuple> value) {
		objectTypesTable.put(key, value);
	}

	@Override
	public Level getLowestLevel() {
		return null;
	}

}
