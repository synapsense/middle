package com.synapsense.impl.nsvc;

import javax.ejb.Remote;

@Remote
public interface NotificationServiceMgmt {

	void create() throws Exception;

	void start() throws Exception;

	void stop() throws Exception;

	void destroy() throws Exception;

	void setIdlePeriod(String compressorName, long period);

	long getIdlePeriod(String compressorName);

	long getNumOfEventsReceived(String compressorName);

	long getNumOfEventsSent(String compressorName);

	void resetStatistics(String compressorName);

	String listCompressors();
}
