package com.synapsense.impl.utils;

public class CommonUtils {

	public static boolean nullSafeBoolean(Boolean b) {
		return b == null ? false : b;
	}

	public static int nullSafeInt(Integer i) {
		return i == null ? 0 : i;
	}

}
