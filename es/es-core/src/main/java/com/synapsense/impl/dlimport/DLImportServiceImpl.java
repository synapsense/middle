package com.synapsense.impl.dlimport;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.*;
import java.util.Map.Entry;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.DependsOn;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.Remote;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.apache.log4j.Logger;

import com.synapsense.cdi.MBean;
import com.synapsense.dal.objectcache.CacheEnvironmentDispatcher;
import com.synapsense.dal.objectcache.CacheSvcImpl;
import com.synapsense.dto.BinaryData;
import com.synapsense.dto.CRERule;
import com.synapsense.dto.DLImportError;
import com.synapsense.dto.DLImportResult;
import com.synapsense.dto.DLObjectTypeImportResult;
import com.synapsense.dto.EnvObject;
import com.synapsense.dto.EnvObjectBundle;
import com.synapsense.dto.ObjectType;
import com.synapsense.dto.PropertyDescr;
import com.synapsense.dto.Relation;
import com.synapsense.dto.RuleType;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOBundle;
import com.synapsense.dto.TOFactory;
import com.synapsense.dto.Tag;
import com.synapsense.dto.TagDescriptor;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.DLImportException;
import com.synapsense.exception.EnvException;
import com.synapsense.exception.IllegalInputParameterException;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.impl.activitylog.ActivityLogger;
import com.synapsense.service.DLImportService;
import com.synapsense.service.Environment;
import com.synapsense.service.RulesEngine;
import com.synapsense.service.impl.activitylog.ActivityLogService;
import com.synapsense.service.impl.activitylog.ActivityRecord;
import com.synapsense.service.impl.dao.to.BinaryDataImpl;
import com.synapsense.service.impl.dao.to.PropertyTO;
import com.synapsense.util.LocalizationUtils;


@Startup @Singleton(name="DLImportService") @Remote(DLImportService.class)
@DependsOn("SynapCache")
@ConcurrencyManagement(ConcurrencyManagementType.BEAN)
@MBean("com.synapsense:type=DLImportService")
public class DLImportServiceImpl implements DLImportService, DLImportServiceImplMBean {

	private final static Logger log = Logger.getLogger(DLImportServiceImpl.class);

	@Resource
	private EJBContext ctx;

	private final static String ActivityLogModule = "MapSense";
	private final static String ActivityLogAction = "UpdateConfiguration";

	private Environment env;
	private RulesEngine rulesEngine;
	private ActivityLogService als;

	/** Map of DLID : TO */
	private Map<String, TO<?>> DLIDToKey = new HashMap<String, TO<?>>();

	/** Map of parent DLID : List of child DLIDs **/
	private Map<String, List<String>> addChildren = new HashMap<String, List<String>>();
	/** Map of parent DLID : List of child DLIDs ro remove **/
	private Map<String, List<String>> removeChildren = new HashMap<String, List<String>>();
	private List<DockingpointBundle> dockingPoints = new ArrayList<DockingpointBundle>();
	private List<CollectionBundle> collections = new ArrayList<CollectionBundle>();
	private List<PierBundle> piers = new ArrayList<PierBundle>();
	private List<CREBundle> allRules = new ArrayList<CREBundle>();
	private List<TO<?>> deletedObjects = new ArrayList<TO<?>>();
	// to support bulk create objects
	private EnvObjectBundle objectsInProject;
	private List<String> allProjectRootDLIDs;
	private List<TO<?>> allProjectRootTOs;

	// Bug[9484]: properties that should take into history at first import
	private static final String[] PROP_TO_RESET = new String[]{"x", "y", "width", "height"};
	private Map<String, List<ValueTO>> propToResetMap = new HashMap<>();

	@Override
	@PostConstruct
	public void create() throws Exception {
		env = CacheEnvironmentDispatcher.getEnvironment(CacheSvcImpl.getInstance(), env);
		log.info("MapSense import service created");
		start();
	}

	@Override
	public void start() throws Exception {
		log.info("MapSense import service started");
	}

	@Override
	@PreDestroy
	public void stop() {
		log.info("MapSense import service stopped");
		destroy();
	}

	@Override
	public void destroy() {
		log.info("MapSense import service destroyed");
	}

	public Environment getEnvironment() {
		return env;
	}

	@EJB(beanName="Environment")
	public void setEnvironment(Environment env) {
		this.env = env;
	}

	public RulesEngine getRulesEngine() {
		return rulesEngine;
	}

	@EJB
	public void setRulesEngine(RulesEngine rulesEngine) {
		this.rulesEngine = rulesEngine;
	}

	@EJB
	public void setAls(ActivityLogService als) {
		this.als = als;
	}

	private void recordActivityLog(TO<?> objTO, String message) {
		// install a record in the Activity Log
		try {
			String userName = ctx.getCallerPrincipal().getName();
			ActivityRecord ar = new ActivityRecord(userName, ActivityLogModule, ActivityLogAction, message);
			if (objTO != null) {
				ar.setObject(objTO);
			}
			als.addRecord(ar);
		} catch (Exception t) {
			log.warn("Cannot log activity", t);
		}
	}

	/**
	 * Convert a zip file into a hash of the contents
	 *
	 * @param config
	 *            a byte[] holding a zipped file
	 * @return a map of entry name : byte[] of contents
	 * @throws DLImportException
	 */
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	private Map<String, byte[]> parseConfiguration(byte[] config) throws DLImportException {
		log.info("Loading configuration");

		Calendar start = Calendar.getInstance();

		Map<String, byte[]> res = new HashMap<String, byte[]>();

		// ZipInputStream zis = new ZipInputStream(new
		// ByteArrayInputStream(config));
		int numberOfBytesToBeRead = 1024 * 500;
		ZipInputStream zis = new ZipInputStream(new BufferedInputStream(new ByteArrayInputStream(config),
		        numberOfBytesToBeRead));

		ZipEntry zipEntry;
		try {
			while ((zipEntry = zis.getNextEntry()) != null) {
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				int b;
				// while ((b = zis.read()) != -1) {
				// baos.write(b);
				// }

				// unloading the streams this way goes MUCH faster
				byte[] buf = new byte[4096];
				int r;
				while ((r = zis.read(buf)) != -1) {
					baos.write(buf, 0, r);
				}

				byte[] buffer = baos.toByteArray();
				log.info("Unzipped " + zipEntry.getName() + "(" + buffer.length + " bytes)");
				// TODO: dump to disk?
				res.put(zipEntry.getName(), buffer);
			}
		} catch (IOException e) {
			throw new DLImportException(e.getMessage(), e);
		}

		Calendar stop = Calendar.getInstance();
		double time = ((stop.getTimeInMillis() - start.getTimeInMillis()) / 1000.0);
		log.trace("unzip time was sec=" + time);

		return res;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Collection<DLObjectTypeImportResult> importObjectTypes(String typeDefinitions) throws DLImportException {
		Collection<DLObjectTypeImportResult> results = new ArrayList<DLObjectTypeImportResult>();
		log.trace("DLIS: Importing Object Types");
		// make sure the RTI is empty for this export
		ruleTypeIndex = new HashMap<String, TO<?>>();

		// System.out.println("IMPORTING OBJECT TYPES!!!!111");
		try {
			XMLInputFactory inputFactory = XMLInputFactory.newInstance();
			InputStream input = new ByteArrayInputStream(typeDefinitions.getBytes("UTF-8"));

			XMLStreamReader parser = inputFactory.createXMLStreamReader(input);

			while (parser.hasNext()) {
				int event = parser.next();
				int inObject = 0;

				switch (event) {
				case XMLStreamConstants.START_ELEMENT:
					if (isObject(parser.getLocalName())) {
						DLObjectTypeImportResult result;
						result = parseObject(parser);
						results.add(result);
					}
					break;
				case XMLStreamConstants.END_ELEMENT:
					if (isObject(parser.getLocalName())) {
						inObject--;
					}
					break;
				/*
				 * case XMLStreamConstants.CHARACTERS: if (inObject > 0){
				 * System.out.print(parser.getText()); } break;
				 */
				} // end switch

			}

		} catch (UnsupportedEncodingException uex) {
			throw new DLImportException("Error while parsing XML definitions encoding format", uex);
		} catch (XMLStreamException ex) {
			throw new DLImportException("Error while parsing XML definitions", ex);
		} catch (Exception ex) {
			throw new DLImportException("Totally unexpected error!", ex);
		}

		// ruleTypeIndex = new HashMap<String, TO<?>>(); //clear it out after
		// we're done

		return results;
	}

	/**
	 * Checks if the current xml element is an object tag
	 *
	 * @param name
	 * @return
	 */
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	private static boolean isObject(String name) {
		if (name.equalsIgnoreCase("object")) {
			return true;
		}
		return false;
	}

	/**
	 * Parses a single object type from the definitions xml file and
	 * creates/updates that type Assumed to be called from the outer loop in
	 * importObjectTypes()
	 *
	 * @param parser
	 *            the STaX parser containing the definitions
	 * @return DLObjectTypeImportResult telling us how this went
	 * @throws DLImportException
	 *             if something goes terribly wrong (generally, the method
	 *             should just return a result obejct with it's complaints if
	 *             the error is recoverable.)
	 *
	 * @see DLImportServiceImpl#importObjectTypes(String)
	 *
	 */
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	private DLObjectTypeImportResult parseObject(XMLStreamReader parser) throws DLImportException {
		// /log.trace("DLIS: parseObject()");

		// we know we're in an object, so:
		String objectName = "";

		if (parser.getAttributeLocalName(0).equalsIgnoreCase("type")) {
			objectName = parser.getAttributeValue(0).toUpperCase();
		}

		// read the object properties
		HashMap<String, PropertyDescr> properties = new HashMap<String, PropertyDescr>();
		List<TagDescriptor> currentTags = new ArrayList<TagDescriptor>();

		String currentPropertyType = "";

		String currentPropertyName = "";
		String currentValueType = "";
		Boolean currentHistoric = false;
		String currentSchedule = "";
		String currentRuleClass = "";
		String currentRuleFile = "";

		try {

			while (parser.hasNext()) {
				int event = parser.next();

				switch (event) {
				case XMLStreamConstants.START_ELEMENT:

					if (parser.getLocalName().equalsIgnoreCase("property")) {

						currentPropertyName = "";
						currentValueType = "";
						currentHistoric = false;
						currentSchedule = "";
						currentRuleClass = "";
						currentRuleFile = "";

						currentPropertyType = parser.getAttributeValue(0);

					} else if (parser.getLocalName().equalsIgnoreCase("name")) {
						currentPropertyName = parser.getElementText();

					} else if (parser.getLocalName().equalsIgnoreCase("historic")) {
						currentHistoric = Boolean.parseBoolean(parser.getElementText());

					} else if (parser.getLocalName().equalsIgnoreCase("valuetype")) {
						currentValueType = parser.getElementText();

					} else if (parser.getLocalName().equalsIgnoreCase("schedule")) {
						currentSchedule = parser.getElementText();
					} else if (parser.getLocalName().equalsIgnoreCase("ruleclass")) {
						currentRuleClass = parser.getElementText();

					} else if (parser.getLocalName().equalsIgnoreCase("rulefile")) {
						// currentRuleFile = URLDecoder.decode(
						// parser.getElementText(), "UTF-8" );
						currentRuleFile = parser.getElementText();

					} else if (parser.getLocalName().equalsIgnoreCase("customtag")) {
						// <customtag name="pluginId" tagType="java.lang.String"
						// >SNMP</customtag>
						TagDescriptor tagDesc = new TagDescriptor(parser.getAttributeValue(0),
						        parser.getAttributeValue(1));
						currentTags.add(tagDesc);
						// / log.info("adding tag " +
						// parser.getAttributeValue(0) + " to prop " +
						// currentPropertyName );
					}
					break;

				case XMLStreamConstants.END_ELEMENT:

					if (parser.getLocalName().equalsIgnoreCase("property")) {
						if (currentPropertyType.equalsIgnoreCase("setting")) {
							properties.put(currentPropertyName, new PropertyDescr(currentPropertyName,
							        currentValueType, currentHistoric));
						} else if (currentPropertyType.equalsIgnoreCase("dockingpoint")) {
							properties.put(currentPropertyName,
							        new PropertyDescr(currentPropertyName, TO.class.getName()));

						} else if (currentPropertyType.equalsIgnoreCase("pier")) {
							properties.put(currentPropertyName,
							        new PropertyDescr(currentPropertyName, PropertyTO.class.getName()));

						} else if (currentPropertyType.equalsIgnoreCase("rule")) {
							properties.put(currentPropertyName, new PropertyDescr(currentPropertyName,
							        currentValueType, currentHistoric));
							// also, build the java rule type itself

							if (!ruleTypeIndex.containsKey(currentRuleClass)) {
								RulesEngine re = this.getRulesEngine();
								TO<?> ruleTO = re.getRuleTypeId(currentRuleClass);
								RuleType javaRule;
								if (currentSchedule.length() > 0) {
									javaRule = new RuleType(currentRuleClass, null, "Java", true, currentSchedule);
								} else {
									javaRule = new RuleType(currentRuleClass, null, "Java", true);
								}
								if (ruleTO != null) {
									re.updateRuleType(ruleTO, javaRule);
								} else {
									ruleTO = re.addRuleType(javaRule);
								}
								ruleTypeIndex.put(currentRuleClass, ruleTO);
							}

						} else if (currentPropertyType.equalsIgnoreCase("groovyrule")) {
							properties.put(currentPropertyName, new PropertyDescr(currentPropertyName,
							        currentValueType, currentHistoric));

							if (!ruleTypeIndex.containsKey(currentRuleClass)) {
								RulesEngine re = this.getRulesEngine();
								TO<?> ruleTO = re.getRuleTypeId(currentRuleClass);
								RuleType gRule;
								if (currentSchedule.length() > 0) {
									gRule = new RuleType(currentRuleClass, currentRuleFile, "Groovy", false,
									        currentSchedule);
								} else {
									gRule = new RuleType(currentRuleClass, currentRuleFile, "Groovy", false);
								}
								if (ruleTO != null) {
									log.trace("Updating rule type " + currentRuleClass + " from source");
									re.updateRuleType(ruleTO, gRule);
								} else {
									log.trace("Adding new rule type " + currentRuleClass);
									ruleTO = re.addRuleType(gRule);
								}
								ruleTypeIndex.put(currentRuleClass, ruleTO);
							}

						} else if (currentPropertyType.equalsIgnoreCase("collection")) {
							properties.put(currentPropertyName,
							        new PropertyDescr(currentPropertyName, TO.class.getName(), false, true));
						}

						// all properties can have tags now
						if (currentTags.size() > 0) {
							for (TagDescriptor tag : currentTags) {
								properties.get(currentPropertyName).getTagDescriptors().add(tag);
							}
						}
						currentTags.clear();

					}

					if (isObject(parser.getLocalName())) {

						ObjectType type = env.getObjectType(objectName);
						if (type == null) {
							log.trace("DLIS: Type Created: " + objectName);
							type = env.createObjectType(objectName);

							if (properties.size() > 0) {
								type.getPropertyDescriptors().addAll(properties.values());
								env.updateObjectType(type);
							} else {
								log.error("Creating nonsense type '" + objectName + "' with no properties");
							}
						} else {
							log.trace("DLIS: Type Updated " + objectName);
							Set<PropertyDescr> descrs = type.getPropertyDescriptors();
							descrs.clear();
							descrs.addAll(properties.values());
							env.updateObjectType(type);
						}
						return new DLObjectTypeImportResult(objectName);
					}
					break;
				} // end switch
			}

		} catch (Exception e) {
			throw new DLImportException("Couldn't create/update type " + objectName, e);
		}
		return new DLObjectTypeImportResult(objectName, new DLImportException("Logic Error"));
	}

	/**
	 * Imports a project from a bytestream containing a zipped dl file.
	 */
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	@Interceptors(ActivityLogger.class)
	public Collection<DLImportResult> importObjectInstancesZipped(byte[] dlzFile, HashMap<String, String> controlOptions)
	        throws DLImportException {

		// the bytestream is a a dlz file
		// open it up, find the file, and move on down
		Map<String, byte[]> fileContents = this.parseConfiguration(dlzFile);

		String dlFileName = "";
		for (String s : fileContents.keySet()) {
			if (s.contains(".xml")) {
				dlFileName = s;
			}
		}

		String objectXML;
		try {
			objectXML = new String(fileContents.get(dlFileName), "utf-8");
		} catch (UnsupportedEncodingException e) {
			throw new DLImportException(e);
		}

		Collection<DLImportResult> results = this.importObjectInstances(objectXML, controlOptions);

		// now! Go back and slug in those images!
		log.trace("Setting images from file");
		for (String s : fileContents.keySet()) {

			if (s.contains(".png")) {
				log.trace("Installing image " + s);

				// propname is always "image". All images will be named after their DLID
				String currentDLID = s.replace(".png", "").replace("_", ":");
				try {
					// log.trace( DLIDToKey.toString() );
					// log.trace("attempting to find TO for dlid " + currentDLID);
					TO<?> to = DLIDToKey.get(currentDLID);
					// log.trace("and that TO is " + to.toString());
					BinaryData bd = new BinaryDataImpl(fileContents.get(s));
					List<ValueTO> image = new ArrayList<ValueTO>();
					image.add(new ValueTO("image", bd));
					env.setAllPropertiesValues(to, image, new ArrayList<Tag>());
				} catch (Exception ex) {
					log.trace("Exception!" + ex.toString());
					ex.printStackTrace();

					DLImportResult r = this.findResult(results, currentDLID);
					DLImportError err = new DLImportError(currentDLID, "Unable to set image!", ex);
					r.addError(err);
				}
			}
		}

		//Bug[9484]: reset necessary props to put its to history
		for(String currentDLID : propToResetMap.keySet()){
			List<ValueTO> propToReset = propToResetMap.get(currentDLID);
			if (propToReset != null && !propToReset.isEmpty()){
				log.debug("Looking for id for "+currentDLID);
				TO<?> to = DLIDToKey.get(currentDLID);
				try {
					log.debug("Set properties for "+to.toString());
					env.setAllPropertiesValues(to, propToReset, new ArrayList<Tag>());
				} catch (Exception ex) {
					log.trace("Exception!" + ex.toString());
					ex.printStackTrace();

					DLImportResult r = this.findResult(results, currentDLID);
					DLImportError err = new DLImportError(currentDLID, "Unable to set Room Image!", ex);
					r.addError(err);
				}
			}
		}

		DLIDToKey = null;
		propToResetMap = null;
		log.trace("DLIS: Export completed");

		String exporterName = controlOptions.get("username") + "@" + controlOptions.get("hostname");
		// Log for each project root
		for (TO<?> key : allProjectRootTOs ) {
			recordActivityLog(
			        key,
			        LocalizationUtils.getLocalizedString("activity_record_project_file_exported",
			                dlFileName.replace(".xml", ""), exporterName));
		}
		return results;
	}

	/**
	 * Searches a Result list for the result belonging to a specific dlid. Why
	 * yes, I DID get tired of typing this.
	 *
	 * @param results
	 *            a Collection of DLImportResult objects to search
	 * @param dlid
	 *            the id to look for
	 * @return the DLImportResult asked for, or null if none were found.
	 */
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	private DLImportResult findResult(Collection<DLImportResult> results, String dlid) {
		DLImportResult target = null;
		for (DLImportResult r : results) {
			if (r.getDlid().equalsIgnoreCase(dlid)) {
				target = r;
				break;
			}
		}
		return target;
	}

	/**
	 * Find the corresponding key for an entry in the map. Used for the few
	 * cases we need to use the DLID->Key map backwards.
	 *
	 * @param <T>
	 * @param <E>
	 * @param map
	 * @param value
	 * @return
	 */
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	private static <T, E> Set<T> getKeysByValue(Map<T, E> map, E value) {
		Set<T> keys = new HashSet<T>();
		for (Entry<T, E> entry : map.entrySet()) {
			if (entry.getValue().equals(value)) {
				keys.add(entry.getKey());
			}
		}
		return keys;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public Collection<DLImportResult> importObjectInstances(String objectXML) throws DLImportException {
		return this.importObjectInstances(objectXML, new HashMap<String, String>());
	}

	/**
	 * Expected controlOptions include:
	 *
	 * refreshAllLinks T/F
	 *
	 * refreshAllRules T/F
	 *
	 * refreshAllObjects T/F
	 *
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public Collection<DLImportResult> importObjectInstances(String objectXML, HashMap<String, String> controlOptions)
	        throws DLImportException {
		log.trace("DLIS: Begin project export");

		log.trace("DLIS Control Flags: " + controlOptions.toString());

		// state vars that track the entire project
		DLIDToKey = new HashMap<String, TO<?>>();
		addChildren = new HashMap<String, List<String>>();
		removeChildren = new HashMap<String, List<String>>();
		dockingPoints = new ArrayList<DockingpointBundle>();
		collections = new ArrayList<CollectionBundle>();
		piers = new ArrayList<PierBundle>();
		allRules = new ArrayList<CREBundle>();
		deletedObjects = new ArrayList<TO<?>>();
		objectsInProject = new EnvObjectBundle();
		allProjectRootDLIDs = new ArrayList<String>();
		propToResetMap = new HashMap<String, List<ValueTO>>();

		log.trace("importObjectInstances()");

		// map the result objects to the DLID of the object from mapsense
		Map<String, DLImportResult> results = new HashMap<String, DLImportResult>();

		// this does most of the work, populates the vars above and gets us a
		// base result set to work on
		List<DLImportResult> createResults = createAndUpdate(objectXML);

		for (DLImportResult dlir : createResults) {
			// log.trace("Storing result for lookup: key= '" + dlir.getKey() +
			// "' result= " + dlir.toString() );
			results.put(dlir.getDlid(), dlir);
		}

		// bulk create all objects:
		if (!objectsInProject.isEmpty()) {
			log.trace("Bulk create all objects");
			TOBundle allKeys;
			try {
				allKeys = env.createObjects(objectsInProject);
			} catch (EnvException ee) {
				log.fatal("Could not create any objects at all!");
				throw new DLImportException(ee);
			}
			for (String dlid : allKeys.keySet()) {
				TO<?> to = allKeys.get(dlid);
				DLImportResult result = results.get(dlid);
				/*
				 * *************************************************************************
				 *
				 * if ( result == null ){
				 * log.warn("had to whip up a result for dlid " + dlid +
				 * " with a TO of " + to.toString() );
				 *
				 * result = new DLImportResult(); result.setDlid(dlid);
				 * results.put(dlid, result); }
				 */
				// Now store the TO : DLID mapping as soon as we can
				DLIDToKey.put(dlid, to);
				if (to != null) {
					result.setKey(to);
					result.setTagsSuccessful(true);
				}
			}
		}

		allProjectRootTOs = new ArrayList<TO<?>>( allProjectRootDLIDs.size());
		for (String d : allProjectRootDLIDs ) {
			allProjectRootTOs.add( DLIDToKey.get( d ) );
		}

		log.trace("Looking up Root Object");
		List<TO<?>> roots = new ArrayList<TO<?>>(env.getObjectsByType("ROOT"));
		TO<?> rootTO = roots.get(0);
		if (log.isTraceEnabled()) {
			log.trace("root to is " + rootTO.toString());
			log.trace("ABOUT TO START WITH THE RELATIONS");
			log.trace("DLIDToKey: " + DLIDToKey.toString());
			log.trace("addChildren: " + addChildren.toString());
			log.trace("removeChildren: " + removeChildren.toString());
		}

		// rip though all the new parent-child relationships
		List<Relation> newRelations = new ArrayList<Relation>();
		// in case there are more than one project root objects, make sure they're all
		// children of the root object
		for (TO<?> key : allProjectRootTOs ) {
			try {
				if (!env.getChildren(rootTO).contains(key)) {
					// only child the object to the root if we haven't already
					Relation rootRelation = new Relation(rootTO, key);
					log.trace("rootRelation is " + rootRelation.toString());
					newRelations.add(rootRelation);
				}
			} catch (ObjectNotFoundException e) {
				throw new DLImportException(e);
			} catch (IllegalInputParameterException e) {
				throw new DLImportException(e);
			}
		}

		for (String dlid : addChildren.keySet()) {
			TO<?> currentTO = DLIDToKey.get(dlid);
			for (String childDLID : addChildren.get(dlid)) {
				// / log.trace("Attempting to construct new relation parent=" +
				// currentTO.toString() + " child=" + DLIDToKey.get( childDLID
				// ));
				try {
					newRelations.add(new Relation(currentTO, DLIDToKey.get(childDLID)));
				} catch (Exception e) {
					log.warn("Bad relation " + currentTO.toString() + "[" + dlid + "] -> " +
					         DLIDToKey.get( childDLID ) +  "[" + childDLID + "]: " + e.getMessage());
					results.get(dlid).setRelationsSuccessful(false);
				}
			}
		}

		if (newRelations.size() > 0) {
			try {
				log.trace("Setting Relations: " + newRelations);
				env.setRelation(newRelations);
				for (Relation r : newRelations) {
					String parentDLID = "";
					for (Map.Entry<String, TO<?>> e : DLIDToKey.entrySet()) {
						if (e.getValue().equals(r.getParent())) {
							parentDLID = e.getKey();
							break;
						}
					}
					DLImportResult currentResult = results.get(parentDLID);
					if (currentResult == null) {

						if (r.getParent().toString().contains("ROOT")) {
							log.trace("No result for root object, but the child to root operation worked.");
						} else {
							log.error("WOAH!  I don't have a result for key " + r.getParent().toString());
						}

					} else {
						// /
						// log.trace("About to set this result as being successful in its relationships: "
						// + currentResult.toString());
						currentResult.setRelationsSuccessful(true);
					}
				}

			} catch (Exception e) {
				for (Map.Entry<String, DLImportResult> ir : results.entrySet()) {
					DLImportError dlex = new DLImportError(ir.getValue().getDlid(), e.getMessage());
					dlex.setCause(e);
					ir.getValue().addError(dlex);
					ir.getValue().setRelationsSuccessful(false);
				}
			}
		}

		// remove old parent-child relations
		log.trace("Beginning to remove old relations");
		log.trace("deleted objects= " + deletedObjects.toString());
		List<Relation> oldRelations = new ArrayList<Relation>();
		for (String dlid : removeChildren.keySet()) {
			if (removeChildren.get(dlid).size() == 0) {
				continue;
			}

			log.trace("looking for children to remove from " + dlid);
			TO<?> currentTO = DLIDToKey.get(dlid);
			log.trace(dlid + " has a TO of " + currentTO.toString());
			for (String childDLID : removeChildren.get(dlid)) {
				log.trace("looking to remove child " + childDLID);
				TO<?> formerChildStar = DLIDToKey.get(childDLID);
				if (formerChildStar != null) {
					log.trace("Former child star = " + formerChildStar.toString());
					// don't bother removing relations about deleted objects
					if (!deletedObjects.contains(formerChildStar)) {
						oldRelations.add(new Relation(currentTO, formerChildStar));
					}
				} else {
					log.error("Attempted to unchild nonexistant object with dlid " + childDLID + " from "
					        + currentTO.toString());
				}
			}
		}

		if (oldRelations.size() > 0) {
			try {
				log.trace("Removing old relations: " + oldRelations);
				env.removeRelation(oldRelations);

				for (Relation r : oldRelations) {
					// find dlid for key of parent
					// /<String,TO<?>>
					// Set<String> possibles = getKeysByValue(DLIDToKey,
					// r.getParent());
					// String dlidForKey = (String) possibles.toArray()[0];

					String parentDLID = "";
					for (Map.Entry<String, TO<?>> e : DLIDToKey.entrySet()) {
						if (e.getValue().equals(r.getParent())) {
							parentDLID = e.getKey();
							break;
						}
					}
					DLImportResult currentResult = results.get(parentDLID);

					// if ( results.containsKey(r.getParent())){
					if (currentResult == null) {
						results.get(currentResult).setRelationsSuccessful(true);
					} else {
						log.trace("No Result for parent in " + r.toString());
					}

				}

			} catch (Exception e) {
				for (Map.Entry<String, DLImportResult> ir : results.entrySet()) {
					DLImportError dlex = new DLImportError(ir.getValue().getDlid(), e.getMessage());
					dlex.setCause(e);
					ir.getValue().addError(dlex);
					ir.getValue().setRelationsSuccessful(false);
				}
			}
		}

		log.trace("ABOUT TO LAY IN POINTERS");
		log.trace("collections: " + collections.toString());
		log.trace("dockpingpoints: " + dockingPoints.toString());
		log.trace("piers: " + piers.toString());

		for (CollectionBundle cb : collections) {
			// / log.trace("Setting Collection Property: " + cb);

			List<TO<?>> currentItemsAsTOs = new ArrayList<TO<?>>();
			for (String s : cb.getTargets()) {
				currentItemsAsTOs.add(DLIDToKey.get(s));
			}

			try {
				env.setPropertyValue(DLIDToKey.get(cb.getDlid()), cb.getPropertyName(), currentItemsAsTOs);
				// / log.trace("collection linksSuccessful on to=" +
				// (DLIDToKey.get(cb.getDlid())) );
			} catch (Exception e) {
				// /
				// results.get(DLIDToKey.get(cb.getDlid())).setLinksSuccessful(false);
				results.get(cb.getDlid()).setLinksSuccessful(false);

				DLImportError dlex = new DLImportError(cb.getDlid(), e.getMessage());
				dlex.setCause(e);
				results.get(cb.getDlid()).addError(dlex);
			}
		}

		for (DockingpointBundle db : dockingPoints) {
			// / log.trace("Setting Dockingpoint Property: " + db);
			try {
				env.setPropertyValue(DLIDToKey.get(db.getDlid()), db.getPropertyName(), DLIDToKey.get(db.getTarget()));
				// / log.trace("dockingpoint linksSuccessful on to=" +
				// (DLIDToKey.get(db.getDlid())) );
			} catch (Exception e) {
				results.get(db.getDlid()).setLinksSuccessful(false);
				DLImportError dlex = new DLImportError(db.getDlid(), e.getMessage());
				dlex.setCause(e);
				results.get(db.getDlid()).addError(dlex);

			}
		}

		for (PierBundle pb : piers) {
			try {
				// log.trace("Pier( [" +
				// DLIDToKey.get(pb.getTargetDLID()).toString() + "] " +
				// pb.toString() );

				//PropertyTO currentPier = new PropertyTO(DLIDToKey.get(pb.getTargetDLID()), pb.getTargetProperty());
				PropertyTO currentPier = null;
				if(DLIDToKey.get(pb.getTargetDLID()) != null){
					currentPier = new PropertyTO(DLIDToKey.get(pb.getTargetDLID()), pb.getTargetProperty());
				}
				env.setPropertyValue(DLIDToKey.get(pb.getDlid()), pb.getPropertyName(), currentPier);
			} catch (Exception e) {
				results.get(pb.getDlid()).setLinksSuccessful(false);
				DLImportError dlex = new DLImportError(pb.getDlid(), e.getMessage());
				dlex.setCause(e);
				results.get(pb.getDlid()).addError(dlex);
			}
		}

		// cache the valid TOs:instanceName per rule type
		List<String> cachedRuleTypes = new ArrayList<String>();
		// Map<String,TO<?>> cachedRuleInstances = new HashMap<String,TO<?>>()
		// List<CRERule> cachedRuleInstances = new ArrayList<CRERule>();

		// map [rule type name : <all CRERules of that type>]
		Map<String, Collection<CRERule>> cachedRuleInstances = new HashMap<String, Collection<CRERule>>();

		log.trace("RTI is :");
		log.trace(ruleTypeIndex.toString());

		RulesEngine re = this.getRulesEngine();
		// CRE, please add the rules. Please.
		log.trace("Adding rules...");
		for (CREBundle rb : allRules) {
			// CRERule r = rb.makeRule(DLIDToKey.get(rb.getDlid()), re);
			// TO<?> ruleTO = re.getRuleId( r.getName() );

			if (!cachedRuleTypes.contains(rb.getRuleTypeName())) {
				// getAllWorld()
				Collection<CRERule> currentRules = null;
				try {
					log.trace("Looking up instances for rule type '" + rb.getRuleTypeName() + "'");
					// currentRules = re.getRulesByType(rb.getRuleTypeName());
					// this way is slightly faster
					currentRules = re.getRulesByType(ruleTypeIndex.get(rb.getRuleTypeName()));

					log.trace(currentRules.size() + " instances found.");

				} catch (ObjectNotFoundException enfe) {
					log.trace("Rule type not defined " + rb.getRuleTypeName());

				} catch (Exception e) {
					log.error("Rule problem!");
					log.error(e.getClass().getName());
					log.error(e.getMessage());
					log.error("Stack Trace:", e);
				}
				// cachedRuleInstances.addAll(currentRules);
				cachedRuleInstances.put(rb.getRuleTypeName(), currentRules);
				cachedRuleTypes.add(rb.getRuleTypeName());
			}

			boolean itExists = false;

			if (cachedRuleInstances.containsKey(rb.getRuleTypeName())
			        && cachedRuleInstances.get(rb.getRuleTypeName()) != null) {
				String ruleTypeName = rb.getRuleTypeName();
				for (CRERule r : cachedRuleInstances.get(ruleTypeName)) {
					if (r.getName().equalsIgnoreCase(rb.getRuleInstanceName(DLIDToKey.get(rb.getDlid())))) {
						itExists = true;
						break;
					}
				}
			}

			if (itExists && controlOptions.containsKey("refreshAllRules")) {
				if (Boolean.parseBoolean(controlOptions.get("refreshAllRules")) == true) {
					// delete the rule instance
					log.trace("force RefreshAllRules: delete rule "
					        + rb.getRuleInstanceName(DLIDToKey.get(rb.getDlid())));
					re.removeRule(rb.getRuleInstanceName(DLIDToKey.get(rb.getDlid())));
					// do we need to replace the rule in the local cache?
					itExists = false;
				}
			}

			// TO<?> ruleTO =
			// re.getRuleId(rb.getRuleInstanceName(DLIDToKey.get(rb.getDlid())));
			// if (ruleTO == null) {
			if (!itExists) {

				try {
					CRERule r = rb.makeRule(DLIDToKey.get(rb.getDlid()), re);
					// /log.trace("adding rule: " + r.getName() + " to=" +
					// r.getDestObjId() );

					// mapsense is in charge of NOT telling us about bogus
					// rules, so we assume all of these are okay
					log.trace("adding rule: " + r.getName() + " to=" + r.getDestObjId());
					re.addRule(r);
				} catch (Exception e) {
					log.error("DLIS: rule construction/binding failed: " + e.getMessage());
					// /DLImportResult currentResult = results.get(
					// r.getDestObjId() );
					DLImportResult currentResult = results.get(rb.getDlid());
					DLImportError err = new DLImportError(currentResult.getDlid(), "Could not bind rule "
					        + rb.getRuleInstanceName(DLIDToKey.get(rb.getDlid())));
					err.setCause(e);
					currentResult.addError(err);
					currentResult.setRulesSuccessful(false);
				}
			}
		}

		addChildren = null;
		removeChildren = null;
		dockingPoints = null;
		collections = null;
		allRules = null;
		deletedObjects = null;
		objectsInProject = null;
		// log.trace("DLIS: Export completed");

		Collection<DLImportResult> finalResults = new ArrayList<DLImportResult>();
		finalResults.addAll(results.values());
		return (finalResults);
	}

	/**
	 * Takes a string holding the objects section of a DL file and uses that to
	 * update the object model
	 *
	 * @param objectXML
	 *            a String holding the objects serialized to XML
	 * @return a DLImportResult with a map of DLID:TO and / or any errors
	 * @throws DLImportException
	 *             if anything untoward happens. Any other exceptions should be
	 *             wrapped in a DLImportException for serialization to
	 *             DeploymentLab (or other clients.)
	 */
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	private List<DLImportResult> createAndUpdate(String objectXML) throws DLImportException {
		log.trace("createAndUpdate()");
		List<DLImportResult> createResult = new ArrayList<DLImportResult>();
		try {
			XMLInputFactory inputFactory = XMLInputFactory.newInstance();
			InputStream input = new ByteArrayInputStream(objectXML.getBytes("UTF-8"));
			XMLStreamReader parser = inputFactory.createXMLStreamReader(input);
			while (parser.hasNext()) {
				int event = parser.next();
				switch (event) {
				case XMLStreamConstants.START_ELEMENT:
					if (isObject(parser.getLocalName())) {
						String currentObjectType = parser.getAttributeValue(0);
						DLImportResult currentResult = createObject(parser, currentObjectType);
						createResult.add(currentResult);
					} else {
						log.trace("Bypassing a START_ELEMENT in outer switch: '" + parser.getLocalName() + "'");
					}
					break;

				case XMLStreamConstants.END_ELEMENT:

					if (parser.getLocalName().equalsIgnoreCase("objects")) {
						log.trace("Reached end of objects section, done with XML");
						return (createResult);
					}

					log.trace("Bypassing an END_ELEMENT in outer switch: '" + parser.getLocalName() + "'");
					break;

				// case XMLStreamConstants.CHARACTERS:
				// log.warn("Bypassing CHARACTERS in outer switch" );
				// log.info( parser.getText() );
				// break;

				case XMLStreamConstants.END_DOCUMENT:
					log.trace("XML: END DOCUMENT");
					break;

				default:
					// log.warn("Bypassing an element in outer switch " + event
					// );
					break;

				} // end switch
			}

		} catch (UnsupportedEncodingException uex) {
			throw new DLImportException("Error while parsing XML definitions encoding format", uex);
		} catch (XMLStreamException ex) {
			throw new DLImportException("Error while parsing XML definitions", ex);
		} catch (Exception ex) {

			log.error("Unexpected error: " + ex.getClass().getName() + " " + ex.getMessage());

			throw new DLImportException("Totally unexpected error!", ex);
		}

		return (createResult);
	}

	/**
	 * Takes the StAX stream and creates or updates a single object
	 *
	 *
	 * todo: transaction support
	 *
	 *
	 * note: this method no longer throws DLImportException, it is responsible
	 * for always returning a DLImportResult
	 *
	 * @param parser
	 * @param currentObjectType
	 * @return a DLImportResult of how this object did
	 *
	 */
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	private DLImportResult createObject(XMLStreamReader parser, String currentObjectType) {
		// log.trace("createObject( type = " + currentObjectType + " ) start");
		DLImportResult result = new DLImportResult();

		// context:
		TO<?> to = null;
		String dlid = "";
		String currentPropertyType = "";
		String currentPropertyName = "";
		Object currentPropertyValue = "";
		Object currentPropertyOldValue = "";
		Object currentPropertyOverride = null;
		String currentValueType = "";
		Boolean currentDeleted = false;
		Boolean currentDirty = false;

		Boolean newChildren = true;
		List<String> currentChildren = new ArrayList<String>();
		List<String> oldChildren = new ArrayList<String>();
		List<String> currentItems = new ArrayList<String>();

		List<CollectionBundle> currentCollections = new ArrayList<CollectionBundle>();
		List<DockingpointBundle> currentDockingpoints = new ArrayList<DockingpointBundle>();
		List<PierBundle> currentPiers = new ArrayList<PierBundle>();

		String currentRuleTypeName = "";
		List<CREBundle> currentRuleBundles = new ArrayList<CREBundle>();
		List<Tag> currentTags = new ArrayList<Tag>();
		Map<String, String> currentBindings = new HashMap<String, String>(5);

		int deleteLevel = 0;

		List<ValueTO> updates = new ArrayList<ValueTO>();
		List<ValueTO> propToReset = new ArrayList<ValueTO>();
		try {
			while (parser.hasNext()) {
				int event = parser.next();
				switch (event) {
				case XMLStreamConstants.START_ELEMENT:
					if (isObject(parser.getLocalName())) {
						currentObjectType = parser.getAttributeValue(0);

					} else if (parser.getLocalName().equalsIgnoreCase("property")) {
						currentPropertyName = parser.getAttributeValue(0);
						currentPropertyType = parser.getAttributeValue(1);

						// if ( ! currentPropertyType.equals("childlink") ){
						// childlinks we ignore here, we just care about their
						// subtags (see below)

						if (currentPropertyType.equals("setting")) {
							currentValueType = parser.getAttributeValue(2);

							currentPropertyOverride = null; // scrub out any
							                                // overrides

							// /look up valuetype from typedef:
							// /log.trace("looking up propDescr for object type "
							// + currentObjectType.toUpperCase() + " prop " +
							// currentPropertyName + " of ptype " +
							// currentPropertyType);
							// /PropertyDescr pd =
							// env.getPropertyDescriptor(currentObjectType.toUpperCase(),
							// currentPropertyName);
							// /currentValueType = pd.getTypeName();

							// note: including the valuetype in the XML is
							// amazingly faster

							// /log.trace("typename is " + currentValueType);
						} else {
							// just to make sure something wacky doesn't end up
							// in there.
							currentValueType = "";
						}

						if (currentPropertyType.equalsIgnoreCase("collection")
						        || currentPropertyType.equalsIgnoreCase("dockingpoint")
						        || currentPropertyType.equalsIgnoreCase("pier")) {
							currentDirty = Boolean.parseBoolean(parser.getAttributeValue(3));
						} else {
							currentDirty = false;
						}

						// we can just wrap dockingpoints now
						if (currentPropertyType.equalsIgnoreCase("dockingpoint")) {
							String target = parser.getElementText().trim();

							// if ( currentDirty && target != null &&
							// target.length() > 0){
							if (currentDirty) {
								DockingpointBundle db = new DockingpointBundle("", currentPropertyName, target);
								currentDockingpoints.add(db);
							}
						}

						// also piers
						if (currentPropertyType.equalsIgnoreCase("pier")) {
							String targetPropName = parser.getAttributeValue(4);
							String target = parser.getElementText().trim();

							// log.trace("Got a Pier named " +
							// currentPropertyName + " with target " + target +
							// " and targetProp " + targetPropName);
							// log.trace("currentDirty = " + currentDirty);

							if (currentDirty) {
								PierBundle pb = new PierBundle(currentPropertyName, target, targetPropName);
								currentPiers.add(pb);
							}
						}

					} else if (parser.getLocalName().equalsIgnoreCase("value")) {
						currentPropertyValue = PropertyConverter.fromString(currentValueType, parser.getElementText()
						        .toString());

					} else if (parser.getLocalName().equalsIgnoreCase("oldvalue")) {
						currentPropertyOldValue = PropertyConverter.fromString(currentValueType, parser
						        .getElementText().toString());

					} else if (parser.getLocalName().equalsIgnoreCase("override")) {
						currentPropertyOverride = PropertyConverter.fromString(currentValueType, parser
						        .getElementText().toString());

					} else if (parser.getLocalName().equalsIgnoreCase("dlid")) {
						dlid = parser.getElementText();

					} else if (parser.getLocalName().equalsIgnoreCase("key")) {
						String key = parser.getElementText().trim();
						if (key.length() > 0) {
							to = loadTO(key);
						} else {
							to = null;
						}

					} else if (parser.getLocalName().equalsIgnoreCase("deleted")) {
						currentDeleted = Boolean.parseBoolean(parser.getElementText());

					} else if (parser.getLocalName().equalsIgnoreCase("current")) {
						newChildren = true;

					} else if (parser.getLocalName().equalsIgnoreCase("child")) {
						if (newChildren) {
							currentChildren.add(parser.getElementText());
						} else {
							oldChildren.add(parser.getElementText());
						}

					} else if (parser.getLocalName().equalsIgnoreCase("item")) {
						// only slug something into the items list if the
						// current collection is dirty
						if (currentDirty) {
							currentItems.add(parser.getElementText());
						}

					} else if (parser.getLocalName().equalsIgnoreCase("ruleTypeName")) {
						currentRuleTypeName = parser.getElementText();

					} else if (parser.getLocalName().equalsIgnoreCase("customtag")) {
						// tags are all one one line, so we can do it all here
						String tagName = parser.getAttributeValue(null, "name");
						String oldVal = parser.getAttributeValue(null, "oldVal");

						// override should only exist if the user added one,
						// so a present but empty tag is empty on purpose
						String override = parser.getAttributeValue(null, "override");

						String currentVal = parser.getElementText();

						// log.debug("tag with name '" + tagName +
						// "' override= '" + override + "' current = '" +
						// currentVal + "' oldval = '" + oldVal + "'");

						String updatedValue;
						// if (override != null && override.trim().length() > 0)
						// {
						if (override != null) {
							updatedValue = override;
						} else {
							updatedValue = currentVal;
						}

						if (!oldVal.equals(updatedValue)) {
							log.trace("Tag delta on '" + tagName + "' old='" + oldVal + "' new='" + updatedValue + "'");

							// look up tag type, convert, store
							TagDescriptor td = env.getTagDescriptor(currentObjectType, currentPropertyName, tagName);
							Object val = PropertyConverter.fromString(td.getTypeName(), updatedValue);
							currentTags.add(new Tag(currentPropertyName, tagName, val));
						}

						// special case: the customtag for deleteLevel
						if (tagName.equals("cascadeDelete")) {
							deleteLevel = Integer.parseInt(currentVal);
						}

					} else if (parser.getLocalName().equalsIgnoreCase("binding")) {
						String rulevar = parser.getAttributeValue(0);
						String r_property = parser.getAttributeValue(1);
						// log.trace("BINDING");
						// log.trace("binding: rulevar=" + rulevar + "  prop=" +
						// r_property);

						currentBindings.put(r_property, rulevar);

					}

					// parentid?

					break;
				case XMLStreamConstants.END_ELEMENT:
					if (parser.getLocalName().equalsIgnoreCase("property")) {
						// construct the property update
						if (currentPropertyType.equalsIgnoreCase("setting")) {

							// update required if current != old UNLESS current
							// is null

							// log.trace("Property " + currentPropertyName +
							// " currentValue: '" + currentPropertyValue + "'");
							// log.trace("Property " + currentPropertyName +
							// " oldValue: '" + currentPropertyOldValue + "'");

							// if (currentPropertyOverride != null &&
							// !currentPropertyOverride.equals(currentPropertyOldValue))
							// {

							// if override is non-null, we want to ignore value
							// altogether
							if (currentPropertyOverride != null) {

								if (!currentPropertyOverride.equals(currentPropertyOldValue)) {

									log.trace("Property " + currentPropertyName + " needs OVERRIDE: '"
									        + currentPropertyOverride + "'");
									/*
									Bug[9484]
									HACK TIME! Since env.createObjects(bundle) method does not put first prop values to history, set necessary props separately.
									 */
									if (Arrays.asList(PROP_TO_RESET).contains(currentPropertyName)){
											log.debug("Property " + currentPropertyName + " stashed. Will be updated via direct set");
											log.debug(dlid+" value: "+currentPropertyOverride);
											propToReset.add(new ValueTO(currentPropertyName, currentPropertyOverride));
									} else {
										updates.add(new ValueTO(currentPropertyName, currentPropertyOverride));
									}
								}

							} else if (currentPropertyValue != null
							        && !currentPropertyValue.equals(currentPropertyOldValue)) {

								// log.trace("DLIS: Update Setting " +
								// currentPropertyName + " to ");
								// log.trace("Property " + currentPropertyName +
								// " oldValue: '" + currentPropertyOldValue +
								// "'");
								log.trace("Property " + currentPropertyName + " needs update: '" + currentPropertyValue
								        + "'");
								//Bug[9484]
								if ( Arrays.asList(PROP_TO_RESET).contains(currentPropertyName)){
									log.debug("Property " + currentPropertyName + " stashed. Will be updated via direct set");
									log.debug(dlid+" value: "+currentPropertyValue);
									propToReset.add(new ValueTO(currentPropertyName, currentPropertyValue));
								} else {
									updates.add(new ValueTO(currentPropertyName, currentPropertyValue));
								}
							}

						} else if (currentPropertyType.equalsIgnoreCase("groovyrule")
						        || currentPropertyType.equalsIgnoreCase("rule")) {

							// for a CRE Rule, we need:
							// rule instance name
							// rule type TO (in ruleTypeIndex)
							// dest obj TO (about to find it)
							// dest property name
							// bindings:
							// src obj TO (for us, always the dest obj TO)
							// src obj propName
							// var name inside rule

							// we can assume the type already exists in
							// ruleTypeIndex by ruleClass aka ruleTypeName
							/*
							 * <property name='lastValue' type='groovyrule'
							 * valuetype=''>
							 * <ruleTypeName>inspector_lastValue</ruleTypeName>
							 * <binding rulevar='input' property='input' />
							 * </property>
							 */
							// don't have TO yet, store:
							// propname : ruleTypeName : bindings
							currentRuleBundles.add(new CREBundle(currentRuleTypeName, "", currentPropertyName,
							        currentBindings));
							currentBindings = new HashMap<String, String>(5);

							// store links for later installment
						} else if (currentPropertyType.equalsIgnoreCase("collection") && currentDirty) {
							log.debug("New Collection Bundle: " + currentItems);

							CollectionBundle cb = new CollectionBundle("", currentPropertyName, currentItems);
							// System.out.println("new CB: " );
							// System.out.println(cb.toString());
							currentCollections.add(cb);

							currentItems = new ArrayList<String>();

						}

					} else if (parser.getLocalName().equalsIgnoreCase("current")) {
						newChildren = false;

					} else if (isObject(parser.getLocalName())) {
						// / log.trace("trying to close out object...");

						// begin filling in the DLImportResult for this object.
						// Start with the DLID
						if (dlid != null && dlid.length() > 1) {
							result.setDlid(dlid);
						}

						// if (currentDeleted && to != null && env.exists(to)) {
						if (currentDeleted && to != null) {
							// DELETE
							// do the delete in here and then save the TO for
							// later reference

							if (env.exists(to)) {
								log.trace("Delete Object with TO " + to.toString() + " at a delete level of "
								        + deleteLevel);
								env.deleteObject(to, deleteLevel);
							} else {
								log.trace("Adding object to already deleted prune list with TO " + to.toString());
							}

							deletedObjects.add(to);
							DLIDToKey.put(dlid, to);
							result.setDeleted(true);
							result.setKey(to);
							return (result);

						} else if (to != null && env.exists(to)) {
							// UPDATE
							// / log.trace("Object " + to.toString() +
							// " exists, updating...");
							// fire off updates for this object
							//Bug[9484]
							updates.addAll(propToReset);

							if (updates.size() > 0 || currentTags.size() > 0) {
								// log.debug("ESAPI: updating $to with $updates")
								// log.trace("setAllPropertiesValues on " +
								// to.toString());
								log.debug("DLIS: updating " + to.toString() + " with " + updates.toString());
								log.debug("...and Tags " + currentTags.toString());
								// env.setAllPropertiesValues(to,
								// updates.toArray(new
								// ValueTO[updates.size()]));

								env.setAllPropertiesValues(to, updates, currentTags);

								result.setTagsSuccessful(true);
							}
							/*
							 * else { log.trace("No updates for " +
							 * to.toString()); }
							 */

							// make sure to store the TO into the result for
							// updated objects
							result.setKey(to);

							// we need to store this on an update case
							DLIDToKey.put(dlid, to);

						} else {
							// CREATE
							// / to = env.createObject(currentObjectType,
							// updates.toArray( new ValueTO[ updates.size() ])
							// );
							// / log.trace("env.createObject( type = " +
							// currentObjectType + " )  ==> " + to.toString());

							// with the bulk create, we do this:
							// objectsInProject.addObject(dlid, new
							// EnvObject(currentObjectType, updates));

							// now, with tags!
							log.trace("tags for this object are:");
							log.trace(currentTags);

							if (currentTags.size() > 0) {
								objectsInProject
								        .addObject(dlid, new EnvObject(currentObjectType, updates, currentTags));
							} else {
								objectsInProject.addObject(dlid, new EnvObject(currentObjectType, updates));
							}

							//Bug[9484]
							log.debug("DLID: "+dlid +" put properties: "+propToReset);
							propToResetMap.put(dlid, propToReset);
						}

						// stash the DLID of the project root objects
						if (currentObjectType.equalsIgnoreCase("DC")) {
							allProjectRootDLIDs.add( dlid );
						}

						// populate this result
						if (!addChildren.containsKey(dlid)) {
							addChildren.put(dlid, new ArrayList<String>());
						}
						if (!removeChildren.containsKey(dlid)) {
							removeChildren.put(dlid, new ArrayList<String>());
						}

						for (CollectionBundle cb : currentCollections) {
							cb.setDlid(dlid);
							collections.add(cb);
						}

						for (DockingpointBundle db : currentDockingpoints) {
							db.setDlid(dlid);
							dockingPoints.add(db);
						}

						for (PierBundle pb : currentPiers) {
							pb.setDlid(dlid);
							piers.add(pb);
						}

						for (String k : currentChildren) {
							if (!oldChildren.contains(k)) {
								addChildren.get(dlid).add(k);
							}
						}
						for (String k : oldChildren) {
							if (!currentChildren.contains(k)) {
								removeChildren.get(dlid).add(k);
							}
						}

						// rules!
						// ruleName = destTO.getID() + ruleProperty.name

						for (CREBundle rb : currentRuleBundles) {
							rb.setDlid(dlid);
							allRules.add(rb);
						}

						return (result);

					}
					break;
				} // end switch
			} // end while parser
		} catch (XMLStreamException ex) {
			log.error("XML Parse Fail:" + ex.getMessage());
			result.addError(new DLImportError(dlid, ex.getMessage(), ex));

		} catch (Exception e) {
			log.error("Error Updating Objects:" + e.getMessage() + " exception type: " + e.getClass().getName());
			result.addError(new DLImportError(dlid, e.getMessage(), e));
		}

		if (to != null) {
			result.setKey(to);
		}
		if (dlid != null && dlid.length() > 1) {
			result.setDlid(dlid);
		}
		return (result);
	}

	private HashMap<String, TO<?>> toCache = new HashMap<String, TO<?>>();

	private TO<?> loadTO(String key) {
		if (key == null || key.equals(""))
			throw new NullPointerException("Can't load empty TO!");
		TO<?> to = toCache.get(key);
		if (to == null) {
			to = TOFactory.getInstance().loadTO(key);
			toCache.put(key, to);
		}
		return to;
	}

	/**
	 * Map of rule type names to their TO. This is populated at export time to
	 * see which rule types have already been created. The type name is provided
	 * from the rule itself via CREMember.getServerTypeName()
	 *
	 * [typename : TO]
	 */
	private Map<String, TO<?>> ruleTypeIndex = new HashMap<String, TO<?>>();

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public boolean storeDLZFile(TO<?> dlzStore, BinaryData config) throws DLImportException {
		log.trace("Storing DLZ file to database");
		List<ValueTO> updates = new ArrayList<ValueTO>();
		updates.add(new ValueTO("dlzFile", config));
		try {
			log.trace("Calling set all properties RIGHT NOW");
			env.setAllPropertiesValues(dlzStore, updates, new ArrayList<Tag>());
		} catch (EnvException e) {
			throw new DLImportException("Unable to stash DLZ File", e);
		}
		// env.setPropertyValue(dlzStore, "dlzFile", config);
		return true;
	}

}
