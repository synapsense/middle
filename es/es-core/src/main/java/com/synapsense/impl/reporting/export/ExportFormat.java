package com.synapsense.impl.reporting.export;

import java.io.Serializable;
import java.util.HashMap;

public abstract class ExportFormat implements Serializable {
	
	public static final ExportFormat PDF = new ExportFormat("pdf") {

		private static final long serialVersionUID = -7931287561417732263L;

		@Override
		public ReportExporter getReportExporter() {
			return new PdfReportExporter();
		}

	};
	public static final ExportFormat HTML = new ExportFormat("html") {

		private static final long serialVersionUID = 4895711825204808970L;

		@Override
		public ReportExporter getReportExporter() {
			return new HtmlReportExporter();
		}
	};

	public static final ExportFormat XML = new ExportFormat("xml") {

		private static final long serialVersionUID = 7853754749266670602L;

		@Override
		public ReportExporter getReportExporter() {
			return new XmlReportExporter();
		}

	};

	public static final ExportFormat XLSX = new ExportFormat("xlsx") {

		private static final long serialVersionUID = 2104936543305945205L;

		@Override
		public ReportExporter getReportExporter() {
			return new XlsxReportExporter();
		}

	};

	public static final ExportFormat[] avFormats;

	private static final long serialVersionUID = 3938608072596323559L;

	private static HashMap<String, ExportFormat> formatMap = new HashMap<String, ExportFormat>();

	static {
		formatMap.put(HTML.toString(), HTML);
		formatMap.put(PDF.toString(), PDF);
		formatMap.put(XML.toString(), XML);
		formatMap.put(XLSX.toString(), XLSX);
		avFormats = formatMap.values().toArray(new ExportFormat[formatMap.size()]);
	}

	public static ExportFormat load(String source) {
		return formatMap.get(source);
	}

	private final String id;

	protected ExportFormat(final String formatId) {
		this.id = formatId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final ExportFormat other = (ExportFormat) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return id;
	}

	public String getFileExtension() {
		return id;
	}

	public String save() {
		return id;
	}

	public abstract ReportExporter getReportExporter();
}
