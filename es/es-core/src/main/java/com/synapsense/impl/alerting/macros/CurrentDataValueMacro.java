package com.synapsense.impl.alerting.macros;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CurrentDataValueMacro extends AbstractPatternMacro {

	private static final Pattern pattern = Pattern.compile("\\$(?i)current_data_value\\$");

	@Override
	public String expandIn(String input, MacroContext ctx) {
		if (input == null)
			return null;
		if (ctx.alert == null)
			throw new IllegalArgumentException("MacroContext.alert must be not null");
		return super.expandIn(input, ctx);
	}

	@Override
	protected String expand(Matcher m, MacroContext ctx) {
		return Matcher.quoteReplacement(dataToConvMacro.expandIn(ctx.alert.getMessage(), ctx));
	}

	@Override
	protected Pattern getPattern() {
		return pattern;
	}

	private Macro dataToConvMacro = new AbstractDataValueMacro() {

		@Override
		public String expandIn(String input, MacroContext ctx) {
			if (input == null)
				return Macro.DEFAULT_EXPANSION;
			// current data value skips message's text
			// but expands data macro into comma-separated
			// any-env-property-value macros
			StringBuilder sb = new StringBuilder();
			Matcher m = getPattern().matcher(input);
			boolean firstMatch = true;
			while (m.find()) {
				if (!firstMatch) {
					sb.append(", ");
				} else {
					firstMatch = false;
				}
				if (m.group(3) != null) {// dimension is specified
					sb.append("$").append(m.group(1)).append("{").append(m.group(3)).append("}$");
				} else {// no dimension, put value macro with no conversion
					sb.append("$").append(m.group(1)).append("$");
				}
			}
			return sb.toString();
		}

		@Override
		protected String expand(Matcher m, MacroContext ctx) {
			return null; // not used anyway - expandIn() is overridden
		}

	};

}
