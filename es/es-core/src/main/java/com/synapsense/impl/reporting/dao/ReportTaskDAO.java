package com.synapsense.impl.reporting.dao;

import com.synapsense.exception.ObjectExistsException;
import com.synapsense.service.reporting.dto.ReportTaskInfo;
import com.synapsense.service.reporting.dto.ReportTaskTitle;
import com.synapsense.service.reporting.exception.ObjectNotFoundDAOReportingException;

import java.util.Collection;

/**
 * @author : shabanov
 */
public interface ReportTaskDAO {
    void saveReportTask(ReportTaskInfo task) throws ObjectNotFoundDAOReportingException, ObjectExistsException;

    void updateReportTask(int taskId, ReportTaskInfo newState) throws ObjectNotFoundDAOReportingException, ObjectExistsException;

    void removeReportTask(int taskId) throws ObjectNotFoundDAOReportingException;

    ReportTaskInfo getReportTaskInfo(int taskId) throws ObjectNotFoundDAOReportingException;

    Collection<ReportTaskTitle> getActiveReportTasks(String reportTemplateName)
            throws ObjectNotFoundDAOReportingException;

    ReportTaskInfo getReportTaskInfo(String taskName) throws ObjectNotFoundDAOReportingException;
}
