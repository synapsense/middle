package com.synapsense.impl.alerting.notification;

import java.util.Collection;

import com.synapsense.dto.Message;

public interface NotificationManager {
	/**
	 * Start processing specified notification
	 * 
	 * @param Notification
	 *            to process
	 */
	void raise(Notification n);

	/**
	 * Add specified notification instance but delays processing it. This method
	 * useful when caller wants to add already acknowledged notification.
	 * 
	 * @param Notification
	 *            to add
	 */
	void add(Notification n);

	void ack(String id);

	void dismiss(String id);

	void resolve(String id);

	void remove(String id);

	boolean send(Message message);

	Collection<Notification> getActiveNotifications();
}