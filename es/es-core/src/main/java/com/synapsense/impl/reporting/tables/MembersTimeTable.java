package com.synapsense.impl.reporting.tables;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.synapsense.impl.reporting.queries.TableProcessor.GroupType;
import com.synapsense.impl.reporting.utils.ReportingConstants.QUERY.SYMBOL;
import com.synapsense.impl.reporting.utils.ReportingConstants.QUERY.WORD;
import com.synapsense.service.reporting.olap.Member;
import com.synapsense.service.reporting.olap.OrderedHierarchy;
import com.synapsense.service.reporting.olap.SimpleMember;
import com.synapsense.service.reporting.olap.custom.BaseLevel;
import com.synapsense.service.reporting.olap.custom.Cube;
import com.synapsense.service.reporting.olap.custom.Level;
import com.synapsense.util.CollectionUtils;

/**
 * The <code>MembersTimeTable</code> class contains Time Members Map.<br>
 * Clause Example: WHERE clause item will be like 
 * (timestamp like '2013-03-28 20%' or timestamp like '2013-03-27 21%')
 * 
 * @author Grigory Ryabov
 */
public class MembersTimeTable implements ReportingTimeTable {

	private static final long serialVersionUID = 5666006225087276757L;

	Map<String, List<String>> timeMembers = CollectionUtils.newLinkedMap();;
	
	protected String lowestTimelevel;
		
	protected OrderedHierarchy hierarchy = Cube.TIME;
	
	@SuppressWarnings("unchecked")
	@Override
	public String get() {
		
		StringBuilder timeMembersSB = new StringBuilder();
		for (Entry<String, List<String>> entry : timeMembers.entrySet()) {
			

			StringBuilder timeMemberValuesSB = new StringBuilder();
			timeMembersSB.append(SYMBOL.OPEN_BRACKET);

			for (String value : entry.getValue()) {
				timeMemberValuesSB.append("timestamp ")
						.append(value)
						.append(SYMBOL.SPACE)
						.append(WORD.OR)
						.append(SYMBOL.SPACE);
			}
			timeMembersSB
					.append(timeMemberValuesSB.substring(0,
							timeMemberValuesSB.length() - 4))
					.append(SYMBOL.CLOSE_BRACKET)
					.append(SYMBOL.SPACE)
					.append(WORD.AND)
					.append(SYMBOL.SPACE);
		}
		return timeMembersSB.substring(0, timeMembersSB.length() - 5);
		
	}

	@Override
	public void add(Member member) {
		
		StringBuilder clauseSB = new StringBuilder();
		StringBuilder startValueSB = new StringBuilder();
		StringBuilder endValueSB = new StringBuilder();
		StringBuilder valueSB = new StringBuilder();
		List<String> clauses = new ArrayList<>();
		List<String> startMemberValues = new ArrayList<>();
		List<String> endMemberValues = new ArrayList<>();
		
		Member current = member;
		Member endCurrentMember = Cube.TIME.getNext(member);
		String startLevelName = member.getLevel();
		String levelName = startLevelName;
		while (Cube.TIME.isDescendant(Cube.LEVEL_MONTH, levelName)
				&& !levelName.equals(Cube.LEVEL_DAY)) {
			levelName = Cube.TIME.getChildLevel(startLevelName);
			current = new SimpleMember(Cube.TIME, levelName, 1, current);
			endCurrentMember = new SimpleMember(Cube.TIME, levelName, 1, endCurrentMember);
		}
		while (current != null) {
			startMemberValues.add(convertValue((Integer) current.getKey()));
			
			endMemberValues.add(convertValue((Integer) endCurrentMember.getKey()));
			
			clauses.add(GroupType.getTypeByName(current.getLevel()).getFormat());
			
			current = current.getParent();
			endCurrentMember = endCurrentMember.getParent();
		}
		
		for (int i = clauses.size() - 1; i >= 0; i--) {
			clauseSB.append(clauses.get(i))
				.append(SYMBOL.DASH);
			startValueSB.append(startMemberValues.get(i))
				.append(SYMBOL.DASH);
			endValueSB.append(endMemberValues.get(i))
				.append(SYMBOL.DASH);
		}
		String clauseItem = clauseSB.substring(0, clauseSB.length() - 1);
		String startValue = startValueSB.substring(0, startValueSB.length() - 1);
		String endValue = endValueSB.substring(0, endValueSB.length() - 1);
		valueSB.append(WORD.BETWEEN)
			.append(SYMBOL.SPACE)
			.append(SYMBOL.APOSTROPHE)
			.append(startValue)
			.append(SYMBOL.APOSTROPHE)
			.append(SYMBOL.SPACE)
			.append(WORD.AND)
			.append(SYMBOL.SPACE)
			.append(SYMBOL.APOSTROPHE)
			.append(endValue)
			.append(SYMBOL.APOSTROPHE);
		String value = valueSB.toString();
		if (timeMembers.containsKey(clauseItem)) {
			timeMembers.get(clauseItem).add(value);
		} else {
			List<String> valuesSet = CollectionUtils.newList();
			valuesSet.add(value);
			timeMembers.put(clauseItem, valuesSet);
		}
		setLowestTimeLevel(member.getLevel());
		
	}

	protected static String convertValue(Integer value) {
		return (value < 10) ? "0" + value : value.toString();
	}

	protected void setLowestTimeLevel(String level) {
		this.lowestTimelevel = level;
	}

	@Override
	public Level getLowestLevel() {
		String lowestLevel = (lowestTimelevel != null) ? lowestTimelevel
				: (lowestTimelevel = Cube.LEVEL_YEAR);
		return new BaseLevel(hierarchy, lowestLevel);
	}

	@Override
	public boolean isEmpty() {
		for (Entry<String, List<String>> entry : timeMembers.entrySet()) {
			if (!entry.getValue().isEmpty()) {
				return false;
			}
		}
		return true;
	}

	@Override
	public int size() {
		return this.timeMembers.size();
	}

	@Override
	public MembersTimeTable getTable() {
		
		MembersTimeTable copy = new MembersTimeTable();
		for (Entry<String, List<String>> entry : timeMembers.entrySet()) {
			copy.put(entry.getKey(), entry.getValue());
		}
		copy.setLowestTimeLevel(lowestTimelevel);
		return copy;
		
	}

	protected void put(String key, List<String> value) {
		timeMembers.put(key, CollectionUtils.newList(value));
	}
	
}