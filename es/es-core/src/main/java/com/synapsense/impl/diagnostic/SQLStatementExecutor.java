package com.synapsense.impl.diagnostic;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.security.auth.login.LoginException;
import javax.sql.DataSource;
import javax.transaction.Transaction;

import org.jboss.tm.TransactionDemarcationSupport;

public class SQLStatementExecutor {
	protected static String dsJndiName = "java:/SynapDS";
	protected boolean suspendResume = false;

	protected Connection conn = null;
	protected PreparedStatement ps = null;
	protected ResultSet rs = null;

	protected Transaction tx = null;

	public void prepareConnection() throws LoginException {
		if (suspendResume) {
			tx = TransactionDemarcationSupport.suspendAnyTransaction();
		}

		try {
			InitialContext ctx = new InitialContext();
			DataSource ds = (DataSource) ctx.lookup(dsJndiName);
			conn = ds.getConnection();
		} catch (NamingException ex) {
			LoginException le = new LoginException("Error looking up DataSource from: " + dsJndiName);
			le.initCause(ex);
			throw le;
		} catch (SQLException ex) {
			LoginException le = new LoginException("Query failed");
			le.initCause(ex);
			throw le;
		}
	}

	public ResultSet executeSQLStatement(String query) throws LoginException {

		try {
			ps = conn.prepareStatement(query);
			rs = ps.executeQuery();

			return rs;
		} catch (SQLException ex) {
			LoginException le = new LoginException("Query failed");
			le.initCause(ex);
			throw le;
		}
	}

	public void closeResultSet() {
		if (rs != null) {
			try {
				rs.close();
			} catch (SQLException e) {
			}
		}
		if (ps != null) {
			try {
				ps.close();
			} catch (SQLException e) {
			}
		}
	}

	public void closeConnection() {
		closeResultSet();

		if (conn != null) {
			try {
				conn.close();
			} catch (Exception ex) {
			}
		}

		if (suspendResume) {
			TransactionDemarcationSupport.resumeAnyTransaction(tx);
		}
	}
}
