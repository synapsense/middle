/**
 * 
 */
package com.synapsense.impl.taskscheduler.tasks;

import java.util.Date;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.apache.log4j.Logger;

import com.synapsense.service.reporting.dto.ReportTitle;
import com.synapsense.service.reporting.management.ReportingException;
import com.synapsense.service.reporting.management.ReportingManagementService;

/**
 * @author apakhunov
 * 
 */
@Stateless
@Remote(ReportCleanerTask.class)
@TransactionAttribute(TransactionAttributeType.NEVER)
@TransactionManagement(TransactionManagementType.BEAN)
public class ReportCleanerTaskImpl implements ReportCleanerTask {

	private final long MS_IN_DAY = 1000 * 60 * 60 * 24;
	private final long DEFAULT_KEEP_PERIOD = 30;
	
	private final static Logger logger = Logger.getLogger(ReportCleanerTaskImpl.class);
	
	private ReportingManagementService rms;

	public ReportingManagementService getRms() {
    	return rms;
    }

	@EJB(beanName = "ReportingManagementService")
	public void setRms(ReportingManagementService rms) {
    	this.rms = rms;
    }

	@Override
	public void execute() {
		logger.debug("ReportCleaner task started");
		long currentTs = new Date().getTime();
		
		long keepPeriod = DEFAULT_KEEP_PERIOD * MS_IN_DAY;
		
		String keepPeriodStr = System.getProperty("com.synapsense.reportKeepPeriod");
		if (keepPeriodStr != null) {
			try {
			keepPeriod = Integer.parseInt(keepPeriodStr) * MS_IN_DAY;
			} catch (NumberFormatException e){
				logger.error("Can't parse com.synapsense.reportKeepPeriod: "  + keepPeriodStr + " is not a number");
			}
		}
		
		try {
			for (ReportTitle report : rms.getGeneratedReports()) {
				long ts = report.getTimestamp();
				if (currentTs - ts > keepPeriod) {
					int reportId = report.getReportId();
					if (logger.isDebugEnabled()) {
						logger.debug("Removing report with id=" + reportId
								+ " ts=" + ts);
					}
					rms.removeReport(reportId);
				}
			}
        } catch (ReportingException e) {
        	logger.error(e.getMessage(), e);
        }
		logger.debug("ReportCleaner task finished");
	}

}
