package com.synapsense.impl.alerting.notification;

/**
 * @author : shabanov
 */
public interface Provider<T> {
    T get();
}
