package com.synapsense.impl.taskscheduler;

/**
 * Base class for <tt>QuartzTaskScheduler</tt> exceptions.
 * 
 * @author Oleg Stepanov
 * 
 */
public class TaskSchedulerException extends Exception {

	private static final long serialVersionUID = 3902333266409710048L;

	public TaskSchedulerException() {
		super();
	}

	public TaskSchedulerException(String message) {
		super(message);
	}

	public TaskSchedulerException(Throwable cause) {
		super(cause);
	}

	public TaskSchedulerException(String message, Throwable cause) {
		super(message, cause);
	}

}
