package com.synapsense.impl.reporting.tables;

import java.io.Serializable;

import com.synapsense.service.reporting.olap.Member;
import com.synapsense.service.reporting.olap.custom.Level;

public interface ReportingTable extends Serializable {
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	<T> T get();

	/**
	 * Adds the time value.
	 *
	 * @param member the member
	 */
	void add(Member member);

	/**
	 * Gets the lowest level.
	 *
	 * @return the lowest level
	 */
	Level getLowestLevel();

	/**
	 * Checks if is empty.
	 *
	 * @return true, if is empty
	 */
	boolean isEmpty();

	/**
	 * Gets table size.
	 *
	 * @return the size
	 */
	int size();

	/**
	 * Gets the table.
	 *
	 * @return the table
	 */
	ReportingTable getTable();

}
