package com.synapsense.impl.quartz;

import java.io.File;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.DependsOn;
import javax.ejb.Singleton;
import javax.ejb.Startup;

import org.apache.log4j.Logger;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.impl.StdSchedulerFactory;

@Startup @Singleton @DependsOn("Registrar")
@ConcurrencyManagement(ConcurrencyManagementType.BEAN)
public class QuartzService {
	
	private final static Logger logger = Logger.getLogger(QuartzService.class);
	
	private Scheduler scheduler;
	private static final String QUARTZ_PROPERTIES = "quartz.properties";
	private static final String FILE_STORAGE = "com.synapsense.filestorage";

	public Scheduler getScheduler() throws SchedulerException {
		return scheduler;
	}
	
	@PostConstruct
	public void start() throws Exception{
		String fileName = System.getProperty(FILE_STORAGE);

		if (fileName == null || fileName.isEmpty())
			throw new RuntimeException("Property [" + FILE_STORAGE + "] is not set");

		fileName += File.separator + QUARTZ_PROPERTIES;
	
		logger.info("Reading quartz properties from "+fileName);
		scheduler = new StdSchedulerFactory(fileName).getScheduler();
		logger.info("QuartzService started");
	}
	
	@PreDestroy
	public void stop() {
		scheduler = null;
	}
}
