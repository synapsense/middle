
package com.synapsense.impl.erlnode.commands;

import java.util.Map;

import com.ericsson.otp.erlang.OtpErlangBinary;
import com.ericsson.otp.erlang.OtpErlangObject;
import com.ericsson.otp.erlang.OtpErlangTuple;
import com.synapsense.impl.erlnode.ExceptionTypes;
import com.synapsense.impl.erlnode.utils.ToConverter;
import com.synapsense.dto.TO;
import com.synapsense.exception.ObjectNotFoundException;

import com.synapsense.impl.erlnode.utils.StringConverter;

/**
 * @author stikhon
 * 
 */
public class GetParentsCommand2 extends Command {

	public GetParentsCommand2(Map<String, Object> state) {
		super(state);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see Command#do_command(com.ericsson.otp.erlang.OtpErlangTuple)
	 */
	@Override
	public OtpErlangObject do_command(OtpErlangTuple command) throws Exception {

		OtpErlangObject objectsTuple = command.elementAt(1);
		OtpErlangObject typeTuple = command.elementAt(2);
		if (objectsTuple == null || typeTuple == null
				|| !(objectsTuple instanceof OtpErlangTuple)
				|| !(typeTuple instanceof OtpErlangBinary)) {
			return makeException(ExceptionTypes.ILLEGAL_ARGUMENTS, null);
		}
		TO<?> to = ToConverter.parseTo((OtpErlangTuple) objectsTuple);
		String typeNameValue = StringConverter.utf8BinaryToString(typeTuple);
		try {
			return makeOkResult(ToConverter.serializeToList(getEnv()
					.getParents(to, typeNameValue)));
		} catch (ObjectNotFoundException e) {
			return makeException(ExceptionTypes.OBJECT_NOT_FOUND, e
					.getMessage());
		}
	}

}
