package com.synapsense.impl.alerting.command;

import java.util.Collection;

import org.apache.log4j.Logger;

import com.synapsense.commandprocessor.api.Command;
import com.synapsense.dto.Alert;
import com.synapsense.dto.TO;
import com.synapsense.exception.EnvException;
import com.synapsense.impl.alerting.Cache;
import com.synapsense.service.Environment;

public class DeleteAlert implements Command {

	private final static Logger logger = Logger.getLogger(DeleteAlert.class);

	private Collection<TO<?>> alerts;
	private Environment env;
	private Cache<Alert, TO<?>> alertStore;

	public DeleteAlert(Environment env, Cache<Alert, TO<?>> alertStore, Collection<TO<?>> alerts) {
		this.alerts = alerts;
		this.env = env;
		this.alertStore = alertStore;
	}
	
	@Override
	public void execute() {
		for (final TO<?> alert : alerts) {

			if (!env.exists(alert)) {
				continue;
			}

			try {
				// if successful try to delete
				env.deleteObject(alert);
				// if successful clear local performance cache
				boolean removed = alertStore.remove(alert);
				if (!removed) {
					logger.warn("There was no index key in local cache for alert : " + alert);
				}
			} catch (EnvException e) {
				logger.warn("Failed to delete alert " + alert, e);
				continue;
			}

			if (logger.isDebugEnabled()) {
				logger.debug("Alert : " + alert + " has been DELETED successfully");
			}
		}

	}

}
