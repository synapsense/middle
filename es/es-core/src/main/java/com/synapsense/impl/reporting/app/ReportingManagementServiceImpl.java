package com.synapsense.impl.reporting.app;

import com.synapsense.cdi.MBean;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.exception.IllegalInputParameterException;
import com.synapsense.exception.ObjectExistsException;
import com.synapsense.impl.reporting.commandengine.CompositeCommand;
import com.synapsense.impl.reporting.commandengine.DefaultCommandManager;
import com.synapsense.impl.reporting.commandengine.ReportingExecutor;
import com.synapsense.impl.reporting.commands.CommandFactory;
import com.synapsense.impl.reporting.commands.ReportCommandProxy;
import com.synapsense.impl.reporting.commands.ReportingCommandManager;
import com.synapsense.impl.reporting.dao.*;
import com.synapsense.impl.reporting.export.ExportFormat;
import com.synapsense.impl.reporting.model.ReportTemplate;
import com.synapsense.impl.reporting.utils.algo.ForEach;
import com.synapsense.impl.reporting.utils.algo.UnaryFunctor;
import com.synapsense.security.RunAsEsUser;
import com.synapsense.service.Environment;
import com.synapsense.service.FileWriter;
import com.synapsense.service.UserManagementService;
import com.synapsense.service.impl.dao.to.GenericObjectTO;
import com.synapsense.service.reporting.dto.*;
import com.synapsense.service.reporting.exception.SchedulingReportingException;
import com.synapsense.service.reporting.management.ReportingException;
import com.synapsense.service.reporting.management.ReportingManagementService;
import com.synapsense.service.reporting.management.ReportingSchedulingService;
import com.synapsense.service.reporting.olap.RangeSet;
import com.synapsense.util.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONException;
import org.json.JSONObject;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.*;
import javax.interceptor.Interceptors;
import java.security.Principal;
import java.text.*;
import java.util.*;

/**
 * The <code>ReportingManagementService</code> implementor. Provides Report Parameter parsing behavior.
 */
@Startup
@Singleton(name = "ReportingManagementService")
@DependsOn({ "ReportingSchedulingService" })
@Remote(ReportingManagementService.class)
@ConcurrencyManagement(ConcurrencyManagementType.BEAN)
@MBean("com.synapsense:type=ReportingManagementService")
@Interceptors(ReportingContextInterceptor.class)
public class ReportingManagementServiceImpl implements ReportingManagementService, ReportingManagementServiceImplMBean {

	private static Log log = LogFactory.getLog(ReportingManagementServiceImpl.class);

	public static final String REPORTING_FILE_STORAGE = System.getProperty("com.synapsense.filestorage");

	private static final int THREAD_POOL_SIZE = 5;
	
	private Environment environment;

	private FileWriter fileWriter;

	private ReportingSchedulingService scheduler;

	/** EJB */
	private UserManagementService userManagement;

	// private JSReportManager manager;
    private ReportingCommandManager executor;

    private ReportTemplateDAO reportTemplateDAO;

    private ReportDAO reportDAO;

    private ReportTaskDAO reportTaskDAO;

	@Resource
	/** EJB session context */
	private SessionContext sessionContext;
	
	/** EJB */
	private ReportingExecutor reportingJBOSSExecutor;

	private static Map<Class<?>, ParameterValueProvider> parameterValueProviderMap = CollectionUtils.newMap();

	static {
		TOValueProvier genericTOValueProvier = new TOValueProvier();
		parameterValueProviderMap.put(GenericObjectTO.class, genericTOValueProvier);
		IntegerValueProvier integerValueProvier = new IntegerValueProvier();
		parameterValueProviderMap.put(Integer.class, integerValueProvier);
		LongValueProvier longValueProvier = new LongValueProvier();
		parameterValueProviderMap.put(Long.class, longValueProvier);
		CollectionTOsValueProvier collectionTOsValueProvier = new CollectionTOsValueProvier();
		parameterValueProviderMap.put(Collection.class, collectionTOsValueProvier);
		DateValueProvier dateValueProvier = new DateValueProvier();
		parameterValueProviderMap.put(Date.class, dateValueProvier);
		StringValueProvier stringValueProvier = new StringValueProvier();
		parameterValueProviderMap.put(String.class, stringValueProvier);
		BooleanValueProvier booleanValueProvier = new BooleanValueProvier();
		parameterValueProviderMap.put(Boolean.class, booleanValueProvier);
		SetTOsValueProvier setTOsValueProvier = new SetTOsValueProvier();
		parameterValueProviderMap.put(HashSet.class, setTOsValueProvier);
		EnumValueProvier enumValueProvier = new EnumValueProvier();
		parameterValueProviderMap.put(Enum.class, enumValueProvier);
		RangeSetValueProvier rangeSetValueProvier = new RangeSetValueProvier();
		parameterValueProviderMap.put(RangeSet.class, rangeSetValueProvier);
	}

	@EJB(beanName = "UserManagementService")
	public void setUserService(final UserManagementService userManagement) {
		this.userManagement = userManagement;
	}

	@EJB(beanName = "GenericEnvironment")
	public void setReportingEnvDAO(final Environment envDAO) {
		this.environment = envDAO;
		log.info("setReportingEnvDAO()");
	}

	@EJB(beanName = "FileWriterService")
	public void setReportingFileWriter(final FileWriter fileWriter) {
		this.fileWriter = fileWriter;
		log.info("setReportingFileWriter()");
	}

	@EJB(beanName = "ReportingSchedulingService")
	public void setSchedulingService(ReportingSchedulingService schedulingService) {
		this.scheduler = schedulingService;
	}
	
	@EJB(beanName = "ReportingExecutorImpl")
	public void setReportingExecutor(final ReportingExecutor executor) {
		this.reportingJBOSSExecutor = executor;
		log.info("setReportingExecutor");
	}

	@Override
	@PostConstruct
	public void create() throws Exception {
		log.info("Creating reporting management service");
		start();
	}

	@Override
	public void start() throws Exception {
		log.info("Starting reporting management service");

		log.info("Setting command executor");
//		executor = new ReportingCommandManager(new DefaultCommandManager(THREAD_POOL_SIZE));
		executor = new ReportingCommandManager(new DefaultCommandManager(reportingJBOSSExecutor));

		log.info("Setting DAOs");
        ReportingDAOFactory daoFactory = new DbESDAOFactory(REPORTING_FILE_STORAGE, environment, fileWriter);

		log.info("Starting Report manager");
        reportTemplateDAO = daoFactory.newReportTemplateDAO();
		reportDAO  = daoFactory.newReportDao();
		reportTaskDAO = daoFactory.newReportTaskDao();

        ReportTemplateDAO reportTemplateDao = daoFactory.newReportTemplateDAO();
		ReportTaskDAO reportTaskDao = daoFactory.newReportTaskDao();

		for (String template : reportTemplateDao.getReportTemplates()) {
			log.info("Scheduling report tasks for template : " + template + " ...");
			for (ReportTaskTitle task : reportTaskDao.getActiveReportTasks(template)) {
				final ReportTaskInfo taskInfo = reportTaskDao.getReportTaskInfo(task.getTaskId());
				log.info("Scheduling report task : " + taskInfo.getName() + ", cron : " + taskInfo.getCron());
				String taskOwnerName = taskInfo.getOwner();
				
				if (taskOwnerName == null || !RunAsEsUser.isPrincipalExist(taskInfo.getOwner())) {
					continue;
				}
				// make run as call due to report task has an owner
				RunAsEsUser.execute(taskInfo.getOwner(), new RunAsEsUser.Action<Void, RuntimeException>() {
					@Override
					public Void run() {
						try {
							scheduler.validateReportTask(taskInfo);
							scheduler.scheduleReportTask(taskInfo);
						} catch (SchedulingReportingException e) {
                            log.warn("Scheduling report task " + taskInfo.getName() + " failed", e);
                        }
						return null;
					}
				});

			}
		}
	}

	@Override
	@PreDestroy
	public void stop() throws Exception {
		log.info("Stopping reporting management service ...");
		destroy();
	}

	@Override
	public void destroy() throws Exception {
		log.info("Destroying reporting management service ...");
		log.info("Destroyed reporting management service");
	}


   private ReportCommandProxy runReportTask(final ReportTaskInfo taskInfo) throws ReportingException{

        final CompositeCommand complexCommand = new CompositeCommand();

        // Create create/export/send complex command
        new ForEach<>(Arrays.asList(taskInfo)).process(new UnaryFunctor<ReportTaskInfo>() {
            @Override
            public void process(final ReportTaskInfo taskInfo) {
                // Add report generation command
                // we need to hold ref to report cause it is used to manipulate
                // state along with commands chain execution
                final ReportInfo newReportInstance = taskInfo.newReportInstance(System.currentTimeMillis());
                newReportInstance.setUser(getCurrentCaller());

                complexCommand.addCommand(CommandFactory.newReportGenerationCommand(taskInfo, newReportInstance, reportTemplateDAO,
                        reportDAO));
                // Add report export commands
                Collection<String> formats = taskInfo.getExportFormats();
                Set<ExportFormat> formatInstances = CollectionUtils.newLinkedSet();
                for (String formatAsString : formats) {
                    ExportFormat format = ExportFormat.load(formatAsString);
                    if (format == null) {
                        continue;
                    }
                    formatInstances.add(format);
                }

                new ForEach<>(formatInstances).process(new UnaryFunctor<ExportFormat>() {
                    @Override
                    public void process(ExportFormat format) {
                        complexCommand.addCommand(CommandFactory.newReportExportCommand(reportDAO, newReportInstance,
                                format));
                    }
                });
                // Add send email command
                if (!taskInfo.getEmailDestinations().isEmpty()) {
                    complexCommand.addCommand(CommandFactory.newReportMailCommand(reportDAO, taskInfo.getEmailDestinations(), newReportInstance));
                }
            }
        });

        // Process separate export commands
        for (Map.Entry<Integer, Set<String>> entry : taskInfo.getExportTasks().entrySet()) {
            for (String format : entry.getValue()) {
                ExportFormat formatInstance = ExportFormat.load(format);
                complexCommand.addCommand(CommandFactory.newReportExportCommand(reportDAO,
                        reportDAO.getReportInfo(entry.getKey()), formatInstance));
            }
        }

        // Process separate email tasks
        for (Map.Entry<Integer, Set<String>> entry : taskInfo.getEmailTasks().entrySet()) {
            complexCommand.addCommand(CommandFactory.newReportMailCommand(reportDAO, entry.getValue(), reportDAO.getReportInfo(entry.getKey())));
        }

        if (complexCommand.getCommandsSize() == 0) {
            throw new ReportingException("Unable to create empty task");
        }

        if (complexCommand.getCommandsSize() == 1) {
            // No need to create a complex command
            return executor.executeCommand(complexCommand.getCommands().get(0), taskInfo);
        }
        // Finally execute the command
        return executor.executeCommand(complexCommand, taskInfo);
    }

//    @Override
//	public RemoteCommandProxy exportReport(int reportId, String format) throws ReportingException {
//        ReportCommandProxy proxy = runReportTask(ReportTaskInfo.newInstance().addExportTask(reportId, format));
//		return new RemoteCommandProxy(proxy.getCommandName(), proxy.getTaskInfo(), proxy.getCommandId());
//	}

	public Collection<String> getAvailableReportTemplates() {
        return reportTemplateDAO.getReportTemplates();
	}

	@Override
	public Collection<ReportTitle> getGeneratedReports() throws ReportingException {
		Collection<ReportTitle> result = CollectionUtils.newList();
		for (ReportTitle reportTitle : reportDAO.getGeneratedReports()) {
			if (executor.isExecutionCompleted(reportTitle.getReportId())) {
				result.add(reportTitle);
			}
		} 
		return result;
	}

	@Override
	public ReportTemplateInfo getReportTemplateInfo(String reportTemplateName) throws ReportingException {
		if (reportTemplateName == null) {
			throw new IllegalInputParameterException(
					"Report template name shouldn't be null");
		}

		return reportTemplateDAO.getReportTemplateInfo(reportTemplateName);
	}

//	@Override
//	public RemoteCommandProxy emailReports(int reportId, Collection<String> destinations) throws ReportingException {
//		ReportCommandProxy proxy =	runReportTask(ReportTaskInfo.newInstance().addEmailTask(reportId, destinations));
//		return new RemoteCommandProxy(proxy.getCommandName(), proxy.getTaskInfo(), proxy.getCommandId());
//	}

	@Override
	public RemoteCommandProxy runComplexReportTask(ReportTaskInfo context) throws ReportingException, ObjectExistsException {
		if (log.isDebugEnabled()) {
			log.debug("Report task " + context.getName() + " is running under principal " + getCurrentCaller());
		}

		if (context.getTaskId() == 0) {
			reportTaskDAO.saveReportTask(context);
		}

		for (String recipient : context.getEmailRecipients()) {
			context.addEmailDestination(getUserEmail(recipient));
		}

		ReportCommandProxy proxy = runReportTask(context);
		return new RemoteCommandProxy(proxy.getCommandName(), proxy.getTaskInfo(), proxy.getCommandId());
	}

//	@Override
//	public void cancelReportCommand(int commandId) {
//        executor.cancelCommand(commandId);
//	}

    @Override
    public void clearReportCommand(int commandId) {
        executor.clearCommand(commandId);
    }

    @Override
	public double getReportCommandProgress(int commandId) {
        return executor.getReportCommandProgress(commandId);
	}

	@Override
	public CommandStatus getReportCommandStatus(int commandId) {
        return executor.getReportCommandStatus(commandId);
	}

//	@Override
//	public void clearReportCommands() {
//        executor.clearCommands();
//	}

	@Override
	public Collection<RemoteCommandProxy> getReportCommands() {
		Collection<RemoteCommandProxy> remoteStubs = CollectionUtils.newArrayList(0);

		Collection<ReportCommandProxy> proxies = executor.getManagedCommands();
		for (ReportCommandProxy proxy : proxies) {
			RemoteCommandProxy remoteStub = new RemoteCommandProxy(proxy.getCommandName(), proxy.getTaskInfo(), proxy.getCommandId());
			remoteStubs.add(remoteStub);

		}
		return remoteStubs;
	}

	@Override
	public void addReportTemplate(String reportTemplateDesign) throws ReportingException, ObjectExistsException {
		if (reportTemplateDesign == null) {
			throw new IllegalInputParameterException(
					"Report template design shouldn't be null");
		}
		ReportTemplate reportTemplate = new ReportTemplate(reportTemplateDesign);
        reportTemplateDAO.createReportTemplate(new ReportTemplateInfo(reportTemplate.getName(), reportTemplate.getParameters()),
                reportTemplateDesign);
	}

//	@Override
//	public void addReportTemplate(String reportTemplateDesign, boolean system) throws ReportingException {
//        ReportTemplate reportTemplate = new ReportTemplate(reportTemplateDesign);
//        reportTemplateDAO.createReportTemplate(new ReportTemplateInfo(reportTemplate.getName(), reportTemplate.getParameters(),
//                system), reportTemplateDesign);
//	}

//	@Override
//	public String getReportTemplateDesign(String reportTemplateName) throws ReportingException {
//        return reportTemplateDAO.getReportTemplateDesign(reportTemplateName);
//	}

	@Override
	public void removeReportTemplate(String reportTemplateName) throws ReportingException {
		if (reportTemplateName == null) {
			throw new IllegalInputParameterException(
					"Report template name shouldn't be null");
		}
		scheduler.removeScheduledTasks(reportTemplateName);
        reportTemplateDAO.removeReportTemplate(reportTemplateName);
    }

//	@Override
//	public void updateReportTemplate(String reportTemplateName, String reportTemplateDesign) throws ReportingException {
//        ReportTemplate reportTemplate = new ReportTemplate(reportTemplateDesign);
//        ReportTemplateInfo rtInfo = reportTemplateDAO.getReportTemplateInfo(reportTemplateName);
//        rtInfo.clearParameters();
//        rtInfo.setReportTemplateName(reportTemplate.getName());
//        for (ReportParameter param : reportTemplate.getParameters()) {
//            rtInfo.addParameter(param);
//        }
//
//        reportTemplateDAO.updateReportTemplate(rtInfo, reportTemplateDesign);
//	}

	@Override
	public void removeReport(int reportId) throws ReportingException {
        reportDAO.removeReport(reportId);
	}

//	@Override
//	public ReportTaskInfo getReportTask(String taskName) throws ReportingException {
//		return reportTaskDAO.getReportTaskInfo(taskName);
//	}

    @Override
    public ReportTaskInfo getReportTask(int taskId) throws ReportingException {
        return reportTaskDAO.getReportTaskInfo(taskId);
    }

	@Override
	public Collection<ReportTaskInfo> getReportTasks(String reportTemplateName) throws ReportingException {
		Collection<ReportTaskTitle> activeTasks = reportTaskDAO.getActiveReportTasks(reportTemplateName);

		Collection<ReportTaskInfo> result = CollectionUtils.newList();

		for (ReportTaskTitle taskTitle : activeTasks) {
			ReportTaskInfo taskInfo = reportTaskDAO.getReportTaskInfo(taskTitle.getTaskId());
			result.add(taskInfo);
		}

		return result;
	}

	@Override
	public void removeReportTask(int id) throws ReportingException {
		ReportTaskInfo taskInfo = reportTaskDAO.getReportTaskInfo(id);
		if (taskInfo == null) {
			// remove schedule anyway(es model inconsistency extra check)
			scheduler.removeScheduledTask(taskInfo.getName(), taskInfo.getReportTemplateName());
			return;
		}
		scheduler.removeScheduledTask(taskInfo.getName(), taskInfo.getReportTemplateName());
        reportTaskDAO.removeReportTask(taskInfo.getTaskId());
	}

	@Override
	public void scheduleReportTask(ReportTaskInfo taskInfo) throws ReportingException, ObjectExistsException {
		final String callerName = getCurrentCaller();
		taskInfo.setOwner(callerName);

		scheduler.validateReportTask(taskInfo);
		reportTaskDAO.saveReportTask(taskInfo);
		scheduler.scheduleReportTask(taskInfo);
	}

	@Override
	public void updateReportTask(ReportTaskInfo taskInfo) throws ReportingException, ObjectExistsException {
		if (taskInfo == null) {
			throw new IllegalInputParameterException("Supplied 'taskInfo' is null");
		}

		if (taskInfo.getTaskId() <= 0) {
			throw new ReportingException("Failed to update report task. Supplied 'taskInfo' has no taskId specified");
		}

		final String callerName = getCurrentCaller();
		// force to update ownership
		taskInfo.setOwner(callerName);

		ReportTaskInfo existingTask = reportTaskDAO.getReportTaskInfo(taskInfo.getTaskId());
		if (existingTask == null) {
			throw new ReportingException("Failed to update report task. Cannot find report task with id "
			        + taskInfo.getTaskId());
		}

		boolean wasScheduled = scheduler.removeScheduledTask(existingTask.getName(),
		        existingTask.getReportTemplateName());
		if (!wasScheduled) {
			if (log.isDebugEnabled()) {
				log.debug("Cannot remove schedule for task : " + existingTask.getName()
				        + ". Report task with that name has not been scheduled before");
			}
		}

		if(taskInfo.getCron() != null) {
			scheduler.validateReportTask(taskInfo);
		}
		
        if (log.isDebugEnabled()) {
            log.debug("Updating report task using new state : " + taskInfo.toString());
        }
        reportTaskDAO.updateReportTask(taskInfo.getTaskId(), taskInfo);

        if (log.isDebugEnabled()) {
            log.debug("Scheduling updated report task with new cron : " + taskInfo.getCron());
        }
        
        if(taskInfo.getCron() == null) {
        	runComplexReportTask(taskInfo);
        } else {
        	scheduler.scheduleReportTask(taskInfo);
        }
	}

	@Override
	public ReportInfo getReportInfo(int reportId) throws ReportingException {
        return reportDAO.getReportInfo(reportId);
	}

    private String getCurrentCaller() {
        final Principal principal = sessionContext.getCallerPrincipal();
        return principal.getName();
    }

	private Map<String, Object> getUserPreferences() {
		TO<?> user = userManagement.getUser(getCurrentCaller());
		return userManagement.getUserPreferences(user);
	}

	private String getUserEmail(String userName) {
		TO<?> user = userManagement.getUser(userName);
		return (String) userManagement.getProperty(user, userManagement.USER_PRIMARY_EMAIL);
	}

	@Override
	public Object parseReportParameterValue(String value, Class<?> type) {
		if (value == null) {
			throw new IllegalInputParameterException("Parameter value shouldn't be null");
		}
		if (type == null) {
			throw new IllegalInputParameterException("Parameter type shouldn't be null");
		}
		ParameterValueProvider valueProvider = null;
		if (type.isEnum()) {
			valueProvider = parameterValueProviderMap.get(Enum.class);
		} else {
			valueProvider = parameterValueProviderMap.get(type);
		}
		Object parsedValue = valueProvider.getValue(value, getUserPreferences(), type);
		if (!parsedValue.getClass().equals(type)) {
			log.error("Object " + parsedValue.toString() + "has not specified type: " + type.toString());
			throw new ResourceDAOReportingException("Unable to parse value: " + value);
		}
		return parsedValue;
	}

	private static interface ParameterValueProvider {
		Object getValue(String value, Map<String, Object> userPreferences, Class<?> type);
	}

	private static class CollectionTOsValueProvier implements ParameterValueProvider {
		@Override
		public Object getValue(String value, Map<String, Object> userPreferences, Class<?> type) {
			String[] genericTOArray = value.split(",");
			List<TO<?>> objects = CollectionUtils.newList();
			for (String genericTO : genericTOArray) {
				objects.add(TOFactory.getInstance().loadTO(genericTO));
			}
			return objects;
		}
	}

	private static class TOValueProvier implements ParameterValueProvider {
		@Override
		public Object getValue(String value, Map<String, Object> userPreferences, Class<?> type) {
			return TOFactory.getInstance().loadTO(value);
		}
	}

	private static class IntegerValueProvier implements ParameterValueProvider {
		@Override
		public Object getValue(String value, Map<String, Object> userPreferences, Class<?> type) {
			String format = (String) userPreferences.get("DecimalFormat");
			NumberFormat numberFormat = new DecimalFormat(format);
			try {
				return numberFormat.parse(numberFormat.format(Integer.valueOf(value))).intValue();
			} catch (ParseException | NumberFormatException e) {
				log.error(e);
				throw new ResourceDAOReportingException("Unable to parse value: " + value);
			}
		}
	}

	private static class LongValueProvier implements ParameterValueProvider {
		@Override
		public Object getValue(String value, Map<String, Object> userPreferences, Class<?> type) {
			String format = (String) userPreferences.get("DecimalFormat");
			NumberFormat numberFormat = new DecimalFormat(format);
			try {
				return numberFormat.parse(numberFormat.format(Long.valueOf(value)));
			} catch (ParseException | NumberFormatException e) {
				log.error(e);
				throw new ResourceDAOReportingException("Unable to parse value: " + value);
			}
		}
	}

	private static class DateValueProvier implements ParameterValueProvider {
		@Override
		public Object getValue(String value, Map<String, Object> userPreferences, Class<?> type) {
			// Setting format without time zone
			String format = (String) userPreferences.get("DateFormat");
			DateFormat simpleDateFormat = new SimpleDateFormat(format);
			String formattedByUserPreference = simpleDateFormat.format(new Date(Long.valueOf(value)));
			try {
				return simpleDateFormat.parse(formattedByUserPreference);
			} catch (ParseException e) {
				log.error(e.getLocalizedMessage(), e);
				throw new ResourceDAOReportingException("Unable to parse value: " + value);

			}
		}
	}

	private static class BooleanValueProvier implements ParameterValueProvider {
		@Override
		public Object getValue(String value, Map<String, Object> userPreferences, Class<?> type) {
			return Boolean.valueOf(value);
		}
	}

	private static class StringValueProvier implements ParameterValueProvider {
		@Override
		public Object getValue(String value, Map<String, Object> userPreferences, Class<?> type) {
			return value.trim();
		}
	}
	
	private static class SetTOsValueProvier implements ParameterValueProvider {
 		@Override
		public Object getValue(String value, Map<String, Object> userPreferences, Class<?> type) {
			String [] genericTOArray = value.split(",");
			Set<TO<?>> objects = CollectionUtils.newSet();
			for (String genericTO : genericTOArray) {
				objects.add(TOFactory.getInstance().loadTO(genericTO));
			}
			return objects;
 		}
	}
	
	private static class EnumValueProvier implements ParameterValueProvider {
		@Override
		public Object getValue(String value, Map<String, Object> userPreferences,  Class<?> type) {
			return Enum.valueOf((Class<? extends Enum>) type, value);
		}
	}
	
	private static class RangeSetValueProvier implements ParameterValueProvider {
		
		@Override
		public Object getValue(String value, Map<String, Object> userPreferences,  Class<?> type) {
			//Parse predefined or custom Time Period
			try {
				return RangeSet.newRangeSet(new JSONObject(value));
			} catch (JSONException e) {
				throw new IllegalInputParameterException("JSON parameter is not correct: " + value);
			}
		}
	}

}
