package com.synapsense.impl.erlnode.filters;

import org.apache.log4j.Logger;

import com.ericsson.otp.erlang.OtpErlangBinary;
import com.ericsson.otp.erlang.OtpErlangObject;
import com.ericsson.otp.erlang.OtpErlangTuple;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.service.nsvc.filters.TOMatcher;

import com.synapsense.impl.erlnode.utils.StringConverter;

class TOMatcherParser {
	
	private final static Logger log = Logger.getLogger(EventFiltersFactory.class);
	
	public static TOMatcher parse(OtpErlangObject def) {
		if(def==null)
			throw new IllegalArgumentException("Null definition for TO matcher");
		if(!(def instanceof OtpErlangTuple)) {
			throw new IllegalArgumentException("TO matcher definition must be a tuple");
		}
		OtpErlangTuple tpl = (OtpErlangTuple) def;
		if(tpl.arity()!=2) {
			throw new IllegalArgumentException("Only TO matchers with arity 2 are supported");
		}
		
		String matcherArg;
		try {
			matcherArg = StringConverter.utf8BinaryToString(tpl.elementAt(1));
		} catch (ClassCastException e) {
			throw new IllegalArgumentException("The second argument of matcher must be a binary string but was"+
					tpl.elementAt(1).getClass().getName(), e);
		}
		String matcherType = StringConverter.utf8BinaryToString(tpl.elementAt(0));
		
		if ("to".equals(matcherType)) {
			TO<?> to = TOFactory.getInstance().loadTO(matcherArg);
			if(log.isDebugEnabled())
				log.debug("Parsed TO matcher: "+matcherArg);
			return new TOMatcher(to);
		} else if ("regex".equals(matcherType)) {
			if(log.isDebugEnabled())
				log.debug("Parsed regex matcher: \""+matcherArg+"\"");
			return new TOMatcher(matcherArg);
		} else {
			throw new IllegalArgumentException("Unknown TO matcher type: "+matcherType);
		}
	}
}
