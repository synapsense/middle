package com.synapsense.impl.alerting.notification;

import java.io.Serializable;

import com.synapsense.dto.MessageType;
import com.synapsense.service.user.UserContext;

public interface Destination extends Serializable {
	String getAddress(MessageType mType);

	UserContext getUserContext();
}
