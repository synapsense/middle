package com.synapsense.impl.performer.snmp.conf;

import java.net.InetAddress;
import java.util.regex.Pattern;

import com.synapsense.util.algo.Pair;

public class IPAddressSplitter {

	private final static Pattern splitter = Pattern.compile(":");

	static public class IPAddress extends Pair<InetAddress, Integer> {
		/**
		 * 
		 */
		private static final long serialVersionUID = -1683691660148167952L;

		public IPAddress(InetAddress addr, Integer port) {
			super(addr, port);
		}
	}

	public static IPAddress getAddress(String destination) throws SNMPConfigurationException {
		try {
			String[] pieces = splitter.split(destination);
			return new IPAddress(InetAddress.getByName(pieces[0]), Integer.parseInt(pieces[1]));
		} catch (Exception e) {
			throw new SNMPConfigurationException("Cannot parse IP Address:port pair " + destination, e);
		}
	}
}
