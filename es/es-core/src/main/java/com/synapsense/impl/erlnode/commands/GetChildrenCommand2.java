package com.synapsense.impl.erlnode.commands;

import java.util.Collection;
import java.util.Map;

import com.ericsson.otp.erlang.OtpErlangBinary;
import com.ericsson.otp.erlang.OtpErlangList;
import com.ericsson.otp.erlang.OtpErlangObject;
import com.ericsson.otp.erlang.OtpErlangTuple;
import com.synapsense.impl.erlnode.ExceptionTypes;
import com.synapsense.impl.erlnode.utils.CollectionToConverter;
import com.synapsense.impl.erlnode.utils.ToConverter;
import com.synapsense.dto.TO;
import com.synapsense.exception.ObjectNotFoundException;

import com.synapsense.impl.erlnode.utils.StringConverter;

/**
 * @author stikhon
 * 
 */
public class GetChildrenCommand2 extends Command {

	public GetChildrenCommand2(Map<String, Object> state) {
		super(state);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see Command#do_command(com.ericsson.otp.erlang.OtpErlangTuple)
	 */
	@Override
	public OtpErlangObject do_command(OtpErlangTuple command) throws Exception {
		OtpErlangObject result = null;

		OtpErlangObject objectsTuple = command.elementAt(1);
		OtpErlangObject typeTuple = command.elementAt(2);
		if (objectsTuple == null || typeTuple == null
		        || !(objectsTuple instanceof OtpErlangTuple || objectsTuple instanceof OtpErlangList)
		        || !(typeTuple instanceof OtpErlangBinary)) {
			// An empty list represented by "" will cause an exception
			result = makeException(ExceptionTypes.ILLEGAL_ARGUMENTS, null);
		} else if (objectsTuple instanceof OtpErlangTuple && typeTuple instanceof OtpErlangBinary) {
			TO<?> to = ToConverter.parseTo((OtpErlangTuple) objectsTuple);
			String typeNameValue = StringConverter.utf8BinaryToString(typeTuple);
			try {
				Collection<TO<?>> resultCollection = getEnv().getChildren(to, typeNameValue);
				result = makeOkResult(ToConverter.serializeToList(resultCollection));
			} catch (ObjectNotFoundException e) {
				result = makeException(ExceptionTypes.NO_OBJECT_TYPE, e.getMessage());
			}
		} else if (objectsTuple instanceof OtpErlangList && typeTuple instanceof OtpErlangBinary) {
			Collection<TO<?>> toList = ToConverter.parseToList((OtpErlangList) objectsTuple);
			String typeNameValue = StringConverter.utf8BinaryToString(typeTuple);
			result = makeOkResult(CollectionToConverter.serializeCollectionToList(getEnv().getChildren(toList,
			        typeNameValue)));
		}

		return result;
	}
}
