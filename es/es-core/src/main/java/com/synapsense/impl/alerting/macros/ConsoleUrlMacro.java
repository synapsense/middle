package com.synapsense.impl.alerting.macros;

import java.io.StringReader;
import java.util.Collection;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;

public class ConsoleUrlMacro extends AbstractPatternMacro {

	private final static Logger log = Logger.getLogger(ConsoleUrlMacro.class);

	private static final Pattern pattern = Pattern.compile("\\$(?i)console_url\\$");

	private static final String OBJ_TYPE = "CONFIGURATION_DATA";
	private static final ValueTO[] OBJ_FILTER = new ValueTO[] { new ValueTO("name", "WebConsole") };
	private static final String CONF_PROP_NAME = "config";
	private static final String URL_PROP_NAME = "console_url";

	@Override
	public String expandIn(String input, MacroContext ctx) {
		if (input == null)
			return null;
		if (ctx.env == null)
			throw new IllegalArgumentException("MacroContext.env must be not null");
		return super.expandIn(input, ctx);
	}

	@Override
	protected String expand(Matcher m, MacroContext ctx) {
		Collection<TO<?>> objects = ctx.env.getObjects(OBJ_TYPE, OBJ_FILTER);
		if (objects.size() == 0) {
			log.warn("Macro " + m.group() + ": no " + OBJ_TYPE + " objects found with "
			        + OBJ_FILTER[0].getPropertyName() + "=\"" + OBJ_FILTER[0].getValue() + "\"");
			return Matcher.quoteReplacement(Macro.DEFAULT_EXPANSION);
		}
		TO<?> consoleConfigTo = objects.iterator().next();
		String res = Macro.DEFAULT_EXPANSION;
		try {
			String serProps = ctx.env.getPropertyValue(consoleConfigTo, CONF_PROP_NAME, String.class);
			Properties props = new Properties();
			props.load(new StringReader(serProps));
			res = props.getProperty(URL_PROP_NAME);
			if (res == null) {
				log.warn("Macro " + m.group() + ": " + OBJ_TYPE + " object with " + OBJ_FILTER[0].getPropertyName()
				        + "=\"" + OBJ_FILTER[0].getValue() + "\" doesn't contain " + URL_PROP_NAME + " field in its "
				        + CONF_PROP_NAME + " property");
				res = Macro.DEFAULT_EXPANSION;
			}
		} catch (Exception e) {
			log.warn("Macro " + m.group() + ": error while reading " + URL_PROP_NAME + " field in " + CONF_PROP_NAME
			        + " property of " + OBJ_TYPE + " object with " + OBJ_FILTER[0].getPropertyName() + "=\""
			        + OBJ_FILTER[0].getValue() + "\"");
		}
		return Matcher.quoteReplacement(res);
	}

	@Override
	protected Pattern getPattern() {
		return pattern;
	}

}
