package com.synapsense.impl;

import java.io.Serializable;
import java.lang.management.ManagementFactory;
import java.rmi.RemoteException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import javax.ejb.EJB;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.management.MBeanServer;
import javax.management.ObjectName;

import org.apache.log4j.Logger;

import com.synapsense.dal.generic.utils.Util;
import com.synapsense.dto.CollectionTO;
import com.synapsense.dto.EnvObjectBundle;
import com.synapsense.dto.EnvObjectHistory;
import com.synapsense.dto.ObjectType;
import com.synapsense.dto.ObjectTypeMatchMode;
import com.synapsense.dto.PropertyDescr;
import com.synapsense.dto.Relation;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOBundle;
import com.synapsense.dto.TOFactory;
import com.synapsense.dto.Tag;
import com.synapsense.dto.TagDescriptor;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.CalculationFailedException;
import com.synapsense.exception.EnvException;
import com.synapsense.exception.IllegalInputParameterException;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.exception.PropertyNotFoundException;
import com.synapsense.exception.ServerConfigurationException;
import com.synapsense.exception.TagNotFoundException;
import com.synapsense.exception.UnableToConvertPropertyException;
import com.synapsense.exception.UnableToConvertTagException;
import com.synapsense.exception.UnsupportedRelationException;
import com.synapsense.impl.utils.EnvironmentUtils;
import com.synapsense.plugin.DeviceManagerRMI;
import com.synapsense.search.QueryDescription;
import com.synapsense.service.Environment;
import com.synapsense.service.Mapper;
import com.synapsense.service.RulesEngine;
import com.synapsense.service.impl.nsvc.EventNotifier;
import com.synapsense.service.impl.registrar.RegistrarSvc;
import com.synapsense.service.nsvc.events.ConfigurationCompleteEvent;
import com.synapsense.service.nsvc.events.ConfigurationStartedEvent;
import com.synapsense.util.algo.Pair;

/**
 * The class delegates all calls to the environment DelegatingEnvironmentImpl2
 */
// See ejb-jar.xml
//@Depends(value = { "jboss.j2ee:ear=SynapServer.ear,jar=SynapEnvCore.jar,name=Registrar,service=EJB3" })
//@Stateless(name = "DelegatingEnvironment")
//@Remote(Environment.class)
//@TransactionAttribute(value = TransactionAttributeType.NOT_SUPPORTED)
public class DelegatingEnvironmentImpl2 implements Environment {
	private final static Logger logger = Logger.getLogger(DelegatingEnvironmentImpl2.class);
	
	@EJB
	private RegistrarSvc registrar;

	private RulesEngine rulesEngine;

	private Environment impl;

	private Mapper mapper;

	private EventNotifier eventNotifier;

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public PropertyDescr getPropertyDescriptor(String typeName, String propName) throws ObjectNotFoundException,
	        PropertyNotFoundException {
		return impl.getPropertyDescriptor(typeName, propName);
	}

	@EJB(beanName = "GenericEnvironment")
	public void setImpl(Environment impl) {
		this.impl = impl;
	}

	@Override
	public TO<?> createObject(String typeName) throws EnvException {
		return impl.createObject(typeName);
	}

	@Override
	public ObjectType createObjectType(String name) throws EnvException {
		return impl.createObjectType(name);
	}

	@Override
	public void deleteObject(TO<?> objId) throws ObjectNotFoundException {
		impl.deleteObject(objId);
	}

	@Override
	public void deleteObject(TO<?> objId, int level) throws ObjectNotFoundException {
		impl.deleteObject(objId, level);
	}

	@Override
	public void deleteObjectType(String typeName) throws EnvException {
		impl.deleteObjectType(typeName);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public Collection<ValueTO> getAllPropertiesValues(TO<?> objId) throws ObjectNotFoundException {
		if (objId == null)
			throw new IllegalInputParameterException("objID cannot be null");
		ObjectType objectType = getObjectType(objId.getTypeName());
		if (objectType == null) {
			// there is no such object type, therefore an object of this type
			// cannot exist
			throw new ObjectNotFoundException(objId);
		}
		Pair<Set<String>, Set<Pair<String, String>>> mapProps = splitPropsIntoDowncastAndMapped(objId.getTypeName(),
		        EnvironmentUtils.getPropNamesForTypeName(objectType));

		// there are no mapped properties for the TO
		if (mapProps.getSecond().size() == 0)
			return impl.getAllPropertiesValues(objId);

		Collection<TO<?>> tos = new Vector<TO<?>>();
		tos.add(objId);
		return new LinkedList<ValueTO>(getPropertyValue(tos, mapProps).iterator().next().getPropValues());
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public <RETURN_TYPE> Collection<ValueTO> getHistory(TO<?> objId, String propName, Class<RETURN_TYPE> clazz)
	        throws ObjectNotFoundException, PropertyNotFoundException, UnableToConvertPropertyException {
		return impl.getHistory(objId, propName, clazz);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public <RETURN_TYPE> Collection<ValueTO> getHistory(TO<?> objId, String propName, Date start, Date end,
	        Class<RETURN_TYPE> clazz) throws ObjectNotFoundException, PropertyNotFoundException,
	        UnableToConvertPropertyException {
		return impl.getHistory(objId, propName, start, end, clazz);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public EnvObjectHistory getHistory(TO<?> objId, Collection<String> propNames, Date start, Date end)
	        throws ObjectNotFoundException, PropertyNotFoundException {
		return impl.getHistory(objId, propNames, start, end);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public <RETURN_TYPE> Collection<ValueTO> getHistory(TO<?> objId, String propName, int size, int start,
	        Class<RETURN_TYPE> clazz) throws ObjectNotFoundException, PropertyNotFoundException,
	        UnableToConvertPropertyException {
		return impl.getHistory(objId, propName, size, start, clazz);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public <RETURN_TYPE> Collection<ValueTO> getHistory(TO<?> objId, String propName, Date start, int numberPoints,
	        Class<RETURN_TYPE> clazz) throws ObjectNotFoundException, PropertyNotFoundException,
	        UnableToConvertPropertyException {
		return impl.getHistory(objId, propName, start, numberPoints, clazz);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public Integer getHistorySize(TO<?> objId, String propertyName) throws ObjectNotFoundException,
	        PropertyNotFoundException {
		return impl.getHistorySize(objId, propertyName);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public Collection<TO<?>> getObjects(String typeName, ValueTO[] propertyValues) {
		return impl.getObjects(typeName, propertyValues);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public Collection<TO<?>> getObjects(QueryDescription queryDescr) {
		return impl.getObjects(queryDescr);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public Collection<TO<?>> getObjectsByType(String typeName) {
		return impl.getObjectsByType(typeName);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public String[] getObjectTypes() {
		return impl.getObjectTypes();
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public ObjectType[] getObjectTypes(String[] names) {
		return impl.getObjectTypes(names);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public ObjectType getObjectType(String name) {
		return impl.getObjectType(name);
	}

	private boolean checkRulesEngine() {
		/*
		 * if (rulesEngine != null) { return true; } try { InitialContext ctx =
		 * new InitialContext(); rulesEngine = (RulesEngine)
		 * ctx.lookup("java:module/RulesEngineService"); return true; } catch
		 * (NamingException e) { if (logger.isDebugEnabled()) {
		 * logger.debug("Unable to find RulesEngine Service", e); } return
		 * false; }
		 */
		return true;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public <RETURN_TYPE> RETURN_TYPE getPropertyValue(TO<?> objId, String propName, Class<RETURN_TYPE> clazz)
	        throws ObjectNotFoundException, PropertyNotFoundException, UnableToConvertPropertyException {
		RETURN_TYPE result = null;
		// recalculate the property if it is not up to date
		Mapper.Access access = mapper.getAccess(objId.getTypeName(), propName);
		if (access == Mapper.Access.READ || access == Mapper.Access.READWRITE) {
			String mappedName = mapper.getMappedName(objId.getTypeName(), propName);
			DeviceManagerRMI dmProxy = registrar.getComponentProxy("DeviceManager", DeviceManagerRMI.class);
			if (dmProxy == null)
				result = impl.getPropertyValue(objId, propName, clazz);

			try {
				result = clazz.cast(dmProxy.getPropertyValue(TOFactory.getInstance().saveTO(objId), mappedName));
			} catch (RemoteException e) {
				logger.warn("Cannot read property from DM", e);
				result = impl.getPropertyValue(objId, propName, clazz);
			} catch (ClassCastException e) {
				throw new UnableToConvertPropertyException(propName, clazz, e);
			}
		} else {
			if (checkRulesEngine()) {
				try {
					rulesEngine.calculateProperty(objId, propName, false);
				} catch (CalculationFailedException e) {
					logger.warn("Calculation of property " + propName + " for object " + objId.toString() + " failed",
					        e);
				}
			}
			result = impl.getPropertyValue(objId, propName, clazz);
		}
		return result;
	}

	@Override
	public void setAllPropertiesValues(TO<?> objId, ValueTO[] values) throws ObjectNotFoundException,
	        PropertyNotFoundException, UnableToConvertPropertyException {

		if (objId == null) {
			throw new IllegalInputParameterException("Supplied 'objId' is null");
		}

		if (values == null) {
			throw new IllegalInputParameterException("Supplied 'values' is null");
		}

		if (values.length == 0) {
			throw new IllegalInputParameterException("Supplied 'values' is empty");
		}

		// check for null-value references
		for (ValueTO value : values) {
			if (value == null) {
				throw new IllegalInputParameterException("Supplied 'values' has null-valued element");
			}
		}

		List<ValueTO> envValues = propagateToDm(objId, values);

		if (envValues.isEmpty())
			return;

		impl.setAllPropertiesValues(objId, envValues.toArray(new ValueTO[envValues.size()]));
	}

	/**
	 * Filters properties which do exist only at dm side and returns ones which
	 * belong to environment model
	 * 
	 * @param objId
	 * @param values
	 * @return
	 */
	private List<ValueTO> propagateToDm(TO<?> objId, ValueTO[] values) {
		List<ValueTO> valuesToDm = new LinkedList<ValueTO>();
		List<ValueTO> valuesToEnv = new LinkedList<ValueTO>();

		// separate dm and env values
		for (ValueTO value : values) {
			String propName = value.getPropertyName();
			Mapper.Access access = mapper.getAccess(objId.getTypeName(), propName);
			if (access == Mapper.Access.WRITE || access == Mapper.Access.READWRITE) {
				valuesToDm.add(value);
			} else {
				valuesToEnv.add(value);
			}
		}

		if (valuesToDm.isEmpty())
			return valuesToEnv;

		DeviceManagerRMI dmProxy = registrar.getComponentProxy("DeviceManager", DeviceManagerRMI.class);
		if (dmProxy == null)
			return valuesToEnv;

		for (ValueTO valueTo : valuesToDm) {
			Object value = valueTo.getValue();
			if (!(value instanceof Serializable)) {
				logger.warn("Cannot save non-serializable value to DM");
				continue;
			}

			String propName = valueTo.getPropertyName();
			String mappedName = mapper.getMappedName(objId.getTypeName(), propName);
			try {
				dmProxy.setPropertyValue(TOFactory.getInstance().saveTO(objId), mappedName, (Serializable) value);
			} catch (Exception e) {
				logger.warn("Cannot save value to DM", e);
			}
		}

		return valuesToEnv;
	}

	@Override
	public <REQUESTED_TYPE> void setPropertyValue(TO<?> objId, String propName, REQUESTED_TYPE value)
	        throws ObjectNotFoundException, PropertyNotFoundException, UnableToConvertPropertyException {

		if (objId == null) {
			throw new IllegalInputParameterException("Supplied 'objId' is null");
		}

		if (propName == null) {
			throw new IllegalInputParameterException("Supplied 'propName' is null");
		}

		if (propName.isEmpty()) {
			throw new IllegalInputParameterException("Supplied 'propName' is empty");
		}

		if (value != null && value.getClass() == ValueTO.class) {
			setAllPropertiesValues(objId, new ValueTO[] { (ValueTO) value });
		} else {
			ValueTO valueTO = new ValueTO(propName, value, System.currentTimeMillis());
			setAllPropertiesValues(objId, new ValueTO[] { valueTO });
		}

	}

	@Override
	public void updateObjectType(ObjectType objType) throws EnvException {
		impl.updateObjectType(objType);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public Collection<TO<?>> getParents(TO<?> objId, String typeName) throws ObjectNotFoundException {
		return impl.getParents(objId, typeName);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public Collection<TO<?>> getParents(TO<?> objId) throws ObjectNotFoundException {
		return this.impl.getParents(objId);
	}

	@Override
	public void setRelation(TO<?> parentId, TO<?> childId) throws ObjectNotFoundException, UnsupportedRelationException {
		impl.setRelation(parentId, childId);
	}

	@Override
	public void setRelation(List<Relation> relations) throws ObjectNotFoundException, UnsupportedRelationException {
		impl.setRelation(relations);
	}

	@Override
	public TO<?> createObject(String typeName, ValueTO[] propertyValues) throws EnvException {
		return impl.createObject(typeName, propertyValues);
	}

	@Override
	public TOBundle createObjects(EnvObjectBundle objectsToCreate) throws EnvException {
		return impl.createObjects(objectsToCreate);
	}

	@Override
	public void removeRelation(TO<?> parentId, TO<?> childId) throws ObjectNotFoundException {
		impl.removeRelation(parentId, childId);
	}

	@Override
	public void removeRelation(List<Relation> relations) throws ObjectNotFoundException {
		impl.removeRelation(relations);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public Collection<TO<?>> getChildren(TO<?> objId, String typeName) throws ObjectNotFoundException {
		return impl.getChildren(objId, typeName);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public Collection<TO<?>> getChildren(TO<?> objId) throws ObjectNotFoundException {
		return impl.getChildren(objId);
	}

	@Override
	public <REQUESTED_TYPE> void addPropertyValue(TO<?> objId, String propName, REQUESTED_TYPE value)
	        throws ObjectNotFoundException, PropertyNotFoundException, UnableToConvertPropertyException {
		impl.addPropertyValue(objId, propName, value);
	}

	@Override
	public <REQUESTED_TYPE> void removePropertyValue(TO<?> objId, String propName, REQUESTED_TYPE value)
	        throws ObjectNotFoundException, PropertyNotFoundException, UnableToConvertPropertyException {
		impl.removePropertyValue(objId, propName, value);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public long getPropertyTimestamp(TO<?> objId, String propName) throws ObjectNotFoundException,
	        PropertyNotFoundException {
		if (objId == null) {
			throw new IllegalInputParameterException("Supplied 'objId' is null");
		}
		if (propName == null) {
			throw new IllegalInputParameterException("Supplied 'propName' is null");
		}
		if (propName.isEmpty()) {
			throw new IllegalInputParameterException("Supplied 'propName' is empty");
		}
		Mapper.Access access = mapper.getAccess(objId.getTypeName(), propName);
		if (access == Mapper.Access.READ || access == Mapper.Access.READWRITE) {
			return System.currentTimeMillis();
		} else {
			return impl.getPropertyTimestamp(objId, propName);
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public Collection<CollectionTO> getAllPropertiesValues(Collection<TO<?>> objIds) {
		if (objIds == null)
			throw new IllegalInputParameterException("objIds cannot be null");
		if (objIds.size() == 0)
			throw new IllegalInputParameterException("objIds cannot be empty");
		if (Util.containsNull(objIds)) {
			throw new IllegalInputParameterException("Supplied 'objIds' has null-valued element");
		}

		String typeName = objIds.iterator().next().getTypeName();
		Pair<Set<String>, Set<Pair<String, String>>> mapProps = splitPropsIntoDowncastAndMapped(typeName,
		        EnvironmentUtils.getPropNamesForTypeName(getObjectType(typeName)));

		if (mapProps.getSecond().size() == 0)
			return impl.getAllPropertiesValues(objIds);

		return getPropertyValue(objIds, mapProps);
	}

	/**
	 * Splits properties list into 2 sublists - non-mapped and mapped
	 * 
	 * @param typeName
	 * @param propNames
	 * @return pair of non-mapped property names (first element) and pairs of
	 *         native and mapped ones (second arg)
	 */
	private Pair<Set<String>, Set<Pair<String, String>>> splitPropsIntoDowncastAndMapped(String typeName,
	        String[] propNames) {
		if (typeName == null)
			throw new IllegalInputParameterException("typeName cannot be null");
		if (propNames == null)
			throw new IllegalInputParameterException("propNames cannot be null");
		if (propNames.length == 0)
			throw new IllegalInputParameterException("propNames cannot be null");

		Set<String> downcastProps = new HashSet<String>(); // using bulk read
		                                                   // for these props
		Set<Pair<String, String>> mappedProps = new HashSet<Pair<String, String>>(); // using
		                                                                             // one-by-one
		// DM calls for
		// these props;
		// downcastProps.addAll(Arrays.asList(propNames));

		for (String propName : propNames) {
			String mappedName = mapper.getMappedName(typeName, propName);
			if (mappedName == null) {
				downcastProps.add(propName);
			} else {
				mappedProps.add(new Pair<String, String>(propName, mappedName));
			}
		}
		return new Pair<Set<String>, Set<Pair<String, String>>>(downcastProps, mappedProps);
	}

	/**
	 * Internal generic implementation of getPropertyValue
	 * 
	 * @param objIds
	 *            - list of TOs to read property values from
	 * @param props
	 *            - pair of non-mapped properties(first arg) and mapped ones
	 *            (second arg)
	 * @return
	 */
	private Collection<CollectionTO> getPropertyValue(Collection<TO<?>> objIds,
	        Pair<Set<String>, Set<Pair<String, String>>> props) {
		if (objIds == null)
			throw new IllegalInputParameterException("objIDs cannot be null");
		if (objIds.size() == 0)
			throw new IllegalInputParameterException("objIds cannot be empty");
		if (!EnvironmentUtils.areTOsWithSameType(objIds))
			throw new IllegalInputParameterException("objIds dont have the same object type");
		if (props == null)
			throw new IllegalInputParameterException("propNames cannot be null");

		Set<String> downcastProps = props.getFirst();
		// Recalculate the properties if necessary
		if (checkRulesEngine()) {
			// by performance consideration
			// we should avoid expensive EJB calling
			// now we just call calculateProperties one time where CRE decide by
			// itself to recalculate or no.
			// instead of checking every time by CRE isCalculated()
			rulesEngine.calculateProperties(objIds, downcastProps);
		}

		Set<Pair<String, String>> mappedProps = props.getSecond();

		Collection<CollectionTO> result = impl.getPropertyValue(objIds,
		        downcastProps.toArray(new String[downcastProps.size()]));
		if (mappedProps.size() != 0) {
			DeviceManagerRMI dmProxy = registrar.getComponentProxy("DeviceManager", DeviceManagerRMI.class);
			if (dmProxy == null)
				return result;

			for (TO<?> objID : objIds) {
				CollectionTO currCollection = EnvironmentUtils.lookForTOInCollection(result, objID);
				for (Pair<String, String> propPair : mappedProps) {
					String nativeName = propPair.getFirst();
					String mappedName = propPair.getSecond();
					try {
						Object value = dmProxy.getPropertyValue(TOFactory.getInstance().saveTO(objID), mappedName);
						currCollection.addValue(new ValueTO(nativeName, value, System.currentTimeMillis()));
					} catch (Exception e) {
						logger.warn("Cannot read property " + propPair + " from DM.", e);
						PropertyDescr pd;
						try {
							pd = getPropertyDescriptor(objID.getTypeName(), propPair.getFirst());
							currCollection.addValue(new ValueTO(nativeName, impl.getPropertyValue(objID, nativeName,
							        pd.getType()), System.currentTimeMillis()));
						} catch (Exception e1) {
							logger.warn("Cannot read property " + propPair + " from Environment.", e1);
							currCollection.addValue(new ValueTO(nativeName, null, System.currentTimeMillis()));
						}
					}
				}
			}
		}
		return result;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public Collection<CollectionTO> getPropertyValue(Collection<TO<?>> objIds, String[] propNames) {
		if (objIds == null)
			throw new IllegalInputParameterException("objIDs cannot be null");
		if (objIds.size() == 0)
			throw new IllegalInputParameterException("objIds cannot be empty");

		String typeName = objIds.iterator().next().getTypeName();

		return getPropertyValue(objIds, splitPropsIntoDowncastAndMapped(typeName, propNames));
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public Collection<TO<?>> getRelatedObjects(TO<?> objId, String objectType, boolean isChild) {
		return this.impl.getRelatedObjects(objId, objectType, isChild);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public Collection<CollectionTO> getChildren(Collection<TO<?>> objIds, String typeName) {
		return this.impl.getChildren(objIds, typeName);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public Collection<CollectionTO> getChildren(Collection<TO<?>> objIds) {
		return this.impl.getChildren(objIds);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public <RETURN_TYPE> RETURN_TYPE getPropertyValue(String typeName, String propName, Class<RETURN_TYPE> clazz)
	        throws ObjectNotFoundException, PropertyNotFoundException, UnableToConvertPropertyException {
		return impl.getPropertyValue(typeName, propName, clazz);
	}

	@Override
	public <REQUESTED_TYPE> void setPropertyValue(String typeName, String propName, REQUESTED_TYPE value)
	        throws ObjectNotFoundException, PropertyNotFoundException, UnableToConvertPropertyException {
		impl.setPropertyValue(typeName, propName, value);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public Collection<TO<?>> getObjects(String typeName, ObjectTypeMatchMode matchMode, ValueTO[] propertyValues) {
		return impl.getObjects(typeName, matchMode, propertyValues);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public Collection<TO<?>> getObjectsByType(String typeName, ObjectTypeMatchMode matchMode) {
		return impl.getObjectsByType(typeName, matchMode);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void configurationComplete() throws ServerConfigurationException {

		try {
			registrar.configureComponent("DeviceManager");
		} catch (Exception e) {
			// just log it because we need to restart DM queue (further lines)
			// anyway
			// allowing MapSense to call configurationStart() again if needed
			logger.warn("Cannot configure Device Manager", e);
		}

		try {

			ObjectName dmTopic = new ObjectName("org.hornetq:module=JMS,type=Queue,name=\"dm\"");
			MBeanServer mbeanSrv = ManagementFactory.getPlatformMBeanServer();

			mbeanSrv.invoke(dmTopic, "resume", new Object[] {}, new String[] {});

			// check if it's not already paused (to comply with MapSense
			// contract)
			Boolean paused = (Boolean) mbeanSrv.getAttribute(dmTopic, "Paused");
			if (!paused) {
				logger.info("The services have been started.");
			} else {
				logger.error("dm queue cannot be restarted");
			}

		} catch (Exception e) {
			logger.warn("Cannot restart component. ", e);
			throw new ServerConfigurationException(e);
		} finally {
			// TODO: This event should be sent in DLImportService once it is
			// implemented
			eventNotifier.notify(new ConfigurationCompleteEvent());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void configurationStart() throws ServerConfigurationException {

		try {
			// TODO: This event should be sent in DLImportService once it is
			// implemented
			eventNotifier.notify(new ConfigurationStartedEvent());

			// The only service we have left here to suspend is processing of
			// JMS messages
			// coming from DM via JMS topic.
			// JMS implementation shipped with JBoss 7.x is HornetQ. It can be
			// managed in two ways:
			// 1) via JBoss Detyped Management Model (like any other JBoss
			// subsystem)
			// 2) via HornetQ JMX management interface
			// I opt for JMX since it is JBoss agnostic.
			// Note: HornetQ management JMX is disabled by default (at least in
			// JBoss 7.1.0.CR1).
			// In order to enable it place
			// <jmx-management-enabled>true</jmx-management-enabled>
			// option inside <subsystem
			// xmlns="urn:jboss:domain:messaging:1.1">/<hornetq-server>
			// element in either standalone.xml or domain.xml file (depends on
			// your deployment)

			// Note 2: due to some reasons HornetQ topics cannot be "paused",
			// only queues can be.
			// Thus I have changed dm from topic to queue

			ObjectName dmTopic = new ObjectName("org.hornetq:module=JMS,type=Queue,name=\"dm\"");
			MBeanServer mbeanSrv = ManagementFactory.getPlatformMBeanServer();

			// check if it's not already paused (to comply with MapSense
			// contract)
			Boolean paused = (Boolean) mbeanSrv.getAttribute(dmTopic, "Paused");
			if (paused) {
				logger.info("The services are currently stopped. Unable to start new export process at this moment...");
				throw new IllegalStateException("Unable to complete requested operation: service is not running");
			}

			mbeanSrv.invoke(dmTopic, "pause", new Object[] {}, new String[] {});

		} catch (IllegalStateException e) {
			throw e;
		} catch (Exception e) {
			logger.warn("Cannot disable some of the services", e);
			throw new ServerConfigurationException(e);
		}
	}

	public RulesEngine getRulesEngine() {
		return rulesEngine;
	}

	@EJB
	public void setRulesEngine(RulesEngine rulesEngine) {
		this.rulesEngine = rulesEngine;
	}

	@Override
	public void setAllPropertiesValues(TO<?> objId, ValueTO[] values, boolean persist) throws ObjectNotFoundException,
	        PropertyNotFoundException, UnableToConvertPropertyException {
		impl.setAllPropertiesValues(objId, values, persist);

	}

	@Override
	public void setAllPropertiesValues(TO<?> objId, Collection<ValueTO> values, Collection<Tag> tags)
	        throws EnvException {
		impl.setAllPropertiesValues(objId, values, tags);
	}

	@Override
	public <REQUESTED_TYPE> void setPropertyValue(TO<?> objId, String propName, REQUESTED_TYPE value, boolean persist)
	        throws PropertyNotFoundException, ObjectNotFoundException, UnableToConvertPropertyException {
		impl.setPropertyValue(objId, propName, value, persist);
	}

	@Override
	public boolean exists(TO<?> objId) {
		return impl.exists(objId);
	}

	@Override
	public TagDescriptor getTagDescriptor(String typeName, String propName, String tagName)
	        throws ObjectNotFoundException, PropertyNotFoundException, TagNotFoundException {
		return impl.getTagDescriptor(typeName, propName, tagName);
	}

	@Override
	public <RETURN_TYPE> RETURN_TYPE getTagValue(TO<?> objId, String propName, String tagName, Class<RETURN_TYPE> clazz)
	        throws ObjectNotFoundException, PropertyNotFoundException, TagNotFoundException,
	        UnableToConvertTagException {
		return impl.getTagValue(objId, propName, tagName, clazz);
	}

	@Override
	public Collection<Tag> getAllTags(TO<?> objId, String propName) throws ObjectNotFoundException,
	        PropertyNotFoundException {
		return impl.getAllTags(objId, propName);
	}

	@Override
	public Collection<Tag> getAllTags(TO<?> objId) throws ObjectNotFoundException {
		return impl.getAllTags(objId);
	}

	@Override
	public Map<TO<?>, Collection<Tag>> getAllTags(String typeName) {
		return impl.getAllTags(typeName);
	}

	@Override
	public void setTagValue(TO<?> objId, String propName, String tagName, Object value)
	        throws PropertyNotFoundException, ObjectNotFoundException, TagNotFoundException,
	        UnableToConvertTagException {
		impl.setTagValue(objId, propName, tagName, value);
	}

	@Override
	public Map<TO<?>, Map<String, Collection<Tag>>> getTags(Collection<TO<?>> objIds, List<String> propertyNames,
	        List<String> tagNames) {
		return impl.getTags(objIds, propertyNames, tagNames);
	}

	@Override
	public void setAllTagsValues(TO<?> objId, Collection<Tag> tagsToSet) throws PropertyNotFoundException,
	        ObjectNotFoundException, TagNotFoundException, UnableToConvertTagException {
		impl.setAllTagsValues(objId, tagsToSet);
	}

	public EventNotifier getEventNotifier() {
		return eventNotifier;
	}

	@EJB(beanName = "LocalNotificationService")
	public void setEventNotifier(EventNotifier eventNotifier) {
		this.eventNotifier = eventNotifier;
	}

	@EJB
	public void setMapper(Mapper mapper) {
		this.mapper = mapper;
	}

	public Environment getImpl() {
		return impl;
	}


}
