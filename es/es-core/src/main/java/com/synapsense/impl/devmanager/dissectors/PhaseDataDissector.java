package com.synapsense.impl.devmanager.dissectors;

import org.apache.log4j.Logger;

import com.synapsense.dto.TO;
import com.synapsense.plugin.messages.GenericMessage;
import com.synapsense.plugin.messages.GenericMessage.GenericMessageTypes;
import com.synapsense.plugin.messages.PowerDataBuilder.FieldConstants;
import com.synapsense.service.impl.messages.base.PhaseData;
import com.synapsense.util.algo.Pair;
import com.synapsense.util.algo.UnaryFunctor;

public class PhaseDataDissector implements MessageDissector {

	private static final Logger log = Logger.getLogger(PhaseDataDissector.class);

	private UnaryFunctor<Pair<TO<?>, PhaseData>> functor;

	public PhaseDataDissector(UnaryFunctor<Pair<TO<?>, PhaseData>> functor) {
		this.functor = functor;
	}

	@Override
	public boolean tasteMessage(GenericMessage msg) {
		return msg.getType() == GenericMessageTypes.PHASE_DATA;
	}

	@Override
	public void process(TO<?> objID, GenericMessage msg) {
		if (msg == null)
			throw new IllegalArgumentException("msg cannot be null");
		if (!tasteMessage(msg))
			throw new IllegalArgumentException("Incompatible message type");

		PhaseData phaseData = new PhaseData((Integer) msg.get(FieldConstants.RACK.name()),
		        (Integer) msg.get(FieldConstants.RPDU.name()), (Integer) msg.get(FieldConstants.PHASE.name()),
		        (Double) msg.get(FieldConstants.VOLTAGE.name()), (Long) msg.get(FieldConstants.TIMESTAMP.name()));
		phaseData.setAvgCurrent(checkAndGetValue(FieldConstants.AVG_CURRENT.name(), msg));
		phaseData.setMaxCurrent(checkAndGetValue(FieldConstants.MAX_CURRENT.name(), msg));
		phaseData.setDemandPower(checkAndGetValue(FieldConstants.DEMANDPOWER.name(), msg));

		if (log.isDebugEnabled()) {
			log.debug("Received Phase data packet. " + phaseData);
		}

		functor.process(new Pair<TO<?>, PhaseData>(objID, phaseData));
	}

	private Double checkAndGetValue(String name, GenericMessage msg) {
		Object val = msg.get(name);
		if (val == null) {
			return null;
		}
		return (Double) val;
	}
}
