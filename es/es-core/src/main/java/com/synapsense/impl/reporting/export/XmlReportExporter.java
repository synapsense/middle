package com.synapsense.impl.reporting.export;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.export.JRXmlExporter;
import net.sf.jasperreports.engine.export.JRXmlExporterParameter;

import com.synapsense.impl.reporting.dao.ResourceDAOReportingException;
import com.synapsense.impl.reporting.model.JSReport;
import com.synapsense.impl.reporting.model.Report;
import com.synapsense.service.FileWriter;
import com.synapsense.service.FileWriter.WriteMode;
import com.synapsense.service.reporting.exception.ExportingReportingException;
import com.synapsense.service.reporting.exception.ReportingRuntimeException;

public class XmlReportExporter implements ReportExporter {

	@Override
	public void export(JSReport report, String outFileName) throws ExportingReportingException {
		try {
			JasperExportManager.exportReportToXmlFile(report.getJasperPrint(), outFileName, false);
		} catch (JRException e) {
			throw new ExportingReportingException("Unable to export \"" + report.getJasperPrint().getName()
			        + "\"report to xml");
		}
	}

	@Override
	public void export(Report report, String outFilePath) throws ExportingReportingException {
		throw new ReportingRuntimeException("Dispatching error when exporting report");
	}

	@Override
	public void export(JSReport report, String outFileName,
			FileWriter fileWriter) throws ExportingReportingException {
		
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		JRXmlExporter exporter = new JRXmlExporter();
		exporter.setParameter(JRExporterParameter.JASPER_PRINT, report.getJasperPrint());
		exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, outputStream);
		exporter.setParameter( JRXmlExporterParameter.IS_EMBEDDING_IMAGES, java.lang.Boolean.TRUE );
		
		try {
			exporter.exportReport();
		} catch (JRException e) {
			throw new ResourceDAOReportingException("Exported File can't be found");
		}
		try {
			fileWriter.writeFileBytes(outFileName, outputStream.toByteArray(), WriteMode.REWRITE);
		} catch (IOException e) {
			throw new ResourceDAOReportingException("Failed to write file bytes" + outFileName
					+ " with mode " + WriteMode.REWRITE, e);
		}
 
	}
}
