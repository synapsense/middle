package com.synapsense.impl.alerting.notification;

import java.util.List;

import com.synapsense.dto.Message;
import com.synapsense.impl.alerting.macros.MacroContext;

public interface NotificationTemplate {
	List<Message> generateMessages(String itemKey, MacroContext ctx);

	List<Message> generateResolveMessages(String itemKey, MacroContext ctx);

	List<Message> generateAckMessages(String itemKey, MacroContext ctx);

	Notification createNotification(String id, MacroContext ctx);

	Notification restoreNotification(String id, MacroContext ctx);
}