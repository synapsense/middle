package com.synapsense.impl.reporting.commandengine;

import java.util.Collection;

import com.synapsense.service.reporting.dto.CommandStatus;

public interface CommandManager {

	CommandProxy executeCommand(Command command);

	Collection<CommandProxy> getManagedCommands();

	void clearCommands();

	void clearCommand(int id);

	void cancelCommand(int id);

	CommandStatus getCommandStatus(int id);

	double getCommandProgress(int id);

	String getCommandName(int id);

	String getCommandDescription(int id);

	double getReportCommandProgress(int id);

	CommandStatus getReportCommandStatus(int id);

	boolean isExecutionCompleted(int reportId);

}