package com.synapsense.impl.reporting.export;

import java.awt.Dimension;
import java.awt.geom.Dimension2D;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import org.w3c.tools.codec.Base64Encoder;

import net.sf.jasperreports.engine.ImageMapRenderable;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JRPen;
import net.sf.jasperreports.engine.JRPrintImage;
import net.sf.jasperreports.engine.JRWrappingSvgRenderer;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.Renderable;
import net.sf.jasperreports.engine.RenderableUtil;
import net.sf.jasperreports.engine.export.JRExporterGridCell;
import net.sf.jasperreports.engine.export.JRHtmlExporter;
import net.sf.jasperreports.engine.export.JRHtmlExporterParameter;
import net.sf.jasperreports.engine.type.ModeEnum;
import net.sf.jasperreports.engine.type.RenderableTypeEnum;
import net.sf.jasperreports.engine.type.ScaleImageEnum;
import net.sf.jasperreports.engine.util.JRColorUtil;
import net.sf.jasperreports.engine.util.JRStringUtil;
import com.synapsense.impl.reporting.dao.ResourceDAOReportingException;
import com.synapsense.impl.reporting.model.JSReport;
import com.synapsense.impl.reporting.model.Report;
import com.synapsense.service.FileWriter;
import com.synapsense.service.FileWriter.WriteMode;
import com.synapsense.service.reporting.exception.ExportingReportingException;
import com.synapsense.service.reporting.exception.ReportingRuntimeException;

public class HtmlReportExporter extends JRHtmlExporter implements ReportExporter {
	
	protected static final String DEFAULT_HTML_ENCODING = "UTF-8";
	
	@Override
	public void export(JSReport report, String outFileName) throws ExportingReportingException {
		try {
			JasperExportManager.exportReportToHtmlFile(report.getJasperPrint(), outFileName);
		} catch (JRException e) {
			throw new ExportingReportingException("Unable to export \"" + report.getJasperPrint().getName()
			        + "\"report to html");
		}
	}

	@Override
	public void export(Report report, String outFilePath) throws ExportingReportingException {
		throw new ReportingRuntimeException("Dispatching error when exporting report");
	}

	@Override
	public void export(JSReport report, String outFileName,
			FileWriter fileWriter) throws ExportingReportingException {
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		setParameter(JRExporterParameter.JASPER_PRINT, report.getJasperPrint());
		setParameter(JRExporterParameter.OUTPUT_STREAM, outputStream);
		setParameter( JRHtmlExporterParameter.IS_USING_IMAGES_TO_ALIGN, java.lang.Boolean.FALSE );
		setParameter( JRHtmlExporterParameter.IS_OUTPUT_IMAGES_TO_DIR, java.lang.Boolean.FALSE );
		try {
			exportReport();
		} catch (JRException e) {
			throw new ResourceDAOReportingException("Exported File can't be found");
		}
 		try {
 			fileWriter.writeFileBytes(outFileName, outputStream.toByteArray(), WriteMode.REWRITE);
		} catch (IOException e) {
			throw new ResourceDAOReportingException("Failed to write file bytes" + outFileName
					+ " with mode " + WriteMode.REWRITE, e);
		}
	}
	
	private void exportEmbeddedImage(JRPrintImage image, JRExporterGridCell gridCell) throws JRException, IOException {
		
		Renderable renderer = image.getRenderable();
		if (renderer != null)
		{
			boolean isEmbeddingImages = true;
			String imageSource = "";
			
			if (renderer.getTypeValue() == RenderableTypeEnum.SVG)
			{
				renderer = 
					new JRWrappingSvgRenderer(
						renderer, 
						new Dimension(image.getWidth(), image.getHeight()),
						ModeEnum.OPAQUE == image.getModeValue() ? image.getBackcolor() : null
						);
			}
				
			if (isEmbeddingImages)
			{
				try
				{
					ByteArrayInputStream bais = new ByteArrayInputStream(renderer.getImageData(jasperReportsContext));
					ByteArrayOutputStream baos = new ByteArrayOutputStream();
					
					Base64Encoder encoder = new Base64Encoder(bais, baos);
					encoder.process();
					
					imageSource = new String(baos.toByteArray(), DEFAULT_HTML_ENCODING);
				}
				catch (IOException e)
				{
					throw new JRException("Error embedding image into HTML.", e);
				}
			}
			writeImageData(imageSource);
		}
		
	}
	
	@Override
	protected void exportImage(JRPrintImage image, JRExporterGridCell gridCell) throws JRException, IOException {
		writeCellStart(gridCell);

		StringBuffer styleBuffer = new StringBuffer();

		String horizontalAlignment = CSS_TEXT_ALIGN_LEFT;

		switch (image.getHorizontalAlignmentValue())
		{
			case RIGHT :
			{
				horizontalAlignment = CSS_TEXT_ALIGN_RIGHT;
				break;
			}
			case CENTER :
			{
				horizontalAlignment = CSS_TEXT_ALIGN_CENTER;
				break;
			}
			case LEFT :
			default :
			{
				horizontalAlignment = CSS_TEXT_ALIGN_LEFT;
			}
		}

		if (!horizontalAlignment.equals(CSS_TEXT_ALIGN_LEFT))
		{
			styleBuffer.append("text-align: ");
			styleBuffer.append(horizontalAlignment);
			styleBuffer.append(";");
		}

		String verticalAlignment = HTML_VERTICAL_ALIGN_TOP;

		switch (image.getVerticalAlignmentValue())
		{
			case BOTTOM :
			{
				verticalAlignment = HTML_VERTICAL_ALIGN_BOTTOM;
				break;
			}
			case MIDDLE :
			{
				verticalAlignment = HTML_VERTICAL_ALIGN_MIDDLE;
				break;
			}
			case TOP :
			default :
			{
				verticalAlignment = HTML_VERTICAL_ALIGN_TOP;
			}
		}

		if (!verticalAlignment.equals(HTML_VERTICAL_ALIGN_TOP))
		{
			styleBuffer.append(" vertical-align: ");
			styleBuffer.append(verticalAlignment);
			styleBuffer.append(";");
		}

		appendBackcolorStyle(gridCell, styleBuffer);
		
		boolean addedToStyle = appendBorderStyle(gridCell.getBox(), styleBuffer);
		if (!addedToStyle)
		{
			appendPen(
				styleBuffer,
				image.getLinePen(),
				null
				);
		}

		appendPaddingStyle(image.getLineBox(), styleBuffer);

		if (styleBuffer.length() > 0)
		{
			writer.write(" style=\"");
			writer.write(styleBuffer.toString());
			writer.write("\"");
		}

		writer.write(">");

		if (image.getAnchorName() != null)
		{
			writer.write("<a name=\"");
			writer.write(image.getAnchorName());
			writer.write("\"/>");
		}
		
		Renderable renderer = image.getRenderable();
		boolean imageMapRenderer = renderer != null 
				&& renderer instanceof ImageMapRenderable
				&& ((ImageMapRenderable) renderer).hasImageAreaHyperlinks();

		boolean hasHyperlinks = false;

		if(renderer != null || isUsingImagesToAlign)
		{
			if (imageMapRenderer)
			{
				hasHyperlinks = true;
				hyperlinkStarted = false;
			}
			else
			{
				hasHyperlinks = startHyperlink(image);
			}
			
			writer.write("<img");
	
			ScaleImageEnum scaleImage = image.getScaleImageValue();
			

			//Write image data
			
			writer.write(" src=\"");
			
			exportEmbeddedImage(image, gridCell);

			writer.write("\"");
		
			int imageWidth = image.getWidth() - image.getLineBox().getLeftPadding().intValue() - image.getLineBox().getRightPadding().intValue();
			if (imageWidth < 0)
			{
				imageWidth = 0;
			}
		
			int imageHeight = image.getHeight() - image.getLineBox().getTopPadding().intValue() - image.getLineBox().getBottomPadding().intValue();
			if (imageHeight < 0)
			{
				imageHeight = 0;
			}
		
			switch (scaleImage)
			{
				case FILL_FRAME :
				{
					writer.write(" style=\"width: ");
					writer.write(toSizeUnit(imageWidth));
					writer.write("; height: ");
					writer.write(toSizeUnit(imageHeight));
					writer.write("\"");
		
					break;
				}
				case CLIP : 
				case RETAIN_SHAPE :
				default :
				{
					double normalWidth = imageWidth;
					double normalHeight = imageHeight;
		
					if (!image.isLazy())
					{
						// Image load might fail. 
						Renderable tmpRenderer = 
							RenderableUtil.getInstance(jasperReportsContext).getOnErrorRendererForDimension(renderer, image.getOnErrorTypeValue());
						Dimension2D dimension = tmpRenderer == null ? null : tmpRenderer.getDimension(jasperReportsContext);
						// If renderer was replaced, ignore image dimension.
						if (tmpRenderer == renderer && dimension != null)
						{
							normalWidth = dimension.getWidth();
							normalHeight = dimension.getHeight();
						}
					}
		
					if (imageHeight > 0)
					{
						double ratio = normalWidth / normalHeight;
		
						if( ratio > (double)imageWidth / (double)imageHeight )
						{
							writer.write(" style=\"width: ");
							writer.write(toSizeUnit(imageWidth));
							writer.write("\"");
						}
						else
						{
							writer.write(" style=\"height: ");
							writer.write(toSizeUnit(imageHeight));
							writer.write("\"");
						}
					}
				}
			}
			
			writer.write(" alt=\"\"");
			
			if (hasHyperlinks)
			{
				writer.write(" border=\"0\"");
			}
			
			if (image.getHyperlinkTooltip() != null)
			{
				writer.write(" title=\"");
				writer.write(JRStringUtil.xmlEncode(image.getHyperlinkTooltip()));
				writer.write("\"");
			}
			
			writer.write("/>");

			endHyperlink();
			
		}
		writeCellEnd(gridCell);
	}
	
	private boolean appendPen(StringBuffer sb, JRPen pen, String side)
	{
		boolean addedToStyle = false;
		
		float borderWidth = pen.getLineWidth().floatValue();
		if (0f < borderWidth && borderWidth < 1f)
		{
			borderWidth = 1f;
		}

		String borderStyle = null;
		switch (pen.getLineStyleValue())
		{
			case DOUBLE :
			{
				borderStyle = "double";
				break;
			}
			case DOTTED :
			{
				borderStyle = "dotted";
				break;
			}
			case DASHED :
			{
				borderStyle = "dashed";
				break;
			}
			case SOLID :
			default :
			{
				borderStyle = "solid";
				break;
			}
		}

		if (borderWidth > 0f)
		{
			sb.append("border");
			if (side != null)
			{
				sb.append("-");
				sb.append(side);
			}

			sb.append(": ");
			sb.append(toSizeUnit((int)borderWidth));
			
			sb.append(" ");
			sb.append(borderStyle);

			sb.append(" #");
			sb.append(JRColorUtil.getColorHexa(pen.getLineColor()));
			sb.append("; ");

			addedToStyle = true;
		}

		return addedToStyle;
	}
	
	private void writeImageData(String data) throws IOException
	{
		StringBuffer buffer = new StringBuffer();
		if (data != null)
		{
			buffer.append("data:image/png;base64,\n");
			buffer.append(data);
			writer.write(buffer.toString());
			buffer = null;
		}
	}
}
