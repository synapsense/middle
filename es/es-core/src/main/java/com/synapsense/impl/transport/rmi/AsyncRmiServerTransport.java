package com.synapsense.impl.transport.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.ExportException;
import java.rmi.server.UID;
import java.rmi.server.UnicastRemoteObject;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import javax.naming.InitialContext;

import org.apache.log4j.Logger;

import com.synapsense.impl.transport.rmi.strategies.RetriesFailStrategy;
import com.synapsense.service.nsvc.EventProcessor;
import com.synapsense.service.nsvc.NotificationService;
import com.synapsense.transport.ServerTransport;
import com.synapsense.transport.rmi.AsyncRmiNotificationClient;
import com.synapsense.transport.rmi.AsyncRmiNotificationService;
import com.synapsense.transport.rmi.Constants;

public class AsyncRmiServerTransport implements ServerTransport, AsyncRmiNotificationService {

	private static final Logger log = Logger.getLogger(AsyncRmiServerTransport.class);

	private NotificationService nsvc;
	private String svcName;

	private int rmiRegistryPort;
	private Remote remoteInterface;

	private Map<UID, EventChannel> channels = new ConcurrentHashMap<UID, EventChannel>();

	private Executor executor;

	private class ChannelRemover implements EventChannel.ChannelListener {
		@Override
		public void channelFailed(EventChannel eventChannel) {
			log.warn("Removing failed event channel processor for " + eventChannel.getClientAsString());
			eventChannel.setChannelListener(null);
			try {
				unSubscribe(new UID[] { eventChannel.getClientId() });
			} catch (RemoteException e) {
				// this is a local call thus
				// the catch is for compiler needs only
			}
		}
	}

	private final ChannelRemover channelRemover = this.new ChannelRemover();

	@Override
	public void start(Properties params) {
		log.info("Starting");
		svcName = params.getProperty(Constants.RMI_SERVICE_NAME, Constants.DEFAULT_SERVICE_NAME);
		String portString = params.getProperty(Constants.RMI_REGISTRY_PORT, "1099");
		try {
			rmiRegistryPort = Integer.parseInt(portString);
			Registry registry;
			try {
				registry = LocateRegistry.createRegistry(rmiRegistryPort);
			} catch (ExportException e) {
				//well, the registry might already be created (e.g. in case of restart)
				registry = LocateRegistry.getRegistry(rmiRegistryPort);
			}
			
			log.info("Starting on localhost:" + rmiRegistryPort + " as " + svcName);
			remoteInterface = UnicastRemoteObject.exportObject((AsyncRmiNotificationService) this, 0);
			registry.bind(svcName, remoteInterface);
			//InitialContext ctx = new InitialContext();
			//ctx.bind(svcName, remoteInterface);
		} catch (NumberFormatException e) {
			throw new IllegalArgumentException("Cannot parse \"" + Constants.RMI_REGISTRY_PORT + "\" parameter", e);
			/*
			 * } catch (RemoteException e) { throw new
			 * IllegalStateException("Cannot obtain RMI Registry on localhost",
			 * e); } catch (AlreadyBoundException e) { throw new
			 * IllegalStateException("Cannot bind transport", e);
			 } catch (NamingException e) {
			throw new IllegalStateException("Cannot bind transport", e);*/
		} catch (Exception e) {
			throw new IllegalStateException("Cannot export remote interface", e);
		}

		try {
			if (nsvc == null) {
				InitialContext ctx = new InitialContext();
				nsvc = (NotificationService) ctx
				        .lookup("java:module/LocalNotificationService!com.synapsense.service.nsvc.NotificationService");
			}
		} catch (Exception e) {
			throw new IllegalStateException("Cannot find local Notification Service", e);
		}

		// TODO: make thread pool configurable
		executor = Executors.newFixedThreadPool(3);

		log.info("Started");
	}

	@Override
	public void stop() {
		try {
			Registry registry = LocateRegistry.getRegistry(rmiRegistryPort);
			registry.unbind(svcName);
			svcName = null;
			UnicastRemoteObject.unexportObject((AsyncRmiNotificationService) this, true);
			remoteInterface = null;
			/*
			 * } catch (RemoteException e) { throw new
			 * IllegalStateException("Cannot obtain RMI Registry on localhost",
			 * e); } catch (NotBoundException e) {
			 * log.warn("Cannot unbind transport", e);
			 */} catch (Exception e) {
			// if(log.isDebugEnabled())
			// log.warn("Cannot unbind transport", e);
			// else
			log.warn("Cannot unbind transport: " + e.getMessage());
		}
		svcName = null;
		rmiRegistryPort = 0;
	}

	@Override
	public UID[] subscribe(AsyncRmiNotificationClient transport, EventProcessor[] filters) throws RemoteException {
		UID[] ids = new UID[filters.length];
		for (int i = 0; i < filters.length; i++) {
			// TODO: make fail strategy configurable
			ids[i] = new UID();
			EventChannel channel = new EventChannel(transport, ids[i], executor, new RetriesFailStrategy(3));
			channel.setChannelListener(channelRemover);
			nsvc.registerProcessor(channel, filters[i]);
			channels.put(ids[i], channel);
		}
		return ids;
	}

	@Override
	public void setFilter(UID[] subscriptions, EventProcessor[] filters) throws RemoteException {
		if (subscriptions.length != filters.length)
			throw new IllegalArgumentException("For each subscription there must be a filter");
		for (int i = 0; i < subscriptions.length; i++) {
			EventChannel channel = channels.get(subscriptions[i]);
			if (channel != null) {
				nsvc.registerProcessor(channel, filters[i]);
			} else {
				log.error("Attempt to set filter for nonexisting subscription with id=" + subscriptions[i]);
			}
		}
	}

	@Override
	public void unSubscribe(UID[] subscriptions) throws RemoteException {
		for (UID subscription : subscriptions) {
			EventChannel channel = channels.get(subscription);
			if (channel != null) {
				nsvc.deregisterProcessor(channel);
				channels.remove(subscription);
			}
		}
	}

	@Override
	public UID[] getNotRegistered(UID[] subscriptions) throws RemoteException {
		Collection<UID> result = new LinkedList<UID>();
		for (UID id : subscriptions) {
			if (!channels.containsKey(id)) {
				result.add(id);
			}
		}
		return result.toArray(new UID[result.size()]);
	}

}
