package com.synapsense.impl.erlnode.filters;

import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;

import com.ericsson.otp.erlang.OtpErlangList;
import com.ericsson.otp.erlang.OtpErlangObject;
import com.ericsson.otp.erlang.OtpErlangTuple;
import com.synapsense.impl.erlnode.commands.RegisterProcessorCommand;
import com.synapsense.impl.erlnode.utils.CollectionToConverter;
import com.synapsense.service.nsvc.EventProcessor;
import com.synapsense.service.nsvc.filters.AndFilter;

public class AndFilterParser implements FilterDefParser {

	private static final Logger log = Logger.getLogger(AndFilterParser.class);

	@Override
	public EventProcessor parse(OtpErlangTuple filterDef) {
		OtpErlangObject tmp = filterDef.elementAt(1);
		if (tmp == null || RegisterProcessorCommand.undefined.equals(tmp)) {
			throw new IllegalArgumentException("The second parameter of and_filter must be a list of filters");
		}
		OtpErlangList subFilterDefinitions = CollectionToConverter.checkList(tmp);
		if (subFilterDefinitions == null) {
			throw new IllegalArgumentException("The second parameter of and_filter must be a list of filters but was"
			        + filterDef.elementAt(1).getClass().getName());
		}
		List<EventProcessor> subFilters = new LinkedList<EventProcessor>();
		for (OtpErlangObject subFilterDef : subFilterDefinitions.elements()) {
			subFilters.add(EventFiltersFactory.createFilter(subFilterDef));
		}

		log.debug("returning AndFilter");
		return new AndFilter(subFilters);
	}

}
