package com.synapsense.impl.auditsvc.auditor;

import java.io.Serializable;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.service.impl.audit.AuditException;

public abstract class BasicAuditor implements Serializable {

	private static final long serialVersionUID = -3939767507066933049L;

	protected String configuration;
	protected Log logger = LogFactory.getLog(this.getClass());

	protected String lastStatus;

	public BasicAuditor() {
		this.configuration = "";
		this.lastStatus = "Undefined";
	}

	public abstract String getConfiguration();

	public BasicAuditor(String configuration) {
		this.configuration = configuration;
		this.lastStatus = "Undefined";
	}

	public String getLastStatus() {
		return lastStatus;
	}

	public void setLastStatus(String lastStatus) {
		this.lastStatus = lastStatus;
	}

	public void audit() throws AuditException {
		try {
			doAudit();
			setLastStatus("OK");
		} catch (AuditException e) {
			setLastStatus(e.getLocalizedMessage());
			throw e;
		}
	}

	protected abstract void doAudit() throws AuditException;

	public abstract String[] configurableProps();

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((configuration == null) ? 0 : configuration.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final BasicAuditor other = (BasicAuditor) obj;
		if (configuration == null) {
			if (other.configuration != null)
				return false;
		} else if (!configuration.equals(other.configuration))
			return false;
		return true;
	}

}