package com.synapsense.impl.devmanager.dissectors;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.synapsense.dto.TO;
import com.synapsense.plugin.messages.GenericMessage;
import com.synapsense.plugin.messages.GenericMessage.GenericMessageTypes;
import com.synapsense.plugin.messages.PowerRackConfBuilder.FieldConstants;
import com.synapsense.plugin.messages.PowerRackConfBuilder.PowerRackFields;
import com.synapsense.service.impl.messages.base.PlugConfData;
import com.synapsense.service.impl.messages.base.RackConfData;
import com.synapsense.service.impl.messages.base.SandcrawlerConfData;
import com.synapsense.service.impl.messages.base.ServerConfData;
import com.synapsense.util.algo.Pair;
import com.synapsense.util.algo.UnaryFunctor;

public class PowerRackConfDissector implements MessageDissector {

	private static final Logger log = Logger.getLogger(PowerRackConfDissector.class);

	private final UnaryFunctor<Pair<TO<?>, SandcrawlerConfData>> functor;

	public PowerRackConfDissector(UnaryFunctor<Pair<TO<?>, SandcrawlerConfData>> functor) {
		this.functor = functor;
	}

	@Override
	public boolean tasteMessage(GenericMessage msg) {
		return msg.getType() == GenericMessageTypes.POWER_RACK_CONF;
	}

	@Override
	@SuppressWarnings("unchecked")
	public void process(TO<?> objID, GenericMessage msg) {
		if (msg == null)
			throw new IllegalArgumentException("msg cannot be null");
		if (!tasteMessage(msg))
			throw new IllegalArgumentException("Incompatible message type");

		log.info("Received power configuration message for sandcrawler " + objID);

		SandcrawlerConfData confData = new SandcrawlerConfData();
		try {
			// Set checksum and program
			confData.setChecksum((Long) msg.get(FieldConstants.CHECKSUM.name()));
			confData.setProgram((String) msg.get(FieldConstants.PROGRAM.name()));

			ArrayList<HashMap<PowerRackFields, Serializable>> racks = (ArrayList<HashMap<PowerRackFields, Serializable>>) msg
			        .get(FieldConstants.RACKS.name());
			for (HashMap<PowerRackFields, Serializable> rack : racks) {
				RackConfData rackConfData = new RackConfData();
				rackConfData.setRack((Integer) rack.get(PowerRackFields.RACK));
				rackConfData.setuHeight((Integer) rack.get(PowerRackFields.UNIT_HEIGHT));
				rackConfData.setRpduType((Integer) rack.get(PowerRackFields.RPDU_TYPE));
				confData.addRackConfData(rackConfData);
			}

			ArrayList<HashMap<PowerRackFields, Serializable>> plugs = (ArrayList<HashMap<PowerRackFields, Serializable>>) msg
			        .get(FieldConstants.PLUGS.name());
			for (HashMap<PowerRackFields, Serializable> plug : plugs) {
				PlugConfData plugConfigData = new PlugConfData();
				plugConfigData.setPlugId((Long) plug.get(PowerRackFields.PLUG_ID));
				plugConfigData.setPhase((Integer) plug.get(PowerRackFields.PHASE_ID));
				plugConfigData.setRpdu((Integer) plug.get(PowerRackFields.RPDU_ID));
				plugConfigData.setuHeight((Integer) plug.get(PowerRackFields.UNIT_HEIGHT));
				plugConfigData.setuPos((Integer) plug.get(PowerRackFields.UNIT_POSITION));
				plugConfigData.setRack((Integer) plug.get(PowerRackFields.RACK));
				confData.addPlugConfData(plugConfigData);
			}
			ArrayList<HashMap<PowerRackFields, Serializable>> servers = (ArrayList<HashMap<PowerRackFields, Serializable>>) msg
			        .get(FieldConstants.SERVERS.name());
			for (HashMap<PowerRackFields, Serializable> server : servers) {
				ServerConfData serverConfigData = new ServerConfData();
				serverConfigData.setuHeight((Integer) server.get(PowerRackFields.UNIT_HEIGHT));
				serverConfigData.setuPos((Integer) server.get(PowerRackFields.UNIT_POSITION));
				serverConfigData.setRack((Integer) server.get(PowerRackFields.RACK));
				serverConfigData.setDemandPower((Double) server.get(PowerRackFields.DEMAND_POWER));
				confData.addServerConfData(serverConfigData);
			}
			functor.process(new Pair<TO<?>, SandcrawlerConfData>(objID, confData));
		} catch (Exception e) {
			throw new IllegalArgumentException("Wrong data is stored in message", e);
		}
	}
}
