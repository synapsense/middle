package com.synapsense.impl.reporting.queries;

import java.util.Set;

import com.synapsense.service.reporting.olap.custom.TimeHierarchy;
import org.apache.log4j.Logger;

import com.synapsense.exception.IllegalInputParameterException;
import com.synapsense.impl.reporting.queries.TableProcessor.QueryClause;
import com.synapsense.impl.reporting.queries.types.ReportingQueryTypeFactory;
import com.synapsense.impl.reporting.tables.ReportingTable;
import com.synapsense.impl.reporting.utils.ReportingConstants.QUERY.SYMBOL;
import com.synapsense.impl.reporting.utils.ReportingConstants.QUERY.WORD;
import com.synapsense.service.reporting.olap.Hierarchy;
import com.synapsense.service.reporting.olap.custom.BaseLevel;
import com.synapsense.service.reporting.olap.custom.Cube;
import com.synapsense.service.reporting.olap.custom.Level;

public class WhereClauseBuilder implements ClauseBuilder {
	
	private static final Logger log = Logger.getLogger(WhereClauseBuilder.class);
	
	private Level lowestTimeLevel;

	@Override
	public String build(TableProcessor owner) {
		
		if (owner == null) {
			throw new IllegalInputParameterException("TableProcessor shouldn't be null");
		}
		StringBuilder whereSB = new StringBuilder();
		//Adding where clause for time
		whereSB.append(QueryClause.WHERE.getName() + " ");

		whereSB.append(ReportingQueryTypeFactory.getClauseBuilder(
				owner.getQueryType()).build(QueryClause.WHERE,
				owner.getValueIdCells()));
		
		Set<ReportingTable> timeReportingTables = owner.getTimeReportingTables();
		
		for (ReportingTable table : timeReportingTables){
			if (table.isEmpty()) {
				continue;
			}
			if (this.getLowestTimeLevel() != null){
				whereSB.append(SYMBOL.SPACE)
					.append(WORD.AND);
			}
			whereSB.append(SYMBOL.SPACE)
				.append(table.<Object>get());
			Level lowestLevel = table.getLowestLevel();
			this.setLowestTimeLevel(lowestLevel.getName(), lowestLevel.getHierarchy());
		}
		
		whereSB.append(SYMBOL.SPACE);

		if (this.getLowestTimeLevel() != null){
			whereSB.append(SYMBOL.SPACE)
				.append(WORD.AND);
		} else {
			this.setLowestTimeLevel(Cube.LEVEL_YEAR, Cube.TIME);
		}
		//Adding where clause for value Ids
		whereSB.append(SYMBOL.SPACE)
			.append("value_id_fk in (:allValueIds)");
		if (log.isDebugEnabled()) {
			log.debug("GetEnvData WHERE clause: " + whereSB.toString());
		}
		//Only 24 Months history is supported not-archived
		int period = TimeHierarchy.getHistoricalPeriod();
		whereSB.append(" and timestamp > DATE_SUB(NOW(), interval  " + period + " MONTH) ")
			.append(SYMBOL.SPACE);

		// [BC 12/2015] The reporting of historical values only uses historical_values.valued (see SelectClauseBuilder.selectClause()),
		// so it's safe to add the hardcoded check valued >= 0 to fix bug 23645.
		// This could break future reports using other fields such as valuei.
		whereSB.append("and valued >= 0 ");

		return whereSB.toString();
		
	}

	Level getLowestTimeLevel() {
		return this.lowestTimeLevel;
	}

	private void setLowestTimeLevel(String timeLevel, Hierarchy hierarchy) {
		lowestTimeLevel = (lowestTimeLevel == null) ? new BaseLevel(Cube.TIME, Cube.LEVEL_YEAR) : lowestTimeLevel;
		
		if (hierarchy.isDescendant(timeLevel, lowestTimeLevel.getName())){
			lowestTimeLevel = new BaseLevel(hierarchy, timeLevel);
		}
	}
}
