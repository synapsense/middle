package com.synapsense.impl.alerting.notification;

import com.synapsense.dto.Message;
import com.synapsense.service.impl.notification.Transport;
import com.synapsense.util.CollectionUtils;
import org.apache.log4j.Logger;

import java.util.*;

public class AlertNotificationManager implements NotificationManager, Runnable {

	private final static Logger logger = Logger.getLogger(AlertNotificationManager.class);

	public AlertNotificationManager(final Queue<Notification> queue, final TransportProvider transports) {
		workingQueue = queue;
		notificationIndex = Collections.synchronizedMap(CollectionUtils.<String, Notification> newMap());
		this.transports = transports;
	}

	@Override
	public void raise(final Notification n) {
		if (logger.isDebugEnabled()) {
			logger.debug("Submitting new notification " + n.toString());
		}
		synchronized (notificationIndex) {
			String id = n.getId();
			if (isValidNotification(id)) {
				return;
			}
			notificationIndex.put(id, n);
			workingQueue.add(n);
		}
	}

	@Override
	public void add(final Notification n) {
		if (logger.isDebugEnabled()) {
			logger.debug("Submitting restored notification " + n.toString());
		}
		synchronized (notificationIndex) {
			String id = n.getId();
			if (isValidNotification(id)) {
				return;
			}
			notificationIndex.put(id, n);
		}
	}

	@Override
	public boolean send(final Message message) {
		if (logger.isDebugEnabled()) {
			logger.debug("Sending notification message by  " + message.getProtocol());
		}

		try {
			// TODO : async sending messages to achieve better performance
			final Transport t = transports.get(message.getProtocol());
			if (t == null) {
				throw new IllegalStateException("There is no transport configured for protocol : "
				        + message.getProtocol());
			}
			if (t.isValid(message)) {
				t.send(message);
			} else {
				logger.debug("Cannot send message cause it has been rejected by transport");
			}
		} catch (final Exception e) {
			logger.warn("Failed to send message via " + message.getProtocol() + " transport", e);
			return false;
		}

		return true;
	}

	@Override
	public void ack(final String id) {
		if (logger.isDebugEnabled()) {
			logger.debug("Acknowledging notification " + id);
		}

		synchronized (notificationIndex) {
			final Notification n = notificationIndex.get(id);
			if (n == null) {
				return;
			}

			final String event = n.currentState();
			final NotificationTemplate nt = n.generatedBy();
			final List<Message> messagesToSend = nt.generateAckMessages(event, n.getContext());
			send(messagesToSend);
			turnOffNotification(id);
		}
	}

	@Override
	public void dismiss(final String id) {
		if (logger.isDebugEnabled()) {
			logger.debug("Dismissing notification " + id);
		}

		remove(id);
	}

	@Override
	public void resolve(final String id) {
		if (logger.isDebugEnabled()) {
			logger.debug("Resolving notification " + id);
		}

		synchronized (notificationIndex) {
			final Notification n = notificationIndex.get(id);
			if (n == null) {
				return;
			}

			final String event = n.currentState();
			final NotificationTemplate nt = n.generatedBy();
			final List<Message> messagesToSend = nt.generateResolveMessages(event, n.getContext());
			send(messagesToSend);
			removeNotification(id);
		}
	}

	@Override
	public Collection<Notification> getActiveNotifications() {
		return Collections.unmodifiableCollection(notificationIndex.values());
	}

	@Override
	public void remove(final String id) {
		removeNotification(id);
	}

	@Override
	public void run() {
		if (logger.isTraceEnabled()) {
			logger.trace("Notification scheduler thread is executing");
		}

		try {
			reap();
		} catch (final Exception e) {
			logger.warn("Failed to reap notification queue.", e);
		}

		if (logger.isTraceEnabled()) {
			logger.trace("Notification scheduler is suspended");
		}
	}

	private void reap() {
		boolean breakWhile = false;
		while (!workingQueue.isEmpty() && !breakWhile) {
			final Notification processingNotification = workingQueue.poll();
			if (logger.isTraceEnabled()) {
				logger.trace("Grabbing next notification in scheduler : " + processingNotification.toString());
			}

			if (processingNotification == null) {
				if (logger.isDebugEnabled()) {
					logger.debug("Working queue is empty");
				}
				return;
			}

			boolean notificationIsStillAlive = true;
			final long nextFireTime = processingNotification.getNextFireTime().getTime();
			if (nextFireTime <= System.currentTimeMillis()) {
				// check interrupted flag before time consuming operation
				if (Thread.currentThread().isInterrupted()) {
					if (logger.isDebugEnabled()) {
						logger.debug("Notification scheduler has been interrupted by external process");
					}
					break;
				}
				processingNotification.process(this);
				notificationIsStillAlive = processingNotification.isActive();
			} else {
				if (logger.isTraceEnabled()) {
					logger.trace("Skip due to no actual notifications exist. Nearest notification time is "
                            + processingNotification.getNextFireTime());

				}
				breakWhile = true;
			}

			if (notificationIsStillAlive) {
				if (isValidNotification(processingNotification.getId())) {
					if (logger.isDebugEnabled()) {
						logger.debug("Returning back notification to scheduler : " + processingNotification.toString());
					}
					workingQueue.add(processingNotification);
				}
			}
		}
	}

	private boolean isValidNotification(final String id) {
		return notificationIndex.containsKey(id);
	}

	private void removeNotification(final String id) {
		if (!isValidNotification(id)) {
			return;
		}
		boolean removedFromQueue = false;
		boolean removedFromIndex = false;

		synchronized (notificationIndex) {
			final Notification n = notificationIndex.get(id);
			if (n == null)
				return;
			removedFromQueue = workingQueue.remove(n);
			removedFromIndex = notificationIndex.remove(id) != null;
		}

		if (logger.isDebugEnabled()) {
			String notificationStatus = removedFromQueue && removedFromIndex ? "Active" : "Already expired";
			logger.debug(notificationStatus + " notification [" + id + "] was removed, queue=" + removedFromQueue
			        + ", index=" + removedFromIndex);
		}
	}

	private void send(final List<Message> messages) {
		for (final Message m : messages) {
			send(m);
		}
	}

	private void turnOffNotification(String id) {
		if (!isValidNotification(id)) {
			return;
		}

		boolean removedFromQueue = false;
		synchronized (notificationIndex) {
			Notification n = notificationIndex.get(id);
			if (n == null)
				return;
			removedFromQueue = workingQueue.remove(n);
		}

		if (removedFromQueue && logger.isDebugEnabled()) {
			if (logger.isDebugEnabled()) {
				logger.debug("Active notification [" + id + "] was turned off");
			}
		}
	}

	private final Queue<Notification> workingQueue;
	private final Map<String, Notification> notificationIndex;
	private final TransportProvider transports;

}