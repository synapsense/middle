package com.synapsense.impl.utils;

import org.jboss.as.controller.client.ModelControllerClient;
import org.jboss.dmr.ModelNode;
import org.jboss.dmr.ModelType;

import javax.security.auth.callback.*;
import javax.security.sasl.RealmCallback;
import java.io.Closeable;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;

    public class JbossManagementAPI implements Closeable {

	public static final String HTTP_PORT = "HTTP_PORT";
	public static final String HTTPS_PORT = "HTTPS_PORT";
	public static final String HTTP_CONNECTOR_ENABLED = "HTTP_CONNECTOR_ENABLED";
	public static final String HTTPS_CONNECTOR_ENABLED = "HTTPS_CONNECTOR_ENABLED";
	public static final String KEYSTORE_PASSWORD = "KEYSTORE_PASSWORD";
	public static final String KEYSTORE_FILENAME = "KEYSTORE_FILENAME";
	public static final String SMTP_HOST = "SMTP_HOST";
	public static final String SMTP_PORT = "SMTP_PORT";
	public static final String MAIL_FROM = "MAIL_FROM";
	public static final String SMTP_SSL = "SMTP_SSL";
	public static final String SMTP_USERNAME = "SMTP_USERNAME";
	public static final String SMTP_PASSWORD = "SMTP_PASSWORD";

	private final static String OUTCOME_SUCCESS = "success";
	private ModelControllerClient client;

	private static class Attribute {
		String name;
		Class<?> type;
		String[] address;

		public Attribute(String name, Class<?> type, String[] address) {
			if (address == null || address.length % 2 == 1) {
				throw new IllegalArgumentException("Length of address must be even");
			}

			this.name = name;
			this.type = type;
			this.address = address;
		}
	}

	private static Map<String, Attribute> attributes = new HashMap<String, Attribute>();

	static {
		attributes.put(HTTP_PORT, new Attribute("port", Integer.class, new String[] { "socket-binding-group",
		        "standard-sockets", "socket-binding", "http" }));
		attributes.put(HTTPS_PORT, new Attribute("port", Integer.class, new String[] { "socket-binding-group",
		        "standard-sockets", "socket-binding", "https" }));
		attributes.put(HTTP_CONNECTOR_ENABLED, new Attribute("enabled", Boolean.class, new String[] { "subsystem",
		        "web", "connector", "http" }));
		attributes.put(HTTPS_CONNECTOR_ENABLED, new Attribute("enabled", Boolean.class, new String[] { "subsystem",
		        "web", "connector", "https" }));
		attributes.put(KEYSTORE_PASSWORD, new Attribute("password", String.class, new String[] { "subsystem", "web",
		        "connector", "https", "ssl", "configuration" }));
		attributes.put(KEYSTORE_FILENAME, new Attribute("certificate-key-file", String.class, new String[] {
		        "subsystem", "web", "connector", "https", "ssl", "configuration" }));
		attributes.put(SMTP_HOST, new Attribute("host", String.class, new String[] { "socket-binding-group",
		        "standard-sockets", "remote-destination-outbound-socket-binding", "mail-smtp" }));
		attributes.put(SMTP_PORT, new Attribute("port", Integer.class, new String[] { "socket-binding-group",
		        "standard-sockets", "remote-destination-outbound-socket-binding", "mail-smtp" }));
		attributes.put(MAIL_FROM, new Attribute("from", String.class, new String[] { "subsystem", "mail",
		        "mail-session", "java:/Mail" }));
		attributes.put(SMTP_SSL, new Attribute("ssl", Boolean.class, new String[] { "subsystem", "mail",
		        "mail-session", "java:/Mail", "server", "smtp" }));
		attributes.put(SMTP_USERNAME, new Attribute("username", String.class, new String[] { "subsystem", "mail",
		        "mail-session", "java:/Mail", "server", "smtp" }));
		attributes.put(SMTP_PASSWORD, new Attribute("password", String.class, new String[] { "subsystem", "mail",
		        "mail-session", "java:/Mail", "server", "smtp" }));
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			JbossManagementAPI util = new JbossManagementAPI(InetAddress.getByName("localhost"), 9999, "admin", "admin");

			util.setAttribute(HTTP_PORT, "8080");

			System.out.println(HTTP_PORT + ": " + util.getAttribute(HTTP_PORT));
			System.out.println(HTTPS_PORT + ": " + util.getAttribute(HTTPS_PORT));
			System.out.println(HTTP_CONNECTOR_ENABLED + ": " + util.getAttribute(HTTP_CONNECTOR_ENABLED));
			System.out.println(HTTPS_CONNECTOR_ENABLED + ": " + util.getAttribute(HTTPS_CONNECTOR_ENABLED));
			System.out.println(KEYSTORE_PASSWORD + ": " + util.getAttribute(KEYSTORE_PASSWORD));
			System.out.println(KEYSTORE_FILENAME + ": " + util.getAttribute(KEYSTORE_FILENAME));
			System.out.println(SMTP_HOST + ": " + util.getAttribute(SMTP_HOST));
			System.out.println(SMTP_PORT + ": " + util.getAttribute(SMTP_PORT));
			System.out.println(MAIL_FROM + ": " + util.getAttribute(MAIL_FROM));
			System.out.println(SMTP_SSL + ": " + util.getAttribute(SMTP_SSL));
			System.out.println(SMTP_USERNAME + ": " + util.getAttribute(SMTP_USERNAME));
			System.out.println(SMTP_PASSWORD + ": " + util.getAttribute(SMTP_PASSWORD));

			// util.reloadServer();

			util.close();
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public JbossManagementAPI(final InetAddress host, final int port, final String username, final String password) {
		client = createClient(host, port, username, password);
	}

	public String undefineAttribute(String name) throws IOException {

		Attribute attribute = attributes.get(name);

		if (attribute == null) {
			throw new IllegalArgumentException("Unknown attribute name: " + name);
		}

		ModelNode modelNode = new ModelNode();
		modelNode.get("operation").set("undefine-attribute");
		modelNode.get("name").set(attribute.name);
		setAddress(modelNode, attribute.address);

		ModelNode returnValNode = client.execute(modelNode);
		validateResult(returnValNode);
		return returnValNode.get("result").asString();
	}

	public String getAttribute(String name) throws IOException {

		Attribute attribute = attributes.get(name);

		if (attribute == null) {
			throw new IllegalArgumentException("Unknown attribute name: " + name);
		}

		ModelNode modelNode = new ModelNode();
		modelNode.get("operation").set("read-attribute");
		modelNode.get("name").set(attribute.name);
		setAddress(modelNode, attribute.address);

		ModelNode returnValNodeNode = client.execute(modelNode);
		validateResult(returnValNodeNode);
		ModelNode resultNode = returnValNodeNode.get("result");
		return ModelType.UNDEFINED.equals(resultNode.getType()) ? null : resultNode.asString();
	}

	public void setAttribute(String name, String value) throws IOException {

		Attribute attribute = attributes.get(name);

		if (attribute == null) {
			throw new IllegalArgumentException("Unknown attribute name: " + name);
		}

		ModelNode modelNode = new ModelNode();
		modelNode.get("operation").set("write-attribute");
		modelNode.get("name").set(attribute.name);
		setAddress(modelNode, attribute.address);

		if (Integer.class.equals(attribute.type)) {
			modelNode.get("value").set(Integer.parseInt(value));
		} else if (Boolean.class.equals(attribute.type)) {
			modelNode.get("value").set(Boolean.parseBoolean(value));
		} else if (String.class.equals(attribute.type)) {
			modelNode.get("value").set(value);
		} else {
			throw new UnsupportedOperationException("No handler for " + attribute.type + " class yet");
		}

		ModelNode returnValNode = client.execute(modelNode);
		validateResult(returnValNode);
	}

	public void reloadServer() throws IOException {
		ModelNode op = new ModelNode();
		op.get("operation").set("reload");

		ModelNode returnValNode = client.execute(op);
		validateResult(returnValNode);
	}

	public void setSystemProperty(String name, String value) throws IOException {
		ModelNode modelNode = new ModelNode();
		modelNode.get("operation").set("write-attribute");
		modelNode.get("name").set("value");
		modelNode.get("value").set(value);

		ModelNode address = modelNode.get("address");
		address.add("system-property", name);

		ModelNode returnValNode = client.execute(modelNode);
		validateResult(returnValNode);
	}

	public boolean isPortAvailable(int port) throws IOException {
		// TODO:
		return true;
	}

	public void close() throws IOException {
		client.close();
	}

	private void setAddress(ModelNode modelNode, String[] address) {
		ModelNode addressNode = modelNode.get("address");
		for (int i = 0; i < address.length; i = i + 2) {
			addressNode.add(address[i], address[i + 1]);

		}
	}

	private void validateResult(ModelNode modelNode) {
		if (!OUTCOME_SUCCESS.equals(modelNode.get("outcome").asString())) {
			// TODO: define exception
			throw new RuntimeException("Error in Jboss Management API operation:\n" + modelNode.toString());
		}
	}

	private ModelControllerClient createClient(final InetAddress host, final int port, final String username,
	        final String password) {

		final CallbackHandler callbackHandler = new CallbackHandler() {

			public void handle(Callback[] callbacks) throws IOException, UnsupportedCallbackException {
				for (Callback current : callbacks) {
					if (current instanceof NameCallback) {
						NameCallback ncb = (NameCallback) current;
						ncb.setName(username);
					} else if (current instanceof PasswordCallback) {
						PasswordCallback pcb = (PasswordCallback) current;
						pcb.setPassword(password.toCharArray());
					} else if (current instanceof RealmCallback) {
						RealmCallback rcb = (RealmCallback) current;
						rcb.setText(rcb.getDefaultText());
					} else {
						throw new UnsupportedCallbackException(current);
					}
				}
			}
		};

		return ModelControllerClient.Factory.create(host, port, callbackHandler);
	}
}
