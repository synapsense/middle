package com.synapsense.impl.reporting.tasks.replace;

import com.synapsense.dto.TO;
import com.synapsense.impl.reporting.queries.TableProcessor.QueryClause;
import com.synapsense.impl.reporting.utils.ReportingConstants.QUERY.SYMBOL;
import com.synapsense.impl.reporting.utils.ReportingConstants.QUERY.WORD;
import com.synapsense.service.reporting.Cell;
import com.synapsense.service.reporting.olap.Member;
import com.synapsense.service.reporting.olap.custom.Cube;
import com.synapsense.util.CollectionUtils;

import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.synapsense.impl.reporting.tasks.replace.QueryDataReplacerFactory.ForwardingObjectIdQueryDataReplacer;

public class ObjectIdPropertyNameQueryDataReplacer extends ForwardingObjectIdQueryDataReplacer {

    public ObjectIdPropertyNameQueryDataReplacer(ObjectIdQueryDataReplacer replacer) {
        super(replacer);
    }

    @Override
	public String replace(String envDataQuery, List<Cell> valueIdCells) {
		return envDataQuery.replaceAll(
				Pattern.quote("historical_values hv"),
				Matcher.quoteReplacement(
                        createObjectPropertyValueIdTable(valueIdCells)
                        + SYMBOL.SPACE
						+ "historical_values hv"));
	}
	
	private String createObjectPropertyValueIdTable(List<Cell> valueIdCells) {
		Set<String> valueIdSubSelectClauses = CollectionUtils.newLinkedSet();
		for (Cell cell : valueIdCells) {
            Member propertyMember = cell.getMember(Cube.PROPERTY);
            String [] propertyArray = ((String)propertyMember.getKey()).split("\\.");
            String propertyName = propertyArray[0] + "." + propertyArray[1];

			for (Integer valueId : cell.getValueIds()) {
				int objectId = (Integer) ((TO<?>)cell.getMember(Cube.OBJECT).getKey()).getID();
				StringBuilder valueIdSubSelectClauseSB = new StringBuilder();
				valueIdSubSelectClauseSB
					.append(QueryClause.SELECT.getName())					
					.append(SYMBOL.SPACE)

					.append(SYMBOL.APOSTROPHE)
					.append(objectId)
					.append(SYMBOL.APOSTROPHE)
					.append(SYMBOL.SPACE)
					.append(WORD.AS)
					.append(SYMBOL.SPACE)
					.append("object_id")
					.append(SYMBOL.COMMA)
					
					.append(SYMBOL.APOSTROPHE)
					.append(propertyName)
					.append(SYMBOL.APOSTROPHE)
					.append(SYMBOL.SPACE)
					.append(WORD.AS)
					.append(SYMBOL.SPACE)
					.append("property_name")
					.append(SYMBOL.COMMA)
					
					.append(SYMBOL.APOSTROPHE)
					.append(valueId)
					.append(SYMBOL.APOSTROPHE)
					.append(SYMBOL.SPACE)
					.append(WORD.AS)
					.append(SYMBOL.SPACE)
					.append("value_id");
				valueIdSubSelectClauses.add(valueIdSubSelectClauseSB.toString());
				
			}
		}
		
		StringBuilder propertyValueIdTableSB = new StringBuilder();
		propertyValueIdTableSB.append(SYMBOL.OPEN_BRACKET);
		StringBuilder propertySubSelectSB = new StringBuilder();
		for (String valueIdSubSelectClause : valueIdSubSelectClauses) {
			propertySubSelectSB
				.append(valueIdSubSelectClause)
				.append(SYMBOL.SPACE)
				.append(WORD.UNION)
				.append(SYMBOL.SPACE);
		}
		if (propertySubSelectSB.length() != 0) {
			propertyValueIdTableSB.append(propertySubSelectSB.substring(0, propertySubSelectSB.length() - 7));
		}
		propertyValueIdTableSB.append(SYMBOL.CLOSE_BRACKET)
			.append(SYMBOL.SPACE)
			.append(WORD.AS)
			.append(SYMBOL.SPACE)
			.append("objects_valueids")
			.append(SYMBOL.COMMA)
			.append(SYMBOL.SPACE);
		return propertyValueIdTableSB.toString();
	}

}
