package com.synapsense.impl.reporting.engine;

import com.synapsense.cdi.MBean;
import com.synapsense.dal.generic.reporting.ReportingQueryExecutor;
import com.synapsense.dto.TO;
import com.synapsense.exception.IllegalInputParameterException;
import com.synapsense.service.UnitSystemsService;
import com.synapsense.service.UserManagementService;
import com.synapsense.service.reporting.DimensionalQuery;
import com.synapsense.service.reporting.ReportingService;
import com.synapsense.service.reporting.dto.TableTO;
import com.synapsense.service.reporting.olap.custom.Cube;
import com.synapsense.service.reporting.olap.custom.EnvironmentDataSource;
import com.synapsense.service.reporting.olap.custom.ReportingEnvironmentDAO;
import com.synapsense.service.user.EnvUserContext;
import org.apache.log4j.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.*;
import java.security.Principal;

@Startup
@Singleton(name = "ReportingService")
@Remote(ReportingService.class)
@ConcurrencyManagement(ConcurrencyManagementType.BEAN)
@MBean("com.synapsense:type=ReportingService")
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class ReportingServiceImpl implements ReportingService, ReportingServiceImplMBean {

	private static final Logger log = Logger.getLogger(ReportingServiceImpl.class);
	
	@Resource
	private EJBContext ctx;
	
	/** EJB */
	private UserManagementService userManagement;

	private EnvironmentDataSource dataSource;

	private UnitSystemsService unitSystemsService;
	
	/** EJB */
	private ReportingQueryExecutor reportingQueryExecutor;

	@EJB(beanName = "ReportingQueryExecutorImpl")
	public void setReportingQueryExecutor(final ReportingQueryExecutor reportingQueryExecutor) {
		this.reportingQueryExecutor = reportingQueryExecutor;
		log.info("setReportingQueryExecutor()");
	}
	
	@EJB(beanName = "UserManagementService")
	public void setUserService(final UserManagementService userManagement) {
		this.userManagement = userManagement;
	}
	
	@EJB(beanName = "UnitSystemsServiceImpl")
	public void setUnitSystemsService(final UnitSystemsService unitSystemsService) {
		this.unitSystemsService = unitSystemsService;
	}

	@Resource
	void setCtx(final EJBContext ctx) {
		this.ctx = ctx;
	}
	
	@Override
	public TableTO executeQuery(DimensionalQuery query) {
		
		if (query == null) {
			throw new IllegalInputParameterException("Dimensional Query shouldn't be null");
		}
		int userId = getUserId();
		ReportUserInfo reportUserInfo = new ReportUserInfo(
				unitSystemsService.getUnitSystems(),
				unitSystemsService.getUnitReslovers(), userId,
				getUserContext());
		ReportQueryInfo reportData = new ReportData(query, dataSource, reportingQueryExecutor, reportUserInfo);
		
		reportData.buildQuery();

		return reportData.executeQuery();
		
	}

	@Override
	@PostConstruct
	public void create() throws Exception {
		
		log.info("Creating reporting service ...");
		start();
		
	}

	@Override
	public void start() throws Exception {
		
		log.info("Starting reporting service...");
		dataSource = new ReportingEnvironmentDAO();
		if (dataSource == null) {
			if (log.isDebugEnabled()) { 
				log.debug("Data Source for Reporting engine hasn't initialized");
			}
			throw new IllegalStateException("Data Source for Reporting engine hasn't initialized");
		}
		Cube.setReportingDataSource(dataSource);
		log.info("Started reporting service");
		
	}

	@Override
	@PreDestroy
	public void stop() throws Exception {
		dataSource = null;
		destroy();
	}

	@Override
	public void destroy() throws Exception {
		log.info("Destroying reporting service ...");
		log.info("Destroyed reporting service");
	}

	private int getUserId() {
		final Principal principal = ctx.getCallerPrincipal();
		final String callerName = principal.getName();
		TO<?> user = userManagement.getUser(callerName);
		return (Integer) user.getID();
	}
	
//	private String getTargetSystem(int userId) {
//		User user = getUserById(userId);
//		UserContext userContext = new EnvUserContext(user.getUserName(), userManagement);
//		return userContext.getUnitSystem();
//	}

    private EnvUserContext getUserContext() {
		final Principal principal = ctx.getCallerPrincipal();
		final String callerName = principal.getName();
        return new EnvUserContext(callerName, userManagement);
    }

}