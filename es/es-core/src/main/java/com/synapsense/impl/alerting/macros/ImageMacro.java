package com.synapsense.impl.alerting.macros;

import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.exception.EnvException;
import com.synapsense.impl.alerting.notification.smtp.Attachments;
import com.synapsense.search.Direction;
import com.synapsense.search.Path;
import com.synapsense.search.QueryDescription;
import com.synapsense.search.Relations;
import com.synapsense.service.Environment;
import com.synapsense.service.user.NonUserContext;
import com.synapsense.util.TranslatingEnvironment;
import org.apache.log4j.Logger;

import java.util.Collection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 
 * All magic of what images to embed in emails basing on alert message
 * 
 * @author : shabanov
 * @version 0.1
 * 
 */
public class ImageMacro extends AbstractDataValueMacro {
	private static final Logger logger = Logger.getLogger(ImageMacro.class);

	private static final String DATACLASS_PROPERTY_NAME = "dataclass";
	private static final String RACK_TYPE_NAME = "RACK";
	private static final String WSNSENSOR_TYPE_NAME = "WSNSENSOR";
	private static final String WSNNODE_TYPE_NAME = "WSNNODE";
	private static final String DISPLAYPOINT_TYPE_NAME = "DISPLAYPOINT";
	private static final String ROOM_TYPE_NAME = "ROOM";
	private static final String DC_TYPE_NAME = "DC";

	private static final String LAYER_PROPERTY_NAME = "z";

	private ImageLevel imageLevel;

	public ImageMacro(ImageLevel imageLevel) {
		this.imageLevel = imageLevel;
	}

	public enum ImageLevel {
	ROOM(Pattern.compile("\\$(?i)room_live_image\\$"), ROOM_TYPE_NAME), DC(Pattern.compile("\\$(?i)dc_live_image\\$"),
	        DC_TYPE_NAME);

	/**
	 * This method should return the real owner(host TO) of LiveImaging image.
	 * If object refers to multiple ROOMs(DCs) the random host will be picked
	 * up.;
	 * 
	 * @param env
	 *            Environment reference
	 * @param alertRootCause
	 *            Alert host
	 * @return Owner of LiveImaging image
	 * */
	TO pickImageOwner(Environment env, TO alertRootCause) {
		if (alertRootCause.getTypeName().equals(typeName))
			return alertRootCause;
		// getting parent of alert host object matching given typeName
		Collection<TO<?>> parents = env.getRelatedObjects(alertRootCause, typeName, false);
		if (parents.isEmpty()) {
			return alertRootCause;
		}
		TO imageObj = parents.iterator().next();
		return imageObj;
	}

	private ImageLevel(Pattern pattern, String typeName) {
		this.pattern = pattern;
		this.typeName = typeName;
	}

	private final Pattern pattern;
	private final String typeName;
	}

	@Override
	protected String expand(Matcher m, MacroContext ctx) {
		Environment env = ctx.env;

		Attachments attachments;

		Attachments existingAttachments = (Attachments) ctx.props.get("attachments");
		if (existingAttachments == null) {
			if (ctx.isAnonymous()) {
				attachments = new Attachments(new NonUserContext(), env, ctx.localizationService);
			} else {
				// attachments may contain localizable strings
				TranslatingEnvironment translatingEnvironment = new TranslatingEnvironment(ctx.env);
				String lang = ctx.usrCtx.getLocale().toString();
				translatingEnvironment.setLanguage(lang);
				env = translatingEnvironment;
				attachments = new Attachments(ctx.usrCtx, env, ctx.localizationService);
			}
		} else {
			// use already existing bundle
			attachments = new Attachments(existingAttachments);
		}

		// convert $data()$ to images
		DataMacro dataMacro = new DataMacro(attachments);
		dataMacro.expandIn(ctx.alert.getMessage(), ctx);

		ctx.props.put("attachments", dataMacro.getAttachments());
		// replace itself by empty string
		return Macro.EMPTY_EXPANSION;
	}

	@Override
	protected Pattern getPattern() {
		return imageLevel.pattern;
	}

	/**
	 * Macro takes all $data$ occurrences, extract object, layer, dataclass info
	 * if possible and push it to macro context
	 */
	private class DataMacro extends AbstractDataValueMacro {
		private Attachments attachments;

		private DataMacro(Attachments attachments) {
			this.attachments = attachments;
		}

		public Attachments getAttachments() {
			return attachments;
		}

		@Override
		public String expand(Matcher m, MacroContext ctx) {
			AlertRootCause alertRootCause = expandObject(ctx.env, m.group(1));

			int dataClassCode = getDataclass(ctx.env, alertRootCause, m);
			if (dataClassCode == -1) {
				logger.warn("Image cannot be embedded due to dataclass info is not available for "
				        + alertRootCause.getHostTo());
				return Matcher.quoteReplacement(m.group());
			}

			int layerCode = LayerPickerFactory.create(ctx.env, alertRootCause, dataClassCode).pickLayer();
			if (layerCode == -1) {
				logger.warn("Image cannot be embedded due to layer info is not available for "
				        + alertRootCause.getHostTo());
				return Matcher.quoteReplacement(m.group());
			}

			TO imageOwner = imageLevel.pickImageOwner(ctx.env, alertRootCause.getHostTo());

			// stash image refs in context
			attachments.attachLiveImage(imageOwner, alertRootCause.getHostTo(), layerCode, dataClassCode);

			return Matcher.quoteReplacement(m.group());
		}

		// get dataclass from parsed macro then from 'dataclass' tag if any
		private int getDataclass(Environment env, AlertRootCause alert, Matcher m) {
			try {
				return Integer.parseInt(m.group(3));
			} catch (NumberFormatException e) {
				if (logger.isDebugEnabled())
					logger.debug(m.group() + " macro \"dataclass\" option should be integer", e);

				if (alert.getProperty() == null)
					return -1;

				try {
					return env.getTagValue(alert.getHostTo(), alert.getProperty(), "dataclass", Integer.class);
				} catch (EnvException tagEx) {
					if (logger.isDebugEnabled())
						logger.debug("Failed to get dataclass tag of [" + alert.getHostTo() + "." + alert.getProperty()
						        + "]", e);
					return -1;
				}
			}
		}
	}

	/**
	 * Modify when new ways to get layer info from ES appear
	 */
	private static final class LayerPickerFactory {

		static LayerPickerStrategy create(Environment env, AlertRootCause alertRootCause, int wantedDataclass) {

			TO<?> hostTo = alertRootCause.getHostTo();

			final String tempHostTypeName = hostTo.getTypeName();

			switch (tempHostTypeName) {
			case WSNSENSOR_TYPE_NAME:
			case DISPLAYPOINT_TYPE_NAME:
				return new ZPropertyPickerStrategy(env, hostTo);
			case WSNNODE_TYPE_NAME:
				return new WsnNodeSpecialPickerStrategy(env, hostTo, wantedDataclass);
			case RACK_TYPE_NAME:
				return new RackSpecialPickerStrategy(env, hostTo);
			case ROOM_TYPE_NAME:
			case DC_TYPE_NAME:
				return new TagsBasedPickerStrategy(env, hostTo, alertRootCause.getProperty());
			default:
				return LayerPickerStrategy.NOT_AVAILABLE_LAYER_LCIKER;
			}

		}

		private LayerPickerFactory() {
		}
	}

	private static interface LayerPickerStrategy {
		public static int NOT_AVAILABLE_LAYER = -1;
		public static LayerPickerStrategy NOT_AVAILABLE_LAYER_LCIKER = new LayerPickerStrategy() {
			@Override
			public int pickLayer() {
				return -1;
			}
		};

		int pickLayer();
	}

	/**
	 * getting layer from 'z' property value
	 */
	private static class ZPropertyPickerStrategy implements LayerPickerStrategy {
		private Environment env;
		private TO<?> hostTo;

		ZPropertyPickerStrategy(Environment env, TO<?> hostTo) {
			this.env = env;
			this.hostTo = hostTo;
		}

		@Override
		public int pickLayer() {
			int layerCode;
			try {
				layerCode = env.getPropertyValue(hostTo, LAYER_PROPERTY_NAME, Integer.class);
			} catch (EnvException e) {
				logger.warn("Failed to get layer from [" + hostTo + "]", e);
				return NOT_AVAILABLE_LAYER;
			}

			return layerCode;
		}
	}

	/**
	 * getting layer if object is wsnnode it has no own layer but may have a
	 * sensor which has it.
	 */
	private static class WsnNodeSpecialPickerStrategy implements LayerPickerStrategy {
		private Environment env;
		private TO<?> hostTo;
		private int wantedDataclass;

		WsnNodeSpecialPickerStrategy(Environment env, TO<?> hostTo, int wantedDataclass) {
			this.env = env;
			this.hostTo = hostTo;
			this.wantedDataclass = wantedDataclass;
		}

		@Override
		public int pickLayer() {
			try {
				for (TO sensor : env.getChildren(hostTo)) {
					int realDataclass = env.getPropertyValue(sensor, DATACLASS_PROPERTY_NAME, Integer.class);
					if (realDataclass == wantedDataclass) {
						return new ZPropertyPickerStrategy(env, sensor).pickLayer();
					}
				}
			} catch (EnvException e) {
				logger.warn("Failed to get " + wantedDataclass + " sensor from node [" + hostTo + "]", e);
			}

			return NOT_AVAILABLE_LAYER;
		}
	}

	/**
	 * Racks are special case. It can be referenced by DISPLAYPOINT virtual
	 * object through lastValue property
	 */
	private static class RackSpecialPickerStrategy implements LayerPickerStrategy {

		private Environment env;
		private TO<?> hostTo;

		RackSpecialPickerStrategy(Environment env, TO<?> hostTo) {
			this.env = env;
			this.hostTo = hostTo;
		}

		@Override
		public int pickLayer() {
			Collection<TO<?>> connectedDisplayPoints = env.getObjects(new QueryDescription(1).startQueryFrom(hostTo)
			        .addPath(1, new Path(Relations.named("lastValue"), Direction.INCOMING)));

			// not participate in any virtual layer
			if (connectedDisplayPoints.isEmpty()) {
				if (logger.isDebugEnabled())
					logger.debug("Rack is not linked to any " + DISPLAYPOINT_TYPE_NAME + "  object");
				return NOT_AVAILABLE_LAYER;
			}

			TO displayPoint = connectedDisplayPoints.iterator().next();
			return new ZPropertyPickerStrategy(env, displayPoint).pickLayer();
		}
	}

	/**
	 * Image layer can be specified in tag
	 */
	private static class TagsBasedPickerStrategy implements LayerPickerStrategy {
		private Environment env;
		private TO<?> hostTo;
		private String taggedProperty;

		private TagsBasedPickerStrategy(Environment env, TO<?> hostTo, String taggedProperty) {
			this.env = env;
			this.hostTo = hostTo;
			this.taggedProperty = taggedProperty;
		}

		@Override
		public int pickLayer() {
			if (taggedProperty == null)
				return -1;
			try {
				Integer layer = env.getTagValue(hostTo, taggedProperty, "layer", Integer.class);
				return layer == null ? -1 : layer;
			} catch (EnvException e) {
				logger.warn("Failed to get layer tag of [" + hostTo + "." + taggedProperty + "]", e);
				return -1;
			}
		}
	}

	/**
	 * To describe alert root cause. Alerts happen on object but having alarmed
	 * property is useful as well.
	 */
	private static class AlertRootCause {
		private TO<?> obj;
		private String property;

		AlertRootCause(TO<?> obj) {
			this(obj, null);
		}

		AlertRootCause(TO<?> obj, String property) {
			this.obj = obj;
			this.property = property;
		}

		TO<?> getHostTo() {
			return obj;
		}

		/**
		 * may be null
		 * 
		 * @return
		 */
		String getProperty() {
			return property;
		}
	}

	/**
	 * Expand "dot notation"
	 * 
	 * @param env
	 *            Environment service
	 * @param refString
	 *            String like TO.prop[.prop[.prop]]
	 */
	private AlertRootCause expandObject(Environment env, String refString) {
		int idxOfFirstReference = refString.indexOf('.');
		if (idxOfFirstReference == -1) {
			return new AlertRootCause(TOFactory.getInstance().loadTO(refString));
		}

		String rootObjectPart = refString.substring(0, idxOfFirstReference);
		TO rootTo = TOFactory.getInstance().loadTO(rootObjectPart);

		String propRefsPart = refString.substring(idxOfFirstReference + 1);
		// property reference right next to last found object reference
		String rootCauseProperty = propRefsPart;

		TO tempHost = rootTo;
		while ((idxOfFirstReference = propRefsPart.indexOf('.')) != -1) {
			propRefsPart = propRefsPart.substring(0, idxOfFirstReference);
			rootCauseProperty = propRefsPart;
			Object link;
			try {
				link = env.getPropertyValue(tempHost, propRefsPart, Object.class);
			} catch (EnvException e) {
				logger.debug("Failed to dereference [" + tempHost + "." + propRefsPart + "]", e);
				break;
			}

			if (link == null) {
				logger.debug("Failed to dereference [" + tempHost + "." + propRefsPart + "] cause its value is null");
				break;
			}

			if (TO.class.isAssignableFrom(link.getClass())) {
				tempHost = (TO<?>) link;
			} else {
				break;
			}
		}

		return new AlertRootCause(tempHost, rootCauseProperty);
	}
}
