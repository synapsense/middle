package com.synapsense.impl.reporting.tables;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.synapsense.dto.TO;
import com.synapsense.impl.reporting.queries.AxisProcessor.ValueIdAttributesProvider;
import com.synapsense.service.reporting.olap.Member;
import com.synapsense.util.CollectionUtils;
import com.synapsense.util.Pair;

/**
 * The <code>HistoricalPropertyTypeProvider</code> class provides Historical Property values
 * 
 * @author Grigory Ryabov
 */
public class HistoricalPropertyTypeProvider implements ValueIdAttributesProvider{

	protected Map<String, Set<Integer>> valueIdsAtrributesMap;
	
	public HistoricalPropertyTypeProvider(){
		this.valueIdsAtrributesMap = CollectionUtils.newMap();
	}

	@Override
	public Map<String, Set<Integer>> getMap() {
		return Collections.unmodifiableMap(valueIdsAtrributesMap);
	}
	
	@Override
	public List<Pair<String, Integer>> addItems(List<TO<?>> objects, Member propertyMember) {

		List<String> propertyNames = CollectionUtils.newList();
		for (Member simplePropertyMember : propertyMember.getSimpleMembers()) {
			String[] keys = ((String) simplePropertyMember.getKey()).split("\\.");
			propertyNames.add(keys[1]);
		}
		List<Pair<String, Integer>> result = CollectionUtils.newArrayList(objects.size());
		for (TO<?> object : objects){
			for (String attributeName : propertyNames) {
				Integer objectId = (Integer) object.getID();
				if (valueIdsAtrributesMap.containsKey(attributeName)){
					valueIdsAtrributesMap.get(attributeName).add(objectId);
				} else {
					Set<Integer> objectIds = CollectionUtils.newSet();
					objectIds.add(objectId);
					valueIdsAtrributesMap.put(attributeName, objectIds);
				}
				result.add(Pair.newPair(attributeName, objectId));
			}
		}
		return result;
		
	}
}
