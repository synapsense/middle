package com.synapsense.impl.erlnode.commands;

import java.util.Map;

import org.apache.log4j.Logger;

import com.ericsson.otp.erlang.OtpErlangAtom;
import com.ericsson.otp.erlang.OtpErlangBinary;
import com.ericsson.otp.erlang.OtpErlangObject;
import com.ericsson.otp.erlang.OtpErlangTuple;
import com.synapsense.impl.erlnode.ExceptionTypes;
import com.synapsense.service.Environment;
import com.synapsense.service.nsvc.NotificationService;

import com.synapsense.impl.erlnode.utils.StringConverter;

public abstract class Command {

	private static final Logger log = Logger.getLogger(Command.class);

	public static OtpErlangAtom ok = new OtpErlangAtom("ok");
	public static OtpErlangAtom exception = new OtpErlangAtom("exception");
	public static OtpErlangAtom not_connected = new OtpErlangAtom("not_connected");

	protected Map<String, Object> globalState;

	public Command(Map<String, Object> state) {
		globalState = state;
	}

	public OtpErlangObject run(OtpErlangTuple command) throws Exception {
		if (command == null || command.arity() == 0) {
			return makeException(ExceptionTypes.ILLEGAL_ARGUMENTS, null);
		}
		Environment env = (Environment) globalState.get("env");
		if (env == null)
			return not_connected;
		try {
			return do_command(command);
		} catch (IllegalArgumentException e) {
			log.warn("Wrong arguments passed into command", e);
			return makeException(ExceptionTypes.ILLEGAL_ARGUMENTS, null);
		}
	}

	public abstract OtpErlangObject do_command(OtpErlangTuple command) throws Exception;

	public void flushState() {
	}

	protected Environment getEnv() {
		return (Environment) globalState.get("env");
	}

	protected NotificationService getNsvc() {
		return (NotificationService) globalState.get("nsvc");
	}

	public static OtpErlangTuple makeException(ExceptionTypes exType, String message) {
		return new OtpErlangTuple(new OtpErlangObject[] { exception, StringConverter.stringToUtf8Binary(exType.getExceptionType()),
		        StringConverter.stringToUtf8Binary((message != null && message != "") ? message : exType.getDefaultMessage()) });
	}

	public static OtpErlangObject makeOkResult(OtpErlangObject result) {
		if (result == null) {
			return ok;
		} else {
			return new OtpErlangTuple(new OtpErlangObject[] { ok, result });
		}
	}

}
