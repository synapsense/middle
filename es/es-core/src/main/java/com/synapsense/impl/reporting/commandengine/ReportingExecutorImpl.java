package com.synapsense.impl.reporting.commandengine;

import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class ReportingExecutorImpl implements ReportingExecutor {

    
    @Override
    @Asynchronous
    public void execute(Runnable command) {
        command.run();
    }

}