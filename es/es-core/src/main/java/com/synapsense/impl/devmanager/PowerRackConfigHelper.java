package com.synapsense.impl.devmanager;

import java.util.Collection;
import java.util.Comparator;

import com.synapsense.util.algo.Pair;
import com.synapsense.util.algo.SetsIntersector;
import com.synapsense.util.algo.SetsIntersector.AllValues;

public class PowerRackConfigHelper {

	private static SetsIntersector<Long> plugsIntersector = new SetsIntersector<Long>();
	private static SetsIntersector<Integer> phaseAndRpduIntersector = new SetsIntersector<Integer>();
	private static SetsIntersector<Pair<Integer, Integer>> serversIntersector = new SetsIntersector<Pair<Integer, Integer>>();

	private static Comparator<Long> plugsComparator = new Comparator<Long>() {
		@Override
		public int compare(Long o1, Long o2) {
			return o1.compareTo(o2);
		}
	};

	private static Comparator<Integer> rpduOrPhaseComparator = new Comparator<Integer>() {
		@Override
		public int compare(Integer o1, Integer o2) {
			return o1.compareTo(o2);
		}
	};

	private static Comparator<Pair<Integer, Integer>> serversComparator = new Comparator<Pair<Integer, Integer>>() {
		@Override
		public int compare(Pair<Integer, Integer> o1, Pair<Integer, Integer> o2) {
			Integer uPosComp = o1.getFirst().compareTo(o2.getFirst());
			if (uPosComp != 0) {
				return uPosComp;
			}
			return o1.getSecond().compareTo(o2.getSecond());
		}
	};

	public static AllValues<Long> intersectPlugsForNode(Collection<Long> newPlugs, Collection<Long> currentNodePlugs) {
		AllValues<Long> allValues = plugsIntersector.doAllJob(newPlugs, currentNodePlugs, plugsComparator);
		return allValues;
	}

	public static AllValues<Pair<Integer, Integer>> intersectServersForRack(
	        Collection<Pair<Integer, Integer>> newServers, Collection<Pair<Integer, Integer>> currentRackServers) {
		AllValues<Pair<Integer, Integer>> allValues = serversIntersector.doAllJob(newServers, currentRackServers,
		        serversComparator);
		return allValues;
	}

	public static AllValues<Integer> intersectRPDUsOrPhases(Collection<Integer> newRPDUsorPhases,
	        Collection<Integer> currentRPDUsorPhases) {
		AllValues<Integer> allValues = phaseAndRpduIntersector.doAllJob(newRPDUsorPhases, currentRPDUsorPhases,
		        rpduOrPhaseComparator);
		return allValues;
	}

}
