package com.synapsense.impl.performer.snmp;

import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;

import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.DependsOn;
import javax.ejb.Remote;
import javax.ejb.Singleton;
import javax.ejb.Startup;

import org.opennms.protocols.snmp.SnmpObjectId;
import org.opennms.protocols.snmp.SnmpOctetString;
import org.opennms.protocols.snmp.SnmpPduTrap;
import org.opennms.protocols.snmp.SnmpVarBind;

import com.synapsense.cdi.MBean;
import com.synapsense.dto.Message;
import com.synapsense.service.impl.notification.Transport;

@Startup @Singleton(name="SNMPTrapPerformer") @DependsOn("Registrar") @Remote(Transport.class)
@ConcurrencyManagement(ConcurrencyManagementType.BEAN)
@MBean("com.synapsense:type=SNMPTrapPerformer")
public class SynapSNMPPerformer extends SNMPTrapPerformer {
	private final static int GENERIC = 6; // 6 - Enterprise specific trap

	private final String PAYLOAD_OID = "1.3.6.1.4.1.29078.1.2";
	private final String ENTERPRISE_OID = "1.3.6.1.4.1.29078";
	private final int ENTERPRISE_SPECIFIC = 0;

	@Override
	protected SnmpPduTrap createPDU(Message data) {
		SnmpPduTrap pdu = new SnmpPduTrap();
		pdu.setGeneric(GENERIC);
		pdu.setSpecific(ENTERPRISE_SPECIFIC);
		pdu.setEnterprise(ENTERPRISE_OID);
		RuntimeMXBean mx = ManagementFactory.getRuntimeMXBean();
		pdu.setTimeStamp(mx.getUptime());

		pdu.addVarBind(new SnmpVarBind(new SnmpObjectId(PAYLOAD_OID), new SnmpOctetString(data.getContent().getBytes())));
		return pdu;
	}
}
