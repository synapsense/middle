package com.synapsense.impl.erlnode.commands;

import java.util.Map;

import com.ericsson.otp.erlang.OtpErlangAtom;
import com.ericsson.otp.erlang.OtpErlangBinary;
import com.ericsson.otp.erlang.OtpErlangBoolean;
import com.ericsson.otp.erlang.OtpErlangObject;
import com.ericsson.otp.erlang.OtpErlangTuple;
import com.synapsense.dto.AlertType;
import com.synapsense.dto.MessageTemplate;
import com.synapsense.dto.MessageType;
import com.synapsense.exception.AlertServiceSystemException;
import com.synapsense.exception.IllegalInputParameterException;
import com.synapsense.impl.erlnode.ExceptionTypes;
import com.synapsense.service.AlertService;
import com.synapsense.service.Environment;

import com.synapsense.impl.erlnode.utils.StringConverter;

public class GetAlertType extends Command {
	public final static String ALERT_TYPE_ATOM_ID = "alert_type";

	public GetAlertType(Map<String, Object> state) {
		super(state);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see Command#do_command(com.ericsson.otp.erlang.OtpErlangTuple)
	 */
	@Override
	public OtpErlangObject do_command(OtpErlangTuple command) throws Exception {
		OtpErlangObject alertTypeTuple = command.elementAt(1);
		if (alertTypeTuple == null || !(alertTypeTuple instanceof OtpErlangBinary)) {
			return makeException(ExceptionTypes.ILLEGAL_ARGUMENTS, null);
		}
		String alertTypeName = StringConverter.utf8BinaryToString(alertTypeTuple);
		try {
			AlertType alertType = ((AlertService) globalState.get("alertsvc")).getAlertType(alertTypeName);
			if (alertType == null) {
				return makeException(ExceptionTypes.OBJECT_NOT_FOUND, null);
			}
			Environment env = getEnv();
			MessageTemplate templ = alertType.getMessageTemplate(MessageType.CONSOLE);
			OtpErlangObject header;
			OtpErlangObject body;
			if (templ != null) {
				header = StringConverter.stringToUtf8Binary(templ.getHeader(env));
				body = StringConverter.stringToUtf8Binary(templ.getBody(env));

			} else {
				header = new OtpErlangAtom("undefined");
				body = new OtpErlangAtom("undefined");
			}
			return makeOkResult(new OtpErlangTuple(new OtpErlangObject[] { new OtpErlangAtom(ALERT_TYPE_ATOM_ID),
			        StringConverter.stringToUtf8Binary(alertType.getName()), StringConverter.stringToUtf8Binary(alertType.getDisplayName()),
			        StringConverter.stringToUtf8Binary(alertType.getDescription()), StringConverter.stringToUtf8Binary(alertType.getPriority()),
			        header, body, new OtpErlangBoolean(alertType.isAutoDismiss()) }));
		} catch (IllegalInputParameterException e) {
			return makeException(ExceptionTypes.ILLEGAL_ARGUMENTS, e.getMessage());
		} catch (AlertServiceSystemException e) {
			return makeException(ExceptionTypes.ALERT_SVC_SYSTEM, e.getMessage());
		}
	}

}
