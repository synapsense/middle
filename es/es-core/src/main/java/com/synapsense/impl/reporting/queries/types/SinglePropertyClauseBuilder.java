package com.synapsense.impl.reporting.queries.types;

import com.synapsense.impl.reporting.queries.TableProcessor.QueryClause;
import com.synapsense.impl.reporting.utils.ReportingConstants;
import com.synapsense.service.reporting.Cell;
import com.synapsense.util.CollectionUtils;
import com.synapsense.impl.reporting.queries.types.ReportingQueryTypeFactory.*;

import java.util.List;
import java.util.Map;

public class SinglePropertyClauseBuilder implements QueryTypeClauseBuilder {

    private static final Map<QueryClause, QueryTypeClauseBuilder> clauseBuilders = CollectionUtils.newMap();


    static {
        clauseBuilders.put(QueryClause.GROUP_BY,
                new GroupBySinglePropertyClauseBuilder());
        clauseBuilders.put(QueryClause.SELECT,
                new EmptyQueryTypeClauseBuilder());
        clauseBuilders.put(QueryClause.WHERE,
                new EmptyQueryTypeClauseBuilder());    }


    public QueryTypeClauseBuilder getClauseBuilder(QueryClause queryClause) {
        QueryTypeClauseBuilder clauseBuilder = clauseBuilders.get(queryClause);
        return clauseBuilder;
    }

    @Override
    public String build(QueryClause queryClause, List<Cell> valueIdCells) {

        return getClauseBuilder(queryClause).build(queryClause, valueIdCells);

    }

    private static class GroupBySinglePropertyClauseBuilder implements QueryTypeClauseBuilder {

        @Override
        public String build(QueryClause queryClause, List<Cell> valueIdCells) {

            StringBuilder clauseBuilder = new StringBuilder();

            clauseBuilder.append("value_id_fk")
                    .append(ReportingConstants.QUERY.SYMBOL.COMMA)
                    .append(ReportingConstants.QUERY.SYMBOL.SPACE);
            return clauseBuilder.toString();
        }
    }

}
