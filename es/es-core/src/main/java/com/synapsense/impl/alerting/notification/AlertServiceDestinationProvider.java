package com.synapsense.impl.alerting.notification;

import java.util.Collection;
import java.util.List;

import org.apache.log4j.Logger;

import com.synapsense.dto.MessageType;
import com.synapsense.exception.NoSuchAlertTypeException;
import com.synapsense.service.AlertService;
import com.synapsense.service.UserManagementService;
import com.synapsense.util.CollectionUtils;

public final class AlertServiceDestinationProvider implements DestinationProvider {
	private final static Logger logger = Logger.getLogger(AlertServiceDestinationProvider.class);

	private final AlertService service;
	private final UserManagementService userService;

	private final String alertTypeName;
	private final String tierName;
	private final MessageType messageType;

	public AlertServiceDestinationProvider(AlertService alertService, UserManagementService userService,
	        String alertTypeName, String tierName, MessageType messageType) {
		this.service = alertService;
		this.userService = userService;
		this.alertTypeName = alertTypeName;
		this.tierName = tierName;
		this.messageType = messageType;
	}

	@Override
	public List<Destination> getDestinations() {
		final List<Destination> result = CollectionUtils.newList();
		if (logger.isDebugEnabled())
			logger.debug("Getting destinations for alert type :" + alertTypeName + ", tier : " + tierName
			        + ", message type :" + messageType.name());
		Collection<String> destinations = CollectionUtils.newList();
		try {
			destinations = service.getActionDestinations(alertTypeName, tierName, messageType);
		} catch (NoSuchAlertTypeException e) {
			logger.error("Can't get destinations. Alert type " + alertTypeName + " not exist", e);
		}

		if (logger.isDebugEnabled())
			logger.debug("Retrieved destinations : " + destinations);

		for (final String destAsString : destinations) {
			logger.debug("Resolving destination : " + destAsString);
			final Destination dest = Destinations.newDestination(destAsString, userService);
			logger.debug("Destination : " + destAsString + " resolved as : " + dest.toString());
			result.add(dest);
		}
		return result;
	}
}
