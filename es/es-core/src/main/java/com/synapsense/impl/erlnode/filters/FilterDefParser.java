package com.synapsense.impl.erlnode.filters;

import com.ericsson.otp.erlang.OtpErlangTuple;
import com.synapsense.service.nsvc.EventProcessor;

interface FilterDefParser {
	EventProcessor parse(OtpErlangTuple filterDef);
}
