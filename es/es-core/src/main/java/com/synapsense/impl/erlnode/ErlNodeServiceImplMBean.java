package com.synapsense.impl.erlnode;

import com.synapsense.exception.AcApiException;
import com.synapsense.service.ErlNodeServiceManagement;

public interface ErlNodeServiceImplMBean extends ErlNodeServiceManagement {
	public String call_ac(String json_rpc) throws AcApiException;
}
