package com.synapsense.impl.transport.rmi.strategies;

import org.apache.log4j.Logger;

import com.synapsense.impl.transport.rmi.EventChannel;
import com.synapsense.impl.transport.rmi.EventChannelFailStrategy;

/**
 * Fail strategy based on count of event delivery tries.
 * 
 * @author Oleg Stepanov
 * 
 */
public class RetriesFailStrategy implements EventChannelFailStrategy {

	private static final Logger log = Logger.getLogger(RetriesFailStrategy.class);

	private final int triesNum;

	/**
	 * Constructs the strategy with allowed number of tries.
	 * 
	 * @param retriesNum
	 *            Maximum number of allowed event delivery tries. For example,
	 *            if triesNum equals 1 then channel is considered failed if it
	 *            was unable to send the event on the first attempt.
	 */
	public RetriesFailStrategy(int triesNum) {
		if (triesNum < 1)
			throw new IllegalArgumentException("triesNum cannot be less that 1 but specified " + triesNum);
		this.triesNum = triesNum;
	}

	@Override
	public boolean isFailed(EventChannel eventChannel) {
		boolean isFailed = eventChannel.getLastSendFailNum() >= triesNum;
		if (log.isDebugEnabled()) {
			log.debug("It was attempt #" + (eventChannel.getLastSendFailNum()) + " of " + triesNum + ", "
			        + (isFailed ? "NOT" : "") + " allowing to resend, client " + eventChannel.getClientAsString());
		}
		return isFailed;
	}

}
