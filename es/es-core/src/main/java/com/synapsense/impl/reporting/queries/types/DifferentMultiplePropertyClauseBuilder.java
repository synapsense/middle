package com.synapsense.impl.reporting.queries.types;

import java.util.List;
import java.util.Map;

import com.synapsense.impl.reporting.queries.TableProcessor;
import com.synapsense.impl.reporting.queries.TableProcessor.QueryClause;
import com.synapsense.impl.reporting.utils.ReportingConstants.QUERY.CLAUSE_PART;
import com.synapsense.impl.reporting.utils.ReportingConstants.QUERY.SYMBOL;
import com.synapsense.impl.reporting.utils.ReportingConstants.QUERY.WORD;
import com.synapsense.service.reporting.Cell;
import com.synapsense.util.CollectionUtils;

public class DifferentMultiplePropertyClauseBuilder implements QueryTypeClauseBuilder {


    private static final Map<QueryClause, QueryTypeClauseBuilder> clauseBuilders = CollectionUtils.newMap();

    static {
        clauseBuilders.put(QueryClause.GROUP_BY,
                new GroupByDifferentMultiplePropertyClauseBuilder());
        clauseBuilders.put(QueryClause.SELECT,
                new SelectDifferentMultiplePropertyClauseBuilder());
        clauseBuilders.put(QueryClause.WHERE,
                new WhereDifferentMultiplePropertyClauseBuilder());    }


    public QueryTypeClauseBuilder getClauseBuilder(QueryClause queryClause) {
        QueryTypeClauseBuilder clauseBuilder = clauseBuilders.get(queryClause);
        return clauseBuilder;
    }

    @Override
    public String build(QueryClause queryClause, List<Cell> valueIdCells) {

        return getClauseBuilder(queryClause).build(queryClause, valueIdCells);

    }
    
    private static class GroupByDifferentMultiplePropertyClauseBuilder implements QueryTypeClauseBuilder {

        @Override
        public String build(QueryClause queryClause, List<Cell> valueIdCells) {

            StringBuilder clauseBuilder = new StringBuilder();
            clauseBuilder.append("objects_valueids.object_id")
				.append(SYMBOL.COMMA)
				.append(SYMBOL.SPACE);
            clauseBuilder.append("objects_valueids.property_name")
				.append(SYMBOL.COMMA)
				.append(SYMBOL.SPACE);
            return clauseBuilder.toString();
            
        }
    }
    
    private static class SelectDifferentMultiplePropertyClauseBuilder implements QueryTypeClauseBuilder {

        @Override
        public String build(QueryClause queryClause, List<Cell> valueIdCells) {

            StringBuilder clauseBuilder = new StringBuilder();
            clauseBuilder.append("objects_valueids.object_id, ");
            clauseBuilder.append("objects_valueids.property_name, ");
            return clauseBuilder.toString();
            
        }
    }
    
    private static class WhereDifferentMultiplePropertyClauseBuilder implements QueryTypeClauseBuilder {

        @Override
        public String build(QueryClause queryClause, List<Cell> valueIdCells) {

            StringBuilder clauseBuilder = new StringBuilder();
            clauseBuilder.append(CLAUSE_PART.OBJECTS_WHERE_JOIN_BY_VALUEID)
				.append(SYMBOL.SPACE)
				.append(WORD.AND);
            return clauseBuilder.toString();
            
        }
    }
    
}
