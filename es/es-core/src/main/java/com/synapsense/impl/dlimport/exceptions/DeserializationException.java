package com.synapsense.impl.dlimport.exceptions;

public class DeserializationException extends RuntimeException {
	private static final long serialVersionUID = -1971901847187574061L;

	public DeserializationException(String _msg) {
		super(_msg);
	}
}
