
package com.synapsense.impl.erlnode.commands;

import java.util.Map;

import com.ericsson.otp.erlang.OtpErlangObject;
import com.ericsson.otp.erlang.OtpErlangTuple;
import com.synapsense.impl.erlnode.ExceptionTypes;
import com.synapsense.impl.erlnode.utils.ToConverter;
import com.synapsense.dto.TO;
import com.synapsense.exception.IllegalInputParameterException;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.exception.UnsupportedRelationException;

/**
 * @author stikhon
 * 
 */
public class SetRelationCommand extends Command {

	public SetRelationCommand(Map<String, Object> state) {
		super(state);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see Command#do_command(com.ericsson.otp.erlang.OtpErlangTuple)
	 */
	@Override
	public OtpErlangObject do_command(OtpErlangTuple command) throws Exception {
		OtpErlangObject parentToTuple = command.elementAt(1);
		OtpErlangObject childToTuple = command.elementAt(2);
		if (parentToTuple == null || childToTuple == null
				|| !(parentToTuple instanceof OtpErlangTuple)
				|| !(childToTuple instanceof OtpErlangTuple)) {
			return makeException(ExceptionTypes.ILLEGAL_ARGUMENTS, null);
		}
		TO<?> parentTo = ToConverter.parseTo((OtpErlangTuple) parentToTuple);
		TO<?> childTo = ToConverter.parseTo((OtpErlangTuple) childToTuple);
		try {
			getEnv().setRelation(parentTo, childTo);
			return makeOkResult(null);
		} catch (IllegalInputParameterException e) {
			return makeException(ExceptionTypes.ILLEGAL_ARGUMENTS, e
					.getMessage());
		} catch (ObjectNotFoundException e) {
			return makeException(ExceptionTypes.OBJECT_NOT_FOUND, e
					.getMessage());
		} catch (UnsupportedRelationException e) {
			return makeException(ExceptionTypes.UNSUPPORTED_RELATION, e
					.getMessage());
		}
	}

}
