package com.synapsense.impl.taskscheduler.tasks;

import java.util.Collection;

import org.apache.log4j.Logger;

import com.synapsense.dto.PropertyDescr;
import com.synapsense.dto.TO;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.exception.PropertyNotFoundException;
import com.synapsense.exception.UnableToConvertPropertyException;
import com.synapsense.service.Environment;

public class ParentChildChecker implements StatusChecker {
	private final static Logger logger = Logger.getLogger(SensorNodeStatusChecker.class);
	private Environment env;
	private String parent_type;
	private String child_type;

	public ParentChildChecker(String parent_type, String child_type) {
		this.parent_type = parent_type;
		this.child_type = child_type;
	}

	@Override
	public void check(Environment env) {
		this.env = env;
		Collection<TO<?>> parents = env.getObjectsByType(parent_type);
		logger.info("Number of " + parent_type + " inst.: " + parents.size());
		for (TO<?> parent : parents) {
			try {
				int status = getChildStatus(parent);
				PropertyDescr pd = env.getPropertyDescriptor(parent.getTypeName(), "status");
				Class<?> propType = pd.getType();
				// set parent status
				if (propType.equals(Boolean.class)) {
					env.setPropertyValue(parent, "status", status == 0 ? true : false);
				} else if (propType.equals(Integer.class)) {
					env.setPropertyValue(parent, "status", status);
				}
			} catch (ObjectNotFoundException | PropertyNotFoundException | UnableToConvertPropertyException e) {
				logger.error(e.getLocalizedMessage(), e);
            }
        }
	}

	private int getChildStatus(TO<?> parent) throws ObjectNotFoundException, PropertyNotFoundException,
	        UnableToConvertPropertyException {
		Collection<TO<?>> children = env.getChildren(parent, child_type);
		Integer result = 0;
		for (TO<?> child : children) {
			PropertyDescr pd = env.getPropertyDescriptor(child.getTypeName(), "status");
			Class<?> propType = pd.getType();
			if (propType.equals(Boolean.class)) {
				boolean tmp = env.getPropertyValue(child, "status", Boolean.class);
				if (tmp == false) {
					result = 1;
				}
			} else if (propType.equals(Integer.class)) {
				result = env.getPropertyValue(child, "status", Integer.class);
			}
			if (result != 0) {
				return result;
			}
		}
		return result;
	}

}
