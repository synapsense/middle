package com.synapsense.impl.alerting.notification;

import com.synapsense.dto.Message;
import com.synapsense.dto.MessageType;
import com.synapsense.impl.alerting.macros.Macro;
import com.synapsense.impl.alerting.macros.MacroContext;
import com.synapsense.util.CollectionUtils;
import com.synapsense.util.TranslatingEnvironment;
import org.apache.log4j.Logger;

import java.util.Locale;
import java.util.Map;

public class CommonMessageTemplate implements MessageTemplate {
	private final static Logger logger = Logger.getLogger(CommonMessageTemplate.class);

	private final String header;
	private final String body;

	// private final MessageType type;

	public CommonMessageTemplate(final String header, final String body) {
		this.header = header;
		this.body = body;
	}

	@Override
	public Message getMessage(final Macro macro, final MacroContext macroCtx, final Destination recipient,
	        final String priority, final MessageType messageType) {
		return createMessage(macro, macroCtx, recipient, priority, messageType);
	}

	private Message createMessage(final Macro macro, final MacroContext macroCtx, final Destination recipient,
	        final String priority, final MessageType messageType) {
		String bodyWithNoMacros = body;
		String headerWithNoMacros = header;

		if (logger.isDebugEnabled()) {
			logger.debug("creating message for destination: " + recipient.toString());
		}

		if (macro == null || macroCtx == null) {
			return new CommonMessage(null, messageType, headerWithNoMacros, bodyWithNoMacros, recipient, priority, null);
		}

		if (logger.isDebugEnabled()) {
			logger.debug("Macro replacing in progress...");
		}
		macroCtx.usrCtx = recipient.getUserContext();
		Locale locale = macroCtx.usrCtx.getLocale();
		String translatedHeader = (header.isEmpty()) ? header : macroCtx.localizationService.getTranslation(header,
		        locale);
		String translatedBody = (body.isEmpty()) ? body : macroCtx.localizationService.getTranslation(body, locale);
		headerWithNoMacros = macro.expandIn(translatedHeader, macroCtx);
		bodyWithNoMacros = macro.expandIn(translatedBody, macroCtx);
		Integer alertId = macroCtx.alert.getAlertId();

		if (logger.isDebugEnabled()) {
			logger.debug("All macros were successfully replaced");
		}

		Map<String, Object> mediaData = CollectionUtils.newMap(macroCtx.props, new CollectionUtils.Predicate<String>() {
            @Override
            public boolean evaluate(String s) {
                return "attachments".equals(s);
            }
        });

		return new CommonMessage(alertId, messageType, headerWithNoMacros, bodyWithNoMacros, recipient, priority,
		        mediaData);
	}

	@Override
	public String toString() {
		return "Message template [header=" + header + ", body=" + body + "]";
	}

}