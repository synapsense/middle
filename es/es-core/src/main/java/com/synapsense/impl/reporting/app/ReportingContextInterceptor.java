package com.synapsense.impl.reporting.app;

import com.synapsense.service.reporting.ReportingSessionContext;
import org.apache.log4j.Logger;

import javax.annotation.Resource;
import javax.ejb.EJBContext;
import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;
import java.security.Principal;

/**
 * EJB interceptor to propagate current caller name to reporting custom session
 * context
 */
public class ReportingContextInterceptor {

	private final static Logger logger = Logger.getLogger(ReportingContextInterceptor.class);

	@AroundInvoke
	public Object intercept(final InvocationContext icx) throws Exception {
		if (logger.isDebugEnabled()) {
			logger.debug("Entering interceptor : " + ReportingContextInterceptor.class.getName());
		}

		// find out that caller is not anonymous
		final Principal user = ctx.getCallerPrincipal();
		final String callerName = user.getName();
		if (logger.isDebugEnabled()) {
			logger.debug("Setting current caller name to : " + callerName);
		}
		ReportingSessionContext.setUserName(callerName);

		try {
			return icx.proceed();
		} finally {
			ReportingSessionContext.clear();
		}
	}

	@Resource
	void setCtx(final EJBContext ctx) {
		this.ctx = ctx;
	}

	@Resource
	private EJBContext ctx;

}
