package com.synapsense.impl.alerting.notification;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;

import com.synapsense.dto.Message;
import com.synapsense.impl.alerting.macros.MacroContext;
import com.synapsense.impl.alerting.macros.MessageNumMacro;
import com.synapsense.impl.alerting.macros.MessageTierMacro;

class AlertNotification implements Notification {
	private final static Logger logger = Logger.getLogger(AlertNotification.class);

	public AlertNotification(final String id, final List<NotificationState> states, final MacroContext ctx,
	        final NotificationTemplate template) {
		if (states.isEmpty()) {
			throw new IllegalArgumentException("Supplied 'states' is empty");
		}
		this.id = id;
		this.template = template;
		this.states = states;
		this.stateIterator = this.states.iterator();
		this.currState = stateIterator.next();
		this.lastFireTime = new Date();
		this.nextFireTime = lastFireTime;
		this.ctx = ctx;
		this.repeatCouter = currState.getLastAttempt();
		// set message tier and message number readers
		this.ctx.props.put(MessageTierMacro.MSG_TIER, new Object() {
			@Override
			public String toString() {
				return Integer.toString(currState.getOrdinal());
			}
		});

		this.ctx.props.put(MessageNumMacro.MSG_NUM, new Object() {
			@Override
			public String toString() {
				// + 1 because internal counter starts from 0(was done
				// to preserve implementation based on 0 as a start
				// value)
				return Integer.toString(repeatCouter + 1);
			}
		});
	}

	@Override
	public String getId() {
		return id;
	}

	@Override
	public ProcessResult process(final NotificationManager manager) {
		if (!isActive()) {
			return null;
		}

		if (logger.isDebugEnabled()) {
			logger.debug("Processing notification id=" + id + ", state=" + currState.getName());
		}

		if (logger.isDebugEnabled()) {
			logger.debug("Generating messages... id=" + id + ", state=" + currState.getName());
		}

		// if 0 then no need to send notifications
		if (currState.getRepeatLimit() > 0) {
			final List<Message> messages = template.generateMessages(currState.getName(), ctx);

			if (logger.isDebugEnabled()) {
				logger.debug(messages.size() + " messages were generated for further sending");
			}

			for (final Message m : messages) {
				if (!manager.send(m)) {
					logger.warn("Notification message : " + m + " was not sent");
				}
			}
		}

		ProcessResult pr = new ProcessResult();

		synchronized (this) {
			repeatCouter++;
			// preserve last fire time
			lastFireTime = new Date();
			// use interval of previous state for calculate next fire time
			// if repeat count is 0 then schedule next tier notification to run
			// immediately
			final long previousStateInterval = currState.getRepeatLimit() == 0 ? 1000L : currState.getInterval();

			if (repeatCouter >= currState.getRepeatLimit()) {
				if (stateIterator.hasNext()) {
					currState = stateIterator.next();
					repeatCouter = 0;
					pr.tier = currState.getName();
					pr.isTierChanged = true;
					if (logger.isDebugEnabled()) {
						logger.debug("Changing state of notification id=" + id + " to state=" + currState.getName());
					}
				} else {
					// set when last notification is sent
					complete();
				}
			}

			if (isActive()) {
				setNextFireTime(calculateNextFireTime(previousStateInterval));
			}
		}
		pr.na = repeatCouter;
		return pr;
	}

	@Override
	public Date getLastFireTime() {
		return lastFireTime;
	}

	@Override
	public Date getNextFireTime() {
		return nextFireTime;
	}

	@Override
	public boolean isActive() {
		return !done;
	}

	@Override
	public NotificationTemplate generatedBy() {
		return template;
	}

	@Override
	public String currentState() {
		synchronized (this) {
			return currState.getName();
		}
	}

	@Override
	public String toString() {
		return "Alert notification [id=" + id + ", current state=" + currState.getName() + ", last fire time="
		        + lastFireTime + ", next fire time=" + nextFireTime + ", times was sent= " + repeatCouter + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final AlertNotification other = (AlertNotification) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public MacroContext getContext() {
		return ctx;
	}

	private void setNextFireTime(final Date time) {
		nextFireTime = time;
	}

	private void complete() {
		done = true;
		if (logger.isDebugEnabled()) {
			logger.debug("Notification id=" + id + " was completed at state =" + currState.getName());
		}
	}

	private Date calculateNextFireTime(final long interval) {
		final long nextTime = lastFireTime.getTime() + interval;
		return new Date(nextTime);
	}

	private final List<NotificationState> states;
	private final Iterator<NotificationState> stateIterator;
	private volatile NotificationState currState;

	private final NotificationTemplate template;

	private Date lastFireTime;
	private Date nextFireTime;

	private int repeatCouter;
	private volatile boolean done;
	private final String id;
	private final MacroContext ctx;
}
