package com.synapsense.impl.reporting.model;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;

import org.jfree.util.Log;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.fill.AsynchronousFillHandle;

import com.synapsense.service.reporting.dto.ReportTaskInfo;
import com.synapsense.service.reporting.exception.CompilationReportingException;
import com.synapsense.service.reporting.exception.GenerationReportingException;
import com.synapsense.service.reporting.management.ReportParameter;
import com.synapsense.util.CollectionUtils;

public class ReportTemplate {

	private JasperReport jasperReport;
	
	private Map<String, ReportParameter> parameters = CollectionUtils.newLinkedMap();

	public ReportTemplate(String source) throws CompilationReportingException {
		this(new ByteArrayInputStream(source.getBytes()));
	}

	public ReportTemplate(InputStream source) throws CompilationReportingException {
		compile(source);
	}
	
	public String getName() {
		return jasperReport.getName();
	}

	public ReportParameter getParameter(String paramName) {
		return parameters.get(paramName);
	}

	public Collection<ReportParameter> getParameters() {
		return Collections.unmodifiableCollection(parameters.values());
	}

	public Report generateReport(ReportTaskInfo report) throws GenerationReportingException {
		try {
			return new JSReport(JasperFillManager.fillReport(jasperReport, getReportParameters(report)));
		} catch (JRException e) {
			throw new GenerationReportingException("Unable to generate report " + getName());
		}
	}

	public ReportGenerationHandler createReportGenerationHandler(Map<String, Object> params)
	        throws GenerationReportingException {
		try {
			return new ReportGenerationHandler(
					ReportingAsynchronousFillHandle.createHandle(jasperReport, params,
							new EmptyDataSource()));
		} catch (JRException e) {
			throw new GenerationReportingException(
					"Unable to create generation handler for report "
							+ getName());
		}
	}

	private static Map<String, Object> getReportParameters(ReportTaskInfo report) {
		Map<String, Object> params = report.getParametersValues();
		return params;
	}

	private void compile(InputStream source) throws CompilationReportingException {
		try {
			jasperReport = JasperCompileManager.compileReport(source);
			
			JRParameter[] paramsArray = jasperReport.getParameters();
			for (JRParameter param : paramsArray) {
				if (!param.isSystemDefined()) {
					if (!(Serializable.class.isAssignableFrom(param.getValueClass()))) {
						throw new CompilationReportingException("Report template parameter " + param.getName()
						        + " is not serializable");
					}
					parameters.put(param.getName(), new ReportParameter(param.getName(), param.getValueClass(), param.getDescription()));
				}
			}
		} catch (JRException e) {
			if (Log.isErrorEnabled()) {
				Log.error("Unable to compile report template ", e);
			}
			throw new CompilationReportingException("Unable to compile report template ");
		}
	}

	private class EmptyDataSource implements JRDataSource {
		private int i = 0;

		@Override
		public Object getFieldValue(JRField arg0) throws JRException {
			return null;
		}

		@Override
		public boolean next() throws JRException {
			if (i++ < 1) {
				return true;
			}
			return false;
		}

	}

}
