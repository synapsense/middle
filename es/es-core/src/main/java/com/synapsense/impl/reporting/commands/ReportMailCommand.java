package com.synapsense.impl.reporting.commands;

import com.synapsense.impl.reporting.commandengine.Command;
import com.synapsense.impl.reporting.commandengine.monitoring.StatusMonitor;
import com.synapsense.impl.reporting.dao.ReportDAO;
import com.synapsense.impl.reporting.export.ExportFormat;
import com.synapsense.impl.reporting.mail.JbossMailSession;
import com.synapsense.impl.reporting.mail.SMTPMailer;
import com.synapsense.service.reporting.dto.CommandStatus;
import com.synapsense.service.reporting.dto.ReportInfo;
import com.synapsense.service.reporting.exception.DAOReportingException;
import com.synapsense.service.reporting.exception.ObjectNotFoundDAOReportingException;
import com.synapsense.service.reporting.exception.SmtpReportingException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map.Entry;

public class ReportMailCommand implements Command {

	public static final String MESSAGE_SUBJECT = "SynapSense Report Notification";

	public static final String SINGLE_FILE_MESSAGE_BODY = "Your scheduled report is attached.";
	public static final String MULTIPLE_FILES_MESSAGE_BODY = "Your scheduled reports are attached.";

	private static final Log log = LogFactory.getLog(ReportMailCommand.class);

	private Collection<String> destinations;

	private ReportDAO reportDAO;

	private ReportInfo reportInfo;

	private boolean cancelled = false;

	public ReportMailCommand(ReportDAO reportDAO, Collection<String> destinations,
	        ReportInfo reportInfo) {
		this.reportDAO = reportDAO;
		this.destinations = destinations;
		this.reportInfo = reportInfo;
	}

	@Override
	public String getName() {
		return "Sending reports via email";
	}

	@Override
	public String getDescription() {
		return new StringBuilder().append("Sending report ").append(reportInfo.getTaskName())
		        .append(" via email to ").append(destinations).toString();
	}

	@Override
	public ReportInfo getReportInfo() {
		return this.reportInfo;
	}
	
	@Override
	public void run(StatusMonitor statusMonitor) {
		statusMonitor.setStatus(CommandStatus.IN_PROGRESS);
		if (log.isDebugEnabled()) {
			log.debug(getName() + " started...Sending report " + reportInfo.getTaskName() + " to " + destinations);
		}

		statusMonitor.setProgress(0.1);
		statusMonitor.setStatus(CommandStatus.inProgress("Processing report"));
		List<String> attachments = new ArrayList<String>();

        if (reportInfo.getReportId()==0) {
            throw  new IllegalStateException("Report instance should be persisted to proceed command execution");
        }
        
		for (Entry<String, String> entry : reportInfo.getExportedMap().entrySet()) {
			// Check if the report is already exported
			
			String path = reportInfo.getExportedPath(entry.getKey());
			try {
				if (path == null) {
					path = reportDAO.exportReport(reportInfo.getReportId(), ExportFormat.load(entry.getKey()));
				}
			} catch (DAOReportingException e) {
				log.error(getName() + " failed", e);
				statusMonitor.setStatus(CommandStatus.failed(e.getMessage()));
				throw new ReportingCommandException(e.getMessage(), e);
			}

			attachments.add(reportDAO.getExportRootPath() + "\\" + path);
		}

		statusMonitor.setProgress(0.5);
		statusMonitor.setStatus(CommandStatus.inProgress("Sending report"));

		if (cancelled) {
			if (log.isDebugEnabled()) {
				log.debug(getName() + " cancelled");
			}
			statusMonitor.setStatus(CommandStatus.CANCELLED);
			return;
		}

        SMTPMailer mailer = new SMTPMailer(new JbossMailSession());
        StringBuilder subjectSB = new StringBuilder();
        subjectSB.append(MESSAGE_SUBJECT);
        
        subjectSB.append(" - " + reportInfo.getTaskName());
        String messageBody = attachments.size() == 1 ? SINGLE_FILE_MESSAGE_BODY : MULTIPLE_FILES_MESSAGE_BODY;
		try {
			mailer.send(subjectSB.toString(), messageBody, attachments.toArray(new String[attachments.size()]),
			        destinations.toArray(new String[destinations.size()]));

			statusMonitor.setProgress(1.0);
			statusMonitor.setStatus(CommandStatus.completed("Message sent"));
			if (log.isDebugEnabled()) {
				log.debug(getName() + " completed successfully");
			}

		} catch (SmtpReportingException e) {
			statusMonitor.setStatus(CommandStatus.failed(e.getMessage()));
			log.error(getName() + " failed", e);
			throw new ReportingCommandException(e.getMessage(), e);
		}
	}

	@Override
	public void renew() {
		throw new UnsupportedOperationException();
	}

	@Override
	public void rollback() {
	}

	@Override
	public void interrupt() {
		if (log.isDebugEnabled()) {
			log.debug(getName() + "...received cancel request");
		}
		cancelled = true;
	}

	@Override
	public boolean pause() {
		return false;
	}
}
