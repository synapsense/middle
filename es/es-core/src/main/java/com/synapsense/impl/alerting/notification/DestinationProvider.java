package com.synapsense.impl.alerting.notification;

import java.util.List;

public interface DestinationProvider {
	List<Destination> getDestinations();
}