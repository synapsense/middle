package com.synapsense.impl.activitylog;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.mvel2.templates.TemplateRuntime;

import com.synapsense.dto.Alert;
import com.synapsense.dto.AlertType;
import com.synapsense.dto.TO;
import com.synapsense.impl.alerting.AlertServiceImpl;
import com.synapsense.impl.dlimport.DLImportServiceImpl;
import com.synapsense.impl.user.UserManagementServiceImpl;
import com.synapsense.service.impl.activitylog.ActivityRecord;
import com.synapsense.util.LocalizationUtils;

/**
 * Helper class for obtaining activity information basing on the invocation
 * context parameters. It is also contains current modules, actions
 * configuration.
 * 
 * @author Stepanov Oleg
 * 
 */
public class Activity {
	// let's use the same error logger as for ActivityLogger
	private final static Logger log = Logger.getLogger(ActivityLogger.class);

	private static Map<Method, String> messages = new HashMap<Method, String>();
	private static Map<Method, String> messageParams = new HashMap<Method, String>();
	private static Map<String, String> actions = new HashMap<String, String>();
	private static Map<Class<?>, String> moduleImpls = new HashMap<Class<?>, String>();

	// Init mappings and messages tables.
	// In the future configuration can be moved to a file.
	static {
		// modules
		moduleImpls.put(AlertServiceImpl.class, "AlertManagement");
		moduleImpls.put(UserManagementServiceImpl.class, "UserManagement");
		moduleImpls.put(DLImportServiceImpl.class, "MapSense");

		// actions
		try {
			final Method createUser = UserManagementServiceImpl.class.getMethod("createUser",
					String.class, String.class, Object.class);
			actions.put(createUser.getName(), "AddUser");
			messages.put(createUser, "activity_record_new_user_added");
			messageParams.put(createUser, "@{params[0]}");

			final Method updateUser = UserManagementServiceImpl.class.getMethod("updateUser", TO.class, Map.class);
			actions.put(updateUser.getName(), "EditUser");
			messages.put(updateUser, "activity_record_user_profile_modified");
			messageParams.put(updateUser, "@{params[0].getTypeName() + ':' + params[0].getID()}");

			final Method setProperty = UserManagementServiceImpl.class.getMethod("setProperty", TO.class, String.class, Object.class);
			actions.put(setProperty.getName(), "EditUser");
			messages.put(setProperty, "activity_record_user_profile_modified");
			messageParams.put(setProperty, "@{params[0].getTypeName() + ':' + params[0].getID()}");

			final Method setUserPassword = UserManagementServiceImpl.class.getMethod("setPassword", TO.class,
			        String.class);
			actions.put(setUserPassword.getName(), "ChangeUserPassword");
			messages.put(setUserPassword, "activity_record_user_password_changed");
			messageParams.put(setUserPassword, "@{params[0].getTypeName() + ':' + params[0].getID()}");

			final Method deleteUser = UserManagementServiceImpl.class.getMethod("deleteUser", TO.class);
			actions.put(deleteUser.getName(), "DeleteUser");
			messages.put(deleteUser, "activity_record_user_deleted");
			messageParams.put(deleteUser, "@{params[0].getTypeName() + ':' + params[0].getID()}");

			actions.put("createAlertType", "AddAlertType");
			{
				final Method method = AlertServiceImpl.class.getMethod("createAlertType", AlertType.class);
				messages.put(method, "activity_record_alert_type_created");
				messageParams.put(method, "@{params[0].displayName}");
			}

			actions.put("dismissAlert", "DismissObjectAlert");
			{
				final Method dismissAlertById = AlertServiceImpl.class.getMethod("dismissAlert", Integer.class,
				        String.class);
				messages.put(dismissAlertById, "activity_record_alert_with_id_dismissed");
				messageParams.put(dismissAlertById, "@{params[0]}");

				final Method dismissAlertByAlertInstance = AlertServiceImpl.class.getMethod("dismissAlert",
				        Alert.class, String.class);
				messages.put(dismissAlertByAlertInstance, "activity_record_alert_dismissed");
				messageParams.put(dismissAlertByAlertInstance, "@{params[0]}");

			}

			actions.put("acknowledgeAlert", "AcknowledgeAlert");
			{
				final Method method = AlertServiceImpl.class.getMethod("acknowledgeAlert", Integer.class, String.class);
				messages.put(method, "activity_record_alert_with_id_acknowledged");
				messageParams.put(method, "@{params[0]}");
			}

			actions.put("resolveAlert", "ResolveAlert");
			{
				final Method method = AlertServiceImpl.class.getMethod("resolveAlert", Integer.class, String.class);
				messages.put(method, "activity_record_alert_with_id_resolved");
				messageParams.put(method, "@{params[0]}");
			}

			actions.put("dismissAlerts", "DismissAllObjectAlerts");
			{

				final Method dismissAlertsOnSingleObject = AlertServiceImpl.class.getMethod("dismissAlerts", TO.class,
				        boolean.class, String.class);
				messages.put(dismissAlertsOnSingleObject, "activity_record_all_alerts_on_object_acknowledged");
				messageParams.put(dismissAlertsOnSingleObject, "@{params[0]}");

			}

			final Method deleteAlertType = AlertServiceImpl.class.getMethod("deleteAlertType", String.class);
			actions.put(deleteAlertType.getName(), "DeleteAlertType");
			messages.put(deleteAlertType, "activity_record_alert_type_deleted");
			messageParams.put(deleteAlertType, "@{target.getAlertType(params[0]).displayName}");

			final Method updateAlertType = AlertServiceImpl.class.getMethod("updateAlertType", String.class,
			        AlertType.class);
			actions.put(updateAlertType.getName(), "UpdateAlertType");
			messages.put(updateAlertType, "activity_record_alert_type_updated");
			messageParams.put(updateAlertType, "@{params[1].displayName},@{params[1].isActive()}");

		} catch (final NoSuchMethodException e) {
			throw new IllegalStateException(
			        "ActivityLogger initialization error. Cannot initialize activity logger service!", e);
		}

	}

	/**
	 * Generates new activity record.
	 * 
	 * @param userName
	 *            Who will be record owner
	 * @param target
	 *            Reference to helper object, can have any type
	 * @param actionId
	 *            Id of action
	 * @param params
	 *            additional parameters from context
	 * @return ActivityRecord instance
	 */
	public ActivityRecord log(String userName, final Object target, final Method actionId, final Object[] params) {
		String module = moduleImpls.get(target.getClass());
		if (module == null) {
			return null;
		}

		String action = actions.get(actionId.getName());
		if (action == null) {
			return null;
		}

		final Map<String, Object> map = new HashMap<String, Object>();
		map.put("params", params);
		map.put("target", target);

		final String template = messageParams.get(actionId);
		final String message = messages.get(actionId);

		String descr = null;

		if (message == null) {
			log.warn("No message found for method " + actionId);
			return null;
		}

		if (template != null) {
			try {
				Object templateInstanceObj = TemplateRuntime.eval(template, map);
				String templateInstance = templateInstanceObj != null ? templateInstanceObj.toString() : "null";
				descr = LocalizationUtils.getLocalizedString(message, templateInstance.split(","));
			} catch (final Exception t) {
				log.warn("Cannot evaluate expr \"" + template + "\" for method " + actionId, t);
				return null;
			}
		} else {
			descr = LocalizationUtils.getLocalizedString(message);
		}

		// if descr is not specified or determined
		if (descr == null) {
			log.warn("No description found for action " + actionId + ", module " + module);
		}

		return new ActivityRecord(userName, module, action, descr);
	}

}
