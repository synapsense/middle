package com.synapsense.impl.performer.audio;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.DependsOn;
import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Singleton;
import javax.ejb.Startup;

import com.synapsense.service.impl.notification.NotificationException;
import org.apache.log4j.Logger;

import com.synapsense.cdi.MBean;
import com.synapsense.dto.Message;
import com.synapsense.impl.performer.snmp.SNMPPerformerException;
import com.synapsense.impl.performer.snmp.conf.ConnectionInfoProvider;
import com.synapsense.impl.performer.snmp.conf.SNMPConfigurationException;
import com.synapsense.service.AlertService;
import com.synapsense.service.nsvc.NotificationService;
import com.synapsense.service.impl.notification.Transport;

@Startup
@Singleton(name = "AudioVisualPerformer")
@DependsOn({ "Registrar", "SynapCache" })
@Remote(Transport.class)
@ConcurrencyManagement(ConcurrencyManagementType.BEAN)
@MBean("com.synapsense:type=AudioVisualPerformer")
public class AudioVisualPerformer implements Transport, AudioVisualPerformerMBean {
	private final static Logger logger = Logger.getLogger(AudioVisualPerformer.class);
	private final String PERF_NAME = "AudioVisualPerformer";
	private static final CharSequence ACK_STATUS = new StringBuffer("acknowledged");
	private static final CharSequence RES_STATUS = new StringBuffer("resolved");
	private static final CharSequence ESCALATED = new StringBuffer("escalated");
	private final SignalSenders senders = new SignalSenders();

	@Override
	public boolean isValid(Message message) {
		return (!(message.getHeader().contains(ACK_STATUS) || message.getHeader().contains(RES_STATUS) || message
		        .getHeader().contains(ESCALATED)));
	}

	@Override
	public void send(Message metadata) throws NotificationException {
		if (!isValid(metadata))
			return;

		SignalSender sender;
		try {
			sender = senders.getSignalSender(metadata.getDestination());
			sender.send(metadata);
		} catch (SNMPConfigurationException e) {
			throw new NotificationException("Could not create SignalSender", e);
		} catch (SNMPPerformerException e) {
			throw new NotificationException("Could not send message", e);
		}
	}

	@EJB
	public void setAlertService(AlertService service) {
		senders.setAlertService(service);
	}

	@EJB(beanName = "LocalNotificationService")
	public void setNotificationService(NotificationService service) {
		StatusChangeListener listener = StatusChangeListener.getInstance();
		listener.setNotificationService(service);
		listener.startListener();
	}

	@Override
	public String getName() {
		return PERF_NAME;
	}

	@Override
	@PostConstruct
	public void create() {
		try {
			ConnectionInfoProvider.readConfig();
		} catch (SNMPConfigurationException e) {
			throw new IllegalStateException("Cannot read configuration", e);
		}
		logger.info("Creating the " + PERF_NAME + " alerts performer");
		start();
	}

	@Override
	public void start() {
		logger.info("Starting the " + PERF_NAME + " alerts performer");

	}

	@Override
	@PreDestroy
	public void stop() {
		logger.info("Stopping the " + PERF_NAME + " alerts performer");
		destroy();
	}

	@Override
	public void destroy() {
		logger.info("Destroying the " + PERF_NAME + " alerts performer");
		senders.clear();

	}

}
