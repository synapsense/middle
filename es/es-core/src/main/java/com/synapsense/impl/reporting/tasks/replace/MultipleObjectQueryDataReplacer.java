package com.synapsense.impl.reporting.tasks.replace;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.synapsense.impl.reporting.queries.TableProcessor.QueryClause;
import com.synapsense.impl.reporting.tasks.replace.QueryDataReplacerFactory.QueryDataReplacer;
import com.synapsense.service.reporting.Cell;

public class MultipleObjectQueryDataReplacer implements QueryDataReplacer {

	@Override
	public String replace(String envDataQuery, List<Cell> valueIdCells) {
		
		String localEnvDataQuery = envDataQuery;
		
		int index = 0;
		for (Cell valueIdCell : valueIdCells) {
			List<Integer> valueIds = valueIdCell.getValueIds();

			if (valueIds.isEmpty()) {
				if (localEnvDataQuery.contains(QueryClause.GROUP_BY.getName() + " value_id_fk in (:cellValueIds" + index +")")) {
					localEnvDataQuery = localEnvDataQuery.replaceAll(
							Pattern.quote("value_id_fk in (:cellValueIds"
									+ index + "), "),
							Matcher.quoteReplacement(""));
				} else {
					localEnvDataQuery = localEnvDataQuery.replaceAll(
							Pattern.quote(", value_id_fk in (:cellValueIds"
									+ index + ")"),
							Matcher.quoteReplacement(""));
				}
				continue;
			}
			index++;
		}
		return localEnvDataQuery;

	}

}
