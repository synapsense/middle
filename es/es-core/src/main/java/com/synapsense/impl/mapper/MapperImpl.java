package com.synapsense.impl.mapper;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import javax.annotation.PostConstruct;
import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.DependsOn;
import javax.ejb.Remote;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.xml.bind.JAXBException;

import org.apache.log4j.Logger;

import com.synapsense.cdi.MBean;
import com.synapsense.dto.PropertyDescr;
import com.synapsense.service.Mapper;
import com.synapsense.util.XmlConfigUtils;
import com.synapsense.util.algo.Pair;

@Startup @Singleton @DependsOn("Registrar") @Remote(Mapper.class)
@ConcurrencyManagement(ConcurrencyManagementType.BEAN)
@MBean("com.synapsense:type=PropertiesMapper")
public class MapperImpl implements Mapper, MapperImplMBean {
	private final static Logger logger = Logger.getLogger(MapperImpl.class);
	private Map<Pair<String, String>, Pair<String, Access>> mappings;

	private Object rwLock = new Object();
	private boolean autoFlush;

	private static class ConfProcessorHolder {
		final static String DATA_PATH = System.getProperty("com.synapsense.filestorage");
		final static String FILE_NAME = DATA_PATH + File.separator + "mapping.xml";
		static XmlConfigUtils proc;
		static {
			try {
				proc = new XmlConfigUtils(DATA_PATH, Mapping.class);
			} catch (JAXBException e) {
				logger.warn("Couldn't create the configuration reader.");
				proc = null;
			}
		}
	}

	private static Pair<String, String> createKey(String typeName, String propertyName) {
		return new Pair<String, String>(typeName, propertyName);
	}

	private Access convertAccess(Accesstype src) {
		switch (src) {
		case READ:
			return Access.READ;
		case WRITE:
			return Access.WRITE;
		case READWRITE:
			return Access.READWRITE;
		default:
			throw new IllegalArgumentException("Unexpected access type " + src.toString());
		}
	}

	private Accesstype convertAccess(Access src) {
		switch (src) {
		case READ:
			return Accesstype.READ;
		case WRITE:
			return Accesstype.WRITE;
		case READWRITE:
			return Accesstype.READWRITE;
		default:
			throw new IllegalArgumentException("Unexpected access type " + src.toString());
		}
	}

	private void readMappings() {
		Mapping map;
		if (ConfProcessorHolder.proc == null) {
			logger.warn("Cannot read mapping info - configuration processor is not set.");
			return;
		}
		synchronized (rwLock) {
			try {
				map = ConfProcessorHolder.proc.read(ConfProcessorHolder.FILE_NAME, Mapping.class);
			} catch (Exception e) {
				logger.warn("Couldn't read the mapping file", e);
				return;
			}
		}

		for (Item mapItem : map.getItem()) {
			setMapping(mapItem.getTypename(), mapItem.getPropname(), mapItem.getMappedname(),
			        convertAccess(mapItem.getAccess()));
		}
	}

	private void writeMappings() throws IOException {
		Mapping map = new Mapping();
		for (Entry<Pair<String, String>, Pair<String, Access>> entry : mappings.entrySet()) {
			Item item = new Item();
			item.setTypename(entry.getKey().getFirst());
			item.setPropname(entry.getKey().getSecond());
			item.setMappedname(entry.getValue().getFirst());
			item.setAccess(convertAccess(entry.getValue().getSecond()));
			map.getItem().add(item);
		}
		synchronized (rwLock) {
			if (ConfProcessorHolder.proc == null) {
				logger.warn("Cannot store mapping info - configuration processor is not set.");
				return;
			}
			ConfProcessorHolder.proc.writeToFile(ConfProcessorHolder.FILE_NAME, map, Mapping.class);
		}
	}

	@PostConstruct
	public void start() {
		readMappings();
	}

	public MapperImpl() {
		mappings = new ConcurrentHashMap<Pair<String, String>, Pair<String, Access>>();
		autoFlush = false;
	}

	@Override
	public Access getAccess(String typeName, String propertyName) {
		Pair<String, Access> mapping = mappings.get(createKey(typeName, propertyName));
		if (mapping == null)
			return Access.NONE;
		return mapping.getSecond();
	}

	@Override
	public Access getAccess(PropertyDescr pd) {
		return getAccess(pd.getTypeName(), pd.getName());
	}

	@Override
	public String getMappedName(String typeName, String propertyName) {
		Pair<String, Access> mapping = mappings.get(createKey(typeName, propertyName));
		if (mapping == null)
			return null;
		return mapping.getFirst();
	}

	@Override
	public String getMappedName(PropertyDescr pd) {
		return getMappedName(pd.getTypeName(), pd.getName());
	}

	@Override
	public void removeMapping(String typeName, String propertyName) {
		logger.info("Removing the DM mapping <" + typeName + ", " + propertyName + ">");
		mappings.remove(createKey(typeName, propertyName));
		if (autoFlush)
			try {
				writeMappings();
			} catch (IOException e) {
				logger.warn("Cannot store mapping info", e);
			}
	}

	@Override
	public void removeMapping(PropertyDescr pd) {
		removeMapping(pd.getTypeName(), pd.getName());
	}

	@Override
	public void setAccess(String typeName, String propertyName, Access access) {
		if (access == Access.NONE) {
			removeMapping(typeName, propertyName);
		} else {
			setMapping(typeName, propertyName, propertyName, access);
		}
	}

	@Override
	public void setAccess(PropertyDescr pd, Access access) {
		setAccess(pd.getTypeName(), pd.getName(), access);
	}

	@Override
	public void setMapping(String typeName, String propertyName, String mappedName, Access access) {
		if (access == Access.NONE)
			throw new IllegalArgumentException("access cannot be NONE");

		logger.info("Setting the DM Mapping <" + typeName + ", " + propertyName + "> to " + mappedName
		        + " with access level " + access.toString());
		mappings.put(createKey(typeName, propertyName), new Pair<String, Access>(mappedName, access));
		if (autoFlush)
			try {
				writeMappings();
			} catch (IOException e) {
				logger.warn("Cannot store mapping info", e);
			}
	}

	@Override
	public void setMapping(PropertyDescr pd, String mappedName, Access access) {
		setMapping(pd.getTypeName(), pd.getName(), mappedName, access);
	}

	@Override
	public void flush() throws IOException {
		writeMappings();
	}

	@Override
	public boolean getAutoFlush() {
		return autoFlush;
	}

	@Override
	public void setAutoFlush(boolean state) {
		autoFlush = state;
		logger.info("Auto flush status is set to " + state);
	}

	@Override
	public boolean isReadable(String typeName, String propertyName) {
		switch (getAccess(typeName, propertyName)) {
		case READ:
		case READWRITE:
			return true;
		default:
			return false;
		}
	}

	@Override
	public boolean isReadable(PropertyDescr pd) {
		return isReadable(pd.getTypeName(), pd.getName());
	}

	@Override
	public boolean isWritable(String typeName, String propertyName) {
		switch (getAccess(typeName, propertyName)) {
		case WRITE:
		case READWRITE:
			return true;
		default:
			return false;
		}
	}

	@Override
	public boolean isWritable(PropertyDescr pd) {
		return isWritable(pd.getTypeName(), pd.getName());
	}
}
