package com.synapsense.impl.erlnode.commands;

import java.util.Collection;
import java.util.Map;

import com.ericsson.otp.erlang.OtpErlangList;
import com.ericsson.otp.erlang.OtpErlangObject;
import com.ericsson.otp.erlang.OtpErlangTuple;
import com.synapsense.impl.erlnode.ExceptionTypes;
import com.synapsense.impl.erlnode.utils.CollectionToConverter;
import com.synapsense.impl.erlnode.utils.ToConverter;
import com.synapsense.impl.erlnode.utils.ValueTOConverter;
import com.synapsense.dto.TO;
import com.synapsense.exception.ObjectNotFoundException;

/**
 * @author stikhon
 * 
 */
public class GetAllPropertiesValuesCommand extends Command {

	public GetAllPropertiesValuesCommand(Map<String, Object> state) {
		super(state);
	}

	/**
	 * 
	 * command = {get_all_properties_values,list(to()) | to()}
	 * 
	 */
	@Override
	public OtpErlangObject do_command(OtpErlangTuple command) throws Exception {
		OtpErlangObject result = null;

		OtpErlangObject object1 = command.elementAt(1);
		// check if we received a list
		OtpErlangList otpList = CollectionToConverter.checkList(object1);
		if (otpList != null && otpList.arity() != 0) {
			Collection<TO<?>> toList = ToConverter.parseToList(otpList);
			result = makeOkResult(CollectionToConverter.serializeCollectionToList(getEnv().getAllPropertiesValues(
			        toList)));
		} else {
			if (object1 == null || !(object1 instanceof OtpErlangTuple)) {
				result = makeException(ExceptionTypes.ILLEGAL_ARGUMENTS, null);
			} else {
				TO<?> to = ToConverter.parseTo((OtpErlangTuple) object1);
				try {
					result = makeOkResult(ValueTOConverter.serializeValueTOList(getEnv().getAllPropertiesValues(to)));
				} catch (ObjectNotFoundException e) {
					result = makeException(ExceptionTypes.OBJECT_NOT_FOUND, e.getMessage());
				}
			}
		}

		return result;
	}
}
