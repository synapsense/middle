package com.synapsense.impl.alerting.macros;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Replaces macro matching given pattern with a particular property value from
 * the context. Can be used for simple replacement macros like
 * Pattern.compile("\\$(?i)msg_tier\\$") and
 * Pattern.compile("\\$(?i)msg_num\\$")
 * 
 * Property must be set in the MacroContext.props. However, it can be set to
 * null: in this case Macro.DEFAULT_EXPANSION will be used instead.
 * 
 * @author Oleg Stepanov
 * 
 */
public class ContextPropertyMacro extends AbstractPatternMacro {

	protected Pattern pattern;
	protected String ctxPropName;

	public ContextPropertyMacro(Pattern pattern, String ctxPropName) {
		this.pattern = pattern;
		this.ctxPropName = ctxPropName;
	}

	@Override
	public String expandIn(String input, MacroContext ctx) {
		if (input == null)
			return null;
		if (!ctx.props.containsKey(ctxPropName))
			throw new IllegalArgumentException("Property " + ctxPropName + " must be set in context");
		return super.expandIn(input, ctx);
	}

	@Override
	protected String expand(Matcher m, MacroContext ctx) {
		Object prop = ctx.props.get(ctxPropName);
		String res = prop != null ? prop.toString() : Macro.DEFAULT_EXPANSION;
		// try to translate result
		res = (res.isEmpty()) ? res : ctx.localizationService.getPartialTranslation(res, ctx.usrCtx.getLocale());
		return Matcher.quoteReplacement(res);
	}

	@Override
	protected Pattern getPattern() {
		return pattern;
	}

}
