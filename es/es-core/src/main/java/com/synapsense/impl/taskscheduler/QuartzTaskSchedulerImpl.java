package com.synapsense.impl.taskscheduler;

import com.synapsense.cdi.MBean;
import com.synapsense.impl.quartz.QuartzService;
import com.synapsense.impl.taskscheduler.config.Ejbtask;
import com.synapsense.impl.taskscheduler.config.Group;
import com.synapsense.service.UserManagementService;
import com.synapsense.service.nsvc.NotificationService;
import com.synapsense.service.nsvc.events.*;
import com.synapsense.service.nsvc.filters.EventTypeFilter;
import com.synapsense.util.CollectionUtils;

import org.apache.log4j.Logger;
import org.quartz.*;
import org.quartz.impl.matchers.GroupMatcher;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.*;

import java.text.ParseException;
import java.util.*;

@TransactionManagement(TransactionManagementType.CONTAINER)
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
@Startup
@Singleton
@Local(QuartzTaskSchedulerImplMBean.class)
@DependsOn("Registrar")
@ConcurrencyManagement(ConcurrencyManagementType.BEAN)
@MBean("com.synapsense:type=QuartzTaskScheduler")
public class QuartzTaskSchedulerImpl implements QuartzTaskSchedulerImplMBean {

	private final static Logger logger = Logger.getLogger(QuartzTaskSchedulerImpl.class);

	private Scheduler scheduler;

	/**
	 * Scheduler configuration
	 */
	private Configuration configuration;

	private NotificationService notificationService;
	private DispatchingProcessor dispProcessor;

	/** EJB session context */
	private EJBContext ctx;

	private UserManagementService userService;

	@EJB(beanName = "UserManagementService")
	public void setUserService(final UserManagementService userService) {
		this.userService = userService;
	}

	@EJB
	public void setQuartzService(QuartzService service) throws Exception {
		scheduler = service.getScheduler();
	}

	@Override
	@PostConstruct
	public void create() throws Exception {
		logger.info("QuartzTaskScheduler created");
		start();
	}

	@Override
	public void destroy() {
		// Bug 5937. Shut down the scheduler even if there are running tasks
		try {
			scheduler.shutdown(false);
		} catch (SchedulerException e) {
			logger.error("Unable to shutdown scheduler", e);
		}
	}

	private final Collection<String> runningTasks = new ArrayList<String>();

	@Override
	public void start() throws Exception {
		try {
			loadConfig();
		} catch (TaskSchedulerException e) {
			logger.warn("Error occurred while loading scheduler config", e);
		}

		// subscribe to events
		ConfigurationEventsProcessor eventProcessor = new ConfigurationEventsProcessor();
		dispProcessor = new DispatchingProcessor().putCcep(eventProcessor).putCsep(eventProcessor);
		notificationService.registerProcessor(
		        dispProcessor,
		        new EventTypeFilter().addEventType(ConfigurationStartedEvent.class).addEventType(
		                ConfigurationCompleteEvent.class));
		scheduler.start();
		logger.info("QuartzTaskScheduler started");
	}

	@Override
	@PreDestroy
	public void stop() {
		logger.info("Stopping QuartzTaskScheduler...");
		notificationService.deregisterProcessor(dispProcessor);
		for (String task : runningTasks) {
			try {
				removeTask(task);
			} catch (TaskSchedulerException e) {
				logger.warn("Couldn't remove the task " + task, e);
			}
		}
		runningTasks.clear();

		logger.info("QuartzTaskScheduler stopped");
		destroy();
	}

	private void loadConfig() throws TaskSchedulerException {
		String fileStorage = System.getProperty("com.synapsense.filestorage");
		if (fileStorage == null || fileStorage.isEmpty()) {
			logger.info("Unable to open file storage, property [com.synapsense.filestorage] is empty");
			return;
		}

		try {
			configuration = Configuration.newConfiguration(fileStorage);
			for (Group taskGroup : configuration.getTaskGroups()) {
				for (Ejbtask task : taskGroup.getEjbtask()) {
					String jndiName = task.getJndiName();
					String clazz = task.getClazz();
					String methodName = task.getMethod();
					String cron = task.getCronExpression();

					Long startTime = System.currentTimeMillis();
					if (task.getStartDelay() != null && task.getStartDelay() > 0) {
						startTime = System.currentTimeMillis() + task.getStartDelay() * 1000;
					}

					addEjbTask(clazz, taskGroup.getName(), null, cron, jndiName, clazz, methodName,
					        new Date(startTime), null);
					runningTasks.add(clazz);
				}
			}
		} catch (Exception e) {
			throw new TaskSchedulerException("Cannot read tasks configuration", e);
		}
	}

	@Override
	public void addEjbTask(final String taskName, final String taskParams, final String jndiName,
	        final String className, final String methodName, final LinkedHashMap<Class<?>, Object> params)
	        throws TaskSchedulerException {
		addEjbTask(taskName, Scheduler.DEFAULT_GROUP, taskParams, jndiName, className, methodName, params);
	}

	@Override
	public void addEjbTask(final String taskName, final String group, final String taskParams, final String jndiName,
	        final String className, final String methodName, final LinkedHashMap<Class<?>, Object> params)
	        throws TaskSchedulerException {
		String userName = ctx.getCallerPrincipal().getName();
		addEjbTask(taskName, group, userName, taskParams, jndiName, className, methodName, new Date(), params);
	}

	private void addEjbTask(final String taskName, final String group, final String principal, final String taskParams,
	        final String jndiName, final String className, final String methodName, final Date startTime,
	        final LinkedHashMap<Class<?>, Object> params) throws TaskSchedulerException {
		try {
			Trigger trigger = TriggerBuilder.newTrigger().withIdentity(taskName, group)
			        .withSchedule(CronScheduleBuilder.cronSchedule(taskParams))
			        .withPriority(configuration.getTaskGroupByName(group).getPriority()).startAt(startTime).build();
			JobBuilder jobBuilder = JobBuilder.newJob(EJB3InvokerJob.class).withIdentity(taskName, group)
			        .usingJobData(EJB3InvokerJob.EJB_JNDI_NAME_KEY, jndiName)
			        .usingJobData(EJB3InvokerJob.EJB_METHOD_KEY, methodName)
			        .usingJobData(EJB3InvokerJob.EJB_INTERFACE_CLASS, className);
			JobDataMap dataMap = new JobDataMap();
			if (params != null && !params.isEmpty()) {
				dataMap.put(EJB3InvokerJob.EJB_ARGS_KEY, params);
			}

			if (principal != null)
				dataMap.put(EJB3InvokerJob.PRINCIPAL, principal);

			jobBuilder.usingJobData(dataMap);

			scheduler.scheduleJob(jobBuilder.build(), trigger);

            if (logger.isDebugEnabled())
                logger.debug("EJB " + taskName + " scheduled for execution at \"" + trigger + "\"");
		} catch (Exception e) {
			throw new TaskSchedulerException("Cannot schedule EJB task " + taskName + " using cron " + taskParams, e);
		}
	}

	@Override
	public void addPlainTask(final String taskName, final String taskParams, final Class<? extends Job> clazz)
	        throws TaskSchedulerException {
		try {
			Trigger trigger = TriggerBuilder.newTrigger().withIdentity(taskName, Scheduler.DEFAULT_GROUP)
			        .withSchedule(CronScheduleBuilder.cronSchedule(taskParams)).build();

			JobDetail jd = JobBuilder.newJob(clazz).withIdentity(taskName, Scheduler.DEFAULT_GROUP).build();
			scheduler.scheduleJob(jd, trigger);

			if (logger.isDebugEnabled())
                logger.debug("Quartz job " + taskName + " scheduled for execution at \"" + taskParams + "\"");
		} catch (Exception e) {
			throw new TaskSchedulerException("Cannot schedule Quartz task " + taskName, e);
		}
	}

	@Override
	public boolean removeTask(final String group, final String taskName) throws TaskSchedulerException {
		return waitAndRemoveTask(group, taskName);
	}

	@Override
	public void removeGroup(String group) throws TaskSchedulerException {
		GroupMatcher<JobKey> matcher = GroupMatcher.groupEquals(group);

		Set<JobKey> jobKeys;
		try {
			jobKeys = scheduler.getJobKeys(matcher);
		} catch (SchedulerException e) {
			throw new TaskSchedulerException("Scheduler service is not available", e);
		}

		for (JobKey key : jobKeys) {
			if (logger.isDebugEnabled()) {
				logger.debug("Deleting job: " + key.getName() + " from group: " + key.getGroup());
			}
			try {
				removeTask(key.getGroup(), key.getName());
				if (logger.isDebugEnabled()) {
					logger.debug("Job: " + key.getName() + " from group: " + key.getGroup()
					        + " has been successfully deleted");
				}
			} catch (TaskSchedulerException e) {
				logger.warn("Failed to delete job: " + key.getName() + " from group: " + key.getGroup(), e);
			}
		}
	}

	@Override
	public boolean removeTask(final String taskName) throws TaskSchedulerException {
		return waitAndRemoveTask(Scheduler.DEFAULT_GROUP, taskName);
	}

	private final boolean waitAndRemoveTask(final String groupName, final String taskName)
	        throws TaskSchedulerException {
        if (logger.isDebugEnabled())
            logger.debug("Removing the task " + taskName);
		// no waits anymore, see #7914
		try {
			boolean deleted = scheduler.deleteJob(new JobKey(taskName, groupName));
			if (deleted)
                if (logger.isDebugEnabled())
                    logger.debug("Task " + taskName + " has been removed");
            else
                if (logger.isDebugEnabled())
                    logger.debug("There is no such scheduled task " + taskName);
			return deleted;
		} catch (SchedulerException e) {
			throw new TaskSchedulerException("Cannot remove task " + taskName, e);
		}
	}

	@Override
	public String showScheduledTasks() {
		try {
			StringBuilder sb = new StringBuilder();
			for (String group : scheduler.getJobGroupNames()) {
				sb.append("GROUP " + group);
				sb.append("\n");
				for (JobKey jobKey : scheduler.getJobKeys(GroupMatcher.<JobKey> groupEquals(group))) {
					sb.append("\t" + jobKey.getName());
					sb.append("\n");
					List<? extends Trigger> triggers = scheduler.getTriggersOfJob(jobKey);
					sb.append("\t" + " Available triggers:");
					sb.append("\n");
					for (Trigger trigger : triggers) {
						sb.append("\t\t" + trigger.getKey().getName() + ";priority " + trigger.getPriority());
					}
					sb.append("\n");
				}
			}
			return sb.toString();
		} catch (SchedulerException e) {
			return "Request error : " + e.getLocalizedMessage();
		}
	}

	@Override
	public Collection<String> getScheduledTasksByGroup(String group) throws TaskSchedulerException {
		try {
			return CollectionUtils.map(scheduler.getJobKeys(GroupMatcher.<JobKey> groupEquals(group)),
			        new CollectionUtils.Functor<String, JobKey>() {
				        @Override
				        public String apply(JobKey input) {
					        return input.getName();
				        }
			        });
		} catch (SchedulerException e) {
			throw new TaskSchedulerException("Failed to get tasks", e);
		}
	}

	@Override
	public Map<Class<?>, Object> getEjbTaskParams(final String taskName, final String group) throws TaskSchedulerException {
		try {
			JobDetail jd = scheduler.getJobDetail(new JobKey(taskName, group));
            return (Map<Class<?>, Object>) jd.getJobDataMap().get(EJB3InvokerJob.EJB_ARGS_KEY);
		} catch (SchedulerException e) {
            throw new TaskSchedulerException("Failed to get task parameter", e);
		}
	}

    @Override
    public boolean isCronValid(String cron) {
		try {
			if (cron == null) return false;
			CronExpression cronExp = new CronExpression(cron);
			
        	Date fireDate = cronExp.getNextValidTimeAfter(new Date());
        	logger.debug("Fire date = " + fireDate);
        	if(fireDate == null) {
        		return false;
        	}
		} catch (ParseException e) {
			return false;
		}
		return true;
    }

    @Override
	public boolean isTaskAlreadyScheduled(final String group, final String taskName) {
		try {
			return scheduler.checkExists(new JobKey(taskName, group));
		} catch (SchedulerException e) {
			logger.warn("Request error : " + e.getLocalizedMessage());
		}
		return false;
	}

	@EJB(beanName = "LocalNotificationService")
	public void setNotificationService(NotificationService notificationService) {
		this.notificationService = notificationService;
	}

    @Resource
    public void setCtx(EJBContext ctx) {
        this.ctx = ctx;
    }

	private class ConfigurationEventsProcessor implements ConfigurationCompleteEventProcessor,
	        ConfigurationStartedEventProcessor {

		@Override
		public ConfigurationStartedEvent process(ConfigurationStartedEvent event) {
			try {
				if (logger.isDebugEnabled()) {
					logger.debug("ES configration started. Switching scheduler to stand by mode");
				}
				scheduler.standby();
			} catch (SchedulerException e) {
				logger.error("Unable to put scheduler on a standby mode", e);
			}
			return event;
		}

		@Override
		public ConfigurationCompleteEvent process(ConfigurationCompleteEvent event) {
			try {
				if (logger.isDebugEnabled()) {
					logger.debug("ES configration complete. Scheduler will wake up in 5 minutes");
				}
				// We start scheduler in 5 minutes afer Mapsense export as we
				// don't want to have dummy alerts based on incorrect data
				scheduler.startDelayed(5 * 60);
			} catch (SchedulerException e) {
				logger.error("Unable to start scheduler", e);
			}
			return event;
		}
	}
}