package com.synapsense.impl.reporting.model;

import com.synapsense.service.reporting.exception.GenerationReportingException;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.fill.AsynchronousFillHandle;
import net.sf.jasperreports.engine.fill.AsynchronousFilllListener;

public class ReportGenerationHandler {

	private AsynchronousFillHandle jasperHandle;

	private boolean isRunning = false;

	public ReportGenerationHandler(AsynchronousFillHandle jasperHandle) {
		this.jasperHandle = jasperHandle;
	}

	public void addListener(ReportGenerationListener listener) {
		jasperHandle.addListener(new JasperReportFillListener(listener));
	}

	public void startGeneration() {
		isRunning = true;
		jasperHandle.startFill();
	}

	public void cancelGeneration() throws GenerationReportingException {
		try {
			jasperHandle.cancellFill();
		} catch (JRException e) {
			throw new GenerationReportingException("Unable to cancel report generation");
		}
	}

	public boolean isRunning() {
		return isRunning;
	}

	private class JasperReportFillListener implements AsynchronousFilllListener {

		private ReportGenerationListener listener;

		private JasperReportFillListener(ReportGenerationListener listener) {
			this.listener = listener;
		}

		@Override
		public void reportCancelled() {
			isRunning = false;
			listener.cancelled();
		}

		@Override
		public void reportFillError(Throwable t) {
			isRunning = false;
			listener.failed(t);
		}

		@Override
		public void reportFinished(JasperPrint jasperPrint) {
			isRunning = false;
			listener.completed(new JSReport(jasperPrint));
		}
	}
}
