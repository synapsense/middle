package com.synapsense.impl.alerting;

import static com.synapsense.impl.alerting.AlertConstants.TYPE.ALERT_DESCRIPTION_TYPE_NAME;
import static com.synapsense.impl.alerting.AlertConstants.TYPE.ALERT_PRIORITY_TIER_TYPE_NAME;
import static com.synapsense.impl.alerting.AlertConstants.TYPE.ALERT_PRIORITY_TYPE_NAME;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

import com.synapsense.dto.ChangedValueTO;
import com.synapsense.dto.CollectionTO;
import com.synapsense.dto.MessageType;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.AlertServiceSystemException;
import com.synapsense.exception.EnvException;
import com.synapsense.search.Direction;
import com.synapsense.search.Path;
import com.synapsense.search.QueryDescription;
import com.synapsense.search.Relations;
import com.synapsense.service.Environment;
import com.synapsense.service.nsvc.events.PropertiesChangedEvent;
import com.synapsense.util.CollectionUtils;

public final class Utils {
	
	public static TO<?> discoverAlertId(PropertiesChangedEvent e) {
		TO<?> objectChanged = e.getObjectTO();
		if (Utils.isAlert(objectChanged)) {
			return objectChanged;
		} else if (Utils.isAlertHistoryType(objectChanged)) {
			ChangedValueTO alertId = CollectionUtils.search(Arrays.asList(e.getChangedValues()),
			        new CollectionUtils.Predicate<ChangedValueTO>() {
				        @Override
				        public boolean evaluate(ChangedValueTO t) {
					        return t.getPropertyName().equals("alertId");
				        }
			        });
			return Utils.loadAlertID((Integer) alertId.getValue());
		}
		return null;
	}
	
	public static boolean isAlert(TO<?> id) { 
		if (id.getTypeName().equals(AlertConstants.TYPE.ALERT_TYPE_NAME)) { 
			return true;
		}
		return false;
	}
	
	public static boolean isAlertHistoryType(TO<?> id) { 
		if (id.getTypeName().equals(AlertConstants.TYPE.ALERT_HISTORY_TYPE_NAME)) { 
			return true;
		}
		return false;
	}
	
	public static TO<?> findDestination(Environment env, TO<?> alertType, String tier, String example, MessageType mt) {
		ValueTO[] criteria = new ValueTO[] { new ValueTO("alertType", alertType), new ValueTO("tier", tier),
		        new ValueTO("destination", example), new ValueTO("messagetype", mt.code()) };
		Collection<TO<?>> destinations = env.getObjects(AlertConstants.TYPE.ALERT_DESTINATION_TYPE_NAME, criteria);
		if (destinations.isEmpty()) {
			return null;
		}

		if (destinations.size() > 1) {
			throw new AlertServiceSystemException("Mupltiple destinations having the same url {" + example + "} , "
			        + destinations);
		}

		return destinations.iterator().next();
	}
	
	public static boolean existTier(Environment env, TO<?> alertType, final String tierName) {
		// get all refs from tiers
		QueryDescription qd = new QueryDescription(2);
		qd.startQueryFrom(alertType);
		qd.addPath(1, new Path(Relations.named("priority"), Direction.OUTGOING, false));
		qd.addPath(2, new Path(Relations.named("tiers"), Direction.OUTGOING));

		Collection<TO<?>> tiersRefs = env.getObjects(qd);
		Collection<CollectionTO> tiersNames = env.getPropertyValue(tiersRefs, new String[] { "name" });

		Collection<String> actualTierNames = CollectionUtils.map(tiersNames,
		        new CollectionUtils.Functor<String, CollectionTO>() {
			        @Override
			        public String apply(CollectionTO input) {
				        ValueTO value = input.getSinglePropValue("name");
				        if (value == null)
					        return "";
				        return value.getValue().toString();
			        }
		        });

		return actualTierNames.contains(tierName);
	}
	
	public static TO<?> findAlertPriorityTier(Environment env, String priority, String tierName) {
		
		TO<?> alertPriorityID = findAlertPriority(env, priority);

		// get all refs from tiers
		QueryDescription qd = new QueryDescription(1);
		qd.startQueryFrom(alertPriorityID);
		qd.addPath(1, new Path(Relations.named("tiers"), Direction.OUTGOING));

		Collection<TO<?>> tiersRefs = env.getObjects(qd);
		for (TO<?> tierRef : tiersRefs) {
			try {
				String tierRefName = env.getPropertyValue(tierRef, "name", String.class);
				if (tierName.equals(tierRefName)) {
					return tierRef;
				}
			} catch (EnvException e) {
				throw new AlertServiceSystemException("Detected malformed object type : "
				        + ALERT_PRIORITY_TIER_TYPE_NAME, e);
			}
		}

		return null;
	}
	
	public static TO<?> findAlertPriority(Environment env, String name) {
		ValueTO[] criteria = new ValueTO[] { new ValueTO("name", name) };
		Collection<TO<?>> alertPriorities = env.getObjects(ALERT_PRIORITY_TYPE_NAME, criteria);
		
		if (alertPriorities.isEmpty()) {
			return null;
		}
		
		if (alertPriorities.size() > 1) {
			throw new AlertServiceSystemException("Mupltiple alert priorities having the same name {" + name + "} , "
			        + alertPriorities);
		}

		return alertPriorities.iterator().next();
	}
	
	public static TO<?> findAlertTypeByName(Environment env, String typeName) { 
		ValueTO[] criteria = new ValueTO[] { new ValueTO("name", typeName) };
		Collection<TO<?>> alertDescriptors = env.getObjects(ALERT_DESCRIPTION_TYPE_NAME, criteria);
		if (alertDescriptors.isEmpty()) {
			return null;
		}
		if (alertDescriptors.size() > 1) {
			throw new AlertServiceSystemException("Mupltiple alert types having the same name {" + typeName + "} , "
			        + alertDescriptors);
		}
		
		return alertDescriptors.iterator().next();
	}
	
	public static Collection<String> findDestinations(Environment env, TO<?> alertType, String tier, MessageType mt) {
		ValueTO[] criteria = new ValueTO[] { new ValueTO("alertType", alertType), new ValueTO("tier", tier),
		        new ValueTO("messagetype", mt.code()) };
		Collection<TO<?>> destinationRefs = env.getObjects(AlertConstants.TYPE.ALERT_DESTINATION_TYPE_NAME, criteria);
		if (destinationRefs.isEmpty()) {
			return Collections.emptyList();
		}

		Collection<CollectionTO> destinations = env.getPropertyValue(destinationRefs, new String[] { "destination" });
		return CollectionUtils.map(destinations, new CollectionUtils.Functor<String, CollectionTO>() {
			@Override
			public String apply(CollectionTO input) {
				return (String) input.getSinglePropValue("destination").getValue();
			}
		});
	}
	
	public static TO<?> loadAlertID(int id) {
		return TOFactory.INSTANCE.loadTO(AlertConstants.TYPE.ALERT_TYPE_NAME, id);
	}
	
	private Utils() { 
	}
}
