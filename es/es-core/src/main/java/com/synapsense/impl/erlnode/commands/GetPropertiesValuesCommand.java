
package com.synapsense.impl.erlnode.commands;

import java.util.Collection;
import java.util.Map;

import com.ericsson.otp.erlang.OtpErlangBinary;
import com.ericsson.otp.erlang.OtpErlangList;
import com.ericsson.otp.erlang.OtpErlangObject;
import com.ericsson.otp.erlang.OtpErlangTuple;
import com.synapsense.impl.erlnode.ExceptionTypes;
import com.synapsense.impl.erlnode.utils.CollectionToConverter;
import com.synapsense.impl.erlnode.utils.ToConverter;
import com.synapsense.dto.TO;

import com.synapsense.impl.erlnode.utils.StringConverter;

/**
 * @author stikhon
 * 
 */
public class GetPropertiesValuesCommand extends Command {

	public GetPropertiesValuesCommand(Map<String, Object> state) {
		super(state);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see Command#do_command(com.ericsson.otp.erlang.OtpErlangTuple)
	 */
	@Override
	public OtpErlangObject do_command(OtpErlangTuple command) throws Exception {

		OtpErlangObject objectsTuple = command.elementAt(1);
		OtpErlangObject typesTuple = command.elementAt(2);
		if (objectsTuple == null || typesTuple == null
				|| !(objectsTuple instanceof OtpErlangList)
				|| !(typesTuple instanceof OtpErlangList)) {
			return makeException(ExceptionTypes.ILLEGAL_ARGUMENTS, null);
		}
		Collection<TO<?>> toList = ToConverter
				.parseToList(((OtpErlangList) objectsTuple));

		OtpErlangList propNamesList = (OtpErlangList) typesTuple;
		int propNamesListSize = propNamesList.arity();
		String[] propNamesValue = new String[propNamesListSize];
		for (int i = 0; i < propNamesListSize; i++) {
			propNamesValue[i] = StringConverter.utf8BinaryToString(propNamesList.elementAt(i));
		}

		return makeOkResult(CollectionToConverter
				.serializeCollectionToList((getEnv().getPropertyValue(toList,
						propNamesValue))));
	}
}
