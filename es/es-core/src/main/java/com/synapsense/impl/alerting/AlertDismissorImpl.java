package com.synapsense.impl.alerting;

import com.synapsense.dto.TO;
import com.synapsense.exception.AlertServiceSystemException;
import com.synapsense.service.AlertService;
import com.synapsense.util.CollectionUtils;
import com.synapsense.util.LocalizationUtils;
import org.apache.log4j.Logger;

import java.util.Map;
import java.util.concurrent.*;

public class AlertDismissorImpl implements AlertDismissor {
	private final static Logger logger = Logger.getLogger(AlertDismissorImpl.class);
	private static final String THREAD_NAME = "ALERT_AUTO_DISMISSOR";

	// map to store all scheduled tasks
	// used for canceling
	private final Map<TO<?>, ScheduledFuture> scheduledAutodismissAlerts;
	private final AlertService alertService;
	private final ScheduledThreadPoolExecutor scheduler;

	public AlertDismissorImpl(AlertService alertService) {
		this.alertService = alertService;
		this.scheduledAutodismissAlerts = CollectionUtils.newMap();
		this.scheduler = new ScheduledThreadPoolExecutor(1, new ThreadFactory() {
			@Override
			public Thread newThread(Runnable r) {
				return new Thread(r, THREAD_NAME);
			}
		});
		this.scheduler.setRemoveOnCancelPolicy(true);
	}

	@Override
	public void scheduleAlertDismission(TO<?> alertTO, long delay) {
		if (logger.isDebugEnabled()) {
			logger.debug("Scheduling alert " + alertTO + " for dismiss after " + delay + " "
			        + TimeUnit.MILLISECONDS.toString());
		}

		// let's prevent from inconsistent state of internal map and scheduler
		synchronized (scheduledAutodismissAlerts) {
			ScheduledFuture future = scheduledAutodismissAlerts.get(alertTO);

			// try to cancel if exists
			if (future != null) {
				if (logger.isDebugEnabled()) {
					logger.debug("Alert " + alertTO + " has already been scheduled, time remains to run is "
					        + future.getDelay(TimeUnit.MILLISECONDS) + " " + TimeUnit.MILLISECONDS.toString());
				}

				cancelAlertDismissing(future, alertTO);

				if (logger.isDebugEnabled()) {
					logger.debug("Alert " + alertTO + " schedule has been cancelled");
				}
			}

			// reschedule with new delay
			submitTask(alertTO, delay);
		}
	}

	@Override
	public void purgeAll() {
		synchronized (scheduledAutodismissAlerts) {
			for (ScheduledFuture f : this.scheduledAutodismissAlerts.values()) {
				f.cancel(true);
			}
			scheduledAutodismissAlerts.clear();
		}
	}

	private void cancelAlertDismissing(ScheduledFuture future, TO<?> alert) {
		if (!future.isDone())
			future.cancel(true);
		scheduledAutodismissAlerts.remove(alert);
	}

	private void submitTask(TO<?> alert, long delay) {
		Runnable schedulingTask = new AlertAutoDismissSchedulingTask(alert,
		        LocalizationUtils.getLocalizedString("alert_comment_automatically_cleared"));
		try {
			ScheduledFuture future = scheduler.schedule(schedulingTask, delay, TimeUnit.MILLISECONDS);
			scheduledAutodismissAlerts.put(alert, future);
		} catch (RejectedExecutionException e) {
			logger.warn("Failed to schedule alert " + alert + " for dismiss due to scheduler internal error", e);
		}

		if (logger.isDebugEnabled()) {
			logger.debug("Alert " + alert + " has been successfully scheduled for dismiss. It will be dismissed after "
			        + delay + " ms");
		}

	}

	private class AlertAutoDismissSchedulingTask implements Runnable {
		private final TO<?> alert;
		private final String dismissComment;

		public AlertAutoDismissSchedulingTask(final TO<?> alert, final String dismissComment) {
			this.alert = alert;
			this.dismissComment = dismissComment;
		}

		@Override
		public void run() {
			try {
				logger.info("Auto-dismissing the alert " + alert);
				alertService.dismissAlert((Integer) alert.getID(), dismissComment);
			} catch (AlertServiceSystemException ex) {
				if (logger.isDebugEnabled())
					logger.debug("The alert is already dismissed " + alert);
			} catch (Exception e) {
				logger.warn("Couldn't dismiss the alert " + alert, e);
			} finally {
				synchronized (scheduledAutodismissAlerts) {
					scheduledAutodismissAlerts.remove(alert);
				}
			}
		}

		@Override
		public boolean equals(Object o) {
			if (this == o)
				return true;
			if (o == null || getClass() != o.getClass())
				return false;

			AlertAutoDismissSchedulingTask that = (AlertAutoDismissSchedulingTask) o;

			if (!alert.equals(that.alert))
				return false;

			return true;
		}

		@Override
		public int hashCode() {
			return alert.hashCode();
		}
	}

}
