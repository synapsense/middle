package com.synapsense.impl.reporting.export;

import com.synapsense.impl.reporting.model.JSReport;
import com.synapsense.impl.reporting.model.Report;
import com.synapsense.service.FileWriter;
import com.synapsense.service.reporting.exception.ExportingReportingException;

public interface ReportExporter {

	void export(Report report, String outFilePath) throws ExportingReportingException;

	void export(JSReport report, String outFilePath) throws ExportingReportingException;

	void export(JSReport jsReport, String fullPath, FileWriter fileWriter) throws ExportingReportingException;
}
