package com.synapsense.impl.fws;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.security.AccessControlException;
import java.util.Collection;
import java.util.LinkedList;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;
import javax.annotation.security.RolesAllowed;
import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.DependsOn;
import javax.ejb.Remote;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import org.apache.log4j.Logger;

import com.synapsense.service.FileWriter;

@Startup @Singleton(name="FileWriterService") @DependsOn("Registrar") @Remote(FileWriter.class)

@ConcurrencyManagement(ConcurrencyManagementType.BEAN)
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)

public class FileWriterImpl implements FileWriter {
	private final static Logger logger = Logger.getLogger(FileWriterImpl.class);

	private static String CRLF = System.getProperty("line.separator");
	
	private static final int BUFFER_SIZE = 20 * 1024 * 1024;
	
	static {
		if (CRLF == null) {
			CRLF = "\n";
		}
	}
	private static String PERMISSIONS_FILE_NAME = "filewriter.txt";
	
	private String rootDir;
	private Collection<Pattern> permissions;

	@PostConstruct
	public void start() {
		logger.info("FileWriter is starting...");

		rootDir = System.getProperty("com.synapsense.filestorage");
		if (rootDir != null && !rootDir.isEmpty() && checkDir(rootDir)) {
			logger.info("The root directory is set to " + rootDir + " using com.synapsense.filestorage system property");
		} else {
			rootDir = ".";
			logger.warn("The root directory is set to the default value " + new File(rootDir).getAbsolutePath());
		}
		
		permissions = readPermissions(rootDir);
	}
	
	private static Collection<Pattern> readPermissions(String rootDir) {
		File fileToRead = new File(rootDir, PERMISSIONS_FILE_NAME);
		logger.info("Reading permissions from file " + fileToRead.getAbsolutePath());

		LinkedList<Pattern> permissions = new LinkedList<Pattern>();
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader(fileToRead));
			String data = null;
			while ((data = reader.readLine()) != null) {
				//ignore comments and empty lines
				if(!data.isEmpty() && !data.startsWith("#")) {
					permissions.add(Pattern.compile(data));
				}
			}
		} catch(Exception e) {
			throw new RuntimeException("Cannot read permissions from file " + fileToRead.getAbsolutePath(), e);
		} finally {
			if(reader!=null)
				try {
					reader.close();
				} catch (IOException e) {}
		}
		return permissions;
	}

	@Override
	public String readFile(String fileName) throws IOException {
		File fileToRead = new File(rootDir, fileName);
		logger.info("Reading the file " + fileToRead.getCanonicalPath());
		
		checkPersmission(fileName, FileAccess.READ);
		
		BufferedReader reader = new BufferedReader(new FileReader(fileToRead));
		String data = null;
		StringBuilder bld = new StringBuilder();
		while ((data = reader.readLine()) != null) {
			if (bld.length() > 0)
				bld.append(CRLF);
			bld.append(data);
		}
		reader.close();
		return bld.toString();
	}

	@Override
	public void writeFile(String fileName, String data, WriteMode mode) throws IOException {
		BufferedWriter writer = createWriter(fileName, mode);
		writer.write(data);
		writer.close();
	}
	
	@Override
	public synchronized byte[] readFileBytes(String fileName, int offset) throws IOException {
		
		int bufferSize = BUFFER_SIZE;
		File fileToRead = new File(rootDir, fileName);
		logger.info("Reading the file bytes " + fileToRead.getCanonicalPath());
		checkPersmission(fileName, FileAccess.READ);
		long length = fileToRead.length();
		if (length > Integer.MAX_VALUE) {
			throw new IOException("File is too big");
		}
		
		
		if (offset < 0 || offset > length) {
			throw new IllegalArgumentException("Offset " + offset + " is incompatible for File " + fileName);
		}
		long remainToRead = length - offset;
		if (remainToRead <= 0) {
			return null; // everything was read
		} else if (remainToRead < bufferSize) {
			bufferSize = (int) remainToRead;
		}
		
		byte[] bytes = new byte[(int) bufferSize];
		
		InputStream istream = new FileInputStream(fileToRead);
		istream.read(bytes, offset, bufferSize);
		istream.close();
		return bytes;
		
	}
	
	@Override
	public byte[] readFileBytes(String fileName) throws IOException {
		File fileToRead = new File(rootDir, fileName);
		logger.info("Reading the file bytes " + fileToRead.getCanonicalPath());
		
		checkPersmission(fileName, FileAccess.READ);
		
		long length = fileToRead.length();

		if (length > Integer.MAX_VALUE) {
			throw new IOException("File is too big");
		}

		byte[] bytes = new byte[(int) length];

		int offset = 0;
		int numRead = 0;
		InputStream istream = new FileInputStream(fileToRead);
		
		while (offset < bytes.length && (numRead = istream.read(bytes, offset, bytes.length - offset)) >= 0) {
			offset += numRead;
		}
		istream.close();
		
		if (offset < bytes.length)
			throw new IOException("Could not completely read file ");
		
		return bytes;
	}

	@Override
	public void writeFileBytes(String fileName, byte[] data, WriteMode mode) throws IOException {
		OutputStream ostream = createOStream(fileName, mode);
		ostream.write(data);
		ostream.close();
	}

	@Override
	public void deleteFile(String fileName) throws IOException {
		File fileToDelete = new File(rootDir, fileName);
		
		checkPersmission(fileName, FileAccess.DELETE);
		
		boolean result = fileToDelete.delete();
		logger.info("Deleting the file " + fileToDelete.getCanonicalPath() + ". Result - " + result);
	}

	private static boolean checkDir(String rootDir) {
		File dir = new File(rootDir);
		return dir.exists() && dir.isDirectory();
	}

	private enum FileAccess {
	READ("read"), WRITE("write"), DELETE("delete"), READWRITE("read, write"), ALL("read,write,delete,execute");
	
		@Override
		public String toString() { 
			return accessMode;
		}
		
		private FileAccess(String accessMode) {
			this.accessMode = accessMode;
		}
	
		private String accessMode;
	}

	private void checkPersmission(String fileName, FileAccess operation) {
		String normFileName = fileName.replaceAll("\\\\", "/");
		for(Pattern perm : permissions) {
			if(logger.isDebugEnabled())
				logger.debug("Matching "+fileName+" against "+perm.toString());
			if(perm.matcher(normFileName).matches()) {
				if(logger.isDebugEnabled())
					logger.debug("Granted "+operation+" access to "+fileName);
				return;
			}
		}
		throw new AccessControlException("access denied \""+fileName+"\" \""+operation+"\"");
	}

	private BufferedWriter createWriter(String fileName, WriteMode mode) throws IOException {
		return new BufferedWriter(new OutputStreamWriter(createOStream(fileName, mode)));
	}

	private OutputStream createOStream(String fileName, WriteMode mode) throws IOException {
		File file = new File(rootDir, fileName);
		
		logger.info("Writting to the file " + file.getCanonicalPath() + " with mode " + mode);
		
		checkPersmission(fileName, FileAccess.WRITE);

		switch (mode) {
		case APPEND:
			if (!file.exists())
				throw new IOException("The file " + file.getCanonicalPath() + " does not exist");
			break;
		case REWRITE:
			file.delete();
			break;
		default:
			throw new IllegalStateException("Forgot to update the switch statement");
		}
		return new FileOutputStream(file, mode.equals(WriteMode.APPEND));
	}
}
