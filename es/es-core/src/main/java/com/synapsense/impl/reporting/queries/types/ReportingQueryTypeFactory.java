package com.synapsense.impl.reporting.queries.types;

import com.synapsense.impl.reporting.queries.TableProcessor.QueryClause;
import com.synapsense.service.reporting.Cell;
import com.synapsense.util.CollectionUtils;

import java.util.List;
import java.util.Map;

public class ReportingQueryTypeFactory {
    
	private ReportingQueryTypeFactory() {
    }

    private static final Map<ReportingQueryType, QueryTypeClauseBuilder> clauseBuilders = CollectionUtils.newMap();

    static {
		clauseBuilders.put(ReportingQueryType.SINGLE_PROPERTY_CELL_TABLE,
				new SinglePropertyClauseBuilder());

		clauseBuilders.put(ReportingQueryType.SAME_MULTIPLE_PROPERTY_CELL_TABLE,
				new SameMultiplePropertyClauseBuilder());
		clauseBuilders.put(ReportingQueryType.MULTIPLE_OBJECT_CELL_TABLE,
				new MultipleObjectClauseBuilder());
		clauseBuilders.put(ReportingQueryType.DIFFERENT_MULTIPLE_PROPERTY_CELL_TABLE,
				new DifferentMultiplePropertyClauseBuilder());
    }


    public static QueryTypeClauseBuilder getClauseBuilder(ReportingQueryType queryType) {
        QueryTypeClauseBuilder clauseBuilder = clauseBuilders.get(queryType);
        return clauseBuilder;
    }
    
    public static class EmptyQueryTypeClauseBuilder implements QueryTypeClauseBuilder {

        @Override
        public String build(QueryClause queryClause, List<Cell> valueIdCells) {
        	return "";
        }
    }

}
