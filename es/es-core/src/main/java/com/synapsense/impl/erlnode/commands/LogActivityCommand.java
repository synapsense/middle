
package com.synapsense.impl.erlnode.commands;

import java.util.Map;

import com.ericsson.otp.erlang.OtpErlangBinary;
import com.ericsson.otp.erlang.OtpErlangObject;
import com.ericsson.otp.erlang.OtpErlangTuple;
import com.synapsense.impl.erlnode.ExceptionTypes;
import com.synapsense.service.impl.activitylog.ActivityLogService;

import com.synapsense.impl.erlnode.utils.StringConverter;

/**
 * @author stikhon
 * 
 */
public class LogActivityCommand extends Command {

	public LogActivityCommand(Map<String, Object> state) {
		super(state);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see Command#do_command(com.ericsson.otp.erlang.OtpErlangTuple)
	 */
	@Override
	public OtpErlangObject do_command(OtpErlangTuple command) throws Exception {
		OtpErlangObject userNameTuple = command.elementAt(1);
		OtpErlangObject moduleTuple = command.elementAt(2);
		OtpErlangObject actionTuple = command.elementAt(3);
		OtpErlangObject descriptionTuple = command.elementAt(4);

		if (userNameTuple == null || moduleTuple == null || actionTuple == null
				|| descriptionTuple == null
				|| !(userNameTuple instanceof OtpErlangBinary)
				|| !(moduleTuple instanceof OtpErlangBinary)
				|| !(actionTuple instanceof OtpErlangBinary)
				|| !(descriptionTuple instanceof OtpErlangBinary)) {
			return makeException(ExceptionTypes.ILLEGAL_ARGUMENTS, null);
		}
		String userName = StringConverter.utf8BinaryToString(userNameTuple);
		String module = StringConverter.utf8BinaryToString(moduleTuple);
		String action = StringConverter.utf8BinaryToString(actionTuple);
		String description = StringConverter.utf8BinaryToString(descriptionTuple);

		((ActivityLogService) globalState.get("logsvc")).log(userName,
				module, action, description);
		return makeOkResult(null);

	}

}
