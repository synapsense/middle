package com.synapsense.impl.dlimport.exceptions;

public class DuplicateObjectException extends RuntimeException {
	private static final long serialVersionUID = 3159479489332826574L;

	public DuplicateObjectException(String _msg) {
		super(_msg);
	}
}
