
package com.synapsense.impl.erlnode.commands;

import java.util.Date;
import java.util.Map;

import com.ericsson.otp.erlang.OtpErlangBinary;
import com.ericsson.otp.erlang.OtpErlangLong;
import com.ericsson.otp.erlang.OtpErlangObject;
import com.ericsson.otp.erlang.OtpErlangTuple;
import com.synapsense.impl.erlnode.ExceptionTypes;
import com.synapsense.impl.erlnode.utils.ToConverter;
import com.synapsense.dto.Alert;
import com.synapsense.dto.TO;
import com.synapsense.exception.AlertServiceSystemException;
import com.synapsense.exception.IllegalInputParameterException;
import com.synapsense.service.AlertService;

import com.synapsense.impl.erlnode.utils.StringConverter;

/**
 * @author stikhon
 * 
 */
public class RaiseAlertCommand extends Command {

	public RaiseAlertCommand(Map<String, Object> state) {
		super(state);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see Command#do_command(com.ericsson.otp.erlang.OtpErlangTuple)
	 */
	@Override
	public OtpErlangObject do_command(OtpErlangTuple command) throws Exception {
		OtpErlangObject nameTuple = command.elementAt(1);
		OtpErlangObject alertTypeTuple = command.elementAt(2);
		OtpErlangObject alertTimeTuple = command.elementAt(3);
		OtpErlangObject messageTuple = command.elementAt(4);
		OtpErlangObject hostToTuple = command.elementAt(5);
		if (nameTuple == null || alertTypeTuple == null
				|| alertTimeTuple == null
				|| messageTuple == null
				|| hostToTuple == null
				|| !(nameTuple instanceof OtpErlangBinary)
				|| !(alertTypeTuple instanceof OtpErlangBinary)
				|| !(alertTimeTuple instanceof OtpErlangLong)
				|| !(messageTuple instanceof OtpErlangBinary)
				|| !(hostToTuple instanceof OtpErlangTuple)) {
			return makeException(ExceptionTypes.ILLEGAL_ARGUMENTS, null);
		}
		String name = StringConverter.utf8BinaryToString(nameTuple);
		String alertType = StringConverter.utf8BinaryToString(alertTypeTuple);
		Long alertTime = ((OtpErlangLong) alertTimeTuple).longValue();
		String message = StringConverter.utf8BinaryToString(messageTuple);
		TO<?> hostTO = ToConverter.parseTo((OtpErlangTuple) hostToTuple);
		try {
			((AlertService) globalState.get("alertsvc")).raiseAlert(new Alert(
					name, alertType, new Date(alertTime), message,
					hostTO));
			return makeOkResult(null);
		} catch (IllegalInputParameterException e) {
			return makeException(ExceptionTypes.ILLEGAL_ARGUMENTS, e
					.getMessage());
		}
		catch (AlertServiceSystemException e) {
			return makeException(ExceptionTypes.ALERT_SVC_SYSTEM, e
					.getMessage());
		}
	}

}
