package com.synapsense.impl.nsvc;

import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Singleton;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import com.synapsense.service.nsvc.EventProcessor;
import com.synapsense.service.nsvc.NotificationService;

@Singleton(name="NotificationService")
@Remote(NotificationService.class)
@ConcurrencyManagement(ConcurrencyManagementType.BEAN)
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class NotificationServiceFrontend implements NotificationService {

	private NotificationService impl;

	@EJB(beanName="LocalNotificationService")
	public void setImpl(NotificationService impl) {
		this.impl = impl;
	}

	@Override
	public void registerProcessor(EventProcessor processor, EventProcessor filter) {
		impl.registerProcessor(processor, filter);
	}

	@Override
	public void deregisterProcessor(EventProcessor processor) {
		impl.deregisterProcessor(processor);

	}

}
