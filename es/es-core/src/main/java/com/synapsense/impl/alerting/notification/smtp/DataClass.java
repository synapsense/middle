package com.synapsense.impl.alerting.notification.smtp;

import java.util.Map;

import com.synapsense.util.CollectionUtils;

/**
 * @author : shabanov
 */
public final class DataClass {
	public static DataClass TEMPERATURE = new DataClass(200, "dimension_temperature");
	public static DataClass HUMIDITY = new DataClass(201, "dimension_humidity");
	public static DataClass PRESSURE = new DataClass(202, "dimension_pressure");
	public static DataClass DEWPOINT = new DataClass(998, "dimension_dew_point");

	private static Map<Integer, DataClass> dataClasses;
	static {
		dataClasses = CollectionUtils.newMap();
		dataClasses.put(TEMPERATURE.getCode(), TEMPERATURE);
		dataClasses.put(HUMIDITY.getCode(), HUMIDITY);
		dataClasses.put(PRESSURE.getCode(), PRESSURE);
		dataClasses.put(DEWPOINT.getCode(), DEWPOINT);
	}

	private DataClass(int code, String name) {
		this.code = code;
		this.name = name;
	}

	public static DataClass decode(int code) {
		return dataClasses.get(code);
	}

	public int getCode() {
		return code;
	}

    public String getName() {
        return name;
    }

    public String getDisplayName() {
		return displayName == null ? name : displayName;
	}

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    @Override
	public String toString() {
		return name;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		DataClass dataClass = (DataClass) o;

		if (code != dataClass.code)
			return false;
		if (!name.equals(dataClass.name))
			return false;

		return true;
	}

	@Override
	public int hashCode() {
		return code;
	}

	private String name;
	private String displayName;
	private int code;
}
