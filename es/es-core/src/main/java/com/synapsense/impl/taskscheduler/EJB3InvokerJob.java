/**
 * 
 */
package com.synapsense.impl.taskscheduler;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;

import javax.naming.NamingException;

import org.quartz.*;

import com.synapsense.security.EnvInitialContext;
import com.synapsense.security.RunAsEsUser;
import com.synapsense.util.CollectionUtils;

/**
 * <p>
 * A <code>Job</code> that invokes a method on an EJB.
 * </p>
 * 
 * <p>
 * Expects the properties corresponding to the following keys to be in the
 * <code>JobDataMap</code> when it executes:
 * <ul>
 * <li><code>EJB_JNDI_NAME_KEY</code>- the JNDI name (location) of the EJB's
 * home interface.</li>
 * <li><code>EJB_METHOD_KEY</code>- the name of the method to invoke on the EJB.
 * </li>
 * <li><code>EJB_ARGS_KEY</code>- an Object[] of the args to pass to the method
 * (optional, if left out, there are no arguments).</li>
 * <li><code>EJB_ARG_TYPES_KEY</code>- an Class[] of the types of the args to
 * pass to the method (optional, if left out, the types will be derived by
 * calling getClass() on each of the arguments).</li>
 * </ul>
 * <br/>
 * The following keys can also be used at need:
 * <ul>
 * <li><code>INITIAL_CONTEXT_FACTORY</code> - the context factory used to build
 * the context.</li>
 * <li><code>PROVIDER_URL</code> - the name of the environment property for
 * specifying configuration information for the service provider to use.</li>
 * </ul>
 * </p>
 * 
 * @author aborisov
 */
public class EJB3InvokerJob implements Job {

	/*
	 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	 * 
	 * Constants.
	 * 
	 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	 */

	public static final String EJB_JNDI_NAME_KEY = "ejb";

	public static final String EJB_METHOD_KEY = "method";

	public static final String EJB_ARGS_KEY = "args";

	public static final String PRINCIPAL = "java.naming.security.principal";

	public static final String EJB_INTERFACE_CLASS = "interfaceClass";

	public EJB3InvokerJob() {
	}

	/*
	 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	 * 
	 * Interface.
	 * 
	 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	 */
	public void execute(JobExecutionContext context) throws JobExecutionException {
		JobDetail detail = context.getJobDetail();
		JobDataMap dataMap = detail.getJobDataMap();
		String ejb = dataMap.getString(EJB_JNDI_NAME_KEY);
		String method = dataMap.getString(EJB_METHOD_KEY);
		String interfaceClass = dataMap.getString(EJB_INTERFACE_CLASS);

        Map<Class<?>, Object> arguments = (Map<Class<?>, Object>) dataMap.get(EJB_ARGS_KEY);
		if (arguments == null) {
			arguments =  CollectionUtils.newMap();
		}

		if (ejb == null) {
			throw new JobExecutionException();
		}

		Class<?> beanClass;
		try {
			beanClass = Class.forName(interfaceClass);
		} catch (ClassNotFoundException e) {
			throw new JobExecutionException(e);
		}

		Method methodExecute = null;
		Class<?>[] argTypes = arguments.keySet().toArray(new Class[]{});
		// get all interfaces
		Class<?>[] interfaces = beanClass.getInterfaces();
		for (int i = 0; i < interfaces.length; i++) {
			try {
				// try to find the method and use the first interface if we do
				methodExecute = interfaces[i].getDeclaredMethod(method, argTypes);
				break;
			} catch (NoSuchMethodException nsme) {
				// do nothing
			}

		}

		if (methodExecute == null) {
			throw new JobExecutionException("BeanClass [" + beanClass.getName() + "] has no interface with method ["
			        + method + "] declared");
		}

		String principal = dataMap.getString(PRINCIPAL);

        try {
            EnvInitialContext jndiContext = new EnvInitialContext();
            Object value = jndiContext.lookup(ejb);
            
           final Method finalMethodExecute = methodExecute;
           final Object finalValue = value;
           final Object[] finalArguments = arguments.values().toArray();

       RunAsEsUser.execute(principal, new RunAsEsUser.Action<Void,JobExecutionException>() {
			@Override
			public Void run() throws JobExecutionException {
                    try {
                        finalMethodExecute.invoke(finalValue, finalArguments);
                    } catch (IllegalAccessException | InvocationTargetException e) {
                        throw new JobExecutionException(e);
                    }
                return null;
            }
        }
		);

        } catch (NamingException e) {
            throw new JobExecutionException(e);
        }
     }
}
