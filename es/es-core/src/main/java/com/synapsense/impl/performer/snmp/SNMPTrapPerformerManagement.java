package com.synapsense.impl.performer.snmp;

public interface SNMPTrapPerformerManagement {
	void create();

	void start();

	void stop();

	void destroy();
}
