package com.synapsense.impl.reporting.queries;

import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.log4j.Logger;

import com.synapsense.exception.IllegalInputParameterException;
import com.synapsense.impl.reporting.queries.TableProcessor.QueryClause;
import com.synapsense.impl.reporting.queries.types.ReportingQueryTypeFactory;
import com.synapsense.impl.reporting.tables.ReportingTable;
import com.synapsense.impl.reporting.utils.ReportingConstants.QUERY.SYMBOL;
import com.synapsense.impl.reporting.utils.ReportingConstants.QUERY.WORD;
import com.synapsense.service.reporting.Cell;
import com.synapsense.util.CollectionUtils;

public class SelectClauseBuilder implements ClauseBuilder {
	
	private static final Logger log = Logger.getLogger(SelectClauseBuilder.class);
	
	private static Map<String, SelectClauseProvier> selectClauseProvierMap = CollectionUtils.newMap();

	static {
		SubSelectClauseProvier subSelectClauseProvier = new SubSelectClauseProvier();
		selectClauseProvierMap.put("last", subSelectClauseProvier);
		SimpleSelectClauseProvier simpleSelectClauseProvier = new SimpleSelectClauseProvier();
		selectClauseProvierMap.put("avg", simpleSelectClauseProvier);
		selectClauseProvierMap.put("min", simpleSelectClauseProvier);
		selectClauseProvierMap.put("max", simpleSelectClauseProvier);
		selectClauseProvierMap.put("STDDEV_SAMP", simpleSelectClauseProvier);
	}
	
	private static interface SelectClauseProvier {
		String getClause(String formulaName, TableProcessor owner, int formulaIndex);
	}


	@Override
	public String build(TableProcessor owner) {

		if (owner == null) {
			throw new IllegalInputParameterException("TableProcessor shouldn't be null");
		}
		
		StringBuilder selectSB = new StringBuilder();
		selectSB.append(QueryClause.SELECT.getName())
			.append(SYMBOL.SPACE);
		
		selectSB.append(ReportingQueryTypeFactory.getClauseBuilder(
				owner.getQueryType()).build(QueryClause.SELECT,
				owner.getValueIdCells()));

		selectSB.append(" hv.value_id_fk, ");
		String timeColumnQuery = "hv.timestamp";
		selectSB.append(timeColumnQuery)
				.append(SYMBOL.COMMA)
				.append(SYMBOL.SPACE)
				.append(processValueIdGrouping(owner))
				.append(" FROM");
		
		selectSB.append(SYMBOL.SPACE)
				.append("historical_values hv")
				.append(SYMBOL.SPACE);
		if (log.isTraceEnabled()) {
			log.trace("GetEnvData SELECT clause: " + selectSB.toString());
		}
		return selectSB.toString();
		
	}
	
	
	/**
	 * Process formula value id grouping: Adds all unique separate valueId groups for each formula.
	 *
	 * @param owner the owner
	 * @return Value Id Grouping string
	 */
	private String processValueIdGrouping(TableProcessor owner) {

		Map<String, Set<Cell>> formulaCellsMap = owner.getFormulaCellsMap();
		if (formulaCellsMap.isEmpty()) {
			throw new IllegalInputParameterException("Formula Cells Map shouldn't be empty");
		}
		StringBuilder cellValueIdsSB = new StringBuilder();
		cellValueIdsSB.append(SYMBOL.SPACE);
		
		int formulaIndex = 0;
		for (Entry<String, Set<Cell>> entry : formulaCellsMap.entrySet() ) {
			cellValueIdsSB.append(selectClauseProvierMap.get(entry.getKey()).getClause(entry.getKey(), owner, formulaIndex++));
		}
		return cellValueIdsSB.substring(0, cellValueIdsSB.length() - 2); 
		
	}
	
	private static class SimpleSelectClauseProvier implements SelectClauseProvier {

		public String getClause(String formulaName, TableProcessor owner, int formulaIndex) {
			StringBuilder cellValueIdsSB = new StringBuilder();
			cellValueIdsSB.append(WORD.CASE)
			.append(SYMBOL.SPACE)
			.append(WORD.WHEN)
			.append(SYMBOL.SPACE)
			.append("hv.value_id_fk")
			.append(SYMBOL.SPACE)
			.append(WORD.IN)
			.append(SYMBOL.OPEN_BRACKET)
			.append(":formulaValueIds")
			.append(formulaIndex)
			.append(SYMBOL.CLOSE_BRACKET)
			.append(SYMBOL.SPACE)
			.append(WORD.THEN)
			.append(SYMBOL.SPACE)
			.append(formulaName)
			.append(SYMBOL.OPEN_BRACKET)
			.append("hv.valued")
			.append(SYMBOL.CLOSE_BRACKET)
			.append(SYMBOL.SPACE)
			.append(WORD.ELSE)
			.append(SYMBOL.SPACE)
			.append("0")
			.append(SYMBOL.SPACE)
			.append(WORD.END)
            .append(SYMBOL.SPACE)
            .append(WORD.AS)
            .append(SYMBOL.SPACE)
            .append(formulaName + "_column")
            .append(SYMBOL.SPACE)
			.append(SYMBOL.COMMA)
			.append(SYMBOL.SPACE);
			return cellValueIdsSB.toString();
		}
		
	}
	
	private static class SubSelectClauseProvier implements SelectClauseProvier {

		/**
		 * Process formula value id grouping: Adds all unique separate valueId groups for each formula.
		 * (select valued FROM historical_values 
		 * where DATE_FORMAT(timestamp, '%Y %m %d %H:%i:%s') between '2013 06 05 00:00:00' and '2013 06 05 22:00:00' 
		 * and value_id_fk in (1018) 
		 * order by timestamp desc limit 1) as lastValue
		 * @param formulaCellsMap the formula cells map
		 * @return Value Id Grouping string
		 */	
		public String getClause(String formulaName, TableProcessor owner, int formulaIndex) {
			StringBuilder subselectSB = new StringBuilder(); 
			subselectSB.append(SYMBOL.OPEN_BRACKET)
			.append(QueryClause.SELECT.getName())
			.append(SYMBOL.SPACE)
			.append("valued from historical_values")
			.append(SYMBOL.SPACE);
	
			//Adding where clause for time
			subselectSB.append(QueryClause.WHERE.getName() + " ");
			Set<ReportingTable> timeReportingTables = owner.getTimeReportingTables();
			int timeTablesCounter = 0;
			for (ReportingTable table : timeReportingTables){
				if (table.isEmpty()) {
					continue;
				}
				if (timeTablesCounter > 1){
					subselectSB.append(SYMBOL.SPACE)
						.append(WORD.AND);
				}
				subselectSB.append(SYMBOL.SPACE)
					.append(table.<Object>get());
				timeTablesCounter++;
			}
			
			
			subselectSB.append(SYMBOL.SPACE);

			if (timeTablesCounter > 0){
				subselectSB.append(SYMBOL.SPACE)
					.append(WORD.AND);
			} 
			
			//Adding where clause for time
			subselectSB.append(SYMBOL.SPACE)
			.append("value_id_fk")
			.append(SYMBOL.SPACE)
			.append(WORD.IN)
			.append(SYMBOL.OPEN_BRACKET)
			.append(":formulaValueIds")
			.append(formulaIndex)
			.append(SYMBOL.CLOSE_BRACKET)
			.append(SYMBOL.SPACE)
			.append("order by hv.timestamp desc limit 1) as lastValue")
			.append(SYMBOL.COMMA)
			.append(SYMBOL.SPACE);
			return subselectSB.toString(); 
		}
		
	}

}
