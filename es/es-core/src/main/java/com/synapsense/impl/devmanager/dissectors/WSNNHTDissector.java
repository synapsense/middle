package com.synapsense.impl.devmanager.dissectors;

import java.util.Date;

import com.synapsense.dal.generic.entities.NhtInfoDO;
import com.synapsense.dto.TO;
import com.synapsense.plugin.messages.GenericMessage;
import com.synapsense.plugin.messages.GenericMessage.GenericMessageTypes;
import com.synapsense.plugin.messages.WSNNHTBuilder.FieldConstants;
import com.synapsense.util.algo.Pair;
import com.synapsense.util.algo.UnaryFunctor;

public class WSNNHTDissector implements MessageDissector {

	private final UnaryFunctor<Pair<TO<?>, NhtInfoDO>> functor;

	public WSNNHTDissector(UnaryFunctor<Pair<TO<?>, NhtInfoDO>> functor) {
		this.functor = functor;
	}

	@Override
	public void process(TO<?> objID, GenericMessage msg) {
		if (msg == null)
			throw new IllegalArgumentException("msg cannot be null");
		if (!tasteMessage(msg))
			throw new IllegalArgumentException("Incompatible message type");
		try {
			NhtInfoDO data = new NhtInfoDO();

            data.setReportTimestamp(new Date((Long) msg.get(FieldConstants.REPORT_TIMESTAMP.name())));
            data.setNodeIdHex(Integer.toHexString((Character)msg.get(FieldConstants.NODE_ID.name())));
            data.setNeighIdHex(Integer.toHexString((Character)msg.get(FieldConstants.NID.name())));
			data.setNumNeigh((Integer) msg.get(FieldConstants.NEIGHBORS.name()));
			data.setRssi((String) msg.get(FieldConstants.RSSI.name()));
			data.setState((Integer) msg.get(FieldConstants.STATE.name()));
            data.setHop((Integer) msg.get(FieldConstants.HOP.name()));
            data.setHoptm((Integer) msg.get(FieldConstants.HOPTM.name()));
			functor.process(new Pair<TO<?>, NhtInfoDO>(objID, data));
		} catch (Exception e) {
			throw new IllegalArgumentException("Wrong data is stored in message", e);
		}
	}

	@Override
	public boolean tasteMessage(GenericMessage msg) {
		return msg.getType() == GenericMessageTypes.WSN_NHT;
	}

}
