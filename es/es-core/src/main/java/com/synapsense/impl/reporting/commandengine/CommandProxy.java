package com.synapsense.impl.reporting.commandengine;

import java.io.Serializable;

public interface CommandProxy extends Serializable {

	int getCommandId();

	String getCommandName();

	String getCommandDescription();
}
