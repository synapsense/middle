package com.synapsense.impl.unitsystems;

import java.io.File;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.DependsOn;
import javax.ejb.Remote;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.cdi.MBean;
import com.synapsense.service.UnitSystemsService;
import com.synapsense.util.unitconverter.UnitResolvers;
import com.synapsense.util.unitconverter.UnitSystems;

@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
@Startup @Singleton @Remote(UnitSystemsService.class)
@DependsOn("Registrar")
@ConcurrencyManagement(ConcurrencyManagementType.BEAN)
@MBean("com.synapsense:type=UnitSystemsService")
public class UnitSystemsServiceImpl implements UnitSystemsService, UnitSystemsServiceImplMBean {

	private static Log log = LogFactory.getLog(UnitSystemsServiceImpl.class);

	final private static String FILE_STORAGE = "com.synapsense.filestorage";
	final private static String UNIT_SYSTEMS_FILE_NAME = "com.synapsense.unitsystems.usfile";
	final private static String UNIT_RESOLVERS_FILE_NAME = "com.synapsense.unitsystems.urfile";

	private UnitSystems unitSystems;
	private UnitResolvers unitResolvers;

	@Override
	@PostConstruct
	public void create() throws Exception {
		start();
	}

	@Override
	public void start() throws Exception {
		String storageFileName = System.getProperty(FILE_STORAGE);

		if (storageFileName == null || storageFileName.isEmpty())
			throw new Exception("Property [" + FILE_STORAGE + "] is not set");

		String fileName = storageFileName + File.separator
		        + System.getProperty(UNIT_SYSTEMS_FILE_NAME, "unitsystems.xml");
		unitSystems = new UnitSystems();
		log.info("Loading unit systems definition from: " + fileName);
		unitSystems.loadFromXml(fileName);

		fileName = storageFileName + File.separator + System.getProperty(UNIT_RESOLVERS_FILE_NAME, "unitresolvers.xml");
		log.info("Loading unit resolvers from: " + fileName);
		unitResolvers = new UnitResolvers();
		unitResolvers.loadFromXml(fileName);

		log.info("UnitSystemsService started");
	}

	@Override
	@PreDestroy
	public void stop() throws Exception {
		log.info("UnitSystemsService stopped");
		destroy();
	}

	@Override
	public void destroy() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public UnitSystems getUnitSystems() {
		return unitSystems;
	}

	@Override
	public UnitResolvers getUnitReslovers() {
		return unitResolvers;
	}

}
