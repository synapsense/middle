package com.synapsense.impl.alerting.mapping;

import java.util.Date;
import java.util.List;

import com.synapsense.dto.Alert;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.EnvException;
import com.synapsense.impl.alerting.Utils;
import com.synapsense.service.Environment;
import com.synapsense.util.CollectionUtils;
import com.synapsense.util.CollectionUtils.Functor;

public class AlertToHistory implements Functor<List<ValueTO>, Alert> {

	private Environment env;

	public AlertToHistory(Environment env) {
		this.env = env;
	}

	@Override
	public List<ValueTO> apply(Alert alert) {
		List<ValueTO> values = CollectionUtils.newArrayList(11);
		values.add(new ValueTO("alertId", alert.getAlertId()));
		values.add(new ValueTO("name", alert.getName()));
		values.add(new ValueTO("typeName", alert.getAlertType()));

		TO<?> alertTypeRef = Utils.findAlertTypeByName(env, alert.getAlertType());
		if (alertTypeRef == null) {
			throw new MappingException("Alert type  :" + alert.getAlertType() + " does not exist");
		}
		String description;
		String priority;
		try {
			description = env.getPropertyValue(alertTypeRef, "description", String.class);
			TO<?> priorityTO = env.getPropertyValue(alertTypeRef, "priority", TO.class);
			priority = env.getPropertyValue(priorityTO, "name", String.class);
		} catch (EnvException e) {
			throw new MappingException("Unable to retrieve property of alert type :" + alert.getAlertType());
		}
		if (description != null && description.isEmpty()) {
			description = null;
		}
		values.add(new ValueTO("typeDescr", description));
		if (priority == null) {
			throw new MappingException("Alert type  :" + alert.getAlertType() + " does not have priority");
		}
		values.add(new ValueTO("priority", priority));
		values.add(new ValueTO("time", alert.getAlertTime()));
		values.add(new ValueTO("status", alert.getStatus().code()));
		values.add(new ValueTO("message", alert.getMessage()));
		values.add(new ValueTO("fullMessage", alert.getFullMessage()));
		TO<?> hostTO = alert.getHostTO();
		if (!TOFactory.EMPTY_TO.equals(hostTO)) {
			values.add(new ValueTO("hostTO", hostTO.getID()));
			values.add(new ValueTO("hostType", hostTO.getTypeName()));
		}
		values.add(new ValueTO("acknowledgement", alert.getAcknowledgement()));
		Date ackTime = alert.getAcknowledgementTime();
		values.add(new ValueTO("acknowledgementTime", ackTime == null ? null : ackTime.getTime()));
		values.add(new ValueTO("user", alert.getUser()));

		return values;
	}
}
