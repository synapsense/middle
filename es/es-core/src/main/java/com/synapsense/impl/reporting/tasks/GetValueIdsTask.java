package com.synapsense.impl.reporting.tasks;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.google.common.collect.ListMultimap;
import com.synapsense.dal.generic.reporting.ReportingQueryExecutor;
import com.synapsense.impl.reporting.tables.ValueIdAttributesTable;
import com.synapsense.service.reporting.Cell;
import com.synapsense.util.Pair;

/**
 * <code>GetValueIdsTask</code> is task for getting valueIds for Properties and Objects of Query
 * 
 * @author Grigory Ryabov
 */
public class GetValueIdsTask implements ReportingTask {

	private static final Logger log = Logger.getLogger(ReportingTask.class);
	
	private ValueIdsTaskData valueIdsTaskData;

	private ReportingQueryExecutor queryExecutor;
	
	public GetValueIdsTask(ValueIdsTaskData valueIdsTaskData, ReportingQueryExecutor reportingQueryExecutor) {
		this.valueIdsTaskData = valueIdsTaskData;
		this.queryExecutor = reportingQueryExecutor;
	}

	@Override
	public void execute() {
		
		String valueidsQuery = valueIdsTaskData.getValueIdsQuery();
		if (valueidsQuery == null || valueidsQuery.isEmpty()) {
			if(log.isDebugEnabled()){
				log.debug("Value Ids Query is empty");
			}
			return;
		}
		//Executing ValueIds Query using Query Executor
		if (log.isTraceEnabled()) {
			log.trace(valueidsQuery);
		}
		Map<Integer, List<Integer>> parameterMap = valueIdsTaskData.getValueIdsParameterMap();

		List<Object[]> valueIdsVsObjectIds = queryExecutor.executeHQLQuery(
				valueidsQuery, parameterMap);
		
		if (valueIdsVsObjectIds.isEmpty()) {
			return;
		}
		//GetValueIdsTask: placing Query results to cells

		placeQueryResultsToCells(valueIdsVsObjectIds, valueIdsTaskData.getCellMap(),
				valueIdsTaskData.getCells());
		
		placeQueryResults(valueIdsVsObjectIds, valueIdsTaskData.getValueIdAttributes(),
				valueIdsTaskData.getValueIdCells());
		return;
		
	}
	
	@Override
	public String getName(){
		
		return "GetValueIdsTask";
		
	}
	
	private void placeQueryResultsToCells(List<Object[]> valueIdsVsObjectIds,
			ListMultimap<Pair<String, Integer>, Integer> cellMap, List<Cell> cells) {

		for (Object[] row : valueIdsVsObjectIds){
			if (row.length == 0) {
				continue;
			}
			List<Integer> cellsList = cellMap.get(Pair.newPair((String)row[1], (Integer)row[2]));
			for (Integer cellIndex : cellsList){
				cells.get(cellIndex).addValueId((Integer) row[0]);
			}
		}
		
	}
	
	private void placeQueryResults(List<Object[]> valueIdsVsObjectIds,
			ValueIdAttributesTable attributes, List<Cell> valueIdCells) {

		for (Object[] row : valueIdsVsObjectIds){
			if (row.length == 0) {
				continue;
			}
			List<Integer> cellsList = attributes.getCellIndexes(Pair.newPair((String)row[1], (Integer)row[2]));
			for (Integer cellIndex : cellsList){
				valueIdCells.get(cellIndex).addValueId((Integer) row[0]);
			}
		}
		
	}

}