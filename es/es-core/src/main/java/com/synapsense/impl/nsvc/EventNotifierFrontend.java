package com.synapsense.impl.nsvc;

import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Singleton;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import com.synapsense.service.impl.nsvc.EventNotifier;
import com.synapsense.service.nsvc.Event;

@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
//@PoolClass(value = org.jboss.ejb3.StrictMaxPool.class, maxSize = 100, timeout = 10000)
@Singleton(name="EventNotifier")
@ConcurrencyManagement(ConcurrencyManagementType.BEAN)
@Remote(EventNotifier.class)
public class EventNotifierFrontend implements EventNotifier {

	private EventNotifier impl;

	@EJB(beanName="LocalNotificationService")
	public void setImpl(EventNotifier impl) {
		this.impl = impl;
	}

	@Override
	public void notify(Event e) {
		impl.notify(e);
	}

}
