package com.synapsense.impl.diagnostic;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Collection;
import java.util.StringTokenizer;

import com.synapsense.dto.TO;
import org.apache.log4j.Logger;

import com.synapsense.service.UserManagementService;

public class UserStatisticCSVGenerator {

	private static final Logger log = Logger.getLogger(UserStatisticCSVGenerator.class);

	public static void generateUserStatistic(UserManagementService userManagement, String destinationFileName) {
		try {
			FileWriter fileWriter = new FileWriter(destinationFileName);

			Collection<TO<?>> groups = userManagement.getAllGroups();
			for (TO<?> group : groups) {
				String statistics = ((String) userManagement.getProperty(group, userManagement.GROUP_STATISTICS)).trim();
				if (!statistics.equals("")) {
					fileWriter.write((String) userManagement.getProperty(group, userManagement.GROUP_NAME));
					fileWriter.write("\n");
					StringTokenizer st = new StringTokenizer(statistics, ",");
					while (st.hasMoreTokens()) {
						String[] array = st.nextToken().split("=");
						fileWriter.write(array[0].trim() + ",");
						fileWriter.write(array[1].trim());
						fileWriter.write("\n");
					}
				}

				fileWriter.write("\n");
			}

			fileWriter.flush();
			fileWriter.close();
		} catch (IOException e) {
			log.warn("Could not generate user statistic file", e);
		}
	}
}
