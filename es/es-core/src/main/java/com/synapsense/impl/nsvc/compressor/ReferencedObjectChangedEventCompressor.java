package com.synapsense.impl.nsvc.compressor;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import com.synapsense.dto.TO;
import com.synapsense.service.nsvc.Event;
import com.synapsense.service.nsvc.events.ReferencedObjectChangedEvent;
import com.synapsense.service.nsvc.events.ReferencedObjectChangedEventProcessor;
import com.synapsense.util.Pair;

/**
 * This compressor accumulates events and then combines those for the same
 * referencing object:property and the same referenced object into a single
 * event holding the resulting collection of changed referenced properties.
 * 
 * @author stepanov
 * 
 */
public class ReferencedObjectChangedEventCompressor implements EventCompressor, ReferencedObjectChangedEventProcessor {

	private Collection<ReferencedObjectChangedEvent> events;

	private long eventsReceived, eventsSent;

	public ReferencedObjectChangedEventCompressor() {
		events = new LinkedList<ReferencedObjectChangedEvent>();
	}

	@Override
	public synchronized ReferencedObjectChangedEvent process(ReferencedObjectChangedEvent e) {
		// we'll do compression in the requester's thread, just store event here
		if (e != null) {
			events.add(e);
			eventsReceived++;
		}
		return e;
	}

	@Override
	public String getName() {
		return ReferencedObjectChangedEventCompressor.class.getSimpleName();
	}

	@Override
	public Collection<Event> getCompressedEvents() {
		// grab the buffer
		Collection<ReferencedObjectChangedEvent> buff;
		synchronized (this) {
			buff = events;
			events = new LinkedList<ReferencedObjectChangedEvent>();
		}
		// 1. preallocate map large enough for holding all events as if they
		// cannot be compressed (the worst case scenario)
		// 2. map's key is referencingTO:referencingProperty:refencedTO
		// wrapped into a nested Pair in this exact order due to
		// performance considerations
		Map<Pair<Pair<TO<?>, String>, TO<?>>, Event> sortedEvents = new HashMap<Pair<Pair<TO<?>, String>, TO<?>>, Event>(
		        buff.size());
		// cycle through events compressing those with the same referencing
		// TO:string
		// into a single one
		for (ReferencedObjectChangedEvent event : buff) {
			Pair<Pair<TO<?>, String>, TO<?>> id = new Pair<Pair<TO<?>, String>, TO<?>>(new Pair<TO<?>, String>(
			        event.getReferencingObj(), event.getReferencingPropName()), event.getReferencedObj());
			Event e = sortedEvents.put(id, event);
			if (e != null) {
				// ok, there are some changes for this id combination already,
				// let's just add new changed property names
				ReferencedObjectChangedEvent oldEvent = (ReferencedObjectChangedEvent) e;
				Collection<String> properties = new LinkedList<String>(oldEvent.getReferencedPropNames());
				properties.addAll(event.getReferencedPropNames());
				sortedEvents.put(id,
				        new ReferencedObjectChangedEvent(event.getReferencingObj(), event.getReferencingPropName(),
				                event.getReferencedObj(), properties));
			}
		}
		eventsSent += sortedEvents.values().size();
		return sortedEvents.values();
	}

	@Override
	public long getNumOfEventsReceived() {
		return eventsReceived;
	}

	@Override
	public long getNumOfEventsSent() {
		return eventsSent;
	}

	@Override
	public synchronized void resetStatistics() {
		eventsReceived = events.size();
		eventsSent = 0;
	}

}
