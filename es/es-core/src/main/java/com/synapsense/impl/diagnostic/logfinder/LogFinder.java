package com.synapsense.impl.diagnostic.logfinder;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.synapsense.impl.diagnostic.MapsenseInfo;
import com.synapsense.impl.diagnostic.logfinder.utils.DirHelper;
import com.synapsense.impl.diagnostic.logfinder.utils.RegistryHelper;

/**
 * Utility class that is looking for log files in Windows registry
 */
public class LogFinder {
	private static final Logger LOG = Logger.getLogger(LogFinder.class);
	public static final String SERVICE_KEY = "HKLM\\SYSTEM\\CurrentControlSet\\Services\\";
	private static final Collection<String> services = new ArrayList<String>();
	private static final long sufficientLogSize = 10 * 1024 * 1024; // 10 Mb

	static {
		services.add("synapserver");
		services.add("synapdm");
		services.add("synapmbgw");
		services.add("synapli");
		services.add("synapsnmp");
		services.add("SynapBACnet");
	}

	private static final String value = "ImagePath";

	private LogFinder() {
	}

	/**
	 * Fills <code>paths</code> collection with paths of found log files
	 */
	public static Collection<String> getLogFiles() {
		Collection<String> paths = new ArrayList<String>();

		// Add services log files
		for (String svc : services) {
			String imagePath = RegistryHelper.readValue(SERVICE_KEY + svc, value, RegistryHelper.REGEXPANDSZ_TOKEN);
			if (imagePath != null) {
				String logsDir = getDir(imagePath);
				if (logsDir != null) {
					if ("synapserver".equals(svc) || "hpieappsvr".equals(svc)) {
						logsDir = logsDir.substring(0, logsDir.indexOf("bin")) + "standalone/log/";
					} else {
						logsDir = logsDir + "logs/";
					}
					addPaths(logsDir, paths);
				}
			} else {
				LOG.info("Image path for service '" + svc + "' was not found in registry");
			}
		}

		// Add MapSense log files
		String mapsenseLogsDir = MapsenseInfo.getMapsenseLogsDir();
		LOG.info("MapSense logs dir: " + mapsenseLogsDir);
		if (mapsenseLogsDir != null) {
			addPaths(mapsenseLogsDir + File.separator, paths);
		}

		return paths;
	}

	private static void addPaths(String logsDir, Collection<String> paths) {
		String[] logFiles = DirHelper.getFileListing(logsDir, "log");
		if (logFiles == null) {
			LOG.info("Directory '" + logsDir + "' doesn't contain *.log files. Return.");
			return;
		}

		List<File> primaryLogFiles = new ArrayList<File>(2);
		Map<String, File> secondaryLatestLogFiles = new HashMap<String, File>();

		for (int i = 0; i < logFiles.length; i++) {
			String fileName = logFiles[i];
			File file = new File(logsDir + fileName);
			if (file.isFile()) {
				if (fileName.endsWith(".log")) {
					primaryLogFiles.add(file);
				} else {
					String primaryFileName = fileName.substring(0, fileName.indexOf(".log") + 4);
					File latestSecFile = secondaryLatestLogFiles.get(primaryFileName);
					if (latestSecFile == null || file.lastModified() > latestSecFile.lastModified()) {
						latestSecFile = file;
					}
					secondaryLatestLogFiles.put(primaryFileName, latestSecFile);
				}
			}
		}

		for (File file : primaryLogFiles) {
			paths.add(file.getAbsolutePath().replace('\\', '/'));
			if (file.length() < sufficientLogSize) {
				File secondaryFile = secondaryLatestLogFiles.get(file.getName());
				if (secondaryFile != null) {
					paths.add(secondaryFile.getAbsolutePath().replace('\\', '/'));
				}
			}
		}
	}

	private static String getDir(String imagePath) {
		imagePath = imagePath.replace('"', ' ').trim();
		int pos = imagePath.lastIndexOf("\\");
		if (pos != -1) {
			imagePath = imagePath.replace('\\', '/');
			return imagePath.substring(0, pos + 1);
		}
		return null;
	}
}
