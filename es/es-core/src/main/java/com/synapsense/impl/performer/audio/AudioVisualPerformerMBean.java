package com.synapsense.impl.performer.audio;

public interface AudioVisualPerformerMBean {
	
	void create();

	void start();

	void stop();

	void destroy();
}
