package com.synapsense.impl.performer;

import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.Remote;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.management.ListenerNotFoundException;
import javax.management.MBeanNotificationInfo;
import javax.management.Notification;
import javax.management.NotificationBroadcaster;
import javax.management.NotificationBroadcasterSupport;
import javax.management.NotificationFilter;
import javax.management.NotificationListener;
import javax.management.ObjectName;

import com.synapsense.service.impl.notification.NotificationException;
import com.synapsense.service.impl.notification.Transport;

import org.apache.log4j.Logger;

import com.synapsense.dto.Message;

@Startup
@Singleton @Remote(Transport.class)
@ConcurrencyManagement(ConcurrencyManagementType.BEAN)
public class JMXNotificationSender implements Transport, NotificationBroadcaster {
	private final static Logger logger = Logger.getLogger(JMXNotificationSender.class);

	private final static String PERF_NAME = "JMXNotificationSender";
	private final static String JMX_NOTIFICATION_TYPE = "synapsense.performer.alert2jmx";
	private final static String SOURCE = "jboss.j2ee:ear=SynapServer.ear,jar=SynapEnvCore.jar,name=JMX Notificator,service=EJB3";
	private final static long SEQUENCE_ROLLOVER = 100000;

	private long sequenceNumber = 0;

	private NotificationBroadcasterSupport broadcaster = new NotificationBroadcasterSupport();

	@Override
	public String getName() {
		return PERF_NAME;
	}

    @Override
    public boolean isValid(Message message) {
        return !checkEvent(message);
    }

    @Override
	public final void send(Message metadata) throws NotificationException {
		logger.info("Processing the alert : " + metadata);

		if (!isValid(metadata)) {
			logger.warn("Could not process the event " + metadata);
			return;
		}
		broadcaster.sendNotification(convert(metadata));
	}

	/**
	 * Converts the event to the JMX Notification
	 *
	 * @param event
	 * @return
	 */
	private Notification convert(Message event) {
		sequenceNumber = (++sequenceNumber) % SEQUENCE_ROLLOVER;
		try {
			return new Notification(JMX_NOTIFICATION_TYPE, new ObjectName(SOURCE), sequenceNumber, event.getContent());
		} catch (Exception e) {
			logger.warn("Cannot create JMX Notification due to: ", e);
			return null;
		}
	}

	/**
	 * Checks the event for consistency
	 *
	 * @param event
	 *            - data to check
	 * @return
	 */
	private boolean checkEvent(Message event) {
		if (event == null)
			throw new IllegalArgumentException("event cannot be null");
		return true;
	}

	@Override
	public void addNotificationListener(NotificationListener arg0, NotificationFilter arg1, Object arg2)
	        throws IllegalArgumentException {
		logger.info("requested to add a notification listener");
		broadcaster.addNotificationListener(arg0, arg1, arg2);
	}

	@Override
	public MBeanNotificationInfo[] getNotificationInfo() {
		logger.info("Requested notification info");
		return new MBeanNotificationInfo[] { new MBeanNotificationInfo(new String[] { JMX_NOTIFICATION_TYPE },
		        Notification.class.getName(), "SynapSense Alert converted to JMX Notification.") };
	}

	@Override
	public void removeNotificationListener(NotificationListener arg0) throws ListenerNotFoundException {
		logger.info("requested to remove a botification listener");
		broadcaster.removeNotificationListener(arg0);
	}
}
