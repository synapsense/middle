package com.synapsense.impl.alerting.notification;

import java.util.LinkedList;
import java.util.List;

import com.synapsense.dto.MessageType;
import com.synapsense.service.Environment;
import com.synapsense.service.UserManagementService;

public class DestinationProviderFactory {
	public static DestinationProvider getDestinationProvider(final Environment env,
	        final UserManagementService userService, final String alertTypeName, final String tierName,
	        final MessageType messageType) {
		if (MessageType.NT_LOG.equals(messageType)) {
			return new DestinationProvider() {

				@Override
				public List<Destination> getDestinations() {
					List<Destination> destinations = new LinkedList<Destination>();
					destinations.add(new PlainDestination(MessageType.NT_LOG.toString()));
					return destinations;
				}
			};
		} else {
			return new EnvDestinationProvider(env, userService, alertTypeName, tierName, messageType);
		}

	}

}
