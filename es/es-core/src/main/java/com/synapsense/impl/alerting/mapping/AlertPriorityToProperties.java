package com.synapsense.impl.alerting.mapping;

import java.util.List;

import com.synapsense.dto.AlertPriority;
import com.synapsense.dto.PriorityTier;
import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;
import com.synapsense.impl.alerting.Utils;
import com.synapsense.service.Environment;
import com.synapsense.util.CollectionUtils;
import com.synapsense.util.CollectionUtils.Functor;

public class AlertPriorityToProperties implements Functor<List<ValueTO>, AlertPriority> {

	private Environment env;

	public AlertPriorityToProperties(Environment env) {
		super();
		this.env = env;
	}

	@Override
	public List<ValueTO> apply(AlertPriority priority) {
		List<ValueTO> values = CollectionUtils.newArrayList(3);
		values.add(new ValueTO("name", priority.getName()));
		values.add(new ValueTO("dismissInterval", priority.getDismissInterval()));
		List<TO<?>> tiers = CollectionUtils.newList();
		for (PriorityTier tier : priority.getTiers()) {
			TO<?> tierRef = Utils.findAlertPriorityTier(env, priority.getName(), tier.getName());
			if (tierRef != null) {
				tiers.add(tierRef);
			} else {
				throw new MappingException("Failed to resolve reference to tier : " + tier.getName()
				        + " on priority : " + priority.getName());
			}
		}
		values.add(new ValueTO("tiers", tiers));
		values.add(new ValueTO("redSignal", priority.isRedSignal() ? 1 : 0));
		values.add(new ValueTO("greenSignal", priority.isGreenSignal() ? 1 : 0));
		values.add(new ValueTO("yellowSignal", priority.isYellowSignal() ? 1 : 0));
		values.add(new ValueTO("audioSignal", priority.isAudioSignal() ? 1 : 0));
		values.add(new ValueTO("audioDuration", priority.getAudioDuration()));
		values.add(new ValueTO("lightDuration", priority.getLightDuration()));
		values.add(new ValueTO("ntLog", priority.isNtLog() ? 1 : 0));
		return values;
	}
}
