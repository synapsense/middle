package com.synapsense.impl.nsvc.compressor;

import java.util.Collection;

import com.synapsense.service.nsvc.Event;

/**
 * Interface to be implemented by any event compressor in addition to the
 * particular [Type]EventProcessor interface.<br>
 * That is, events are supposed to be fed into compressor via regular
 * EventProcessor.process() method<br>
 * 
 * Implementation notes: this interface does not impose any timer logic on the
 * implementing class for the sake of testing simplicity. However,
 * implementation must be thread-safe since its getCompressedEvents() and
 * EventProcessor.process() methods will be likely invoked in different threads.
 * 
 * @author Oleg Stepanov
 * 
 */
public interface EventCompressor {

	String getName();

	long getNumOfEventsReceived();

	long getNumOfEventsSent();

	void resetStatistics();

	Collection<Event> getCompressedEvents();

}
