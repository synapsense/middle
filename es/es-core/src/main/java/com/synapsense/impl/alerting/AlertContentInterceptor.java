package com.synapsense.impl.alerting;

import com.synapsense.dal.objectcache.CacheEnvironmentDispatcher;
import com.synapsense.dal.objectcache.CacheSvc;
import com.synapsense.dto.Alert;
import com.synapsense.dto.AlertType;
import com.synapsense.dto.MessageType;
import com.synapsense.impl.alerting.macros.*;
import com.synapsense.service.*;
import com.synapsense.service.user.CachedUserContext;
import com.synapsense.service.user.EnvUserContext;
import com.synapsense.service.user.UserContext;
import com.synapsense.util.CollectionUtils;
import com.synapsense.util.ConvertingEnvironmentProxy;
import com.synapsense.util.unitconverter.UnitResolvers;
import com.synapsense.util.unitconverter.UnitSystems;
import org.apache.log4j.Logger;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.SessionContext;
import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.Set;

public class AlertContentInterceptor {

	// [BC 07/2015] Keep track of the number of bean setters so we can invoke postConstruct() at the end
	private int injectedBeansCount;
	private static final int INJECTED_BEANS_TOTAL = 7;

	private final static Logger logger = Logger.getLogger(AlertContentInterceptor.class);

	@AroundInvoke
	public Object intercept(final InvocationContext icx) throws Exception {
		if (logger.isDebugEnabled()) {
			logger.debug("Entering interceptor : " + AlertContentInterceptor.class.getName());
		}

		final Object result = icx.proceed();
		if (result == null)
			return result;

		// take returning collection
		final Collection<Alert> alerts;
		boolean isCollection;
		if (result instanceof Collection) {
			alerts = (Collection<Alert>) result;
			isCollection = true;
		} else {
			alerts = new ArrayList<Alert>();
			alerts.add((Alert) result);
			isCollection = false;
		}
		if (alerts.isEmpty()) {
			return result;
		}

		// find out that caller is not anonymous
		final Principal user = ctx.getCallerPrincipal();
		final String callerName = user.getName();
		final UserContext userCtx = new CachedUserContext(new EnvUserContext(callerName, userService));
		final String targetSystem = userCtx.getUnitSystem();
		// using caller's preferred unit system
		convertingEnv.setTargetSystem(targetSystem);

		final Set<String> typeNames = CollectionUtils.newSet();
		for (final Alert alert : alerts) {
			typeNames.add(alert.getAlertType());
		}

		final Map<String, AlertType> aTypes = CollectionUtils.newMap();

		final Collection<AlertType> types = alertService.getAlertTypes(typeNames.toArray(new String[] {}));

		for (final AlertType t : types) {
			aTypes.put(t.getName(), t);
		}

		macroCtx.usrCtx = userCtx;

		// replace full message

		for (final Alert alert : alerts) {
			final String body;
			final String description;

			final AlertType at = aTypes.get(alert.getAlertType());
			if (at != null) {
				// alert type exists in system. get info from alert type
				com.synapsense.dto.MessageTemplate template = at.getMessageTemplate(MessageType.CONSOLE);
				if (template == null) {
					template = AlertConstants.MESSAGE.CONSOLE_SYSTEM_ALERT_TEMPLATE;
					if (logger.isDebugEnabled()) {
						logger.debug("Alert type has no template specified for " + MessageType.CONSOLE.name()
						        + " message type. Default template will be used");
					}
				}

				String s;
				body = ((s = template.getBody(env)).isEmpty()) ? s : localizationService.getTranslation(s,
				        userCtx.getLocale());
				if (logger.isDebugEnabled()) {
					logger.debug("Using body = {" + body + "} from template " + template.getRef());
				}

				description = at.getDescription();
			} else {
				// alert type deleted. use default message
				body = localizationService.getString("alert_message_template_body_console", userCtx.getLocale());
				description = alert.getTypeDescr();
			}
			macroCtx.alert = alert;
			macroCtx.props.put(AlertDescrMacro.ALERT_DESCRIPTION, description);

			alert.setFullMessage(macro.expandIn(body, macroCtx));
			final String comments = alert.getAcknowledgement();
			alert.setAcknowledgement(localizeMacro.expandIn(comments, macroCtx));
		}

		if (logger.isDebugEnabled()) {
			logger.debug("Exitting interceptor : " + AlertContentInterceptor.class.getName());
		}

		return isCollection ? alerts : alerts.iterator().next();
	}

	void postConstruct() {
		logger.info("interceptor init ... ");
		final Environment cachedEnv = CacheEnvironmentDispatcher.getEnvironment(cache, genericEnv);

		unitSystems = unitSystemService.getUnitSystems();
		final UnitResolvers unitRes = unitSystemService.getUnitReslovers();

		convertingEnv = new ConvertingEnvironmentProxy();

		convertingEnv.setUnitSystems(unitSystems);
		convertingEnv.setUnitResolvers(unitRes);
		convertingEnv.setProxiedEnvironment(cachedEnv);

		env = convertingEnv.getProxy();

        // TODO : image macro is useless here
		macro = AlertMacroFactory.getFormattingMacros();
		localizeMacro = new LocalizeMacro();
		macroCtx = new MacroContext();
		macroCtx.env = cachedEnv;
		macroCtx.convEnv = convertingEnv;
		macroCtx.unitSystems = unitSystems;
		macroCtx.localizationService = localizationService;
		macroCtx.props.put(MessageTierMacro.MSG_TIER, null);
		macroCtx.props.put(MessageNumMacro.MSG_NUM, null);
		macroCtx.props.put(AlertDescrMacro.ALERT_DESCRIPTION, null);
		logger.info("interceptor init is done");
	}

	@Resource
	void setCtx(final SessionContext ctx) {
		this.ctx = ctx;
		if (++injectedBeansCount == INJECTED_BEANS_TOTAL) postConstruct();
	}

	@EJB
	void setCache(final CacheSvc cache) {
		this.cache = cache;
		if (++injectedBeansCount == INJECTED_BEANS_TOTAL) postConstruct();
	}

	@EJB
	void setUnitSystemService(final UnitSystemsService unitSystemService) {
		this.unitSystemService = unitSystemService;
		if (++injectedBeansCount == INJECTED_BEANS_TOTAL) postConstruct();
	}

	@EJB(beanName = "LocalUserManagementService")
	void setUserService(final UserManagementService userService) {
		this.userService = userService;
		if (++injectedBeansCount == INJECTED_BEANS_TOTAL) postConstruct();
	}

	@EJB(beanName = "GenericEnvironment")
	void setGenericEnv(final Environment genericEnv) {
		this.genericEnv = genericEnv;
		if (++injectedBeansCount == INJECTED_BEANS_TOTAL) postConstruct();
	}

	@EJB
	void setAlertService(final AlertService alertService) {
		this.alertService = alertService;
		if (++injectedBeansCount == INJECTED_BEANS_TOTAL) postConstruct();
	}

	@EJB
	void setLocalizationService(final LocalizationService localizationService) {
		this.localizationService = localizationService;
		if (++injectedBeansCount == INJECTED_BEANS_TOTAL) postConstruct();
	}

	void setMacro(Macro macro) {
		this.macro = macro;
	}

	private SessionContext ctx;

	private ConvertingEnvironmentProxy convertingEnv;

	private LocalizationService localizationService;

	private CacheSvc cache;

	private UnitSystemsService unitSystemService;

	private UserManagementService userService;

	private Environment genericEnv;

	private AlertService alertService;

	private Environment env;

	private UnitSystems unitSystems;

	private Macro macro;

	private Macro localizeMacro;

	private MacroContext macroCtx;

}
