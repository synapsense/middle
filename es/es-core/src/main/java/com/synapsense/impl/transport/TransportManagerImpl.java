package com.synapsense.impl.transport;

import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.DependsOn;
import javax.ejb.Singleton;
import javax.ejb.Startup;

import org.apache.log4j.Logger;

import com.synapsense.cdi.MBean;
import com.synapsense.transport.TransportManager;

/**
 * Session Bean implementation class TransportManagerBean
 */
@Startup @Singleton
@DependsOn("LocalNotificationService")
@ConcurrencyManagement(ConcurrencyManagementType.BEAN)
@MBean("com.synapsense:type=TransportManager")
public class TransportManagerImpl implements TransportManagerImplMBean {

	private static final Logger log = Logger.getLogger(TransportManagerImpl.class);

	@Override
	@PostConstruct
	public void start() throws Exception {
		log.info("Starting");
		TransportManager.startServerTransports(new Properties());
		log.info("Started");
	}

	@Override
	@PreDestroy
	public void stop() throws Exception {
		log.info("Stopping");
		TransportManager.stopServerTransports();
		log.info("Stopped");
	}

	@Override
	public String listServerTransports() {
		StringBuilder sb = new StringBuilder();
		for(String transport : TransportManager.getServerTransports().keySet()) {
			sb.append(transport).append("\r\n");
		}
		return sb.toString();
	}
}
