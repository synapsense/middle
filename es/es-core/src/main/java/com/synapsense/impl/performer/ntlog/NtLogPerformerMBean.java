package com.synapsense.impl.performer.ntlog;

public interface NtLogPerformerMBean {
	
	void addRecord(String level, String record);
	
}
