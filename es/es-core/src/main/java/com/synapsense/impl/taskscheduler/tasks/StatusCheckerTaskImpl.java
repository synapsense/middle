package com.synapsense.impl.taskscheduler.tasks;

import java.util.LinkedList;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import org.apache.log4j.Logger;

import com.synapsense.dal.objectcache.CacheEnvironmentDispatcher;
import com.synapsense.dal.objectcache.CacheSvcImpl;
import com.synapsense.service.Environment;

@Stateless
@Remote(StatusCheckerTask.class)
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class StatusCheckerTaskImpl implements StatusCheckerTask {
	private final static Logger logger = Logger.getLogger(StatusCheckerTaskImpl.class);

	@EJB(beanName="LocalEnvironment")
	public void setEnvironment(Environment env) {
		this.env = CacheEnvironmentDispatcher.getEnvironment(CacheSvcImpl.getInstance(), env);
	}

	private Environment env;

	private LinkedList<StatusChecker> listOfCheckers = new LinkedList<StatusChecker>();

	public StatusCheckerTaskImpl() {
		listOfCheckers.add(new SensorNodeStatusChecker());
		listOfCheckers.add(new LinkedSensorsStatusChecker("RACK"));
		listOfCheckers.add(new LinkedSensorsStatusChecker("CRAC"));
		listOfCheckers.add(new LinkedSensorsStatusChecker("PRESSURE"));
		listOfCheckers.add(new LinkedSensorsStatusChecker("ION_WIRELESS"));
		listOfCheckers.add(new LinkedSensorsStatusChecker("GENERICDEVICE"));
		listOfCheckers.add(new LinkedSensorsStatusChecker("DOOR"));
		listOfCheckers.add(new LinkedSensorsStatusChecker("GENERICTEMPERATURE"));
		listOfCheckers.add(new LinkedSensorsStatusChecker("LEAK"));
		listOfCheckers.add(new LinkedSensorsStatusChecker("EQUIPMENTSTATUS"));
		listOfCheckers.add(new LinkedSensorsStatusChecker("VERTICALTEMPERATURE"));
		listOfCheckers.add(new LinkedSensorsStatusChecker("CONTROLPOINT_SINGLECOMPARE"));
		listOfCheckers.add(new LinkedSensorsStatusChecker("CONTROLPOINT_STRAT"));
		listOfCheckers.add(new ChildrenStatusChecker("ROOM"));
		listOfCheckers.add(new ChildrenStatusChecker("WSNNETWORK"));
		listOfCheckers.add(new ChildrenStatusChecker("DC"));
	}

	@Override
	public void execute() {

		logger.debug("<< Start setting statuses of objects...");
		for (int i = 0; i < listOfCheckers.size(); i++) {
			listOfCheckers.get(i).check(env);
		}
		logger.debug("... Stop setting statuses of objects >>");
	}

	public void addChecker(StatusChecker sc) {
		listOfCheckers.add(sc);
	}
}
