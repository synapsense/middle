package com.synapsense.impl.devmanager.dissectors;

import org.apache.log4j.Logger;

import com.synapsense.dto.TO;
import com.synapsense.plugin.messages.GenericMessage;
import com.synapsense.plugin.messages.GenericMessage.GenericMessageTypes;
import com.synapsense.plugin.messages.PowerDataBuilder.FieldConstants;
import com.synapsense.service.impl.messages.base.PlugData;
import com.synapsense.util.algo.Pair;
import com.synapsense.util.algo.UnaryFunctor;

public class PlugDataDissector implements MessageDissector {

	private static final Logger log = Logger.getLogger(PlugDataDissector.class);

	private UnaryFunctor<Pair<TO<?>, PlugData>> functor;

	public PlugDataDissector(UnaryFunctor<Pair<TO<?>, PlugData>> functor) {
		this.functor = functor;
	}

	@Override
	public boolean tasteMessage(GenericMessage msg) {
		return msg.getType() == GenericMessageTypes.JAWA_DATA;
	}

	@Override
	public void process(TO<?> objID, GenericMessage msg) {
		if (msg == null)
			throw new IllegalArgumentException("msg cannot be null");
		if (!tasteMessage(msg))
			throw new IllegalArgumentException("Incompatible message type");

		PlugData plugData = new PlugData((Long) msg.get(FieldConstants.JAWA_ID.name()),
		        (Double) msg.get(FieldConstants.AVG_CURRENT.name()),
		        (Double) msg.get(FieldConstants.MAX_CURRENT.name()),
		        (Double) msg.get(FieldConstants.DEMANDPOWER.name()), (Long) msg.get(FieldConstants.TIMESTAMP.name()));

		if (log.isDebugEnabled()) {
			log.debug("Received Jawa data packet. " + plugData);
		}

		functor.process(new Pair<TO<?>, PlugData>(objID, plugData));
	}
}
