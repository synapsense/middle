package com.synapsense.impl.erlnode.commands;

import java.util.Collection;
import java.util.Map;

import com.ericsson.otp.erlang.OtpErlangList;
import com.ericsson.otp.erlang.OtpErlangObject;
import com.ericsson.otp.erlang.OtpErlangTuple;
import com.synapsense.impl.erlnode.ExceptionTypes;
import com.synapsense.impl.erlnode.utils.ToConverter;
import com.synapsense.impl.erlnode.utils.ValueTOConverter;
import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.IllegalInputParameterException;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.exception.PropertyNotFoundException;
import com.synapsense.exception.UnableToConvertPropertyException;

/**
 * %% Object = to() %% Values = list(value_to())
 * 
 */
public class SetAllPropertiesValuesCommand extends Command {

	public SetAllPropertiesValuesCommand(Map<String, Object> state) {
		super(state);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see Command#do_command(com.ericsson.otp.erlang.OtpErlangTuple)
	 */
	@Override
	public OtpErlangObject do_command(OtpErlangTuple command) throws Exception {
		OtpErlangObject objectsTuple = command.elementAt(1);
		OtpErlangObject valuesTuple = command.elementAt(2);
		if (objectsTuple == null || valuesTuple == null || !(objectsTuple instanceof OtpErlangTuple)
		        || !(valuesTuple instanceof OtpErlangList)) {
			//An empty list represented by "" will cause an exception
			return makeException(ExceptionTypes.ILLEGAL_ARGUMENTS, null);
		}

		TO<?> objId = ToConverter.parseTo((OtpErlangTuple) command.elementAt(1));
		Collection<ValueTO> values = ValueTOConverter.parseValueTOList(getEnv(), objId.getTypeName(),
		        (OtpErlangList) valuesTuple);
		try {
			getEnv().setAllPropertiesValues(objId, values.toArray(new ValueTO[values.size()]));
		} catch (IllegalInputParameterException e) {
			return makeException(ExceptionTypes.ILLEGAL_ARGUMENTS, e.getMessage());
		} catch (ObjectNotFoundException e) {
			return makeException(ExceptionTypes.OBJECT_NOT_FOUND, e.getMessage());
		} catch (PropertyNotFoundException e) {
			return makeException(ExceptionTypes.PROPERTY_NOT_FOUND, e.getMessage());
		} catch (UnableToConvertPropertyException e) {
			return makeException(ExceptionTypes.UNABLE_TO_CONVERT_PROPERTY, e.getMessage());
		}

		return makeOkResult(null);
	}

}
