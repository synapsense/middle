package com.synapsense.impl.alerting.notification.smtp;

import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.EnvException;
import com.synapsense.service.Environment;
import org.apache.commons.mail.ByteArrayDataSource;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;
import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.util.Collection;

import static com.synapsense.impl.alerting.AlertConstants.NOTIFICATIONS.IMAGE_SERVER_CONNECTION_TIMEOUT;

/**
 * @author : shabanov
 */
public abstract class AbstractImageDataSource implements ImageDataSource {
	private final static Logger logger = Logger.getLogger(AbstractImageDataSource.class);

	protected Environment env;

	protected AbstractImageDataSource(Environment environment) {
		this.env = environment;
	}

	@Override
	public byte[] getImage() {
		URL imageUrl = getURL();
		if (imageUrl == null) {
			return null;
		}
		try (InputStream imageStream = loadImageFrom(imageUrl)) {
			return streamToByteArray(imageStream);
		} catch (IOException e) {
			logger.warn("Failed to get image", e);
			return null;
		}
	}

	public URL getURL() {
		try {
			TO liConfigTo = getLIConfigTO();
			if (liConfigTo == null) {
				logger.warn("Live imaging service is not configured yet");
				return null;
			}

			String liConfig = getConfig(liConfigTo);
			if (liConfig == null) {
				logger.warn("Live imaging service is not configured yet");
				return null;
			}

			String liServerUrl = getServerPath(liConfig);
			if (liServerUrl == null) {
				logger.warn("Failed to obtain LiveImaging service url");
				return null;
			}

			return new URL(liServerUrl + getFile());
		} catch (Exception e) {
			logger.warn("Failed to obtain url of LiveImaging image", e);
			return null;
		}
	}

	protected abstract String getFile();

	private String getConfig(TO to) throws EnvException {
		return env.getPropertyValue(to, "config", String.class);
	}

	private TO getLIConfigTO() {
		ValueTO[] vals = { new ValueTO("name", "LiveImaging") };
		Collection<TO<?>> ctos = env.getObjects("CONFIGURATION_DATA", vals);
		if (ctos.isEmpty())
			return null;

		return ctos.iterator().next();
	}

	private String getServerPath(String config) throws Exception {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document doc = builder.parse(new ByteArrayInputStream(config.getBytes()));
		XPathFactory xPathfactory = XPathFactory.newInstance();
		XPath xpath = xPathfactory.newXPath();
		XPathExpression exprIP = xpath.compile("//webserver-ip/text()");
		XPathExpression exprPort = xpath.compile("//webserver-port/text()");
		String host = (String) exprIP.evaluate(doc, XPathConstants.STRING);
		String port = (String) exprPort.evaluate(doc, XPathConstants.STRING);
		return "http://" + host + ":" + port + "/";
	}

	private InputStream loadImageFrom(URL url) throws IOException {
		URLConnection conn = url.openConnection();
		conn.setConnectTimeout(IMAGE_SERVER_CONNECTION_TIMEOUT);
		conn.connect();
		return conn.getInputStream();
	}

	private byte[] streamToByteArray(InputStream aIs) throws IOException {
		BufferedInputStream bis = null;
		BufferedOutputStream osWriter = null;
		ByteArrayOutputStream baos = null;
		try {
			int length = 0;
			byte[] buffer = new byte[ByteArrayDataSource.BUFFER_SIZE];

			bis = new BufferedInputStream(aIs);
			baos = new ByteArrayOutputStream();
			osWriter = new BufferedOutputStream(baos);

			// Write the InputData to OutputStream
			while ((length = bis.read(buffer)) != -1) {
				osWriter.write(buffer, 0, length);
			}
			osWriter.flush();
			osWriter.close();

			return baos.toByteArray();
		} finally {
			if (bis != null) {
				bis.close();
			}
			if (baos != null) {
				baos.close();
			}
			if (osWriter != null) {
				osWriter.close();
			}
		}
	}
}
