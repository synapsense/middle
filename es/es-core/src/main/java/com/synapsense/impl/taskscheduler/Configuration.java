package com.synapsense.impl.taskscheduler;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.JAXBException;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;

import org.apache.log4j.Logger;
import org.xml.sax.SAXException;

import com.synapsense.impl.taskscheduler.config.Ejbtask;
import com.synapsense.impl.taskscheduler.config.Group;
import com.synapsense.impl.taskscheduler.config.Tasks;
import com.synapsense.util.unitconverter.XmlConfigUtils;

/**
 * Class implements {@link QuartzTaskScheduler}'s configuration holder.
 * Configuration is read from xml configuration file(<tt>tasks.xml</tt>)and
 * parsed according to XML schema file <tt>QuartzTasks.xsd</tt>. There is no
 * another way to initialize configuration except to point paths to these two
 * files.
 * 
 * @author shabanov
 * 
 */
public class Configuration {

	private final static Logger logger = Logger.getLogger(Configuration.class);

	/**
	 * Default XML schema location
	 */
	private static final String DEFAULT_SCHEMA_FILE_NAME = "schemas/QuartzTasks.xsd";

	/**
	 * Default task group name
	 */
	private static final String DEFAULT_TASK_GROUP_NAME = "DEFAULT";

	/**
	 * Default XML configuration file name
	 */
	private static final String DEFAULT_CONFIG_FILE_NAME = "tasks.xml";

	/**
	 * schema file full name
	 */
	private String schemaFile;

	/**
	 * configuration file full name
	 */
	private String configFile;

	/**
	 * local field which stores parsed xml file as <code>Tasks</code> instance.
	 */
	private Tasks tasks;

	/**
	 * map of groups for faster access to them by group name
	 */
	private Map<String, Group> groupsMap;

	/**
	 * Creates new instance.
	 * 
	 * XML schema location : <tt>DEFAULT_SCHEMA_FILE_NAME</tt> Configuration
	 * file location :
	 * <tt>System.getProperty("com.synapsense.filestorage")/DEFAULT_CONFIG_FILE_NAME</tt>
	 * 
	 * @throws TaskSchedulerException
	 *             If new instance cannot be instantiated because of errors
	 */
	private Configuration() throws TaskSchedulerException {
		this(DEFAULT_SCHEMA_FILE_NAME, DEFAULT_CONFIG_FILE_NAME);
	}

	/**
	 * Creates new instance. Allows to specify XML schema location and XML
	 * confgiguration file location.
	 * 
	 * @param schemaFileName
	 * @param configFileName
	 * @throws TaskSchedulerException
	 */
	private Configuration(String schemaFileName, String configFileName) {
		this.schemaFile = schemaFileName;
		this.configFile = configFileName;
		// key map to fast access to task groups
		groupsMap = new HashMap<String, Group>();
		try {
			this.tasks = readConfigurationFromFile(schemaFileName, configFileName);
			for (Group group : this.tasks.getGroup()) {
				groupsMap.put(group.getName(), group);
			}
		} catch (TaskSchedulerException e) {
			logger.warn("Configuration file error. Default task configuration will be used instead" + ".", e);
		} finally {
			// if conf file has no DEFAULT group definition or there is some
			// problem with conf file
			if (groupsMap.get(DEFAULT_TASK_GROUP_NAME) == null) {
				Group defaultGroup = new Group();
				defaultGroup.setName(DEFAULT_TASK_GROUP_NAME);
				groupsMap.put(DEFAULT_TASK_GROUP_NAME, defaultGroup);
			}
		}
	}

	private Tasks readConfigurationFromFile(String schemaFileName, String configFileName) throws TaskSchedulerException {

		InputStream quartzTaskSchema = this.getClass().getClassLoader().getResourceAsStream(schemaFileName);

		if (quartzTaskSchema == null) {
			throw new TaskSchedulerException("File " + schemaFileName + " is not available in classpath");
		}

		File confFile = new File(configFileName);

		Source[] res = new Source[1];
		res[0] = new StreamSource(quartzTaskSchema, schemaFileName);

		try {
			XmlConfigUtils tasksConf = new XmlConfigUtils(res, Tasks.class);
			return tasksConf.read(confFile, Tasks.class);
		} catch (IOException e) {
			throw new TaskSchedulerException("IO error reading file :" + configFileName, e);
		} catch (JAXBException e) {
			throw new TaskSchedulerException("Error reading file :" + configFileName, e);
		} catch (SAXException e) {
			throw new TaskSchedulerException("Error parsing file :" + configFileName, e);
		}
	}

	/**
	 * Creates configuration from <tt>DEFAULT_CONFIG_FILE_NAME</tt> <br>
	 * XML Schema to parse configuration file placed in
	 * <tt>SCHEMA_FILE_NAME</tt>
	 * 
	 * @return Configuration instance
	 * 
	 * @throws TaskSchedulerException
	 *             If errors occured
	 */
	public static Configuration newConfiguration() throws TaskSchedulerException {
		if (logger.isDebugEnabled()) {
			logger.debug("schema:" + DEFAULT_SCHEMA_FILE_NAME + ", reading/parsing task configuration from file: "
			        + DEFAULT_CONFIG_FILE_NAME);
		}
		String fileStorage = System.getProperty("com.synapsense.filestorage", "");
		return newConfiguration(fileStorage);
	}

	/**
	 * Creates <code>Configuration</code> instance from
	 * <tt>DEFAULT_CONFIG_FILE_NAME</tt> which is placed in
	 * <tt>configFileStorage</tt> directory.
	 * 
	 * @param configFileStorage
	 *            Path to configuration file
	 * @return New <code>Configuration</code> instance
	 * @throws TaskSchedulerException
	 *             If errors occurred
	 */
	public static Configuration newConfiguration(String configFileStorage) throws TaskSchedulerException {
		if (logger.isDebugEnabled()) {
			logger.debug("schema:" + DEFAULT_SCHEMA_FILE_NAME + ", reading/parsing task configuration from file: "
			        + configFileStorage + "/" + DEFAULT_CONFIG_FILE_NAME);
		}
		// TODO : configFileStorage should be checked to unsupported characters
		String fileStorage = configFileStorage.isEmpty() ? DEFAULT_CONFIG_FILE_NAME : configFileStorage + "/"
		        + DEFAULT_CONFIG_FILE_NAME;
		return new Configuration(DEFAULT_SCHEMA_FILE_NAME, fileStorage);
	}

	/**
	 * Returns all configured task groups
	 * 
	 * @return Collection of groups definitions
	 * 
	 */
	public Collection<Group> getTaskGroups() {
		return this.groupsMap.values();
	}

	/**
	 * Returns one selected task group by its name. If such group does not exist
	 * returns DEFAULT group.
	 * 
	 * @param groupName
	 *            Name of task group
	 * 
	 * @return Task group definition
	 */
	public Group getTaskGroupByName(String groupName) {
		Group group = groupsMap.get(groupName);
		if (group == null) {
			if (logger.isDebugEnabled()) {
				logger.debug("requested task group[" + groupName + "] does not exist. returning group ["
				        + DEFAULT_TASK_GROUP_NAME + "]");
			}
			return groupsMap.get(DEFAULT_TASK_GROUP_NAME);
		} else {
			return group;
		}
	}

	public static void main(String[] args) throws TaskSchedulerException {
		Configuration conf = Configuration.newConfiguration("test/com/synapsense/scheduler");

		for (Group group : conf.getTaskGroups()) {
			logger.debug(group.getName() + ":" + group.getPriority());
			for (Ejbtask task : group.getEjbtask()) {
				logger.debug("	" + ":jndi: " + task.getJndiName() + ":cron: " + task.getCronExpression());
			}
		}

		Group group = conf.getTaskGroupByName("DEFAULT");
		logger.debug(group.getName() + ":" + group.getPriority());

		group = conf.getTaskGroupByName("ALERT");
		logger.debug(group.getName() + ":" + group.getPriority());

		conf = Configuration.newConfiguration("");
		group = conf.getTaskGroupByName("DEFAULT");
		logger.debug(group.getName() + ":" + group.getPriority());

		group = conf.getTaskGroupByName("ALERT");
		logger.debug(group.getName() + ":" + group.getPriority());
	}
}
