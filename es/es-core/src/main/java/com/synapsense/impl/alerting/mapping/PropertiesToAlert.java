package com.synapsense.impl.alerting.mapping;

import static com.synapsense.impl.alerting.AlertConstants.TYPE.ALERT_DESCRIPTION_TYPE_NAME;

import java.util.Collection;
import java.util.Date;
import java.util.Map;

import org.apache.log4j.Logger;

import com.synapsense.dto.Alert;
import com.synapsense.dto.AlertStatus;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.exception.PropertyNotFoundException;
import com.synapsense.exception.UnableToConvertPropertyException;
import com.synapsense.impl.alerting.AlertConstants;
import com.synapsense.impl.utils.EnvironmentUtils;
import com.synapsense.service.Environment;
import com.synapsense.util.CollectionUtils.Functor;

public class PropertiesToAlert implements Functor<Alert, Collection<ValueTO>> {

	private final static Logger logger = Logger.getLogger(PropertiesToAlert.class);

	private Environment env;
	private TO<?> alertRef;

	public PropertiesToAlert(Environment env) {
		this(env, TOFactory.EMPTY_TO);
	}

	public PropertiesToAlert(Environment env, TO<?> alertRef) {
		this.env = env;
		this.alertRef = alertRef;
	}

	@Override
	public Alert apply(Collection<ValueTO> input) {
		Map<String, Object> values = EnvironmentUtils.valuesAsMap(input);
		Integer alertId = (Integer) values.get("alertId");

		String name = (String) values.get("name");
		Long time = (Long) values.get("time");
		AlertStatus status = AlertStatus.decode((Integer) values.get("status"));
		String message = (String) values.get("message");
		String fullMessage = (String) values.get("fullMessage");
		String typeDescr = (String) values.get("typeDescr");
		String priority = (String) values.get("priority");

		TO<?> host = (TO<?>) values.get("hostTO");
		host = host == null ? TOFactory.EMPTY_TO : host;

		String acknowledgement = (String) values.get("acknowledgement");
		Long acknowledgementTime = (Long) values.get("acknowledgementTime");
		String user = (String) values.get("user");

		String typeName = (String) values.get("typeName");

		String alertTypeName = null;
		if (typeName != null) {
			alertTypeName = typeName;
		} else {
			TO<?> typeRef = (TO<?>) values.get("type");
			try {
				alertTypeName = env.getPropertyValue(typeRef, "name", String.class);
				typeDescr = env.getPropertyValue(typeRef, "description", String.class);
				TO<?> priorityTO = env.getPropertyValue(typeRef, "priority", TO.class);
				priority = env.getPropertyValue(priorityTO, "name", String.class);
			} catch (PropertyNotFoundException e) {
				throw new MappingException("Detected malformed object type : " + ALERT_DESCRIPTION_TYPE_NAME, e);
			} catch (UnableToConvertPropertyException e) {
				throw new MappingException("Detected malformed object type : " + ALERT_DESCRIPTION_TYPE_NAME, e);
			} catch (ObjectNotFoundException e) {
				logger.debug("Failed to get alert type while converting properties to alert instance. "
				        + AlertConstants.TYPE.UNKNOWN_ALERT_TYPE_NAME + " will be used instead", e);
				alertTypeName = AlertConstants.TYPE.UNKNOWN_ALERT_TYPE_NAME;
				typeDescr = "Generic alert, any alert with non-existing type is saved as UNKNOWN";
				priority = "MAJOR";
			}
		}

		Alert alert = new Alert(name, alertTypeName, new Date(time), message, host);
		// if explicitly passed alert ref
		if (alertRef != null && !TOFactory.EMPTY_TO.equals(alertRef)) {
			alert.setAlertId((Integer) alertRef.getID());
		} else {
			// if "alertId" property is given
			alert.setAlertId(alertId);
		}
		alert.setStatus(status);
		alert.setAcknowledgement(acknowledgement);
		if (acknowledgementTime != null) {
			alert.setAcknowledgementTime(new Date(acknowledgementTime));
		}
		alert.setUser(user);
		alert.setFullMessage(fullMessage);
		alert.setTypeDescr(typeDescr);
		alert.setPriority(priority);

		return alert;
	}
}
