package com.synapsense.impl.alerting.notification;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.synapsense.dto.Message;
import com.synapsense.impl.alerting.macros.Macro;
import com.synapsense.impl.alerting.macros.MacroContext;
import com.synapsense.util.CollectionUtils;

public class AlertNotificationTemplate implements NotificationTemplate {
	private final static Logger logger = Logger.getLogger(AlertNotificationTemplate.class);

	private final String name;

	private final Map<String, MessageTemplate> templates;
	private final Map<String, EscalationTier> tiers;
	private final Macro macro;
	private final String priority;
	private final NotificationStateManager stateManager;

	public AlertNotificationTemplate(final String name, final String priority,
	        final Map<String, MessageTemplate> templates, final List<EscalationTier> tiers, final Macro macro,
	        final NotificationStateManager stateManger) {
		this.name = name;
		this.priority = priority;
		this.templates = templates;
		this.tiers = CollectionUtils.newLinkedMap();
		this.macro = macro;
		this.stateManager = stateManger;
		for (final EscalationTier t : tiers) {
			this.tiers.put(t.getName(), t);
		}
	}

	@Override
	public Notification createNotification(final String id, final MacroContext ctx) {
		if (logger.isDebugEnabled()) {
			logger.debug("Creating " + tiers.size() + "-tiers notification instance corresponding to ALERT:" + id);
		}

		final List<NotificationState> states = new LinkedList<NotificationState>();
		for (final Map.Entry<String, EscalationTier> tierEntry : tiers.entrySet()) {
			final String tierName = tierEntry.getKey();
			final EscalationTier tier = tierEntry.getValue();
			states.add(new NotificationState(tierName, tier.getOrdinal(), tier.getRepeat(), tier.getInterval()));
		}

		final AlertNotificationPersistent nInstance = new AlertNotificationPersistent(new AlertNotification(id, states,
		        ctx, this), stateManager);
		return nInstance;
	}

	@Override
	public Notification restoreNotification(final String id, final MacroContext ctx) {
		if (logger.isDebugEnabled()) {
			logger.debug("Restoring tiers notification instance corresponding to ALERT:" + id);
		}

		boolean isTierFind = false;
		// load last tier and attempt
		final String lastNTierName = stateManager.loadLastTierName(id);
		Integer lastAttempt = stateManager.loadLastAttempt(id);

		/*
		 * if lastAttempt is null it's mean than alert hasn't send notifications
		 * yet. We need to create notification
		 */
		if (lastAttempt == null) {
			return createNotification(id, ctx);
		}

		if (lastNTierName == null) {
			isTierFind = true;
		}

		final List<NotificationState> states = new LinkedList<NotificationState>();
		for (final Map.Entry<String, EscalationTier> tierEntry : tiers.entrySet()) {
			final String tierName = tierEntry.getKey();

			if (tierName.equals(lastNTierName)) {
				isTierFind = true;
			}

			if (isTierFind) {
				final EscalationTier tier = tierEntry.getValue();
				/*
				 * if lastAttempt == repeatLimit it's mean that all
				 * notifications has send and haven't needed to restore
				 * notification
				 */
				if (lastAttempt >= tier.getRepeat()) {
					return null;
				}
				states.add(new NotificationState(tierName, tier.getOrdinal(), tier.getRepeat(), tier.getInterval(),
				        lastAttempt));
				lastAttempt = 0;
			}
		}

		final AlertNotificationPersistent nInstance = new AlertNotificationPersistent(new AlertNotification(id, states,
		        ctx, this), stateManager);
		return nInstance;
	}

	@Override
	public List<Message> generateMessages(final String itemKey, final MacroContext ctx) {
		if (logger.isDebugEnabled()) {
			logger.debug("Generating messages for item : " + itemKey + "...");
		}
		final EscalationTier t = getTier(itemKey);
		if (logger.isDebugEnabled()) {
			logger.debug("Current tier is " + t.getName());
		}
		final Collection<NTemplateItem> items = t.getMainItems();
		return generate(items, ctx);
	}

	@Override
	public List<Message> generateAckMessages(final String itemKey, final MacroContext ctx) {
		if (logger.isDebugEnabled()) {
			logger.debug("Generating messages for item : " + itemKey + "...");
		}
		final EscalationTier tier = getTier(itemKey);
		final Collection<NTemplateItem> items = tier.getAckItems();
		return generate(items, ctx);
	}

	@Override
	public List<Message> generateResolveMessages(final String itemKey, final MacroContext ctx) {
		final EscalationTier tier = getTier(itemKey);
		final Collection<NTemplateItem> items = tier.getResolveItems();
		return generate(items, ctx);
	}

	private EscalationTier getTier(final String itemKey) {
		final EscalationTier t = tiers.get(itemKey);
		if (t == null) {
			throw new IllegalStateException("Alert type configuration error. No escalation tier : " + itemKey
			        + " was configured");
		}
		return t;
	}

	private List<Message> generate(final Collection<NTemplateItem> items, final MacroContext ctx) {
		if (items == null)
			return Collections.emptyList();
		if (items.isEmpty())
			return Collections.emptyList();

		final List<Message> messages = new LinkedList<Message>();
		if (logger.isDebugEnabled()) {
			logger.debug("Iterating destination items...");
		}
		for (final NTemplateItem i : items) {
			final MessageTemplate template = templates.get(i.getTemplateRef());
			if (template == null) {
				logger.warn("Cannot generate message. There is no message template corresponding to message type : "
				        + i.getProtocol());
				continue;
			}

			if (logger.isDebugEnabled()) {
				logger.debug("Creating message from template  : " + i.getTemplateRef() + ", message type : "
				        + i.getProtocol());
			}
			// valid recipients list is required to generate message
			final List<Destination> destinations = i.getRecipients();
			if (!destinations.isEmpty()) {
				for (final Destination d : destinations) {

					final Message m = template.getMessage(macro, ctx, d, priority, i.getProtocol());
					messages.add(m);
					if (logger.isDebugEnabled()) {
						logger.debug("New message was created : " + m.toString());
					}
				}
			} else {
				if (logger.isDebugEnabled()) {
					logger.debug("There is no destinatons configured for item :  " + i.getProtocol());
				}
			}
		}
		return messages;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((priority == null) ? 0 : priority.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final AlertNotificationTemplate other = (AlertNotificationTemplate) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (priority == null) {
			if (other.priority != null)
				return false;
		} else if (!priority.equals(other.priority))
			return false;
		return true;
	}

}