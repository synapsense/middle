package com.synapsense.impl.reporting.tables;

import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.synapsense.impl.reporting.queries.TableProcessor.GroupType;
import com.synapsense.impl.reporting.utils.ReportingConstants.QUERY.SYMBOL;
import com.synapsense.impl.reporting.utils.ReportingConstants.QUERY.WORD;
import com.synapsense.service.reporting.olap.Member;
import com.synapsense.service.reporting.olap.custom.BaseLevel;
import com.synapsense.service.reporting.olap.custom.Cube;
import com.synapsense.service.reporting.olap.custom.Level;
import com.synapsense.util.CollectionUtils;

/**
 * The <code>LevelValuesTimeTable</code> class contains Time Level values Map.<br>
 * Clause Example: WHERE clause item will be like YEAR(timestamp) IN ('2012',
 * '2013')
 * 
 * @author Grigory Ryabov
 */
public class LevelValuesTimeTable implements ReportingTimeTable {

	private static final long serialVersionUID = -3562114448338177423L;
	
	Map<String, Set<Integer>> timeLevelValuesTable = CollectionUtils
			.newLinkedMap();

	public LevelValuesTimeTable() {
		timeLevelValuesTable.put(Cube.LEVEL_HOUR,
				CollectionUtils.newSet(Integer.class));
		timeLevelValuesTable.put(Cube.LEVEL_DAY,
				CollectionUtils.newSet(Integer.class));
		timeLevelValuesTable.put(Cube.LEVEL_MONTH,
				CollectionUtils.newSet(Integer.class));
		timeLevelValuesTable.put(Cube.LEVEL_YEAR,
				CollectionUtils.newSet(Integer.class));
	}

	@Override
	public void add(Member member) {
		timeLevelValuesTable.get(member.getLevel()).add(
				(Integer) member.getKey());
	}

	@Override
	public Level getLowestLevel() {
		String lowestLevel = null;
		for (Entry<String, Set<Integer>> entry : timeLevelValuesTable
				.entrySet()) {
			if (!entry.getValue().isEmpty()) {
				lowestLevel = entry.getKey();
			}
		}
		if (lowestLevel == null) {
			lowestLevel = Cube.LEVEL_YEAR;
		}
		return new BaseLevel(Cube.TIME, lowestLevel);
	}

	@Override
	public boolean isEmpty() {
		for (Entry<String, Set<Integer>> entry : timeLevelValuesTable
				.entrySet()) {
			if (!entry.getValue().isEmpty()) {
				return false;
			}
		}
		return true;
	}

	@SuppressWarnings("unchecked")
	@Override
	public String get() {
		StringBuilder timeValuesSB = new StringBuilder();
		for (Entry<String, Set<Integer>> entry : this.timeLevelValuesTable
				.entrySet()) {
			if (!entry.getValue().isEmpty()) {

				String levelParameter = GroupType.getTypeByName(entry.getKey())
						.getGroupType();
				timeValuesSB.append(levelParameter + " in(");
				StringBuilder timeLevelValuesSB = new StringBuilder();
				for (Integer value : entry.getValue()) {
					timeLevelValuesSB.append(value + ", ");
				}
				timeValuesSB.append(timeLevelValuesSB.substring(0,
						timeLevelValuesSB.length() - 2));
				timeValuesSB.append(")").append(SYMBOL.SPACE)
						.append(WORD.AND)
						.append(SYMBOL.SPACE);
			}
		}
		return timeValuesSB.substring(0, timeValuesSB.length() - 5);
	}

	@Override
	public LevelValuesTimeTable getTable() {
		
		LevelValuesTimeTable copy = new LevelValuesTimeTable();
		for (Entry<String, Set<Integer>> entry : timeLevelValuesTable
				.entrySet()) {
			copy.put(entry.getKey(), entry.getValue());
		}
		return copy;
		
	}

	private void put(String key, Set<Integer> value) {
		timeLevelValuesTable.put(key, value);
	}

	@Override
	public int size() {
		return this.timeLevelValuesTable.size();
	}

}
