package com.synapsense.impl.activitylog;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;

import org.apache.log4j.Logger;

import com.synapsense.impl.alerting.AlertConstants;
import com.synapsense.service.impl.activitylog.ActivityLogService;
import com.synapsense.service.impl.activitylog.ActivityRecord;

public class ActivityLogger {

	private final static Logger logger = Logger.getLogger(ActivityLogger.class);

	@EJB
	private ActivityLogService als;

	@Resource
	private EJBContext ctx;

	@AroundInvoke
	public Object log(final InvocationContext icx) throws Exception {
		String userName = ctx.getCallerPrincipal().getName();
		if ("anonymous".equals(userName)) {
			userName = AlertConstants.SYSTEM_ACTIVITY_IDENTITY;
		}

		// prepare log record based on input parameters
		ActivityRecord recordToLog = new Activity()
		        .log(userName, icx.getTarget(), icx.getMethod(), icx.getParameters());

		Object result = icx.proceed();
		if (recordToLog == null) {
			logger.debug("Cannot log activity : " + icx.toString() + ". Failed to create activity record instance");
			return result;
		}

		try { // catch all exceptions
			als.addRecord(recordToLog);
		} catch (Exception e) {
			logger.warn("Failed to log activity due to", e);
		}

		return result;
	}

}
