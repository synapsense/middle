package com.synapsense.impl.alerting.notification;

import java.util.Map;

import com.synapsense.service.impl.notification.Transport;
import com.synapsense.util.CollectionUtils;

public class DefaultTransportProvider implements TransportProvider {
	private final Map<String, Transport> transports = CollectionUtils.newMap();

	@Override
	public Transport get(final String protocol) {
		return transports.get(protocol);
	}

	@Override
	public void registerTransport(final String name, final Transport impl) {
		transports.put(name, impl);
	}

}