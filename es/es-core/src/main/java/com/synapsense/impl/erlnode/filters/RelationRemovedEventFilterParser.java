package com.synapsense.impl.erlnode.filters;

import org.apache.log4j.Logger;

import com.ericsson.otp.erlang.OtpErlangObject;
import com.ericsson.otp.erlang.OtpErlangTuple;
import com.synapsense.impl.erlnode.commands.RegisterProcessorCommand;
import com.synapsense.service.nsvc.filters.RelationRemovedEventFilter;
import com.synapsense.service.nsvc.filters.TOMatcher;

//-record(relation_set_event_filter, {
//pmatcher = #regex{} :: #regex{} | #to{},
//cmatcher = #regex{} :: #regex{} | #to{}}).
class RelationRemovedEventFilterParser {

	private static final Logger log =
		Logger.getLogger(RelationRemovedEventFilter.class);

	public RelationRemovedEventFilter parse(OtpErlangTuple filterDef) {
		TOMatcher pmatcher = null, cmatcher = null;

		OtpErlangObject tmp = filterDef.elementAt(1);
		if (tmp != null && !RegisterProcessorCommand.undefined.equals(tmp))
			pmatcher = TOMatcherParser.parse(tmp);

		tmp = filterDef.elementAt(2);
		if (tmp != null && !RegisterProcessorCommand.undefined.equals(tmp))
			cmatcher = TOMatcherParser.parse(tmp);

		if (pmatcher == null && cmatcher == null)
			throw new IllegalArgumentException(
					"Either one of or both parent and child matchers must be specified");

		log.debug("returning RelationRemovedEventFilter");
		return new RelationRemovedEventFilter(pmatcher, cmatcher);
	}

}
