package com.synapsense.impl.alerting.command;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.apache.log4j.Logger;

import com.synapsense.commandprocessor.api.Command;
import com.synapsense.dto.AlertStatus;
import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.AlertServiceSystemException;
import com.synapsense.exception.EnvException;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.exception.PropertyNotFoundException;
import com.synapsense.exception.UnableToConvertPropertyException;
import com.synapsense.service.Environment;
import com.synapsense.util.CollectionUtils;

/**
 * Alert status update command.
 * Disable notifications if target status 
 * 
 * @author shabanov
 * 
 */
public class UpdateStatus implements Command {

	private final static Logger logger = Logger.getLogger(UpdateStatus.class);

	private Environment env;

	private Collection<TO<?>> alerts;
	private AlertStatus newStatus;
	private String comment;
	private TO<?> user;
	private long time;

	public UpdateStatus(Environment env, Collection<TO<?>> alerts, AlertStatus newStatus, String comment, TO<?> user,
	        long time) {
		this.env = env;
		this.alerts = alerts;
		this.newStatus = newStatus;
		this.comment = comment;
		this.user = user;
		this.time = time;
	}

	public UpdateStatus(Environment env, TO<?> alert, AlertStatus newStatus, String comment, TO<?> user, long time) {
		this(env, Arrays.<TO<?>> asList(alert), newStatus, comment, user, time);
	}

	@Override
	public void execute() {
		for (final TO<?> alert : alerts) {
			int status;
			try {
				status = getStatus(alert);
			} catch (ObjectNotFoundException e) {
				if (logger.isDebugEnabled()) {
					logger.debug("Failed to get status of alert : " + alert, e);
				}
				continue;
			}
			AlertStatus oldStatus = AlertStatus.decode(status);

			if (!oldStatus.canProceedTo(newStatus)) {
				if (logger.isDebugEnabled()) {
					logger.debug("Failed to update status on alert : " + alert + ". Cannot proceed from : " + oldStatus
					        + " to : " + newStatus);
				}
				continue;
			}

			List<ValueTO> values = CollectionUtils.newList();
			values.add(new ValueTO("status", newStatus.code()));

			try {
				String oldAcknowledgement = env.getPropertyValue(alert, "acknowledgement", String.class);
				values.add(new ValueTO("acknowledgement", newStatus.compoundComment(oldAcknowledgement).concat(comment)));
			} catch (ObjectNotFoundException e) {
				if (logger.isDebugEnabled()) {
					logger.debug("Alert[" + alert + "] does not exist", e);
				}
				continue;
			} catch (PropertyNotFoundException e) {
				throw new AlertServiceSystemException("Failed to get alert : " + alert + "'s 'acknowledgement'", e);
			} catch (UnableToConvertPropertyException e) {
				throw new AlertServiceSystemException("Failed to get alert : " + alert + "'s 'acknowledgement'", e);
			}
			values.add(new ValueTO("acknowledgementTime", time));

			// ignore not meaningful properties
			if (user != null) {
				String userName = null;
				try {
					userName = this.env.getPropertyValue(user, "FName", String.class);
					userName += " ";
					userName += this.env.getPropertyValue(user, "LName", String.class);
				} catch (ObjectNotFoundException | PropertyNotFoundException | UnableToConvertPropertyException e) {
					throw new AlertServiceSystemException("Failed to get user name for user : " + user.toString() + ".", e);
				}
				values.add(new ValueTO("user", userName));
			}

			boolean isNotificationEnabled = newStatus.isActive();
			try {
				env.setAllPropertiesValues(alert, values.toArray(new ValueTO[] {}), isNotificationEnabled);
			} catch (EnvException e) {
				logger.warn("Unable to update alert : " + alert + " due to : ", e);
				continue;
			}

			if (logger.isDebugEnabled()) {
				logger.debug("Alert[" + alert + "] has been " + newStatus + " successfully");
			}
		}
	}

	private int getStatus(TO<?> alert) throws ObjectNotFoundException {
		Integer status = null;
		try {
			status = env.getPropertyValue(alert, "status", Integer.class);
		} catch (PropertyNotFoundException e) {
			throw new AlertServiceSystemException("Failed to get alert : " + alert + "'s 'status'", e);
		} catch (UnableToConvertPropertyException e) {
			throw new AlertServiceSystemException("Failed to get alert : " + alert + "'s 'status'", e);
		}

		if (status == null) {
			throw new AlertServiceSystemException("Broken alert : " + alert + ". Property 'status' cannot be null");
		}

		return status;
	}

}
