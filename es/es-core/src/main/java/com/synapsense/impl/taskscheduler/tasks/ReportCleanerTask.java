/**
 * 
 */
package com.synapsense.impl.taskscheduler.tasks;

/**
 * @author apakhunov
 * 
 */
public interface ReportCleanerTask {

	void execute();

}
