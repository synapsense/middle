package com.synapsense.impl.reporting.model;

import java.util.Map;
import java.util.concurrent.Executor;

import net.sf.jasperreports.engine.DefaultJasperReportsContext;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.JasperReportsContext;
import net.sf.jasperreports.engine.fill.AsynchronousFillHandle;

public class ReportingAsynchronousFillHandle extends AsynchronousFillHandle{

	protected ReportingAsynchronousFillHandle(
			JasperReportsContext jasperReportsContext,
			JasperReport jasperReport, Map<String, Object> parameters,
			JRDataSource dataSource) throws JRException {
		super(jasperReportsContext, jasperReport, parameters, dataSource);
	}
	
	public static ReportingAsynchronousFillHandle createHandle(
			JasperReport jasperReport,
			Map<String,Object> parameters,
			JRDataSource dataSource
			) throws JRException
	{
		return createHandle(DefaultJasperReportsContext.getInstance(), jasperReport, parameters, dataSource);
	}
	
	public static ReportingAsynchronousFillHandle createHandle(
			JasperReportsContext jasperReportsContext,
			JasperReport jasperReport,
			Map<String,Object> parameters,
			JRDataSource dataSource
			) throws JRException
	{
		return new ReportingAsynchronousFillHandle(jasperReportsContext, jasperReport, parameters, dataSource);
	}
	
	@Override
	protected Executor getReportExecutor()
	{
		return new ThreadExecutor();
	}
	
	protected class ThreadExecutor implements Executor
	{
		@Override 
		public void execute(Runnable command)
		{
			command.run();
		}
	}
	
}
