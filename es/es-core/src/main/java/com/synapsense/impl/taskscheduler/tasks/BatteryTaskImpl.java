package com.synapsense.impl.taskscheduler.tasks;

import com.synapsense.impl.utils.CommonUtils;
import com.synapsense.shared.NodeStatus;
import java.util.*;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import com.synapsense.service.impl.dao.NetworkStatisticDAOIF;
import com.synapsense.util.CollectionUtils;
import org.apache.log4j.Logger;

import com.synapsense.dto.Alert;
import com.synapsense.dto.CollectionTO;
import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.exception.PropertyNotFoundException;
import com.synapsense.exception.UnableToConvertPropertyException;
import com.synapsense.service.AlertService;
import com.synapsense.service.Environment;
import com.synapsense.service.impl.messages.base.StatisticData;
import com.synapsense.util.LocalizationUtils;
import com.synapsense.util.TimeSample;

/**
 * Runs battery status and battery capacity calculations for battery operated
 * nodes and updates their properties accordingly.
 *
 * @author aborisov
 * @author Oleg Stepanov (stepanov@mera.ru)
 * @author shabanov
 */
@Stateless
@Remote(BatteryTask.class)
@TransactionAttribute(TransactionAttributeType.NEVER)
@TransactionManagement(TransactionManagementType.BEAN)
public class BatteryTaskImpl implements BatteryTask {

	private final static Logger logger = Logger.getLogger(BatteryTaskImpl.class);

	private static final double MSEC_IN_HOUR = 60D * 60 * 1000;

    private static final String LAST_VALUE = "lastValue";

    /**
     * Enumeration of battery status where ordinals map directly to database values.
     */
    private enum BatteryStatus {
        OK, LOW, CRITICAL, UNKNOWN;

        public static BatteryStatus valueOf(int n) {
            for (BatteryStatus bs : BatteryStatus.values()) {
                if (bs.ordinal() == n) return bs;
            }
            throw new IllegalArgumentException(n + " is not a valid ordinal value of BatteryStatus.");
        }
    }

	// Ywings
	private static final Set<Integer> yWingPlatformIds = CollectionUtils.newSet(82,92);

	// platform DX has special energy consumption calc formula
	private static final Set<Integer> thermaNodeDxPlatformIds = CollectionUtils.newSet(20);

    // platforms that use battery voltage to determine battery status
    private static final Set<Integer> batteryVoltagePlatformIds = CollectionUtils.newSet(32);

	private Environment env;

	private NetworkStatisticDAOIF<Integer, StatisticData> netStatDAO;

	private AlertService alertService;

    private double lowCapacityThreshold;  // mAh
    private double criticalCapacityThreshold;  // mAh
    private double lowVoltageThreshold;  // volts
    private double criticalVoltageThreshold;  // volts
    private double voltageVarianceThreshold;  // volts
    private int voltageStableHistorySize;  // entries
    private int voltageRecentHistorySize;  // entries
    private int voltageStableHistoryAge;  // in milliseconds

    public BatteryTaskImpl() {
        super();
    }

    /**
     * Loads parameters for tuning battery status calculations.
     */
    private void loadTunableParameters() {
        // capacity thresholds
        lowCapacityThreshold = getPropDouble("com.synapsense.batteryStatus.lowCapacityThreshold", 200.0D, 1D, 100000D);
        double d = getPropDouble("com.synapsense.batteryStatus.criticalCapacityThreshold", 100.0D, 0D, 100000D);
        // enforce: critical < low
        if (d >= lowCapacityThreshold)
            criticalCapacityThreshold = lowCapacityThreshold - 1d;
        else
            criticalCapacityThreshold = d;

        // voltage thresholds
        lowVoltageThreshold = getPropDouble("com.synapsense.batteryStatus.lowVoltageThreshold", 3.3D, 0.02D, 3.7D);
        d = getPropDouble("com.synapsense.batteryStatus.criticalVoltageThreshold", 3.25D, 0.01, 3.7);
        // enforce: critical < low
        if (d >= lowVoltageThreshold)
            criticalVoltageThreshold = lowVoltageThreshold - 0.01d;
        else
            criticalVoltageThreshold = d;

        // stability algorithm params
        voltageVarianceThreshold = getPropDouble("com.synapsense.batteryStatus.voltageVarianceThreshold", 0.03D, 0D, 5D);
        voltageStableHistorySize = getPropInt("com.synapsense.batteryStatus.voltageStableHistorySize", 12, 1, 10000);
        int n = getPropInt("com.synapsense.batteryStatus.voltageRecentHistorySize", 5, 1, 1000);
        // enforce: recent <= stable
        if (n > voltageStableHistorySize)
            voltageRecentHistorySize = voltageStableHistorySize;
        else
            voltageRecentHistorySize = n;
        voltageStableHistoryAge = getPropInt("com.synapsense.batteryStatus.voltageStableHistoryAge", 125*60*1000, 300000, 604800000); // 125m, 5m, 1w
    }

    private double getPropDouble(String prop, double def, double min, double max) {
        try {
            double d = Double.parseDouble(System.getProperty(prop, "" + def));
            if (d < min || d > max)
                return def;
            else
                return d;
        } catch(NumberFormatException e) {
            return def;
        }
    }

    private int getPropInt(String prop, int def, int min, int max) {
        try {
            int i = Integer.parseInt(System.getProperty(prop, "" + def));
            if (i < min || i > max)
                return def;
            else
                return i;
        } catch(NumberFormatException e) {
            return def;
        }
    }

    private int getSamplingInMinutes(TO<?> nodeID) {
		try {
			return TimeSample.get(env.getPropertyValue(nodeID, "period", String.class)).toSeconds() / 60;
		} catch (Exception e) {
			return 5; // default sampling interval
		}
	}

	private double calculateUsage(TO<?> node, long startTime, long endTime) throws ObjectNotFoundException,
	        PropertyNotFoundException, UnableToConvertPropertyException {
		double total_slot_count = 0;
		double radio_on_time = 0;
		final double hoursSpent = (endTime - startTime) / MSEC_IN_HOUR;

		final Date start = new Date(startTime);
		final Date end = new Date(endTime);

		Collection<ValueTO> rawStat = netStatDAO.getData((Integer) node.getID(), start, end);
		for (ValueTO item : rawStat) {
			StatisticData data = (StatisticData) item.getValue();
			total_slot_count += data.getTotalSlotCount();
			radio_on_time += data.getRadioOnTime();
		}

		double result = 0;
		Integer platformId = env.getPropertyValue(node, "platformId", Integer.class);

		if (yWingPlatformIds.contains(platformId)) {
			if (hoursSpent <= 0D)
				return result; 
			Collection<TO<?>> sensors = env.getChildren(node, "WSNSENSOR");
			Collection<CollectionTO> sensorsSubSamples = env.getPropertyValue(sensors, new String[] { "dataclass",
			        "subSamples", "channel" });
            boolean isUsageActual = false;
			for (CollectionTO sensorSubSamples : sensorsSubSamples) {
                int channel = (Integer) sensorSubSamples.getSinglePropValue("channel").getValue();
                int dataclass = (Integer) sensorSubSamples.getSinglePropValue("dataclass").getValue();
				if (dataclass == 200 && channel == 3) {
					double avgRadioOnTimeInSeconds = (radio_on_time / hoursSpent) / 1000;
					int subSamplingIntervalInSeconds = (Integer) sensorSubSamples.getSinglePropValue("subSamples")
					        .getValue();
					result = ((0.006772 * avgRadioOnTimeInSeconds) + 0.015838 + (0.0141327 / subSamplingIntervalInSeconds))
					        * hoursSpent;
                    isUsageActual = true;
 				}
			}
            if (!isUsageActual) {
                logger.warn("YWing Node " + node.toString() + " should have thermistor on third channel. Until that the usage will be 0");
            }
		} else if (thermaNodeDxPlatformIds.contains(platformId)) {
			if (hoursSpent <= 0D)
				return result;
            int samplingIntervalSec = getSamplingInMinutes(node) * 60;
			double smartSendSampling = 3600 / samplingIntervalSec * (Math.ceil(samplingIntervalSec / 30) - 1);
			double totalSmartSendSampling = 3600 / samplingIntervalSec + smartSendSampling + 240;
			double avgRadioOnTimeInSeconds = (radio_on_time / hoursSpent) / 1000;
			double avgWakeSlots = (total_slot_count / hoursSpent);
			result = (0.005611 * avgRadioOnTimeInSeconds + 2.909832 / samplingIntervalSec + 0.0000583848
			        * smartSendSampling + 0.0128266 + 0.00000262222 * totalSmartSendSampling + 0.00118 + 0.00000364810
			        * avgWakeSlots + 0.0153 * (3600 - 0.004 * totalSmartSendSampling - 1.8 - 0.0055649 * avgWakeSlots) / 3600)
			        * hoursSpent;
		} else {
			result = total_slot_count / 400 + radio_on_time / 180 + (29 + 55 / getSamplingInMinutes(node)) * hoursSpent;
			result = result / 1000;
		}

		return result;
	}

    private BatteryStatus calculateStatusFromVolts(TO<?> node) throws Exception {
        // first get the battery sensor for the node
        Collection<TO<?>> sensors = env.getChildren(node, "WSNSENSOR");
        TO<?> sensor = null;
        for (TO<?> s : sensors) {
            if (env.getPropertyValue(s, "type", Integer.class) == 43) {
                sensor = s;
                break;
            }
        }
        if (sensor == null) {
            logger.warn("No battery sensors found for node " + node);
            return BatteryStatus.UNKNOWN;
        }

        // if lastValue is good, status is good (no need for stability check)
        double lastValue = env.getPropertyValue(sensor, LAST_VALUE, Double.class);
        if (lastValue > lowVoltageThreshold)
            return BatteryStatus.OK;

        // get a sampling of sensor's historic values
        // using a time-based window with a minimum number of entries
        long now = System.currentTimeMillis();
        Collection<ValueTO> hist = env.getHistory(sensor, LAST_VALUE,
                new Date(now - voltageStableHistoryAge), new Date(now),
                Double.class);
        if (hist.size() < voltageStableHistorySize) {
            logger.warn("Insufficient battery voltage history for sensor " +
                    sensor + " (only " + hist.size() + " of the required " +
                    voltageStableHistorySize +
                    " in the past " + voltageStableHistoryAge + " ms).");
            return BatteryStatus.UNKNOWN;
        }

        // determine if the sample is stable (using min/max variance)
        // (also calculate recent average while we're in the loop)
        double min = Double.MAX_VALUE;
        double max = Double.MIN_VALUE;
        double sum = 0; // used for average
        int i = 0;
        for(ValueTO v : hist) {
            i++;
            Double d = (Double)v.getValue();
            if (i <= voltageRecentHistorySize)
                sum += d;  // only sum first few for the avg, not the whole history
            logger.debug("Sensor " + sensor.getID() + ": " + d + " at " +
                    (new Date(v.getTimeStamp()) + "."));
            if (d < min) min = d;
            if (d > max) max = d;
        }
        logger.debug("min/max battery voltage: " + min + "/" + max);
        if (max - min > voltageVarianceThreshold) {
            logger.warn("The range of battery voltage history (" + min + " to " +
                    max + ") exceeds the stability threshold for the latest " +
                    hist.size() + " values.");
            return BatteryStatus.UNKNOWN;
        }

        // determine status based on average of recent stable values, since lastValue may oscillate around a threshold
        double recentAvg = sum / voltageRecentHistorySize;
        logger.debug("recent average of battery voltage: " + sum + "/" +
                voltageRecentHistorySize + " = " + recentAvg);
        if (recentAvg > lowVoltageThreshold)
            return BatteryStatus.OK;
        else if (recentAvg > criticalVoltageThreshold)
            return BatteryStatus.LOW;
        else
            return BatteryStatus.CRITICAL;
    }

    private double calculateRemainingCapacity(TO<?> node, long curTime) throws Exception {
        final ValueTO capacity = env.getPropertyValue(node, "batteryCapacity", ValueTO.class);
        final double initialCapacity = (Double) capacity.getValue();
        final long startTime = capacity.getTimeStamp();
        final double used = calculateUsage(node, startTime, curTime);
        final double daysSpent = (curTime - startTime) / (MSEC_IN_HOUR * 24);
        final double remainingBattery = initialCapacity - used;

        // update capacity only if needed
        if (remainingBattery > 0D && remainingBattery == initialCapacity) {
            logger.debug("Node : " + node + " has not been discharged for " + daysSpent + " days");
        } else {
            env.setPropertyValue(node, "batteryCapacity", remainingBattery);
        }
        return remainingBattery;
    }

	@Override
	public void execute() {
		logger.debug("Battery task started");
		long curTime = System.currentTimeMillis();
        loadTunableParameters();
		Collection<TO<?>> nodes = env.getObjectsByType("WSNNODE");
		int counter = 0;
		for (TO<?> node : nodes) {
			counter++;
			logger.debug("Processing node " + node + " (" + counter + " of " + nodes.size() + ")");
			try {
				// skip nodes that aren't battery operated
                if (env.getPropertyValue(node, "batteryOperated", Integer.class) != 1)
                    continue;

                final Integer platformId = env.getPropertyValue(node, "platformId", Integer.class);

                // calculate (and store) remaining battery capacity
                double remainingCapacity = calculateRemainingCapacity(node, curTime);

                // calculate battery status
                BatteryStatus batteryStatus;
                if (CommonUtils.nullSafeInt(env.getPropertyValue(node, "status", Integer.class)) != NodeStatus.OK) {
                    // [24432] Set batteryStatus to UNKNOWN when status != OK
                    batteryStatus = BatteryStatus.UNKNOWN;
                } else if (batteryVoltagePlatformIds.contains(platformId)) {
                    // based on volts
                    batteryStatus = calculateStatusFromVolts(node);
                } else {
                    // based on remaining battery capacity
                    if(remainingCapacity > lowCapacityThreshold) {
                        batteryStatus = BatteryStatus.OK;
                    } else if (remainingCapacity > criticalCapacityThreshold) {
                        batteryStatus = BatteryStatus.LOW;
                    } else {
                        batteryStatus = BatteryStatus.CRITICAL;
                    }
                }

                // store node battery status
                env.setPropertyValue(node, "batteryStatus", batteryStatus.ordinal());
                logger.debug("Set batteryStatus = " + batteryStatus.ordinal() +
                        " for node " + node);

                // send alerts if necessary
                // TODO: if exception is thrown prior to this, we will not re-raise alerts to keep them active
                if (batteryStatus == BatteryStatus.LOW) {
                    // low battery status
                    String nodeName = env.getPropertyValue(node, "name", String.class);
                    alertService.raiseAlert(new Alert("Low battery alert", "Low battery", new Date(), LocalizationUtils
                            .getLocalizedString("alert_message_low_battery_alert", nodeName), node));
                    logger.debug("Raised low battery alert for node " + node);
                } else if (batteryStatus == BatteryStatus.CRITICAL) {
                    // critical battery status
                    String nodeName = env.getPropertyValue(node, "name", String.class);
                    alertService.raiseAlert(new Alert("Critical battery alert", "Critical battery", new Date(), LocalizationUtils
                            .getLocalizedString("alert_message_critical_battery_alert",
                                    nodeName), node));
                    logger.debug("Raised critical battery alert for node " + node);
                }
                logger.debug("Node " + node + " (" + counter + " of " + nodes.size() + ") processed");
			} catch (Exception e) {
                // here we want the stack as this is more unexpected
                logger.warn("Cannot calculate battery status for node " + node + ". ", e);
            }
		}
		logger.info("Battery task completed. Processed " + counter + " nodes in " +
                (System.currentTimeMillis() - curTime) + " ms.");
	}

    public Environment getEnv() {
		return env;
	}

	@EJB(beanName = "LocalEnvironment")
	public void setEnv(Environment env) {
		this.env = env;
	}

	public NetworkStatisticDAOIF<Integer, StatisticData> getNetStatDAO() {
		return netStatDAO;
	}

	@EJB(beanName = "NetworkStatisticDAO")
	public void setNetStatDAO(NetworkStatisticDAOIF<Integer, StatisticData> netStatDAO) {
		this.netStatDAO = netStatDAO;
	}

	public AlertService getAlertService() {
		return alertService;
	}

	@EJB
	public void setAlertService(AlertService alertService) {
		this.alertService = alertService;
	}

}
