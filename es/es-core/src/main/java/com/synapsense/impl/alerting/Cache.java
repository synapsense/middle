package com.synapsense.impl.alerting;

/**
 * Cache abstraction
 *  
 * 
 * @author shabanov
 *
 * @param <K> Key type 
 * @param <V> Value type
 */
public interface Cache<K, V> {
	V get(K key);

	void put(K key, V value);

	boolean remove(V value);
}
