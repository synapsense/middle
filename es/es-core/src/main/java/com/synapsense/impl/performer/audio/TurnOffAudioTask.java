package com.synapsense.impl.performer.audio;


public class TurnOffAudioTask implements Runnable {
	private SignalSender sender;

	public TurnOffAudioTask(SignalSender sender) {
		this.sender = sender;
	}

	@Override
	public void run() {
		sender.sendTurnOffAudioRequest();
	}

}
