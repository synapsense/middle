package com.synapsense.impl.devmanager.dissectors;

import com.synapsense.dto.TO;
import com.synapsense.plugin.messages.GenericMessage;

/**
 * 
 * An generic MessageDissector interface. Implementations of the interface are
 * responsible to build appropriate JMS BusMessages (see
 * com.synapsense.service.messages package) from a <code>GenericMessage</code>.
 * 
 * @author anechaev
 * 
 */
public interface MessageDissector {
	/**
	 * Returns true if the Dissector can transform the
	 * <code>GenericMessage</code> into <code>BusMessage</code>
	 * 
	 * @return
	 */
	boolean tasteMessage(GenericMessage msg);

	/**
	 * @throws IllegalArgumentException
	 *             if the msg cannot be transformed by given dissector due to:
	 *             msg is null, msg is incompatible with dissector, msg contains
	 *             wrong data type
	 */
	void process(TO<?> objID, GenericMessage msg);
}
