package com.synapsense.impl.alerting;

import static com.synapsense.impl.alerting.AlertConstants.TYPE.ALERT_HISTORY_TYPE_NAME;
import static com.synapsense.impl.alerting.AlertConstants.TYPE.ALERT_TYPE_NAME;

import java.util.Arrays;

import com.synapsense.service.nsvc.EventProcessor;
import com.synapsense.service.nsvc.events.DispatchingProcessor;
import com.synapsense.service.nsvc.filters.OrFilter;
import com.synapsense.service.nsvc.filters.PropertiesChangedEventFilter;
import com.synapsense.service.nsvc.filters.TOMatcher;

public final class AlertNotifications {
	private AlertNotifications() {

	}

	public static EventProcessor alertStatusFilter() {
		PropertiesChangedEventFilter alertStatusFilter = new PropertiesChangedEventFilter(
		        new TOMatcher(ALERT_TYPE_NAME), new String[] { "status" });
		PropertiesChangedEventFilter alertHStatusFilter = new PropertiesChangedEventFilter(new TOMatcher(
		        ALERT_HISTORY_TYPE_NAME), new String[] { "status","alertId" });

		DispatchingProcessor d1 = new DispatchingProcessor().putPcep(alertStatusFilter);
		DispatchingProcessor d2 = new DispatchingProcessor().putPcep(alertHStatusFilter);

		OrFilter alertAndHistoryStatusFilter = new OrFilter(Arrays.<EventProcessor> asList(d1, d2));
		return alertAndHistoryStatusFilter;
	}

}
