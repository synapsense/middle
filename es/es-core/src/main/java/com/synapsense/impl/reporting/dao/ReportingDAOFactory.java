package com.synapsense.impl.reporting.dao;

import com.synapsense.service.reporting.exception.DAOReportingException;

public interface ReportingDAOFactory {

	ReportTemplateDAO newReportTemplateDAO();

	ReportDAO newReportDao();

    ReportTaskDAO newReportTaskDao();

	void shutdown() throws DAOReportingException;
}
