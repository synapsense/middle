package com.synapsense.impl.reporting.commandengine;

import java.util.concurrent.Executor;

public interface ReportingExecutor extends Executor {

    public void execute(Runnable command);

}
