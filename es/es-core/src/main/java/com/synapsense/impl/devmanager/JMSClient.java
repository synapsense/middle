package com.synapsense.impl.devmanager;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.EJB;
import javax.ejb.MessageDriven;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.dal.generic.entities.NhtInfoDO;
import com.synapsense.dal.objectcache.CacheEnvironmentDispatcher;
import com.synapsense.dal.objectcache.CacheSvc;
import com.synapsense.dal.objectcache.CacheSvcImpl;
import com.synapsense.dal.objectcache.Status;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.PropertyNotFoundException;
import com.synapsense.exception.UnableToConvertPropertyException;
import com.synapsense.impl.devmanager.dissectors.DuoConfDissector;
import com.synapsense.impl.devmanager.dissectors.DuoDataDissector;
import com.synapsense.impl.devmanager.dissectors.MessageDissector;
import com.synapsense.impl.devmanager.dissectors.PhaseDataDissector;
import com.synapsense.impl.devmanager.dissectors.PlugDataDissector;
import com.synapsense.impl.devmanager.dissectors.PowerRackConfDissector;
import com.synapsense.impl.devmanager.dissectors.RefinedAlertsDissector;
import com.synapsense.impl.devmanager.dissectors.WSNGWStatusDissector;
import com.synapsense.impl.devmanager.dissectors.WSNNHTDissector;
import com.synapsense.impl.devmanager.dissectors.WSNNodeVersionDissector;
import com.synapsense.impl.devmanager.dissectors.WSNRoutingDissector;
import com.synapsense.impl.devmanager.dissectors.WSNSensorDataDissector;
import com.synapsense.impl.devmanager.dissectors.WSNStatisticsDissector;
import com.synapsense.plugin.messages.GenericMessage;
import com.synapsense.plugin.messages.GenericMessage.GenericMessageTypes;
import com.synapsense.service.Environment;
import com.synapsense.service.impl.dao.DataObjectDAO;
import com.synapsense.service.impl.devmanager.DeviceEventListener;
import com.synapsense.service.impl.messages.SensedMessage;
import com.synapsense.service.impl.messages.base.DuoConfData;
import com.synapsense.service.impl.messages.base.NodeVersionData;
import com.synapsense.service.impl.messages.base.PhaseData;
import com.synapsense.service.impl.messages.base.PlugData;
import com.synapsense.service.impl.messages.base.RefinedAlertData;
import com.synapsense.service.impl.messages.base.RoutingData;
import com.synapsense.service.impl.messages.base.SandcrawlerConfData;
import com.synapsense.service.impl.messages.base.SensorData;
import com.synapsense.service.impl.messages.base.StatisticData;
import com.synapsense.util.algo.Pair;
import com.synapsense.util.algo.UnaryFunctor;

//other activation properties are defined in ejb-jar.xml per instance
@MessageDriven(activationConfig = {
		@ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
		@ActivationConfigProperty(propertyName = "destination", propertyValue = "queue/dm")
})
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class JMSClient implements MessageListener {
	private Log log = LogFactory.getLog(JMSClient.class);

	private DeviceEventListener eventListener;
	private Environment env;
	private final Map<GenericMessageTypes, MessageDissector> dissectors;

	private DataObjectDAO<Integer, NhtInfoDO> nhtInfo;

	/*
	 * this reference is used to prevent JMSClient beans from starting before
	 * CacheSvc; see init method;
	 */
	@EJB
	private CacheSvc cacheSvc;

	@EJB(beanName="NhtInfoDAO")
	public void setNhtInfo(final DataObjectDAO<Integer, NhtInfoDO> nhtInfo) {
		this.nhtInfo = nhtInfo;
	}

	public JMSClient() {
		dissectors = new HashMap<GenericMessageTypes, MessageDissector>();
		dissectors.put(GenericMessageTypes.WSN_SENSOR_DATA, new WSNSensorDataDissector(
		        new UnaryFunctor<Pair<TO<?>, SensorData>>() {
			        @Override
			        public void process(final Pair<TO<?>, SensorData> value) {
				        eventListener.onSensorData(value.getFirst(), value.getSecond());
			        }
		        }));
		dissectors.put(GenericMessageTypes.WSN_STATISTICS, new WSNStatisticsDissector(
		        new UnaryFunctor<Pair<TO<?>, StatisticData>>() {
			        @Override
			        public void process(final Pair<TO<?>, StatisticData> value) {
				        eventListener.onStatistic(value.getFirst(), value.getSecond());
			        }
		        }));
		dissectors.put(GenericMessageTypes.WSN_ROUTING, new WSNRoutingDissector(
		        new UnaryFunctor<Pair<TO<?>, RoutingData>>() {
			        @Override
			        public void process(final Pair<TO<?>, RoutingData> value) {
				        storeRoute(value.getSecond());
			        }
		        }));

		dissectors.put(GenericMessageTypes.WSN_GWSTATUS, new WSNGWStatusDissector(
		        new UnaryFunctor<Pair<TO<?>, Integer>>() {
			        @Override
			        public void process(final Pair<TO<?>, Integer> value) {
				        eventListener.onGWStatus(value.getFirst(), value.getSecond());
			        }
		        }));
		dissectors.put(GenericMessageTypes.REFINED_ALERT, new RefinedAlertsDissector(
		        new UnaryFunctor<Pair<TO<?>, RefinedAlertData>>() {
			        @Override
			        public void process(final Pair<TO<?>, RefinedAlertData> value) {
				        eventListener.onAlert(value.getFirst(), value.getSecond());
			        }
		        }));
		dissectors.put(GenericMessageTypes.WSN_NODEVERSION, new WSNNodeVersionDissector(
		        new UnaryFunctor<Pair<TO<?>, NodeVersionData>>() {
			        @Override
			        public void process(final Pair<TO<?>, NodeVersionData> value) {
				        eventListener.onNodeVersion(value.getFirst(), value.getSecond());
			        }
		        }));
		dissectors.put(GenericMessageTypes.WSN_NHT, new WSNNHTDissector(new UnaryFunctor<Pair<TO<?>, NhtInfoDO>>() {
			@Override
			public void process(final Pair<TO<?>, NhtInfoDO> value) {
				try {
					nhtInfo.addData((Integer) value.getFirst().getID(), value.getSecond());
				} catch (Exception e) {
					log.warn("Couldn't store NHT info message", e);
				}
			}
		}));
		dissectors.put(GenericMessageTypes.POWER_RACK_CONF, new PowerRackConfDissector(
		        new UnaryFunctor<Pair<TO<?>, SandcrawlerConfData>>() {

			        @Override
			        public void process(Pair<TO<?>, SandcrawlerConfData> value) {
				        eventListener.onPowerPackConf(value.getFirst(), value.getSecond());

			        }
		        }));
		dissectors.put(GenericMessageTypes.JAWA_DATA, new PlugDataDissector(new UnaryFunctor<Pair<TO<?>, PlugData>>() {

			@Override
			public void process(Pair<TO<?>, PlugData> value) {
				eventListener.onPlugData(value.getFirst(), value.getSecond());
			}
		}));
		dissectors.put(GenericMessageTypes.PHASE_DATA, new PhaseDataDissector(
		        new UnaryFunctor<Pair<TO<?>, PhaseData>>() {

			        @Override
			        public void process(Pair<TO<?>, PhaseData> value) {
				        eventListener.onPhaseData(value.getFirst(), value.getSecond());
			        }
		        }));
		dissectors.put(GenericMessageTypes.DUO_CONF, new DuoConfDissector(new UnaryFunctor<Pair<TO<?>, DuoConfData>>() {

			@Override
			public void process(Pair<TO<?>, DuoConfData> value) {
				eventListener.onDuoConf(value.getFirst(), value.getSecond());
			}
		}));
		dissectors.put(GenericMessageTypes.DUO_DATA, new DuoDataDissector(new UnaryFunctor<Pair<TO<?>, PhaseData>>() {

			@Override
			public void process(Pair<TO<?>, PhaseData> value) {
				eventListener.onPhaseData(value.getFirst(), value.getSecond());
			}
		}));
	}

	@PostConstruct
	public void init() {
		log.debug("init start");
		// call to cacheSvc will put this thread on hold while CacheSvc is
		// starting
		if (cacheSvc.getStatus() != Status.ONLINE) {
			// should never happen
			log.error("JMSClinet started before CacheSvc initialization");
		}
		log.debug("init complete");
	}

	@EJB(beanName="GenericEnvironment")
	public void setEnv(final Environment env) {
		// supposing that we have started cache service
		this.env = CacheEnvironmentDispatcher.getEnvironment(CacheSvcImpl.getInstance(), env);
	}

	public Environment getEnv() {
		return this.env;
	}

	public DeviceEventListener getDeviceEventListener() {
		return eventListener;
	}

	@EJB
	public void setDeviceEventListener(final DeviceEventListener eventListener) {
		this.eventListener = eventListener;
	}

	private void process(final SensedMessage msg) {
		TO<?> obj = TOFactory.getInstance().loadTO(msg.getObjectID());
		try {
			if (msg.getPayload() == null) {
				log.warn("Message payload is null (" + msg.getObjectID() + ", " + msg.getPropertyName() + ")");
				return;
			}

			if (msg.getPayload().getClass() == GenericMessage.class) {
				GenericMessage data = (GenericMessage) msg.getPayload();
				MessageDissector dissector = dissectors.get(data.getType());
				if (dissector == null) {
					log.warn("Couldn't find a dissector for the message type " + data.getType().toString());
				} else {
					dissector.process(obj, data);
				}
			} else {
				//[8941] if object has status property then check it is not disabled (=2)
				Integer status = null;
				try {
					status = env.getPropertyValue(obj, "status", Integer.class);
				} catch (PropertyNotFoundException | UnableToConvertPropertyException e) {
					// this is OK; it only means that it's not even possible
					// to disable this object
				}
				if (status == null || status != 2) {
					env.setPropertyValue(obj, msg.getPropertyName(), msg.getPayload());
				} else if (log.isDebugEnabled()) {
						log.debug("Ignoring setting property " + msg.getPropertyName() + " for disabled object " + obj);
				}
			}
		} catch (Exception e) {
			log.warn("Couldn't process message [" + msg.getObjectID() + ", " + msg.getPropertyName() + "] due to ", e);
		}
	}

	@Override
	public void onMessage(final Message msg) {
		if (!(msg instanceof ObjectMessage)) {
			log.warn("Unexpected JMS message format: " + msg.getClass().getName());
			return;
		}
		ObjectMessage om = (ObjectMessage) msg;
		try {
			Serializable data = om.getObject();
			if (!(data instanceof SensedMessage)) {
				log.warn("Unexpected JMS message payload format: " + data.getClass().getName());
				return;
			}
			process((SensedMessage) data);
		} catch (JMSException e) {
			log.warn("Unable to process JMS message", e);
		}
	}

	private void storeRoute(final RoutingData data) {
		if (data == null) {
			log.warn("Unable to store route data");
			return;
		}

		int networkId = data.getCurId();
		long nodeToBeRouted = data.getChildId();
		long nodeRouter = data.getParentId();

		try {
			ValueTO[] props = { new ValueTO("mac", nodeToBeRouted) };
			Collection<TO<?>> nodes = env.getObjects("WSNNODE", props);
			if (nodes == null) {
				log.warn("Reporting node with mac : " + nodeToBeRouted + " does not exists");
				return;
			}
			if (nodes.isEmpty()) {
				log.warn("Reporting node with mac : " + nodeToBeRouted + " does not exists");
				return;
			}
			if (nodes.size() > 1) {
				log.warn("Too many nodes with the same mac address : " + nodeToBeRouted);
				return;
			}

			TO<?> node = nodes.iterator().next();

			props = new ValueTO[] { new ValueTO("mac", nodeRouter) };
			Collection<TO<?>> routes = env.getObjects("WSNNODE", props);

			// performance concern:
			// If node doesn't exist , try to search gateway
			if (routes.isEmpty()) {
				routes.addAll(env.getObjects("WSNGATEWAY", props));
			}
			// double check
			if (routes.isEmpty()) {
				log.warn("Route with mac : " + nodeRouter + " does not exists. Node route won't be changed");
				return;
			}

			if (routes.size() > 1) {
				log.warn("Too many routes with the same mac : " + nodeRouter + ". Object list ["
				        + Arrays.toString(routes.toArray()) + "]");
				return;
			}

			TO<?> route = routes.iterator().next();

			if (!route.equals(env.getPropertyValue(node, "route", TO.class))) {
				if (log.isDebugEnabled()) {
					log.info("Updating routing info of node : " + node + "...");
				}
				env.setPropertyValue(node, "route", route);
				log.info("Updated route of the node : " + node + ". New route device is " + route);
			}

		} catch (Exception e) {
			log.warn("Can't process route with networkId = " + networkId + ", node mac = " + nodeToBeRouted
			        + ", neighbor mac = " + nodeRouter, e);
		}
	}

}
