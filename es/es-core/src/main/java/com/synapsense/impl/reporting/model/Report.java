package com.synapsense.impl.reporting.model;

import java.io.Serializable;

import com.synapsense.impl.reporting.export.ExportFormat;
import com.synapsense.service.FileWriter;
import com.synapsense.service.reporting.exception.ExportingReportingException;

public interface Report extends Serializable {

	void export(ExportFormat format, String path) throws ExportingReportingException;

	void export(ExportFormat format, String fullPath, FileWriter fileWriter) throws ExportingReportingException;

}
