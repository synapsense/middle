package com.synapsense.impl.dlimport.exceptions;

public class ConversionNotSupportedException extends RuntimeException {
	private static final long serialVersionUID = -8792145169931695492L;

	public ConversionNotSupportedException(Object from, String toType) {
		super("Cannot convert from '" + from.getClass().getName() + "' to '" + toType + "'!");
	}
}
