package com.synapsense.impl.taskscheduler.tasks;

import java.util.Collection;

import org.apache.log4j.Logger;

import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.exception.PropertyNotFoundException;
import com.synapsense.exception.UnableToConvertPropertyException;
import com.synapsense.impl.utils.CommonUtils;
import com.synapsense.service.Environment;

/**
 * Class implements status checker behavior. It takes all objects of the
 * specified type and tries to check their linked objects. At least one active
 * linked object means that owner is active.
 * 
 * @author ashabanov
 * 
 */
public class LinkedSensorsStatusChecker implements StatusChecker {
	private final static Logger logger = Logger.getLogger(LinkedSensorsStatusChecker.class);
	private Environment env;
	private String objType;

	public LinkedSensorsStatusChecker(String objType) {
		this.objType = objType;
	}

	@Override
	public void check(Environment env) {
		this.env = env;
		Collection<TO<?>> objects = env.getObjectsByType(objType);
		for (TO<?> to : objects) {
			try {
				int currentStatus = CommonUtils.nullSafeInt(env.getPropertyValue(to, "status", Integer.class));

				// check if object is disabled
				if (currentStatus != 2) {
					int status = getLinkedSensorsStatus(to);
					if (currentStatus != status) {
						env.setPropertyValue(to, "status", status);
					}
				}
			} catch (Exception e) {
				logger.error(e.getLocalizedMessage(), e);
			}
		}
	}

	private int getLinkedSensorsStatus(TO<?> to) throws ObjectNotFoundException {
		Collection<ValueTO> vtos = env.getAllPropertiesValues(to);
		for (ValueTO valueTO : vtos) {
			if (valueTO.getValue() instanceof TO<?>) {
				try {
					int st = CommonUtils.nullSafeInt(env.getPropertyValue((TO<?>) valueTO.getValue(), "status",
					        Integer.class));
					// if at least one sensor is not reporting, the object is
					// offline; if at least one sensor is disabled, the object
					// is disabled
					if (st == 0 || st == 2) {
						return st;
					}
				} catch (ObjectNotFoundException | UnableToConvertPropertyException e) {
					logger.error(e.getLocalizedMessage(), e);
				} catch (PropertyNotFoundException e) {
					logger.trace(e.getLocalizedMessage(), e);
				}
            }
		}
		// otherwise object is online
		return 1;
	}
}
