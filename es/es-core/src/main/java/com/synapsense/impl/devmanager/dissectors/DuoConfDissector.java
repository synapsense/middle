package com.synapsense.impl.devmanager.dissectors;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.synapsense.dto.TO;
import com.synapsense.plugin.messages.DuoConfBuilder.DuoRpduFields;
import com.synapsense.plugin.messages.DuoConfBuilder.FieldConstants;
import com.synapsense.plugin.messages.GenericMessage;
import com.synapsense.plugin.messages.GenericMessage.GenericMessageTypes;
import com.synapsense.service.impl.messages.base.DuoConfData;
import com.synapsense.util.algo.Pair;
import com.synapsense.util.algo.UnaryFunctor;

public class DuoConfDissector implements MessageDissector {

	private static final Logger log = Logger.getLogger(PowerRackConfDissector.class);

	private final UnaryFunctor<Pair<TO<?>, DuoConfData>> functor;

	public DuoConfDissector(UnaryFunctor<Pair<TO<?>, DuoConfData>> functor) {
		this.functor = functor;
	}

	@Override
	public boolean tasteMessage(GenericMessage msg) {
		return msg.getType() == GenericMessageTypes.DUO_CONF;
	}

	@Override
	public void process(TO<?> objID, GenericMessage msg) {
		if (msg == null)
			throw new IllegalArgumentException("msg cannot be null");
		if (!tasteMessage(msg))
			throw new IllegalArgumentException("Incompatible message type");

		log.info("Received DUO configuration data for node " + objID);

		DuoConfData conf = new DuoConfData();
		conf.setChecksum((Long) msg.get(FieldConstants.CHECKSUM.name()));
		conf.setProgram((String) msg.get(FieldConstants.PROGRAM.name()));

		@SuppressWarnings("unchecked")
		ArrayList<HashMap<DuoRpduFields, Serializable>> duoRpdus = (ArrayList<HashMap<DuoRpduFields, Serializable>>) msg
		        .get(FieldConstants.DUORPDUS.name());

		for (HashMap<DuoRpduFields, Serializable> duoRpdu : duoRpdus) {
			conf.addRpduConfData((Integer) duoRpdu.get(DuoRpduFields.ID), (Boolean) duoRpdu.get(DuoRpduFields.ISDELTA));
		}

		if (log.isDebugEnabled()) {
			log.debug("DUO " + objID + ": " + conf);
		}

		functor.process(new Pair<TO<?>, DuoConfData>(objID, conf));
	}
}
