package com.synapsense.impl.auditsvc;

import java.security.Principal;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.SessionContext;
import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;

import org.apache.log4j.Logger;

import com.synapsense.impl.alerting.macros.LocalizeMacro;
import com.synapsense.impl.alerting.macros.Macro;
import com.synapsense.impl.alerting.macros.MacroContext;
import com.synapsense.service.LocalizationService;
import com.synapsense.service.UserManagementService;
import com.synapsense.service.impl.audit.Auditor;
import com.synapsense.service.user.CachedUserContext;
import com.synapsense.service.user.EnvUserContext;

public class AuditorInterceptor {
	private final static Logger logger = Logger.getLogger(AuditorInterceptor.class);

	@AroundInvoke
	@SuppressWarnings("unchecked")
	public Object intercept(final InvocationContext icx) throws Exception {
		if (logger.isDebugEnabled()) {
			logger.debug("Entering interceptor : " + AuditorInterceptor.class.getName());
		}

		final Object result = icx.proceed();
		if (result == null)
			return result;

		// find out that caller is not anonymous
		final Principal user = ctx.getCallerPrincipal();
		final String callerName = user.getName();
		macroCtx.usrCtx = new CachedUserContext(new EnvUserContext(callerName, userService));

		// replace lastStatus
		Map<String, Auditor>  auditorsMap = (Map<String, Auditor>) result;
		
		for (Auditor auditor : auditorsMap.values()) {
			String lastStatus = auditor.getLastStatus();
			auditor.setLastStatus(macro.expandIn(lastStatus, macroCtx));
		}

		if (logger.isDebugEnabled()) {
			logger.debug("Exitting interceptor : " + AuditorInterceptor.class.getName());
		}

		return auditorsMap;
	}

	private void initMacroCtx() {
		logger.info("interceptor init ... ");

		macro = new LocalizeMacro();
		macroCtx = new MacroContext();
		macroCtx.localizationService = localizationService;

		logger.info("interceptor init is done");
	}

	@Resource
	void setCtx(final SessionContext ctx) {
		this.ctx = ctx;
	}


	@EJB(beanName = "LocalUserManagementService")
	void setUserService(final UserManagementService userService) {
		this.userService = userService;
	}

	@EJB
	void setLocalizationService(final LocalizationService localizationService) {
		this.localizationService = localizationService;
		initMacroCtx();
	}

	private SessionContext ctx;
	
	private LocalizationService localizationService;

	private UserManagementService userService;

	private Macro macro;

	private MacroContext macroCtx;

}
