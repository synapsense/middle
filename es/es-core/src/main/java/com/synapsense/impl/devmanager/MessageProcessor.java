package com.synapsense.impl.devmanager;

import java.io.Serializable;

interface MessageProcessor<T extends Serializable> {
	void process(T msg);
}
