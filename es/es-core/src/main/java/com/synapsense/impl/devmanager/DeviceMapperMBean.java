package com.synapsense.impl.devmanager;

public interface DeviceMapperMBean {

	String getSensorsMapping();
}
