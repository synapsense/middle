package com.synapsense.impl.alerting;

import com.synapsense.dto.TO;

public interface AlertDismissor {

	/**
	 * Schedule dismissing of <code>alertTO</code> delayed for
	 * <code>dismissInterval</code>
	 * 
	 * @param alertTO
	 *            Alert ID to dismiss
	 * @param dismissInterval
	 *            Dismissing delay
	 */
	void scheduleAlertDismission(TO<?> alertTO, long dismissInterval);

    void purgeAll();
}
