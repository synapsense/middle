package com.synapsense.impl.utils;

import java.util.Collection;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.log4j.Logger;

import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;
import com.synapsense.service.Environment;

public class ConfigurationDataUtils {
	private static final Logger LOG = Logger.getLogger(ConfigurationDataUtils.class);
	private static final String envProxyName = "java:global/es-ear/es-core/Environment";
	private static final String CONFIGURATION_DATA_TYPE_NAME = "CONFIGURATION_DATA";
	private static final String CONFIGURATION_PROPERTY_NAME = "config";

	private static Environment env;

	public static String getConfigurationString(String confDataName) {
		String configurationString = null;
		try {
			TO<?> configDataTO = getConfigurationDataObject(confDataName);
			if (configDataTO != null) {
				configurationString = getEnv()
				        .getPropertyValue(configDataTO, CONFIGURATION_PROPERTY_NAME, String.class);
			}
		} catch (Exception e) {
			LOG.error(e.getLocalizedMessage(), e);
		}
		return configurationString;
	}

	public static TO<?> getConfigurationDataObject(String confDataName) {
		TO<?> configDataTO = null;
		try {
			ValueTO[] vals = { new ValueTO("name", confDataName) };
			Collection<TO<?>> ctos = getEnv().getObjects(CONFIGURATION_DATA_TYPE_NAME, vals);
			if (!ctos.isEmpty()) {
				configDataTO = ctos.iterator().next();
			}
		} catch (Exception e) {
			LOG.error(e.getLocalizedMessage(), e);
		}
		return configDataTO;
	}

	private static Environment getEnv() {
		if (env == null) {
			try {
				InitialContext ctx = new InitialContext();
				env = (Environment) ctx.lookup(envProxyName);
			} catch (NamingException e) {
				throw new RuntimeException("Cannot obtain Environment proxy", e);
			}
		}
		return env;
	}
}
