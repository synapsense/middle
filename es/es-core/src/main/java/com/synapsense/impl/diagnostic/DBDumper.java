package com.synapsense.impl.diagnostic;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.synapsense.impl.dlimport.XMLHelper;

public class DBDumper {
	private static final Logger LOG = Logger.getLogger(DBDumper.class);
	final private static String FILE_STORAGE = "com.synapsense.filestorage";
	final private static String SUPPORT_INFO_FILE_NAME = "support-info.xml";
	private static final String DUMP_FILE_EXTENSION = ".dump_txt";

	// private static String[] tables = new String[]{"object_types", "objects"};
	private static final String[] DEFAULT_TABLES = new String[] { "object_types", "objects" };
	private static List<String> tables = new ArrayList<String>();
	private static Map<String, TableRestrictor> tblRestrictors = new HashMap<String, TableRestrictor>();

	public static void dumpTables(String folderPath) throws IOException {
		LOG.info("dumpTables started.");
		try {
			loadDataFromConfig();

			for (String tblFileNames : getTableFiles()) {// just in case delete
				                                         // temporary dump table
				                                         // files if they were
				                                         // not previously
				                                         // deleted for some
				                                         // reason...
				new File(folderPath + File.separator + tblFileNames).delete();
			}

			SQLStatementExecutor sqlExecutor = new SQLStatementExecutor();
			try {
				sqlExecutor.prepareConnection();
				for (String tblName : tables) {
					String tblDumpFileName = getFileNameByTableName(tblName);
					String currentDir = new File(folderPath).getAbsolutePath().replace("\\", "/") + "/";

					try {
						String restriction = getRestriction(tblName);

						sqlExecutor.executeSQLStatement("SELECT * INTO OUTFILE '" + currentDir + tblDumpFileName
						        + "' from " + tblName + restriction + ";");
					} catch (Exception e) {
						// Something wrong with one table dump. Log exception
						// and continue to dump other tables.
						LOG.error(e.getLocalizedMessage(), e);
					} finally {
						sqlExecutor.closeResultSet();
					}
				}
			} catch (Exception e) {
				LOG.error(e.getLocalizedMessage(), e);
			} finally {
				sqlExecutor.closeConnection();
			}
		} catch (Exception e) {
			LOG.error(e.getLocalizedMessage(), e);
		}
		LOG.info("dumpTables finished.");
	}

	public static void writeTableCreationScriptToFile(String fileName) throws IOException {
		FileWriter fw = new FileWriter(fileName);
		SQLStatementExecutor sqlExecutor = new SQLStatementExecutor();
		try {
			fw.write("create database if not exists `synap`" + "\n");
			fw.write("character set utf8;" + "\n");
			fw.write("\n" + "use `synap`;" + "\n");
			fw.write("\n" + "set foreign_key_checks=0;" + "\n");

			sqlExecutor.prepareConnection();
			for (String tblName : tables) {
				String queryStr = "SHOW CREATE TABLE " + tblName + ";";
				try {
					ResultSet rs = sqlExecutor.executeSQLStatement(queryStr);
					if (rs.next()) {
						String createTableStr = rs.getString(2) + ";";
						String dropTblStr = "drop table if exists `" + tblName + "`;";

						fw.write(dropTblStr + "\n" + createTableStr + "\n");
					} else {
						LOG.info("Show create has no result for " + tblName);
					}
				} catch (Exception e) {
					// Something wrong with one table. Log exception and
					// continue.
					LOG.error(e.getLocalizedMessage(), e);
				} finally {
					sqlExecutor.closeResultSet();
				}
			}
			fw.write("\n" + "set foreign_key_checks=1;" + "\n");

		} catch (Exception e) {
			LOG.error(e.getLocalizedMessage(), e);
		} finally {
			sqlExecutor.closeConnection();
			fw.close();
		}
	}

	public static Collection<String> getTableFiles() {
		Collection<String> result = new ArrayList<String>();
		for (String tblName : tables) {
			result.add(getFileNameByTableName(tblName));
		}
		return result;
	}

	public static String getMysqlImportStr() {
		String fileNames = "";
		for (String tblName : tables) {
			fileNames += " " + getFileNameByTableName(tblName);
		}
		return "mysqlimport -u<user_name> -p<password> --local <database_name>" + fileNames;
	}

	private static String getRestriction(String tableName) {
		String restriction = "";
		if (tblRestrictors.containsKey(tableName)) {
			restriction = " " + tblRestrictors.get(tableName).generateRestriction();
		}
		return restriction;
	}

	private static String getFileNameByTableName(String tableName) {
		return tableName + DUMP_FILE_EXTENSION;
	}

	private static void loadDataFromConfig() {
		tables.clear();
		tblRestrictors.clear();

		try {
			String storageFileName = System.getProperty(FILE_STORAGE);
			if (storageFileName == null || storageFileName.isEmpty())
				throw new Exception("Property [" + FILE_STORAGE + "] is not set");

			String fileName = storageFileName + File.separator + SUPPORT_INFO_FILE_NAME;
			Document confDoc = XMLHelper.loadXmlFromFile(fileName);
			XPathFactory factoryX = XPathFactory.newInstance();
			XPath xPath = factoryX.newXPath();

			// Load table names
			XPathExpression xPathTableNames = xPath.compile("support/tables/@value");
			String tableNames = xPathTableNames.evaluate(confDoc);
			LOG.info("TableNames for dump: " + tableNames);

			if (tableNames != null && !tableNames.isEmpty()) {
				String[] tblNamesArray = tableNames.split(",");
				for (String tbl : tblNamesArray) {
					tables.add(tbl.trim());
				}
			}

			// Load table restrictions
			XPathExpression xPathTableRestrictions = xPath.compile("support/table_restrictions/table");
			XPathExpression xPathTblName = xPath.compile("@tbl_name");
			XPathExpression xPathDaysLimit = xPath.compile("@days_limit");
			XPathExpression xPathTimestampColumn = xPath.compile("@timestampColumn");
			NodeList restrictions = (NodeList) xPathTableRestrictions.evaluate(confDoc, XPathConstants.NODESET);
			if (restrictions != null) {
				for (int i = 0; i < restrictions.getLength(); i++) {
					Node currRestr = restrictions.item(i);
					String tableName = xPathTblName.evaluate(currRestr);
					TableRestrictor tblRestr = new TableRestrictor(tableName, xPathDaysLimit.evaluate(currRestr),
					        xPathTimestampColumn.evaluate(currRestr));
					tblRestrictors.put(tableName, tblRestr);
				}
			}
		} catch (Exception e) {
			LOG.error(e.getLocalizedMessage(), e);

			// add default tables
			LOG.info("Loading from config file was failed. Adding default tables...");
			tables.clear();
			for (String tbl : DEFAULT_TABLES) {
				tables.add(tbl);
			}
		}
	}

	/*
	 * Methods for "mysqldump"
	 */

	/*
	 * public static void dump(String destinationFileName) throws IOException{
	 * try{ LOG.info("Started.");
	 * 
	 * Runtime rt = Runtime.getRuntime(); String cmdStr = ""; String mySqlPath =
	 * getMySqlBinPath(); String userPwd = "dbu$er"; String userName = "dbuser";
	 * String dbName = "synap";
	 * 
	 * cmdStr = mySqlPath + "mysqldump"; cmdStr += " -u" + userName + " -p" +
	 * userPwd + " " + dbName; cmdStr += getTables(); cmdStr +=
	 * " --result-file=\"" + destinationFileName + "\""; LOG.info("cmd string: "
	 * + cmdStr);
	 * 
	 * Process proc = rt.exec(cmdStr.toString());
	 * 
	 * StreamGobbler errorGobbler = new StreamGobbler(proc.getErrorStream(),
	 * "ERROR"); StreamGobbler outputGobbler = new
	 * StreamGobbler(proc.getInputStream(), "OUTPUT");
	 * 
	 * errorGobbler.start(); outputGobbler.start();
	 * 
	 * int exitVal = proc.waitFor(); LOG.info("ExitValue: " + exitVal);
	 * 
	 * }catch(Exception e){ LOG.error(e.getLocalizedMessage(), e); }
	 * LOG.info("Finished."); }
	 * 
	 * private static String getMySqlBinPath(){ String result = ""; String
	 * mySqlServiceName = "MySQL"; String imagePath =
	 * RegistryHelper.readValue(RegistryHelper.SERVICE_KEY + mySqlServiceName,
	 * "ImagePath", RegistryHelper.REGEXPANDSZ_TOKEN); if(imagePath != null){
	 * int indx = imagePath.indexOf("bin"); if(indx != -1){ result =
	 * imagePath.substring(0, indx + 3).replace("\"", "").replace("\\", "/") +
	 * "/"; } } return result; }
	 * 
	 * private static String getTables(){ String result = ""; for(String tblName
	 * : tables){ result += " " + tblName; } return result; }
	 */
}
