/**
 * 
 */
package com.synapsense.impl.taskscheduler.tasks;

/**
 * @author aborisov
 * 
 */
public interface BatteryTask {

	void execute();

}
