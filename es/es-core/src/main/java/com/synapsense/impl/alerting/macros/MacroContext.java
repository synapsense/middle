package com.synapsense.impl.alerting.macros;

import java.util.Map;

import com.synapsense.dto.Alert;
import com.synapsense.service.Environment;
import com.synapsense.service.LocalizationService;
import com.synapsense.service.user.UserContext;
import com.synapsense.util.CollectionUtils;
import com.synapsense.util.ConvertingEnvironmentProxy;
import com.synapsense.util.unitconverter.UnitSystems;

/**
 * Represents context (that is, data source) for macros expansion.
 * 
 * @author Oleg Stepanov
 * 
 */
public class MacroContext {

	public Environment env;
	public ConvertingEnvironmentProxy convEnv;
	public UserContext usrCtx;
	public UnitSystems unitSystems;
	public LocalizationService localizationService;

	public Alert alert;
	public Map<String, Object> props;

	public MacroContext() {
		props = CollectionUtils.newMap();
	}

	public MacroContext(final MacroContext copy) {
		this.env = copy.env;
		this.convEnv = copy.convEnv;
		this.usrCtx = copy.usrCtx;
		this.unitSystems = copy.unitSystems;
		this.localizationService = copy.localizationService;
		this.alert = copy.alert;

		this.props = CollectionUtils.newMap();
		if (copy.props != null)
			this.props.putAll(copy.props);
	}

	public boolean isAnonymous() {
		return usrCtx == null;
	}
}
