package com.synapsense.impl.reporting.commands;

import java.util.Collection;
import java.util.Properties;

import com.synapsense.impl.reporting.dao.ReportDAO;
import com.synapsense.impl.reporting.dao.ReportTemplateDAO;
import com.synapsense.impl.reporting.export.ExportFormat;
import com.synapsense.service.reporting.dto.ReportInfo;
import com.synapsense.service.reporting.dto.ReportTaskInfo;

public final class CommandFactory {

	public static ReportGenerationCommand newReportGenerationCommand(ReportTaskInfo reportTask, ReportInfo reportInfo,
	        ReportTemplateDAO rtDAO, ReportDAO reportDAO) {
		return new ReportGenerationCommand(reportTask, reportInfo, rtDAO, reportDAO);
	}

	public static ReportExportCommand newReportExportCommand(ReportDAO reportDAO, ReportInfo reportInfo,
	        ExportFormat format) {
		return new ReportExportCommand(reportInfo, format, reportDAO);
	}

	public static ReportMailCommand newReportMailCommand(ReportDAO reportDAO,
			Collection<String> destinations, ReportInfo reportInfo) {
		return new ReportMailCommand(reportDAO, destinations, reportInfo);
	}

	private CommandFactory() {
	}
}
