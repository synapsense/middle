package com.synapsense.impl.fws;

import java.io.IOException;

import com.synapsense.service.FileWriter;

public class FileWriterFrontend implements FileWriter {
	private FileWriter impl;

	public void setImpl(FileWriter impl) {
		this.impl = impl;
	}

	@Override
	public String readFile(String fileName) throws IOException {
		return impl.readFile(fileName);
	}

	@Override
	public void writeFile(String fileName, String data, WriteMode mode) throws IOException {
		impl.writeFile(fileName, data, mode);
	}

	@Override
	public byte[] readFileBytes(String fileName) throws IOException {
		return impl.readFileBytes(fileName);
	}

	@Override
	public void writeFileBytes(String fileName, byte[] data, WriteMode mode) throws IOException {
		impl.writeFileBytes(fileName, data, mode);
	}

	@Override
	public void deleteFile(String fileName) throws IOException {
		impl.deleteFile(fileName);
	}

	@Override
	public byte[] readFileBytes(String fileName, int offset) throws IOException {
		return impl.readFileBytes(fileName, offset);
	}

}
