package com.synapsense.impl.alerting.notification;

import com.synapsense.dto.Message;
import com.synapsense.dto.MessageType;
import com.synapsense.impl.alerting.macros.Macro;
import com.synapsense.impl.alerting.macros.MacroContext;

public interface MessageTemplate {
	Message getMessage(Macro macro, MacroContext macroCtx, Destination dest, String priority, MessageType messageType);
}