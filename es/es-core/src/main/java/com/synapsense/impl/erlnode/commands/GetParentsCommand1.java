
package com.synapsense.impl.erlnode.commands;

import java.util.Map;

import com.ericsson.otp.erlang.OtpErlangObject;
import com.ericsson.otp.erlang.OtpErlangTuple;
import com.synapsense.impl.erlnode.ExceptionTypes;
import com.synapsense.impl.erlnode.utils.ToConverter;
import com.synapsense.dto.TO;
import com.synapsense.exception.ObjectNotFoundException;

/**
 * @author stikhon
 * 
 */
public class GetParentsCommand1 extends Command {

	public GetParentsCommand1(Map<String, Object> state) {
		super(state);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see Command#do_command(com.ericsson.otp.erlang.OtpErlangTuple)
	 */
	@Override
	public OtpErlangObject do_command(OtpErlangTuple command) throws Exception {

		OtpErlangObject toTuple = command.elementAt(1);
		if (toTuple == null || !(toTuple instanceof OtpErlangTuple)) {
			return makeException(ExceptionTypes.ILLEGAL_ARGUMENTS, null);
		}
		TO<?> to = ToConverter.parseTo((OtpErlangTuple) toTuple);
		try {
			return makeOkResult(ToConverter.serializeToList(getEnv()
					.getParents(to)));
		} catch (ObjectNotFoundException e) {
			return makeException(ExceptionTypes.OBJECT_NOT_FOUND, e.getMessage());
		}
	}
}
