package com.synapsense.impl.alerting.notification;

import java.util.Date;

import com.synapsense.impl.alerting.macros.MacroContext;

public interface Notification {
	static class ProcessResult{
		String tier;
		int na;
		boolean isTierChanged;
	}
	String getId();

	Date getLastFireTime();

	Date getNextFireTime();

	boolean isActive();

	ProcessResult process(NotificationManager ctx);

	NotificationTemplate generatedBy();

	String currentState();

	MacroContext getContext();
}