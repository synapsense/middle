package com.synapsense.impl.reporting.tasks;

/**
 * The <code>ReportingTask</code> interface defines common
 * functionality for all reporting tasks which execute DB queries
 * 
 * @author Grigory Ryabov
 */
public interface ReportingTask{
	
	/**
	 * Executes the task.
	 *
	 */
	void execute();

	/**
	 * Gets task name.
	 *
	 * @return task name
	 */
	String getName();
}
