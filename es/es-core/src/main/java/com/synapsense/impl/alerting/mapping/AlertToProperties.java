package com.synapsense.impl.alerting.mapping;

import java.util.Date;
import java.util.List;

import com.synapsense.dto.Alert;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.dto.ValueTO;
import com.synapsense.impl.alerting.Utils;
import com.synapsense.service.Environment;
import com.synapsense.util.CollectionUtils;
import com.synapsense.util.CollectionUtils.Functor;

public class AlertToProperties implements Functor<List<ValueTO>, Alert> {

	private Environment env;
	
	public AlertToProperties(Environment env) {
	    this.env = env;
    }

	@Override
	public List<ValueTO> apply(Alert alert) {
		List<ValueTO> values = CollectionUtils.newArrayList(10);
		values.add(new ValueTO("name", alert.getName()));

		TO<?> alertTypeRef =Utils.findAlertTypeByName(env, alert.getAlertType());
		if (alertTypeRef == null) {
			throw new MappingException("Alert type  :" + alert.getAlertType() + " does not exist");
		}
		values.add(new ValueTO("type", alertTypeRef));
		values.add(new ValueTO("time", alert.getAlertTime()));
		values.add(new ValueTO("status", alert.getStatus().code()));
		values.add(new ValueTO("message", alert.getMessage()));
		values.add(new ValueTO("fullMessage", alert.getFullMessage()));
		values.add(new ValueTO("hostTO", TOFactory.EMPTY_TO.equals(alert.getHostTO()) ? null : alert
		        .getHostTO()));
		values.add(new ValueTO("acknowledgement", alert.getAcknowledgement()));
		Date ackTime = alert.getAcknowledgementTime();
		values.add(new ValueTO("acknowledgementTime", ackTime == null ? null : ackTime.getTime()));
		values.add(new ValueTO("user", alert.getUser()));

		return values;
	}
}
