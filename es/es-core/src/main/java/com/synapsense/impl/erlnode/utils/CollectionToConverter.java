package com.synapsense.impl.erlnode.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.ericsson.otp.erlang.OtpErlangAtom;
import com.ericsson.otp.erlang.OtpErlangList;
import com.ericsson.otp.erlang.OtpErlangObject;
import com.ericsson.otp.erlang.OtpErlangTuple;

import com.synapsense.dto.CollectionTO;
import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;

import com.synapsense.service.Environment;

/**
 * @author stikhon
 * 
 */
public class CollectionToConverter {
	public final static String COLLECTION_TO_ATOM_ID = "collection_to";

	public static CollectionTO parseCollectionTo(Environment env, String objType, OtpErlangTuple tuple) {
		if (tuple == null || tuple.arity() != 3 || !(COLLECTION_TO_ATOM_ID.equals(tuple.elementAt(0).toString()))
		        || !(tuple.elementAt(1) instanceof OtpErlangTuple) || !(tuple.elementAt(2) instanceof OtpErlangList)) {
			throw new IllegalArgumentException();
		}
		OtpErlangTuple toTuple = (OtpErlangTuple) tuple.elementAt(1);
		OtpErlangList valueToList = (OtpErlangList) tuple.elementAt(2);
		TO<?> to = ToConverter.parseTo(toTuple);
		List<ValueTO> valueTOs;
		if (valueToList.arity() == 0) {
			valueTOs = new ArrayList<ValueTO>();
		} else {
			valueTOs = new ArrayList<ValueTO>(ValueTOConverter.parseValueTOList(env, objType, valueToList));
		}
		return new CollectionTO(to, valueTOs);
	}

	public static OtpErlangTuple serializeCollectionTo(CollectionTO collectionTo) {
		if (collectionTo == null) {
			throw new IllegalArgumentException();
		}
		OtpErlangTuple toTuple = ToConverter.serializeTo(collectionTo.getObjId());
		OtpErlangList valueToList;
		if (collectionTo.getPropValues().isEmpty()) {
			valueToList = new OtpErlangList();
		} else {
			valueToList = ValueTOConverter.serializeValueTOList(collectionTo.getPropValues());
		}

		return new OtpErlangTuple(new OtpErlangObject[] { new OtpErlangAtom(COLLECTION_TO_ATOM_ID), toTuple,
		        valueToList });
	}

	public static OtpErlangList serializeCollectionToList(Collection<CollectionTO> list) {
		if (list == null) {
			throw new IllegalArgumentException();
		}
		ArrayList<OtpErlangTuple> objectsList = new ArrayList<OtpErlangTuple>();
		for (CollectionTO to : list) {
			objectsList.add(serializeCollectionTo(to));
		}
		return new OtpErlangList(objectsList.toArray(new OtpErlangObject[objectsList.size()]));
	}

	/**
	 * Converts OtpErlangObject to OtpErlangList
	 * 
	 * @param otpList
	 * @return converted OtpErlangList object in case of successful conversion,
	 *         null otherwise
	 */
	public static OtpErlangList checkList(OtpErlangObject otpList) {
		if (otpList != null && otpList instanceof OtpErlangList) {
			return (OtpErlangList) otpList;
		} else {
			return null;
		}
	}

}
