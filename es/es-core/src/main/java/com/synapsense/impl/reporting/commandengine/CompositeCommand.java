package com.synapsense.impl.reporting.commandengine;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Stack;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.impl.reporting.commandengine.monitoring.DefaultStatusMonitor;
import com.synapsense.impl.reporting.commandengine.monitoring.StatusMonitor;
import com.synapsense.service.reporting.dto.CommandStatus;
import com.synapsense.service.reporting.dto.ReportInfo;

public class CompositeCommand implements Command {

	private static final Log log = LogFactory.getLog(CompositeCommand.class);

	private List<Command> commands;

	private Stack<Command> commandStack;

	private boolean interrupted = false;

	private boolean paused = false;

	private ReentrantLock pauselock;

	private Condition pauseCondition;

	public CompositeCommand() {
		commands = new ArrayList<Command>();
		commandStack = new Stack<Command>();
		pauselock = new ReentrantLock();
		pauseCondition = pauselock.newCondition();
	}

	@Override
	public String getName() {
		return "Composite Command";
	}

	public void addCommand(Command command) {
		commands.add(command);
	}

	public List<Command> getCommands() {
		return Collections.unmodifiableList(commands);
	}

	public int getCommandsSize() {
		return commands.size();
	}

	public int completed() {
		if (commandStack.isEmpty()) {
			return 0;
		}
		return commandStack.size() - 1;
	}

	public Command getRunningCommand() {
		return commandStack.peek();
	}

	@Override
	public String getDescription() {
		StringBuilder descrBuilder = new StringBuilder().append("Composite command: ");
		for (Command command : commands) {
			descrBuilder.append("\n").append(command.getDescription()).append("  ");
		}
		return descrBuilder.toString();
	}
	
	@Override
	public ReportInfo getReportInfo() {
		for (Command command : commands) { 
			return command.getReportInfo();
		}
		return null;
	}

	@Override
	public void run(final StatusMonitor statusMonitor) {
		statusMonitor.setStatus(CommandStatus.inProgress("Starting"));
		for (Command command : commands) {

			if (interrupted) {
				statusMonitor.setStatus(CommandStatus.cancelled("Command has been cancelled"));
				return;
			}

			if (paused) {
				statusMonitor.setStatus(CommandStatus.suspended("Command suspended"));
				try {
					pauselock.lock();
					try {
						pauseCondition.await();
					} catch (InterruptedException e) {
						return;
					}
				} finally {
					pauselock.unlock();
				}
			}

			commandStack.add(command);

			statusMonitor.setStatus(CommandStatus.inProgress("Running command " + command.getName()));

			StatusMonitor curMonitor = new DefaultStatusMonitor() {
				public void setProgress(double progress) {
					super.setProgress(progress);
					statusMonitor.setProgress((commandStack.size() * 1.0 - 1.0 + progress) / commands.size() * 1.0);
				}
			};
			try {
				if (log.isDebugEnabled()) {
					log.debug("Running command '" + command.getName() + "'");
				}
				command.run(curMonitor);
			} catch (RuntimeException e) {
				statusMonitor.setStatus(CommandStatus.failed(curMonitor.getStatus().getStatusMessage()));
				throw e;
			}
			statusMonitor.setProgress(commandStack.size() * 1.0 / commands.size() * 1.0);
		}

		statusMonitor.setStatus(CommandStatus.completed("Composite command completed successfully"));
	}

	@Override
	public void rollback() {
		while (!commandStack.isEmpty()) {
			commandStack.pop().rollback();
		}
	}

	@Override
	public void interrupt() {
		interrupted = true;
		if (!commandStack.isEmpty()) {
			try {
				commandStack.peek().interrupt();
			} catch (RuntimeException e) {
				if (log.isDebugEnabled()) {
					log.debug("Failed to interrupt command '" + commandStack.peek().getName() + "'");
				}
			}
		}
	}

	@Override
	public boolean pause() {
		paused = true;
		return true;
	}

	@Override
	public void renew() {
		paused = false;
		pauseCondition.signal();
	}
}
