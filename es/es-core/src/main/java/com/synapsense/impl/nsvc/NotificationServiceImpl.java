package com.synapsense.impl.nsvc;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.Local;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import org.apache.log4j.Logger;

import com.synapsense.cdi.MBean;
import com.synapsense.impl.nsvc.compressor.EventCompressor;
import com.synapsense.impl.nsvc.compressor.ReferencedObjectChangedEventCompressor;
import com.synapsense.service.impl.nsvc.EventNotifier;
import com.synapsense.service.impl.nsvc.NotificationServiceInternal;
import com.synapsense.service.nsvc.Event;
import com.synapsense.service.nsvc.EventProcessor;
import com.synapsense.service.nsvc.NotificationService;
import com.synapsense.service.nsvc.events.DispatchingProcessor;
import com.synapsense.service.nsvc.filters.CompositeFilter;

/**
 * Session Bean implementation class NotificationServiceBean
 */
// @Service(name = "NotificationService")
// @Remote(NotificationService.class)
// @Management(NotificationServiceMgmt.class)
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
@Startup @Singleton(name="LocalNotificationService")
@Local({NotificationService.class, EventNotifier.class})
@ConcurrencyManagement(ConcurrencyManagementType.BEAN)
@MBean("com.synapsense:type=NotificationService")
public class NotificationServiceImpl implements NotificationServiceInternal, NotificationServiceImplMBean {

	private static final Logger log = Logger.getLogger(NotificationServiceImpl.class);

	private Map<EventProcessor, EventProcessor> processors = new ConcurrentHashMap<EventProcessor, EventProcessor>();

	/**
	 * Class for coupling of compressor and its timer. Since we want instances
	 * of this class to use outer class's sendEvents method, this class is made
	 * abstract and its anonymous local instances are returned by
	 * newScheduledCompressor method. Note that the class itself can be static.
	 * 
	 * @author Oleg Stepanov
	 * 
	 */
	private static abstract class ScheduledCompressor extends TimerTask {
		protected Timer timer;
		protected final EventCompressor compressor;
		protected long idlePeriod;

		public ScheduledCompressor(EventCompressor c) {
			if (c == null)
				throw new IllegalArgumentException("EventCompressor arg must not be null");
			compressor = c;
		}

		public long getIdlePeriod() {
			return idlePeriod;
		}

		/**
		 * Set period for compressor to accumulate events.
		 * 
		 * @param period
		 *            accumulation period. 0 means stop scheduling.
		 */
		public void setIdlePeriod(long period) {
			if (idlePeriod == period)
				return;
			idlePeriod = period;
			if (timer != null) {
				timer.cancel();
				timer = null;
			}
			if (idlePeriod == 0)
				return;
			timer = new Timer("Timer for " + compressor.getName(), true);
			timer.schedule(this, idlePeriod, idlePeriod);
		}

	}

	private ScheduledCompressor newScheduledCompressor(EventCompressor c) {
		return new ScheduledCompressor(c) {
			@Override
			public void run() {
				// just grab events and send them via outer class. easy, huh?
				sendEvents(compressor.getCompressedEvents());
			}
		};
	}

	private static final long DEFAULT_COMPRESSOR_IDLE_PERIOD = 500;

	private Map<String, ScheduledCompressor> compressors = new HashMap<String, ScheduledCompressor>();

	private EventProcessor compositeCompressor;

	private volatile boolean isStarted;

	@Override
	@PostConstruct
	public void create() throws Exception {
		start();
	}

	@Override
	public void destroy() throws Exception {
	}

	@Override
	public void start() throws Exception {
		// this dispatcher is for known compressors
		DispatchingProcessor compressorsDispatcher = new DispatchingProcessor(false);

		// create known event compressors, plug each one into dispatcher
		// and add into compressors' map
		ReferencedObjectChangedEventCompressor c = new ReferencedObjectChangedEventCompressor();
		compressorsDispatcher.putRocep(c);
		compressors.put(c.getName(), newScheduledCompressor(c));
		// IMPLEMENTATION NOTE: new compressors can be added as above

		// create processor for events without compressors
		EventProcessor directSendProcessor = new EventProcessor() {
			@Override
			public Event process(Event e) {
				sendEvent(e);
				return e;
			}
		};

		// combine compressors dispatcher and direct processor via OR filter:
		// compressors dispatcher has its bypass set to false thus all events
		// without installed compressor will be routed by OR filter into direct
		// processor
		CompositeFilter orFilter = new CompositeFilter(compressorsDispatcher);
		orFilter.or(directSendProcessor);
		compositeCompressor = orFilter;

		// set started to true (it guards compressors map from being used by
		// setCompressorIdlePeriod method but at this point we're done
		// with the map itself)
		isStarted = true;
		// schedule compressors:
		for (String compressorName : compressors.keySet()) {
			setIdlePeriod(compressorName, DEFAULT_COMPRESSOR_IDLE_PERIOD);
		}

		log.info("Started");
	}

	@Override
	@PreDestroy
	public void stop() throws Exception {
		// stop compressors:
		for (String compressorName : compressors.keySet()) {
			setIdlePeriod(compressorName, 0);
		}
		compressors.clear();
		compositeCompressor = null;
		isStarted = false;
		log.info("Stopped");
		destroy();
	}

	private ScheduledCompressor checkedGetCompressor(String compressorName) {
		if (!isStarted)
			throw new IllegalStateException("Service is not running");
		ScheduledCompressor c = compressors.get(compressorName);
		if (c == null)
			throw new IllegalArgumentException("Cannot find compressor: " + compressorName);
		return c;
	}

	@Override
	public void setIdlePeriod(String compressorName, long period) {
		checkedGetCompressor(compressorName).setIdlePeriod(period);
	}

	@Override
	public long getIdlePeriod(String compressorName) {
		return checkedGetCompressor(compressorName).getIdlePeriod();
	}

	@Override
	public long getNumOfEventsReceived(String compressorName) {
		return checkedGetCompressor(compressorName).compressor.getNumOfEventsReceived();
	}

	@Override
	public long getNumOfEventsSent(String compressorName) {
		return checkedGetCompressor(compressorName).compressor.getNumOfEventsSent();
	}

	@Override
	public void resetStatistics(String compressorName) {
		checkedGetCompressor(compressorName).compressor.resetStatistics();
	}

	@Override
	public String listCompressors() {
		if (!isStarted)
			throw new IllegalStateException("Service is not running");
		StringBuilder sb = new StringBuilder();
		for (ScheduledCompressor sc : compressors.values()) {
			Double compressionRate = sc.compressor.getNumOfEventsReceived() == 0 ? 0. : 1.0
			        - (double) sc.compressor.getNumOfEventsSent() / (double) sc.compressor.getNumOfEventsReceived();
			sb.append("Compressor: ").append(sc.compressor.getName()).append("\r\n").append('\t')
			        .append("events received: ").append(sc.compressor.getNumOfEventsReceived()).append("\r\n")
			        .append('\t').append("events sent: ").append(sc.compressor.getNumOfEventsSent()).append("\r\n")
			        .append('\t').append("compression rate: ").append(compressionRate).append("\r\n");
		}
		return sb.toString();
	}

	@Override
	public void registerProcessor(EventProcessor processor, EventProcessor filter) {
		if (filter == null)
			filter = new VoidFilter();

		boolean newProcessor = false;
		if (log.isDebugEnabled()) {
			newProcessor = !processors.containsKey(processor);
		}

		processors.put(processor, filter);

		if (log.isDebugEnabled()) {
			if (newProcessor) {
				log.debug("New processor registered, processors num: " + processors.size());
			} else {
				log.debug("Processor's filter replaced, processors num: " + processors.size());
			}
		}
	}

	@Override
	public void deregisterProcessor(EventProcessor processor) {
		if (!processors.containsKey(processor))
			log.warn("Removing non-registered processor!");

		processors.remove(processor);
		if (log.isDebugEnabled())
			log.debug("Processor DEregistered, total: " + processors.size());
	}

	@Override
	public void notify(Event e) {
		if (compositeCompressor != null)
			compositeCompressor.process(e);
	}

	private void sendEvents(Collection<Event> events) {
		for (Event e : events) {
			sendEvent(e);
		}
	}

	private void sendEvent(Event e) {
		if (!isStarted)
			return;

		for (Entry<EventProcessor, EventProcessor> entry : processors.entrySet()) {
			EventProcessor filter = entry.getValue();
			long startTime = 0;
			if (log.isDebugEnabled()) {
				startTime = System.currentTimeMillis();
			}
			Event filteredEvent = filter.process(e);
			if (log.isDebugEnabled()) {
				long resultTime = System.currentTimeMillis() - startTime;
				log.debug("Event filtering took " + resultTime + " ms, filter=" + filter + ", event=" + e);
			}
			if (filteredEvent != null) {
				EventProcessor processor = entry.getKey();
				if (log.isDebugEnabled()) {
					startTime = System.currentTimeMillis();
					log.debug("Sending " + e);
				}
				try {
					processor.process(filteredEvent);
				} catch (Throwable t) {
					log.warn("Processor " + processor + " threw an exception", t);
				}
				if (log.isDebugEnabled()) {
					long resultTime = System.currentTimeMillis() - startTime;
					log.debug("Sending event took " + resultTime + " ms, event=" + e);
				}
			}
		}
	}

}

class VoidFilter implements EventProcessor {
	@Override
	public Event process(Event e) {
		return e;
	}
}