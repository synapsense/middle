package com.synapsense.impl.reporting.export;

import java.io.IOException;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;

import com.synapsense.impl.reporting.dao.ResourceDAOReportingException;
import com.synapsense.impl.reporting.model.JSReport;
import com.synapsense.impl.reporting.model.Report;
import com.synapsense.service.FileWriter;
import com.synapsense.service.FileWriter.WriteMode;
import com.synapsense.service.reporting.exception.ExportingReportingException;
import com.synapsense.service.reporting.exception.ReportingRuntimeException;

public class PdfReportExporter implements ReportExporter {

	@Override
	public void export(JSReport report, String outFileName) throws ExportingReportingException {
		try {
			JasperExportManager.exportReportToPdfFile(report.getJasperPrint(), outFileName);
		} catch (JRException e) {
			throw new ExportingReportingException("Unable to export \"" + report.getJasperPrint().getName()
			        + "\"report to pdf");
		}
	}

	@Override
	public void export(Report report, String outFilePath) throws ExportingReportingException {
		throw new ReportingRuntimeException("Dispatching error when exporting report");
	}

	@Override
	public void export(JSReport report, String outFileName, FileWriter fileWriter) throws ExportingReportingException {
		try {
			byte[] buffer = JasperExportManager.exportReportToPdf(report.getJasperPrint());
			try {
				fileWriter.writeFileBytes(outFileName, buffer, WriteMode.REWRITE);
			} catch (IOException e) {
				throw new ResourceDAOReportingException("Failed to write file bytes" + outFileName
						+ " with mode " + WriteMode.REWRITE, e);
			}
		} catch (JRException e) {
			throw new ExportingReportingException("Unable to export \"" + report.getJasperPrint().getName()
			        + "\"report to pdf");
		}
	}

}
