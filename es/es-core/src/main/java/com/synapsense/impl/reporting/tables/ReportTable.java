package com.synapsense.impl.reporting.tables;

import com.synapsense.service.reporting.dto.TableTO;

/**
 * The <code>ReportTable</code> interface presents Report Table.
 * 
 * @author Grigory Ryabov
 */
public interface ReportTable extends Iterable<ReportRow> {

	/**
	 * Gets the table.
	 *
	 * @return the table
	 */
	TableTO getTable();
}
