package com.synapsense.impl.alerting.notification;

import com.synapsense.dto.TO;
import org.apache.log4j.Logger;

import com.synapsense.dto.MessageType;
import com.synapsense.service.UserManagementService;
import com.synapsense.service.user.EnvUserContext;
import com.synapsense.service.user.UserContext;

class UserDestination implements Destination {
	private static final long serialVersionUID = -8016356705273165655L;
	private final static Logger logger = Logger.getLogger(UserDestination.class);

	private final String userName;
	private final UserManagementService userService;

	public UserDestination(final String userName, final UserManagementService userService) {
		this.userName = userName;
		this.userService = userService;
	}

	@Override
	public String getAddress(final MessageType mType) {
		final TO<?> user = userService.getUser(userName);
		if (user == null) {
			if (logger.isDebugEnabled()) {
				logger.debug("Failed to get " + mType + " address of user " + userName + "  because it does not exist");
			}
			return null;
		}

		switch (mType) {
		case SMTP: {
			String email = (String) userService.getProperty(user, userService.USER_PRIMARY_EMAIL);
			if (email == null || email.isEmpty())
				logger.warn("Environment user:" + userName + " has no primary email specified");
			return email;
		}
		case SMS: {
			String sms = (String) userService.getProperty(user, userService.USER_SMS_ADDRESS);
			if (sms == null || sms.isEmpty())
				logger.warn("Environment user:" + userName + " has no SMS address specified");
			return sms;
		}
		default:
			throw new IllegalArgumentException("Unsupported type of destination : " + mType.name());
		}
	}

	@Override
	public UserContext getUserContext() {
		return new EnvUserContext(userName, userService);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((userName == null) ? 0 : userName.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final UserDestination other = (UserDestination) obj;
		if (userName == null) {
			if (other.userName != null)
				return false;
		} else if (!userName.equals(other.userName))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Env.user destination [userName=" + userName + "]";
	}

}