package com.synapsense.impl.alerting.notification.smtp;

import com.synapsense.util.CollectionUtils;

import java.util.List;

/**
 * @author : shabanov
 */
public final class ImageLayer {

	public static ImageLayer TOP = new ImageLayer(0x20000, "visual_layer_top");
	public static ImageLayer MIDDLE = new ImageLayer(0x04000, "visual_layer_middle");
	public static ImageLayer BOTTOM = new ImageLayer(0x00800, "visual_layer_bottom");
    public static ImageLayer SUBFLOOR = new ImageLayer(0x00100, "visual_layer_subfloor");
    public static ImageLayer CONTROL = new ImageLayer(0x00080, "visual_layer_control");

	private static final ImageLayer[] layers = new ImageLayer[] { TOP, MIDDLE, BOTTOM, SUBFLOOR, CONTROL };

	private ImageLayer(int code, String name) {
		this.code = code;
		this.name = name;
	}

	public static List<ImageLayer> decode(int code) {
		List<ImageLayer> result = CollectionUtils.newList();

		for (int i = 0; i < layers.length; i++) {
			ImageLayer layer = layers[i];
			if ((code & layer.code) == layer.code) {
				result.add(layer);
			}
		}

		return result;
	}

    public String getName() {
        return name;
    }

    public String getDisplayName() {
		return displayName == null ? name : displayName;
	}

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public int getCode() {
		return code;
	}

	@Override
	public String toString() {
		return name;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		ImageLayer that = (ImageLayer) o;

		if (code != that.code)
			return false;
		if (!name.equals(that.name))
			return false;

		return true;
	}

	@Override
	public int hashCode() {
		return code;
	}

	private String name;
	private String displayName;
	private int code;
}
