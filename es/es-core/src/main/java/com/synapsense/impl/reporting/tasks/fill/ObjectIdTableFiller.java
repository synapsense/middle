package com.synapsense.impl.reporting.tasks.fill;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;

import com.google.common.collect.ListMultimap;
import com.synapsense.impl.reporting.tasks.EnvValuesTaskData;
import com.synapsense.impl.reporting.tasks.fill.ReportTableFiller.TableFiller;
import static com.synapsense.impl.reporting.tasks.fill.ReportTableFiller.*;
import com.synapsense.service.reporting.Cell;
import com.synapsense.service.reporting.olap.Member;
import com.synapsense.service.reporting.olap.custom.Cube;
import com.synapsense.util.CollectionUtils;
import com.synapsense.util.unitconverter.UnitConverter;

public class ObjectIdTableFiller implements TableFiller {

	@Override
	public void fill(Object[] row, Calendar calendar,
			EnvValuesTaskData envValuesTaskData) {
		int valueIdColumn = 1;
		int rowValueId = (int) row[valueIdColumn];
		int timestampColumn = 2;
		Timestamp timestamp = (Timestamp) row[timestampColumn];
		List<UnitConverter> unitConverters = envValuesTaskData.getUnitConverters();
		List<String> aggregationTypes = CollectionUtils.newList(envValuesTaskData.getFormulaCellsMap().keySet());
		
		int objectIdColumn = 0;
		int objectId = Integer.valueOf((String) row[objectIdColumn]);
		ListMultimap<Integer, Integer> cellObjectMap = envValuesTaskData.getCellObjectMap();
		List<Integer> objectCellNumbers = cellObjectMap.get(objectId);
		//Find cell by timestamp
		List<Cell> cells = envValuesTaskData.getDataSet();
		List<Cell> resultRowCells = CollectionUtils.newList();

		for (Integer objectCellNumber : objectCellNumbers) {
			Cell currentCell = cells.get(objectCellNumber);
			Member timeMember = currentCell.getMember(Cube.DIM_TIME);
			calendar.setTime(timestamp); 
			if (timeMember == null
					|| containsTime(timeMember, calendar)) {
				resultRowCells.add(currentCell);
			}
		}
		
		for (Cell cell : resultRowCells) {
				int aggTypeIndex = 3;
				addValue(cell, row, aggregationTypes, aggTypeIndex, timestamp, unitConverters);
		}
	}

}
