package com.synapsense.impl.performer.snmp;

import java.util.HashMap;
import java.util.Map;

import com.synapsense.impl.performer.snmp.conf.SNMPConfigurationException;

public class TrapSenders {
	private final Map<String, TrapSender> senders;

	public TrapSenders() {
		senders = new HashMap<String, TrapSender>();
	}

	public synchronized TrapSender getTrapSender(String destination) throws SNMPConfigurationException {
		TrapSender sender = senders.get(destination);
		if (sender == null) {
			sender = new TrapSender(destination);
			senders.put(destination, sender);
		}
		return sender;
	}

	public void clear() {
		for (TrapSender sender : senders.values()) {
			sender.close();
		}
		senders.clear();
	}
}
