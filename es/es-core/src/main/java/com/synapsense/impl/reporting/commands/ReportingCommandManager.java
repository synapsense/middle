package com.synapsense.impl.reporting.commands;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import com.synapsense.impl.reporting.commandengine.Command;
import com.synapsense.impl.reporting.commandengine.CommandManager;
import com.synapsense.impl.reporting.commandengine.CommandProxy;
import com.synapsense.service.reporting.dto.CommandStatus;
import com.synapsense.service.reporting.dto.ReportInfo;
import com.synapsense.service.reporting.dto.ReportTaskInfo;

public class ReportingCommandManager implements Serializable {

	private static final long serialVersionUID = 1433447985038741728L;

	private CommandManager manager;

	private Map<Integer, ReportTaskInfo> taskMap = new HashMap<Integer, ReportTaskInfo>();

	public ReportingCommandManager() {
	}

	public ReportingCommandManager(CommandManager manager) {
		this.manager = manager;
	}

	public void cancelCommand(int id) {
		manager.cancelCommand(id);
	}

	public double getReportCommandProgress(int id) {
		return manager.getReportCommandProgress(id);
	}

	public CommandStatus getReportCommandStatus(int id) {
		return manager.getReportCommandStatus(id);
	}

	public void clearCommand(int commandId) {
		manager.clearCommand(commandId);
	    
    }

	public void clearCommands() {
		manager.clearCommands();
	}

	public ReportCommandProxy executeCommand(Command command, ReportTaskInfo taskInfo) {
		CommandProxy proxy = manager.executeCommand(command);
		taskMap.put(proxy.getCommandId(), taskInfo);
		return new ReportCommandProxyImpl(proxy, taskInfo);
	}

	public Collection<ReportCommandProxy> getManagedCommands() {
		Collection<ReportCommandProxy> commands = new LinkedList<ReportCommandProxy>();
		Collection<CommandProxy> proxies = manager.getManagedCommands();
		for (CommandProxy proxy : proxies) {
			commands.add(new ReportCommandProxyImpl(proxy, taskMap.get(proxy.getCommandId())));
		}
		return commands;
	}
	

	private static class ReportCommandProxyImpl implements ReportCommandProxy {

		// ESCA-JAVA0096:
		private static final long serialVersionUID = 668546539705329461L;

		private CommandProxy commandProxy;
		private ReportTaskInfo taskInfo;

		private ReportCommandProxyImpl(CommandProxy commandProxy, ReportTaskInfo taskInfo) {
			this.commandProxy = commandProxy;
			this.taskInfo = taskInfo;
		}

		@Override
		public String getCommandDescription() {
			return commandProxy.getCommandDescription();
		}

		@Override
		public int getCommandId() {
			return commandProxy.getCommandId();
		}

		@Override
		public String getCommandName() {
			return commandProxy.getCommandName();
		}

		@Override
		public ReportTaskInfo getTaskInfo() {
			return taskInfo;
		}

	}

	public void setManager(CommandManager manager) {
		this.manager = manager;
	}

	public boolean isExecutionCompleted(int reportId) {
		return manager.isExecutionCompleted(reportId);
	}

}
