package com.synapsense.impl.erlnode.filters;

import com.ericsson.otp.erlang.OtpErlangAtom;
import com.ericsson.otp.erlang.OtpErlangBoolean;
import com.ericsson.otp.erlang.OtpErlangObject;
import com.ericsson.otp.erlang.OtpErlangTuple;
import com.synapsense.service.nsvc.EventProcessor;

public class SimpleFilterParser implements FilterDefParser {
	
	private static final OtpErlangAtom dispatching_filter =
		new OtpErlangAtom("dispatching_filter");

	@Override
	public EventProcessor parse(OtpErlangTuple filterDef) {
		//whatever the filter definition is, we need to
		//wrap it into a DispatchingProcessor at this point
		//so let's pretend it is a dispatching_filter definition
		//with a single simple filter inside
		OtpErlangTuple dispFilterDef =
			new OtpErlangTuple(new OtpErlangObject[]{
					dispatching_filter,
					new OtpErlangBoolean(false),//going to be a non-bypassing filter
					filterDef
			});
		//since the dispatching filter is a filter we can parse
		//its definition recursively
		return EventFiltersFactory.createFilter(dispFilterDef);
	}

}
