package com.synapsense.impl.reporting.commandengine;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

import com.synapsense.exception.IllegalInputParameterException;
import com.synapsense.impl.reporting.utils.algo.ForEach;
import com.synapsense.impl.reporting.utils.algo.UnaryFunctor;
import com.synapsense.service.reporting.dto.CommandStatus;
import com.synapsense.util.CollectionUtils;

public class DefaultCommandManager implements Serializable, CommandManager {

	private static final long serialVersionUID = -8437369181630826938L;

	private Executor executor;

	private Map<Integer, CommandContext> managedCommands;

	private AtomicInteger ids = new AtomicInteger();

	public DefaultCommandManager(int threadNumber) {
		executor = Executors.newFixedThreadPool(threadNumber);
		managedCommands = CollectionUtils.newLinkedMap();
	}

	public DefaultCommandManager(ReportingExecutor reportingJBOSSExecutor) {
		executor = reportingJBOSSExecutor;
		managedCommands = CollectionUtils.newLinkedMap();
	}
	
	@Override
	public CommandProxy executeCommand(Command command) {
		final CommandContext context = new CommandContext(command);
		int cId = ids.getAndIncrement();

		managedCommands.put(cId, context);

		executor.execute(new Runnable() {

			@Override
			public void run() {
				context.start();
			}
		});
		return new ManagedProxy(cId, command.getName(), command.getDescription());
	}

	@Override
	public Collection<CommandProxy> getManagedCommands() {
		final Collection<CommandProxy> proxies = new ArrayList<CommandProxy>();
		Set<Entry<Integer, CommandContext>> entries = managedCommands.entrySet();
		new ForEach<Entry<Integer, CommandContext>>(entries).process(new UnaryFunctor<Entry<Integer, CommandContext>>() {

			@Override
			public void process(Entry<Integer, CommandContext> entry) {
				Command command = entry.getValue().getCommand();
				proxies.add(new ManagedProxy(entry.getKey(), command.getName(), command.getDescription()));
			}
		});
		return proxies;
	}
	
	@Override
	public boolean isExecutionCompleted(int reportId) {
		for (Entry<Integer, CommandContext> entry : managedCommands.entrySet()) {
			CommandContext context = entry.getValue();
			Command command = context.getCommand();
			if (reportId == command.getReportInfo().getReportId()) {
				return context.getStatus().equals(CommandStatus.COMPLETED) ? true : false;
			}
		}
		return true;
	}

	@Override
	public void clearCommands() {
		for (Iterator<Entry<Integer, CommandContext>> iter = managedCommands.entrySet().iterator(); iter.hasNext();) {
			CommandContext context = iter.next().getValue();
			if (context.removable()) {
				iter.remove();
			}
		}
	}

	@Override
	public void clearCommand(int id) {
		CommandContext context = checkAndGetContext(id);
		if (context.removable()) {
			managedCommands.remove(id);
		} else {
			throw new IllegalStateException("CommandContext is not removable: " + context);
		}
	}

	@Override
	public void cancelCommand(int id) {
		checkAndGetContext(id).cancel();
	}

	@Override
	public double getReportCommandProgress(int id) {
		return checkAndGetContext(id).getProgress();
	}

	@Override
	public CommandStatus getReportCommandStatus(int id) {
		return checkAndGetContext(id).getStatus();
	}
	
	@Override
	public CommandStatus getCommandStatus(int id) {
		return checkAndGetContext(id).getStatus();
	}

	@Override
	public double getCommandProgress(int id) {
		return checkAndGetContext(id).getProgress();
	}

	@Override
	public String getCommandName(int id) {
		return checkAndGetContext(id).getCommand().getName();
	}

	@Override
	public String getCommandDescription(int id) {
		return checkAndGetContext(id).getCommand().getDescription();
	}

	private CommandContext checkAndGetContext(int id) {
		CommandContext context = managedCommands.get(id);
		if (context == null) {
			throw new IllegalInputParameterException("No command managed with id " + id);
		}
		return context;
	}

	private static class ManagedProxy implements CommandProxy {

		// ESCA-JAVA0096:
		private static final long serialVersionUID = 4071432692959236648L;

		private int commandId;
		
		private String commandName;
		
		private String commandDescription;

		private ManagedProxy(int commandId, String commandName, String commandDescription) {
			this.commandId = commandId;
			this.commandName = commandName;
			this.commandDescription = commandDescription;
		}

		@Override
		public int getCommandId() {
			return this.commandId;
		}

		@Override
		public String getCommandName() {
			return this.commandName;
		}

		@Override
		public String getCommandDescription() {
			return this.commandDescription;
		}

	}

}
