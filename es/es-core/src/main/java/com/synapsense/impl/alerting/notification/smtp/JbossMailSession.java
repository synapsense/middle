package com.synapsense.impl.alerting.notification.smtp;

import com.synapsense.impl.alerting.notification.Provider;
import com.synapsense.impl.utils.JbossManagementAPI;
import org.apache.log4j.Logger;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Properties;

/**
 * @author : shabanov
 */
public class JbossMailSession implements Provider<Session> {
	private final static Logger logger = Logger.getLogger(JbossMailSession.class);

	@Override
	public Session get() {
		return getCustomSession();
	}

	private Session getCustomSession() {
		final String SMTP_HOST;
		final String SMTP_PORT;
		final String MAIL_FROM;
		final String SMTP_USERNAME;
		final String SMTP_PASSWORD;
		final boolean SMTP_SSL;
		final boolean SMTP_AUTH;

        InetAddress localhost;
        try {
            localhost = InetAddress.getLocalHost();
        } catch (UnknownHostException e) {
            logger.warn("Can't get internet address by name = localhost due to" ,e);
            return null;
        }

        Authenticator authenticator = null;

		try (JbossManagementAPI mAPI = new JbossManagementAPI(localhost, 9999, "synapsense", "synap$ense")) {
			SMTP_HOST = mAPI.getAttribute("SMTP_HOST");
			SMTP_PORT = mAPI.getAttribute("SMTP_PORT");
			MAIL_FROM = mAPI.getAttribute("MAIL_FROM");
			if (SMTP_HOST == null || SMTP_PORT == null || MAIL_FROM == null) {
				logger.warn("Can't configure mail session. Propreties not valid: host=" + SMTP_HOST + " port="
				        + SMTP_PORT + " from=" + MAIL_FROM);
				return null;
			}

			String ssl;
			SMTP_SSL = (ssl = mAPI.getAttribute("SMTP_SSL")) != null && (ssl.equals("true"));
			SMTP_USERNAME = mAPI.getAttribute("SMTP_USERNAME");
			SMTP_PASSWORD = mAPI.getAttribute("SMTP_PASSWORD");

			if (SMTP_USERNAME != null && SMTP_PASSWORD != null) {
				authenticator = new Authenticator() {
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(SMTP_USERNAME, SMTP_PASSWORD);
					}
				};
				SMTP_AUTH = true;
			} else {
				SMTP_AUTH = false;
			}
		} catch (IOException e) {
			logger.warn("Can't configure mail session.", e);
			return null;
		}

		Properties properties = new Properties();
		properties.put("mail.transport.protocol", "smtp");
		properties.put("mail.smtp.connectiontimeout", "10000");
		properties.put("mail.smtp.host", SMTP_HOST);
		properties.put("mail.smtp.port", SMTP_PORT);
		properties.put("mail.from", MAIL_FROM);
		properties.put("mail.smtp.auth", SMTP_AUTH);
		if (SMTP_SSL) {
			properties.setProperty("mail.smtp.ssl.enable", Boolean.TRUE.toString());
			// JBoss 7 doesn't support tls
			// properties.setProperty("mail.smtp.starttls.enable",
			// Boolean.TRUE.toString());
			// properties.setProperty("mail.smtp.starttls.required",
			// Boolean.TRUE.toString());
			properties.setProperty("mail.smtp.socketFactory.fallback", Boolean.FALSE.toString());
			properties.setProperty("mail.smtp.ssl.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
			properties.setProperty("mail.smtp.ssl.socketFactory.port", new Integer(SMTP_PORT).toString());
		}

		if (SMTP_AUTH) {
			return Session.getInstance(properties, authenticator);
		} else {
			return Session.getInstance(properties);
		}

	}

}
