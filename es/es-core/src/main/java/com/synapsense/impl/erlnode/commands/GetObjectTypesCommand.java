
package com.synapsense.impl.erlnode.commands;

import java.util.Map;

import com.ericsson.otp.erlang.OtpErlangBinary;
import com.ericsson.otp.erlang.OtpErlangList;
import com.ericsson.otp.erlang.OtpErlangObject;
import com.ericsson.otp.erlang.OtpErlangTuple;

import com.synapsense.impl.erlnode.utils.StringConverter;

public class GetObjectTypesCommand extends Command {

	public GetObjectTypesCommand(Map<String, Object> state) {
		super(state);
	}

	@Override
	public OtpErlangObject do_command(OtpErlangTuple command) throws Exception {
		String []types = getEnv().getObjectTypes();
		OtpErlangBinary[] res_types = new OtpErlangBinary[types.length];
		for(int i=0; i<types.length; i++)
			res_types[i] = (OtpErlangBinary)StringConverter.stringToUtf8Binary(types[i]);
		return makeOkResult(
				new OtpErlangList(res_types));
	}

}
