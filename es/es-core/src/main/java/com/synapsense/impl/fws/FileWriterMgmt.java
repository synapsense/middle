package com.synapsense.impl.fws;

import java.io.IOException;

public interface FileWriterMgmt {
	String getRootDir();

	void setRootDir(String rootDir) throws IOException;

	void create();

	void start();

	void stop();

	void destroy();
}
