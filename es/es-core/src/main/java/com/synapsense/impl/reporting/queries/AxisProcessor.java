package com.synapsense.impl.reporting.queries;

import static com.synapsense.impl.reporting.tables.ReportingTableHolder.ReportingHelperTableType;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.synapsense.service.reporting.olap.*;
import org.apache.log4j.Logger;

import com.synapsense.dto.PropertyDescr;
import com.synapsense.dto.TO;
import com.synapsense.exception.IllegalInputParameterException;
import com.synapsense.impl.reporting.tables.ReportingTable;
import com.synapsense.impl.reporting.tables.ReportingTableHolder;
import com.synapsense.impl.reporting.tables.ValueIdAttributesTable;
import com.synapsense.impl.reporting.tasks.QueryDataReportingException;
import com.synapsense.service.reporting.Cell;
import com.synapsense.service.reporting.exception.ReportingQueryException;
import com.synapsense.service.reporting.exception.ReportingServiceSystemException;
import com.synapsense.service.reporting.olap.custom.Cube;
import com.synapsense.service.reporting.olap.custom.EnvironmentDataSource;
import com.synapsense.util.CollectionUtils;
import com.synapsense.util.Pair;

/**
 * The <code>AxisProcessor</code> class processes all supported AxisSet types:
 * retrieves Member values for further query build 
 * 
 * @author Grigory Ryabov
 */
public class AxisProcessor implements PlanProcessor {

	private static final Logger log = Logger.getLogger(AxisProcessor.class);

	private ValueIdAttributesTable valueIdAttributes = new ValueIdAttributesTable();
	
	private EnvironmentDataSource environmentDataSource;

	private TableProcessor owner;

	private ReportingTableHolder reportingTables;
	
	private Map<Pair<String, List<TO<?>>>, Cell> valueIdsMap = CollectionUtils.newMap();
	
	public static interface ValueIdAttributesProvider {

		Map<String, Set<Integer>> getMap();

		List<Pair<String, Integer>> addItems(List<TO<?>> objects,
				Member propertyMember);
	}
	
	public static interface ReportingTypeTable extends ReportingTable {

		Map<String, TupleSet> get();

	}
	
	public AxisProcessor(EnvironmentDataSource ds, TableProcessor tableProcessor, ReportingTableHolder reportingTables) {
		super();
		this.environmentDataSource = ds;
		this.owner = tableProcessor;
		this.reportingTables = reportingTables;
	}

	@Override
	public void processSet(AxisSet axisSet) {
		axisSet.buildQuery(this);
	}

    @Override
    public void processSet(Union union) {
        for (Tuple tuple : union.getTuples()) {
            tuple.buildQuery(this);
        }
    }

	@Override
	public void processSet(CrossJoin crossJoin) {
		AxisSet[] sets = crossJoin.getSets();
		ReportingTable slicerObjectTable = reportingTables.get(ReportingHelperTableType.SLICER_OBJECT_TYPE_TABLE);
		for (AxisSet set : sets) {
			List<Hierarchy> hierarchies = set.getDimensionalities();
			if (!slicerObjectTable.isEmpty()
					&& hierarchies.size() == 1
					&& hierarchies.iterator().next().equals(Cube.OBJECT)) {
				continue;
			} else {
				set.buildQuery(this);
			}
		}
		//For case if we have one OBJECT set and one Property set
		//after adding PM and NOM parameters to corresponding maps we need to cross them using equality of ObjectPropertyType
		//and then call tuple.buildQuery
		if (slicerObjectTable.isEmpty()
				&& isCrossingObjectPropertyNeeded(crossJoin)){
			//for each objectType (e.g. RACK, CRAC) make crossjoin of in(property) and in(object). 
			//Then getTuples and for each tuple.buildQuery()
			Map<String, TupleSet> objectLevelTupleSets = this.reportingTables.get(ReportingHelperTableType.OBJECT_TYPE_TABLE).get();
			Map<String, TupleSet> propertyTypeTupleSets = this.reportingTables.get(ReportingHelperTableType.PROPERTY_TYPE_TABLE).get();
			if (objectLevelTupleSets.isEmpty()){
				log.error("No objects found that meet query criteria");
				throw new ReportingServiceSystemException("No objects found that meet query criteria");
			}
			//first case is one level: [Object].[RACK] and [Measures].[RACK.cTop]
			for (Entry<String, TupleSet> entry : propertyTypeTupleSets.entrySet()){
				String objectLevel = entry.getKey();
				//First case is to create different CrossJoins for e.g. RACKs - [Object].[RACK].&[Rack1],[Measures].[RACK.cTop]
				//And for CRACs
				CrossJoin typeCrossJoin;
				if (objectLevelTupleSets.containsKey(objectLevel)){
					typeCrossJoin = new CrossJoin(entry.getValue()
							, objectLevelTupleSets.get(objectLevel));
				} 
				//Second case is like [Object].[ROOM].&[Room1] and [Measures].[RACK.cTop]
				else {
					typeCrossJoin = new CrossJoin(entry.getValue()
							, objectLevelTupleSets.get(objectLevelTupleSets.keySet().iterator().next()));
				}
				for (Tuple tuple : typeCrossJoin.getTuples()){
					tuple.buildQuery(this);
				}
			}
		}
	}

	@Override
	public void processSet(TupleSet tupleSet) {
		for (Tuple tuple : tupleSet.getTuples()) {
			tuple.buildQuery(this);
		}
	}

	@Override
	public void processSet(Tuple tuple) {
		List<Hierarchy> hierarchies = tuple.getDimensionalities();
		if (hierarchies.contains(Cube.OBJECT) && hierarchies.contains(Cube.PROPERTY)){
			Member propertyMember = tuple.itemByHierarchy(Cube.PROPERTY);
			Member objectMember = tuple.itemByHierarchy(Cube.OBJECT);

			if (propertyMember == null || objectMember == null) {
				return;
			}
			
			List<Member> objectMembers = CollectionUtils.newList();
			objectMembers.add(objectMember);
			try {
				processValueIdAttributes(propertyMember, objectMembers, tuple);
			} catch (QueryDataReportingException e) {
				return;
			}
		} else {
			for (Hierarchy hierarchy : hierarchies) {
				Member member = tuple.itemByHierarchy(hierarchy);
				member.buildQuery(this);
			}
		}
	}

	@Override
	public void processSet(Member member) {
		member.buildQuery(this);
	}
	
	@Override
	public void processSet(SimpleMember member) {
		if (member.getHierarchy().equals(Cube.OBJECT)) {
			this.reportingTables.get(ReportingHelperTableType.OBJECT_TYPE_TABLE).add(member);
		} else if (member.getHierarchy().equals(Cube.PROPERTY)) {
			processPropertyMember(member);
		}
		//If Time Member has no parent than it can be added to IN for its level otherwise it will be added to common IN for formatted dates 
		else if (Cube.getDimension(member.getHierarchy().getName()).equals(Cube.DIM_TIME)) {
			if (member.getParent() == null) {
				this.reportingTables.get(ReportingHelperTableType.LEVEL_VALUES_TIME_TABLE).add(member);
			} else {
				this.reportingTables.get(ReportingHelperTableType.MEMBERS_TIME_TABLE).add(member);
			}
		}
	}

	@Override
	public void processSet(RangeMember member) {
		if (Cube.getDimension(member.getHierarchy().getName()).equals(Cube.DIM_TIME)) {
			if (member.getParent() == null) {
				for (Member simpleMember : member.getSimpleMembers()) {
					this.reportingTables.get(ReportingHelperTableType.LEVEL_VALUES_TIME_TABLE).add(simpleMember);
				}
			} else {
				for (Member simpleMember : member.getSimpleMembers()) {
					this.reportingTables.get(ReportingHelperTableType.MEMBERS_TIME_TABLE).add(simpleMember);
				}
				this.reportingTables.get(ReportingHelperTableType.RANGE_MEMBER_TIME_TABLE).add(member);
				//Processing Range Member parent for adding time values to Time Tables
				member.getParent().buildQuery(this);
			}
		} else if (member.getHierarchy().equals(Cube.PROPERTY)) {
			processPropertyMember(member);
		}
	}
	
	@Override
	public void processSet(AllMemberSet allMemberSet) {
		Hierarchy hierarchy = allMemberSet.getDimensionalities().iterator().next();
		if (hierarchy.equals(Cube.OBJECT)) {
			for (Tuple tuple : allMemberSet.getTuples()){
				tuple.buildQuery(this);
			}
		}
		if (Cube.getDimension(hierarchy.getName()).equals(Cube.DIM_TIME)) {
			addTimeRanges(allMemberSet);
		}
	}
	
	@Override
	public void processSet(FilterableAllMemberSet set) {
		for (Tuple tuple : set.getTuples()){
			tuple.buildQuery(this);
		} 
	}
	
	@Override
	public void processSet(RangeSet rangeSet) {
		Hierarchy hierarchy = rangeSet.getDimensionalities().get(0);
		if (!OrderedHierarchy.class.isInstance(hierarchy)) {
			throw new IllegalInputParameterException("RangeSet is not supported for hierarchy " + hierarchy.toString());
		}
		String aggregationRange = this.owner.getQuery().getFormula().getAggregationRange();
		rangeSet.setAggregationRange(aggregationRange);
		ReportingTable rangesTimeTable = reportingTables.get(ReportingHelperTableType.RANGES_TIME_TABLE);
		rangesTimeTable.add(rangeSet.getStartMember());
		rangesTimeTable.add(rangeSet.getEndMember());
	}
	
	private void addTimeRanges(AxisSet set){
		List<Tuple> rangeTuples = set.getTuples();
		Member startMember = rangeTuples.get(0).itemByDimension(Cube.DIM_TIME);
		Member endMember = rangeTuples.get(rangeTuples.size() - 1).itemByDimension(Cube.DIM_TIME);
		
		ReportingTable rangesTimeTable = reportingTables.get(ReportingHelperTableType.RANGES_TIME_TABLE);
		rangesTimeTable.add(startMember);
		rangesTimeTable.add(endMember);
	}
	
	private boolean isCrossingObjectPropertyNeeded(CrossJoin crossJoin){
		
		AxisSet[] sets = crossJoin.getSets();
		boolean hasObjectHierarchy = false;
		boolean hasPropertyHierarchy = false;
		for (AxisSet set : sets) {
			List<Hierarchy> hierarchies = set.getDimensionalities();
			if (hierarchies.contains(Cube.OBJECT)
					&& !hierarchies.contains(Cube.PROPERTY)){
				hasObjectHierarchy = true;
			}
			if (hierarchies.contains(Cube.PROPERTY)
					&& !hierarchies.contains(Cube.OBJECT)){
				hasPropertyHierarchy = true;
			}			
		}
		return hasObjectHierarchy == true && hasPropertyHierarchy == true ? true : false;
		
	}
	
	
	public ValueIdAttributesTable getValueIdAttributesTable() {
		return this.valueIdAttributes;
	}
	
	private void processValueIdAttributes(Member propertyMember,
			List<Member> objectMembers, Tuple tuple) {
		//Check arguments
		if (objectMembers.isEmpty()) {
			throw new IllegalInputParameterException("Object Members shouldn't be empty");
		}
		
		// Getting PropertyDescr
		String[] keys = ((String) propertyMember.getKey()).split("\\.");
		String propertyName = keys[1];
		String propertyObjectType = keys[0];
		ValueIdAttributesProvider provider = null;
		PropertyDescr propertyDescr = environmentDataSource.getPropertyDescriptor(
				propertyObjectType, propertyName);
		// Getting provider
		provider = owner.getProvider(propertyDescr.getTypeName());
		if (provider == null) {
			log.debug("Unsupported Java Property Type: "
					+ propertyDescr.getTypeName());
			if (log.isDebugEnabled()) {
				log.debug("Unsupported Java Property Type: "
						+ propertyDescr.getTypeName());
			}
			throw new QueryDataReportingException("Unsupported Java Property Type: "
					+ propertyDescr.getTypeName());
		}
		Member objectMember = objectMembers.iterator().next();
		boolean isObjectLevelEqualsPropertyType = objectMember.getLevel()
				.equals(propertyObjectType);
		// Getting value Id attributes pair from ValueIdAttributesProvider
		List<Cell> valueIdCells = owner.getValueIdCells();
		List<TO<?>> objectsByPropertyLevel = getObjectsByLevel(objectMembers, propertyObjectType, isObjectLevelEqualsPropertyType);
		
		Cell valueIdCell = null;
		if (objectsByPropertyLevel.isEmpty()) {
			return;
		}
		
		Pair<String, List<TO<?>>> valueIdPairKey = Pair.newPair(propertyObjectType + "." + propertyName, objectsByPropertyLevel);
		
		if (!valueIdsMap.containsKey(valueIdPairKey)) {
			valueIdCell = new Cell(tuple);
			int index = valueIdCells.size();
			this.owner.addValueIdCell(valueIdCell);
			
			List<Pair<String, Integer>> attributesPairs = provider.addItems(
					objectsByPropertyLevel, propertyMember);
			for (Pair<String, Integer> attributesPair : attributesPairs) {
				valueIdAttributes.addValueIdAttributes(attributesPair,
						index);
			}
			valueIdsMap.put(valueIdPairKey, valueIdCell);
		}
	}
	
	private List<TO<?>> getObjectsByLevel(List<Member> objectMembers, String propertyObjectType, boolean isObjectLevelEqualsPropertyType){
		
		List<TO<?>> objects = CollectionUtils.newList();
		for (Member objectMember : objectMembers) {
			TO<?> cellObject = environmentDataSource.getObjectId(objectMember);
			if (isObjectLevelEqualsPropertyType) {
				objects.add(cellObject);
			} else {
				Collection<TO<?>> children = environmentDataSource.getRelatedObjects(
						cellObject, propertyObjectType, true);
				objects.addAll(children);
			}
		}
		return objects;
		
	}
	
	private void processPropertyMember(Member propertyMember) {
		
		ReportingTable slicerObjectTable = reportingTables.get(ReportingHelperTableType.SLICER_OBJECT_TYPE_TABLE);
		if (slicerObjectTable.isEmpty()){
			reportingTables.get(ReportingHelperTableType.PROPERTY_TYPE_TABLE).add(propertyMember);
		} else {
			Map<String, TupleSet> allValues = slicerObjectTable.get();
			List<Tuple> objectTuples = allValues.values().iterator().next().getTuples();
			Member firstObjectMember = objectTuples.get(0).itemByHierarchy(Cube.OBJECT);
			
			if (propertyMember == null && objectTuples.isEmpty()) {
				return;
			}
			Tuple tuple = new MemberTuple(firstObjectMember, propertyMember);
			List<Member> objectMembers = CollectionUtils.newList();
			for (Tuple objectTuple : objectTuples){
				objectMembers.add(objectTuple.itemByHierarchy(Cube.OBJECT));
			}
			
			try {
				processValueIdAttributes(propertyMember, objectMembers, tuple);
			} catch (QueryDataReportingException e) {
				log.error(e.getLocalizedMessage(), e);
				throw new ReportingQueryException(e);
			}
		}

	}
	
}
