package com.synapsense.impl.erlnode;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import com.synapsense.exception.AcApiException;
import org.apache.log4j.Logger;

import com.ericsson.otp.erlang.OtpErlangAtom;
import com.ericsson.otp.erlang.OtpErlangDecodeException;
import com.ericsson.otp.erlang.OtpErlangExit;
import com.ericsson.otp.erlang.OtpErlangObject;
import com.ericsson.otp.erlang.OtpErlangPid;
import com.ericsson.otp.erlang.OtpErlangTuple;
import com.ericsson.otp.erlang.OtpMbox;
import com.ericsson.otp.erlang.OtpMsg;
import com.ericsson.otp.erlang.OtpNode;
import com.synapsense.exception.IllegalInputParameterException;
import com.synapsense.impl.erlnode.commands.Command;
import com.synapsense.impl.erlnode.commands.CreateAlertTypeCommand;
import com.synapsense.impl.erlnode.commands.CreateObjectCommand1;
import com.synapsense.impl.erlnode.commands.CreateObjectCommand2;
import com.synapsense.impl.erlnode.commands.DeleteObjectCommand;
import com.synapsense.impl.erlnode.commands.DeregisterProcessorCommand;
import com.synapsense.impl.erlnode.commands.ExistsCommand;
import com.synapsense.impl.erlnode.commands.GetAlertType;
import com.synapsense.impl.erlnode.commands.GetAllPropertiesValuesCommand;
import com.synapsense.impl.erlnode.commands.GetChildrenCommand1;
import com.synapsense.impl.erlnode.commands.GetChildrenCommand2;
import com.synapsense.impl.erlnode.commands.GetObjectTypesCommand;
import com.synapsense.impl.erlnode.commands.GetObjectsByTypeCommand;
import com.synapsense.impl.erlnode.commands.GetParentsCommand1;
import com.synapsense.impl.erlnode.commands.GetParentsCommand2;
import com.synapsense.impl.erlnode.commands.GetPropertiesValuesCommand;
import com.synapsense.impl.erlnode.commands.GetPropertyValueCommand;
import com.synapsense.impl.erlnode.commands.LogActivityCommand;
import com.synapsense.impl.erlnode.commands.RaiseAlertCommand;
import com.synapsense.impl.erlnode.commands.RegisterProcessorCommand;
import com.synapsense.impl.erlnode.commands.RemoveRelationCommand;
import com.synapsense.impl.erlnode.commands.SetAllPropertiesValuesCommand;
import com.synapsense.impl.erlnode.commands.SetPropertyValueCommand;
import com.synapsense.impl.erlnode.commands.SetRelationCommand;
import com.synapsense.impl.erlnode.utils.StringConverter;
import com.synapsense.service.AlertService;
import com.synapsense.service.Environment;
import com.synapsense.service.impl.activitylog.ActivityLogService;
import com.synapsense.service.nsvc.NotificationService;

public class ThreadDriver extends Thread {
	private static final Logger log = Logger.getLogger(ThreadDriver.class);

	public static final String nodeName = "esapi";
	public static final String mboxName = "esapi";
	public static final String cookie = "controller";
	public static final String ac_api_process = "ac_api_json_erl";

	public static final OtpErlangAtom ok = new OtpErlangAtom("ok");
	public static final OtpErlangAtom result = new OtpErlangAtom("esapi_result");
	public static final OtpErlangAtom exception = new OtpErlangAtom("exception");
	public static final OtpErlangAtom error = new OtpErlangAtom("error");
	public static final OtpErlangAtom ac_api_json = new OtpErlangAtom("ac_api_json");
	public static final OtpErlangAtom shutdown = new OtpErlangAtom("shutdown");

	public static final OtpErlangTuple not_connected = makeError("not_connected");
	public static final OtpErlangTuple bad_method = makeError("bad_method");

	public Map<String, Object> globalState = new HashMap<String, Object>();

	public HashMap<String, Command> commandTable;

	private OtpMbox mbox = null;
	private OtpNode node = null;

	private NodeStatusHandler nsh = null;

	public ThreadDriver(Environment env, NotificationService nsvc, AlertService asvc, ActivityLogService als) {
		globalState.put("env", env);
		globalState.put("nsvc", nsvc);
		globalState.put("alertsvc", asvc);
		globalState.put("logsvc", als);

		commandTable = new HashMap<String, Command>();
		commandTable.put("get_object_types", new GetObjectTypesCommand(globalState));
		commandTable.put("get_objects_by_type", new GetObjectsByTypeCommand(globalState));
		commandTable.put("set_property_value", new SetPropertyValueCommand(globalState));
		commandTable.put("create_object_1", new CreateObjectCommand1(globalState));
		commandTable.put("create_object_2", new CreateObjectCommand2(globalState));
		commandTable.put("delete_object", new DeleteObjectCommand(globalState));
		commandTable.put("exists", new ExistsCommand(globalState));
		commandTable.put("get_all_properties_values", new GetAllPropertiesValuesCommand(globalState));
		commandTable.put("get_children_1", new GetChildrenCommand1(globalState));
		commandTable.put("get_children_2", new GetChildrenCommand2(globalState));
		commandTable.put("get_parents_1", new GetParentsCommand1(globalState));
		commandTable.put("get_parents_2", new GetParentsCommand2(globalState));
		commandTable.put("get_properties_values", new GetPropertiesValuesCommand(globalState));
		commandTable.put("get_property_value", new GetPropertyValueCommand(globalState));
		commandTable.put("remove_relation", new RemoveRelationCommand(globalState));
		commandTable.put("set_all_properties_values", new SetAllPropertiesValuesCommand(globalState));
		commandTable.put("set_property_value", new SetPropertyValueCommand(globalState));
		commandTable.put("set_relation", new SetRelationCommand(globalState));
		commandTable.put("register_processor", new RegisterProcessorCommand(globalState));
		commandTable.put("deregister_processor", new DeregisterProcessorCommand(globalState));
		commandTable.put("log_activity", new LogActivityCommand(globalState));
		commandTable.put("raise_alert", new RaiseAlertCommand(globalState));
		commandTable.put("get_alert_type", new GetAlertType(globalState));
		commandTable.put("create_alert_type", new CreateAlertTypeCommand(globalState));
	}

	public void halt() throws InterruptedException {
		if (mbox != null) {
			log.info("sending \"shutdown\" message to mbox");
			mbox.send(mbox.self(), shutdown);
			log.info("joining thread...");
			this.join(1000);
			log.info("shutdown complete (or timed out...)");
		}
	}

	public String call_ac(String json_rpc) throws AcApiException {
		if(node == null) {
			log.error("Attempted AC API call, but ErlNode is not initialized.");
			throw new AcApiException("ErlNode not initialized");
		} else {
			Set<String> acNodes = nsh.connectedNodes();
			if(acNodes.isEmpty()) {
				log.error("Attempted AC API call, but no AC service is connected.");
				throw new AcApiException("API unavailable - AC not connected");
			}
			String acNode = acNodes.iterator().next();
			OtpMbox mbox = node.createMbox();
			try {
				OtpErlangTuple sendTup = new OtpErlangTuple(new OtpErlangObject[]{ac_api_json, mbox.self(), StringConverter.stringToUtf8Binary(json_rpc)});
				log.info("Sending API call to " + acNode + " : " + json_rpc);
				mbox.send(ac_api_process, acNode, sendTup);
				String reply = StringConverter.utf8BinaryToString(mbox.receive());
				log.info("API reply: " + reply);
				return reply;
			} catch(OtpErlangExit oee) {
				log.error("Remote node exited", oee);
				throw new AcApiException("Remote node exited", oee);
			} catch(OtpErlangDecodeException oede) {
				log.error("Invalid response", oede);
				throw new AcApiException("Invalid response", oede);
			/*} catch(OtpErlangException oee) {
				log.error("Couldn't decode response", oee);
				throw new AcApiException("Couldn't decode response", oee);
				*/
			} catch(Exception e) {
				log.error("Unhandled exception", e);
				throw new AcApiException("Unhandled exception", e);
			} finally {
				mbox.close();
			}
		}
	}

	private OtpNode startNodeRetry(String canonicalHostName, int retries)
		throws
			InterruptedException,
			java.io.IOException {
		try {
			log.info("Starting Erlang node '" + nodeName + "'");
			OtpNode node = new OtpNode(nodeName + "@" + canonicalHostName, cookie);
			//Fix for Bug 8556, this flag turns on int lists as strings decoding
			node.setFlags(1);
			return node;
		} catch (java.io.IOException e) {
			if (retries > 0) {
				log.error("Exception starting Erlang node, trying again: ", e);
				Thread.sleep(5000);
				return startNodeRetry(canonicalHostName, retries-1);
			} else {
				throw e;
			}
		}
	}

	protected void startNodeMbox(String canonicalHostName)
		throws
			InterruptedException,
			java.io.IOException {
		OtpNode node = startNodeRetry(canonicalHostName, 3);
		log.info("Creating mailbox '" + mboxName + "'");
		OtpMbox mbox = node.createMbox(mboxName);
		this.nsh = new NodeStatusHandler((RegisterProcessorCommand)commandTable.get("register_processor"), globalState);
		node.registerStatusHandler(nsh);
		globalState.put("mbox", mbox);
		this.mbox = mbox;
		this.node = node;
	}

	public void run() {
		log.info("ES API interface starting...");

		String chostname = null;
		try {
			chostname = InetAddress.getLocalHost().getCanonicalHostName();
			log.info("starting node on host: " + chostname);
		} catch (UnknownHostException e) {
			log.error("UnknownHostException, ErlNode can't start", e);
			return;
		}

		try {
			startNodeMbox(chostname);
		} catch (InterruptedException e) {
			log.error("Interrupted during startup, exiting", e);
			return;
		} catch (java.io.IOException e) {
			log.error("Gave up setting up Erlang node and mailbox: ", e);
			return;
		}

		try {
			while (true) {
				log.trace("Waiting for message...");
				OtpMsg m = mbox.receiveMsg();
				OtpErlangPid fromPid = m.getSenderPid();
				OtpErlangObject command = m.getMsg();

                if (log.isDebugEnabled()) {
                    log.debug("Driver received command: " + ((command != null) ? command.toString() : "NULL") +
                                     " from PID: " + ((fromPid != null) ? fromPid.toString() : "NULL"));
                }

                if (command == null || command.toString().equals(shutdown.toString())) {
					log.info("Driver received shutdown command");
					return; // finally block is still executed
				}

				log.debug("Handling message from '" + fromPid + "'");

				OtpErlangObject resp = parseMessage(command);

				log.debug("Sending response");
				mbox.send(fromPid, resp);
				log.trace("Send complete");
			}
		} catch (OtpErlangExit e) {
			log.error("OtpErlangExit: ", e);
		} catch (OtpErlangDecodeException e) {
			log.error("OtpErlangException: ", e);
		} catch (Throwable t) {
			log.error("Exiting from unhandled exception:", t);
		} finally {
			mbox.close();
			node.close();
			flushState();
			log.info("Driver Exiting...");
		}
	}

	private void flushState() {
		for (Command cmd : commandTable.values())
			cmd.flushState();
	}

	public static OtpErlangTuple makeError(String atom) {
		return new OtpErlangTuple(new OtpErlangObject[] { error, new OtpErlangAtom(atom) });
	}

	public static OtpErlangTuple makeException(String message) {
		return new OtpErlangTuple(new OtpErlangObject[] { exception, StringConverter.stringToUtf8Binary(message) });
	}

	public static OtpErlangTuple makeResult(OtpErlangTuple tup) {
		return new OtpErlangTuple(new OtpErlangObject[] { result, tup });
	}

	public OtpErlangObject parseMessage(OtpErlangObject object) {
		// Message format is:
		// {command_name, Param1, ..., ParamN}

		try {
			OtpErlangTuple command = (OtpErlangTuple) object;

			// 1. Get command name
			String commandValue = command.elementAt(0).toString();
			log.debug("Processing command '" + commandValue + "'");

			// 3. Select and run command
			if (commandTable.containsKey(commandValue)) {
				Command c = commandTable.get(commandValue);
				try {
					return new OtpErlangTuple(new OtpErlangObject[] { result, c.run(command) });
				} catch (IllegalInputParameterException e) {
					OtpErlangTuple ex = Command.makeException(ExceptionTypes.ILLEGAL_INPUT_PARAMETER, e.getMessage());
					log.error("Got an exception while executing the command '" + commandValue + "'", e);
					return makeResult(ex);
				} catch (Throwable t) {
					log.error("Got an exception while executing the command '" + commandValue + "'", t);
					return makeResult(makeException(t.getMessage()));
				}
			} else {
				return makeResult(bad_method);
			}
		} catch (Throwable t) {
			log.error("Error parsing command " + object.toString());
			return makeResult(makeException(t.getMessage()));
		}
	}

}
