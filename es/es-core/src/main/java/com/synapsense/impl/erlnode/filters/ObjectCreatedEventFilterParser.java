package com.synapsense.impl.erlnode.filters;

import org.apache.log4j.Logger;

import com.ericsson.otp.erlang.OtpErlangTuple;
import com.synapsense.service.nsvc.filters.ObjectCreatedEventFilter;
import com.synapsense.service.nsvc.filters.TOMatcher;

//-record(object_created_event_filter, {matcher = #regex{} :: #regex{} | #to{}}).
class ObjectCreatedEventFilterParser {
	
	private static final Logger log =
		Logger.getLogger(ObjectCreatedEventFilter.class);

	public ObjectCreatedEventFilter parse(OtpErlangTuple filterDef) {
		TOMatcher matcher =
			TOMatcherParser.parse(filterDef.elementAt(1));
		log.debug("returning ObjectCreatedEventFilter");
		return new ObjectCreatedEventFilter(matcher);
	}
	
}
