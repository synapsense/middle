package com.synapsense.impl.reporting.dao;

import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.IllegalInputParameterException;
import com.synapsense.exception.ObjectExistsException;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.exception.PropertyNotFoundException;
import com.synapsense.exception.UnableToConvertPropertyException;
import com.synapsense.impl.reporting.utils.ReportingConstants;
import com.synapsense.impl.reporting.utils.ReportingConstants.ReportingParameterType;
import com.synapsense.service.Environment;
import com.synapsense.service.reporting.dto.ReportTaskInfo;
import com.synapsense.service.reporting.dto.ReportTaskTitle;
import com.synapsense.service.reporting.exception.EnvDAOReportingException;
import com.synapsense.service.reporting.exception.ObjectNotFoundDAOReportingException;
import com.synapsense.service.reporting.olap.RangeSet;
import com.synapsense.util.CollectionUtils;
import com.synapsense.util.Pair;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Collection;
import java.util.Date;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class DbReportTaskDAO extends AbstractReportingDAO implements ReportTaskDAO {

	private static final Log log = LogFactory.getLog(DbReportTaskDAO.class);

	public DbReportTaskDAO(Environment environment) {
		super(environment);
	}

	private static Map<ReportingParameterType, ParameterDbValueProvider> parameterDbValueProviderMap = CollectionUtils.newMap();

	static {
		ParameterDbValueProvider rangeSetDbValueProvider = new RangeSetDbValueProvider();
		parameterDbValueProviderMap.put(ReportingParameterType.RANGESET, rangeSetDbValueProvider);
		ParameterDbValueProvider timestampDbValueProvider = new TimestampDbValueProvider();
		parameterDbValueProviderMap.put(ReportingParameterType.TIMESTAMP, timestampDbValueProvider);
		ParameterDbValueProvider hashsetDbValueProvider = new HashSetDbValueProvider();
		parameterDbValueProviderMap.put(ReportingParameterType.SET_TO, hashsetDbValueProvider);
		
		ParameterDbValueProvider stringDbValueProvider = new DefaultDbValueProvider();
		parameterDbValueProviderMap.put(ReportingParameterType.STRING, stringDbValueProvider);
		ParameterDbValueProvider integerDbValueProvider = new DefaultDbValueProvider();
		parameterDbValueProviderMap.put(ReportingParameterType.INTEGER, integerDbValueProvider);
		ParameterDbValueProvider dateDbValueProvider = new DefaultDbValueProvider();
		parameterDbValueProviderMap.put(ReportingParameterType.DATE, dateDbValueProvider);
		ParameterDbValueProvider toDbValueProvider = new DefaultDbValueProvider();
		parameterDbValueProviderMap.put(ReportingParameterType.TO, toDbValueProvider);
		ParameterDbValueProvider longDbValueProvider = new DefaultDbValueProvider();
		parameterDbValueProviderMap.put(ReportingParameterType.LONG, longDbValueProvider);
	}
	
	private static interface ParameterDbValueProvider {
		Object getValue(Object value);
	}

	@Override
	public void saveReportTask(ReportTaskInfo task) throws ObjectExistsException, ObjectNotFoundDAOReportingException {
		if (task == null) {
			throw new IllegalInputParameterException("Report task info shouldn't be null");
		}
		String templateName = task.getReportTemplateName();
		TO<?> reportTemplate = getObjectByName(ReportingConstants.TYPE.REPORT_TEMPLATE, templateName);
		if (reportTemplate == null) {
			throw new ObjectNotFoundDAOReportingException("Report template " + templateName + " not found");
		}
		Collection<ReportTaskTitle> templateTasks = getReportTasksByTemplate(templateName);
		for (ReportTaskTitle taskTitle : templateTasks) {
			if (taskTitle.getName().equals(task.getName())){
				throw new ObjectExistsException(
						"Report task with such name : " + task.getName()
								+ " is scheduled");
			}
		}
		
		// Create new report object
		TO<?> newTask = createObject(ReportingConstants.TYPE.REPORT_TASK, new ValueTO[] {
		        new ValueTO(ReportingConstants.FIELD.REPORT_TASK.NAME, task.getName()),
		        new ValueTO(ReportingConstants.FIELD.REPORT_TASK.TEMPLATE_ID, reportTemplate.getID()),
		        new ValueTO(ReportingConstants.FIELD.REPORT_TASK.CRON, task.getCron()),
		        new ValueTO(ReportingConstants.FIELD.REPORT_TASK.OWNER, task.getOwner()),
		        new ValueTO(ReportingConstants.FIELD.REPORT_TASK.USERS, task.getEmailRecipients()),
		        new ValueTO(ReportingConstants.FIELD.REPORT_TASK.FORMATS, task.getExportFormats()),
		        // use 0 and 1 only
		        new ValueTO(ReportingConstants.FIELD.REPORT_TASK.STATUS, task.isActive() ? 1 : 0) });
		// Set report id to dto
		int taskId = (Integer) newTask.getID();
		task.setTaskId(taskId);

		// Add parameter values to report object
		setPropertyValue(newTask, ReportingConstants.FIELD.REPORT_TASK.PARAMETER_VALUES,
		        persistReportParameterValues(reportTemplate, task));

	}

	@Override
	public void updateReportTask(int taskId, ReportTaskInfo newState) throws ObjectNotFoundDAOReportingException, ObjectExistsException {
		if (newState == null) {
			throw new IllegalInputParameterException("Supplied 'newState' is null");
		}

		TO<?> reportTaskToUpdate = getObjectById(ReportingConstants.TYPE.REPORT_TASK, taskId);
		if (reportTaskToUpdate == null) {
			throw new ObjectNotFoundDAOReportingException("Report task with id " + taskId + " has not been found");
		}
		TO<?> reportTask = this.getObjectById(ReportingConstants.TYPE.REPORT_TASK, taskId);
		if (reportTask != null && !reportTask.equals(reportTaskToUpdate)) {
			throw new ObjectExistsException("Report task with such name : " + newState.getName() + " is scheduled");
		}
		
		TO<?> reportTemplate = getObjectByName(ReportingConstants.TYPE.REPORT_TEMPLATE,
		        newState.getReportTemplateName());
		if (reportTemplate == null) {
			throw new ObjectNotFoundDAOReportingException("Report template " + newState.getReportTemplateName()
			        + " has not been found");
		}
        // rewrite all props
		try {
			environment.setAllPropertiesValues(reportTaskToUpdate, new ValueTO[] {
			        new ValueTO(ReportingConstants.FIELD.REPORT_TASK.NAME, newState.getName()),
			        new ValueTO(ReportingConstants.FIELD.REPORT_TASK.TEMPLATE_ID, reportTemplate.getID()),
			        new ValueTO(ReportingConstants.FIELD.REPORT_TASK.CRON, newState.getCron()),
			        new ValueTO(ReportingConstants.FIELD.REPORT_TASK.OWNER, newState.getOwner()),
			        new ValueTO(ReportingConstants.FIELD.REPORT_TASK.USERS, newState.getEmailRecipients()),
			        new ValueTO(ReportingConstants.FIELD.REPORT_TASK.FORMATS, newState.getExportFormats()),
			        // use 0 and 1 only
			        new ValueTO(ReportingConstants.FIELD.REPORT_TASK.STATUS, newState.isActive() ? 1 : 0) });
		} catch (ObjectNotFoundException e) {
			throw new ObjectNotFoundDAOReportingException("Report task with id " + taskId + " has not been found", e);
		} catch (PropertyNotFoundException e) {
			throw new ObjectNotFoundDAOReportingException("Report task has not property " + e.getProperty(), e);
		} catch (UnableToConvertPropertyException e) {
			throw new ObjectNotFoundDAOReportingException("Failed to cast some of the properties values", e);
		}

		Collection<TO<?>> paramValues = environment.getObjects(ReportingConstants.TYPE.REPORT_PARAMETER_VALUE,
		        new ValueTO[] { new ValueTO("reportTaskId", taskId) });
		for (TO<?> paramValId : paramValues) {
			try {
				environment.deleteObject(paramValId);
			} catch (ObjectNotFoundException e) {
				log.debug("Could not delete parameter value", e);
			}
		}

		setPropertyValue(reportTaskToUpdate, ReportingConstants.FIELD.REPORT_TASK.PARAMETER_VALUES,
		        persistReportParameterValues(reportTemplate, newState));

	}
	
	@Override
	public void removeReportTask(int taskId) throws ObjectNotFoundDAOReportingException {
		if (log.isDebugEnabled()) {
			log.debug("Removing report task " + taskId);
		}

		String objectType = ReportingConstants.TYPE.REPORT_TASK;
		TO<?> reportTask = getObjectById(objectType, taskId); 
		if (reportTask == null) {
			throw new ObjectNotFoundDAOReportingException("Report task with id " + taskId + " not found");
		}
		//Remove Report Paramater Values
		removeReportParamtersValues(reportTask);
		//Remove Report Task
		try {
			removeObject(reportTask);
		} catch (ObjectNotFoundDAOReportingException e) {
			throw new ResourceDAOReportingException("Failed to remove report task with ID=" + taskId, e);
		}
	}

	@Override
	public Collection<ReportTaskTitle> getActiveReportTasks(String reportTemplateName)
	        throws ObjectNotFoundDAOReportingException {

		if (reportTemplateName == null) {
			throw new IllegalInputParameterException("Report template name shouldn't be null");
		}

		if (log.isDebugEnabled()) {
			log.debug("Retrieving all active report tasks of report template " + reportTemplateName);
		}

		return CollectionUtils.searchAll(getReportTasksByTemplate(reportTemplateName),
		        new CollectionUtils.Predicate<ReportTaskTitle>() {
			        @Override
			        public boolean evaluate(ReportTaskTitle reportTaskTitle) {
				        return reportTaskTitle.isActive();
			        }
		        });
	}

	@Override
	public ReportTaskInfo getReportTaskInfo(String taskName) throws ObjectNotFoundDAOReportingException {
		Collection<TO<?>> tasks = environment.getObjects(ReportingConstants.TYPE.REPORT_TASK,
		        new ValueTO[] { new ValueTO(ReportingConstants.FIELD.REPORT_TASK.NAME, taskName) });
		if (tasks.size() == 1) {
			return getReportTaskInfo((Integer) tasks.iterator().next().getID());
		}

		return null;
	}

	@Override
	public ReportTaskInfo getReportTaskInfo(int taskId) throws ObjectNotFoundDAOReportingException {
		if (log.isDebugEnabled()) {
			log.debug("Retrieving report task :" + taskId);
		}

		TO<?> reportTask = this.getObjectById(ReportingConstants.TYPE.REPORT_TASK, taskId);
		if (reportTask == null) {
			throw new ObjectNotFoundDAOReportingException("Report task with id " + taskId + " not found");
		}
		Map<String, ValueTO> reportTaskProperties;
		try {
			reportTaskProperties = getPropertyValues(reportTask, new String[] {
			        ReportingConstants.FIELD.REPORT_TASK.NAME, ReportingConstants.FIELD.REPORT_TASK.TEMPLATE_ID,
			        ReportingConstants.FIELD.REPORT_TASK.STATUS, ReportingConstants.FIELD.REPORT_TASK.CRON,
			        ReportingConstants.FIELD.REPORT_TASK.USERS, ReportingConstants.FIELD.REPORT_TASK.PARAMETER_VALUES,
			        ReportingConstants.FIELD.REPORT_TASK.OWNER, ReportingConstants.FIELD.REPORT_TASK.FORMATS });
		} catch (EnvDAOReportingException e) {
			throw new ResourceDAOReportingException("Error retrieving properties for report task : " + reportTask);
		}

		int reportTemplateId = (int) reportTaskProperties.get(ReportingConstants.FIELD.REPORT_TASK.TEMPLATE_ID)
		        .getValue();
		String templateName = this.getTemplateNameById(reportTemplateId);

		int status = (Integer) reportTaskProperties.get(ReportingConstants.FIELD.REPORT_TASK.STATUS).getValue();
		String reportName = (String) reportTaskProperties.get(ReportingConstants.FIELD.REPORT_TASK.NAME).getValue();
		String cronExpression = (String) reportTaskProperties.get(ReportingConstants.FIELD.REPORT_TASK.CRON).getValue();
		Collection<String> users = (Collection) reportTaskProperties.get(ReportingConstants.FIELD.REPORT_TASK.USERS)
		        .getValue();
		Collection<String> formats = (Collection) reportTaskProperties
		        .get(ReportingConstants.FIELD.REPORT_TASK.FORMATS).getValue();
		String owner = (String) reportTaskProperties.get(ReportingConstants.FIELD.REPORT_TASK.OWNER).getValue();
		ReportTaskInfo taskInfo = ReportTaskInfo.newInstance(templateName, reportName);
		taskInfo.setTaskId((Integer) reportTask.getID());
		taskInfo.setCron(cronExpression);
		taskInfo.addMailRecipients(users);
		taskInfo.setStatus(status);
		taskInfo.setOwner(owner);
		taskInfo.addExportFormats(formats);
		// Fetch parameters
		taskInfo = fetchParameterValues(taskInfo);

		return taskInfo;

	}

	private Collection<TO<?>> persistReportParameterValues(TO<?> reportTemplate, ReportTaskInfo taskInfo)
	        throws ObjectNotFoundDAOReportingException {

		Map<String, Object> parameterValues = taskInfo.getParametersValues();

		Collection<TO<?>> reportParameterValues = CollectionUtils.newList();
		// Create each parameter value
		for (Entry<String, Object> paramEntry : parameterValues.entrySet()) {
			String paramName = paramEntry.getKey();
			Object value = paramEntry.getValue();

			// Get report parameter id
			Class<?> rpType;
			Collection<TO<?>> allParameters;
			try {
				allParameters = getCollectionPropertyValue(reportTemplate,
				        ReportingConstants.FIELD.REPORT_TEMPLATE.PARAMETERS);
			} catch (EnvDAOReportingException e) {
				log.error(e.getLocalizedMessage(), e);
				throw new ResourceDAOReportingException("Error retrieving report template" + reportTemplate.toString()
				        + " parameters");
			}
			// Get parameter object and its properties
			TO<?> parameter = getObjectsByProperty(allParameters, "name", paramName).iterator().next();
			if (parameter == null) {
				if (log.isDebugEnabled()) {
					log.debug("Report parameter " + paramName + " not found");
				}
				throw new ObjectNotFoundDAOReportingException("Report parameter " + paramName + " not found");
			}

			Map<String, ValueTO> parameterPropertiesMap;
			try {
				parameterPropertiesMap = this.getPropertyValues(parameter,
				        new String[] { ReportingConstants.FIELD.REPORT_PARAMETER.TYPE });
			} catch (EnvDAOReportingException e) {
				log.error(e.getLocalizedMessage(), e);
				throw new ResourceDAOReportingException("Error retrieving report parameter" + parameter.toString() + " properties");
			}

			int rpId = (Integer) parameter.getID();
			String typeStr = (String) parameterPropertiesMap.get(ReportingConstants.FIELD.REPORT_PARAMETER.TYPE)
			        .getValue();
			try {
				rpType = Class.forName(typeStr);
			} catch (ClassNotFoundException e) {
				throw new ResourceDAOReportingException("Unable to cast parameter to type " + typeStr, e);
			}

			if (!rpType.isInstance(value)) {
				throw new ResourceDAOReportingException("Parameter " + paramName + " value is of type "
				        + value.getClass().getName() + " instead of " + rpType.getName()
				        + " specified for report template " + taskInfo.getReportTemplateName());
			}
			if (rpType.equals(RangeSet.class)) {
				value = value.toString();
			}
			ValueTO parameterValueTO = new ValueTO(ReportingConstants.ReportingParameterType
			        .getTypeByClassName(typeStr).getField(), value);
			reportParameterValues.add(createObject(ReportingConstants.TYPE.REPORT_PARAMETER_VALUE, new ValueTO[] {
			        parameterValueTO,
			        new ValueTO(ReportingConstants.FIELD.REPORT_PARAMETER_VALUE.REPORT_TASK_ID, taskInfo.getTaskId()),
			        new ValueTO(ReportingConstants.FIELD.REPORT_PARAMETER_VALUE.PARAMETER_ID, rpId) }));
		}
		return reportParameterValues;

	}

	private ReportTaskInfo fetchParameterValues(ReportTaskInfo taskInfo) throws ObjectNotFoundDAOReportingException {
		Collection<TO<?>> parameterValues = getReportTaskParameterValues(taskInfo.getTaskId());
		// Map stores specified in template order of report parameters
		Map<Integer, Pair<String, Object>> parameterOrderMap = CollectionUtils.newMap();
		for (TO<?> parameterValue : parameterValues) {
			Map<String, ValueTO> parameterValueMap;
			try {
				parameterValueMap = this.getPropertyValues(parameterValue,
				        new String[] { ReportingConstants.FIELD.REPORT_PARAMETER_VALUE.PARAMETER_ID });
			} catch (EnvDAOReportingException e) {
				throw new ResourceDAOReportingException("Error retrieving properties for " + parameterValue.toString());
			}

			int reportParameterId = (int) parameterValueMap.get(
			        ReportingConstants.FIELD.REPORT_PARAMETER_VALUE.PARAMETER_ID).getValue();
			TO<?> reportParameter = getObjectById(ReportingConstants.TYPE.REPORT_PARAMETER, reportParameterId);
			if (reportParameter == null) {
				throw new ObjectNotFoundDAOReportingException("Report task parameter with id " + reportParameterId
				        + " not found");
			}

			Map<String, ValueTO> reportParameterMap;
			try {
				reportParameterMap = this.getPropertyValues(reportParameter, new String[] {
				        ReportingConstants.FIELD.REPORT_PARAMETER.NAME, ReportingConstants.FIELD.REPORT_PARAMETER.TYPE,
				        ReportingConstants.FIELD.REPORT_PARAMETER.ORDER_ID });
			} catch (EnvDAOReportingException e) {
				throw new ResourceDAOReportingException("Error retrieving properties for " + parameterValue.toString());
			}

			String name = (String) reportParameterMap.get(ReportingConstants.FIELD.REPORT_PARAMETER.NAME).getValue();
			String typeStr = (String) reportParameterMap.get(ReportingConstants.FIELD.REPORT_PARAMETER.TYPE).getValue();
			

			String field = ReportingConstants.ReportingParameterType.getTypeByClassName(typeStr).getField();
			
			String dbClassName = ReportingParameterType.getTypeByClassName(typeStr).getDbClassName();
			Class<?> type;
			try {
				type = Class.forName(typeStr);
			} catch (ClassNotFoundException e) {
				throw new ResourceDAOReportingException("Error loading type " + typeStr + " of parameter " + name, e);
			}
			
			Class<?> dbClass = null;
			try {
				dbClass = Class.forName(dbClassName);
			} catch (ClassNotFoundException e) {
				throw new ResourceDAOReportingException("Error loading type " + dbClassName + " of parameter " + name, e);
			}
			
            Object value;
			try {
				value = getPropertyValue(parameterValue, field, dbClass);
				value = parameterDbValueProviderMap.get(ReportingParameterType.getTypeByClassName(typeStr)).getValue(value);
			} catch (EnvDAOReportingException e) {
				throw new ResourceDAOReportingException("Error retrieving report parameter " + field + " value");
			}

			if (!type.isInstance(value)) {
				throw new ResourceDAOReportingException("Error casting parameter " + name + " to type " + typeStr);
			}

			int orderId = (int) reportParameterMap.get(ReportingConstants.FIELD.REPORT_PARAMETER.ORDER_ID).getValue();
			parameterOrderMap.put(orderId, Pair.newPair(name, value));

		}

		for (int i = 0; i < parameterOrderMap.size(); i++) {
			Pair<String, Object> parameterValue = parameterOrderMap.get(i);
			taskInfo.addParameterValue(parameterValue.getFirst(), parameterValue.getSecond());
		}

		return taskInfo;

	}

	private Collection<TO<?>> getReportTaskParameterValues(int taskId) throws ObjectNotFoundDAOReportingException {

		TO<?> report = getObjectById(ReportingConstants.TYPE.REPORT_TASK, taskId);
		if (report == null) {
			throw new ObjectNotFoundDAOReportingException("Report task with id " + taskId + " not found");
		}
		try {
			return getCollectionPropertyValue(report, ReportingConstants.FIELD.REPORT_TASK.PARAMETER_VALUES);
		} catch (EnvDAOReportingException e) {
			throw new ResourceDAOReportingException("Error retrieving report task " + taskId + " parameter values");
		}

	}

	private String getTemplateNameById(int reportTemplateId) throws ObjectNotFoundDAOReportingException {

		TO<?> reportTemplateObject = getObjectById(ReportingConstants.TYPE.REPORT_TEMPLATE, reportTemplateId);
		if (reportTemplateObject == null) {
			throw new ObjectNotFoundDAOReportingException("Report template with id " + reportTemplateId + " not found");
		}
		String templateName;
		try {
			templateName = (String) getPropertyValue(reportTemplateObject,
			        ReportingConstants.FIELD.REPORT_TEMPLATE.NAME, String.class);
		} catch (EnvDAOReportingException e) {
			throw new ResourceDAOReportingException("Error retrieving name for report template " + reportTemplateObject.toString());
		}
		return templateName;

	}
	
	private static class DefaultDbValueProvider implements ParameterDbValueProvider {
		
		@Override
		public Object getValue(Object value) {
			return value;
		}
	}
	
	private static class HashSetDbValueProvider implements ParameterDbValueProvider {
		
		@Override
		public Object getValue(Object value) {
			Set<TO<?>> objects = CollectionUtils.newSet();
			objects.addAll((Collection<TO<?>>) value);
			return objects;
		}
	}

	private static class RangeSetDbValueProvider implements ParameterDbValueProvider {
		
		@Override
		public Object getValue(Object value) {
			JSONObject jsonObject = null;
			try {
				jsonObject = new JSONObject(value.toString());
			} catch (JSONException e) {
				throw new IllegalInputParameterException("JSON Object is not correct: " + jsonObject);
			}
			return RangeSet.newRangeSet(jsonObject);
		}
	}
	
	private static class TimestampDbValueProvider implements ParameterDbValueProvider {
		
		@Override
		public Object getValue(Object value) {
			return new Date(((Date) value).getTime());
		}
	}
	
}
