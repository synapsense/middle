package com.synapsense.impl.alerting.notification.smtp;

import com.synapsense.dto.Message;
import com.synapsense.impl.alerting.notification.Provider;
import freemarker.template.Template;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.EmailException;
import org.apache.log4j.Logger;

import javax.mail.Session;

/**
 * Html email sender which has an ability to send a simple(plain text) email in
 * case of failure
 * 
 * @author : shabanov
 */
public class FailSafeHtmlMailer extends SMTPMailer {
	private final static Logger logger = Logger.getLogger(FailSafeHtmlMailer.class);

	private SMTPMailer html;
	private SMTPMailer plain;

	public FailSafeHtmlMailer(Provider<Session> sessionPrv, Provider<Template> templateProvider) {
		super(sessionPrv);
		html = new HtmlMailer(sessionPrv, templateProvider);
		plain = new SimpleMailer(sessionPrv);
	}

	@Override
	protected Email generateEmail(Message message) throws EmailException {
		try {
			return html.generateEmail(message);
		} catch (EmailException htmlEx) {
			logger.warn("Failed to generate html message. Plain text email will be generated instead", htmlEx);
			return plain.generateEmail(message);
		}
	}

}
