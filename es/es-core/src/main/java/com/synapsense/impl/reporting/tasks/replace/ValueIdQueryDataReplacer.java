package com.synapsense.impl.reporting.tasks.replace;

import java.util.List;

import com.synapsense.impl.reporting.tasks.replace.QueryDataReplacerFactory.QueryDataReplacer;
import com.synapsense.service.reporting.Cell;

public class ValueIdQueryDataReplacer implements QueryDataReplacer {

	@Override
	public String replace(String envDataQuery, List<Cell> valueIdCells) {
		return envDataQuery;
	}

}
