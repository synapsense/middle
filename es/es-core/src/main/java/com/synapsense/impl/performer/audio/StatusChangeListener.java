package com.synapsense.impl.performer.audio;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import org.apache.log4j.Logger;

import com.synapsense.dto.TO;
import com.synapsense.impl.alerting.AlertNotifications;
import com.synapsense.impl.alerting.Utils;
import com.synapsense.service.nsvc.NotificationService;
import com.synapsense.service.nsvc.events.DispatchingProcessor;
import com.synapsense.service.nsvc.events.PropertiesChangedEvent;
import com.synapsense.service.nsvc.events.PropertiesChangedEventProcessor;

public class StatusChangeListener implements PropertiesChangedEventProcessor {

	private NotificationService notificationService;
	private volatile static StatusChangeListener instance;
	private List<AlertSwitchObject> objects = new ArrayList<AlertSwitchObject>();
	private final static Logger logger = Logger.getLogger(StatusChangeListener.class);

	private StatusChangeListener() {

	}

	public static StatusChangeListener getInstance() {
		if (instance == null)
			synchronized (StatusChangeListener.class) {
				if (instance == null) {
					instance = new StatusChangeListener();
				}
			}
		return instance;
	}

	public void setNotificationService(NotificationService service) {
		this.notificationService = service;
	}

	public void startListener() {
		
		DispatchingProcessor dispProcessor = new DispatchingProcessor().putPcep(this);
		notificationService.registerProcessor(dispProcessor, AlertNotifications.alertStatusFilter());
	}

	public void subscribe(AlertSwitchObject object) {
		objects.add(object);
		logger.debug("Tower " + object.getDestination() + "is subscribed");
	}

	public void unsubscribe(String destination) {
		for (ListIterator<AlertSwitchObject> listIt = objects.listIterator(); listIt.hasNext();) {
			AlertSwitchObject object = listIt.next();
			String a = object.getDestination();
			if (a.equals(destination)) {
				listIt.remove();
				logger.debug("Tower " + a + " is unsubscribed");
			}
		}
	}

	@Override
	public PropertiesChangedEvent process(PropertiesChangedEvent e) {
		TO<?> alertChanged = Utils.discoverAlertId(e);
		if (alertChanged==null) {
			if (logger.isDebugEnabled()) {
				logger.debug("Changed object is not alert. Wrong filter was configured");
			}
			return e;
		}
		
		Integer objectId = (Integer) alertChanged.getID();
		for (ListIterator<AlertSwitchObject> listIt = objects.listIterator(); listIt.hasNext();) {
			AlertSwitchObject object = listIt.next();
			Integer a = object.getAlertID();
			if (a.equals(objectId)) {
				object.intervent();
				listIt.remove();
			}
		}

		return e;
	}
}
