package com.synapsense.impl.alerting.notification;

import com.synapsense.impl.alerting.macros.MacroContext;

import java.util.Date;

public class AlertNotificationPersistent implements Notification {

	private final AlertNotification notification;
	private final NotificationStateManager stateManager;

	public AlertNotificationPersistent(AlertNotification notification, NotificationStateManager stateManager) {
		this.notification = notification;
		this.stateManager = stateManager;
	}

	@Override
	public String getId() {
		return notification.getId();
	}

	@Override
	public Date getLastFireTime() {
		return notification.getLastFireTime();
	}

	@Override
	public Date getNextFireTime() {
		return notification.getNextFireTime();
	}

	@Override
	public boolean isActive() {
		return notification.isActive();
	}

	@Override
	public ProcessResult process(NotificationManager ctx) {
		ProcessResult pr = notification.process(ctx);

		if (pr == null) {
			return null;
		}

		if (pr.isTierChanged) {
			stateManager.saveLastTierName(getId(), pr.tier);
		} else {
			stateManager.saveLastAttempt(getId(), pr.na);
		}
		return pr;
	}

	@Override
	public NotificationTemplate generatedBy() {
		return notification.generatedBy();
	}

	@Override
	public String currentState() {
		return notification.currentState();
	}

	@Override
	public MacroContext getContext() {
		return notification.getContext();
	}

    @Override
    public String toString() {
        return notification.toString() + ", persistent = true";
    }
}
