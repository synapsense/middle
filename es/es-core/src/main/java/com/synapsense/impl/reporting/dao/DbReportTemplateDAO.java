package com.synapsense.impl.reporting.dao;

import java.io.File;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.dto.BinaryData;
import com.synapsense.dto.CollectionTO;
import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.IllegalInputParameterException;
import com.synapsense.exception.ObjectExistsException;
import com.synapsense.impl.reporting.utils.FileUtils;
import com.synapsense.impl.reporting.utils.ReportingConstants;
import com.synapsense.service.Environment;
import com.synapsense.service.impl.dao.to.BinaryDataImpl;
import com.synapsense.service.reporting.dto.ReportTemplateInfo;
import com.synapsense.service.reporting.exception.EnvDAOReportingException;
import com.synapsense.service.reporting.exception.ObjectNotFoundDAOReportingException;
import com.synapsense.service.reporting.management.ReportParameter;
import com.synapsense.util.CollectionUtils;

public class DbReportTemplateDAO extends AbstractReportingDAO implements ReportTemplateDAO {

	private static final long serialVersionUID = 5361778026580954297L;

	private static final Log log = LogFactory
			.getLog(DbReportTemplateDAO.class);

	private File fileStorage;

	public DbReportTemplateDAO(File fileStorage, Environment environment) {
		super(environment);
		this.fileStorage = fileStorage;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public void createReportTemplate(ReportTemplateInfo reportTemplateInfo,
			String reportTemplateDesign) throws ObjectExistsException {

		if (reportTemplateInfo.getReportTemplateName() == null) {
			throw new IllegalInputParameterException(
					"Report template name shouldn't be null");
		}

		if (reportTemplateDesign == null) {
			throw new IllegalInputParameterException(
					"Report template source shouldn't be null");
		}

		if (log.isDebugEnabled()) {
			log.debug("Report template "
					+ reportTemplateInfo.getReportTemplateName()
					+ " received to persist");
		}

		String reportTemplateName = reportTemplateInfo.getReportTemplateName();

		// Check report template uniqueness
		if (isReportTemplateExists(reportTemplateName)) {
			if (log.isDebugEnabled()) {
				log.debug("Report template with name "
						+ reportTemplateInfo.getReportTemplateName()
						+ " already exists");
			}
			throw new ObjectExistsException("Report template with name "
					+ reportTemplateInfo.getReportTemplateName()
					+ " already exists");
		}
		Collection<ValueTO> valueTOs = CollectionUtils.newSet();
		ValueTO templateName = new ValueTO(ReportingConstants.FIELD.REPORT_TEMPLATE.NAME, reportTemplateName);
		valueTOs.add(templateName);
		
		//Adding template xml as BinaryData
		ValueTO templateDesign = new ValueTO(ReportingConstants.FIELD.REPORT_TEMPLATE.DESIGN, getBinaryDataFromString(reportTemplateDesign));
		valueTOs.add(templateDesign);
		//Setting system parameter
		ValueTO isTemplateSystem = new ValueTO(ReportingConstants.FIELD.REPORT_TEMPLATE.SYSTEM, reportTemplateInfo.isSystem() ? 1 : 0);
		valueTOs.add(isTemplateSystem);
		// Fill report template info with newly created id
		int rtId;
		TO<?> reportTemplate = createObject(ReportingConstants.TYPE.REPORT_TEMPLATE, valueTOs.toArray(new ValueTO[] {}));;
		rtId = (Integer) reportTemplate.getID();
		reportTemplateInfo.setReportTemplateId(rtId);
		Collection<TO<?>> templateParameters = persistReportTemplateParameters(
				reportTemplateInfo, reportTemplateInfo.getParameters());
		setPropertyValue(reportTemplate, ReportingConstants.FIELD.REPORT_TEMPLATE.PARAMETERS, templateParameters);
		
		if (log.isTraceEnabled()) {
			log.trace("Report template "
					+ reportTemplateInfo.getReportTemplateName()
					+ " has been created successfully");
		}

	}
	
	private BinaryData getBinaryDataFromString (String design) {
		return new BinaryDataImpl(design.getBytes());
	}
	
	private String getStringFromBinaryData (BinaryData storedDesign) {
		if (storedDesign == null) {
			return "";
		}
		return new String(((BinaryDataImpl) storedDesign).getValue());
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public String getReportTemplateDesign(String reportTemplateName)
			throws ObjectNotFoundDAOReportingException {

		if (reportTemplateName == null) {
			throw new IllegalInputParameterException(
					"Report template name shouldn't be null");
		}

		if (log.isDebugEnabled()) {
			log.debug("Retreiving report template " + reportTemplateName
					+ " design");
		}

		String design;
		try {
			TO<?> reportTemplate = getObjectByName(
					ReportingConstants.TYPE.REPORT_TEMPLATE, reportTemplateName);
			if (reportTemplate == null) {
				if (log.isDebugEnabled()) {
					log.debug("Report template " + reportTemplateName
							+ " not found");
				}
				throw new ObjectNotFoundDAOReportingException("Report template "
						+ reportTemplateName + " not found");
			}
			design = getStringFromBinaryData((BinaryData) getPropertyValue(reportTemplate,
					ReportingConstants.FIELD.REPORT_TEMPLATE.DESIGN, BinaryData.class));
		} catch (EnvDAOReportingException e) {
			log.error(e.getLocalizedMessage(), e);
			throw new ResourceDAOReportingException("Error retrieving report template "
					+ reportTemplateName + " design");
		}
		return design;

	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public ReportTemplateInfo getReportTemplateInfo(String reportTemplateName) {

		if (log.isDebugEnabled()) {
			log.debug("Retreiving report template " + reportTemplateName);
		}

		// Create ReportTemplateInfo by DB objects
		Collection<TO<?>> parameters = CollectionUtils.newLinkedSet();
		ReportTemplateInfo rtInfo = new ReportTemplateInfo();
		int reportTemplateId;
		try {
			TO<?> reportTemplate = getObjectByName(
					ReportingConstants.TYPE.REPORT_TEMPLATE, reportTemplateName);
			if (reportTemplate == null) {
				if (log.isDebugEnabled()) {
					log.debug("Report template " + reportTemplateName
							+ " not found");
				}
				throw new ObjectNotFoundDAOReportingException("Report template "
						+ reportTemplateName + " not found");
			}
			reportTemplateId = (Integer) reportTemplate.getID();

			rtInfo.setReportTemplateName(reportTemplateName);
			rtInfo.setReportTemplateId(reportTemplateId);
			Integer system = (Integer) this.getPropertyValue(reportTemplate, ReportingConstants.FIELD.REPORT_TEMPLATE.SYSTEM, Integer.class);
			rtInfo.setSystem(Integer.valueOf(1).equals(system));

			parameters = this.getCollectionPropertyValue(reportTemplate,
					ReportingConstants.FIELD.REPORT_TEMPLATE.PARAMETERS);
		} catch (EnvDAOReportingException e) {
			log.error(e.getLocalizedMessage(), e);
			throw new ResourceDAOReportingException("Error retrieving report template "
					+ reportTemplateName + " template info");
		}
		
		// Add parameters to reportInfo
		Map<Integer, ReportParameter> parameterOrderMap = CollectionUtils.newMap();
		for (TO<?> parameter : parameters) {

			Map<String, ValueTO> parameterPropertiesMap = CollectionUtils
					.newMap();
			try {
				parameterPropertiesMap = this
						.getPropertyValues(
								parameter,
								new String[] {
										ReportingConstants.FIELD.REPORT_PARAMETER.NAME,
										ReportingConstants.FIELD.REPORT_PARAMETER.TYPE,
										ReportingConstants.FIELD.REPORT_PARAMETER.ORDER_ID
										, ReportingConstants.FIELD.REPORT_PARAMETER.DESCRIPTION
										});
			} catch (EnvDAOReportingException e) {
				log.error(e.getLocalizedMessage(), e);
				throw new ResourceDAOReportingException("Error retrieving report parameter"
						+ parameter.toString() + " properties");
			}
			String typeStr = (String) parameterPropertiesMap.get(
								ReportingConstants.FIELD.REPORT_PARAMETER.TYPE)
								.getValue();
			Class<?> clazz;
			try {
				clazz = Class.forName(typeStr);
			} catch (ClassNotFoundException e) {
				throw new ResourceDAOReportingException("Unable to cast parameter to type " + typeStr, e);

			}
			ValueTO description = (ValueTO) parameterPropertiesMap.get(ReportingConstants.FIELD.REPORT_PARAMETER.DESCRIPTION);
			String reportName =  (String) parameterPropertiesMap.get(ReportingConstants.FIELD.REPORT_PARAMETER.NAME).getValue();
			ReportParameter reportParameter = (description == null) ? new ReportParameter(reportName, clazz) : new ReportParameter(reportName, clazz, (String) description.getValue());
			int orderId = (Integer) parameterPropertiesMap.get(ReportingConstants.FIELD.REPORT_PARAMETER.ORDER_ID).getValue();
			parameterOrderMap.put(orderId, reportParameter);

		}
		for (int i = 0; i < parameterOrderMap.size(); i++) {
			rtInfo.addParameter(parameterOrderMap.get(i));
		}
		
		return rtInfo;

	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public Collection<String> getReportTemplates() {

		if (log.isDebugEnabled()) {
			log.debug("Retreiving report templates");
		}

		Collection<String> templates = getObjectNamesByType(ReportingConstants.TYPE.REPORT_TEMPLATE);

		if (log.isTraceEnabled() && !templates.isEmpty()) {
			log.trace("Report Templates : " + templates.toString());
		}
		List<String> sortedTemplates = CollectionUtils.newList(templates);
		Collections.sort(sortedTemplates);
		return sortedTemplates;
		
	}
	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public void removeReportTemplate(String reportTemplateName)
			throws ObjectNotFoundDAOReportingException {
		TO<?> object = getObjectByName(ReportingConstants.TYPE.REPORT_TEMPLATE,
				reportTemplateName);
		if (object == null) {
			if (log.isDebugEnabled()) {
				log.debug("Report template " + reportTemplateName
						+ " not found");
			}
			throw new ObjectNotFoundDAOReportingException("Report template "
					+ reportTemplateName + " not found");
		}
		removeReportTemplate(object);
	}
	
	private void removeReportTemplate(TO<?> reportTemplate)
			throws ObjectNotFoundDAOReportingException {
		
		//Remove report template parameters
		Collection<TO<?>> parameters;
		try {
		parameters = this.getCollectionPropertyValue(reportTemplate,
				ReportingConstants.FIELD.REPORT_TEMPLATE.PARAMETERS);
		} catch (EnvDAOReportingException e) {
			log.error(e.getLocalizedMessage(), e);
			throw new ResourceDAOReportingException("Error retrieving report template "
					+ reportTemplate + " template info");
		}
		for (TO<?> parameter : parameters) {
			try {
				removeObject(parameter);
			} catch (ObjectNotFoundDAOReportingException e) {
				log.error(e.getLocalizedMessage(), e);
				throw new ResourceDAOReportingException("Object " + parameter.getTypeName()
						+ " and objectId " + parameter.getID() + " remove execution error",
						e);
			}
		}
		//Remove report template tasks
		Collection<TO<?>> reportTemplateTasks = this.getObjectsByProperty(
				this.getObjectsByType(ReportingConstants.TYPE.REPORT_TASK)
				, ReportingConstants.FIELD.REPORT_TASK.TEMPLATE_ID, reportTemplate.getID());
		for (TO<?> reportTask : reportTemplateTasks) {
			removeReportParamtersValues(reportTask);
			try {
				removeObject(reportTask);
			} catch (EnvDAOReportingException e) {
				log.error(e.getLocalizedMessage(), e);
				throw new ResourceDAOReportingException("Object " + reportTask + " remove execution error",
						e);
			}
		}
		//Remove report template

		String templateName;
		try {
			templateName = (String) getPropertyValue(reportTemplate,
					ReportingConstants.FIELD.REPORT_TEMPLATE.NAME, String.class);
		} catch (EnvDAOReportingException e) {
			throw new ResourceDAOReportingException(
					"Error retrieving name for report template "
							+ reportTemplate.toString());
		}
		try {
			removeObject(reportTemplate);
			if (log.isDebugEnabled()) {
				log.debug("Report template " + reportTemplate + " removed");
			}
		} catch (EnvDAOReportingException e) {
			log.error(e.getLocalizedMessage(), e);
			throw new ResourceDAOReportingException(
					"Report template remove execution error", e);
		}
		if (!FileUtils.deleteDirectory(new File(fileStorage, templateName))) {
			log.error("Error removing report template " + templateName + " folder");
			throw new ResourceDAOReportingException("Error removing report template " + templateName + " folder");
		}
		
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public void updateReportTemplate(ReportTemplateInfo reportTemplateInfo,
			String reportTemplateDesign)
			throws ObjectNotFoundDAOReportingException, ObjectExistsException {

		if (reportTemplateInfo.getReportTemplateName() == null) {
			throw new IllegalInputParameterException(
					"Report template name shouldn't be null");
		}

		if (reportTemplateDesign == null) {
			throw new IllegalInputParameterException(
					"Report template design shouldn't be null");
		}

		if (log.isDebugEnabled()) {
			log.debug("Updating report template "
					+ reportTemplateInfo.getReportTemplateId());
		}

		// Remove old version of template from DB. Such needed because of hidden
		// complex structure of template design
		removeReportTemplate(this.getObjectById(
				ReportingConstants.TYPE.REPORT_TEMPLATE,
				reportTemplateInfo.getReportTemplateId()));

		// Create updated report template
		createReportTemplate(reportTemplateInfo,
				reportTemplateDesign);

	}
	
	private Collection<TO<?>> persistReportTemplateParameters(
			ReportTemplateInfo reportTemplateInfo,
			Collection<ReportParameter> parameters) {
		
		Collection<TO<?>> templateParameters = CollectionUtils.newList();
		if (parameters.isEmpty()) {
			return templateParameters;
		}

		int rtId = reportTemplateInfo.getReportTemplateId();
		
		// Create each parameter
		int orderId = 0;
		for (ReportParameter parameter : parameters) {
			templateParameters
					.add(createObject(
							ReportingConstants.TYPE.REPORT_PARAMETER,
							new ValueTO[] {
									new ValueTO(
											ReportingConstants.FIELD.REPORT_PARAMETER.NAME,
											parameter.getName()),
									new ValueTO(
											ReportingConstants.FIELD.REPORT_PARAMETER.TYPE,
											parameter.getType().getName()),
									new ValueTO(
											ReportingConstants.FIELD.REPORT_PARAMETER.TEMPLATE_ID,
											rtId), 
									new ValueTO(
											ReportingConstants.FIELD.REPORT_PARAMETER.ORDER_ID,
											orderId++)
									, new ValueTO(
											ReportingConstants.FIELD.REPORT_PARAMETER.DESCRIPTION,
											parameter.getDesription())
												}));
		}

		return templateParameters;

	}

	private boolean isReportTemplateExists(String reportTemplateName) {

		return getObjectByName(ReportingConstants.TYPE.REPORT_TEMPLATE,
				reportTemplateName) != null ? true : false;

	}
	
	private Collection<String> getObjectNamesByType(String objectType) {

		final Set<TO<?>> allObjects = CollectionUtils.newSet();
		allObjects.addAll(getObjectsByType(objectType));
		// if no objects
		if (allObjects.isEmpty()) {
			return Collections.emptyList();
		}

		Collection<String> objects = CollectionUtils.newList();
		Collection<CollectionTO> allNames = environment.getPropertyValue(
				allObjects, new String[] { "name" });
		for (CollectionTO nameCollection : allNames) {
			objects.add(nameCollection.getSinglePropValue("name").getValue()
					.toString());
		}
		return objects;

	}

}
