package com.synapsense.impl.alerting.macros;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class AbstractPatternMacro implements Macro {

	@Override
	public String expandIn(String input, MacroContext ctx) {
		StringBuffer sb = new StringBuffer();
		Matcher m = getPattern().matcher(input);
		// scan input string for this macro's pattern
		while (m.find()) {
			m.appendReplacement(sb, expand(m, ctx));
		}
		m.appendTail(sb);
		return sb.toString();
	}

	/**
	 * 
	 * @param m
	 * @param ctx
	 * @return Replacement string. The string will be used as an argument for
	 *         Matcher.appendReplacement() method thus all requirements and
	 *         syntax apply.
	 */
	protected abstract String expand(Matcher m, MacroContext ctx);

	protected abstract Pattern getPattern();
}
