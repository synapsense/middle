package com.synapsense.impl.alerting.macros;

import com.synapsense.util.CollectionUtils;

import java.util.Collection;

public class AlertMacroFactory {

	public static Macro getAllMacros() {
		Macro formatting = getFormattingMacros();
		Macro image = new PrivateContextMacro(new MacrosCollection(new ImageMacro(ImageMacro.ImageLevel.DC),
		        new ImageMacro(ImageMacro.ImageLevel.ROOM)));
		// then go macros related to conversion
		return new MacrosCollection(image, formatting);
	}

	public static Macro getFormattingMacros() {
		Collection<Macro> macros = CollectionUtils.newList();
        // don't change an order of macros
		// message macro must be first in a list because it contains other macros
		// then go simple one-step macros
		macros.add(new TimestampMacro());
		macros.add(new AlertNameMacro());
		macros.add(new AlertDescrMacro());
		macros.add(new AlertIdMacro());
		macros.add(new ConsoleUrlMacro());
		macros.add(new MessageTierMacro());
		macros.add(new MessageNumMacro());
		macros.add(new TriggerDataValueMacro());
		macros.add(new CurrentDataValueMacro());
		macros.add(new MessageMacro());
		// 
		macros.add(new AlertHostPropertyMacro());
		macros.add(new EnvPropertyMacro());
		macros.add(new RelationMacro());
		macros.add(new ConversionMacro());
		macros.add(new LocalizeMacro());

		return new MacrosCollection(macros);
	}

    private AlertMacroFactory() {
    }
}
