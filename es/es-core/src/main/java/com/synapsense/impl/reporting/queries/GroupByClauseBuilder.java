package com.synapsense.impl.reporting.queries;

import java.util.List;

import org.apache.log4j.Logger;

import com.synapsense.exception.IllegalInputParameterException;
import com.synapsense.impl.reporting.queries.TableProcessor.GroupType;
import com.synapsense.impl.reporting.queries.TableProcessor.QueryClause;
import com.synapsense.impl.reporting.queries.types.ReportingQueryType;
import com.synapsense.impl.reporting.queries.types.ReportingQueryTypeFactory;
import com.synapsense.impl.reporting.utils.ReportingConstants.QUERY.SYMBOL;
import com.synapsense.service.reporting.Cell;
import com.synapsense.service.reporting.olap.AxisSet;
import com.synapsense.service.reporting.olap.custom.Cube;
import com.synapsense.service.reporting.olap.custom.Level;

/**
 * The Class <code>GroupByClauseBuilder</code> construct where clause for Environment Data Query
 */
public class GroupByClauseBuilder implements ClauseBuilder {

	private static final Logger log = Logger.getLogger(GroupByClauseBuilder.class);
	
	String valueIdsGroupingItems;
	
	String timeGroupingItems;

	private Level lowestTimeLevel;

	public GroupByClauseBuilder(Level lowestTimeLevel) {
		this.lowestTimeLevel = lowestTimeLevel;
	}

	@Override
	public String build(TableProcessor owner) {

		if (owner == null) {
			throw new IllegalInputParameterException("TableProcessor shouldn't be null");
		}
		StringBuilder groupBySB = new StringBuilder();
		groupBySB.append(QueryClause.GROUP_BY.getName())
			.append(SYMBOL.SPACE);
		processTimeGrouping(lowestTimeLevel);
		
		if (!timeGroupingItems.isEmpty()) {
			groupBySB.append(timeGroupingItems)
				.append(SYMBOL.COMMA)
				.append(SYMBOL.SPACE);
		}
		processValueIdGrouping(owner.getValueIdCells(), owner.getQuery()
				.getSlicer()
				, owner.getQueryType()
				);
		groupBySB.append(valueIdsGroupingItems);
		if (log.isTraceEnabled()) {
			log.trace("GetEnvData GROUP BY clause: " + groupBySB.toString());
		}		
		return groupBySB.toString();
		
	}
	
	protected String getValueIdsGroupingItems() {
		return valueIdsGroupingItems;
	}

	protected String getTimeGroupingItems() {
		return timeGroupingItems;
	}

	/**
	 * Process time grouping: Adds time clause items for grouping
	 *
	 * @param lowestTimeLevel the lowest time level
	 */
	private void processTimeGrouping(Level lowestLevel) {
		String lowestTimeLevel = lowestLevel.getName();
		if (lowestTimeLevel == null) {
			throw new IllegalInputParameterException("Lowest Time Level shouldn't be null");
		}
		if (lowestTimeLevel.equals(Cube.ALL_RANGE)) {
			timeGroupingItems = "";
			return;
		}
		StringBuilder timeGroupSB = new StringBuilder();
		String level = lowestTimeLevel;
		while(level != null){
			timeGroupSB.append(GroupType.getTypeByName(level).getGroupType())
					.append(SYMBOL.COMMA)
					.append(SYMBOL.SPACE);
			level = Cube.getBaseHierarchy(lowestLevel.getHierarchy().getName()).getParentLevel(level);
		}
		timeGroupingItems = timeGroupSB.substring(0, timeGroupSB.length() - 2);
		
	}
	
	/**
	 * Process value id grouping: Adds all unique separate valueId groups for each valueIdCell
	 *
	 * @param valueIdCells the value id cells
	 */
	private void processValueIdGrouping(List<Cell> valueIdCells,
			AxisSet slicer, 
            ReportingQueryType queryType
			) {

		if (valueIdCells.isEmpty()) {
			throw new IllegalInputParameterException("Value Id Cells List shouldn't be empty");
		}
		StringBuilder cellValueIdsSB = new StringBuilder();
		
		cellValueIdsSB.append(ReportingQueryTypeFactory.getClauseBuilder(
				queryType).build(QueryClause.GROUP_BY, valueIdCells));
		
		valueIdsGroupingItems = cellValueIdsSB.substring(0, cellValueIdsSB.length() - 2);
		
	}

}
