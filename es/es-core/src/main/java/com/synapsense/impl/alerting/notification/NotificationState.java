package com.synapsense.impl.alerting.notification;

class NotificationState {

	private final String name;
	private final int repeatLimit;
	private final long interval;
	private final int ordinal;
	private int lastAttempt;

	public NotificationState(final String name, final int ordinal, final int repeatLimit, final long interval,
	        int lastAttempt) {
		if (repeatLimit < 0 || repeatLimit > 100) {
			throw new IllegalArgumentException("Supplied 'repeatLimit'=" + repeatLimit + " is out of range [0..100]");
		}

		if (interval < 1000) {
			throw new IllegalArgumentException("Supplied 'interval' value minimum is 1 second");
		}

		if (lastAttempt < 0 || lastAttempt > repeatLimit) {
			throw new IllegalArgumentException("Supplied 'lastAttempt' = " + lastAttempt + " is out of range [0.."
			        + repeatLimit + "]");
		}

		this.name = name;
		this.ordinal = ordinal;
		this.repeatLimit = repeatLimit;
		this.interval = interval;
		this.lastAttempt = lastAttempt;
	}

	public NotificationState(final String name, final int ordinal, final int repeatLimit, final long interval) {
		this(name, ordinal, repeatLimit, interval, 0);
	}

	public String getName() {
		return name;
	}

	public int getOrdinal() {
		return ordinal;
	}

	public int getRepeatLimit() {
		return repeatLimit;
	}

	public long getInterval() {
		return interval;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (interval ^ (interval >>> 32));
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ordinal;
		result = prime * result + repeatLimit;
		result = prime * result + lastAttempt;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NotificationState other = (NotificationState) obj;
		if (interval != other.interval)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (ordinal != other.ordinal)
			return false;
		if (repeatLimit != other.repeatLimit)
			return false;
		if (lastAttempt != other.lastAttempt)
			return false;
		return true;
	}

	public void setLastAttempt(int lastAttempt) {
		this.lastAttempt = lastAttempt;
	}

	public int getLastAttempt() {
		return lastAttempt;
	}

}