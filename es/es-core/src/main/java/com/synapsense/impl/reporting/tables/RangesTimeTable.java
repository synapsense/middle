package com.synapsense.impl.reporting.tables;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.synapsense.impl.reporting.queries.TableProcessor.GroupType;
import com.synapsense.impl.reporting.utils.ReportingConstants.QUERY.SYMBOL;
import com.synapsense.impl.reporting.utils.ReportingConstants.QUERY.WORD;
import com.synapsense.service.reporting.olap.BaseHierarchy;
import com.synapsense.service.reporting.olap.Member;
import com.synapsense.service.reporting.olap.SimpleMember;
import com.synapsense.service.reporting.olap.custom.Cube;
import com.synapsense.util.CollectionUtils;
import com.synapsense.util.Pair;

/**
 * The <code>RangesTimeTable</code> class contains Time Ranges Map.<>br Clause
 * Example: WHERE clause item will be like DATE_FORMAT(timestamp, '%Y %m %d %H')
 * between '2013 01 24 00' and '2013 01 24 22'
 * 
 * @author Grigory Ryabov
 */
public class RangesTimeTable extends MembersTimeTable {

	private static final long serialVersionUID = -8394136044373967114L;
	
	private static Map<String, RangeBuilder> rangeBuilderMap = CollectionUtils.newMap();
	
	static {
		rangeBuilderMap.put(Cube.DIM_WEEK_TIME, new WeekRangeBuilder());
		rangeBuilderMap.put(Cube.DIM_TIME, new MonthRangeBuilder());
	}
	
	public static interface RangeBuilder {

		Pair<String, String> getEdgeTimeValue(Member member, Map<String, List<String>> timeMembers);
		
		String getClauseItem(Member member);

	}
	
	public static class WeekRangeBuilder implements RangeBuilder{
		
		@Override
		public Pair<String, String> getEdgeTimeValue(Member member, Map<String, List<String>> timeMembers) {
			return Pair.newPair(GroupType.getWeekType().getFormat(), convertValue((Integer) member.getKey() - 1));			
		}

		@Override
		public String getClauseItem(Member member) {
			return (member.getLevel().equals(Cube.LEVEL_WEEK)) ? GroupType
					.getWeekType().getFormat() : GroupType.getTypeByName(
					member.getLevel()).getFormat();
		}
		
	}
	
	public static class MonthRangeBuilder implements RangeBuilder{

		@Override
		public Pair<String, String> getEdgeTimeValue(Member member, Map<String, List<String>> timeMembers) {
			
			Member current = member;
			String startLevelName = member.getLevel();
			String levelName = startLevelName;
			BaseHierarchy currentHierarchy = Cube.getBaseHierarchy(member.getHierarchy().getName());
			
			while (currentHierarchy.isDescendant(Cube.LEVEL_MONTH, levelName)
					&& !levelName.equals(Cube.LEVEL_DAY)) {
				levelName = currentHierarchy.getChildLevel(levelName);
				current = new SimpleMember(currentHierarchy, levelName, 1, current);
			}
			
			StringBuilder valueSB = new StringBuilder();
			StringBuilder clauseSB = new StringBuilder();
			List<String> clauses = new ArrayList<>();
			List<String> values = new ArrayList<>();
			while (current != null) {
				values.add(convertValue((Integer) current.getKey()));
				clauses.add(GroupType.getTypeByName(current.getLevel()).getFormat());
				current = current.getParent();
			}
			for (int i = clauses.size() - 1; i >= 0; i--) {
				clauseSB.append(clauses.get(i))
					.append(SYMBOL.DASH);
				valueSB.append(values.get(i))
					.append(SYMBOL.DASH);
			}
			String clauseItem = clauseSB.substring(0, clauseSB.length() - 1);
			String value = valueSB.substring(0, valueSB.length() - 1);
			List<String> valuesSet = CollectionUtils.newList();
			valuesSet.add(value);
			return Pair.newPair(clauseItem, value);
			
		}

		@Override
		public String getClauseItem(Member member) {
			//Since Month and Year requests like 'between 2013-08 and 2013-09' are not acceptable we decrease level to Day
			Member current = member;
			String startLevelName = member.getLevel();
			String levelName = startLevelName;
			BaseHierarchy hierarchy = Cube.getBaseHierarchy(member.getHierarchy().getName());
			if (hierarchy.equals(Cube.WEEK) 
					&& current.getLevel().equals(Cube.LEVEL_WEEK)) {
				return GroupType.getWeekType().getFormat();
			}
			
			while (hierarchy.isDescendant(Cube.LEVEL_MONTH, levelName)
					&& !levelName.equals(Cube.LEVEL_DAY)) {
				levelName = hierarchy.getChildLevel(levelName);
				current = new SimpleMember(hierarchy, levelName, 1, current);
			}
			StringBuilder clauseSB = new StringBuilder();
			List<String> clauses = new ArrayList<>();
			while (current != null) {
				String format = GroupType.getTypeByName(current.getLevel()).getFormat();
				clauses.add(format);
				current = current.getParent();
			}
			for (int i = clauses.size() - 1; i >= 0; i--) {
				clauseSB.append(clauses.get(i)).append(SYMBOL.DASH);
			}
			return clauseSB.substring(0, clauseSB.length() - 1);
		}
		
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public String get() {
		
		StringBuilder timeRangesSB = new StringBuilder();
		for (Entry<String, List<String>> entry : timeMembers.entrySet()) {
			StringBuilder timeRangeEntrySB = new StringBuilder();
			String clauseFunction = getClauseFunction(entry.getKey());
			timeRangeEntrySB.append(clauseFunction)
					.append(SYMBOL.SPACE)
					.append("between");
			Iterator<String> iterator = entry.getValue().iterator();
			String start = iterator.next();
			String end = (iterator.hasNext()) ? iterator.next() : start;
			timeRangeEntrySB.append(SYMBOL.SPACE)
					.append(SYMBOL.APOSTROPHE).append(start)
					.append(SYMBOL.APOSTROPHE)
					.append(SYMBOL.SPACE)
					.append(WORD.AND)
					.append(SYMBOL.SPACE)
					.append(SYMBOL.APOSTROPHE).append(end)
					.append(SYMBOL.APOSTROPHE)
					.append(SYMBOL.SPACE)
					.append(WORD.AND)
					.append(SYMBOL.SPACE);
			timeRangesSB
					.append(timeRangeEntrySB.substring(0,
							timeRangeEntrySB.length() - 5))
					.append(SYMBOL.SPACE)
					.append(WORD.AND)
					.append(SYMBOL.SPACE);
		}
		return timeRangesSB.substring(0, timeRangesSB.length() - 5);
		
	}

	@Override
	public void add(Member member) {
		
		List<Member> members = member.getSimpleMembers();
		if (!Cube.getOrderedHierarchy(member.getHierarchy().getName()).equals(hierarchy)) {			
			hierarchy = Cube.getOrderedHierarchy(member.getHierarchy().getName());
		}
		if (getMembers(member).isEmpty()) {
			Member startMember = members.iterator().next();
			addEdgeTimeValue(startMember);
		} else {
			Member endMember = this.hierarchy.getNext(members.get(members.size() - 1));
			addEdgeTimeValue(endMember);
		}
		
	}

	private String getClauseFunction(String clause) {
		return clause.contains("%Y") 
				? "timestamp"
					: "DATE_FORMAT(timestamp, '" + clause +"')";
	}
	
	private void addEdgeTimeValue(Member member) {
		
		Pair<String, String> entry = rangeBuilderMap.get(hierarchy.getName()).getEdgeTimeValue(member, timeMembers);
		if (timeMembers.containsKey(entry.getFirst())) {
			timeMembers.get(entry.getFirst()).add(entry.getSecond());
		} else {
			List<String> valuesSet = CollectionUtils.newList();
			valuesSet.add(entry.getSecond());
			timeMembers.put(entry.getFirst(), valuesSet);
		}	
		setLowestTimeLevel(member.getLevel());
		
	}

	private List<String> getMembers(Member member) {
		
		List<String> itemMembers = CollectionUtils.newList();
		String clauseItem = rangeBuilderMap.get(member.getHierarchy().getName()).getClauseItem(member);
		if (timeMembers.containsKey(clauseItem)) {
			itemMembers = timeMembers.get(clauseItem);
		}
		return itemMembers;
	}

	@Override
	public RangesTimeTable getTable() {
		
		RangesTimeTable copy = new RangesTimeTable();
		for (Entry<String, List<String>> entry : timeMembers.entrySet()) {
			copy.put(entry.getKey(), entry.getValue());
		}
		copy.setLowestTimeLevel(lowestTimelevel);
		return copy;
		
	}

}
