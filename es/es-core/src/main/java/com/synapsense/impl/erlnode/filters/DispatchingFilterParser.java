package com.synapsense.impl.erlnode.filters;

import com.ericsson.otp.erlang.OtpErlangBoolean;
import com.ericsson.otp.erlang.OtpErlangObject;
import com.ericsson.otp.erlang.OtpErlangTuple;
import com.synapsense.impl.erlnode.commands.RegisterProcessorCommand;
import com.synapsense.service.nsvc.EventProcessor;
import com.synapsense.service.nsvc.events.DispatchingProcessor;

public class DispatchingFilterParser implements FilterDefParser {
	
	private static final ObjectCreatedEventFilterParser ocefp =
		new ObjectCreatedEventFilterParser();
	private static final ObjectDeletedEventFilterParser odefp =
		new ObjectDeletedEventFilterParser();
	private static final PropertiesChangedEventFilterParser pcefp =
		new PropertiesChangedEventFilterParser();
	private static final RelationSetEventFilterParser rsefp =
		new RelationSetEventFilterParser();
	private static final RelationRemovedEventFilterParser rrefp =
		new RelationRemovedEventFilterParser();

	@Override
	public EventProcessor parse(OtpErlangTuple filterDef) {
		DispatchingProcessor processor = new DispatchingProcessor();
		try {
			boolean bypass = ((OtpErlangBoolean)filterDef.elementAt(1)).booleanValue();
			processor.setBypass(bypass);
		} catch (ClassCastException e) {
			throw new IllegalArgumentException("The second argument of dispatching_filter must be a boolean but was"+
					filterDef.elementAt(1).getClass().getName(), e);
		}
		//only five simple filter definitions are known so far
		//and only one of each can be specified
		for(int i=2; i<7; i++) {
			OtpErlangTuple simpleFilterDef;
			try {
				OtpErlangObject tmp = filterDef.elementAt(i);
				if(tmp==null || RegisterProcessorCommand.undefined.equals(tmp))
					continue;
				simpleFilterDef = (OtpErlangTuple) tmp;
			} catch (ClassCastException e) {
				throw new IllegalArgumentException("Argument #"+i+" of dispatching_filter must be a tuple but was"+
						filterDef.elementAt(i).getClass().getName(), e);
			}
			String simpleFilterName = simpleFilterDef.elementAt(0).toString();
			if("object_created_event_filter".equals(simpleFilterName)) {
				if(processor.getOcep()!=null)
					throw new IllegalArgumentException("object_created_event_filter defined twice in dispatching_filter");
				processor.putOcep(ocefp.parse(simpleFilterDef));
			} else if("object_deleted_event_filter".equals(simpleFilterName)) {
				if(processor.getOdep()!=null)
					throw new IllegalArgumentException("object_deleted_event_filter defined twice in dispatching_filter");
				processor.putOdep(odefp.parse(simpleFilterDef));
			} else if("properties_changed_event_filter".equals(simpleFilterName)) {
				if(processor.getPcep()!=null)
					throw new IllegalArgumentException("properties_changed_event_filter defined twice in dispatching_filter");
				processor.putPcep(pcefp.parse(simpleFilterDef));
			} else if("relation_set_event_filter".equals(simpleFilterName)) {
				if(processor.getRsep()!=null)
					throw new IllegalArgumentException("relation_set_event_filter defined twice in dispatching_filter");
				processor.putRsep(rsefp.parse(simpleFilterDef));
			} else if("relation_removed_event_filter".equals(simpleFilterName)) {
				if(processor.getRrep()!=null)
					throw new IllegalArgumentException("relation_removed_event_filter defined twice in dispatching_filter");
				processor.putRrep(rrefp.parse(simpleFilterDef));
			} else {
				throw new IllegalArgumentException("Unknown simple filter "
						+simpleFilterName);
			}
		}
		return processor;
	}

}
