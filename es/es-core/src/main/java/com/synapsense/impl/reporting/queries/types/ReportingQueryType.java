package com.synapsense.impl.reporting.queries.types;

public enum ReportingQueryType {
    SINGLE_PROPERTY_CELL_TABLE,
    SAME_MULTIPLE_PROPERTY_CELL_TABLE,
    MULTIPLE_OBJECT_CELL_TABLE,    
    DIFFERENT_MULTIPLE_PROPERTY_CELL_TABLE;    
}
