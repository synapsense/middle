package com.synapsense.impl.transport;


public interface TransportManagerImplMBean {

	void start() throws Exception;

	void stop() throws Exception;
	
	String listServerTransports();

}
