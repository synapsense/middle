package com.synapsense.impl.diagnostic;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.math.RoundingMode;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.Map.Entry;

import org.apache.log4j.Logger;

public class SystemInfo {
	private static final Logger LOG = Logger.getLogger(SystemInfo.class);
	private static final String TITLE_WRAPPER = "*****";
	private static final String ENV_VARIABLES_TITLE = "System Environment Variables";
	private static final String SYSTEM_PROPS_TITLE = "System Properties";
	private static final String SYSTEM_ROOTS_TITLE = "System Roots Info";
	private static final String SYSTEM_INFO_TITLE = "System Info";
	private static final String JAVA_MEMORY_TITLE = "Java Runtime memory";
	private static NumberFormat numberFormatter;
	private static ThreadLocal<FileWriter> fileWriter = new ThreadLocal<FileWriter>();

	static {
		numberFormatter = NumberFormat.getInstance(Locale.US);
		numberFormatter.setRoundingMode(RoundingMode.HALF_UP);
		numberFormatter.setGroupingUsed(false);
		numberFormatter.setMaximumFractionDigits(2);
	}

	public static void writeInfo(String destinationFileName) throws IOException {
		try {
			fileWriter.set(new FileWriter(destinationFileName));

			try {
				writeOSMemory();
				writeJavaMemory();
				writeDisksSpace();
				writeSystemEnv();
				writeSystemProps();
			} catch (Throwable e) {
				LOG.error(e.getLocalizedMessage(), e);
			} finally {
				fileWriter.get().close();
				fileWriter.set(null);
			}
		} catch (Throwable e) {
			LOG.error(e.getLocalizedMessage(), e);
		}
	}

	/**
	 * Writes system properties
	 * 
	 * @throws IOException
	 */
	private static void writeSystemProps() throws IOException {
		try {
			addHeaderLineToInfo(SYSTEM_PROPS_TITLE);
			Properties ps = System.getProperties();
			for (Object obj : ps.entrySet()) {
				addLineToInfo(obj.toString());
			}
		} catch (IOException e) {
			throw e;
		} catch (Throwable e) {
			LOG.error(e.getLocalizedMessage(), e);
		}
	}

	/**
	 * Writes environmental properties
	 * 
	 * @throws IOException
	 */
	private static void writeSystemEnv() throws IOException {
		try {
			addHeaderLineToInfo(ENV_VARIABLES_TITLE);
			Map<String, String> envVariables = System.getenv();
			for (Entry<String, String> entry : envVariables.entrySet()) {
				addLineToInfo(entry.getKey() + " = " + entry.getValue());
			}
		} catch (IOException e) {
			throw e;
		} catch (Throwable e) {
			LOG.error(e.getLocalizedMessage(), e);
		}
	}

	/**
	 * Writes disks space
	 * 
	 * @throws IOException
	 */
	private static void writeDisksSpace() throws IOException {
		try {
			addHeaderLineToInfo(SYSTEM_ROOTS_TITLE);
			File[] roots = File.listRoots();
			for (File root : roots) {
				addLineToInfo("File system root: '" + root.getAbsolutePath() + "'");
				addLineToInfo("Total space: " + formatBytesSize(root.getTotalSpace()));
				addLineToInfo("Free space: " + formatBytesSize(root.getFreeSpace()));
				addLineToInfo("Usable space: " + formatBytesSize(root.getUsableSpace()));
				addLineToInfo("");
			}
		} catch (IOException e) {
			throw e;
		} catch (Throwable e) {
			LOG.error(e.getLocalizedMessage(), e);
		}
	}

	/**
	 * Writes operating system memory
	 * 
	 * @throws IOException
	 */
	private static void writeOSMemory() throws IOException {
		try {
			java.lang.management.OperatingSystemMXBean operatingSystemMXBean = ManagementFactory
			        .getOperatingSystemMXBean();
			if (operatingSystemMXBean instanceof com.sun.management.OperatingSystemMXBean) {
				addHeaderLineToInfo(SYSTEM_INFO_TITLE);
				com.sun.management.OperatingSystemMXBean osMXBean = (com.sun.management.OperatingSystemMXBean) operatingSystemMXBean;
				addLineToInfo("TotalPhysicalMemorySize: " + formatBytesSize(osMXBean.getTotalPhysicalMemorySize()));
				addLineToInfo("FreePhysicalMemorySize: " + formatBytesSize(osMXBean.getFreePhysicalMemorySize()));
			}
		} catch (IOException e) {
			throw e;
		} catch (Throwable e) {
			LOG.error(e.getLocalizedMessage(), e);
		}
	}

	/**
	 * @throws IOException
	 * 
	 */
	private static void writeJavaMemory() throws IOException {
		try {
			addHeaderLineToInfo(JAVA_MEMORY_TITLE);

			Runtime rt = Runtime.getRuntime();
			addLineToInfo("Free memory: " + formatBytesSize(rt.freeMemory()));
			addLineToInfo("Total memory: " + formatBytesSize(rt.totalMemory()));
			addLineToInfo("Max memory: " + formatBytesSize(rt.maxMemory()));
		} catch (IOException e) {
			throw e;
		} catch (Throwable e) {
			LOG.error(e.getLocalizedMessage(), e);
		}
	}

	/**
	 * 
	 * @param sizeInBytes
	 *            - size in bytes
	 * @return
	 */
	private static String formatBytesSize(long sizeInBytes) {
		final String[] units = { "bytes", "KB", "MB", "GB" };
		double castedSize = sizeInBytes;

		int i = 0;
		for (i = 0; i < units.length; i++) {
			if (castedSize / 1024.0 > 1) {
				castedSize = castedSize / 1024.0;
			} else {
				break;
			}
		}
		i = i < units.length ? i : units.length - 1;

		String formattedString = numberFormatter.format(castedSize) + " " + units[i];
		if (i != 0) {
			formattedString += " (" + sizeInBytes + " bytes)";
		}
		return formattedString;
	}

	/**
	 * Adds line to info
	 * 
	 * @param line
	 * @throws IOException
	 */
	private static void addLineToInfo(String line) throws IOException {
		fileWriter.get().write(line + "\n");
	}

	private static void addHeaderLineToInfo(String header) throws IOException {
		addLineToInfo("");
		addLineToInfo(TITLE_WRAPPER + " " + header + " " + TITLE_WRAPPER);
	}
}
