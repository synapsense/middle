package com.synapsense.impl.alerting.notification.smtp;

import org.apache.commons.mail.HtmlEmail;

import java.util.Date;

/**
 * Attachment interface
 * 
 * @author : shabanov
 * @version 0.1
 */

interface Attachment {
	static final InlinedAttachment EMPTY = new InlinedAttachment("-", new Date(0));

	public class InlinedAttachment {
		private Date timestamp;
		private String markup;

		public InlinedAttachment(String markup, Date timestamp) {
			this.markup = markup;
			this.timestamp = timestamp;
		}

		public InlinedAttachment(String markup) {
			this(markup, new Date());
		}

		/**
		 * Time of creation
		 * 
		 * @return String representing time stamp
		 */
		public Date createdAt() {
			return timestamp;
		}

		/**
		 * @return String view of inlined attachment
		 */
		public String getMarkup() {
			return markup;
		}
	}

	/**
	 * It can have name
	 * 
	 * @return Name
	 */
	String getName();

	/**
	 * Attachment description getter
	 * 
	 * @return
	 */
	String getDescription();

	/**
	 * Inline attachment into given email
	 * 
	 * @param email
	 *            Email with html support
	 * @return String representation of attachment inside email
	 */
	InlinedAttachment inline(HtmlEmail email);
}
