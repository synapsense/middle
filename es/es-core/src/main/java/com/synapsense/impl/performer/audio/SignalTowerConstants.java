package com.synapsense.impl.performer.audio;

public interface SignalTowerConstants {
	String RED_LED_OID = ".1.3.6.1.4.1.20916.1.6.2.1.0";
	String GREEN_LED_OID = ".1.3.6.1.4.1.20916.1.6.2.3.0";
	String YELLOW_LED_OID = ".1.3.6.1.4.1.20916.1.6.2.2.0";
	String SIGNAL_OID = ".1.3.6.1.4.1.20916.1.6.2.6.0";

	String ALERT_PRIORITY = "ALERT_PRIORITY";
	String ALERT_PRIORITY_INFORMATIONAL = "INFORMATIONAL";
	String ALERT_PRIORITY_MINOR = "MINOR";
	String ALERT_PRIORITY_MAJOR = "MAJOR";
	String ALERT_PRIORITY_CRITICAL = "CRITICAL";
}
