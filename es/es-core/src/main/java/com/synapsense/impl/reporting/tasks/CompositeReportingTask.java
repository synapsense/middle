package com.synapsense.impl.reporting.tasks;

import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;

import com.synapsense.util.CollectionUtils;

/**
 * Composite Task composed of collection of tasks
 * 
 * @author Grigory Ryabov
 */
public class CompositeReportingTask implements ReportingTask{

	private static final Logger log = Logger.getLogger(CompositeReportingTask.class);
	
	private List<ReportingTask> tasks = CollectionUtils.newList();;

	public void addTask(ReportingTask task) {
		tasks.add(task);
	}
	
	public List<ReportingTask> getTasks() {
		
		return Collections.unmodifiableList(tasks);
		
	}

	@Override
	public void execute() {
		
		for (ReportingTask task : tasks){
			if (log.isDebugEnabled()) {
				log.debug("Starting " + task.getName() + "...");
			}
			task.execute();
		}
		return;
		
	}

	public int getTasksSize() {
		return tasks.size();
	}
	
	@Override
	public String getName() {
		
		return "CompositeReportingTask";
		
	}
	
}