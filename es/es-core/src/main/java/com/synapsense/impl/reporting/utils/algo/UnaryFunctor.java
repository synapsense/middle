package com.synapsense.impl.reporting.utils.algo;

public interface UnaryFunctor<T> {
	void process(T value);
}
