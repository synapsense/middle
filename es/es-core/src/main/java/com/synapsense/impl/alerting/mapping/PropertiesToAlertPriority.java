package com.synapsense.impl.alerting.mapping;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import com.synapsense.dto.AlertPriority;
import com.synapsense.dto.PriorityTier;
import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.impl.utils.EnvironmentUtils;
import com.synapsense.service.Environment;
import com.synapsense.util.CollectionUtils;
import com.synapsense.util.CollectionUtils.Functor;

public class PropertiesToAlertPriority implements Functor<AlertPriority, Collection<ValueTO>> {

	public PropertiesToAlertPriority(Environment env) {
		super();
		this.env = env;
	}

	private Environment env;

	@Override
	public AlertPriority apply(Collection<ValueTO> input) {
		Map<String, Object> values = EnvironmentUtils.valuesAsMap(input);
		String name = (String) values.get("name");
		long dismissInterval = (Long) values.get("dismissInterval");
		Collection<TO<?>> tiers = (Collection<TO<?>>) values.get("tiers");
		List<PriorityTier> tiersToSet = CollectionUtils.newList();
		for (TO<?> ref : tiers) {
			Collection<ValueTO> tierValues;
			try {
				tierValues = env.getAllPropertiesValues(ref);
			} catch (ObjectNotFoundException e) {
				continue;
			}
			PriorityTier tier = new PropertiesToTier().apply(tierValues);
			tiersToSet.add(tier);
		}
		Boolean redSignal = ((Integer) values.get("redSignal")) == 1 ? true : false;
		Boolean greenSignal = ((Integer) values.get("greenSignal")) == 1 ? true : false;
		Boolean yellowSignal = ((Integer) values.get("yellowSignal")) == 1 ? true : false;
		Boolean audioSignal = ((Integer) values.get("audioSignal")) == 1 ? true : false;
		Long audioDuration = (Long) values.get("audioDuration");
		Long lightDuration = (Long) values.get("lightDuration");
		Boolean ntLog = ((Integer) values.get("ntLog")) == 1 ? true : false;
		AlertPriority priority = new AlertPriority(name, dismissInterval, tiersToSet, redSignal, greenSignal,
		        yellowSignal, lightDuration, audioSignal, audioDuration, ntLog);
		return priority;
	}
}
