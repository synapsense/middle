package com.synapsense.impl.alerting.macros;

public interface Macro {

	String expandIn(String input, MacroContext ctx);

	String DEFAULT_EXPANSION = "N/A";

    String EMPTY_EXPANSION = "";
}
