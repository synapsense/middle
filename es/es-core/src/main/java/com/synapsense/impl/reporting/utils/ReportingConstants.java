package com.synapsense.impl.reporting.utils;

import com.synapsense.exception.IllegalInputParameterException;
import com.synapsense.impl.reporting.utils.ReportingConstants.FIELD.REPORT_PARAMETER_VALUE;
import com.synapsense.impl.reporting.utils.ReportingConstants.QUERY.SYMBOL;

public final class ReportingConstants {

	public static final String SYSTEM_ACTIVITY_MESSAGE = "system operation";

	public static final String SYSTEM_ACTIVITY_IDENTITY = "System";

	public static final class FIELD {
		
		public static final class REPORT_TEMPLATE {
			public static final String NAME = "name";
			public static final String DESIGN = "design";
			public static final String OWNER = "owner";
			public static final String PRIVATE = "private";
			public static final String SYSTEM = "system";
			public static final String PARAMETERS = "parameters";
			
			private REPORT_TEMPLATE() {
			}
		}

        public static final class REPORT_TASK {
            public static final String NAME = "name";
            public static final String TEMPLATE_ID = "templateId";
            public static final String PARAMETER_VALUES = "parameterValues";
            public static final String USERS = "users";
            public static final String CRON = "cron";
            public static final String STATUS = "status";
            public static final String OWNER = "owner";
            public static String FORMATS = "formats";

            private REPORT_TASK() {
            }
        }

		public static final class REPORT {
			public static final String TASK_ID = "taskId";
			public static final String TASK_NAME = "taskName";
			public static final String TIMESTAMP = "timestamp";
			public static final String DATA_PATH = "dataPath";
			public static final String USER_NAME = "userName";

			private REPORT() {
			}
		}
		
		public static final class REPORT_PARAMETER {
			public static final String NAME = "name";
			public static final String TYPE = "type";
			public static final String TEMPLATE_ID = "templateId";
			public static final String ORDER_ID = "orderId";
			public static final String DESCRIPTION = "description";
			
			private REPORT_PARAMETER() {
			}
		}

		public static final class REPORT_PARAMETER_VALUE {
			public static final String STRING_VALUE = "value";
			public static final String INTEGER_VALUE = "integerValue";
			public static final String DATE_VALUE = "dateValue";
			public static final String TO_VALUE = "toValue";
			public static final String SET_TO_VALUE = "setTOValue";
			public static final String REPORT_TASK_ID = "reportTaskId";
			public static final String PARAMETER_ID = "parameterId";
			public static final String LONG_VALUE = "longValue";
			
			private REPORT_PARAMETER_VALUE() {
			}
		}

		public static final class EXPORTED_REPORT {
			public static final String REPORT_ID = "reportId";
			public static final String FORMAT = "format";
			public static final String EXPORTED_PATH = "exportedPath";
			
			private EXPORTED_REPORT() {
			}
		}
		
		private FIELD() {
		}
	}

	public static final class TYPE {
		public static final String REPORT_TEMPLATE = "REPORT_TEMPLATE";
        public static final String REPORT_TASK = "REPORT_TASK";
        public static final String REPORT = "REPORT";
		public static final String EXPORTED_REPORT = "EXPORTED_REPORT";
		public static final String REPORT_PARAMETER = "REPORT_PARAMETER";
		public static final String REPORT_PARAMETER_VALUE = "REPORT_PARAMETER_VALUE";
		
		private TYPE() {
		}
	}
	
	public final static class QUERY {

		public static final class WORD {
			
			public static final String AND = "and";
			
			public static final String BETWEEN = "between";
			
			public static final String CASE = "case";
			
			public static final String ELSE = "else";
			
			public static final String END = "end";
			
			public static final String IN = "in";

			public static final String OR = "or";
			
			public static final String THEN = "then";
			
			public static final String WHEN = "when";
			
			public static final String JOIN = "join";
			
			public static final String UNION = "union";
			
			public static final String AS = "as";

			private WORD() {
			}
		}

		public static final class SYMBOL {
			
			public static final String APOSTROPHE = "'";

			public static final String CLOSE_BRACKET = ")";
			
			public static final String COMMA = ",";
			
			public static final String DASH = "-";
			
			public static final String OPEN_BRACKET = "(";
			
			public static final String PERCENT = "%";
			
			public static final String SPACE = " ";

			private SYMBOL() {
			}
		}

		public static final class CLAUSE_PART {
			
			// [BC 09/2015] The reporting of historical values only uses valued (see SelectClauseBuilder.selectClause()), so it's safe
			// to add the hardcoded check hv.valued > 0 to fix bug 23645. This would break future reports using other fields such as valuei.
			public static final String OBJECTS_WHERE_JOIN_BY_VALUEID = "objects_valueids.value_id = hv.value_id_fk and hv.valued >= 0";

			public static final String PROPERTIES_WHERE_JOIN_BY_VALUEID = "properties_valueids.value_id = hv.value_id_fk and hv.valued >= 0";

			private CLAUSE_PART() {
			}
		}
		private QUERY() {
		}

	}
	
	public enum ReportingParameterType {
		STRING("java.lang.String", REPORT_PARAMETER_VALUE.STRING_VALUE, "java.lang.String")
		, INTEGER("java.lang.Integer", REPORT_PARAMETER_VALUE.INTEGER_VALUE, "java.lang.Integer")
		, DATE("java.util.Date", REPORT_PARAMETER_VALUE.DATE_VALUE, "java.util.Date")
		, TO("com.synapsense.service.impl.dao.to.GenericObjectTO", REPORT_PARAMETER_VALUE.TO_VALUE, "com.synapsense.dto.TO")
		, SET_TO("java.util.HashSet", REPORT_PARAMETER_VALUE.SET_TO_VALUE, "java.util.Collection")
		, LONG("java.lang.Long", REPORT_PARAMETER_VALUE.LONG_VALUE, "java.lang.Long")
		, RANGESET("com.synapsense.service.reporting.olap.RangeSet", REPORT_PARAMETER_VALUE.STRING_VALUE, "java.lang.String")
		, TIMESTAMP("java.sql.Timestamp", REPORT_PARAMETER_VALUE.DATE_VALUE, "java.util.Date");
		
		private String className;
		private String type;
		private String field;
		private String dbClassName;
		
		private ReportingParameterType(String className, String field, String dbClassName) {
			this.className = className;
			this.field = field;
			this.dbClassName = dbClassName;
		}

		public String getField() {
			return field;
		}

		public static ReportingParameterType getTypeByClassName(String className) {
			for (ReportingParameterType type : ReportingParameterType.values()) {
				if (type.className.equals(className))
					return type;
			}
			throw new IllegalInputParameterException("Not supported Reporting Parameter Type, class name =" + className);	
		}

		public String getType() {
			return type;
		}

		public String getDbClassName() {
			return dbClassName;
		}		
		
	} 

	private ReportingConstants() {
	}

}