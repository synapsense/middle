package com.synapsense.impl.reporting.tasks.fill;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import com.synapsense.impl.reporting.queries.types.ReportingQueryType;
import com.synapsense.impl.reporting.tasks.EnvValuesTaskData;
import com.synapsense.service.reporting.Cell;
import com.synapsense.service.reporting.olap.Member;
import com.synapsense.service.reporting.olap.OrderedHierarchy;
import com.synapsense.service.reporting.olap.custom.BaseLevel;
import com.synapsense.service.reporting.olap.custom.Cube;
import com.synapsense.util.CollectionUtils;
import com.synapsense.util.convimpl.ValueConverter;
import com.synapsense.util.unitconverter.UnitConverter;

public class ReportTableFiller {

	
	static interface TableFiller {
		void fill(Object[] row, Calendar calendar, EnvValuesTaskData envValuesTaskData);
	}
	
    private static final Map<ReportingQueryType, TableFiller> tableFillers = CollectionUtils.newMap();

    static {
    	tableFillers.put(ReportingQueryType.SINGLE_PROPERTY_CELL_TABLE,
                new ValueIdTableFiller());
    	tableFillers.put(ReportingQueryType.SAME_MULTIPLE_PROPERTY_CELL_TABLE,
                new ObjectIdTableFiller());
    	tableFillers.put(ReportingQueryType.MULTIPLE_OBJECT_CELL_TABLE,
                new ValueIdTableFiller());
    	tableFillers.put(ReportingQueryType.DIFFERENT_MULTIPLE_PROPERTY_CELL_TABLE,
                new ObjectIdPropertyNameTableFiller());
    }
    
    private static TableFiller getTableFiller(ReportingQueryType queryType) {
    	return tableFillers.get(queryType);
    }

	public static void fill(List<Object[]> queryResult, EnvValuesTaskData envValuesTaskData) {
		
		Calendar calendar = Calendar.getInstance();
		TableFiller tableFiller = getTableFiller(envValuesTaskData.getQueryType());
		
		for (Object[] row : queryResult) {
			tableFiller.fill(row, calendar, envValuesTaskData);
			
		}
		
	}
	
	static boolean containsTime(Member timeMember, Calendar calendar) {
		Member member = null;
		OrderedHierarchy hierarchy = Cube.getOrderedHierarchy(timeMember.getHierarchy().getName());
		if (hierarchy.isFullMember(timeMember)) {
			member = hierarchy.getMemberOfCalendar(calendar, timeMember.getCustomLevel(), new BaseLevel(hierarchy, Cube.LEVEL_YEAR));
			return timeMember.getSimpleMembers().contains(member);
		}
		else {
			Member current = timeMember;
			String limitLevel = null;
			while (current != null) {
				limitLevel = current.getLevel();
				current = current.getParent();
			}
			member = hierarchy.getMemberOfCalendar(calendar, timeMember.getCustomLevel()
					, new BaseLevel(hierarchy, limitLevel));
			return timeMember.getSimpleMembers().contains(member);
		}
	}
	
	static Object convert(Object res, Cell cell, Class<?> clazz,
			List<UnitConverter> unitConverters) {

		if (res == null)
			return res;
		if (!ValueConverter.isConvertible(clazz))
			return res;
		Integer ucId = cell.getUnitConverterId();

		if (ucId == null) {
			return res;
		}

		UnitConverter uc = unitConverters.get(ucId);
		return ValueConverter.fromDouble(
				uc.convert(ValueConverter.toDouble(res)), clazz);

	}
	
	static void addValue(Cell cell, Object[] row,
			List<String> aggregationTypes, int aggTypeIndex,
			Timestamp timestamp, List<UnitConverter> unitConverters) {
		Member propertyMember = cell.getMember(Cube.PROPERTY);
		String cellAggregationType = null;
		if (propertyMember != null) {
			String [] propertyKey = ((String)propertyMember.getKey()).split("\\.");
			cellAggregationType = propertyKey[2];
		} else {
			cellAggregationType = aggregationTypes.iterator().next();
		}
		
		for (String aggregationType : aggregationTypes) {
			Number value = null;
			if (aggregationType.equals(cellAggregationType)) {
				value = (Number) convert(row[aggTypeIndex], cell, row[aggTypeIndex].getClass(), unitConverters);
				cell.setValue(value);
				cell.setTimestamp(timestamp.getTime());
				break;
			}
			aggTypeIndex++;
		}
	}
}
