package com.synapsense.impl.reporting.commandengine.monitoring;

import com.synapsense.service.reporting.dto.CommandStatus;

public class DefaultStatusMonitor implements StatusMonitor {

	private CommandStatus status;

	private double progress;

	public CommandStatus getStatus() {
		return status;
	}

	public void setStatus(CommandStatus status) {
		this.status = status;
	}

	public double getProgress() {
		return progress;
	}

	public void setProgress(double progress) {
		this.progress = progress;
	}
}
