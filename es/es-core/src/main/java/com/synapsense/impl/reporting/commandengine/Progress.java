package com.synapsense.impl.reporting.commandengine;

public interface Progress {
	void showProgress(String message);

	void reportError(Exception e);
}
