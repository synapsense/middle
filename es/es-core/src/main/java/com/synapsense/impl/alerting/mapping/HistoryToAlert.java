package com.synapsense.impl.alerting.mapping;

import static com.synapsense.impl.alerting.AlertConstants.TYPE.ALERT_DESCRIPTION_TYPE_NAME;
import static com.synapsense.impl.alerting.AlertConstants.TYPE.UNKNOWN_ALERT_TYPE_NAME;

import java.util.Collection;
import java.util.Map;

import com.synapsense.dto.Alert;
import com.synapsense.dto.HistoricalRecord;
import com.synapsense.dto.TOFactory;
import com.synapsense.dto.ValueTO;
import com.synapsense.impl.utils.EnvironmentUtils;
import com.synapsense.util.CollectionUtils.Functor;

public class HistoryToAlert implements Functor<Alert, HistoricalRecord> {

	private Functor<Alert, Collection<ValueTO>> propertiesToAlertConverter;

	public HistoryToAlert(Functor<Alert, Collection<ValueTO>> f) {
		this.propertiesToAlertConverter = f;
	}

	@Override
	public Alert apply(HistoricalRecord input) {
		// transformation
		Map<String, Object> map = EnvironmentUtils.valuesAsMap(input.getValues());

		Integer hostId = (Integer) map.get("hostTO");
		String hostType = (String) map.get("hostType");
		if (hostId == null || hostType == null) {
			map.put("hostTO", TOFactory.EMPTY_TO);
		} else {
			map.put("hostTO", TOFactory.INSTANCE.loadTO(hostType, hostId));
		}
		
		return propertiesToAlertConverter.apply(EnvironmentUtils.mapAsValues(map));
	}

}
