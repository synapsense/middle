package com.synapsense.impl.dlimport.exceptions;

public class UnknownPropertyTypeException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public UnknownPropertyTypeException(String _msg) {
		super(_msg);
	}
}
