package com.synapsense.impl.reporting.model;

import net.sf.jasperreports.engine.JasperPrint;

import com.synapsense.impl.reporting.export.ExportFormat;
import com.synapsense.service.FileWriter;
import com.synapsense.service.reporting.exception.ExportingReportingException;

public class JSReport implements Report {

	private static final long serialVersionUID = -2117697696328046564L;

	private JasperPrint jasperPrint;

	public JSReport(JasperPrint jp) {
		this.jasperPrint = jp;
	}

	@Override
	public void export(ExportFormat format, String path) throws ExportingReportingException {
		format.getReportExporter().export(this, path);
	}

	public JasperPrint getJasperPrint() {
		return jasperPrint;
	}

	@Override
	public void export(ExportFormat format, String fullPath, FileWriter fileWriter) throws ExportingReportingException {
		format.getReportExporter().export(this, fullPath, fileWriter);
	}
}
