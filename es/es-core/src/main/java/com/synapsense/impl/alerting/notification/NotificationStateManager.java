package com.synapsense.impl.alerting.notification;

/**
 * Interface provides access to last notification tier and last notification
 * attempt of alert.
 * 
 * @author vadgusev
 * 
 */

public interface NotificationStateManager {

	/**
	 * Save last notification attempt to alert
	 * 
	 * @param alertId
	 *            alert ID
	 * @param lastAttempt
	 *            last fire attempt
	 */
	void saveLastAttempt(String alertId, int lastAttempt);

	/**
	 * Save last notification tier to alert and reset last attempt to 0
	 * 
	 * @param alertId
	 *            alert ID
	 * @param lastTierName
	 *            last tier name
	 */

	void saveLastTierName(String alertId, String lastTierName);

	/**
	 * Load last notification attempt from alert
	 * 
	 * @param alertId
	 *            alert ID
	 * @return last fire attempt if property found, otherwise null
	 */
	Integer loadLastAttempt(String alertId);

	/**
	 * Load last notification tier from alert
	 * 
	 * @param alertId
	 *            alert ID
	 * @return last tier name if property found, otherwise null
	 */
	String loadLastTierName(String alertId);
}
