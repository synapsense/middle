package com.synapsense.impl.diagnostic;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;

import org.apache.log4j.Logger;

public class StreamGobbler extends Thread {
	private static final Logger LOG = Logger.getLogger(StreamGobbler.class);

	protected InputStream is;
	protected OutputStream os;
	protected String type = "";

	public StreamGobbler(InputStream is, String type) {
		this(is, type, null);
	}

	public StreamGobbler(InputStream is, String type, OutputStream redirect) {
		this.is = is;
		this.type = type;
		this.os = redirect;
	}

	public void run() {
		try {
			PrintWriter pw = null;
			if (os != null) {
				pw = new PrintWriter(os);
			}

			BufferedReader br = new BufferedReader(new InputStreamReader(is));
			String line = null;
			while ((line = br.readLine()) != null) {
				if (pw != null) {
					pw.println(line);
				}

				if (this.type == "ERROR") {
					LOG.info(line);
				}
			}
			if (pw != null) {
				pw.flush();
			}
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
	}
}
