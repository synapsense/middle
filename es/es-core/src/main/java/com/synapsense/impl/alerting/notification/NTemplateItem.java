package com.synapsense.impl.alerting.notification;

import java.util.List;

import com.synapsense.dto.MessageType;

public class NTemplateItem {

	private final String templateRef;
	private final MessageType protocol;
	private final DestinationProvider recipients;

	public NTemplateItem(final String templateRef, final MessageType protocol, final DestinationProvider recipients) {
		this.templateRef = templateRef;
		this.protocol = protocol;
		this.recipients = recipients;
	}

	public MessageType getProtocol() {
		return protocol;
	}

	public DestinationProvider getDestinationProvider() {
		return recipients;
	}

	public List<Destination> getRecipients() {
		return recipients.getDestinations();
	}

	public String getTemplateRef() {
		return templateRef;
	}

}