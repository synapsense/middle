package com.synapsense.impl.erlnode.commands;

import java.util.Collection;
import java.util.Map;

import com.ericsson.otp.erlang.OtpErlangBinary;
import com.ericsson.otp.erlang.OtpErlangList;
import com.ericsson.otp.erlang.OtpErlangObject;
import com.ericsson.otp.erlang.OtpErlangTuple;
import com.synapsense.impl.erlnode.ExceptionTypes;
import com.synapsense.impl.erlnode.utils.CollectionToConverter;
import com.synapsense.impl.erlnode.utils.ToConverter;
import com.synapsense.impl.erlnode.utils.ValueTOConverter;
import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.IllegalInputParameterException;
import com.synapsense.exception.InvalidInitialDataException;
import com.synapsense.exception.ObjectExistsException;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.exception.PropertyNotFoundException;
import com.synapsense.exception.UnableToConvertPropertyException;

import com.synapsense.impl.erlnode.utils.StringConverter;

/**
 * @author stikhon
 * 
 */
public class CreateObjectCommand2 extends Command {

	public CreateObjectCommand2(Map<String, Object> state) {
		super(state);
	}

	/**
	 * %% TypeName = string() %% PropertyValues = list(value_to())
	 */
	@Override
	public OtpErlangObject do_command(OtpErlangTuple command) throws Exception {

		OtpErlangObject typeName = command.elementAt(1);
		OtpErlangObject object2 = command.elementAt(2);
		OtpErlangList valueTOList = CollectionToConverter.checkList(object2);
		if (typeName == null || !(typeName instanceof OtpErlangBinary) || valueTOList == null) {
			return makeException(ExceptionTypes.ILLEGAL_ARGUMENTS, null);
		}
		String typeNameValue = StringConverter.utf8BinaryToString(typeName);
		try {
			Collection<ValueTO> valueTOs = ValueTOConverter.parseValueTOList(getEnv(), typeNameValue, valueTOList);
			TO<?> to = getEnv().createObject(typeNameValue, valueTOs.toArray(new ValueTO[valueTOs.size()]));

			return makeOkResult(ToConverter.serializeTo(to));
		} catch (InvalidInitialDataException e) {
			return makeException(ExceptionTypes.ILLEGAL_ARGUMENTS, e.getMessage());
		} catch (ObjectNotFoundException e) {
			return makeException(ExceptionTypes.NO_OBJECT_TYPE, e.getMessage());
		} catch (ObjectExistsException e) {
			return makeException(ExceptionTypes.OBJECT_ALREADY_EXISTS, e.getMessage());
		} catch (PropertyNotFoundException e) {
			return makeException(ExceptionTypes.PROPERTY_NOT_FOUND, e.getMessage());
		} catch (IllegalInputParameterException e) {
			return makeException(ExceptionTypes.UNABLE_TO_CONVERT_PROPERTY, e.getMessage());
		} catch (UnableToConvertPropertyException e) {
			return makeException(ExceptionTypes.ILLEGAL_INPUT_PARAMETER, e.getMessage());
		}

	}

}
