package com.synapsense.impl.dlimport;

import java.util.List;

/**
 * @author Gabriel Helman
 * @since Mars Date: 5/6/11 Time: 4:30 PM
 * 
 *        Represents a collection (an object property pointing to more than one
 *        object) to be created lated in the export process.
 */
public class CollectionBundle {

	private String dlid;
	private String propertyName;
	private List<String> targets;

	public CollectionBundle(String dlid, String propertyName, List<String> targets) {
		this.dlid = dlid;
		this.propertyName = propertyName;
		this.targets = targets;
	}

	public String getDlid() {
		return dlid;
	}

	public void setDlid(String dlid) {
		this.dlid = dlid;
	}

	public String getPropertyName() {
		return propertyName;
	}

	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}

	public List<String> getTargets() {
		return targets;
	}

	public void setTargets(List<String> targets) {
		this.targets = targets;
	}

	@Override
	public String toString() {
		return "CollectionBundle{" + "dlid='" + dlid + '\'' + ", propertyName='" + propertyName + '\'' + ", targets="
		        + targets + '}';
	}
}
