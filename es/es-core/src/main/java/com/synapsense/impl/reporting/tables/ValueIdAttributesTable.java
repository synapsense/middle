package com.synapsense.impl.reporting.tables;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.synapsense.util.CollectionUtils;
import com.synapsense.util.Pair;

public class ValueIdAttributesTable {
	
	private Map<Pair<String, Integer>, List<Integer>> valueIdAttributesToCellMap = CollectionUtils.newMap();
	
	public List<Integer> getCellIndexes(Pair<String, Integer> attributes) {
		return valueIdAttributesToCellMap.get(Pair.newPair(attributes.getFirst(), attributes.getSecond()));
	}

	public void addValueIdAttributes(
			Pair<String, Integer> attributes, Integer cellIndex) {
		Pair<String, Integer> key = Pair.newPair(attributes.getFirst(), attributes.getSecond());
		if (valueIdAttributesToCellMap.containsKey(key)){
			valueIdAttributesToCellMap.get(key).add(cellIndex);
		} else {
			List<Integer> indexes = new ArrayList<>();
			indexes.add(cellIndex);
			this.valueIdAttributesToCellMap.put(key, indexes);
		}

	}

	public boolean isEmpty() {
		return valueIdAttributesToCellMap.isEmpty();
	}

	public int size() {
		return valueIdAttributesToCellMap.size();
	}

	public ValueIdAttributesTable getTable() {
		
		ValueIdAttributesTable copy = new ValueIdAttributesTable();
		for (Entry<Pair<String, Integer>, List<Integer>> entry : valueIdAttributesToCellMap.entrySet()) {
			copy.put(entry.getKey(), entry.getValue());
		}
		return copy;
		
	}

	private void put(Pair<String, Integer> key, List<Integer> value) {
		valueIdAttributesToCellMap.put(Pair.newPair(key.getFirst(), key.getSecond()), CollectionUtils.newList(value));
	}
	
}
