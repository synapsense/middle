package com.synapsense.impl.reporting.commandengine;

import com.synapsense.impl.reporting.commandengine.monitoring.StatusMonitor;
import com.synapsense.service.reporting.dto.ReportInfo;

public interface Command {

	String getName();

	String getDescription();

	void run(StatusMonitor statusMonitor);

	boolean pause();

	void renew();

	void interrupt();

	void rollback();

	ReportInfo getReportInfo();
}
