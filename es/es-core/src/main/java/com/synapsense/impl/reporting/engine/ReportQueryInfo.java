package com.synapsense.impl.reporting.engine;

import com.synapsense.service.reporting.dto.TableTO;

/**
 * The <code>ReportInfo</code> interface contains report information functionality.
 * 
 * @author Grigory Ryabov
 */
public interface ReportQueryInfo {

	/**
	 * Builds the query.
	 */
	void buildQuery();

	/**
	 * Execute query.
	 *
	 * @return the table to
	 */
	TableTO executeQuery();
}
