package com.synapsense.impl.auditsvc.auditor;

import java.rmi.registry.LocateRegistry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.naming.InitialContext;

import com.synapsense.plugin.DeviceManagerRMI;
import com.synapsense.service.impl.audit.AuditException;
import com.synapsense.service.impl.registrar.RegistrarSvc;
import com.synapsense.util.LocalizationUtils;

/**
 * Class for RMI services audit.
 * 
 * NOTE: since Callisto, it is essentially a DeviceManager auditor only, not a
 * generic RMI Auditor.
 * 
 * @author Stepanov Oleg
 * 
 */
public class RmiServiceAuditor extends BasicAuditor {

	private static final long serialVersionUID = 850104678208104108L;

	private static final transient Pattern CONFIG_PATTERN = Pattern.compile("(.+):(\\d+)");
	private String config;

	@Override
	public void doAudit() throws AuditException {
		// get config from Registrar
		try {
			config = ((RegistrarSvc) new InitialContext().lookup("java:global/es-ear/es-core/Registrar"))
			        .getComponentConnection("DeviceManager");
		} catch (Exception e) {
			throw new AuditException("Cannot get DM config from Registrar", e);
		}

		if (config == null)
			throw new AuditException(LocalizationUtils.getLocalizedString("audit_message_dm_service_is_not_running"));

		// now parse config
		String host;
		int port;
		Matcher m = CONFIG_PATTERN.matcher(config);
		if (m.matches()) {
			host = m.group(1);
			port = Integer.valueOf(m.group(2));
		} else {
			throw new AuditException("Cannot parse DM connection string: " + config);
		}

		try {
			DeviceManagerRMI dm = (DeviceManagerRMI) LocateRegistry.getRegistry(host, port).lookup("DeviceManager");
			dm.enumeratePlugins();
		} catch (Exception e) {
			throw new AuditException(LocalizationUtils.getLocalizedString("audit_message_dm_service_is_not_accessible",
			        host, Integer.toString(port)), e);
		}
	}

	@Override
	public String[] configurableProps() {
		// We've got no configurable properties here;
		// we get everything from Registrar
		return new String[] {};
	}

	@Override
	public String getConfiguration() {
		return config + "/" + "DeviceManager";
	}

}
