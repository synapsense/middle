package com.synapsense.impl.alerting.mapping;

import java.util.Collection;
import java.util.List;

import com.synapsense.dto.PriorityTier;
import com.synapsense.dto.ValueTO;
import com.synapsense.util.CollectionUtils;
import com.synapsense.util.CollectionUtils.Functor;

public class TierToProperties implements Functor<Collection<ValueTO>, PriorityTier> {

	@Override
	public Collection<ValueTO> apply(PriorityTier input) {
		List<ValueTO> values = CollectionUtils.newArrayList(5);
		values.add(new ValueTO("name", input.getName()));
		values.add(new ValueTO("interval", input.getInterval()));
		values.add(new ValueTO("notificationAttempts", input.getNotificationAttempts()));
		values.add(new ValueTO("sendEscalation", input.isSendEscalation() ? 1 : 0));
		values.add(new ValueTO("sendAck", input.isSendAck() ? 1 : 0));
		values.add(new ValueTO("sendResolve", input.isSendResolve() ? 1 : 0));

		return values;
	}
}
