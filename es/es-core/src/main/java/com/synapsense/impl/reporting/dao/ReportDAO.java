package com.synapsense.impl.reporting.dao;

import com.synapsense.exception.ObjectExistsException;
import com.synapsense.impl.reporting.export.ExportFormat;
import com.synapsense.impl.reporting.model.Report;
import com.synapsense.service.reporting.dto.ReportInfo;
import com.synapsense.service.reporting.dto.ReportTitle;
import com.synapsense.service.reporting.exception.ObjectNotFoundDAOReportingException;

import java.io.Serializable;
import java.util.Collection;

public interface ReportDAO extends Serializable {

	Collection<ReportTitle> getGeneratedReports() throws ObjectNotFoundDAOReportingException;

	ReportInfo getReportInfo(int reportId) throws ObjectNotFoundDAOReportingException;

	Report getReportObject(int reportId) throws ObjectNotFoundDAOReportingException;

	void saveReport(ReportInfo reportInfo, Report reportObject, String templateName) throws ObjectNotFoundDAOReportingException,
	        ObjectExistsException;

	void removeReport(int reportId) throws ObjectNotFoundDAOReportingException;

	String exportReport(int reportId, ExportFormat format) throws ObjectNotFoundDAOReportingException;
	
	String getExportRootPath();

	String getTemplateNameByReportId(int reportId) throws ObjectNotFoundDAOReportingException;
}
