package com.synapsense.impl.auditsvc;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.annotation.PostConstruct;
import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.DependsOn;
import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.CronScheduleBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.synapsense.cdi.MBean;
import com.synapsense.dal.generic.utils.Util;
import com.synapsense.dto.Alert;
import com.synapsense.impl.alerting.macros.LocalizeMacro;
import com.synapsense.impl.alerting.macros.Macro;
import com.synapsense.impl.alerting.macros.MacroContext;
import com.synapsense.impl.auditsvc.auditor.BasicAuditor;
import com.synapsense.impl.quartz.QuartzService;
import com.synapsense.impl.taskscheduler.EJB3InvokerJob;
import com.synapsense.service.AlertService;
import com.synapsense.service.LocalizationService;
import com.synapsense.service.impl.audit.AuditConfigurationException;
import com.synapsense.service.impl.audit.AuditException;
import com.synapsense.service.impl.audit.Auditor;
import com.synapsense.service.impl.audit.AuditorSvc;
import com.synapsense.service.user.NonUserContext;
import com.synapsense.util.Pair;

@Startup @Singleton @DependsOn("Registrar") @Remote(AuditorSvc.class)
@MBean("com.synapsense:type=AuditorService")
@ConcurrencyManagement(ConcurrencyManagementType.BEAN)
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class AuditorSvcImpl implements AuditorSvcImplMBean, AuditorSvc {

	private static Log logger = LogFactory.getLog(AuditorSvcImpl.class);

	private ConcurrentHashMap<String, BasicAuditor> auditors;

	final private static String FILE_STORAGE = "com.synapsense.filestorage";
	final private static String AUDIT_FILE_NAME = "com.synapsense.audit.configurationfile";

	/**
	 * First auditor service run delay in ms
	 * 
	 * @see #start()
	 */
	final private static long AUDITOR_TASK_START_DELAY = 5 * 60 * 1000;

	private Scheduler scheduler;

	@EJB
	private AlertService alertService;
	
	private Macro localizeMacro;
	private MacroContext macroCtx;

	private String auditConfig;

	@EJB
	public void setQuartzService(QuartzService service) throws Exception {
		scheduler = service.getScheduler();
	}

	@EJB
	public void setLocalizationService(LocalizationService localizationService) {
		localizeMacro = new LocalizeMacro();
		macroCtx = new MacroContext();
		macroCtx.localizationService = localizationService;
		macroCtx.usrCtx = new NonUserContext();
	}

	public void addAuditor(final String name, final BasicAuditor auditor) {
		auditors.put(name, auditor);
	}

	@Override
	@PostConstruct
	public void create() throws Exception {
		auditors = new ConcurrentHashMap<String, BasicAuditor>();
		auditConfig = System.getProperty("com.synapsense.audit.auditinterval", "0 0/1 * * * ?");
		start();
	}

	@Override
	public void destroy() {
		logger.info("Audit svc destroyed");
	}

	@Override
	public void removeAuditor(final String auditorName) {
		auditors.remove(auditorName);
	}

	@Override
	public String listAuditors() {
		StringBuilder builder = new StringBuilder();
		for (Map.Entry<String, Auditor> entry : getAuditors().entrySet()) {
			builder.append("Name: ").append(entry.getKey()).append("\n");
			Auditor auditor = entry.getValue();
			builder.append("Configuration: ").append(auditor.getConfiguration()).append("\n");
			builder.append("Last status: ").append(auditor.getLastStatus()).append("\n\n");
		}
		return builder.toString();
	}

	@Override
	// AFAIR it is J2EE convention to use such signature
	public void start() throws Exception {
		String fileName = System.getProperty(FILE_STORAGE);

		if (fileName == null || fileName.isEmpty())
			throw new AuditConfigurationException("Property [" + FILE_STORAGE + "] is not set");

		fileName += File.separator + System.getProperty(AUDIT_FILE_NAME, "audit.xml");

		loadXMLData(fileName);

		Trigger trigger = TriggerBuilder.newTrigger()
							.withIdentity(this.getClass().getName(), Scheduler.DEFAULT_GROUP)
							.withSchedule(CronScheduleBuilder.cronSchedule(auditConfig))
							// Delayed first run of auditor task
							.startAt(new Date(System.currentTimeMillis() + AUDITOR_TASK_START_DELAY))
							.build();

		JobDetail jd = JobBuilder.newJob(EJB3InvokerJob.class)
						.withIdentity(this.getClass().getName(), Scheduler.DEFAULT_GROUP)
						.usingJobData(EJB3InvokerJob.EJB_JNDI_NAME_KEY, "java:global/es-ear/es-core/AuditorSvcImpl")
						.usingJobData(EJB3InvokerJob.EJB_METHOD_KEY, "audit")
						.usingJobData(EJB3InvokerJob.EJB_INTERFACE_CLASS, AuditorSvcImpl.class.getName())
						.build();

		scheduler.scheduleJob(jd, trigger);

		logger.info("Audit svc started");
	}

	@Override
	public void stop() {
		auditors.clear();
		try {
			scheduler.deleteJob(new JobKey(this.getClass().getName(), Scheduler.DEFAULT_GROUP));
		} catch (SchedulerException e) {
			logger.warn(e.getLocalizedMessage(), e);
		}
		logger.info("Audit svc stoped");
	}

	@SuppressWarnings("unchecked")
	protected void loadXMLData(final String fileName) throws ParserConfigurationException, SAXException, IOException,
	        ClassNotFoundException {
		XMLReader reader = new XMLReader(fileName);
		Collection<Pair> classes = reader.getObjectData("auditor", new String[] { "class" });
		for (Pair classToLoad : classes) {
			// Loading config to the storage for each class
			if (!classToLoad.getFirst().equals("class"))
				continue;
			Class<BasicAuditor> toLoad = (Class<BasicAuditor>) Class.forName((String) classToLoad.getSecond());
			for (BasicAuditor ba : reader.loadDataFromXml(toLoad)) {
				auditors.put(ba.getClass().getName(), ba);
			}
		}
	}

	@Override
	@Interceptors(AuditorInterceptor.class)
	public Map<String, Auditor> getAuditors() {
		HashMap<String, Auditor> as = new HashMap<String, Auditor>();
		for (Map.Entry<String, BasicAuditor> entry : auditors.entrySet()) {
			BasicAuditor ba = entry.getValue();
			as.put(entry.getKey(), new Auditor(entry.getKey(), ba.getConfiguration(), ba.getLastStatus()));
		}
		return as;
	}

	@Override
	public void audit() {
		for (BasicAuditor ba : auditors.values()) {
			try {
				ba.audit();
			} catch (AuditException e) {
				try {
					raiseAuditError(e);
				} catch (Throwable t) {
					logger.warn("Cannot raise an error " + ba.getClass().getName(), t);
				}
			} catch (Throwable t) {
				logger.warn("Got exception from auditor " + ba.getClass().getName(), t);
			}
		}
	}

	private void raiseAuditError(final AuditException e) {
		if (e.getRecords().isEmpty()) {
			Alert alert = new Alert("audit_alert", "Audit Alert", new Date(), e.getLocalizedMessage());
			alertService.raiseAlert(alert);
		} else {
			for (AuditException.AuditRecord rec : e.getRecords()) {
				Alert alert = new Alert("audit_alert", "Audit Alert", new Date(), rec.details, rec.objId);
				alertService.raiseAlert(alert);
			}
		}

		if (logger.isDebugEnabled())
			logger.warn(localizeMacro.expandIn(e.getLocalizedMessage(), macroCtx), e);
		else
			logger.warn(localizeMacro.expandIn(e.getLocalizedMessage(), macroCtx));
	}
}

class XMLReader {
	private Document xmlDoc;
	protected static Log logger = LogFactory.getLog(XMLReader.class);

	public XMLReader(final String fileName) throws ParserConfigurationException, SAXException, IOException {
		File f = new File(fileName);
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setValidating(false);
		DocumentBuilder builder = factory.newDocumentBuilder();
		xmlDoc = builder.parse(f);
	}

	public Collection<BasicAuditor> loadDataFromXml(final Class<? extends BasicAuditor> clazz) {
		Collection<BasicAuditor> res = new LinkedList<BasicAuditor>();
		NodeList nl = xmlDoc.getElementsByTagName(clazz.getSimpleName());

		for (int i = 0; i < nl.getLength(); i++) {
			if (!nl.item(i).getNodeName().equals(clazz.getSimpleName()))
				continue;
			BasicAuditor auditor;
			try {
				auditor = clazz.newInstance();
				for (String att : auditor.configurableProps()) {
					try {
						Util.setProperty(auditor, att, getAttribute(nl.item(i), att));
					} catch (Exception e) {
						logger.warn("Unable to set property", e);
					}
				}
				res.add(auditor);
			} catch (Exception e) {
				logger.warn("Unable to create auditor", e);
			}

		}
		return res;
	}

	@SuppressWarnings("unchecked")
	public Collection<Pair> getObjectData(final String tagname, final String[] attLits) {
		Collection<Pair> res = new LinkedList<Pair>();
		NodeList nl = xmlDoc.getElementsByTagName(tagname);

		for (int i = 0; i < nl.getLength(); i++) {
			if (!nl.item(i).getNodeName().equals(tagname))
				continue;
			for (String att : attLits) {
				res.add(new Pair(att, getAttribute(nl.item(i), att)));
			}
		}
		return res;
	}

	public String getAttribute(final Node node, final String atName) {
		if (node.hasAttributes()) {
			Node node2 = node.getAttributes().getNamedItem(atName);
			if (node2 != null) {
				return node2.getNodeValue();
			} else {
				return "";
			}
		} else {
			return "";
		}
	}
}