package com.synapsense.impl.auditsvc.auditor;

import java.io.IOException;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.UnknownHostException;

import com.synapsense.service.impl.audit.AuditException;
import com.synapsense.util.LocalizationUtils;

public class HostAuditor extends BasicAuditor {

	private static final long serialVersionUID = 391204025602979046L;

	private String hostName;
	private int timeout = 3000;

	public HostAuditor() {
		super();

	}

	public HostAuditor(String confString) {
		super(confString);
		hostName = confString;
	}

	@Override
	public void doAudit() throws AuditException {
		if (logger.isInfoEnabled()) {
			logger.info("Starting audit for [" + hostName + "]");
		}
		try {
			InetAddress[] ipAddresses = Inet4Address.getAllByName(hostName);
			for (InetAddress ip : ipAddresses) {
				try {
					if (!ip.isReachable(timeout)) {
						logger.info("Host [" + hostName + "] is not availiable");
						throw new AuditException(LocalizationUtils.getLocalizedString("audit_message_host_does_not_respond", ip.toString()));
					}
					logger.info("Host [" + hostName + "] is alive");
				} catch (IOException e) {
					if (logger.isWarnEnabled()) {
						logger.warn(e.getLocalizedMessage(), e);
					}
					throw new AuditException(e.getLocalizedMessage(), e);
				}
			}
		} catch (Exception e) {
			if (logger.isWarnEnabled()) {
				logger.warn(e.getLocalizedMessage(), e);
			}
			throw new AuditException(e.getLocalizedMessage(), e);
		}

		if (logger.isInfoEnabled()) {
			logger.info("HostAuditor finished for [" + hostName + "]");
		}

	}

	@Override
	public String[] configurableProps() {
		return new String[] { "hostName", "timeout" };
	}

	public String getHostName() {
		return hostName;
	}

	public void setHostName(String hostName) {
		this.hostName = hostName;
	}

	public int getTimeout() {
		return timeout;
	}

	public void setTimeout(int timeout) {
		this.timeout = timeout;
	}

	public void setTimeout(String timeout) {
		this.timeout = Integer.parseInt(timeout);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((hostName == null) ? 0 : hostName.hashCode());
		result = prime * result + timeout;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		final HostAuditor other = (HostAuditor) obj;
		if (hostName == null) {
			if (other.hostName != null)
				return false;
		} else if (!hostName.equals(other.hostName))
			return false;
		if (timeout != other.timeout)
			return false;
		return true;
	}

	@Override
	public String getConfiguration() {
		configuration = hostName;
		return null;
	}
}
