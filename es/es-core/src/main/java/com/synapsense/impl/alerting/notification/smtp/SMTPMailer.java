package com.synapsense.impl.alerting.notification.smtp;

import com.synapsense.dto.Message;
import com.synapsense.impl.alerting.notification.Provider;
import com.synapsense.service.impl.notification.Transport;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.EmailException;
import org.apache.log4j.Logger;

import javax.mail.Session;

/**
 * Abstract SMTP message sender. Prepares common parts of the SMTP message and
 * send it to destinations. Delegates generating of email instance to
 * descendants.
 * 
 * @author shabanov
 * 
 */
abstract class SMTPMailer implements Transport {
	private final static Logger logger = Logger.getLogger(SMTPMailer.class);

	public static final String ENCODING = "UTF-8";

	private Provider<Session> sessionProvider;

	protected SMTPMailer(Provider<Session> sessionPrv) {
		this.sessionProvider = sessionPrv;
	}

	@Override
	public boolean isValid(Message message) {
		final String destination = message.getDestination();

		if (destination == null || destination.isEmpty()) {
			logger.warn("Destination of email is not specified");
			return false;
		}

		// add more preconditions if needed

		return true;
	}

	@Override
	public void send(final Message message) {
		if (!isValid(message))
			return;

		final String destination = message.getDestination();

		try {
			Session session = sessionProvider.get();
			if (session == null) {
				logger.warn("Cannot send email, there is no mail session acquired");
				return;
			}

			String from = session.getProperty("mail.from");
			if (from == null || from.isEmpty()) {
				logger.warn("Cannot send email, session has no 'mail.from' property specified");
				return;
			}

			if (logger.isDebugEnabled()) {
				logger.debug("Preparing email...");
			}

			Email email = generateEmail(message);

			email.setCharset(ENCODING);
			email.setSubject(message.getHeader());

			email.addTo(destination);
			email.setFrom(from);

			email.setMailSession(session);

			if (logger.isDebugEnabled()) {
				logger.debug("Sending email:\nHeader [" + message.getHeader() + "]\nContent [" + message.getContent()
				        + "]");
			}

			email.send();

			if (logger.isDebugEnabled()) {
				logger.debug("Email has been sent successfully");
			}

		} catch (final Exception e) {
			logger.warn("Unable to send email", e);
		}
	}

    @Override
    public String getName() {
        return "SMTPMailer";
    }

    protected abstract Email generateEmail(Message message) throws EmailException;
}
