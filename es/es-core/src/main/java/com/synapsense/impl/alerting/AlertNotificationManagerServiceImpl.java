package com.synapsense.impl.alerting;

import com.synapsense.cdi.MBean;
import com.synapsense.commandprocessor.CommandEngineFactory;
import com.synapsense.commandprocessor.api.Command;
import com.synapsense.commandprocessor.api.CommandEngine;
import com.synapsense.dal.objectcache.CacheEnvironmentDispatcher;
import com.synapsense.dal.objectcache.CacheSvc;
import com.synapsense.dal.objectcache.CacheSvcImpl;
import com.synapsense.dto.*;
import com.synapsense.exception.AlertServiceSystemException;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.exception.PropertyNotFoundException;
import com.synapsense.exception.UnableToConvertPropertyException;
import com.synapsense.impl.alerting.macros.AlertDescrMacro;
import com.synapsense.impl.alerting.macros.AlertMacroFactory;
import com.synapsense.impl.alerting.macros.Macro;
import com.synapsense.impl.alerting.macros.MacroContext;
import com.synapsense.impl.alerting.mapping.Converters;
import com.synapsense.impl.alerting.notification.*;
import com.synapsense.impl.alerting.notification.MessageTemplate;
import com.synapsense.impl.alerting.notification.smtp.AlertEmailTemplate;
import com.synapsense.impl.alerting.notification.smtp.FailSafeHtmlMailer;
import com.synapsense.impl.alerting.notification.smtp.JbossMailSession;
import com.synapsense.impl.alerting.notification.smtp.SimpleMailer;
import com.synapsense.impl.utils.EnvironmentUtils;
import com.synapsense.service.Environment;
import com.synapsense.service.LocalizationService;
import com.synapsense.service.UnitSystemsService;
import com.synapsense.service.UserManagementService;
import com.synapsense.service.impl.notification.Transport;
import com.synapsense.service.nsvc.NotificationService;
import com.synapsense.service.nsvc.events.*;
import com.synapsense.service.nsvc.filters.*;
import com.synapsense.util.CollectionUtils;
import com.synapsense.util.ConvertingEnvironmentProxy;
import com.synapsense.util.LocalizationUtils;
import com.synapsense.util.TranslatingEnvironment;
import com.synapsense.util.unitconverter.UnitResolvers;
import com.synapsense.util.unitconverter.UnitSystems;
import org.apache.log4j.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.*;
import java.io.File;
import java.nio.file.Paths;
import java.util.*;
import java.util.Map.Entry;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

import static com.synapsense.impl.alerting.AlertConstants.MESSAGE.*;
import static com.synapsense.impl.alerting.AlertConstants.TYPE.*;

@Startup
@Singleton(name = "AlertNotificationManagerService")
@DependsOn({ "SNMPTrapPerformer", "AudioVisualPerformer", "SynapCache" })
@ConcurrencyManagement(ConcurrencyManagementType.BEAN)
@MBean("com.synapsense:type=AlertNotificationManagerService")
public class AlertNotificationManagerServiceImpl implements AlertNotificationManagerService,
        AlertNotificationManagerServiceImplMBean {

	private final static Logger logger = Logger.getLogger(AlertNotificationManagerServiceImpl.class);

	/** EJB */
	private Environment env;

	/** EJB */
	private UserManagementService userService;

	/**
	 * notification manager
	 */
	private NotificationManager manager;

	/**
	 * queue of active notifications
	 */
	private Queue<Notification> queue;

	/**
	 * available transports
	 */
	private TransportProvider transports;

	/**
	 * internal scheduer
	 */
	private ScheduledExecutorService scheduler;

	/** EJB */
	private UnitSystemsService unitSystemService;

	private UnitSystems unitSystems;

	private NotificationService notificationService;

	private LocalizationService localizationService;

    private ConvertingEnvironmentProxy convertingEnv;

	private Set<DispatchingProcessor> dispProcessors = CollectionUtils.newSet();

	/**
	 * notification templates synchronized storage
	 */
	private Map<TO<?>, NotificationTemplate> notificationTemplates;

	private CommandEngine commandEngine;

	private NotificationStateManager stateManager;

	private class AlertRaisedProcessor implements ObjectCreatedEventProcessor {

		@Override
		public ObjectCreatedEvent process(final ObjectCreatedEvent e) {
			if (logger.isDebugEnabled()) {
				logger.debug("Received raise alert notification");
			}

			Command c = new Command() {
				@Override
				public void execute() {
					TO<?> alertRaised = e.getObjectTO();

					Collection<ValueTO> alertInititalValues = Arrays.asList(e.getValues());

					Map<String, Object> alertInititalValuesAsMap = EnvironmentUtils.valuesAsMap(alertInititalValues);
					TO<?> alertTypeId = (TO<?>) alertInititalValuesAsMap.get("type");

					if (alertTypeId == null) {
						logger.warn("Raised alert event : "
						        + e
						        + " has no initital proprerty 'type' which refers to type of alert. Notification won't be scheduled");
						return;
					}

					Alert alertDto = CollectionUtils.map(alertInititalValues,
                            Converters.propertiesToAlertConverter(alertRaised, env));
					raiseAlertNotification(alertRaised, alertTypeId, alertDto);
				}
			};

			commandEngine.addCommand(c);
			return null;
		}
	}

	private class AlertDeleteProcessor implements ObjectDeletedEventProcessor {

		@Override
		public ObjectDeletedEvent process(final ObjectDeletedEvent e) {
			if (logger.isDebugEnabled()) {
				logger.debug("Received alert deleted notification");
			}
			Command c = new Command() {
				@Override
				public void execute() {

					TO<?> deletedAlert = e.getObjectTO();
					removeAlertNotification(deletedAlert);
				}
			};

			commandEngine.addCommand(c);
			return null;
		}
	}

	private class AlertStatusProcessor implements PropertiesChangedEventProcessor {

		@Override
		public PropertiesChangedEvent process(final PropertiesChangedEvent e) {
			if (logger.isDebugEnabled()) {
				logger.debug("Received alert status udpated notification");
			}

			Command c = new Command() {
				@Override
				public void execute() {
					TO<?> alertChanged = Utils.discoverAlertId(e);
					if (alertChanged == null) {
						if (logger.isDebugEnabled()) {
							logger.debug("Changed object is not alert. Wrong filter was configured");
						}
						return;
					}

					ChangedValueTO status = CollectionUtils.search(Arrays.asList(e.getChangedValues()),
					        new CollectionUtils.Predicate<ChangedValueTO>() {
						        @Override
						        public boolean evaluate(ChangedValueTO t) {
							        return t.getPropertyName().equals("status");
						        }
					        });

					if (status == null) {
						return;
					}

					switch (AlertStatus.decode((Integer) status.getValue())) {
					case ACKNOWLEDGED:
						manager.ack(alertChanged.getID().toString());
						break;
					case DISMISSED:
						manager.dismiss(alertChanged.getID().toString());
						break;
					case RESOLVED:
						manager.resolve(alertChanged.getID().toString());
						break;
					default:
						break;
					}
				}
			};

			commandEngine.addCommand(c);
			return null;
		}
	}

	/**
	 * do reload template corresponding to changed alert type
	 * 
	 * @author shabanov
	 * 
	 */
	private class AlertTypeAnyPropertyChangeProcessor implements PropertiesChangedEventProcessor {

		@Override
		public PropertiesChangedEvent process(final PropertiesChangedEvent e) {
			if (logger.isDebugEnabled()) {
				logger.debug("Received alert type any property udpated notification");
			}

			Command c = new Command() {
				@Override
				public void execute() {
					reloadTemplate(e.getObjectTO());
				}
			};

			commandEngine.addCommand(c);
			return null;
		}

	}

	private class AlertPriorityChangeProcessor implements ReferencedObjectChangedEventProcessor {

		@Override
		public ReferencedObjectChangedEvent process(final ReferencedObjectChangedEvent e) {
			if (logger.isDebugEnabled()) {
				logger.debug("Received alert priority udpated notification");
			}

			Command c = new Command() {
				@Override
				public void execute() {
					reloadTemplate(e.getReferencingObj());
				}
			};

			commandEngine.addCommand(c);
			return null;
		}

	}

	private class AlertTypeCreateProcessor implements ObjectCreatedEventProcessor {

		@Override
		public ObjectCreatedEvent process(final ObjectCreatedEvent e) {
			if (logger.isDebugEnabled()) {
				logger.debug("Received alert type created notification");
			}

			Command c = new Command() {
				@Override
				public void execute() {
					loadTemplate(e.getObjectTO());
				}
			};

			commandEngine.addCommand(c);
			return null;
		}

	}

	private class AlertTypeDeleteProcessor implements ObjectDeletedEventProcessor {

		@Override
		public ObjectDeletedEvent process(final ObjectDeletedEvent e) {
			if (logger.isDebugEnabled()) {
				logger.debug("Received alert type deleted notification");
			}

			Command c = new Command() {
				@Override
				public void execute() {
					removeTemplate(e.getObjectTO());
				}
			};

			commandEngine.addCommand(c);
			return null;
		}

	}

	@Override
	public void start() throws Exception {
		logger.info("Starting alert notitication service...");

        unitSystems = unitSystemService.getUnitSystems();
        final UnitResolvers unitRes = unitSystemService.getUnitReslovers();

        CacheSvc cacheSvc = CacheSvcImpl.getInstance();
        env = CacheEnvironmentDispatcher.getEnvironment(cacheSvc, env);

        convertingEnv = new ConvertingEnvironmentProxy();
        convertingEnv.setUnitSystems(unitSystems);
        convertingEnv.setUnitResolvers(unitRes);
        convertingEnv.setProxiedEnvironment(env);

        env = new TranslatingEnvironment(env);

		commandEngine = CommandEngineFactory.newEngineSharedBlockingQueue("ALERT_NOTIFICATION_MANAGER_PROCESSOR",
		        new LinkedBlockingDeque<Command>());

		stateManager = new EnvNotificationStateManager(env);

		initNotificationQueue();
		// pool of two threads - one for commandEngine and second for other
		// services
		initNotificationScheduler(commandEngine.identify(), 2);
		initTransports();
		initTemplates();

		AlertNotificationManager alertNotificationManager = new AlertNotificationManager(queue, transports);
		manager = alertNotificationManager;
		scheduler.scheduleAtFixedRate(alertNotificationManager, 1, 5, TimeUnit.SECONDS);

		restoreAllAlertNotifications();

		scheduler.execute(commandEngine);

		if (logger.isDebugEnabled()) {
			logger.debug("Subscribing to notifications...");
		}
		registerDispatchProcessor();

		logger.info("Started alert notitication service");
	}

	@Override
	@PreDestroy
	public void stop() throws Exception {
		deregistrerDispatchProcessor();

		commandEngine.shutdown();

		// safe shutdown
		scheduler.shutdownNow();
		// then wait all tasks normal completion
		while (!scheduler.isTerminated()) {
			logger.debug("Awaiting termination of working thread...");
			scheduler.awaitTermination(5, TimeUnit.SECONDS);
		}
		destroy();
	}

	@Override
	@PostConstruct
	public void create() throws Exception {
		logger.info("Creating alert notification service ...");
		logger.info("Started alert notification service");
		start();
	}

	@Override
	public void destroy() throws Exception {
		logger.info("Destroying alert notification service ...");
		logger.info("Destroyed alert notification service");
	}

	// dependency setters
	@EJB
	public void setUnitSystemService(final UnitSystemsService unitSystemService) {
		this.unitSystemService = unitSystemService;
	}

	@EJB(beanName = "GenericEnvironment")
	public void setEnvironment(final Environment env) {
		this.env = env;
	}

	@EJB(beanName = "LocalUserManagementService")
	public void setUserService(final UserManagementService userService) {
		this.userService = userService;
	}

	@EJB(beanName = "LocalNotificationService")
	public void setNotificationService(NotificationService notificationService) {
		this.notificationService = notificationService;
	}

	@EJB
	public void setLocalizationService(LocalizationService localizationService) {
		this.localizationService = localizationService;
	}

	// private members

	/**
	 * Note: the comparator imposes orderings that are inconsistent with equals
	 */
	private void initNotificationQueue() {

		queue = new PriorityBlockingQueue<>(10, new Comparator<Notification>() {
			@Override
			public int compare(final Notification n1, final Notification n2) {
				final Date n1Next = n1.getNextFireTime();
				final Date n2Next = n2.getNextFireTime();

				if (n1Next.after(n2Next)) {
					return 1;
				}

				if (n1Next.before(n2Next)) {
					return -1;
				}

				return 0;
			}
		});
	}

	private void initNotificationScheduler(final String threadName, int poolSize) {
		scheduler = Executors.newScheduledThreadPool(poolSize, new ThreadFactory() {
            private AtomicInteger threadCounter = new AtomicInteger(1);
			@Override
			public Thread newThread(final Runnable r) {
				if (logger.isDebugEnabled()) {
					logger.debug("Created new thread for alert notification manager");
				}

				final Thread t = new Thread(r, threadName + "_" + threadCounter.getAndIncrement());

				t.setUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
					@Override
					public void uncaughtException(final Thread t, final Throwable e) {
						logger.error("Alert notification manager was stopped due to", e);
					}
				});

				return t;
			}
		});
	}

	/**
	 * register dedicated processor for each filter to simplify event dispatch
	 * process on client side. TODO : !subject to change
	 */
	private void registerDispatchProcessor() {
		// subscribe to events
		dispProcessors = CollectionUtils.newSet();

		DispatchingProcessor alertStatusDP = new DispatchingProcessor().putPcep(new AlertStatusProcessor());
		notificationService.registerProcessor(alertStatusDP, AlertNotifications.alertStatusFilter());
		dispProcessors.add(alertStatusDP);

		DispatchingProcessor alertRaisedDP = new DispatchingProcessor().putOcep(new AlertRaisedProcessor());
		ObjectCreatedEventFilter alertRaisedFilter = new ObjectCreatedEventFilter(new TOMatcher(ALERT_TYPE_NAME));
		notificationService.registerProcessor(alertRaisedDP, new DispatchingProcessor().putOcep(alertRaisedFilter));

		dispProcessors.add(alertRaisedDP);

		DispatchingProcessor alertDeletedDP = new DispatchingProcessor().putOdep(new AlertDeleteProcessor());
		ObjectDeletedEventFilter alertDeletedFilter = new ObjectDeletedEventFilter(new TOMatcher(ALERT_TYPE_NAME));
		notificationService.registerProcessor(alertDeletedDP, new DispatchingProcessor().putOdep(alertDeletedFilter));

		dispProcessors.add(alertDeletedDP);

		DispatchingProcessor alertTypeAnyChangeDP = new DispatchingProcessor()
		        .putPcep(new AlertTypeAnyPropertyChangeProcessor());
		PropertiesChangedEventFilter alertTypeAnyChangeFilter = new PropertiesChangedEventFilter(new TOMatcher(
		        ALERT_DESCRIPTION_TYPE_NAME));
		notificationService.registerProcessor(alertTypeAnyChangeDP,
		        new DispatchingProcessor().putPcep(alertTypeAnyChangeFilter));

		dispProcessors.add(alertTypeAnyChangeDP);

		DispatchingProcessor alertPriorityChangeDP = new DispatchingProcessor()
		        .putRocep(new AlertPriorityChangeProcessor());
		ReferencedObjectChangedEventFilter alertPriorityChangeFilter = new ReferencedObjectChangedEventFilter(
		        new TOMatcher(ALERT_DESCRIPTION_TYPE_NAME), "priority", new TOMatcher(ALERT_PRIORITY_TYPE_NAME), null);
		notificationService.registerProcessor(alertPriorityChangeDP,
		        new DispatchingProcessor().putRocep(alertPriorityChangeFilter));

		dispProcessors.add(alertPriorityChangeDP);

		DispatchingProcessor alertTypeCreatedDP = new DispatchingProcessor().putOcep(new AlertTypeCreateProcessor());
		ObjectCreatedEventFilter alertTypeCreatedFilter = new ObjectCreatedEventFilter(new TOMatcher(
		        ALERT_DESCRIPTION_TYPE_NAME));
		notificationService.registerProcessor(alertTypeCreatedDP,
		        new DispatchingProcessor().putOcep(alertTypeCreatedFilter));

		dispProcessors.add(alertTypeCreatedDP);

		DispatchingProcessor alertTypeDeletedDP = new DispatchingProcessor().putOdep(new AlertTypeDeleteProcessor());
		ObjectDeletedEventFilter alertTypeDeletedFilter = new ObjectDeletedEventFilter(new TOMatcher(
		        ALERT_DESCRIPTION_TYPE_NAME));
		notificationService.registerProcessor(alertTypeDeletedDP,
		        new DispatchingProcessor().putOdep(alertTypeDeletedFilter));

		dispProcessors.add(alertTypeDeletedDP);
	}

	private void deregistrerDispatchProcessor() {
		// not a subscriber now
		for (DispatchingProcessor dp : dispProcessors) {
			notificationService.deregisterProcessor(dp);
		}

		dispProcessors.clear();
	}

	private void initTransports() {
		final TransportProvider transports = new DefaultTransportProvider();

		String fileStorage = System.getProperty("com.synapsense.filestorage");
		String alertEmailTemplateFile = fileStorage + File.separator + "alert_email.template";
		final Transport mailer = new FailSafeHtmlMailer(new JbossMailSession(), new AlertEmailTemplate(Paths.get(
		        alertEmailTemplateFile).toFile(), true));
        transports.registerTransport(MessageType.SMTP.name(), mailer);

        final Transport simpleMailer = new SimpleMailer(new JbossMailSession());
		transports.registerTransport(MessageType.SMS.name(), simpleMailer);

		if (logger.isDebugEnabled()) {
			logger.debug("SMTP mailer service has been initialized properly");
		}

		final Transport snmpSender = new TransportAsService(
		        "java:global/es-ear/es-core/SNMPTrapPerformer!com.synapsense.service.impl.notification.Transport");
		transports.registerTransport(MessageType.SNMP.name(), snmpSender);
		if (logger.isDebugEnabled()) {
			logger.debug("SNMPTrapPerformer service was initialized properly");
		}

		final Transport audioVisualSwitch = new TransportAsService(
		        "java:global/es-ear/es-core/AudioVisualPerformer!com.synapsense.service.impl.notification.Transport");
		transports.registerTransport(MessageType.AUDIO_VISUAL.name(), audioVisualSwitch);
		if (logger.isDebugEnabled()) {
			logger.debug("AudioVisualPerformer service was initialized properly");
		}
		final Transport ntLogPerformer = new TransportAsService(
		        "java:global/es-ear/es-core/NtLogPerformer!com.synapsense.service.impl.notification.Transport");
		transports.registerTransport(MessageType.NT_LOG.name(), ntLogPerformer);
		if (logger.isDebugEnabled()) {
			logger.debug("NT Event Logger service was initialized succesfully.");
		}

		this.transports = transports;
	}

	private void raiseAlertNotification(final TO<?> alertId, TO<?> alertTypeId, final Alert alertDto) {
		Notification newNotification = createAlertNotification(alertId, alertTypeId, alertDto, false);
		if (newNotification != null) {
			manager.raise(newNotification);
		}
	}

	private void restoreAlertNotification(final TO<?> alertId, TO<?> alertTypeId, final Alert alertDto) {
		Notification newNotification = createAlertNotification(alertId, alertTypeId, alertDto, true);
		if (newNotification != null) {
			if (alertDto.getStatus().code() == 0) {
				manager.raise(newNotification);
			} else {
				manager.add(newNotification);
			}
		}
	}

	private Notification createAlertNotification(final TO<?> alertId, TO<?> alertTypeId, final Alert alertDto,
	        final boolean isRestore) {
		if (logger.isDebugEnabled()) {
			logger.debug("Adding alert notification for alert : " + alertId);
		}

		final NotificationTemplate template = getNotificationTemplate(alertTypeId);
		if (template == null) {
			logger.warn("Trying to add notification for alert of type : " + alertDto.getAlertType()
			        + " which has no configured notification template");
			return null;
		}

		MacroContext macroCtx = new MacroContext();
		macroCtx.env = env;
        macroCtx.convEnv = convertingEnv;
		macroCtx.unitSystems = unitSystems;
		macroCtx.alert = alertDto;
		macroCtx.localizationService = localizationService;
        macroCtx.props.put(AlertDescrMacro.ALERT_DESCRIPTION, alertDto.getTypeDescr());

		if (isRestore) {
			return template.restoreNotification(alertId.getID().toString(), macroCtx);
		} else {
			return template.createNotification(alertId.getID().toString(), macroCtx);
		}
	}

	private void removeAlertNotification(final TO<?> id) {
		if (logger.isDebugEnabled()) {
			logger.debug("Removing alert notification for alert : " + id);
		}
		manager.remove(id.getID().toString());
	}

	private NotificationTemplate getNotificationTemplate(final TO<?> alertType) {
		return notificationTemplates.get(alertType);
	}

	private void removeTemplate(final TO<?> alertTypeId) {
		if (logger.isDebugEnabled()) {
			logger.debug("Removing notification template of alert type : " + alertTypeId);
		}

		notificationTemplates.remove(alertTypeId);

		if (logger.isDebugEnabled()) {
			logger.debug("Removed notification template of alert type : " + alertTypeId);
		}
	}

	private void initTemplates() {
		if (logger.isDebugEnabled()) {
			logger.debug("Loading notification templates...");
		}

		notificationTemplates = CollectionUtils.newSynchronizedMap();
		Collection<TO<?>> alertTypes = env.getObjectsByType(ALERT_DESCRIPTION_TYPE_NAME);

		for (final TO<?> at : alertTypes) {
			loadTemplate(at);
		}

	}

	private void reloadTemplate(final TO<?> alertType) {
		if (logger.isDebugEnabled()) {
			logger.debug("Reloading alert type : " + alertType + " notification settings");
		}
		synchronized (this) {
			removeTemplate(alertType);
			loadTemplate(alertType);
		}
	}

	private void loadTemplate(final TO<?> alertTypeRef) {
		Collection<ValueTO> values;
		try {
			values = env.getAllPropertiesValues(alertTypeRef);
		} catch (ObjectNotFoundException e) {
			logger.debug("Failed to load alert notification template", e);
			return;
		}

		AlertType alertType = CollectionUtils.map(values, Converters.propertiesToAlertTypeConverter(env));

		final String alertTypeName = alertType.getName();
		if (logger.isDebugEnabled()) {
			logger.debug("Loading alert notification template for alert type :" + alertTypeName);
		}

		Map<String, MessageTemplate> templates = initTemplates(alertType.getMessageTemplates());

		TO<?> priorityRef = null;
		try {
			priorityRef = env.getPropertyValue(alertTypeRef, "priority", TO.class);
		} catch (ObjectNotFoundException e) {
			logger.warn("Failed to load alert notification template for alert type : " + alertTypeName, e);
		} catch (PropertyNotFoundException | UnableToConvertPropertyException  e) {
			throw new AlertServiceSystemException("Detected malformed object type : " + ALERT_DESCRIPTION_TYPE_NAME, e);
		}

		if (priorityRef == null) {
			throw new AlertServiceSystemException("Alert type : " + alertTypeRef + " refers no alert priority");
		}

		Collection<ValueTO> priorityValues;
		try {
			priorityValues = env.getAllPropertiesValues(priorityRef);
		} catch (ObjectNotFoundException e) {
			logger.warn("Failed to load alert notification template for alert type : " + alertTypeName, e);
			return;
		}

		AlertPriority priority = CollectionUtils
		        .map(priorityValues, Converters.propertiesToAlertPriorityConverter(env));

		final List<EscalationTier> tiers = CollectionUtils.newList();

		List<PriorityTier> persistentTiers = priority.getTiers();

		final Iterator<PriorityTier> i = persistentTiers.iterator();

		if (logger.isDebugEnabled()) {
			logger.debug("Loading alert notification template tiers...");
		}

		int tierCount = 0;
		while (i.hasNext()) {
			tierCount++;
			final PriorityTier tier = i.next();
			final EscalationTier nextEscTier = new EscalationTier(tier.getName(), tierCount,
			        tier.getNotificationAttempts(), tier.getInterval());
			tiers.add(nextEscTier);

			for (final MessageType messageType : MessageType.values()) {
				DestinationProvider destProv = DestinationProviderFactory.getDestinationProvider(env, userService,
				        alertTypeName, tier.getName(), messageType);
				nextEscTier.addMainItem(new NTemplateItem(messageType.name(), messageType, destProv));
			}

			// don't check single tier setup or last tier in multiple tiers
			// setup
			boolean isEscalationRequired = tier.isSendEscalation() && persistentTiers.size() != tierCount;

			if (isEscalationRequired) {
				// the trick is we create escalation templates dynamically
				String templateName = "ESCALATION_" + tierCount;
				if (!templates.containsKey((templateName))) {
					String msgHeader = LocalizationUtils.getLocalizedString(
					        "email_message_alert_escalated_to_tier_header", Integer.toString(tierCount + 1));
					String msgBody = LocalizationUtils.getLocalizedString("email_message_alert_escalated_to_tier_body",
					        "$ALERT_NAME$", "$name$");
					templates.put(templateName, new CommonMessageTemplate(msgHeader, msgBody));
				}

				// this tier is surrogate for internal purposes, tricky name
				// tier ordinal is the same as for outer var 'tier' because the
				// real tier is not really changed yet
				// this is required by send ack,res,dis behaviour - showing
				// current tier ordinal
				final EscalationTier sendEscTier = new EscalationTier(tier.getName().concat("_ESCALATION"), tierCount,
				        1, 1000);
				// escalation message should be sent to current tier recipient
				// list
				for (final NTemplateItem item : nextEscTier.getMainItems()) {
					sendEscTier.addMainItem(new NTemplateItem(templateName, item.getProtocol(), item
					        .getDestinationProvider()));
				}

				tiers.add(sendEscTier);
			}

			if (tier.isSendAck()) {
				for (final NTemplateItem item : nextEscTier.getMainItems()) {
					nextEscTier.addAckItem(new NTemplateItem("ACK", item.getProtocol(), item.getDestinationProvider()));
				}
			}

			if (tier.isSendResolve()) {
				for (final NTemplateItem item : nextEscTier.getMainItems()) {
					nextEscTier.addResolveItem(new NTemplateItem("RESOLVE", item.getProtocol(), item
					        .getDestinationProvider()));
				}
			}

			if (logger.isDebugEnabled()) {
				logger.debug("Added tier : " + tier.toString());
			}

		}

		Macro postProcessingMacro = AlertMacroFactory.getAllMacros();

		if (logger.isDebugEnabled()) {
			StringBuilder logMessage = new StringBuilder();
			logMessage.append("Loading message templates :\n");
			for (Entry<String, MessageTemplate> t : templates.entrySet()) {
				logMessage.append(t.getKey());
				logMessage.append("=");
				logMessage.append(t.getValue().toString());
				logMessage.append("\n");
			}
			logger.debug(logMessage);
		}

		final NotificationTemplate template = new AlertNotificationTemplate(alertTypeName, priority.getName(),
		        templates, tiers, postProcessingMacro, stateManager);

		notificationTemplates.put(alertTypeRef, template);

		if (logger.isDebugEnabled()) {
			logger.debug("Alert notification template of alert type {" + alertTypeName
			        + "} has been successfully loaded");
		}
	}

	private Map<String, MessageTemplate> initTemplates(Set<com.synapsense.dto.MessageTemplate> dtoTemplates) {
		// init templates
		final Map<String, MessageTemplate> templates = CollectionUtils.newLinkedMap();
		for (final com.synapsense.dto.MessageTemplate mt : dtoTemplates) {
			String body = mt.getBody(env);
			if (body == null || body.isEmpty()) {
				body = DEFAULT_TEMPLATE_BODY;
			}
			templates.put(mt.getMessageType().name(), new CommonMessageTemplate(mt.getHeader(env), body));
		}
		// set default template for not configured transport types except
		// console type
		for (MessageType t : MessageType.values()) {
			if (!MessageType.CONSOLE.equals(t) && !templates.containsKey(t.name())) {
				templates.put(t.name(), DEFAULT_MESSAGE_TEMPLATE);
			}
		}

		// special template for acknowledge messages
		templates.put("ACK", ACKNOWLEDGE_MESSAGE_TEMPLATE);
		// special template for resolve messages
		templates.put("RESOLVE", RESOLVE_MESSAGE_TEMPLATE);

		return templates;
	}

	private void restoreAllAlertNotifications() {

		Command c = new Command() {

			@Override
			public void execute() {
                if (logger.isDebugEnabled()) {
                    logger.debug("Restoring alert notifications...");
                }
				final Collection<TO<?>> alerts = env.getObjectsByType(ALERT_TYPE_NAME);
				for (TO<?> alertToRestore : alerts) {
                    if (logger.isDebugEnabled()) {
                        logger.debug("Restoring alert " + alertToRestore);
                    }
					Collection<ValueTO> alertInititalValues;
					try {
						alertInititalValues = env.getAllPropertiesValues(alertToRestore);
					} catch (ObjectNotFoundException e) {
						logger.debug("Failed to load alert = " + alertToRestore + " properties", e);
                        continue;
					}

					Map<String, Object> alertInititalValuesAsMap = EnvironmentUtils.valuesAsMap(alertInititalValues);
					TO<?> alertTypeId = (TO<?>) alertInititalValuesAsMap.get("type");

					if (alertTypeId == null) {
						logger.warn("Restoring alert : "
						        + alertToRestore
						        + " has no initial property 'type' which should refer to a type of alert. Notification will not be restored");
                        continue;
					}

					Alert alertDto = CollectionUtils.map(alertInititalValues,
					        Converters.propertiesToAlertConverter(alertToRestore, env));
					restoreAlertNotification(alertToRestore, alertTypeId, alertDto);
				}

			}
		};

		commandEngine.addCommand(c);
	}

}
