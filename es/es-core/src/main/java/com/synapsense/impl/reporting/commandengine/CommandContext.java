package com.synapsense.impl.reporting.commandengine;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.impl.reporting.commandengine.state.CommandState;
import com.synapsense.service.reporting.dto.CommandStatus;

public final class CommandContext {

	private Log log = LogFactory.getLog(CommandContext.class);

	private CommandState state;

	private Command command;

	private double progress;

	private CommandStatus status;

	private Progress commandProgress = new ProgressNul();

	public CommandContext(Command command) {
		this.command = command;
		setState(CommandState.READY);
		setStatus(CommandStatus.notStarted(command.getName() + " is not started"));
	}

	public CommandState getState() {
		return state;
	}

	public Command getCommand() {
		return command;
	}

	public void setState(CommandState commandState) {
		if (log.isDebugEnabled()) {
			log.debug("Changing command '" + command.getName() + "' state to " + commandState.toString());
		}
		this.state = commandState;
	}

	public void start() {
		state.start(this);
	}

	public void cancel() {
		state.cancel(this);
	}

	public void suspend() {
		state.suspend(this);
	}

	public void resume() {
		state.resume(this);
	}

	public void complete() {
		state.complete(this);
	}

	public void fail(Exception error) {
		commandProgress.reportError(error);
		state.fail(this);
	}

	public boolean removable() {
		return state.removable();
	}

	public double getProgress() {
		return progress;
	}

	public void setProgress(double progress) {
		this.progress = progress;
		commandProgress.showProgress(progress * 100 + "%");
	}

	public CommandStatus getStatus() {
		return status;
	}

	public void setStatus(CommandStatus status) {
		this.status = status;
		commandProgress.showProgress(status.getStatusMessage());
	}

	public void setCommandProgress(Progress commandProgress) {
		this.commandProgress = commandProgress;
	}
}
