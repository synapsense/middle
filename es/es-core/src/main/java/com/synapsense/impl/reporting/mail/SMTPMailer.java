package com.synapsense.impl.reporting.mail;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import com.synapsense.service.reporting.exception.SmtpReportingException;

public class SMTPMailer {

	private Provider<Session> sessionProvider;

	public SMTPMailer(Provider<Session> sessionProvider) {
		this.sessionProvider = sessionProvider;
	}

	public void send(String subject, String body, String[] attachFiles, String recipient) throws SmtpReportingException {
		send(subject, body, attachFiles, new String[] { recipient });
	}

	public void send(String subject, String body, String[] attachFiles, String[] recipients) throws SmtpReportingException {
		Session session = sessionProvider.get();

		Message smtpMessage = new MimeMessage(session);
		try {
			String from = session.getProperty("mail.from");
			if (from == null || from.isEmpty()) {
				return;
			}
			smtpMessage.setFrom(new InternetAddress(from));
			smtpMessage.setSubject(subject);

			// Create the message part
			MimeBodyPart messageBodyPart = new MimeBodyPart();

			// Fill message
			messageBodyPart.setText(body);
			Multipart multipart = new MimeMultipart();
			multipart.addBodyPart(messageBodyPart);

			if (attachFiles != null) {
				// Attachments
				for (String fileAttachment : attachFiles) {
					messageBodyPart = new MimeBodyPart();
					DataSource source = new FileDataSource(fileAttachment);
					messageBodyPart.setDataHandler(new DataHandler(source));
					messageBodyPart.setFileName(fileAttachment.substring(fileAttachment.lastIndexOf('/') + 1));
					multipart.addBodyPart(messageBodyPart);
				}
			}

			// Put parts in message
			smtpMessage.setContent(multipart);

			// Fill recipients
			for (String nextRecipient : recipients) {
				if (nextRecipient.isEmpty()) {
					continue;
				}
				smtpMessage.addRecipient(javax.mail.Message.RecipientType.TO, new InternetAddress(nextRecipient));
			}
			if (smtpMessage.getRecipients(javax.mail.Message.RecipientType.TO) == null) {
				throw new SmtpReportingException("No correct destination addresses specified");
			}
            Transport.send(smtpMessage);

		} catch (AddressException e) {
			throw new SmtpReportingException("Unable to deliver the message due to address error", e);
		} catch (MessagingException e) {
			throw new SmtpReportingException("Unable to deliver the message", e);
		}
	}

}
