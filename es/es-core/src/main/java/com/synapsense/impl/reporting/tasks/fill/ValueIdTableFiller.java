package com.synapsense.impl.reporting.tasks.fill;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;

import com.synapsense.impl.reporting.tasks.EnvValuesTaskData;
import com.synapsense.impl.reporting.tasks.fill.ReportTableFiller.TableFiller;
import static com.synapsense.impl.reporting.tasks.fill.ReportTableFiller.*;
import com.synapsense.service.reporting.Cell;
import com.synapsense.service.reporting.olap.Member;
import com.synapsense.service.reporting.olap.custom.Cube;
import com.synapsense.util.CollectionUtils;
import com.synapsense.util.unitconverter.UnitConverter;


public class ValueIdTableFiller implements TableFiller {

	@Override
	public void fill(Object[] row, Calendar calendar, EnvValuesTaskData envValuesTaskData) {
		int valueIdColumn = 0;
		int rowValueId = (Integer) row[valueIdColumn];
		int timestampColumn = 1;
		Timestamp timestamp = (Timestamp) row[timestampColumn];
		List<UnitConverter> unitConverters = envValuesTaskData.getUnitConverters();
		List<String> aggregationTypes = CollectionUtils.newList(envValuesTaskData.getFormulaCellsMap().keySet());
		for (Cell cell : envValuesTaskData.getDataSet()) {
			Member timeMember = cell.getMember(Cube.DIM_TIME);
			//This is for the case if TimeHierarchy is only in slicer
			calendar.setTime(timestamp);
			if ((timeMember == null
					|| ReportTableFiller.containsTime(timeMember, calendar))
					&& cell.getValueIds().contains(rowValueId)) {
				int aggTypeIndex = 2;
				
				addValue(cell, row, aggregationTypes, aggTypeIndex, timestamp, unitConverters);
			}
		}
	}

}
