package com.synapsense.impl.reporting.queries;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.log4j.Logger;

import com.synapsense.exception.IllegalInputParameterException;
import com.synapsense.impl.reporting.engine.ReportUserInfo;
import com.synapsense.impl.reporting.queries.AxisProcessor.ValueIdAttributesProvider;
import com.synapsense.impl.reporting.queries.types.ReportingQueryType;
import com.synapsense.impl.reporting.tables.HistoricalPropertyTypeProvider;
import com.synapsense.impl.reporting.tables.LinkedPropertyTypeProvider;
import com.synapsense.impl.reporting.tables.ObjectTypeTable;
import com.synapsense.impl.reporting.tables.ReportingTable;
import com.synapsense.impl.reporting.tables.ReportingTableHolder;
import com.synapsense.impl.reporting.tables.ReportingTableHolder.ReportingHelperTableType;
import com.synapsense.impl.reporting.tables.ValueIdAttributesTable;
import com.synapsense.impl.reporting.tasks.EnvValuesTaskData;
import com.synapsense.impl.reporting.tasks.EnvValuesTaskData.EnvValuesTaskDataBuilder;
import com.synapsense.impl.reporting.tasks.ValueIdsTaskData;
import com.synapsense.impl.reporting.utils.ReportingConstants.QUERY.SYMBOL;
import com.synapsense.service.reporting.Cell;
import com.synapsense.service.reporting.DimensionalQuery;
import com.synapsense.service.reporting.olap.AxisSet;
import com.synapsense.service.reporting.olap.CrossJoin;
import com.synapsense.service.reporting.olap.OrderedHierarchy;
import com.synapsense.service.reporting.olap.custom.BaseLevel;
import com.synapsense.service.reporting.olap.custom.Cube;
import com.synapsense.service.reporting.olap.custom.EnvironmentDataSource;
import com.synapsense.service.reporting.olap.custom.Level;
import com.synapsense.util.CollectionUtils;
import com.synapsense.util.unitconverter.UnitConverter;

/**
 * The <code>TableProcessor</code> class builds Report HQL/SQL queries and Parameter Map
 * 
 * @author Grigory Ryabov
 */
public class TableProcessor {
	
	private static final Logger log = Logger.getLogger(TableProcessor.class);
	
	private EnvironmentDataSource dataSource;
	
	private List<Cell> valueIdCells;
	
	ValueIdAttributesTable valueIdAttributesTable;

	private DimensionalQuery query;
		
	private ValueIdAttributesProvider linkedPropertyTypeProvider, historicalPropertyTypeProvider;
	
	private Map<String, ValueIdAttributesProvider> valueIdAttributesProvidersMap;
	
	private ReportingTableHolder reportingTables;
	
	private List<UnitConverter> unitConverters = CollectionUtils.newList();

	private ReportUserInfo reportUserInfo;

	private AxisProcessor axisProcessor;

	private Map<String, Set<Cell>> formulaCellsMap;

	private boolean singleValueIdPerCellTable;

	private boolean multipleObjectIdCellTable;

	private boolean differentMultiplePropertyCellsTable;

	private ReportingQueryType queryType;
	
	public TableProcessor(EnvironmentDataSource ds, DimensionalQuery query, ReportUserInfo reportUserInfo) {
		this.dataSource = ds;
		this.query = query;
		this.valueIdCells = new ArrayList<Cell>();
		this.reportUserInfo = reportUserInfo;
		
		linkedPropertyTypeProvider = new LinkedPropertyTypeProvider(dataSource);
		historicalPropertyTypeProvider = new HistoricalPropertyTypeProvider();
		valueIdAttributesProvidersMap = CollectionUtils.newMap();
		valueIdAttributesProvidersMap.put("com.synapsense.dto.TO", linkedPropertyTypeProvider);
		valueIdAttributesProvidersMap.put("java.lang.Double", historicalPropertyTypeProvider);
	}
	
	public List<Cell> processSets(){
		
		AxisSet slicer = query.getSlicer();
		AxisSet rows = query.getAxisSet(0);
		AxisSet columns = query.getAxisSet(1);
		if (rows == null || columns == null) {
			throw new IllegalInputParameterException("Both Query Axises should be not null");
		}
		this.reportingTables = new ReportingTableHolder();
		ReportingTable slicerObjectTable = null;
		if (slicer != null && slicer.getDimensionalities().contains(Cube.OBJECT)){
			AxisProcessor slicerAxisProcessor = new AxisProcessor(this.dataSource, this, reportingTables);
			SlicerProcessor slicerProcessor = new SlicerProcessor(slicerAxisProcessor);
			slicer.buildQuery(slicerProcessor);
			slicerObjectTable = slicerProcessor.getSlicerObjectTable();			
		} else {
			slicerObjectTable = new ObjectTypeTable();
		}
		
		reportingTables.put(ReportingHelperTableType.SLICER_OBJECT_TYPE_TABLE, slicerObjectTable);
		AxisSet tableSet = query.getTableSet();
//		CrossJoin axisCrossJoin = (slicer == null) ? new CrossJoin(rows, columns) : new CrossJoin(slicer, rows, columns);
		axisProcessor = new AxisProcessor(this.dataSource, this, reportingTables);
        tableSet.buildQuery(axisProcessor);
		this.valueIdAttributesTable = axisProcessor.getValueIdAttributesTable();
		
		return this.valueIdCells;
		
	}
	
	public ValueIdsTaskData buildValueIdQuery(){	
		
		StringBuilder valueIdsQuerySB = new StringBuilder();
		
		int objectIdsIndex = 0;
		StringBuilder tupleSetWhereSB = new StringBuilder();
		Map<Integer, List<Integer>> indexObjectIdsMap = CollectionUtils.newMap();
		for (Entry<String, ValueIdAttributesProvider> providersMapEntry: valueIdAttributesProvidersMap.entrySet()) {
			Map<String, Set<Integer>> providerMap = providersMapEntry.getValue().getMap();
			for (Entry<String, Set<Integer>>  providerMapEntry : providerMap.entrySet()) {
				indexObjectIdsMap.put(objectIdsIndex, new LinkedList<>(providerMapEntry.getValue()));
				tupleSetWhereSB.append(" val.object.objectId in (:objectIds"
						+ objectIdsIndex++
						+ ")"
						+ " and val.attribute.attributeName = '"
						+ providerMapEntry.getKey() + "' or");
			}
		}
		if (tupleSetWhereSB.length() == 0) {
			if(log.isDebugEnabled()){
				log.debug("No value Ids requested by DimensionalQuery. Please specify both objects and properties");
			}
		} else {
			valueIdsQuerySB.append(QueryClause.SELECT.name()).append(SYMBOL.SPACE);
			valueIdsQuerySB.append("val.valueId, val.attribute.attributeName, val.object.objectId FROM ValueDO val ");
			valueIdsQuerySB.append(QueryClause.WHERE.name());
			valueIdsQuerySB.append(tupleSetWhereSB.substring(0, tupleSetWhereSB.length() - 3));
		}
		//Set data needed for GetValueIdsTask
		return new ValueIdsTaskData(valueIdsQuerySB.toString(), valueIdAttributesTable, valueIdCells, indexObjectIdsMap);		
	
	}
	
	public EnvValuesTaskData buildEnvDataQuery(EnvValuesTaskData envValuesTaskData) {
		this.queryType = envValuesTaskData.getQueryType();

		this.formulaCellsMap = envValuesTaskData.getFormulaCellsMap();
		WhereClauseBuilder whereClauseBuilder = new WhereClauseBuilder();
		String whereClause = whereClauseBuilder.build(this);
		
		Level lowestTimeLevel = whereClauseBuilder.getLowestTimeLevel();
		String lowestTimeLevelName = lowestTimeLevel.getName();
		OrderedHierarchy timeHierarchy = Cube.getOrderedHierarchy(lowestTimeLevel.getHierarchy().getName());
		//After getting lowest time level from query itself user sets preferred one
		String commonLowestTimeLevel = query.getFormula().getAggregationRange();
		if (commonLowestTimeLevel.equals(Cube.ALL_RANGE)) {
			lowestTimeLevelName = Cube.ALL_RANGE;
		} else if (commonLowestTimeLevel != null) {
			lowestTimeLevelName = commonLowestTimeLevel;
		}
		String selectClause = new SelectClauseBuilder().build(this);
		GroupByClauseBuilder groupByClauseBuilder = new GroupByClauseBuilder(new BaseLevel(timeHierarchy, lowestTimeLevelName));
		String groupByClause = groupByClauseBuilder.build(this);
		String orderByClause = new OrderByClauseBuilder(groupByClauseBuilder).build(this);
		StringBuilder envDataQuerySB = new StringBuilder();
		envDataQuerySB.append(selectClause)
			.append(SYMBOL.SPACE)
			.append(whereClause)
			.append(SYMBOL.SPACE)
			.append(groupByClause)
			.append(SYMBOL.SPACE)
			.append(orderByClause);
		return new EnvValuesTaskDataBuilder()
			.envDataQuery(envDataQuerySB.toString())
			.valueIdCells(valueIdCells)
			.unitConverters(unitConverters)
			.formulaCellsMap(envValuesTaskData.getFormulaCellsMap())
			.queryType(queryType)
			.cells(envValuesTaskData.getDataSet())
			.cellObjectMap(envValuesTaskData.getCellObjectMap())
			.cellPropertyMap(envValuesTaskData.getCellPropertyMap())
			.build();
	}
	
	public DimensionalQuery getQuery() {
		return query;
	}

	public Set<ReportingTable> getTimeReportingTables() {
		return reportingTables.getReportingTimeTables();
	}

	public ValueIdAttributesProvider getProvider (String className){
		return valueIdAttributesProvidersMap.get(className);
	}
	
	public List<Cell> getValueIdCells() {
		return Collections.unmodifiableList(valueIdCells);
	}
	
	public void addValueIdCell(Cell valueIdCell) {
		this.valueIdCells.add(valueIdCell);
	}
	
	public enum GroupType {
		OBJECT("Object", "item.id.value", "", false, null)
		, SECOND("Second", "SECOND(timestamp)", "DATE_FORMAT(timestamp,'%Y-%m-%d %H:%i:%s')", true, "%s")
		, MINUTE("OneMin", "MINUTE(timestamp)", "DATE_FORMAT(timestamp,'%Y-%m-%d %H:%i')", true, "%i")
		, HOUR("Hour", "HOUR(timestamp)", "DATE_FORMAT(timestamp,'%Y-%m-%d %H:00')", true, "%H")
		, DAY("Day", "DAY(timestamp)", "DATE_FORMAT(timestamp,'%Y-%m-%d 00:00')", true, "%d")
		, MONTH("Month", "MONTH(timestamp)", "DATE_FORMAT(timestamp,'%Y-%m-01 00:00')", true, "%m")
		, YEAR("Year", "YEAR(timestamp)", "DATE_FORMAT(timestamp,'%Y-01-01 00:00')", true, "%Y")
		, MONDAY_WEEK(Cube.LEVEL_WEEK, "WEEKOFYEAR(timestamp)", "DATE_FORMAT(timestamp,'%Y %u 00:00')", true, "%u")
		, SUNDAY_WEEK(Cube.LEVEL_WEEK, "WEEKOFYEAR(timestamp)", "DATE_FORMAT(timestamp,'%Y %U 00:00')", true, "%U");

		private String typeName;
		private String groupType;
		private String columnQuery;
		private String format;
		
		public String getFormat() {
			return format;
		}

		public String getColumnQuery() {
			return columnQuery;
		}

		private GroupType(String typeName, String groupType, String columnQuery, boolean isTimeType, String format) {
			this.typeName = typeName;
			this.groupType = groupType;
			this.columnQuery = columnQuery;
			this.format = format;
		}

		public static GroupType getTypeByName(String typeName) {
			for (GroupType type : GroupType.values()) {
				if (type.typeName.equals(typeName))
					return type;
			}
			throw new IllegalInputParameterException("Not supported Group Type, type=" + typeName);	
		}

		public String getGroupType() {
			return groupType;
		}
		
		public static GroupType getWeekType() {
			int firstDayOfWeek = Calendar.getInstance().getFirstDayOfWeek();
			return firstDayOfWeek == Calendar.MONDAY ? MONDAY_WEEK : SUNDAY_WEEK;
		}
	}
	
	public enum QueryClause {
		SELECT("select"), 
		WHERE("where"), 
		GROUP_BY("group by"),
		ORDER_BY("order by"), 
		VALUE_IDS_QUERY ("valueIdsQuery"), 
		ENV_DATA_QUERY ("envDataQuery");
		private String name;

		private QueryClause(String name){
			this.name = name;
		}

		public String getName() {
			return this.name;
		}
	}
	
	Map<String, Set<Cell>> getFormulaCellsMap() {
		return Collections.unmodifiableMap(formulaCellsMap);
	}
	

	public void addUnitConverter(UnitConverter unitConverter) {
		if (!unitConverters.contains(unitConverter)) {
			this.unitConverters.add(unitConverter);
		}		
	}

	public List<UnitConverter> getUnitConverters() {
		return Collections.unmodifiableList(this.unitConverters);
	}

	public ReportUserInfo getReportUserInfo() {
		return this.reportUserInfo;
	}
	
	public boolean isSingleValueIdPerCellTable() {
		return this.singleValueIdPerCellTable;
	}

	public boolean isMultipleObjectIdCellTable() {
		return multipleObjectIdCellTable;
	}

	boolean isDifferentMultiplePropertyCellsTable() {
		return differentMultiplePropertyCellsTable;
	}
	
    public ReportingQueryType getQueryType() {
        return queryType;
    }

	
}
