package com.synapsense.impl.alerting;

import javax.ejb.EJB;
import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;

import org.apache.log4j.Logger;

import com.synapsense.dto.Alert;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.exception.PropertyNotFoundException;
import com.synapsense.exception.UnableToConvertPropertyException;
import com.synapsense.service.Environment;

/**
 * This interceptor blocks calls to AlertService.raiseAlert() for alerts on
 * disabled objects
 * 
 * @author OStepanov
 * 
 */
public class DisabledObjectsAlertInterceptor {

	private final static Logger logger = Logger.getLogger(DisabledObjectsAlertInterceptor.class);

	@EJB(beanName = "GenericEnvironment")
	private Environment env;

	// intercepting public void raiseAlert(final Alert alert)
	@AroundInvoke
	public Object intercept(final InvocationContext icx) throws Exception {

		Object alert = icx.getParameters()[0];

		if (alert == null || !Alert.class.isAssignableFrom(alert.getClass())) {
			return icx.proceed();// let the main logic deal with that
		}

		TO<?> hostObject = ((Alert) alert).getHostTO();

		if (hostObject == null || TOFactory.EMPTY_TO.equals(hostObject)) {
			if (logger.isDebugEnabled()) {
				logger.debug("Allowed raising of system alert: " + alert);
			}
			return icx.proceed();
		}

		Integer status = null;
		try {
			status = env.getPropertyValue(hostObject, "status", Integer.class);
		} catch (PropertyNotFoundException | UnableToConvertPropertyException e) {
			// this is OK; it only means that it's not even possible
			// to disable this object
		}
		if (status == null || status != 2) {
			if (logger.isDebugEnabled()) {
				logger.debug("Object " + hostObject + "is enabled; allowed raising alert: " + alert);
			}
			return icx.proceed();
		}

		if (logger.isDebugEnabled()) {
			logger.debug("Object " + hostObject + "is disabled; disallowed raising alert: " + alert);
		}
		return null;
	}
}
