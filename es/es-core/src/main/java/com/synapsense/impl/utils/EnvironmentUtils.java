package com.synapsense.impl.utils;

import com.synapsense.dto.CollectionTO;
import com.synapsense.dto.ObjectType;
import com.synapsense.dto.PropertyDescr;
import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.IllegalInputParameterException;
import com.synapsense.util.CollectionUtils;
import com.synapsense.util.algo.Finder;
import com.synapsense.util.algo.ForEach;
import com.synapsense.util.algo.Predicate;
import com.synapsense.util.algo.UnaryFunctor;
import java.util.Collection;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Vector;

public final class EnvironmentUtils {

	private EnvironmentUtils() {
	}

	public static boolean areTOsWithSameType(Collection<TO<?>> tos) {
		if (tos == null)
			throw new IllegalInputParameterException("tos cannot be null");
		if (tos.size() == 0)
			throw new IllegalInputParameterException("objIds cannot be empty");

		if (tos.size() == 1)
			return true;
		final String initialTypeName = tos.iterator().next().getTypeName();
		return (new Finder<TO<?>>().find(tos, new Predicate<TO<?>>() {
			// if an object type name differs from initial typename - return the
			// Object
			@Override
			public boolean evaluate(TO<?> value) {
				return (!value.getTypeName().equals(initialTypeName));
			}
		}) == null);
	}

	public static CollectionTO lookForTOInCollection(final Collection<CollectionTO> where, final TO<?> what) {
		return new Finder<CollectionTO>().find(where, new Predicate<CollectionTO>() {
			@Override
			public boolean evaluate(CollectionTO value) {
				return value.getObjId().equals(what);
			}
		});
	}

	public static String[] getPropNamesForTypeName(ObjectType ot) {
		final Collection<String> propNames = new Vector<String>(ot.getPropertyDescriptors().size());

		new ForEach<PropertyDescr>(ot.getPropertyDescriptors()).process(new UnaryFunctor<PropertyDescr>() {
			@Override
			public void process(PropertyDescr value) {
				propNames.add(value.getName());
			}
		});
		return propNames.toArray(new String[propNames.size()]);
	}
	
	/** Similar to {@link com.synapsense.dto.DTO#toMap DTO.toMap()}. */
	public static Map<String, Object> valuesAsMap(Collection<ValueTO> values) {
		Map<String, Object> map = CollectionUtils.newMap();
		for (ValueTO v : values) {
			map.put(v.getPropertyName(), v.getValue());
		}
		return map;
	}

	public static Collection<ValueTO> mapAsValues(Map<String, Object> map) {
		Collection<ValueTO> values = CollectionUtils.newList();
		for (Entry<String, Object> entry : map.entrySet()) {
			values.add(new ValueTO(entry.getKey(), entry.getValue()));
		}
		return values;
	}
	
}
