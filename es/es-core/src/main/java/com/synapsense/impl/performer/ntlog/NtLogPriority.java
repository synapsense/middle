package com.synapsense.impl.performer.ntlog;

import org.apache.log4j.Level;

public enum NtLogPriority {
/*
 * NT EventLog only knows 3 event types of interest to us: ERROR, WARNING, and
 * INFO.
 */
CRITICAL("CRITICAL", Level.FATAL), MAJOR("MAJOR", Level.WARN), MINOR("MINOR", Level.WARN), INFORMATIONAL(
        "INFORMATIONAL", Level.INFO);

private Level eventLogLevel;
private String esPriority;

private NtLogPriority(final String esPriority, final Level eventLogLevel) {
	this.eventLogLevel = eventLogLevel;
	this.esPriority = esPriority;
}

public String getEsPriority() {
	return esPriority;
}

public Level getEventLogLevel() {
	return eventLogLevel;
}

public static Level getLevelByEsPriority(String esPriority) {
	for (NtLogPriority priority : NtLogPriority.values()) {
		if (esPriority.equals(priority.getEsPriority())) {
			return priority.eventLogLevel;
		}
	}
	return MAJOR.getEventLogLevel();
}
}
