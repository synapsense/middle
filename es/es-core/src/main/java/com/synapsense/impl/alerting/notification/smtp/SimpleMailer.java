package com.synapsense.impl.alerting.notification.smtp;

import com.synapsense.dto.Message;
import com.synapsense.impl.alerting.notification.Provider;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.SimpleEmail;

import javax.mail.Session;

/**
 * Plain text email sender
 *
 * @author : shabanov
 */
public class SimpleMailer extends SMTPMailer {

	public SimpleMailer(Provider<Session> sessionPrv) {
		super(sessionPrv);
	}

	@Override
	protected Email generateEmail(Message message) throws EmailException {
		SimpleEmail email = new SimpleEmail();

        email.setMsg(message.getContent());
		return email;
	}
}
