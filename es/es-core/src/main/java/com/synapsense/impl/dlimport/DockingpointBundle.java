package com.synapsense.impl.dlimport;

/**
 * Represents a dockingpoint (a pointer to a single other object) to be
 * filled in later in the export process.
 * 
 * @author Gabriel Helman
 * @since Mars
 * Date: 5/6/11
 * Time: 4:29 PM
 */
public class DockingpointBundle {
	private String dlid;
	private String propertyName;
	private String target;

	public DockingpointBundle(String dlid, String propertyName, String target) {
		this.dlid = dlid;
		this.propertyName = propertyName;
		this.target = target;
	}

	public String getDlid() {
		return dlid;
	}

	public void setDlid(String dlid) {
		this.dlid = dlid;
	}

	public String getPropertyName() {
		return propertyName;
	}

	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}

	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}

	@Override
	public String toString() {
		return "DockingpointBundle{" + "dlid='" + dlid + '\'' + ", propertyName='" + propertyName + '\'' + ", target='"
		        + target + '\'' + '}';
	}
}
