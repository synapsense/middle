package com.synapsense.impl.performer.audio;


public class TurnOffLightTask implements Runnable {

	private SignalSender sender;

	public TurnOffLightTask(SignalSender sender) {
		this.sender = sender;
	}

	@Override
	public void run() {

		sender.sendTurnOffLightRequest();

	}

}
