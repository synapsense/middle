package com.synapsense.impl.devmanager.dissectors;

import com.synapsense.dto.TO;
import com.synapsense.plugin.messages.GenericMessage;
import com.synapsense.plugin.messages.WSNNodeVersionBuilder;
import com.synapsense.plugin.messages.GenericMessage.GenericMessageTypes;
import com.synapsense.service.impl.messages.base.NodeVersionData;
import com.synapsense.util.algo.Pair;
import com.synapsense.util.algo.UnaryFunctor;

public class WSNNodeVersionDissector implements MessageDissector {
	private final UnaryFunctor<Pair<TO<?>, NodeVersionData>> functor;

	public WSNNodeVersionDissector(UnaryFunctor<Pair<TO<?>, NodeVersionData>> functor) {
		this.functor = functor;
	}

	@Override
	public void process(TO<?> objID, GenericMessage msg) {
		if (msg == null)
			throw new IllegalArgumentException("msg cannot be null");
		if (!tasteMessage(msg))
			throw new IllegalArgumentException("Incompatible message type");
		try {
			NodeVersionData data = new NodeVersionData();
			data.setAppVersion((Integer) msg.get(WSNNodeVersionBuilder.FieldConstants.APP_VERSION.name()));
			data.setAppRevision((Integer) msg.get(WSNNodeVersionBuilder.FieldConstants.APP_REVISION.name()));
			data.setNwkVersion((Integer) msg.get(WSNNodeVersionBuilder.FieldConstants.NWK_VERSION.name()));
			data.setNwkRevision((Integer) msg.get(WSNNodeVersionBuilder.FieldConstants.NWK_REVISION.name()));
			data.setOsVersion((Integer) msg.get(WSNNodeVersionBuilder.FieldConstants.OS_VERSION.name()));
			data.setOsRevision((Integer) msg.get(WSNNodeVersionBuilder.FieldConstants.OS_REVISION.name()));
			functor.process(new Pair<TO<?>, NodeVersionData>(objID, data));
		} catch (Exception e) {
			throw new IllegalArgumentException("Wrong data is stored in message", e);
		}
	}

	@Override
	public boolean tasteMessage(GenericMessage msg) {
		return msg.getType() == GenericMessageTypes.WSN_NODEVERSION;
	}
}
