package com.synapsense.impl.diagnostic;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.Authenticator;
import java.net.PasswordAuthentication;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Singleton;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import org.apache.log4j.Logger;

import com.synapsense.cdi.MBean;
import com.synapsense.dto.CollectionTO;
import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;
import com.synapsense.impl.diagnostic.logfinder.LogFinder;
import com.synapsense.service.DiagnosticService;
import com.synapsense.service.Environment;
import com.synapsense.service.UserManagementService;

@Singleton(name = "DiagnosticService")
@Remote(DiagnosticService.class)
@ConcurrencyManagement(ConcurrencyManagementType.BEAN)
@MBean("com.synapsense:type=DiagnosticService")
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class DiagnosticServiceImpl implements DiagnosticService, DiagnosticServiceImplMBean {
	private static final Logger LOG = Logger.getLogger(DiagnosticServiceImpl.class);

	private static final String infoDestinationFileName = "systemInfo.txt";
	private static final String dumpInfoFileName = "dumpInfo.txt";
	private static final String tablesCreationFileName = "create_tables.sql";
	private static final String userStatisticFileName = "userStatistic.csv";
	private static final int BUFFER_SIZE = 20 * 1024 * 1024; // 20 Mb

	private Environment env;
	private UserManagementService userManagement;

	@Override
	public synchronized String generateDiagnosticPackage() throws IOException {
		String packId = "package_" + System.currentTimeMillis() + ".zip";

		CommonUtils.makeTemporaryFolderIfNotExists();

		Collection<String> existingFileNames = findFiles();
		Collection<String> generatedFileNames = generateFiles();

		ZipBuilder zipBuilder = new ZipBuilder(CommonUtils.getTemporaryFolderPath() + File.separator + packId);
		zipBuilder.append(existingFileNames);
		zipBuilder.append(generatedFileNames, CommonUtils.getTemporaryFolderPath());
		appendActiveControlPackage(zipBuilder);

		zipBuilder.endBuild();
		deleteTemporaryFiles();

		return packId;
	}

	@Override
	public synchronized byte[] readDiagnosticFileBytes(String packId, long fileOffset) throws IOException {
		int bufferSize = BUFFER_SIZE;
		String fileName = CommonUtils.getTemporaryFolderPath() + File.separator + packId;

		File f = new File(fileName);

		long remainToRead = f.length() - fileOffset;
		if (remainToRead <= 0) {
			return null; // everything was read
		} else if (remainToRead < bufferSize) {
			bufferSize = (int) remainToRead;
		}

		byte data[] = new byte[bufferSize];
		int count = -1;
		RandomAccessFile raf = null;

		try {
			raf = new RandomAccessFile(fileName, "r");
			raf.seek(fileOffset);
			count = raf.read(data, 0, bufferSize);
		} catch (Exception e) {
			LOG.error(e.getLocalizedMessage(), e);
		} finally {
			if (raf != null) {
				raf.close();
			}
		}

		if (count != -1) {
			return data;
		} else {
			return null; // everything was read
		}
	}

	@Override
	public void deletePackage(String packId) {
		(new File(CommonUtils.getTemporaryFolderPath() + File.separator + packId)).delete();
	}

	@EJB(beanName = "Environment")
	public void setEnvironment(Environment env) {
		this.env = env;
	}
	
	@EJB(beanName = "UserManagementService")
	public void setUserManagement(UserManagementService userManagement) {
		this.userManagement = userManagement;
	}

	@Override
	@PostConstruct
	public void create() throws Exception {
		start();
	}

	@Override
	public void start() throws Exception {
	}

	@Override
	@PreDestroy
	public void stop() throws Exception {
		destroy();
	}

	@Override
	public void destroy() throws Exception {
	}

	private Collection<String> findFiles() throws IOException {
		Collection<String> fileNames = new ArrayList<String>();

		// log files
		fileNames.addAll(LogFinder.getLogFiles());

		// MapSense DL-files
		fileNames.addAll(MapsenseInfo.getMapsenseLatestDlFiles());

		return fileNames;
	}

	private Collection<String> generateFiles() throws IOException {
		Collection<String> fileNames = new ArrayList<String>();

		// SystemInfo
		SystemInfo.writeInfo(CommonUtils.getTemporaryFolderPath() + File.separator + infoDestinationFileName);
		fileNames.add(infoDestinationFileName);

		// DBDumper
		DBDumper.dumpTables(CommonUtils.getTemporaryFolderPath());
		fileNames.addAll(DBDumper.getTableFiles());

		// dump info
		writeStringToFileInfo(DBDumper.getMysqlImportStr(), CommonUtils.getTemporaryFolderPath() + File.separator
		        + dumpInfoFileName);
		fileNames.add(dumpInfoFileName);

		// tables creation script
		DBDumper.writeTableCreationScriptToFile(CommonUtils.getTemporaryFolderPath() + File.separator
		        + tablesCreationFileName);
		fileNames.add(tablesCreationFileName);

		// User statistic
		UserStatisticCSVGenerator.generateUserStatistic(userManagement,CommonUtils.getTemporaryFolderPath() + File.separator
		        + userStatisticFileName);
		fileNames.add(userStatisticFileName);

		return fileNames;
	}

	private void writeStringToFileInfo(String str, String fileName) throws IOException {
		FileWriter fw = new FileWriter(fileName);
		try {
			fw.write(str);
		} catch (Exception e) {
			LOG.error(e.getLocalizedMessage(), e);
		} finally {
			fw.close();
		}
	}

	private void deleteTemporaryFiles() {
		new File(CommonUtils.getTemporaryFolderPath() + File.separator + infoDestinationFileName).delete();
		new File(CommonUtils.getTemporaryFolderPath() + File.separator + dumpInfoFileName).delete();
		new File(CommonUtils.getTemporaryFolderPath() + File.separator + tablesCreationFileName).delete();
		new File(CommonUtils.getTemporaryFolderPath() + File.separator + userStatisticFileName).delete();
		for (String tblFileNames : DBDumper.getTableFiles()) {
			new File(CommonUtils.getTemporaryFolderPath() + File.separator + tblFileNames).delete();
		}
	}

	private static final String CONTROLLER = "CONTROLLER_STATUS";
	private static final String NAME = "name";
	private static final String PORT = "port";
	private static final String ACTIVE = "active";

	private static final String FILE = "/support_package.yaws";

	private static final String[] CONTROLLER_PROPS = { NAME, PORT, ACTIVE };

	private static HashMap<Long, String> STATUS_LABELS = new HashMap<Long, String>();

	static {
		STATUS_LABELS.put(0L, "passive");
		STATUS_LABELS.put(1L, "active");
	}

	private void appendActiveControlPackage(ZipBuilder zipBuilder) {
		// TODO: This code is specific to a certain object model and breaks the
		// ES generic design
		// In the future it is necessary to move it to a custom code package
		// like CRE or the console

		Collection<TO<?>> controllers = env.getObjectsByType(CONTROLLER);

		if (controllers.isEmpty()) {
			return;
		}

		// Set login/password for controller servers
		Authenticator.setDefault(new Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication("admin", "admin".toCharArray());
			}
		});
		Collection<CollectionTO> allProps = env.getPropertyValue(controllers, CONTROLLER_PROPS);
		for (CollectionTO collTO : allProps) {
			Long active = getValue(collTO, ACTIVE, Long.class);
			String host = getValue(collTO, NAME, String.class);
			Integer port = 80;
			if (active == null || host == null) {
				continue;
			}

			String[] parts = host.split("@", 2);
			if (parts.length > 1) {
				host = parts[1];
			}
			try {
				URL acURL = new URL("http", host, port, FILE);
				LOG.info("Getting AC support info from " + acURL.toString());
				zipBuilder.append(host + "_" + STATUS_LABELS.get(active) + ".zip",
				        acURL.openStream());
			} catch (Exception e) {
				LOG.warn("Unable to get Active Control support package from " + host, e);
			}
		}
	}

	@SuppressWarnings("unchecked")
	private <T> T getValue(CollectionTO collTo, String name, Class<T> clazz) {
		ValueTO valTo = collTo.getSinglePropValue(name);
		if (valTo == null) {
			return null;
		}
		Object val = valTo.getValue();
		return val == null ? null : (T) val;
	}
}
