package com.synapsense.impl.reporting.tasks;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import com.google.common.collect.ListMultimap;
import com.google.common.collect.Multimaps;
import com.synapsense.impl.reporting.tables.ValueIdAttributesTable;
import com.synapsense.service.reporting.Cell;
import com.synapsense.util.Pair;

public class ValueIdsTaskData {
	
	private String valueIdsQuery;

	private ValueIdAttributesTable valueIdAttributes;
	
	private List<Cell> valueIdCells;
	
	private List<Cell> cells;

	private Map<Integer, List<Integer>> valueIdsParameterMap;
	
	ListMultimap<Pair<String, Integer>, Integer> cellMap;

	public ValueIdsTaskData(String valueIdsQuery,
			ValueIdAttributesTable valueIdAttributes, List<Cell> valueIdCells,
			Map<Integer, List<Integer>> valueIdsParameterMap) {
		this.valueIdsQuery = valueIdsQuery;
		this.valueIdAttributes = valueIdAttributes;
		this.valueIdCells = valueIdCells;
		this.valueIdsParameterMap = valueIdsParameterMap;
	}

	public String getValueIdsQuery() {
		return valueIdsQuery;
	}

	public ValueIdAttributesTable getValueIdAttributes() {
		return valueIdAttributes.getTable();
	}

	public List<Cell> getValueIdCells() {
		return valueIdCells;
	}

	public Map<Integer, List<Integer>> getValueIdsParameterMap() {
		return Collections.unmodifiableMap(valueIdsParameterMap);
	}

	public void setCellMap(ListMultimap<Pair<String, Integer>, Integer> cellMap) {
		this.cellMap = cellMap;
	}

	public ListMultimap<Pair<String, Integer>, Integer> getCellMap() {
		return this.cellMap;
	}
	

	public void setCells(List<Cell> cells) {
		this.cells = cells;
	}

	public List<Cell> getCells() {
		return cells;
	}
	
}
