package com.synapsense.impl.devmanager;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.log4j.Logger;

import com.synapsense.dto.TOFactory;
import com.synapsense.plugin.messages.GenericMessage;
import com.synapsense.service.Environment;
import com.synapsense.service.impl.messages.SensedMessage;

@Stateless
public class SensedMessageProcessor implements MessageProcessor<SensedMessage> {
	private final static Logger logger = Logger.getLogger(SensedMessageProcessor.class);

	@EJB(beanName = "Environment")
	private Environment env;

	public SensedMessageProcessor() {
		InitialContext ctx;
		try {
			ctx = new InitialContext();
			env = (Environment) ctx.lookup("SynapServer/EnvironmentProxy/remote");
		} catch (NamingException e) {
			logger.warn("Couldn't obtain Environment proxy", e);
		}
	}

	@Override
	public void process(SensedMessage msg) {
		if (msg.getPayload().getClass() == GenericMessage.class) {
			// TODO: use dissectors here
			return;
		}

		try {
			env.setPropertyValue(TOFactory.getInstance().loadTO(msg.getObjectID()), msg.getPropertyName(),
			        msg.getPayload());
		} catch (Exception e) {
			logger.warn("Couldn't process message [" + msg.getObjectID() + ", " + msg.getPropertyName() + "] due to ",
			        e);
		}
	}
}
