package com.synapsense.impl.devmanager;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeSet;

import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Singleton;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import org.apache.log4j.Logger;
import org.apache.log4j.Priority;

import com.synapsense.cdi.MBean;
import com.synapsense.dto.Alert;
import com.synapsense.dto.CollectionTO;
import com.synapsense.dto.EnvObject;
import com.synapsense.dto.EnvObjectBundle;
import com.synapsense.dto.Relation;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOBundle;
import com.synapsense.dto.TOFactory;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.EnvException;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.plugin.messages.PowerRackConfBuilder;
import com.synapsense.plugin.messages.PowerRackConfBuilder.PhasesLabels;
import com.synapsense.service.AlertService;
import com.synapsense.service.Environment;
import com.synapsense.service.impl.dao.DataObjectDAO;
import com.synapsense.service.impl.dao.NetworkStatisticDAOIF;
import com.synapsense.service.impl.devmanager.DeviceEventListener;
import com.synapsense.service.impl.messages.base.DuoConfData;
import com.synapsense.service.impl.messages.base.NodeVersionData;
import com.synapsense.service.impl.messages.base.PhaseData;
import com.synapsense.service.impl.messages.base.PlugConfData;
import com.synapsense.service.impl.messages.base.PlugData;
import com.synapsense.service.impl.messages.base.RackConfData;
import com.synapsense.service.impl.messages.base.RefinedAlertData;
import com.synapsense.service.impl.messages.base.SandcrawlerConfData;
import com.synapsense.service.impl.messages.base.SensorData;
import com.synapsense.service.impl.messages.base.ServerConfData;
import com.synapsense.service.impl.messages.base.ServerInfo;
import com.synapsense.service.impl.messages.base.StatisticData;
import com.synapsense.util.CRC8Converter;
import com.synapsense.util.LocalizationUtils;
import com.synapsense.util.algo.Pair;
import com.synapsense.util.algo.SetsIntersector;
import com.synapsense.util.algo.SetsIntersector.AllValues;

@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
@Singleton @Remote(DeviceEventListener.class)
@ConcurrencyManagement(ConcurrencyManagementType.BEAN)
@MBean("com.synapsense:type=DeviceMapper")
public class DeviceMapper implements DeviceEventListener, DeviceMapperMBean {
	private static final Logger log = Logger.getLogger(DeviceMapper.class);
	private Environment generic;

	private AlertService alertService;

	/**
	 * Network node statistic DAO
	 */
	private DataObjectDAO<Integer, StatisticData> statDAO;

	/**
	 * Network base statistic DAO
	 */
	private DataObjectDAO<Integer, StatisticData> statBaseDAO;

	@Override
	public void onSensorData(TO<?> objID, SensorData data) {
		try {
			Integer sensorStatus = generic.getPropertyValue(objID, "status", Integer.class);
			// Check if sensor is disabled
			if (sensorStatus != 2) {
				Integer type = generic.getPropertyValue(objID, "type", Integer.class);

				if (type == data.getTypeId()) {

					ValueTO lastValue = new ValueTO("lastValue", data.getData(), data.getTimestamp());
					generic.setAllPropertiesValues(objID, new ValueTO[] { lastValue });

					if (sensorStatus != 1) {
						generic.setPropertyValue(objID, "status", 1);
					}

					TO<?> nto = generic.getParents(objID, "WSNNODE").iterator().next();
					Integer oldStatus = generic.getPropertyValue(nto, "status", Integer.class);
					if (oldStatus == 0) {
						log.info("Node " + nto + " became alive.");
						generic.setPropertyValue(nto, "status", 1);
					}
				} else {
					if (log.isEnabledFor(Priority.WARN)) {
						log.warn("Unable to store sensor data [" + data + "]\n sensor type mismatch, sensor = " + objID);
					}
				}
			} else {
				log.debug("Sensor: " + objID + " is currently disabled. Ignoring data packet.");
			}
		} catch (Exception e) {
			log.warn("Unable to store sensor data", e);
		}
	}

	public void onStatistic(TO<?> objID, StatisticData data) {
		DataObjectDAO<Integer, StatisticData> actualDAO;
		if (data.getLogicalId() >= 0xF000) { // here we store Network-related
			                                 // data
			actualDAO = statBaseDAO;
		} else { // and here the node-related statistic is stored
			actualDAO = statDAO;
		}
		data.setLogicalId((Integer) objID.getID());
		try {
			actualDAO.addData(data.getLogicalId(), data);
		} catch (ObjectNotFoundException e) {
			if (log.isEnabledFor(Priority.WARN)) {
				log.warn("Unable to store statistic data for " + objID);
			}
		}
	}

	public DataObjectDAO<Integer, StatisticData> getStatDAO() {
		return statDAO;
	}

	@EJB(beanName="NetworkStatisticDAO", beanInterface=NetworkStatisticDAOIF.class)
	public void setStatDAO(DataObjectDAO<Integer, StatisticData> statDAO) {
		this.statDAO = statDAO;
	}

	public DataObjectDAO<Integer, StatisticData> getStatBaseDAO() {
		return statBaseDAO;
	}

	@EJB(beanName="NetworkStatisticBaseDAO")
	public void setStatBaseDAO(DataObjectDAO<Integer, StatisticData> statBaseDAO) {
		this.statBaseDAO = statBaseDAO;
	}

	@Override
	public void onAlert(TO<?> objID, RefinedAlertData payload) {
		alertService
		        .raiseAlert(new Alert(payload.getName(), payload.getType(), new Date(), payload.getMessage(), objID));
	}

	@Override
	public void onGWStatus(TO<?> objID, int status) {
		try {
			generic.setPropertyValue(objID, "status", status);
			if (status == 0) {
				alertService.raiseAlert(new Alert("Gateway failure", "WSN_CONFIGURATION", new Date(), LocalizationUtils
				        .getLocalizedString("alert_message_gateway_failure", "$name$"), objID));
			}
		} catch (Exception e) {
			log.warn("Cannot set status: " + status + " for Gateway " + objID, e);
		}
	}

	@Override
	public void onNodeVersion(TO<?> objID, NodeVersionData data) {
		try {
			generic.setAllPropertiesValues(objID, new ValueTO[] {
			        new ValueTO("nodeAppVersion", data.getAppVersion() + " " + data.getAppRevision()),
			        new ValueTO("nodeNwkVersion", data.getNwkVersion() + " " + data.getNwkRevision()),
			        new ValueTO("nodeOSVersion", data.getOsVersion() + " " + data.getOsRevision()), });
		} catch (Exception e) {
			log.warn("Cannot store node version info", e);
		}
	}

	private static final String NODE_CHECKSUM = "checksum";
	private static final String NODE_PROGRAM = "program";
	private static final String NODE_LEFTRACK = "leftRack";
	private static final String NODE_CENTERRACK = "centerRack";
	private static final String NODE_RIGHTRACK = "rightRack";

	private static final String PLUG = "PLUG";
	private static final String PHASE = "PHASE";
	private static final String RPDU = "RPDU";
	private static final String SERVER = "SERVER";
	private static final String POWER_RACK = "POWER_RACK";

	private static final String PLUG_ID = "pId";
	private static final String RPDU_ID = "rId";
	private static final int DELTA_RPDU = 1;

	private static final String ULOCATION = "uLocation";
	private static final String UHEIGHT = "uHeight";
	private static final String RPDU_TYPE = "type";
	private static final String RPDU_PHOUTBAL = "phaseOutOfBalance";

	private static final String RACK_UHEIGHT = "height";
	private static final String RACK_USED = "uSpaceUsed";
	private static final String RACK_MAXUBLOCK = "maxUBlock";
	private static final String RACK_STATUS = "status";

	private static final String RACK_INSTRUMENTATION = "instrumentation";
	private static final int RACK_FI = 1;
	private static final int RACK_NFI = 0;

	private static final String RPDU_STATUS = "status";
	private static final int RPDU_STATUS_OK = 1;
	private static final int RPDU_STATUS_DELETED = 0;

	private static final String PHASE_ALERT_NAME = "Node not reporting";

	private class LabelRelationBundle implements Iterable<Pair<String, String>> {
		private LinkedList<Pair<String, String>> relations = new LinkedList<Pair<String, String>>();

		public void add(String label1, String label2) {
			relations.add(new Pair<String, String>(label1, label2));
		}

		@Override
		public Iterator<Pair<String, String>> iterator() {
			return Collections.unmodifiableCollection(relations).iterator();
		}
	}

	@Override
	// FIXME: method needs to be redesigned and simplified
	public void onPowerPackConf(TO<?> objID, SandcrawlerConfData data) {
		try {

			// Bundle for the objects to create
			EnvObjectBundle objects = new EnvObjectBundle();

			// Bundle for already existing objects
			TOBundle toBundle = new TOBundle();

			// Bundle for relations
			LabelRelationBundle relationBundle = new LabelRelationBundle();

			TOFactory toFactory = TOFactory.getInstance();

			// Build rack to rack TO map
			HashMap<Integer, TO<?>> rackIdToRack = createRackIdToTOMap(objID);
			// Build rack TO to rack configuration map
			HashMap<TO<?>, RackConfData> rackToToConf = createRackToConfMap(objID, data.getRackData(), rackIdToRack);
			for (TO<?> rackTO : rackToToConf.keySet()) {
				toBundle.add(toFactory.saveTO(rackTO), rackTO);
			}

			// map of plugs from the received config to RACKs
			Map<TO<?>, List<ServerInfo>> allServers = sortItemsByRacks(data.getPlugData(), rackIdToRack);

			// map of servers from the received config to RACKs
			Map<TO<?>, List<ServerInfo>> faceplates = sortItemsByRacks(data.getServerData(), rackIdToRack);
			for (Map.Entry<TO<?>, List<ServerInfo>> entry : faceplates.entrySet()) {
				List<ServerInfo> list = allServers.get(entry.getKey());
				if (list != null) {
					list.addAll(entry.getValue());
				} else {
					allServers.put(entry.getKey(), entry.getValue());
				}
			}

			// Calculate rack height, uSpaceAvailable and maxUBlock values
			for (Map.Entry<TO<?>, List<ServerInfo>> rackServers : allServers.entrySet()) {
				TO<?> rackTo = rackServers.getKey();

				// Rack instrumentation
				int instr = RACK_FI;
				for (ServerInfo faceplate : faceplates.get(rackTo)) {
					if (((ServerConfData) faceplate).getDemandPower() > 0D) {
						// There is at least one faceplate server
						instr = RACK_NFI;
						break;
					}
				}
				generic.setPropertyValue(rackTo, RACK_INSTRUMENTATION, instr);

				Integer rackHeight = rackToToConf.get(rackTo).getuHeight();
				List<ServerInfo> servers = rackServers.getValue();
				if (rackHeight == 0) {
					generic.setPropertyValue(rackTo, RACK_UHEIGHT, null);
					generic.setPropertyValue(rackTo, RACK_MAXUBLOCK, null);
					generic.setPropertyValue(rackTo, RACK_USED, null);
				} else if (servers.isEmpty()) {
					generic.setPropertyValue(rackTo, RACK_UHEIGHT, rackHeight);
					generic.setPropertyValue(rackTo, RACK_MAXUBLOCK, rackHeight);
					generic.setPropertyValue(rackTo, RACK_USED, 0);
				} else {
					generic.setPropertyValue(rackTo, RACK_UHEIGHT, rackHeight);
					// We need to obtain only distinct servers sorted by
					// position
					TreeSet<ServerInfo> sortedServers = new TreeSet<ServerInfo>(new Comparator<ServerInfo>() {
						public int compare(ServerInfo o1, ServerInfo o2) {
							return new Integer(o1.getuPos()).compareTo(o2.getuPos());
						}
					});
					sortedServers.addAll(servers);

					// Now iterate over servers and calculate necessary values
					// Return to array list to have direct access to elements
					servers = new ArrayList<ServerInfo>(sortedServers);
					int used = 0;
					int maxUBlock = servers.get(0).getuPos() - 1;
					for (int i = 0; i < servers.size(); i++) {
						ServerInfo curServer = servers.get(i);
						used += curServer.getuHeight();
						int tempUblock = 0;
						int serverTopPos = curServer.getuPos() + curServer.getuHeight();
						if (i == servers.size() - 1) {
							tempUblock = rackHeight - serverTopPos + 1;
						} else {
							tempUblock = servers.get(i + 1).getuPos() - serverTopPos;
						}
						if (tempUblock > maxUBlock) {
							maxUBlock = tempUblock;
						}
					}
					generic.setPropertyValue(rackTo, RACK_USED, used);
					generic.setPropertyValue(rackTo, RACK_MAXUBLOCK, maxUBlock);
				}
			}

			// PLUGS
			Map<Long, String> existingPlugsIdMap = new HashMap<Long, String>();
			// extract plugs info from the config
			Map<Long, PlugConfData> plugsFromConfigIdMap = new HashMap<Long, PlugConfData>();
			for (PlugConfData plug : data.getPlugData()) {
				plugsFromConfigIdMap.put(plug.getPlugId(), plug);
			}

			// if node was configured before, we need to get info from
			// environment about existing PLUG objects and merge them to the
			// sent config.

			// Get "pid" property for all the plugs in the ES
			Collection<TO<?>> currentPlugsInNode = getDescendants(rackIdToRack.values(), PLUG);
			if (!currentPlugsInNode.isEmpty()) {
				Collection<CollectionTO> plugsIds = generic.getPropertyValue(currentPlugsInNode,
				        new String[] { PLUG_ID });
				// put the PLUG objects to the map with pid as a key
				for (CollectionTO idVal : plugsIds) {
					String label = toFactory.saveTO(idVal.getObjId());
					existingPlugsIdMap.put((Long) idVal.getSinglePropValue(PLUG_ID).getValue(), label);
					toBundle.add(label, idVal.getObjId());
				}
			}
			// do merge
			AllValues<Long> val = PowerRackConfigHelper.intersectPlugsForNode(plugsFromConfigIdMap.keySet(),
			        existingPlugsIdMap.keySet());
			// we need to delete all the plugs, which don't exist in the
			// sent config
			Collection<Long> toListToRemove = val.getDifference().getSMinusF();
			// delete none existing plugs
			for (Long plugIdToRemove : toListToRemove) {
				generic.deleteObject(toBundle.get(existingPlugsIdMap.remove(plugIdToRemove)));
			}
			// this method has multiple purpose:
			// - creates new plugs, which are not in the existingPlugsIdMap
			// - removes relations with SERVER and PHASE objects
			// - adds newly created plug TOs to the existingPlugsIdMap
			createPlugs(plugsFromConfigIdMap, existingPlugsIdMap, toBundle, objects);

			// yahooo, we merged plugs!! now it's time to handle servers

			// SERVERS
			// Servers are uniquely identified within RACK only, we need to
			// perform all modifications for each RACK

			for (TO<?> rackTO : allServers.keySet()) {
				Map<Pair<Integer, Integer>, List<ServerInfo>> serversFromConfig = new HashMap<Pair<Integer, Integer>, List<ServerInfo>>();
				Map<Pair<Integer, Integer>, String> existingServers = new HashMap<Pair<Integer, Integer>, String>();
				// extract servers information for plug
				for (ServerInfo plug : allServers.get(rackTO)) {
					Pair<Integer, Integer> uPosAndHeight = new Pair<Integer, Integer>(plug.getuPos(), plug.getuHeight());
					List<ServerInfo> plugsForOneServer = serversFromConfig.get(uPosAndHeight);
					if (plugsForOneServer == null) {
						plugsForOneServer = new ArrayList<ServerInfo>();
						serversFromConfig.put(uPosAndHeight, plugsForOneServer);
					}
					plugsForOneServer.add(plug);
				}
				Collection<TO<?>> servers = generic.getChildren(rackTO, SERVER);
				if (servers != null && !servers.isEmpty()) {
					Collection<CollectionTO> serverProperties = generic.getPropertyValue(servers, new String[] {
					        ULOCATION, UHEIGHT });
					for (CollectionTO serverProp : serverProperties) {
						Pair<Integer, Integer> uPosAndHeight = new Pair<Integer, Integer>((Integer) serverProp
						        .getSinglePropValue(ULOCATION).getValue(), (Integer) serverProp.getSinglePropValue(
						        UHEIGHT).getValue());
						String label = toFactory.saveTO(serverProp.getObjId());
						existingServers.put(uPosAndHeight, label);
						toBundle.add(label, serverProp.getObjId());
					}
					AllValues<Pair<Integer, Integer>> intersectedServers = PowerRackConfigHelper
					        .intersectServersForRack(serversFromConfig.keySet(), existingServers.keySet());
					Collection<Pair<Integer, Integer>> serverIdsToRemove = intersectedServers.getDifference()
					        .getSMinusF();
					// delete not existing servers
					for (Pair<Integer, Integer> serverIdToRemove : serverIdsToRemove) {
						generic.deleteObject(toBundle.get(existingServers.remove(serverIdToRemove)));
					}
				}
				// Create servers
				createServers(serversFromConfig, existingServers, existingPlugsIdMap, toFactory.saveTO(rackTO),
				        objects, toBundle, relationBundle);
			}

			// done with the SERVERS here, process RPDUs and PHASEs
			for (TO<?> rackTO : allServers.keySet()) {
				Map<Integer, Map<Integer, List<PlugConfData>>> phasesToRpdu = new HashMap<Integer, Map<Integer, List<PlugConfData>>>();
				Map<Integer, String> existingRPDUs = new HashMap<Integer, String>();
				// this cycle groups plugs by phases and RPDUs.
				for (ServerInfo plugInfo : allServers.get(rackTO)) {
					if (plugInfo instanceof PlugConfData) {
						// double index (RPDU/PHASE is needed here to connect
						// the
						// created
						// plug to the right phase)
						PlugConfData plug = (PlugConfData) plugInfo;
						Integer rpduId = plug.getRpdu();
						Integer phaseId = plug.getPhase();
						// this map will contain all the phases with associated
						// PLUG
						// TOs
						Map<Integer, List<PlugConfData>> rpduMap = phasesToRpdu.get(rpduId);
						if (rpduMap == null) {
							rpduMap = new HashMap<Integer, List<PlugConfData>>();
							phasesToRpdu.put(rpduId, rpduMap);
						}

						// If delta need to create objects the following way
						// phase A(AB) -> A,B
						// phase B(BC) -> B,C
						// phase C(CA) -> C,A
						if (parseRpduType(rackToToConf.get(rackTO).getRpduType(), rpduId) == DELTA_RPDU) {
							if (!rpduMap.containsKey(phaseId)) {
								rpduMap.put(phaseId, new ArrayList<PlugConfData>());
							}
							int secondPhaseId = PhasesLabels.convertId(phaseId + 1);
							if (!rpduMap.containsKey(secondPhaseId)) {
								rpduMap.put(secondPhaseId, new ArrayList<PlugConfData>());
							}
						}

						// this List contains all the plugs for one phase
						List<PlugConfData> phaseList = rpduMap.get(phaseId);
						if (phaseList == null) {
							phaseList = new ArrayList<PlugConfData>();
							rpduMap.put(phaseId, phaseList);
						}
						phaseList.add(plug);
					}
				}
				Collection<TO<?>> rpdus = generic.getChildren(rackTO, RPDU);
				if (rpdus != null && !rpdus.isEmpty()) {
					Collection<CollectionTO> rpduProperties = generic.getPropertyValue(rpdus, new String[] { RPDU_ID });
					for (CollectionTO rpduProp : rpduProperties) {
						String label = toFactory.saveTO(rpduProp.getObjId());
						toBundle.add(label, rpduProp.getObjId());
						existingRPDUs.put((Integer) rpduProp.getSinglePropValue(RPDU_ID).getValue(), label);
					}

					AllValues<Integer> intersectedRPDUs = PowerRackConfigHelper.intersectRPDUsOrPhases(
					        phasesToRpdu.keySet(), existingRPDUs.keySet());

					Collection<Integer> rpdusToRemove = intersectedRPDUs.getDifference().getSMinusF();
					// delete none existing RPDUs
					for (Integer prduIdToRemove : rpdusToRemove) {
						generic.deleteObject(toBundle.get(existingRPDUs.remove(prduIdToRemove)));
					}
				}
				createRPDUs(phasesToRpdu, existingRPDUs, toFactory.saveTO(rackTO), rackToToConf.get(rackTO)
				        .getRpduType(), toBundle, objects, relationBundle);

				for (Map.Entry<Integer, String> entry : existingRPDUs.entrySet()) {
					Integer rpduId = entry.getKey();
					String rpdu = entry.getValue();
					TO<?> rpduTO = toBundle.get(rpdu);
					Map<Integer, String> existingPhases = new HashMap<Integer, String>();
					Map<Integer, List<PlugConfData>> phasesFromConfig = phasesToRpdu.get(rpduId);
					if (rpduTO != null) {
						Collection<TO<?>> children = generic.getChildren(rpduTO, PHASE);
						if (children != null && !children.isEmpty()) {
							Collection<CollectionTO> phaseProperties = generic.getPropertyValue(children,
							        new String[] { "name" });
							for (CollectionTO phaseProp : phaseProperties) {
								String phaseName = (String) phaseProp.getSinglePropValue("name").getValue();
								String label = toFactory.saveTO(phaseProp.getObjId());
								toBundle.add(label, phaseProp.getObjId());
								existingPhases.put(PowerRackConfBuilder.PhasesLabels.getPhaseIdByLabel(phaseName),
								        label);
							}
							AllValues<Integer> intersectedPhases = PowerRackConfigHelper.intersectRPDUsOrPhases(
							        phasesFromConfig.keySet(), existingPhases.keySet());

							Collection<Integer> phasesToRemove = intersectedPhases.getDifference().getSMinusF();
							// delete none existing Phases
							for (Integer phaseIdToRemove : phasesToRemove) {
								generic.deleteObject(toBundle.get(existingPhases.remove(phaseIdToRemove)));
							}
						}
					}
					createPhases(phasesFromConfig, existingPhases, rpdu, rackTO, objects, relationBundle);
					for (Integer phaseId : existingPhases.keySet()) {
						for (PlugConfData plug : phasesToRpdu.get(rpduId).get(phaseId)) {
							String phase = existingPhases.get(phaseId);
							if (phase != null) {
								// If the phase has been already resolved
								relationBundle.add(phase, existingPlugsIdMap.get(plug.getPlugId()));
							}
						}
					}
				}
			}

			// Create all new objects
			if (!objects.isEmpty()) {
				toBundle.addAll(generic.createObjects(objects));
			}

			// Create relations
			List<Relation> relations = new ArrayList<Relation>();
			for (Pair<String, String> relation : relationBundle) {
				relations.add(new Relation(toBundle.get(relation.getFirst()), toBundle.get(relation.getSecond())));
			}
			if (!relations.isEmpty()) {
				generic.setRelation(relations);
			}

			// Update available current and power
			for (TO<?> rackTO : allServers.keySet()) {
				updateVAthresholds(rackTO);
			}

			// Set checksum and program properties of the sandcrawler
			generic.setPropertyValue(objID, NODE_CHECKSUM, data.getChecksum());
			generic.setPropertyValue(objID, NODE_PROGRAM, data.getProgram());
		} catch (EnvException e) {
			alertService.raiseAlert(new Alert("Power Rack configuration failure", "WSN_CONFIGURATION", new Date(),
			        LocalizationUtils.getLocalizedString("alert_message_power_rack_configuration_failure",
			                objID.toString(), e.getMessage()), objID));
			log.error(e.getMessage(), e);
		}
	}

	private static final String DC = "DC";
	private static final String DERATEFACTOR = "derateFactor";
	private static final String BREAKER_POWER = "breakerPowerThreshold";
	private static final String MAX_POWER_TR = "maxPowerThreshold";

	private void updateVAthresholds(TO<?> rack) throws EnvException {

		TO<?> dc = generic.getRelatedObjects(rack, DC, false).iterator().next();
		Double derateFactor = generic.getPropertyValue(dc, DERATEFACTOR, Double.class);
		Double rackMPTh = 0d;
		Collection<TO<?>> rpdus = generic.getChildren(rack, RPDU);
		if (rpdus.isEmpty()) {
			rackMPTh = null;
		} else {
			for (TO<?> rpdu : rpdus) {

				if (generic.getPropertyValue(rpdu, RPDU_STATUS, Integer.class) != RPDU_STATUS_OK) {
					continue;
				}

				Double rpduMPTh = null;

				Collection<TO<?>> ps = generic.getChildren(rpdu, PHASE);

				if (ps.isEmpty()) {
					rackMPTh = null;
					continue;
				}

				Collection<CollectionTO> phases = generic.getPropertyValue(ps, new String[] { BREAKER_POWER });

				for (CollectionTO phase : phases) {
					rpduMPTh = updatePhaseThreshold(phase, BREAKER_POWER, MAX_POWER_TR, rpduMPTh, derateFactor);
				}
				rackMPTh = updateRpduThreshold(rpdu, MAX_POWER_TR, rpduMPTh, rackMPTh);
			}
		}
		generic.setPropertyValue(rack, MAX_POWER_TR, rackMPTh);
	}

	private Double updateRpduThreshold(TO<?> rpdu, String prop, Double val, Double rackVal) throws EnvException {
		generic.setPropertyValue(rpdu, prop, val);
		if (val == null || rackVal == null) {
			return null;
		}
		return rackVal + val;
	}

	private Double updatePhaseThreshold(CollectionTO phase, String breakerProp, String maxProp, Double rpduVal,
	        Double derateFactor) throws EnvException {

		Double breakerVal = (Double) phase.getSinglePropValue(breakerProp).getValue();
		if (breakerVal == null) {
			return rpduVal;
		}
		double maxVal = breakerVal * derateFactor / 100;
		generic.setPropertyValue(phase.getObjId(), maxProp, maxVal);
		if (rpduVal != null) {
			return rpduVal + maxVal;
		}
		return maxVal;
	}

	private Map<TO<?>, List<ServerInfo>> sortItemsByRacks(List<? extends ServerInfo> itemsFromConfig,
	        Map<Integer, TO<?>> rackIdToRack) throws EnvException {
		// split items by the rack property value 0 = left RACK, 1 = Center
		// RACK, 2 = Right RACK
		Map<TO<?>, List<ServerInfo>> racksToItems = new HashMap<TO<?>, List<ServerInfo>>();

		for (TO<?> rackTo : rackIdToRack.values()) {
			racksToItems.put(rackTo, new ArrayList<ServerInfo>());
		}

		for (ServerInfo item : itemsFromConfig) {
			int rackId = item.getRack();
			TO<?> rack = rackIdToRack.get(rackId);
			if (rack == null) {
				// Configuration, provided by Mapsense is incorrect. Cannot
				// proceed.
				throw new EnvException("Rack " + rackId + " is not configured in Mapsense.");
			}
			racksToItems.get(rack).add(item);
		}
		return racksToItems;
	}

	private void createPlugs(Map<Long, PlugConfData> plugsFromConfigIdMap, Map<Long, String> existingPlugsIdMap,
	        TOBundle toBundle, EnvObjectBundle objBundle) throws EnvException {
		for (Long plugId : plugsFromConfigIdMap.keySet()) {
			String to = existingPlugsIdMap.get(plugId);
			if (to != null) {
				// get rid of all parents of type SERVER, PHASE
				Collection<TO<?>> parentObjToBreakupWith = generic.getParents(toBundle.get(to), SERVER);
				parentObjToBreakupWith.addAll(generic.getParents(toBundle.get(to), PHASE));
				for (TO<?> parentTo : parentObjToBreakupWith) {
					generic.removeRelation(parentTo, toBundle.get(to));
				}
			} else {
				to = String.valueOf(plugId);
				objBundle.addObject(to, new EnvObject(PLUG, Arrays.asList(new ValueTO(PLUG_ID, plugId))));
				existingPlugsIdMap.put(plugId, to);
			}
		}
	}

	private void createServers(Map<Pair<Integer, Integer>, List<ServerInfo>> serversFromConfigIdMap,
	        Map<Pair<Integer, Integer>, String> existingServersIdMap, Map<Long, String> existingPlugsIdMap,
	        String rack, EnvObjectBundle objects, TOBundle tobundle, LabelRelationBundle relations) throws EnvException {

		String rackName = generic.getPropertyValue(tobundle.get(rack), "name", String.class);
		long timeStamp = System.currentTimeMillis();

		for (Entry<Pair<Integer, Integer>, List<ServerInfo>> entry : serversFromConfigIdMap.entrySet()) {
			Pair<Integer, Integer> serverId = entry.getKey();
			List<ServerInfo> serversInfo = entry.getValue();
			String newServer = existingServersIdMap.get(serverId);
			if (newServer == null) {
				newServer = rack + serverId.toString();
				ArrayList<ValueTO> values = new ArrayList<ValueTO>();
				values.add(new ValueTO("name", "*" + rackName + "_U" + serverId.getFirst()));
				values.add(new ValueTO(ULOCATION, serverId.getFirst()));
				values.add(new ValueTO(UHEIGHT, serverId.getSecond()));
				values.add(new ValueTO("lastResetTime", timeStamp));
				for (ServerInfo config : serversInfo) {
					if (config instanceof ServerConfData) {
						double demandPower = ((ServerConfData) config).getDemandPower();
						values.add(new ValueTO("fpDemandPower", demandPower));
						values.add(new ValueTO("fpApPower", demandPower));
						values.add(new ValueTO("maxHistApPower", demandPower));
					}
				}

				objects.addObject(newServer, new EnvObject(SERVER, values));
				relations.add(rack, newServer);
				existingServersIdMap.put(serverId, newServer);
			} else {
				generic.setPropertyValue(tobundle.get(newServer), "lastResetTime", timeStamp);
			}

			for (ServerInfo config : serversInfo) {
				if (config instanceof PlugConfData) {
					PlugConfData plugConfig = (PlugConfData) config;
					relations.add(newServer, existingPlugsIdMap.get(plugConfig.getPlugId()));
				} else if (config instanceof ServerConfData) {
					TO<?> serverTo = tobundle.get(newServer);
					if (serverTo != null) {
						double demandPower = ((ServerConfData) config).getDemandPower();
						generic.setAllPropertiesValues(
						        serverTo,
						        new ValueTO[] { new ValueTO("fpDemandPower", demandPower),
						                new ValueTO("fpApPower", demandPower),
						                new ValueTO("maxHistApPower", demandPower),
						                new ValueTO("lastResetTime", System.currentTimeMillis()) });
					}
				}
			}
		}
	}

	private void createRPDUs(Map<Integer, Map<Integer, List<PlugConfData>>> rpduFromConfigIdMap,
	        Map<Integer, String> existingRpduIdMap, String rack, int rackType, TOBundle toBundle,
	        EnvObjectBundle objects, LabelRelationBundle relations) throws EnvException {
		// get RACK name to use it in the RPDU name
		String rackName = generic.getPropertyValue(toBundle.get(rack), "name", String.class);

		for (Integer rpduId : rpduFromConfigIdMap.keySet()) {
			// As type is a bitmask for the whole rack
			int type = parseRpduType(rackType, rpduId);

			String rpdu = existingRpduIdMap.get(rpduId);
			if (rpdu == null) {
				rpdu = rack + "_" + rpduId;
				List<ValueTO> rpduValues = Arrays.asList(new ValueTO("name", "RPDU_" + (rpduId + 1) + "_" + rackName),
				        new ValueTO(RPDU_ID, rpduId), new ValueTO(RPDU_TYPE, type), new ValueTO(RPDU_PHOUTBAL, 0),
				        new ValueTO(RPDU_STATUS, RPDU_STATUS_OK));
				objects.addObject(rpdu, new EnvObject(RPDU, rpduValues));
				relations.add(rack, rpdu);
				existingRpduIdMap.put(rpduId, rpdu);
			} else {
				int exType = generic.getPropertyValue(toBundle.get(rpdu), RPDU_TYPE, Integer.class);
				if (type != exType) {
					// Update RPDU type
					generic.setPropertyValue(toBundle.get(rpdu), RPDU_TYPE, type);
				}
				if (generic.getPropertyValue(toBundle.get(rpdu), RPDU_STATUS, Integer.class) == null) {
					generic.setPropertyValue(toBundle.get(rpdu), RPDU_STATUS, RPDU_STATUS_OK);
				}
			}
		}
	}

	private void createPhases(Map<Integer, List<PlugConfData>> phaseFromConfigIdMap,
	        Map<Integer, String> existingPhaseIdMap, String rpdu, TO<?> rack, EnvObjectBundle objects,
	        LabelRelationBundle relations) throws EnvException {
		long timeStamp = System.currentTimeMillis();
		for (Entry<Integer, List<PlugConfData>> phaseEntry : phaseFromConfigIdMap.entrySet()) {
			Integer phaseId = phaseEntry.getKey();
			String phaseLabel = PowerRackConfBuilder.PhasesLabels.getPhaseLabelById(phaseId);

			if (phaseLabel == null) {
				List<String> plugIds = new LinkedList<String>();
				for (PlugConfData plugConf : phaseEntry.getValue()) {
					plugIds.add(CRC8Converter.codeToHr(plugConf.getPlugId()));
				}
				alertService.raiseAlert(new Alert("P3 SmartPlug - Unknown Phase Information", PHASE_ALERT_NAME,
				        new Date(), LocalizationUtils.getLocalizedString("alert_message_unknown_phase_information",
				                plugIds.toString()), rack));
				log.warn("Received incorrect phase id " + phaseId + ". Phase object won't be created.");
				// Phase not yet resolved. We don't create an object.
				continue;
			}

			String phase = existingPhaseIdMap.get(phaseId);
			if (phase == null) {
				phase = rack.toString() + rpdu + phaseLabel;
				List<ValueTO> phaseValues = Arrays.asList(new ValueTO("name", phaseLabel), new ValueTO("lastResetTime",
				        timeStamp), new ValueTO("avgCurrent", 0D), new ValueTO("maxCurrent", 0D));
				objects.addObject(phase, new EnvObject(PHASE, phaseValues));
				relations.add(rpdu, phase);
				existingPhaseIdMap.put(phaseId, phase);
			}
		}
	}

	private HashMap<Integer, TO<?>> createRackIdToTOMap(TO<?> objID) throws EnvException {
		// collect RACK TOs, node has to have central RACK, other RACKs are
		// optional
		TO<?> leftRack = generic.getPropertyValue(objID, NODE_LEFTRACK, TO.class);
		TO<?> centerRack = generic.getPropertyValue(objID, NODE_CENTERRACK, TO.class);
		TO<?> rightRack = generic.getPropertyValue(objID, NODE_RIGHTRACK, TO.class);

		HashMap<Integer, TO<?>> rackIdToRack = new HashMap<Integer, TO<?>>();
		putRack(rackIdToRack, PowerRackConfBuilder.LEFT_SERVER_ID, leftRack);
		putRack(rackIdToRack, PowerRackConfBuilder.CENTER_SERVER_ID, centerRack);
		putRack(rackIdToRack, PowerRackConfBuilder.RIGHT_SERVER_ID, rightRack);

		return rackIdToRack;
	}

	private HashMap<TO<?>, RackConfData> createRackToConfMap(TO<?> objID, Collection<RackConfData> conf,
	        HashMap<Integer, TO<?>> rackIdToRack) throws EnvException {
		HashMap<TO<?>, RackConfData> rackToToConf = new HashMap<TO<?>, RackConfData>();

		// Link ES objects to configuration
		for (RackConfData rackData : conf) {
			String rackProperty = RackConfData.RACK_PROP_MAP.get(rackData.getRack());
			if (rackProperty == null) {
				log.warn("Rack id in conf data should be in " + RackConfData.RACK_PROP_MAP.keySet() + " but it is "
				        + rackData.getRack() + ". Height for that rack won't be set.");
				continue;
			}

			TO<?> rackTo = rackIdToRack.get(rackData.getRack());
			if (rackTo == null) {
				if (rackData.getuHeight() > 0) {
					log.warn("Received rack height for " + rackProperty
					        + " but that rack isn't configured in Mapsense " + objID + ". Rack height won't be set.");
				}
			} else {
				rackToToConf.put(rackTo, rackData);
			}
		}
		return rackToToConf;
	}

	private void putRack(HashMap<Integer, TO<?>> rackIdToRack, int rack, TO<?> rackTo) {
		if (rackTo != null) {
			rackIdToRack.put(rack, rackTo);
		}
	}

	private Collection<TO<?>> getDescendants(Collection<TO<?>> parents, String descendant) {
		LinkedList<TO<?>> allDesc = new LinkedList<TO<?>>();
		for (TO<?> parent : parents) {
			if (parent != null) {
				allDesc.addAll(generic.getRelatedObjects(parent, descendant, true));
			}
		}
		return allDesc;
	}

	@Override
	public void onPlugData(TO<?> objId, PlugData data) {
		Collection<TO<?>> plugs = generic.getObjects(PLUG, new ValueTO[] { new ValueTO(PLUG_ID, data.getpId()) });

		if (plugs.isEmpty()) {
			log.warn("Unable to find plug for pId=" + data.getpId() + ". Power data won't be stored.");
			return;
		}

		if (plugs.size() > 1) {
			for (TO<?> plug : plugs) {
				TO<?> server;
				try {
					server = generic.getParents(plug, SERVER).iterator().next();
				} catch (ObjectNotFoundException e) {
					log.error(e.getLocalizedMessage(), e);
					return;
				}
				alertService.raiseAlert(new Alert("Duplicate P3 SmartPlug detected", "WSN_CONFIGURATION", new Date(),
				        LocalizationUtils.getLocalizedString("alert_message_duplicate_p3_smartplug_detected",
				                CRC8Converter.codeToHr(data.getpId())), server));
			}
			log.warn("Duplicate plugs for pId=" + data.getpId() + " found. Power data won't be stored.");
			return;
		}

		TO<?> plug = plugs.iterator().next();
		TO<?> rack = generic.getRelatedObjects(plug, POWER_RACK, false).iterator().next();
		try {
			if (isRackDisabled(rack)) {
				// We don't store data in this case
				return;
			}
			generic.setAllPropertiesValues(plug,
			        new ValueTO[] { new ValueTO("avgCurrent", data.getAvgCurrent(), data.getTimestamp()),
			                new ValueTO("maxCurrent", data.getMaxCurrent(), data.getTimestamp()),
			                new ValueTO("demandPower", data.getDemandPower(), data.getTimestamp()) });
		} catch (EnvException e) {
			log.warn("Unable to set plug power data", e);
		}
	}

	@Override
	public void onPhaseData(TO<?> objId, PhaseData data) {

		// 0 - left, 1 - center, 2 - right
		String rackProperty = RackConfData.RACK_PROP_MAP.get(data.getRack());
		if (rackProperty == null) {
			log.warn("Rack id in phase data should be in " + RackConfData.RACK_PROP_MAP.keySet() + " but it is "
			        + data.getRack() + ". Phase data won't be set");
		}

		try {
			TO<?> rack = generic.getPropertyValue(objId, rackProperty, TO.class);
			if (rack == null) {
				log.warn("Power rack object is null for node: " + objId + ", PhaseData: " + data
				        + ". Phase data won't be set.");
				return;
			}

			if (isRackDisabled(rack)) {
				// We don't store data in this case
				return;
			}

			TO<?> rpdu = findObjectByProperty(generic.getChildren(rack, RPDU), RPDU_ID, data.getRpdu());
			if (rpdu == null) {
				log.warn("RPDU object is not found for node: " + objId + ", PhaseData: " + data
				        + ". Phase data won't be set");
				return;
			}

			TO<?> phase = findObjectByProperty(generic.getChildren(rpdu, PHASE), "name",
			        PowerRackConfBuilder.PhasesLabels.getPhaseLabelById(data.getPhase()));
			if (phase == null) {
				log.warn("PHASE object is not found for node: " + objId + ", PhaseData: " + data
				        + ". Phase data won't be set");
				return;
			}

			// Set values
			setPhaseValue(phase, "voltage", data.getVoltage(), data.getTimestamp());
			setPhaseValue(phase, data.getAvgCurrentProp(), data.getAvgCurrent(), data.getTimestamp());
			setPhaseValue(phase, data.getMaxCurrentProp(), data.getMaxCurrent(), data.getTimestamp());
			setPhaseValue(phase, "demandPower", data.getDemandPower(), data.getTimestamp());
		} catch (Exception e) {
			log.warn("Unable to set phase data", e);
		}
	}

	private boolean isRackDisabled(TO<?> rack) throws EnvException {
		Integer status = generic.getPropertyValue(rack, RACK_STATUS, Integer.class);
		if (status == 2) {
			if (log.isDebugEnabled()) {
				log.debug("Received power data packet for rack " + rack + " which is disabled. Data won't be set");
			}
			return true;
		}
		return false;
	}

	private <T> TO<?> findObjectByProperty(Collection<TO<?>> objects, String propName, T srcValue) {
		if (objects == null || objects.isEmpty() || srcValue == null) {
			return null;
		}
		Collection<CollectionTO> collTos = generic.getPropertyValue(objects, new String[] { propName });
		for (CollectionTO collTO : collTos) {
			@SuppressWarnings("unchecked")
			T propValue = (T) collTO.getSinglePropValue(propName).getValue();
			if (srcValue.equals(propValue)) {
				return collTO.getObjId();
			}
		}
		return null;
	}

	private void setPhaseValue(TO<?> phase, String propName, Double value, long timestamp) throws Exception {
		if (value != null) {
			generic.setAllPropertiesValues(phase, new ValueTO[] { new ValueTO(propName, value, timestamp) });
		}
	}

	private static int parseRpduType(int rackType, int rpduId) {
		return (rackType >> rpduId) & 1;
	}

	@Override
	public String getSensorsMapping() {
		return null;
	}

	public Environment getGeneric() {
		return generic;
	}

	@EJB(beanName="GenericEnvironment")
	public void setGeneric(Environment generic) {
		this.generic = generic;
	}

	public AlertService getAlertService() {
		return alertService;
	}

	@EJB
	public void setAlertService(AlertService alertService) {
		this.alertService = alertService;
	}

	@Override
	public void onDuoConf(TO<?> objId, DuoConfData data) {
		try {
			TO<?> powerRack = generic.getPropertyValue(objId, NODE_CENTERRACK, TO.class);
			String rackName = generic.getPropertyValue(powerRack, "name", String.class);

			HashMap<Integer, TO<?>> existingRpdusMap = new HashMap<Integer, TO<?>>();

			Collection<TO<?>> existingRpdus = generic.getChildren(powerRack, RPDU);
			if (!existingRpdus.isEmpty()) {
				Collection<CollectionTO> rpdusIds = generic.getPropertyValue(existingRpdus, new String[] { RPDU_ID });
				for (CollectionTO collTo : rpdusIds) {
					existingRpdusMap.put((Integer) collTo.getSinglePropValue(RPDU_ID).getValue(), collTo.getObjId());
				}
			}

			SetsIntersector<Integer> rpduIntersector = new SetsIntersector<Integer>();
			AllValues<Integer> intResult = rpduIntersector.doAllJob(data.getRpdus().keySet(),
			        existingRpdusMap.keySet(), new Comparator<Integer>() {
				        @Override
				        public int compare(Integer o1, Integer o2) {
					        return o1.compareTo(o2);
				        }
			        });

			// [6671], [6805]. We delete disconnected rpdus in the 'lazy' style
			for (int rpdu : intResult.getDifference().getSMinusF()) {
				generic.setPropertyValue(existingRpdusMap.remove(rpdu), RPDU_STATUS, RPDU_STATUS_DELETED);
			}
			// Create new rpdus
			long lastResetTime = System.currentTimeMillis();
			for (int rpduId : intResult.getDifference().getFMinusS()) {
				TO<?> rpdu = generic.createObject(RPDU, new ValueTO[] {
				        new ValueTO("name", "RPDU_" + (rpduId + 1) + "_" + rackName), new ValueTO(RPDU_ID, rpduId),
				        new ValueTO(RPDU_TYPE, data.getRpdus().get(rpduId)), new ValueTO(RPDU_PHOUTBAL, 0),
				        new ValueTO(RPDU_STATUS, RPDU_STATUS_OK) });
				generic.setRelation(powerRack, rpdu);

				// Create all phases
				for (PhasesLabels label : PhasesLabels.values()) {
					TO<?> phase = generic.createObject(PHASE, new ValueTO[] { new ValueTO("name", label.name()),
					        new ValueTO("lastResetTime", lastResetTime) });
					generic.setRelation(rpdu, phase);
				}
			}

			// Update existing rpdus
			for (int rpdu : intResult.getIntersection()) {
				generic.setAllPropertiesValues(existingRpdusMap.get(rpdu), new ValueTO[] {
				        new ValueTO(RPDU_TYPE, data.getRpdus().get(rpdu)), new ValueTO(RPDU_STATUS, RPDU_STATUS_OK) });
			}

			// Update thresholds
			updateVAthresholds(powerRack);

			// Update node checksum and program
			generic.setPropertyValue(objId, NODE_CHECKSUM, data.getChecksum());
			generic.setPropertyValue(objId, NODE_PROGRAM, data.getProgram());
		} catch (EnvException e) {
			alertService.raiseAlert(new Alert("Duo configuration failure", "WSN_CONFIGURATION", new Date(),
			        LocalizationUtils.getLocalizedString("alert_message_duo_configuration_failure", objId.toString(),
			                e.getMessage()), objId));
			log.error(e.getMessage(), e);
		}
	}
}
