package com.synapsense.impl.alerting.mapping;

import java.util.List;

import org.apache.log4j.Logger;

import com.synapsense.dto.AlertType;
import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;
import com.synapsense.impl.alerting.Utils;
import com.synapsense.service.Environment;
import com.synapsense.util.CollectionUtils;
import com.synapsense.util.CollectionUtils.Functor;

public class AlertTypeToProperties implements Functor<List<ValueTO>, AlertType> {
	private final static Logger logger = Logger.getLogger(AlertTypeToProperties.class);

	private Environment env;

	public AlertTypeToProperties(Environment env) {
	    this.env = env;
    }
	
	@Override
	public List<ValueTO> apply(AlertType alertType) {
		final TO<?> priorityDO = Utils.findAlertPriority(env, alertType.getPriority());
		if (priorityDO == null) {
			throw new MappingException("Alert priority :" + alertType.getPriority() + " does not exist");
		}

		List<ValueTO> values = CollectionUtils.newArrayList(9);
		values.add(new ValueTO("name", alertType.getName()));
		values.add(new ValueTO("displayName", alertType.getDisplayName()));
		values.add(new ValueTO("description", alertType.getDescription()));
		values.add(new ValueTO("active", alertType.isActive() ? 1 : 0));
		values.add(new ValueTO("autoAck", alertType.isAutoAcknowledge() ? 1 : 0));
		values.add(new ValueTO("autoDismiss", alertType.isAutoDismiss() ? 1 : 0));
		values.add(new ValueTO("autoDismissAllowed", alertType.isAutoDismissAllowed() ? 1 : 0));
		values.add(new ValueTO("priority", priorityDO));

		List<TO<?>> templates = CollectionUtils.newArrayList(1);
		for (com.synapsense.dto.MessageTemplate mt : alertType.getMessageTemplates()) {
			if (!env.exists(mt.getRef())) {
				if (logger.isDebugEnabled()) {
					logger.warn("Message template refers to unexisting object " + mt.getRef());
				}
			} else {
				templates.add(mt.getRef());
			}
		}
		values.add(new ValueTO("templates", templates));

		return values;
	}

}
