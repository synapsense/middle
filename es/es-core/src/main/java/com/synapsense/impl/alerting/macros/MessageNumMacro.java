package com.synapsense.impl.alerting.macros;

import java.util.regex.Pattern;

public class MessageNumMacro extends ContextPropertyMacro {

	public static final String MSG_NUM = "MESSAGE_NUMBER";

	public MessageNumMacro() {
		super(Pattern.compile("\\$(?i)msg_num\\$"), MSG_NUM);
	}
}
