package com.synapsense.impl.activitylog;

import java.util.Collection;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;

import com.synapsense.service.impl.activitylog.ActivityLogService;
import com.synapsense.service.impl.activitylog.ActivityRecord;
import com.synapsense.service.impl.activitylog.ActivityRecordFilter;

@Stateless(name="ActivityLogService")
@Remote(ActivityLogService.class)
public class ActivityLogServiceImpl implements ActivityLogService {

	@EJB(beanName = "ActivityLogDAO")
	ActivityLogService impl;

	@Override
	public List<String> getActivityActions() {
		return impl.getActivityActions();
	}

	@Override
	public List<String> getActivityModules() {
		return impl.getActivityModules();
	}

	@Override
	@Interceptors(ActivityLogInterceptor.class)
	public ActivityRecord getRecord(int ID) {
		return impl.getRecord(ID);
	}

	@SuppressWarnings("deprecation")
	@Override
	public void log(String userName, String moduleName, String actionName, String description) {
		impl.log(userName, moduleName, actionName, description);
	}

	@Override
	public void addRecord(ActivityRecord record) {
		impl.addRecord(record);
	}

	@Override
	public void addRecords(Collection<ActivityRecord> recordsList) {
		impl.addRecords(recordsList);
	}

	@Override
	public void deleteRecord(int ID) {
		impl.deleteRecord(ID);
	}

	@Override
	public void deleteRecord(Collection<Integer> IDs) {
		impl.deleteRecord(IDs);
	}

	@Override
	public void updateRecord(ActivityRecord record) {
		impl.updateRecord(record);
	}

	@Override
	public void updateRecord(Collection<ActivityRecord> recordsList) {
		impl.updateRecord(recordsList);
	}

	@Override
	@Interceptors(ActivityLogInterceptor.class)
	public Collection<ActivityRecord> getRecords(ActivityRecordFilter filter) {
		return impl.getRecords(filter);
	}

}
