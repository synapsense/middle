package com.synapsense.impl.alerting.notification;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;

import com.synapsense.dto.MessageType;
import com.synapsense.dto.TO;
import com.synapsense.impl.alerting.Utils;
import com.synapsense.service.Environment;
import com.synapsense.service.UserManagementService;
import com.synapsense.util.CollectionUtils;

public class EnvDestinationProvider implements DestinationProvider {
	private final static Logger logger = Logger.getLogger(EnvDestinationProvider.class);

	private final Environment env;
	private final UserManagementService userService;

	private final String alertTypeName;
	private final String tierName;
	private final MessageType messageType;

	public EnvDestinationProvider(Environment env, UserManagementService userService, String alertTypeName,
	        String tierName, MessageType messageType) {
		super();
		this.env = env;
		this.userService = userService;
		this.alertTypeName = alertTypeName;
		this.tierName = tierName;
		this.messageType = messageType;
	}

	@Override
	public List<Destination> getDestinations() {
		final List<Destination> result = CollectionUtils.newList();
		if (logger.isDebugEnabled())
			logger.debug("Getting destinations for alert type :" + alertTypeName + ", tier : " + tierName
			        + ", message type :" + messageType.name());

		final TO<?> alertType = Utils.findAlertTypeByName(env, alertTypeName);
		if (alertType == null) {
			if (logger.isDebugEnabled()) {
				logger.debug("Failed to get destinations. Alert type : " + alertTypeName + " does not exist");
			}
			return Collections.emptyList();
		}

		Collection<String> destinations = Utils.findDestinations(env, alertType, tierName, messageType);

		if (logger.isDebugEnabled())
			logger.debug("Retrieved destinations : " + destinations);

		for (final String destAsString : destinations) {
			logger.debug("Resolving destination : " + destAsString);
			final Destination dest = Destinations.newDestination(destAsString, userService);
			logger.debug("Destination : " + destAsString + " resolved as : " + dest.toString());
			result.add(dest);
		}
		return result;
	}

}
