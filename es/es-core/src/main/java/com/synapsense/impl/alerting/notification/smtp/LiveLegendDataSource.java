package com.synapsense.impl.alerting.notification.smtp;

import com.synapsense.service.Environment;

/**
 * @author : shabanov
 */
public final class LiveLegendDataSource extends AbstractImageDataSource {

	private DataClass dataClass;
	private String unitSystem;

	public LiveLegendDataSource(Environment environment, DataClass dataClass, String unitSystem) {
		super(environment);
        if (dataClass == null) throw  new IllegalArgumentException("Supplied 'dataClass' is null");
        if (unitSystem == null) throw  new IllegalArgumentException("Supplied 'unitSystem' is null");
        this.dataClass = dataClass;
		this.unitSystem = unitSystem;
	}

	@Override
	public String getFileName() {
		return "legend_" + dataClass.getCode() + ".png";
	}

	@Override
	public String getType() {
		return "image/png";
	}

	@Override
	public String getDescription() {
		return dataClass.getDisplayName();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		LiveLegendDataSource that = (LiveLegendDataSource) o;

		if (!dataClass.equals(that.dataClass))
			return false;
		if (!unitSystem.equals(that.unitSystem))
			return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result = dataClass.hashCode();
		result = 31 * result + unitSystem.hashCode();
		return result;
	}

	@Override
	protected String getFile() {
		return "getCurrentLegendImage?dataclass=" + dataClass.getCode() + "&unitSystem=" + unitSystem;
	}

}
