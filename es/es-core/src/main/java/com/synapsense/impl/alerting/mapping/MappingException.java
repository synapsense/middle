package com.synapsense.impl.alerting.mapping;

import com.synapsense.exception.AlertServiceSystemException;

public class MappingException extends AlertServiceSystemException {

	/**
     * 
     */
	private static final long serialVersionUID = -4317563022129361422L;

	public MappingException(String message) {
		super(message);
	}

	public MappingException(String message, Throwable e) {
		super(message, e);
	}

}
