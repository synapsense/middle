package com.synapsense.impl.diagnostic;

import java.io.File;

import org.apache.log4j.Logger;

public class CommonUtils {
	private static final Logger LOG = Logger.getLogger(CommonUtils.class);
	final private static String FILE_STORAGE = "com.synapsense.filestorage";
	private static final String TEMPORARY_FOLDER_NAME = "ssp_temp";

	public static String getTemporaryFolderPath() {
		String temporaryFolderPath = TEMPORARY_FOLDER_NAME;
		String storageFileName = System.getProperty(FILE_STORAGE);
		if (storageFileName == null || storageFileName.isEmpty()) {
			LOG.error("Property [" + FILE_STORAGE + "] is not set");
		} else {
			temporaryFolderPath = storageFileName + File.separator + TEMPORARY_FOLDER_NAME;
		}
		return temporaryFolderPath;
	}

	public static void makeTemporaryFolderIfNotExists() {
		File tempFolder = new File(getTemporaryFolderPath());
		if (!tempFolder.exists()) {
			tempFolder.mkdir();
		}
	}
}
