package com.synapsense.impl.reporting.commands;

public class ReportingCommandException extends RuntimeException {

	private static final long serialVersionUID = 237222873540225398L;

	public ReportingCommandException() {
	}

	public ReportingCommandException(String message, Throwable cause) {
		super(message, cause);
	}

	public ReportingCommandException(String message) {
		super(message);
	}

	public ReportingCommandException(Throwable cause) {
		super(cause);
	}
}
