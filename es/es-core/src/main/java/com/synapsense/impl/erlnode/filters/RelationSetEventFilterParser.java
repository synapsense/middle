package com.synapsense.impl.erlnode.filters;

import org.apache.log4j.Logger;

import com.ericsson.otp.erlang.OtpErlangObject;
import com.ericsson.otp.erlang.OtpErlangTuple;
import com.synapsense.impl.erlnode.commands.RegisterProcessorCommand;
import com.synapsense.service.nsvc.filters.RelationSetEventFilter;
import com.synapsense.service.nsvc.filters.TOMatcher;

//-record(relation_set_event_filter, {
//pmatcher = #regex{} :: #regex{} | #to{},
//cmatcher = #regex{} :: #regex{} | #to{}}).
class RelationSetEventFilterParser {

	private static final Logger log =
		Logger.getLogger(RelationSetEventFilter.class);


	public RelationSetEventFilter parse(OtpErlangTuple filterDef) {
		TOMatcher pmatcher = null, cmatcher = null;

		OtpErlangObject tmp = filterDef.elementAt(1);
		if (tmp != null && !RegisterProcessorCommand.undefined.equals(tmp))
			pmatcher = TOMatcherParser.parse(tmp);

		tmp = filterDef.elementAt(2);
		if (tmp != null && !RegisterProcessorCommand.undefined.equals(tmp))
			cmatcher = TOMatcherParser.parse(tmp);

		if (pmatcher == null && cmatcher == null)
			throw new IllegalArgumentException(
					"Either one of or both parent and child matchers must be specified");

		log.debug("returning RelationSetEventFilter");
		return new RelationSetEventFilter(pmatcher, cmatcher);
	}

}
