package com.synapsense.impl.performer.audio;

import org.apache.log4j.Logger;

public class AlertSwitchObject {

	private Integer alertID;
	private SignalSender sender;
	private String destination;
	private boolean audioIntervention = false;
	private boolean lightIntervention = false;
	private final static Logger logger = Logger.getLogger(StatusChangeListener.class);

	public AlertSwitchObject(Integer alertID, SignalSender sender, String destination) {
		this.alertID = alertID;
		this.sender = sender;
		this.destination = destination;

	}

	public Integer getAlertID() {
		return alertID;
	}

	public String getDestination() {
		return destination;
	}

	public void setAudioIntervention(boolean audioIntervention) {
		this.audioIntervention = audioIntervention;
	}

	public void setLightIntervention(boolean lightIntervention) {
		this.lightIntervention = lightIntervention;
	}

	public void intervent() {
		if (audioIntervention)
			sender.sendTurnOffAudioRequest();
		logger.debug("send audio intervention signal to" + destination);
		if (lightIntervention)
			sender.sendTurnOffLightRequest();
		logger.debug("send light intervention signal to" + destination);
	}

	public boolean isIntervention() {
		if (audioIntervention || lightIntervention) {
			return true;
		}
		return false;
	}
}
