package com.synapsense.impl.transport.rmi;

/**
 * Interface for event channels fail strategies.
 * 
 * @author Oleg Stepanov
 * 
 */
public interface EventChannelFailStrategy {

	/**
	 * Check if channel has failed according to this strategy.
	 * 
	 * @param eventChannel
	 *            channel to judge upon.
	 * @return true if the channel should be considered as failed, false
	 *         otherwise.
	 */
	boolean isFailed(EventChannel eventChannel);

}
