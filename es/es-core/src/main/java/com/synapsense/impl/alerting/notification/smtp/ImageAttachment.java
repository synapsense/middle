package com.synapsense.impl.alerting.notification.smtp;

import org.apache.commons.mail.ByteArrayDataSource;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.Date;

import static com.synapsense.impl.alerting.AlertConstants.NOTIFICATIONS.IMAGE_SERVER_CONNECTION_TIMEOUT;

/**
 * Image type attachment
 * 
 * @author : shabanov
 */
public class ImageAttachment implements Attachment {

	private final static Logger logger = Logger.getLogger(ImageAttachment.class);

	// datasource
	private ImageDataSource ds;

	private final String name;

	protected ImageAttachment(String name, ImageDataSource ds) {
		this.name = name;
		this.ds = ds;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public String getDescription() {
		return ds.getDescription();
	}

	@Override
	public InlinedAttachment inline(HtmlEmail email) {
		byte[] image = ds.getImage();
		if (image == null)
			return EMPTY;

		ByteArrayDataSource imageAsByteArray = null;
		try {
			imageAsByteArray = new ByteArrayDataSource(image, ds.getType());
		} catch (IOException e) {
			return EMPTY;
		}

		try {
			String contentID = email.embed(imageAsByteArray, ds.getFileName());
			return new InlinedAttachment("<img src=\"cid:" + contentID + "\">", new Date());
		} catch (EmailException e) {
			if (logger.isDebugEnabled()) {
				return new InlinedAttachment("Debug mode is on. Error has occurred : " + e.getLocalizedMessage());
			} else {
				return EMPTY;
			}
		}
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		ImageAttachment that = (ImageAttachment) o;

		if (!ds.equals(that.ds))
			return false;

		return true;
	}

	@Override
	public int hashCode() {
		return ds.hashCode();
	}

}
