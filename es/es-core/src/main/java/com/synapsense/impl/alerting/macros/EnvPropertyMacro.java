package com.synapsense.impl.alerting.macros;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.exception.EnvException;

public class EnvPropertyMacro extends AbstractPatternMacro {

	private final static Logger log = Logger.getLogger(EnvPropertyMacro.class);

	private static final Pattern pattern = Pattern.compile("\\$(\\w+:\\d+)((?:\\.\\w+){1,})(?:\\{([ \\w]+)\\})?\\$");
	private static final Pattern propertyPattern = Pattern.compile("\\.(\\w+)");

	@Override
	public String expandIn(String input, MacroContext ctx) {
		if (input == null)
			return null;
		if (ctx.env == null)
			throw new IllegalArgumentException("MacroContext.env must be not null");
		return super.expandIn(input, ctx);
	}

	@Override
	protected String expand(Matcher m, MacroContext ctx) {
		TO<?> tempHost;
		try {
			tempHost = TOFactory.getInstance().loadTO(m.group(1));
		} catch (Exception e) {
			log.warn("Macro " + m.group() + ": cannot load TO " + m.group(1) + ", returning default expansion", e);
			return Matcher.quoteReplacement(Macro.DEFAULT_EXPANSION);
		}

		Matcher propMatcher = propertyPattern.matcher(m.group(2));
		Object replacement = Macro.DEFAULT_EXPANSION;
		while (propMatcher.find()) {
			final String property = propMatcher.group(1);
			try {
				
				replacement = ctx.env.getPropertyValue(tempHost, property, Object.class);
				if (replacement == null) {
					// just return N/A if prop is null
					return Matcher.quoteReplacement(Macro.DEFAULT_EXPANSION);
				}
				
				if (TO.class.isAssignableFrom(replacement.getClass())) {
					tempHost = (TO<?>) replacement;
				} else {
					// break the loop because of wrong reference
					if (!propMatcher.hitEnd())
						log.warn("Macro " + m.group() + ": " + tempHost.toString() + "." + property
						        + " is not a TO reference, dereferencing stopped");
					break;
				}

			} catch (final EnvException e) {
				log.warn("Macro " + m.group() + ": " + tempHost.toString() + "." + property
				        + ": cannot read value, dereferencing stopped", e);
				break;
			}
		}

		// if dimension was specified, return result as a conversion macro
		if (m.group(3) != null) {
			return Matcher.quoteReplacement("$conv(" + replacement.toString() + "," + m.group(3) + ")$");
		}

		return Matcher.quoteReplacement(replacement.toString());
	}

	@Override
	protected Pattern getPattern() {
		return pattern;
	}

}
