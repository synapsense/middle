package com.synapsense.impl.alerting.notification.smtp;

import com.synapsense.dto.TO;
import com.synapsense.exception.EnvException;
import com.synapsense.service.Environment;
import org.apache.log4j.Logger;

/**
 * @author : shabanov
 */
public final class LiveImageDataSource extends AbstractImageDataSource {
	private final static Logger logger = Logger.getLogger(LiveImageDataSource.class);

	private TO object;
	private ImageLayer layer;
	private DataClass dataClass;

	public LiveImageDataSource(Environment environment, TO object, ImageLayer layer, DataClass dataClass) {
		super(environment);
		this.object = object;
		this.layer = layer;
		this.dataClass = dataClass;
	}

	@Override
	public String getFileName() {
		return object.getID() + "-" + dataClass.getCode() + "-" + layer.getCode() + ".png";
	}

	@Override
	public String getType() {
		return "image/png";
	}

	@Override
	public String getDescription() {
		String location;
		try {
			location = env.getPropertyValue(object, "name", String.class);
		} catch (EnvException e) {
			if (logger.isDebugEnabled())
				logger.debug("Failed to get name of object " + object, e);
			location = object.toString();
		}

		return location + ", " + layer.getDisplayName() + ", " + dataClass.getDisplayName();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		LiveImageDataSource that = (LiveImageDataSource) o;

		if (!dataClass.equals(that.dataClass))
			return false;
		if (!layer.equals(that.layer))
			return false;
		if (!object.equals(that.object))
			return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result = object.hashCode();
		result = 31 * result + layer.hashCode();
		result = 31 * result + dataClass.hashCode();
		return result;
	}

	@Override
	protected String getFile() {
		if (logger.isDebugEnabled()) {
			logger.debug("Getting the latest LiveImaging image of " + object + ", " + dataClass + ", on " + layer);
		}

		return "generateLatestLiteImage?objId=" + object.toString() + "&dataclass=" + dataClass.getCode() + "&layer="
		        + layer.getCode();
	}
}
