package com.synapsense.impl.devmanager.dissectors;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;

import com.synapsense.dto.TO;
import com.synapsense.plugin.messages.DuoDataBuilder.FieldConstants;
import com.synapsense.plugin.messages.DuoDataType;
import com.synapsense.plugin.messages.GenericMessage;
import com.synapsense.plugin.messages.GenericMessage.GenericMessageTypes;
import com.synapsense.plugin.messages.PowerRackConfBuilder.PhasesLabels;
import com.synapsense.service.impl.messages.base.DeltaPhaseData;
import com.synapsense.service.impl.messages.base.PhaseData;
import com.synapsense.util.algo.Pair;
import com.synapsense.util.algo.UnaryFunctor;

public class DuoDataDissector implements MessageDissector {

	private static final Logger log = Logger.getLogger(DuoDataDissector.class);

	private static final int RACK_ID = 1;

	private final UnaryFunctor<Pair<TO<?>, PhaseData>> functor;

	public DuoDataDissector(UnaryFunctor<Pair<TO<?>, PhaseData>> functor) {
		this.functor = functor;
	}

	@Override
	public boolean tasteMessage(GenericMessage msg) {
		return msg.getType() == GenericMessageTypes.DUO_DATA;
	}

	@Override
	public void process(TO<?> objId, GenericMessage msg) {
		long timestamp = (Long) msg.get(FieldConstants.TIMESTAMP.name());

		@SuppressWarnings("unchecked")
		HashMap<Integer, Map<DuoDataType, Double>> duoDataMap = (HashMap<Integer, Map<DuoDataType, Double>>) msg
		        .get(FieldConstants.RPDUS.name());

		for (Entry<Integer, Map<DuoDataType, Double>> entryRpduData : duoDataMap.entrySet()) {
			Map<DuoDataType, Double> rpduData = entryRpduData.getValue();
			if (rpduData == null) {
				continue;
			}

			PhaseDataFactory phF;
			if (rpduData.get(DuoDataType.AVG_CURRENT_PHASE_AB) != null
			        || rpduData.get(DuoDataType.AVG_CURRENT_PHASE_BC) != null
			        || rpduData.get(DuoDataType.AVG_CURRENT_PHASE_CA) != null) {
				// Wow!!! This rpdu is capable of sending line currents!

				phF = new PhaseDataFactory() {
					public PhaseData createPhaseData(int rack, int rpdu, int phase, Double voltage, long timestamp) {
						return new DeltaPhaseData(rack, rpdu, phase, voltage, timestamp);
					}
				};

				// Dissect phase A
				sendToProcess(
				        objId,
				        getPhaseData(entryRpduData.getKey(), PhasesLabels.getPhaseIdByLabel("A"),
				                DuoDataType.AVG_CURRENT_PHASE_AB, DuoDataType.MAX_CURRENT_PHASE_AB,
				                DuoDataType.DEMAND_POWER_A, DuoDataType.VOLTAGE_A_N, DuoDataType.VOLTAGE_A_B, rpduData,
				                timestamp, phF));
				// Dissect phase B
				sendToProcess(
				        objId,
				        getPhaseData(entryRpduData.getKey(), PhasesLabels.getPhaseIdByLabel("B"),
				                DuoDataType.AVG_CURRENT_PHASE_BC, DuoDataType.MAX_CURRENT_PHASE_BC,
				                DuoDataType.DEMAND_POWER_B, DuoDataType.VOLTAGE_B_N, DuoDataType.VOLTAGE_B_C, rpduData,
				                timestamp, phF));
				// Dissect phase C
				sendToProcess(
				        objId,
				        getPhaseData(entryRpduData.getKey(), PhasesLabels.getPhaseIdByLabel("C"),
				                DuoDataType.AVG_CURRENT_PHASE_CA, DuoDataType.MAX_CURRENT_PHASE_CA,
				                DuoDataType.DEMAND_POWER_C, DuoDataType.VOLTAGE_C_N, DuoDataType.VOLTAGE_C_A, rpduData,
				                timestamp, phF));
			} else {
				phF = new PhaseDataFactory() {
					public PhaseData createPhaseData(int rack, int rpdu, int phase, Double voltage, long timestamp) {
						return new PhaseData(rack, rpdu, phase, voltage, timestamp);
					}
				};
				// Dissect phase A
				sendToProcess(
				        objId,
				        getPhaseData(entryRpduData.getKey(), PhasesLabels.getPhaseIdByLabel("A"),
				                DuoDataType.AVG_CURRENT_PHASE_A, DuoDataType.MAX_CURRENT_A, DuoDataType.DEMAND_POWER_A,
				                DuoDataType.VOLTAGE_A_N, DuoDataType.VOLTAGE_A_B, rpduData, timestamp, phF));
				// Dissect phase B
				sendToProcess(
				        objId,
				        getPhaseData(entryRpduData.getKey(), PhasesLabels.getPhaseIdByLabel("B"),
				                DuoDataType.AVG_CURRENT_PHASE_B, DuoDataType.MAX_CURRENT_B, DuoDataType.DEMAND_POWER_B,
				                DuoDataType.VOLTAGE_B_N, DuoDataType.VOLTAGE_B_C, rpduData, timestamp, phF));
				// Dissect phase C
				sendToProcess(
				        objId,
				        getPhaseData(entryRpduData.getKey(), PhasesLabels.getPhaseIdByLabel("C"),
				                DuoDataType.AVG_CURRENT_PHASE_C, DuoDataType.MAX_CURRENT_C, DuoDataType.DEMAND_POWER_C,
				                DuoDataType.VOLTAGE_C_N, DuoDataType.VOLTAGE_C_A, rpduData, timestamp, phF));

			}
		}
	}

	private void sendToProcess(TO<?> objId, PhaseData phaseData) {
		if (log.isDebugEnabled()) {
			log.debug("Processing phase data " + phaseData);
		}
		functor.process(new Pair<TO<?>, PhaseData>(objId, phaseData));
	}

	private PhaseData getPhaseData(int rpduId, int phaseId, DuoDataType acType, DuoDataType mcType, DuoDataType dpType,
	        DuoDataType vnType, DuoDataType ppType, Map<DuoDataType, Double> data, long timestamp, PhaseDataFactory phF) {

		Double voltage = data.get(vnType);
		if (voltage == null) {
			voltage = data.get(ppType);
		}

		PhaseData phaseData = phF.createPhaseData(RACK_ID, rpduId, phaseId, voltage, timestamp);
		phaseData.setAvgCurrent(data.get(acType));
		phaseData.setMaxCurrent(data.get(mcType));
		phaseData.setDemandPower(data.get(dpType));
		return phaseData;
	}

	private interface PhaseDataFactory {
		PhaseData createPhaseData(int rack, int rpdu, int phase, Double voltage, long timestamp);
	}
}
