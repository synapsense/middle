
package com.synapsense.impl.erlnode.commands;

import java.util.Map;

import com.ericsson.otp.erlang.OtpErlangObject;
import com.ericsson.otp.erlang.OtpErlangBinary;
import com.ericsson.otp.erlang.OtpErlangTuple;
import com.synapsense.impl.erlnode.ExceptionTypes;
import com.synapsense.impl.erlnode.utils.ToConverter;
import com.synapsense.impl.erlnode.utils.StringConverter;
import com.synapsense.dto.TO;
import com.synapsense.exception.ObjectNotFoundException;

/**
 * @author stikhon
 * 
 */
public class CreateObjectCommand1 extends Command {

	public CreateObjectCommand1(Map<String, Object> state) {
		super(state);
	}

	@Override
	public OtpErlangObject do_command(OtpErlangTuple command) throws Exception {

		OtpErlangObject typeName = command.elementAt(1);
		if (typeName == null || !(typeName instanceof OtpErlangBinary)) {
			return makeException(ExceptionTypes.ILLEGAL_ARGUMENTS, null);
		}
		String typeNameValue = StringConverter.utf8BinaryToString(typeName);
		try {
			TO<?> to = getEnv().createObject(typeNameValue);
			return makeOkResult(ToConverter.serializeTo(to));
		} catch (ObjectNotFoundException e) {
			return makeException(ExceptionTypes.NO_OBJECT_TYPE, e.getMessage());
		}

	}

}
