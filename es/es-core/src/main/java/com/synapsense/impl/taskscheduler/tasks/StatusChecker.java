package com.synapsense.impl.taskscheduler.tasks;

import com.synapsense.service.Environment;

public interface StatusChecker {
	void check(Environment env);
}
