package com.synapsense.impl.reporting.mail;

/**
 * @author : shabanov
 */
public interface Provider<T> {
    T get();
}
