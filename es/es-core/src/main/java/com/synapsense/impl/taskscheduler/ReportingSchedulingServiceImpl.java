package com.synapsense.impl.taskscheduler;

import com.synapsense.cdi.MBean;
import com.synapsense.impl.reporting.app.ReportingManagementServiceImpl;
import com.synapsense.service.reporting.dto.ReportTaskInfo;
import com.synapsense.service.reporting.exception.SchedulingReportingException;
import com.synapsense.service.reporting.management.ReportingSchedulingService;
import com.synapsense.util.CollectionUtils;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.CronExpression;
import org.quartz.SchedulerException;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.*;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

@Startup
@Singleton(name = "ReportingSchedulingService")
@DependsOn("QuartzService")
@Remote(ReportingSchedulingService.class)
@ConcurrencyManagement(ConcurrencyManagementType.BEAN)
@MBean("com.synapsense:type=ReportingSchedulingService")
public class ReportingSchedulingServiceImpl implements ReportingSchedulingService, ReportingSchedulingServiceImplMBean {

	private static final Log logger = LogFactory.getLog(ReportingSchedulingServiceImpl.class);

	private QuartzTaskSchedulerImplMBean scheduler;

	@Override
	public void validateReportTask(ReportTaskInfo taskInfo) throws SchedulingReportingException {
		if (taskInfo.getCron() == null) {
            throw new SchedulingReportingException("Failed to schedule task " + taskInfo.getName() + " as its cron expression is null");
        }

        if (!scheduler.isCronValid(taskInfo.getCron())) {
            throw new SchedulingReportingException("Failed to schedule task " + taskInfo.getName() + " as it has invalid cron expression : " + taskInfo.getCron());
        }
	}
	
	@Override
	public void scheduleReportTask(ReportTaskInfo taskInfo) throws SchedulingReportingException {
		if (logger.isDebugEnabled()) {
			logger.debug("Scheduling task for report template " + taskInfo.getReportTemplateName() + " and cron "
			        + taskInfo.getCron());
		}

		LinkedHashMap<Class<?>, Object> params = new LinkedHashMap<>();
		params.put(ReportTaskInfo.class, taskInfo);

        String taskName = taskInfo.getName();
		try {
			scheduler.addEjbTask(taskName, taskInfo.getReportTemplateName(), taskInfo.getCron(),
			        "java:global/es-ear/es-core/ReportingManagementService",
			        ReportingManagementServiceImpl.class.getName(), "runComplexReportTask", params);
		} catch (TaskSchedulerException e) {
			logger.warn("Cannot schedule task : " + taskName, e);
			throw new SchedulingReportingException("Cannot schedule task : " + taskName, e);
		}
	}

	@Override
	public Collection<ReportTaskInfo> getScheduledTasks(String reportTemplateName) throws SchedulingReportingException {
		Collection<ReportTaskInfo> tasks = CollectionUtils.newList();
		try {
			Collection<String> taskNames = scheduler.getScheduledTasksByGroup(reportTemplateName);
			for (String taskName : taskNames) {
				Map<Class<?>, Object> arguments = scheduler.getEjbTaskParams(taskName, reportTemplateName);
				ReportTaskInfo taskInfo = (ReportTaskInfo) arguments.get(ReportTaskInfo.class);
				if (taskInfo != null)
					tasks.add(taskInfo);
			}
		} catch (TaskSchedulerException e) {
			throw new SchedulingReportingException("Failed to get scheduled tasks for template: " + reportTemplateName, e);
		}

		return tasks;
	}

	@Override
	public boolean removeScheduledTask(String taskName, String reportTemplateName) throws SchedulingReportingException {
		try {
			return scheduler.removeTask(reportTemplateName, taskName);
		} catch (TaskSchedulerException e) {
			throw new SchedulingReportingException("Failed to remove task", e);
		}
	}

	@Override
	public void removeScheduledTasks(String reportTemplateName) throws SchedulingReportingException {
		try {
			scheduler.removeGroup(reportTemplateName);
		} catch (TaskSchedulerException e) {
			throw new SchedulingReportingException("Unable to remove scheduled tasks for template " + reportTemplateName);
		}
	}

	@Override
	@PostConstruct
	public void create() {
		logger.info("Creating reporting scheduling service...");
		start();
	}

	@Override
	public void start() {
		logger.info("Started reporting scheduling service");
	}

	@Override
	@PreDestroy
	public void stop() throws SchedulerException {
		logger.info("Stopping reporting scheduling service...");
		destroy();
	}

	@Override
	public void destroy() {
		logger.info("Destroyed reporting scheduling service");
	}

	@EJB
	public void setScheduler(QuartzTaskSchedulerImplMBean scheduler) {
		this.scheduler = scheduler;
	}
}
