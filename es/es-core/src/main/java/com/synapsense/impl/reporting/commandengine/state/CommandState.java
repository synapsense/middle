package com.synapsense.impl.reporting.commandengine.state;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.impl.reporting.commandengine.CommandContext;

public abstract class CommandState {

	public static final CommandState READY = new CommandReadyState();

	public static final CommandState RUNNING = new CommandRunningState();

	public static final CommandState CANCELLING = new CommandCancellingState();

	public static final CommandState COMPLETED = new CommandFiniteState("COMPLETED");

	public static final CommandState CANCELLED = new CommandFiniteState("CANCELLED");

	public static final CommandState SUSPENDED = new CommandSuspendedState();

	public static final CommandState ERROR = new CommandRemovableState("ERROR");

	public static final String TOKEN_START = "start";

	public static final String TOKEN_CANCEL = "cancel";

	public static final String TOKEN_FAIL = "fail";

	public static final String TOKEN_COMPLETE = "complete";

	public static final String TOKEN_SUSPEND = "suspend";

	public static final String TOKEN_RESUME = "resume";

	public static final Log log = LogFactory.getLog(CommandState.class);

	private String name;

	protected CommandState(String stateName) {
		this.name = stateName;
	}

	public String getName() {
		return name;
	}

	public abstract void start(CommandContext context);

	public abstract void cancel(CommandContext context);

	public abstract void suspend(CommandContext context);

	public abstract void resume(CommandContext context);

	public abstract void complete(CommandContext context);

	public abstract void fail(CommandContext context);

	public abstract boolean removable();

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final CommandState other = (CommandState) obj;
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		return true;
	}

	public String toString() {
		return name;
	}

	protected static final void throwIllegalStateException(CommandState state, CommandContext context, String token) {
		debugStateMethodCall(state, context, token);
		throw new IllegalStateException("Illegal token '" + token + "' on state " + state.getName() + " on command "
		        + context.getCommand().getName());
	}

	protected static final void debugStateMethodCall(CommandState state, CommandContext context, String token) {
		if (log.isDebugEnabled()) {
			log.debug("State " + state.getName() + " received token '" + token + "' on command '"
			        + context.getCommand().getName() + "'");
		}
	}
}
