package com.synapsense.impl.reporting.dao;

import javax.ejb.ApplicationException;

@ApplicationException(rollback = true)
public class ResourceDAOReportingException extends DAOReportingRuntimeException {

	private static final long serialVersionUID = 6340025844623362211L;

	public ResourceDAOReportingException() {
	}

	public ResourceDAOReportingException(String message, Throwable cause) {
		super(message, cause);
	}

	public ResourceDAOReportingException(String message) {
		super(message);
	}

	public ResourceDAOReportingException(Throwable cause) {
		super(cause);
	}
}
