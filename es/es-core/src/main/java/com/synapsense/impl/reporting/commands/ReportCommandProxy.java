package com.synapsense.impl.reporting.commands;

import com.synapsense.impl.reporting.commandengine.CommandProxy;
import com.synapsense.service.reporting.dto.ReportTaskInfo;

public interface ReportCommandProxy extends CommandProxy {

	ReportTaskInfo getTaskInfo();
}
