package com.synapsense.impl.alerting.notification.smtp;

import java.net.URL;

/**
 * URL data source of the image
 * 
 * @author : shabanov
 */
public interface ImageDataSource {
	//URL getURL();

	/**
	 * Returns image as byte array or null
	 * 
	 * @return
	 */
	byte[] getImage();

	String getFileName();

	String getType();

	String getDescription();
}
