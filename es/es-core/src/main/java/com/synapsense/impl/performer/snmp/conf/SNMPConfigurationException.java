package com.synapsense.impl.performer.snmp.conf;

import com.synapsense.impl.performer.snmp.SNMPPerformerException;

public class SNMPConfigurationException extends SNMPPerformerException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7583914971239653937L;

	public SNMPConfigurationException(String desc) {
		super(desc);
	}

	public SNMPConfigurationException(String desc, Throwable cause) {
		super(desc, cause);
	}
}
