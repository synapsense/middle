/**
 * 
 */
package com.synapsense.impl.auditsvc.auditor;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import com.synapsense.service.impl.audit.AuditException;
import com.synapsense.util.LocalizationUtils;

/**
 * @author aborisov
 * 
 */
public class MySQLAuditor extends BasicAuditor {

	private static final long serialVersionUID = -742771059237614299L;

	private String datasourceName;

	private final String PROBE_QUERY = "select 1 from dual;";

	public MySQLAuditor() {
		super();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsense.impl.auditsvc.auditor.BasicAuditor#audit()
	 */
	@Override
	public void doAudit() throws AuditException {
		if (logger.isInfoEnabled()) {
			logger.debug("Starting audit for [" + datasourceName + "]");
		}

		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection c = null;
		try {
			DataSource ds = (DataSource) new InitialContext().lookup(datasourceName);
			c = ds.getConnection();
			ps = c.prepareStatement(PROBE_QUERY);
			rs = ps.executeQuery();
		} catch (NamingException e) {
			logger.info("[" + datasourceName + "] unavailiable");
			if (logger.isDebugEnabled()) {
				logger.debug("Cannot obtain datasource", e);
			}
			throw new AuditException(LocalizationUtils.getLocalizedString("audit_message_unable_to_load_driver"));
		} catch (SQLException e) {
			logger.info("[" + datasourceName + "] unavailiable");
			throw new AuditException(e.getLocalizedMessage(), e);
		} catch (Exception e) {
			logger.info("[" + datasourceName + "] unavailiable");
			throw new AuditException(e.getLocalizedMessage(), e);
		} finally {
			try {
				if (rs != null)
					rs.close();
			} catch (SQLException e) {
				// We have no ideas what should we do, just ignore it.
			}
			try {
				if (ps != null)
					ps.close();
			} catch (SQLException e) {
				// We have no ideas what should we do, just ignore it.
			}
			try {
				if (c != null && !c.isClosed())
					c.close();
			} catch (SQLException e) {
				// We have no ideas what should we do, just ignore it.
			}
		}

		logger.debug("Finished for [" + datasourceName + "]");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.synapsense.impl.auditsvc.auditor.BasicAuditor#configurableProps()
	 */
	@Override
	public String[] configurableProps() {
		return new String[] { "datasourceName" };
	}

	public String getDatasourceName() {
		return datasourceName;
	}

	public void setDatasourceName(String driverName) {
		this.datasourceName = driverName;
	}

	@Override
	public String getConfiguration() {
		return configuration = datasourceName;
	}

}
