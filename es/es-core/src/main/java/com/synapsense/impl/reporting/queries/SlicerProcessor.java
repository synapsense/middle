package com.synapsense.impl.reporting.queries;

import java.util.List;

import com.synapsense.service.reporting.olap.*;
import org.apache.log4j.Logger;

import com.synapsense.impl.reporting.tables.ObjectTypeTable;
import com.synapsense.impl.reporting.tables.ReportingTable;
import com.synapsense.service.reporting.olap.custom.Cube;

/**
 * The <code>SlicerProcessor</code> class processes slicer 
 * 
 * @author Grigory Ryabov
 */
public class SlicerProcessor implements PlanProcessor {
	
	private static final Logger log = Logger.getLogger(SlicerProcessor.class);
	
	private AxisProcessor axisProcessor;

	protected ReportingTable slicerObjectTable = new ObjectTypeTable();
	
	public SlicerProcessor(AxisProcessor axisProcessor) {
		super();
		this.axisProcessor = axisProcessor;
	}

	@Override
	public void processSet(AxisSet axisSet) {
		axisProcessor.processSet(axisSet);
	}

	@Override
	public void processSet(CrossJoin crossJoin) {
		AxisSet[] sets = crossJoin.getSets();
		//Processing OBJECT Dimensionality at first
		for (AxisSet set : sets) {
			if (set.getDimensionalities().get(0).equals(Cube.OBJECT)){
				set.buildQuery(this);
			}
		}
		//After that Processing other Dimensionalities
		for (AxisSet set : sets) {
			if (!set.getDimensionalities().get(0).equals(Cube.OBJECT)){
				set.buildQuery(this);
			}
		}
	}

	@Override
	public void processSet(TupleSet tupleSet) {
		if (tupleSet.getDimensionalities().iterator().next().equals(Cube.OBJECT)){
			for (Tuple tuple : tupleSet.getTuples()){
				tuple.buildQuery(this);
			}
		} else {
			axisProcessor.processSet(tupleSet);
		}
	}

	@Override
	public void processSet(Tuple tuple) {
		if (tuple.getDimensionalities().contains(Cube.OBJECT)){
			tuple.buildQuery(this);
		}
		for (int index = 0; index < tuple.getTupleSize();index++){
			if (!tuple.itemByIndex(index).getHierarchy().equals(Cube.OBJECT)){
				axisProcessor.processSet(tuple);
			}
		}
	}

	@Override
	public void processSet(SimpleMember member) {
		if (member.getHierarchy().equals(Cube.OBJECT)){
			this.slicerObjectTable.add(member);
		} else {
			axisProcessor.processSet(member);
		}
	}

    @Override
    public void processSet(Union union) {
        axisProcessor.processSet(union);
    }

    @Override
	public void processSet(AllMemberSet allMemberSet) {
		List<Hierarchy> hierarchies = allMemberSet.getDimensionalities();
		if (hierarchies.iterator().next().equals(Cube.OBJECT)){
			for (Tuple tuple : allMemberSet.getTuples()){
				tuple.buildQuery(this);
			}
		} else {
			this.axisProcessor.processSet(allMemberSet);
		}
	}

	@Override
	public void processSet(RangeSet rangeSet) {
		axisProcessor.processSet(rangeSet);
	}

	@Override
	public void processSet(FilterableAllMemberSet filterableAllMemberSet) {
		List<Hierarchy> hierarchies = filterableAllMemberSet.getDimensionalities();
		if (hierarchies.iterator().next().equals(Cube.OBJECT)){
			for (Tuple tuple : filterableAllMemberSet.getTuples()){
				tuple.buildQuery(this);
			}
		} else {
			this.axisProcessor.processSet(filterableAllMemberSet);
		}
	}

	public ReportingTable getSlicerObjectTable() {
		return this.slicerObjectTable;
	}

	@Override
	public void processSet(RangeMember rangeMember) {
		axisProcessor.processSet(rangeMember);
	}

	@Override
	public void processSet(Member member) {
		axisProcessor.processSet(member);
	}
}
