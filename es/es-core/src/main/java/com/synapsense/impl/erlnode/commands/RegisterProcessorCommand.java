
package com.synapsense.impl.erlnode.commands;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.log4j.Logger;

import com.ericsson.otp.erlang.OtpErlangAtom;
import com.ericsson.otp.erlang.OtpErlangList;
import com.ericsson.otp.erlang.OtpMbox;
import com.ericsson.otp.erlang.OtpErlangObject;
import com.ericsson.otp.erlang.OtpErlangPid;
import com.ericsson.otp.erlang.OtpErlangTuple;

import com.synapsense.impl.erlnode.ExceptionTypes;
import com.synapsense.impl.erlnode.filters.EventFiltersFactory;
import com.synapsense.impl.erlnode.utils.ToConverter;
import com.synapsense.impl.erlnode.utils.ValueTOConverter;

import com.synapsense.dto.ChangedValueTO;
import com.synapsense.dto.ValueTO;

import com.synapsense.service.nsvc.EventProcessor;
import com.synapsense.service.nsvc.NotificationService;
import com.synapsense.service.nsvc.events.*;

public class RegisterProcessorCommand extends Command {
	
	private static final Logger log =
		Logger.getLogger(RegisterProcessorCommand.class);
	
	public static final Map<OtpErlangPid, EventProcessor> processors =
		new HashMap<OtpErlangPid, EventProcessor>();
	
	public static final OtpErlangAtom undefined =
		new OtpErlangAtom("undefined");

	public RegisterProcessorCommand(Map<String, Object> state) {
		super(state);
	}

	public synchronized void deregisterProcessorsForNode(String nodeName) {
		Iterator<OtpErlangPid> it = processors.keySet().iterator();
		while(it.hasNext()) {
			OtpErlangPid p = it.next();
			if(p.node().equals(nodeName)) {
				log.info("Deregistering processor for pid " + p);
				getNsvc().deregisterProcessor(processors.get(p));
				it.remove();
			}
		}
	}

	@Override
	public synchronized OtpErlangObject do_command(OtpErlangTuple command) throws Exception {
		OtpErlangObject tmp = command.elementAt(1);
		if (tmp == null || !(tmp instanceof OtpErlangPid)) {
			return makeException(ExceptionTypes.ILLEGAL_ARGUMENTS, null);
		}
		OtpErlangPid pid = (OtpErlangPid) tmp;

		//create filter
		EventProcessor filter = null;
		OtpErlangObject filterDef = command.elementAt(2);
		if(filterDef!=null && !undefined.equals(filterDef))
			filter = EventFiltersFactory.createFilter(filterDef);
		//create processor
		EventProcessor processor = processors.get(pid);
		if(processor == null) {
			MarshallingProcessor mp =
				new MarshallingProcessor(pid, (OtpMbox)globalState.get("mbox"));

			processor =
				new DispatchingProcessor()
				.putOcep(mp)
				.putOdep(mp)
				.putRsep(mp)
				.putRrep(mp)
				.putPcep(mp)
				.putCcep(mp)
				.putCsep(mp);
			processors.put(pid, processor);
			if(log.isDebugEnabled())
				log.debug("New event marshaller created for "+pid);
		} else {
			if(log.isDebugEnabled())
				log.debug("Replacing event marshaller filter for "+pid);
		}
		
		getNsvc().registerProcessor(processor, filter);
		
		return makeOkResult(null);
	}
	
	@Override
	public void flushState() {
		processors.clear();
	}

}

class MarshallingProcessor implements
	ObjectCreatedEventProcessor, ObjectDeletedEventProcessor,
	RelationSetEventProcessor, RelationRemovedEventProcessor,
	PropertiesChangedEventProcessor,
	ConfigurationStartedEventProcessor,
	ConfigurationCompleteEventProcessor {
	
	private final static Logger log =
		Logger.getLogger(MarshallingProcessor.class);
	
	private final OtpErlangPid pid;
	private final OtpMbox out;
	
	private final static OtpErlangAtom object_created_event =
		new OtpErlangAtom("object_created_event");
	private final static OtpErlangAtom object_deleted_event =
		new OtpErlangAtom("object_deleted_event");
	private final static OtpErlangAtom relation_set_event =
		new OtpErlangAtom("relation_set_event");
	private final static OtpErlangAtom relation_removed_event =
		new OtpErlangAtom("relation_removed_event");
	private final static OtpErlangAtom values_changed_event =
		new OtpErlangAtom("values_changed_event");
	public static final OtpErlangAtom disconnected_event =
		new OtpErlangAtom("disconnected_event");
	public static final OtpErlangAtom reconnected_event =
		new OtpErlangAtom("reconnected_event");
	public static final OtpErlangAtom configuration_started_event =
		new OtpErlangAtom("configuration_started_event");
	public static final OtpErlangAtom configuration_complete_event =
		new OtpErlangAtom("configuration_complete_event");


	public MarshallingProcessor(OtpErlangPid pid, OtpMbox out) {
		this.pid = pid;
		this.out = out;
	}

	@Override
	public ObjectCreatedEvent process(ObjectCreatedEvent event) {
		log.debug("Marshalling event: "+event);
		//-record(object_created_event, {obj_id :: #to{}, initial_values :: list(#value_to{})}).
		OtpErlangTuple evtBody = new OtpErlangTuple(new OtpErlangObject[]{
				object_created_event,
				ToConverter.serializeTo(event.getObjectTO()),
				ValueTOConverter.serializeValueTOList(Arrays.asList(event.getValues()))});
		
		//serialize
		write(evtBody);
		return null;
	}

	@Override
	public ObjectDeletedEvent process(ObjectDeletedEvent event) {
		log.debug("Marshalling event: "+event);
		//-record(object_deleted_event, {obj_id :: #to{}}).
		OtpErlangTuple evtBody = new OtpErlangTuple(new OtpErlangObject[]{
				object_deleted_event,
				ToConverter.serializeTo(event.getObjectTO())});
		
		//serialize
		write(evtBody);
		return null;
	}

	@Override
	public RelationSetEvent process(RelationSetEvent event) {
		log.debug("Marshalling event: "+event);
		//-record(relation_set_event, {parent_id = #to{}, child_id = #to{}}).
		OtpErlangTuple evtBody = new OtpErlangTuple(new OtpErlangObject[]{
				relation_set_event,
				new OtpErlangTuple(new OtpErlangObject[]{
						ToConverter.serializeTo(event.getParentTO()),
						ToConverter.serializeTo(event.getChildTO())})});
		
		//serialize
		write(evtBody);
		return null;
	}

	@Override
	public RelationRemovedEvent process(RelationRemovedEvent event) {
		log.debug("Marshalling event: "+event);
		//-record(relation_set_event, {parent_id = #to{}, child_id = #to{}}).
		OtpErlangTuple evtBody = new OtpErlangTuple(new OtpErlangObject[]{
				relation_removed_event,
				new OtpErlangTuple(new OtpErlangObject[]{
						ToConverter.serializeTo(event.getParentTO()),
						ToConverter.serializeTo(event.getChildTO())})});
		
		//serialize
		write(evtBody);
		return null;
	}

	@Override
	public PropertiesChangedEvent process(PropertiesChangedEvent event) {
		log.debug("Marshalling event: "+event);
		//-record(values_changed_event, {obj_id :: #to{}, updates :: list({#value_to{}, #value_to{}})}).
		//make the value_tos list
		ChangedValueTO[] receivedValues = event.getChangedValues();
		OtpErlangObject[] changedValues =
			new OtpErlangObject[receivedValues.length];
		for(int i=0; i<changedValues.length; i++) {
			ValueTO oldVal =
				new ValueTO(receivedValues[i].getPropertyName(),
						receivedValues[i].getOldValue());
			changedValues[i] = new OtpErlangTuple(new OtpErlangObject[]{
					ValueTOConverter.serializeValueTO(oldVal),//old value
					ValueTOConverter.serializeValueTO(receivedValues[i])});//new value
		}
		OtpErlangTuple evtBody = new OtpErlangTuple(new OtpErlangObject[]{
				values_changed_event,
				new OtpErlangTuple(new OtpErlangObject[]{
						ToConverter.serializeTo(event.getObjectTO()),
						new OtpErlangList(changedValues)})});
		
		//serialize
		write(evtBody);
		return null;
	}

	@Override
	public ConfigurationStartedEvent process(ConfigurationStartedEvent event) {
		log.debug("Marshalling event: "+event);
		write(configuration_started_event);
		return null;
	}

	@Override
	public ConfigurationCompleteEvent process(ConfigurationCompleteEvent event) {
		log.debug("Marshalling event: "+event);
		write(configuration_complete_event);
		return null;
	}

	private void write(OtpErlangObject data) {
		log.debug("Sending event '" + data + "' to '" + pid + "'");
		out.send(pid, data);
	}

	public boolean equals(MarshallingProcessor mp) {
		return pid.node().equals(mp.pid.node());
	}

	public int hashCode() {
		return pid.node().hashCode();
	}
}
