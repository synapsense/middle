package com.synapsense.impl.reporting.utils;

import java.io.File;
import java.io.FileFilter;
import java.io.FilenameFilter;

public final class FileUtils {

	public static final String LINE_SEPARATOR = System.getProperty("line.separator");

	public static String getExtensiion(File file) {
		return file.getName().substring(file.getName().lastIndexOf('.') + 1);
	}

	public static String getSimpleName(File file) {
		return file.getName().substring(0, file.getName().lastIndexOf('.'));
	}

	public static boolean deleteDirectory(File path) {
		if (!path.exists()) {
			return true;
		}
		if (path.isDirectory()) {
			File[] files = path.listFiles();
			for (File file : files) {
				if (file.isDirectory()) {
					deleteDirectory(file);
				} else {
					file.delete();
				}
			}
		}
		return path.delete();
	}

	public static boolean deleteDirectory(String path) {
		return deleteDirectory(new File(path));
	}

	public static boolean checkOrCreateDirectory(File path) {
		if (path.exists()) {
			if (path.isDirectory()) {
				return true;
			}
			return false;
		}
		return path.mkdirs();
	}

	public static boolean checkOrCreateDirectory(String path) {
		return checkOrCreateDirectory(new File(path));
	}

	public static File[] getChildDirectories(File root) {
		return root.listFiles(new FileFilter() {

			@Override
			public boolean accept(File path) {
				return path.isDirectory();
			}
		});
	}

	public static File[] getChildrenByExtension(File root, final String ext) {
		return root.listFiles(new FilenameFilter() {

			@Override
			public boolean accept(File dir, String name) {
				return getExtensiion(new File(dir, name)).equals(ext);
			}
		});
	}

	public static boolean checkFileExists(File dir, String fileName) {
		File file = new File(dir, fileName);
		if (file.exists() && file.isFile()) {
			return true;
		}
		return false;
	}

	public static File createFile(File parent, String name, String ext) {
		return new File(parent, name + "." + ext);
	}

	private FileUtils() {
	}
}
