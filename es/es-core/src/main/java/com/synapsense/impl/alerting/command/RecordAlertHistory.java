package com.synapsense.impl.alerting.command;

import com.synapsense.commandprocessor.api.Command;
import com.synapsense.dto.Alert;
import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.EnvException;
import com.synapsense.impl.alerting.mapping.AlertToHistory;
import com.synapsense.impl.alerting.mapping.PropertiesToAlert;
import com.synapsense.service.Environment;
import org.apache.log4j.Logger;

import java.util.Collection;

public class RecordAlertHistory implements Command {
    private final static Logger logger = Logger.getLogger(RecordAlertHistory.class);

    private TO<?> historyHolder;
	private Environment env;
	private Collection<TO<?>> alerts;

	public RecordAlertHistory(Environment env, Collection<TO<?>> alerts, TO<?> historyHolder) {
		this.env = env;
		this.alerts = alerts;
		this.historyHolder = historyHolder;
	}

	@Override
	public void execute() {
		for (TO<?> alert : alerts) {
			if (!env.exists(alert)) {
				continue;
			}

			try {
				saveAlertHistory(historyHolder, alert);
			} catch (EnvException e) {
                logger.warn("Failed to record alert " + alert, e);
			}
		}
	}

	private void saveAlertHistory(TO<?> historyHolder, TO<?> alertID) throws EnvException {
		Collection<ValueTO> alertValues = env.getAllPropertiesValues(alertID);
		Alert alert = new PropertiesToAlert(env, alertID).apply(alertValues);
		alertValues = new AlertToHistory(env).apply(alert);
		env.setAllPropertiesValues(historyHolder, alertValues.toArray(new ValueTO[alertValues.size()]));
	}
}
