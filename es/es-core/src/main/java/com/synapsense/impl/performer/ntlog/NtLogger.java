package com.synapsense.impl.performer.ntlog;

import org.apache.log4j.Hierarchy;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.apache.log4j.nt.NTEventLogAppender;

public class NtLogger extends Logger {

	private String source;

	public NtLogger(String name, String source) {
		super(name);
		this.source = source;
		this.repository = new Hierarchy(this);
		this.level = Level.INFO;
		NTEventLogAppender eventLogAppender = new NTEventLogAppender();
		eventLogAppender.setSource(this.source);
		eventLogAppender.setLayout(new PatternLayout("%m"));
		eventLogAppender.activateOptions();
		this.addAppender(eventLogAppender);
	}

}
