package com.synapsense.impl.alerting.notification;

import com.synapsense.dto.Message;
import com.synapsense.dto.MessageType;

import java.util.Map;

public class CommonMessage implements Message {
	private static final long serialVersionUID = -303105353414219470L;

	private final Integer alertId;
	private final String header;
	private final String body;
	private final Destination recipient;
	private final MessageType mType;
	private final String priority;
	private final transient Map<String, Object> mediaData;

	public CommonMessage(final Integer alertId, final MessageType type, final String header, final String body,
	        final Destination recipient, final String priority, Map<String, Object> mediaData) {
		this.alertId = alertId;
		this.header = header;
		this.body = body;
		this.recipient = recipient;
		this.mType = type;
		this.priority = priority;
		this.mediaData = mediaData;
	}

	@Override
	public String getProtocol() {
		return mType.name();
	}

	@Override
	public String getHeader() {
		return header;
	}

	@Override
	public String getContent() {
		return body;
	}

	@Override
	public String getDestination() {
		return recipient.getAddress(mType);
	}

	@Override
	public Map<String, Object> getMediaData() {
		return mediaData;
	}

	@Override
	public String getPriority() {
		return priority;
	}

	@Override
	public String toString() {
		return "CommonMessage [header=" + header + ", mType=" + mType + ", priority=" + priority + "]";
	}

	@Override
	public Integer getAlertID() {
		return alertId;
	}

}