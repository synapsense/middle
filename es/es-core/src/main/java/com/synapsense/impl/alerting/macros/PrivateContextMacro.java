package com.synapsense.impl.alerting.macros;

import com.synapsense.util.CollectionUtils;

/**
 * Use if you need to avoid changes in caller's context during macro expanding.
 * All macros from {@link #macros} use the same properties set which is not
 * visible to any other macro. At the end these changes propagate to up level.
 * 
 * @author : shabanov
 */
public class PrivateContextMacro implements Macro {
	private MacrosCollection macros;

	public PrivateContextMacro(MacrosCollection macros) {
		this.macros = macros;
	}

	@Override
	public String expandIn(String input, MacroContext ctx) {
		// do not change parent context
		MacroContext tempCtx = new MacroContext(ctx);

		// reset parent context props
		tempCtx.props = CollectionUtils.newMap();

		String expanded = macros.expandIn(input, tempCtx);

		// propagate all props from temporary context to up level
		ctx.props.putAll(tempCtx.props);

		return expanded;
	}
}
