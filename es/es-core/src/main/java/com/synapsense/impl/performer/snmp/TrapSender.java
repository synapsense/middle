package com.synapsense.impl.performer.snmp;

import java.net.SocketException;

import org.apache.log4j.Logger;
import org.opennms.protocols.snmp.SnmpHandler;
import org.opennms.protocols.snmp.SnmpIPAddress;
import org.opennms.protocols.snmp.SnmpParameters;
import org.opennms.protocols.snmp.SnmpPduPacket;
import org.opennms.protocols.snmp.SnmpPduTrap;
import org.opennms.protocols.snmp.SnmpPeer;
import org.opennms.protocols.snmp.SnmpSMI;
import org.opennms.protocols.snmp.SnmpSession;
import org.opennms.protocols.snmp.SnmpSyntax;

import com.synapsense.impl.performer.snmp.conf.ConnectionInfoProvider;
import com.synapsense.impl.performer.snmp.conf.SNMPConfigurationException;
import com.synapsense.impl.performer.snmp.conf.ConnectionInfoProvider.ConnectionInfo;

public class TrapSender implements SnmpHandler {
	private final static Logger logger = Logger.getLogger(TrapSender.class);

	private final static int VERSION = SnmpSMI.SNMPV1;

	private final String destination;
	private SnmpSession session;
	private final ConnectionInfo connInfo;

	public TrapSender(String destination) throws SNMPConfigurationException {

		this.destination = destination;
		connInfo = ConnectionInfoProvider.getConnectionInfo(destination,"TRAP");
		if (logger.isInfoEnabled()) {
			logger.info("creating a sender from " + connInfo.getLocAddr() + ":" + connInfo.getLocPort() + " to "
			        + connInfo.getRemAddr() + ":" + connInfo.getRemPort());
		}
		SnmpPeer peer = new SnmpPeer(connInfo.getRemAddr(), connInfo.getRemPort(), connInfo.getLocAddr(),
		        connInfo.getLocPort());
		SnmpParameters params = peer.getParameters();
		params.setReadCommunity(connInfo.getReadCommunity());
		params.setVersion(VERSION);
		params.setWriteCommunity(connInfo.getWriteCommunity());

		peer.setParameters(params);

		try {
			session = new SnmpSession(peer);
		} catch (SocketException e) {
			throw new SNMPConfigurationException("Cannot create SNMP session,", e);
		}
		session.setDefaultHandler(this);
		logger.info("Sender creation done.");
	}

	public void send(SnmpPduTrap pdu) throws SNMPPerformerException {
		try {
			SnmpIPAddress addr = new SnmpIPAddress(ConnectionInfoProvider.getLocalAddress());
			pdu.setAgentAddress(addr);
			session.send(pdu);
		} catch (Exception e) {
			throw new SNMPPerformerException("Could not send the SNMP Trap ", e);
		}
	}

	public void close() {
		logger.info("Closing the SNMP Trap Sender to " + destination);
		session.close();
		session = null;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((destination == null) ? 0 : destination.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final TrapSender other = (TrapSender) obj;
		if (destination == null) {
			if (other.destination != null)
				return false;
		} else if (!destination.equals(other.destination))
			return false;
		return true;
	}

	@Override
	public void snmpInternalError(SnmpSession arg0, int arg1, SnmpSyntax arg2) {
		logger.info("Got snmp internal error " + arg2);
	}

	@Override
	public void snmpReceivedPdu(SnmpSession arg0, int arg1, SnmpPduPacket arg2) {
		logger.info("Got SNMP PDU " + arg2);
	}

	@Override
	public void snmpTimeoutError(SnmpSession arg0, SnmpSyntax arg1) {
		logger.info("Got SNMP Timeout error " + arg1);
	}
}
