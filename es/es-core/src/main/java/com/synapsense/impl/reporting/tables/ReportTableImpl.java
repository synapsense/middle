package com.synapsense.impl.reporting.tables;

import com.synapsense.service.reporting.Cell;
import com.synapsense.service.reporting.DimensionalQuery;
import com.synapsense.service.reporting.dto.RowTO;
import com.synapsense.service.reporting.dto.TableTO;
import com.synapsense.service.reporting.exception.ReportingQueryException;
import com.synapsense.service.reporting.olap.*;
import com.synapsense.service.reporting.olap.custom.Cube;
import com.synapsense.service.reporting.olap.custom.Level;
import com.synapsense.service.reporting.olap.custom.TimeHierarchy;
import com.synapsense.util.CollectionUtils;
import java.io.Serializable;
import java.util.*;

public class ReportTableImpl implements ReportTable, Serializable {

	private static final long serialVersionUID = -3957905569403762434L;
	
	private ReportTableIterator reportTableIterator;
	private final ArrayList<Cell> cells;
	private int rowsNumber;
	private final int columnsNumber;
	private Map<Integer, List<String>> headerMap = CollectionUtils.newMap();
	private List<Tuple> joinedTableTimeMembers;
	private int startRow = 1;

	public static final int ROW_HEADER = 0;
	public static final int COLUMN_HEADER = 1;
	public static final int SLICER_AXIS_NUMBER = 2;
	public static final String DATA_VALUE_HEADER = "Data Value";
	public static final String ALL_HEADER = "All";
	private Map<Integer, List<String>> axisesTimeLevels = CollectionUtils.newMap();
	private DimensionalQuery query;

	private int userId;

	private BaseHierarchy timeHierarchy;
	
	public ReportTableImpl(List<Cell> cells, DimensionalQuery query, int userId) {
		this.reportTableIterator = new ReportTableIterator();
		this.cells = new ArrayList<>(cells);
		this.query = query;
		
        CrossJoin table = crossjoinTable(query);
        this.setTimeHierarchy(table);
		
		fillAxisesTimeLevels();
		if (headerMap.isEmpty()){
			fillHeaders(table);
		}

		this.rowsNumber = getAxisSize(0);
		this.columnsNumber = getAxisSize(1);

		this.userId = userId;
	}

//	public void setPage(int startRow, int endRow) {
//		this.startRow = startRow;
//		int pageRowsNumber = endRow - startRow + 1;
//		int tableRowsNumber = getAxisSize(0);
//		this.rowsNumber = (tableRowsNumber < pageRowsNumber)? tableRowsNumber : pageRowsNumber;
//	}
	
    private CrossJoin crossjoinTable(DimensionalQuery query) {
        AxisSet slicer = query.getSlicer();
        return (slicer == null)
                ? new CrossJoin(query.getAxisSet(0), query.getAxisSet(1))
                : new CrossJoin(query.getSlicer(), query.getAxisSet(0), query.getAxisSet(1));
    }


    private void setTimeHierarchy(CrossJoin table) {

        AxisSet timeHierarchySet = isJoinedTableNeeded() ? table : query.getSlicer();
        List<Hierarchy> hierarchyList = timeHierarchySet.getDimensionalities();
        for (Hierarchy hierarchy : hierarchyList) {
            if (hierarchy instanceof TimeHierarchy) {
            	Tuple firstTuple = timeHierarchySet.getTuples().iterator().next();
            	this.timeHierarchy = Cube.getBaseHierarchy(firstTuple.itemByDimension(Cube.DIM_TIME).getHierarchy().getName());
            }
        }

    }
    
	private void fillAxisesTimeLevels(){
        AxisSet[] axisSets = new AxisSet[] {query.getAxisSet(0), query.getAxisSet(1)};
		for (int axisIndex = 0; axisIndex < axisSets.length; axisIndex++){
			AxisSet axisSet = axisSets[axisIndex];
			List<String> axisTimeLevels = getTimeLevels(axisIndex, axisSet);
			if (!axisTimeLevels.isEmpty()) {
				axisesTimeLevels.put(axisIndex, axisTimeLevels);
			}
		}
		AxisSet slicer = query.getSlicer();
		if (slicer != null){
			axisesTimeLevels.put(SLICER_AXIS_NUMBER, getTimeLevels(SLICER_AXIS_NUMBER, slicer));
		}
	}
	
	//Fill all time levels of axisSet
	private List<String> getTimeLevels(int axisIndex, AxisSet axisSet) {
		
		List<String> timeLevels = CollectionUtils.newList();
		List<Tuple> tuples = axisSet.getTuples();
		if (tuples.isEmpty()) {
			return timeLevels;
		}

		Member timeMember = null;
		if (axisIndex == SLICER_AXIS_NUMBER && axisSet instanceof RangeSet) {
			timeMember = ((RangeSet)axisSet).getStartMember();
		} else {
			timeMember = tuples.iterator().next().itemByHierarchy(timeHierarchy);
		}
		Member current = timeMember;
		while (current != null) {
			timeLevels.add(current.getLevel());
			current = current.getParent();
		}

		return timeLevels;
	}


    private boolean isJoinedTableNeeded() {
        AxisSet[] axisSets = new AxisSet[] {query.getAxisSet(0), query.getAxisSet(1)};
		AxisSet slicer = query.getSlicer();
		//We need to join time members from all axises to consider only the cross-joined ones
		boolean joinedTableNeeded = true;
        if (slicer!= null
        		&& containsTimeHierarchy(slicer)
                && !containsTimeHierarchy(axisSets[0])
                && !containsTimeHierarchy(axisSets[1])
                && slicer instanceof RangeSet) {
            joinedTableNeeded = false;
        }
        return joinedTableNeeded;
    }

//Composing headers lists for whole Dimensional Query
   private void fillHeaders(CrossJoin table) {
       AxisSet[] axisSets = new AxisSet[] {query.getAxisSet(0), query.getAxisSet(1)};
       AxisSet slicer = query.getSlicer();
       if (isJoinedTableNeeded()) {
            joinedTableTimeMembers = CrossJoin.getTuplesByHierarchy(table, timeHierarchy);
        }
		int axisIndex = 0;
		for (AxisSet axisSet : axisSets){
			if (!axisSet.getTuples().isEmpty()) {
				headerMap.put(axisIndex, fillHeader(axisIndex, axisSet));
			} else if (axisIndex == COLUMN_HEADER) {
				List<String> singleHeader = CollectionUtils.newList();
				singleHeader.add(DATA_VALUE_HEADER);
				headerMap.put(axisIndex, singleHeader);
			}
			axisIndex++;
		}
		if (slicer != null) {
			headerMap.put(SLICER_AXIS_NUMBER, fillHeader(SLICER_AXIS_NUMBER, slicer));
		}
	}
	
    private boolean containsTimeHierarchy(AxisSet axisSet) {
          return axisSet.getDimensionalities().contains(Cube.TIME)
                  || axisSet.getDimensionalities().contains(Cube.WEEK);
    }
    
	//Filling headers list for specified axis
	private List<String> fillHeader(int axisIndex, AxisSet axisSet){
		List<Tuple> tuples = CollectionUtils.newList();
		if (axisIndex == SLICER_AXIS_NUMBER && axisSet instanceof RangeSet) {
			RangeSet rangeSet = (RangeSet)axisSet;
			tuples.add(rangeSet.getStartMember());
			tuples.add(rangeSet.getEndMember());
			joinedTableTimeMembers = CollectionUtils.newList();
			joinedTableTimeMembers.addAll(tuples);
		} else {
			tuples = axisSet.getTuples();
		}
		Set<Member> axisTableTimeMembers = CollectionUtils.newSet();
		if (axisSet.getDimensionalities().contains(Cube.TIME)
				|| axisSet.getDimensionalities().contains(Cube.WEEK)){
			for (Tuple tuple : joinedTableTimeMembers){
				axisTableTimeMembers.add(getTableTimeMember(tuple, axisesTimeLevels.get(axisIndex)));
			}
		}
		List<String> headers = new ArrayList<>();
		for (Tuple tuple : tuples){
			StringBuilder header = new StringBuilder();
			//We need to check Time Member if it is comes to requested range. 
			//If not then user doesn't want such header and we shouldn't add this header since no such row expected.
			Member timeTupleMember = tuple.itemByHierarchy(this.timeHierarchy);
			if (timeTupleMember != null) {
				String timeHeaderItem = processHeaderItem(timeTupleMember, axisTableTimeMembers);
				if (timeHeaderItem.isEmpty()) {
					continue;
				}
			}
			Member objectTupleMember = tuple.itemByHierarchy(Cube.OBJECT);
			if (objectTupleMember != null) {
				header.append(processHeaderItem(objectTupleMember, axisTableTimeMembers));
			}
			Member propertyTupleMember = tuple.itemByHierarchy(Cube.PROPERTY);
			if (propertyTupleMember != null) {
				header.append(processHeaderItem(propertyTupleMember, axisTableTimeMembers));
			}

			if (timeTupleMember != null) {
				header.append(processHeaderItem(timeTupleMember, axisTableTimeMembers));
			}
			if (header.length() != 0){
				headers.add(header.substring(0, header.length() - 1));
			}
		}
		return headers;
	}
	
	//Compose time member with only levels contained in timeLevels corresponding to axis
	private Member getTableTimeMember(Tuple tuple, List<String> timeLevels){
		Member currentMember = new SimpleMember(tuple.itemByHierarchy(timeHierarchy));
		//Go upper till lowest timeLevel for this axis
		while (!timeLevels.contains(currentMember.getLevel())) {
			currentMember = currentMember.getParent();
		}
		
		Member resultMember = new SimpleMember(timeHierarchy, currentMember.getLevel(), currentMember.getKey());
		//Go upper by levels to find each from axis time levels
		while ((currentMember = currentMember.getParent()) != null){
			if (timeLevels.contains(currentMember.getLevel())){
				Member resultParent = new SimpleMember(timeHierarchy, currentMember.getLevel(), currentMember.getKey());	
				Member timeMemberParent = resultMember;
				// Going upper to set member to level for which setting parent is needed
				while (!timeMemberParent.getLevel()
						.equals(timeHierarchy.getChildLevel(currentMember.getLevel()))) {
					timeMemberParent = timeMemberParent.getParent();
				}
 				((SimpleMember) timeMemberParent).setParent(resultParent);
			}
		}
		return resultMember;
	}
	
	//We need to check here if axis time member is in table time range
	private String processHeaderItem(Member member, Set<Member> axisTableTimeMembers){
		StringBuilder header = new StringBuilder();
		if (OrderedHierarchy.class.isInstance(member.getHierarchy())){
			if (member.getLevel().equals(axisTableTimeMembers.iterator().next().getLevel())
					&& !axisTableTimeMembers.contains(member)){
				return "";
			}
			if (member.getParent() != null) {
				header.append(processHeaderItem(member.getParent(), axisTableTimeMembers));
			}
			Level customLevel = member.getCustomLevel();
			if (customLevel.getMembersRange() > 1) {
				header.append(customLevel.getName())
					.append(".")
					.append(Cube.TIME.getSimpleMembers(member).iterator().next().getKey())
					.append(",");
				return header.toString();
			}
		}
		Object key = member.getKey();
		
		String level = member.getLevel();
		Hierarchy hierarchy = member.getHierarchy();
		if (!(hierarchy instanceof TimeHierarchy)) {
			header.append(member.getHierarchy().getName())
				.append(".");
		}
		header.append(level)
			.append(".")
			.append(key)
			.append(",");
		return header.toString();
	}

	private int getAxisSize(int axisIndex) {
		if (!headerMap.containsKey(axisIndex)) {
			return 0;
		}
		return this.headerMap.get(axisIndex).size();
	}

	private List<String> getHeader(int index) {
		List<String> result = CollectionUtils.newList();
		if (!headerMap.containsKey(index)) {
			return result;
		}
		List<String> headers = headerMap.get(index);
		switch(index){
			case ROW_HEADER:
				//If requested page start row is more than table rows size we return empty list
				if (startRow > headers.size()){
					return result;
				}
				//If requested page end row is more than rows size we cut returned list by table rows size
				int headerEndRow = startRow + rowsNumber - 1;
				if (startRow + rowsNumber - 1 > headers.size()){
					headerEndRow = headers.size();
				}
				result.addAll(headers.subList(startRow - 1, headerEndRow));
				if (result.isEmpty()) {
					throw new ReportingQueryException("Table Header shouldn't be empty");
				}
				break;
			case COLUMN_HEADER:
				result.addAll(headers);
				if (result.isEmpty()) {
					throw new ReportingQueryException("Table Header shouldn't be empty");
				}
				break;
			case SLICER_AXIS_NUMBER:
				result.addAll(headers);
				break;
		}
		
		return result;
	}

	@Override
	public TableTO getTable() {
		List<RowTO> reportRows = CollectionUtils.newList();
		for (ReportRow complexReportRow : this) {
			reportRows.add(complexReportRow.getRow());
		}
		return new TableTO(reportRows, userId, Collections.unmodifiableMap(headerMap), rowsNumber);
	}

	@Override
	public Iterator<ReportRow> iterator() {
		return this.reportTableIterator;
	}
	
	public class ReportTableIterator implements Iterator<ReportRow>, Serializable {

		private static final long serialVersionUID = -4095505060427645782L;
		
		private int row = -1;
		
		public ReportTableIterator() {
		}

		@Override
		public boolean hasNext() {
			if (row + 1 > rowsNumber || rowsNumber == 0) {
				return false;
			}
			return true;
		}

		@Override
		public ReportRow next() {
			if (!hasNext()){
				return null;
			}
			row = (row == -1) ? startRow : ++row;
			Map<Integer, List<String>> rowTableHeaders = CollectionUtils.newMap();
			List<String> rowHeaders = CollectionUtils.newList();
			rowHeaders.add(getHeader(ROW_HEADER, row));
			rowTableHeaders.put(ROW_HEADER, rowHeaders);
			rowTableHeaders.put(COLUMN_HEADER, getHeader(COLUMN_HEADER));
			rowTableHeaders.put(SLICER_AXIS_NUMBER, getHeader(SLICER_AXIS_NUMBER));
			ReportRow reportingRow = new ReportRow(row, columnsNumber, getRowCells(row), rowTableHeaders);
			return reportingRow;
		}
		
		@Override
		public void remove() {
			throw new UnsupportedOperationException("Remove is not supported"); 
		}
	}
	
	private String getHeader(int header, int itemIndex) {
		List<String> headers = getHeader(header);
		return headers.get(itemIndex - 1);
	}

	private List<Cell> getRowCells(int row){
		List<Cell> rowCells = CollectionUtils.newList();
		for (int i = 0; i < columnsNumber;i++){
				rowCells.add(cells.get((row - 1) * columnsNumber + i));
		}
		return rowCells;
	}

}

