package com.synapsense.impl.devmanager.dissectors;

import com.synapsense.dto.TO;
import com.synapsense.plugin.messages.GenericMessage;
import com.synapsense.plugin.messages.GenericMessage.GenericMessageTypes;
import com.synapsense.plugin.messages.RefinedAlertsBuilder.FieldConstants;
import com.synapsense.service.impl.messages.base.RefinedAlertData;
import com.synapsense.util.algo.Pair;
import com.synapsense.util.algo.UnaryFunctor;

public class RefinedAlertsDissector implements MessageDissector {
	private final UnaryFunctor<Pair<TO<?>, RefinedAlertData>> functor;

	public RefinedAlertsDissector(UnaryFunctor<Pair<TO<?>, RefinedAlertData>> functor) {
		this.functor = functor;
	}

	@Override
	public void process(TO<?> objID, GenericMessage msg) {
		try {
			RefinedAlertData data = new RefinedAlertData((String) msg.get(FieldConstants.TYPE.name()),
			        (String) msg.get(FieldConstants.NAME.name()), (String) msg.get(FieldConstants.MESSAGE.name()));
			functor.process(new Pair<TO<?>, RefinedAlertData>(objID, data));
		} catch (Exception e) {
			throw new IllegalArgumentException("Wrong data is stored in message", e);
		}
	}

	@Override
	public boolean tasteMessage(GenericMessage msg) {
		return msg.getType() == GenericMessageTypes.REFINED_ALERT;
	}
}
