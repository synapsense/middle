package com.synapsense.impl.erlnode.filters;

import org.apache.log4j.Logger;

import com.ericsson.otp.erlang.OtpErlangObject;
import com.ericsson.otp.erlang.OtpErlangTuple;
import com.synapsense.impl.erlnode.commands.RegisterProcessorCommand;
import com.synapsense.service.nsvc.EventProcessor;
import com.synapsense.service.nsvc.filters.NotFilter;

public class NotFilterParser implements FilterDefParser {

	private static final Logger log =
		Logger.getLogger(NotFilterParser.class);
	
	@Override
	public EventProcessor parse(OtpErlangTuple filterDef) {
		OtpErlangTuple subFilterDefinition;
		try {
			OtpErlangObject tmp = filterDef.elementAt(1);
			if(tmp==null || RegisterProcessorCommand.undefined.equals(tmp))
				throw new IllegalArgumentException("The second parameter of not_filter must be a tuple");
			subFilterDefinition = (OtpErlangTuple) tmp;
		} catch (ClassCastException e) {
			throw new IllegalArgumentException("The second parameter of not_filter must be a tuple but was"+
					filterDef.elementAt(1).getClass().getName(), e);
		}
		
		log.debug("returning NotFilter");
		return new NotFilter(EventFiltersFactory.createFilter(subFilterDefinition));
	}

}
