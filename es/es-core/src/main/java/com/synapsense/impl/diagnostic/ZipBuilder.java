package com.synapsense.impl.diagnostic;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.log4j.Logger;

public class ZipBuilder {
	private static final Logger log = Logger.getLogger(ZipBuilder.class);
	private static final int BUFFER = 2048;

	protected ZipOutputStream zipOut;

	public ZipBuilder(String destinationFileName) throws IOException {
		FileOutputStream dest = new FileOutputStream(destinationFileName);
		zipOut = new ZipOutputStream(new BufferedOutputStream(dest));
	}

	public void append(Collection<String> fileNames) throws IOException {
		append(fileNames, null);
	}

	public void append(Collection<String> fileNames, String rootFolderPath) throws IOException {
		BufferedInputStream origin = null;
		byte data[] = new byte[BUFFER];

		for (String fileName : fileNames) {
			try {
				log.info("Zip file: " + fileName);
				String zipEntryName = fileName.replace(":", "").replace("\\", "/");
				String realFileName = fileName;
				if (rootFolderPath != null) {
					realFileName = rootFolderPath + File.separator + fileName;
				}
				origin = new BufferedInputStream(new FileInputStream(realFileName), BUFFER);
				zipOut.putNextEntry(new ZipEntry(zipEntryName));
				int count;
				while ((count = origin.read(data, 0, BUFFER)) != -1) {
					zipOut.write(data, 0, count);
				}
				zipOut.closeEntry();
				log.info("Zip (file " + fileName + ") completed.");
			} catch (Exception e) {
				log.error(e.getLocalizedMessage(), e); // write exception into
				                                       // "log" and continue
			} finally {
				if (origin != null) {
					origin.close();
				}
			}
		}
	}

	public void append(String zipEntryName, InputStream in) throws IOException {
		log.info("Zip entry: " + zipEntryName);
		byte buf[] = new byte[BUFFER];
		zipOut.putNextEntry(new ZipEntry(zipEntryName));
		int len;
		while ((len = in.read(buf)) > 0) {
			zipOut.write(buf, 0, len);
		}
		zipOut.closeEntry();
		in.close();
		log.info("Zip (file " + zipEntryName + ") completed.");
	}

	public void endBuild() throws IOException {
		zipOut.close();
	}
}
