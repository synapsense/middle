package com.synapsense.impl.auditsvc.auditor;

import java.io.IOException;

import org.apache.commons.httpclient.HttpConnection;
import org.apache.commons.httpclient.HttpState;
import org.apache.commons.httpclient.methods.GetMethod;

import com.synapsense.service.impl.audit.AuditException;
import com.synapsense.util.LocalizationUtils;

public class WebServerAuditor extends BasicAuditor {

	private static final long serialVersionUID = 3211812772517075585L;

	private int timeout = 3000;
	private String webServer;
	private int port;
	private String page;
	private String msg;

	public WebServerAuditor() {
		super();
		configuration = getURLString();
	}

	public WebServerAuditor(String connection) {
		super(connection);
	}

	public WebServerAuditor(String webServer, int port, String page) {
		super();
		this.page = page;
		this.port = port;
		this.webServer = webServer;
		this.configuration = getURLString();
	}

	protected String getURLString() {
		String prefix = "";
		if (webServer == null || !webServer.startsWith("http://"))
			prefix = "http://";
		return prefix + webServer + ":" + port + "/" + page;
	}

	@Override
	public void doAudit() throws AuditException {
		if (logger.isInfoEnabled()) {
			logger.debug("Starting web service auditor");
		}
		HttpConnection clientConnection = null;
		String errMsg = LocalizationUtils.getLocalizedString(msg);

		try {
			clientConnection = new HttpConnection(webServer, port);
			clientConnection.getParams().setConnectionTimeout(timeout);
			clientConnection.open();

			GetMethod httpGet = new GetMethod(configuration);
			if (logger.isDebugEnabled()) {
				logger.debug("Sending " + httpGet.getQueryString());
			}
			int respCode = httpGet.execute(new HttpState(), clientConnection);

			if (!clientConnection.isResponseAvailable(timeout)) {
				throw new AuditException("Unable to get response in ["
						+ timeout + "] mS");
			}

			if (logger.isDebugEnabled()) {
				String respBody = httpGet.getResponseBodyAsString();
				logger.debug("http response code" + respCode);
				logger.debug("http response " + respBody);
			}

			// We consider only HTTP 2xx codes (SUCCESS) as acceptable
			if (respCode < 200 || respCode >= 300) {
				throw new AuditException(errMsg
						+ LocalizationUtils.getLocalizedString(
								"audit_message_server_response_code",
								Integer.toString(respCode)));
			}
		} catch (IOException e1) {
			throw new AuditException(errMsg
					+ LocalizationUtils.getLocalizedString(
							"audit_message_server_response_code", "N/A"), e1);
		} catch (Throwable e) {
			if (logger.isWarnEnabled()) {
				logger.warn(e.getLocalizedMessage(), e);
			}
			throw new AuditException(e.getLocalizedMessage(), e);
		} finally {
			if (logger.isInfoEnabled()) {
				logger.debug("Web service auditor finished");
			}
			if (clientConnection != null && clientConnection.isOpen())
				clientConnection.close();
		}
	}

	@Override
	public String[] configurableProps() {
		return new String[] { "webServer", "port", "page", "timeout", "msg" };
	}

	public int getTimeout() {
		return timeout;
	}

	public void setTimeout(int timeout) {
		this.timeout = timeout;
	}

	public void setTimeout(String timeout) {
		this.timeout = Integer.parseInt(timeout);
	}

	public String getWebServer() {
		return webServer;
	}

	public void setWebServer(String webServer) {
		this.webServer = webServer;
		this.configuration = getURLString();
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
		this.configuration = getURLString();
	}

	public void setPort(String port) {
		this.port = Integer.parseInt(port);
		this.configuration = getURLString();
	}

	public String getPage() {
		return page;
	}

	public void setPage(String page) {
		this.page = page;
		this.configuration = getURLString();
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((page == null) ? 0 : page.hashCode());
		result = prime * result + port;
		result = prime * result + timeout;
		result = prime * result
				+ ((webServer == null) ? 0 : webServer.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		final WebServerAuditor other = (WebServerAuditor) obj;
		if (page == null) {
			if (other.page != null)
				return false;
		} else if (!page.equals(other.page))
			return false;
		if (port != other.port)
			return false;
		if (timeout != other.timeout)
			return false;
		if (webServer == null) {
			if (other.webServer != null)
				return false;
		} else if (!webServer.equals(other.webServer))
			return false;
		return true;
	}

	@Override
	public String getConfiguration() {
		return configuration;
	}

}
