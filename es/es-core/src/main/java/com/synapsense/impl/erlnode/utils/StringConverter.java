package com.synapsense.impl.erlnode.utils;

import java.nio.charset.Charset;

import com.ericsson.otp.erlang.OtpErlangAtom;
import com.ericsson.otp.erlang.OtpErlangBinary;
import com.ericsson.otp.erlang.OtpErlangObject;

public class StringConverter {
	static OtpErlangAtom nullAtom = new OtpErlangAtom("null");
	static Charset utf8 = Charset.forName("UTF-8");
	public static String utf8BinaryToString(OtpErlangObject utf8Bin) {
		if(utf8Bin instanceof OtpErlangAtom && utf8Bin == nullAtom) {
			return null;
		} else {
			return new String(((OtpErlangBinary)utf8Bin).binaryValue(), utf8);
		}
	}
	public static OtpErlangObject stringToUtf8Binary(String s) {
		if(s == null) {
			return nullAtom;
		} else {
			return new OtpErlangBinary(s.getBytes(utf8));
		}
	}
}
