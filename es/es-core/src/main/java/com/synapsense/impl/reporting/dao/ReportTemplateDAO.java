package com.synapsense.impl.reporting.dao;

import java.io.Serializable;
import java.util.Collection;

import com.synapsense.exception.ObjectExistsException;
import com.synapsense.service.reporting.dto.ReportTemplateInfo;
import com.synapsense.service.reporting.exception.ObjectNotFoundDAOReportingException;

public interface ReportTemplateDAO extends Serializable {

	void createReportTemplate(ReportTemplateInfo reportTemplateInfo, String reportTemplateDesign)
	        throws ObjectNotFoundDAOReportingException, ObjectExistsException;

	void updateReportTemplate(ReportTemplateInfo reportTemplateInfo, String reportTemplateDesign)
	        throws ObjectNotFoundDAOReportingException, ObjectExistsException;

	void removeReportTemplate(String reportTemplateName) throws ObjectNotFoundDAOReportingException;

	Collection<String> getReportTemplates();

	String getReportTemplateDesign(String reportTemplateName) throws ObjectNotFoundDAOReportingException;

	ReportTemplateInfo getReportTemplateInfo(String reportTemplateName) throws ObjectNotFoundDAOReportingException;
}
