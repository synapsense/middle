package com.synapsense.impl.reporting.utils.algo;

public class ForEach<T> {

	private Iterable<? extends T> collection;

	public ForEach() {
		collection = null;
	}

	public ForEach(Iterable<? extends T> collection) {
		this.collection = collection;
	}

	public void process(Iterable<? extends T> collection, UnaryFunctor<T> functor) {
		for (T element : collection) {
			functor.process(element);
		}
	}

	public void process(UnaryFunctor<T> functor) {
		if (collection == null) {
			throw new IllegalStateException("Collection to iterate over is not set at ForEach ctor");
		}
		process(collection, functor);
	}
}
