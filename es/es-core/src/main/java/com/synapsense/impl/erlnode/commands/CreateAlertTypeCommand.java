package com.synapsense.impl.erlnode.commands;

import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.ericsson.otp.erlang.OtpErlangAtom;
import com.ericsson.otp.erlang.OtpErlangBinary;
import com.ericsson.otp.erlang.OtpErlangBoolean;
import com.ericsson.otp.erlang.OtpErlangObject;
import com.ericsson.otp.erlang.OtpErlangTuple;
import com.synapsense.dto.AlertType;
import com.synapsense.dto.MessageTemplate;
import com.synapsense.dto.MessageTemplateRef;
import com.synapsense.dto.MessageTemplateType;
import com.synapsense.dto.MessageType;
import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.AlertTypeAlreadyExistsException;
import com.synapsense.impl.erlnode.ExceptionTypes;
import com.synapsense.service.AlertService;

import com.synapsense.impl.erlnode.utils.StringConverter;

public class CreateAlertTypeCommand extends Command {
	public final static OtpErlangAtom ALERT_TYPE_ATOM = new OtpErlangAtom("alert_type");
	public final static String DEFAULT_TEMPLATE_TYPE = "ALERT_MESSAGE_TEMPLATE";
	public final static String DEFAULT_TEMPLATE_NAME = "Console System Notification";
	public final static OtpErlangAtom undefined = new OtpErlangAtom("undefined");

	public CreateAlertTypeCommand(Map<String, Object> state) {
		super(state);
	}

	@Override
	public OtpErlangObject do_command(OtpErlangTuple command) throws Exception {
		OtpErlangObject elem = command.elementAt(1);
		if (elem == null || !(elem instanceof OtpErlangTuple)) {
			throw new IllegalArgumentException();
		}
		OtpErlangTuple tuple = (OtpErlangTuple) elem;
		if (tuple == null || tuple.arity() != 8 || !ALERT_TYPE_ATOM.equals(tuple.elementAt(0))) {
			throw new IllegalArgumentException();
		}

		OtpErlangObject elem1 = tuple.elementAt(1);
		OtpErlangObject elem2 = tuple.elementAt(2);
		OtpErlangObject elem3 = tuple.elementAt(3);
		OtpErlangObject elem4 = tuple.elementAt(4);
		OtpErlangObject elem5 = tuple.elementAt(5);
		OtpErlangObject elem6 = tuple.elementAt(6);
		OtpErlangObject elem7 = tuple.elementAt(7);
		if (!(elem1 instanceof OtpErlangBinary) || !(elem2 instanceof OtpErlangBinary)
		        || !(elem3 instanceof OtpErlangBinary) || !(elem4 instanceof OtpErlangBinary)
		        || !(elem7 instanceof OtpErlangAtom)) {
			throw new IllegalArgumentException();
		}
		String name = StringConverter.utf8BinaryToString(elem1);
		String displayName = StringConverter.utf8BinaryToString(elem2);
		String description = StringConverter.utf8BinaryToString(elem3);
		String priority = StringConverter.utf8BinaryToString(elem4);
		String defaultHeader = null;
		String defaultBody = null;
		boolean autoDismiss = ((OtpErlangAtom)elem7).booleanValue();
		MessageTemplateRef messageTemplate;
		
		if ((elem5 instanceof OtpErlangBinary) && (elem6 instanceof OtpErlangBinary)) {

			defaultHeader = StringConverter.utf8BinaryToString(elem5);
			defaultBody = StringConverter.utf8BinaryToString(elem6);
			TO<?> templateHolder = getEnv().createObject(
			        DEFAULT_TEMPLATE_TYPE,
			        new ValueTO[] { new ValueTO("name", name),
			                new ValueTO("messagetype", Integer.toString(MessageType.CONSOLE.code())),
			                new ValueTO("type", MessageTemplateType.CUSTOM.code()),
			                new ValueTO("header", defaultHeader), new ValueTO("body", defaultBody) });
			messageTemplate = new MessageTemplateRef(MessageType.CONSOLE, templateHolder);
		} else {
			Collection<TO<?>> tos = getEnv().getObjects(DEFAULT_TEMPLATE_TYPE,
			        new ValueTO[] { new ValueTO("name", DEFAULT_TEMPLATE_NAME) });
			TO<?> templateHolder = tos.iterator().next();
			messageTemplate = new MessageTemplateRef(MessageType.CONSOLE, templateHolder);
		}
		final Set<MessageTemplate> templates = new HashSet<MessageTemplate>(1);
		templates.add(messageTemplate);
		try {
			((AlertService) globalState.get("alertsvc")).createAlertType(new AlertType(name, displayName, description,
			        priority, defaultHeader, defaultBody, templates, true, autoDismiss, false, false));
			return makeOkResult(null);
		} catch (AlertTypeAlreadyExistsException e) {
			return makeException(ExceptionTypes.OBJECT_ALREADY_EXISTS, e.getMessage());
		}

	}

}
