package com.synapsense.impl.i18n;

import com.synapsense.cdi.MBean;
import com.synapsense.exception.IllegalInputParameterException;
import com.synapsense.service.LocalizationService;
import com.synapsense.util.LocalizationUtils;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.DependsOn;
import javax.ejb.Remote;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import com.synapsense.util.Translator;
import org.apache.log4j.Logger;

@Startup
@Singleton(name = "LocalizationService")
@DependsOn("Registrar")
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
@Remote(LocalizationService.class)
@ConcurrencyManagement(ConcurrencyManagementType.BEAN)
@MBean("com.synapsense:type=LocalizationService")
public class LocalizationServiceImpl implements LocalizationService, LocalizationServiceImplMBean {

	private final static Logger logger = Logger.getLogger(LocalizationServiceImpl.class);

	private final static String BUNDLE_NAME = "strings";

	private final static String TRANSLATIONS = "translations";

	private final static String EXTENSION = ".properties";

	private final static String DIR = "/i18n/";

	private final static String DEFAULT_LOCALE_ID = "en_US";

	private final static String DEFAULT_UNIT_SYSTEM = "Server";

	private final static String ENCODING = "UTF-8";

	private String fileStorage = "C:/Program Files/jboss-4.2.3.GA/server/default/conf";

	private Map<Locale, Properties> translations = new HashMap<Locale, Properties>(); // locale->(eng_string->loc_string)

	private Map<Locale, Properties> strings = new HashMap<Locale, Properties>(); // locale->(id->loc_string)

	@Override
	@PostConstruct
	public void create() throws Exception {
		logger.info("creating");
		start();
	}

	@Override
	public void destroy() throws Exception {
		logger.info("destroying");
	}

	@Override
	@PreDestroy
	public void stop() throws Exception {
		logger.info("stoping");
		destroy();
	}

	public static void main(String[] args) throws Exception {
		LocalizationServiceImpl l = new LocalizationServiceImpl();
		l.start();
		// System.out.println(l.getTranslations());
		String t = l.getTranslation("DataCenter", LocalizationUtils.getLocale("en_US"));
		System.out.println(t);
	}

	@Override
	public void start() throws Exception {
		logger.info("starting");
		fileStorage = System.getProperty("com.synapsense.filestorage", fileStorage);

		// predefined strings
		File dir = new File(fileStorage + DIR);

		String[] fileNames = dir.list(new FilenameFilter() {

			@Override
			public boolean accept(File dir, String name) {
				return name.startsWith(BUNDLE_NAME) && name.endsWith("_" + DEFAULT_LOCALE_ID + EXTENSION);
			}
		});

		Locale[] locales = this.listLocales();
		translations.clear();

		if (fileNames != null && fileNames.length > 0) {
			String fileName = fileNames[0];

			Properties en = new Properties();
			Reader reader = new InputStreamReader(new FileInputStream(fileStorage + DIR + fileName), ENCODING);
			en.load(reader);
			reader.close();

			for (Locale locale : locales) {
				String localeName = locale.toString();

				String otherFileName = fileName.replace("_" + DEFAULT_LOCALE_ID, "_" + localeName);
				Properties otherLang = new Properties();
				reader = new InputStreamReader(new FileInputStream(fileStorage + DIR + otherFileName), ENCODING);
				otherLang.load(reader);
				reader.close();

				for (String key : en.stringPropertyNames()) {
					String value1 = en.getProperty(key);
					String value2 = otherLang.getProperty(key);
					if (value2 != null) {
						put(strings, locale, key, value2);
					}

					if (!DEFAULT_LOCALE_ID.equals(localeName)) {
						if (value1 != null && value2 != null) {
							put(translations, locale, value1, value2);
						}
					}
				}

			}
		}

		// merge with user-defined strings
		mergeUserTranslations(getUserTranslations());
		Translator.setLocalizationService(this);
	}

	@Override
	public Locale[] listLocales() {
		File dir = new File(fileStorage + DIR);
		List<Locale> locales = new ArrayList<Locale>();

		String[] fileNames = dir.list(new FilenameFilter() {

			@Override
			public boolean accept(File dir, String name) {
				return name.startsWith(BUNDLE_NAME) && name.endsWith(EXTENSION);
			}
		});

		for (String fileName : fileNames) {
			int beginIndex = BUNDLE_NAME.length() + 1;
			int endIndex = fileName.length() - EXTENSION.length();
			if (beginIndex < endIndex) {
				String localeId = fileName.substring(beginIndex, endIndex);
				Locale locale = null;

				String[] tokens = localeId.split("_");
				if (tokens.length == 1) {
					locale = new Locale(tokens[0]);
				} else if (tokens.length == 2) {
					locale = new Locale(tokens[0], tokens[1]);
				} else if (tokens.length == 3) {
					locale = new Locale(tokens[0], tokens[1], tokens[2]);
				}

				if (locale != null) {
					locales.add(locale);
				}
			}
		}

		return locales.toArray(new Locale[locales.size()]);
	}

	@Override
	public Locale getDefaultLocale() {
		return Locale.US;
	}

	@Override
	public String[] listUnitSystems() {
		return new String[] { DEFAULT_UNIT_SYSTEM, "SI" };
	}

	@Override
	public String getDefaultUnitSystem() {
		return DEFAULT_UNIT_SYSTEM;
	}

	@Override
	public String getTranslation(String string, Locale locale) {
		if (string == null) {
			throw new IllegalInputParameterException("Supplied 'string' is null");
		}
		if (string.isEmpty()) {
			throw new IllegalInputParameterException("Supplied 'string' is empty");
		}
		if (locale == null) {
			throw new IllegalInputParameterException("Supplied 'locale' is null");
		}

		Properties p = translations.get(locale);
		if (p != null) {
			if (logger.isDebugEnabled())
				logger.debug("Translation for [" + string + "] is " + p.getProperty(string));
			return p.getProperty(string, string);
		}
		return string;
	}

	@Override
	public String getPartialTranslation(String string, Locale locale) {
		if (string == null) {
			throw new IllegalInputParameterException("Supplied 'string' is null");
		}
		if (string.isEmpty()) {
			throw new IllegalInputParameterException("Supplied 'string' is empty");
		}
		if (locale == null) {
			throw new IllegalInputParameterException("Supplied 'locale' is null");
		}

		Properties p = translations.get(locale);
		if (p == null) {
			return string;
		}

		String res = p.getProperty(string);
		if (res != null) {
			return res;
		}

		// well, now try to split phrase by words and find longest sub-phase
		// that has translation
		List<String> words = Arrays.asList(string.split(" "));

		if (words.size() > 1) {
			for (int i = words.size() - 1; i > 0; i--) {
				String s = join(words.subList(0, i));
				res = p.getProperty(s);
				if (res != null) {
					return res + " " + getPartialTranslation(join(words.subList(i, words.size())), locale);
				}
			}

			for (int i = 1; i < words.size(); i++) {
				String s = join(words.subList(i, words.size()));
				res = p.getProperty(s);
				if (res != null) {
					return getPartialTranslation(join(words.subList(0, i)), locale) + " " + res;
				}
			}
		}

		return string;
	}

	@Override
	public Map<Locale, Properties> getTranslations() {
		return Collections.unmodifiableMap(translations);
	}

	@Override
	public Map<Locale, Properties> getUserTranslations() {
		Map<Locale, Properties> userTranslations = new HashMap<Locale, Properties>(); // locale->(string->string)
		File dir = new File(fileStorage + DIR);

		String[] fileNames = dir.list(new FilenameFilter() {

			@Override
			public boolean accept(File dir, String name) {
				return name.startsWith(TRANSLATIONS) && name.endsWith(EXTENSION);
			}
		});

		if (fileNames != null) {
			for (String fileName : fileNames) {
				String localeId = fileName.substring(TRANSLATIONS.length() + 1, fileName.length() - EXTENSION.length());
				Locale locale = LocalizationUtils.getLocale(localeId);
				Properties tr = new Properties();
				try {
					Reader reader;
					reader = new InputStreamReader(new FileInputStream(fileStorage + DIR + "/" + fileName), ENCODING);
					tr.load(reader);
					for (Map.Entry<Object, Object> entry : tr.entrySet()) {
						String key = (String) entry.getKey();
						String value = (String) entry.getValue();
						put(userTranslations, locale, key, value);
						put(translations, locale, key, value);
					}
				} catch (UnsupportedEncodingException e) {
					logger.error(e.getMessage(), e);
				} catch (FileNotFoundException e) {
					// was deleted?
					logger.warn(e.getMessage(), e);
				} catch (IOException e) {
					logger.error(e.getMessage(), e);
				}
			}
		}

		return userTranslations;
	}

	@Override
	public void setUserTranslations(Map<Locale, Properties> newUserTranslations) {
		if (newUserTranslations == null) {
			throw new IllegalInputParameterException("Supplied 'newUserTranslations' is null");
		}

		Map<Locale, Properties> userTranslations = getUserTranslations();
		// first remove old user translations
		for (Map.Entry<Locale, Properties> entry : userTranslations.entrySet()) {
			Properties p = translations.get(entry.getKey());
			for (String propName : entry.getValue().stringPropertyNames()) {
				p.remove(propName);
			}
		}

		// then add new user translations
		mergeUserTranslations(newUserTranslations);

		// write to disk
		File dir = new File(fileStorage + DIR);
		if (!dir.exists()) {
			dir.mkdir();
		}

		for (Map.Entry<Locale, Properties> entry : newUserTranslations.entrySet()) {
			String locale = entry.getKey().toString();
			Properties properties = entry.getValue();
			Writer writer;
			try {
				writer = new OutputStreamWriter(new FileOutputStream(fileStorage + DIR + "/" + TRANSLATIONS + "_"
				        + locale + EXTENSION), ENCODING);
				properties.store(writer, null);
				writer.close();
			} catch (IOException e) {
				logger.error(e.getMessage(), e);
			}
		}

	}

	@Override
	public String getString(String id) {
		return getString(id, getDefaultLocale());
	}

	@Override
	public String getString(String id, Locale locale) {
		if (id == null) {
			throw new IllegalInputParameterException("Supplied 'id' is null");
		}
		if (id.isEmpty()) {
			throw new IllegalInputParameterException("Supplied 'id' is empty");
		}
		if (locale == null) {
			throw new IllegalInputParameterException("Supplied 'locale' is null");
		}

		Properties p = strings.get(locale);

		if (p != null) {
			return p.getProperty(id);
		}

		return null;
	}

	@Override
	public Map<Locale, Properties> getStrings() {
		return strings;
	}

	private String join(List<String> strs) {

		if (strs.isEmpty()) {
			return "";
		}

		StringBuffer b = new StringBuffer();

		for (String str : strs) {
			b.append(str).append(" ");
		}

		return b.substring(0, b.length() - 1);
	}

	private void put(Map<Locale, Properties> map, Locale locale, String key, String value) {
		Properties p = map.get(locale);
		if (p == null) {
			p = new Properties();
			map.put(locale, p);
		}
		p.put(key, value);
	}

	private void mergeUserTranslations(Map<Locale, Properties> userTranslations) {
		for (Map.Entry<Locale, Properties> entry : userTranslations.entrySet()) {
			Locale locale = entry.getKey();
			Properties p = entry.getValue();
			for (String propName : p.stringPropertyNames()) {
				put(translations, locale, propName, p.getProperty(propName));
			}
		}
	}

}
