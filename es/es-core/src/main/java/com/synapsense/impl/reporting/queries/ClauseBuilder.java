package com.synapsense.impl.reporting.queries;

/**
 * The Interface ClauseBuilder.
 */
public interface ClauseBuilder {
	
	/**
	 * Builds the clause.
	 *
	 * @param owner the owner
	 * @return the string
	 */
	String build(TableProcessor owner);
}
