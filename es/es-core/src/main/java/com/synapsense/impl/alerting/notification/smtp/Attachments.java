package com.synapsense.impl.alerting.notification.smtp;

import com.synapsense.dto.TO;
import com.synapsense.exception.EnvException;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.exception.PropertyNotFoundException;
import com.synapsense.exception.UnableToConvertPropertyException;
import com.synapsense.service.Environment;
import com.synapsense.service.LocalizationService;
import com.synapsense.service.user.UserContext;
import com.synapsense.util.CollectionUtils;
import com.synapsense.util.Pair;
import org.apache.commons.mail.HtmlEmail;
import org.apache.log4j.Logger;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * Bundle of LiveImaging images with legends. Add one or several images, then
 * attach them to email
 * 
 * @author : shabanov
 */
public final class Attachments {

	private final static Logger logger = Logger.getLogger(Attachments.class);
	/**
	 * LiveImaging images, preserve order
	 */
	private Map<String, Pair<Attachment, String>> images = CollectionUtils.newLinkedMap();
	/**
	 * LiveImaging legends
	 */
	private Map<String, Attachment> legends = CollectionUtils.newMap();

	private final Environment environment;

	private final LocalizationService localizationService;
	/**
	 * context of the user which is recipient of this attachments bundle
	 */
	private final UserContext recipientContext;

	public Attachments(Attachments copy) {
		this.environment = copy.environment;
		this.localizationService = copy.localizationService;
		this.legends.putAll(copy.legends);
		this.images.putAll(copy.images);
		this.recipientContext = copy.recipientContext;
	}

	public Attachments(final UserContext recipientContext, Environment env, LocalizationService localizationService) {
		this.environment = env;
		this.localizationService = localizationService;
		this.recipientContext = recipientContext;
	}

	public boolean hasAttachment(TO<?> objId, ImageLayer layer, DataClass dataclass) {
		return images.containsKey(uniqueKey(objId, layer.getCode(), dataclass.getCode()));
	}

	/**
	 * Add new LiveImaging image attachment into bundle
	 * 
	 * @param imageOwner
	 *            Object owner of image
	 * @param alertTo
	 */
	public void attachLiveImage(final TO<?> imageOwner, final TO<?> alertTo, final int layerCode,
	        final int dataClassCode) {

		final DataClass dataClass = DataClass.decode(dataClassCode);
		if (dataClass == null) {
			logger.warn("Failed to attach image. Data class with code = " + dataClassCode + " is not supported");
			return;
		}

		List<ImageLayer> layersEncoded = ImageLayer.decode(layerCode);
		if (layersEncoded.isEmpty()) {
			logger.warn("Failed to attach image. There is no visual layers encoded in  " + layerCode);
			return;
		}

		for (final ImageLayer layer : layersEncoded) {
			if (hasAttachment(imageOwner, layer, dataClass)) {
				if (logger.isDebugEnabled()) {
					logger.debug("LiveImaging image on [" + imageOwner.toString() + ", layer: " + layer.getName()
					        + ", data class: " + dataClass.getName() + " has already been attached. Skipping...");
				}
				continue;
			}
			String imageUniqueId = uniqueKey(imageOwner, layer.getCode(), dataClass.getCode());

			// localize objects
			Locale callerLocale = recipientContext.getLocale();
			dataClass.setDisplayName(localizationService.getString(dataClass.getName(), callerLocale));
			layer.setDisplayName(localizationService.getString(layer.getName(), callerLocale));

            // FIXME : proof of concept !
			ImageDataSource liveImageDs = new ImageDataSource() {
				private ImageDataSource delegate = new LiveImageDataSource(environment, imageOwner, layer, dataClass);

				@Override
				public byte[] getImage() {
					byte[] imgArray = delegate.getImage();

					if (imgArray == null)
						return null;

					// image is needed for scaling algo
					BufferedImage img = null;
					try {
						img = ImageIO.read(new ByteArrayInputStream(imgArray));
					} catch (IOException e) {
						logger.warn("Cannot draw square area", e);
						return imgArray;
					}

					// if image owner has relative coord set we need to use them
					// in scaling algo
					Double imageOwnerX = 0.0;
					Double imageOwnerY = 0.0;
					try {
						imageOwnerX = environment.getPropertyValue(imageOwner, "x", Double.class);
						imageOwnerY = environment.getPropertyValue(imageOwner, "y", Double.class);
					} catch (EnvException e) {
						// means object has no own relative coord
					}

					Double targetY;
					Double targetX;
					Double width;
					Double height;
					try {
						// size of image area
						width = environment.getPropertyValue(imageOwner, "width", Double.class);
						height = environment.getPropertyValue(imageOwner, "height", Double.class);
						// location of alert
						targetX = environment.getPropertyValue(alertTo, "x", Double.class);
						targetY = environment.getPropertyValue(alertTo, "y", Double.class);
					} catch (EnvException e) {
						logger.warn("Cannot get x,y", e);
						return imgArray;
					}

					// scale alert location according to image area
					int scaledX = (targetX.intValue() - imageOwnerX.intValue()) * img.getWidth() / width.intValue();
					int scaledY = (targetY.intValue() - imageOwnerY.intValue()) * img.getHeight() / height.intValue();

					try {
						Graphics2D g = (Graphics2D) img.getGraphics();
						g.setColor(Color.RED);
						g.setStroke(new BasicStroke(3));
                        int squareSideLen = (1/100) * Math.max(img.getHeight(), img.getWidth());
						g.drawRect(scaledX, scaledY, squareSideLen, squareSideLen);
						ByteArrayOutputStream baos = new ByteArrayOutputStream();
						ImageIO.write(img, "png", baos);
						return baos.toByteArray();
					} catch (IOException e) {
						logger.warn("Cannot draw square area", e);
						return imgArray;
					}
				}

				@Override
				public String getFileName() {
					return delegate.getFileName();
				}

				@Override
				public String getType() {
					return delegate.getType();
				}

				@Override
				public String getDescription() {
					return delegate.getDescription();
				}
			};

			Attachment liveImage = new ImageAttachment(imageUniqueId, liveImageDs);

			if (!legends.containsKey(dataClass.toString())) {
				Attachment legend = new ImageAttachment(dataClass.toString(), new LiveLegendDataSource(environment,
				        dataClass, recipientContext.getUnitSystem()));
				legends.put(dataClass.toString(), legend);
			}

			Pair<Attachment, String> imageWithLegendRef = Pair.newPair(liveImage, dataClass.toString());

			images.put(imageUniqueId, imageWithLegendRef);
		}
	}

	/**
	 * Attach all added images to specified email
	 * 
	 * @param email
	 *            Email messages
	 * @return Hierarchical view of this bundle
	 */
	public Map<String, Object> putIntoEmail(HtmlEmail email) {
		// map can be hierarchical
		Map<String, Object> dataModel = CollectionUtils.newMap();

		List<Map<String, Object>> attachmentList = CollectionUtils.newList();
		dataModel.put("attachments", attachmentList);

		if (images.isEmpty()) {
			return dataModel;
		}

		Map<String, String> embeddedLegends = CollectionUtils.newMap();

		for (Attachment legend : this.legends.values()) {
			Attachment.InlinedAttachment embeddedLegendId = legend.inline(email);
			embeddedLegends.put(legend.getName(), embeddedLegendId.getMarkup());
		}

		for (Pair<Attachment, String> attachmentPair : this.images.values()) {
			Map<String, Object> attachmentAsMap = CollectionUtils.newMap();

			Attachment liveImage = attachmentPair.getFirst();
			attachmentAsMap.put("description", liveImage.getDescription());

			Attachment.InlinedAttachment embeddedLiveImageId = liveImage.inline(email);
			attachmentAsMap.put("img", embeddedLiveImageId.getMarkup());
			// recipient may have specific date format preferences
			String timestamp = recipientContext.getDateFormat().format(embeddedLiveImageId.createdAt());
			attachmentAsMap.put("timestamp", timestamp);
			attachmentAsMap.put("legend", embeddedLegends.get(attachmentPair.getSecond()));

			attachmentList.add(attachmentAsMap);
		}

		return dataModel;
	}

	private String uniqueKey(TO<?> objId, int layer, int dataclass) {
		return objId.toString() + layer + dataclass;
	}

}
