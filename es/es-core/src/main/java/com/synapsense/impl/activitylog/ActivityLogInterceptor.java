package com.synapsense.impl.activitylog;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Collection;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.SessionContext;
import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;

import org.apache.log4j.Logger;

import com.synapsense.dal.objectcache.CacheEnvironmentDispatcher;
import com.synapsense.dal.objectcache.CacheSvc;
import com.synapsense.impl.alerting.macros.ConversionMacro;
import com.synapsense.impl.alerting.macros.LocalizeMacro;
import com.synapsense.impl.alerting.macros.Macro;
import com.synapsense.impl.alerting.macros.MacroContext;
import com.synapsense.impl.alerting.macros.MacrosCollection;
import com.synapsense.service.Environment;
import com.synapsense.service.LocalizationService;
import com.synapsense.service.UnitSystemsService;
import com.synapsense.service.UserManagementService;
import com.synapsense.service.impl.activitylog.ActivityRecord;
import com.synapsense.service.user.CachedUserContext;
import com.synapsense.service.user.EnvUserContext;
import com.synapsense.service.user.UserContext;
import com.synapsense.util.ConvertingEnvironmentProxy;
import com.synapsense.util.unitconverter.UnitResolvers;
import com.synapsense.util.unitconverter.UnitSystems;

public class ActivityLogInterceptor {
	private final static Logger logger = Logger.getLogger(ActivityLogInterceptor.class);

	@AroundInvoke
	@SuppressWarnings("unchecked")
	public Object intercept(final InvocationContext icx) throws Exception {
		if (logger.isDebugEnabled()) {
			logger.debug("Entering interceptor : " + ActivityLogInterceptor.class.getName());
		}

		final Object result = icx.proceed();
		if (result == null)
			return result;

		// take returning collection
		final Collection<ActivityRecord> records;
		boolean isCollection;
		if (result instanceof Collection) {
			records = (Collection<ActivityRecord>) result;
			isCollection = true;
		} else {
			records = new ArrayList<ActivityRecord>();
			records.add((ActivityRecord) result);
			isCollection = false;
		}
		if (records.isEmpty()) {
			return result;
		}

		// find out that caller is not anonymous
		final Principal user = ctx.getCallerPrincipal();
		final String callerName = user.getName();
		final UserContext userCtx = new CachedUserContext(new EnvUserContext(callerName, userService));
		final String targetSystem = userCtx.getUnitSystem();
		// using caller's preferred unit system
		convertingEnv.setTargetSystem(targetSystem);

		macroCtx.usrCtx = userCtx;

		// replace full message
		for (final ActivityRecord record : records) {

			String description = record.getDescription();
			record.setDescription(macro.expandIn(description, macroCtx));
		}

		if (logger.isDebugEnabled()) {
			logger.debug("Exitting interceptor : " + ActivityLogInterceptor.class.getName());
		}

		return isCollection ? records : records.iterator().next();
	}

	private void checkDependencies() {
		if(cache == null || genericEnv == null ||
				unitSystemService == null || localizationService == null)
			return;
		
		logger.info("interceptor init ... ");
		final Environment cachedEnv = CacheEnvironmentDispatcher.getEnvironment(cache, genericEnv);

		final UnitSystems unitSystems = unitSystemService.getUnitSystems();
		final UnitResolvers unitRes = unitSystemService.getUnitReslovers();

		convertingEnv = new ConvertingEnvironmentProxy();

		convertingEnv.setUnitSystems(unitSystems);
		convertingEnv.setUnitResolvers(unitRes);
		convertingEnv.setProxiedEnvironment(cachedEnv);

		Collection<Macro> macros = new ArrayList<Macro>(2);
		macros.add(new ConversionMacro());
		macros.add(new LocalizeMacro());
		macro = new MacrosCollection(macros);

		macroCtx = new MacroContext();
		macroCtx.env = cachedEnv;
		macroCtx.convEnv = convertingEnv;
		macroCtx.unitSystems = unitSystems;
		macroCtx.localizationService = localizationService;
		logger.info("interceptor init is done");
	}

	@Resource
	void setCtx(final SessionContext ctx) {
		this.ctx = ctx;
	}

	@EJB
	void setCache(final CacheSvc cache) {
		this.cache = cache;
		checkDependencies();
	}

	@EJB
	void setUnitSystemService(final UnitSystemsService unitSystemService) {
		this.unitSystemService = unitSystemService;
		checkDependencies();
	}

	@EJB(beanName = "LocalUserManagementService")
	void setUserService(final UserManagementService userService) {
		this.userService = userService;
	}

	@EJB(beanName = "GenericEnvironment")
	void setGenericEnv(final Environment genericEnv) {
		this.genericEnv = genericEnv;
		checkDependencies();
	}

	@EJB
	void setLocalizationService(final LocalizationService localizationService) {
		this.localizationService = localizationService;
		checkDependencies();
	}

	private SessionContext ctx;

	private ConvertingEnvironmentProxy convertingEnv;

	private LocalizationService localizationService;

	private CacheSvc cache;

	private UnitSystemsService unitSystemService;

	private UserManagementService userService;

	private Environment genericEnv;

	private Macro macro;

	private MacroContext macroCtx;

}
