
package com.synapsense.impl.erlnode.utils;

import java.util.ArrayList;
import java.util.Collection;

import com.ericsson.otp.erlang.OtpErlangAtom;
import com.ericsson.otp.erlang.OtpErlangList;
import com.ericsson.otp.erlang.OtpErlangObject;
import com.ericsson.otp.erlang.OtpErlangBinary;
import com.ericsson.otp.erlang.OtpErlangTuple;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;

import com.synapsense.impl.erlnode.utils.StringConverter;

/**
 * @author stikhon
 *
 */
public class ToConverter {
	public final static OtpErlangAtom toAtom = new OtpErlangAtom("to");
	private static final OtpErlangAtom undefined =
		new OtpErlangAtom("undefined");

	public static TO<?> parseTo(OtpErlangTuple to) {
		if (to == null || to.arity() != 2
				|| !toAtom.equals(to.elementAt(0))) {
			throw new IllegalArgumentException();
		}

		OtpErlangObject tmp = to.elementAt(1);
		if(undefined.equals(tmp))
			return null;

		String toData = StringConverter.utf8BinaryToString(tmp);
		return TOFactory.getInstance().loadTO(toData);
	}

	public static OtpErlangTuple serializeTo(TO<?> to) {
		if (to == null) {
			throw new IllegalArgumentException();
		}
		return new OtpErlangTuple(new OtpErlangObject[] {
				toAtom,
				StringConverter.stringToUtf8Binary(TOFactory.getInstance().saveTO(to)) });
	}

	public static OtpErlangList serializeToList(Collection<TO<?>> list) {
		if (list == null) {
			throw new IllegalArgumentException();
		}
		ArrayList<OtpErlangTuple> objectsList = new ArrayList<OtpErlangTuple>();
		for (TO<?> to : list) {
			objectsList.add(serializeTo(to));
		}
		return new OtpErlangList(objectsList
				.toArray(new OtpErlangObject[objectsList.size()]));
	}

	public static Collection<TO<?>> parseToList(OtpErlangList list) {
		if (list == null) {
			throw new IllegalArgumentException();
		}
		int size = list.arity();
		Collection<TO<?>> tos = new ArrayList<TO<?>>(size);
		for (int i = 0; i < size; i++) {
			tos.add(parseTo((OtpErlangTuple) list.elementAt(i)));
		}
		return tos;
	}
}
