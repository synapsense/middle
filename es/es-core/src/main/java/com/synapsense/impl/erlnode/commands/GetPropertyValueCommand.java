
package com.synapsense.impl.erlnode.commands;

import java.util.Map;

import com.ericsson.otp.erlang.OtpErlangBinary;
import com.ericsson.otp.erlang.OtpErlangDouble;
import com.ericsson.otp.erlang.OtpErlangInt;
import com.ericsson.otp.erlang.OtpErlangLong;
import com.ericsson.otp.erlang.OtpErlangObject;
import com.ericsson.otp.erlang.OtpErlangTuple;
import com.synapsense.impl.erlnode.ExceptionTypes;
import com.synapsense.impl.erlnode.utils.ToConverter;
import com.synapsense.dto.ObjectType;
import com.synapsense.dto.PropertyDescr;
import com.synapsense.dto.TO;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.exception.PropertyNotFoundException;

import com.synapsense.impl.erlnode.utils.StringConverter;

public class GetPropertyValueCommand extends Command {

	public GetPropertyValueCommand(Map<String, Object> state) {
		super(state);
	}

	public OtpErlangObject do_command(OtpErlangTuple command) throws Exception {
		OtpErlangObject toTuple = command.elementAt(1);
		OtpErlangObject typeNameTuple = command.elementAt(2);
		if (toTuple == null || typeNameTuple == null
				|| !(toTuple instanceof OtpErlangTuple)
				|| !(typeNameTuple instanceof OtpErlangBinary)) {
			return makeException(ExceptionTypes.ILLEGAL_ARGUMENTS, null);
		}

		TO<?> objId = ToConverter.parseTo((OtpErlangTuple) toTuple);
		String propertyName = StringConverter.utf8BinaryToString(typeNameTuple);
		ObjectType oType = getEnv().getObjectType(objId.getTypeName());
		if (oType == null)
			return makeException(ExceptionTypes.NO_OBJECT_TYPE, objId
					.getTypeName());

		java.lang.Class<?> type = getPropertyType(oType, propertyName);
		try {
			if (type == String.class)
				return makeOkResult(StringConverter.stringToUtf8Binary(getEnv()
						.getPropertyValue(objId, propertyName, String.class)));
			else if (type == Integer.class)
				return makeOkResult(new OtpErlangInt(getEnv().getPropertyValue(
						objId, propertyName, Integer.class)));
			else if (type == Long.class)
				return makeOkResult(new OtpErlangLong(getEnv()
						.getPropertyValue(objId, propertyName, Long.class)));
			else if (type == Double.class)
				return makeOkResult(new OtpErlangDouble(getEnv()
						.getPropertyValue(objId, propertyName, Double.class)));
			else if (type == TO.class)
				return makeOkResult(ToConverter.serializeTo(getEnv()
						.getPropertyValue(objId, propertyName, TO.class)));
			else if (type == null)
				return makeException(ExceptionTypes.PROPERTY_NOT_FOUND,
						propertyName);
			else
				// type == null
				return makeException(ExceptionTypes.BAD_TYPE, propertyName);
		} catch (ObjectNotFoundException e) {
			return makeException(ExceptionTypes.OBJECT_NOT_FOUND, e
					.getMessage());
		} catch (PropertyNotFoundException e) {
			return makeException(ExceptionTypes.PROPERTY_NOT_FOUND, e
					.getMessage());
		}
	}

	public java.lang.Class<?> getPropertyType(ObjectType oType,
			String propertyName) {
		for (PropertyDescr pd : oType.getPropertyDescriptors()) {
			if (pd.getName().equals(propertyName)) {
				return pd.getType();
			}
		}
		return null;
	}
}
