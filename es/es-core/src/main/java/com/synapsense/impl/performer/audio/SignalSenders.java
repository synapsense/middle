package com.synapsense.impl.performer.audio;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

import com.synapsense.impl.performer.snmp.conf.SNMPConfigurationException;
import com.synapsense.service.AlertService;

public class SignalSenders {
	private final Map<String, SignalSender> senders;
	private AlertService alertService;
	ScheduledExecutorService executorService;

	public SignalSenders() {
		senders = new HashMap<String, SignalSender>();
		executorService = Executors.newSingleThreadScheduledExecutor();
	}

	public synchronized SignalSender getSignalSender(String destination) throws SNMPConfigurationException {
		SignalSender sender = senders.get(destination);
		if (sender == null) {
			sender = new SignalSender(destination, alertService, executorService);
			senders.put(destination, sender);

		}
		return sender;
	}

	public void clear() {
		for (SignalSender towerConnection : senders.values()) {
			towerConnection.close();
		}
		senders.clear();
	}

	protected void setAlertService(AlertService service) {
		this.alertService = service;

	}
}
