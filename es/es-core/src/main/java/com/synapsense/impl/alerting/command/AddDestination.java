package com.synapsense.impl.alerting.command;

import static com.synapsense.impl.alerting.AlertConstants.TYPE.ALERT_DESTINATION_TYPE_NAME;

import org.apache.log4j.Logger;

import com.synapsense.commandprocessor.api.Command;
import com.synapsense.dto.MessageType;
import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.AlertServiceSystemException;
import com.synapsense.exception.EnvException;
import com.synapsense.impl.alerting.Utils;
import com.synapsense.service.Environment;
import com.synapsense.service.UserManagementService;
 
public class AddDestination  implements Command {

	private final static Logger logger = Logger.getLogger(AddDestination.class);

	private Environment env;
	private String alertTypeName;
	private UserManagementService userService;
	private String destination;
	private MessageType messageType;
	private String tierName;

	public AddDestination(Environment env, UserManagementService userService, String alertTypeName, String destination,
            MessageType messageType, String tierName) {
	    this.env = env;
	    this.alertTypeName = alertTypeName;
	    this.userService = userService;
	    this.destination = destination;
	    this.messageType = messageType;
	    this.tierName = tierName;
    }

	@Override
    public void execute() {
		TO<?> alertTypeRef = Utils.findAlertTypeByName(env, alertTypeName);
		if (alertTypeRef == null) {
			return;
			// FIXME : was like throw new NoSuchAlertTypeException(alertTypeName);
		}

		TO<?> userAccount = userService.getUser(destination);
		if (userAccount == null) {
			if (logger.isDebugEnabled()) {
				logger.debug("User with name : " + destination + " does not exist");
			}
		} else {
			if (MessageType.SNMP.equals(messageType)) {
				throw new AlertServiceSystemException("User cannot have " + MessageType.SNMP + " destinations");
			}
		}

		if (!Utils.existTier(env, alertTypeRef, tierName)) {
			throw new AlertServiceSystemException("Priority tier : " + tierName
			        + " cannot be determined for alert type : " + alertTypeRef);
		}

		final TO<?> destinationRef = Utils.findDestination(env, alertTypeRef, tierName, destination, messageType);
		if (destinationRef == null) {
			ValueTO[] destValues = new ValueTO[] { new ValueTO("alertType", alertTypeRef),
			        new ValueTO("tier", tierName), new ValueTO("destination", destination),
			        new ValueTO("messagetype", messageType.code()) };
			try {
				TO<?> destinationID = env.createObject(ALERT_DESTINATION_TYPE_NAME, destValues);
				if (logger.isDebugEnabled()) {
					logger.debug("Created new destination : " + destination + ", refered by : " + destinationID);
				}
			} catch (EnvException e) {
				logger.warn("Failed to create new destination instance : " + destination, e);
			}
		} else {
			if (logger.isDebugEnabled()) {
				logger.debug("Alert type : " + alertTypeName + ", tier : " + tierName
				        + " already associated with destination : " + destination);
			}
		}
	    
    }

}
