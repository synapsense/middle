package com.synapsense.impl.alerting.command;

import com.synapsense.commandprocessor.api.Command;
import com.synapsense.dto.*;
import com.synapsense.exception.AlertServiceSystemException;
import com.synapsense.exception.EnvException;
import com.synapsense.exception.IllegalInputParameterException;
import com.synapsense.impl.activitylog.Activity;
import com.synapsense.impl.alerting.AlertDismissor;
import com.synapsense.impl.alerting.AlertServiceImpl;
import com.synapsense.impl.alerting.Cache;
import com.synapsense.impl.alerting.Utils;
import com.synapsense.search.Direction;
import com.synapsense.search.Path;
import com.synapsense.search.QueryDescription;
import com.synapsense.search.Relations;
import com.synapsense.service.AlertService;
import com.synapsense.service.Environment;
import com.synapsense.service.impl.activitylog.ActivityLogService;
import com.synapsense.service.impl.activitylog.ActivityRecord;
import com.synapsense.util.CollectionUtils.Functor;
import org.apache.log4j.Logger;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import static com.synapsense.impl.alerting.AlertConstants.*;
import static com.synapsense.impl.alerting.AlertConstants.TYPE.ALERT_TYPE_NAME;
import static com.synapsense.impl.alerting.AlertConstants.TYPE.UNKNOWN_ALERT_TYPE_NAME;

public class RaiseAlert implements Command {

	private final static Logger logger = Logger.getLogger(RaiseAlert.class);

	private final Alert alert;
    private volatile TO<?> alertTo;

	private final Cache<Alert, TO<?>> alertStore;

    private final long alertTime;

	private final ActivityLogService activityLogService;

	private final AlertDismissor alertDismissor;

	private final Environment env;

	private final Functor<List<ValueTO>, Alert> converter;

	private final AlertService alertService;

	public RaiseAlert(Environment env, AlertService alertService, Cache<Alert, TO<?>> alertStore,
	        AlertDismissor alertDismissor, ActivityLogService activityLogService, Alert alert, long alertTime,
	        Functor<List<ValueTO>, Alert> converter) {
		if (alert == null) {
			throw new IllegalInputParameterException("Supplied 'alert' is null");
		}

		this.env = env;
		this.alertService = alertService;
		this.alertDismissor = alertDismissor;
		this.activityLogService = activityLogService;
		this.alert = alert;
		this.alertStore = alertStore;
		this.alertTime = alertTime;
		this.converter = converter;
        // set ID if in a cache
		this.alertTo = alertStore.get(alert);
	}

	@Override
	public void execute() {
		if (logger.isDebugEnabled()) {
			logger.debug("Raising alert... Alert initial data:" + alert.toString());
		}

		// help to GC
		if (isRaised()) {
			// just reschedule
			scheduleAutoDismiss();
			return;
		}

		String alertTypeName = alert.getAlertType();
		TypeInfo alertTypeDescriptor = findActiveAlertType(alertTypeName);
		if (alertTypeDescriptor == null) {
			logger.warn("Failed to raise alert : " + alert + " of type : " + alertTypeName
			        + " because such alert type does not exist as well as special purpose type : "
			        + UNKNOWN_ALERT_TYPE_NAME);
			return;
		}

		if (!alertTypeDescriptor.active) {
			if (logger.isDebugEnabled()) {
				logger.debug("Failed to raise alert due to alert type [" + alertTypeName + "] is disabled");
			}
			return;
		}

		alert.setAlertTime(alertTime);
		alert.setAlertType(alertTypeDescriptor.name);
		// raising alert is always opened
		alert.setStatus(AlertStatus.OPENED);
		// except auto ack option is chosen
		if (alertTypeDescriptor.autoAck) {
			alert.setStatus(AlertStatus.ACKNOWLEDGED);
			alert.setAcknowledgement(AUTO_ACKNOWLEDGED_BY_SYSTEM_COMMENT);
			alert.setAcknowledgementTime(new Date(alert.getAlertTime()));
			alert.setUser(SYSTEM_ACTIVITY_IDENTITY);
		}

        // one more check of local cache
		TO<?> newAlert = alertStore.get(alert);

		boolean createdNewAlert = false;
		if (newAlert == null) {
			newAlert = persistAlert(alert);
			alertStore.put(alert, newAlert);
			createdNewAlert = true;
		}

		if (createdNewAlert) {
			// notify that the new alert was raised
			if (alertTypeDescriptor.autoAck) {
				logAcknowledmentActivity(newAlert);
			}
		} else {
			if (logger.isDebugEnabled()) {
				logger.debug("Alert[" + alert.toString() + "] already exists and has ID=[" + newAlert + "]");
			}
		}

        this.alertTo = newAlert;

		// schedule or reschedule auto dismission
		if (alertTypeDescriptor.autoDismiss) {
			scheduleAutoDismiss();
		}
	}

	public boolean isRaised() {
		return alertTo != null;
	}

	private long getAlertPriorityDissmissInterval(TO<?> alert) throws EnvException {
		QueryDescription qd = new QueryDescription(2);
		qd.startQueryFrom(alert);
		qd.addPath(1, new Path(Relations.named("type"), Direction.OUTGOING, false));
		qd.addPath(2, new Path(Relations.named("priority"), Direction.OUTGOING));
		Collection<TO<?>> priorities = env.getObjects(qd);
		if (priorities.isEmpty()) {
			return -1;
		}
		TO<?> priority = priorities.iterator().next();
		return env.getPropertyValue(priority, "dismissInterval", Long.class);
	}

	// private members

	private class TypeInfo {
		String name;
		boolean autoAck;
		boolean autoDismiss;
		boolean active;
	}

	public void scheduleAutoDismiss() {
		try {
			long interval = getAlertPriorityDissmissInterval(alertTo);
			if (interval > 0) {
				alertDismissor.scheduleAlertDismission(alertTo, interval);
			} else {
				logger.warn("Alert " + alertTo
				        + "'s type has no priority association. auto dismiss will not be rescheduled");
			}
		} catch (EnvException e) {
			logger.warn("Failed to schedule auto dismiss of alert " + alertTo + " due to", e);
		}
	}

	private TypeInfo findActiveAlertType(String alertTypeName) {
		TO<?> alertTypeRef = Utils.findAlertTypeByName(env, alertTypeName);
		// if specified alert type
		if (alertTypeRef == null) {
			alertTypeRef = Utils.findAlertTypeByName(env, UNKNOWN_ALERT_TYPE_NAME);
		}

		if (alertTypeRef == null) {
			throw new AlertServiceSystemException("Either " + alertTypeName
			        + " or UNKNOWN  alert types  must be defined");
		}

		Collection<CollectionTO> typeInfo = env.getPropertyValue(Arrays.<TO<?>> asList(alertTypeRef), new String[] {
		        "name", "active", "autoAck", "autoDismiss" });
		if (typeInfo.isEmpty()) {
			return null;
		}

		CollectionTO alertTypeInfo = typeInfo.iterator().next();

		TypeInfo result = new TypeInfo();
		result.name = (String) alertTypeInfo.getSinglePropValue("name").getValue();
		result.autoAck = (Integer) alertTypeInfo.getSinglePropValue("autoAck").getValue() == 1;
		result.autoDismiss = (Integer) alertTypeInfo.getSinglePropValue("autoDismiss").getValue() == 1;
		result.active = (Integer) alertTypeInfo.getSinglePropValue("active").getValue() == 1;

		return result;
	}

	/**
	 * Saves an alert event to storage.
	 * 
	 * @param alert
	 *            alert event (DTO Alert)
	 * @throws AlertServiceSystemException
	 *             if problem occurred while saving alert to db
	 */
	private TO<?> persistAlert(final Alert alert) {
		if (logger.isDebugEnabled()) {
			logger.debug("Persisting alert [" + alert.getName() + "]...");
		}

		try {
			final List<ValueTO> values = converter.apply(alert);
			TO<?> ID = env.createObject(ALERT_TYPE_NAME, values.toArray(new ValueTO[values.size()]));
			if (logger.isDebugEnabled()) {
				logger.debug("Persisted new alert [" + ID + "] of type : " + alert.getAlertType());
			}
			return ID;
		} catch (final EnvException e) {
			throw new AlertServiceSystemException("Error occurred while persisting alert :" + alert.toString(), e);
		}

	}

	// activity logger for local calls(when out of scope of EJB injection)
	private void logMethodCall(Method callingMethod, final int alertId) {
		Object[] params = new Object[2];
		params[0] = alertId;
		params[1] = SYSTEM_ACTIVITY_MESSAGE;
		ActivityRecord logRecord = new Activity().log(SYSTEM_ACTIVITY_IDENTITY, alertService, callingMethod, params);
		if (logRecord != null) {
			activityLogService.addRecord(logRecord);
		}
	}

	private void logAcknowledmentActivity(TO<?> alert) {
		try {
			Method method = AlertServiceImpl.class.getMethod("acknowledgeAlert", Integer.class, String.class);
			logMethodCall(method, (Integer) alert.getID());
		} catch (Exception e) {
			logger.warn("Alert service activity cannot be logged due to ", e);
		}
	}

}
