package com.synapsense.impl.alerting.macros;

import java.util.regex.Pattern;

public abstract class AbstractDataValueMacro extends AbstractPatternMacro {

	private static final Pattern pattern = Pattern
	        .compile("\\$(?i)data\\((\\w+:\\d+\\.(?:\\w+\\.?){1,}),(-?\\d+(?:\\.\\d+)?)(?:,([ \\w]+))?\\)\\$");

	@Override
	protected Pattern getPattern() {
		return pattern;
	}

}
