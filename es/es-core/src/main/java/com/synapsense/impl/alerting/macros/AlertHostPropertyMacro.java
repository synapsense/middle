package com.synapsense.impl.alerting.macros;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;

public class AlertHostPropertyMacro extends AbstractPatternMacro {

	private static final Pattern pattern = Pattern.compile("\\$(\\w+)((\\.\\w+){1,})?\\$");

	@Override
	public String expandIn(String input, MacroContext ctx) {
		if (input == null)
			return null;
		if (ctx.alert == null)
			throw new IllegalArgumentException("MacroContext.alert must be not null");
		if (ctx.env == null)
			throw new IllegalArgumentException("MacroContext.env must be not null");
		return super.expandIn(input, ctx);
	}

	@Override
	protected String expand(Matcher m, MacroContext ctx) {
		final TO<?> tempHost = ctx.alert.getHostTO();
		if (TOFactory.EMPTY_TO.equals(tempHost)) {
			return Matcher.quoteReplacement(Macro.DEFAULT_EXPANSION);
		}
		// now let's pretend it is EnvPropertyMacro
		String macro = m.group().replace("$", "");
		String newMacro = "$" + TOFactory.getInstance().saveTO(tempHost) + "." + macro + "$";
		return new EnvPropertyMacro().expandIn(newMacro, ctx);
	}

	@Override
	protected Pattern getPattern() {
		return pattern;
	}

}
