package com.synapsense.impl.dlimport;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.log4j.Logger;
import org.w3c.dom.DOMConfiguration;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSOutput;
import org.w3c.dom.ls.LSSerializer;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class XMLHelper {
	private static final Logger log = Logger.getLogger(XMLHelper.class);

	/**
	 * Retrieves the string version of an XML document
	 * 
	 * @param xmlDoc
	 * @return
	 */
	public static String getDocumentXml(Document xmlDoc) {
		DOMImplementation domImplementation = xmlDoc.getImplementation();
		if (domImplementation.hasFeature("LS", "3.0") && domImplementation.hasFeature("Core", "2.0")) {
			DOMImplementationLS domImplementationLS = (DOMImplementationLS) domImplementation.getFeature("LS", "3.0");
			LSSerializer lsSerializer = domImplementationLS.createLSSerializer();
			DOMConfiguration domConfiguration = lsSerializer.getDomConfig();
			if (domConfiguration.canSetParameter("format-pretty-print", Boolean.TRUE)) {
				lsSerializer.getDomConfig().setParameter("format-pretty-print", Boolean.TRUE);
				LSOutput lsOutput = domImplementationLS.createLSOutput();
				lsOutput.setEncoding("UTF-8");
				Writer stringWriter = new StringWriter();
				lsOutput.setCharacterStream(stringWriter);
				lsSerializer.write(xmlDoc, lsOutput);
				return stringWriter.toString();
			} else {
				// throw new
				// RuntimeException("DOMConfiguration 'format-pretty-print' parameter isn't settable.");
			}
		} else {
			// throw new
			// RuntimeException("DOM 3.0 LS and/or DOM 2.0 Core not supported.");
		}
		// set up a transformer
		try {
			TransformerFactory transfac = TransformerFactory.newInstance();
			Transformer trans = transfac.newTransformer();
			trans.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
			trans.setOutputProperty(OutputKeys.INDENT, "yes");

			// create string from xml tree
			StringWriter sw = new StringWriter();
			// sw.write("<foo>bar</foo>");
			StreamResult result = new StreamResult(sw);
			DOMSource source = new DOMSource(xmlDoc);

			if (source == null || result == null || trans == null) {
				log.warn("PROBLEM FOOL...");
			}
			trans.transform(source, result);
			String xmlString = sw.toString();

			return xmlString;
		} catch (TransformerConfigurationException tce) {
			log.error("getDocumentXml()", tce);
			return null;
		} catch (TransformerException te) {
			log.error("getDocumentXml()", te);
			return null;
		}
	}

	public static String getAttribute(Node node, String attribute) {
		Node attrNode = node.getAttributes().getNamedItem(attribute);
		if (attrNode == null) {
			return "";
		}
		return attrNode.getNodeValue();
	}

	public static String transformXmlToHtml(String xslUrl, Document xmlDoc, OutputStream os) {
		String trxOutput = null;

		try {

			// JAXP reads data using the Source interface
			String xmlString = getDocumentXml(xmlDoc);
			Source xmlSource = new StreamSource(new StringReader(xmlString));
			Source xsltSource = new StreamSource(new File(xslUrl));

			TransformerFactory tFactory = TransformerFactory.newInstance();
			Transformer transformer = tFactory.newTransformer(xsltSource);

			transformer.transform(xmlSource, new StreamResult(os));
		} catch (Exception e) {
			log.error("TransformXmlToHtml()", e);
		}

		return trxOutput;

	}

	public static void appendAttribute(Node node, String name, String value) {
		node.getAttributes().setNamedItem(node.getOwnerDocument().createAttribute(name));
		node.getAttributes().getNamedItem(name).setNodeValue(value);
	}

	public static Document createNewXmlDocument(String rootNodeName) throws ParserConfigurationException {

		// Create the XML Document
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setValidating(true);
		DocumentBuilder docBuilder = factory.newDocumentBuilder();
		Document xmlDoc = docBuilder.newDocument();

		Node rootNode = xmlDoc.createElement(rootNodeName);
		xmlDoc.appendChild(rootNode);

		return xmlDoc;
	}

	public static Document createNewXmlDocumentFromNode(Node fromNode) throws ParserConfigurationException {

		// Create the XML Document
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setValidating(true);
		DocumentBuilder docBuilder = factory.newDocumentBuilder();
		Document xmlDoc = docBuilder.newDocument();

		Node rootNode = xmlDoc.importNode(fromNode, true);
		xmlDoc.appendChild(rootNode);

		return xmlDoc;
	}

	public static Node selectSingleXmlNode(Document xmlDoc, String xPath) {
		NodeList nodeList = xmlDoc.getElementsByTagName(xPath);
		Node returnedNode = null;
		if (nodeList != null) {
			if (nodeList.getLength() != 0) {
				returnedNode = nodeList.item(0);
			}
		}
		return returnedNode;
	}

	public static Node selectSingleXmlNode(Node node, String xPath) {
		Element nodeElem = (Element) node;
		NodeList nodeList = nodeElem.getElementsByTagName(xPath);
		Node returnedNode = null;
		if (nodeList != null) {
			if (nodeList.getLength() != 0) {
				returnedNode = nodeList.item(0);
			}
		}
		return returnedNode;
	}

	public static String getXmlNodeTextValue(Node node) {

		String returnedVal = null;
		if (node.getNodeType() == Node.TEXT_NODE) {
			Node textNode = node.getFirstChild();
			returnedVal = textNode.getNodeValue();
		}

		return returnedVal;
	}

	@SuppressWarnings("rawtypes")
	public static Node createXmlNode(Document doc, String nodeName, Map map) {
		Node root = doc.createElement(nodeName);

		Set entries = map.entrySet();
		Iterator it = entries.iterator();

		while (it.hasNext()) {
			Map.Entry entry = (Map.Entry) it.next();

			Node nameNode = doc.createElement((String) (entry.getKey()));
			Node valueNode = null;
			Object value = entry.getValue();

			if (value instanceof Map) {
				root.appendChild(createXmlNode(doc, "entity", (Map) value));
				continue;
			} else if (value instanceof String) {
				valueNode = doc.createTextNode((String) (entry.getValue()));
			} else {
				log.warn((String) entry.getKey());
				log.warn(entry.getValue());
				log.warn("NOT YET IMPLEMENTED@");
			}

			nameNode.appendChild(valueNode);
			root.appendChild(nameNode);
		}
		return root;
	}

	/**
	 * Return org.w3c.dom.Node object. If value is null - it is considered as
	 * empty string ("") to avoid exceptions while XML transformations.
	 * 
	 * @param doc
	 * @param name
	 * @param value
	 * @return
	 */

	public static Node createXmlTextNode(Document doc, String name, String value) {
		Node root = doc.createElement(name);
		root.appendChild(doc.createTextNode((value != null) ? value : ""));
		return root;
	}

	public static Element createXmlNodeWithValue(Document doc, String name, String value) {
		Element root = doc.createElement(name);
		root.setAttribute("v", (value != null) ? value : "");
		return root;
	}

	public static Document loadXmlFromFile(String fileAbsolutePath) throws IOException {
		Document xmlDoc = null;
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			xmlDoc = builder.parse(new FileInputStream(fileAbsolutePath));
		} catch (ParserConfigurationException pcE) {
			log.error(pcE.getMessage());
		} catch (SAXException sE) {
			log.error(sE.getMessage());
		}

		return xmlDoc;
	}

	public static Document loadXmlFromByteArray(byte[] buf) throws IOException {
		Document xmlDoc = null;
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			xmlDoc = builder.parse(new ByteArrayInputStream(buf));
		} catch (ParserConfigurationException pcE) {
			log.error(pcE.getMessage());
		} catch (SAXException sE) {
			log.error(sE.getMessage());
		}

		return xmlDoc;
	}

	public static Document loadXmlFromStream(InputStream is) throws IOException {
		Document xmlDoc = null;
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			xmlDoc = builder.parse(is);
		} catch (ParserConfigurationException pcE) {
			log.error(pcE.getMessage());
		} catch (SAXException sE) {
			log.error(sE.getMessage());
		}

		return xmlDoc;
	}

	public static String getErrorXML(int code, String error) {
		String errorXML = "<error>";
		errorXML += "<code>" + code + "</code>";
		errorXML += "<info>" + error + "</info>";
		errorXML += "</error>";
		return errorXML;
	}

	public static Document getErrorDoc(int code, String info) {
		Document doc = null;
		try {
			doc = XMLHelper.createNewXmlDocument("error");
			Element root = doc.getDocumentElement();
			root.appendChild(XMLHelper.createXmlTextNode(doc, "info", info));
			root.appendChild(XMLHelper.createXmlTextNode(doc, "code", Integer.toString(code)));
		} catch (ParserConfigurationException e) {
			log.error(e.getLocalizedMessage(), e);
		}

		return doc;
	}

	public static Document getSuccessDoc(String info) {
		Document doc = null;
		try {
			doc = XMLHelper.createNewXmlDocument("success");
			Element root = doc.getDocumentElement();
			root.appendChild(XMLHelper.createXmlTextNode(doc, "info", info));
		} catch (ParserConfigurationException e) {
			log.error(e.getLocalizedMessage(), e);
		}

		return doc;
	}

	public static Document getDocument(String xmlFile) {
		Document document = null;
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			factory.setValidating(false);

			DocumentBuilder builder = null;

			builder = factory.newDocumentBuilder();

			builder.setEntityResolver(new EntityResolver() {

				@Override
				public InputSource resolveEntity(String publicId, String systemId) throws SAXException, IOException {
					return new InputSource(new StringReader(""));
				}
			});
			document = builder.parse(new ByteArrayInputStream(xmlFile.getBytes()));
			return document;
		} catch (IOException e) {
			log.error(e.getMessage(), e);
		} catch (SAXException e) {
			log.error(e.getMessage(), e);
		} catch (ParserConfigurationException e) {
			log.error(e.getMessage(), e);
		}
		return null;
	}

	public static String getXPathString(Node xmlNode, String xPathString) {
		// As xPathExpression.evaluate() performance sucks
		// try to evaluate it manually

		String[] tokens = xPathString.split("/");
		if (tokens != null && tokens.length == 2 && "text()".equals(tokens[1]) && !"*".equals(tokens[0])) {
			NodeList values = ((Element) xmlNode).getElementsByTagName(tokens[0]);
			if (values != null && values.getLength() > 0) {
				return values.item(0).getTextContent();
			}
			return null;
		} else {
			XPathFactory factoryX = XPathFactory.newInstance();
			XPath xPath = factoryX.newXPath();
			try {
				XPathExpression xPathExpression = xPath.compile(xPathString);
				// removal of the node magically speed-up xpath query
				Node parent = xmlNode.getParentNode();
				if (parent != null) {
					parent.removeChild(xmlNode);
				}
				String res;
				res = (String) xPathExpression.evaluate(xmlNode, XPathConstants.STRING);
				if (parent != null) {
					parent.appendChild(xmlNode);
				}
				return res;
			} catch (XPathExpressionException e) {
				// TODO:
				throw new RuntimeException(e.getLocalizedMessage(), e);
			}
		}
	}

	public static Set<String> getXPathStringSet(Node xmlNode, String xPathString) {
		String[] tokens = xPathString.split("/");
		Set<String> set = new HashSet<String>();
		if (tokens != null && tokens.length == 2 && "text()".equals(tokens[1]) && !"*".equals(tokens[0])) {
			NodeList values = ((Element) xmlNode).getElementsByTagName(tokens[0]);
			if (values != null) {
				for (int i = 0; i < values.getLength(); i++) {
					set.add(values.item(i).getTextContent());
				}
			}
		} else {
			XPathFactory factoryX = XPathFactory.newInstance();
			XPath xPath = factoryX.newXPath();
			XPathExpression xPathExpression;
			try {
				xPathExpression = xPath.compile(xPathString);

				// removal of the node magically speed-up xpath query
				Node parent = xmlNode.getParentNode();
				if (parent != null) {
					parent.removeChild(xmlNode);
				}
				NodeList nodes = (NodeList) xPathExpression.evaluate(xmlNode, XPathConstants.NODESET);
				if (parent != null) {
					parent.appendChild(xmlNode);
				}

				for (int j = 0; j < nodes.getLength(); j++) {
					set.add(nodes.item(j).getTextContent());
				}
			} catch (XPathExpressionException e) {
				// TODO:
				throw new RuntimeException(e.getLocalizedMessage(), e);
			}
		}

		return set;
	}

	public static NodeList getXPathNodeList(Node xmlNode, String xPathString) throws XPathExpressionException {
		XPathFactory factoryX = XPathFactory.newInstance();
		XPath xPath = factoryX.newXPath();
		XPathExpression xPathExpression = xPath.compile(xPathString);
		return (NodeList) xPathExpression.evaluate(xmlNode, XPathConstants.NODESET);
	}

	public static Collection<Node> getXPathNodes(Node xmlNode, String xPathString) {
		String[] tokens = xPathString.split("/");
		Collection<Node> result = new LinkedList<Node>();

		if (tokens != null) {
			String tagName = tokens[0];
			NodeList children = xmlNode.getChildNodes();
			for (int j = 0; j < children.getLength(); j++) {
				Node child = children.item(j);

				if (!(child instanceof Element)
				        || (!"*".equals(tagName) && !((Element) child).getTagName().equals(tagName))) {
					continue;
				}

				String subXPath = getSubXPath(xPathString);
				if (subXPath == null) {
					result.add(child);
				} else {
					result.addAll(getXPathNodes(child, subXPath));
				}
			}
		}
		return result;
	}

	public static String getSubXPath(String xPathString) {
		String[] tokens = xPathString.split("/");
		StringBuffer res = null;
		if (tokens.length > 1) {
			res = new StringBuffer();
			res.append(tokens[1]);
			for (int i = 2; i < tokens.length; i++) {
				res.append("/").append(tokens[i]);
			}
		}

		return res == null ? null : res.toString();
	}
}
