package com.synapsense.impl.alerting.macros;

import java.util.regex.Pattern;

public class MessageTierMacro extends ContextPropertyMacro {

	public static final String MSG_TIER = "MESSAGE_TIER";

	public MessageTierMacro() {
		super(Pattern.compile("\\$(?i)msg_tier\\$"), MSG_TIER);
	}
}
