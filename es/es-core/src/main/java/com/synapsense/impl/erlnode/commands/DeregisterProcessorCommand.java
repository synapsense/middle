
package com.synapsense.impl.erlnode.commands;

import java.util.Map;

import org.apache.log4j.Logger;

import com.ericsson.otp.erlang.OtpErlangObject;
import com.ericsson.otp.erlang.OtpErlangPid;
import com.ericsson.otp.erlang.OtpErlangTuple;
import com.synapsense.impl.erlnode.ExceptionTypes;
import com.synapsense.service.nsvc.EventProcessor;


public class DeregisterProcessorCommand extends Command {

	private final static Logger log =
		Logger.getLogger(DeregisterProcessorCommand.class);

	public DeregisterProcessorCommand(Map<String, Object> state) {
		super(state);
	}

	@Override
	public OtpErlangObject do_command(OtpErlangTuple command) throws Exception {
		OtpErlangObject tmp = command.elementAt(1);
		if (tmp == null || !(tmp instanceof OtpErlangPid)) {
			return makeException(ExceptionTypes.ILLEGAL_ARGUMENTS, null);
		}
		OtpErlangPid pid = (OtpErlangPid) tmp;
		
		EventProcessor processor = RegisterProcessorCommand.processors.remove(pid);
		if(processor != null) {
			getNsvc().deregisterProcessor(processor);
			if(log.isDebugEnabled())
				log.debug("Deregistered processor for: "+pid);
		}
		return makeOkResult(null);
	}

}
