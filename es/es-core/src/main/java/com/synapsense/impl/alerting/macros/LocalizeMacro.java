package com.synapsense.impl.alerting.macros;

import java.util.IllegalFormatException;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

public class LocalizeMacro extends AbstractPatternMacro {

	private static final Logger log = Logger.getLogger(LocalizeMacro.class);

	private static final Pattern pattern = Pattern.compile("\\$(?i)localize\\((.+?)\\)\\$", Pattern.DOTALL);

	public static void main(String[] _args) {
		String inputString = "$localize(alert_message1)$$localize(alert_message_out_of_recommended_range,\"Rear Exhaust w/ \\\"Subfloor\\\"\",\"[62.85,62.85,62.86]\")$$localize(alert_message2)$$localize(alert_message_out_of_recommended_range,\"Rear Exhaust w/ Subfloor\",\"[62.85,62.85,62.86]\")$";// $localize(alert_message_out_of_recommended_range,Rear
		                                                                                                                                                                                                                                                                                                  // Exhaust
		                                                                                                                                                                                                                                                                                                  // w/
		                                                                                                                                                                                                                                                                                                  // Subfloor,[62.85,62.85,62.86])$$localize(alert_message)$";

		Matcher m = pattern.matcher(inputString);

		while (m.find()) {
			String args = m.group(1);
			System.out.println("args: " + args);

			int index = args.indexOf(',');
			String id;

			if (index != -1) {
				id = args.substring(0, index);
				args = args.substring(index + 2, args.length() - 1);
				String[] ss = args.split("\",\"");
				for (int i = 0; i < ss.length; i++) {
					String s = ss[i];
					// unescape \"
					ss[i] = unescapeQuotes(s);
					System.out.println(ss[i]);
				}
			} else {
				id = args;
			}

			System.out.println("id: " + id);
		}
	}

	private static String unescapeQuotes(String str) {
		return str.replaceAll(Pattern.quote("\\\""), "\"");
	}

	@Override
	public String expandIn(String input, MacroContext ctx) {
		if (input == null)
			return null;
		if (ctx.usrCtx == null)
			throw new IllegalArgumentException("MacroContext.userCtx must be not null");
		if (ctx.localizationService == null)
			throw new IllegalArgumentException("MacroContext.localizationService must be not null");
		return super.expandIn(input, ctx);
	}

	@Override
	protected String expand(Matcher m, MacroContext ctx) {

		String args = m.group(1);
		int index = args.indexOf(',');
		String replacementToReturn;
		String id;
		Locale locale = ctx.usrCtx.getLocale();

		String[] params = null;

		if (index != -1) {
			id = args.substring(0, index);
			args = args.substring(index + 2, args.length() - 1);
			params = args.split("\",\"");
			for (int i = 0; i < params.length; i++) {
				String s = params[i];
				// try to translate params
				String woQuotes = unescapeQuotes(s);
				params[i] = (woQuotes.isEmpty()) ? woQuotes : ctx.localizationService.getTranslation(woQuotes, locale);
			}
		} else {
			id = args;
		}

		String formatString = (id.isEmpty()) ? id : ctx.localizationService.getString(id, locale);

		if (formatString == null) {
			log.error("There is not string for id: " + id);
			replacementToReturn = id;
		} else {
			if (params == null) {
				replacementToReturn = formatString;
			} else {
				try {
					replacementToReturn = String.format(formatString, (Object[]) params);
				} catch (IllegalFormatException e) {
					replacementToReturn = formatString;
					StringBuffer sb = new StringBuffer("Wrong format string for ").append(id).append(" : ")
					        .append(formatString);
					for (String param : params) {
						sb.append(", ").append(param);
					}
					log.error(sb.toString());
				}
			}
		}

		return Matcher.quoteReplacement(replacementToReturn);
	}

	@Override
	protected Pattern getPattern() {
		return pattern;
	}

}
