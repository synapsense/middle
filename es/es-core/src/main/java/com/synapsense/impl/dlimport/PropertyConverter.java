package com.synapsense.impl.dlimport;

import java.awt.Color;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;

import org.apache.commons.codec.binary.Base64;

import com.synapsense.dto.BinaryData;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.impl.dlimport.exceptions.ConversionNotSupportedException;
import com.synapsense.impl.dlimport.exceptions.IncompatibleTypeException;
import com.synapsense.service.impl.dao.to.BinaryDataImpl;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.w3c.dom.DOMException;
import org.w3c.dom.Node;

public class PropertyConverter {

	// static converters = [:]
	static Map<String, PropertyValueConverter> converters = new HashMap<String, PropertyValueConverter>();

	static {
		converters.put("java.lang.String", new StringValueConverter());
		converters.put("java.lang.Integer", new IntegerValueConverter());
		converters.put("java.lang.Short", new ShortValueConverter());
		converters.put("java.lang.Float", new FloatValueConverter());
		converters.put("java.lang.Double", new DoubleValueConverter());
		converters.put("java.lang.Long", new LongValueConverter());
		converters.put("java.lang.Boolean", new BooleanValueConverter());
		converters.put("java.awt.Image", new ImageValueConverter());
		converters.put("java.awt.Color", new ColorValueConverter());
		converters.put("java.util.Date", new DateValueConverter());
		converters.put("DeploymentLab.ChartContents", new ChartContentsConverter());
		converters.put("DeploymentLab.MultiLineContents", new MultiLineContentsConverter());
		converters.put("com.synapsense.dto.BinaryData", new BinaryDataConverter());
		converters.put("DeploymentLab.ChartContentsCRAH", new ChartContentsCRAHConverter());
		converters.put("DeploymentLab.ChartContentsBTU", new ChartContentsCRAHConverter());
		converters.put("DeploymentLab.ChartContentsCFM", new ChartContentsCRAHConverter());
		converters.put("com.synapsense.dto.TO", new TOObjectConverter());
		converters.put("DeploymentLab.ConfigModbus", new ConfigModbusConverter());
		converters.put("DeploymentLab.ConfigModbusRead", new ConfigModbusConverter());
		converters.put("DeploymentLab.ConfigModbusWrite", new ConfigModbusConverter());
		// converters.put("DeploymentLab.ChartContentsBTU", new
		// ChartContentsBTUConverter());

	}

	static Object fromXml(String type, Node xml) {
		if (!converters.containsKey(type))
			throw new RuntimeException("No converter found for '" + type + "'!");
		return converters.get(type).fromXml(xml);
	}

	static String toXml(String type, Object value) {
		if (!converters.containsKey(type))
			throw new RuntimeException("No converter found for '" + type + "'!");
		return converters.get(type).toXml(value);
	}

	static Object fromString(String type, String value) {
		if (!converters.containsKey(type))
			throw new RuntimeException("No converter found for '" + type + "'!");
		return converters.get(type).fromString(value);
	}

	static Object fromObject(String type, Object value) {
		if (!converters.containsKey(type))
			throw new RuntimeException("No converter found for '" + type + "'!");
		return converters.get(type).fromObject(value);
	}
}

interface PropertyValueConverter {
	Object fromXml(Node xml);

	String toXml(Object value);

	Object fromString(String value);

	Object fromObject(Object value);
}

class StringValueConverter implements PropertyValueConverter {
	public Object fromXml(Node xml) {
		return xml.getTextContent().trim();
	}

	public String toXml(Object value) {
		if (value == null)
			return null;
		return value.toString();
	}

	public Object fromString(String value) {
		return value.trim();
	}

	public Object fromObject(Object value) {
		return value.toString().trim();
	}
}

class IntegerValueConverter implements PropertyValueConverter {
	public Object fromXml(Node xml) {
		return fromString(xml.getTextContent());
	}

	public String toXml(Object value) {
		if (value == null)
			return "";
		return value.toString();
	}

	public Object fromString(String value) {
		if (value.equals(""))
			return null;
		return Double.valueOf(value).intValue();
	}

	public Object fromObject(Object value) {
		return Double.valueOf(value.toString()).intValue();
	}
}

class ShortValueConverter implements PropertyValueConverter {
	public Object fromXml(Node xml) {
		return fromString(xml.getTextContent());
	}

	public String toXml(Object value) {
		if (value == null)
			return "";
		return value.toString();
	}

	public Object fromString(String value) {
		if (value.equals(""))
			return null;
		return Double.valueOf(value).shortValue();
	}

	public Object fromObject(Object value) {
		return Double.valueOf(value.toString()).shortValue();
	}
}

class FloatValueConverter implements PropertyValueConverter {
	public Object fromXml(Node xml) {
		return fromString(xml.getTextContent());
	}

	public String toXml(Object value) {
		if (value == null)
			return "";
		return value.toString();
	}

	public Object fromString(String value) {
		if (value.equals(""))
			return null;
		return Float.parseFloat(value);
	}

	public Object fromObject(Object value) {
		return Float.parseFloat(value.toString());
	}
}

class DoubleValueConverter implements PropertyValueConverter {
	public Object fromXml(Node xml) {
		return fromString(xml.getTextContent());
	}

	public String toXml(Object value) {
		if (value == null)
			return "";
		return value.toString();
	}

	public Object fromString(String value) {
		if (value.equals(""))
			return null;
		return Double.parseDouble(value);
	}

	public Object fromObject(Object value) {
		return Double.parseDouble(value.toString());
	}
}

class LongValueConverter implements PropertyValueConverter {
	public Object fromXml(Node xml) {
		return fromString(xml.getTextContent());
	}

	public String toXml(Object value) {
		if (value == null)
			return "";
		return value.toString();
	}

	public Object fromString(String value) {
		if (value.equals(""))
			return null;
		return Long.valueOf(value);
	}

	public Object fromObject(Object value) {
		return Long.parseLong(value.toString());
	}
}

class ImageValueConverter implements PropertyValueConverter {
	public Object fromXml(Node xml) {
		BufferedImage temp;
		try {
			temp = ImageIO.read(new ByteArrayInputStream(Base64.decodeBase64(xml.getTextContent().getBytes())));
		} catch (DOMException e) {
			throw new RuntimeException(e.getMessage(), e);
		} catch (IOException e) {
			throw new RuntimeException(e.getMessage(), e);
		}
		if (temp == null) {
			return null;
		}
		// turn temp into .type_int_arbg
		BufferedImage result = ARGBConverter.convert(temp);
		return result;
	}

	public String toXml(Object value) {
		if (value == null)
			return "";
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		try {
			ImageIO.write((RenderedImage) value, "png", output);
		} catch (IOException e) {
			throw new RuntimeException(e.getMessage(), e);
		}
		String imageXml = new String(Base64.encodeBase64(output.toByteArray()));
		output.reset(); // empty out buffer
		return imageXml;
	}

	public Object fromString(String value) {
		throw new ConversionNotSupportedException(value, "java.awt.Image");
	}

	public Object fromObject(Object value) {
		if (value.getClass().getName().equals("java.awt.Image")
		        || value.getClass().getName().equals("java.awt.image.BufferedImage")) {
			return value;
		}
		throw new ConversionNotSupportedException(value, "java.awt.Image");
	}
}

class ColorValueConverter implements PropertyValueConverter {
	public Object fromXml(Node xml) {
		String[] c = xml.getTextContent().split(",");
		return new Color(Float.parseFloat(c[0]), Float.parseFloat(c[1]), Float.parseFloat(c[2]), Float.parseFloat(c[3]));
	}

	public String toXml(Object value) {
		if (value == null)
			return "";

		float[] components = ((Color) value).getComponents(null);
		if (components.length == 0) {
			return "";
		}

		StringBuffer res = new StringBuffer();
		for (float c : components) {
			res.append(c).append(",");
		}
		return res.substring(0, res.length() - 1);
	}

	public Object fromString(String value) {
		throw new ConversionNotSupportedException(value, "java.awt.Color");
	}

	public Object fromObject(Object value) {
		if (value.getClass().getName().equals("java.awt.Color"))
			return value;
		throw new ConversionNotSupportedException(value, "java.awt.Color");
	}
}

class DateValueConverter implements PropertyValueConverter {
	SimpleDateFormat sdf;

	DateValueConverter() {
		sdf = new SimpleDateFormat("M/d/yy HH:mm");
	}

	public Object fromXml(Node xml) {
		return fromString(xml.getTextContent());
	}

	public String toXml(Object value) {
		if (value == null)
			return "";
		if (value.getClass().getName().equals("java.util.Date")) {
			return sdf.format(value);
		}
		throw new IncompatibleTypeException("Can't convert value '$value' of type '" + value.getClass().getName()
		        + "' to XML form.");
	}

	@SuppressWarnings("deprecation")
	public Object fromString(String value) {
		if (value.equals("")) {
			return new Date();
		} else {
			try {
				// return sdf.parse(value)
				return new Date(value);
			} catch (Exception e) {
				// if parsing in the standard locale fails, try the european
				// locale. This should handle older files created in different
				// locales.
				// TODO:: no clue why using eurpean locale if fails..
				// SimpleDateFormat europeanLocale = new
				// SimpleDateFormat("d.M.yy h:mm")
				// return europeanLocale.parse(value)
				try {
					return sdf.parse(value);
				} catch (ParseException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
					return "";
				}
			}
		}
	}

	public Object fromObject(Object value) {
		if (value.getClass().getName().equals("java.util.Date"))
			return value;
		throw new ConversionNotSupportedException(value, "java.util.Date");
	}
}

class BooleanValueConverter implements PropertyValueConverter {
	public Object fromXml(Node xml) {
		return fromString(xml.getTextContent());
	}

	public String toXml(Object value) {
		if (value == null)
			return "";
		return value.toString();
	}

	public Object fromString(String value) {
		return Boolean.parseBoolean(value);
	}

	public Object fromObject(Object value) {
		if (value.getClass().getName().equals("java.lang.Boolean"))
			return value;
		throw new ConversionNotSupportedException(value, "java.lang.Boolean");
	}
}

class ChartContentsConverter implements PropertyValueConverter {
	public Object fromXml(Node xml) {
		return xml.getTextContent().trim();
	}

	public String toXml(Object value) {
		if (value == null)
			return null;
		return value.toString();
	}

	public Object fromString(String value) {
		return value.trim();
	}

	public Object fromObject(Object value) {
		return value.toString().trim();
	}
}

class MultiLineContentsConverter implements PropertyValueConverter {
	public Object fromXml(Node xml) {
		return xml.getTextContent().trim();
	}

	public String toXml(Object value) {
		if (value == null)
			return null;
		return value.toString();
	}

	public Object fromString(String value) {
		return value.trim();
	}

	public Object fromObject(Object value) {
		return value.toString().trim();
	}
}

class BinaryDataConverter implements PropertyValueConverter {
	public Object fromXml(Node xml) {
		if (xml.getTextContent().trim() == "") {
			return null;
		}
		String value = xml.getTextContent().trim();
		return this.fromString(value);
	}

	public String toXml(Object value) {
		if (value == null) {
			return "";
		}
		// base64 encode and return that as a string
		String dataXml = new String(Base64.encodeBase64(((BinaryDataImpl) value).getValue()));
		return dataXml;
	}

	public Object fromString(String value) {
		// assume a base64-encoded string of bytes
		// return new BinaryDataImpl( Base64.decodeBase64(value.getBytes()) );
		BinaryData result = null;

		result = new BinaryDataImpl(Base64.decodeBase64(value.getBytes()));

		return result;
	}

	public Object fromObject(Object value) {
		// BinaryData data = new BinaryDataImpl(value)
		if (value == null) {
			return null;
		}
		if (value instanceof BinaryData) {
			return value;
		} else {
			throw new ConversionNotSupportedException(value, "BinaryData");
		}
	}
}

class TOObjectConverter implements PropertyValueConverter {
	public Object fromXml(Node xml) {
		return null;
	}

	public String toXml(Object value) {
		if (value == null)
			return null;
		return "";
	}

	public Object fromString(String value) {
		return "";
	}

	public Object fromObject(Object value) {
		TO<?> empty_to = TOFactory.EMPTY_TO;
		return empty_to;
	}

}

class ChartContentsCRAHConverter implements PropertyValueConverter {
	public Object fromXml(Node xml) {
		return xml.getTextContent().trim();
	}

	public String toXml(Object value) {
		if (value == null)
			return null;
		return value.toString();
	}

	public Object fromString(String value) {
		return value.trim();
	}

	public Object fromObject(Object value) {
		return value.toString().trim();
	}
}

class ChartContentsBTUConverter implements PropertyValueConverter {
	public Object fromXml(Node xml) {
		return xml.getTextContent().trim();
	}

	public String toXml(Object value) {
		if (value == null)
			return null;
		return value.toString();
	}

	public Object fromString(String value) {
		return value.trim();
	}

	public Object fromObject(Object value) {
		return value.toString().trim();
	}
}

class ConfigModbusConverter implements PropertyValueConverter {
	public Object fromXml(Node xml) {
		return (xml.getTextContent().trim()).intern();
	}

	public String toXml(Object value) {
		if (value == null)
			return null;
		return value.toString();
	}

	public Object fromString(String value) {
		return (value.trim()).intern();
	}

	public Object fromObject(Object value) {
		return (value.toString().trim()).intern();
	}
}
