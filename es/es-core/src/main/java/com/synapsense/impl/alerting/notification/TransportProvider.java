package com.synapsense.impl.alerting.notification;

import com.synapsense.service.impl.notification.Transport;

public interface TransportProvider {
	void registerTransport(String name, Transport impl);

	Transport get(String protocol);
}