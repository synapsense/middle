package com.synapsense.impl.erlnode.filters;

import org.apache.log4j.Logger;

import com.ericsson.otp.erlang.OtpErlangBinary;
import com.ericsson.otp.erlang.OtpErlangList;
import com.ericsson.otp.erlang.OtpErlangObject;
import com.ericsson.otp.erlang.OtpErlangTuple;
import com.synapsense.impl.erlnode.commands.RegisterProcessorCommand;
import com.synapsense.impl.erlnode.utils.CollectionToConverter;
import com.synapsense.impl.erlnode.utils.StringConverter;
import com.synapsense.service.nsvc.filters.PropertiesChangedEventFilter;
import com.synapsense.service.nsvc.filters.TOMatcher;

//-record(properties_changed_event_filter,
//{matcher = #regex{} :: #regex{} | #to{}, properties :: list(binary())}).
class PropertiesChangedEventFilterParser {

	private static final Logger log = Logger.getLogger(PropertiesChangedEventFilter.class);

	public PropertiesChangedEventFilter parse(OtpErlangTuple filterDef) {
		// parse matcher part
		TOMatcher matcher = TOMatcherParser.parse(filterDef.elementAt(1));
		// parse properties part
		OtpErlangObject tmp = filterDef.elementAt(2);
		if (tmp == null || RegisterProcessorCommand.undefined.equals(tmp)) {
			log.debug("returning PropertiesChangedEventFilter");
			return new PropertiesChangedEventFilter(matcher);
		}
		OtpErlangList otpList = CollectionToConverter.checkList(tmp);
		if (otpList == null) {
			throw new IllegalArgumentException(
			        "The properties argument of properties_changed_event_filter must be a list of binary strings but was"
			                + tmp.getClass().getName());
		}
		OtpErlangObject[] propsList = otpList.elements();
		try {
			String[] props = new String[propsList.length];
			for (int i = 0; i < props.length; i++) {
				props[i] = StringConverter.utf8BinaryToString(propsList[i]);
			}
			log.debug("returning PropertiesChangedEventFilter");
			return new PropertiesChangedEventFilter(matcher, props);
		} catch (ClassCastException e) {
			throw new IllegalArgumentException("The elemets of the properties argument expected to be binary strings", e);
		}
	}

}
