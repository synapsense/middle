package com.synapsense.impl.performer.snmp;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import com.synapsense.service.impl.notification.NotificationException;
import org.apache.log4j.Logger;
import org.opennms.protocols.snmp.SnmpPduTrap;

import com.synapsense.dto.Message;
import com.synapsense.impl.performer.snmp.conf.ConnectionInfoProvider;
import com.synapsense.impl.performer.snmp.conf.SNMPConfigurationException;
import com.synapsense.service.impl.notification.Transport;

public abstract class SNMPTrapPerformer implements Transport, SNMPTrapPerformerMBean {
	private final static Logger logger = Logger.getLogger(SNMPTrapPerformer.class);
	private final String PERF_NAME = "SNMPTrapPerformer";

	private final TrapSenders senders = new TrapSenders();

    @Override
    public boolean isValid(Message message) {
        return true;
    }


    @Override
	public final String getName() {
		return PERF_NAME;
	}

    @Override
	public void send(Message metadata) throws NotificationException {
		SnmpPduTrap pdu = createPDU(metadata);
		try {
			senders.getTrapSender(metadata.getDestination()).send(pdu);
		} catch (SNMPPerformerException e) {
			logger.warn("Couldn't send the event " + metadata.toString(), e);
		}
	}

	protected abstract SnmpPduTrap createPDU(Message data);

	@Override
	@PostConstruct
	public void create() {
		try {
			ConnectionInfoProvider.readConfig();
		} catch (SNMPConfigurationException e) {
			throw new IllegalStateException("Cannot read configuration", e);
		}
		logger.info("Creating the " + PERF_NAME + " alerts performer");
		start();
	}

	@Override
	public void start() {
		logger.info("Starting the " + PERF_NAME + " alerts performer");
	}

	@Override
	@PreDestroy
	public void stop() {
		logger.info("Stopping the " + PERF_NAME + " alerts performer");
		destroy();
	}

	@Override
	public void destroy() {
		logger.info("Destroying the " + PERF_NAME + " alerts performer");
		senders.clear();
	}
}
