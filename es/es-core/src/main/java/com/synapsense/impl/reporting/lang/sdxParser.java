// $ANTLR 3.4 D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g 2013-01-17 11:44:24

package com.synapsense.impl.reporting.lang;

import com.synapsense.service.reporting.DimensionalQuery;
import com.synapsense.service.reporting.olap.*;
import com.synapsense.service.reporting.olap.custom.Cube;
import org.antlr.runtime.*;
import org.antlr.runtime.tree.CommonTree;
import org.antlr.runtime.tree.CommonTreeAdaptor;
import org.antlr.runtime.tree.TreeAdaptor;

import java.util.ArrayList;
import java.util.List;


@SuppressWarnings({"all", "warnings", "unchecked"})
public class sdxParser extends Parser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "AMP_QUOTED_ID", "ASTERISK", "AXIS", "COLON", "COLUMNS", "COMMA", "CONCAT", "DOT", "EMPTY", "EQ", "FUN", "GE", "GT", "ID", "LBRACE", "LE", "LPAREN", "LT", "MEMBERS", "MINUS", "NE", "NON", "NUMBER", "ON", "PLUS", "QUOTE", "QUOTED_ID", "RBRACE", "ROWS", "RPAREN", "SELECT", "SEMICOLON", "SOLIDUS", "STRING", "WHERE", "WS"
    };

    public static final int EOF=-1;
    public static final int AMP_QUOTED_ID=4;
    public static final int ASTERISK=5;
    public static final int AXIS=6;
    public static final int COLON=7;
    public static final int COLUMNS=8;
    public static final int COMMA=9;
    public static final int CONCAT=10;
    public static final int DOT=11;
    public static final int EMPTY=12;
    public static final int EQ=13;
    public static final int FUN=14;
    public static final int GE=15;
    public static final int GT=16;
    public static final int ID=17;
    public static final int LBRACE=18;
    public static final int LE=19;
    public static final int LPAREN=20;
    public static final int LT=21;
    public static final int MEMBERS=22;
    public static final int MINUS=23;
    public static final int NE=24;
    public static final int NON=25;
    public static final int NUMBER=26;
    public static final int ON=27;
    public static final int PLUS=28;
    public static final int QUOTE=29;
    public static final int QUOTED_ID=30;
    public static final int RBRACE=31;
    public static final int ROWS=32;
    public static final int RPAREN=33;
    public static final int SELECT=34;
    public static final int SEMICOLON=35;
    public static final int SOLIDUS=36;
    public static final int STRING=37;
    public static final int WHERE=38;
    public static final int WS=39;

    // delegates
    public Parser[] getDelegates() {
        return new Parser[] {};
    }

    // delegators


    public sdxParser(TokenStream input) {
        this(input, new RecognizerSharedState());
    }
    public sdxParser(TokenStream input, RecognizerSharedState state) {
        super(input, state);
    }

protected TreeAdaptor adaptor = new CommonTreeAdaptor();

public void setTreeAdaptor(TreeAdaptor adaptor) {
    this.adaptor = adaptor;
}
public TreeAdaptor getTreeAdaptor() {
    return adaptor;
}
    public String[] getTokenNames() { return sdxParser.tokenNames; }
    public String getGrammarFileName() { return "D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g"; }


    	private DimensionalQuery dq;

    	public DimensionalQuery getDimensionalQuery() {
    		return dq;
    	}
    	  
    	@Override
    	public void reportError(RecognitionException e) {
        		throw new RuntimeException("I quit!\n" + e, e); 
      	}
      	
      	private SimpleMember buildMemberFromPath(List<SimpleMember> members, Hierarchy hierarchy) {
      		for(int i = members.size() - 1; i >= 0; i --){ 
      			members.get(i).setHierarchy(hierarchy); 
    	  		if (i != 0) members.get(i).setParent(members.get(i-1));
      		} 
      		return members.get(members.size() - 1);
      	}
      	


    public static class sdx_statement_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "sdx_statement"
    // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:81:1: sdx_statement : select_statement EOF ;
    public final sdxParser.sdx_statement_return sdx_statement() throws RecognitionException {
        sdxParser.sdx_statement_return retval = new sdxParser.sdx_statement_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token EOF2=null;
        sdxParser.select_statement_return select_statement1 =null;


        CommonTree EOF2_tree=null;

        try {
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:82:2: ( select_statement EOF )
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:82:5: select_statement EOF
            {
            root_0 = (CommonTree)adaptor.nil();


            pushFollow(FOLLOW_select_statement_in_sdx_statement273);
            select_statement1=select_statement();

            state._fsp--;

            adaptor.addChild(root_0, select_statement1.getTree());

            EOF2=(Token)match(input,EOF,FOLLOW_EOF_in_sdx_statement275); 
            EOF2_tree = 
            (CommonTree)adaptor.create(EOF2)
            ;
            adaptor.addChild(root_0, EOF2_tree);


            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "sdx_statement"


    public static class select_statement_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "select_statement"
    // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:85:1: select_statement : SELECT ( fun )? axis_specification_list ( WHERE slicer_specification )? ;
    public final sdxParser.select_statement_return select_statement() throws RecognitionException {
        sdxParser.select_statement_return retval = new sdxParser.select_statement_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token SELECT3=null;
        Token WHERE6=null;
        sdxParser.fun_return fun4 =null;

        sdxParser.axis_specification_list_return axis_specification_list5 =null;

        sdxParser.slicer_specification_return slicer_specification7 =null;


        CommonTree SELECT3_tree=null;
        CommonTree WHERE6_tree=null;

        try {
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:86:2: ( SELECT ( fun )? axis_specification_list ( WHERE slicer_specification )? )
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:86:4: SELECT ( fun )? axis_specification_list ( WHERE slicer_specification )?
            {
            root_0 = (CommonTree)adaptor.nil();


            dq = new DimensionalQuery();

            SELECT3=(Token)match(input,SELECT,FOLLOW_SELECT_in_select_statement288); 
            SELECT3_tree = 
            (CommonTree)adaptor.create(SELECT3)
            ;
            adaptor.addChild(root_0, SELECT3_tree);


            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:86:42: ( fun )?
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0==FUN) ) {
                alt1=1;
            }
            switch (alt1) {
                case 1 :
                    // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:86:43: fun
                    {
                    pushFollow(FOLLOW_fun_in_select_statement291);
                    fun4=fun();

                    state._fsp--;

                    adaptor.addChild(root_0, fun4.getTree());

                    }
                    break;

            }


            pushFollow(FOLLOW_axis_specification_list_in_select_statement295);
            axis_specification_list5=axis_specification_list();

            state._fsp--;

            adaptor.addChild(root_0, axis_specification_list5.getTree());

            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:86:73: ( WHERE slicer_specification )?
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==WHERE) ) {
                alt2=1;
            }
            switch (alt2) {
                case 1 :
                    // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:86:74: WHERE slicer_specification
                    {
                    WHERE6=(Token)match(input,WHERE,FOLLOW_WHERE_in_select_statement298); 
                    WHERE6_tree = 
                    (CommonTree)adaptor.create(WHERE6)
                    ;
                    adaptor.addChild(root_0, WHERE6_tree);


                    pushFollow(FOLLOW_slicer_specification_in_select_statement300);
                    slicer_specification7=slicer_specification();

                    state._fsp--;

                    adaptor.addChild(root_0, slicer_specification7.getTree());

                    }
                    break;

            }


            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "select_statement"


    public static class axis_specification_list_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "axis_specification_list"
    // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:89:1: axis_specification_list : axis_specification ( COMMA axis_specification )* ;
    public final sdxParser.axis_specification_list_return axis_specification_list() throws RecognitionException {
        sdxParser.axis_specification_list_return retval = new sdxParser.axis_specification_list_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token COMMA9=null;
        sdxParser.axis_specification_return axis_specification8 =null;

        sdxParser.axis_specification_return axis_specification10 =null;


        CommonTree COMMA9_tree=null;

        try {
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:90:2: ( axis_specification ( COMMA axis_specification )* )
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:90:4: axis_specification ( COMMA axis_specification )*
            {
            root_0 = (CommonTree)adaptor.nil();


            pushFollow(FOLLOW_axis_specification_in_axis_specification_list317);
            axis_specification8=axis_specification();

            state._fsp--;

            adaptor.addChild(root_0, axis_specification8.getTree());

            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:90:23: ( COMMA axis_specification )*
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==COMMA) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:90:24: COMMA axis_specification
            	    {
            	    COMMA9=(Token)match(input,COMMA,FOLLOW_COMMA_in_axis_specification_list320); 
            	    COMMA9_tree = 
            	    (CommonTree)adaptor.create(COMMA9)
            	    ;
            	    adaptor.addChild(root_0, COMMA9_tree);


            	    pushFollow(FOLLOW_axis_specification_in_axis_specification_list322);
            	    axis_specification10=axis_specification();

            	    state._fsp--;

            	    adaptor.addChild(root_0, axis_specification10.getTree());

            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "axis_specification_list"


    public static class axis_specification_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "axis_specification"
    // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:93:1: axis_specification : ( NON EMPTY )? setDef= set_definition ON axisDef= axis_name ;
    public final sdxParser.axis_specification_return axis_specification() throws RecognitionException {
        sdxParser.axis_specification_return retval = new sdxParser.axis_specification_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token NON11=null;
        Token EMPTY12=null;
        Token ON13=null;
        sdxParser.set_definition_return setDef =null;

        sdxParser.axis_name_return axisDef =null;


        CommonTree NON11_tree=null;
        CommonTree EMPTY12_tree=null;
        CommonTree ON13_tree=null;

        try {
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:94:2: ( ( NON EMPTY )? setDef= set_definition ON axisDef= axis_name )
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:94:4: ( NON EMPTY )? setDef= set_definition ON axisDef= axis_name
            {
            root_0 = (CommonTree)adaptor.nil();


            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:94:4: ( NON EMPTY )?
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==NON) ) {
                alt4=1;
            }
            switch (alt4) {
                case 1 :
                    // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:94:5: NON EMPTY
                    {
                    NON11=(Token)match(input,NON,FOLLOW_NON_in_axis_specification340); 
                    NON11_tree = 
                    (CommonTree)adaptor.create(NON11)
                    ;
                    adaptor.addChild(root_0, NON11_tree);


                    EMPTY12=(Token)match(input,EMPTY,FOLLOW_EMPTY_in_axis_specification342); 
                    EMPTY12_tree = 
                    (CommonTree)adaptor.create(EMPTY12)
                    ;
                    adaptor.addChild(root_0, EMPTY12_tree);


                    }
                    break;

            }


            pushFollow(FOLLOW_set_definition_in_axis_specification350);
            setDef=set_definition();

            state._fsp--;

            adaptor.addChild(root_0, setDef.getTree());

            ON13=(Token)match(input,ON,FOLLOW_ON_in_axis_specification352); 
            ON13_tree = 
            (CommonTree)adaptor.create(ON13)
            ;
            adaptor.addChild(root_0, ON13_tree);


            pushFollow(FOLLOW_axis_name_in_axis_specification358);
            axisDef=axis_name();

            state._fsp--;

            adaptor.addChild(root_0, axisDef.getTree());

            dq.addAxisSet(setDef.axisSet, axisDef.axisId);

            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "axis_specification"


    public static class slicer_specification_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "slicer_specification"
    // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:97:1: slicer_specification : slicerDef= set_definition ;
    public final sdxParser.slicer_specification_return slicer_specification() throws RecognitionException {
        sdxParser.slicer_specification_return retval = new sdxParser.slicer_specification_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        sdxParser.set_definition_return slicerDef =null;



        try {
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:98:2: (slicerDef= set_definition )
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:98:4: slicerDef= set_definition
            {
            root_0 = (CommonTree)adaptor.nil();


            pushFollow(FOLLOW_set_definition_in_slicer_specification380);
            slicerDef=set_definition();

            state._fsp--;

            adaptor.addChild(root_0, slicerDef.getTree());

            dq.setSlicer(slicerDef.axisSet);

            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "slicer_specification"


    public static class axis_name_return extends ParserRuleReturnScope {
        public int axisId;;
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "axis_name"
    // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:101:1: axis_name returns [int axisId;] : ( ROWS | COLUMNS | AXIS axisNum= LPAREN NUMBER RPAREN );
    public final sdxParser.axis_name_return axis_name() throws RecognitionException {
        sdxParser.axis_name_return retval = new sdxParser.axis_name_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token axisNum=null;
        Token ROWS14=null;
        Token COLUMNS15=null;
        Token AXIS16=null;
        Token NUMBER17=null;
        Token RPAREN18=null;

        CommonTree axisNum_tree=null;
        CommonTree ROWS14_tree=null;
        CommonTree COLUMNS15_tree=null;
        CommonTree AXIS16_tree=null;
        CommonTree NUMBER17_tree=null;
        CommonTree RPAREN18_tree=null;

        try {
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:102:3: ( ROWS | COLUMNS | AXIS axisNum= LPAREN NUMBER RPAREN )
            int alt5=3;
            switch ( input.LA(1) ) {
            case ROWS:
                {
                alt5=1;
                }
                break;
            case COLUMNS:
                {
                alt5=2;
                }
                break;
            case AXIS:
                {
                alt5=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 5, 0, input);

                throw nvae;

            }

            switch (alt5) {
                case 1 :
                    // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:102:5: ROWS
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    ROWS14=(Token)match(input,ROWS,FOLLOW_ROWS_in_axis_name403); 
                    ROWS14_tree = 
                    (CommonTree)adaptor.create(ROWS14)
                    ;
                    adaptor.addChild(root_0, ROWS14_tree);


                    retval.axisId = 0;

                    }
                    break;
                case 2 :
                    // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:103:5: COLUMNS
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    COLUMNS15=(Token)match(input,COLUMNS,FOLLOW_COLUMNS_in_axis_name411); 
                    COLUMNS15_tree = 
                    (CommonTree)adaptor.create(COLUMNS15)
                    ;
                    adaptor.addChild(root_0, COLUMNS15_tree);


                    retval.axisId = 1;

                    }
                    break;
                case 3 :
                    // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:104:6: AXIS axisNum= LPAREN NUMBER RPAREN
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    AXIS16=(Token)match(input,AXIS,FOLLOW_AXIS_in_axis_name420); 
                    AXIS16_tree = 
                    (CommonTree)adaptor.create(AXIS16)
                    ;
                    adaptor.addChild(root_0, AXIS16_tree);


                    axisNum=(Token)match(input,LPAREN,FOLLOW_LPAREN_in_axis_name426); 
                    axisNum_tree = 
                    (CommonTree)adaptor.create(axisNum)
                    ;
                    adaptor.addChild(root_0, axisNum_tree);


                    NUMBER17=(Token)match(input,NUMBER,FOLLOW_NUMBER_in_axis_name428); 
                    NUMBER17_tree = 
                    (CommonTree)adaptor.create(NUMBER17)
                    ;
                    adaptor.addChild(root_0, NUMBER17_tree);


                    RPAREN18=(Token)match(input,RPAREN,FOLLOW_RPAREN_in_axis_name430); 
                    RPAREN18_tree = 
                    (CommonTree)adaptor.create(RPAREN18)
                    ;
                    adaptor.addChild(root_0, RPAREN18_tree);


                    retval.axisId = Integer.valueOf(axisNum.getText());

                    }
                    break;

            }
            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "axis_name"


    public static class set_definition_return extends ParserRuleReturnScope {
        public AxisSet axisSet = null;;
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "set_definition"
    // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:107:1: set_definition returns [AxisSet axisSet = null;] : firstSetDef= simple_set_definition (opDef= set_operation setDef= simple_set_definition )* ;
    public final sdxParser.set_definition_return set_definition() throws RecognitionException {
        sdxParser.set_definition_return retval = new sdxParser.set_definition_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        sdxParser.simple_set_definition_return firstSetDef =null;

        sdxParser.set_operation_return opDef =null;

        sdxParser.simple_set_definition_return setDef =null;



        try {
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:108:2: (firstSetDef= simple_set_definition (opDef= set_operation setDef= simple_set_definition )* )
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:108:5: firstSetDef= simple_set_definition (opDef= set_operation setDef= simple_set_definition )*
            {
            root_0 = (CommonTree)adaptor.nil();


            pushFollow(FOLLOW_simple_set_definition_in_set_definition455);
            firstSetDef=simple_set_definition();

            state._fsp--;

            adaptor.addChild(root_0, firstSetDef.getTree());

            retval.axisSet = firstSetDef.set;

            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:108:77: (opDef= set_operation setDef= simple_set_definition )*
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( (LA6_0==ASTERISK||LA6_0==PLUS) ) {
                    alt6=1;
                }


                switch (alt6) {
            	case 1 :
            	    // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:108:78: opDef= set_operation setDef= simple_set_definition
            	    {
            	    pushFollow(FOLLOW_set_operation_in_set_definition464);
            	    opDef=set_operation();

            	    state._fsp--;

            	    adaptor.addChild(root_0, opDef.getTree());

            	    pushFollow(FOLLOW_simple_set_definition_in_set_definition470);
            	    setDef=simple_set_definition();

            	    state._fsp--;

            	    adaptor.addChild(root_0, setDef.getTree());

            	    retval.axisSet = opDef.opSet.add(retval.axisSet).add(setDef.set);

            	    }
            	    break;

            	default :
            	    break loop6;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "set_definition"


    public static class simple_set_definition_return extends ParserRuleReturnScope {
        public AxisSet set = null;;
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "simple_set_definition"
    // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:111:1: simple_set_definition returns [AxisSet set = null;] : ( LBRACE (tupleListDef= tuple_list )? RBRACE |tupleDef= tuple_definition |memberDef= member_definition |allmemberDef= all_member_definition |rangeDef= range_set_definition );
    public final sdxParser.simple_set_definition_return simple_set_definition() throws RecognitionException {
        sdxParser.simple_set_definition_return retval = new sdxParser.simple_set_definition_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token LBRACE19=null;
        Token RBRACE20=null;
        sdxParser.tuple_list_return tupleListDef =null;

        sdxParser.tuple_definition_return tupleDef =null;

        sdxParser.member_definition_return memberDef =null;

        sdxParser.all_member_definition_return allmemberDef =null;

        sdxParser.range_set_definition_return rangeDef =null;


        CommonTree LBRACE19_tree=null;
        CommonTree RBRACE20_tree=null;

        try {
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:112:2: ( LBRACE (tupleListDef= tuple_list )? RBRACE |tupleDef= tuple_definition |memberDef= member_definition |allmemberDef= all_member_definition |rangeDef= range_set_definition )
            int alt8=5;
            alt8 = dfa8.predict(input);
            switch (alt8) {
                case 1 :
                    // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:113:3: LBRACE (tupleListDef= tuple_list )? RBRACE
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    LBRACE19=(Token)match(input,LBRACE,FOLLOW_LBRACE_in_simple_set_definition493); 
                    LBRACE19_tree = 
                    (CommonTree)adaptor.create(LBRACE19)
                    ;
                    adaptor.addChild(root_0, LBRACE19_tree);


                    // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:113:10: (tupleListDef= tuple_list )?
                    int alt7=2;
                    int LA7_0 = input.LA(1);

                    if ( (LA7_0==LPAREN||LA7_0==QUOTED_ID) ) {
                        alt7=1;
                    }
                    switch (alt7) {
                        case 1 :
                            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:113:11: tupleListDef= tuple_list
                            {
                            pushFollow(FOLLOW_tuple_list_in_simple_set_definition500);
                            tupleListDef=tuple_list();

                            state._fsp--;

                            adaptor.addChild(root_0, tupleListDef.getTree());

                            }
                            break;

                    }


                    RBRACE20=(Token)match(input,RBRACE,FOLLOW_RBRACE_in_simple_set_definition504); 
                    RBRACE20_tree = 
                    (CommonTree)adaptor.create(RBRACE20)
                    ;
                    adaptor.addChild(root_0, RBRACE20_tree);


                    retval.set = tupleListDef.tupleSet;

                    }
                    break;
                case 2 :
                    // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:114:4: tupleDef= tuple_definition
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    pushFollow(FOLLOW_tuple_definition_in_simple_set_definition515);
                    tupleDef=tuple_definition();

                    state._fsp--;

                    adaptor.addChild(root_0, tupleDef.getTree());

                    retval.set = tupleDef.tuple;

                    }
                    break;
                case 3 :
                    // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:115:4: memberDef= member_definition
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    pushFollow(FOLLOW_member_definition_in_simple_set_definition526);
                    memberDef=member_definition();

                    state._fsp--;

                    adaptor.addChild(root_0, memberDef.getTree());

                    retval.set = memberDef.member;

                    }
                    break;
                case 4 :
                    // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:116:4: allmemberDef= all_member_definition
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    pushFollow(FOLLOW_all_member_definition_in_simple_set_definition537);
                    allmemberDef=all_member_definition();

                    state._fsp--;

                    adaptor.addChild(root_0, allmemberDef.getTree());

                    retval.set = allmemberDef.set;

                    }
                    break;
                case 5 :
                    // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:117:4: rangeDef= range_set_definition
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    pushFollow(FOLLOW_range_set_definition_in_simple_set_definition548);
                    rangeDef=range_set_definition();

                    state._fsp--;

                    adaptor.addChild(root_0, rangeDef.getTree());

                    retval.set = rangeDef.set;

                    }
                    break;

            }
            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "simple_set_definition"


    public static class set_operation_return extends ParserRuleReturnScope {
        public CompositeAxisSet opSet = null;;
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "set_operation"
    // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:120:1: set_operation returns [CompositeAxisSet opSet = null;] : ( ASTERISK | PLUS );
    public final sdxParser.set_operation_return set_operation() throws RecognitionException {
        sdxParser.set_operation_return retval = new sdxParser.set_operation_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token ASTERISK21=null;
        Token PLUS22=null;

        CommonTree ASTERISK21_tree=null;
        CommonTree PLUS22_tree=null;

        try {
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:121:2: ( ASTERISK | PLUS )
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==ASTERISK) ) {
                alt9=1;
            }
            else if ( (LA9_0==PLUS) ) {
                alt9=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 9, 0, input);

                throw nvae;

            }
            switch (alt9) {
                case 1 :
                    // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:121:5: ASTERISK
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    ASTERISK21=(Token)match(input,ASTERISK,FOLLOW_ASTERISK_in_set_operation566); 
                    ASTERISK21_tree = 
                    (CommonTree)adaptor.create(ASTERISK21)
                    ;
                    adaptor.addChild(root_0, ASTERISK21_tree);


                    retval.opSet = new CrossJoin();

                    }
                    break;
                case 2 :
                    // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:122:4: PLUS
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    PLUS22=(Token)match(input,PLUS,FOLLOW_PLUS_in_set_operation573); 
                    PLUS22_tree = 
                    (CommonTree)adaptor.create(PLUS22)
                    ;
                    adaptor.addChild(root_0, PLUS22_tree);


                    retval.opSet = new Union();

                    }
                    break;

            }
            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "set_operation"


    public static class tuple_list_return extends ParserRuleReturnScope {
        public TupleSet tupleSet = new TupleSet();;
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "tuple_list"
    // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:125:1: tuple_list returns [TupleSet tupleSet = new TupleSet();] : (firstTupleDef= tuple_definition ( COMMA tupleDef= tuple_definition )? | member_list );
    public final sdxParser.tuple_list_return tuple_list() throws RecognitionException {
        sdxParser.tuple_list_return retval = new sdxParser.tuple_list_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token COMMA23=null;
        sdxParser.tuple_definition_return firstTupleDef =null;

        sdxParser.tuple_definition_return tupleDef =null;

        sdxParser.member_list_return member_list24 =null;


        CommonTree COMMA23_tree=null;

        try {
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:126:2: (firstTupleDef= tuple_definition ( COMMA tupleDef= tuple_definition )? | member_list )
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0==LPAREN) ) {
                alt11=1;
            }
            else if ( (LA11_0==QUOTED_ID) ) {
                alt11=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 11, 0, input);

                throw nvae;

            }
            switch (alt11) {
                case 1 :
                    // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:126:4: firstTupleDef= tuple_definition ( COMMA tupleDef= tuple_definition )?
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    pushFollow(FOLLOW_tuple_definition_in_tuple_list594);
                    firstTupleDef=tuple_definition();

                    state._fsp--;

                    adaptor.addChild(root_0, firstTupleDef.getTree());

                    retval.tupleSet.add(firstTupleDef.tuple);

                    // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:126:81: ( COMMA tupleDef= tuple_definition )?
                    int alt10=2;
                    int LA10_0 = input.LA(1);

                    if ( (LA10_0==COMMA) ) {
                        alt10=1;
                    }
                    switch (alt10) {
                        case 1 :
                            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:126:82: COMMA tupleDef= tuple_definition
                            {
                            COMMA23=(Token)match(input,COMMA,FOLLOW_COMMA_in_tuple_list599); 
                            COMMA23_tree = 
                            (CommonTree)adaptor.create(COMMA23)
                            ;
                            adaptor.addChild(root_0, COMMA23_tree);


                            pushFollow(FOLLOW_tuple_definition_in_tuple_list605);
                            tupleDef=tuple_definition();

                            state._fsp--;

                            adaptor.addChild(root_0, tupleDef.getTree());

                            retval.tupleSet.add(tupleDef.tuple);

                            }
                            break;

                    }


                    }
                    break;
                case 2 :
                    // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:127:5: member_list
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    pushFollow(FOLLOW_member_list_in_tuple_list615);
                    member_list24=member_list();

                    state._fsp--;

                    adaptor.addChild(root_0, member_list24.getTree());

                    }
                    break;

            }
            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "tuple_list"


    public static class tuple_definition_return extends ParserRuleReturnScope {
        public Tuple tuple = null;;
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "tuple_definition"
    // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:130:1: tuple_definition returns [Tuple tuple = null;] : LPAREN listDef= member_list RPAREN ;
    public final sdxParser.tuple_definition_return tuple_definition() throws RecognitionException {
        sdxParser.tuple_definition_return retval = new sdxParser.tuple_definition_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token LPAREN25=null;
        Token RPAREN26=null;
        sdxParser.member_list_return listDef =null;


        CommonTree LPAREN25_tree=null;
        CommonTree RPAREN26_tree=null;

        try {
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:131:2: ( LPAREN listDef= member_list RPAREN )
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:131:4: LPAREN listDef= member_list RPAREN
            {
            root_0 = (CommonTree)adaptor.nil();


            LPAREN25=(Token)match(input,LPAREN,FOLLOW_LPAREN_in_tuple_definition631); 
            LPAREN25_tree = 
            (CommonTree)adaptor.create(LPAREN25)
            ;
            adaptor.addChild(root_0, LPAREN25_tree);


            pushFollow(FOLLOW_member_list_in_tuple_definition637);
            listDef=member_list();

            state._fsp--;

            adaptor.addChild(root_0, listDef.getTree());

            RPAREN26=(Token)match(input,RPAREN,FOLLOW_RPAREN_in_tuple_definition639); 
            RPAREN26_tree = 
            (CommonTree)adaptor.create(RPAREN26)
            ;
            adaptor.addChild(root_0, RPAREN26_tree);


            retval.tuple = listDef.memberList.size() == 1 ? listDef.memberList.get(0) : new MemberTuple(listDef.memberList);

            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "tuple_definition"


    public static class member_list_return extends ParserRuleReturnScope {
        public ArrayList<Member> memberList = new ArrayList<>();;
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "member_list"
    // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:134:1: member_list returns [ArrayList<Member> memberList = new ArrayList<>();] : firstMemberDef= member_definition ( COMMA memberDef= member_definition )* ;
    public final sdxParser.member_list_return member_list() throws RecognitionException {
        sdxParser.member_list_return retval = new sdxParser.member_list_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token COMMA27=null;
        sdxParser.member_definition_return firstMemberDef =null;

        sdxParser.member_definition_return memberDef =null;


        CommonTree COMMA27_tree=null;

        try {
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:135:2: (firstMemberDef= member_definition ( COMMA memberDef= member_definition )* )
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:135:4: firstMemberDef= member_definition ( COMMA memberDef= member_definition )*
            {
            root_0 = (CommonTree)adaptor.nil();


            pushFollow(FOLLOW_member_definition_in_member_list660);
            firstMemberDef=member_definition();

            state._fsp--;

            adaptor.addChild(root_0, firstMemberDef.getTree());

            retval.memberList.add(firstMemberDef.member);

            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:135:87: ( COMMA memberDef= member_definition )*
            loop12:
            do {
                int alt12=2;
                int LA12_0 = input.LA(1);

                if ( (LA12_0==COMMA) ) {
                    alt12=1;
                }


                switch (alt12) {
            	case 1 :
            	    // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:135:88: COMMA memberDef= member_definition
            	    {
            	    COMMA27=(Token)match(input,COMMA,FOLLOW_COMMA_in_member_list665); 
            	    COMMA27_tree = 
            	    (CommonTree)adaptor.create(COMMA27)
            	    ;
            	    adaptor.addChild(root_0, COMMA27_tree);


            	    pushFollow(FOLLOW_member_definition_in_member_list671);
            	    memberDef=member_definition();

            	    state._fsp--;

            	    adaptor.addChild(root_0, memberDef.getTree());

            	    retval.memberList.add(memberDef.member);

            	    }
            	    break;

            	default :
            	    break loop12;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "member_list"


    public static class all_member_definition_return extends ParserRuleReturnScope {
        public AllMemberSet set = null;;
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "all_member_definition"
    // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:138:1: all_member_definition returns [AllMemberSet set = null;] : hierarchy= quoted_identifier path= member_path_definition DOT level= quoted_identifier DOT MEMBERS ;
    public final sdxParser.all_member_definition_return all_member_definition() throws RecognitionException {
        sdxParser.all_member_definition_return retval = new sdxParser.all_member_definition_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token DOT28=null;
        Token DOT29=null;
        Token MEMBERS30=null;
        sdxParser.quoted_identifier_return hierarchy =null;

        sdxParser.member_path_definition_return path =null;

        sdxParser.quoted_identifier_return level =null;


        CommonTree DOT28_tree=null;
        CommonTree DOT29_tree=null;
        CommonTree MEMBERS30_tree=null;

        try {
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:139:2: (hierarchy= quoted_identifier path= member_path_definition DOT level= quoted_identifier DOT MEMBERS )
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:139:4: hierarchy= quoted_identifier path= member_path_definition DOT level= quoted_identifier DOT MEMBERS
            {
            root_0 = (CommonTree)adaptor.nil();


            pushFollow(FOLLOW_quoted_identifier_in_all_member_definition694);
            hierarchy=quoted_identifier();

            state._fsp--;

            adaptor.addChild(root_0, hierarchy.getTree());

            pushFollow(FOLLOW_member_path_definition_in_all_member_definition700);
            path=member_path_definition();

            state._fsp--;

            adaptor.addChild(root_0, path.getTree());

            DOT28=(Token)match(input,DOT,FOLLOW_DOT_in_all_member_definition702); 
            DOT28_tree = 
            (CommonTree)adaptor.create(DOT28)
            ;
            adaptor.addChild(root_0, DOT28_tree);


            pushFollow(FOLLOW_quoted_identifier_in_all_member_definition708);
            level=quoted_identifier();

            state._fsp--;

            adaptor.addChild(root_0, level.getTree());

            DOT29=(Token)match(input,DOT,FOLLOW_DOT_in_all_member_definition710); 
            DOT29_tree = 
            (CommonTree)adaptor.create(DOT29)
            ;
            adaptor.addChild(root_0, DOT29_tree);


            MEMBERS30=(Token)match(input,MEMBERS,FOLLOW_MEMBERS_in_all_member_definition712); 
            MEMBERS30_tree = 
            (CommonTree)adaptor.create(MEMBERS30)
            ;
            adaptor.addChild(root_0, MEMBERS30_tree);


            retval.set = new AllMemberSet(Cube.getMemberHierarchy(hierarchy.idValue), level.idValue, buildMemberFromPath(path.members, Cube.getHierarchy(hierarchy.idValue)));

            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "all_member_definition"


    public static class range_set_definition_return extends ParserRuleReturnScope {
        public RangeSet set = null;;
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "range_set_definition"
    // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:143:1: range_set_definition returns [RangeSet set = null;] : startDef= member_definition COLON endDef= member_definition ;
    public final sdxParser.range_set_definition_return range_set_definition() throws RecognitionException {
        sdxParser.range_set_definition_return retval = new sdxParser.range_set_definition_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token COLON31=null;
        sdxParser.member_definition_return startDef =null;

        sdxParser.member_definition_return endDef =null;


        CommonTree COLON31_tree=null;

        try {
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:144:2: (startDef= member_definition COLON endDef= member_definition )
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:144:4: startDef= member_definition COLON endDef= member_definition
            {
            root_0 = (CommonTree)adaptor.nil();


            pushFollow(FOLLOW_member_definition_in_range_set_definition736);
            startDef=member_definition();

            state._fsp--;

            adaptor.addChild(root_0, startDef.getTree());

            COLON31=(Token)match(input,COLON,FOLLOW_COLON_in_range_set_definition738); 
            COLON31_tree = 
            (CommonTree)adaptor.create(COLON31)
            ;
            adaptor.addChild(root_0, COLON31_tree);


            pushFollow(FOLLOW_member_definition_in_range_set_definition744);
            endDef=member_definition();

            state._fsp--;

            adaptor.addChild(root_0, endDef.getTree());

            retval.set = new RangeSet(Cube.getOrderedHierarchy(startDef.member.getHierarchy().getName()), startDef.member, endDef.member);

            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "range_set_definition"


    public static class member_definition_return extends ParserRuleReturnScope {
        public SimpleMember member = null;;
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "member_definition"
    // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:147:1: member_definition returns [SimpleMember member = null;] : hierarchy= quoted_identifier path= non_empty_member_path_definition ;
    public final sdxParser.member_definition_return member_definition() throws RecognitionException {
        sdxParser.member_definition_return retval = new sdxParser.member_definition_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        sdxParser.quoted_identifier_return hierarchy =null;

        sdxParser.non_empty_member_path_definition_return path =null;



        try {
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:148:2: (hierarchy= quoted_identifier path= non_empty_member_path_definition )
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:148:5: hierarchy= quoted_identifier path= non_empty_member_path_definition
            {
            root_0 = (CommonTree)adaptor.nil();


            pushFollow(FOLLOW_quoted_identifier_in_member_definition766);
            hierarchy=quoted_identifier();

            state._fsp--;

            adaptor.addChild(root_0, hierarchy.getTree());

            pushFollow(FOLLOW_non_empty_member_path_definition_in_member_definition772);
            path=non_empty_member_path_definition();

            state._fsp--;

            adaptor.addChild(root_0, path.getTree());

            retval.member = buildMemberFromPath(path.members, Cube.getHierarchy(hierarchy.idValue));

            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "member_definition"


    public static class non_empty_member_path_definition_return extends ParserRuleReturnScope {
        public ArrayList<SimpleMember> members = new ArrayList<SimpleMember>();;
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "non_empty_member_path_definition"
    // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:151:1: non_empty_member_path_definition returns [ArrayList<SimpleMember> members = new ArrayList<SimpleMember>();] : ( DOT memberDef= one_level_member_definition )+ ;
    public final sdxParser.non_empty_member_path_definition_return non_empty_member_path_definition() throws RecognitionException {
        sdxParser.non_empty_member_path_definition_return retval = new sdxParser.non_empty_member_path_definition_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token DOT32=null;
        sdxParser.one_level_member_definition_return memberDef =null;


        CommonTree DOT32_tree=null;

        try {
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:152:2: ( ( DOT memberDef= one_level_member_definition )+ )
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:152:4: ( DOT memberDef= one_level_member_definition )+
            {
            root_0 = (CommonTree)adaptor.nil();


            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:152:4: ( DOT memberDef= one_level_member_definition )+
            int cnt13=0;
            loop13:
            do {
                int alt13=2;
                int LA13_0 = input.LA(1);

                if ( (LA13_0==DOT) ) {
                    alt13=1;
                }


                switch (alt13) {
            	case 1 :
            	    // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:152:5: DOT memberDef= one_level_member_definition
            	    {
            	    DOT32=(Token)match(input,DOT,FOLLOW_DOT_in_non_empty_member_path_definition791); 
            	    DOT32_tree = 
            	    (CommonTree)adaptor.create(DOT32)
            	    ;
            	    adaptor.addChild(root_0, DOT32_tree);


            	    pushFollow(FOLLOW_one_level_member_definition_in_non_empty_member_path_definition797);
            	    memberDef=one_level_member_definition();

            	    state._fsp--;

            	    adaptor.addChild(root_0, memberDef.getTree());

            	    retval.members.add(memberDef.member);

            	    }
            	    break;

            	default :
            	    if ( cnt13 >= 1 ) break loop13;
                        EarlyExitException eee =
                            new EarlyExitException(13, input);
                        throw eee;
                }
                cnt13++;
            } while (true);


            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "non_empty_member_path_definition"


    public static class member_path_definition_return extends ParserRuleReturnScope {
        public ArrayList<SimpleMember> members = new ArrayList<SimpleMember>();;
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "member_path_definition"
    // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:155:1: member_path_definition returns [ArrayList<SimpleMember> members = new ArrayList<SimpleMember>();] : ( DOT memberDef= one_level_member_definition )* ;
    public final sdxParser.member_path_definition_return member_path_definition() throws RecognitionException {
        sdxParser.member_path_definition_return retval = new sdxParser.member_path_definition_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token DOT33=null;
        sdxParser.one_level_member_definition_return memberDef =null;


        CommonTree DOT33_tree=null;

        try {
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:156:2: ( ( DOT memberDef= one_level_member_definition )* )
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:156:4: ( DOT memberDef= one_level_member_definition )*
            {
            root_0 = (CommonTree)adaptor.nil();


            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:156:4: ( DOT memberDef= one_level_member_definition )*
            loop14:
            do {
                int alt14=2;
                int LA14_0 = input.LA(1);

                if ( (LA14_0==DOT) ) {
                    int LA14_1 = input.LA(2);

                    if ( (LA14_1==QUOTED_ID) ) {
                        int LA14_2 = input.LA(3);

                        if ( (LA14_2==DOT) ) {
                            int LA14_3 = input.LA(4);

                            if ( (LA14_3==AMP_QUOTED_ID) ) {
                                alt14=1;
                            }


                        }


                    }


                }


                switch (alt14) {
            	case 1 :
            	    // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:156:5: DOT memberDef= one_level_member_definition
            	    {
            	    DOT33=(Token)match(input,DOT,FOLLOW_DOT_in_member_path_definition817); 
            	    DOT33_tree = 
            	    (CommonTree)adaptor.create(DOT33)
            	    ;
            	    adaptor.addChild(root_0, DOT33_tree);


            	    pushFollow(FOLLOW_one_level_member_definition_in_member_path_definition823);
            	    memberDef=one_level_member_definition();

            	    state._fsp--;

            	    adaptor.addChild(root_0, memberDef.getTree());

            	    retval.members.add(memberDef.member);

            	    }
            	    break;

            	default :
            	    break loop14;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "member_path_definition"


    public static class one_level_member_definition_return extends ParserRuleReturnScope {
        public SimpleMember member = null;
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "one_level_member_definition"
    // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:159:1: one_level_member_definition returns [SimpleMember member = null] : level= quoted_identifier DOT memberName= amp_quoted_identifier ;
    public final sdxParser.one_level_member_definition_return one_level_member_definition() throws RecognitionException {
        sdxParser.one_level_member_definition_return retval = new sdxParser.one_level_member_definition_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token DOT34=null;
        sdxParser.quoted_identifier_return level =null;

        sdxParser.amp_quoted_identifier_return memberName =null;


        CommonTree DOT34_tree=null;

        try {
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:160:2: (level= quoted_identifier DOT memberName= amp_quoted_identifier )
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:160:4: level= quoted_identifier DOT memberName= amp_quoted_identifier
            {
            root_0 = (CommonTree)adaptor.nil();


            pushFollow(FOLLOW_quoted_identifier_in_one_level_member_definition846);
            level=quoted_identifier();

            state._fsp--;

            adaptor.addChild(root_0, level.getTree());

            DOT34=(Token)match(input,DOT,FOLLOW_DOT_in_one_level_member_definition848); 
            DOT34_tree = 
            (CommonTree)adaptor.create(DOT34)
            ;
            adaptor.addChild(root_0, DOT34_tree);


            pushFollow(FOLLOW_amp_quoted_identifier_in_one_level_member_definition854);
            memberName=amp_quoted_identifier();

            state._fsp--;

            adaptor.addChild(root_0, memberName.getTree());

            retval.member = new SimpleMember(null, level.idValue, memberName.idValue); 

            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "one_level_member_definition"


    public static class unquoted_identifier_return extends ParserRuleReturnScope {
        public String name = null;
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "unquoted_identifier"
    // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:163:1: unquoted_identifier returns [String name = null] : nameToken= ID ;
    public final sdxParser.unquoted_identifier_return unquoted_identifier() throws RecognitionException {
        sdxParser.unquoted_identifier_return retval = new sdxParser.unquoted_identifier_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token nameToken=null;

        CommonTree nameToken_tree=null;

        try {
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:164:2: (nameToken= ID )
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:164:4: nameToken= ID
            {
            root_0 = (CommonTree)adaptor.nil();


            nameToken=(Token)match(input,ID,FOLLOW_ID_in_unquoted_identifier882); 
            nameToken_tree = 
            (CommonTree)adaptor.create(nameToken)
            ;
            adaptor.addChild(root_0, nameToken_tree);


            retval.name = nameToken.getText();

            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "unquoted_identifier"


    public static class amp_quoted_identifier_return extends ParserRuleReturnScope {
        public String idValue = null;;
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "amp_quoted_identifier"
    // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:167:1: amp_quoted_identifier returns [String idValue = null;] : idToken= AMP_QUOTED_ID ;
    public final sdxParser.amp_quoted_identifier_return amp_quoted_identifier() throws RecognitionException {
        sdxParser.amp_quoted_identifier_return retval = new sdxParser.amp_quoted_identifier_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token idToken=null;

        CommonTree idToken_tree=null;

        try {
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:168:2: (idToken= AMP_QUOTED_ID )
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:168:4: idToken= AMP_QUOTED_ID
            {
            root_0 = (CommonTree)adaptor.nil();


            idToken=(Token)match(input,AMP_QUOTED_ID,FOLLOW_AMP_QUOTED_ID_in_amp_quoted_identifier908); 
            idToken_tree = 
            (CommonTree)adaptor.create(idToken)
            ;
            adaptor.addChild(root_0, idToken_tree);


            String allStr = idToken.getText(); retval.idValue = allStr.substring(2, allStr.length() - 1);

            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "amp_quoted_identifier"


    public static class quoted_identifier_return extends ParserRuleReturnScope {
        public String idValue = null;;
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "quoted_identifier"
    // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:171:1: quoted_identifier returns [String idValue = null;] : idToken= QUOTED_ID ;
    public final sdxParser.quoted_identifier_return quoted_identifier() throws RecognitionException {
        sdxParser.quoted_identifier_return retval = new sdxParser.quoted_identifier_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token idToken=null;

        CommonTree idToken_tree=null;

        try {
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:172:2: (idToken= QUOTED_ID )
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:172:4: idToken= QUOTED_ID
            {
            root_0 = (CommonTree)adaptor.nil();


            idToken=(Token)match(input,QUOTED_ID,FOLLOW_QUOTED_ID_in_quoted_identifier933); 
            idToken_tree = 
            (CommonTree)adaptor.create(idToken)
            ;
            adaptor.addChild(root_0, idToken_tree);


            String allStr = idToken.getText(); retval.idValue = allStr.substring(1, allStr.length() - 1);

            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "quoted_identifier"


    public static class fun_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "fun"
    // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:175:1: fun : formulaToken= FUN ;
    public final sdxParser.fun_return fun() throws RecognitionException {
        sdxParser.fun_return retval = new sdxParser.fun_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token formulaToken=null;

        CommonTree formulaToken_tree=null;

        try {
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:175:5: (formulaToken= FUN )
            // D:\\Synapsense\\repos\\middle\\es\\es-api\\src\\main\\java\\com\\synapsense\\service\\reporting\\lang\\sdx.g:175:7: formulaToken= FUN
            {
            root_0 = (CommonTree)adaptor.nil();


            formulaToken=(Token)match(input,FUN,FOLLOW_FUN_in_fun946); 
            formulaToken_tree = 
            (CommonTree)adaptor.create(formulaToken)
            ;
            adaptor.addChild(root_0, formulaToken_tree);


            dq.setFormula(formulaToken.getText());

            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "fun"

    // Delegated rules


    protected DFA8 dfa8 = new DFA8(this);
    static final String DFA8_eotS =
        "\13\uffff";
    static final String DFA8_eofS =
        "\10\uffff\1\11\2\uffff";
    static final String DFA8_minS =
        "\1\22\2\uffff\1\13\1\36\1\13\1\4\1\uffff\1\5\2\uffff";
    static final String DFA8_maxS =
        "\1\36\2\uffff\1\13\1\36\1\13\1\26\1\uffff\1\34\2\uffff";
    static final String DFA8_acceptS =
        "\1\uffff\1\1\1\2\4\uffff\1\4\1\uffff\1\3\1\5";
    static final String DFA8_specialS =
        "\13\uffff}>";
    static final String[] DFA8_transitionS = {
            "\1\1\1\uffff\1\2\11\uffff\1\3",
            "",
            "",
            "\1\4",
            "\1\5",
            "\1\6",
            "\1\10\21\uffff\1\7",
            "",
            "\1\11\1\uffff\1\12\3\uffff\1\4\17\uffff\2\11",
            "",
            ""
    };

    static final short[] DFA8_eot = DFA.unpackEncodedString(DFA8_eotS);
    static final short[] DFA8_eof = DFA.unpackEncodedString(DFA8_eofS);
    static final char[] DFA8_min = DFA.unpackEncodedStringToUnsignedChars(DFA8_minS);
    static final char[] DFA8_max = DFA.unpackEncodedStringToUnsignedChars(DFA8_maxS);
    static final short[] DFA8_accept = DFA.unpackEncodedString(DFA8_acceptS);
    static final short[] DFA8_special = DFA.unpackEncodedString(DFA8_specialS);
    static final short[][] DFA8_transition;

    static {
        int numStates = DFA8_transitionS.length;
        DFA8_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA8_transition[i] = DFA.unpackEncodedString(DFA8_transitionS[i]);
        }
    }

    class DFA8 extends DFA {

        public DFA8(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 8;
            this.eot = DFA8_eot;
            this.eof = DFA8_eof;
            this.min = DFA8_min;
            this.max = DFA8_max;
            this.accept = DFA8_accept;
            this.special = DFA8_special;
            this.transition = DFA8_transition;
        }
        public String getDescription() {
            return "111:1: simple_set_definition returns [AxisSet set = null;] : ( LBRACE (tupleListDef= tuple_list )? RBRACE |tupleDef= tuple_definition |memberDef= member_definition |allmemberDef= all_member_definition |rangeDef= range_set_definition );";
        }
    }
 

    public static final BitSet FOLLOW_select_statement_in_sdx_statement273 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_sdx_statement275 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_SELECT_in_select_statement288 = new BitSet(new long[]{0x0000000042144000L});
    public static final BitSet FOLLOW_fun_in_select_statement291 = new BitSet(new long[]{0x0000000042140000L});
    public static final BitSet FOLLOW_axis_specification_list_in_select_statement295 = new BitSet(new long[]{0x0000004000000002L});
    public static final BitSet FOLLOW_WHERE_in_select_statement298 = new BitSet(new long[]{0x0000000040140000L});
    public static final BitSet FOLLOW_slicer_specification_in_select_statement300 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_axis_specification_in_axis_specification_list317 = new BitSet(new long[]{0x0000000000000202L});
    public static final BitSet FOLLOW_COMMA_in_axis_specification_list320 = new BitSet(new long[]{0x0000000042140000L});
    public static final BitSet FOLLOW_axis_specification_in_axis_specification_list322 = new BitSet(new long[]{0x0000000000000202L});
    public static final BitSet FOLLOW_NON_in_axis_specification340 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_EMPTY_in_axis_specification342 = new BitSet(new long[]{0x0000000040140000L});
    public static final BitSet FOLLOW_set_definition_in_axis_specification350 = new BitSet(new long[]{0x0000000008000000L});
    public static final BitSet FOLLOW_ON_in_axis_specification352 = new BitSet(new long[]{0x0000000100000140L});
    public static final BitSet FOLLOW_axis_name_in_axis_specification358 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_set_definition_in_slicer_specification380 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ROWS_in_axis_name403 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_COLUMNS_in_axis_name411 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_AXIS_in_axis_name420 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_LPAREN_in_axis_name426 = new BitSet(new long[]{0x0000000004000000L});
    public static final BitSet FOLLOW_NUMBER_in_axis_name428 = new BitSet(new long[]{0x0000000200000000L});
    public static final BitSet FOLLOW_RPAREN_in_axis_name430 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_simple_set_definition_in_set_definition455 = new BitSet(new long[]{0x0000000010000022L});
    public static final BitSet FOLLOW_set_operation_in_set_definition464 = new BitSet(new long[]{0x0000000040140000L});
    public static final BitSet FOLLOW_simple_set_definition_in_set_definition470 = new BitSet(new long[]{0x0000000010000022L});
    public static final BitSet FOLLOW_LBRACE_in_simple_set_definition493 = new BitSet(new long[]{0x00000000C0100000L});
    public static final BitSet FOLLOW_tuple_list_in_simple_set_definition500 = new BitSet(new long[]{0x0000000080000000L});
    public static final BitSet FOLLOW_RBRACE_in_simple_set_definition504 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_tuple_definition_in_simple_set_definition515 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_member_definition_in_simple_set_definition526 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_all_member_definition_in_simple_set_definition537 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_range_set_definition_in_simple_set_definition548 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ASTERISK_in_set_operation566 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_PLUS_in_set_operation573 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_tuple_definition_in_tuple_list594 = new BitSet(new long[]{0x0000000000000202L});
    public static final BitSet FOLLOW_COMMA_in_tuple_list599 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_tuple_definition_in_tuple_list605 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_member_list_in_tuple_list615 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_LPAREN_in_tuple_definition631 = new BitSet(new long[]{0x0000000040000000L});
    public static final BitSet FOLLOW_member_list_in_tuple_definition637 = new BitSet(new long[]{0x0000000200000000L});
    public static final BitSet FOLLOW_RPAREN_in_tuple_definition639 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_member_definition_in_member_list660 = new BitSet(new long[]{0x0000000000000202L});
    public static final BitSet FOLLOW_COMMA_in_member_list665 = new BitSet(new long[]{0x0000000040000000L});
    public static final BitSet FOLLOW_member_definition_in_member_list671 = new BitSet(new long[]{0x0000000000000202L});
    public static final BitSet FOLLOW_quoted_identifier_in_all_member_definition694 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_member_path_definition_in_all_member_definition700 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_DOT_in_all_member_definition702 = new BitSet(new long[]{0x0000000040000000L});
    public static final BitSet FOLLOW_quoted_identifier_in_all_member_definition708 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_DOT_in_all_member_definition710 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_MEMBERS_in_all_member_definition712 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_member_definition_in_range_set_definition736 = new BitSet(new long[]{0x0000000000000080L});
    public static final BitSet FOLLOW_COLON_in_range_set_definition738 = new BitSet(new long[]{0x0000000040000000L});
    public static final BitSet FOLLOW_member_definition_in_range_set_definition744 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_quoted_identifier_in_member_definition766 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_non_empty_member_path_definition_in_member_definition772 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_DOT_in_non_empty_member_path_definition791 = new BitSet(new long[]{0x0000000040000000L});
    public static final BitSet FOLLOW_one_level_member_definition_in_non_empty_member_path_definition797 = new BitSet(new long[]{0x0000000000000802L});
    public static final BitSet FOLLOW_DOT_in_member_path_definition817 = new BitSet(new long[]{0x0000000040000000L});
    public static final BitSet FOLLOW_one_level_member_definition_in_member_path_definition823 = new BitSet(new long[]{0x0000000000000802L});
    public static final BitSet FOLLOW_quoted_identifier_in_one_level_member_definition846 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_DOT_in_one_level_member_definition848 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_amp_quoted_identifier_in_one_level_member_definition854 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ID_in_unquoted_identifier882 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_AMP_QUOTED_ID_in_amp_quoted_identifier908 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_QUOTED_ID_in_quoted_identifier933 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_FUN_in_fun946 = new BitSet(new long[]{0x0000000000000002L});

}