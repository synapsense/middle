package com.synapsense.impl.alerting.macros;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MessageMacro extends AbstractPatternMacro {

	private static final Pattern pattern = Pattern.compile("\\$(?i)message\\$");

	@Override
	public String expandIn(String input, MacroContext ctx) {
		if (input == null)
			return null;
		if (ctx.alert == null)
			throw new IllegalArgumentException("MacroContext.alert must be not null");
		return super.expandIn(input, ctx);
	}

	@Override
	protected String expand(Matcher m, MacroContext ctx) {
		return Matcher.quoteReplacement(
				AlertMacroFactory.getFormattingMacros().expandIn(
					dataToConvMacro.expandIn(ctx.alert.getMessage(), ctx),
					ctx)
				);
	}

	@Override
	protected Pattern getPattern() {
		return pattern;
	}

	private Macro dataToConvMacro = new AbstractDataValueMacro() {

		@Override
		public String expandIn(String input, MacroContext ctx) {
			if (input == null)
				return Macro.DEFAULT_EXPANSION;
			// message macro saves message's text
			// but expands data macro into comma-separated conv
			StringBuffer sb = new StringBuffer();
			Matcher m = getPattern().matcher(input);
			while (m.find()) {
				if (m.group(3) != null) {// dimension is specified
					m.appendReplacement(sb, "\\$conv($2,$3)\\$");
				} else {
					m.appendReplacement(sb, "$2");
				}
			}
			m.appendTail(sb);
			return sb.toString();
		}

		@Override
		protected String expand(Matcher m, MacroContext ctx) {
			return null; // not used anyway - expandIn() is overridden
		}

	};

}
