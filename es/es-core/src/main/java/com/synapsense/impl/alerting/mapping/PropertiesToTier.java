package com.synapsense.impl.alerting.mapping;

import java.util.Collection;
import java.util.Map;

import com.synapsense.dto.PriorityTier;
import com.synapsense.dto.ValueTO;
import com.synapsense.impl.utils.EnvironmentUtils;
import com.synapsense.util.CollectionUtils.Functor;

public class PropertiesToTier implements Functor<PriorityTier, Collection<ValueTO>> {
	@Override
	public PriorityTier apply(Collection<ValueTO> input) {
		Map<String, Object> values = EnvironmentUtils.valuesAsMap(input);
		String tierName = (String) values.get("name");
		long tierInterval = (Long) values.get("interval");
		int tierNotifications = (Integer) values.get("notificationAttempts");
		boolean tierSendEsc = (Integer) values.get("sendEscalation") == 1;
		boolean tierSendAck = (Integer) values.get("sendAck") == 1;
		boolean tierSendRes = (Integer) values.get("sendResolve") == 1;

		PriorityTier tier = new PriorityTier(tierName, tierInterval, tierNotifications, tierSendEsc, tierSendAck,
		        tierSendRes);

		return tier;
	}
}
