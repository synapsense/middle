package com.synapsense.impl.performer.ntlog;

import javax.annotation.PostConstruct;
import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.DependsOn;
import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Singleton;
import javax.ejb.Startup;

import com.synapsense.service.impl.notification.NotificationException;
import com.synapsense.service.impl.notification.Transport;
import org.apache.log4j.Logger;

import com.synapsense.cdi.MBean;
import com.synapsense.dto.AlertPriority;
import com.synapsense.dto.Message;
import com.synapsense.service.AlertService;

@Startup
@Singleton(name = "NtLogPerformer")
@DependsOn({ "Registrar", "SynapCache" })
@Remote(Transport.class)
@ConcurrencyManagement(ConcurrencyManagementType.BEAN)
@MBean("com.synapsense:type=NtLogPerformer")
public class NtLogPerformer implements NtLogPerformerMBean, Transport {

	private final static Logger logger = Logger.getLogger(NtLogPerformerMBean.class);
	private final String SOURCE_NAME = "SynapSense Environment Server";
	private final String PERF_NAME = "NtLogPerformer";
	private NtLogger ntLogger = null;
	private AlertService alertService;

	@EJB
	public void setAlertService(AlertService service) {
		alertService = service;
	}

	@PostConstruct
	public void start() throws Exception {
		logger.info("Creating the " + PERF_NAME + " alerts performer");
		try {
			ntLogger = new NtLogger(NtLogPerformerMBean.class.toString(), SOURCE_NAME);
		} catch (UnsatisfiedLinkError e) {
			logger.warn("Unable to start " + PERF_NAME
			        + " due to missing native librares in modules\\org\\apache\\log4j\\main\\lib\\");
		}
	}

	@Override
	public void addRecord(String level, String record) {
		if (ntLogger != null) {
			ntLogger.log(NtLogPriority.getLevelByEsPriority(level), record);
		}
	}

	@Override
	public boolean isValid(Message message) {
		AlertPriority priority = alertService.getAlertPriority(message.getPriority());
		if (priority == null) {
			if (logger.isDebugEnabled()) {
				logger.debug("Failed to get alert priority by name :" + message.getPriority());
			}
			return false;
		}
		return priority.isNtLog();
	}

	@Override
	public void send(Message metadata) throws NotificationException {
		if (logger.isDebugEnabled()) {
			logger.debug("Received raise alert notification " + metadata.toString());
		}
		AlertPriority priority = alertService.getAlertPriority(metadata.getPriority());
		if (priority.isNtLog()) {
			addRecord(priority.getName(), metadata.getContent());
		}

	}

	@Override
	public String getName() {
		return PERF_NAME;
	}
}
