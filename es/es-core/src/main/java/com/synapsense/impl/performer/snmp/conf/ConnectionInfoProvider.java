package com.synapsense.impl.performer.snmp.conf;

import java.io.File;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Enumeration;

import org.apache.log4j.Logger;

import com.synapsense.impl.performer.snmp.conf.IPAddressSplitter.IPAddress;
import com.synapsense.util.XmlConfigUtils;
import com.synapsense.util.algo.Finder;
import com.synapsense.util.algo.Predicate;

public class ConnectionInfoProvider {
	private final static Logger logger = Logger.getLogger(ConnectionInfoProvider.class);
	private static Performer config = new Performer();
	private static InetAddress localAddr;

	public synchronized static void readConfig() throws SNMPConfigurationException {
		try {
			logger.info("reading the SNMP trap performer configuration");
			final String dataPath = System.getProperty("com.synapsense.filestorage") + File.separator + "snmp";
			XmlConfigUtils configReader = new XmlConfigUtils(dataPath, Performer.class);
			config = configReader.read(dataPath + File.separator + "snmpconf.xml", Performer.class);
			localAddr = determineLocalIP();
		} catch (Exception e) {
			logger.warn("Cannot instantiate configuration reader", e);
			throw new SNMPConfigurationException("Cannot instantiate configuration reader.", e);
		}
	}

	public static InetAddress getLocalAddress() {
		return localAddr;
	}

	/**
	 * if the IP address in config differs from 127.0.0.1, then use it If it is
	 * equal to localhost, try to enumerate all interfaces available and use the
	 * first non-localhost IP.
	 * 
	 */
	private static InetAddress determineLocalIP() {

		InetAddress cur;
		try {
			final InetAddress local = InetAddress.getByName("127.0.0.1");
			cur = InetAddress.getByName(config.getGeneral().getLocalIp());
			if (!cur.equals(local))
				return cur;

			logger.info("The predefined local IP address seems to be localhost (" + cur.toString()
			        + "). Trying to determine it dynamically.");

			Enumeration<NetworkInterface> nis = NetworkInterface.getNetworkInterfaces();
			while (nis.hasMoreElements()) {
				NetworkInterface ni = nis.nextElement();
				Enumeration<InetAddress> adrs = ni.getInetAddresses();
				while (adrs.hasMoreElements()) {
					cur = adrs.nextElement();
					if (cur instanceof Inet4Address && !cur.equals(local)) {
						logger.warn("Dynamically assigned " + cur.toString() + " to the local address");
						return cur;
					}
				}
			}
		} catch (UnknownHostException e) {
			throw new IllegalStateException("Cannot determine local IP", e);
		} catch (SocketException e) {
			throw new IllegalStateException("Cannot determine local IP", e);
		}
		return cur; // still localhost here
	}

	public static class ConnectionInfo {
		private final InetAddress remAddr;
		private final int remPort;
		private final String readCommunity;
		private final String writeCommunity;
		private final InetAddress locAddr;
		private final int locPort;

		public ConnectionInfo(InetAddress locAddr, int locPort, InetAddress remAddr, int remPort, String readCommunity,
		        String writeCommunity) throws SNMPConfigurationException {
			this.locAddr = locAddr;
			this.locPort = locPort;
			this.remAddr = remAddr;
			this.remPort = remPort;
			this.readCommunity = readCommunity;
			this.writeCommunity = writeCommunity;
		}

		public InetAddress getRemAddr() {
			return remAddr;
		}

		public int getRemPort() {
			return remPort;
		}

		public String getReadCommunity() {
			return readCommunity;
		}

		public String getWriteCommunity() {
			return writeCommunity;
		}

		public InetAddress getLocAddr() {
			return locAddr;
		}

		public int getLocPort() {
			return locPort;
		}
	}

	public synchronized static ConnectionInfo getConnectionInfo(String destination, String type)
	        throws SNMPConfigurationException {
		final IPAddress dest = IPAddressSplitter.getAddress(destination);
		String readComm=null;
		String writeComm=null;
		if (type.equals("TRAP")) {
			final Trap manager = new Finder<Trap>(config.getDestinations().getTrap())
			        .find(new Predicate<Trap>() {
				        @Override
				        public boolean evaluate(Trap value) {
					        return value.getRemoteIp().equals(dest.getFirst().getHostAddress())
					                && (value.getRemotePort() == dest.getSecond());
				        }
			        });

			if (manager == null) {
				readComm = config.getDestinations().getDefaultReadCommunity();
				writeComm = config.getDestinations().getDefaultWriteCommunity();
			} else {
				readComm = manager.getReadCommunity();
				writeComm = manager.getWriteCommunity();
			}
		}
		if (type.equals("AV")) {
			final Avdevice device = new Finder<Avdevice>(config.getDestinations().getAvdevice()).find(new Predicate<Avdevice>() {
				@Override
				public boolean evaluate(Avdevice value) {
					return value.getRemoteIp().equals(dest.getFirst().getHostAddress())
					        && (value.getRemotePort() == dest.getSecond());
				}
			});

			if (device == null) {
				readComm = config.getDestinations().getDefaultReadCommunity();
				writeComm = config.getDestinations().getDefaultWriteCommunity();
			} else {
				readComm = device.getReadCommunity();
				writeComm = device.getWriteCommunity();
			}
		}
		return new ConnectionInfo(localAddr, 0, dest.getFirst(), dest.getSecond(), readComm, writeComm);
	}
}
