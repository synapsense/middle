package com.synapsense.impl.alerting;

import com.synapsense.dto.MessageType;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.dto.Types;
import com.synapsense.impl.alerting.notification.CommonMessageTemplate;
import com.synapsense.impl.alerting.notification.MessageTemplate;
import com.synapsense.service.Environment;
import com.synapsense.util.LocalizationUtils;

public final class AlertConstants {

	public final static String AUTO_ACKNOWLEDGED_BY_SYSTEM_COMMENT = "Auto-acknowledged by system";

	public static final String SYSTEM_ACTIVITY_MESSAGE = "system operation";

	public static final String SYSTEM_ACTIVITY_IDENTITY = "System";

	public static final class TYPE {
		public static final String UNKNOWN_ALERT_TYPE_NAME = "UNKNOWN";
		public static final String ALERT_TYPE_NAME = Types.ALERT.toString();
		public static final String ALERT_DESCRIPTION_TYPE_NAME = "ALERT_TYPE";
		public static final String ALERT_PRIORITY_TYPE_NAME = "ALERT_PRIORITY";
		public static final String ALERT_PRIORITY_TIER_TYPE_NAME = "ALERT_PRIORITY_TIER";
		public static final String ALERT_HISTORY_TYPE_NAME = "ALERT_HISTORY";
		public static final String ALERT_DESTINATION_TYPE_NAME = "ALERT_DESTINATION";

		private TYPE() {
		}
	}

	public static final class MESSAGE {
		public static final String DEFAULT_TEMPLATE_BODY = "$message$";
		public static final MessageTemplate ACKNOWLEDGE_MESSAGE_TEMPLATE = new CommonMessageTemplate(
		        LocalizationUtils.getLocalizedString("email_message_alert_acknowledged_header", "$ALERT_NAME$",
		                "$MSG_TIER$"), LocalizationUtils.getLocalizedString("email_message_alert_acknowledged_body",
		                "$ALERT_NAME$", "$name$"));
		public static final MessageTemplate RESOLVE_MESSAGE_TEMPLATE = new CommonMessageTemplate(
		        LocalizationUtils.getLocalizedString("email_message_alert_resolved_header", "$ALERT_NAME$",
		                "$MSG_TIER$"), LocalizationUtils.getLocalizedString("email_message_alert_resolved_body",
		                "$ALERT_NAME$", "$name$"));

		/**
		 * Default template for alert notifications. Header is empty , body
		 * contains only $message$ macro which is expanding in origin alert
		 * message.
		 * 
		 */
		public static final MessageTemplate DEFAULT_MESSAGE_TEMPLATE = new CommonMessageTemplate("",
		        DEFAULT_TEMPLATE_BODY);

		/**
		 * Console system alert template
		 * 
		 */
		public static final com.synapsense.dto.MessageTemplate CONSOLE_SYSTEM_ALERT_TEMPLATE = new com.synapsense.dto.MessageTemplate() {

			@Override
			public TO<?> getRef() {
				return TOFactory.EMPTY_TO;
			}

			@Override
			public MessageType getMessageType() {
				return MessageType.CONSOLE;
			}

			@Override
			public String getHeader(Environment env) {
				return "";
			}

			@Override
			public String getBody(Environment env) {
				return "$message$";
			}
		};

		private MESSAGE() {
		}
	}

	public static final class NOTIFICATIONS {
		public static final int IMAGE_SERVER_CONNECTION_TIMEOUT = 1000;
		public static final int IMAGE_SERVER_READ_TIMEOUT = 5000;
	}

	private AlertConstants() {
	}

}
