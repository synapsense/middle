package com.synapsense.impl.alerting;

import javax.ejb.EJB;
import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;

import org.apache.log4j.Logger;

import com.synapsense.dal.objectcache.CacheEnvironmentDispatcher;
import com.synapsense.dal.objectcache.CacheSvc;
import com.synapsense.dto.Alert;
import com.synapsense.impl.alerting.macros.DimensionResolveMacro;
import com.synapsense.impl.alerting.macros.MacroContext;
import com.synapsense.service.Environment;
import com.synapsense.service.UnitSystemsService;
import com.synapsense.util.ConvertingEnvironmentProxy;

public class AlertContentIncomingInterceptor {

	// [BC 07/2015] Keep track of the number of bean setters so we can invoke postConstruct() at the end
	private int injectedBeansCount;
	private static final int INJECTED_BEANS_TOTAL = 3;

	private final static Logger logger = Logger.getLogger(AlertContentIncomingInterceptor.class);

	// intercepting public void raiseAlert(final Alert alert)
	@AroundInvoke
	public Object intercept(final InvocationContext icx) throws Exception {
		if (logger.isDebugEnabled()) {
			logger.debug("Entering interceptor : " + AlertContentIncomingInterceptor.class.getName());// +
																									  // " on method : "
																									  // +
																									  // icx.getMethod().getName());
		}
		Object rawObject = icx.getParameters()[0];

		if (rawObject == null || !Alert.class.isAssignableFrom(rawObject.getClass())) {
			return icx.proceed();// let the main logic deal with that
		}

		Alert alert = (Alert) rawObject;
		// scan through alert message and add dimension to data macro where
		// available
		MacroContext ctx = new MacroContext();
		ctx.convEnv = convertingEnv;
		DimensionResolveMacro macro = new DimensionResolveMacro();
		alert.setMessage(macro.expandIn(alert.getMessage(), ctx));
		icx.setParameters(new Object[] { alert });

		if (logger.isDebugEnabled()) {
			logger.debug("Leaving interceptor : " + AlertContentInterceptor.class.getName()); // +
																							  // " on method : "
																							  // +
																							  // icx.getMethod().getName());
		}

		return icx.proceed();
	}

	void postConstruct() {
		logger.info("interceptor init ... ");
		final Environment cachedEnv = CacheEnvironmentDispatcher.getEnvironment(cache, genericEnv);

		convertingEnv = new ConvertingEnvironmentProxy();

		convertingEnv.setUnitSystems(unitSystemsService.getUnitSystems());
		convertingEnv.setUnitResolvers(unitSystemsService.getUnitReslovers());
		convertingEnv.setProxiedEnvironment(cachedEnv);

		logger.info("interceptor init is done");
	}

	@EJB
	void setCache(final CacheSvc cache) {
		this.cache = cache;
		if (++injectedBeansCount == INJECTED_BEANS_TOTAL) postConstruct();
	}

	@EJB(beanName = "GenericEnvironment")
	void setGenericEnv(final Environment genericEnv) {
		this.genericEnv = genericEnv;
		if (++injectedBeansCount == INJECTED_BEANS_TOTAL) postConstruct();
	}

	@EJB
	void setUnitSystemService(final UnitSystemsService unitSystemService) {
		this.unitSystemsService = unitSystemService;
		if (++injectedBeansCount == INJECTED_BEANS_TOTAL) postConstruct();
	}

	private UnitSystemsService unitSystemsService;
	private CacheSvc cache;
	private Environment genericEnv;
	private ConvertingEnvironmentProxy convertingEnv;
}
