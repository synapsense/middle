package com.synapsense.impl.alerting.macros;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AlertNameMacro extends AbstractPatternMacro {

	private static final Pattern pattern = Pattern.compile("\\$(?i)alert_name\\$");

	@Override
	public String expandIn(String input, MacroContext ctx) {
		if (input == null)
			return null;
		if (ctx.alert == null)
			throw new IllegalArgumentException("MacroContext.alert must be not null");
		return super.expandIn(input, ctx);
	}

	@Override
	protected String expand(Matcher m, MacroContext ctx) {
		// user-defined alerts have "ConsoleAlert_" prefix, let's cut it
		String alertName = ctx.alert.getName().replace("ConsoleAlert_", "");
		// try to translate into user language
		alertName = (alertName.isEmpty()) ? alertName : ctx.localizationService.getPartialTranslation(alertName,
		        ctx.usrCtx.getLocale());
		return Matcher.quoteReplacement(alertName);
	}

	@Override
	protected Pattern getPattern() {
		return pattern;
	}

}
