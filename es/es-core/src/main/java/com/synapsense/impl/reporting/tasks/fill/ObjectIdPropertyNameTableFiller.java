package com.synapsense.impl.reporting.tasks.fill;

import com.google.common.collect.ListMultimap;
import com.synapsense.impl.reporting.tasks.EnvValuesTaskData;
import com.synapsense.service.reporting.Cell;
import com.synapsense.service.reporting.olap.Member;
import com.synapsense.service.reporting.olap.custom.Cube;
import com.synapsense.util.CollectionUtils;
import com.synapsense.util.unitconverter.UnitConverter;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;

import static com.synapsense.impl.reporting.tasks.fill.ReportTableFiller.*;

public class ObjectIdPropertyNameTableFiller implements TableFiller {



    @Override
    public void fill(Object[] row, Calendar calendar,
                     EnvValuesTaskData envValuesTaskData) {

        int valueIdColumn = 2;
        int rowValueId = (int) row[valueIdColumn];
        int timestampColumn = 3;
        Timestamp timestamp = (Timestamp) row[timestampColumn];
        List<UnitConverter> unitConverters = envValuesTaskData.getUnitConverters();
        List<String> aggregationTypes = CollectionUtils.newList(envValuesTaskData.getFormulaCellsMap().keySet());

        //Get crossjoin of needed cells
        List<Integer> objectCellNumbers = getObjectCells(row, envValuesTaskData);
        List<Integer> propertyCellNumbers = getPropertyCells(row, envValuesTaskData);
        List<Integer> valueIdCellNumbers = CollectionUtils.newList();
        List<Cell> resultRowCells = CollectionUtils.newList();

        List<Cell> cells = envValuesTaskData.getDataSet();
        for (Integer objectCellNumber : objectCellNumbers) {
            if (propertyCellNumbers.contains(objectCellNumber)) {
                valueIdCellNumbers.add(objectCellNumber);
            }
        }

        for (Integer valueIdCellNumber : valueIdCellNumbers) {
            Cell currentCell = cells.get(valueIdCellNumber);
            Member timeMember = currentCell.getMember(Cube.DIM_TIME);
            calendar.setTime(timestamp);
            if (timeMember == null
                    || containsTime(timeMember, calendar)) {
                resultRowCells.add(currentCell);
            }
        }

        for (Cell cell : resultRowCells) {
            //This is for the case if TimeHierarchy is only in slicer
            int aggTypeIndex = 4;
            addValue(cell, row, aggregationTypes, aggTypeIndex, timestamp, unitConverters);
        }
    }


	private List<Integer> getObjectCells(Object[] row,
			EnvValuesTaskData envValuesTaskData) {
		
		int objectIdColumn = 0;
		int objectId = Integer.valueOf((String) row[objectIdColumn]);

        ListMultimap<Integer, Integer> cellObjectMap = envValuesTaskData.getCellObjectMap();
		List<Integer> objectCellNumbers = cellObjectMap.get(objectId);
        return objectCellNumbers;

	}

    private List<Integer> getPropertyCells(Object[] row,
                     EnvValuesTaskData envValuesTaskData) {

        int propertyNameColumn = 1;
        String propertyName = (String) row[propertyNameColumn];

        return envValuesTaskData.getCellPropertyMap().get(propertyName);

    }


}
