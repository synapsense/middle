package com.synapsense.impl.devmanager.dissectors;

import java.sql.Timestamp;

import org.apache.log4j.Logger;

import com.synapsense.dto.TO;
import com.synapsense.plugin.messages.GenericMessage;
import com.synapsense.plugin.messages.GenericMessage.GenericMessageTypes;
import com.synapsense.plugin.messages.WSNStatisticsBuilder.FieldConstants;
import com.synapsense.service.impl.messages.base.StatisticData;
import com.synapsense.util.algo.Pair;
import com.synapsense.util.algo.UnaryFunctor;

public class WSNStatisticsDissector implements MessageDissector {
	private final static Logger logger = Logger.getLogger(WSNStatisticsDissector.class);
	private final UnaryFunctor<Pair<TO<?>, StatisticData>> functor;

	public WSNStatisticsDissector(UnaryFunctor<Pair<TO<?>, StatisticData>> functor) {
		this.functor = functor;
	}

	@Override
	public boolean tasteMessage(GenericMessage msg) {
		return msg.getType() == GenericMessageTypes.WSN_STATISTICS;
	}

	@Override
	public void process(TO<?> objID, GenericMessage msg) {
		if (msg == null)
			throw new IllegalArgumentException("msg cannot be null");
		if (!tasteMessage(msg))
			throw new IllegalArgumentException("Incompatible message type");
		try {
			StatisticData statData = new StatisticData();
			for (String field : msg.keySet()) {
				switch (FieldConstants.valueOf(field)) {
				case BROADCAST_RETRANSMISSION:
					statData.setBroadcastRetransmission((Integer) msg.get(field));
					break;
				case CRC_FAIL_PACKET_COUNT:
					statData.setCrcFailPacketCount((Integer) msg.get(field));
					break;
				case FORWARDING_PACKETS_DROPPED:
					statData.setForwardingPacketsDropped((Integer) msg.get(field));
					break;
				case FREE_SLOT_COUNT:
					statData.setFreeSlotCount((Integer) msg.get(field));
					break;
				case LOGICAL_ID:
					statData.setLogicalId((Integer) msg.get(field));
					break;
				case NETWORK_ID:
					statData.setNetworkId((Integer) msg.get(field));
					break;
				case NODE_ID:
					break; // use it further
				case NODE_LATENCY:
					statData.setNodeLatency((Integer) msg.get(field));
					break;
				case NUMBER_ONE_HOP_RETRANSMISSION:
					statData.setNumberOneHopRetransmission((Integer) msg.get(field));
					break;
				case NWK_TIMESTAMP:
					statData.setNwkTimestamp(new Timestamp((Long) msg.get(field)));
					break;
				case PACKETS_FORWARDED:
					statData.setPacketsForwarded((Integer) msg.get(field));
					break;
				case PACKETS_FROM_BASE_STATION:
					statData.setPacketsFromBaseStation((Integer) msg.get(field));
					break;
				case PACKETS_RCVD_COUNT:
					statData.setPacketsRcvdCount((Integer) msg.get(field));
					break;
				case PACKETS_SENT:
					statData.setPacketsSent((Integer) msg.get(field));
					break;
				case PACKETS_TO_BASE_STATION:
					statData.setPacketsToBaseStation((Integer) msg.get(field));
					break;
				case PARENT_FAIL_COUNT:
					statData.setParentFailCount((Integer) msg.get(field));
					break;
				case RADIO_ONTIME:
					statData.setRadioOnTime((Integer) msg.get(field));
					break;
				case SEND_FAILS_FULL_QUEUE:
					statData.setSendFailsFullQueue((Integer) msg.get(field));
					break;
				case SEND_FAILS_NO_PATH:
					statData.setSendFailsNoPath((Integer) msg.get(field));
					break;
				case TIME_SYNCS_LOST_DRIFT_DETECTION:
					statData.setTimeSyncsLostDriftDetection((Integer) msg.get(field));
					break;
				case TIME_SYNCS_LOST_TIMEOUT:
					statData.setTimeSyncsLostTimeout((Integer) msg.get(field));
					break;
				case TOTAL_SLOT_COUNT:
					statData.setTotalSlotCount((Integer) msg.get(field));
					break;
				case WRONG_SLOT_COUNT:
					statData.setWrongSlotCount((Integer) msg.get(field));
					break;
				case OUT_OF_ORDER_PACKETS:
					statData.setOutOfOrderPackets((Integer) msg.get(field));
					break;
				case DUPLICATE_PACKETS:
					statData.setDuplicatePackets((Integer) msg.get(field));
					break;
				case PACKETS_FROM_NODE:
					statData.setPacketsFromNodes((Integer) msg.get(field));
					break;
				case CHANNEL_STATUS:
					statData.setChStatus((int[]) msg.get(field));
					break;
				case HOP_COUNT:
					statData.setHopCount((Integer) msg.get(field));
					break;
				case NOISE_THRESH:
					statData.setNoiseThresh((Integer) msg.get(field));
					break;
				case PARENT_STATE:
					statData.setParentState((Integer) msg.get(field));
					break;
				case ROUTER_STATE:
					statData.setRouterState((Integer) msg.get(field));
					break;
				default:
					logger.warn("Field " + field + " is unexpected in statistics message.");
				}
			}
			functor.process(new Pair<TO<?>, StatisticData>(objID, statData));
		} catch (Exception e) {
			throw new IllegalArgumentException("Wrong data is stored in message", e);
		}
	}
}
