package com.synapsense.impl.alerting.command;

import com.synapsense.commandprocessor.Commands;
import com.synapsense.commandprocessor.api.Command;
import com.synapsense.dto.Alert;
import com.synapsense.dto.TO;
import com.synapsense.impl.alerting.Cache;
import com.synapsense.service.Environment;

import java.util.Collection;

public class RecordAndDeleteAlert implements Command {
	private Collection<TO<?>> alerts;
	private Environment env;
	private Cache<Alert, TO<?>> alertStore;
	private TO<?> alertHistoryHolder;

	public RecordAndDeleteAlert(Environment env, Cache<Alert, TO<?>> alertStore, Collection<TO<?>> alerts,
	        TO<?> alertHistoryHolder) {
		this.alerts = alerts;
		this.env = env;
		this.alertStore = alertStore;
		this.alertHistoryHolder = alertHistoryHolder;
	}

	@Override
	public void execute() {
		RecordAlertHistory save = new RecordAlertHistory(env, alerts, alertHistoryHolder);
		DeleteAlert delete = new DeleteAlert(env, alertStore, alerts);

		Commands.chaining("logThenDeleteAlertChain", save, delete).execute();
	}

}
