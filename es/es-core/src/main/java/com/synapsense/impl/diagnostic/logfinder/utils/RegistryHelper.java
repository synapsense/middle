package com.synapsense.impl.diagnostic.logfinder.utils;

import java.io.ByteArrayOutputStream;

import com.synapsense.impl.diagnostic.StreamGobbler;

public class RegistryHelper {
	private static final String REGQUERY_UTIL = "reg query ";

	public static final String SERVICE_KEY = "HKLM\\SYSTEM\\CurrentControlSet\\Services\\";
	public static final String REGEXPANDSZ_TOKEN = "REG_EXPAND_SZ";
	public static final String REGDWORD_TOKEN = "REG_DWORD";
	public static final String REGSZ_TOKEN = "REG_SZ";
	public static final String REGMULTISZ_TOKEN = "REG_MULTI_SZ";

	public static String readValue(String key, String value, String token) {
		String result = null;
		try {
			StringBuilder command = new StringBuilder();
			command.append(REGQUERY_UTIL);
			command.append(key);
			command.append(" /v ");
			command.append(value);

			Process process = Runtime.getRuntime().exec(command.toString());
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			StreamGobbler errorGobbler = new StreamGobbler(process.getErrorStream(), "ERROR");
			StreamGobbler outputGobbler = new StreamGobbler(process.getInputStream(), "OUTPUT", out);

			errorGobbler.start();
			outputGobbler.start();

			process.waitFor();
			
			errorGobbler.join(10000); // join after main process was terminated (just in case allow only 10 seconds for waiting)
			outputGobbler.join(10000); // join after main process was terminated (just in case allow only 10 seconds for waiting)

			String temp = out.toString();
			int pos = temp.indexOf(token);
			if (pos != -1) {
				result = temp.substring(pos + token.length()).trim();
			}

			/*
			 * InputStreamReader reader = new
			 * InputStreamReader(process.getInputStream());
			 * 
			 * char[] buffer = new char[1000]; int len = reader.read(buffer);
			 * 
			 * if(len != -1) { StringBuffer strbuf = new StringBuffer();
			 * strbuf.append(buffer, 0, len);
			 * 
			 * String temp = strbuf.toString(); int pos = temp.indexOf(token);
			 * if(pos != -1) { result = temp.substring(pos +
			 * token.length()).trim(); } }
			 */
		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}
}
