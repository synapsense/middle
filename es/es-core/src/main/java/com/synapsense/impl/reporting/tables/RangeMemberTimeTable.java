package com.synapsense.impl.reporting.tables;

import java.util.Map;
import java.util.Map.Entry;

import com.synapsense.impl.reporting.utils.ReportingConstants.QUERY.SYMBOL;
import com.synapsense.impl.reporting.utils.ReportingConstants.QUERY.WORD;
import com.synapsense.service.reporting.olap.Member;
import com.synapsense.service.reporting.olap.RangeMember;
import com.synapsense.service.reporting.olap.custom.BaseLevel;
import com.synapsense.service.reporting.olap.custom.Cube;
import com.synapsense.service.reporting.olap.custom.Level;
import com.synapsense.service.reporting.olap.custom.TimeHierarchy;
import com.synapsense.util.CollectionUtils;

public class RangeMemberTimeTable implements ReportingTimeTable {

	private static final long serialVersionUID = 5674495780815122409L;

	Map<Level, String> rangeMemberTimeTable = CollectionUtils.newLinkedMap();

	private String lowestTimelevel = Cube.LEVEL_YEAR;

	@SuppressWarnings("unchecked")
	@Override
	public String get() {
		
		StringBuilder rangeTimeMembersSB = new StringBuilder();
		for (Entry<Level, String> entry : rangeMemberTimeTable.entrySet()) {
			rangeTimeMembersSB.append(entry.getValue())
					.append(SYMBOL.SPACE)
					.append(WORD.AND)
					.append(SYMBOL.SPACE);
		}
		return rangeTimeMembersSB.substring(0, rangeTimeMembersSB.length() - 5);
		
	}

	@Override
	public void add(Member member) {
		
		StringBuilder timeValueSB = new StringBuilder();
		RangeMember rangeMember = (RangeMember) member;
		Level level = rangeMember.getCustomLevel();
		int membersRange = level.getMembersRange();
		String levelName = level.getName();

		int rangeMemberMultiplier = membersRange;
		levelName = Cube.TIME.getChildLevel(levelName);
		while (levelName != null) {
			rangeMemberMultiplier *= TimeHierarchy.getMaximum(levelName);
			levelName = Cube.TIME.getChildLevel(levelName);
		}
		timeValueSB.append("ROUND(UNIX_TIMESTAMP(timestamp)/(60 * ")
				.append(rangeMemberMultiplier)
				.append(SYMBOL.CLOSE_BRACKET)
				.append(SYMBOL.CLOSE_BRACKET);
		put(level, timeValueSB.toString());
		
	}

	@Override
	public Level getLowestLevel() {
		return new BaseLevel(Cube.TIME, lowestTimelevel);
	}

	@Override
	public boolean isEmpty() {
		return rangeMemberTimeTable.isEmpty();
	}

	@Override
	public int size() {
		return rangeMemberTimeTable.size();
	}

	@Override
	public ReportingTimeTable getTable() {
		
		RangeMemberTimeTable copy = new RangeMemberTimeTable();
		for (Entry<Level, String> entry : rangeMemberTimeTable.entrySet()) {
			copy.put(entry.getKey(), entry.getValue());
		}
		copy.setLowestTimeLevel(lowestTimelevel);
		return copy;
		
	}

	protected void setLowestTimeLevel(String level) {
		this.lowestTimelevel = level;
	}

	private void put(Level level, String value) {
		rangeMemberTimeTable.put(level.copy(), value);
	}
	
}
