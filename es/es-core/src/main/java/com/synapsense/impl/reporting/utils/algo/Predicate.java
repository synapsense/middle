package com.synapsense.impl.reporting.utils.algo;

public interface Predicate<T> {
	boolean evaluate(T value);
}
