package com.synapsense.impl.taskscheduler.tasks;

import java.util.Collection;

import org.apache.log4j.Logger;

import com.synapsense.dto.TO;
import com.synapsense.exception.EnvException;
import com.synapsense.impl.utils.CommonUtils;
import com.synapsense.service.Environment;

public class SensorNodeStatusChecker implements StatusChecker {
	private final static Logger logger = Logger.getLogger(SensorNodeStatusChecker.class);

	private static final int ON_ERROR_NODE_SAMPLING_INTERVAL = 300000;

	private Environment env;

	@Override
	public void check(Environment env) {
		this.env = env;
		if (env == null) {
			logger.error("env=null");
			return;
		}
		Collection<TO<?>> nodes = env.getObjectsByType("WSNNODE");
		for (TO<?> node : nodes) {
			try {
				setSensorNodeState(node);
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
	}

	private void setSensorNodeState(TO<?> node) throws EnvException {
		long samInt = getNodeSamplingInterval(node);
		long currentTime = System.currentTimeMillis();
		int nodeStatus = 0;

		Collection<TO<?>> children = env.getChildren(node, "WSNSENSOR");
		for (TO<?> sensor : children) {
			long sdts = env.getPropertyTimestamp(sensor, "lastValue");
			int sensorCurrentStatus = CommonUtils.nullSafeInt(env.getPropertyValue(sensor, "status", Integer.class));
			// check if sensor is disabled
			if (sensorCurrentStatus != 2) {
				int sensorStatus = ((sdts + 2 * samInt) < currentTime) ? 0 : 1;

				if (sensorCurrentStatus != sensorStatus) {
					// don't update status if it changed less than 2x sampling
					// interval time ago to allow data to be collected after
					// node is re-enabled
					if ((env.getPropertyTimestamp(sensor, "status") + 2 * samInt) < currentTime) {
						env.setPropertyValue(sensor, "status", sensorStatus);
					} else {
						sensorStatus = sensorCurrentStatus;
					}
				}

				if (sensorStatus == 1)
					nodeStatus = 1; // if there is at least one live sensor, the
									// whole node is alive;
			}
		}

		int nodeCurrentStatus = CommonUtils.nullSafeInt(env.getPropertyValue(node, "status", Integer.class));

		// check if node is not disabled and its status changed
		if ((nodeCurrentStatus != 2) && (nodeCurrentStatus != nodeStatus)) {
			env.setPropertyValue(node, "status", nodeStatus);
			logger.info("Node (" + Long.toHexString(env.getPropertyValue(node, "mac", Long.class))
			        + ") status changed from " + nodeCurrentStatus + " to " + nodeStatus);
		}

	}

	private long getNodeSamplingInterval(TO<?> node) throws EnvException {
		String sampleIntervalAsString = env.getPropertyValue(node, "period", String.class);
		long sampleInterval = parseTimeStamp(sampleIntervalAsString);

		return sampleInterval <= 0 ? ON_ERROR_NODE_SAMPLING_INTERVAL : sampleInterval;
	}

	private long parseTimeStamp(String data) {
		if (data == null) {
			return 0;
		}

		String[] t = data.split(" ");

		if (t.length != 2) {
			return 0;
		}

		long samInt = Long.valueOf(t[0]);

		if (t[1].equalsIgnoreCase("sec")) {
			samInt *= 1000;
		} else if (t[1].equalsIgnoreCase("min")) {
			samInt *= 1000 * 60;
		} else if (t[1].equalsIgnoreCase("hr")) {
			samInt *= 1000 * 60 * 60;
		}

		return samInt;
	}
}
