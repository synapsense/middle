package com.synapsense.impl.devmanager.dissectors;

import com.synapsense.dto.TO;
import com.synapsense.plugin.messages.GenericMessage;
import com.synapsense.plugin.messages.GenericMessage.GenericMessageTypes;
import com.synapsense.plugin.messages.WSNGWStatusBuilder.FieldConstants;
import com.synapsense.util.algo.Pair;
import com.synapsense.util.algo.UnaryFunctor;

public class WSNGWStatusDissector implements MessageDissector {
	private final UnaryFunctor<Pair<TO<?>, Integer>> functor;

	public WSNGWStatusDissector(UnaryFunctor<Pair<TO<?>, Integer>> functor) {
		this.functor = functor;
	}

	@Override
	public boolean tasteMessage(GenericMessage msg) {
		return msg.getType() == GenericMessageTypes.WSN_GWSTATUS;
	}

	@Override
	public void process(TO<?> objID, GenericMessage msg) {
		try {
			functor.process(new Pair<TO<?>, Integer>(objID, (Integer) msg.get(FieldConstants.STATUS.name())));
		} catch (Exception e) {
			throw new IllegalArgumentException("Wrong data is stored in message", e);
		}

	}
}
