package com.synapsense.impl.reporting.dao;

import java.util.*;

import org.apache.log4j.Logger;

import com.synapsense.dto.CollectionTO;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.EnvException;
import com.synapsense.exception.IllegalInputParameterException;
import com.synapsense.impl.reporting.dao.ResourceDAOReportingException;
import com.synapsense.impl.reporting.utils.ReportingConstants;
import com.synapsense.service.Environment;
import com.synapsense.service.reporting.dto.ReportTaskTitle;
import com.synapsense.service.reporting.exception.EnvDAOReportingException;
import com.synapsense.service.reporting.exception.ObjectNotFoundDAOReportingException;
import com.synapsense.util.CollectionUtils;

public class AbstractReportingDAO {

	private static final Logger log = Logger
			.getLogger(AbstractReportingDAO.class);

	protected Environment environment;

	public AbstractReportingDAO(Environment environment) {
		this.environment = environment;
	}
	
	protected <REQUESTED_TYPE> void setPropertyValue(TO<?> objId, String propName, REQUESTED_TYPE value) {

		try {
			environment.setPropertyValue(objId, propName, value);
		} catch (EnvException e) {
			throw new ResourceDAOReportingException("Unable to add property " + propName
					+ " value " + value.toString() + " to object "
					+ objId.toString());
		}

	}


	protected void removeObject(TO<?> object) throws ObjectNotFoundDAOReportingException {

		try {
			environment.deleteObject(object);
		} catch (EnvException e) {
			log.debug("Failed to delete object " + object.toString(), e);
			throw new ObjectNotFoundDAOReportingException("Failed to delete object "
					+ object.toString(), e);
		}

	}
	
	protected void removeReportParamtersValues(TO<?> reportTask) throws ObjectNotFoundDAOReportingException{
		Collection<TO<?>> parameterValues = this.getObjectsByProperty(
				this.getObjectsByType(ReportingConstants.TYPE.REPORT_PARAMETER_VALUE)
				, ReportingConstants.FIELD.REPORT_PARAMETER_VALUE.REPORT_TASK_ID, reportTask.getID());
		for (TO<?> parameterValue : parameterValues) {
			try {
				removeObject(parameterValue);
			} catch (ObjectNotFoundDAOReportingException e) {
				log.error(e.getLocalizedMessage(), e);
				throw new ResourceDAOReportingException("Object " + parameterValue + " remove execution error",
						e);
			}
		}
	}
	
	protected Collection<TO<?>> getCollectionPropertyValue(TO<?> object,
			String propertyName)
			throws EnvDAOReportingException {

		Collection<TO<?>> values;
		try {
			values = environment.getPropertyValue(object, propertyName,
					Collection.class);
		} catch (EnvException e) {

			if (log.isDebugEnabled()) {
				log.debug("Failed to get property " + propertyName
						+ " for object " + object.toString());
			}
			throw new EnvDAOReportingException("Failed to get property "
					+ propertyName + " for object " + object.toString(), e);
		}
		return values;

	}
	
	protected TO<?> getObjectById(String objectType, int objectId) throws ObjectNotFoundDAOReportingException {

		TO<?> object = TOFactory.INSTANCE.loadTO(objectType, objectId);
		if (object.equals(TOFactory.EMPTY_TO) || !environment.exists(object)) {
			return null;
		}
		return object;

	}
	
	protected Collection<TO<?>> getObjectsByType(String objectType) {

		final Set<TO<?>> allObjects = CollectionUtils.newSet();
		allObjects.addAll(environment.getObjectsByType(objectType));

		return allObjects;

	}

	protected TO<?> getObjectByName(String objectType, String propValue) {

		Collection<TO<?>> allObjects = environment.getObjectsByType(objectType);
		TO<?> object = null;
		// If no object of this type at all in DB
		if (allObjects.isEmpty()) {
			if (log.isDebugEnabled()) {
				log.debug("Failed to get object with type " + objectType
						+ " and name " + propValue);
			}	
			return object;
		}
		Collection<TO<?>> objects = getObjectsByProperty(allObjects, "name",
				propValue);
		if (objects.isEmpty()) {
			return null;
		}
		return objects.iterator().next();

	}

	protected Collection<TO<?>> getObjectsByProperty(
			Collection<TO<?>> allObjects, String propName, Object propValue) {

		Collection<TO<?>> objects = CollectionUtils.newArrayList(0);
		if (allObjects.isEmpty()) {
			return objects;
		}
		Collection<CollectionTO> allProperties = environment.getPropertyValue(
				allObjects, new String[] { propName });
		if (allProperties.isEmpty()) {
			if (log.isDebugEnabled()) {
				log.debug("Failed to get property " + propName + " for objects"
						+ allObjects);
			}
			return Collections.emptyList();
		}

		for (CollectionTO propCollection : allProperties) {
			Object propCollectionValue = propCollection.getSinglePropValue(
					propName).getValue();
			if (propCollectionValue.equals(propValue)) {
				objects.add(propCollection.getObjId());
			}
		}
		return objects;

	}

	protected Map<String, ValueTO> getPropertyValues(TO<?> object,
			String[] propertyNames) throws EnvDAOReportingException {

		Collection<ValueTO> allProperties;
		Map<String, ValueTO> properties = CollectionUtils.newMap();

		Set<String> propNames = CollectionUtils.newSet(propertyNames);
		try {
			allProperties = environment.getAllPropertiesValues(object);
			for (ValueTO valueTO : allProperties) {
				if (propNames.contains(valueTO.getPropertyName())) {
					properties.put(valueTO.getPropertyName(), valueTO);
				}
			}

		} catch (EnvException e) {
			if (log.isDebugEnabled()) {
				log.debug(
						"Failed to get properties for object"
								+ object.toString(), e);
			}
			throw new EnvDAOReportingException(
					"Failed to get properties for object" + object.toString(),
					e);
		}
		return properties;

	}

	protected Object getPropertyValue(TO<?> object, String propertyName, Class<?> clazz)
			throws EnvDAOReportingException {

		try {
			return environment.getPropertyValue(object, propertyName, clazz);
		} catch (EnvException e) {

			if (log.isDebugEnabled()) {
				log.debug("Failed to get property " + propertyName
						+ " for object " + object.toString());
			}
			throw new EnvDAOReportingException("Failed to get property "
					+ propertyName + " for object " + object.toString(), e);
		}

	}

	protected Collection<ReportTaskTitle> getReportTasksByTemplate(String reportTemplateName)
	        throws ObjectNotFoundDAOReportingException {

		if (reportTemplateName == null) {
			throw new IllegalInputParameterException("Supplied 'reportTemplateName' is null");
		}

		if (log.isDebugEnabled()) {
			log.debug("Retrieving all report tasks of report template " + reportTemplateName);
		}

		Collection<ReportTaskTitle> taskTitles = CollectionUtils.newList();

		TO<?> reportTemplate = getObjectByName(ReportingConstants.TYPE.REPORT_TEMPLATE, reportTemplateName);
		if (reportTemplate == null) {
			if (log.isDebugEnabled()) {
				log.debug("Report template " + reportTemplateName + " not found");
			}
			throw new ObjectNotFoundDAOReportingException("Report template " + reportTemplateName + " not found");
		}

		int templateId = (Integer) reportTemplate.getID();
		Collection<TO<?>> reportTaskIds = environment.getObjects(ReportingConstants.TYPE.REPORT_TASK,
		        new ValueTO[] { new ValueTO(ReportingConstants.FIELD.REPORT_TASK.TEMPLATE_ID, templateId) });
		for (TO<?> taskInstance : reportTaskIds) {
			Map<String, ValueTO> reportProperties;
			try {
				reportProperties = getPropertyValues(taskInstance, new String[] {
				        ReportingConstants.FIELD.REPORT_TASK.NAME, ReportingConstants.FIELD.REPORT_TASK.STATUS });
			} catch (EnvDAOReportingException e) {
				throw new ResourceDAOReportingException("Error retrieving report task properties", e);
			}

			int taskId = (Integer) taskInstance.getID();
			ReportTaskTitle taskTitle = new ReportTaskTitle(reportTemplateName, (String) reportProperties.get(
			        ReportingConstants.FIELD.REPORT_TASK.NAME).getValue());
			taskTitle.setTaskId(taskId);

			int status = (Integer) reportProperties.get(ReportingConstants.FIELD.REPORT_TASK.STATUS).getValue();
			taskTitle.setStatus(status);

			taskTitles.add(taskTitle);
		}

        List<ReportTaskTitle> sortedTasks = CollectionUtils.newList(taskTitles);
        Collections.sort(sortedTasks, new ReportTaskComparator());
        return sortedTasks;

	}

    private static class ReportTaskComparator implements Comparator<ReportTaskTitle> {

        @Override
        public int compare(ReportTaskTitle taskTitle1, ReportTaskTitle taskTitle2) {
            return taskTitle1.getName().compareToIgnoreCase(taskTitle2.getName());
        }

    }

    protected TO<?> createObject(String typeName, ValueTO[] propertyValues) {

		try {
			return environment.createObject(typeName, propertyValues);
		} catch (EnvException e) {
			throw new ResourceDAOReportingException("Failed to create object of type "
					+ typeName, e);
		}

	}
	
}
