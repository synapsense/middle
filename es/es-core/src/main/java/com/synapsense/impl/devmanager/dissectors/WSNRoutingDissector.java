package com.synapsense.impl.devmanager.dissectors;

import com.synapsense.dto.TO;
import com.synapsense.plugin.messages.GenericMessage;
import com.synapsense.plugin.messages.GenericMessage.GenericMessageTypes;
import com.synapsense.plugin.messages.WSNRoutingBuilder.FieldConstants;
import com.synapsense.service.impl.messages.base.RoutingData;
import com.synapsense.util.algo.Pair;
import com.synapsense.util.algo.UnaryFunctor;

public class WSNRoutingDissector implements MessageDissector {
	private final UnaryFunctor<Pair<TO<?>, RoutingData>> functor;

	public WSNRoutingDissector(UnaryFunctor<Pair<TO<?>, RoutingData>> functor) {
		this.functor = functor;
	}

	@Override
	public boolean tasteMessage(GenericMessage msg) {
		return msg.getType() == GenericMessageTypes.WSN_ROUTING;
	}

	@Override
	public void process(TO<?> objID, GenericMessage msg) {
		if (msg == null)
			throw new IllegalArgumentException("msg cannot be null");
		if (!tasteMessage(msg))
			throw new IllegalArgumentException("Incompatible message type");
		try {
			RoutingData data = new RoutingData((Long) msg.get(FieldConstants.PARENT_ID.name()),
			        (Integer) msg.get(FieldConstants.NETWORK_ID.name()), (Long) msg.get(FieldConstants.CHILD_ID.name()));
			functor.process(new Pair<TO<?>, RoutingData>(objID, data));
		} catch (Exception e) {
			throw new IllegalArgumentException("Wrong data is stored in message", e);
		}
	}
}
