package com.synapsense.impl.alerting.macros;

import java.util.Collection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.exception.EnvException;
import com.synapsense.service.Environment;

public class RelationMacro extends AbstractPatternMacro {

	private final static Logger log = Logger.getLogger(RelationMacro.class);

	private static final Pattern pattern = Pattern.compile("\\$(parent|child)\\((\\w+)\\)\\.((?:\\w+\\.?){1,})\\$");

	@Override
	public String expandIn(String input, MacroContext ctx) {
		if (input == null)
			return null;
		if (ctx.alert == null)
			throw new IllegalArgumentException("MacroContext.alert must be not null");
		if (ctx.env == null)
			throw new IllegalArgumentException("MacroContext.env must be not null");
		return super.expandIn(input, ctx);
	}

	@Override
	protected String expand(Matcher m, MacroContext ctx) {
		final TO<?> host = ctx.alert.getHostTO();
		if (TOFactory.EMPTY_TO.equals(host)) {
			return Matcher.quoteReplacement(Macro.DEFAULT_EXPANSION);
		}

		final String relationDirection = m.group(1);
		final String typeNameFilter = m.group(2);
		Collection<TO<?>> relatedObjects = null;
		if ("parent".equals(relationDirection)) {
			relatedObjects = getRelated(host, typeNameFilter, true, ctx.env);
		} else {// means 'child'
			relatedObjects = getRelated(host, typeNameFilter, false, ctx.env);
		}

		if (relatedObjects.isEmpty()) {
			log.warn("Macro " + m.group() + ": no related objects of type " + typeNameFilter + " found for object "
			        + host.toString());
			return Matcher.quoteReplacement(Macro.DEFAULT_EXPANSION);
		}

		if (relatedObjects.size() > 1) {
			log.warn("Macro " + m.group() + ": multiple related objects are not supported");
			return Matcher.quoteReplacement(Macro.DEFAULT_EXPANSION);
		}

		TO<?> relatedObject = relatedObjects.iterator().next();
		String result = Macro.DEFAULT_EXPANSION;

		final String propertyToGet = m.group(3);
		Object value;
		try {
			value = ctx.env.getPropertyValue(relatedObject, propertyToGet, Object.class);
			if (value != null)
				result = value.toString();
		} catch (final EnvException e) {
			log.warn("Macro " + m.group() + ": cannot read property \"" + propertyToGet + "\" of object "
			        + relatedObject + " which is " + relationDirection + " of " + host);
		}

		result = (result.isEmpty()) ? result : ctx.localizationService.getPartialTranslation(result,
		        ctx.usrCtx.getLocale());

		return Matcher.quoteReplacement(result);
	}

	private static Collection<TO<?>> getRelated(final TO<?> host, final String typeName, final boolean up,
	        Environment env) {
		final Collection<TO<?>> related = env.getRelatedObjects(host, typeName, !up);
		return related;
	}

	@Override
	protected Pattern getPattern() {
		return pattern;
	}

}
