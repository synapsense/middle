package com.synapsense.impl.reporting.engine;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import com.synapsense.impl.reporting.tasks.*;
import com.synapsense.service.reporting.Formula;
import org.apache.log4j.Logger;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;
import com.synapsense.dal.generic.reporting.ReportingQueryExecutor;
import com.synapsense.dto.PropertyDescr;
import com.synapsense.dto.TO;
import com.synapsense.impl.reporting.queries.TableProcessor;
import com.synapsense.impl.reporting.queries.AxisProcessor.ValueIdAttributesProvider;
import com.synapsense.impl.reporting.queries.types.ReportingQueryType;
import com.synapsense.impl.reporting.tables.ReportTable;
import com.synapsense.impl.reporting.tables.ReportTableImpl;
import com.synapsense.impl.reporting.tables.ReportTableTransformer;
import com.synapsense.impl.reporting.tasks.EnvValuesTaskData.EnvValuesTaskDataBuilder;
import com.synapsense.service.reporting.Cell;
import com.synapsense.service.reporting.DimensionalQuery;
import com.synapsense.service.reporting.dto.TableTO;
import com.synapsense.service.reporting.olap.AxisSet;
import com.synapsense.service.reporting.olap.CrossJoin;
import com.synapsense.service.reporting.olap.Hierarchy;
import com.synapsense.service.reporting.olap.Member;
import com.synapsense.service.reporting.olap.Tuple;
import com.synapsense.service.reporting.olap.custom.Cube;
import com.synapsense.service.reporting.olap.custom.EnvironmentDataSource;
import com.synapsense.util.CollectionUtils;
import com.synapsense.util.Pair;
import com.synapsense.util.unitconverter.ConverterException;
import com.synapsense.util.unitconverter.UnitConverter;
import com.synapsense.util.unitconverter.UnitResolvers;
import com.synapsense.util.unitconverter.UnitSystems;
import com.synapsense.util.unitconverter.UnitResolvers.DimensionRef;
import com.synapsense.util.unitconverter.UnitResolvers.PropertyUnits;

/**
 * The <code>ReportData</code> class contains data related to report.
 * 
 * @author Grigory Ryabov
 */
@TransactionManagement(TransactionManagementType.CONTAINER)
public class ReportData implements ReportQueryInfo, Serializable {

	private static final long serialVersionUID = 6016837851770699481L;

	private static final Logger log = Logger.getLogger(ReportData.class);

    private DimensionalQuery query;

	private TableProcessor tableProcessor;

	private ArrayList<Cell> cells;

	private int userId;
	
	private ValueIdsTaskData valueIdsTaskData;

	private EnvValuesTaskData envValuesTaskData;
	
	private ReportingQueryExecutor queryExecutor;

	private EnvironmentDataSource dataSource;
	
	private ListMultimap<Pair<String, Integer>, Integer> cellMap;
	
	private Map<String, Set<Cell>> formulaCellsMap;

	private Map<Pair<List<TO<?>>, Member>, List<Pair<String, Integer>>> attributesMap;

	private boolean multipleObjectIdCellTable = false;

    private final ReportUserInfo reportUserInfo;

    public ReportData(DimensionalQuery query, EnvironmentDataSource ds, ReportingQueryExecutor reportingQueryExecutor, ReportUserInfo reportUserInfo) {
		this.query = query;
		this.queryExecutor = reportingQueryExecutor;
		this.tableProcessor = new TableProcessor(ds, query, reportUserInfo);
		this.userId = reportUserInfo.getUserId();
		this.dataSource = ds;
        this.reportUserInfo = reportUserInfo;
	}
	
	@Override
	public void buildQuery() {

		//Filling Reporting Helper tables
		List<Cell> valueIdCells = tableProcessor.processSets();
		
		//Create table cells by crossjoining of axises
		makeCrossJoin();
		
		mapCells();

		//Build ValueIdsTaskData
		this.valueIdsTaskData = tableProcessor.buildValueIdQuery();
		if (valueIdsTaskData == null 
				|| valueIdsTaskData.getValueIdsQuery().isEmpty()) {
			if(log.isDebugEnabled()){
				log.debug("Value Ids Query isEmpty");
			}
			return;
		}
		
		valueIdsTaskData.setCellMap(cellMap);
		valueIdsTaskData.setCells(cells);

        //Build EnvValuesTaskData
        fillFormulaCellsMap();

        boolean differentMultiplePropertyCellsTable = checkIfTableHasDifferentMultiplePropertyCells();

		ReportingQueryType queryType = null;
        ListMultimap<String, Integer> cellPropertyMap = ArrayListMultimap.create();
        ListMultimap<Integer, Integer> cellObjectMap = createCellObjectMap();
		boolean singleValueIdPerCellTable = checkSingleValueIdPerCellTable(valueIdCells);
		if (singleValueIdPerCellTable) {
			queryType = ReportingQueryType.SINGLE_PROPERTY_CELL_TABLE;
		} else if (multipleObjectIdCellTable) {
			queryType = ReportingQueryType.MULTIPLE_OBJECT_CELL_TABLE;
		} else if (differentMultiplePropertyCellsTable) {
			cellPropertyMap = createCellPropertyMap();
			queryType = ReportingQueryType.DIFFERENT_MULTIPLE_PROPERTY_CELL_TABLE;
		} else {
			queryType = ReportingQueryType.SAME_MULTIPLE_PROPERTY_CELL_TABLE;
		}
		if (log.isInfoEnabled()) {
			log.info("Reporting Query Type is " + queryType);
		}
		EnvValuesTaskData localEnvValuesTaskData = new EnvValuesTaskDataBuilder()
			.formulaCellsMap(formulaCellsMap)
			.cells(cells)
			.cellObjectMap(cellObjectMap)
			.cellPropertyMap(cellPropertyMap)
			.queryType(queryType)
			.build();
		
		this.envValuesTaskData = tableProcessor.buildEnvDataQuery(localEnvValuesTaskData);
		
	}
	
	private boolean checkSingleValueIdPerCellTable(List<Cell> valueIdCells) {
		boolean singleValueIdPerCellTable = true;
		singleValueIdPerCellTable = isSameLevel(valueIdCells.iterator().next())
				&& !isMultipleMemberSlicer(query.getSlicer(), Cube.OBJECT);
		if (singleValueIdPerCellTable) {
			for (Cell cell : valueIdCells ) {
				Member propertyMember = cell.getMember(Cube.PROPERTY);
				if (propertyMember == null) {
					List<Tuple> objectTuples = query.getSlicer().getTuples();
					propertyMember = objectTuples.iterator().next().itemByHierarchy(Cube.PROPERTY);
				}
				if (propertyMember.getSimpleMembers().size() > 1) {
					singleValueIdPerCellTable = false;
					break;
				}
			} 
		}
		return singleValueIdPerCellTable;
	}
	
	private boolean isMultipleMemberSlicer(AxisSet slicer, Hierarchy hierarchy) {
		if (slicer == null) {
			return false;
		}
		return CrossJoin.getTuplesByHierarchy(new CrossJoin(slicer), hierarchy).size() > 1;
	}
	
	private boolean isSameLevel(Cell cell) {
		String [] propertyArray = ((String) cell.getMember(Cube.PROPERTY).getKey()).split("\\.");
		return cell.getMember(Cube.OBJECT).getLevel().equals(propertyArray[0]);
	}
	
	private boolean checkIfTableHasDifferentMultiplePropertyCells() {
		
		boolean differentMultiplePropertyCellsTable = false;
		List<String> cellProperties = CollectionUtils.newList();

		boolean isMultiplePropertyCellsTable = false;
		
		for (Cell cell : cells) {
			Member propertyMember = cell.getMember(Cube.PROPERTY);

			if (propertyMember == null) {
				List<Tuple> objectTuples = query.getSlicer().getTuples();
				propertyMember = objectTuples.iterator().next().itemByHierarchy(Cube.PROPERTY);
			} 
			List<Member> simplePropertyMembers = propertyMember.getSimpleMembers();
			if (simplePropertyMembers.size() > 1) {
				isMultiplePropertyCellsTable = true;
				break;
			}
		}
		
		
		if (!isMultiplePropertyCellsTable) {
			return differentMultiplePropertyCellsTable;
		}
		
		for (Cell cell : cells) {
			Member propertyMember = cell.getMember(Cube.PROPERTY);

			if (propertyMember == null) {
				List<Tuple> objectTuples = query.getSlicer().getTuples();
				propertyMember = objectTuples.iterator().next().itemByHierarchy(Cube.PROPERTY);
			} 
			List<Member> simplePropertyMembers = propertyMember.getSimpleMembers();
			
			List<String> currentCellProperties = CollectionUtils.newList();

			if (cellProperties.isEmpty()) {
				for (Member simplePropertyMember : simplePropertyMembers) {
					String[] propertyArray = ((String) simplePropertyMember
							.getKey()).split("\\.");
					cellProperties.add(propertyArray[0] + "."
							+ propertyArray[1]);
				}
			}

			for (Member simplePropertyMember : simplePropertyMembers) {
				String[] propertyArray = ((String) simplePropertyMember
						.getKey()).split("\\.");
				currentCellProperties.add(propertyArray[0] + "."
						+ propertyArray[1]);
			}

			if (currentCellProperties.equals(cellProperties)) {
				differentMultiplePropertyCellsTable = false;
			} else {
				differentMultiplePropertyCellsTable = true;
				return differentMultiplePropertyCellsTable;
			}
		}
		return differentMultiplePropertyCellsTable;
	}
	
	private ListMultimap<Integer, Integer> createCellObjectMap() {
		ListMultimap<Integer, Integer> cellObjectMap = ArrayListMultimap.create();		
		List<Member> objectMembers = CollectionUtils.newList();
		Cell firstCell = cells.iterator().next();
		Member firstObjectMember = firstCell.getMember(Cube.OBJECT);
		boolean slicerObjectDimensionTable = false;
		if (firstObjectMember == null) {
			slicerObjectDimensionTable = true;
			if (query.getSlicer() == null) {
				if (log.isDebugEnabled()) {
					log.debug("Object dimension has no specification for cell: "
							+ firstCell.toString());
				}
				throw new QueryDataReportingException("Object dimension has no specification for cell: "
						+ firstCell.toString());
			}	
			List<Tuple> objectTuples = query.getSlicer().getTuples();
			firstObjectMember = objectTuples.iterator().next().itemByHierarchy(Cube.OBJECT);
			for (Tuple objectTuple : objectTuples) {
				objectMembers.add(objectTuple.itemByHierarchy(Cube.OBJECT));
			}
		}
		
		int index = 0;
		for (Cell cell : cells) {
			if (!slicerObjectDimensionTable) {
				objectMembers = CollectionUtils.newList();
				Member objectMember = cell.getMember(Cube.OBJECT);
				objectMembers.add(objectMember);
			}
			if (objectMembers.size() > 1) {
				if (!multipleObjectIdCellTable) {
					multipleObjectIdCellTable = true;
				}			
			}			
			for (Member member : objectMembers) {
					int key = (Integer) ((TO<?>)member.getKey()).getID();
					int value = index;
					cellObjectMap.put(key, value);
			}
			index++;
		}
		return cellObjectMap;
	}
	
    private ListMultimap<String, Integer> createCellPropertyMap() {
        ListMultimap<String, Integer> cellPropertyMap = ArrayListMultimap.create();
        int index = 0;
        for (Cell cell : cells) {

            Member propertyMember = cell.getMember(Cube.PROPERTY);

            if (propertyMember == null) {
            	if (query.getSlicer() == null) {
    				if (log.isDebugEnabled()) {
    					log.debug("Property dimension has no specification for cell: "
    							+ cell.toString());
    				}
    				throw new QueryDataReportingException("Property dimension has no specification for cell: "
    						+ cell.toString());
    			}
                List<Tuple> propertyTuples = query.getSlicer().getTuples();
                propertyMember = propertyTuples.iterator().next().itemByHierarchy(Cube.PROPERTY);

            }

            for (Member member : propertyMember.getSimpleMembers()) {

                String [] propertyArray = ((String)member.getKey()).split("\\.");

                int value = index;
                cellPropertyMap.put(propertyArray[0] + "." + propertyArray[1], value);
            }

            index++;
        }
        return cellPropertyMap;
    }
	
	@Override
	public TableTO executeQuery() {

        CompositeReportingTask compositeReportingTask = new CompositeReportingTask();
        compositeReportingTask.addTask(new GetValueIdsTask(valueIdsTaskData, queryExecutor));
        compositeReportingTask.addTask(new GetEnvDataTask(envValuesTaskData, queryExecutor));
		compositeReportingTask.execute();
		ReportTable reportTable = new ReportTableImpl(cells, query, userId);
		ReportTableTransformer reportTableTransformer = new ReportTableTransformer(reportTable.getTable());
		return reportTableTransformer.transform(query.getFormula(), dataSource, this.reportUserInfo);
		
	}

//    @Override
	public void makeCrossJoin(){

        cells = query.getCells();

	}
	
	private void mapCells() {
		cellMap = ArrayListMultimap.create();
		this.attributesMap = CollectionUtils.newMap();
		int index = 0;
		for (Cell cell : cells) {
			List<Pair<String, Integer>> attributes = getAttributes(cell);
			
			if (!attributes.isEmpty()) {
				
				for (Pair<String, Integer> attributesPair : attributes) {
					cellMap.put(attributesPair, index);
				}
				
			}
			index++;
		}
	}
	
	private void fillFormulaCellsMap() {
		
		String aggregationType = query.getFormula().getAggregationType();
		formulaCellsMap = CollectionUtils.newLinkedMap();
		//Aggregation type is common for all cells
		if (aggregationType != null) {
			Set<Cell> formulaCells = CollectionUtils.newSet();
			formulaCells.addAll(cells);
			formulaCellsMap.put(aggregationType, formulaCells);
			return;
		}
		
		List<Tuple> properties = CrossJoin.getTuplesByHierarchy(new CrossJoin(query.getAxisSet(0), query.getAxisSet(1)), Cube.PROPERTY);
		
		Map<Member, String> propertyToAggregationTypeMap = CollectionUtils.newMap();
		for (Tuple propertyTuple : properties) {
			Member propertyMember = propertyTuple.itemByHierarchy(Cube.PROPERTY);
			String[] keys = ((String) propertyMember.getKey()).split("\\.");
			String propertyAggregationType = keys[2];
			propertyToAggregationTypeMap.put(propertyMember, propertyAggregationType);
		}
		for (Cell cell : cells) {
			String formula = propertyToAggregationTypeMap.get(cell.getMember(Cube.PROPERTY));
			if (formulaCellsMap.containsKey(formula)){
				formulaCellsMap.get(formula).add(cell);
			} else {
				Set<Cell> formulaCells = CollectionUtils.newSet();
				formulaCells.add(cell);
				formulaCellsMap.put(formula, formulaCells);
			}
		}
		
	}
	
	private List<Pair<String, Integer>> getAttributes(Cell cell) {
		List<Member> objectMembers = CollectionUtils.newList();
		Member objectMember = cell.getMember(Cube.OBJECT);
		
		Member propertyMember = cell.getMember(Cube.PROPERTY);
		if (propertyMember == null) {
			if (query.getSlicer() == null) {
				if (log.isDebugEnabled()) {
					log.debug("Property dimension has no specification for cell: "
							+ cell.toString());
				}
				throw new QueryDataReportingException("Property dimension has no specification for cell: "
						+ cell.toString());
			}
			propertyMember = query.getSlicer().getTuples().iterator().next().itemByHierarchy(Cube.PROPERTY);
		}
		List<Pair<String, Integer>> emptyAttributes = CollectionUtils.newList();
		if (objectMember == null) {
			List<Tuple> objectTuples = query.getSlicer().getTuples();
			for (Tuple objectTuple : objectTuples) {
				objectMember = objectTuple.itemByHierarchy(Cube.OBJECT);
				if (objectMember != null) {
					objectMembers.add(objectMember);
				}
			}

		} else {
			objectMembers.add(objectMember);
		}
		if (objectMembers.isEmpty()) {
			return emptyAttributes;
		}
		String[] keys = ((String) propertyMember.getKey()).split("\\.");
		String propertyName = keys[1];
		String propertyObjectType = keys[0];
		ValueIdAttributesProvider provider = getProvider(propertyObjectType, propertyName);
		boolean isObjectLevelEqualsPropertyType = objectMember.getLevel()
				.equals(propertyObjectType);

		List<TO<?>> objectsByPropertyLevel = getObjectsByLevel(objectMembers, propertyObjectType, isObjectLevelEqualsPropertyType);
		
		if (objectsByPropertyLevel.isEmpty()) {
			return emptyAttributes;
		}
		
		//Add Unit Converters
		UnitConverter unitConverter = null;
		TO<?> firstObject = objectsByPropertyLevel.iterator().next();
		unitConverter = getConverter(firstObject, propertyObjectType, propertyName);
		if (unitConverter != null) {
			tableProcessor.addUnitConverter(unitConverter);
			cell.setUnitConverterId(tableProcessor.getUnitConverters().indexOf(unitConverter));
		}
		Pair<List<TO<?>>, Member> cellMembers = Pair.newPair(objectsByPropertyLevel, propertyMember);
		
		if (attributesMap.get(cellMembers) == null) {
			List<Pair<String, Integer>> attributePairs = provider.addItems(objectsByPropertyLevel, propertyMember);
			attributesMap.put(cellMembers, attributePairs);
		}
		return attributesMap.get(cellMembers);
		
	}
	
	private ValueIdAttributesProvider getProvider(String propertyObjectType, String propertyName) {
		ValueIdAttributesProvider provider = null;
		PropertyDescr propertyDescr = dataSource.getPropertyDescriptor(
				propertyObjectType, propertyName);
		// Getting provider
		provider = tableProcessor.getProvider(propertyDescr.getTypeName());
		if (provider == null) {
			log.debug("Unsupported Java Property Type: "
					+ propertyDescr.getTypeName());
			if (log.isDebugEnabled()) {
				log.debug("Unsupported Java Property Type: "
						+ propertyDescr.getTypeName());
			}
			throw new QueryDataReportingException("Unsupported Java Property Type: "
					+ propertyDescr.getTypeName());
		}
		return provider;
	}
	
	private List<TO<?>> getObjectsByLevel(List<Member> objectMembers, String propertyObjectType, boolean isObjectLevelEqualsPropertyType){
		
		List<TO<?>> objects = CollectionUtils.newList();
		for (Member objectMember : objectMembers) {
			TO<?> cellObject = dataSource.getObjectId(objectMember);
			if (isObjectLevelEqualsPropertyType) {
				objects.add(cellObject);
			} else {
				Collection<TO<?>> children = dataSource.getRelatedObjects(
						cellObject, propertyObjectType, true);
				objects.addAll(children);
			}
		}
		return objects;
		
	}
	
	private UnitConverter getConverter(TO<?> to, String typeName, String propName) {
		ReportUserInfo reportUserInfo = tableProcessor.getReportUserInfo();
		UnitSystems unitSystems = reportUserInfo.getUnitSystems();
		UnitResolvers unitResolvers = reportUserInfo.getUnitResolvers();
		String targetSystem = reportUserInfo.getTargetSystem();
		// get property units
		String tName = to != null ? to.getTypeName() : typeName;
		PropertyUnits pu = unitResolvers.getUnits(tName, propName);
		if (pu == null) {// no units specified
			return null;// means no need to convert
		}
		DimensionRef dimRef = to != null ? pu.getDimensionReference(to, dataSource.getEnvironment()) : pu.getDimensionReference();
		// check if target system is the same as property's base system
		if (dimRef.getSystem().equals(targetSystem)) {
			return null;// no need to convert to the same system
		}
		// check if dimension is convertible
		if (unitSystems.getSimpleDimensions().containsKey(dimRef.getDimension()))
			return null;// means no need to convert
		try {
			return unitSystems.getUnitConverter(dimRef.getSystem(), targetSystem, dimRef.getDimension());
		} catch (ConverterException e) {
			log.warn("Cannot find converter for type \"" + to.getTypeName() + "\", property \"" + propName + "\"", e);
			return null;
		}
	}
	
}