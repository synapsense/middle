package com.synapsense.impl.alerting.notification;

import com.synapsense.dto.Message;
import com.synapsense.service.impl.notification.NotificationException;
import com.synapsense.service.impl.notification.Transport;
import org.apache.log4j.Logger;

import javax.naming.InitialContext;
import javax.naming.NamingException;

public class TransportAsService implements Transport {
	private final static Logger logger = Logger.getLogger(TransportAsService.class);

	private final String service;

	public TransportAsService(final String service) {
		this.service = service;
	}

	@Override
	public boolean isValid(Message message) {
		return lookup().isValid(message);
	}

	@Override
	public void send(final Message message) {
		try {
			lookup().send(message);
		} catch (final NotificationException e) {
			logger.warn("Cannot send message via service [" + service + "]", e);
		}
	}

	@Override
	public String getName() {
		return lookup().getName();
	}

	private Transport lookup() {
		InitialContext ctx = null;
		try {
			ctx = new InitialContext();
			return (Transport) ctx.lookup(service);
		} catch (final NamingException e) {
			logger.warn("Service with such name [" + service + "] does not exist", e);
		} finally {
			if (ctx != null) {
				try {
					ctx.close();
				} catch (final NamingException e) {
					logger.error("Unable to close initial context. "
					        + "Such error may lead to JVM 'out of memory' errors!", e);
				}
			}
		}
		return new NullTransport();
	}

	private class NullTransport implements Transport {

		@Override
		public boolean isValid(Message message) {
			return false;
		}

		@Override
		public void send(Message message) throws NotificationException {
			throw new NotificationException("Failed to lookup service [" + service + "]");
		}

		@Override
		public String getName() {
			return "Null";
		}
	}

}