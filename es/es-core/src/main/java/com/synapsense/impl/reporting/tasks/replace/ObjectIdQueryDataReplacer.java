package com.synapsense.impl.reporting.tasks.replace;

import com.synapsense.dto.TO;
import com.synapsense.impl.reporting.queries.TableProcessor.QueryClause;
import com.synapsense.impl.reporting.tasks.replace.QueryDataReplacerFactory.QueryDataReplacer;
import com.synapsense.impl.reporting.utils.ReportingConstants.QUERY.SYMBOL;
import com.synapsense.impl.reporting.utils.ReportingConstants.QUERY.WORD;
import com.synapsense.service.reporting.Cell;
import com.synapsense.service.reporting.olap.custom.Cube;
import com.synapsense.util.CollectionUtils;

import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ObjectIdQueryDataReplacer implements QueryDataReplacer {

	@Override
	public String replace(String envDataQuery, List<Cell> valueIdCells) {
		return envDataQuery.replaceAll(
				Pattern.quote("historical_values hv"),
				Matcher.quoteReplacement(
						createObjectValueIdTable(valueIdCells)
						+ SYMBOL.SPACE
						+ "historical_values hv"));
	}
	
	protected String createObjectValueIdTable(List<Cell> valueIdCells) {
		Set<String> valueIdSubSelectClauses = CollectionUtils.newLinkedSet();
		for (Cell cell : valueIdCells) {
			int objectId = (Integer) ((TO<?>)cell.getMember(Cube.OBJECT).getKey()).getID();
			
			for (Integer valueId : cell.getValueIds()) {
				StringBuilder valueIdSubSelectClauseSB = new StringBuilder();
				valueIdSubSelectClauseSB
					.append(QueryClause.SELECT.getName())
					.append(SYMBOL.SPACE)
					.append(SYMBOL.APOSTROPHE)
					.append(objectId)
					.append(SYMBOL.APOSTROPHE)
					.append(SYMBOL.SPACE)
					.append(WORD.AS)
					.append(SYMBOL.SPACE)
					.append("object_id")
					.append(SYMBOL.COMMA)
					.append(SYMBOL.APOSTROPHE)
					.append(valueId)
					.append(SYMBOL.APOSTROPHE)
					.append(SYMBOL.SPACE)
					.append(WORD.AS)
					.append(SYMBOL.SPACE)
					.append("value_id");
				valueIdSubSelectClauses.add(valueIdSubSelectClauseSB.toString());
				
			}
		}
		
		StringBuilder objectValueIdTableSB = new StringBuilder();
		objectValueIdTableSB.append(SYMBOL.OPEN_BRACKET);
		StringBuilder objectSubSelectSB = new StringBuilder();
		for (String valueIdSubSelectClause : valueIdSubSelectClauses) {
			objectSubSelectSB
				.append(valueIdSubSelectClause)
				.append(SYMBOL.SPACE)
				.append(WORD.UNION)
				.append(SYMBOL.SPACE);
		}
		if (objectSubSelectSB.length() != 0) {
			objectValueIdTableSB.append(objectSubSelectSB.substring(0, objectSubSelectSB.length() - 7));
		}
		objectValueIdTableSB.append(SYMBOL.CLOSE_BRACKET)
			.append(SYMBOL.SPACE)
			.append(WORD.AS)
			.append(SYMBOL.SPACE)
			.append("objects_valueids")
			.append(SYMBOL.COMMA)
			.append(SYMBOL.SPACE);
		return objectValueIdTableSB.toString();
	}

}
