package com.synapsense.impl.reporting.engine;

import java.io.Serializable;

import com.synapsense.service.user.EnvUserContext;
import com.synapsense.util.unitconverter.UnitResolvers;
import com.synapsense.util.unitconverter.UnitSystems;

public class ReportUserInfo implements Serializable {

	private static final long serialVersionUID = -6163794619656121258L;

    private UnitSystems unitSystems;

	private UnitResolvers unitResolvers;

	private int userId;
	
	private String targetSystem;

    private final EnvUserContext userContext;

	public ReportUserInfo(UnitSystems unitSystems, UnitResolvers unitResolvers,
			int userId, EnvUserContext userContext) {
		this.unitSystems = unitSystems;
		this.unitResolvers = unitResolvers;
		this.userId = userId;
        this.userContext = userContext;
		this.targetSystem = userContext.getUnitSystem();
	}

	public UnitSystems getUnitSystems() {
		return unitSystems;
	}

	public UnitResolvers getUnitResolvers() {
		return unitResolvers;
	}

	public int getUserId() {
		return userId;
	}
	
	public String getTargetSystem() {
		return targetSystem;
	}

    public EnvUserContext getUserContext() {
        return userContext;
    }

    @Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((targetSystem == null) ? 0 : targetSystem.hashCode());
		result = prime * result
				+ ((unitResolvers == null) ? 0 : unitResolvers.hashCode());
		result = prime * result
				+ ((unitSystems == null) ? 0 : unitSystems.hashCode());
		result = prime * result + userId;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		ReportUserInfo other = (ReportUserInfo) obj;
		if (targetSystem == null) {
			if (other.targetSystem != null) {
				return false;
			}
		} else if (!targetSystem.equals(other.targetSystem)) {
			return false;
		}
		if (unitResolvers == null) {
			if (other.unitResolvers != null) {
				return false;
			}
		} else if (!unitResolvers.equals(other.unitResolvers)) {
			return false;
		}
		if (unitSystems == null) {
			if (other.unitSystems != null) {
				return false;
			}
		} else if (!unitSystems.equals(other.unitSystems)) {
			return false;
		}
		if (userId != other.userId) {
			return false;
		}
		return true;
	}
	
}
