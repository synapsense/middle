package com.synapsense.impl.alerting;

import com.synapsense.service.AlertServiceManagement;

//dummy interface to make original AlertServiceManagement compatible
//with JMX standard MBean naming convention
public interface AlertServiceImplMBean extends AlertServiceManagement {

}
