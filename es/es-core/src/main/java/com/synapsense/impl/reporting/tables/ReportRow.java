package com.synapsense.impl.reporting.tables;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.synapsense.dto.TO;
import com.synapsense.service.reporting.Cell;
import com.synapsense.service.reporting.dto.CellTO;
import com.synapsense.service.reporting.dto.RowTO;
import com.synapsense.service.reporting.olap.Member;
import com.synapsense.service.reporting.olap.SimpleMember;
import com.synapsense.service.reporting.olap.custom.Cube;
import com.synapsense.util.CollectionUtils;

/**
 * The <code>ReportRow</code> class presents Row in ReportTable<br>
 * 
 * @author Grigory Ryabov
 * 
 */
public class ReportRow implements Iterator<Number>, Iterable<Cell>, Serializable {

	private static final long serialVersionUID = -4960106562080148922L;

	private int row;
	private int columnsNumber;
	private int column = 0;
	public static final int ROW_HEADER = 0;
	public static final int COLUMN_HEADER = 1;
	public static final int SLICER_AXIS_NUMBER = 2;

	public static final String OBJECT_NAME = "ObjectName";
	public static final String PROPERTY_NAME = "DataType";
	public static final String VALUE = "DataValue";
	public static final String TIMESTAMP = "Timestamp";
	public static final String SLICER_TIMESTAMP = "SlicerTimestamp";

	private Map<Integer, List<String>> tableHeaders = CollectionUtils.newMap();

	private ReportRowIterator reportRowIterator;

	private ArrayList<Cell> rowTableCells;

	private Map<String, Integer> axisIndexesMap =CollectionUtils.newMap();

	private List<Long> slicerTimestampList = CollectionUtils.newList();

	private long rowTimestamp; 

	public int getRowNumber() {
		return this.row;
	}
	
	public int getColumnNumber() {
		return this.column;
	}
	

	public ReportRow(int row, int columnsNumber, List<Cell> rowTableCells, Map<Integer, List<String>> tableHeaders) {
		this.row = row;
		this.columnsNumber = columnsNumber;
		this.rowTableCells = new ArrayList<>(rowTableCells);
		this.tableHeaders.putAll(tableHeaders);
		this.reportRowIterator = new ReportRowIterator();

		DimensionHeadersParsersMap.put(Cube.LEVEL_YEAR, TimestampParser);
		DimensionHeadersParsersMap.put(Cube.DIM_OBJECT, ObjectParser);
		DimensionHeadersParsersMap.put(Cube.DIM_PROPERTY, PropertyNameParser);
	}
	
	private List<CellTO> getCellTOs() {
		List<CellTO> rowCells = CollectionUtils.newList();
		calculateAxisIndexesFromHeaders();
		for (Cell cell : rowTableCells){
			Map<String, Object> cellMap = CollectionUtils.newMap();
			
			//Adding object
			Member objectMember = cell.getMember(Cube.OBJECT);
			if (objectMember != null) {
				TO<?> object = (TO<?>)objectMember.getKey();
				cellMap.put(Cube.DIM_OBJECT, object);
			}
			//Adding property
			Member propertyMember = cell.getMember(Cube.PROPERTY);
			if (propertyMember != null) {
				String propertyName = ((String)propertyMember.getKey());
				cellMap.put(propertyName, propertyName);
				cellMap.put(Cube.DIM_PROPERTY, propertyName);
			}
			//Adding time
			List<Long> allrowTimestamps = CollectionUtils.newList();
			if (rowTimestamp != 0) {
				allrowTimestamps.add(rowTimestamp);
			}
			cellMap.put(TIMESTAMP, allrowTimestamps);
			if (!slicerTimestampList.isEmpty()) {
				cellMap.put(TIMESTAMP, slicerTimestampList);
			}
			//Adding data value
			Number value = cell.getValue();
			cellMap.put(VALUE, value);

			rowCells.add(new CellTO(Collections.unmodifiableMap(cellMap)));
		}
		return rowCells;
	}
	
	private void calculateAxisIndexesFromHeaders() {
		for (Entry<Integer, List<String>> entry : this.tableHeaders.entrySet()) {
			Integer key = entry.getKey();
			List<String> headers = entry.getValue();
			switch (key) {
			case ROW_HEADER:
				parseHeader(headers.iterator().next(), ROW_HEADER, false);
				break;
			case COLUMN_HEADER:
				for (String header : headers){
					this.parseHeader(header, key, false);
				}
				break;
			case SLICER_AXIS_NUMBER:
				if (!headers.isEmpty()) {
					parseHeader(headers.iterator().next(), SLICER_AXIS_NUMBER, false);
					parseHeader(headers.get(headers.size() - 1), SLICER_AXIS_NUMBER, true);
				}
				break;
			}
		}
	}
	
	private DimensionHeadersParser ObjectParser = new ObjectParser();
	private DimensionHeadersParser PropertyNameParser = new PropertyNameParser();
	private DimensionHeadersParser TimestampParser = new TimestampParser();
	private Map<String, DimensionHeadersParser> DimensionHeadersParsersMap = CollectionUtils.newMap();

	public static interface DimensionHeadersParser {
		void parse(int axisIndex, List<String> dimHeaders, int headerIndex, boolean nextNeeded);
	}

	private class ObjectParser implements DimensionHeadersParser {
		@Override
		public void parse(int axisIndex, List<String> dimHeaders, int headerIndex, boolean nextNeeded) {
			axisIndexesMap.put(Cube.DIM_OBJECT, axisIndex);
		}
	}

	private class PropertyNameParser implements DimensionHeadersParser {
		@Override
		public void parse(int axisIndex, List<String> dimHeaders, int headerIndex, boolean nextNeeded) {
			axisIndexesMap.put(Cube.DIM_PROPERTY, axisIndex);
		}
	}

	private class TimestampParser implements DimensionHeadersParser {
		@Override
		public void parse(int axisIndex, List<String> dimHeaders, int headerIndex, boolean nextNeeded) {
			if (!Integer.valueOf(axisIndex).equals(SLICER_AXIS_NUMBER)){
				rowTimestamp = parseTimestamp(dimHeaders.toArray(new String[dimHeaders.size()]), headerIndex, nextNeeded);
			} else {
				slicerTimestampList.add(parseTimestamp(dimHeaders.toArray(new String[dimHeaders.size()]), 0, nextNeeded));
			}
		}
	}

	private void parseHeader(String header, int axisIndex, boolean nextNeeded) {
		String[] dimensionHeaders = header.split(",");
		List<String> dimHeaders = CollectionUtils.newArrayList(dimensionHeaders.length);
		for (String headerItem : dimensionHeaders) {
			dimHeaders.add(headerItem);
		}
		for (String dimensionHeader : dimHeaders) {
			String headerItems [] = dimensionHeader.split("\\.");
			DimensionHeadersParser parser = DimensionHeadersParsersMap.get(headerItems[0]);
			if (parser != null) {
				parser.parse(axisIndex, dimHeaders, axisIndex, nextNeeded);
			}
		}
	}

	@Override
	public boolean hasNext() {
		if (column + 1 > this.columnsNumber) {
			return false;
		}
		return true;
	}

	@Override
	public Number next() {
		return rowTableCells.get(++column - 1).getValue();
	}
	
	@Override
	public void remove() {
		throw new UnsupportedOperationException("Remove is not supported"); 
	}
	
	public RowTO getRow() {
		return new RowTO(getCellTOs());
	}
	
	private long parseTimestamp(String[] dimensionHeaders, int headerIndex, boolean nextNeeded) {
		long timeInMillis = 0;
		Cell cell = rowTableCells.iterator().next();
		Calendar calendar = Calendar.getInstance();
		Member timeMember = cell.getMember(Cube.TIME);
		
		if (timeMember == null) {
			calendar = Cube.TIME.getCalendarOfMember(getMemberByTimestamp(dimensionHeaders, headerIndex, nextNeeded));
			return calendar.getTimeInMillis();
		}
		String timeLevel = timeMember.getLevel();
		if (timeLevel.equals(Cube.LEVEL_ONE_MINUTE)) {
			timeInMillis = cell.getTimestamp();
			calendar.setTimeInMillis(timeInMillis);
		} else {
			calendar = Cube.TIME.getCalendarOfMember(getMemberByTimestamp(dimensionHeaders, headerIndex, nextNeeded));
		}
		return calendar.getTimeInMillis();
	}
	
	private Member getMemberByTimestamp(String[] dimensionHeaders, int headerIndex, boolean nextNeeded) {
		Map<String, String> timeMap = CollectionUtils.newMap();
		String lastTimeLevel = null;
		for (; headerIndex < dimensionHeaders.length; headerIndex++){
			String [] timeDimension =  dimensionHeaders[headerIndex].split("\\.");
			timeMap.put(timeDimension[0], timeDimension[1]);
			lastTimeLevel = timeDimension[0];
		}
		Member timeMember = new SimpleMember(Cube.TIME, lastTimeLevel, Integer.valueOf(timeMap.get(lastTimeLevel)));
		Member timeMemberParent = timeMember, parent;
		String parentLevel = Cube.TIME.getParentLevel(lastTimeLevel);
		while(timeMap.containsKey(parentLevel)) {
			parent = new SimpleMember(Cube.TIME, parentLevel, Integer.valueOf(timeMap.get(parentLevel)));
			((SimpleMember)timeMemberParent).setParent(parent);
			parentLevel = Cube.TIME.getParentLevel(parentLevel);
			timeMemberParent = timeMemberParent.getParent();
		}
		return (nextNeeded == true) ? Cube.TIME.getNext(timeMember) : timeMember;
	}

	@Override
	public Iterator<Cell> iterator() {
		return reportRowIterator;
	}
	
	public class ReportRowIterator implements Iterator<Cell>, Serializable {

		private static final long serialVersionUID = -4095505060427645782L;
		
		public ReportRowIterator() {
		}

		@Override
		public boolean hasNext() {
			if (column + 1 > columnsNumber) {
				return false;
			}
			return true;
		}

		@Override
		public Cell next() {
			return rowTableCells.get(++column - 1);
		}
		
		@Override
		public void remove() {
			throw new UnsupportedOperationException("Remove is not supported"); 
		}
	}
	
	public int size() {
		return columnsNumber;
	}
}
