package com.synapsense.impl.reporting.dao;

import javax.ejb.ApplicationException;

import com.synapsense.service.reporting.exception.ReportingRuntimeException;

@ApplicationException(rollback = true)
public class DAOReportingRuntimeException extends ReportingRuntimeException {

	private static final long serialVersionUID = 1042912078648382832L;

	public DAOReportingRuntimeException() {
	}

	public DAOReportingRuntimeException(String message) {
		super(message);
	}

	public DAOReportingRuntimeException(Throwable cause) {
		super(cause);
	}

	public DAOReportingRuntimeException(String message, Throwable cause) {
		super(message, cause);
	}
}
