package com.synapsense.impl.dlimport;

/**
 * Represents a Pier (the DL version of a PropertyTO) to be filled in later in
 * the export process.
 * 
 * @author Gabriel Helman
 * @since Jupiter
 * Date: 1/9/12
 * 
 */
public class PierBundle {
	private String DLID;
	private String propertyName;
	private String targetDLID;
	private String targetProperty;

	public PierBundle(String sourceProperty, String targetDLID, String targetProperty) {
		this.propertyName = sourceProperty;
		this.targetDLID = targetDLID;
		this.targetProperty = targetProperty;
	}

	public String getDlid() {
		return DLID;
	}

	public void setDlid(String dlid) {
		this.DLID = dlid;
	}

	public String getPropertyName() {
		return propertyName;
	}

	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}

	public String getTargetDLID() {
		return targetDLID;
	}

	public void setTargetDLID(String targetDLID) {
		this.targetDLID = targetDLID;
	}

	public String getTargetProperty() {
		return targetProperty;
	}

	public void setTargetProperty(String targetProperty) {
		this.targetProperty = targetProperty;
	}

	@Override
	public String toString() {
		return "PierBundle [DLID=" + DLID + ", propertyName=" + propertyName + ", targetDLID=" + targetDLID
		        + ", targetProperty=" + targetProperty + "]";
	}

}
