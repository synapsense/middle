package com.synapsense.impl.reporting.commandengine.state;

import com.synapsense.impl.reporting.commandengine.CommandContext;

public class CommandCancellingState extends CommandDefaultState {

	protected CommandCancellingState() {
		super("CANCELLING");
	}

	@Override
	public void complete(CommandContext context) {
		debugStateMethodCall(this, context, TOKEN_COMPLETE);
		try {
			context.getCommand().rollback();
			context.setState(CommandState.CANCELLED);
		} catch (RuntimeException e) {
			log.error(e.getLocalizedMessage(), e);
			context.fail(e);
		}
	}

	@Override
	public void fail(CommandContext context) {
		debugStateMethodCall(this, context, TOKEN_FAIL);
		context.setState(CommandState.ERROR);
	}
}
