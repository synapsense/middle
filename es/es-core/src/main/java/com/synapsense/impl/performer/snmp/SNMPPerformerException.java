package com.synapsense.impl.performer.snmp;

public class SNMPPerformerException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2953119646299414813L;

	public SNMPPerformerException(String desc) {
		super(desc);
	}

	public SNMPPerformerException(String desc, Throwable cause) {
		super(desc, cause);
	}
}
