
package com.synapsense.impl.erlnode;

import com.synapsense.dto.Alert;
import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.EnvException;
import com.synapsense.service.AlertService;
import com.synapsense.service.Environment;
import org.apache.log4j.Logger;

import java.util.*;

import com.ericsson.otp.erlang.OtpNodeStatus;

import com.synapsense.impl.erlnode.commands.RegisterProcessorCommand;

public class NodeStatusHandler extends OtpNodeStatus {
	private static final Logger log = Logger.getLogger(NodeStatusHandler.class);
    private static final String CONTROLLER_STATUS = "CONTROLLER_STATUS";

	private HashSet<String> connectedNodes = new HashSet<String>();
	private RegisterProcessorCommand rp;
	private Map<String, Object> globalState;

	public NodeStatusHandler(RegisterProcessorCommand rp, Map<String, Object> state) {
		this.rp = rp;
		this.globalState = state;
	}

	public Set<String> connectedNodes() {
		return connectedNodes;
	}

	@Override
	public void remoteStatus(String node, boolean up, Object info) {
		if(info != null && info.getClass().equals(java.io.IOException.class)) {
			java.io.IOException ioe = (java.io.IOException)info;
			log.info("remoteStatus(node:" + node + ", up:" + up + ", info:" + info + ":" + ioe.getMessage() + ")", ioe);
		} else {
			log.info("remoteStatus(node:" + node + ", up:" + up + ", info:" + info + ")");
		}

		// Create or update controller_status object
		try {
			TO<?> nodeStatus = null;
			ValueTO[] props = new ValueTO[1];
			props[0] = new ValueTO("name", node);
			Environment env = (Environment) globalState.get("env");
			Collection<TO<?>> statusObjs = env.getObjects(CONTROLLER_STATUS, props);
			if (statusObjs.isEmpty()) {
				nodeStatus = env.createObject(CONTROLLER_STATUS, props);
			} else {
				nodeStatus = (TO<?>)statusObjs.toArray()[0];
			}

			if (nodeStatus != null) {
				Long active = up ? 1L : 0L;
				env.setPropertyValue(nodeStatus, "active", active);
			}
		} catch (EnvException e) {
			log.error(e.getMessage(), e);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}

		if(up) {
			connectedNodes.add(node);
		} else {
			((AlertService) globalState.get("alertsvc")).raiseAlert(new Alert(
					null,
					"Controller went offline",
					"Active Control Critical Alert",
					new Date(),
					node + " has gone offline."));

			connectedNodes.remove(node);
			log.info("Deregistering NSVC processors for node '" + node + "'");
			rp.deregisterProcessorsForNode(node);
		}

	}

	@Override
	public void localStatus(String node, boolean up, Object info) {
		log.info("localStatus(node:" + node + ", up:" + up + ", info:" + info + ")");
	}

	@Override
	public void connAttempt(String node, boolean incoming, Object info) {
		if(info instanceof java.io.IOException) {
			java.io.IOException ioe = (java.io.IOException)info;
			log.info("connAttempt(node:" + node + ", incoming:" + incoming + ", info:" + info + ":" + ioe.getMessage() + ")", ioe);
		} else {
			log.info("connAttempt(node:" + node + ", incoming:" + incoming + ", info:" + info + ")");
		}
	}
}

