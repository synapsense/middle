package com.synapsense.impl.alerting.notification;

import static com.synapsense.impl.alerting.AlertConstants.TYPE.ALERT_TYPE_NAME;

import org.apache.log4j.Logger;

import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.EnvException;
import com.synapsense.service.Environment;

public class EnvNotificationStateManager implements NotificationStateManager {

	private final static Logger logger = Logger.getLogger(EnvNotificationStateManager.class);
	private final Environment env;

	public EnvNotificationStateManager(Environment env) {
		this.env = env;
	}

	@Override
	public void saveLastAttempt(String alertId, int lastAttempt) {

		TO<?> alertTO = TOFactory.getInstance().loadTO(ALERT_TYPE_NAME, Integer.parseInt(alertId));
		try {
			env.setPropertyValue(alertTO, "lastAttempt", lastAttempt);
		} catch (EnvException e) {
			logger.debug("Cannot save last attempt to alert " + alertId, e);
		}
	}

	@Override
	public void saveLastTierName(String alertId, String lastTierName) {

		TO<?> alertTO = TOFactory.getInstance().loadTO(ALERT_TYPE_NAME, Integer.parseInt(alertId));
		ValueTO[] values = { new ValueTO("lastNTierName", lastTierName), new ValueTO("lastAttempt", 0) };
		try {
			env.setAllPropertiesValues(alertTO, values);
		} catch (EnvException e) {
			logger.debug("Cannot save last tier name to alert " + alertId, e);
		}
	}

	@Override
	public Integer loadLastAttempt(String alertId) {

		Integer lastAttempt = null;
		TO<?> alertTO = TOFactory.getInstance().loadTO(ALERT_TYPE_NAME, Integer.parseInt(alertId));
		try {
			lastAttempt = env.getPropertyValue(alertTO, "lastAttempt", Integer.class);
		} catch (EnvException e) {
			logger.debug("Cannot load last attempt from alert " + alertId, e);
		}

		return lastAttempt;
	}

	@Override
	public String loadLastTierName(String alertId) {

		String lastTierName = null;
		TO<?> alertTO = TOFactory.getInstance().loadTO(ALERT_TYPE_NAME, Integer.parseInt(alertId));
		try {
			lastTierName = env.getPropertyValue(alertTO, "lastNTierName", String.class);
		} catch (EnvException e) {
			logger.debug("Cannot load last tier name from " + alertId, e);
		}

		return lastTierName;
	}

}
