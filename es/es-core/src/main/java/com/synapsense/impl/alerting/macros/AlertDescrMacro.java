package com.synapsense.impl.alerting.macros;

import java.util.regex.Pattern;

public class AlertDescrMacro extends ContextPropertyMacro {

	public static final String ALERT_DESCRIPTION = "ALERT_DESCRIPTION";
	
	public AlertDescrMacro() {
	    super(Pattern.compile("\\$(?i)alert_description\\$"), ALERT_DESCRIPTION);
    }

}
