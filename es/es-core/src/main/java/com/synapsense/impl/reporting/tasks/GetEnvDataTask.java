package com.synapsense.impl.reporting.tasks;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.Set;

import org.apache.log4j.Logger;

import com.synapsense.dal.generic.reporting.ReportingQueryExecutor;
import com.synapsense.dto.TO;
import com.synapsense.impl.reporting.queries.TableProcessor.QueryClause;
import com.synapsense.impl.reporting.queries.types.ReportingQueryType;
import com.synapsense.impl.reporting.tasks.fill.ReportTableFiller;
import com.synapsense.impl.reporting.tasks.replace.QueryDataReplacerFactory;
import com.synapsense.impl.reporting.utils.ReportingConstants.QUERY.SYMBOL;
import com.synapsense.impl.reporting.utils.ReportingConstants.QUERY.WORD;
import com.synapsense.service.reporting.Cell;
import com.synapsense.service.reporting.olap.custom.Cube;
import com.synapsense.util.CollectionUtils;

/**
 * <code>GetEnvDataTask</code> is task for getting historical environment data
 * 
 * @author Grigory Ryabov
 */
public class GetEnvDataTask implements ReportingTask {

	private static final Logger log = Logger.getLogger(ReportingTask.class);

	private EnvValuesTaskData envValuesTaskData;

	private ReportingQueryExecutor queryExecutor;
	
	public GetEnvDataTask(EnvValuesTaskData envValuesTaskData,
			ReportingQueryExecutor queryExecutor) {
		this.envValuesTaskData = envValuesTaskData;
		this.queryExecutor = queryExecutor;
	}

	@Override
	public void execute() {
		
		// Adding valueIds to all Cells
		if (envValuesTaskData == null) {
			if (log.isDebugEnabled()) {
				log.debug("EnvValuesTaskData is null. No Data to proceed task " + this.getName());
			}
			return;
		}
		List<Cell> valueIdCells = envValuesTaskData.getValueIdCells();
		
		String envDataQuery = envValuesTaskData.getEnvDataQuery();
		
		ReportingQueryType queryType = envValuesTaskData.getQueryType();
		
		envDataQuery = QueryDataReplacerFactory.getQueryDataReplacer(queryType).replace(envDataQuery, valueIdCells);
		
		//Add query parameters
		Set<Integer> allValueIds = CollectionUtils.newSet();
		Map<String, Collection<Integer>> envDataParameterMap = CollectionUtils.newMap();
		
		int index = 0;
		for (Cell valueIdCell : valueIdCells) {
			List<Integer> valueIds = valueIdCell.getValueIds();
			allValueIds.addAll(valueIds);
			
			if (queryType.equals(ReportingQueryType.MULTIPLE_OBJECT_CELL_TABLE)) {
				if (!valueIds.isEmpty()) {
					envDataParameterMap.put("cellValueIds" + index, valueIds);
					
				}
				index++;
			}
		}
		//If GetValueIdsTask hasn't returned any Value Ids we just return empty Report Data
		if (allValueIds.isEmpty()) {
			log.debug("No Value Ids returned from GetValueIdsTask. So returning empty ReportTable");
			return;
		}
		//Add all valueIds parameter
		envDataParameterMap.put("allValueIds", allValueIds);
		
		//Add formula valueId parameters
		Map<String, Set<Cell>> formulaCellsMap = envValuesTaskData.getFormulaCellsMap();
		int formulaIndex = 0;
		for (Entry<String, Set<Cell>> entry : formulaCellsMap.entrySet()) {
			Set<Cell> formulaCells = entry.getValue();
			Set<Integer> distinctFormulaValueIds = CollectionUtils.newSet();
			for (Cell formulaCell : formulaCells) {
				List<Integer> formulaCellValueIds = formulaCell.getValueIds();
				if (formulaCellValueIds.isEmpty()) {
					continue;
				}
				distinctFormulaValueIds.addAll(formulaCellValueIds);
			}
			envDataParameterMap.put("formulaValueIds" + formulaIndex, new LinkedList<>(distinctFormulaValueIds));
			formulaIndex++;
		}
		
		if (log.isTraceEnabled()) {
			log.trace(envValuesTaskData.getEnvDataQuery());
		}
		if (log.isInfoEnabled()) {
			log.info("GetEnvDataTask: Executing SQL Query");
		}
		List<Object[]> queryResult = queryExecutor.executeSQLQuery(envDataQuery
				, envDataParameterMap);
		if (log.isInfoEnabled()) {
			log.info("GetEnvDataTask: SQL Query executed");
		}
		
		if (queryResult.isEmpty()) {
			log.debug("No historical data returned. So returning empty ReportTable");
			return;
		}
		// Placing Query results to cells
		ReportTableFiller.fill(queryResult, envValuesTaskData);

		return;
		
	}
	
	@Override
	public String getName() {
		
		return "GetEnvDataTask";
		
	}	

}