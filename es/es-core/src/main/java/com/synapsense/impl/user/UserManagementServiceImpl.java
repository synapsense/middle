package com.synapsense.impl.user;

import com.synapsense.dto.*;
import com.synapsense.exception.EnvException;
import com.synapsense.exception.IllegalInputParameterException;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.exception.PropertyNotFoundException;
import com.synapsense.exception.UnableToConvertPropertyException;
import com.synapsense.exception.UnsupportedRelationException;
import com.synapsense.exception.UserManagementException;
import com.synapsense.impl.activitylog.ActivityLogger;
import com.synapsense.impl.utils.EnvironmentUtils;
import com.synapsense.search.Direction;
import com.synapsense.search.Path;
import com.synapsense.search.QueryDescription;
import com.synapsense.search.Relations;
import com.synapsense.service.Environment;
import com.synapsense.service.UserManagementService;
import com.synapsense.util.CollectionUtils;
import com.synapsense.util.CommonUtils;
import com.synapsense.util.ServerLoginModule.PasswordService;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.ejb.SessionContext;
import javax.interceptor.Interceptors;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.security.Principal;
import java.util.*;

// See ejb-jar.xml
//@Singleton(name="UserManagementService")
//@ConcurrencyManagement(ConcurrencyManagementType.BEAN)
//@Remote(UserManagementService.class)
public class UserManagementServiceImpl implements UserManagementService {

    private final static Logger log = Logger.getLogger(UserManagementServiceImpl.class);

    private static final String NULL_INPUT_PARAMETER_MESSAGE = "Supplied argument {%s} cannot be null or empty";

    private static final String SYSTEM_USER = "synapsense";
    private static final String ADMIN_USER = "admin";
    private static final String ADMIN_GROUP = "Administrators";

    private static final String USER_TYPE = "USER";
    private static final String PRIVILEGE_TYPE = "USER_PRIVILEGE";
    private static final String GROUP_TYPE = "USER_GROUP";
    private static final String PREFERENCE_TYPE = "USER_PREFERENCE";

    @Resource
    private SessionContext ctx;

    @EJB(beanName = "LocalEnvironment")
    private Environment env;

    private TO<?> findObjectByName(final String type, final String name) {
        if (CommonUtils.isStringEmpty(type)) {
            throw new IllegalInputParameterException(String.format(NULL_INPUT_PARAMETER_MESSAGE, "type"));
        }

        String propName = "";
        switch (type) {
            case USER_TYPE:
                propName = USER_NAME;
                break;
            case GROUP_TYPE:
                propName = GROUP_NAME;
                break;
            case PRIVILEGE_TYPE:
                propName = PRIVILEGE_NAME;
                break;
            default:
                break;
        }
        if (CommonUtils.isStringEmpty(name)) {
            throw new IllegalInputParameterException(String.format(NULL_INPUT_PARAMETER_MESSAGE, propName));
        }

        switch (type) {
            case USER_TYPE:
                if (name.equalsIgnoreCase(SYSTEM_USER)) return null;
                break;
            default:
                break;
        }

        Collection<TO<?>> objIds = env.getObjectsByType(type);
        for (TO<?> to : objIds) {
            String objName = (String)this.getProperty(to, propName);
            if (name.equalsIgnoreCase(objName)) {
                log.debug("Returning " + to.getTypeName() + ":" + to.getID() + " for '" + name + "'");
                return to;
            }
        }

        return null;
    }

    private TO<?> findUserPreference(TO<?> user, String prefName) throws ObjectNotFoundException,
            PropertyNotFoundException, UnableToConvertPropertyException {
        for (TO<?> pref : (Collection<TO<?>>) env.getPropertyValue(user, USER_PREFERENCES, Collection.class)) {
            if (prefName.equals(env.getPropertyValue(pref, USER_PREFERENCE_NAME, String.class))) {
                return pref;
            }
        }

        return null;
    }

    private Map<String, Object> getAllProperties(TO<?> obj) {
        try {
            return EnvironmentUtils.valuesAsMap(this.env.getAllPropertiesValues(obj));
        } catch (ObjectNotFoundException e) {
            e.printStackTrace();
        }

        return Collections.emptyMap();
    }

    private boolean isValidGroup(TO<?> obj) {
        return ((obj != null) && GROUP_TYPE.equals(obj.getTypeName())) ? true : false;
    }

    private boolean isValidPrivilege(TO<?> obj) {
        return ((obj != null) && PRIVILEGE_TYPE.equals(obj.getTypeName())) ? true : false;
    }

    private boolean isValidUser(TO<?> obj) {
        return ((obj != null) && USER_TYPE.equals(obj.getTypeName())) ? true : false;
    }

    @Override
    public TO<?> createGroup(String name, String description) throws UserManagementException {
        if (CommonUtils.isStringEmpty(name)) {
            throw new IllegalInputParameterException(String.format(NULL_INPUT_PARAMETER_MESSAGE, "name"));
        }

        if (findObjectByName(GROUP_TYPE, name) != null) {
            throw new UserManagementException("Group name is already in use. Please choose another name.");
        }

        try {
            Collection<ValueTO> props = CollectionUtils.newArrayList(1);
            props.add(new ValueTO(GROUP_NAME, name));
            props.add(new ValueTO(GROUP_DESCRIPTION, description));
            TO<?> created = env.createObject(GROUP_TYPE, props.toArray(new ValueTO[]{}));
            return created;
        } catch (EnvException e) {
            throw new UserManagementException("Unable to create group: " + e.getLocalizedMessage(), e);
        }

    }

    @Override
    public TO<?> createPrivilege(String name) throws UserManagementException {
        if (CommonUtils.isStringEmpty(name)) {
            throw new IllegalInputParameterException(String.format(NULL_INPUT_PARAMETER_MESSAGE, "name"));
        }

        if (findObjectByName(PRIVILEGE_TYPE, name) != null) {
            throw new UserManagementException("Privilege name is already in use. Please choose another name.");
        }

        try {
            Collection<ValueTO> props = CollectionUtils.newArrayList(1);
            props.add(new ValueTO(PRIVILEGE_NAME, name));
            TO<?> created = env.createObject(PRIVILEGE_TYPE, props.toArray(new ValueTO[]{}));
            return created;
        } catch (EnvException e) {
            throw new UserManagementException("Unable to create privilege: " + e.getLocalizedMessage(), e);
        }

    }

    @Interceptors(ActivityLogger.class)
    @Override
    public TO<?> createUser(String userName, String pwd, Object group) throws UserManagementException {
        if (CommonUtils.isStringEmpty(userName)) {
            throw new IllegalInputParameterException(String.format(NULL_INPUT_PARAMETER_MESSAGE, "user name"));
        }
        if (CommonUtils.isStringEmpty(pwd)) {
            throw new IllegalInputParameterException(String.format(NULL_INPUT_PARAMETER_MESSAGE, "password"));
        }
        Collection<Object> userGroups = this.validateUserGroups(group);
        if (userGroups.isEmpty()) {
            userGroups.add("Standard");
        }

        if (findObjectByName(USER_TYPE, userName) != null) {
            throw new UserManagementException("User name is already in use. Please choose another user name.");
        }

        try {
            Collection<ValueTO> userProps = CollectionUtils.newArrayList(1);
            userProps.add(new ValueTO(USER_NAME, userName));
            TO<?> createdUser = env.createObject(USER_TYPE, userProps.toArray(new ValueTO[]{}));

            try {
                env.setPropertyValue(createdUser, USER_PREFERENCES, CollectionUtils.newSet());

                this.setPassword(createdUser, pwd);
                this.setUserGroups(createdUser, userGroups);

            } catch (Exception e) {
                env.deleteObject(createdUser);
                throw e;
            }

            return createdUser;
        } catch (EnvException e) {
            throw new UserManagementException("Unable to create user: " + e.getLocalizedMessage(), e);
        }

    }

    @Interceptors(AuthCacheEvictionInterceptor.class)
    @Override
    public void deleteGroup(TO<?> group) throws UserManagementException {
        if (!isValidGroup(group)) {
            throw new IllegalInputParameterException(String.format(NULL_INPUT_PARAMETER_MESSAGE, "group"));
        }

        // check to see if the object exists
        Collection<TO<?>> allGroups = this.getAllGroups();
        if (!allGroups.contains(group)) {
            return;
        }

        // Don't allow delete of the 'Administrators' group
        String name = (String) this.getProperty(group, GROUP_NAME);
        if (ADMIN_GROUP.equals(name)) {
            throw new IllegalInputParameterException("Cannot delete the " + ADMIN_GROUP + " group.");
        }

        // Don't allow delete if users still in group
        for (TO<?> user : this.getAllUsers()) {
            Collection<TO<?>> usersGroups = this.getUserGroups(user);
            if (usersGroups.contains(group)) {
                throw new UserManagementException("This group cannot be deleted until all users have been removed from the group.");
            }
        }

        try {
            env.deleteObject(group);
        } catch (ObjectNotFoundException e) {
            log.warn("Unable to delete group '" + group.toString() + "'.");
        }
    }

    @Interceptors(AuthCacheEvictionInterceptor.class)
    @Override
    public void deletePrivilege(TO<?> privilege) throws UserManagementException {
        if (!isValidPrivilege(privilege)) {
            throw new IllegalInputParameterException(String.format(NULL_INPUT_PARAMETER_MESSAGE, "privilege"));
        }

        // check to see if the object exists
        Collection<TO<?>> allPrivileges = this.getAllPrivileges();
        if (!allPrivileges.contains(privilege)) {
            return;
        }

        // Don't allow delete if still used by groups or users
        for (TO<?> group : this.getAllGroups()) {
            Collection<TO<?>> groupPrivs = this.getGroupPrivileges(group);
            if (groupPrivs.contains(privilege)) {
                throw new UserManagementException("This privilege cannot be deleted when there are groups who have this privilege.");
            }
        }

        try {
            env.deleteObject(privilege);
        } catch (ObjectNotFoundException e) {
            log.warn("Unable to delete privilege '" + privilege.toString() + "'.");
        }
    }

    @Override
    @Interceptors({ActivityLogger.class, AuthCacheEvictionInterceptor.class})
    public void deleteUser(TO<?> user) throws UserManagementException {
        if (!isValidUser(user)) {
            throw new IllegalInputParameterException(String.format(NULL_INPUT_PARAMETER_MESSAGE, "user"));
        }

        // check to see if the object exists
        Collection<TO<?>> allUsers = env.getObjectsByType(USER_TYPE);
        if (!allUsers.contains(user)) {
            return;
        }

        String name = (String) this.getProperty(user, USER_NAME);

        // Don't allow delete of the 'admin' user
        if (ADMIN_USER.equals(name)) {
            throw new IllegalInputParameterException("Cannot delete the " + ADMIN_USER + " user.");
        }

        // Don't allow delete of the 'synapsense' user
        if (SYSTEM_USER.equals(name)) {
            throw new IllegalInputParameterException("Cannot delete the " + SYSTEM_USER + " user.");
        }

        // delete all user preferences
        try {
            for (TO<?> prefId : (Collection<TO<?>>) env.getPropertyValue(user, USER_PREFERENCES, Collection.class)) {
                env.deleteObject(prefId);
            }
        } catch (ObjectNotFoundException | PropertyNotFoundException | UnableToConvertPropertyException e1) {
            log.warn("Problem with deleting preferences of user '" + user.toString() + "'.", e1);
        }

        try {
            env.deleteObject(user);
        } catch (ObjectNotFoundException e) {
            log.warn("Unable to delete user '" + user.toString() + "'.");
        }
    }

    @Override
    public Collection<TO<?>> getAllGroups() {
        return env.getObjectsByType(GROUP_TYPE);
    }

    @Override
    public Collection<TO<?>> getAllPrivileges() {
        return env.getObjectsByType(PRIVILEGE_TYPE);
    }

    @Override
    public Collection<TO<?>> getAllUsers() {
        Collection<TO<?>> users = env.getObjectsByType(USER_TYPE);
        Iterator<TO<?>> it = users.iterator();
        while (it.hasNext()) {
            TO<?> u = it.next();
            try {
                String name = this.env.getPropertyValue(u, USER_NAME, String.class);
                if (SYSTEM_USER.equals(name)) {
                    it.remove();
                }
            } catch (ObjectNotFoundException | PropertyNotFoundException | UnableToConvertPropertyException e) {
                log.warn("Unable to get user information. User object " + u + " not found.");
            }
        }

        return users;
    }

    @Override
    public Collection<String> getChildrenPrivilegeIds(String parentName) throws UserManagementException {
        if (CommonUtils.isStringEmpty(parentName))
            throw new IllegalInputParameterException(String.format(NULL_INPUT_PARAMETER_MESSAGE, "parentName"));

        List<String> privilegeIds = new ArrayList<>();
        TO<?> privId = findObjectByName(PRIVILEGE_TYPE, parentName);
        if (!isValidPrivilege(privId)) {
            log.warn("No such privilege: " + parentName);
        }

        try {
            for (TO<?> sysPrivId : env.getChildren(privId)) {
                privilegeIds.add(env.getPropertyValue(sysPrivId, PRIVILEGE_NAME, String.class));
            }
        } catch (ObjectNotFoundException | PropertyNotFoundException | UnableToConvertPropertyException e) {
            log.warn("Unable to get children privileges of '" + parentName + "'.", e);
        }

        Collections.sort(privilegeIds, new Comparator<String>() {
            @Override public int compare(final String o1, final String o2) {
                return o1.compareTo(o2);
            }
        });

        return privilegeIds;
    }

    @Override
    public TO<?> getGroup(String name) {
        return findObjectByName(GROUP_TYPE, name);
    }

    @Override
    public Map<String, Object> getGroupInformation(TO<?> group) {
        if (!isValidGroup(group)) {
            throw new IllegalInputParameterException(String.format(NULL_INPUT_PARAMETER_MESSAGE, "group"));
        }

        return this.getAllProperties(group);
    }

    @Override
    public Collection<String> getGroupPrivilegeIds(TO<?> group) throws UserManagementException {
        try {
            Collection<TO<?>> privs = getGroupPrivileges(group);
            List<String> res = new ArrayList<String>(privs.size());
            for (TO<?> priv : privs) {
                String name = this.env.getPropertyValue(priv, PRIVILEGE_NAME, String.class);
                res.add(name);
            }

            Collections.sort(res, new Comparator<String>() {
                @Override public int compare(final String o1, final String o2) {
                    return o1.compareTo(o2);
                }
            });

            return res;

        } catch (ObjectNotFoundException | PropertyNotFoundException | UnableToConvertPropertyException e) {
            log.warn("Unable to get group privileges for " + group.toString() + ".");
        }

        return Collections.emptyList();
    }

    @Override
    public Collection<TO<?>> getGroupPrivileges(TO<?> group) throws UserManagementException {
        if (!isValidGroup(group)) {
            throw new IllegalInputParameterException(String.format(NULL_INPUT_PARAMETER_MESSAGE, "group"));
        }

        try {
            return env.getChildren(group, PRIVILEGE_TYPE);
        } catch (ObjectNotFoundException e) {
            log.warn("Unable to get group privileges for " + group.toString() + ".", e);
        }

        return Collections.emptyList();
    }

    @Override
    public Collection<String> getGroupUserIds(TO<?> group) throws UserManagementException {
        try {
            Collection<TO<?>> users = getGroupUsers(group);
            List<String> res = new ArrayList<String>(users.size());
            for (TO<?> user : users) {
                String name = this.env.getPropertyValue(user, USER_NAME, String.class);
                res.add(name);
            }

            Collections.sort(res, new Comparator<String>() {
                @Override public int compare(final String o1, final String o2) {
                    return o1.compareTo(o2);
                }
            });

            return res;

        } catch (ObjectNotFoundException | PropertyNotFoundException | UnableToConvertPropertyException e) {
            log.warn("Unable to get group users for " + group.toString() + ".");
        }

        return Collections.emptyList();
    }

    @Override
    public Collection<TO<?>> getGroupUsers(TO<?> group) throws UserManagementException {
        if (!isValidGroup(group)) {
            throw new IllegalInputParameterException(String.format(NULL_INPUT_PARAMETER_MESSAGE, "group"));
        }

        QueryDescription qd = new QueryDescription(1);
        qd.startQueryFrom(group);

        qd.addPath(1, new Path(Relations.named(USER_GROUPS), Direction.INCOMING));
        try {
            Collection<TO<?>> users = env.getObjects(qd);
            Iterator<TO<?>> it = users.iterator();
            while (it.hasNext()) {
                TO<?> u = it.next();
                String name = this.env.getPropertyValue(u, USER_NAME, String.class);
                if (SYSTEM_USER.equals(name)) {
                    it.remove();
                }
            }

            return users;
        } catch (ObjectNotFoundException | PropertyNotFoundException | UnableToConvertPropertyException e) {
            log.warn("Unable to get group users for " + group.toString() + ".");
        }

        return Collections.emptyList();
    }

    @Override
    public TO<?> getPrivilege(String name) {
        return findObjectByName(PRIVILEGE_TYPE, name);
    }

    @Override
    public Map<String, Object> getPrivilegeInformation(TO<?> privilege) {
        if (!isValidPrivilege(privilege)) {
            throw new IllegalInputParameterException(String.format(NULL_INPUT_PARAMETER_MESSAGE, "privilege"));
        }

        return this.getAllProperties(privilege);
    }

    @Override
    public Object getProperty(TO<?> objId, String propName) {
        if (objId == null) {
            throw new IllegalInputParameterException(String.format(NULL_INPUT_PARAMETER_MESSAGE, "privilege"));
        }
        if (CommonUtils.isStringEmpty(propName)) {
            throw new IllegalInputParameterException(String.format(NULL_INPUT_PARAMETER_MESSAGE, "propName"));
        }

        try {
            Object value = env.getPropertyValue(objId, propName, Object.class);
            return value != null ? value : "";
        } catch (ObjectNotFoundException | PropertyNotFoundException | UnableToConvertPropertyException e) {
            log.warn("Unable to get property '" + propName + " from " + objId.toString(), e);
        }

        return "";
    }

    @Override
    public String getRawPassword(TO<?> user) throws UserManagementException {
        if (!isValidUser(user)) {
            throw new IllegalInputParameterException(String.format(NULL_INPUT_PARAMETER_MESSAGE, "user"));
        }

        try {
            return env.getPropertyValue(user, USER_PASSWORD, String.class);
        } catch (ObjectNotFoundException | PropertyNotFoundException | UnableToConvertPropertyException e) {
            throw new UserManagementException("Unable to get raw password.", e);
        }
    }

    @Override
    public TO<?> getUser(String userName) {
        return findObjectByName(USER_TYPE, userName);
    }

    @Override
    public Collection<String> getUserGroupIds(TO<?> user) throws UserManagementException {
        if (!isValidUser(user)) {
            throw new IllegalInputParameterException(String.format(NULL_INPUT_PARAMETER_MESSAGE, "user"));
        }

        try {
            List<String> res = new ArrayList<>();
            Collection<TO<?>> groupTOs = env.getPropertyValue(user, USER_GROUPS, Collection.class);
            for (TO<?> g : groupTOs) {
                res.add(env.getPropertyValue(g, GROUP_NAME, String.class));
            }

            Collections.sort(res, new Comparator<String>() {
                @Override public int compare(final String o1, final String o2) {
                    return o1.compareTo(o2);
                }
            });

            return res;

        } catch (ObjectNotFoundException | PropertyNotFoundException | UnableToConvertPropertyException e) {
            log.warn("Unable to get groups of user '" + user.toString() + "'.", e);
        }

        return Collections.emptyList();
    }

    @Override
    public Collection<TO<?>> getUserGroups(TO<?> user) throws UserManagementException {
        if (!isValidUser(user)) {
            throw new IllegalInputParameterException(String.format(NULL_INPUT_PARAMETER_MESSAGE, "user"));
        }

        try {
            return env.getPropertyValue(user, USER_GROUPS, Collection.class);
        } catch (ObjectNotFoundException | PropertyNotFoundException | UnableToConvertPropertyException e) {
            log.warn("Unable to get groups of user '" + user.toString() + "'.", e);
        }

        return Collections.emptyList();
    }

    @Override
    public Map<String, Object> getUserInformation(TO<?> user) {
        if (!isValidUser(user)) {
            throw new IllegalInputParameterException(String.format(NULL_INPUT_PARAMETER_MESSAGE, "user"));
        }

        return this.getAllProperties(user);
    }

    @Override
    public Map<String, Object> getUserPreferences(TO<?> user) {
        if (!isValidUser(user)) {
            throw new IllegalInputParameterException(String.format(NULL_INPUT_PARAMETER_MESSAGE, "user"));
        }
        try {
            Map<String, Object> result = CollectionUtils.newMap();
            for (TO<?> preferenceTO : (Collection<TO<?>>) env.getPropertyValue(user, USER_PREFERENCES, Collection.class)) {
                Map<String, Object> values = EnvironmentUtils.valuesAsMap(env.getAllPropertiesValues(preferenceTO));
                result.put((String) values.get(USER_PREFERENCE_NAME), values.get(USER_PREFERENCE_VALUE));
            }

            return result;

        } catch (ObjectNotFoundException | PropertyNotFoundException | UnableToConvertPropertyException e) {
            log.warn("Unable to get preferences of user '" + user.toString() + "'.", e);
        }

        return Collections.emptyMap();
    }

    @Override
    public Collection<String> getUserPrivilegeIds(TO<?> user) throws UserManagementException {
        if (!isValidUser(user)) {
            throw new IllegalInputParameterException(String.format(NULL_INPUT_PARAMETER_MESSAGE, "user"));
        }

        try {
            List<String> privilegeIds = new ArrayList<>();
            for (Object group : env.getPropertyValue(user, USER_GROUPS, Collection.class)) {
                for (TO<?> privilege : env.getChildren((TO<?>) group)) {
                    privilegeIds.add(env.getPropertyValue(privilege, PRIVILEGE_NAME, String.class));
                }
            }

            Collections.sort(privilegeIds, new Comparator<String>() {
                @Override public int compare(final String o1, final String o2) {
                    return o1.compareTo(o2);
                }
            });

            return privilegeIds;
        } catch (ObjectNotFoundException | PropertyNotFoundException | UnableToConvertPropertyException e) {
            log.warn("Unable to get privileges of user '" + user.toString() + "'.", e);
        }

        return Collections.emptyList();
    }

    @Override
    public void removeUserPreference(TO<?> user, String preferenceName) throws UserManagementException {
        if (!isValidUser(user)) {
            throw new IllegalInputParameterException(String.format(NULL_INPUT_PARAMETER_MESSAGE, "user"));
        }
        if (CommonUtils.isStringEmpty(preferenceName)) {
            throw new IllegalInputParameterException(String.format(NULL_INPUT_PARAMETER_MESSAGE, "preference name"));
        }

        // removes it from the user's
        try {
            TO<?> toRemove = findUserPreference(user, preferenceName);

            if (toRemove == null) {
                log.warn("User has no such preference : " + preferenceName + ".Nothing to remove!");
                return;
            }

            env.removePropertyValue(user, USER_PREFERENCES, toRemove);
        } catch (ObjectNotFoundException | PropertyNotFoundException | UnableToConvertPropertyException e) {
            log.warn("Unable to remove user preference " + preferenceName, e);
        }
    }

    @Interceptors(AuthCacheEvictionInterceptor.class)
    @Override
    public void setGroupPrivileges(TO<?> group, Collection<String> privileges) throws UserManagementException {
        if (!isValidGroup(group)) {
            throw new IllegalInputParameterException(String.format(NULL_INPUT_PARAMETER_MESSAGE, "group"));
        }

        if (privileges == null || privileges.isEmpty())
            throw new IllegalInputParameterException(String.format(NULL_INPUT_PARAMETER_MESSAGE, "privileges"));

        try {
            Collection<TO<?>> oldPrivileges = env.getChildren(group, PRIVILEGE_TYPE);
            Collection<String> tmpPrivileges = new LinkedList<String>(privileges);
            // 1. copy retained privileges
            for (TO<?> oldPriv : oldPrivileges) {
                String oldName = env.getPropertyValue(oldPriv, PRIVILEGE_NAME, String.class);
                if (!tmpPrivileges.remove(oldName)) {
                    env.removeRelation(group, oldPriv);
                }
            }
            // 2. add assigned privileges
            for (String privName : tmpPrivileges) {
                TO<?> privTO = findObjectByName(PRIVILEGE_TYPE, privName);
                if (privTO == null)
                    throw new UserManagementException("Invalid privilege specified.");
                env.setRelation(group, privTO);
            }
        } catch (ObjectNotFoundException | PropertyNotFoundException | UnableToConvertPropertyException
                | UnsupportedRelationException e) {
            throw new UserManagementException("Unable to set group privileges.", e);
        }
    }

    @Override
    @Interceptors({ActivityLogger.class, AuthCacheEvictionInterceptor.class})
    public void setPassword(TO<?> user, String password) throws UserManagementException {
        try {
            this.setRawPassword(user, PasswordService.encrypt(password));
        } catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
            throw new UserManagementException("Unable to set user password.", e);
        }
    }

    @Override
    @Interceptors({ActivityLogger.class, AuthCacheEvictionInterceptor.class})
    public void setProperty(TO<?> objId, String propName, Object propValue) throws UserManagementException {
        if (objId == null) {
            throw new IllegalInputParameterException(String.format(NULL_INPUT_PARAMETER_MESSAGE, "privilege"));
        }
        if (CommonUtils.isStringEmpty(propName)) {
            throw new IllegalInputParameterException(String.format(NULL_INPUT_PARAMETER_MESSAGE, "propName"));
        }

        Map<String, Object> changedProps = CollectionUtils.newMap();
        changedProps.put(propName, propValue);

        switch (objId.getTypeName()) {
            case GROUP_TYPE:
                this.updateGroup(objId, changedProps);
                break;
            case PRIVILEGE_TYPE:
                this.updatePrivilege(objId, changedProps);
                break;
            case USER_TYPE:
                this.updateUser(objId, changedProps);
                break;
        }
    }

    @Interceptors(AuthCacheEvictionInterceptor.class)
    @Override
    public void setRawPassword(TO<?> user, String password) throws UserManagementException {
        if (!isValidUser(user)) {
            throw new IllegalInputParameterException(String.format(NULL_INPUT_PARAMETER_MESSAGE, "user"));
        }
        if (CommonUtils.isStringEmpty(password)) {
            throw new IllegalInputParameterException(String.format(NULL_INPUT_PARAMETER_MESSAGE, "password"));
        }

        Principal p = ctx.getCallerPrincipal();
        String userName = null;
        try {
            userName = env.getPropertyValue(user, USER_NAME, String.class);
        } catch (ObjectNotFoundException | PropertyNotFoundException | UnableToConvertPropertyException e) {
        }

        // hard-coded
        if (!(ctx.isCallerInRole("root_permission") || ctx.isCallerInRole("CREATE_EDIT_USERS") || p.getName().equals(
                userName))) {
            throw new UserManagementException(
                    "Unable to change another's password or you don't have 'edit users' privileges!");
        }

        try {
            env.setPropertyValue(user, USER_PASSWORD, password);
        } catch (ObjectNotFoundException | PropertyNotFoundException | UnableToConvertPropertyException e) {
            throw new UserManagementException("Unable to set password.", e);
        }
    }

    private Collection<Object> validateUserGroups(Object groups) {
        Collection<Object> tmpGroups = CollectionUtils.newSet();
        if (groups instanceof String) {
            if (!CommonUtils.isStringEmpty((String) groups)) {
                tmpGroups.add(groups);
            }
        } else if (groups instanceof Collection) {
            tmpGroups = (Collection) groups;
        }

        return tmpGroups;
    }

    @Override
    public void setUserGroups(TO<?> user, Object groups) throws UserManagementException {
        if (!isValidUser(user)) {
            throw new IllegalInputParameterException(String.format(NULL_INPUT_PARAMETER_MESSAGE, "userName"));
        }

        Collection<Object> tmpGroups = this.validateUserGroups(groups);
        if (tmpGroups.isEmpty()) {
            throw new IllegalInputParameterException(String.format(NULL_INPUT_PARAMETER_MESSAGE, "groups"));
        }

        Collection<TO<?>> newGroups = new HashSet<TO<?>>(tmpGroups.size());
        TO<?> groupTO;
        for (Object group : (Collection) tmpGroups) {
            if (group instanceof String) {
                groupTO = findObjectByName(GROUP_TYPE, (String) group);
                if (groupTO != null) {
                    newGroups.add(groupTO);
                } else {
                    throw new UserManagementException("Invalid group specified.");
                }
            } else if (group instanceof TO) {
                newGroups.add((TO) group);
            } else {
                throw new IllegalInputParameterException(String.format(NULL_INPUT_PARAMETER_MESSAGE, "groups"));
            }
        }

        try {
            env.setPropertyValue(user, USER_GROUPS, newGroups);
        } catch (ObjectNotFoundException | PropertyNotFoundException | UnableToConvertPropertyException e) {
            throw new UserManagementException("Unable to set user groups.", e);
        }

    }

    @Override
    public void setUserPreferenceValue(TO<?> user, String preferenceName, String value) throws UserManagementException {
        if (!isValidUser(user)) {
            throw new IllegalInputParameterException(String.format(NULL_INPUT_PARAMETER_MESSAGE, "user"));
        }
        if (CommonUtils.isStringEmpty(preferenceName)) {
            throw new IllegalInputParameterException(String.format(NULL_INPUT_PARAMETER_MESSAGE, "preference name"));
        }
        if (value == null) {
            throw new IllegalInputParameterException(String.format(NULL_INPUT_PARAMETER_MESSAGE, "value"));
        }

        TO<?> changingEntry = null;
        try {
            // looking for preference
            changingEntry = findUserPreference(user, preferenceName);

            // if preference exists just set its value
            if (changingEntry != null) {
                env.setPropertyValue(changingEntry, USER_PREFERENCE_VALUE, value);
                return;
            }

            // if no , execute all creation logic
            changingEntry = env.createObject(
                    PREFERENCE_TYPE,
                    new ValueTO[]{new ValueTO(USER_PREFERENCE_NAME, preferenceName),
                            new ValueTO(USER_PREFERENCE_VALUE, value)});
            env.addPropertyValue(user, USER_PREFERENCES, changingEntry);
        } catch (EnvException e) {
            throw new UserManagementException("Unable to set user preference.", e);
        }
    }

    @Interceptors(AuthCacheEvictionInterceptor.class)
    @Override
    public void updateGroup(TO<?> group, Map<String, Object> changedProps) throws UserManagementException {
        if (!isValidGroup(group)) {
            throw new IllegalInputParameterException(String.format(NULL_INPUT_PARAMETER_MESSAGE, "group"));
        }

        String oldName = "";
        String newName = (String) changedProps.get(GROUP_NAME);

        try {
            oldName = env.getPropertyValue(group, GROUP_NAME, String.class);

            if (newName != null && !oldName.equals(newName)) {
                if (findObjectByName(GROUP_TYPE, newName) != null) {
                    throw new UserManagementException("Group name is already in use. Please choose another name.");
                }
            }

            Collection<ValueTO> changedValues = EnvironmentUtils.mapAsValues(changedProps);
            env.setAllPropertiesValues(group, changedValues.toArray(new ValueTO[]{}));
        } catch (ObjectNotFoundException | PropertyNotFoundException | UnableToConvertPropertyException e) {
            throw new UserManagementException("Unable to update group properties.", e);
        }
    }

    @Override
    public void updateGroupStatistics(TO<?> group, String statistics) throws UserManagementException {
        if (!isValidGroup(group)) {
            throw new IllegalInputParameterException(String.format(NULL_INPUT_PARAMETER_MESSAGE, "group"));
        }

        try {
            env.setPropertyValue(group, GROUP_STATISTICS, statistics);
        } catch (ObjectNotFoundException | PropertyNotFoundException | UnableToConvertPropertyException e) {
            throw new UserManagementException("Unable to update group properties.", e);
        }
    }

    @Interceptors(AuthCacheEvictionInterceptor.class)
    @Override
    public void updatePrivilege(TO<?> privilege, Map<String, Object> changedProps) throws UserManagementException {
        if (!isValidPrivilege(privilege)) {
            throw new IllegalInputParameterException(String.format(NULL_INPUT_PARAMETER_MESSAGE, "privilege"));
        }

        String oldName = "";
        String newName = (String) changedProps.get(PRIVILEGE_NAME);

        try {
            oldName = env.getPropertyValue(privilege, PRIVILEGE_NAME, String.class);

            if (newName != null && !oldName.equals(newName)) {
                if (findObjectByName(PRIVILEGE_TYPE, newName) != null) {
                    throw new UserManagementException("Privilege name is already in use. Please choose another name.");
                }
            }

            Integer system = env.getPropertyValue(privilege, PRIVILEGE_SYSTEM, Integer.class);
            if (system != null && system == 1)
                throw new UserManagementException("Cannot modify system privilege: " + oldName);

            // set new name and description.
            // ignore whatever user specified for system flag:
            // system privileges cannot be updated and non-system
            // cannot be turned into system ones.
            Collection<ValueTO> changedValues = EnvironmentUtils.mapAsValues(changedProps);
            env.setAllPropertiesValues(privilege, changedValues.toArray(new ValueTO[]{}));
        } catch (ObjectNotFoundException | PropertyNotFoundException | UnableToConvertPropertyException e) {
            throw new UserManagementException("Unable to update privilege properties.", e);
        }
    }

    @Override
    @Interceptors({ActivityLogger.class, AuthCacheEvictionInterceptor.class})
    public void updateUser(TO<?> user, Map<String, Object> changedProps) throws UserManagementException {
        if (!isValidUser(user)) {
            throw new IllegalInputParameterException(String.format(NULL_INPUT_PARAMETER_MESSAGE, "user"));
        }

        String oldName = "";
        String newName = (String) changedProps.get(USER_NAME);

        try {
            oldName = env.getPropertyValue(user, USER_NAME, String.class);

            if (newName != null && !oldName.equals(newName)) {
                if (findObjectByName(USER_TYPE, newName) != null) {
                    throw new UserManagementException("User name is already in use. Please choose another user name.");
                } else if (SYSTEM_USER.equals(newName)) {
                    throw new UserManagementException("Unable to change user name, 'synapsense' is a reserved user name.");
                }
            }

            String pwd = (String) changedProps.get(USER_PASSWORD);
            changedProps.remove(USER_PASSWORD);
            if (!CommonUtils.isStringEmpty(pwd)) {
                this.setPassword(user, pwd);
            }

            Object groups = changedProps.get(USER_GROUPS);
            if (groups != null) {
                changedProps.remove(USER_GROUPS);
                this.setUserGroups(user, groups);
            }

            if (!changedProps.isEmpty()) {
                Collection<ValueTO> changedValues = EnvironmentUtils.mapAsValues(changedProps);
                env.setAllPropertiesValues(user, changedValues.toArray(new ValueTO[]{}));
            }

        } catch (ObjectNotFoundException | PropertyNotFoundException | UnableToConvertPropertyException e) {
            throw new UserManagementException("Unable to update privilege properties.", e);
        }
    }

}
