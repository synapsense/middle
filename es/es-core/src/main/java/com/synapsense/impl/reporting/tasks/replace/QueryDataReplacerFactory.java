package com.synapsense.impl.reporting.tasks.replace;

import com.synapsense.impl.reporting.queries.types.ReportingQueryType;
import com.synapsense.service.reporting.Cell;
import com.synapsense.util.CollectionUtils;

import java.util.List;
import java.util.Map;

public class QueryDataReplacerFactory {
	
	public static interface QueryDataReplacer {
		public String replace(String envDataQuery, List<Cell> valueIdCells);
	}
	
    private static final Map<ReportingQueryType, QueryDataReplacer> replacers = CollectionUtils.newMap();

    static {
    	replacers.put(ReportingQueryType.SINGLE_PROPERTY_CELL_TABLE,
                new ValueIdQueryDataReplacer());
    	replacers.put(ReportingQueryType.SAME_MULTIPLE_PROPERTY_CELL_TABLE,
                new ObjectIdQueryDataReplacer());
    	replacers.put(ReportingQueryType.MULTIPLE_OBJECT_CELL_TABLE,
                new MultipleObjectQueryDataReplacer());
    	replacers.put(ReportingQueryType.DIFFERENT_MULTIPLE_PROPERTY_CELL_TABLE,
                new ObjectIdPropertyNameQueryDataReplacer(new ObjectIdQueryDataReplacer()));
    }
    
    public static QueryDataReplacer getQueryDataReplacer(ReportingQueryType queryType) {
    	return replacers.get(queryType);
    }

    public static class ForwardingObjectIdQueryDataReplacer extends ObjectIdQueryDataReplacer {

        private ObjectIdQueryDataReplacer replacer;

        public ForwardingObjectIdQueryDataReplacer(ObjectIdQueryDataReplacer replacer) {
            this.replacer = replacer;
        }

        @Override
        public String replace (String envDataQuery, List<Cell> valueIdCells) {
            return replacer.replace(envDataQuery, valueIdCells);
        }
        
        @Override
        protected String createObjectValueIdTable(List<Cell> valueIdCells) {
        	return replacer.createObjectValueIdTable(valueIdCells);
        }
 
    }

}
