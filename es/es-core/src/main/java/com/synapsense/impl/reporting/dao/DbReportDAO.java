package com.synapsense.impl.reporting.dao;

import com.synapsense.dto.CollectionTO;
import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.IllegalInputParameterException;
import com.synapsense.exception.ObjectExistsException;
import com.synapsense.impl.reporting.export.ExportFormat;
import com.synapsense.impl.reporting.model.JSReport;
import com.synapsense.impl.reporting.model.Report;
import com.synapsense.impl.reporting.utils.FileUtils;
import com.synapsense.impl.reporting.utils.ReportingConstants;
import com.synapsense.service.Environment;
import com.synapsense.service.FileWriter;
import com.synapsense.service.reporting.dto.ReportInfo;
import com.synapsense.service.reporting.dto.ReportTaskTitle;
import com.synapsense.service.reporting.dto.ReportTitle;
import com.synapsense.service.reporting.exception.EnvDAOReportingException;
import com.synapsense.service.reporting.exception.ExportingReportingException;
import com.synapsense.service.reporting.exception.ObjectNotFoundDAOReportingException;
import com.synapsense.util.CollectionUtils;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.xml.JRPrintXmlLoader;

import org.apache.commons.digester.SimpleRegexMatcher;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DbReportDAO extends AbstractReportingDAO implements ReportDAO {

	private static final long serialVersionUID = -2365964169470927642L;

	private static final Log log = LogFactory.getLog(DbReportDAO.class);

	private File fileStorage;
	
	protected FileWriter fileWriter;
	
	private static final String timestampFormat = "yyyy-MM-dd-HH_mm_ss";

	public DbReportDAO(File fileStorage, Environment environment, FileWriter fileWriter) {
		super(environment);
		this.fileStorage = fileStorage;
		this.fileWriter = fileWriter;
	}

	@Override
	public void saveReport(ReportInfo reportInfo, Report report, String templateName) throws ObjectExistsException,
	        ObjectNotFoundDAOReportingException {
		if (reportInfo == null) {
			throw new IllegalInputParameterException("Report info shouldn't be null");
		}

		if (report == null) {
			throw new IllegalInputParameterException("Report object shouldn't be null");
		}

		long timestamp = new Date().getTime();
		// Create new report object
		TO<?> newReport = createObject(ReportingConstants.TYPE.REPORT,
				new ValueTO[] {
						new ValueTO(ReportingConstants.FIELD.REPORT.TASK_NAME,
								reportInfo.getTaskName()),
						new ValueTO(ReportingConstants.FIELD.REPORT.TIMESTAMP,
								timestamp),
						new ValueTO(ReportingConstants.FIELD.REPORT.TASK_ID,
								reportInfo.getReportTaskId()),
						new ValueTO(ReportingConstants.FIELD.REPORT.USER_NAME,
								reportInfo.getUser()) });

		// Set report id to dto
		int reportId = (Integer) newReport.getID();
		reportInfo.setReportId(reportId);
		
		String absolutePath = fileStorage.getAbsolutePath();
		String taskName = reportInfo.getTaskName();
		String exportPath = getExportFolder(reportId, templateName);
		if (!FileUtils.checkOrCreateDirectory(absolutePath + "\\" + exportPath)) {
			log.error("Error creating export folder " + absolutePath + exportPath);
			throw new ResourceDAOReportingException("Error creating export folder");
		}
		String fileName = "\\" + getValidTaskName(taskName) + "_" + this.getFormattedTimestamp(timestamp) + "." + ExportFormat.XML.getFileExtension(); 
		String relativePath = exportPath + fileName;
		
		this.setPropertyValue(newReport, ReportingConstants.FIELD.REPORT.DATA_PATH, relativePath);
		try {
			report.export(ExportFormat.XML, relativePath, fileWriter);
		} catch (ExportingReportingException e) {
			log.error(e.getLocalizedMessage(), e);
			throw new ResourceDAOReportingException("Error exporting report to format " + ExportFormat.XML.toString(), e);
		}
	}

	@Override
	public void removeReport(int reportId) throws ObjectNotFoundDAOReportingException {

		if (log.isDebugEnabled()) {
			log.debug("Removing report " + reportId);
		}
		String exportPath = getExportFolder(reportId, null);
		String objectType = ReportingConstants.TYPE.REPORT;
		TO<?> report = getObjectById(objectType, reportId);

		//Remove Exported Reports
		Collection<TO<?>> exportedReports = this.getObjectsByProperty(
				this.getObjectsByType(ReportingConstants.TYPE.EXPORTED_REPORT)
				, ReportingConstants.FIELD.EXPORTED_REPORT.REPORT_ID, reportId);
		for (TO<?> exportedReport : exportedReports) {
			try {
				removeObject(exportedReport);
			} catch (ObjectNotFoundDAOReportingException e) {
				log.error(e.getLocalizedMessage(), e);
				throw new ResourceDAOReportingException("Object " + exportedReport + " remove execution error",
						e);
			}
		}
		//Remove Report
		try {
			removeObject(report);
		} catch (ObjectNotFoundDAOReportingException e) {
			log.error(e.getLocalizedMessage(), e);
			throw new ResourceDAOReportingException("Object with type " + objectType + " and objectId " + reportId
			        + " remove execution error", e);
		}

		if (!FileUtils.deleteDirectory(fileStorage.getAbsolutePath() + "\\" + exportPath)) {
			log.error("Error removing report " + reportId + " export directory");
			throw new ResourceDAOReportingException("Error removing report " + reportId + " export directory");
		}

	}

	@Override
	public Collection<ReportTitle> getGeneratedReports() throws ObjectNotFoundDAOReportingException {

		if (log.isDebugEnabled()) {
			log.debug("Retrieving all available generated reports");
		}
		//Get all available reports
		Collection<TO<?>> allReports = getObjectsByType(ReportingConstants.TYPE.REPORT);
		Collection<ReportTitle> reports = CollectionUtils.newList();
		for (TO<?> report : allReports) {
			Map<String, ValueTO> reportProperties;
			try {
				reportProperties = getPropertyValues(report, new String[] { ReportingConstants.FIELD.REPORT.TIMESTAMP
						, ReportingConstants.FIELD.REPORT.TASK_ID
						, ReportingConstants.FIELD.REPORT.DATA_PATH});
			} catch (EnvDAOReportingException e) {
				throw new ResourceDAOReportingException("Error retrieving report properties for report " + report);
			}
			String dataPath = (String) reportProperties.get(ReportingConstants.FIELD.REPORT.DATA_PATH).getValue();
			String taskName = getTaskNameFromReport(report, dataPath);
			ReportTitle reportTitle = new ReportTitle(taskName, (long) reportProperties.get(
			        ReportingConstants.FIELD.REPORT.TIMESTAMP).getValue());
			int reportId = (Integer) report.getID();
			reportTitle.setReportId(reportId);

			reports.add(reportTitle);
		}
		return reports;

	}

	@Override
	public ReportInfo getReportInfo(int reportId) throws ObjectNotFoundDAOReportingException {

		if (log.isDebugEnabled()) {
			log.debug("Retreiving information for report " + reportId);
		}

		if (log.isDebugEnabled()) {
			log.debug("retrieving information for report " + reportId);
		}

		TO<?> report = this.getObjectById(ReportingConstants.TYPE.REPORT, reportId);
		if (report == null) {
			throw new ObjectNotFoundDAOReportingException("Report with id " + reportId + " not found");
		}
		Map<String, ValueTO> reportProperties;
		try {
			reportProperties = getPropertyValues(report, new String[] {
					ReportingConstants.FIELD.REPORT.TASK_ID,
					ReportingConstants.FIELD.REPORT.TIMESTAMP,
					ReportingConstants.FIELD.REPORT.USER_NAME,
					ReportingConstants.FIELD.REPORT.DATA_PATH });
		} catch (EnvDAOReportingException e) {
			throw new ResourceDAOReportingException("Error retrieving properties for report with id " + reportId);
		}
		int reportTaskId = (int) reportProperties.get(ReportingConstants.FIELD.REPORT.TASK_ID).getValue();
		String dataPath = (String) reportProperties.get(ReportingConstants.FIELD.REPORT.DATA_PATH).getValue();
		String taskName = getTaskNameFromReport(report, dataPath);
		long reportTimestamp = (long) reportProperties.get(ReportingConstants.FIELD.REPORT.TIMESTAMP).getValue();

        String userName = (String) reportProperties.get(ReportingConstants.FIELD.REPORT.USER_NAME).getValue();

		ReportInfo reportInfo = new ReportInfo(taskName, reportTimestamp);
		reportInfo.setReportId((Integer) report.getID());
		reportInfo.setReportTaskId(reportTaskId);
        reportInfo.setUser(userName);

		// Fetch exported reports
		Collection<TO<?>> exportedReports = getObjectsByProperty(
		        this.getObjectsByType(ReportingConstants.TYPE.EXPORTED_REPORT),
		        ReportingConstants.FIELD.EXPORTED_REPORT.REPORT_ID, reportId);

		if (exportedReports.isEmpty())
			return reportInfo;

		Collection<CollectionTO> allProperties = getPropertyValue(exportedReports,
		        new String[] { ReportingConstants.FIELD.EXPORTED_REPORT.FORMAT,
		                ReportingConstants.FIELD.EXPORTED_REPORT.EXPORTED_PATH });

		for (CollectionTO exportedReportProperties : allProperties) {
			String typeStr = (String) exportedReportProperties.getSinglePropValue(
			        ReportingConstants.FIELD.EXPORTED_REPORT.FORMAT).getValue();
			String path = (String) exportedReportProperties.getSinglePropValue(
			        ReportingConstants.FIELD.EXPORTED_REPORT.EXPORTED_PATH).getValue();
			ExportFormat format = ExportFormat.load(typeStr);
			if (format == null) {
				throw new ResourceDAOReportingException("Error loading exported report format " + typeStr);
			}
			reportInfo.addExportedPath(typeStr, path);
		}
		return reportInfo;

	}

	@Override
	public Report getReportObject(int reportId) throws ObjectNotFoundDAOReportingException {

		if (log.isDebugEnabled()) {
			log.debug("Retreiving report object for report " + reportId);
		}

		JasperPrint data;
		String path;
		try {
			TO<?> report = getObjectById(ReportingConstants.TYPE.REPORT, reportId);
			if (report == null) {
				throw new ObjectNotFoundDAOReportingException("Report with id " + reportId + " not found");
			}
			path = (String) getPropertyValue(report, ReportingConstants.FIELD.REPORT.DATA_PATH, String.class);
			byte[] jasperPrintFile = readFileBytes(path);
			try {
				data = JRPrintXmlLoader.load(new ByteArrayInputStream(jasperPrintFile));
			} catch (JRException e) {
				throw new ResourceDAOReportingException("Report object can't be loaded");
			}
			if (!(data instanceof JasperPrint)) {
				throw new ResourceDAOReportingException("Unrecognized object stored for report " + reportId);
			}
		} catch (EnvDAOReportingException e) {
			throw new ResourceDAOReportingException("Error retrieving report data for report by id " + reportId);
		}
		return new JSReport(data);

	}

	@Override
	public String exportReport(int reportId, ExportFormat format) throws ObjectNotFoundDAOReportingException {

		if (log.isDebugEnabled()) {
			log.debug("Exporting report " + reportId + " to " + format + " format ");
		}

		String exportPath = getExportFolder(reportId, null);
		String absolutePath = fileStorage.getAbsolutePath();
		if (!FileUtils.checkOrCreateDirectory(absolutePath + "\\" + exportPath)) {
			log.error("Error creating export folder " + absolutePath + exportPath);
			throw new ResourceDAOReportingException("Error creating export folder");
		}
		Map<String, ValueTO> reportPropertyMap = getReportFileNameMap(reportId);
		TO<?> report = this.getObjectById(ReportingConstants.TYPE.REPORT, reportId);
		String dataPath;
		try {
			dataPath = (String)this.getPropertyValue(report, ReportingConstants.FIELD.REPORT.DATA_PATH, String.class);
		} catch (EnvDAOReportingException e) {
			throw new ResourceDAOReportingException("Error retrieving report " + ReportingConstants.FIELD.REPORT.DATA_PATH + " value for " + report);
		}
		
		String taskName = getTaskNameFromReport(report, dataPath);
		long timestamp = (long) reportPropertyMap.get(
				ReportingConstants.FIELD.REPORT.TIMESTAMP).getValue();		
		StringBuilder fileNameSB = new StringBuilder();
		fileNameSB.append("/")
			.append(getValidTaskName(taskName))
			.append("_")
			.append(getFormattedTimestamp(timestamp))
			.append(".")
			.append(format.getFileExtension());
		String fileName = fileNameSB.toString(); 
		String relativePath = exportPath + fileName;

		try {
			getReportObject(reportId).export(format, relativePath, fileWriter);
		} catch (ExportingReportingException e) {
			log.error(e.getLocalizedMessage(), e);
			throw new ResourceDAOReportingException("Error exporting report with id " + reportId + " to format "
			        + format.toString(), e);
		}

		// Creating Exported Report object
		createObject(ReportingConstants.TYPE.EXPORTED_REPORT, new ValueTO[] {
		        new ValueTO(ReportingConstants.FIELD.EXPORTED_REPORT.REPORT_ID, reportId),
		        new ValueTO(ReportingConstants.FIELD.EXPORTED_REPORT.FORMAT, format.save()),
		        new ValueTO(ReportingConstants.FIELD.EXPORTED_REPORT.EXPORTED_PATH, relativePath) });

		return relativePath;

	}

	@Override
	public String getExportRootPath() {
		return fileStorage.getAbsolutePath();
	}

	@Override
	public String getTemplateNameByReportId(int reportId) throws ObjectNotFoundDAOReportingException {
		TO<?> report = this.getObjectById(ReportingConstants.TYPE.REPORT, reportId);
		if (report == null) {
			throw new ObjectNotFoundDAOReportingException("Report with id " + reportId + " not found");
		}

		Map<String, ValueTO> reportPropertyMap = CollectionUtils.newMap();
		try {
			reportPropertyMap = this.getPropertyValues(report, new String[] {
			        ReportingConstants.FIELD.REPORT.TIMESTAMP, ReportingConstants.FIELD.REPORT.TASK_ID });
		} catch (EnvDAOReportingException e) {
			throw new ResourceDAOReportingException("Error retrieving properties for " + report.toString());
		}
		int taskId = (int) reportPropertyMap.get(ReportingConstants.FIELD.REPORT.TASK_ID)
		        .getValue();
		
		//Getting templateName
		TO<?> task = this.getObjectById(ReportingConstants.TYPE.REPORT_TASK, taskId);
		if (task == null) {
			throw new ObjectNotFoundDAOReportingException("Task with id " + taskId + " not found");
		}
		Map<String, ValueTO> taskPropertyMap = CollectionUtils.newMap();
		try {
			taskPropertyMap = this.getPropertyValues(task, new String[] {
					ReportingConstants.FIELD.REPORT_TASK.TEMPLATE_ID });
		} catch (EnvDAOReportingException e) {
			throw new ResourceDAOReportingException("Error retrieving properties for " + report.toString());
		}
		
		int templateId = (int) taskPropertyMap.get(ReportingConstants.FIELD.REPORT_TASK.TEMPLATE_ID)
		        .getValue();
		return getTemplateNameById(templateId);
	}
	
	private Collection<CollectionTO> getPropertyValue(Collection<TO<?>> objIds, String[] propNames) {

		Collection<CollectionTO> propertyValues = environment.getPropertyValue(objIds, propNames);
		if (propertyValues == null || propertyValues.isEmpty()) {
			throw new ResourceDAOReportingException("Error retrieving properties for " + objIds.toString());
		}
		return propertyValues;

	}

	private String getExportFolder(int reportId, String templateName) throws ObjectNotFoundDAOReportingException {

		TO<?> report = this.getObjectById(ReportingConstants.TYPE.REPORT, reportId);
		if (report == null) {
			throw new ObjectNotFoundDAOReportingException("Report with id " + reportId + " not found");
		}

		Map<String, ValueTO> reportPropertyMap = CollectionUtils.newMap();
		try {
			reportPropertyMap = this.getPropertyValues(report, new String[] {
			        ReportingConstants.FIELD.REPORT.TIMESTAMP
			        , ReportingConstants.FIELD.REPORT.TASK_ID
			        , ReportingConstants.FIELD.REPORT.DATA_PATH});
		} catch (EnvDAOReportingException e) {
			throw new ResourceDAOReportingException("Error retrieving properties for " + report.toString());
		}
		int taskId = (int) reportPropertyMap.get(ReportingConstants.FIELD.REPORT.TASK_ID)
		        .getValue();
		String taskName = getTaskNameById(taskId);
		
		//if task was removed for the report
		if (taskName == null) {
			String dataPath = (String) reportPropertyMap.get(ReportingConstants.FIELD.REPORT.DATA_PATH).getValue();
			if (templateName == null) {
				String[] fullFileName = new File(dataPath).getPath().split("\\\\");
				templateName = fullFileName[2];
			}
			
		} else if (templateName == null){
			//Getting templateName
			TO<?> task = this.getObjectById(ReportingConstants.TYPE.REPORT_TASK, taskId);
			if (task == null) {
				throw new ObjectNotFoundDAOReportingException("Task with id " + taskId + " not found");
			}
			Map<String, ValueTO> taskPropertyMap = CollectionUtils.newMap();
			try {
				taskPropertyMap = this.getPropertyValues(task, new String[] {
						ReportingConstants.FIELD.REPORT_TASK.TEMPLATE_ID });
			} catch (EnvDAOReportingException e) {
				throw new ResourceDAOReportingException("Error retrieving properties for " + report.toString());
			}
			
			int templateId = (int) taskPropertyMap.get(ReportingConstants.FIELD.REPORT_TASK.TEMPLATE_ID)
			        .getValue();
			templateName = getTemplateNameById(templateId);
		}
		String dataPath = (String) reportPropertyMap.get(ReportingConstants.FIELD.REPORT.DATA_PATH).getValue();
		taskName = getTaskNameFromReport(report, dataPath);
		
		long timestamp = (long) reportPropertyMap.get(ReportingConstants.FIELD.REPORT.TIMESTAMP).getValue();

		StringBuilder path = new StringBuilder("reporting\\export\\");
		path.append(templateName).append("\\");
		path.append(getValidTaskName(taskName)).append("\\");
		path.append(reportId).append("_").append(timestamp);
		return path.toString();

	}
	
	private String getTemplateNameById(int reportTemplateId)
			throws ObjectNotFoundDAOReportingException {

		TO<?> reportTemplateObject = getObjectById(
				ReportingConstants.TYPE.REPORT_TEMPLATE, reportTemplateId);
		if (reportTemplateObject == null) {
			throw new ObjectNotFoundDAOReportingException(
					"Report template with id " + reportTemplateId
							+ " not found");
		}
		String templateName;
		try {
			templateName = (String) getPropertyValue(reportTemplateObject,
					ReportingConstants.FIELD.REPORT_TEMPLATE.NAME, String.class);
		} catch (EnvDAOReportingException e) {
			throw new ResourceDAOReportingException(
					"Error retrieving name for report template "
							+ reportTemplateObject.toString());
		}
		return templateName;

	}

	private String getTaskNameById(int reportTaskId) throws ObjectNotFoundDAOReportingException {

		TO<?> reportTask = getObjectById(ReportingConstants.TYPE.REPORT_TASK, reportTaskId);
		if (reportTask == null) {
			return null;
		}
		String taskName;
		try {
			taskName = (String) getPropertyValue(reportTask, ReportingConstants.FIELD.REPORT_TASK.NAME,
			        String.class);
		} catch (EnvDAOReportingException e) {
			throw new ResourceDAOReportingException("Error retrieving name for reporting task " + reportTask.toString());
		}
		return taskName;

	}

	private byte[] readFileBytes(String fileName) {
		try {
			return fileWriter.readFileBytes(fileName);
		} catch (IOException e) {
			throw new ResourceDAOReportingException("Failed to read file bytes" + fileName, e);
		}
	}
	
	private String getFormattedTimestamp(long timestamp) throws ObjectNotFoundDAOReportingException {
		
		DateFormat dateFormat = new SimpleDateFormat(timestampFormat);
		String formattedByUserPreference = dateFormat.format(new Date(timestamp));
		return formattedByUserPreference;
		
	}
	
	private Map<String, ValueTO> getReportFileNameMap(int reportId) throws ObjectNotFoundDAOReportingException {

		TO<?> report = this.getObjectById(ReportingConstants.TYPE.REPORT, reportId);
		if (report == null) {
			throw new ObjectNotFoundDAOReportingException("Report with id " + reportId + " not found");
		}

		Map<String, ValueTO> reportPropertyMap = CollectionUtils.newMap();
		try {
			reportPropertyMap = this.getPropertyValues(report, new String[] {
			        ReportingConstants.FIELD.REPORT.TIMESTAMP, ReportingConstants.FIELD.REPORT.TASK_ID });
		} catch (EnvDAOReportingException e) {
			throw new ResourceDAOReportingException("Error retrieving properties for " + report.toString());
		}
		return reportPropertyMap;

	}
	
	private String getValidTaskName(String taskName) {
		String fileNamePermittedTaskName = taskName.replaceAll(Pattern.quote("\\"), Matcher.quoteReplacement("_"));
		return fileNamePermittedTaskName.replaceAll("[/:*?\"<>|]","_");
	}
	
	private String getTaskNameFromReport(TO<?> report, String dataPath) throws ObjectNotFoundDAOReportingException {
		String taskName = null;
		try {
			taskName = (String)getPropertyValue(report, ReportingConstants.FIELD.REPORT.TASK_NAME, String.class);
		} catch (EnvDAOReportingException e) {
			//valid case if task was created before retrieving task name from report
		}
		if (taskName == null) {
			int reportTaskId;
			try {
				reportTaskId = (int) getPropertyValue(report, ReportingConstants.FIELD.REPORT.TASK_ID, Integer.class);
			} catch (EnvDAOReportingException e) {
				throw new ResourceDAOReportingException("Error retrieving report " + ReportingConstants.FIELD.REPORT.TASK_ID + " value for " + report);
			}
			taskName = this.getTaskNameById(reportTaskId);
		}
		if (taskName == null) {
			if (dataPath == null) {
				taskName = "";
			} else {
				String[] fileName = FileUtils.getSimpleName(new File(dataPath)).split("_");
				StringBuilder taskNameSB = new StringBuilder();
				for (int i = 0; i < fileName.length - 3; i++) {
					taskNameSB.append(fileName[i])
					.append("_");
				}
				taskName = taskNameSB.substring(0, taskNameSB.length() - 1);
			}
		}
		return taskName;
	}
	
}
