package com.synapsense.impl.transport.rmi;

import java.rmi.server.UID;
import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.Executor;

import org.apache.log4j.Logger;

import com.synapsense.service.nsvc.Event;
import com.synapsense.service.nsvc.EventProcessor;
import com.synapsense.transport.rmi.AsyncRmiNotificationClient;

/**
 * Class encapsulates logic for asynchronous event sending using a predefined
 * Executor as a thread provider.
 * 
 * @author Oleg Stepanov
 * 
 */
public class EventChannel implements Runnable, EventProcessor {

	/**
	 * Listener for channel's internal events
	 * 
	 * @author Oleg Stepanov
	 * 
	 */
	public static interface ChannelListener {

		void channelFailed(EventChannel eventChannel);

	}

	private static final Logger log = Logger.getLogger(EventChannel.class);

	private final AsyncRmiNotificationClient client;
	private final UID uid;
	private final Executor executor;
	private final EventChannelFailStrategy failStrategy;

	private final Queue<Event> events;

	private ChannelListener channelListener;

	// marks channel as failed to prevent execution scheduling
	private boolean failed;

	// some channel's statistic
	private long eventsSent;
	private long lastSendFailNum;
	private long lastSendTotalTime;
	private Throwable lastError;

	public EventChannel(AsyncRmiNotificationClient client, UID uid, Executor executor,
	        EventChannelFailStrategy failStrategy) {
		this.client = client;
		this.uid = uid;
		this.executor = executor;
		this.failStrategy = failStrategy;
		events = new LinkedList<Event>();
	}

	@Override
	public Event process(Event e) {
		if (failed) {
			log.warn("Event inserted into an already failed channel for client " + getClientAsString());
			return null;
		}
		// 1. Add event into the channel's events queue
		synchronized (events) {
			events.add(e);
			if (events.size() == 1) {
				// 2. Schedule the channel for sending
				executor.execute(this);
			}
		}
		return null;
	}

	@Override
	public void run() {
		if (failed) {
			log.warn("Failed channel scheduled for execution, client " + getClientAsString());
			return;
		}

		Event e;
		synchronized (events) {
			e = events.peek();
		}
		if (e == null) {
			log.warn("Empty event queue scheduled for sending");
			return;
		}

		long sendStart = System.currentTimeMillis();
		try {
			if (log.isDebugEnabled())
				log.debug("Sending " + e + " to " + getClientAsString());
			// try to send the event from the queue
			boolean active = client.notify(e, uid);
			lastSendTotalTime = System.currentTimeMillis() - sendStart;

			if (log.isDebugEnabled()) {
				log.debug("It took " + lastSendTotalTime + " ms" + " for sending " + e + " to " + getClientAsString());
			}

			if (!active) {
				// for some reason client has removed this processor
				// but failed to notify us. let's remove the channel
				if (channelListener != null)
					channelListener.channelFailed(this);
			}
			// update statistic
			eventsSent++;
			lastSendFailNum = 0;
			lastError = null;

			// remove event from the queue
			synchronized (events) {
				events.poll();
				if (events.size() != 0) {
					executor.execute(this);
				}
			}
		} catch (Throwable t) {
			// update statistic
			lastSendTotalTime += System.currentTimeMillis() - sendStart;
			lastSendFailNum++;
			lastError = t;
			log.warn("Cannot deliver event " + e + " to " + client, t);
			// Consult the EventChannelFailStrategy if channel
			// should try to redeliver the event
			if (failed = failStrategy.isFailed(this)) {
				log.warn("Channel fail for client " + client + ", UID =" + uid);
				if (channelListener != null)
					channelListener.channelFailed(this);
			} else {
				if (log.isDebugEnabled())
					log.debug("Rescheduling event " + e + " sending for client " + getClientAsString());
				executor.execute(this);
			}
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((uid == null) ? 0 : uid.hashCode());
		return result;
	}

	public ChannelListener getChannelListener() {
		return channelListener;
	}

	public void setChannelListener(ChannelListener channelListener) {
		this.channelListener = channelListener;
	}

	public UID getClientId() {
		return uid;
	}

	public AsyncRmiNotificationClient getClient() {
		return client;
	}

	public String getClientAsString() {
		return "proxy=" + client.toString() + ", UID=" + uid;
	}

	/**
	 * @return Current size of this channel's events queue
	 */
	public int getEventsQueueSize() {
		synchronized (events) {
			return events.size();
		}
	}

	/**
	 * @return Total number of events sent through this channel
	 */
	public long getEventsSent() {
		return eventsSent;
	}

	/**
	 * @return Number of fails to send the last event. Zero if the last event
	 *         was sent on the first attempt.
	 */
	public long getLastSendFailNum() {
		return lastSendFailNum;
	}

	/**
	 * @return Total time in milliseconds it took to send the last event
	 *         (including retries if any occurred).
	 */
	public long getLastSendTotalTime() {
		return lastSendTotalTime;
	}

	/**
	 * @return Error occurred during the last send attempt, if any.
	 */
	public Throwable getLastError() {
		return lastError;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EventChannel other = (EventChannel) obj;
		if (uid == null) {
			if (other.uid != null)
				return false;
		} else if (!uid.equals(other.uid))
			return false;
		return true;
	}

}