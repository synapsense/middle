package com.synapsense.impl.transport;

import javax.ejb.Remote;

@Remote
public interface TransportManagerMngmt {

	void create() throws Exception;

	void start() throws Exception;

	void stop() throws Exception;

	void destroy() throws Exception;

}
