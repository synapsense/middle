package com.synapsense.impl.reporting.queries;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import com.synapsense.exception.IllegalInputParameterException;
import com.synapsense.impl.reporting.queries.TableProcessor.QueryClause;
import com.synapsense.impl.reporting.utils.ReportingConstants.QUERY.SYMBOL;
import com.synapsense.service.reporting.DimensionalQuery;
import com.synapsense.service.reporting.Formula;
import com.synapsense.service.reporting.Formula.TransformType;
import com.synapsense.service.reporting.olap.AxisSet;
import com.synapsense.service.reporting.olap.Hierarchy;
import com.synapsense.service.reporting.olap.custom.Cube;
import com.synapsense.util.CollectionUtils;

public class OrderByClauseBuilder implements ClauseBuilder {

	private static final Logger log = Logger.getLogger(OrderByClauseBuilder.class);
	
	private Map<String, String> hierarchyGroupingItemsMap = CollectionUtils.newMap();
	
	public OrderByClauseBuilder(GroupByClauseBuilder groupByClauseBuilder) {
		hierarchyGroupingItemsMap.put(Cube.DIM_TIME, groupByClauseBuilder.getTimeGroupingItems());
		hierarchyGroupingItemsMap.put(Cube.DIM_PROPERTY, groupByClauseBuilder.getValueIdsGroupingItems());
		hierarchyGroupingItemsMap.put(Cube.DIM_OBJECT, groupByClauseBuilder.getValueIdsGroupingItems());
	}

	@Override
	public String build(TableProcessor owner) {
		
		if (owner == null) {
			throw new IllegalInputParameterException("TableProcessor shouldn't be null");
		}
		StringBuilder orderBySB = new StringBuilder();
		orderBySB.append(QueryClause.ORDER_BY.getName())
			.append(SYMBOL.SPACE);
		DimensionalQuery query = owner.getQuery();
		AxisSet rows = query.getAxisSet(0);
		List<Hierarchy> hierarchies = rows.getDimensionalities();
		//Order By depends on Rows hierarchies
		Set<String> groupingItems = CollectionUtils.newLinkedSet();
		String timeGroupingItem = hierarchyGroupingItemsMap.get(Cube.DIM_TIME);
		if (!timeGroupingItem.isEmpty()) {
			groupingItems.add(timeGroupingItem);
		}
				
		for (Hierarchy hierarchy : hierarchies) {
			String groupingItem = hierarchyGroupingItemsMap.get(Cube.getDimension(hierarchy.getName()));
			if (!groupingItem.isEmpty()) {
				groupingItems.add(groupingItem);
			}
		}
		//Formula grouping item
		Formula formula = owner.getQuery().getFormula();
		if (formula.getTransformTypes().contains(TransformType.COLUMN_10)) {
			groupingItems.add(formula.getOrderByClausePart());
		}
		for (String groupingItem : groupingItems) {
			if (!groupingItem.isEmpty()) {
				orderBySB.append(groupingItem)
						.append(SYMBOL.COMMA)
						.append(SYMBOL.SPACE);
			}
		}

		if (log.isTraceEnabled()) {
			log.trace("ORDER BY clause: " + orderBySB.substring(0, orderBySB.length() - 2));
		}		
		return groupingItems.isEmpty() ? "" : orderBySB.substring(0, orderBySB.length() - 2);
		
	}
}
