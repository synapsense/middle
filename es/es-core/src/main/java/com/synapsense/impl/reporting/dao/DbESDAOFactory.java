package com.synapsense.impl.reporting.dao;

import java.io.File;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.impl.reporting.utils.FileUtils;
import com.synapsense.service.Environment;
import com.synapsense.service.FileWriter;
import com.synapsense.service.reporting.exception.DAOReportingException;

public class DbESDAOFactory implements ReportingDAOFactory {

	private static final Log log = LogFactory.getLog(DbESDAOFactory.class);


	private File fileStorage;

	private Environment environment;

	private FileWriter fileWriter;

	public DbESDAOFactory(String fileStoragePath, Environment environment, FileWriter fileWriter) throws DAOReportingException {
		if (log.isDebugEnabled()) {
			log.debug("Creating DB DAO factory");
		}
		this.environment = environment;
		this.fileWriter = fileWriter;
		this.fileStorage = new File(fileStoragePath);
		if (!FileUtils.checkOrCreateDirectory(fileStorage)) {
			throw new DAOReportingException("Unable to resolve storage patth " + fileStoragePath);
		}
	}
	
	@Override
	public ReportTemplateDAO newReportTemplateDAO() {
		return new DbReportTemplateDAO(fileStorage, environment);
	}

	@Override
	public ReportDAO newReportDao() {
		return new DbReportDAO(fileStorage, environment, fileWriter);
	}

    @Override
    public ReportTaskDAO newReportTaskDao() {
        return new DbReportTaskDAO(environment);
    }

    @Override
	public void shutdown() throws DAOReportingException {
		
	}

}
