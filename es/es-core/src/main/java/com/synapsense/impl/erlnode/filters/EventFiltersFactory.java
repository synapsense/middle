package com.synapsense.impl.erlnode.filters;

import java.util.HashMap;
import java.util.Map;

import com.ericsson.otp.erlang.OtpErlangObject;
import com.ericsson.otp.erlang.OtpErlangTuple;
import com.synapsense.service.nsvc.EventProcessor;


/**
 * Factory for creating Java filter from its Erlang description
 * (see esapi.hrl for the definitions).
 * Internally represents a simple top-down parser.
 * @author Oleg Stepanov
 *
 */
public class EventFiltersFactory {
	
	private static Map<String, FilterDefParser> parsers;
	
	static {
		parsers = new HashMap<String, FilterDefParser>();
		//in our grammar the top level filters can be the following:
		//1. Simple filters (all will be parsed by SimpleFilterParser
		// which wraps any into DispatchingProcessor):
		parsers.put("object_created_event_filter", 
				new SimpleFilterParser());
		parsers.put("object_deleted_event_filter", 
				new SimpleFilterParser());
		parsers.put("properties_changed_event_filter", 
				new SimpleFilterParser());
		parsers.put("relation_set_event_filter", 
				new SimpleFilterParser());
		parsers.put("relation_removed_event_filter", 
				new SimpleFilterParser());
		//2. dispatching_filter
		parsers.put("dispatching_filter", 
				new DispatchingFilterParser());
		//3. Logical filters
		parsers.put("and_filter", 
				new AndFilterParser());
		parsers.put("or_filter", 
				new OrFilterParser());
		parsers.put("not_filter", 
				new NotFilterParser());

		parsers.put("event_type_filter",
				new EventTypeFilterParser());
	}

	public static EventProcessor createFilter(OtpErlangObject filterDef)
			throws IllegalArgumentException {
		try {
			return filterDef == null? null :createFilter((OtpErlangTuple)filterDef);
		} catch(ClassCastException e) {
			throw new IllegalArgumentException("Filter definition must be a tuple");
		}
	}

	public static EventProcessor createFilter(OtpErlangTuple filterDef)
			throws IllegalArgumentException {
		
		if(filterDef==null)
			return null;
		
		if(filterDef.arity()==0)
			throw new IllegalArgumentException(
				"Filter definition must contain at least an atom");
		
		String filterName = filterDef.elementAt(0).toString();
		FilterDefParser parser = parsers.get(filterName);
		if(parser==null)
			throw new IllegalArgumentException("Unknown filter definition "+filterName);
		
		return parser.parse(filterDef);
	}
}
