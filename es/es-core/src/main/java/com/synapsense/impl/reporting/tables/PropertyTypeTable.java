package com.synapsense.impl.reporting.tables;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.synapsense.impl.reporting.queries.AxisProcessor.ReportingTypeTable;
import com.synapsense.service.reporting.olap.Member;
import com.synapsense.service.reporting.olap.Tuple;
import com.synapsense.service.reporting.olap.TupleSet;
import com.synapsense.service.reporting.olap.custom.BaseLevel;
import com.synapsense.service.reporting.olap.custom.Cube;
import com.synapsense.service.reporting.olap.custom.Level;
import com.synapsense.util.CollectionUtils;

/**
 * The <code>PropertyTypeTable</code> class contains Property Types Map
 * 
 * @author Grigory Ryabov
 */
public class PropertyTypeTable implements ReportingTypeTable{

	private static final long serialVersionUID = 6891229804215248930L;

	Map<String, List<Tuple>> propertyObjectTypesTable;
	
	public PropertyTypeTable() {
		propertyObjectTypesTable = new LinkedHashMap<String, List<Tuple>>();
	}	

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, TupleSet> get() {
		
		Map<String, TupleSet> result = CollectionUtils.newMap();
		for (Entry<String, List<Tuple>> entry : propertyObjectTypesTable.entrySet()){
			List<Tuple> values = entry.getValue();
			TupleSet set = new TupleSet(values.toArray(new Tuple[values.size()]));
			result.put(entry.getKey(), set);
		}
		return result;
		
	}

	@Override
	public void add(Member object) {
		
		String [] keys= ((String)object.getKey()).split("\\.");
		String propertyObjectType = keys[0];
		if (propertyObjectTypesTable.containsKey(propertyObjectType)){
			propertyObjectTypesTable.get(propertyObjectType).add(object);
		} else {
			List<Tuple> properties = CollectionUtils.newList();
			properties.add(object);
			propertyObjectTypesTable.put(propertyObjectType, properties);
		}
		
	}

	@Override
	public int size() {
		return propertyObjectTypesTable.size();
	}

	@Override
	public boolean isEmpty() {
		return size() == 0 ? true : false;
	}

	@Override
	public PropertyTypeTable getTable() {
		
		PropertyTypeTable copy = new PropertyTypeTable();
		for (Entry<String, List<Tuple>> entry : propertyObjectTypesTable.entrySet()) {
			copy.put(entry.getKey(), entry.getValue());
		}
		return copy;
		
	}
	
	private void put(String key, List<Tuple> value) {
		propertyObjectTypesTable.put(key, value);
	}

	@Override
	public Level getLowestLevel() {
		return new BaseLevel(Cube.PROPERTY, Cube.LEVEL_PROPERTY);
	}

}
