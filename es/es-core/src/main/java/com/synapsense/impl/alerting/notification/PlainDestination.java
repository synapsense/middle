package com.synapsense.impl.alerting.notification;

import com.synapsense.dto.MessageType;
import com.synapsense.service.user.NonUserContext;
import com.synapsense.service.user.UserContext;

class PlainDestination implements Destination {

    private static final long serialVersionUID = -5511945971807137086L;
    
	private final String dest;

	public PlainDestination(final String dest) {
		this.dest = dest;
	}

	@Override
	public String getAddress(final MessageType mType) {
        // TODO force to check message type
		return dest;
	}

	@Override
	public UserContext getUserContext() {
		return new NonUserContext();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dest == null) ? 0 : dest.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final PlainDestination other = (PlainDestination) obj;
		if (dest == null) {
			if (other.dest != null)
				return false;
		} else if (!dest.equals(other.dest))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Simple destination [destination=" + dest + "]";
	}

}