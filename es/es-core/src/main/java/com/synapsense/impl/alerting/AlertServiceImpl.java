package com.synapsense.impl.alerting;

import com.synapsense.cdi.MBean;
import com.synapsense.commandprocessor.CommandEngineFactory;
import com.synapsense.commandprocessor.Commands;
import com.synapsense.commandprocessor.api.Command;
import com.synapsense.commandprocessor.api.CommandEngine;
import com.synapsense.dal.generic.utils.Util;
import com.synapsense.dal.objectcache.CacheEnvironmentDispatcher;
import com.synapsense.dal.objectcache.CacheSvc;
import com.synapsense.dal.objectcache.CacheSvcImpl;
import com.synapsense.dto.*;
import com.synapsense.exception.*;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.impl.activitylog.ActivityLogger;
import com.synapsense.impl.alerting.command.*;
import com.synapsense.impl.alerting.mapping.*;
import com.synapsense.search.Direction;
import com.synapsense.search.Path;
import com.synapsense.search.QueryDescription;
import com.synapsense.search.Relations;
import com.synapsense.service.AlertService;
import com.synapsense.service.Environment;
import com.synapsense.service.UserManagementService;
import com.synapsense.service.impl.activitylog.ActivityLogService;
import com.synapsense.util.CollectionUtils;
import com.synapsense.util.CollectionUtils.Functor;
import com.synapsense.util.CollectionUtils.Predicate;
import com.synapsense.util.LocalizationUtils;
import com.synapsense.util.Pair;
import org.apache.log4j.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.*;
import javax.interceptor.Interceptors;
import java.lang.Thread.UncaughtExceptionHandler;
import java.security.Principal;
import java.util.*;
import java.util.concurrent.*;

import static com.synapsense.impl.alerting.AlertConstants.TYPE.*;

/**
 * <code>AlertingService</code> implementor. Provides alert events processing
 * behavior. Dispatches all incoming alert events to actions queue for further
 * executing.
 * 
 * @author shabanov
 * 
 */
@Startup
@Singleton(name = "AlertService")
@Remote(AlertService.class)
@DependsOn("SynapCache")
@ConcurrencyManagement(ConcurrencyManagementType.BEAN)
@MBean("com.synapsense:type=AlertService")
public class AlertServiceImpl implements AlertService, AlertServiceImplMBean {

	private final static Logger logger = Logger.getLogger(AlertServiceImpl.class);

	/**
	 * Unexpected null input parameter message
	 */
	private static final String NULL_INPUT_PARAMETER_MESSAGE = "Supplied argument {%s} cannot be null or empty";

	@Resource
	/** EJB session context */
	private SessionContext ctx;

	/** EJB */
	private Environment env;

	/** EJB */
	private UserManagementService userService;

	private AlertDismissor alertDismissor;

	/**
	 * performance cache
	 */
	private AlertStore alertStore;

	private ActivityLogService activityLogService;

	private CommandEngine alertsEngine;

	/**
	 * internal scheduler
	 */
	private ExecutorService alertsEngineExecutor;

	private volatile TO<?> alertHistoryHolder;

	@Override
	@PostConstruct
	public void create() throws Exception {
		logger.info("Creating alert service ...");
		start();
	}

	@Override
	public void destroy() throws Exception {
		logger.info("Destroying alert service ...");
		logger.info("Destroyed alert service");
	}

	@Override
	public void start() throws Exception {
		logger.info("Starting alert service...");

		CacheSvc cacheSvc = CacheSvcImpl.getInstance();

		env = CacheEnvironmentDispatcher.getEnvironment(cacheSvc, env);

		alertHistoryHolder = createAlertHistoryHolder();

		alertStore = new AlertStore(env);

		// do this to support interceptors for local client calls
		alertDismissor = new AlertDismissorImpl(ctx.getBusinessObject(AlertService.class));

		alertsEngine = CommandEngineFactory.newEngineSharedBlockingQueue("ALERT_LIFECYCLE_PROCESSOR",
		        new LinkedBlockingQueue<Command>());

		// alerts will be handled in sequential mode
		alertsEngineExecutor = Executors.newSingleThreadExecutor(new ThreadFactory() {
			@Override
			public Thread newThread(Runnable runnable) {
				Thread t = new Thread(runnable, alertsEngine.identify());
				t.setUncaughtExceptionHandler(new UncaughtExceptionHandler() {
					@Override
					public void uncaughtException(Thread thread, Throwable e) {
						logger.error("Failed " + thread.getName() + " with uncaught exception", e);
					}
				});

				return t;
			}
		});

		alertsEngineExecutor.execute(alertsEngine);

		logger.info("Started alert service");
	}

	@Override
	@PreDestroy
	public void stop() throws Exception {
		logger.info("Stopping alert service...");

		alertsEngine.shutdown();

		// stopping queue
		// safe shutdown
		alertsEngineExecutor.shutdownNow();
		// then wait all tasks normal completion
		while (!alertsEngineExecutor.isTerminated()) {
			if (logger.isDebugEnabled()) {
				logger.debug("Awaiting termination of working thread...");
			}
			alertsEngineExecutor.awaitTermination(5, TimeUnit.SECONDS);
		}

		// alert storage clean
		alertStore.clear();
		logger.info("Stopped alert service");
		destroy();
	}

	@Override
	@Interceptors({ DisabledObjectsAlertInterceptor.class, AlertContentIncomingInterceptor.class })
	public void raiseAlert(final Alert alert) {

		if (alert == null) {
			throw new IllegalInputParameterException("Supplied 'alert' is null");
		}

		RaiseAlert raiseAlert = new RaiseAlert(env, this, alertStore, alertDismissor, activityLogService, alert,
		        System.currentTimeMillis(), new AlertToProperties(env));

		// add a bit of concurrency
		if (raiseAlert.isRaised()) { // help gc
			raiseAlert.scheduleAutoDismiss();
			return;
		}

		alertsEngine.addCommand(raiseAlert);
	}

	@Override
	@Interceptors(ActivityLogger.class)
	public void resolveAlert(final Integer alertId, final String comment) {
		final TO<?> alert = Utils.loadAlertID(alertId);
		final TO<?> user = identifyServiceCaller();

		UpdateStatus update = new UpdateStatus(env, alert, AlertStatus.RESOLVED, comment, user,
		        System.currentTimeMillis());

		RecordAndDeleteAlert delete = new RecordAndDeleteAlert(env, alertStore, Arrays.<TO<?>> asList(alert),
		        alertHistoryHolder);

		Command resolve = Commands.chaining("resolveAlertChain", update, delete);

		alertsEngine.addCommand(resolve);
	}

	@Override
	@Interceptors(ActivityLogger.class)
	public void acknowledgeAlert(final Integer alertId, final String comment) {
		if (alertId == null) {
			throw new IllegalInputParameterException(String.format(NULL_INPUT_PARAMETER_MESSAGE, "alertId"));
		}
		if (alertId < 1) {
			throw new IllegalInputParameterException("Supplied 'alertId' is negative or zero");
		}

		final TO<?> alert = Utils.loadAlertID(alertId);
		final TO<?> user = identifyServiceCaller();

		Command acknowledge = new UpdateStatus(env, alert, AlertStatus.ACKNOWLEDGED, comment, user,
		        System.currentTimeMillis());
		alertsEngine.addCommand(acknowledge);
	}

	@Override
	@Interceptors(ActivityLogger.class)
	public void dismissAlert(final Integer alertId, final String comment) {
		if (alertId == null) {
			throw new IllegalInputParameterException(String.format(NULL_INPUT_PARAMETER_MESSAGE, "alertId"));
		}
		if (alertId < 1) {
			throw new IllegalInputParameterException("Supplied 'alertId' is negative or zero");
		}

		final Alert alertToDismiss = new Alert();
		alertToDismiss.setAlertId(alertId);

		dismissAlert(alertToDismiss, comment);
	}

	@Override
	@Interceptors(ActivityLogger.class)
	public void dismissAlert(final Alert alert, final String comment) {
		if (alert == null) {
			throw new IllegalInputParameterException(String.format(NULL_INPUT_PARAMETER_MESSAGE, "alert"));
		}

		final TO<?> alertRef = Utils.loadAlertID(alert.getAlertId());

		dismissAlerts(Arrays.<TO<?>> asList(alertRef), comment);
	}

	@Override
	@Interceptors(ActivityLogger.class)
	public void dismissAlerts(final TO<?> objId, final boolean propagate, final String comment) {
		if (objId == null) {
			throw new IllegalInputParameterException(String.format(NULL_INPUT_PARAMETER_MESSAGE, "objId"));
		}

		Collection<Alert> activeAlerts = getActiveAlerts(objId, propagate);

		final Collection<TO<?>> alertsToDismiss = CollectionUtils.map(activeAlerts,
		        new CollectionUtils.Functor<TO<?>, Alert>() {
			        @Override
			        public TO<?> apply(Alert input) {
				        return Utils.loadAlertID(input.getAlertId());
			        }
		        });

		dismissAlerts(alertsToDismiss, comment);
	}

	@Override
	@Interceptors(AlertContentInterceptor.class)
	public Alert getAlert(Integer alertId) {
		if (alertId == null) {
			throw new IllegalInputParameterException(String.format(NULL_INPUT_PARAMETER_MESSAGE, "alertId"));
		}
		if (alertId < 1) {
			throw new IllegalInputParameterException("Supplied 'alertId' is negative or zero");
		}

		TO<?> alertRef = Utils.loadAlertID(alertId);
		if (!env.exists(alertRef)) {
			return null;
		}

		try {
			Collection<ValueTO> values = env.getAllPropertiesValues(alertRef);
			return CollectionUtils.map(values, Converters.propertiesToAlertConverter(alertRef, env));
		} catch (ObjectNotFoundException e) {
			return null;
		}

	}

	@Override
	@Interceptors(AlertContentInterceptor.class)
	public Collection<Alert> getActiveAlerts(final TO<?> objId, final boolean propagate) {
		if (objId == null) {
			throw new IllegalInputParameterException(String.format(NULL_INPUT_PARAMETER_MESSAGE, "objId"));
		}

		// fill the object ID list
		final Collection<TO<?>> objIds = CollectionUtils.newList();
		objIds.add(objId);
		return getActiveAlerts(objIds, propagate);
	}

	@Override
	@Interceptors(AlertContentInterceptor.class)
	public Collection<Alert> getActiveAlerts(final Collection<TO<?>> objIds, final boolean propagate) {
		if (objIds == null) {
			throw new IllegalInputParameterException(String.format(NULL_INPUT_PARAMETER_MESSAGE, "objIds"));
		}

		// retrieves all active alerts from env
		final Set<TO<?>> activeAlerts = CollectionUtils.newSet();
		activeAlerts.addAll(env.getObjectsByType(ALERT_TYPE_NAME));
		// if no active alerts
		if (activeAlerts.isEmpty()) {
			return Collections.emptyList();
		}

		Collection<TO<?>> allRelatedObjects = objIds;
		if (propagate) {
			allRelatedObjects = Util.getIDsWithPropagatingByHierarchy(env, objIds);
		}

		// alerts as collectionTO list
		final Collection<CollectionTO> alertsHosts = env.getAllPropertiesValues(activeAlerts);
		// alerts on hosts
		final Map<TO<?>, List<CollectionTO>> hostAlerts = CollectionUtils.newMap();
		// system alerts
		final Map<TO<?>, List<CollectionTO>> systemAlerts = CollectionUtils.newMap();

		for (final CollectionTO item : alertsHosts) {
			final List<ValueTO> values = item.getPropValue("hostTO");
			if (values == null || values.isEmpty()) {
				continue;
			}
			// checkout the host object
			final TO<?> host = (TO<?>) values.iterator().next().getValue();
			if (host == null || TOFactory.EMPTY_TO.equals(host)) {
				// system alert
				List<CollectionTO> sysAlertsList = systemAlerts.get(TOFactory.EMPTY_TO);
				if (sysAlertsList == null) {
					sysAlertsList = CollectionUtils.newList();
					systemAlerts.put(TOFactory.EMPTY_TO, sysAlertsList);
				}
				sysAlertsList.add(item);
			} else {
				// host alert
				List<CollectionTO> alertsList = hostAlerts.get(host);
				if (alertsList == null) {
					alertsList = CollectionUtils.newList();
					hostAlerts.put(host, alertsList);
				}
				alertsList.add(item);
			}
		}

		Collection<List<CollectionTO>> alerts = CollectionUtils.newList();
		// returns all: system and on host
		if (objIds.isEmpty()) {
			alerts.addAll(systemAlerts.values());
			alerts.addAll(hostAlerts.values());
		} else {
			// if on hosts were requested
			hostAlerts.keySet().retainAll(allRelatedObjects);
			if (objIds.contains(TOFactory.EMPTY_TO)) {
				alerts.addAll(systemAlerts.values());
			}
			alerts.addAll(hostAlerts.values());
		}

		Collection<Alert> result = CollectionUtils.newList();
		for (List<CollectionTO> alertsAsValues : alerts) {
			for (CollectionTO alert : alertsAsValues) {
				Alert alertDto = CollectionUtils.map(alert.getPropValues(),
				        Converters.propertiesToAlertConverter(alert.getObjId(), env));
				result.add(alertDto);
			}
		}

		return result;
	}

	@Override
	public int getNumActiveAlerts(final TO<?> objId, final boolean propagate) {
		if (objId == null) {
			throw new IllegalInputParameterException(String.format(NULL_INPUT_PARAMETER_MESSAGE, "objId"));
		}

		if (TOFactory.EMPTY_TO.equals(objId) || !propagate) {
			return getActiveAlerts(objId, propagate).size();
		} else {
			try {
				return env.getPropertyValue(objId, "numAlerts", Integer.class);
			} catch (ObjectNotFoundException e) {
				// just to be consistent with getActiveAlerts behavior
				return 0;
			} catch (PropertyNotFoundException e) {
				throw new AlertServiceSystemException(e.getMessage(), e);
			} catch (UnableToConvertPropertyException e) {
				throw new AlertServiceSystemException(e.getMessage(), e);
			}
		}
	}

	@Override
	public int getNumAlerts(AlertFilter filter) {
		if (filter.isHistorical()) {
			throw new IllegalInputParameterException("Supplied 'filter' cannot have historical condition enabled");
		}

		return getAlerts(filter).size();
	}

	@Override
	@Interceptors(AlertContentInterceptor.class)
	public Collection<Alert> getAlertsHistory(final TO<?> objId, final Date start, final Date end,
	        final boolean propagate) {
		if (objId == null) {
			throw new IllegalInputParameterException(String.format(NULL_INPUT_PARAMETER_MESSAGE, "objId"));
		}
		if (start == null) {
			throw new IllegalInputParameterException(String.format(NULL_INPUT_PARAMETER_MESSAGE, "start"));
		}
		if (end == null) {
			throw new IllegalInputParameterException(String.format(NULL_INPUT_PARAMETER_MESSAGE, "end"));
		}

		if (start.after(end)) {
			throw new IllegalInputParameterException("Invalid date interval specified");
		}

		final Collection<TO<?>> objIds = CollectionUtils.newList();
		objIds.add(objId);

		final AlertFilterBuilder afb = new AlertFilterBuilder().historical().onHosts(objIds).propagate(propagate)
		        .between(start, end);

		List<Alert> alerts = CollectionUtils.newList(getAlerts(afb.build()));

		Collections.sort(alerts, new Comparator<Alert>() {
			@Override
			public int compare(Alert o1, Alert o2) {
				if (o1.getAlertTime() > o2.getAlertTime()) {
					return -1;
				}
				if (o1.getAlertTime() < o2.getAlertTime()) {
					return 1;
				}
				return 0;
			}
		});

		return alerts;
	}

	@Override
	@Interceptors(AlertContentInterceptor.class)
	public Collection<Alert> getAlerts(final Integer[] alertIds) {
		if (alertIds == null)
			throw new IllegalInputParameterException("Supplied 'alertIds' is null");
		if (alertIds.length == 0)
			throw new IllegalInputParameterException("Supplied 'alertIds' is empty");

		for (final Integer id : alertIds) {
			if (id == null)
				throw new IllegalInputParameterException("Supplied 'alertIds' contains nulls");
			if (id <= 0)
				throw new IllegalInputParameterException("Supplied 'alertIds' contains negative or zero elements");
		}

		List<Alert> alerts = CollectionUtils.newArrayList(alertIds.length);
		for (Integer id : alertIds) {
			Alert alert = getAlert(id);
			if (alert != null) {
				alerts.add(alert);
			}
		}

		return alerts;
	}

	@Override
	public Collection<AlertType> getAlertTypes(final String[] typeNames) {
		if (typeNames == null || typeNames.length == 0) {
			throw new IllegalInputParameterException(String.format(NULL_INPUT_PARAMETER_MESSAGE, "typeNames"));
		}
		for (String typeName : typeNames) {
			if (typeName == null || typeName.length() == 0) {
				throw new IllegalInputParameterException(String.format(NULL_INPUT_PARAMETER_MESSAGE, "typeName"));
			}
		}

		Collection<TO<?>> existingAlertTypes = env.getObjectsByType(ALERT_DESCRIPTION_TYPE_NAME);
		Collection<CollectionTO> names = env.getPropertyValue(existingAlertTypes, new String[] { "name" });

		final Set<String> typeNamesSet = CollectionUtils.newSet(typeNames);

		Collection<CollectionTO> matchingTypes = CollectionUtils.searchAll(names,
		        new CollectionUtils.Predicate<CollectionTO>() {
			        @Override
			        public boolean evaluate(CollectionTO bundle) {
				        ValueTO name = bundle.getSinglePropValue("name");
				        if (name == null)
					        return false;
				        return typeNamesSet.contains(name.getValue());
			        }
		        });

		Collection<TO<?>> alertTypesIds = CollectionUtils.map(matchingTypes,
		        new CollectionUtils.Functor<TO<?>, CollectionTO>() {
			        @Override
			        public TO<?> apply(CollectionTO input) {
				        return input.getObjId();
			        }
		        });

		Collection<AlertType> result = CollectionUtils.newList();

		Functor<AlertType, Collection<ValueTO>> converter = Converters.propertiesToAlertTypeConverter(env);

		Collection<CollectionTO> alertTypesBundle = env.getAllPropertiesValues(alertTypesIds);
		for (CollectionTO alertType : alertTypesBundle) {
			result.add(converter.apply(alertType.getPropValues()));
		}

		return result;
	}

	@Override
	public Collection<AlertType> getAllAlertTypes() {
		Collection<TO<?>> alertTypesIds = env.getObjectsByType(ALERT_DESCRIPTION_TYPE_NAME);
		Collection<CollectionTO> alertTypeNames = env.getPropertyValue(alertTypesIds, new String[] { "name" });
		List<String> names = CollectionUtils.newArrayList(alertTypeNames.size());
		for (CollectionTO id : alertTypeNames) {
			names.add((String) id.getSinglePropValue("name").getValue());
		}
		return getAlertTypes(names.toArray(new String[] {}));
	}

	@Override
	@Interceptors(ActivityLogger.class)
	public void createAlertType(final AlertType alertType) throws AlertTypeAlreadyExistsException {
		if (alertType == null) {
			throw new IllegalInputParameterException(String.format(NULL_INPUT_PARAMETER_MESSAGE, "alertType"));
		}

		String alertTypeName = alertType.getName();
		final TO<?> alertTypeRef = Utils.findAlertTypeByName(env, alertTypeName);
		if (alertTypeRef != null) {
			throw new AlertTypeAlreadyExistsException("Duplicating alert type : " + alertTypeName);
			// logger.warn("Duplicating alert type : " + alertTypeName); return;
		}

		String priorityName = alertType.getPriority();
		final TO<?> priorityDO = Utils.findAlertPriority(env, priorityName);
		if (priorityDO == null) {
			throw new AlertServiceSystemException("Alert priority :" + priorityName + " does not exist");
			// logger.warn("Alert priority :" + priorityName +
			// " does not exist"); return;
		}

		List<ValueTO> values = new AlertTypeToProperties(env).apply(alertType);
		try {
			env.createObject(ALERT_DESCRIPTION_TYPE_NAME, values.toArray(new ValueTO[] {}));
		} catch (EnvException e) {
			throw new AlertServiceSystemException("Failed to create alert type", e);
			// logger.warn("Failed to create alert type", e);
		}
	}

	@Override
	@Interceptors(ActivityLogger.class)
	public void deleteAlertType(final String alertTypeName) {
		if (alertTypeName == null || alertTypeName.isEmpty()) {
			throw new IllegalInputParameterException(String.format(NULL_INPUT_PARAMETER_MESSAGE, "alertTypeName"));
		}

		final TO<?> alertType = Utils.findAlertTypeByName(env, alertTypeName);

		if (alertType == null) {
			if (logger.isDebugEnabled()) {
				logger.debug("No such alert type : " + alertTypeName + ". Nothing to delete");
			}
			return;
		}

		Collection<TO<?>> alerts = findAlertsBelongToType(alertType);
		if (!alerts.isEmpty()) {
			dismissAlertsSync(alerts, "Dismissed because alert definition was deleted");
		}

		try {
			// removes all destinations
			// TODO : consider more declarative way to describe reference
			// integrity (in cache?)
			for (TO<?> destId : findDestinationTOs(alertType)) {
				env.deleteObject(destId);
			}
			env.deleteObject(alertType);
		} catch (ObjectNotFoundException e) {
			logger.warn("Failed to delete alert type " + alertType, e);
		}
	}

	private Collection<TO<?>> findAlertsBelongToType(TO<?> id) {
		QueryDescription qd = new QueryDescription(1);
		qd.startQueryFrom(id);

		qd.addPath(1, new Path(Relations.named("type"), Direction.INCOMING));
		Collection<TO<?>> alerts = env.getObjects(qd);
		return alerts;
	}

	private Collection<TO<?>> findDestinationTOs(TO<?> alertType) {
		ValueTO[] criteria = new ValueTO[] { new ValueTO("alertType", alertType) };
		Collection<TO<?>> destinationRefs = env.getObjects(AlertConstants.TYPE.ALERT_DESTINATION_TYPE_NAME, criteria);
		if (destinationRefs.isEmpty()) {
			return Collections.emptyList();
		}

		return destinationRefs;
	}

	@Override
	@Interceptors(ActivityLogger.class)
	public void updateAlertType(final String alertTypeName, final AlertType alertType) throws NoSuchAlertTypeException {
		if (alertTypeName == null || alertTypeName.isEmpty()) {
			throw new IllegalInputParameterException(String.format(NULL_INPUT_PARAMETER_MESSAGE, "alertTypeName"));
		}

		if (alertType == null) {
			throw new IllegalInputParameterException(String.format(NULL_INPUT_PARAMETER_MESSAGE, "alertType"));
		}

		// Command updateAlertType = new UpdateAlertType(env, alertTypeName,
		// alertType, new AlertTypeToProperties(env));
		// alertsEngine.addCommand(updateAlertType);
		final TO<?> alertTypeId = Utils.findAlertTypeByName(env, alertTypeName);
		if (alertTypeId == null) {
			// ? exception handler policy required
			throw new NoSuchAlertTypeException(alertTypeName);
		}

		List<ValueTO> newValues = new AlertTypeToProperties(env).apply(alertType);

		// FIXME : cache level constraints required to preserve
		try {
			env.setAllPropertiesValues(alertTypeId, newValues.toArray(new ValueTO[] {}));
		} catch (EnvException e) {
			throw new AlertServiceSystemException("Failed to create alert type", e);
		}
	}

	@Override
	public void deleteAllAlerts() {
		// removing only from cache
		final Collection<TO<?>> allAlerts = env.getObjectsByType(ALERT_TYPE_NAME);

		final Command dismissNoHistoryRecord = getDismissOnlyAlertsCommand(allAlerts,
		        LocalizationUtils.getLocalizedString("alert_comment_all_alerts_deleted"));
		// hide previous history
		alertsEngine.addCommand(new Command() {
			@Override
			public void execute() {
				try {
					dismissNoHistoryRecord.execute();
					// no interleaving of delete and update on alert history
					// holder object
					env.deleteObject(alertHistoryHolder);
					alertDismissor.purgeAll();
				} catch (Exception firstTryEx) {
					if (logger.isDebugEnabled()) {
						logger.debug("Failed to delete alert history holder " + alertHistoryHolder, firstTryEx);
					}
					// FIXME : it is possible to get commit failed exception
					// consider to use some policy to remove objects
					// recall that there is async db batch updater working which
					// is root
					// cause for such failures
					if (logger.isDebugEnabled()) {
						logger.debug("Trying to delete alert history holder again...");
					}
					try {
						env.deleteObject(alertHistoryHolder);
						alertDismissor.purgeAll();
						if (logger.isDebugEnabled()) {
							logger.debug(alertHistoryHolder + " has been deleted successfully");
						}
					} catch (Exception retryEx) {
						throw new AlertServiceSystemException("Failed to delete alert history holder "
						        + alertHistoryHolder, retryEx);
					}
				}

				alertHistoryHolder = createAlertHistoryHolder();
			}
		});
	}

	@Override
	@Deprecated
	public void deleteAlerts(final TO<?> objId, final boolean propagate) {
		logger.warn("This operation does not make sense anymore. Use dismiss , resolve to delete alerts");
	}

	@Override
	@Deprecated
	public void deleteAlertsHistoryOlderThan(final Date date) {
		logger.warn("This operation does not make sense anymore. Specify exact time interval to filter unnecessary historical points");
	}

	@Override
	public boolean hasAlerts(final TO<?> objId, final boolean propagate) {
		if (objId == null) {
			throw new IllegalInputParameterException(String.format(NULL_INPUT_PARAMETER_MESSAGE, "objId"));
		}

		if (propagate) {
			try {
				return env.getPropertyValue(objId, "numAlerts", Integer.class) > 0;
			} catch (EnvException e) {
				throw new AlertServiceSystemException("Failed to get 'numAlerts' value of object : " + objId
				        + " due to", e);
			}
		}

		return getActiveAlerts(objId, propagate).size() > 0;
	}

	@Override
	public Collection<String> getAlertPrioritiesNames() {
		Collection<TO<?>> priorities = env.getObjectsByType(ALERT_PRIORITY_TYPE_NAME);
		if (priorities.isEmpty()) {
			throw new AlertServiceSystemException(
			        "Detected malformed object type model. No alert priorities configured");
		}

		final Collection<String> prioritiesNames = CollectionUtils.newList();
		for (final TO<?> priority : priorities) {
			String priorityName;
			try {
				priorityName = env.getPropertyValue(priority, "name", String.class);
			} catch (EnvException e) {
				throw new AlertServiceSystemException("Detected malformed object type", e);
			}
			prioritiesNames.add(priorityName);
		}
		return prioritiesNames;
	}

	@Override
	public String showActiveAlerts() {
		final StringBuilder sb = new StringBuilder();
		final AlertFilterBuilder afb = new AlertFilterBuilder();
		afb.active();

		for (final Alert alert : getAlerts(afb.build())) {
			sb.append(alert.toString());
			sb.append(Util.CRLF);
			sb.append(Util.CRLF);
		}
		return sb.toString();
	}

	@Override
	public AlertPriority getAlertPriority(final String name) {
		if (name == null) {
			throw new IllegalInputParameterException(String.format(NULL_INPUT_PARAMETER_MESSAGE, "name"));
		}
		if (name.isEmpty()) {
			throw new IllegalInputParameterException("Supplied 'name' is empty");
		}

		TO<?> priorityRef = Utils.findAlertPriority(env, name);
		if (priorityRef == null) {
			return null;
		}

		try {
			Collection<ValueTO> values = env.getAllPropertiesValues(priorityRef);
			return CollectionUtils.map(values, Converters.propertiesToAlertPriorityConverter(env));
		} catch (ObjectNotFoundException e) {
			return null;
		}
	}

	@Override
	public void updateAlertPriority(final String name, final AlertPriority newPriority)
	        throws NoSuchAlertPriorityException {

		if (name == null || name.isEmpty()) {
			throw new IllegalInputParameterException(String.format(NULL_INPUT_PARAMETER_MESSAGE, "name"));
		}

		if (newPriority == null) {
			throw new IllegalInputParameterException(String.format(NULL_INPUT_PARAMETER_MESSAGE, "newPriority"));
		}

		// priority name is constant by design
		if (!name.equals(newPriority.getName())) {
			throw new AlertServiceSystemException("Alert priority name cannot be changed");
		}

		TO<?> alertPriorityRef = Utils.findAlertPriority(env, name);
		if (alertPriorityRef == null) {
			throw new NoSuchAlertPriorityException(name);
		}

		List<ValueTO> newValues = CollectionUtils.map(newPriority, Converters.alertPriorityToPropertiesConverter(env));
		try {
			env.setAllPropertiesValues(alertPriorityRef, newValues.toArray(new ValueTO[] {}));
		} catch (EnvException e) {
			logger.warn("Failed to update alert priority : " + name, e);
			return;
		}

		Functor<Collection<ValueTO>, PriorityTier> tierConverter = Converters.tierToPropertiesConverter();

		for (PriorityTier tier : newPriority.getTiers()) {
			TO<?> tierRef = Utils.findAlertPriorityTier(env, newPriority.getName(), tier.getName());

			String oldTierName = null;
			try {
				oldTierName = env.getPropertyValue(tierRef, "name", String.class);
			} catch (EnvException e) {
				logger.warn("Failed to update priority " + name + "'s tier " + tier.getName(), e);
				continue;
			}

			if (oldTierName == null) {
				throw new AlertServiceSystemException("Alert priority tier name cannot be null");
			}

			// priority tier name is constant by design
			if (!oldTierName.equals(tier.getName())) {
				throw new AlertServiceSystemException("Alert priority tier name cannot be changed");
			}

			Collection<ValueTO> tierValues = tierConverter.apply(tier);
			try {
				env.setAllPropertiesValues(tierRef, tierValues.toArray(new ValueTO[] {}));
			} catch (EnvException e) {
				logger.warn("Failed to update priority " + name + "'s tier " + tier.getName());
			}
		}
	}

	@Override
	@Interceptors(AlertContentInterceptor.class)
	public Collection<Alert> getAlerts(final AlertFilter filter) {
		final Set<TO<?>> allRelatedObjects = CollectionUtils.newSet(filter.getHosts());

		if (filter.isPropagate()) {
			allRelatedObjects.addAll(Util.getIDsWithPropagatingByHierarchy(env, filter.getHosts()));
		}

		Functor<Alert, HistoricalRecord> historyToAlertConverter = new HistoryToAlert(new PropertiesToAlert(env));

		Collection<Alert> alerts = CollectionUtils.newList();

		if (filter.isHistorical()) {
			if (alertHistoryHolder == null) {
				throw new AlertServiceSystemException(
				        "Failed to get alert history because history holder object does not exist");
			}
			ObjectType typeAlertHistory = env.getObjectType(ALERT_HISTORY_TYPE_NAME);
			Collection<String> propNames = CollectionUtils.newList();
			for (PropertyDescr pd : typeAlertHistory.getPropertyDescriptors()) {
				propNames.add(pd.getName());
			}

			for (Pair<Date, Date> interval : filter.getIntervals()) {
				try {
					EnvObjectHistory alertsHistory = env.getHistory(alertHistoryHolder, propNames, interval.getFirst(),
					        interval.getSecond());
					for (HistoricalRecord hr : alertsHistory) {
						alerts.add(historyToAlertConverter.apply(hr));
					}
				} catch (ObjectNotFoundException e) {
					if (logger.isDebugEnabled()) {
						logger.debug("Failed to get history of alerts. Cannot find alert history holder object : "
						        + alertHistoryHolder, e);
					}
				} catch (PropertyNotFoundException e) {
					throw new AlertServiceSystemException("Failed to get history of alerts due to ", e);
				}
			}
		}

		if (filter.isActive()) {
			Collection<Alert> activeAlerts = getActiveAlerts(allRelatedObjects, filter.isPropagate());
			for (Alert alert : activeAlerts) {
				alerts.add(alert);
			}
		}

		Predicate<Alert> compoundFilter = new Predicate<Alert>() {
			@Override
			public boolean evaluate(Alert alert) {
				// set true if no intervals
				boolean matchTime = filter.getIntervals().isEmpty();

				for (Pair<Date, Date> interval : filter.getIntervals()) {
					Date alertTime = new Date(alert.getAlertTime());
					if (alertTime.after(interval.getFirst()) && alertTime.before(interval.getSecond())) {
						matchTime = true;
					}
				}

				boolean matchHost = false;
				if (allRelatedObjects.isEmpty()) {
					matchHost = true;
				} else if (allRelatedObjects.contains(alert.getHostTO())) {
					matchHost = true;
				}

				boolean matchStatus = false;
				if (filter.isAnyStatus()) {
					matchStatus = true;
				} else {
					for (AlertStatus status : filter.getStatuses()) {
						if (alert.getStatus().equals(status)) {
							matchStatus = true;
							break;
						}
					}
				}

				return matchHost && matchTime && matchStatus;
			}
		};

		alerts = CollectionUtils.searchAll(alerts, compoundFilter);

		return CollectionUtils.newList(alerts);
	}

	@Override
	public void setTemplate(final String typeName, final com.synapsense.dto.MessageTemplate template)
	        throws NoSuchAlertTypeException {
		TO<?> alertTypeRef = Utils.findAlertTypeByName(env, typeName);
		if (alertTypeRef == null) {
			throw new NoSuchAlertTypeException(typeName);
		}

		TO<?> templateRef = template.getRef();
		if (!env.exists(templateRef)) {
			return;
		}

		try {
			env.getPropertyValue(templateRef, "messagetype", String.class);
		} catch (EnvException e) {
			throw new AlertServiceSystemException("Failed to get 'messagetype' of template : " + templateRef
			        + " due to ", e);
		}

		try {
			Collection<TO<?>> templates = CollectionUtils.newList(env.getPropertyValue(alertTypeRef, "templates",
			        Collection.class));
			TO<?> templateToReplace = null;
			for (TO<?> t : templates) {
				// walk through all templates
				String mt = env.getPropertyValue(t, "messagetype", String.class);
				if (MessageType.decode(mt) == template.getMessageType()) {
					// remove existing template
					templateToReplace = t;
				}
			}

			if (templateToReplace != null)
				templates.remove(templateToReplace);

			templates.add(templateRef);
			// add new one
			env.setPropertyValue(alertTypeRef, "templates", templates);
		} catch (ObjectNotFoundException e) {
			throw new NoSuchAlertTypeException(typeName);
		} catch (PropertyNotFoundException e) {
			throw new AlertServiceSystemException("Failed to remove template of alert type : " + alertTypeRef
			        + " due to object type : " + ALERT_DESCRIPTION_TYPE_NAME + " has no property 'templates'", e);
		} catch (UnableToConvertPropertyException e) {
			throw new AlertServiceSystemException("Failed to remove template due to object type : "
			        + ALERT_DESCRIPTION_TYPE_NAME + "'s 'templates' property has invalid data type", e);
		}
	}

	@Override
	public void removeTemplate(final String typeName, final MessageType type) throws NoSuchAlertTypeException {
		TO<?> alertTypeRef = Utils.findAlertTypeByName(env, typeName);
		if (alertTypeRef == null) {
			throw new NoSuchAlertTypeException(typeName);
		}

		List<TO<?>> toRemoveList = CollectionUtils.newList();
		Collection<TO<?>> templates = CollectionUtils.newList();
		try {
			templates.addAll(env.getPropertyValue(alertTypeRef, "templates", Collection.class));
		} catch (ObjectNotFoundException e) {
			throw new NoSuchAlertTypeException(typeName);
		} catch (PropertyNotFoundException e) {
			throw new AlertServiceSystemException("Failed to get template of alert type : " + alertTypeRef
			        + " due to object type : " + ALERT_DESCRIPTION_TYPE_NAME + " has no property 'templates'", e);
		} catch (UnableToConvertPropertyException e) {
			throw new AlertServiceSystemException("Failed to get template due to object type : "
			        + ALERT_DESCRIPTION_TYPE_NAME + "'s 'templates' property has invalid data type", e);
		}

		for (TO<?> template : templates) {
			try {
				String typeId = env.getPropertyValue(template, "messagetype", String.class);
				if (type.equals(MessageType.decode(typeId))) {
					toRemoveList.add(template);
				}
			} catch (EnvException e) {
				logger.warn("Cannot remove template of type " + type.name() + ".Message template object : " + template
				        + " has no property \'messagetype\'", e);
			}
		}

		if (toRemoveList.isEmpty()) {
			return;
		}

		templates.removeAll(toRemoveList);

		try {
			env.setPropertyValue(alertTypeRef, "templates", templates);
		} catch (ObjectNotFoundException e) {
			throw new NoSuchAlertTypeException(typeName);
		} catch (PropertyNotFoundException e) {
			throw new AlertServiceSystemException("Failed to remove template of alert type : " + alertTypeRef
			        + " due to object type : " + ALERT_DESCRIPTION_TYPE_NAME + " has no property 'templates'", e);
		} catch (UnableToConvertPropertyException e) {
			throw new AlertServiceSystemException("Failed to remove template due to object type : "
			        + ALERT_DESCRIPTION_TYPE_NAME + "'s 'templates' property has invalid data type", e);
		}
	}

	@Override
	public void addActionDestination(final String alertTypeName, final String tierName, final MessageType messageType,
	        final String destination) throws NoSuchAlertTypeException {
		if (alertTypeName == null || alertTypeName.isEmpty()) {
			throw new IllegalInputParameterException(String.format(NULL_INPUT_PARAMETER_MESSAGE, "alertTypeName"));
		}

		if (tierName == null || tierName.isEmpty()) {
			throw new IllegalInputParameterException(String.format(NULL_INPUT_PARAMETER_MESSAGE, "tierName"));
		}

		if (destination == null || destination.isEmpty()) {
			throw new IllegalInputParameterException(String.format(NULL_INPUT_PARAMETER_MESSAGE, "userName"));
		}

		if (messageType == null) {
			throw new IllegalInputParameterException(String.format(NULL_INPUT_PARAMETER_MESSAGE, "messageType"));
		}

		// Command addDestination = new AddDestination(env, userService,
		// alertTypeName, destination, messageType,
		// tierName);
		// alertsEngine.addCommand(addDestination);

		TO<?> alertTypeRef = Utils.findAlertTypeByName(env, alertTypeName);
		if (alertTypeRef == null) {
			throw new NoSuchAlertTypeException(alertTypeName);
		}

		TO<?> userAccount = userService.getUser(destination);
		if (userAccount == null) {
			if (logger.isDebugEnabled()) {
				logger.debug("User with name : " + destination + " does not exist");
			}
		} else {
			if (MessageType.SNMP.equals(messageType)) {
				throw new AlertServiceSystemException("User cannot have " + MessageType.SNMP + " destinations");
			}
		}

		if (!Utils.existTier(env, alertTypeRef, tierName)) {
			throw new AlertServiceSystemException("Priority tier : " + tierName
			        + " cannot be determined for alert type : " + alertTypeRef);
		}

		final TO<?> destinationRef = Utils.findDestination(env, alertTypeRef, tierName, destination, messageType);
		if (destinationRef == null) {
			ValueTO[] destValues = new ValueTO[] { new ValueTO("alertType", alertTypeRef),
			        new ValueTO("tier", tierName), new ValueTO("destination", destination),
			        new ValueTO("messagetype", messageType.code()) };
			try {
				TO<?> destinationID = env.createObject(ALERT_DESTINATION_TYPE_NAME, destValues);
				if (logger.isDebugEnabled()) {
					logger.debug("Created new destination : " + destination + ", refered by : " + destinationID);
				}
			} catch (EnvException e) {
				logger.warn("Failed to create new destination instance : " + destination, e);
			}
		} else {
			if (logger.isDebugEnabled()) {
				logger.debug("Alert type : " + alertTypeName + ", tier : " + tierName
				        + " already associated with destination : " + destination);
			}
		}

	}

	@Override
	public void removeActionDestination(final String alertTypeName, final String tierName,
	        final MessageType messageType, final String destination) throws NoSuchAlertTypeException {
		if (alertTypeName == null || alertTypeName.isEmpty()) {
			throw new IllegalInputParameterException(String.format(NULL_INPUT_PARAMETER_MESSAGE, "alertTypeName"));
		}

		if (tierName == null || tierName.isEmpty()) {
			throw new IllegalInputParameterException(String.format(NULL_INPUT_PARAMETER_MESSAGE, "tierName"));
		}

		if (destination == null || destination.isEmpty()) {
			throw new IllegalInputParameterException(String.format(NULL_INPUT_PARAMETER_MESSAGE, "userName"));
		}

		if (messageType == null) {
			throw new IllegalInputParameterException(String.format(NULL_INPUT_PARAMETER_MESSAGE, "messageType"));
		}

		TO<?> alertTypeRef = Utils.findAlertTypeByName(env, alertTypeName);
		if (alertTypeRef == null) {
			throw new NoSuchAlertTypeException(alertTypeName);
		}

		if (!Utils.existTier(env, alertTypeRef, tierName)) {
			throw new AlertServiceSystemException("Priority tier : " + tierName
			        + " cannot be determined for alert type : " + alertTypeRef);
		}

		final TO<?> destinationRef = Utils.findDestination(env, alertTypeRef, tierName, destination, messageType);
		if (destinationRef == null) {
			if (logger.isDebugEnabled()) {
				logger.debug("Failed to remove destination : " + alertTypeName + "/" + tierName + "/" + messageType
				        + "/" + destination + ". Destination does not exist");
			}
			return;
		}

		try {
			env.deleteObject(destinationRef);
		} catch (EnvException e) {
			logger.warn("Failed to remove destination : " + alertTypeName + "/" + tierName + "/" + messageType + "/"
			        + destination, e);
		}
	}

	@Override
	public Collection<String> getActionDestinations(final String alertTypeName, final String tierName,
	        final MessageType messageType) throws NoSuchAlertTypeException {
		if (alertTypeName == null || alertTypeName.isEmpty()) {
			throw new IllegalInputParameterException(String.format(NULL_INPUT_PARAMETER_MESSAGE, "alertTypeName"));
		}

		if (tierName == null || tierName.isEmpty()) {
			throw new IllegalInputParameterException(String.format(NULL_INPUT_PARAMETER_MESSAGE, "tierName"));
		}

		if (messageType == null) {
			throw new IllegalInputParameterException(String.format(NULL_INPUT_PARAMETER_MESSAGE, "messageType"));
		}

		TO<?> alertTypeRef = Utils.findAlertTypeByName(env, alertTypeName);
		if (alertTypeRef == null) {
			if (logger.isDebugEnabled()) {
				logger.debug("Failed to get destinations. Alert type : " + alertTypeName + " does not exist");
			}
			throw new NoSuchAlertTypeException(alertTypeName);
		}

		Collection<String> destinations = Utils.findDestinations(env, alertTypeRef, tierName, messageType);

		return destinations;
	}

	@Override
	public String getActionDestination(final String alertTypeName, final String tierName,
	        final MessageType messageType, final String userName) throws NoSuchAlertTypeException {
		if (alertTypeName == null || alertTypeName.isEmpty()) {
			throw new IllegalInputParameterException(String.format(NULL_INPUT_PARAMETER_MESSAGE, "alertTypeName"));
		}

		if (userName == null || userName.isEmpty()) {
			throw new IllegalInputParameterException(String.format(NULL_INPUT_PARAMETER_MESSAGE, "userName"));
		}

		if (tierName == null || tierName.isEmpty()) {
			throw new IllegalInputParameterException(String.format(NULL_INPUT_PARAMETER_MESSAGE, "tierName"));
		}

		if (messageType == null) {
			throw new IllegalInputParameterException(String.format(NULL_INPUT_PARAMETER_MESSAGE, "messageType"));
		}

		if (messageType.equals(MessageType.SNMP)) {
			return null;
		}

		Collection<String> destinations = getActionDestinations(alertTypeName, tierName, messageType);

		boolean userMatchDestination = false;
		for (String dest : destinations) {
			if (dest.equals(userName)) {
				userMatchDestination = true;
			}
		}

		if (!userMatchDestination) {
			return null;
		}

		final TO<?> user = userService.getUser(userName);
		switch (messageType) {
		case SMTP: {
			final String email = (String) userService.getProperty(user, userService.USER_PRIMARY_EMAIL);
			return email == null ? "" : email;
		}
		case SMS: {
			final String smsAddress = (String) userService.getProperty(user, userService.USER_SMS_ADDRESS);
			return smsAddress == null ? "" : smsAddress;
		}
		default:
			return null;
		}
	}

	// dependency injection methods

	@EJB(beanName = "GenericEnvironment")
	public void setEnvironment(final Environment env) {
		this.env = env;
	}

	@EJB(beanName = "LocalUserManagementService")
	public void setUserService(final UserManagementService userService) {
		this.userService = userService;
	}

	@EJB(beanName = "ActivityLogDAO")
	public void setActivityLogService(ActivityLogService activityLogService) {
		this.activityLogService = activityLogService;
	}

	public void setAlertCommandProcessor(CommandEngine processor) {
		alertsEngine = processor;
	}

	// private members
	private TO<?> createAlertHistoryHolder() {

		TO<?> existingHolder = getAlertHistoryHolder();
		if (existingHolder != null) {
			return existingHolder;
		}

		try {
			return env.createObject(ALERT_HISTORY_TYPE_NAME);
		} catch (EnvException e) {
			throw new AlertServiceSystemException(
			        "Cannot create alert history holder. Alert historical data cannot be collected", e);
		}
	}

	private TO<?> getAlertHistoryHolder() {
		Collection<TO<?>> alertHistoryHolders = env.getObjectsByType(ALERT_HISTORY_TYPE_NAME);
		if (!alertHistoryHolders.isEmpty()) {
			return alertHistoryHolders.iterator().next();
		} else {
			return null;
		}
	}

	private TO<?> identifyServiceCaller() {
		final Principal principal = ctx.getCallerPrincipal();
		final String callerName = principal.getName();
		TO<?> user = userService.getUser(callerName);
		return user;
	}

	private void dismissAlerts(final Collection<TO<?>> alerts, final String comment) {
		alertsEngine.addCommand(getLogAndDismissAlertsCommand(alerts, comment));
	}

	private void dismissAlertsSync(final Collection<TO<?>> alerts, final String comment) {
		alertsEngine.addSyncCommand(getLogAndDismissAlertsCommand(alerts, comment));
	}

	private Command getDismissOnlyAlertsCommand(final Collection<TO<?>> alerts, final String comment) {
		final TO<?> user = identifyServiceCaller();

		UpdateStatus udpate = new UpdateStatus(env, alerts, AlertStatus.DISMISSED, comment, user,
		        System.currentTimeMillis());

		DeleteAlert deleteAlert = new DeleteAlert(env, alertStore, alerts);

		return Commands.chaining("dismissAlertChain", udpate, deleteAlert);
	}

	private Command getLogAndDismissAlertsCommand(final Collection<TO<?>> alerts, final String comment) {
		final TO<?> user = identifyServiceCaller();

		UpdateStatus update = new UpdateStatus(env, alerts, AlertStatus.DISMISSED, comment, user,
		        System.currentTimeMillis());

		RecordAlertHistory saveAlert = new RecordAlertHistory(env, alerts, alertHistoryHolder);
		DeleteAlert deleteAlert = new DeleteAlert(env, alertStore, alerts);

		return Commands.chaining("dismissAndLogAlertChain", update, saveAlert, deleteAlert);
	}

	@Override
	public AlertType getAlertType(String typeName) {
		Collection<AlertType> types = getAlertTypes(new String[] { typeName });
		if (types.isEmpty())
			return null;
		return types.iterator().next();
	}

	@Override
	public Collection<AlertType> getAlertTypesByDestination(String destination, MessageType messageType) {
		if (destination == null) {
			throw new IllegalInputParameterException(String.format(NULL_INPUT_PARAMETER_MESSAGE, "destination"));
		}

		if (messageType == null) {
			throw new IllegalInputParameterException(String.format(NULL_INPUT_PARAMETER_MESSAGE, "messageType"));
		}

		// TODO : consider to extend Search API (QueryDescriptor)
		ValueTO[] filter = new ValueTO[2];
		filter[0] = new ValueTO("destination", destination);
		filter[1] = new ValueTO("messagetype", messageType.code());

		Collection<AlertType> result = CollectionUtils.newList();

		Collection<TO<?>> destinations = env.getObjects(AlertConstants.TYPE.ALERT_DESTINATION_TYPE_NAME, filter);
		if (destinations.isEmpty()) {
			return result;
		}

		Collection<CollectionTO> alertTypeRefsBundle = env.getPropertyValue(destinations, new String[] { "alertType" });
		Collection<TO<?>> alertTypeIds = CollectionUtils.map(alertTypeRefsBundle,
		        new CollectionUtils.Functor<TO<?>, CollectionTO>() {
			        @Override
			        public TO<?> apply(CollectionTO input) {
				        return (TO<?>) input.getSinglePropValue("alertType").getValue();
			        }
		        });

		Collection<CollectionTO> alertTypesBundle = env.getAllPropertiesValues(alertTypeIds);

		final Functor<AlertType, Collection<ValueTO>> converter = Converters.propertiesToAlertTypeConverter(env);

		result.addAll(CollectionUtils.map(alertTypesBundle, new CollectionUtils.Functor<AlertType, CollectionTO>() {
			@Override
			public AlertType apply(CollectionTO input) {
				return converter.apply(input.getPropValues());
			}
		}));

		return result;
	}

}
