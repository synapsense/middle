package com.synapsense.impl.reporting.model;

public interface ReportGenerationListener {

	void cancelled();

	void completed(Report report);

	void failed(Throwable e);
}
