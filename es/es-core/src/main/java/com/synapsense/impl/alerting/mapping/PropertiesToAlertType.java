package com.synapsense.impl.alerting.mapping;

import static com.synapsense.impl.alerting.AlertConstants.TYPE.ALERT_PRIORITY_TYPE_NAME;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import com.synapsense.dto.AlertType;
import com.synapsense.dto.MessageTemplateRef;
import com.synapsense.dto.MessageType;
import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.EnvException;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.exception.PropertyNotFoundException;
import com.synapsense.exception.UnableToConvertPropertyException;
import com.synapsense.impl.utils.EnvironmentUtils;
import com.synapsense.service.Environment;
import com.synapsense.util.CollectionUtils;
import com.synapsense.util.CollectionUtils.Functor;

public class PropertiesToAlertType implements Functor<AlertType, Collection<ValueTO>> {
	public PropertiesToAlertType(Environment env) {
		super();
		this.env = env;
	}

	private final static Logger logger = Logger.getLogger(PropertiesToAlertType.class);

	private Environment env;

	@Override
	public AlertType apply(Collection<ValueTO> input) {
		Map<String, Object> values = EnvironmentUtils.valuesAsMap(input);
		String name = (String) values.get("name");
		String displayName = (String) values.get("displayName");
		String description = (String) values.get("description");
		boolean isActive = (Integer) values.get("active") == 1;
		boolean autoAcknowledge = (Integer) values.get("autoAck") == 1;
		boolean autoDismiss = (Integer) values.get("autoDismiss") == 1;
		boolean autoDismissAllowed = (Integer) values.get("autoDismissAllowed") == 1;
		TO<?> priority = (TO<?>) values.get("priority");
		String priorityName = null;
		try {
			priorityName = env.getPropertyValue(priority, "name", String.class);
		} catch (EnvException e) {
			throw new MappingException("Detected malformed object type : " + ALERT_PRIORITY_TYPE_NAME, e);
		}
		Collection<TO<?>> templates = (Collection<TO<?>>) values.get("templates");
		Set<com.synapsense.dto.MessageTemplate> templatesToSet = CollectionUtils.newSet();
		for (TO<?> ref : templates) {
			try {
				String messageType = env.getPropertyValue(ref, "messagetype", String.class);
				MessageType mt = MessageType.decode(messageType);
				templatesToSet.add(new MessageTemplateRef(mt, ref));
			} catch (ObjectNotFoundException e) {
				logger.warn("Failed to load message template : " + ref  + " for alert type : " + name, e);
			} catch (PropertyNotFoundException e) {
				throw new MappingException("Detected malformed object : " + ref + ". Missing required property 'messagetype':String", e);
            } catch (UnableToConvertPropertyException e) {
            	throw new MappingException("Detected malformed object : " + ref + ". Missing required property 'messagetype':String", e);
            }
		}

		AlertType type = new AlertType(name, displayName , description, priorityName, isActive, autoDismiss, autoAcknowledge);
		type.setAutoDismissAllowed(autoDismissAllowed);
		type.setMessageTemplates(templatesToSet);
		return type;
	}
}
