package com.synapsense.impl.alerting.notification;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.synapsense.util.CollectionUtils;

public class EscalationTier {
	private final int repeat;
	private final long interval;
	private final Map<String, List<NTemplateItem>> items;
	private final String name;
	private final int ordinal;

	public EscalationTier(final String name, final int ordinal, final int repeat, final long interval) {
		this.name = name;
		this.repeat = repeat;
		this.interval = interval;
		this.ordinal = ordinal;
		items = CollectionUtils.newMap();
	}

	public int getRepeat() {
		return repeat;
	}
	
	public int getOrdinal() {
		return ordinal;
	}

	public long getInterval() {
		return interval;
	}

	public String getName() {
		return name;
	}

	public List<NTemplateItem> getMainItems() {
		return items.get("_main");
	}

	public List<NTemplateItem> getAckItems() {
		return items.get("_ack");
	}

	public List<NTemplateItem> getResolveItems() {
		return items.get("_resolve");
	}

	public void addMainItem(final NTemplateItem item) {
		addItem("_main", item);
	}

	public void addAckItem(final NTemplateItem item) {
		addItem("_ack", item);
	}

	public void addResolveItem(final NTemplateItem item) {
		addItem("_resolve", item);
	}

	@Override
    public int hashCode() {
	    final int prime = 31;
	    int result = 1;
	    result = prime * result + (int) (interval ^ (interval >>> 32));
	    result = prime * result + ((items == null) ? 0 : items.hashCode());
	    result = prime * result + ((name == null) ? 0 : name.hashCode());
	    result = prime * result + ordinal;
	    result = prime * result + repeat;
	    return result;
    }

	@Override
    public boolean equals(Object obj) {
	    if (this == obj)
		    return true;
	    if (obj == null)
		    return false;
	    if (getClass() != obj.getClass())
		    return false;
	    EscalationTier other = (EscalationTier) obj;
	    if (interval != other.interval)
		    return false;
	    if (items == null) {
		    if (other.items != null)
			    return false;
	    } else if (!items.equals(other.items))
		    return false;
	    if (name == null) {
		    if (other.name != null)
			    return false;
	    } else if (!name.equals(other.name))
		    return false;
	    if (ordinal != other.ordinal)
		    return false;
	    if (repeat != other.repeat)
		    return false;
	    return true;
    }

	void addMainItems(final List<NTemplateItem> items) {
		for (final NTemplateItem item : items) {
			addMainItem(item);
		}
	}

	void addAckItems(final List<NTemplateItem> items) {
		for (final NTemplateItem item : items) {
			addAckItem(item);
		}
	}

	void addResolveItems(final List<NTemplateItem> items) {
		for (final NTemplateItem item : items) {
			addResolveItem(item);
		}
	}

	private void addItem(final String mapKey, final NTemplateItem item) {
		if (items.containsKey(mapKey)) {
			items.get(mapKey).add(item);
		} else {
			LinkedList<NTemplateItem> lst = new LinkedList<NTemplateItem>();
			lst.add(item);
			items.put(mapKey, lst);
		}
	}

}