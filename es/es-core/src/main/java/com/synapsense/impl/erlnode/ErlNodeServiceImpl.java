package com.synapsense.impl.erlnode;

import com.synapsense.exception.AcApiException;
import com.synapsense.service.ErlNodeService;
import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecuteResultHandler;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.log4j.Logger;

import com.synapsense.dal.objectcache.CacheEnvironmentDispatcher;
import com.synapsense.dal.objectcache.CacheSvcImpl;

import com.synapsense.service.AlertService;
import com.synapsense.service.Environment;
import com.synapsense.service.impl.activitylog.ActivityLogService;
import com.synapsense.service.nsvc.NotificationService;

import com.synapsense.cdi.MBean;

import java.io.IOException;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.DependsOn;
import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Singleton;
import javax.ejb.Startup;

@Startup @Singleton(name="ErlNodeService") @Remote(ErlNodeService.class)
@MBean("com.synapsense:type=ErlNodeService")
@ConcurrencyManagement(ConcurrencyManagementType.BEAN)
@DependsOn("SynapCache")
public class ErlNodeServiceImpl implements ErlNodeServiceImplMBean, ErlNodeService {

	private final static Logger log = Logger.getLogger(ErlNodeServiceImpl.class);

	private Environment env;
	private NotificationService nsvc;
	private AlertService asvc;
	private ActivityLogService als;

	private ThreadDriver driver;

	@Override
	public void create() throws Exception {
		log.info("ErlNode service created");

	}

	@Override
	@PostConstruct
	public void start() throws Exception {
		log.info("Starting Erlang Process Mapper Daemon...");
		try {
			startProcess("epmd.exe -daemon -relaxed_command_check");
		} catch (IOException e) {
			// Not fatal, epmd might already be running.
			log.warn("Failed to start Erlang Process Mapper Daemon", e);
		}
		log.info("Starting ErlNode service...");
		driver = new ThreadDriver(env, nsvc, asvc, als);
		driver.start();
		log.info("ErlNode service started");
	}

	@Override
	@PreDestroy
	public void stop() {
		log.info("Stopping ErlNode service...");
		try {
			driver.halt();
		} catch (InterruptedException e) {
			log.error("ErlNode service could not be stopped");
		}
		log.info("Stopping Erlang Process Mapper Daemon...");
		try {
			executeProcess("epmd.exe -kill");
		} catch (IOException e) {
			log.warn("Failed to stop Erlang Process Mapper Daemon", e);
		}
		log.info("ErlNode service stopped");
	}

	@Override
	public void destroy() {
		log.info("ErlNode service destroyed");
	}

	public Environment getEnvironment() {
		return env;
	}

	@EJB(beanName="LocalEnvironment")
	public void setEnvironment(Environment env) {
		this.env = CacheEnvironmentDispatcher.getEnvironment(CacheSvcImpl.getInstance(), env);
	}

	public NotificationService getNsvc() {
		return nsvc;
	}

	@EJB(beanName="LocalNotificationService")
	public void setNsvc(NotificationService nsvc) {
		this.nsvc = nsvc;
	}

	public AlertService getAsvc() {
		return asvc;
	}

	@EJB
	public void setAsvc(AlertService asvc) {
		this.asvc = asvc;
	}

	public ActivityLogService getAls() {
		return als;
	}

	@EJB
	public void setAls(ActivityLogService als) {
		this.als = als;
	}

	public String call_ac(String json_rpc) throws AcApiException {
		return driver.call_ac(json_rpc);
	}

	/**
	 * executes an external process. stdout and stderr are redirected to the current process. The execution is synchronous, this method
	 * returns only after the process terminates.
	 *
	 * <p>TODO: Move this in a commons project next time we refactor the code.</p>
	 *
	 * @param commandLine the command to be executed, for example "epmd.exe -kill"
	 *
	 * @throws IOException
	 */
	private static void executeProcess(String commandLine) throws IOException {
		new DefaultExecutor().execute(CommandLine.parse(commandLine));
	}

	/**
	 * starts an external process. stdout and stderr are redirected to the current process. The execution is asynchronous, this method
	 * returns as soon as the process is started.
	 *
	 * <p>TODO: Move this in a commons project next time we refactor the code.</p>
	 *
	 * @param commandLine the command to be executed, for example "epmd.exe -daemon -relaxed_command_check"
	 *
	 * @throws IOException
	 */
	private static void startProcess(String commandLine) throws IOException {
		new DefaultExecutor().execute(CommandLine.parse(commandLine), new DefaultExecuteResultHandler());
	}

}