package com.synapsense.impl.erlnode.filters;

import org.apache.log4j.Logger;

import com.ericsson.otp.erlang.OtpErlangTuple;
import com.synapsense.service.nsvc.filters.ObjectDeletedEventFilter;
import com.synapsense.service.nsvc.filters.TOMatcher;

//-record(object_deleted_event_filter, {matcher = #regex{} :: #regex{} | #to{}}).
class ObjectDeletedEventFilterParser{

	private static final Logger log =
		Logger.getLogger(ObjectDeletedEventFilter.class);

	public ObjectDeletedEventFilter parse(OtpErlangTuple filterDef) {
		TOMatcher matcher =
			TOMatcherParser.parse(filterDef.elementAt(1));
		log.debug("returning ObjectDeletedEventFilter");
		return new ObjectDeletedEventFilter(matcher);
	}
	
}
