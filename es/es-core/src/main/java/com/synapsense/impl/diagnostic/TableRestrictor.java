package com.synapsense.impl.diagnostic;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TableRestrictor {
	private static DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	public String tableName = null;
	public String daysLimit = null;
	public String timestampColumn = null;

	public TableRestrictor() {
	}

	public TableRestrictor(String tableName, String daysLimit, String timestampColumn) {
		super();
		this.tableName = tableName;
		this.daysLimit = daysLimit;
		this.timestampColumn = timestampColumn;
	}

	public String generateRestriction() {
		String result = "";

		if (daysLimit != null && timestampColumn != null) {
			Date nowTime = new Date();
			long endTime = nowTime.getTime();
			long beginTime = endTime - (Integer.parseInt(daysLimit)) * 24 * 60 * 60 * 1000;

			String endDateStr = dateFormatter.format(nowTime);
			nowTime.setTime(beginTime);
			String beginDateStr = dateFormatter.format(nowTime);

			result = " where " + timestampColumn + " between '" + beginDateStr + "' and '" + endDateStr + "'";
		}

		return result;
	}

}
