package com.synapsense.registrar;

import com.google.common.net.InetAddresses;
import com.synapsense.cdi.MBean;
import com.synapsense.dal.objectcache.CacheEnvironmentDispatcher;
import com.synapsense.dal.objectcache.CacheSvcImpl;
import com.synapsense.dto.Alert;
import com.synapsense.plugin.DeviceManagerRMI;
import com.synapsense.registrar.configuring.DMConfigurator;
import com.synapsense.registrar.configuring.ModuleConfigurator;
import com.synapsense.service.AlertService;
import com.synapsense.service.Environment;
import com.synapsense.service.impl.registrar.RegistrarSvc;
import com.synapsense.util.LocalizationUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.*;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Date;
import java.util.HashMap;

/**
 * @author Alexander Borisov
 * 
 */
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
@Startup
@Singleton(name = "Registrar")
@DependsOn("SynapCache")
@Remote(RegistrarSvc.class)
@ConcurrencyManagement(ConcurrencyManagementType.BEAN)
@MBean("com.synapsense:type=RegistrarSvc")
public class RegistrarSvcImpl implements RegistrarSvcImplMBean, RegistrarSvc {
	private Log log = LogFactory.getLog(RegistrarSvcImpl.class);

	private static final String SYNAP_FILESTORAGE_PROPERTY = "com.synapsense.filestorage";

	private Environment env;
	private HashMap<String, Connection> svc;

	private ModuleConfigurator<DeviceManagerRMI> configurator;

	@EJB
	private AlertService alertSvc;

	@EJB(beanName = "GenericEnvironment")
	public void setEnvironment(Environment env) {
		this.env = CacheEnvironmentDispatcher.getEnvironment(CacheSvcImpl.getInstance(), env);
	}

	@Override
	public String print() {
		StringBuffer res = new StringBuffer(100);
		for (String name : svc.keySet()) {
			res.append(name);
			res.append(":");
			res.append(svc.get(name));
			res.append(System.getProperty("line.separator", "\n"));

		}
		return res.toString();
	}

	private class Connection {
		private int port;
		private String address;

		public Connection(String connectionInfo) throws UnknownHostException {
			String data[] = connectionInfo.split(":");

			if (data.length != 2) {
				throw new IllegalArgumentException(
				        "Incorrect connection information is provided in the connection string [" + connectionInfo
				                + "]");
			}

			address = data[0];
			// try resolve host name
			if (!InetAddresses.isInetAddress(address)) {
				address = InetAddress.getByName(address).getHostAddress();
			}

			try {
				port = Integer.parseInt(data[1]);
			} catch (NumberFormatException e) {
				throw new IllegalArgumentException("Wrong port number is provided in the connection string ["
				        + connectionInfo + "]");
			}
		}

		public int getPort() {
			return port;
		}

		public String getIP() {
			return address;
		}

		@Override
		public String toString() {
			return address + ":" + port;
		}

		@Override
		public boolean equals(Object o) {
			if (this == o)
				return true;
			if (o == null || getClass() != o.getClass())
				return false;

			Connection that = (Connection) o;

			if (port != that.port)
				return false;
			if (!address.equals(that.address))
				return false;

			return true;
		}

		@Override
		public int hashCode() {
			int result = port;
			result = 31 * result + address.hashCode();
			return result;
		}
	}

	private boolean isConnectionValid(Connection connectionInfo, String componentName) {
		try {
			return getComponentProxy(componentName, DeviceManagerRMI.class) != null;
		} catch (IllegalArgumentException e) {
			if (log.isDebugEnabled()) {
				log.info("Previously registered DM is not accessible on " + svc.get(componentName)
				        + ". Allowing the new one from " + connectionInfo + " to register", e);
			} else {
				log.info("Previously registered DM is not accessible on " + svc.get(componentName)
				        + ". Allowing the new one from " + connectionInfo + " to register");
			}

			return false;
		}
	}

	@Override
	synchronized public void registerComponent(String componentName, String connectionInfo) {
		if (log.isDebugEnabled()) {
			log.debug("Registering new component [" + componentName + "]");
		}

		Connection newConnection = null;
		try {
			newConnection = new Connection(connectionInfo);
		} catch (UnknownHostException e) {
			// throw an exception to the new DM so it doesn't attempt to
			// proceed
			throw new IllegalStateException("Unknown host name is given : " + connectionInfo);
		}

		// we do not allow multiple DM registrations
		Connection oldConnection = svc.get(componentName);
		if (oldConnection != null && !oldConnection.equals(newConnection)
		        && isConnectionValid(oldConnection, componentName)) {
			// disallow new one to register and raise an alert
			log.warn(String
			        .format("Attempt to register the second DM from %1$s while the one on %2$s is still accessible. Registration denied.",
			                connectionInfo, oldConnection));

			String message = LocalizationUtils.getLocalizedString("audit_message_multiple_dm_registration",
			        newConnection.toString(), oldConnection.toString());
			alertSvc.raiseAlert(new Alert("audit_alert", "Audit Alert", new Date(), message));
			// throw an exception to the new D M so it doesn't attempt to
			// proceed
			throw new IllegalStateException("Another Device Manager is already running on " + oldConnection);

		} else {
			svc.put(componentName, newConnection);
			configureComponent(componentName);
			if (log.isDebugEnabled()) {
				log.debug("[" + componentName + "] registered");
			}
		}

	}

	@Override
	public void configureComponent(final String appName) {
		log.info("Configuring " + appName + " component.");

		try {
			DeviceManagerRMI dm = getComponentProxy(appName, DeviceManagerRMI.class);
			if (dm == null) {
				log.warn(appName + " component is not registered by Registrar. Cannot configure it.");
				return;
			}
			configurator.configureModule(dm);
		} catch (Exception e) {
			throw new IllegalArgumentException("Unable to configure module [" + appName + "]", e);
		}

	}

	@Override
	public void unRegisterComponent(String componentName) {
		svc.remove(componentName);
	}

	// Lifecycle methods
	@PostConstruct
	public void create() throws Exception {
		log.info("RegistrarSvc - Creating");
		svc = new HashMap<>();

		// set com.synapsense.filestorage pointing to jboss.server.config.dir
		// by default if not specified otherwise
		if (!System.getProperties().containsKey(SYNAP_FILESTORAGE_PROPERTY)) {
			String confFolder = System.getProperties().getProperty("jboss.server.config.dir");
			confFolder = confFolder.replaceAll("\\\\", "/");
			System.getProperties().put(SYNAP_FILESTORAGE_PROPERTY, confFolder);
			log.info(SYNAP_FILESTORAGE_PROPERTY + " not set; setting to default: " + confFolder);
		}

		start();
	}

	public void start() throws Exception {
		log.info("RegistrarSvc - Starting");

		configurator = new DMConfigurator(env);
	}

	@PreDestroy
	public void stop() {
		log.info("RegistrarSvc - Stopping");
		svc.clear();
		destroy();
	}

	public void destroy() {
		log.info("RegistrarSvc - Destroying");
		svc = null;
	}

	@Override
	public String getComponentConnection(String componentName) {
		Connection conn = svc.get(componentName);
		if (conn == null)
			return null;
		return conn.toString();
	}

	@Override
	public String[] getComponents() {
		return svc.keySet().toArray(new String[svc.size()]);
	}

	@Override
	public <COMPONENT_PROXY> COMPONENT_PROXY getComponentProxy(String name, Class<COMPONENT_PROXY> clazz) {
		Registry tmp;
		Connection connectionInfo = svc.get(name);
		if (connectionInfo == null) {
			if (log.isDebugEnabled()) {
				log.debug(name + " is not registered");
			}

			return null;
		}

		COMPONENT_PROXY result;
		try {
			tmp = LocateRegistry.getRegistry(connectionInfo.getIP(), connectionInfo.getPort());
			result = clazz.cast(tmp.lookup(name));
		} catch (Exception e) {
			throw new IllegalArgumentException("Cannot lookup component " + name + " with connection info "
			        + connectionInfo);
		}
		return result;

	}
}
