package com.synapsense.registrar.configuring;

import com.synapsense.exception.ConfigurationException;

public interface ModuleConfigurator<T> {
	void configureModule(T module) throws ConfigurationException;
}
