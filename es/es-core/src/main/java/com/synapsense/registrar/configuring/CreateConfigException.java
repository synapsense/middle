package com.synapsense.registrar.configuring;

public class CreateConfigException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = -961635915178697730L;

	public CreateConfigException(String desc) {
		super(desc);
	}

	public CreateConfigException(String desc, Throwable cause) {
		super(desc, cause);
	}
}
