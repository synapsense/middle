package com.synapsense.registrar;

import com.synapsense.service.impl.registrar.RegistrarMgmt;

//dummy interface for JMX MBean name compatibility
public interface RegistrarSvcImplMBean extends RegistrarMgmt {

}
