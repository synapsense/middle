package com.synapsense.registrar.configuring;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import com.synapsense.dto.ValueTO;

public class ValueTOExtractor {
	private Map<String, Object> data;

	public ValueTOExtractor(Collection<ValueTO> values) {
		data = new HashMap<String, Object>(values.size());
		for (ValueTO value : values) {
			data.put(value.getPropertyName(), value.getValue());
		}
	}

	@SuppressWarnings("unchecked")
	public <T> T get(String name, Class<T> cls) {
		return (T) get(name);
	}

	public Object get(String name) {
		return data.get(name);
	}
}
