package com.synapsense.registrar.configuring;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import org.apache.log4j.Logger;

import com.synapsense.dto.CollectionTO;
import com.synapsense.dto.ObjectType;
import com.synapsense.dto.PropertyDescr;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.dto.Tag;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.ConfigurationException;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.exception.PropertyNotFoundException;
import com.synapsense.plugin.DeviceManagerRMI;
import com.synapsense.service.Environment;

public class DMConfigurator implements ModuleConfigurator<DeviceManagerRMI> {

	private Executor executor = Executors.newSingleThreadExecutor();
	private final Environment env;

	public DMConfigurator(Environment env) {
		this.env = env;
	}

	@Override
	/**
	 * This method creates new configuration task and submits it for immediate execution to the executor.
	 * The actual configuration process runs asynchronously in a separate thread.
	 */
	public void configureModule(DeviceManagerRMI module) throws ConfigurationException {
		executor.execute(new DMConfigurationTask(env, module));

	}

}

class DMConfigurationTask implements Runnable {
	private final static Logger logger = Logger.getLogger(DMConfigurator.class);
	public final static String TAG_NAME = "pluginId";
	public final static String PROPERTY_NAME = "name";
	private final static String XML_HEADER = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<DMConfig xmlns=\"http://www.synapsense.com/plugin/apinew\" "
	        + "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" "
	        + "xsi:schemaLocation=\"http://www.synapsense.com/plugin/apinew/dmPluginConfig.xsd\">\n";
	private final static String XML_FOOTER = "</DMConfig>\n";
	private final static String PLUGIN_HEADER = "<plugin name=\"%1$s\">\n";
	private final static String PLUGIN_FOOTER = "</plugin>\n";
	private final static String OBJECT_HEADER = "<obj id=\"%1$s\">\n";
	private final static String OBJECT_FOOTER = "</obj>\n";
	private final static String OBJECT_SELF_CONTAINED = "<obj id=\"%1$s\"/>\n";
	private final static String PROPERTY_SELF_CONTAINED = "<prop name=\"%1$s\" value=\"%2$s\"/>\n";
	private final static String PROPERTY_HEADER = "<prop name=\"%1$s\" value=\"%2$s\">\n";
	private final static String PROPERTY_FOOTER = "</prop>\n";
	private final static String TAG_SELF_CONTAINED = "<tag name=\"%1$s\" value=\"%2$s\" />\n";
	private Map<TO<?>, List<TO<?>>> allObjectsChildren;
	private Map<TO<?>, List<ValueTO>> allObjectsProperties = new HashMap<TO<?>, List<ValueTO>>();
	private Map<String, ObjectType> objectTypesDescription;

	private final Environment env;
	private final DeviceManagerRMI dm;

	public DMConfigurationTask(Environment env, DeviceManagerRMI dm) {
		this.env = env;
		this.dm = dm;
	}

	@Override
	public void run() {
		if (logger.isDebugEnabled()) {
			logger.debug("Sending config to component" + dm.getClass().toString());
		}

		try {
			List<Properties> plugins = dm.enumeratePlugins();

			if (plugins.size() == 0) {
				logger.debug("Empty plugins list, nothing to configure.");
				return;
			}
			String config = createConfig(plugins);
			if (config == null) {
				logger.warn("No configuration info to configure plug-ins" + plugins);
			} else {
				dm.configurePlugins(config);
			}
		} catch (Exception e) {
			logger.warn("Cannot configure plugins due to ", e);
		}
	}

	private String createConfig(final List<Properties> plugins) {
		logger.info("createConfig(): " + plugins);

		allObjectsProperties = new HashMap<TO<?>, List<ValueTO>>();
		objectTypesDescription = new HashMap<String, ObjectType>();
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(XML_HEADER);

		// For each plugin find root objects, which has "name" property tagged
		// with the provided pluginID
		for (Properties pluginDesc : plugins) {
			String pluginID = pluginDesc.getProperty("PLUGIN_ID", "");
			String[] rootTypes = pluginDesc.getProperty("ROOT_TYPES", "").split(",");
			List<TO<?>> rootTOs = new ArrayList<TO<?>>();
			for (String type : rootTypes) {
				if (type != null && !type.isEmpty()) {
					Map<TO<?>, Collection<Tag>> typeTags = env.getAllTags(type);
					for (TO<?> to : typeTags.keySet()) {
						for (Tag tag : typeTags.get(to)) {
							// check if tag belongs to "name" property and the
							// value
							// is the plugin ID
							if (PROPERTY_NAME.equals(tag.getPropertyName()) && TAG_NAME.equals(tag.getTagName())
							        && pluginID.equals(tag.getValue())) {
								rootTOs.add(to);
							}
						}
					}
				}
			}
			if (!rootTOs.isEmpty()) {
				// add <plugin> tag to the config
				try {
					stringBuilder.append(format(PLUGIN_HEADER, pluginID));

					stringBuilder.append(format(PROPERTY_SELF_CONTAINED, "timestamp", "0"));
					if (logger.isDebugEnabled()) {
						logger.debug("Adding config for the plugin with ID: " + pluginID);
					}
					// get All objects' children
					allObjectsChildren = findChildren(rootTOs);

					// combine all objects in the hierarchy into one list
					List<TO<?>> allObjects = new ArrayList<TO<?>>(rootTOs);
					for (Collection<TO<?>> children : allObjectsChildren.values()) {
						allObjects.addAll(children);
					}
					// Get properties for all the objects in hierarchy
					Collection<CollectionTO> allProperties = env.getAllPropertiesValues(allObjects);
					// Transform it into a map with object TO as a key for quick
					// search
					for (CollectionTO collectionTO : allProperties) {
						allObjectsProperties.put(collectionTO.getObjId(), collectionTO.getPropValues());
					}
					// Add root elements to the config string and initiate
					// recursive
					// tree walk through
					for (TO<?> to : rootTOs) {
						stringBuilder.append(fillObject(to));
					}
					// close <plugin> tag
					stringBuilder.append(PLUGIN_FOOTER);
					if (logger.isDebugEnabled()) {
						logger.debug("Config for the plugin with ID: " + pluginID + " is added.");
					}
				} catch (ConfigurationException e) {
					logger.error("Unable to generate configuration for plugin [" + pluginID + "]\n Reason: "
					        + e.getMessage());
				}
			}
		}
		// close <DMConfig> tag
		stringBuilder.append(XML_FOOTER);
		return stringBuilder.toString();
	}

	private Map<TO<?>, List<TO<?>>> findChildren(List<TO<?>> rootTOs) {
		Map<TO<?>, List<TO<?>>> allObjects = new HashMap<TO<?>, List<TO<?>>>();
		for (TO<?> to : rootTOs) {
			LinkedList<TO<?>> queue = new LinkedList<TO<?>>();
			queue.offer(to);
			while (!queue.isEmpty()) {
				TO<?> nextTOtoHandle = queue.poll();
				Collection<TO<?>> children = null;
				try {
					children = env.getChildren(nextTOtoHandle);
					allObjects.put(nextTOtoHandle, new ArrayList<TO<?>>(children));
					for (TO<?> child : children) {
						queue.offer(child);
					}
				} catch (ObjectNotFoundException e) {
					logger.warn("Unable to find object with id " + nextTOtoHandle.toString()
					        + ", the object is skipped");
				}
			}
		}
		return allObjects;
	}

	// Recursively walks through the objects tree. Adds info about object
	// and it's children into config.
	private String fillObject(final TO<?> to) throws ConfigurationException {
		// will contain object tag, properties and all child objects
		StringBuilder stringBuilder = new StringBuilder();
		String typeName = to.getTypeName();
		ObjectType objectType = objectTypesDescription.get(typeName);
		if (objectType == null) {
			objectType = env.getObjectType(typeName);
			objectTypesDescription.put(typeName, objectType);
		}
		String objectProperties = fillObjectProperties(to, objectType);

		Collection<TO<?>> children = allObjectsChildren.get(to);
		// will contain all child objects
		StringBuilder childrenStringBuilder = new StringBuilder();
		for (TO<?> child : children) {
			childrenStringBuilder.append(fillObject(child));
		}
		if (!objectProperties.isEmpty() || childrenStringBuilder.length() > 0) {

			stringBuilder.append(format(OBJECT_HEADER, TOFactory.getInstance().saveTO(to)));
			stringBuilder.append(objectProperties);
			stringBuilder.append(childrenStringBuilder);
			stringBuilder.append(OBJECT_FOOTER);
		} else {
			stringBuilder.append(format(OBJECT_SELF_CONTAINED, TOFactory.getInstance().saveTO(to)));
		}
		return stringBuilder.toString();
	}

	private String fillObjectProperties(final TO<?> to, ObjectType objectType)
	        throws ConfigurationException {
		StringBuilder stringBuilder = new StringBuilder();
		Collection<ValueTO> properties = allObjectsProperties.get(to);
		for (ValueTO property : properties) {

			String propertyName = property.getPropertyName();
			long timeStamp = property.getTimeStamp();

			PropertyDescr propertyDescr = objectType.getPropertyDescriptor(propertyName);

			if (propertyDescr != null && propertyDescr.isHistorical() && !"lastValue".equalsIgnoreCase(propertyName)&& !"nextValue".equalsIgnoreCase(propertyName)) {
				// dm is not interested in historical properties except
				// lastValue, skip all of them
				continue;
			}

			Collection<Tag> tags = null;
			try {
				tags = env.getAllTags(to, propertyName);
			} catch (ObjectNotFoundException e) {
				logger.warn("Unable to find object with id " + to.toString()
				        + ", while getting tags. The property will be skipped.");
				continue;
			} catch (PropertyNotFoundException e) {
				logger.warn("Unable to find property " + propertyName + " for the object with id: " + to.toString()
				        + ", while getting tags. The property will be skipped.");
				continue;
			}
			String propertyValue = (property.getValue() != null ? property.getValue().toString() : "");

			if (tags != null && !tags.isEmpty()) {
				stringBuilder.append(format(PROPERTY_HEADER, propertyName, propertyValue));
				for (Tag tag : tags) {
					String tagValue = (tag.getValue() != null ? tag.getValue().toString() : "");
					stringBuilder.append(format(TAG_SELF_CONTAINED, tag.getTagName(), tagValue));
				}
				stringBuilder.append(PROPERTY_FOOTER);
			} else {
				stringBuilder.append(format(PROPERTY_SELF_CONTAINED, propertyName, propertyValue));
			}
		}
		return stringBuilder.toString();
	}

	private String format(String pattern, String... args) throws ConfigurationException {
		try {
			for (int i = 0; i < args.length; i++) {
				args[i] = escape(args[i]);
			}
		} catch (IOException e) {

			throw new ConfigurationException("Unable to escape charachter sequence: " + args);
		}
		return String.format(pattern, (Object[]) args);
	}

	private String escape(String string) throws IOException {
		StringBuffer strBuffer = new StringBuffer();
		for (int i = 0; i < string.length(); i++) {
			char ch = string.charAt(i);
			switch (ch) {
			case '"':
				strBuffer.append("&quot;");
				break;
			case '\'':
				strBuffer.append("&apos;");
				break;
			case '&':
				strBuffer.append("&amp;");
				break;
			case '<':
				strBuffer.append("&lt;");
				break;
			case '>':
				strBuffer.append("&gt;");
				break;
			default:
				strBuffer.append(ch);
				break;
			}
		}
		return strBuffer.toString();
	}

}
