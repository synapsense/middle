package com.synapsense.rulesengine;

import com.synapsense.cdi.MBean;
import com.synapsense.dal.generic.RuleInstanceDAO;
import com.synapsense.dal.objectcache.CacheEnvironmentDispatcher;
import com.synapsense.dal.objectcache.CacheSvcImpl;
import com.synapsense.dto.*;
import com.synapsense.exception.*;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.impl.taskscheduler.QuartzTaskSchedulerImplMBean;
import com.synapsense.rulesengine.core.environment.*;
import com.synapsense.rulesengine.core.environment.visitor.NotifyingVisitor;
import com.synapsense.service.AlertService;
import com.synapsense.service.Environment;
import com.synapsense.service.RuleTypeService;
import com.synapsense.service.RulesEngine;
import com.synapsense.service.impl.dao.to.PropertyTO;
import com.synapsense.service.nsvc.NotificationService;
import com.synapsense.service.nsvc.events.*;
import com.synapsense.util.LocalizationUtils;
import org.apache.log4j.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.*;
import java.util.*;

/**
 * Annotations replaced with jboss.xml config
 * 
 * @Service // JNDI name will be jboss.j2ee:service=EJB3,name=<Fully qualified
 *          name of
 * @Service bean>,type=service
 * @Remote(RulesEngine.class)
 * @Depends( { "jboss:service=Naming" })
 */
@Startup @Singleton @Remote(RulesEngine.class)
@DependsOn({"SynapCache"})
@ConcurrencyManagement(ConcurrencyManagementType.BEAN)
@MBean("com.synapsense:type=RulesEngineService")
public class RulesEngineService implements RulesEngineServiceMBean, RulesEngine, PropertiesChangedEventProcessor,
        ObjectDeletedEventProcessor {
	private final static Logger logger = Logger.getLogger(RulesEngineService.class);

	private static final String CRITICAL_ALERT = "Critical system alert";
	private static final String CIRDEP_ALERT_NAME = "alert_name_circular_dependency";
	private static final String CIRDEP_ALERT_MESSAGE = "alert_message_circular_dependency";

	// Environment without CRE checks
	private Environment cacheEnv;

	private Environment environment;
	private QuartzTaskSchedulerImplMBean scheduler;
	private AlertService alertService;
	private RuleTypeService<Integer> ruleTypeService;
	private RuleInstanceDAO<Integer> ruleInstanceDAO;
	private NotificationService notificationService;
	private DispatchingProcessor dispProcessor;

	private static boolean started = false;
	private long lastPropCalculationTime;

	private GraphManager graphManager;

	@Override
	@PostConstruct
	public synchronized void create() throws Exception {
		logger.info("RulesEngine - Creating");
		start();
	}

	@Override
	public void start() throws Exception {
		if (started) {
			logger.warn("Request to start CRE received but it is already started. No action will be performed");
			return;
		}

		logger.info("RulesEngine - Starting");

		// Build graph of property's relations
		if (logger.isDebugEnabled()) {
			logger.debug("Loading calculation graph...");
		}

		try {
			graphManager = new GraphManager(environment, ruleTypeService, ruleInstanceDAO, scheduler, alertService);
			graphManager.loadGraph();
		} catch (Exception e) {
			logger.info("Failed loading graph. " + e.getLocalizedMessage());
		}

		if (logger.isDebugEnabled()) {
			logger.debug("Loading calculation graph completed.");
		}

		try {
			// subscribe to events
			dispProcessor = new DispatchingProcessor().putPcep(this).putOdep(this);
			notificationService.registerProcessor(dispProcessor, null);
		} catch (Exception e) {
			logger.info("Failed registering for events. " + e.getLocalizedMessage());
		}

		started = true;
		logger.info("Started RulesEngine");
	}

	@Override
	@PreDestroy
	public void stop() throws Exception {
		if (!started) {
			logger.warn("Request to stop CRE received but it is already stopped. No action will be performed");
			return;
		}

		started = false;
		logger.info("RulesEngine - Stopping");

		// not a subscriber now
		notificationService.deregisterProcessor(dispProcessor);
		dispProcessor = null;

		// Unload graph
		graphManager.unloadGraph();
		graphManager = null;
		destroy();
	}

	@Override
	public void destroy() {
		logger.info("RulesEngine - Destroying");
	}

	@Override
	public Collection<RuleType> getRuleTypes() {
		checkService();
		return ruleTypeService.getAllRuleTypesDTOs();
	}

	@Override
	public TO<?> addRuleType(RuleType ruleType) throws ObjectExistsException, ConfigurationException {
		checkService();

		if (logger.isDebugEnabled()) {
			logger.debug("Creating rule type " + ruleType.getName());
		}

		// First check if we can instantiate it
		graphManager.loadRuleType(ruleType, true);

		// Save the new rule type
		return ruleTypeService.create(ruleType);
	}

	@Override
	public RuleType getRuleType(TO<?> ruleTypeId) {
		checkService();

		String name = null;
		try {
			Integer id = getObjectIntegerKey(ruleTypeId);
			if (id == null) {
				return null;
			}
			name = ruleTypeService.getName(id);
		} catch (ObjectNotFoundException e) {
			return null;
		}
		return ruleTypeService.getRuleTypeDTOByName(name);
	}

	@Override
	public void updateRuleType(TO<?> ruleTypeId, RuleType updated) throws ObjectNotFoundException,
	        ObjectExistsException, ConfigurationException {
		checkService();

		RuleType old = getRuleType(ruleTypeId);

		if (old == null) {
			throw new ObjectNotFoundException("Rule type with id " + ruleTypeId + " not found");
		}

		Integer id = getObjectIntegerKey(ruleTypeId);

		if (logger.isDebugEnabled()) {
			logger.debug("Updating rule type " + old.getName());
		}

		if (updated.getSource() != null && !updated.getSource().equals(old.getSource())) {
			if (!old.getSource().equals(updated.getSource())) {
				ruleTypeService.setSource(id, updated.getSource());
				ruleTypeService.setSourceType(id, updated.getSourceType());
				graphManager.loadRuleType(updated, true);
			}
			Collection<CRERule> rules = ruleInstanceDAO.getRulesDTOByType(old.getName());
			for (CRERule rule : rules) {
				graphManager.unloadRule(rule.getName());
				graphManager.loadRule(updated, rule);
			}
		}
		if ((updated.getRuleSchedulerConfig() != null && !updated.getRuleSchedulerConfig().equals(
		        old.getRuleSchedulerConfig()))
		        || (updated.getRuleSchedulerConfig() == null && old.getRuleSchedulerConfig() != null)) {
			ruleTypeService.setRuleRunConig(id, updated.getRuleSchedulerConfig());
		}
	}

	@Override
	public TO<?> getRuleTypeId(String name) {
		checkService();
		return ruleTypeService.getRuleTypeByName(name);
	}

	@Override
	public boolean removeRuleType(TO<?> ruleTypeId) {
		checkService();

		if (logger.isDebugEnabled()) {
			logger.debug("Removing rule type  " + ruleTypeId);
		}

		Integer id = getObjectIntegerKey(ruleTypeId);
		if (id == null) {
			return false;
		}

		Collection<TO<Integer>> rules;
		RuleType ruleType;
		try {
			ruleType = ruleTypeService.getRuleTypeDTOByName(ruleTypeService.getName(id));
			rules = ruleInstanceDAO.getRuleInstancesByType(ruleTypeService.getName(id));
		} catch (ObjectNotFoundException e) {
			return false;
		}

		try {
			// To delete this rule type from the graph
			graphManager.unloadRuleType(ruleType);
			// delete all the rules
			for (TO<Integer> rule : rules) {
				graphManager.unloadRule(rule.getID());
			}
			ruleTypeService.delete(id);
			return true;
		} catch (ConfigurationException e) {
			logger.error(e.getLocalizedMessage(), e);
			return false;
		}
	}

	@Override
	public Collection<CRERule> getRulesByType(TO<?> ruleType) throws ObjectNotFoundException {
		checkService();
		Integer id = getObjectIntegerKey(ruleType);
		if (id == null) {
			return new ArrayList<CRERule>();
		}
		return getRulesByType(ruleTypeService.getName(id));
	}

	@Override
	public Collection<CRERule> getRulesByType(String ruleTypeName) throws ObjectNotFoundException {
		checkService();
		if (ruleTypeName == null) {
			throw new IllegalArgumentException("ruleTypeName is null");
		}
		return ruleInstanceDAO.getRulesDTOByType(ruleTypeName);
	}

	@Override
	public Collection<CRERule> getRulesByObjectId(TO<?> objectId) {
		checkService();
		return ruleInstanceDAO.getRuleDTOsByHostObject(objectId);
	}

	@Override
	public TO<?> addRule(CRERule rule) throws ObjectExistsException, ObjectNotFoundException,
	        PropertyNotFoundException, ConfigurationException {
		checkService();

		if (logger.isDebugEnabled()) {
			logger.debug("Creating rule " + rule.getName());
		}

		TO<?> ruleTO = ruleInstanceDAO.create(rule);

		RuleType ruleType = getRuleType(rule.getRuleType());
		graphManager.loadRule(ruleType, rule);

		return ruleTO;
	}

	@Override
	public CRERule getRule(TO<?> ruleId) {
		checkService();

		Integer id = getObjectIntegerKey(ruleId);
		if (id == null) {
			return null;
		}

		return ruleInstanceDAO.getRuleDTOById(id);
	}

	@Override
	public TO<?> getRuleId(String name) {
		return ruleInstanceDAO.getRuleInstanceByName(name);
	}

	@Override
	public boolean removeRule(String name) {
		TO<?> ruleTO = getRuleId(name);
		if (ruleTO == null) {
			return false;
		}
		return removeRule(ruleTO);
	}

	@Override
	public boolean removeRule(TO<?> rule) {
		checkService();

		if (logger.isDebugEnabled()) {
			logger.debug("Removing rule " + rule);
		}

		Integer id = getObjectIntegerKey(rule);
		if (id == null) {
			return false;
		}
		try {
			graphManager.unloadRule(id);
		} catch (ConfigurationException e) {
			logger.error(e.getLocalizedMessage(), e);
			return false;
		}

		return ruleInstanceDAO.delete(id);
	}

	@Override
	public Result calculateProperty(TO<?> objId, String propName, Boolean onTimer) throws CalculationFailedException {
		try {
			Result res = new ResultImpl();// void result by default
			// If CRE is not started, no process required
			if (!started) {
				return res;
			}

			lastPropCalculationTime = System.currentTimeMillis();

			String uniqueName = GraphManager.getUniquePropertyName(objId, propName);
			Property reProperty = graphManager.getProperty(uniqueName);

			if (reProperty == null) {
				if (logger.isDebugEnabled()) {
					logger.debug("No RE Property '" + uniqueName + "' (type/id/prop) was registered. Native type="
					        + objId.toString() + ". Object Id= " + objId.getID() + ". Property name= " + propName);
				}
				return res;
			}

			if (reProperty.getCalcRule() == null) {
				if (logger.isDebugEnabled()) {
					logger.debug("Property " + uniqueName + " is not computational, nothing to calculate");
				}
				return res;
			}

			RuleI calcRule = reProperty.getCalcRule();

			if (!onTimer && calcRule.getSourceProperties().isEmpty()
			        && calcRule.getBehavior() == RuleType.RUN_ON_SCHEDULE) {
				// We recalculate such rules only by schedule
				return res;
			}

			// Trying to lock rule
			int lockResult = calcRule.lockRule();
			if (lockResult == RuleI.LOCK_ACKNOWLEDGED) {
				try {
					// Calculate the property
					res = new ResultImpl(graphManager.calculateProperty(reProperty));
					reProperty.setLastCalcTime(System.currentTimeMillis());

					if (logger.isDebugEnabled()) {
						if (res.getValue() != null) {
							logger.debug("CRE property '" + reProperty.getName()
							        + "' (type/id/prop) was calculated to " + res.getValue());
						} else {
							logger.debug("CRE property '" + reProperty.getName()
							        + "' was calculated to null. Check the rule '" + reProperty.getCalcRule().getName()
							        + "', probably it contains wrong code.");
						}
					}

					boolean persist = onTimer || !calcRule.isScheduled();

					if (logger.isDebugEnabled()) {
						if (persist) {
							logger.debug("CRE property '" + reProperty.getName()
							        + "' (type/id/prop): persisting value " + res.getValue());
						} else {
							logger.debug("CRE property '" + reProperty.getName()
							        + "' (type/id/prop) has schedule but was requested explicitly, not persisting");
						}
					}

					environment.setPropertyValue(objId, propName, res.getValue(), persist);
				} finally {
					calcRule.unlockRule();
				}
			} else {
				if (logger.isDebugEnabled()) {
					logger.debug("CRE property '" + reProperty.getName() + "' (type/id/prop) was not calculated");
				}

				// If cycle calls appeared during calculation raise an alert
				if (lockResult == RuleI.LOCK_REFUSED_CYCLE_DETECTED) {
					res = new ResultImpl(true);
					String fRuleName = reProperty.getCalcRule().getRuleAction().getClass().getName();
					Alert alert = new Alert(CIRDEP_ALERT_NAME, CRITICAL_ALERT, new Date(), LocalizationUtils.getLocalizedString(CIRDEP_ALERT_MESSAGE, fRuleName));
					alertService.raiseAlert(alert);
				}

				if (onTimer) {
					Object val = cacheEnv.getPropertyValue(objId, propName, Object.class);
					if (logger.isDebugEnabled()) {
						logger.debug("CRE property '" + reProperty.getName()
						        + "' (type/id/prop): persisting (on timer) request-updated value " + val);
					}
					environment.setPropertyValue(objId, propName, val);
				}
			}

			return res;
		} catch (Exception e) {
			logger.warn("An error occured during calculation", e);
			// here we loose the stack trace but get the ability to break
			// customized class loader's domain
			throw new CalculationFailedException("Cannot calculate property. Reason - " + e.getMessage());
		}
	}

	public boolean isCalculated(TO<?> objId, String propName) {
		if (!started) {
			return false;
		}

		Property REProperty = null;
		String uniqueName = GraphManager.getUniquePropertyName(objId, propName);
		REProperty = graphManager.getProperty(uniqueName);
		if (REProperty != null) {
			return (REProperty.getCalcRule() != null);
		} else if (logger.isDebugEnabled())
			logger.debug("No RE Property '" + uniqueName + "' (type/id/prop) was registered. Native type="
			        + objId.toString() + ". Object Id= " + objId.getID() + ". Property name= " + propName);
		return false;
	}

	public void recalculateAllProperties() {
		if (!started) {
			return;
		}

		Map<String, ? extends Property> calculated = graphManager.getCalculatedProperties();
		for (Property envProp : calculated.values()) {
			Property prop = (Property) envProp;
			DataSource ds = prop.getDataSource();
			try {
				calculateProperty(ds.getHostObjectTO(), ds.getNativePropertyName(), false);
			} catch (CalculationFailedException e) {
				logger.error("Error while periodical recalculating all properties", e);
			} catch (RuntimeException e) {
				logger.error("Error while periodical recalculating all properties", e);
			} catch (Exception e) {
				logger.error("Error while periodical recalculating all properties", e);
			}
		}
	}

	@Override
	public void calculateProperties(Collection<TO<?>> objIds, Set<String> propNames) {
		if (!started) {
			return;
		}

		long start = 0;
		if (logger.isTraceEnabled()) {
			start = System.currentTimeMillis();
		}

		for (TO<?> objId : objIds) {
			for (String propName : propNames) {
				try {
					calculateProperty(objId, propName, false);
				} catch (CalculationFailedException e) {
					logger.warn("Calculation of property " + propName + " for object " + objId.toString() + " failed",
					        e);
				}
			}
		}

		if (logger.isTraceEnabled()) {
			logger.trace("CRE.calculateProperties: " + (System.currentTimeMillis() - start) + " ms");
		}
	}

	@Override
	public void runDataRefresher() {
		recalculateAllProperties();
	}

	@Override
	public long getLastPropertyCalculationTime() {
		return lastPropCalculationTime;
	}

	@Override
	public PropertiesChangedEvent process(PropertiesChangedEvent e) {

		if (!started) {
			if (logger.isDebugEnabled()) {
				logger.debug("Notification on property value change received but CRE is stopped. No action to be performed");
			}
			return e;
		}

		TO<?> to = e.getObjectTO();
		for (ChangedValueTO cvTO : e.getChangedValues()) {
			if (notChanged(cvTO)) {
				continue;
			}
			String propName = cvTO.getPropertyName();

			if (logger.isDebugEnabled()) {
				logger.debug("CRE: Received ChangedValueTO notification "
				        + GraphManager.getUniquePropertyName(to, propName));
			}

			String uniqueName = GraphManager.getUniquePropertyName(to, propName);
			Property property = graphManager.getProperty(uniqueName);
			if (property == null) {
				continue;
			}
			RuleI rule = property.getCalcRule();
			if (rule == null || rule.getSourceProperties().isEmpty()) {
				property.accept(new NotifyingVisitor(Long.toString(System.currentTimeMillis())));
				try {
					PropertyDescr descr = environment.getPropertyDescriptor(to.getTypeName(), propName);
					if (descr != null
					        && (descr.getTypeName().equals(TO.class.getName()) || descr.getTypeName().equals(
					                PropertyTO.class.getName()))) {
						if (logger.isDebugEnabled()) {
							logger.debug("Updating links for property " + uniqueName);
						}
						graphManager.updateLink(to, propName, descr);
					} else if (descr != null) {
						if (logger.isDebugEnabled()) {
							logger.debug("Not a link description " + descr.getTypeName() + " " + descr.getType() + " "
							        + descr.isCollection());
						}
					}
				} catch (EnvException e1) {
					logger.error(e);
				}
			} else if (logger.isDebugEnabled())
				logger.debug("No RE Property '" + uniqueName + "' (type/id/prop) was registered. Native type="
				        + to.toString() + ". Object Id= " + to.getID() + ". Property name= " + propName);
		}
		return e;
	}

	@Override
	public ObjectDeletedEvent process(ObjectDeletedEvent event) {
		TO<?> objId = event.getObjectTO();
		try {
			graphManager.unloadRulesByObjId(objId);
		} catch (ConfigurationException e) {
			logger.error(e.getLocalizedMessage(), e);
		}
		return event;
	}

	@EJB
	public void setScheduler(QuartzTaskSchedulerImplMBean scheduler) {
		this.scheduler = scheduler;
	}

	@EJB
	public void setAlertService(AlertService alertService) {
		this.alertService = alertService;
	}

	@EJB(beanName="LocalNotificationService")
	public void setNotificationService(NotificationService notificationService) {
		this.notificationService = notificationService;
	}

	public Environment getEnvironment() {
		return environment;
	}

	@EJB(beanName="LocalEnvironment")
	public void setEnvironment(Environment environment) {
		this.cacheEnv = CacheEnvironmentDispatcher.getEnvironment(CacheSvcImpl.getInstance(), environment);
		this.environment = new CREEnvironmentDispatcher(cacheEnv, this);
	}

	public RuleTypeService<?> getRuleTypeDAO() {
		return ruleTypeService;
	}

	@EJB
	public void setRuleTypeDAO(RuleTypeService<Integer> ruleTypeDAO) {
		this.ruleTypeService = ruleTypeDAO;
	}

	public RuleInstanceDAO<?> getRuleInstanceDAO() {
		return ruleInstanceDAO;
	}

	@EJB
	public void setRuleInstanceDAO(RuleInstanceDAO<Integer> ruleInstanceDAO) {
		this.ruleInstanceDAO = ruleInstanceDAO;
	}

	private void checkService() {
		if (!started) {
			throw new IllegalStateException("CRE is not running. It must be started to perform any operations.");
		}
	}

	private Integer getObjectIntegerKey(TO<?> to) {
		Object id = to.getID();
		if (id.getClass() != Integer.class) {
			return null;
		}
		return (Integer) id;
	}

	private static boolean notChanged(ChangedValueTO chValTo) {
		if (chValTo.getOldValue() == null && chValTo.getValue() == null) {
			return true;
		}
		if (chValTo.getOldValue() == null || chValTo.getValue() == null) {
			return false;
		}
		return chValTo.getOldValue().equals(chValTo.getValue());
	}

	@Override
	public String listGraphBranch(String type, Integer id, String propertyName) {
		String name = GraphManager.getUniquePropertyName(TOFactory.getInstance().loadTO(type + ":" + id), propertyName);
		Property prop = graphManager.getProperty(name);
		if (prop == null) {
			return "";
		}
		return recursiveGraphBranch(prop);
	}

	private String recursiveGraphBranch(Property prop) {
		StringBuilder builder = new StringBuilder("[").append(prop.getName());
		RuleI ruleI = prop.getCalcRule();
		if (ruleI != null) {
			builder.append("-").append(ruleI.getName()).append("\n");
			if (!ruleI.getSourceProperties().isEmpty()) {
				builder.append("\n");
			}
			for (Property src : ruleI.getSourceProperties()) {
				builder.append(recursiveGraphBranch(src));
			}
		} else {
			for (Property src : prop.getSourceProperties()) {
				builder.append(recursiveGraphBranch(src));
			}
		}
		builder.append("]").append("\n");
		return builder.toString();
	}
}
