package com.synapsense.rulesengine.core.environment;

import com.synapsense.dto.TO;
import com.synapsense.rulesengine.core.environment.exception.CalculationException;
import com.synapsense.rulesengine.core.environment.exception.DataSourceException;

/**
 * The interface for retrieving data from the storage.
 * 
 * @author alperep
 * 
 */
public interface DataSource {

	/**
	 * Value of the property
	 * 
	 * @return value or null if no value is set
	 * @throws DataSourceException
	 */
	Object getValue() throws DataSourceException;

	/**
	 * Timestamp of the last time when the value has been calculated.
	 * 
	 * @return
	 * @throws DataSourceException
	 */
	long getTimestamp() throws DataSourceException;

	/**
	 * 
	 * @param value
	 * @throws CalculationException
	 *             - when calculations caused by changing this property leaded
	 *             to error
	 */
	void setValue(Object value) throws DataSourceException;

	void setTimestamp(long timestamp);

	TO<?> getHostObjectTO();

	String getNativePropertyName();

	Class<?> getEnvPropType();

}
