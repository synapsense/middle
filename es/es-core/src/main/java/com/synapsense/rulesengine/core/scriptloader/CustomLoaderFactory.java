/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.synapsense.rulesengine.core.scriptloader;

import java.util.HashMap;
import java.util.Map;

/**
 * Container for the various types of script loader classes.
 */
public class CustomLoaderFactory {

	private static Map<String, CustomClassLoaderCover> loaders;

	static {
		CustomClassLoaderCover gsl = new CustomClassLoaderCover(new CustomGroovyClassLoader());
		CustomClassLoaderCover jsl = new CustomClassLoaderCover(new CustomJavaClassLoader());

		loaders = new HashMap<String, CustomClassLoaderCover>();
		loaders.put(gsl.getType(), gsl);
		loaders.put(jsl.getType(), jsl);
	}

	public static CustomClassLoaderCover getCustomLoader(String scriptType) {
		CustomClassLoaderCover sl = loaders.get(scriptType);
		if (sl == null) {
			throw new IllegalArgumentException("Unsupported script type: '" + scriptType + "'");
		}
		return sl;
	}
}
