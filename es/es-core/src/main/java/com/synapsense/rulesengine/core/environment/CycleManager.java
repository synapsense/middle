package com.synapsense.rulesengine.core.environment;

import java.util.Collection;

import com.synapsense.exception.ConfigurationException;

public class CycleManager {
	/**
	 * Decides what to do with cycles.
	 * 
	 * @param cycles
	 * @throws ConfigurationException
	 */
	public static String decide(Collection<Cycle> cycles) throws ConfigurationException {
		StringBuilder msg = new StringBuilder("");
		msg.append("Cycles inside the graph are detected! (" + cycles.size() + ")\n");
		int i = 0;
		for (Cycle cycle : cycles) {
			msg.append("[" + ++i + "]: " + cycle.explainCycle() + "\n");
		}
		return msg.toString();
	}
}
