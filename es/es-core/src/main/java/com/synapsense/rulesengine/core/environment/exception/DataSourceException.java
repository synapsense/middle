package com.synapsense.rulesengine.core.environment.exception;

public class DataSourceException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = -1960139738518283223L;

	public DataSourceException() {
		super();
	}

	public DataSourceException(String message) {
		super(message);
	}

	public DataSourceException(String message, Throwable cause) {
		super(message, cause);
	}

	public DataSourceException(Throwable cause) {
		super(cause);
	}
}
