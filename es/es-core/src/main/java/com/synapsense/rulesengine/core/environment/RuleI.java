/**
 * 
 */
package com.synapsense.rulesengine.core.environment;

import java.util.Collection;

import com.synapsense.exception.ConfigurationException;
import com.synapsense.rulesengine.core.environment.visitor.AbstractVisitor;

/**
 * The Rule for the processing the property. Rule decides when the property must
 * be recalculated basing on the timestamp of the passed property and timestamps
 * of the source property. If one of the sources' timestamps is newer than
 * passed one, then the passed property will be recalculated; otherwise, not.
 * That is applied recursively.
 */
public interface RuleI {

	/**
	 * Sets that the rule must be recalculated during next processing
	 * 
	 * @return false if the rule was already not up to date
	 */
	boolean invalidate();

	public static final int LOCK_ACKNOWLEDGED = 0;
	public static final int LOCK_REFUSED = 1;
	public static final int LOCK_REFUSED_CYCLE_DETECTED = 2;

	/**
	 * When starting processing the rule it must be locked in order to prevent
	 * other processors from working with it. If it is necessary to recalculate
	 * the rule the method locks the rule and returns 0. If the rule is already
	 * locked by another process the current thread waits until it is free and
	 * returns 1. If the rule is already being processed by this thread it
	 * returns 2. If the the rule is free and up to date the method returns 1.
	 * 
	 * @returns the result of acknowledgement attempt
	 */
	int lockRule();

	/**
	 * Releases the rule. Must be called only if <code>lockRule()</code>
	 * returned true
	 */
	void unlockRule();

	/**
	 * @return true if this rule has scheduled trigger
	 */
	boolean isScheduled();

	void accept(AbstractVisitor visitor);

	RuleAction getRuleAction();

	void setRuleAction(RuleAction ruleAction);

	void addSourceProperty(Property sourceProperty) throws ConfigurationException;

	void setDependantProperty(Property dependantProperty);

	String getName();

	Collection<Property> getSourceProperties();

	Property getDependantProperty();

	int getBehavior();
}
