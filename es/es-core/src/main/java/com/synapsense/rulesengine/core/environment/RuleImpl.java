package com.synapsense.rulesengine.core.environment;

import java.util.Collection;
import java.util.LinkedList;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.log4j.Logger;

import com.synapsense.rulesengine.core.environment.visitor.AbstractVisitor;

/**
 * Implementation class of the environment rule.
 * 
 * @author alperep
 * 
 */
class RuleImpl implements RuleI {

	private final static Logger logger = Logger.getLogger(RuleImpl.class);

	/**
	 * Name
	 */
	protected String name;

	/**
	 * The HashMap of properties which are used to calculate this rule. HashMap
	 * is for clone() method.
	 */
	protected LinkedList<Property> sourceProperties = new LinkedList<Property>();

	private Property dependantProperty;

	private RuleAction ruleAction;

	/**
	 * If true the rule is up to date and it is not necessary to recalculate it
	 * 
	 * @default false
	 */
	private AtomicBoolean upToDate = new AtomicBoolean();

	/**
	 * The id of the TO passed from Env layer
	 */
	// private Object transferObjId;
	/**
	 * The ordinal number in depth-first forest.
	 */
	public int ordinal = 0;

	private boolean scheduled;

	private int behavior;

	public RuleImpl() {
	}

	@Override
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public Collection<Property> getSourceProperties() {
		return sourceProperties;
	}

	public void addSourceProperty(Property sourceProperty) {
		sourceProperties.add(sourceProperty);
	}

	public void setDependantProperty(Property dependantProperty) {
		this.dependantProperty = dependantProperty;
	}

	@Override
	public void accept(AbstractVisitor visitor) {
		// tree traversal is provided by the visitor
		visitor.visitRule(this);
	}

	@Override
	public boolean invalidate() {
		boolean invalidated = upToDate.compareAndSet(true, false);
		if (logger.isTraceEnabled()) {
			if (invalidated) {
				logger.trace("Property " + dependantProperty + " invalidated");
			} else {
				logger.trace("Property " + dependantProperty + " already invalid");
			}
		}
		return invalidated;
	}

	private volatile long lockId = 0;

	public synchronized int lockRule() {

		if (logger.isDebugEnabled()) {
			logger.debug("Thread " + Thread.currentThread().getId() + " tries to acknowledge the lock on the rule "
			        + this.getName());
		}

		boolean upToDate = !this.upToDate.compareAndSet(false, true);

		if (upToDate && !sourceProperties.isEmpty() && lockId == 0) {
			if (logger.isDebugEnabled()) {
				logger.debug("Rule " + getName() + " is up to date");
			}
			return LOCK_REFUSED;
		}

		if (lockId > 0) {
			if (lockId == Thread.currentThread().getId()) {
				if (logger.isDebugEnabled()) {
					logger.debug("Rule is being calculated by the same " + Thread.currentThread().getId() + " already.");
				}
				return LOCK_REFUSED_CYCLE_DETECTED;
			}
			while (lockId > 0) {
				if (logger.isDebugEnabled()) {
					logger.debug("Thread " + Thread.currentThread().getId() + " waiting for rule to be calculated");
				}
				try {
					wait();
				} catch (InterruptedException e) {
					logger.warn("Thread " + Thread.currentThread().getName() + " interrupted on rule " + name);
				}
			}
			return LOCK_REFUSED;
		}
		if (logger.isDebugEnabled()) {
			logger.debug("Thread " + Thread.currentThread().getId() + " has acknowledged the lock on the rule "
			        + this.getName() + " successfuly");
		}
		lockId = Thread.currentThread().getId();
		return LOCK_ACKNOWLEDGED;
	}

	public synchronized void unlockRule() {
		if (logger.isDebugEnabled()) {
			logger.debug("Thread " + Thread.currentThread().getId() + " released the lock on the rule " + getName());
		}
		lockId = 0;
		notifyAll();
	}

	@Override
	public String toString() {
		return getName();
	}

	public void setScheduled(boolean scheduled) {
		this.scheduled = scheduled;
	}

	@Override
	public boolean isScheduled() {
		return scheduled;
	}

	@Override
	public RuleAction getRuleAction() {
		return ruleAction;
	}

	@Override
	public void setRuleAction(RuleAction ruleAction) {
		this.ruleAction = ruleAction;
	}

	@Override
	public Property getDependantProperty() {
		return dependantProperty;
	}

	@Override
	public int getBehavior() {
		return behavior;
	}

	public void setBehavior(int behavior) {
		this.behavior = behavior;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RuleImpl other = (RuleImpl) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
}
