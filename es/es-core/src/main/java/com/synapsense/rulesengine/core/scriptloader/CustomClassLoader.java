package com.synapsense.rulesengine.core.scriptloader;

/**
 * Defines the custom loader interface. Allows to load, unload classes of
 * different source types.
 * 
 * @author alperep
 * 
 */
interface CustomClassLoader {
	/**
	 * Allow to check has JVM already loaded the class or not yet
	 * 
	 * @param name
	 * @return
	 */
	Class<?> findLoadedClass0(String name);

	/**
	 * Return new classLoader of the same type. Needed when it's time to load
	 * class which already has been loaded by another loader.
	 * 
	 * @return
	 */
	CustomClassLoader renew();

	/**
	 * Load class w/o compilation
	 * 
	 * @param name
	 * @return
	 */
	Class<?> loadClass(String name, boolean resolve) throws ClassNotFoundException;

	/**
	 * Compile script and load class
	 * 
	 * @param name
	 * @param contents
	 * @return
	 */
	Class<?> loadClass(String name, String source, boolean resolve) throws ClassNotFoundException;

	String getType();

}