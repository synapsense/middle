package com.synapsense.rulesengine.core.environment;

import java.util.Collection;
import java.util.Collections;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.log4j.Logger;

import com.synapsense.exception.EnvException;
import com.synapsense.rulesengine.core.environment.exception.CalculationException;
import com.synapsense.rulesengine.core.environment.exception.DataSourceException;
import com.synapsense.rulesengine.core.environment.visitor.AbstractVisitor;
import com.synapsense.service.AlertService;
import com.synapsense.service.Environment;

/**
 * Implementation class of the environment property. Definitions: 1. Property
 * depends on the single rule. That rule is called each time when property is
 * asked for the actual value 2. Rule depends on one or more properties. Each of
 * those properties may change its value. Some properties which update their
 * value permanently (not upon extern request), need TO<?> have a feedback when
 * any of their predecessors are changed. The feedback is provided through the
 * 'propagating' rules. In such a way every property has TO<?> maintain the list
 * of rules which must be notified when the property gets updated.
 */
class PropertyImpl implements Property {
	private final static Logger logger = Logger.getLogger(PropertyImpl.class);

	/**
	 * Name
	 */
	protected String name;

	/**
	 * The rule which is used TO<?> calculate self value.
	 */
	protected RuleI calcRule;

	/**
	 * The rules which are dependent on this property, i.e. use the value of
	 * this property in calculations.
	 */
	protected CopyOnWriteArrayList<RuleI> dependantRules = new CopyOnWriteArrayList<RuleI>();

	/**
	 * Properties which depend on this one through the collection links.
	 */
	protected CopyOnWriteArrayList<Property> dependantProperties = new CopyOnWriteArrayList<Property>();

	protected CopyOnWriteArrayList<Property> sourceProperties = new CopyOnWriteArrayList<Property>();

	/**
	 * The supplier/consumer of the data. Datasources might be switched in order
	 * to allow CE work in different environments.
	 */
	protected DataSourceEnv dataSource;

	/**
	 * The ordinal number in depth-first forest.
	 */
	public int ordinal = 0;

	/**
	 * last calculation time
	 */
	private long lastCalcTime = 0;

	/**
	 * last timer-triggered calculation time
	 */
	private long lastTriggeredCalcTime = 0;

	public PropertyImpl(String name, DataSourceEnv ds) {
		this.name = name;
		this.dataSource = ds;
	}

	@Override
	public Object getValue() throws EnvException {
		try {
			return dataSource.getValue();
		} catch (DataSourceException e) {
			throw new EnvException(e);
		}
	}

	/**
	 * Set the value of this property. Synchronized method, other calls TO<?>
	 * setValue on this property are blocked until notification will be thrown.
	 * Notification is thrown TO<?> all upstairs properties/rules
	 * 
	 * Must be called ONLY by internal calculations, not an external interface
	 * 
	 * @throws CalculationException
	 */
	@Override
	public void setValue(Object value) {
		try {
			// if null was calculated prints debug message
			if (value == null) {
				if (logger.isDebugEnabled()) {
					logger.debug("The property '" + name + "' is calculated to null. Check the rule '"
					        + calcRule.getName() + "', probably it contains wrong code.");
				}
			}
			dataSource.setValue(value);
			lastCalcTime = System.currentTimeMillis();
		} catch (DataSourceException e) {
			logger.error(e.getLocalizedMessage(), e);
		}
	}

	private AtomicInteger aa = new AtomicInteger(0);

	public int getCounter() {
		return aa.get();
	}

	public void setCounter(int i) {
		aa.set(i);
	}

	public String toString() {
		return getName();
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public long getTimestamp() {
		return lastCalcTime;
	}

	public RuleI getCalcRule() {
		return calcRule;
	}

	public Collection<RuleI> getDependantRules() {
		return dependantRules;
	}

	public void putDependantRule(RuleI dependantRule) {
		dependantRules.add(dependantRule);
	}

	public void setCalcRule(RuleI calcRule) {
		this.calcRule = calcRule;
	}

	@Override
	public void accept(AbstractVisitor visitor) {
		// tree traversal is provided by the visitor
		visitor.visitProperty(this);
	}

	public DataSourceEnv getDataSource() {
		return dataSource;
	}

	void setDataSource(DataSourceEnv dataSource) {
		this.dataSource = dataSource;
	}

	public Environment getEnvironment() {
		return DataSourceEnv.getEnvironment();
	}

	@Override
	public long getTimerTimestamp() {
		return lastTriggeredCalcTime;
	}

	@Override
	public void setTimerTimestamp(long ts) {
		lastTriggeredCalcTime = ts;
	}

	@Override
	public void addDependantProperty(Property prop) {
		dependantProperties.add(prop);
	}

	@Override
	public void removeDependantProperty(Property prop) {
		dependantProperties.remove(prop.getName());
	}

	public Collection<Property> getDependantProperties() {
		return Collections.unmodifiableCollection(dependantProperties);
	}

	@Override
	public void addSourceProperty(Property prop) {
		sourceProperties.add(prop);
	}

	@Override
	public void removeSourceProperty(Property prop) {
		sourceProperties.remove(prop.getName());
	}

	@Override
	public Collection<Property> getSourceProperties() {
		return Collections.unmodifiableCollection(sourceProperties);
	}

	public void setLastCalcTime(long lastCalcTime) {
		this.lastCalcTime = lastCalcTime;
	}

	@Override
	public void removeDependantRule(RuleI rule) {
		dependantRules.remove(rule);
	}

	@Override
	public void removeSourceProperties() {
		sourceProperties.clear();
	}

	@Override
	public AlertService getAlertService() {
		return DataSourceEnv.getAlertService();
	}
}
