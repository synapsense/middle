package com.synapsense.rulesengine;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.dto.CollectionTO;
import com.synapsense.dto.EnvObjectBundle;
import com.synapsense.dto.EnvObjectHistory;
import com.synapsense.dto.ObjectType;
import com.synapsense.dto.ObjectTypeMatchMode;
import com.synapsense.dto.PropertyDescr;
import com.synapsense.dto.Relation;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOBundle;
import com.synapsense.dto.Tag;
import com.synapsense.dto.TagDescriptor;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.CalculationFailedException;
import com.synapsense.exception.EnvException;
import com.synapsense.exception.IllegalInputParameterException;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.exception.PropertyNotFoundException;
import com.synapsense.exception.ServerConfigurationException;
import com.synapsense.exception.TagNotFoundException;
import com.synapsense.exception.UnableToConvertPropertyException;
import com.synapsense.exception.UnableToConvertTagException;
import com.synapsense.exception.UnsupportedRelationException;
import com.synapsense.impl.utils.EnvironmentUtils;
import com.synapsense.search.QueryDescription;
import com.synapsense.service.Environment;
import com.synapsense.service.RulesEngine;

/**
 * 
 * This class decorates some environment implementation. It adds calculation of
 * the property requested in the getPropertyValue method if it is necessary. The
 * purpose of creating this class comes from performance optimizations that was
 * done to decrease the number of ejb calls inside the server, especially from
 * com.synapsense.dal.objectcache.CacheEnvironmentDispatcher class that always
 * reads values from cache without any checks. The instance of this class is
 * used only inside RulesEngineService and passed to the calculation rules as a
 * datasource. It is a temporary consideration.
 * 
 * @author dgrudzin
 * 
 */
public class CREEnvironmentDispatcher implements Environment {

	private Environment impl;
	private RulesEngine rulesEngine;

	private static final Log logger = LogFactory.getLog(CREEnvironmentDispatcher.class);

	public CREEnvironmentDispatcher(Environment impl, RulesEngine rulesEngine) {
		this.impl = impl;
		this.rulesEngine = rulesEngine;
	}

	public <REQUESTED_TYPE> void addPropertyValue(TO<?> objId, String propName, REQUESTED_TYPE value)
	        throws ObjectNotFoundException, PropertyNotFoundException, UnableToConvertPropertyException {
		impl.addPropertyValue(objId, propName, value);
	}

	@Override
	public void configurationComplete() throws ServerConfigurationException {
		impl.configurationComplete();
	}

	@Override
	public void configurationStart() throws ServerConfigurationException {
		impl.configurationStart();
	}

	@Override
	public TO<?> createObject(String typeName) throws EnvException {
		return impl.createObject(typeName);
	}

	@Override
	public TO<?> createObject(String typeName, ValueTO[] propertyValues) throws EnvException {
		return impl.createObject(typeName, propertyValues);
	}

	public TOBundle createObjects(EnvObjectBundle objectsToCreate) throws EnvException {
		return impl.createObjects(objectsToCreate);
	}

	public ObjectType createObjectType(String name) throws EnvException {
		return impl.createObjectType(name);
	}

	public void deleteObject(TO<?> objId) throws ObjectNotFoundException {
		impl.deleteObject(objId);
	}

	public void deleteObject(TO<?> objId, int level) throws ObjectNotFoundException {
		impl.deleteObject(objId, level);
	}

	public void deleteObjectType(String typeName) throws EnvException {
		impl.deleteObjectType(typeName);
	}

	public boolean exists(TO<?> objId) throws IllegalInputParameterException {
		return impl.exists(objId);
	}

	public Collection<CollectionTO> getAllPropertiesValues(Collection<TO<?>> objIds) {
		rulesEngine.calculateProperties(
		        objIds,
		        new HashSet<String>(Arrays.asList(EnvironmentUtils.getPropNamesForTypeName(getObjectType(objIds
		                .iterator().next().getTypeName())))));
		return impl.getAllPropertiesValues(objIds);
	}

	public Collection<ValueTO> getAllPropertiesValues(TO<?> objId) throws ObjectNotFoundException {
		Collection<TO<?>> coll = new ArrayList<TO<?>>();
		coll.add(objId);
		rulesEngine.calculateProperties(
		        coll,
		        new HashSet<String>(Arrays.asList(EnvironmentUtils.getPropNamesForTypeName(getObjectType(objId
		                .getTypeName())))));
		return impl.getAllPropertiesValues(objId);
	}

	public Collection<CollectionTO> getChildren(Collection<TO<?>> objIds, String typeName) {
		return impl.getChildren(objIds, typeName);
	}

	public Collection<CollectionTO> getChildren(Collection<TO<?>> objIds) {
		return impl.getChildren(objIds);
	}

	public Collection<TO<?>> getChildren(TO<?> objId, String typeName) throws ObjectNotFoundException {
		return impl.getChildren(objId, typeName);
	}

	public Collection<TO<?>> getChildren(TO<?> objId) throws ObjectNotFoundException {
		return impl.getChildren(objId);
	}

	public <RETURN_TYPE> Collection<ValueTO> getHistory(TO<?> objId, String propName, Class<RETURN_TYPE> clazz)
	        throws ObjectNotFoundException, UnableToConvertPropertyException, PropertyNotFoundException {
		return impl.getHistory(objId, propName, clazz);
	}

	public <RETURN_TYPE> Collection<ValueTO> getHistory(TO<?> objId, String propName, Date start, Date end,
	        Class<RETURN_TYPE> clazz) throws ObjectNotFoundException, PropertyNotFoundException,
	        UnableToConvertPropertyException {
		return impl.getHistory(objId, propName, start, end, clazz);
	}

	public EnvObjectHistory getHistory(TO<?> objId, Collection<String> propNames, Date start, Date end)
	        throws ObjectNotFoundException, PropertyNotFoundException {
		return impl.getHistory(objId, propNames, start, end);
	}

	public <RETURN_TYPE> Collection<ValueTO> getHistory(TO<?> objId, String propName, Date start, int numberPoints,
	        Class<RETURN_TYPE> clazz) throws ObjectNotFoundException, PropertyNotFoundException,
	        UnableToConvertPropertyException {
		return impl.getHistory(objId, propName, start, numberPoints, clazz);
	}

	public <RETURN_TYPE> Collection<ValueTO> getHistory(TO<?> objId, String propName, int size, int start,
	        Class<RETURN_TYPE> clazz) throws ObjectNotFoundException, PropertyNotFoundException,
	        UnableToConvertPropertyException {
		return impl.getHistory(objId, propName, size, start, clazz);
	}

	public Integer getHistorySize(TO<?> objId, String propName) throws ObjectNotFoundException,
	        PropertyNotFoundException {
		return impl.getHistorySize(objId, propName);
	}

	public Collection<TO<?>> getObjects(String typeName, ObjectTypeMatchMode matchMode, ValueTO[] propertyValues) {
		return impl.getObjects(typeName, matchMode, propertyValues);
	}

	public Collection<TO<?>> getObjects(String typeName, ValueTO[] propertyValues) {
		return impl.getObjects(typeName, propertyValues);
	}

	public Collection<TO<?>> getObjects(QueryDescription queryDescr) {
		return impl.getObjects(queryDescr);
	}

	public Collection<TO<?>> getObjectsByType(String typeName, ObjectTypeMatchMode matchMode) {
		return impl.getObjectsByType(typeName, matchMode);
	}

	public Collection<TO<?>> getObjectsByType(String typeName) {
		return impl.getObjectsByType(typeName);
	}

	public ObjectType getObjectType(String name) {
		return impl.getObjectType(name);
	}

	public String[] getObjectTypes() {
		return impl.getObjectTypes();
	}

	public ObjectType[] getObjectTypes(String[] names) {
		return impl.getObjectTypes(names);
	}

	public Collection<TO<?>> getParents(TO<?> objId, String typeName) throws ObjectNotFoundException {
		return impl.getParents(objId, typeName);
	}

	public Collection<TO<?>> getParents(TO<?> objId) throws ObjectNotFoundException {
		return impl.getParents(objId);
	}

	public PropertyDescr getPropertyDescriptor(String typeName, String propName) throws ObjectNotFoundException,
	        PropertyNotFoundException {
		return impl.getPropertyDescriptor(typeName, propName);
	}

	public long getPropertyTimestamp(TO<?> objId, String propName) throws ObjectNotFoundException,
	        PropertyNotFoundException {
		return impl.getPropertyTimestamp(objId, propName);
	}

	public Collection<CollectionTO> getPropertyValue(Collection<TO<?>> objIds, String[] propNames) {
		rulesEngine.calculateProperties(objIds, new HashSet<String>(Arrays.asList(propNames)));
		return impl.getPropertyValue(objIds, propNames);
	}

	public <RETURN_TYPE> RETURN_TYPE getPropertyValue(String typeName, String propName, Class<RETURN_TYPE> clazz)
	        throws ObjectNotFoundException, PropertyNotFoundException, UnableToConvertPropertyException {
		return impl.getPropertyValue(typeName, propName, clazz);
	}

	public <RETURN_TYPE> RETURN_TYPE getPropertyValue(TO<?> objId, String propName, Class<RETURN_TYPE> clazz)
	        throws ObjectNotFoundException, PropertyNotFoundException, UnableToConvertPropertyException {
		try {
			// This call will recalculate the property value if it is obsolete
			rulesEngine.calculateProperty(objId, propName, false);
		} catch (CalculationFailedException e) {
			logger.warn("Calculation of property " + propName + " for object " + objId.toString() + " failed", e);
		}

		return impl.getPropertyValue(objId, propName, clazz);
	}

	public Collection<TO<?>> getRelatedObjects(TO<?> objId, String objectType, boolean isChild) {
		return impl.getRelatedObjects(objId, objectType, isChild);
	}

	public <REQUESTED_TYPE> void removePropertyValue(TO<?> objId, String propName, REQUESTED_TYPE value)
	        throws ObjectNotFoundException, PropertyNotFoundException, UnableToConvertPropertyException {
		impl.removePropertyValue(objId, propName, value);
	}

	public void removeRelation(TO<?> parentId, TO<?> childId) throws ObjectNotFoundException {
		impl.removeRelation(parentId, childId);
	}

	public void removeRelation(List<Relation> relations) throws ObjectNotFoundException {
		impl.removeRelation(relations);
	}

	public void setAllPropertiesValues(TO<?> objId, ValueTO[] values, boolean persist) throws ObjectNotFoundException,
	        PropertyNotFoundException, UnableToConvertPropertyException {
		impl.setAllPropertiesValues(objId, values, persist);
	}

	public void setAllPropertiesValues(TO<?> objId, Collection<ValueTO> values, Collection<Tag> tags)
	        throws EnvException {
		impl.setAllPropertiesValues(objId, values, tags);
	}

	public void setAllPropertiesValues(TO<?> objId, ValueTO[] values) throws ObjectNotFoundException,
	        PropertyNotFoundException, UnableToConvertPropertyException {
		impl.setAllPropertiesValues(objId, values);
	}

	public <REQUESTED_TYPE> void setPropertyValue(String typeName, String propName, REQUESTED_TYPE value)
	        throws ObjectNotFoundException, PropertyNotFoundException, UnableToConvertPropertyException {
		impl.setPropertyValue(typeName, propName, value);
	}

	public <REQUESTED_TYPE> void setPropertyValue(TO<?> objId, String propName, REQUESTED_TYPE value, boolean persist)
	        throws PropertyNotFoundException, ObjectNotFoundException, UnableToConvertPropertyException {
		impl.setPropertyValue(objId, propName, value, persist);
	}

	public <REQUESTED_TYPE> void setPropertyValue(TO<?> objId, String propName, REQUESTED_TYPE value)
	        throws ObjectNotFoundException, PropertyNotFoundException, UnableToConvertPropertyException {
		impl.setPropertyValue(objId, propName, value);
	}

	public void setRelation(TO<?> parentId, TO<?> childId) throws ObjectNotFoundException, UnsupportedRelationException {
		impl.setRelation(parentId, childId);
	}

	public void setRelation(List<Relation> relations) throws ObjectNotFoundException, UnsupportedRelationException {
		impl.setRelation(relations);
	}

	public void updateObjectType(ObjectType objType) throws EnvException {
		impl.updateObjectType(objType);
	}

	@Override
	public TagDescriptor getTagDescriptor(String typeName, String propName, String tagName)
	        throws ObjectNotFoundException, PropertyNotFoundException, TagNotFoundException {
		return impl.getTagDescriptor(typeName, propName, tagName);
	}

	@Override
	public <RETURN_TYPE> RETURN_TYPE getTagValue(TO<?> objId, String propName, String tagName, Class<RETURN_TYPE> clazz)
	        throws ObjectNotFoundException, PropertyNotFoundException, TagNotFoundException,
	        UnableToConvertTagException {
		return impl.getTagValue(objId, propName, tagName, clazz);
	}

	@Override
	public Collection<Tag> getAllTags(TO<?> objId, String propName) throws ObjectNotFoundException,
	        PropertyNotFoundException {
		return impl.getAllTags(objId, propName);
	}

	@Override
	public Collection<Tag> getAllTags(TO<?> objId) throws ObjectNotFoundException {
		return impl.getAllTags(objId);
	}

	@Override
	public Map<TO<?>, Collection<Tag>> getAllTags(String typeName) {
		return impl.getAllTags(typeName);
	}

	@Override
	public void setTagValue(TO<?> objId, String propName, String tagName, Object value)
	        throws PropertyNotFoundException, ObjectNotFoundException, TagNotFoundException,
	        UnableToConvertTagException {
		impl.setTagValue(objId, propName, tagName, value);
	}

	@Override
	public Map<TO<?>, Map<String, Collection<Tag>>> getTags(Collection<TO<?>> objIds, List<String> propertyNames,
	        List<String> tagNames) {
		return impl.getTags(objIds, propertyNames, tagNames);
	}

	@Override
	public void setAllTagsValues(TO<?> objId, Collection<Tag> tagsToSet) throws PropertyNotFoundException,
	        ObjectNotFoundException, TagNotFoundException, UnableToConvertTagException {
		impl.setAllTagsValues(objId, tagsToSet);
	}


}
