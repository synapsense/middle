package com.synapsense.rulesengine.core.environment.visitor;

import java.util.ArrayList;
import java.util.List;

import com.synapsense.rulesengine.core.environment.Property;
import com.synapsense.rulesengine.core.environment.RuleI;

/**
 * Container for the list of other visitors.
 * 
 * @author alperep
 * 
 */
public class CompositeVisitor extends AbstractVisitor {

	public List<AbstractVisitor> visitors = new ArrayList<AbstractVisitor>();

	public void addVisitor(AbstractVisitor visitor) {
		visitors.add(visitor);
	}

	@Override
	public void visitRule(RuleI rule) {
		for (AbstractVisitor visitor : visitors) {
			visitor.visitRule(rule);
		}

	}

	@Override
	public void visitProperty(Property property) {
		for (AbstractVisitor visitor : visitors) {
			visitor.visitProperty(property);
		}
	}

}
