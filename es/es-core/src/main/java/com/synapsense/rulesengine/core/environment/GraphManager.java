package com.synapsense.rulesengine.core.environment;

import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import com.synapsense.dal.generic.RuleInstanceDAO;
import com.synapsense.dto.CRERule;
import com.synapsense.dto.CRERule.PropertyBinding;
import com.synapsense.dto.PropertyDescr;
import com.synapsense.dto.RuleType;
import com.synapsense.dto.TO;
import com.synapsense.exception.ConfigurationException;
import com.synapsense.exception.EnvException;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.exception.PropertyNotFoundException;
import com.synapsense.impl.taskscheduler.QuartzTaskSchedulerImplMBean;
import com.synapsense.impl.taskscheduler.TaskSchedulerException;
import com.synapsense.rulesengine.RulesEngineService;
import com.synapsense.rulesengine.core.scriptloader.CustomLoaderFactory;
import com.synapsense.service.AlertService;
import com.synapsense.service.Environment;
import com.synapsense.service.RuleTypeService;
import com.synapsense.service.impl.dao.to.PropertyTO;

public class GraphManager {

	private final static Logger logger = Logger.getLogger(GraphManager.class);

	private RuleTypeService<Integer> ruleTypeService;

	private RuleInstanceDAO<Integer> ruleInstanceDAO;

	private Environment environment;

	private QuartzTaskSchedulerImplMBean scheduler;

	private Graph graph;

	private ConcurrentHashMap<RuleType, RuleAction> ruleActionMap = new ConcurrentHashMap<RuleType, RuleAction>();

	private HashMap<TO<?>, LinkedList<String>> objectRuleMap = new HashMap<TO<?>, LinkedList<String>>();

	public static final String getUniquePropertyName(TO<?> trObj, String propName) {
		return new StringBuilder().append(trObj.getTypeName()).append("/").append(trObj.getID()).append("/")
		        .append(propName).toString();
	}

	public GraphManager(Environment environment, RuleTypeService<Integer> ruleTypeDAO,
	        RuleInstanceDAO<Integer> ruleInstanceDAO, QuartzTaskSchedulerImplMBean scheduler, AlertService alertService)
	        throws ConfigurationException {
		this.ruleTypeService = ruleTypeDAO;
		this.ruleInstanceDAO = ruleInstanceDAO;
		this.environment = environment;
		this.scheduler = scheduler;
		DataSourceEnv.setEnvironment(environment);
		DataSourceEnv.setAlertService(alertService);
		loadRuleTypes();
	}

	public void loadGraph() throws ConfigurationException {
		if (logger.isDebugEnabled()) {
			logger.debug("Starting building RE Graph...");
		}
		graph = new Graph();
		Collection<RuleType> ruleTypes = ruleTypeService.getAllRuleTypesDTOs();
		for (RuleType ruleType : ruleTypes) {
			Collection<CRERule> rules = ruleInstanceDAO.getRulesDTOByType(ruleType.getName());
			for (CRERule rule : rules) {
				loadRule(ruleType, rule);
			}
		}

		if (logger.isDebugEnabled()) {
			logger.debug("Finished building RE Graph");
		}
	}

	public Property getProperty(String name) {
		return graph.getProperty(name);
	}

	public Map<String, ? extends Property> getCalculatedProperties() {
		return graph.getCalculatedProperties();
	}

	public synchronized void loadRuleType(RuleType ruleType, boolean reload) throws ConfigurationException {

		String name = ruleType.getName();
		String actionBody = ruleType.getSource();
		String actionType = ruleType.getSourceType();
		String actionClass = ruleType.getName();
		Boolean precompiled = ruleType.isPrecompiled();

		if (logger.isDebugEnabled()) {
			logger.debug("Loading '" + actionType + "', '" + actionClass + "', rule: " + name + ", body: " + actionBody);
		}

		Class<?> actionClazz = null; // the loaded class
		try {
			if (!reload) {
				actionClazz = (precompiled) ? CustomLoaderFactory.getCustomLoader(actionType).loadClass(actionClass,
				        true) : CustomLoaderFactory.getCustomLoader(actionType)
				        .loadClass(actionClass, actionBody, true);
			} else {
				actionClazz = (precompiled) ? CustomLoaderFactory.getCustomLoader(actionType).reloadClass(actionClass,
				        true) : CustomLoaderFactory.getCustomLoader(actionType).reloadClass(actionClass, actionBody,
				        true);
			}
		} catch (ClassNotFoundException ce) {
			throw new ConfigurationException("Rule {type='" + actionType + "', class='" + actionClass + "', name='"
			        + name + "': can not instantiate the rule action! ", ce);
		} catch (Exception e) {
			throw new ConfigurationException("Rule {type='" + actionType + "', class='" + actionClass + "', name='"
			        + name + "': can not instantiate the rule action! - " + e.getMessage());
		}

		Object ruleActionInst;
		try {
			ruleActionInst = actionClazz.newInstance();
		} catch (InstantiationException e) {
			throw new ConfigurationException("Rule '" + actionClazz + "': can not instantiate the rule action! ", e);
		} catch (IllegalAccessException e) {
			throw new ConfigurationException("Rule '" + actionClazz + "': can not instantiate the rule action! ", e);
		}

		if (logger.isDebugEnabled()) {
			logger.debug("Rule template '" + actionClass + "/" + actionType + "' loaded successfully");
		}

		RuleAction ruleAction = (RuleAction) ruleActionInst;
		ruleActionMap.put(ruleType, ruleAction);
	}

	public synchronized void unloadRuleType(RuleType ruleType) {
		ruleActionMap.remove(ruleType);
	}

	public synchronized void loadRule(RuleType ruleType, CRERule rule) throws ConfigurationException {
		RuleImpl ruleInstance = new RuleImpl();
		try {
			ruleInstance.setName(rule.getName());
			ruleInstance.setBehavior(ruleType.getMethodBehavior());
			RuleAction template = ruleActionMap.get(ruleType);
			try {
				ruleInstance.setRuleAction(template.getClass().newInstance());
			} catch (InstantiationException e) {
				throw new ConfigurationException("Error when trying to instantiate rule action " + template.getClass(),
				        e);
			} catch (IllegalAccessException e) {
				throw new ConfigurationException("Error when trying to instantiate rule action " + template.getClass(),
				        e);
			}
			if (!graph.putRule(ruleInstance)) {
				throw new ConfigurationException("Rule with the name " + ruleInstance.getName() + " is duplicating!");
			}
			if (logger.isDebugEnabled()) {
				logger.debug("Building RE Graph: Reading bindings for " + ruleInstance.getName());
			}
			processSourceProperties(rule);
			processTargetProperty(rule);

			String trigger = ruleType.getRuleSchedulerConfig();
			if (trigger != null && !trigger.isEmpty()) {
				ruleInstance.setScheduled(true);
				scheduleRule(rule.getName(), trigger, rule.getDestObjId(), rule.getDestPropertyName());
			}
			addObjectRuleEntry(rule.getDestObjId(), rule.getName());
		} catch (EnvException e) {
			// shouldn't happen
			throw new ConfigurationException(e);
		}

	}

	public synchronized void unloadRule(String name) throws ConfigurationException {
		try {
			// find appropriate RE rule
			RuleI currentRule = graph.getRule(name);
			if (currentRule == null) {
				// Rule is not in the graph
				return;
			}
			graph.removeRule(currentRule);

			Property dep = currentRule.getDependantProperty();
			dep.setCalcRule(null);
			recursivePropertyRemove(dep);

			Collection<Property> srcProps = currentRule.getSourceProperties();
			for (Property prop : srcProps) {
				prop.removeDependantRule(currentRule);
				recursivePropertyRemove(prop);
			}

			scheduler.removeTask(name);
			removeObjectRuleEntry(name);
		} catch (TaskSchedulerException e) {
			throw new ConfigurationException(e);
		}
	}

	public synchronized void unloadRule(Integer ruleId) throws ConfigurationException {
		try {
			unloadRule(ruleInstanceDAO.getName(ruleId));
		} catch (EnvException e) {
			throw new ConfigurationException(e);
		}
	}

	public synchronized void unloadRulesByObjId(TO<?> objId) throws ConfigurationException {
		LinkedList<String> ruleNames = objectRuleMap.get(objId);
		if (ruleNames == null) {
			return;
		}

		// Need a copy to avoid concurrent modification
		ArrayList<String> rules = new ArrayList<String>(ruleNames);
		for (String ruleName : rules) {
			unloadRule(ruleName);
		}
	}

	public synchronized void updateLink(TO<?> objId, String propName, PropertyDescr descr) {
		try {
			String uniquePropName = getUniquePropertyName(objId, propName);
			Property prop = graph.getProperty(uniquePropName);
			if (prop == null) {
				return;
			}

			if (logger.isDebugEnabled()) {
				logger.debug("Updating link " + prop.getName());
			}

			for (Property src : prop.getSourceProperties()) {
				src.removeDependantProperty(prop);
				recursivePropertyRemove(src);
			}

			prop.removeSourceProperties();
			processPropertyLink(objId, prop, descr);
		} catch (Exception e) {
			logger.error("Unable to update property link " + objId + ":" + propName, e);
		}
	}

	public synchronized void unloadGraph() {
		if (logger.isDebugEnabled()) {
			logger.debug("Destroying CRE Graph...");
		}

		// clear references to the RuleAction classes from rule instances
		for (RuleI rule : graph.getRules().values()) {
			try {
				scheduler.removeTask(rule.getName());
			} catch (TaskSchedulerException e) {
				logger.error("Unable to unload rule " + rule.getName() + "from the scheduler");
			}
		}

		for (RuleType ruleTemplate : ruleActionMap.keySet()) {
			try {
				CustomLoaderFactory.getCustomLoader(ruleTemplate.getSourceType()).unloadClass(ruleTemplate.getName());
			} catch (ClassNotFoundException e) {
				logger.error("Unable to unload rule " + ruleTemplate.getName() + "... Lost it?");
			}
		}

		// clear references to the RuleAction classes from the map
		ruleActionMap.clear();

		graph.clear();
		graph = null;
		if (logger.isDebugEnabled()) {
			logger.debug("Finished destroying RE Graph");
		}
	}

	public Object calculateProperty(Property prop) {
		RuleI calcRule = prop.getCalcRule();
		if (calcRule != null) {
			return calcRule.getRuleAction().run(calcRule, prop);
		}
		return null;
	}

	public void detectCycles() throws ConfigurationException {
		Collection<Cycle> cycles = graph.detectCycles();
		if (!cycles.isEmpty()) {
			logger.error(CycleManager.decide(cycles));
			throw new ConfigurationException("CRE graph contains cycles!");
		}
	}

	private void recursivePropertyRemove(Property prop) {
		if (prop.getDependantRules().isEmpty() && prop.getDependantProperties().isEmpty() && prop.getCalcRule() == null) {
			graph.removeProperty(prop.getName());
			Collection<Property> linkedProps = prop.getSourceProperties();
			for (Property link : linkedProps) {
				link.removeDependantProperty(prop);
				recursivePropertyRemove(link);
			}
		}
	}

	private void scheduleRule(String ruleName, String triggerExpression, TO<?> objId, String propName)
	        throws EnvException {
		if (triggerExpression == null || triggerExpression.isEmpty()) {
			logger.warn("Null or empty cron expression is specified for rule " + ruleName
			        + "; the rule is not scheduled");
			return;
		}

		LinkedHashMap<Class<?>, Object> params = new LinkedHashMap<Class<?>, Object>();
		params.put(TO.class, objId);
		params.put(String.class, propName);
		params.put(Boolean.class, true);

		try {
			scheduler.addEjbTask(ruleName, triggerExpression, "java:global/es-ear/es-core/RulesEngineService",
			        RulesEngineService.class.getName(), "calculateProperty", params);
		} catch (TaskSchedulerException e) {
			logger.warn("Cannot schedule rule " + ruleName, e);
		}
	}

	private void processTargetProperty(CRERule ruleDTO) throws EnvException, ConfigurationException {
		RuleI rule = graph.getRule(ruleDTO.getName());
		String uniquePropName = getUniquePropertyName(ruleDTO.getDestObjId(), ruleDTO.getDestPropertyName());
		PropertyImpl depProp = (PropertyImpl) graph.getProperty(uniquePropName);
		if (depProp == null) {
			PropertyDescr pd = environment.getPropertyDescriptor(ruleDTO.getDestObjId().getTypeName(),
			        ruleDTO.getDestPropertyName());
			depProp = new PropertyImpl(uniquePropName, new DataSourceEnv(ruleDTO.getDestObjId(),
			        ruleDTO.getDestPropertyName(), pd.getType()));
			graph.putProperty(depProp);
		}
		rule.setDependantProperty(depProp);
		depProp.setCalcRule(rule);
	}

	private void processSourceProperties(CRERule ruleDTO) throws EnvException, ConfigurationException {
		RuleI rule = graph.getRule(ruleDTO.getName());
		Collection<PropertyBinding> props = ruleDTO.getSrcProperties();

		for (PropertyBinding pB : props) {
			String uniquePropName = getUniquePropertyName(pB.getObjId(), pB.getPropName());
			Property srcProp = graph.getProperty(uniquePropName);

			PropertyDescr descr = environment.getPropertyDescriptor(pB.getObjId().getTypeName(), pB.getPropName());

			if (srcProp == null) {
				srcProp = new PropertyImpl(uniquePropName, new DataSourceEnv(pB.getObjId(), pB.getPropName(),
				        descr.getType()));
				graph.putProperty(srcProp);
			}

			rule.addSourceProperty(srcProp);
			srcProp.putDependantRule(rule);

			// Process links
			try {
				processPropertyLink(pB.getObjId(), srcProp, descr);
			} catch (Exception e) {
				logger.error("Unable to process property links of property " + srcProp.getName() + " of rule "
				        + ruleDTO.getName() + ". Reason: ", e);
			}
			if (logger.isDebugEnabled()) {
				logger.debug("setupBean('" + pB.getPropBinding() + "' to '" + srcProp.getName() + "' for rule '"
				        + rule.getName() + "'");
			}
			// set up bean so it will refer to RE Properties
			setupBean(pB.getPropBinding(), srcProp, rule.getRuleAction());
		}
	}

	private void processPropertyLink(TO<?> objId, Property prop, PropertyDescr descr) throws Exception {
		if (descr.isCollection()) {
			if (logger.isDebugEnabled()) {
				logger.debug("Processing collection link " + prop.getName());
			}
			Object val = environment.getPropertyValue(objId, descr.getName(), Collection.class);
			if (Class.forName(descr.getTypeName()).equals(TO.class)) {
				@SuppressWarnings("unchecked")
				Collection<TO<?>> referencedObjs = (Collection<TO<?>>) val;
				if (referencedObjs != null) {
					for (TO<?> refObj : referencedObjs) {
						loadObjectReference(objId, descr.getName(), refObj);
					}
				}
			} else if (Class.forName(descr.getTypeName()).equals(PropertyTO.class)) {
				@SuppressWarnings("unchecked")
				Collection<PropertyTO> referencedProps = (Collection<PropertyTO>) val;
				if (referencedProps != null) {
					for (PropertyTO refProp : referencedProps) {
						loadPropertyReference(objId, descr.getName(), refProp);
					}
				}
			}
		} else if (descr.getType().equals(TO.class)) {
			TO<?> refObj = environment.getPropertyValue(objId, descr.getName(), TO.class);
			loadObjectReference(objId, descr.getName(), refObj);
		} else if (descr.getType().equals(PropertyTO.class)) {
			PropertyTO refProp = environment.getPropertyValue(objId, descr.getName(), PropertyTO.class);
			loadPropertyReference(objId, descr.getName(), refProp);
		}
	}

	private void setupBean(String fieldName, Object fieldValue, Object beanStorage) throws ConfigurationException {
		try {
			BeanInfo info = Introspector.getBeanInfo(beanStorage.getClass());
			boolean beanFieldFound = false;
			for (PropertyDescriptor pd : info.getPropertyDescriptors()) {
				if (pd.getName().equals(fieldName)) {
					// setter found
					logger.debug("Bean property '" + pd.getName() + "' is being set to value '" + fieldValue
					        + "' in object " + beanStorage); // Introspector
					                                         // debug
					beanFieldFound = true;
					Method writeMethod = pd.getWriteMethod();
					Method readMethod = pd.getReadMethod();
					if (readMethod != null) {
						Object alreadyKept = readMethod.invoke(beanStorage);
						if (alreadyKept != null) {
							throw new ConfigurationException("Bean property '" + pd.getName()
							        + "' is already set to value '" + alreadyKept + "' in object " + beanStorage);
						}
					}
					writeMethod.invoke(beanStorage, fieldValue);
					break;
				}
			}
			if (!beanFieldFound) {
				throw new ConfigurationException("Bean field's '" + fieldName + "' setter is not defined for class '"
				        + beanStorage.getClass() + "'");
			}
		} catch (Exception e) {
			throw new ConfigurationException("Error while setting the '" + fieldName + "' field of class '"
			        + beanStorage.getClass() + "' to the '" + fieldValue + "' (of type "
			        + fieldValue.getClass().getSimpleName() + ")", e);
		}
	}

	private void loadRuleTypes() throws ConfigurationException {
		Collection<RuleType> ruleTypes = ruleTypeService.getAllRuleTypesDTOs();
		for (RuleType ruleType : ruleTypes) {
			loadRuleType(ruleType, false);
		}
	}

	private void loadPropertyReference(TO<?> referencingObject, String referencingProperty, PropertyTO propTo)
	        throws ObjectNotFoundException, PropertyNotFoundException {
		if (propTo == null) {
			return;
		}
		PropertyDescr descr = environment.getPropertyDescriptor(propTo.getObjId().getTypeName(),
		        propTo.getPropertyName());
		if (descr != null) {
			loadReference(referencingObject, referencingProperty, propTo.getObjId(), descr.getName(), descr.getType());
		}
	}

	private void loadObjectReference(TO<?> referencingObject, String referencingProperty, TO<?> referencedObject) {
		if (referencedObject == null) {
			return;
		}
		Set<PropertyDescr> props = environment.getObjectType(referencedObject.getTypeName()).getPropertyDescriptors();
		for (PropertyDescr descr : props) {
			loadReference(referencingObject, referencingProperty, referencedObject, descr.getName(), descr.getType());
		}
	}

	private void loadReference(TO<?> referencingObject, String referencingProperty, TO<?> referencedObject,
	        String referencedProperty, Class<?> propClass) {
		// get referencing CRE property
		Property refProperty = graph.getProperty(getUniquePropertyName(referencingObject, referencingProperty));
		if (refProperty == null)
			return;

		// get referenced CRE properties
		String propName = getUniquePropertyName(referencedObject, referencedProperty);
		Property prop = graph.getProperty(propName);
		if (prop == null) {
			prop = new PropertyImpl(propName, new DataSourceEnv(referencedObject, referencedProperty, propClass));
			graph.putProperty(prop);
		}
		if (logger.isDebugEnabled()) {
			logger.debug("CRE: adding dependency: " + prop.getName() + "->" + refProperty.getName());
		}
		prop.addDependantProperty(refProperty);
		refProperty.addSourceProperty(prop);
	}

	private void addObjectRuleEntry(TO<?> objId, String ruleName) {
		LinkedList<String> ruleNames = objectRuleMap.get(objId);
		if (ruleNames == null) {
			ruleNames = new LinkedList<String>();
			objectRuleMap.put(objId, ruleNames);
		}
		ruleNames.add(ruleName);
	}

	private void removeObjectRuleEntry(String ruleName) {
		for (LinkedList<String> list : objectRuleMap.values()) {
			if (list.remove(ruleName)) {
				return;
			}
		}
	}
}
