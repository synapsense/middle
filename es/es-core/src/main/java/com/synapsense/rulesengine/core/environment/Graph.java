package com.synapsense.rulesengine.core.environment;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Environment graph. Contains map of: property name - property rule name - rule
 * 
 * Singleton.
 * 
 * @author alperep
 * 
 */
public final class Graph {

	public Graph() {
		super();
	}

	/**
	 * The HashMap of all properties in the environment graph. Filled in at
	 * graph's instantiation time.
	 */
	private Map<String, Property> allProperties = new ConcurrentHashMap<String, Property>();

	/**
	 * The HashMap of all rules in the environment graph. Filled in at graph's
	 * instantiation time.
	 */
	private Map<String, RuleI> allRules = new ConcurrentHashMap<String, RuleI>();

	/**
	 * Check whether the graph is empty.
	 * 
	 * @return
	 */
	public boolean isEmpty() {
		return allProperties.isEmpty() && allRules.isEmpty();
	}

	/**
	 * Return the property by its name
	 * 
	 * @param name
	 */
	public Property getProperty(String name) {
		return allProperties.get(name);

	}

	/**
	 * Put the rule to the set. Return null if there was no rule with such name
	 * or previous Rule if it was.
	 * 
	 * @param rule
	 * @return
	 */
	public boolean putRule(RuleI rule) {
		if (allRules.containsKey(rule.getName())) {
			return false;
		}
		allRules.put(rule.getName(), rule);
		return true;
	}

	/**
	 * Return the rule by its name
	 * 
	 * @param name
	 */
	public RuleI getRule(String name) {
		return allRules.get(name);
	}

	/**
	 * Put the property to the set. Return null if there was no property with
	 * such name or previous Property if it was.
	 * 
	 * @param prop
	 * @return
	 */
	public Property putProperty(Property prop) {
		return allProperties.put(prop.getName(), prop);
	}

	/**
	 * Return the unmodifiable map of the properties
	 * 
	 * @return
	 */
	public Map<String, Property> getProperties() {

		return java.util.Collections.unmodifiableMap(allProperties);
		// return (HashMap<String, Property>)allProperties.clone();
	}

	/**
	 * Return the unmodifiable map of the rules
	 * 
	 * @return
	 */
	public Map<String, RuleI> getRules() {
		return java.util.Collections.unmodifiableMap(allRules);
		// return (HashMap<String, Rule>)allRules.clone();
	}

	/**
	 * Detects if there are cycles within the graph.
	 */
	public Collection<Cycle> detectCycles() {
		Collection<Cycle> cycles = new HashSet<Cycle>();

		for (Property property : allProperties.values()) {
			if (property.getCalcRule() == null && property.getSourceProperties().isEmpty()) {
				CycleDetectorVisitor detector = new CycleDetectorVisitor();
				property.accept(detector);
				cycles.addAll(detector.getCycles());
			}
		}
		return cycles;
	}

	public void clear() {
		allProperties.clear();
		allRules.clear();
	}

	protected RuleI removeRule(RuleI rule) {
		return allRules.remove(rule.getName());
	}

	public Property removeProperty(String name) {
		return allProperties.remove(name);
	}

	public Map<String, ? extends Property> getCalculatedProperties() {
		HashMap<String, Property> calculated = new HashMap<String, Property>();
		for (Property prop : allProperties.values()) {
			if (prop.getCalcRule() != null) {
				calculated.put(prop.getName(), prop);
			}
		}
		return java.util.Collections.unmodifiableMap(calculated);
	}
}
