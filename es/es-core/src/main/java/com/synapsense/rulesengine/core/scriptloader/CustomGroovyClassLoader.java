package com.synapsense.rulesengine.core.scriptloader;

import groovy.lang.GroovyClassLoader;

/**
 * The auxiliary classloader which is used as spare resource and replaced with
 * the new instance when request on reloading of the class arrives.
 * 
 * @author alperep
 * @source http://www.javalobby.org/java/forums/t18345.html
 */
public class CustomGroovyClassLoader extends GroovyClassLoader implements CustomClassLoader {

	public CustomGroovyClassLoader() {
		super(Thread.currentThread().getContextClassLoader());
	}

	/**
	 * This is the central method which is the reason for creating that class.
	 * It allows to ask JVM if it already has the definition of the class with
	 * that name.
	 * 
	 * Returns the class with the given <a href="#name">binary name</a> if this
	 * loader has been recorded by the Java virtual machine as an initiating
	 * loader of a class with that <a href="#name">binary name</a>. Otherwise
	 * <tt>null</tt> is returned. </p>
	 */
	@Override
	public final Class<?> findLoadedClass0(String name) {
		return findLoadedClass(name);
	}

	@Override
	public CustomClassLoader renew() {
		return new CustomGroovyClassLoader();
	}

	@Override
	public Class<?> loadClass(String name, boolean resolve) throws ClassNotFoundException {
		return super.loadClass(name, resolve);
	}

	@Override
	public Class<?> loadClass(String name, String source, boolean resolve) throws ClassNotFoundException {
		Class<?> classToLoad = this.parseClass(source, name + ".groovy");

		return classToLoad;
	}

	@Override
	public String getType() {
		return "Groovy";
	}
}
