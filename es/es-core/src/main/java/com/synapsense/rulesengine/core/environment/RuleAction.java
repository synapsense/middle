package com.synapsense.rulesengine.core.environment;

import java.io.Serializable;

/**
 * The implementing classes are responsible for running calculations, making
 * actions, and anything else.
 */
public interface RuleAction extends Serializable {

	/**
	 * The interface method is called when recalculation is required.
	 * 
	 * @param triggeredRule
	 *            - the Rule which triggered that action.
	 * @param calculated
	 *            - the Property which will be set to the result of this
	 *            calculation
	 * @return the result of calculations; the result will be set to the
	 *         property which asked for recalculation
	 */
	Object run(final RuleI triggeredRule, final Property calculated);

}
