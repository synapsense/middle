package com.synapsense.rulesengine.core.environment;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.rulesengine.core.environment.visitor.AbstractVisitor;

public class CycleDetectorVisitor extends AbstractVisitor {

	private static Log logger = LogFactory.getLog(CycleDetectorVisitor.class);

	private List<String> route = new ArrayList<String>();

	private Set<String> visitedProperties = new HashSet<String>();

	private Collection<Cycle> cycles = new ArrayList<Cycle>();

	@Override
	public void visitProperty(Property property) {
		if (logger.isDebugEnabled()) {
			logger.debug("Processing property " + property.getName());
		}
		int index = route.lastIndexOf(property.getName());
		if (index != -1) {
			if (logger.isDebugEnabled()) {
				logger.debug("The property is a finish point of the cycle " + property.getName());
			}
			cycles.add(new Cycle(route.subList(index, route.size())));
			return;
		}
		if (!visitedProperties.add(property.getName())) {
			if (logger.isDebugEnabled()) {
				logger.debug("Property " + property.getName() + " has been processed already");
			}
			return;
		}
		route.add(property.getName());

		for (RuleI rule : property.getDependantRules()) {
			rule.accept(this);
		}

		for (Property prop : property.getDependantProperties()) {
			prop.accept(this);
		}
		if (logger.isDebugEnabled()) {
			logger.debug("Removing property from stack " + property.getName());
		}
		route.remove(route.size() - 1);
	}

	@Override
	public void visitRule(RuleI rule) {
		if (logger.isDebugEnabled()) {
			logger.debug("Processing rule " + rule.getName());
		}
		rule.getDependantProperty().accept(this);
	}

	public Collection<Cycle> getCycles() {
		return cycles;
	}
}
