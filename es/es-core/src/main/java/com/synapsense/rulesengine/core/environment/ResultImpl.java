package com.synapsense.rulesengine.core.environment;

import com.synapsense.service.RulesEngine.Result;

public class ResultImpl implements Result {

	private static final long serialVersionUID = 350646899277941689L;

	private final Object value;
	private final boolean calculated;
	private final boolean cycleDetected;

	public ResultImpl() {
		this.value = null;
		this.calculated = false;
		this.cycleDetected = false;
	}

	public ResultImpl(Object value) {
		this.value = value;
		this.calculated = true;
		this.cycleDetected = false;
	}

	public ResultImpl(boolean cycleDetected) {
		this.value = null;
		this.calculated = false;
		this.cycleDetected = cycleDetected;
	}

	public Object getValue() {
		return value;
	}

	public boolean isCalculated() {
		return calculated;
	}

	public boolean isCycleDetected() {
		return cycleDetected;
	}
}
