package com.synapsense.rulesengine.core.scriptloader;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

/**
 * The ClassLoader which is able to 'reload' classes on demand, i.e. load the
 * updated class with the same name but different content. Covers actual loaders
 * which can be replaced with new instances in case of necessity.
 * 
 * @author alperep
 * 
 */
public class CustomClassLoaderCover extends ClassLoader {

	private final static Logger logger = Logger.getLogger(CustomClassLoaderCover.class);

	CustomClassLoader loader;

	/**
	 * Classes which were loaded already
	 */
	private Map<String, Class<?>> loadedClasses = new HashMap<String, Class<?>>();

	static {
		String compilationDir = System.getProperty("user.dir");
		if (!compilationDir.endsWith(File.separator)) {
			compilationDir = compilationDir + File.separator;
		}
		System.setProperty("javacompilation.dir", compilationDir);
	}

	public CustomClassLoaderCover(CustomClassLoader loader) {
		super(Thread.currentThread().getContextClassLoader());
		this.loader = loader;
	}

	public Class<?> reloadClass(String name, boolean resolve) throws ClassNotFoundException {
		return reloadClass(name, null, resolve);
	}

	/**
	 * Unloads previously loaded class if any and loads new version.
	 * 
	 * @param name
	 * @return
	 * @throws ClassNotFoundException
	 *             if unable to load new class. Old class is not unloaded in
	 *             that case.
	 */
	public Class<?> reloadClass(String name, String contents, boolean resolve) throws ClassNotFoundException {
		Class<?> c = loadedClasses.get(name);

		byte serializedClass[] = null;

		if (c != null) {

			try {
				// serialize it to keep old version
				ByteArrayOutputStream byteOutStream = new ByteArrayOutputStream(1024);
				ObjectOutputStream objOutStream = new ObjectOutputStream(byteOutStream);
				objOutStream.writeObject(c);
				objOutStream.close();
				serializedClass = byteOutStream.toByteArray();
			} catch (IOException e) {
				logger.error("Serialization error..." + e);
			}

			loadedClasses.remove(name);
			c = null;
		}

		try {
			// this takes care of possible Linkage Error due to redefinition of
			// the same class
			if (contents != null) {
				c = loadClass(name, contents, resolve);
			} else {
				c = loadClass(name, resolve);
			}

		} catch (ClassNotFoundException ce) {

			try {
				// deserialize old one
				ByteArrayInputStream byteInStream = new ByteArrayInputStream(serializedClass);
				ObjectInputStream objInStream = new ObjectInputStream(byteInStream);
				c = (Class<?>) objInStream.readObject();
				objInStream.close();
			} catch (IOException e) {
				logger.error("Serialization error..." + e);
			}

			// put old class back
			loadedClasses.put(name, c);
			// throw exception again
			throw ce;
		}

		// put the new class
		loadedClasses.put(name, c);
		return c;
	}

	/**
	 * Removes the class from this ClassLoader so it will not be loaded from the
	 * cache later when the ClassLoader is asked to load the class with the same
	 * name.
	 * 
	 * @param className
	 * @throws ClassNotFoundException
	 *             if there were no class with such name loaded.
	 */
	public void unloadClass(String name) throws ClassNotFoundException {
		if (loadedClasses.remove(name) == null) {
			throw new ClassNotFoundException(name);
		}
	}

	/**
	 * Due to existence of field Vector ClassLoader.classes which keeps strong
	 * references to all classes loaded through this classloader we can not
	 * reload class with the same name through the same classloader! Thus we are
	 * forced to change classloader each time when we want to clearly reload
	 * class
	 */
	@Override
	public Class<?> loadClass(String name, boolean resolve) throws ClassNotFoundException {
		Class<?> c = null;

		// check to see if loaded already by this loader (and was not unloaded)
		c = (Class<?>) loadedClasses.get(name);
		if (c != null) {
			return c;
		}

		if (loader.findLoadedClass0(name) != null) {
			// it's time to change the horse... (class was unloaded from our
			// impl)
			loader = loader.renew();
		}

		c = loader.loadClass(name, resolve);

		loadedClasses.put(name, c);

		return c;
	}

	/**
	 * Copy of loadClass(String name, boolean resolve), but plus contents
	 * 
	 * @param name
	 * @param contents
	 * @param resolve
	 * @return
	 * @throws ClassNotFoundException
	 */
	public Class<?> loadClass(String name, String contents, boolean resolve) throws ClassNotFoundException {
		Class<?> c = null;

		// check to see if loaded already by this loader (and was not unloaded)
		c = (Class<?>) loadedClasses.get(name);
		if (c != null) {
			return c;
		}

		if (loader.findLoadedClass0(name) != null) {
			// it's time to change the horse... (class was unloaded from our
			// impl)
			loader = loader.renew();
		}

		c = loader.loadClass(name, contents, resolve);

		loadedClasses.put(name, c);

		return c;
	}

	public String getType() {
		return loader.getType();
	}

}

/**
 * Small wrapper which is using tools.jar for invoking javac
 * 
 * @author alperep
 * 
 */
class JavaCompiler {

	String workingDir;
	private final static Logger logger = Logger.getLogger(JavaCompiler.class);

	/**
	 * Return compiled file.
	 * 
	 * @param name
	 * @param src
	 * @return
	 */
	File compile(String name, String src) {

		File res = null;

		FileWriter fw = null;
		File sourceFile = new File(JavaCompiler.getSourceFileAbsolutePath(name));
		File classFile = new File(JavaCompiler.getClassFileAbsolutePath(name));
		try {

			String compileDir = System.getProperty("javacompilation.dir");
			// we must create directory in which to place source and compiled
			// files to
			File containingDir = new File(sourceFile.getCanonicalFile().getParent());
			if (!containingDir.exists() && !containingDir.mkdirs()) {
				logger.error("Unable to create directory " + containingDir.getAbsolutePath());
			}
			// write source
			fw = new FileWriter(sourceFile);
			fw.write(src);
			fw.flush();
			fw.close();

			// compile it
			StringWriter errWriter = new StringWriter();
			PrintWriter pw = new PrintWriter(errWriter);
			String[] args = new String[] { "-d", compileDir,
			        // "-sourcepath", compilationCatalog,
			        sourceFile.getCanonicalPath() };
			int compileReturnCode = com.sun.tools.javac.Main.compile(args, pw);
			pw.flush();
			// errors?
			if (logger.isDebugEnabled()) {
				logger.debug(errWriter.toString());
			}
			pw.close();
			if (compileReturnCode == 0) {
				res = classFile;
			}
		} catch (IOException e) {
			logger.error("Error compiling file " + sourceFile);
		} finally {
			if (fw != null)
				try {
					fw.close();
				} catch (IOException e) {
					logger.warn("Error closing file " + sourceFile);
					fw = null;
				}
		}

		return res;

	}

	public static String getClassFileAbsolutePath(String className) {

		return System.getProperty("javacompilation.dir") + className.replace('.', File.separatorChar) + ".class";
	}

	public static String getSourceFileAbsolutePath(String className) {

		return System.getProperty("javacompilation.dir") + className.replace('.', File.separatorChar) + ".java";
	}
}
