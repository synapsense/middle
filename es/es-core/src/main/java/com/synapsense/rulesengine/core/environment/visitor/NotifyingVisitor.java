package com.synapsense.rulesengine.core.environment.visitor;

import org.apache.log4j.Logger;

import com.synapsense.rulesengine.core.environment.Property;
import com.synapsense.rulesengine.core.environment.RuleI;

public class NotifyingVisitor extends AbstractVisitor {

	private final static Logger logger = Logger.getLogger(NotifyingVisitor.class);
	private String reqId;

	public NotifyingVisitor(String reqId) {
		super();
		this.reqId = reqId;
	}

	@Override
	public void visitProperty(Property property) {
		if (logger.isTraceEnabled()) {
			logger.trace("Req#" + reqId + ": Property '" + property.getName() + "': Notified");
		}
		// propagate itself to all dependant rules
		for (RuleI rule : property.getDependantRules()) {
			rule.accept(this);
		}

		// notify dependent properties, too
		for (Property prop : property.getDependantProperties()) {
			prop.accept(this);
		}
	}

	@Override
	public void visitRule(RuleI rule) {
		if (logger.isTraceEnabled()) {
			logger.trace("Req#" + reqId + ": Rule '" + rule.getName() + "': Notified");
		}

		// Check if the rule is up to date.
		if (!rule.invalidate()) {
			// die on this step. The rule has not been recalculated since last
			// notification.
			return;
		}

		// Pass itself to the destination property
		rule.getDependantProperty().accept(this);
	}
}
