package com.synapsense.rulesengine.core.environment.visitor;

import com.synapsense.rulesengine.core.environment.Property;
import com.synapsense.rulesengine.core.environment.RuleI;

/**
 * The visitor is a design pattern. Provides iteration through the graph and
 * executing an algorithm on the nodes of graph.
 * 
 * @author alperep
 * 
 */
public abstract class AbstractVisitor {

	public abstract void visitProperty(Property property);

	public abstract void visitRule(RuleI rule);

}
