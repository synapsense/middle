package com.synapsense.rulesengine.core.environment.exception;

public class CalculationException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5583814010012925563L;

	public CalculationException() {
		super();
	}

	public CalculationException(String message) {
		super(message);
	}

	public CalculationException(String message, Throwable cause) {
		super(message, cause);
	}

	public CalculationException(Throwable cause) {
		super(cause);
	}
}
