package com.synapsense.rulesengine.core.scriptloader;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * The auxiliary classloader which is used as spare resource and replaced with
 * the new instance when request on reloading of the class arrives. Load classes
 * from the .class files
 * 
 * @author alperep
 * @source http://www.javalobby.org/java/forums/t18345.html
 */
public class CustomJavaClassLoader extends ClassLoader implements CustomClassLoader {

	public CustomJavaClassLoader() {
		super(Thread.currentThread().getContextClassLoader());
	}

	/**
	 * Default implementation is overriden to get the system classloader no
	 * chance to search for classes.
	 */
	@Override
	public Class<?> loadClass(String name, boolean resolve) throws ClassNotFoundException {

		// Here we just look for java classes.
		return Class.forName(name);

	}

	@Override
	public Class<?> loadClass(String name, String source, boolean resolve) throws ClassNotFoundException {
		// Here we just look for java classes.
		return Class.forName(name);
	}

	@Override
	public Class<?> findClass(String name) throws ClassNotFoundException {
		byte classByte[];
		Class<?> c = null;

		try {

			String classFilePath = JavaCompiler.getClassFileAbsolutePath(name);
			classByte = loadClassFromFile(classFilePath);

			c = defineClass(name, classByte, 0, classByte.length, null);

			return c;
		} catch (Exception e) {
			c = null;
		}

		// check system?
		try {
			c = findSystemClass(name);
		} catch (ClassNotFoundException e) {
			c = null;
		}

		if (c == null) {
			throw new ClassNotFoundException(name);
		}

		return c;
	}

	private byte[] loadClassFromFile(String className) throws IOException {

		File f;
		f = new File(className);
		int size = (int) f.length();
		byte buff[] = new byte[size];
		FileInputStream fis = new FileInputStream(f);
		DataInputStream dis = new DataInputStream(fis);
		dis.readFully(buff);
		dis.close();
		return buff;
	}

	/**
	 * This is the central method which is the reason for creating that class.
	 * It allows to ask JVM if it already has the definition of the class with
	 * that name.
	 * 
	 * Returns the class with the given <a href="#name">binary name</a> if this
	 * loader has been recorded by the Java virtual machine as an initiating
	 * loader of a class with that <a href="#name">binary name</a>. Otherwise
	 * <tt>null</tt> is returned. </p>
	 */
	public final Class<?> findLoadedClass0(String name) {
		return findLoadedClass(name);
	}

	@Override
	public CustomClassLoader renew() {
		return new CustomJavaClassLoader();
	}

	@Override
	public String getType() {
		return "Java";
	}
}
