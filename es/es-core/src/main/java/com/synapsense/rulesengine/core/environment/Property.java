/**
 * 
 */
package com.synapsense.rulesengine.core.environment;

import java.util.Collection;

import com.synapsense.exception.EnvException;
import com.synapsense.rulesengine.core.environment.visitor.AbstractVisitor;
import com.synapsense.service.AlertService;
import com.synapsense.service.Environment;

/**
 * The named source of data within the environment object.
 */
public interface Property {

	void setValue(Object value);

	DataSource getDataSource();

	Environment getEnvironment();

	AlertService getAlertService();

	void putDependantRule(RuleI dependantRule);

	void addDependantProperty(Property prop);

	void removeDependantProperty(Property prop);

	Collection<Property> getDependantProperties();

	void addSourceProperty(Property prop);

	void removeSourceProperty(Property prop);

	void removeSourceProperties();

	Collection<Property> getSourceProperties();

	Object getValue() throws EnvException;

	String getName();

	void setName(String name);

	long getTimestamp();

	long getTimerTimestamp();

	void setTimerTimestamp(long ts);

	RuleI getCalcRule();

	void setCalcRule(RuleI rule);

	Collection<RuleI> getDependantRules();

	void removeDependantRule(RuleI rule);

	void setLastCalcTime(long lastCalcTime);

	void accept(AbstractVisitor visitor);
}
