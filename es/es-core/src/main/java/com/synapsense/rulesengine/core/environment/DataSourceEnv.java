package com.synapsense.rulesengine.core.environment;

import com.synapsense.dto.TO;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.exception.PropertyNotFoundException;
import com.synapsense.exception.UnableToConvertPropertyException;
import com.synapsense.rulesengine.core.environment.exception.DataSourceException;
import com.synapsense.service.AlertService;
import com.synapsense.service.Environment;

/**
 * Used for retrieving data from DAL. Keeps necessary identifiers for that.
 * 
 * @author alperep
 * 
 */
public class DataSourceEnv implements DataSource {

	/**
	 * The id of the TO<?> passed from Env layer
	 */
	protected TO<?> hostObjectTO;

	/**
	 * Holds name of the env Property
	 */
	String envPropName;

	/**
	 * The java type of the env. property. Needed to pass it to Environment when
	 * asking for a value.
	 */
	Class<?> envPropType;

	/**
	 * The reference to the env.
	 */
	protected static Environment environment;
	protected static AlertService alertService;

	public DataSourceEnv(TO<?> hostObjectTO, String envPropName, Class<?> envPropType) {
		super();
		if (hostObjectTO == null || envPropName == null) {
			throw new IllegalArgumentException("Incorrect parameter: hostObjectTO=" + hostObjectTO + ", envPropName="
			        + envPropName);
		}

		this.hostObjectTO = hostObjectTO;
		this.envPropName = envPropName;
		this.envPropType = envPropType;
	}

	@Override
	public void setValue(Object value) throws DataSourceException {
		// some properties might be calculated to null. This most probably means
		// that an calculation error occurred or rule is incorrect.
		// the 'null' is one of the possible returned values of rule now
		// it shows that rule was not calculated or the result of calclulation
		// is 'null' in any case 'null' must be saved.
		/*
		 * if (value == null) { throw new
		 * DataSourceException("DataSource tried to set null value: forbidden");
		 * }
		 */
		try {
			environment.setPropertyValue(hostObjectTO, envPropName, value);
		} catch (PropertyNotFoundException e) {
			throw new DataSourceException(e);
		} catch (ObjectNotFoundException e) {
			throw new DataSourceException(e);
		} catch (UnableToConvertPropertyException e) {
			throw new DataSourceException(e);
		}

	}

	@Override
	public long getTimestamp() throws DataSourceException {
		try {
			return environment.getPropertyTimestamp(hostObjectTO, envPropName);
		} catch (ObjectNotFoundException e) {
			throw new DataSourceException(e);
		} catch (PropertyNotFoundException e) {
			throw new DataSourceException(e);
		}
	}

	@Override
	public Object getValue() throws DataSourceException {
		try {
			return environment.getPropertyValue(hostObjectTO, envPropName, envPropType);
		} catch (UnableToConvertPropertyException e) {
			throw new DataSourceException(e);
		} catch (PropertyNotFoundException e) {
			throw new DataSourceException(e);
		} catch (ObjectNotFoundException e) {
			throw new DataSourceException(e);
		}
	}

	@Override
	public void setTimestamp(long timestamp) {
		// that will be done by environment itself
		// when it decides to write the value to the storage
	}

	public static Environment getEnvironment() {
		return environment;
	}

	public static void setEnvironment(Environment environment) {
		DataSourceEnv.environment = environment;
	}

	public static AlertService getAlertService() {
		return alertService;
	}

	public static void setAlertService(AlertService alertService) {
		DataSourceEnv.alertService = alertService;
	}

	@Override
	public String getNativePropertyName() {
		return envPropName;
	}

	@Override
	public TO<?> getHostObjectTO() {
		return hostObjectTO;
	}

	@Override
	public Class<?> getEnvPropType() {
		return envPropType;
	}
}
