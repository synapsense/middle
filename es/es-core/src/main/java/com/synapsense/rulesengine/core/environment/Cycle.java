package com.synapsense.rulesengine.core.environment;

import java.util.ArrayList;
import java.util.Collection;

public class Cycle {

	protected Collection<String> verticesOnCycle = null;

	/**
	 * Construct the string explaining the cycle.
	 * 
	 * @return
	 */
	public String explainCycle() {

		if (verticesOnCycle.isEmpty()) {
			return "";
		}

		java.lang.StringBuilder expl = new StringBuilder();

		for (String prop : verticesOnCycle) {
			expl.append(prop.toString()).append(" -> ");
		}
		expl.append(verticesOnCycle.iterator().next().toString());

		return expl.toString();
	}

	public Cycle(Collection<String> props) {
		verticesOnCycle = new ArrayList<String>(props);
	}

	public String toString() {
		return explainCycle();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((verticesOnCycle == null) ? 0 : verticesOnCycle.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final Cycle other = (Cycle) obj;
		if (verticesOnCycle == null) {
			if (other.verticesOnCycle != null)
				return false;
		} else if (!verticesOnCycle.equals(other.verticesOnCycle))
			return false;
		return true;
	}
}