<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="RacksTempDaily" language="groovy" pageWidth="760" pageHeight="595" columnWidth="720" leftMargin="20" rightMargin="20" topMargin="20" bottomMargin="20" uuid="6e83898a-70df-4a86-a40a-703eb2898507">
	<property name="ireport.zoom" value="1.0"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
    <import value="com.synapsense.service.reporting.datasource.ReportingTableDataSource"/>
    <import value="com.synapsense.service.reporting.Formula"/>
    <import value="com.synapsense.service.reporting.Formula.TransformType"/>
    <import value="com.synapsense.service.reporting.olap.RangeSet"/>
	<style name="Title" fontName="Arial" fontSize="26" isBold="true" pdfFontName="Helvetica-Bold"/>
	<style name="SubTitle" forecolor="#666666" fontName="Arial" fontSize="18"/>
	<style name="Column header" forecolor="#666666" fontName="Arial" fontSize="12" isBold="true" pdfFontName="Helvetica-Bold"/>
	<style name="Detail" fontName="Arial" fontSize="12"/>
	<style name="table">
		<box>
			<pen lineWidth="1.0" lineColor="#000000"/>
		</box>
	</style>
	<style name="table_TH" mode="Opaque" backcolor="#F0F8FF">
		<box>
			<pen lineWidth="0.5" lineColor="#000000"/>
		</box>
	</style>
	<style name="table_CH" mode="Opaque" backcolor="#BFE1FF">
		<box>
			<pen lineWidth="0.5" lineColor="#000000"/>
		</box>
	</style>
	<style name="table_TD" mode="Opaque" backcolor="#FFFFFF">
		<box>
			<pen lineWidth="0.5" lineColor="#000000"/>
		</box>
	</style>
	<style name="table 1">
		<box>
			<pen lineWidth="1.0" lineColor="#000000"/>
		</box>
	</style>
	<style name="table 1_TH" mode="Opaque" backcolor="#F0F8FF">
		<box>
			<pen lineWidth="0.5" lineColor="#000000"/>
		</box>
	</style>
	<style name="table 1_CH" mode="Opaque" backcolor="#BFE1FF">
		<box>
			<pen lineWidth="0.5" lineColor="#000000"/>
		</box>
	</style>
	<style name="table 1_TD" mode="Opaque" backcolor="#FFFFFF">
		<box>
			<pen lineWidth="0.5" lineColor="#000000"/>
		</box>
	</style>
	<subDataset name="Intake Table" uuid="975d2a89-db86-4532-8d91-d82a25dbe3b3">
		<field name="ObjectName" class="java.lang.String">
            <property name="Hierarchy" value="Object"/>
        </field>
		<field name="RACK.cTop.avg" class="java.lang.Double">
            <property name="Hierarchy" value="Measures"/>
		</field>
		<field name="RACK.cMid.avg" class="java.lang.Double">
            <property name="Hierarchy" value="Measures"/>
		</field>
		<field name="RACK.cBot.avg" class="java.lang.Double">
            <property name="Hierarchy" value="Measures"/>
		</field>
		<field name="Timestamp" class="java.lang.String">
            <property name="Hierarchy" value="Time"/>
        </field>
	</subDataset>
    <parameter name="Objects" class="java.util.HashSet">
		<parameterDescription><![CDATA[{"uiComponentType": "ObjectSelector", "attributes" : [{"name" : "types", "value": ["RACK"]},{"name" : "multiselect", "value": true}]}]]></parameterDescription>
	</parameter>
    <parameter name="DateRange" class="com.synapsense.service.reporting.olap.RangeSet">
		<parameterDescription><![CDATA[{"uiComponentType": "DataRangeSelector"}]]></parameterDescription>
	</parameter>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band height="33" splitType="Stretch">
			<staticText>
				<reportElement uuid="7abf160c-87af-43c5-8266-c1ec0484f139" style="Title" x="103" y="0" width="390" height="33"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="20"/>
				</textElement>
				<text><![CDATA[Racks Temperature Daily]]></text>
			</staticText>
		</band>
	</title>
	<pageHeader>
		<band splitType="Stretch"/>
	</pageHeader>
	<columnHeader>
		<band height="7" splitType="Stretch"/>
	</columnHeader>
	<detail>
		<band splitType="Stretch"/>
	</detail>
	<columnFooter>
		<band height="7" splitType="Stretch"/>
	</columnFooter>
	<pageFooter>
		<band splitType="Stretch"/>
	</pageFooter>
	<summary>
		<band height="50" splitType="Stretch">
			<componentElement>
				<reportElement uuid="3b3b8fee-7a60-4c6d-91c5-3f07b06d0a32" key="table 1" style="table 1" stretchType="RelativeToTallestObject" x="59" y="0" width="525" height="50" isRemoveLineWhenBlank="true"/>
				<jr:table xmlns:jr="http://jasperreports.sourceforge.net/jasperreports/components" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports/components http://jasperreports.sourceforge.net/xsd/components.xsd">
					<datasetRun subDataset="Intake Table" uuid="13ad075d-f787-449e-aa49-befe4517e642">
						<dataSourceExpression><![CDATA[ReportingTableDataSource.newRAWTable($P{Objects}, ["RACK.cTop.avg", "RACK.cMid.avg", "RACK.cBot.avg"].toArray(new String[0]), "Day", $P{DateRange})]]></dataSourceExpression>
					</datasetRun>
					<jr:column width="103" uuid="f2eeb8f4-684c-4529-90d7-3ed1a3eac5c1">
						<jr:tableFooter height="2" rowSpan="1"/>
						<jr:columnHeader style="table 1_CH" height="31" rowSpan="1">
							<staticText>
								<reportElement uuid="980e6b59-26b2-401e-ab0f-85f7274c436f" positionType="Float" x="0" y="0" width="102" height="30"/>
								<textElement textAlignment="Center" verticalAlignment="Middle">
									<font size="10" isBold="true"/>
									<paragraph tabStopWidth="10"/>
								</textElement>
								<text><![CDATA[Object Name]]></text>
							</staticText>
						</jr:columnHeader>
						<jr:detailCell style="table 1_TD" height="30" rowSpan="1">
							<textField isBlankWhenNull="true">
								<reportElement uuid="6ee1268b-37a4-4a60-96fb-914b76da610d" positionType="Float" stretchType="RelativeToTallestObject" x="0" y="0" width="102" height="21"/>
								<textElement textAlignment="Center" verticalAlignment="Middle">
									<font size="10"/>
									<paragraph leftIndent="2" tabStopWidth="10"/>
								</textElement>
								<textFieldExpression><![CDATA[$F{ObjectName}]]></textFieldExpression>
							</textField>
						</jr:detailCell>
					</jr:column>
					<jr:column width="100" uuid="02bcc846-cc5a-4a7c-aa55-7e4447b6e6be">
						<jr:columnHeader style="table 1_CH" height="31" rowSpan="1">
							<staticText>
								<reportElement uuid="94c19e4b-bd7d-4e6f-af88-eb91b91e903f" positionType="Float" x="0" y="0" width="100" height="30"/>
								<textElement textAlignment="Center" verticalAlignment="Middle">
									<font size="10" isBold="true"/>
									<paragraph tabStopWidth="10"/>
								</textElement>
								<text><![CDATA[Avg Top Intake Temp]]></text>
							</staticText>
						</jr:columnHeader>
						<jr:detailCell style="table 1_TD" height="30" rowSpan="1">
							<textField isStretchWithOverflow="true" isBlankWhenNull="true">
								<reportElement uuid="8f2bdabd-bc72-42bb-b7eb-3389c0594af1" positionType="Float" stretchType="RelativeToTallestObject" x="0" y="0" width="100" height="21"/>
								<textElement textAlignment="Center" verticalAlignment="Middle">
									<font size="10"/>
									<paragraph leftIndent="2" tabStopWidth="10"/>
								</textElement>
								<textFieldExpression><![CDATA[$F{RACK.cTop.avg}]]></textFieldExpression>
							</textField>
						</jr:detailCell>
					</jr:column>
					<jr:column width="100" uuid="02bcc846-cc5a-4a7c-aa55-7e4447b6e6be">
						<jr:columnHeader style="table 1_CH" height="31" rowSpan="1">
							<staticText>
								<reportElement uuid="94c19e4b-bd7d-4e6f-af88-eb91b91e903f" positionType="Float" x="0" y="0" width="100" height="30"/>
								<textElement textAlignment="Center" verticalAlignment="Middle">
									<font size="10" isBold="true"/>
									<paragraph tabStopWidth="10"/>
								</textElement>
								<text><![CDATA[Avg Mid Intake Temp]]></text>
							</staticText>
						</jr:columnHeader>
						<jr:detailCell style="table 1_TD" height="30" rowSpan="1">
							<textField isStretchWithOverflow="true" isBlankWhenNull="true">
								<reportElement uuid="8f2bdabd-bc72-42bb-b7eb-3389c0594af1" positionType="Float" stretchType="RelativeToTallestObject" x="0" y="0" width="100" height="21"/>
								<textElement textAlignment="Center" verticalAlignment="Middle">
									<font size="10"/>
									<paragraph leftIndent="2" tabStopWidth="10"/>
								</textElement>
								<textFieldExpression><![CDATA[$F{RACK.cMid.avg}]]></textFieldExpression>
							</textField>
						</jr:detailCell>
					</jr:column>
					<jr:column width="100" uuid="02bcc846-cc5a-4a7c-aa55-7e4447b6e6be">
						<jr:columnHeader style="table 1_CH" height="31" rowSpan="1">
							<staticText>
								<reportElement uuid="94c19e4b-bd7d-4e6f-af88-eb91b91e903f" positionType="Float" x="0" y="0" width="100" height="30"/>
								<textElement textAlignment="Center" verticalAlignment="Middle">
									<font size="10" isBold="true"/>
									<paragraph tabStopWidth="10"/>
								</textElement>
								<text><![CDATA[Avg Bot Intake Temp]]></text>
							</staticText>
						</jr:columnHeader>
						<jr:detailCell style="table 1_TD" height="30" rowSpan="1">
							<textField isStretchWithOverflow="true" isBlankWhenNull="true">
								<reportElement uuid="8f2bdabd-bc72-42bb-b7eb-3389c0594af1" positionType="Float" stretchType="RelativeToTallestObject" x="0" y="0" width="100" height="21"/>
								<textElement textAlignment="Center" verticalAlignment="Middle">
									<font size="10"/>
									<paragraph leftIndent="2" tabStopWidth="10"/>
								</textElement>
								<textFieldExpression><![CDATA[$F{RACK.cBot.avg}]]></textFieldExpression>
							</textField>
						</jr:detailCell>
					</jr:column>
					<jr:column width="94" uuid="56973cbc-8cb2-4387-97fe-cb8ce3ae0558">
						<jr:columnHeader style="table 1_CH" height="31" rowSpan="1">
							<staticText>
								<reportElement uuid="9bf21c32-8766-4fa1-a162-f54207c5a43b" positionType="Float" x="0" y="0" width="93" height="30"/>
								<textElement textAlignment="Center" verticalAlignment="Middle">
									<font size="10" isBold="true"/>
									<paragraph tabStopWidth="10"/>
								</textElement>
								<text><![CDATA[Timestamp]]></text>
							</staticText>
						</jr:columnHeader>
						<jr:detailCell style="table 1_TD" height="30" rowSpan="1">
							<textField isBlankWhenNull="true">
								<reportElement uuid="5e537807-7f4e-402c-8f2a-244dcc77a70a" positionType="Float" stretchType="RelativeToTallestObject" x="0" y="0" width="93" height="21"/>
								<textElement textAlignment="Center" verticalAlignment="Middle">
									<font size="10"/>
									<paragraph tabStopWidth="10"/>
								</textElement>
								<textFieldExpression><![CDATA[$F{Timestamp}]]></textFieldExpression>
							</textField>
						</jr:detailCell>
					</jr:column>
				</jr:table>
			</componentElement>
		</band>
	</summary>
</jasperReport>
