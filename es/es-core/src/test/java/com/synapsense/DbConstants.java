package com.synapsense;


public final class DbConstants {
	public static final String TBL_HISTORICAL_VALUES = "historical_values";
	public static final String TBL_ACTIVITY_LOG = "tbl_activity_log";

	public static final String DB_DRIVER = "com.mysql.jdbc.Driver";
	public static final String DB_CONNECTION = "jdbc:mysql://localhost/synap";
	public static final String DB_USER = "root";
	public static final String DB_PASSWORD = "dbuser";

	private DbConstants() {
	}
}
