package com.synapsense.func.environment;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.io.File;
import java.util.Set;

import javax.ejb.EJBAccessException;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.synapsense.func.ConsoleApp;
import com.synapsense.service.FileWriter;
import com.synapsense.service.FileWriter.WriteMode;
import com.synapsense.util.CollectionUtils;
import com.synapsense.util.JNDINames;

public class FileWriterTest {

	private static FileWriter fileWriterService;

	/**
	 * root directory as it is configured
	 */
	private static String rootDir = System.getenv().get("JBOSS_HOME") + "\\server\\default\\conf";
	private static Set<File> filesToDeleteAfterTest = CollectionUtils.newSet();
	
	private static void markFileDeleted(File f) { 
		filesToDeleteAfterTest.add(f);
	}
	
	@BeforeClass
	public static void setupBeforeClass() throws Exception {
		ConsoleApp.init();
		fileWriterService = (FileWriter) ConsoleApp.ctx.lookup(JNDINames.FILE_WRITTER_NAME);
	}

	@AfterClass
	public static void teardownafterClass() throws Exception {
		for (File f : filesToDeleteAfterTest) {
			f.delete();
		}
	}

	public void readUnexistingFileFailure() throws Exception {
		fileWriterService.readFile("unexistingfile");
	}

	@Test
	public void writeThenReadFileAllowableDirectorySuccess() throws Exception {
		fileWriterService.writeFile("writeThenReadFileAllowableDirectorySuccess.txt", "data", WriteMode.REWRITE);
		
		markFileDeleted(new File(rootDir,"writeThenReadFileAllowableDirectorySuccess.txt"));
		
		String fileData = fileWriterService.readFile("writeThenReadFileAllowableDirectorySuccess.txt");
		assertEquals("data", fileData);
	}

	@Test
	public void appendThenReadFileAllowableDirectorySuccess() throws Exception {
		fileWriterService.writeFile("appendThenReadFileAllowableDirectorySuccess.txt", "data", WriteMode.REWRITE);
		fileWriterService.writeFile("appendThenReadFileAllowableDirectorySuccess.txt", "more data", WriteMode.APPEND);
	
		markFileDeleted(new File(rootDir,"appendThenReadFileAllowableDirectorySuccess.txt"));
		
		String fileData = fileWriterService.readFile("appendThenReadFileAllowableDirectorySuccess.txt");
		assertEquals("datamore data", fileData);
	}

	@Test(expected = EJBAccessException.class)
	public void readFileFromTwoLevelUpDirectoryThanRootFailure() throws Exception {
		fileWriterService.readFile("..\\..\\file.txt");
	}

	@Test(expected = EJBAccessException.class)
	public void writeFileToTwoLevelUpDirectoryThanRootFailure() throws Exception {
		fileWriterService.writeFile("..\\..\\file.txt", "data", WriteMode.REWRITE);
	}

	@Test
	public void policyFileTestOfConfDirectoryReadAccessSuccess() throws Exception {
		fileWriterService.writeFile("policyFileTestOfConfDirectoryReadAccessSuccess.txt", "data", WriteMode.REWRITE);
		fileWriterService.readFile("policyFileTestOfConfDirectoryReadAccessSuccess.txt");
		markFileDeleted(new File(rootDir,"policyFileTestOfConfDirectoryReadAccessSuccess.txt"));
	}

	@Test
	public void policyReadMailServiceConfigFileSuccess() throws Exception {
		String mailConf = fileWriterService.readFile("..\\deploy\\mail-service.xml");
		assertFalse(mailConf.isEmpty());
	}

	@Test(expected = EJBAccessException.class)
	public void policyDeleteMailServiceConfigFileFailure() throws Exception {
		fileWriterService.deleteFile("..\\deploy\\mail-service.xml");
	}

	@Test
	public void policyReadWebDeployerServiceConfigFileSuccess() throws Exception {
		String fileContent = fileWriterService.readFile("..\\deploy\\jboss-web.deployer\\server.xml");
		assertFalse(fileContent.isEmpty());
	}

	@Test(expected = EJBAccessException.class)
	public void policyDeleteWebDeployerServiceConfigFileFailure() throws Exception {
		fileWriterService.deleteFile("..\\deploy\\jboss-web.deployer\\server.xml");
	}

}
