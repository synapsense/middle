package com.synapsense.func.reporting.dao;

import static com.synapsense.impl.reporting.TestUtils.EMPTY_REPORT_DESIGN_PATH;
import static com.synapsense.impl.reporting.TestUtils.FILE_STORAGE;
import static com.synapsense.impl.reporting.TestUtils.EMPTY_REPORT_NAME;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Collection;
import net.sf.jasperreports.engine.JasperPrint;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.synapsense.exception.ObjectExistsException;
import com.synapsense.impl.reporting.TestUtils;
import com.synapsense.impl.reporting.dao.DbESDAOFactory;
import com.synapsense.impl.reporting.dao.ReportDAO;
import com.synapsense.impl.reporting.dao.ReportTaskDAO;
import com.synapsense.impl.reporting.dao.ReportTemplateDAO;
import com.synapsense.impl.reporting.dao.ReportingDAOFactory;
import com.synapsense.impl.reporting.export.ExportFormat;
import com.synapsense.impl.reporting.model.JSReport;
import com.synapsense.impl.reporting.model.Report;
import com.synapsense.impl.reporting.model.ReportTemplate;
import com.synapsense.service.reporting.dto.ReportInfo;
import com.synapsense.service.reporting.dto.ReportTaskInfo;
import com.synapsense.service.reporting.dto.ReportTemplateInfo;
import com.synapsense.service.reporting.dto.ReportTitle;
import com.synapsense.service.reporting.exception.CompilationReportingException;
import com.synapsense.service.reporting.exception.DAOReportingException;
import com.synapsense.service.reporting.exception.GenerationReportingException;
import com.synapsense.service.reporting.management.ReportParameter;

public class DbReportDAOTest {

	private static ReportingDAOFactory daoFactory;
	private static ReportTaskDAO tDao;
	private static ReportDAO rDao;
	private static ReportTemplateDAO rtDao;

	@BeforeClass
	public static void setUpBeforeClass() throws DAOReportingException, ObjectExistsException {
		daoFactory = new DbESDAOFactory(TestUtils.FILE_STORAGE, TestUtils.getEnvironment(), TestUtils.getFileWriter());
		rtDao = daoFactory.newReportTemplateDAO();
		ReportTemplateInfo rtInfo = new ReportTemplateInfo("TestReportTemplate");
		rtInfo.addParameter(new ReportParameter("Param", String.class));
		rtDao.createReportTemplate(rtInfo, "TestDesign");
		tDao = daoFactory.newReportTaskDao();
		rDao = daoFactory.newReportDao();
	}

	@AfterClass
	public static void tearDownAfterClass() throws DAOReportingException {
		ReportTemplateDAO rtDao = daoFactory.newReportTemplateDAO();
		rtDao.removeReportTemplate("TestReportTemplate");
		daoFactory.shutdown();
	}

	@Test
	public void testCreateReportDao() throws DAOReportingException {
		ReportDAO rDao = daoFactory.newReportDao();
		Assert.assertNotNull("Report DAO is null", rDao);
	}

	@Test
	public void testSaveReport() throws DAOReportingException, ObjectExistsException {

		JasperPrint jPrint = new JasperPrint();

		jPrint.setName("TestReport");
		
		String taskName = "Test";
        ReportTaskInfo reportTaskInfo = ReportTaskInfo.newInstance("TestReportTemplate", taskName);
        tDao.saveReportTask(reportTaskInfo);
        
        ReportInfo rInfo = new ReportInfo("Test", System.currentTimeMillis());

		rDao.saveReport(rInfo, new JSReport(jPrint), reportTaskInfo.getReportTemplateName());

		Assert.assertFalse("Report id", 0 == rInfo.getReportId());

		Collection<ReportTitle> coll = rDao.getGeneratedReports();
		int size = coll.size();

		Object reportObject = rDao.getReportObject(rInfo.getReportId());
		Assert.assertNotNull("Report Object null", reportObject);
		
		Assert.assertEquals("Report object type", JSReport.class, reportObject.getClass());
		Assert.assertEquals("Report object name", "TestReport", ((JSReport) reportObject).getJasperPrint().getName());
		
		tDao.removeReportTask(reportTaskInfo.getTaskId());
		rDao.removeReport(rInfo.getReportId());

		coll = rDao.getGeneratedReports();
		Assert.assertEquals("Reports size", coll.size(), size - 1);
	}
	
	@Test
	public void testExportReport() throws DAOReportingException, CompilationReportingException, FileNotFoundException,
	        GenerationReportingException, ObjectExistsException {

		ReportTemplate template = new ReportTemplate(new FileInputStream(FILE_STORAGE + EMPTY_REPORT_DESIGN_PATH));

		rtDao.createReportTemplate(new ReportTemplateInfo(template.getName(), template.getParameters()),
		        TestUtils.readTestFile(FILE_STORAGE + EMPTY_REPORT_DESIGN_PATH));
		
		String taskName = "Test";
        ReportTaskInfo reportTaskInfo = ReportTaskInfo.newInstance(EMPTY_REPORT_NAME, taskName);

        reportTaskInfo.addParameterValue("StringParameter", "StringValue");
        reportTaskInfo.addParameterValue("IntegerParameter", 0);
        tDao.saveReportTask(reportTaskInfo);
        
		Report report = template.generateReport(reportTaskInfo);
		
		ReportInfo rInfo = new ReportInfo(taskName, System.currentTimeMillis());
		rDao.saveReport(rInfo, report, reportTaskInfo.getReportTemplateName());

		try {
			String relPath = rDao.exportReport(rInfo.getReportId(), ExportFormat.HTML);
			String path = TestUtils.FILE_STORAGE + "/" + relPath;
			Assert.assertNotNull("Exported path not null", relPath);
			Assert.assertTrue("Exported file exists", new File(path).exists());

			rInfo = rDao.getReportInfo(rInfo.getReportId());
			Assert.assertEquals(relPath, rInfo.getExportedPath(ExportFormat.HTML.getFileExtension()));

			rDao.removeReport(rInfo.getReportId());
			tDao.removeReportTask(reportTaskInfo.getTaskId());
			rtDao.removeReportTemplate(template.getName());
			Assert.assertFalse("File does not exist", new File(path).exists());
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}
}
