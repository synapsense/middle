package com.synapsense.func.environment;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Collection;
import java.util.Date;
import java.util.LinkedList;

import junit.framework.Assert;

import org.junit.BeforeClass;
import org.junit.Test;

import com.synapsense.dto.CollectionTO;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.exception.IllegalInputParameterException;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.exception.UnsupportedRelationException;
import com.synapsense.func.ConsoleApp;
import com.synapsense.service.Environment;

public class EnvironmentEmptyTOTest {
	private static Environment env;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		ConsoleApp.init();

		env = ConsoleApp.env;

	}

	@Test
	public void testEmptyTOInMethods() throws Exception {
		String typeName = "TYPE";
		String propName = "property";
		String[] propNames = { "property" };
		Class clazz = String.class;
		Date start = new Date();
		Date end = new Date();
		// always must be EMPTY_TO
		TO<?> objId = TOFactory.EMPTY_TO;

		Collection<TO<?>> objIds = new LinkedList<TO<?>>();
		objIds.add(objId);

		String value = "value";

		try {
			env.addPropertyValue(objId, propName, value);
			assertTrue(false);
		} catch (ObjectNotFoundException e) {
		}

		try {
			env.removePropertyValue(objId, propName, value);
			assertTrue(false);
		} catch (ObjectNotFoundException e) {
		}
		try {
			env.deleteObject(objId);
			assertTrue(false);
		} catch (ObjectNotFoundException e) {
		}

		Collection<CollectionTO> collectionChildren = env.getChildren(objIds);
		assertEquals(1, collectionChildren.size());

		try {
			Collection<TO<?>> children = env.getChildren(objId);
			fail(ObjectNotFoundException.class + " is expected");
		} catch (ObjectNotFoundException e) {
		}

		collectionChildren = env.getChildren(objIds, "SOME");
		assertEquals(1, collectionChildren.size());

		try {
			Collection<TO<?>> children = env.getChildren(objId, "SOME");
			fail(ObjectNotFoundException.class + " is expected");
		} catch (ObjectNotFoundException e) {
		}

		try {
			env.getParents(objId);
			assertTrue(false);
		} catch (ObjectNotFoundException e) {
		}
		try {
			env.getParents(objId, typeName);
			assertTrue(false);
		} catch (ObjectNotFoundException e) {
		}

		Collection<TO<?>> related = env.getRelatedObjects(objId, typeName, true);
		assertEquals(0, related.size());

		try {
			env.getHistorySize(objId, propName);
			Assert.fail();
		} catch (IllegalInputParameterException e) {
		}
		try {
			env.getHistory(objId, propName, clazz);
			Assert.fail();
		} catch (IllegalInputParameterException e) {
		}

		try {
			env.getHistory(objId, propName, 1, 0, clazz);
			Assert.fail();
		} catch (IllegalInputParameterException e) {
		}

		try {
			env.getHistory(objId, propName, start, 1, clazz);
			Assert.fail();
		} catch (IllegalInputParameterException e) {
		}

		try {
			env.getHistory(objId, propName, start, end, clazz);
			Assert.fail();
		} catch (IllegalInputParameterException e) {
		}

		env.getPropertyValue(objIds, propNames);

		try {
			env.getPropertyValue(objId, propName, clazz);
			assertTrue(false);
		} catch (ObjectNotFoundException e) {
		}

		try {
			env.getAllPropertiesValues(objIds);
			assertTrue(false);
		} catch (IllegalInputParameterException e) {
		}

		try {
			env.getAllPropertiesValues(objId);
			assertTrue(false);
		} catch (IllegalInputParameterException e) {
		}

		/*
		 * set is deffered try { env.setPropertyValue(objId, propName, value);
		 * assertTrue(false); } catch (ObjectNotFoundException e) { }
		 */

		try {
			env.setRelation(TOFactory.EMPTY_TO, TOFactory.EMPTY_TO);
			assertTrue(false);
		} catch (UnsupportedRelationException e) {
		}

		try {
			env.removeRelation(TOFactory.EMPTY_TO, TOFactory.EMPTY_TO);
			assertTrue(false);
		} catch (ObjectNotFoundException e) {
		}

		try {
			env.getPropertyTimestamp(objId, propName);
			assertTrue(false);
		} catch (ObjectNotFoundException e) {
		}

		// doesn't use the TO
		// env.getPropertyValue(typeName, propName, clazz);

	}

}
