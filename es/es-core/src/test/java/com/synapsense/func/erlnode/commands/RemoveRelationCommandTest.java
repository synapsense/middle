package com.synapsense.func.erlnode.commands;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.ericsson.otp.erlang.OtpErlangAtom;
import com.ericsson.otp.erlang.OtpErlangObject;
import com.ericsson.otp.erlang.OtpErlangString;
import com.ericsson.otp.erlang.OtpErlangTuple;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.impl.erlnode.ExceptionTypes;
import com.synapsense.impl.erlnode.commands.RemoveRelationCommand;
import com.synapsense.impl.erlnode.utils.ToConverter;
import com.synapsense.service.Environment;

public class RemoveRelationCommandTest {
	static UnitTestHelper utHelper = null;
	static String testType1 = "testType1";
	static String testType2 = "testType2";
	static Map<String, Object> envMap;
	static Environment env;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		utHelper = new UnitTestHelper();
		utHelper.initializeEnvironment();
		env = utHelper.getEnvironment();
		envMap = new HashMap<String, Object>();
		envMap.put("env", env);

	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		utHelper.removeTypeCompletly(testType1);
		utHelper.removeTypeCompletly(testType2);
		utHelper.dispose();
	}

	@Before
	public void setUp() throws Exception {
		env.createObjectType(testType1);
		env.createObjectType(testType2);
	}

	@After
	public void tearDown() throws Exception {
		env.deleteObjectType(testType1);
		env.deleteObjectType(testType2);
	}

	@Test
	public void SuccessRemoveRelationTest() throws Exception {

		TO<?> parent = env.createObject(testType1);
		TO<?> child = env.createObject(testType2);
		env.setRelation(parent, child);
		assertEquals(child, env.getChildren(parent).toArray()[0]);
		RemoveRelationCommand command = new RemoveRelationCommand(envMap);
		OtpErlangAtom obj = (OtpErlangAtom) command.run(new OtpErlangTuple(new OtpErlangObject[] {
		        new OtpErlangString("RemoveRelationCommand"), ToConverter.serializeTo(parent),
		        ToConverter.serializeTo(child) }));
		assertEquals("ok", obj.toString());
		assertEquals(0, env.getChildren(parent).size());
		env.deleteObject(parent);
		env.deleteObject(child);

	}

	@Test
	public void FailRemoveRelationTest() throws Exception {

		TO<?> parent = env.createObject(testType1);
		TO<?> child = env.createObject(testType2);
		env.setRelation(parent, child);
		assertEquals(child, env.getChildren(parent).toArray()[0]);
		RemoveRelationCommand command = new RemoveRelationCommand(envMap);
		TO<?> to = TOFactory.getInstance().loadTO("NoneExistingType:11");
		OtpErlangTuple obj = (OtpErlangTuple) command.run(new OtpErlangTuple(new OtpErlangObject[] {
		        new OtpErlangString("RemoveRelationCommand"), ToConverter.serializeTo(to),
		        ToConverter.serializeTo(child) }));
		assertEquals("exception", obj.elementAt(0).toString());
		assertEquals(ExceptionTypes.OBJECT_NOT_FOUND.getExceptionType(),
		        ((OtpErlangString) obj.elementAt(1)).stringValue());
		env.deleteObject(parent);
		env.deleteObject(child);

	}

	@Test
	public void InvalidParametersTest() throws Exception {
		RemoveRelationCommand command = new RemoveRelationCommand(envMap);
		OtpErlangTuple obj = (OtpErlangTuple) command.run(new OtpErlangTuple(
		        new OtpErlangObject[] { new OtpErlangString("RemoveRelationCommand"), }));
		assertEquals("exception", obj.elementAt(0).toString());
		assertEquals(ExceptionTypes.ILLEGAL_ARGUMENTS.getExceptionType(),
		        ((OtpErlangString) obj.elementAt(1)).stringValue());
	}
}
