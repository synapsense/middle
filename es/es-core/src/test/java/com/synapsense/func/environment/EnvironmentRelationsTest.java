package com.synapsense.func.environment;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.synapsense.dto.EnvObject;
import com.synapsense.dto.EnvObjectBundle;
import com.synapsense.dto.ObjectType;
import com.synapsense.dto.Relation;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOBundle;
import com.synapsense.exception.IllegalInputParameterException;
import com.synapsense.exception.UnsupportedRelationException;
import com.synapsense.func.ConsoleApp;
import com.synapsense.service.Environment;
import com.synapsense.util.JNDINames;

public class EnvironmentRelationsTest {

	private static Environment env;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		ConsoleApp.init();

		env = ConsoleApp.env;
	}

	@Test(expected = IllegalInputParameterException.class)
	public void testBulkSetRelationNullArgumentFailure() throws Exception {
		env.setRelation(null);
	}

	@Test(expected = IllegalInputParameterException.class)
	public void testBulkRemoveRelationNullArgumentFailure() throws Exception {
		env.removeRelation(null);
	}

	@Test(expected = IllegalInputParameterException.class)
	public void testBulkSetRelationEmptyArgumentFailure() throws Exception {
		env.setRelation(Collections.EMPTY_LIST);
	}

	@Test(expected = IllegalInputParameterException.class)
	public void testBulkRemoveRelationEmptyArgumentFailure() throws Exception {
		env.removeRelation(Collections.EMPTY_LIST);
	}

	@Test(expected = IllegalInputParameterException.class)
	public void testBulkSetRelationWithNullArgumentFailure() throws Exception {
		env.setRelation(Collections.singletonList((Relation) null));
	}

	@Test(expected = IllegalInputParameterException.class)
	public void testBulkRemoveRelationWithNullArgumentFailure() throws Exception {
		env.removeRelation(Collections.singletonList((Relation) null));
	}

	@Test(expected = UnsupportedRelationException.class)
	public void testBulkSeRelationCycleFailure() throws Exception {
		String TYPE_TEST = "test type";
		try {
			ObjectType t1 = env.createObjectType(TYPE_TEST);

			TO parent1 = env.createObject(TYPE_TEST);
			TO child1 = env.createObject(TYPE_TEST);

			env.setRelation(parent1, child1);
			env.setRelation(child1, parent1);

		} finally {
			env.deleteObjectType(TYPE_TEST);
		}
	}

	@Test
	public void testBulkSetRemoveRelationsSuccess() throws Exception {
		String TYPE_TEST = "test type";
		try {
			ObjectType t1 = env.createObjectType(TYPE_TEST);

			TO<?> parent1 = env.createObject(TYPE_TEST);
			TO<?> child1 = env.createObject(TYPE_TEST);
			TO<?> child2 = env.createObject(TYPE_TEST);

			Relation relation = new Relation(parent1, Arrays.<TO<?>>asList(child1, child2));
			env.setRelation(Arrays.asList(relation));

			Collection<TO<?>> descendants = env.getRelatedObjects(parent1, null, true);
			assertNotNull(descendants);
			assertEquals(2, descendants.size());
			assertTrue(descendants.contains(child1));
			assertTrue(descendants.contains(child2));

			Collection<TO<?>> ancestors = env.getRelatedObjects(child1, null, false);
			assertNotNull(ancestors);
			assertEquals(1, ancestors.size());
			assertTrue(ancestors.contains(parent1));

			ancestors = env.getRelatedObjects(child2, null, false);
			assertNotNull(ancestors);
			assertEquals(1, ancestors.size());
			assertTrue(ancestors.contains(parent1));

			env.removeRelation(Arrays.asList(relation));
			descendants = env.getRelatedObjects(parent1, null, true);
			assertNotNull(descendants);
			assertEquals(0, descendants.size());

			ancestors = env.getRelatedObjects(child1, null, false);
			assertNotNull(ancestors);
			assertEquals(0, ancestors.size());

			ancestors = env.getRelatedObjects(child2, null, false);
			assertNotNull(ancestors);
			assertEquals(0, ancestors.size());

		} finally {
			env.deleteObjectType(TYPE_TEST);
		}
	}

	@Test
	public void testBulkSetRemoveRelationChildrenIntersectionsSuccess() throws Exception {
		String TYPE_TEST = "test type";
		try {
			ObjectType t1 = env.createObjectType(TYPE_TEST);

			TO<?> parent1 = env.createObject(TYPE_TEST);
			TO<?> parent2 = env.createObject(TYPE_TEST);
			TO<?> child1 = env.createObject(TYPE_TEST);
			TO<?> child2 = env.createObject(TYPE_TEST);

			Relation relation1 = new Relation(parent1, Arrays.<TO<?>>asList(child1, child2));
			Relation relation2 = new Relation(parent2, Arrays.<TO<?>>asList(child1, child2));

			env.setRelation(Arrays.asList(relation1, relation2));

			Collection<TO<?>> descendants = env.getRelatedObjects(parent1, null, true);
			assertNotNull(descendants);
			assertEquals(2, descendants.size());
			assertTrue(descendants.contains(child1));
			assertTrue(descendants.contains(child2));

			Collection<TO<?>> ancestors = env.getRelatedObjects(child1, null, false);
			assertNotNull(ancestors);
			assertEquals(2, ancestors.size());
			assertTrue(ancestors.contains(parent1));
			assertTrue(ancestors.contains(parent2));

			ancestors = env.getRelatedObjects(child2, null, false);
			assertNotNull(ancestors);
			assertEquals(2, ancestors.size());
			assertTrue(ancestors.contains(parent1));
			assertTrue(ancestors.contains(parent2));

			env.removeRelation(Arrays.asList(relation1, relation2));
			descendants = env.getRelatedObjects(parent1, null, true);
			assertNotNull(descendants);
			assertEquals(0, descendants.size());

			ancestors = env.getRelatedObjects(child1, null, false);
			assertNotNull(ancestors);
			assertEquals(0, ancestors.size());

			ancestors = env.getRelatedObjects(child2, null, false);
			assertNotNull(ancestors);
			assertEquals(0, ancestors.size());

		} finally {
			env.deleteObjectType(TYPE_TEST);
		}
	}

	@Test
	public void testBulkSetRemoveRelationParentIntersectionsSuccess() throws Exception {
		String TYPE_TEST = "test type";
		try {
			ObjectType t1 = env.createObjectType(TYPE_TEST);

			TO<?> parent1 = env.createObject(TYPE_TEST);
			TO<?> child1 = env.createObject(TYPE_TEST);
			TO<?> child2 = env.createObject(TYPE_TEST);

			Relation relation1 = new Relation(parent1, Arrays.<TO<?>>asList(child1));
			Relation relation2 = new Relation(parent1, Arrays.<TO<?>>asList(child2));

			env.setRelation(Arrays.asList(relation1, relation2));

			Collection<TO<?>> descendants = env.getRelatedObjects(parent1, null, true);
			assertNotNull(descendants);
			assertEquals(2, descendants.size());
			assertTrue(descendants.contains(child1));
			assertTrue(descendants.contains(child2));

			Collection<TO<?>> ancestors = env.getRelatedObjects(child1, null, false);
			assertNotNull(ancestors);
			assertEquals(1, ancestors.size());
			assertTrue(ancestors.contains(parent1));

			ancestors = env.getRelatedObjects(child2, null, false);
			assertNotNull(ancestors);
			assertEquals(1, ancestors.size());
			assertTrue(ancestors.contains(parent1));

			env.removeRelation(Arrays.asList(relation1, relation2));
			descendants = env.getRelatedObjects(parent1, null, true);
			assertNotNull(descendants);
			assertEquals(0, descendants.size());

			ancestors = env.getRelatedObjects(child1, null, false);
			assertNotNull(ancestors);
			assertEquals(0, ancestors.size());

			ancestors = env.getRelatedObjects(child2, null, false);
			assertNotNull(ancestors);
			assertEquals(0, ancestors.size());

		} finally {
			env.deleteObjectType(TYPE_TEST);
		}
	}

	@Test
	public void testBulkSetRemoveRelationTwoLevelOfHierarchySuccess() throws Exception {
		String TYPE_TEST = "test type";
		try {
			ObjectType t1 = env.createObjectType(TYPE_TEST);

			TO<?> flParent = env.createObject(TYPE_TEST);
			TO<?> slParent = env.createObject(TYPE_TEST);
			TO<?> leafChild = env.createObject(TYPE_TEST);

			Relation relation1 = new Relation(flParent, Arrays.<TO<?>>asList(slParent));
			Relation relation2 = new Relation(slParent, Arrays.<TO<?>>asList(leafChild));

			env.setRelation(Arrays.asList(relation1, relation2));

			Collection<TO<?>> descendants = env.getRelatedObjects(flParent, null, true);
			assertNotNull(descendants);
			assertEquals(2, descendants.size());
			assertTrue(descendants.contains(slParent));
			assertTrue(descendants.contains(leafChild));

			descendants = env.getRelatedObjects(slParent, null, true);
			assertNotNull(descendants);
			assertEquals(1, descendants.size());
			assertTrue(descendants.contains(leafChild));

			Collection<TO<?>> ancestors = env.getRelatedObjects(slParent, null, false);
			assertNotNull(ancestors);
			assertEquals(1, ancestors.size());
			assertTrue(ancestors.contains(flParent));

			ancestors = env.getRelatedObjects(leafChild, null, false);
			assertNotNull(ancestors);
			assertEquals(2, ancestors.size());
			assertTrue(ancestors.contains(flParent));
			assertTrue(ancestors.contains(slParent));

			env.removeRelation(Arrays.asList(relation1, relation2));
			descendants = env.getRelatedObjects(flParent, null, true);
			assertNotNull(descendants);
			assertEquals(0, descendants.size());

			descendants = env.getRelatedObjects(slParent, null, true);
			assertNotNull(descendants);
			assertEquals(0, descendants.size());

			ancestors = env.getRelatedObjects(slParent, null, false);
			assertNotNull(ancestors);
			assertEquals(0, ancestors.size());

			ancestors = env.getRelatedObjects(leafChild, null, false);
			assertNotNull(ancestors);
			assertEquals(0, ancestors.size());

		} finally {
			env.deleteObjectType(TYPE_TEST);
		}
	}

	@Test
	public void testBulkSetRelationPerformance() throws Exception {
		final int ObjectCount = 1000;
		String TYPE_TEST = "test type";
		try {
			ObjectType t1 = env.createObjectType(TYPE_TEST);

			EnvObjectBundle bundle = new EnvObjectBundle();
			for (int i = 0; i < ObjectCount; i++) {
				bundle.addObject(Integer.toString(i), new EnvObject(TYPE_TEST, null));
			}

			long start = System.currentTimeMillis();
			TOBundle objects = env.createObjects(bundle);
			System.out.println("Bulk Create objects : " + (System.currentTimeMillis() - start));

			List<Relation> relations = new java.util.LinkedList<Relation>();
			for (int i = 1; i < ObjectCount - 1; i += 2) {
				relations.add(new Relation(objects.get(i), Arrays.asList(objects.get(i + 1), objects.get(i + 2))));
			}

			start = System.currentTimeMillis();
			env.setRelation(relations);
			System.out.println("Bulk SetRelation : " + (System.currentTimeMillis() - start));

		} finally {
			long start = System.currentTimeMillis();
			env.deleteObjectType(TYPE_TEST);
			System.out.println("Delete type time : " + (System.currentTimeMillis() - start));
		}
	}

	@Test
	public void testSimpleSetRelationPerformance() throws Exception {
		final int ObjectCount = 1000;
		String TYPE_TEST = "test type";
		try {
			ObjectType t1 = env.createObjectType(TYPE_TEST);

			EnvObjectBundle bundle = new EnvObjectBundle();
			for (int i = 0; i < ObjectCount; i++) {
				bundle.addObject(Integer.toString(i), new EnvObject(TYPE_TEST, null));
			}

			long start = System.currentTimeMillis();
			TOBundle objects = env.createObjects(bundle);
			System.out.println("Bulk Create objects : " + (System.currentTimeMillis() - start));


			start = System.currentTimeMillis();
			for (int i = 1; i < ObjectCount - 1; i += 2) {
				env.setRelation(objects.get(i), objects.get(i + 1));
				env.setRelation(objects.get(i), objects.get(i + 2));
			}
			System.out.println("Simple SetRelation : " + (System.currentTimeMillis() - start));

		} finally {
			long start = System.currentTimeMillis();
			env.deleteObjectType(TYPE_TEST);
			System.out.println("Delete type time : " + (System.currentTimeMillis() - start));
		}
	}

	@Test
	@Ignore
	public void testBulkSetRelationHugeObjectNumber() throws Exception {
		final int ObjectCount = 20000;
		String TYPE_TEST = "test type";
		env.configurationStart();

		try {
			ObjectType[] types = env.getObjectTypes(new String[] { TYPE_TEST });
			ObjectType t1;
			if (types.length == 0)
				t1 = env.createObjectType(TYPE_TEST);
			else
				t1 = types[0];

			EnvObjectBundle bundle = new EnvObjectBundle();
			for (int i = 0; i < ObjectCount; i++) {
				bundle.addObject(Integer.toString(i), new EnvObject(TYPE_TEST, null));
			}

			long start = System.currentTimeMillis();
			TOBundle objects = env.createObjects(bundle);
			System.out.println("Bulk Create: " + (System.currentTimeMillis() - start));

			List<Relation> relations = new java.util.LinkedList<Relation>();
			for (int i = 1; i < ObjectCount - 1; i += 1) {
				relations.add(new Relation(objects.get(i), Arrays.<TO<?>>asList(objects.get(i + 1))));
			}

			start = System.currentTimeMillis();
			env.setRelation(relations);
			System.out.println("Bulk SetRelation : " + (System.currentTimeMillis() - start));

		} finally {
			long start = System.currentTimeMillis();
			env.deleteObjectType(TYPE_TEST);
			System.out.println("Delete type time : " + (System.currentTimeMillis() - start));
			env.configurationComplete();
		}
	}

}
