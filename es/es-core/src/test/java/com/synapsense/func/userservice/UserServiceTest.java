package com.synapsense.func.userservice;

import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.UserManagementException;
import com.synapsense.service.UserManagementService;
import com.synapsense.util.CollectionUtils;
import com.synapsense.util.ServerLoginModule;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.naming.NamingException;
import javax.security.auth.login.LoginException;
import javax.security.sasl.SaslException;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import static org.junit.Assert.*;

public class UserServiceTest extends AbstractUserServiceTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		AbstractUserServiceTest.setUpBeforeClass();
	}

	@Before
	public void setUp() throws Exception {
		super.setUp();
	}

	@After
	public void tearDown() throws Exception {
		super.tearDown();
	}

	@Test
	public void testSynapSenseUserExists() {
		Collection<TO<?>> synapsense = this.console.getEnv().getObjects("USER", new ValueTO[]{new ValueTO("userName", "synapsense")});
		assertTrue(synapsense != null);
		assertFalse(synapsense.isEmpty());
		assertEquals(1, synapsense.size());
	}

	@Test
	public void testUserCreation() throws UserManagementException {
		// more appropriate - minimum fields
		String userName = "userservicetest_1";
		TO<?> info = null;

		try {
			// creates user
			info = this.USER_SERVICE.createUser(userName, "new_pass", null);

			// tests exception on duplicate user
			try {
				this.USER_SERVICE.createUser(userName, "new_pass", null);
				assertTrue("Can't create a duplicate user, " + userName, false);
			} catch (UserManagementException e) {
			}

			// changes user name to existing
			try {
				this.USER_SERVICE.setProperty(info, UserManagementService.USER_NAME, "admin");
				assertTrue("Can't change user name to an existing name", false);
			} catch (UserManagementException e) {

			}

			// changes user name
			try {
				this.USER_SERVICE.setProperty(info, UserManagementService.USER_NAME, "userservicetest_2");
				userName = "userservicetest_2";
			} catch (UserManagementException e) {
				assertTrue("Unable to updateUser for " + userName, false);
			}

			// check that user has old data except name
			info = this.USER_SERVICE.getUser(userName);
			assertEquals(userName, this.USER_SERVICE.getProperty(info, UserManagementService.USER_NAME));
		} finally {
			if (info != null) {
				this.USER_SERVICE.deleteUser(info);
			}
			info = this.USER_SERVICE.getUser(userName);
			assertNull(info);
		}
	}

	@Test
	public void testGettingAllUsers() throws Exception {
		Collection<TO<?>> allUsers = console.getUserService().getAllUsers();
		boolean foundAdmin = false;
		for (TO<?> user : allUsers) {
			String userName = (String) this.USER_SERVICE.getProperty(user, UserManagementService.USER_NAME);
			if ("admin".equals(userName)) {
				foundAdmin = true;
				break;
			}
		}

		assertTrue("No admin user!", foundAdmin);
	}

	@Test
	public void testUserPreferences() throws Exception {
		String newUser1 = "userservicetest_3";
		String newUser2 = "userservicetest_4";

		TO<?> user1 = null;
		TO<?> user2 = null;

		try {
			// creates new user
			user1 = this.USER_SERVICE.createUser(newUser1, "new_pass", null);

			// creates new user
			user2 = this.USER_SERVICE.createUser(newUser2, "new_pass", null);

			// check preferences of user
			Map<String, Object> preferences = this.USER_SERVICE.getUserPreferences(user1);
			assertTrue(preferences.isEmpty());

			// adds "mode" preference
			this.USER_SERVICE.setUserPreferenceValue(user1, "DEFAULT_MODE", "");
			preferences = this.USER_SERVICE.getUserPreferences(user1);
			assertEquals(1, preferences.size());
			preferences.containsKey("DEFAULT_MODE");

			// adds "image" preference
			this.USER_SERVICE.setUserPreferenceValue(user1, "NO_IMAGE", "");
			preferences = this.USER_SERVICE.getUserPreferences(user1);
			assertEquals(2, preferences.size());
			preferences.containsKey("DEFAULT_MODE");
			preferences.containsKey("NO_IMAGE");

			// adds "view" preference
			this.USER_SERVICE.setUserPreferenceValue(user1, "PLAIN_VIEW", "");
			preferences = this.USER_SERVICE.getUserPreferences(user1);
			assertEquals(3, preferences.size());
			preferences.containsKey("DEFAULT_MODE");
			preferences.containsKey("NO_IMAGE");
			preferences.containsKey("PLAIN_VIEW");

			this.USER_SERVICE.removeUserPreference(user1, "DEFAULT_MODE");
			preferences = this.USER_SERVICE.getUserPreferences(user1);
			assertEquals(2, preferences.size());
			preferences.containsKey("NO_IMAGE");
			preferences.containsKey("PLAIN_VIEW");

			this.USER_SERVICE.removeUserPreference(user1, "NO_IMAGE");
			preferences = this.USER_SERVICE.getUserPreferences(user1);
			assertEquals(1, preferences.size());
			preferences.containsKey("PLAIN_VIEW");

			this.USER_SERVICE.setUserPreferenceValue(user2, "PLAIN_VIEW", "");

			preferences = this.USER_SERVICE.getUserPreferences(user1);
			assertEquals(1, preferences.size());

			preferences = this.USER_SERVICE.getUserPreferences(user2);
			assertEquals(1, preferences.size());

		} finally {
			this.USER_SERVICE.deleteUser(user1);
			this.USER_SERVICE.deleteUser(user2);
		}

	}

	@Test
	public void testUserAuthAfterChangingUserGroup() throws UserManagementException, LoginException, NamingException {
		// more appropriate - minimum fields
		String userName = "userservicetest_5";
		TO<?> user = null;

		try {
			// creates user
			// with admin rights
			user = this.USER_SERVICE.createUser(userName, "new_pass", null);
			Map<String, Object> allProps = this.USER_SERVICE.getUserInformation(user);
			assertNotNull(allProps);
			assertEquals(userName, allProps.get(UserManagementService.USER_NAME));
			Collection<String> standardPrivs = this.USER_SERVICE.getUserPrivilegeIds(user);

			this.USER_SERVICE.setUserGroups(user, "Power");
			Collection<String> userGroups = this.USER_SERVICE.getUserGroupIds(user);
			assertTrue(userGroups.contains("Power"));

			Collection<String> powerPrivs = this.USER_SERVICE.getUserPrivilegeIds(user);
			assertFalse(powerPrivs.equals(standardPrivs));

			pause(1000);

			this.login(userName, "new_pass", true);

		} finally {
			this.login("admin", "admin", true);
			this.USER_SERVICE.deleteUser(user);
		}
	}

	@Test
	public void testUserAuthAfterDeletingUser() throws UserManagementException, LoginException, NamingException {
		// more appropriate - minimum fields
		String userName = "userservicetest_6";
		TO<?> user = null;

		try {
			// creates user
			user = this.USER_SERVICE.createUser(userName, "new_pass", null);
			// with admin rights
			Map<String, Object> allProps = this.USER_SERVICE.getUserInformation(user);
			assertNotNull(allProps);
			assertEquals(userName, allProps.get(UserManagementService.USER_NAME));

			// deletes user
			this.USER_SERVICE.deleteUser(user);
			user = null;

			this.login(userName, "new_pass", false);
			try {
				ServerLoginModule.login(userName, "new_pass");
				assertTrue("Shouldn't be able to login as deleted user!", false);
			} catch (NamingException | LoginException e) {
			}

		} finally {
			try {
				if (user != null) {
					this.USER_SERVICE.deleteUser(user);
				}
			} catch (UserManagementException e) {
			}
		}

	}

	@Test
	public void testUserAuthAfterChangingPassword() throws UserManagementException, LoginException, NamingException,
														   SaslException {
		// more appropriate - minimum fields
		String userName = "userservicetest_7";
		TO<?> user = null;

		try {
			// with admin rights
			String pwd = "new_pass";
			user = this.USER_SERVICE.createUser(userName, pwd, null);
			Map<String, Object> allProps = this.USER_SERVICE.getUserInformation(user);
			assertNotNull(allProps);
			assertEquals(userName, allProps.get(UserManagementService.USER_NAME));

			// with admin rights
			String rawPassword = null;
			try {
				rawPassword = ServerLoginModule.PasswordService.encrypt(pwd);
			} catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
			}
			assertEquals(rawPassword, this.USER_SERVICE.getRawPassword(user));
			this.pause(1000);

			// new credentials - admin
			this.login(userName, pwd, true);

			pwd = "new_another_pass";
			rawPassword = null;
			try {
				rawPassword = ServerLoginModule.PasswordService.encrypt(pwd);
			} catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
			}
			this.USER_SERVICE.setPassword(user, pwd);

			// Can't check raw password unless in Administrators group
			this.login("admin", "admin", true);
			assertEquals(rawPassword, this.USER_SERVICE.getRawPassword(user));

			Collection<String> groups = new ArrayList<>();
			groups.add("Standard");
			this.USER_SERVICE.setUserGroups(user, groups);
			Collection<String> userGroups = this.USER_SERVICE.getUserGroupIds(user);
			assertTrue(userGroups.contains("Standard"));

		} finally {
			this.login("admin", "admin", true);
			if (user != null) {
				this.USER_SERVICE.deleteUser(user);
			}
		}

	}

	@Test
	public void testUserAuthAfterUpdatingUserData() throws UserManagementException, LoginException, NamingException,
														   SaslException {
		// more appropriate - minimum fields
		String userName = "userservicetest_8";
		TO<?> user = null;

		try {
			// creates user
			// with admin rights
			user = this.USER_SERVICE.createUser(userName, "new_pass", "Standard");
			Map<String, Object> allProps = this.USER_SERVICE.getUserInformation(user);
			assertNotNull(allProps);
			assertEquals(userName, allProps.get(UserManagementService.USER_NAME));

			// updates user name
			this.USER_SERVICE.setProperty(user, UserManagementService.USER_NAME, "userservicetest_9");
			assertEquals("userservicetest_9", (String) this.USER_SERVICE.getProperty(user, UserManagementService.USER_NAME));
			userName = "userservicetest_9";

		} finally {
			this.login("admin", "admin", true);
			this.USER_SERVICE.deleteUser(user);
		}

	}

	@Test
	public void testSettingUserPassword() throws UserManagementException, LoginException, NamingException,
												 SaslException {
		// more appropriate - minimum fields
		String userName = "userservicetest_fred",
				pwd1 = "yabba",
				user2Name = "userservicetest_barney",
				pwd2 = "dabba";
		TO<?> user = null;
		TO<?> user2 = null;

		try {
			// creates user
			// with admin rights
			user = USER_SERVICE.createUser(userName, pwd1, null);
			Map<String, Object> userProps = CollectionUtils.newMap();
			userProps.put(UserManagementService.USER_FIRST_NAME, "fred");
			userProps.put(UserManagementService.USER_LAST_NAME, "flintstone");
			USER_SERVICE.updateUser(user, userProps);

			user2 = USER_SERVICE.createUser(user2Name, pwd2, null);
			USER_SERVICE.setProperty(user2, UserManagementService.USER_FIRST_NAME, "barney");
			USER_SERVICE.setProperty(user2, UserManagementService.USER_LAST_NAME, "rubble");

			this.pause(1000);

			// with admin rights
			Map<String, Object> allProps = this.USER_SERVICE.getUserInformation(user);
			assertNotNull(allProps);
			assertEquals(userName, allProps.get(UserManagementService.USER_NAME));

			// new credentials - user
			this.login(userName, pwd1, true);

			// changes its own password
			this.USER_SERVICE.setPassword(user, "wilma");
			this.pause();

			this.login("admin", "admin", true);

			// its ok , admin can do it
			this.USER_SERVICE.setPassword(user, pwd1);
			this.pause();

			// new credentials - user
			this.login(user2Name, pwd2, true);

			try {
				// it's not ok , another user cannot change another's password
				this.USER_SERVICE.setPassword(user, "betty");
				assertTrue("As " + userName + ", can't change password of " + user2Name, false);
			} catch (Exception e) {
			}

		} finally {
			this.login("admin", "admin", true);
			if (user != null) {
				this.USER_SERVICE.deleteUser(user);
			}
			if (user2 != null) {
				this.USER_SERVICE.deleteUser(user2);
			}
		}

	}

}
