package com.synapsense.func.erlnode.commands;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.ericsson.otp.erlang.OtpErlangAtom;
import com.ericsson.otp.erlang.OtpErlangDouble;
import com.ericsson.otp.erlang.OtpErlangInt;
import com.ericsson.otp.erlang.OtpErlangList;
import com.ericsson.otp.erlang.OtpErlangLong;
import com.ericsson.otp.erlang.OtpErlangObject;
import com.ericsson.otp.erlang.OtpErlangString;
import com.ericsson.otp.erlang.OtpErlangTuple;
import com.synapsense.dto.CollectionTO;
import com.synapsense.dto.ObjectType;
import com.synapsense.dto.PropertyDescr;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.dto.ValueTO;
import com.synapsense.impl.erlnode.ExceptionTypes;
import com.synapsense.impl.erlnode.commands.GetAllPropertiesValuesCommand;
import com.synapsense.impl.erlnode.utils.CollectionToConverter;
import com.synapsense.impl.erlnode.utils.ToConverter;
import com.synapsense.impl.erlnode.utils.ValueTOConverter;
import com.synapsense.service.Environment;

public class GetAllPropertiesValuesCommandTest {
	static UnitTestHelper utHelper = null;
	static String testType = "testingObjType_GetAllPropertiesValuesTwoParTest";
	static Map<String, Object> envMap;
	static Environment env;

	static OtpErlangTuple valueTO1;
	static OtpErlangTuple valueTO2;
	static OtpErlangTuple valueTO3;
	static OtpErlangTuple valueTO4;
	static OtpErlangTuple valueTO5;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		utHelper = new UnitTestHelper();
		utHelper.initializeEnvironment();
		env = utHelper.getEnvironment();
		envMap = new HashMap<String, Object>();
		envMap.put("env", env);

		valueTO1 = new OtpErlangTuple(new OtpErlangObject[] { new OtpErlangAtom("value_to"),
		        new OtpErlangString("name"), new OtpErlangString("someName") });

		valueTO2 = new OtpErlangTuple(new OtpErlangObject[] { new OtpErlangAtom("value_to"),
		        new OtpErlangString("name"), new OtpErlangString("someName2") });

		valueTO3 = new OtpErlangTuple(new OtpErlangObject[] { new OtpErlangAtom("value_to"),
		        new OtpErlangString("int"), new OtpErlangInt(1) });

		valueTO4 = new OtpErlangTuple(new OtpErlangObject[] { new OtpErlangAtom("value_to"),
		        new OtpErlangString("long"), new OtpErlangLong(123123123123L) });

		valueTO5 = new OtpErlangTuple(new OtpErlangObject[] { new OtpErlangAtom("value_to"),
		        new OtpErlangString("double"), new OtpErlangDouble(1212.2), new OtpErlangLong(1212L) });
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		utHelper.removeTypeCompletly(testType);
		utHelper.dispose();
	}

	@Before
	public void setUp() throws Exception {

		ObjectType otype = env.createObjectType(testType);

		otype.getPropertyDescriptors().add(new PropertyDescr("name", String.class.getName()));
		otype.getPropertyDescriptors().add(new PropertyDescr("int", Integer.class.getName()));
		otype.getPropertyDescriptors().add(new PropertyDescr("long", Long.class.getName()));
		otype.getPropertyDescriptors().add(new PropertyDescr("double", Double.class.getName()));
		otype.getPropertyDescriptors().add(new PropertyDescr("to", TO.class.getName()));
		env.updateObjectType(otype);
	}

	@After
	public void tearDown() throws Exception {
		env.deleteObjectType(testType);
	}

	@Test
	public void SuccessGetAllPropertiesValuesTest() throws Exception {
		GetAllPropertiesValuesCommand command = new GetAllPropertiesValuesCommand(envMap);
		TO<?> obj1 = env.createObject(testType, new ValueTO[] { new ValueTO("name", "obj1"),
		        new ValueTO("int", new Integer(1)), new ValueTO("double", new Double(3.3)), });
		TO<?> obj2 = env.createObject(testType, new ValueTO[] { new ValueTO("name", "obj2"),
		        new ValueTO("long", new Long(1L)) });

		OtpErlangTuple res = (OtpErlangTuple) command.run(new OtpErlangTuple(new OtpErlangObject[] {
		        new OtpErlangString("GetAllPropertiesValues"), ToConverter.serializeTo(obj1) }));
		assertEquals("ok", res.elementAt(0).toString());
		Collection<ValueTO> values = ValueTOConverter.parseValueTOList(env, testType, (OtpErlangList) res.elementAt(1));
		for (ValueTO valueTO : values) {
			if ("name".equals(valueTO.getPropertyName()))
				assertEquals("obj1", valueTO.getValue());
			else if ("int".equals(valueTO.getPropertyName()))
				assertEquals(new Integer(1), valueTO.getValue());
			else if ("double".equals(valueTO.getPropertyName()))
				assertEquals(new Double(3.3), valueTO.getValue());
			else if ("long".equals(valueTO.getPropertyName()))
				assertNull(valueTO.getValue());
		}

		res = (OtpErlangTuple) command.run(new OtpErlangTuple(new OtpErlangObject[] {
		        new OtpErlangString("GetAllPropertiesValues"), ToConverter.serializeTo(obj2) }));
		assertEquals("ok", res.elementAt(0).toString());
		values = ValueTOConverter.parseValueTOList(env, testType, (OtpErlangList) res.elementAt(1));
		for (ValueTO valueTO : values) {
			if ("name".equals(valueTO.getPropertyName()))
				assertEquals("obj2", valueTO.getValue());
			else if ("long".equals(valueTO.getPropertyName()))
				assertEquals(new Long(1L), valueTO.getValue());

		}

		List<TO<?>> list = new ArrayList<TO<?>>();
		list.add(obj1);
		list.add(obj2);
		res = (OtpErlangTuple) command.run(new OtpErlangTuple(new OtpErlangObject[] {
		        new OtpErlangString("GetAllPropertiesValues"), ToConverter.serializeToList(list) }));
		assertEquals("ok", res.elementAt(0).toString());
		OtpErlangList otpList = (OtpErlangList) res.elementAt(1);
		assertEquals(2, otpList.arity());
		CollectionTO res1 = CollectionToConverter.parseCollectionTo(env, testType,
		        (OtpErlangTuple) otpList.elementAt(0));
		CollectionTO res2 = CollectionToConverter.parseCollectionTo(env, testType,
		        (OtpErlangTuple) otpList.elementAt(1));

		assertEquals(res1.getPropValues().size(), res2.getPropValues().size());
		assertEquals("obj1", res1.getPropValue("name").get(0).getValue());
		assertEquals("obj2", res2.getPropValue("name").get(0).getValue());

		env.deleteObject(obj1);
		env.deleteObject(obj2);
	}

	@Test
	public void FailedEmtyListAllPropertiesValuesTest() throws Exception {
		GetAllPropertiesValuesCommand command = new GetAllPropertiesValuesCommand(envMap);

		OtpErlangTuple res = (OtpErlangTuple) command.run(new OtpErlangTuple(new OtpErlangObject[] {
		        new OtpErlangString("GetAllPropertiesValues"), new OtpErlangList() }));

		assertEquals("exception", res.elementAt(0).toString());
		assertEquals(ExceptionTypes.ILLEGAL_ARGUMENTS.getExceptionType(),
		        ((OtpErlangString) res.elementAt(1)).stringValue());
	}

	@Test
	public void FailGetAllPropertiesValues() throws Exception {
		TO<?> obj2 = env.createObject(testType);
		env.deleteObject(obj2);
		GetAllPropertiesValuesCommand command = new GetAllPropertiesValuesCommand(envMap);
		OtpErlangTuple res = (OtpErlangTuple) command.run(new OtpErlangTuple(new OtpErlangObject[] {
		        new OtpErlangString("GetAllPropertiesValues"), ToConverter.serializeTo(obj2) }));
		assertEquals("exception", res.elementAt(0).toString());
		assertEquals(ExceptionTypes.OBJECT_NOT_FOUND.getExceptionType(),
		        ((OtpErlangString) res.elementAt(1)).stringValue());

		res = (OtpErlangTuple) command.run(new OtpErlangTuple(new OtpErlangObject[] {
		        new OtpErlangString("GetAllPropertiesValues"),
		        ToConverter.serializeTo(TOFactory.getInstance().loadTO("someType:1")) }));

		assertEquals("exception", res.elementAt(0).toString());
		assertEquals(ExceptionTypes.OBJECT_NOT_FOUND.getExceptionType(),
		        ((OtpErlangString) res.elementAt(1)).stringValue());

	}

	@Test
	public void InvalidParametersTest() throws Exception {
		GetAllPropertiesValuesCommand command = new GetAllPropertiesValuesCommand(envMap);
		OtpErlangTuple obj = (OtpErlangTuple) command.run(new OtpErlangTuple(
		        new OtpErlangObject[] { new OtpErlangString("createObjectCommand1"), }));
		assertEquals("exception", obj.elementAt(0).toString());
		assertEquals(ExceptionTypes.ILLEGAL_ARGUMENTS.getExceptionType(),
		        ((OtpErlangString) obj.elementAt(1)).stringValue());
		obj = (OtpErlangTuple) command.run(new OtpErlangTuple(new OtpErlangObject[] {
		        new OtpErlangString("createObjectCommand1"), new OtpErlangString(testType) }));
		assertEquals("exception", obj.elementAt(0).toString());
		assertEquals(ExceptionTypes.ILLEGAL_ARGUMENTS.getExceptionType(),
		        ((OtpErlangString) obj.elementAt(1)).stringValue());
	}

}
