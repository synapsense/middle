package com.synapsense.func.reporting.dao;

import static com.synapsense.impl.reporting.TestUtils.EMPTY_REPORT_DESIGN_PATH;
import static com.synapsense.impl.reporting.TestUtils.FILE_STORAGE;
import static com.synapsense.impl.reporting.TestUtils.EMPTY_REPORT_NAME;

import java.util.Collection;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.synapsense.exception.ObjectExistsException;
import com.synapsense.impl.reporting.TestUtils;
import com.synapsense.impl.reporting.dao.DbESDAOFactory;
import com.synapsense.impl.reporting.dao.ReportDAO;
import com.synapsense.impl.reporting.dao.ReportTaskDAO;
import com.synapsense.impl.reporting.dao.ReportTemplateDAO;
import com.synapsense.impl.reporting.dao.ReportingDAOFactory;
import com.synapsense.service.reporting.dto.ReportTaskInfo;
import com.synapsense.service.reporting.dto.ReportTaskTitle;
import com.synapsense.service.reporting.dto.ReportTemplateInfo;
import com.synapsense.service.reporting.exception.DAOReportingException;
import com.synapsense.service.reporting.exception.ObjectNotFoundDAOReportingException;
import com.synapsense.service.reporting.management.ReportParameter;

public class DbReportTaskDAOTest {

	private static ReportingDAOFactory daoFactory;
	private static ReportTaskDAO reportTaskDao;
	private static ReportTemplateDAO rtDao;

	@BeforeClass
	public static void setUpBeforeClass() throws DAOReportingException, ObjectExistsException {
		daoFactory = new DbESDAOFactory(TestUtils.FILE_STORAGE, TestUtils.getEnvironment(), TestUtils.getFileWriter());

		rtDao = daoFactory.newReportTemplateDAO();
		ReportTemplateInfo rtInfo = new ReportTemplateInfo("TestReportTemplate");
		rtInfo.addParameter(new ReportParameter("Param", String.class));
		rtDao.createReportTemplate(rtInfo, "TestDesign");
		ReportTemplateInfo reportTemplate = new ReportTemplateInfo(TestUtils.EMPTY_REPORT_NAME);
		rtDao.createReportTemplate(reportTemplate, TestUtils.readTestFile(FILE_STORAGE + EMPTY_REPORT_DESIGN_PATH));		

		reportTaskDao = daoFactory.newReportTaskDao();
	}

	@AfterClass
	public static void tearDownAfterClass() throws DAOReportingException {
		for (ReportTaskTitle task : reportTaskDao.getActiveReportTasks("TestReportTemplate")) {
			reportTaskDao.removeReportTask(task.getTaskId());
		}
		rtDao.removeReportTemplate("TestReportTemplate");
		
		for (ReportTaskTitle task : reportTaskDao.getActiveReportTasks(EMPTY_REPORT_NAME)) {
			reportTaskDao.removeReportTask(task.getTaskId());
		}
		rtDao.removeReportTemplate(EMPTY_REPORT_NAME);
		daoFactory.shutdown();
	}

	@Test
	public void testCreateReportTaskDao() throws DAOReportingException {
		ReportTaskDAO tDao = daoFactory.newReportTaskDao();
		Assert.assertNotNull("Report Task DAO is null", tDao);
	}
	
	@Test
	public void testSaveReportTask() throws ObjectNotFoundDAOReportingException, ObjectExistsException {
		
		//Creation
		Collection<ReportTaskTitle> coll = reportTaskDao.getActiveReportTasks(EMPTY_REPORT_NAME);
		int size = coll.size();
		String taskName = "Test";
        ReportTaskInfo reportTaskInfo = ReportTaskInfo.newInstance(EMPTY_REPORT_NAME, taskName);
        reportTaskDao.saveReportTask(reportTaskInfo);
        
        //Verification
        int taskId = reportTaskInfo.getTaskId();
        Assert.assertFalse("Report id", 0 == taskId);

		coll = reportTaskDao.getActiveReportTasks(EMPTY_REPORT_NAME);
		Assert.assertEquals("Report tasks size", coll.size(), size + 1);
		
		reportTaskDao.removeReportTask(taskId);
		coll = reportTaskDao.getActiveReportTasks(EMPTY_REPORT_NAME);
		Assert.assertEquals("Report tasks size", coll.size(), size);
		
	}
	
	@Test(expected = ObjectExistsException.class)
	public void testDuplicateReportTask() throws ObjectNotFoundDAOReportingException, ObjectExistsException {
		
		//Task Creation
		int size1 = reportTaskDao.getActiveReportTasks(EMPTY_REPORT_NAME).size();
		String taskName = "Test";
        ReportTaskInfo reportTaskInfo = ReportTaskInfo.newInstance(EMPTY_REPORT_NAME, taskName);
        reportTaskDao.saveReportTask(reportTaskInfo);
        Assert.assertTrue("Report template size", reportTaskDao.getActiveReportTasks(EMPTY_REPORT_NAME).size() == size1 + 1);
        
        //Task with same name creation for another template(expected OK creation)
        int size2 = reportTaskDao.getActiveReportTasks("TestReportTemplate").size();
        ReportTaskInfo anotherTemplateReportTaskInfo = ReportTaskInfo.newInstance("TestReportTemplate", taskName);
        reportTaskDao.saveReportTask(anotherTemplateReportTaskInfo);
        Assert.assertTrue("Report template size", reportTaskDao.getActiveReportTasks("TestReportTemplate").size() == size2 + 1);
        reportTaskDao.removeReportTask(anotherTemplateReportTaskInfo.getTaskId());
        
        //Task with same name creation for this template(expected ObjectExistsException)
        reportTaskDao.saveReportTask(reportTaskInfo);
	}
	
}