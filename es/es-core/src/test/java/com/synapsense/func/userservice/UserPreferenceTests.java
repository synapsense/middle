package com.synapsense.func.userservice;

import com.synapsense.dto.TO;
import com.synapsense.exception.IllegalInputParameterException;
import com.synapsense.exception.UserManagementException;
import com.synapsense.service.UserManagementService;
import com.synapsense.util.CollectionUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.ejb.EJBException;
import java.util.Collection;
import java.util.Map;

import static org.junit.Assert.*;

public class UserPreferenceTests extends AbstractUserServiceTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		AbstractUserServiceTest.setUpBeforeClass();
	}

	@Before
	public void setUp() throws Exception {
		super.setUp();
	}

	@After
	public void tearDown() throws Exception {
		super.tearDown();
	}

	@Test
	public void testGetUserPreferences() throws Exception {
		String userName = "userpreferencetests_1";
		TO<?> user = null;

		try {
			// creates user 'userpreferencetests_1' with password 'new_pass' and puts in 'Standard' group
			user = this.USER_SERVICE.createUser(userName, "new_pass", null);
			assertNotNull(user);

			Map<String, Object> prefs = this.USER_SERVICE.getUserPreferences(user);
			assertNotNull(prefs);
			assertEquals(0, prefs.size());
		} finally {
			if (user != null) {
				this.USER_SERVICE.deleteUser(user);
			}
			user = this.USER_SERVICE.getUser(userName);
			assertNull(user);
		}
	}

	@Test
	public void testSetUserPreferences() throws Exception {
		String userName = "userpreferencetests_2";
		TO<?> user = null;

		try {
			// creates user 'userpreferencetests_2' with password 'new_pass' and puts in 'Standard' group
			user = this.USER_SERVICE.createUser(userName, "new_pass", null);
			assertNotNull(user);

			this.USER_SERVICE.setUserPreferenceValue(user, "preference1", "pie");
			Map<String, Object> prefs = this.USER_SERVICE.getUserPreferences(user);
			assertNotNull(prefs);
			assertEquals(1, prefs.size());
			assertEquals("pie", prefs.get("preference1"));

			this.USER_SERVICE.setUserPreferenceValue(user, "preference2", "steak");
			prefs = this.USER_SERVICE.getUserPreferences(user);
			assertNotNull(prefs);
			assertEquals(2, prefs.size());
			assertEquals("pie", prefs.get("preference1"));
			assertEquals("steak", prefs.get("preference2"));

		} finally {
			if (user != null) {
				this.USER_SERVICE.deleteUser(user);
			}
			user = this.USER_SERVICE.getUser(userName);
			assertNull(user);
		}
	}

	@Test
	public void testChangeUserPreferences() throws Exception {
		String userName = "userpreferencetests_3";
		TO<?> user = null;

		try {
			// creates user 'userpreferencetests_3' with password 'new_pass' and puts in 'Standard' group
			user = this.USER_SERVICE.createUser(userName, "new_pass", null);
			assertNotNull(user);

			this.USER_SERVICE.setUserPreferenceValue(user, "preference1", "pie");
			Map<String, Object> prefs = this.USER_SERVICE.getUserPreferences(user);
			assertNotNull(prefs);
			assertEquals(1, prefs.size());
			assertEquals("pie", prefs.get("preference1"));

			this.USER_SERVICE.setUserPreferenceValue(user, "preference2", "steak");
			prefs = this.USER_SERVICE.getUserPreferences(user);
			assertNotNull(prefs);
			assertEquals(2, prefs.size());
			assertEquals("pie", prefs.get("preference1"));
			assertEquals("steak", prefs.get("preference2"));

			this.USER_SERVICE.setUserPreferenceValue(user, "preference1", "ice cream");
			prefs = this.USER_SERVICE.getUserPreferences(user);
			assertNotNull(prefs);
			assertEquals(2, prefs.size());
			assertEquals("ice cream", prefs.get("preference1"));
			assertEquals("steak", prefs.get("preference2"));

		} finally {
			if (user != null) {
				this.USER_SERVICE.deleteUser(user);
			}
			user = this.USER_SERVICE.getUser(userName);
			assertNull(user);
		}
	}

	@Test
	public void testInvalidSetUserPreferences() throws Exception {
		String userName = "userpreferencetests_4";
		TO<?> user = null;

		try {
			// creates user 'userpreferencetests_4' with password 'new_pass' and puts in 'Standard' group
			user = this.USER_SERVICE.createUser(userName, "new_pass", null);
			assertNotNull(user);

			try {
				this.USER_SERVICE.setUserPreferenceValue(null, "preference1", "pie");
				assertTrue("Can't have null user when setting user preference.", false);
			} catch (IllegalInputParameterException e) {
			}

			try {
				this.USER_SERVICE.setUserPreferenceValue(user, null, "pie");
				assertTrue("Can't have null or empty preference name when setting user preference.", false);
			} catch (IllegalInputParameterException e) {
			}

			try {
				this.USER_SERVICE.setUserPreferenceValue(user, "", "pie");
				assertTrue("Can't have null or empty preference name when setting user preference.", false);
			} catch (IllegalInputParameterException e) {
			}

			try {
				this.USER_SERVICE.setUserPreferenceValue(user, "   ", "pie");
				assertTrue("Can't have null or empty preference name when setting user preference.", false);
			} catch (IllegalInputParameterException e) {
			}

			try {
				this.USER_SERVICE.setUserPreferenceValue(user, "preference1", null);
				assertTrue("Can't have null preference value when setting user preference.", false);
			} catch (IllegalInputParameterException e) {
			}

			try {
				this.USER_SERVICE.setUserPreferenceValue(this.USER_SERVICE.getGroup("Standard"), "preference1", "pie");
				assertTrue("Can't have set user preference on a group.", false);
			} catch (IllegalInputParameterException e) {
			}


		} finally {
			if (user != null) {
				this.USER_SERVICE.deleteUser(user);
			}
			user = this.USER_SERVICE.getUser(userName);
			assertNull(user);
		}
	}

	@Test
	public void testRemoveUserPreferences() throws Exception {
		String userName = "userpreferencetests_5";
		TO<?> user = null;

		try {
			// creates user 'userpreferencetests_5' with password 'new_pass' and puts in 'Standard' group
			user = this.USER_SERVICE.createUser(userName, "new_pass", null);
			assertNotNull(user);

			this.USER_SERVICE.setUserPreferenceValue(user, "preference1", "pie");
			Map<String, Object> prefs = this.USER_SERVICE.getUserPreferences(user);
			assertNotNull(prefs);
			assertEquals(1, prefs.size());
			assertEquals("pie", prefs.get("preference1"));

			this.USER_SERVICE.setUserPreferenceValue(user, "preference2", "steak");
			prefs = this.USER_SERVICE.getUserPreferences(user);
			assertNotNull(prefs);
			assertEquals(2, prefs.size());
			assertEquals("pie", prefs.get("preference1"));
			assertEquals("steak", prefs.get("preference2"));

			this.USER_SERVICE.removeUserPreference(user, "preference1");
			prefs = this.USER_SERVICE.getUserPreferences(user);
			assertNotNull(prefs);
			assertEquals(1, prefs.size());
			assertEquals("steak", prefs.get("preference2"));

			this.USER_SERVICE.removeUserPreference(user, "preference2");
			prefs = this.USER_SERVICE.getUserPreferences(user);
			assertNotNull(prefs);
			assertEquals(0, prefs.size());

		} finally {
			if (user != null) {
				this.USER_SERVICE.deleteUser(user);
			}
			user = this.USER_SERVICE.getUser(userName);
			assertNull(user);
		}
	}

	@Test
	public void testInvalidRemoveUserPreferences() throws Exception {
		String userName = "userpreferencetests_6";
		TO<?> user = null;

		try {
			// creates user 'userpreferencetests_6' with password 'new_pass' and puts in 'Standard' group
			user = this.USER_SERVICE.createUser(userName, "new_pass", null);
			assertNotNull(user);

			try {
				this.USER_SERVICE.removeUserPreference(null, "preference2");
				assertTrue("Can't specify invalid user.", false);
			} catch (IllegalInputParameterException e) {
			}

			try {
				this.USER_SERVICE.removeUserPreference(user, null);
				assertTrue("Preference name can't be null or empty.", false);
			} catch (IllegalInputParameterException e) {
			}

			try {
				this.USER_SERVICE.removeUserPreference(user, "");
				assertTrue("Preference name can't be null or empty.", false);
			} catch (IllegalInputParameterException e) {
			}

			try {
				this.USER_SERVICE.removeUserPreference(user, "    ");
				assertTrue("Preference name can't be null or empty.", false);
			} catch (IllegalInputParameterException e) {
			}

		} finally {
			if (user != null) {
				this.USER_SERVICE.deleteUser(user);
			}
			user = this.USER_SERVICE.getUser(userName);
			assertNull(user);
		}
	}

	@Test
	public void tryToRemoveUserPreferenceTwice() throws UserManagementException {
		String userName = "userpreferencetests_6";
		TO<?> user = null;

		try {
			// creates user 'userpreferencetests_6' with password 'new_pass' and puts in 'Standard' group
			user = this.USER_SERVICE.createUser(userName, "new_pass", null);
			assertNotNull(user);
			assertEquals(userName, this.USER_SERVICE.getProperty(user, UserManagementService.USER_NAME));

			this.USER_SERVICE.setUserPreferenceValue(user, "preference1", "pie");
			Map<String, Object> prefs = this.USER_SERVICE.getUserPreferences(user);
			assertNotNull(prefs);
			assertEquals(1, prefs.size());
			assertEquals("pie", prefs.get("preference1"));

			this.USER_SERVICE.removeUserPreference(user, "preference1");
			prefs = this.USER_SERVICE.getUserPreferences(user);
			assertNotNull(prefs);
			assertEquals(0, prefs.size());

			try {
				this.USER_SERVICE.removeUserPreference(user, "preference2");
			} catch (UserManagementException e) {
				assertTrue("Should be able to remove non-existing user preference.", false);
			}

		} finally {
			if (user != null) {
				this.USER_SERVICE.deleteUser(user);
			}
			user = this.USER_SERVICE.getUser(userName);
			assertNull(user);
		}
	}

}
