package com.synapsense.func;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.util.*;

import javax.management.MBeanServerConnection;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.security.auth.login.LoginContext;
import javax.security.auth.login.LoginException;
import javax.security.sasl.SaslException;

import org.apache.log4j.Logger;
import org.jboss.ejb.client.ContextSelector;
import org.jboss.ejb.client.EJBClientConfiguration;
import org.jboss.ejb.client.EJBClientContext;
import org.jboss.ejb.client.PropertiesBasedEJBClientConfiguration;
import org.jboss.ejb.client.remoting.ConfigBasedEJBClientContextSelector;

import com.synapsense.dal.generic.RuleInstanceDAO;
import com.synapsense.dal.generic.entities.NhtInfoDO;
import com.synapsense.dal.objectcache.CacheSvc;
import com.synapsense.dal.objectcache.CacheSvcMgmt;
import com.synapsense.dto.CRERule;
import com.synapsense.dto.ObjectType;
import com.synapsense.dto.PropertyDescr;
import com.synapsense.dto.RuleType;
import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.EnvException;
import com.synapsense.exception.ObjectExistsException;
import com.synapsense.service.AlertService;
import com.synapsense.service.DRulesEngine;
import com.synapsense.service.DRulesEngineManagement;
import com.synapsense.service.Environment;
import com.synapsense.service.RuleTypeService;
import com.synapsense.service.RulesEngine;
import com.synapsense.service.RulesEngineManagement;
import com.synapsense.service.UserManagementService;
import com.synapsense.service.impl.activitylog.ActivityLogService;
import com.synapsense.service.impl.dao.DataObjectDAO;
import com.synapsense.service.impl.dao.NetworkStatisticDAOIF;
import com.synapsense.service.impl.dao.to.PropertyTO;
import com.synapsense.service.impl.messages.base.StatisticData;
import com.synapsense.service.impl.registrar.RegistrarSvc;
import com.synapsense.util.JNDINames;
import com.synapsense.util.ServerLoginModule;

public class ConsoleApp {
	protected final static Logger log = Logger.getLogger(ConsoleApp.class);

	public static InitialContext ctx;

	public static AlertService ALERTING_SERVICE = null;
	public static CacheSvc CACHE_SERVICE = null;
	public static UserManagementService USER_SERVICE = null;
	public static DataObjectDAO<Integer, StatisticData> NET_STAT_NODE = null;
	public static NetworkStatisticDAOIF<Integer, StatisticData> NET_STAT_NODE_SPECIAL = null;
	public static DataObjectDAO<Integer, StatisticData> NET_STAT_BASE = null;
	public static DataObjectDAO<Integer, NhtInfoDO> NHT_INFO_DAO = null;
	public static Environment env = null;
	public static RegistrarSvc reg = null;
	public static DRulesEngine dre = null;
	public static RulesEngine cre = null;
	public static ActivityLogService ACTIVITY_LOG_SERVICE = null;

	protected static LoginContext lc;
	public static RuleTypeService<Integer> ruleTypeService;
	public static RuleInstanceDAO<Integer> ruleInstanceDAO;
	protected static String pduDeviceType = "PDU_SNMP";
	protected static String pduDeviceName = "APC_PDU";
	protected static TO<?> PDU_objectStaff = null;

	protected static TO network = null;
	protected static TO node = null;
	protected static TO sensor = null;
	protected static TO<?> toSNMP = null;

	public static DRulesEngineManagement drembean;
	public static RulesEngineManagement crembean;

	public static TO<?> getObject(final Environment env, final String type, final String name) throws EnvException {
		ValueTO[] criteria = new ValueTO[] { new ValueTO("name", name) };
		Collection<TO<?>> objList = env.getObjects(type, criteria);

		if (objList != null && objList.size() > 0) {
			return (TO) objList.toArray()[0];
		}
		return null;
	}

	public static void createTestSNMP() {
		try {
			// env.deleteObjectType("SNMP");
			ObjectType SNMP = null;
			ObjectType[] types = env.getObjectTypes(new String[] { "SNMP" });
			if (types.length == 0) {
				// create object type CRAC
				SNMP = env.createObjectType("SNMP");
				log.info("Object type:" + SNMP.getName() + " was created");
			} else {
				SNMP = types[0];
			}
			SNMP.getPropertyDescriptors().add(new PropertyDescr("networkID", "java.lang.Integer"));
			SNMP.getPropertyDescriptors().add(
			        new PropertyDescr("networkName", "synapsnmp.networks.networksTable[1].networkName",
			                "java.lang.String"));
			SNMP.getPropertyDescriptors().add(
			        new PropertyDescr("networkDescription", "synapsnmp.networks.networksTable[1].networkDescription",
			                "java.lang.String"));
			SNMP.getPropertyDescriptors().add(
			        new PropertyDescr("networkHostname", "synapsnmp.networks.networksTable[1].networkHostname",
			                "java.lang.Integer"));
			SNMP.getPropertyDescriptors().add(
			        new PropertyDescr("networkHostport", "synapsnmp.networks.networksTable[1].networkHostport",
			                "java.lang.Integer"));
			SNMP.getPropertyDescriptors().add(
			        new PropertyDescr("networkDefaultSampleInterval",
			                "synapsnmp.networks.networksTable[1].networkDefaultSampleInterval", "java.lang.Integer"));
			env.updateObjectType(SNMP);

			toSNMP = getObject(env, "SNMP", "Synapsense SNMP Agent");
			if (toSNMP == null) {
				// Create CRAC object
				toSNMP = env.createObject("SNMP", new ValueTO[] { new ValueTO("name", "Synapsense SNMP Agent") });
				log.info("New object:" + "Synapsense SNMP Agent" + " of type:" + "SNMP" + " was created");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	protected static boolean initialized = false;
	protected static boolean loggedin = false;

	protected static Map<String, ContextSelector<EJBClientContext>> mapUserPassToContext = new HashMap<>();

	public static void init() throws Exception {

		if (initialized)
			return;

		ConsoleApp.login("admin", "admin");

		initialized = true;
	}

	public static void logout() throws LoginException, NamingException {
		if (loggedin) {
			ServerLoginModule.logout();

			env = null;
			cre = null;
			ALERTING_SERVICE = null;
			USER_SERVICE = null;

			if (ctx != null) {
				ctx.close();
				ctx = null;
			}

			loggedin = false;
		}
	}

	private static void setClientContext(String user, String pwd) throws SaslException {
		ContextSelector<EJBClientContext> selector = mapUserPassToContext.get(user+"/"+pwd);

		if (selector == null) {
			Properties p = new Properties();
			{
				p.put("endpoint.name", "client_1");
				p.put("remote.connectionprovider.create.options.org.xnio.Options.SSL_ENABLED", "false");
				p.put("remote.connections", "default");
				p.put("remote.connection.default.host", "localhost");
				p.put("remote.connection.default.port", "4447");
				p.put("remote.connection.default.connect.options.org.xnio.Options.SASL_POLICY_NOANONYMOUS", "true");
				p.put("remote.connection.default.username", user);
				p.put("remote.connection.default.password", pwd);
				p.put("remote.connection.default.connect.options.org.xnio.Options.SASL_DISALLOWED_MECHANISMS", "JBOSS-LOCAL-USER");
				p.put("remote.connection.default.connect.options.org.xnio.Options.SASL_POLICY_NOPLAINTEXT", "false");
			}

			EJBClientConfiguration cc = new PropertiesBasedEJBClientConfiguration(p);
			selector = new ConfigBasedEJBClientContextSelector(cc);
			mapUserPassToContext.put(user+"/"+pwd, selector);
		}

		EJBClientContext.setSelector(selector);
	}

	private static InitialContext getContext(final String user, final String pwd) throws NamingException {

		Properties props = new Properties();
		props.put(Context.URL_PKG_PREFIXES, "org.jboss.ejb.client.naming");
		props.put(Context.SECURITY_PRINCIPAL, user);
		props.put(Context.SECURITY_CREDENTIALS, pwd);

		return new InitialContext(props);
	}

	public static void login(final String user, final String pwd) throws NamingException, LoginException, SaslException {

		ConsoleApp.logout();

		//ctx = getContext(user, pwd);
		setClientContext(user, pwd);

		log.info("Looking for Environment");

		ServerLoginModule.login(user, pwd);

		env = ServerLoginModule.getService("Environment", Environment.class);
		cre = ServerLoginModule.getProxy(JNDINames.CRE_NAME, RulesEngine.class);
		ALERTING_SERVICE = ServerLoginModule.getService("AlertService", AlertService.class);
		USER_SERVICE = ServerLoginModule.getService("UserManagementService", UserManagementService.class);
		ACTIVITY_LOG_SERVICE= ServerLoginModule.getService("ActivityLogService", ActivityLogService.class);

		loggedin = true;
	}

	public static void startCache() throws Exception {
        JMXServiceURL target = new JMXServiceURL("service:jmx:remoting-jmx://localhost:9999");
        JMXConnector connector = JMXConnectorFactory.connect(target);
        MBeanServerConnection remote = connector.getMBeanServerConnection();
        remote.invoke(new ObjectName("com.synapsense:type=CacheSvc"), "start", new Object[] {}, new String[]{});
    }

    public static void stopCache() throws Exception {
        JMXServiceURL target = new JMXServiceURL("service:jmx:remoting-jmx://localhost:9999");
        JMXConnector connector = JMXConnectorFactory.connect(target);
        MBeanServerConnection remote = connector.getMBeanServerConnection();
        remote.invoke(new ObjectName("com.synapsense:type=CacheSvc"), "stop", new Object[] {}, new String[]{});
    }

	private static TO configNetwork(final String networkName) throws EnvException {
		// String networkName = "Net-" + networkId;
		String networkDescription = "Test Synapsense Network";
		String baseStationHostname = "localhost";
		Integer baseStationPort = 1025;
		String networkDefaultSampleInterval = "5 sec";
		String comport = "COM3";
		Integer panId = Integer.parseInt("EE56", 16);
		String version = "1.1";
		Boolean softbased = true;
		Date networkStartTimestamp = new Date();

		// Search object by network ID
		criteria = new ValueTO[] { new ValueTO("name", networkName) };
		objList = env.getObjects("NETWORK", criteria);

		if (objList.size() == 0) {
			propData = new ValueTO[] {
			        new ValueTO("name", networkName),
			        // case insensitive
			        new ValueTO("networkDescription", networkDescription),
			        new ValueTO("baseStationHostname", baseStationHostname),
			        new ValueTO("baseStationPort", baseStationPort),
			        new ValueTO("networkDefaultSampleInterval", networkDefaultSampleInterval),
			        new ValueTO("comport", comport), new ValueTO("panId", panId), new ValueTO("version", version),
			        new ValueTO("softbased", softbased), new ValueTO("networkStartTimestamp", networkStartTimestamp) };

			// System.out.println("--> Creating network...");
			log.info("Creating network...");
			// Create new network object
			network = env.createObject("NETWORK", propData);
		} else {

			// Use existing network object
			network = (TO) objList.toArray()[0];
			log.info("Found network with ID " + network.getID().toString());

			// Set property values
			env.setPropertyValue(network, "name", networkName);
			env.setPropertyValue(network, "networkDescription", networkDescription);
			env.setPropertyValue(network, "baseStationHostname", baseStationHostname);
			env.setPropertyValue(network, "baseStationPort", baseStationPort);
			env.setPropertyValue(network, "networkDefaultSampleInterval", networkDefaultSampleInterval);
			env.setPropertyValue(network, "comport", comport);
			env.setPropertyValue(network, "panId", panId);
			env.setPropertyValue(network, "version", version);
			env.setPropertyValue(network, "softbased", softbased);
		}

		return network;
	}

	private static TO configNode(final TO networkId, final String nodeName, final Long nodePhysicalId)
	        throws EnvException {

		// Search object by node physical ID
		criteria = new ValueTO[] { new ValueTO("name", nodeName), new ValueTO("nodePhysicalId", nodePhysicalId) };
		objList = env.getObjects("NODE", criteria);

		if (objList.size() > 0) {
			node = (TO) objList.toArray()[0];
			log.info("Found node: " + env.getPropertyValue(node, "name", String.class));
			// System.out.println("-->));
		} else {
			propData = new ValueTO[] {
			        // case insensitive
			        new ValueTO("name", nodeName), new ValueTO("nodePhysicalId", nodePhysicalId),
			        new ValueTO("platform", nodePlatform), new ValueTO("location", "{x,y} = 110,120") };

			node = env.createObject("NODE", propData);
			env.setRelation(networkId, node);
			log.info("Node " + nodeName + " was created");
		}
		return node;
	}

	private static TO configSensor(final TO node, final int channel, final String sensorName, final Integer sensorTypeId)
	        throws EnvException {
		String sensorDescription = null;
		Integer status = 1;
		Integer sensorId = 0;

		criteria = new ValueTO[] { new ValueTO("nodeId", node.getID()), new ValueTO("channel", channel) };
		Collection<TO<?>> sensors = env.getObjects("SENSOR", criteria);

		if (sensors.size() > 0) {
			sensor = (TO) sensors.toArray()[0];
			log.info("Found sensor: " + env.getPropertyValue(sensor, "name", String.class));
		} else {
			ValueTO[] data1 = new ValueTO[] {
			        // case insensitive
			        new ValueTO("name", sensorName), new ValueTO("sensorDescription", sensorDescription),
			        new ValueTO("status", status), new ValueTO("type", sensorTypeId), new ValueTO("channel", channel),
			        new ValueTO("lastValue", -5000.0) };
			// automatically creates new topology link
			sensor = env.createObject("SENSOR", data1);
			env.setRelation(node, sensor);
			log.info("Sensor : " + env.getPropertyValue(sensor, "name", String.class) + " was created");
		}

		sensorId = (Integer) sensor.getID();
		return sensor;
	}

	static ValueTO[] criteria = null;
	static Collection<TO<?>> objList = null;
	static ObjectType[] types = null;
	static ObjectType objType = null;
	static ValueTO[] propData = null;
	static final int nodePlatform = 1;

	private static void configureWSN() throws Exception {
		log.info("Configuring WSN...");

		// ////////////////////////////////////////////////////////////////////////////////////////////
		// NETWORK CREATION
		network = configNetwork("SensorNet");

		// ////////////////////////////////////////////////////////////////////////////////////////////
		// NODES & SENSORS PROVISIONING
		// log.info("Creating node...");

		Integer networkId = (Integer) network.getID();
		Long nodePhysicalId = 0L;
		String sensorName = null;
		Integer channel = 0;

		// ////////////////////////////////////////////////////////////////////////////////////////////
		// Node 1 with sensors
		nodePhysicalId = Long.parseLong("0172519D0D0000FF", 16);
		node = configNode(network, "Node-1", nodePhysicalId);
		channel = 0;
		sensorName = "Node-1 Temperature";
		configSensor(node, channel, sensorName, 1);

		channel = 1;
		sensorName = "Node-1 Humidity";
		configSensor(node, channel, sensorName, 2);

		channel = 2;
		sensorName = "Node-1 Battery Voltage";
		configSensor(node, channel, sensorName, 5);

		channel = 3;
		sensorName = "Node-1 Door Open Status";
		configSensor(node, channel, sensorName, 9);

		channel = 4;
		sensorName = "Node-1 Current";
		configSensor(node, channel, sensorName, 15);

		channel = 5;
		sensorName = "Node-1 Liquid Level";
		configSensor(node, channel, sensorName, 12);

		// ////////////////////////////////////////////////////////////////////////////////////////////
		// Node 2 with sensors
		nodePhysicalId = Long.parseLong("016266160D00003C", 16);
		node = configNode(network, "Node-2", nodePhysicalId);

		channel = 0;
		sensorName = "Node-2 Temperature";
		configSensor(node, channel, sensorName, 1);

		channel = 1;
		sensorName = "Node-2 Humidity";
		configSensor(node, channel, sensorName, 2);

		channel = 2;
		sensorName = "Node-2 Battery Voltage";
		configSensor(node, channel, sensorName, 5);

		channel = 3;
		sensorName = "Node-2 Ext Temp 1";
		configSensor(node, channel, sensorName, 21);

		channel = 4;
		sensorName = "Node-2 Ext Temp 2";
		configSensor(node, channel, sensorName, 21);

		// ////////////////////////////////////////////////////////////////////////////////////////////
		// Node 3 with sensors
		nodePhysicalId = Long.parseLong("0167E38B0D0000B2", 16);
		node = configNode(network, "Node-3", nodePhysicalId);

		channel = 0;
		sensorName = "Node-3 Temperature";
		configSensor(node, channel, sensorName, 1);

		channel = 1;
		sensorName = "Node-3 Humidity";
		configSensor(node, channel, sensorName, 2);

		channel = 2;
		sensorName = "Node-3 Battery Voltage";
		configSensor(node, channel, sensorName, 5);

		channel = 3;
		sensorName = "Node-3 Ext Temp 1";
		configSensor(node, channel, sensorName, 21);

		channel = 4;
		sensorName = "Node-3 Ext Temp 2";
		configSensor(node, channel, sensorName, 21);

	}

	public static void configureDevices() throws Exception {
		ObjectType[] types = null;
		ObjectType objType = null;

		// ////////////////////////////////////////////////////////////////////////////////////////////
		// Creating SNMP PDU device

		// Search existing object types
		types = env.getObjectTypes(new String[] { pduDeviceType });

		if (types.length == 0) {
			// Creating object type PDU_SNMP
			objType = env.createObjectType(pduDeviceType);

			// Add attributes to an object type
			objType.getPropertyDescriptors().add(
			        new PropertyDescr("rpduidentname", "products.hardware.rPDU.rPDUIdent.rPDUIdentName",
			                "java.lang.String"));
			objType.getPropertyDescriptors().add(
			        new PropertyDescr("rpduidentdevicerating",
			                "products.hardware.rPDU.rPDUIdent.rPDUIdentDeviceRating", "java.lang.Long"));

			env.updateObjectType(objType);
		}

		// Search object by name
		PDU_objectStaff = getObject(env, pduDeviceType, pduDeviceName);
		if (PDU_objectStaff == null) {
			// Creating object instance APC_PDU of type PDU_SNMP
			PDU_objectStaff = env.createObject(pduDeviceType, new ValueTO[] { new ValueTO("name", pduDeviceName) });
		}

	}

	public static final String CRAC_SensorA = "sensorA";
	public static final String CRAC_SensorB = "sensorB";
	public static final String CRAC_Efficiency = "CRACEfficiency";

	public static String CRAC_type = "CRAC_TYPE";
	protected static String CRAC_name = "CRAC_001";
	protected static TO<?> CRAC_object = null;
	protected static TO<?> ruleType_CRACEfficiency;

	public static ObjectType createCRACType() throws EnvException {
		ObjectType[] types = null;
		ObjectType CRACObjType;
		types = env.getObjectTypes(new String[] { CRAC_type });
		if (types.length == 0) {
			// create object type CRAC
			CRACObjType = env.createObjectType(CRAC_type);
			log.info("Object type:" + CRACObjType.getName() + " was created");
		} else {
			CRACObjType = types[0];
			log.info("Object type:" + CRACObjType.getName() + " found");
		}

		// add Calculated Property
		PropertyDescr cE = new PropertyDescr(CRAC_Efficiency, Double.class.getName());
		if (!CRACObjType.getPropertyDescriptors().contains(cE)) {
			CRACObjType.getPropertyDescriptors().add(cE);
		}

		PropertyDescr sensorA = new PropertyDescr(CRAC_SensorA, PropertyTO.class.getName());
		if (!CRACObjType.getPropertyDescriptors().contains(sensorA)) {
			CRACObjType.getPropertyDescriptors().add(sensorA);
		}
		PropertyDescr sensorB = new PropertyDescr(CRAC_SensorB, PropertyTO.class.getName());
		if (!CRACObjType.getPropertyDescriptors().contains(sensorB)) {
			CRACObjType.getPropertyDescriptors().add(sensorB);
		}

		env.updateObjectType(CRACObjType);
		return CRACObjType;
	}

	public static final String Rack_sensorTop = "sensorTop";
	public static final String Rack_sensorMid = "sensorMid";
	public static final String Rack_sensorBot = "sensorBot";
	public static final String Rack_sensorRef = "sensorRef";
	public static final String Rack_sensorHot = "sensorHot";
	public static final String RackCSI = "RackCSI";

	public static String Rack_type = "RACK_TYPE";
	protected static String Rack_name = "RACK_001";
	protected static TO<?> Rack_object = null;
	protected static TO<?> ruleType_RackCSI;

	public static ObjectType createRackType() throws EnvException {
		ObjectType[] types = null;
		ObjectType RackObjType;
		// Search existing object types
		types = env.getObjectTypes(new String[] { Rack_type });
		if (types.length == 0) {
			// create object type Rack
			RackObjType = env.createObjectType(Rack_type);
			log.info("Object type:" + RackObjType.getName() + " was created");
		} else {
			RackObjType = types[0];
			log.info("Object type:" + RackObjType.getName() + " found");
		}

		// add Calculated Property
		PropertyDescr csi = new PropertyDescr(RackCSI, Double.class.getName());
		if (!RackObjType.getPropertyDescriptors().contains(csi)) {
			RackObjType.getPropertyDescriptors().add(csi);
		}

		PropertyDescr sensorTop = new PropertyDescr("sensorTop", PropertyTO.class.getName());
		if (!RackObjType.getPropertyDescriptors().contains(sensorTop)) {
			RackObjType.getPropertyDescriptors().add(sensorTop);
		}
		PropertyDescr sensorMid = new PropertyDescr("sensorMid", PropertyTO.class.getName());
		if (!RackObjType.getPropertyDescriptors().contains(sensorMid)) {
			RackObjType.getPropertyDescriptors().add(sensorMid);
		}

		PropertyDescr sensorBot = new PropertyDescr("sensorBot", PropertyTO.class.getName());
		if (!RackObjType.getPropertyDescriptors().contains(sensorBot)) {
			RackObjType.getPropertyDescriptors().add(sensorBot);
		}

		PropertyDescr sensorRef = new PropertyDescr("sensorRef", PropertyTO.class.getName());
		if (!RackObjType.getPropertyDescriptors().contains(sensorRef)) {
			RackObjType.getPropertyDescriptors().add(sensorRef);
		}

		PropertyDescr sensorHot = new PropertyDescr("sensorHot", PropertyTO.class.getName());
		if (!RackObjType.getPropertyDescriptors().contains(sensorHot)) {
			RackObjType.getPropertyDescriptors().add(sensorHot);
		}

		env.updateObjectType(RackObjType);

		return RackObjType;

	}

	public static TO createCRACObject(final String name, final String sensorAName, final String sensorBName)
	        throws EnvException {

		// Search CRAC by name
		CRAC_object = getObject(env, CRAC_type, name);
		if (CRAC_object == null) {
			// Create CRAC object
			CRAC_object = env.createObject(CRAC_type, new ValueTO[] { new ValueTO("name", name) });
			log.info("New object:" + name + " of type:" + CRAC_type + " was created");
		}

		// adds props values - links to objects
		PropertyTO propLink1 = new PropertyTO(getObject(env, "SENSOR", sensorAName), "lastValue");

		PropertyTO propLink2 = new PropertyTO(getObject(env, "SENSOR", sensorBName), "lastValue");

		env.setPropertyValue(CRAC_object, CRAC_SensorA, propLink1);
		env.setPropertyValue(CRAC_object, CRAC_SensorB, propLink2);

		// attach rules on calculated properties
		Collection<CRERule> rules = new ArrayList<CRERule>();
		String ruleName = null;

		ruleName = name + "Efficiency";

		// rules.add(new CRERule(ruleName, CRAC_object, CRAC_Efficiency,
		// "supplyData", CRAC_SensorA));
		// rules.add(new CRERule(ruleName, CRAC_object, CRAC_Efficiency,
		// "returnData", CRAC_SensorB));

		// env.configRules(CRAC_object, rules
		// .toArray(new CRERule[rules.size()]));

		return CRAC_object;
	}

	public static TO createRackObject(final String name, final String topName, final String midName,
	        final String botName, final String refName, final String hotName) throws EnvException {

		// Search Rack by name
		Rack_object = getObject(env, Rack_type, name);
		if (Rack_object == null) {
			// Create Rack object
			Rack_object = env.createObject(Rack_type, new ValueTO[] { new ValueTO("name", name) });
			log.info("New object:" + name + " of type:" + Rack_type + " was created");
		}

		// adds props values - links to objects
		PropertyTO propLink = new PropertyTO(getObject(env, "SENSOR", topName), "lastValue");
		env.setPropertyValue(Rack_object, Rack_sensorTop, propLink);

		propLink = new PropertyTO(getObject(env, "SENSOR", midName), "lastValue");
		env.setPropertyValue(Rack_object, Rack_sensorMid, propLink);

		propLink = new PropertyTO(getObject(env, "SENSOR", botName), "lastValue");
		env.setPropertyValue(Rack_object, Rack_sensorBot, propLink);

		propLink = new PropertyTO(getObject(env, "SENSOR", refName), "lastValue");
		env.setPropertyValue(Rack_object, Rack_sensorRef, propLink);

		propLink = new PropertyTO(getObject(env, "SENSOR", hotName), "lastValue");
		env.setPropertyValue(Rack_object, Rack_sensorHot, propLink);

		// attach rules on calculated properties
		Collection<CRERule> rules = new ArrayList<CRERule>();
		String ruleName = null;

		ruleName = name + "CSI";
		// rules.add(new CRERule(ruleName, Rack_object, RackCSI, "top",
		// Rack_sensorTop));
		// rules.add(new CRERule(ruleName, Rack_object, RackCSI, "mid",
		// Rack_sensorMid));
		// rules.add(new CRERule(ruleName, Rack_object, RackCSI, "bot",
		// Rack_sensorBot));
		// rules.add(new CRERule(ruleName, Rack_object, RackCSI, "ref",
		// Rack_sensorRef));
		// rules.add(new CRERule(ruleName, Rack_object, RackCSI, "hot",
		// Rack_sensorHot));
		//
		// env.configRules(Rack_object, rules
		// .toArray(new CRERule[rules.size()]));

		return Rack_object;
	}

	public static void configureEnvObjects() throws Exception {
		log.info("Configuring Environment objects...");
		ObjectType CRACObjType, RackObjType;

		// ////////////////////////////////////////////////////////////////////////////////////////////
		// Creating CRAC object
		CRACObjType = createCRACType();
		CRAC_object = createCRACObject(CRAC_name, "Node-1 Temperature", "Node-2 Temperature");

		// ////////////////////////////////////////////////////////////////////////////////////////////
		// Creating Rack object type
		RackObjType = createRackType();
		Rack_object = createRackObject(Rack_name, "Node-3 Temperature", "Node-2 Ext Temp 1", "Node-2 Ext Temp 2",
		        "Node-3 Ext Temp 1", "Node-3 Ext Temp 2");

	}

	/*
	 * public static boolean createCRACRule(TO CRAC_obj, String ruleName) throws
	 * EnvException, IOException { final String ruleTypeName = "CRACEfficiency";
	 * 
	 * TO<?> ruleId = null; ruleId = ruleInstanceDAO.getRuleInstance(ruleName);
	 * 
	 * if (ruleId == null) { ruleId = ruleInstanceDAO.create(new Rule(ruleName,
	 * ruleTypeName)); log.info("RULE instance " + ruleName + " was created");
	 * // bind Properties to the Rule ruleInstanceDAO.setDependProp((Integer)
	 * ruleId.getID(), CRAC_obj, "CRACEfficiency");
	 * ruleInstanceDAO.setSourceProp((Integer) ruleId.getID(), new Rule(
	 * CRAC_obj, "supplyData", "sensorA", Rule.LinkType.LINK_TO_PROPERTY));
	 * ruleInstanceDAO.setSourceProp((Integer) ruleId.getID(), new Rule(
	 * CRAC_obj, "returnData", "sensorB", Rule.LinkType.LINK_TO_PROPERTY));
	 * return true; }
	 * 
	 * return false; }
	 * 
	 * public static boolean createRackRule(TO Rack_obj, String ruleName) throws
	 * EnvException, IOException { final String ruleTypeName = "RackCSI";
	 * ObjectType[] types = null; String actionBody = null; TO<?> ruleTypeId =
	 * null;
	 * 
	 * ruleTypeId = ruleTypeService.getRuleTypeByName(ruleTypeName); if
	 * (ruleTypeId == null) { // create Propagating Rule depending on the
	 * Calculated Property actionBody = FileTools .read(new File(
	 * "conf/scripts/com/synapsense/rulesengine/core/script/base/RackCSI.groovy"
	 * )); ruleTypeId = ruleTypeService.create(new RuleType(ruleTypeName,
	 * actionBody, "Groovy", false)); log.info("RULE type " + ruleTypeName + "
	 * was created"); }
	 * 
	 * TO<?> ruleId = null; ruleId = ruleInstanceDAO.getRuleInstance(ruleName);
	 * 
	 * if (ruleId == null) { ruleId = ruleInstanceDAO.create(new Rule(ruleName,
	 * ruleTypeName)); log.info("RULE instance " + ruleName + " was created");
	 * // bind Properties to the Rule ruleInstanceDAO .setDependProp((Integer)
	 * ruleId.getID(), Rack_obj, "csi"); ruleInstanceDAO.setSourceProp((Integer)
	 * ruleId.getID(), new Rule( Rack_obj, "top", "sensorTop",
	 * Rule.LinkType.LINK_TO_PROPERTY)); ruleInstanceDAO.setSourceProp((Integer)
	 * ruleId.getID(), new Rule( Rack_obj, "mid", "sensorMid",
	 * Rule.LinkType.LINK_TO_PROPERTY)); ruleInstanceDAO.setSourceProp((Integer)
	 * ruleId.getID(), new Rule( Rack_obj, "bot", "sensorBot",
	 * Rule.LinkType.LINK_TO_PROPERTY)); ruleInstanceDAO.setSourceProp((Integer)
	 * ruleId.getID(), new Rule( Rack_obj, "ref", "sensorRef",
	 * Rule.LinkType.LINK_TO_PROPERTY)); ruleInstanceDAO.setSourceProp((Integer)
	 * ruleId.getID(), new Rule( Rack_obj, "hot", "sensorHot",
	 * Rule.LinkType.LINK_TO_PROPERTY)); return true; } return false; }
	 */

	public static void seedRuleTemplates() throws ObjectExistsException, IOException {
		ruleType_CRACEfficiency = seedRuleTemplate("com.synapsense.rulesengine.core.script.base.CRACEfficiency",
		        "Groovy", false);
		ruleType_RackCSI = seedRuleTemplate("com.synapsense.rulesengine.core.script.base.RackCSI", "Groovy", false);
	}

	public static void configureRules() throws Exception {
		/*
		 * Boolean rulesAdded = false; log.info("Configuring RULES..."); //
		 * /////
		 * /////////////////////////////////////////////////////////////////
		 * ////////////////////// // Creating rule "CRAC Efficiency" rulesAdded
		 * |= createCRACRule(CRAC_object, "CRAC001Efficiency"); //
		 * //////////////
		 * ////////////////////////////////////////////////////////
		 * ////////////////////// // Creating rule "Rack CSI" rulesAdded |=
		 * createRackRule(Rack_object, "RackCSI001"); //
		 * ////////////////////////
		 * ////////////////////////////////////////////////////////////////////
		 * // Creating rule "PDU Power Loss" for all PDU_objects int i = 0; for
		 * (TO<?> PDU_object: PDU_objects) { i++; rulesAdded |=
		 * createPDURules(PDU_object, new Integer(i).toString()); } //
		 * ruleType_CRACEfficiency = seedRuleTemplate("CRACEfficiency",
		 * "Groovy", false); // ruleType_RackCSI = seedRuleTemplate("RackCSI",
		 * "Groovy", false);
		 * 
		 * rulesAdded |= createPDUTotalRule(PDU_Totalobject, "Single");
		 * rulesAdded |= createPDUTotalAverageRule(PDU_TotalAverageobject,
		 * "Single");
		 */
		Boolean rulesAdded = true;
		if (rulesAdded) {
			log.info("Restarting CRE");
			crembean.stop();
			crembean.start();
		}

		// Attaching Drools rules
		log.info("Attaching Drools rules from file: " + JNDINames.DRE_FILE.toString());
		dre.attachObject(Rack_object);
		dre.attachObject(PDU_objectStaff);
		drembean.addRulesFromFile(JNDINames.DRE_FILE);

	}

	public static void fireRules(final int times, final long latency) throws Exception {
		log.info("Getting calculated properties");
		// Getting calculated property value
		for (int i = 0; i < times; i++) {
			Double csi = env.getPropertyValue(Rack_object, "csi", Double.class);
			log.info("Getting csi from " + Rack_object + ". csi=" + csi);
			Thread.sleep(latency);
			Double efficiency = env.getPropertyValue(CRAC_object, "CRACEfficiency", Double.class);
			log.info("Getting CRACEfficiency from " + CRAC_object + ".CRACEfficiency=" + efficiency);
			Thread.sleep(latency);
		}
	}

	public static TO<?> seedRuleTemplate(final String baseDir, final String ruleTypeName, final String ruleType,
	        Boolean precompiled) throws IOException, ObjectExistsException {
		String actionBody = null;
		TO<?> ruleTypeId = null;
		ruleTypeId = ruleTypeService.getRuleTypeByName(ruleTypeName);
		if (ruleTypeId == null) {
			if (!precompiled) {
				String ruleTypePath = baseDir + "conf/scripts/" + ruleTypeName.replace('.', '/');
				File srcFile = new File(ruleTypePath + "." + ruleType.toLowerCase());

				actionBody = FileTools.read(srcFile);
				ruleTypeId = ruleTypeService.create(new RuleType(ruleTypeName, actionBody, ruleType, precompiled));
			} else {
				ruleTypeId = ruleTypeService.create(new RuleType(ruleTypeName, null, ruleType, precompiled));
			}
			log.info("RULE type " + ruleTypeName + " was created");
		}
		return ruleTypeId;
	}

	public static TO<?> seedRuleTemplate(final String ruleTypeName, final String ruleType, final Boolean precompiled)
	        throws IOException, ObjectExistsException {
		return seedRuleTemplate("", ruleTypeName, ruleType, precompiled);
	}

	public static void main(final String[] args) throws Exception {
		ValueTO[] criteria = null;
		Collection<TO<?>> objList = null;

		// Setup initial context
		init();
		// if(env.getObjectTypes(new String[] { "envy" }).length == 0) {
		// ObjectType a = env.createObjectType("envy");
		// a.getPropertyDescriptors().add(new
		// PropertyDescr("prop1",String.class.getName()));
		// env.updateObjectType(a);
		// }
		// if(env.getObjectsByType("envy") == null) {
		// TO b = env.createObject("PullLikeName", "envy");
		// }
		// TO b = env.getObjects("envy", new ValueTO[] { new
		// ValueTO("name", "PullLikeName") } ).iterator().next();
		// log.info("Reading prop1... Value - " + env.getPropertyValue(b,
		// "prop1",String.class));
		//
		// ObjectType a = env.getObjectType("SENSOR");
		// log.info("Dumping SENSOR ot...");
		// for(PropertyDescr b : a.getPropertyDescriptors()) {
		// log.info("\t" + b.getName() + "-" + b.getTypeName());
		// }
		// log.info("end of sensor");
		// a = env.getObjectType("NODE");
		// log.info("Dumping NODE ot...");
		// for(PropertyDescr b : a.getPropertyDescriptors()) {
		// log.info("\t" + b.getName() + "-" + b.getTypeName());
		// }
		// log.info("end of node");

		// for (int i = 0; i < 5000; i++) {
		// env.getObjectsByType("NODE");
		// }
		configureWSN();

		Long nodePhysicalId = Long.parseLong("1f8f9f310000008", 16);
		node = configNode(network, "Node-ANechaev1f8f9f310000008", nodePhysicalId);

		TO<?> nodeTO = env.getObjects("NODE", new ValueTO[] { new ValueTO("name", "Node-ANechaev1f8f9f310000008") })
		        .iterator().next();

		env.setPropertyValue(nodeTO, "nodeSamplingInterval", "10 sec");

		// createTestSNMP();]
		seedRuleTemplates();
		configureEnvObjects();
		configureDevices();
		configureRules();

		// Creating a Virtual Sensor for SNMP device
		// reg.getComponentProxy("DeviceManager",
		// DeviceManagerRemote.class).createVirtualSensor("APC_PDU","products.hardware.rPDU.rPDUIdent.rPDUIdentDeviceRating",
		// 5000, VirtualSensorsRemoveStrategy.DONT_REMOVE);

		// ////////////////////////////////////////////////////////////////////////////////////////////
		// WORKING WITH WSN AND SNMP DEVICES IN PUSH MODE
		// ////////////////////////////////////////////////////////////////////////////////////////////
		// Start sending WSN and virtual sensors data
		// ////////////////////////////////////////////////////////////////////////////////////////////
		// WORKING WITH WSN DEVICES IN PULL MODE
		// ////////////////////////////////////////////////////////////////////////////////////////////
		/*
		 * // Search object by node physical ID Long nodePhysicalId =
		 * Long.parseLong("0172519D0D0000FF", 16); criteria = new ValueTO[] {
		 * new ValueTO( "nodePhysicalId", nodePhysicalId) }; objList =
		 * env.getObjects("NODE", criteria);
		 * 
		 * if (objList.size() > 0) { node = (TO) objList.toArray()[0];
		 * 
		 * String res = env.getPropertyValue(node, "nodeSamplingInterval",
		 * String.class); System.out.println("networkName="+res);
		 * 
		 * env.setPropertyValue(node, "nodeSamplingInterval", "new name");
		 * 
		 * res = env.getPropertyValue(toSNMP, "nodeSamplingInterval",
		 * String.class); System.out.println("networkName="+res); } else {
		 * System.out.println("No nodes found with physical ID:
		 * 0172519D0D0000FF"); }
		 */

		// ////////////////////////////////////////////////////////////////////////////////////////////
		// WORKING WITH SNMP DEVICES IN PULL MODE
		// ////////////////////////////////////////////////////////////////////////////////////////////
		/*
		 * // Reading property value String sPropVal =
		 * env.getPropertyValue(PDU_object, "rpduidentname", String.class);
		 * log.info("rPDUIdentName old value = '" + sPropVal + "'");
		 * System.out.println("rPDUIdentName old value = '" + sPropVal + "'");
		 * // Writing property value env.setPropertyValue(PDU_object,
		 * "rpduidentname", "PDUx"); // Checking property value sPropVal =
		 * env.getPropertyValue(PDU_object, "rpduidentname", String.class);
		 * log.info("rPDUIdentName new value = '" + sPropVal + "'");
		 * System.out.println("rPDUIdentName new value = '" + sPropVal + "'");
		 */
		fireRules(10, 1000);
	}

	public static MBeanServerConnection lookupMBeanServerProxy(InitialContext ic) throws NamingException {
		if (ic == null) {
			ic = new InitialContext();
		}
		return (MBeanServerConnection) ic.lookup("jmx/invoker/RMIAdaptor");
	}

	public static void restartCE() throws Exception {
		log.info("Restarting CRE");
		crembean.stop();
		crembean.start();
	}

	public static void restartDRE() throws Exception {
		log.info("Reinitializing DRE...");
		drembean.stop();
		drembean.destroy();

		drembean.create();
		drembean.start();
	}

}

class FileTools {
	public static String read(final File src) throws IOException {
		long size = src.length();
		FileInputStream fileStream = new FileInputStream(src);
		byte b[] = new byte[(int) size];
		fileStream.read(b);
		return new String(b);
	}
}
