package com.synapsense.func.userservice;

import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.UserManagementException;
import com.synapsense.func.MockConsole;
import com.synapsense.service.UserManagementService;
import com.synapsense.util.CollectionUtils;
import com.synapsense.util.ServerLoginModule;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.ejb.EJBAccessException;
import javax.naming.NamingException;
import javax.security.auth.login.LoginException;
import javax.security.sasl.SaslException;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import static org.junit.Assert.*;

public abstract class AbstractUserServiceTest {

	protected MockConsole console = null;
	protected UserManagementService USER_SERVICE = null;
	private int startingNumberOfUsers = 0;

	public static void setUpBeforeClass() throws Exception {
	}

	public void setUp() throws Exception {
		console = new MockConsole();
		this.login("admin", "admin", true);
		Collection<TO<?>> allUsers = console.getUserService().getAllUsers();
		this.startingNumberOfUsers = allUsers.size();
	}

	public void tearDown() throws Exception {
		this.login("admin", "admin", true);
		Collection<TO<?> > allUsers = console.getUserService().getAllUsers();
		assertEquals("There should be the same number of users as when started the test", this.startingNumberOfUsers, allUsers.size());

		console.logout();
		console = null;
	}

	protected void login(final String user, final String pwd, boolean loginValid) {
		System.out.println("Logging in as " + user + " / " + pwd);
		try {
			this.console.login(user, pwd);
			this.USER_SERVICE = this.console.getUserService();
			assertTrue("SHOULD NOT have been able to login with user: " + user + " and password: " + pwd, loginValid);
		} catch (NamingException | LoginException | SaslException | EJBAccessException e) {
			System.out.println("EXCEPTION: (" + e.getClass().getName() + ") " + e.getLocalizedMessage());
			this.USER_SERVICE = null;
			assertFalse("SHOULD have been able to login with user: " + user + " and password: " + pwd, loginValid);
		}
	}

	protected void pause() {
		this.pause(500);
	}

	protected void pause(int ms) {
		try {
			Thread.sleep(ms);
		} catch (InterruptedException e) {
		}
	}
}
