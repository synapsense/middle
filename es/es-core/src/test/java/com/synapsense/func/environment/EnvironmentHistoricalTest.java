package com.synapsense.func.environment;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.internal.runners.JUnit4ClassRunner;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;

import com.synapsense.dto.EnvObjectHistory;
import com.synapsense.dto.HistoricalRecord;
import com.synapsense.dto.ObjectType;
import com.synapsense.dto.PropertyDescr;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.EnvException;
import com.synapsense.exception.IllegalInputParameterException;
import com.synapsense.exception.PropertyNotFoundException;
import com.synapsense.func.ConsoleApp;
import com.synapsense.service.Environment;

public class EnvironmentHistoricalTest {

	private static final String TYPE_TEST = "ALERT_HISTORY";
	private static Environment env;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		ConsoleApp.init();

		env = ConsoleApp.env;
	}

	@Test(expected = IllegalInputParameterException.class)
	public void shouldSupportOnlyAlertHistory() throws Exception {
		TO notAlertHistoryHolder = TOFactory.getInstance().loadTO("TYPE", 1);
		EnvObjectHistory alertHistory = env.getHistory(notAlertHistoryHolder,
		        Arrays.asList("alertId", "time", "name", "status", "message"), new Date(), new Date());
	}

	@Test
	public void getHistoricalTuples() throws Exception {
		final int HISTORY_SIZE = 100;
		final long initialSetTime = System.currentTimeMillis() - 1000 * HISTORY_SIZE;
		long setTime = initialSetTime;

		TO<?> overallAlertHistory = env.createObject(TYPE_TEST,
		        createTuple(setTime, 0, "name_" + 0, 0, "message_" + 0, System.currentTimeMillis()));
		try {

			for (int i = 1; i < HISTORY_SIZE; i++) {
				setTime += 1000L;
				env.setAllPropertiesValues(overallAlertHistory,
				        createTuple(setTime, i, "name_" + i, i % 3, "message_" + i, System.currentTimeMillis()));
			}

			Thread.sleep(1500);

			long start = System.currentTimeMillis();
			int toGetSize = 10;
			Date reqBegin = new Date(initialSetTime);
			Date reqEnd = new Date(initialSetTime + toGetSize * 1000L);
			System.out.println(reqBegin + " - " + reqEnd);
			EnvObjectHistory alertHistory = env.getHistory(overallAlertHistory,
			        Arrays.asList("alertId", "time", "name", "status", "message"), reqBegin, reqEnd);
			System.out.println("getting history time :" + (System.currentTimeMillis() - start));
			assertEquals(alertHistory.historyOwner(), overallAlertHistory);

			assertEquals(toGetSize, alertHistory.size());

			HistoricalRecord lastHistoryRecord = alertHistory.getLast();
			assertEquals(5, lastHistoryRecord.getFieldsNum());

			assertEquals("name_" + (toGetSize), lastHistoryRecord.getValue("name"));
			assertEquals(toGetSize, lastHistoryRecord.getValue("alertId"));
			assertEquals(toGetSize % 3, lastHistoryRecord.getValue("status"));
			assertEquals("message_" + (toGetSize), lastHistoryRecord.getValue("message"));

		} finally {
			env.deleteObject(overallAlertHistory);
		}
	}

	@Test
	public void getHistoricalTuplesDifferentTimestampInterpreting() throws Exception {
		long initialSetTime = System.currentTimeMillis() - 2 * 1000L;
		long setTime = initialSetTime;
		TO<?> overallAlertHistory = env.createObject(TYPE_TEST,
		        createTuple(setTime, 0, "name_" + 0, 0, "message_" + 0, System.currentTimeMillis()));

		try {
			setTime += 1000L;

			ValueTO[] firstRec = new ValueTO[] { new ValueTO("alertId", 1, setTime),
			        new ValueTO("name", "first name", setTime), new ValueTO("status", 1, setTime) };
			env.setAllPropertiesValues(overallAlertHistory, firstRec);

			setTime += 1000L;

			ValueTO[] secondRec = new ValueTO[] { new ValueTO("time", setTime, setTime),
			        new ValueTO("message", "second message", setTime) };
			env.setAllPropertiesValues(overallAlertHistory, secondRec);

			Thread.sleep(1500);

			long start = System.currentTimeMillis();
			Date reqBegin = new Date(initialSetTime);
			Date reqEnd = new Date(initialSetTime + 2 * 1000L);

			EnvObjectHistory alertHistory = env.getHistory(overallAlertHistory,
			        Arrays.asList("alertId", "time", "name", "status", "message"), reqBegin, reqEnd);

			assertEquals(alertHistory.historyOwner(), overallAlertHistory);

			System.out.println("getting history time :" + (System.currentTimeMillis() - start));
			assertEquals(2, alertHistory.size());

			Iterator<HistoricalRecord> iterator = alertHistory.iterator();

			HistoricalRecord lastRecord = iterator.next();

			assertEquals(2, lastRecord.getFieldsNum());
			assertNull(lastRecord.getValue("name"));
			assertNull(lastRecord.getValue("status"));
			assertNull(lastRecord.getValue("alertId"));
			assertEquals(setTime, lastRecord.getValue("time"));
			assertEquals("second message", lastRecord.getValue("message"));

			lastRecord = iterator.next();
			assertEquals(3, lastRecord.getFieldsNum());
			assertNull(lastRecord.getValue("time"));
			assertNull(lastRecord.getValue("message"));
			assertEquals("first name", lastRecord.getValue("name"));
			assertEquals(1, lastRecord.getValue("status"));
			assertEquals(1, lastRecord.getValue("alertId"));

		} finally {
			env.deleteObject(overallAlertHistory);
		}
	}

	@Test(expected = PropertyNotFoundException.class)
	public void getHistoryForUnexistingProperties() throws Exception {
		long initialSetTime = System.currentTimeMillis() - 2 * 1000L;
		long setTime = initialSetTime;
		TO<?> overallAlertHistory = env.createObject(TYPE_TEST,
		        createTuple(setTime, 0, "name_" + 0, 0, "message_" + 0, System.currentTimeMillis()));

		try { 
			EnvObjectHistory alertHistory = env.getHistory(overallAlertHistory,
		        Arrays.asList("no_such_property", "and_even_this_one_does_not_exist"), new Date(), new Date());
		}finally { 
			env.deleteObject(overallAlertHistory);
		}
	}

	private static void deleteType(String name) throws EnvException {
		env.deleteObjectType(name);
	}

	private static void createType(String name) throws EnvException {
		if (env.getObjectType(name) != null)
			return;
		ObjectType alertHistoryType = env.createObjectType(name);
		alertHistoryType.getPropertyDescriptors().add(new PropertyDescr("alertId", Integer.class.getName(), true));
		alertHistoryType.getPropertyDescriptors().add(new PropertyDescr("name", String.class.getName(), true));
		alertHistoryType.getPropertyDescriptors().add(new PropertyDescr("status", Integer.class.getName(), true));
		alertHistoryType.getPropertyDescriptors().add(new PropertyDescr("message", String.class.getName(), true));
		alertHistoryType.getPropertyDescriptors().add(new PropertyDescr("time", Long.class.getName(), true));
		env.updateObjectType(alertHistoryType);
	}

	private ValueTO[] createTuple(long timestamp, int alertId, String name, int status, String message, long time) {
		return new ValueTO[] { new ValueTO("alertId", alertId, timestamp), new ValueTO("name", name, timestamp),
		        new ValueTO("status", status, timestamp), new ValueTO("time", time, timestamp),
		        new ValueTO("message", message, timestamp) };
	}
}
