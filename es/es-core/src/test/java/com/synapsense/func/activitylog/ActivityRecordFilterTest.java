package com.synapsense.func.activitylog;

import static org.junit.Assert.assertEquals;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Test;

import com.synapsense.service.impl.activitylog.ActivityRecordFilter;

public class ActivityRecordFilterTest {

	private final String datePattern = "yyyy-MM-dd HH:mm:ss";
	private final DateFormat dateFormatter = new SimpleDateFormat(datePattern);

	@Test
	public void testByID() {
		ActivityRecordFilter filter = new ActivityRecordFilter().filterById(1);
		assertEquals("Select record from ActivityRecordDO as record " + "left join fetch record.object as obj "
		        + "left join fetch obj.objectType as type " + "WHERE record.id = 1 order by record.time desc ",
		        filter.getQuery());

	}

	@Test
	public void testByUserName() {
		ActivityRecordFilter filter = new ActivityRecordFilter().filterByUser("admin");
		assertEquals("Select record from ActivityRecordDO as record " + "left join fetch record.object as obj "
		        + "left join fetch obj.objectType as type "
		        + "WHERE record.userName = 'admin' order by record.time desc ", filter.getQuery());

	}

	@Test
	public void testByAction() {
		ActivityRecordFilter filter = new ActivityRecordFilter().filterByAction("UserLogin");
		assertEquals("Select record from ActivityRecordDO as record " + "left join fetch record.object as obj "
		        + "left join fetch obj.objectType as type "
		        + "WHERE record.action = 'UserLogin' order by record.time desc ", filter.getQuery());

	}

	@Test
	public void testGetAll() {
		ActivityRecordFilter filter = new ActivityRecordFilter();
		assertEquals("Select record from ActivityRecordDO as record " + "left join fetch record.object as obj "
		        + "left join fetch obj.objectType as type " + "order by record.time desc ", filter.getQuery());

	}

	@Test
	public void testByDate() {
		ActivityRecordFilter filter = new ActivityRecordFilter().filterByTime(new Date(0), new Date(86400));
		assertEquals(
		        "Select record from ActivityRecordDO as record " + "left join fetch record.object as obj "
		                + "left join fetch obj.objectType as type " + "WHERE record.time between '"
		                + dateFormatter.format(new Date(0)) + "' and '" + dateFormatter.format(new Date(86400))
		                + "' order by record.time desc ", filter.getQuery());

	}

	@Test
	public void testByUserAndTime() {
		ActivityRecordFilter filterByUserAndTime = new ActivityRecordFilter().filterByUser("admin").filterByTime(
		        new Date(0), new Date(86400));

		assertEquals("Select record from ActivityRecordDO as record " + "left join fetch record.object as obj "
		        + "left join fetch obj.objectType as type "
		        + "WHERE record.userName = 'admin' AND record.time between '" + dateFormatter.format(new Date(0))
		        + "' and '" + dateFormatter.format(new Date(86400)) + "' order by record.time desc ",
		        filterByUserAndTime.getQuery());
	}

	@Test
	public void testByActionAndTime() {
		ActivityRecordFilter filterByUserAndTime = new ActivityRecordFilter().filterByAction("alert").filterByTime(
		        new Date(0), new Date(86400));

		assertEquals("Select record from ActivityRecordDO as record " + "left join fetch record.object as obj "
		        + "left join fetch obj.objectType as type " + "WHERE record.action = 'alert' AND record.time between '"
		        + dateFormatter.format(new Date(0)) + "' and '" + dateFormatter.format(new Date(86400))
		        + "' order by record.time desc ", filterByUserAndTime.getQuery());

	}
}
