package com.synapsense.func.environment;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.synapsense.dto.BinaryData;
import com.synapsense.dto.ObjectType;
import com.synapsense.dto.PropertyDescr;
import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.EnvException;
import com.synapsense.service.Environment;
import com.synapsense.service.impl.dao.to.BinaryDataImpl;
import com.synapsense.util.JNDINames;
import com.synapsense.util.ServerLoginModule;

public class HistoricalBinaryTest {
	
	private static final String TEST_TYPE = "HISTORICAL_BINARY";
	private static final String TEST_PROP = "image";
	private static final BinaryData[] data = new BinaryData[]{
		new BinaryDataImpl(new byte[]{1, 0, 0, 0, 0}),
		new BinaryDataImpl(new byte[]{0, 1, 0, 0, 0}),
		new BinaryDataImpl(new byte[]{0, 0, 1, 0, 0}),
		new BinaryDataImpl(new byte[]{0, 0, 0, 1, 0}),
		new BinaryDataImpl(new byte[]{0, 0, 0, 0, 1})
	};
	
	private static Environment e;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		Properties env = new Properties();
		env.put(Context.INITIAL_CONTEXT_FACTORY, "org.jboss.naming.remote.client.InitialContextFactory");
		env.put(Context.PROVIDER_URL, "remote://localhost:4447");
		env.put("jboss.naming.client.ejb.context", "true");
		env.put("jboss.naming.client.connect.options.org.xnio.Options.SASL_POLICY_NOPLAINTEXT", "false");
		env.put(Context.SECURITY_PRINCIPAL, "admin");
		env.put(Context.SECURITY_CREDENTIALS, "admin");
		e = ServerLoginModule.getProxy(new InitialContext(env), JNDINames.ENVIRONMENT,
		        Environment.class);
		ObjectType type  = e.createObjectType(TEST_TYPE);
		type.getPropertyDescriptors().add(new PropertyDescr(TEST_PROP, BinaryData.class.getName(), true));
		e.updateObjectType(type);
	}
	
	@AfterClass
	public static void setUpAfterClass() throws Exception{
		e.deleteObjectType(TEST_TYPE);
	}
	
	@Test
	public void setHistoricalBinaryValues() throws Exception{	
		TO<?> to = null;
		long start = System.currentTimeMillis();
		try{
			to = e.createObject(TEST_TYPE, new ValueTO[]{new ValueTO(TEST_PROP, data[4])});
			Integer histCount = 0;
			for(BinaryData bd : data){
				e.setPropertyValue(to, TEST_PROP, bd);
				histCount++;
				assertEquals(histCount, e.getHistorySize(to, TEST_PROP));
				assertEquals(bd, e.getPropertyValue(to, TEST_PROP, BinaryData.class));
			}

			Collection<BinaryData> hist = histToBinary(e.getHistory(to, "image", BinaryData.class));
			assertTrue(hist.containsAll(Arrays.asList(data)));
			assertEquals(2, e.getHistory(to, TEST_PROP, new Date(start), 2, BinaryData.class).size());
			
		}finally{
			e.deleteObject(to);
		}
	}
	
	@Test
	public void getHistoricalBinaryValues() throws Exception{	
		TO<?> to = null;
		try{
			long start_1 = System.currentTimeMillis();
			Thread.sleep(1000);
			to = e.createObject(TEST_TYPE, new ValueTO[]{new ValueTO(TEST_PROP, data[4])});
			e.setPropertyValue(to, TEST_PROP, data[0]);
			e.setPropertyValue(to, TEST_PROP, data[1]);
			Thread.sleep(1000);
			long end_1 = System.currentTimeMillis();
		
			Thread.sleep(3000);
			
			long start_2 = System.currentTimeMillis();
			Thread.sleep(1000);
			e.setPropertyValue(to, TEST_PROP, data[2]);
			e.setPropertyValue(to, TEST_PROP, data[3]);
			e.setPropertyValue(to, TEST_PROP, data[4]);
			Thread.sleep(1000);
			long end_2 = System.currentTimeMillis();
	

			Collection<BinaryData> hist = histToBinary(e.getHistory(to, TEST_PROP, new Date(start_1), new Date(end_2), BinaryData.class));
			assertEquals(5, hist.size());
			hist = histToBinary(e.getHistory(to, TEST_PROP, new Date(start_1), new Date(end_1), BinaryData.class));
			assertEquals(3, hist.size());
			assertTrue(hist.contains(data[4]));
			assertTrue(hist.contains(data[0]));
			assertTrue(hist.contains(data[1]));
			hist = histToBinary(e.getHistory(to, TEST_PROP, new Date(start_2), new Date(end_2), BinaryData.class));
			assertEquals(2, hist.size());
			assertTrue(hist.contains(data[2]));
			assertTrue(hist.contains(data[3]));
			
			hist = histToBinary(e.getHistory(to, TEST_PROP, new Date(start_1), 2, BinaryData.class));
			assertEquals(2, hist.size());
			assertTrue(hist.contains(data[4]));
			assertTrue(hist.contains(data[0]));
			
			hist = histToBinary(e.getHistory(to, TEST_PROP, 3, 2, BinaryData.class));
			assertEquals(3, hist.size());
			assertTrue(hist.contains(data[4]));
			assertTrue(hist.contains(data[0]));
			assertTrue(hist.contains(data[1]));
			
		}finally{
			e.deleteObject(to);
		}
	}
	
	@Test
	public void getHistoricalBinaryValuesAsValueTO() throws EnvException{	
		TO<?> to = null;
		try{
			to = e.createObject(TEST_TYPE, new ValueTO[]{new ValueTO(TEST_PROP, data[4])});
			e.setPropertyValue(to, TEST_PROP, data[0]);
			
			Collection<ValueTO> hist = e.getHistory(to, "image", ValueTO.class);
			assertEquals(1, hist.size());
			
		}finally{
			e.deleteObject(to);
		}
	}
	
	private Collection<BinaryData> histToBinary(Collection<ValueTO> hist){
		Collection<BinaryData> result = new ArrayList<BinaryData>(hist.size());
		for (ValueTO vto : hist){
			result.add((BinaryData)vto.getValue());
		}
		return result;
	}

}
