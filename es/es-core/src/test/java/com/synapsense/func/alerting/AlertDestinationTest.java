package com.synapsense.func.alerting;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertTrue;

import java.util.Collection;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.synapsense.dto.AlertType;
import com.synapsense.dto.MessageType;
import com.synapsense.exception.AlertServiceSystemException;
import com.synapsense.exception.IllegalInputParameterException;
import com.synapsense.exception.NoSuchAlertTypeException;
import com.synapsense.func.ConsoleApp;
import com.synapsense.service.AlertService;
import com.synapsense.service.Environment;
import com.synapsense.util.CollectionUtils;
import com.synapsense.util.algo.Predicate;

public class AlertDestinationTest {
	private static final String TEST_ALERT_TYPE = AlertDestinationTest.class.getName();
	private static AlertService ALERTING_SERVICE;
	private static Environment ENVIRONMENT;
	private static AlertingTestHelper helper;

	public static void sleep(long time) {
		try {
			Thread.sleep(time);
		} catch (final InterruptedException e) {
			e.printStackTrace();
		}
	}

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		ConsoleApp.init();

		ENVIRONMENT = ConsoleApp.env;

		ALERTING_SERVICE = ConsoleApp.ALERTING_SERVICE;

		helper = new AlertingTestHelper(ENVIRONMENT, ALERTING_SERVICE);

		helper.newPersistentAlertType(TEST_ALERT_TYPE, "MAJOR", true, false);
		sleep(1000);
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		ALERTING_SERVICE.deleteAlertType(TEST_ALERT_TYPE);
		ALERTING_SERVICE.deleteAllAlerts();
	}

	@Test(expected = NoSuchAlertTypeException.class)
	public void addSimpleEmailDestinationWrongAlertTypeName() throws NoSuchAlertTypeException {
		ALERTING_SERVICE.addActionDestination("NOSUCHTYPE", "TIER1", MessageType.SMTP, "shabanov@mera.ru");
	}

	@Test(expected = AlertServiceSystemException.class)
	public void addSimpleEmailDestinationWrongTierName() throws NoSuchAlertTypeException {
		ALERTING_SERVICE.addActionDestination(TEST_ALERT_TYPE, "NOSUCHTIER", MessageType.SMTP, "shabanov@mera.ru");
	}

	@Test
	public void addSimpleEmailDestination() throws NoSuchAlertTypeException {
		ALERTING_SERVICE.addActionDestination(TEST_ALERT_TYPE, "TIER1", MessageType.SMTP, "shabanov@mera.ru");

		final Collection<String> destinations = ALERTING_SERVICE.getActionDestinations(TEST_ALERT_TYPE, "TIER1",
		        MessageType.SMTP);
		assertNotNull(destinations);
		assertFalse(destinations.isEmpty());
		assertEquals(1, destinations.size());
		assertEquals("shabanov@mera.ru", destinations.iterator().next());

		ALERTING_SERVICE.removeActionDestination(TEST_ALERT_TYPE, "TIER1", MessageType.SMTP, "shabanov@mera.ru");
	}

	@Test
	public void addUserReferenceAsDestination() throws NoSuchAlertTypeException {
		ALERTING_SERVICE.addActionDestination(TEST_ALERT_TYPE, "TIER1", MessageType.SMTP, "admin");

		final Collection<String> destinations = ALERTING_SERVICE.getActionDestinations(TEST_ALERT_TYPE, "TIER1",
		        MessageType.SMTP);
		assertNotNull(destinations);
		assertFalse(destinations.isEmpty());
		assertEquals(1, destinations.size());
		assertEquals("admin", destinations.iterator().next());

		ALERTING_SERVICE.removeActionDestination(TEST_ALERT_TYPE, "TIER1", MessageType.SMTP, "admin");
	}

	@Test(expected = IllegalInputParameterException.class)
	public void testGetAlertTypesByDestinationNullInputDestinationFailure() {
		ALERTING_SERVICE.getAlertTypesByDestination(null, null);
	}

	@Test(expected = IllegalInputParameterException.class)
	public void testGetAlertTypesByDestinationNullInputMessageTypeFailure() {
		ALERTING_SERVICE.getAlertTypesByDestination("admin", null);
	}

	@Test
	public void testGetAlertTypesByExistingDestination() throws NoSuchAlertTypeException {
		ALERTING_SERVICE.addActionDestination(TEST_ALERT_TYPE, "TIER1", MessageType.SMTP, "admin");
		try {
			// check destinations are set
			final Collection<String> destinations = ALERTING_SERVICE.getActionDestinations(TEST_ALERT_TYPE, "TIER1",
			        MessageType.SMTP);
			assertNotNull(destinations);
			assertFalse(destinations.isEmpty());
			assertEquals(1, destinations.size());
			assertEquals("admin", destinations.iterator().next());

			Collection<AlertType> alertTypes = ALERTING_SERVICE.getAlertTypesByDestination("admin", MessageType.SMTP);
			assertNotNull(alertTypes);
			assertEquals(alertTypes.size(), 1);
			assertEquals(TEST_ALERT_TYPE, alertTypes.iterator().next().getName());

		} finally {
			ALERTING_SERVICE.removeActionDestination(TEST_ALERT_TYPE, "TIER1", MessageType.SMTP, "admin");
		}
	}

	@Test
	public void testGetAlertTypesByNonExistingDestination() throws NoSuchAlertTypeException {
		// check destinations are set
		final Collection<String> destinations = ALERTING_SERVICE.getActionDestinations(TEST_ALERT_TYPE, "TIER1",
		        MessageType.SMTP);
		assertNotNull(destinations);
		assertTrue(destinations.isEmpty());

		Collection<AlertType> alertTypes = ALERTING_SERVICE.getAlertTypesByDestination("admin", MessageType.SMTP);
		assertNotNull(alertTypes);
		assertTrue(alertTypes.isEmpty());
	}

	@Test
	public void testGetMultipleAlertTypesByDestination() throws NoSuchAlertTypeException {
			
		AlertType one = helper.newPersistentAlertType("test one", "MAJOR", true, false);
		AlertType two = helper.newPersistentAlertType("test two", "MAJOR", true, false);

		sleep(1000);
		
		ALERTING_SERVICE.addActionDestination(one.getName(), "TIER1", MessageType.SMTP, "admin");
		ALERTING_SERVICE.addActionDestination(two.getName(), "TIER2", MessageType.SMTP, "admin");
		try {
			// check destinations are set
			Collection<String> destinations = ALERTING_SERVICE.getActionDestinations(one.getName(), "TIER1",
			        MessageType.SMTP);
			assertNotNull(destinations);
			assertFalse(destinations.isEmpty());
			assertEquals(1, destinations.size());
			assertEquals("admin", destinations.iterator().next());
			
			destinations = ALERTING_SERVICE.getActionDestinations(two.getName(), "TIER2",
			        MessageType.SMTP);
			assertNotNull(destinations);
			assertFalse(destinations.isEmpty());
			assertEquals(1, destinations.size());
			assertEquals("admin", destinations.iterator().next());

			Collection<AlertType> alertTypes = ALERTING_SERVICE.getAlertTypesByDestination("admin", MessageType.SMTP);
			assertNotNull(alertTypes);
			assertEquals(alertTypes.size(), 2);
			AlertType[] alertTypesArray = alertTypes.toArray(new AlertType[]{});
			assertNotSame(alertTypesArray[0], alertTypesArray[1]);
			
		} finally {
			ALERTING_SERVICE.removeActionDestination(one.getName(), "TIER1", MessageType.SMTP, "admin");
			ALERTING_SERVICE.removeActionDestination(two.getName(), "TIER2", MessageType.SMTP, "admin");
			ALERTING_SERVICE.deleteAlertType(one.getName());
			ALERTING_SERVICE.deleteAlertType(two.getName());
		}
	}
	
}