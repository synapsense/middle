package com.synapsense.func.erlnode.commands;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.ericsson.otp.erlang.OtpErlangDouble;
import com.ericsson.otp.erlang.OtpErlangInt;
import com.ericsson.otp.erlang.OtpErlangLong;
import com.ericsson.otp.erlang.OtpErlangObject;
import com.ericsson.otp.erlang.OtpErlangString;
import com.ericsson.otp.erlang.OtpErlangTuple;
import com.synapsense.dto.ObjectType;
import com.synapsense.dto.PropertyDescr;
import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;
import com.synapsense.impl.erlnode.ExceptionTypes;
import com.synapsense.impl.erlnode.commands.GetPropertyValueCommand;
import com.synapsense.impl.erlnode.utils.ToConverter;
import com.synapsense.service.Environment;

public class GetPropertyValueCommandTest {
	static UnitTestHelper utHelper = null;
	static String testType = "testingObjType_GetPropertyValueTwoParTest";
	static Map<String, Object> envMap;
	static Environment env;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		utHelper = new UnitTestHelper();
		utHelper.initializeEnvironment();
		env = utHelper.getEnvironment();
		envMap = new HashMap<String, Object>();
		envMap.put("env", env);

	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		utHelper.removeTypeCompletly(testType);
		utHelper.dispose();
	}

	@Before
	public void setUp() throws Exception {

		ObjectType otype = env.createObjectType(testType);

		otype.getPropertyDescriptors().add(new PropertyDescr("name", String.class.getName()));
		otype.getPropertyDescriptors().add(new PropertyDescr("int", Integer.class.getName()));
		otype.getPropertyDescriptors().add(new PropertyDescr("long", Long.class.getName()));
		otype.getPropertyDescriptors().add(new PropertyDescr("double", Double.class.getName()));
		otype.getPropertyDescriptors().add(new PropertyDescr("to", TO.class.getName()));
		env.updateObjectType(otype);
	}

	@After
	public void tearDown() throws Exception {
		env.deleteObjectType(testType);
	}

	@Test
	public void SuccessGetPropertyValueTest() throws Exception {
		GetPropertyValueCommand command = new GetPropertyValueCommand(envMap);
		TO<?> obj1 = env.createObject(testType, new ValueTO[] { new ValueTO("name", "obj1"),
		        new ValueTO("int", new Integer(1)), new ValueTO("double", new Double(3.3)), });
		TO<?> obj2 = env.createObject(testType, new ValueTO[] { new ValueTO("name", "obj2"),
		        new ValueTO("long", new Long(1L)), new ValueTO("to", obj1) });

		OtpErlangTuple params2 = new OtpErlangTuple(new OtpErlangObject[] { new OtpErlangString("GetPropertyValue"),
		        ToConverter.serializeTo(obj1), new OtpErlangString("name") });
		OtpErlangTuple res = (OtpErlangTuple) command.run(params2);
		assertEquals("ok", res.elementAt(0).toString());
		assertEquals("obj1", ((OtpErlangString) res.elementAt(1)).stringValue());

		params2 = new OtpErlangTuple(new OtpErlangObject[] { new OtpErlangString("GetPropertyValue"),
		        ToConverter.serializeTo(obj1), new OtpErlangString("int") });
		res = (OtpErlangTuple) command.run(params2);
		assertEquals("ok", res.elementAt(0).toString());
		assertEquals(1, ((OtpErlangInt) res.elementAt(1)).intValue());

		params2 = new OtpErlangTuple(new OtpErlangObject[] { new OtpErlangString("GetPropertyValue"),
		        ToConverter.serializeTo(obj1), new OtpErlangString("double") });
		res = (OtpErlangTuple) command.run(params2);
		assertEquals("ok", res.elementAt(0).toString());
		assertEquals(3.3D, ((OtpErlangDouble) res.elementAt(1)).doubleValue(), 1);

		params2 = new OtpErlangTuple(new OtpErlangObject[] { new OtpErlangString("GetPropertyValue"),
		        ToConverter.serializeTo(obj2), new OtpErlangString("long") });
		res = (OtpErlangTuple) command.run(params2);
		assertEquals("ok", res.elementAt(0).toString());
		assertEquals(1L, ((OtpErlangLong) res.elementAt(1)).longValue());

		params2 = new OtpErlangTuple(new OtpErlangObject[] { new OtpErlangString("GetPropertyValue"),
		        ToConverter.serializeTo(obj2), new OtpErlangString("to") });
		res = (OtpErlangTuple) command.run(params2);
		assertEquals("ok", res.elementAt(0).toString());
		assertEquals(obj1, ToConverter.parseTo((OtpErlangTuple) res.elementAt(1)));
		env.deleteObject(obj1);
		env.deleteObject(obj2);
	}

	@Test
	public void FailGetPropertyValue() throws Exception {
		GetPropertyValueCommand command = new GetPropertyValueCommand(envMap);
		TO<?> obj1 = env.createObject(testType, new ValueTO[] { new ValueTO("name", "obj1"),
		        new ValueTO("int", new Integer(1)), new ValueTO("double", new Double(3.3)), });
		env.deleteObject(obj1);

		OtpErlangTuple params = new OtpErlangTuple(new OtpErlangObject[] { new OtpErlangString("GetPropertyValue"),
		        ToConverter.serializeTo(obj1), new OtpErlangString("long") });
		OtpErlangTuple res = (OtpErlangTuple) command.run(params);
		assertEquals("exception", res.elementAt(0).toString());
		assertEquals(ExceptionTypes.OBJECT_NOT_FOUND.getExceptionType(),
		        ((OtpErlangString) res.elementAt(1)).stringValue());
		TO<?> obj2 = env.createObject(testType, new ValueTO[] { new ValueTO("name", "obj1"),
		        new ValueTO("int", new Integer(1)), new ValueTO("double", new Double(3.3)), });
		params = new OtpErlangTuple(new OtpErlangObject[] { new OtpErlangString("GetPropertyValue"),
		        ToConverter.serializeTo(obj1), new OtpErlangString("nonExistingProp") });
		res = (OtpErlangTuple) command.run(params);
		assertEquals("exception", res.elementAt(0).toString());
		assertEquals(ExceptionTypes.PROPERTY_NOT_FOUND.getExceptionType(),
		        ((OtpErlangString) res.elementAt(1)).stringValue());
		env.deleteObject(obj2);

	}

	@Test
	public void InvalidParametersTest() throws Exception {
		GetPropertyValueCommand command = new GetPropertyValueCommand(envMap);
		OtpErlangTuple obj = (OtpErlangTuple) command.run(new OtpErlangTuple(
		        new OtpErlangObject[] { new OtpErlangString("createObjectCommand1"), }));
		assertEquals("exception", obj.elementAt(0).toString());
		assertEquals(ExceptionTypes.ILLEGAL_ARGUMENTS.getExceptionType(),
		        ((OtpErlangString) obj.elementAt(1)).stringValue());
		obj = (OtpErlangTuple) command.run(new OtpErlangTuple(new OtpErlangObject[] {
		        new OtpErlangString("createObjectCommand1"), new OtpErlangString(testType) }));
		assertEquals("exception", obj.elementAt(0).toString());
		assertEquals(ExceptionTypes.ILLEGAL_ARGUMENTS.getExceptionType(),
		        ((OtpErlangString) obj.elementAt(1)).stringValue());
	}

}
