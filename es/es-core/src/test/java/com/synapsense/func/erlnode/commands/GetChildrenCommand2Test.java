package com.synapsense.func.erlnode.commands;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.ericsson.otp.erlang.OtpErlangList;
import com.ericsson.otp.erlang.OtpErlangObject;
import com.ericsson.otp.erlang.OtpErlangString;
import com.ericsson.otp.erlang.OtpErlangTuple;
import com.synapsense.dto.CollectionTO;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.impl.erlnode.ExceptionTypes;
import com.synapsense.impl.erlnode.commands.GetChildrenCommand2;
import com.synapsense.impl.erlnode.utils.CollectionToConverter;
import com.synapsense.impl.erlnode.utils.ToConverter;
import com.synapsense.service.Environment;

public class GetChildrenCommand2Test {
	static UnitTestHelper utHelper = null;
	static String testType1 = "testType1";
	static String testType2 = "testType2";
	static String testType3 = "testType3";
	static Map<String, Object> envMap;
	static Environment env;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		utHelper = new UnitTestHelper();
		utHelper.initializeEnvironment();
		env = utHelper.getEnvironment();
		envMap = new HashMap<String, Object>();
		envMap.put("env", env);

	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		utHelper.removeTypeCompletly(testType1);
		utHelper.removeTypeCompletly(testType2);
		utHelper.removeTypeCompletly(testType3);
		utHelper.dispose();
	}

	@Before
	public void setUp() throws Exception {
		env.deleteObjectType(testType1);
		env.deleteObjectType(testType2);
		env.deleteObjectType(testType3);
		env.createObjectType(testType1);
		env.createObjectType(testType2);
		env.createObjectType(testType3);

	}

	@After
	public void tearDown() throws Exception {
		env.deleteObjectType(testType1);
		env.deleteObjectType(testType2);
		env.deleteObjectType(testType3);
	}

	@Test
	public void SuccessGetChildrenTest() throws Exception {

		TO<?> parent = env.createObject(testType1);
		TO<?> child1 = env.createObject(testType2);
		TO<?> child2 = env.createObject(testType3);
		env.setRelation(parent, child1);
		env.setRelation(parent, child2);
		GetChildrenCommand2 command = new GetChildrenCommand2(envMap);
		OtpErlangTuple obj = (OtpErlangTuple) command.run(new OtpErlangTuple(new OtpErlangObject[] {
		        new OtpErlangString("GetChildrenCommand2"), ToConverter.serializeTo(parent),
		        new OtpErlangString(testType2) }));
		assertEquals("ok", obj.elementAt(0).toString());
		Collection<TO<?>> res = ToConverter.parseToList((OtpErlangList) obj.elementAt(1));
		assertEquals(1, res.size());
		assertEquals(child1, res.toArray()[0]);

		obj = (OtpErlangTuple) command.run(new OtpErlangTuple(new OtpErlangObject[] {
		        new OtpErlangString("GetChildrenCommand2"), ToConverter.serializeTo(parent),
		        new OtpErlangString(testType1) }));
		assertEquals("ok", obj.elementAt(0).toString());
		res = ToConverter.parseToList((OtpErlangList) obj.elementAt(1));
		assertEquals(0, res.size());

		obj = (OtpErlangTuple) command.run(new OtpErlangTuple(new OtpErlangObject[] {
		        new OtpErlangString("GetChildrenCommand2"), ToConverter.serializeTo(parent),
		        new OtpErlangString(testType1) }));
		assertEquals("ok", obj.elementAt(0).toString());
		res = ToConverter.parseToList((OtpErlangList) obj.elementAt(1));
		assertEquals(0, res.size());

		List<TO<?>> objs = new ArrayList<TO<?>>();
		objs.add(parent);
		objs.add(child1);
		objs.add(child2);
		obj = (OtpErlangTuple) command.run(new OtpErlangTuple(new OtpErlangObject[] {
		        new OtpErlangString("GetChildrenCommand2"), ToConverter.serializeToList(objs),
		        new OtpErlangString(testType3) }));
		assertEquals("ok", obj.elementAt(0).toString());
		OtpErlangList otpList = (OtpErlangList) obj.elementAt(1);
		assertEquals(3, otpList.arity());

		CollectionTO res1 = CollectionToConverter.parseCollectionTo(env, testType1,
		        (OtpErlangTuple) otpList.elementAt(0));
		CollectionTO res2 = CollectionToConverter.parseCollectionTo(env, testType2,
		        (OtpErlangTuple) otpList.elementAt(1));
		assertEquals(child2, res1.getPropValues().get(0).getValue());
		assertEquals(0, res2.getPropValues().size());

	}

	@Test
	public void FailGetChildrenTest() throws Exception {

		GetChildrenCommand2 command = new GetChildrenCommand2(envMap);
		TO<?> to = TOFactory.getInstance().loadTO(testType1 + ":11");
		OtpErlangTuple obj = (OtpErlangTuple) command.run(new OtpErlangTuple(
		        new OtpErlangObject[] { new OtpErlangString("GetChildrenCommand2"), ToConverter.serializeTo(to),
		                new OtpErlangString(testType1) }));
		assertEquals("exception", obj.elementAt(0).toString());
		assertEquals(ExceptionTypes.NO_OBJECT_TYPE.getExceptionType(),
		        ((OtpErlangString) obj.elementAt(1)).stringValue());

		List<TO<?>> objs = new ArrayList<TO<?>>();
		objs.add(to);
		obj = (OtpErlangTuple) command.run(new OtpErlangTuple(new OtpErlangObject[] {
		        new OtpErlangString("GetChildrenCommand2"), ToConverter.serializeToList(objs),
		        new OtpErlangString(testType1) }));
		assertEquals("ok", obj.elementAt(0).toString());
		OtpErlangList otpList = (OtpErlangList) obj.elementAt(1);
		assertEquals(1, otpList.arity());

		CollectionTO res1 = CollectionToConverter.parseCollectionTo(env, testType1,
		        (OtpErlangTuple) otpList.elementAt(0));
		assertEquals(0, res1.getPropValues().size());
	}

	@Test
	public void InvalidParametersTest() throws Exception {
		GetChildrenCommand2 command = new GetChildrenCommand2(envMap);
		OtpErlangTuple obj = (OtpErlangTuple) command.run(new OtpErlangTuple(
		        new OtpErlangObject[] { new OtpErlangString("GetChildrenCommand2"), }));
		assertEquals("exception", obj.elementAt(0).toString());
		assertEquals(ExceptionTypes.ILLEGAL_ARGUMENTS.getExceptionType(),
		        ((OtpErlangString) obj.elementAt(1)).stringValue());
	}
}
