package com.synapsense.func.alerting;

import java.util.Arrays;
import java.util.Collection;

import com.synapsense.dto.AlertFilter;
import com.synapsense.dto.AlertFilterBuilder;
import com.synapsense.dto.TO;

final class AlertFilters {
	private AlertFilters() {
	}

	public static final AlertFilter dismissedAlertsPropagate(TO<?>... hosts) {
		AlertFilterBuilder filter = new AlertFilterBuilder();
		return filter.dismissed().onHosts(Arrays.asList(hosts)).propagate(true).build();
	}

	public static final AlertFilter dismissedAlerts(TO<?>... hosts) {
		AlertFilterBuilder filter = new AlertFilterBuilder();
		return filter.dismissed().onHosts(Arrays.asList(hosts)).build();
	}

	public static final AlertFilter allSystemAlerts() {
		AlertFilterBuilder filter = new AlertFilterBuilder();
		return filter.system().build();
	}

	public static final AlertFilter dismissedSystemAlerts() {
		AlertFilterBuilder filter = new AlertFilterBuilder();
		return filter.dismissed().system().build();
	}

	public static final AlertFilter allAlertsPropagate(TO<?>... hosts) {
		return allAlertsPropagate(Arrays.asList(hosts));
	}

	public static final AlertFilter allAlertsPropagate(Collection<TO<?>> hosts) {
		AlertFilterBuilder filter = new AlertFilterBuilder();
		return filter.onHosts(hosts).propagate(true).build();
	}

	public static final AlertFilter allAlerts(TO<?> host) {
		AlertFilterBuilder filter = new AlertFilterBuilder();
		return filter.onHost(host).build();
	}

}
