package com.synapsense.func.alerting;

import static org.junit.Assert.assertEquals;

import java.math.RoundingMode;
import java.text.NumberFormat;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.synapsense.dto.Alert;
import com.synapsense.dto.AlertPriority;
import com.synapsense.dto.AlertType;
import com.synapsense.dto.MessageTemplate;
import com.synapsense.dto.MessageTemplateRef;
import com.synapsense.dto.MessageType;
import com.synapsense.dto.ObjectType;
import com.synapsense.dto.PriorityTier;
import com.synapsense.dto.PropertyDescr;
import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.AlertTypeAlreadyExistsException;
import com.synapsense.func.ConsoleApp;
import com.synapsense.service.AlertService;
import com.synapsense.service.Environment;
import com.synapsense.service.UserManagementService;
import com.synapsense.util.CollectionUtils;

public class NotificationFunctionalTest {

	private static AlertService ALERTING_SERVICE;
	private static Environment ENVIRONMENT;
	private static UserManagementService USER_SERVICE;

	private static String TEST_TYPE = "TEST_TYPE";

	private static AlertingTestHelper helper;

	public static void main(String[] args) {
		Double d = Double.parseDouble("1.5");
		NumberFormat numberFormatter = NumberFormat.getInstance(Locale.US);
		numberFormatter.setRoundingMode(RoundingMode.HALF_UP);
		numberFormatter.setGroupingUsed(false);
		numberFormatter.setMaximumFractionDigits(2);
		numberFormatter.setMinimumFractionDigits(2);
		System.out.println(numberFormatter.format(d));
	}

	public static void sleep() {
		try {
			Thread.sleep(0);
		} catch (final InterruptedException e) {
			e.printStackTrace();
		}
	}

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		ConsoleApp.init();

		ENVIRONMENT = ConsoleApp.env;

		ALERTING_SERVICE = ConsoleApp.ALERTING_SERVICE;

		helper = new AlertingTestHelper(ENVIRONMENT, ALERTING_SERVICE);

		USER_SERVICE = ConsoleApp.USER_SERVICE;

		final ObjectType testType = ENVIRONMENT.createObjectType(TEST_TYPE);
		testType.getPropertyDescriptors().add(new PropertyDescr("name", String.class.getName()));
		testType.getPropertyDescriptors().add(new PropertyDescr("lastValue", Double.class.getName()));
		testType.getPropertyDescriptors().add(new PropertyDescr("location", String.class.getName()));
		testType.getPropertyDescriptors().add(new PropertyDescr("body", String.class.getName()));
		testType.getPropertyDescriptors().add(new PropertyDescr("header", String.class.getName()));

		ENVIRONMENT.updateObjectType(testType);
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		ALERTING_SERVICE.deleteAllAlerts();
		ENVIRONMENT.deleteObjectType(TEST_TYPE);
	}

	@Test
	public void sendingNotifications() throws Exception {

		final TO<?> admin = USER_SERVICE.getUser("admin");
		USER_SERVICE.setUserPreferenceValue(admin, "UnitsSystem", "SI");

		USER_SERVICE.setProperty(admin, USER_SERVICE.USER_PRIMARY_EMAIL, "shabanov@mera.ru");
		USER_SERVICE.setProperty(admin, USER_SERVICE.USER_SMS_ADDRESS, "shabanov@mera.ru");

		final String headerText = "Tier <$MSG_TIER$>, message num=<$MSG_NUM$> System  Alert - $parent(TEST_TYPE).name$  $location$   $ALERT_NAME$ @ $TRIGGER_DATA_VALUE$";
		final String bodyText = "Tier $MSG_TIER$ System  Alert - in room: $parent(TEST_TYPE).name$ which is located in $parent(TEST_TYPE).location$ \\ $location$ Alert: $ALERT_NAME$ Trigger Value: $MESSAGE$";

		final TO roomRef = ENVIRONMENT.createObject(TEST_TYPE, new ValueTO[] { new ValueTO("name", "ROOM"),
		        new ValueTO("location", "room location") });
		final TO alertHost = ENVIRONMENT.createObject(TEST_TYPE, new ValueTO[] { new ValueTO("location", "x,y"),
		        new ValueTO("lastValue", 1.5) });
		ENVIRONMENT.setRelation(roomRef, alertHost);

		final TO templateRef = ENVIRONMENT.createObject("ALERT_MESSAGE_TEMPLATE", new ValueTO[] {
		        new ValueTO("name", "email"), new ValueTO("messagetype", "1"), new ValueTO("header", headerText),
		        new ValueTO("body", bodyText) });

		AlertPriority major = ALERTING_SERVICE.getAlertPriority("MAJOR");
		final PriorityTier newT1 = new PriorityTier("TIER1", 10000L, 3, true, true, true);
		final PriorityTier newT2 = new PriorityTier("TIER2", 10000L, 3, true, true, true);
		final PriorityTier newT3 = new PriorityTier("TIER3", 10000L, 3, false, true, true);
		final List<PriorityTier> tiers = CollectionUtils.newList();
		tiers.add(newT1);
		tiers.add(newT2);
		tiers.add(newT3);

		major = new AlertPriority("MAJOR", Long.MAX_VALUE, tiers);
		ALERTING_SERVICE.updateAlertPriority("MAJOR", major);

		Set<MessageTemplate> templates = CollectionUtils.newSet();
		templates.add(new MessageTemplateRef(MessageType.SMTP, templateRef));
		templates.add(new MessageTemplateRef(MessageType.SMS, templateRef));

		final AlertType at = new AlertType("ATYPE1", "ATYPE1", "info", "MAJOR", "header", "body", templates, true, false, false, false);

		try {
			ALERTING_SERVICE.deleteAlertType("ATYPE1");
			ALERTING_SERVICE.createAlertType(at);
		} catch (final AlertTypeAlreadyExistsException e) {
			e.printStackTrace();
		}

		ALERTING_SERVICE.addActionDestination("ATYPE1", "TIER1", MessageType.SMTP, "admin");
		ALERTING_SERVICE.addActionDestination("ATYPE1", "TIER2", MessageType.SMS, "admin");
		ALERTING_SERVICE.addActionDestination("ATYPE1", "TIER3", MessageType.SMTP, "admin");

		final Alert alertInstance = new Alert("alertFullMessageReplacingWithRealMacros", "ATYPE1", new Date(), "$"
		        + templateRef.toString() + ".name$ , $conv(1.5,209)$", alertHost);
		ALERTING_SERVICE.raiseAlert(alertInstance);

		try {
			// testing macros replacer
			final Collection<Alert> alerts = ALERTING_SERVICE.getActiveAlerts(alertHost, false);
			assertEquals(1, alerts.size());
			final Alert raised = alerts.iterator().next();
			// ALERTING_SERVICE.acknowledgeAlert(raised.getAlertId(),
			// "ack_test");
			ALERTING_SERVICE.resolveAlert(raised.getAlertId(), "resolve_test");

			/*
			 * assertEquals(
			 * "Tier <no_replacement> System  Alert - in room: ROOM which is located in room location \\ x,y Alert: alertFullMessageReplacingWithRealMacros Trigger Value: email , 1.5"
			 * , raised.getFullMessage());
			 */

		} finally {
			USER_SERVICE.removeUserPreference(admin, "UnitsSystem");
			/*
			 * ALERTING_SERVICE.deleteAlertType("ATYPE1");
			 * 
			 * ENVIRONMENT.deleteObject(roomRef);
			 * ENVIRONMENT.deleteObject(templateRef);
			 * ENVIRONMENT.deleteObject(alertHost);
			 */
		}
	}
}
