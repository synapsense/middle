package com.synapsense.func.reporting.mail;

import static com.synapsense.impl.reporting.TestUtils.getEmptyReportDesignPath;

import java.util.Properties;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import com.synapsense.impl.reporting.mail.JbossMailSession;
import com.synapsense.impl.reporting.mail.SMTPMailer;
import com.synapsense.service.reporting.exception.SmtpReportingException;

/**
 * This test shouldn't be used as a part of unit tests suite as it sends mail
 * messages
 * 
 * 
 */
public class SmtpMailerTest {

	private Properties props;

	@Before
	public void setUp() throws Exception {
		props = new Properties();

		/*
		 * props.put("mail.smtp.host", "smtp.gmail.com");
		 * props.put("mail.smtp.auth", "true"); props.put("mail.debug", "true");
		 * props.put("mail.smtp.port", "465");
		 * props.put("mail.smtp.socketFactory.port", "465");
		 * props.put("mail.smtp.socketFactory.class",
		 * "javax.net.ssl.SSLSocketFactory");
		 * props.put("mail.smtp.socketFactory.fallback", "false");
		 */

		/*
		 * props.put("mail.transport.protocol", "smtps");
		 * props.put("mail.smtps.port", "465"); props.put("mail.smtps.host",
		 * "smtp.gmail.com"); props.put("mail.smtps.user", "dgrudzinskiy");
		 * props.put("mail.smtps.password", "Mac3_dash"); props.put("mail.from",
		 * "dgrudzinskiy@gmail.com"); props.put("mail.smtps.auth", true);
		 */

		props.put("mail.transport.protocol", "smtp");
		props.put("mail.smtp.port", "25");
		props.put("mail.smtp.host", "qmail.merann.ru");
		props.put("mail.from", "prj.synapsense@mera.ru");
	}

	@Test
	public void testSendSimpleMessage() {
		SMTPMailer mailer = new SMTPMailer(new JbossMailSession());

		try {
			mailer.send("Test Subject", "Test Body",
			        new String[] { getEmptyReportDesignPath() },
			        "gryabov@mera.ru");
		} catch (SmtpReportingException e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}

	@Test
	public void testSendAttachment() {
		SMTPMailer mailer = new SMTPMailer(new JbossMailSession());

		try {
			mailer.send("Test Subject", "Test Body", null, "gryabov@mera.ru");
		} catch (SmtpReportingException e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}
}
