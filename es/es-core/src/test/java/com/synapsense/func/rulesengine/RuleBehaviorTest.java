package com.synapsense.func.rulesengine;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.synapsense.dto.CRERule;
import com.synapsense.dto.ObjectType;
import com.synapsense.dto.PropertyDescr;
import com.synapsense.dto.RuleType;
import com.synapsense.dto.TO;
import com.synapsense.exception.EnvException;
import com.synapsense.func.ConsoleApp;
import com.synapsense.service.Environment;
import com.synapsense.service.RulesEngine;

public class RuleBehaviorTest {

	private static RulesEngine cre;
	private static Environment env;

	private static final String RULE = "import com.synapsense.service.Environment; public class TestRule implements  com.synapsense.rulesengine.core.environment.RuleAction {"
	        + "\n public Object run(com.synapsense.rulesengine.core.environment.RuleI triggeredRule, com.synapsense.rulesengine.core.environment.Property calculated) {"
	        + "        Environment env = calculated.getEnvironment(); return env.getPropertyValue(calculated.getDataSource().getHostObjectTO(), \"input\", Double.class);} "
	        + " \n public void setInput1( com.synapsense.rulesengine.core.environment.Property i1) {input1 = i1;} \n }";

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		ConsoleApp.init();

		cre = ConsoleApp.cre;
		env = ConsoleApp.env;

		ObjectType type = env.createObjectType("BEH_TEST");
		type.getPropertyDescriptors().add(new PropertyDescr("lastValue", Double.class));
		type.getPropertyDescriptors().add(new PropertyDescr("input", Double.class));
		env.updateObjectType(type);
	}

	@Test
	public void ruleAllwaysRun() throws Exception {
		TO<?> rtO = cre.getRuleTypeId("TEST_RULE_TYPE");
		try {
			if (rtO != null) {
				cre.removeRuleType(rtO);
			}
			RuleType rt = new RuleType("TEST_RULE_TYPE", RULE, "Groovy", false, "0 0/5 * * * ?", RuleType.ALWAYS_RUN);
			rtO = cre.addRuleType(rt);

			TO<?> obj = env.createObject("BEH_TEST");

			CRERule rule = new CRERule("test_rule1", rtO, obj, "lastValue");
			cre.addRule(rule);

			env.setPropertyValue(obj, "input", 0.0);
			Assert.assertEquals(0.0, env.getPropertyValue(obj, "lastValue", Double.class), 0.001);

			env.setPropertyValue(obj, "input", 5.0);
			Assert.assertEquals(5.0, env.getPropertyValue(obj, "lastValue", Double.class), 0.001);

			env.setPropertyValue(obj, "input", 10.0);
			Assert.assertEquals(10.0, env.getPropertyValue(obj, "lastValue", Double.class), 0.001);
		} finally {
			cre.removeRuleType(rtO);
		}
	}

	@Test
	public void parallelPropertyLoad() throws Exception {
		TO<?> rtO = cre.getRuleTypeId("TEST_RULE_TYPE");
		try {
			if (rtO != null) {
				cre.removeRuleType(rtO);
			}
			RuleType rt = new RuleType("TEST_RULE_TYPE", RULE, "Groovy", false, "0 0/5 * * * ?", RuleType.ALWAYS_RUN);
			rtO = cre.addRuleType(rt);

			final TO<?> obj = env.createObject("BEH_TEST");

			CRERule rule = new CRERule("test_rule1", rtO, obj, "lastValue");
			cre.addRule(rule);

			env.setPropertyValue(obj, "input", 10.0);
			Assert.assertEquals(10.0, env.getPropertyValue(obj, "lastValue", Double.class), 0.001);

			int nbChecks = 10000;
			final CountDownLatch latch = new CountDownLatch(nbChecks);
			ExecutorService pool = Executors.newFixedThreadPool(10);

			long start = System.currentTimeMillis();
			for (int i = 0; i < nbChecks; i++) {
				pool.execute(new Runnable() {

					@Override
					public void run() {
						try {
							Assert.assertEquals(10.0, env.getPropertyValue(obj, "lastValue", Double.class), 0.001);
						} catch (Exception e) {
							e.printStackTrace();
						}
						latch.countDown();
					}
				});
			}
			latch.await();
			System.out.println("Time: " + (System.currentTimeMillis() - start));
		} finally {
			cre.removeRuleType(rtO);
		}
	}

	@AfterClass
	public static void tearDownAfterClass() throws EnvException {
		env.deleteObjectType("BEH_TEST");
	}
}
