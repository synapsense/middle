package com.synapsense.func;

import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.EnvException;
import com.synapsense.service.*;
import com.synapsense.util.JNDINames;
import com.synapsense.util.ServerLoginModule;
import org.apache.log4j.Logger;
import org.jboss.ejb.client.ContextSelector;
import org.jboss.ejb.client.EJBClientConfiguration;
import org.jboss.ejb.client.EJBClientContext;
import org.jboss.ejb.client.PropertiesBasedEJBClientConfiguration;
import org.jboss.ejb.client.remoting.ConfigBasedEJBClientContextSelector;

import javax.naming.NamingException;
import javax.security.auth.login.LoginException;
import javax.security.sasl.SaslException;
import java.util.Collection;
import java.util.Properties;

/**
 * Created by wfi on 6/29/2015.
 */
public class MockConsole {
    protected final static Logger log = Logger.getLogger(ConsoleApp.class);

    private Properties ejbProps = null;

    private boolean loggedIn = false;

    public MockConsole() {


        this.ejbProps = new Properties();
        {
            this.ejbProps.put("endpoint.name", "client_1");
            this.ejbProps.put("remote.connectionprovider.create.options.org.xnio.Options.SSL_ENABLED", "false");
            this.ejbProps.put("remote.connections", "default");
            this.ejbProps.put("remote.connection.default.host", "localhost");
            this.ejbProps.put("remote.connection.default.port", "4447");
            this.ejbProps.put("remote.connection.default.connect.options.org.xnio.Options.SASL_POLICY_NOANONYMOUS", "true");
            this.ejbProps.put("remote.connection.default.connect.options.org.xnio.Options.SASL_DISALLOWED_MECHANISMS", "JBOSS-LOCAL-USER");
            this.ejbProps.put("remote.connection.default.connect.options.org.xnio.Options.SASL_POLICY_NOPLAINTEXT", "false");
        }

    }

    public TO<?> getObject(final String type, final String name) throws EnvException {
        ValueTO[] criteria = new ValueTO[] { new ValueTO("name", name) };
        Collection<TO<?>> objList = this.getEnv().getObjects(type, criteria);

        if (objList != null && objList.size() > 0) {
            return (TO) objList.toArray()[0];
        }
        return null;
    }

    public void login(final String user, final String pwd) throws LoginException, NamingException, SaslException {

        if (loggedIn) {
            this.logout();
        }

        this.ejbProps.put("remote.connection.default.username", user);
        this.ejbProps.put("remote.connection.default.password", pwd);

        try {
            EJBClientConfiguration cc = new PropertiesBasedEJBClientConfiguration(this.ejbProps);
            ContextSelector<EJBClientContext> selector = new ConfigBasedEJBClientContextSelector(cc);
            EJBClientContext.setSelector(selector);
        } catch (Exception e) {
            System.out.println("EXCEPTION FROM EJB");
            throw new LoginException(e.getLocalizedMessage());
        }

        ServerLoginModule.login(user, pwd);
        loggedIn = true;
    }

    public void logout() throws LoginException {
        ServerLoginModule.logout();
        loggedIn = false;
    }

    public Environment getEnv() {
        try {
            return ServerLoginModule.getService("Environment", Environment.class);
        } catch (NamingException e) {
            return null;
        }
    }

    public RulesEngine getCRE() {
        try {
            return ServerLoginModule.getProxy(JNDINames.CRE_NAME, RulesEngine.class);
        } catch (NamingException e) {
            return null;
        }
    }

    public AlertService getAlertingService() {
        try {
            return ServerLoginModule.getService("AlertService", AlertService.class);
        } catch (NamingException e) {
            return null;
        }
    }

    public UserManagementService getUserService() {
        try {
            return ServerLoginModule.getService("UserManagementService", UserManagementService.class);
        } catch (NamingException e) {
            return null;
        }
    }

    public <INTERFACE> INTERFACE getService(String serviceName, Class<INTERFACE> clazz) {
        try {
            return ServerLoginModule.getService(serviceName, clazz);
        } catch (NamingException e) {
            return null;
        }
    }

}
