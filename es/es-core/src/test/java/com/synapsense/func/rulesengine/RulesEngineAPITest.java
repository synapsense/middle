package com.synapsense.func.rulesengine;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import com.synapsense.exception.ObjectExistsException;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.synapsense.dto.CRERule;
import com.synapsense.dto.ObjectType;
import com.synapsense.dto.PropertyDescr;
import com.synapsense.dto.Relation;
import com.synapsense.dto.RuleType;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.func.ConsoleApp;
import com.synapsense.service.Environment;
import com.synapsense.service.RulesEngine;

public class RulesEngineAPITest {

	private static RulesEngine cre;

	private static String TEST_RULE = "public class TestRule implements  com.synapsense.rulesengine.core.environment.RuleAction {"
	        + " com.synapsense.rulesengine.core.environment.Property input1; \n com.synapsense.rulesengine.core.environment.Property input2;\n public Object run(com.synapsense.rulesengine.core.environment.RuleI triggeredRule, com.synapsense.rulesengine.core.environment.Property calculated) {return input1.getValue() + input2.getValue();} "
	        + " \n public void setInput1( com.synapsense.rulesengine.core.environment.Property i1) {input1 = i1;} \n public void setInput2( com.synapsense.rulesengine.core.environment.Property i2) {input2 = i2;}}";

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		ConsoleApp.init();

		cre = ConsoleApp.cre;
	}

	@Test
	public void testCreateRuleType() throws Exception {
		String name = "com.synapsense.environment.RulesEngineAPITest.TestRule";
		RuleType rt = new RuleType(name, TEST_RULE, "Groovy", false, "0 0/5 * * * ?");
		TO<?> ruleId = cre.addRuleType(rt);

		RuleType ruleType = cre.getRuleType(ruleId);

		Assert.assertEquals(name, ruleType.getName());
		Assert.assertEquals(TEST_RULE, ruleType.getSource());
		Assert.assertEquals("0 0/5 * * * ?", ruleType.getRuleSchedulerConfig());

		if (!cre.removeRuleType(ruleId)) {
			Assert.fail("Remove failed");
		}

		Assert.assertNull(cre.getRuleType(ruleId));
		Assert.assertNull(cre.getRuleTypeId(name));
	}

	@Test
	public void testUpdateRuleType() throws Exception {
		String name = "com.synapsense.environment.RulesEngineAPITest.TestRule";
		RuleType rt = new RuleType(name, TEST_RULE, "Groovy", false, "0 0/5 * * * ?");

		TO<?> rtId = cre.addRuleType(rt);

		String newSource = TEST_RULE.replaceAll("return null", "return 1.0D");
		String newConf = "0 0/5 * * *?";

		rt.setRuleSchedulerConfig(newConf);
		rt.setSource(newSource);

		cre.updateRuleType(rtId, rt);

		rt = cre.getRuleType(rtId);

		Assert.assertEquals(newSource, rt.getSource());
		Assert.assertEquals(newConf, rt.getRuleSchedulerConfig());

		if (!cre.removeRuleType(rtId)) {
			Assert.fail("Remove rule failed");
		}

		Assert.assertNull(cre.getRuleType(rtId));
		Assert.assertNull(cre.getRuleTypeId(name));
	}

	@Test
	public void testCreateRule() throws Exception {

		Environment env = ConsoleApp.env;
		String typeName = "TEST";

		ObjectType type = env.getObjectType(typeName);
		if (type != null) {
			env.deleteObjectType(typeName);
		}
		type = env.createObjectType(typeName);
		Set<PropertyDescr> propsDescr = type.getPropertyDescriptors();
		propsDescr.add(new PropertyDescr("a", Double.class));
		propsDescr.add(new PropertyDescr("b", Double.class));
		propsDescr.add(new PropertyDescr("c", Double.class));
		env.updateObjectType(type);

		TO<?> obj = env.createObject(typeName);
		env.setPropertyValue(obj, "a", 5.0);
		env.setPropertyValue(obj, "b", 2.0);

		String rtName = "com.synapsense.rulesengine.core.script.base.Sum";

		TO<?> rtTo = cre.getRuleTypeId(rtName);
		try {
			if (rtTo != null) {
				boolean removed = cre.removeRuleType(rtTo);
				if (!removed) {
					Assert.fail("Removing failed!");
				}
			}
			RuleType rt = new RuleType(rtName, TEST_RULE, "Groovy", false, "0 0/5 * * * ?");
			rtTo = cre.addRuleType(rt);

			CRERule rule = new CRERule("TestRule", rtTo, obj, "c");
			rule.addSrcProperty(obj, "a", "input1");
			rule.addSrcProperty(obj, "b", "input2");

			cre.addRule(rule);

			cre.calculateProperty(obj, "c", false);

			Double cVal = env.getPropertyValue(obj, "c", Double.class);

			Assert.assertEquals(7.0, cVal.doubleValue(), 0.001);

			env.deleteObjectType(typeName);
		} finally {
			cre.removeRuleType(rtTo);
		}
	}

	// an attempt to recreate the same problems we're having with the
	// RulesEngine in DLIS as a unit test
	@Test
	public void testCreateObjectsAndRules() throws Exception {

		int totalObjects = 50;

		Environment env = ConsoleApp.env;

		env.configurationStart();

		String typeNameParent = "TEST PARENT";
		ObjectType parentType = env.getObjectType(typeNameParent);
		if (parentType != null) {
			env.deleteObjectType(typeNameParent);
		}
		parentType = env.createObjectType(typeNameParent);
		Set<PropertyDescr> propsDescr = parentType.getPropertyDescriptors();
		propsDescr.add(new PropertyDescr("name", String.class));
		env.updateObjectType(parentType);

		String typeName = "GTEST";
		ObjectType type = env.getObjectType(typeName);
		if (type != null) {
			env.deleteObjectType(typeName);
		}

		type = env.createObjectType(typeName);
		propsDescr = type.getPropertyDescriptors();
		propsDescr.add(new PropertyDescr("a", Double.class));
		propsDescr.add(new PropertyDescr("b", Double.class));
		propsDescr.add(new PropertyDescr("c", Double.class));
		propsDescr.add(new PropertyDescr("name", String.class));
		propsDescr.add(new PropertyDescr("pointer", TO.class.getName()));

		env.updateObjectType(type);

		String rtName = "com.synapsense.rulesengine.core.script.base.Sum";
		TO<?> rtTo = cre.getRuleTypeId(rtName);
		if (rtTo != null) {
			boolean removed = cre.removeRuleType(rtTo);
			if (!removed) {
				Assert.fail("Removing failed!");
			}
		}
		RuleType rt = new RuleType(rtName, TEST_RULE, "Groovy", false, "0 0/5 * * * ?");
		rtTo = cre.addRuleType(rt);

		TO<?> parentObj = env.createObject(typeNameParent);
		env.setPropertyValue(parentObj, "name", "the parent");

		List<TO<?>> createdObjects = new ArrayList<TO<?>>();
		List<CRERule> createdRules = new ArrayList<CRERule>();

		for (int i = 0; i < totalObjects; i++) {
			// and a whole bunch of others

			List<ValueTO> props = new ArrayList<ValueTO>();
			props.add(new ValueTO("a", 5.0));
			props.add(new ValueTO("b", 2.0));
			props.add(new ValueTO("name", "objectNum" + i));

			createdObjects.add(env.createObject(typeName, props.toArray(new ValueTO[0])));

			CRERule rule = new CRERule("TestRule" + createdObjects.get(i).toString(), rtTo, createdObjects.get(i), "c");
			rule.addSrcProperty(createdObjects.get(i), "a", "input1");
			rule.addSrcProperty(createdObjects.get(i), "b", "input2");
			createdRules.add(rule);

		}

		// relations

		List<Relation> newRelations = new ArrayList<Relation>();

		List<TO<?>> roots = new ArrayList<TO<?>>(env.getObjectsByType("ROOT"));
		TO<?> rootTO = roots.get(0);
		Relation rootRelation = new Relation(rootTO, parentObj);
		newRelations.add(rootRelation);

		for (TO<?> o : createdObjects) {
			newRelations.add(new Relation(parentObj, o));
		}
		env.setRelation(newRelations);

		// pointers
		for (TO<?> o : createdObjects) {
			env.setPropertyValue(o, "pointer", parentObj);
		}

		try {

			for (CRERule rule : createdRules) {
				cre.addRule(rule);
			}

			for (TO<?> o : createdObjects) {
				cre.calculateProperty(o, "c", false);
				Double cVal = env.getPropertyValue(o, "c", Double.class);
				Assert.assertEquals(7.0, cVal.doubleValue(), 0.001);
			}

			env.deleteObjectType(typeName);
			env.deleteObjectType(typeNameParent);
		} finally {
			cre.removeRuleType(rtTo);
		}

		env.configurationComplete();
	}

	@Test
	public void testBug5232() throws Exception {
		Environment env = ConsoleApp.env;
		String typeName = "TEST";

		ObjectType type = env.getObjectType(typeName);
		if (type != null) {
			env.deleteObjectType(typeName);
		}
		type = env.createObjectType(typeName);
		Set<PropertyDescr> propsDescr = type.getPropertyDescriptors();
		propsDescr.add(new PropertyDescr("a", Double.class));
		propsDescr.add(new PropertyDescr("b", Double.class));
		propsDescr.add(new PropertyDescr("c", Double.class));
		env.updateObjectType(type);

		TO<?> obj = env.createObject(typeName);
		env.setPropertyValue(obj, "a", 5.0);
		env.setPropertyValue(obj, "b", 2.0);

		String rtName = "com.synapsense.rulesengine.core.script.base.Sum";

		TO<?> rtTo = cre.getRuleTypeId(rtName);
		try {
			if (rtTo != null) {
				boolean removed = cre.removeRuleType(rtTo);
				if (!removed) {
					Assert.fail("Removing failed!");
				}
			}
			RuleType rt = new RuleType(rtName, TEST_RULE, "Groovy", false, "0 0/5 * * * ?");
			rtTo = cre.addRuleType(rt);

			CRERule rule = new CRERule("TestRule1", rtTo, obj, "c");
			rule.addSrcProperty(obj, "a", "input1");
			rule.addSrcProperty(obj, "b", "input2");

			cre.addRule(rule);

			cre.removeRule(rule.getName());

			TO<?> existedRuleId = cre.getRuleId(rule.getName());
			if (existedRuleId != null)
				Assert.fail("The rule hasn't been removed");

			cre.addRule(rule);
			env.deleteObjectType(typeName);
		} finally {
			cre.removeRuleType(rtTo);
		}
	}

	@Test
	public void testBug5233() throws Exception {
		Environment env = ConsoleApp.env;
		String typeName = "TEST";

		ObjectType type = env.getObjectType(typeName);
		if (type != null) {
			env.deleteObjectType(typeName);
		}
		type = env.createObjectType(typeName);
		Set<PropertyDescr> propsDescr = type.getPropertyDescriptors();
		propsDescr.add(new PropertyDescr("a", Double.class));
		propsDescr.add(new PropertyDescr("b", Double.class));
		propsDescr.add(new PropertyDescr("c", Double.class));
		env.updateObjectType(type);

		TO<?> obj = env.createObject(typeName);
		env.setPropertyValue(obj, "a", 5.0);
		env.setPropertyValue(obj, "b", 2.0);

		String rtName = "com.synapsense.rulesengine.core.script.base.Sum";

		TO<?> rtTo = cre.getRuleTypeId(rtName);
		try {
			if (rtTo != null) {
				boolean removed = cre.removeRuleType(rtTo);
				if (!removed) {
					Assert.fail("Removing failed!");
				}
			}
			RuleType rt = new RuleType(rtName, TEST_RULE, "Groovy", false, "0 0/5 * * * ?");
			rtTo = cre.addRuleType(rt);

			CRERule rule = new CRERule("TestRule1", rtTo, obj, "c");
			rule.addSrcProperty(obj, "a", "input1");
			rule.addSrcProperty(obj, "b", "input2");

			cre.addRule(rule);

			Collection<CRERule> rules = cre.getRulesByObjectId(obj);
			Assert.assertEquals(1, rules.size());
			Assert.assertEquals("TestRule1", rules.iterator().next().getName());
			env.deleteObjectType(typeName);
		} finally {
			cre.removeRuleType(rtTo);
		}
	}

	@Test(expected = ObjectNotFoundException.class)
	public void testBug5234() throws Exception {
		TO<?> typeTO = TOFactory.getInstance().loadTO("RULE_TYPE:999999999");
		cre.updateRuleType(typeTO, new RuleType("TEst", null, "Java", true, "* 0/5 * * *"));
	}

	@Test(expected = ObjectExistsException.class)
	public void createSameRuleTypeFails() throws Exception {
		String name = "com.synapsense.environment.RulesEngineAPITest.TestRule";
		RuleType rt = new RuleType(name, TEST_RULE, "Groovy", false, "0 0/5 * * * ?");
		TO ruleId = cre.addRuleType(rt);
		try {
			cre.addRuleType(rt);
		} finally {
			if (!cre.removeRuleType(ruleId)) {
				Assert.fail("Remove failed");
			}
		}
	}

	@Test(expected = ObjectExistsException.class)
	public void createSameRuleFails() throws Exception {
		Environment env = ConsoleApp.env;
		String typeName = "TEST";

		ObjectType type = env.getObjectType(typeName);
		if (type != null) {
			env.deleteObjectType(typeName);
		}
		type = env.createObjectType(typeName);
        Set<PropertyDescr> propsDescr = type.getPropertyDescriptors();
        propsDescr.add(new PropertyDescr("a", Double.class));
        env.updateObjectType(type);

		TO obj = env.createObject(typeName);

		String name = "com.synapsense.environment.RulesEngineAPITest.TestRule";
		RuleType rt = new RuleType(name, TEST_RULE, "Groovy", false, "0 0/5 * * * ?");
		TO ruleId = cre.addRuleType(rt);
		CRERule rule = new CRERule("TestRule", ruleId, obj, "a");
		cre.addRule(rule);
		try {
			cre.addRule(rule);
		} finally {
			env.deleteObjectType(typeName);
			if (!cre.removeRuleType(ruleId)) {
				Assert.fail("Remove failed");
			}
		}
	}
}
