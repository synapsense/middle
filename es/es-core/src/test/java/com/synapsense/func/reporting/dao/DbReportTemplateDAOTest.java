package com.synapsense.func.reporting.dao;

import static com.synapsense.impl.reporting.TestUtils.EMPTY_REPORT_DESIGN_PATH;
import static com.synapsense.impl.reporting.TestUtils.FILE_STORAGE;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Collection;
import java.util.Iterator;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.synapsense.exception.ObjectExistsException;
import com.synapsense.impl.reporting.TestUtils;
import com.synapsense.impl.reporting.dao.DbESDAOFactory;
import com.synapsense.impl.reporting.dao.ReportTemplateDAO;
import com.synapsense.impl.reporting.dao.ReportingDAOFactory;
import com.synapsense.impl.reporting.dao.ResourceDAOReportingException;
import com.synapsense.impl.reporting.model.ReportTemplate;
import com.synapsense.service.reporting.dto.ReportTemplateInfo;
import com.synapsense.service.reporting.exception.CompilationReportingException;
import com.synapsense.service.reporting.exception.DAOReportingException;
import com.synapsense.service.reporting.management.ReportParameter;

public class DbReportTemplateDAOTest {

	private static ReportingDAOFactory daoFactory;
	
	@BeforeClass
	public static void setUp() throws DAOReportingException {
		daoFactory = new DbESDAOFactory(TestUtils.FILE_STORAGE, TestUtils.getEnvironment(), TestUtils.getFileWriter());

	}

	@Test
	public void testCreateDAO() throws DAOReportingException {
		ReportTemplateDAO rtDao = daoFactory.newReportTemplateDAO();
		Assert.assertNotNull("Report template DAO object is null", rtDao);
	}

	@Test(expected = ResourceDAOReportingException.class)
	public void testCreateReportTemplate() throws DAOReportingException, ObjectExistsException {
		ReportTemplateDAO rtDAO = daoFactory.newReportTemplateDAO();
		ReportTemplateInfo reportTemplate = new ReportTemplateInfo(TestUtils.EMPTY_REPORT_NAME);
		rtDAO.createReportTemplate(reportTemplate, TestUtils.readTestFile(FILE_STORAGE + EMPTY_REPORT_DESIGN_PATH));
		try {

			Assert.assertFalse("Report template id", reportTemplate.getReportTemplateId() == 0);

			reportTemplate = rtDAO.getReportTemplateInfo(TestUtils.EMPTY_REPORT_NAME);

			Assert.assertNotNull("Report template object is null", reportTemplate);

			Assert.assertEquals("Report template name is incorrect", TestUtils.EMPTY_REPORT_NAME,
			        reportTemplate.getReportTemplateName());

		} catch (DAOReportingException e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		} finally {
			rtDAO.removeReportTemplate(reportTemplate.getReportTemplateName());
		}

		rtDAO.getReportTemplateInfo(TestUtils.EMPTY_REPORT_NAME);
	}

	@Test(expected = ResourceDAOReportingException.class)
	public void testCreateReportTemplateCheckParameters() throws DAOReportingException, CompilationReportingException,
	        FileNotFoundException, ObjectExistsException {
		ReportTemplateDAO rtDAO = daoFactory.newReportTemplateDAO();
		try {

			ReportTemplate rt = new ReportTemplate(new FileInputStream(FILE_STORAGE + EMPTY_REPORT_DESIGN_PATH));

			ReportTemplateInfo rtInfo = new ReportTemplateInfo(TestUtils.EMPTY_REPORT_NAME, rt.getParameters());

			rtDAO.createReportTemplate(rtInfo, TestUtils.readTestFile(FILE_STORAGE + EMPTY_REPORT_DESIGN_PATH));

			ReportTemplateInfo reportTemplate = rtDAO.getReportTemplateInfo(TestUtils.EMPTY_REPORT_NAME);

			Assert.assertNotNull("Report template object is null", reportTemplate);

			Assert.assertEquals("Report template name is incorrect", TestUtils.EMPTY_REPORT_NAME,
			        reportTemplate.getReportTemplateName());

			Collection<ReportParameter> parameters = reportTemplate.getParameters();
			Assert.assertEquals("Paramters number", 2, parameters.size());

			Iterator<ReportParameter> iter = parameters.iterator();
			ReportParameter stringParameter = iter.next();
			Assert.assertEquals("String parameter type", String.class, stringParameter.getType());
			Assert.assertEquals("String parameter name", "StringParameter", stringParameter.getName());

			ReportParameter integerParameter = iter.next();
			Assert.assertEquals("Integer parameter type", Integer.class, integerParameter.getType());
			Assert.assertEquals("Integer parameter name", "IntegerParameter", integerParameter.getName());

		} catch (DAOReportingException e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		} finally {
			rtDAO.removeReportTemplate(TestUtils.EMPTY_REPORT_NAME);
		}

		rtDAO.getReportTemplateInfo(TestUtils.EMPTY_REPORT_NAME);
	}

	@Test(expected = ObjectExistsException.class)
	public void testDuplicateReportTemplate() throws DAOReportingException, ObjectExistsException {
		ReportTemplateDAO rtDAO = daoFactory.newReportTemplateDAO();

		try {
			rtDAO.createReportTemplate(new ReportTemplateInfo("Test"), "");
			rtDAO.createReportTemplate(new ReportTemplateInfo("Test"), "");
		} finally {
			rtDAO.removeReportTemplate("Test");
		}
	}

	@Test
	public void testGetTemplateDesign() throws DAOReportingException, CompilationReportingException, FileNotFoundException, ObjectExistsException {
		ReportTemplateDAO rtDAO = daoFactory.newReportTemplateDAO();

		ReportTemplate rt = new ReportTemplate(new FileInputStream(FILE_STORAGE + EMPTY_REPORT_DESIGN_PATH));

		String design = TestUtils.readTestFile(FILE_STORAGE + EMPTY_REPORT_DESIGN_PATH);

		try {
			rtDAO.createReportTemplate(new ReportTemplateInfo(rt.getName(), rt.getParameters()), design);

			Assert.assertEquals("Report template design by name", design, rtDAO.getReportTemplateDesign(rt.getName()));

		} finally {
			rtDAO.removeReportTemplate(rt.getName());
		}
	}

	@Test
	public void testGetReportTemplates() throws DAOReportingException, ObjectExistsException {
		ReportTemplateDAO rtDAO = daoFactory.newReportTemplateDAO();
		
		rtDAO.createReportTemplate(new ReportTemplateInfo("Test1"), "");
		rtDAO.createReportTemplate(new ReportTemplateInfo("Test2"), "");
		rtDAO.createReportTemplate(new ReportTemplateInfo("Test3"), "");

		Collection<String> rts = rtDAO.getReportTemplates();

		Assert.assertTrue("Templates contains Test1", rts.contains("Test1"));
		Assert.assertTrue("Templates contains Test2", rts.contains("Test2"));
		Assert.assertTrue("Templates contains Test3", rts.contains("Test3"));

		rtDAO.removeReportTemplate("Test1");
		rtDAO.removeReportTemplate("Test2");
		rtDAO.removeReportTemplate("Test3");
	}

	@Test
	public void testUpdateReportTemplate() throws DAOReportingException, ObjectExistsException {

		ReportTemplateDAO rtDAO = daoFactory.newReportTemplateDAO();
		ReportTemplateInfo rtInfo = new ReportTemplateInfo("Test1");
		rtInfo.addParameter(new ReportParameter("testParam1", String.class));

		rtDAO.createReportTemplate(rtInfo, "design1");

		// change design and parameters
		rtInfo = rtDAO.getReportTemplateInfo("Test1");

		rtInfo.clearParameters();
		ReportParameter param = new ReportParameter("testParam2", Double.class);
		rtInfo.addParameter(param);

		rtDAO.updateReportTemplate(rtInfo, "design2");

		rtInfo = rtDAO.getReportTemplateInfo("Test1");

		Assert.assertEquals("Parameters size", 1, rtInfo.getParameters().size());
		Assert.assertEquals("Parameter", param, rtInfo.getParameters().iterator().next());

		Assert.assertEquals("New design", "design2", rtDAO.getReportTemplateDesign("Test1"));

		// change name
		rtInfo.setReportTemplateName("Test2");
		rtDAO.updateReportTemplate(rtInfo, "design2");

		rtInfo = rtDAO.getReportTemplateInfo("Test2");
		Assert.assertEquals("Parameters size", 1, rtInfo.getParameters().size());
		Assert.assertEquals("Parameter", param, rtInfo.getParameters().iterator().next());

		Assert.assertEquals("New design", "design2", rtDAO.getReportTemplateDesign("Test2"));

		try {
			rtDAO.getReportTemplateInfo("Test1");
			Assert.fail("Report Test1 is stored");
		} catch (ResourceDAOReportingException e) {
			// normal execution
		}

		rtDAO.removeReportTemplate("Test2");
	}

	@AfterClass
	public static void tearDownAfterClass() throws DAOReportingException {
		daoFactory.shutdown();
	}
}
