package com.synapsense.func.reporting.dao;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ DbReportTemplateDAOTest.class, DbReportDAOTest.class })
public class DAOTestSuite {

}
