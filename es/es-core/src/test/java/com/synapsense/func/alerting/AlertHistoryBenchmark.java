package com.synapsense.func.alerting;

import static org.junit.Assert.assertTrue;

import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.dbunit.JdbcDatabaseTester;
import org.dbunit.dataset.IDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.synapsense.DbConstants;
import com.synapsense.dto.Alert;
import com.synapsense.dto.EnvObject;
import com.synapsense.dto.EnvObjectBundle;
import com.synapsense.dto.Relation;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOBundle;
import com.synapsense.func.ConsoleApp;
import com.synapsense.service.AlertService;
import com.synapsense.service.Environment;
import com.synapsense.util.CollectionUtils;

public class AlertHistoryBenchmark {
	private static AlertService ALERTING_SERVICE;
	private static Environment ENVIRONMENT;
	private static String TEST_TYPE = "TEST_TYPE";
	private static String TEST_ALERT_TYPE = "TEST_ALERT_TYPE";
	private static List<TO<?>> objects = new LinkedList<TO<?>>();
	private static TO<?> ROOT;
	private static int OBJECTS_NUM = 100;
	private static JdbcDatabaseTester databaseTester;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		databaseTester = new JdbcDatabaseTester(DbConstants.DB_DRIVER, DbConstants.DB_CONNECTION, DbConstants.DB_USER,
		        DbConstants.DB_PASSWORD);
		IDataSet databaseDataSet = databaseTester.getConnection().createDataSet(
		        new String[] { DbConstants.TBL_HISTORICAL_VALUES });
		DatabaseOperation.TRUNCATE_TABLE.execute(databaseTester.getConnection(), databaseDataSet);

		ConsoleApp.init();

		ENVIRONMENT = ConsoleApp.env;

		ALERTING_SERVICE = ConsoleApp.ALERTING_SERVICE;

		ENVIRONMENT.createObjectType(TEST_TYPE);

		ROOT = ENVIRONMENT.createObject(TEST_TYPE);

		EnvObjectBundle bundle = new EnvObjectBundle();
		for (int i = 0; i < OBJECTS_NUM; i++) {
			bundle.addObject(Integer.toString(i), new EnvObject(TEST_TYPE));
		}

		TOBundle tos = ENVIRONMENT.createObjects(bundle);
		List<Relation> relations = CollectionUtils.newList();
		for (int i = 1; i <= OBJECTS_NUM; i++) {
			relations.add(new Relation(ROOT, tos.get(i)));
			objects.add(tos.get(i));
		}
		ENVIRONMENT.setRelation(relations);

		Alert alert = new Alert("alert_error", TEST_ALERT_TYPE, new Date(), "problem", null);

		Iterator<TO<?>> iterator = objects.iterator();
		long start = System.currentTimeMillis();
		while (iterator.hasNext()) {
			TO<?> id = iterator.next();
			alert.setHostTO(id);
			String newname = id.getID().toString();
			if (newname.length() > 126) {
				assertTrue(newname, false);
			}
			alert.setName(newname);
			ALERTING_SERVICE.raiseAlert(alert);
		}

		System.err.println(objects.size() + " alerts initital generation time : "
		        + (System.currentTimeMillis() - start));
	}

	@Test(timeout = 1000)
	public void getHistoryPerformance() {
		Date till = new Date();
		Date from = new Date();
		List<Alert> alerts = CollectionUtils.newList();
		for (TO<?> object : objects) {
			alerts.addAll(ALERTING_SERVICE.getAlertsHistory(object, from, till, true));
		}
	}

	@Test(timeout = 200)
	public void getHistoryPerformanceRootHierarchical() {
		Date till = new Date();
		Date from = new Date();
		List<Alert> alerts = CollectionUtils.newList();
		alerts.addAll(ALERTING_SERVICE.getAlertsHistory(ROOT, from, till, true));
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		ALERTING_SERVICE.deleteAllAlerts();
		ENVIRONMENT.deleteObjectType(TEST_TYPE);
		IDataSet databaseDataSet = databaseTester.getConnection().createDataSet(
		        new String[] { DbConstants.TBL_HISTORICAL_VALUES });
		DatabaseOperation.TRUNCATE_TABLE.execute(databaseTester.getConnection(), databaseDataSet);
	}

}
