package com.synapsense.func.environment;

import static org.junit.Assert.assertEquals;

import java.util.Collection;
import java.util.Date;

import org.dbunit.Assertion;
import org.dbunit.JdbcDatabaseTester;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.operation.DatabaseOperation;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.synapsense.DbConstants;
import com.synapsense.dto.ObjectType;
import com.synapsense.dto.PropertyDescr;
import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;
import com.synapsense.func.ConsoleApp;
import com.synapsense.service.Environment;

public class HistoricalDataDbTest {


	private static final int INSERT_DELAY = 200;
	private final int HISTORY_SIZE = 10;

	private static JdbcDatabaseTester databaseTester;
	private static Environment env;

	public static void waitForDbBatchUpdater(final long time) {
		try {
			Thread.sleep(time);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	protected static IDatabaseConnection getConnection() throws Exception {
		return databaseTester.getConnection();
	}

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		ConsoleApp.init();

		env = ConsoleApp.env;

		databaseTester = new JdbcDatabaseTester(DbConstants.DB_DRIVER, DbConstants.DB_CONNECTION, DbConstants.DB_USER,
		        DbConstants.DB_PASSWORD);
	}

	@AfterClass
	public static void teardownAfterClass() throws Exception {
		IDataSet databaseDataSet = getConnection().createDataSet(new String[] { DbConstants.TBL_HISTORICAL_VALUES });
		DatabaseOperation.TRUNCATE_TABLE.execute(getConnection(), databaseDataSet);
	}

	@Test
	public void testGenericHistoricalDataAutomaticRemovalDisabled() throws Exception {
		String TYPE_TEST = "Type_tested";
		try {
			ObjectType t1 = env.createObjectType(TYPE_TEST);

			PropertyDescr doubleProp = new PropertyDescr("double", Double.class.getName(), true);
			t1.getPropertyDescriptors().add(doubleProp);

			env.updateObjectType(t1);

			TO oid = env.createObject(TYPE_TEST, new ValueTO[] { new ValueTO(doubleProp.getName(), 0.0) });

			Date start = new Date();

			for (int i = 0; i < HISTORY_SIZE; i++) {
				env.setPropertyValue(oid, doubleProp.getName(), i + 0.1);
				Thread.sleep(INSERT_DELAY);
			}
			waitForDbBatchUpdater(3000);

			Date end = new Date();
			// all methods must return specified history size
			int doubleHistory = env.getHistorySize(oid, doubleProp.getName());
			assertEquals(HISTORY_SIZE, doubleHistory);

			Collection<ValueTO> doubleHistoryPoints = env.getHistory(oid, doubleProp.getName(), Double.class);
			assertEquals(HISTORY_SIZE, doubleHistoryPoints.size());

			doubleHistoryPoints = env.getHistory(oid, doubleProp.getName(), start, end, Double.class);
			assertEquals(HISTORY_SIZE, doubleHistoryPoints.size());

			doubleHistoryPoints = env.getHistory(oid, doubleProp.getName(), start, 100, Double.class);
			assertEquals(HISTORY_SIZE, doubleHistoryPoints.size());

			// retrieves historical_values table content before object deletion
			IDataSet databaseDataSet = getConnection().createDataSet();
			ITable actualTable = databaseDataSet.getTable(DbConstants.TBL_HISTORICAL_VALUES);

			env.deleteObject(oid);

			// retrieves historical_values table content rigth after object
			// deletion
			IDataSet expectedDataSet = getConnection().createDataSet();
			ITable expectedTable = expectedDataSet.getTable(DbConstants.TBL_HISTORICAL_VALUES);

			// Assert actual database table match expected table
			Assertion.assertEquals(expectedTable, actualTable);

		} finally {
			env.deleteObjectType(TYPE_TEST);
		}
	}

}
