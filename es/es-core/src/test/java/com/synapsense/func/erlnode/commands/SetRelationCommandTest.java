package com.synapsense.func.erlnode.commands;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.ericsson.otp.erlang.OtpErlangAtom;
import com.ericsson.otp.erlang.OtpErlangObject;
import com.ericsson.otp.erlang.OtpErlangString;
import com.ericsson.otp.erlang.OtpErlangTuple;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.impl.erlnode.ExceptionTypes;
import com.synapsense.impl.erlnode.commands.SetRelationCommand;
import com.synapsense.impl.erlnode.utils.ToConverter;
import com.synapsense.service.Environment;

public class SetRelationCommandTest {
	static UnitTestHelper utHelper = null;
	static String testType1 = "testType1";
	static String testType2 = "testType2";
	static Map<String, Object> envMap;
	static Environment env;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		utHelper = new UnitTestHelper();
		utHelper.initializeEnvironment();
		env = utHelper.getEnvironment();
		envMap = new HashMap<String, Object>();
		envMap.put("env", env);

	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		utHelper.removeTypeCompletly(testType1);
		utHelper.removeTypeCompletly(testType2);
		utHelper.dispose();
	}

	@Before
	public void setUp() throws Exception {
		env.deleteObjectType(testType1);
		env.deleteObjectType(testType2);
		env.createObjectType(testType1);
		env.createObjectType(testType2);
	}

	@After
	public void tearDown() throws Exception {
		env.deleteObjectType(testType1);
		env.deleteObjectType(testType2);
	}

	@Test
	public void SuccessExistsTest() throws Exception {

		TO<?> parent = env.createObject(testType1);
		TO<?> child = env.createObject(testType2);

		SetRelationCommand command = new SetRelationCommand(envMap);
		OtpErlangAtom obj = (OtpErlangAtom) command.run(new OtpErlangTuple(new OtpErlangObject[] {
		        new OtpErlangString("SetRelationCommand"), ToConverter.serializeTo(parent),
		        ToConverter.serializeTo(child) }));

		assertEquals("ok", obj.toString());
		assertEquals(1, env.getChildren(parent).size());
		assertEquals(child, env.getChildren(parent).toArray()[0]);
		env.removeRelation(parent, child);
		env.deleteObject(parent);
		env.deleteObject(child);

	}

	@Test
	public void FailExistsTest() throws Exception {

		TO<?> parent = env.createObject(testType1);
		TO<?> child = env.createObject(testType1);

		SetRelationCommand command = new SetRelationCommand(envMap);
		OtpErlangTuple obj = (OtpErlangTuple) command.run(new OtpErlangTuple(new OtpErlangObject[] {
		        new OtpErlangString("SetRelationCommand"), ToConverter.serializeTo(parent),
		        ToConverter.serializeTo(parent) }));
		assertEquals("exception", obj.elementAt(0).toString());
		assertEquals(ExceptionTypes.UNSUPPORTED_RELATION.getExceptionType(),
		        ((OtpErlangString) obj.elementAt(1)).stringValue());

		command = new SetRelationCommand(envMap);
		TO<?> to = TOFactory.getInstance().loadTO("NonExistingType:46533");
		OtpErlangTuple obj2 = (OtpErlangTuple) command.run(new OtpErlangTuple(
		        new OtpErlangObject[] { new OtpErlangString("SetRelationCommand"), ToConverter.serializeTo(to),
		                ToConverter.serializeTo(child) }));
		assertEquals("exception", obj2.elementAt(0).toString());
		assertEquals(ExceptionTypes.OBJECT_NOT_FOUND.getExceptionType(),
		        ((OtpErlangString) obj2.elementAt(1)).stringValue());
		env.deleteObject(parent);
		env.deleteObject(child);

	}

	@Test
	public void InvalidParametersTest() throws Exception {
		SetRelationCommand command = new SetRelationCommand(envMap);
		OtpErlangTuple obj = (OtpErlangTuple) command.run(new OtpErlangTuple(
		        new OtpErlangObject[] { new OtpErlangString("SetRelationCommand"), }));
		assertEquals("exception", obj.elementAt(0).toString());
		assertEquals(ExceptionTypes.ILLEGAL_ARGUMENTS.getExceptionType(),
		        ((OtpErlangString) obj.elementAt(1)).stringValue());
	}
}
