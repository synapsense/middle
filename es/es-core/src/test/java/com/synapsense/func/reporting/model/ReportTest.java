package com.synapsense.func.reporting.model;

import static com.synapsense.impl.reporting.TestUtils.EMPTY_REPORT_NAME;
import static com.synapsense.impl.reporting.TestUtils.EMPTY_REPORT_TMP_PATH;
import static com.synapsense.impl.reporting.TestUtils.checkAndRemoveTestFiles;
import static com.synapsense.impl.reporting.TestUtils.getEmptyReportDesignPath;
import static com.synapsense.impl.reporting.TestUtils.getFileWriter;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.synapsense.impl.reporting.export.ExportFormat;
import com.synapsense.impl.reporting.model.Report;
import com.synapsense.impl.reporting.model.ReportTemplate;
import com.synapsense.impl.reporting.utils.FileUtils;
import com.synapsense.service.reporting.dto.ReportTaskInfo;
import com.synapsense.service.reporting.management.ReportingException;

public class ReportTest {
	
	private static String reportingDir = System.getenv().get("JBOSS_HOME") + "\\standalone\\configuration";
	private Report testedReport;

	@Before
	public void setUp() {
		try {
			ReportTemplate template = new ReportTemplate(new FileInputStream(getEmptyReportDesignPath()));
			ReportTaskInfo taskInfo = ReportTaskInfo.newInstance(EMPTY_REPORT_NAME, "Test");
			testedReport = template.generateReport(taskInfo);
			FileUtils.checkOrCreateDirectory(reportingDir + "\\" + EMPTY_REPORT_TMP_PATH + "\\export\\");
		} catch (FileNotFoundException e) {
			Assert.fail(e.getMessage());
		} catch (ReportingException e) {
			Assert.fail(e.getMessage());
		}
	}

	@Test
	public void testSuccessfulReportExportToFile() {
		try {

			String exportPath = EMPTY_REPORT_TMP_PATH + "\\export\\" + EMPTY_REPORT_NAME + ".";
			testedReport.export(ExportFormat.PDF, exportPath + ExportFormat.PDF.getFileExtension(), getFileWriter());
			String fullPath = reportingDir + "\\" + exportPath + ExportFormat.PDF.getFileExtension();
			File exportedFile = new File(fullPath);
			Assert.assertTrue(exportedFile.isFile());
			
			fullPath = reportingDir + "\\" + exportPath + ExportFormat.HTML.getFileExtension();
			testedReport.export(ExportFormat.HTML, exportPath + ExportFormat.HTML.getFileExtension(), getFileWriter());
			exportedFile = new File(fullPath);
			Assert.assertTrue(exportedFile.isFile());
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}

	@After
	public void tearDown() {
		checkAndRemoveTestFiles(new String[] {  reportingDir + "\\" + EMPTY_REPORT_TMP_PATH });
	}
}
