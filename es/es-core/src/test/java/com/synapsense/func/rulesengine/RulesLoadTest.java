package com.synapsense.func.rulesengine;

import static org.junit.Assert.assertEquals;

import java.util.Set;

import org.junit.BeforeClass;
import org.junit.Test;

import com.synapsense.dto.CRERule;
import com.synapsense.dto.ObjectType;
import com.synapsense.dto.PropertyDescr;
import com.synapsense.dto.RuleType;
import com.synapsense.dto.TO;
import com.synapsense.func.ConsoleApp;
import com.synapsense.service.Environment;
import com.synapsense.service.RulesEngine;

public class RulesLoadTest {

	private static Environment env;
	private static RulesEngine cre;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		ConsoleApp.init();
		env = ConsoleApp.env;
		cre = ConsoleApp.cre;
	}

	private static final String RULE_BEGIN = "import com.synapsense.service.impl.dao.to.PropertyTO \n"
	        + "import com.synapsense.rulesengine.core.environment.* \n"
	        + "import com.synapsense.dto.TO; \n"
	        + "import com.synapsense.service.Environment; \n\n"
	        + "class Generic2InputRule implements RuleAction { \n"
	        + "	private Property input1 \n"
	        + "	private Property input2 \n"
	        +

	        "	public Object run(RuleI triggeredRule, Property calculated) { \n"
	        + "		Environment env = calculated.getEnvironment() \n"
	        + "		println triggeredRule.getName() + \" \" + calculated.getDataSource().getHostObjectTO().getID() + \" \" + input1.getDataSource().getHostObjectTO().getID() + \" \" + input2.getDataSource().getHostObjectTO().getID()\n"
	        + " 		println triggeredRule.getName() + \" \" + input1.getValue() + \" \" + input2.getValue() + \"\\n\"\n";

	private static final String RULE_END = " \n" + "} \n\n" + "private double extractValue(env, val) { \n"
	        + "	if(val instanceof Double) \n" + "		return val.doubleValue() \n" + "	if(val instanceof PropertyTO) \n"
	        + "		return (double)env.getPropertyValue(val.getObjId(), val.getPropertyName(), Integer.class) \n"
	        + "	println \"Cannot extract value from '\" + val + \"'\" \n" + "	return null \n" + "} \n\n"
	        + "public Property getInput1() { \n" + "	return input1 \n" + "} \n\n"
	        + "public void setInput1(Property i) { \n" + "	input1 = i \n" + "} \n\n"
	        + "public Property getInput2() { \n" + "	return input2 \n" + "} \n\n"
	        + "public void setInput2(Property i) { \n" + "	input2 = i \n" + "} \n" + "} \n";

	private static final String GROOVY_TEST_OBJ = "TESTOBJ";
	private static final String GROOVY_RULE_NAME = "GroovyTestRule";

	@Test
	public void testUpdateGroovyRuleCode() throws Exception {
		ObjectType otype = env.getObjectType(GROOVY_TEST_OBJ);
		if (otype != null) {
			env.deleteObjectType(GROOVY_TEST_OBJ);
		}

		ObjectType objType = env.createObjectType(GROOVY_TEST_OBJ);
		Set<PropertyDescr> objPds = objType.getPropertyDescriptors();

		objPds.add(new PropertyDescr("a", Double.class.getName()));
		objPds.add(new PropertyDescr("b", Double.class.getName()));
		objPds.add(new PropertyDescr("c", Double.class.getName()));
		objPds.add(new PropertyDescr("d", Double.class.getName()));
		objPds.add(new PropertyDescr("e", Double.class.getName()));
		objPds.add(new PropertyDescr("f", Double.class.getName()));

		env.updateObjectType(objType);

		// Create a rule type from Groovy code
		TO<?> rule = cre.getRuleTypeId(GROOVY_RULE_NAME);
		if (rule != null) {
			cre.removeRuleType(rule);
		}
		RuleType ruleType = new RuleType(GROOVY_RULE_NAME, RULE_BEGIN
		        + "return extractValue(env, input1.getValue()) + extractValue(env, input2.getValue())" + RULE_END,
		        "Groovy", false);
		rule = cre.addRuleType(ruleType);

		TO<?> object1;
		TO<?> object2;

		// Now create some instances
		object1 = env.createObject(GROOVY_TEST_OBJ);
		env.setPropertyValue(object1, "a", 10.0);
		env.setPropertyValue(object1, "b", 10.0);
		env.setPropertyValue(object1, "c", 10.0);

		object2 = env.createObject(GROOVY_TEST_OBJ);
		env.setPropertyValue(object2, "a", 10.0);
		env.setPropertyValue(object2, "b", 10.0);
		env.setPropertyValue(object2, "c", 10.0);

		String ruleId = GROOVY_RULE_NAME + object1.getID();
		CRERule ruleInst = new CRERule(ruleId, rule, object1, "a");
		ruleInst.addSrcProperty(object2, "b", "input1");
		ruleInst.addSrcProperty(object2, "c", "input2");
		cre.addRule(ruleInst);

		cre.calculateProperty(object1, "a", false);
		assertEquals(Double.valueOf(20.0), env.getPropertyValue(object1, "a", Double.class));

		// update source
		ruleType.setSource(RULE_BEGIN
		        + "return extractValue(env, input1.getValue()) * extractValue(env, input2.getValue())" + RULE_END);
		cre.updateRuleType(rule, ruleType);

		env.setPropertyValue(object2, "b", 10.0);
		env.setPropertyValue(object2, "c", 10.0);
		cre.calculateProperty(object1, "a", false);
		assertEquals(Double.valueOf(100.0), env.getPropertyValue(object1, "a", Double.class));

		env.deleteObjectType(GROOVY_TEST_OBJ);
	}
}
