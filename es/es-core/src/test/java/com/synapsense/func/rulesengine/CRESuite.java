package com.synapsense.func.rulesengine;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ RulesLoadTest.class, RuleServiceTest.class, RulesEngineAPITest.class, RulesCyclesTest.class,
        RulesCollisionTest.class, RuleBehaviorTest.class })
public class CRESuite {
}
