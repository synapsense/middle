package com.synapsense.func.rulesengine;

import junit.framework.Assert;

import org.junit.BeforeClass;
import org.junit.Test;

import com.synapsense.dto.CRERule;
import com.synapsense.dto.ObjectType;
import com.synapsense.dto.PropertyDescr;
import com.synapsense.dto.RuleType;
import com.synapsense.dto.TO;
import com.synapsense.func.ConsoleApp;
import com.synapsense.service.Environment;
import com.synapsense.service.RulesEngine;

public class RulesLinkUpdateTest {

	private static RulesEngine cre;
	private static Environment env;

	private static final String TYPE_OUT = "OUT";
	private static final String TYPE_IN = "IN";

	private static final String TEST_RULE = "import com.synapsense.service.Environment; public class TestRule implements  com.synapsense.rulesengine.core.environment.RuleAction {"
	        + " com.synapsense.rulesengine.core.environment.Property input1; \n public Object run(com.synapsense.rulesengine.core.environment.RuleI triggeredRule, com.synapsense.rulesengine.core.environment.Property calculated) {"
	        + "        Environment env = calculated.getEnvironment(); if (input1.getValue() == null) return null; return env.getPropertyValue(input1.getValue(), \"lastValue\", Double.class);} "
	        + " \n public void setInput1( com.synapsense.rulesengine.core.environment.Property i1) {input1 = i1;} \n }";

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		ConsoleApp.init();

		cre = ConsoleApp.cre;
		env = ConsoleApp.env;
	}

	@Test
	public void testLinkUpdate() throws Exception {
		String name = "com.synapsense.environment.RulesEngineAPITest.TestRule";
		TO<?> ruleId = cre.getRuleTypeId(name);
		if (ruleId != null) {
			cre.removeRuleType(ruleId);
		}

		RuleType rt = new RuleType(name, TEST_RULE, "Groovy", false, "0 0/5 * * * ?");
		ruleId = cre.addRuleType(rt);

		ObjectType type = env.createObjectType(TYPE_IN);
		ObjectType typeOut = env.createObjectType(TYPE_OUT);
		try {
			type.getPropertyDescriptors().add(new PropertyDescr("lastValue", Double.class));
			env.updateObjectType(type);

			typeOut.getPropertyDescriptors().add(new PropertyDescr("input", TO.class));
			typeOut.getPropertyDescriptors().add(new PropertyDescr("lastValue", Double.class));
			env.updateObjectType(typeOut);

			TO<?> outObj = env.createObject(TYPE_OUT);
			TO<?> inObj1 = env.createObject(TYPE_IN);
			env.setPropertyValue(outObj, "input", inObj1);

			TO<?> inObj2 = env.createObject(TYPE_IN);

			CRERule rule = new CRERule("test", ruleId, outObj, "lastValue");
			rule.addSrcProperty(outObj, "input", "input1");
			cre.addRule(rule);

			env.setPropertyValue(inObj1, "lastValue", 1.0D);
			Assert.assertEquals(1.0, env.getPropertyValue(outObj, "lastValue", Double.class), 0);

			// To give Notification service some time
			Thread.sleep(100);
			env.setPropertyValue(inObj2, "lastValue", 15.0D);
			env.setPropertyValue(outObj, "input", inObj2);
			Assert.assertEquals(15.0, env.getPropertyValue(outObj, "lastValue", Double.class), 0);

		} finally {
			env.deleteObjectType(TYPE_IN);
			env.deleteObjectType(TYPE_OUT);
			cre.removeRuleType(ruleId);
		}

	}

	@Test
	public void testLinkUpdateFromNull() throws Exception {
		String name = "com.synapsense.environment.RulesEngineAPITest.TestRule";
		TO<?> ruleId = cre.getRuleTypeId(name);
		if (ruleId != null) {
			cre.removeRuleType(ruleId);
		}
		RuleType rt = new RuleType(name, TEST_RULE, "Groovy", false, "0 0/5 * * * ?");
		ruleId = cre.addRuleType(rt);

		ObjectType type = env.createObjectType(TYPE_IN);
		ObjectType typeOut = env.createObjectType(TYPE_OUT);
		try {
			type.getPropertyDescriptors().add(new PropertyDescr("lastValue", Double.class));
			env.updateObjectType(type);

			typeOut.getPropertyDescriptors().add(new PropertyDescr("input", TO.class));
			typeOut.getPropertyDescriptors().add(new PropertyDescr("lastValue", Double.class));
			env.updateObjectType(typeOut);

			TO<?> outObj = env.createObject(TYPE_OUT);
			TO<?> inObj1 = env.createObject(TYPE_IN);

			CRERule rule = new CRERule("test", ruleId, outObj, "lastValue");
			rule.addSrcProperty(outObj, "input", "input1");
			cre.addRule(rule);

			Assert.assertNull(env.getPropertyValue(outObj, "lastValue", Double.class));

			// To give Notification service some time
			Thread.sleep(100);
			env.setPropertyValue(inObj1, "lastValue", 1.0D);
			env.setPropertyValue(outObj, "input", inObj1);
			Assert.assertEquals(1.0, env.getPropertyValue(outObj, "lastValue", Double.class), 0);
		} finally {
			env.deleteObjectType(TYPE_IN);
			env.deleteObjectType(TYPE_OUT);
			cre.removeRuleType(ruleId);
		}
	}

	@Test
	public void testLinkUpdateValue() throws Exception {
		String name = "com.synapsense.environment.RulesEngineAPITest.TestRule";
		TO<?> ruleId = cre.getRuleTypeId(name);
		if (ruleId != null) {
			cre.removeRuleType(ruleId);
		}
		RuleType rt = new RuleType(name, TEST_RULE, "Groovy", false, "0 0/5 * * * ?");
		ruleId = cre.addRuleType(rt);

		ObjectType type = env.createObjectType(TYPE_IN);
		ObjectType typeOut = env.createObjectType(TYPE_OUT);
		try {
			type.getPropertyDescriptors().add(new PropertyDescr("lastValue", Double.class));
			env.updateObjectType(type);

			typeOut.getPropertyDescriptors().add(new PropertyDescr("input", TO.class));
			typeOut.getPropertyDescriptors().add(new PropertyDescr("value", Double.class));
			env.updateObjectType(typeOut);

			TO<?> outObj = env.createObject(TYPE_OUT);
			TO<?> inObj1 = env.createObject(TYPE_IN);

			env.setPropertyValue(inObj1, "lastValue", 1.0D);
			env.setPropertyValue(outObj, "input", inObj1);

			CRERule rule = new CRERule("test", ruleId, outObj, "value");
			rule.addSrcProperty(outObj, "input", "input1");
			cre.addRule(rule);

			Assert.assertEquals(1.0, env.getPropertyValue(outObj, "value", Double.class), 0);

			env.setPropertyValue(inObj1, "lastValue", 15.0D);
			// To give Notification service some time
			Thread.sleep(100);
			Assert.assertEquals(15.0, env.getPropertyValue(outObj, "value", Double.class), 0);
		} finally {
			env.deleteObjectType(TYPE_IN);
			env.deleteObjectType(TYPE_OUT);
			cre.removeRuleType(ruleId);
		}
	}
}
