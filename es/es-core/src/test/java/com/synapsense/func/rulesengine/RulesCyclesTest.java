package com.synapsense.func.rulesengine;

import java.util.Collection;
import java.util.Set;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.synapsense.dto.Alert;
import com.synapsense.dto.CRERule;
import com.synapsense.dto.ObjectType;
import com.synapsense.dto.PropertyDescr;
import com.synapsense.dto.RuleType;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.func.ConsoleApp;
import com.synapsense.service.Environment;
import com.synapsense.service.RulesEngine;

public class RulesCyclesTest {

	private static Environment env;
	private static RulesEngine cre;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		ConsoleApp.init();
		env = ConsoleApp.env;
		cre = ConsoleApp.cre;
	}

	private TO<?> topRuleType;
	private TO<?> topObj;

	@Before
	public void setUp() throws Exception {
		// /// RULE BODY ///////////////////////
		String ruleBody = "import com.synapsense.service.impl.dao.to.PropertyTO \n"
		        + "import com.synapsense.rulesengine.core.environment.* \n"
		        + "import com.synapsense.dto.TO; \n"
		        + "import com.synapsense.service.Environment; \n\n"
		        + "class CycleTestRule implements RuleAction { \n"
		        + " private Property input;\n"
		        + "	public Object run(RuleI triggeredRule, Property calculated) { \n"
		        + "		Environment env = calculated.getEnvironment() \n"
		        + "		Double val = env.getPropertyValue(calculated.getDataSource().getHostObjectTO(), \"lastValue\", Double.class);\n"
		        + "	    return val + input.getValue(); \n" + "}\n" + " public void setInput(Property input) { \n"
		        + "   this.input = input;\n" + " }\n" + "}";

		// Test sensor type
		ObjectType sens_type = env.getObjectType("TSENSOR");
		if (sens_type != null) {
			System.out.println("Deleting type 'TSENSOR' " + sens_type);
			env.deleteObjectType("TSENSOR");
		} else {
			System.out.println("Not deleting type 'TSENSOR' ");
		}

		sens_type = env.createObjectType("TSENSOR");
		Set<PropertyDescr> objPds = sens_type.getPropertyDescriptors();

		objPds.add(new PropertyDescr("lastValue", Double.class));
		objPds.add(new PropertyDescr("input", Double.class));

		env.updateObjectType(sens_type);

		String topRuleTypeName = "topRuleType";
		topRuleType = cre.getRuleTypeId(topRuleTypeName);
		if (topRuleType != null) {
			cre.removeRuleType(topRuleType);
		}
		topRuleType = cre.addRuleType(new RuleType(topRuleTypeName, ruleBody, "Groovy", false, "0 0 0 * * ?"));

		// Now let's create some instances
		topObj = env.createObject("TSENSOR");
		System.out.println("Top = " + TOFactory.getInstance().saveTO(topObj));
		CRERule rule = new CRERule("topRule", topRuleType, topObj, "lastValue");
		rule.addSrcProperty(topObj, "input", "input");
		cre.addRule(rule);
	}

	@After
	public void tearDown() throws Exception {
		env.deleteObjectType("TSENSOR");
		cre.removeRuleType(topRuleType);
	}

	@Test
	public void testCircularDependencies() throws Exception {
		env.setPropertyValue(topObj, "lastValue", 0.0);
		env.setPropertyValue(topObj, "input", 5.0);
		Assert.assertEquals(Double.valueOf(5.0), env.getPropertyValue(topObj, "lastValue", Double.class));
		env.setPropertyValue(topObj, "input", 6.0);
		Assert.assertEquals(Double.valueOf(11.0), env.getPropertyValue(topObj, "lastValue", Double.class));
		env.setPropertyValue(topObj, "input", 5.0);
		Assert.assertEquals(Double.valueOf(16.0), env.getPropertyValue(topObj, "lastValue", Double.class));
		Collection<Alert> alerts = ConsoleApp.ALERTING_SERVICE.getActiveAlerts(TOFactory.EMPTY_TO, false);
		boolean alertRaised = false;
		for (Alert alert : alerts) {
			if (alert.getMessage().equals("\"CycleTestRule\"")) {
				alertRaised = true;
			}
		}
		Assert.assertEquals(true, alertRaised);
	}
}
