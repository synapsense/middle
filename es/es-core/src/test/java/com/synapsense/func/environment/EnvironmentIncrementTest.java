package com.synapsense.func.environment;

import com.google.common.base.Strings;
import com.synapsense.DbConstants;
import com.synapsense.dto.*;
import com.synapsense.exception.*;
import com.synapsense.func.ConsoleApp;
import com.synapsense.search.Direction;
import com.synapsense.search.Path;
import com.synapsense.search.QueryDescription;
import com.synapsense.search.Relations;
import com.synapsense.service.Environment;
import com.synapsense.service.impl.dao.to.BinaryDataImpl;
import com.synapsense.service.impl.dao.to.PropertyTO;
import com.synapsense.service.impl.exception.DALSystemException;
import com.synapsense.util.CollectionUtils;
import com.synapsense.util.executor.Action;
import com.synapsense.util.executor.Executor;
import com.synapsense.util.executor.Executors;
import org.dbunit.JdbcDatabaseTester;
import org.dbunit.dataset.IDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import javax.naming.InitialContext;
import javax.transaction.UserTransaction;
import java.util.*;

import static org.junit.Assert.*;

/**
 * Group of functional tests. Works only with full ES server deployment(Database
 * server, Application server, SynapServer.ear)
 * 
 * @author shabanov
 * 
 */
public class EnvironmentIncrementTest {
	private static Environment env;

	private static int initialCountOfTypes = 0;

	private static JdbcDatabaseTester databaseTester;

	public static void waitForDbBatchUpdater() {
		waitForDbBatchUpdater(2000);
	}

	public static void waitForDbBatchUpdater(final long time) {
		try {
			// Thread.sleep(1000);
			Thread.sleep(time);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		ConsoleApp.init();

		env = ConsoleApp.env;

		String[] types = env.getObjectTypes();
		initialCountOfTypes = (types == null) ? 0 : types.length;

		databaseTester = new JdbcDatabaseTester(DbConstants.DB_DRIVER, DbConstants.DB_CONNECTION, DbConstants.DB_USER,
		        DbConstants.DB_PASSWORD);

		IDataSet databaseDataSet = databaseTester.getConnection().createDataSet(
		        new String[] { DbConstants.TBL_HISTORICAL_VALUES });
		DatabaseOperation.TRUNCATE_TABLE.execute(databaseTester.getConnection(), databaseDataSet);
	}

	@Test
	public void testCall() throws EnvException {
		assertEquals(initialCountOfTypes, env.getObjectTypes().length);
	}

	@Test
	public void testTypeCreateDelete() throws Exception {
		env.createObjectType("Type1");
		env.createObjectType("Second Variety");
		Collection<String> typeNames;
		try {
			typeNames = Arrays.asList(env.getObjectTypes());
			assertEquals(initialCountOfTypes + 2, typeNames.size());
			assertTrue(typeNames.contains("Type1"));
			assertTrue(typeNames.contains("Second Variety"));

			ObjectType[] types = env.getObjectTypes(new String[] { "Type1" });
			assertEquals(1, types.length);
		} finally {
			env.deleteObjectType("Type1");
			env.deleteObjectType("Second Variety");
		}
		typeNames = Arrays.asList(env.getObjectTypes());
		assertTrue(!typeNames.contains("Type1"));
		assertTrue(!typeNames.contains("Second Variety"));

	}

	@Test
	public void testTypeSearching() throws Exception {
		// creation of two generic types
		env.createObjectType("Type1");
		env.createObjectType("Second Variety");
		Collection<String> typeNames;
		try {
			typeNames = Arrays.asList(env.getObjectTypes());
			assertEquals(initialCountOfTypes + 2, typeNames.size());
			assertTrue(typeNames.contains("Type1"));
			assertTrue(typeNames.contains("Second Variety"));

			// getting just created type
			ObjectType[] types = env.getObjectTypes(new String[] { "Type1" });
			assertEquals(1, types.length);
			// getting just created type by UPPERCASEd name
			types = env.getObjectTypes(new String[] { "TYPE1" });
			assertEquals(1, types.length);

			// getting just created type
			types = env.getObjectTypes(new String[] { "Second Variety" });
			assertEquals(1, types.length);

			// getting just created type by partly UPPERCASEd name
			types = env.getObjectTypes(new String[] { "SECOND Variety" });
			assertEquals(1, types.length);

			ObjectType type = env.getObjectType("Type1");
			assertNotNull(type);
			assertEquals("Type1", type.getName());

			// not case sensitive
			type = env.getObjectType("TYPE1");
			assertNotNull(type);
			// returns name as it is stored in db (see above, how it was
			// created)
			assertEquals("Type1", type.getName());

			type = env.getObjectType("Second Variety");
			assertNotNull(type);
			assertEquals("Second Variety", type.getName());

			type = env.getObjectType("SECOND Variety");
			assertNotNull(type);
			assertEquals("Second Variety", type.getName());

			types = env.getObjectTypes(new String[] { "ALERT" });
			assertEquals(1, types.length);

			type = env.getObjectType("ALERT");
			assertNotNull(type);
			assertEquals("ALERT", type.getName());

			types = env.getObjectTypes(new String[] { "Stat" });
			assertEquals(0, types.length);

			type = env.getObjectType("Stat");
			assertNull(type);

			types = env.getObjectTypes(new String[] { "NonExisting" });
			assertEquals(0, types.length);

			type = env.getObjectType("NonExisting");
			assertNull(type);

		} finally {
			env.deleteObjectType("Type1");
			env.deleteObjectType("Second Variety");
		}
	}

	@Test
	public void testTypeCreateUpdate() throws Exception {
		try {
			ObjectType t1 = env.createObjectType("Type1");
			t1.getPropertyDescriptors().add(new PropertyDescr("color", Double.class.getName()));

			env.updateObjectType(t1);
			ObjectType[] types = env.getObjectTypes(new String[] { "Type1" });
			assertEquals(1, types.length);
			t1 = types[0];
			assertEquals(1, t1.getPropertyDescriptors().size());

			t1.getPropertyDescriptors().add(new PropertyDescr("model", Double.class.getName()));
			env.updateObjectType(t1);
			t1 = env.getObjectTypes(new String[] { "Type1" })[0];
			assertEquals(2, t1.getPropertyDescriptors().size());

			env.createObject("Type1", new ValueTO[] { new ValueTO("color", 1.5) });
			PropertyDescr pd = t1.getPropertyDescriptor("color");
			t1.getPropertyDescriptors().remove(pd);
			env.updateObjectType(t1);

			t1 = env.getObjectTypes(new String[] { "Type1" })[0];
			assertEquals(1, t1.getPropertyDescriptors().size());

			t1.getPropertyDescriptors().clear();
			env.updateObjectType(t1);
			t1 = env.getObjectTypes(new String[] { "Type1" })[0];
			assertEquals(0, t1.getPropertyDescriptors().size());
		} finally {
			env.deleteObjectType("Type1");
		}
	}

	@Test
	public void testObjectCreation() throws Exception {
		try {
			env.createObjectType("Type1");
			TO<?> obj = env.createObject("Type1");
			env.deleteObject(obj);
		} finally {
			env.deleteObjectType("Type1");
		}

	}

	@Test
	public void testSetGetProperties() throws Exception {
		try {
			ObjectType t1 = env.createObjectType("Type1");
			t1.getPropertyDescriptors().add(new PropertyDescr("comport", String.class.getName()));
			t1.getPropertyDescriptors().add(new PropertyDescr("pan", Integer.class.getName()));

			t1.getPropertyDescriptors().add(new PropertyDescr("dt", Date.class.getName()));

			env.updateObjectType(t1);

			TO<?> obj = env.createObject("Type1");

			ValueTO p1 = new ValueTO("comport", "COM3");
			ValueTO p2 = new ValueTO("pan", 1000);
			Date currentDate = new Date();
			ValueTO p3 = new ValueTO("dt", currentDate);

			env.setPropertyValue(obj, p1.getPropertyName(), p1.getValue());

			env.setPropertyValue(obj, p2.getPropertyName(), p2.getValue());

			env.setPropertyValue(obj, p3.getPropertyName(), p3.getValue());

			assertEquals("COM3", env.getPropertyValue(obj, "comport", String.class));

			Collection<ValueTO> propValList = env.getAllPropertiesValues(obj);

			assertFalse(propValList.isEmpty());
			// 3 added above + "numAlerts"
			assertEquals(4, propValList.size());

			env.deleteObject(obj);
		} finally {
			env.deleteObjectType("Type1");
		}
	}

	@Test(expected = DALSystemException.class)
	public void testBinaryPropertyCreateAsHistoricalFailure() throws Exception {
		try {
			ObjectType t1 = env.createObjectType("Type1");
			t1.getPropertyDescriptors().add(new PropertyDescr("binary_prop", BinaryData.class.getName(), true));
			env.updateObjectType(t1);
		} finally {
			env.deleteObjectType("Type1");
		}
	}

	@Test(expected = IllegalArgumentException.class)
	public void testBinaryPropertyExceedLimitFailure() throws Exception {
		byte[] data = new byte[16 * 1024 * 1024 + 1];
		new BinaryDataImpl(data);
	}

	@Test
	public void testSetGetBinaryProperties() throws Exception {
		try {
			env.configurationComplete();
			env.configurationStart();
			String testStr = "Test String";
			ObjectType t1 = env.createObjectType("Type1");
			t1.getPropertyDescriptors().add(new PropertyDescr("double_prop", Double.class.getName()));
			t1.getPropertyDescriptors().add(new PropertyDescr("binary_prop", BinaryData.class.getName()));

			env.updateObjectType(t1);

			TO<?> obj = env.createObject("Type1");
			env.configurationComplete();

			byte[] arr = testStr.getBytes();
			BinaryData data = new BinaryDataImpl(arr);
			ValueTO p1 = new ValueTO("binary_prop", data);
			env.setPropertyValue(obj, p1.getPropertyName(), p1.getValue());
			env.setPropertyValue(obj, "double_prop", 1.0);
			env.setPropertyValue(obj, "double_prop", 1.1);
			env.setPropertyValue(obj, "double_prop", 1.2);

			waitForDbBatchUpdater();

			BinaryData data2 = env.getPropertyValue(obj, "binary_prop", BinaryData.class);
			String result = new String(data2.getValue());
			assertEquals(testStr, result);

			ConsoleApp.stopCache();
            ConsoleApp.startCache();

			data2 = env.getPropertyValue(obj, "binary_prop", BinaryData.class);
			result = new String(data2.getValue());
			assertEquals(testStr, result);

			String testStr2 = "Test String2";
			arr = testStr2.getBytes();
			data = new BinaryDataImpl(arr);
			p1 = new ValueTO("binary_prop", data);
			env.setPropertyValue(obj, p1.getPropertyName(), p1.getValue());

			waitForDbBatchUpdater();

			data2 = env.getPropertyValue(obj, "binary_prop", BinaryData.class);
			result = new String(data2.getValue());
			assertEquals(testStr2, result);

			ConsoleApp.stopCache();
			ConsoleApp.startCache();

			data2 = env.getPropertyValue(obj, "binary_prop", BinaryData.class);
			result = new String(data2.getValue());
			assertEquals("new value was not saved", testStr2, result);

		} finally {
			env.deleteObjectType("Type1");
		}
	}

	@Test
	public void testSetGetBinaryCollectionProperties() throws Exception {
		try {
			String testStr = "Test String";
			String testStr2 = "Test String2";

			ObjectType t1 = env.createObjectType("Type1");
			t1.getPropertyDescriptors().add(new PropertyDescr("binary_prop", BinaryData.class.getName(), false, true));

			env.updateObjectType(t1);

			BinaryData data = new BinaryDataImpl(testStr.getBytes());
			BinaryData data2 = new BinaryDataImpl(testStr2.getBytes());
			java.util.Set<BinaryData> collection = new java.util.HashSet<BinaryData>();
			collection.add(data);
			collection.add(data2);

			ValueTO p1 = new ValueTO("binary_prop", collection);

			TO<?> obj = env.createObject("Type1");
			env.setPropertyValue(obj, p1.getPropertyName(), p1.getValue());

			// check binary properties before cache restart
			Collection<BinaryData> binaryCollection = env.getPropertyValue(obj, "binary_prop", Collection.class);
			assertEquals(2, binaryCollection.size());
			assertTrue("Not all binary objects was returned", binaryCollection.contains(data));
			assertTrue("Not all binary objects was returned", binaryCollection.contains(data2));

			ConsoleApp.stopCache();
			// waits for db batch udpdater checkpoint(wait time may vary)
			Thread.sleep(3000);
			ConsoleApp.startCache();

			// check binary properties after cache restart
			binaryCollection = env.getPropertyValue(obj, "binary_prop", Collection.class);
			assertEquals(2, binaryCollection.size());
			assertTrue("Not all binary objects was returned", binaryCollection.contains(data));
			assertTrue("Not all binary objects was returned", binaryCollection.contains(data2));

			// add new value
			String testStr3 = "Test String3";
			BinaryData data3 = new BinaryDataImpl(testStr3.getBytes());
			env.addPropertyValue(obj, "binary_prop", data3);

			binaryCollection = env.getPropertyValue(obj, "binary_prop", Collection.class);
			assertEquals(3, binaryCollection.size());
			assertTrue("Not all binary objects was returned", binaryCollection.contains(data));
			assertTrue("Not all binary objects was returned", binaryCollection.contains(data2));
			assertTrue("Not all binary objects was returned", binaryCollection.contains(data3));

			env.removePropertyValue(obj, "binary_prop", data);

			binaryCollection = env.getPropertyValue(obj, "binary_prop", Collection.class);
			assertEquals(2, binaryCollection.size());
			assertTrue("Not all binary objects was returned", binaryCollection.contains(data2));
			assertTrue("Not all binary objects was returned", binaryCollection.contains(data3));

		} finally {
			env.deleteObjectType("Type1");
		}
	}

	@Test
	public void testCollectionProperties() throws Exception {
		String TYPE = "Type1";
		try {
			ObjectType t1 = env.createObjectType(TYPE);
			t1.getPropertyDescriptors().add(new PropertyDescr("singleInt", Integer.class.getName(), false, false));

			t1.getPropertyDescriptors().add(new PropertyDescr("singleTO", TO.class.getName(), false, false));

			t1.getPropertyDescriptors()
			        .add(new PropertyDescr("singlePropTO", PropertyTO.class.getName(), false, false));

			t1.getPropertyDescriptors().add(new PropertyDescr("comport", String.class.getName(), false, true));

			t1.getPropertyDescriptors().add(new PropertyDescr("pan", Integer.class.getName(), false, true));

			t1.getPropertyDescriptors().add(new PropertyDescr("dt", Date.class.getName(), false, true));

			t1.getPropertyDescriptors().add(new PropertyDescr("TOs", TO.class.getName(), false, true));

			t1.getPropertyDescriptors().add(new PropertyDescr("propertyTOs", PropertyTO.class.getName(), false, true));

			env.updateObjectType(t1);

			// bug#1570
			// tests object creation with initial collection property
			Collection<String> comports = new LinkedList<String>();
			comports.add("COM1");
			comports.add("COM2");

			ValueTO[] initialValues = { new ValueTO("comport", comports) };
			// creates object with collection of comports
			TO<?> objInitialCollection = env.createObject(TYPE, initialValues);
			// checks that all props are set
			Collection<String> gotComports = env.getPropertyValue(objInitialCollection, "comport", Collection.class);
			assertEquals(2, gotComports.size());
			// cleanup after test
			env.deleteObject(objInitialCollection);

			TO<?> obj = env.createObject(TYPE);
			TO<?> obj_01 = env.createObject(TYPE, new ValueTO[] { new ValueTO("TOs", new HashSet(0)) });
			TO<?> obj_02 = env.createObject(TYPE);
			TO<?> obj_03 = env.createObject(TYPE);

			env.createObject(TYPE, new ValueTO[] { new ValueTO("TOs", null) });

			ConsoleApp.stopCache();
			ConsoleApp.startCache();

			// check cache state , TOs value should be empty collection after
			// cache restart
			Collection<String> obj01TOs = env.getPropertyValue(obj_01, "TOs", Collection.class);
			assertNotNull(obj01TOs);
			assertTrue(obj01TOs.isEmpty());

			Collection<String> obj02TOs = env.getPropertyValue(obj_02, "TOs", Collection.class);
			assertNotNull(obj02TOs);
			assertTrue(obj02TOs.isEmpty());

			Collection<String> obj03TOs = env.getPropertyValue(obj_03, "TOs", Collection.class);
			assertNotNull(obj03TOs);
			assertTrue(obj03TOs.isEmpty());

			Collection<TO> objLinks = new LinkedList<TO>();
			objLinks.add(obj_01);
			objLinks.add(obj_02);
			objLinks.add(obj_03);
			objLinks.add(obj_02);
			objLinks.add(obj_03);
			objLinks.add(null);

			env.setPropertyValue(obj, "TOs", objLinks);

			env.setPropertyValue(obj, "TOs", objLinks);

			env.setPropertyValue(obj, "TOs", objLinks);

			env.setPropertyValue(obj_01, "TOs", objLinks);
			env.setPropertyValue(obj_01, "TOs", new HashSet(0));
			obj01TOs = env.getPropertyValue(obj_01, "TOs", Collection.class);
			assertNotNull(obj01TOs);
			assertTrue(obj01TOs.isEmpty());

			// ??? configurationStart missed
			// env.configurationComplete();

			ValueTO pvto = env.getPropertyValue(obj, "TOs", ValueTO.class);
			long setTime = pvto.getTimeStamp();

			objLinks = env.getPropertyValue(obj, "TOs", Collection.class);
			assertEquals(4, objLinks.size());
			assertTrue(objLinks.contains(obj_01));
			assertTrue(objLinks.contains(obj_02));
			assertTrue(objLinks.contains(obj_03));

			pvto = env.getPropertyValue(obj, "TOs", ValueTO.class);
			objLinks = (Collection<TO>) pvto.getValue();
			assertEquals(4, objLinks.size());
			assertTrue(objLinks.contains(obj_01));
			assertTrue(objLinks.contains(obj_02));
			assertTrue(objLinks.contains(obj_03));

			// latency to check timestamps
			final long LATENCY = 100;

			{// updates object contained in collection
				// on that step - TOs property timestamp must be refreshed
				// changing state of obj_01
				env.setPropertyValue(obj_01, "singleInt", 10);

				// get property as ValueTO with timestamp
				pvto = env.getPropertyValue(obj, "TOs", ValueTO.class);

				// timestamp after update reference to object
				long timeAfterUpdateRef = pvto.getTimeStamp();
				ValueTO pvto1 = env.getPropertyValue(obj_01, "singleInt", ValueTO.class);
				// must be more than LATENCY
				// TODO : is not valid because of ReferencedObjectChangedEvent
				// is raised instead of direct set of timestamp
				// assertEquals(timeAfterUpdateRef, pvto1.getTimeStamp());

			}

			{// updates object contained in link to object
				env.setPropertyValue(obj, "singleTO", obj_01);
				pvto = env.getPropertyValue(obj, "singleTO", ValueTO.class);
				setTime = pvto.getTimeStamp();

				// on that step - TOs property timestamp must be refreshed
				// changing state of obj_01
				env.setPropertyValue(obj_01, "singleInt", 10);
				ValueTO pvto1 = env.getPropertyValue(obj_01, "singleInt", ValueTO.class);

				pvto = env.getPropertyValue(obj, "singleTO", ValueTO.class);
				long timeAfterUpdateRef = pvto.getTimeStamp();
				// TODO : is not valid because of ReferencedObjectChangedEvent
				// is raised instead of direct set of timestamp
				// assertEquals(timeAfterUpdateRef, pvto1.getTimeStamp());
			}
			{// updates object prop.timestamp contained in link to object's
				PropertyTO propTo = new PropertyTO(obj_01, "singleInt");
				env.setPropertyValue(obj, "singlePropTO", propTo);
				pvto = env.getPropertyValue(obj, "singlePropTO", ValueTO.class);
				setTime = pvto.getTimeStamp();

				// on that step - TOs property timestamp must be refreshed when
				// changing state of obj_01
				env.setPropertyValue(obj_01, "singleInt", 10);

				ValueTO pvto1 = env.getPropertyValue(obj_01, "singleInt", ValueTO.class);

				pvto = env.getPropertyValue(obj, "singlePropTO", ValueTO.class);

				long timeAfterUpdateRef = pvto.getTimeStamp();
				assertEquals(timeAfterUpdateRef, pvto.getTimeStamp());
			}

			env.removePropertyValue(obj, "TOs", obj_01);

			objLinks = env.getPropertyValue(obj, "TOs", Collection.class);
			assertEquals(3, objLinks.size());
			assertTrue(objLinks.contains(obj_02));
			assertTrue(objLinks.contains(obj_03));

			pvto = env.getPropertyValue(obj, "TOs", ValueTO.class);
			objLinks = (Collection<TO>) pvto.getValue();
			assertEquals(3, objLinks.size());
			assertTrue(objLinks.contains(obj_02));
			assertTrue(objLinks.contains(obj_03));

			env.removePropertyValue(obj, "TOs", obj_02);

			objLinks = env.getPropertyValue(obj, "TOs", Collection.class);
			assertEquals(2, objLinks.size());
			assertTrue(objLinks.contains(obj_03));

			pvto = env.getPropertyValue(obj, "TOs", ValueTO.class);
			objLinks = (Collection<TO>) pvto.getValue();
			assertEquals(2, objLinks.size());
			assertTrue(objLinks.contains(obj_03));

			//env.removePropertyValue(obj, "TOs", null);

			objLinks = env.getPropertyValue(obj, "TOs", Collection.class);
			assertEquals(2, objLinks.size());
			assertTrue(objLinks.contains(obj_03));

			pvto = env.getPropertyValue(obj, "TOs", ValueTO.class);
			objLinks = (Collection<TO>) pvto.getValue();
			assertEquals(2, objLinks.size());
			assertTrue(objLinks.contains(obj_03));

		} finally {
			env.deleteObjectType(TYPE);
		}

	}

	@Test
	public void testPropDeleting() throws Exception {
		try {
			ObjectType t1 = env.createObjectType("Type1");
			t1.getPropertyDescriptors().add(new PropertyDescr("comport1", String.class.getName()));
			t1.getPropertyDescriptors().add(new PropertyDescr("comport2", String.class.getName()));
			t1.getPropertyDescriptors().add(new PropertyDescr("comport3", String.class.getName()));
			env.updateObjectType(t1);

			TO<?> obj = env.createObject("Type1");
			TO<?> obj2 = env.createObject("Type1");
			TO<?> obj3 = env.createObject("Type1");

			ValueTO pvComport1 = new ValueTO("comport1", "COM1");
			ValueTO pvComport2 = new ValueTO("comport1", "COM2");
			ValueTO pvComport3 = new ValueTO("comport1", "COM3");
			env.setPropertyValue(obj, pvComport1.getPropertyName(), pvComport1.getValue());
			env.setPropertyValue(obj, pvComport2.getPropertyName(), pvComport2.getValue());
			env.setPropertyValue(obj, pvComport3.getPropertyName(), pvComport3.getValue());

			env.setPropertyValue(obj2, pvComport1.getPropertyName(), pvComport1.getValue());
			env.setPropertyValue(obj2, pvComport2.getPropertyName(), pvComport2.getValue());
			env.setPropertyValue(obj2, pvComport3.getPropertyName(), pvComport3.getValue());

			env.setPropertyValue(obj3, pvComport1.getPropertyName(), pvComport1.getValue());
			env.setPropertyValue(obj3, pvComport2.getPropertyName(), pvComport2.getValue());
			env.setPropertyValue(obj3, pvComport3.getPropertyName(), pvComport3.getValue());

			t1.getPropertyDescriptors().remove(new PropertyDescr("comport1", String.class.getName()));

			env.updateObjectType(t1);

			ObjectType[] objTypes = env.getObjectTypes(new String[] { "Type1" });
			assertEquals(2, objTypes[0].getPropertyDescriptors().size());

			t1.getPropertyDescriptors().remove(new PropertyDescr("comport2", String.class.getName()));

			env.updateObjectType(t1);
			objTypes = env.getObjectTypes(new String[] { "Type1" });
			assertEquals(1, objTypes[0].getPropertyDescriptors().size());

			try {
				env.getPropertyValue(obj, "comport1", String.class);

				env.getPropertyValue(obj2, "comport1", String.class);

				env.getPropertyValue(obj3, "comport1", String.class);

				assertTrue(false);
			} catch (PropertyNotFoundException e) {
			}

			t1.getPropertyDescriptors().clear();
			env.updateObjectType(t1);
			objTypes = env.getObjectTypes(new String[] { "Type1" });
			assertEquals(0, objTypes[0].getPropertyDescriptors().size());

		} finally {
			env.deleteObjectType("Type1");
		}
	}

	@Test(expected = PropertyNotFoundException.class)
	public void testIncorrectProperties() throws Exception {
		try {
			env.createObjectType("Type1");

			TO<?> obj = env.createObject("Type1");

			ValueTO p1 = new ValueTO("comport", "COM3");

			env.setPropertyValue(obj, p1.getPropertyName(), p1.getValue());
		} finally {
			env.deleteObjectType("Type1");

		}
	}

	@Test(expected = UnableToConvertPropertyException.class)
	public void testIncorrectPropertiesValues() throws Exception {
		try {
			ObjectType t1 = env.createObjectType("Type1");
			t1.getPropertyDescriptors().add(new PropertyDescr("comport", String.class.getName()));
			env.updateObjectType(t1);

			TO<?> obj = env.createObject("Type1");

			ValueTO p1 = new ValueTO("comport", "COM3");

			env.setPropertyValue(obj, p1.getPropertyName(), "COM3");
			Double d = env.getPropertyValue(obj, p1.getPropertyName(), Double.class);
		} finally {
			env.deleteObjectType("Type1");
		}
	}

	@Test
	public void testSearhingObjects() throws Exception {
		String TYPE_TEST = "Type_tested";
		String TYPE_TEST_like1 = "Type_tested_Like1";
		try {
			ObjectType t1 = env.createObjectType(TYPE_TEST);
			PropertyDescr[] props = new PropertyDescr[] { new PropertyDescr("name", String.class.getName()),
			        new PropertyDescr("propertyString", String.class.getName()),
			        new PropertyDescr("propertyDouble", Double.class.getName()),
			        new PropertyDescr("propertyInt", Integer.class.getName()),
			        new PropertyDescr("propertyDate", Date.class.getName()),
			        new PropertyDescr("propertyLong", Long.class.getName()),
			        new PropertyDescr("objectLink", TO.class.getName()),
			        new PropertyDescr("propertyLink", PropertyTO.class.getName()) };
			for (PropertyDescr pd : props) {
				t1.getPropertyDescriptors().add(pd);
			}

			env.updateObjectType(t1);
			String object010 = "object010";
			String object011 = "object011";
			String object012 = "object012";
			String object013 = "object013";
			String valueS = "value";
			Integer valueI = 1024;
			Double valueD = 10.01;

			TO to010 = env.createObject(TYPE_TEST, new ValueTO[] { new ValueTO("name", object010) });
			TO to011 = env.createObject(TYPE_TEST, new ValueTO[] { new ValueTO("name", object011) });
			TO to012 = env.createObject(TYPE_TEST, new ValueTO[] { new ValueTO("name", object012) });
			TO to013 = env.createObject(TYPE_TEST, new ValueTO[] { new ValueTO("name", object013) });
			env.setPropertyValue(to010, "propertyString", valueS);
			env.setPropertyValue(to010, "propertyInt", valueI);
			env.setPropertyValue(to010, "propertyDouble", valueD);

			env.setPropertyValue(to011, "propertyString", valueS);
			env.setPropertyValue(to011, "propertyDouble", valueD);

			env.setPropertyValue(to012, "propertyString", valueS);
			env.setPropertyValue(to013, "propertyString", valueS);

			env.setPropertyValue(to010, "objectLink", to013);
			env.setPropertyValue(to011, "objectLink", to013);
			env.setPropertyValue(to012, "objectLink", null);

			env.setPropertyValue(to010, "propertyLink", new PropertyTO(to013, "propertyString"));

			env.setPropertyValue(to011, "propertyLink", null);

			env.setPropertyValue(to012, "propertyLink", null);

			waitForDbBatchUpdater();

			{// by link
				ValueTO[] criteria = new ValueTO[] { new ValueTO("objectLink", to013) };
				Collection<TO<?>> objects = env.getObjects(TYPE_TEST, criteria);
				assertEquals(2, objects.size());
				assertTrue(objects.contains(to010));
				assertTrue(objects.contains(to011));
			}

			{// by link and scalar
				ValueTO[] criteria = new ValueTO[] { new ValueTO("objectLink", to013), new ValueTO("name", object010) };
				Collection<TO<?>> objects = env.getObjects(TYPE_TEST, criteria);
				assertEquals(1, objects.size());
				assertTrue(objects.contains(to010));
			}

			{// by plink
				ValueTO[] criteria = new ValueTO[] { new ValueTO("propertyLink",
				        new PropertyTO(to013, "propertyString")) };
				Collection<TO<?>> objects = env.getObjects(TYPE_TEST, criteria);
				assertEquals(1, objects.size());
				assertTrue(objects.contains(to010));
			}

			{// by type
				try {
					ValueTO[] criteria = new ValueTO[] {};
					env.getObjects(TYPE_TEST, criteria);
					fail("Illegal input criteria");
				} catch (Exception e) {
				}
			}

			{// by name
				ValueTO[] criteria = new ValueTO[] { new ValueTO("name", object010) };

				Collection<TO<?>> objects = env.getObjects(TYPE_TEST, criteria);
				assertEquals(1, objects.size());
			}

			{
				ValueTO[] criteria = new ValueTO[] { new ValueTO("propertyString", valueS) };

				Collection<TO<?>> objects = env.getObjects(TYPE_TEST, criteria);
				assertEquals(4, objects.size());
			}
			{
				ValueTO[] criteria = new ValueTO[] { new ValueTO("propertyString", valueS),
				        new ValueTO("propertyInt", valueI) };

				Collection<TO<?>> objects = env.getObjects(TYPE_TEST, criteria);
				assertEquals(1, objects.size());
			}

			{
				ValueTO[] criteria = new ValueTO[] { new ValueTO("propertyDouble", valueD) };

				Collection<TO<?>> objects = env.getObjects(TYPE_TEST, criteria);
				assertEquals(2, objects.size());

			}

			{// no objects with property value
				ValueTO[] criteria = new ValueTO[] { new ValueTO("propertyDouble", Double.MIN_VALUE) };

				Collection<TO<?>> objects = env.getObjects(TYPE_TEST, criteria);
				assertEquals(0, objects.size());
			}

			{// null scalar property
				ValueTO[] criteria = new ValueTO[] { new ValueTO("propertyDouble", null) };

				Collection<TO<?>> objects = env.getObjects(TYPE_TEST, criteria);
				assertEquals(2, objects.size());
				assertTrue(objects.contains(to012));
				assertTrue(objects.contains(to013));
			}

			ObjectType t2 = env.createObjectType(TYPE_TEST_like1);
			PropertyDescr[] t2Properties = new PropertyDescr[] { new PropertyDescr("name", String.class.getName()) };
			for (PropertyDescr pd : t2Properties) {
				t2.getPropertyDescriptors().add(pd);
			}
			env.updateObjectType(t2);

			String object010T2 = "object010t2";
			String object011T2 = "object011t2";
			TO to010T2 = env.createObject(TYPE_TEST_like1, new ValueTO[] { new ValueTO("name", object010T2) });
			TO to011T2 = env.createObject(TYPE_TEST_like1, new ValueTO[] { new ValueTO("name", object011T2) });
			{
				Collection<TO<?>> objects = env.getObjects(TYPE_TEST, ObjectTypeMatchMode.START_WITH, null);
				assertEquals(6, objects.size());
			}

			{
				Collection<TO<?>> objects = env.getObjects(TYPE_TEST_like1, ObjectTypeMatchMode.START_WITH, null);
				assertEquals(2, objects.size());
			}

			{
				Collection<TO<?>> objects = env.getObjects("Some_type", ObjectTypeMatchMode.START_WITH, null);
				assertEquals(0, objects.size());
			}

			{
				Collection<TO<?>> objects = env.getObjects("ike1", ObjectTypeMatchMode.END_WITH, null);
				assertEquals(2, objects.size());
			}

			{
				Collection<TO<?>> objects = env.getObjects("ke", ObjectTypeMatchMode.END_WITH, null);
				assertEquals(0, objects.size());
			}

			{
				Collection<TO<?>> objects = env.getObjects("tested", ObjectTypeMatchMode.CONTAINS, null);
				assertEquals(6, objects.size());
			}

			{
				Collection<TO<?>> objects = env.getObjects("non", ObjectTypeMatchMode.CONTAINS, null);
				assertEquals(0, objects.size());
			}

			{// by name and "like" type
				ValueTO[] criteria = new ValueTO[] { new ValueTO("name", object010) };

				Collection<TO<?>> objects = env.getObjects(TYPE_TEST, ObjectTypeMatchMode.START_WITH, criteria);
				assertEquals(1, objects.size());
			}

			{// by type via special method
				Collection<TO<?>> objects = env.getObjectsByType(TYPE_TEST);
				assertEquals(4, objects.size());
			}

			{// by type via special method
				Collection<TO<?>> objects = env.getObjectsByType(TYPE_TEST, ObjectTypeMatchMode.START_WITH);
				assertEquals(6, objects.size());
			}

		} finally {
			env.deleteObjectType(TYPE_TEST);
			env.deleteObjectType(TYPE_TEST_like1);
			;
		}
	}

	@Test
	public void testMultipleParentsSuccess() throws Exception {
		String PARENT_TYPE = "PARENT";
		String CHILD_TYPE = "CHILD";
		try {
			// init generic
			ObjectType tParent = env.createObjectType(PARENT_TYPE);
			ObjectType tChild = env.createObjectType(CHILD_TYPE);

			TO parent1 = env.createObject(PARENT_TYPE);
			TO parent2 = env.createObject(PARENT_TYPE);
			TO parent3 = env.createObject(PARENT_TYPE);
			TO parent4 = env.createObject(PARENT_TYPE);
			TO child = env.createObject(CHILD_TYPE);

			env.setRelation(parent1, child);
			env.setRelation(parent2, child);
			env.setRelation(parent3, child);
			env.setRelation(parent4, child);

		} finally {
			env.deleteObjectType(CHILD_TYPE);
			env.deleteObjectType(PARENT_TYPE);
		}
	}

	@Test
	public void testWorkingWithGenericRelations() throws Exception {
		String TYPE_TEST = "Type_tested";
		String TYPE_TEST2 = "Type_tested2";
		try {
			// init generic
			ObjectType t1 = env.createObjectType(TYPE_TEST);
			ObjectType t2 = env.createObjectType(TYPE_TEST2);
			String object010 = "object010";
			String object011 = "object011";
			String object012 = "object012";
			String object013 = "object013";
			String object014 = "object014";
			String object015 = "object015";
			String object016 = "object016";
			String object017 = "object017";
			String object018 = "object018";

			TO to010 = env.createObject(TYPE_TEST);
			TO to011 = env.createObject(TYPE_TEST);
			TO to012 = env.createObject(TYPE_TEST);
			TO to013 = env.createObject(TYPE_TEST);
			TO to014 = env.createObject(TYPE_TEST);
			TO to015 = env.createObject(TYPE_TEST);

			TO to016 = env.createObject(TYPE_TEST2);
			TO to017 = env.createObject(TYPE_TEST2);
			TO to018 = env.createObject(TYPE_TEST2);

			env.setRelation(to010, to011);
			env.setRelation(to010, to016);
			env.setRelation(to010, to012);
			env.setRelation(to011, to013);
			env.setRelation(to011, to014);
			env.setRelation(to011, to018);
			env.setRelation(to013, to015);
			env.setRelation(to013, to017);

			Collection<TO<?>> ancestors = null;
			Collection<TO<?>> childs = null;

			{
				{
//					childs = env.getRelatedObjects(to010, null, true);
//					assertEquals(8, childs.size());
//
//					childs = env.getRelatedObjects(to010, "", true);
//					assertEquals(8, childs.size());

					childs = env.getRelatedObjects(to010, TYPE_TEST, true);
					assertEquals(5, childs.size());

					childs = env.getRelatedObjects(to010, TYPE_TEST2, true);
					assertEquals(3, childs.size());

//					ancestors = env.getRelatedObjects(to015, null, false);
//					assertEquals(3, ancestors.size());
//
//					ancestors = env.getRelatedObjects(to017, null, false);
//					assertEquals(3, ancestors.size());

					// all first level children
					childs = env.getChildren(to010);
					assertEquals(3, childs.size());
					assertTrue(childs.contains(to011));
					assertTrue(childs.contains(to012));
					assertTrue(childs.contains(to016));

					// all first level children
					childs = env.getChildren(to011);
					assertEquals(3, childs.size());
					assertTrue(childs.contains(to013));
					assertTrue(childs.contains(to014));
					assertTrue(childs.contains(to018));

					// all first level children
					childs = env.getChildren(to012);
					assertEquals(0, childs.size());

					// all first level children
					childs = env.getChildren(to013);
					assertEquals(2, childs.size());
					assertTrue(childs.contains(to015));
					assertTrue(childs.contains(to017));

					// all first level children by specified type
					childs = env.getChildren(to010, TYPE_TEST);
					assertEquals(2, childs.size());
					assertTrue(childs.contains(to011));
					assertTrue(childs.contains(to012));

					childs = env.getChildren(to010, TYPE_TEST2);
					assertEquals(1, childs.size());
					assertTrue(childs.contains(to016));

					// all first level children by type which is non-existing
					childs = env.getChildren(to010, "FAKE_TYPE");
					assertEquals(0, childs.size());

					Collection<TO<?>> ids = new ArrayList<TO<?>>(3);
					ids.add(to010);
					ids.add(to011);
					ids.add(to013);
					Collection<CollectionTO> childrenArray = env.getChildren(ids);
					Iterator<CollectionTO> childrenIterator = childrenArray.iterator();

					CollectionTO to10Children = childrenIterator.next();
					assertEquals(3, to10Children.getPropValues().size());
					assertTrue(to10Children.getPropValues().contains(new ValueTO(to011.getID().toString(), to011)));
					assertTrue(to10Children.getPropValues().contains(new ValueTO(to016.getID().toString(), to016)));
					assertTrue(to10Children.getPropValues().contains(new ValueTO(to012.getID().toString(), to012)));

					CollectionTO to11Children = childrenIterator.next();
					assertEquals(3, to11Children.getPropValues().size());
					assertTrue(to11Children.getPropValues().contains(new ValueTO(to013.getID().toString(), to013)));
					assertTrue(to11Children.getPropValues().contains(new ValueTO(to014.getID().toString(), to014)));
					assertTrue(to11Children.getPropValues().contains(new ValueTO(to018.getID().toString(), to018)));

					CollectionTO to13Children = childrenIterator.next();
					assertEquals(2, to13Children.getPropValues().size());
					assertTrue(to13Children.getPropValues().contains(new ValueTO(to015.getID().toString(), to015)));
					assertTrue(to13Children.getPropValues().contains(new ValueTO(to017.getID().toString(), to017)));
				}
			}
			// cyclic references
			try {
				env.setRelation(to011, to010);
				assertTrue(false);
			} catch (UnsupportedRelationException e) {
			}

			// cyclic references
			try {
				env.setRelation(to014, to010);
				assertTrue(false);
			} catch (UnsupportedRelationException e) {
			}

			// cyclic references
			try {
				env.setRelation(to014, to014);
				assertTrue(false);
			} catch (UnsupportedRelationException e) {
			}

			// test cases to check remove relation behavior
			env.removeRelation(to011, to013);
//			childs = env.getRelatedObjects(to011, null, true);
//			assertEquals(2, childs.size());
//
//			ancestors = env.getRelatedObjects(to011, null, false);
//			assertEquals(1, ancestors.size());

			childs = env.getChildren(to011);
			assertEquals(2, childs.size());

			ancestors = env.getParents(to013);
			assertEquals(0, ancestors.size());
//
//			childs = env.getRelatedObjects(to013, null, true);
//			assertEquals(2, childs.size());
//
//			ancestors = env.getRelatedObjects(to013, null, false);
//			assertEquals(0, ancestors.size());

			ancestors = env.getParents(to015);
			assertEquals(1, ancestors.size());

			ancestors = env.getParents(to017);
			assertEquals(1, ancestors.size());

//			ancestors = env.getRelatedObjects(to015, null, false);
//			assertEquals(1, ancestors.size());
//
//			ancestors = env.getRelatedObjects(to017, null, false);
//			assertEquals(1, ancestors.size());

			// restores deleted relation
			env.setRelation(to011, to013);

//			childs = env.getRelatedObjects(to011, null, true);
//			assertEquals(5, childs.size());
//
//			ancestors = env.getRelatedObjects(to011, null, false);
//			assertEquals(1, ancestors.size());

			childs = env.getChildren(to011);
			assertEquals(3, childs.size());

			ancestors = env.getParents(to013);
			assertEquals(1, ancestors.size());

//			childs = env.getRelatedObjects(to013, null, true);
//			assertEquals(2, childs.size());
//
//			ancestors = env.getRelatedObjects(to013, null, false);
//			assertEquals(2, ancestors.size());

			ancestors = env.getParents(to015);
			assertEquals(1, ancestors.size());

			ancestors = env.getParents(to017);
			assertEquals(1, ancestors.size());

//			ancestors = env.getRelatedObjects(to015, null, false);
//			assertTrue(ancestors.contains(to013));
//			assertTrue(ancestors.contains(to011));
//			assertTrue(ancestors.contains(to010));
//			assertEquals(3, ancestors.size());
//
//			ancestors = env.getRelatedObjects(to017, null, false);
//			assertTrue(ancestors.contains(to013));
//			assertTrue(ancestors.contains(to011));
//			assertTrue(ancestors.contains(to010));
//			assertEquals(3, ancestors.size());

			env.deleteObject(to011);

			ancestors = env.getParents(to013);
			assertEquals(0, ancestors.size());
//
//			childs = env.getRelatedObjects(to013, null, true);
//			assertEquals(2, childs.size());
//
//			ancestors = env.getRelatedObjects(to013, null, false);
//			assertEquals(0, ancestors.size());

			ancestors = env.getParents(to015);
			assertEquals(1, ancestors.size());

			ancestors = env.getParents(to017);
			assertEquals(1, ancestors.size());

//			ancestors = env.getRelatedObjects(to015, null, false);
//			assertEquals(1, ancestors.size());
//
//			ancestors = env.getRelatedObjects(to017, null, false);
//			assertEquals(1, ancestors.size());

			env.deleteObjectType(TYPE_TEST2);

//			ancestors = env.getRelatedObjects(to013, null, true);
//			assertEquals(1, ancestors.size());
//
//			childs = env.getRelatedObjects(to010, null, true);
//			assertEquals(1, childs.size());

		} finally {
			env.deleteObjectType(TYPE_TEST);
			env.deleteObjectType(TYPE_TEST2);
			;
		}
	}

	@Test
	public void testHistoricalData() throws Exception {

		String TYPE_TEST = "Type_tested";
		try {
			env.configurationComplete();
			env.configurationStart();

			ObjectType t1 = env.createObjectType(TYPE_TEST);

			PropertyDescr doubleProp = new PropertyDescr("double", Double.class.getName(), true);
			t1.getPropertyDescriptors().add(doubleProp);

			PropertyDescr intProp = new PropertyDescr("integer", Integer.class.getName(), true);
			t1.getPropertyDescriptors().add(intProp);
			PropertyDescr stringProp = new PropertyDescr("string", String.class.getName(), true);
			t1.getPropertyDescriptors().add(stringProp);

			PropertyDescr longProp = new PropertyDescr("long", Long.class.getName(), true);
			t1.getPropertyDescriptors().add(longProp);

			env.updateObjectType(t1);

			String object010 = "object010";
			ValueTO[] initialValues = new ValueTO[] { new ValueTO(doubleProp.getName(), 0.0),
			        new ValueTO(intProp.getName(), 0), new ValueTO(stringProp.getName(), "0"),
			        new ValueTO(longProp.getName(), 0L) };
			TO to010 = env.createObject(TYPE_TEST, initialValues);

			env.configurationComplete();

			final int HISTORY_SIZE = 1000;
			Date start = new Date();

			for (int i = 0; i < HISTORY_SIZE; i++) {
				ValueTO[] values = { new ValueTO(doubleProp.getName(), i + 0.1), new ValueTO(intProp.getName(), i),
				        new ValueTO(stringProp.getName(), String.valueOf(i)),
				        new ValueTO(longProp.getName(), Long.MAX_VALUE / 2 - i) };
				env.setAllPropertiesValues(to010, values);
				if (i % 1000 == 0) {
					System.out.println("next 1000 records were performed...");
				}
			}
			waitForDbBatchUpdater();

			Date end = new Date();
			// all methods must return specified history size
			int intHistory = env.getHistorySize(to010, intProp.getName());
			assertEquals(HISTORY_SIZE, intHistory);
			int doubleHistory = env.getHistorySize(to010, doubleProp.getName());
			assertEquals(HISTORY_SIZE, doubleHistory);
			int stringHistory = env.getHistorySize(to010, stringProp.getName());
			assertEquals(HISTORY_SIZE, stringHistory);

			Collection<ValueTO> intHistoryPoints = env.getHistory(to010, intProp.getName(), Integer.class);
			assertEquals(HISTORY_SIZE, intHistoryPoints.size());

			Collection<ValueTO> doubleHistoryPoints = env.getHistory(to010, doubleProp.getName(), Double.class);
			assertEquals(HISTORY_SIZE, doubleHistoryPoints.size());

			Collection<ValueTO> stringHistoryPoints = env.getHistory(to010, stringProp.getName(), String.class);
			assertEquals(HISTORY_SIZE, stringHistoryPoints.size());

			Collection<ValueTO> longHistoryPoints = env.getHistory(to010, longProp.getName(), Long.class);
			assertEquals(HISTORY_SIZE, longHistoryPoints.size());

			intHistoryPoints = env.getHistory(to010, intProp.getName(), start, end, Integer.class);
			assertEquals(HISTORY_SIZE, intHistoryPoints.size());

			doubleHistoryPoints = env.getHistory(to010, doubleProp.getName(), start, end, Double.class);
			assertEquals(HISTORY_SIZE, doubleHistoryPoints.size());

			stringHistoryPoints = env.getHistory(to010, stringProp.getName(), start, end, String.class);
			assertEquals(HISTORY_SIZE, stringHistoryPoints.size());

			longHistoryPoints = env.getHistory(to010, longProp.getName(), start, end, Long.class);
			assertEquals(HISTORY_SIZE, longHistoryPoints.size());

			intHistoryPoints = env.getHistory(to010, intProp.getName(), start, HISTORY_SIZE, Integer.class);
			assertEquals(HISTORY_SIZE, intHistoryPoints.size());

			doubleHistoryPoints = env.getHistory(to010, doubleProp.getName(), start, HISTORY_SIZE, Double.class);
			assertEquals(HISTORY_SIZE, doubleHistoryPoints.size());

			stringHistoryPoints = env.getHistory(to010, stringProp.getName(), start, HISTORY_SIZE, String.class);
			assertEquals(HISTORY_SIZE, stringHistoryPoints.size());

			longHistoryPoints = env.getHistory(to010, longProp.getName(), start, HISTORY_SIZE, Long.class);
			assertEquals(HISTORY_SIZE, longHistoryPoints.size());

			intHistoryPoints = env.getHistory(to010, intProp.getName(), start, 5, Integer.class);
			assertEquals(5, intHistoryPoints.size());
			int i = 0;
			for (ValueTO next : intHistoryPoints) {
				assertEquals(i, next.getValue());
				i++;
			}

		} finally {
			env.deleteObjectType(TYPE_TEST);
		}
	}

	@Ignore
	@Test
	// (timeout = 100*100)
	public void testExecuteTimeCreate() throws Throwable {
		/*
		 * UserTransaction tx = (UserTransaction)
		 * ctx.lookup("java://UserTransaction"); tx.begin();
		 */
		try {
			ObjectType t1 = env.createObjectType("Type1");
			PropertyDescr doubleProp = new PropertyDescr("double", Double.class.getName(), true);
			PropertyDescr stringProp = new PropertyDescr("string", String.class.getName(), true);

			PropertyDescr intProp = new PropertyDescr("integer", Integer.class.getName(), true);

			PropertyDescr longProp = new PropertyDescr("long", Long.class.getName(), true);

			t1.getPropertyDescriptors().add(doubleProp);
			t1.getPropertyDescriptors().add(stringProp);
			t1.getPropertyDescriptors().add(intProp);
			t1.getPropertyDescriptors().add(longProp);

			Collection<Long> times = new HashSet<Long>();
			long avg = 0L;
			env.updateObjectType(t1);
			;
			// props to init object
			ValueTO[] initPropVals = new ValueTO[] { new ValueTO(doubleProp.getName(), 0.00),
			        new ValueTO(stringProp.getName(), "value"), new ValueTO(intProp.getName(), 0),
			        new ValueTO(longProp.getName(), 11111111L) };
			long startTotal = System.currentTimeMillis();
			for (int i = 0; i < 100; i++) {
				long start = System.currentTimeMillis();
				TO<?> obj = env.createObject("Type1", initPropVals);
				long end = System.currentTimeMillis();
				times.add(end - start);
				System.err.println("create:" + i + "=" + (end - start) + "ms");
			}
			long endTotal = System.currentTimeMillis();

			System.err.println("min=" + Collections.min(times));
			System.err.println("max=" + Collections.max(times));

			System.err.println("TOTAL=" + (endTotal - startTotal));
		} finally {
			env.deleteObjectType("Type1");
			;
		}
	}

	/*
	 * @Ignore @Test public void testRulesCreateDelete() throws Exception { try
	 * { ObjectType t1 = env.createObjectType("Type1"); PropertyDescr doubleProp
	 * = new PropertyDescr("double", Double.class.getName(), true);
	 * t1.getPropertyDescriptors().add(doubleProp);
	 * 
	 * PropertyDescr src1Prop = new PropertyDescr("source1",
	 * String.class.getName(), true); PropertyDescr src2Prop = new
	 * PropertyDescr("source2", String.class.getName(), true); PropertyDescr
	 * src3Prop = new PropertyDescr("source3", String.class.getName(), true);
	 * PropertyDescr src4Prop = new PropertyDescr("source4",
	 * String.class.getName(), true); t1.getPropertyDescriptors().add(src1Prop);
	 * t1.getPropertyDescriptors().add(src2Prop);
	 * t1.getPropertyDescriptors().add(src3Prop);
	 * t1.getPropertyDescriptors().add(src4Prop);
	 * 
	 * env.updateObjectType(t1);
	 * 
	 * String ruleTypeName = "efficiency"; String source = "source code";
	 * RuleType rtConfig = new RuleType(ruleTypeName, source, "Groovy" , false);
	 * TO<Integer> rtEfficiency = ruleTypeDao.create(rtConfig);
	 * 
	 * long total = 0L; char[] longString = new char[10000];
	 * Arrays.fill(longString, 'a'); String data = new String(longString); for
	 * (int i = 0; i < 100; i++) { long start = System.currentTimeMillis();
	 * String ruleName = "rule_" + i; Rule ri = new Rule(ruleName,
	 * ruleTypeName); TO<Integer> rule = null; rule =
	 * ruleInstanceDao.create(ri); TO<?> obj = env.createObject("testObject" +
	 * i, "Type1"); // env.setPropertyValue(obj, "double", i+0.1);
	 * ruleInstanceDao.setDependProp(rule.getID(), obj, "double"); Rule binding
	 * = new Rule(obj, "ruleSrc1", src1Prop .getName(), LinkType.PROPERTY);
	 * ruleInstanceDao.setSourceProp(rule.getID(), binding); binding = new
	 * Rule(obj, "ruleSrc2", src2Prop.getName(), LinkType.PROPERTY);
	 * ruleInstanceDao.setSourceProp(rule.getID(), binding); binding = new
	 * Rule(obj, "ruleSrc32", src3Prop.getName(), LinkType.PROPERTY);
	 * ruleInstanceDao.setSourceProp(rule.getID(), binding); binding = new
	 * Rule(obj, "ruleSrc4", src4Prop.getName(), LinkType.PROPERTY);
	 * ruleInstanceDao.setSourceProp(rule.getID(), binding); long end =
	 * System.currentTimeMillis(); total = total + (end - start);
	 * System.err.println("create:" + i + "=" + (end - start) + "ms"); }
	 * System.err.println("avg creating :" + total / 100 + "ms"); } finally {
	 * for (TO<?> rule : ruleInstanceDao.getAllRuleInstances()) {
	 * ruleInstanceDao.delete((Integer) rule.getID()); }
	 * env.deleteObjectType("Type1"); } }
	 */

	@Ignore
	@Test
	public void testLoadGetProperty() throws Throwable {
		try {
			String TYPE_TEST = "Type1";
			ObjectType t1 = env.createObjectType(TYPE_TEST);
			t1.getPropertyDescriptors().add(new PropertyDescr("propertyString", String.class.getName()));
			t1.getPropertyDescriptors().add(new PropertyDescr("propertyInt", Integer.class.getName()));
			env.updateObjectType(t1);
			;
			Collection<TO<?>> allObjects = new LinkedList<TO<?>>();

			ValueTO[] values = new ValueTO[] { new ValueTO("propertyString", "value"), new ValueTO("propertyInt", 10) };

			final int COUT_OBJECTS = 1;
			for (int i = 1; i < COUT_OBJECTS + 1; i++) {
				allObjects.add(env.createObject(TYPE_TEST, values));
			}

			long startTime = System.currentTimeMillis();
			for (TO to : allObjects) {
				String value = env.getPropertyValue(to, "propertyString", String.class);
				Integer intValue = env.getPropertyValue(to, "propertyInt", Integer.class);
			}
			long endTime = System.currentTimeMillis();

			long getPropTook = (endTime - startTime);
			System.err.println("Get 2 props :" + getPropTook + " ms has took for " + allObjects.size() + " objects");
			System.err.println("Avg time:" + getPropTook / allObjects.size() + " ms for 2 property");

			startTime = System.currentTimeMillis();
			env.getPropertyValue(allObjects, new String[] { "propertyString", "propertyInt" });
			/*
			 * for (TO to : allObjects) { String value =
			 * env.getFastPropertyValue(to, "propertyString", String.class);
			 * Integer intValue = env.getFastPropertyValue(to, "propertyInt",
			 * Integer.class); }
			 */
			endTime = System.currentTimeMillis();

			long getPropFastTook = (endTime - startTime);
			System.err.println("Get fast 2 props :" + getPropFastTook + " ms has took for " + allObjects.size()
			        + " objects");
			System.err.println("Avg time:" + getPropFastTook / allObjects.size() + " ms for 2 property");

		} finally {
			env.deleteObjectType("Type1");
			;
		}
	}

	@Test
	public void testLoadGetAllProperties() throws Throwable {
		try {
			String TYPE_TEST = "Type1";
			ObjectType t1 = env.createObjectType(TYPE_TEST);
			t1.getPropertyDescriptors().add(new PropertyDescr("propertyString", String.class.getName()));
			t1.getPropertyDescriptors().add(new PropertyDescr("propertyInt", Integer.class.getName()));
			t1.getPropertyDescriptors().add(new PropertyDescr("propertyDouble", Double.class.getName()));
			env.updateObjectType(t1);
			;
			Collection<TO<?>> allObjects = new LinkedList<TO<?>>();

			ValueTO[] values = new ValueTO[] { new ValueTO("propertyString", "value"), new ValueTO("propertyInt", 10),
			        new ValueTO("propertyDouble", 1.05) };

			final int COUT_OBJECTS = 10;
			for (int i = 1; i < COUT_OBJECTS + 1; i++) {
				allObjects.add(env.createObject(TYPE_TEST, values));
			}

			long startTime = System.currentTimeMillis();
			Collection<CollectionTO> props = env.getAllPropertiesValues(allObjects);
			long endTime = System.currentTimeMillis();

			long getPropFastTook = (endTime - startTime);
			System.err.println("Get fast props :" + getPropFastTook + " ms has took for " + allObjects.size()
			        + " objects");
			System.err.println("Avg time:" + getPropFastTook / allObjects.size() + " ms");

		} finally {
			env.deleteObjectType("Type1");
			;
		}
	}

	@Test
	public void testStaticProperties() throws Exception {
		// some use cases for static props
		String TYPE1 = "Type1";
		try {
			ObjectType t1 = env.createObjectType(TYPE1);
			t1.getPropertyDescriptors().add(new PropertyDescr("name", String.class.getName()));

			t1.getPropertyDescriptors().add(new PropertyDescr("color", String.class.getName()));

			t1.getPropertyDescriptors().add(
			        new PropertyDescr("icon_static", null, String.class.getName(), false, false, true));

			env.updateObjectType(t1);
			ObjectType[] types = env.getObjectTypes(new String[] { TYPE1 });
			assertEquals(1, types.length);

			Collection<TO<?>> type1Objects = env.getObjectsByType(TYPE1);
			assertTrue(type1Objects.isEmpty());

			env.setPropertyValue(TYPE1, "icon_static", "icon.jpg");

			String iconValue = env.getPropertyValue(TYPE1, "icon_static", String.class);
			assertEquals("icon.jpg", iconValue);

			try {
				env.setPropertyValue(TYPE1, "color", "red");
				assertTrue(false);
			} catch (PropertyNotFoundException e) {
			}

            try {
                env.setPropertyValue(TYPE1, "icon_static", 123);
                assertTrue("Type check failed", false);
            } catch (UnableToConvertPropertyException e) {
            }

			ValueTO to1Name = new ValueTO("name", "new object 1");
			ValueTO iconStatic = new ValueTO("icon_static", "icon.jpg");
			ValueTO to2Name = new ValueTO("name", "new object 2");
			TO<?> to1 = env.createObject(TYPE1, new ValueTO[] { to1Name });
			try {
				TO<?> to2 = env.createObject(TYPE1, new ValueTO[] { to2Name, iconStatic });
				assertTrue(false);
			} catch (PropertyNotFoundException e) {
			}
			type1Objects = env.getObjectsByType(TYPE1);
			assertEquals(1, type1Objects.size());

			ValueTO[] criteria = new ValueTO[] { new ValueTO("name", TYPE1) };

			type1Objects = env.getObjects(TYPE1, criteria);
			assertEquals(0, type1Objects.size());

			iconValue = env.getPropertyValue(to1, "icon_static", String.class);
			assertEquals("icon.jpg", iconValue);

			Collection<ValueTO> propValues = env.getAllPropertiesValues(to1);
			assertEquals(3, propValues.size());

			long timeOfSet = System.currentTimeMillis();

			ValueTO colorGreen = new ValueTO("color", "green", timeOfSet);
			env.setPropertyValue(to1, "color", colorGreen);

			propValues = env.getAllPropertiesValues(to1);
			assertEquals(3, propValues.size());
			assertTrue(propValues.contains(colorGreen));
			env.deleteObject(to1);

		} finally {
			env.deleteObjectType(TYPE1);
			;
		}
	}

	@Test
	public void testGettingPropDescr() throws Exception {
		String type1 = "Type1";
		try {
			ObjectType t1 = env.createObjectType(type1);

			PropertyDescr comport = new PropertyDescr("comport1", String.class.getName());
			t1.getPropertyDescriptors().add(comport);

			PropertyDescr hostName = new PropertyDescr("hostName", String.class.getName());
			t1.getPropertyDescriptors().add(hostName);

			PropertyDescr hostPort = new PropertyDescr("hostPort", Integer.class.getName());
			t1.getPropertyDescriptors().add(hostPort);

			env.updateObjectType(t1);
			;
			PropertyDescr gotComport = env.getPropertyDescriptor(type1, comport.getName());
			assertEquals(comport, gotComport);

			PropertyDescr gotHostName = env.getPropertyDescriptor(type1, hostName.getName());
			assertEquals(hostName, gotHostName);

			PropertyDescr gotHostPort = env.getPropertyDescriptor(type1, hostPort.getName());
			assertEquals(hostPort, gotHostPort);

			try {
				env.getPropertyDescriptor("blah-blah", "property");
				assertTrue(false);
			} catch (ObjectNotFoundException e) {
			}

		} finally {
			env.deleteObjectType(type1);
			;
		}

	}

	@Test
	public void testGenericExists() throws Exception {
		String typeName = "Type1";
		String propName = "PropName";
		InitialContext ctx = null;
		boolean ex = false;
		try {
			PropertyDescr props = new PropertyDescr(propName, String.class);
			env.deleteObjectType(typeName);
			env.createObjectType(typeName);

			ValueTO[] propArray1 = new ValueTO[] { new ValueTO(propName, "testStr") };

			ObjectType objType = null;
			objType = env.getObjectType(typeName);
			objType.getPropertyDescriptors().add(props);

			env.updateObjectType(objType);
			TO<?> obj = null;
			obj = env.createObject(typeName, propArray1);
			TO<?> objErr = TOFactory.getInstance().EMPTY_TO;

			assertTrue(env.exists(obj));
			try {
				env.exists(null);

			} catch (IllegalInputParameterException iae) {
				ex = true;
			}
			assertTrue(ex);
			ex = false;

			try {
				env.exists(objErr);
			} catch (IllegalInputParameterException iae) {
				ex = true;
			}
			assertTrue(ex);
			ex = false;

			ConsoleApp.stopCache();
			assertTrue(env.exists(obj));

			try {
				env.exists(null);

			} catch (IllegalInputParameterException iae) {
				ex = true;
			}
			assertTrue(ex);
			ex = false;

			try {
				env.exists(objErr);
			} catch (IllegalInputParameterException iae) {
				ex = true;
			}
			assertTrue(ex);
			ex = false;

			ConsoleApp.startCache();
			env.deleteObject(obj);
			assertFalse(env.exists(obj));
			env.deleteObjectType(typeName);
			assertFalse(env.exists(obj));

		} finally {
			env.deleteObjectType(typeName);
		}
	}

	@Test
	public void testGetPropertyValueForCollectionOfObjects() throws Exception {
		String type1 = "Type1";
		try {
			ObjectType t1 = env.createObjectType(type1);
			PropertyDescr comport = new PropertyDescr("comport1", String.class.getName());
			t1.getPropertyDescriptors().add(comport);

			PropertyDescr hostName = new PropertyDescr("hostName", String.class.getName());
			t1.getPropertyDescriptors().add(hostName);

			PropertyDescr hostPort = new PropertyDescr("hostPort", Integer.class.getName());
			t1.getPropertyDescriptors().add(hostPort);

			env.updateObjectType(t1);

			TO<?> newObject = env.createObject(type1, new ValueTO[] { new ValueTO(comport.getName(), "COM3"),
			        new ValueTO(hostName.getName(), "localhost") });

			Collection<TO<?>> objects = new LinkedList<TO<?>>();
			objects.add(newObject);
			// checks non-existent property
			Collection<CollectionTO> propValues = env.getPropertyValue(objects,
			        new String[] { "non-existing-property" });
			assertNotNull(propValues);
			assertEquals(1, propValues.size());
			assertTrue(propValues.iterator().next().getPropValues().isEmpty());

			// getting one of existing properties
			propValues = env.getPropertyValue(objects, new String[] { comport.getName() });
			assertNotNull(propValues);
			assertEquals(1, propValues.size());
			CollectionTO valueBundle = propValues.iterator().next();
			assertEquals(1, valueBundle.getPropValues().size());
			ValueTO comportValue = valueBundle.getSinglePropValue(comport.getName());
			assertEquals("COM3", comportValue.getValue());

			// no cache available
			ConsoleApp.stopCache();
			try {
				// single prop query
				propValues = env.getPropertyValue(objects, new String[] { comport.getName() });
				assertNotNull(propValues);
				assertEquals(1, propValues.size());
				valueBundle = propValues.iterator().next();
				assertEquals(1, valueBundle.getPropValues().size());
				comportValue = valueBundle.getSinglePropValue(comport.getName());
				assertEquals("COM3", comportValue.getValue());

				// multiple props query
				propValues = env.getPropertyValue(objects, new String[] { comport.getName(), hostName.getName() });
				assertNotNull(propValues);
				assertEquals(1, propValues.size());
				valueBundle = propValues.iterator().next();
				assertEquals(2, valueBundle.getPropValues().size());

				comportValue = valueBundle.getSinglePropValue(comport.getName());
				assertEquals("COM3", comportValue.getValue());

				ValueTO hostValue = valueBundle.getSinglePropValue(hostName.getName());
				assertEquals("localhost", hostValue.getValue());

			} finally {
				ConsoleApp.startCache();
			}

			propValues = env.getAllPropertiesValues(objects);
			assertNotNull(propValues);
			assertEquals(1, propValues.size());

		} finally {
			env.deleteObjectType(type1);
		}
	}

	@Test
	public void testSetGetPropertiesWithNullValues() throws Exception {
		try {
			ObjectType t1 = env.createObjectType("Type1");
			t1.getPropertyDescriptors().add(new PropertyDescr("comport", String.class.getName()));
			t1.getPropertyDescriptors().add(new PropertyDescr("historical", Integer.class.getName(), true));

			t1.getPropertyDescriptors().add(new PropertyDescr("collection", Integer.class.getName(), false, true));

			env.updateObjectType(t1);

			TO<?> obj = env.createObject("Type1");

			ValueTO p1 = new ValueTO("comport", "COM3");

			// checks the initital state of property after creation
			String comport = env.getPropertyValue(obj, p1.getPropertyName(), String.class);
			assertNull(comport);
			Collection<ValueTO> objValues = env.getAllPropertiesValues(obj);
			assertEquals(4, objValues.size());

			// sets new value
			env.setPropertyValue(obj, p1.getPropertyName(), p1.getValue());
			comport = env.getPropertyValue(obj, p1.getPropertyName(), String.class);
			assertEquals(p1.getValue(), comport);
			objValues = env.getAllPropertiesValues(obj);
			assertEquals(4, objValues.size());

			// sets null value
			env.setPropertyValue(obj, p1.getPropertyName(), null);
			comport = env.getPropertyValue(obj, p1.getPropertyName(), String.class);
			assertNull(comport);
			objValues = env.getAllPropertiesValues(obj);
			assertEquals(4, objValues.size());

			// again sets not null property value , but value record already
			// created in db
			env.setPropertyValue(obj, p1.getPropertyName(), p1.getValue());
			comport = env.getPropertyValue(obj, p1.getPropertyName(), String.class);
			assertEquals(p1.getValue(), comport);
			objValues = env.getAllPropertiesValues(obj);
			assertEquals(4, objValues.size());

			// test with ValueTO as value with ValueTO.value = null
			p1.setValue(null);
			env.setPropertyValue(obj, p1.getPropertyName(), p1);
			comport = env.getPropertyValue(obj, p1.getPropertyName(), String.class);
			assertNull(comport);
			objValues = env.getAllPropertiesValues(obj);
			assertEquals(4, objValues.size());

			// historical props test
			ValueTO p2 = new ValueTO("historical", 1);

			// checks that history is not saved on nulls
			Integer historical = env.getPropertyValue(obj, p2.getPropertyName(), Integer.class);
			assertNull(historical);

			// sets property value
			env.setPropertyValue(obj, p2.getPropertyName(), p2.getValue());
			waitForDbBatchUpdater();

			Collection<ValueTO> history = env.getHistory(obj, p2.getPropertyName(), Integer.class);
			assertNotNull(history);
			// history must be increased for one point
			assertEquals(1, history.size());

			env.setPropertyValue(obj, p2.getPropertyName(), p2.getValue());

			waitForDbBatchUpdater();

			historical = env.getPropertyValue(obj, p2.getPropertyName(), Integer.class);
			assertEquals(p2.getValue(), historical);

			history = env.getHistory(obj, p2.getPropertyName(), Integer.class);
			assertNotNull(history);
			// history must be increased for one more point
			assertEquals(2, history.size());

			// sets null value
			env.setPropertyValue(obj, p2.getPropertyName(), null);
			// property value is null now
			historical = env.getPropertyValue(obj, p2.getPropertyName(), Integer.class);
			assertNull(historical);

			// history does not contain null values
			history = env.getHistory(obj, p2.getPropertyName(), Integer.class);
			assertNotNull(history);
			assertEquals(2, history.size());

			// checks collection property
			ValueTO p3 = new ValueTO("collection", 1);
			// checks the initital state of property after creation
			Collection<Integer> collection = env.getPropertyValue(obj, p3.getPropertyName(), Collection.class);
			assertNotNull(collection);
			objValues = env.getAllPropertiesValues(obj);
			assertEquals(4, objValues.size());

			Collection<Integer> ints = new LinkedList<Integer>();
			ints.add(1);
			ints.add(2);
			ints.add(null);

			env.setPropertyValue(obj, p3.getPropertyName(), ints);
			collection = env.getPropertyValue(obj, p3.getPropertyName(), Collection.class);
			assertNotNull(collection);
			assertEquals(3, collection.size());

			env.addPropertyValue(obj, p3.getPropertyName(), 3);
			collection = env.getPropertyValue(obj, p3.getPropertyName(), Collection.class);
			assertNotNull(collection);
			assertEquals(4, collection.size());
            //TODO : Null elements support?
//			env.removePropertyValue(obj, p3.getPropertyName(), null);
//			collection = env.getPropertyValue(obj, p3.getPropertyName(), Collection.class);
//			assertNotNull(collection);
//			assertEquals(3, collection.size());
//
//			env.addPropertyValue(obj, p3.getPropertyName(), null);
//			collection = env.getPropertyValue(obj, p3.getPropertyName(), Collection.class);
//			assertNotNull(collection);
//			assertEquals(4, collection.size());

		} catch (Exception e) {
			fail(e.getMessage());
		} finally {
			env.deleteObjectType("Type1");
		}

	}

	@Test
	@Ignore
	public void testCRERunningRulesWithComparingTimestamps() throws Exception {
		long timeSetup = System.currentTimeMillis() - 1000;
		// for (int i = 1;i<2;i++) {
		ValueTO status = new ValueTO("status", 1);
		ValueTO lastValue = new ValueTO("lastValue", 55.02, timeSetup);
		env.setAllPropertiesValues(TOFactory.getInstance().loadTO("SENSOR:" + 104), new ValueTO[] { lastValue, status });
		// }
	}

	@Test
	public void testGettingNumAlertsProperty() throws Exception {
		String type1 = "Type1";
		try {
			ObjectType t1 = env.createObjectType(type1);
			TO<?> obj1 = env.createObject(t1.getName());
			Collection<TO<?>> objIds = new LinkedList<TO<?>>();
			objIds.add(obj1);

			Collection<CollectionTO> values = env.getPropertyValue(objIds, new String[] { "numAlerts" });
			assertEquals(1, values.size());
		} finally {
			env.deleteObjectType(type1);
			;
		}
	}

	/**
	 * Tests the case when write/read operations are executed in single
	 * transaction All changes are made by "write" method must have to be
	 * visible for any method in the same transaction
	 * 
	 * @throws Exception
	 */
	@Test
	@Ignore
	public void testTransactionPropagating() throws Exception {
		UserTransaction ut = (UserTransaction) ConsoleApp.ctx.lookup("UserTransaction");
		ut.begin();
		try {
			ObjectType type = env.createObjectType("test type");
			Set<String> gTypes = new HashSet<String>(Arrays.asList(env.getObjectTypes()));
			assertTrue(gTypes.contains("test type"));
		} finally {
			ut.rollback();
		}

	}

	@Test
	public void testAncestorsWhenDeletingObjectType() throws Exception {
		String TYPE_TEST = "Type_tested";
		String TYPE_TEST2 = "Type_tested2";
		try {
			// init generic
			ObjectType t1 = env.createObjectType(TYPE_TEST);
			ObjectType t2 = env.createObjectType(TYPE_TEST2);

			TO to010 = env.createObject(TYPE_TEST);
			TO to011 = env.createObject(TYPE_TEST);
			TO to012 = env.createObject(TYPE_TEST);
			TO to013 = env.createObject(TYPE_TEST2);
			TO to014 = env.createObject(TYPE_TEST2);
			TO to015 = env.createObject(TYPE_TEST2);

			env.setRelation(to010, to011);
			env.setRelation(to011, to013);
			env.setRelation(to013, to014);
			env.setRelation(to014, to015);
			env.setRelation(to015, to012);

			Collection<TO<?>> children;
//                    = env.getRelatedObjects(to010, null, true);
//			assertEquals(5, children.size());

			Collection<TO<?>> ancestors;
//                    = env.getRelatedObjects(to015, null, false);
//			assertEquals(4, ancestors.size());

			env.deleteObjectType(TYPE_TEST);

//			ancestors = env.getRelatedObjects(to015, null, false);
//			assertEquals(2, ancestors.size());
//
//			ancestors = env.getRelatedObjects(to014, null, false);
//			assertEquals(1, ancestors.size());
//
//			ancestors = env.getRelatedObjects(to013, null, false);
//			assertEquals(0, ancestors.size());
//
//			ancestors = env.getRelatedObjects(to012, null, false);
//			assertEquals(0, ancestors.size());

		} finally {
			env.deleteObjectType(TYPE_TEST);
			env.deleteObjectType(TYPE_TEST2);
		}
	}

	@Test
	public void testSettingPropertyValueToNullForTheFirstTime() throws EnvException {
		try {
			ObjectType t1 = env.createObjectType("Type1");
			t1.getPropertyDescriptors().add(new PropertyDescr("string", String.class.getName()));
			t1.getPropertyDescriptors().add(new PropertyDescr("int", Integer.class.getName()));
			t1.getPropertyDescriptors().add(new PropertyDescr("date", Date.class.getName()));
			t1.getPropertyDescriptors().add(new PropertyDescr("long", Long.class.getName()));
			t1.getPropertyDescriptors().add(new PropertyDescr("double", Double.class.getName()));
			t1.getPropertyDescriptors().add(new PropertyDescr("to", TO.class.getName()));
			t1.getPropertyDescriptors().add(new PropertyDescr("propertyto", PropertyTO.class.getName()));

			env.updateObjectType(t1);

			TO<?> obj = env.createObject("Type1");
			env.setPropertyValue(obj, "string", null);
			String sval = env.getPropertyValue(obj, "string", String.class);
			assertNull(sval);

			env.setPropertyValue(obj, "int", null);
			Integer ival = env.getPropertyValue(obj, "int", Integer.class);
			assertNull(ival);

			env.setPropertyValue(obj, "date", null);
			Date dateval = env.getPropertyValue(obj, "date", Date.class);
			assertNull(dateval);

			env.setPropertyValue(obj, "long", null);
			Long lval = env.getPropertyValue(obj, "long", Long.class);
			assertNull(lval);

			env.setPropertyValue(obj, "double", null);
			Double dval = env.getPropertyValue(obj, "double", Double.class);
			assertNull(dval);

			env.setPropertyValue(obj, "to", null);
			TO link = env.getPropertyValue(obj, "to", TO.class);
			assertNull(link);

			env.setPropertyValue(obj, "propertyto", null);
			PropertyTO plink = env.getPropertyValue(obj, "propertyto", PropertyTO.class);
			assertNull(plink);

		} finally {
			env.deleteObjectType("Type1");
			;
		}

	}

	@Test(timeout = 6000)
	@Ignore
	public void testBatchCreateOfObjects() throws EnvException {
		try {
			ObjectType t1 = env.createObjectType("Type1");
			t1.getPropertyDescriptors().add(new PropertyDescr("comport", String.class.getName()));
			t1.getPropertyDescriptors().add(new PropertyDescr("pan", Integer.class.getName()));

			t1.getPropertyDescriptors().add(new PropertyDescr("dt", Date.class.getName()));

			env.updateObjectType(t1);

			ValueTO p1 = new ValueTO("comport", "COM3");
			ValueTO p2 = new ValueTO("pan", 1000);
			Date currentDate = new Date();
			ValueTO p3 = new ValueTO("dt", currentDate);
			final ValueTO[] initialState = new ValueTO[] { p1, p2, p3 };

			Executor loader = Executors.newPooledExecutor(4);
			List<Action> actionList = new LinkedList<Action>();
			long start = System.currentTimeMillis();
			for (int j = 0; j < 4; j++) {
				actionList.add(new Action() {
					@Override
					public void execute() {
						for (int i = 0; i < 100; i++) {
							try {
								TO<?> obj = env.createObject("Type1", initialState);
							} catch (EnvException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					}
				});
			}
			loader.doAction(actionList, true);
			System.out.println("testBatchCreateOfObjects ms: " + (System.currentTimeMillis() - start));

			assertEquals(400, env.getObjectsByType("Type1").size());

		} finally {
			env.deleteObjectType("Type1");
		}

	}

	@Test(timeout = 25000)
	@Ignore
	public void testBatchCreateOfObjectsWithRelations() throws EnvException {
		try {
			ObjectType t1 = env.createObjectType("Type1");

			long startTotal = System.currentTimeMillis();
			for (int i = 0; i < 10; i++) {
				TO to = env.createObject("Type1");
				for (int j = 0; j < 5; j++) {
					TO to1 = env.createObject("Type1");
					env.setRelation(to, to1);
					for (int k = 0; k < 5; k++) {
						TO to2 = env.createObject("Type1");
						env.setRelation(to1, to2);
					}
				}
			}
			System.out.println("testBatchCreateOfObjectsWithRelations total time ms: "
			        + (System.currentTimeMillis() - startTotal));

		} finally {
			env.deleteObjectType("Type1");
		}

	}

	@Test
	public void testConfigurationStartCompletePair() throws Exception {
		try {
			env.configurationStart();

			ObjectType t1 = env.createObjectType("Type1");
			t1.getPropertyDescriptors().add(new PropertyDescr("name", String.class.getName()));
			t1.getPropertyDescriptors().add(new PropertyDescr("x", Double.class.getName()));
			t1.getPropertyDescriptors().add(new PropertyDescr("y", Double.class.getName()));

			env.updateObjectType(t1);

			TO<?> obj1 = env.createObject("Type1");
			TO<?> obj2 = env.createObject("Type1");
			TO<?> obj3 = env.createObject("Type1");

			long timestamp1 = new Date().getTime();
			ValueTO vName = new ValueTO("name", "name1", timestamp1);
			ValueTO vX = new ValueTO("x", 1.00, timestamp1);
			ValueTO vY = new ValueTO("y", 1.00, timestamp1);
			for (Integer i = 0; i < 100; i++) {
				timestamp1 = new Date().getTime();
				vName.setValue("name" + i.toString());
				vName.setTimeStamp(timestamp1);
				vX.setValue(i + 0.1);
				vX.setTimeStamp(timestamp1);
				vY.setValue(i + 0.1);
				vY.setTimeStamp(timestamp1);

				env.setPropertyValue(obj1, "name", vName);
				env.setPropertyValue(obj1, "x", vX);
				env.setPropertyValue(obj1, "y", vY);
				env.setPropertyValue(obj2, "name", vName);
				env.setPropertyValue(obj2, "x", vX);
				env.setPropertyValue(obj2, "y", vY);
				env.setPropertyValue(obj3, "name", vName);
				env.setPropertyValue(obj3, "x", vX);
				env.setPropertyValue(obj3, "y", vY);
			}

			ValueTO gotName = env.getPropertyValue(obj1, "name", ValueTO.class);
			assertEquals(timestamp1, gotName.getTimeStamp());

			ValueTO gotX = env.getPropertyValue(obj1, "x", ValueTO.class);
			assertEquals(timestamp1, gotX.getTimeStamp());

			ValueTO gotY = env.getPropertyValue(obj1, "y", ValueTO.class);
			assertEquals(timestamp1, gotY.getTimeStamp());

			env.configurationComplete();

			gotName = env.getPropertyValue(obj1, "name", ValueTO.class);
			assertEquals(timestamp1, gotName.getTimeStamp());

			gotX = env.getPropertyValue(obj1, "x", ValueTO.class);
			assertEquals(timestamp1, gotX.getTimeStamp());

			gotY = env.getPropertyValue(obj1, "y", ValueTO.class);
			assertEquals(timestamp1, gotY.getTimeStamp());

		} finally {
			env.deleteObjectType("Type1");
		}

		env.configurationStart();

		env.configurationComplete();

	}

	@Test
	public void testGetAllPropertiesValuesDisabledCache() throws Exception {
		ObjectType t1 = env.createObjectType("Type1");

		try {
			t1.getPropertyDescriptors().add(new PropertyDescr("name", String.class.getName()));

			env.updateObjectType(t1);

			long timestamp1 = new Date().getTime();
			ValueTO vName = new ValueTO("name", "name1", timestamp1);

			TO<?> obj1 = env.createObject("Type1", new ValueTO[] { vName });

			ConsoleApp.stopCache();

			try {
				Collection<ValueTO> values = env.getAllPropertiesValues(obj1);

				try {
					values.clear();
				} catch (UnsupportedOperationException e) {
					assertTrue("getAllPropertiesValues should return modifiable collection!", false);
				}
			} finally {
				ConsoleApp.startCache();
			}

		} finally {
			env.deleteObjectType("Type1");
		}
	}

	@Test(expected = ObjectNotFoundException.class)
	public void testGetChildrenNonExistingParentFailure() throws ObjectNotFoundException {
		env.getChildren(TOFactory.getInstance().loadTO("TYPE:1"));
	}

	@Test(expected = ObjectNotFoundException.class)
	public void testGetChildrenTypeNameFilterNonExistingParentFailure() throws ObjectNotFoundException {
		env.getChildren(TOFactory.getInstance().loadTO("TYPE:1"), "FILTER_BY_TYPE");
	}

	@Test(expected = ObjectNotFoundException.class)
	public void testGetChildrenTypeNameFilterNullNonExistingParentFailure() throws ObjectNotFoundException {
		env.getChildren(TOFactory.getInstance().loadTO("TYPE:1"), null);
	}

	@Test(expected = ObjectNotFoundException.class)
	public void testSetPropertyValueNonExistingObjectFailure() throws EnvException {
		env.setPropertyValue(TOFactory.getInstance().loadTO("TYPE:1"), "property", "value");
	}

	@Test(expected = PropertyNotFoundException.class)
	public void testSetPropertyValueNonExistingPropertyFailure() throws EnvException {
		ObjectType t1 = env.createObjectType("Type1");

		try {
			TO<?> obj1 = env.createObject("Type1");

			env.setPropertyValue(obj1, "property", "value");

		} finally {
			env.deleteObjectType("Type1");
		}
	}

	@Test(expected = ObjectNotFoundException.class)
	public void testSetAllPropertiesValuesNonExistingObjectFailure() throws EnvException {
		env.setAllPropertiesValues(TOFactory.getInstance().loadTO("TYPE:1"), new ValueTO[] { new ValueTO("property",
		        "value") });
	}

	@Test(expected = PropertyNotFoundException.class)
	public void testSetAllPropertiesValuesNonExistingPropertyFailure() throws EnvException {
		ObjectType t1 = env.createObjectType("Type1");

		try {
			TO<?> obj1 = env.createObject("Type1");

			env.setAllPropertiesValues(obj1, new ValueTO[] { new ValueTO("property", "value") });

		} finally {
			env.deleteObjectType("Type1");
		}
	}

	@Test
	public void testSetRandomProp() throws Exception {
		Collection<TO<?>> sensors = env.getObjectsByType("WSNSENSOR");

		int i = 1;
		for (TO sensor : sensors) {
			if (i % 2 == 0)
				env.setPropertyValue(sensor, "lastValue", 111.777);
			i++;
		}

	}

	@Test
	public void testCustomSetProperty() throws Exception {
		String type1 = "Type1";
		try {
			ObjectType t1 = env.createObjectType(type1);
			t1.getPropertyDescriptors().add(new PropertyDescr("name", String.class.getName()));
			env.updateObjectType(t1);

			ValueTO[] valueArr = { new ValueTO("name", "Test query 1") };
			TO<?> cq = env.createObject("Type1", valueArr);

			Thread.sleep(2000);

			ValueTO[] newValueArr = { new ValueTO("name", "Test query 2") };
			env.setAllPropertiesValues(cq, newValueArr);
		} finally {
			env.deleteObjectType(type1);
		}

	}

	@Test
	@Ignore
	// only manual run
	public void testCreateMultipleObjects() throws Exception {
		String type1 = "Type1";
		try {
			ObjectType t1 = env.createObjectType(type1);
			t1.getPropertyDescriptors().add(new PropertyDescr("name", String.class.getName()));
			t1.getPropertyDescriptors().add(new PropertyDescr("x", Double.class.getName()));
			t1.getPropertyDescriptors().add(new PropertyDescr("link", TO.class.getName()));

			env.updateObjectType(t1);

			env.configurationStart();

			long start = System.currentTimeMillis();

			ValueTO[] valueArr = { new ValueTO("name", "Test query 1"), new ValueTO("x", 1.0) };
			List<TO> objIds = new LinkedList<TO>();
			TO last = null;
			for (int i = 0; i < 20; i++) {
				TO<?> cq = env.createObject("Type1", valueArr);
				objIds.add(cq);
				last = cq;
			}

			System.out.println(System.currentTimeMillis() - start);

			env.configurationComplete();

			for (TO to : objIds) {
				for (int i = 0; i < 2; i++) {
					env.setPropertyValue(to, "link", last);
				}
			}

		} finally {
			env.deleteObjectType(type1);
		}

	}

	@Test
	public void testDeleteObjectCascadeAll() throws Exception {
		String type1 = "Type1";
		try {
			ObjectType t1 = env.createObjectType(type1);
			TO<?> obj1 = env.createObject(type1);
			TO<?> obj2 = env.createObject(type1);
			TO<?> obj3 = env.createObject(type1);
			TO<?> obj4 = env.createObject(type1);
			TO<?> obj5 = env.createObject(type1);
			TO<?> obj6 = env.createObject(type1);
			TO<?> obj7 = env.createObject(type1);

			List<Relation> relationSet = new LinkedList<Relation>();
			relationSet.add(new Relation(obj1, obj2));
			relationSet.add(new Relation(obj2, Arrays.asList(obj3, obj5)));
			relationSet.add(new Relation(obj3, obj4));
			relationSet.add(new Relation(obj5, Arrays.asList(obj6, obj7)));
			env.setRelation(relationSet);

			env.deleteObject(obj1, -1);

			assertFalse(env.exists(obj1));
			assertFalse(env.exists(obj2));
			assertFalse(env.exists(obj3));
			assertFalse(env.exists(obj4));
			assertFalse(env.exists(obj5));
			assertFalse(env.exists(obj6));
			assertFalse(env.exists(obj7));

			assertTrue(env.getObjectsByType(type1).isEmpty());

		} finally {
			env.deleteObjectType(type1);
		}

	}

	@Test
	public void testDeleteObjectCascadeOneLevel() throws Exception {
		String type1 = "Type1";
		try {
			ObjectType t1 = env.createObjectType(type1);
			TO obj1 = env.createObject(type1);
			TO obj2 = env.createObject(type1);
			TO obj3 = env.createObject(type1);
			TO obj4 = env.createObject(type1);
			TO obj5 = env.createObject(type1);
			TO obj6 = env.createObject(type1);

			List<Relation> relationSet = new LinkedList<Relation>();
			relationSet.add(new Relation(obj1, obj2));
			relationSet.add(new Relation(obj1, obj3));
			relationSet.add(new Relation(obj1, obj4));
			relationSet.add(new Relation(obj2, obj5));
			relationSet.add(new Relation(obj3, obj6));
			env.setRelation(relationSet);

			env.deleteObject(obj1, 1);

			assertFalse(env.exists(obj1));
			assertFalse(env.exists(obj2));
			assertFalse(env.exists(obj3));
			assertFalse(env.exists(obj4));
			assertTrue(env.exists(obj5));
			assertTrue(env.exists(obj6));

			assertEquals(2, env.getObjectsByType(type1).size());

			assertTrue(env.getParents(obj5).isEmpty());
			assertTrue(env.getParents(obj6).isEmpty());
		} finally {
			env.deleteObjectType(type1);
		}
	}

	@Test
	public void testDeleteObjectCascadeZeroLevel() throws Exception {
		String type1 = "Type1";
		try {
			ObjectType t1 = env.createObjectType(type1);
			TO obj1 = env.createObject(type1);
			TO obj2 = env.createObject(type1);
			TO obj3 = env.createObject(type1);
			TO obj4 = env.createObject(type1);
			TO obj5 = env.createObject(type1);
			TO obj6 = env.createObject(type1);

			List<Relation> relationSet = new LinkedList<Relation>();
			relationSet.add(new Relation(obj1, obj2));
			relationSet.add(new Relation(obj1, obj3));
			relationSet.add(new Relation(obj2, obj4));
			relationSet.add(new Relation(obj4, obj5));
			relationSet.add(new Relation(obj4, obj6));
			env.setRelation(relationSet);

			env.deleteObject(obj1, 0);

			assertFalse(env.exists(obj1));
			assertTrue(env.exists(obj2));
			assertTrue(env.exists(obj3));
			assertTrue(env.exists(obj4));
			assertTrue(env.exists(obj5));
			assertTrue(env.exists(obj6));

			assertEquals(5, env.getObjectsByType(type1).size());

			assertTrue(env.getParents(obj2).isEmpty());
			assertEquals(0, env.getParents(obj3).size());
			assertEquals(1, env.getParents(obj4).size());

		} finally {
			env.deleteObjectType(type1);
		}
	}

	@Test(expected = ObjectNotFoundException.class)
	public void testDeleteOriginalObjectByInfectedTO() throws Exception {
		String type1 = "Type1";
		try {
			ObjectType t1 = env.createObjectType(type1);
			TO obj1 = env.createObject(type1);

			TO fake = TOFactory.getInstance().loadTO("Type2:" + obj1.getID());
			env.deleteObject(fake);

		} finally {
			env.deleteObjectType(type1);
		}
	}

	@Test
	public void testDeleteObjectProperly() throws Exception {
		String type1 = "Type1";
		try {
			ObjectType t1 = env.createObjectType(type1);
			TO obj1 = env.createObject(type1);

			env.deleteObject(obj1);

			assertFalse(env.exists(obj1));
			;

		} finally {
			env.deleteObjectType(type1);
		}
	}

	@Test
	public void testSetAllPropertiesValuesAndTagsSuccess() throws Exception {
		String type1 = "Type1";
		try {
			ObjectType t1 = env.createObjectType(type1);
			PropertyDescr namePd = new PropertyDescr("name", String.class);
			namePd.getTagDescriptors().add(new TagDescriptor("encoding", String.class.getName()));
			t1.getPropertyDescriptors().add(namePd);
			env.updateObjectType(t1);

			Collection<ValueTO> values = CollectionUtils.newList();
			values.add(new ValueTO("name", "new name"));

			Collection<Tag> tags = CollectionUtils.newList();
			tags.add(new Tag("name", "encoding", "UTF8"));

			TO obj1 = env.createObject(type1);
			env.setAllPropertiesValues(obj1, values, tags);
			Thread.sleep(50);

			assertEquals("new name", env.getPropertyValue(obj1, "name", String.class));
			assertEquals("UTF8", env.getTagValue(obj1, "name", "encoding", String.class));
			assertEquals(1, env.getAllTags(obj1, "name").size());

		} finally {
			env.deleteObjectType(type1);
		}
	}

	@Test
	public void testSetAllPropertiesValuesWithNoTagsSpecifiedSuccess() throws Exception {
		String type1 = "Type1";
		try {
			ObjectType t1 = env.createObjectType(type1);
			PropertyDescr namePd = new PropertyDescr("name", String.class);
			namePd.getTagDescriptors().add(new TagDescriptor("encoding", String.class.getName()));
			t1.getPropertyDescriptors().add(namePd);
			env.updateObjectType(t1);

			Collection<ValueTO> values = CollectionUtils.newList();
			values.add(new ValueTO("name", "new name"));

			Collection<Tag> tags = CollectionUtils.newList();

			TO obj1 = env.createObject(type1);
			env.setAllPropertiesValues(obj1, values, tags);
			Thread.sleep(50);

			assertEquals("new name", env.getPropertyValue(obj1, "name", String.class));
			assertNull(env.getTagValue(obj1, "name", "encoding", String.class));
			assertEquals(1, env.getAllTags(obj1, "name").size());

		} finally {
			env.deleteObjectType(type1);
		}
	}

	@Test
	public void testSetAllPropertiesValuesWithNoValuesSpecifiedSuccess() throws Exception {
		String type1 = "Type1";
		try {
			ObjectType t1 = env.createObjectType(type1);
			PropertyDescr namePd = new PropertyDescr("name", String.class);
			namePd.getTagDescriptors().add(new TagDescriptor("encoding", String.class.getName()));
			t1.getPropertyDescriptors().add(namePd);
			env.updateObjectType(t1);

			Collection<ValueTO> values = CollectionUtils.newList();

			Collection<Tag> tags = CollectionUtils.newList();
			tags.add(new Tag("name", "encoding", "UTF8"));

			TO obj1 = env.createObject(type1);
			env.setAllPropertiesValues(obj1, values, tags);
			Thread.sleep(50);

			assertNull(env.getPropertyValue(obj1, "name", String.class));
			assertEquals("UTF8", env.getTagValue(obj1, "name", "encoding", String.class));
			assertEquals(1, env.getAllTags(obj1, "name").size());

		} finally {
			env.deleteObjectType(type1);
		}
	}

	@Test(expected = PropertyNotFoundException.class)
	public void testSetAllPropertiesValuesButWrongTagFailure() throws Exception {
		String type1 = "Type1";
		try {
			ObjectType t1 = env.createObjectType(type1);
			PropertyDescr namePd = new PropertyDescr("name", String.class);
			namePd.getTagDescriptors().add(new TagDescriptor("encoding", String.class.getName()));
			t1.getPropertyDescriptors().add(namePd);
			env.updateObjectType(t1);

			Collection<ValueTO> values = CollectionUtils.newList();
			values.add(new ValueTO("name", "new name"));

			Collection<Tag> tags = CollectionUtils.newList();
			tags.add(new Tag("name1", "encoding", "UTF8"));

			TO obj1 = env.createObject(type1);
			try {
				env.setAllPropertiesValues(obj1, values, tags);
				Thread.sleep(50);
			} finally {
				assertNull(env.getPropertyValue(obj1, "name", String.class));
				assertNull(env.getTagValue(obj1, "name", "encoding", String.class));
			}

		} finally {
			env.deleteObjectType(type1);
		}
	}

	@Ignore
	// ALERTs now can have tags
	@Test(expected = IllegalInputParameterException.class)
	public void testSetAllPropertiesValuesAndTagsOnAlertFailure() throws Exception {
		Collection<ValueTO> values = CollectionUtils.newList();
		values.add(new ValueTO("name", "new name"));

		Collection<Tag> tags = CollectionUtils.newList();
		tags.add(new Tag("name1", "encoding", "UTF8"));

		env.setAllPropertiesValues(TOFactory.getInstance().loadTO("ALERT:1"), values, tags);
	}

	@Test
	public void testQueryObjects() throws Exception {
		String TYPE_TEST = "Type_tested";
		try {
			ObjectType t1 = env.createObjectType(TYPE_TEST);
			PropertyDescr[] props = new PropertyDescr[] { new PropertyDescr("link1", TO.class.getName()),
			        new PropertyDescr("link2", TO.class.getName()) };
			for (PropertyDescr pd : props) {
				t1.getPropertyDescriptors().add(pd);
			}
			env.updateObjectType(t1);

			// by example is Alert priority MAJOR
			TO to1 = env.createObject(TYPE_TEST);
			// this is alert type 1
			TO to2 = env.createObject(TYPE_TEST);
			// alert type 2
			TO to3 = env.createObject(TYPE_TEST);

			// alerts of different types
			TO to4 = env.createObject(TYPE_TEST);
			TO to5 = env.createObject(TYPE_TEST);
			TO to6 = env.createObject(TYPE_TEST);

			// alert types refers to priority MAJOR
			env.setPropertyValue(to2, "link1", to1);
			env.setPropertyValue(to3, "link1", to1);

			// alerts refer to their alert types
			env.setPropertyValue(to4, "link2", to2);
			env.setPropertyValue(to5, "link2", to2);
			env.setPropertyValue(to6, "link2", to3);

			// incoming traverse only
			QueryDescription qd = new QueryDescription(2);
			// begin quering from
			qd.startQueryFrom(to1);

			// go through propery name link1 at first level , don't include
			// matching object to result
			qd.addPath(1, new Path(Relations.named("link1"), Direction.INCOMING, false));
			// go through propery name link2 at second level and include
			// matching objects to result
			qd.addPath(2, new Path(Relations.named("link2"), Direction.INCOMING, true));

			Collection<TO<?>> objects = env.getObjects(qd);
			assertFalse(objects.isEmpty());
			assertEquals(3, objects.size());
			assertTrue(objects.contains(to4));
			assertTrue(objects.contains(to5));
			assertTrue(objects.contains(to6));

			// try outgoing traverse
			// can be described in dot notation : to6.link2.link1
			qd = new QueryDescription(2);
			// begin quering from alert which refers alert type which refers
			// priority
			qd.startQueryFrom(to6);

			// go through propery name link1 at first level , don't include
			// matching object to result
			qd.addPath(1, new Path(Relations.named("link2"), Direction.OUTGOING, false));
			// go through propery name link2 at second level and include
			// matching objects to result
			qd.addPath(2, new Path(Relations.named("link1"), Direction.OUTGOING, true));

			objects = env.getObjects(qd);
			assertFalse(objects.isEmpty());
			assertEquals(1, objects.size());
			assertTrue(objects.contains(to1));

		} finally {
			env.deleteObjectType(TYPE_TEST);
		}
	}

	@Test
	public void testObjectTypeNameCaseInsensitivity() throws Exception {
		String TYPE_TEST = "TYPE1";
		String TYPE1 = "type1";
		String TYPE2 = "TyPe1";
		String TYPE3 = "tyPe1";
		try {
			ObjectType t1 = env.createObjectType(TYPE_TEST);

			TO to1 = env.createObject(TYPE_TEST);

			Collection<TO<?>> objects = env.getObjectsByType(TYPE_TEST);
			assertEquals(TYPE_TEST, objects.iterator().next().getTypeName());

			objects = env.getObjectsByType(TYPE1);
			assertEquals(TYPE_TEST, objects.iterator().next().getTypeName());

			objects = env.getObjectsByType(TYPE2);
			assertEquals(TYPE_TEST, objects.iterator().next().getTypeName());

			objects = env.getObjectsByType(TYPE3);
			assertEquals(TYPE_TEST, objects.iterator().next().getTypeName());
		} finally {
			env.deleteObjectType(TYPE_TEST);
		}
	}

	@Test
	public void testObjectRelationTypeNameCaseInsensitivity() throws Exception {
		String PARENT_TYPE = "PARENT";
		String CHILD_TYPE = "CHILD";
		try {
			ObjectType tParent = env.createObjectType(PARENT_TYPE);
			ObjectType tChild = env.createObjectType(CHILD_TYPE);

			TO parent = env.createObject(PARENT_TYPE);
			TO child = env.createObject(CHILD_TYPE);

			env.setRelation(parent, child);

			assertEquals(CHILD_TYPE, env.getChildren(parent).iterator().next().getTypeName());
			assertEquals(PARENT_TYPE, env.getParents(child).iterator().next().getTypeName());

			assertEquals(PARENT_TYPE, env.getRelatedObjects(child, PARENT_TYPE, false).iterator().next().getTypeName());
			assertEquals(CHILD_TYPE, env.getRelatedObjects(parent, CHILD_TYPE, true).iterator().next().getTypeName());

		} finally {
			env.deleteObjectType(CHILD_TYPE);
			env.deleteObjectType(PARENT_TYPE);
		}
	}

	@Test(expected = InvalidValueException.class)
	public void testCreateObjectHavingTooLongStringValueThrowsException() throws Exception {
		String type1 = "Type1";
		try {
			ObjectType t1 = env.createObjectType(type1);
			t1.getPropertyDescriptors().add(new PropertyDescr("text", String.class.getName()));
			env.updateObjectType(t1);

			ValueTO[] valueArr = { new ValueTO("text", Strings.repeat("X", 21834)) };
			TO<?> cq = env.createObject("Type1", valueArr);

		} finally {
			env.deleteObjectType(type1);
		}
	}

	@Test(expected = InvalidValueException.class)
	public void testSetTooLongStringValueThrowsException() throws Exception {
		String type1 = "Type1";
		try {
			ObjectType t1 = env.createObjectType(type1);
			t1.getPropertyDescriptors().add(new PropertyDescr("text", String.class.getName()));
			env.updateObjectType(t1);

			TO<?> cq = env.createObject("Type1");
			env.setPropertyValue(cq, "text", Strings.repeat("X", 21834));

		} finally {
			env.deleteObjectType(type1);
		}
	}

    @Test(expected = InvalidValueException.class)
    public void testSetAllTooLongStringValueThrowsException() throws Exception {
        String type1 = "Type1";
        try {
            ObjectType t1 = env.createObjectType(type1);
            t1.getPropertyDescriptors().add(new PropertyDescr("text", String.class.getName()));
            env.updateObjectType(t1);

            TO<?> cq = env.createObject("Type1");
            env.setAllPropertiesValues(cq, new ValueTO[]{new ValueTO("text", Strings.repeat("X", 21834))});

        } finally {
            env.deleteObjectType(type1);
        }
    }

}
