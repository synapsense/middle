package com.synapsense.func.alerting;

import java.util.Arrays;
import java.util.Collection;
import java.util.Set;

import com.synapsense.dto.AlertPriority;
import com.synapsense.dto.AlertType;
import com.synapsense.dto.MessageTemplate;
import com.synapsense.dto.MessageTemplateRef;
import com.synapsense.dto.MessageType;
import com.synapsense.dto.PriorityTier;
import com.synapsense.dto.TO;
import com.synapsense.exception.AlertTypeAlreadyExistsException;
import com.synapsense.service.AlertService;
import com.synapsense.service.Environment;
import com.synapsense.util.CollectionUtils;

public class AlertingTestHelper {
	public AlertingTestHelper(final Environment environment, final AlertService as) {
		this.as = as;
	}

	private final AlertService as;

	public AlertPriority getPriority(final String name) {
		AlertPriority ap = as.getAlertPriority(name);
		if (ap == null) {
			ap = new AlertPriority(name, 600000L, Arrays.asList(new PriorityTier("TIER1", 1000, 2, true, true, true)));
		}
		return ap;
	}

	public AlertType makeCopy(final String name, final String priority, final boolean isActive,
	        final boolean autoDismiss, final boolean autoAck) {
		return new AlertType(name, name, "descr", priority, isActive, autoDismiss, autoAck);
	}

	public AlertType newPersistentAlertType(final String name, final String descr, final String priority,
	        final boolean active, final boolean autoDismiss, final boolean autoAck, TO ref) {

		final Collection<AlertType> pers = as.getAlertTypes(new String[] { name });
		if (!pers.isEmpty()) {
			return pers.iterator().next();
		}

		AlertPriority ap = as.getAlertPriority(priority);
		if (ap == null) {
			ap = new AlertPriority(priority, 600000L,
			        Arrays.asList(new PriorityTier("TIER1", 1000, 2, true, true, true)));
		}

		Set<MessageTemplate> templates = CollectionUtils.newSet();
		if (ref != null)
			templates.add(new MessageTemplateRef(MessageType.CONSOLE, ref));
		final AlertType at = new AlertType(name, name, descr, priority, "header", "body", templates, active,
		        autoDismiss, autoAck, true);

		try {
			as.createAlertType(at);
		} catch (final AlertTypeAlreadyExistsException e) {
			e.printStackTrace();
		}

		return at;

	}

	public AlertType newPersistentAlertType(final String name, final String descr, final String priority,
	        final boolean active, final boolean autoDismiss, TO ref) {
		return this.newPersistentAlertType(name, descr, priority, active, autoDismiss, false, ref);

	}

	public AlertType newPersistentAlertType(final String name, final String priority, final boolean active,
	        final boolean autoDismiss) {
		return this.newPersistentAlertType(name, "descr", priority, active, autoDismiss, null);
	}
}
