package com.synapsense.func.userservice;

import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.IllegalInputParameterException;
import com.synapsense.exception.UserManagementException;
import com.synapsense.func.MockConsole;
import com.synapsense.service.UserManagementService;
import com.synapsense.util.CollectionUtils;
import com.synapsense.util.ServerLoginModule;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.ejb.EJBAccessException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.security.auth.login.LoginException;
import javax.security.sasl.SaslException;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.*;

import static org.junit.Assert.*;

public class UserTests extends AbstractUserServiceTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		AbstractUserServiceTest.setUpBeforeClass();
	}

	@Before
	public void setUp() throws Exception {
		super.setUp();
	}

	@After
	public void tearDown() throws Exception {
		super.tearDown();
	}

	@Test
	public void testGetAllUsers() throws Exception {
		Collection<TO<?>> allItems = console.getUserService().getAllUsers();
		assertNotNull(allItems);

		Collection<String> names = CollectionUtils.newSet();
		for (TO<?> item : allItems) {
			names.add((String) this.USER_SERVICE.getProperty(item, UserManagementService.USER_NAME));
		}

		assertTrue("No admin user!", names.contains("admin"));
	}

	@Test
	public void testSynapSenseUserExists() {
		// Can't get synapsense user via UserManagementService
		Collection<TO<?>> synapsense = this.console.getEnv().getObjects("USER", new ValueTO[]{new ValueTO("userName", "synapsense")});
		assertTrue(synapsense != null);
		assertFalse(synapsense.isEmpty());
		assertEquals(1, synapsense.size());
	}

	@Test
	public void createSimpleUser() throws UserManagementException {
		String userName = "createusertest_1";
		TO<?> user = null;

		try {
			// creates user 'createusertest_1' with password 'new_pass' and puts in 'Standard' group
			user = this.USER_SERVICE.createUser(userName, "new_pass", null);
			assertNotNull(user);

			// check that user was created
			user = this.USER_SERVICE.getUser(userName);
			assertNotNull(user);
			assertEquals(userName, (String) this.USER_SERVICE.getProperty(user, UserManagementService.USER_NAME));

			Collection<String> userGroups = this.USER_SERVICE.getUserGroupIds(user);
			assertTrue(userGroups.contains("Standard"));
		} finally {
			if (user != null) {
				this.USER_SERVICE.deleteUser(user);
			}
			user = this.USER_SERVICE.getUser(userName);
			assertNull(user);
		}
	}

	@Test
	public void createUserInPowerGroup() throws UserManagementException {
		String userName = "createusertest_2";
		TO<?> user = null;

		try {
			// creates user 'createusertest_2' with password 'new_pass' and puts in 'Power' group
			user = this.USER_SERVICE.createUser(userName, "new_pass", "Power");
			assertNotNull(user);

			// check that user was created
			user = this.USER_SERVICE.getUser(userName);
			assertNotNull(user);
			assertEquals(userName, (String) this.USER_SERVICE.getProperty(user, UserManagementService.USER_NAME));

			Collection<String> userGroups = this.USER_SERVICE.getUserGroupIds(user);
			assertTrue(userGroups.contains("Power"));
		} finally {
			if (user != null) {
				this.USER_SERVICE.deleteUser(user);
			}
			user = this.USER_SERVICE.getUser(userName);
			assertNull(user);
		}
	}

	@Test
	public void createUserInAdminGroup() throws UserManagementException {
		String userName = "createusertest_3";
		TO<?> user = null;

		try {
			// creates user 'createusertest_3' with password 'new_pass' and puts in 'Administrators' group
			user = this.USER_SERVICE.createUser(userName, "new_pass", "Administrators");
			assertNotNull(user);

			// check that user was created
			user = this.USER_SERVICE.getUser(userName);
			assertNotNull(user);
			assertEquals(userName, (String) this.USER_SERVICE.getProperty(user, UserManagementService.USER_NAME));

			Collection<String> userGroups = this.USER_SERVICE.getUserGroupIds(user);
			assertTrue(userGroups.contains("Administrators"));
		} finally {
			if (user != null) {
				this.USER_SERVICE.deleteUser(user);
			}
			user = this.USER_SERVICE.getUser(userName);
			assertNull(user);
		}
	}

	@Test
	public void createUserNoUserName() throws UserManagementException {
		TO<?> user = null;

		try {
			// creates user null with password 'new_pass' and puts in 'Standard' group
			// Should fail
			try {
				user = this.USER_SERVICE.createUser(null, "new_pass", null);
				assertTrue("Can't create user with null user name.", false);
			} catch (IllegalInputParameterException e) {
			}

			try {
				user = this.USER_SERVICE.createUser("", "new_pass", null);
				assertTrue("Can't create user with empty user name.", false);
			} catch (IllegalInputParameterException e) {
			}

			try {
				user = this.USER_SERVICE.createUser("      ", "new_pass", null);
				assertTrue("Can't create user with empty user name.", false);
			} catch (IllegalInputParameterException e) {
			}

		} finally {
			if (user != null) {
				this.USER_SERVICE.deleteUser(user);
			}
		}
	}

	@Test
	public void createUserNoPassword() throws UserManagementException {
		String userName = "createusertest_5";
		TO<?> user = null;

		try {
			// creates user 'createusertest_5' with empty password and puts in 'Standard' group
			// Should fail
			try {
				user = this.USER_SERVICE.createUser(userName, null, null);
				assertTrue("Can't create user with null password.", false);
			} catch (IllegalInputParameterException e) {
			}

			try {
				user = this.USER_SERVICE.createUser(userName, "", null);
				assertTrue("Can't create user with empty password.", false);
			} catch (IllegalInputParameterException e) {
			}

			try {
				user = this.USER_SERVICE.createUser(userName, "   ", null);
				assertTrue("Can't create user with empty password.", false);
			} catch (IllegalInputParameterException e) {
			}

			user = this.USER_SERVICE.getUser(userName);
			assertNull(user);

		} finally {
			if (user != null) {
				this.USER_SERVICE.deleteUser(user);
			}
			user = this.USER_SERVICE.getUser(userName);
			assertNull(user);
		}
	}

	@Test
	public void createDuplicateUser() throws UserManagementException {
		String userName = "createusertest_6";
		TO<?> user = null;

		try {
			// creates user 'createusertest_6' with password 'new_pass' and puts in 'Standard' group
			user = this.USER_SERVICE.createUser(userName, "new_pass", null);
			assertNotNull(user);

			// check that user was created
			user = this.USER_SERVICE.getUser(userName);
			assertNotNull(user);
			assertEquals(userName, (String) this.USER_SERVICE.getProperty(user, UserManagementService.USER_NAME));

			// tests exception on duplicate user
			try {
				this.USER_SERVICE.createUser(userName, "new_pass", null);
				assertTrue("Can't create a duplicate user, " + userName, false);
			} catch (UserManagementException e) {
			}

		} finally {
			if (user != null) {
				this.USER_SERVICE.deleteUser(user);
			}
			user = this.USER_SERVICE.getUser(userName);
			assertNull(user);
		}
	}

	@Test
	public void createUserWithEmptyGroup() throws UserManagementException {
		String userName = "createusertest_7";
		TO<?> user = null;

		try {
			// creates user 'createusertest_7' with password 'new_pass' and empty group
			user = this.USER_SERVICE.createUser(userName, "new_pass", "");
			assertNotNull(user);

			Collection<String> userGroups = this.USER_SERVICE.getUserGroupIds(user);
			assertTrue(userGroups.contains("Standard"));

			this.USER_SERVICE.deleteUser(user);

			user = this.USER_SERVICE.createUser(userName, "new_pass", "   ");
			assertNotNull(user);

			userGroups = this.USER_SERVICE.getUserGroupIds(user);
			assertTrue(userGroups.contains("Standard"));

		} finally {
			if (user != null) {
				this.USER_SERVICE.deleteUser(user);
			}
			user = this.USER_SERVICE.getUser(userName);
			assertNull(user);
		}
	}

	@Test
	public void createUserInvalidGroup() throws UserManagementException {
		String userName = "createusertest_8";
		TO<?> user = null;

		try {
			// creates user 'createusertest_8' with password 'new_pass' and invalid group
			// Should fail
			try {
				user = this.USER_SERVICE.createUser(userName, "new_pass", "NonExistentGroup");
				assertTrue("Can't create user with invalid group.", false);
			} catch (UserManagementException | IllegalInputParameterException e) {
			}

		} finally {
			if (user != null) {
				this.USER_SERVICE.deleteUser(user);
			}
			user = this.USER_SERVICE.getUser(userName);
			assertNull(user);
		}
	}

	@Test
	public void deleteCreatedUser() throws UserManagementException {
		String userName = "deleteusertest_1";
		TO<?> user = null;

		try {
			// creates user 'deleteusertest_1' with password 'new_pass' and puts in 'Standard' group
			user = this.USER_SERVICE.createUser(userName, "new_pass", null);
			assertNotNull(user);
			assertEquals(userName, (String) this.USER_SERVICE.getProperty(user, UserManagementService.USER_NAME));

			this.USER_SERVICE.deleteUser(user);
			user = null;

			// check that user was deleted
			user = this.USER_SERVICE.getUser(userName);
			assertNull(user);
		} finally {
			if (user != null) {
				this.USER_SERVICE.deleteUser(user);
			}
			user = this.USER_SERVICE.getUser(userName);
			assertNull(user);
		}
	}

	@Test
	public void tryToDeleteUserTwice() throws UserManagementException {
		String userName = "deleteusertest_2";
		TO<?> user = null;

		try {
			// creates user 'deleteusertest_2' with password 'new_pass' and puts in 'Standard' group
			user = this.USER_SERVICE.createUser(userName, "new_pass", null);
			assertNotNull(user);
			assertEquals(userName, (String) this.USER_SERVICE.getProperty(user, UserManagementService.USER_NAME));

			TO<?> userCopy = user;

			this.USER_SERVICE.deleteUser(user);
			user = null;

			// check that user was deleted
			assertNull(this.USER_SERVICE.getUser(userName));

			try {
				this.USER_SERVICE.deleteUser(userCopy);
			} catch (EJBException | UserManagementException | IllegalInputParameterException e) {
				assertTrue("Should be able to delete the same user twice.", false);
			}
		} finally {
			if (user != null) {
				this.USER_SERVICE.deleteUser(user);
			}
			user = this.USER_SERVICE.getUser(userName);
			assertNull(user);
		}
	}

	@Test
	public void tryToDeleteGroupUsingDeleteUser() throws UserManagementException {
		String userName = "deleteusertest_3";
		TO<?> user = null;
		TO<?> group = null;

		try {
			// creates user 'deleteusertest_3' with password 'new_pass' and puts in 'Standard' group
			user = this.USER_SERVICE.createUser(userName, "new_pass", null);
			assertNotNull(user);
			assertEquals(userName, (String) this.USER_SERVICE.getProperty(user, UserManagementService.USER_NAME));

			group = this.USER_SERVICE.createGroup("deleteusertest_group1", null);

			try {
				this.USER_SERVICE.deleteUser(group);
				assertTrue("Can't delete a group using deleteUser.", false);
			} catch (UserManagementException | IllegalInputParameterException e) {
			}
		} finally {
			if (user != null) {
				this.USER_SERVICE.deleteUser(user);
			}
			user = this.USER_SERVICE.getUser(userName);
			assertNull(user);

			if (group != null) {
				this.USER_SERVICE.deleteGroup(group);
			}
			group = this.USER_SERVICE.getGroup("deleteusertest_group1");
			assertNull(group);
		}
	}

	@Test
	public void tryToDeleteNullUser() throws UserManagementException {
		try {
			this.USER_SERVICE.deleteUser(null);
			assertTrue("Can't delete a null user.", false);
		} catch (UserManagementException | IllegalInputParameterException e) {
		}
	}

	@Test
	public void tryToDeleteSynapsenseUser() throws UserManagementException {

		Collection<TO<?>> objIds = this.console.getEnv().getObjects("USER",new ValueTO[]{new ValueTO(UserManagementService.USER_NAME, "synapsense")});
		assertNotNull(objIds);
		assertFalse(objIds.isEmpty());
		assertEquals(1, objIds.size());

		TO<?> user = objIds.iterator().next();
		try {
			this.USER_SERVICE.deleteUser(user);
			assertTrue("Can't delete synapsense user.", false);
		} catch (UserManagementException | IllegalInputParameterException e) {
		}
	}

	@Test
		public void testGetUser() throws Exception {
		TO<?> user = this.USER_SERVICE.getUser("admin");
		assertNotNull(user);
		assertEquals("admin", this.USER_SERVICE.getProperty(user, UserManagementService.USER_NAME));
	}

	@Test
	public void testGetUserInvalidName() throws Exception {
		TO<?> user = this.USER_SERVICE.getUser("DEAD_BEEF");
		assertNull(user);
	}

	@Test
	public void testGetUserNullOrEmptyName() throws Exception {
		try {
			TO<?> user = this.USER_SERVICE.getUser(null);
			assertTrue("Can't get user with null name.", false);
		} catch (IllegalInputParameterException e) {
		}

		try {
			TO<?> user = this.USER_SERVICE.getUser("");
			assertTrue("Can't get user with empty name.", false);
		} catch (IllegalInputParameterException e) {
		}

		try {
			TO<?> user = this.USER_SERVICE.getUser("    ");
			assertTrue("Can't get user with empty name.", false);
		} catch (IllegalInputParameterException e) {
		}
	}

	@Test
	public void testUpdateUser() throws Exception {
		TO<?> user = null;

		try {
			user = this.USER_SERVICE.createUser("usertests_1", "new_pass", null);
			assertNotNull(user);

			Map<String, Object> props = CollectionUtils.newMap();
			props.put(UserManagementService.USER_DESCRIPTION, "This is a description.");
			props.put(UserManagementService.USER_FIRST_NAME, "Fred");
			props.put(UserManagementService.USER_GROUPS, "Power");
			props.put(UserManagementService.USER_LAST_NAME, "Flintstone");
			props.put(UserManagementService.USER_MIDDLE_INITIAL, "F");
			props.put(UserManagementService.USER_PASSWORD, "changed_pass");
			props.put(UserManagementService.USER_PASSWORD_HINT, "Slate Mining");
			props.put(UserManagementService.USER_PASSWORD_HINT_ANSWER, "Crane Operator");
			props.put(UserManagementService.USER_PRIMARY_EMAIL, "fred@bedrock.com");
			props.put(UserManagementService.USER_PRIMARY_PHONE, "123-456-7890");
			props.put(UserManagementService.USER_SECONDARY_EMAIL, "fred.flintstone@bedrock.com");
			props.put(UserManagementService.USER_SECONDARY_PHONE, "234-567-8901");
			props.put(UserManagementService.USER_SMS_ADDRESS, "1@bedrock.com");
			props.put(UserManagementService.USER_TITLE, "Sir");
			this.USER_SERVICE.updateUser(user, props);

			String encPwd = ServerLoginModule.PasswordService.encrypt("changed_pass");

			props.clear();
			props = this.USER_SERVICE.getUserInformation(user);
			assertEquals("usertests_1", props.get(UserManagementService.USER_NAME));
			assertEquals("This is a description.", props.get(UserManagementService.USER_DESCRIPTION));
			assertEquals("Fred", props.get(UserManagementService.USER_FIRST_NAME));
			assertEquals("Flintstone", props.get(UserManagementService.USER_LAST_NAME));
			assertEquals("F", props.get(UserManagementService.USER_MIDDLE_INITIAL));
			assertEquals(encPwd, props.get(UserManagementService.USER_PASSWORD));
			assertEquals("Slate Mining", props.get(UserManagementService.USER_PASSWORD_HINT));
			assertEquals("Crane Operator", props.get(UserManagementService.USER_PASSWORD_HINT_ANSWER));
			assertEquals("fred@bedrock.com", props.get(UserManagementService.USER_PRIMARY_EMAIL));
			assertEquals("123-456-7890", props.get(UserManagementService.USER_PRIMARY_PHONE));
			assertEquals("fred.flintstone@bedrock.com", props.get(UserManagementService.USER_SECONDARY_EMAIL));
			assertEquals("234-567-8901", props.get(UserManagementService.USER_SECONDARY_PHONE));
			assertEquals("1@bedrock.com", props.get(UserManagementService.USER_SMS_ADDRESS));
			assertEquals("Sir", props.get(UserManagementService.USER_TITLE));

			Collection<TO<?>> groups = (Collection<TO<?>>) props.get(UserManagementService.USER_GROUPS);
			assertNotNull(groups);
			assertEquals(1, groups.size());
			TO<?> powerGroup = this.USER_SERVICE.getGroup("Power");
			assertNotNull(powerGroup);
			assertTrue(groups.contains(powerGroup));

			this.USER_SERVICE.setProperty(user, UserManagementService.USER_DESCRIPTION, "A description this is.");
			assertEquals("A description this is.", this.USER_SERVICE.getProperty(user, UserManagementService.USER_DESCRIPTION));
		} finally {
			if (user != null) {
				this.USER_SERVICE.deleteUser(user);
			}
		}
	}

	@Test
	public void testUpdateUserWithInvalidProperty() throws Exception {
		TO<?> user = null;

		try {
			user = this.USER_SERVICE.createUser("usertests_2", "new_pass", null);
			assertNotNull(user);

			Map<String, Object> props = CollectionUtils.newMap();
			props.put("SOME_PROP", "Invalid property");
			try {
				this.USER_SERVICE.updateUser(user, props);
				assertTrue("Can't add an unknown property name to a user.", false);
			} catch (UserManagementException e) {
			}

			try {
				this.USER_SERVICE.setProperty(user, "SOME_PROP", "Invalid property");
				assertTrue("Can't add an unknown property name to a user.", false);
			} catch (UserManagementException e) {
			}
		} finally {
			if (user != null) {
				this.USER_SERVICE.deleteUser(user);
			}
		}
	}

	@Test
	public void testGetUserGroupIds() throws Exception {
		String name = "usertests_3";
		TO<?> user = null;

		try {
			user = this.USER_SERVICE.createUser(name, "new_pass", Arrays.asList("Power", "Standard"));
			assertNotNull(user);
			assertEquals(name, this.USER_SERVICE.getProperty(user, UserManagementService.USER_NAME));

			Collection<String> groups = this.USER_SERVICE.getUserGroupIds(user);
			assertNotNull(groups);
			assertEquals(2, groups.size());
			assertTrue(groups.contains("Power"));
			assertTrue(groups.contains("Standard"));

		} finally {
			if (user != null) {
				this.USER_SERVICE.deleteUser(user);
			}
			user = this.USER_SERVICE.getUser(name);
			assertNull(user);
		}
	}

	@Test
	public void testGetUserGroups() throws Exception {
		String name = "usertests_4";
		TO<?> user = null;

		try {
			user = this.USER_SERVICE.createUser(name, "new_pass", Arrays.asList("Power", "Standard"));
			assertNotNull(user);
			assertEquals(name, this.USER_SERVICE.getProperty(user, UserManagementService.USER_NAME));

			Collection<TO<?>> groups = this.USER_SERVICE.getUserGroups(user);
			assertNotNull(groups);
			assertEquals(2, groups.size());
			assertTrue(groups.contains(this.USER_SERVICE.getGroup("Power")));
			assertTrue(groups.contains(this.USER_SERVICE.getGroup("Standard")));

		} finally {
			if (user != null) {
				this.USER_SERVICE.deleteUser(user);
			}
			user = this.USER_SERVICE.getUser(name);
			assertNull(user);
		}
	}

	@Test
	public void testPasswords() throws Exception {
		String name = "usertests_5";
		TO<?> user = null;

		try {
			user = this.USER_SERVICE.createUser(name, "pass1", null);
			assertNotNull(user);
			assertEquals(name, this.USER_SERVICE.getProperty(user, UserManagementService.USER_NAME));

			String encPwd = ServerLoginModule.PasswordService.encrypt("pass1");
			assertEquals(encPwd, this.USER_SERVICE.getRawPassword(user));
			assertEquals(encPwd, this.USER_SERVICE.getProperty(user, UserManagementService.USER_PASSWORD));
			Map<String, Object> props = this.USER_SERVICE.getUserInformation(user);
			assertEquals(encPwd, props.get(UserManagementService.USER_PASSWORD));

			this.USER_SERVICE.setPassword(user, "pass2");
			encPwd = ServerLoginModule.PasswordService.encrypt("pass2");
			assertEquals(encPwd, this.USER_SERVICE.getRawPassword(user));
			assertEquals(encPwd, this.USER_SERVICE.getProperty(user, UserManagementService.USER_PASSWORD));
			props = this.USER_SERVICE.getUserInformation(user);
			assertEquals(encPwd, props.get(UserManagementService.USER_PASSWORD));

			this.USER_SERVICE.setProperty(user, UserManagementService.USER_PASSWORD, "pass3");
			encPwd = ServerLoginModule.PasswordService.encrypt("pass3");
			assertEquals(encPwd, this.USER_SERVICE.getRawPassword(user));
			assertEquals(encPwd, this.USER_SERVICE.getProperty(user, UserManagementService.USER_PASSWORD));
			props = this.USER_SERVICE.getUserInformation(user);
			assertEquals(encPwd, props.get(UserManagementService.USER_PASSWORD));

			this.USER_SERVICE.setRawPassword(user, "pass4");
			encPwd = "pass4";
			assertEquals(encPwd, this.USER_SERVICE.getRawPassword(user));
			assertEquals(encPwd, this.USER_SERVICE.getProperty(user, UserManagementService.USER_PASSWORD));
			props = this.USER_SERVICE.getUserInformation(user);
			assertEquals(encPwd, props.get(UserManagementService.USER_PASSWORD));

		} finally {
			if (user != null) {
				this.USER_SERVICE.deleteUser(user);
			}
			user = this.USER_SERVICE.getUser(name);
			assertNull(user);
		}
	}

	@Test
	public void testGetUserPrivilegeIds() throws Exception {
		String name = "usertests_6";
		TO<?> user = null;

		try {
			user = this.USER_SERVICE.createUser(name, "pass1", "Standard");
			assertNotNull(user);
			assertEquals(name, this.USER_SERVICE.getProperty(user, UserManagementService.USER_NAME));

			List<String> userPrivileges = Arrays.asList(this.USER_SERVICE.getUserPrivilegeIds(user).toArray(new String[0]));
			assertNotNull(userPrivileges);
			Collections.sort(userPrivileges);

			List<String> groupPrivileges = Arrays.asList(this.USER_SERVICE.getGroupPrivilegeIds(this.USER_SERVICE.getGroup("Standard")).toArray(new String[0]));
			assertNotNull(groupPrivileges);
			Collections.sort(groupPrivileges);

			assertEquals(groupPrivileges.size(), userPrivileges.size());
			assertEquals(groupPrivileges, userPrivileges);

		} finally {
			if (user != null) {
				this.USER_SERVICE.deleteUser(user);
			}
			user = this.USER_SERVICE.getUser(name);
			assertNull(user);
		}
	}

	@Test
	public void testSetUserGroupsUsingNames() throws Exception {
		String name = "usertests_7";
		TO<?> user = null;

		try {
			user = this.USER_SERVICE.createUser(name, "new_pass", "Standard");
			assertNotNull(user);
			assertEquals(name, this.USER_SERVICE.getProperty(user, UserManagementService.USER_NAME));

			// Set using setUserGroups
			this.USER_SERVICE.setUserGroups(user, Arrays.asList("Administrators", "Power", "Standard"));
			Collection<String> groups = this.USER_SERVICE.getUserGroupIds(user);
			assertNotNull(groups);
			assertEquals(3, groups.size());
			assertTrue(groups.contains("Administrators"));
			assertTrue(groups.contains("Power"));
			assertTrue(groups.contains("Standard"));

			// Set using setProperty
			this.USER_SERVICE.setProperty(user, UserManagementService.USER_GROUPS, Arrays.asList("Power", "Standard"));
			groups = this.USER_SERVICE.getUserGroupIds(user);
			assertNotNull(groups);
			assertEquals(2, groups.size());
			assertTrue(groups.contains("Power"));
			assertTrue(groups.contains("Standard"));

			// Set using updateUser
			Map<String, Object> props = CollectionUtils.newMap();
			props.put(UserManagementService.USER_GROUPS, Arrays.asList("Standard"));
			this.USER_SERVICE.updateUser(user, props);
			groups = this.USER_SERVICE.getUserGroupIds(user);
			assertNotNull(groups);
			assertEquals(1, groups.size());
			assertTrue(groups.contains("Standard"));

			// Set to invalid group
			try {
				this.USER_SERVICE.setUserGroups(user, Arrays.asList("NON_EXISTENT_GROUP"));
				assertTrue("Can't set user group to group that doesn't exist.", false);
			} catch (UserManagementException e) {

			}

			// Set to empty group list
			try {
				this.USER_SERVICE.setUserGroups(user, null);
				assertTrue("Can't set user group to group that doesn't exist.", false);
			} catch (IllegalInputParameterException | UserManagementException e) {

			}

			try {
				this.USER_SERVICE.setUserGroups(user, "");
				assertTrue("Can't set user group to group that doesn't exist.", false);
			} catch (IllegalInputParameterException | UserManagementException e) {

			}

			try {
				this.USER_SERVICE.setUserGroups(user, "   ");
				assertTrue("Can't set user group to group that doesn't exist.", false);
			} catch (IllegalInputParameterException | UserManagementException e) {

			}

		} finally {
			if (user != null) {
				this.USER_SERVICE.deleteUser(user);
			}
			user = this.USER_SERVICE.getUser(name);
			assertNull(user);
		}
	}

	@Test
	public void testSetUserGroupsUsingObjects() throws Exception {
		String name = "usertests_8";
		TO<?> user = null;

		try {
			TO<?> adminGroup = this.USER_SERVICE.getGroup("Administrators");
			assertNotNull(adminGroup);

			TO<?> powerGroup = this.USER_SERVICE.getGroup("Power");
			assertNotNull(powerGroup);

			TO<?> standardGroup = this.USER_SERVICE.getGroup("Standard");
			assertNotNull(standardGroup);

			user = this.USER_SERVICE.createUser(name, "new_pass", standardGroup);
			assertNotNull(user);
			assertEquals(name, this.USER_SERVICE.getProperty(user, UserManagementService.USER_NAME));

			// Set using setUserGroups
			this.USER_SERVICE.setUserGroups(user, Arrays.asList(adminGroup, powerGroup, standardGroup));
			Collection<TO<?>> groups = this.USER_SERVICE.getUserGroups(user);
			assertNotNull(groups);
			assertEquals(3, groups.size());
			assertTrue(groups.contains(adminGroup));
			assertTrue(groups.contains(powerGroup));
			assertTrue(groups.contains(standardGroup));

			// Set using setProperty
			this.USER_SERVICE.setProperty(user, UserManagementService.USER_GROUPS, Arrays.asList(powerGroup, standardGroup));
			groups = this.USER_SERVICE.getUserGroups(user);
			assertNotNull(groups);
			assertEquals(2, groups.size());
			assertTrue(groups.contains(powerGroup));
			assertTrue(groups.contains(standardGroup));

			// Set using updateUser
			Map<String, Object> props = CollectionUtils.newMap();
			props.put(UserManagementService.USER_GROUPS, Arrays.asList(standardGroup));
			this.USER_SERVICE.updateUser(user, props);
			groups = this.USER_SERVICE.getUserGroups(user);
			assertNotNull(groups);
			assertEquals(1, groups.size());
			assertTrue(groups.contains(standardGroup));

		} finally {
			if (user != null) {
				this.USER_SERVICE.deleteUser(user);
			}
			user = this.USER_SERVICE.getUser(name);
			assertNull(user);
		}
	}

	@Test
	public void testChangeUserName() throws Exception {
		String name = "usertests_8";
		TO<?> user = null;

		try {
			user = this.USER_SERVICE.createUser(name, "new_pass", null);
			assertNotNull(user);
			assertEquals(name, this.USER_SERVICE.getProperty(user, UserManagementService.USER_NAME));

			this.USER_SERVICE.setProperty(user, UserManagementService.USER_NAME, "usertests_9");
			user = this.USER_SERVICE.getUser("usertests_9");
			assertNotNull(user);
			name = "usertests_9";
			assertEquals(name, this.USER_SERVICE.getProperty(user, UserManagementService.USER_NAME));

			// Set using updateUser
			Map<String, Object> props = CollectionUtils.newMap();
			props.put(UserManagementService.USER_NAME, "usertests_10");
			this.USER_SERVICE.updateUser(user, props);
			user = this.USER_SERVICE.getUser("usertests_10");
			assertNotNull(user);
			name = "usertests_10";
			assertEquals(name, this.USER_SERVICE.getProperty(user, UserManagementService.USER_NAME));

		} finally {
			if (user != null) {
				this.USER_SERVICE.deleteUser(user);
			}
			user = this.USER_SERVICE.getUser(name);
			assertNull(user);
		}
	}

}
