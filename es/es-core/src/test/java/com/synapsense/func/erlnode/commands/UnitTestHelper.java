package com.synapsense.func.erlnode.commands;

import java.util.Properties;

import javax.naming.InitialContext;

import com.synapsense.service.Environment;
import com.synapsense.util.ServerLoginModule;

public class UnitTestHelper {

	Environment env;

	public Environment getEnvironment() throws Exception {
		return env;
	}

	public boolean initializeEnvironment() throws Exception {
		try {
			Properties clientProps = new Properties();
			{
				clientProps.put("endpoint.name", "client-endpoint");
				clientProps.put("remote.connections", "default");
				clientProps.put("remote.connection.default.host", "localhost");
				clientProps.put("remote.connection.default.port", "4447");
				clientProps.put("remote.connection.default.username", "admin");
				clientProps.put("remote.connection.default.password", "admin");

				clientProps.put("remote.connectionprovider.create.options.org.xnio.Options.SSL_ENABLED", "false");
				clientProps.put("remote.connection.default.connect.options.org.xnio.Options.SASL_POLICY_NOANONYMOUS",
				        "false");
				clientProps.put(
				        "remote.connection.default.connect.options.org.xnio.Options.SASL_DISALLOWED_MECHANISMS",
				        "JBOSS-LOCAL-USER");
			}
			InitialContext ctx = new InitialContext(clientProps);

			env = ServerLoginModule.getService(ctx, "Environment", Environment.class);
		} catch (Exception e) {
			System.err.println("Service initialization failed: " + e);
			return false;
		}
		return true;
	}

	public void dispose() throws Exception {
	}

	public boolean isTypeExists(String typeName) throws Exception {
		String[] types = getEnvironment().getObjectTypes();
		for (String tName : types)
			if (tName.equals(typeName))
				return true;
		return false;
	}

	public boolean removeTypeCompletly(String typeName) {

		try {
			if (isTypeExists(typeName))
				getEnvironment().deleteObjectType(typeName);
		} catch (Exception ex) {
			return false;
		}
		return true;
	}

}
