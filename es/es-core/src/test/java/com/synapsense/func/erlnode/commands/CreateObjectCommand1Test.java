package com.synapsense.func.erlnode.commands;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.ericsson.otp.erlang.OtpErlangObject;
import com.ericsson.otp.erlang.OtpErlangString;
import com.ericsson.otp.erlang.OtpErlangTuple;
import com.synapsense.dto.ObjectType;
import com.synapsense.dto.PropertyDescr;
import com.synapsense.dto.TO;
import com.synapsense.impl.erlnode.ExceptionTypes;
import com.synapsense.impl.erlnode.commands.CreateObjectCommand1;
import com.synapsense.impl.erlnode.utils.ToConverter;
import com.synapsense.service.Environment;

/**
 * @author ser
 * 
 */
public class CreateObjectCommand1Test {

	static UnitTestHelper utHelper = null;
	static String testType = "testingObjType_CreateObjectTwoParTest";
	static Map<String, Object> envMap;
	static Environment env;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		utHelper = new UnitTestHelper();
		utHelper.initializeEnvironment();
		env = utHelper.getEnvironment();
		envMap = new HashMap<String, Object>();
		envMap.put("env", env);
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		utHelper.removeTypeCompletly(testType);
		utHelper.dispose();
	}

	@Before
	public void setUp() throws Exception {

		ObjectType otype = env.createObjectType(testType);

		otype.getPropertyDescriptors().add(new PropertyDescr("name", String.class.getName()));
		env.updateObjectType(otype);
	}

	@After
	public void tearDown() throws Exception {
		env.deleteObjectType(testType);
	}

	@Test
	public void SuccessCreateObjectTest() throws Exception {
		CreateObjectCommand1 command = new CreateObjectCommand1(envMap);
		OtpErlangTuple obj = (OtpErlangTuple) command.run(new OtpErlangTuple(new OtpErlangObject[] {
		        new OtpErlangString("createObjectCommand1"), new OtpErlangString(testType) }));
		assertEquals("ok", obj.elementAt(0).toString());
		TO<?> to = ToConverter.parseTo((OtpErlangTuple) obj.elementAt(1));
		assertEquals(testType, to.getTypeName());

		env.deleteObject(ToConverter.parseTo((OtpErlangTuple) obj.elementAt(1)));

	}

	@Test
	public void FailObjectTypeTest() throws Exception {

		CreateObjectCommand1 command = new CreateObjectCommand1(envMap);
		OtpErlangTuple obj = (OtpErlangTuple) command.run(new OtpErlangTuple(new OtpErlangObject[] {
		        new OtpErlangString("createObjectCommand1"), new OtpErlangString("someNotExistingType") }));
		assertEquals("exception", obj.elementAt(0).toString());
		assertEquals(ExceptionTypes.NO_OBJECT_TYPE.getExceptionType(),
		        ((OtpErlangString) obj.elementAt(1)).stringValue());
	}

	@Test
	public void InvalidParametersTest() throws Exception {
		CreateObjectCommand1 command = new CreateObjectCommand1(envMap);
		OtpErlangTuple obj = (OtpErlangTuple) command.run(new OtpErlangTuple(
		        new OtpErlangObject[] { new OtpErlangString("createObjectCommand1"), }));
		assertEquals("exception", obj.elementAt(0).toString());
		assertEquals(ExceptionTypes.ILLEGAL_ARGUMENTS.getExceptionType(),
		        ((OtpErlangString) obj.elementAt(1)).stringValue());
	}

}
