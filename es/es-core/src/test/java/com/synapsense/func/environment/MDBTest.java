package com.synapsense.func.environment;

import java.util.Date;
import java.util.Properties;

import javax.jms.ObjectMessage;
import javax.jms.Topic;
import javax.jms.TopicConnection;
import javax.jms.TopicConnectionFactory;
import javax.jms.TopicPublisher;
import javax.jms.TopicSession;
import javax.naming.Context;
import javax.naming.InitialContext;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.synapsense.service.impl.messages.BusMessage;
import com.synapsense.service.impl.messages.RoutingMessage;
import com.synapsense.service.impl.messages.SensorMessage;
import com.synapsense.service.impl.messages.VirtualSensorMessage;
import com.synapsense.service.impl.messages.base.RoutingData;
import com.synapsense.service.impl.messages.base.SensorData;

public class MDBTest {

	private static Topic topic;
	private static TopicConnectionFactory factory;
	private static TopicConnection cnn;
	private static TopicSession sess;
	private static TopicPublisher publisher;

	private static String PROVIDER_URL = "jnp://localhost:1099";
	private static String JNDI_NAME = "topic/dm";

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		try {
			Properties environment = new Properties();
			environment.put(Context.INITIAL_CONTEXT_FACTORY, "org.jnp.interfaces.NamingContextFactory");
			environment.put(Context.URL_PKG_PREFIXES, "org.jboss.naming:org.jnp.interfaces");
			environment.put(Context.PROVIDER_URL, PROVIDER_URL);
			InitialContext ctx = new InitialContext(environment);

			topic = (Topic) ctx.lookup(JNDI_NAME);
			factory = (TopicConnectionFactory) ctx.lookup("ConnectionFactory");
			cnn = factory.createTopicConnection();
			sess = cnn.createTopicSession(false, TopicSession.AUTO_ACKNOWLEDGE);
			publisher = sess.createPublisher(topic);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@AfterClass
	public static void setUpAfterClass() throws Exception {
		if (sess != null)
			sess.close();
	}

	@Test
	@Ignore
	public void testSendMessage() throws Exception {
		SensorData data = new SensorData(1, 123456789.0, new Date().getTime(), 1, 1, 1);
		BusMessage<SensorData, Long> payload = new SensorMessage(1, data);
		ObjectMessage msg = sess.createObjectMessage();
		msg.setObject(payload);
		publisher.publish(msg);
	}

	@Test
	public void testVirtualMessage() throws Exception {
		// VirtualSensorMessage data = new
		// VirtualSensorMessage("APC_PDU","products.hardware.rPDU.rPDUIdent.rPDUIdentDeviceRating","-123");
		VirtualSensorMessage data = new VirtualSensorMessage("APC_PDU",
		        "products.hardware.rPDU.rPDUIdent.rPDUIdentDeviceRating", "-123");
		ObjectMessage msg = sess.createObjectMessage();
		msg.setObject(data);
		publisher.publish(msg);
	}

	@Test
	@Ignore
	public void testSendRoute() throws Exception {
		RoutingData data = new RoutingData(1, 1, 1);
		BusMessage<RoutingData, Integer> payload = new RoutingMessage(0, data);
		ObjectMessage msg = sess.createObjectMessage();
		msg.setObject(payload);
		publisher.publish(msg);
	}

}
