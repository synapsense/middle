package com.synapsense.func.environment;

import java.io.IOException;
import java.util.Properties;

import javax.ejb.EJBAccessException;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.security.auth.login.LoginException;

import org.junit.Test;

import com.synapsense.service.FileWriter;
import com.synapsense.util.JNDINames;
import com.synapsense.util.ServerLoginModule;

public class FileWriterSecurityTest {

	@Test(expected = EJBAccessException.class)
	public void callReadFileOnServiceAsUnauthenticatedFailure() throws IOException, NamingException {
		lookupFileWriterService().readFile("file.txt");
	}

	@Test(expected = EJBAccessException.class)
	public void callReadFilebytesOnServiceAsUnauthenticatedFailure() throws IOException, NamingException {
		lookupFileWriterService().readFileBytes("file.txt");
	}

	@Test(expected = EJBAccessException.class)
	public void callReadFileAsUnauthenticatedFailure() throws IOException, NamingException {
		lookupFileWriterFrontend().readFile("file.txt");
	}

	@Test(expected = EJBAccessException.class)
	public void callReadFilebytesAsUnauthenticatedFailure() throws IOException, NamingException {
		lookupFileWriterFrontend().readFileBytes("file.txt");
	}

	@Test(expected = EJBAccessException.class)
	public void callReadFileOnServiceAsAdminFailure() throws IOException, NamingException, LoginException {
		ServerLoginModule.login("admin", "admin");
		try {
			lookupFileWriterService().readFile("file.txt");
		} finally {
			ServerLoginModule.logout();
		}
	}

	@Test(expected = EJBAccessException.class)
	public void callReadFilebytesOnServiceAsAdminFailure() throws IOException, NamingException, LoginException {
		ServerLoginModule.login("admin", "admin");
		try {
			lookupFileWriterService().readFileBytes("file.txt");
		} finally {
			ServerLoginModule.logout();
		}
	}
	
	private FileWriter lookupFileWriterService() throws NamingException {
		FileWriter fws = (FileWriter) ctx.lookup("SynapServer/FileWriterService/remote");
		return fws;
	}

	private FileWriter lookupFileWriterFrontend() throws NamingException {
		FileWriter fws = (FileWriter) ctx.lookup("SynapServer/FileWriterFrontend/remote");
		return fws;
	}

	private static InitialContext ctx;

	static {
		Properties environment = new Properties();
		environment.put(Context.INITIAL_CONTEXT_FACTORY, "org.jboss.naming.NamingContextFactory");
		environment.put(Context.URL_PKG_PREFIXES, "org.jboss.naming.client:org.jnp.interfaces");
		environment.put(Context.PROVIDER_URL, JNDINames.PROVIDER_URL);
		try {
			ctx = new InitialContext(environment);
		} catch (NamingException e) {
			e.printStackTrace();
		}
		
		try {
			ServerLoginModule.init(ctx);
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
	        ServerLoginModule.logout();
        } catch (LoginException e) {
        }
	}
}
