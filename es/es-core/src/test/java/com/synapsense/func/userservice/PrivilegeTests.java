package com.synapsense.func.userservice;

import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.IllegalInputParameterException;
import com.synapsense.exception.UserManagementException;
import com.synapsense.service.UserManagementService;
import com.synapsense.util.CollectionUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.ejb.EJBException;
import java.util.Collection;
import java.util.Map;

import static org.junit.Assert.*;

public class PrivilegeTests extends AbstractUserServiceTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		AbstractUserServiceTest.setUpBeforeClass();
	}

	@Before
	public void setUp() throws Exception {
		super.setUp();
	}

	@After
	public void tearDown() throws Exception {
		super.tearDown();
	}

	@Test
	public void testGetAllPrivileges() throws Exception {
		Collection<TO<?>> allItems = console.getUserService().getAllPrivileges();
		assertNotNull(allItems);

		Collection<String> names = CollectionUtils.newSet();
		for (TO<?> item : allItems) {
			names.add((String) this.USER_SERVICE.getProperty(item, UserManagementService.PRIVILEGE_NAME));
		}

		assertTrue("No root_permission privilege!", names.contains("root_permission"));
	}

	@Test
	public void createPrivilege() throws UserManagementException {
		String name = "createprivilegetest_1";
		TO<?> privilege = null;

		try {
			// creates privilege 'createprivilegetest_1'
			privilege = this.USER_SERVICE.createPrivilege(name);
			assertNotNull(privilege);

			// check that privilege was created
			privilege = this.USER_SERVICE.getPrivilege(name);
			assertNotNull(privilege);
			assertEquals(name, this.USER_SERVICE.getProperty(privilege, UserManagementService.PRIVILEGE_NAME));
		} finally {
			if (privilege != null) {
				this.USER_SERVICE.deletePrivilege(privilege);
			}
			privilege = this.USER_SERVICE.getPrivilege(name);
			assertNull(privilege);
		}
	}

	@Test
	public void createPrivilegeNoname() throws UserManagementException {
		TO<?> privilege = null;

		try {
			// creates privilege null
			// Should fail
			try {
				privilege = this.USER_SERVICE.createPrivilege(null);
				assertTrue("Can't create privilege with null privilege name.", false);
			} catch (IllegalInputParameterException e) {
			}

			try {
				privilege = this.USER_SERVICE.createPrivilege("");
				assertTrue("Can't create privilege with empty privilege name.", false);
			} catch (IllegalInputParameterException e) {
			}

			try {
				privilege = this.USER_SERVICE.createPrivilege("    ");
				assertTrue("Can't create privilege with empty privilege name.", false);
			} catch (IllegalInputParameterException e) {
			}

		} finally {
			if (privilege != null) {
				this.USER_SERVICE.deletePrivilege(privilege);
			}
		}
	}

	@Test
	public void createDuplicatePrivilege() throws UserManagementException {
		String name = "createprivilegetest_6";
		TO<?> privilege = null;

		try {
			// creates privilege 'createprivilegetest_6'
			privilege = this.USER_SERVICE.createPrivilege(name);
			assertNotNull(privilege);

			// check that privilege was created
			privilege = this.USER_SERVICE.getPrivilege(name);
			assertNotNull(privilege);
			assertEquals(name, (String) this.USER_SERVICE.getProperty(privilege, UserManagementService.PRIVILEGE_NAME));

			// tests exception on duplicate privilege
			try {
				this.USER_SERVICE.createPrivilege(name);
				assertTrue("Can't create a duplicate privilege, " + name, false);
			} catch (UserManagementException e) {
			}

		} finally {
			if (privilege != null) {
				this.USER_SERVICE.deletePrivilege(privilege);
			}
			privilege = this.USER_SERVICE.getPrivilege(name);
			assertNull(privilege);
		}
	}

	@Test
	public void deleteCreatedPrivilege() throws UserManagementException {
		String name = "deleteprivilegetest_1";
		TO<?> privilege = null;

		try {
			// creates privilege 'deleteprivilegetest_1'
			privilege = this.USER_SERVICE.createPrivilege(name);
			assertNotNull(privilege);
			assertEquals(name, this.USER_SERVICE.getProperty(privilege, UserManagementService.PRIVILEGE_NAME));

			this.USER_SERVICE.deletePrivilege(privilege);
			privilege = null;

			// check that privilege was deleted
			privilege = this.USER_SERVICE.getPrivilege(name);
			assertNull(privilege);
		} finally {
			if (privilege != null) {
				this.USER_SERVICE.deletePrivilege(privilege);
			}
			privilege = this.USER_SERVICE.getPrivilege(name);
			assertNull(privilege);
		}
	}

	@Test
	public void tryToDeletePrivilegeTwice() throws UserManagementException {
		String name = "deleteprivilegetest_2";
		TO<?> privilege = null;

		try {
			// creates privilege 'deleteprivilegetest_2'
			privilege = this.USER_SERVICE.createPrivilege(name);
			assertNotNull(privilege);
			assertEquals(name, this.USER_SERVICE.getProperty(privilege, UserManagementService.PRIVILEGE_NAME));

			this.USER_SERVICE.deletePrivilege(privilege);

			// check that privilege was deleted
			assertNull(this.USER_SERVICE.getPrivilege(name));

			try {
				this.USER_SERVICE.deletePrivilege(privilege);
			} catch (EJBException | UserManagementException | IllegalInputParameterException e) {
				assertTrue("Should be able to delete the same privilege twice.", false);
			}

			privilege = null;
		} finally {
			if (privilege != null) {
				this.USER_SERVICE.deletePrivilege(privilege);
			}
			privilege = this.USER_SERVICE.getPrivilege(name);
			assertNull(privilege);
		}
	}

	@Test
	public void tryToDeleteUserUsingDeletePrivilege() throws UserManagementException {
		String name = "deleteprivilegetest_3";
		TO<?> user = null;
		TO<?> privilege = null;

		try {
			// creates privilege 'deleteprivilegetest_3'
			privilege = this.USER_SERVICE.createPrivilege(name);
			assertNotNull(privilege);
			assertEquals(name, this.USER_SERVICE.getProperty(privilege, UserManagementService.PRIVILEGE_NAME));

			user = this.USER_SERVICE.createUser("deleteprivilegetest_user1", "new_pass", null);

			try {
				this.USER_SERVICE.deletePrivilege(user);
				assertTrue("Can't delete a user using deletePrivilege.", false);
			} catch (UserManagementException | IllegalInputParameterException e) {
			}
		} finally {
			if (privilege != null) {
				this.USER_SERVICE.deletePrivilege(privilege);
			}
			privilege = this.USER_SERVICE.getPrivilege(name);
			assertNull(privilege);

			if (user != null) {
				this.USER_SERVICE.deleteUser(user);
			}
			user = this.USER_SERVICE.getUser("deleteprivilegetest_user1");
			assertNull(user);
		}
	}

	@Test
	public void tryToDeleteNullPrivilege() throws UserManagementException {
		TO<?> privilege = null;

		try {
			try {
				this.USER_SERVICE.deletePrivilege(privilege);
				assertTrue("Can't delete a null privilege.", false);
			} catch (UserManagementException | IllegalInputParameterException e) {
			}
		} finally {
		}
	}

	@Test
	public void testGetPrivilege() throws Exception {
		TO<?> priv = this.USER_SERVICE.getPrivilege("root_permission");
		assertNotNull(priv);
		assertEquals("root_permission", this.USER_SERVICE.getProperty(priv, UserManagementService.PRIVILEGE_NAME));
	}

	@Test
	public void testGetPrivilegeInvalidName() throws Exception {
		TO<?> priv = this.USER_SERVICE.getPrivilege("DEAD_BEEF");
		assertNull(priv);
	}

	@Test
	public void testGetPrivilegeNullOrEmptyName() throws Exception {
		try {
			TO<?> priv = this.USER_SERVICE.getPrivilege(null);
			assertTrue("Can't get privilege with null name.", false);
		} catch (IllegalInputParameterException e) {
		}

		try {
			TO<?> priv = this.USER_SERVICE.getPrivilege("");
			assertTrue("Can't get privilege with empty name.", false);
		} catch (IllegalInputParameterException e) {
		}

		try {
			TO<?> priv = this.USER_SERVICE.getPrivilege("    ");
			assertTrue("Can't get privilege with empty name.", false);
		} catch (IllegalInputParameterException e) {
		}
	}

	@Test
	public void testUpdatePrivilege() throws Exception {
		TO<?> priv = null;

		try {
			priv = this.USER_SERVICE.createPrivilege("privilegetests_1");
			assertNotNull(priv);

			Map<String, Object> props = CollectionUtils.newMap();
			props.put(UserManagementService.PRIVILEGE_DESCRIPTION, "This is a description.");
			props.put(UserManagementService.PRIVILEGE_TAG, "UNIT_TESTS");
			props.put(UserManagementService.PRIVILEGE_SORT_ORDER, 12345);
			props.put(UserManagementService.PRIVILEGE_SYSTEM, 6);
			this.USER_SERVICE.updatePrivilege(priv, props);

			props.clear();
			props = this.USER_SERVICE.getPrivilegeInformation(priv);
			assertEquals("privilegetests_1", props.get(UserManagementService.PRIVILEGE_NAME));
			assertEquals("This is a description.", props.get(UserManagementService.PRIVILEGE_DESCRIPTION));
			assertEquals("UNIT_TESTS", props.get(UserManagementService.PRIVILEGE_TAG));
			assertEquals(12345, props.get(UserManagementService.PRIVILEGE_SORT_ORDER));
			assertEquals(6, props.get(UserManagementService.PRIVILEGE_SYSTEM));

			this.USER_SERVICE.setProperty(priv, UserManagementService.PRIVILEGE_DESCRIPTION, "A description this is.");
			assertEquals("A description this is.", this.USER_SERVICE.getProperty(priv, UserManagementService.PRIVILEGE_DESCRIPTION));
		} finally {
			if (priv != null) {
				this.USER_SERVICE.deletePrivilege(priv);
			}
		}
	}

	@Test
	public void testUpdatePrivilegeWithInvalidProperty() throws Exception {
		TO<?> priv = null;

		try {
			priv = this.USER_SERVICE.createPrivilege("privilegetests_1");
			assertNotNull(priv);

			Map<String, Object> props = CollectionUtils.newMap();
			props.put("SOME_PROP", "Invalid property");
			try {
				this.USER_SERVICE.updatePrivilege(priv, props);
				assertTrue("Can't add an unknown property name to a privilege.", false);
			} catch (UserManagementException e) {
			}

			try {
				this.USER_SERVICE.setProperty(priv, "SOME_PROP", "Invalid property");
				assertTrue("Can't add an unknown property name to a privilege.", false);
			} catch (UserManagementException e) {
			}
		} finally {
			if (priv != null) {
				this.USER_SERVICE.deletePrivilege(priv);
			}
		}
	}

	@Test
	public void testGetChildrenPrivilegeIds() throws Exception {
		TO<?> standardGroup = this.USER_SERVICE.getGroup("Standard");
		assertNotNull(standardGroup);
		Collection<String> standardPrivileges = this.USER_SERVICE.getGroupPrivilegeIds(standardGroup);
		assertNotNull(standardPrivileges);

		for (String parentName : standardPrivileges) {
			Collection<String> children = this.USER_SERVICE.getChildrenPrivilegeIds(parentName);
			assertNotNull(children);
		}
	}

	@Test
	public void testChangePrivilegeName() throws Exception {
		String name = "privilegetests_2";
		TO<?> privilege = null;

		try {
			privilege = this.USER_SERVICE.createPrivilege(name);
			assertNotNull(privilege);
			assertEquals(name, this.USER_SERVICE.getProperty(privilege, UserManagementService.PRIVILEGE_NAME));

			this.USER_SERVICE.setProperty(privilege, UserManagementService.PRIVILEGE_NAME, "privilegetests_3");
			privilege = this.USER_SERVICE.getPrivilege("privilegetests_3");
			assertNotNull(privilege);
			name = "privilegetests_3";
			assertEquals(name, this.USER_SERVICE.getProperty(privilege, UserManagementService.PRIVILEGE_NAME));

			// Set using updateUser
			Map<String, Object> props = CollectionUtils.newMap();
			props.put(UserManagementService.PRIVILEGE_NAME, "privilegetests_4");
			this.USER_SERVICE.updatePrivilege(privilege, props);
			privilege = this.USER_SERVICE.getPrivilege("privilegetests_4");
			assertNotNull(privilege);
			name = "privilegetests_4";
			assertEquals(name, this.USER_SERVICE.getProperty(privilege, UserManagementService.PRIVILEGE_NAME));

		} finally {
			if (privilege != null) {
				this.USER_SERVICE.deletePrivilege(privilege);
			}
			privilege = this.USER_SERVICE.getPrivilege(name);
			assertNull(privilege);
		}
	}

}
