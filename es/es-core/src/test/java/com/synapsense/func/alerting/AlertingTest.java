package com.synapsense.func.alerting;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.dbunit.JdbcDatabaseTester;
import org.dbunit.dataset.IDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.synapsense.DbConstants;
import com.synapsense.dto.Alert;
import com.synapsense.dto.AlertFilter;
import com.synapsense.dto.AlertFilterBuilder;
import com.synapsense.dto.AlertPriority;
import com.synapsense.dto.AlertType;
import com.synapsense.dto.MessageType;
import com.synapsense.dto.ObjectType;
import com.synapsense.dto.PropertyDescr;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.AlertServiceSystemException;
import com.synapsense.exception.AlertTypeAlreadyExistsException;
import com.synapsense.exception.EnvException;
import com.synapsense.exception.IllegalInputParameterException;
import com.synapsense.exception.NoSuchAlertTypeException;
import com.synapsense.exception.UserManagementException;
import com.synapsense.func.MockConsole;
import com.synapsense.service.AlertService;
import com.synapsense.service.Environment;
import com.synapsense.service.UserManagementService;
import com.synapsense.util.CollectionUtils;

import javax.ejb.EJBAccessException;
import javax.naming.NamingException;
import javax.security.auth.login.LoginException;
import javax.security.sasl.SaslException;

/**
 * Alerting service tests
 * 
 * @author shabanov
 * 
 */
public class AlertingTest {
	private MockConsole console;
	private AlertService ALERTING_SERVICE;
	private Environment ENVIRONMENT;
	private UserManagementService USER_SERVICE;

	private static String TEST_TYPE = "TEST_TYPE";

	private static TO<?> TEST_OBJECT_1;
	private static TO<?> TEST_OBJECT_2;

	private static String TEST_ALERT_TYPE = "TEST_ALERT_TYPE";

	private static AlertingTestHelper helper;

	private static JdbcDatabaseTester databaseTester;
	
	public static void sleep() { 
		sleep(1000);
	}
	
	public static void sleep(long time) {
		try {
			Thread.sleep(time);
		} catch (final InterruptedException e) {
			e.printStackTrace();
		}
	}

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		databaseTester = new JdbcDatabaseTester(DbConstants.DB_DRIVER, DbConstants.DB_CONNECTION, DbConstants.DB_USER,
		        DbConstants.DB_PASSWORD);
		
		IDataSet databaseDataSet = databaseTester.getConnection().createDataSet(new String[] { DbConstants.TBL_HISTORICAL_VALUES });
		DatabaseOperation.TRUNCATE_TABLE.execute(databaseTester.getConnection(), databaseDataSet);

	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		databaseTester.getConnection().createDataSet(new String[] { DbConstants.TBL_HISTORICAL_VALUES });
	}

	@After
	public void afterTest() throws LoginException, EnvException {
		sleep(1000);

		ALERTING_SERVICE.deleteAllAlerts();
		ALERTING_SERVICE.deleteAlertType(TEST_ALERT_TYPE);
		ENVIRONMENT.deleteObjectType(TEST_TYPE);


		console.logout();
		console = null;
		sleep();
	}

	@Before
	public void beforeTest() throws EnvException {
		console = new MockConsole();
		this.login("admin", "admin", true);

		helper = new AlertingTestHelper(ENVIRONMENT, ALERTING_SERVICE);

		final ObjectType testType = ENVIRONMENT.createObjectType(TEST_TYPE);
		testType.getPropertyDescriptors().add(new PropertyDescr("name", String.class.getName()));
		testType.getPropertyDescriptors().add(new PropertyDescr("lastValue", Double.class.getName()));
		testType.getPropertyDescriptors().add(new PropertyDescr("location", String.class.getName()));
		testType.getPropertyDescriptors().add(new PropertyDescr("body", String.class.getName()));
		testType.getPropertyDescriptors().add(new PropertyDescr("header", String.class.getName()));
		testType.getPropertyDescriptors().add(new PropertyDescr("messagetype", String.class.getName()));

		ENVIRONMENT.updateObjectType(testType);

		sleep();

		TEST_OBJECT_1 = ENVIRONMENT.createObject(TEST_TYPE, new ValueTO[] { new ValueTO("name", "new object1"),
				new ValueTO("location", "100 150"), new ValueTO("header", "header"), new ValueTO("body", "body") ,new ValueTO("messagetype", "4") });
		TEST_OBJECT_2 = ENVIRONMENT.createObject(TEST_TYPE, new ValueTO[] { new ValueTO("name", "new object2") });
		ENVIRONMENT.setRelation(TEST_OBJECT_1, TEST_OBJECT_2);

		helper.newPersistentAlertType(TEST_ALERT_TYPE, "description", "MAJOR", true, false,
				TEST_OBJECT_1);
		ALERTING_SERVICE.deleteAllAlerts();

		final Alert alert_2 = new Alert("alert_error", TEST_ALERT_TYPE, new Date(), "problem with dal", TEST_OBJECT_1);
		ALERTING_SERVICE.raiseAlert(alert_2);

		final Alert alert_1 = new Alert("alert_error", "NON", new Date(), "problem with dal", TEST_OBJECT_1);
		ALERTING_SERVICE.raiseAlert(alert_1);

		final Alert alert_3 = new Alert("alert_error", TEST_ALERT_TYPE, new Date(), "problem with dal", TEST_OBJECT_1);
		ALERTING_SERVICE.raiseAlert(alert_3);

		final Alert alert_4 = new Alert("alert_error", TEST_ALERT_TYPE, new Date(), "problem with dal", TEST_OBJECT_2);
		ALERTING_SERVICE.raiseAlert(alert_4);

		final Alert alert_5 = new Alert("alert_error", TEST_ALERT_TYPE, new Date(), "problem with dal", TEST_OBJECT_2);
		ALERTING_SERVICE.raiseAlert(alert_5);

		final Alert alert_7 = new Alert("alert_error", TEST_ALERT_TYPE, new Date(), "problem with dal");
		ALERTING_SERVICE.raiseAlert(alert_7);

		final Alert alert_8 = new Alert("alert_error", TEST_ALERT_TYPE, new Date(), "problem with dal",
		        TOFactory.EMPTY_TO);
		ALERTING_SERVICE.raiseAlert(alert_8);
		// 4 alerts shall be generated
		sleep(1000);
	}

	@Test
	public void testGeneratedAlertContent() {
		// testing macros replacer
		final Collection<Alert> alerts = ALERTING_SERVICE.getActiveAlerts(TEST_OBJECT_1, false);
		assertEquals(2, alerts.size());
		for (final Alert alert : alerts) {
			if (alert.getAlertType().equals(TEST_ALERT_TYPE)) {
				assertEquals("body", alert.getFullMessage());
			}
		}
	}

	@Test
	public void testUpdateAlertTypeNameCacheSyncSuccess() throws NoSuchAlertTypeException {
		try {
			helper.newPersistentAlertType("NEW_ALERT_TYPE", "info", "MAJOR", true, false,
			        TEST_OBJECT_1);

			final Alert alert = new Alert("name", "NEW_ALERT_TYPE", new Date(), "message", TEST_OBJECT_1);
			ALERTING_SERVICE.raiseAlert(alert);
			sleep(500);
			
			final AlertType expected = helper.makeCopy("NEW_ALERT_TYPE_2", "MAJOR", true, true, false);

			ALERTING_SERVICE.updateAlertType("NEW_ALERT_TYPE", expected);
			
			final Collection<Alert> alerts = ALERTING_SERVICE.getActiveAlerts(TEST_OBJECT_1, false);
			assertEquals(3,alerts.size());
			
			boolean flagRenamedAlert = false;
			for (final Alert activeAlert : alerts) {
				flagRenamedAlert = activeAlert.getAlertType().equals(expected.getName());
				if (flagRenamedAlert)
					break;
			}
			assertTrue(flagRenamedAlert);

		} finally {
			ALERTING_SERVICE.deleteAlertType("NEW_ALERT_TYPE_2");
			sleep(500);
			final String[] typeNames = new String[] { "NEW_ALERT_TYPE", "NEW_ALERT_TYPE_2" };
			final Collection<AlertType> types = ALERTING_SERVICE.getAlertTypes(typeNames);
			assertEquals(0, types.size());
		}

	}
	
	@Test
	public void testDeleteAlertTypeHavingActiveAlerts() throws AlertTypeAlreadyExistsException {

		helper.newPersistentAlertType("SOME_TYPE", "MAJOR", true, false);

		final Alert alert_1 = new Alert("alert_error", "SOME_TYPE", new Date(), "problem 1 with dal", TEST_OBJECT_1);
		ALERTING_SERVICE.raiseAlert(alert_1);
		
		final Alert alert_2 = new Alert("alert_error", "SOME_TYPE", new Date(), "problem 2 with dal", TEST_OBJECT_2);
		ALERTING_SERVICE.raiseAlert(alert_2);
		sleep(1500);
		
		Collection<Alert> alerts = ALERTING_SERVICE.getActiveAlerts(TEST_OBJECT_1, true);
		assertEquals(5, alerts.size());
		
		alerts = ALERTING_SERVICE.getAlerts(AlertFilters.allAlertsPropagate(TEST_OBJECT_2));
		assertEquals(2, alerts.size());
		
		ALERTING_SERVICE.deleteAlertType("SOME_TYPE");
		sleep(1500);
		
		alerts = ALERTING_SERVICE.getActiveAlerts(TEST_OBJECT_1, true);
		assertEquals(3, alerts.size());
		
		alerts = ALERTING_SERVICE.getActiveAlerts(TEST_OBJECT_2, false);
		assertEquals(1, alerts.size());
		
		alerts = ALERTING_SERVICE.getAlerts(AlertFilters.allAlertsPropagate(TEST_OBJECT_2));
		assertEquals(2, alerts.size());
	}

	@Test
	public void testDeleteAlertTypeWhenRaisingNewAlerts() throws Exception {

		final TO<?> one = ENVIRONMENT.createObject(TEST_TYPE);
		final TO<?> two = ENVIRONMENT.createObject(TEST_TYPE);

		try {
			helper.newPersistentAlertType("SOME_TYPE", "MAJOR", true, false);

			final Alert alert_1 = new Alert("alert_error", "SOME_TYPE", new Date(), "problem 1 with dal", one);
			ALERTING_SERVICE.raiseAlert(alert_1);

			final Alert alert_2 = new Alert("alert_error", "SOME_TYPE", new Date(), "problem 2 with dal", two);
			ALERTING_SERVICE.raiseAlert(alert_2);

			sleep(1000);
			
			Collection<Alert> alerts = ALERTING_SERVICE.getActiveAlerts(one, false);
			assertEquals(1, alerts.size());

			alerts = ALERTING_SERVICE.getAlerts(AlertFilters.allAlerts(two));
			assertEquals(1, alerts.size());

			Thread raiseT = new Thread(new Runnable() {
				@Override
				public void run() {
					for (int i = 0; i < 2; i++) {
						try {
							ALERTING_SERVICE.raiseAlert(alert_1);
							Thread.yield();
							ALERTING_SERVICE.raiseAlert(alert_2);
						} catch (AlertServiceSystemException e) {
						}
					}
				}

			});

			raiseT.start();
			raiseT.join();
			
			ALERTING_SERVICE.deleteAlertType("SOME_TYPE");
			sleep(1500);

			Collection<AlertType> types = ALERTING_SERVICE.getAlertTypes(new String[] { "SOME_TYPE" });
			assertEquals(0, types.size());

			alerts = ALERTING_SERVICE.getActiveAlerts(one, false);
			if (alerts.size() == 1) {
				Alert a = alerts.iterator().next();
				assertEquals("UNKNOWN", a.getAlertType());
			} else {
				assertEquals(0, alerts.size());
			}

			alerts = ALERTING_SERVICE.getAlerts(AlertFilters.allAlerts(two));
			if (alerts.size() == 1) {
				Alert a = alerts.iterator().next();
				assertEquals("UNKNOWN", a.getAlertType());
			} else {
				assertEquals(0, alerts.size());
			}
		} finally {
			ENVIRONMENT.deleteObject(one);
			ENVIRONMENT.deleteObject(two);
		}
	}

	@Test
	public void testDeleteAlertTypeHavingTwoOrMoreDismissedAlertsOnTheSameObject() throws Exception {

		final TO<?> one = ENVIRONMENT.createObject(TEST_TYPE);
		try {
			helper.newPersistentAlertType("SOME_TYPE", "MAJOR", true, false);

			final Alert alert_1 = new Alert("alert_error", "SOME_TYPE", new Date(), "problem 1 with dal", one);
			ALERTING_SERVICE.raiseAlert(alert_1);
			sleep(800);
			
			Collection<Alert> alerts = ALERTING_SERVICE.getActiveAlerts(one, false);
			assertEquals(1, alerts.size());

			ALERTING_SERVICE.dismissAlert(alerts.iterator().next().getAlertId(), "dismissing alert , try 1");
			sleep(800);
			
			alerts = ALERTING_SERVICE.getActiveAlerts(one, false);
			assertEquals(0, alerts.size());

			alerts = ALERTING_SERVICE.getAlerts(AlertFilters.dismissedAlerts(one));
			assertEquals(1, alerts.size());

			ALERTING_SERVICE.raiseAlert(alert_1);
			sleep(800);
			
			alerts = ALERTING_SERVICE.getActiveAlerts(one, false);
			assertEquals(1, alerts.size());

			ALERTING_SERVICE.dismissAlert(alerts.iterator().next().getAlertId(), "dismissing alert , try 2");
			sleep(800);
			alerts = ALERTING_SERVICE.getActiveAlerts(one, false);
			assertEquals(0, alerts.size());
			
			alerts = ALERTING_SERVICE.getAlerts(AlertFilters.dismissedAlerts(one));
			assertEquals(2, alerts.size());
			try {
				ALERTING_SERVICE.deleteAlertType("SOME_TYPE");
				sleep(800);
			} catch (Exception e) {
				fail(e.getMessage());
			}

			Collection<AlertType> types = ALERTING_SERVICE.getAlertTypes(new String[] { "SOME_TYPE" });
			assertEquals(0, types.size());

		} finally {
			ALERTING_SERVICE.deleteAlertType("SOME_TYPE");
		}
	}

	@Test
	public void testDeleteAlertTypeHavingTwoOrMoreAckAlertsOnTheSameObject() throws Exception {

		final TO<?> one = ENVIRONMENT.createObject(TEST_TYPE);
		try {
			helper.newPersistentAlertType("SOME_TYPE", "MAJOR", true, false);

			final Alert alert_1 = new Alert("alert_error", "SOME_TYPE", new Date(), "problem 1 with dal", one);
			ALERTING_SERVICE.raiseAlert(alert_1);

			sleep(800);
			
			Collection<Alert> alerts = ALERTING_SERVICE.getActiveAlerts(one, false);
			assertEquals(1, alerts.size());

			ALERTING_SERVICE.acknowledgeAlert(alerts.iterator().next().getAlertId(), "ack alert, try 1");
			sleep(800);
			
			alerts = ALERTING_SERVICE.getActiveAlerts(one, false);
			assertEquals(1, alerts.size());

			ALERTING_SERVICE.raiseAlert(alert_1);
			sleep(800);
			
			alerts = ALERTING_SERVICE.getActiveAlerts(one, false);
			assertEquals(1, alerts.size());

			ALERTING_SERVICE.acknowledgeAlert(alerts.iterator().next().getAlertId(), "ack alert, try 2");
			sleep(800);
			
			alerts = ALERTING_SERVICE.getActiveAlerts(one, false);
			assertEquals(1, alerts.size());

			try {
				ALERTING_SERVICE.deleteAlertType("SOME_TYPE");
			} catch (Exception e) {
				fail(e.getMessage());
			}
			sleep(800);
			
			Collection<AlertType> types = ALERTING_SERVICE.getAlertTypes(new String[] { "SOME_TYPE" });
			assertEquals(0, types.size());

		} finally {
			ALERTING_SERVICE.deleteAlertType("SOME_TYPE");
		}
	}

	@Test
	public void testDeleteAlertTypeHavingTwoOrMoreResolvedAlertsOnTheSameObject() throws Exception {

		final TO<?> one = ENVIRONMENT.createObject(TEST_TYPE);
		try {
			helper.newPersistentAlertType("SOME_TYPE", "MAJOR", true, false);

			final Alert alert_1 = new Alert("alert_error", "SOME_TYPE", new Date(), "problem 1 with dal", one);
			ALERTING_SERVICE.raiseAlert(alert_1);
			sleep(800);
			
			Collection<Alert> alerts = ALERTING_SERVICE.getActiveAlerts(one, false);
			assertEquals(1, alerts.size());

			ALERTING_SERVICE.resolveAlert(alerts.iterator().next().getAlertId(), "resolving alert, try 1");
			sleep(800);
			alerts = ALERTING_SERVICE.getActiveAlerts(one, false);
			assertEquals(0, alerts.size());

			ALERTING_SERVICE.raiseAlert(alert_1);
			sleep(800);
			alerts = ALERTING_SERVICE.getActiveAlerts(one, false);
			assertEquals(1, alerts.size());

			ALERTING_SERVICE.resolveAlert(alerts.iterator().next().getAlertId(), "resolving alert, try 2");
			sleep(800);
			alerts = ALERTING_SERVICE.getActiveAlerts(one, false);
			assertEquals(0, alerts.size());

			try {
				ALERTING_SERVICE.deleteAlertType("SOME_TYPE");
			} catch (Exception e) {
				fail(e.getMessage());
			}
			sleep(800);
			
			Collection<AlertType> types = ALERTING_SERVICE.getAlertTypes(new String[] { "SOME_TYPE" });
			assertEquals(0, types.size());

		} finally {
			ALERTING_SERVICE.deleteAlertType("SOME_TYPE");
		}
	}

	@Test
	public void testAlertsDeletionWithoutDismiss() throws EnvException {
		ALERTING_SERVICE.deleteAlerts(TEST_OBJECT_1, true);
		Collection<Alert> alerts = ALERTING_SERVICE.getActiveAlerts(TEST_OBJECT_1, true);
		assertEquals(3, alerts.size());
		int numAlerts = ENVIRONMENT.getPropertyValue(TEST_OBJECT_1, "numAlerts", Integer.class);
		assertEquals(3, numAlerts);

		ALERTING_SERVICE.deleteAlerts(TEST_OBJECT_2, true);
		alerts = ALERTING_SERVICE.getAlerts(AlertFilters.allAlertsPropagate(TEST_OBJECT_2));
		assertEquals(1, alerts.size());

		alerts = ALERTING_SERVICE.getActiveAlerts(TEST_OBJECT_1, true);
		assertEquals(3, alerts.size());
		numAlerts = ENVIRONMENT.getPropertyValue(TEST_OBJECT_1, "numAlerts", Integer.class);
		assertEquals(3, numAlerts);

		alerts = ALERTING_SERVICE.getAlerts(AlertFilters.allAlertsPropagate(TEST_OBJECT_2));
		assertEquals(1, alerts.size());

	}

	@Test
	// bug#1541 new test for checking equality
	/**
	 * Test checks that alerts equals() works well.
	 */
	public void testAlertsEquality() {
		// must be one system alert
		Collection<Alert> systemAlerts = ALERTING_SERVICE.getActiveAlerts(TOFactory.EMPTY_TO, true);
		assertEquals(1, systemAlerts.size());

		// System.out.println(systemAlerts.iterator().next().getFullMessage());
		// only alerts on TEST_OBJECT_1
		Collection<Alert> hostAlerts = ALERTING_SERVICE.getActiveAlerts(TEST_OBJECT_1, false);
		assertEquals(2, hostAlerts.size());
		assertEquals(TEST_OBJECT_1, hostAlerts.iterator().next().getHostTO());

		// from all children of TEST_OBJECT_1
		hostAlerts = ALERTING_SERVICE.getActiveAlerts(TEST_OBJECT_1, true);
		// 2 on test_object_1 + 1 on test_object_2
		assertEquals(3, hostAlerts.size());

		// additional copy of alert_3( beforeTest() ) with changed message to
		// check that message is not used for checking alert equality
		final Alert alert_31 = new Alert("alert_error", TEST_ALERT_TYPE, new Date(), "problem with dal 2",
		        TEST_OBJECT_1);
		ALERTING_SERVICE.raiseAlert(alert_31);
		sleep(800);

		// there is must be only 2 alerts as it has configured in beforeTest()
		hostAlerts = ALERTING_SERVICE.getActiveAlerts(TEST_OBJECT_1, false);
		assertEquals(2, hostAlerts.size());
		assertEquals(TEST_OBJECT_1, hostAlerts.iterator().next().getHostTO());

		// from all children of TEST_OBJECT_1
		hostAlerts = ALERTING_SERVICE.getActiveAlerts(TEST_OBJECT_1, true);
		assertEquals(3, hostAlerts.size());

		// new system alert with the same message as in beforeTest()
		final Alert alert_71 = new Alert("alert_error", TEST_ALERT_TYPE, new Date(), "problem with dal");
		ALERTING_SERVICE.raiseAlert(alert_71);
		sleep(800);
		
		// no alerts must be generated
		systemAlerts = ALERTING_SERVICE.getActiveAlerts(TOFactory.EMPTY_TO, true);
		assertEquals(1, systemAlerts.size());

		// the system alert with changed message , must be generated
		final Alert alert_72 = new Alert("alert_error", TEST_ALERT_TYPE, new Date(), "problem with dal 2");
		ALERTING_SERVICE.raiseAlert(alert_72);
		sleep(800);
		
		// system alert count must be increased
		systemAlerts = ALERTING_SERVICE.getActiveAlerts(TOFactory.EMPTY_TO, true);
		assertEquals(2, systemAlerts.size());

	}

	@Test
	public void testRaiseAlertOfNonExistingType() throws EnvException {
		// the message has changed since it was configured in beforeTest()
		// new message 'no info'
		final Alert alert = new Alert("alert_error", "NO_SUCH_ALERT_TYPE", new Date(), "no info", TEST_OBJECT_1);
		ALERTING_SERVICE.raiseAlert(alert);
		sleep(800);

		// nothing must be raised
		Collection<Alert> alertsOnGeneric = ALERTING_SERVICE.getActiveAlerts(TEST_OBJECT_1, true);
		// 2 on test_object_1 + 1 on test_object_2
		assertEquals(3, alertsOnGeneric.size());

		alertsOnGeneric = ALERTING_SERVICE.getActiveAlerts(TEST_OBJECT_1, false);
		assertEquals(2, alertsOnGeneric.size());

	}

	@Test
	public void testGetActiveAlerts() throws Exception {
		Collection<Alert> alerts = ALERTING_SERVICE.getActiveAlerts(TEST_OBJECT_1, true);
		// 2 on test_object_1 + 1 on test_object_2 + 1 on node + 1 on sensor
		assertEquals(3, alerts.size());
		int numAlerts = ENVIRONMENT.getPropertyValue(TEST_OBJECT_1, "numAlerts", Integer.class);
		assertEquals(3, numAlerts);

		alerts = ALERTING_SERVICE.getActiveAlerts(TEST_OBJECT_1, false);
		assertEquals(2, alerts.size());
		for (final Alert alert : alerts) {
			assertEquals(TEST_OBJECT_1, alert.getHostTO());
		}

		alerts = ALERTING_SERVICE.getActiveAlerts(TEST_OBJECT_2, true);
		assertEquals(1, alerts.size());
		numAlerts = ENVIRONMENT.getPropertyValue(TEST_OBJECT_2, "numAlerts", Integer.class);
		assertEquals(1, numAlerts);

		alerts = ALERTING_SERVICE.getActiveAlerts(TEST_OBJECT_2, false);
		assertEquals(1, alerts.size());
		for (final Alert alert : alerts) {
			assertEquals(TEST_OBJECT_2, alert.getHostTO());
		}

		final Collection<TO<?>> ids = new LinkedList<TO<?>>();
		ids.add(TEST_OBJECT_1);
		ids.add(TEST_OBJECT_2);

		alerts = ALERTING_SERVICE.getActiveAlerts(ids, true);
		assertEquals(3, alerts.size());

		alerts = ALERTING_SERVICE.getActiveAlerts(ids, false);
		assertEquals(3, alerts.size());
	}

	@Test
	public void testDismissAlerts() throws Exception {
		Collection<Alert> alerts = ALERTING_SERVICE.getActiveAlerts(TEST_OBJECT_2, true);
		assertEquals(1, alerts.size());

		final Alert alertToDismiss = alerts.iterator().next();
		ALERTING_SERVICE.dismissAlert(alertToDismiss.getAlertId(), "test");
		sleep(800);

		alerts = ALERTING_SERVICE.getActiveAlerts(TEST_OBJECT_2, true);
		assertEquals(0, alerts.size());
		int numAlerts = ENVIRONMENT.getPropertyValue(TEST_OBJECT_2, "numAlerts", Integer.class);
		assertEquals(0, numAlerts);

		alerts = ALERTING_SERVICE.getActiveAlerts(TEST_OBJECT_2, false);
		assertEquals(0, alerts.size());

		// new alert to dismiss it again
		// it's needed to check that all alerts are returned from service, not
		// only unique
		final Alert alert_4 = new Alert("alert_error", TEST_ALERT_TYPE, new Date(), "problem with dal", TEST_OBJECT_2);
		ALERTING_SERVICE.raiseAlert(alert_4);
		sleep(800);
		
		// check that alert is in db
		alerts = ALERTING_SERVICE.getActiveAlerts(TEST_OBJECT_2, false);
		assertEquals(1, alerts.size());
		// dismiss just created alert
		ALERTING_SERVICE.dismissAlert(alerts.iterator().next(), "test");
		// 2 dismissed alerts should be in db
		sleep(1000);
		
		alerts = ALERTING_SERVICE.getAlerts(AlertFilters.dismissedAlertsPropagate(TEST_OBJECT_2));
		assertEquals(2, alerts.size());
		// 2 alerts in history
		alerts = ALERTING_SERVICE.getAlertsHistory(TEST_OBJECT_2, new Date(0L), new Date(), false);
		assertEquals(2, alerts.size());

		alerts = ALERTING_SERVICE.getAlerts(AlertFilters.dismissedAlertsPropagate(TEST_OBJECT_2));
		assertEquals(2, alerts.size());
		for (final Alert alert : alerts) {
			assertEquals(TEST_OBJECT_2, alert.getHostTO());
		}
		
		alerts = ALERTING_SERVICE.getAlerts( AlertFilters.dismissedAlerts(TEST_OBJECT_2));
		assertEquals(2, alerts.size());
		for (final Alert alert : alerts) {
			assertEquals(TEST_OBJECT_2, alert.getHostTO());
		}

		for (final Alert alert : ALERTING_SERVICE.getActiveAlerts(TEST_OBJECT_1, false)) {
			ALERTING_SERVICE.dismissAlert(alert.getAlertId(), "test");
		}
		
		sleep(2000);

		alerts = ALERTING_SERVICE.getActiveAlerts(TEST_OBJECT_1, true);
		assertEquals(0, alerts.size());
		numAlerts = ENVIRONMENT.getPropertyValue(TEST_OBJECT_1, "numAlerts", Integer.class);
		assertEquals(0, numAlerts);

		alerts = ALERTING_SERVICE.getActiveAlerts(TEST_OBJECT_1, false);
		assertEquals(0, alerts.size());

		alerts = ALERTING_SERVICE.getAlerts(AlertFilters.dismissedAlertsPropagate(TEST_OBJECT_1));
		assertEquals(4, alerts.size());

		alerts = ALERTING_SERVICE.getAlerts(AlertFilters.dismissedAlerts(TEST_OBJECT_1));
		assertEquals(2, alerts.size());
		for (final Alert alert : alerts) {
			assertEquals(TEST_OBJECT_1, alert.getHostTO());
		}

		alerts = ALERTING_SERVICE.getAlerts(AlertFilters.dismissedAlertsPropagate(TEST_OBJECT_1,TEST_OBJECT_2));
		assertEquals(4, alerts.size());

		alerts = ALERTING_SERVICE.getAlerts(AlertFilters.dismissedAlerts(TEST_OBJECT_1,TEST_OBJECT_2));
		assertEquals(4, alerts.size());
	}

	@Test
	public void testGetAlertsHistoryWhenAlertsAreActive() throws Exception {
		final Date beginOfInterval = new Date(System.currentTimeMillis() - 10000);
		final Date endOfInterval = new Date();

		// 2 on test_object_1 + 1 on test_object_2 + 1 on node + 1 on sensor
		Collection<Alert> alerts = ALERTING_SERVICE.getAlertsHistory(TEST_OBJECT_1, beginOfInterval, endOfInterval,
		        true);
		assertEquals(0, alerts.size());

		alerts = ALERTING_SERVICE.getAlertsHistory(TEST_OBJECT_1, beginOfInterval, endOfInterval, false);
		assertEquals(0, alerts.size());

		alerts = ALERTING_SERVICE.getAlertsHistory(TEST_OBJECT_2, beginOfInterval, endOfInterval, true);
		assertEquals(0, alerts.size());

		alerts = ALERTING_SERVICE.getAlertsHistory(TEST_OBJECT_2, beginOfInterval, endOfInterval, false);
		assertEquals(0, alerts.size());
	}
	
	@Test
	public void testGetAlertsHistoryWhenAllAlertsAreDismissed() throws Exception {
		ALERTING_SERVICE.dismissAlerts(TEST_OBJECT_1, true, "test ack");
		sleep(1500);
		
		final Date beginOfInterval = new Date(System.currentTimeMillis() - 10000);
		final Date endOfInterval = new Date(System.currentTimeMillis() + 10000);

		
		Collection<Alert> alerts = ALERTING_SERVICE.getAlertsHistory(TEST_OBJECT_1, beginOfInterval, endOfInterval,
		        true);
		// 2 on test_object_1 + 1 on test_object_2
		assertEquals(3, alerts.size());

		alerts = ALERTING_SERVICE.getAlertsHistory(TEST_OBJECT_1, beginOfInterval, endOfInterval, false);
		assertEquals(2, alerts.size());

		alerts = ALERTING_SERVICE.getAlertsHistory(TEST_OBJECT_2, beginOfInterval, endOfInterval, true);
		assertEquals(1, alerts.size());

		alerts = ALERTING_SERVICE.getAlertsHistory(TEST_OBJECT_2, beginOfInterval, endOfInterval, false);
		assertEquals(1, alerts.size());
	}

	@Test
	public void testHasAlerts() throws Exception {
		// even after deleting alerts from object it has alerts on children
		ALERTING_SERVICE.dismissAlerts(TEST_OBJECT_1, false, "");
		sleep(800);
		// hasAlerts = ALERTING_SERVICE.hasAlerts(TEST_OBJECT_1, true);
		// assertTrue(hasAlerts);

		boolean hasAlerts = ALERTING_SERVICE.hasAlerts(TEST_OBJECT_1, true);
		assertTrue(hasAlerts);

		hasAlerts = ALERTING_SERVICE.hasAlerts(TEST_OBJECT_2, true);
		assertTrue(hasAlerts);

		// even after deleting alerts from object it has alerts on children
		ALERTING_SERVICE.dismissAlerts(TEST_OBJECT_1, false, "");
		sleep(800);
		hasAlerts = ALERTING_SERVICE.hasAlerts(TEST_OBJECT_1, true);
		assertTrue(hasAlerts);

		ALERTING_SERVICE.dismissAlerts(TEST_OBJECT_1, false, "");
		sleep(800);
		hasAlerts = ALERTING_SERVICE.hasAlerts(TEST_OBJECT_1, false);
		assertFalse(hasAlerts);

		ALERTING_SERVICE.dismissAlerts(TEST_OBJECT_2, false, "");
		sleep(800);
		hasAlerts = ALERTING_SERVICE.hasAlerts(TEST_OBJECT_1, true);
		assertFalse(hasAlerts);

		hasAlerts = ALERTING_SERVICE.hasAlerts(TEST_OBJECT_1, false);
		assertFalse(hasAlerts);

		hasAlerts = ALERTING_SERVICE.hasAlerts(TEST_OBJECT_2, true);
		assertFalse(hasAlerts);

		hasAlerts = ALERTING_SERVICE.hasAlerts(TEST_OBJECT_2, false);
		assertFalse(hasAlerts);
	}

	@Test
	public void testGettingAlertsViaEnvironment() throws Exception {
		Collection<Alert> alertsViaAS = ALERTING_SERVICE.getActiveAlerts(TEST_OBJECT_1, true);
		assertNotNull(alertsViaAS);
		assertEquals(3, alertsViaAS.size());

		// getting alerts via env. property 'allAlerts'
		// alerts on test_object_1 and its children
		int numAlerts = ENVIRONMENT.getPropertyValue(TEST_OBJECT_1, "numAlerts", Integer.class);
		assertEquals(3, numAlerts);

		// alerts on test_object_2 and its children

		numAlerts = ENVIRONMENT.getPropertyValue(TEST_OBJECT_2, "numAlerts", Integer.class);
		assertEquals(1, numAlerts);

		// dismiss alerts on test_object_2
		ALERTING_SERVICE.dismissAlerts(TEST_OBJECT_2, false, "test");
		sleep(800);

		// active alerts count must be decreased
		alertsViaAS = ALERTING_SERVICE.getActiveAlerts(TEST_OBJECT_1, true);
		assertEquals(2, alertsViaAS.size());

		alertsViaAS = ALERTING_SERVICE.getActiveAlerts(TEST_OBJECT_2, true);
		assertEquals(0, alertsViaAS.size());

		numAlerts = ENVIRONMENT.getPropertyValue(TEST_OBJECT_1, "numAlerts", Integer.class);
		assertEquals(2, numAlerts);

		// alerts on test_object_2 and its children
		numAlerts = ENVIRONMENT.getPropertyValue(TEST_OBJECT_2, "numAlerts", Integer.class);
		assertEquals(0, numAlerts);

		alertsViaAS = ALERTING_SERVICE.getActiveAlerts(TEST_OBJECT_1, true);
		assertEquals(2, alertsViaAS.size());

		numAlerts = ENVIRONMENT.getPropertyValue(TEST_OBJECT_1, "numAlerts", Integer.class);
		assertEquals(2, numAlerts);

		// ALERTING_SERVICE.dismissAlerts(TEST_OBJECT_1, true);
		for (final Alert alert : ALERTING_SERVICE.getActiveAlerts(TEST_OBJECT_1, true)) {
			ALERTING_SERVICE.dismissAlert(alert.getAlertId(), "test");
		}
		sleep(800);

		alertsViaAS = ALERTING_SERVICE.getActiveAlerts(TEST_OBJECT_1, true);
		assertEquals(0, alertsViaAS.size());

		numAlerts = ENVIRONMENT.getPropertyValue(TEST_OBJECT_1, "numAlerts", Integer.class);
		assertEquals(0, numAlerts);
	}

	@Test
	public void testSystemAlerts() {
		Collection<Alert> alerts = ALERTING_SERVICE.getActiveAlerts(TOFactory.EMPTY_TO, true);
		assertEquals(1, alerts.size());

		alerts = ALERTING_SERVICE.getAlerts(AlertFilters.dismissedSystemAlerts());
		assertEquals(0, alerts.size());

		alerts = ALERTING_SERVICE.getAlertsHistory(TOFactory.EMPTY_TO, new Date(0), new Date(), true);
		// Should be 0 since getAlertsHistory() gets resolved and dismissed alerts, and there are no
		// alerts assigned to EMPTY_TO which are in that state
		assertEquals(0, alerts.size());

		alerts = ALERTING_SERVICE.getAlerts(AlertFilters.allSystemAlerts());
		assertTrue(alerts.size() >= 1);

		ALERTING_SERVICE.dismissAlerts(TOFactory.EMPTY_TO, false, "test");
		sleep(1000);
		
		alerts = ALERTING_SERVICE.getActiveAlerts(TOFactory.EMPTY_TO, true);
		assertEquals(0, alerts.size());

		alerts = ALERTING_SERVICE.getAlerts(AlertFilters.dismissedSystemAlerts());
		assertEquals(1, alerts.size());

		alerts = ALERTING_SERVICE.getAlertsHistory(TOFactory.EMPTY_TO, new Date(0), new Date(), true);
		assertEquals(1, alerts.size());

		alerts = ALERTING_SERVICE.getAlerts(AlertFilters.allSystemAlerts());
		assertTrue(alerts.size()>=1);

		ALERTING_SERVICE.deleteAlerts(TOFactory.EMPTY_TO, false);

		alerts = ALERTING_SERVICE.getActiveAlerts(TOFactory.EMPTY_TO, true);
		assertEquals(0, alerts.size());

		// TODO : 1 dismissed alert is still here because deleteAlerts cannot delete alert from database
		alerts = ALERTING_SERVICE.getAlerts(AlertFilters.dismissedSystemAlerts());
		assertEquals(1, alerts.size());

		alerts = ALERTING_SERVICE.getAlertsHistory(TOFactory.EMPTY_TO, new Date(0), new Date(), true);
		assertEquals(1, alerts.size());

		alerts = ALERTING_SERVICE.getAlerts(AlertFilters.allSystemAlerts());
		assertEquals(1, alerts.size());
	}

	@Test
	/**
	 * Shows that when multiple threads performing alert no duplicates will
	 * appear.
	 */
	public void testParallelAlertsPerforming() throws InterruptedException {
		ALERTING_SERVICE.deleteAllAlerts();

		final Thread t1 = new Thread(new Runnable() {
			@Override
			public void run() {
				final Alert alert = new Alert("alert_error", TEST_ALERT_TYPE, new Date(), "parallel alert 1",
				        TEST_OBJECT_1);
				ALERTING_SERVICE.raiseAlert(alert);
			}
		});

		final Thread t2 = new Thread(new Runnable() {
			@Override
			public void run() {
				final Alert alert = new Alert("alert_error", TEST_ALERT_TYPE, new Date(), "parallel alert 2",
				        TEST_OBJECT_1);
				ALERTING_SERVICE.raiseAlert(alert);
			}
		});

		final Thread t3 = new Thread(new Runnable() {
			@Override
			public void run() {
				final Alert alert = new Alert("alert_error", TEST_ALERT_TYPE, new Date(), "parallel alert 3",
				        TEST_OBJECT_1);
				ALERTING_SERVICE.raiseAlert(alert);
			}
		});

		// starts two threads
		t1.start();
		t2.start();
		t3.start();

		// wait for each other
		t1.join();
		t2.join();
		t3.join();

		sleep(2000);
		
		// checks that alert is only one
		final Collection<Alert> alerts = ALERTING_SERVICE.getActiveAlerts(TEST_OBJECT_1, false);
		assertEquals(1, alerts.size());

	}

	@Test(expected = IllegalInputParameterException.class)
	public void testGetAlertsEmptyIDArrayFailure() throws Exception {
		ALERTING_SERVICE.getAlerts(new Integer[0]);
	}

	@Test(expected = IllegalInputParameterException.class)
	public void testGetAlertsNullIDInArrayFailure() throws Exception {
		final List<Integer> alertIds = CollectionUtils.newList();
		alertIds.add(1);
		alertIds.add(null);

		ALERTING_SERVICE.getAlerts(alertIds.toArray(new Integer[0]));

	}
	
	@Test(expected = IllegalInputParameterException.class)
	public void testGetAlertsZeroIDInArrayFailure() throws Exception {
		final List<Integer> alertIds = CollectionUtils.newList();
		alertIds.add(1);
		alertIds.add(0);
		ALERTING_SERVICE.getAlerts(alertIds.toArray(new Integer[0]));
	}
	
	
	@Test(expected = IllegalInputParameterException.class)
	public void testGetAlertsNegativeIDInArrayFailure() throws Exception {
		final List<Integer> alertIds = CollectionUtils.newList();
		alertIds.add(1);
		alertIds.add(-1);
		ALERTING_SERVICE.getAlerts(alertIds.toArray(new Integer[0]));
	}
	
	@Test(expected = IllegalInputParameterException.class)
	public void testGetAlertsNullInputFailure() throws Exception {
		ALERTING_SERVICE.getAlerts((Integer[]) null);
	}

	@Test
	public void testGetAlertsById() throws Exception {
		final Collection<Alert> allAlerts = ALERTING_SERVICE.getAlerts(AlertFilters.allAlerts(TEST_OBJECT_1));
		final Iterator<Alert> i = allAlerts.iterator();
		final Alert alert1 = i.next();
		final Alert alert2 = i.next();
	
		final List<Integer> alertIds = CollectionUtils.newList();
		alertIds.add(alert1.getAlertId());
		alertIds.add(alert2.getAlertId());
		
		Collection<Alert> getAlerts = ALERTING_SERVICE.getAlerts(alertIds.toArray(new Integer[0]));
		assertEquals(2, getAlerts.size());
		assertTrue(getAlerts.contains(alert1));
		assertTrue(getAlerts.contains(alert2));
		
		getAlerts = ALERTING_SERVICE.getAlerts(new Integer[] { alert1.getAlertId(), Integer.MAX_VALUE });
		assertEquals(1, getAlerts.size());
		assertTrue(getAlerts.contains(alert1));
		
		getAlerts = ALERTING_SERVICE.getAlerts(new Integer[] { Integer.MAX_VALUE });
		assertTrue(getAlerts.isEmpty());

		// querying dismissed alerts (which have user assoc)
		ALERTING_SERVICE.dismissAlert(alert1, "test");
		sleep(800);
		
		// TODO : alert is not accessible by ID after dismiss
		getAlerts = ALERTING_SERVICE.getAlerts(new Integer[] { alert1.getAlertId() });
		assertTrue(getAlerts.isEmpty());
	}

	@Test
	public void testDismissAlertWhenAlreadyAcknowledged() throws Exception {
		TO<?> u = null;

		try {
			Collection<Alert> alerts = ALERTING_SERVICE.getActiveAlerts(TEST_OBJECT_1, false);
			assertEquals(2, alerts.size());
			Iterator<Alert> i = alerts.iterator();
			final Alert alert1 = i.next();
			final Alert alert2 = i.next();

			for (Alert alert : alerts) {
				System.out.println("Alert = " + alert.toString());
				System.out.println("Alert user: " + alert.getUser());
			}

			ALERTING_SERVICE.dismissAlert(alert1, "acknowledgement");
			sleep(1000);

			alerts = ALERTING_SERVICE.getAlerts(AlertFilters.dismissedAlerts(TEST_OBJECT_1));
			assertEquals(1, alerts.size());
			
			Alert dismissedAlert = alerts.iterator().next();
			assertEquals("DIS:acknowledgement", dismissedAlert.getAcknowledgement());
			assertTrue(dismissedAlert.getAcknowledgementTime() != null);
			assertTrue(dismissedAlert.getAcknowledgementTime().getTime() > 0L);
			assertEquals("Admin Admin", dismissedAlert.getUser());
			
			u = USER_SERVICE.createUser("tester", "tester", null);
			USER_SERVICE.setProperty(u, USER_SERVICE.USER_FIRST_NAME, "user");
			USER_SERVICE.setProperty(u, USER_SERVICE.USER_LAST_NAME, "user");
			final TO<?> USER_TESTER = USER_SERVICE.getUser("tester");
			sleep();

			// login with new credentials
			this.login((String)USER_SERVICE.getProperty(USER_TESTER, USER_SERVICE.USER_NAME), "tester", true);

			// dismiss with new credentials
			ALERTING_SERVICE.dismissAlert(alert2, "acknowledgement2");
			sleep(1000);

			alerts = ALERTING_SERVICE.getAlerts(AlertFilters.dismissedAlerts(TEST_OBJECT_1));
			assertEquals(2, alerts.size());
			
			boolean dismissDone = false;
			for (Alert alert : alerts) {
				if (alert.getUser().equals("user user")) {
					assertEquals("DIS:acknowledgement2", alert.getAcknowledgement());
					assertTrue(alert.getAcknowledgementTime() != null);
					assertTrue(alert.getAcknowledgementTime().getTime() > 0L);
					dismissDone = true;
				}
			}
			assertTrue("Failed to dismiss alert " + alert2 ,dismissDone);
		} finally {
			// remove temporary user account
			this.login("admin", "admin", true);
			try {
				USER_SERVICE.deleteUser(u);
			} catch (final UserManagementException e) {
			}
		}
	}

	@Test
	public void testDeleteAlertsHistoryOlderThan() throws Exception {
		Collection<Alert> alerts = ALERTING_SERVICE.getActiveAlerts(TEST_OBJECT_1, false);
		assertEquals(2, alerts.size());
		final Iterator<Alert> i = alerts.iterator();
		final Alert alert1 = i.next();

		ALERTING_SERVICE.dismissAlert(alert1, "test");
		sleep(1000);
		
		Date date = new Date(alert1.getAlertTime() - 1);
		// TODO : cannot delete historical frames anymore
		ALERTING_SERVICE.deleteAlertsHistoryOlderThan(date);

		// returns either resolved and dismissed alerts on test_object_1
		alerts = ALERTING_SERVICE.getAlertsHistory(TEST_OBJECT_1, new Date(0L), new Date(), false);
		assertEquals(1, alerts.size());

		date = new Date(alert1.getAlertTime() + 1);
		// TODO : cannot delete historical frames anymore
		ALERTING_SERVICE.deleteAlertsHistoryOlderThan(date);

		alerts = ALERTING_SERVICE.getAlertsHistory(TEST_OBJECT_1, new Date(0L), new Date(), false);
		assertEquals(1, alerts.size());

		alerts = ALERTING_SERVICE.getActiveAlerts(TEST_OBJECT_1, false);
		assertEquals(1, alerts.size());
	}

	@Test
	public void testNumAlertsCounting() throws Exception {
		final String testType1 = "numAlertsTest1";
		final String testType2 = "numAlertsTest2";
		final String numAlertsProperty = "numAlerts";

		try {
			ENVIRONMENT.createObjectType(testType1);
			ENVIRONMENT.createObjectType(testType2);

			final TO<?> root = ENVIRONMENT.createObject(testType1);
			final TO<?> parent1 = ENVIRONMENT.createObject(testType1);
			final TO<?> parent2 = ENVIRONMENT.createObject(testType2);
			final TO<?> child1 = ENVIRONMENT.createObject(testType1);
			final TO<?> child2 = ENVIRONMENT.createObject(testType2);
			final TO<?> child3 = ENVIRONMENT.createObject(testType2);

			ENVIRONMENT.setRelation(root, parent1);
			ENVIRONMENT.setRelation(root, parent2);
			ENVIRONMENT.setRelation(parent1, child1);
			ENVIRONMENT.setRelation(parent1, child2);
			ENVIRONMENT.setRelation(parent2, child1);
			ENVIRONMENT.setRelation(parent2, child3);

			final Alert alert1 = new Alert("alert_error", TEST_ALERT_TYPE, new Date(), "problem with dal", child1);
			ALERTING_SERVICE.raiseAlert(alert1);

			final Alert alert2 = new Alert("alert_error", TEST_ALERT_TYPE, new Date(), "problem with dal", child2);
			ALERTING_SERVICE.raiseAlert(alert2);

			final Alert alert3 = new Alert("alert_error", TEST_ALERT_TYPE, new Date(), "problem with dal", child3);
			ALERTING_SERVICE.raiseAlert(alert3);

			sleep(1000);
			
			int numAlertsRoot = ENVIRONMENT.getPropertyValue(root, numAlertsProperty, Integer.class);
			assertEquals(3, numAlertsRoot);

			int numAlertsParent1 = ENVIRONMENT.getPropertyValue(parent1, numAlertsProperty, Integer.class);
			assertEquals(2, numAlertsParent1);

			int numAlertsParent2 = ENVIRONMENT.getPropertyValue(parent2, numAlertsProperty, Integer.class);
			assertEquals(2, numAlertsParent2);

			int numAlertsChild1 = ENVIRONMENT.getPropertyValue(child1, numAlertsProperty, Integer.class);
			assertEquals(1, numAlertsChild1);

			int numAlertsChild2 = ENVIRONMENT.getPropertyValue(child2, numAlertsProperty, Integer.class);
			assertEquals(1, numAlertsChild2);

			int numAlertsChild3 = ENVIRONMENT.getPropertyValue(child3, numAlertsProperty, Integer.class);
			assertEquals(1, numAlertsChild3);

			ALERTING_SERVICE.dismissAlerts(child1, false, "test");
			sleep(800);

			numAlertsRoot = ENVIRONMENT.getPropertyValue(root, numAlertsProperty, Integer.class);
			assertEquals(2, numAlertsRoot);

			numAlertsParent1 = ENVIRONMENT.getPropertyValue(parent1, numAlertsProperty, Integer.class);
			assertEquals(1, numAlertsParent1);

			numAlertsParent2 = ENVIRONMENT.getPropertyValue(parent2, numAlertsProperty, Integer.class);
			assertEquals(1, numAlertsParent2);

			numAlertsChild1 = ENVIRONMENT.getPropertyValue(child1, numAlertsProperty, Integer.class);
			assertEquals(0, numAlertsChild1);

			numAlertsChild2 = ENVIRONMENT.getPropertyValue(child2, numAlertsProperty, Integer.class);
			assertEquals(1, numAlertsChild2);

			numAlertsChild3 = ENVIRONMENT.getPropertyValue(child3, numAlertsProperty, Integer.class);
			assertEquals(1, numAlertsChild3);

			ALERTING_SERVICE.raiseAlert(alert1);
			sleep(800);
			numAlertsRoot = ENVIRONMENT.getPropertyValue(root, numAlertsProperty, Integer.class);
			assertEquals(3, numAlertsRoot);

			numAlertsParent1 = ENVIRONMENT.getPropertyValue(parent1, numAlertsProperty, Integer.class);
			assertEquals(2, numAlertsParent1);

			numAlertsParent2 = ENVIRONMENT.getPropertyValue(parent2, numAlertsProperty, Integer.class);
			assertEquals(2, numAlertsParent2);

			numAlertsChild1 = ENVIRONMENT.getPropertyValue(child1, numAlertsProperty, Integer.class);
			assertEquals(1, numAlertsChild1);

			numAlertsChild2 = ENVIRONMENT.getPropertyValue(child2, numAlertsProperty, Integer.class);
			assertEquals(1, numAlertsChild2);

			numAlertsChild3 = ENVIRONMENT.getPropertyValue(child3, numAlertsProperty, Integer.class);
			assertEquals(1, numAlertsChild3);

			ENVIRONMENT.removeRelation(parent1, child1);

			numAlertsRoot = ENVIRONMENT.getPropertyValue(root, numAlertsProperty, Integer.class);
			assertEquals(3, numAlertsRoot);

			numAlertsParent2 = ENVIRONMENT.getPropertyValue(parent2, numAlertsProperty, Integer.class);
			assertEquals(2, numAlertsParent2);

			numAlertsParent1 = ENVIRONMENT.getPropertyValue(parent1, numAlertsProperty, Integer.class);
			assertEquals(1, numAlertsParent1);

			numAlertsChild1 = ENVIRONMENT.getPropertyValue(child1, numAlertsProperty, Integer.class);
			assertEquals(1, numAlertsChild1);

			numAlertsChild2 = ENVIRONMENT.getPropertyValue(child2, numAlertsProperty, Integer.class);
			assertEquals(1, numAlertsChild2);

			numAlertsChild3 = ENVIRONMENT.getPropertyValue(child3, numAlertsProperty, Integer.class);
			assertEquals(1, numAlertsChild3);

			ENVIRONMENT.setRelation(parent1, child1);

			numAlertsRoot = ENVIRONMENT.getPropertyValue(root, numAlertsProperty, Integer.class);
			assertEquals(3, numAlertsRoot);

			numAlertsParent1 = ENVIRONMENT.getPropertyValue(parent1, numAlertsProperty, Integer.class);
			assertEquals(2, numAlertsParent1);

			numAlertsParent2 = ENVIRONMENT.getPropertyValue(parent2, numAlertsProperty, Integer.class);
			assertEquals(2, numAlertsParent2);

			numAlertsChild1 = ENVIRONMENT.getPropertyValue(child1, numAlertsProperty, Integer.class);
			assertEquals(1, numAlertsChild1);

			numAlertsChild2 = ENVIRONMENT.getPropertyValue(child2, numAlertsProperty, Integer.class);
			assertEquals(1, numAlertsChild2);

			numAlertsChild3 = ENVIRONMENT.getPropertyValue(child3, numAlertsProperty, Integer.class);
			assertEquals(1, numAlertsChild3);

			ENVIRONMENT.removeRelation(root, parent1);

			numAlertsRoot = ENVIRONMENT.getPropertyValue(root, numAlertsProperty, Integer.class);
			assertEquals(2, numAlertsRoot);

			numAlertsParent1 = ENVIRONMENT.getPropertyValue(parent1, numAlertsProperty, Integer.class);
			assertEquals(2, numAlertsParent1);

			numAlertsParent2 = ENVIRONMENT.getPropertyValue(parent2, numAlertsProperty, Integer.class);
			assertEquals(2, numAlertsParent2);

			numAlertsChild1 = ENVIRONMENT.getPropertyValue(child1, numAlertsProperty, Integer.class);
			assertEquals(1, numAlertsChild1);

			numAlertsChild2 = ENVIRONMENT.getPropertyValue(child2, numAlertsProperty, Integer.class);
			assertEquals(1, numAlertsChild2);

			numAlertsChild3 = ENVIRONMENT.getPropertyValue(child3, numAlertsProperty, Integer.class);
			assertEquals(1, numAlertsChild3);

			ENVIRONMENT.setRelation(root, parent1);

			numAlertsRoot = ENVIRONMENT.getPropertyValue(root, numAlertsProperty, Integer.class);
			assertEquals(3, numAlertsRoot);

			numAlertsParent1 = ENVIRONMENT.getPropertyValue(parent1, numAlertsProperty, Integer.class);
			assertEquals(2, numAlertsParent1);

			numAlertsParent2 = ENVIRONMENT.getPropertyValue(parent2, numAlertsProperty, Integer.class);
			assertEquals(2, numAlertsParent2);

			numAlertsChild1 = ENVIRONMENT.getPropertyValue(child1, numAlertsProperty, Integer.class);
			assertEquals(1, numAlertsChild1);

			numAlertsChild2 = ENVIRONMENT.getPropertyValue(child2, numAlertsProperty, Integer.class);
			assertEquals(1, numAlertsChild2);

			numAlertsChild3 = ENVIRONMENT.getPropertyValue(child3, numAlertsProperty, Integer.class);
			assertEquals(1, numAlertsChild3);

			ENVIRONMENT.deleteObject(parent1);

			numAlertsRoot = ENVIRONMENT.getPropertyValue(root, numAlertsProperty, Integer.class);
			assertEquals(2, numAlertsRoot);

			numAlertsParent2 = ENVIRONMENT.getPropertyValue(parent2, numAlertsProperty, Integer.class);
			assertEquals(2, numAlertsParent2);

			numAlertsChild1 = ENVIRONMENT.getPropertyValue(child1, numAlertsProperty, Integer.class);
			assertEquals(1, numAlertsChild1);

			numAlertsChild2 = ENVIRONMENT.getPropertyValue(child2, numAlertsProperty, Integer.class);
			assertEquals(1, numAlertsChild2);

			numAlertsChild3 = ENVIRONMENT.getPropertyValue(child3, numAlertsProperty, Integer.class);
			assertEquals(1, numAlertsChild3);

			
		} finally {
			ENVIRONMENT.deleteObjectType(testType1);
			ENVIRONMENT.deleteObjectType(testType2);
		}
	}

	@Test
	public void testDeleteObjectHavingActiveAlertsSuccess() throws Exception {

		ALERTING_SERVICE.deleteAlertType("ATYPE1");
		ALERTING_SERVICE.deleteAlertType("ATYPE2");

		helper.newPersistentAlertType("ATYPE1", "MAJOR", true, false);
		helper.newPersistentAlertType("ATYPE2", "MAJOR", true, false);
		try {
			final TO<?> to1 = ENVIRONMENT.createObject(TEST_TYPE, new ValueTO[] { new ValueTO("name", "new object2") });

			final Thread t1 = new Thread(new Runnable() {
				@Override
				public void run() {
					Alert alert = new Alert("alert_error", "ATYPE1", new Date(), "problem with dal", to1);
					ALERTING_SERVICE.raiseAlert(alert);

					alert = new Alert("alert_error", "ATYPE2", new Date(), "problem with dal", to1);
					ALERTING_SERVICE.raiseAlert(alert);

				}
			});

			t1.start();
			ENVIRONMENT.deleteObject(to1);
			t1.join();
		} finally {
			ALERTING_SERVICE.deleteAlertType("ATYPE1");
			ALERTING_SERVICE.deleteAlertType("ATYPE2");
		}
	}

	@Test
	public void findActiveAlertsByFilter() throws Exception {
		AlertFilter onlyActive = new AlertFilterBuilder().onHost(TEST_OBJECT_1).active().build();
		Collection<Alert> alerts = ALERTING_SERVICE.getAlerts(onlyActive);
		assertEquals(2, alerts.size());
		
		onlyActive = new AlertFilterBuilder().onHost(TEST_OBJECT_2).active().build();
		alerts = ALERTING_SERVICE.getAlerts(onlyActive);
		assertEquals(1, alerts.size());
	}
	
	@Test
	public void findActiveAndHistoricalAlertsByFilter() throws Exception {
		final AlertFilter all =  new AlertFilterBuilder().active().historical().build();
		final Collection<Alert> alerts = ALERTING_SERVICE.getAlerts(all);
		assertEquals(4, alerts.size());
	}
	
	@Test
	public void findAnyAlertsByFilter() throws Exception {
		final Collection<Alert> alerts = ALERTING_SERVICE.getAlerts(new AlertFilterBuilder().build());
		assertEquals(4, alerts.size());
	}
	
	@Test
	public void findOnHostAndSystemAlertsInOneCallByFilter() throws Exception {
		final Collection<Alert> alerts = ALERTING_SERVICE.getAlerts(new AlertFilterBuilder().onHost(TEST_OBJECT_1).system().build());
		assertEquals(3, alerts.size());
	}

	@Test
	public void findAcknowledgedAlertsByFilter() throws Exception {
		final Collection<Alert> alertsOnObject = ALERTING_SERVICE.getActiveAlerts(TEST_OBJECT_1, false);
		ALERTING_SERVICE.acknowledgeAlert(alertsOnObject.iterator().next().getAlertId(), "test");
		sleep(800);

		final AlertFilter onlyAck = new AlertFilterBuilder().onHost(TEST_OBJECT_1).acknowledged().build();

		final Collection<Alert> alerts = ALERTING_SERVICE.getAlerts(onlyAck);
		assertEquals(1, alerts.size());
	}

	@Test
	public void alertFullMessageReplacingWithRealMacros() throws Exception {
		TO<?> admin = USER_SERVICE.getUser("admin");
		assertNotNull(admin);

		USER_SERVICE.setUserPreferenceValue(admin, "UnitsSystem", "SI");
		sleep(1000);

		this.login("admin", "admin", true);
		sleep(1000);

		final String bodyText = "Tier $MSG_TIER$ $ALERT_ID$ System  Alert - in room: $parent(TEST_TYPE).name$ which is located in $parent(TEST_TYPE).location$ \\ $location$ Alert: $ALERT_NAME$ Trigger Value: $MESSAGE$";

		final TO<?> roomRef = ENVIRONMENT.createObject(TEST_TYPE, new ValueTO[] { new ValueTO("name", "ROOM"),
		        new ValueTO("location", "room location") });
		final TO<?> alertHost = ENVIRONMENT.createObject(TEST_TYPE, new ValueTO[]{new ValueTO("location", "x,y"),
				new ValueTO("lastValue", 1.5)});
		ENVIRONMENT.setRelation(roomRef, alertHost);

		final TO<?> templateRef = ENVIRONMENT.createObject("ALERT_MESSAGE_TEMPLATE", new ValueTO[]{
				new ValueTO("name", "email"), new ValueTO("messagetype", "4"), new ValueTO("header", "header"),
				new ValueTO("body", bodyText)});

		helper.newPersistentAlertType("ATYPE1", "info", "MAJOR", true, false, templateRef);

		final Alert alertInstance = new Alert("alertFullMessageReplacingWithRealMacros", "ATYPE1", new Date(), "$"
		        + templateRef.toString() + ".name$", alertHost);
		ALERTING_SERVICE.raiseAlert(alertInstance);

		sleep(800);
		try {
			// testing macros replacer
			final Collection<Alert> alerts = ALERTING_SERVICE.getActiveAlerts(alertHost, false);
			assertEquals(1, alerts.size());
			final Alert raised = alerts.iterator().next();
			assertEquals(
			        "Tier N/A "
			                + raised.getAlertId()
			                + " System  Alert - in room: ROOM which is located in room location \\ x,y Alert: alertFullMessageReplacingWithRealMacros Trigger Value: email",
			        raised.getFullMessage());

		} finally {
			ALERTING_SERVICE.deleteAlertType("ATYPE1");

			ENVIRONMENT.deleteObject(roomRef);
			ENVIRONMENT.deleteObject(templateRef);
			ENVIRONMENT.deleteObject(alertHost);

			USER_SERVICE.removeUserPreference(admin, "UnitsSystem");

		}
	}

	@Test
	public void alertWithDefaultConsoleNotificationTemplate() throws Exception {
		TO<?> admin = USER_SERVICE.getUser("admin");
		assertNotNull(admin);

		USER_SERVICE.setUserPreferenceValue(admin, "UnitsSystem", "SI");

		final TO<?> roomRef = ENVIRONMENT.createObject(TEST_TYPE, new ValueTO[] { new ValueTO("name", "ROOM"),
		        new ValueTO("location", "room location") });
		final TO<?> alertHost = ENVIRONMENT.createObject(TEST_TYPE, new ValueTO[] { new ValueTO("location", "x,y"),
		        new ValueTO("lastValue", 1.5) });
		ENVIRONMENT.setRelation(roomRef, alertHost);

		final TO<?> templateRef = ENVIRONMENT
		        .getObjects("ALERT_MESSAGE_TEMPLATE", new ValueTO[] { new ValueTO("name", "Console Notification") })
		        .iterator().next();
		helper.newPersistentAlertType("ATYPE1", "info", "MAJOR", true, false, templateRef);

		final Alert alertInstance = new Alert("alertWithDefaultConsoleNotificationTemplate", "ATYPE1", new Date(), "$"
		        + templateRef.toString() + ".name$ , $conv(1.5,209)$", alertHost);
		ALERTING_SERVICE.raiseAlert(alertInstance);

		sleep(800);
		
		try {
			// testing macros replacer
			final Collection<Alert> alerts = ALERTING_SERVICE.getActiveAlerts(alertHost, false);
			assertEquals(1, alerts.size());
			
			final Alert raised = alerts.iterator().next();
			assertEquals("Trigger Value: \nCurrent Value: ", raised.getFullMessage());

		} finally {
			ALERTING_SERVICE.deleteAlertType("ATYPE1");
			ENVIRONMENT.deleteObject(roomRef);
			ENVIRONMENT.deleteObject(alertHost);
			USER_SERVICE.removeUserPreference(admin, "UnitsSystem");
		}
	}

	@Test
	public void testDeleteAlertTypeAfterDismissing() throws EnvException {
		final AlertType alertTypeToTest = helper.newPersistentAlertType("NEW_ALERT_TYPE", "MAJOR", true, false);
		final TO<?> roomRef = ENVIRONMENT.createObject(TEST_TYPE, new ValueTO[] { new ValueTO("name", "ROOM"),
		        new ValueTO("location", "room location") });
		try {
			final Alert alert = new Alert("alert_error", alertTypeToTest.getName(), new Date(), "problem with dal",
			        roomRef);
			ALERTING_SERVICE.raiseAlert(alert);
			sleep(1000);

			ALERTING_SERVICE.dismissAlerts(roomRef, false, "test ack");

			ALERTING_SERVICE.deleteAlertType(alertTypeToTest.getName());
			
			// TODO : need to improve test by checking alert type name
			Collection<Alert> activeAlerts = ALERTING_SERVICE.getActiveAlerts(roomRef, false);
			Collection<Alert> dismissedAlerts = ALERTING_SERVICE.getAlerts(AlertFilters.dismissedAlerts(roomRef));
			assertTrue(activeAlerts.isEmpty());
			assertTrue(dismissedAlerts.isEmpty());

		} finally {
			ENVIRONMENT.deleteObject(roomRef);
		}

	}

	@Test
	@Ignore
	public void sendingNotifications() throws Exception {
		TO<?> admin = USER_SERVICE.getUser("admin");
		assertNotNull(admin);

		USER_SERVICE.setUserPreferenceValue(admin, "UnitsSystem", "SI");

		USER_SERVICE.setProperty(admin, USER_SERVICE.USER_PRIMARY_EMAIL, "shabanov@mera.ru");

		final String bodyText = "Tier $MSG_TIER$ : System  Alert - in room , descr : $ALERT_DESCRIPTION$,  $parent(TEST_TYPE).name$ which is located in $parent(TEST_TYPE).location$ \\ $location$ Alert: $ALERT_NAME$ Trigger Value: $TRIGGER_DATA_VALUE$";

		final TO roomRef = ENVIRONMENT.createObject(TEST_TYPE, new ValueTO[] { new ValueTO("name", "ROOM"),
		        new ValueTO("location", "room location") });
		final TO alertHost = ENVIRONMENT.createObject(TEST_TYPE, new ValueTO[] { new ValueTO("location", "x,y"),
		        new ValueTO("lastValue", 1.5) , new ValueTO("name", "Host object name")});
		ENVIRONMENT.setRelation(roomRef, alertHost);

		final TO templateRef = ENVIRONMENT.createObject("ALERT_MESSAGE_TEMPLATE", new ValueTO[] {
		        new ValueTO("name", "email"), new ValueTO("messagetype", "1"), new ValueTO("header", "header"),
		        new ValueTO("body", bodyText) });

		AlertPriority major = ALERTING_SERVICE.getAlertPriority("MAJOR");
		major.getTier("TIER1").setInterval(5000);
		major.getTier("TIER1").setSendAck(true);
		major.getTier("TIER1").setSendResolve(true);
		major.getTier("TIER1").setSendEscalation(true);
		
		major.getTier("TIER2").setInterval(5000);
		major.getTier("TIER2").setSendAck(true);
		major.getTier("TIER2").setSendResolve(true);
		major.getTier("TIER2").setSendEscalation(true);
		
		major.getTier("TIER3").setInterval(5000);
		major.getTier("TIER3").setSendAck(true);
		major.getTier("TIER3").setSendResolve(true);
		major.getTier("TIER3").setSendEscalation(true);
		
		ALERTING_SERVICE.updateAlertPriority("MAJOR", major);

		helper.newPersistentAlertType("ATYPE1", "description", "MAJOR", true, false, templateRef);
		ALERTING_SERVICE.addActionDestination("ATYPE1", "TIER1", MessageType.SMTP, "admin");
		ALERTING_SERVICE.addActionDestination("ATYPE1", "TIER2", MessageType.SMTP, "admin");
		ALERTING_SERVICE.addActionDestination("ATYPE1", "TIER3", MessageType.SMTP, "admin");
		

		final Alert alertInstance = new Alert("alertFullMessageReplacingWithRealMacros", "ATYPE1", new Date(), "$"
		        + templateRef.toString() + ".name$  , $conv(1.5,209)$", alertHost);
		ALERTING_SERVICE.raiseAlert(alertInstance);
		
		try {
			// testing macros replacer
			final Collection<Alert> alerts = ALERTING_SERVICE.getActiveAlerts(alertHost, false);
			assertEquals(1, alerts.size());
			Alert alert = alerts.iterator().next();
			
			ALERTING_SERVICE.acknowledgeAlert(alert.getAlertId(), "acknowledged");
			ALERTING_SERVICE.resolveAlert(alert.getAlertId(), "resolved");
			
		} finally {
			  ALERTING_SERVICE.deleteAlertType("ATYPE1");
			  
			  ENVIRONMENT.deleteObject(roomRef);
			  ENVIRONMENT.deleteObject(templateRef);
			  ENVIRONMENT.deleteObject(alertHost);
			 
		}
	}

	@Test
	public void testAlertAcknowledgmentTextCompounding() throws Exception {
		// time descrete is 1 second
		Date stampBeforeAck = new Date(System.currentTimeMillis()-1000);
		
		Collection<Alert> alerts = ALERTING_SERVICE.getActiveAlerts(TEST_OBJECT_2, false);
		assertEquals(1, alerts.size());
		
		Alert alert = alerts.iterator().next();
		assertNull(alert.getAcknowledgement());
		ALERTING_SERVICE.acknowledgeAlert(alert.getAlertId(), "acknowledge");
		sleep(800);
		
		alerts = ALERTING_SERVICE.getAlerts(new Integer[] { alert.getAlertId() });
		assertEquals(1, alerts.size());
		alert = alerts.iterator().next();
		assertEquals("ACK:acknowledge", alert.getAcknowledgement());
		assertTrue(stampBeforeAck.before(alert.getAcknowledgementTime()));
		assertTrue(new Date().after(alert.getAcknowledgementTime()));
		
		final Integer alertId = alert.getAlertId();
		// different method calling
		alerts = ALERTING_SERVICE.getActiveAlerts(TEST_OBJECT_1, true);
		Alert acknowledgedAlert = CollectionUtils.search(alerts, new CollectionUtils.Predicate<Alert>() {
			@Override
			public boolean evaluate(Alert t) {
				return t.getAlertId().equals(alertId);
			}
		});
		assertEquals("ACK:acknowledge", acknowledgedAlert.getAcknowledgement());
		assertTrue(stampBeforeAck.before(alert.getAcknowledgementTime()));
		assertTrue(new Date().after(alert.getAcknowledgementTime()));
		
		ALERTING_SERVICE.resolveAlert(alert.getAlertId(), "resolve");
		sleep(1000);
		alerts = ALERTING_SERVICE.getAlerts(AlertFilters.allAlerts(TEST_OBJECT_2));
		assertEquals(1, alerts.size());
		alert = alerts.iterator().next();
		assertEquals("ACK:acknowledge; RES:resolve", alert.getAcknowledgement());

		alerts = ALERTING_SERVICE.getActiveAlerts(TEST_OBJECT_1, false);
		assertEquals(2, alerts.size());
		alert = alerts.iterator().next();
		assertNull(alert.getAcknowledgement());

		ALERTING_SERVICE.dismissAlert(alert.getAlertId(), "dismiss");
		sleep(1000);
		alerts = ALERTING_SERVICE.getAlerts(AlertFilters.dismissedAlerts(TEST_OBJECT_1));
		assertEquals(1, alerts.size());
		alert = alerts.iterator().next();
		assertEquals("DIS:dismiss", alert.getAcknowledgement());
	}
	
	@Test
	public void testAlertHistorySpecialCharactersInStrings() throws Exception {
		Collection<Alert> alerts = ALERTING_SERVICE.getActiveAlerts(TEST_OBJECT_2, false);
		assertEquals(1, alerts.size());
		
		Alert alert = alerts.iterator().next();
		assertNull(alert.getAcknowledgement());
		
		ALERTING_SERVICE.dismissAlert(alert.getAlertId(), "acknowledge with special \" characters");
		sleep(1000);
		
		Collection<Alert> history = ALERTING_SERVICE.getAlertsHistory(TEST_OBJECT_2, new Date(System.currentTimeMillis()-3000), new Date(System.currentTimeMillis()+1000), false);
		assertEquals(1, history.size());
		
		Alert historyPoint = history.iterator().next();
		assertEquals("DIS:acknowledge with special \" characters", historyPoint.getAcknowledgement());
	}
	
	@Test
	public void testDeleteAllAlerts() throws Exception {
		for (AlertType at : ALERTING_SERVICE.getAllAlertTypes()) { 
			final Alert alert = new Alert("alert_error", at.getName(), new Date(), "no info", TEST_OBJECT_1);
			ALERTING_SERVICE.raiseAlert(alert);
		}
		sleep(3000);
		System.out.println(ALERTING_SERVICE.getNumActiveAlerts(TEST_OBJECT_1, false));

	}

	@Test(expected = IllegalInputParameterException.class)
	public void testGetNumOfHistoricalAlerts() {
		AlertFilterBuilder afb = new AlertFilterBuilder();
		afb.historical();
		ALERTING_SERVICE.getNumAlerts(afb.build());
	}

	@Test
	public void testGetNumberOfAllOpenedAlerts() {
		AlertFilterBuilder afb = new AlertFilterBuilder();
		afb.opened();

		int num = ALERTING_SERVICE.getNumAlerts(afb.build());
		assertEquals(4, num);
	}

	@Test
	public void testGetNumberOfAcknowledgedAlertsOnObject() {
		for (Alert alert : ALERTING_SERVICE.getActiveAlerts(TEST_OBJECT_1, false)) {
			ALERTING_SERVICE.acknowledgeAlert(alert.getAlertId(), "testGetNumOfAcknowledgedAlerts");
		}

		sleep(1000);

		AlertFilterBuilder afb = new AlertFilterBuilder();
		afb.acknowledged();

		int num = ALERTING_SERVICE.getNumAlerts(afb.build());
		assertEquals(2, num);
	}

	private void login(final String user, final String pwd, boolean loginValid) {
		System.out.println("Logging in as " + user + " / " + pwd);
		try {
			this.console.login(user, pwd);
			this.ENVIRONMENT = this.console.getEnv();
			this.ALERTING_SERVICE = this.console.getAlertingService();
			this.USER_SERVICE = this.console.getUserService();
			assertTrue("SHOULD NOT have been able to login with user: " + user + " and password: " + pwd, loginValid);
		} catch (NamingException | LoginException | SaslException | EJBAccessException e) {
			System.out.println("EXCEPTION: (" + e.getClass().getName() + ") " + e.getLocalizedMessage());
			this.ENVIRONMENT = null;
			this.ALERTING_SERVICE = null;
			this.USER_SERVICE = null;
			assertFalse("SHOULD have been able to login with user: " + user + " and password: " + pwd, loginValid);
		}
	}

}