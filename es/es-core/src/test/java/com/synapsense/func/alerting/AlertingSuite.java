package com.synapsense.func.alerting;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.synapsense.impl.alerting.AlertContentInterceptorTest;
import com.synapsense.impl.alerting.AlertCounterTest;
import com.synapsense.impl.alerting.AlertStatusTest;

@RunWith(Suite.class)
@SuiteClasses({AlertingTest.class, AlertAutodismissTest.class, AlertContentInterceptorTest.class,
        AlertCounterTest.class, AlertDestinationTest.class, AlertHistoryBenchmark.class,
        AlertServiceTimeTest.class, AlertStatusTest.class} )
public class AlertingSuite {
}
