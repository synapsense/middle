package com.synapsense.func.reporting.scheduling;

import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;

import com.synapsense.service.reporting.ReportingService;
import com.synapsense.service.reporting.management.ReportingManagementService;
import com.synapsense.service.reporting.management.ReportingSchedulingService;
import com.synapsense.util.JNDINames;
import com.synapsense.util.ServerLoginModule;

public class FacadeManager {

	private static InitialContext ctx;
	private static ReportingService repServ;
	private static ReportingManagementService repManager;
	private static ReportingSchedulingService repSch;

	public static ReportingManagementService getReportManager() throws Exception {
		if (ctx == null) {
			init();
		}
		return repManager;
	}
	

	public static ReportingSchedulingService getReportScheduler() throws Exception {
		if (ctx == null) {
			init();
		}
		return repSch;
	}
	
	private static void init() {
		final String SERVER_NAME = "localhost";
		final String INITIAL_CONTEXT_FACTORY = "org.jboss.naming.remote.client.InitialContextFactory";
		final String PROVIDER_URL = "remote://" + SERVER_NAME + ":4447";

		Properties connectProperties = new Properties();
		connectProperties.put(Context.INITIAL_CONTEXT_FACTORY, INITIAL_CONTEXT_FACTORY);
		connectProperties.put(Context.PROVIDER_URL, PROVIDER_URL);

		connectProperties.put("jboss.naming.client.ejb.context", "true");
		connectProperties.put("jboss.naming.client.connect.options.org.xnio.Options.SASL_POLICY_NOPLAINTEXT", "false");

		connectProperties.put(Context.SECURITY_PRINCIPAL, "admin");
		connectProperties.put(Context.SECURITY_CREDENTIALS, "admin");
		connectProperties.put("SynapEnvRemote", "SynapServer/Environment/remote");

		connectProperties.put(Context.URL_PKG_PREFIXES, "org.jboss.naming.client:org.jnp.interfaces");

		try {
			ctx = new InitialContext(connectProperties);
			repServ = ServerLoginModule.getProxy(ctx, JNDINames.REPORTING_SERVICE_NAME, ReportingService.class);
			repManager = ServerLoginModule.getProxy(ctx, JNDINames.REPORTING_MANAGEMENT_SERVICE_NAME, ReportingManagementService.class);
			repSch = ServerLoginModule.getProxy(ctx, JNDINames.REPORTING_SCHEDULING_SERVICE_NAME, ReportingSchedulingService.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
