package com.synapsense.func.rulesengine;

import java.util.Set;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.synapsense.dto.CRERule;
import com.synapsense.dto.ObjectType;
import com.synapsense.dto.PropertyDescr;
import com.synapsense.dto.RuleType;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.func.ConsoleApp;
import com.synapsense.service.Environment;
import com.synapsense.service.RulesEngine;

public class RulesCollisionTest {

	private static Environment env;

	private static RulesEngine rulesEngine;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		ConsoleApp.init();
		env = ConsoleApp.env;
		rulesEngine = ConsoleApp.cre;
	}

	private TO<?> topRuleType;
	private TO<?> middleRuleType;
	private TO<?> inputObj, middleObj, topObj;

	@Before
	public void setUp() throws Exception {
		// /// RULE BODY ///////////////////////
		String ruleBody = "import com.synapsense.service.impl.dao.to.PropertyTO \n"
		        + "import com.synapsense.rulesengine.core.environment.* \n" + "import com.synapsense.dto.TO; \n"
		        + "import com.synapsense.service.Environment; \n\n"
		        + "class Generic2InputRule implements RuleAction { \n" + "	private Property input \n"
		        + "	private Property delay \n" +

		        "	public Object run(RuleI triggeredRule, Property calculated) { \n"
		        + "		Environment env = calculated.getEnvironment() \n" + "		 \n"
		        + "		Double val = extractValue(env, input.getValue()) \n"
		        + "		println triggeredRule.getName()+\" - readed \"+val \n" + "		Integer del = delay.getValue()\n"
		        + "		if(del!=0) {\n" + "			println triggeredRule.getName()+\" - sleep for \"+del.toString() \n"
		        + "			Thread.sleep(del) \n" + "		} \n" + "		println triggeredRule.getName()+\" - returning \"+val \n"
		        + "		return val \n" + " \n" + "} \n\n" +

		        "private Double extractValue(env, to) { \n" + "	if(to instanceof TO<?>) { \n"
		        + "		\n  return env.getPropertyValue(to, \"lastValue\", Double.class);  }\n"
		        + "	println \"Cannot extract value from '\" + to + \"'\" \n" + "	return null \n" + "} \n\n" +

		        "public Property getInput() { \n" + "	return input \n" + "} \n\n" +

		        "public void setInput(Property i) { \n" + "	input = i \n" + "} \n\n" +

		        "public Property getDelay() { \n" + "	return delay \n" + "} \n\n" +

		        "public void setDelay(Property d) { \n" + "	delay = d \n" + "} \n\n" + "} \n";

		// Test sensor type
		ObjectType sens_type = env.getObjectType("TSENSOR");
		if (sens_type != null) {
			System.out.println("Deleting type 'TSENSOR' " + sens_type);
			env.deleteObjectType("TSENSOR");
		} else {
			System.out.println("Not deleting type 'TSENSOR' ");
		}

		sens_type = env.createObjectType("TSENSOR");
		Set<PropertyDescr> objPds = sens_type.getPropertyDescriptors();

		objPds.add(new PropertyDescr("name", String.class));
		objPds.add(new PropertyDescr("lastValue", Double.class));
		objPds.add(new PropertyDescr("delay", Integer.class));
		objPds.add(new PropertyDescr("input", TO.class.getName(), false, false));

		env.updateObjectType(sens_type);

		String middleRuleTypeName = "middleRuleType";
		// Create a rule type from Groovy code

		middleRuleType = rulesEngine.getRuleId(middleRuleTypeName);
		if (middleRuleType != null) {
			rulesEngine.removeRuleType(middleRuleType);
		}
		middleRuleType = rulesEngine.addRuleType(new RuleType(middleRuleTypeName, ruleBody, "Groovy", false,
		        "0 0 0 * * ?"));

		String topRuleTypeName = "topRuleType";
		topRuleType = rulesEngine.getRuleId(topRuleTypeName);
		if (topRuleType != null) {
			rulesEngine.removeRuleType(topRuleType);
		}
		topRuleType = rulesEngine.addRuleType(new RuleType(topRuleTypeName, ruleBody, "Groovy", false, "0 0 0 * * ?"));

		// Now let's create some instances

		inputObj = env.createObject("TSENSOR");
		System.out.println("Input = " + TOFactory.getInstance().saveTO(inputObj));
		env.setPropertyValue(inputObj, "name", "Input object");
		env.setPropertyValue(inputObj, "lastValue", 10.0);

		middleObj = env.createObject("TSENSOR");
		System.out.println("Middle = " + TOFactory.getInstance().saveTO(middleObj));
		env.setPropertyValue(middleObj, "name", "Middle object");
		env.setPropertyValue(middleObj, "delay", Integer.valueOf(30));
		env.setPropertyValue(middleObj, "input", inputObj);

		topObj = env.createObject("TSENSOR");
		System.out.println("Top = " + TOFactory.getInstance().saveTO(topObj));
		env.setPropertyValue(topObj, "name", "Top object");
		env.setPropertyValue(topObj, "delay", Integer.valueOf(300));
		env.setPropertyValue(topObj, "input", middleObj);

		String middleRuleName = "middleRule";
		CRERule rule = new CRERule(middleRuleName, middleRuleType, middleObj, "lastValue");
		rule.addSrcProperty(middleObj, "input", "input");
		rule.addSrcProperty(middleObj, "delay", "delay");

		rulesEngine.addRule(rule);

		String topRuleName = "topRule";
		rule = new CRERule(topRuleName, topRuleType, topObj, "lastValue");
		rule.addSrcProperty(topObj, "input", "input");
		rule.addSrcProperty(topObj, "delay", "delay");

		rulesEngine.addRule(rule);
		env.setPropertyValue(middleObj, "input", inputObj);
	}

	@After
	public void tearDown() throws Exception {
		env.deleteObjectType("TSENSOR");
		rulesEngine.removeRuleType(topRuleType);
		rulesEngine.removeRuleType(middleRuleType);
	}

	@Test
	public void testLinkedPropertiesCalculation() throws Exception {
		env.setPropertyValue(inputObj, "lastValue", 20.0);
		Assert.assertEquals(20.0, env.getPropertyValue(topObj, "lastValue", Double.class), 0.0);

		env.setPropertyValue(inputObj, "lastValue", 30.0);
		Assert.assertEquals(30.0, env.getPropertyValue(topObj, "lastValue", Double.class), 0.0);
		Assert.assertEquals(30.0, env.getPropertyValue(middleObj, "lastValue", Double.class), 0.0);
	}

	@Test
	public void testLinkUpdate() throws Exception {
		env.setPropertyValue(inputObj, "lastValue", 20.0);
		Assert.assertEquals(20.0, env.getPropertyValue(topObj, "lastValue", Double.class), 0.0);

		env.setPropertyValue(topObj, "input", inputObj);
		env.setPropertyValue(middleObj, "input", topObj);

		env.setPropertyValue(inputObj, "lastValue", 15.0);
		Assert.assertEquals(15.0, env.getPropertyValue(topObj, "lastValue", Double.class), 0.0);

		env.setPropertyValue(inputObj, "lastValue", 50.0);
		Assert.assertEquals(50.0, env.getPropertyValue(topObj, "lastValue", Double.class), 0.0);
		Assert.assertEquals(50.0, env.getPropertyValue(middleObj, "lastValue", Double.class), 0.0);
	}
}
