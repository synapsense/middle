package com.synapsense.func.environment;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Collection;
import java.util.Date;
import java.util.LinkedList;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.IllegalInputParameterException;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.func.ConsoleApp;
import com.synapsense.service.Environment;
import com.synapsense.service.impl.dao.DataObjectDAO;
import com.synapsense.service.impl.dao.NetworkStatisticDAOIF;
import com.synapsense.service.impl.messages.base.StatisticData;

public class NetStatTest {
	private static Environment ENVIRONMENT;

	private static DataObjectDAO<Integer, StatisticData> NET_STAT_NODE;
	
	private static NetworkStatisticDAOIF<Integer, StatisticData> NETWORK_STATISTIC_SPECIAL;

	private static DataObjectDAO<Integer, StatisticData> NET_STAT_BASE;

	private static TO<Integer> network;

	private static TO<Integer> node;

	private static int NODE_LOGICAL_ID;

	private StatisticData createStatisticData() {
		StatisticData sData = new StatisticData();
		sData.setNetworkId(network.getID());
		sData.setLogicalId(NODE_LOGICAL_ID);
		sData.setNwkTimestamp(new Date());
		return sData;
	}

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		ConsoleApp.init();

		ENVIRONMENT = ConsoleApp.env;

		NET_STAT_NODE = ConsoleApp.NET_STAT_NODE;

		NETWORK_STATISTIC_SPECIAL = ConsoleApp.NET_STAT_NODE_SPECIAL;
		
		NET_STAT_BASE = ConsoleApp.NET_STAT_BASE;

		ENVIRONMENT.createObjectType("test_type");

		network = (TO<Integer>) ENVIRONMENT.createObject("test_type");

		node = (TO<Integer>) ENVIRONMENT.createObject("test_type");

		NODE_LOGICAL_ID = node.getID();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		ENVIRONMENT.deleteObjectType("test_type");
	}

	@Before
	public void beforeTest() throws ObjectNotFoundException {
		Collection<StatisticData> statisticDataList = new LinkedList<StatisticData>();
		StatisticData sData1 = createStatisticData();
		sData1.setDuplicatePackets(1);
		
		statisticDataList.add(sData1);

		StatisticData sData2 = createStatisticData();
		sData2.setForwardingPacketsDropped(1);

		statisticDataList.add(sData2);

		StatisticData sData3 = createStatisticData();
		sData3.setNodeLatency(1000);

		statisticDataList.add(sData3);

		NET_STAT_BASE.addBulk(node.getID(), statisticDataList);

		Collection<ValueTO> baseStatistic = NET_STAT_BASE.getData(NODE_LOGICAL_ID);
		assertNotNull(baseStatistic);
		assertFalse(baseStatistic.isEmpty());
		assertEquals(3, baseStatistic.size());

		NET_STAT_NODE.addBulk(node.getID(), statisticDataList);

		Collection<ValueTO> nodeStatistic = NET_STAT_NODE.getData(NODE_LOGICAL_ID);
		assertNotNull(nodeStatistic);
		assertFalse(nodeStatistic.isEmpty());
		assertEquals(3, nodeStatistic.size());
	}

	@After
	public void afterTest() {
		NET_STAT_NODE.removeData(NODE_LOGICAL_ID);
		NET_STAT_BASE.removeData(NODE_LOGICAL_ID);
	}

	@Test
	public void testAddBaseData() throws ObjectNotFoundException {
		NET_STAT_BASE.removeData(NODE_LOGICAL_ID);

		StatisticData sData = createStatisticData();
		sData.setDuplicatePackets(1);
		sData.setNoiseThresh(10);

		// 16 elements array
		int[] chStatus = new int[] { 2456236, 2, 34560, 80232134, 3524562, 4634231, 150, 0, 0, 0, 0, 0, 0, 0, 0, 123 };
		sData.setChStatus(chStatus);

		NET_STAT_BASE.addData(node.getID(), sData);

		Collection<ValueTO> baseStatistic = NET_STAT_BASE.getData(NODE_LOGICAL_ID);
		assertNotNull(baseStatistic);
		assertFalse(baseStatistic.isEmpty());
		assertEquals(1, baseStatistic.size());

		StatisticData lastStat = (StatisticData) baseStatistic.iterator().next().getValue();
		assertArrayEquals(chStatus, lastStat.getChStatus());
		assertEquals(Integer.valueOf(10), lastStat.getNoiseThresh());
		assertEquals(Integer.valueOf(1), lastStat.getDuplicatePackets());

	}

	@Test
	public void testStatisticDataLimits() {
		StatisticData sData = createStatisticData();
		// null is not supported parameter
		try {
			sData.setChStatus(null);
			assertTrue(false);
		} catch (IllegalArgumentException e) {
		}

		// broken array's size limit
		int[] wrongSizeChStatus = new int[18];
		try {
			sData.setChStatus(wrongSizeChStatus);
			assertTrue(false);
		} catch (IllegalArgumentException e) {
		}

		// arg is ok
		int[] okChStatus = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		sData.setChStatus(okChStatus);
		// internal array must be cloned
		assertTrue(okChStatus != sData.getChStatus());
		// content should be equal
		assertArrayEquals(okChStatus, sData.getChStatus());
	}

	@Test
	public void testAddNodeData() throws ObjectNotFoundException {

		StatisticData sData = createStatisticData();
		sData.setHopCount(10);
		sData.setParentState(10);
		sData.setNoiseThresh(10);
		sData.setRouterState(10);

		// 16 elements array
		int[] chStatus = new int[] { 2456236, 2, 34560, 80232134, 3524562, 4634231, 150, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
		sData.setChStatus(chStatus);

		NET_STAT_NODE.addData(node.getID(), sData);

		Collection<ValueTO> nodeStatistic = NET_STAT_NODE.getData(NODE_LOGICAL_ID);
		assertNotNull(nodeStatistic);
		assertFalse(nodeStatistic.isEmpty());
		assertEquals(4, nodeStatistic.size());

		for (ValueTO nextValue : nodeStatistic) {
			StatisticData lastStat = (StatisticData) nextValue.getValue();
			if (lastStat.getHopCount() == 10) {
				assertArrayEquals(chStatus, lastStat.getChStatus());
				assertEquals(Integer.valueOf(10), lastStat.getParentState());
				assertEquals(Integer.valueOf(10), lastStat.getNoiseThresh());
				assertEquals(Integer.valueOf(10), lastStat.getRouterState());
			}
		}

	}

	@Test(expected = IllegalInputParameterException.class)
	public void testAddBaseDataNullObjectID() throws ObjectNotFoundException {
		StatisticData sData = createStatisticData();
		sData.setDuplicatePackets(1);

		NET_STAT_BASE.addData(null, sData);
	}

	@Test(expected = IllegalInputParameterException.class)
	public void testAddBaseDataNegativeObjectID() throws ObjectNotFoundException {
		StatisticData sData = createStatisticData();
		sData.setDuplicatePackets(1);

		NET_STAT_BASE.addData(0, sData);
	}

	@Test(expected = IllegalInputParameterException.class)
	public void testAddBaseDataNullData() throws ObjectNotFoundException {
		NET_STAT_BASE.addData(node.getID(), null);
	}

	@Test(expected = IllegalInputParameterException.class)
	public void testAddNodeDataNullObjectID() throws ObjectNotFoundException {
		StatisticData sData = createStatisticData();
		sData.setDuplicatePackets(1);

		NET_STAT_NODE.addData(null, sData);
	}

	@Test(expected = IllegalInputParameterException.class)
	public void testAddNodeDataNegativeObjectID() throws ObjectNotFoundException {
		StatisticData sData = createStatisticData();
		sData.setDuplicatePackets(1);

		NET_STAT_NODE.addData(0, sData);
	}

	@Test(expected = IllegalInputParameterException.class)
	public void testAddNodeDataNullData() throws ObjectNotFoundException {
		NET_STAT_NODE.addData(node.getID(), null);
	}

	@Test
	public void testAddBaseDataBulk() throws ObjectNotFoundException {
		Collection<StatisticData> statisticDataList = new LinkedList<StatisticData>();
		StatisticData sData1 = createStatisticData();
		sData1.setDuplicatePackets(1);

		statisticDataList.add(sData1);

		StatisticData sData2 = createStatisticData();
		sData2.setForwardingPacketsDropped(1);

		statisticDataList.add(sData2);

		StatisticData sData3 = createStatisticData();
		sData3.setNodeLatency(1000);

		statisticDataList.add(sData3);

		NET_STAT_BASE.addBulk(node.getID(), statisticDataList);

		Collection<ValueTO> baseStatistic = NET_STAT_BASE.getData(NODE_LOGICAL_ID);
		assertNotNull(baseStatistic);
		assertFalse(baseStatistic.isEmpty());
		assertEquals(6, baseStatistic.size());
	}

	@Test(expected = IllegalInputParameterException.class)
	public void testAddBaseDataBulkNullObjectID() throws ObjectNotFoundException {
		Collection<StatisticData> statisticDataList = new LinkedList<StatisticData>();
		NET_STAT_BASE.addBulk(null, statisticDataList);
	}

	@Test(expected = IllegalInputParameterException.class)
	public void testAddBaseDataBulkNegativeObjectID() throws ObjectNotFoundException {
		Collection<StatisticData> statisticDataList = new LinkedList<StatisticData>();
		NET_STAT_BASE.addBulk(0, statisticDataList);
	}

	@Test(expected = IllegalInputParameterException.class)
	public void testAddBaseDataBulkNullData() throws ObjectNotFoundException {
		NET_STAT_BASE.addBulk(node.getID(), null);
	}

	@Test
	public void testAddNodeDataBulk() throws ObjectNotFoundException {
		Collection<StatisticData> statisticDataList = new LinkedList<StatisticData>();
		StatisticData sData1 = createStatisticData();
		sData1.setDuplicatePackets(1);

		statisticDataList.add(sData1);

		StatisticData sData2 = createStatisticData();
		sData2.setForwardingPacketsDropped(1);

		statisticDataList.add(sData2);

		StatisticData sData3 = createStatisticData();
		sData3.setNodeLatency(1000);

		statisticDataList.add(sData3);

		NET_STAT_NODE.addBulk(node.getID(), statisticDataList);

		Collection<ValueTO> nodeStatistic = NET_STAT_NODE.getData(NODE_LOGICAL_ID);
		assertNotNull(nodeStatistic);
		assertFalse(nodeStatistic.isEmpty());
		assertEquals(6, nodeStatistic.size());

	}

	@Test(expected = IllegalInputParameterException.class)
	public void testAddNodeDataBulkNullObjectID() throws ObjectNotFoundException {
		Collection<StatisticData> statisticDataList = new LinkedList<StatisticData>();
		NET_STAT_NODE.addBulk(null, statisticDataList);
	}

	@Test(expected = IllegalInputParameterException.class)
	public void testAddNodeDataBulkNegativeObjectID() throws ObjectNotFoundException {
		Collection<StatisticData> statisticDataList = new LinkedList<StatisticData>();
		NET_STAT_NODE.addBulk(0, statisticDataList);
	}

	@Test(expected = IllegalInputParameterException.class)
	public void testAddNodeDataBulkNullData() throws ObjectNotFoundException {
		NET_STAT_NODE.addBulk(node.getID(), null);
	}

	@Test
	public void testGetBaseDataFrameById() {
		Collection<ValueTO> baseStatistic = NET_STAT_BASE.getData(NODE_LOGICAL_ID, 10, 0);
		assertNotNull(baseStatistic);
		assertFalse(baseStatistic.isEmpty());
		assertEquals(3, baseStatistic.size());

		baseStatistic = NET_STAT_BASE.getData(NODE_LOGICAL_ID, 1, 0);
		assertNotNull(baseStatistic);
		assertFalse(baseStatistic.isEmpty());
		assertEquals(1, baseStatistic.size());

		baseStatistic = NET_STAT_BASE.getData(NODE_LOGICAL_ID, 10, 2);
		assertNotNull(baseStatistic);
		assertFalse(baseStatistic.isEmpty());
		assertEquals(1, baseStatistic.size());

		baseStatistic = NET_STAT_BASE.getData(NODE_LOGICAL_ID, 10, 3);
		assertNotNull(baseStatistic);
		assertTrue(baseStatistic.isEmpty());
	}

	@Test
	public void testGetNodeDataFrameById() {
		Collection<ValueTO> nodeStatistic = NET_STAT_NODE.getData(NODE_LOGICAL_ID, 10, 0);
		assertNotNull(nodeStatistic);
		assertFalse(nodeStatistic.isEmpty());
		assertEquals(3, nodeStatistic.size());

		nodeStatistic = NET_STAT_NODE.getData(NODE_LOGICAL_ID, 1, 0);
		assertNotNull(nodeStatistic);
		assertFalse(nodeStatistic.isEmpty());
		assertEquals(1, nodeStatistic.size());

		nodeStatistic = NET_STAT_NODE.getData(NODE_LOGICAL_ID, 10, 2);
		assertNotNull(nodeStatistic);
		assertFalse(nodeStatistic.isEmpty());
		assertEquals(1, nodeStatistic.size());

		nodeStatistic = NET_STAT_NODE.getData(NODE_LOGICAL_ID, 10, 3);
		assertNotNull(nodeStatistic);
		assertTrue(nodeStatistic.isEmpty());
	}

	@Test
	public void testGetBaseDataSizeById() throws ObjectNotFoundException {
		int baseAmount = NET_STAT_BASE.getDataSize(NODE_LOGICAL_ID);
		assertEquals(3, baseAmount);
	}

	@Test
	public void testGetNodeDataSizeById() throws ObjectNotFoundException {
		int nodeAmount = NET_STAT_NODE.getDataSize(NODE_LOGICAL_ID);
		assertEquals(3, nodeAmount);
	}

	@Test
	public void testGetBaseDataByTimeInterval() {
		Date start = new Date(0L);
		Date end = new Date();

		Collection<ValueTO> baseStatistic = NET_STAT_BASE.getData(NODE_LOGICAL_ID, start, end);
		assertNotNull(baseStatistic);
		assertFalse(baseStatistic.isEmpty());
		assertEquals(3, baseStatistic.size());
	}

	@Test
	public void testGetNodeDataByTimeInterval() {
		Date start = new Date(0L);
		Date end = new Date();

		Collection<ValueTO> nodeStatistic = NET_STAT_NODE.getData(NODE_LOGICAL_ID, start, end);
		assertNotNull(nodeStatistic);
		assertFalse(nodeStatistic.isEmpty());
		assertEquals(3, nodeStatistic.size());
	}

	@Test
	public void testGetBaseDataFromStartToNumPoints() {
		Date start = new Date(0L);

		Collection<ValueTO> baseStatistic = NET_STAT_BASE.getData(NODE_LOGICAL_ID, start, 1);
		assertNotNull(baseStatistic);
		assertFalse(baseStatistic.isEmpty());
		assertEquals(1, baseStatistic.size());

		baseStatistic = NET_STAT_BASE.getData(NODE_LOGICAL_ID, start, 0);
		assertNotNull(baseStatistic);
		assertTrue(baseStatistic.isEmpty());

		baseStatistic = NET_STAT_BASE.getData(NODE_LOGICAL_ID, start, 2);
		assertNotNull(baseStatistic);
		assertFalse(baseStatistic.isEmpty());
		assertEquals(2, baseStatistic.size());

		baseStatistic = NET_STAT_BASE.getData(NODE_LOGICAL_ID, start, 10);
		assertNotNull(baseStatistic);
		assertFalse(baseStatistic.isEmpty());
		assertEquals(3, baseStatistic.size());
	}

	@Test
	public void testGetNodeDataFromStartToNumPoints() {
		Date start = new Date(0L);

		Collection<ValueTO> nodeStatistic = NET_STAT_NODE.getData(NODE_LOGICAL_ID, start, 1);
		assertNotNull(nodeStatistic);
		assertFalse(nodeStatistic.isEmpty());
		assertEquals(1, nodeStatistic.size());

		nodeStatistic = NET_STAT_NODE.getData(NODE_LOGICAL_ID, start, 0);
		assertNotNull(nodeStatistic);
		assertTrue(nodeStatistic.isEmpty());

		nodeStatistic = NET_STAT_NODE.getData(NODE_LOGICAL_ID, start, 2);
		assertNotNull(nodeStatistic);
		assertFalse(nodeStatistic.isEmpty());
		assertEquals(2, nodeStatistic.size());

		nodeStatistic = NET_STAT_NODE.getData(NODE_LOGICAL_ID, start, 10);
		assertNotNull(nodeStatistic);
		assertFalse(nodeStatistic.isEmpty());
		assertEquals(3, nodeStatistic.size());

	}

	@Test
	public void testRemoveBaseData() {
		Collection<ValueTO> baseStatistic = NET_STAT_BASE.getData(NODE_LOGICAL_ID);
		assertNotNull(baseStatistic);
		assertFalse(baseStatistic.isEmpty());
		assertEquals(3, baseStatistic.size());

		NET_STAT_BASE.removeData(NODE_LOGICAL_ID);

		baseStatistic = NET_STAT_BASE.getData(NODE_LOGICAL_ID);
		assertNotNull(baseStatistic);
		assertTrue(baseStatistic.isEmpty());
	}

	@Test
	public void testRemoveNodeData() {
		Collection<ValueTO> nodeStatistic = NET_STAT_NODE.getData(NODE_LOGICAL_ID);
		assertNotNull(nodeStatistic);
		assertFalse(nodeStatistic.isEmpty());
		assertEquals(3, nodeStatistic.size());

		NET_STAT_NODE.removeData(NODE_LOGICAL_ID);

		nodeStatistic = NET_STAT_NODE.getData(NODE_LOGICAL_ID);
		assertNotNull(nodeStatistic);
		assertTrue(nodeStatistic.isEmpty());
	}
	
	@Test
	public void testGetLatestData() throws Exception {
		Thread.sleep(1000);
		StatisticData sData = createStatisticData();
		sData.setCrcFailPacketCount(10);
		NET_STAT_NODE.addData(NODE_LOGICAL_ID, sData);
		
		ValueTO latestRec = NETWORK_STATISTIC_SPECIAL.getLatestData(NODE_LOGICAL_ID);
		assertNotNull(latestRec);
		StatisticData data = (StatisticData) latestRec.getValue();
		assertEquals((Integer) 10, data.getCrcFailPacketCount());
	}
}
