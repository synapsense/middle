package com.synapsense.func.userservice;

import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.IllegalInputParameterException;
import com.synapsense.exception.UserManagementException;
import com.synapsense.service.UserManagementService;
import com.synapsense.util.CollectionUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.ejb.EJBException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Map;

import static org.junit.Assert.*;

public class GroupTests extends AbstractUserServiceTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		AbstractUserServiceTest.setUpBeforeClass();
	}

	@Before
	public void setUp() throws Exception {
		super.setUp();
	}

	@After
	public void tearDown() throws Exception {
		super.tearDown();
	}

	@Test
	public void testGetAllGroups() throws Exception {
		Collection<TO<?>> allItems = console.getUserService().getAllGroups();
		assertNotNull(allItems);

		Collection<String> names = CollectionUtils.newSet();
		for (TO<?> item : allItems) {
			names.add((String) this.USER_SERVICE.getProperty(item, UserManagementService.GROUP_NAME));
		}

		assertTrue("No Administrators group!", names.contains("Administrators"));
		assertTrue("No Power group!", names.contains("Power"));
		assertTrue("No Standard group!", names.contains("Standard"));
	}

	@Test
	public void createSimpleGroup() throws UserManagementException {
		String name = "creategrouptest_1";
		TO<?> group = null;

		try {
			// creates group 'creategrouptest_1' with empty description
			group = this.USER_SERVICE.createGroup(name, null);
			assertNotNull(group);

			// check that group was created
			group = this.USER_SERVICE.getGroup(name);
			assertNotNull(group);
			assertEquals(name, this.USER_SERVICE.getProperty(group, UserManagementService.GROUP_NAME));
			assertEquals("", this.USER_SERVICE.getProperty(group, UserManagementService.GROUP_DESCRIPTION));
		} finally {
			if (group != null) {
				this.USER_SERVICE.deleteGroup(group);
			}
			group = this.USER_SERVICE.getGroup(name);
			assertNull(group);
		}
	}

	@Test
	public void createSimpleGroupWithDescription() throws UserManagementException {
		String name = "creategrouptest_2";
		String desc = "This is a description";
		TO<?> group = null;

		try {
			// creates group 'creategrouptest_2' with description
			group = this.USER_SERVICE.createGroup(name, desc);
			assertNotNull(group);

			// check that group was created
			group = this.USER_SERVICE.getGroup(name);
			assertNotNull(group);
			assertEquals(name, this.USER_SERVICE.getProperty(group, UserManagementService.GROUP_NAME));
			assertEquals(desc, this.USER_SERVICE.getProperty(group, UserManagementService.GROUP_DESCRIPTION));
		} finally {
			if (group != null) {
				this.USER_SERVICE.deleteGroup(group);
			}
			group = this.USER_SERVICE.getGroup(name);
			assertNull(group);
		}
	}

	@Test
	public void createGroupNoName() throws UserManagementException {
		TO<?> group = null;

		try {
			// creates group null
			// Should fail
			try {
				group = this.USER_SERVICE.createGroup(null, null);
				assertTrue("Can't create user with null group name.", false);
			} catch (IllegalInputParameterException e) {
			}

			try {
				group = this.USER_SERVICE.createGroup("", null);
				assertTrue("Can't create user with empty group name.", false);
			} catch (IllegalInputParameterException e) {
			}

			try {
				group = this.USER_SERVICE.createGroup("   ", null);
				assertTrue("Can't create user with empty group name.", false);
			} catch (IllegalInputParameterException e) {
			}

		} finally {
			if (group != null) {
				this.USER_SERVICE.deleteGroup(group);
			}
		}
	}

	@Test
	public void createDuplicateGroup() throws UserManagementException {
		String name = "creategrouptest_3";
		TO<?> group = null;

		try {
			// creates group 'creategrouptest_1' with empty description
			group = this.USER_SERVICE.createGroup(name, null);
			assertNotNull(group);

			// check that group was created
			group = this.USER_SERVICE.getGroup(name);
			assertNotNull(group);

			// tests exception on duplicate group
			try {
				this.USER_SERVICE.createGroup(name, null);
				assertTrue("Can't create a duplicate group, " + name, false);
			} catch (UserManagementException e) {
			}

		} finally {
			if (group != null) {
				this.USER_SERVICE.deleteGroup(group);
			}
			group = this.USER_SERVICE.getGroup(name);
			assertNull(group);
		}
	}

	@Test
	public void deleteCreatedGroup() throws UserManagementException {
		String name = "deletegrouptest_1";
		TO<?> group = null;

		try {
			// creates group 'deletegrouptest_1'
			group = this.USER_SERVICE.createGroup(name, null);
			assertNotNull(group);
			assertEquals(name, this.USER_SERVICE.getProperty(group, UserManagementService.GROUP_NAME));

			this.USER_SERVICE.deleteGroup(group);
			group = null;

			// check that group was deleted
			group = this.USER_SERVICE.getGroup(name);
			assertNull(group);
		} finally {
			if (group != null) {
				this.USER_SERVICE.deleteGroup(group);
			}
			group = this.USER_SERVICE.getGroup(name);
			assertNull(group);
		}
	}

	@Test
	public void tryToDeleteGroupTwice() throws UserManagementException {
		String name = "deletegrouptest_2";
		TO<?> group = null;

		try {
			// creates group 'deletegrouptest_2'
			group = this.USER_SERVICE.createGroup(name, null);
			assertNotNull(group);
			assertEquals(name, (String) this.USER_SERVICE.getProperty(group, UserManagementService.GROUP_NAME));

			this.USER_SERVICE.deleteGroup(group);

			// check that group was deleted
			assertNull(this.USER_SERVICE.getGroup(name));

			try {
				this.USER_SERVICE.deleteGroup(group);
			} catch (EJBException | UserManagementException | IllegalInputParameterException e) {
				assertTrue("Should be able to delete the same group twice.", false);
			}

			group = null;
		} finally {
			if (group != null) {
				this.USER_SERVICE.deleteGroup(group);
			}
			group = this.USER_SERVICE.getGroup(name);
			assertNull(group);
		}
	}

	@Test
	public void tryToDeleteUserUsingDeleteGroup() throws UserManagementException {
		String name = "deletegrouptest_3";
		TO<?> user = null;
		TO<?> group = null;

		try {
			// creates group 'deletegrouptest_3'
			group = this.USER_SERVICE.createGroup(name, null);
			assertNotNull(group);
			assertEquals(name, this.USER_SERVICE.getProperty(group, UserManagementService.GROUP_NAME));

			user = this.USER_SERVICE.createUser("deletegrouptest_user1", "new_pass", null);

			try {
				this.USER_SERVICE.deleteGroup(user);
				assertTrue("Can't delete a user using deleteGroup.", false);
			} catch (UserManagementException | IllegalInputParameterException e) {
			}
		} finally {
			if (group != null) {
				this.USER_SERVICE.deleteGroup(group);
			}
			group = this.USER_SERVICE.getGroup(name);
			assertNull(group);

			if (user != null) {
				this.USER_SERVICE.deleteUser(user);
			}
			user = this.USER_SERVICE.getUser("deletegrouptest_user1");
			assertNull(user);
		}
	}

	@Test
	public void tryToDeleteNullGroup() throws UserManagementException {
		try {
			this.USER_SERVICE.deleteGroup(null);
			assertTrue("Can't delete a null group.", false);
		} catch (UserManagementException | IllegalInputParameterException e) {
		}
	}

	@Test
	public void tryToDeleteAdministratorsGroup() throws UserManagementException {
		TO<?> group = this.USER_SERVICE.getGroup("Administrators");
		try {
			this.USER_SERVICE.deleteGroup(group);
			assertTrue("Can't delete 'Administrators' group.", false);
		} catch (UserManagementException | IllegalInputParameterException e) {
		}
	}

	@Test
	public void deleteGroupWithUsers() throws UserManagementException {
		String name = "deletegrouptest_4";
		TO<?> group = null;
		TO<?> user1 = null;
		TO<?> user2 = null;

		try {
			// creates group 'deletegrouptest_4'
			group = this.USER_SERVICE.createGroup(name, null);
			assertNotNull(group);
			assertEquals(name, this.USER_SERVICE.getProperty(group, UserManagementService.GROUP_NAME));

			user1 = this.USER_SERVICE.createUser("deletegrouptest_user2", "new_pass", name);
			user2 = this.USER_SERVICE.createUser("deletegrouptest_user3", "new_pass", name);

			try {
				this.USER_SERVICE.deleteGroup(group);
				group = null;
				assertTrue("Can't delete group which contains users.", false);
			} catch (UserManagementException e) {
			}

			this.USER_SERVICE.setUserGroups(user1, "Standard");
			this.USER_SERVICE.setUserGroups(user2, "Standard");

			this.USER_SERVICE.deleteGroup(group);
			group = null;

			// check that group was deleted
			group = this.USER_SERVICE.getGroup(name);
			assertNull(group);

			Collection<String> groups = this.USER_SERVICE.getUserGroupIds(user1);
			assertTrue(!groups.contains(name));

			groups = this.USER_SERVICE.getUserGroupIds(user2);
			assertTrue(!groups.contains(name));

		} finally {
			if (user1 != null) {
				this.USER_SERVICE.deleteUser(user1);
			}
			user1 = this.USER_SERVICE.getUser("deletegrouptest_user1");
			assertNull(user1);

			if (user2 != null) {
				this.USER_SERVICE.deleteUser(user2);
			}
			user2 = this.USER_SERVICE.getUser("deletegrouptest_user1");
			assertNull(user2);
			if (group != null) {
				this.USER_SERVICE.deleteGroup(group);
			}
			group = this.USER_SERVICE.getGroup(name);
			assertNull(group);
		}
	}

	@Test
	public void testGetGroup() throws Exception {
		TO<?> group = this.USER_SERVICE.getGroup("Standard");
		assertNotNull(group);
		assertEquals("Standard", this.USER_SERVICE.getProperty(group, UserManagementService.GROUP_NAME));
	}

	@Test
	public void testGetGroupInvalidName() throws Exception {
		TO<?> group = this.USER_SERVICE.getGroup("DEAD_BEEF");
		assertNull(group);
	}

	@Test
	public void testGetGroupNullOrEmptyName() throws Exception {
		try {
			TO<?> group = this.USER_SERVICE.getGroup(null);
			assertTrue("Can't get group with null name.", false);
		} catch (IllegalInputParameterException e) {
		}

		try {
			TO<?> group = this.USER_SERVICE.getGroup("");
			assertTrue("Can't get group with empty name.", false);
		} catch (IllegalInputParameterException e) {
		}

		try {
			TO<?> group = this.USER_SERVICE.getGroup("    ");
			assertTrue("Can't get group with empty name.", false);
		} catch (IllegalInputParameterException e) {
		}
	}

	@Test
	public void testUpdateGroup() throws Exception {
		TO<?> group = null;

		try {
			group = this.USER_SERVICE.createGroup("grouptests_1", null);
			assertNotNull(group);

			Map<String, Object> props = CollectionUtils.newMap();
			props.put(UserManagementService.GROUP_DESCRIPTION, "This is a description.");
			this.USER_SERVICE.updateGroup(group, props);

			props.clear();
			props = this.USER_SERVICE.getGroupInformation(group);
			assertEquals("grouptests_1", props.get(UserManagementService.GROUP_NAME));
			assertEquals("This is a description.", props.get(UserManagementService.GROUP_DESCRIPTION));

			this.USER_SERVICE.setProperty(group, UserManagementService.GROUP_DESCRIPTION, "A description this is.");
			assertEquals("A description this is.", this.USER_SERVICE.getProperty(group, UserManagementService.GROUP_DESCRIPTION));
		} finally {
			if (group != null) {
				this.USER_SERVICE.deleteGroup(group);
			}
		}
	}

	@Test
	public void testUpdateGroupWithInvalidProp() throws Exception {
		TO<?> group = null;

		try {
			group = this.USER_SERVICE.createGroup("grouptests_2", null);
			assertNotNull(group);

			Map<String, Object> props = CollectionUtils.newMap();
			props.put("SOME_PROP", "Invalid property");
			try {
				this.USER_SERVICE.updateGroup(group, props);
				assertTrue("Can't add an unknown property name to a group.", false);
			} catch (UserManagementException e) {
			}

			try {
				this.USER_SERVICE.setProperty(group, "SOME_PROP", "Invalid property");
				assertTrue("Can't add an unknown property name to a group.", false);
			} catch (UserManagementException e) {
			}
		} finally {
			if (group != null) {
				this.USER_SERVICE.deleteGroup(group);
			}
		}
	}

	@Test
	public void testGetGroupPrivileges() throws Exception {
		TO<?> adminGroup = this.USER_SERVICE.getGroup("Administrators");
		assertNotNull(adminGroup);
		Collection<TO<?>> adminPrivileges = this.USER_SERVICE.getGroupPrivileges(adminGroup);
		assertNotNull(adminPrivileges);

		TO<?> powerGroup = this.USER_SERVICE.getGroup("Power");
		assertNotNull(powerGroup);
		Collection<TO<?>> powerPrivileges = this.USER_SERVICE.getGroupPrivileges(powerGroup);
		assertNotNull(powerPrivileges);

		TO<?> standardGroup = this.USER_SERVICE.getGroup("Standard");
		assertNotNull(standardGroup);
		Collection<TO<?>> standardPrivileges = this.USER_SERVICE.getGroupPrivileges(standardGroup);
		assertNotNull(standardPrivileges);

		assertEquals(1, adminPrivileges.size());
		assertTrue(powerPrivileges.size() > standardPrivileges.size());
	}

	@Test
	public void testGetGroupPrivilegeIds() throws Exception {
		TO<?> adminGroup = this.USER_SERVICE.getGroup("Administrators");
		assertNotNull(adminGroup);
		Collection<String> adminPrivileges = this.USER_SERVICE.getGroupPrivilegeIds(adminGroup);
		assertNotNull(adminPrivileges);

		TO<?> powerGroup = this.USER_SERVICE.getGroup("Power");
		assertNotNull(powerGroup);
		Collection<String> powerPrivileges = this.USER_SERVICE.getGroupPrivilegeIds(powerGroup);
		assertNotNull(powerPrivileges);

		TO<?> standardGroup = this.USER_SERVICE.getGroup("Standard");
		assertNotNull(standardGroup);
		Collection<String> standardPrivileges = this.USER_SERVICE.getGroupPrivilegeIds(standardGroup);
		assertNotNull(standardPrivileges);

		assertEquals(1, adminPrivileges.size());
		assertTrue(powerPrivileges.size() > standardPrivileges.size());

		assertTrue(adminPrivileges.contains("root_permission"));
		assertFalse(powerPrivileges.contains("root_permission"));
		assertFalse(standardPrivileges.contains("root_permission"));

		assertFalse(adminPrivileges.contains("MANAGE_NODES"));
		assertTrue(powerPrivileges.contains("MANAGE_NODES"));
		assertFalse(standardPrivileges.contains("MANAGE_NODES"));

		assertFalse(adminPrivileges.contains("ACCESS_WEBCONSOLE"));
		assertTrue(powerPrivileges.contains("ACCESS_WEBCONSOLE"));
		assertTrue(standardPrivileges.contains("ACCESS_WEBCONSOLE"));
	}

	@Test
	public void testGetGroupUserIds() throws Exception {
		String name = "deletegrouptest_5";
		TO<?> group = null;
		TO<?> user1 = null;
		TO<?> user2 = null;

		try {
			// creates group 'deletegrouptest_5'
			group = this.USER_SERVICE.createGroup(name, null);
			assertNotNull(group);
			assertEquals(name, this.USER_SERVICE.getProperty(group, UserManagementService.GROUP_NAME));

			user1 = this.USER_SERVICE.createUser("deletegrouptest_user4", "new_pass", name);
			user2 = this.USER_SERVICE.createUser("deletegrouptest_user5", "new_pass", name);

			Collection<String> users = this.USER_SERVICE.getGroupUserIds(group);
			assertNotNull(users);
			assertEquals(2, users.size());
			assertTrue(users.contains("deletegrouptest_user4"));
			assertTrue(users.contains("deletegrouptest_user5"));

		} finally {
			if (user1 != null) {
				this.USER_SERVICE.deleteUser(user1);
			}
			user1 = this.USER_SERVICE.getUser("deletegrouptest_user4");
			assertNull(user1);

			if (user2 != null) {
				this.USER_SERVICE.deleteUser(user2);
			}
			user2 = this.USER_SERVICE.getUser("deletegrouptest_user5");
			assertNull(user2);
			if (group != null) {
				this.USER_SERVICE.deleteGroup(group);
			}
			group = this.USER_SERVICE.getGroup(name);
			assertNull(group);
		}
	}

	@Test
	public void testGetGroupUsers() throws Exception {
		String name = "deletegrouptest_6";
		TO<?> group = null;
		TO<?> user1 = null;
		TO<?> user2 = null;

		try {
			// creates group 'deletegrouptest_6'
			group = this.USER_SERVICE.createGroup(name, null);
			assertNotNull(group);
			assertEquals(name, this.USER_SERVICE.getProperty(group, UserManagementService.GROUP_NAME));

			user1 = this.USER_SERVICE.createUser("deletegrouptest_user6", "new_pass", name);
			user2 = this.USER_SERVICE.createUser("deletegrouptest_user7", "new_pass", name);

			Collection<TO<?>> users = this.USER_SERVICE.getGroupUsers(group);
			assertNotNull(users);
			assertEquals(2, users.size());
			assertTrue(users.contains(user1));
			assertTrue(users.contains(user2));

		} finally {
			if (user1 != null) {
				this.USER_SERVICE.deleteUser(user1);
			}
			user1 = this.USER_SERVICE.getUser("deletegrouptest_user6");
			assertNull(user1);

			if (user2 != null) {
				this.USER_SERVICE.deleteUser(user2);
			}
			user2 = this.USER_SERVICE.getUser("deletegrouptest_user7");
			assertNull(user2);
			if (group != null) {
				this.USER_SERVICE.deleteGroup(group);
			}
			group = this.USER_SERVICE.getGroup(name);
			assertNull(group);
		}
	}

	@Test
	public void testSetGroupPrivileges() throws Exception {
		TO<?> group = null;

		try {
			group = this.USER_SERVICE.createGroup("grouptests_3", null);
			assertNotNull(group);

			this.USER_SERVICE.setGroupPrivileges(group, Arrays.asList("ACCESS_WEBCONSOLE", "ACCESS_MANAGE_ES", "MANAGE_REPORT_TEMPLATES"));

			Collection<String> privileges = this.USER_SERVICE.getGroupPrivilegeIds(group);
			assertNotNull(privileges);
			assertEquals(3, privileges.size());
			assertTrue(privileges.contains("ACCESS_WEBCONSOLE"));
			assertTrue(privileges.contains("ACCESS_MANAGE_ES"));
			assertTrue(privileges.contains("MANAGE_REPORT_TEMPLATES"));
		} finally {
			if (group != null) {
				this.USER_SERVICE.deleteGroup(group);
			}
		}
	}

	@Test
	public void testChangeGroupName() throws Exception {
		String name = "grouptests_4";
		TO<?> group = null;

		try {
			group = this.USER_SERVICE.createGroup(name, null);
			assertNotNull(group);
			assertEquals(name, this.USER_SERVICE.getProperty(group, UserManagementService.GROUP_NAME));

			this.USER_SERVICE.setProperty(group, UserManagementService.GROUP_NAME, "usertests_9");
			group = this.USER_SERVICE.getGroup("usertests_9");
			assertNotNull(group);
			name = "usertests_9";
			assertEquals(name, this.USER_SERVICE.getProperty(group, UserManagementService.GROUP_NAME));

			// Set using updateUser
			Map<String, Object> props = CollectionUtils.newMap();
			props.put(UserManagementService.GROUP_NAME, "usertests_10");
			this.USER_SERVICE.updateGroup(group, props);
			group = this.USER_SERVICE.getGroup("usertests_10");
			assertNotNull(group);
			name = "usertests_10";
			assertEquals(name, this.USER_SERVICE.getProperty(group, UserManagementService.GROUP_NAME));

		} finally {
			if (group != null) {
				this.USER_SERVICE.deleteGroup(group);
			}
			group = this.USER_SERVICE.getGroup(name);
			assertNull(group);
		}
	}

}
