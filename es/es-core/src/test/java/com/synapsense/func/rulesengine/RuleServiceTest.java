package com.synapsense.func.rulesengine;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import junit.framework.Assert;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.synapsense.dal.generic.RuleInstanceDAO;
import com.synapsense.dto.CRERule;
import com.synapsense.dto.CRERule.PropertyBinding;
import com.synapsense.dto.ObjectType;
import com.synapsense.dto.PropertyDescr;
import com.synapsense.dto.RuleType;
import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.EnvException;
import com.synapsense.func.ConsoleApp;
import com.synapsense.service.Environment;
import com.synapsense.service.RuleTypeService;

public class RuleServiceTest {
	private static Environment env;
	private static RuleInstanceDAO<Integer> ruleService;
	private static RuleTypeService<Integer> ruleTypeService;

	private static final String TEST_TYPE_NAME = "TEST_TYPE_NAME";
	private static final String TEST_RULE_TYPE_NAME = "TEST_RULE_TYPE";
	private static TO<Integer> TEST_RULE_TYPE;

	private static ObjectType TEST_TYPE;
	private TO<Integer> ri01;
	private TO<?> newObj;
	private String ri01_name;

	private CRERule config01;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		ConsoleApp.init();

		env = ConsoleApp.env;

		ruleService = ConsoleApp.ruleInstanceDAO;

		ruleTypeService = ConsoleApp.ruleTypeService;
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		if (TEST_RULE_TYPE != null) {
			for (TO<Integer> rule : ruleService.getRuleInstancesByType(TEST_RULE_TYPE_NAME)) {
				ruleService.delete(rule.getID());
			}
			ruleTypeService.delete(TEST_RULE_TYPE.getID());
		}
		env.deleteObjectType(TEST_TYPE_NAME);
	}

	@Before
	public void beforeTest() throws EnvException {
		RuleType config_01 = new RuleType(TEST_RULE_TYPE_NAME, "no source", "Java", false);
		TO<Integer> ruleType = ruleTypeService.getRuleTypeByName(TEST_RULE_TYPE_NAME);
		if (ruleType != null) {
			for (TO<Integer> rule : ruleService.getRuleInstancesByType(TEST_RULE_TYPE_NAME)) {
				ruleService.delete(rule.getID());
			}
			ruleTypeService.delete(ruleType.getID());
		}
		TEST_RULE_TYPE = ruleTypeService.create(config_01);

		TEST_TYPE = env.createObjectType(TEST_TYPE_NAME);
		TEST_TYPE.getPropertyDescriptors().add(new PropertyDescr("name", String.class));
		TEST_TYPE.getPropertyDescriptors().add(new PropertyDescr("cracEfficiency", Double.class.getName()));
		TEST_TYPE.getPropertyDescriptors().add(new PropertyDescr("topsensor", TO.class.getName()));
		TEST_TYPE.getPropertyDescriptors().add(new PropertyDescr("bottomsensor", TO.class.getName()));
		TEST_TYPE.getPropertyDescriptors().add(new PropertyDescr("node", TO.class.getName()));

		env.updateObjectType(TEST_TYPE);

		newObj = env.createObject(TEST_TYPE_NAME, new ValueTO[] { new ValueTO("name", "new") });

		ri01_name = "ri01";
		config01 = new CRERule(ri01_name, TEST_RULE_TYPE, newObj, "cracEfficiency");
		config01.addSrcProperty(newObj, "topsensor", "input1");
		config01.addSrcProperty(newObj, "bottomsensor", "input2");
		ri01 = ruleService.create(config01);
	}

	@After
	public void afterTest() throws EnvException {
		if (TEST_RULE_TYPE != null) {
			ruleTypeService.delete(TEST_RULE_TYPE.getID());
		}
		env.deleteObjectType(TEST_TYPE_NAME);
	}

	@Test
	public void testCreateDeleteUpdateRuleType() throws EnvException {
		String rt01_name = "rt01";
		String rt02_name = "rt02";

		RuleType config_01 = new RuleType(rt01_name, "no source", "Java", false);
		TO<Integer> rt01 = ruleTypeService.create(config_01);

		RuleType config_02 = new RuleType(rt02_name, "no source", "Java", false);
		TO<Integer> rt02 = ruleTypeService.create(config_02);

		assertEquals(rt01, ruleTypeService.getRuleTypeByID(rt01.getID()));
		assertEquals(rt01, ruleTypeService.getRuleTypeByName(rt01_name));

		assertEquals(rt02, ruleTypeService.getRuleTypeByID(rt02.getID()));
		assertEquals(rt02, ruleTypeService.getRuleTypeByName(rt02_name));

		ruleTypeService.delete(rt01.getID());
		assertNull(ruleTypeService.getRuleTypeByName(rt01_name));

		ruleTypeService.delete(rt02.getID());
		assertNull(ruleTypeService.getRuleTypeByName(rt02_name));
	}

	@Test
	public void testCreateRule() {
		CRERule rule = ruleService.getRuleDTOById(ri01.getID());
		Assert.assertEquals(config01, rule);
		Assert.assertEquals(config01.getName(), rule.getName());
		Assert.assertEquals(config01.getRuleType(), rule.getRuleType());

		Set<PropertyBinding> initProps = new HashSet<CRERule.PropertyBinding>(config01.getSrcProperties());
		Set<PropertyBinding> resProps = new HashSet<CRERule.PropertyBinding>(rule.getSrcProperties());

		Assert.assertEquals(initProps, resProps);
	}

	@Test
	public void testRuleDeleteAll() throws EnvException {

		ruleService.deleteAll();

		Collection<TO<Integer>> allRules = ruleService.getAllRuleInstances();
		assertTrue(allRules.isEmpty());
	}

	@Test
	public void testRuleTypeDeleteAll() throws EnvException {
		ruleTypeService.deleteAll();

		Collection<TO<Integer>> allRuleTypes = ruleTypeService.getAllRuleTypes();
		assertTrue(allRuleTypes.isEmpty());

		Collection<TO<Integer>> allRules = ruleService.getAllRuleInstances();
		assertTrue(allRules.isEmpty());
	}

	@Test
	public void testGetRuleInstancesByHostObject() throws EnvException {
		Collection<TO<Integer>> allRules = ruleService.getRulesByHostObject(newObj);
		assertNotNull(allRules);
		assertEquals(1, allRules.size());
		assertEquals(ri01, allRules.iterator().next());
		allRules = ruleService.getAllRuleInstances();
		assertNotNull(allRules);
		assertEquals(1, allRules.size());
		assertEquals(ri01, allRules.iterator().next());

		allRules = ruleService.getRuleInstancesByType(TEST_RULE_TYPE_NAME);
		assertNotNull(allRules);
		assertEquals(1, allRules.size());
		assertEquals(ri01, allRules.iterator().next());
	}
}
