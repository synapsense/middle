package com.synapsense.func.environment;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ejb.EJBException;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.synapsense.dto.EnvObject;
import com.synapsense.dto.EnvObjectBundle;
import com.synapsense.dto.ObjectType;
import com.synapsense.dto.PropertyDescr;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOBundle;
import com.synapsense.dto.TOFactory;
import com.synapsense.dto.Tag;
import com.synapsense.dto.TagDescriptor;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.EnvException;
import com.synapsense.exception.PropertyNotFoundException;
import com.synapsense.exception.TagNotFoundException;
import com.synapsense.exception.UnableToConvertTagException;
import com.synapsense.func.ConsoleApp;
import com.synapsense.service.Environment;
import com.synapsense.util.CollectionUtils;

public class EnvironmentCreateObjectTest {

	private static final String TYPE_NAME = "TEST TYPE";
	private static int OBJECT_NUMBER = 100;
	private static Environment env;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		ConsoleApp.init();
		env = ConsoleApp.env;
		env.configurationStart();
		createType(TYPE_NAME);
	}

	@AfterClass
	public static void teardownAfterClass() throws Exception {
		env.deleteObjectType(TYPE_NAME);
		env.configurationComplete();
	}

	@Test
	public void envObjectInitSuccess() throws Exception {
		new EnvObject("TYPE", Arrays.asList(new ValueTO("name", 1)));
	}

	@Test(expected = IllegalArgumentException.class)
	public void envObjectNullTypeNameFailure() throws Exception {
		new EnvObject(null, Collections.<ValueTO> emptyList());
	}

	@Test(expected = IllegalArgumentException.class)
	public void envObjectEmptyTypeNameFailure() throws Exception {
		new EnvObject("", Collections.<ValueTO> emptyList());
	}

	@Test
	public void envObjectNullValuesSuccess() throws Exception {
		EnvObject envObj = new EnvObject("TYPE", null);
		assertNotNull(envObj.getValues());
		assertEquals(0, envObj.getValues().size());
	}

	@Test
	public void envObjectNullValuesAndTagsSuccess() throws Exception {
		EnvObject envObj = new EnvObject("TYPE", null, null);
		assertNotNull(envObj.getValues());
		assertNotNull(envObj.getTags());

		assertTrue(envObj.getValues().isEmpty());
		assertTrue(envObj.getTags().isEmpty());

	}

	@Test
	public void envObjectEmptyValuesSuccess() throws Exception {
		new EnvObject("TYPE", Collections.<ValueTO> emptyList());
	}

	@Test
	public void envObjectEmptyValuesAndTagsSuccess() throws Exception {
		new EnvObject("TYPE", Collections.<ValueTO> emptyList(), Collections.<Tag> emptyList());
	}

	@Test
	public void envObjectBundleInitSuccess() throws Exception {
		EnvObjectBundle objectsToCreate = new EnvObjectBundle();
		assertTrue(objectsToCreate.getLabels().isEmpty());
		assertNull(objectsToCreate.get("fake_label"));
	}

	@Test
	public void envObjectSeedObjectSuccess() throws Exception {
		EnvObjectBundle objectsToCreate = new EnvObjectBundle();
		EnvObject eo = new EnvObject("TYPE", Arrays.asList(new ValueTO("name", 1)));
		objectsToCreate.addObject("label", eo);
		assertEquals(1, objectsToCreate.size());
		assertNotNull(objectsToCreate.get("label"));
		assertEquals(eo, objectsToCreate.get("label"));
	}

	@Test(expected = IllegalArgumentException.class)
	public void envObjectSeedNonUniqLabeledObjectFailure() throws Exception {
		EnvObjectBundle objectsToCreate = new EnvObjectBundle();
		EnvObject eo = new EnvObject("TYPE", Arrays.asList(new ValueTO("name", 1)));
		objectsToCreate.addObject("label", eo);
		objectsToCreate.addObject("label", eo);
	}

	@Test(expected = UnsupportedOperationException.class)
	public void envObjectMutatingBundleViaIteratorFailure() throws Exception {
		EnvObjectBundle objectsToCreate = new EnvObjectBundle();
		EnvObject eo1 = new EnvObject("TYPE", Arrays.asList(new ValueTO("name", 1)));
		new EnvObject("TYPE", Arrays.asList(new ValueTO("name", 2)));
		objectsToCreate.addObject("label1", eo1);
		objectsToCreate.addObject("label2", eo1);
		Iterator i = objectsToCreate.iterator();
		while (i.hasNext()) {
			i.remove();
		}
	}

	@Test(expected = UnsupportedOperationException.class)
	public void envObjectMutatingLabelSetFailure() throws Exception {
		EnvObjectBundle objectsToCreate = new EnvObjectBundle();
		EnvObject eo1 = new EnvObject("TYPE", Arrays.asList(new ValueTO("name", 1)));
		objectsToCreate.addObject("label1", eo1);
		objectsToCreate.getLabels().add("new_label");
	}

	@Test(expected = IllegalArgumentException.class)
	public void newTOBundleNullTypeNameFailure() throws Exception {
		TOBundle to = new TOBundle();
		to.add("1", TOFactory.getInstance().loadTO("T:1"));
		to.add("1", TOFactory.getInstance().loadTO("T:2"));
	}

	@Test
	public void bulkCreateObjectsSuccess() throws Exception {
		EnvObjectBundle objectsToCreate = new EnvObjectBundle();

		int OBJECT_NUMBER = 5;
		for (int i = 0; i < OBJECT_NUMBER; i++) {
			ValueTO vName = new ValueTO("name", "name" + i);
			ValueTO vValue = new ValueTO("value", i + 0.1);
			ValueTO vX = new ValueTO("x", i + 0.1);
			ValueTO vY = new ValueTO("y", i + 0.1);
			Tag xScale = new Tag("x", "scale", 1);
			EnvObject envObj = new EnvObject(TYPE_NAME, Arrays.asList(vName, vValue, vX, vY), Arrays.asList(xScale));
			objectsToCreate.addObject("label:" + i, envObj);
		}

		TOBundle toBundle = env.createObjects(objectsToCreate);
		assertNotNull(toBundle);
		assertEquals(OBJECT_NUMBER, toBundle.size());

		TO<?> first = toBundle.getFirst();
		assertEquals(first, toBundle.get(1));
		assertEquals(first, toBundle.get("label:0"));

		assertEquals("name0", env.getPropertyValue(first, "name", String.class));
		assertEquals(Double.valueOf(0.1), env.getPropertyValue(first, "value", Double.class));
		assertEquals(Double.valueOf(0.1), env.getPropertyValue(first, "x", Double.class));
		assertEquals(Double.valueOf(0.1), env.getPropertyValue(first, "y", Double.class));
		assertEquals(Integer.valueOf(1), env.getTagValue(first, "x", "scale", Integer.class));
		assertNotNull(env.getAllTags(first));
		assertEquals(4, env.getAllTags(first).size());
		assertEquals(2, env.getAllTags(first, "x").size());
		Map<TO<?>, Map<String, Collection<Tag>>> tags = env.getTags(Arrays.<TO<?>> asList(first), Arrays.asList("x"),
		        Arrays.asList("scale"));
		assertNotNull(tags);
		assertEquals(1, tags.size());

		Map<String, Collection<Tag>> tagValues = tags.get(first);
		assertNotNull(tagValues);
		assertEquals(1, tagValues.size());
		assertEquals(1, tagValues.get("x").size());
		assertEquals(1, tagValues.get("x").iterator().next().getValue());

		TO second = toBundle.get(2);
		assertEquals("name1", env.getPropertyValue(second, "name", String.class));
		assertEquals(Double.valueOf(1.1), env.getPropertyValue(second, "value", Double.class));
		assertEquals(Double.valueOf(1.1), env.getPropertyValue(second, "x", Double.class));
		assertEquals(Double.valueOf(1.1), env.getPropertyValue(second, "y", Double.class));

		TO labeled = toBundle.get("label:1");
		TO seek = toBundle.get(2);
		assertEquals(labeled, seek);

		TO last = toBundle.getLast();
		assertEquals(last, toBundle.get(OBJECT_NUMBER));
		assertEquals(last, toBundle.get("label:" + (OBJECT_NUMBER - 1)));
	}

	@Test(expected = IllegalArgumentException.class)
	public void bulkCreateObjectDuplicateValueForObjectsFailure() throws Exception {
		new EnvObjectBundle();

		ValueTO vName = new ValueTO("name", "name");
		new EnvObject(TYPE_NAME, Arrays.asList(vName, vName));
	}

	@Test(expected = IllegalArgumentException.class)
	public void bulkCreateObjectDuplicateTagForObjectsFailure() throws Exception {
		new EnvObjectBundle();

		ValueTO vName = new ValueTO("name", "name");
		Tag xScale = new Tag("x", "scale", 1);
		new EnvObject(TYPE_NAME, Arrays.asList(vName), Arrays.asList(xScale, xScale));
	}

	@Test(expected = EJBException.class)
	public void bulkCreateObjectsIncorrectPropertyValueFailure() throws Exception {
		EnvObjectBundle objectsToCreate = new EnvObjectBundle();

		int OBJECT_NUMBER = 5;
		for (int i = 0; i < OBJECT_NUMBER; i++) {
			ValueTO vName = new ValueTO("name", "name" + i);
			ValueTO vValue = new ValueTO("value", i + 0.1);
			ValueTO vX = new ValueTO("x", i + 0.1);
			ValueTO vY = new ValueTO("y", i + 0.1);
			EnvObject envObj = new EnvObject(TYPE_NAME, Arrays.asList(vName, vValue, vX, vY));
			objectsToCreate.addObject("label:" + i, envObj);
		}

		ValueTO vName = new ValueTO("name", "name");
		ValueTO vValue = new ValueTO("value", Double.NaN);
		objectsToCreate.addObject("label:5", new EnvObject(TYPE_NAME, Arrays.asList(vName, vValue)));

		env.createObjects(objectsToCreate);
	}

	@Test(expected = PropertyNotFoundException.class)
	public void bulkCreateObjectsWrongPropertyNameForTagFailure() throws Exception {
		EnvObjectBundle objectsToCreate = new EnvObjectBundle();
		ValueTO vName = new ValueTO("name", "name");
		ValueTO vValue = new ValueTO("value", 1.1);
		ValueTO vX = new ValueTO("x", 1.1);
		ValueTO vY = new ValueTO("y", 1.1);
		Tag xScale = new Tag("fake", "scale", 1.2);

		EnvObject envObj = new EnvObject(TYPE_NAME, Arrays.asList(vName, vValue, vX, vY), Arrays.asList(xScale));

		objectsToCreate.addObject("label:1", envObj);

		env.createObjects(objectsToCreate);
	}

	@Test(expected = TagNotFoundException.class)
	public void bulkCreateObjectsWrongTagNameFailure() throws Exception {
		EnvObjectBundle objectsToCreate = new EnvObjectBundle();
		ValueTO vName = new ValueTO("name", "name");
		ValueTO vValue = new ValueTO("value", 1.1);
		ValueTO vX = new ValueTO("x", 1.1);
		ValueTO vY = new ValueTO("y", 1.1);
		Tag xScale = new Tag("x", "fake", 1.2);

		EnvObject envObj = new EnvObject(TYPE_NAME, Arrays.asList(vName, vValue, vX, vY), Arrays.asList(xScale));

		objectsToCreate.addObject("label:1", envObj);

		env.createObjects(objectsToCreate);
	}

	@Test(expected = UnableToConvertTagException.class)
	public void bulkCreateObjectsIncorrectTypeOfTagValueFailure() throws Exception {
		EnvObjectBundle objectsToCreate = new EnvObjectBundle();

		ValueTO vName = new ValueTO("name", "name");
		ValueTO vValue = new ValueTO("value", 1.1);
		ValueTO vX = new ValueTO("x", 1.1);
		ValueTO vY = new ValueTO("y", 1.1);
		Tag xScale = new Tag("x", "scale", 1.2);

		EnvObject envObj = new EnvObject(TYPE_NAME, Arrays.asList(vName, vValue, vX, vY), Arrays.asList(xScale));
		objectsToCreate.addObject("label:1", envObj);

		env.createObjects(objectsToCreate);
	}

	@Test
	@Ignore
	public void simpleCreateObjectsTimeLimited() throws Exception {
		List<ValueTO[]> values = new LinkedList<ValueTO[]>();
		for (int i = 0; i < OBJECT_NUMBER; i++) {
			ValueTO vName = new ValueTO("name", "name" + i);
			ValueTO vValue = new ValueTO("value", i + 0.1);
			ValueTO vX = new ValueTO("x", i + 0.1);
			ValueTO vY = new ValueTO("y", i + 0.1);
			ValueTO vZ = new ValueTO("z", i + 0.1);
			ValueTO vTag = new ValueTO("tag", "tag" + i);
			ValueTO vImage = new ValueTO("image", "image" + i);
			ValueTO vInt = new ValueTO("int", i);
			values.add(new ValueTO[] { vName, vValue, vX, vY, vZ, vTag, vImage, vInt });
		}

		Tag imageScale = new Tag("image", "scale", 1.0);
		Tag imageRes = new Tag("image", "resolution", 1L);

		Tag xScale = new Tag("x", "scale", 1);
		Tag xSystem = new Tag("x", "system", "res_1");

		Set<Tag> tagsToSet = CollectionUtils.newSet();
		tagsToSet.add(imageScale);
		tagsToSet.add(imageRes);
		tagsToSet.add(xScale);
		tagsToSet.add(xSystem);

		long start = System.currentTimeMillis();
		for (ValueTO[] nextValues : values) {
			TO newObject = env.createObject(TYPE_NAME, nextValues);
			env.setAllTagsValues(newObject, tagsToSet);
		}
		System.out.println("simple create : " + (System.currentTimeMillis() - start));

		Map<TO<?>, Collection<Tag>> tags = env.getAllTags(TYPE_NAME);
		assertEquals(OBJECT_NUMBER + 5, tags.size());
		for (Map.Entry<TO<?>, Collection<Tag>> entry : tags.entrySet()) {
			assertEquals(4, entry.getValue().size());
		}
	}

	@Test
	@Ignore
	public void bulkCreateObjectsTimeLimited() throws Exception {
		long start = System.currentTimeMillis();
		int thNum = 1;
		List<Thread> ths = new LinkedList<Thread>();
		for (int i = 0; i < thNum; i++) {
			Thread t = startCreateThread(OBJECT_NUMBER);
			ths.add(t);
		}
		for (Thread t : ths) {
			t.join();
		}
		System.out.println("bulk create : " + (System.currentTimeMillis() - start));

		Map<TO<?>, Collection<Tag>> tags = env.getAllTags(TYPE_NAME);
		assertEquals(OBJECT_NUMBER * 2 + 5, tags.size());
		for (Map.Entry<TO<?>, Collection<Tag>> entry : tags.entrySet()) {
			assertEquals(4, entry.getValue().size());
		}
	}

	private Thread startCreateThread(final int objNum) {
		Thread t = new Thread(new Runnable() {
			@Override
			public void run() {
				EnvObjectBundle objectsToCreate = new EnvObjectBundle();
				for (int i = 0; i < objNum; i++) {
					ValueTO vName = new ValueTO("name", "name" + i);
					ValueTO vTag = new ValueTO("tag", "tag" + i);
					ValueTO vImage = new ValueTO("image", "image" + i);

					ValueTO vValue = new ValueTO("value", i + 0.1);
					ValueTO vX = new ValueTO("x", i + 0.1);
					ValueTO vY = new ValueTO("y", i + 0.1);
					ValueTO vZ = new ValueTO("z", i + 0.1);

					ValueTO vInt = new ValueTO("int", i);

					Tag imageScale = new Tag("image", "scale", i + 0.1);
					Tag imageRes = new Tag("image", "resolution", 1L);

					Tag xScale = new Tag("x", "scale", i);
					Tag xSystem = new Tag("x", "system", "res_" + i);

					EnvObject envObj = new EnvObject(TYPE_NAME, Arrays.asList(vName, vValue, vX, vY, vZ, vTag, vImage,
					        vInt), Arrays.asList(imageScale, imageRes, xScale, xSystem));
					objectsToCreate.addObject("label:" + i, envObj);
				}

				try {
					env.createObjects(objectsToCreate);
				} catch (EnvException e) {
					e.printStackTrace();
				}
			}
		});
		t.start();
		return t;
	}

	private static ObjectType createType(String TYPE_NAME) throws EnvException {
		ObjectType ot = env.getObjectType(TYPE_NAME);
		if (ot == null) {
			ot = env.createObjectType(TYPE_NAME);
			PropertyDescr name = new PropertyDescr("name", String.class);
			PropertyDescr value = new PropertyDescr("value", Double.class.getName(), true);
			PropertyDescr x = new PropertyDescr("x", Double.class.getName());
			{
				x.getTagDescriptors().add(new TagDescriptor("scale", Integer.class.getName()));
				x.getTagDescriptors().add(new TagDescriptor("system", String.class.getName()));
			}

			PropertyDescr y = new PropertyDescr("y", Double.class.getName());
			PropertyDescr z = new PropertyDescr("z", Double.class.getName());
			PropertyDescr tag = new PropertyDescr("tag", String.class.getName());
			PropertyDescr image = new PropertyDescr("image", String.class.getName());
			{
				image.getTagDescriptors().add(new TagDescriptor("scale", Double.class.getName()));
				image.getTagDescriptors().add(new TagDescriptor("resolution", Long.class.getName()));
			}
			PropertyDescr integer = new PropertyDescr("int", Integer.class.getName());

			PropertyDescr link = new PropertyDescr("link", TO.class);

			ot.getPropertyDescriptors().add(name);
			ot.getPropertyDescriptors().add(value);
			ot.getPropertyDescriptors().add(link);
			ot.getPropertyDescriptors().add(x);
			ot.getPropertyDescriptors().add(y);
			ot.getPropertyDescriptors().add(z);
			ot.getPropertyDescriptors().add(tag);
			ot.getPropertyDescriptors().add(image);
			ot.getPropertyDescriptors().add(integer);

			env.updateObjectType(ot);
		}

		return ot;
	}
}
