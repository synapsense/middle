package com.synapsense.func.reporting.scheduling;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.synapsense.exception.ObjectExistsException;
import com.synapsense.impl.reporting.TestUtils;
import com.synapsense.impl.reporting.dao.DbESDAOFactory;
import com.synapsense.impl.reporting.dao.ReportTemplateDAO;
import com.synapsense.impl.reporting.dao.ReportingDAOFactory;
import com.synapsense.service.reporting.dto.ReportTaskInfo;
import com.synapsense.service.reporting.dto.ReportTemplateInfo;
import com.synapsense.service.reporting.exception.DAOReportingException;
import com.synapsense.service.reporting.management.ReportingManagementService;

public class SchedulingTasksTest {

	private static ReportingDAOFactory daoFactory;
	private static ReportTemplateDAO rtDao;
	
	@BeforeClass
	public static void setUpBeforeClass() throws DAOReportingException, ObjectExistsException {
		daoFactory = new DbESDAOFactory(TestUtils.FILE_STORAGE, TestUtils.getEnvironment(), TestUtils.getFileWriter());
		rtDao = daoFactory.newReportTemplateDAO();
		ReportTemplateInfo rtInfo = new ReportTemplateInfo("TestReportTemplate");
		rtDao.createReportTemplate(rtInfo, "TestDesign");
	}

	@AfterClass
	public static void tearDownAfterClass() throws DAOReportingException {
		daoFactory.shutdown();
	}
	
	@Test
	public void testAddSchedulingTask() throws Exception {
		
		ReportingManagementService manager = FacadeManager.getReportManager();
		ReportTaskInfo task = ReportTaskInfo.newInstance("TestReportTemplate", "TestTask");
        task.setCron("0/1 * * * * ?");
        
		manager.scheduleReportTask(task);
		rtDao.removeReportTemplate("TestReportTemplate");

		FacadeManager.getReportScheduler().removeScheduledTask("TestTask", "TestReportTemplate");
	}
}
