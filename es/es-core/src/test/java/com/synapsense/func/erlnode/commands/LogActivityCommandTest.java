package com.synapsense.func.erlnode.commands;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;

import org.junit.BeforeClass;
import org.junit.Test;

import com.ericsson.otp.erlang.OtpErlangObject;
import com.ericsson.otp.erlang.OtpErlangString;
import com.ericsson.otp.erlang.OtpErlangTuple;
import com.synapsense.impl.erlnode.ExceptionTypes;
import com.synapsense.service.Environment;
import com.synapsense.service.impl.activitylog.ActivityLogService;
import com.synapsense.util.JNDINames;
import com.synapsense.util.ServerLoginModule;

public class LogActivityCommandTest {
	static UnitTestHelper utHelper = null;
	static Map<String, Object> envMap;
	static Environment env;
	static ActivityLogService activityLogSvc;
	/*
	 * @BeforeClass public static void setUpBeforeClass() throws Exception {
	 * Properties environment = new Properties();
	 * environment.put(Context.INITIAL_CONTEXT_FACTORY,
	 * "org.jnp.interfaces.NamingContextFactory");
	 * environment.put(Context.URL_PKG_PREFIXES,
	 * "org.jboss.naming:org.jnp.interfaces");
	 * environment.put(Context.PROVIDER_URL, JNDINames.PROVIDER_URL);
	 * InitialContext ctx = new InitialContext(environment); env =
	 * ServerLoginModule.getProxy(ctx, JNDINames.ENVIRONMENT,
	 * Environment.class); activityLogSvc = ServerLoginModule.getProxy(ctx,
	 * JNDINames.ACTIVITY_LOG_SERVICE_JNDI_NAME, ActivityLogService.class);
	 * 
	 * ServerLoginModule.login("admin", "admin");
	 * 
	 * envMap = new HashMap<String, Object>(); envMap.put("env", env);
	 * envMap.put("logsvc", activityLogSvc);
	 * 
	 * }
	 * 
	 * @Test public void SuccessLogActivityTest() throws Exception {
	 * 
	 * LogActivityCommand command = new LogActivityCommand(envMap);
	 * OtpErlangTuple obj = (OtpErlangTuple) command .run(new OtpErlangTuple(
	 * new OtpErlangObject[] { new OtpErlangString("LogActivityCommand"), new
	 * OtpErlangString("someUser"), new OtpErlangString(
	 * ActivityLogService.ActivityModule.UserManagement .name()), new
	 * OtpErlangString( ActivityLogService.ActivityAction.AddUser .name()), new
	 * OtpErlangString("someDescr"), })); assertEquals("ok",
	 * obj.elementAt(0).toString());
	 * 
	 * }
	 * 
	 * @Test public void FailLogActivityTest() throws Exception {
	 * 
	 * LogActivityCommand command = new LogActivityCommand(envMap);
	 * OtpErlangTuple obj = (OtpErlangTuple) command.run(new OtpErlangTuple( new
	 * OtpErlangObject[] { new OtpErlangString("LogActivityCommand"), new
	 * OtpErlangString("someUser"), new OtpErlangString("nonExistingModule"),
	 * new OtpErlangString( ActivityLogService.ActivityAction.AddUser .name()),
	 * new OtpErlangString("someDescr"), })); assertEquals("exception",
	 * obj.elementAt(0).toString());
	 * assertEquals(ExceptionTypes.NO_SUCH_MODULE.getExceptionType(),
	 * ((OtpErlangString) obj.elementAt(1)).stringValue()); obj =
	 * (OtpErlangTuple) command .run(new OtpErlangTuple( new OtpErlangObject[] {
	 * new OtpErlangString("LogActivityCommand"), new
	 * OtpErlangString("someUser"), new OtpErlangString(
	 * ActivityLogService.ActivityModule.UserManagement .name()), new
	 * OtpErlangString("NoneExistingActivity"), new
	 * OtpErlangString("someDescr"), })); assertEquals("exception",
	 * obj.elementAt(0).toString());
	 * assertEquals(ExceptionTypes.NO_SUCH_ACTIVITY.getExceptionType(),
	 * ((OtpErlangString) obj.elementAt(1)).stringValue());
	 * 
	 * }
	 * 
	 * @Test public void InvalidParametersTest() throws Exception {
	 * LogActivityCommand command = new LogActivityCommand(envMap);
	 * OtpErlangTuple obj = (OtpErlangTuple) command.run(new OtpErlangTuple( new
	 * OtpErlangObject[] { new OtpErlangString( "LogActivityCommand"), }));
	 * assertEquals("exception", obj.elementAt(0).toString());
	 * assertEquals(ExceptionTypes.ILLEGAL_ARGUMENTS.getExceptionType(),
	 * ((OtpErlangString) obj.elementAt(1)).stringValue()); }
	 */
}
