package com.synapsense.func;

import java.util.HashMap;

import javax.security.auth.login.AppConfigurationEntry;
import javax.security.auth.login.Configuration;

public class JBossLoginConfiguration extends Configuration {

	public static final String CONFIG_NAME = "JBoss Client Login Module Only";
	private static final AppConfigurationEntry[] confEntry = new AppConfigurationEntry[] { new AppConfigurationEntry(
	        "org.jboss.security.ClientLoginModule", AppConfigurationEntry.LoginModuleControlFlag.REQUIRED,
	        new HashMap<String, Object>()) };

	public static JBossLoginConfiguration config = new JBossLoginConfiguration();

	@Override
	public AppConfigurationEntry[] getAppConfigurationEntry(String name) {
		return CONFIG_NAME.equals(name) ? confEntry : null;
	}
}
