package com.synapsense.func.erlnode.commands;

import static org.junit.Assert.assertEquals;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.ericsson.otp.erlang.OtpErlangLong;
import com.ericsson.otp.erlang.OtpErlangObject;
import com.ericsson.otp.erlang.OtpErlangString;
import com.ericsson.otp.erlang.OtpErlangTuple;
import com.synapsense.dto.Alert;
import com.synapsense.dto.RuleType;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.impl.erlnode.ExceptionTypes;
import com.synapsense.impl.erlnode.commands.RaiseAlertCommand;
import com.synapsense.impl.erlnode.utils.ToConverter;
import com.synapsense.service.AlertService;
import com.synapsense.service.Environment;
import com.synapsense.service.RuleTypeService;
import com.synapsense.util.JNDINames;
import com.synapsense.util.ServerLoginModule;

public class RaiseAlertCommandTest {
	static UnitTestHelper utHelper = null;
	static String testType1 = "testType1";
	static Map<String, Object> envMap;
	static Environment env;
	static RuleTypeService<Integer> ruleTypeService;
	static AlertService alertsvc;
	static TO<?> ruleTO;
	/*
	 * @BeforeClass
	 * 
	 * @SuppressWarnings("unchecked") public static void setUpBeforeClass()
	 * throws Exception { Properties environment = new Properties();
	 * environment.put(Context.INITIAL_CONTEXT_FACTORY,
	 * "org.jnp.interfaces.NamingContextFactory");
	 * environment.put(Context.URL_PKG_PREFIXES,
	 * "org.jboss.naming:org.jnp.interfaces");
	 * environment.put(Context.PROVIDER_URL, JNDINames.PROVIDER_URL);
	 * InitialContext ctx = new InitialContext(environment); env =
	 * ServerLoginModule.getProxy(ctx, JNDINames.ENVIRONMENT,
	 * Environment.class); alertsvc = ServerLoginModule.getProxy(ctx,
	 * JNDINames.ALERTING_SERVICE_JNDI_NAME, AlertService.class);
	 * ruleTypeService = ServerLoginModule.getProxy(ctx, JNDINames.RULE_TYPE,
	 * RuleTypeService.class); ServerLoginModule.login("admin", "admin");
	 * 
	 * envMap = new HashMap<String, Object>(); envMap.put("env", env);
	 * envMap.put("alertsvc", alertsvc);
	 * 
	 * }
	 * 
	 * @Before public void setUp() throws Exception {
	 * env.createObjectType(testType1); TO<?> rule =
	 * ruleTypeService.getRuleTypeByName("Rule1"); if (rule != null) {
	 * ruleTypeService.delete((Integer) rule.getID()); } RuleType conf = new
	 * RuleType("Rule1",
	 * "package com.synapsense.unittestDrulesEngineManagment; " +
	 * "rule \"Rule 01\"  " + "when   eval (1==1)  " +
	 * " then  System.out.println(\"Rule 01 Works\");" + " end", "type", "dsl");
	 * ruleTO = ruleTypeService.create(conf); }
	 * 
	 * @After public void tearDown() throws Exception {
	 * env.deleteObjectType(testType1); ruleTypeService.delete((Integer)
	 * ruleTO.getID()); }
	 * 
	 * @Test public void SuccessRiseAlertTest() throws Exception {
	 * 
	 * TO<?> hostTO = env.createObject(testType1);
	 * 
	 * RaiseAlertCommand command = new RaiseAlertCommand(envMap); OtpErlangTuple
	 * obj = (OtpErlangTuple) command.run(new OtpErlangTuple( new
	 * OtpErlangObject[] { new OtpErlangString("RaiseAlertCommand"), new
	 * OtpErlangString("name"), new OtpErlangString("someAlertType"), new
	 * OtpErlangLong(System.currentTimeMillis()), new
	 * OtpErlangString("someMessage"), ToConverter.serializeTo(ruleTO),
	 * ToConverter.serializeTo(hostTO) })); assertEquals("ok",
	 * obj.elementAt(0).toString()); Collection<Alert> alerts =
	 * alertsvc.getActiveAlerts(hostTO, false); assertEquals(1, alerts.size());
	 * Alert a = alerts.iterator().next(); alertsvc.dismissAlert(a, "comment");
	 * alertsvc.deleteAlerts(hostTO, false); alerts =
	 * alertsvc.getActiveAlerts(hostTO, false); assertEquals(0, alerts.size());
	 * env.deleteObject(hostTO);
	 * 
	 * }
	 * 
	 * @Test public void FailRiseAlertTest() throws Exception { TO<?> hostTO =
	 * TOFactory.getInstance().loadTO("sdfs:11"); RaiseAlertCommand command =
	 * new RaiseAlertCommand(envMap); OtpErlangTuple obj = (OtpErlangTuple)
	 * command.run(new OtpErlangTuple( new OtpErlangObject[] { new
	 * OtpErlangString("RaiseAlertCommand"), new OtpErlangString("name"), new
	 * OtpErlangString("someAlertType"), new
	 * OtpErlangLong(System.currentTimeMillis()), new
	 * OtpErlangString("someMessage"), ToConverter.serializeTo(ruleTO),
	 * ToConverter.serializeTo(hostTO) })); assertEquals("exception",
	 * obj.elementAt(0).toString());
	 * assertEquals(ExceptionTypes.ALERT_SVC_SYSTEM.getExceptionType(),
	 * ((OtpErlangString) obj.elementAt(1)).stringValue());
	 * 
	 * 
	 * Collection<Alert> alerts = alertsvc.getActiveAlerts(hostTO, false);
	 * assertEquals(0, alerts.size()); }
	 * 
	 * @Test public void InvalidParametersTest() throws Exception {
	 * RaiseAlertCommand command = new RaiseAlertCommand(envMap); OtpErlangTuple
	 * obj = (OtpErlangTuple) command .run(new OtpErlangTuple( new
	 * OtpErlangObject[] { new OtpErlangString( "RaiseAlertCommand"), }));
	 * assertEquals("exception", obj.elementAt(0).toString());
	 * assertEquals(ExceptionTypes.ILLEGAL_ARGUMENTS.getExceptionType(),
	 * ((OtpErlangString) obj.elementAt(1)).stringValue()); }
	 */
}
