package com.synapsense.func.erlnode.commands;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.ericsson.otp.erlang.OtpErlangAtom;
import com.ericsson.otp.erlang.OtpErlangDecodeException;
import com.ericsson.otp.erlang.OtpErlangFloat;
import com.ericsson.otp.erlang.OtpErlangInt;
import com.ericsson.otp.erlang.OtpErlangLong;
import com.ericsson.otp.erlang.OtpErlangObject;
import com.ericsson.otp.erlang.OtpErlangRangeException;
import com.ericsson.otp.erlang.OtpErlangString;
import com.ericsson.otp.erlang.OtpErlangTuple;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.dto.ValueTO;
import com.synapsense.impl.erlnode.utils.ToConverter;
import com.synapsense.impl.erlnode.utils.ValueTOConverter;

/**
 * @author stikhon
 * 
 */
public class ValueTOConverterTest {

	static UnitTestHelper utHelper = null;
	/*
	 * @Test public void serializeValueTOTest() { OtpErlangTuple tuple;
	 * 
	 * try { tuple = ValueTOConverter.serializeValueTO(null); assertTrue(false);
	 * } catch (IllegalArgumentException e) { assertTrue(true); } tuple =
	 * ValueTOConverter.serializeValueTO(new ValueTO("nodePhysicalId", new
	 * Long(143390159260352764L)));
	 * assertEquals("{value_to,\"nodePhysicalId\",143390159260352764,0}",
	 * tuple.toString()); tuple = ValueTOConverter.serializeValueTO(new
	 * ValueTO("nodePhysicalId", new Integer(1433901)));
	 * assertEquals("{value_to,\"nodePhysicalId\",1433901,0}", tuple
	 * .toString()); tuple = ValueTOConverter.serializeValueTO(new
	 * ValueTO("name", "SetPropertyValueTest_nodeName", 22L)); assertEquals(
	 * "{value_to,\"name\",\"SetPropertyValueTest_nodeName\",22}",
	 * tuple.toString()); tuple = ValueTOConverter.serializeValueTO(new
	 * ValueTO("double", new Double(12.2), 11L));
	 * assertEquals("{value_to,\"double\",12.2,11}", tuple.toString()); TO<?> to
	 * = TOFactory.getInstance().loadTO("SENSOR:11"); tuple =
	 * ValueTOConverter.serializeValueTO(new ValueTO("platform", to));
	 * assertEquals("{value_to,\"platform\",{to,\"" +
	 * TOFactory.getInstance().saveTO(to) + "\"},0}", tuple .toString());
	 * 
	 * }
	 * 
	 * @Test public void parseValueTOTest() throws OtpErlangDecodeException,
	 * OtpErlangRangeException {
	 * 
	 * ValueTO valueTo; OtpErlangTuple tuple; try { valueTo =
	 * ValueTOConverter.parseValueTO(null); assertTrue(false); } catch
	 * (IllegalArgumentException e) { assertTrue(true); } tuple = new
	 * OtpErlangTuple(new OtpErlangObject[] { new OtpErlangAtom("value_to"), new
	 * OtpErlangString("stringProperty"), }); try { valueTo =
	 * ValueTOConverter.parseValueTO(tuple); assertTrue(false); } catch
	 * (IllegalArgumentException e) { assertTrue(true); } tuple = new
	 * OtpErlangTuple(new OtpErlangObject[] { new OtpErlangAtom("value_to"), new
	 * OtpErlangAtom("value_to_1"), new OtpErlangString("stringProperty"), });
	 * try { valueTo = ValueTOConverter.parseValueTO(tuple); assertTrue(false);
	 * } catch (IllegalArgumentException e) { assertTrue(true); } tuple = new
	 * OtpErlangTuple(new OtpErlangObject[] { new OtpErlangAtom("value_to_1"),
	 * new OtpErlangString("stringProperty"), new OtpErlangString("value"), new
	 * OtpErlangInt(0) }); try { valueTo = ValueTOConverter.parseValueTO(tuple);
	 * assertTrue(false); } catch (IllegalArgumentException e) {
	 * assertTrue(true); }
	 * 
	 * tuple = new OtpErlangTuple(new OtpErlangObject[] { new
	 * OtpErlangAtom("value_to"), new OtpErlangString("stringProperty"), new
	 * OtpErlangString("value"), new OtpErlangLong(2L) }); valueTo =
	 * ValueTOConverter.parseValueTO(tuple); assertNotNull(valueTo);
	 * assertEquals(valueTo.getPropertyName(), "stringProperty");
	 * assertEquals(valueTo.getTimeStamp(), 2L);
	 * assertEquals(valueTo.getValue(), "value");
	 * 
	 * tuple = new OtpErlangTuple(new OtpErlangObject[] { new
	 * OtpErlangAtom("value_to"), new OtpErlangString("stringProperty"), new
	 * OtpErlangString("value") }); valueTo =
	 * ValueTOConverter.parseValueTO(tuple); assertNotNull(valueTo);
	 * assertEquals(valueTo.getPropertyName(), "stringProperty");
	 * assertEquals(valueTo.getTimeStamp(), 0L);
	 * assertEquals(valueTo.getValue(), "value");
	 * 
	 * tuple = new OtpErlangTuple(new OtpErlangObject[] { new
	 * OtpErlangAtom("value_to"), new OtpErlangString("property"), new
	 * OtpErlangLong(1212) }); valueTo = ValueTOConverter.parseValueTO(tuple);
	 * assertNotNull(valueTo); assertEquals(valueTo.getPropertyName(),
	 * "property"); assertEquals(valueTo.getTimeStamp(), 0L);
	 * assertEquals(valueTo.getValue(), 1212L);
	 * 
	 * tuple = new OtpErlangTuple(new OtpErlangObject[] { new
	 * OtpErlangAtom("value_to"), new OtpErlangString("property"), new
	 * OtpErlangFloat(1212.2F) }); valueTo =
	 * ValueTOConverter.parseValueTO(tuple); assertNotNull(valueTo);
	 * assertEquals(valueTo.getPropertyName(), "property");
	 * assertEquals(valueTo.getTimeStamp(), 0L); assertEquals(1212.2, ((Double)
	 * valueTo.getValue()).doubleValue(), 1);
	 * 
	 * tuple = new OtpErlangTuple(new OtpErlangObject[] { new
	 * OtpErlangAtom("value_to"), new OtpErlangString("property"),
	 * ToConverter.serializeTo(TOFactory.getInstance().loadTO( "SENSOR:11")) });
	 * valueTo = ValueTOConverter.parseValueTO(tuple); assertNotNull(valueTo);
	 * assertEquals(valueTo.getPropertyName(), "property");
	 * assertEquals(valueTo.getTimeStamp(), 0L);
	 * assertEquals(valueTo.getValue(), TOFactory.getInstance().loadTO(
	 * "SENSOR:11"));
	 * 
	 * }
	 */
}
