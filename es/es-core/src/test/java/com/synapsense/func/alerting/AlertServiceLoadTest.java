package com.synapsense.func.alerting;

import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.synapsense.dto.Alert;
import com.synapsense.dto.AlertPriority;
import com.synapsense.dto.AlertType;
import com.synapsense.dto.MessageTemplate;
import com.synapsense.dto.MessageTemplateRef;
import com.synapsense.dto.MessageType;
import com.synapsense.dto.PriorityTier;
import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.exception.PropertyNotFoundException;
import com.synapsense.exception.UnableToConvertPropertyException;
import com.synapsense.func.ConsoleApp;
import com.synapsense.service.AlertService;
import com.synapsense.service.Environment;
import com.synapsense.util.CollectionUtils;

public class AlertServiceLoadTest {
	private static AlertService ALERTING_SERVICE;

	private static Environment ENVIRONMENT;

	private static String TEST_TYPE = "TEST_TYPE";

	private static String TEST_ALERT_TYPE = "TEST_ALERT_TYPE";

	private static List<TO<?>> objects = new LinkedList<TO<?>>();

	private static TO<?> ROOT;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		ConsoleApp.init();

		ENVIRONMENT = ConsoleApp.env;

		ALERTING_SERVICE = ConsoleApp.ALERTING_SERVICE;

		ENVIRONMENT.createObjectType(TEST_TYPE);

		ROOT = ENVIRONMENT.createObject(TEST_TYPE);

		Collection<TO<?>> templatesRefs = ENVIRONMENT.getObjects("ALERT_MESSAGE_TEMPLATE", new ValueTO[] { new ValueTO(
		        "name", "Console System Notification") });
		TO<?> ref = templatesRefs.iterator().next();

		AlertPriority ap = ALERTING_SERVICE.getAlertPriority("CRITICAL");
		if (ap == null) {
			ap = new AlertPriority("CRITICAL", 600000L, Arrays.asList(new PriorityTier("TIER1", 1000, 2, true, true,
			        true)));
		}

		Set<MessageTemplate> templates = CollectionUtils.newSet();
		if (ref != null)
			templates.add(new MessageTemplateRef(MessageType.CONSOLE, ref));
		final AlertType at = new AlertType(TEST_ALERT_TYPE, TEST_ALERT_TYPE, "", "CRITICAL", "header", "body",
		        templates, true, false, false, true);
		ALERTING_SERVICE.createAlertType(at);

		for (int i = 0; i < 100; i++) {
			TO child = ENVIRONMENT.createObject(TEST_TYPE);
			objects.add(child);
			ENVIRONMENT.setRelation(ROOT, child);
		}

		Alert alert = new Alert("alert_error", TEST_ALERT_TYPE, new Date(), "$" + ref.toString()
		        + ".name$, $conv(1.5,209)$", null);

		long start = System.currentTimeMillis();
		for (int i = 0; i < 1; i++) {
			Iterator<TO<?>> iterator = objects.iterator();
			while (iterator.hasNext()) {
				TO<?> id = iterator.next();
				alert.setHostTO(id);
				String newname = id.getID().toString();
				if (newname.length() > 126) {
					assertTrue(newname, false);
				}
				alert.setName(newname);
				ALERTING_SERVICE.raiseAlert(alert);
			}
			// ALERTING_SERVICE.dismissAlerts(ROOT, true, "dismissing_" + i);
		}

		System.err.println(objects.size() + " alerts initital generation time : "
		        + (System.currentTimeMillis() - start));
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		ALERTING_SERVICE.deleteAllAlerts();
		ALERTING_SERVICE.deleteAlertType(TEST_ALERT_TYPE);
		ENVIRONMENT.deleteObjectType(TEST_TYPE);
	}

	@Test(timeout = 200)
	public void testGetAlertsOnParentObject() {
		long start = System.currentTimeMillis();
		ALERTING_SERVICE.getActiveAlerts(ROOT, true);
		System.err.println("testGetAlertsOnParentObject:" + (System.currentTimeMillis() - start));
	}

	@Test(timeout = 200)
	public void testGetAlertsHistoryOnParent() {
		long start = System.currentTimeMillis();
		ALERTING_SERVICE.getAlertsHistory(ROOT, new Date(0), new Date(start), true);
		System.err.println("testGetAlertsHistoryOnParent:" + (System.currentTimeMillis() - start));
	}

	@Test(timeout = 100)
	public void testHasAlerts() {
		long start = System.currentTimeMillis();
		ALERTING_SERVICE.hasAlerts(ROOT, true);
		System.err.println("testHasAlerts:" + (System.currentTimeMillis() - start));
	}

	@Test(timeout = 500)
	public void testGetAlertsOnObjects() {
		long start = System.currentTimeMillis();
		ALERTING_SERVICE.getActiveAlerts(objects, true);
		System.err.println("testGetAlertsOnObjects:" + (System.currentTimeMillis() - start));
	}

	@Test(timeout = 100)
	public void testGetAlertsViaEnvAndService() throws ObjectNotFoundException, PropertyNotFoundException,
	        UnableToConvertPropertyException {
		TO<?> obj = objects.iterator().next();
		long start = System.currentTimeMillis();
		ALERTING_SERVICE.getActiveAlerts(obj, true);
		System.err.println("testGetAlertsViaEnvAndService AS time : " + (System.currentTimeMillis() - start));

	}

	@Test(timeout = 50)
	public void testGetNumAlerts() throws ObjectNotFoundException, PropertyNotFoundException,
	        UnableToConvertPropertyException {
		TO<?> obj = objects.iterator().next();
		long start = System.currentTimeMillis();
		ENVIRONMENT.getPropertyValue(obj, "numAlerts", Integer.class);
		System.err.println("testGetNumAlerts time : " + (System.currentTimeMillis() - start));

	}

}
