package com.synapsense.func.activitylog;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.Method;
import java.util.*;

import com.synapsense.util.CollectionUtils;
import org.dbunit.JdbcDatabaseTester;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;

import com.synapsense.DbConstants;
import com.synapsense.dal.generic.entities.ActivityRecordDO;
import com.synapsense.dto.Alert;
import com.synapsense.dto.AlertType;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.exception.EnvException;
import com.synapsense.exception.IllegalInputParameterException;
import com.synapsense.func.ConsoleApp;
import com.synapsense.service.AlertService;
import com.synapsense.service.Environment;
import com.synapsense.service.UserManagementService;
import com.synapsense.service.impl.activitylog.ActivityLogService;
import com.synapsense.service.impl.activitylog.ActivityRecord;
import com.synapsense.service.impl.activitylog.ActivityRecordFilter;

public class ActivityLogTest {

	private static ActivityLogService activityLogService;
	private static AlertService alertService;
	private static Environment envService;
	private static UserManagementService ums;
	private static JdbcDatabaseTester databaseTester;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		ConsoleApp.init();
		activityLogService = ConsoleApp.ACTIVITY_LOG_SERVICE;
		ums = ConsoleApp.USER_SERVICE;
		alertService = ConsoleApp.ALERTING_SERVICE;
		envService = ConsoleApp.env;
		databaseTester = new JdbcDatabaseTester(DbConstants.DB_DRIVER, DbConstants.DB_CONNECTION, DbConstants.DB_USER,
		        DbConstants.DB_PASSWORD);
	}

	@After
	public void testAfter() throws Exception {
		IDataSet databaseDataSet = getConnection().createDataSet(new String[] { DbConstants.TBL_ACTIVITY_LOG });
		DatabaseOperation.TRUNCATE_TABLE.execute(getConnection(), databaseDataSet);

		Thread.sleep(2000);
	}

	@Test(expected = IllegalInputParameterException.class)
	public void testLogNullUserNameFailure() {
		activityLogService.addRecord(new ActivityRecord(null, "module", "action", "descr"));
	}

	@Test(expected = IllegalInputParameterException.class)
	public void testLogEmptyUserNameFailure() {
		activityLogService.addRecord(new ActivityRecord("", "module", "action", "descr"));
	}

	@Test(expected = IllegalInputParameterException.class)
	public void testLogNullModuleNameFailure() {
		activityLogService.addRecord(new ActivityRecord("admin", null, "action", "descr"));
	}

	@Test(expected = IllegalInputParameterException.class)
	public void testLogEmptyModuleNameFailure() {
		activityLogService.addRecord(new ActivityRecord("admin", "", "action", "descr"));
	}

	@Test(expected = IllegalInputParameterException.class)
	public void testLogNullActionNameFailure() {
		activityLogService.addRecord(new ActivityRecord("admin", "module", null, "descr"));
	}

	@Test(expected = IllegalInputParameterException.class)
	public void testLogEmptyActionNameFailure() {
		activityLogService.addRecord(new ActivityRecord("admin", "module", "", "descr"));
	}

	@Test(expected = IllegalInputParameterException.class)
	public void testLogNullDesriptionFailure() {
		activityLogService.addRecord(new ActivityRecord("admin", "module", "action", null));
	}

	@Test(expected = IllegalInputParameterException.class)
	public void testLogEmptyDesriptionFailure() {
		activityLogService.addRecord(new ActivityRecord("admin", "module", "action", ""));
	}

	@Test
	public void testLogValidInputSuccess() {
		activityLogService.addRecord(new ActivityRecord("admin", "module", "action", "descr"));
	}

	@Test
	public void testGetAllRecords() throws Exception {
		ActivityRecordFilter getAllFilter = new ActivityRecordFilter();
		Collection<ActivityRecord> records = activityLogService.getRecords(getAllFilter);
		assertNotNull(records);
		assertTrue(records.isEmpty());

		for (int i = 1; i < 10; i++) {
			activityLogService.addRecord(new ActivityRecord("admin", "module", "action", "descr"));
			records = activityLogService.getRecords(getAllFilter);
			assertNotNull(records);
			assertEquals(i, records.size());
		}
	}

	@Test
	public void testGetRecordsById() {
		activityLogService.addRecord(new ActivityRecord("admin", "module1", "UserLogin", "descr"));
		activityLogService.addRecord(new ActivityRecord("admin", "module2", "UserLogin", "descr"));
		activityLogService.addRecord(new ActivityRecord("admin", "module1", "UserLogin", "descr"));
		activityLogService.addRecord(new ActivityRecord("admin", "module2", "UserLogin", "descr"));
		Integer ID = 1;
		ActivityRecord record = activityLogService.getRecord(ID);
		assertEquals(ID, record.getId());

	}

	@Test
	public void testGetRecordsByIds() {

		activityLogService.addRecord(new ActivityRecord("admin", "module1", "UserLogin", "descr"));
		activityLogService.addRecord(new ActivityRecord("admin", "module2", "UserLogin", "descr"));
		activityLogService.addRecord(new ActivityRecord("admin", "module1", "UserLogin", "descr"));
		activityLogService.addRecord(new ActivityRecord("admin", "module2", "UserLogin", "descr"));

		Integer ID = 1;
		ActivityRecordFilter filterById = new ActivityRecordFilter();
		filterById.filterById(ID);
		Collection<ActivityRecord> records = activityLogService.getRecords(filterById);
		assertNotNull(records);
		assertFalse(records.isEmpty());
		assertEquals(1, records.size());

		ActivityRecord record = null;
		Iterator<ActivityRecord> recItr = records.iterator();
		if (recItr.hasNext()) {
			record = recItr.next();
		}
		assertNotNull(record);

	}

	@Test
	public void testGetRecordsByActivity() throws Exception {
		activityLogService.addRecord(new ActivityRecord("admin", "module1", "UserLogin", "descr"));
		activityLogService.addRecord(new ActivityRecord("admin", "module2", "UserLogin", "descr"));

		String action = "UserLogin";
		ActivityRecordFilter filterByAction = new ActivityRecordFilter();
		filterByAction.filterByAction(action);

		assertTrue(allPropertiesEqualTo(activityLogService.getRecords(filterByAction), "getAction", action));
		Collection<ActivityRecord> records = activityLogService.getRecords(filterByAction);
		assertEquals(2, records.size());

	}

	@Test
	public void testGetRecordsByModule() throws Exception {
		activityLogService.addRecord(new ActivityRecord("admin", "UserManagement", "UserLogin1", "descr"));
		activityLogService.addRecord(new ActivityRecord("admin", "UserManagement", "UserLogin2", "descr"));
		String module = "UserManagement";
		ActivityRecordFilter filterByModule = new ActivityRecordFilter();
		filterByModule.filterByModule(module);
		assertTrue(allPropertiesEqualTo(activityLogService.getRecords(filterByModule), "getModule", module));
	}

	@Test
	public void testGetRecordsByUser() throws Exception {
		activityLogService.addRecord(new ActivityRecord("admin", "UserManagement", "UserLogin1", "descr"));
		activityLogService.addRecord(new ActivityRecord("admin", "UserManagement", "UserLogin2", "descr"));
		ActivityRecordFilter filterByUser = new ActivityRecordFilter();
		filterByUser.filterByUser("admin");

		assertTrue(allPropertiesEqualTo(activityLogService.getRecords(filterByUser), "getUserName", "admin"));
	}

	@Test
	public void testGetRecordsByObject() throws Exception {
		TO<?> objId = TOFactory.EMPTY_TO;
		activityLogService.addRecord(new ActivityRecord(null, "admin", "UserManagement", "UserLogin1", "descr",
		        new Date(), objId));
		activityLogService.addRecord(new ActivityRecord(null, "admin", "UserManagement", "UserLogin2", "descr",
		        new Date(), objId));
		ActivityRecordFilter filterByObject = new ActivityRecordFilter();
		filterByObject.filterByObject(objId);

		Collection<ActivityRecord> records = activityLogService.getRecords(filterByObject);

		assertEquals(2, records.size());
	}

	@Test
	public void testUserActivitySuccess() throws Exception {
		Collection<ActivityRecord> logsByAction = null;
		TO<?> u = null;

		ActivityRecordFilter filterByModule = new ActivityRecordFilter();
		filterByModule.filterByModule("UserManagement");
		Collection<ActivityRecord> logsByModule = activityLogService.getRecords(filterByModule);
		int logCount = logsByModule.size();

		try {
			u = ums.createUser("test", "temp", null);
			logCount++;
			ActivityRecordFilter filterByAddAction = new ActivityRecordFilter();
			filterByAddAction.filterByAction("AddUser");
			logsByAction = activityLogService.getRecords(filterByAddAction);
			assertEquals(1, logsByAction.size());
			assertTrue(allPropertiesEqualTo(logsByAction, "getAction", "AddUser"));

			ums.setPassword(u, "test");
			logCount++;
			ActivityRecordFilter filterByChAction = new ActivityRecordFilter();
			filterByChAction.filterByAction("ChangeUserPassword");
			logsByAction = activityLogService.getRecords(filterByChAction);
			assertEquals(1, logsByAction.size());
			assertTrue(allPropertiesEqualTo(logsByAction, "getAction", "ChangeUserPassword"));

			Map<String, Object> props = ums.getUserInformation(u);
			props.put(ums.USER_DESCRIPTION, "updated desc");
			Collection<String> groups = CollectionUtils.newSet();
			groups.add("Administrators");
			props.put(ums.USER_GROUPS, groups);
			ums.updateUser(u, props);
			logCount++;
			ActivityRecordFilter filterByEditAction = new ActivityRecordFilter();
			filterByEditAction.filterByAction("EditUser");
			logsByAction = activityLogService.getRecords(filterByEditAction);
			assertEquals(1, logsByAction.size());
			assertTrue(allPropertiesEqualTo(logsByAction, "getAction", "EditUser"));

			ums.setProperty(u, ums.USER_DESCRIPTION, "another desc");
			logCount++;
			logsByAction = activityLogService.getRecords(filterByEditAction);
			assertEquals(2, logsByAction.size());
			assertTrue(allPropertiesEqualTo(logsByAction, "getAction", "EditUser"));

		} finally {
			ums.deleteUser(u);
			logCount++;
			ActivityRecordFilter filterByDeleteAction = new ActivityRecordFilter();
			filterByDeleteAction.filterByAction("DeleteUser");
			logsByAction = activityLogService.getRecords(filterByDeleteAction);
			assertEquals(1, logsByAction.size());
			assertTrue(allPropertiesEqualTo(logsByAction, "getAction", "DeleteUser"));
		}

		logsByModule = activityLogService.getRecords(filterByModule);
		assertEquals(logCount, logsByModule.size());
		assertTrue(allPropertiesEqualTo(logsByModule, "getModule", "UserManagement"));

	}

	@Test
	public void testDeleteUnxisingAlertType() throws Exception {
		alertService.deleteAlertType("NO_SUCH_ALERT_TYPE");
		
		ActivityRecordFilter filterByDeleteAction = new ActivityRecordFilter().filterByAction("DeleteAlertType");
		Collection<ActivityRecord> logsByAction = activityLogService.getRecords(filterByDeleteAction);
		// no record has been logged
		assertEquals(0, logsByAction.size());
	}
	
	@Test
	public void testAlertManagementActivitySuccess() throws Exception {
		alertService.deleteAllAlerts();

		AlertType at = new AlertType("test", "test", "", "MINOR", "", "", true, false, false);
		Collection<ActivityRecord> logsByAction = null;
		try {
			alertService.createAlertType(at);

			ActivityRecordFilter filterByAddAction = new ActivityRecordFilter();
			filterByAddAction.filterByAction("AddAlertType");
			logsByAction = activityLogService.getRecords(filterByAddAction);
			assertEquals(1, logsByAction.size());
			assertTrue(allPropertiesEqualTo(logsByAction, "getAction", "AddAlertType"));

			// at.setBody("body");
			alertService.updateAlertType("test", at);
			ActivityRecordFilter filterByUpdateAction = new ActivityRecordFilter();
			filterByUpdateAction.filterByAction("UpdateAlertType");
			logsByAction = activityLogService.getRecords(filterByUpdateAction);
			assertEquals(1, logsByAction.size());
			assertTrue(allPropertiesEqualTo(logsByAction, "getAction", "UpdateAlertType"));

			Alert alert = new Alert("name", "test", new Date(), "message");
			alertService.raiseAlert(alert);
			Thread.sleep(800);
			
			Collection<Alert> activeAlerts = alertService.getActiveAlerts(TOFactory.EMPTY_TO, false);
			for (Alert a : activeAlerts) {
				alertService.dismissAlert(a, "test");
			}
			
			ActivityRecordFilter filterByDismissAction = new ActivityRecordFilter();
			filterByDismissAction.filterByAction("DismissObjectAlert");

			logsByAction = activityLogService.getRecords(filterByDismissAction);
			assertEquals(1, logsByAction.size());
			assertTrue(allPropertiesEqualTo(logsByAction, "getAction", "DismissObjectAlert"));

			alertService.raiseAlert(alert);
			Thread.sleep(800);
			
			activeAlerts = alertService.getActiveAlerts(TOFactory.EMPTY_TO, false);
			for (Alert a : activeAlerts) {
				alertService.dismissAlert(a, "ack");
			}

			logsByAction = activityLogService.getRecords(filterByDismissAction);
			assertEquals(2, logsByAction.size());
			assertTrue(allPropertiesEqualTo(logsByAction, "getAction", "DismissObjectAlert"));

			alertService.raiseAlert(alert);
			Thread.sleep(800);
			
			activeAlerts = alertService.getActiveAlerts(TOFactory.EMPTY_TO, false);
			for (Alert a : activeAlerts) {
				alertService.dismissAlert(a.getAlertId(), "test");
			}
			logsByAction = activityLogService.getRecords(filterByDismissAction);
			assertEquals(3, logsByAction.size());
			assertTrue(allPropertiesEqualTo(logsByAction, "getAction", "DismissObjectAlert"));

			alertService.raiseAlert(alert);
			Thread.sleep(800);
			
			activeAlerts = alertService.getActiveAlerts(TOFactory.EMPTY_TO, false);
			for (Alert a : activeAlerts) {
				alertService.dismissAlert(a.getAlertId(), "ack");
			}
			logsByAction = activityLogService.getRecords(filterByDismissAction);
			assertEquals(4, logsByAction.size());
			assertTrue(allPropertiesEqualTo(logsByAction, "getAction", "DismissObjectAlert"));

		} finally {
			alertService.deleteAlertType("test");
			ActivityRecordFilter filterByDeleteAction = new ActivityRecordFilter().filterByAction("DeleteAlertType");
			logsByAction = activityLogService.getRecords(filterByDeleteAction);
			assertEquals(1, logsByAction.size());
			assertTrue(allPropertiesEqualTo(logsByAction, "getAction", "DeleteAlertType"));
		}

		ActivityRecordFilter filterByModule = new ActivityRecordFilter().filterByModule("AlertManagement");
		Collection<ActivityRecord> logsByModule = activityLogService.getRecords(filterByModule);
		assertEquals(7, logsByModule.size());
		assertTrue(allPropertiesEqualTo(logsByModule, "getModule", "AlertManagement"));

	}

	@Test
	public void testGetRecordsByUserAndTimeSuccess() throws Exception {

		final Date start = new Date(System.currentTimeMillis() - 1000);
		new LinkedList<ActivityRecordDO>();
		activityLogService.addRecord(new ActivityRecord("admin", "alert", "raise", "description"));

		final Date end = new Date(System.currentTimeMillis() + 1000);

		ActivityRecordFilter filterByUserAndTime = new ActivityRecordFilter().filterByUser("admin").filterByTime(start,
		        end);
		Collection<ActivityRecord> result = activityLogService.getRecords(filterByUserAndTime);
		assertNotNull(result);
		assertEquals(1, result.size());

		assertTrue(allPropertiesEqualTo(result, "getUserName", "admin"));

	}

	@Test
	public void testGetRecordsByModuleAndTimeSuccess() throws Exception {

		final Date start = new Date(System.currentTimeMillis() - 1000);

		activityLogService.addRecord(new ActivityRecord("admin", "alert", "raise", "description"));
		final Date end = new Date(System.currentTimeMillis() + 1000);

		ActivityRecordFilter filterByModuleAndTime = new ActivityRecordFilter().filterByModule("alert").filterByTime(
		        start, end);
		Collection<ActivityRecord> result = activityLogService.getRecords(filterByModuleAndTime);

		assertNotNull(result);
		assertEquals(1, result.size());

		assertTrue(allPropertiesEqualTo(result, "getModule", "alert"));

	}

	@Test
	public void testGetRecordsByActionAndTimeSuccess() throws Exception {

		final Date start = new Date(System.currentTimeMillis() - 1000);
		activityLogService.addRecord(new ActivityRecord("admin", "alert", "raise", "description"));

		final Date end = new Date(System.currentTimeMillis() + 1000);
		ActivityRecordFilter filterByActionAndTime = new ActivityRecordFilter().filterByAction("raise").filterByTime(
		        start, end);
		Collection<ActivityRecord> result = activityLogService.getRecords(filterByActionAndTime);
		assertNotNull(result);
		assertEquals(1, result.size());

		assertTrue(allPropertiesEqualTo(result, "getAction", "raise"));

	}

	@Test(expected = IllegalInputParameterException.class)
	public void testUpdateNullModuleFailure() {
		activityLogService.addRecord(new ActivityRecord("admin", "alert", "raise", "description"));
		ActivityRecordFilter filterByUser = new ActivityRecordFilter().filterByUser("admin");
		Collection<ActivityRecord> actRecords = activityLogService.getRecords(filterByUser);
		Iterator itr = actRecords.iterator();
		ActivityRecord record = null;
		while (itr.hasNext()) {
			record = (ActivityRecord) itr.next();
		}
		assertEquals(1, actRecords.size());
		record.setModule(null);
		activityLogService.updateRecord(record);
	}

	@Test(expected = IllegalInputParameterException.class)
	public void testUpdateNullUserFailure() {
		activityLogService.addRecord(new ActivityRecord("admin", "alert", "raise", "description"));
		ActivityRecordFilter filterByUser = new ActivityRecordFilter().filterByUser("admin");
		Collection<ActivityRecord> actRecords = activityLogService.getRecords(filterByUser);
		Iterator itr = actRecords.iterator();
		ActivityRecord record = null;
		while (itr.hasNext()) {
			record = (ActivityRecord) itr.next();
		}
		assertEquals(1, actRecords.size());
		record.setUserName(null);
		activityLogService.updateRecord(record);
	}

	@Test(expected = IllegalInputParameterException.class)
	public void testUpdateNullActionFailure() {
		activityLogService.addRecord(new ActivityRecord("admin", "alert", "raise", "description"));
		ActivityRecordFilter filterByUser = new ActivityRecordFilter().filterByUser("admin");
		Collection<ActivityRecord> actRecords = activityLogService.getRecords(filterByUser);
		Iterator itr = actRecords.iterator();
		ActivityRecord record = null;
		while (itr.hasNext()) {
			record = (ActivityRecord) itr.next();
		}
		assertEquals(1, actRecords.size());
		record.setAction(null);
		activityLogService.updateRecord(record);
	}

	@Test(expected = IllegalInputParameterException.class)
	public void testUpdateEmptyModuleFailure() {
		activityLogService.addRecord(new ActivityRecord("admin", "alert", "raise", "description"));
		ActivityRecordFilter filterByUser = new ActivityRecordFilter().filterByUser("admin");
		Collection<ActivityRecord> actRecords = activityLogService.getRecords(filterByUser);
		Iterator itr = actRecords.iterator();
		ActivityRecord record = null;
		while (itr.hasNext()) {
			record = (ActivityRecord) itr.next();
		}
		assertEquals(1, actRecords.size());
		String module = "";

		record.setModule(module);
		activityLogService.updateRecord(record);
	}

	@Test(expected = IllegalInputParameterException.class)
	public void testUpdateEmptyUserFailure() {
		activityLogService.addRecord(new ActivityRecord("admin", "alert", "raise", "description"));
		ActivityRecordFilter filterByUser = new ActivityRecordFilter().filterByUser("admin");
		Collection<ActivityRecord> actRecords = activityLogService.getRecords(filterByUser);
		Iterator itr = actRecords.iterator();
		ActivityRecord record = null;
		while (itr.hasNext()) {
			record = (ActivityRecord) itr.next();
		}
		assertEquals(1, actRecords.size());
		String userName = "";

		record.setUserName(userName);
		activityLogService.updateRecord(record);
	}

	@Test(expected = IllegalInputParameterException.class)
	public void testUpdateEmptyActionFailure() {
		activityLogService.addRecord(new ActivityRecord("admin", "alert", "raise", "description"));
		ActivityRecordFilter filterByUser = new ActivityRecordFilter().filterByUser("admin");
		Collection<ActivityRecord> actRecords = activityLogService.getRecords(filterByUser);
		Iterator itr = actRecords.iterator();
		ActivityRecord record = null;
		while (itr.hasNext()) {
			record = (ActivityRecord) itr.next();
		}
		assertEquals(1, actRecords.size());
		String action = "";

		record.setAction(action);
		activityLogService.updateRecord(record);
	}

	// @Ignore
	@Test(expected = IllegalInputParameterException.class)
	public void testUpdateNullRecordFailure() {
		ActivityRecord record = null;
		activityLogService.updateRecord(record);

	}

	@Test
	public void testUpdateErrObjectRecord() {
		activityLogService.addRecord(new ActivityRecord("admin", "alert", "raise", "description"));
		ActivityRecordFilter filterByUser = new ActivityRecordFilter().filterByUser("admin");
		Collection<ActivityRecord> actRecords = activityLogService.getRecords(filterByUser);
		Iterator itr = actRecords.iterator();
		ActivityRecord record = null;
		while (itr.hasNext()) {
			record = (ActivityRecord) itr.next();
		}

		record.setObject(TOFactory.getInstance().loadTO("SENSOR:" + 104));
		activityLogService.updateRecord(record);
	}

	@Test
	public void testUpdateNullObjectRecord() {
		activityLogService.addRecord(new ActivityRecord("admin", "alert", "raise", "description"));
		ActivityRecordFilter filterByUser = new ActivityRecordFilter().filterByUser("admin");
		Collection<ActivityRecord> actRecords = activityLogService.getRecords(filterByUser);
		Iterator itr = actRecords.iterator();
		ActivityRecord record = null;
		while (itr.hasNext()) {
			record = (ActivityRecord) itr.next();
		}

		record.setObject(null);
		activityLogService.updateRecord(record);
	}

	@Test
	public void testUpdateNullRecord() {
		activityLogService.addRecord(new ActivityRecord("admin", "alert", "raise", "description"));
		ActivityRecordFilter filterByUser = new ActivityRecordFilter().filterByUser("admin");
		Collection<ActivityRecord> actRecords = activityLogService.getRecords(filterByUser);
		Iterator itr = actRecords.iterator();
		ActivityRecord record = null;
		while (itr.hasNext()) {
			record = (ActivityRecord) itr.next();
		}

		record.setObject(null);
		activityLogService.updateRecord(record);
	}

	@Test
	public void testSettingTimeRecord() {
		Date start = new Date(System.currentTimeMillis() - 3000);
		ActivityRecord record = new ActivityRecord("admin", "alert", "raise", "description");
		record.setTime(new Date(System.currentTimeMillis() - 2000));
		activityLogService.addRecord(record);
		Date end = new Date(System.currentTimeMillis() - 1000);
		ActivityRecordFilter filterByTime = new ActivityRecordFilter().filterByTime(start, end);

		Collection<ActivityRecord> results = activityLogService.getRecords(filterByTime);
		assertNotNull(results);
		assertEquals(1, results.size());

	}

	@Test
	public void testUpdateTimeRecord() {
		ActivityRecord record = new ActivityRecord("admin", "alert", "raise", "description");

		record.setTime(new Date(System.currentTimeMillis() - 100000000));
		activityLogService.addRecord(record);
		Date start = new Date(System.currentTimeMillis() - 1000);
		activityLogService.addRecord(new ActivityRecord("admin", "alert", "raise", "description"));

		Date end = new Date(System.currentTimeMillis() + 1000);

		ActivityRecordFilter filterByTime = new ActivityRecordFilter().filterByTime(start, end);
		Collection<ActivityRecord> results = activityLogService.getRecords(filterByTime);
		assertNotNull(results);
		assertEquals(1, results.size());

		Iterator itr = results.iterator();
		record = null;
		while (itr.hasNext()) {
			record = (ActivityRecord) itr.next();
		}

		start = new Date(System.currentTimeMillis() - 3000);
		record.setTime(new Date(System.currentTimeMillis() - 2000));
		activityLogService.updateRecord(record);

		end = new Date(System.currentTimeMillis() - 1000);

		filterByTime = new ActivityRecordFilter().filterByTime(start, end);
		results = activityLogService.getRecords(filterByTime);
		assertNotNull(results);
		assertEquals(1, results.size());
	}

	@Test
	public void testUpdateRecord() {
		activityLogService.addRecord(new ActivityRecord("admin", "alert", "raise", "description"));
		ActivityRecordFilter filterByUser = new ActivityRecordFilter().filterByUser("admin");
		Collection<ActivityRecord> actRecords = activityLogService.getRecords(filterByUser);
		Iterator itr = actRecords.iterator();
		ActivityRecord record = null;
		while (itr.hasNext()) {
			record = (ActivityRecord) itr.next();
		}
		assertEquals(1, actRecords.size());
		String action = "action";
		String user = "User";
		String descr = "New Description";
		String module = "Module";

		record.setAction(action);
		record.setUserName(user);
		record.setModule(module);
		record.setDescription(descr);
		activityLogService.updateRecord(record);

		ActivityRecordFilter filterByAction = new ActivityRecordFilter().filterByAction(action);
		actRecords = activityLogService.getRecords(filterByAction);
		assertEquals(1, actRecords.size());

		ActivityRecordFilter filterByUserName = new ActivityRecordFilter().filterByUser(user);
		actRecords = activityLogService.getRecords(filterByUserName);
		assertEquals(1, actRecords.size());

		ActivityRecordFilter filterByModule = new ActivityRecordFilter().filterByModule(module);
		actRecords = activityLogService.getRecords(filterByModule);
		assertEquals(1, actRecords.size());

	}

	@Test
	public void testUpdateRecords() throws Exception {
		for (int i = 0; i < 10; i++) {
			activityLogService.addRecord(new ActivityRecord("admin", "alert", "raise" + i, "description" + i));
		}
		ActivityRecordFilter filterByUser = new ActivityRecordFilter().filterByUser("admin");
		Collection<ActivityRecord> actRecords = activityLogService.getRecords(filterByUser);
		Iterator itr = actRecords.iterator();

		assertEquals(10, actRecords.size());

		ActivityRecord record = null;
		while (itr.hasNext()) {
			record = (ActivityRecord) itr.next();
			record.setAction("action");
		}

		activityLogService.updateRecord(actRecords);

		ActivityRecordFilter filterByAction = new ActivityRecordFilter().filterByAction("action");
		actRecords = activityLogService.getRecords(filterByAction);
		itr = actRecords.iterator();

		assertEquals(10, actRecords.size());

		assertTrue(allPropertiesEqualTo(actRecords, "getAction", "action"));

	}

	@Test
	public void testDeleteRecord() {
		activityLogService.addRecord(new ActivityRecord("admin", "module1", "UserLogin", "descr"));
		activityLogService.addRecord(new ActivityRecord("admin", "module2", "UserLogin", "descr"));
		activityLogService.addRecord(new ActivityRecord("admin", "module1", "UserLogin", "descr"));
		activityLogService.addRecord(new ActivityRecord("admin", "module2", "UserLogin", "descr"));
		ActivityRecordFilter filter = new ActivityRecordFilter();
		Collection<ActivityRecord> result = activityLogService.getRecords(filter);
		assertEquals(4, result.size());
		int ID = 1;
		activityLogService.deleteRecord(ID);
		result = activityLogService.getRecords(filter);
		assertEquals(3, result.size());

	}

	@Test
	public void testDeleteRecords() {
		activityLogService.addRecord(new ActivityRecord("admin", "module1", "UserLogin", "descr"));
		activityLogService.addRecord(new ActivityRecord("admin", "module2", "UserLogin", "descr"));
		activityLogService.addRecord(new ActivityRecord("admin", "module3", "UserLogin", "descr"));
		activityLogService.addRecord(new ActivityRecord("admin", "module4", "UserLogin", "descr"));
		activityLogService.addRecord(new ActivityRecord("admin", "module5", "UserLogin", "descr"));
		activityLogService.addRecord(new ActivityRecord("admin", "module6", "UserLogin", "descr"));
		activityLogService.addRecord(new ActivityRecord("admin", "module7", "UserLogin", "descr"));
		activityLogService.addRecord(new ActivityRecord("admin", "module8", "UserLogin", "descr"));
		ActivityRecordFilter filter = new ActivityRecordFilter();
		Collection<ActivityRecord> result = activityLogService.getRecords(filter);
		assertEquals(8, result.size());
		HashSet<Integer> ID = new HashSet();
		ID.add(1);
		ID.add(2);
		ID.add(3);
		activityLogService.deleteRecord(ID);
		result = activityLogService.getRecords(filter);
		assertEquals(5, result.size());
	}

	@Test
	public void testDeleteObjectSetReferenceToNullOnActivityLogSuccess() throws EnvException {
		try {
			envService.createObjectType("TEST_TYPE");
			TO object = envService.createObject("TEST_TYPE");

			ActivityRecord ar = new ActivityRecord("admin", "module1", "UserLogin", "descr");
			ar.setObject(object);
			activityLogService.addRecord(ar);

			ActivityRecordFilter filter = new ActivityRecordFilter();
			Collection<ActivityRecord> result = activityLogService.getRecords(filter);
			assertEquals(1, result.size());

			ActivityRecord returnedRecord = result.iterator().next();
			assertEquals(object, returnedRecord.getObject());

			envService.deleteObject(object);

			result = activityLogService.getRecords(filter);
			assertEquals(1, result.size());

			returnedRecord = result.iterator().next();
			assertEquals(TOFactory.EMPTY_TO, returnedRecord.getObject());

		} finally {
			envService.deleteObjectType("TEST_TYPE");
		}
	}

	@Test
	public void testAddRecords() {
		String module = "Module";
		String action = "Action";
		ActivityRecordFilter filter = new ActivityRecordFilter();
		Collection<ActivityRecord> result = activityLogService.getRecords(filter);
		assertEquals(0, result.size());
		for (int i = 1; i < 11; i++) {
			activityLogService.addRecord(new ActivityRecord("admin", module + i % 5, action + i % 3, "some descr"));
			result = activityLogService.getRecords(filter);
			assertEquals(i, result.size());
		}
	}

	@Test
	public void testGetDistinctSortedModulesSuccess() {
		String module = "Module";
		String action = "Action";
		Set<String> requiredModules = new HashSet<String>();
		// generating log records of 5 different mouldes and 3 actions
		for (int i = 0; i < 10; i++) {
			activityLogService.addRecord(new ActivityRecord("admin", module + i % 5, action + i % 3, "some descr"));
			requiredModules.add(module + i % 5);
		}

		List<String> modules = activityLogService.getActivityModules();
		assertNotNull(modules);
		assertEquals("'distinct' option doesn't work properly", modules.size(),
		        new LinkedHashSet<String>(modules).size());
		assertTrue("not all configured modules returned", modules.containsAll(requiredModules));
	}

	@Test
	public void testGetDistinctSortedActivitiesSuccess() {
		String module = "Module";
		String action = "Action";
		Set<String> requiredActions = new HashSet<String>();
		// generating log records of 5 different mouldes and 3 actions
		for (int i = 0; i < 10; i++) {
			activityLogService.addRecord(new ActivityRecord("admin", module + i % 5, action + i % 3, "some descr"));
			requiredActions.add(action + i % 3);
		}

		List<String> actions = activityLogService.getActivityActions();
		assertNotNull(actions);
		assertEquals("'distinct' option doesn't work properly", actions.size(),
		        new LinkedHashSet<String>(actions).size());
		assertTrue("not all configured actions returned", actions.containsAll(requiredActions));
	}

	protected static IDatabaseConnection getConnection() throws Exception {
		return databaseTester.getConnection();
	}

	private static boolean allPropertiesEqualTo(final Collection<ActivityRecord> records, final String propName,
	        final Object value) throws Exception {
		Method getMethod = ActivityRecord.class.getMethod(propName);
		for (ActivityRecord record : records) {
			Object val = getMethod.invoke(record);
			if ((val == null && value != null) || !value.equals(val))
				return false;
		}
		return true;
	}

}
