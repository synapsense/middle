package com.synapsense.func.erlnode.commands;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.ericsson.otp.erlang.OtpErlangAtom;
import com.ericsson.otp.erlang.OtpErlangDouble;
import com.ericsson.otp.erlang.OtpErlangInt;
import com.ericsson.otp.erlang.OtpErlangList;
import com.ericsson.otp.erlang.OtpErlangLong;
import com.ericsson.otp.erlang.OtpErlangObject;
import com.ericsson.otp.erlang.OtpErlangString;
import com.ericsson.otp.erlang.OtpErlangTuple;
import com.synapsense.dto.ObjectType;
import com.synapsense.dto.PropertyDescr;
import com.synapsense.dto.TO;
import com.synapsense.impl.erlnode.ExceptionTypes;
import com.synapsense.impl.erlnode.commands.CreateObjectCommand2;
import com.synapsense.impl.erlnode.utils.ToConverter;
import com.synapsense.service.Environment;

public class CreateObjectCommand2Test {
	static UnitTestHelper utHelper = null;
	static String testType = "testingObjType_CreateObjectTwoParTest";
	static Map<String, Object> envMap;
	static Environment env;

	static OtpErlangTuple valueTO1;
	static OtpErlangTuple valueTO2;
	static OtpErlangTuple valueTO3;
	static OtpErlangTuple valueTO4;
	static OtpErlangTuple valueTO5;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		utHelper = new UnitTestHelper();
		utHelper.initializeEnvironment();
		env = utHelper.getEnvironment();
		envMap = new HashMap<String, Object>();
		envMap.put("env", env);

		valueTO1 = new OtpErlangTuple(new OtpErlangObject[] { new OtpErlangAtom("value_to"),
		        new OtpErlangString("name"), new OtpErlangString("someName") });

		valueTO2 = new OtpErlangTuple(new OtpErlangObject[] { new OtpErlangAtom("value_to"),
		        new OtpErlangString("name"), new OtpErlangString("someName2") });

		valueTO3 = new OtpErlangTuple(new OtpErlangObject[] { new OtpErlangAtom("value_to"),
		        new OtpErlangString("int"), new OtpErlangInt(1) });

		valueTO4 = new OtpErlangTuple(new OtpErlangObject[] { new OtpErlangAtom("value_to"),
		        new OtpErlangString("long"), new OtpErlangLong(123123123123L) });

		valueTO5 = new OtpErlangTuple(new OtpErlangObject[] { new OtpErlangAtom("value_to"),
		        new OtpErlangString("double"), new OtpErlangDouble(1212.2), new OtpErlangLong(1212L) });

	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		utHelper.removeTypeCompletly(testType);
		utHelper.dispose();
	}

	@Before
	public void setUp() throws Exception {
		env.deleteObjectType(testType);
		ObjectType otype = env.createObjectType(testType);

		otype.getPropertyDescriptors().add(new PropertyDescr("name", String.class.getName()));
		otype.getPropertyDescriptors().add(new PropertyDescr("int", Integer.class.getName()));
		otype.getPropertyDescriptors().add(new PropertyDescr("long", Long.class.getName()));
		otype.getPropertyDescriptors().add(new PropertyDescr("double", Double.class.getName()));
		otype.getPropertyDescriptors().add(new PropertyDescr("to", TO.class.getName()));
		env.updateObjectType(otype);
	}

	@After
	public void tearDown() throws Exception {
		env.deleteObjectType(testType);
	}

	@Test
	public void SuccessCreateObjectTest() throws Exception {
		CreateObjectCommand2 command = new CreateObjectCommand2(envMap);
		OtpErlangList valueTOList1 = new OtpErlangList(valueTO1);
		OtpErlangTuple params1 = new OtpErlangTuple(new OtpErlangObject[] {
		        new OtpErlangString("createObjectCommand2"), new OtpErlangString(testType), valueTOList1 });
		OtpErlangTuple obj1 = (OtpErlangTuple) command.run(params1);
		assertEquals("ok", obj1.elementAt(0).toString());
		TO<?> to1 = ToConverter.parseTo((OtpErlangTuple) obj1.elementAt(1));
		assertEquals(testType, to1.getTypeName());
		assertEquals("someName", env.getPropertyValue(to1, "name", String.class));

		OtpErlangTuple valueTO6 = new OtpErlangTuple(new OtpErlangObject[] { new OtpErlangAtom("value_to"),
		        new OtpErlangString("to"), ToConverter.serializeTo(to1) });
		OtpErlangList valueTOList2 = new OtpErlangList(new OtpErlangTuple[] { valueTO2, valueTO3, valueTO4, valueTO5,
		        valueTO6 });
		OtpErlangTuple params2 = new OtpErlangTuple(new OtpErlangObject[] {
		        new OtpErlangString("createObjectCommand2"), new OtpErlangString(testType), valueTOList2 });
		OtpErlangTuple obj2 = (OtpErlangTuple) command.run(params2);
		assertEquals("ok", obj2.elementAt(0).toString());
		TO<?> to2 = ToConverter.parseTo((OtpErlangTuple) obj2.elementAt(1));
		assertEquals(testType, to1.getTypeName());
		assertEquals("someName2", env.getPropertyValue(to2, "name", String.class));
		assertEquals(new Integer(1), env.getPropertyValue(to2, "int", Integer.class));
		assertEquals(new Long(123123123123L), env.getPropertyValue(to2, "long", Long.class));
		assertEquals(new Double(1212.2), env.getPropertyValue(to2, "double", Double.class));
		assertEquals(to1, env.getPropertyValue(to2, "to", TO.class));
		// test with empty parameters list
		command = new CreateObjectCommand2(envMap);
		valueTOList1 = new OtpErlangList();
		OtpErlangTuple obj3 = (OtpErlangTuple) command.run(new OtpErlangTuple(new OtpErlangObject[] {
		        new OtpErlangString("createObjectCommand2"), new OtpErlangString(testType), valueTOList1 }));
		assertEquals("ok", obj3.elementAt(0).toString());

		env.deleteObject(ToConverter.parseTo((OtpErlangTuple) obj1.elementAt(1)));
		env.deleteObject(ToConverter.parseTo((OtpErlangTuple) obj2.elementAt(1)));
		env.deleteObject(ToConverter.parseTo((OtpErlangTuple) obj3.elementAt(1)));

	}

	@Test
	public void FailCreateObject() throws Exception {
		// test with non existing object type
		CreateObjectCommand2 command = new CreateObjectCommand2(envMap);
		OtpErlangList valueTOList1 = new OtpErlangList(valueTO1);
		OtpErlangTuple obj0 = (OtpErlangTuple) command
		        .run(new OtpErlangTuple(new OtpErlangObject[] { new OtpErlangString("createObjectCommand2"),
		                new OtpErlangString("someNotExistingType"), valueTOList1 }));
		assertEquals("exception", obj0.elementAt(0).toString());
		assertEquals(ExceptionTypes.UNABLE_TO_CONVERT_PROPERTY.getExceptionType(),
		        ((OtpErlangString) obj0.elementAt(1)).stringValue());

		// test with non existing property
		OtpErlangTuple valueTO6 = new OtpErlangTuple(new OtpErlangObject[] { new OtpErlangAtom("value_to"),
		        new OtpErlangString("notExistingProperty"), new OtpErlangString("notExistingProperty") });
		OtpErlangList valueTOList2 = new OtpErlangList(new OtpErlangTuple[] { valueTO2, valueTO3, valueTO3, valueTO4,
		        valueTO5, valueTO6 });
		OtpErlangTuple params2 = new OtpErlangTuple(new OtpErlangObject[] {
		        new OtpErlangString("createObjectCommand2"), new OtpErlangString(testType), valueTOList2 });
		OtpErlangTuple obj2 = (OtpErlangTuple) command.run(params2);
		assertEquals("exception", obj2.elementAt(0).toString());
		assertEquals(ExceptionTypes.UNABLE_TO_CONVERT_PROPERTY.getExceptionType(),
		        ((OtpErlangString) obj2.elementAt(1)).stringValue());

	}

	@Test
	public void InvalidParametersTest() throws Exception {
		CreateObjectCommand2 command = new CreateObjectCommand2(envMap);
		OtpErlangTuple obj = (OtpErlangTuple) command.run(new OtpErlangTuple(
		        new OtpErlangObject[] { new OtpErlangString("createObjectCommand1"), }));
		assertEquals("exception", obj.elementAt(0).toString());
		assertEquals(ExceptionTypes.ILLEGAL_ARGUMENTS.getExceptionType(),
		        ((OtpErlangString) obj.elementAt(1)).stringValue());
		obj = (OtpErlangTuple) command.run(new OtpErlangTuple(new OtpErlangObject[] {
		        new OtpErlangString("createObjectCommand1"), new OtpErlangString(testType) }));
		assertEquals("exception", obj.elementAt(0).toString());
		assertEquals(ExceptionTypes.ILLEGAL_ARGUMENTS.getExceptionType(),
		        ((OtpErlangString) obj.elementAt(1)).stringValue());
	}

}
