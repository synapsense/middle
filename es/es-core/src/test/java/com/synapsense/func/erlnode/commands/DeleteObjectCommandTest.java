package com.synapsense.func.erlnode.commands;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.ericsson.otp.erlang.OtpErlangAtom;
import com.ericsson.otp.erlang.OtpErlangObject;
import com.ericsson.otp.erlang.OtpErlangString;
import com.ericsson.otp.erlang.OtpErlangTuple;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.impl.erlnode.ExceptionTypes;
import com.synapsense.impl.erlnode.commands.DeleteObjectCommand;
import com.synapsense.impl.erlnode.utils.ToConverter;
import com.synapsense.service.Environment;

public class DeleteObjectCommandTest {
	static UnitTestHelper utHelper = null;
	static String testType = "testingObjType_CreateObjectTwoParTest";
	static Map<String, Object> envMap;
	static Environment env;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		utHelper = new UnitTestHelper();
		utHelper.initializeEnvironment();
		env = utHelper.getEnvironment();
		envMap = new HashMap<String, Object>();
		envMap.put("env", env);

	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		utHelper.removeTypeCompletly(testType);
		utHelper.dispose();
	}

	@Before
	public void setUp() throws Exception {

		// ObjectType otype =
		env.createObjectType(testType);

	}

	@After
	public void tearDown() throws Exception {
		env.deleteObjectType(testType);
	}

	@Test
	public void SuccessDeleteObjectTest() throws Exception {

		TO<?> to = env.createObject(testType);
		DeleteObjectCommand command = new DeleteObjectCommand(envMap);
		OtpErlangAtom obj = (OtpErlangAtom) command.run(new OtpErlangTuple(new OtpErlangObject[] {
		        new OtpErlangString("deleteObjectCommand"), ToConverter.serializeTo(to) }));
		assertEquals("ok", obj.toString());
	}

	@Test
	public void FailDeleteObjectTest() throws Exception {
		// delete none existing object
		DeleteObjectCommand command = new DeleteObjectCommand(envMap);
		TO<?> to = TOFactory.getInstance().loadTO(testType + ":11");
		OtpErlangTuple obj = (OtpErlangTuple) command.run(new OtpErlangTuple(new OtpErlangObject[] {
		        new OtpErlangString("deleteObjectCommand"), ToConverter.serializeTo(to) }));
		assertEquals("exception", obj.elementAt(0).toString());
		assertEquals(ExceptionTypes.OBJECT_NOT_FOUND.getExceptionType(),
		        ((OtpErlangString) obj.elementAt(1)).stringValue());
	}

	@Test
	public void InvalidParametersTest() throws Exception {
		DeleteObjectCommand command = new DeleteObjectCommand(envMap);
		OtpErlangTuple obj = (OtpErlangTuple) command.run(new OtpErlangTuple(
		        new OtpErlangObject[] { new OtpErlangString("deleteObjectCommand"), }));
		assertEquals("exception", obj.elementAt(0).toString());
		assertEquals(ExceptionTypes.ILLEGAL_ARGUMENTS.getExceptionType(),
		        ((OtpErlangString) obj.elementAt(1)).stringValue());
	}

}
