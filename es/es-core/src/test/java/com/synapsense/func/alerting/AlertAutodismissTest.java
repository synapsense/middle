package com.synapsense.func.alerting;

import static org.junit.Assert.assertEquals;

import java.util.Collection;
import java.util.Date;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.synapsense.dto.Alert;
import com.synapsense.dto.AlertPriority;
import com.synapsense.dto.AlertStatus;
import com.synapsense.dto.ObjectType;
import com.synapsense.dto.PropertyDescr;
import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.ObjectExistsException;
import com.synapsense.func.ConsoleApp;
import com.synapsense.service.AlertService;
import com.synapsense.service.Environment;

public class AlertAutodismissTest {
	private static AlertService ALERTING_SERVICE;
	private static Environment ENVIRONMENT;

	private static String TEST_TYPE = "TEST_TYPE";

	private static TO<?> TEST_OBJECT_1;
	private static TO<?> TEST_OBJECT_2;

	private static String TEST_ALERT_TYPE = "TEST_ALERT_TYPE";

	private static String TEST_AUTO_ACK_ALERT_TYPE = "TEST_AUTO_ACK_ALERT_TYPE";

	private static final long DISMISS_INTERVAL = 1200L;

	private static AlertingTestHelper helper;

	public static void sleep(long time) {
		try {
			Thread.sleep(time);
		} catch (final InterruptedException e) {
			e.printStackTrace();
		}
	}

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		ConsoleApp.init();

		ENVIRONMENT = ConsoleApp.env;

		ALERTING_SERVICE = ConsoleApp.ALERTING_SERVICE;

		helper = new AlertingTestHelper(ENVIRONMENT, ALERTING_SERVICE);
		ObjectType testType;
		try {
			testType = ENVIRONMENT.createObjectType(TEST_TYPE);
		} catch (ObjectExistsException e) {
			System.out.println("Object type was not deleted properly upon previous test run completion.");
			testType = ENVIRONMENT.getObjectType(TEST_TYPE);
		}
		testType.getPropertyDescriptors().add(new PropertyDescr("name", String.class.getName()));
		testType.getPropertyDescriptors().add(new PropertyDescr("lastValue", Double.class.getName()));
		testType.getPropertyDescriptors().add(new PropertyDescr("location", String.class.getName()));
		testType.getPropertyDescriptors().add(new PropertyDescr("body", String.class.getName()));
		testType.getPropertyDescriptors().add(new PropertyDescr("header", String.class.getName()));

		ENVIRONMENT.updateObjectType(testType);

		TEST_OBJECT_1 = ENVIRONMENT.createObject(TEST_TYPE, new ValueTO[] { new ValueTO("name", "new object1"),
		        new ValueTO("location", "100 150"), new ValueTO("header", "header"), new ValueTO("body", "body") });
		TEST_OBJECT_2 = ENVIRONMENT.createObject(TEST_TYPE, new ValueTO[] { new ValueTO("name", "new object2") });
		ENVIRONMENT.setRelation(TEST_OBJECT_1, TEST_OBJECT_2);

		helper.newPersistentAlertType(TEST_ALERT_TYPE, "Descr", "MAJOR", true, true, false,
		        null);
		helper.newPersistentAlertType(TEST_AUTO_ACK_ALERT_TYPE, "Descr", "MAJOR",
		        true, true, true, null);
		AlertPriority ap = ALERTING_SERVICE.getAlertPriority("MAJOR");
		// change autodismiss interval to one second
		ALERTING_SERVICE.updateAlertPriority("MAJOR", new AlertPriority("MAJOR", DISMISS_INTERVAL, ap.getTiers()));

	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		ALERTING_SERVICE.deleteAllAlerts();
		ALERTING_SERVICE.deleteAlertType(TEST_ALERT_TYPE);
		ALERTING_SERVICE.deleteAlertType(TEST_AUTO_ACK_ALERT_TYPE);
		ENVIRONMENT.deleteObjectType(TEST_TYPE);
		AlertPriority ap = ALERTING_SERVICE.getAlertPriority("MAJOR");
		// change autodismiss interval to one second
		ALERTING_SERVICE.updateAlertPriority("MAJOR", new AlertPriority("MAJOR", 1200000L, ap.getTiers()));
	}

	@After
	public void afterTest() {
		ALERTING_SERVICE.deleteAllAlerts();
	}

	@Test
	public void testAutoAckAlerts() {
		Alert alert_3 = new Alert("alert_error", TEST_AUTO_ACK_ALERT_TYPE, new Date(), "problem with dal",
		        TEST_OBJECT_1);

		ALERTING_SERVICE.raiseAlert(alert_3);
		sleep(DISMISS_INTERVAL / 4);

		Collection<Alert> alerts = ALERTING_SERVICE.getAlerts(AlertFilters.allAlerts(TEST_OBJECT_1));

		assertEquals(1, alerts.size());
		assertEquals(AlertStatus.ACKNOWLEDGED, alerts.iterator().next().getStatus());
		sleep(DISMISS_INTERVAL * 3);

		alerts = ALERTING_SERVICE.getAlerts(AlertFilters.allAlerts(TEST_OBJECT_1));
		assertEquals(1, alerts.size());
		assertEquals(AlertStatus.DISMISSED, alerts.iterator().next().getStatus());
	}

	@Test
	public void testManuallyAckAlerts() {
		Alert alert_3 = new Alert("alert_error", TEST_ALERT_TYPE, new Date(), "problem with dal", TEST_OBJECT_1);

		ALERTING_SERVICE.raiseAlert(alert_3);
		sleep(DISMISS_INTERVAL / 4);

		Collection<Alert> alerts = ALERTING_SERVICE.getAlerts(AlertFilters.allAlerts(TEST_OBJECT_1));
		assertEquals(1, alerts.size());
		assertEquals(AlertStatus.OPENED, alerts.iterator().next().getStatus());
		// wait a bit
		sleep(DISMISS_INTERVAL / 3);
		// should not change state
		alerts = ALERTING_SERVICE.getAlerts(AlertFilters.allAlerts(TEST_OBJECT_1));
		assertEquals(1, alerts.size());
		Alert alert = alerts.iterator().next();
		assertEquals(AlertStatus.OPENED, alert.getStatus());

		ALERTING_SERVICE.acknowledgeAlert(alert.getAlertId(), "ack");
		sleep(DISMISS_INTERVAL / 4);

		alerts = ALERTING_SERVICE.getAlerts(AlertFilters.allAlerts(TEST_OBJECT_1));
		assertEquals(1, alerts.size());
		assertEquals(AlertStatus.ACKNOWLEDGED, alerts.iterator().next().getStatus());

		// wait for dismissor
		sleep(DISMISS_INTERVAL * 3);

		alerts = ALERTING_SERVICE.getAlerts(AlertFilters.allAlerts(TEST_OBJECT_1));
		assertEquals(1, alerts.size());
		assertEquals(AlertStatus.DISMISSED, alerts.iterator().next().getStatus());
	}

	@Test
	public void testAutoAckAlertsReraise() {
		Alert alert_3 = new Alert("alert_error", TEST_AUTO_ACK_ALERT_TYPE, new Date(), "problem with dal",
		        TEST_OBJECT_1);

		ALERTING_SERVICE.raiseAlert(alert_3);
		sleep(DISMISS_INTERVAL / 4);

		Collection<Alert> alerts = ALERTING_SERVICE.getAlerts(AlertFilters.allAlerts(TEST_OBJECT_1));
		assertEquals(1, alerts.size());
		assertEquals(AlertStatus.ACKNOWLEDGED, alerts.iterator().next().getStatus());

		sleep(DISMISS_INTERVAL / 3);

		// raise one more alert the same state
		ALERTING_SERVICE.raiseAlert(alert_3);
		sleep(DISMISS_INTERVAL / 4);

		alerts = ALERTING_SERVICE.getAlerts(AlertFilters.allAlerts(TEST_OBJECT_1));
		assertEquals(1, alerts.size());
		assertEquals(AlertStatus.ACKNOWLEDGED, alerts.iterator().next().getStatus());

		// wait till auto dismiss triggers
		sleep(DISMISS_INTERVAL * 3);

		alerts = ALERTING_SERVICE.getAlerts(AlertFilters.allAlerts(TEST_OBJECT_1));
		assertEquals(1, alerts.size());
		for (Alert alert : alerts) {
			assertEquals(AlertStatus.DISMISSED, alert.getStatus());
		}
	}

	@Test
	public void testDeferAutoDismissWhenAlertReraise() {

		Alert alert_3 = new Alert("alert_error", TEST_ALERT_TYPE, new Date(), "problem with dal", TEST_OBJECT_1);

		ALERTING_SERVICE.raiseAlert(alert_3);
		sleep(DISMISS_INTERVAL / 4);

		Collection<Alert> alerts = ALERTING_SERVICE.getAlerts(AlertFilters.allAlerts(TEST_OBJECT_1));
		assertEquals(1, alerts.size());
		assertEquals(AlertStatus.OPENED, alerts.iterator().next().getStatus());
		// wait a bit
		sleep(DISMISS_INTERVAL / 3);
		
		// wait a bit again , not enough time for dismissing
		ALERTING_SERVICE.raiseAlert(alert_3);
		sleep(DISMISS_INTERVAL / 4);

		// raise again does nothing
		alerts = ALERTING_SERVICE.getAlerts(AlertFilters.allAlerts(TEST_OBJECT_1));
		assertEquals(1, alerts.size());
		assertEquals(AlertStatus.OPENED, alerts.iterator().next().getStatus());

		sleep(DISMISS_INTERVAL * 3);

		// should be one alert
		alerts = ALERTING_SERVICE.getAlerts(AlertFilters.allAlerts(TEST_OBJECT_1));
		assertEquals(1, alerts.size());
		assertEquals(AlertStatus.DISMISSED, alerts.iterator().next().getStatus());
	}

}
