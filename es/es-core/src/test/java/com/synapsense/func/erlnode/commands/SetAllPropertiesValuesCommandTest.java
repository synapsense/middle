package com.synapsense.func.erlnode.commands;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.ericsson.otp.erlang.OtpErlangAtom;
import com.ericsson.otp.erlang.OtpErlangObject;
import com.ericsson.otp.erlang.OtpErlangString;
import com.ericsson.otp.erlang.OtpErlangTuple;
import com.synapsense.dto.ObjectType;
import com.synapsense.dto.PropertyDescr;
import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;
import com.synapsense.impl.erlnode.ExceptionTypes;
import com.synapsense.impl.erlnode.commands.SetAllPropertiesValuesCommand;
import com.synapsense.impl.erlnode.utils.ToConverter;
import com.synapsense.impl.erlnode.utils.ValueTOConverter;
import com.synapsense.service.Environment;

public class SetAllPropertiesValuesCommandTest {
	static UnitTestHelper utHelper = null;
	static String testType = "testingObjType_GetAllPropertiesValuesTwoParTest";
	static Map<String, Object> envMap;
	static Environment env;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		utHelper = new UnitTestHelper();
		utHelper.initializeEnvironment();
		env = utHelper.getEnvironment();
		envMap = new HashMap<String, Object>();
		envMap.put("env", env);

	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		utHelper.removeTypeCompletly(testType);
		utHelper.dispose();
	}

	@Before
	public void setUp() throws Exception {

		ObjectType otype = env.createObjectType(testType);

		otype.getPropertyDescriptors().add(new PropertyDescr("name", String.class.getName()));
		otype.getPropertyDescriptors().add(new PropertyDescr("int", Integer.class.getName()));
		otype.getPropertyDescriptors().add(new PropertyDescr("long", Long.class.getName()));
		otype.getPropertyDescriptors().add(new PropertyDescr("double", Double.class.getName()));
		otype.getPropertyDescriptors().add(new PropertyDescr("to", TO.class.getName()));
		env.updateObjectType(otype);
	}

	@After
	public void tearDown() throws Exception {
		env.deleteObjectType(testType);
	}

	@Test
	public void SuccessSetAllPropertiesValuesTest() throws Exception {
		SetAllPropertiesValuesCommand command = new SetAllPropertiesValuesCommand(envMap);
		TO<?> obj1 = env.createObject(testType);

		OtpErlangAtom res = (OtpErlangAtom) command.run(new OtpErlangTuple(new OtpErlangObject[] {
		        new OtpErlangString("SetAllPropertiesValues"),
		        ToConverter.serializeTo(obj1),
		        ValueTOConverter.serializeValueTOList(Arrays.asList(new ValueTO[] { new ValueTO("name", "obj1"),
		                new ValueTO("int", new Integer(1)), new ValueTO("double", new Double(3.3)), })) }));
		assertEquals("ok", res.toString());

		Collection<ValueTO> values = env.getAllPropertiesValues(obj1);
		for (ValueTO valueTO : values) {
			if ("name".equals(valueTO.getPropertyName()))
				assertEquals("obj1", valueTO.getValue());
			else if ("int".equals(valueTO.getPropertyName()))
				assertEquals(new Integer(1), valueTO.getValue());
			else if ("double".equals(valueTO.getPropertyName()))
				assertEquals(new Double(3.3), valueTO.getValue());
			else if ("long".equals(valueTO.getPropertyName()))
				assertNull(valueTO.getValue());
		}

		env.deleteObject(obj1);
	}

	@Test
	public void InvalidParametersTest() throws Exception {
		TO<?> obj1 = env.createObject(testType);
		SetAllPropertiesValuesCommand command = new SetAllPropertiesValuesCommand(envMap);
		OtpErlangTuple obj = (OtpErlangTuple) command.run(new OtpErlangTuple(
		        new OtpErlangObject[] { new OtpErlangString("createObjectCommand1") }));
		assertEquals("exception", obj.elementAt(0).toString());
		assertEquals(ExceptionTypes.ILLEGAL_ARGUMENTS.getExceptionType(),
		        ((OtpErlangString) obj.elementAt(1)).stringValue());
		obj = (OtpErlangTuple) command.run(new OtpErlangTuple(new OtpErlangObject[] {
		        new OtpErlangString("createObjectCommand1"), ToConverter.serializeTo(obj1) }));
		assertEquals("exception", obj.elementAt(0).toString());
		assertEquals(ExceptionTypes.ILLEGAL_ARGUMENTS.getExceptionType(),
		        ((OtpErlangString) obj.elementAt(1)).stringValue());
	}

}
