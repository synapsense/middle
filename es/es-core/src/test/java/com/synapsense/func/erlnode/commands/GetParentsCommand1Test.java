package com.synapsense.func.erlnode.commands;

import static org.junit.Assert.assertEquals;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.ericsson.otp.erlang.OtpErlangList;
import com.ericsson.otp.erlang.OtpErlangObject;
import com.ericsson.otp.erlang.OtpErlangString;
import com.ericsson.otp.erlang.OtpErlangTuple;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.impl.erlnode.ExceptionTypes;
import com.synapsense.impl.erlnode.commands.GetParentsCommand1;
import com.synapsense.impl.erlnode.utils.ToConverter;
import com.synapsense.service.Environment;

public class GetParentsCommand1Test {
	static UnitTestHelper utHelper = null;
	static String testType1 = "testType1";
	static String testType2 = "testType2";
	static Map<String, Object> envMap;
	static Environment env;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		utHelper = new UnitTestHelper();
		utHelper.initializeEnvironment();
		env = utHelper.getEnvironment();
		envMap = new HashMap<String, Object>();
		envMap.put("env", env);

	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		utHelper.removeTypeCompletly(testType1);
		utHelper.removeTypeCompletly(testType2);
		utHelper.dispose();
	}

	@Before
	public void setUp() throws Exception {

		env.createObjectType(testType1);
		env.createObjectType(testType2);

	}

	@After
	public void tearDown() throws Exception {
		env.deleteObjectType(testType1);
		env.deleteObjectType(testType2);
	}

	@Test
	public void SuccessGetParentsTest() throws Exception {

		TO<?> parent = env.createObject(testType1);
		TO<?> child = env.createObject(testType2);
		env.setRelation(parent, child);
		GetParentsCommand1 command = new GetParentsCommand1(envMap);
		OtpErlangTuple obj = (OtpErlangTuple) command.run(new OtpErlangTuple(new OtpErlangObject[] {
		        new OtpErlangString("GetParentsCommand1"), ToConverter.serializeTo(child) }));
		assertEquals("ok", obj.elementAt(0).toString());
		Collection<TO<?>> res = ToConverter.parseToList((OtpErlangList) obj.elementAt(1));
		assertEquals(1, res.size());
		assertEquals(parent, res.toArray()[0]);

		obj = (OtpErlangTuple) command.run(new OtpErlangTuple(new OtpErlangObject[] {
		        new OtpErlangString("GetParentsCommand1"), ToConverter.serializeTo(parent) }));
		assertEquals("ok", obj.elementAt(0).toString());
		res = ToConverter.parseToList((OtpErlangList) obj.elementAt(1));
		assertEquals(0, res.size());

		env.deleteObject(parent);
		env.deleteObject(child);
	}

	@Test
	public void FailGetParentsTest() throws Exception {

		GetParentsCommand1 command = new GetParentsCommand1(envMap);
		TO<?> to = TOFactory.getInstance().loadTO(testType1 + ":11");
		OtpErlangTuple obj = (OtpErlangTuple) command.run(new OtpErlangTuple(new OtpErlangObject[] {
		        new OtpErlangString("GetParentsCommand1"), ToConverter.serializeTo(to) }));
		assertEquals("exception", obj.elementAt(0).toString());
		assertEquals(ExceptionTypes.OBJECT_NOT_FOUND.getExceptionType(),
		        ((OtpErlangString) obj.elementAt(1)).stringValue());

	}

	@Test
	public void InvalidParametersTest() throws Exception {
		GetParentsCommand1 command = new GetParentsCommand1(envMap);
		OtpErlangTuple obj = (OtpErlangTuple) command.run(new OtpErlangTuple(
		        new OtpErlangObject[] { new OtpErlangString("GetParentsCommand1"), }));
		assertEquals("exception", obj.elementAt(0).toString());
		assertEquals(ExceptionTypes.ILLEGAL_ARGUMENTS.getExceptionType(),
		        ((OtpErlangString) obj.elementAt(1)).stringValue());
	}
}
