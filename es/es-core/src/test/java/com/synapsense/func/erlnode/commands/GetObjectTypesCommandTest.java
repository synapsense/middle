package com.synapsense.func.erlnode.commands;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.ericsson.otp.erlang.OtpErlangList;
import com.ericsson.otp.erlang.OtpErlangObject;
import com.ericsson.otp.erlang.OtpErlangString;
import com.ericsson.otp.erlang.OtpErlangTuple;
import com.synapsense.impl.erlnode.ExceptionTypes;
import com.synapsense.impl.erlnode.commands.GetObjectTypesCommand;
import com.synapsense.service.Environment;

public class GetObjectTypesCommandTest {
	static UnitTestHelper utHelper = null;
	static String testType1 = "testType1";
	static String testType2 = "testType2";
	static Map<String, Object> envMap;
	static Environment env;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		utHelper = new UnitTestHelper();
		utHelper.initializeEnvironment();
		env = utHelper.getEnvironment();
		envMap = new HashMap<String, Object>();
		envMap.put("env", env);

	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		utHelper.removeTypeCompletly(testType1);
		utHelper.removeTypeCompletly(testType2);
		utHelper.dispose();
	}

	@Before
	public void setUp() throws Exception {

		env.createObjectType(testType1);
		env.createObjectType(testType2);

	}

	@After
	public void tearDown() throws Exception {
		env.deleteObjectType(testType1);
		env.deleteObjectType(testType2);
	}

	@Test
	public void SuccessGetObjectTypesTest() throws Exception {

		GetObjectTypesCommand command = new GetObjectTypesCommand(envMap);
		OtpErlangTuple obj = (OtpErlangTuple) command.run(new OtpErlangTuple(
		        new OtpErlangObject[] { new OtpErlangString("GetObjectTypesCommand") }));
		assertEquals("ok", obj.elementAt(0).toString());
		OtpErlangList list = (OtpErlangList) obj.elementAt(1);

		List<String> types = new ArrayList<String>();
		for (int i = 0; i < list.arity(); i++) {
			types.add(((OtpErlangString) list.elementAt(i)).stringValue());
		}
		assertTrue(types.contains(testType1));
		assertTrue(types.contains(testType2));
	}

	@Test
	public void InvalidParametersTest() throws Exception {
		GetObjectTypesCommand command = new GetObjectTypesCommand(envMap);
		OtpErlangTuple obj = (OtpErlangTuple) command.run(new OtpErlangTuple(new OtpErlangObject[] {}));
		assertEquals("exception", obj.elementAt(0).toString());
		assertEquals(ExceptionTypes.ILLEGAL_ARGUMENTS.getExceptionType(),
		        ((OtpErlangString) obj.elementAt(1)).stringValue());
	}
}
