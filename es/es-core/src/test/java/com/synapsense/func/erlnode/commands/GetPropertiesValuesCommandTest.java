package com.synapsense.func.erlnode.commands;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.ericsson.otp.erlang.OtpErlangList;
import com.ericsson.otp.erlang.OtpErlangObject;
import com.ericsson.otp.erlang.OtpErlangString;
import com.ericsson.otp.erlang.OtpErlangTuple;
import com.synapsense.dto.CollectionTO;
import com.synapsense.dto.ObjectType;
import com.synapsense.dto.PropertyDescr;
import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;
import com.synapsense.impl.erlnode.ExceptionTypes;
import com.synapsense.impl.erlnode.commands.GetPropertiesValuesCommand;
import com.synapsense.impl.erlnode.utils.CollectionToConverter;
import com.synapsense.impl.erlnode.utils.ToConverter;
import com.synapsense.service.Environment;

public class GetPropertiesValuesCommandTest {
	static UnitTestHelper utHelper = null;
	static String testType = "testingObjType_GetPropertiesValuesTwoParTest";
	static Map<String, Object> envMap;
	static Environment env;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		utHelper = new UnitTestHelper();
		utHelper.initializeEnvironment();
		env = utHelper.getEnvironment();
		envMap = new HashMap<String, Object>();
		envMap.put("env", env);

	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		utHelper.removeTypeCompletly(testType);
		utHelper.dispose();
	}

	@Before
	public void setUp() throws Exception {

		ObjectType otype = env.createObjectType(testType);

		otype.getPropertyDescriptors().add(new PropertyDescr("name", String.class.getName()));
		otype.getPropertyDescriptors().add(new PropertyDescr("int", Integer.class.getName()));
		otype.getPropertyDescriptors().add(new PropertyDescr("long", Long.class.getName()));
		otype.getPropertyDescriptors().add(new PropertyDescr("double", Double.class.getName()));
		otype.getPropertyDescriptors().add(new PropertyDescr("to", TO.class.getName()));
		env.updateObjectType(otype);
	}

	@After
	public void tearDown() throws Exception {
		env.deleteObjectType(testType);
	}

	@Test
	public void SuccessGetPropertiesValuesTest() throws Exception {
		GetPropertiesValuesCommand command = new GetPropertiesValuesCommand(envMap);
		TO<?> obj1 = env.createObject(testType, new ValueTO[] { new ValueTO("name", "obj1"),
		        new ValueTO("int", new Integer(1)), new ValueTO("double", new Double(3.3)), });
		TO<?> obj2 = env.createObject(testType, new ValueTO[] { new ValueTO("name", "obj2"),
		        new ValueTO("long", new Long(1L)), new ValueTO("to", obj1) });
		List<TO<?>> toList = new ArrayList<TO<?>>();
		toList.add(obj1);
		toList.add(obj2);
		OtpErlangList paramNames = new OtpErlangList(new OtpErlangString[] { new OtpErlangString("name"),
		        new OtpErlangString("int"), new OtpErlangString("to") });
		OtpErlangTuple params2 = new OtpErlangTuple(new OtpErlangObject[] { new OtpErlangString("GetPropertiesValues"),
		        ToConverter.serializeToList(toList), paramNames });
		OtpErlangTuple res = (OtpErlangTuple) command.run(params2);
		assertEquals("ok", res.elementAt(0).toString());

		OtpErlangList otpList = (OtpErlangList) res.elementAt(1);
		assertEquals(2, otpList.arity());

		CollectionTO list1 = CollectionToConverter.parseCollectionTo(env, testType,
		        (OtpErlangTuple) otpList.elementAt(0));
		CollectionTO list2 = CollectionToConverter.parseCollectionTo(env, testType,
		        (OtpErlangTuple) otpList.elementAt(1));

		assertEquals(3, list1.getPropValues().size());
		assertEquals(3, list2.getPropValues().size());
		assertEquals("obj1", list1.getSinglePropValue("name").getValue());
		assertEquals("obj2", list2.getSinglePropValue("name").getValue());
		assertEquals(1, list1.getSinglePropValue("int").getValue());
		assertNull(list2.getSinglePropValue("int").getValue());
		assertNull(list1.getSinglePropValue("to").getValue());
		assertEquals(obj1, list2.getSinglePropValue("to").getValue());
		env.deleteObject(obj1);
		env.deleteObject(obj2);
	}

	@Test
	public void InvalidParametersTest() throws Exception {
		GetPropertiesValuesCommand command = new GetPropertiesValuesCommand(envMap);
		OtpErlangTuple obj = (OtpErlangTuple) command.run(new OtpErlangTuple(
		        new OtpErlangObject[] { new OtpErlangString("createObjectCommand1"), }));
		assertEquals("exception", obj.elementAt(0).toString());
		assertEquals(ExceptionTypes.ILLEGAL_ARGUMENTS.getExceptionType(),
		        ((OtpErlangString) obj.elementAt(1)).stringValue());
		obj = (OtpErlangTuple) command.run(new OtpErlangTuple(new OtpErlangObject[] {
		        new OtpErlangString("createObjectCommand1"), new OtpErlangString(testType) }));
		assertEquals("exception", obj.elementAt(0).toString());
		assertEquals(ExceptionTypes.ILLEGAL_ARGUMENTS.getExceptionType(),
		        ((OtpErlangString) obj.elementAt(1)).stringValue());
	}
}
