package com.synapsense.func.environment;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedList;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.synapsense.dal.generic.entities.NhtInfoDO;
import com.synapsense.dto.ObjectType;
import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.IllegalInputParameterException;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.func.ConsoleApp;
import com.synapsense.service.Environment;
import com.synapsense.service.impl.dao.DataObjectDAO;

public class NHTStatTest {
	private static Environment ENVIRONMENT;

	private static DataObjectDAO<Integer, NhtInfoDO> NHT_INFO_DAO;

	private static TO<Integer> node;

	private static int NODE_LOGICAL_ID;

	private NhtInfoDO createNhtInfo() {
		NhtInfoDO data = new NhtInfoDO();
		data.setNumNeigh(NODE_LOGICAL_ID);
		data.setReportTimestamp(new Date());
		return data;
	}
/*

	private Collection<NhtInfoDO> createBulkNhtInfo(final int size) {
		Collection<NhtInfoDO> nhtData = new LinkedList<NhtInfoDO>();
		for (int i = 0; i < size; i++) {
			NhtInfoDO data1 = createNhtInfo();
			data1.setNids(Integer.toString(i).getBytes());
			nhtData.add(data1);
		}

		return nhtData;
	}

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		ConsoleApp.init();

		ENVIRONMENT = ConsoleApp.env;

		NHT_INFO_DAO = ConsoleApp.NHT_INFO_DAO;

		ObjectType t = ENVIRONMENT.createObjectType("test_type");

		node = (TO<Integer>) ENVIRONMENT.createObject("test_type");

		NODE_LOGICAL_ID = node.getID();
		// creates new net
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		ENVIRONMENT.deleteObjectType("test_type");
	}

	@After
	public void afterTest() {
		NHT_INFO_DAO.removeData(NODE_LOGICAL_ID);
	}

	@Test
	public void testAddData() throws ObjectNotFoundException {
		NhtInfoDO data = createNhtInfo();
		data.setNids("123456789".getBytes());
		int[] hop = { 1, 2, 3, 4, 5 };
		int[] hoptm = { 6, 7, 8, 9, 10 };
		int[] state = { 10, 9, 8, 7, 6 };
		int[] rssi = { 10, 9, 8, 7, 6, 5, 4, 3 };
		data.setHop(hop);
		data.setHoptm(hoptm);
		data.setState(state);
		data.setRssi(rssi);

		NHT_INFO_DAO.addData(node.getID(), data);

		Collection<ValueTO> nhtInfo = NHT_INFO_DAO.getData(node.getID());
		assertNotNull(nhtInfo);
		assertEquals(1, nhtInfo.size());

		ValueTO value = nhtInfo.iterator().next();
		NhtInfoDO gottenData = (NhtInfoDO) value.getValue();
		assertArrayEquals(data.getHop(), gottenData.getHop());
		assertArrayEquals(data.getState(), gottenData.getState());
		assertArrayEquals(data.getHoptm(), gottenData.getHoptm());
		assertArrayEquals(Arrays.copyOf(data.getNids(), 20), gottenData.getNids());
		assertArrayEquals(data.getRssi(), gottenData.getRssi());
	}

	@Test
	public void testNhtArrays() throws ObjectNotFoundException {
		NhtInfoDO data = createNhtInfo();
		data.setNids("123456789".getBytes());
		int[] hop = { -1, 2, 3, 4, 0 };
		int[] hoptm = {};
		int[] state = null;
		data.setHop(hop);
		data.setHoptm(hoptm);
		data.setState(state);
		NHT_INFO_DAO.addData(node.getID(), data);

		Collection<ValueTO> nhtInfo = NHT_INFO_DAO.getData(node.getID());
		assertNotNull(nhtInfo);
		assertEquals(1, nhtInfo.size());

		ValueTO value = nhtInfo.iterator().next();
		NhtInfoDO gottenData = (NhtInfoDO) value.getValue();
		assertArrayEquals(data.getHop(), gottenData.getHop());
		assertArrayEquals(data.getState(), gottenData.getState());
		assertArrayEquals(data.getHoptm(), gottenData.getHoptm());
		assertArrayEquals(Arrays.copyOf(data.getNids(), 20), gottenData.getNids());
	}

	@Test(expected = IllegalInputParameterException.class)
	public void testAddNhtNullObjectID() throws ObjectNotFoundException {
		NhtInfoDO data = createNhtInfo();
		NHT_INFO_DAO.addData(null, data);
	}

	@Test(expected = IllegalInputParameterException.class)
	public void testAddNhtNegativeOrZeroObjectID() throws ObjectNotFoundException {
		NhtInfoDO data = createNhtInfo();
		NHT_INFO_DAO.addData(0, data);
	}

	@Test(expected = IllegalInputParameterException.class)
	public void testAddNhtNullData() throws ObjectNotFoundException {
		NHT_INFO_DAO.addData(node.getID(), null);
	}

	@Test
	public void testAddNhtBulk() throws ObjectNotFoundException {
		Collection<NhtInfoDO> nhtData = createBulkNhtInfo(3);

		NHT_INFO_DAO.addBulk(node.getID(), nhtData);

		Collection<ValueTO> nhtInfo = NHT_INFO_DAO.getData(NODE_LOGICAL_ID);
		assertNotNull(nhtInfo);
		assertFalse(nhtInfo.isEmpty());
		assertEquals(3, nhtInfo.size());
	}

	@Test(expected = IllegalInputParameterException.class)
	public void testAddNhtBulkNullObjectID() throws ObjectNotFoundException {
		Collection<NhtInfoDO> nhtData = new LinkedList<NhtInfoDO>();
		NHT_INFO_DAO.addBulk(null, nhtData);
	}

	@Test(expected = IllegalInputParameterException.class)
	public void testAddNhtBulkNegativeObjectID() throws ObjectNotFoundException {
		Collection<NhtInfoDO> nhtData = new LinkedList<NhtInfoDO>();
		NHT_INFO_DAO.addBulk(0, nhtData);
	}

	@Test(expected = IllegalInputParameterException.class)
	public void testAddBaseDataBulkNullData() throws ObjectNotFoundException {
		NHT_INFO_DAO.addBulk(node.getID(), null);
	}

	@Test
	public void testGetNhtFrameById() throws ObjectNotFoundException {
		Collection<NhtInfoDO> nhtData = createBulkNhtInfo(3);
		NHT_INFO_DAO.addBulk(node.getID(), nhtData);

		Collection<ValueTO> nhtInfo = NHT_INFO_DAO.getData(NODE_LOGICAL_ID, 10, 0);
		assertNotNull(nhtInfo);
		assertFalse(nhtInfo.isEmpty());
		assertEquals(3, nhtInfo.size());

		nhtInfo = NHT_INFO_DAO.getData(NODE_LOGICAL_ID, 1, 0);
		assertNotNull(nhtInfo);
		assertFalse(nhtInfo.isEmpty());
		assertEquals(1, nhtInfo.size());

		nhtInfo = NHT_INFO_DAO.getData(NODE_LOGICAL_ID, 10, 2);
		assertNotNull(nhtInfo);
		assertFalse(nhtInfo.isEmpty());
		assertEquals(1, nhtInfo.size());

		nhtInfo = NHT_INFO_DAO.getData(NODE_LOGICAL_ID, 10, 3);
		assertNotNull(nhtInfo);
		assertTrue(nhtInfo.isEmpty());
	}

	@Test
	public void testGetNhtSizeById() throws ObjectNotFoundException {
		Collection<NhtInfoDO> nhtData = createBulkNhtInfo(3);
		NHT_INFO_DAO.addBulk(node.getID(), nhtData);
		int nhtAmount = NHT_INFO_DAO.getDataSize(NODE_LOGICAL_ID);
		assertEquals(3, nhtAmount);
	}

	@Test
	public void testGetNhtByTimeInterval() throws ObjectNotFoundException {
		Date start = new Date(0L);
		Date end = new Date();

		Collection<NhtInfoDO> nhtData = createBulkNhtInfo(3);
		NHT_INFO_DAO.addBulk(node.getID(), nhtData);

		Collection<ValueTO> nhtInfo = NHT_INFO_DAO.getData(NODE_LOGICAL_ID, start, end);
		assertNotNull(nhtInfo);
		assertFalse(nhtInfo.isEmpty());
		assertEquals(3, nhtInfo.size());
	}

	@Test
	public void testGetNhtFromStartToNumPoints() throws ObjectNotFoundException {
		Date start = new Date(0L);

		Collection<NhtInfoDO> nhtData = createBulkNhtInfo(3);
		NHT_INFO_DAO.addBulk(node.getID(), nhtData);

		Collection<ValueTO> nhtInfo = NHT_INFO_DAO.getData(NODE_LOGICAL_ID, start, 1);
		assertNotNull(nhtInfo);
		assertFalse(nhtInfo.isEmpty());
		assertEquals(1, nhtInfo.size());

		nhtInfo = NHT_INFO_DAO.getData(NODE_LOGICAL_ID, start, 0);
		assertNotNull(nhtInfo);
		assertTrue(nhtInfo.isEmpty());

		nhtInfo = NHT_INFO_DAO.getData(NODE_LOGICAL_ID, start, 2);
		assertNotNull(nhtInfo);
		assertFalse(nhtInfo.isEmpty());
		assertEquals(2, nhtInfo.size());

		nhtInfo = NHT_INFO_DAO.getData(NODE_LOGICAL_ID, start, 10);
		assertNotNull(nhtInfo);
		assertFalse(nhtInfo.isEmpty());
		assertEquals(3, nhtInfo.size());
	}

	@Test
	public void testRemoveBaseData() throws ObjectNotFoundException {
		Collection<NhtInfoDO> nhtData = createBulkNhtInfo(3);
		NHT_INFO_DAO.addBulk(node.getID(), nhtData);

		Collection<ValueTO> nhtInfo = NHT_INFO_DAO.getData(NODE_LOGICAL_ID);
		assertNotNull(nhtInfo);
		assertFalse(nhtInfo.isEmpty());
		assertEquals(3, nhtInfo.size());

		NHT_INFO_DAO.removeData(NODE_LOGICAL_ID);

		nhtInfo = NHT_INFO_DAO.getData(NODE_LOGICAL_ID);
		assertNotNull(nhtInfo);
		assertTrue(nhtInfo.isEmpty());
	}
*/

	public static void main(String[] args) {
		byte[] arr = "123456789".getBytes();
		byte[] newarr = Arrays.copyOf(arr, 20);

		String s = new String(newarr);
		System.out.println(s);
	}
}
