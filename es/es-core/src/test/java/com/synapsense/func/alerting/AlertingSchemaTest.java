package com.synapsense.func.alerting;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

import org.dbunit.JdbcDatabaseTester;
import org.dbunit.dataset.IDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.synapsense.DbConstants;
import com.synapsense.dto.AlertType;
import com.synapsense.dto.MessageTemplate;
import com.synapsense.dto.MessageTemplateRef;
import com.synapsense.dto.MessageType;
import com.synapsense.dto.ObjectType;
import com.synapsense.dto.PropertyDescr;
import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.AlertServiceSystemException;
import com.synapsense.exception.AlertTypeAlreadyExistsException;
import com.synapsense.exception.EnvException;
import com.synapsense.exception.NoSuchAlertTypeException;
import com.synapsense.func.ConsoleApp;
import com.synapsense.service.AlertService;
import com.synapsense.service.Environment;
import com.synapsense.service.UserManagementService;
import com.synapsense.util.CollectionUtils;

public class AlertingSchemaTest {
	private static AlertService ALERTING_SERVICE;
	private static Environment ENVIRONMENT;
	private static UserManagementService USER_SERVICE;

	private static String TEST_TYPE = "TEST_TYPE";

	private static TO<?> TEST_OBJECT_1;
	private static TO<?> TEST_OBJECT_2;

	private static String TEST_ALERT_TYPE = "TEST_ALERT_TYPE";

	private static AlertingTestHelper helper;

	private static JdbcDatabaseTester databaseTester;

	public static void sleep() {
		sleep(1000);
	}

	public static void sleep(long time) {
		try {
			Thread.sleep(time);
		} catch (final InterruptedException e) {
			e.printStackTrace();
		}
	}

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		databaseTester = new JdbcDatabaseTester(DbConstants.DB_DRIVER, DbConstants.DB_CONNECTION, DbConstants.DB_USER,
		        DbConstants.DB_PASSWORD);

		IDataSet databaseDataSet = databaseTester.getConnection().createDataSet(
		        new String[] { DbConstants.TBL_HISTORICAL_VALUES });
		DatabaseOperation.TRUNCATE_TABLE.execute(databaseTester.getConnection(), databaseDataSet);

		ConsoleApp.init();
		ENVIRONMENT = ConsoleApp.env;

		ALERTING_SERVICE = ConsoleApp.ALERTING_SERVICE;

		helper = new AlertingTestHelper(ENVIRONMENT, ALERTING_SERVICE);

		USER_SERVICE = ConsoleApp.USER_SERVICE;

		try {
			ENVIRONMENT.deleteObjectType(TEST_TYPE);
		} finally {
		}

		final ObjectType testType = ENVIRONMENT.createObjectType(TEST_TYPE);
		testType.getPropertyDescriptors().add(new PropertyDescr("name", String.class.getName()));
		testType.getPropertyDescriptors().add(new PropertyDescr("lastValue", Double.class.getName()));
		testType.getPropertyDescriptors().add(new PropertyDescr("location", String.class.getName()));
		testType.getPropertyDescriptors().add(new PropertyDescr("body", String.class.getName()));
		testType.getPropertyDescriptors().add(new PropertyDescr("header", String.class.getName()));
		testType.getPropertyDescriptors().add(new PropertyDescr("messagetype", String.class.getName()));

		ENVIRONMENT.updateObjectType(testType);

		TEST_OBJECT_1 = ENVIRONMENT.createObject(TEST_TYPE, new ValueTO[] { new ValueTO("name", "new object1"),
		        new ValueTO("location", "100 150"), new ValueTO("header", "header"), new ValueTO("body", "body"),
		        new ValueTO("messagetype", "4") });
		TEST_OBJECT_2 = ENVIRONMENT.createObject(TEST_TYPE, new ValueTO[] { new ValueTO("name", "new object2") });
		ENVIRONMENT.setRelation(TEST_OBJECT_1, TEST_OBJECT_2);

		helper.newPersistentAlertType(TEST_ALERT_TYPE, "description", "MAJOR", true, false, TEST_OBJECT_1);
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		ALERTING_SERVICE.deleteAlertType(TEST_ALERT_TYPE);
		ENVIRONMENT.deleteObjectType(TEST_TYPE);

		IDataSet databaseDataSet = databaseTester.getConnection().createDataSet(
		        new String[] { DbConstants.TBL_HISTORICAL_VALUES });
		DatabaseOperation.TRUNCATE_TABLE.execute(databaseTester.getConnection(), databaseDataSet);
	}

	@Test
	public void testGetAllAlertTypes() throws Exception {
		final Collection<AlertType> types = ALERTING_SERVICE.getAllAlertTypes();
		assertFalse(types.isEmpty());

		final AlertType unknown = CollectionUtils.search(types, new CollectionUtils.Predicate<AlertType>() {
			@Override
			public boolean evaluate(final AlertType t) {
				return t.getName().equals("UNKNOWN");
			}
		});

		assertNotNull(unknown);
		assertEquals("MAJOR", unknown.getPriority());
	}

	@Test
	public void testGetAlertTypes() throws Exception {
		final String[] typeNames = new String[] { "UNKNOWN" };
		final Collection<AlertType> types = ALERTING_SERVICE.getAlertTypes(typeNames);
		assertEquals(1, types.size());
	}

	@Test
	public void testCreateDeleteAlertType() throws Exception {
		String[] typeNames = null;
		Collection<AlertType> types = null;
		try {

			final AlertType typeConfig = helper.newPersistentAlertType("NEW_ALERT_TYPE", "MAJOR", true, false);

			typeNames = new String[] { "NEW_ALERT_TYPE" };

			types = ALERTING_SERVICE.getAlertTypes(typeNames);
			final AlertType type = types.iterator().next();
			assertEquals("NEW_ALERT_TYPE", type.getName());
			assertEquals("MAJOR", type.getPriority());
			assertEquals("descr", type.getDescription());
			// assertEquals("header",
			// type.getMessageTemplates().iterator().next().getHeader(ENVIRONMENT));
			// assertEquals("body",
			// type.getMessageTemplates().iterator().next().getBody(ENVIRONMENT));
			assertEquals(true, type.isActive());

			try {
				ALERTING_SERVICE.createAlertType(typeConfig);
				assertTrue(false);
			} catch (final AlertTypeAlreadyExistsException e) {
			}
		} finally {
			ALERTING_SERVICE.deleteAlertType("NEW_ALERT_TYPE");
			typeNames = new String[] { "NEW_ALERT_TYPE" };
			types = ALERTING_SERVICE.getAlertTypes(typeNames);
			assertEquals(0, types.size());
		}
	}

	@Test
	public void testCreateUpdateDeleteAlertType() throws Exception {
		Collection<AlertType> types = null;
		String[] typeNames = null;

		try {
			final AlertType typeConfig = helper.newPersistentAlertType("NEW_ALERT_TYPE", "info", "MAJOR", true, false,
			        TEST_OBJECT_1);

			typeNames = new String[] { "NEW_ALERT_TYPE" };

			types = ALERTING_SERVICE.getAlertTypes(typeNames);

			AlertType type = types.iterator().next();
			assertEquals("NEW_ALERT_TYPE", type.getName());
			assertEquals("MAJOR", type.getPriority());
			assertEquals("info", type.getDescription());
			assertEquals(false, type.isAutoDismiss());
			assertEquals(true, type.isActive());

			try {
				ALERTING_SERVICE.createAlertType(typeConfig);
				assertTrue(false);
			} catch (final AlertTypeAlreadyExistsException e) {
			}

			final AlertType expected = helper.makeCopy("NEW_ALERT_TYPE", "MAJOR", false, true, true);

			ALERTING_SERVICE.updateAlertType(type.getName(), expected);

			types = ALERTING_SERVICE.getAlertTypes(typeNames);

			type = types.iterator().next();
			assertEquals("NEW_ALERT_TYPE", type.getName());
			assertEquals("MAJOR", type.getPriority());
			assertEquals(false, type.isActive());
			assertEquals(true, type.isAutoAcknowledge());
			assertEquals(true, type.isAutoDismiss());

			final AlertType expected2 = helper.makeCopy("NEW_ALERT_TYPE_2", "MINOR", true, true, false);
			ALERTING_SERVICE.updateAlertType(type.getName(), expected2);

			typeNames = new String[] { "NEW_ALERT_TYPE_2" };
			types = ALERTING_SERVICE.getAlertTypes(typeNames);
			type = types.iterator().next();
			assertEquals("NEW_ALERT_TYPE_2", type.getName());
			assertEquals("MINOR", type.getPriority());
			assertEquals(true, type.isActive());
			assertEquals(true, type.isAutoDismiss());
			assertEquals(false, type.isAutoAcknowledge());

		} finally {
			ALERTING_SERVICE.deleteAlertType("NEW_ALERT_TYPE");
			ALERTING_SERVICE.deleteAlertType("NEW_ALERT_TYPE_2");
			typeNames = new String[] { "NEW_ALERT_TYPE", "NEW_ALERT_TYPE_2" };
			types = ALERTING_SERVICE.getAlertTypes(typeNames);
			assertEquals(0, types.size());
		}
	}

	@Test
	public void testCreateAlertTypeWithTemplates() throws Exception {
		final TO<?> templateHolderEmail = ENVIRONMENT.createObject("ALERT_MESSAGE_TEMPLATE", new ValueTO[] {
		        new ValueTO("name", "email"), new ValueTO("messagetype", "1"), new ValueTO("header", "header"),
		        new ValueTO("body", "body") });

		final TO<?> templateHolderSms = ENVIRONMENT.createObject("ALERT_MESSAGE_TEMPLATE", new ValueTO[] {
		        new ValueTO("name", "sms"), new ValueTO("messagetype", "2"), new ValueTO("header", "header"),
		        new ValueTO("body", "body") });

		try {
			final Set<MessageTemplate> templates = CollectionUtils.newSet();
			templates.add(new MessageTemplateRef(MessageType.SMTP, templateHolderEmail));

			final AlertType typeConfig = new AlertType("NEW_ALERT_TYPE", "NEW_ALERT_TYPE", "", "MAJOR", "header",
			        "body", templates, true, false, false, false);

			ALERTING_SERVICE.createAlertType(typeConfig);

			Collection<AlertType> actualTypes = ALERTING_SERVICE.getAlertTypes(new String[] { "NEW_ALERT_TYPE" });

			AlertType actualType = actualTypes.iterator().next();
			assertFalse(actualType.getMessageTemplates().isEmpty());

			final MessageTemplate template = actualType.getMessageTemplates().iterator().next();
			assertEquals(templateHolderEmail, template.getRef());
			assertEquals(MessageType.SMTP, template.getMessageType());
			assertEquals("header", template.getHeader(ENVIRONMENT));
			assertEquals("body", template.getBody(ENVIRONMENT));

			final MessageTemplate newTemplate = new MessageTemplateRef(MessageType.SMS, templateHolderSms);
			ALERTING_SERVICE.setTemplate("NEW_ALERT_TYPE", newTemplate);

			actualTypes = ALERTING_SERVICE.getAlertTypes(new String[] { "NEW_ALERT_TYPE" });
			actualType = actualTypes.iterator().next();
			assertEquals(2, actualType.getMessageTemplates().size());

			assertTrue(actualType.getMessageTemplates().contains(newTemplate));
			assertTrue(actualType.getMessageTemplates().containsAll(templates));

			ALERTING_SERVICE.removeTemplate("NEW_ALERT_TYPE", MessageType.SMS);

			actualTypes = ALERTING_SERVICE.getAlertTypes(new String[] { "NEW_ALERT_TYPE" });
			actualType = actualTypes.iterator().next();
			assertEquals(1, actualType.getMessageTemplates().size());
			assertTrue(actualType.getMessageTemplates().containsAll(templates));

		} finally {
			ALERTING_SERVICE.deleteAlertType("NEW_ALERT_TYPE");
			ENVIRONMENT.deleteObject(templateHolderEmail);
			ENVIRONMENT.deleteObject(templateHolderSms);
		}

	}

	@Test
	public void testPerformersDestinations() throws Exception {
		final String NEW_ALERT_TYPE = "NEW_ALERT_TYPE";
		Collection<AlertType> types = null;
		String[] typeNames = null;
		try {
			helper.newPersistentAlertType(NEW_ALERT_TYPE, "MAJOR", true, false);

			typeNames = new String[] { NEW_ALERT_TYPE };

			types = ALERTING_SERVICE.getAlertTypes(typeNames);
			assertEquals(1, types.size());

			ALERTING_SERVICE.addActionDestination(NEW_ALERT_TYPE, "TIER1", MessageType.SMTP, "1@mera.ru");
			ALERTING_SERVICE.addActionDestination(NEW_ALERT_TYPE, "TIER1", MessageType.SMTP, "2@mera.ru");

			Collection<String> destinations = ALERTING_SERVICE.getActionDestinations(NEW_ALERT_TYPE, "TIER1",
			        MessageType.SMTP);
			assertEquals(2, destinations.size());
			assertTrue(destinations.contains("1@mera.ru"));
			assertTrue(destinations.contains("2@mera.ru"));

			// existing destination add-on must not create new destination rec
			ALERTING_SERVICE.addActionDestination(NEW_ALERT_TYPE, "TIER1", MessageType.SMTP, "1@mera.ru");

			destinations = ALERTING_SERVICE.getActionDestinations(NEW_ALERT_TYPE, "TIER1", MessageType.SMTP);
			assertEquals(2, destinations.size());
			assertTrue(destinations.contains("1@mera.ru"));
			assertTrue(destinations.contains("2@mera.ru"));

			ALERTING_SERVICE.removeActionDestination(NEW_ALERT_TYPE, "TIER1", MessageType.SMTP, "1@mera.ru");

			destinations = ALERTING_SERVICE.getActionDestinations(NEW_ALERT_TYPE, "TIER1", MessageType.SMTP);
			assertEquals(1, destinations.size());
			assertTrue(destinations.contains("2@mera.ru"));

		} finally {
			ALERTING_SERVICE.deleteAlertType(NEW_ALERT_TYPE);
		}
	}

	@Test
	public void testDifferentKindOfDestinations() throws Exception {
		final String NEW_ALERT_TYPE = "NEW_ALERT_TYPE";
		TO<?> user = null;
		try {
			// TODO fix issue with tiers
			user = USER_SERVICE.createUser("tester", "temp", null);
			Map<String, Object> userProps = CollectionUtils.newMap();
			userProps.put(USER_SERVICE.USER_PRIMARY_EMAIL, "admin@admin.com");
			userProps.put(USER_SERVICE.USER_SMS_ADDRESS, "123456789@admin.com");
			USER_SERVICE.updateUser(user, userProps);

			helper.newPersistentAlertType(NEW_ALERT_TYPE, "MAJOR", true, false);

			ALERTING_SERVICE.addActionDestination(NEW_ALERT_TYPE, "TIER1", MessageType.SMTP, "tester");
			ALERTING_SERVICE.addActionDestination(NEW_ALERT_TYPE, "TIER1", MessageType.SMTP, "shabanov@mera.ru");

			ALERTING_SERVICE.addActionDestination(NEW_ALERT_TYPE, "TIER2", MessageType.SMS, "tester");

			ALERTING_SERVICE.addActionDestination(NEW_ALERT_TYPE, "TIER3", MessageType.SMTP, "tester");
			ALERTING_SERVICE.addActionDestination(NEW_ALERT_TYPE, "TIER3", MessageType.SMS, "tester");

			final Collection<String> dest = ALERTING_SERVICE.getActionDestinations(NEW_ALERT_TYPE, "TIER1",
			        MessageType.SMTP);
			assertEquals(2, dest.size());
			assertTrue(dest.contains("tester"));
			assertTrue(dest.contains("shabanov@mera.ru"));

			String userDest = ALERTING_SERVICE
			        .getActionDestination(NEW_ALERT_TYPE, "TIER1", MessageType.SMTP, "tester");
			assertNotNull(userDest);
			assertEquals("admin@admin.com", userDest);

			userDest = ALERTING_SERVICE.getActionDestination(NEW_ALERT_TYPE, "TIER1", MessageType.SMTP, "admin");
			assertNull(userDest);

			userDest = ALERTING_SERVICE.getActionDestination(NEW_ALERT_TYPE, "TIER2", MessageType.SMS, "tester");
			assertNotNull(userDest);
			assertEquals("123456789@admin.com", userDest);

			userDest = ALERTING_SERVICE.getActionDestination(NEW_ALERT_TYPE, "TIER3", MessageType.SMS, "tester");
			assertNotNull(userDest);
			assertEquals("123456789@admin.com", userDest);

			userDest = ALERTING_SERVICE.getActionDestination(NEW_ALERT_TYPE, "TIER3", MessageType.SMTP, "tester");
			assertNotNull(userDest);
			assertEquals("admin@admin.com", userDest);

			userDest = ALERTING_SERVICE.getActionDestination(NEW_ALERT_TYPE, "_TIER", MessageType.SMS, "tester");
			assertNull(userDest);

			userDest = ALERTING_SERVICE.getActionDestination("NEW_ALERT_TYPE", "TIER1", MessageType.SMS, "tester");
			assertNull(userDest);

			userDest = ALERTING_SERVICE.getActionDestination(NEW_ALERT_TYPE, "TIER1", MessageType.SNMP, "tester");
			assertNull(userDest);

			userDest = ALERTING_SERVICE.getActionDestination(NEW_ALERT_TYPE, "TIER1", MessageType.SNMP, "no_user");
			assertNull(userDest);

		} finally {
			ALERTING_SERVICE.deleteAlertType(NEW_ALERT_TYPE);
			USER_SERVICE.deleteUser(user);
		}
	}

	@Test
	public void shouldPreventTemplateDuplicates() throws EnvException, AlertTypeAlreadyExistsException,
	        NoSuchAlertTypeException {

		final TO<?> templateHolderEmail1 = ENVIRONMENT.createObject("ALERT_MESSAGE_TEMPLATE", new ValueTO[] {
		        new ValueTO("name", "email1"), new ValueTO("messagetype", "1"), new ValueTO("header", "header1"),
		        new ValueTO("body", "body1") });
		final TO<?> templateHolderEmail2 = ENVIRONMENT.createObject("ALERT_MESSAGE_TEMPLATE", new ValueTO[] {
		        new ValueTO("name", "email2"), new ValueTO("messagetype", "1"), new ValueTO("header", "header2"),
		        new ValueTO("body", "body2") });

		final AlertType typeConfig = new AlertType("NEW_ALERT_TYPE", "NEW_ALERT_TYPE", "", "MAJOR", false, false, false);

		try {
			ALERTING_SERVICE.createAlertType(typeConfig);

			Collection<AlertType> actualTypes = ALERTING_SERVICE.getAlertTypes(new String[] { "NEW_ALERT_TYPE" });

			AlertType actualType = actualTypes.iterator().next();
			assertTrue(actualType.getMessageTemplates().isEmpty());

			MessageTemplate newTemplate = new MessageTemplateRef(MessageType.SMTP, templateHolderEmail1);
			ALERTING_SERVICE.setTemplate("NEW_ALERT_TYPE", newTemplate);

			actualTypes = ALERTING_SERVICE.getAlertTypes(new String[] { "NEW_ALERT_TYPE" });
			actualType = actualTypes.iterator().next();

			assertEquals(1, actualType.getMessageTemplates().size());
			MessageTemplate template = actualType.getMessageTemplates().iterator().next();
			assertEquals(templateHolderEmail1, template.getRef());
			assertEquals(MessageType.SMTP, template.getMessageType());
			assertEquals("header1", template.getHeader(ENVIRONMENT));

			newTemplate = new MessageTemplateRef(MessageType.SMTP, templateHolderEmail2);
			ALERTING_SERVICE.setTemplate("NEW_ALERT_TYPE", newTemplate);

			actualTypes = ALERTING_SERVICE.getAlertTypes(new String[] { "NEW_ALERT_TYPE" });
			actualType = actualTypes.iterator().next();

			assertEquals(1, actualType.getMessageTemplates().size());
			template = actualType.getMessageTemplates().iterator().next();
			assertEquals(templateHolderEmail2, template.getRef());
			assertEquals(MessageType.SMTP, template.getMessageType());
			assertEquals("header2", template.getHeader(ENVIRONMENT));
		} finally {
			ALERTING_SERVICE.deleteAlertType("NEW_ALERT_TYPE");
			ENVIRONMENT.deleteObject(templateHolderEmail1);
			ENVIRONMENT.deleteObject(templateHolderEmail2);
		}
	}

	@Test(expected = AlertServiceSystemException.class)
	public void testSetSnmpDestinationForUserFailure() throws Exception {
		final String NEW_ALERT_TYPE = "NEW_ALERT_TYPE";
		TO<?> u = null;
		try {
			helper.newPersistentAlertType(NEW_ALERT_TYPE, "MAJOR", true, false);
			u = USER_SERVICE.createUser("tester", "temp", null);
			ALERTING_SERVICE.addActionDestination(NEW_ALERT_TYPE, "TIER1", MessageType.SNMP, "tester");
		} finally {
			ALERTING_SERVICE.deleteAlertType(NEW_ALERT_TYPE);
			USER_SERVICE.deleteUser(u);
		}
	}

}
