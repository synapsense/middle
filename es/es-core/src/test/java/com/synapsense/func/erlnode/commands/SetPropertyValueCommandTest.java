package com.synapsense.func.erlnode.commands;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.ericsson.otp.erlang.OtpErlangAtom;
import com.ericsson.otp.erlang.OtpErlangDouble;
import com.ericsson.otp.erlang.OtpErlangInt;
import com.ericsson.otp.erlang.OtpErlangLong;
import com.ericsson.otp.erlang.OtpErlangObject;
import com.ericsson.otp.erlang.OtpErlangString;
import com.ericsson.otp.erlang.OtpErlangTuple;
import com.synapsense.dto.ObjectType;
import com.synapsense.dto.PropertyDescr;
import com.synapsense.dto.TO;
import com.synapsense.impl.erlnode.ExceptionTypes;
import com.synapsense.impl.erlnode.commands.SetPropertyValueCommand;
import com.synapsense.impl.erlnode.utils.ToConverter;
import com.synapsense.service.Environment;

public class SetPropertyValueCommandTest {

	static UnitTestHelper utHelper = null;
	static String testType = "testingObjType_GetAllPropertiesValuesTwoParTest";
	static Map<String, Object> envMap;
	static Environment env;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		utHelper = new UnitTestHelper();
		utHelper.initializeEnvironment();
		env = utHelper.getEnvironment();
		envMap = new HashMap<String, Object>();
		envMap.put("env", env);

	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		utHelper.removeTypeCompletly(testType);
		utHelper.dispose();
	}

	@Before
	public void setUp() throws Exception {
		env.deleteObjectType(testType);
		ObjectType otype = env.createObjectType(testType);

		otype.getPropertyDescriptors().add(new PropertyDescr("name", String.class.getName()));
		otype.getPropertyDescriptors().add(new PropertyDescr("int", Integer.class.getName()));
		otype.getPropertyDescriptors().add(new PropertyDescr("long", Long.class.getName()));
		otype.getPropertyDescriptors().add(new PropertyDescr("double", Double.class.getName()));
		otype.getPropertyDescriptors().add(new PropertyDescr("to", TO.class.getName()));
		env.updateObjectType(otype);
	}

	@After
	public void tearDown() throws Exception {
		env.deleteObjectType(testType);
	}

	@Test
	public void SuccessSetPropertyValueTest() throws Exception {
		SetPropertyValueCommand command = new SetPropertyValueCommand(envMap);
		TO<?> obj1 = env.createObject(testType);

		OtpErlangAtom res = (OtpErlangAtom) command.run(new OtpErlangTuple(new OtpErlangObject[] {
		        new OtpErlangString("SetPropertyValue"), ToConverter.serializeTo(obj1), new OtpErlangString("name"),
		        new OtpErlangString("obj1") }));

		assertEquals("ok", res.toString());
		assertEquals("obj1", env.getPropertyValue(obj1, "name", String.class));

		res = (OtpErlangAtom) command.run(new OtpErlangTuple(new OtpErlangObject[] {
		        new OtpErlangString("SetPropertyValue"), ToConverter.serializeTo(obj1), new OtpErlangString("int"),
		        new OtpErlangInt(1) }));

		assertEquals("ok", res.toString());
		assertEquals(new Integer(1), env.getPropertyValue(obj1, "int", Integer.class));

		res = (OtpErlangAtom) command.run(new OtpErlangTuple(new OtpErlangObject[] {
		        new OtpErlangString("SetPropertyValue"), ToConverter.serializeTo(obj1), new OtpErlangString("long"),
		        new OtpErlangLong(2L) }));

		assertEquals("ok", res.toString());
		assertEquals(new Long(2), env.getPropertyValue(obj1, "long", Long.class));

		res = (OtpErlangAtom) command.run(new OtpErlangTuple(new OtpErlangObject[] {
		        new OtpErlangString("SetPropertyValue"), ToConverter.serializeTo(obj1), new OtpErlangString("double"),
		        new OtpErlangDouble(3.3) }));

		assertEquals("ok", res.toString());
		assertEquals(new Double(3.3), env.getPropertyValue(obj1, "double", Double.class));

		TO<?> obj2 = env.createObject(testType);

		res = (OtpErlangAtom) command.run(new OtpErlangTuple(new OtpErlangObject[] {
		        new OtpErlangString("SetPropertyValue"), ToConverter.serializeTo(obj1), new OtpErlangString("to"),
		        ToConverter.serializeTo(obj2) }));

		assertEquals("ok", res.toString());
		assertEquals(obj2, env.getPropertyValue(obj1, "to", TO.class));

		env.deleteObject(obj1);
		env.deleteObject(obj2);
	}

	@Test
	public void InvalidParametersTest() throws Exception {
		TO<?> obj1 = env.createObject(testType);
		SetPropertyValueCommand command = new SetPropertyValueCommand(envMap);
		OtpErlangTuple obj = (OtpErlangTuple) command.run(new OtpErlangTuple(
		        new OtpErlangObject[] { new OtpErlangString("createObjectCommand1") }));
		assertEquals("exception", obj.elementAt(0).toString());
		assertEquals(ExceptionTypes.ILLEGAL_ARGUMENTS.getExceptionType(),
		        ((OtpErlangString) obj.elementAt(1)).stringValue());
		obj = (OtpErlangTuple) command.run(new OtpErlangTuple(new OtpErlangObject[] {
		        new OtpErlangString("createObjectCommand1"), ToConverter.serializeTo(obj1) }));
		assertEquals("exception", obj.elementAt(0).toString());
		assertEquals(ExceptionTypes.ILLEGAL_ARGUMENTS.getExceptionType(),
		        ((OtpErlangString) obj.elementAt(1)).stringValue());
	}

}
