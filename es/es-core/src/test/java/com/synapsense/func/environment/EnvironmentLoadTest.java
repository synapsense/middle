package com.synapsense.func.environment;

import java.util.LinkedList;
import java.util.List;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.synapsense.dto.ObjectType;
import com.synapsense.dto.TO;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.func.ConsoleApp;
import com.synapsense.service.Environment;
import com.synapsense.util.JNDINames;

public class EnvironmentLoadTest {

	private static Environment env;

	private static ObjectType type;

	private static TO<?> dc;
	private static List<TO<?>> rooms;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		ConsoleApp.init();

		env = ConsoleApp.env;

		type = env.createObjectType("test type");
		dc = env.createObject(type.getName());
		rooms = new LinkedList<TO<?>>();
		for (int i = 0; i < 5; i++) {
			TO room = env.createObject(type.getName());
			rooms.add(room);
			env.setRelation(dc, room);
			for (int j = 0; j < 20; j++) {
				TO rack = env.createObject(type.getName());
				env.setRelation(room, rack);
				for (int k = 0; k < 10; k++) {
					TO sensor = env.createObject(type.getName());
					env.setRelation(rack, sensor);
				}
			}

		}

	}

	@AfterClass
	public static void teardownAfterClass() throws Exception {
		env.deleteObjectType(type.getName());
	}

	@Test(timeout = 100)
	public void testGetChildrenDc() throws ObjectNotFoundException {
		long start = System.currentTimeMillis();
		env.getChildren(dc);
		System.err.println(":" + (System.currentTimeMillis() - start));

	}

	@Test(timeout = 100)
	public void testGetChildrenRoom() throws ObjectNotFoundException {
		long start = System.currentTimeMillis();
		env.getChildren(rooms.iterator().next());
		System.err.println(":" + (System.currentTimeMillis() - start));
	}
}
