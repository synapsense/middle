package com.synapsense.func.erlnode.commands;

import java.util.HashMap;
import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.jboss.ejb.client.ContextSelector;
import org.jboss.ejb.client.EJBClientConfiguration;
import org.jboss.ejb.client.EJBClientContext;
import org.jboss.ejb.client.PropertiesBasedEJBClientConfiguration;
import org.jboss.ejb.client.remoting.ConfigBasedEJBClientContextSelector;

import com.synapsense.service.Environment;
import com.synapsense.util.ServerLoginModule;

public class UnitTestESConnector {
	private InitialContext context = null;

	private Environment environment = null;
	private String SERVER_NAME = "";
	private HashMap<Class<?>, String> mappings = new HashMap<Class<?>, String>();

	/**
	 * Creates connection to ES with context for credentials provided
	 * 
	 * @param username
	 * @param password
	 */
	public UnitTestESConnector(String username, String password) {

		SERVER_NAME = "denton";

		mappings.put(Environment.class, "es-core%sEnvironment");

		initContext(username, password);
	}

	/**
	 * Creates connection with context for admin/admin credentials
	 */
	public UnitTestESConnector() {
		this("admin", "admin");
	}

	/**
	 * Method to get context. Each time it's called old context will be
	 * disposed.<br>
	 * Also all links to ES services will be set to <code>null<code/>
	 */
	private void initContext(String username, String password) {
		if (context != null)
			dispose();

		Properties clientProps = new Properties();
		{
			clientProps.put("remote.connections", "default");
			clientProps.put("remote.connection.default.host", SERVER_NAME);
			clientProps.put("remote.connection.default.port", "4447");
			clientProps.put("remote.connection.default.username", username);
			clientProps.put("remote.connection.default.password", password);

			clientProps.put("remote.connectionprovider.create.options.org.xnio.Options.SSL_ENABLED", "false");
			clientProps.put("remote.connection.default.connect.options.org.xnio.Options.SASL_POLICY_NOANONYMOUS",
			        "false");
			clientProps.put("remote.connection.default.connect.options.org.xnio.Options.SASL_DISALLOWED_MECHANISMS",
			        "JBOSS-LOCAL-USER");
			clientProps.put("remote.connection.default.connect.options.org.xnio.Options.SASL_POLICY_NOPLAINTEXT",
			        "false");
		}
		EJBClientConfiguration clientConf = new PropertiesBasedEJBClientConfiguration(clientProps);
		ContextSelector<EJBClientContext> selector = new ConfigBasedEJBClientContextSelector(clientConf);
		EJBClientContext.setSelector(selector);

		// connection properties
		Properties jndiProps = new Properties();
		jndiProps.put(Context.URL_PKG_PREFIXES, "org.jboss.ejb.client.naming");
		jndiProps.put(Context.SECURITY_PRINCIPAL, username);
		jndiProps.put(Context.SECURITY_CREDENTIALS, password);

		try {
			context = new InitialContext(jndiProps);
		} catch (NamingException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Return name of user logged in with current context. It returns null if
	 * some problems with naming appears
	 * 
	 * @return
	 */
	public String getCurrentUsername() {
		String username = null;
		try {
			username = (String) context.getEnvironment().get(Context.SECURITY_PRINCIPAL);
		} catch (NamingException e) {
			// this should never happen, of course
		}
		return username;
	}

	/**
	 * Purges old context and creates a new one with credentials provided.<br/>
	 * Method requires since more than one context does not work in one thread.
	 * 
	 * @param username
	 * @param password
	 */
	public void relogin(String username, String password) {
		dispose();
		initContext(username, password);
	}

	/**
	 * Cleans up service objects and closes context
	 */
	public void dispose() {
		environment = null;

		try {
			context.close();
			context = null;
		} catch (NamingException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Method to get service for requested interface
	 */
	private <INTERFACE> INTERFACE getService(Class<INTERFACE> clazz) {
		INTERFACE service = null;

		// building JNDI name from mapped template
		String jndiName = String.format("ejb:es-ear/%s/!%s", mappings.get(clazz), clazz.getName());

		try {
			if (context == null)
				initContext("admin", "admin");
			service = ServerLoginModule.getProxy(context, jndiName, clazz);
		} catch (NamingException e) {
			e.printStackTrace();
		}

		return service;
	}

	public InitialContext getContext() {
		return context;
	}

	public Environment getEnvironment() {
		environment = getService(Environment.class);
		return environment;
	}

}