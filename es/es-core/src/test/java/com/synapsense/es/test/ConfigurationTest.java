package com.synapsense.es.test;

import static org.junit.Assert.assertEquals;

import java.security.Security;
import java.util.Arrays;
import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;

import org.jboss.sasl.JBossSaslProvider;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.synapsense.service.Environment;

//https://docs.jboss.org/author/display/AS71/EJB+invocations+from+a+remote+client+using+JNDI
public class ConfigurationTest {
	
	private static Environment env;
	
	static {
        Security.addProvider(new JBossSaslProvider());
    }

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		Properties environment = new Properties();
		environment.put(Context.URL_PKG_PREFIXES, "org.jboss.ejb.client.naming");
		//other properties are now stored in jboss-ejb-client.properties

		InitialContext ctx = new InitialContext(environment);
		env = (Environment) ctx.lookup("ejb:es-ear/es-core-0.0.1-SNAPSHOT//Environment!com.synapsense.service.Environment");
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() throws Exception {
		System.out.println(Arrays.toString(env.getObjectTypes()));
	}

}
