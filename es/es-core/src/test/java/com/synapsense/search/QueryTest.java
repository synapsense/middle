package com.synapsense.search;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.jmock.Expectations;
import org.junit.Ignore;
import org.junit.Test;

import com.synapsense.AbstractTest;
import com.synapsense.util.CollectionUtils;

public class QueryTest extends AbstractTest {

	final Element element = mock(Element.class);
	final Traversable traversable = mock(Traversable.class);

	@Test
	public void createQuerySuccess() {
		final QueryDescription qd = mock(QueryDescription.class);
		final Map m = mock(Map.class);
		checking(new Expectations() {
			{
				one(qd).getDepth();
				will(returnValue(1));

				one(qd).getPaths();
				will(returnValue(m));

				one(qd).isAddStartElementToResult();
				will(returnValue(false));

			}
		});
		final Query query = new QueryImpl(qd);

		assertNotNull(query);
	}

	@Test
	public void executeOneLevelOnePathQueryNoLinksSuccess() {
		final QueryDescription qd = new QueryDescription(1);
		final Relation r = mock(Relation.class);

		final Path path = new Path(r, Direction.OUTGOING);
		qd.addPath(1, path);

		checking(new Expectations() {
			{
				one(traversable).traverseNext(element, path);
				will(returnValue(Collections.emptyList()));
			}
		});

		final Query query = new QueryImpl(qd);
		Results result = query.execute(element, traversable);
		assertEquals(0, result.size());
	}

	@Test
	public void executeQueryTwoLevelDepthButOnlyFirstLevelPathIsSpecifiedSuccess() {
		final QueryDescription qd = new QueryDescription(2);
		final Relation r = mock(Relation.class);
		final Path path = new Path(r, Direction.OUTGOING, true);
		qd.addPath(1, path);

		final Element secondLevelElement = mock(Element.class, "sle");
		final List<Element> traverseList = CollectionUtils.newList();
		traverseList.add(secondLevelElement);

		checking(new Expectations() {
			{
				one(traversable).traverseNext(element, path);
				will(returnValue(traverseList));
			}
		});

		final Query query = new QueryImpl(qd);
		Results result = query.execute(element, traversable);
		assertEquals(1, result.size());
	}

	@Test
	/**
	 * real use case of give all alerts of given priority 
	 * alert.type->alertype.priority->alertpriority
	 */
	public void executeQueryTwoLevelDepthTwoLevelPathIsSpecifiedSuccess() {
		final QueryDescription qd = new QueryDescription(2);

		final Relation rTypeToPriority = mock(Relation.class, "priority");
		final Relation rAlertToType = mock(Relation.class, "type");

		final Path path1 = new Path(rTypeToPriority, Direction.OUTGOING, false);
		qd.addPath(1, path1);

		final Path path2 = new Path(rAlertToType, Direction.OUTGOING, true);
		qd.addPath(2, path2);

		final Element alertType = mock(Element.class, "alertType");
		final List<Element> alertTypes = CollectionUtils.newList();
		alertTypes.add(alertType);

		final Element alert1 = mock(Element.class, "alert1");
		final Element alert2 = mock(Element.class, "alert2");

		final List<Element> alerts = CollectionUtils.newList();
		alerts.add(alert1);
		alerts.add(alert2);

		checking(new Expectations() {
			{
				one(traversable).traverseNext(element, path1);
				will(returnValue(alertTypes));

				one(traversable).traverseNext(alertType, path2);
				will(returnValue(alerts));
			}
		});

		final Query query = new QueryImpl(qd);
		Results result = query.execute(element, traversable);
		assertEquals(2, result.size());

	}

	@Test
	public void executeQueryIncludeStartElementToResultSuccess() {
		final QueryDescription qd = new QueryDescription(2);

		qd.addStartElementToResult(true);

		final Relation rTypeToPriority = mock(Relation.class, "priority");

		final Path path1 = new Path(rTypeToPriority, Direction.OUTGOING, true);
		qd.addPath(1, path1);

		final Element alert1 = mock(Element.class, "alert1");
		final Element alert2 = mock(Element.class, "alert2");

		final List<Element> alerts = CollectionUtils.newList();
		alerts.add(alert1);
		alerts.add(alert2);

		checking(new Expectations() {
			{
				one(traversable).traverseNext(element, path1);

				will(returnValue(alerts));
			}
		});

		final Query query = new QueryImpl(qd);
		Results result = query.execute(element, traversable);
		assertEquals(3, result.size());

	}

	@Test(expected=IllegalArgumentException.class)
	@Ignore
	public void pathDoesNotSupportIncomingDirection() {
		final Relation r = mock(Relation.class);
		final Path path = new Path(r, Direction.INCOMING);
	}
	
	@Test
	public void executeOneLevelAnyPathQuerySuccess() {
		final QueryDescription qd = new QueryDescription(1);
		final Relation r = mock(Relation.class);

		final Path path = new Path(r, Direction.OUTGOING, true);
		qd.addPath(1, path);

		checking(new Expectations() {
			{
				one(traversable).traverseNext(element, path);
				will(returnValue(Collections.emptyList()));
			}
		});

		final Query query = new QueryImpl(qd);
		Results result = query.execute(element, traversable);
		assertEquals(0, result.size());
	}
	
	@Test
	@Ignore
	public void queryDescriptionBuilderUsability() {
		final QueryD qd = new QueryD();
		Predicate p = null;
		Element e = null;
		qd.startFrom(e).moveTo("cTop").prune(p).then().moveBack("sensorType").prune(p).end();
	}

	private class QueryD {

		// changing level , depth
		public QueryD then() {
			return null;
		}

		public QueryD moveTo(String string) {
			return null;
		}

		public QueryD moveBack(String string) {
			return null;
		}

		// returns immutable queryD
		public QueryD end() {
			return null;
		}

		public QueryD prune(Predicate expression) {
			return null;
		}

		public QueryD startFrom(Element e) {
			return null;
		}
	}

}
