package com.synapsense;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.synapsense.func.activitylog.ActivityLogTest;
import com.synapsense.func.activitylog.ActivityRecordFilterTest;
import com.synapsense.func.alerting.AlertingSuite;
import com.synapsense.func.environment.EnvironmentCreateObjectTest;
import com.synapsense.func.environment.EnvironmentEmptyTOTest;
import com.synapsense.func.environment.EnvironmentIncrementTest;
import com.synapsense.func.environment.EnvironmentRelationsTest;
import com.synapsense.func.environment.FileWriterSecurityTest;
import com.synapsense.func.environment.FileWriterTest;
import com.synapsense.func.environment.HistoricalDataDbTest;
import com.synapsense.func.environment.NHTStatTest;
import com.synapsense.func.environment.NetStatTest;
import com.synapsense.func.rulesengine.CRESuite;
import com.synapsense.func.userservice.UserServiceTest;
import com.synapsense.impl.nsvc.NotificationServiceImplTest;
import com.synapsense.impl.nsvc.ReferencedObjectChangedEventCompressorTest;
import com.synapsense.search.QueryTest;

@RunWith(Suite.class)
@SuiteClasses({ UserServiceTest.class, EnvironmentIncrementTest.class, EnvironmentRelationsTest.class,
        EnvironmentCreateObjectTest.class, HistoricalDataDbTest.class, EnvironmentEmptyTOTest.class, QueryTest.class,
        AlertingSuite.class, CRESuite.class, NetStatTest.class, NHTStatTest.class, ActivityLogTest.class,
        ActivityRecordFilterTest.class, FileWriterTest.class, FileWriterSecurityTest.class,
        NotificationServiceImplTest.class, ReferencedObjectChangedEventCompressorTest.class })
public class ServerSuite {
}
