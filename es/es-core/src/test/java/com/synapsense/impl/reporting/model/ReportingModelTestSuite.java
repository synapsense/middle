package com.synapsense.impl.reporting.model;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

// ESCA-JAVA0024: Just a suite class
@RunWith(Suite.class)
@SuiteClasses({ ReportTemplateTest.class })
public class ReportingModelTestSuite {
}
