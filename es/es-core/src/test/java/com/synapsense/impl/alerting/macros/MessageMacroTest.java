package com.synapsense.impl.alerting.macros;

import static org.junit.Assert.assertEquals;
import mockit.Expectations;

import mockit.Mocked;
import mockit.integration.junit4.JMockit;
import org.junit.Test;

import com.synapsense.dto.Alert;
import com.synapsense.impl.alerting.macros.MessageMacro;
import com.synapsense.impl.alerting.macros.MessageNumMacro;
import com.synapsense.impl.alerting.macros.MessageTierMacro;
import com.synapsense.service.Environment;
import com.synapsense.service.LocalizationService;
import com.synapsense.service.user.UserContext;
import com.synapsense.util.unitconverter.UnitSystems;

import org.junit.runner.RunWith;

@RunWith(JMockit.class)
public class MessageMacroTest {

	@Test
	public void testDataInMessageExpansion(@Mocked Environment env, @Mocked Alert alert, @Mocked UserContext uctx, @Mocked UnitSystems unitsys, @Mocked LocalizationService ls) {
		final String message = "RL1-rack1 intake temp is Out of allowable range. Data: []";

		MacroContext ctx = new MacroContext();
		ctx.props.put(MessageTierMacro.MSG_TIER, null);
		ctx.props.put(MessageNumMacro.MSG_NUM, null);
		ctx.props.put(AlertDescrMacro.ALERT_DESCRIPTION, null);
		ctx.env = env;
		ctx.alert = alert;
		ctx.usrCtx = uctx;
		ctx.unitSystems = unitsys;
		ctx.localizationService = ls;

		new Expectations() {
			{
				alert.getMessage();
				returns(message);
			}
		};

		Macro macro = new MessageMacro();
		String output = macro.expandIn("$message$", ctx);
		assertEquals(
		        "RL1-rack1 intake temp is Out of allowable range. Data: []",
		        output);

	}

	@Test
	public void testNegativeDataInMessageExpansion(@Mocked Environment env, @Mocked Alert alert, @Mocked UserContext uctx, @Mocked UnitSystems unitsys, @Mocked LocalizationService ls) {
		final String message = "RL1-rack1 intake temp is Out of allowable range. Data: []";

		MacroContext ctx = new MacroContext();
		ctx.props.put(MessageTierMacro.MSG_TIER, null);
		ctx.props.put(MessageNumMacro.MSG_NUM, null);
		ctx.props.put(AlertDescrMacro.ALERT_DESCRIPTION, null);
		ctx.env = env;
		ctx.alert = alert;
		ctx.usrCtx = uctx;
		ctx.unitSystems = unitsys;
		ctx.localizationService = ls;

		new Expectations() {
			{
				alert.getMessage();
				returns(message);
			}
		};

		Macro macro = new MessageMacro();
		String output = macro.expandIn("$message$", ctx);
		assertEquals(
		        "RL1-rack1 intake temp is Out of allowable range. Data: []",
		        output);

	}
}
