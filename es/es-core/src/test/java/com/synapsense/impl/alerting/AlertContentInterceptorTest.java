package com.synapsense.impl.alerting;

import static org.junit.Assert.assertEquals;

import java.security.Principal;
import java.util.*;

import javax.ejb.SessionContext;
import javax.interceptor.InvocationContext;

import com.synapsense.dto.*;
import org.jmock.Expectations;
import org.jmock.Mockery;
import org.jmock.integration.junit4.JUnit4Mockery;
import org.jmock.lib.legacy.ClassImposteriser;
import org.junit.Test;

import com.synapsense.dal.objectcache.CacheSvc;
import com.synapsense.impl.alerting.macros.AlertMacroFactory;
import com.synapsense.impl.alerting.macros.Macro;
import com.synapsense.impl.alerting.macros.MacroContext;
import com.synapsense.service.AlertService;
import com.synapsense.service.Environment;
import com.synapsense.service.LocalizationService;
import com.synapsense.service.UnitSystemsService;
import com.synapsense.service.UserManagementService;
import com.synapsense.service.user.UserContext;
import com.synapsense.util.CollectionUtils;
import com.synapsense.util.unitconverter.UnitConverter;
import com.synapsense.util.unitconverter.UnitResolvers;
import com.synapsense.util.unitconverter.UnitSystem;
import com.synapsense.util.unitconverter.UnitSystems;

/**
 * The test will compile but not succeed since there's no easy way for mocking
 * MacroFactory static methods with jmock. Consider to rewrite test with jMockit
 * later
 */
public class AlertContentInterceptorTest {

	private Mockery context = new JUnit4Mockery() {
		{
			setImposteriser(ClassImposteriser.INSTANCE);
		}
	};

	private final Environment environment = context.mock(Environment.class);
	private final UserContext userContextMock = context.mock(UserContext.class);
	private final MacroContext macroContextMock = context.mock(MacroContext.class);
	private final AlertMacroFactory macroFactoryMock = context.mock(AlertMacroFactory.class);
	private final Macro macroMock = context.mock(Macro.class);
	private final UnitSystems unitSysMock = context.mock(UnitSystems.class);
	private final UnitSystem unitSystemMock = context.mock(UnitSystem.class);
	private final UnitResolvers unitResolversMock = context.mock(UnitResolvers.class);
	private final UnitConverter unitConvMock = context.mock(UnitConverter.class);
	private final SessionContext sessionCtxMock = context.mock(SessionContext.class);
	private final CacheSvc cacheMock = context.mock(CacheSvc.class);
	private final UnitSystemsService usServiceMock = context.mock(UnitSystemsService.class);
	private final UserManagementService umServiceMock = context.mock(UserManagementService.class);
	private final InvocationContext invocationCtxMock = context.mock(InvocationContext.class);
	private final Principal principalMock = context.mock(Principal.class);
	private final AlertService alertServiceMock = context.mock(AlertService.class);
	private final AlertType alertTypeMock = context.mock(AlertType.class);
	private final MessageTemplate templateMock = context.mock(MessageTemplate.class);
	private final Map<String, Object> userPreferenceMock = context.mock(Map.class);
	private final TO<?> userMock = context.mock(TO.class);
	private final LocalizationService localizationServiceMock = context.mock(LocalizationService.class);

	@Test
	public void alertMessageUsingExistingTemplate() throws Exception {
		AlertContentInterceptor invoker = newInvokerInstance();

		final Collection<AlertType> alertTypes = CollectionUtils.newSet();
		alertTypes.add(alertTypeMock);

		final Collection<Alert> alerts = CollectionUtils.newList();
		alerts.add(new Alert("alert", "UNKNOWN", new Date(), "message"));

		context.checking(new Expectations() {
			{
				exactly(1).of(invocationCtxMock).proceed();
				will(returnValue(alerts));

				exactly(1).of(sessionCtxMock).getCallerPrincipal();
				will(returnValue(principalMock));

				exactly(1).of(principalMock).getName();
				will(returnValue("admin"));

				exactly(1).of(umServiceMock).getUser("admin");
				will(returnValue(userMock));

				exactly(1).of(umServiceMock).getUserPreferences(userMock);
				will(returnValue(userPreferenceMock));

				exactly(1).of(userPreferenceMock).get("UnitsSystem");
				will(returnValue("Server"));

				exactly(1).of(unitSysMock).getUnitSystem("Server");
				will(returnValue(unitSystemMock));

				exactly(1).of(alertServiceMock).getAlertTypes(new String[]{"UNKNOWN"});
				will(returnValue(alertTypes));

				exactly(1).of(alertTypeMock).getName();
				will(returnValue("UNKNOWN"));

				exactly(1).of(alertTypeMock).getMessageTemplate(MessageType.CONSOLE);
				will(returnValue(templateMock));

				exactly(1).of(templateMock).getBody(with(any(Environment.class)));
				will(returnValue("message"));
				
				exactly(1).of(umServiceMock).getUserPreferences(userMock);
				will(returnValue(userPreferenceMock));

				exactly(1).of(userPreferenceMock).get("SavedLanguage");
				will(returnValue("en_US"));

				exactly(1).of(localizationServiceMock).getTranslation("message", Locale.US);
				will(returnValue("localized_message"));
				
				exactly(1).of(alertTypeMock).getDescription();
				will(returnValue("description"));
				
				exactly(1).of(macroMock).expandIn(with("localized_message"), with(any(MacroContext.class)));
				will(returnValue("updated message"));
			}
		});

		Collection<Alert> updatedAlerts = (Collection<Alert>) invoker.intercept(invocationCtxMock);
		Alert alert = updatedAlerts.iterator().next();
		assertEquals("updated message", alert.getFullMessage());

		context.assertIsSatisfied();
	}

	@Test
	public void alertMessageHasNoTemplates() throws Exception {
		AlertContentInterceptor invoker = newInvokerInstance();

		final Collection<AlertType> alertTypes = CollectionUtils.newSet();
		alertTypes.add(alertTypeMock);

		final Collection<Alert> alerts = CollectionUtils.newList();
		alerts.add(new Alert("alert", "UNKNOWN", new Date(), "message"));

		context.checking(new Expectations() {
			{
				exactly(1).of(invocationCtxMock).proceed();
				will(returnValue(alerts));

				exactly(1).of(sessionCtxMock).getCallerPrincipal();
				will(returnValue(principalMock));

				exactly(1).of(principalMock).getName();
				will(returnValue("admin"));

				exactly(1).of(umServiceMock).getUser("admin");
				will(returnValue(userMock));

				exactly(1).of(umServiceMock).getUserPreferences(userMock);
				will(returnValue(userPreferenceMock));

				exactly(1).of(userPreferenceMock).get("UnitsSystem");
				will(returnValue("Server"));

				exactly(1).of(unitSysMock).getUnitSystem("Server");
				will(returnValue(unitSystemMock));
				exactly(1).of(alertServiceMock).getAlertTypes(new String[] { "UNKNOWN" });
				will(returnValue(alertTypes));

				exactly(1).of(alertTypeMock).getName();
				will(returnValue("UNKNOWN"));

				// not templates specified
				exactly(1).of(alertTypeMock).getMessageTemplate(MessageType.CONSOLE);
				will(returnValue(null));

				exactly(1).of(umServiceMock).getUserPreferences(userMock);
				will(returnValue(userPreferenceMock));

				exactly(1).of(userPreferenceMock).get("SavedLanguage");
				will(returnValue("en_US"));

				exactly(1).of(localizationServiceMock).getTranslation("$message$", Locale.US);
				will(returnValue("$localized_message$"));
				
				exactly(1).of(alertTypeMock).getDescription();
				will(returnValue("description"));

				// deafault body is used
				exactly(1).of(macroMock).expandIn(with("$localized_message$"), with(any(MacroContext.class)));
				will(returnValue("updated message"));

			}
		});

		Collection<Alert> updatedAlerts = (Collection<Alert>) invoker.intercept(invocationCtxMock);
		Alert alert = updatedAlerts.iterator().next();
		// full message was not replaced
		assertEquals("updated message", alert.getFullMessage());

		context.assertIsSatisfied();
	}

	private AlertContentInterceptor newInvokerInstance() {
		AlertContentInterceptor invoker = new AlertContentInterceptor();
		invoker.setCache(cacheMock);
		invoker.setCtx(sessionCtxMock);
		invoker.setUnitSystemService(usServiceMock);
		invoker.setUserService(umServiceMock);
		invoker.setAlertService(alertServiceMock);
		invoker.setLocalizationService(localizationServiceMock);
		
		context.checking(new Expectations() {
			{
				exactly(1).of(usServiceMock).getUnitSystems();
				will(returnValue(unitSysMock));

				exactly(1).of(usServiceMock).getUnitReslovers();
				will(returnValue(unitResolversMock));

			}
		});

		invoker.setGenericEnv(environment);
		invoker.setMacro(macroMock);

		return invoker;
	}
}
