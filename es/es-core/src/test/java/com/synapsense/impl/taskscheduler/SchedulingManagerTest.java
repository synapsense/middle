package com.synapsense.impl.taskscheduler;

import com.synapsense.impl.reporting.app.ReportingManagementServiceImpl;
import com.synapsense.service.reporting.dto.ReportTaskInfo;
import com.synapsense.service.reporting.exception.SchedulingReportingException;
import com.synapsense.util.CollectionUtils;
import mockit.Expectations;
import mockit.Mocked;
import mockit.integration.junit4.JMockit;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

@RunWith(JMockit.class)
public class SchedulingManagerTest {

	@Mocked
	QuartzTaskSchedulerImplMBean scheduler;

	@Test
    public void workingWithScheduledTasks() throws TaskSchedulerException, SchedulingReportingException {
        ReportingSchedulingServiceImpl manager = new ReportingSchedulingServiceImpl();
        manager.setScheduler(scheduler);

       final ReportTaskInfo taskInfo = ReportTaskInfo.newInstance("Test", "Test");
       final String cron = "0 0/5 * * * ?";
       taskInfo.setCron(cron);

        new Expectations() {
            {
                LinkedHashMap<Class<?>, Object> params = new LinkedHashMap<>();
                params.put(ReportTaskInfo.class, taskInfo);

                scheduler.isCronValid(taskInfo.getCron());
                result = true;

                scheduler.addEjbTask(withPrefix(taskInfo.getReportTemplateName()), taskInfo.getReportTemplateName(), taskInfo.getCron(),
                        "java:global/es-ear/es-core/ReportingManagementService",
                        ReportingManagementServiceImpl.class.getName(), "runComplexReportTask", params);


                scheduler.getScheduledTasksByGroup(taskInfo.getReportTemplateName());
                result = CollectionUtils.list(taskInfo.getReportTemplateName());

                scheduler.getEjbTaskParams(taskInfo.getReportTemplateName(), taskInfo.getReportTemplateName());
                Map<Class<?>, Object> map = CollectionUtils.newMap();
                map.put(ReportTaskInfo.class, taskInfo);
                result = map;

                scheduler.removeGroup(taskInfo.getReportTemplateName());

            }
        };

        manager.validateReportTask(taskInfo);
        manager.scheduleReportTask(taskInfo);

        Collection<ReportTaskInfo> tasks = manager.getScheduledTasks(taskInfo.getReportTemplateName());
        assertThat(tasks.size(), is(1));

        manager.removeScheduledTasks(tasks.iterator().next().getReportTemplateName());
    }
}
