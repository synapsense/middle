package com.synapsense.impl.alerting;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.experimental.theories.Theories;
import org.junit.runner.RunWith;

import com.synapsense.dto.AlertStatus;

@RunWith(Theories.class)
public class AlertStatusTest {

	@Test
	public void proceedOpenedToAck() {
		assertTrue(AlertStatus.OPENED.canProceedTo(AlertStatus.ACKNOWLEDGED));
	}

	@Test
	public void proceedAckToDismiss() {
		assertTrue(AlertStatus.ACKNOWLEDGED.canProceedTo(AlertStatus.DISMISSED));
	}

	@Test
	public void proceedAckToResolve() {
		assertTrue(AlertStatus.ACKNOWLEDGED.canProceedTo(AlertStatus.RESOLVED));
	}

	@Test
	public void proceedAckToOpened() {
		assertFalse(AlertStatus.ACKNOWLEDGED.canProceedTo(AlertStatus.OPENED));
	}

	@Test
	public void proceedDismissToOpened() {
		assertFalse(AlertStatus.DISMISSED.canProceedTo(AlertStatus.OPENED));
	}

	@Test
	public void proceedResolveToOpened() {
		assertFalse(AlertStatus.RESOLVED.canProceedTo(AlertStatus.OPENED));
	}

	@Test
	public void compoundAckOnNullString() {
		AlertStatus newStatus = AlertStatus.ACKNOWLEDGED;
		assertEquals("$localize(alert_comment_ack)$", newStatus.compoundComment(null));
	}

	@Test
	public void compoundResOnNullString() {
		AlertStatus newStatus = AlertStatus.RESOLVED;
		assertEquals("$localize(alert_comment_res)$", newStatus.compoundComment(null));
	}

	@Test
	public void compoundDisOnNullString() {
		AlertStatus newStatus = AlertStatus.DISMISSED;
		assertEquals("$localize(alert_comment_dis)$", newStatus.compoundComment(null));
	}

	@Test
	public void compoundEmptyCommentOnResolve() {
		AlertStatus newStatus = AlertStatus.RESOLVED;
		assertEquals("$localize(alert_comment_res)$", newStatus.compoundComment(""));
	}

	@Test
	public void compoundNotEmptyCommentOnResolve() {
		AlertStatus newStatus = AlertStatus.RESOLVED;
		assertEquals("comment; $localize(alert_comment_res)$", newStatus.compoundComment("comment"));
	}

	@Test
	public void compoundEmptyCommentOnDismiss() {
		AlertStatus newStatus = AlertStatus.DISMISSED;
		assertEquals("$localize(alert_comment_dis)$", newStatus.compoundComment(""));
	}

	@Test
	public void compoundNotEmptyCommentOnDismiss() {
		AlertStatus newStatus = AlertStatus.DISMISSED;
		assertEquals("comment; $localize(alert_comment_dis)$", newStatus.compoundComment("comment"));
	}

	@Test
	public void compoundEmptyCommentOnAck() {
		AlertStatus newStatus = AlertStatus.ACKNOWLEDGED;
		assertEquals("$localize(alert_comment_ack)$", newStatus.compoundComment(""));
	}

	@Test
	public void compoundNotEmptyCommentOnAck() {
		AlertStatus newStatus = AlertStatus.ACKNOWLEDGED;
		assertEquals("comment; $localize(alert_comment_ack)$", newStatus.compoundComment("comment"));
	}

}
