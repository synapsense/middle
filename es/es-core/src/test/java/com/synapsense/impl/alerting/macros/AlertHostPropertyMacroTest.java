package com.synapsense.impl.alerting.macros;

import com.synapsense.dto.Alert;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.service.Environment;
import java.util.Date;
import mockit.Expectations;
import mockit.Mocked;
import mockit.integration.junit4.JMockit;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;

@RunWith(JMockit.class)
public class AlertHostPropertyMacroTest {

	@Test
	public void testNotMatchingMacroExpansion(@Mocked Environment env, @Mocked Alert alert) throws Exception {
		MacroContext ctx = new MacroContext();
		ctx.env = env;
		ctx.alert = alert;
		
		Macro macro = new AlertHostPropertyMacro();
		
		assertEquals("$ALERT_MESSAGE_TEMPLATE:11$", macro.expandIn("$ALERT_MESSAGE_TEMPLATE:11$", ctx));
		assertEquals("$$", macro.expandIn("$$", ctx));
		assertEquals("$123:$", macro.expandIn("$123:$", ctx));
		assertEquals("$conv(1.5)$", macro.expandIn("$conv(1.5)$", ctx));
	}
	
	@Test
	public void testSimplePropertyExpansion(@Mocked Environment env) throws Exception {
		final TO<?> to = TOFactory.getInstance().loadTO("ALERT_MESSAGE_TEMPLATE:11");
		MacroContext ctx = new MacroContext();
		ctx.env = env;
		ctx.alert = new Alert("alert", "type", new Date(), "$" + to.toString() + ".name$", to);
		
		new Expectations() {
			{
				env.getPropertyValue(to, "name", Object.class);
				returns("Some name");
			}
		};
		
		Macro macro = new AlertHostPropertyMacro();
		
		String inputString = "$name$";
		
		assertEquals("Some name", macro.expandIn(inputString, ctx));
	}
	

	@Test
	public void testOneDotValueExpansion(@Mocked Environment env) throws Exception {
		final TO<?> to = TOFactory.getInstance().loadTO("MY_TYPE:23");
		final TO<?> ref = TOFactory.getInstance().loadTO("MY_TYPE:24");
		String inputString = "$ref.name$";
		MacroContext ctx = new MacroContext();
		ctx.env = env;
		ctx.alert = new Alert("alert", "type", new Date(), "$message$", to);
		new Expectations() {
			{
				env.getPropertyValue(to, "ref", Object.class);
				returns(ref);

				env.getPropertyValue(ref, "name", Object.class);
				returns("Some name");
			}
		};

		Macro macro = new AlertHostPropertyMacro();
		assertEquals("Some name", macro.expandIn(inputString, ctx));
	}
	
	@Test
	public void testTwoDotValueExpansion(@Mocked Environment env) throws Exception {
		final TO<?> to = TOFactory.getInstance().loadTO("MY_TYPE:23");
		final TO<?> ref = TOFactory.getInstance().loadTO("MY_TYPE:24");
		final TO<?> ref1 = TOFactory.getInstance().loadTO("MY_TYPE:25");
		MacroContext ctx = new MacroContext();
		ctx.env = env;
		ctx.alert = new Alert("alert", "type", new Date(), "$message$", to);
		new Expectations() {
			{
				env.getPropertyValue(to, "ref", Object.class);
				returns(ref);
				
				env.getPropertyValue(ref, "ref1", Object.class);
				returns(ref1);
				
				env.getPropertyValue(ref1, "name", Object.class);
				returns("Some name");
			}
		};

		String inputString = "$ref.ref1.name$";
		Macro macro = new AlertHostPropertyMacro();
		assertEquals("Some name", macro.expandIn(inputString, ctx));
	}
	
	// [BC 01/2015] Does this timeout accounts for infinite recursion?
	@Test(timeout=1000)
	public void testSimpleValueExpansion(@Mocked Environment env) throws Exception {
		final TO<?> to = TOFactory.getInstance().loadTO("ALERT_MESSAGE_TEMPLATE:11");
		MacroContext ctx = new MacroContext();
		ctx.env = env;
		ctx.alert = new Alert("alert", "type", new Date(), "$" + to.toString() + ".name$", to);
		
		Macro macro = new AlertHostPropertyMacro();
		
		// explicit reference to another object property
		String inputString = "$" + to.toString() + ".name$";
		assertEquals(inputString, macro.expandIn(inputString, ctx));
	}

}
