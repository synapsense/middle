package com.synapsense.impl.reporting.commandengine;

import java.lang.reflect.InvocationTargetException;

import junit.framework.Assert;
import mockit.Delegate;
import mockit.Expectations;
import mockit.Injectable;
import mockit.NonStrictExpectations;
import mockit.Tested;
import mockit.integration.junit4.JMockit;

import org.junit.Test;
import org.junit.runner.RunWith;

import com.synapsense.impl.reporting.commandengine.monitoring.StatusMonitor;
import com.synapsense.impl.reporting.commandengine.state.CommandState;

@RunWith(JMockit.class)
public class CommandStateMachineTest {

	@Tested
	CommandContext commandContext;
	@Injectable
	Command command;
	
	@Test
	public void testCommandCompletedCancelled() {

		new Expectations() {
			{
				command.getName();
				minTimes = 0;
				returns("Test Command");

				command.run(withInstanceOf(StatusMonitor.class));
				result = checkState(CommandState.RUNNING, commandContext);

				command.interrupt();
				result = checkState(CommandState.CANCELLING, commandContext);

				command.rollback();
				result = checkState(CommandState.CANCELLING, commandContext);
			}
		};
		commandContext.cancel();
		Assert.assertEquals("Command canceled, state = CANCELLED", CommandState.CANCELLED, commandContext.getState());

		commandContext.setState(CommandState.READY);
		commandContext.start();
		Assert.assertEquals("Command successfully finished, state = COMPLETED", CommandState.COMPLETED,
		        commandContext.getState());

		commandContext.setState(CommandState.RUNNING);
		commandContext.cancel();
		Assert.assertEquals("Command cancelling, state = CANCELLING", CommandState.CANCELLING,
		        commandContext.getState());

		commandContext.complete();
		Assert.assertEquals("Command canceled, state = CANCELLED", CommandState.CANCELLED, commandContext.getState());

	}

	@Test
	public void testCommandFailed() {
		final CommandContext commandContext = createCommandStateContext(command);

		new Expectations() {
			{
				command.getName();
				minTimes = 0;
				returns("Test Command");
				
				command.run(withInstanceOf(StatusMonitor.class));
				result = new RuntimeException();

				command.rollback();
				result = checkState(CommandState.CANCELLING, commandContext);
			}
		};

		commandContext.start();
		Assert.assertEquals("Command failed, state = CANCELLED", CommandState.CANCELLED, commandContext.getState());

	}
	
	@Test
	public void testErrorStateResult() {

		new Expectations() {
			{
				command.getName();
				minTimes = 0;
				returns("Test Command");
				
				command.interrupt();
				result = new RuntimeException();

				command.rollback();
				result = new RuntimeException();
			}
		};
		commandContext.setState(CommandState.RUNNING);
		commandContext.cancel();
		Assert.assertEquals("Command interruption failed, state = ERROR", CommandState.ERROR, commandContext.getState());

		commandContext.setState(CommandState.CANCELLING);
		commandContext.complete();
		Assert.assertEquals("Command rollback failed, state = ERROR", CommandState.ERROR, commandContext.getState());

	}

	@Test
	public void testRestartCancelledOrCompletedCommand() {

		new NonStrictExpectations() {
			{
				command.getName();
				returns("Test Command");
				
				command.run(withInstanceOf(StatusMonitor.class));
				times = 3;
				result = checkState(CommandState.RUNNING, commandContext);
				
				command.interrupt();
				result = checkState(CommandState.CANCELLING, commandContext);
				
				command.rollback();
				result = checkState(CommandState.CANCELLING, commandContext);
			}
		};
		commandContext.start();
		commandContext.start();
		Assert.assertEquals("Command successfully finished, state = COMPLETED", CommandState.COMPLETED,
		        commandContext.getState());

		commandContext.setState(CommandState.RUNNING);
		commandContext.cancel();
		commandContext.complete();
		commandContext.start();
		Assert.assertEquals("Command successfully finished, state = COMPLETED", CommandState.COMPLETED,
		        commandContext.getState());

	}

	@Test
	public void testCommandSuspend() {

		new Expectations() {
			{
				command.getName();
				times = 0;
				returns("Test Command");
				
				command.pause();
				result = Boolean.TRUE;

				command.renew();
				result = checkState(CommandState.SUSPENDED, commandContext);
			}
		};

		commandContext.setState(CommandState.RUNNING);
		commandContext.suspend();
		commandContext.resume();
		commandContext.complete();

		Assert.assertEquals("Command successfully finished, state = COMPLETED", CommandState.COMPLETED,
		        commandContext.getState());


	}

	@Test
	public void testCommandSuspendFail() {

		new Expectations() {
			{
				command.getName();
				times = 0;
				returns("Test Command");
				
				command.pause();
				result = new RuntimeException();

				command.rollback();
				result = checkState(CommandState.CANCELLING, commandContext);
			}
		};

		commandContext.setState(CommandState.RUNNING);
		commandContext.suspend();
		Assert.assertEquals("Suspend failed, state = CANCELLED", CommandState.CANCELLED, commandContext.getState());
	}
	
	@Test
	public void testCommandResumeFail() {
		
		commandContext = createCommandStateContext(command);

		new Expectations() {
			{
				command.getName();
				minTimes = 0;
				returns("Test Command");

				command.pause();
				result = Boolean.TRUE;

				command.renew();
				result = new RuntimeException();
			}
		};

		commandContext.setState(CommandState.RUNNING);
		commandContext.suspend();
		commandContext.resume();
		Assert.assertEquals("Resume failed, state = ERROR", CommandState.ERROR, commandContext.getState());
		
	}

	@Test
	public void testStatesIllegalTokens() {

		new NonStrictExpectations() {
			{
				command.getName();
				returns("Test Command");
			}
		};

		checkIllegalCommandState(commandContext, CommandState.READY, CommandState.TOKEN_COMPLETE);
		checkIllegalCommandState(commandContext, CommandState.READY, CommandState.TOKEN_FAIL);
		checkIllegalCommandState(commandContext, CommandState.READY, CommandState.TOKEN_RESUME);

		checkIllegalCommandState(commandContext, CommandState.RUNNING, CommandState.TOKEN_START);
		checkIllegalCommandState(commandContext, CommandState.RUNNING, CommandState.TOKEN_RESUME);

		checkIllegalCommandState(commandContext, CommandState.COMPLETED, CommandState.TOKEN_COMPLETE);
		checkIllegalCommandState(commandContext, CommandState.COMPLETED, CommandState.TOKEN_FAIL);
		checkIllegalCommandState(commandContext, CommandState.COMPLETED, CommandState.TOKEN_RESUME);

		checkIllegalCommandState(commandContext, CommandState.CANCELLED, CommandState.TOKEN_COMPLETE);
		checkIllegalCommandState(commandContext, CommandState.CANCELLED, CommandState.TOKEN_FAIL);
		checkIllegalCommandState(commandContext, CommandState.CANCELLED, CommandState.TOKEN_RESUME);

		checkIllegalCommandState(commandContext, CommandState.ERROR, CommandState.TOKEN_COMPLETE);
		checkIllegalCommandState(commandContext, CommandState.ERROR, CommandState.TOKEN_FAIL);
		checkIllegalCommandState(commandContext, CommandState.ERROR, CommandState.TOKEN_RESUME);

		checkIllegalCommandState(commandContext, CommandState.ERROR, CommandState.TOKEN_START);

		checkIllegalCommandState(commandContext, CommandState.CANCELLING, CommandState.TOKEN_RESUME);
		checkIllegalCommandState(commandContext, CommandState.CANCELLING, CommandState.TOKEN_START);

		checkIllegalCommandState(commandContext, CommandState.SUSPENDED, CommandState.TOKEN_COMPLETE);
		checkIllegalCommandState(commandContext, CommandState.SUSPENDED, CommandState.TOKEN_START);

	}

	
	
	private void checkIllegalCommandState(CommandContext context, CommandState state, String token) {
		try {
			state.getClass().getMethod(token, CommandContext.class).invoke(state, context);
			Assert.fail("Expected illegal state were not thrown from state " + state);
		} catch (InvocationTargetException e) {
			if (e.getCause().getClass() != IllegalStateException.class) {
				Assert.fail(e.getMessage());
			}
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
	}
	
	private Delegate checkState(final CommandState state, final CommandContext commandContext) {
        return new Delegate() {
        	Object checkState() {
        		Assert.assertEquals("State Assertion", state, commandContext.getState());
        		return null;
        	}
           
        };
	}

	private CommandContext createCommandStateContext(final Command command) {
		new Expectations() {
			{
				command.getName();
				minTimes = 0;
				returns("Test Command");
			}
		};

		return new CommandContext(command);
	}

}
