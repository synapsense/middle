package com.synapsense.impl.reporting.commandengine;

import com.synapsense.impl.reporting.commandengine.CommandManager;
import com.synapsense.impl.reporting.commandengine.CommandProxy;
import com.synapsense.service.reporting.dto.CommandStatus;
import org.junit.Assert;

public class CommandTestUtils {

	private CommandTestUtils() {
	}

	public static boolean waitUntilFinished(CommandProxy proxy, CommandManager commandManager) {
		while (true) {
			int commandId = proxy.getCommandId();
			CommandStatus commandStatus = commandManager.getReportCommandStatus(commandId);

			if (CommandStatus.COMPLETED.equals(commandStatus)) {
				return true;
			} else if (CommandStatus.FAILED.equals(commandStatus)) {
				Assert.fail("Task failed");
				return false;
			}
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				Assert.fail(e.getMessage());
			}
		}
	}
}
