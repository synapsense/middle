package com.synapsense.impl.alerting.notification.smtp;

import com.synapsense.service.Environment;
import java.net.MalformedURLException;
import mockit.Expectations;
import mockit.Mocked;
import mockit.Verifications;
import mockit.integration.junit4.JMockit;
import org.apache.commons.mail.HtmlEmail;
import org.junit.Test;

import javax.activation.DataSource;
import java.io.ByteArrayInputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import org.junit.runner.RunWith;

import static com.synapsense.impl.alerting.AlertConstants.NOTIFICATIONS.IMAGE_SERVER_CONNECTION_TIMEOUT;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.StringEndsWith.endsWith;
import static org.hamcrest.core.StringStartsWith.startsWith;
import static org.junit.Assert.assertThat;

/**
 * @author : shabanov
 */
@RunWith(JMockit.class)
public class ImageAttachmentTest {

	@Mocked
	Environment env;

	@Mocked
	private URL url;

	@Mocked
	private HttpURLConnection urlConn;

	@Mocked
	private HtmlEmail email;

	@Mocked
	private ImageDataSource ds;

	@Test
	public void inlineAttachmentEmptyData() throws Exception {
		ImageAttachment attachment = new ImageAttachment("image", ds);

		new Expectations() {
			{
				ds.getImage();
				result = null;
			}
		};

		Attachment.InlinedAttachment contentID = attachment.inline(email);
		assertThat(contentID.getMarkup(), is("-"));
	}

	@Test
	public void inlineAttachmentSuccess() throws Exception {
		ImageAttachment attachment = new ImageAttachment("image", ds);

		new Expectations() {
			{
				ds.getImage();
				result = new byte[] {};

				ds.getType();
				result = "image/png";

				ds.getFileName();
				result = "image.png";
			}
		};

		Attachment.InlinedAttachment contentID = attachment.inline(email);
		assertThat(contentID.getMarkup(), startsWith("<img src=\"cid:"));
		assertThat(contentID.getMarkup(), endsWith("\">"));

		new Verifications() {
			{
				email.embed(withInstanceOf(DataSource.class), withInstanceOf(String.class));
			}
		};
	}
}
