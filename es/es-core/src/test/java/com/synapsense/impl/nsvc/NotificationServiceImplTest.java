package com.synapsense.impl.nsvc;

import java.util.Collection;

import org.jmock.Expectations;
import org.jmock.Mockery;
import org.jmock.integration.junit4.JMock;
import org.jmock.integration.junit4.JUnit4Mockery;
import org.jmock.integration.junit4.JUnitRuleMockery;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.synapsense.dto.TOFactory;
import com.synapsense.impl.nsvc.compressor.ReferencedObjectChangedEventCompressor;
import com.synapsense.service.nsvc.events.DispatchingProcessor;
import com.synapsense.service.nsvc.events.ObjectCreatedEvent;
import com.synapsense.service.nsvc.events.ObjectCreatedEventProcessor;
import com.synapsense.service.nsvc.events.ObjectDeletedEvent;
import com.synapsense.service.nsvc.events.ObjectDeletedEventProcessor;
import com.synapsense.service.nsvc.events.ReferencedObjectChangedEvent;
import com.synapsense.service.nsvc.events.ReferencedObjectChangedEventProcessor;
import com.synapsense.service.nsvc.filters.ObjectCreatedEventFilter;
import com.synapsense.service.nsvc.filters.ObjectDeletedEventFilter;
import com.synapsense.service.nsvc.filters.TOMatcher;
import com.synapsense.util.Pair;

public class NotificationServiceImplTest {
	@Rule public final JUnitRuleMockery context = new JUnitRuleMockery();
	static private NotificationServiceImpl nsvc = null;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		nsvc = new NotificationServiceImpl();
		nsvc.create();
		nsvc.start();
	}

	@Test
	public void testDeliveringWithoutFilters() {
		DispatchingProcessor eventProcessor = new DispatchingProcessor();

		final ObjectCreatedEventProcessor ocep = context.mock(ObjectCreatedEventProcessor.class);
		eventProcessor.putOcep(ocep);

		nsvc.registerProcessor(eventProcessor, null);

		final ObjectCreatedEvent event = new ObjectCreatedEvent(TOFactory.getInstance().loadTO("UNKNOWN:1"));

		context.checking(new Expectations() {
			{
				oneOf(ocep).process(event);
				will(returnValue(event));
			}
		});

		nsvc.notify(event);

		context.assertIsSatisfied();
	}

	@Test
	public void testDeliveringWithFilters() {
		DispatchingProcessor eventProcessor = new DispatchingProcessor();
		DispatchingProcessor filterProcessor = new DispatchingProcessor();

		final ObjectCreatedEventProcessor ocep = context.mock(ObjectCreatedEventProcessor.class);
		final ObjectDeletedEventProcessor odep = context.mock(ObjectDeletedEventProcessor.class);

		eventProcessor.putOcep(ocep);
		eventProcessor.putOdep(odep);

		final ObjectCreatedEventFilter ocef = new ObjectCreatedEventFilter(new TOMatcher("SENSOR"));
		final ObjectDeletedEventFilter odef = new ObjectDeletedEventFilter(new TOMatcher("UNKNOWN"));

		filterProcessor.putOcep(ocef);
		filterProcessor.putOdep(odef);

		nsvc.registerProcessor(eventProcessor, filterProcessor);

		final ObjectCreatedEvent oce = new ObjectCreatedEvent(TOFactory.getInstance().loadTO("UNKNOWN:1"));
		final ObjectDeletedEvent ode = new ObjectDeletedEvent(TOFactory.getInstance().loadTO("UNKNOWN:1"));

		context.checking(new Expectations() {
			{
				never(ocep).process(oce);
			}
		});

		nsvc.notify(oce);

		context.checking(new Expectations() {
			{
				oneOf(odep).process(ode);
			}
		});

		nsvc.notify(ode);

		context.assertIsSatisfied();
	}

	@Test
	@Ignore
	public void testDeliveringWithCompressors() throws Exception {
		DispatchingProcessor eventProcessor = new DispatchingProcessor();

		final ReferencedObjectChangedEventProcessor rocep = context.mock(ReferencedObjectChangedEventProcessor.class);

		eventProcessor.putRocep(rocep);

		nsvc.registerProcessor(eventProcessor, null);

		final Pair<Collection<ReferencedObjectChangedEvent>, Integer> data = ReferencedObjectChangedEventCompressorTest
		        .generateRandomData(100, 0.7);

		context.checking(new Expectations() {
			{
				exactly(data.getSecond()).of(rocep).process(with(aNonNull(ReferencedObjectChangedEvent.class)));
			}
		});

		for (ReferencedObjectChangedEvent event : data.getFirst()) {
			nsvc.notify(event);
		}

		// wait till events are compressed and sent
		Thread.sleep(nsvc.getIdlePeriod(ReferencedObjectChangedEventCompressor.class.getSimpleName()));
		context.assertIsSatisfied();
		System.out.println(nsvc.listCompressors());
	}

}
