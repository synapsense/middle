package com.synapsense.impl.reporting.model;

import static com.synapsense.impl.reporting.TestUtils.EMPTY_REPORT_NAME;
import static com.synapsense.impl.reporting.TestUtils.getEmptyReportDesignPath;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.concurrent.CountDownLatch;

import mockit.Delegate;
import mockit.Expectations;
import mockit.Mocked;

import mockit.integration.junit4.JMockit;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.synapsense.impl.reporting.TestUtils;
import com.synapsense.service.reporting.dto.ReportTaskInfo;
import com.synapsense.service.reporting.exception.CompilationReportingException;
import com.synapsense.service.reporting.exception.GenerationReportingException;
import com.synapsense.service.reporting.management.ReportParameter;
import org.junit.runner.RunWith;

@RunWith(JMockit.class)
public class ReportTemplateTest {

	@Before
	public void setUp() {
	}

	@Test
	public void testSuccessfullEmptyReportTemplateCompilation() {
		try {
			ReportTemplate template = new ReportTemplate(new FileInputStream(getEmptyReportDesignPath()));
			Assert.assertEquals("EmptyReport", template.getName());
			template = new ReportTemplate(TestUtils.readTestFile(getEmptyReportDesignPath()));
			Assert.assertEquals("EmptyReport", template.getName());
		} catch (CompilationReportingException e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		} catch (FileNotFoundException e) {
			Assert.fail(e.getMessage());
		}
	}

	@Test
	public void testCorrectReportParametersLoading() {
		try {
			ReportTemplate template = null;
			try {
				template = new ReportTemplate(new FileInputStream(getEmptyReportDesignPath()));
			} catch (FileNotFoundException e) {
				Assert.fail(e.getMessage());
				return;
			}
			ReportParameter stringParameter = template.getParameter("StringParameter");
			ReportParameter integerParameter = template.getParameter("IntegerParameter");

			Assert.assertEquals(stringParameter.getType(), java.lang.String.class);
			Assert.assertEquals(integerParameter.getType(), java.lang.Integer.class);
		} catch (CompilationReportingException e) {
			Assert.fail(e.getMessage());
		}
	}

	@Test
	public void testSuccessfulEmptyReportGeneration() {
		ReportTemplate template = null;
		try {
			template = new ReportTemplate(new FileInputStream(getEmptyReportDesignPath()));
		} catch (FileNotFoundException e) {
			Assert.fail(e.getMessage());
			return;
		} catch (CompilationReportingException e) {
			Assert.fail(e.getMessage());
			return;
		}
		try {
			ReportTaskInfo taskInfo = ReportTaskInfo.newInstance(EMPTY_REPORT_NAME, "Test");
			template.generateReport(taskInfo);
		} catch (GenerationReportingException e) {
			Assert.fail(e.getMessage());
		}
	}
	@Test
	public void testAsynchroniousReportGeneration(@Mocked ReportGenerationListener listener) {
		try {

			final CountDownLatch doneLatch = new CountDownLatch(1);

			ReportTemplate template = new ReportTemplate(new FileInputStream(getEmptyReportDesignPath()));

			new Expectations() {
				{
					listener.completed(withInstanceOf(Report.class));
		            result = new Delegate<CountDownLatch>() {
		                void countDown()
		                {
		                	doneLatch.countDown();
		                }
		             };
				}
			};
			ReportTaskInfo task = ReportTaskInfo.newInstance("Test", "test");
			task.addParameterValue("TestParam", "TestParamValue");
			
			ReportGenerationHandler handler = template.createReportGenerationHandler(task.getParametersValues());
			handler.addListener(listener);
			handler.startGeneration();

			doneLatch.await();
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}

}
