package com.synapsense.impl.reporting;

import com.synapsense.impl.reporting.commandengine.CommandEngineTestSuite;
import com.synapsense.impl.reporting.commands.CommandsTestSuite;
import com.synapsense.impl.reporting.model.ReportingModelTestSuite;
import com.synapsense.impl.reporting.tasks.TasksTestSuite;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
 @SuiteClasses({ TasksTestSuite.class, ReportingModelTestSuite.class, CommandsTestSuite.class, CommandEngineTestSuite.class })
 public class ReportingTestSuite {
}
