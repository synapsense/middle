package com.synapsense.impl.alerting.macros;

import com.synapsense.dto.Alert;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.service.Environment;
import com.synapsense.service.user.UserContext;
import com.synapsense.util.ConvertingEnvironmentProxy;
import com.synapsense.util.unitconverter.UnitConverter;
import com.synapsense.util.unitconverter.UnitSystems;
import mockit.Expectations;
import mockit.Mocked;
import mockit.integration.junit4.JMockit;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.math.RoundingMode;
import java.text.NumberFormat;
import java.util.Locale;

import static org.junit.Assert.assertEquals;

@RunWith(JMockit.class)
public class FullMacroSequenceTest {

	@Test
	public void testDimensionResolveMacro(@Mocked Environment env, @Mocked Alert alert, @Mocked UserContext userCtx,
										  @Mocked UnitSystems uSys, @Mocked ConvertingEnvironmentProxy convertingEnv) {
		MacroContext ctx = new MacroContext();
		ctx.env = env;
		ctx.alert = alert;
		ctx.usrCtx = userCtx;
		ctx.unitSystems = uSys;
		ctx.convEnv = convertingEnv;
		
		new Expectations() {
			{
				onInstance(alert).getMessage();
				returns("alert origin message : $data(WSNSENSOR:23.lastValue,1.5)$");

			}
		};

		String message = new DimensionResolveMacro().expandIn(alert.getMessage(), ctx);
		//System.out.println(message);
	}

	@Test
	@Ignore
	public void testMacroExpansion(@Mocked Environment env, @Mocked Alert alert, @Mocked UserContext userCtx,
								   @Mocked UnitSystems uSys, @Mocked UnitConverter uc, @Mocked ConvertingEnvironmentProxy convertingEnv)
	        throws Exception {
		String template = "Message $message$ , Trigger value: $TRIGGER_DATA_VALUE$, Current value: $CURRENT_DATA_VALUE$";
		MacroContext ctx = new MacroContext();
		ctx.env = env;
		ctx.alert = alert;
		ctx.usrCtx = userCtx;
		ctx.unitSystems = uSys;
		ctx.convEnv = convertingEnv;

		final TO<?> to = TOFactory.getInstance().loadTO("WSNSENSOR:23");

		new Expectations() {
			{
				onInstance(alert).getMessage();
				returns("alert origin message : $WSNSENSOR:23.lastValue$");

				onInstance(alert).getMessage();
				returns("alert origin message : $data(WSNSENSOR:23.lastValue,1.0,temperature)$");

				onInstance(alert).getMessage();
				returns("alert origin message : $data(WSNSENSOR:23.lastValue,1.0,temperature)$");

				onInstance(alert).getMessage();
				returns("alert origin message : $data(WSNSENSOR:23.lastValue,1.0,temperature)$");

				onInstance(env).getPropertyValue(to, "lastValue", Object.class);
				returns(5.5);

				onInstance(userCtx).getUnitSystem();
				returns("SI");

				onInstance(uSys).getUnitConverter("Server", "SI", "temperature");
				returns(uc);

				onInstance(userCtx).getDataFormat();
				returns(getNumberFormat());

				onInstance(uc).convert(1.0);

			}

		};

		Macro macro = AlertMacroFactory.getAllMacros();

		assertEquals("Message alert origin message : $conv()", macro.expandIn(template, ctx));
	}

	private NumberFormat getNumberFormat() {
		NumberFormat numberFormatter = NumberFormat.getInstance(Locale.US);
		numberFormatter.setRoundingMode(RoundingMode.HALF_UP);
		numberFormatter.setGroupingUsed(false);
		numberFormatter.setMaximumFractionDigits(2);
		numberFormatter.setMinimumFractionDigits(2);
		return numberFormatter;
	}

}
