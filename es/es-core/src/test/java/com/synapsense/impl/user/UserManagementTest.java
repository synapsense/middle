package com.synapsense.impl.user;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.*;

import javax.naming.Context;
import javax.naming.InitialContext;

import com.sun.tools.javac.comp.Env;
import com.synapsense.dto.*;
import com.synapsense.util.CollectionUtils;
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;

import com.synapsense.exception.UserManagementException;
import com.synapsense.service.UserManagementService;
import com.synapsense.util.JNDINames;
import com.synapsense.util.ServerLoginModule;

public class UserManagementTest {

	private static UserManagementService ums;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		Properties env = new Properties();
		env.put(Context.INITIAL_CONTEXT_FACTORY, "org.jboss.naming.remote.client.InitialContextFactory");
		env.put(Context.PROVIDER_URL, "remote://localhost:4447");
		env.put("jboss.naming.client.ejb.context", "true");
		env.put("jboss.naming.client.connect.options.org.xnio.Options.SASL_POLICY_NOPLAINTEXT", "false");
		env.put(Context.SECURITY_PRINCIPAL, "admin");
		env.put(Context.SECURITY_CREDENTIALS, "admin");
		ums = ServerLoginModule.getProxy(new InitialContext(env), JNDINames.USER_SERVICE_JNDI_NAME,
		        UserManagementService.class);
	}

	@After
	public void testAfter() throws Exception {
	}

	@Test
	// (expected = IllegalInputParameterException.class)
	public void testSystemPrivilegesExist() {
		Collection<TO<?>> privs = ums.getAllPrivileges();
		assertTrue(!privs.isEmpty());
		for (TO<?> p : privs) {
			Map<String, Object> props = ums.getPrivilegeInformation(p);
			if (props != null) {
				StringBuilder sb = new StringBuilder();
				sb.append(props.get(ums.PRIVILEGE_NAME)).append(" - ");
				if (props.get(ums.PRIVILEGE_SYSTEM) != null) {
					sb.append(App.getApps((Integer) props.get(ums.PRIVILEGE_SYSTEM)).toString()).append(" - ");
				}
				sb.append(props.get(ums.PRIVILEGE_DESCRIPTION));
				System.out.println(sb.toString());
			}
		}
	}

	@Test
	public void createUpdateDeletePrivilege() throws UserManagementException {
		String privName = "test_priv";
		TO<?> priv = null;

		try {
			priv = ums.createPrivilege(privName);
			assertTrue(ums.getAllPrivileges().contains(priv));

			ums.setProperty(priv, ums.PRIVILEGE_NAME, "new_test_priv");
			ums.setProperty(priv, ums.PRIVILEGE_DESCRIPTION, "new description");

			assertEquals("new_test_priv", (String) ums.getProperty(priv, ums.PRIVILEGE_NAME));
			assertEquals("new description", (String) ums.getProperty(priv, ums.PRIVILEGE_DESCRIPTION));
		} finally {
			ums.deletePrivilege(priv);
			assertTrue(!ums.getAllPrivileges().contains(priv));
		}
	}

	@Test
	public void testAdminGroupExists() {
		Collection<TO<?>> groups = ums.getAllGroups();
		for (TO<?> g : groups) {
			Map<String, Object> props = ums.getGroupInformation(g);
			System.out.println(props.get(ums.GROUP_NAME) + "-" + props.get(ums.GROUP_DESCRIPTION));
			if (props.get("name").equals("Administrators")) {
				return;
			}
		}

		assertTrue("No Administrators Group", false);
	}

	@Test
	public void testAdminGroupHasPrivileges() throws UserManagementException {
		TO<?> group = ums.getGroup("Administrators");
		Collection<String> privs = ums.getGroupPrivilegeIds(group);
		assertTrue(!privs.isEmpty());
		for (String p : privs) {
			System.out.println("admin privilege: " + p);
		}
	}

	@Test
	public void testAdminGroupHasUsers() throws UserManagementException {
		TO<?> group = ums.getGroup("Administrators");
		Collection<String> users = ums.getGroupUserIds(group);
		assertTrue(!users.isEmpty());
		assertTrue(users.contains("admin"));
		for (String u : users) {
			System.out.println("admin: " + u);
		}
	}

	@Test
	public void testAdminUserHasValidGroup() throws UserManagementException {
		TO<?> user = ums.getUser("admin");
		Collection<String> groups = ums.getUserGroupIds(user);
		assertTrue(!groups.isEmpty());
		assertTrue(groups.contains("Administrators"));
	}

	@Test
	public void testAdminUserHasPrivileges() throws UserManagementException {
		TO<?> user = ums.getUser("admin");
		Collection<String> privileges = ums.getUserPrivilegeIds(user);
		assertTrue(!privileges.isEmpty());
		assertTrue(privileges.contains("root_permission"));
	}

	@Test
	public void createUpdateDeleteGroup() throws UserManagementException {
		TO<?> group = null;

		try {
			group = ums.createGroup("test_group", "");
			assertTrue(ums.getAllGroups().contains(group));

			ums.setProperty(group, ums.GROUP_NAME, "new_test_group");
			ums.setProperty(group, ums.GROUP_DESCRIPTION, "new description");

			assertEquals("new_test_group", ums.getProperty(group, ums.GROUP_NAME));
			assertEquals("new description", ums.getProperty(group, ums.GROUP_DESCRIPTION));

		} finally {
			ums.deleteGroup(group);
			assertTrue(!ums.getAllGroups().contains(group));
		}

	}

	@Test
	public void testChangeGroupPrivileges() throws UserManagementException {
		TO<?> group = null;
		TO<?> priv1 = null;
		TO<?> priv2 = null;

		try {
			group = ums.createGroup("test_group", "");
			assertTrue(ums.getAllGroups().contains(group));
			assertTrue(ums.getGroupPrivileges(group).isEmpty());

			try {
				priv1 = ums.createPrivilege("test_priv_1");
				String priv1Name = (String) ums.getProperty(priv1, ums.PRIVILEGE_NAME);
				ums.setGroupPrivileges(group, Arrays.asList(priv1Name));
				Collection<String> privs = ums.getGroupPrivilegeIds(group);
				assertTrue(privs.contains(priv1Name));
				assertEquals(1, privs.size());

				priv2 = ums.createPrivilege("test_priv_2");
				String priv2Name = (String) ums.getProperty(priv2, ums.PRIVILEGE_NAME);
				ums.setGroupPrivileges(group, Arrays.asList(priv2Name));
				privs = ums.getGroupPrivilegeIds(group);
				assertTrue(privs.contains(priv2Name));
				assertEquals(1, privs.size());

			} finally {
				ums.deletePrivilege(priv1);
				// removing of a privilege must remove it from all groups, too
				ums.deletePrivilege(priv2);
				assertTrue(ums.getGroupPrivileges(group).isEmpty());
			}

		} finally {
			ums.deleteGroup(group);
			assertTrue(!ums.getAllGroups().contains(group));
		}

	}

	private boolean doesUserExist(String userName) {
		return ums.getUser(userName) != null;
	}

	@Test
	public void testChangeUserGroups() throws UserManagementException {
		String userName = "test_user";
		assertFalse(this.doesUserExist(userName));

		TO<?> user = null;
		TO<?> group1 = null;
		TO<?> group2 = null;
		try {
			user = ums.createUser(userName, "new_pass", null);
			assertTrue(this.doesUserExist(userName));
			assertEquals(1, ums.getUserGroups(user).size());

			try {
				group1 = ums.createGroup("test_group_1", "");
				ums.setUserGroups(user, Arrays.asList("test_group_1"));
				Collection<TO<?>> groups = ums.getUserGroups(user);
				assertTrue(groups.contains(group1));
				assertEquals(1, groups.size());

				group2 = ums.createGroup("test_group_2", "");
				ums.setUserGroups(user, Arrays.asList("test_group_2"));
				groups = ums.getUserGroups(user);
				assertTrue(groups.contains(group2));
				assertEquals(1, groups.size());

			} finally {
				ums.deleteGroup(group1);
				// removing group must remove it from all users, too
				ums.deleteGroup(group2);
				assertTrue(ums.getUserGroups(user).isEmpty());

			}

		} finally {
			ums.deleteUser(user);
			assertFalse(this.doesUserExist(userName));
		}
	}

	@Test
	public void testCopyPrivileges() throws UserManagementException {
		TO<?> group = ums.createGroup("newTestGroup_2", "");

		Collection<TO<?>> allPrivileges = ums.getAllPrivileges();
		Collection<String> privilegesList = new ArrayList<String>(allPrivileges.size());
		for (TO<?>  p : allPrivileges) {
			privilegesList.add((String) ums.getProperty(p, ums.PRIVILEGE_NAME));
		}

		try {
			ums.setGroupPrivileges(group, privilegesList);
			Collection<String> res = ums.getGroupPrivilegeIds(group);
			assertTrue(res.size() == privilegesList.size() && res.containsAll(privilegesList));
		} finally {
			ums.deleteGroup(group);
		}

	}
}
