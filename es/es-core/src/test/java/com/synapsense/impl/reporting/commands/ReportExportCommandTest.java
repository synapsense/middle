package com.synapsense.impl.reporting.commands;

import mockit.Expectations;
import mockit.Mocked;
import mockit.integration.junit4.JMockit;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.synapsense.impl.reporting.commandengine.monitoring.DefaultStatusMonitor;
import com.synapsense.impl.reporting.dao.ReportDAO;
import com.synapsense.impl.reporting.export.ExportFormat;
import com.synapsense.service.reporting.dto.ReportInfo;
import com.synapsense.service.reporting.exception.DAOReportingException;
import com.synapsense.service.reporting.exception.ObjectNotFoundDAOReportingException;
import org.junit.runner.RunWith;

@RunWith(JMockit.class)
public class ReportExportCommandTest {

	@Before
	public void setUp() {
	}

	@Test
	public void testSuccessfullReportExport(@Mocked ReportDAO reportDao) {
		try {
			new Expectations() {
				{
					reportDao.exportReport(1, ExportFormat.PDF);
				}
			};
		} catch (DAOReportingException e) {
			Assert.fail(e.getMessage());
		}

		ReportInfo rInfo = new ReportInfo("Test", System.currentTimeMillis());
		rInfo.setReportId(1);

		ReportExportCommand command = CommandFactory.newReportExportCommand(reportDao, rInfo, ExportFormat.PDF);
		Assert.assertEquals("Name does not equal", "Export of "
				+ rInfo.getReportId() + " command", command.getName());
		Assert.assertEquals("ReportInfo does not equal", rInfo, command.getReportInfo());
		command.run(new DefaultStatusMonitor());
	}
	
	@Test(expected = ReportingCommandException.class)
	public void testFailedReportExport(@Mocked ReportDAO reportDao) throws Exception {
		new Expectations() {
			{
				reportDao.exportReport(2, ExportFormat.PDF);
				result = new ObjectNotFoundDAOReportingException();
			}
		};

		ReportInfo rInfo = new ReportInfo("Test", System.currentTimeMillis());
		rInfo.setReportId(2);

		ReportExportCommand command = CommandFactory.newReportExportCommand(reportDao, rInfo, ExportFormat.PDF);
		command.run(new DefaultStatusMonitor());
	}
	
	@Test(expected = IllegalStateException.class)
	public void testNotPersistedReportExport(@Mocked ReportDAO reportDao) throws Exception {

		ReportInfo rInfo = new ReportInfo("Test", System.currentTimeMillis());

		ReportExportCommand command = CommandFactory.newReportExportCommand(reportDao, rInfo, ExportFormat.PDF);
		command.run(new DefaultStatusMonitor());
	}
}
