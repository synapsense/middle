package com.synapsense.impl.reporting.engine;

import java.util.Collections;
import java.util.Map;
import java.util.Set;

import mockit.Expectations;
import mockit.Injectable;
import mockit.Mocked;
import mockit.NonStrictExpectations;
import mockit.integration.junit4.JMockit;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.synapsense.dal.generic.reporting.ReportingQueryExecutor;
import com.synapsense.impl.reporting.queries.TableProcessor;
import com.synapsense.impl.reporting.tables.ReportTableImpl;
import com.synapsense.impl.reporting.tables.ReportTableTransformer;
import com.synapsense.impl.reporting.tasks.CompositeReportingTask;
import com.synapsense.impl.reporting.tasks.EnvValuesTaskData;
import com.synapsense.impl.reporting.tasks.ValueIdsTaskData;
import com.synapsense.service.reporting.Cell;
import com.synapsense.service.reporting.DimensionalQuery;
import com.synapsense.service.reporting.Formula;
import com.synapsense.service.reporting.Formula.TransformType;
import com.synapsense.service.reporting.olap.EmptySet;
import com.synapsense.service.reporting.olap.custom.EnvironmentDataSource;
import com.synapsense.util.CollectionUtils;

@RunWith(JMockit.class)
public class ReportDataTest {

	private static final Log log = LogFactory.getLog(ReportDataTest.class);
	
	ReportData reportData;
	
	Formula formula = new Formula("", TransformType.ORIGINAL);
	
	DimensionalQuery query = new DimensionalQuery(formula,
			new EmptySet(), new EmptySet(), new EmptySet());
	
	@Mocked
	TableProcessor tableProcessor;
	
	EnvironmentDataSource ds;
	
	ReportingQueryExecutor reportingQueryExecutor;	
	
	@Mocked
	ReportUserInfo reportUserInfo;
	
	@Mocked
	EnvValuesTaskData envValuesTaskData;
	
	@Mocked
	ReportTableImpl reportTableImpl;
	
	@Injectable
	java.util.List<Cell> cells = Collections.<Cell>emptyList();

	@Test
	public void testBuildQuery() {

		new NonStrictExpectations() {
			{
				new TableProcessor(ds, query, reportUserInfo);
				 
				tableProcessor.processSets();
				
				tableProcessor.buildValueIdQuery();
				result = withInstanceOf(ValueIdsTaskData.class);
				Map<String, Set<Cell>> formulaMap = CollectionUtils.newLinkedMap();
				tableProcessor.buildEnvDataQuery(envValuesTaskData);
				result = envValuesTaskData;
				
			}
		};
		
		reportData = new ReportData(query, ds, reportingQueryExecutor, reportUserInfo);
		reportData.buildQuery();
		
	}
	
	@Test
	public void testExecuteQuery(@Mocked CompositeReportingTask task,
								 @Mocked ReportTableTransformer reportTableTransformer) {
	
		new Expectations() {
			{
				query.getFormula();
			}
		};
		
		reportData = new ReportData(query, ds, reportingQueryExecutor, reportUserInfo);
		reportData.executeQuery();
		
	}

}
