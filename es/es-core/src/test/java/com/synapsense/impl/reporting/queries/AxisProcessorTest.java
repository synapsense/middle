package com.synapsense.impl.reporting.queries;

import com.synapsense.dal.generic.reporting.ReportingQueryExecutor;
import com.synapsense.impl.reporting.engine.ReportData;
import com.synapsense.impl.reporting.engine.ReportUserInfo;
import com.synapsense.impl.reporting.tables.ReportingTableHolder;
import com.synapsense.service.reporting.DimensionalQuery;
import com.synapsense.service.reporting.Formula;
import com.synapsense.service.reporting.Formula.TransformType;
import com.synapsense.service.reporting.olap.CrossJoin;
import com.synapsense.service.reporting.olap.EmptySet;
import com.synapsense.service.reporting.olap.custom.EnvironmentDataSource;
import mockit.Expectations;
import mockit.Injectable;
import mockit.Mocked;
import mockit.integration.junit4.JMockit;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(JMockit.class)
public class AxisProcessorTest {

    @Mocked
    AxisProcessor axisProcessor;

    ReportingTableHolder reportingTables;

    ReportData reportData;

    Formula formula = new Formula("", TransformType.ORIGINAL);

    DimensionalQuery query = new DimensionalQuery(formula,
            new EmptySet(), new EmptySet(), new EmptySet());

    @Injectable
    TableProcessor tableProcessor;

    EnvironmentDataSource ds;

    ReportingQueryExecutor reportingQueryExecutor;

    @Mocked
    ReportUserInfo reportUserInfo;

    @Test
    public void testAxisProcessorCreation() {

        new Expectations() {
            {
                new AxisProcessor(ds, tableProcessor, reportingTables);
            }
        };

        axisProcessor = new AxisProcessor(ds, tableProcessor, reportingTables);

    }

    @Test
    public void testProcessCrossJoin() {

        new Expectations() {
            {
                new AxisProcessor(ds, withInstanceLike(tableProcessor), reportingTables);

                axisProcessor.processSet(withInstanceOf(CrossJoin.class));

                axisProcessor.getValueIdAttributesTable();
            }
        };
        reportData = new ReportData(query, ds, reportingQueryExecutor, reportUserInfo);
        reportData.buildQuery();

    }

}
