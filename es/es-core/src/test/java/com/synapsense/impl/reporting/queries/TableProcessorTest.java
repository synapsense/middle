package com.synapsense.impl.reporting.queries;

import mockit.Expectations;
import mockit.Injectable;
import mockit.Mocked;

import mockit.integration.junit4.JMockit;
import org.junit.Test;

import com.synapsense.impl.reporting.engine.ReportUserInfo;
import com.synapsense.impl.reporting.tables.ReportTableTransformer;
import com.synapsense.impl.reporting.tables.ValueIdAttributesTable;
import com.synapsense.impl.reporting.tasks.CompositeReportingTask;
import com.synapsense.service.reporting.DimensionalQuery;
import com.synapsense.service.reporting.olap.AxisSet;
import com.synapsense.service.reporting.olap.custom.EnvironmentDataSource;
import org.junit.runner.RunWith;

@RunWith(JMockit.class)
public class TableProcessorTest {
	
	@Mocked
	DimensionalQuery query;
	
	@Mocked
	ValueIdAttributesTable valueIdAttributesTable;
	
	@Injectable
	AxisSet axisSet;
	
	@Mocked
	EnvironmentDataSource ds;

	@Mocked
	ReportUserInfo reportUserInfo;
	
	TableProcessor tableProcessor;
	
	@Test
	public void testTableProcessorCreation() {
	
		new Expectations() {
			{
				new TableProcessor(ds, query, reportUserInfo);
			}
		};
		
		tableProcessor = new TableProcessor(ds, query, reportUserInfo);
		
	}
	
	@Test
	public void testProcessSets(@Mocked CompositeReportingTask task,
								@Mocked ReportTableTransformer reportTableTransformer) {
	
		new Expectations() {
			{
				query.getSlicer();
				result = axisSet;
				
				query.getAxisSet(0);
				result = axisSet;
				
				query.getAxisSet(1);
				result = axisSet;
				
				query.getTableSet();
				result = axisSet;
				
				new ValueIdAttributesTable();
			}
		};
		
		tableProcessor = new TableProcessor(ds, query, reportUserInfo);
		tableProcessor.processSets();
		
	}
}
