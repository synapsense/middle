package com.synapsense.impl.alerting.notification.smtp;

import com.synapsense.dto.Message;
import com.synapsense.impl.alerting.notification.Provider;
import com.synapsense.service.Environment;
import com.synapsense.util.CollectionUtils;
import freemarker.template.Template;
import mockit.*;
import mockit.integration.junit4.JMockit;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.junit.Test;

import javax.mail.Session;
import java.io.StringWriter;
import java.util.Map;
import org.junit.runner.RunWith;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * @author : shabanov
 */
@RunWith(JMockit.class)
public class MailerTest {

	@Mocked
	private Provider<Session> sessionPrv;

	@Mocked
	private Provider<Template> templatePrv;

	@Mocked
	private Message msg;

	@Mocked
	private Session session;

	@Mocked
	private Template template;

	@Mocked
	private Attachment att;

	@Mocked
	private Email mail;

	@Mocked
	private Attachments attachments;

	@Mocked
	private Environment env;

	private void preSmtpMailerIsWorkingWell() {
		preSmtpMailerIsWorkingWell("valid@email.com");
	}

	private void preSmtpMailerIsWorkingWell(@Mocked String dest) {
		willMessageBeValid(dest);

		new Expectations() {
			{
				sessionPrv.get();
				result = session;

				session.getProperty("mail.from");
				result = dest;
			}
		};
	}

	private void postSmtpMailerIsWorkingWell() {
		new Expectations() {
			{
				msg.getHeader();
				result = "header";
			}
		};
	}

	private void willMessageBeValid(final String dest) {
		if (dest != null && !dest.isEmpty()) {
			new Expectations() {
				{
					msg.getDestination();
					result = dest;

					// repeat call cause it is in another method
					msg.getDestination();
					result = dest;
				}
			};
		} else {
			new Expectations() {
				{
					msg.getDestination();
					result = dest;
				}
			};
		}
	}

	@Test
	public void messageIsNotValidIfNullDestination() throws Exception {
		final SMTPMailer mailer = new HtmlMailer(sessionPrv, templatePrv);
		willMessageBeValid(null);
		assertThat(mailer.isValid(msg), is(false));
	}

	@Test
	public void messageIsNotValidIfEmptyDestination() throws Exception {
		final SMTPMailer mailer = new HtmlMailer(sessionPrv, templatePrv);
		willMessageBeValid("");
		assertThat(mailer.isValid(msg), is(false));
	}

	@Test
	public void notSentIfSessionIsNotConfigured() throws Exception {
		final SMTPMailer mailer = new HtmlMailer(sessionPrv, templatePrv);

		willMessageBeValid("valid@email.com");

		new Expectations() {
			{
				sessionPrv.get();
				result = session;

				session.getProperty("mail.from");
				result = null;
			}

		};

		mailer.send(msg);

	}

	@Test
	public void sendEmailBaseClassOnly() throws EmailException {
		final SMTPMailer mailer = new HtmlMailer(sessionPrv, templatePrv);

		preSmtpMailerIsWorkingWell("valid@email.com");
		postSmtpMailerIsWorkingWell();
		// mock call to template method
		new MockUp<HtmlMailer>() {
			@Mock
			protected Email generateEmail(Message message) {
				return mail;
			}
		};

		mailer.send(msg);

		new Verifications() {
			{
				mail.setCharset(SMTPMailer.ENCODING);
				mail.setSubject("header");

				mail.addTo("valid@email.com");
				mail.setFrom("valid@email.com");

				mail.setMailSession(session);
			}
		};
	}

	@Test
	public void sendEmailWithNoAttachmentsSuccess() throws Exception {
		final SMTPMailer mailer = new HtmlMailer(sessionPrv, templatePrv);

		preSmtpMailerIsWorkingWell();

		new Expectations() {
			{

				msg.getMediaData();
				result = CollectionUtils.newMap();

                msg.getContent();
                result = "mail content";

				templatePrv.get();
				result = template;

				template.process(withInstanceOf(Map.class), with(new Delegate<StringWriter>() {
					public boolean result(StringWriter w) {
						if (w == null)
							return false;
						w.append("plain text");
						return true;
					}
				}));
			}
		};

		postSmtpMailerIsWorkingWell();

		mailer.send(msg);

	}

	@Test
	public void sendFullEmailSuccess() throws Exception {

		final SMTPMailer mailer = new HtmlMailer(sessionPrv, templatePrv);

		preSmtpMailerIsWorkingWell();

		new Expectations() {
			@Mocked
			public Attachment liveImage;
			{
				Map<String, Object> media = CollectionUtils.newMap();
				media.put("attachments", attachments);
				msg.getMediaData();
				result = media;

			    attachments.putIntoEmail(withInstanceOf(HtmlEmail.class));

				msg.getContent();
				result = "mail content";

				templatePrv.get();
				result = template;

				template.process(withInstanceOf(Map.class), with(new Delegate<StringWriter>() {
					public boolean result(StringWriter w) {
						if (w == null)
							return false;
						w.append("plain text");
						return true;
					}
				}));
			}

		};

		postSmtpMailerIsWorkingWell();

		mailer.send(msg);
	}

	@Test
	public void sendSimpleEmailSuccess() throws Exception {

		final SMTPMailer mailer = new SimpleMailer(sessionPrv);

		preSmtpMailerIsWorkingWell();

		new Expectations() {
			{
				msg.getContent();
				result = "mail content";
			}
		};

		postSmtpMailerIsWorkingWell();

		mailer.send(msg);
	}
}
