package com.synapsense.impl.alerting.notification;

import static com.synapsense.impl.alerting.AlertConstants.TYPE.ALERT_TYPE_NAME;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import mockit.StrictExpectations;

import mockit.Mocked;
import mockit.integration.junit4.JMockit;
import org.junit.Test;

import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.EnvException;
import com.synapsense.impl.alerting.notification.Notification.ProcessResult;
import com.synapsense.service.Environment;
import org.junit.runner.RunWith;

@RunWith(JMockit.class)
public class RestoreNotificationStateTest {

	final TO<?> alertTO = TOFactory.getInstance().loadTO(ALERT_TYPE_NAME, 1);

	@Test
	public void saveStateToAlert(@Mocked Environment env) throws Exception {
		final NotificationStateManager stateManager = new EnvNotificationStateManager(env);
		final ValueTO[] values = { new ValueTO("lastNTierName", "TIER1"), new ValueTO("lastAttempt", 0) };

		new StrictExpectations() {
			{
				env.setPropertyValue(alertTO, "lastAttempt", 0);
				times = 1;

				env.setAllPropertiesValues(alertTO, values);
				times = 1;

				env.setPropertyValue(alertTO, "lastAttempt", 1);
				times = 1;
				result = new EnvException();

				env.setAllPropertiesValues(alertTO, values);
				times = 1;
				result = new EnvException();
			}
		};
		stateManager.saveLastAttempt("1", 0);
		stateManager.saveLastTierName("1", "TIER1");

		stateManager.saveLastAttempt("1", 1);
		stateManager.saveLastTierName("1", "TIER1");
	}

	@Test
	public void loadStateFromAlert(@Mocked Environment env) throws Exception {
		final NotificationStateManager stateManager = new EnvNotificationStateManager(env);

		new StrictExpectations() {
			{
				env.getPropertyValue(alertTO, "lastNTierName", String.class);
				times = 1;
				returns("TIER2");

				env.getPropertyValue(alertTO, "lastAttempt", Integer.class);
				times = 1;
				returns(1);
			}
		};
		assertEquals("TIER2", stateManager.loadLastTierName("1"));
		assertEquals((Integer) 1, stateManager.loadLastAttempt("1"));
	}

	@Test
	public void loadStateFromAlertFailed(@Mocked Environment env) throws Exception {
		final NotificationStateManager stateManager = new EnvNotificationStateManager(env);

		new StrictExpectations() {
			{
				env.getPropertyValue(alertTO, "lastNTierName", String.class);
				times = 1;
				result = new EnvException();

				env.getPropertyValue(alertTO, "lastAttempt", Integer.class);
				times = 1;
				result = new EnvException();
			}
		};
		assertNull(stateManager.loadLastTierName("1"));
		assertNull(stateManager.loadLastAttempt("1"));
	}

	@Test
	public void processPersistentNotification(@Mocked AlertNotification notification,
											  @Mocked NotificationStateManager stateManager, @Mocked NotificationManager ctx) throws Exception {
		final AlertNotificationPersistent an = new AlertNotificationPersistent(notification, stateManager);
		final ProcessResult pr = new ProcessResult();
		pr.na = 1;
		pr.tier = "TIER2";
		pr.isTierChanged = true;

		new StrictExpectations() {
			{
				notification.process(ctx);
				times = 1;
				returns(pr);

				notification.getId();
				times = 1;
				returns("1");

				stateManager.saveLastTierName("1", pr.tier);
				times = 1;

				notification.process(ctx);
				times = 1;
				returns(pr);

				notification.getId();
				times = 1;
				returns("1");

				stateManager.saveLastAttempt("1", pr.na);
				times = 1;
			}
		};
		assertEquals(pr, an.process(ctx));
		pr.isTierChanged = false;
		assertEquals(pr, an.process(ctx));
	}

	@Test
	public void processPersistentNotificationWithNullProcessResult(@Mocked AlertNotification notification,
																   @Mocked NotificationStateManager stateManager, @Mocked NotificationManager ctx) throws Exception {
		final AlertNotificationPersistent an = new AlertNotificationPersistent(notification, stateManager);

		new StrictExpectations() {
			{
				notification.process(ctx);
				times = 1;
				returns(null);
			}
		};
		assertNull(an.process(ctx));
	}
}
