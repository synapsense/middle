package com.synapsense.impl.reporting.tasks;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import mockit.Expectations;
import mockit.Injectable;
import mockit.Mocked;
import mockit.NonStrictExpectations;
import mockit.Tested;
import mockit.integration.junit4.JMockit;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.synapsense.dal.generic.reporting.ReportingQueryExecutor;
import com.synapsense.util.CollectionUtils;

@RunWith(JMockit.class)
public class GetValueIdsTaskTest {

	private static final Log log = LogFactory.getLog(GetValueIdsTaskTest.class);
	
	@Tested
	GetValueIdsTask task;
	
	@Injectable
	@Mocked
	ValueIdsTaskData valueIdsTaskData;
	@Injectable
	@Mocked
	ReportingQueryExecutor queryExecutor;
	@Injectable
	java.util.Map<Integer, List<Integer>> parameterMap = Collections.<Integer, List<Integer>>emptyMap();

	@Injectable
	java.util.List<Object[]> valueIdsVsObjectIds = CollectionUtils.newList();
	
	@Test
	public void testSuccessfulGetValueIdsTaskRun() {

		new Expectations() {
			{
				queryExecutor.executeHQLQuery(anyString, withInstanceOf(Map.class));
				times=0;
				
			}
		};
		task = new GetValueIdsTask(valueIdsTaskData, queryExecutor);
		task.execute();
		
	}
	
	@Test
	public void testGetValueIdsTaskRun() {

		new NonStrictExpectations() {
			{

				valueIdsTaskData.getValueIdsQuery();
				
				valueIdsTaskData.getValueIdsParameterMap();
				result = parameterMap;
				
				queryExecutor.executeHQLQuery(anyString, parameterMap);			
				result = valueIdsVsObjectIds;
				valueIdsVsObjectIds.add(new Object[]{});
				
				valueIdsTaskData.getValueIdAttributes();
				
				valueIdsTaskData.getValueIdCells();
				
			}
		};
		task = new GetValueIdsTask(valueIdsTaskData, queryExecutor);
		task.execute();
		
	}

}