package com.synapsense.impl.user;

import static org.junit.Assert.*;

import java.util.Collection;
import java.util.Map;
import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;

import com.synapsense.dto.TO;
import com.synapsense.util.CollectionUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.synapsense.service.UserManagementService;
import com.synapsense.util.JNDINames;
import com.synapsense.util.ServerLoginModule;

public class UserManagementServiceTest {

	static UserManagementService ums;
	static final String TEST_USER_NAME = "___this_is_a_test_user___";
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		Properties env = new Properties();
		env.put(Context.INITIAL_CONTEXT_FACTORY, "org.jboss.naming.remote.client.InitialContextFactory");
		env.put(Context.PROVIDER_URL, "remote://localhost:4447");
		env.put("jboss.naming.client.ejb.context", "true");
		env.put("jboss.naming.client.connect.options.org.xnio.Options.SASL_POLICY_NOPLAINTEXT", "false");
		env.put(Context.SECURITY_PRINCIPAL, "admin");
		env.put(Context.SECURITY_CREDENTIALS, "admin");
		ums = ServerLoginModule.getProxy(new InitialContext(env), JNDINames.USER_SERVICE_JNDI_NAME, UserManagementService.class);
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void createUser() throws Exception {
		TO<?> u = null;

		try {
			u = ums.createUser(TEST_USER_NAME, "blah-blah", null);
			String encPwd = ums.getRawPassword(u);

			TO<?> checkUser = ums.getUser(TEST_USER_NAME);
			assertNotNull(checkUser);
			assertEquals(encPwd, (String) ums.getProperty(checkUser, ums.USER_PASSWORD));

		} finally {
			ums.deleteUser(u);
		}
	}

	@Test
	public void createUpdatePrivTest() throws Exception{
		TO<?> testPrivilege = null;
		try{
			testPrivilege = ums.createPrivilege("privilege");
			Map<String, Object> props = CollectionUtils.newMap();
			props.put(ums.PRIVILEGE_DESCRIPTION, "Lorem ipsum dolor sit amet");
			props.put(ums.PRIVILEGE_TAG, "TAG");
			props.put(ums.PRIVILEGE_SORT_ORDER, 111);
			ums.updatePrivilege(testPrivilege, props);
			Collection<TO<?>> r = ums.getAllPrivileges();
			assertTrue(r.contains(testPrivilege));

			String testPrivName = (String) ums.getProperty(testPrivilege, ums.PRIVILEGE_NAME);
			for (TO<?> p : r){
				String privName = (String) ums.getProperty(p, ums.PRIVILEGE_NAME);
				if (privName.equals(testPrivName)){
					StringBuilder sb = new StringBuilder();
					sb.append(privName).append(":  ");
					sb.append((String) ums.getProperty(p, ums.PRIVILEGE_DESCRIPTION)).append(", ");
					sb.append((String) ums.getProperty(p, ums.PRIVILEGE_TAG)).append(", ");
					sb.append(ums.getProperty(p, ums.PRIVILEGE_SORT_ORDER).toString()).append(", ");
					System.out.println(sb.toString());
				}
			}

			ums.setProperty(testPrivilege, ums.PRIVILEGE_TAG, "HHHHH");
			ums.setProperty(testPrivilege, ums.PRIVILEGE_SORT_ORDER, 1300);
			r = ums.getAllPrivileges();
			assertTrue(r.contains(testPrivilege));

			testPrivName = (String) ums.getProperty(testPrivilege, ums.PRIVILEGE_NAME);
			for (TO<?> p : r){
				String privName = (String) ums.getProperty(p, ums.PRIVILEGE_NAME);
				if (privName.equals(testPrivName)){
					StringBuilder sb = new StringBuilder();
					sb.append(privName).append(":  ");
					sb.append((String) ums.getProperty(p, ums.PRIVILEGE_DESCRIPTION)).append(", ");
					sb.append((String) ums.getProperty(p, ums.PRIVILEGE_TAG)).append(", ");
					sb.append(ums.getProperty(p, ums.PRIVILEGE_SORT_ORDER).toString()).append(", ");
					System.out.println(sb.toString());
				}
			}

		}finally{
			ums.deletePrivilege(testPrivilege);
		}
	}

}
