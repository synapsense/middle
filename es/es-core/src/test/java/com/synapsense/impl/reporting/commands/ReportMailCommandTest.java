package com.synapsense.impl.reporting.commands;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import mockit.Expectations;
import mockit.Mocked;

import mockit.integration.junit4.JMockit;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.synapsense.impl.reporting.TestUtils;
import com.synapsense.impl.reporting.commandengine.Command;
import com.synapsense.impl.reporting.commandengine.CompositeCommand;
import com.synapsense.impl.reporting.commandengine.monitoring.DefaultStatusMonitor;
import com.synapsense.impl.reporting.dao.ReportDAO;
import com.synapsense.impl.reporting.export.ExportFormat;
import com.synapsense.impl.reporting.mail.JbossMailSession;
import com.synapsense.impl.reporting.mail.SMTPMailer;
import com.synapsense.service.reporting.dto.ReportInfo;
import com.synapsense.service.reporting.exception.ObjectNotFoundDAOReportingException;
import com.synapsense.service.reporting.exception.SmtpReportingException;
import org.junit.runner.RunWith;

/**
 * This test shouldn't be used as a part of unit tests suite as it sends mail
 * messages
 */

@RunWith(JMockit.class)
public class ReportMailCommandTest {

	private static final String TEST_TASK_NAME = "Test Task";

	@Before
	public void setUp() {
	}

	@Test
	public void testSendReportViaEmail(@Mocked ReportDAO rDao, @Mocked SMTPMailer mailer) {

		try {
			new Expectations() {
				{
					rDao.exportReport(anyInt, withInstanceOf(ExportFormat.class));
					rDao.getExportRootPath();
					returns(TestUtils.EMPTY_REPORT_DESIGN_PATH);
					new SMTPMailer(withInstanceOf(JbossMailSession.class));
					mailer.send(ReportMailCommand.MESSAGE_SUBJECT + " - " + TEST_TASK_NAME
							, ReportMailCommand.SINGLE_FILE_MESSAGE_BODY
							, withInstanceOf(String[].class)
							, withInstanceOf(String[].class));
				}

			};
		} catch (
				ObjectNotFoundDAOReportingException | 
				SmtpReportingException e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}

		Properties props = new Properties();
		props.put("mail.transport.protocol", "smtp");
		props.put("mail.smtp.port", "25");
		props.put("mail.smtp.host", "qmail.merann.ru");
		props.put("mail.from", "prj.synapsense@mera.ru");

		List<String> destinations = new ArrayList<String>();
		destinations.add("gryabov@mera.ru");

		CompositeCommand compCommand = new CompositeCommand();

		ReportInfo reportInfo = new ReportInfo(TEST_TASK_NAME , System.currentTimeMillis());
		reportInfo.setReportId(1);
		reportInfo.addExportedPath("pdf", null);
		
		Command reportMailCommand = CommandFactory.newReportMailCommand(rDao, destinations, reportInfo);
		String expectedDescription = new StringBuilder().append("Sending report ").append(reportInfo.getTaskName())
		        .append(" via email to ").append(destinations).toString();
		Assert.assertEquals("Description is incorrect", expectedDescription, reportMailCommand.getDescription());
		Assert.assertEquals("ReportInfo does not equal", reportInfo, reportMailCommand.getReportInfo());
		compCommand.addCommand(reportMailCommand);

		compCommand.run(new DefaultStatusMonitor());

	}
	
	@Test(expected = ReportingCommandException.class)
	public void testFailedSendReportViaEmail(@Mocked ReportDAO rDao, @Mocked SMTPMailer mailer) throws Exception {

		try {
			new Expectations() {
				{
					rDao.exportReport(anyInt, withInstanceOf(ExportFormat.class));
					rDao.getExportRootPath();
					new SMTPMailer(withInstanceOf(JbossMailSession.class));
					mailer.send(ReportMailCommand.MESSAGE_SUBJECT + " - " + TEST_TASK_NAME
							, ReportMailCommand.SINGLE_FILE_MESSAGE_BODY
							, withInstanceOf(String[].class)
							, withInstanceOf(String[].class));
					result = new SmtpReportingException();
				}

			};
		} catch (
				ObjectNotFoundDAOReportingException e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}

		Properties props = new Properties();
		props.put("mail.transport.protocol", "smtp");
		props.put("mail.smtp.port", "25");
		props.put("mail.smtp.host", "qmail.merann.ru");
		props.put("mail.from", "prj.synapsense@mera.ru");

		List<String> destinations = new ArrayList<String>();
		destinations.add("gryabov@mera.ru");

		CompositeCommand compCommand = new CompositeCommand();

		ReportInfo reportInfo = new ReportInfo(TEST_TASK_NAME , System.currentTimeMillis());
		reportInfo.setReportId(1);
		reportInfo.addExportedPath("pdf", null);
		
		Command reportMailCommand = CommandFactory.newReportMailCommand(rDao, destinations, reportInfo);
		compCommand.addCommand(reportMailCommand);
		compCommand.run(new DefaultStatusMonitor());

	}
	
	@Test(expected = IllegalStateException.class)
	public void testNotPersistedReportSend(@Mocked ReportDAO rDao) {

		Properties props = new Properties();
		props.put("mail.transport.protocol", "smtp");
		props.put("mail.smtp.port", "25");
		props.put("mail.smtp.host", "qmail.merann.ru");
		props.put("mail.from", "prj.synapsense@mera.ru");

		List<String> destinations = new ArrayList<String>();
		destinations.add("gryabov@mera.ru");

		CompositeCommand compCommand = new CompositeCommand();

		ReportInfo reportInfo = new ReportInfo(TEST_TASK_NAME , System.currentTimeMillis());

		reportInfo.addExportedPath("pdf", null);
		
		Command reportMailCommand = CommandFactory.newReportMailCommand(rDao, destinations, reportInfo);
		compCommand.addCommand(reportMailCommand);
		compCommand.run(new DefaultStatusMonitor());

	}

}
