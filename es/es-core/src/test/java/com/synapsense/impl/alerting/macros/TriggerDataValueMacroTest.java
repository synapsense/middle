package com.synapsense.impl.alerting.macros;

import static org.junit.Assert.assertEquals;
import mockit.Expectations;

import mockit.Mocked;
import mockit.integration.junit4.JMockit;
import org.junit.Test;

import com.synapsense.dto.Alert;
import com.synapsense.service.Environment;
import org.junit.runner.RunWith;

@RunWith(JMockit.class)
public class TriggerDataValueMacroTest {

	@Test
	public void testExpansion(@Mocked Environment env, @Mocked Alert alert) throws Exception {
		String inputString = "Trigger value: $TRIGGER_DATA_VALUE$";
		MacroContext ctx = new MacroContext();
		ctx.env = env;
		ctx.alert = alert;

		new Expectations() {
			{
				onInstance(alert).getMessage();
				returns("alert message : $data(WSNSENSOR:23.lastValue,1.0,temperature)$");

			}
		};

		Macro macro = new TriggerDataValueMacro();
		assertEquals("Trigger value: $conv(1.0,temperature)$", macro.expandIn(inputString, ctx));
	}

	@Test
	public void testNoDimensionExpansion(@Mocked Environment env, @Mocked Alert alert) throws Exception {
		String inputString = "Trigger value: $TRIGGER_DATA_VALUE$";
		MacroContext ctx = new MacroContext();
		ctx.env = env;
		ctx.alert = alert;

		new Expectations() {
			{
				onInstance(alert).getMessage();
				returns("alert message : $data(WSNSENSOR:23.lastValue,1.0)$");

			}
		};

		Macro macro = new TriggerDataValueMacro();
		assertEquals("Trigger value: 1.0", macro.expandIn(inputString, ctx));
	}

}
