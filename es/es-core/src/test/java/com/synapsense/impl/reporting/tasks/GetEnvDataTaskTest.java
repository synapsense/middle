package com.synapsense.impl.reporting.tasks;

import java.util.Collections;
import mockit.Expectations;
import mockit.Injectable;
import mockit.Tested;
import mockit.integration.junit4.JMockit;
import org.junit.Test;

import com.synapsense.dal.generic.reporting.ReportingQueryExecutor;
import com.synapsense.impl.reporting.queries.types.ReportingQueryType;
import org.junit.runner.RunWith;

@RunWith(JMockit.class)
public class GetEnvDataTaskTest {

	@Tested
	GetEnvDataTask task;

	@Injectable
	EnvValuesTaskData envValuesTaskData;

	@Injectable
	ReportingQueryExecutor queryExecutor;

	@Test
	public void testCutGetEnvDataTaskRun() {

		new Expectations() {
			{
				
				envValuesTaskData.getValueIdCells();
				result = Collections.emptyList();
				
				envValuesTaskData.getEnvDataQuery();
				result = anyString;

				envValuesTaskData.getQueryType();
				result = ReportingQueryType.SINGLE_PROPERTY_CELL_TABLE;

			}
		};

		task = new GetEnvDataTask(envValuesTaskData, queryExecutor);
		task.execute();
		
	}

}