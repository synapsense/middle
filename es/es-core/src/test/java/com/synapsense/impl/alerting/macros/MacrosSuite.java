package com.synapsense.impl.alerting.macros;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ AlertHostPropertyMacroTest.class, ConsoleUrlMacroTest.class, ConversionMacroTest.class,
        CurrentDataValueMacroTest.class, EnvPropertyMacroTest.class, FullMacroSequenceTest.class,
        RelationMacroTest.class, TriggerDataValueMacroTest.class, TimestampMacroTest.class, MessageMacroTest.class,
        LocalizeMacroTest.class, ImageMacroTest.class })
public class MacrosSuite {

}
