package com.synapsense.impl.alerting.macros;

import static org.junit.Assert.assertEquals;

import java.text.DecimalFormat;
import java.util.HashMap;

import mockit.Expectations;

import mockit.Mocked;
import mockit.integration.junit4.JMockit;
import org.junit.Test;

import com.synapsense.service.user.UserContext;
import com.synapsense.util.unitconverter.Converter;
import com.synapsense.util.unitconverter.UnitConverter;
import com.synapsense.util.unitconverter.UnitSystem;
import com.synapsense.util.unitconverter.UnitSystems;
import org.junit.runner.RunWith;

@RunWith(JMockit.class)
public class ConversionMacroTest {

    private static DecimalFormat DECIMAL_FORMAT = new DecimalFormat("#0.00###");

	@Test
	public void testValueExpansion(@Mocked UserContext usrCtx, @Mocked UnitSystems us, @Mocked UnitSystem usone)
	        throws Exception {
		String inputString = "$conv(23.5,my dim)$";
		MacroContext ctx = new MacroContext();
		ctx.usrCtx = usrCtx;
		ctx.unitSystems = us;

		new Expectations() {
			{
				usrCtx.getUnitSystem();
				returns("SI");

				us.getUnitConverter("Server", "SI", "my dim");
				returns(new UnitConverter(null, null, "my dim", new Converter() {
					@Override
					public double convert(double value) {
						return value * 2;
					}
				}));

				usrCtx.getDataFormat();
				returns(new DecimalFormat("0.00"));

				us.getSimpleDimensions();
				returns(new HashMap());

				us.getUnitSystem("SI");
				returns(usone);

				usone.getDimension("my dim");
				returns(null);
			}
		};

		Macro macro = new ConversionMacro();

		assertEquals(DECIMAL_FORMAT.format(47.00), macro.expandIn(inputString, ctx));
	}

	@Test
	public void testNegativeValueExpansion(@Mocked UserContext usrCtx, @Mocked UnitSystems us, @Mocked UnitSystem usone)
	        throws Exception {
		String inputString = "$conv(-23.5,my dim)$";
		MacroContext ctx = new MacroContext();
		ctx.usrCtx = usrCtx;
		ctx.unitSystems = us;

		new Expectations() {
			{
				usrCtx.getUnitSystem();
				returns("SI");

				us.getUnitConverter("Server", "SI", "my dim");
				returns(new UnitConverter(null, null, "my dim", new Converter() {
					@Override
					public double convert(double value) {
						return value * 2;
					}
				}));

				usrCtx.getDataFormat();
				returns(new DecimalFormat("0.00"));

				us.getSimpleDimensions();
				returns(new HashMap());

				us.getUnitSystem("SI");
				returns(usone);

				usone.getDimension("my dim");
				returns(null);
			}
		};

		Macro macro = new ConversionMacro();
		assertEquals(DECIMAL_FORMAT.format(-47.00), macro.expandIn(inputString, ctx));
	}

	@Test
	public void testOutOfRangeNegativeValueExpansion(@Mocked UserContext usrCtx, @Mocked UnitSystems us) throws Exception {
		String inputString = "$conv(-1001.0,my dim)$";
		MacroContext ctx = new MacroContext();
		ctx.usrCtx = usrCtx;
		ctx.unitSystems = us;

		Macro macro = new ConversionMacro();
		assertEquals(Macro.DEFAULT_EXPANSION, macro.expandIn(inputString, ctx));
	}

}
