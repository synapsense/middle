package com.synapsense.impl.activitylog;

import com.synapsense.dto.Alert;
import com.synapsense.dto.AlertType;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.impl.alerting.AlertServiceImpl;
import com.synapsense.impl.user.UserManagementServiceImpl;
import com.synapsense.service.impl.activitylog.ActivityLogService;
import com.synapsense.service.impl.activitylog.ActivityRecord;
import com.synapsense.util.CollectionUtils;
import java.lang.reflect.Method;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import mockit.Expectations;
import mockit.Mocked;
import mockit.integration.junit4.JMockit;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mvel2.PropertyAccessException;
import org.mvel2.templates.TemplateRuntime;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;


@RunWith(JMockit.class)
public class ActivityTest {

	@Mocked
	AlertServiceImpl alertService;
	@Mocked
	UserManagementServiceImpl userService;

	@Test
	public void logDeleteAlertTypeActivity(@Mocked AlertType at) throws SecurityException, NoSuchMethodException {
		Activity a = new Activity();

		new Expectations() {
			{
				alertService.getAlertType("ALERT_TYPE_NAME");
				times = 1;
				result = at;

				at.getDisplayName();
				times = 1;
				result = "ALERT_TYPE_DISPLAY_NAME";
			}
		};

		Method method = AlertServiceImpl.class.getMethod("deleteAlertType", String.class);

		ActivityRecord ar = a.log("admin", alertService, method, new Object[] { "ALERT_TYPE_NAME" });
		assertEquals("AlertManagement", ar.getModule());
		assertEquals("DeleteAlertType", ar.getAction());
		assertEquals("$localize(activity_record_alert_type_deleted,\"ALERT_TYPE_DISPLAY_NAME\")$", ar.getDescription());
		assertNull(ar.getObject());
		assertEquals("admin", ar.getUserName());
	}

	@Test
	public void logDeleteUnexistingAlertTypeActivity(@Mocked ActivityLogService als, @Mocked AlertServiceImpl alertService)
	        throws SecurityException, NoSuchMethodException {
		Activity a = new Activity();

		new Expectations() {
			{
				alertService.getAlertType("ALERT_TYPE_NAME");
				times = 1;
				result = null;
			}
		};

		Method method = AlertServiceImpl.class.getMethod("deleteAlertType", String.class);

		ActivityRecord ar = null;
		try {
			ar = a.log("admin", alertService, method, new Object[]{"ALERT_TYPE_NAME"});
		} catch (Exception e) {
		}
		assertNull(ar);
	}

	@Test
	public void logCreateAlertTypeActivity(@Mocked AlertType at) throws SecurityException, NoSuchMethodException {
		Activity a = new Activity();

		new Expectations() {
			{

				at.getDisplayName();
				times = 1;
				result = "ALERT_TYPE_DISPLAY_NAME";
			}
		};

		Method method = AlertServiceImpl.class.getMethod("createAlertType", AlertType.class);

		ActivityRecord ar = a.log("admin", alertService, method, new Object[] { at });
		assertEquals("AlertManagement", ar.getModule());
		assertEquals("AddAlertType", ar.getAction());
		assertEquals("$localize(activity_record_alert_type_created,\"ALERT_TYPE_DISPLAY_NAME\")$", ar.getDescription());
		assertNull(ar.getObject());
		assertEquals("admin", ar.getUserName());
	}

	@Test
	public void logCreateUpdateTypeActivity(@Mocked AlertType at) throws SecurityException, NoSuchMethodException {
		Activity a = new Activity();

		new Expectations() {
			{

				at.getDisplayName();
				times = 1;
				result = "ALERT_TYPE_DISPLAY_NAME";

				at.isActive();
				times = 1;
				result = true;
			}
		};

		Method method = AlertServiceImpl.class.getMethod("updateAlertType", String.class, AlertType.class);

		ActivityRecord ar = a.log("admin", alertService, method, new Object[] { "ALERT_TYPE_NAME", at });
		assertEquals("AlertManagement", ar.getModule());
		assertEquals("UpdateAlertType", ar.getAction());
		assertEquals("$localize(activity_record_alert_type_updated,\"ALERT_TYPE_DISPLAY_NAME\",\"true\")$",
		        ar.getDescription());
		assertNull(ar.getObject());
		assertEquals("admin", ar.getUserName());
	}

	@Test
	public void logDismissAlertByIdActivity() throws SecurityException, NoSuchMethodException {
		Activity a = new Activity();

		Method method = AlertServiceImpl.class.getMethod("dismissAlert", Integer.class, String.class);

		ActivityRecord ar = a.log("admin", alertService, method, new Object[] { 1, "ack" });
		assertEquals("AlertManagement", ar.getModule());
		assertEquals("DismissObjectAlert", ar.getAction());
		assertEquals("$localize(activity_record_alert_with_id_dismissed,\"1\")$", ar.getDescription());
		assertNull(ar.getObject());
		assertEquals("admin", ar.getUserName());
	}

	@Test
	public void logDismissAlertLikeSomeInstanceActivity() throws SecurityException,
	        NoSuchMethodException {
		Activity a = new Activity();
		Alert alert = new Alert("test", "test", new Date(), "none");

		Method method = AlertServiceImpl.class.getMethod("dismissAlert", Alert.class, String.class);

		ActivityRecord ar = a.log("admin", alertService, method, new Object[] { alert, "ack" });
		assertEquals("AlertManagement", ar.getModule());
		assertEquals("DismissObjectAlert", ar.getAction());
		assertEquals("$localize(activity_record_alert_dismissed,\""+alert.toString()+"\")$", ar.getDescription());
		assertNull(ar.getObject());
		assertEquals("admin", ar.getUserName());
	}

	@Test
	public void logAcknowledgeAlertByIdActivity() throws SecurityException, NoSuchMethodException {
		Activity a = new Activity();

		Method method = AlertServiceImpl.class.getMethod("acknowledgeAlert", Integer.class, String.class);

		ActivityRecord ar = a.log("admin", alertService, method, new Object[] { 1, "ack" });
		assertEquals("AlertManagement", ar.getModule());
		assertEquals("AcknowledgeAlert", ar.getAction());
		assertEquals("$localize(activity_record_alert_with_id_acknowledged,\"1\")$", ar.getDescription());
		assertNull(ar.getObject());
		assertEquals("admin", ar.getUserName());
	}

	@Test
	public void logResolveAlertByIdActivity() throws SecurityException, NoSuchMethodException {
		Activity a = new Activity();

		Method method = AlertServiceImpl.class.getMethod("resolveAlert", Integer.class, String.class);

		ActivityRecord ar = a.log("admin", alertService, method, new Object[] { 1, "ack" });
		assertEquals("AlertManagement", ar.getModule());
		assertEquals("ResolveAlert", ar.getAction());
		assertEquals("$localize(activity_record_alert_with_id_resolved,\"1\")$", ar.getDescription());
		assertNull(ar.getObject());
		assertEquals("admin", ar.getUserName());
	}

	@Test
	public void logDismissAllAlertsOnObjectActivity() throws SecurityException, NoSuchMethodException {
		Activity a = new Activity();

		Method method = AlertServiceImpl.class.getMethod("dismissAlerts", TO.class, boolean.class, String.class);

		ActivityRecord ar = a.log("admin", alertService, method, new Object[] { TOFactory.INSTANCE.loadTO("TYPE", 1),
		        false, "ack" });
		assertEquals("AlertManagement", ar.getModule());
		assertEquals("DismissAllObjectAlerts", ar.getAction());
		assertEquals("$localize(activity_record_all_alerts_on_object_acknowledged,\"TYPE:1\")$", ar.getDescription());
		assertNull(ar.getObject());
		assertEquals("admin", ar.getUserName());
	}

	@Test
	public void logCreateUserActivity(@Mocked TO<?> user) throws SecurityException, NoSuchMethodException {
		Activity a = new Activity();

		/*
		new Expectations() {
			{

				userService.getProperty(user, "userName");
				times = 1;
				result = "user";
			}
		};
		*/

		final Method method = UserManagementServiceImpl.class.getMethod("createUser",
				String.class, String.class, Object.class);

		ActivityRecord ar = a.log("admin", userService, method, new Object[] { "user" });
		assertEquals("UserManagement", ar.getModule());
		assertEquals("AddUser", ar.getAction());
		assertEquals("$localize(activity_record_new_user_added,\"user\")$", ar.getDescription());
		assertNull(ar.getObject());
		assertEquals("admin", ar.getUserName());
	}

	@Test
	public void logUpdateUserActivity(@Mocked TO<?> user) throws SecurityException, NoSuchMethodException {
		Activity a = new Activity();

		/*
		new Expectations() {
			{

				userService.getProperty(user, "userName");
				times = 1;
				result = "user";

			}
		};
		*/

		final Method method = UserManagementServiceImpl.class.getMethod("updateUser", TO.class, Map.class);

		final Map<String, Object> props = CollectionUtils.newMap();
		props.put("description", "new_description");
		ActivityRecord ar = a.log("admin", userService, method, new Object[] { user, props });
		assertEquals("UserManagement", ar.getModule());
		assertEquals("EditUser", ar.getAction());
		assertEquals("$localize(activity_record_user_profile_modified,\"" + user.getTypeName() + ":" + user.getID() + "\")$", ar.getDescription());
		assertNull(ar.getObject());
		assertEquals("admin", ar.getUserName());
	}

	@Test
	public void logSetUserPasswordActivity(@Mocked TO<?> user) throws SecurityException, NoSuchMethodException {
		Activity a = new Activity();

		final Method method = UserManagementServiceImpl.class.getMethod("setPassword", TO.class, String.class);

		ActivityRecord ar = a.log("admin", userService, method, new Object[]{user, "pass"});
		assertEquals("UserManagement", ar.getModule());
		assertEquals("ChangeUserPassword", ar.getAction());
		assertEquals("$localize(activity_record_user_password_changed,\"" + user.getTypeName() + ":" + user.getID() + "\")$", ar.getDescription());
		assertNull(ar.getObject());
		assertEquals("admin", ar.getUserName());
	}

	@Test
	public void logDeleteUserActivity(@Mocked TO<?> user) throws SecurityException, NoSuchMethodException {
		Activity a = new Activity();

		final Method method = UserManagementServiceImpl.class.getMethod("deleteUser", TO.class);

		ActivityRecord ar = a.log("admin", userService, method,  new Object[]{user});
		assertEquals("UserManagement", ar.getModule());
		assertEquals("DeleteUser", ar.getAction());
		assertEquals("$localize(activity_record_user_deleted,\"" + user.getTypeName() + ":" + user.getID() + "\")$", ar.getDescription());
		assertNull(ar.getObject());
		assertEquals("admin", ar.getUserName());
	}

	@Test(expected=PropertyAccessException.class)
	public void deleteUnExistingAlertTypeMvelExpression() {
		final Map<String, Object> map = new HashMap<String, Object>();
		map.put("params", new Object[] { "AT" });
		map.put("target", alertService);

		String expr = "@{target.getAlertType(params[0]).displayName}";

		new Expectations() {
			{
				alertService.getAlertType("AT");
				times = 1;
				result = null;
			}
		};

		TemplateRuntime.eval(expr, map);
	}

}
