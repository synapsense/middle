package com.synapsense.impl.nsvc;

import static org.junit.Assert.*;

import java.security.Security;
import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;

import org.jboss.sasl.JBossSaslProvider;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.dto.ValueTO;
import com.synapsense.service.Environment;
import com.synapsense.service.impl.nsvc.EventNotifier;
import com.synapsense.service.nsvc.Event;
import com.synapsense.service.nsvc.EventProcessor;
import com.synapsense.service.nsvc.NotificationService;
import com.synapsense.service.nsvc.events.ConfigurationCompleteEvent;
import com.synapsense.service.nsvc.events.ConfigurationStartedEvent;
import com.synapsense.transport.TransportManager;

//https://docs.jboss.org/author/display/AS71/EJB+invocations+from+a+remote+client+using+JNDI
public class RemoteRoundtripTest {
	
	private static EventNotifier en;
	private static NotificationService nsvc;
	private static EventCounter eventCounter;
	private static Environment env;
	
	static {
        Security.addProvider(new JBossSaslProvider());
    }

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		Properties environment = new Properties();
		environment.put(Context.URL_PKG_PREFIXES, "org.jboss.ejb.client.naming");
		//other properties are now stored in jboss-ejb-client.properties

		InitialContext ctx = new InitialContext(environment);
		en = (EventNotifier) ctx.lookup("ejb:es-ear/es-core//EventNotifier!com.synapsense.service.impl.nsvc.EventNotifier");
		
		environment.put("com.synapsense.transport",
				"com.synapsense.transport.rmi.AsyncRmiClientTransport");

		nsvc = TransportManager.getProxy(environment, 
				NotificationService.class).getService();
		
		eventCounter = new EventCounter();
		//nsvc.registerProcessor(eventCounter, null);
		
		env = (Environment) ctx.lookup("ejb:es-ear/es-core//Environment!com.synapsense.service.Environment");

	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Ignore
	public void test() {
		eventCounter.setCount(0);
		en.notify(new ConfigurationStartedEvent());
		en.notify(new ConfigurationCompleteEvent());
		assertEquals(2, eventCounter.getCount());
		nsvc.deregisterProcessor(eventCounter);
	}
	
	@Test
	public void setData() throws Exception {
		TO<?> objID = TOFactory.getInstance().loadTO("WSNSENSOR:75");
		ValueTO lastValue = new ValueTO("lastValue", 56.0d);
		ValueTO status = new ValueTO("status", 1);
		env.setAllPropertiesValues(objID, new ValueTO[] {lastValue, status});
	}

	static private class EventCounter implements EventProcessor {
		
		private int count;

		@Override
		public Event process(Event e) {
			System.out.println("Got event: "+e);
			count++;
			return e;
		}

		public int getCount() {
			return count;
		}

		public void setCount(int count) {
			this.count = count;
		}
		
	}
}
