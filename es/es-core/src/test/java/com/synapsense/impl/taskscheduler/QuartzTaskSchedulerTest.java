package com.synapsense.impl.taskscheduler;

import com.synapsense.impl.quartz.QuartzService;
import mockit.Deencapsulation;
import mockit.Expectations;
import mockit.Mocked;
import mockit.integration.junit4.JMockit;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.quartz.*;

import javax.ejb.EJBContext;
import java.util.HashMap;
import java.util.LinkedHashMap;

import static org.junit.Assert.assertTrue;

/**
 * @author : shabanov
 */
@RunWith(JMockit.class)
public class QuartzTaskSchedulerTest {

	@Mocked
	Scheduler scheduler;

	@Mocked
	QuartzService quartz;

	@Test
	public void workingWithEjbTask(@Mocked EJBContext ctx, @Mocked Configuration conf,
								   @Mocked JobDetail jobDetail) throws Exception {
		QuartzTaskSchedulerImpl impl = new QuartzTaskSchedulerImpl();
		impl.setCtx(ctx);
		Deencapsulation.setField(impl, "configuration", conf);
		new Expectations() {
			{
				quartz.getScheduler();
				result = scheduler;

				ctx.getCallerPrincipal().getName();
				result = "admin";

				conf.getTaskGroupByName("test").getPriority();
				result = 1;

				scheduler.scheduleJob(withInstanceOf(JobDetail.class), withInstanceOf(Trigger.class));

				scheduler.getJobDetail(new JobKey("test", "test"));
				result = jobDetail;

				jobDetail.getJobDataMap();
				// empty map
				result = new JobDataMap() {
					{
                        put(EJB3InvokerJob.EJB_ARGS_KEY, new HashMap());
					}
				};
			}
		};
		impl.setQuartzService(quartz);

		impl.addEjbTask("test", "test", "0 0/5 * * * ?", "jndi", "class", "method",
		        new LinkedHashMap<Class<?>, Object>());

		assertTrue(impl.getEjbTaskParams("test", "test").isEmpty());
	}
}
