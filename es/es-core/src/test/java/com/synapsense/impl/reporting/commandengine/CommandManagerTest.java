package com.synapsense.impl.reporting.commandengine;

import java.util.Collection;

import mockit.Delegate;
import mockit.Injectable;
import mockit.NonStrictExpectations;
import mockit.integration.junit4.JMockit;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.synapsense.impl.reporting.commandengine.monitoring.StatusMonitor;
import com.synapsense.service.reporting.dto.CommandStatus;

@RunWith(JMockit.class)
public class CommandManagerTest {

	@Injectable
	Command command;

	@Test
	public void testCreateCommandManager() {

		new NonStrictExpectations() {
			{
				command.getName();
				returns("Test Command");
				command.run(withInstanceOf(StatusMonitor.class));
			}
		};
		CommandManager commandManager = new DefaultCommandManager(2);
		commandManager.executeCommand(command);
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			Assert.fail(e.getMessage());
		}
		
	}

	@Test
	public void testListAllManagedTasks() {

		new NonStrictExpectations() {
			{
				command.getName();
				returns("Test Command");
				command.run(withInstanceOf(StatusMonitor.class));
			}
		};
		CommandManager commandManager = new DefaultCommandManager(2);
		commandManager.executeCommand(command);

		Collection<CommandProxy> commands = commandManager.getManagedCommands();

		Assert.assertEquals(1, commands.size());
	}

	@Test
	public void testMonitorCommandProxy() {
		
		new NonStrictExpectations() {
			{
				command.getName();
				minTimes = 0;
				returns("Test Command");
				command.run(withInstanceOf(StatusMonitor.class));
	            result = new Delegate() {
	                void run(StatusMonitor statusMonitor) throws Throwable
	                {
	                	Thread.sleep(100);
	                	statusMonitor.setStatus(CommandStatus.COMPLETED);
	                }
	             };
				
			}
		};
		CommandManager commandManager = new DefaultCommandManager(1);
		CommandProxy proxy = commandManager.executeCommand(command);
		CommandTestUtils.waitUntilFinished(proxy, commandManager);
		
	}
}
