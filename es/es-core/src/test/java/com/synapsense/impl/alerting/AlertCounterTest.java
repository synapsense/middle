package com.synapsense.impl.alerting;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.LinkedList;

import org.jmock.Expectations;
import org.jmock.Mockery;
import org.jmock.Sequence;
import org.jmock.integration.junit4.JUnit4Mockery;
import org.junit.Test;

import com.synapsense.dal.objectcache.AlertCounter;
import com.synapsense.dal.objectcache.CacheSvc;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.exception.EnvException;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.exception.PropertyNotFoundException;
import com.synapsense.exception.UnableToConvertPropertyException;

public class AlertCounterTest {

	@Test
	public void testAlertCounterInitSuccess() {
		Mockery context = new JUnit4Mockery();
		CacheSvc cacheMock = context.mock(CacheSvc.class);

		new AlertCounter(cacheMock, "numAlerts");
	}

	@Test
	public void testAlertCounterIncSingleObjectSuccess() throws ObjectNotFoundException, PropertyNotFoundException,
	        UnableToConvertPropertyException {
		Mockery context = new JUnit4Mockery();
		final CacheSvc cacheMock = context.mock(CacheSvc.class);

		final TO to = TOFactory.getInstance().loadTO("ALERT:1");
		context.checking(new Expectations() {
			{
				exactly(1).of(cacheMock).getPropertyValue(to, "numAlerts", Integer.class);
				will(returnValue(0));

				exactly(1).of(cacheMock).getRelatedObjects(to, null, false);
				will(returnValue(new LinkedList<TO<?>>()));

				exactly(1).of(cacheMock).setPropertyValue(to, "numAlerts", 1);
			}
		});

		AlertCounter ac = new AlertCounter(cacheMock, "numAlerts");
		ac.inc(to);
		context.assertIsSatisfied();
	}

	@Test
	public void testAlertCounterIncTwoRelatedObjectsSuccess() throws ObjectNotFoundException,
	        PropertyNotFoundException, UnableToConvertPropertyException {
		Mockery context = new JUnit4Mockery();
		final CacheSvc cacheMock = context.mock(CacheSvc.class);

		final TO to1 = TOFactory.getInstance().loadTO("RACK:1");
		final TO to2 = TOFactory.getInstance().loadTO("RACK:2");

		context.checking(new Expectations() {
			{

				exactly(1).of(cacheMock).getPropertyValue(to2, "numAlerts", Integer.class);
				will(returnValue(0));

				exactly(1).of(cacheMock).setPropertyValue(to2, "numAlerts", 1);

				exactly(1).of(cacheMock).getRelatedObjects(to2, null, false);
				will(returnValue(new LinkedList<TO<?>>(Arrays.<TO<?>>asList(to1))));

				exactly(1).of(cacheMock).getPropertyValue(to1, "numAlerts", Integer.class);
				will(returnValue(0));

				exactly(1).of(cacheMock).setPropertyValue(to1, "numAlerts", 1);

			}
		});

		AlertCounter ac = new AlertCounter(cacheMock, "numAlerts");
		ac.inc(to2);

		context.assertIsSatisfied();
	}

	@Test
	public void testAlertCounterMultipleIncOnRelatedObjectsSuccess() throws ObjectNotFoundException,
	        PropertyNotFoundException, UnableToConvertPropertyException {
		Mockery context = new JUnit4Mockery();
		final CacheSvc cacheMock = context.mock(CacheSvc.class);

		final TO to1 = TOFactory.getInstance().loadTO("RACK:1");
		final TO to2 = TOFactory.getInstance().loadTO("RACK:2");

		context.checking(new Expectations() {
			{

				exactly(3).of(cacheMock).getPropertyValue(to2, "numAlerts", Integer.class);
				will(returnValue(0));

				exactly(3).of(cacheMock).setPropertyValue(to2, "numAlerts", 1);

				exactly(3).of(cacheMock).getRelatedObjects(to2, null, false);
				will(returnValue(new LinkedList<TO<?>>(Arrays.<TO<?>>asList(to1))));

				exactly(3).of(cacheMock).getPropertyValue(to1, "numAlerts", Integer.class);
				will(returnValue(0));

				exactly(3).of(cacheMock).setPropertyValue(to1, "numAlerts", 1);

			}
		});

		AlertCounter ac = new AlertCounter(cacheMock, "numAlerts");
		ac.inc(to2);
		ac.inc(to2);
		ac.inc(to2);

		context.assertIsSatisfied();
	}

	@Test
	public void testAlertCounterDecSingleObjectNoAlertsSuccess() throws EnvException {
		Mockery context = new JUnit4Mockery();
		final CacheSvc cacheMock = context.mock(CacheSvc.class);

		final TO to = TOFactory.getInstance().loadTO("ALERT:1");
		AlertCounter ac = new AlertCounter(cacheMock, "numAlerts");

		assertEquals(0, ac.dec(to));

		context.assertIsSatisfied();
	}

	@Test
	public void testAlertCounterDecSingleObjectWithAlertsSuccess() throws EnvException {
		Mockery context = new JUnit4Mockery();
		final CacheSvc cacheMock = context.mock(CacheSvc.class);

		final TO to = TOFactory.getInstance().loadTO("RACK:1");
		AlertCounter ac = new AlertCounter(cacheMock, "numAlerts");

		final Sequence seq = context.sequence("seq");
		context.checking(new Expectations() {
			{

				exactly(1).of(cacheMock).getPropertyValue(to, "numAlerts", Integer.class);
				will(returnValue(0));
				inSequence(seq);

				exactly(1).of(cacheMock).setPropertyValue(to, "numAlerts", 1);
				inSequence(seq);

				exactly(1).of(cacheMock).getRelatedObjects(to, null, false);
				will(returnValue(new LinkedList<TO<?>>()));
				inSequence(seq);

				exactly(1).of(cacheMock).getPropertyValue(to, "numAlerts", Integer.class);
				will(returnValue(1));
				inSequence(seq);

				exactly(1).of(cacheMock).setPropertyValue(to, "numAlerts", 0);
				inSequence(seq);

				exactly(1).of(cacheMock).getRelatedObjects(to, null, false);
				will(returnValue(new LinkedList<TO<?>>()));
				inSequence(seq);

			}
		});

		ac.inc(to);

		assertEquals(0, ac.dec(to));

		context.assertIsSatisfied();
	}

	@Test
	public void testAlertCounterIncDecComplexRelations() throws EnvException {
		Mockery context = new JUnit4Mockery();
		final CacheSvc cacheMock = context.mock(CacheSvc.class);

		TOFactory.getInstance().loadTO("RACK:1");
		AlertCounter ac = new AlertCounter(cacheMock, "numAlerts");

		final TO to1 = TOFactory.getInstance().loadTO("RACK:1");
		final TO to2 = TOFactory.getInstance().loadTO("RACK:2");
		final TO to3 = TOFactory.getInstance().loadTO("RACK:3");
		final TO to4 = TOFactory.getInstance().loadTO("RACK:4");

		context.checking(new Expectations() {
			{

				exactly(1).of(cacheMock).getPropertyValue(to4, "numAlerts", Integer.class);
				will(returnValue(0));

				exactly(1).of(cacheMock).setPropertyValue(to4, "numAlerts", 1);

				exactly(1).of(cacheMock).getRelatedObjects(to4, null, false);
				will(returnValue(new LinkedList<TO<?>>(Arrays.<TO<?>>asList(to1, to2, to3))));

				exactly(1).of(cacheMock).getPropertyValue(to1, "numAlerts", Integer.class);
				will(returnValue(0));

				exactly(1).of(cacheMock).setPropertyValue(to1, "numAlerts", 1);

				exactly(1).of(cacheMock).getPropertyValue(to2, "numAlerts", Integer.class);
				will(returnValue(0));

				exactly(1).of(cacheMock).setPropertyValue(to2, "numAlerts", 1);

				exactly(1).of(cacheMock).getPropertyValue(to3, "numAlerts", Integer.class);
				will(returnValue(0));

				exactly(1).of(cacheMock).setPropertyValue(to3, "numAlerts", 1);

				// next inc() call
				exactly(1).of(cacheMock).getPropertyValue(to3, "numAlerts", Integer.class);
				will(returnValue(1));

				exactly(1).of(cacheMock).setPropertyValue(to3, "numAlerts", 2);

				exactly(1).of(cacheMock).getRelatedObjects(to3, null, false);
				will(returnValue(new LinkedList<TO<?>>(Arrays.<TO<?>>asList(to1))));

				exactly(1).of(cacheMock).getPropertyValue(to1, "numAlerts", Integer.class);
				will(returnValue(1));

				exactly(1).of(cacheMock).setPropertyValue(to1, "numAlerts", 2);
				// next inc() call
				exactly(1).of(cacheMock).getPropertyValue(to2, "numAlerts", Integer.class);
				will(returnValue(1));

				exactly(1).of(cacheMock).setPropertyValue(to2, "numAlerts", 2);

				exactly(1).of(cacheMock).getRelatedObjects(to2, null, false);
				will(returnValue(new LinkedList<TO<?>>(Arrays.<TO<?>>asList(to1))));

				exactly(1).of(cacheMock).getPropertyValue(to1, "numAlerts", Integer.class);
				will(returnValue(2));

				exactly(1).of(cacheMock).setPropertyValue(to1, "numAlerts", 3);
				// next inc() call
				exactly(1).of(cacheMock).getPropertyValue(to1, "numAlerts", Integer.class);
				will(returnValue(3));

				exactly(1).of(cacheMock).setPropertyValue(to1, "numAlerts", 4);

				exactly(1).of(cacheMock).getRelatedObjects(to1, null, false);
				will(returnValue(new LinkedList<TO<?>>()));
				// next dec() call
				exactly(1).of(cacheMock).getPropertyValue(to4, "numAlerts", Integer.class);
				will(returnValue(1));

				exactly(1).of(cacheMock).setPropertyValue(to4, "numAlerts", 0);

				exactly(1).of(cacheMock).getRelatedObjects(to4, null, false);
				will(returnValue(new LinkedList<TO<?>>(Arrays.<TO<?>>asList(to1, to2, to3))));

				exactly(1).of(cacheMock).getPropertyValue(to1, "numAlerts", Integer.class);
				will(returnValue(4));

				exactly(1).of(cacheMock).setPropertyValue(to1, "numAlerts", 3);

				exactly(1).of(cacheMock).getPropertyValue(to2, "numAlerts", Integer.class);
				will(returnValue(2));

				exactly(1).of(cacheMock).setPropertyValue(to2, "numAlerts", 1);

				exactly(1).of(cacheMock).getPropertyValue(to3, "numAlerts", Integer.class);
				will(returnValue(2));

				exactly(1).of(cacheMock).setPropertyValue(to3, "numAlerts", 1);

			}
		});

		ac.inc(to4);
		ac.inc(to3);
		ac.inc(to2);
		ac.inc(to1);

		ac.dec(to4);

		context.assertIsSatisfied();
	}

}
