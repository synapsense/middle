package com.synapsense.impl.reporting;

import com.synapsense.impl.reporting.utils.FileUtils;
import com.synapsense.service.Environment;
import com.synapsense.service.FileWriter;
import com.synapsense.service.reporting.management.ReportingManagementService;
import com.synapsense.service.reporting.management.ReportingSchedulingService;
import com.synapsense.util.JNDINames;
import com.synapsense.util.ServerLoginModule;
import org.junit.Assert;
import org.junit.BeforeClass;

import javax.naming.Context;
import javax.naming.InitialContext;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.Properties;

public final class TestUtils {

	// Test reports storage
	public static final String STORAGE_PATH = "reporting\\test\\resources";
	public static final String FILE_STORAGE = System.getenv().get("JBOSS_HOME") + "\\standalone\\configuration\\";
	private static InitialContext ctx;
	private static ReportingSchedulingService repScheduler;
	private static FileWriter fileWriter;
	public static ReportingManagementService repManager;
	private static Environment environment;
	
	public static final String TEST_REPORTS_PATH = STORAGE_PATH + "/fs";

	public static final String STORAGE_TEMPLATES = STORAGE_PATH + "\\templates";

	public static final String EMPTY_REPORT_NAME = "EmptyReport";
	public static final String EMPTY_REPORT_ROOT_PATH = STORAGE_TEMPLATES;
	public static final String EMPTY_REPORT_DESIGN_PATH = STORAGE_TEMPLATES + "\\" + EMPTY_REPORT_NAME + ".jrxml";
	public static final String EMPTY_REPORT_TMP_PATH = STORAGE_PATH + "\\tmp";

	public static final String CRAC_REPORT_NAME = "CracSupplyReturn";
	public static final String CRAC_REPORT_DESIGN_PATH = STORAGE_TEMPLATES + "/" + CRAC_REPORT_NAME + ".jrxml";

	public static final String SUPPLYRETURN_REPORT_NAME = "CracSupplyReturn";
	public static final String RACKOVERCOOLING_REPORT_NAME = "RackOverCooling";
	public static final String TEST_REPORT_NAME = "TestReport";

	public static final String TEMPLATE_EXT = ".jrxml";
	public static final String REPORT_EXT = ".jprint";

	public static final String DB_FOLDER = "./test/resources/testStorage/database/";
	public static final String DB_NAME = "testdb";


	public static Environment getEnvironment() {
		if (environment == null) {
			init();
		}
		return environment;
	}
	
	public static ReportingManagementService getReportingManagementService() {
		if (repManager == null) {
			init();
		}
		return repManager;
	}
	
	public static String getEmptyReportDesignPath() {
		URL url = TestUtils.class.getResource(EMPTY_REPORT_NAME + TEMPLATE_EXT);
		return url.getFile();
	}

	public static ReportingSchedulingService getReportingScheduler() {
		if (repScheduler == null) {
			init();
		}
		return repScheduler;
	}

	public static FileWriter getFileWriter() {
		if (fileWriter == null) {
			init();
		}
		return fileWriter;
	}
	
	public static void checkAndRemoveTestFiles(String[] paths) {
		for (String path : paths) {
			File file = new File(path);
			if (!file.exists()) {
				Assert.fail("File " + path + " not found in test repository");
				return;
			}

			if (file.isDirectory()) {
				if (!FileUtils.deleteDirectory(file)) {
					Assert.fail("Unable to delete directory " + path);
					return;
				}

			} else if (!file.delete()) {
				Assert.fail("Unable to delete file " + path);
			}
		}
	}

	public static String readTestFile(String fileName) {
		StringBuilder builder = new StringBuilder();
		try {
			BufferedReader reader = new BufferedReader(new FileReader(fileName));
			String line = null;
			while ((line = reader.readLine()) != null) {
				builder.append(line);
			}
			reader.close();
			builder.trimToSize();
		} catch (IOException e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
		return builder.toString();
	}
	
	@BeforeClass
	private static void init() {
		final String SERVER_NAME = "localhost";
		final String INITIAL_CONTEXT_FACTORY = "org.jboss.naming.remote.client.InitialContextFactory";
		final String PROVIDER_URL = "remote://" + SERVER_NAME + ":4447";

		Properties connectProperties = new Properties();
		connectProperties.put(Context.INITIAL_CONTEXT_FACTORY, INITIAL_CONTEXT_FACTORY);
		connectProperties.put(Context.PROVIDER_URL, PROVIDER_URL);

		connectProperties.put("jboss.naming.client.ejb.context", "true");
		connectProperties.put("jboss.naming.client.connect.options.org.xnio.Options.SASL_POLICY_NOPLAINTEXT", "false");

		connectProperties.put(Context.SECURITY_PRINCIPAL, "admin");
		connectProperties.put(Context.SECURITY_CREDENTIALS, "admin");
		connectProperties.put("SynapEnvRemote", "SynapServer/Environment/remote");

		connectProperties.put(Context.URL_PKG_PREFIXES, "org.jboss.naming.client:org.jnp.interfaces");

		try {
			ctx = new InitialContext(connectProperties);
			repScheduler = ServerLoginModule.getProxy(ctx, JNDINames.REPORTING_SCHEDULING_SERVICE_NAME, ReportingSchedulingService.class);
			repManager = ServerLoginModule.getProxy(ctx, JNDINames.REPORTING_MANAGEMENT_SERVICE_NAME, ReportingManagementService.class);
//			byte[] bytes = repManager.readFileBytes(path + "/quartz.properties", 0);
//			byte[] bytes = repManager.readFileBytes("snmp/snmpconf.xml", 0);
//			fileWriter = repManager.getFileWriter();
			fileWriter = ServerLoginModule.getProxy(ctx, JNDINames.FILE_WRITTER_NAME, FileWriter.class);
			environment = ServerLoginModule.getProxy(ctx, JNDINames.ENVIRONMENT, Environment.class);
//			fileWriter = ServerLoginModule.getService(ctx, "es-ear/es-core/FileWriterService!com.synapsense.service.FileWriter", FileWriter.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private TestUtils() {
		init();
	}

}
