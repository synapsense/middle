package com.synapsense.impl.alerting.macros;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Locale;

import mockit.Expectations;

import mockit.Mocked;
import mockit.integration.junit4.JMockit;
import org.junit.Test;

import com.synapsense.dto.Alert;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.service.Environment;
import com.synapsense.service.LocalizationService;
import com.synapsense.service.user.NonUserContext;

import org.junit.runner.RunWith;

@RunWith(JMockit.class)
public class RelationMacroTest {

	@Test
	public void testParentExpansion(@Mocked Environment env, @Mocked Alert alert, @Mocked LocalizationService ls) throws Exception {
		String inputString = "$parent(WSNNODE).location$";
		final TO<?> to = TOFactory.getInstance().loadTO("WSNSENSOR:23");
		final TO<?> parent = TOFactory.getInstance().loadTO("WSNNODE:1");
		MacroContext ctx = new MacroContext();
		ctx.env = env;
		ctx.alert = alert;
		ctx.localizationService = ls;
		ctx.usrCtx = new NonUserContext();

		new Expectations() {
			{
				alert.getHostTO();
				returns(to);

				Collection<TO<?>> related = new ArrayList<TO<?>>(1);
				related.add(parent);
				env.getRelatedObjects(to, "WSNNODE", false);
				returns(related);

				env.getPropertyValue(parent, "location", Object.class);
				returns("right here");
				
				ls.getPartialTranslation("right here", Locale.US);
				returns("right here");
			}
		};

		Macro macro = new RelationMacro();
		assertEquals("right here", macro.expandIn(inputString, ctx));
	}

	@Test
	public void testNoParentsExpansion(@Mocked Environment env, @Mocked Alert alert) throws Exception {
		String inputString = "$parent(WSNNODE).location$";
		final TO<?> to = TOFactory.getInstance().loadTO("WSNSENSOR:23");
		MacroContext ctx = new MacroContext();
		ctx.env = env;
		ctx.alert = alert;

		new Expectations() {
			{
				alert.getHostTO();
				returns(to);

				env.getRelatedObjects(to, "WSNNODE", false);
				returns(Collections.emptyList());
			}
		};

		Macro macro = new RelationMacro();
		assertEquals(Macro.DEFAULT_EXPANSION, macro.expandIn(inputString, ctx));
	}

	@Test
	public void testMultipleParentsExpansion(@Mocked Environment env, @Mocked Alert alert) throws Exception {
		String inputString = "$parent(WSNNODE).location$";

		final TO<?> sensor = TOFactory.getInstance().loadTO("WSNSENSOR:1");
		final TO<?> node1 = TOFactory.getInstance().loadTO("WSNNODE:2");
		final TO<?> node2 = TOFactory.getInstance().loadTO("WSNNODE:3");

		MacroContext ctx = new MacroContext();
		ctx.env = env;
		ctx.alert = alert;

		new Expectations() {
			{
				alert.getHostTO();
				returns(sensor);

				Collection<TO<?>> related = new ArrayList<TO<?>>(2) {
					{
						add(node1);
						add(node2);
					}
				};

				env.getRelatedObjects(sensor, "WSNNODE", false);
				returns(related);
			}
		};

		Macro macro = new RelationMacro();
		assertEquals(Macro.DEFAULT_EXPANSION, macro.expandIn(inputString, ctx));
	}

	@Test
	public void testChildExpansion(@Mocked Environment env, @Mocked Alert alert, @Mocked LocalizationService ls) throws Exception {
		String inputString = "$child(WSNSENSOR).location$";
		final TO<?> to = TOFactory.getInstance().loadTO("WSNNODE:1");
		final TO<?> child = TOFactory.getInstance().loadTO("WSNSENSOR:2");
		MacroContext ctx = new MacroContext();
		ctx.env = env;
		ctx.alert = alert;
		ctx.localizationService = ls;
		ctx.usrCtx = new NonUserContext();

		new Expectations() {
			{
				alert.getHostTO();
				returns(to);

				Collection<TO<?>> related = new ArrayList<TO<?>>(1);
				related.add(child);
				env.getRelatedObjects(to, "WSNSENSOR", true);
				returns(related);

				env.getPropertyValue(child, "location", Object.class);
				returns("right here");
				
				ls.getPartialTranslation("right here", Locale.US);
				returns("right here");
			}
		};

		Macro macro = new RelationMacro();
		assertEquals("right here", macro.expandIn(inputString, ctx));
	}

	@Test
	public void testNoChildrenExpansion(@Mocked Environment env, @Mocked Alert alert) throws Exception {
		String inputString = "$child(WSNSENSOR).location$";
		final TO<?> to = TOFactory.getInstance().loadTO("WSNNODE:1");
		MacroContext ctx = new MacroContext();
		ctx.env = env;
		ctx.alert = alert;

		new Expectations() {
			{
				alert.getHostTO();
				returns(to);

				env.getRelatedObjects(to, "WSNSENSOR", true);
				returns(Collections.emptyList());
			}
		};

		Macro macro = new RelationMacro();
		assertEquals(Macro.DEFAULT_EXPANSION, macro.expandIn(inputString, ctx));
	}

	@Test
	public void testMultipleChildrenExpansion(@Mocked Environment env, @Mocked Alert alert) throws Exception {
		String inputString = "$child(WSNSENSOR).location$";

		final TO<?> node = TOFactory.getInstance().loadTO("WSNNODE:1");
		final TO<?> sensor1 = TOFactory.getInstance().loadTO("WSNSENSOR:23");
		final TO<?> sensor2 = TOFactory.getInstance().loadTO("WSNSENSOR:24");

		MacroContext ctx = new MacroContext();
		ctx.env = env;
		ctx.alert = alert;

		new Expectations() {
			{
				alert.getHostTO();
				returns(node);

				Collection<TO<?>> related = new ArrayList<TO<?>>(2) {
					{
						add(sensor1);
						add(sensor2);
					}
				};

				env.getRelatedObjects(node, "WSNSENSOR", true);
				returns(related);
			}
		};

		Macro macro = new RelationMacro();
		assertEquals(Macro.DEFAULT_EXPANSION, macro.expandIn(inputString, ctx));
	}

}
