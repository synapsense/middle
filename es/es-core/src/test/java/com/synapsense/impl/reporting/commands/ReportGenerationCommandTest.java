package com.synapsense.impl.reporting.commands;

import static com.synapsense.impl.reporting.TestUtils.EMPTY_REPORT_NAME;
import static com.synapsense.impl.reporting.TestUtils.getEmptyReportDesignPath;

import java.io.File;
import java.util.Date;

import mockit.Expectations;
import mockit.Mocked;
import mockit.integration.junit4.JMockit;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.synapsense.exception.ObjectExistsException;
import com.synapsense.impl.reporting.TestUtils;
import com.synapsense.impl.reporting.commandengine.monitoring.DefaultStatusMonitor;
import com.synapsense.impl.reporting.commandengine.monitoring.StatusMonitor;
import com.synapsense.impl.reporting.dao.ReportDAO;
import com.synapsense.impl.reporting.dao.ReportTemplateDAO;
import com.synapsense.impl.reporting.model.JSReport;
import com.synapsense.service.reporting.dto.CommandStatus;
import com.synapsense.service.reporting.dto.ReportInfo;
import com.synapsense.service.reporting.dto.ReportTaskInfo;
import com.synapsense.service.reporting.exception.DAOReportingException;

@RunWith(JMockit.class)
public class ReportGenerationCommandTest {

	private static final Log log = LogFactory.getLog(ReportGenerationCommandTest.class);
	
	@Mocked
	ReportTemplateDAO rtDao;
	
	@Mocked
	ReportDAO rDao;
	
	@Before
	public void setUp() {
	}

	@Test//(timeout = 15000)
	public void testSuccessfullyRunReportGenerationCommand() {
		// Creating Mock object for DAO to check if the command saves the result
		// to DAO
		
		File emptyDesign = new File(getEmptyReportDesignPath());
		final String reportDesign = TestUtils.readTestFile(emptyDesign.getAbsolutePath());
		
		try {
			new Expectations() {
				{
					rtDao.getReportTemplateDesign(EMPTY_REPORT_NAME);
					returns(reportDesign);
					rDao.saveReport(withInstanceOf(ReportInfo.class), withInstanceOf(JSReport.class), EMPTY_REPORT_NAME);
					
				}
			};
		} catch (DAOReportingException | ObjectExistsException e) {
			Assert.fail(e.getMessage());
		}
		
		ReportTaskInfo taskInfo = ReportTaskInfo.newInstance(EMPTY_REPORT_NAME, "testTask");
		ReportInfo reportInfo = new ReportInfo(taskInfo.getName(), new Date().getTime());
		final ReportGenerationCommand command = CommandFactory.newReportGenerationCommand(taskInfo, reportInfo, rtDao, rDao);


		StatusMonitor monitor = new DefaultStatusMonitor() {
			public void setStatus(CommandStatus status) {
				super.setStatus(status);
				log.debug(command.getName() + " status " + status + " - " + status.getStatusMessage());
			}

			public void setProgress(double progress) {
				super.setProgress(progress);
				log.debug(command.getName() + " progress " + progress);
			}
		};

		command.run(monitor);

		Assert.assertEquals("Status must be COMPLETED", CommandStatus.COMPLETED, monitor.getStatus());
		Assert.assertEquals("Progress must be 1.0", 1.0, monitor.getProgress(), 0.0);
	}
	
}
