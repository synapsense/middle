package com.synapsense.impl.reporting.lang;

import org.antlr.runtime.ANTLRStringStream;
import org.antlr.runtime.CommonTokenStream;
import org.antlr.runtime.RecognitionException;

import com.synapsense.impl.reporting.lang.sdxLexer;
import com.synapsense.impl.reporting.lang.sdxParser;
import com.synapsense.service.reporting.DimensionalQuery;

public class ParserTest {

	public static void main(String[] args) {
		try {
			sdxLexer lexer = new sdxLexer(
					new ANTLRStringStream(
							"SELECT MAX [Object].[ROOM].[&Room1].[RACK].[&Rack1].[SERVER].members ON ROWS, " +
							"{([Time].[Year].[&2012].[Month].[&6], [Measures].[Name].[&cTop]), " +
							"([Time].[Year].[&2012].[Month].[&7], [Measures].[Name].[&cBot])} ON COLUMNS " +
							"WHERE [Object].[DC].[&DC1]"));

			CommonTokenStream tokens = new CommonTokenStream(lexer);
			sdxParser parser = new sdxParser(tokens);
			parser.sdx_statement();

			DimensionalQuery query = parser.getDimensionalQuery();
			System.out.println(query);
		} catch (RecognitionException e) {
			System.err.println(e.getMessage());
			System.err.println("JOPA");
		}

	}
}
