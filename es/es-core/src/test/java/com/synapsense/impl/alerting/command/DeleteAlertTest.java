package com.synapsense.impl.alerting.command;

import static org.junit.Assert.assertNotNull;

import java.util.Collection;
import java.util.List;

import mockit.Expectations;

import mockit.Mocked;
import mockit.integration.junit4.JMockit;
import org.junit.Test;

import com.synapsense.commandprocessor.api.Command;
import com.synapsense.dto.Alert;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.EnvException;
import com.synapsense.impl.alerting.Cache;
import com.synapsense.service.Environment;
import com.synapsense.util.CollectionUtils;
import org.junit.runner.RunWith;

@RunWith(JMockit.class)
public class DeleteAlertTest {

	@Test
	public void instantiateCommand(@Mocked Environment env, @Mocked Cache<Alert, TO<?>> alertStore, @Mocked Collection<TO<?>> alerts,
								   @Mocked TO<?> historyHolder) {
		Command deleteAlert = new DeleteAlert(env, alertStore, alerts);
		assertNotNull(deleteAlert);
	}

	@Test
	public void deleteOneExistingAlert(@Mocked Environment env, @Mocked Cache<Alert, TO<?>> alertStore,
									   @Mocked TO<?> historyHolder) throws EnvException {

		final TO<?> alertTypeRef = TOFactory.INSTANCE.loadTO("ALERT_TYPE", 2);
		final Collection<TO<?>> alertTypes = CollectionUtils.newList();
		alertTypes.add(alertTypeRef);

		Collection<TO<?>> alerts = CollectionUtils.newSet();
		final TO<?> alert = TOFactory.INSTANCE.loadTO("ALERT", 1);
		alerts.add(alert);

		final TO<?> host = TOFactory.INSTANCE.loadTO("HOST", 3);

		Command deleteAlert = new DeleteAlert(env, alertStore, alerts);

		new Expectations() {
			{
				env.exists(alert);
				times = 1;
				returns(true);

				List<ValueTO> returned = CollectionUtils.newList();
				returned.add(new ValueTO("name", "alert"));
				returned.add(new ValueTO("time", 1L));
				returned.add(new ValueTO("type", alertTypeRef));
				returned.add(new ValueTO("status", 1));
				returned.add(new ValueTO("message", "message"));
				returned.add(new ValueTO("hostTO", host));

				env.deleteObject(alert);
				times = 1;

				alertStore.remove(alert);
				times = 1;
				returns(true);

			}
		};

		deleteAlert.execute();
	}

	@Test
	public void deleteOneUnExistingAlert(@Mocked Environment env, @Mocked Cache<Alert, TO<?>> alertStore,
										 @Mocked TO<?> historyHolder) throws EnvException {

		Collection<TO<?>> alerts = CollectionUtils.newSet();
		final TO<?> alert = TOFactory.INSTANCE.loadTO("ALERT", 1);
		alerts.add(alert);

		Command deleteAlert = new DeleteAlert(env, alertStore, alerts);

		new Expectations() {
			{
				env.exists(alert);
				times = 1;
				returns(false);
			}
		};

		deleteAlert.execute();
	}

	@Test
	public void deleteMultipleExistingAlerts(@Mocked Environment env, @Mocked Cache<Alert, TO<?>> alertStore,
											 @Mocked TO<?> historyHolder) throws EnvException {

		final TO<?> alertTypeRef = TOFactory.INSTANCE.loadTO("ALERT_TYPE", 2);
		final Collection<TO<?>> alertTypes = CollectionUtils.newList();
		alertTypes.add(alertTypeRef);

		Collection<TO<?>> alerts = CollectionUtils.newList();
		final TO<?> alert1 = TOFactory.INSTANCE.loadTO("ALERT", 1);
		final TO<?> alert2 = TOFactory.INSTANCE.loadTO("ALERT", 2);
		alerts.add(alert1);
		alerts.add(alert2);

		final TO<?> host = TOFactory.INSTANCE.loadTO("HOST", 3);

		Command deleteAlert = new DeleteAlert(env, alertStore, alerts);

		new Expectations() {
			{
				env.exists(alert1);
				times = 1;
				returns(true);

				env.deleteObject(alert1);
				times = 1;

				alertStore.remove(alert1);
				times = 1;
				returns(true);

				// next alert in collection
				env.exists(alert2);
				times = 1;
				returns(true);

				env.deleteObject(alert2);
				times = 1;

				alertStore.remove(alert2);
				times = 1;
				returns(true);

			}
		};

		deleteAlert.execute();
	}

}
