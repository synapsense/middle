package com.synapsense.impl.alerting.macros;

import static org.junit.Assert.assertEquals;
import mockit.Expectations;

import mockit.Mocked;
import mockit.integration.junit4.JMockit;
import org.junit.Test;

import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.service.Environment;
import org.junit.runner.RunWith;

@RunWith(JMockit.class)
public class EnvPropertyMacroTest {

	@Test
	public void testValueExpansion(@Mocked Environment env) throws Exception {
		String inputString = "$MY_TYPE:23.refSensor.lastValue$";
		final TO<?> to = TOFactory.getInstance().loadTO("MY_TYPE:23");
		MacroContext ctx = new MacroContext();
		ctx.env = env;

		new Expectations() {
			{
				env.getPropertyValue(to, "refSensor", Object.class);
				returns(TOFactory.EMPTY_TO);

				env.getPropertyValue(TOFactory.EMPTY_TO, "lastValue", Object.class);
				returns(0.3);
			}
		};

		Macro macro = new EnvPropertyMacro();
		assertEquals("0.3", macro.expandIn(inputString, ctx));
	}

	@Test
	public void testConvExpansion(@Mocked Environment env) throws Exception {
		String inputString = "$MY_TYPE:23.refSensor.lastValue{some dim}$";
		final TO<?> to = TOFactory.getInstance().loadTO("MY_TYPE:23");
		MacroContext ctx = new MacroContext();
		ctx.env = env;

		new Expectations() {
			{
				// it doesn't matter which TO is used as a referenced object,
				// let's use EMPTY one
				env.getPropertyValue(to, "refSensor", Object.class);
				returns(TOFactory.EMPTY_TO);

				env.getPropertyValue(TOFactory.EMPTY_TO, "lastValue", Object.class);
				returns(0.3);
			}
		};

		Macro macro = new EnvPropertyMacro();
		assertEquals("$conv(0.3,some dim)$", macro.expandIn(inputString, ctx));
	}

	@Test
	public void testObjectLinkIsNullExpansionToNA(@Mocked Environment env) throws Exception {
		String inputString = "$MY_TYPE:23.refSensor.lastValue$";
		final TO<?> to = TOFactory.getInstance().loadTO("MY_TYPE:23");
		MacroContext ctx = new MacroContext();
		ctx.env = env;

		new Expectations() {
			{
				onInstance(env).getPropertyValue(to, "refSensor", Object.class);
				returns(null);
			}
		};

		// refSensor is null
		Macro macro = new EnvPropertyMacro();
		assertEquals("N/A", macro.expandIn(inputString, ctx));

	}

	@Test
	public void testSecondObjectLinkIsNullExpansionToNA(@Mocked Environment env) throws Exception {
		String inputString = "$MY_TYPE:23.refRack.refSensor.lastValue$";
		final TO<?> to = TOFactory.getInstance().loadTO("MY_TYPE:23");
		final TO<?> refRack = TOFactory.getInstance().loadTO("RACK:1");
		MacroContext ctx = new MacroContext();
		ctx.env = env;

		new Expectations() {
			{
				onInstance(env).getPropertyValue(to, "refRack", Object.class);
				returns(refRack);
				// second reference is null
				onInstance(env).getPropertyValue(refRack, "refSensor", Object.class);
				returns(null);
			}
		};

		Macro macro = new EnvPropertyMacro();
		assertEquals("N/A", macro.expandIn(inputString, ctx));
	}

}
