package com.synapsense.impl.performer.audio;

import com.synapsense.dto.Message;
import com.synapsense.impl.performer.snmp.SNMPPerformerException;
import com.synapsense.service.impl.notification.NotificationException;
import mockit.Mocked;
import mockit.NonStrictExpectations;
import mockit.integration.junit4.JMockit;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(JMockit.class)
public class AudioVisualPerformerTest {
	@Mocked
	SignalSender sender;
	@Mocked
	Message metadata;
	
	AudioVisualPerformer performer=new AudioVisualPerformer();
	
	@Test
	public void perfomTest() throws SNMPPerformerException, NotificationException {
		final SignalSenders senders = new SignalSenders();

		new NonStrictExpectations(){
			{	
				metadata.getHeader();
				returns("");
				senders.getSignalSender((String) any);
				sender.send(metadata);
			}
		};
		performer.send(metadata);
	
	}

}
