package com.synapsense.impl.alerting.macros;

import static org.junit.Assert.assertEquals;

import java.text.SimpleDateFormat;
import java.util.Date;

import mockit.Expectations;

import mockit.Mocked;
import mockit.integration.junit4.JMockit;
import org.junit.Test;

import com.synapsense.dto.Alert;
import com.synapsense.service.Environment;
import com.synapsense.service.user.UserContext;

import org.junit.runner.RunWith;

@RunWith(JMockit.class)
public class TimestampMacroTest {

	@Test
	public void testValueExpansion(@Mocked Environment env, @Mocked Alert alert, @Mocked UserContext uctx) throws Exception {
		String inputString = "$timestamp$";
		
		MacroContext ctx = new MacroContext();
		ctx.props.put(MessageTierMacro.MSG_TIER, null);
		ctx.props.put(MessageNumMacro.MSG_NUM, null);
		ctx.env = env;
		ctx.alert = alert;
		ctx.usrCtx = uctx;
		
		final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss z");
		final Date alertGenTime = new Date();
		new Expectations() {
			{
				alert.getAlertTime();
				returns(alertGenTime.getTime());

				uctx.getDateFormat();
				returns(sdf);
			}
		};

		Macro macro = new TimestampMacro();
		
		assertEquals(sdf.format(alertGenTime), macro.expandIn(inputString, ctx));
	}

}
