package com.synapsense.impl.taskscheduler.tasks;

import com.synapsense.shared.NodeStatus;
import java.util.ArrayList;
import java.util.Calendar;

import com.synapsense.service.impl.dao.NetworkStatisticDAOIF;
import mockit.Mocked;

import mockit.StrictExpectations;
import mockit.integration.junit4.JMockit;
import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.junit.Ignore;
import org.junit.Test;

import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;
import com.synapsense.service.Environment;
import com.synapsense.service.impl.messages.base.StatisticData;
import org.junit.runner.RunWith;

@RunWith(JMockit.class)
public class BatteryTaskTest {

	@Mocked
	private TO node;
	@Mocked
	private Environment env;
	@Mocked
	private NetworkStatisticDAOIF<Integer, StatisticData> netStatDAO;

	@Test
	@Ignore
	// The cascading issue, see https://rtc-prod.panduit.com/ccm/resource/itemName/com.ibm.team.workitem.WorkItem/31794
	public void basicNodeOperatedZeroRadioTimeCalc() throws Exception {
		BatteryTaskImpl task = new BatteryTaskImpl();

		task.setEnv(env);
		task.setNetStatDAO(netStatDAO);

		final int daysOperated = 1;
		final double initialCapacity = 1000.0;
		final String nodeSampling = "5 min";
		final double deviationMagnitude = 0.01;

		new StrictExpectations() {
			{
				env.getObjectsByType("WSNNODE");
				times = 1;
				returns(new ArrayList<TO<?>>(1) {
					{
						add(node);
					}
				});

				env.getPropertyValue(node, "batteryOperated", Integer.class);
				times = 1;
				returns(1);

                env.getPropertyValue(node, "platformId", Integer.class);
                times = 1;
                returns(1);

				env.getPropertyValue(node, "batteryCapacity", ValueTO.class);
				times = 1;

				Calendar c = Calendar.getInstance();
				c.add(Calendar.DATE, -daysOperated);
				returns(new ValueTO("batteryCapacity", initialCapacity, c.getTime().getTime()));

				env.getPropertyValue(node, "platformId", Integer.class);
				times = 1;
				returns(1);

				env.getPropertyValue(node, "period", String.class);
				times = 1;
				returns(nodeSampling);

				// don't expose calc formula , just catch violations of it
				double usedPower = 0.96;
				
				final double expected = initialCapacity - usedPower;
				env.setPropertyValue(node, "batteryCapacity", withArgThat(new BaseMatcher<Double>() {
					@Override
					public boolean matches(Object setVal) {
						return Math.abs(expected - (Double) setVal) < deviationMagnitude;
					}

					@Override
					public void describeTo(Description arg0) {
					}
				}));
				times = 1;

                env.getPropertyValue(node, "status", Integer.class);
				times = 1;
				returns(NodeStatus.OK);

                env.setPropertyValue(node, "batteryStatus", 0);
                times = 1;
            }
		};

		task.execute();
	}

	@Test
	@Ignore
	// The cascading issue, see https://rtc-prod.panduit.com/ccm/resource/itemName/com.ibm.team.workitem.WorkItem/31794
	public void thermaNodeDxZeroRadioTimeCalc() throws Exception {
		BatteryTaskImpl task = new BatteryTaskImpl();

		task.setEnv(env);
		task.setNetStatDAO(netStatDAO);

		final int daysOperated = 1;
		final double initialCapacity = 1000.0;
		final String nodeSampling = "5 min";
		final double deviationMagnitude = 0.01;

		new StrictExpectations() {
			{
				env.getObjectsByType("WSNNODE");
				times = 1;
				returns(new ArrayList<TO<?>>(1) {
					{
						add(node);
					}
				});

				env.getPropertyValue(node, "batteryOperated", Integer.class);
				times = 1;
				returns(1);

                env.getPropertyValue(node, "platformId", Integer.class);
                times = 1;
                returns(20);

                env.getPropertyValue(node, "batteryCapacity", ValueTO.class);
				times = 1;

				Calendar c = Calendar.getInstance();
				c.add(Calendar.DATE, -daysOperated);
				returns(new ValueTO("batteryCapacity", initialCapacity, c.getTime().getTime()));

				env.getPropertyValue(node, "platformId", Integer.class);
				times = 1;
				returns(20);

				env.getPropertyValue(node, "period", String.class);
				times = 1;
				returns(nodeSampling);

				// don't expose calc formula , just catch violations of it 
				double usedPower = 1.1098;
				final double expected = initialCapacity - usedPower;

				env.setPropertyValue(node, "batteryCapacity", withArgThat(new BaseMatcher<Double>() {
					@Override
					public boolean matches(Object setVal) {
						return Math.abs(expected - (Double) setVal) < deviationMagnitude;
					}

					@Override
					public void describeTo(Description arg0) {
					}
				}));
				times = 1;

				env.getPropertyValue(node, "status", Integer.class);
				times = 1;
				returns(NodeStatus.OK);

                env.setPropertyValue(node, "batteryStatus", 0);
                times = 1;
			}
		};

		task.execute();
	}

    // FURTHER TEST CASES:
    // TODO: bongoOkToOk
    // TODO: bongoNotStable
    // TODO: bongoOkToLow
    // TODO: bongoLowToCritical
    // TODO: bongoCriticalToOk
    // TODO: bongoLowToOk
}
