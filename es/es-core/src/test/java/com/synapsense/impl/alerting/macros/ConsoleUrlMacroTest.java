package com.synapsense.impl.alerting.macros;

import static org.junit.Assert.assertEquals;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Properties;

import mockit.Expectations;

import mockit.Mocked;
import mockit.integration.junit4.JMockit;
import org.junit.Test;

import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.dto.ValueTO;
import com.synapsense.service.Environment;
import org.junit.runner.RunWith;

@RunWith(JMockit.class)
public class ConsoleUrlMacroTest {

	private static final String OBJ_TYPE = "CONFIGURATION_DATA";
	private static final ValueTO[] OBJ_FILTER = new ValueTO[] { new ValueTO("name", "WebConsole") };
	private static final String CONF_PROP_NAME = "config";
	private static final String URL_PROP_NAME = "console_url";

	@Test
	public void testValueExpansion(@Mocked Environment env) throws Exception {
		String inputString = "$console_url$";
		final TO<?> to = TOFactory.getInstance().loadTO("MY_TYPE:23");
		MacroContext ctx = new MacroContext();
		ctx.env = env;
		Properties props = new Properties();
		props.setProperty(URL_PROP_NAME, "http://localhost/synapsense/console.html");
		final StringWriter sw = new StringWriter();
		props.store(sw, null);

		new Expectations() {
			{
				Collection<TO<?>> res = new ArrayList<TO<?>>(1);
				res.add(TOFactory.EMPTY_TO);
				env.getObjects(OBJ_TYPE, OBJ_FILTER);
				returns(res);

				env.getPropertyValue(TOFactory.EMPTY_TO, CONF_PROP_NAME, String.class);
				returns(sw.toString());
			}
		};

		Macro macro = new ConsoleUrlMacro();
		assertEquals("http://localhost/synapsense/console.html", macro.expandIn(inputString, ctx));
	}

}
