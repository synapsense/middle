package com.synapsense.impl.alerting.macros;

import static org.junit.Assert.assertEquals;

import java.util.Locale;

import mockit.Expectations;

import mockit.Mocked;
import mockit.integration.junit4.JMockit;
import org.junit.Test;

import com.synapsense.service.LocalizationService;
import com.synapsense.service.user.UserContext;

import org.junit.runner.RunWith;

@RunWith(JMockit.class)
public class LocalizeMacroTest {

	@Test
	public void testMacroWithParams(@Mocked UserContext usrCtx, @Mocked LocalizationService ls) throws Exception {
		String inputString = "$localize(alert_message_out_of_recommended_range,\"Rear Exhaust w/ Subfloor\",\"[62.85,62.85,62.86]\")$";
		MacroContext ctx = new MacroContext();
		ctx.usrCtx = usrCtx;
		ctx.localizationService = ls;

		new Expectations() {
			{
				usrCtx.getLocale();
				returns(Locale.US);

				ls.getTranslation("Rear Exhaust w/ Subfloor", Locale.US);
				returns("Rear Exhaust w/ Subfloor");

				ls.getTranslation("[62.85,62.85,62.86]", Locale.US);
				returns("[62.85,62.85,62.86]");
				
				ls.getString("alert_message_out_of_recommended_range", Locale.US);
				returns("%1$s intake temp is Out of recommended range. Data: %2$s");
			}
		};

		Macro macro = new LocalizeMacro();
		assertEquals("Rear Exhaust w/ Subfloor intake temp is Out of recommended range. Data: [62.85,62.85,62.86]",
		        macro.expandIn(inputString, ctx));
	}

	@Test
	public void testMacroWithoutParams(@Mocked UserContext usrCtx, @Mocked LocalizationService ls) throws Exception {
		String inputString = "$localize(alert_message_property_value_is_failed_to_get)$";
		MacroContext ctx = new MacroContext();
		ctx.usrCtx = usrCtx;
		ctx.localizationService = ls;

		new Expectations() {
			{
				usrCtx.getLocale();
				returns(Locale.US);

				ls.getString("alert_message_property_value_is_failed_to_get", Locale.US);
				returns("Property value is failed to get");
			}
		};

		Macro macro = new LocalizeMacro();
		assertEquals("Property value is failed to get", macro.expandIn(inputString, ctx));
	}

	@Test
	public void testMarcoCombination(@Mocked UserContext usrCtx, @Mocked LocalizationService ls) throws Exception {
		String inputString = "$localize(alert_message_property_value_is_failed_to_get)$. $localize(alert_message_out_of_recommended_range,\"Rear Exhaust w/ Subfloor\",\"[62.85,62.85,62.86]\")$";
		MacroContext ctx = new MacroContext();
		ctx.usrCtx = usrCtx;
		ctx.localizationService = ls;

		new Expectations() {
			{
				usrCtx.getLocale();
				returns(Locale.US);

				ls.getString("alert_message_property_value_is_failed_to_get", Locale.US);
				returns("Property value is failed to get");

				usrCtx.getLocale();
				returns(Locale.US);

				ls.getTranslation("Rear Exhaust w/ Subfloor", Locale.US);
				returns("Rear Exhaust w/ Subfloor");

				ls.getTranslation("[62.85,62.85,62.86]", Locale.US);
				returns("[62.85,62.85,62.86]");

				ls.getString("alert_message_out_of_recommended_range", Locale.US);
				returns("%1$s intake temp is Out of recommended range. Data: %2$s");
			}
		};

		Macro macro = new LocalizeMacro();
		assertEquals(
		        "Property value is failed to get. Rear Exhaust w/ Subfloor intake temp is Out of recommended range. Data: [62.85,62.85,62.86]",
		        macro.expandIn(inputString, ctx));
	}
}
