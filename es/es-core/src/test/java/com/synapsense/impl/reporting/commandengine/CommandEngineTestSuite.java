package com.synapsense.impl.reporting.commandengine;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ CommandManagerTest.class, CommandStateMachineTest.class, CompositeCommandTest.class })
public class CommandEngineTestSuite {

}
