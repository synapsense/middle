package com.synapsense.impl.reporting.commandengine;

import junit.framework.Assert;
import mockit.Expectations;
import mockit.Injectable;
import mockit.integration.junit4.JMockit;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.synapsense.impl.reporting.commandengine.monitoring.DefaultStatusMonitor;
import com.synapsense.impl.reporting.commandengine.monitoring.StatusMonitor;
import com.synapsense.service.reporting.dto.CommandStatus;

@RunWith(JMockit.class)
public class CompositeCommandTest {

	private static final Log log = LogFactory.getLog(CompositeCommandTest.class);

	
	@Injectable
	Command command1;
	@Injectable
	Command command2;

	@Test
	public void testCompositeCommandCreation() {
		CompositeCommand compCommand = new CompositeCommand();
		Assert.assertEquals("Command number in a composite command", 0, compCommand.getCommandsSize());
	}
	
	@Test
	public void testSuccessfulCompositeCommandRun() {

		new Expectations() {
			{
				
				command1.getName();
				minTimes = 0;
				result = "Command1";

				command1.run(withInstanceOf(StatusMonitor.class));
				
				command2.getName();
				minTimes = 0;
				result = "Command2";
				
				command2.run(withInstanceOf(StatusMonitor.class));
			}
		};

		CompositeCommand command = new CompositeCommand();
		command.addCommand(command1);
		command.addCommand(command2);
		command.run(new DefaultStatusMonitor() {
			public void setStatus(CommandStatus status) {
				super.setStatus(status);
				log.debug("Composite command status " + status + " - " + status.getStatusMessage());
			}

			public void setProgress(double progress) {
				super.setProgress(progress);
				log.debug("Composite command progress " + progress);
			}
		});
	}

	@Test
	public void testCompositeCommandRollback() {
	
		new Expectations() {
			{
				
				command1.getName();
				minTimes = 0;
				result = "Command1";
				
				command1.run(withInstanceOf(StatusMonitor.class));
				
				command2.getName();
				minTimes = 0;
				result = "Command2";
				
				command2.run(withInstanceOf(StatusMonitor.class));
				
				command2.rollback();
				
				command1.rollback();
				
			}
		};

		CompositeCommand command = new CompositeCommand();
		command.addCommand(command1);
		command.addCommand(command2);
		command.run(new DefaultStatusMonitor());
		command.rollback();
	}
}
