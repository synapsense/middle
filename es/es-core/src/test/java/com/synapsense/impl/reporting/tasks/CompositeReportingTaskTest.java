package com.synapsense.impl.reporting.tasks;

import junit.framework.Assert;
import mockit.Injectable;
import mockit.Mocked;
import mockit.StrictExpectations;
import mockit.Tested;

import mockit.integration.junit4.JMockit;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(JMockit.class)
public class CompositeReportingTaskTest {

	private static final Log log = LogFactory.getLog(CompositeReportingTaskTest.class);
	
	@Tested
	CompositeReportingTask task;
	@Injectable
	ReportingTask task1;
	@Injectable
	ReportingTask task2;
	
	@Test
	public void testCompositeReportingTaskCreation() {
		CompositeReportingTask compositeTask = new CompositeReportingTask();
		Assert.assertEquals("Tasks number in a composite task", 0, compositeTask.getTasksSize());
		Assert.assertEquals("Composite Reporting Task Name", "CompositeReportingTask", task.getName());	
		Assert.assertEquals("Tasks number in a composite task", 0, compositeTask.getTasks().size());
	}
	
	@Test
	public void testSuccessfulCompositeReportingTaskRun() {

		new StrictExpectations() {
			{
				
				task1.getName();
				minTimes = 0;
				result = "GetValueIdsTask";

				task1.execute();

				task1.getName();
				minTimes = 0;
				result = "GetEnvDataTask";
				
				task2.execute();
				
			}
		};

		task = new CompositeReportingTask();
		task.addTask(task1);
		task.addTask(task2);
		task.execute();
		
	}

}
