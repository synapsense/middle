package com.synapsense.impl.alerting.mapping;

import com.synapsense.dto.Alert;
import com.synapsense.dto.AlertStatus;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.dto.ValueTO;
import com.synapsense.impl.utils.EnvironmentUtils;
import com.synapsense.service.Environment;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import mockit.Injectable;
import mockit.Mocked;
import mockit.StrictExpectations;
import mockit.integration.junit4.JMockit;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

@RunWith(JMockit.class)
public class AlertToPropertiesTest {

	@Mocked private Environment env;
	@Mocked private Alert alert;
	@Mocked private TO<?> alertTypeRef;

	@Test
	public void convertProperlyConfiguredAlert() {
		AlertToProperties converter = new AlertToProperties(env);

		new StrictExpectations() {
			{
				alert.getName();
				times = 1;
				returns("alert_name");

				alert.getAlertType();
				times = 1;
				returns("TEST");

				ValueTO[] criteria = new ValueTO[] { new ValueTO("name", "TEST") };
				env.getObjects("ALERT_TYPE", criteria);
				times = 1;
				returns(Arrays.asList(alertTypeRef));

				alert.getAlertTime();
				times = 1;
				returns(1L);

				alert.getStatus();
				times = 1;
				returns(AlertStatus.OPENED);

				alert.getMessage();
				times = 1;
				returns("$message$");

				alert.getFullMessage();
				times = 1;
				returns("$message$");

				alert.getHostTO();
				times = 1;
				returns(TOFactory.EMPTY_TO);

				alert.getAcknowledgement();
				times = 1;
				returns(null);

				alert.getAcknowledgementTime();
				times = 1;
				returns(null);

				alert.getUser();
				times = 1;
				returns(null);

			}
		};

		List<ValueTO> properties = converter.apply(alert);
		Map<String, Object> propertiesMap = EnvironmentUtils.valuesAsMap(properties);

		assertEquals(10, propertiesMap.size());
		assertEquals("alert_name", propertiesMap.get("name"));
		assertEquals(alertTypeRef, propertiesMap.get("type"));
		assertEquals(1L, propertiesMap.get("time"));
		assertEquals(0, propertiesMap.get("status"));
		assertEquals("$message$", propertiesMap.get("message"));
		assertEquals("$message$", propertiesMap.get("fullMessage"));
		assertNull(propertiesMap.get("hostTO"));
		assertNull(propertiesMap.get("acknowledgement"));
		assertNull(propertiesMap.get("acknowledgementTime"));
		assertNull(propertiesMap.get("user"));
	}
	
	@Test
	public void convertAlertOnHost(@Mocked TO host) {
		AlertToProperties converter = new AlertToProperties(env);

		new StrictExpectations() {
			{
				alert.getName();
				times = 1;
				returns("alert_name");

				alert.getAlertType();
				times = 1;
				returns("TEST");

				ValueTO[] criteria = new ValueTO[] { new ValueTO("name", "TEST") };
				env.getObjects("ALERT_TYPE", criteria);
				times = 1;
				returns(Arrays.asList(alertTypeRef));

				alert.getAlertTime();
				times = 1;
				returns(1L);

				alert.getStatus();
				times = 1;
				returns(AlertStatus.OPENED);

				alert.getMessage();
				times = 1;
				returns("$message$");

				alert.getFullMessage();
				times = 1;
				returns("$message$");

				alert.getHostTO();
				times = 2;
				returns(host);

				alert.getAcknowledgement();
				times = 1;
				returns(null);

				alert.getAcknowledgementTime();
				times = 1;
				returns(null);

				alert.getUser();
				times = 1;
				returns(null);

			}
		};

		List<ValueTO> properties = converter.apply(alert);
		Map<String, Object> propertiesMap = EnvironmentUtils.valuesAsMap(properties);

		assertEquals(10, propertiesMap.size());
		assertEquals("alert_name", propertiesMap.get("name"));
		assertEquals(alertTypeRef, propertiesMap.get("type"));
		assertEquals(1L, propertiesMap.get("time"));
		assertEquals(0, propertiesMap.get("status"));
		assertEquals("$message$", propertiesMap.get("message"));
		assertEquals("$message$", propertiesMap.get("fullMessage"));
		assertEquals(host, propertiesMap.get("hostTO"));
		assertNull(propertiesMap.get("acknowledgement"));
		assertNull(propertiesMap.get("acknowledgementTime"));
		assertNull(propertiesMap.get("user"));
	}
	
	@Test
	public void convertAcknoledgedAlert(@Mocked TO host, @Injectable Date acknowledgmentTime) {
		AlertToProperties converter = new AlertToProperties(env);

		new StrictExpectations() {
			{
				alert.getName();
				times = 1;
				returns("alert_name");

				alert.getAlertType();
				times = 1;
				returns("TEST");

				ValueTO[] criteria = new ValueTO[] { new ValueTO("name", "TEST") };
				env.getObjects("ALERT_TYPE", criteria);
				times = 1;
				returns(Arrays.asList(alertTypeRef));

				alert.getAlertTime();
				times = 1;
				returns(1L);

				alert.getStatus();
				times = 1;
				returns(AlertStatus.OPENED);

				alert.getMessage();
				times = 1;
				returns("$message$");

				alert.getFullMessage();
				times = 1;
				returns("$message$");

				alert.getHostTO();
				times = 2;
				returns(host);

				alert.getAcknowledgement();
				times = 1;
				returns(null);

				alert.getAcknowledgementTime();
				times = 1;
				returns(acknowledgmentTime);
				
				acknowledgmentTime.getTime();
				times=1;
				returns(1L);

				alert.getUser();
				times = 1;
				returns(null);

			}
		};

		List<ValueTO> properties = converter.apply(alert);
		Map<String, Object> propertiesMap = EnvironmentUtils.valuesAsMap(properties);

		assertEquals(10, propertiesMap.size());
		assertEquals("alert_name", propertiesMap.get("name"));
		assertEquals(alertTypeRef, propertiesMap.get("type"));
		assertEquals(1L, propertiesMap.get("time"));
		assertEquals(0, propertiesMap.get("status"));
		assertEquals("$message$", propertiesMap.get("message"));
		assertEquals("$message$", propertiesMap.get("fullMessage"));
		assertEquals(host, propertiesMap.get("hostTO"));
		assertNull(propertiesMap.get("acknowledgement"));
		assertEquals(1L, propertiesMap.get("acknowledgementTime"));
		assertNull(propertiesMap.get("user"));
	}

	@Test(expected = MappingException.class)
	public void convertAlertWhichRefersNonExistingType() {
		AlertToProperties converter = new AlertToProperties(env);

		new StrictExpectations() {
			{
				alert.getName();
				times = 1;
				returns("alert_name");

				alert.getAlertType();
				times = 1;
				returns("TEST");

				ValueTO[] criteria = new ValueTO[] { new ValueTO("name", "TEST") };
				env.getObjects("ALERT_TYPE", criteria);
				times = 1;
				returns(Collections.EMPTY_LIST);
				
				alert.getAlertType();
				times = 1;
				returns("TEST");
			}
		};

		converter.apply(alert);
	}

}
