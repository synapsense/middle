package com.synapsense.impl.nsvc;

import java.util.Collection;
import java.util.LinkedList;
import java.util.Random;

import junit.framework.Assert;

import org.junit.Test;

import com.synapsense.dto.TO;
import com.synapsense.impl.nsvc.compressor.ReferencedObjectChangedEventCompressor;
import com.synapsense.service.impl.dao.to.GenericObjectTO;
import com.synapsense.service.nsvc.events.ReferencedObjectChangedEvent;
import com.synapsense.util.Pair;

public class ReferencedObjectChangedEventCompressorTest {

	@Test
	public void testCompression() {
		final int eventsNum = 1000;
		final double prob = 0.7;
		Pair<Collection<ReferencedObjectChangedEvent>, Integer> data = generateRandomData(eventsNum, prob);
		// some self checks
		Assert.assertEquals(eventsNum, data.getFirst().size());
		Assert.assertTrue("some duplicated data generated", data.getSecond() != eventsNum);
		Assert.assertEquals("duplicates probability is correct", prob, 1.0 - (double) data.getSecond()
		        / (double) eventsNum, 0.1);// yeah, i know i'm paranoid

		// ok, feed data into compressor
		ReferencedObjectChangedEventCompressor compressor = new ReferencedObjectChangedEventCompressor();
		for (ReferencedObjectChangedEvent event : data.getFirst()) {
			compressor.process(event);
		}

		// check that 'duplicates' were compressed
		Assert.assertEquals((int) data.getSecond(), compressor.getCompressedEvents().size());
		// the first call to getCompressedEvents should have drained buffer
		Assert.assertEquals(0, compressor.getCompressedEvents().size());
		// check stats
		Assert.assertEquals((long) data.getFirst().size(), compressor.getNumOfEventsReceived());
		Assert.assertEquals((long) data.getSecond(), compressor.getNumOfEventsSent());
		// check stats reset
		compressor.resetStatistics();
		Assert.assertEquals(0L, compressor.getNumOfEventsReceived());
		Assert.assertEquals(0L, compressor.getNumOfEventsSent());
	}

	// Returned pair has collection of events as the first element and number of
	// 'non-duplicates' as the second.
	// n is number of events to generate, k is probability of the next generated
	// event being a 'duplicate'
	public static Pair<Collection<ReferencedObjectChangedEvent>, Integer> generateRandomData(int n, double k) {
		Collection<ReferencedObjectChangedEvent> events = new LinkedList<ReferencedObjectChangedEvent>();
		int nonduplicates = 0;
		final String typePrefix = "TYPE_";
		final String propPrefix = "PROP_";
		Random rnd = new Random();
		ReferencedObjectChangedEvent lastEvent = null;
		for (int i = 0; i < n; i++) {
			if (rnd.nextDouble() >= k || lastEvent == null) {// generate new
															 // event
				ReferencedObjectChangedEvent event = new ReferencedObjectChangedEvent(getRandomTO(rnd, typePrefix),
				        getRandomName(rnd, propPrefix), getRandomTO(rnd, typePrefix), getRandomName(rnd, propPrefix));
				events.add(event);
				nonduplicates++;
				lastEvent = event;
			} else {// make a 'duplicate'
				ReferencedObjectChangedEvent event = new ReferencedObjectChangedEvent(lastEvent.getReferencingObj(),
				        lastEvent.getReferencingPropName(), lastEvent.getReferencedObj(),
				        getRandomName(rnd, propPrefix));
				events.add(event);
			}
		}
		return new Pair<Collection<ReferencedObjectChangedEvent>, Integer>(events, nonduplicates);
	}

	private static String getRandomName(Random rnd, String prefix) {
		return prefix += rnd.nextInt();
	}

	private static TO<?> getRandomTO(Random rnd, String typePrefix) {
		return new GenericObjectTO(rnd.nextInt(), getRandomName(rnd, typePrefix));
	}

}
