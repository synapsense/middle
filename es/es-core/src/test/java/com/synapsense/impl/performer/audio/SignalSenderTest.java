package com.synapsense.impl.performer.audio;

import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;

import javax.naming.NamingException;

import mockit.Deencapsulation;
import mockit.Mocked;
import mockit.NonStrictExpectations;

import mockit.integration.junit4.JMockit;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.opennms.protocols.snmp.SnmpPeer;
import org.opennms.protocols.snmp.SnmpSession;

import com.synapsense.dto.Alert;
import com.synapsense.dto.AlertPriority;
import com.synapsense.dto.Message;
import com.synapsense.dto.PriorityTier;
import com.synapsense.impl.performer.snmp.SNMPPerformerException;
import com.synapsense.impl.performer.snmp.conf.ConnectionInfoProvider;
import com.synapsense.impl.performer.snmp.conf.ConnectionInfoProvider.ConnectionInfo;
import com.synapsense.service.AlertService;

@RunWith(JMockit.class)
public class SignalSenderTest {

	@SuppressWarnings("unused")
	@Mocked
	private SnmpSession unusedSnmpSession;
	@SuppressWarnings("unused")
	@Mocked
	private ConnectionInfoProvider unusedConnectionInfoProvider;

	@Test
	public void signalSenderTest(@Mocked AlertService alertService, @Mocked ScheduledExecutorService executorService) throws NamingException, UnknownHostException, SNMPPerformerException,
	        SocketException {
		final ConnectionInfo connInfo = new ConnectionInfo(InetAddress.getLocalHost(), 161, InetAddress.getByName("192.168.102.100"), 161,
		        "public", "public");
		new NonStrictExpectations() {
			{
				ConnectionInfoProvider.getConnectionInfo((String) any, "AV");
				result = connInfo;
				new SnmpSession((SnmpPeer) any);

			}
		};
		new SignalSender("192.168.102.100:161", alertService, executorService);

	}

	@Test
	public void signalSenderSendInactiveTowerTest(@Mocked AlertService alertService, @Mocked Message metadata,
												  @Mocked ScheduledExecutorService executorService)
					throws NamingException, UnknownHostException, SNMPPerformerException, SocketException {
		final ConnectionInfo connInfo = new ConnectionInfo(InetAddress.getLocalHost(), 161, InetAddress.getByName("192.168.102.100"), 161,
		        "public", "public");

		final AlertPriority priority = new AlertPriority("MINOR", 200000l, new ArrayList<PriorityTier>(), true, true,
		        false, 10l, true, 10l, false);
		new NonStrictExpectations() {
			{
				ConnectionInfoProvider.getConnectionInfo((String) any, "AV");
				result = connInfo;
				new SnmpSession((SnmpPeer) any);
				alertService.getAlertPriority((String) any);
				result = priority;
				metadata.getAlertID();
				result = 100;
				alertService.getAlert(100);
				result = new Alert();
			}
		};
		new SignalSender("192.168.102.100:161", alertService, executorService).send(metadata);
	}

	@Test
	public void signalSenderSendActiveTheSameIDTowerTest(@Mocked AlertService alertService, @Mocked Message metadata,
														 @Mocked ScheduledExecutorService executorService, @Mocked ScheduledFuture<?> audioScheduledFuture,
														 @Mocked ScheduledFuture<?> lightScheduledFuture)
					throws NamingException, UnknownHostException, SNMPPerformerException, SocketException {
		final ConnectionInfo connInfo = new ConnectionInfo(InetAddress.getLocalHost(), 161, InetAddress.getByName("192.168.102.100"), 161,
		        "public", "public");

		final AlertPriority priority = new AlertPriority("MINOR", 200000l, new ArrayList<PriorityTier>(), true, true,
		        false, 10l, true, 10l, false);
		new NonStrictExpectations() {
			{
				ConnectionInfoProvider.getConnectionInfo((String) any, "AV");
				result = connInfo;
				new SnmpSession((SnmpPeer) any);
				alertService.getAlertPriority((String) any);
				result = priority;
				metadata.getAlertID();
				result = 100;
				alertService.getAlert(100);
				result = new Alert();
				audioScheduledFuture.isDone();
				result = false;
				lightScheduledFuture.isDone();
				result = false;

			}
		};

		SignalSender sender = new SignalSender("192.168.102.100:161", alertService, executorService);
		Deencapsulation.setField(sender, "activeSignalTower", true);
		Deencapsulation.setField(sender, "activeAlertId", 100);
		Deencapsulation.setField(sender, "audioScheduledFuture", audioScheduledFuture);
		Deencapsulation.setField(sender, "lightScheduledFuture", lightScheduledFuture);

		sender.send(metadata);
	}

	@Test
	public void signalSenderSendActiveHighPriorityTowerTest(@Mocked AlertService alertService, @Mocked Message metadata,
															@Mocked ScheduledExecutorService executorService, @Mocked ScheduledFuture<?> audioScheduledFuture,
															@Mocked ScheduledFuture<?> lightScheduledFuture)
					throws NamingException, UnknownHostException, SNMPPerformerException, SocketException {
		final ConnectionInfo connInfo = new ConnectionInfo(InetAddress.getLocalHost(), 161, InetAddress.getByName("192.168.102.100"), 161,
		        "public", "public");

		final AlertPriority priority = new AlertPriority("MAJOR", 200000l, new ArrayList<PriorityTier>(), true, true,
		        false, 10l, true, 10l, false);
		new NonStrictExpectations() {
			{
				ConnectionInfoProvider.getConnectionInfo((String) any, "AV");
				result = connInfo;
				new SnmpSession((SnmpPeer) any);
				alertService.getAlertPriority((String) any);
				result = priority;

				metadata.getAlertID();
				result = 100;
				metadata.getPriority();
				result = "MAJOR";
				alertService.getAlert(100);
				result = new Alert();

			}
		};

		SignalSender sender = new SignalSender("192.168.102.100:161", alertService, executorService);
		Deencapsulation.setField(sender, "activeAlertPriority", "MINOR");
		Deencapsulation.setField(sender, "activeSignalTower", true);
		Deencapsulation.setField(sender, "audioScheduledFuture", audioScheduledFuture);
		Deencapsulation.setField(sender, "lightScheduledFuture", lightScheduledFuture);
		Deencapsulation.setField(sender, "activeAlertId", 105);
		sender.send(metadata);
	}

}
