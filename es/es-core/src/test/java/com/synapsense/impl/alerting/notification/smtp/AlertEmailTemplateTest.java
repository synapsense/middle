package com.synapsense.impl.alerting.notification.smtp;

import com.google.common.base.Charsets;
import com.google.common.io.Files;
import freemarker.template.Template;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExternalResource;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

/**
 * @author : shabanov
 */
public class AlertEmailTemplateTest {

	@Rule
	public FileResource noSuchFile;

	@Rule
	public FileResource testTemplate;

	@Before
	public void setUp() throws Exception {
		testTemplate = new FileResource(new File(AlertEmailTemplateTest.class.getResource("test.template").toURI()));
		noSuchFile = new FileResource(new File(".", "nosuch.file"));
	}

	@Test
	public void getTemplateFromSpecifiedFile() throws Exception {
		AlertEmailTemplate t = new AlertEmailTemplate(Paths.get(this.getClass().getResource("test.template").toURI())
		        .toFile(), false);

		Template tInst = t.get();
		assertNotNull(tInst);

		assertThat(tInst.toString(), is(Files.toString(testTemplate.getFile(), Charsets.UTF_8)));
	}

	@Test
	public void getDefaultTemplateIfFileDoesNotExists() throws Exception {
		AlertEmailTemplate t = new AlertEmailTemplate(noSuchFile.getFile(), false);

		Template tInst = t.get();
		assertNotNull(tInst);

		assertThat(tInst.toString(), is(AlertEmailTemplate.DEFAULT_TEMPLATE));

	}

	@Test
	public void main() throws InterruptedException, BrokenBarrierException {
		ExecutorService watchThread = Executors.newSingleThreadExecutor();
		watchThread.execute(new Runnable() {
			@Override
			public void run() {
				while (true) {

					if (Thread.currentThread().isInterrupted()) {
						System.out.println("irr flag check...");
						break;
					}

					try {
						Thread.sleep(1);
					} catch (InterruptedException e) {
						System.out.println("irr exception ... ");
						break;
					}

				}
			}
		});

		Thread.sleep(2);

		watchThread.shutdownNow();
		try {
			if (watchThread.awaitTermination(5, TimeUnit.SECONDS)) {
				System.out.println("graceful stop of service");
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}

class FileResource extends ExternalResource {
	File file;

	FileResource(File file) {
		this.file = file;
	}

	public File getFile() throws IOException {
		return file;
	}
}
