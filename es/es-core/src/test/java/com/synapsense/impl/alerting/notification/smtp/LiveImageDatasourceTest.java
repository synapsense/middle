package com.synapsense.impl.alerting.notification.smtp;

import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.dto.ValueTO;
import com.synapsense.service.Environment;
import com.synapsense.util.CollectionUtils;
import mockit.Expectations;
import mockit.Mocked;
import mockit.integration.junit4.JMockit;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.net.URL;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * @author : shabanov
 */
@RunWith(JMockit.class)
public class LiveImageDatasourceTest {

	@Mocked
	Environment env;

	@Test
	public void testDescriptionOfLiveImageDatasource() throws Exception {
		final TO dc = TOFactory.getInstance().loadTO("DC:1");
		final LiveImageDataSource ds = new LiveImageDataSource(env, dc, ImageLayer.BOTTOM, DataClass.TEMPERATURE);

		new Expectations() {
			{
				env.getPropertyValue(dc, "name", String.class);
				result = "DC";
			}
		};
		assertThat(ds.getDescription(),
		        is("DC, " + ImageLayer.BOTTOM.toString() + ", " + DataClass.TEMPERATURE.toString()));
	}

	@Test
	public void testFileNameOfLiveImageDatasource() throws Exception {
		final TO dc = TOFactory.getInstance().loadTO("DC:1");
		final LiveImageDataSource ds = new LiveImageDataSource(env, dc, ImageLayer.BOTTOM, DataClass.TEMPERATURE);

		assertThat(ds.getFileName(), is("1-" + DataClass.TEMPERATURE.getCode() + "-" + ImageLayer.BOTTOM.getCode()
		        + ".png"));
	}

	@Test
	public void imageTypeIsConstant() throws Exception {
		final TO dc = TOFactory.getInstance().loadTO("DC:1");
		final LiveImageDataSource ds = new LiveImageDataSource(env, dc, ImageLayer.BOTTOM, DataClass.TEMPERATURE);

		assertThat(ds.getType(), is("image/png"));
	}

	@Test
	public void testGetUrlSuccess() throws Exception {
		final TO dc = TOFactory.getInstance().loadTO("DC:1");
		final LiveImageDataSource ds = new LiveImageDataSource(env, dc, ImageLayer.BOTTOM, DataClass.TEMPERATURE);

		new Expectations() {
			{
				TO configId = TOFactory.getInstance().loadTO("CONFIGURATION_DATA:2");
				ValueTO[] vals = { new ValueTO("name", "LiveImaging") };
				env.getObjects("CONFIGURATION_DATA", vals);
				result = CollectionUtils.list(configId);

				env.getPropertyValue(configId, "config", String.class);
				result = convertStreamToString(LiveImageDatasourceTest.class.getResourceAsStream("conf.xml"));
			}
		};

		URL url = ds.getURL();

		assertThat(url, is(new URL("http://localhost:9091/generateLatestLiteImage?objId=" + dc.toString()
		        + "&dataclass=" + DataClass.TEMPERATURE.getCode() + "&layer=" + ImageLayer.BOTTOM.getCode())));
	}

	private static String convertStreamToString(java.io.InputStream is) {
		java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
		return s.hasNext() ? s.next() : "";
	}
}
