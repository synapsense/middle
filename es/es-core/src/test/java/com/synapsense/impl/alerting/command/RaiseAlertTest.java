package com.synapsense.impl.alerting.command;

import com.synapsense.commandprocessor.api.Command;
import com.synapsense.dto.*;
import com.synapsense.exception.AlertServiceSystemException;
import com.synapsense.exception.EnvException;
import com.synapsense.impl.alerting.AlertDismissor;
import com.synapsense.impl.alerting.AlertServiceImpl;
import com.synapsense.impl.alerting.Cache;
import com.synapsense.search.QueryDescription;
import com.synapsense.service.Environment;
import com.synapsense.service.impl.activitylog.ActivityLogService;
import com.synapsense.service.impl.activitylog.ActivityRecord;
import com.synapsense.util.CollectionUtils.Functor;
import mockit.StrictExpectations;
import mockit.Mocked;
import mockit.integration.junit4.JMockit;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static com.synapsense.impl.alerting.AlertConstants.TYPE.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(JMockit.class)
public class RaiseAlertTest {

	// should be reference to implementation class because it is used in Activity logger as target
	@Mocked
	AlertServiceImpl alertService;

	@Test
	public void instantiateCommand(@Mocked Environment env, @Mocked Cache<Alert, TO<?>> cache, @Mocked AlertDismissor dismissor,
								   @Mocked ActivityLogService activityLog, @Mocked Alert alert, @Mocked Functor<List<ValueTO>, Alert> f) {
		Command raise = new RaiseAlert(env, alertService, cache, dismissor, activityLog, alert, 0, f);
		assertNotNull(raise);
	}

	@Test(expected = AlertServiceSystemException.class)
	public void newRaiseAlertNoAlertTypesDefinedAtAll(@Mocked Environment env, @Mocked Cache<Alert, TO<?>> cache,
													  @Mocked AlertDismissor dismissor, @Mocked ActivityLogService activityLog, @Mocked Alert alert, @Mocked Functor<List<ValueTO>, Alert> f) {
		new StrictExpectations() {
			{
				alert.getAlertType();
				times = 1;
				returns("type");

				ValueTO[] criteria = new ValueTO[] { new ValueTO("name", "type") };
				env.getObjects(ALERT_DESCRIPTION_TYPE_NAME, criteria);
				times = 1;
				returns(Collections.EMPTY_LIST);

				criteria = new ValueTO[] { new ValueTO("name", UNKNOWN_ALERT_TYPE_NAME) };
				env.getObjects(ALERT_DESCRIPTION_TYPE_NAME, criteria);
				times = 1;
				returns(Collections.EMPTY_LIST);
			}
		};
		Command raise = new RaiseAlert(env, alertService, cache, dismissor, activityLog, alert, 0, f);
		assertNotNull(raise);
		raise.execute();
	}

	@Test
	public void raiseNewAlertOfUnexisitngType(@Mocked Environment env, @Mocked Cache<Alert, TO<?>> cache,
											  @Mocked AlertDismissor dismissor, @Mocked ActivityLogService activityLog, @Mocked TO<?> alertTypeRef,
											  @Mocked AlertType alertType, @Mocked CollectionTO alertTypeInfo, @Mocked Functor<List<ValueTO>, Alert> f)
	        throws EnvException {

		final TO<?> alertID = TOFactory.INSTANCE.loadTO("ALERT", 1);

		final Alert alert = new Alert("name", "type", new Date(), "message");
		new StrictExpectations() {
			{
                cache.get(alert);
                times = 1;
                returns(null);

				ValueTO[] criteria = new ValueTO[] { new ValueTO("name", "type") };
				env.getObjects(ALERT_DESCRIPTION_TYPE_NAME, criteria);
				times = 1;
				returns(Collections.EMPTY_LIST);

				criteria = new ValueTO[] { new ValueTO("name", UNKNOWN_ALERT_TYPE_NAME) };
				env.getObjects(ALERT_DESCRIPTION_TYPE_NAME, criteria);
				times = 1;
				returns(Arrays.asList(alertTypeRef));

				env.getPropertyValue(Arrays.<TO<?>> asList(alertTypeRef), new String[] { "name", "active", "autoAck",
				        "autoDismiss" });
				times = 1;
				returns(Arrays.asList(alertTypeInfo));

				alertTypeInfo.getSinglePropValue("name");
				times = 1;
				returns(new ValueTO("name", UNKNOWN_ALERT_TYPE_NAME));

				// this call is for status setup
				alertTypeInfo.getSinglePropValue("autoAck");
				times = 1;
				returns(new ValueTO("autoAck", 0));

				// this call is for status setup
				alertTypeInfo.getSinglePropValue("autoDismiss");
				times = 1;
				returns(new ValueTO("autoDismiss", 0));

				alertTypeInfo.getSinglePropValue("active");
				times = 1;
				returns(new ValueTO("active", 1));

				cache.get(alert);
				times = 1;
				returns(null);

				f.apply(alert);
				times = 1;
				returns(Collections.EMPTY_LIST);

				env.createObject(ALERT_TYPE_NAME, new ValueTO[] {});
				times = 1;
				returns(alertID);

				cache.put(alert, alertID);
				times = 1;
			}
		};

		Command raise = new RaiseAlert(env, alertService, cache, dismissor, activityLog, alert, 0, f);
		assertNotNull(raise);

		raise.execute();

		assertEquals(UNKNOWN_ALERT_TYPE_NAME, alert.getAlertType());
		assertEquals(AlertStatus.OPENED, alert.getStatus());
		assertEquals(Long.valueOf(0), alert.getAlertTime());

	}

	@Test
	public void raiseNewAlertOfCertainType(@Mocked Environment env, @Mocked Cache<Alert, TO<?>> cache,
										   @Mocked AlertDismissor dismissor, @Mocked ActivityLogService activityLog, @Mocked TO<?> alertTypeRef,
										   @Mocked AlertType alertType, @Mocked CollectionTO alertTypeInfo, @Mocked Functor<List<ValueTO>, Alert> f)
	        throws EnvException {

		final TO<?> alertID = TOFactory.INSTANCE.loadTO("ALERT", 1);

		final Alert alert = new Alert("name", "type", new Date(), "message");
		new StrictExpectations() {
			{
                cache.get(alert);
                times = 1;
                returns(null);

				ValueTO[] criteria = new ValueTO[] { new ValueTO("name", "type") };
				env.getObjects(ALERT_DESCRIPTION_TYPE_NAME, criteria);
				times = 1;
				returns(Arrays.asList(alertTypeRef));

				env.getPropertyValue(Arrays.<TO<?>> asList(alertTypeRef), new String[] { "name", "active", "autoAck",
				        "autoDismiss" });
				times = 1;
				returns(Arrays.asList(alertTypeInfo));

				alertTypeInfo.getSinglePropValue("name");
				times = 1;
				returns(new ValueTO("name", "type"));

				// this call is for status setup
				alertTypeInfo.getSinglePropValue("autoAck");
				times = 1;
				returns(new ValueTO("autoAck", 0));

				// this call is for status setup
				alertTypeInfo.getSinglePropValue("autoDismiss");
				times = 1;
				returns(new ValueTO("autoDismiss", 0));

				alertTypeInfo.getSinglePropValue("active");
				times = 1;
				returns(new ValueTO("active", 1));

				cache.get(alert);
				times = 1;
				returns(null);

				f.apply(alert);
				times = 1;
				returns(Collections.EMPTY_LIST);

				env.createObject(ALERT_TYPE_NAME, new ValueTO[] {});
				times = 1;
				returns(alertID);

				cache.put(alert, alertID);
				times = 1;
			}
		};

		Command raise = new RaiseAlert(env, alertService, cache, dismissor, activityLog, alert, 0, f);
		assertNotNull(raise);

		raise.execute();

		assertEquals("type", alert.getAlertType());
		assertEquals(AlertStatus.OPENED, alert.getStatus());
		assertEquals(Long.valueOf(0), alert.getAlertTime());

	}

	@Test
	public void raiseNewAutoAcknowledgeableAlertOfCertainType(@Mocked Environment env, @Mocked Cache<Alert, TO<?>> cache,
															  @Mocked AlertDismissor dismissor, @Mocked ActivityLogService activityLog, @Mocked TO<?> alertTypeRef,
															  @Mocked AlertType alertType, @Mocked CollectionTO alertTypeInfo, @Mocked Functor<List<ValueTO>, Alert> f)
	        throws EnvException {

		final TO<?> alertID = TOFactory.INSTANCE.loadTO("ALERT", 1);

		final Alert alert = new Alert("name", "type", new Date(), "message");
		new StrictExpectations() {
			{
                cache.get(alert);
                times = 1;
                returns(null);

				ValueTO[] criteria = new ValueTO[] { new ValueTO("name", "type") };
				env.getObjects(ALERT_DESCRIPTION_TYPE_NAME, criteria);
				times = 1;
				returns(Arrays.asList(alertTypeRef));

				env.getPropertyValue(Arrays.<TO<?>> asList(alertTypeRef), new String[] { "name", "active", "autoAck",
				        "autoDismiss" });
				times = 1;
				returns(Arrays.asList(alertTypeInfo));

				alertTypeInfo.getSinglePropValue("name");
				times = 1;
				returns(new ValueTO("name", "type"));

				// this call is for status setup
				alertTypeInfo.getSinglePropValue("autoAck");
				times = 1;
				returns(new ValueTO("autoAck", 1));

				// this call is for status setup
				alertTypeInfo.getSinglePropValue("autoDismiss");
				times = 1;
				returns(new ValueTO("autoDismiss", 0));

				alertTypeInfo.getSinglePropValue("active");
				times = 1;
				returns(new ValueTO("active", 1));

				cache.get(alert);
				times = 1;
				returns(null);

				f.apply(alert);
				times = 1;
				returns(Collections.EMPTY_LIST);

				env.createObject(ALERT_TYPE_NAME, new ValueTO[] {});
				times = 1;
				returns(alertID);

				cache.put(alert, alertID);
				times = 1;

				onInstance(activityLog).addRecord(withInstanceOf(ActivityRecord.class));
				times = 1;

			}
		};

		Command raise = new RaiseAlert(env, alertService, cache, dismissor, activityLog, alert, 0, f);
		assertNotNull(raise);

		raise.execute();

		assertEquals("type", alert.getAlertType());
		assertEquals(AlertStatus.ACKNOWLEDGED, alert.getStatus());
		assertEquals(Long.valueOf(0), alert.getAlertTime());

	}

	@Test
	public void raiseNewAutoDismissableeAlertOfCertainType(@Mocked Environment env, @Mocked Cache<Alert, TO<?>> cache,
														   @Mocked AlertDismissor dismissor, @Mocked ActivityLogService activityLog, @Mocked TO<?> alertTypeRef,
														   @Mocked AlertType alertType, @Mocked CollectionTO alertTypeInfo, @Mocked TO<?> priority,
														   @Mocked Functor<List<ValueTO>, Alert> f) throws EnvException {

		final TO<?> alertID = TOFactory.INSTANCE.loadTO("ALERT", 1);

		final Alert alert = new Alert("name", "type", new Date(), "message");
		new StrictExpectations() {
			{
                cache.get(alert);
                times = 1;
                returns(null);

				ValueTO[] criteria = new ValueTO[] { new ValueTO("name", "type") };
				env.getObjects(ALERT_DESCRIPTION_TYPE_NAME, criteria);
				times = 1;
				returns(Arrays.asList(alertTypeRef));

				env.getPropertyValue(Arrays.<TO<?>> asList(alertTypeRef), new String[] { "name", "active", "autoAck",
				        "autoDismiss" });
				times = 1;
				returns(Arrays.asList(alertTypeInfo));

				alertTypeInfo.getSinglePropValue("name");
				times = 1;
				returns(new ValueTO("name", "type"));

				// this call is for status setup
				alertTypeInfo.getSinglePropValue("autoAck");
				times = 1;
				returns(new ValueTO("autoAck", 0));

				// this call is for status setup
				alertTypeInfo.getSinglePropValue("autoDismiss");
				times = 1;
				returns(new ValueTO("autoDismiss", 1));

				alertTypeInfo.getSinglePropValue("active");
				times = 1;
				returns(new ValueTO("active", 1));

				cache.get(alert);
				times = 1;
				returns(null);

				f.apply(alert);
				times = 1;
				returns(Collections.EMPTY_LIST);

				env.createObject(ALERT_TYPE_NAME, new ValueTO[] {});
				times = 1;
				returns(alertID);

				cache.put(alert, alertID);
				times = 1;

				env.getObjects(withInstanceOf(QueryDescription.class));
				times = 1;
				returns(Arrays.asList(priority));

				env.getPropertyValue(priority, "dismissInterval", Long.class);
				times = 1;
				returns(1L);

				dismissor.scheduleAlertDismission(alertID, 1L);
				times = 1;
			}
		};

		Command raise = new RaiseAlert(env, alertService , cache, dismissor, activityLog, alert, 0, f);
		assertNotNull(raise);

		raise.execute();

		assertEquals("type", alert.getAlertType());
		assertEquals(AlertStatus.OPENED, alert.getStatus());
		assertEquals(Long.valueOf(0), alert.getAlertTime());

	}

}
