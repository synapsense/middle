package com.synapsense.impl.alerting.macros;

import com.synapsense.dto.Alert;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.exception.PropertyNotFoundException;
import com.synapsense.impl.alerting.notification.smtp.Attachments;
import com.synapsense.impl.alerting.notification.smtp.DataClass;
import com.synapsense.impl.alerting.notification.smtp.ImageLayer;
import com.synapsense.search.QueryDescription;
import com.synapsense.service.Environment;
import com.synapsense.service.LocalizationService;
import com.synapsense.service.user.NonUserContext;
import com.synapsense.util.CollectionUtils;
import java.util.Date;
import mockit.Expectations;
import mockit.Mocked;
import mockit.integration.junit4.JMockit;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.matchers.JUnitMatchers.hasItem;

/**
 * Author : shabanov
 */
@RunWith(JMockit.class)
public class ImageMacroTest {

	@Mocked
	Environment env;

    @Mocked
    LocalizationService localizationService;

	@Mocked
	TO objectWithImage;

	@Mocked
	TO room1;
	@Mocked
	TO room2;
	@Mocked
	TO dc;

	@Test
	public void testDirectSensorReference() throws Exception {
		ImageMacro imageMacro = new ImageMacro(ImageMacro.ImageLevel.ROOM);

		final String input = "$room_live_image$, $data(WSNSENSOR:1.lastValue,55.0,200)$";

		MacroContext ctx = new MacroContext();
		ctx.env = env;
		ctx.alert = new Alert("alert", "type", new Date(), input);
        ctx.localizationService = localizationService;

		new Expectations() {
			{
				env.getPropertyValue(withInstanceOf(TO.class), "z", Integer.class);
				result = ImageLayer.TOP.getCode();

				env.getRelatedObjects(TOFactory.getInstance().loadTO("WSNSENSOR", 1), "ROOM", false);
				result = CollectionUtils.newList(objectWithImage);

                localizationService.getString(DataClass.TEMPERATURE.getName(), new NonUserContext().getLocale());
                localizationService.getString(ImageLayer.TOP.getName(), new NonUserContext().getLocale());

			}
		};

		final String output = ", $data(WSNSENSOR:1.lastValue,55.0,200)$";
		assertThat(imageMacro.expandIn(input, ctx), is(output));
		assertThat(ctx.props.keySet(), hasItem("attachments"));
		assertThat(ctx.props.get("attachments"), is(instanceOf(Attachments.class)));

		Attachments images = (Attachments) ctx.props.get("attachments");
		assertTrue(images.hasAttachment(objectWithImage, ImageLayer.TOP, DataClass.TEMPERATURE));
	}

	@Test
	public void testTwoLevelReferenceToSensor() throws Exception {
		ImageMacro imageMacro = new ImageMacro(ImageMacro.ImageLevel.ROOM);

		final String input = "$room_live_image$, $data(RACK:1.cTop.lastValue,55.0,201)$";
		MacroContext ctx = new MacroContext();
		ctx.env = env;
		ctx.alert = new Alert("alert", "type", new Date(), input);
        ctx.localizationService = localizationService;
		new Expectations() {
			{
				env.getPropertyValue(TOFactory.getInstance().loadTO("RACK", 1), "cTop", Object.class);
				TO sensor = TOFactory.getInstance().loadTO("WSNSENSOR", 1);
				result = sensor;

				env.getPropertyValue(sensor, "z", Integer.class);
				result = ImageLayer.TOP.getCode();

				env.getRelatedObjects(sensor, "ROOM", false);
				result = CollectionUtils.newList(objectWithImage);

                localizationService.getString(DataClass.HUMIDITY.getName(), new NonUserContext().getLocale());
                localizationService.getString(ImageLayer.TOP.getName(), new NonUserContext().getLocale());

			}
		};

		final String output = ", $data(RACK:1.cTop.lastValue,55.0,201)$";
		assertThat(imageMacro.expandIn(input, ctx), is(output));
		assertThat(ctx.props.keySet(), hasItem("attachments"));
		assertThat(ctx.props.get("attachments"), is(instanceOf(Attachments.class)));

		Attachments images = (Attachments) ctx.props.get("attachments");
		assertTrue(images.hasAttachment(objectWithImage, ImageLayer.TOP, DataClass.HUMIDITY));
	}

	@Test
	public void testPlainPropertyReference() throws Exception {
		ImageMacro imageMacro = new ImageMacro(ImageMacro.ImageLevel.ROOM);

		final String input = "$room_live_image$, $data(RACK:1.coldDp,55.0,201)$";

		MacroContext ctx = new MacroContext();
		ctx.env = env;
		ctx.alert = new Alert("alert", "type", new Date(), input);
        ctx.localizationService = localizationService;

		new Expectations() {
			{

				TO rack = TOFactory.getInstance().loadTO("RACK", 1);
				TO displayPointOfRack = TOFactory.getInstance().loadTO("DISPLAYPOINT", 2);

                env.getObjects(withInstanceOf(QueryDescription.class));
				times = 1;
				result = CollectionUtils.newList(displayPointOfRack);

				env.getPropertyValue(displayPointOfRack, "z", Integer.class);
				result = ImageLayer.TOP.getCode();

				env.getRelatedObjects(rack, "ROOM", false);
				result = CollectionUtils.newList(objectWithImage);

                localizationService.getString(DataClass.HUMIDITY.getName(), new NonUserContext().getLocale());
                localizationService.getString(ImageLayer.TOP.getName(), new NonUserContext().getLocale());
			}
		};

		final String output = ", $data(RACK:1.coldDp,55.0,201)$";
		assertThat(imageMacro.expandIn(input, ctx), is(output));
		assertThat(ctx.props.keySet(), hasItem("attachments"));
		assertThat(ctx.props.get("attachments"), is(instanceOf(Attachments.class)));

		Attachments images = (Attachments) ctx.props.get("attachments");
		assertTrue(images.hasAttachment(objectWithImage, ImageLayer.TOP, DataClass.HUMIDITY));
	}

	@Test
	public void testNoOccurrencesOfDataMacro() throws Exception {
		ImageMacro imageMacro = new ImageMacro(ImageMacro.ImageLevel.ROOM);

		String input = "message does not contain data macro";
		assertThat(imageMacro.expandIn(input, null), is(input));

	}

	@Test
	public void testExpansionIfNoDataclassSpecified() throws Exception {
		ImageMacro imageMacro = new ImageMacro(ImageMacro.ImageLevel.ROOM);
		final String input = "$data(RACK:1.coldDp,55.0)$";
		assertThat(imageMacro.expandIn(input, null), is(input));
	}

	@Test
	public void testReferencedObjectHasNoLayer() throws Exception {
		ImageMacro imageMacro = new ImageMacro(ImageMacro.ImageLevel.ROOM);

		final String input = "$room_live_image$, $data(WSNSENSOR:1.lastValue,55.0,200)$";

		MacroContext ctx = new MacroContext();
		ctx.env = env;
		ctx.alert = new Alert("alert", "type", new Date(), input);
		new Expectations() {
			{
				env.getPropertyValue(withInstanceOf(TO.class), "z", Integer.class);
				result = new PropertyNotFoundException("z");
			}
		};

		final String output = ", $data(WSNSENSOR:1.lastValue,55.0,200)$";
		assertThat(imageMacro.expandIn(input, ctx), is(output));
	}

	@Test
	public void testMultipleOccurrencesOfDataMacro() throws Exception {
		ImageMacro imageMacro = new ImageMacro(ImageMacro.ImageLevel.ROOM);

		final String input = "$room_live_image$, $data(WSNSENSOR:1.lastValue,55.0,200)$ , $data(WSNSENSOR:2.lastValue,50.0,201)$";

		MacroContext ctx = new MacroContext();
		ctx.env = env;
		ctx.alert = new Alert("alert", "type", new Date(), input);
        ctx.localizationService = localizationService;
		new Expectations() {
			{
				TO sensor1 = TOFactory.getInstance().loadTO("WSNSENSOR", 1);

				env.getPropertyValue(sensor1, "z", Integer.class);
				result = ImageLayer.TOP.getCode();

				env.getRelatedObjects(sensor1, "ROOM", false);
				result = CollectionUtils.newList(objectWithImage);

                localizationService.getString(DataClass.TEMPERATURE.getName(), new NonUserContext().getLocale());
                localizationService.getString(ImageLayer.TOP.getName(), new NonUserContext().getLocale());

				TO sensor2 = TOFactory.getInstance().loadTO("WSNSENSOR", 2);

				env.getPropertyValue(sensor2, "z", Integer.class);
				result = ImageLayer.MIDDLE.getCode();

				env.getRelatedObjects(sensor2, "ROOM", false);
				result = CollectionUtils.newList(objectWithImage);

                localizationService.getString(DataClass.HUMIDITY.getName(), new NonUserContext().getLocale());
                localizationService.getString(ImageLayer.MIDDLE.getName(), new NonUserContext().getLocale());
            }
		};

		final String output = ", $data(WSNSENSOR:1.lastValue,55.0,200)$ , $data(WSNSENSOR:2.lastValue,50.0,201)$";
		assertThat(imageMacro.expandIn(input, ctx), is(output));

		assertThat(ctx.props.keySet(), hasItem("attachments"));
		Attachments images = (Attachments) ctx.props.get("attachments");
		assertTrue(images.hasAttachment(objectWithImage, ImageLayer.TOP, DataClass.TEMPERATURE));
		assertTrue(images.hasAttachment(objectWithImage, ImageLayer.MIDDLE, DataClass.HUMIDITY));
	}

	@Test
	public void testUniqueImagesSupportedOnly() throws Exception {
		ImageMacro imageMacro = new ImageMacro(ImageMacro.ImageLevel.ROOM);

		final String input = "$room_live_image$, $data(WSNSENSOR:1.lastValue,55.0,200)$ , $data(WSNSENSOR:2.lastValue,50.0,200)$ ";

		MacroContext ctx = new MacroContext();
		ctx.env = env;
		ctx.alert = new Alert("alert", "type", new Date(), input);
        ctx.localizationService = localizationService;
		new Expectations() {
			{
				TO sensor1 = TOFactory.getInstance().loadTO("WSNSENSOR", 1);

				env.getPropertyValue(sensor1, "z", Integer.class);
				result = ImageLayer.TOP.getCode();

				env.getRelatedObjects(sensor1, "ROOM", false);
				result = CollectionUtils.newList(objectWithImage);

                localizationService.getString(DataClass.TEMPERATURE.getName(), new NonUserContext().getLocale());
                localizationService.getString(ImageLayer.TOP.getName(), new NonUserContext().getLocale());

				TO sensor2 = TOFactory.getInstance().loadTO("WSNSENSOR", 2);

				env.getPropertyValue(sensor2, "z", Integer.class);
				result = ImageLayer.TOP.getCode();

				env.getRelatedObjects(sensor2, "ROOM", false);
				result = CollectionUtils.newList(objectWithImage);
			}
		};

		final String output = ", $data(WSNSENSOR:1.lastValue,55.0,200)$ , $data(WSNSENSOR:2.lastValue,50.0,200)$ ";
		assertThat(imageMacro.expandIn(input, ctx), is(output));
		assertThat(ctx.props.keySet(), hasItem("attachments"));
		Attachments images = (Attachments) ctx.props.get("attachments");
		assertTrue(images.hasAttachment(objectWithImage, ImageLayer.TOP, DataClass.TEMPERATURE));
	}

	@Test
	public void testSingleRoomdDcHandledAsRoom() throws Exception {
		ImageMacro imageMacro = new ImageMacro(ImageMacro.ImageLevel.DC);

		final String input = "$dc_live_image$, $data(WSNSENSOR:1.lastValue,55.0,200)$";

		MacroContext ctx = new MacroContext();
		ctx.env = env;
		ctx.alert = new Alert("alert", "type", new Date(), input);
        ctx.localizationService = localizationService;
		new Expectations() {
			{
				env.getPropertyValue(withInstanceOf(TO.class), "z", Integer.class);
				result = ImageLayer.TOP.getCode();

                TO sensor1 = TOFactory.getInstance().loadTO("WSNSENSOR", 1);
                env.getRelatedObjects(sensor1, "DC", false);
				result = CollectionUtils.newList(objectWithImage);

                localizationService.getString(DataClass.TEMPERATURE.getName(), new NonUserContext().getLocale());
                localizationService.getString(ImageLayer.TOP.getName(), new NonUserContext().getLocale());
			}
		};

		final String output = ", $data(WSNSENSOR:1.lastValue,55.0,200)$";
		assertThat(imageMacro.expandIn(input, ctx), is(output));
		assertThat(ctx.props.keySet(), hasItem("attachments"));
		assertThat(ctx.props.get("attachments"), is(instanceOf(Attachments.class)));

		Attachments images = (Attachments) ctx.props.get("attachments");
		assertTrue(images.hasAttachment(objectWithImage, ImageLayer.TOP, DataClass.TEMPERATURE));
	}

	@Test
	public void testMultipleRoomdDcHandledAsDc() throws Exception {
		ImageMacro imageMacro = new ImageMacro(ImageMacro.ImageLevel.DC);

		final String input = "$dc_live_image$, $data(WSNSENSOR:1.lastValue,55.0,200)$";

		MacroContext ctx = new MacroContext();
		ctx.env = env;
		ctx.alert = new Alert("alert", "type", new Date(), input);
        ctx.localizationService = localizationService;

		new Expectations() {
			@Mocked
			TO room1;
			@Mocked
			TO room2;

			{
				env.getPropertyValue(withInstanceOf(TO.class), "z", Integer.class);
				result =  ImageLayer.TOP.getCode();

				TO sensor1 = TOFactory.getInstance().loadTO("WSNSENSOR", 1);

				env.getRelatedObjects(sensor1, "DC", false);
				result = CollectionUtils.newList(dc);

                localizationService.getString(DataClass.TEMPERATURE.getName(), new NonUserContext().getLocale());
                localizationService.getString(ImageLayer.TOP.getName(), new NonUserContext().getLocale());
			}
		};

		final String output = ", $data(WSNSENSOR:1.lastValue,55.0,200)$";
		assertThat(imageMacro.expandIn(input, ctx), is(output));
		assertThat(ctx.props.keySet(), hasItem("attachments"));
		assertThat(ctx.props.get("attachments"), is(instanceOf(Attachments.class)));

		Attachments images = (Attachments) ctx.props.get("attachments");
		assertTrue(images.hasAttachment(dc, ImageLayer.TOP, DataClass.TEMPERATURE));
	}

	@Test
	public void testDcAndRoomMacrosSequentialReplacing() throws Exception {
		ImageMacro imageMacroDc = new ImageMacro(ImageMacro.ImageLevel.DC);
		ImageMacro imageMacroRoom = new ImageMacro(ImageMacro.ImageLevel.ROOM);
		Macro compositeMacro = new MacrosCollection(CollectionUtils.<Macro> newList(imageMacroDc, imageMacroRoom));

		final String input = "$data(WSNSENSOR:1.lastValue,55.0,200)$,$dc_live_image$$room_live_image$";

		MacroContext ctx = new MacroContext();
		ctx.env = env;
		ctx.alert = new Alert("alert", "type", new Date(), input);
        ctx.localizationService = localizationService;

		new Expectations() {
			{
				// dc macro
				TO sensor1 = TOFactory.getInstance().loadTO("WSNSENSOR", 1);

				env.getPropertyValue(sensor1, "z", Integer.class);
				result = ImageLayer.TOP.getCode();

				env.getRelatedObjects(sensor1, "DC", false);
				result = CollectionUtils.newList(dc);

                localizationService.getString(DataClass.TEMPERATURE.getName(), new NonUserContext().getLocale());
                localizationService.getString(ImageLayer.TOP.getName(), new NonUserContext().getLocale());

				// then goes room macro
				env.getPropertyValue(sensor1, "z", Integer.class);
				result = ImageLayer.TOP.getCode();

				env.getRelatedObjects(sensor1, "ROOM", false);
				result = CollectionUtils.newList(room1);

                localizationService.getString(DataClass.TEMPERATURE.getName(), new NonUserContext().getLocale());
                localizationService.getString(ImageLayer.TOP.getName(), new NonUserContext().getLocale());
			}
		};

		final String output = "$data(WSNSENSOR:1.lastValue,55.0,200)$,";
		assertThat(compositeMacro.expandIn(input, ctx), is(output));
		assertThat(ctx.props.keySet(), hasItem("attachments"));
		assertThat(ctx.props.get("attachments"), is(instanceOf(Attachments.class)));

		Attachments images = (Attachments) ctx.props.get("attachments");
		assertTrue(images.hasAttachment(dc, ImageLayer.TOP, DataClass.TEMPERATURE));
		assertTrue(images.hasAttachment(room1, ImageLayer.TOP, DataClass.TEMPERATURE));
	}

}
