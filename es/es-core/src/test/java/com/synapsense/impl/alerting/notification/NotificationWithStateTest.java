package com.synapsense.impl.alerting.notification;

import com.synapsense.AbstractTest;
import com.synapsense.dto.Alert;
import com.synapsense.dto.Message;
import com.synapsense.dto.MessageType;
import com.synapsense.impl.alerting.macros.Macro;
import com.synapsense.impl.alerting.macros.MacroContext;
import com.synapsense.service.LocalizationService;
import com.synapsense.service.user.UserContext;
import com.synapsense.util.CollectionUtils;
import org.jmock.Expectations;
import org.jmock.Sequence;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class NotificationWithStateTest extends AbstractTest {

	private final UserContext userContextMock = mock(UserContext.class);
	private final Destination destMock = mock(Destination.class);
	private final Macro macroReplacerMock = mock(Macro.class);
	private final DestinationProvider destinationsMock = mock(DestinationProvider.class);
	private final Alert alert=mock(Alert.class);

	private final TransportProvider transportProviderMock = mock(TransportProvider.class);
	private final Queue<Notification> queueMock = mock(Queue.class);
	private final Notification notificationMock = mock(Notification.class, "n1");
	private final Notification notificationMock2 = mock(Notification.class, "n2");
	private final NotificationManager notificationManagerMock = mock(NotificationManager.class);
	private final MacroContext eventMock = mock(MacroContext.class);
	private final NotificationTemplate notificationTemplateMock = mock(NotificationTemplate.class);
	private final Message messageMock = mock(Message.class);
	private final LocalizationService localizationMock = mock(LocalizationService.class);
	private final NotificationStateManager stateManagerMock = mock(NotificationStateManager.class);
    private final Map<String,Object> macroCtxProps = mock(Map.class);

	@Test
	public void newNotificationManager() throws Exception {
		new AlertNotificationManager(queueMock, transportProviderMock);
	}

	@Test
	public void submitNewNotification() throws Exception {

		checking(new Expectations() {
			{
				exactly(1).of(notificationMock).getId();
				exactly(1).of(queueMock).add(notificationMock);
			}
		});

		NotificationManager nm = new AlertNotificationManager(queueMock, transportProviderMock);
		nm.raise(notificationMock);

		assertEquals(1, nm.getActiveNotifications().size());

	}

	@Test
	public void submitNewNotificationAndRemoveImmediately() throws Exception {

		final Sequence seq = sequence("order");
		checking(new Expectations() {
			{
				exactly(1).of(notificationMock).getId();
				will(returnValue("ID"));
				inSequence(seq);

				exactly(1).of(queueMock).add(notificationMock);
				inSequence(seq);

				exactly(1).of(queueMock).remove(notificationMock);
				inSequence(seq);

			}
		});

		NotificationManager nm = new AlertNotificationManager(queueMock, transportProviderMock);
		nm.raise(notificationMock);
		assertEquals(1, nm.getActiveNotifications().size());

		nm.remove("ID");

	}

	@Test
	public void reapOneStepActiveNotification() throws Exception {

		final AlertNotificationManager manager = new AlertNotificationManager(queueMock, transportProviderMock);

		final Sequence seq = sequence("order");

		checking(new Expectations() {
			{
				exactly(1).of(queueMock).isEmpty();
				will(returnValue(false));
				inSequence(seq);

				exactly(1).of(queueMock).poll();
				will(returnValue(notificationMock));
				inSequence(seq);

				exactly(1).of(notificationMock).getNextFireTime();
				will(returnValue(new Date()));
				inSequence(seq);

				exactly(1).of(notificationMock).process(manager);
				inSequence(seq);

				exactly(1).of(notificationMock).isActive();
				will(returnValue(false));
				inSequence(seq);

				exactly(1).of(queueMock).isEmpty();
				will(returnValue(true));
				inSequence(seq);

			}
		});

		manager.run();
	}

	@Test
	public void reapTwoStepActiveNotification() throws Exception {
		final AlertNotificationManager manager = new AlertNotificationManager(queueMock, transportProviderMock);

		final Sequence seq = sequence("order");

		checking(new Expectations() {
			{
				exactly(1).of(notificationMock).getId();
				will(returnValue("n1"));
				inSequence(seq);

				exactly(1).of(queueMock).add(notificationMock);
				inSequence(seq);

				exactly(1).of(queueMock).isEmpty();
				will(returnValue(false));
				inSequence(seq);

				exactly(1).of(queueMock).poll();
				will(returnValue(notificationMock));
				inSequence(seq);

				exactly(1).of(notificationMock).getNextFireTime();
				will(returnValue(new Date()));
				inSequence(seq);

				exactly(1).of(notificationMock).process(manager);
				inSequence(seq);

				exactly(1).of(notificationMock).isActive();
				will(returnValue(true));
				inSequence(seq);

				exactly(1).of(notificationMock).getId();
				will(returnValue("n1"));
				inSequence(seq);

				exactly(1).of(queueMock).add(notificationMock);
				inSequence(seq);

				exactly(1).of(queueMock).isEmpty();
				will(returnValue(true));
				inSequence(seq);

			}
		});

		manager.raise(notificationMock);
		manager.run();

	}

	@Test
	public void reapNotificationWhichShouldBeFiredInSomeTimeLater() throws Exception {

		AlertNotificationManager manager = new AlertNotificationManager(queueMock, transportProviderMock);

		final Sequence seq = sequence("order");

		checking(new Expectations() {
			{
				exactly(1).of(notificationMock).getId();
				will(returnValue("n1"));
				inSequence(seq);

				exactly(1).of(queueMock).add(notificationMock);
				inSequence(seq);

				exactly(1).of(queueMock).isEmpty();
				will(returnValue(false));
				inSequence(seq);

				exactly(1).of(queueMock).poll();
				will(returnValue(notificationMock));
				inSequence(seq);

				exactly(1).of(notificationMock).getNextFireTime();
				will(returnValue(new Date(System.currentTimeMillis() + 60 * 60 * 1000)));
				inSequence(seq);

				exactly(1).of(notificationMock).getId();
				will(returnValue("n1"));
				inSequence(seq);

				exactly(1).of(queueMock).add(notificationMock);
				inSequence(seq);

				exactly(1).of(queueMock).isEmpty();
				will(returnValue(false));
				inSequence(seq);

			}
		});

		manager.raise(notificationMock);

		manager.run();

	}

	@Test
	public void reapTwoMultipleStepAlredyExpiredNotifications() throws Exception {

		final AlertNotificationManager manager = new AlertNotificationManager(queueMock, transportProviderMock);

		final Sequence seq = sequence("order");

		checking(new Expectations() {
			{
				exactly(1).of(notificationMock).getId();
				will(returnValue("n1"));
				inSequence(seq);

				exactly(1).of(queueMock).add(notificationMock);
				inSequence(seq);

				exactly(1).of(queueMock).isEmpty();
				will(returnValue(false));
				inSequence(seq);

				exactly(1).of(queueMock).poll();
				will(returnValue(notificationMock));
				inSequence(seq);

				exactly(1).of(notificationMock).getNextFireTime();
				will(returnValue(new Date()));
				// System.currentTimeMillis()+60*60*1000
				inSequence(seq);

				exactly(1).of(notificationMock).process(manager);
				inSequence(seq);

				exactly(1).of(notificationMock).isActive();
				will(returnValue(true));
				inSequence(seq);

				exactly(1).of(notificationMock).getId();
				will(returnValue("n1"));
				inSequence(seq);

				exactly(1).of(queueMock).add(notificationMock);
				inSequence(seq);

				exactly(1).of(queueMock).isEmpty();
				will(returnValue(false));
				inSequence(seq);

				exactly(1).of(queueMock).poll();
				will(returnValue(notificationMock));
				inSequence(seq);

				exactly(1).of(notificationMock).getNextFireTime();
				will(returnValue(new Date()));
				inSequence(seq);

				exactly(1).of(notificationMock).process(manager);
				inSequence(seq);

				exactly(1).of(notificationMock).isActive();
				will(returnValue(true));
				inSequence(seq);

				exactly(1).of(notificationMock).getId();
				will(returnValue("n1"));
				inSequence(seq);

				exactly(1).of(queueMock).add(notificationMock);
				inSequence(seq);

				exactly(1).of(queueMock).isEmpty();
				will(returnValue(true));
				inSequence(seq);
			}
		});

		manager.raise(notificationMock);
		manager.run();

	}

	@Test(expected = IllegalArgumentException.class)
	public void notificationHasAtLeastOneState() throws Exception {
		new AlertNotification("1", Collections.<NotificationState> emptyList(), eventMock, notificationTemplateMock);
	}

	@Test
	public void notificationTwoStateSwitching() throws Exception {
		// two notifications with interval 1000
		NotificationState state1 = new NotificationState("state1", 1, 2, 1000);
		// then one notification with interval 5000
		NotificationState state2 = new NotificationState("state2", 2, 1, 5000);

		final List<Message> messages = Arrays.asList(messageMock);

		final Sequence seq = sequence("order");

		checking(new Expectations() {
			{

				exactly(1).of(notificationTemplateMock).generateMessages(with("state1"), with(any(MacroContext.class)));
				will(returnValue(messages));
				inSequence(seq);

				exactly(1).of(notificationManagerMock).send(messageMock);
				inSequence(seq);

				exactly(1).of(notificationTemplateMock).generateMessages(with("state1"), with(any(MacroContext.class)));
				will(returnValue(messages));
				inSequence(seq);

				exactly(1).of(notificationManagerMock).send(messageMock);
				inSequence(seq);

				exactly(1).of(notificationTemplateMock).generateMessages(with("state2"), with(any(MacroContext.class)));
				will(returnValue(messages));
				inSequence(seq);

				exactly(1).of(notificationManagerMock).send(messageMock);
				inSequence(seq);
			}
		});

		Notification notification = new AlertNotification("2", Arrays.asList(state1, state2), new MacroContext(),
		        notificationTemplateMock);

		notification.process(notificationManagerMock);
		assertEquals(notification.getNextFireTime(), new Date(notification.getLastFireTime().getTime() + 1000));

		notification.process(notificationManagerMock);
		// state1 is done here
		assertEquals(notification.getNextFireTime(), new Date(notification.getLastFireTime().getTime() + 1000));

		notification.process(notificationManagerMock);
		// state2 is done here
		assertFalse(notification.isActive());

		notification.process(notificationManagerMock);
		assertFalse(notification.isActive());

	}

	@Test
	public void notificationGenerateMessage() throws Exception {

		EscalationTier t = new EscalationTier("t1", 1, 2, 10000);
		t.addMainItem(new NTemplateItem(MessageType.SMS.name(), MessageType.SMS, destinationsMock));

		Map<String, MessageTemplate> templates = CollectionUtils.newMap();
		templates.put(MessageType.SMS.name(), new CommonMessageTemplate("header", "body"));

		AlertNotificationTemplate nt = new AlertNotificationTemplate("ALERT TYPE", "MAJOR", templates,
		        Arrays.asList(t), macroReplacerMock, stateManagerMock);

		final List<Destination> destinations = CollectionUtils.newList();
		destinations.add(destMock);

		checking(new Expectations() {
			{
				exactly(1).of(destinationsMock).getDestinations();
				will(returnValue(destinations));

				exactly(1).of(destMock).getUserContext();
				will(returnValue(userContextMock));

				exactly(1).of(userContextMock).getLocale();
				Locale locale = Locale.ENGLISH;
				will(returnValue(locale));

				eventMock.localizationService = localizationMock;
				eventMock.alert=alert;
                eventMock.props=macroCtxProps;
				exactly(1).of(localizationMock).getTranslation("header", locale);
				will(returnValue("localized_header"));
				
//				exactly(1).of(localizationMock).getTranslation("header", locale);
//				will(returnValue("localized_header"));

				exactly(1).of(localizationMock).getTranslation("body", locale);
				will(returnValue("localized_body"));

				exactly(1).of(macroReplacerMock).expandIn("localized_header", eventMock);
				will(returnValue("header_replaced"));

				exactly(1).of(macroReplacerMock).expandIn("localized_body", eventMock);
				will(returnValue("body_replaced"));
				
				exactly(1).of(alert).getAlertId();
				will(returnValue(100));

                exactly(1).of(macroCtxProps).entrySet();
                will(returnValue(Collections.emptySet()));
			}
		});

		List<Message> messages = nt.generateMessages(t.getName(), eventMock);
		assertEquals(1, messages.size());
		Message m = messages.iterator().next();
		assertEquals("header_replaced", m.getHeader());
		// only body can be replaced
		assertEquals("body_replaced", m.getContent());

	}

	@Test(expected = IllegalStateException.class)
	public void generateMessageButTierDoesNotExistFail() throws Exception {
		EscalationTier t = new EscalationTier("t1", 1, 2, 10000);
		t.addMainItem(new NTemplateItem(MessageType.SMS.name(), MessageType.SMS, destinationsMock));

		Map<String, MessageTemplate> templates = CollectionUtils.newMap();
		templates.put(MessageType.SMS.name(), new CommonMessageTemplate("header", "body"));

		AlertNotificationTemplate nt = new AlertNotificationTemplate("ALERT TYPE", "CRITICAL", templates,
		        Arrays.asList(t), macroReplacerMock, stateManagerMock);

		nt.generateMessages("NO_TIER", eventMock);
	}

	@Test
	public void reapTwoStepActiveNotificationOnRealQueue() throws Exception {
		// Note: this comparator imposes orderings that are inconsistent with
		// equals
		Queue<Notification> queue = new PriorityQueue<Notification>(10, new Comparator<Notification>() {
			@Override
			public int compare(final Notification n1, final Notification n2) {
				Date n1Next = n1.getNextFireTime();
				Date n2Next = n2.getNextFireTime();

				if (n1Next.after(n2Next)) {
					return 1;
				}
				if (n1Next.before(n2Next)) {
					return -1;
				}
				return 0;
			}
		});

		final AlertNotificationManager manager = new AlertNotificationManager(queue, transportProviderMock);

		final Sequence seq = sequence("order");

		checking(new Expectations() {
			{

				exactly(1).of(notificationMock).getId();
				will(returnValue("n1"));
				inSequence(seq);

				exactly(1).of(notificationMock2).getId();
				will(returnValue("n2"));
				inSequence(seq);

				// from queue
				exactly(1).of(notificationMock2).getNextFireTime();
				inSequence(seq);
				will(returnValue(new Date(System.currentTimeMillis() + 10000L)));

				// from queue
				exactly(1).of(notificationMock).getNextFireTime();
				inSequence(seq);
				will(returnValue(new Date()));

				exactly(1).of(notificationMock).getNextFireTime();
				inSequence(seq);
				will(returnValue(new Date()));

				exactly(1).of(notificationMock).process(manager);
				inSequence(seq);

				exactly(1).of(notificationMock).isActive();
				inSequence(seq);
				will(returnValue(true));

				exactly(1).of(notificationMock).getId();
				will(returnValue("n1"));
				inSequence(seq);

				exactly(1).of(notificationMock).getNextFireTime();
				inSequence(seq);
				Date n1Time = new Date(System.currentTimeMillis() + 100000L);
				will(returnValue(n1Time));

				exactly(1).of(notificationMock2).getNextFireTime();
				inSequence(seq);
				will(returnValue(new Date(System.currentTimeMillis() + 10000L)));

				exactly(1).of(notificationMock2).getNextFireTime();
				inSequence(seq);
				will(returnValue(n1Time));

				exactly(1).of(notificationMock2).getId();
				will(returnValue("n2"));
				inSequence(seq);

				exactly(1).of(notificationMock2).getNextFireTime();
				inSequence(seq);
				will(returnValue(n1Time));

				exactly(1).of(notificationMock).getNextFireTime();
				inSequence(seq);
				will(returnValue(n1Time));
			}
		});

		manager.raise(notificationMock);
		manager.raise(notificationMock2);

		manager.run();

	}

	@Test
	public void reapingLastTierNotificationShouldNotRemoveItFromIndex() throws Exception {
		final AlertNotificationManager manager = new AlertNotificationManager(queueMock, transportProviderMock);

		final Sequence order = sequence("order");

		checking(new Expectations() {
			{
				exactly(1).of(notificationMock).getId();
				will(returnValue("id"));
				inSequence(order);

				exactly(1).of(queueMock).add(notificationMock);
				inSequence(order);

				exactly(1).of(queueMock).isEmpty();
				will(returnValue(false));
				inSequence(order);

				exactly(1).of(queueMock).poll();
				will(returnValue(notificationMock));
				inSequence(order);

				exactly(1).of(notificationMock).getNextFireTime();
				will(returnValue(new Date(System.currentTimeMillis() - 1000L)));
				inSequence(order);

				exactly(1).of(notificationMock).process(manager);
				inSequence(order);

				exactly(1).of(notificationMock).isActive();
				will(returnValue(false));
				inSequence(order);

				exactly(1).of(queueMock).isEmpty();
				will(returnValue(true));
				inSequence(order);

			}
		});

		manager.raise(notificationMock);
		manager.run();

		assertEquals(1, manager.getActiveNotifications().size());
	}

	@Test
	public void ackOfAlreadyReapedNotificationShouldSendMessage() throws Exception {
		AlertNotificationManager manager = new AlertNotificationManager(queueMock, transportProviderMock);

		final Sequence order = sequence("order");

		checking(new Expectations() {
			{
				exactly(1).of(notificationMock).getId();
				will(returnValue("id"));
				inSequence(order);

				exactly(1).of(queueMock).add(notificationMock);
				inSequence(order);

				exactly(1).of(notificationMock).currentState();
				will(returnValue("TIER3"));
				inSequence(order);

				exactly(1).of(notificationMock).generatedBy();
				will(returnValue(notificationTemplateMock));
				inSequence(order);

				exactly(1).of(notificationMock).getContext();
				will(returnValue(eventMock));
				inSequence(order);

				exactly(1).of(notificationTemplateMock).generateAckMessages("TIER3", eventMock);
				will(returnValue(Collections.EMPTY_LIST));
				inSequence(order);

				exactly(1).of(queueMock).remove(notificationMock);
				inSequence(order);

			}
		});

		manager.raise(notificationMock);
		manager.ack("id");
	}

	@Test
	public void resolveOfAlreadyReapedNotificationShouldSendMessage() throws Exception {
		AlertNotificationManager manager = new AlertNotificationManager(queueMock, transportProviderMock);

		final Sequence order = sequence("order");

		checking(new Expectations() {
			{
				exactly(1).of(notificationMock).getId();
				will(returnValue("id"));
				inSequence(order);

				exactly(1).of(queueMock).add(notificationMock);
				inSequence(order);

				exactly(1).of(notificationMock).currentState();
				will(returnValue("TIER3"));
				inSequence(order);

				exactly(1).of(notificationMock).generatedBy();
				will(returnValue(notificationTemplateMock));
				inSequence(order);

				exactly(1).of(notificationMock).getContext();
				will(returnValue(eventMock));
				inSequence(order);

				exactly(1).of(notificationTemplateMock).generateResolveMessages("TIER3", eventMock);
				will(returnValue(Collections.EMPTY_LIST));
				inSequence(order);

				exactly(1).of(queueMock).remove(notificationMock);
				inSequence(order);
			}
		});

		manager.raise(notificationMock);
		manager.resolve("id");
	}

	@Test
	public void dismissOfAlreadyReapedNotificationShouldNotSendAnyMessages() throws Exception {
		AlertNotificationManager manager = new AlertNotificationManager(queueMock, transportProviderMock);

		final Sequence order = sequence("order");

		checking(new Expectations() {
			{
				exactly(1).of(notificationMock).getId();
				will(returnValue("id"));
				inSequence(order);

				exactly(1).of(queueMock).add(notificationMock);
				inSequence(order);

				exactly(1).of(queueMock).remove(notificationMock);
				inSequence(order);
			}
		});

		manager.raise(notificationMock);
		manager.dismiss("id");
	}

	@Test(expected = IllegalArgumentException.class)
	public void notificationStateIntervalOutOfBounds() throws Exception {
		new NotificationState("state1", 1, 2, 0);
	}

	@Test(expected = IllegalArgumentException.class)
	public void notificationStateRepeatLimitOutOfBound() throws Exception {
		new NotificationState("state1", 1, -1, 10);
		new NotificationState("state2", 1, 101, 10);
	}

    @Test
    public void lastAttemptCanBeEqualToRepeatLimit() throws Exception {
        new NotificationState("TIER1", 1, 3, 10000L, 3);
    }

	@Test(expected = IllegalArgumentException.class)
	public void lastAttemptShouldBeLesserThanRepeatLimit() throws Exception {
		new NotificationState("TIER1", 1, 3, 10000L, 4);
	}

}
