package com.synapsense.impl.reporting.tasks;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ CompositeReportingTaskTest.class, GetValueIdsTaskTest.class, GetEnvDataTaskTest.class })
public class TasksTestSuite {
}
