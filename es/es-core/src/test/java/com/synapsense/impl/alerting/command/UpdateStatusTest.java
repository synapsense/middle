package com.synapsense.impl.alerting.command;

import static org.junit.Assert.assertNotNull;

import java.util.Collection;

import mockit.Expectations;

import mockit.Mocked;
import mockit.integration.junit4.JMockit;
import org.junit.Test;

import com.synapsense.commandprocessor.api.Command;
import com.synapsense.dto.AlertStatus;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.AlertServiceSystemException;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.exception.PropertyNotFoundException;
import com.synapsense.exception.UnableToConvertPropertyException;
import com.synapsense.service.Environment;
import com.synapsense.util.CollectionUtils;
import org.junit.runner.RunWith;

@RunWith(JMockit.class)
public class UpdateStatusTest {
	@Test
	public void instantiateCommand(@Mocked Environment env, @Mocked Collection<TO<?>> alerts,
								   @Mocked TO<?> user) {
		Command raise = new UpdateStatus(env, alerts, AlertStatus.ACKNOWLEDGED, "", user, 0L);
		assertNotNull(raise);
	}

	@Test
	public void updateStatusSetAcknowledge(@Mocked Environment env, @Mocked TO<?> user)
	        throws ObjectNotFoundException, PropertyNotFoundException, UnableToConvertPropertyException {
		Collection<TO<?>> alerts = CollectionUtils.newSet();
		final TO<?> alert = TOFactory.INSTANCE.loadTO("ALERT", 1);
		alerts.add(alert);

		final long time = System.currentTimeMillis();

		Command updateStatus = new UpdateStatus(env, alerts, AlertStatus.ACKNOWLEDGED, "comment", user, time);

		new Expectations() {
			{

				env.getPropertyValue(alert, "status", Integer.class);
				times = 1;
				returns(0);

				env.getPropertyValue(alert, "acknowledgement", String.class);
				times = 1;
				returns("acknowledgement");

				env.getPropertyValue(user, "FName", String.class);
				times = 1;
				returns("Admin");

				env.getPropertyValue(user, "LName", String.class);
				times = 1;
				returns("Admin");

				ValueTO[] values = new ValueTO[4];
				values[0] = new ValueTO("status", AlertStatus.ACKNOWLEDGED.code());
				values[1] = new ValueTO("acknowledgement", AlertStatus.ACKNOWLEDGED.compoundComment("acknowledgement")
				        .concat("comment"));
				values[2] = new ValueTO("acknowledgementTime", time);
				values[3] = new ValueTO("user", "Admin Admin");

				env.setAllPropertiesValues(alert, withEqual(values), true);
				times = 1;
			}
		};

		updateStatus.execute();
	}

	@Test
	public void updateStatusSetResolved(@Mocked Environment env, @Mocked TO<?> user)
	        throws ObjectNotFoundException, PropertyNotFoundException, UnableToConvertPropertyException {
		Collection<TO<?>> alerts = CollectionUtils.newSet();
		final TO<?> alert = TOFactory.INSTANCE.loadTO("ALERT", 1);
		alerts.add(alert);

		final long time = System.currentTimeMillis();

		Command updateStatus = new UpdateStatus(env, alerts, AlertStatus.RESOLVED, "comment", user, time);

		new Expectations() {
			{
				env.getPropertyValue(alert, "status", Integer.class);
				times = 1;
				returns(0);

				env.getPropertyValue(alert, "acknowledgement", String.class);
				times = 1;
				returns("acknowledgement");

				env.getPropertyValue(user, "FName", String.class);
				times = 1;
				returns("Admin");

				env.getPropertyValue(user, "LName", String.class);
				times = 1;
				returns("Admin");

				ValueTO[] values = new ValueTO[4];
				values[0] = new ValueTO("status", AlertStatus.RESOLVED.code());
				values[1] = new ValueTO("acknowledgement", AlertStatus.RESOLVED.compoundComment("acknowledgement")
				        .concat("comment"));
				values[2] = new ValueTO("acknowledgementTime", time);
				values[3] = new ValueTO("user", "Admin Admin");

				env.setAllPropertiesValues(alert, withEqual(values),false);
				times = 1;
			}
		};

		updateStatus.execute();
	}

	@Test
	public void updateStatusSetResolvedUserIsNull(@Mocked Environment env)
	        throws ObjectNotFoundException, PropertyNotFoundException, UnableToConvertPropertyException {
		Collection<TO<?>> alerts = CollectionUtils.newSet();
		final TO<?> alert = TOFactory.INSTANCE.loadTO("ALERT", 1);
		alerts.add(alert);

		final long time = System.currentTimeMillis();

		Command updateStatus = new UpdateStatus(env, alerts, AlertStatus.RESOLVED, "comment", null, time);

		new Expectations() {
			{
				env.getPropertyValue(alert, "status", Integer.class);
				times = 1;
				returns(0);

				env.getPropertyValue(alert, "acknowledgement", String.class);
				times = 1;
				returns("acknowledgement");

				ValueTO[] values = new ValueTO[3];
				values[0] = new ValueTO("status", AlertStatus.RESOLVED.code());
				values[1] = new ValueTO("acknowledgement", AlertStatus.RESOLVED.compoundComment("acknowledgement")
				        .concat("comment"));
				values[2] = new ValueTO("acknowledgementTime", time);

				env.setAllPropertiesValues(alert, withEqual(values),false);
				times = 1;
			}
		};

		updateStatus.execute();
	}

	@Test
	public void updateToTheSameStatusFailure(@Mocked Environment env, @Mocked TO<?> user)
	        throws ObjectNotFoundException, PropertyNotFoundException, UnableToConvertPropertyException {
		Collection<TO<?>> alerts = CollectionUtils.newSet();
		final TO<?> alert = TOFactory.INSTANCE.loadTO("ALERT", 1);
		alerts.add(alert);

		Command updateStatus = new UpdateStatus(env, alerts, AlertStatus.RESOLVED, "comment", user, 0L);

		new Expectations() {
			{
				env.getPropertyValue(alert, "status", Integer.class);
				times = 1;
				returns(3);
				// early return if status is not approachable
			}
		};

		updateStatus.execute();
	}

	@Test(expected = AlertServiceSystemException.class)
	public void updateStatusPropertyStatusIsNull(@Mocked Environment env, @Mocked TO<?> user)
	        throws ObjectNotFoundException, PropertyNotFoundException, UnableToConvertPropertyException {
		Collection<TO<?>> alerts = CollectionUtils.newSet();
		final TO<?> alert = TOFactory.INSTANCE.loadTO("ALERT", 1);
		alerts.add(alert);

		Command updateStatus = new UpdateStatus(env, alerts, AlertStatus.RESOLVED, "comment", user, 0L);

		new Expectations() {
			{
				env.getPropertyValue(alert, "status", Integer.class);
				times = 1;
				returns(null);
			}
		};

		updateStatus.execute();
	}

}
