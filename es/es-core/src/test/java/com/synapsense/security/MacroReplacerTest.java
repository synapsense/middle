package com.synapsense.security;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;

import org.junit.BeforeClass;
import org.junit.Test;

public class MacroReplacerTest {

	private static Map<String, String> values = new HashMap<String, String>();

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		values.put("username", "Alice");
		values.put("password", "secret");
		values.put("Kampfpanzer", "Jagdpanzer");
	}

	@Test
	public void testReplacement() {
		assertEquals("Das Jagdpanzer ist ein Sturmgeschütz",
		        MacroReplacer.replace("Das {$Kampfpanzer} ist ein Sturmgeschütz", values));

		assertEquals("mydomain\\Alice", MacroReplacer.replace("mydomain\\{$username}", values));
		assertEquals("secret", MacroReplacer.replace("{$password}", values));
	}

	@Test
	public void testNoMacroString() {
		String in = "A qui{$}ck brown fox{}";
		String out = MacroReplacer.replace(in, null);
		assertEquals(in, out);
	}

	@Test
	public void testNoReplacementValue() {
		String in = "Doctor {$Who}?";
		String out = MacroReplacer.replace(in, values);
		assertEquals(in, out);
	}

}
