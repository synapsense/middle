package com.synapsense.security;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.Principal;
import java.security.acl.Group;
import java.util.Enumeration;
import java.util.Scanner;

import javax.security.auth.Subject;
import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.NameCallback;
import javax.security.auth.callback.PasswordCallback;
import javax.security.auth.callback.TextOutputCallback;
import javax.security.auth.callback.UnsupportedCallbackException;
import javax.security.auth.login.Configuration;
import javax.security.auth.login.LoginContext;

import junit.framework.Assert;

import org.jboss.security.auth.login.XMLLoginConfigImpl;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

public class LdapExtLoginModuleTest {

	private static XMLLoginConfigImpl xmlLoginConfig = null;
	private static LoginContext lc = null;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		System.setProperty("jboss.security.disable.secdomain.option", "true");
		xmlLoginConfig = XMLLoginConfigImpl.getInstance();
		Configuration.setConfiguration(xmlLoginConfig);
		xmlLoginConfig.setConfigResource("com/synapsense/security/test-policies.xml");
		xmlLoginConfig.loadConfig();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	@Ignore
	public void test() throws Exception {
		Assert.assertNotNull(Configuration.getConfiguration().getAppConfigurationEntry("LDAP Domain 1"));
		lc = new LoginContext("LDAP Domain 1", new TextCallbackHandler());
		lc.login();
		Subject subj = lc.getSubject();
		for (Principal principal : subj.getPrincipals()) {
			if (principal instanceof Group && "Roles".equals(principal.getName())) {
				Enumeration<? extends Principal> roles = ((Group) principal).members();
				while (roles.hasMoreElements()) {
					System.out.println("Role: " + roles.nextElement().getName());
				}
			}
		}
		lc.logout();
	}

	@Test
	public void testDelegation() throws Exception {
		Assert.assertNotNull(Configuration.getConfiguration().getAppConfigurationEntry("LDAP Domain 1"));
		Assert.assertNotNull(Configuration.getConfiguration().getAppConfigurationEntry("Main"));
		lc = new LoginContext("Main", new TextCallbackHandler());
		lc.login();
		Subject subj = lc.getSubject();
		for (Principal principal : subj.getPrincipals()) {
			if (principal instanceof Group && "Roles".equals(principal.getName())) {
				Enumeration<? extends Principal> roles = ((Group) principal).members();
				while (roles.hasMoreElements()) {
					System.out.println("Role: " + roles.nextElement().getName());
				}
			}
		}
		lc.logout();
	}

}

class TextCallbackHandler implements CallbackHandler {

	public void handle(Callback[] callbacks) throws IOException, UnsupportedCallbackException {

		for (int i = 0; i < callbacks.length; i++) {
			if (callbacks[i] instanceof TextOutputCallback) {

				// display the message according to the specified type
				TextOutputCallback toc = (TextOutputCallback) callbacks[i];
				switch (toc.getMessageType()) {
				case TextOutputCallback.INFORMATION:
					System.out.println(toc.getMessage());
					break;
				case TextOutputCallback.ERROR:
					System.out.println("ERROR: " + toc.getMessage());
					break;
				case TextOutputCallback.WARNING:
					System.out.println("WARNING: " + toc.getMessage());
					break;
				default:
					throw new IOException("Unsupported message type: " + toc.getMessageType());
				}

			} else if (callbacks[i] instanceof NameCallback) {

				// prompt the user for a username
				NameCallback nc = (NameCallback) callbacks[i];

				// ignore the provided defaultName
				System.err.print(nc.getPrompt());
				System.err.flush();
				nc.setName((new BufferedReader(new InputStreamReader(System.in))).readLine());

			} else if (callbacks[i] instanceof PasswordCallback) {

				// prompt the user for sensitive information
				PasswordCallback pc = (PasswordCallback) callbacks[i];
				System.err.print(pc.getPrompt());
				System.err.flush();
				pc.setPassword(readPassword(System.in));

			} else {
				throw new UnsupportedCallbackException(callbacks[i], "Unrecognized Callback");
			}
		}
	}

	// Reads user password from given input stream.
	private char[] readPassword(InputStream in) throws IOException {
		try (Scanner scanner = new Scanner(in)) {
			return scanner.nextLine().toCharArray();
		}
	}

}