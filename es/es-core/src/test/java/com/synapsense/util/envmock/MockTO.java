package com.synapsense.util.envmock;

import com.synapsense.dto.TO;
import com.synapsense.service.impl.dao.to.AbstractTO;

public class MockTO extends AbstractTO<String> implements TO<String> {

	private static final long serialVersionUID = -5014551469003433578L;

	public MockTO(String objName, String typeName) {
		super(objName, typeName);
	}

}
