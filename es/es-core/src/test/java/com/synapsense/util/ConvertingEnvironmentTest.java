package com.synapsense.util;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.synapsense.dto.CollectionTO;
import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;
import com.synapsense.impl.unitsystems.UnitSystemsServiceImpl;
import com.synapsense.util.envmock.EnvironmentMock;
import com.synapsense.util.envmock.MockEnvObject;
import com.synapsense.util.envmock.MockTO;

public class ConvertingEnvironmentTest {

	private static EnvironmentMock mockEnv;
	private static ConvertingEnvironmentProxy cEnv;
	private static ConvertingEnvironment testEnv;


	@BeforeClass
	public static void setUp() throws Exception {
		cEnv = new ConvertingEnvironmentProxy();
		mockEnv = new EnvironmentMock();

		URL url = ConvertingEnvironmentTest.class.getResource("mockconfig.xml");
		File confFolder = new File(url.getFile()).getParentFile();
		System.setProperty("com.synapsense.filestorage", confFolder.getCanonicalPath().replaceAll("%20", " "));
		System.setProperty("com.synapsense.unitsystems.usfile", "unitconverter/systems.xml");
		System.setProperty("com.synapsense.unitsystems.urfile", "mockconfig.xml");
		UnitSystemsServiceImpl impl = new UnitSystemsServiceImpl();
		impl.create();
		impl.start();

		cEnv.setUnitSystems(impl.getUnitSystems());
		cEnv.setUnitResolvers(impl.getUnitReslovers());
		cEnv.setProxiedEnvironment(mockEnv);
		testEnv = cEnv.getProxy();
	}

	@Test(expected = ConvertingEnvironmentException.class)
	public final void testSetWrongTargetSystem() throws Exception {
		cEnv.setTargetSystem("NoSuchSystem");
	}

	@Test(expected = ConvertingEnvironmentException.class)
	public final void testSetWrongTargetSystem2() throws Exception {
		testEnv.setTargetSystem("NoSuchSystem");
	}

	@Test
	public final void testSetGetDimension() throws Exception {
		MockTO to = new MockTO("dummy", "RACK");
		testEnv.setTargetSystem(null);
		assertEquals("Server", cEnv.getBaseSystem(to, "avgTopTemp").getName());
		assertEquals("°F", cEnv.getDimension(to, "avgTopTemp", null).getUnits());
		assertEquals("°F", testEnv.getDimension(to, "avgTopTemp").getUnits());
		assertEquals("°C", cEnv.getDimension(to, "avgTopTemp", "SI").getUnits());
		testEnv.setTargetSystem("SI");
		assertEquals("°C", testEnv.getDimension(to, "avgTopTemp").getUnits());
	}

	@Test(expected = ConvertingEnvironmentException.class)
	public final void testGetWrongDimension() throws Exception {
		MockTO to = new MockTO("dummy", "RACK");
		assertEquals("°F", cEnv.getDimension(to, "noProperty", null).getUnits());
	}

	@Test
	public final void testGetWrongDimension2() throws Exception {
		MockTO to = new MockTO("dummy", "RACK");
		assertEquals(null, testEnv.getDimension(to, "noProperty"));
	}

	@Test
	public final void testGetConvertedPropertyValue() throws Exception {
		final Double tempC = 24.0;
		final Double tempF = tempC * 9 / 5 + 32;

		MockEnvObject obj = new MockEnvObject("ObjTest1");
		obj.setProperty("avgTopTemp", tempF);
		MockTO to = new MockTO(obj.getName(), "RACK");
		mockEnv.getObjects().put(to, obj);

		testEnv.setTargetSystem(null);
		assertEquals(tempF, testEnv.getPropertyValue(to, "avgTopTemp", Double.class));
		assertEquals(tempC, testEnv.convertValueToBaseSystem(to, "avgTopTemp", tempC));
		assertEquals(tempF, testEnv.convertValueToBaseSystem(to, "avgTopTemp", tempF));

		cEnv.setTargetSystem("SI");
		assertEquals(tempC, testEnv.getPropertyValue(to, "avgTopTemp", Double.class));
		assertEquals(tempF, testEnv.convertValueToBaseSystem(to, "avgTopTemp", tempC));
	}

	@Test
	public final void testGetNonConvertiblePropertyValue() throws Exception {
		Integer val = 42;
		String propName = "ratio";
		MockEnvObject obj = new MockEnvObject("ObjTest1");
		obj.setProperty(propName, new Integer(val));
		MockTO to = new MockTO(obj.getName(), "RACK");
		mockEnv.getObjects().put(to, obj);

		assertEquals(val, testEnv.getPropertyValue(to, propName, Integer.class));

		cEnv.setTargetSystem("SI");
		assertEquals(val, testEnv.getPropertyValue(to, propName, Double.class));
	}

	@Test
	public final void testGetNonDescribedPropertyValue() throws Exception {
		Integer val = 42;
		String propName = "noUnitsProperty";
		MockEnvObject obj = new MockEnvObject("ObjTest1");
		obj.setProperty(propName, new Integer(val));
		MockTO to = new MockTO(obj.getName(), "RACK");
		mockEnv.getObjects().put(to, obj);

		assertEquals(val, testEnv.getPropertyValue(to, propName, Integer.class));

		cEnv.setTargetSystem("SI");
		assertEquals(val, testEnv.getPropertyValue(to, propName, Double.class));
	}

	@Test
	public final void testGetNoConverterPropertyValue() throws Exception {
		Integer val = 42;
		String propName = "noConverterProp";
		MockEnvObject obj = new MockEnvObject("ObjTest1");
		obj.setProperty(propName, new Integer(val));
		MockTO to = new MockTO(obj.getName(), "RACK");
		mockEnv.getObjects().put(to, obj);

		cEnv.setTargetSystem(null);// disables conversion so the next line shall
								   // pass
		assertEquals(val, testEnv.getPropertyValue(to, propName, Integer.class));

		cEnv.setTargetSystem("SI");
		cEnv.setConversionEnabled(false);// disables conversion so the next line
										 // shall pass
		assertEquals(val, testEnv.getPropertyValue(to, propName, Integer.class));

		cEnv.setConversionEnabled(true);
		// this line shall pass with an exception logged
		assertEquals(val, testEnv.getPropertyValue(to, propName, Double.class));
	}

	@Test
	public final void testGetSensorPropertyValue() throws Exception {
		final Double tempC = 24.0;
		final Double tempF = tempC * 9 / 5 + 32;
		String propName = "lastValue";
		MockEnvObject obj = new MockEnvObject("ObjTest2");
		obj.setProperty(propName, new Double(tempF));
		obj.setProperty("dataclass", 200);
		MockTO to = new MockTO(obj.getName(), "SENSOR");
		mockEnv.getObjects().put(to, obj);

		cEnv.setTargetSystem(null);
		assertEquals(tempF, testEnv.getPropertyValue(to, propName, Double.class));

		cEnv.setTargetSystem("SI");
		assertEquals(tempC, testEnv.getPropertyValue(to, propName, Double.class));

		assertEquals("Server", cEnv.getBaseSystem(to, propName).getName());
		assertEquals("°F", cEnv.getDimension(to, propName, null).getUnits());
		assertEquals("°C", cEnv.getDimension(to, propName, "SI").getUnits());
	}

	@Test
	public final void testGetSensorSystemPropertyValue() throws Exception {
		String propName = "lastValue";
		MockEnvObject obj = new MockEnvObject("ObjTest2");
		obj.setProperty("dataclass", 200);
		MockTO to = new MockTO(obj.getName(), "SENSOR");
		mockEnv.getObjects().put(to, obj);

		cEnv.setTargetSystem(null);
		obj.setProperty(propName, -2000.0);
		assertEquals(new Double(-2000.0), testEnv.getPropertyValue(to, propName, Double.class));
		obj.setProperty(propName, -3000.0);
		assertEquals(new Double(-3000.0), testEnv.getPropertyValue(to, propName, Double.class));

		cEnv.setTargetSystem("SI");
		obj.setProperty(propName, -2000.0);
		assertEquals(new Double(-2000.0), testEnv.getPropertyValue(to, propName, Double.class));
		obj.setProperty(propName, -3000.0);
		assertEquals(new Double(-3000.0), testEnv.getPropertyValue(to, propName, Double.class));
	}

	@Test
	public final void testConvertNullValue() throws Exception {
		String propName = "lastValue";
		MockEnvObject obj = new MockEnvObject("ObjTest2");
		obj.setProperty("type", 200);
		MockTO to = new MockTO(obj.getName(), "SENSOR");
		mockEnv.getObjects().put(to, obj);

		cEnv.setTargetSystem(null);
		obj.setProperty(propName, null);
		assertEquals(null, testEnv.getPropertyValue(to, propName, Double.class));

		cEnv.setTargetSystem("SI");
		assertEquals(null, testEnv.getPropertyValue(to, propName, Double.class));
	}

	@Test
	public final void testGetStaticPropertyValue() throws Exception {
		final Double tempC = 24.0;
		final Double tempF = tempC * 9 / 5 + 32;
		final Integer ratio = 12;
		final String[] props = new String[] { "avgTopTemp", "ratio" };
		MockEnvObject obj = new MockEnvObject("Obj1");
		obj.setProperty(props[0], new Double(tempF));
		obj.setProperty(props[1], new Integer(ratio));
		MockTO to1 = new MockTO(obj.getName(), "RACK");
		mockEnv.getObjects().put(to1, obj);

		cEnv.setTargetSystem(null);
		assertEquals(tempF, testEnv.getPropertyValue("RACK", props[0], Double.class));
		assertEquals(ratio, testEnv.getPropertyValue("RACK", props[1], Double.class));

		cEnv.setTargetSystem("SI");
		assertEquals(tempC, testEnv.getPropertyValue("RACK", props[0], Double.class));
		assertEquals(ratio, testEnv.getPropertyValue("RACK", props[1], Double.class));
	}

	@Test
	public final void testGetPropertyValueMultiple() throws Exception {
		final double tempC = 24;
		final double tempF = tempC * 9 / 5 + 32;
		final int ratio = 12;
		final String[] props = new String[] { "avgTopTemp", "ratio" };
		MockEnvObject obj = new MockEnvObject("Obj1");
		obj.setProperty(props[0], new Double(tempF));
		obj.setProperty(props[1], new Integer(ratio));
		MockTO to1 = new MockTO(obj.getName(), "RACK");
		mockEnv.getObjects().put(to1, obj);
		MockTO to2 = new MockTO(obj.getName(), "RACK");
		mockEnv.getObjects().put(to2, obj);
		Collection<TO<?>> tos = new LinkedList<TO<?>>();
		tos.add(to1);
		tos.add(to2);

		cEnv.setTargetSystem(null);
		Collection<CollectionTO> res = testEnv.getPropertyValue(tos, props);
		assertEquals(2, res.size());
		for (CollectionTO cto : res) {
			assertEquals(tempF, getPropValue(cto.getPropValues(), props[0]));
			assertEquals(ratio, getPropValue(cto.getPropValues(), props[1]));
		}

		cEnv.setTargetSystem("SI");
		res = testEnv.getPropertyValue(tos, props);
		assertEquals(2, res.size());
		for (CollectionTO cto : res) {
			assertEquals(tempC, getPropValue(cto.getPropValues(), props[0]));
			assertEquals(ratio, getPropValue(cto.getPropValues(), props[1]));
		}
	}

	@Test
	public final void testGetAllPropertiesValues() throws Exception {
		final double tempC = 24;
		final double tempF = tempC * 9 / 5 + 32;
		final int ratio = 12;
		final String[] props = new String[] { "avgTopTemp", "ratio" };
		MockEnvObject obj = new MockEnvObject("Obj1");
		obj.setProperty(props[0], new Double(tempF));
		obj.setProperty(props[1], new Integer(ratio));
		MockTO to1 = new MockTO(obj.getName(), "RACK");
		mockEnv.getObjects().put(to1, obj);

		cEnv.setTargetSystem(null);
		Collection<ValueTO> res = testEnv.getAllPropertiesValues(to1);
		assertEquals(2, res.size());
		assertEquals(tempF, getPropValue(res, props[0]));
		assertEquals(ratio, getPropValue(res, props[1]));

		cEnv.setTargetSystem("SI");
		res = testEnv.getAllPropertiesValues(to1);
		assertEquals(2, res.size());
		assertEquals(tempC, getPropValue(res, props[0]));
		assertEquals(ratio, getPropValue(res, props[1]));
	}

	@Test
	public final void testGetAllPropertiesValues2() throws Exception {
		final double tempC = 24;
		final double tempF = tempC * 9 / 5 + 32;
		final int ratio = 12;
		final String[] props = new String[] { "avgTopTemp", "ratio" };
		MockEnvObject obj = new MockEnvObject("Obj1");
		obj.setProperty(props[0], new Double(tempF));
		obj.setProperty(props[1], new Integer(ratio));
		MockTO to1 = new MockTO(obj.getName(), "RACK");
		mockEnv.getObjects().put(to1, obj);
		MockTO to2 = new MockTO(obj.getName(), "RACK");
		mockEnv.getObjects().put(to2, obj);
		Collection<TO<?>> tos = new LinkedList<TO<?>>();
		tos.add(to1);
		tos.add(to2);

		cEnv.setTargetSystem(null);
		Collection<CollectionTO> res = testEnv.getAllPropertiesValues(tos);
		assertEquals(2, res.size());
		for (CollectionTO cto : res) {
			assertEquals(tempF, getPropValue(cto.getPropValues(), props[0]));
			assertEquals(ratio, getPropValue(cto.getPropValues(), props[1]));
		}

		cEnv.setTargetSystem("SI");
		res = testEnv.getAllPropertiesValues(tos);
		assertEquals(2, res.size());
		for (CollectionTO cto : res) {
			assertEquals(tempC, getPropValue(cto.getPropValues(), props[0]));
			assertEquals(ratio, getPropValue(cto.getPropValues(), props[1]));
		}
	}

	@Test
	public final void testGetHistory() throws Exception {
		final double[] tempC = new double[5];
		final double[] tempF = new double[5];
		ArrayList<ValueTO> history = new ArrayList<ValueTO>(tempC.length);
		for (int i = 0; i < tempC.length; i++) {
			tempC[i] = 25 + i * 1.74;
			tempF[i] = tempC[i] * 9 / 5 + 32;
			history.add(new ValueTO("avgTopTemp", tempF[i]));
		}

		MockEnvObject obj = new MockEnvObject("ObjTest1");
		obj.setHistory("avgTopTemp", history);
		MockTO to = new MockTO(obj.getName(), "RACK");
		mockEnv.getObjects().put(to, obj);

		cEnv.setTargetSystem(null);
		history = (ArrayList<ValueTO>) testEnv.getHistory(to, "avgTopTemp", Double.class);
		assertArrayEquals(tempF, extractValues(history));

		cEnv.setTargetSystem("SI");
		history = (ArrayList<ValueTO>) testEnv.getHistory(to, "avgTopTemp", Double.class);
		assertArrayEquals(tempC, extractValues(history));
	}

	private static Object getPropValue(final Collection<ValueTO> vtos, final String propName) {
		for (ValueTO vto : vtos) {
			if (vto.getPropertyName().equals(propName))
				return vto.getValue();
		}
		return null;
	}

	private static double[] extractValues(final Collection<ValueTO> values) {
		double[] res = new double[values.size()];
		int i = 0;
		for (ValueTO val : values) {
			res[i] = (Double) val.getValue();
			i++;
		}
		return res;
	}

	private static void assertArrayEquals(final double[] a1, final double a2[]) {
		assertEquals(a1.length, a2.length);
		for (int i = 0; i < a1.length; i++)
			assertEquals(a1[i], a2[i], 0.5f);
	}
}
