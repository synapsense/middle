package com.synapsense.util.unitconverter;

import static org.junit.Assert.*;

import java.io.InputStreamReader;

import org.junit.BeforeClass;
import org.junit.Test;

import com.synapsense.util.unitconverter.ConverterException;
import com.synapsense.util.unitconverter.UnitSystems;
import com.synapsense.util.unitconverter.Dimension;
import com.synapsense.util.unitconverter.SystemConverter;
import com.synapsense.util.unitconverter.UnitConverter;

public class ConvertersMapTest {

	private static UnitSystems unitSystems;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		InputStreamReader systems = new InputStreamReader(ConvertersMapTest.class.getResourceAsStream("systems.xml"));
		unitSystems = new UnitSystems();
		unitSystems.loadFromXml(systems);
	}

	@Test
	public final void testUnitSystemCollection() throws Exception {
		assertEquals(3, unitSystems.getUnitSystems().values().size());
		assertEquals("SI", unitSystems.getUnitSystem("SI").getName());
		assertEquals("Server", unitSystems.getUnitSystem("Server").getName());
		assertEquals("Imperial US", unitSystems.getUnitSystem("Imperial US").getName());
		assertEquals(4, unitSystems.getUnitSystem("Server").getDimensionsMap().values().size());
		try {
			unitSystems.getUnitSystem("NoSuchSystem");
			fail();
		} catch (ConverterException e) {
			// this one was expected
		}
	}

	@Test
	public final void testNonConvertibleDimensions() {
		assertEquals(14, unitSystems.getSimpleDimensions().size());
		assertEquals("%", unitSystems.getSimpleDimensions().get("percents").getUnits());
	}

	@Test
	public final void testNonConvertibleDimensions2() throws Exception {
		assertEquals(98.0, unitSystems.getUnitConverter("SI", "Imperial US", "percents").convert(98.0), 0);
	}

	@Test
	public final void testDimensions() throws Exception {
		Dimension temp = unitSystems.getUnitSystem("Server").getDimension("temperature");
		assertEquals("temperature", temp.getName());
		assertEquals("\u00B0F", temp.getUnits());
		assertEquals("Fahrenheit", temp.getLongUnits());
	}

	@Test(expected = ConverterException.class)
	public final void testGetWrongSystemConverter() throws Exception {
		unitSystems.getSystemConverter("NoSuchSystem", "Imperial US");
	}

	@Test
	public final void testGetSystemConverter() throws Exception {
		SystemConverter sc = unitSystems.getSystemConverter("SI", "Imperial US");
		assertEquals("SI", sc.getBaseSystemName());
		assertEquals("Imperial US", sc.getTargetSystem().getName());
	}

	@Test(expected = ConverterException.class)
	public final void testGetWrongUnitConverter() throws Exception {
		unitSystems.getUnitConverter("SI", "Imperial US", "noSuchDimension");
	}

	@Test(expected = ConverterException.class)
	public final void testGetWrongUnitConverter2() throws Exception {
		unitSystems.getUnitConverter("SI", "Imperial US", "pressure");
	}

	@Test
	public final void testGetUnitConverter() throws Exception {
		UnitConverter uc = unitSystems.getUnitConverter("SI", "Imperial US", "temperature");
		assertEquals("temperature", uc.getDimensionName());
	}

	@Test
	public final void testGetReverseUnitConverter() throws Exception {
		UnitConverter uc = unitSystems.getUnitConverter("Imperial US", "SI", "temperature");
		assertEquals("temperature", uc.getDimensionName());
	}

	@Test
	public final void testUnitConversion() throws Exception {
		final double tempC = 24;
		final double tempF = tempC * 9 / 5 + 32;
		UnitConverter uc = unitSystems.getUnitConverter("SI", "Imperial US", "temperature");
		assertEquals(tempF, uc.convert(tempC), 0);
	}

	@Test
	public final void testReverseUnitConversion() throws Exception {
		final double tempF = 78;
		final double tempC = (tempF - 32) * 5 / 9;
		UnitConverter uc = unitSystems.getUnitConverter("Imperial US", "SI", "temperature");
		assertEquals(tempC, uc.convert(tempF), 0.00000000000001);
	}

	@Test
	public final void testSystemValuesFilter() throws Exception {
		// values -2000 and -3000 must not be converted by the custom converters
		// specified for dimension "200" in systems.xml
		UnitConverter uc = unitSystems.getUnitConverter("SI", "Server", "200");
		assertEquals(-2000.0, uc.convert(-2000), 0);
		assertEquals(-3000.0, uc.convert(-3000), 0);
		uc = unitSystems.getUnitConverter("Server", "SI", "200");
		assertEquals(-2000.0, uc.convert(-2000), 0);
		assertEquals(-3000.0, uc.convert(-3000), 0);
	}
}
