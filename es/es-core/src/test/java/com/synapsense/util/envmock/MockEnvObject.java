package com.synapsense.util.envmock;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import com.synapsense.dto.ValueTO;

public class MockEnvObject {

	private String name;

	private Map<String, Object> properties = new HashMap<String, Object>();

	private Map<String, Collection<ValueTO>> histories = new HashMap<String, Collection<ValueTO>>();

	public MockEnvObject(String name) {
		super();
		this.name = name;
	}

	public Object getProperty(String propName) {
		if ("name".equals(propName))
			return name;
		return properties.get(propName);
	}

	public void setProperty(String propName, Object val) {
		properties.put(propName, val);
	}

	public Collection<ValueTO> getHistory(String propName) {
		return histories.get(propName);
	}

	public void setHistory(String propName, Collection<ValueTO> val) {
		histories.put(propName, val);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Map<String, Object> getProperties() {
		return properties;
	}

}
