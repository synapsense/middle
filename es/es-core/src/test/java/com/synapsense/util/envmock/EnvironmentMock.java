package com.synapsense.util.envmock;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.synapsense.dto.CollectionTO;
import com.synapsense.dto.EnvObjectBundle;
import com.synapsense.dto.EnvObjectHistory;
import com.synapsense.dto.ObjectType;
import com.synapsense.dto.ObjectTypeMatchMode;
import com.synapsense.dto.PropertyDescr;
import com.synapsense.dto.Relation;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOBundle;
import com.synapsense.dto.Tag;
import com.synapsense.dto.TagDescriptor;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.EnvException;
import com.synapsense.exception.IllegalInputParameterException;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.exception.PropertyNotFoundException;
import com.synapsense.exception.ServerConfigurationException;
import com.synapsense.exception.TagNotFoundException;
import com.synapsense.exception.UnableToConvertPropertyException;
import com.synapsense.exception.UnableToConvertTagException;
import com.synapsense.exception.UnsupportedRelationException;
import com.synapsense.search.QueryDescription;
import com.synapsense.service.Environment;

public class EnvironmentMock implements Environment  {

	private Map<TO<String>, MockEnvObject> genObjects;

	public Map<TO<String>, MockEnvObject> getObjects() {
		if (genObjects == null)
			genObjects = new HashMap<TO<String>, MockEnvObject>();
		return genObjects;
	}

	@Override
	public <REQUESTED_TYPE> void addPropertyValue(TO<?> objId, String propName, REQUESTED_TYPE value)
	        throws ObjectNotFoundException, PropertyNotFoundException, UnableToConvertPropertyException {
		throw new UnsupportedOperationException();

	}

	@Override
	public TO<?> createObject(String typeName) throws EnvException {
		throw new UnsupportedOperationException();
	}

	@Override
	public TO<?> createObject(String typeName, ValueTO[] propertyValues) throws EnvException {
		throw new UnsupportedOperationException();
	}

	@Override
	public TOBundle createObjects(EnvObjectBundle objectsToCreate) throws EnvException {
		throw new UnsupportedOperationException();
	}

	@Override
	public ObjectType createObjectType(String name) throws EnvException {
		throw new UnsupportedOperationException();
	}

	@Override
	public void deleteObject(TO<?> objId) throws ObjectNotFoundException {
		throw new UnsupportedOperationException();
	}

	@Override
	public void deleteObject(TO<?> objId, int level) throws ObjectNotFoundException {
		throw new UnsupportedOperationException();
	}

	@Override
	public void deleteObjectType(String typeName) throws EnvException {
		throw new UnsupportedOperationException();
	}

	@Override
	public Collection<ValueTO> getAllPropertiesValues(TO<?> objId) throws ObjectNotFoundException {
		MockEnvObject obj = genObjects.get(objId);
		if (obj == null)
			throw new ObjectNotFoundException("Cannot find object with id=" + objId);
		Collection<ValueTO> res = new LinkedList<ValueTO>();
		for (Entry<String, Object> entry : obj.getProperties().entrySet())
			res.add(new ValueTO(entry.getKey(), entry.getValue()));
		return res;
	}

	@Override
	public Collection<CollectionTO> getAllPropertiesValues(Collection<TO<?>> objIds) {
		Collection<CollectionTO> result = new LinkedList<CollectionTO>();
		for (TO<?> to : objIds) {
			try {
				result.add(new CollectionTO(to, (LinkedList<ValueTO>) getAllPropertiesValues(to)));
			} catch (ObjectNotFoundException e) {
				result.add(new CollectionTO(to));
			}
		}
		return result;
	}

	@Override
	public Collection<TO<?>> getChildren(TO<?> objId) throws ObjectNotFoundException {
		throw new UnsupportedOperationException();
	}

	@Override
	public Collection<TO<?>> getChildren(TO<?> objId, String typeName) throws ObjectNotFoundException {
		throw new UnsupportedOperationException();
	}

	@Override
	public Collection<CollectionTO> getChildren(Collection<TO<?>> objIds, String typeName) {
		throw new UnsupportedOperationException();
	}

	@Override
	public Collection<CollectionTO> getChildren(Collection<TO<?>> objIds) {
		throw new UnsupportedOperationException();
	}

	@Override
	public <RETURN_TYPE> Collection<ValueTO> getHistory(TO<?> objId, String propName, Class<RETURN_TYPE> clazz)
	        throws ObjectNotFoundException, UnableToConvertPropertyException, PropertyNotFoundException {
		MockEnvObject obj = genObjects.get(objId);
		if (obj == null)
			throw new ObjectNotFoundException("Cannot find object with id=" + objId);
		Collection<ValueTO> val = obj.getHistory(propName);
		if (val == null)
			throw new PropertyNotFoundException("Object id=" + objId + ", property not found: " + propName);
		return val;
	}

	@Override
	public <RETURN_TYPE> Collection<ValueTO> getHistory(TO<?> objId, String propName, int size, int start,
	        Class<RETURN_TYPE> clazz) throws ObjectNotFoundException, PropertyNotFoundException,
	        UnableToConvertPropertyException {
		throw new UnsupportedOperationException();
	}

	@Override
	public <RETURN_TYPE> Collection<ValueTO> getHistory(TO<?> objId, String propName, Date start, Date end,
	        Class<RETURN_TYPE> clazz) throws ObjectNotFoundException, PropertyNotFoundException,
	        UnableToConvertPropertyException {
		throw new UnsupportedOperationException();
	}
	

	@Override
	public EnvObjectHistory getHistory(TO<?> objId, Collection<String> propNames, Date start, Date end) { 		
		throw new UnsupportedOperationException();
	}

	@Override
	public <RETURN_TYPE> Collection<ValueTO> getHistory(TO<?> objId, String propName, Date start, int numberPoints,
	        Class<RETURN_TYPE> clazz) throws ObjectNotFoundException, PropertyNotFoundException,
	        UnableToConvertPropertyException {
		throw new UnsupportedOperationException();
	}

	@Override
	public Integer getHistorySize(TO<?> objId, String propName) throws ObjectNotFoundException,
	        PropertyNotFoundException {
		throw new UnsupportedOperationException();
	}

	/*
	 * @Override public <RETURN_TYPE> RETURN_TYPE getLatestPropertyValue(TO<?>
	 * objId, String propName, Class<RETURN_TYPE> clazz) throws
	 * ObjectNotFoundException, PropertyNotFoundException,
	 * UnableToConvertPropertyException { throw new
	 * UnsupportedOperationException(); }
	 * 
	 * @Override public TO<?> getObject(String typeName, String name) throws
	 * TooManyObjectsException { throw new UnsupportedOperationException(); }
	 */
	@Override
	public ObjectType getObjectType(String name) {
		throw new UnsupportedOperationException();
	}

	@Override
	public ObjectType[] getObjectTypes(String[] names) {
		throw new UnsupportedOperationException();
	}

	@Override
	public String[] getObjectTypes() {
		throw new UnsupportedOperationException();
	}

	@Override
	public Collection<TO<?>> getObjects(String typeName, ValueTO[] propertyValues) {
		throw new UnsupportedOperationException();
	}

	@Override
	public Collection<TO<?>> getObjects(String typeName, ObjectTypeMatchMode matchMode, ValueTO[] propertyValues) {
		throw new UnsupportedOperationException();
	}

	@Override
	public Collection<TO<?>> getObjectsByType(String typeName) {
		throw new UnsupportedOperationException();
	}

	@Override
	public Collection<TO<?>> getObjectsByType(String typeName, ObjectTypeMatchMode matchMode) {
		throw new UnsupportedOperationException();
	}

	@Override
	public Collection<TO<?>> getParents(TO<?> objId, String typeName) throws ObjectNotFoundException {
		throw new UnsupportedOperationException();
	}

	@Override
	public Collection<TO<?>> getParents(TO<?> objId) throws ObjectNotFoundException {
		throw new UnsupportedOperationException();
	}

	@Override
	public PropertyDescr getPropertyDescriptor(String typeName, String propName) throws ObjectNotFoundException,
	        PropertyNotFoundException {
		throw new UnsupportedOperationException();
	}

	@Override
	public long getPropertyTimestamp(TO<?> objId, String propName) throws ObjectNotFoundException,
	        PropertyNotFoundException {
		throw new UnsupportedOperationException();
	}

	@Override
	@SuppressWarnings("unchecked")
	public <RETURN_TYPE> RETURN_TYPE getPropertyValue(TO<?> objId, String propName, Class<RETURN_TYPE> clazz)
	        throws ObjectNotFoundException, PropertyNotFoundException, UnableToConvertPropertyException {
		MockEnvObject obj = genObjects.get(objId);
		if (obj == null)
			throw new ObjectNotFoundException("Cannot find object with id=" + objId);
		if (!obj.getProperties().containsKey(propName))
			throw new PropertyNotFoundException("Object id=" + objId + ", property not found: " + propName);
		return (RETURN_TYPE) obj.getProperty(propName);
	}

	@Override
	public Collection<CollectionTO> getPropertyValue(Collection<TO<?>> objIds, String[] propNames) {
		Collection<CollectionTO> result = new LinkedList<CollectionTO>();
		for (TO<?> to : objIds) {
			CollectionTO props = new CollectionTO(to);
			result.add(props);
			for (String propName : propNames) {
				try {
					props.addValue(new ValueTO(propName, getPropertyValue(to, propName, Object.class)));
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public <RETURN_TYPE> RETURN_TYPE getPropertyValue(String typeName, String propName, Class<RETURN_TYPE> clazz)
	        throws ObjectNotFoundException, PropertyNotFoundException, UnableToConvertPropertyException {
		// find the first object of this type
		for (Entry<TO<String>, MockEnvObject> entry : genObjects.entrySet()) {
			if (entry.getKey().getTypeName().equals(typeName)) {
				Object val = entry.getValue().getProperty(propName);
				if (val == null)
					throw new PropertyNotFoundException("Object type=" + typeName + ", property not found: " + propName);
				return (RETURN_TYPE) val;
			}
		}
		throw new ObjectNotFoundException("Cannot find object with type=" + typeName);
	}

	@Override
	public Collection<TO<?>> getRelatedObjects(TO<?> objId, String objectType, boolean isChild) {
		throw new UnsupportedOperationException();
	}

	/*
	 * @Override public TO<?> queryObject(String typeName, Object id) throws
	 * ObjectNotFoundException { throw new UnsupportedOperationException(); }
	 */
	@Override
	public <REQUESTED_TYPE> void removePropertyValue(TO<?> objId, String propName, REQUESTED_TYPE value)
	        throws ObjectNotFoundException, PropertyNotFoundException, UnableToConvertPropertyException {
		throw new UnsupportedOperationException();
	}

	@Override
	public void removeRelation(TO<?> parentId, TO<?> childId) throws ObjectNotFoundException {
		throw new UnsupportedOperationException();
	}

	@Override
	public void removeRelation(List<Relation> relations) throws ObjectNotFoundException {
		throw new UnsupportedOperationException();
	}

	@Override
	public void setAllPropertiesValues(TO<?> objId, ValueTO[] values) throws ObjectNotFoundException,
	        PropertyNotFoundException, UnableToConvertPropertyException {
		throw new UnsupportedOperationException();
	}

	@Override
	public <REQUESTED_TYPE> void setPropertyValue(TO<?> objId, String propName, REQUESTED_TYPE value)
	        throws ObjectNotFoundException, PropertyNotFoundException, UnableToConvertPropertyException {
		throw new UnsupportedOperationException();
	}

	@Override
	public <REQUESTED_TYPE> void setPropertyValue(String typeName, String propName, REQUESTED_TYPE value)
	        throws ObjectNotFoundException, PropertyNotFoundException, UnableToConvertPropertyException {
		throw new UnsupportedOperationException();
	}

	@Override
	public void setRelation(TO<?> parentId, TO<?> childId) throws ObjectNotFoundException, UnsupportedRelationException {
		throw new UnsupportedOperationException();
	}

	@Override
	public void setRelation(List<Relation> relations) throws ObjectNotFoundException, UnsupportedRelationException {
		throw new UnsupportedOperationException();
	}

	@Override
	public void updateObjectType(ObjectType objType) throws EnvException {
		throw new UnsupportedOperationException();
	}

	@Override
	public void configurationComplete() throws ServerConfigurationException {
		// log message should be here
	}

	@Override
	public void configurationStart() throws ServerConfigurationException {
		// log message should be here
	}

	@Override
	public void setAllPropertiesValues(TO<?> objId, ValueTO[] values, boolean persist) throws ObjectNotFoundException,
	        PropertyNotFoundException, UnableToConvertPropertyException {
		setAllPropertiesValues(objId, values);
	}

	@Override
	public void setAllPropertiesValues(TO<?> objId, Collection<ValueTO> values, Collection<Tag> tags)
	        throws EnvException {
		throw new UnsupportedOperationException();
	}

	@Override
	public <REQUESTED_TYPE> void setPropertyValue(TO<?> objId, String propName, REQUESTED_TYPE value, boolean persist)
	        throws PropertyNotFoundException, ObjectNotFoundException, UnableToConvertPropertyException {
		setPropertyValue(objId, propName, value);
	}

	@Override
	public boolean exists(TO<?> objId) {
		if (objId == null) {
			throw new IllegalInputParameterException("object is null");
		}
		return genObjects.containsKey(objId);
	}

	@Override
	public TagDescriptor getTagDescriptor(String typeName, String propName, String tagName)
	        throws ObjectNotFoundException, PropertyNotFoundException, TagNotFoundException {
		throw new UnsupportedOperationException();
	}

	@Override
	public <RETURN_TYPE> RETURN_TYPE getTagValue(TO<?> objId, String propName, String tagName, Class<RETURN_TYPE> clazz)
	        throws ObjectNotFoundException, PropertyNotFoundException, TagNotFoundException,
	        UnableToConvertTagException {
		throw new UnsupportedOperationException();
	}

	@Override
	public Collection<Tag> getAllTags(TO<?> objId, String propName) throws ObjectNotFoundException,
	        PropertyNotFoundException {
		throw new UnsupportedOperationException();
	}

	@Override
	public Collection<Tag> getAllTags(TO<?> objId) throws ObjectNotFoundException {
		throw new UnsupportedOperationException();
	}

	@Override
	public Map<TO<?>, Collection<Tag>> getAllTags(String typeName) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void setTagValue(TO<?> objId, String propName, String tagName, Object value)
	        throws PropertyNotFoundException, ObjectNotFoundException, TagNotFoundException,
	        UnableToConvertTagException {
		throw new UnsupportedOperationException();
	}

	@Override
	public Map<TO<?>, Map<String, Collection<Tag>>> getTags(Collection<TO<?>> objIds, List<String> propertyNames,
	        List<String> tagNames) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void setAllTagsValues(TO<?> objId, Collection<Tag> tagsToSet) throws PropertyNotFoundException,
	        ObjectNotFoundException, TagNotFoundException, UnableToConvertTagException {
		throw new UnsupportedOperationException();
	}

	@Override
    public Collection<TO<?>> getObjects(QueryDescription queryDescr) {
		throw new UnsupportedOperationException();
    }

}
