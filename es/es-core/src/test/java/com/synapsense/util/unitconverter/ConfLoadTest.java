package com.synapsense.util.unitconverter;

import java.io.InputStreamReader;
import java.io.Reader;

import javax.xml.bind.JAXBException;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class ConfLoadTest {

	private static Reader emptyXml;
	private static Reader noSchemaXml;
	private static Reader properXml;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		emptyXml = new InputStreamReader(ConfLoadTest.class.getResourceAsStream("empty.xml"));
		noSchemaXml = new InputStreamReader(ConfLoadTest.class.getResourceAsStream("noSchemaRef.xml"));
		properXml = new InputStreamReader(ConfLoadTest.class.getResourceAsStream("systems.xml"));
	}

	@Test
	public final void testLoadEmptyFile() throws Exception {
		UnitSystems us = new UnitSystems();
		us.loadFromXml(emptyXml);
	}

	@Test(expected = JAXBException.class)
	public final void testLoadNoSchemaFile() throws Exception {
		UnitSystems us = new UnitSystems();
		us.loadFromXml(noSchemaXml);
	}

	@Test
	public final void testLoadBadFiles() throws Exception {
		for (int i = 1; i <= 7; i++) {
			String fileName = "badFile" + i + ".xml";
			Reader badXml = new InputStreamReader(ConfLoadTest.class.getResourceAsStream(fileName));
			UnitSystems us = new UnitSystems();
			try {
				us.loadFromXml(badXml);
				Assert.fail("failed on: " + fileName);
			} catch (ConverterConfigurationException e) {
				// this one is expected
			}
		}
	}

	@Test
	public final void testLoadProperFile() throws Exception {
		UnitSystems us = new UnitSystems();
		us.loadFromXml(properXml);
	}
}
