package com.synapsense;

import java.io.PrintStream;

import org.jmock.Mockery;
import org.jmock.Sequence;
import org.jmock.integration.junit4.JUnit4Mockery;
import org.jmock.lib.legacy.ClassImposteriser;
import org.junit.After;

import com.synapsense.dto.TOFactory;

public abstract class AbstractTest {
	private PrintStream out = System.out;

	@After
	public void afterTest() {
		context.assertIsSatisfied();
	}

	protected <T> T mock(Class<T> mock) {
		return context.mock(mock);
	}

	protected <T> T mock(Class<T> mock, String name) {
		return context.mock(mock, name);
	}

	protected Sequence sequence(String name) {
		return context.sequence(name);
	}

	protected void checking(org.jmock.Expectations e) {
		context.checking(e);
	}
	
	protected void printOutput(String format, Object... args) {
		out.println(String.format(format, args));
	}
	
	protected void printOutput(PrintStream out, String format, Object... args) {
		out.println(String.format(format, args));
	}

	private Mockery context = new JUnit4Mockery() {
		{
			setImposteriser(ClassImposteriser.INSTANCE);
		}
	};

	protected static final TOFactory TO_FACTORY = TOFactory.getInstance();

}

