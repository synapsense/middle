select
  src.sensorId ,
  'lastValue', 
  ruleProperty.binding          
from
 RuleObjectSrcPropertyDO as ruleProperty,
 ObjectDO as genericObject,
 AttributeDO attr,
 ValueDO value,
 ValueObjectLink as valueObjlink ,
 Topology as src          
where
 not exists (
  select
   1 
  from
   ObjectTypeDO as type 
  where
   type.generic = 0 
   and type.name = valueObjlink.linkObjectType
 )          
 and valueObjlink.linkObjectId = src.sensorId          
 and ruleProperty.object.objectId = genericObject.objectId          
 and genericObject.objectId = value.object.objectId          
 and ruleProperty.object.objectId = value.object.objectId          
 and valueObjlink.valueId = value.valueId          
 and attr.attributeId = value.attributeId          
 and ruleProperty.attribute.attributeId = attr.attributeId          
 and ruleProperty.genericSrc=0          
 and ruleProperty.rule.ruleId=1