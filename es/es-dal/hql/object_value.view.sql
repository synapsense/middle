create or replace view all_objects_props_values
as
select a.name as propname ,o.name as object_name ,ot.name as type_name, v.value_id as val_id , o.id as object_id, ot.objtype_id as type_id,
case
when vv.value is not null then vv.value
when vi.value is not null then vi.value
when vd.value is not null then vd.value
when vdt.value is not null then vdt.value
when vl.value is not null then vl.value
else 'non-scalar'
end as val
from
objects o left outer join obj_values v on (o.id = v.object_id_fk)
left outer  join attributes a on (a.attr_id = v.attr_id_fk)
inner join object_types ot on (ot.objtype_id = o.object_type_id_fk )
left outer join  valuev vv on (v.value_id = vv.id)
left outer join  valuei vi on (v.value_id = vi.id)
left outer join  valued vd on (v.value_id = vd.id)
left outer join  valuedt vdt on (v.value_id = vdt.id)
left outer join  valuel vl on (v.value_id = vl.id)


