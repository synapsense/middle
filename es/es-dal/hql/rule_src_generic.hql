select
  src.objectId ,
  src.objectType.name,
  attr.attributeName ,
  ruleProperty.binding        
 from
  RuleObjectSrcPropertyDO as ruleProperty,
  ObjectDO as src,
  AttributeDO as attr          
 where
  ruleProperty.attribute.attributeId = attr.attributeId          
  and ruleProperty.object.objectId = src.objectId          
  and ruleProperty.genericSrc=1          
  and ruleProperty.rule.ruleId = 14