package com.synapsense.dal.generic.entities;

import java.util.HashMap;

import com.synapsense.service.impl.exception.DALSystemException;

public final class TagTypes {
	/**
	 * POJO implementations of all supported types used for determine what class
	 * is responsible for storing values of some java type
	 */
	private static HashMap<String, Class<? extends AbstractValue<?>>> tagTypes = new HashMap<String, Class<? extends AbstractValue<?>>>();
	static {
		tagTypes.put(java.lang.String.class.getName(), Valuev.class);

		tagTypes.put(java.lang.Long.class.getName(), Valuel.class);

		tagTypes.put(java.lang.Integer.class.getName(), Valuei.class);

		tagTypes.put(java.lang.Double.class.getName(), Valued.class);
	}

	/**
	 * Try to get Hibernate class reference by specified tagType type
	 * 
	 * @param tagType
	 * @return Hibernate mapped Class (POJO)
	 * @throws IllegalArgumentException
	 *             if requested propertyType is not supported
	 */
	public static Class<?> checkAndGetTagTypeClass(String tagType) throws DALSystemException {
		Class<?> clazz = tagTypes.get(tagType);

		if (clazz == null) {
			throw new DALSystemException("The type [" + tagType
			        + "] is not supported by the system and has to be defined before using");
		}

		return clazz;
	}

	public static Object getInstance(Class<?> clazz, String string) {
		checkAndGetTagTypeClass(clazz.getName());
		if (clazz.getName().equals(java.lang.String.class.getName())) {
			return string;
		} else if (clazz.getName().equals(java.lang.Long.class.getName())) {
			return new Long(string);
		} else if (clazz.getName().equals(java.lang.Integer.class.getName())) {
			return new Integer(string);
		} else {
			return new Double(string);
		}
	}

}
