package com.synapsense.dal.generic.batch;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.synapsense.dal.generic.batch.loader.HistoricalDataLoader;
import com.synapsense.dal.generic.entities.HistoricalValueDO;
import com.synapsense.dal.generic.entities.TypedValue;
import com.synapsense.dal.generic.utils.Util;
import com.synapsense.dto.TO;
import com.synapsense.dto.Types;
import com.synapsense.dto.ValueTO;
import com.synapsense.util.CollectionUtils;
import com.synapsense.util.Pair;

/**
 * Updater of generic part of database
 * 
 * @author ashabanov
 * 
 */
public class GenericModelUpdater implements ModelUpdater {

	public static class Statistics {
		private Date startedIn = new Date();
		private int historicalRecords = 0;

		private void incHistoryCounter(int num) {
			historicalRecords += num;
		}

		public String toString() {
			return "Started in:" + startedIn + "; historical points saved since=" + historicalRecords;
		}
	}

	private static final Logger logger = Logger.getLogger(GenericModelUpdater.class);

	private HistoricalDataLoader historicalDataLoader;
	private ValueCache valMapping;
	private AttributeCache attrMapping;
	private DataSource datasource;
	private volatile boolean isStatisticEnabled = false;
	private Statistics statistics;

	private EntityManager em;

	public GenericModelUpdater(final HistoricalDataLoader loader, final ValueCache valMapping,
	        final AttributeCache attrMapping, final DataSource datasource, final EntityManager em) {
		this.historicalDataLoader = loader;
		this.valMapping = valMapping;
		this.attrMapping = attrMapping;
		this.datasource = datasource;
		this.em = em;
	}

	public Statistics getStatistics() {
		return statistics;
	}

	public void collectStatistics() {
		isStatisticEnabled = true;

		if (isStatisticEnabled) {
			logger.debug("Start collecting statistic of model updater at " + new Date());
			statistics = new Statistics();
		}

	}

	public void update(final Collection<SingleUpdateUnit> updateUnits) throws ModelUpdateException {
		Connection conn = getConnection();

		ValuesSQLUpdater updater = getSqlUpdater(updateUnits.size(), conn);

		List<HistoricalValueDO> historicalValues = new LinkedList<HistoricalValueDO>();
		try {
			long updateDataTimeStart = System.currentTimeMillis();

			updater.startBatch();
			for (SingleUpdateUnit unit : updateUnits) {
				ValueTO actualValue = unit.getLast();

				final Pair<Integer, String> unitKey = Pair.newPair((Integer) unit.getKey().getFirst().getID(), unit
				        .getKey().getSecond());

				AttributeTuple attrTuple = findAttributeTuple(unit.getTo().getTypeName(), unitKey.getSecond());
				if (attrTuple == null) {
					logger.warn("Cannot save value {" + actualValue + "}.Property mapping does not contain key : "
					        + "{" + unit.getTo().getTypeName() + "," + unitKey.getSecond()
					        + "}. Seems that there was no event about ES object model change");
					continue;
				}

				TypedValue typedActualValue = new TypedValue(attrTuple.getType());
				try {
					typedActualValue.setValue(actualValue.getValue());
				} catch (ClassCastException e) {
					logger.warn("Cannot assign value {" + String.valueOf(actualValue.getValue()) + "} to  {"
					        + unit.getKey() + "}. Uncompatile types detected" , e);
					throw e;
				}

				int unitObjId = (Integer) unit.getTo().getID();

				ValueTuple valueTuple = valMapping.get(unitKey);
				if (valueTuple == null) {
					int valueId = updater.insertValue(unitObjId, attrTuple.getAttrId(), actualValue.getTimeStamp(),
					        typedActualValue);
					if (valueId != -1) {
						valMapping.put(unitKey,
						        new ValueTuple(attrTuple.getAttrId(), valueId, attrTuple.isHistorical()));
						if (attrTuple.isHistorical()) {
							historicalValues.addAll(wrapUnitToHistoricalValues(unit, valueId));
						}
					}
				} else if (unit.isTimestampOnly()) {
					updater.updateTimestamp(valueTuple.getValueId(), actualValue.getTimeStamp());
				} else {
					updater.updateValue(valueTuple.getValueId(), actualValue.getTimeStamp(), typedActualValue);
					if (attrTuple.isHistorical()) {
						historicalValues.addAll(wrapUnitToHistoricalValues(unit, valueTuple.getValueId()));
					}
				}
			}

			if (logger.isDebugEnabled()) {
				logger.debug("Update obj_values , memory operation time(ms) : "
				        + (System.currentTimeMillis() - updateDataTimeStart));
			}

			long updateJDBCDataTimeStart = System.currentTimeMillis();
			try {
				updater.commit();
				if (logger.isDebugEnabled()) {
					logger.debug("Update obj_values , JDBC operation time(ms) : "
					        + (System.currentTimeMillis() - updateJDBCDataTimeStart));
					logger.debug("Update obj_values , total time(ms) : "
					        + (System.currentTimeMillis() - updateDataTimeStart));
				}
			} catch (SQLException e) {
				throw new ModelUpdateException("Cannot commit series of updates, " + updateUnits.size()
				        + " udpates are lost", e);
			}

		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				logger.warn("Error releasing connection. This can lead to connection pool starvation!", e);
			}
		}

		logger.debug("Preparing to load historical data...");

		long loadDataTimeStart = System.currentTimeMillis();
		historicalDataLoader.load(historicalValues);
		long loadDataTime = (System.currentTimeMillis() - loadDataTimeStart);
		logger.debug("Insert into historical_values time(ms) : " + loadDataTime);

		if (isStatisticEnabled) {
			statistics.incHistoryCounter(historicalValues.size());
		}

	}

	protected Pair<Set<Object>[], Boolean> extractQueuedInternals(final Collection<SingleUpdateUnit> elements) {
		Set<Object>[] sets = new Set[2];
		sets[0] =  CollectionUtils.newSet();
		sets[1] = CollectionUtils.newSet();
		boolean containsBlobs = false;
		for (SingleUpdateUnit elem : elements) {
			sets[0].add(elem.getTo());
			sets[1].add(elem.getPropertyName());
			if (!containsBlobs && elem.containsBlobs()) {
				containsBlobs = true;
			}
		}

		return Pair.newPair(sets, containsBlobs);
	}

	protected Map<Types, Collection<Integer>> extractIdsByType(final Collection<TO<?>> objIds, final Types[] types) {
		return Util.extractIDs(objIds, Integer.class, types);
	}
	
	private AttributeTuple findAttributeTuple(String typeName, String propName) {
		Pair<String, String> attrKey = Pair.newPair(typeName, propName);
		return attrMapping.get(attrKey);
	}

	private List<HistoricalValueDO> wrapUnitToHistoricalValues(SingleUpdateUnit unit, int valueId) {
		List<HistoricalValueDO> historicalValues = new LinkedList<HistoricalValueDO>();

		for (ValueTO valueTo : unit.getValues()) {
			if (valueTo.getValue() == null) {
				if (logger.isTraceEnabled()) {
					logger.trace("Null values are not supported as historical");
				}
				continue;
			}
			HistoricalValueDO item = new HistoricalValueDO(valueId, new Date(valueTo.getTimeStamp()), unit.getGroupId());
			if (item.setUnifiedValue(valueTo.getValue()))
				historicalValues.add(item);
		}

		return historicalValues;

	}

	private ValuesSQLUpdater getSqlUpdater(int bufferSize, Connection conn) {
		ValuesSQLUpdater updater = new ValuesSQLUpdater(conn, em, bufferSize);
		return updater;
	}

	private Connection getConnection() {
		try {
			return datasource.getConnection();
		} catch (SQLException e) {
			throw new ModelUpdateException("Error occured when accuiring connection from the pool", e);
		}
	}

}
