package com.synapsense.dal.generic.entities;

import java.util.HashMap;
import java.util.Map;

import com.synapsense.dto.TagDescriptor;

// Generated 18-Jan-2010 15:59:59 by Hibernate Tools 3.2.0.CR1

/**
 * TagsDO generated by hbm2java
 */
public class TagDO implements java.io.Serializable {

	private static final long serialVersionUID = 6085875105699322417L;
	private Integer tagId;
	private String name;
	private AttributeDO attribute;
	private AttributeTypeDO tagType;

	private Map<Integer, TagValueDO> tagValues = new HashMap<Integer, TagValueDO>(0);

	public TagDO() {
	}

	public TagDO(String name, AttributeDO attribute, AttributeTypeDO tagDescriptor) {
		this.name = name;
		this.attribute = attribute;
		this.tagType = tagDescriptor;
	}

	public Integer getTagId() {
		return this.tagId;
	}

	public void setTagId(Integer tagId) {
		this.tagId = tagId;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public AttributeDO getAttribute() {
		return this.attribute;
	}

	public void setAttribute(AttributeDO attribute) {
		this.attribute = attribute;
	}

	public AttributeTypeDO getTagType() {
		return this.tagType;
	}

	public void setTagType(AttributeTypeDO tagType) {
		this.tagType = tagType;
	}

	public TagDescriptor getTagDescr() {
		return new TagDescriptor(getName(), tagType.getType());
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((attribute == null) ? 0 : attribute.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((tagType == null) ? 0 : tagType.hashCode());
		result = prime * result + ((tagId == null) ? 0 : tagId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final TagDO other = (TagDO) obj;
		if (attribute == null) {
			if (other.attribute != null)
				return false;
		} else if (!attribute.equals(other.attribute))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (tagType == null) {
			if (other.tagType != null)
				return false;
		} else if (!tagType.equals(other.tagType))
			return false;
		return true;
	}

	public Map<Integer, TagValueDO> getTagValues() {
		return tagValues;
	}

	public void setTagValues(Map<Integer, TagValueDO> tagValues) {
		this.tagValues = tagValues;
	}

}
