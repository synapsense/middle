package com.synapsense.dal.generic.batch;

import java.util.List;

import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import org.apache.log4j.Logger;

import com.synapsense.commandprocessor.api.CommandEngine;
import com.synapsense.dal.generic.utils.Util;
import com.synapsense.util.executor.Action;
import com.synapsense.util.executor.Executor;
import com.synapsense.util.executor.ProcessingException;

/**
 * Batch save of changed values action.
 * 
 * @author ashabanov
 * 
 */
public class BatchSaveAction implements Action {
	private static final Logger logger = Logger.getLogger(BatchSaveAction.class);

	private List<SingleUpdateUnit> processingStack;
	private CommandEngine engine;
	private Executor executor;
	private ModelUpdater updater;
	private UserTransaction userTransaction;

	private BatchSaveAction(List<SingleUpdateUnit> processingStack, CommandEngine engine, Executor executor,
	        ModelUpdater updater, UserTransaction userTransaction) {
		this.processingStack = processingStack;
		this.executor = executor;
		this.engine = engine;
		this.updater = updater;
		this.userTransaction = userTransaction;
	}

	public static Action newBatchSaveAction(List<SingleUpdateUnit> list, CommandEngine engine, Executor executor,
	        ModelUpdater updater, UserTransaction userTransaction) {
		return new BatchSaveAction(list, engine, executor, updater, userTransaction);
	}

	@Override
	public void execute() throws ProcessingException {
		long totalTimeStart = System.currentTimeMillis();

		try {
			if (logger.isDebugEnabled())
				logger.debug("Beginning batch update transaction...");
			userTransaction.begin();

			if (logger.isDebugEnabled())
				logger.debug("Updating object model...");

			long memoryUpdateTimeStart = System.currentTimeMillis();
			updater.update(processingStack);

			if (logger.isDebugEnabled())
				logger.debug("BATCH : memory udpate operation execution time(ms): "
				        + (System.currentTimeMillis() - memoryUpdateTimeStart));

			if (logger.isDebugEnabled())
				logger.debug("Flushing updated objects to database...");
			long startCommit = System.currentTimeMillis();
			userTransaction.commit();
			if (logger.isDebugEnabled())
				logger.debug("COMMIT : execution time(ms):" + (System.currentTimeMillis() - startCommit));

		} catch (Exception e) {
			StringBuilder message = new StringBuilder();
			message.append("Batch update transaction has failed!The batch of : " + processingStack.size()
			        + " was refused!");
			if (logger.isTraceEnabled()) {
				message.append("Batch content is : " + processingStack.toString());
			}

			logger.error(message, e);

			try {
				userTransaction.rollback();
			} catch (IllegalStateException e1) {
				logger.error("Transaction rollback has failed", e1);
			} catch (SystemException e1) {
				logger.error("Transaction rollback has failed", e1);
			}

			// continue dividing until size is > 1
			// by example for batch size =1000 the dihotomy algo will generate
			// log^2(1000) parts of batch to separately save them to db
			// in a case when error is only in one half of the processing stack
			// worst case is when every element in processing stack is wrong
			if (processingStack.size() > 1) {
				logger.debug("Batch update will be divided into two halves which will be sent back to model updater...");
				List<List<SingleUpdateUnit>> sublists = Util
				        .divideByPieces(processingStack, processingStack.size() / 2);
				engine.addCommand(CommandFactory.executeBatchTasks(sublists, engine, executor, updater, userTransaction));
			} else {
				logger.debug(
				        "Processing stack size is less or equal to one, dihotomy algorithm should stop here. Means that stack containts only corrupted units.["
				                + processingStack.toString() + "]", e);
			}

		}

		if (logger.isDebugEnabled())
			logger.debug("Total time batch operation took (ms): " + (System.currentTimeMillis() - totalTimeStart));

	}

}