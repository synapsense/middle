package com.synapsense.dal.objectcache;

import java.io.Serializable;

public final class Status implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -939419280948303374L;

	public static final int ONLINE = 0x00;
	public static final int OFFLINE = 0xFF;
}
