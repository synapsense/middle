package com.synapsense.dal.generic.utils;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Query;

import com.synapsense.dal.generic.DTOGetter;
import com.synapsense.dal.generic.entities.AbstractValue;
import com.synapsense.dal.generic.entities.PropertyTypes;
import com.synapsense.dal.generic.entities.TypedValue;
import com.synapsense.dal.generic.entities.ValueDO;
import com.synapsense.dto.CollectionTO;
import com.synapsense.dto.FindCriteria;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.dto.Types;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.*;
import com.synapsense.service.Environment;
import com.synapsense.service.impl.exception.DALSystemException;
import com.synapsense.util.CollectionUtils;

public final class Util {

	public static String CRLF = System.getProperty("line.separator", "\n");

	public static void assertNullEntity(final String message, final Object object) throws ObjectExistsException {
		if (object != null) {
			throw new ObjectExistsException(message + "." + object.toString() + " already exists");
		}
	}

	public static void assertNotNullEntity(final String message, final Object object) throws ObjectNotFoundException {
		if (object == null) {
			throw new ObjectNotFoundException(message);
		}
	}

	/**
	 * 
	 * @param item
	 * @param propName
	 * @return
	 * @throws SecurityException
	 * @throws NoSuchMethodException
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 */
	private static Object getProperty(final Object item, final String propName) {
		StringBuilder sb = new StringBuilder(5);
		sb.append("get");
		sb.append(Character.toUpperCase(propName.charAt(0)));
		sb.append(propName.substring(1));
		Method m;
		try {
			m = item.getClass().getMethod(sb.toString(), null);
			return m.invoke(item, null);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * 
	 * @param <PROP_VALUE>
	 * @param item
	 * @param propName
	 * @param value
	 * @throws SecurityException
	 * @throws NoSuchMethodException
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 */
	public static <PROP_VALUE> void setProperty(final Object item, final String propName, final PROP_VALUE value)
	        throws PropertyNotFoundException, UnableToConvertPropertyException {
		if (item == null)
			throw new IllegalArgumentException("Supplied 'item' is null");
		if (propName == null)
			throw new IllegalArgumentException("Supplied 'propName' is null");
		if (propName.isEmpty())
			throw new IllegalArgumentException("Supplied 'propName' is empty string");

		// if value is null we cannot use 'getMethod' approach
		// but null is one of the possible values
		if (value == null) {
			try {
				Field field = item.getClass().getDeclaredField(propName);
				field.setAccessible(true);
				field.set(item, null);
			} catch (IllegalArgumentException e) {
				throw new UnableToConvertPropertyException(propName, e);
			} catch (SecurityException e) {
				throw new PropertyNotFoundException(propName, e);
			} catch (IllegalAccessException e) {
				throw new PropertyNotFoundException(propName, e);
			} catch (NoSuchFieldException e) {
				throw new PropertyNotFoundException(propName, e);
			}
			return;
		}

		StringBuilder sb = new StringBuilder(50);
		sb.append("set");
		sb.append(Character.toUpperCase(propName.charAt(0)));
		sb.append(propName.substring(1));
		Method m = null;
		Class<?> c = value.getClass();
		NoSuchMethodException e1 = null;

		while (c instanceof Object && m == null) {
			try {
				m = item.getClass().getMethod(sb.toString(), c);
				break;
			} catch (NoSuchMethodException e) {
				for (Class<?> c1 : c.getInterfaces()) {
					try {
						m = item.getClass().getMethod(sb.toString(), c1);
						break;
					} catch (NoSuchMethodException e2) {
						e1 = e2;
					}
				}
				c = c.getSuperclass();
				e1 = e;
			}
		}

		if (m == null) {
			throw new PropertyNotFoundException(propName, e1);
		}

		try {
			m.invoke(item, value);
		} catch (IllegalArgumentException e) {
			throw new PropertyNotFoundException(propName, e);
		} catch (IllegalAccessException e) {
			throw new PropertyNotFoundException(propName, e);
		} catch (InvocationTargetException e) {
			throw new UnableToConvertPropertyException(propName, e);
		}

	}

	/**
	 * Unified <code>toString</code> method Returns string - all fields of
	 * object's class whith values
	 * 
	 * @param item
	 *            Object for which will be <code>toString</code> called
	 * @return String representation of specified object <code>item</code>
	 */
	public static String toString(final Object item) {
		StringBuilder sb = new StringBuilder(20);
		sb.append("[");

		PropertyDescriptor[] pds = getPropDescriptors(item.getClass());
		for (PropertyDescriptor p : pds) {
			if (p.getPropertyType().getPackage().getName().startsWith("java.lang") == false)
				continue;
			try {
				sb.append(p.getName() + "=" + p.getReadMethod().invoke(item, null) + ";\n");
			} catch (Exception e) {
				continue;
			}
		}
		sb.append("]");
		return sb.toString();
	}

	public static String[] getPropertyNames(final Class<?> clazz) {
		List<String> namesList = new LinkedList<String>();
		for (PropertyDescriptor pd : Util.getPropDescriptors(clazz, null, null)) {
			namesList.add(pd.getName());
		}
		return namesList.toArray(new String[0]);
	}

	public static PropertyDescriptor[] getPropDescriptors(final Class<?> clazz) {
		return Util.getPropDescriptors(clazz, null, null);
	}

	public static PropertyDescriptor[] getPropDescriptors(final Class<?> clazz, final String[] fieldsIncluded,
	        final String[] fieldsExcluded) {
		BeanInfo bi = null;
		try {
			bi = Introspector.getBeanInfo(clazz);
		} catch (IntrospectionException e) {
			throw new IllegalStateException(e.getMessage(), e);
		}

		Collection<String> includedSet = null;
		if (fieldsIncluded != null) {
			includedSet = new HashSet<String>(Arrays.asList(fieldsIncluded));
		}

		Collection<String> excludedSet = new HashSet<String>();
		excludedSet.add("class");
		excludedSet.add("identity");

		if (fieldsExcluded != null) {
			excludedSet.addAll(Arrays.asList(fieldsExcluded));
		}

		Collection<PropertyDescriptor> result = new LinkedList<PropertyDescriptor>();
		String classPrefix;
		String className;
		for (PropertyDescriptor p : bi.getPropertyDescriptors()) {

			// if property must be included in requested list but not in list
			if (includedSet != null && !includedSet.contains(p.getName())) {
				continue;
			}
			// if property must be excluded from requested list but in list
			if (excludedSet != null && excludedSet.contains(p.getName())) {
				continue;
			}

			if (p.getReadMethod() == null) {
				continue;
			}

			classPrefix = p.getPropertyType().getPackage().getName();
			className = p.getPropertyType().getName();
			// filtering returning fields
			// 3 case for return
			// 1:field is of java.lang
			// 2:field is com.synapsense
			// 3:field is Date cause it is not it lang
			if ((classPrefix.startsWith("java.lang") == true) || (className.startsWith("java.util.Date") == true)
			        || (classPrefix.startsWith("com.synapsense") == true)) {
				result.add(p);
			}
		}

		return result.toArray(new PropertyDescriptor[result.size()]);
	}

	/**
	 * Returns entity reference without hit db Just created entity is proxy to
	 * real object state of the entity can be got later via
	 * <code>EntityManager</code> method <code>refresh</code> When entity's
	 * state is first accessed and entity is not in db , then
	 * <code>EntityNotFoundException</code> will be thrown
	 * 
	 * @param <POJO_TYPE>
	 *            One of the hibernate's POJO types
	 * @param em
	 *            Entity manager reference
	 * @param id
	 *            Object's id
	 * @param clazz
	 *            POJO class reference
	 * @return
	 * @throws EntityNotFoundException
	 */
	public static <POJO_TYPE> POJO_TYPE getEntityReferenceById(final EntityManager em, final Class<POJO_TYPE> clazz,
	        final Object id) {
		return em.getReference(clazz, id);
	}

	public static <POJO_TYPE> POJO_TYPE getEntityById(final EntityManager em, final Class<POJO_TYPE> clazz,
	        final Object id) {
		return em.find(clazz, id);
	}

	public static <POJO_TYPE> Collection<POJO_TYPE> getEntityByName(final EntityManager em,
	        final Class<POJO_TYPE> clazz, final String name) {
		return Util.getEntityByName(em, clazz, "name", name);
	}

	public static <POJO_TYPE> Collection<POJO_TYPE> getEntityByName(final EntityManager em,
	        final Class<POJO_TYPE> clazz, final String fieldName, final String name) {
		Query qryItemByName = em.createQuery(
		        "select item from " + clazz.getSimpleName() + " as item where item." + fieldName + " = :name")
		        .setParameter("name", name);
		Collection<POJO_TYPE> result = qryItemByName.getResultList();
		return result;
	}

	public static <POJO_TYPE> Collection<POJO_TYPE> getAllEntities(final EntityManager em, final Class<POJO_TYPE> clazz) {
		Query qryAllItems = em.createQuery("select item from " + clazz.getSimpleName() + " as item");
		Collection<POJO_TYPE> result = qryAllItems.getResultList();
		return result;
	}

	/**
	 * Deletes all entities of the specified type <tt>clazz</tt>. The
	 * <tt>clazz</tt> must be mapped with defined cascades to correct removing
	 * of entities.
	 * 
	 * @param <POJO_TYPE>
	 * @param em
	 * @param clazz
	 * @return
	 */
	public static <POJO_TYPE> int deleteAllEntities(final EntityManager em, final Class<POJO_TYPE> clazz) {
		Query qryAllItems = em.createQuery("select item from " + clazz.getSimpleName() + " as item");
		Collection<POJO_TYPE> allEntities = qryAllItems.getResultList();
		for (POJO_TYPE entity : allEntities) {
			em.remove(entity);
		}
		return allEntities.size();
	}

	public static Query createCriteriaQuery(final EntityManager em, final String what, final String from,
	        final ValueTO[] criteria) {
		StringBuilder sb = new StringBuilder();
		sb.append("select item" + ((what == null || what.isEmpty()) ? "" : ("." + what)) + " from " + from
		        + " as item where ");
		int i = 0;
		for (i = 0; i < criteria.length - 1; i++) {
			sb.append("item.");
			sb.append(criteria[i].getPropertyName());
			sb.append("=:");
			sb.append("val");
			sb.append(i);
			sb.append(" and ");
		}

		sb.append("item.");
		sb.append(criteria[i].getPropertyName());
		sb.append("=:");
		sb.append("val");
		sb.append(i);

		Query query = null;
		try {
			query = em.createQuery(sb.toString());
		} catch (IllegalArgumentException e) {
			throw new DALSystemException("Searching query is not valid.Check query criteria.\nDetails:"
			        + e.getLocalizedMessage());
		}
		int j = 0;
		do {
			query.setParameter("val" + j, criteria[j].getValue());
			j++;
		} while (j <= i);

		return query;
	}

	public static Query createCriteriaQuery(final EntityManager em, final String what, final String from,
	        final FindCriteria findCriteria) {
		StringBuilder sb = new StringBuilder();
		findCriteria.changeAliasName("item");
		sb.append("select item" + ((what == null || what.isEmpty()) ? "" : ("." + what)) + " from " + from
		        + " as item where ");
		sb.append(findCriteria.getWhere());

		Query query = null;
		try {
			query = em.createQuery(sb.toString());
		} catch (IllegalArgumentException e) {
			throw new DALSystemException("Searching query is not valid.Check query criteria.\nDetails:"
			        + e.getLocalizedMessage());
		}
		int i = 1;
		for (Object val : findCriteria.getOrderedParams()) {
			query.setParameter(i, val);
			i++;
		}
		return query;
	}

	public static <ID_TYPE> Collection<ID_TYPE> extractIDs(final Collection<? extends TO<?>> objIds,
	        final Class<ID_TYPE> clazz, final Types type) {
		Collection<ID_TYPE> ids = extractIDs(objIds, clazz, new Types[] { type }).get(type);
		return ids == null ? new LinkedList<ID_TYPE>() : ids;
	}

	/**
	 * Extracts native id from every TO instance from specified collection
	 * 
	 * @param <ID_TYPE>
	 * @param objIds
	 * @param clazz
	 * @return Map of <code>Types</code> and collection of native IDs
	 */

	public static <ID_TYPE> Map<Types, Collection<ID_TYPE>> extractIDs(final Collection<? extends TO<?>> objIds,
	        final Class<ID_TYPE> clazz, final Types[] types) {
		if (objIds == null || objIds.isEmpty()) {
			throw new IllegalArgumentException("Argument {objIds} cannot be null");
		}
		if (clazz == null) {
			throw new IllegalArgumentException("Argument {clazz} cannot be null");
		}
		if (types == null) {
			throw new IllegalArgumentException("Argument {types} cannot be null");
		}

		if (types.length == 0) {
			throw new IllegalArgumentException("Argument {types} cannot be empty");
		}

		Map<Types, Collection<ID_TYPE>> result = new HashMap<Types, Collection<ID_TYPE>>(types.length);
		for (Types type : types) {
			result.put(type, new HashSet<ID_TYPE>());
		}
		outer: for (TO<?> to : objIds) {
			for (Types type : types) {
				if (type.equals(Types.decode(to.getTypeName()))) {
					result.get(type).add((ID_TYPE) to.getID());
					continue outer;
				}
			}
		}
		return result;

	}

	public static <DTO_TYPE> DTO_TYPE convertEntityToDTO(final Class<DTO_TYPE> clazz, final DTOGetter<DTO_TYPE> entity) {
		return convertEntitiesToDTOs(clazz, Arrays.asList(entity), LinkedList.class).iterator().next();
	}

	/**
	 * Converts entities from DO to DTO(transfer objects)
	 * 
	 * @param <DTO_TYPE>
	 * @param clazz
	 *            What DTO is needed
	 * @param entities
	 *            Collection of entities which needed to convert , must
	 *            implement <code>DTOCompatible</code>
	 * @return Collection of DTOs
	 */
	public static <DTO_TYPE> Collection<DTO_TYPE> convertEntitiesToDTOs(final Class<DTO_TYPE> clazz,
	        final Collection<? extends DTOGetter<DTO_TYPE>> entities) {
		return convertEntitiesToDTOs(clazz, entities, LinkedList.class);
	}

	public static <DTO_TYPE> Collection<DTO_TYPE> convertEntitiesToDTOs(final Class<DTO_TYPE> clazz,
	        final Collection<? extends DTOGetter<DTO_TYPE>> entities, final Class<? extends Collection> container) {
		Collection<DTO_TYPE> result;
		try {
			result = container.newInstance();
		} catch (InstantiationException e) {
			throw new IllegalArgumentException("Supplied 'container' is not acceptable", e);
		} catch (IllegalAccessException e) {
			throw new IllegalArgumentException("Supplied 'container' is not acceptable", e);
		}
		// loop by alert types
		for (DTOGetter<?> entity : entities) {
			result.add(clazz.cast(entity.getDTO()));
		}
		return result;
	}

	public static <DTO> Collection<DTO> convertPropertiesToDTOs(final Class<DTO> clazz,
	        final Collection<List<CollectionTO>> values) {
		final List<DTO> dtos = CollectionUtils.newList();
		for (final List<CollectionTO> list : values) {
			for (final CollectionTO item : list) {
				dtos.add(Util.convertPropertiesToDTO(clazz, item.getPropValues()));
			}
		}
		return dtos;
	}

	public static <DTO> DTO convertPropertiesToDTO(final Class<DTO> clazz, final Collection<ValueTO> pValues) {
		DTO dtoInstance;
		try {
			dtoInstance = clazz.newInstance();
		} catch (InstantiationException e) {
			throw new IllegalArgumentException("Supplied 'clazz' is wrong", e);
		} catch (IllegalAccessException e) {
			throw new IllegalArgumentException("Supplied 'clazz' is wrong", e);
		}
		for (ValueTO pvto : pValues) {
			try {
				if (pvto.getValue() != null)
					setProperty(dtoInstance, pvto.getPropertyName(), pvto.getValue());
			} catch (Exception e) {
				throw new IllegalArgumentException("Supplied 'clazz' cannot be instantiated by given 'pValues'", e);
			}
		}
		return dtoInstance;
	}

	public static <DTO> List<ValueTO> convertDTOToProperties(final DTO instance) {
		if (instance == null)
			return null;
		PropertyDescriptor[] descs = getPropDescriptors(instance.getClass());

		List<ValueTO> values = new ArrayList<ValueTO>(descs.length);
		for (PropertyDescriptor prop : descs) {
			values.add(new ValueTO(prop.getName(), getProperty(instance, prop.getName())));
		}

		return values;
	}

	/**
	 * Returns object IDs for the specified object ID list which are
	 * hierarchically related to them.
	 * 
	 * @param objIds
	 *            IDs of objects
	 * @return Collection of TOs related to <code>objIds</code>
	 */
	public static Collection<TO<?>> getIDsWithPropagatingByHierarchy(final Environment env,
	        final Collection<TO<?>> objIds) {
		Collection<TO<?>> result = new HashSet<TO<?>>();
		for (TO<?> to : objIds) {
			result.add(to);
			if (TOFactory.EMPTY_TO.equals(to)) {
				continue;
			}
			for (TO<?> child : env.getRelatedObjects(to, null, true)) {
				result.add(child);
			}
		}
		return result;
	}

	public static <T extends TO<?>> Collection<T> filterObjectsByTypeName(final Collection<T> objIds,
	        final String typeName) {
		// early return if no filter defined
		if (typeName == null || typeName.isEmpty()) {
			return objIds;
		}

		Collection<T> result = new LinkedList<T>();
		for (T id : objIds) {
			// filter by type name
			T object = Util.filterObjectByTypeName(id, typeName);
			// if object does not match the filter
			if (object == null) {
				continue;
			}
			result.add(id);
		}
		return result;
	}

	public static <T extends TO<?>> T filterObjectByTypeName(final T objId, final String typeName) {
		if (typeName == null || typeName.isEmpty()) {
			return objId;
		}

		if (typeName.equalsIgnoreCase(objId.getTypeName())) {
			return objId;
		}

		return null;
	}

	public static boolean containsNull(final Collection<?> items) {
		for (Object item : items) {
			if (item == null) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Checks the type of given object If <tt>typeName</tt> is null or empty
	 * returns <tt>true</tt>
	 * 
	 * @param id
	 * @param typeName
	 * @return
	 */
	public static boolean hasType(final TO id, final String typeName) {
		if (typeName == null || typeName.isEmpty()) {
			return true;
		}
		return id.getTypeName().equalsIgnoreCase(typeName);
	}

	public static <T> List<List<T>> divideByPieces(final Collection<T> input, int pieceSize) {
		if (pieceSize <= 0) {
			throw new IllegalArgumentException("Supplied 'pieceSize' is negative or zero");
		}

		// divide input collection to pieces by singlePieceSize
		List<List<T>> pieces = new LinkedList<List<T>>();
		List<T> piece = new LinkedList<T>();

		int counter = 1;
		for (T val : input) {
			piece.add(val);
			if (counter >= pieceSize) {
				pieces.add(new LinkedList<T>(piece));
				piece.clear();
				counter = 0;
			}
			counter++;
		}
		// if piece has elements after dividing
		if (piece.size() > 0) {
			// forms a last piece
			pieces.add(new LinkedList<T>(piece));
		}
		return pieces;
	}

	public static int countArray(final int[] arr) {
		int count = 0;
		for (int i : arr) {
			count += i;
		}
		return count;
	}

	public static List<AbstractValue> createNewTypedValues(ValueDO root, TypedValue value)
	        throws UnableToConvertPropertyException {
		List<AbstractValue> result = new LinkedList<AbstractValue>();
		List<Object> valuesToCreate = new LinkedList<Object>();
		String classNameToResolve = value.getType().getName();

		final boolean isCollection = value.isCollection();
		if (isCollection) {
			if (!((Collection) value.getValue()).isEmpty()) {
				valuesToCreate.addAll((Collection) value.getValue());
			}
		} else {
			valuesToCreate.add(value.getValue());
		}

		Class<?> clazz = PropertyTypes.checkAndGetPropertyTypeClass(classNameToResolve);
		Constructor valueConstructor = null;
		try {
			valueConstructor = clazz.getConstructor(ValueDO.class, Object.class);
		} catch (NoSuchMethodException e) {
			throw new DALSystemException("No such constructor in " + clazz.getSimpleName() + " class.", e);
		}

		for (Object nextValue : valuesToCreate) {
			try {
				// creates new concrete value (scalar , link ...)
				result.add((AbstractValue) valueConstructor.newInstance(root, nextValue));
			} catch (InstantiationException e) {
				throw new DALSystemException("Object of [" + clazz.getSimpleName() + "] class cannot be instantiated",
				        e);
			} catch (IllegalAccessException e) {
				throw new DALSystemException("Constructor in [" + clazz.getSimpleName() + "] is non-public", e);
			} catch (InvocationTargetException e) {
				if (e.getCause() instanceof ClassCastException) {
					throw new UnableToConvertPropertyException(root.getAttribute().getAttributeName(), e);
				}

                if (e.getCause() instanceof InvalidValueException) {
                    throw (InvalidValueException)e.getCause();
                }

				throw new DALSystemException("Problems occurred during instantiating an object of ["
					        + clazz.getSimpleName() + "] class", e);
			}
		}

		return result;
	}

	public static Class classByName(String name) throws ClassNotFoundException {
		return Class.forName(name);
	}
}
