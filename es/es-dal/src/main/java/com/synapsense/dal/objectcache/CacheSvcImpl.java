/**
 * 
 */
package com.synapsense.dal.objectcache;

import com.google.common.base.Supplier;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;
import com.synapsense.cdi.MBean;
import com.synapsense.dal.generic.utils.Util;
import com.synapsense.dto.*;
import com.synapsense.exception.*;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.search.EnvElement;
import com.synapsense.search.QueryDescription;
import com.synapsense.search.QueryImpl;
import com.synapsense.search.Results;
import com.synapsense.service.Environment;
import com.synapsense.service.impl.dao.to.EnvObjTypeTO;
import com.synapsense.service.impl.dao.to.GenericObjectTO;
import com.synapsense.service.impl.dao.to.PropertyTO;
import com.synapsense.service.impl.nsvc.EventNotifier;
import com.synapsense.service.nsvc.Event;
import com.synapsense.service.nsvc.NotificationService;
import com.synapsense.service.nsvc.events.*;
import com.synapsense.service.nsvc.filters.EventTypeFilter;
import com.synapsense.util.CollectionUtils;
import com.synapsense.util.Pair;
import com.synapsense.util.executor.Action;
import com.synapsense.util.executor.Executor;
import com.synapsense.util.executor.Executors;
import org.apache.log4j.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.*;
import java.util.*;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author aborisov
 * 
 */
@Startup
@Singleton(name = "SynapCache")
@DependsOn({ "GenericEnvironment", "LocalNotificationService" })
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
@Remote(CacheSvc.class)
@ConcurrencyManagement(ConcurrencyManagementType.BEAN)
@MBean("com.synapsense:type=CacheSvc")
public class CacheSvcImpl implements CacheSvc, CacheSvcImplMBean, ConfigurationStartedEventProcessor,
        ConfigurationCompleteEventProcessor {
	private final static Logger logger = Logger.getLogger(CacheSvcImpl.class);

	private volatile int status;

	private volatile boolean configuring = false;

	// Cache for links resolving, need to time stamp resolving
	// The entry's key is a TO of an object which is referenced by the
	// properties
	// specified as a set TO/propName pair in the entry's value.
	// Each property identified by TO/propName is of TO type.
	private Map<TO<?>, Set<PropertyTO>> linksOfObjectsCache;

	// Cache for property links resolving, need to time stamp resolving
	// The entry's key is a property (TO/propName) of an object which is
	// referenced
	// by the properties specified as a set TO/propName pair in the entry's
	// value.
	// Each property identified by TO/propName in value set is of PropertyTO
	// type.
	private Map<PropertyTO, Set<PropertyTO>> linksOfPropertyCache;

	private Environment env;

	@EJB(beanName = "GenericEnvironment")
	private CacheDependant cdp;

	private EventNotifier eventNotifier;

	private NotificationService notificationService;

	private DispatchingProcessor dispProcessor;

	private AlertCounter alertCounter;

	/**
	 * index implementation provider for SimpleTypes
	 */
	private IndexProvider indexProvider;

	private static class DoubleFilter {
		private final static Double NAN = new Double(Double.NaN);
		private final static Double POS_INFINITY = new Double(Double.POSITIVE_INFINITY);
		private final static Double NEG_INFINITY = new Double(Double.NEGATIVE_INFINITY);

		static boolean isSpecial(final Object value) {
			if (NAN.equals(value))
				return true;
			if (POS_INFINITY.equals(value))
				return true;
			if (NEG_INFINITY.equals(value))
				return true;
			return false;
		}
	}

	@EJB(beanName = "LocalNotificationService")
	public void setEventNotifier(final EventNotifier notifier) {
		this.eventNotifier = notifier;
	}

	@EJB(beanName = "LocalNotificationService")
	public void setNotificationService(NotificationService notificationService) {
		this.notificationService = notificationService;
	}

	@Override
	public Collection<CollectionTO> getAllPropertiesValues(final Collection<TO<?>> objIds) {

		Collection<CollectionTO> ret = new LinkedList<CollectionTO>();
		Collection<ValueTO> tmp = null;
		for (TO<?> to : objIds) {
			CollectionTO newResultItem = new CollectionTO(to);
			ret.add(newResultItem);
			try {
				tmp = getAllPropertiesValues(to);
				newResultItem.addAll(tmp);
			} catch (ObjectNotFoundException e) {
                logger.warn(e.getMessage(), e);
            }
		}
		return ret;
	}

	@Override
	public Collection<ValueTO> getAllPropertiesValues(final TO<?> objId) throws ObjectNotFoundException {

		SimpleObject so = getObject(objId);

		if (so == null) {
			throw new ObjectNotFoundException(objId);
		}

		Collection<ValueTO> ret = new LinkedList<ValueTO>();
		Collection<SimplePropertyValue> data = so.getAllPropertiesValues();
		for (SimplePropertyValue sp : data) {
			ret.add(new ValueTO(sp.getPropName(), sp.getValue().getValue(), sp.getValue().getTimestamp()));
		}

		return ret;
	}

	@Override
	public Collection<TO<?>> getObjects(final String typeName, final ValueTO[] propertyValues) {
		return getObjects(typeName, ObjectTypeMatchMode.DEFAULT, propertyValues);
	}

	@Override
	public Collection<TO<?>> getObjects(final String typeName, final ObjectTypeMatchMode matchMode,
	        final ValueTO[] propertyValues) {
		Collection<TO<?>> result = new LinkedList<TO<?>>();
		Collection<SimpleType> types = getTypes(typeName, matchMode);
		for (SimpleType st : types) {
			result.addAll(st.find(propertyValues));
		}
		return result;
	}

	@Override
	public Collection<TO<?>> getObjectsByType(final String typeName) {
		return getObjectsByType(typeName, ObjectTypeMatchMode.EXACT_MATCH);
	}

	@Override
	public Collection<TO<?>> getObjectsByType(final String typeName, final ObjectTypeMatchMode matchMode) {
		Collection<TO<?>> ret = new LinkedList<TO<?>>();
		Collection<SimpleType> sts = getTypes(typeName, matchMode);
		if (sts.isEmpty())
			return ret;
		for (SimpleType st : sts) {
			for (Object o : st.getAllObjects()) {
				ret.add(TOFactory.getInstance().loadTO(st.getName() + ":" + o));
			}
		}
		return ret;
	}

	@Override
	public Collection<CollectionTO> getPropertyValue(final Collection<TO<?>> objIds, final String[] propNames) {

		Collection<CollectionTO> ret = new LinkedList<CollectionTO>();
		SimpleType st = null;
		ValueTO value = null;
		for (TO<?> to : objIds) {
			st = getType(to.getTypeName());
			if (st == null)
				continue;
			CollectionTO pvto = new CollectionTO(to);
			ret.add(pvto);

			for (String prop : propNames) {
				try {
					value = st.getValue((Integer) to.getID(), prop, ValueTO.class);
					if (value != null) {
						pvto.addValue(value);
					}
				} catch (ObjectNotFoundException e) {
					if (logger.isDebugEnabled()) {
						logger.debug("No such object : " + to, e);
					}
				} catch (PropertyNotFoundException e) {
					if (logger.isDebugEnabled()) {
						logger.debug("No such property : object " + to + ", property " + prop, e);
					}
				} catch (UnableToConvertPropertyException e) {
					if (logger.isDebugEnabled()) {
						logger.debug("Converting error : object " + to + ", property " + prop, e);
					}
				}
			}
		}

		return ret;
	}

	@Override
	public <RETURN_TYPE> RETURN_TYPE getPropertyValue(final TO<?> objId, final String propName,
	        final Class<RETURN_TYPE> clazz) throws ObjectNotFoundException, PropertyNotFoundException,
	        UnableToConvertPropertyException {

		if (objId == null) {
			throw new ObjectNotFoundException("Cannot find object by null TO");
		}

		SimpleType st = getType(objId.getTypeName());

		if (st == null) {
			throw new ObjectNotFoundException(objId);
		}
		try {
			return st.getValue((Integer) objId.getID(), propName, clazz);
		} catch (ObjectNotFoundException | PropertyNotFoundException | UnableToConvertPropertyException e) {
			if (logger.isDebugEnabled()) {
				logger.debug("Couldn't get property value for object : " + objId.getID()
						+ ". Caught " + e.getClass().getName() + ": " + e.getLocalizedMessage());
			}
			return null;
		}

	}

	@Override
	@PostConstruct
	public void create() throws Exception {
		if(instance!=null)
			return;
		linksOfObjectsCache = new ConcurrentHashMap<>();
		linksOfPropertyCache = new ConcurrentHashMap<>();
		alertCounter = new AlertCounter(this, SimpleType.NUM_ALERTS_PROPERTY_NAME);

		final Map<Object, Collection<Integer>> indexMap = new ConcurrentHashMap<Object, Collection<Integer>>();
		final Supplier<Collection<Integer>> factory = new Supplier<Collection<Integer>>() {
			@Override
			public Collection<Integer> get() {
				return Collections.synchronizedSet(new HashSet<Integer>(0));
			}
		};

		indexProvider = new IndexProvider() {
			@Override
			public Multimap<Object, Integer> get() {
				return Multimaps.newMultimap(indexMap, factory);
			}
		};
	
		logger.info("CacheSvc created");
		status = Status.OFFLINE;
		instance = this;
		start();
	}

	@Override
	public void destroy() {
		logger.info("CacheSvc destroy");
		instance = null;
	}

	@Override
	public void start() throws Exception {
		logger.info("CacheSvc starting...");
		if (status != Status.OFFLINE) {
			logger.warn("Cannot start cache service because it is already started");
			return;
		}

		cdp.setCache(null);

		if (Boolean.parseBoolean(System.getProperty("com.synapsense.dal.objectcache.cache.isdebug", "false"))) {
			status = Status.OFFLINE;
		} else {
			logger.info("Loading information regarding all system types");
			// loads types in sequential mode till the more effective approach
			// to get data from mysql(instead of batch fetching by BATCH_SIZE)
			// be found
			int threadsNum = 1;// Runtime.getRuntime().availableProcessors();
			if (logger.isDebugEnabled())
				logger.debug("Cache will start on " + threadsNum + " threads");
			// cache load helper(can be used as separate component to parallel
			// operations)
			Executor dataLoader = Executors.newPooledExecutor("cache service objects loader", threadsNum);
			try {
				String types[] = env.getObjectTypes();
				for (final String type : types) {
					loadTypeInfo(type, dataLoader);
				}
			} finally {
				dataLoader.shutdownAwait();
			}

			// loads relations in sequential mode
			threadsNum = 1;
			dataLoader = Executors.newPooledExecutor("cache service relations loader", threadsNum);
			try {
				// load info about object relations to the cache
				logger.info("Loading relations between objects...");
				// load relations action list
				List<Action> loadRelationActionList = new LinkedList<Action>();
				for (final SimpleType st : SimpleType.enumerateAllTypes()) {
					// add new separate task
					loadRelationActionList.add(new Action() {
						@Override
						public void execute() {
							logger.info("Loading relations of object type : " + st.getName());
							processRelations(getObjectsByType(st.getName()));
						}

						@Override
						public String toString() {
							return "Loading relations of objects of type :" + st.getName();
						}

					});
				}

				// executes tasks and waits for they finish
				dataLoader.doAction(loadRelationActionList, true);

				// repack sets tp copy-on-write ones when cache state is already
				// loaded
				// this makes cache to start twice as fast
				for (Entry<TO<?>, Set<PropertyTO>> entry : linksOfObjectsCache.entrySet()) {
					entry.setValue(CollectionUtils.newCopyOnWriteSet(entry.getValue()));
				}

				for (Entry<PropertyTO, Set<PropertyTO>> entry : linksOfPropertyCache.entrySet()) {
					entry.setValue(CollectionUtils.newCopyOnWriteSet(entry.getValue()));
				}

				status = Status.ONLINE;

				// loading alerts.
				// note that it must be done only after loading relations
				logger.info("Processing active alerts...");

				// rebase uses env - all changes made in GenericEnv should
				// propagate back to cache
				cdp.setCache(this);// hack, must be
				                   // SessionContext.businessObject
				alertCounter.rebaseAll(env);

				// subscribe to events
				dispProcessor = new DispatchingProcessor().putCcep(this).putCsep(this);
				Collection<Class<? extends Event>> events = new HashSet<Class<? extends Event>>();
				events.add(ConfigurationStartedEvent.class);
				events.add(ConfigurationCompleteEvent.class);
				notificationService.registerProcessor(dispProcessor, new EventTypeFilter(events));

				logger.info("Cache is ready for usage");
			} finally {
				dataLoader.shutdownAwait();
			}
		}
	}

	private void loadTypeInfo(final String type, final Executor dataLoader) {
		/**
		 * Loading type information
		 */
		logger.info("processing [" + type + "]");

		ObjectType ot = env.getObjectType(type);
		if (ot == null) {
			logger.warn("Unable to get properties information for type [" + type + "]");
			return;
		}

		final SimpleType st = SimpleType.newType(ot, indexProvider);

		final HashSet<String> links = new HashSet<String>();
		final HashSet<String> props = new HashSet<String>();
		for (PropertyDescr pd : ot.getPropertyDescriptors()) {
			if (pd.getTypeName().equals(PropertyTO.class.getName())) {
				props.add(pd.getName());
				if (logger.isDebugEnabled()) {
					logger.debug("Found property link [" + pd.getName() + "]:[" + pd.getType() + "]");
				}
			}
			if (pd.getTypeName().equals(TO.class.getName())) {
				links.add(pd.getName());
				if (logger.isDebugEnabled()) {
					logger.debug("Found link [" + pd.getName() + "]:[" + pd.getType() + "]");
				}
			}
		}

		Collection<TO<?>> objects = env.getObjectsByType(type);
		if (objects == null || objects.isEmpty()) {
			return;
		}

		// size of single piece of data
		int threadWorkSize = objects.size();
		if (dataLoader.threadsNum() > 1) {
			threadWorkSize = objects.size() < dataLoader.threadsNum() * 2 ? objects.size() : objects.size()
			        / dataLoader.threadsNum();
		}
		// collection divided to smaller collections of single piece size
		final List<List<TO<?>>> dividedObjects = Util.divideByPieces(objects, threadWorkSize);
		// load type action list
		List<Action> loadTypeActionList = new LinkedList<Action>();
		// add tasks for execution
		for (final List<TO<?>> list : dividedObjects) {
			// add new separate task
			loadTypeActionList.add(new Action() {
				@Override
				public void execute() {
					loadObjectsContent(st, links, props, list);
				}

				@Override
				public String toString() {
					return "Loading objects of type:" + st.getName() + "; objects number : " + list.size();
				}

			});
		}

		// executes tasks and waits for they finish
		dataLoader.doAction(loadTypeActionList, true);

	}

	/**
	 * Loading objects information
	 */
	private void loadObjectsContent(final SimpleType st, final Set<String> links, final Set<String> props,
	        final Collection<TO<?>> objects) {
		if (objects == null || objects.isEmpty()) {
			return;
		}

		Collection<CollectionTO> objectsPropValues = env.getAllPropertiesValues(objects);
		if (objectsPropValues == null || objectsPropValues.isEmpty()) {
			return;
		}

		for (CollectionTO collectionValue : objectsPropValues) {
			TO<?> to = collectionValue.getObjId();
			// creates object via local create object
			createObject(to, collectionValue.getPropValues().toArray(new ValueTO[0]));
			// Performs any kind of links
			for (ValueTO pvto : collectionValue.getPropValues()) {
				if (links.contains(pvto.getPropertyName())) {
					try {
						if (this.getPropertyDescriptor(st.getName(), pvto.getPropertyName()).isCollection()) {
							Collection<TO<?>> toSet = (Collection) pvto.getValue();
							for (TO<?> pto : toSet) {
								loadLinks(pvto, pto, to);
							}
						} else {
							loadLinks(pvto, (TO<?>) pvto.getValue(), to);
						}
					} catch (ObjectNotFoundException e) {
					} catch (PropertyNotFoundException e) {
					}
				}

				if (props.contains(pvto.getPropertyName()) && pvto.getValue() != null) {
					try {
						if (this.getPropertyDescriptor(st.getName(), pvto.getPropertyName()).isCollection()) {
							Collection<PropertyTO> propertySet = (Collection) pvto.getValue();
							for (PropertyTO pto : propertySet) {
								loadPropLinks(pto, new PropertyTO(to, pvto.getPropertyName()));
							}
						} else {
							loadPropLinks((PropertyTO) pvto.getValue(), new PropertyTO(to, pvto.getPropertyName()));
						}
					} catch (ObjectNotFoundException e) {
					} catch (PropertyNotFoundException e) {
					}

				}
			}
		}
		loadTags(st);
	}

	/**
	 * Loads all relations between objects into the cache. Must be invoked only
	 * after loading all types and objects information from storage.
	 * 
	 */
	private void processRelations(final Collection<TO<?>> objects) {
		// gets all objects of the current type
		if (objects == null || objects.isEmpty()) {
			return;
		}
		// gets all children of objects
		Collection<CollectionTO> allChildren = env.getChildren(objects);
		if (allChildren == null || allChildren.isEmpty()) {
			return;
		}
		// loading relations
		// loops by collection of objects that have children
		for (CollectionTO objectChildren : allChildren) {
			// current object which may have children
			TO<?> toParent = objectChildren.getObjId();
			// gets cache object regarding to current TO
			SimpleObject soParent = this.getObject(toParent);
			if (soParent == null) {
				throw new EnvSystemException("The TO : " + toParent.toString() + " does not exist in the cache!");
			}
			// loops by certain object's children
			for (ValueTO child : objectChildren.getPropValues()) {
				// current child as TO
				TO<?> toChild = (TO<?>) child.getValue();

				if (toParent.equals(toChild)) {
					continue;
				}

				// adds it to the
				soParent.getChildren().add(Pair.newPair(toChild.getTypeName(), (Integer) toChild.getID()));
				// child object from cache
				SimpleObject soChild = this.getObject(toChild);
				if (soChild == null) {
					throw new EnvSystemException("The TO : " + toChild.toString() + " does not exist in the cache!");
				}

				soChild.getParents().add(Pair.newPair(toParent.getTypeName(), (Integer) toParent.getID()));

			}
		}
	}

	private void loadLinks(final ValueTO srcData, final TO<?> srcTo, final TO<?> dstTo) {
		if (srcTo == null) {
			return;
		}

		if (logger.isDebugEnabled()) {
			logger.debug("src [" + srcData + "] -> [" + srcTo + "]");
			logger.debug("Processing link to TO [" + srcTo + "]");
		}

		PropertyTO srcPointer = new PropertyTO(dstTo, srcData.getPropertyName());
		checkAndSetObjectLink(srcTo, srcPointer);
	}

	private void loadPropLinks(final PropertyTO srcData, PropertyTO destData) {
		checkAndSetObjectPropertyLink(srcData, destData);
	}

	private void updateTimeStamp(final TO<?> to, ChangedValueTO value) {
		final String propName = value.getPropertyName();

		SimpleObject so = getObject(to);

		if (so == null) {
			return;
		}

		Set<PropertyTO> l = linksOfObjectsCache.get(to);
		if (l == null) {
			if (logger.isDebugEnabled()) {
				logger.debug("Object [" + to + "] has no links referencing it");
			}

		} else {
			for (PropertyTO p : l) {
				so = getObject(p.getObjId());
				if (so != null) {
					eventNotifier.notify(new ReferencedObjectChangedEvent(p.getObjId(), p.getPropertyName(), to,
					        new HashSet<String>(Arrays.asList(propName))));
				}
			}
		}

		l = linksOfPropertyCache.get(new PropertyTO(to, propName));
		if (l == null) {
			if (logger.isDebugEnabled()) {
				logger.debug("Object [" + to + "] has no links referencing its properties");
			}
			return;
		}
		// for those objects which referenced by other objects
		// send special event
		for (PropertyTO p : l) {
			so = getObject(p.getObjId());
			if (so != null) {
				eventNotifier.notify(new ReferencedPropertyChangedEvent(p.getObjId(), to, p.getPropertyName(), value));
			}
		}
	}

	@Override
	public int getStatus() {
		return status;
	}

	@Override
	@PreDestroy
	public void stop() {
		if (instance == null)
			return;
		if (status != Status.ONLINE) {
			logger.warn("Cannot stop cache cause it is not started yet");
			return;
		}

		logger.info("CacheSvc stopping...");

		logger.info("Removing all current cache state...");

		SimpleType.removeAllTypes();
		linksOfObjectsCache.clear();
		linksOfPropertyCache.clear();

		// not a subscriber now
		if (dispProcessor != null) {
			notificationService.deregisterProcessor(dispProcessor);
			dispProcessor = null;
		}

		status = Status.OFFLINE;
		logger.info("CacheSvc status changed to : " + status);

		logger.info("Stopped CacheSvc");
		destroy();
	}

	public Object queryPropInfo(final String type, final Integer oid, final String propName, final String clazz) {

		try {
			return getPropertyValue(new GenericObjectTO(oid, type), propName, Class.forName(clazz));
		} catch (Exception e) {
			return e;
		}
	}

	@Override
	public Object queryObjectInfo(final String type, final Integer obj) {
		TO<?> id = TOFactory.getInstance().loadTO(type, obj);
		SimpleObject so = getObject(id);
		return so == null ? "Object does not exist" : so.toString();
	}

	public Environment getEnv() {
		return env;
	}

	@EJB(beanName = "GenericEnvironment")
	public void setEnv(final Environment env) {
		this.env = env;
	}

	@Override
	public Object printAllInfo() {
		StringBuffer sb = new StringBuffer(5000);
		sb.append("Type cache\n");
		for (SimpleType type : SimpleType.enumerateAllTypes()) {
			sb.append(type.getName());
			sb.append("\n-----------\n");
			sb.append(type);
			sb.append("\n=================================\n");
		}

		sb.append("Links cache\n");
		for (Map.Entry<TO<?>, Set<PropertyTO>> entry : linksOfObjectsCache.entrySet()) {
			sb.append("TO=[").append(entry.getKey()).append("]->\n");
			for (PropertyTO p1 : entry.getValue()) {
				sb.append("\t");
				sb.append(p1);
				sb.append("\n");
			}

			sb.append("\n----------------------------\n");
		}

		sb.append("\n=================================\n");

		sb.append("PropertyLink cache\n");
		for (Map.Entry<PropertyTO, Set<PropertyTO>> entry : linksOfPropertyCache.entrySet()) {
			sb.append(entry.getKey()).append("->\n");
			for (PropertyTO p1 : entry.getValue()) {
				sb.append("\t");
				sb.append(p1);
				sb.append("\n");
			}

			sb.append("\n----------------------------\n");
		}
		sb.append("\n=================================\n");

		return sb.toString();

	}

	@Override
	public Object queryTypeInfo(final String typeName) {
		StringBuffer sb = new StringBuffer(5000);
		sb.append("Type [");
		sb.append(typeName);
		sb.append("]\n");
		sb.append(SimpleType.findType(typeName));
		return sb.toString();
	}

	@Override
	public void createObjectType(final ObjectType type) {
		String name = type.getName();
		if (logger.isDebugEnabled()) {
			logger.debug("Creating type [" + name + "]");
		}

		SimpleType.newType(type, indexProvider);
		eventNotifier.notify(new ObjectTypeCreatedEvent(type));

		if (logger.isDebugEnabled()) {
			logger.debug("Type [" + name + "] created");
		}

	}

	@Override
	public void updateObjectType(final ObjectType objType) {
		String envTypeName = objType.getName();
		if (logger.isDebugEnabled()) {
			logger.debug("Updating type [" + envTypeName + "]");
		}
		SimpleType cachedType = getType(envTypeName);

		if (cachedType == null)
			return;

		ObjectTypeUpdatedEvent e = cachedType.updateEnvType(objType);
		eventNotifier.notify(e);

		if (logger.isDebugEnabled()) {
			logger.debug("Type [" + envTypeName + "] updated");
		}
	}

	@Override
	public void deleteObjectType(final String name) {
		if (logger.isDebugEnabled()) {
			logger.debug("Removing type [" + name + "]");
		}

		Collection<String> toRemove = new LinkedList<String>();
		toRemove.add(name);

		for (String t : toRemove) {
			for (TO<?> obj : this.getObjectsByType(t)) {
				this.deleteObject(obj);
			}
		}

		for (String t : toRemove) {
			if (SimpleType.removeType(t) != null) {
				removeTypeFromLinksCache(t.toUpperCase());
				removeTypeFromPropertyLinksCache(t.toUpperCase());

			} else {
				if (logger.isDebugEnabled()) {
					logger.debug("Type [" + name + "] not found");
				}
			}

		}

		eventNotifier.notify(new ObjectTypeDeletedEvent(name));

		if (logger.isDebugEnabled()) {
			logger.debug("Type [" + name + "] was removed");
		}
	}

	private SimpleObject createObject(final TO<?> ID, final ValueTO[] propertyValues) {
		String typeName = ID.getTypeName();

		if (logger.isDebugEnabled()) {
			logger.debug("Creating  object of type [" + typeName + "]");
		}

		SimpleType st = getType(typeName);
		if (st == null) {
			if (logger.isDebugEnabled()) {
				logger.debug("Wrong type = [" + typeName + "], ignoring");
			}
			throw new IllegalStateException("Cache is incomplete state");
		}

		Integer oid = (Integer) ID.getID();
		SimpleObject so = st.addObject(oid);

		// if prop.values array is not null
		if (propertyValues != null) {
			try {
				setAllPropertiesValues(ID, propertyValues, false);
			} catch (ObjectNotFoundException e) {
				logger.warn("Object " + ID + " does not exist or it has just been removed", e);
			} catch (PropertyNotFoundException e) {
				logger.warn("Error occured when creating object", e);
			} catch (UnableToConvertPropertyException e) {
				logger.warn("Error occured when creating object", e);
			}
		}

		return so;
	}

	@Override
	public void createObject(final TO<?> ID, final ValueTO[] propertyValues, final boolean notify) throws EnvException {

		SimpleObject so = createObject(ID, propertyValues);

		String typeName = ID.getTypeName();

		if (Types.decode(typeName).equals(Types.ALERT)) {
			try {
				TO<?> host = (TO<?>) so.getPropertyValue("hostTO").getValue().getValue();
				if (!configuring) {
					alertCounter.inc(host);
				}
			} catch (PropertyNotFoundException e) {
				logger.error("Detected malformed object type : " + Types.ALERT, e);
			}
		}

		if (notify) {
			eventNotifier.notify(new ObjectCreatedEvent(ID, propertyValues));
		}

		if (logger.isDebugEnabled()) {
			logger.debug("Object [" + ID + "] was put in a cache");
		}

	}

	@Override
	public void deleteObject(final TO<?> objId) {
		if (objId == null) {
			logger.warn("Requested to delete an object by null TO, ignoring");
			return;
		}

		if (logger.isDebugEnabled()) {
			logger.debug("Removing object [" + objId + "]");
		}

		Collection<TO<?>> toRemove = new LinkedList<TO<?>>();
		toRemove.add(objId);

		// object's type name
		String name = objId.getTypeName();
		// if deleted object is alert
		if (Types.decode(name).equals(Types.ALERT)) {
			try {
				// decrease numAlerts counter on related host
				TO<?> host = this.getPropertyValue(objId, "hostTO", TO.class);
				if (!configuring) {
					alertCounter.dec(host);
				}
			} catch (EnvException e) {
				logger.warn("Error occurred while getting 'hostTO' property of alert : " + objId, e);
			}
		}

		for (TO<?> to : toRemove) {
			SimpleType st = getType(to.getTypeName());
			if (st == null)
				continue;
			Collection<TO<?>> firstLevelParents = null;
			try {
				firstLevelParents = this.getParents(to, null);
			} catch (ObjectNotFoundException e) {
				logger.warn("Cache threads interferrence. Object " + to + " already deleted");
				continue;
			}

			st.removeObject((Integer) to.getID());
			// when relations have broken
			// ready to rebase numAlerts
			if (firstLevelParents != null && !configuring) {
				for (TO<?> parent : firstLevelParents) {
					alertCounter.rebase(parent);
				}
			}

			removeObjectFromLinksCache(to);

			removeObjectFromPropLinkCache(to);

			eventNotifier.notify(new ObjectDeletedEvent(to));
		}

		if (logger.isDebugEnabled()) {
			logger.debug("Object [" + objId + "] was removed");
		}

	}

	private void loadTags(SimpleType st) {
		String typeName = st.getName();
		// Get all the tags for the type.
		Map<TO<?>, Collection<Tag>> tags = env.getAllTags(typeName);
		if (tags != null) {
			// For each object which has tags (i.e. it is included in the
			// keySet of
			// the map, returned by the getAllTags method) we need to
			// fulfill tag
			// details of associated SimpleObject object.
			for (Map.Entry<TO<?>, Collection<Tag>> entry : tags.entrySet()) {
				TO obj = entry.getKey();
				SimpleObject so = st.getObject((Integer) obj.getID());
				if (so == null) {
					if (logger.isDebugEnabled()) {
						logger.debug("Wrong object of type = [" + st.getName() + "] id = [" + obj.getID()
						        + "], has not tags");
					}
					throw new IllegalStateException("Cache is incomplete state");
				}
				// Fill tag values in the SimpleObject object for each tag,
				// which is found for
				// the associated TO.
				for (Tag tag : entry.getValue()) {
					try {
						so.setTagValue(tag.getPropertyName(), tag.getTagName(), tag.getValue());
					} catch (UnableToConvertTagException e) {
						if (logger.isDebugEnabled()) {
							logger.debug("tag [" + tag.getTagName() + "] of Object of type = [" + st.getName()
							        + "] id = [" + obj.getID() + "], not set value [" + tag.getValue()
							        + "with Exception: " + e.getMessage());
						}
					} catch (PropertyNotFoundException e) {
						if (logger.isDebugEnabled()) {
							logger.debug("tag [" + tag.getTagName() + "] of Object of type = [" + st.getName()
							        + "] id = [" + obj.getID() + "], not set value [" + tag.getValue()
							        + "with Exception: " + e.getMessage());
						}
					} catch (TagNotFoundException e) {
						if (logger.isDebugEnabled()) {
							logger.debug("tag [" + tag.getTagName() + "] of Object of type = [" + st.getName()
							        + "] id = [" + obj.getID() + "], not set value [" + tag.getValue()
							        + "with Exception: " + e.getMessage());
						}
					}
				}
			}
		}

	}

	@Override
	public void setAllPropertiesValues(final TO<?> objId, final ValueTO[] values) throws ObjectNotFoundException,
	        PropertyNotFoundException, UnableToConvertPropertyException {
		setAllPropertiesValues(objId, values, true);
	}

	@Override
	public void setAllPropertiesValues(final TO<?> objId, final ValueTO[] values, final boolean notify)
	        throws ObjectNotFoundException, PropertyNotFoundException, UnableToConvertPropertyException {
		SimpleType st = getType(objId.getTypeName());
		if (st == null)
			throw new ObjectNotFoundException(objId);

		// check for 0L timestamps and update them to current time
		List<ValueTO> verifiedValues = verifyAndConvert(values);

		Map<String, ChangedValueTO> changedValues = null;
		if (notify) {
			changedValues = findChangedValues(objId, verifiedValues);
		}

		ChangedValueTO changedValue = null;
		for (ValueTO verifiedValue : verifiedValues) {
			if (notify) {
				changedValue = changedValues.get(verifiedValue.getPropertyName());
			} else {
				changedValue = getChangedValue(objId, verifiedValue);
			}

			st.setPropValue((Integer) objId.getID(), verifiedValue);

			if (verifiedValue.getValue() instanceof TO) {
				updateLinksCache(objId, verifiedValue.getPropertyName(), (TO<?>) verifiedValue.getValue());
			}

			if (verifiedValue.getValue() instanceof Collection && !((Collection<?>) verifiedValue.getValue()).isEmpty()) {
				try {
					if ((this.getPropertyDescriptor(objId.getTypeName(), verifiedValue.getPropertyName()).getTypeName()
					        .equals(TO.class.getName()))) {
						updateCollectionTOCache(objId, verifiedValue.getPropertyName(),
						        (Collection<TO<?>>) verifiedValue.getValue());
					} else if ((this.getPropertyDescriptor(objId.getTypeName(), verifiedValue.getPropertyName())
					        .getTypeName().equals(PropertyTO.class.getName()))) {
						updateCollectionLinksPropCache(new PropertyTO(objId, verifiedValue.getPropertyName()),
						        (Collection<PropertyTO>) verifiedValue.getValue());
					}
				} catch (ObjectNotFoundException onfe) {
					logger.debug("Cannot obtain property descriptor to update links cache", onfe);
				} catch (PropertyNotFoundException onfe) {
					logger.debug("Cannot obtain property descriptor to update links cache", onfe);
				}
			}

			if (verifiedValue.getValue() instanceof PropertyTO) {
				updateLinksPropsCache(new PropertyTO(objId, verifiedValue.getPropertyName()),
				        (PropertyTO) verifiedValue.getValue());
			}

			updateTimeStamp(objId, changedValue);

			if (logger.isDebugEnabled()) {
				logger.debug("[" + objId + "].[" + verifiedValue.getPropertyName() + "]=[" + verifiedValue.getValue()
				        + "],timestamp=[" + verifiedValue.getTimeStamp() + "]");
			}
		}

		if (notify && changedValues != null)
			eventNotifier.notify(new PropertiesChangedEvent(objId, changedValues.values().toArray(
			        new ChangedValueTO[changedValues.size()])));
	}

	private ChangedValueTO getChangedValue(final TO<?> objId, ValueTO value) {
		Object oldValue;
		try {
			oldValue = this.getPropertyValue(objId, value.getPropertyName(), Object.class);
		} catch (Throwable t) {
			logger.warn("Error while retrieving old value from cache", t);
			oldValue = null;
		}
		return new ChangedValueTO(value, oldValue);
	}

	private Map<String, ChangedValueTO> findChangedValues(final TO<?> objId, final List<ValueTO> verifiedValues) {
		Map<String, ChangedValueTO> changedValues = CollectionUtils.newMap();

		for (ValueTO valueTO : verifiedValues) {
			changedValues.put(valueTO.getPropertyName(), getChangedValue(objId, valueTO));
		}

		return changedValues;
	}

	private List<ValueTO> verifyAndConvert(final ValueTO[] values) {
		List<ValueTO> verifiedValues = new LinkedList<ValueTO>();

		long recordTime = System.currentTimeMillis();

		for (ValueTO valueTO : values) {
			// sets property value with time stamp propagating
			if (DoubleFilter.isSpecial(valueTO.getValue())) {
				logger.warn("The property value (" + valueTO.getPropertyName() + "="
				        + String.valueOf(valueTO.getValue()) + ") cannot be stored in the DB.");
				continue;
			}

			if (valueTO.getTimeStamp() == 0L) {
				valueTO.setTimeStamp(recordTime);
			}

			verifiedValues.add(valueTO);
		}

		return verifiedValues;
	}

	@Override
	public <REQUESTED_TYPE> void setPropertyValue(final TO<?> objId, final String propName, final REQUESTED_TYPE value)
	        throws ObjectNotFoundException, PropertyNotFoundException, UnableToConvertPropertyException {
		if (value != null && value.getClass() == ValueTO.class) {
			setAllPropertiesValues(objId, new ValueTO[] { (ValueTO) value });
		} else {
			ValueTO valueTO = new ValueTO(propName, value, System.currentTimeMillis());
			setAllPropertiesValues(objId, new ValueTO[] { valueTO });
		}
	}

	private void checkAndSetObjectLink(TO<?> link, PropertyTO ref) {
		// add new pointer to dst objects as necessary
		Set<PropertyTO> l = linksOfObjectsCache.get(link);
		if (l == null) {
			if (status == Status.ONLINE) {
				// create copy-on-write set only when cache is online
				// copy-on-write data structure is required to handle concurrent
				// modification exceptions
				l = CollectionUtils.newCopyOnWriteSet();
			} else {
				// if offline - use simple hash set
				l = CollectionUtils.newSet();
			}
			linksOfObjectsCache.put(link, l);
		}

		l.add(ref);
	}

	private void checkAndSetObjectPropertyLink(PropertyTO link, PropertyTO ref) {
		// add new pointer to dst objects as necessary
		Set<PropertyTO> l = linksOfPropertyCache.get(link);
		if (l == null) {
			if (status == Status.ONLINE) {
				// create copy-on-write set only when cache is online
				// copy-on-write data structure is required to handle concurrent
				// modification exceptions
				l = CollectionUtils.newCopyOnWriteSet();
			} else {
				// if offline - use simple hash set
				l = CollectionUtils.newSet();
			}
			linksOfPropertyCache.put(link, l);
		}
		l.add(ref);
	}

	private void updateLinksCache(final TO<?> srcTo, final String propName, final TO<?> dstTo) {

		if (dstTo == null) {
			if (logger.isDebugEnabled()) {
				logger.debug("Null element is in collection property : " + propName + " of object :" + srcTo);
			}
			return;
		}

		PropertyTO srcPointer = new PropertyTO(srcTo, propName);

		// [BC 05/2015] During initialization there are no previous links to remove. This brings down startup time from 833s to 144s
		if (status == Status.ONLINE) {
			// update reverse links removing those objects which are no longer
			// pointed by srcTo
			for (Iterator<Entry<TO<?>, Set<PropertyTO>>> linksIter = linksOfObjectsCache.entrySet().iterator(); linksIter
                    .hasNext();) {
                Entry<TO<?>, Set<PropertyTO>> links = linksIter.next();
                // let's see if current object still should be pointed by srcPointer
                TO<?> dstObj = links.getKey();
                if (!dstTo.equals(dstObj) && links.getValue().contains(srcPointer)) {
                    links.getValue().remove(srcPointer);
                    if (links.getValue().isEmpty())
                        linksIter.remove();
                }
            }
		}

		// add new pointer to dst objects as necessary
		checkAndSetObjectLink(dstTo, srcPointer);
	}

	private void updateCollectionTOCache(final TO<?> srcTo, final String propName,
	        final Collection<TO<?>> dstToCollection) {

		PropertyTO srcPointer = new PropertyTO(srcTo, propName);

		// update reverse links removing those objects which are no longer
		// pointed by srcTo
		for (Iterator<Entry<TO<?>, Set<PropertyTO>>> linksIter = linksOfObjectsCache.entrySet().iterator(); linksIter
		        .hasNext();) {
			Entry<TO<?>, Set<PropertyTO>> links = linksIter.next();
			// let's see if srcPointer points to
			if (links.getValue().contains(srcPointer)) {
				// let's see if dstObj still should be pointed by srcPointer
				TO<?> dstObj = links.getKey();
				if (!dstToCollection.contains(dstObj)) {
					links.getValue().remove(srcPointer);
					if (links.getValue().isEmpty())
						linksIter.remove();
				}
			}
		}

		// add new pointer to dst objects as necessary
		for (TO<?> dstTo : dstToCollection) {
			// if collection contains null element
			if (dstTo == null) {
				if (logger.isDebugEnabled()) {
					logger.debug("Null element is in collection property : " + propName + " of object :" + srcTo);
				}
				continue;
			}
			checkAndSetObjectLink(dstTo, srcPointer);
		}

	}

	private void updateLinksPropsCache(final PropertyTO srcPointer, final PropertyTO dstPointer) {

		// [BC 05/2015] During initialization there are no previous links to remove. This brings down startup time from 833s to 144s
		if (status == Status.ONLINE) {
			// update reverse links removing those properties which are no longer
			// pointed by srcTo
			for (Iterator<Entry<PropertyTO, Set<PropertyTO>>> linksIter = linksOfPropertyCache.entrySet().iterator(); linksIter
                    .hasNext();) {
                Entry<PropertyTO, Set<PropertyTO>> links = linksIter.next();
                // let's see if current property still should be pointed by
                // srcPointer
                PropertyTO dstObj = links.getKey();
                if (!dstPointer.equals(dstObj) && links.getValue().contains(srcPointer)) {
                    links.getValue().remove(srcPointer);
                    if (links.getValue().isEmpty())
                        linksIter.remove();
                }
            }
		}
		// add new pointer to dst objects as necessary
		checkAndSetObjectPropertyLink(dstPointer, srcPointer);
	}

	private void updateCollectionLinksPropCache(final PropertyTO srcPointer,
	        final Collection<PropertyTO> dstToCollection) {

		// update reverse links removing those objects which are no longer
		// pointed by srcTo
		for (Iterator<Entry<PropertyTO, Set<PropertyTO>>> linksIter = linksOfPropertyCache.entrySet().iterator(); linksIter
		        .hasNext();) {
			Entry<PropertyTO, Set<PropertyTO>> links = linksIter.next();
			// let's see if srcPointer points to
			if (links.getValue().contains(srcPointer)) {
				// let's see if dstObj still should be pointed by srcPointer
				PropertyTO dstObj = links.getKey();
				if (!dstToCollection.contains(dstObj)) {
					links.getValue().remove(srcPointer);
					if (links.getValue().isEmpty())
						linksIter.remove();
				}
			}
		}

		// add new pointer to dst objects as necessary
		for (PropertyTO dstPointer : dstToCollection) {
			// if collection contains null element
			if (dstPointer == null) {
				if (logger.isDebugEnabled()) {
					logger.debug("Null element is in collection property : " + srcPointer);
				}
				continue;
			}
			checkAndSetObjectPropertyLink(dstPointer, srcPointer);
		}

	}

	@Override
	public void setRelation(List<Relation> relations) {
		boolean isBulkMode = relations.size() > 1 || relations.iterator().next().getChildren().size() > 1;
		if (isBulkMode) {
			configuring = true;
		}
		try {
			for (Relation r : relations) {
				for (TO<?> child : r.getChildren()) {
					setRelation(r.getParent(), child);
				}
			}
		} finally {
			if (isBulkMode) {
				configuring = false;
				rebaseNumAlerts();
			}
		}
	}

	@Override
	public void setRelation(final TO<?> parentId, final TO<?> childId) {
		SimpleObject parentObject = getObject(parentId);
		SimpleObject childObject = getObject(childId);
		if (parentObject == null) {
			if (logger.isDebugEnabled()) {
				logger.debug("Relation cannot be set because supplied 'parentId' is null");
			}
			return;
		}
		if (childObject == null) {
			if (logger.isDebugEnabled()) {
				logger.debug("Relation cannot be set because supplied 'childId' is null");
			}
			return;
		}

		childObject.getParents().add(new Pair(parentId.getTypeName(), parentId.getID()));
		parentObject.getChildren().add(new Pair(childId.getTypeName(), childId.getID()));

		if (!configuring) {
			alertCounter.rebase(parentId);
		}

		eventNotifier.notify(new RelationSetEvent(parentId, childId));
	}

	@Override
	public void removeRelation(List<Relation> relations) {
		boolean isBulkMode = relations.size() > 1 || relations.iterator().next().getChildren().size() > 1;
		if (isBulkMode) {
			configuring = true;
		}
		try {
			for (Relation r : relations) {
				for (TO<?> child : r.getChildren()) {
					removeRelation(r.getParent(), child);
				}
			}
		} finally {
			if (isBulkMode) {
				configuring = false;
				rebaseNumAlerts();
			}
		}
	}

	@Override
	public void removeRelation(final TO<?> parentId, final TO<?> childId) {
		SimpleObject parentObject = getObject(parentId);
		SimpleObject childObject = getObject(childId);
		if (parentObject == null) {
			if (logger.isDebugEnabled()) {
				logger.debug("Relation cannot be removed because supplied 'parentId' is null");
			}
			return;
		}
		if (childObject == null) {
			if (logger.isDebugEnabled()) {
				logger.debug("Relation cannot be removed because supplied 'childId' is null");
			}
			return;
		}

		parentObject.getChildren().remove(new Pair(childId.getTypeName(), childId.getID()));
		childObject.getParents().remove(new Pair(parentId.getTypeName(), parentId.getID()));

		if (!configuring) {
			alertCounter.rebase(parentId);
		}

		eventNotifier.notify(new RelationRemovedEvent(parentId, childId));
	}

	private void removeTypeFromLinksCache(final String type) {
		for (TO<?> t : linksOfObjectsCache.keySet()) {
			if (type.equalsIgnoreCase(t.getTypeName())) {
				linksOfObjectsCache.remove(t);
			}
		}

		for (Set<PropertyTO> values : linksOfObjectsCache.values()) {
			for (PropertyTO item : values) {
				if (type.equalsIgnoreCase(item.getObjId().getTypeName())) {
					values.remove(item);
				}
			}
		}
	}

	private void removeTypeFromPropertyLinksCache(final String type) {
		for (PropertyTO data : linksOfPropertyCache.keySet()) {
			if (type.equalsIgnoreCase(data.getObjId().getTypeName())) {
				linksOfPropertyCache.remove(data);
			}
		}

		for (Set<PropertyTO> values : linksOfPropertyCache.values()) {
			for (PropertyTO item : values) {
				if (type.equalsIgnoreCase(item.getObjId().getTypeName())) {
					values.remove(item);
				}
			}
		}
	}

	private void removeObjectFromLinksCache(final TO<?> objId) {
		// remove links to this object
		Set<PropertyTO> parents = linksOfObjectsCache.remove(objId);
		// notify parents on collection changes
		long timestamp = System.currentTimeMillis();
		if (parents != null) {
			for (PropertyTO p : parents) {
				SimpleObject so = getObject(p.getObjId());
				if (so != null) {
					so.setPropertyTimestamp(p.getPropertyName(), timestamp);
					try {
						Object val = so.getPropertyValue(p.getPropertyName()).getValue().getValue();
						ChangedValueTO[] cvTOs = new ChangedValueTO[] { new ChangedValueTO(p.getPropertyName(), val,
						        val, timestamp) };
						eventNotifier.notify(new PropertiesChangedEvent(p.getObjId(), cvTOs));
					} catch (PropertyNotFoundException e) {
						if (logger.isDebugEnabled()) {
							logger.debug("Failed to set timestamp", e);
						}
					}
				}
			}
		}

		// remove links from this object
		for (Set<PropertyTO> values : linksOfObjectsCache.values()) {
			for (PropertyTO item : values) {
				if (item.getObjId().equals(objId))
					values.remove(item);
			}
		}
	}

	private void removeObjectFromPropLinkCache(final TO<?> objId) {
		// remove links to this object's properties
		for (PropertyTO tmp : linksOfPropertyCache.keySet()) {
			if (tmp.getObjId().equals(objId)) {
				Set<PropertyTO> parents = linksOfPropertyCache.remove(tmp);
				// notify parents on collection changes
				long timestamp = System.currentTimeMillis();
				if (parents != null) {
					for (PropertyTO p : parents) {
						SimpleObject so = getObject(p.getObjId());
						if (so != null) {
							so.setPropertyTimestamp(p.getPropertyName(), timestamp);
							try {
								Object val = so.getPropertyValue(p.getPropertyName()).getValue().getValue();
								ChangedValueTO[] cvTOs = new ChangedValueTO[] { new ChangedValueTO(p.getPropertyName(),
								        val, val, timestamp) };
								eventNotifier.notify(new PropertiesChangedEvent(p.getObjId(), cvTOs));
							} catch (PropertyNotFoundException e) {
								if (logger.isDebugEnabled()) {
									logger.debug("Failed to set timestamp", e);
								}
							}
						}
					}
				}
			}
		}

		for (Set<PropertyTO> tmp : linksOfPropertyCache.values()) {
			for (PropertyTO p : tmp) {
				if (p.getObjId().equals(objId))
					tmp.remove(p);
			}
		}
	}

	private final SimpleObject getObject(final TO<?> obj) {
		SimpleType st = SimpleType.findType(obj.getTypeName());
		if (st != null)
			return st.getObject((Integer) obj.getID());
		return null;
	}

	private final SimpleType getType(final String typeName) {
		return SimpleType.findType(typeName);
	}

	private final Collection<SimpleType> getTypes(final String typeName, final ObjectTypeMatchMode matchMode) {
		Collection<String> matchTypes = matchMode.filterCollection(SimpleType.enumerateAllTypeNames(), typeName);
		Collection<SimpleType> result = new LinkedList<SimpleType>();
		for (String type : matchTypes) {
			SimpleType st = SimpleType.findType(type);
			if (st != null)
				result.add(st);
		}
		return result;
	}

	@Override
	public Collection<TO<?>> getChildren(final TO<?> objId, final String typeName) throws ObjectNotFoundException {
		SimpleObject so = getObject(objId);
		if (so == null) {
			throw new ObjectNotFoundException(objId);
		}
		// prepares collection of TO
		Collection<TO<?>> objIds = new LinkedList<TO<?>>();
		objIds.add(objId);
		// calls local method
		CollectionTO item = getChildren(objIds, typeName).iterator().next();
		// parses the returned collection
		Collection<TO<?>> result = new LinkedList<TO<?>>();
		for (ValueTO value : item.getPropValues()) {
			result.add((TO<?>) value.getValue());
		}
		return result;
	}

	@Override
	public Collection<CollectionTO> getChildren(final Collection<TO<?>> objIds, final String typeName) {
		Collection<CollectionTO> result = new LinkedList<CollectionTO>();
		for (TO<?> objId : objIds) {
			// new item to return
			CollectionTO item = new CollectionTO(objId);
			// add it to result, whatever
			result.add(item);
			// lookup object in a cache
			SimpleObject so = getObject(objId);
			if (so == null) {
				// CollectionTO item is already in the result , it will not
				// contain properties but must be returned anyway
				continue;
			}
			// get cache object children
			Set<Pair<String, Integer>> children = so.getChildren();
			// filter them by given type
			for (Pair<String, Integer> pair : children) {
				TO<?> object = Util.filterObjectByTypeName(
				        TOFactory.getInstance().loadTO(pair.getFirst() + ":" + pair.getSecond()), typeName);
				if (object == null) {
					continue;
				}
				// new ValueTO with prop.name = TO.getID().toString()
				ValueTO value = new ValueTO(object.getID().toString(), object);
				item.addValue(value);
			}
		}

		return result;
	}

	@Override
	public Collection<TO<?>> getParents(final TO<?> objId, final String typeName) throws ObjectNotFoundException {
		Collection<TO<?>> result = new LinkedList<TO<?>>();
		SimpleObject so = getObject(objId);
		if (so == null)
			throw new ObjectNotFoundException(objId);
		Set<Pair<String, Integer>> children = so.getParents();
		for (Pair<String, Integer> pair : children) {
			result.add(TOFactory.getInstance().loadTO(pair.getFirst() + ":" + pair.getSecond()));
		}
		return Util.filterObjectsByTypeName(result, typeName);
	}

	@Override
	public Collection<TO<?>> getRelatedObjects(final TO<?> objId, final String objectType, final boolean isChild) {
		Collection<TO<?>> result = getRelatedObjects(objId, 0, isChild);

		return Util.filterObjectsByTypeName(result, objectType);
	}

	@Override
	public Collection<TO<?>> getRelatedObjects(final TO<?> objId, final int level, final boolean isChild) {
		Collection<TO<?>> result = new HashSet<TO<?>>();
		if (isChild) {
			result.addAll(getDescendants(objId, level));
		} else {
			result.addAll(getAncestors(objId, level));
		}
		return result;
	}

	private Collection<TO<?>> getAncestors(final TO<?> objId, final int level) {
		Collection<TO<?>> result = new HashSet<TO<?>>();
		// temporary set to store the result of getChildren , getParents methods
		Collection<TO<?>> relatedObjects = new HashSet<TO<?>>();
		try {
			relatedObjects.addAll(this.getParents(objId, null));
		} catch (ObjectNotFoundException e) {
			// if object will be deleted during execution of this function
			if (logger.isDebugEnabled()) {
				logger.debug("Exception in getAncestors(). Seems that object[" + objId
				        + "] has been deleted during method execution");
			}
			return result;
		}

		// stores related objects of input objId
		result.addAll(relatedObjects);

		// additional flag to exit function
		// we don't care if level==-1 because level variable is always
		// decreasing
		if (level == 1)
			return result;

		for (TO to : relatedObjects) {
			// recursive calls by hierarchy
			result.addAll(getAncestors(to, level - 1));
		}

		return result;
	}

	private Collection<TO<?>> getDescendants(final TO<?> objId, final int level) {
		if (logger.isTraceEnabled()) {
			logger.trace("getting descendants of : " + objId + " level:" + level);
		}
		Collection<TO<?>> result = new HashSet<TO<?>>();
		// temporary set to store the result of getChildren , getParents methods
		Collection<TO<?>> relatedObjects = new HashSet<TO<?>>();
		try {
			relatedObjects.addAll(this.getChildren(objId, null));
			if (logger.isTraceEnabled()) {
				logger.trace(objId + " children : " + relatedObjects);
			}
		} catch (ObjectNotFoundException e) {
			// if object will be deleted during execution of this function
			if (logger.isDebugEnabled()) {
				logger.debug("Exception in getDescendants(). Seems that object[" + objId
				        + "] has been deleted during method execution");
			}
			return result;
		}

		// stores related objects of input objId
		result.addAll(relatedObjects);

		// additional flag to exit function
		// we don't care if level==-1 because level variable is always
		// decreasing
		if (level == 1)
			return result;

		for (TO to : relatedObjects) {
			// recursive call by hierarchy
			result.addAll(getDescendants(to, level - 1));
		}

		return result;
	}

	@Override
	public <REQUESTED_TYPE> void setPropertyValue(final String type, final String propName, final REQUESTED_TYPE value)
	        throws ObjectNotFoundException, PropertyNotFoundException, UnableToConvertPropertyException {
		SimpleType st = getType(type);
		if (st == null)
			throw new ObjectNotFoundException(type);

		ValueTO tmpValue = new ValueTO(propName, value);
		st.setPropValue(null, tmpValue);

		// sendMessage(new SetStaticPropertyEvent(type,propName, value));

		if (logger.isDebugEnabled()) {
			logger.debug("Props for [" + type + "] prop name [" + propName + "] [" + value + "] set");
		}
	}

	@Override
	public <REQUESTED_TYPE> REQUESTED_TYPE getPropertyValue(final String type, final String propName,
	        final Class<REQUESTED_TYPE> clazz) throws ObjectNotFoundException, PropertyNotFoundException,
	        UnableToConvertPropertyException {
		SimpleType st = getType(type);

		if (st == null) {
			throw new ObjectNotFoundException(type);
		}
		return st.getValue(null, propName, clazz);

	}

	@Override
	public PropertyDescr getPropertyDescriptor(final String typeName, final String propName)
	        throws ObjectNotFoundException, PropertyNotFoundException {
		SimpleType st = getType(typeName);
		if (st == null) {
			throw new ObjectNotFoundException("There is no type with name : " + typeName);
		}

		// finds property with the specified name
		PropertyDescr propertyDescriptor = st.getEnvType().getPropertyDescriptor(propName);
		// if no such property
		if (propertyDescriptor == null) {
			throw new PropertyNotFoundException("There is no property : " + propName + " of type  : " + typeName);
		}
		// copy state to avoid state leaking
		return new PropertyDescr(propertyDescriptor);
	}

	@Override
	public ObjectType[] getObjectTypes(final String[] names) {
		List<ObjectType> types = new LinkedList<ObjectType>();
		for (String typeName : names) {
			SimpleType st = getType(typeName);
			if (st != null)
				types.add(new EnvObjTypeTO(st.getEnvType()));
		}
		return types.toArray(new ObjectType[0]);
	}

	@Override
	public String[] getObjectTypes() {
		return SimpleType.enumerateAllTypeNames().toArray(new String[0]);
	}

	@Override
	public void runAudit() {
		if (logger.isDebugEnabled()) {
			logger.debug("Starting cache audit at [" + new Date() + "]");
		}

		if (logger.isDebugEnabled()) {
			logger.debug("Cache audit finished");
		}
	}

	private static CacheSvc instance;

	/**
	 * Returns direct reference to itself. it's a dirty hack , need to clarify
	 * this(spring or something else to create container inside jboss) May be
	 * used if very fast access to the cache service is needed. No interceptors,
	 * no transactions, no security,no EJB at all , but ugly implementation )
	 * 
	 * @return Direct reference to cache service
	 */
	public static CacheSvc getInstance() {
		if (instance == null)
			throw new IllegalStateException("Cache service is not created yet");
		return instance;
	}

	/*
	 * private void sendMessage(CacheEvent<?, ?> event){ TopicConnection
	 * connection = null; TopicSession session = null; TopicPublisher publisher
	 * = null;
	 * 
	 * try { connection = topicConnectionFactory.createTopicConnection();
	 * session = connection.createTopicSession(false,
	 * TopicSession.AUTO_ACKNOWLEDGE); publisher =
	 * session.createPublisher(topic); publisher.publish(topic,
	 * session.createObjectMessage(event)); } catch (JMSException e) {
	 * logger.warn(e.getLocalizedMessage(), e); } finally { try { if (publisher
	 * != null) publisher.close(); } catch (JMSException e) {
	 * logger.warn(e.getLocalizedMessage(), e); } try { if (session != null)
	 * session.close(); } catch (JMSException e) {
	 * logger.warn(e.getLocalizedMessage(), e); } try { if (connection != null)
	 * connection.close(); } catch (JMSException e) {
	 * logger.warn(e.getLocalizedMessage(), e); } } }
	 */

	@Override
	public boolean exists(final TO<?> objId) {
		if (objId == null) {
			throw new IllegalInputParameterException("object is null");
		}

		if (objId.getTypeName() == null) {
			throw new IllegalInputParameterException("object Type name is null");
		}

		if (objId.getTypeName().isEmpty()) {
			throw new IllegalInputParameterException("object Type name is empty");
		}
		if (objId.getID() == null) {
			throw new IllegalInputParameterException("object id is null");
		}

		// gets object type by name
		SimpleType type = getType(objId.getTypeName());
		// if no such
		if (type == null) {
			return false;
		}

		return type.exists(objId.getID());
	}

	@Override
	public void rebaseNumAlerts() {
		alertCounter.rebaseAll(env);
	}

	@Override
	public TagDescriptor getTagDescriptor(String typeName, String propName, String tagName)
	        throws ObjectNotFoundException, PropertyNotFoundException, TagNotFoundException {
		PropertyDescr propertyDescriptor = getPropertyDescriptor(typeName, propName);
		propertyDescriptor.getTagDescriptors();

		TagDescriptor tagDescriptor = propertyDescriptor.getTagDescriptor(tagName);

		if (tagDescriptor == null) {
			throw new TagNotFoundException("There is no tag : " + tagName + " of property  : " + propName
			        + " in type : " + typeName);
		}
		return tagDescriptor;
	}

	@Override
	public <RETURN_TYPE> RETURN_TYPE getTagValue(TO<?> objId, String propName, String tagName, Class<RETURN_TYPE> clazz)
	        throws ObjectNotFoundException, PropertyNotFoundException, TagNotFoundException,
	        UnableToConvertTagException {
		SimpleObject so = getObject(objId);
		if (so == null) {
			throw new ObjectNotFoundException(objId.toString());
		}
		return so.getTagValue(propName, tagName, clazz);
	}

	@Override
	public Collection<Tag> getAllTags(TO<?> objId, String propName) throws ObjectNotFoundException,
	        PropertyNotFoundException {
		SimpleObject so = getObject(objId);
		if (so == null) {
			throw new ObjectNotFoundException(objId.toString());
		}

		return so.getAllTags(propName);
	}

	@Override
	public Collection<Tag> getAllTags(TO<?> objId) throws ObjectNotFoundException {
		SimpleObject so = getObject(objId);
		if (so == null) {
			throw new ObjectNotFoundException(objId.toString());
		}

		return so.getAllTags();
	}

	@Override
	public Map<TO<?>, Collection<Tag>> getAllTags(String objectType) {
		Map<TO<?>, Collection<Tag>> objectTags = new HashMap<TO<?>, Collection<Tag>>();
		for (TO<?> obj : getObjectsByType(objectType)) {
			Collection<Tag> tags = null;
			try {
				tags = getAllTags(obj);
			} catch (ObjectNotFoundException onf) {
				continue;
			}
			if (tags != null && tags.size() > 0) {
				objectTags.put(obj, tags);
			}
		}
		return objectTags;
	}

	@Override
	public Map<TO<?>, Map<String, Collection<Tag>>> getTags(Collection<TO<?>> objIds, List<String> propertyNames,
	        List<String> tagNames) {
		Map<TO<?>, Map<String, Collection<Tag>>> allTags = new HashMap<TO<?>, Map<String, Collection<Tag>>>();
		for (TO<?> obj : objIds) {
			SimpleObject so = getObject(obj);
			if (so == null) {
				continue;
			}
			allTags.put(obj, so.getTags(propertyNames, tagNames));
		}
		return allTags;
	}

	@Override
	public void setTagValue(TO<?> objId, String propName, String tagName, Object value)
	        throws PropertyNotFoundException, ObjectNotFoundException, TagNotFoundException,
	        UnableToConvertTagException {
		SimpleObject so = getObject(objId);
		if (so == null) {
			throw new ObjectNotFoundException(objId.toString());
		}

		so.setTagValue(propName, tagName, value);

		if (logger.isDebugEnabled()) {
			logger.debug("Tag value of object [" + objId + "] property [" + propName + "] tagName [" + tagName
			        + "] was set to value [" + value + "]");
		}
	}

	@Override
	public ConfigurationCompleteEvent process(ConfigurationCompleteEvent e) {
		logger.info("Rebasing alert num counter on all objects");
		configuring = false;
		alertCounter.rebaseAll(env);
		return e;
	}

	@Override
	public ConfigurationStartedEvent process(ConfigurationStartedEvent e) {
		configuring = true;
		return e;
	}

	@Override
	public Results executeQuery(QueryDescription queryDescr) {
		if (queryDescr == null) {
			throw new IllegalInputParameterException("Supplied 'queryDescr' is null");
		}
		if (queryDescr.getStartElement() == null) {
			throw new IllegalInputParameterException("Supplied 'queryDescr' is not configured");
		}
		return new QueryImpl(queryDescr).execute(new EnvElement(queryDescr.getStartElement()),
		        new NonRepeatableTraversable(new CacheTraversable(this)));
	}

	Set<PropertyTO> getLinksToObject(TO<?> referenced) {
		Set<PropertyTO> result = CollectionUtils.newSet();
		Set<PropertyTO> linksObjects = linksOfObjectsCache.get(referenced);

		if (linksObjects != null)
			result.addAll(linksObjects);

		SimpleType st = getType(referenced.getTypeName());
		for (PropertyDescr pd : st.getEnvType().getPropertyDescriptors()) {
			if (pd.getType().equals(PropertyTO.class)) {
				Set<PropertyTO> linksProperties = linksOfPropertyCache.get(new PropertyTO(referenced, pd.getName()));
				if (linksProperties != null)
					result.addAll(linksProperties);
			}
		}

		return Collections.unmodifiableSet(result);
	}
}
