package com.synapsense.dal.generic.batch;

public class ModelUpdateException extends RuntimeException {

	private static final long serialVersionUID = 746517315444132085L;

	public ModelUpdateException(String message, Throwable e) {
		super(message, e);
	}

}
