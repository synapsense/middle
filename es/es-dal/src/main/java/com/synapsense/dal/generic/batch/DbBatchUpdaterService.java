package com.synapsense.dal.generic.batch;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.DependsOn;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.sql.DataSource;
import javax.transaction.UserTransaction;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.cfg.NamingStrategy;
import org.hibernate.cfg.ObjectNameNormalizer;
import org.hibernate.dialect.MySQL5Dialect;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.id.IdentifierGenerator;
import org.hibernate.id.enhanced.SequenceStyleGenerator;
import org.hibernate.type.IntegerType;

import com.synapsense.cdi.MBean;
import com.synapsense.commandprocessor.CommandEngineFactory;
import com.synapsense.commandprocessor.Commands;
import com.synapsense.commandprocessor.api.Command;
import com.synapsense.commandprocessor.api.CommandEngine;
import com.synapsense.dal.generic.CommonDAO;
import com.synapsense.dal.generic.batch.HashedBlockingQueue.Predicate;
import com.synapsense.dal.generic.batch.loader.HistoricalDataLoader;
import com.synapsense.dal.generic.batch.loader.HistoricalDataRowFormatter;
import com.synapsense.dal.generic.batch.loader.ScriptellaStore;
import com.synapsense.dal.generic.batch.loader.Store;
import com.synapsense.dal.generic.utils.QueryFactoryConfiguration;
import com.synapsense.dto.ChangedValueTO;
import com.synapsense.dto.PropertyDescr;
import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;
import com.synapsense.service.nsvc.NotificationService;
import com.synapsense.service.nsvc.events.ConfigurationCompleteEvent;
import com.synapsense.service.nsvc.events.ConfigurationCompleteEventProcessor;
import com.synapsense.service.nsvc.events.ConfigurationStartedEvent;
import com.synapsense.service.nsvc.events.ConfigurationStartedEventProcessor;
import com.synapsense.service.nsvc.events.DispatchingProcessor;
import com.synapsense.service.nsvc.events.ObjectCreatedEvent;
import com.synapsense.service.nsvc.events.ObjectCreatedEventProcessor;
import com.synapsense.service.nsvc.events.ObjectDeletedEvent;
import com.synapsense.service.nsvc.events.ObjectDeletedEventProcessor;
import com.synapsense.service.nsvc.events.ObjectTypeCreatedEvent;
import com.synapsense.service.nsvc.events.ObjectTypeCreatedEventProcessor;
import com.synapsense.service.nsvc.events.ObjectTypeDeletedEvent;
import com.synapsense.service.nsvc.events.ObjectTypeDeletedEventProcessor;
import com.synapsense.service.nsvc.events.ObjectTypeUpdatedEvent;
import com.synapsense.service.nsvc.events.ObjectTypeUpdatedEventProcessor;
import com.synapsense.service.nsvc.events.PropertiesChangedEvent;
import com.synapsense.service.nsvc.events.PropertiesChangedEventProcessor;
import com.synapsense.util.CollectionUtils;
import com.synapsense.util.Pair;
import com.synapsense.util.executor.Executor;
import com.synapsense.util.executor.Executors;

/**
 * Database batch update service.<br>
 * Works as Notification service subscriber.<br>
 * Collects incoming notifications about changed property values and stores them
 * in batch mode.
 * 
 * @author ashabanov
 * 
 */
@Singleton
@MBean("com.synapsense:type=DbBatchUpdaterService")
@ConcurrencyManagement(ConcurrencyManagementType.BEAN)
@Startup @DependsOn({"Registrar", "LocalNotificationService"})
public class DbBatchUpdaterService implements DbBatchUpdaterServiceMBean, PropertiesChangedEventProcessor,
        ObjectTypeCreatedEventProcessor, ObjectTypeUpdatedEventProcessor, ObjectTypeDeletedEventProcessor,
        ObjectCreatedEventProcessor, ObjectDeletedEventProcessor, ConfigurationStartedEventProcessor,
        ConfigurationCompleteEventProcessor {

	static final Logger logger = Logger.getLogger(DbBatchUpdaterService.class);

	/**
	 * "is service started" flag
	 */
	private static volatile boolean isStarted = false;

	/**
	 * "is service created" flag
	 */
	private static volatile boolean isCreated = false;

	/**
	 * "is service in configuring mode" flag
	 */
	private static volatile boolean isConfiguring = false;

	/**
	 * queue reentrant lock
	 */
	private static final Lock queueLock = new ReentrantLock();

	/**
	 * "empty queue" condition
	 */
	private static final Condition queueIsEmpty = queueLock.newCondition();

	private DispatchingProcessor dispProcessor;

	/**
	 * internal scheduler, safe alternative to manually created Thread
	 */
	private ExecutorService executorService;

	// ********************************************************************
	// EJB section
	/**
	 * to access dal layer
	 */
	private CommonDAO commonDao;
	
	@Resource
	private UserTransaction userTransaction;

	@Resource(mappedName = "java:/SynapDS", type = DataSource.class)
	private DataSource datasource;

	@PersistenceContext(unitName = "HibernateDAL")
	public void setEM(final EntityManager em) {
		this.em = em;
	}

	private EntityManager em;

	private NotificationService notificationService;

	@EJB
	public void setCommonDao(CommonDAO commonDao) {
		this.commonDao = commonDao;
	}

	@EJB(beanName="LocalNotificationService")
	public void setNotificationService(NotificationService notificationService) {
		this.notificationService = notificationService;
	}

	public NotificationService getNotificationService() {
		return notificationService;
	}

	// ********************************************************************

	/**
	 * very simple internal storage for incoming events
	 */
	private HashedBlockingQueue queue;

	/**
	 * tasks executor
	 */
	private Executor executor;

	/**
	 * internal timer to run save tasks
	 */
	private CommandEngine engine;

	private GenericModelUpdater mUpdater;

	private volatile ValueCache valueMapping;

	private volatile AttributeCache attrMapping;

	private IdentifierGenerator historicalGroupIdGenerator;

	@Override
	@PostConstruct
	public void create() throws Exception {
		if (isCreated)
			return;
		// new storage for incoming events
		queue = new HashedBlockingQueue(DbBatchServiceConfiguration.QUEUE_SIZE);

		isCreated = true;
		logger.info("DbBatchUpdaterService is created");
		start();
	}

	@Override
	public void destroy() throws Exception {
		if (!isCreated) {
			logger.warn("The service is not created yet!");
			return;
		}
		if (isStarted) {
			logger.warn("The service is not stopped!Stop it before destroying");
			return;
		}

		executor = null;

		queue = null;

		engine = null;

		logger.info("DbBatchUpdaterService is destroyed");

		isCreated = false;
	}

	@Override
	@PreDestroy
	public void stop() throws Exception {
		logger.info("Stopping DbBatchUpdaterService...");

		if (!isStarted && !isConfiguring) {
			logger.warn("The service is not started yet!");
			return;
		}
		// shut up incoming notifications
		isStarted = false;

		// not a subscriber now
		notificationService.deregisterProcessor(dispProcessor);
		dispProcessor = null;

		// waiting for threads to poll all notifications from the queue
		queueLock.lock();
		try {
			while (!queue.isEmpty()) {
				logger.debug("Waiting for queue to empty... queue size=" + queue.size());
				// will wait signal for 1 sec until the queue is not empty or
				// till queueIsEmpty be signaled.
				// the time limit is for the case when all scheduled tasks are
				// done by execution pool and no one can signal about queue
				// state.
				// so we need to limit wait time to check queue one more time in
				// the loop
				queueIsEmpty.await(1, TimeUnit.SECONDS);
			}
		} finally {
			queueLock.unlock();
		}

		// wait for perform all pooled notifications
		executor.shutdownAwait();

		// stops flush task timer
		engine.shutdown();

		// safe shutdown 
		executorService.shutdownNow();
		// then wait all tasks normal completion
		while (!executorService.isTerminated()) {
			logger.debug("Awaiting termination of working thread...");
			executorService.awaitTermination(5, TimeUnit.SECONDS);
		}

		valueMapping.clear();
		attrMapping.clear();

		logger.info("DbBatchUpdaterService is stopped");
		destroy();
	}

	@Override
	public void start() throws Exception {
		if (!isCreated) {
			logger.warn("The service is not created yet!");
			return;
		}

		if (isStarted) {
			logger.warn("The service is already started!");
			return;
		}

		logger.info("Starting DbBatchUpdaterService...");

		// new execution pool
		executor = Executors.newPooledExecutor("Batch updater task pool threads["
		        + DbBatchServiceConfiguration.THREADS_NUMBER + "]", DbBatchServiceConfiguration.THREADS_NUMBER);
		// new command engine
		engine = CommandEngineFactory.newEngine(DbBatchServiceConfiguration.SERVICE_NAME + " timer");

		// configures query factory properties.
		// moved here from registrar
		QueryFactoryConfiguration.configure(System.getProperties());

		HistoricalDataRowFormatter rowFormatter = new HistoricalDataRowFormatter(
		        DbBatchServiceConfiguration.DECIMAL_FORMAT, DbBatchServiceConfiguration.TIMESTAMP_FORMAT,
		        DbBatchServiceConfiguration.DECIMAL_SEPARATOR, DbBatchServiceConfiguration.NULL_VALUE_REPLACER);

		String ROOT_DIR = System.getProperty("com.synapsense.filestorage");
		if(ROOT_DIR==null)
			throw new Exception("com.synapsense.filestorage property not set");

		String script = ROOT_DIR + "/" + "dbupdater.xml";
		Store store = new ScriptellaStore(script);

		String bufferFile = ROOT_DIR + "/" + "buffer.csv";
		// consider appropriate buffer size if needed(if 0 then each call of
		// load() method will persist data to database)
		HistoricalDataLoader csvLoader = new HistoricalDataLoader(bufferFile, rowFormatter, 0, store);

		loadValueIdMapping();
		loadAttributeMapping();
		// pass this mapping to model updater
		mUpdater = new GenericModelUpdater(csvLoader, valueMapping, attrMapping, datasource, em);

		// new flush task timer
		Command pollQueue = CommandFactory.pollQueue(engine, executor, userTransaction, queue, mUpdater,
		        DbBatchServiceConfiguration.BATCH_SIZE, DbBatchServiceConfiguration.THREADS_NUMBER, queueLock,
		        queueIsEmpty, DbBatchServiceConfiguration.FLUSH_CHANGES_DELAY);

		// scheduling new task on timer
		engine.addCommand(Commands.makeLoop(pollQueue, Commands.noop(), engine));

		// subscribe to events
		dispProcessor = new DispatchingProcessor().putPcep(this).putOcep(this).putOdep(this).putOtcep(this)
		        .putOtdep(this).putOtuep(this).putCcep(this).putCsep(this);
		notificationService.registerProcessor(dispProcessor, null);

		historicalGroupIdGenerator = initHistoricalGroupIdGenerator();
		
		executorService = java.util.concurrent.Executors.newSingleThreadExecutor();
		executorService.execute(engine);

		logger.info("DbBatchUpdaterService is started with config:\n" + DbBatchServiceConfiguration.getInfo());
		isStarted = true;
	}

	@Override
	public PropertiesChangedEvent process(PropertiesChangedEvent e) {
		if (!isStarted && !isConfiguring)
			return e;

		Integer ID = 0; // [BC 05/2016] groupId defaults to 0, because it's part of a unique index.

		// FIXME : just proof of concept
		// required refactoring for this code
		if ("ALERT_HISTORY".equals(e.getObjectTO().getTypeName())) {
			Session hibernateSession = (Session) em.getDelegate();
			// assing group id
			ID = (Integer) historicalGroupIdGenerator.generate((SessionImplementor) hibernateSession, null);
			logger.debug("Generated next group_id [" + ID + "]  for alert " + e.getObjectTO());
		}

		// new elements should be added to tail
		for (ChangedValueTO cvTO : e.getChangedValues()) {
			if (cvTO == null) {
				if (logger.isDebugEnabled()) {
					logger.debug("Property change event on object : " + e.getObjectTO() + " contains null "
					        + ChangedValueTO.class + " instance");
				}
				continue;
			}

			if (cvTO.getValue() == null) {
				if (logger.isDebugEnabled()) {
					logger.debug("Property change event on object : " + e.getObjectTO()
					        + " contains null referenced value of property : " + cvTO.getPropertyName());
				}
			}

			// FIXME required to use advanced filtering of NVC service
			if ("numAlerts".equals(cvTO.getPropertyName())) {
				continue;
			}

			boolean timestampOnly = false;

			if (cvTO.getValue() instanceof Collection) {
				Set<Object> newValue = CollectionUtils.newSet();
				for (Object elem : Collection.class.cast(cvTO.getValue())) {
					if (elem != null)
						newValue.add(elem);
				}

				Set<Object> oldValue = CollectionUtils.newSet();
				if (cvTO.getOldValue() != null) {
					for (Object elem : Collection.class.cast(cvTO.getOldValue())) {
						if (elem != null)
							oldValue.add(elem);
					}
				}

				cvTO.setValue(newValue);

				timestampOnly = newValue.equals(oldValue);
			}

			logger.debug("Property change event for " + e.getObjectTO() + ": " + cvTO);
			queue.offer(e.getObjectTO(), cvTO.getPropertyName(), cvTO, timestampOnly, ID);
		}

		return e;
	}

	@Override
	public ObjectTypeDeletedEvent process(final ObjectTypeDeletedEvent e) {
		if (!isStarted && !isConfiguring)
			return e;

		// escalate locking
		queue.lock();
		try {
			queue.clear(new Predicate<SingleUpdateUnit>() {
				@Override
				public boolean eval(SingleUpdateUnit value) {
					return value.getTo().getTypeName().equals(e.getTypeName());
				}
			});
			loadAttributeMapping();
		} finally {
			queue.unlock();
		}

		return e;
	}


	@Override
	public ObjectTypeUpdatedEvent process(ObjectTypeUpdatedEvent e) {
		if (!isStarted && !isConfiguring)
			return e;

		// escalate locking
		queue.lock();
		try {
			String typeName = e.getTypeName();

			for (final String propName : e.getRemovedProperties()) {
				attrMapping.remove(Pair.newPair(typeName, propName));
				queue.clear(new Predicate<SingleUpdateUnit>() {
					@Override
					public boolean eval(SingleUpdateUnit value) {
						return value.getPropertyName().equals(propName);
					}
				});
			}

			for (PropertyDescr pd : e.getAddedProperties()) {
				Integer attrId = getAttributeIdFromDB(typeName, pd.getName());
				if (attrId == null) {
					logger.warn("No such attribute in database {type=" + typeName + " ,attribute=" + pd.getName()
					        + "}");
					continue;
				}

				AttributeTuple newTuple = new AttributeTuple(attrId, pd.isHistorical(), pd.getTypeName());
				attrMapping.put(Pair.newPair(typeName, pd.getName()), newTuple);
			}

		} finally {
			queue.unlock();
		}

		return e;
	}

	@Override
	public ObjectTypeCreatedEvent process(ObjectTypeCreatedEvent e) {
		if (!isStarted && !isConfiguring)
			return e;

		process(new ObjectTypeUpdatedEvent(e.getObjectType().getName(), e.getObjectType().getPropertyDescriptors(),
		        Collections.<String> emptySet()));
		return e;
	}

	@Override
	public ObjectDeletedEvent process(final ObjectDeletedEvent e) {
		if (!isStarted && !isConfiguring)
			return e;
		
		final TO<?> justRemovedObject = e.getObjectTO();
		
		// firstly clear working queue
		queue.clear(new Predicate<SingleUpdateUnit>() {
			@Override
			public boolean eval(SingleUpdateUnit value) {
				return value.getTo().equals(justRemovedObject);
			}
		});
		
		// then all value mappings for this object
		valueMapping.remove(new CollectionUtils.Predicate<Pair<Integer, String>>() {
			@Override
			public boolean evaluate(Pair<Integer, String> t) {
				return t.getFirst().equals(justRemovedObject.getID());
			}
		});
		
		return e;
	}

	@Override
	public ObjectCreatedEvent process(ObjectCreatedEvent e) {
		if (!isStarted || isConfiguring)
			return e;

		TO<?> to = e.getObjectTO();
		Integer objId = (Integer) to.getID();
		ValueTO[] values = e.getValues();
		for (ValueTO vto : values) {
			ValueTuple vt = getValueTupleFromDB(objId, to.getTypeName(), vto.getPropertyName());
			if (vt == null) {
				logger.warn("ObjectCreatedEvent: cannot update value mapping." + to + "'s property "
				        + vto.getPropertyName() + " is not yet initialized");
				continue;
			}

			// public changes in mapping before new working queue item has been taken to perform
			queue.lock();
			try {
				valueMapping.put(Pair.newPair(objId, vto.getPropertyName()), vt);
			} finally {
				queue.unlock();
			}
		}

		return e;
	}

	@Override
	public String showQueueInfo() {
		StringBuilder sb = new StringBuilder(1000);
		int i = 1;
		for (SingleUpdateUnit element : queue) {
			sb.append(i++ + ". " + element + "\n");
		}
		return sb.toString();
	}

	@Override
	public int showQueueSize() {
		return queue.size();
	}

	@Override
	public String showSettings() {
		return DbBatchServiceConfiguration.getInfo();
	}

	@Override
	public void setBatchSize(int newSize) {
		if (newSize <= 0)
			throw new IllegalArgumentException();
		try {
			stop();
			DbBatchServiceConfiguration.BATCH_SIZE = newSize;
			start();
		} catch (Exception e) {
			logger.error("Error when configuring service.Configuration is not updated due to :" + e.getMessage(), e);
			throw new RuntimeException(e);
		}

	}

	@Override
	public void setFlushDelay(long newDelay) {
		if (newDelay <= 1000)
			throw new IllegalArgumentException();
		try {
			stop();
			DbBatchServiceConfiguration.FLUSH_CHANGES_DELAY = newDelay;
			start();
		} catch (Exception e) {
			logger.error("Error when configuring service.Configuration is not updated due to :" + e.getMessage(), e);
			throw new RuntimeException(e);
		}

	}

	@Override
	public void setQueueSize(int newSize) {
		if (newSize < 1)
			throw new IllegalArgumentException();
		try {
			stop();
		} catch (Exception e) {
			logger.error("Error when configuring service.Configuration was not updated due to :" + e.getMessage(), e);
			throw new RuntimeException(e);
		}

		synchronized (this) {
			DbBatchServiceConfiguration.QUEUE_SIZE = newSize;

			if (queue.isEmpty()) {
				queue = new HashedBlockingQueue(newSize);
			} else {
				HashedBlockingQueue tempQueue = new HashedBlockingQueue(newSize);
				tempQueue.rewriteBy(queue);
				queue = tempQueue;
			}
		}

		try {
			start();
		} catch (Exception e) {
			logger.error("Error when configuring service.Configuration was not updated due to :" + e.getMessage(), e);
			throw new RuntimeException(e);
		}
	}

	@Override
	public void setThreadsNumber(int newThreadsNumber) {
		throw new UnsupportedOperationException(
		        "Because of deadlocks. Blocked till new algorithm of queue distributing is done");
	}

	private void loadAttributeMapping() {
		if (this.attrMapping == null)
			this.attrMapping = new AttributeCache();
		else
			this.attrMapping.clear();
		
		Collection<Object[]> rowSet = commonDao.findRowSetByNamedQuery("attribute.name.to.id.mapping",
		        new HashMap<String, Object>());
		for (Object[] objects : rowSet) {
			String typeName = (String) objects[0];
			String attrName = (String) objects[1];
			int attrId = (Integer) objects[2];
			boolean historical = (Boolean) objects[3];
			String type = (String) objects[4];

			this.attrMapping.put(Pair.newPair(typeName, attrName), new AttributeTuple(attrId, historical, type));
		}
	}

	private void loadValueIdMapping() {
		if (this.valueMapping==null) 
			this.valueMapping = new ValueCache();
		else 
			this.valueMapping.clear();
		
		Collection<Object[]> rowSet = commonDao.findRowSetByNamedQuery("value.to.object.attribute.mapping",
		        new HashMap<String, Object>());

		for (Object[] objects : rowSet) {
			int objId = (Integer) objects[0];
			String attrName = (String) objects[1];
			int attrId = (Integer) objects[2];
			int valueId = (Integer) objects[3];
			boolean historical = (Boolean) objects[4];

			this.valueMapping.put(Pair.newPair(objId, attrName), new ValueTuple(attrId, valueId, historical));
		}
	}

	private Integer getAttributeIdFromDB(String typeName, String attrName) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("typeName", typeName);
		params.put("attrName", attrName);

		Collection<Integer> rowSet = commonDao.findByNamedQuery("attribute.id.by.type.and.name", params, Integer.class);
		if (rowSet.isEmpty()) {
			return null;
		}

		if (rowSet.size() > 1) {
			return null;
		}

		return rowSet.iterator().next();
	}

	private ValueTuple getValueTupleFromDB(Integer objId, String typeName, String attrName) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("typeName", typeName);
		params.put("attrName", attrName);
		params.put("objId", objId);

		Collection<Object[]> rowSet = commonDao
		        .findRowSetByNamedQuery("value.tuple.by.typeName.attrName.objId", params);
		if (rowSet.isEmpty()) {
			return null;
		}

		if (rowSet.size() > 1) {
			return null;
		}

		Object[] row = rowSet.iterator().next();

		ValueTuple vt = new ValueTuple((Integer) row[1], (Integer) row[0], (Boolean) row[2]);
		return vt;
	}

	@Override
	public String showValueMappingFor(int objId, String propName) {
		return valueMapping.get(Pair.newPair(objId, propName)).toString();
	}

	@Override
	public String showAttributeMappingFor(String typeName, String propName) {
		return attrMapping.get(Pair.newPair(typeName, propName)).toString();
	}

	@Override
	public String showStatistic() {
		return mUpdater.getStatistics().toString();
	}

	@Override
	public void startCollectStatistics() {
		mUpdater.collectStatistics();
	}

	@Override
	public ConfigurationCompleteEvent process(ConfigurationCompleteEvent e) {
		if (!isConfiguring)
			return e;

		logger.debug("Configuration complete event has been recieved");

		Command reloadMappings = new Command() {
			@Override
			public void execute() {
				logger.debug("Reloading mappings....");
				loadAttributeMapping();
				loadValueIdMapping();
				logger.debug("Mappings were reloaded successfully");
				isStarted = true;
				isConfiguring = false;
			}
		};

		// just adding new command into main thread execution queue which pauses
		// all jobs
		// until all mappings will be reloaded
		engine.addCommand(reloadMappings);

		return e;
	}

	@Override
	public ConfigurationStartedEvent process(ConfigurationStartedEvent e) {
		// mark service as in "configuring" state
		if (isConfiguring)
			return e;
		logger.debug("Starting configration process. Service will be unavailable till process complete");

		isConfiguring = true;
		isStarted = false;

		return e;
	}

	private IdentifierGenerator initHistoricalGroupIdGenerator() {
		SequenceStyleGenerator sg = new SequenceStyleGenerator();
		Properties params = new Properties();
		params.put(SequenceStyleGenerator.IDENTIFIER_NORMALIZER,
				new ObjectNameNormalizer() {
					
					@Override
					protected boolean isUseQuotedIdentifiersGlobally() {
						// TODO Auto-generated method stub
						return false;
					}
					
					@Override
					protected NamingStrategy getNamingStrategy() {
						// TODO Auto-generated method stub
						return null;
					}
				});
		params.setProperty(SequenceStyleGenerator.SEQUENCE_PARAM, "uid_table");
		params.setProperty(SequenceStyleGenerator.VALUE_COLUMN_PARAM, "group_id");
		params.setProperty(SequenceStyleGenerator.INCREMENT_PARAM, "50");
		params.setProperty("optimizer", "pooled");

		sg.configure(IntegerType.INSTANCE, params, new MySQL5Dialect());

		return sg;
	}

}
