package com.synapsense.dal.generic.batch.loader;

import java.io.File;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import scriptella.execution.EtlExecutor;
import scriptella.execution.EtlExecutorException;
import scriptella.util.IOUtils;

public class ScriptellaStore implements Store {
	private static final Logger logger = Logger.getLogger(ScriptellaStore.class);

	private Map<String, Object> scriptellaParameters = new HashMap<String, Object>();
	private String script;

	public ScriptellaStore(String script) {
		this.script = script;
	}

	@Override
	public void persist(Resource resource) {
		this.scriptellaParameters.put("file.buffer", resource.getName());

		EtlExecutor etlExec;
		try {
			etlExec = EtlExecutor.newExecutor(IOUtils.toUrl(new File(script)), scriptellaParameters);
			etlExec.execute();
		} catch (MalformedURLException e) {
			logger.warn("url " + script + " to etl file is not specified properly", e);
		} catch (EtlExecutorException e) {
			logger.warn("cannot load historical data due to", e);
		}
	}
}
