package com.synapsense.dal.generic.utils;

import java.io.File;
import java.io.Serializable;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projection;
import org.hibernate.stat.Statistics;

/**
 * This class is responsible for accessing to database
 * 
 * @author Alexander Borisov
 * 
 */
public class DBInstance {
	static private SessionFactory sessions;

	static private DBInstance instance = null;

	static {
		Configuration c = new Configuration();
		try {
			c.configure(new File("conf\\hibernate.cfg.xml"));
			c.addDirectory(new File(
			        "D:\\git\\synapsense3\\SynapServ\\HibernateDAL\\src\\com\\synapsense\\dal\\generic\\entities"));
			configure(c);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static Statistics getStatistics() {
		return sessions.getStatistics();
	}

	static public void configure(Configuration c) {
		if (c != null) {
			sessions = c.buildSessionFactory();
			instance = new DBInstance();
		}
	}

	protected DBInstance() {

	}

	public static Session newSession() {
		return sessions.openSession();
	}

	public static DBInstance getInstance() {
		return instance;
	}

	protected Session getSessionFromPool() {
		return sessions.getCurrentSession();
	}

	protected void returnSessionToPool(Session s) {
		if (s.isOpen() == true)
			s.close();
	}

	/**
	 * Executes a query and returns query result
	 * 
	 * @param query
	 *            Query to execute
	 * @return result of query execution
	 * @throws HibernateException
	 */
	public List<?> executeQuery(String query, boolean hasresult) throws HibernateException {
		Session s = getSessionFromPool();
		Transaction t = null;
		List<?> list = null;

		try {
			t = s.beginTransaction();
			Query q = s.createQuery(query);
			q.setCacheable(true);
			if (hasresult == true)
				list = (List<?>) q.list();
			else {
				q.executeUpdate();
			}
			t.commit();
		} catch (HibernateException e) {
			t.rollback();
			throw e;
		} finally {
			returnSessionToPool(s);
		}
		return list;

	}

	/**
	 * Verifies status of SQL server by executing an SQL query
	 * 
	 * @param query
	 *            query to execute
	 * @throws HibernateException
	 *             in case of any SQL server error
	 */
	public void verify(String query) throws HibernateException {
		Session s = getSessionFromPool();
		Transaction t = null;
		try {
			t = s.beginTransaction();
			s.createSQLQuery(query).list();
			t.commit();
		} finally {
			returnSessionToPool(s);
		}
	}

	/**
	 * Gets list of data by specified criteria
	 * 
	 * @param clazz
	 *            class
	 * @param citeria
	 *            map of criteria
	 * @return list of data
	 * @throws HibernateException
	 */
	@SuppressWarnings("unchecked")
	public List<? extends Serializable> getCriterialData(Class<?> clazz, Collection<? extends Criterion> citeria)
	        throws HibernateException {
		Session s = getSessionFromPool();
		Transaction t = null;
		List<? extends Serializable> list = null;
		try {
			t = s.beginTransaction();
			Criteria c = s.createCriteria(clazz);
			if (citeria != null) {
				for (Criterion r : citeria) {
					c = c.add(r);
				}
			}
			list = (List<? extends Serializable>) c.list();
			t.commit();
		} catch (HibernateException e) {
			t.rollback();
			throw e;
		} finally {
			returnSessionToPool(s);
		}
		return list;
	}

	/**
	 * Gets list of data by specified criteria
	 * 
	 * @param clazz
	 *            class
	 * @param citeria
	 *            map of criteria
	 * @param maxSize
	 *            max result size
	 * @return list of data
	 * @throws HibernateException
	 */
	@SuppressWarnings("unchecked")
	public List<? extends Serializable> getCriterialData(Class<?> clazz, Collection<? extends Criterion> citeria,
	        int maxSize) throws HibernateException {
		Session s = getSessionFromPool();
		Transaction t = null;
		List<? extends Serializable> list = null;
		try {
			t = s.beginTransaction();
			Criteria c = s.createCriteria(clazz);
			if (citeria != null) {
				for (Criterion r : citeria) {
					c = c.add(r);
				}
			}
			list = (List<? extends Serializable>) c.setMaxResults(maxSize).list();
			t.commit();
		} catch (HibernateException e) {
			t.rollback();
			throw e;
		} finally {
			returnSessionToPool(s);
		}
		return list;
	}

	/**
	 * Returns list of data by specified criteria and order
	 * 
	 * @param clazz
	 *            class
	 * @param citeria
	 *            collection of criteria
	 * @return list of data by specified criteria and order
	 * @throws HibernateException
	 */
	@SuppressWarnings("unchecked")
	public List<? extends Serializable> getCriterialData(Class<?> clazz, Collection<? extends Criterion> citeria,
	        Order order) throws HibernateException {
		Session s = getSessionFromPool();
		Transaction t = null;
		List<? extends Serializable> list = null;
		try {
			t = s.beginTransaction();
			Criteria c = s.createCriteria(clazz);
			if (citeria != null) {
				for (Criterion r : citeria) {
					c = c.add(r);
				}
			}
			if (order != null) {
				c.addOrder(order);
			}
			list = (List<? extends Serializable>) c.list();
			t.commit();
		} catch (HibernateException e) {
			t.rollback();
			throw e;
		} finally {
			returnSessionToPool(s);
		}
		return list;
	}

	/**
	 * Returns list of data by specified criteria and order
	 * 
	 * @param clazz
	 * @param citeria
	 * @param order
	 * @param maxsize
	 * @return
	 * @throws HibernateException
	 */
	@SuppressWarnings("unchecked")
	public List<? extends Serializable> getCriterialData(Class<?> clazz, Collection<? extends Criterion> citeria,
	        Order order, int maxsize) throws HibernateException {
		Session s = getSessionFromPool();
		Transaction t = null;
		List<? extends Serializable> list = null;
		try {
			t = s.beginTransaction();
			Criteria c = s.createCriteria(clazz);
			if (citeria != null) {
				for (Criterion r : citeria) {
					c = c.add(r);
				}
			}
			if (order != null) {
				c.addOrder(order);
			}
			list = (List<? extends Serializable>) c.setMaxResults(maxsize).list();
			t.commit();
		} catch (HibernateException e) {
			t.rollback();
			throw e;
		} finally {
			returnSessionToPool(s);
		}
		return list;
	}

	/**
	 * Returns list of data by specified criteria and projection
	 * 
	 * @param clazz
	 *            class
	 * @param citeria
	 *            collection of criteria
	 * @param projection
	 *            projection
	 * @return list of data by specified criteria and projection
	 * @throws HibernateException
	 */
	@SuppressWarnings("unchecked")
	public List<? extends Serializable> getCriterialData(Class<? extends Serializable> clazz,
	        Collection<Criterion> citeria, Projection projection) throws HibernateException {
		Session s = getSessionFromPool();
		Transaction t = null;
		List<? extends Serializable> list = null;
		try {
			t = s.beginTransaction();
			Criteria c = s.createCriteria(clazz);
			if (citeria != null) {
				for (Criterion r : citeria) {
					c = c.add(r);
				}
			}
			if (projection != null) {
				c.setProjection(projection);
			}
			list = (List<? extends Serializable>) c.list();
			t.commit();
		} catch (HibernateException e) {
			t.rollback();
			throw e;
		} finally {
			returnSessionToPool(s);
		}
		return list;
	}

	/**
	 * Returns list of data by specified criteria and projection
	 * 
	 * @param clazz
	 * @param citeria
	 * @param projection
	 * @param maxSize
	 * @return
	 * @throws HibernateException
	 */
	@SuppressWarnings("unchecked")
	public List<? extends Serializable> getCriterialData(Class<? extends Serializable> clazz,
	        Collection<Criterion> citeria, Projection projection, int maxSize) throws HibernateException {
		Session s = getSessionFromPool();
		Transaction t = null;
		List<? extends Serializable> list = null;
		try {
			t = s.beginTransaction();
			Criteria c = s.createCriteria(clazz);
			if (citeria != null) {
				for (Criterion r : citeria) {
					c = c.add(r);
				}
			}
			if (projection != null) {
				c.setProjection(projection);
			}
			list = (List<? extends Serializable>) c.setMaxResults(maxSize).list();
			t.commit();
		} catch (HibernateException e) {
			t.rollback();
			throw e;
		} finally {
			returnSessionToPool(s);
		}
		return list;
	}

	/**
	 * Returns list of data by specified projection
	 * 
	 * @param clazz
	 *            class
	 * @param projection
	 *            projection
	 * @return list of data by specified projection
	 * @throws HibernateException
	 */
	public List<?> getCriterialData(Class<? extends Serializable> clazz, Projection projection)
	        throws HibernateException {
		Session s = getSessionFromPool();
		Transaction t = null;
		List<?> list = null;
		try {
			t = s.beginTransaction();
			Criteria c = s.createCriteria(clazz);
			if (projection != null) {
				c.setProjection(projection);
			}
			list = (List<?>) c.list();
			t.commit();
		} catch (HibernateException e) {
			t.rollback();
			throw e;
		} finally {
			returnSessionToPool(s);
		}
		return list;
	}

	/**
	 * 
	 * @param clazz
	 * @param projection
	 * @param maxSize
	 * @return
	 * @throws HibernateException
	 */
	public List<?> getCriterialData(Class<? extends Serializable> clazz, Projection projection, int maxSize)
	        throws HibernateException {
		Session s = getSessionFromPool();
		Transaction t = null;
		List<?> list = null;
		try {
			t = s.beginTransaction();
			Criteria c = s.createCriteria(clazz);
			if (projection != null) {
				c.setProjection(projection);
			}
			list = (List<?>) c.setMaxResults(maxSize).list();
			t.commit();
		} catch (HibernateException e) {
			t.rollback();
			throw e;
		} finally {
			returnSessionToPool(s);
		}
		return list;
	}

	/**
	 * Returns full list of specified objects from DB
	 * 
	 * @param clazz
	 *            class
	 * @return full list of specified objects from DB
	 * @throws HibernateException
	 */
	@SuppressWarnings("unchecked")
	public List<? extends Serializable> getCriterialData(Class<? extends Serializable> clazz) throws HibernateException {
		Session s = getSessionFromPool();
		Transaction t = null;
		List<? extends Serializable> list = null;
		try {
			t = s.beginTransaction();
			list = (List<? extends Serializable>) s.createCriteria(clazz).list();
			t.commit();
		} catch (HibernateException e) {
			t.rollback();
			throw e;
		} finally {
			returnSessionToPool(s);
		}
		return list;
	}

	@SuppressWarnings("unchecked")
	public List<? extends Serializable> getCriterialData(Class<? extends Serializable> clazz, int maxSize)
	        throws HibernateException {
		Session s = getSessionFromPool();
		Transaction t = null;
		List<? extends Serializable> list = null;
		try {
			t = s.beginTransaction();
			list = (List<? extends Serializable>) s.createCriteria(clazz).setMaxResults(maxSize).list();
			t.commit();
		} catch (HibernateException e) {
			t.rollback();
			throw e;
		} finally {
			returnSessionToPool(s);
		}
		return list;
	}

	/**
	 * Returns full list of specified objects from DB ordered by specified
	 * column
	 * 
	 * @param clazz
	 *            class
	 * @return full list of specified objects from DB
	 * @throws HibernateException
	 */
	@SuppressWarnings("unchecked")
	public List<? extends Serializable> getCriterialData(Class<? extends Serializable> clazz, Order order)
	        throws HibernateException {
		Session s = getSessionFromPool();
		Transaction t = null;
		List<? extends Serializable> list = null;
		try {
			t = s.beginTransaction();
			Criteria c = s.createCriteria(clazz);
			if (order != null) {
				c.addOrder(order);
			}
			list = (List<? extends Serializable>) c.list();
			t.commit();
		} catch (HibernateException e) {
			t.rollback();
			throw e;
		} finally {
			returnSessionToPool(s);
		}
		return list;
	}

	@SuppressWarnings("unchecked")
	public List<? extends Serializable> getCriterialData(Class<? extends Serializable> clazz, Order order, int maxSize)
	        throws HibernateException {
		Session s = getSessionFromPool();
		Transaction t = null;
		List<? extends Serializable> list = null;
		try {
			t = s.beginTransaction();
			Criteria c = s.createCriteria(clazz);
			if (order != null) {
				c.addOrder(order);
			}
			list = (List<? extends Serializable>) c.setMaxResults(maxSize).list();
			t.commit();
		} catch (HibernateException e) {
			t.rollback();
			throw e;
		} finally {
			returnSessionToPool(s);
		}
		return list;
	}

	/**
	 * Returns object instance by specified key
	 * 
	 * @param object
	 *            Instance to return
	 * @param key
	 *            Key to find an instance
	 * @return object instance by specified key
	 * @throws HibernateException
	 */
	public Object getObject(Class<? extends Serializable> object, Serializable key) throws HibernateException {
		Session s = getSessionFromPool();
		Transaction t = null;
		Object o = null;
		try {
			t = s.beginTransaction();
			o = s.get(object, key);
			t.commit();
		} catch (HibernateException e) {
			t.rollback();
			throw e;
		} finally {
			returnSessionToPool(s);
		}
		return o;
	}

	/**
	 * Deletes object instance from DB
	 * 
	 * @param o
	 *            Object instance to delete
	 * @throws HibernateException
	 */
	public void deleteObject(Object o) throws HibernateException {
		Session s = getSessionFromPool();
		Transaction t = null;
		try {
			t = s.beginTransaction();
			s.delete(o);
			t.commit();
		} catch (HibernateException e) {
			t.rollback();
			throw e;
		} finally {
			returnSessionToPool(s);
		}

	}

	public void deleteObjectBulk(Collection<? extends Serializable> o) throws HibernateException {
		Session s = getSessionFromPool();
		Transaction t = null;
		try {
			t = s.beginTransaction();
			for (Object o1 : o) {
				s.delete(o1);
			}
			t.commit();
		} catch (HibernateException e) {
			t.rollback();
			throw e;
		} finally {
			returnSessionToPool(s);
		}

	}

	/**
	 * Deletes object instance from DB
	 * 
	 * @param o
	 *            Object instance to delete
	 * @param name
	 *            entity name
	 * @throws HibernateException
	 */
	public void deleteObject(Object o, String name) throws HibernateException {
		Session s = getSessionFromPool();
		Transaction t = null;
		try {
			t = s.beginTransaction();
			s.delete(name, o);
			t.commit();
		} catch (HibernateException e) {
			t.rollback();
			throw e;
		} finally {
			returnSessionToPool(s);
		}
	}

	/**
	 * Updates object instance in DB
	 * 
	 * @param o
	 *            object instance to update
	 * @throws HibernateException
	 */
	public void updateObject(Object o) throws HibernateException {
		Session s = getSessionFromPool();
		Transaction t = null;
		try {
			t = s.beginTransaction();
			s.update(o);
			t.commit();
		} catch (HibernateException e) {
			t.rollback();
			throw e;
		} finally {
			returnSessionToPool(s);
		}
	}

	public void updateBulk(Collection<Object> o) throws HibernateException {
		Session s = getSessionFromPool();
		Transaction t = null;
		try {
			t = s.beginTransaction();
			for (Object o1 : o) {
				s.update(o1);
			}
			t.commit();
		} catch (HibernateException e) {
			t.rollback();
			throw e;
		} finally {
			returnSessionToPool(s);
		}
	}

	/**
	 * Update the persistent instance with the identifier of the given detached
	 * 
	 * @param o
	 *            object instance to update
	 * @param name
	 *            entity name
	 * @throws HibernateException
	 */
	public void updateObject(Object o, String name) throws HibernateException {
		Session s = getSessionFromPool();
		Transaction t = null;
		try {
			t = s.beginTransaction();
			s.update(name, o);
			t.commit();
		} catch (HibernateException e) {
			t.rollback();
			throw e;
		} finally {
			returnSessionToPool(s);
		}
	}

	/**
	 * Updates an object instance in DB or saves it to DB if it has not been
	 * saved yet
	 * 
	 * @param o
	 *            object instance to update or save
	 * @throws HibernateException
	 */
	public void saveOrUpdateObject(Object o) throws HibernateException {
		Session s = getSessionFromPool();
		Transaction t = null;
		try {
			t = s.beginTransaction();
			s.saveOrUpdate(o);
			t.commit();
		} catch (HibernateException e) {
			t.rollback();
			throw e;
		} finally {
			returnSessionToPool(s);
		}
	}

	public void saveOrUpdateBulk(Collection<Object> o) throws HibernateException {
		Session s = getSessionFromPool();
		Transaction t = null;
		try {
			t = s.beginTransaction();
			for (Object o1 : o) {
				s.saveOrUpdate(o1);
			}
			t.commit();
		} catch (HibernateException e) {
			t.rollback();
			throw e;
		} finally {
			returnSessionToPool(s);
		}
	}

	public Serializable saveObject(Object o) throws HibernateException {
		Session s = getSessionFromPool();
		Transaction t = null;
		Serializable result = null;
		try {
			t = s.beginTransaction();
			result = s.save(o);
			t.commit();
			return result;
		} catch (HibernateException e) {
			t.rollback();
			throw e;
		} finally {
			returnSessionToPool(s);
		}

	}

	public Collection<Serializable> saveBulk(Collection<Object> o) throws HibernateException {
		Session s = getSessionFromPool();
		Transaction t = null;
		Collection<Serializable> result = new LinkedList<Serializable>();
		try {
			t = s.beginTransaction();
			for (Object o1 : o) {
				result.add(s.save(o1));
			}
			t.commit();
			return result;
		} catch (HibernateException e) {
			t.rollback();
			throw e;
		} finally {
			returnSessionToPool(s);
		}

	}

}
