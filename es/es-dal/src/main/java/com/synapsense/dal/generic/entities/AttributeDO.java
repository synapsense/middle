package com.synapsense.dal.generic.entities;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.synapsense.dto.PropertyDescr;

public class AttributeDO implements java.io.Serializable {
	private static final long serialVersionUID = 6067329585356043267L;

	private Integer attributeId;
	private String attributeName;
	private String mappedName;
	private boolean historical;
	private boolean collection;
	private boolean statical;
	private AttributeTypeDO attributeType;
	private ObjectTypeDO objectType;
	private Set<RuleDO> rules = new HashSet<RuleDO>(0);
	private Set<RuleObjectSrcPropertyDO> ruleSrcProperties = new HashSet<RuleObjectSrcPropertyDO>(0);
	private Set<ValueDO> values = new HashSet<ValueDO>(0);
	private Map<String, TagDO> tags = new HashMap<String, TagDO>(0);

	public static AttributeDO newInstance(String name, AttributeTypeDO attributeType, ObjectTypeDO objectType) {
		AttributeDO attribute = new AttributeDO(name, attributeType, objectType);
		return attribute;
	}

	public static AttributeDO newInstance(String name, AttributeTypeDO attributeType, ObjectTypeDO objectType,
	        boolean historical, boolean collection, boolean statical) {
		AttributeDO attribute = new AttributeDO(name, attributeType, objectType, historical, collection, statical);
		return attribute;
	}

	AttributeDO() {
	}

	AttributeDO(String attributeName, AttributeTypeDO attributeType, ObjectTypeDO objectType) {
		this(attributeName, null, attributeType, objectType, false, false, false);
	}

	AttributeDO(String attributeName, String mappedName, AttributeTypeDO attributeType, ObjectTypeDO objectType) {
		this(attributeName, mappedName, attributeType, objectType, false, false, false);
	}

	AttributeDO(String attributeName, AttributeTypeDO attributeType, ObjectTypeDO objectType, boolean historical,
	        boolean collection, boolean staticFlag) {
		this(attributeName, null, attributeType, objectType, historical, collection, staticFlag);
	}

	AttributeDO(String attributeName, String mappedName, AttributeTypeDO attributeType, ObjectTypeDO objectType,
	        boolean historical, boolean collection, boolean staticFlag) {
		this.attributeName = attributeName;
		this.mappedName = mappedName;
		this.attributeType = attributeType;
		this.objectType = objectType;
		this.statical = staticFlag;

	}

	public Object getDefaultValue() {
		return null;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder(200);
		sb.append("Property:" + this.attributeName);
		sb.append(";");
		sb.append("Type:" + this.attributeType.getType());
		sb.append(";");
		sb.append("Mapped:" + this.mappedName);
		sb.append(";");
		sb.append("Historical:" + this.historical);
		sb.append(";");
		sb.append("Collection:" + this.collection);
		sb.append(";");
		sb.append(";");
		sb.append("ObjectType:" + this.objectType);
		return sb.toString();
	}

	public Set<RuleDO> getRules() {
		return rules;
	}

	public void setRules(Set<RuleDO> rules) {
		this.rules = rules;
	}

	public Integer getAttributeId() {
		return this.attributeId;
	}

	private void setAttributeId(Integer attributeId) {
		this.attributeId = attributeId;
	}

	public String getAttributeName() {
		return this.attributeName;
	}

	public void setAttributeName(String attributeName) {
		this.attributeName = attributeName;
	}

	public AttributeTypeDO getAttributeType() {
		return this.attributeType;
	}

	public ObjectTypeDO getObjectType() {
		return this.objectType;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((attributeName == null) ? 0 : attributeName.hashCode());
		result = prime * result + ((objectType == null) ? 0 : objectType.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final AttributeDO other = (AttributeDO) obj;
		if (attributeName == null) {
			if (other.attributeName != null)
				return false;
		} else if (!attributeName.equals(other.attributeName))
			return false;
		if (objectType == null) {
			if (other.objectType != null)
				return false;
		} else if (!objectType.equals(other.objectType))
			return false;
		return true;
	}

	public boolean getHistorical() {
		return historical;
	}

	public void setHistorical(boolean historical) {
		this.historical = historical;
	}

	public Set<ValueDO> getValues() {
		return values;
	}

	public Set<RuleObjectSrcPropertyDO> getRuleSrcProperties() {
		return ruleSrcProperties;
	}

	public void setRuleSrcProperties(Set<RuleObjectSrcPropertyDO> ruleSrcProperties) {
		this.ruleSrcProperties = ruleSrcProperties;
	}

	public String getMappedName() {
		return mappedName;
	}

	public void setMappedName(String mappedName) {
		this.mappedName = mappedName;
	}

	public void setValues(Set<ValueDO> values) {
		this.values = values;
	}

	/**
	 * Generates <code>PropertyDescr</code> representation of attribute POJO
	 * 
	 * @return
	 */
	public PropertyDescr getPropertyDescr() {
		PropertyDescr pd = new PropertyDescr(this.attributeName, this.mappedName, this.attributeType.getType(),
		        this.historical, this.collection, this.statical);
		for (TagDO tagDO : tags.values()) {
			pd.getTagDescriptors().add(tagDO.getTagDescr());
		}
		return pd;
	}

	public boolean getCollection() {
		return collection;
	}

	public void setCollection(boolean collection) {
		this.collection = collection;
	}

	public boolean isStatical() {
		return statical;
	}

	public void setStatical(boolean statical) {
		this.statical = statical;
	}

	public Map<String, TagDO> getTags() {
		return tags;
	}

	public void setTags(Map<String, TagDO> tags) {
		this.tags = tags;
	}
}
