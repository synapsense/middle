package com.synapsense.dal.generic.batch;

import java.util.Iterator;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.log4j.Logger;

import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;

/**
 * Simple class to represent queue of <code>SimpleUpdateUnit</code>s with hash
 * access to its elements. Necessary to group elements by <TO,property> pair to
 * avoid the same property updates in different batch tasks. Has no
 * <code>Queue</code> interface cause it's just util class.
 * 
 * @author shabanov
 * 
 */
public class HashedBlockingQueue implements Iterable<SingleUpdateUnit> {

	@SuppressWarnings("unused")
    private static final long serialVersionUID = 2729405805242193764L;

	private static final Logger logger = Logger.getLogger(HashedBlockingQueue.class);

	private Map<HashKey, SingleUpdateUnit> hash;

	private Queue<SingleUpdateUnit> queue;

	private int initialSize = 0;
	/**
	 * lock for modifying queue content
	 */
	private final ReentrantLock modifyLock = new ReentrantLock();

	HashedBlockingQueue(final int size) {
		this.hash = new ConcurrentHashMap<HashKey, SingleUpdateUnit>(size);
		this.queue = new LinkedBlockingQueue<SingleUpdateUnit>(size);
		this.initialSize = size;
	}

	private boolean offer(final SingleUpdateUnit unit) {
		boolean offered = false;
		for (ValueTO val : unit.getValues()) {
			offered = offer(unit.getTo(), unit.getPropertyName(), val, false, unit.getGroupId());
		}
		return offered;
	}

	public boolean offer(final TO<?> to, final String property, final ValueTO value, boolean timestampOnly, Integer ID) {
		HashKey hashKey = generateHash(to, property, ID);
		boolean offered = false;
		modifyLock.lock();
		try {
			// checks hash table for already existing unit
			SingleUpdateUnit hashed = hash.get(hashKey);
			if (hashed == null) {
				// puts new unit to queue and to the hash table
				SingleUpdateUnit tempUnit = new SingleUpdateUnit(to, property, value, timestampOnly, ID);
				offered = queue.offer(tempUnit);
				if (offered) {
					hash.put(hashKey, tempUnit);
				} else {
					logger.warn("Queue if full , max size [" + DbBatchServiceConfiguration.QUEUE_SIZE
					        + "] has been exchaused!Lost value: " + tempUnit);
				}
			} else {
				// set back timestamponly update flag to false if new
				// notification contains different value.
				if (!timestampOnly && hashed.isTimestampOnly()) {
					hashed.setTimestampOnly(false);
				}

				// queue size does not change , only new value is added to
				// existing update unit
				hashed.addValue(value);
			}

		} finally {
			modifyLock.unlock();
		}

		return offered;
	}

	public SingleUpdateUnit peek() {
		return queue.peek();
	}

	public SingleUpdateUnit poll() {
		SingleUpdateUnit pollUnit = null;
		modifyLock.lock();
		try {
			pollUnit = queue.poll();
			if (pollUnit != null) {
				HashKey hashKey = generateHash(pollUnit.getTo(), pollUnit.getPropertyName(), pollUnit.getGroupId());
				hash.remove(hashKey);
			}
		} finally {
			modifyLock.unlock();
		}
		return pollUnit;
	}

	public boolean isEmpty() {
		return size() == 0;
	}

	public int size() {
		modifyLock.lock();
		try {
			return queue.size();
		} finally {
			modifyLock.unlock();
		}
	}

	public void rewriteBy(final HashedBlockingQueue anotherQueue) {
		modifyLock.lock();
		try {
			// clear
			queue.clear();
			hash.clear();
			// fill new data
			int i = 0;
			while (i < initialSize && !anotherQueue.isEmpty()) {
				this.offer(anotherQueue.peek());
			}
		} finally {
			modifyLock.unlock();
		}
	}

	@Override
	public Iterator<SingleUpdateUnit> iterator() {
		return queue.iterator();
	}

	interface Predicate<T> {
		boolean eval(T value);
	}

	public void clear(Predicate<SingleUpdateUnit> p) {
		modifyLock.lock();
		try {
			Iterator<SingleUpdateUnit> iterator = queue.iterator();
			while (iterator.hasNext()) {
				SingleUpdateUnit unit = iterator.next();
				if (p.eval(unit)) {
					iterator.remove();
				}
			}
		} finally {
			modifyLock.unlock();
		}
	}

	public void lock() {
		modifyLock.lock();
	}

	public void unlock() {
		modifyLock.unlock();
	}

	private HashKey generateHash(TO<?> to, String property, Integer iD) {
		return new BasicHashKey(to, property, iD);
	}

	private interface HashKey {
		int hash();
	}

	private class BasicHashKey implements HashKey {
		public BasicHashKey(TO<?> to, String property, Integer id) {
			this.to = to;
			this.property = property;
			this.id = id;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((to == null) ? 0 : to.hashCode());
			result = prime * result + ((property == null) ? 0 : property.hashCode());
			result = prime * result + ((id == null) ? 0 : id.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			BasicHashKey other = (BasicHashKey) obj;
			if (id == null) {
				if (other.id != null)
					return false;
			} else if (!id.equals(other.id))
				return false;
			if (property == null) {
				if (other.property != null)
					return false;
			} else if (!property.equals(other.property))
				return false;
			if (to == null) {
				if (other.to != null)
					return false;
			} else if (!to.equals(other.to))
				return false;
			return true;
		}

		private TO<?> to;
		private String property;
		private Integer id;

		@Override
		public int hash() {
			return hashCode();
		}
	}
}