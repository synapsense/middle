package com.synapsense.dal.generic.batch;

public final class ValueTuple {
	private int attrId;
	private int valueId;
	private boolean historical;

	public ValueTuple(int attrId, int valueId, boolean historical) {
		this.attrId = attrId;
		this.valueId = valueId;
		this.historical = historical;
	}

	public boolean isHistorical() {
		return historical;
	}

	public int getValueId() {
		return valueId;
	}

	public int getAttrId() {
		return attrId;
	}

	@Override
	public String toString() {
		return "ValueTuple [attrId=" + attrId + ", valueId=" + valueId + ", historical=" + historical + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + attrId;
		result = prime * result + (historical ? 1231 : 1237);
		result = prime * result + valueId;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ValueTuple other = (ValueTuple) obj;
		if (attrId != other.attrId)
			return false;
		if (historical != other.historical)
			return false;
		if (valueId != other.valueId)
			return false;
		return true;
	}

}
