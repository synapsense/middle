package com.synapsense.dal.generic.batch.loader;

public interface Resource {
	String getName();
}
