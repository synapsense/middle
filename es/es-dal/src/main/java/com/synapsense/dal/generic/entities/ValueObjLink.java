package com.synapsense.dal.generic.entities;

import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;

public class ValueObjLink extends AbstractValue<TO<?>> implements java.io.Serializable {
	private static final long serialVersionUID = 2079875577350067760L;

	private Integer id;
	private ValueDO valueRef;
	private Object objectId;
	private String objectType;

	ValueObjLink() {
		super();
	}

	public ValueObjLink(ValueDO valueRef) {
		this(valueRef, null);
	}

	public ValueObjLink(ValueDO valueRef, Object value) {
		this();
		this.valueRef = valueRef;
		if (value == null) {
			return;
		}
		TO<?> typedValue = (TO<?>) value;
		this.objectId = typedValue.getID();
		this.objectType = typedValue.getTypeName();
	}

	public Integer getId() {
		return this.id;
	}

	private void setId(Integer id) {
		this.id = id;
	}

	public ValueDO getValueRef() {
		return this.valueRef;
	}

	public void setValueRef(ValueDO valueRef) {
		this.valueRef = valueRef;
	}

	public TO<?> getValue() {
		if (this.objectId == null) {
			return null;
		}
		if (this.objectType == null) {
			return null;
		}
		return TOFactory.getInstance().loadTO(this.objectType + ":" + this.objectId);
	}

	public void setValue(TO<?> value) {
		if (value == null) {
			this.objectId = null;
			this.objectType = null;
		} else {
			this.objectId = (Integer) value.getID();
			this.objectType = value.getTypeName();
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((objectId == null) ? 0 : objectId.hashCode());
		result = prime * result + ((objectType == null) ? 0 : objectType.hashCode());
		result = prime * result + ((valueRef == null) ? 0 : valueRef.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final ValueObjLink other = (ValueObjLink) obj;
		if (objectId == null) {
			if (other.objectId != null)
				return false;
		} else if (!objectId.equals(other.objectId))
			return false;
		if (objectType == null) {
			if (other.objectType != null)
				return false;
		} else if (!objectType.equals(other.objectType))
			return false;
		if (valueRef == null) {
			if (other.valueRef != null)
				return false;
		} else if (!valueRef.equals(other.valueRef))
			return false;
		return true;
	}

}
