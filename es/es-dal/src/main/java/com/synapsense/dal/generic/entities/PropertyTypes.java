package com.synapsense.dal.generic.entities;

import java.util.HashMap;

import com.synapsense.dto.BinaryData;
import com.synapsense.dto.TO;
import com.synapsense.service.impl.dao.to.BinaryDataImpl;
import com.synapsense.service.impl.dao.to.PropertyTO;
import com.synapsense.service.impl.exception.DALSystemException;

public final class PropertyTypes {
	/**
	 * POJO implementations of all supported types used for determine what class
	 * is responsible for storing values of some java type
	 */
	private static HashMap<String, Class<? extends AbstractValue<?>>> propertiesTypes = new HashMap<String, Class<? extends AbstractValue<?>>>();
	static {
		propertiesTypes.put(java.lang.String.class.getName(), Valuev.class);

		propertiesTypes.put(java.lang.Long.class.getName(), Valuel.class);

		propertiesTypes.put(java.lang.Integer.class.getName(), Valuei.class);

		propertiesTypes.put(java.lang.Double.class.getName(), Valued.class);

		propertiesTypes.put(java.util.Date.class.getName(), Valuedt.class);

		propertiesTypes.put(TO.class.getName(), ValueObjLink.class);

		propertiesTypes.put(PropertyTO.class.getName(), ValuePropertyLink.class);

		propertiesTypes.put(BinaryData.class.getName(), ValueBinary.class);
		// additional synonym
		propertiesTypes.put(BinaryDataImpl.class.getName(), ValueBinary.class);
	}

	/**
	 * Try to get Hibernate class reference by specified property type
	 * 
	 * @param propertyType
	 * @return Hibernate mapped Class (POJO)
	 * @throws IllegalArgumentException
	 *             if requested propertyType is not supported
	 */
	public static Class<?> checkAndGetPropertyTypeClass(String propertyType) throws DALSystemException {
		Class<?> clazz = propertiesTypes.get(propertyType);

		if (clazz == null) {
			throw new DALSystemException("The type [" + propertyType
			        + "] is not supported by the system and has to be defined before using");
		}

		return clazz;
	}

}
