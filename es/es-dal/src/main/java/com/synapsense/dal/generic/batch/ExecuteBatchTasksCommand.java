package com.synapsense.dal.generic.batch;

import java.util.LinkedList;
import java.util.List;

import javax.transaction.UserTransaction;

import org.apache.log4j.Logger;

import com.synapsense.commandprocessor.api.Command;
import com.synapsense.commandprocessor.api.CommandEngine;
import com.synapsense.util.executor.Action;
import com.synapsense.util.executor.Executor;

public class ExecuteBatchTasksCommand implements Command {

	private static final Logger logger = Logger.getLogger(ExecuteBatchTasksCommand.class);

	private List<List<SingleUpdateUnit>> sublists;
	private CommandEngine engine;
	private Executor executor;
	private ModelUpdater updater;
	private UserTransaction userTransaction;

	ExecuteBatchTasksCommand(List<List<SingleUpdateUnit>> sublists, CommandEngine engine, Executor executor,
	        ModelUpdater updater, UserTransaction userTransaction) {
		super();
		this.sublists = sublists;
		this.engine = engine;
		this.updater = updater;
		this.executor = executor;
		this.userTransaction = userTransaction;
	}

	@Override
	public void execute() {
		// creates task for every queue chunk
		List<Action> subTasks = new LinkedList<Action>();
		for (List<SingleUpdateUnit> list : sublists) {
			subTasks.add(BatchSaveAction.newBatchSaveAction(list, engine, executor, updater, userTransaction));
		}

		if (logger.isDebugEnabled())
			logger.debug("Starting batch udpate operation.Update task was divided into : " + sublists.size()
			        + " subtasks");

		long start = System.currentTimeMillis();

		// executes all subtask and blocks execution
		// till last subtask is finished
		executor.doAction(subTasks, true);

		if (logger.isDebugEnabled())
			logger.debug("END : subtasks [" + subTasks.size() + "] execution time(ms):"
			        + (System.currentTimeMillis() - start));
	}

}
