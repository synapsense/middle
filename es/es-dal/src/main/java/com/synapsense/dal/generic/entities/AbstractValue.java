package com.synapsense.dal.generic.entities;

/**
 * All classes which represents generic values must extends it
 * 
 * @author shabanov
 * 
 * @param <NATIVE_TYPE>
 *            Underlying value type
 */
public abstract class AbstractValue<NATIVE_TYPE> {
	public abstract void setValue(NATIVE_TYPE value);

	public abstract NATIVE_TYPE getValue();
}
