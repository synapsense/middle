package com.synapsense.dal.generic.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ejb.DependsOn;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.interceptor.Interceptors;
import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.apache.log4j.Logger;
import org.hibernate.Session;

import com.synapsense.dal.generic.entities.AbstractValue;
import com.synapsense.dal.generic.entities.AttributeDO;
import com.synapsense.dal.generic.entities.AttributeTypeDO;
import com.synapsense.dal.generic.entities.HistoricalBinaryValueDO;
import com.synapsense.dal.generic.entities.ObjectDO;
import com.synapsense.dal.generic.entities.ObjectTypeDO;
import com.synapsense.dal.generic.entities.PropertyTypes;
import com.synapsense.dal.generic.entities.TagDO;
import com.synapsense.dal.generic.entities.TagTypes;
import com.synapsense.dal.generic.entities.TagValueDO;
import com.synapsense.dal.generic.entities.TypedValue;
import com.synapsense.dal.generic.entities.ValueDO;
import com.synapsense.dal.generic.entities.ValueProxy;
import com.synapsense.dal.generic.interceptors.SessionFilterNonStaticOnly;
import com.synapsense.dal.generic.interceptors.SessionFilterStaticOnly;
import com.synapsense.dal.generic.utils.FindGenericSQL;
import com.synapsense.dal.generic.utils.QueryFactory;
import com.synapsense.dal.generic.utils.Util;
import com.synapsense.dto.BinaryData;
import com.synapsense.dto.CollectionTO;
import com.synapsense.dto.Criterion;
import com.synapsense.dto.EnvObject;
import com.synapsense.dto.EnvObjectBundle;
import com.synapsense.dto.EnvObjectHistory;
import com.synapsense.dto.HistoricalRecord;
import com.synapsense.dto.ObjectType;
import com.synapsense.dto.ObjectTypeMatchMode;
import com.synapsense.dto.PropertyDescr;
import com.synapsense.dto.Relation;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOBundle;
import com.synapsense.dto.TOFactory;
import com.synapsense.dto.Tag;
import com.synapsense.dto.TagDescriptor;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.EnvException;
import com.synapsense.exception.IllegalInputParameterException;
import com.synapsense.exception.ObjectExistsException;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.exception.PropertyNotFoundException;
import com.synapsense.exception.ServerConfigurationException;
import com.synapsense.exception.TagNotFoundException;
import com.synapsense.exception.UnableToConvertPropertyException;
import com.synapsense.exception.UnableToConvertTagException;
import com.synapsense.exception.UnsupportedRelationException;
import com.synapsense.search.QueryDescription;
import com.synapsense.service.Environment;
import com.synapsense.service.impl.dao.to.EnvObjTypeTO;
import com.synapsense.service.impl.dao.to.GenericObjectTO;
import com.synapsense.service.impl.dao.to.PropertyTO;
import com.synapsense.service.impl.exception.DALSystemException;
import com.synapsense.util.CollectionUtils;

@Stateless
@Remote(Environment.class)
//@PoolClass annotation is not available in JBoss 7
//@PoolClass(value = org.jboss.ejb3.StrictMaxPool.class, maxSize = 100, timeout = 10000)
@TransactionManagement(TransactionManagementType.CONTAINER)
@DependsOn("NetworkStatisticDAO")
public class GenericDAO implements Environment {

	private static final Logger log = Logger.getLogger(GenericDAO.class);

	@PersistenceContext(unitName = "HibernateDAL")
	public void setEM(final EntityManager em) {
		this.em = em;
	}

	private EntityManager em;

	/**
	 * Factory for ValueDO representations
	 * 
	 * @author shabanov
	 * 
	 */
	private class ValueProxyFactory {
		private Map<Class<?>, ValueReadOnlyProxy> proxies = new HashMap<Class<?>, ValueReadOnlyProxy>();

		private ValueReadOnlyProxy getProxy(final Class<?> clazz) {

			return proxies.get(clazz);
		}

		private void addProxy(final ValueReadOnlyProxy instance) {
			proxies.put(instance.getClass(), instance);
		}

		/**
		 * creates proxy for value the proxy responsible for value
		 * representation
		 * 
		 * @param clazz
		 *            Value of that class we need to get
		 * @param propName
		 *            Property name
		 * @param dbValue
		 *            Value POJO
		 * @return Related <code>ValueProxy</code> implementation
		 */
		public ValueReadOnlyProxy createPropertyValueProxy(final Class<?> clazz, final String propName,
		        final ValueDO dbValue) {
			ValueReadOnlyProxy result = null;
			// value is not present in db but client need result as
			// ValueTO which cannot be a null
			if (dbValue == null && clazz == ValueTO.class) {
				result = this.getProxy(PVTONullValue.class);
				if (result == null) {
					result = new PVTONullValue(propName);
					this.addProxy(result);
				} else {
					result.reInit(propName, null);
				}
				return result;
			}
			// if value is not present in db and client wants to get value as is
			// returns null
			if (dbValue == null) {
				result = this.getProxy(NullValue.class);
				if (result == null) {
					result = new NullValue();
					this.addProxy(result);
				}
				return result;
			}

			AttributeDO property = dbValue.getAttribute();
			if (clazz == ValueTO.class && !property.getCollection()) {
				result = this.getProxy(PVTOSingleValue.class);
				if (result == null) {
					result = new PVTOSingleValue(propName, dbValue);
					this.addProxy(result);
				} else {
					result.reInit(propName, dbValue);
				}
				return result;
			}
			if (clazz == ValueTO.class && property.getCollection()) {
				result = this.getProxy(PVTOCollectionValue.class);
				if (result == null) {
					result = new PVTOCollectionValue(propName, dbValue);
					this.addProxy(result);
				} else {
					result.reInit(propName, dbValue);
				}
				return result;
			}
			if (clazz == Collection.class && property.getCollection()) {
				result = this.getProxy(CollectionSimpleValue.class);
				if (result == null) {
					result = new CollectionSimpleValue(dbValue);
					this.addProxy(result);
				} else {
					result.reInit(propName, dbValue);
				}
				return result;
			}
			if (clazz == Collection.class && !property.getCollection()) {
				throw new DALSystemException("Property[" + propName + "] is not collection");
			}

			result = this.getProxy(SimpleSingleValue.class);
			if (result == null) {
				result = new SimpleSingleValue(dbValue);
				this.addProxy(result);
			} else {
				result.reInit(propName, dbValue);
			}
			return result;
		}
	}

	private ValueProxyFactory valuePropxyFactory = new ValueProxyFactory();

	private interface ValueReadOnlyProxy extends Serializable {
		void reInit(String propName, ValueDO dbValue);

		Object getValue();
	}

	private class SimpleSingleValue implements ValueReadOnlyProxy {
		/**
		 * 
		 */
		private static final long serialVersionUID = -7369531477142841452L;

		private ValueDO wrappedValue;

		public SimpleSingleValue(final ValueDO nativeValue) {
			wrappedValue = nativeValue;
		}

		@Override
		public Object getValue() {
			return wrappedValue.getValue().iterator().next();
		}

		@Override
		public void reInit(final String propName, final ValueDO dbValue) {
			wrappedValue = dbValue;
		}
	}

	private class NullValue implements ValueReadOnlyProxy {
		/**
		 * 
		 */
		private static final long serialVersionUID = 2338931081021755004L;

		@Override
		public Object getValue() {
			return null;
		}

		@Override
		public void reInit(final String propName, final ValueDO dbValue) {
			// no state
		}
	}

	private class PVTOSingleValue implements ValueReadOnlyProxy {
		private static final long serialVersionUID = 4814615688121390240L;
		private ValueDO wrappedValue;
		private String propName;

		public PVTOSingleValue(final String propName, final ValueDO nativeValue) {
			this.propName = propName;
			wrappedValue = nativeValue;
		}

		@Override
		public ValueTO getValue() {
			Object propValue = wrappedValue.getValue().iterator().next();
			long propTimestamp = wrappedValue.getTimestamp();
			ValueTO result = new ValueTO(propName, propValue, propTimestamp);
			return result;
		}

		@Override
		public void reInit(final String propName, final ValueDO dbValue) {
			this.propName = propName;
			wrappedValue = dbValue;
		}
	}

	private class PVTONullValue implements ValueReadOnlyProxy {
		private static final long serialVersionUID = 1426509171725322198L;
		private String propName;

		public PVTONullValue(final String propName) {
			this.propName = propName;
		}

		@Override
		public ValueTO getValue() {
			return new ValueTO(propName, null, 0L);
		}

		@Override
		public void reInit(final String propName, final ValueDO dbValue) {
			this.propName = propName;
		}
	}

	private class CollectionSimpleValue implements ValueReadOnlyProxy {
		private static final long serialVersionUID = -6127982697616512038L;
		private ValueDO wrappedValue;

		public CollectionSimpleValue(final ValueDO nativeValue) {
			wrappedValue = nativeValue;
		}

		@Override
		public Collection<Object> getValue() {
			return wrappedValue.getValue();
		}

		@Override
		public void reInit(final String propName, final ValueDO dbValue) {
			wrappedValue = dbValue;
		}
	}

	private class PVTOCollectionValue implements ValueReadOnlyProxy {
		private static final long serialVersionUID = 790722488460676486L;
		private ValueDO wrappedValue;
		private String propName;

		public PVTOCollectionValue(final String propName, final ValueDO nativeValue) {
			this.propName = propName;
			wrappedValue = nativeValue;
		}

		@Override
		public ValueTO getValue() {
			Long propTiemstamp = wrappedValue.getTimestamp();
			ValueTO result = new ValueTO(propName, wrappedValue.getValue(), propTiemstamp);
			return result;
		}

		@Override
		public void reInit(final String propName, final ValueDO dbValue) {
			this.propName = propName;
			wrappedValue = dbValue;
		}
	}

	/**
	 * Synchronizes attribute POJO from <code>PropertyDescr</code>
	 * 
	 * @param pd
	 *            <code>PropertyDescr</code> descriptor of property
	 */
	private void updateAttributeFromPD(final AttributeDO attribute, final PropertyDescr pd) {
		// TO and PropertyTO cannot be historical
		if (pd.isHistorical()
		        && (pd.getType() == TO.class || pd.getType() == PropertyTO.class )) {
			throw new DALSystemException("Property of types [" + TO.class + ","
					+ PropertyTO.class
					+ "] cannot store history!.Feature is not implemented yet");
		}

		// property cannot be historical and collection simultaneously now
		if (pd.isHistorical() && pd.isCollection()) {
			throw new DALSystemException(
			        "Property cannot be historical and collection simultaneously.Feature is not implemented yet");
		}

		if (!attribute.getAttributeType().getType().equals(pd.getTypeName())) {
			throw new DALSystemException(
			        "To change property type , you must delete current property and create new one");
		}

		attribute.setAttributeName(pd.getName());
		attribute.setHistorical(pd.isHistorical());
		attribute.setMappedName(pd.getMappedName());
		attribute.setCollection(pd.isCollection());
		attribute.setStatical(pd.isStatic());
	}

	private void updateTagFromTagDescr(TagDO tagDO, TagDescriptor td) {
		if (!tagDO.getTagType().getType().equals(td.getTypeName())) {
			throw new DALSystemException("To change tag type , you must delete current tag and create new one");
		}
		tagDO.setName(td.getName());
	}

	private void updateTagFromPD(AttributeDO attribute, PropertyDescr pd, ObjectType objType) {
		Set<TagDescriptor> tds = pd.getTagDescriptors();
		Map<String, TagDO> tagsDB = attribute.getTags();
		// update exist tags and create new tags
		for (TagDescriptor tagDescr : tds) {
			TagDO tagDO = tagsDB.get(tagDescr.getName());
			if (tagDO == null) {
				TagDO newTagDO = newTagFromTagDescr(tagDescr, attribute);
				tagsDB.put(newTagDO.getName(), newTagDO);
			} else {
				updateTagFromTagDescr(tagDO, tagDescr);
			}
		}

		// checks tags which were deleted
		Collection<TagDO> existingTagDOs = tagsDB.values();

		Collection<String> tagsToRemove = new HashSet<String>();

		for (TagDO a : existingTagDOs) {
			TagDescriptor td = a.getTagDescr();
			// if updated type does not contain the tag which are
			// persistent(already exist in storage)
			// adds them to the transient tag list
			if (!pd.getTagDescriptors().contains(td)) {
				tagsToRemove.add(td.getName());
			}
		}

		// and removes it
		for (String tagName : tagsToRemove) {
			TagDO tagToRemove = tagsDB.get(tagName);
			tagsDB.remove(tagName);
			em.remove(tagToRemove);

			/*
			 * sbLog.append("Property [" + attributeName + "] of type [" +
			 * objTypeDO.getName() + "] was removed"); sbLog.append(Util.CRLF);
			 */
		}
		// save changes of type's attributes to persistence collection
		attribute.setTags(tagsDB);
	}

	private TagDO newTagFromTagDescr(TagDescriptor tagDescr, AttributeDO attribute) {
		// check that type supported. Should be uncommented and implemented
		checkTagType(tagDescr.getTypeName());

		AttributeTypeDO tagTypeDO = getAttributeType(tagDescr.getTypeName());

		if (tagTypeDO == null) {
			throw new DALSystemException("DB is in inconsistent state.Tag type [" + tagDescr.getTypeName()
			        + "] has no related row in a db table. Check that 'seed_db' is done!");
		}

		TagDO tagDO = new TagDO(tagDescr.getName(), attribute, tagTypeDO);
		updateTagFromTagDescr(tagDO, tagDescr);
		return tagDO;
	}

	private AttributeDO newAttributeFromPD(PropertyDescr pd, ObjectTypeDO objectType) throws ObjectNotFoundException {
		// check - is property type supported ?
		checkPropertyType(pd.getTypeName());

		// check - is property type existing in db
		AttributeTypeDO attributeTypeDO = getAttributeType(pd.getTypeName());
		if (attributeTypeDO == null) {
			throw new DALSystemException("DB is in inconsistent state.Property type [" + pd.getTypeName()
			        + "] has no related row in a db table. Check that 'seed_db' is done!");
		}

		AttributeDO attribute = AttributeDO.newInstance(pd.getName(), attributeTypeDO, objectType);

		updateAttributeFromPD(attribute, pd);
		return attribute;
	}

	/**
	 * Checks supporting of property type with specified name
	 * 
	 * @param typeName
	 *            Type's name
	 * @throws DALSystemException
	 *             When type is not supported
	 */
	private void checkPropertyType(final String typeName) {
		PropertyTypes.checkAndGetPropertyTypeClass(typeName);
	}

	/**
	 * Checks supporting of property type with specified name
	 * 
	 * @param tagName
	 *            Name of tag to check
	 * @throws DALSystemException
	 *             When type is not supported
	 */
	private void checkTagType(String tagName) {
		TagTypes.checkAndGetTagTypeClass(tagName);
	}

	/**
	 * check existing of property within the border of object type
	 */
	private void checkPropertyExists(final ObjectTypeDO objectType, final String propertyName)
	        throws PropertyNotFoundException {
		if (!objectType.getAttributes().containsKey(propertyName)) {
			throw new PropertyNotFoundException(objectType.toString(), propertyName);
		}
	}

	/**
	 * check existing of tag within the border of property type
	 */
	private void checkTagExists(AttributeDO attribute, String tagName) throws TagNotFoundException {
		if (!attribute.getTags().containsKey(tagName)) {
			throw new TagNotFoundException(attribute.getAttributeName(), tagName);
		}
	}

	/**
	 * Sets value of object's property
	 * 
	 * @param object
	 * @param attribute
	 * @param propValue
	 * @param timestamp
	 * @throws UnableToConvertPropertyException
	 */
	private void setValueOf(final ObjectDO object, final AttributeDO attribute, final Object propValue,
	        final long timestamp) throws UnableToConvertPropertyException {
		ValueDO value = null;
		boolean isValueStated = object.getAttributesValues().containsKey(attribute);

		if (!isValueStated) {
			// creates new Value instance
			value = new ValueDO(attribute, object);
			value.setValue(propValue, timestamp);
			// put link to value in property
			attribute.getValues().add(value);
			// put link to bundle of property and value to object's values map
			object.getAttributesValues().put(attribute, value);
		} else {
			value = object.getAttributesValues().get(attribute);
			if (propValue instanceof BinaryData && attribute.getHistorical()){
				em.persist(new HistoricalBinaryValueDO(value));
			}
			value.setValue(propValue, timestamp);
		}

	}

	private void removeValueOf(final ObjectDO object, final AttributeDO attribute, final Object propValue,
	        final long timestamp) throws UnableToConvertPropertyException {
		ValueDO value = null;
		boolean isValueStated = object.getAttributesValues().containsKey(attribute);

		if (!isValueStated) {// nothing to remove , value is not stated
			return;
		}

		value = object.getAttributesValues().get(attribute);
		value.removeValue(propValue, timestamp);
	}

	private ObjectTypeDO getObjectTypeByName(final String typeName) {
		ObjectTypeDO objTypeDO = null;
		try {
			objTypeDO = (ObjectTypeDO) em.createNamedQuery("type.by.names")
			        .setParameter("names", Arrays.asList(typeName)).getSingleResult();
		} catch (NoResultException e) {
		}

		return objTypeDO;
	}

	private ObjectTypeDO getObjectTypeByNameFetchAttributes(final String typeName) {
		List<ObjectTypeDO> types = em
		        .createQuery(
		                "SELECT item FROM ObjectTypeDO item " + "left join fetch item.attributes as attr "
		                        + "left join fetch attr.attributeType as attrType " + "WHERE item.name in (:names) "
		                        + "and item.generic=1", ObjectTypeDO.class).setParameter("names", Arrays.asList(typeName)).getResultList();

		if (types.isEmpty())
			return null;

		// filter duplicate rows because of left join fetch HQL instruction
		return new LinkedHashSet<ObjectTypeDO>(types).iterator().next();
	}

	private ObjectTypeDO getObjectTypeByNameFetchAttributesAndTags(final String typeName) {
		List<ObjectTypeDO> types = em
		        .createQuery(
		                "SELECT item FROM ObjectTypeDO item " + "left join fetch item.attributes as attr "
		                        + "left join fetch attr.attributeType as attrType "
		                        + "left join fetch attr.tags as tag " + "WHERE item.name in (:names) "
		                        + "and item.generic=1", ObjectTypeDO.class).setParameter("names", Arrays.asList(typeName)).getResultList();

		if (types.isEmpty())
			return null;

		// filter duplicate rows because of left join fetch HQL instruction
		return new LinkedHashSet<ObjectTypeDO>(types).iterator().next();
	}

	private ObjectType createObjectTypeTO(final ObjectTypeDO typeDO) {
		ObjectType objTypeTO = new EnvObjTypeTO(typeDO.getName(), "");
		for (Object a : typeDO.getAttributes().values().toArray()) {
			AttributeDO attr = (AttributeDO) a;

			objTypeTO.getPropertyDescriptors().add(attr.getPropertyDescr());

		}
		return objTypeTO;
	}

	private ObjectDO getObjectDO(final TO<?> objId) {
		ObjectDO objectDO = Util.getEntityById(em, ObjectDO.class, objId.getID());
		if (objectDO != null && objectDO.getIdentity().equals(objId)) {
			return objectDO;
		}
		return null;
	}

	private Collection<ObjectDO> getObjectEntitiesByType(final String typeName) {
		Collection<ObjectDO> objects = null;
		objects = em.createNamedQuery("object.by.type", ObjectDO.class).setParameter("name", typeName).getResultList();

		return objects;
	}

	private AttributeTypeDO getAttributeType(final String typeName) {
		AttributeTypeDO attrTypeDO = null;
		try {
			attrTypeDO = (AttributeTypeDO) em.createNamedQuery("attributeType.by.name").setParameter("name", typeName)
			        .getSingleResult();
		} catch (NoResultException e) {

		} catch (NonUniqueResultException e) {
			throw new DALSystemException("DB is in inconsistent state.Attribute type[" + typeName + "] must be unique",
			        e);
		}
		return attrTypeDO;
	}

	private Collection<ValueTO> packHistoricalValuesToValueTOCollection(final Iterator<?> iterator,
	        final String propName, final Class<?> clazz) throws UnableToConvertPropertyException {
		Collection<ValueTO> result = new LinkedList<ValueTO>();
		while (iterator.hasNext()) {
			Object[] row = (Object[]) iterator.next();
			ValueProxy nextHistValue;
			if (clazz != BinaryData.class){
				nextHistValue = new ValueProxy(propName, (Date) row[0], (String) row[1], (Integer) row[2],
				        (Long) row[3], (Double) row[4]);
			}else{
				nextHistValue = new ValueProxy(propName, (Date) row[0], (BinaryData) row[1]);
			}
			
			result.add(nextHistValue.getProxy());
		}
		return result;
	}

	private Map<String, ObjectDO> persistObjects(final EnvObjectBundle objectsToCreate, final int batchSize)
	        throws EnvException {
		Map<String, ObjectDO> result = new HashMap<String, ObjectDO>(1);

		Map<String, ObjectTypeDO> types = new HashMap<String, ObjectTypeDO>(1);
		boolean flushed = false;
		int i = 0;
		for (String label : objectsToCreate.getLabels()) {
			EnvObject envObj = objectsToCreate.get(label);

			ObjectTypeDO ot = types.get(envObj.getTypeName());
			if (ot == null || flushed) {
				ObjectTypeDO persistentType = getObjectTypeByName(envObj.getTypeName());
				if (persistentType == null)
					throw new ObjectNotFoundException("Object type " + envObj.getTypeName() + " does not exist");

				types.put(envObj.getTypeName(), persistentType);
				ot = persistentType;
				flushed = false;
			}

			// create object itself
			ObjectDO obj = new ObjectDO();
			obj.setObjectType(ot);
			obj.setStaticFlag(false);
			em.persist(obj);

			result.put(label, obj);
			flushed = flushAndClearSession(++i, batchSize);
		}
		return result;
	}

	private Map<String, ValueDO> persistRootValues(final EnvObjectBundle objectsToCreate,
	        final Map<String, ObjectDO> objectDOs, final int batchSize) throws EnvException {
		Map<String, ValueDO> result = new HashMap<String, ValueDO>(1);

		Map<String, ObjectTypeDO> types = new HashMap<String, ObjectTypeDO>(1);
		boolean flushed = false;
		int i = 0;
		for (String label : objectsToCreate.getLabels()) {
			EnvObject envObj = objectsToCreate.get(label);

			// create obj_values
			Collection<ValueTO> propertyValues = envObj.getValues();
			// the only non static properties will be set
			if (propertyValues.isEmpty()) {
				continue;
			}

			ObjectDO objectDO = objectDOs.get(label);

			for (ValueTO valueTO : propertyValues) {
				ObjectTypeDO ot = types.get(envObj.getTypeName());
				if (ot == null || flushed) {
					ObjectTypeDO persistentType = getObjectTypeByNameFetchAttributes(envObj.getTypeName());
					if (persistentType == null)
						throw new ObjectNotFoundException("Object type " + envObj.getTypeName() + " does not exist");

					types.put(envObj.getTypeName(), persistentType);
					ot = persistentType;
					flushed = false;
				}

				AttributeDO attribute = ot.getAttributes().get(valueTO.getPropertyName());
				if (attribute == null) {
					throw new PropertyNotFoundException(ot.toString(), valueTO.getPropertyName());
				}

				ValueDO newValueDo = new ValueDO(attribute, objectDO, valueTO.getTimeStamp());
				em.persist(newValueDo);
				result.put(label + ":" + valueTO.getPropertyName(), newValueDo);
				flushed = flushAndClearSession(++i, batchSize);
			}
		}

		return result;
	}

	private void persistTagValues(final EnvObjectBundle objectsToCreate, final Map<String, ObjectDO> objectDOs,
	        final int batchSize) throws EnvException {
		Map<String, ObjectTypeDO> types = new HashMap<String, ObjectTypeDO>(1);
		boolean flushed = false;
		int i = 0;

		for (String label : objectsToCreate.getLabels()) {
			EnvObject envObj = objectsToCreate.get(label);
			// create obj_values
			Collection<Tag> tags = envObj.getTags();
			// the only non static properties will be set
			if (tags.isEmpty()) {
				continue;
			}

			for (Tag tag : tags) {
				ObjectTypeDO ot = types.get(envObj.getTypeName());
				if (ot == null || flushed) {
					ObjectTypeDO persistentType = getObjectTypeByNameFetchAttributesAndTags(envObj.getTypeName());
					if (persistentType == null) {
						throw new ObjectNotFoundException("Object type " + envObj.getTypeName() + " does not exist");
					}
					types.put(envObj.getTypeName(), persistentType);
					ot = persistentType;
					flushed = false;
				}

				AttributeDO attributeDO = ot.getAttributes().get(tag.getPropertyName());
				if (attributeDO == null) {
					throw new PropertyNotFoundException(tag.getPropertyName());
				}

				TagDO tagDO = attributeDO.getTags().get(tag.getTagName());
				if (tagDO == null) {
					throw new TagNotFoundException(tag.getPropertyName(), tag.getTagName());
				}

				String tagName = tag.getTagName();
				String tagValueType = tag.getValue().getClass().getName();
				String tagDescrTypeName = tagDO.getTagDescr().getTypeName();

				if (!tagDescrTypeName.equals(tagValueType)) {
					throw new UnableToConvertTagException(tagName, tagValueType, tagDescrTypeName);
				}

				ObjectDO objectDO = objectDOs.get(label);

				TagValueDO newTagValueDO = new TagValueDO(tag.getValue().toString(), tagDO, objectDO);

				em.persist(newTagValueDO);
				flushed = flushAndClearSession(++i, batchSize);
			}

		}
	}

	private void persistTypedValues(final EnvObjectBundle objectsToCreate, final Map<String, ValueDO> valueDOs,
	        final int batchSize) throws EnvException {
		Map<String, ObjectTypeDO> types = new HashMap<String, ObjectTypeDO>(1);
		boolean flushed = false;
		int i = 0;

		for (String label : objectsToCreate.getLabels()) {
			EnvObject envObj = objectsToCreate.get(label);
			// create obj_values
			Collection<ValueTO> propertyValues = envObj.getValues();
			// the only non static properties will be set
			if (propertyValues.isEmpty()) {
				continue;
			}

			for (ValueTO valueTO : propertyValues) {
				ObjectTypeDO ot = types.get(envObj.getTypeName());
				if (ot == null || flushed) {
					ObjectTypeDO persistentType = getObjectTypeByNameFetchAttributes(envObj.getTypeName());
					if (persistentType == null)
						throw new ObjectNotFoundException("Object type " + envObj.getTypeName() + " does not exist");
					types.put(envObj.getTypeName(), persistentType);
					ot = persistentType;
					flushed = false;
				}

				ValueDO newValueDo = valueDOs.get(label + ":" + valueTO.getPropertyName());

				AttributeDO attributeDO = ot.getAttributes().get(valueTO.getPropertyName());
				if (attributeDO == null) {
					throw new PropertyNotFoundException(ot.toString(), valueTO.getPropertyName());
				}

				String attrTypeName = attributeDO.getAttributeType().getType();
				try {
					Class<?> clazz = Util.classByName(attrTypeName);
					TypedValue typedValue = new TypedValue(clazz);
					typedValue.setValue(valueTO.getValue());
					List<AbstractValue> typedValues = Util.createNewTypedValues(newValueDo, typedValue);
					for (AbstractValue newTypedValue : typedValues) {
						em.persist(newTypedValue);
						flushed = flushAndClearSession(++i, batchSize);
					}
				} catch (ClassNotFoundException e) {
					throw new DALSystemException("Database contains unsupported attribute type " + attrTypeName, e);
				} catch (ClassCastException e) {
					throw new UnableToConvertPropertyException(valueTO.getPropertyName(), e);
				}

			}
		}
	}

	private boolean flushAndClearSession(int index, int batchsize) throws ObjectExistsException {
		if (index % batchsize == 0) {
			try {
				em.flush();
				em.clear();
				return true;
			} catch (PersistenceException e) {
				throw new ObjectExistsException(e.getMessage(), e);
			}
		}
		return false;
	}

	private <REQUESTED_TYPE> void setPropertyValue(final ObjectDO objectDO, final String propName,
	        final REQUESTED_TYPE value) throws PropertyNotFoundException, ObjectNotFoundException,
	        UnableToConvertPropertyException {
		String tempPropertyName = propName;
		Object tempValue = value;
		long tempTimeStamp = 0L;

		// parse ValueTO
		// to single components for future using
		if (tempValue.getClass() == ValueTO.class) {
			tempPropertyName = propName;
			tempTimeStamp = ((ValueTO) value).getTimeStamp();
			tempValue = ((ValueTO) value).getValue();
		}

		// check for existing of property
		checkPropertyExists(objectDO.getObjectType(), tempPropertyName);

		AttributeDO attribute = objectDO.getObjectType().getAttributes().get(tempPropertyName);

		// when trying to set value as Collection but property is not
		// collection
		// bug#1570
		if (tempValue instanceof Collection && !attribute.getCollection()) {
			throw new UnableToConvertPropertyException(attribute.toString(),
			        "Property is not collection and must be set as single value");
		}
		// when trying to set property (collection) by single value
		// bug#1570
		if (!(tempValue instanceof Collection) && attribute.getCollection()) {
			throw new UnableToConvertPropertyException(attribute.toString(),
			        "Property is collection and must be set as collection.");
		}

		setValueOf(objectDO, attribute, tempValue, (tempTimeStamp == 0L) ? System.currentTimeMillis() : tempTimeStamp);
	}

	private void setAllPropertiesValues(final ObjectDO objectDO, final ValueTO[] values)
	        throws ObjectNotFoundException, PropertyNotFoundException, UnableToConvertPropertyException {
		for (ValueTO p : values) {
			// pass ValueTO instance as last parameter for setting
			// updated timestamp for prop
			setPropertyValue(objectDO, p.getPropertyName(), p);
		}
	}

	@Override
	public ObjectType createObjectType(final String name) throws EnvException {
		ObjectTypeDO type = null;
		// create and persist new type
		{
			type = new ObjectTypeDO();
			type.setName(name);
			type.setGeneric(1);
			try {
				em.persist(type);
				em.flush();
			} catch (EntityExistsException e) {
				throw new ObjectExistsException("Type with name[" + name + "] already exists", e);
			} catch (PersistenceException e) {
				throw new EnvException("Error while creating new type " + name, e);
			}

			ObjectDO staticsHolder = new ObjectDO(type.getName(), type, true);
			try {
				type.getObjects().add(staticsHolder);
				em.flush();
			} catch (EntityExistsException e) {
				throw new DALSystemException("Static holder object for type[" + name
				        + "] already exists.It is not possible to create new type properly!", e);
			} catch (PersistenceException e) {
				throw new EnvException("Error while creating static holder object for type " + name, e);
			}
		}

		ObjectType ot = createObjectTypeTO(type);
		if (log.isDebugEnabled()) {
			log.debug("New object type [" + name + "] was successfully created");
		}
		return ot;
	}

	@Override
	public void updateObjectType(final ObjectType type) throws EnvException {
		ObjectTypeDO objTypeDO = getObjectTypeByName(type.getName());
		Util.assertNotNullEntity("Object type :" + type.getName().toString(), objTypeDO);
		// attributes from db
		Map<String, AttributeDO> typeAttributes = objTypeDO.getAttributes();

		StringBuilder sbLog = new StringBuilder();
		if (type.getPropertyDescriptors().size() == 0) {
			// remove all references to ValueDO from ObjectDO
			Collection<ObjectDO> objThatHasValueReference = getObjectEntitiesByType(objTypeDO.getName());
			for (ObjectDO obj : objThatHasValueReference) {
				obj.getAttributesValues().clear();
			}

			for (Object attr : objTypeDO.getAttributes().values()) {
				em.remove(attr);
			}
			typeAttributes.clear();
		} else {
			for (PropertyDescr pd : type.getPropertyDescriptors()) {
				// check existing of property of object type
				AttributeDO attribute = typeAttributes.get(pd.getName());
				if (attribute != null) {
					updateAttributeFromPD(attribute, pd);
					updateTagFromPD(attribute, pd, type);
					continue;
				}
				// create new type's property
				attribute = newAttributeFromPD(pd, objTypeDO);

				typeAttributes.put(attribute.getAttributeName(), attribute);

				sbLog.append("New property [" + pd.getName() + "] of type [" + objTypeDO.getName() + "] was registered");
				sbLog.append(Util.CRLF);
				updateTagFromPD(attribute, pd, type);
			}

			// checks properties which were deleted
			Collection<AttributeDO> existingAttributeDOs = typeAttributes.values();

			Collection<String> attributes2Remove = new HashSet<String>();

			for (AttributeDO a : existingAttributeDOs) {
				PropertyDescr pd = a.getPropertyDescr();
				// if updated type does not contain the property which are
				// persistent(already exist in storage)
				// adds them to the transient properties list
				if (!type.getPropertyDescriptors().contains(pd)) {
					attributes2Remove.add(pd.getName());
				}
			}

			// and removes it
			for (String attributeName : attributes2Remove) {
				AttributeDO attribute2Remove = typeAttributes.get(attributeName);

				// remove attribute from ObjectType
				typeAttributes.remove(attributeName);
				// remove attribute and all its assoc.objects
				em.remove(attribute2Remove);

				sbLog.append("Property [" + attributeName + "] of type [" + objTypeDO.getName() + "] was removed");
				sbLog.append(Util.CRLF);
			}
		}
		// save changes of type's attributes to persistence collection
		objTypeDO.setAttributes(typeAttributes);

		if (log.isDebugEnabled()) {
			sbLog.append("Trying to update object type [" + type.getName() + "]");
			log.debug(sbLog.toString());
		}
	}

	@Override
	public void deleteObjectType(final String typeName) throws EnvException {
		ObjectTypeDO objType = getObjectTypeByName(typeName);
		if (objType == null) {
			log.warn("Object type with NAME [" + typeName + "] does not exist.Nothing to delete");
			return;
		}

		em.remove(objType);

		if (log.isDebugEnabled()) {
			log.debug("Object type [" + typeName + "] was successfully removed");
		}
	}

	@Override
	public String[] getObjectTypes() {
		Collection<String> types = em.createNamedQuery("type.all.name", String.class).getResultList();
		return types.toArray(new String[types.size()]);
	}

	@Override
	public ObjectType[] getObjectTypes(final String[] names) {
		TypedQuery<ObjectTypeDO> q = em.createNamedQuery("type.by.names", ObjectTypeDO.class);

		q.setParameter("names", Arrays.asList(names));

		Collection<ObjectTypeDO> objTypes = q.getResultList();

		Collection<ObjectType> result = new LinkedList<ObjectType>();
		for (ObjectTypeDO type : objTypes) {
			// generates transfer object
			ObjectType objTypeTO = createObjectTypeTO(type);
			result.add(objTypeTO);
		}
		return result.toArray(new ObjectType[result.size()]);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public ObjectType getObjectType(final String name) {
		ObjectType[] ots = getObjectTypes(new String[] { name });
		if (ots.length == 0)
			return null;
		return ots[0];
	}

	@Override
	@Interceptors({ SessionFilterNonStaticOnly.class })
	public TO<?> createObject(final String typeName) throws EnvException {
		return createObject(typeName, null);
	}

	@Override
	@Interceptors({ SessionFilterNonStaticOnly.class })
	public TO<?> createObject(final String typeName, final ValueTO[] propertyValues) throws EnvException {
		EnvObjectBundle singletonBundle = new EnvObjectBundle();
		EnvObject envObject = new EnvObject(typeName, Arrays.asList(propertyValues));
		singletonBundle.addObject("1", envObject);
		TOBundle newTO = createObjects(singletonBundle);
		return newTO.getFirst();
	}

	@Override
	@Interceptors({ SessionFilterNonStaticOnly.class })
	public TOBundle createObjects(EnvObjectBundle objectsToCreate) throws EnvException {
		TOBundle result = new TOBundle();

		if (objectsToCreate.isEmpty())
			return result;

		final int HIBERNATE_JDBC_BATCH_SIZE = 50;
		// persisting data in order , table by table , no mix. increases
		// performance significantly
		// hibernate 3.2 cannot order sql instructions properly
		Map<String, ObjectDO> objectDOs = persistObjects(objectsToCreate, HIBERNATE_JDBC_BATCH_SIZE);

		Map<String, ValueDO> valueDOs = persistRootValues(objectsToCreate, objectDOs, HIBERNATE_JDBC_BATCH_SIZE);
		// if no properties speceified for bundled objects
		if (!valueDOs.isEmpty()) {
			persistTypedValues(objectsToCreate, valueDOs, HIBERNATE_JDBC_BATCH_SIZE);
		}

		persistTagValues(objectsToCreate, objectDOs, HIBERNATE_JDBC_BATCH_SIZE);

		// filling result bundle of TOs
		for (String label : objectsToCreate.getLabels()) {
			result.add(label, objectDOs.get(label).getIdentity());
		}

		return result;
	}

	@Override
	@Interceptors({ SessionFilterNonStaticOnly.class })
	public void deleteObject(final TO<?> objId) throws ObjectNotFoundException {
		ObjectDO objectDO = getObjectDO(objId);
		if (objectDO == null) {
			throw new ObjectNotFoundException(objId);
		}

		em.remove(objectDO);
		if (log.isDebugEnabled()) {
			log.debug("Object [" + objectDO.getObjectType().getName() + "." + objectDO.getName() + "] was deleted");
		}
	}

	@Override
	public void deleteObject(final TO<?> objId, int level) throws ObjectNotFoundException {
		// fake method
		throw new UnsupportedOperationException("This is only stub for Environment method");
	}

	@Override
	@Interceptors({ SessionFilterNonStaticOnly.class })
	public <REQUESTED_TYPE> void setPropertyValue(final TO<?> objId, final String propName, final REQUESTED_TYPE value)
	        throws PropertyNotFoundException, ObjectNotFoundException, UnableToConvertPropertyException {
		ObjectDO objectDO = getObjectDO(objId);
		Util.assertNotNullEntity("Object : " + objId.toString(), objectDO);
		setPropertyValue(objectDO, propName, value);
	}

	@Override
	@Interceptors({ SessionFilterNonStaticOnly.class })
	public <REQUESTED_TYPE> void addPropertyValue(final TO<?> objId, final String propName, final REQUESTED_TYPE value)
	        throws ObjectNotFoundException, PropertyNotFoundException, UnableToConvertPropertyException {
		String tempPropertyName = propName;
		Object tempValue = value;
		long tempTimeStamp = System.currentTimeMillis();

		ObjectDO objectDO = getObjectDO(objId);
		Util.assertNotNullEntity("Object : " + objId.toString(), objectDO);

		// check for property existing
		checkPropertyExists(objectDO.getObjectType(), tempPropertyName);

		AttributeDO attribute = objectDO.getObjectType().getAttributes().get(tempPropertyName);

		// add can be called only for collection
		if (!attribute.getCollection()) {
			throw new UnableToConvertPropertyException(attribute.toString(), "Property is not a collection!");
		}

		if (log.isDebugEnabled())
			log.debug("Adding new value[" + value + "] to collection property[" + objId + "/" + propName + "]");

		// try to set value
		setValueOf(objectDO, attribute, tempValue, tempTimeStamp);

	}

	@Override
	@Interceptors({ SessionFilterNonStaticOnly.class })
	public <REQUESTED_TYPE> void removePropertyValue(final TO<?> objId, final String propName,
	        final REQUESTED_TYPE value) throws ObjectNotFoundException, PropertyNotFoundException,
	        UnableToConvertPropertyException {
		String tempPropertyName = propName;
		Object tempValue = value;
		long tempTimeStamp = System.currentTimeMillis();

		ObjectDO objectDO = getObjectDO(objId);
		Util.assertNotNullEntity("Object : " + objId.toString(), objectDO);

		// check for property existing
		checkPropertyExists(objectDO.getObjectType(), tempPropertyName);

		AttributeDO attribute = objectDO.getObjectType().getAttributes().get(tempPropertyName);

		// add can be called only for collection
		if (!attribute.getCollection()) {
			throw new UnableToConvertPropertyException(attribute.toString(), "Property must be a collection!");
		}

		if (log.isDebugEnabled())
			log.debug("Removing value[" + value + "] from collection property[" + objId + "/" + propName + "]");

		// try to remove value from collection value
		removeValueOf(objectDO, attribute, tempValue, tempTimeStamp);
	}

	@Override
	@Interceptors(SessionFilterNonStaticOnly.class)
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public Collection<TO<?>> getObjectsByType(final String typeName) {
		return getObjects(typeName, ObjectTypeMatchMode.EXACT_MATCH, null);
	}

	@Override
	@Interceptors(SessionFilterNonStaticOnly.class)
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public Collection<TO<?>> getObjectsByType(final String typeName, final ObjectTypeMatchMode matchMode) {
		return getObjects(typeName, matchMode, null);
	}

	@Override
	@Interceptors({ SessionFilterNonStaticOnly.class })
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public Collection<TO<?>> getObjects(final String typeName, final ValueTO[] propertyValues) {
		return getObjects(typeName, ObjectTypeMatchMode.EXACT_MATCH, propertyValues);
	}
	
	@Override
	@Interceptors({ SessionFilterNonStaticOnly.class })
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public Collection<TO<?>> getObjects(QueryDescription queryDescr) { 
		throw new UnsupportedOperationException();
	}

	@Override
	@Interceptors({ SessionFilterNonStaticOnly.class })
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public Collection<TO<?>> getObjects(final String typeName, final ObjectTypeMatchMode matchMode,
	        final ValueTO[] propertyValues) {
		ValueTO[] tempPropValues;
		// correct if null
		if (propertyValues == null) {
			tempPropValues = new ValueTO[] {};
		} else {
			tempPropValues = propertyValues;
		}
		Criterion[] criteria = new Criterion[tempPropValues.length + 1];
		int i = 0;
		for (ValueTO val : tempPropValues) {
			criteria[i] = Criterion.eq(val);
			i++;
		}

		// get typeName and put it into criteria array
		criteria[i] = matchMode.createCriterion(new ValueTO("type", typeName));

		// build sql for searching objects and return Hibernate Query impl.
		org.hibernate.Query q = FindGenericSQL.buildSQL((Session) em.getDelegate(), criteria);

		Collection<TO<?>> result = new LinkedList<TO<?>>();
		Iterator iterator = q.list().iterator();
		while (iterator.hasNext()) {
			Object[] row = (Object[]) iterator.next();
			Integer objectId = (Integer) row[0];
			String type = (String) row[1];
			result.add(new GenericObjectTO(objectId, type));
		}
		return result;
	}

	@Override
	@Interceptors({ SessionFilterNonStaticOnly.class })
	public void setAllPropertiesValues(final TO<?> objId, final ValueTO[] values) throws ObjectNotFoundException,
	        PropertyNotFoundException, UnableToConvertPropertyException {
		ObjectDO objectDO = getObjectDO(objId);
		Util.assertNotNullEntity("Object : " + objId.toString(), objectDO);
		setAllPropertiesValues(objectDO, values);
	}

	@Override
	@Interceptors({ SessionFilterNonStaticOnly.class })
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public Integer getHistorySize(final TO<?> objId, final String propName) throws PropertyNotFoundException,
	        ObjectNotFoundException {
		// all history of attribute with specified id
		Query q = em.createNamedQuery("history.count.object.attribute.all");
		q.setParameter("objId", objId.getID());
		q.setParameter("attrId", propName);
		Integer count = (Integer) q.getSingleResult();
		if (count == 0) {
			q = em.createNamedQuery("history.binary.count.object.attribute.all");
			q.setParameter("objId", objId.getID());
			q.setParameter("attrId", propName);
			count = (Integer) q.getSingleResult();
		}

		return count;
	}

	@Override
	@Interceptors({ SessionFilterNonStaticOnly.class })
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public <RETURN_TYPE> Collection<ValueTO> getHistory(final TO<?> objId, final String propName,
	        final Class<RETURN_TYPE> clazz) throws ObjectNotFoundException, PropertyNotFoundException,
	        UnableToConvertPropertyException {
		// all history of attribute with specified id
		Query q = em.createNamedQuery("history.object.attribute.all");
		q.setParameter("objId", objId.getID());
		q.setParameter("attrId", propName);

		List<?> resultList =  q.getResultList();
		if (resultList.isEmpty() && (clazz == BinaryData.class || clazz == ValueTO.class)){
			q = em.createNamedQuery("history.binary.object.attribute.all");
			q.setParameter("objId", objId.getID());
			q.setParameter("attrId", propName);

			Iterator<?> iterator = q.getResultList().iterator();
			return packHistoricalValuesToValueTOCollection(iterator, propName, BinaryData.class);
		}
		Iterator<?> iterator = resultList.iterator();
		
		return packHistoricalValuesToValueTOCollection(iterator, propName, clazz);
	}

	@Override
	@Interceptors({ SessionFilterNonStaticOnly.class })
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public <RETURN_TYPE> Collection<ValueTO> getHistory(final TO<?> objId, final String propName, final int size,
	        final int start, final Class<RETURN_TYPE> clazz) throws ObjectNotFoundException, PropertyNotFoundException,
	        UnableToConvertPropertyException {
		// all history of attribute with specified id
		Query q = em.createNamedQuery("history.object.attribute.all");
		q.setParameter("objId", objId.getID());
		q.setParameter("attrId", propName);
		q.setFirstResult(start);
		q.setMaxResults(size);
		
		List<?> resultList =  q.getResultList();
		if (resultList.isEmpty() && (clazz == BinaryData.class || clazz == ValueTO.class)){
			q = em.createNamedQuery("history.binary.object.attribute.all");
			q.setParameter("objId", objId.getID());
			q.setParameter("attrId", propName);
			q.setFirstResult(start);
			q.setMaxResults(size);
			
			Iterator<?> iterator = q.getResultList().iterator();
			return packHistoricalValuesToValueTOCollection(iterator, propName, BinaryData.class);
		}
		Iterator<?> iterator = resultList.iterator();
		
		return packHistoricalValuesToValueTOCollection(iterator, propName, clazz);
	}

	
	@Override
	@Interceptors({ SessionFilterNonStaticOnly.class })
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public EnvObjectHistory getHistory(TO<?> objId, Collection<String> propNames, Date start, Date end) {
		
		Set<String> propNamesUniqSet = CollectionUtils.newSetFromCollection(propNames);
		
		Query queryValueIdsByObjectId = em.createNamedQuery("valueId.attrName.by.object");
		queryValueIdsByObjectId.setParameter("objId", objId.getID());
		List<Object[]> resultList = queryValueIdsByObjectId.getResultList();
	
		// makes value_id -> prop.name mapping for performance 
		Map<Integer, String> valueIdToAttrNameMap = CollectionUtils.newMap();
		for (Iterator<Object[]> i = resultList.iterator(); i.hasNext(); ) { 
			Object[] row = i.next();
			String propName = (String)row[1];
			if (propNamesUniqSet.contains(propName)) { 
				valueIdToAttrNameMap.put((Integer)row[0], propName);
			} 
		}
		
		EnvObjectHistory result = new EnvObjectHistory(objId);
		
		if (valueIdToAttrNameMap.isEmpty()) { 
			return result;
		}
		
		Query queryHistory = em.createNamedQuery("history.object.interval");
		queryHistory.setParameter("valueIds", valueIdToAttrNameMap.keySet());
		queryHistory.setParameter("startDate", start);
		queryHistory.setParameter("endDate", end);
		
		// time frame var to split tuples 
		Integer itemSplitter = null;
		// last use is outside the loop
		Date timestamp = null;
		
		List<ValueTO> objectValueTuple = CollectionUtils.newList(); 
		// main loop through historical records, records should be sorted by splitter (in our case it is timestamp)
		for (Iterator<?> iterator = queryHistory.getResultList().iterator(); iterator.hasNext();) {
			Object[] row = (Object[]) iterator.next();
			Integer valueId = (Integer) row[0];
			timestamp = (Date) row[1];
			Integer groupId = (Integer) row[2];
			if (itemSplitter == null) {
				itemSplitter = groupId;
			}
			// prop name from map instead of slow DB join 
			String propName = valueIdToAttrNameMap.get(valueId);
			if (propName == null) {
				throw new DALSystemException(
						"Query : history.object.interval returns wrong data. There is no property mapping for value_id="
				                + valueId);
			}
			
			if (!itemSplitter.equals(groupId)) {
				result.addHistoricalRecord(new HistoricalRecord(objectValueTuple.toArray(new ValueTO[] {}), timestamp
				        .getTime(), itemSplitter));
				itemSplitter = groupId;
				objectValueTuple = CollectionUtils.newList();
			}
			
			ValueProxy nextHistValue = new ValueProxy(propName, timestamp, (String) row[3], (Integer) row[4],
			        (Long) row[5], (Double) row[6]);
			objectValueTuple.add(nextHistValue.getProxy());
		}
		
		if (itemSplitter != null) {
			// add last tuple to result because it is not triggered by splitter
			result.addHistoricalRecord(new HistoricalRecord(objectValueTuple.toArray(new ValueTO[] {}), timestamp.getTime(), itemSplitter));
		}
		
		return result;
	}

	@Override
	@Interceptors({ SessionFilterNonStaticOnly.class })
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public <RETURN_TYPE> Collection<ValueTO> getHistory(final TO<?> objId, final String propName, final Date start,
	        final Date end, final Class<RETURN_TYPE> clazz) throws ObjectNotFoundException, PropertyNotFoundException,
	        UnableToConvertPropertyException {
		// all history of attribute with specified id
		Query q = em.createNamedQuery("history.object.attribute.interval");
		q.setParameter("objId", objId.getID());
		q.setParameter("attrId", propName);
		q.setParameter("startDate", start);
		q.setParameter("endDate", end);

		List<?> resultList =  q.getResultList();
		if (resultList.isEmpty() && (clazz == BinaryData.class || clazz == ValueTO.class)){
			q = em.createNamedQuery("history.binary.object.attribute.interval");
			q.setParameter("objId", objId.getID());
			q.setParameter("attrId", propName);
			q.setParameter("startDate", start);
			q.setParameter("endDate", end);
			
			Iterator<?> iterator = q.getResultList().iterator();
			return packHistoricalValuesToValueTOCollection(iterator, propName, BinaryData.class);
		}
		Iterator<?> iterator = resultList.iterator();
		
		return packHistoricalValuesToValueTOCollection(iterator, propName, clazz);
	}

	@Override
	@Interceptors({ SessionFilterNonStaticOnly.class })
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public <RETURN_TYPE> Collection<ValueTO> getHistory(final TO<?> objId, final String propName, final Date start,
	        final int numberPoints, final Class<RETURN_TYPE> clazz) throws ObjectNotFoundException,
	        PropertyNotFoundException, UnableToConvertPropertyException {
		// all history of attribute with specified id
		Query q = em.createNamedQuery("history.object.attribute.startdate");
		q.setParameter("objId", objId.getID());
		q.setParameter("attrId", propName);
		q.setParameter("startDate", start);
		
		List<?> resultList =  q.setMaxResults(numberPoints).getResultList();
		if (resultList.isEmpty() && (clazz == BinaryData.class || clazz == ValueTO.class)){
			q = em.createNamedQuery("history.binary.object.attribute.startdate");
			q.setParameter("objId", objId.getID());
			q.setParameter("attrId", propName);
			q.setParameter("startDate", start);
			
			Iterator<?> iterator = q.setMaxResults(numberPoints).getResultList().iterator();
			return packHistoricalValuesToValueTOCollection(iterator, propName, BinaryData.class);
		}
		Iterator<?> iterator = resultList.iterator();

		return packHistoricalValuesToValueTOCollection(iterator, propName, clazz);
	}

	@Override
	@Interceptors({ SessionFilterNonStaticOnly.class })
	public void setRelation(final TO<?> parentId, final TO<?> childId) throws ObjectNotFoundException,
	        UnsupportedRelationException {
		setRelation(Arrays.asList(new Relation(parentId, childId)));
	}

	@Override
	@Interceptors({ SessionFilterNonStaticOnly.class })
	public void setRelation(List<Relation> relations) throws UnsupportedRelationException, ObjectNotFoundException {
		modifyRelations(relations, "objects.by.ids", "objects.by.ids.fetchParents", new RelationModifier() {
			@Override
			public void execute(ObjectDO parent, ObjectDO child) {
				child.getParents().add(parent);
			}
		});
	}

	@Override
	@Interceptors({ SessionFilterNonStaticOnly.class })
	public void removeRelation(final TO<?> parentId, final TO<?> childId) throws ObjectNotFoundException {
		removeRelation(Arrays.asList(new Relation(parentId, childId)));
	}

	@Override
	@Interceptors({ SessionFilterNonStaticOnly.class })
	public void removeRelation(List<Relation> relations) throws ObjectNotFoundException {
		modifyRelations(relations, "objects.by.ids.fetchChildren", "objects.by.ids.fetchParents",
		        new RelationModifier() {
			        @Override
			        public void execute(ObjectDO parent, ObjectDO child) {
				        child.getParents().remove(parent);
				        parent.getChilds().remove(child);
			        }
		        });
	}

	/**
	 * Relation modifier to use as closure
	 * 
	 * @author ashabanov
	 * 
	 */
	private interface RelationModifier {
		void execute(ObjectDO parent, ObjectDO child);
	}

	private void modifyRelations(Collection<Relation> relations, String parentsQueryName, String childrenQueryName,
	        RelationModifier modifier) throws ObjectNotFoundException {
		// mapping child to parent
		Map<Integer, Set<Integer>> childToParent = new HashMap<Integer, Set<Integer>>();

		// params for queries
		Set<Integer> parentIds = new HashSet<Integer>();
		Set<Integer> childrenIds = new HashSet<Integer>();

		for (final Relation relation : relations) {
			final Integer parentNativeId = (Integer) relation.getParent().getID();
			parentIds.add(parentNativeId);
			for (TO<?> child : relation.getChildren()) {
				Integer childNativeId = (Integer) child.getID();
				childrenIds.add(childNativeId);
				Set<Integer> childHashedId = childToParent.get(childNativeId);
				if (childHashedId == null) {
					HashSet<Integer> parents = new HashSet<Integer>();
					parents.add(parentNativeId);
					childToParent.put(childNativeId, parents);
				} else {
					childHashedId.add(parentNativeId);
				}
			}
		}

		TypedQuery<ObjectDO> parentsQuery = em.createNamedQuery(parentsQueryName, ObjectDO.class);
		parentsQuery.setParameter("ids", parentIds);
		Set<ObjectDO> parents = new LinkedHashSet<ObjectDO>(parentsQuery.getResultList());
		if (parents.isEmpty()) {
			throw new ObjectNotFoundException("There are no specified parents");
		}
		if (parents.size() != parentIds.size()) {
			List<TO<?>> exceptionInfo = new LinkedList<TO<?>>();
			for (ObjectDO parent : parents) {
				if (!parentIds.contains(parent.getObjectId())) {
					exceptionInfo.add(parent.getIdentity());
				}
			}
			throw new ObjectNotFoundException("There are no such parents : " + exceptionInfo);
		}

		Map<Integer, ObjectDO> parentIdToDO = new HashMap<Integer, ObjectDO>();
		for (ObjectDO objectDO : parents) {
			parentIdToDO.put(objectDO.getObjectId(), objectDO);
		}

		TypedQuery<ObjectDO> childrenQuery = em.createNamedQuery(childrenQueryName, ObjectDO.class);
		childrenQuery.setParameter("ids", childrenIds);
		Set<ObjectDO> children = new LinkedHashSet<ObjectDO>(childrenQuery.getResultList());
		if (children.isEmpty()) {
			throw new ObjectNotFoundException("There are no specified children");
		}
		if (children.size() != childrenIds.size()) {
			List<TO<?>> exceptionInfo = new LinkedList<TO<?>>();
			for (ObjectDO child : children) {
				if (!childrenIds.contains(child.getObjectId())) {
					exceptionInfo.add(child.getIdentity());
				}
			}
			throw new ObjectNotFoundException("There are no such children : " + exceptionInfo);
		}

		for (ObjectDO childDO : children) {
			Integer childNativeId = childDO.getObjectId();
			Set<Integer> parentNativeIds = childToParent.get(childNativeId);
			for (Integer parentNativeId : parentNativeIds) {
				ObjectDO parentDO = parentIdToDO.get(parentNativeId);
				modifier.execute(parentDO, childDO);
			}
		}
	}

	@Override
	@Interceptors({ SessionFilterNonStaticOnly.class })
	public Collection<TO<?>> getParents(final TO<?> objId, final String typeName) throws ObjectNotFoundException {
		ObjectDO objectDO = getObjectDO(objId);
		if (objectDO == null) {
			throw new ObjectNotFoundException(objId);
		}
		Util.assertNotNullEntity("Object : " + objId.toString(), objectDO);

		Collection<TO<?>> result = new LinkedList<TO<?>>();
		for (ObjectDO objDO : objectDO.getParents()) {
			TO<?> id = objDO.getIdentity();
			if (Util.hasType(id, typeName)) {
				result.add(id);
			}
		}
		return result;
	}

	@Override
	@Interceptors({ SessionFilterNonStaticOnly.class })
	public Collection<TO<?>> getParents(final TO<?> objId) throws ObjectNotFoundException {
		return getParents(objId, null);
	}

	@Override
	@Interceptors({ SessionFilterNonStaticOnly.class })
	public Collection<CollectionTO> getChildren(final Collection<TO<?>> objIds, final String typeName) {

		Map<Integer, CollectionTO> itemTable = new HashMap<Integer, CollectionTO>();

		Collection<Integer> rawIds = new ArrayList<Integer>(objIds.size());

		Collection<CollectionTO> result = new ArrayList<CollectionTO>(objIds.size());
		for (TO<?> id : objIds) {
			rawIds.add((Integer) id.getID());
			CollectionTO newItem = new CollectionTO(id);
			// put just created item into result
			itemTable.put((Integer) id.getID(), newItem);
			result.add(newItem);
		}

		Query qryGenericChildren = null;

		// get query factory
		QueryFactory qf = QueryFactory.getInstance();

		// different queries when type == null or type = "something" . used for
		// better performance
		if (typeName == null || typeName.isEmpty()) {
			qryGenericChildren = qf.createSingleParamBatchQuery(em, "object.id.with.genericFirstLevelChild.by.ids",
			        "objects", rawIds, "object.count", "object.id.with.genericFirstLevelChild.all");
		} else {
			Map<String, Object> typeParameter = new HashMap<String, Object>();
			typeParameter.put("type", objIds.iterator().next().getTypeName());
			qryGenericChildren = qf.createSingleParamBatchQuery(em,
			        "object.id.with.genericFirstLevelChild.by.ids.and.type", "objects", rawIds, typeParameter,
			        "object.count.by.type", typeParameter, "object.id.with.genericFirstLevelChild.by.type",
			        typeParameter);
		}

		CollectionTO item = null;
		Collection<?> genericChildren = qryGenericChildren.getResultList();
		Iterator genericChildrenIterator = genericChildren.iterator();
		while (genericChildrenIterator.hasNext()) {
			Object[] row = (Object[]) genericChildrenIterator.next();
			Integer objectId = (Integer) row[0];
			ObjectDO objectDO = (ObjectDO) row[1];
			item = itemTable.get(objectId);
			if (item == null) {
				continue;
			}
			item.addValue(new ValueTO(objectDO.getObjectId().toString(), objectDO.getIdentity()));
		}

		return result;
	}

	@Override
	@Interceptors({ SessionFilterNonStaticOnly.class })
	public Collection<CollectionTO> getChildren(final Collection<TO<?>> objIds) {
		return getChildren(objIds, null);
	}

	@Override
	@Interceptors({ SessionFilterNonStaticOnly.class })
	public Collection<TO<?>> getChildren(final TO<?> objId) throws ObjectNotFoundException {
		return getChildren(objId, null);
	}

	@Override
	@Interceptors({ SessionFilterNonStaticOnly.class })
	public Collection<TO<?>> getChildren(final TO<?> objId, final String typeName) throws ObjectNotFoundException {
		Collection<TO<?>> objIds = new LinkedList<TO<?>>();
		objIds.add(objId);
		Collection<CollectionTO> children = getChildren(objIds, typeName);

		CollectionTO arrayOfChildren = children.iterator().next();

		Collection<TO<?>> result = new LinkedList<TO<?>>();
		for (ValueTO pvto : arrayOfChildren.getPropValues()) {
			result.add((TO<?>) pvto.getValue());
		}
		return result;
	}

	@Override
	@Interceptors({ SessionFilterNonStaticOnly.class })
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public long getPropertyTimestamp(final TO<?> objId, final String propName) throws ObjectNotFoundException,
	        PropertyNotFoundException {
		ObjectDO objectDO = getObjectDO(objId);
		Util.assertNotNullEntity("Object : " + objId.toString(), objectDO);

		checkPropertyExists(objectDO.getObjectType(), propName);

		AttributeDO attribute = objectDO.getObjectType().getAttributes().get(propName);

		ValueDO value = objectDO.getAttributesValues().get(attribute);

		return (value == null) ? 0L : value.getTimestamp();
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public PropertyDescr getPropertyDescriptor(final String typeName, final String propName)
	        throws ObjectNotFoundException, PropertyNotFoundException {
		ObjectTypeDO objTypeDO = getObjectTypeByName(typeName);
		Util.assertNotNullEntity("Object type : " + typeName, objTypeDO);
		checkPropertyExists(objTypeDO, propName);
		AttributeDO attribute = objTypeDO.getAttributes().get(propName);
		return attribute.getPropertyDescr();
	}

	@Override
	@Interceptors({ SessionFilterNonStaticOnly.class })
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public Collection<CollectionTO> getPropertyValue(final Collection<TO<?>> objIds, final String[] propNames) {
		if (objIds == null || objIds.isEmpty()) {
			return null;
		}

		Collection<String> propsList = Arrays.asList(propNames);

		Map<Integer, CollectionTO> itemTable = new HashMap<Integer, CollectionTO>();

		Collection<CollectionTO> result = new ArrayList<CollectionTO>(objIds.size());

		Collection<Integer> rawIds = new ArrayList<Integer>(objIds.size());
		for (TO<?> id : objIds) {
			rawIds.add((Integer) id.getID());
			CollectionTO newItem = new CollectionTO(id);
			// put just created item into result
			itemTable.put((Integer) id.getID(), newItem);
			result.add(newItem);
		}

		QueryFactory qf = QueryFactory.getInstance();
		Query getPropsValues = qf.createSingleParamBatchQuery(em, "value.by.objects.and.attributes", "objects", rawIds,
		        "object.count", "value.by.attributes");
		getPropsValues.setParameter("attributes", propsList);

		Collection<ValueDO> values = getPropsValues.getResultList();

		for (ValueDO val : values) {
			ValueReadOnlyProxy valueProxy = valuePropxyFactory.createPropertyValueProxy(ValueTO.class, val
			        .getAttribute().getAttributeName(), val);
			CollectionTO item = itemTable.get(val.getObject().getObjectId());
			if (item == null) {
				continue;
			}
			item.addValue((ValueTO) valueProxy.getValue());
		}
		return result;
	}

	@Override
	@Interceptors({ SessionFilterNonStaticOnly.class })
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public Collection<CollectionTO> getAllPropertiesValues(final Collection<TO<?>> objIds) {
		if (objIds == null || objIds.isEmpty()) {
			return null;
		}
		// table for locating record regarding to host object
		Map<Integer, CollectionTO> itemTable = new HashMap<Integer, CollectionTO>();
		// function's result
		Collection<CollectionTO> result = new ArrayList<CollectionTO>(objIds.size());
		// fill object location table
		Collection<Integer> rawIds = new ArrayList<Integer>(objIds.size());
		for (TO<?> id : objIds) {
			rawIds.add((Integer) id.getID());
			CollectionTO newItem = new CollectionTO(id);
			// put just created item into result
			itemTable.put((Integer) id.getID(), newItem);
			result.add(newItem);
		}

		Map<String, Object> typeParameter = new HashMap<String, Object>();
		typeParameter.put("type", objIds.iterator().next().getTypeName());

		QueryFactory qf = QueryFactory.getInstance();
		Query getPropsValues = qf.createSingleParamBatchQuery(em, "value.by.objects", "objects", rawIds,
		        "object.count.by.type", typeParameter, "value.all.by.type", typeParameter);
		// retrieve data
		Collection<ValueProxy> items = getPropsValues.getResultList();
		// iterate all tuples
		for (ValueProxy item : items) {
			// if value does not exist , anyway return null
			Object objId = item.getProxyKey();
			// adds value
			CollectionTO tableItem = itemTable.get(objId);
			if (tableItem == null)
				continue;
			// repackaging values as it was done in an old implementation
			// proxy value
			ValueTO originValueTO = item.getProxy();
			// if item does not belong to some collection
			if (!item.isCollection()) {
				// just add new value to CollectionTO
				tableItem.addValue(originValueTO);
				continue;
			}
			// already seeded values with the value.getPropertyName
			List<ValueTO> valuesList = tableItem.getPropValue(originValueTO.getPropertyName());
			// if value does not put into table yet
			if (valuesList == null || valuesList.isEmpty()) {
				// changes value type to collection
				Set collectionValue = new HashSet();
				if (originValueTO.getValue() != null)
					collectionValue.add(originValueTO.getValue());
				// packages collection into value to
				originValueTO.setValue(collectionValue);
				// adds new valueto to result table
				tableItem.addValue(originValueTO);
			} else {
				// gets allready packed valueto to replace its content
				ValueTO packedValue = valuesList.iterator().next();
				// gets native(raw) value from valueto
				Set packedNativeValue = (Set) packedValue.getValue();
				// adds new value to collection
				packedNativeValue.add(originValueTO.getValue());
			}
		}

		return result;
	}

	@Override
	@Interceptors({ SessionFilterNonStaticOnly.class })
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public <RETURN_TYPE> RETURN_TYPE getPropertyValue(final TO<?> objId, final String propName,
	        final Class<RETURN_TYPE> clazz) throws UnableToConvertPropertyException, PropertyNotFoundException,
	        ObjectNotFoundException {
		// creates new collection of one element - objId
		Collection<TO<?>> objectIds = new ArrayList<TO<?>>(1);
		objectIds.add(objId);
		// get one prop.value for onr object
		Collection<CollectionTO> objectProp = getPropertyValue(objectIds, new String[] { propName });

		// gets prop.value from container (got value must be only one)
		Collection<ValueTO> value = objectProp.iterator().next().getPropValues();
		// if no such properties or objects
		if (value == null || value.isEmpty()) {
			return null;
		}
		// get prop.value from collection
		ValueTO pvto = value.iterator().next();

		// cast value to returning type
		RETURN_TYPE result = null;

		// if client needs value as ValueTO
		if (clazz == ValueTO.class) {
			return clazz.cast(pvto);
		}

		try {
			result = clazz.cast(pvto.getValue());
		} catch (ClassCastException e) {
			throw new UnableToConvertPropertyException(propName, clazz, e);
		}

		return result;
	}

	@Override
	@Interceptors({ SessionFilterNonStaticOnly.class })
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public Collection<ValueTO> getAllPropertiesValues(final TO<?> objId) throws ObjectNotFoundException {
		// creates new colleciton of one element - objId
		Collection<TO<?>> objectIds = new ArrayList<TO<?>>(1);
		objectIds.add(objId);
		// get one prop.value for onr object
		Collection<CollectionTO> objectAllProps = getAllPropertiesValues(objectIds);
		// if no prop.values or there is no such property or there is no such
		// object
		if (objectAllProps.isEmpty()) {
			return null;
		}
		// gets prop.value from container
		Collection<ValueTO> value = new LinkedList<ValueTO>(objectAllProps.iterator().next().getPropValues());
		// cast value to returning type
		return value;
	}

	@Override
	@Interceptors({ SessionFilterNonStaticOnly.class })
	public Collection<TO<?>> getRelatedObjects(final TO<?> objId, final String objectType, final boolean isChild) {

		ObjectDO objectDO = getObjectDO(objId);
		if (objectDO == null) {
			return Collections.emptySet();
		}

		Collection<TO<?>> result = new HashSet<TO<?>>();
		// temporary set to store the result of getChildren , getParents methods
		Collection<TO<?>> relatedObjects = new HashSet<TO<?>>();
		if (isChild) {
			try {
				relatedObjects.addAll(this.getChildren(objId));
			} catch (ObjectNotFoundException e) {
				// if object will be deleted during execution of this function
				if (log.isDebugEnabled()) {
					log.debug("Exception in getRelatedObjects(). Seems that object[" + objId
					        + "] has been deleted during method execution");
				}
			}
		} else {
			try {
				relatedObjects.addAll(this.getParents(objId));
			} catch (ObjectNotFoundException e) {
				// if object will be deleted during execution of this function
				if (log.isDebugEnabled()) {
					log.debug("Exception in getRelatedObjects(). Seems that object[" + objId
					        + "] has been deleted during method execution");
				}
			}
		}
		// stores got related objects
		result.addAll(relatedObjects);
		for (TO<?> to : relatedObjects) {
			// recursive calls by hierarchy
			result.addAll(getRelatedObjects(to, objectType, isChild));
		}
		// filter of result collection by object type
		return Util.filterObjectsByTypeName(result, objectType);
	}

	@Override
	@Interceptors({ SessionFilterStaticOnly.class })
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public <RETURN_TYPE> RETURN_TYPE getPropertyValue(final String typeName, final String propName,
	        final Class<RETURN_TYPE> clazz) throws ObjectNotFoundException, PropertyNotFoundException,
	        UnableToConvertPropertyException {
		ObjectDO staticHolder = null;
		try {
			staticHolder = (ObjectDO) em.createNamedQuery("object.staticHolder.by.typeName")
			        .setParameter("name", typeName).getSingleResult();
		} catch (NoResultException e) {
			throw new DALSystemException("There is no static holder object for type : " + typeName);
		}
		return getPropertyValue(staticHolder.getIdentity(), propName, clazz);
	}

	@Override
	@Interceptors({ SessionFilterStaticOnly.class })
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public <REQUESTED_TYPE> void setPropertyValue(final String typeName, final String propName,
	        final REQUESTED_TYPE value) throws ObjectNotFoundException, PropertyNotFoundException,
	        UnableToConvertPropertyException {
		ObjectTypeDO objectTypeDO = getObjectTypeByName(typeName);
		if (objectTypeDO == null) {
			throw new ObjectNotFoundException("Object type : " + typeName + " does not exist");
		}

		ObjectDO staticHolder = null;
		try {
			staticHolder = (ObjectDO) em.createNamedQuery("object.staticHolder.by.typeName")
			        .setParameter("name", typeName).getSingleResult();
		} catch (NoResultException e) {
			throw new DALSystemException("There is no static holder object for type : " + typeName);
		}

		setPropertyValue(staticHolder, propName, value);
	}

	@Override
	public void configurationComplete() throws ServerConfigurationException {
		// log message should be here
	}

	@Override
	public void configurationStart() throws ServerConfigurationException {
		// log message should be here
	}

	@Override
	public void setAllPropertiesValues(final TO<?> objId, final ValueTO[] values, final boolean persist)
	        throws ObjectNotFoundException, PropertyNotFoundException, UnableToConvertPropertyException {
		if (persist)
			setAllPropertiesValues(objId, values);
	}

	@Override
	public void setAllPropertiesValues(TO<?> objId, Collection<ValueTO> values, Collection<Tag> tags)
	        throws EnvException {
		this.setAllPropertiesValues(objId, values.toArray(new ValueTO[] {}));
		this.setAllTagsValues(objId, tags);
	}

	@Override
	public <REQUESTED_TYPE> void setPropertyValue(final TO<?> objId, final String propName, final REQUESTED_TYPE value,
	        final boolean persist) throws PropertyNotFoundException, ObjectNotFoundException,
	        UnableToConvertPropertyException {
		if (persist)
			setPropertyValue(objId, propName, value);
	}

	@Override
	public boolean exists(final TO<?> objId) {
		if (objId == null) {
			throw new IllegalInputParameterException("object is null");
		}
		if (objId.getID() == null) {
			throw new IllegalInputParameterException("object id is null");
		}
		return em.find(ObjectDO.class, objId.getID()) != null;
	}

	@Override
	public TagDescriptor getTagDescriptor(String typeName, String propName, String tagName)
	        throws ObjectNotFoundException, PropertyNotFoundException, TagNotFoundException {
		ObjectTypeDO objTypeDO = getObjectTypeByName(typeName);
		Util.assertNotNullEntity("Object type : " + typeName, objTypeDO);
		checkPropertyExists(objTypeDO, propName);
		AttributeDO attribute = objTypeDO.getAttributes().get(propName);
		TagDO tag = attribute.getTags().get(tagName);
		if (tag == null) {
			throw new TagNotFoundException(typeName, propName, tagName);
		}
		return tag.getTagDescr();
	}

	@Override
	public <RETURN_TYPE> RETURN_TYPE getTagValue(TO<?> objId, String propName, String tagName, Class<RETURN_TYPE> clazz)
	        throws ObjectNotFoundException, PropertyNotFoundException, TagNotFoundException,
	        UnableToConvertTagException {

		TagValueDO resultTagValue = null;
		try {
			Query query = em.createNamedQuery("tag.by.name.and.property.and.object");
			query.setParameter("object", objId.getID());
			query.setParameter("tagName", tagName);
			query.setParameter("attribute", propName);

			resultTagValue = (TagValueDO) query.getSingleResult();

		} catch (NoResultException e) {
			return null;
		} catch (NonUniqueResultException e) {
			throw new DALSystemException("DB is in inconsistent state.Attribute type[" + "] must be unique", e);
		}

		RETURN_TYPE res = null;
		try {
			String value = resultTagValue.getValue();
			res = clazz.cast(TagTypes.getInstance(clazz, value));
		} catch (NumberFormatException en) {
			throw new UnableToConvertTagException(tagName, resultTagValue.getTag().getTagType().getType(),
			        clazz.getName(), en);
		} catch (ClassCastException e) {
			throw new UnableToConvertTagException(tagName, resultTagValue.getTag().getTagType().getType(),
			        clazz.getName(), e);
		}
		return res;
	}

	@Override
	public Collection<Tag> getAllTags(TO<?> objId, String propName) throws ObjectNotFoundException,
	        PropertyNotFoundException {
		List<TagValueDO> tagsValues = null;
		List<Tag> tags = new ArrayList<Tag>();
		TypedQuery<TagValueDO> query = em.createNamedQuery("tags.by.property.and.object", TagValueDO.class);
		query.setParameter("object", objId.getID());
		query.setParameter("attribute", propName);

		tagsValues = query.getResultList();

		for (TagValueDO tagVal : tagsValues) {
			Tag tag = null;
			try {
				tag = new Tag(tagVal.getTag().getAttribute().getAttributeName(), tagVal.getTag().getName(),
				        TagTypes.getInstance(Class.forName(tagVal.getTag().getTagType().getType()), tagVal.getValue()));
			} catch (ClassNotFoundException e) {
				throw new DALSystemException("DB is in inconsistent state. Tag type["
				        + tagVal.getTag().getTagType().getType() + "] is not supported", e);
			}

			tags.add(tag);
		}

		return tags;
	}

	@Override
	public Collection<Tag> getAllTags(TO<?> objId) throws ObjectNotFoundException {
		List<TagValueDO> tagsValues = null;
		List<Tag> tags = new ArrayList<Tag>();

		TypedQuery<TagValueDO> query = em.createNamedQuery("tags.by.object", TagValueDO.class);
		query.setParameter("object", objId.getID());
		tagsValues = query.getResultList();

		for (TagValueDO tagVal : tagsValues) {
			Tag tag = null;
			try {
				tag = new Tag(tagVal.getTag().getAttribute().getAttributeName(), tagVal.getTag().getName(),
				        TagTypes.getInstance(Class.forName(tagVal.getTag().getTagType().getType()), tagVal.getValue()));
			} catch (ClassNotFoundException e) {
				throw new DALSystemException("DB is in inconsistent state. Tag type["
				        + tagVal.getTag().getTagType().getType() + "] is not supported", e);
			}

			tags.add(tag);
		}

		return tags;
	}

	@Override
	public Map<TO<?>, Collection<Tag>> getAllTags(String objectType) {
		List<TagValueDO> tagsValues = null;
		Map<TO<?>, Collection<Tag>> allTags = new HashMap<TO<?>, Collection<Tag>>();

		TypedQuery<TagValueDO> query = em.createNamedQuery("tags.by.object.type", TagValueDO.class);
		query.setParameter("typeName", objectType);
		tagsValues = query.getResultList();

		for (TagValueDO tagVal : tagsValues) {
			Tag tag = null;
			try {
				tag = new Tag(tagVal.getTag().getAttribute().getAttributeName(), tagVal.getTag().getName(),
				        TagTypes.getInstance(Class.forName(tagVal.getTag().getTagType().getType()), tagVal.getValue()));
				TO<?> obj = TOFactory.getInstance().loadTO(objectType + ":" + tagVal.getObject().getObjectId());
				if (!allTags.containsKey(obj)) {
					HashSet<Tag> objectTags = new HashSet<Tag>();
					objectTags.add(tag);
					allTags.put(obj, objectTags);
				} else {
					allTags.get(obj).add(tag);
				}
			} catch (ClassNotFoundException e) {
				throw new DALSystemException("DB is in inconsistent state. Tag type["
				        + tagVal.getTag().getTagType().getType() + "] is not supported", e);
			}
		}
		return allTags;
	}

	@Override
	public Map<TO<?>, Map<String, Collection<Tag>>> getTags(Collection<TO<?>> objIds, List<String> propertyNames,
	        List<String> tagNames) {
		ArrayList<Integer> ids = new ArrayList<Integer>();
		HashMap<Integer, TO<?>> idMap = new HashMap<Integer, TO<?>>();
		Map<TO<?>, Map<String, Collection<Tag>>> result = new HashMap<TO<?>, Map<String, Collection<Tag>>>();
		for (TO<?> obj : objIds) {
			ids.add((Integer) obj.getID());
			idMap.put((Integer) obj.getID(), obj);
		}
		TypedQuery<TagValueDO> query = null;

		if ((propertyNames == null || propertyNames.size() == 0) && (tagNames == null || tagNames.size() == 0)) {
			query = em.createNamedQuery("tags.by.objects", TagValueDO.class);
			query.setParameter("objects", ids);
		} else if (tagNames == null || tagNames.size() == 0) {
			query = em.createNamedQuery("tags.by.objects.and.properties", TagValueDO.class);
			query.setParameter("objects", ids);
			query.setParameter("attributes", propertyNames);
		} else if (propertyNames == null || propertyNames.size() == 0) {
			query = em.createNamedQuery("tags.by.objects.and.names", TagValueDO.class);
			query.setParameter("objects", ids);
			query.setParameter("tagNames", tagNames);
		} else {
			query = em.createNamedQuery("tags.by.objects.and.properties.and.names", TagValueDO.class);
			query.setParameter("objects", ids);
			query.setParameter("attributes", propertyNames);
			query.setParameter("tagNames", tagNames);
		}

		List<TagValueDO> tagsValues = query.getResultList();
		for (TagValueDO tagDO : tagsValues) {
			Tag tag = null;
			try {
				tag = new Tag(tagDO.getTag().getAttribute().getAttributeName(), tagDO.getTag().getName(),
				        TagTypes.getInstance(Class.forName(tagDO.getTag().getTagType().getType()), tagDO.getValue()));
			} catch (ClassNotFoundException e) {
				throw new DALSystemException("DB is in inconsistent state. Tag type["
				        + tagDO.getTag().getTagType().getType() + "] is not supported", e);
			}
			TO<?> obj = idMap.get(tagDO.getObject().getObjectId());
			if (!result.containsKey(obj)) {
				result.put(obj, new HashMap<String, Collection<Tag>>());
			}
			if (!result.get(obj).containsKey(tag.getPropertyName())) {
				result.get(obj).put(tag.getPropertyName(), new ArrayList<Tag>());
			}
			result.get(obj).get(tag.getPropertyName()).add(tag);
		}
		return result;
	}

	@Override
	public void setTagValue(TO<?> objId, String propName, String tagName, Object value)
	        throws PropertyNotFoundException, ObjectNotFoundException, TagNotFoundException,
	        UnableToConvertTagException {

		ObjectDO objectDO = getObjectDO(objId);
		Util.assertNotNullEntity("Object : " + objId.toString(), objectDO);

		// check for existing of property
		checkPropertyExists(objectDO.getObjectType(), propName);

		AttributeDO attribute = objectDO.getObjectType().getAttributes().get(propName);

		// check for existing of tag type.
		checkTagExists(attribute, tagName);

		// set Tag value

		// check Type of value
		TagDO tag = attribute.getTags().get(tagName);
		if (!tag.getTagDescr().getTypeName().equals(value.getClass().getName())) {
			throw new UnableToConvertTagException(tagName, value.getClass().getName(), tag.getTagDescr().getTypeName());
		}

		TagValueDO tagVal = tag.getTagValues().get(objId.getID());
		if (tagVal == null) {
			tagVal = new TagValueDO(value.toString(), tag, objectDO);
			tag.getTagValues().put((Integer) objId.getID(), tagVal);
		} else {
			tagVal.setValue(value.toString());
		}
	}

	@Override
	public void setAllTagsValues(TO<?> objId, Collection<Tag> tagsToSet) throws PropertyNotFoundException,
	        ObjectNotFoundException, TagNotFoundException, UnableToConvertTagException {
		if (tagsToSet == null) {
			throw new IllegalInputParameterException("Supplied 'tagsToSet' is null");
		}
		for (Tag tag : tagsToSet) {
			setTagValue(objId, tag.getPropertyName(), tag.getTagName(), tag.getValue());
		}
	}
}
