package com.synapsense.dal.generic.types;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.StringTokenizer;
import java.util.regex.Pattern;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.type.IntegerType;
import org.hibernate.type.Type;
import org.hibernate.usertype.CompositeUserType;

public class SampleIntervalType implements CompositeUserType {

	private final String INVALID_FORMAT_MSG = "Invalid format: sample interval string should contain number and type delimited by space where type is hr,min or sec";
	private static final int[] TYPES = { Types.INTEGER, Types.INTEGER };

	/**
	 * pattern to validate node sampling interval format
	 */
	private static final Pattern SAMPLING_INTERVAL_PATTERN = Pattern.compile("\\d+\\s((sec)|(min)|(hr))");

	/**
	 * Validates input value to satisfy the internal format
	 * 
	 * @param value
	 *            String to validate
	 * @return true - if given value is correct or false
	 */
	public static boolean validate(String value) {
		return SAMPLING_INTERVAL_PATTERN.matcher(value).matches();
	}

	public int[] sqlTypes() {
		return TYPES;
	}

	public Integer number = null; //
	public Integer type = null; // 0 - sec, 1 - min, 2 - hours

	/**
	 * The class returned by <tt>nullSafeGet()</tt>.
	 * 
	 * @return Class
	 */
	public Class<String> returnedClass() {
		return String.class;
	}

	/**
	 * Compare two instances of the class mapped by this type for persistence
	 * "equality". Equality of the persistent state.
	 * 
	 * @param x
	 * @param y
	 * @return boolean
	 * @throws HibernateException
	 */
	public boolean equals(Object x, Object y) {
		if (x == y)
			return true;
		if (x == null || y == null)
			return false;
		return ((String) x).equals(((String) y));
	}

	/**
	 * Get a hashcode for the instance, consistent with persistence "equality"
	 */
	public int hashCode(Object x) {
		String a = (String) x;
		return a.hashCode();
	}

	/**
	 * Return a deep copy of the persistent state, stopping at entities and at
	 * collections.
	 * 
	 * @param value
	 *            generally a collection element or entity field
	 * @return Object a copy
	 * @throws HibernateException
	 */
	public Object deepCopy(Object x) {
		if (x == null)
			return null;
		String result = new String();
		String input = (String) x;
		result = input;
		return result;
	}

	/**
	 * Check if objects of this type mutable.
	 * 
	 * @return boolean
	 */
	public boolean isMutable() {
		return true;
	}

	/**
	 * Retrieve an instance of the mapped class from a JDBC resultset.
	 * Implementors should handle possibility of null values.
	 * 
	 * @param rs
	 *            a JDBC result set
	 * @param names
	 *            the column names
	 * @param session
	 * @param owner
	 *            the containing entity
	 * @return Object
	 * @throws HibernateException
	 * @throws SQLException
	 */
	@Override
	public Object nullSafeGet(ResultSet rs, String[] names, SessionImplementor session, Object owner)
	        throws HibernateException, SQLException {

		Integer interval = IntegerType.INSTANCE.nullSafeGet(rs, names[0], session);
		Integer units = IntegerType.INSTANCE.nullSafeGet(rs, names[1], session);

		// bug#1583 if some part of value is null then the whole value must be
		// null
		if (interval == null || units == null)
			return null;
		else {
			String num = Integer.toString(interval);
			String type;
			switch (units) {
			case 0:
				type = " sec";
				break;
			case 1:
				type = " min";
				break;
			case 2:
				type = " hr";
				break;
			// bug#1583 if type is not recognized - then returns null
			default:
				return null;
			}
			return num + type;
		}

	}

	/**
	 * Write an instance of the mapped class to a prepared statement.
	 * Implementors should handle possibility of null values. A multi-column
	 * type should be written to parameters starting from <tt>index</tt>.
	 * 
	 * @param st
	 *            a JDBC prepared statement
	 * @param value
	 *            the object to write
	 * @param index
	 *            statement parameter index
	 * @param session
	 * @throws HibernateException
	 * @throws SQLException
	 */
	public void nullSafeSet(PreparedStatement st, Object value, int index, SessionImplementor session)
	        throws HibernateException, SQLException {

		String sampleString = (value == null) ? new String() : (String) value;
		parseSampleString(sampleString);

		IntegerType.INSTANCE.nullSafeSet(st, number, index, session);
		IntegerType.INSTANCE.nullSafeSet(st, type, index + 1, session);
	}

	/**
	 * Get the "property names" that may be used in a query.
	 * 
	 * @return an array of "property names"
	 */
	public String[] getPropertyNames() {
		return new String[] { "number", "type" };
	}

	/**
	 * Get the corresponding "property types".
	 * 
	 * @return an array of Hibernate types
	 */
	public Type[] getPropertyTypes() {
		return new Type[] { IntegerType.INSTANCE, IntegerType.INSTANCE };
	}

	/**
	 * Get the value of a property.
	 * 
	 * @param component
	 *            an instance of class mapped by this "type"
	 * @param property
	 * @return the property value
	 * @throws HibernateException
	 */
	public Object getPropertyValue(Object component, int property) throws HibernateException {

		String sampleString = (component == null) ? new String() : (String) component;
		parseSampleString(sampleString);
		switch (property) {
		case 0:
			return number;
		case 1:
			return type;
		default:
			throw new HibernateException("Invalid property index");
		}
	}

	/**
	 * Set the value of a property.
	 * 
	 * @param component
	 *            an instance of class mapped by this "type"
	 * @param property
	 * @param value
	 *            the value to set
	 * @throws HibernateException
	 */
	public void setPropertyValue(Object component, int property, Object value) throws HibernateException {

		switch (property) {
		case 0:
			number = ((Integer) value).intValue();
			if (number < 0)
				throw new HibernateException("Value can be only positive");
			break;
		case 1:
			int t = ((Integer) value).intValue();
			if ((t >= 0) && (t <= 3)) {
				type = t;
			} else {
				throw new HibernateException("Value can be only 1, 2 or 3");
			}
			break;
		default:
			throw new HibernateException("Invalid property index");
		}
	}

	/**
	 * Reconstruct an object from the cacheable representation. At the very
	 * least this method should perform a deep copy. (optional operation)
	 * 
	 * @param cached
	 *            the object to be cached
	 * @param session
	 * @param owner
	 *            the owner of the cached object
	 * @return a reconstructed object from the cachable representation
	 * @throws HibernateException
	 */
	public Object assemble(Serializable cached, SessionImplementor session, Object owner) {

		return deepCopy(cached);
	}

	/**
	 * Transform the object into its cacheable representation. At the very least
	 * this method should perform a deep copy. That may not be enough for some
	 * implementations, however; for example, associations must be cached as
	 * identifier values. (optional operation)
	 * 
	 * @param value
	 *            the object to be cached
	 * @param session
	 * @return a cachable representation of the object
	 * @throws HibernateException
	 */
	public Serializable disassemble(Object value, SessionImplementor session) {
		return (Serializable) deepCopy(value);
	}

	/**
	 * During merge, replace the existing (target) value in the entity we are
	 * merging to with a new (original) value from the detached entity we are
	 * merging. For immutable objects, or null values, it is safe to simply
	 * return the first parameter. For mutable objects, it is safe to return a
	 * copy of the first parameter. However, since composite user types often
	 * define component values, it might make sense to recursively replace
	 * component values in the target object.
	 * 
	 * @param original
	 * @param target
	 * @param session
	 * @param owner
	 * @return
	 * @throws HibernateException
	 */
	public Object replace(Object original, Object target, SessionImplementor session, Object owner)
	        throws HibernateException {
		return original;
	}

	/**
	 * Converts String to Sample Interval and Sample Interval Units
	 * 
	 * @param sampleString
	 *            String to parse
	 * @throws HibernateException
	 */
	private void parseSampleString(String sampleString) throws HibernateException {
		if (sampleString.isEmpty() == true)
			return;
		StringTokenizer t = new StringTokenizer(sampleString);
		if (t.countTokens() != 2)
			throw new HibernateException(INVALID_FORMAT_MSG);
		try {
			number = Integer.parseInt(t.nextToken());
			if (number < 0) {
				throw new HibernateException(INVALID_FORMAT_MSG);
			}
			String typeToken = t.nextToken();
			if (typeToken.equalsIgnoreCase("hr")) {
				type = 2;
			} else if (typeToken.equalsIgnoreCase("min")) {
				type = 1;
			} else if (typeToken.equalsIgnoreCase("sec")) {
				type = 0;
			} else {
				throw new HibernateException(INVALID_FORMAT_MSG);
			}
		} catch (NumberFormatException e) {
			throw new HibernateException(INVALID_FORMAT_MSG);
		}
	}
}
