package com.synapsense.dal.generic.utils;

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.FlushModeType;
import javax.persistence.LockModeType;
import javax.persistence.Parameter;
import javax.persistence.Query;
import javax.persistence.TemporalType;

/**
 * Batch query implementation.<br>
 * Can be specified by batch fetching strategy {@link BatchQueryStrategy}. Such
 * strategy is applied only for one query parameter which is collection.
 * 
 * @author shabanov
 */
class BatchQueryImpl implements Query {
	private BatchQueryStrategy getResultListStrategy;
	private Query nativeQuery;
	private String paramNameToBatch;
	private Collection<? extends Comparable<?>> paramValue;

	BatchQueryImpl(Query nativeQuery, BatchQueryStrategy getResultListStrategy, String paramNameToBatch,
	        Collection<? extends Comparable<?>> paramValue) {
		this.nativeQuery = nativeQuery;
		this.getResultListStrategy = getResultListStrategy;
		this.paramNameToBatch = paramNameToBatch;
		this.paramValue = paramValue;
	}

	public int executeUpdate() {
		return nativeQuery.executeUpdate();
	}

	public List getResultList() {
		return getResultListStrategy.getResultList(this.nativeQuery, this.paramNameToBatch, this.paramValue);
	}

	public Object getSingleResult() {
		throw new UnsupportedOperationException("");
	}

	public Query setFirstResult(int arg0) {
		return nativeQuery.setFirstResult(arg0);
	}

	public Query setFlushMode(FlushModeType arg0) {
		return nativeQuery.setFlushMode(arg0);
	}

	public Query setHint(String arg0, Object arg1) {
		return nativeQuery.setHint(arg0, arg1);
	}

	public Query setMaxResults(int arg0) {
		return nativeQuery.setMaxResults(arg0);
	}

	public Query setParameter(int arg0, Calendar arg1, TemporalType arg2) {
		throw new UnsupportedOperationException("");
	}

	public Query setParameter(int arg0, Date arg1, TemporalType arg2) {
		throw new UnsupportedOperationException("");
	}

	public Query setParameter(int arg0, Object arg1) {
		throw new UnsupportedOperationException("");
	}

	public Query setParameter(String arg0, Calendar arg1, TemporalType arg2) {
		throw new UnsupportedOperationException("");
	}

	public Query setParameter(String arg0, Date arg1, TemporalType arg2) {
		throw new UnsupportedOperationException("");
	}

	public Query setParameter(String arg0, Object arg1) {
		if (!arg0.equals(this.paramNameToBatch)) {
			return this.nativeQuery.setParameter(arg0, arg1);
		}
		return this;
	}

	@Override
	public int getFirstResult() {
		return nativeQuery.getFirstResult();
	}

	@Override
	public FlushModeType getFlushMode() {
		return nativeQuery.getFlushMode();
	}

	@Override
	public Map<String, Object> getHints() {
		return nativeQuery.getHints();
	}

	@Override
	public LockModeType getLockMode() {
		return nativeQuery.getLockMode();
	}

	@Override
	public int getMaxResults() {
		return nativeQuery.getMaxResults();
	}

	@Override
	public Parameter<?> getParameter(String arg0) {
		throw new UnsupportedOperationException("");
	}

	@Override
	public Parameter<?> getParameter(int arg0) {
		throw new UnsupportedOperationException("");
	}

	@Override
	public <T> Parameter<T> getParameter(String arg0, Class<T> arg1) {
		throw new UnsupportedOperationException("");
	}

	@Override
	public <T> Parameter<T> getParameter(int arg0, Class<T> arg1) {
		throw new UnsupportedOperationException("");
	}

	@Override
	public <T> T getParameterValue(Parameter<T> arg0) {
		throw new UnsupportedOperationException("");
	}

	@Override
	public Object getParameterValue(String arg0) {
		throw new UnsupportedOperationException("");
	}

	@Override
	public Object getParameterValue(int arg0) {
		throw new UnsupportedOperationException("");
	}

	@Override
	public Set<Parameter<?>> getParameters() {
		throw new UnsupportedOperationException("");
	}

	@Override
	public boolean isBound(Parameter<?> arg0) {
		throw new UnsupportedOperationException("");
	}

	@Override
	public Query setLockMode(LockModeType arg0) {
		throw new UnsupportedOperationException("");
	}

	@Override
	public <T> Query setParameter(Parameter<T> arg0, T arg1) {
		throw new UnsupportedOperationException("");
	}

	@Override
	public Query setParameter(Parameter<Calendar> arg0, Calendar arg1,
			TemporalType arg2) {
		throw new UnsupportedOperationException("");
	}

	@Override
	public Query setParameter(Parameter<Date> arg0, Date arg1, TemporalType arg2) {
		throw new UnsupportedOperationException("");
	}

	@Override
	public <T> T unwrap(Class<T> arg0) {
		throw new UnsupportedOperationException("");
	}
}
