package com.synapsense.dal.generic;

/**
 * DAO level runtime exception. Thrown when not recoverable problem occurs.
 * 
 * @author shabanov
 * 
 */
public class DAOGenericException extends RuntimeException {

	public DAOGenericException(String message, Throwable cause) {
		super(message, cause);
	}

	private static final long serialVersionUID = 5252361632595053400L;

}
