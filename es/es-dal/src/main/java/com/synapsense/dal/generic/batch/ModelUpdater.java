package com.synapsense.dal.generic.batch;

import java.util.Collection;

/**
 * The interface of database updater.
 * 
 * @author ashabanov
 * 
 */
public interface ModelUpdater {
	void update(Collection<SingleUpdateUnit> updateUnits);
}
