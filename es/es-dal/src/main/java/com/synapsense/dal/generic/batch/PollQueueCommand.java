package com.synapsense.dal.generic.batch;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

import javax.transaction.UserTransaction;

import org.apache.log4j.Logger;

import com.synapsense.commandprocessor.Commands;
import com.synapsense.commandprocessor.api.Command;
import com.synapsense.commandprocessor.api.CommandEngine;
import com.synapsense.dal.generic.utils.Util;
import com.synapsense.util.executor.Executor;

class PollQueueCommand implements Command {

	private static final Logger logger = Logger.getLogger(PollQueueCommand.class);

	private HashedBlockingQueue queue;

	private Executor executor;

	private int batchSize;

	private int threadsNumber;

	private Lock lock;

	private Condition condition;

	private CommandEngine engine;

	private ModelUpdater updater;

	private long delayWhenEmpty;

	private UserTransaction userTransaction;

	PollQueueCommand(CommandEngine engine, ModelUpdater updater, Executor executor, UserTransaction userTransaction, HashedBlockingQueue queue,
	        int batchSize, int threadsNumber, Lock lock, Condition condition, long delayWhenEmpty) {
		this.engine = engine;
		this.queue = queue;
		this.executor = executor;
		this.batchSize = batchSize;
		this.threadsNumber = threadsNumber;
		this.lock = lock;
		this.condition = condition;
		this.updater = updater;
		this.delayWhenEmpty = delayWhenEmpty;
		this.userTransaction = userTransaction;
	}

	@Override
	public void execute() {

		if (notifyEmptyQueue()) {
			engine.addCommand(Commands.sleep(engine, delayWhenEmpty));
			return;
		}

		if (logger.isDebugEnabled())
			logger.debug("START : batch update of database. current queue size:" + queue.size() + ", batch size:"
			        + batchSize);
		// divide input queue to chunks
		long queueDivideStart = System.currentTimeMillis();
		List<List<SingleUpdateUnit>> sublists = pollAndDivideQueue(queue, threadsNumber * batchSize, batchSize);
		if (logger.isDebugEnabled()) {
			logger.debug("queue poll/divide operation execution time(ms):"
			        + (System.currentTimeMillis() - queueDivideStart));
			logger.debug("Queue was successfully polled out");
		}

		engine.addCommand(CommandFactory.executeBatchTasks(sublists, engine, executor, updater, userTransaction));
	}

	/**
	 * Signal the specified condition to notify waiting thread.
	 * 
	 * @return true if queue is empty , of false
	 */
	private boolean notifyEmptyQueue() {
		// notify if queue is empty
		lock.lock();
		try {
			if (queue.isEmpty()) {
				// signal one thread about queue is empty
				condition.signal();
				return true;
			}
		} finally {
			lock.unlock();
		}

		return false;
	}

	/**
	 * Retrieves elements from queue head and divides them by pieces of size
	 * <code>chunkSize</code>
	 * 
	 * @param queue
	 * @param poolSize
	 * @param chunkSize
	 * @return
	 */
	private List<List<SingleUpdateUnit>> pollAndDivideQueue(HashedBlockingQueue queue, int poolSize, int chunkSize) {
		Collection<SingleUpdateUnit> result = new LinkedList<SingleUpdateUnit>();
		int i = 0;
		while (i < poolSize && !queue.isEmpty()) {
			SingleUpdateUnit su = queue.poll();
			result.add(su);
			i += su.getValues().size();
		}
		return Util.divideByPieces(result, chunkSize);
	}
}
