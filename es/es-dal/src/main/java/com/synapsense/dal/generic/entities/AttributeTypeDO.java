package com.synapsense.dal.generic.entities;

public class AttributeTypeDO implements java.io.Serializable {

	private static final long serialVersionUID = 2340203728625505009L;
	
	private Integer attributeTypeId;
	private String type;

	public AttributeTypeDO() {
	}

	public AttributeTypeDO(String type) {
		this.type = type;
	}

	public Integer getAttributeTypeId() {
		return this.attributeTypeId;
	}

	public void setAttributeTypeId(Integer attributeTypeId) {
		this.attributeTypeId = attributeTypeId;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((attributeTypeId == null) ? 0 : attributeTypeId.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final AttributeTypeDO other = (AttributeTypeDO) obj;
		if (attributeTypeId == null) {
			if (other.attributeTypeId != null)
				return false;
		} else if (!attributeTypeId.equals(other.attributeTypeId))
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		return true;
	}

}
