package com.synapsense.dal.generic.types;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Arrays;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.usertype.UserType;

import com.synapsense.dto.BinaryData;
import com.synapsense.service.impl.dao.to.BinaryDataImpl;

public class BlobToByteArrayType implements UserType {

	private static final int[] TYPES = { Types.VARBINARY };

	@Override
	public int[] sqlTypes() {
		return TYPES;
	}

	/**
	 * The class returned by <tt>nullSafeGet()</tt>.
	 * 
	 * @return Class
	 */
	@Override
	public Class<?> returnedClass() {
		return BinaryData.class;
	}

	/**
	 * Compare two instances of the class mapped by this type for persistence
	 * "equality". Equality of the persistent state.
	 * 
	 * @param x
	 * @param y
	 * @return boolean
	 * @throws HibernateException
	 */
	@Override
	public boolean equals(Object x, Object y) {
		return (x == y)
		        || (x != null && y != null && java.util.Arrays.equals(((BinaryData) x).getValue(),
		                ((BinaryData) y).getValue()));
	}

	/**
	 * Get a hashcode for the instance, consistent with persistence "equality"
	 */
	@Override
	public int hashCode(Object x) {
		return x.hashCode();
	}

	/**
	 * Return a deep copy of the persistent state, stopping at entities and at
	 * collections.
	 * 
	 * @param value
	 *            generally a collection element or entity field
	 * @return Object a copy
	 * @throws HibernateException
	 */
	@Override
	public Object deepCopy(Object x) {
		if (x == null)
			return null;

		BinaryData data = (BinaryData) x;
		return new BinaryDataImpl(Arrays.copyOf(data.getValue(), (data.getValue().length)));
	}

	/**
	 * Check if objects of this type mutable.
	 * 
	 * @return boolean
	 */
	@Override
	public boolean isMutable() {
		return true;
	}

	/**
	 * Retrieve an instance of the mapped class from a JDBC resultset.
	 * Implementors should handle possibility of null values.
	 * 
	 * @param rs
	 *            a JDBC result set
	 * @param names
	 *            the column names
	 * @param owner
	 *            the containing entity
	 * @return Object
	 * @throws HibernateException
	 * @throws SQLException
	 */
	@Override
	public BinaryData nullSafeGet(ResultSet rs, String[] names, SessionImplementor session, Object owner) throws HibernateException, SQLException {
		// field value from storage
		InputStream blob = rs.getBinaryStream(names[0]);
		if (blob == null)
			return null;
		try {
			// if len is too big we can have OutOfMemoryError
			int len = (int) blob.available();
			byte[] buffer = new byte[len];
			blob.read(buffer, 0, len);
			return new BinaryDataImpl(buffer);
		} catch (IOException e) {
			throw new HibernateException("Error reading blob as byte[]", e);
		} finally {
			try {
				blob.close();
			} catch (IOException e) {
				throw new HibernateException("Error closing stream", e);
			}
		}
	}

	/**
	 * Write an instance of the mapped class to a prepared statement.
	 * Implementors should handle possibility of null values. A multi-column
	 * type should be written to parameters starting from <tt>index</tt>.
	 * 
	 * @param st
	 *            a JDBC prepared statement
	 * @param value
	 *            the object to write
	 * @param index
	 *            statement parameter index
	 * @throws HibernateException
	 * @throws SQLException
	 */
	@Override
	public void nullSafeSet(PreparedStatement st, Object value, int index, SessionImplementor session) throws HibernateException, SQLException {
		if (value == null) {
			st.setNull(index, Types.VARBINARY);
			return;
		}

		byte[] tempValue = ((BinaryData) value).getValue();

		st.setBytes(index, tempValue);
	}

	/**
	 * Reconstruct an object from the cacheable representation. At the very
	 * least this method should perform a deep copy. (optional operation)
	 * 
	 * @param cached
	 *            the object to be cached
	 * @param owner
	 *            the owner of the cached object
	 * @return a reconstructed object from the cachable representation
	 * @throws HibernateException
	 */
	@Override
	public Object assemble(Serializable cached, Object owner) {
		return deepCopy(cached);
	}

	/**
	 * Transform the object into its cacheable representation. At the very least
	 * this method should perform a deep copy. That may not be enough for some
	 * implementations, however; for example, associations must be cached as
	 * identifier values. (optional operation)
	 * 
	 * @param value
	 *            the object to be cached
	 * @return a cachable representation of the object
	 * @throws HibernateException
	 */
	@Override
	public Serializable disassemble(Object value) {
		return (Serializable) deepCopy(value);
	}

	/**
	 * During merge, replace the existing (target) value in the entity we are
	 * merging to with a new (original) value from the detached entity we are
	 * merging. For immutable objects, or null values, it is safe to simply
	 * return the first parameter. For mutable objects, it is safe to return a
	 * copy of the first parameter. However, since composite user types often
	 * define component values, it might make sense to recursively replace
	 * component values in the target object.
	 * 
	 * @param original
	 * @param target
	 * @param session
	 * @param owner
	 * @return
	 * @throws HibernateException
	 */
	@Override
	public Object replace(Object original, Object target, Object owner) throws HibernateException {
		return original;
	}

}
