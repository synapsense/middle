package com.synapsense.dal.generic.interceptors;

import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Session;

public class SessionFilterNonStaticOnly {

	@PersistenceContext(unitName = "HibernateDAL")
	public void setEM(EntityManager em) {
		this.em = em;
	}

	private EntityManager em;

	@AroundInvoke
	public Object execute(InvocationContext icx) throws Exception {
		// gets native hibernate session
		// it won't work if delegate is not hibernate.
		Session nativeHibernateSession = (Session) em.getDelegate();
		// enable filtering mechanism with "only non-static entities" filter
		nativeHibernateSession.enableFilter("non_static_entities").setParameter("static", false);
		// proceeds execution
		return icx.proceed();
	}
}
