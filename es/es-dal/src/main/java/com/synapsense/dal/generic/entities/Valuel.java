package com.synapsense.dal.generic.entities;

public class Valuel extends AbstractValue<Long> implements java.io.Serializable {
	private static final long serialVersionUID = -5119735591750729905L;

	private Integer id;
	private ValueDO valueRef;
	private Long value;

	Valuel() {
		super();
	}

	public Valuel(ValueDO valueRef) {
		this(valueRef, null);
	}

	public Valuel(ValueDO valueRef, Object value) {
		this();
		this.valueRef = valueRef;
		if (value == null) {
			return;
		}
		this.value = (Long) value;
	}

	public Integer getId() {
		return this.id;
	}

	private void setId(Integer id) {
		this.id = id;
	}

	public ValueDO getValueRef() {
		return this.valueRef;
	}

	public void setValueRef(ValueDO valueRef) {
		this.valueRef = valueRef;
	}

	public Long getValue() {
		return this.value;
	}

	public void setValue(Long value) {
		this.value = value;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		result = prime * result + ((valueRef == null) ? 0 : valueRef.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final Valuel other = (Valuel) obj;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		if (valueRef == null) {
			if (other.valueRef != null)
				return false;
		} else if (!valueRef.equals(other.valueRef))
			return false;
		return true;
	}

}
