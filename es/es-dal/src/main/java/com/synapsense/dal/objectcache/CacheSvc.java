package com.synapsense.dal.objectcache;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import com.synapsense.dto.CollectionTO;
import com.synapsense.dto.ObjectType;
import com.synapsense.dto.ObjectTypeMatchMode;
import com.synapsense.dto.PropertyDescr;
import com.synapsense.dto.Relation;
import com.synapsense.dto.TO;
import com.synapsense.dto.Tag;
import com.synapsense.dto.TagDescriptor;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.EnvException;
import com.synapsense.exception.IllegalInputParameterException;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.exception.PropertyNotFoundException;
import com.synapsense.exception.TagNotFoundException;
import com.synapsense.exception.UnableToConvertPropertyException;
import com.synapsense.exception.UnableToConvertTagException;
import com.synapsense.search.Queryable;

public interface CacheSvc extends Queryable {

	/**
	 * Allows to get all property values of an object Returns all defined
	 * property values of an object If specified object does not exist then
	 * returns <tt>null</tt>
	 * 
	 * @param objIds
	 *            object IDs
	 * @return values of object properties
	 */
	Collection<CollectionTO> getAllPropertiesValues(Collection<TO<?>> objIds);

	/**
	 * Allows to set values of several object properties in one call
	 * 
	 * @param objId
	 *            object ID
	 * @throws ObjectNotFoundException
	 *             if object with id <code>objId</code> does not exist
	 */
	Collection<ValueTO> getAllPropertiesValues(TO<?> objId) throws ObjectNotFoundException;

	/**
	 * Returns <tt>IDs</tt> of all objects of specified type<br>
	 * If there are no objects of the specified type then returns empty
	 * <code>Collection</code>
	 * 
	 * @param typeName
	 *            type name of objects to be returned
	 * @return Collection of object <tt>ID</tt>s
	 */
	Collection<TO<?>> getObjectsByType(String typeName);

	/**
	 * Returns <tt>IDs</tt> of all objects of the specified type<br>
	 * and of types which are match to the <code>typeName</code> by the
	 * specified <code>matchMode</code>. If there are no objects of the
	 * specified type then returns empty <code>Collection</code>
	 * 
	 * @param typeName
	 *            type name of objects to be returned
	 * @param matchMode
	 *            Object type match mode
	 * @return Collection of object <tt>ID</tt>s
	 */
	Collection<TO<?>> getObjectsByType(String typeName, ObjectTypeMatchMode matchMode);

	/**
	 * Allows to get a bundle of objects property values
	 * <p>
	 * The method is more effective when it's necessary to get concrete
	 * properties of several objects. Can return objects properties values of
	 * different types.
	 * <p>
	 * If <code>propNames</code> is <tt>null</tt> then all prop.values would be
	 * returned
	 * 
	 * @param objIds
	 *            Collection of TO
	 * @param propNames
	 *            Array of properties names
	 * @return Collection of <code>PropertyValueBundleTO</code>
	 */
	Collection<CollectionTO> getPropertyValue(Collection<TO<?>> objIds, String[] propNames);

	/**
	 * Allows to get a value of specified object's property
	 * 
	 * @param objId
	 *            object ID
	 * @param propName
	 *            property name
	 * @param clazz
	 *            expected type of requested property value
	 * @return property value if property exists or null
	 * @throws ObjectNotFoundException
	 *             if object with specified ID does not exist
	 * @throws PropertyNotFoundException
	 *             if property with specified name does not exist
	 * @throws UnableToConvertPropertyException
	 *             if property value cannot be casted to the specified class
	 */
	<RETURN_TYPE> RETURN_TYPE getPropertyValue(TO<?> objId, String propName, Class<RETURN_TYPE> clazz)
	        throws ObjectNotFoundException, PropertyNotFoundException, UnableToConvertPropertyException;

	/**
	 * Allows to request IDs of objects with matching property values<br>
	 * If there are no objects matching criteria then returns empty
	 * <code>Collection</code>
	 * 
	 * @param typeName
	 *            type name of objects to be returned
	 * @param propertyValues
	 *            criteria to find objects
	 * @return IDs of objects matching criteria
	 */
	Collection<TO<?>> getObjects(String typeName, ValueTO[] propertyValues);

	/**
	 * Allows to request IDs of objects with matching property values.<br>
	 * If there are no objects matching criteria then returns empty
	 * <code>Collection</code>
	 * 
	 * @param typeName
	 *            type name of objects to be returned
	 * @param propertyValues
	 *            criteria to find objects
	 * @param matchMode
	 *            type name math mode <code>ObjectTypeMatchMode</code>
	 * @return IDs of objects matching criteria
	 */
	Collection<TO<?>> getObjects(String typeName, ObjectTypeMatchMode matchMode, ValueTO[] propertyValues);

	/**
	 * Gets cache status
	 * 
	 * @return status of cacher
	 */
	int getStatus();

	/**
	 * Creates new object type with specified name.
	 * 
	 * @param name
	 *            new type name
	 */
	void createObjectType(ObjectType name);

	/**
	 * Updates object type. If object type is one of predefined types then
	 * nothing will be done
	 * 
	 * @param objType
	 *            Object type to update
	 */
	void updateObjectType(ObjectType objType);

	/**
	 * Deletes object type with specified name. Also deletes all objects of
	 * specified type (cascade deleting)
	 * 
	 * @param typeName
	 *            name of object type to delete
	 */
	void deleteObjectType(String typeName);

	/**
	 * Creates new object of specified type and sets its properties<br>
	 * Can create objects of predefined types, too.<br>
	 * <p>
	 * To create a new object it is necessary to define all properties which are
	 * mandatory for the specified object type.
	 * 
	 * @param id
	 *            of created object
	 * @param propertyValues
	 *            initial properties values
	 * @param notify
	 *            to send "create object" notification or not
	 * @throws EnvException
	 * 
	 */
	void createObject(TO<?> id, ValueTO[] propertyValues, boolean notify) throws EnvException;

	/**
	 * Deletes object and all links to related objects Clears all links from
	 * other objects to deleted object
	 * 
	 * @param objId
	 *            ID of object to delete
	 */
	void deleteObject(TO<?> objId);

	/**
	 * Allows to set values of several object properties in one call
	 * 
	 * @param objId
	 *            object ID
	 * @param values
	 *            values of properties to set
	 * @throws ObjectNotFoundException
	 * @throws PropertyNotFoundException
	 * @throws UnableToConvertPropertyException
     * @throws InvalidValueException
	 */
	void setAllPropertiesValues(TO<?> objId, ValueTO[] values) throws ObjectNotFoundException,
	        PropertyNotFoundException, UnableToConvertPropertyException;

	void setAllPropertiesValues(TO<?> objId, ValueTO[] values, boolean notify) throws ObjectNotFoundException,
	        PropertyNotFoundException, UnableToConvertPropertyException;

	/**
	 * Static property setter. Allows to set static properties values of the
	 * specified type.
	 * 
	 * @param objId
	 *            ID of object
	 * @param propName
	 *            name of the property to be set
	 * @param value
	 *            new property value
	 * @throws UnableToConvertPropertyException
     * @throws InvalidValueException
	 */
	<REQUESTED_TYPE> void setPropertyValue(TO<?> objId, String propName, REQUESTED_TYPE value)
	        throws ObjectNotFoundException, PropertyNotFoundException, UnableToConvertPropertyException;

	void setRelation(TO<?> parentId, TO<?> childId);

	void removeRelation(TO<?> parentId, TO<?> childId);

	Collection<TO<?>> getChildren(TO<?> objId, String typeName) throws ObjectNotFoundException;

	/**
	 * Returns all children of an object (from one level down by hierarchy) or
	 * empty collection if object has no children<br>
	 * All objects will be returned regardless of object type
	 * 
	 * @param objIds
	 *            ID of object
	 * @return CCollection of Object IDs
	 *
	 */
	Collection<CollectionTO> getChildren(Collection<TO<?>> objIds, String typeName);

	/**
	 * Returns all parents of specified object. Only objects with specified
	 * object type or empty collection will be returned if object has no
	 * parents.
	 * 
	 * @param objId
	 *            ID of object
	 * @param typeName
	 *            filter result by type name
	 * @return Collection of Object IDs as TO
	 * @throws ObjectNotFoundException
	 */
	Collection<TO<?>> getParents(TO<?> objId, String typeName) throws ObjectNotFoundException;

	/**
	 * Allows to get object's children or parents. Returns all objects from
	 * hierarchy. Result can be filtered by type name of hierarchical objects.
	 * 
	 * @param objId
	 *            ID of object
	 * @param objectType
	 *            filter by object type
	 * @param isChild
	 *            defines what to return - parents or children
	 * @return Collection of TO which are related to the specified object from
	 *         all layers of hierarchy
	 * @throws IllegalInputParameterException
	 *             if objId is null or objectType is null or empty
	 */
	Collection<TO<?>> getRelatedObjects(TO<?> objId, String objectType, boolean isChild);

	/**
	 * Returns object's children or parents. Performs only specified number of
	 * hierarchy levels.
	 * 
	 * @param objId
	 *            ID of object
	 * @param level
	 *            How many levels of hierarchy to perform. 0 means to perform
	 *            all levels.
	 * 
	 * @param isChild
	 *            defines what to return - parents or children Collection of TO
	 *            which are related to the specified object from specfied number
	 *            of layers of hierarchy
	 * @throws IllegalInputParameterException
	 *             if objId is null or objectType is null or empty
	 */
	Collection<TO<?>> getRelatedObjects(TO<?> objId, int level, boolean isChild);

	/**
	 * Static property setter. Allows to set static properties values of the
	 * specified type.
	 * 
	 * @param type
	 *            name of type
	 * @param propName
	 *            name of the property to be set
	 * @param value
	 *            new property value
	 * @throws ObjectNotFoundException
	 *             if object with specified ID does not exist
	 * @throws PropertyNotFoundException
	 *             if property with specified name does not exist
	 * @throws UnableToConvertPropertyException
	 *             if property value class cannot be casted to stored class
     * @throws InvalidValueException
	 */
	<REQUESTED_TYPE> void setPropertyValue(String type, String propName, REQUESTED_TYPE value)
	        throws ObjectNotFoundException, PropertyNotFoundException, UnableToConvertPropertyException;

	/**
	 * Static property getter. Allows to get static properties values of the
	 * specified type. If specified type has no static properties the
	 * <tt>null</tt> will be returned
	 * 
	 * @param type
	 *            name of type
	 * @param propName
	 *            property name
	 * @param clazz
	 *            expected type of requested property value
	 * @return property value if property exists or null
	 * @throws ObjectNotFoundException
	 *             if object with specified ID does not exist
	 * @throws PropertyNotFoundException
	 *             if property with specified name does not exist
	 * @throws UnableToConvertPropertyException
	 *             if property value cannot be casted to the specified class
	 */
	<REQUESTED_TYPE> REQUESTED_TYPE getPropertyValue(String type, String propName, Class<REQUESTED_TYPE> clazz)
	        throws ObjectNotFoundException, PropertyNotFoundException, UnableToConvertPropertyException;

	/**
	 * Get <code>PropertyDescr</code> of specified property
	 * 
	 * @param typeName
	 *            Object type name
	 * @param propName
	 *            Property name
	 * @return PropertyDescr descriptor of requested property
	 * @throws ObjectNotFoundException
	 *             if the specified object type does not exist
	 * @throws PropertyNotFoundException
	 *             if the specified property does not exist
	 * @throws IllegalInputParameterException
	 *             if typeName is null or empty or if propName is null or empty
	 */
	PropertyDescr getPropertyDescriptor(String typeName, String propName) throws ObjectNotFoundException,
	        PropertyNotFoundException;

	/**
	 * Allows to query existing object types by their names.
	 * <p>
	 * If type with a name specified in the <code>names</code> does not exist,
	 * the name is ignored. Thus returned array may contain less entries than
	 * the <code>names</code> array.
	 * 
	 * @param names
	 *            type names array
	 * @return <tt>ObjectType</tt>s with specified names or empty array
	 * 
	 * @throws IllegalInputParameterException
	 *             if names is null or empty
	 */
	ObjectType[] getObjectTypes(String[] names);

	/**
	 * Allows to get names of all existing object types.
	 * 
	 * @return names of existing object types
	 */
	String[] getObjectTypes();

	/**
	 * check if object with objId exists in cache
	 * 
	 * @param objId  Id of object
	 * 
	 * @return true if object with objId exists in cache
	 * @throws IllegalInputParameterException
	 *             if objId has illegal values or is null.
	 */
	boolean exists(TO<?> objId) throws IllegalInputParameterException;

	/**
	 * Get <code>TagDescriptor</code> of specified property tag
	 * 
	 * @param typeName
	 *            Object type name
	 * @param propName
	 *            Property name
	 * @param tagName
	 *            Tag name
	 * @return TagDescriptor descriptor of requested property tag
	 * @throws ObjectNotFoundException
	 *             if the specified object type does not exist
	 * @throws PropertyNotFoundException
	 *             if the specified property does not exist
	 * @throws TagNotFoundException
	 *             if the specified property tag does not exist
	 * @throws IllegalInputParameterException
	 *             if object type isn't Generic or if typeName is null or empty
	 *             or if propName is null or empty or if tagName is null or
	 *             empty
	 */
	TagDescriptor getTagDescriptor(String typeName, String propName, String tagName) throws ObjectNotFoundException,
	        PropertyNotFoundException, TagNotFoundException;

	/**
	 * Allows to get a value of specified object's property tag
	 * 
	 * @param objId
	 *            object ID
	 * @param propName
	 *            property name
	 * @param tagName
	 *            Property Tag name
	 * @param clazz
	 *            expected type of requested property tag
	 * @return property tag value or null if tag doesn't contain value
	 * @throws ObjectNotFoundException
	 *             if object with specified ID does not exist
	 * @throws PropertyNotFoundException
	 *             if property with specified name does not exist
	 * @throws TagNotFoundException
	 *             if the specified property tag does not exist
	 * @throws UnableToConvertTagException
	 *             if property tag value cannot be casted to the specified class
	 * @throws IllegalInputParameterException
	 *             if object type isn't Generic or if objId is null or if
	 *             propName is null or empty or if tagName is null or empty or
	 *             if clazz is null
	 */
	<RETURN_TYPE> RETURN_TYPE getTagValue(TO<?> objId, String propName, String tagName, Class<RETURN_TYPE> clazz)
	        throws ObjectNotFoundException, PropertyNotFoundException, TagNotFoundException,
	        UnableToConvertTagException;

	/**
	 * Allows to get all property tags of specified object's property.
	 * 
	 * @param objId
	 *            object ID
	 * @param propName
	 *            property name
	 * @return collection of property tags, or empty collection if nothing was
	 *         found
	 * @throws ObjectNotFoundException
	 *             if specified object does not exist
	 * @throws PropertyNotFoundException
	 *             if property with specified name does not exist
	 * @throws IllegalInputParameterException
	 *             if object type isn't Generic or if objId is null or empty or
	 *             if propName is null or empty
	 */
	Collection<Tag> getAllTags(TO<?> objId, String propName) throws ObjectNotFoundException, PropertyNotFoundException;

	/**
	 * Allows to get all tags of all properties of specified object.
	 * 
	 * @param objId
	 *            object ID
	 * @return collection of property tags or empty collection if nothing was
	 *         found
	 * @throws ObjectNotFoundException
	 *             if specified object does not exist
	 * @throws IllegalInputParameterException
	 *             if object type isn't Generic or if objId is null or empty
	 */
	Collection<Tag> getAllTags(TO<?> objId) throws ObjectNotFoundException;

	/**
	 * Allows to get all tags of all properties of all specified type objects.
	 * 
	 * @param typeName
	 *            type name
	 * @return Map of property tags or empty Map if nothing was found.
	 * @throws IllegalInputParameterException
	 *             if object type isn't Generic or if typeName is null or
	 *             typeName is empty
	 */
	Map<TO<?>, Collection<Tag>> getAllTags(String typeName);

/**
	 * Allows to get tags of specified names of specified properties  of specified type objects.
	 * 
	 * @param objIds
	 *            Collection of object ids whose property tags needs
	 * @param propertyNames
	 *            List names of properties of ids onjects whose property tags needs, 
	 *            		if list is empty or null then tags with 'TagNames' names of all properties are searched 
	 * @param tagNames
	 *            List names of needed tags.
	 *            		if list is empty or null then all tags are searched
	 * @return Map of property tags or empty collection if nothing was found.
	 * 			Map<'object id', Map<Property Name, Collection<Tag>>>
	 * @throws IllegalInputParameterException
	 * 			   if even if one object type in objIds isn't Generic or 
	 *             if objIds is null or objIds is empty
	 */
	Map<TO<?>, Map<String, Collection<Tag>>> getTags(Collection<TO<?>> objIds, List<String> propertyNames,
	        List<String> tagNames);

	/**
	 * Allows to set property tag value
	 * 
	 * @param objId
	 *            object ID
	 * @param propName
	 *            name of the property to be set
	 * @param tagName
	 *            Property Tag name
	 * @param value
	 *            new property tag value
	 * @throws ObjectNotFoundException
	 *             if object with specified ID does not exist
	 * @throws PropertyNotFoundException
	 *             if property with specified name does not exist
	 * @throws TagNotFoundException
	 *             if the specified property tag does not exist
	 * @throws IllegalInputParameterException
	 *             if object type is not Generic or if objId is null or if
	 *             propName is null or empty or if value is null or if tagName
	 *             is null or empty
	 */
	void setTagValue(TO<?> objId, String propName, String tagName, Object value) throws PropertyNotFoundException,
	        ObjectNotFoundException, TagNotFoundException, UnableToConvertTagException;

	void setRelation(List<Relation> relations);

	void removeRelation(List<Relation> relations);

}
