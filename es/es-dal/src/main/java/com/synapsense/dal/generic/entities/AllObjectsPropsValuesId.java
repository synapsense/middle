package com.synapsense.dal.generic.entities;

public class AllObjectsPropsValuesId implements java.io.Serializable {

	private static final long serialVersionUID = 330251299078457640L;

	private String propname;
	private String objectName;
	private String typeName;
	private Integer valId;
	private int objectId;
	private int typeId;
	// private byte[] val;
	private String val;

	public AllObjectsPropsValuesId() {
	}

	public AllObjectsPropsValuesId(String propname, String objectName, String typeName, Integer valId, int objectId,
	        int typeId, String val) {
		this.propname = propname;
		this.objectName = objectName;
		this.typeName = typeName;
		this.valId = valId;
		this.objectId = objectId;
		this.typeId = typeId;
		this.val = val;
	}

	public String getPropname() {
		return this.propname;
	}

	public void setPropname(String propname) {
		this.propname = propname;
	}

	public String getObjectName() {
		return this.objectName;
	}

	public void setObjectName(String objectName) {
		this.objectName = objectName;
	}

	public String getTypeName() {
		return this.typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public Integer getValId() {
		return this.valId;
	}

	public void setValId(Integer valId) {
		this.valId = valId;
	}

	public int getObjectId() {
		return this.objectId;
	}

	public void setObjectId(int objectId) {
		this.objectId = objectId;
	}

	public int getTypeId() {
		return this.typeId;
	}

	public void setTypeId(int typeId) {
		this.typeId = typeId;
	}

	public String getVal() {
		return this.val;
	}

	public void setVal(String val) {
		this.val = val;
	}

}
