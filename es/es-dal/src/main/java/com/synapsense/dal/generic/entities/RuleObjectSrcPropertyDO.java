package com.synapsense.dal.generic.entities;

// Generated 16.04.2008 17:37:47 by Hibernate Tools 3.2.0.CR1

/**
 * RuleObjectSrcPropertyDO generated by hbm2java
 */
public class RuleObjectSrcPropertyDO implements java.io.Serializable {

	private static final long serialVersionUID = 1692848486928803732L;

	private RuleObjectSrcPropertyId id;
	private RuleDO rule;
	private AttributeDO attribute;
	private ObjectDO object;
	private Short genericSrc;
	private String binding;

	public RuleObjectSrcPropertyDO() {
	}

	public RuleObjectSrcPropertyDO(RuleObjectSrcPropertyId id) {
		this(id, null, null, null);
	}

	public RuleObjectSrcPropertyDO(RuleObjectSrcPropertyId id, RuleDO rule, AttributeDO attribute, ObjectDO object) {
		this(id, rule, attribute, object, null, null);
	}

	public RuleObjectSrcPropertyDO(RuleObjectSrcPropertyId id, RuleDO rule, AttributeDO attribute, ObjectDO object,
	        Short genericSrc, String binding) {
		this.id = id;
		this.rule = rule;
		this.attribute = attribute;
		this.object = object;
		this.genericSrc = genericSrc;
		this.binding = binding;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final RuleObjectSrcPropertyDO other = (RuleObjectSrcPropertyDO) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public RuleObjectSrcPropertyId getId() {
		return this.id;
	}

	public void setId(RuleObjectSrcPropertyId id) {
		this.id = id;
	}

	public RuleDO getRule() {
		return this.rule;
	}

	public void setRule(RuleDO rule) {
		this.rule = rule;
	}

	public AttributeDO getAttribute() {
		return this.attribute;
	}

	public void setAttribute(AttributeDO attribute) {
		this.attribute = attribute;
	}

	public ObjectDO getObject() {
		return this.object;
	}

	public void setObject(ObjectDO object) {
		this.object = object;
	}

	public String getBinding() {
		return binding;
	}

	public void setBinding(String binding) {
		this.binding = binding;
	}

	public Short getGenericSrc() {
		return genericSrc;
	}

}
