/**
 * 
 */
package com.synapsense.dal.objectcache;

import com.google.common.collect.Multimap;
import com.synapsense.dto.*;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.exception.PropertyNotFoundException;
import com.synapsense.exception.UnableToConvertPropertyException;
import com.synapsense.exception.UnableToConvertValueException;
import com.synapsense.service.impl.dao.to.EnvObjTypeImmutable;
import com.synapsense.service.nsvc.events.ObjectTypeUpdatedEvent;
import com.synapsense.util.CollectionUtils;
import com.synapsense.util.Pair;

import java.io.Serializable;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author Alexander Borisov
 * 
 */
public class SimpleType {
	// Key - type name Value - Simple type
	private static ConcurrentHashMap<String, SimpleType> typeCache = new ConcurrentHashMap<String, SimpleType>();

	static final String NUM_ALERTS_PROPERTY_NAME = "numAlerts";

	/**
	 * Name of type
	 */
	private String name;
	/**
	 * Name of key for accessing to type
	 */
	private String keyName;
	/**
	 * Environment type reference
	 */
	private ObjectType envType;
	/**
	 * Set of props
	 */
	private Map<String, PropertyType> props;
	/**
	 * Hashed map of objects;
	 */
	private Map<Integer, SimpleObject> objs;
	/**
	 * static properties references
	 */
	private Map<String, SimplePropertyValue> staticProperties;

	private Map<String, Multimap<Object, Integer>> indexes = new ConcurrentHashMap<String, Multimap<Object, Integer>>(0);

	private IndexProvider indexProvider;

	private SimpleType(final ObjectType type, IndexProvider indexProvider) {
		props = new ConcurrentHashMap<String, PropertyType>();
		objs = new ConcurrentHashMap<Integer, SimpleObject>();
		staticProperties = new ConcurrentHashMap<String, SimplePropertyValue>();
		props.put(NUM_ALERTS_PROPERTY_NAME, new PropertyType(PropType.NORMAL, Integer.class));

		String name = type.getName();
		this.name = name;
		this.keyName = name.toUpperCase();
		this.indexProvider = indexProvider;

		this.setEnvType(type);

		for (PropertyDescr pd : type.getPropertyDescriptors()) {
			addProperty(pd.getName(), new PropertyType(pd));
		}
	}

	static class PropertyType implements Serializable {

		private static final long serialVersionUID = -8057483034476344451L;

		private PropType type;
		private Class<?> clazz;

		public PropertyType(final PropType type, final Class<?> clazz) {
			this.type = type;
			this.clazz = clazz;
		}

		public PropertyType(final PropertyDescr pd) {
			this(PropType.NORMAL, pd.getType());
			if (pd.isCollection()) {
				this.type = PropType.COLLECTION;
			}
			if (pd.isStatic()) {
				this.type = PropType.STATIC;
			}
			if (pd.isHistorical()) {
				this.type = PropType.HISTORICAL;
			}
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((clazz == null) ? 0 : clazz.hashCode());
			result = prime * result + ((type == null) ? 0 : type.hashCode());
			return result;
		}

		@Override
		public boolean equals(final Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			final PropertyType other = (PropertyType) obj;
			if (clazz == null) {
				if (other.clazz != null)
					return false;
			} else if (!clazz.equals(other.clazz))
				return false;
			if (type == null) {
				if (other.type != null)
					return false;
			} else if (!type.equals(other.type))
				return false;
			return true;
		}

		public PropType getType() {
			return type;
		}

		public void setType(final PropType type) {
			this.type = type;
		}

		public Class<?> getClazz() {
			return clazz;
		}

		public void setClazz(final Class<?> clazz) {
			this.clazz = clazz;
		}

		@Override
		public String toString() {
			StringBuilder sb = new StringBuilder(50);
			sb.append("Type=[");
			sb.append(type);
			sb.append("] Class=[");
			sb.append(clazz);
			sb.append("]\n");
			return sb.toString();
		}
	}

	static enum PropType {
	NORMAL, STATIC, HISTORICAL, COLLECTION
	}

	private static void putTypeToCache(final SimpleType type) {
		typeCache.put(type.keyName, type);
	}

	/**
	 * Factory method to create <code>SimpleType</code> instances
	 * 
	 * @param type
	 *            Env type to use as source
	 * 
	 * @return New SimpleType instance
	 */
	public static SimpleType newType(final ObjectType type, IndexProvider indexProvider) {
		SimpleType newSimpleType;
		if ((newSimpleType = SimpleType.findType(type.getName())) == null) {
			newSimpleType = new SimpleType(type, indexProvider);
			SimpleType.putTypeToCache(newSimpleType);
		}

		return newSimpleType;
	}

	/**
	 * Removes <code>SimpleType</code> instance by given name and returns it.
	 * 
	 * @param name
	 *            Type name
	 * 
	 * @return <code>SimpleType</code> instance if exists
	 */
	public static SimpleType removeType(final String name) {
		return typeCache.remove(name.toUpperCase());
	}

	/**
	 * Removes all types
	 */
	public static void removeAllTypes() {
		typeCache.clear();
	}

	/**
	 * Finds type by given name
	 * 
	 * @param typeName
	 *            Type name
	 * @return Found <code>SimpleType</code> instence or <code>null</code>
	 */
	public static SimpleType findType(final String typeName) {
		return typeCache.get(typeName.toUpperCase());
	}

	/**
	 * Returns all configured types
	 * 
	 * @return Unmodifiable collection of configured <code>SimpleType</code>
	 *         instances
	 */
	public static Collection<SimpleType> enumerateAllTypes() {
		return Collections.unmodifiableCollection(SimpleType.typeCache.values());
	}

	/**
	 * Returns configured types' names
	 * 
	 * @return Collection of types' names
	 */
	public static Collection<String> enumerateAllTypeNames() {
		Collection<String> typesNames = new LinkedList<String>();
		for (SimpleType st : enumerateAllTypes()) {
			typesNames.add(st.getName());
		}
		return typesNames;
	}

	/**
	 * Gets name of type
	 * 
	 * @return name of type
	 */
	public String getName() {
		return name;
	}

	/**
	 * Gets value from cache
	 * 
	 * @param <RETURN_TYPE>
	 *            value
	 * @param oid
	 *            object id
	 * @param propName
	 *            name of property
	 * @param clazz
	 *            class to cast
	 * @return value
	 * @throws ObjectNotFoundException
	 *             if there are no object
	 * @throws UnableToConvertPropertyException
	 *             if unable to convert to type
	 */
	public <RETURN_TYPE> RETURN_TYPE getValue(final Integer oid, final String propName, final Class<RETURN_TYPE> clazz)
	        throws ObjectNotFoundException, UnableToConvertPropertyException, PropertyNotFoundException {
		PropertyType pt = props.get(propName);
		if (pt == null) {
			throw new PropertyNotFoundException(this.name, propName);
		}
		Value<?> v;
		switch (pt.getType()) {
		case NORMAL:
		case COLLECTION:
		case HISTORICAL: {
			if (oid == null)
				throw new IllegalArgumentException("Supplied 'oid' is null");

			SimpleObject so = objs.get(oid);

			if (so == null)
				throw new ObjectNotFoundException(TOFactory.getInstance().loadTO(this.name + ":" + oid.toString()));

			v = so.getPropertyValue(propName).getValue();
			break;

		}
		case STATIC: {
			SimplePropertyValue spv = staticProperties.get(propName);
			if (spv == null) {
				throw new PropertyNotFoundException(propName);
			}
			v = spv.getValue();
			break;
		}
		default:
			throw new PropertyNotFoundException(oid.toString(), propName);
		}

		RETURN_TYPE result;
		if (clazz == ValueTO.class) {
			result = (RETURN_TYPE) new ValueTO(propName, v.getValue(), v.getTimestamp());
		} else {
			try {
				result = clazz.cast(v.getValue());
			} catch (ClassCastException e) {
				throw new UnableToConvertPropertyException(propName, clazz, e);
			}
		}
		return result;

	}

	public void setPropValue(final Integer oid, final ValueTO valueTo) throws ObjectNotFoundException,
	        PropertyNotFoundException, UnableToConvertPropertyException {
		String propName = valueTo.getPropertyName();
		Object value = valueTo.getValue();
		long timestamp = valueTo.getTimeStamp();
		PropertyType pt = props.get(propName);
		if (pt == null)
			throw new PropertyNotFoundException(propName);
		switch (pt.getType()) {
		case NORMAL:
		case COLLECTION:
		case HISTORICAL: {
			if (oid == null)
				throw new IllegalArgumentException("Supplied 'oid' is null");

			SimpleObject so = objs.get(oid);
			if (so == null) {
				throw new ObjectNotFoundException(TOFactory.getInstance().loadTO(name + ":" + oid));
			}

			Object oldValue = so.setPropertyValue(propName, value, timestamp);
			updateIndexData(propName, oldValue, value, oid);
			break;
		}
		case STATIC: {
			SimplePropertyValue spv = staticProperties.get(propName);
			if (spv != null) {
				try {
					((Value<Object>) spv.getValue()).setValue(value);
				} catch (ClassCastException e) {
					throw new UnableToConvertPropertyException(propName, e);
				}
				spv.getValue().setTimestamp(timestamp);
			}
		}
		default:
			break;
		}
	}

	/**
	 * Finds objects regarding criteria
	 * 
	 * @param propertyValues
	 *            criteria for search
	 * @return collection of objects id
	 */
	public Collection<TO<?>> find(final ValueTO[] propertyValues) {
		Map<String, Value<?>> conditions = CollectionUtils.newMap();

		Map<String, Object> getByIndex = CollectionUtils.newMap();
		for (ValueTO pvto : propertyValues) {
			if (indexes.containsKey(pvto.getPropertyName())) {
				getByIndex.put(pvto.getPropertyName(), pvto.getValue());
			} else { // exclude conditions which are indexed
				conditions.put(pvto.getPropertyName(), Value.createDynamicTypedValue(pvto.getValue()));
			}
		}

		// filter object using indexes
		Map<String, Collection<Integer>> foundByIndex = CollectionUtils.newMap();

		for (java.util.Map.Entry<String, Object> index : getByIndex.entrySet()) {
			Multimap<Object, Integer> indexData = indexes.get(index.getKey());
			foundByIndex.put(index.getKey(), indexData.get(index.getValue()));
		}

		// if no available indexes - do full scan
		if (getByIndex.isEmpty()) {
			return match(objs.keySet(), conditions);
		}

		// if there is no objects exactly corresponding to all indexes - returns
		// empty
		if (foundByIndex.isEmpty()) {
			return Collections.emptyList();
		}

		// if we are here , we located some objects by index and need to
		Iterator<Collection<Integer>> i = foundByIndex.values().iterator();
		Collection<Integer> initial = i.next();
		while (i.hasNext() && !initial.isEmpty()) {
			initial.retainAll(i.next());
		}
		return match(initial, conditions);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	/**
	 * Adds object to type
	 * 
	 * @param oid
	 *            object id
	 * @return object
	 */
	public SimpleObject addObject(final Integer oid) {
		SimpleObject so = new SimpleObject(this, oid);
		objs.put(oid, so);
		for (Map.Entry<String, SimpleType.PropertyType> entry : props.entrySet()) {
			if (!entry.getValue().getType().equals(PropType.STATIC)) {
				so.addProperty(entry.getKey(), entry.getValue());
			}
		}
		return so;
	}

	/**
	 * Removes object from cache
	 * 
	 * @param oid
	 *            Id of object
	 */
	public void removeObject(final Integer oid) {
		SimpleObject so = objs.remove(oid);
		if (so == null)
			return;
		// removing object hash value(hash value in collections)
		Pair<String, Integer> removedObjectPair = Pair.newPair(this.name, oid);
		// clear links from first level parents
		for (Pair<String, Integer> parentId : so.getParents()) {
			SimpleType st = findType(parentId.getFirst());
			if (st == null)
				continue;
			SimpleObject soParent = st.getObject(parentId.getSecond());
			if (soParent == null)
				continue;
			soParent.getChildren().remove(removedObjectPair);
		}
		// clear links from first level children
		for (Pair<String, Integer> childId : so.getChildren()) {
			SimpleType st = findType(childId.getFirst());
			if (st == null)
				continue;
			SimpleObject soChild = st.getObject(childId.getSecond());
			if (soChild == null)
				continue;
			soChild.getParents().remove(removedObjectPair);
		}

		removeIndexData(oid);
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final SimpleType other = (SimpleType) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder(100);
		sb.append("Type=[");
		sb.append(name);
		sb.append("]\nProps:\n");
		for (String s : props.keySet()) {
			sb.append(s);
			sb.append("->");
			sb.append(props.get(s));
			// sb.append("\n");
		}

		sb.append("\nStatic:\n");
		for (String prop : staticProperties.keySet()) {
			sb.append(prop);
			sb.append('=');
			sb.append(staticProperties.get(prop));
		}

		sb.append("\n\nObjects:\n");

		for (Integer oid : objs.keySet()) {
			sb.append("Object ID [");
			sb.append(oid);
			sb.append("]\nProperties and values\n");
			sb.append(objs.get(oid));
			sb.append("\n");
		}

		return sb.toString();
	}

	public Set<Integer> getAllObjects() {
		return objs.keySet();
	}

	/**
	 * Gets object of type
	 * 
	 * @param id
	 *            id of object
	 * @return object or null if object is not exist
	 */
	public SimpleObject getObject(final Integer id) {
		return objs.get(id);
	}

	/**
	 * Returns immutable <code>ObjectType</code> instance
	 * 
	 * @return <code>ObjectType</code> instance
	 */
	ObjectType getEnvType() {
		return envType;
	}

	/**
	 * Merges the internal state with given <code>ObjectType</code> instance
	 * 
	 * @param objType
	 *            <code>ObjectType</code> instance
	 * @return ObjectTypeUpdatedEvent containing type's changes
	 */
	public ObjectTypeUpdatedEvent updateEnvType(final ObjectType objType) {
		Set<PropertyDescr> addedProperties = null;
		Set<String> propsToRemove = new HashSet<String>();
		Set<PropertyDescr> newTypeProperties = objType.getPropertyDescriptors();
		if (newTypeProperties.size() == 0) {
			for (PropertyDescr pd : getEnvType().getPropertyDescriptors()) {
				propsToRemove.add(pd.getName());
			}
			this.removeAllProperties();
		} else {
			addedProperties = CollectionUtils.newSet();
			Set<String> newTypePropsNames = CollectionUtils.newSet();
			for (PropertyDescr pd : newTypeProperties) {
				newTypePropsNames.add(pd.getName());
				if (!queryProperty(pd.getName())) {
					addProperty(pd.getName(), new PropertyType(pd));
					addedProperties.add(pd);
				}
			}

			for (String prop : getProperties()) {
				// if simple type does not have some properties
				if (!newTypePropsNames.contains(prop))
					propsToRemove.add(prop);
			}

			for (String prop : propsToRemove) {
				removeProperty(prop);
			}
		}

		setEnvType(objType);

		for (PropertyDescr pd : newTypeProperties) {
			updatePropertyTags(pd.getName());
		}

		// TODO: hack! we hide numAlerts property manipulations from client
		// since this property is internal
		propsToRemove.remove(NUM_ALERTS_PROPERTY_NAME);
		return new ObjectTypeUpdatedEvent(objType.getName(), addedProperties, propsToRemove);
	}

	/**
	 * check if SimpleObject with id exists in cache
	 * 
	 * @param id
	 *            - object id
	 * 
	 * @return true if SimpleObject with id exists in cache
	 */
	public boolean exists(final Object id) {
		return objs.containsKey(id);
	}

	// package level members
	void addIndex(final String property) {
		if (!props.containsKey(property)) {
			return;
		}

		Multimap<Object, Integer> newIndex = buildIndex(property);
		if (newIndex != null) {
			indexes.put(property, newIndex);
		}
	}

	void removeIndex(String property) {
		if (indexes.containsKey(property)) {
			indexes.remove(property);
		}
	}

	// private members

	private void setEnvType(ObjectType objType) {
		this.envType = new EnvObjTypeImmutable(objType);
	}

	/**
	 * Removes property from type
	 * 
	 * @param propName
	 *            Property name
	 */
	private void removeProperty(final String propName) {
		// ignores 'alerts' property , it cannot be removed as it's mandatory
		// for all object types
		if (NUM_ALERTS_PROPERTY_NAME.equalsIgnoreCase(propName)) {
			return;
		}

		// disable index access early
		removeIndex(propName);

		props.remove(propName);
		for (SimpleObject so : objs.values()) {
			so.removeProperty(propName);
		}
	}

	private void removeAllProperties() {
		// ignores 'alerts' property , it cannot be removed as it's mandatory
		// for all object types
		for (String propName : props.keySet()) {
			removeProperty(propName);
		}
	}

	/**
	 * Updates property tag in type
	 * 
	 * @param propName
	 *            Property name
	 */
	private void updatePropertyTags(String propName) {
		for (SimpleObject so : objs.values()) {
			so.updatePropertyTags(propName);
		}
	}

	/**
	 * Adds property to type
	 * 
	 * @param propName
	 *            name of property
	 * @param type
	 *            property type
	 */
	private void addProperty(final String propName, final PropertyType type) {

		if (type.getType().equals(PropType.STATIC)) {
			staticProperties.put(propName, new SimplePropertyValue(propName, type.getClazz()));
		}
		props.put(propName, type);
		for (SimpleObject so : objs.values()) {
			if (!type.getType().equals(PropType.STATIC))
				so.addProperty(propName, type);
		}
	}

	private Collection<TO<?>> match(Collection<Integer> oids, Map<String, Value<?>> conditions) {
		Collection<TO<?>> res = CollectionUtils.newList();
		for (Integer id : oids) {
			SimpleObject so = getObject(id);
			if (so.isComplain(conditions)) {
				res.add(TOFactory.INSTANCE.loadTO(this.name, (Integer) so.getOid()));
			}
		}
		return res;
	}

	/**
	 * Gets set of props
	 * 
	 * @return set
	 */
	private Set<String> getProperties() {
		Set<String> res = CollectionUtils.newSet();
		for (Map.Entry<String, SimpleType.PropertyType> entry : props.entrySet()) {
			if (!entry.getValue().getType().equals(PropType.STATIC)) {
				res.add(entry.getKey());
			}
		}
		return res;
	}

	/**
	 * Query existence
	 * 
	 * @param propName
	 *            Property name
	 * @return true if property name exist
	 */
	private boolean queryProperty(final String propName) {
		return props.containsKey(propName);
	}

	private void updateIndexData(String property, Object oldValue, Object newValue, Integer id) {
		if (!indexes.containsKey(property)) {
			return;
		}

		if (!objs.containsKey(id)) {
			return;
		}

		removeIndexData(property, id, oldValue);
		addIndexData(property, id, newValue);
	}

	private void removeIndexData(Integer id) {
		for (Multimap<Object, Integer> index : indexes.values()) {
			index.values().remove(id);
		}
	}

	private void removeIndexData(String indexName, Integer id, Object value) {
		Multimap<Object, Integer> index = indexes.get(indexName);
		if (value instanceof Collection) {
			for (Object v : (Collection) value) {
				index.remove(v, id);
			}
		} else {
			index.remove(value, id);
		}
	}

	private void addIndexData(String indexName, Integer id, Object value) {
		Multimap<Object, Integer> index = indexes.get(indexName);

		if (value instanceof Collection) {
			for (Object v : (Collection) value) {
				index.put(v, id);
			}
		} else {
			index.put(value, id);
		}
	}

	/**
	 * expensive operation
	 * 
	 * @param property
	 *            Property name
	 */
	private Multimap<Object, Integer> buildIndex(String property) {
		Multimap<Object, Integer> newIndex = indexProvider.get();

		for (SimpleObject so : objs.values()) {
			try {
				SimplePropertyValue spv = so.getPropertyValue(property);
				Object value = spv.getValue().getValue();
				if (value instanceof Collection) {
					for (Object valueInCollection : (Collection) value) {
						newIndex.put(valueInCollection, so.getOid());
					}
				} else {
					newIndex.put(value, so.getOid());
				}
			} catch (PropertyNotFoundException e) {
				throw new IllegalStateException("Failed to create index on property : " + property, e);
			}
		}

		return newIndex;
	}
}
