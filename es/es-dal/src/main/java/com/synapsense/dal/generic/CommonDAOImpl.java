package com.synapsense.dal.generic;

import java.io.Serializable;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Example;

/**
 * Common DAO implementation. It is suitable for any entity. Encapsulates
 * storage access behavior.
 * 
 * @author shabanov
 * 
 */
@Stateless
public class CommonDAOImpl implements CommonDAO {

	private final static Logger log = Logger.getLogger(CommonDAOImpl.class);

	/**
	 * JPA entity manager
	 */
	private EntityManager em;

	/**
	 * Injects entity manager.
	 * 
	 * @param em
	 *            JPA Entity manager
	 */

	@PersistenceContext(unitName = "HibernateDAL")
	public void setEM(EntityManager em) {
		this.em = em;
	}

	/**
	 * hibernate session getter (supposing that underlying implementation is
	 * hibernate)
	 * 
	 * @return
	 */
	private Session getSession() {
		return (Session) this.em.getDelegate();
	}

	@Override
	public <ENTITY extends Serializable> ENTITY create(ENTITY entity) throws DAOEntityExistsException {
		return create(entity, true);
	}

	@Override
	public <ENTITY extends Serializable> ENTITY create(ENTITY entity, boolean flush) throws DAOEntityExistsException {
		try {
			// when calling persist without flush the actual record to db is not
			// executed
			// if we use <generator native + sequence> the persist call only get
			// select sequence.nextVal()
			em.persist(entity);
			if (flush) {
				em.flush();
			}
		} catch (PersistenceException e) {
			throw new DAOEntityExistsException(entity, e);
		}
		return entity;
	}

	@Override
	public <ENTITY extends Serializable> void createBatch(Collection<ENTITY> entities, boolean flush)
	        throws DAOEntityExistsException {
		for (ENTITY entity : entities) {
			create(entity, false);
		}

		if (flush) {
			try {
				em.flush();
			} catch (PersistenceException e) {
				throw new DAOGenericException("", e);
			}
		}
	}

	@Override
	public <ENTITY extends Serializable> boolean update(ENTITY entity) {
		return update(entity, true);
	}

	@Override
	public <ENTITY extends Serializable> boolean update(ENTITY entity, boolean flush) {
		em.merge(entity);
		if (flush) {
			try {
				em.flush();
			} catch (PersistenceException e) {
				throw new DAOEntityExistsException("db access exception while updating entity : " + entity.toString(),
				        e);
			}
		}
		return true;
	}

	@Override
	public <ENTITY extends Serializable> void updateBatch(Collection<ENTITY> entities, boolean flush) {
		for (ENTITY entity : entities) {
			update(entity, false);
		}

		if (flush) {
			try {
				em.flush();
			} catch (PersistenceException e) {
				throw new DAOGenericException("", e);
			}
		}
	}

	@Override
	public <ENTITY extends Serializable> void delete(Class<ENTITY> clazz, Serializable id) {
		ENTITY entity = em.getReference(clazz, id);
		try {
			em.remove(entity);
		} catch (EntityNotFoundException e) {
			log.warn("Trying to remove non-existent entity {type:" + clazz + " , ID:" + id + "}");
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public <ENTITY extends Serializable> ENTITY findById(Class<ENTITY> clazz, Object entityId) {
		return em.find(clazz, (Serializable) entityId);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public <ENTITY extends Serializable> Collection<ENTITY> findByExample(ENTITY entity) {
		return findByExample(entity, DAOInvocationContext.DEFAULT);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public <ENTITY extends Serializable> ENTITY findUniqueByExample(ENTITY entity)
	        throws DAOTooManyEntitiesFoundException {
		return findUniqueByExample(entity, DAOInvocationContext.DEFAULT);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public <ENTITY extends Serializable> ENTITY findUniqueByExample(ENTITY entity, DAOInvocationContext invocContext)
	        throws DAOTooManyEntitiesFoundException {
		Collection<ENTITY> matchEntities = findByExample(entity, invocContext);

		// no such entities
		if (matchEntities.isEmpty()) {
			return null;
		}
		// more than one
		if (matchEntities.size() > 1) {
			throw new DAOTooManyEntitiesFoundException(matchEntities);
		}
		// only one was found
		return matchEntities.iterator().next();
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public <ENTITY extends Serializable> Collection<ENTITY> findByExample(ENTITY entity,
	        DAOInvocationContext invocContext) {
		Example example = Example.create(entity);
		// exclude zeroes property(which have null values)
		if (invocContext.isExcludeZeroes()) {
			example.excludeZeroes();
		}
		// props to exclude from criteria
		Set<String> excludedProperties = invocContext.getExcludedProperties();
		for (String property : excludedProperties) {
			example.excludeProperty(property);
		}
		return (Collection<ENTITY>) findByCriteria(entity.getClass(), invocContext, example);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public <ENTITY extends Serializable> Collection<ENTITY> findAll(Class<ENTITY> clazz) {
		return findByCriteria(clazz);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public <ENTITY extends Serializable> Collection<ENTITY> findAll(Class<ENTITY> clazz,
	        DAOInvocationContext invocContext) {
		return findByCriteria(clazz, invocContext);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public <ENTITY extends Serializable> Collection<ENTITY> findByNamedQuery(String namedQuery,
	        Map<String, Object> params, Class<ENTITY> clazz) {
		return findByNamedQuery(namedQuery, params, clazz, DAOInvocationContext.DEFAULT);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public <ENTITY extends Serializable> Collection<ENTITY> findByNamedQuery(String namedQuery,
	        Map<String, Object> params, Class<ENTITY> clazz, DAOInvocationContext invocContext) {

		Query query = null;
		try {
			query = getSession().getNamedQuery(namedQuery);
		} catch (HibernateException e) {
			throw new DAOGenericException("Wrong named query [" + namedQuery + "].No such query definition!", e);
		}

		setupQueryFromContext(query, invocContext);

		Iterator<Map.Entry<String, Object>> paramsIterator = params.entrySet().iterator();

		while (paramsIterator.hasNext()) {
			Map.Entry<String, Object> paramsEntry = paramsIterator.next();
			try {
				if (paramsEntry.getValue() instanceof Collection) {
					query.setParameterList(paramsEntry.getKey(), (Collection) paramsEntry.getValue());
				} else {
					query.setParameter(paramsEntry.getKey(), paramsEntry.getValue());
				}
			} catch (HibernateException e) {
				throw new DAOGenericException("Wrong type of the named parameter [" + paramsEntry.getKey()
				        + "] of the query[" + namedQuery + "]", e);
			}
		}

		return query.list();
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Collection<Object[]> findRowSetByNamedQuery(String namedQuery, Map<String, Object> params) {
		return findByNamedQuery(namedQuery, params, Object[].class);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Collection<Object[]> findRowSetByNamedQuery(String namedQuery, Map<String, Object> params,
	        DAOInvocationContext invocContext) {
		return findByNamedQuery(namedQuery, params, Object[].class, invocContext);
	}

	@Override
	public void flushSession() throws DAOGenericException {
		try {
			em.flush();
		} catch (PersistenceException e) {
			throw new DAOGenericException("Could not flush current session", e);
		}
	}

	/**
	 * Use this inside subclasses as a convenience method. Will be useful as
	 * public method only if <code>Criterion</code> package will be public(in
	 * some form , "as is" is not mandatory).
	 */
	protected <ENTITY extends Serializable> Collection<ENTITY> findByCriteria(Class<ENTITY> clazz,
	        DAOInvocationContext invocContext, Criterion... criterion) {

		Criteria criteria = setupCriteriaFromContext(clazz, invocContext);

		for (Criterion c : criterion) {
			criteria.add(c);
		}

		// filters duplicates of ROOT ENTITY
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

		return criteria.list();
	}

	/**
	 * Clone of #findByCriteria(Class, DAOInvocationContext, Criterion...) with
	 * <code>DAOInvocationContext.DEFAULT</code> parameter
	 * 
	 * @see #findByCriteria(Class, DAOInvocationContext, Criterion...)
	 */
	protected <ENTITY extends Serializable> Collection<ENTITY> findByCriteria(Class<ENTITY> clazz,
	        Criterion... criterion) {
		return findByCriteria(clazz, DAOInvocationContext.DEFAULT, criterion);
	}

	private <ENTITY extends Serializable> Criteria setupCriteriaFromContext(final Class<ENTITY> clazz,
	        final DAOInvocationContext invocContext) {
		Criteria criteria = getSession().createCriteria(clazz);
		if (invocContext.getMaxResults() > 0) {
			criteria.setMaxResults(invocContext.getMaxResults());
		}
		if (invocContext.getFirstResult() > 0) {
			criteria.setFirstResult(invocContext.getFirstResult());
		}
		if (invocContext.getFetchSize() > 0) {
			criteria.setFetchSize(invocContext.getFetchSize());
		}
		if (invocContext.getTimeout() > 0) {
			criteria.setTimeout(invocContext.getTimeout());
		}
		// association fetching settings setup
		Set<Map.Entry<String, DAOFetchMode>> assocFetchMode = invocContext.getFetchModeOfAssoc().entrySet();
		Iterator<Map.Entry<String, DAOFetchMode>> assocFetchModeIterator = assocFetchMode.iterator();
		while (assocFetchModeIterator.hasNext()) {
			Map.Entry<String, DAOFetchMode> nextEntry = assocFetchModeIterator.next();
			FetchMode hibernateFetchMode = resolveFetchMode(nextEntry.getValue());
			criteria.setFetchMode(nextEntry.getKey(), hibernateFetchMode);
		}

		return criteria;
	}

	private Query setupQueryFromContext(final Query query, final DAOInvocationContext invocContext) {
		// FIXME : add configuration code , copy context to query settings
		if (invocContext.getMaxResults() > 0) {
			query.setMaxResults(invocContext.getMaxResults());
		}
		if (invocContext.getFirstResult() > 0) {
			query.setFirstResult(invocContext.getFirstResult());
		}
		if (invocContext.getFetchSize() > 0) {
			query.setFetchSize(invocContext.getFetchSize());
		}
		if (invocContext.getTimeout() > 0) {
			query.setTimeout(invocContext.getTimeout());
		}

		return query;
	}

	private FetchMode resolveFetchMode(DAOFetchMode value) {
		if ("JOIN".equals(value.getModeName())) {
			return FetchMode.JOIN;
		}

		if ("SELECT".equals(value.getModeName())) {
			return FetchMode.SELECT;
		}

		return FetchMode.DEFAULT;
	}

	@Override
	public <ENTITY extends Serializable> Collection<ENTITY> findByQuery(String queryString) {
		Query query = null;
		try {
			query = getSession().createQuery(queryString);
		} catch (HibernateException e) {
			throw new DAOGenericException("Wrong query [" + queryString + "]!", e);
		}
		setupQueryFromContext(query, DAOInvocationContext.DEFAULT);

		return query.list();
	}

}
