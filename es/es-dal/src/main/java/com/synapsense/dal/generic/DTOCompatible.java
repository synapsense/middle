package com.synapsense.dal.generic;

import java.io.Serializable;

/**
 * If a business entity has some data transfer representations it must implement
 * this interface.
 * 
 * @author shabanov
 * 
 * @param <DTO_TYPE>
 *            Class that represents DTO
 */
public interface DTOCompatible<DTO_TYPE> extends DTOGetter<DTO_TYPE>, DTOSetter<DTO_TYPE>, Serializable {
}
