package com.synapsense.dal.objectcache;

import com.google.common.collect.Multimap;

public interface IndexProvider {
	Multimap<Object, Integer> get();
}
