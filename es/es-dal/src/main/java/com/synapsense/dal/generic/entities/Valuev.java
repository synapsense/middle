package com.synapsense.dal.generic.entities;

import com.synapsense.exception.InvalidValueException;

public class Valuev extends AbstractValue<String> implements java.io.Serializable {
	private static final long serialVersionUID = -2544380604985546038L;

    // mysql row cannot hold more than 64K data
    // that is where this limit come from
    // update it when update synap.valuev table
    private static final int MAX_LENGTH = 21833;

	private Integer id;
	private ValueDO valueRef;
	private String value;

	Valuev() {
		super();
	}

	public Valuev(ValueDO valueRef) {
		this(valueRef, null);
	}

	public Valuev(ValueDO valueRef, Object value) {
		this();
		this.valueRef = valueRef;
		setValue((String)value);
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public ValueDO getValueRef() {
		return this.valueRef;
	}

	public void setValueRef(ValueDO valueRef) {
		this.valueRef = valueRef;
	}

	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
        if (value != null && value.length() > MAX_LENGTH) {
            throw new InvalidValueException("Specified text exceeds the limit of " + MAX_LENGTH + " characters");
        }
		this.value = value;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		result = prime * result + ((valueRef == null) ? 0 : valueRef.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final Valuev other = (Valuev) obj;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		if (valueRef == null) {
			if (other.valueRef != null)
				return false;
		} else if (!valueRef.equals(other.valueRef))
			return false;
		return true;
	}

}
