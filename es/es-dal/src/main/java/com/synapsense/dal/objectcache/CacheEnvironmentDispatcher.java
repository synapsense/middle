package com.synapsense.dal.objectcache;

import java.util.Collection;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.synapsense.dto.CollectionTO;
import com.synapsense.dto.EnvObjectBundle;
import com.synapsense.dto.EnvObjectHistory;
import com.synapsense.dto.ObjectType;
import com.synapsense.dto.ObjectTypeMatchMode;
import com.synapsense.dto.PropertyDescr;
import com.synapsense.dto.Relation;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOBundle;
import com.synapsense.dto.Tag;
import com.synapsense.dto.TagDescriptor;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.EnvException;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.exception.PropertyNotFoundException;
import com.synapsense.exception.ServerConfigurationException;
import com.synapsense.exception.TagNotFoundException;
import com.synapsense.exception.UnableToConvertPropertyException;
import com.synapsense.exception.UnableToConvertTagException;
import com.synapsense.exception.UnsupportedRelationException;
import com.synapsense.search.QueryDescription;
import com.synapsense.service.Environment;

/**
 * Class dispatches requests by two services : cache service and environment
 * service.
 * 
 * @author ashabanov
 * 
 */
public class CacheEnvironmentDispatcher implements Environment {
	/**
	 * Simple factory method to create cache_env adaptor
	 * 
	 * @param cache
	 *            Cache service reference
	 * @param env
	 * @return Reference to environment
	 */
	public static Environment getEnvironment(CacheSvc cache, Environment env) {
		if (cache == null)
			throw new IllegalArgumentException("Supplied 'cache' is null");
		if (env == null)
			throw new IllegalArgumentException("Supplied 'env' is null");
		if (env instanceof CacheEnvironmentDispatcher)
			throw new IllegalArgumentException("Supplied 'env' cannot reference to instances of Cache2EnvAdaptor.class");
		return new CacheEnvironmentDispatcher(cache, env);
	}

	private CacheSvc cache;

	private Environment env;

	private CacheEnvironmentDispatcher(CacheSvc cache, Environment env) {
		super();
		this.cache = cache;
		this.env = env;
	}

	@Override
	public Collection<ValueTO> getAllPropertiesValues(TO<?> objId) throws ObjectNotFoundException {
		if (cache.getStatus() == Status.ONLINE)
			return cache.getAllPropertiesValues(objId);
		return env.getAllPropertiesValues(objId);
	}

	@Override
	public Collection<CollectionTO> getAllPropertiesValues(Collection<TO<?>> objIds) {
		if (cache.getStatus() == Status.ONLINE)
			return cache.getAllPropertiesValues(objIds);
		return env.getAllPropertiesValues(objIds);
	}

	@Override
	public ObjectType getObjectType(String name) {
		if (cache.getStatus() == Status.ONLINE) {
			ObjectType types[] = cache.getObjectTypes(new String[] { name });
			if (types.length == 0)
				return null;
			return types[0];
		}
		return env.getObjectType(name);
	}

	@Override
	public ObjectType[] getObjectTypes(String[] names) {
		if (cache.getStatus() == Status.ONLINE)
			return cache.getObjectTypes(names);
		return env.getObjectTypes(names);
	}

	@Override
	public String[] getObjectTypes() {
		if (cache.getStatus() == Status.ONLINE)
			return cache.getObjectTypes();
		return env.getObjectTypes();
	}

	@Override
	public Collection<TO<?>> getObjectsByType(String typeName) {
		if (cache.getStatus() == Status.ONLINE)
			return cache.getObjectsByType(typeName);
		return env.getObjectsByType(typeName);
	}

	@Override
	public Collection<TO<?>> getObjectsByType(String typeName, ObjectTypeMatchMode matchMode) {
		if (cache.getStatus() == Status.ONLINE)
			return cache.getObjectsByType(typeName, matchMode);
		return env.getObjectsByType(typeName, matchMode);
	}

	@Override
	public <RETURN_TYPE> RETURN_TYPE getPropertyValue(TO<?> objId, String propName, Class<RETURN_TYPE> clazz)
	        throws ObjectNotFoundException, PropertyNotFoundException, UnableToConvertPropertyException {
		if (cache.getStatus() == Status.ONLINE)
			return cache.getPropertyValue(objId, propName, clazz);
		return env.getPropertyValue(objId, propName, clazz);
	}

	@Override
	public Collection<CollectionTO> getPropertyValue(Collection<TO<?>> objIds, String[] propNames) {
		if (cache.getStatus() == Status.ONLINE)
			return cache.getPropertyValue(objIds, propNames);
		return env.getPropertyValue(objIds, propNames);
	}

	@Override
	public <RETURN_TYPE> RETURN_TYPE getPropertyValue(String typeName, String propName, Class<RETURN_TYPE> clazz)
	        throws ObjectNotFoundException, PropertyNotFoundException, UnableToConvertPropertyException {
		if (cache.getStatus() == Status.ONLINE)
			return cache.getPropertyValue(typeName, propName, clazz);
		return env.getPropertyValue(typeName, propName, clazz);
	}

	@Override
	public <REQUESTED_TYPE> void addPropertyValue(TO<?> objId, String propName, REQUESTED_TYPE value)
	        throws ObjectNotFoundException, PropertyNotFoundException, UnableToConvertPropertyException {
		env.addPropertyValue(objId, propName, value);
	}

	@Override
	public void configurationComplete() throws ServerConfigurationException {
		env.configurationComplete();
	}

	@Override
	public void configurationStart() throws ServerConfigurationException {
		env.configurationStart();
	}

	@Override
	public TO<?> createObject(String typeName, ValueTO[] propertyValues) throws EnvException {
		return env.createObject(typeName, propertyValues);
	}

	@Override
	public TO<?> createObject(String typeName) throws EnvException {
		return env.createObject(typeName);
	}

	@Override
	public TOBundle createObjects(EnvObjectBundle objectsToCreate) throws EnvException {
		return env.createObjects(objectsToCreate);
	}

	@Override
	public ObjectType createObjectType(String name) throws EnvException {
		return env.createObjectType(name);
	}

	@Override
	public void deleteObject(TO<?> objId) throws ObjectNotFoundException {
		env.deleteObject(objId);
	}

	@Override
	public void deleteObject(TO<?> objId, int level) throws ObjectNotFoundException {
		env.deleteObject(objId, level);
	}

	@Override
	public void deleteObjectType(String typeName) throws EnvException {
		env.deleteObjectType(typeName);
	}

	@Override
	public Collection<CollectionTO> getChildren(Collection<TO<?>> objIds, String typeName) {
		if (cache.getStatus() == Status.ONLINE)
			return cache.getChildren(objIds, typeName);
		return env.getChildren(objIds, typeName);
	}

	@Override
	public Collection<CollectionTO> getChildren(Collection<TO<?>> objIds) {
		if (cache.getStatus() == Status.ONLINE)
			return cache.getChildren(objIds, null);
		return env.getChildren(objIds);
	}

	@Override
	public Collection<TO<?>> getChildren(TO<?> objId, String typeName) throws ObjectNotFoundException {
		if (cache.getStatus() == Status.ONLINE) {
			Collection<TO<?>> objIds = new LinkedList<TO<?>>();
			objIds.add(objId);
			Collection<CollectionTO> children = cache.getChildren(objIds, typeName);
			CollectionTO item = children.iterator().next();
			Collection<TO<?>> result = new LinkedList<TO<?>>();
			for (ValueTO value : item.getPropValues()) {
				result.add((TO<?>) value.getValue());
			}
			return result;
		}
		return env.getChildren(objId, typeName);
	}

	@Override
	public Collection<TO<?>> getChildren(TO<?> objId) throws ObjectNotFoundException {
		if (cache.getStatus() == Status.ONLINE)
			return this.getChildren(objId, null);
		return env.getChildren(objId);
	}
	
	@Override
	public <RETURN_TYPE> Collection<ValueTO> getHistory(TO<?> objId, String propName, Class<RETURN_TYPE> clazz)
	        throws ObjectNotFoundException, UnableToConvertPropertyException, PropertyNotFoundException {
		return env.getHistory(objId, propName, clazz);
	}

	@Override
	public <RETURN_TYPE> Collection<ValueTO> getHistory(TO<?> objId, String propName, Date start, Date end,
	        Class<RETURN_TYPE> clazz) throws ObjectNotFoundException, PropertyNotFoundException,
	        UnableToConvertPropertyException {
		return env.getHistory(objId, propName, start, end, clazz);
	}

	@Override
	public EnvObjectHistory getHistory(TO<?> objId, Collection<String> propNames, Date start, Date end)
	        throws ObjectNotFoundException, PropertyNotFoundException {
		return env.getHistory(objId, propNames, start, end);
	}

	@Override
	public <RETURN_TYPE> Collection<ValueTO> getHistory(TO<?> objId, String propName, Date start, int numberPoints,
	        Class<RETURN_TYPE> clazz) throws ObjectNotFoundException, PropertyNotFoundException,
	        UnableToConvertPropertyException {
		return env.getHistory(objId, propName, start, numberPoints, clazz);
	}

	@Override
	public <RETURN_TYPE> Collection<ValueTO> getHistory(TO<?> objId, String propName, int size, int start,
	        Class<RETURN_TYPE> clazz) throws ObjectNotFoundException, PropertyNotFoundException,
	        UnableToConvertPropertyException {
		return env.getHistory(objId, propName, size, start, clazz);
	}

	@Override
	public Integer getHistorySize(TO<?> objId, String propName) throws ObjectNotFoundException,
	        PropertyNotFoundException {
		return env.getHistorySize(objId, propName);
	}

	@Override
	public Collection<TO<?>> getObjects(String typeName, ObjectTypeMatchMode matchMode, ValueTO[] propertyValues) {
		if (cache.getStatus() == Status.ONLINE)
			return cache.getObjects(typeName, matchMode, propertyValues);
		return env.getObjects(typeName, matchMode, propertyValues);
	}

	@Override
	public Collection<TO<?>> getObjects(String typeName, ValueTO[] propertyValues) {
		return env.getObjects(typeName, propertyValues);
	}

	@Override
	public Collection<TO<?>> getObjects(QueryDescription queryDescr) {
		return env.getObjects(queryDescr);
	}

	@Override
	public Collection<TO<?>> getParents(TO<?> objId, String typeName) throws ObjectNotFoundException {
		if (cache.getStatus() == Status.ONLINE)
			return cache.getParents(objId, typeName);
		return env.getParents(objId, typeName);
	}

	@Override
	public Collection<TO<?>> getParents(TO<?> objId) throws ObjectNotFoundException {
		return this.getParents(objId, null);
	}

	@Override
	public PropertyDescr getPropertyDescriptor(String typeName, String propName) throws ObjectNotFoundException,
	        PropertyNotFoundException {
		if (cache.getStatus() == Status.ONLINE)
			return cache.getPropertyDescriptor(typeName, propName);
		return env.getPropertyDescriptor(typeName, propName);
	}

	@Override
	public long getPropertyTimestamp(TO<?> objId, String propName) throws ObjectNotFoundException,
	        PropertyNotFoundException {
		if (cache.getStatus() == Status.ONLINE) {
			try {
				return cache.getPropertyValue(objId, propName, ValueTO.class).getTimeStamp();
			} catch (UnableToConvertPropertyException e) {
				return 0;
			}
		}
		return env.getPropertyTimestamp(objId, propName);
	}

	@Override
	public Collection<TO<?>> getRelatedObjects(TO<?> objId, String objectType, boolean isChild) {
		if (cache.getStatus() == Status.ONLINE)
			return cache.getRelatedObjects(objId, objectType, isChild);
		return env.getRelatedObjects(objId, objectType, isChild);
	}

	@Override
	public <REQUESTED_TYPE> void removePropertyValue(TO<?> objId, String propName, REQUESTED_TYPE value)
	        throws ObjectNotFoundException, PropertyNotFoundException, UnableToConvertPropertyException {
		env.removePropertyValue(objId, propName, value);
	}

	@Override
	public void removeRelation(TO<?> parentId, TO<?> childId) throws ObjectNotFoundException {
		env.removeRelation(parentId, childId);
	}

	@Override
	public void removeRelation(List<Relation> relations) throws ObjectNotFoundException {
		env.removeRelation(relations);
	}

	@Override
	public void setAllPropertiesValues(TO<?> objId, ValueTO[] values) throws ObjectNotFoundException,
	        PropertyNotFoundException, UnableToConvertPropertyException {
		this.setAllPropertiesValues(objId, values, true);
	}

	@Override
	public <REQUESTED_TYPE> void setPropertyValue(String typeName, String propName, REQUESTED_TYPE value)
	        throws ObjectNotFoundException, PropertyNotFoundException, UnableToConvertPropertyException {
		if (cache.getStatus() == Status.ONLINE) {
			cache.setPropertyValue(typeName, propName, value);
			return;
		}
		env.setPropertyValue(typeName, propName, value);
	}

	@Override
	public <REQUESTED_TYPE> void setPropertyValue(TO<?> objId, String propName, REQUESTED_TYPE value)
	        throws ObjectNotFoundException, PropertyNotFoundException, UnableToConvertPropertyException {
		this.setPropertyValue(objId, propName, value, true);
	}

	@Override
	public void setRelation(TO<?> parentId, TO<?> childId) throws ObjectNotFoundException, UnsupportedRelationException {
		env.setRelation(parentId, childId);
	}

	@Override
	public void setRelation(List<Relation> relations) throws ObjectNotFoundException, UnsupportedRelationException {
		env.setRelation(relations);
	}

	public void updateObjectType(ObjectType objType) throws EnvException {
		env.updateObjectType(objType);
	}

	@Override
	public void setAllPropertiesValues(TO<?> objId, ValueTO[] values, boolean persist) throws ObjectNotFoundException,
	        PropertyNotFoundException, UnableToConvertPropertyException {

		if (cache.getStatus() == Status.ONLINE) {
			cache.setAllPropertiesValues(objId, values, persist);
			return;
		}

		env.setAllPropertiesValues(objId, values, persist);
	}

	@Override
	public void setAllPropertiesValues(TO<?> objId, Collection<ValueTO> values, Collection<Tag> tags)
	        throws EnvException {
		env.setAllPropertiesValues(objId, values, tags);
	}

	@Override
	public <REQUESTED_TYPE> void setPropertyValue(TO<?> objId, String propName, REQUESTED_TYPE value, boolean persist)
	        throws PropertyNotFoundException, ObjectNotFoundException, UnableToConvertPropertyException {
		this.setAllPropertiesValues(objId, new ValueTO[] { new ValueTO(propName, value) });
	}

	@Override
	public boolean exists(TO<?> objId) {
		if (cache.getStatus() == Status.ONLINE)
			return cache.exists(objId);
		return env.exists(objId);
	}

	@Override
	public TagDescriptor getTagDescriptor(String typeName, String propName, String tagName)
	        throws ObjectNotFoundException, PropertyNotFoundException, TagNotFoundException {
		if (cache.getStatus() == Status.ONLINE) {
			return cache.getTagDescriptor(typeName, propName, tagName);
		}
		return env.getTagDescriptor(typeName, propName, tagName);
	}

	@Override
	public <RETURN_TYPE> RETURN_TYPE getTagValue(TO<?> objId, String propName, String tagName, Class<RETURN_TYPE> clazz)
	        throws ObjectNotFoundException, PropertyNotFoundException, TagNotFoundException,
	        UnableToConvertTagException {
		if (cache.getStatus() == Status.ONLINE)
			return cache.getTagValue(objId, propName, tagName, clazz);
		return env.getTagValue(objId, propName, tagName, clazz);
	}

	@Override
	public Collection<Tag> getAllTags(TO<?> objId, String propName) throws ObjectNotFoundException,
	        PropertyNotFoundException {
		if (cache.getStatus() == Status.ONLINE)
			return cache.getAllTags(objId, propName);
		return env.getAllTags(objId, propName);
	}

	@Override
	public Collection<Tag> getAllTags(TO<?> objId) throws ObjectNotFoundException {
		if (cache.getStatus() == Status.ONLINE)
			return cache.getAllTags(objId);
		return env.getAllTags(objId);
	}

	@Override
	public Map<TO<?>, Collection<Tag>> getAllTags(String typeName) {
		if (cache.getStatus() == Status.ONLINE)
			return cache.getAllTags(typeName);
		return env.getAllTags(typeName);
	}

	@Override
	public void setTagValue(TO<?> objId, String propName, String tagName, Object value)
	        throws PropertyNotFoundException, ObjectNotFoundException, TagNotFoundException,
	        UnableToConvertTagException {
		env.setTagValue(objId, propName, tagName, value);
	}

	@Override
	public Map<TO<?>, Map<String, Collection<Tag>>> getTags(Collection<TO<?>> objIds, List<String> propertyNames,
	        List<String> tagNames) {
		if (cache.getStatus() == Status.ONLINE)
			return cache.getTags(objIds, propertyNames, tagNames);
		return env.getTags(objIds, propertyNames, tagNames);
	}

	@Override
	public void setAllTagsValues(TO<?> objId, Collection<Tag> tagsToSet) throws PropertyNotFoundException,
	        ObjectNotFoundException, TagNotFoundException, UnableToConvertTagException {
		env.setAllTagsValues(objId, tagsToSet);
	}

}
