package com.synapsense.dal.generic.batch.loader;

public class FileResource implements Resource {

	private String file;

	public FileResource(String bufferFile) {
		this.file = bufferFile;
	}

	@Override
	public String getName() {
		return file;
	}

}
