package com.synapsense.dal.generic.dao;

import java.util.Collection;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.log4j.Logger;

import com.synapsense.dal.generic.entities.NhtInfoDO;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.IllegalInputParameterException;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.service.impl.dao.DataObjectDAO;

//@PoolClass annotation is not available in JBoss 7
//@PoolClass(value = org.jboss.ejb3.StrictMaxPool.class, maxSize = 100, timeout = 10000)
@Stateless
public class NhtInfoDAO implements DataObjectDAO<Integer, NhtInfoDO> {

	private final static Logger log = Logger.getLogger(NhtInfoDAO.class);

	@PersistenceContext(unitName = "HibernateDAL")
	public void setEM(EntityManager em) {
		this.em = em;
	}

	private EntityManager em;

	private Collection<ValueTO> fillNhtInfoCollectionToReturn(Collection<NhtInfoDO> nhtInfo) {
		Collection<ValueTO> result = new LinkedList<ValueTO>();
		for (NhtInfoDO data : nhtInfo) {
			result.add(new ValueTO("lastValue", data, data.getReportTimestamp().getTime()));
		}
		return result;

	}

	@Override
	public void addBulk(Integer id, Collection<NhtInfoDO> data) throws ObjectNotFoundException {

		if (id == null) {
			throw new IllegalInputParameterException("Supplied 'id' is null");
		}

		if (id < 1) {
			throw new IllegalInputParameterException("Supplied 'id' is negative or 0");
		}

		if (data == null) {
			throw new IllegalInputParameterException("Supplied 'data' is null");
		}

		for (NhtInfoDO d : data) {
			addData(id, d);
		}
	}

	@Override
	public void addData(Integer id, NhtInfoDO data) throws ObjectNotFoundException {

		if (id == null) {
			throw new IllegalInputParameterException("Supplied 'id' is null");
		}

		if (id < 1) {
			throw new IllegalInputParameterException("Supplied 'id' is negative or 0");
		}

		if (data == null) {
			throw new IllegalInputParameterException("Supplied 'data' is null");
		}

		// updates node id
		data.setNodeId(id);

		em.persist(data);

		if (log.isDebugEnabled()) {
			log.debug("Trying to add nht statistic from Node with ID[" + id + "]");
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public Collection<ValueTO> getData(Integer id) {

		if (id == null) {
			throw new IllegalInputParameterException("Supplied 'id' is null");
		}

		if (id < 1) {
			throw new IllegalInputParameterException("Supplied 'id' is negative or 0");
		}

		List<NhtInfoDO> nhtInfo = em.createNamedQuery("NhtInfoDO.by.nodeId", NhtInfoDO.class).setParameter("nodeId", id).getResultList();

		return fillNhtInfoCollectionToReturn(nhtInfo);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public Collection<ValueTO> getData(Integer id, int size, int start) {

		if (id == null) {
			throw new IllegalInputParameterException("Supplied 'id' is null");
		}

		if (id < 1) {
			throw new IllegalInputParameterException("Supplied 'id' is negative or 0");
		}

		if (size < 1) {
			throw new IllegalInputParameterException("Supplied 'size' is negative of 0");
		}

		if (start < 0) {
			throw new IllegalInputParameterException("Supplied 'start' is negative");
		}

		List<NhtInfoDO> networkStatistic = em.createNamedQuery("NhtInfoDO.by.nodeId", NhtInfoDO.class).setParameter("nodeId", id)
		        .setFirstResult(start).setMaxResults(size).getResultList();

		return fillNhtInfoCollectionToReturn(networkStatistic);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public Collection<ValueTO> getData(Integer id, Date startDate, Date endDate) {

		if (id == null) {
			throw new IllegalInputParameterException("Supplied 'id' is null");
		}

		if (id < 1) {
			throw new IllegalInputParameterException("Supplied 'id' is negative or 0");
		}

		if (startDate == null) {
			throw new IllegalInputParameterException("Supplied 'startDate' is null");
		}

		if (endDate == null) {
			throw new IllegalInputParameterException("Supplied 'endDate' is null");
		}

		if (endDate.before(startDate)) {
			throw new IllegalInputParameterException("Supplied time period is wrong : 'endDate' < 'startDate'");
		}

		List<NhtInfoDO> networkStatistic = em.createNamedQuery("NhtInfoDO.by.nodeId.and.between.start.and.end", NhtInfoDO.class)
		        .setParameter("nodeId", id).setParameter("start", startDate).setParameter("end", endDate)
		        .getResultList();

		return fillNhtInfoCollectionToReturn(networkStatistic);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public Collection<ValueTO> getData(Integer id, Date startDate, int numberPoints) {

		if (id == null) {
			throw new IllegalInputParameterException("Supplied 'id' is null");
		}

		if (id < 1) {
			throw new IllegalInputParameterException("Supplied 'id' is negative or 0");
		}

		if (startDate == null) {
			throw new IllegalInputParameterException("Supplied 'startDate' is null");
		}

		List<NhtInfoDO> networkStatistic = em.createNamedQuery("NhtInfoDO.by.nodeId.and.start", NhtInfoDO.class)
		        .setParameter("nodeId", id).setParameter("start", startDate).setMaxResults(numberPoints)
		        .getResultList();

		return fillNhtInfoCollectionToReturn(networkStatistic);
	}

	@Override
	public void removeData(Integer id) {
		em.createNamedQuery("NhtInfoDO.delete.by.nodeId").setParameter("nodeId", id).executeUpdate();
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public int getDataSize(Integer id) {
		return (Integer) em.createNamedQuery("NhtInfoDO.count.by.nodeId").setParameter("nodeId", id).getSingleResult();
	}

}
