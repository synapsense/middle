package com.synapsense.dal.generic.dao;

import com.synapsense.dto.TO;

/**
 * If object can be identified in the system it must provide implementation of
 * <code>Identifiable</code> interface that means it is possible to request this
 * object from the environment
 * 
 * @author shabanov
 * 
 * @param <PK_TYPE>
 */
public interface Identifiable<PK_TYPE> {
	/**
	 * Allows to get identifier of underlying entity
	 * 
	 * @return <code>TO</code> ID of object
	 */
	TO<PK_TYPE> getIdentity();
	// public ObjectType getType();
}
