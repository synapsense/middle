package com.synapsense.dal.generic.entities;

// Generated 04.06.2008 11:50:34 by Hibernate Tools 3.2.0.CR1

import java.util.HashSet;
import java.util.Set;

import com.synapsense.dal.generic.DTOCompatible;
import com.synapsense.dal.generic.dao.Identifiable;
import com.synapsense.dto.RuleType;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;

/**
 * Class <code>RuleTypeDO</code> represents Rule template
 * 
 */
public class RuleTypeDO implements Identifiable<Integer>, DTOCompatible<RuleType>, java.io.Serializable {

	private static final long serialVersionUID = -4730399012713353313L;
	/**
	 * PK
	 */
	private int ruleTypeId;

	/**
	 * Rule type name
	 */
	private String name;
	/**
	 * Type of rule 1 - cre 2 - dre
	 */
	private Integer type;
	/**
	 * Type of source that is stored in <code>source</code>
	 */
	private String sourceType;
	/**
	 * Source code of rule
	 */
	private String source;

	// Behavior in case of no src props
	private Integer methodBehavior;

	/**
	 * Domain specific language specific feature of drools
	 */
	private String dsl;
	/**
	 * Is rule precompiled means that rule is already compiled
	 */
	private Boolean precompiled;

	/**
	 * Rule recalculation interval
	 */
	private String ruleSchedulerConfig;
	/**
	 * Optimistic control counter
	 */
	private Integer version;
	/**
	 * Instances of rule type
	 */
	private Set<RuleDO> ruleInstances = new HashSet<RuleDO>(0);

	// bug#1529 new set
	/**
	 * attributes which have reference to rule type(means that they are
	 * calculated)
	 */
	private Set<AttributeDO> attributes = new HashSet<AttributeDO>(0);

	RuleTypeDO() {
	}

	public RuleTypeDO(String name, String sourceType, String source, Boolean precompiled, String dsl, Integer type,
	        Integer methodBehavior) {
		this.name = name;
		this.sourceType = sourceType;
		this.source = source;
		this.precompiled = precompiled;
		this.dsl = dsl;
		this.setType(type);
		this.methodBehavior = methodBehavior;
	}

	public int getRuleTypeId() {
		return ruleTypeId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSourceType() {
		return sourceType;
	}

	public void setSourceType(String sourceType) {
		this.sourceType = sourceType;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public Boolean getPrecompiled() {
		return precompiled;
	}

	public void setPrecompiled(Boolean precompiled) {
		this.precompiled = precompiled;
	}

	public Set<RuleDO> getRuleInstances() {
		return ruleInstances;
	}

	public void setRuleInstances(Set<RuleDO> ruleInstances) {
		this.ruleInstances = ruleInstances;
	}

	public String getDsl() {
		return dsl;
	}

	public void setDsl(String dsl) {
		this.dsl = dsl;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		/*
		 * if (type == null) throw new
		 * IllegalArgumentException("Supplied 'type' is null"); if (type != 1 ||
		 * type != 2) throw new
		 * IllegalArgumentException("Supplied 'type' can have only value: 1 or 2"
		 * );
		 */
		this.type = type;
	}

	@Override
	@SuppressWarnings("unchecked")
	public TO<Integer> getIdentity() {
		return (TO<Integer>) TOFactory.getInstance().loadTO("RULE_TYPE:" + this.getRuleTypeId());
	}

	@Override
	public String toString() {
		return "[RULE_TYPE:" + this.name + "]";
	}

	protected Integer getMethodBehavior() {
		return methodBehavior;
	}

	protected void setMethodBehavior(Integer methodBehavior) {
		this.methodBehavior = methodBehavior;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final RuleTypeDO other = (RuleTypeDO) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	/**
	 * @return the ruleSchedulerConfig
	 */
	public String getRuleSchedulerConfig() {
		return ruleSchedulerConfig;
	}

	/**
	 * @param ruleSchedulerConfig
	 *            the ruleSchedulerConfig to set
	 */
	public void setRuleSchedulerConfig(String ruleSchedulerConfig) {
		this.ruleSchedulerConfig = ruleSchedulerConfig;
	}

	public Set<AttributeDO> getAttributes() {
		return attributes;
	}

	public void setAttributes(Set<AttributeDO> attributes) {
		this.attributes = attributes;
	}

	@Override
	public RuleType getDTO() {
		synchronized (this) {
			if (type == 1)
				return new RuleType(this.getName(), this.getSource(), this.getSourceType(), this.getPrecompiled(),
				        this.getRuleSchedulerConfig(), this.getMethodBehavior());
			if (type == 2)
				return new RuleType(this.getName(), this.getSource(), this.getSourceType(), this.getDsl());

			throw new IllegalStateException("Data object is not properly configured. Wrong value of 'type' field");
		}
	}

	public int getVersion() {
		return version;
	}

	@Override
	public void setDTO(RuleType dto) {
		synchronized (this) {
			this.setName(dto.getName());
			this.setSource(dto.getSource());
			this.setSourceType(dto.getSourceType());
			this.setPrecompiled(dto.isPrecompiled());
			this.setRuleSchedulerConfig(dto.getRuleSchedulerConfig());
			this.setDsl(dto.getDsl());
			this.setMethodBehavior(dto.getMethodBehavior());
		}
	}

	@SuppressWarnings("unused")
	private void setRuleTypeId(int ruleTypeId) {
		this.ruleTypeId = ruleTypeId;
	}
}
