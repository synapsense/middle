package com.synapsense.dal.objectcache;

import com.synapsense.exception.InvalidValueException;

/**
 * Text value to store in the cache
 */
public class TextValue extends Value<String> {

    // mysql row cannot hold more than 64K data
    // that is where this limit come from
    // update it when update synap.valuev table
    private static final int MAX_LENGTH = 21833;


    TextValue() {
    }

    TextValue(String value) {
        this.setValue(value);
    }

    @Override
    public String getValue() {
        return this.data;
    }

    @Override
    public void setValue(String value) {
        // FIXME : WTH? Casts string to string?
        // looks ugly but this is reality of current design
        // Value has a template in declaration but clients ignore this fact
        String tempData = String.class.cast(value);
        if (tempData != null && tempData.length() > MAX_LENGTH) {
            throw new InvalidValueException("Specified text exceeds the limit of " + MAX_LENGTH + " characters");
        }
        this.data = tempData;
    }
}
