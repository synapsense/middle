package com.synapsense.dal.objectcache;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.synapsense.dal.objectcache.SimpleType.PropertyType;
import com.synapsense.dto.PropertyDescr;
import com.synapsense.dto.Tag;
import com.synapsense.dto.TagDescriptor;
import com.synapsense.dto.Types;
import com.synapsense.exception.*;
import com.synapsense.util.CollectionUtils;
import com.synapsense.util.Pair;
import org.apache.log4j.Logger;

public class SimpleObject {
    private final static Logger logger = Logger.getLogger(SimpleObject.class);

	private Integer oid;

	private SimpleType type;

	private Map<String, SimplePropertyValue> objectData;

	// key - property Name
	private Map<String, Map<String, Tag>> tagsData;

	private Set<Pair<String, Integer>> children;

	private Set<Pair<String, Integer>> parents;

	public SimpleObject(SimpleType type, int oid) {
		this.objectData = Collections.synchronizedMap(new HashMap<String, SimplePropertyValue>(0));
		this.children = Collections.synchronizedSet(new HashSet<Pair<String, Integer>>(0));
		this.parents = Collections.synchronizedSet(new HashSet<Pair<String, Integer>>(0));
		this.tagsData = Collections.synchronizedMap(new HashMap<String, Map<String, Tag>>(0));
		this.objectData.put(SimpleType.NUM_ALERTS_PROPERTY_NAME, new SimplePropertyValue(
		        SimpleType.NUM_ALERTS_PROPERTY_NAME, Value.createTypedValue(0)));

		this.type = type;
		this.oid = oid;
	}

	public Integer getOid() {
		return oid;
	}

	public Set<Pair<String, ?>> removeParents(String typeName) {
		Set<Pair<String, ?>> parentsToRemove = new HashSet<Pair<String, ?>>();
		for (Pair<String, ?> parent : this.getParents()) {
			if (Types.decode(parent.getFirst()) == Types.decode(typeName)) {
				parentsToRemove.add(parent);
			}
		}
		parents.removeAll(parentsToRemove);
		return parentsToRemove;
	}

	public Set<Pair<String, Integer>> getChildren() {
		return children;
	}

	public Set<Pair<String, Integer>> getParents() {
		return parents;
	}

	public SimplePropertyValue getPropertyValue(String propName) throws PropertyNotFoundException {
		SimplePropertyValue v = objectData.get(propName);
		if (v == null)
			throw new PropertyNotFoundException(Integer.toString(oid), propName);
		else
			return v;
	}

	public long getPropertyTimestamp(String propName) {// throws
		                                               // PropertyNotFoundException
		                                               // {
		SimplePropertyValue v = objectData.get(propName);
		if (v == null)
			return 0;
		else
			return v.getValue().getTimestamp();
	}

	@SuppressWarnings("unchecked")
	Object setPropertyValue(String propName, Object value, long timestamp) throws UnableToConvertPropertyException {
		SimplePropertyValue v = objectData.get(propName);
        if (v == null)
            return null;

        Object old = v.getValue().getValue();
        try {
            ((Value) v.getValue()).setValue(value);
        } catch (ClassCastException e) {
            throw new UnableToConvertPropertyException(propName, e);
        }
        v.getValue().setTimestamp(timestamp);
        return old;
	}

	public <VALUE_DATA> void setTagValue(String propName, String tagName, VALUE_DATA value)
	        throws UnableToConvertTagException, PropertyNotFoundException, TagNotFoundException {
		Map<String, Tag> propertyTags = tagsData.get(propName);
		if (propertyTags == null) {
			throw new PropertyNotFoundException(Integer.toString(oid), propName);
		}

		Tag tag = propertyTags.get(tagName);
		if (tag == null) {
			throw new TagNotFoundException(Integer.toString(oid), propName, tagName);
		}

		PropertyDescr envPropertyDescr = type.getEnvType().getPropertyDescriptor(propName);
		if (envPropertyDescr == null) {
			throw new PropertyNotFoundException(Integer.toString(oid), propName);
		}

		TagDescriptor td = envPropertyDescr.getTagDescriptor(tagName);
		if (td == null) {
			throw new TagNotFoundException(Integer.toString(oid), propName, tagName);
		}

		if (!value.getClass().getName().equals(td.getTypeName())) {
			throw new UnableToConvertTagException(tagName, value.getClass().getName(), td.getTypeName());
		}
		tag.setValue(value);
	}

	public <RETURN_TYPE> RETURN_TYPE getTagValue(String propName, String tagName, Class<RETURN_TYPE> clazz)
	        throws PropertyNotFoundException, TagNotFoundException, UnableToConvertTagException {
		Map<String, Tag> propertyTags = tagsData.get(propName);
		if (propertyTags == null) {
			throw new PropertyNotFoundException(Integer.toString(oid), propName);
		}

		Tag tag = propertyTags.get(tagName);
		if (tag == null) {
			throw new TagNotFoundException(Integer.toString(oid), propName, tagName);
		}
		RETURN_TYPE val = null;
		try {
			val = clazz.cast(tag.getValue());
		} catch (ClassCastException e) {
			throw new UnableToConvertTagException(tagName, clazz.getName(), tag.getValue().getClass().getName());
		}
		return val;
	}

	public Collection<Tag> getAllTags(String propName) throws PropertyNotFoundException {
		if (this.objectData.get(propName) == null) {
			throw new PropertyNotFoundException(Integer.toString(oid), propName);
		}
		Map<String, Tag> propertyTags = tagsData.get(propName);
		Collection<Tag> tags = new HashSet<Tag>();
		if (propertyTags != null) {
			tags.addAll(propertyTags.values());
		}
		return tags;
	}

	public Tag getTag(String propName, String tagName) throws PropertyNotFoundException, TagNotFoundException {
		Map<String, Tag> propertyTags = tagsData.get(propName);
		if (this.objectData.get(propName) == null) {
			throw new PropertyNotFoundException(Integer.toString(oid), propName);
		}
		if (propertyTags == null || !propertyTags.containsKey(tagName)) {
			throw new TagNotFoundException(propName, tagName);
		}

		return propertyTags.get(tagName);
	}

	public Map<String, Collection<Tag>> getTags(List<String> propertyNames, List<String> tagNames) {
		Map<String, Collection<Tag>> allTags = new HashMap<String, Collection<Tag>>();

		if (propertyNames == null || propertyNames.size() == 0) {
			propertyNames = new ArrayList<String>(tagsData.keySet());
		}
		if (tagNames == null || tagNames.size() == 0) {
			for (String propertyName : propertyNames) {
				if (tagsData.containsKey(propertyName)) {
					Collection<Tag> tags = tagsData.get(propertyName).values();
					allTags.put(propertyName, new ArrayList<Tag>(tags));
				}
			}
		} else {
			for (String propertyName : propertyNames) {
				Collection<Tag> tags = new ArrayList<Tag>();
				for (String tagName : tagNames) {
					try {
						tags.add(getTag(propertyName, tagName));
					} catch (EnvException e) {
                        logger.warn("Could not get tag " + propertyName + ":" + tagName, e);
					}
				}
				allTags.put(propertyName, tags);
			}
		}
		return allTags;
	}

	public Collection<Tag> getAllTags() {
		Collection<Map<String, Tag>> propertyTags = tagsData.values();
		if (propertyTags == null) {
			return null;
		}
		Collection<Tag> tags = new HashSet<Tag>();
		for (Map<String, Tag> propTags : propertyTags) {
			tags.addAll(propTags.values());
		}
		return tags;

	}

	public void setPropertyTimestamp(String propName, Date timestamp) throws PropertyNotFoundException {
		setPropertyTimestamp(propName, timestamp.getTime());
	}

	public void setPropertyTimestamp(String propName, long timestamp) {
		SimplePropertyValue v = objectData.get(propName);
		if (v != null)
			v.getValue().setTimestamp(timestamp);

	}

	public void addProperty(String propName, PropertyType type) {
		if (!objectData.containsKey(propName))
			switch (type.getType()) {
			case HISTORICAL:
			case NORMAL:
				objectData.put(propName, new SimplePropertyValue(propName, type.getClazz()));
				break;
			case COLLECTION:
                objectData.put(propName, new SimplePropertyValue(propName, Value.createTypedValue(
                        CollectionUtils.newSet())));
                break;
			default:
				break;
			}
		createTags(propName);
	}

	public void removeProperty(String propName) {
		objectData.remove(propName);
		tagsData.remove(propName);
	}

	public boolean isComplain(Map<String, Value<?>> conditions) {
		for (Map.Entry<String, Value<?>> entry : conditions.entrySet()) {
			try {
				Value cacheValue = getPropertyValue(entry.getKey()).getValue();
				if (!cacheValue.equals(entry.getValue()))
					return false;
			} catch (PropertyNotFoundException e) {
				return false;
			}
		}

		return true;
	}

	private void createTags(String propName) {
		PropertyDescr pd = type.getEnvType().getPropertyDescriptor(propName);
		if (pd == null) {
			return;
		}
		Set<TagDescriptor> tds = pd.getTagDescriptors();
		if (tds == null || tds.isEmpty()) {
			return;
		}
		Map<String, Tag> propertyTags = new HashMap<String, Tag>(0);
		for (TagDescriptor td : tds) {
			propertyTags.put(td.getName(), new Tag(propName, td.getName(), null));
		}
		tagsData.put(propName, propertyTags);
	}

	public Collection<SimplePropertyValue> getAllPropertiesValues() {
		Collection<SimplePropertyValue> res = new LinkedList<SimplePropertyValue>();
		res.addAll(objectData.values());
		return res;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder(500);
		sb.append("OID [");
		sb.append(oid);
		sb.append("]\n Properties:\n");
		for (SimplePropertyValue spv : objectData.values()) {
			sb.append(spv);
			sb.append('\n');
		}

		sb.append("Parent:\n");
		sb.append(this.parents);

		sb.append("\nChildren:\n");
		sb.append(this.children);
		sb.append('\n');

		return sb.toString();
	}

	void updatePropertyTags(String propName) {
		PropertyDescr pd = type.getEnvType().getPropertyDescriptor(propName);
		Set<TagDescriptor> tds = pd.getTagDescriptors();

		if (tds.size() == 0) {
			tagsData.remove(propName);
		} else {
			Map<String, Tag> tags = tagsData.get(propName);
			if (tags == null) {
				tags = new HashMap<String, Tag>(0);
				tagsData.put(propName, tags);
			}
			Set<String> tagssToRemove = new HashSet<String>();
			Set<String> tagDescrNames = new HashSet<String>();
			for (TagDescriptor td : tds) {
				if (!tags.containsKey((td.getName()))) {
					tags.put(td.getName(), new Tag(propName, td.getName(), null));
				}
				tagDescrNames.add(td.getName());
			}

			for (String tagName : tags.keySet()) {
				if (!tagDescrNames.contains(tagName))
					tagssToRemove.add(tagName);
			}

			for (String tagName : tagssToRemove) {
				tags.remove(tagName);
			}
		}
	}

}
