package com.synapsense.dal.generic.utils;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.Query;

class BatchQueryStaticPiecesStrategy implements BatchQueryStrategy {

	private int singlePieceSize;

	public BatchQueryStaticPiecesStrategy(int singlePieceSize) {
		this.singlePieceSize = singlePieceSize;
	}

	@Override
	public <ENTITY> List<ENTITY> getResultList(Query query, String paramName, Collection<?> value) {
		// divide input collection to pieces by singlePieceSize
		List<?> pieces = Util.divideByPieces(value, singlePieceSize);

		// getting result by pieces
		List<ENTITY> result = new LinkedList<ENTITY>();
		Iterator<?> piecesIterator = pieces.iterator();
		while (piecesIterator.hasNext()) {
			query.setParameter(paramName, piecesIterator.next());
			result.addAll(query.getResultList());
		}

		return result;
	}
}
