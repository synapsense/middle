package com.synapsense.dal.generic.entities;

import java.io.Serializable;
import java.util.Date;

import com.synapsense.dto.BinaryData;

/**
 * @author Vadim Gusev
 * @since Europa
 *        Date: 06.06.13
 *        Time: 14:23
 */
public class HistoricalBinaryValueDO implements Serializable {

	private static final long serialVersionUID = -1118243377003062281L;
	
	private HistoricalBinaryValueId id;
	
	private BinaryData valueb;
	
	HistoricalBinaryValueDO() {
	}
	
	public HistoricalBinaryValueDO(HistoricalBinaryValueId id) {
		this.id = id;
	}

	public HistoricalBinaryValueDO(int value, Date date) {
		this(new HistoricalBinaryValueId(value, date));
	}
	
	public HistoricalBinaryValueDO(ValueDO valueDo) {
		this(new HistoricalBinaryValueId(valueDo.getValueId(), new Date(valueDo.getTimestamp())));
		setValueb((BinaryData) valueDo.getValue().iterator().next());
	}
	
	public int getValue() {
		return id.getValue();
	}

	public Date getTimestamp() {
		return id.getTimestamp();
	}
	
	public BinaryData getValueb(){
		return valueb;
	}
	
	public void setValueb(BinaryData valueb){
		this.valueb = valueb;
	}
	
	public HistoricalBinaryValueId getId() {
		return id;
	}

	public void setId(HistoricalBinaryValueId id) {
		this.id = id;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final HistoricalBinaryValueDO other = (HistoricalBinaryValueDO) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "HistoricalBinaryValueDO [getValue()=" + getValue() + ", getTimestamp()=" + getTimestamp().getTime() + "]";
	}
}
