package com.synapsense.dal.generic.dao;

import java.util.Collection;
import java.util.LinkedList;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;

import org.apache.log4j.Logger;

import com.synapsense.dal.generic.DTOCompatible;
import com.synapsense.dal.generic.entities.RuleTypeDO;
import com.synapsense.dal.generic.utils.Util;
import com.synapsense.dto.RuleType;
import com.synapsense.dto.TO;
import com.synapsense.exception.ObjectExistsException;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.service.RuleTypeService;

@Stateless
@Remote(RuleTypeService.class)
public class RuleTypeObjectDAO extends AbstractRuleType implements RuleTypeService<Integer> {
	private final static Logger log = Logger.getLogger(RuleTypeObjectDAO.class);

	/**
	 * defines what rule type is ... [Drools rule or CRE rule]
	 */
	private static final int DISCRIMINATOR = 1;

	@PersistenceContext(unitName = "HibernateDAL")
	@Override
	public void setEM(EntityManager em) {
		this.em = em;
	}

	private EntityManager em;

	@Override
	public TO<Integer> create(RuleType config) throws ObjectExistsException {
		RuleTypeDO ruleTypeDO = new RuleTypeDO(config.getName(), config.getSourceType(), config.getSource(),
		        config.isPrecompiled(), config.getDsl(), this.getDiscriminator(), config.getMethodBehavior());

		ruleTypeDO.setRuleSchedulerConfig(config.getRuleSchedulerConfig());

		try {
			em.persist(ruleTypeDO);
			em.flush();
		} catch (PersistenceException e) {
			throw new ObjectExistsException("RULE_TYPE : " + ruleTypeDO.toString() + " already exists");
		}
		if (log.isDebugEnabled()) {
			log.debug("Trying to create RULE_TYPE with configuration [" + config.toString() + "]");
		}
		return ruleTypeDO.getIdentity();
	}

	@Override
	public void delete(Integer id) {
		RuleTypeDO ruleTypeDO = findRuleTypeById(id);
		if (ruleTypeDO == null) {
			if (log.isDebugEnabled()) {
				log.debug("RULE_TYPE with ID [" + id + "] does not exist");
			}
			return;
		}
		em.remove(ruleTypeDO);
	}

	@Override
	public void deleteAll() {
		Collection<RuleTypeDO> allRules = this.findAllRuleTypes();
		if (log.isDebugEnabled()) {
			log.debug("Trying to delelte all rules");
		}
		for (RuleTypeDO ruleType : allRules) {
			// bug#1529
			delete(ruleType.getRuleTypeId());
		}
	}

	@Override
	public Collection<TO<Integer>> getAllRuleTypes() {
		Collection<TO<Integer>> result = new LinkedList<TO<Integer>>();
		Collection<RuleTypeDO> ruleTypeDOs = this.findAllRuleTypes();
		for (RuleTypeDO ruleType : ruleTypeDOs) {
			result.add(ruleType.getIdentity());
		}
		return result;
	}

	@Override
	public Collection<RuleType> getAllRuleTypesDTOs() {
		Collection<RuleType> result = new LinkedList<RuleType>();
		Collection<RuleTypeDO> ruleTypeDOs = this.findAllRuleTypes();
		for (DTOCompatible<RuleType> ruleTypeDO : ruleTypeDOs) {
			result.add(ruleTypeDO.getDTO());
		}
		return result;
	}

	@Override
	public String getName(Integer id) throws ObjectNotFoundException {
		RuleTypeDO ruleTypeDO = findRuleTypeById(id);
		Util.assertNotNullEntity("Rule type with id : " + id.toString() + " does not exist", ruleTypeDO);
		return ruleTypeDO.getName();
	}

	@Override
	public TO<Integer> getRuleTypeByName(String name) {
		RuleTypeDO ruleTypeDO = findRuleTypeByName(name);
		return ruleTypeDO == null ? null : ruleTypeDO.getIdentity();
	}

	@Override
	public RuleType getRuleTypeDTOByName(String name) {
		RuleTypeDO ruleTypeDO = findRuleTypeByName(name);
		return ruleTypeDO == null ? null : ruleTypeDO.getDTO();
	}

	@Override
	public TO<Integer> getRuleTypeByID(Integer id) {
		RuleTypeDO ruleTypeDO = findRuleTypeById(id);
		return ruleTypeDO == null ? null : ruleTypeDO.getIdentity();
	}

	@Override
	public String getSource(Integer id) throws ObjectNotFoundException {
		RuleTypeDO ruleTypeDO = findRuleTypeById(id);
		Util.assertNotNullEntity("Rule type with id : " + id.toString() + " does not exist", ruleTypeDO);
		return ruleTypeDO.getSource();
	}

	@Override
	public String getSourceType(Integer id) throws ObjectNotFoundException {
		RuleTypeDO ruleTypeDO = findRuleTypeById(id);
		Util.assertNotNullEntity("Rule type with id : " + id.toString() + " does not exist", ruleTypeDO);
		return ruleTypeDO.getSourceType();
	}

	@Override
	public Boolean isPrecompiled(Integer id) throws ObjectNotFoundException {
		RuleTypeDO ruleTypeDO = findRuleTypeById(id);
		Util.assertNotNullEntity("Rule type with id : " + id.toString() + " does not exist", ruleTypeDO);
		return ruleTypeDO.getPrecompiled();
	}

	@Override
	public void setName(Integer id, String name) throws ObjectNotFoundException, ObjectExistsException {
		RuleTypeDO ruleTypeDO = findRuleTypeById(id);
		Util.assertNotNullEntity("Rule type with id : " + id.toString() + " does not exist", ruleTypeDO);
		RuleTypeDO ruleTypeByNameDO = findRuleTypeByName(name);
		Util.assertNullEntity("Rule type with name  : " + name.toString() + " already  exists", ruleTypeByNameDO);
		ruleTypeDO.setName(name);
	}

	@Override
	public void setPrecompiled(Integer id, boolean isPrecompiled) throws ObjectNotFoundException {
		RuleTypeDO ruleTypeDO = findRuleTypeById(id);
		Util.assertNotNullEntity("Rule type with id : " + id.toString() + " does not exist", ruleTypeDO);
		ruleTypeDO.setPrecompiled(isPrecompiled);
	}

	@Override
	public void setSource(Integer id, String source) throws ObjectNotFoundException {
		RuleTypeDO ruleTypeDO = findRuleTypeById(id);
		Util.assertNotNullEntity("Rule type with id : " + id.toString() + " does not exist", ruleTypeDO);
		ruleTypeDO.setSource(source);
	}

	@Override
	public void setSourceType(Integer id, String sourceType) throws ObjectNotFoundException {
		RuleTypeDO ruleTypeDO = findRuleTypeById(id);
		Util.assertNotNullEntity("Rule type with id : " + id.toString() + " does not exist", ruleTypeDO);
		ruleTypeDO.setSourceType(sourceType);
	}

	@Override
	protected Integer getDiscriminator() {
		return RuleTypeObjectDAO.DISCRIMINATOR;
	}

	@Override
	public String getRuleRunConig(Integer id) throws ObjectNotFoundException {
		RuleTypeDO ruleTypeDO = findRuleTypeById(id);
		Util.assertNotNullEntity("Rule type with id : " + id.toString() + " does not exist", ruleTypeDO);
		return ruleTypeDO.getRuleSchedulerConfig();
	}

	@Override
	public void setRuleRunConig(Integer id, String config) throws ObjectNotFoundException {
		RuleTypeDO ruleTypeDO = findRuleTypeById(id);
		Util.assertNotNullEntity("Rule type with id : " + id.toString() + " does not exist", ruleTypeDO);
		ruleTypeDO.setRuleSchedulerConfig(config);
	}

	@Override
	public EntityManager getEM() {
		return this.em;
	}

}
