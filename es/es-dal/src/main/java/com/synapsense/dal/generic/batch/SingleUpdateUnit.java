package com.synapsense.dal.generic.batch;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

import com.synapsense.dto.BinaryData;
import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;
import com.synapsense.util.Pair;

public class SingleUpdateUnit {
	/**
	 * ID of updated object
	 */
	private TO<?> to;
	/**
	 * property name of updated object
	 */
	private String propertyName;
	/**
	 * values which should be saved
	 */
	private List<ValueTO> values;
	private boolean timestampOnly;
	private Integer groupId;

	public Integer getGroupId() {
    	return groupId;
    }

	private SingleUpdateUnit(final TO<?> to, final String propertyName, final List<ValueTO> values, boolean timestampOnly, Integer groupId) {
		if (to == null)
			throw new IllegalArgumentException("Supplied 'to' is null");
		if (propertyName == null)
			throw new IllegalArgumentException("Supplied 'propertyName' is null");
		if (values == null)
			throw new IllegalArgumentException("Supplied 'values' is null");
		if (values.isEmpty())
			throw new IllegalArgumentException("Supplied 'values' is empty.Provide at least one value.");
		if (groupId == null)
			throw new IllegalArgumentException("Supplied 'groupId' is null");
		this.to = to;
		this.propertyName = propertyName;
		this.values = new LinkedList<ValueTO>(values);
		this.timestampOnly = timestampOnly;
		this.groupId = groupId;
	}

	public SingleUpdateUnit(final TO<?> to, final String propertyName, final ValueTO value, boolean timestampOnly, Integer groupId) {
		this(to, propertyName, Arrays.asList(value), timestampOnly, groupId);
	}

	public TO<?> getTo() {
		return to;
	}

	public String getPropertyName() {
		return propertyName;
	}

	public List<ValueTO> getValues() {
		return values;
	}

	public void addValue(final ValueTO val) {
		if (val == null)
			throw new NullPointerException("ValueTO is null");
		// if (val.getValue()==null) throw new
		// NullPointerException("Value is null");
		this.values.add(val);
	}

	public Pair<TO<?>, String> getKey() {
		return Pair.<TO<?>, String>newPair(this.to, this.propertyName);
	}

	public boolean containsBlobs() {
		for (ValueTO value : values) {
			if (value.getValue() instanceof BinaryData) {
				return true;
			}
		}
		return false;
	}

	public ValueTO getLast() {
		ListIterator<ValueTO> reverse = values.listIterator(values.size());
		return reverse.previous();
	}

	@Override
	public String toString() {
		return "{oid:" + to + ",value:" + values + "}";
	}

	public boolean isTimestampOnly() {
		return timestampOnly;
	}

	public void setTimestampOnly(boolean timestampOnly) {
		this.timestampOnly = timestampOnly;
	}
}
