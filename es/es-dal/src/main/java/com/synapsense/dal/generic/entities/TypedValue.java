package com.synapsense.dal.generic.entities;

import java.util.Collection;

public final class TypedValue {

	private Object value;
	private Class<?> type;
	private boolean isCollection;

	public TypedValue(Class<?> type)  {
		this.type = type;
	}

	public void setValue(Object value) throws ClassCastException { 
		if (value == null) {
			return;
		}
	
		Object temp  = value;
		
		this.isCollection = value instanceof Collection;
		
		if (isCollection() && (Collection.class.cast(value)).isEmpty()) {
			this.value = value;
			return;
		}
		
		if(isCollection) { 
			temp = (Collection.class.cast(value)).iterator().next();
		}
		
		checkCompatibility(temp);
		
		this.value = value;
	}

	public boolean isA(Class<?> clazz) {
		return type.isAssignableFrom(clazz);
	}

	public boolean isCollection() {
		return isCollection;
	}

	@Override
	public String toString() {
		return "TypedValue [value=" + value + ", type=" + type + "]";
	}

	public Object getValue() {
		return value;
	}

	public Class<?> getType() {
		return type;
	}
	
	private void checkCompatibility(Object value) { 
		if (!this.type.isInstance(value)) {
			throw new ClassCastException("Specified value {" + String.valueOf(value)
			        + "} cannot be assigned to property of type {" + type + "} because of property type mismatch");
		}
	}
}
