package com.synapsense.dal.generic.reporting;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import com.synapsense.dal.generic.DAOGenericException;
import com.synapsense.service.reporting.exception.ReportingQueryException;

@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class ReportingQueryExecutorImpl implements ReportingQueryExecutor {
	
	private EntityManager em;
	
	private static final Logger log = Logger.getLogger(ReportingQueryExecutorImpl.class);
	

	@PersistenceContext(unitName = "HibernateDAL")
	public void setEM(final EntityManager em) {
		this.em = em;
	}
	
	@Override
	public List<Object[]> executeHQLQuery(final String queryString, final Map<Integer, List<Integer>> parameterMap) {
		
		if (queryString == null) {
			throw new IllegalArgumentException("Query String shouldn't be null");
		}
		if (parameterMap == null) {
			throw new IllegalArgumentException("Parameter Map shouldn't be null");
		}
		if (parameterMap.isEmpty()) {
			throw new IllegalArgumentException("ParameterMap shouldn't be empty");
		}
		Query qValueId;
		try {
			qValueId = em.createQuery(queryString);
		} catch (HibernateException e) {
			throw new DAOGenericException("Wrong query [" + queryString + "]!", e);
		}
		
		if (parameterMap != null) {
			for (Entry<Integer, List<Integer>> entry : parameterMap.entrySet()) {
				qValueId.setParameter("objectIds" + entry.getKey(), entry.getValue());
			}
		}
		if (log.isDebugEnabled()) {
			log.debug("Executing GetValueIdsTask Query...");
		}
		try {
			return qValueId.getResultList();
		} catch (HibernateException e) {
			throw new ReportingQueryException("Failed to execute HQL query " + queryString, e);
		}
		
	}
	
	@Override
	public List<Object[]> executeSQLQuery(final String queryString, final Map<String, Collection<Integer>> collectionParameterMap) {

		if (queryString == null) {
			throw new IllegalArgumentException("Query string shouldn't be null");
		}
		if (collectionParameterMap == null) {
			throw new IllegalArgumentException("Parameter Map shouldn't be null");
		}
		if (collectionParameterMap.isEmpty()) {
			throw new IllegalArgumentException("ParameterMap shouldn't be empty");
		}
		Session session = (Session) em.getDelegate();
		SQLQuery sqlQuery;
		try {
			sqlQuery = session.createSQLQuery(queryString);
		} catch (HibernateException e) {
			throw new DAOGenericException("Wrong query [" + queryString + "]!", e);
		}
		
		for (Entry<String, Collection<Integer>> entry : collectionParameterMap.entrySet()) {
			sqlQuery.setParameterList(entry.getKey(), entry.getValue());
		}
		
		if (log.isTraceEnabled()) {
			log.trace(queryString);
		}
		if (log.isDebugEnabled()) {
			log.debug("Executing GetEnvDataTask Query...");
		}
		try {
			return sqlQuery.list();
		} catch (HibernateException e) {
			throw new ReportingQueryException("Failed to run SQL query " + queryString, e);
		}

		
	}

}
