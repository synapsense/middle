package com.synapsense.dal.generic.dao;

import javax.ejb.Remote;
import javax.ejb.Stateless;

import com.synapsense.dal.generic.entities.RuleTypeDO;
import com.synapsense.dal.generic.utils.Util;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.service.DRuleTypeService;

@Stateless
@Remote(DRuleTypeService.class)
public class DRuleTypeObjectDAO extends RuleTypeObjectDAO implements DRuleTypeService<Integer> {

	private static final int DISCRIMINATOR = 2;

	@Override
	protected Integer getDiscriminator() {
		return DRuleTypeObjectDAO.DISCRIMINATOR;
	}

	@Override
	public String getDSL(Integer id) throws ObjectNotFoundException {
		RuleTypeDO ruleTypeDO = findRuleTypeById(id);
		Util.assertNotNullEntity("DRule with id : " + id.toString() + " does not exist", ruleTypeDO);
		return ruleTypeDO.getDsl();
	}

	@Override
	public void setDSL(Integer id, String dsl) throws ObjectNotFoundException {
		RuleTypeDO ruleTypeDO = findRuleTypeById(id);
		Util.assertNotNullEntity("Rule type with id : " + id.toString() + " does not exist", ruleTypeDO);
		ruleTypeDO.setDsl(dsl);
	}
}
