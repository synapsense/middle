package com.synapsense.dal.generic.batch.loader;

public class HistoricalDataLoaderException extends RuntimeException {

	public HistoricalDataLoaderException(Throwable e) {
		super(e);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -4794658833569532167L;

}
