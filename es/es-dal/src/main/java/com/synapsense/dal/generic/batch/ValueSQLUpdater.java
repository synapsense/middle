package com.synapsense.dal.generic.batch;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.TransactionRequiredException;

import org.apache.log4j.Logger;

import com.synapsense.dal.generic.entities.AbstractValue;
import com.synapsense.dal.generic.entities.AttributeDO;
import com.synapsense.dal.generic.entities.ObjectDO;
import com.synapsense.dal.generic.entities.TypedValue;
import com.synapsense.dal.generic.entities.ValueDO;
import com.synapsense.dal.generic.utils.Util;
import com.synapsense.dto.BinaryData;
import com.synapsense.dto.TO;
import com.synapsense.exception.UnableToConvertPropertyException;
import com.synapsense.service.impl.dao.to.PropertyTO;

class ValuesSQLUpdater {
	private static final Logger logger = Logger.getLogger(ValuesSQLUpdater.class);

	private volatile boolean batchInProgress = false;

	// private PreparedStatement insertObjValue;
	private PreparedStatement updateObjValue;

	private Map<String, PreparedStatement> statements = new LinkedHashMap<String, PreparedStatement>();

	private Connection connection;
	private EntityManager em;

	public ValuesSQLUpdater(Connection conn, EntityManager em, int insertBufferSize) {
		this.connection = conn;
		this.em = em;
	}

	public void startBatch() {
		if (batchInProgress)
			throw new IllegalStateException("This batch operation is already started, commit first");
		logger.debug("Starting batch operation...");

		try {
			// required to work inside single transaction for all db access
			// operations
			em.joinTransaction();
		} catch (TransactionRequiredException e) {
			throw new IllegalStateException("The batch operation cannot start because there is no active transaction!",
			        e);
		}

		try {
			allocateResources();
		} catch (SQLException e) {
			disposeResources();
			throw new IllegalStateException("Cannot allocate resources due to", e);
		}
		batchInProgress = true;
	}

	public void commit() throws SQLException {
		if (!batchInProgress)
			throw new IllegalStateException("Batch operation is not started");

		logger.debug("Commiting changes of batch operation...");
		try {
			pushChanges();
			em.flush();
			em.clear();
			logger.debug("Batch operation is committed");
		} finally {
			disposeResources();
			batchInProgress = false;
		}

	}

	public int insertValue(int objId, int attrId, long timestamp, final TypedValue value) {
		if (!batchInProgress)
			throw new IllegalStateException("This batch operation is not started. Call startBatch() first");

		if (!checkValueIsValid(value)) {
			logger.warn("Cannot insert new value [objId=" + objId + ", attrId=" + attrId + ", value=" + value + "]");
			return -1;
		}

		if (logger.isTraceEnabled()) {
			logger.trace("Inserting value [objId=" + objId + ", attrId=" + attrId + ",value=" + value + "]");
		}

		try {
			int objValueId = newRootValue(objId, attrId, timestamp);
			insertNativeValue(objValueId, value);
			return objValueId;
		} catch (SQLException e) {
			logger.warn("Cannot insert new value [objId=" + objId + ", attrId=" + attrId + ", value=" + value
			        + "] due to ", e);
			return -1;
		}
	}

	public void updateTimestamp(int valId, long timestamp) {
		try {
			updateTimestampOnly(valId, timestamp);
		} catch (SQLException e) {
			logger.warn("Cannot update timestamp of value [refId=" + valId + "] due to", e);
		}
	}

	public void updateValue(int valId, long timestamp, final TypedValue value) {
		if (!batchInProgress)
			throw new IllegalStateException("Not started");

		if (!checkValueIsValid(value)) {
			logger.warn("Cannot update value [ref=" + valId + ", value=" + value + "]");
			return;
		}

		if (logger.isTraceEnabled()) {
			logger.trace("Updating value  [ref=" + valId + ",value=" + value + "]");
		}

		try {
			boolean isCollection = value.isCollection();

			if (isCollection) {
				deleteNativeValue(valId, value);
				insertNativeValue(valId, value);
				updateTimestampOnly(valId, timestamp);
			} else {
				updateNativeValue(valId, timestamp, value);
			}

		} catch (SQLException e) {
			logger.warn("Cannot update value [refId=" + valId + ",value=" + value + "] due to", e);
		}

	}

	private void updateNativeValue(int valId, long timestamp, final TypedValue value) throws SQLException {
		PreparedStatement stmtToUpdate = getUpdateStatement(value);
		if (value.isA(TO.class)) {
			final TO<?> link = (TO<?>) value.getValue();
			stmtToUpdate.setLong(1, timestamp);
			stmtToUpdate.setObject(2, link == null ? null : (Integer) link.getID());
			stmtToUpdate.setString(3, link == null ? null : link.getTypeName());
			stmtToUpdate.setInt(4, valId);
		} else if (value.isA(PropertyTO.class)) {
			final PropertyTO link = (PropertyTO) value.getValue();
			stmtToUpdate.setLong(1, timestamp);
			stmtToUpdate.setObject(2, link == null ? null : (Integer) link.getObjId().getID());
			stmtToUpdate.setString(3, link == null ? null : link.getObjId().getTypeName());
			stmtToUpdate.setString(4, link == null ? null : link.getPropertyName());
			stmtToUpdate.setInt(5, valId);
		} else if (value.isA(BinaryData.class)) {
			final BinaryData bData = (BinaryData) value.getValue();
			stmtToUpdate.setLong(1, timestamp);
			stmtToUpdate.setObject(2, bData == null ? null : bData.getValue());
			stmtToUpdate.setInt(3, valId);
		} else {
			stmtToUpdate.setLong(1, timestamp);
			stmtToUpdate.setObject(2, value.getValue());
			stmtToUpdate.setInt(3, valId);
		}
		stmtToUpdate.addBatch();
	}

	private void updateTimestampOnly(int valId, long timestamp) throws SQLException {
		PreparedStatement stmtToUpdate = updateObjValue;
		stmtToUpdate.setLong(1, timestamp);
		stmtToUpdate.setInt(2, valId);
		stmtToUpdate.addBatch();
	}

	private void insertNativeValue(int valueId, final TypedValue value) throws SQLException {
		ValueDO valueDO = em.getReference(ValueDO.class, valueId);

		List<AbstractValue> abstractValues = null;
		try {
			abstractValues = Util.createNewTypedValues(valueDO, value);
			for (AbstractValue valueToPersist : abstractValues) {
				em.persist(valueToPersist);
			}
		} catch (UnableToConvertPropertyException e) {
			logger.warn("Cannot insert native value", e);
		}

	}

	private void deleteNativeValue(int valId, final TypedValue value) throws SQLException {
		PreparedStatement stmtToUpdate = getDeleteStatement(value);
		stmtToUpdate.setInt(1, valId);
		stmtToUpdate.addBatch();
	}

	private boolean checkValueIsValid(TypedValue value) {
		if (value == null) {
			return false;
		}

		return true;
	}

	private void pushChanges() throws SQLException {
		logger.debug("Sending batch to jdbc driver...");
		
		int[] udpatedRows = null;
		
		for (Map.Entry<String, PreparedStatement> entry : statements.entrySet()) {
			udpatedRows = entry.getValue().executeBatch();
			int counter = Util.countArray(udpatedRows);
			if (counter > 0)
				logger.debug("Prepared statement : " + entry.getKey() + " was pushed to database, updated "
				        + counter + " rows");
		}

		logger.debug("UPDATING only timestamps of properties that were not changed");
		udpatedRows = updateObjValue.executeBatch();
		
		int counter = Util.countArray(udpatedRows);
		if (counter>0)
			logger.debug("UPDATE timestamps " + "was pushed to database, updated " + counter + " rows");

		logger.debug("Batch was successfully pushed into jdbc driver");

	}

	private void allocateResources() throws SQLException {
		updateObjValue = connection.prepareStatement("update obj_values set timestamp = ? where value_id = ?");

		// deletes
		statements.put("DELETE_VARCHAR", connection.prepareStatement("delete from valuev where value_id_fk =?"));

		statements.put("DELETE_INTEGER", connection.prepareStatement("delete from valuei where value_id_fk =?"));

		statements.put("DELETE_DOUBLE", connection.prepareStatement("delete from valued where value_id_fk =?"));

		statements.put("DELETE_LONG", connection.prepareStatement("delete from valuel where value_id_fk =?"));

		statements.put("DELETE_LINK", connection.prepareStatement("delete from value_obj_link where value_id_fk =?"));

		statements.put("DELETE_PLINK",
		        connection.prepareStatement("delete from value_property_link where value_id_fk =?"));

		statements.put("DELETE_BINARY", connection.prepareStatement("delete from value_binary where value_id_fk =?"));

		// updates
		statements
		        .put("UPDATE_VARCHAR",
		                connection
		                        .prepareStatement("update obj_values ov, valuev val set ov.timestamp = ?, val.value = ? where ov.value_id = val.value_id_fk "
		                                + "and ov.value_id = ?"));
		statements
		        .put("UPDATE_INTEGER",
		                connection
		                        .prepareStatement("update obj_values ov, valuei val set ov.timestamp = ?, val.value = ? where ov.value_id = val.value_id_fk "
		                                + "and ov.value_id = ?"));
		statements
		        .put("UPDATE_DOUBLE",
		                connection
		                        .prepareStatement("update obj_values ov, valued val set ov.timestamp = ?, val.value = ? where ov.value_id = val.value_id_fk "
		                                + "and ov.value_id = ?"));
		statements
		        .put("UPDATE_LONG",
		                connection
		                        .prepareStatement("update obj_values ov, valuel val set ov.timestamp = ?, val.value = ? where ov.value_id = val.value_id_fk "
		                                + "and ov.value_id = ?"));
		statements
		        .put("UPDATE_LINK",
		                connection
		                        .prepareStatement("update obj_values ov, value_obj_link val set ov.timestamp = ?, val.object_id = ? , val.object_type = ? where ov.value_id = val.value_id_fk "
		                                + "and ov.value_id = ?"));
		statements
		        .put("UPDATE_PLINK",
		                connection
		                        .prepareStatement("update obj_values ov, value_property_link val set ov.timestamp = ?, val.object_id = ? , val.object_type = ? , val.property_name = ? where ov.value_id = val.value_id_fk "
		                                + "and ov.value_id = ?"));
		statements
		        .put("UPDATE_BINARY",
		                connection
		                        .prepareStatement("update obj_values ov, value_binary val set ov.timestamp = ?, val.value = ? where ov.value_id = val.value_id_fk "
		                                + "and ov.value_id = ?"));
	}

	private void disposeResources() {
		logger.debug("Disposing allocated resources...");

		if (updateObjValue != null) {
			try {
				updateObjValue.close();
			} catch (SQLException e) {
				logger.warn("Cannot close prepared statement properly", e);
			}
		}

		for (PreparedStatement ps : statements.values()) {
			try {
				ps.close();
			} catch (SQLException e) {
				logger.warn("Cannot close prepared statement properly", e);
			}
		}
	}

	private PreparedStatement getUpdateStatement(final TypedValue value) {
		if (value.isA(Integer.class)) {
			return getStatement("UPDATE_INTEGER");
		}
		if (value.isA(Double.class)) {
			return getStatement("UPDATE_DOUBLE");
		}
		if (value.isA(Long.class)) {
			return getStatement("UPDATE_LONG");
		}
		if (value.isA(String.class)) {
			return getStatement("UPDATE_VARCHAR");
		}
		if (value.isA(TO.class)) {
			return getStatement("UPDATE_LINK");
		}
		if (value.isA(PropertyTO.class)) {
			return getStatement("UPDATE_PLINK");
		}
		if (value.isA(BinaryData.class)) {
			return getStatement("UPDATE_BINARY");
		}

		throw new IllegalArgumentException("Not supported type of value : " + value.getClass());
	}

	private PreparedStatement getDeleteStatement(final TypedValue value) {
		if (value.isA(Integer.class)) {
			return getStatement("DELETE_INTEGER");
		}
		if (value.isA(Double.class)) {
			return getStatement("DELETE_DOUBLE");
		}
		if (value.isA(Long.class)) {
			return getStatement("DELETE_LONG");
		}
		if (value.isA(String.class)) {
			return getStatement("DELETE_VARCHAR");
		}
		if (value.isA(TO.class)) {
			return getStatement("DELETE_LINK");
		}
		if (value.isA(PropertyTO.class)) {
			return getStatement("DELETE_PLINK");
		}
		if (value.isA(BinaryData.class)) {
			return getStatement("DELETE_BINARY");
		}

		throw new IllegalArgumentException("Not supported type of value : " + value.getClass());
	}

	private PreparedStatement getStatement(String name) {
		return statements.get(name);
	}

	private int newRootValue(int objId, int attrId, long timestamp) throws SQLException {
		ObjectDO objRef = em.getReference(ObjectDO.class, objId);
		AttributeDO attrRef = em.getReference(AttributeDO.class, attrId);
		ValueDO vdo = new ValueDO(attrRef, objRef);
		vdo.setTimestamp(timestamp);
		em.persist(vdo);
		return vdo.getValueId();
	}

}