package com.synapsense.dal.generic.dao;

import java.util.Collection;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.log4j.Logger;

import com.synapsense.dal.generic.entities.NetworkStatbase;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.IllegalInputParameterException;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.service.impl.dao.DataObjectDAO;
import com.synapsense.service.impl.messages.base.StatisticData;

@Stateless
public class NetworkStatisticBaseDAO implements DataObjectDAO<Integer, StatisticData> {

	private final static Logger log = Logger.getLogger(NetworkStatisticBaseDAO.class);

	@PersistenceContext(unitName = "HibernateDAL")
	public void setEM(EntityManager em) {
		this.em = em;
	}

	private EntityManager em;

	private Collection<ValueTO> fillStatdataCollectionToReturn(Collection<NetworkStatbase> netStatistics) {
		Collection<ValueTO> result = new LinkedList<ValueTO>();
		for (NetworkStatbase data : netStatistics) {
			StatisticData sd = new StatisticData();
			sd.setCrcFailPacketCount(data.getCrcFailPacketCount());
			sd.setDuplicatePackets(data.getDuplicatePackets());
			sd.setFreeSlotCount(data.getFreeSlotCount());
			sd.setLogicalId(data.getLogicalId());
			sd.setNwkTimestamp(data.getNwkTimestamp());
			sd.setOutOfOrderPackets(data.getOutOfOrderPackets());
			sd.setPacketsFromNodes(data.getPacketsFromNodes());
			sd.setPacketsRcvdCount(data.getPacketsRcvdCount());
			sd.setPacketsSent(data.getPacketsSent());
			sd.setTimeSyncsLostDriftDetection(data.getTimeSyncsLostDriftDetection());
			sd.setTimeSyncsLostTimeout(data.getTimeSyncsLostTimeout());
			sd.setTotalSlotCount(data.getTotalSlotCount());
			sd.setWrongSlotCount(data.getWrongSlotCount());
			sd.setChStatus(data.getChStatus());
			sd.setNoiseThresh(data.getNoiseThresh());

			result.add(new ValueTO("lastValue", sd, sd.getNwkTimestamp().getTime()));
		}
		return result;

	}

	@Override
	public void addBulk(Integer id, Collection<StatisticData> data) throws ObjectNotFoundException {
		if (id == null) {
			throw new IllegalInputParameterException("Supplied 'id' is null");
		}

		if (id < 1) {
			throw new IllegalInputParameterException("Supplied 'id' is negative or 0");
		}

		if (data == null) {
			throw new IllegalInputParameterException("Supplied 'data' is null");
		}

		for (StatisticData d : data) {
			addData(id, d);
		}
	}

	@Override
	public void addData(Integer id, StatisticData data) throws ObjectNotFoundException {

		if (id == null) {
			throw new IllegalInputParameterException("Supplied 'id' is null");
		}

		if (id < 1) {
			throw new IllegalInputParameterException("Supplied 'id' is negative or 0");
		}

		if (data == null) {
			throw new IllegalInputParameterException("Supplied 'data' is null");
		}

		NetworkStatbase netstat = new NetworkStatbase(data.getNwkTimestamp());
		{
			netstat.setLogicalId(id);
			netstat.setCrcFailPacketCount(data.getCrcFailPacketCount());
			netstat.setDuplicatePackets(data.getDuplicatePackets());
			netstat.setFreeSlotCount(data.getFreeSlotCount());
			netstat.setOutOfOrderPackets(data.getOutOfOrderPackets());
			netstat.setPacketsFromNodes(data.getPacketsFromNodes());
			netstat.setPacketsRcvdCount(data.getPacketsRcvdCount());
			netstat.setPacketsSent(data.getPacketsSent());
			netstat.setTimeSyncsLostDriftDetection(data.getTimeSyncsLostDriftDetection());
			netstat.setTimeSyncsLostTimeout(data.getTimeSyncsLostTimeout());
			netstat.setTotalSlotCount(data.getTotalSlotCount());
			netstat.setWrongSlotCount(data.getWrongSlotCount());

			// additional fields
			netstat.setChStatus(data.getChStatus());// array of ints, stored as
													// string of hex
			netstat.setNoiseThresh(data.getNoiseThresh());

			em.persist(netstat);

			if (log.isDebugEnabled()) {
				log.debug("Trying to add statistic from GATEWAY with ID[" + id + "]");
			}
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public Collection<ValueTO> getData(Integer id) {
		if (id == null) {
			throw new IllegalInputParameterException("Supplied 'id' is null");
		}

		if (id < 1) {
			throw new IllegalInputParameterException("Supplied 'id' is negative or 0");
		}

		List<NetworkStatbase> networkStatistic = em.createNamedQuery("NetworkStatbase.by.logicalId")
		        .setParameter("logicalId", id).getResultList();

		return fillStatdataCollectionToReturn(networkStatistic);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public Collection<ValueTO> getData(Integer id, int size, int start) {
		if (id == null) {
			throw new IllegalInputParameterException("Supplied 'id' is null");
		}

		if (id < 1) {
			throw new IllegalInputParameterException("Supplied 'id' is negative or 0");
		}

		if (size < 1) {
			throw new IllegalInputParameterException("Supplied 'size' is negative of 0");
		}

		if (start < 0) {
			throw new IllegalInputParameterException("Supplied 'start' is negative");
		}

		List<NetworkStatbase> networkStatistic = em.createNamedQuery("NetworkStatbase.by.logicalId")
		        .setParameter("logicalId", id).setFirstResult(start).setMaxResults(size).getResultList();

		return fillStatdataCollectionToReturn(networkStatistic);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public Collection<ValueTO> getData(Integer id, Date startDate, Date endDate) {
		if (id == null) {
			throw new IllegalInputParameterException("Supplied 'id' is null");
		}

		if (id < 1) {
			throw new IllegalInputParameterException("Supplied 'id' is negative or 0");
		}

		if (startDate == null) {
			throw new IllegalInputParameterException("Supplied 'startDate' is null");
		}

		if (endDate == null) {
			throw new IllegalInputParameterException("Supplied 'endDate' is null");
		}

		if (endDate.before(startDate)) {
			throw new IllegalInputParameterException("Supplied time period is wrong : 'endDate' < 'startDate'");
		}

		List<NetworkStatbase> networkStatistic = em
		        .createNamedQuery("NetworkStatbase.by.logicalId.and.between.start.and.end")
		        .setParameter("logicalId", id).setParameter("start", startDate).setParameter("end", endDate)
		        .getResultList();

		return fillStatdataCollectionToReturn(networkStatistic);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public Collection<ValueTO> getData(Integer id, Date startDate, int numberPoints) {

		if (id == null) {
			throw new IllegalInputParameterException("Supplied 'id' is null");
		}

		if (id < 1) {
			throw new IllegalInputParameterException("Supplied 'id' is negative or 0");
		}

		if (startDate == null) {
			throw new IllegalInputParameterException("Supplied 'startDate' is null");
		}

		List<NetworkStatbase> networkStatistic = em.createNamedQuery("NetworkStatbase.by.logicalId.and.start")
		        .setParameter("logicalId", id).setParameter("start", startDate).setMaxResults(numberPoints)
		        .getResultList();

		return fillStatdataCollectionToReturn(networkStatistic);
	}

	@Override
	public void removeData(Integer id) {

		if (id == null) {
			throw new IllegalInputParameterException("Supplied 'id' is null");
		}

		if (id < 1) {
			throw new IllegalInputParameterException("Supplied 'id' is negative or 0");
		}

		em.createNamedQuery("NetworkStatbase.delete.by.logicalId").setParameter("logicalId", id).executeUpdate();
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public int getDataSize(Integer id) {

		if (id == null) {
			throw new IllegalInputParameterException("Supplied 'id' is null");
		}

		if (id < 1) {
			throw new IllegalInputParameterException("Supplied 'id' is negative or 0");
		}

		return (Integer) em.createNamedQuery("NetworkStatbase.count.by.logicalId").setParameter("logicalId", id)
		        .getSingleResult();
	}

}
