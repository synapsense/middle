package com.synapsense.dal.generic.dao;

import java.sql.Date;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;

import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.DependsOn;
import javax.ejb.Remote;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.apache.log4j.Logger;

import com.synapsense.cdi.MBean;
import com.synapsense.dal.generic.entities.ActivityRecordDO;
import com.synapsense.dal.generic.entities.ObjectDO;
import com.synapsense.dal.objectcache.CacheSvc;
import com.synapsense.exception.IllegalInputParameterException;
import com.synapsense.service.impl.activitylog.ActivityLogService;
import com.synapsense.service.impl.activitylog.ActivityRecord;
import com.synapsense.service.impl.activitylog.ActivityRecordFilter;

/**
 * <code>ActivityLogService</code> implementation.<br>
 * By performance consideration it contains internal static cache of available
 * options(a.k.a filters)<br>
 * <br>
 * Note that this breaches the pure EJB 3.0 specification.<br>
 * and may lead to impossibility to use this bean in a cluster environment<br>
 * without some additional modifications.
 * 
 * @author ashabanov
 * 
 */
@Startup
@Singleton(name="ActivityLogDAO")
@ConcurrencyManagement(ConcurrencyManagementType.BEAN)
@Remote(ActivityLogService.class)
public class ActivityLogDAO implements ActivityLogService {

	private final static Logger logger = Logger.getLogger(ActivityLogDAO.class);

	/**
	 * As incoming activity description is variable size string we need to check
	 * its length before persisting it to storage.
	 * 
	 */
	private static final int MAX_DESCRIPTION_LENGTH = 300;

	private static final String moduleKey = "module";
	private static final String actionKey = "action";

	/**
	 * internal cache status holder
	 */
	private static volatile boolean isInternalCacheReady = false;

	/**
	 * internal cache to store all possible filters on modules , actions it is
	 * static , mean that it exists till JVM shutdown. so there is no
	 * possibility to flush this cache state except by restarting host JVM
	 */
	private static final Map<String, SortedSet<String>> moduleActionCache;
	static {
		if (logger.isDebugEnabled()) {
			logger.debug("Initializing internal cache of modules/actions");
		}

		moduleActionCache = new ConcurrentHashMap<String, SortedSet<String>>();
		// two new sorted sets to store filters
		moduleActionCache.put(moduleKey, Collections.synchronizedSortedSet(new TreeSet<String>()));
		moduleActionCache.put(actionKey, Collections.synchronizedSortedSet(new TreeSet<String>()));
	}

	private EntityManager em;

	@PersistenceContext(unitName = "HibernateDAL")
	public void setEM(final EntityManager em) {
		this.em = em;
	}

	@Override
	public void addRecord(ActivityRecord record) {

		if (record.getUserName() == null || record.getUserName().isEmpty()) {
			throw new IllegalInputParameterException("Supplied 'userName' is null or empty");
		}

		if (record.getModule() == null || record.getModule().isEmpty()) {
			throw new IllegalInputParameterException("Supplied 'moduleName' is null or empty");
		}

		if (record.getAction() == null || record.getAction().isEmpty()) {
			throw new IllegalInputParameterException("Supplied 'actionName' is null or empty");
		}

		if (record.getDescription() == null || record.getDescription().isEmpty()) {
			throw new IllegalInputParameterException("Supplied 'description' is null or empty");
		}

		String truncatedDescription = record.getDescription().length() > MAX_DESCRIPTION_LENGTH ? record
		        .getDescription().substring(0, MAX_DESCRIPTION_LENGTH) : record.getDescription();

		if (logger.isDebugEnabled() && record.getDescription().length() > MAX_DESCRIPTION_LENGTH) {
			logger.warn("Log activity description [" + record.getDescription() + "] has been truncated to ["
			        + truncatedDescription + "]");
		}

		ObjectDO odo = null;
		if (record.getObject() != null) {

			odo = em.find(ObjectDO.class, record.getObject().getID());
		}

		if (record.getTime() == null) {
			record.setTime(new Date(System.currentTimeMillis()));
		}
		em.persist(new ActivityRecordDO(null, record.getUserName(), record.getModule(), record.getAction(),
		        truncatedDescription, record.getTime(), odo));
		if (logger.isDebugEnabled() && record.getDescription().length() > MAX_DESCRIPTION_LENGTH) {
			logger.warn("Log activity record: " + record.toString() + " was added ");
		}

		SortedSet<String> moduleCache = moduleActionCache.get(moduleKey);
		if (!moduleCache.contains(record.getModule())) {
			moduleCache.add(record.getModule());
		}

		SortedSet<String> actionCache = moduleActionCache.get(actionKey);
		if (!actionCache.contains(record.getAction())) {
			actionCache.add(record.getAction());
		}

	}

	@Override
	public void log(final String userName, final String moduleName, final String actionName, final String description) {
		this.addRecord(new ActivityRecord(userName, moduleName, actionName, description));

	}

	@Override
	public void addRecords(Collection<ActivityRecord> recordsList) {
		Iterator<ActivityRecord> itr = recordsList.iterator();
		while (itr.hasNext()) {
			addRecord(itr.next());
		}
	}

	@Override
	public void deleteRecord(int ID) {
		Collection<Integer> IDs = new HashSet<Integer>();
		IDs.add(ID);
		deleteRecord(IDs);
	}

	@Override
	public void deleteRecord(Collection<Integer> IDs) {
		if (IDs == null) {
			throw new IllegalInputParameterException("Supplied 'IDs' is null or empty");
		}

		Iterator<Integer> itr = IDs.iterator();
		while (itr.hasNext()) {
			Integer id = itr.next();
			if (id == null) {
				continue;
			}
			Query genericSrcs = em.createQuery("DELETE from ActivityRecordDO as record where record.id = :Id");

			genericSrcs.setParameter("Id", id);
			genericSrcs.executeUpdate();

		}
	}

	@Override
	public void updateRecord(ActivityRecord record) {
		if (record == null) {
			throw new IllegalInputParameterException("Supplied 'record'is null");
		}
		Collection<ActivityRecord> records = new HashSet<ActivityRecord>();
		records.add(record);
		updateRecord(records);
	}

	@Override
	public void updateRecord(Collection<ActivityRecord> recordsList) {
		if (recordsList == null) {
			throw new IllegalInputParameterException("Supplied 'recordsList'is null");
		}

		Iterator<ActivityRecord> recrdItr = recordsList.iterator();
		while (recrdItr.hasNext()) {
			ActivityRecord record = recrdItr.next();

			if (record == null) {
				throw new IllegalInputParameterException("Supplied 'recordsList' contains null element");
			}

			if (record.getUserName() == null || record.getUserName().isEmpty()) {
				throw new IllegalInputParameterException("Supplied 'userName' is null or empty");
			}

			if (record.getModule() == null || record.getModule().isEmpty()) {
				throw new IllegalInputParameterException("Supplied 'moduleName' is null or empty");
			}

			if (record.getAction() == null || record.getAction().isEmpty()) {
				throw new IllegalInputParameterException("Supplied 'actionName' is null or empty");
			}

			if (record.getDescription() == null || record.getDescription().isEmpty()) {
				throw new IllegalInputParameterException("Supplied 'description' is null or empty");
			}

			ActivityRecordDO ado = null;
			if (record.getId() == null) {
				throw new IllegalInputParameterException("Supplied 'recordsList' contains element with null Id:"
				        + record.toString());
			}

			ado = em.find(ActivityRecordDO.class, record.getId());

			if (ado == null) {
				continue;
			}

			ado.setDTO(record);
			ObjectDO odo = null;
			if (record.getObject() != null) {
				odo = em.find(ObjectDO.class, record.getObject().getID());
			}

			ado.setObject(odo);
			em.merge(ado);
		}
	}

	@Override
	public Collection<ActivityRecord> getRecords(ActivityRecordFilter filter) {
		Collection<ActivityRecord> records = new HashSet<ActivityRecord>();
		TypedQuery<ActivityRecordDO> genericSrcs = em.createQuery(filter.getQuery(), ActivityRecordDO.class);

		Iterator<ActivityRecordDO> genericSrcIterator = genericSrcs.getResultList().iterator();
		while (genericSrcIterator.hasNext()) {
			ActivityRecordDO row = genericSrcIterator.next();
			records.add(row.getDTO());
		}
		return records;
	}

	@Override
	public ActivityRecord getRecord(int ID) {
		ActivityRecord record = null;
		ActivityRecordFilter filterById = new ActivityRecordFilter().filterById(ID);

		Collection<ActivityRecord> records = this.getRecords(filterById);
		Iterator<ActivityRecord> recItr = records.iterator();
		if (recItr.hasNext()) {
			record = recItr.next();
		}

		return record;
	}

	private void initInternalCache() {
		if (isInternalCacheReady)
			return;
		synchronized (ActivityLogDAO.class) {
			if (isInternalCacheReady)
				return;

			if (logger.isDebugEnabled()) {
				logger.debug("Seeding internal cache state ...");
			}

			Collection<String> persistentActions = em.createQuery(
			        "select distinct(action) from ActivityRecordDO order by 1", String.class).getResultList();
			SortedSet<String> cachedActions = moduleActionCache.get(actionKey);
			cachedActions.addAll(persistentActions);

			Collection<String> persistentModules = em.createQuery(
			        "select distinct(module) from ActivityRecordDO order by 1", String.class).getResultList();
			SortedSet<String> cachedModules = moduleActionCache.get(moduleKey);
			cachedModules.addAll(persistentModules);

			isInternalCacheReady = true;

			if (logger.isDebugEnabled()) {
				logger.debug("Internal cache has been successfully loaded");
			}

		}
	}

	@Override
	public List<String> getActivityActions() {
		if (!isInternalCacheReady) {
			initInternalCache();
		}
		return new LinkedList<String>(moduleActionCache.get(actionKey));
	}

	@Override
	public List<String> getActivityModules() {
		if (!isInternalCacheReady) {
			initInternalCache();
		}
		return new LinkedList<String>(moduleActionCache.get(moduleKey));
	}

}
