package com.synapsense.dal.generic.batch;

import com.synapsense.service.DbBatchUpdaterManagement;

public interface DbBatchUpdaterServiceMBean extends DbBatchUpdaterManagement {
}
