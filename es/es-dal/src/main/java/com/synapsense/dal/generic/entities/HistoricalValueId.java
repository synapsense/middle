package com.synapsense.dal.generic.entities;

import java.util.Date;

/**
 * ID of historical value
 * 
 * @author shabanov
 * 
 */
class HistoricalValueId implements java.io.Serializable {

	private static final long serialVersionUID = -7601755493525384009L;

	private int value;
	private Date timestamp;

	HistoricalValueId() {
	}

	public HistoricalValueId(int value, Date timestamp) {
		this.value = value;
		this.timestamp = timestamp;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((timestamp == null) ? 0 : timestamp.hashCode());
		result = prime * result + value;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final HistoricalValueId other = (HistoricalValueId) obj;
		if (timestamp == null) {
			if (other.timestamp != null)
				return false;
		} else if (!timestamp.equals(other.timestamp))
			return false;
		if (value != other.value)
			return false;
		return true;
	}

}
