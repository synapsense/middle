package com.synapsense.dal.generic;

import java.io.Serializable;

import javax.ejb.ApplicationException;

/**
 * DAO level exception. Thrown when client try to persist already existed
 * entity.
 * 
 * @author shabanov
 * 
 */
@ApplicationException(rollback = true)
public class DAOEntityExistsException extends RuntimeException {

	/**
	 * Entity which already exists
	 */
	private Serializable entity;

	public DAOEntityExistsException(Serializable entity, Throwable cause) {
		super("Entity : " + entity.toString() + " already exists!", cause);
		this.entity = entity;
	}

	public DAOEntityExistsException(Serializable entity) {
		this(entity, null);
	}

	private static final long serialVersionUID = 1866035311047187322L;

	public Serializable getEntity() {
		return entity;
	}

}
