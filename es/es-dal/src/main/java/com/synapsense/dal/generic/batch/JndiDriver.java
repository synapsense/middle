package com.synapsense.dal.generic.batch;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

import org.apache.log4j.Logger;

import scriptella.jdbc.JdbcConnection;
import scriptella.spi.ConnectionParameters;

public class JndiDriver extends scriptella.driver.jndi.Driver {

	protected JdbcConnection connect(ConnectionParameters parameters, Properties props) throws SQLException {
		return new CustomizedJdbcConnection(getConnection(parameters.getUrl(), props), parameters);
	}
}

class CustomizedJdbcConnection extends JdbcConnection {

	private final static Logger logger = Logger.getLogger(CustomizedJdbcConnection.class);

	public CustomizedJdbcConnection(Connection connection, ConnectionParameters parameters) {
		super(ConnectionWrapper.newInstance(connection), parameters);
		((ConnectionWrapperControl)getNativeConnection()).restoreTransactionIsolation();
	}

	@Override
	public void commit() {
		// don't want to commit, wait for JTA context
		logger.debug("Scriptella driver omit commiting on current JTA transaction");
	}

	@Override
	public void rollback() {
		// don't want to rollback, wait for JTA context
		logger.debug("Scriptella driver omit rollback on current JTA transaction");
	}
}

interface ConnectionWrapperControl extends Connection {
	void restoreTransactionIsolation();
}

class ConnectionWrapper implements InvocationHandler {
	
	private boolean fakeTransactionIsolation;
	private Connection con;
	
	public ConnectionWrapper(Connection con) {
		this.fakeTransactionIsolation = true;
		this.con = con;
	}

	@Override
	public Object invoke(Object proxy, Method m, Object[] args)
			throws Throwable {
		if("restoreTransactionIsolation".equals(m.getName())) {
			fakeTransactionIsolation = false;
			return null;
		}
		
		if("getTransactionIsolation".equals(m.getName()) && fakeTransactionIsolation)
			return Connection.TRANSACTION_NONE;
		
		return m.invoke(con, args);
	}
	
	public static Connection newInstance(Connection con) {
		return (Connection)Proxy.newProxyInstance(ConnectionWrapper.class.getClassLoader(),
				new Class<?>[] { ConnectionWrapperControl.class },
				new ConnectionWrapper(con));
	}
}
