package com.synapsense.dal.objectcache;

import com.synapsense.dto.*;
import com.synapsense.exception.EnvException;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.exception.PropertyNotFoundException;
import com.synapsense.exception.UnableToConvertPropertyException;
import com.synapsense.service.Environment;
import com.synapsense.util.CollectionUtils;
import com.synapsense.util.CollectionUtils.Functor;
import org.apache.log4j.Logger;

import java.util.*;

public class AlertCounter {
	private final static Logger logger = Logger.getLogger(AlertCounter.class);

	/**
	 * cache service to access metadata
	 */
	private CacheSvc datasource;

	private String numAlertsPropertyName = "numAlerts";

	/**
	 * local storage of alert number on objects. stores information about how
	 * many alerts are active on specified object
	 * 
	 */
	private Map<TO<?>, Integer> ownerCounterMap = CollectionUtils.newMap();

	/**
	 * 
	 * @param datasource
	 *            Cache reference
	 * @param numAlertsProperty
	 *            Name of property to update. Should have an integer type.
	 */
	public AlertCounter(CacheSvc datasource, String numAlertsProperty) {
		this.datasource = datasource;
		this.numAlertsPropertyName = numAlertsProperty;
	}

	public synchronized int inc(TO id) {
		return inc(id, 1);
	}

	synchronized int inc(TO id, final int num) {
		if (id == null || TOFactory.EMPTY_TO.equals(id)) {
			return -1;
		}

		if (logger.isDebugEnabled()) {
			logger.debug("Increasing " + id + ".numAlerts to " + num);
		}

		Integer numAlerts = getLocalNumAlerts(id);
		setLocalNumAlerts(id, numAlerts + num);

		operateNumAlerts(id, new Functor<Integer, TO>() {
			@Override
			public Integer apply(TO operand) {
				try {
					int numAlerts = datasource.getPropertyValue(operand, numAlertsPropertyName, Integer.class);
					datasource.setPropertyValue(operand, numAlertsPropertyName, numAlerts + num);
					return numAlerts + num;
				} catch (EnvException e) {
					logger.debug("Failed to increase numAlerts on " + operand, e);
					return -1;
				}
			}
		});

		return numAlerts + num;
	}

	public synchronized int dec(TO id) {
		return dec(id, 1);
	}

	synchronized int dec(TO id, final int num) {
		if (id == null || TOFactory.EMPTY_TO.equals(id)) {
			return -1;
		}

		if (logger.isDebugEnabled()) {
			logger.debug("Decreasing " + id + ".numAlerts to " + num);
		}

		int numAlerts = getLocalNumAlerts(id);
		if (numAlerts < num) {
			logger.warn("Cannot decrease 'numAlerts' on " + id + " to " + num + ". Current 'numAlerts='" + numAlerts);
			return 0;
		}

		setLocalNumAlerts(id, numAlerts - num);

		operateNumAlerts(id, new Functor<Integer, TO>() {
			@Override
			public Integer apply(TO operand) {
				try {
					int numAlerts = datasource.getPropertyValue(operand, numAlertsPropertyName, Integer.class);
					datasource.setPropertyValue(operand, numAlertsPropertyName, numAlerts - num);
					return numAlerts - num;
				} catch (EnvException e) {
					logger.debug("Failed to decrease numAlerts on " + operand, e);
					return -1;
				}

			}
		});

		if (logger.isDebugEnabled()) {
			logger.debug(id + ".numAlerts has been successfully decreased to " + (numAlerts - num));
		}

		return numAlerts - num;
	}

	/**
	 * Recalculates alert counter on object <code>id</code> and on its parents.
	 * 
	 * @param id
	 *            Object to rebase
	 * 
	 */
	public synchronized void rebase(TO id) {
		if (id == null || TOFactory.EMPTY_TO.equals(id)) {
			return;
		}

		if (logger.isDebugEnabled()) {
			logger.debug("Rebasing " + id + ".numAlerts...");
		}
		// specified object's(id) counter also should be rebased
		Collection<TO<?>> relatedObjects = CollectionUtils.newList(datasource.getRelatedObjects(id, null, false));
		relatedObjects.add(id);

		for (TO parent : relatedObjects) {
			// total alerts on parent and its children
			int numAlerts = getLocalNumAlerts(parent);
			for (TO child : datasource.getRelatedObjects(parent, null, true)) {
				// counting...
				numAlerts += getLocalNumAlerts(child);
			}
			// updates by total value
			try {
				datasource.setPropertyValue(parent, numAlertsPropertyName, numAlerts);
			} catch (ObjectNotFoundException e) {
				logger.warn("Cannot rebase alert num counter for object " + parent
				        + " because it does not exist or has just been removed", e);
			} catch (PropertyNotFoundException e) {
				logger.error("Cannot rebase alert num counter for object " + parent + "  because it has no property "
				        + numAlertsPropertyName, e);
			} catch (UnableToConvertPropertyException e) {
				logger.error("Cannot rebase alert num counter for object " + parent
				        + "  because property has another type" + numAlertsPropertyName, e);
			}

			if (logger.isDebugEnabled()) {
				logger.debug(parent + ".numAlerts current value is " + numAlerts);
			}
		}

		if (logger.isDebugEnabled()) {
			logger.debug(id + ".numAlerts has been successfully rebased");
		}
	}

	/**
	 * Rebase alert counter on all objects
	 * 
	 * @param env
	 *            Environment to use for broken alerts deleting
	 */
	public synchronized void rebaseAll(Environment env) {
		if (logger.isDebugEnabled()) {
			logger.debug("Rebasing all objects numAlerts...");
		}

		resetAll();

		Collection<TO<?>> alerts = datasource.getObjectsByType("ALERT");
		if (alerts.isEmpty()) {
			if (logger.isDebugEnabled()) {
				logger.debug("There are no active alerts. Skipping process alerts...");
			}
			return;
		}

		Collection<CollectionTO> hostObjects = datasource.getPropertyValue(alerts, new String[] { "hostTO" });
		for (CollectionTO entry : hostObjects) {
			List<ValueTO> propValuesList = entry.getPropValue("hostTO");
			if (propValuesList == null || propValuesList.isEmpty()) {
				continue;
			}

			ValueTO hostTOValue = propValuesList.iterator().next();
			TO<?> hostTO = (TO<?>) hostTOValue.getValue();
			if (hostTO == null || TOFactory.EMPTY_TO.equals(hostTO)) {
				continue;
			}

			if (datasource.exists(hostTO)) {
				inc(hostTO); // inc counter on well-formed TO only
			} else {
				try {
					// delete alert if its host has been removed
					env.deleteObject(entry.getObjId());
				} catch (ObjectNotFoundException e) {
					logger.debug("Failed to delete alert", e);
				}
			}
		}

		if (logger.isDebugEnabled()) {
			logger.debug("All objects numAlerts counter has been successfully rebased");
		}
	}

	private void resetAll() {
		// resets numAlerts counters
		ownerCounterMap.clear();
		for (String typeName : datasource.getObjectTypes()) {
			if (Types.decode(typeName).equals(Types.ALERT)) {
				continue;
			}

			for (TO to : datasource.getObjectsByType(typeName)) {
				try {
					if (datasource.getPropertyValue(to, numAlertsPropertyName, Integer.class) > 0) {
						datasource.setPropertyValue(to, numAlertsPropertyName, 0);
					}
				} catch (ObjectNotFoundException e) {
					logger.warn("Cannot reset alert num counter for object " + to
					        + " because it does not exist or has just been removed", e);
				} catch (PropertyNotFoundException e) {
					logger.error("Cannot reset alert num counter for object " + to + "  because it has no property "
					        + numAlertsPropertyName, e);
				} catch (UnableToConvertPropertyException e) {
					logger.error("Cannot reset alert num counter for object " + to + "  because of "
					        + numAlertsPropertyName + " data type mismatch", e);
				}
			}
		}
	}

	private void setLocalNumAlerts(TO id, int num) {
		ownerCounterMap.put(id, num);
	}

	private int getLocalNumAlerts(TO id) {
		Integer numAlerts = ownerCounterMap.get(id);
		return numAlerts == null ? 0 : numAlerts;
	}

	private void operateNumAlerts(TO<?> id, Functor<Integer, TO> operator) {
		operator.apply(id);

		for (TO<?> parent : datasource.getRelatedObjects(id, null, false)) {
			operator.apply(parent);
		}
	}
}
