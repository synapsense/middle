package com.synapsense.dal.generic.utils;

import java.util.Collection;
import java.util.List;

import javax.persistence.Query;

/**
 * Simple batch strategy. The fetch is done by one call of query.getResultList()
 * 
 * @author shabanov
 * 
 */
class SimpleBatchQueryStrategy implements BatchQueryStrategy {

	@Override
	public <ENTITY> List<ENTITY> getResultList(Query query, String paramName, Collection<?> value) {
		return query.setParameter(paramName, value).getResultList();
	}

}
