/**
 * 
 */
package com.synapsense.dal.objectcache;

/**
 * @author aborisov
 * 
 */
public class SimplePropertyValue {
	private String propName;
	private Value<?> value;

    public SimplePropertyValue(String propName, Class type) {
        this(propName, Value.createNullValue(type));
    }

	public SimplePropertyValue(String propName, Value<?> value) {
		this.propName = propName;
		this.value = value;
	}

	public String toString() {
		StringBuilder sb = new StringBuilder(50);
		sb.append("Property=[");
		sb.append(propName);
		sb.append("] Value=[");
		sb.append(value.toString());
		sb.append("]");
		return sb.toString();
	}

	public Value<?> getValue() {
		return value;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((propName == null) ? 0 : propName.hashCode());
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final SimplePropertyValue other = (SimplePropertyValue) obj;
		if (propName == null) {
			if (other.propName != null)
				return false;
		} else if (!propName.equals(other.propName))
			return false;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}

	public String getPropName() {
		return propName;
	}

}
