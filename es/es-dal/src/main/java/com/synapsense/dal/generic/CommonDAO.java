package com.synapsense.dal.generic;

import java.io.Serializable;
import java.util.Collection;
import java.util.Map;

/**
 * Common DAO interface. Reflects CRUD(create retrieve update delete) pattern.
 * 
 * The "retrieve" option is represented by basic methods like "by id" and "by
 * example" and some additional methods which have the reference to mapped JPA
 * compatible query as an input parameter. Such queries must be defined in the
 * ORM mapping and serve as more flexible way for retrieving objects from the
 * storage.
 * 
 * @author shabanov
 * 
 */
public interface CommonDAO {
	/**
	 * Makes transient entity persistent. Stores it into persistent storage.
	 * 
	 * @param entity
	 *            Object of class that has mapping
	 * @return just created entity
	 * @throws DAOEntityExistsException
	 *             if the specified entity already exists
	 */
	<ENTITY extends Serializable> ENTITY create(ENTITY entity) throws DAOEntityExistsException;

	/**
	 * Synchronizes detached entity with persistent one.
	 * 
	 * @param entity
	 *            Object of class that has mapping
	 * @throws DAOEntityExistsException
	 *             if one of the entity constraints has failed, this means that
	 *             entity with such state already exists.
	 */
	<ENTITY extends Serializable> boolean update(ENTITY entity);

	/**
	 * Makes persistent entity transient. Removes it from persistent storage.
	 * 
	 * @param entity
	 *            Object of class that has mapping
	 * @param id
	 *            PK value of deleting entity
	 */
	<ENTITY extends Serializable> void delete(Class<ENTITY> clazz, Serializable id);

	/**
	 * Finds entity by PK(primary key). Useful if the PK value of the searching
	 * entity is known(was fetched in previous call for example)
	 * 
	 * @param clazz
	 *            Class that has mapping
	 * @param entityId
	 *            PK value of searching entity
	 * @return Found entity or null if not such entity
	 */
	<ENTITY extends Serializable> ENTITY findById(Class<ENTITY> clazz, Object entityId);

	/**
	 * Clone of {@link #findByExample(Serializable, DAOInvocationContext)}. The
	 * <code>DAOInvocationContext.DEFAULT</code> will be used instead its second
	 * input parameter.
	 * 
	 * @param <ENTITY>
	 * @param entity
	 *            Object of class that has mapping
	 * @return Collection of found entities or empty collection
	 * @see #findByExample(Serializable, DAOInvocationContext) if you want to
	 *      specify invocation context manually
	 */
	<ENTITY extends Serializable> Collection<ENTITY> findByExample(ENTITY entity);

	/**
	 * Finds entities by exact example. Example is an instance of the mapped
	 * class initialized by example values. Uses invocation context to configure
	 * query settings.
	 * 
	 * @param <ENTITY>
	 * @param entity
	 * @param invocContext
	 *            Invocation context
	 * @return Collection of found entities or empty collection
	 * @see #findByExample(Serializable) if you want to use DEFAULT context
	 */
	<ENTITY extends Serializable> Collection<ENTITY> findByExample(ENTITY entity, DAOInvocationContext invocContext);

	/**
	 * Clone of {@link #findByExample(Serializable, DAOInvocationContext)}. The
	 * difference is only one entity must match to example , if not the
	 * DAOTooManyEntitiesFoundException exception will be thrown.
	 * 
	 * @param <ENTITY>
	 * @param entity
	 *            Example of searching entity
	 * @param invocContext
	 *            Invocation context
	 * @return Single entity if found or null
	 * @throws DAOTooManyEntitiesFoundException
	 *             if more than one entity matches example
	 */
	<ENTITY extends Serializable> ENTITY findUniqueByExample(ENTITY entity, DAOInvocationContext invocContext)
	        throws DAOTooManyEntitiesFoundException;

	/**
	 * Clone of {@link #findUniqueByExample(Serializable, DAOInvocationContext)}
	 * . The <code>DAOInvocationContext.DEFAULT</code> will be used instead its
	 * last input parameter.
	 * 
	 * @param <ENTITY>
	 * @param entity
	 *            Example of searching entity
	 * @return Single entity if found or null
	 * @throws DAOTooManyEntitiesFoundException
	 *             if more than one entity matches example
	 */
	<ENTITY extends Serializable> ENTITY findUniqueByExample(ENTITY entity) throws DAOTooManyEntitiesFoundException;

	/**
	 * Clone of the {@link #findAll(Class, DAOInvocationContext)}. The
	 * <code>DAOInvocationContext.DEFAULT</code> will be used instead its last
	 * input parameter.
	 * 
	 * @param <ENTITY>
	 * @param clazz
	 *            Class that has mapping
	 * @return Collection of found entities or empty collection
	 */
	<ENTITY extends Serializable> Collection<ENTITY> findAll(Class<ENTITY> clazz);

	/**
	 * Finds all entities of the specified class. Uses polymorphism concept.For
	 * example : if <code>Object</code> class will be passed as parameter the
	 * all its descendants(all existing mapped entities cause
	 * <code>Object</code> is a root of java object model) will be returned and
	 * so on (as it is defined in semantic of HQL(JPA_QL) polymorphic queries)
	 * 
	 * @param <ENTITY>
	 * @param clazz
	 *            Class that has mapping
	 * @param invocContext
	 *            Invocation context
	 * @return Collection of found entities or empty collection
	 */
	<ENTITY extends Serializable> Collection<ENTITY> findAll(Class<ENTITY> clazz, DAOInvocationContext invocContext);

	/**
	 * Clone of
	 * {@link #findByNamedQuery(String, Map, Class, DAOInvocationContext)}.The
	 * <code>DAOInvocationContext.DEFAULT</code> will be used instead its last
	 * input parameter.
	 * 
	 * @param <ENTITY>
	 * @param namedQuery
	 *            Name of already existed query that defined in the mappings
	 * @param params
	 *            Query parameters
	 * @param clazz
	 *            Class that has mapping
	 * @return Collection of found entities
	 */
	<ENTITY extends Serializable> Collection<ENTITY> findByNamedQuery(String namedQuery, Map<String, Object> params,
	        Class<ENTITY> clazz);

	/**
	 * Finds certain entities by executing already existed named query with the
	 * specified name and named parameters. Uses <code>invocContext</code> to
	 * configure query settings.
	 * 
	 * @param <ENTITY>
	 * @param namedQuery
	 *            Name of already existed query that defined in the mappings
	 * @param params
	 *            Query parameters
	 * @param invocContext
	 *            Invocation context
	 * @return Collection of found entities
	 */
	<ENTITY extends Serializable> Collection<ENTITY> findByNamedQuery(String namedQuery, Map<String, Object> params,
	        Class<ENTITY> clazz, DAOInvocationContext invocContext);

	/**
	 * Clone of
	 * {@link #findRowSetByNamedQuery(String, Map, DAOInvocationContext)}.The
	 * <code>DAOInvocationContext.DEFAULT</code> will be used instead its last
	 * input parameter.
	 * 
	 * @param namedQuery
	 *            Name of already existed query that defined in the mappings
	 * @param params
	 *            Query parameters
	 * @return Collection of found rows (every element of collection is a array
	 *         of select clause elements by example : select item.name ,
	 *         item.age ... will convert to Object[]{"some name",22})
	 * @see #findRowSetByNamedQuery(String, Map, DAOInvocationContext) that
	 *      allows to specify invocation context
	 */
	Collection<Object[]> findRowSetByNamedQuery(String namedQuery, Map<String, Object> params);

	/**
	 * Returns query result as collection of object arrays. Every object in such
	 * array is a field value from select clause of query.It is needed for
	 * returning not just single entity but the set of any data which has
	 * mapped. Uses <code>invocContext</code> to configure query settings.
	 * Differs from
	 * {@link #findByNamedQuery(String, Map, Class, DAOInvocationContext)} only
	 * by resulting value.
	 * 
	 * @param namedQuery
	 *            Name of already existed query that defined in the mappings
	 * @param params
	 *            Query parameters
	 * @param invocContext
	 *            Invocation context
	 * @return Collection of found rows (every element of collection is a array
	 *         of select clause elements by example : select item.name ,
	 *         item.age ... will convert to Object[]{"some name",22})
	 * @see #findRowSetByNamedQuery(String, Map) if you dont want to specify
	 *      invocation context
	 */
	Collection<Object[]> findRowSetByNamedQuery(String namedQuery, Map<String, Object> params,
	        DAOInvocationContext invocContext);

	<ENTITY extends Serializable> ENTITY create(ENTITY entity, boolean flush) throws DAOEntityExistsException;

	<ENTITY extends Serializable> boolean update(ENTITY entity, boolean flush);

	<ENTITY extends Serializable> void createBatch(Collection<ENTITY> entity, boolean flush)
	        throws DAOEntityExistsException;

	<ENTITY extends Serializable> void updateBatch(Collection<ENTITY> entities, boolean flush);

	<ENTITY extends Serializable> Collection<ENTITY> findByQuery(String query);

	void flushSession();
}