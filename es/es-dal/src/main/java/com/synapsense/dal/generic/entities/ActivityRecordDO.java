package com.synapsense.dal.generic.entities;

import java.io.Serializable;
import java.util.Date;

import com.synapsense.dal.generic.DTOCompatible;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.service.impl.activitylog.ActivityRecord;

/**
 * Class mapped to tbl_activity_log records.
 * 
 * @author Oleg Stepanov
 * 
 */
public class ActivityRecordDO implements DTOCompatible<ActivityRecord>, Serializable {

	private Integer id;
	private String action;
	private String module;
	private String description;
	private String userName;
	private Date time;
	private ObjectDO object;

	private static final long serialVersionUID = -4477206350048282044L;

	public ActivityRecordDO() {
		action = "";
		module = "";
		description = "";
		userName = "";
		time = new Date();
		object = null;
	}

	public ActivityRecordDO(Integer id, String userName, String module, String action, String description, Date time,
	        ObjectDO objId) {
		this.id = id;
		this.action = action;
		this.module = module;
		this.description = description;
		this.userName = userName;
		this.time = time;
		this.object = objId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getModule() {
		return module;
	}

	public void setModule(String module) {
		this.module = module;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public ObjectDO getObject() {
		return object;
	}

	public void setObject(ObjectDO object) {
		this.object = object;
	}

	@Override
	public ActivityRecord getDTO() {
		TO<?> objId = TOFactory.EMPTY_TO;
		if (this.object != null)
			objId = this.object.getIdentity();

		return new ActivityRecord(this.id, this.userName, this.module, this.action, this.description, this.time, objId);
	}

	@Override
	public void setDTO(ActivityRecord dto) {
		this.id = dto.getId();
		this.userName = dto.getUserName();
		this.module = dto.getModule();
		this.action = dto.getAction();
		this.description = dto.getDescription();
		this.time = dto.getTime();
		// this.object = dto.getObject().;

	}

}
