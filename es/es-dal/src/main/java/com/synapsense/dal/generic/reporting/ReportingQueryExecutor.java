package com.synapsense.dal.generic.reporting;

import java.util.Collection;
import java.util.List;
import java.util.Map;

public interface ReportingQueryExecutor {

	List<Object[]> executeHQLQuery(final String queryString,
			final Map<Integer, List<Integer>> parameterMap);

	List<Object[]> executeSQLQuery(String queryString,
			Map<String, Collection<Integer>> collectionParameterMap);

}