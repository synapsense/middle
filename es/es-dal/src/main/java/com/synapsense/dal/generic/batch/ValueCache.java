package com.synapsense.dal.generic.batch;

import com.synapsense.util.Pair;

class ValueCache extends BaseCache<Pair<Integer, String>, ValueTuple> {

	@Override
	public boolean isHistorical(Pair<Integer, String> key) {
		ValueTuple tuple = getContainer().get(key);
		if (tuple == null)
			return false;
		return tuple.isHistorical();
	}

	
}