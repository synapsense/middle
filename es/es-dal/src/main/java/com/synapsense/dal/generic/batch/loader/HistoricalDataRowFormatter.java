package com.synapsense.dal.generic.batch.loader;

import com.synapsense.dal.generic.entities.HistoricalValueDO;

import java.io.IOException;
import java.io.PrintStream;
import java.text.*;
import java.util.Calendar;

/**
 * This class's purpose is to convert historical data point to format understood
 * by database loader
 * 
 */
public class HistoricalDataRowFormatter {
	// int(10) + timestamp(20) +
	// (double(10)|integer(10)|long(20)|string(50))+int(10) + nulls(10)
	private static byte avgRecordLength = 100;

	// char sequence to separate fields
	private String fieldSeparator = "\0";
	// how to write is nulls
	private String nullValue = "\\N";

	private NumberFormat decimalFormat;
	private SimpleDateFormat timestampFormat;

	public HistoricalDataRowFormatter(String decimalFormat, String dateFormat, char decimalSeparator, String nullValue) {
		DecimalFormatSymbols sym = DecimalFormatSymbols.getInstance();
		sym.setDecimalSeparator(decimalSeparator);
		this.decimalFormat = new DecimalFormat(decimalFormat, sym);
		this.timestampFormat = new SimpleDateFormat(dateFormat);
		this.nullValue = nullValue;
	}

	public String format(HistoricalValueDO historicalPoint) {
		StringBuilder builder = new StringBuilder(avgRecordLength);
		int valueId = historicalPoint.getValue();

		String timestamp = timestampFormat.format(historicalPoint.getTimestamp());
		String vi = formatNull(historicalPoint.getValuei());
		String vd = formatDecimal(historicalPoint.getValued());
		String vs = formatNull(historicalPoint.getValuev());
		String vl = formatNull(historicalPoint.getValuel());
		String groupId = formatNull(historicalPoint.getGroupId());

		enclose(builder, valueId);
		builder.append(fieldSeparator);
		enclose(builder, timestamp);
		builder.append(fieldSeparator);
		enclose(builder, vi);
		builder.append(fieldSeparator);
		enclose(builder, vd);
		builder.append(fieldSeparator);
		enclose(builder, vl);
		builder.append(fieldSeparator);
		enclose(builder, removeSpecialCharactersFrom(vs));
		builder.append(fieldSeparator);
		enclose(builder, groupId);

		return builder.toString();
	}

	public String getFieldSeparator() {
		return fieldSeparator;
	}

	public DateFormat getDateFormat() {
		return timestampFormat;
	}

	private void enclose(StringBuilder builder, Object value) {
		builder.append(value);
	}

	private String formatDecimal(Object obj) {
		if (obj == null)
			return nullValue;
		return decimalFormat.format(obj);
	}

	private String formatNull(Object obj) {
		if (obj == null)
			return nullValue;
		return obj.toString();
	}

	private String removeSpecialCharactersFrom(String input) {
		if (nullValue.equals(input))
			return input;

		String out = input.replace(fieldSeparator, "");
		// string are rather historical so regex is OK here
		return out.replaceAll("(\r\n|\r|\n)", "\\\\n");
	};

	public static void main(String[] args) throws IOException {

		Calendar cal = Calendar.getInstance();

		PrintStream bufferFileStream = new PrintStream("buffer.csv");
		HistoricalDataRowFormatter ldf = new HistoricalDataRowFormatter("#########.###", "yyyy-MM-dd H:m:s", '.', "\\N");
		for (int i = 1; i < 10; i++) {
			HistoricalValueDO hvd = new HistoricalValueDO(i, cal.getTime(), i);
			hvd.setUnifiedValue("$localize(alert_msg_io_failure_2,\"DX\",\"{\0eval_error\r,{\"cr\r\nah\",xpr,indings},\nunknown_property}\")$");
			// hvd.setUnifiedValue(null);
			String row = ldf.format(hvd);
			System.out.println(row);
			bufferFileStream.println(row);
		}
	}

}
