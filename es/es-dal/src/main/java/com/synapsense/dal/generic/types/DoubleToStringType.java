package com.synapsense.dal.generic.types;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.type.DoubleType;
import org.hibernate.type.Type;
import org.hibernate.usertype.UserType;

public class DoubleToStringType implements UserType {

	private static final int[] TYPES = { Types.DOUBLE };
	private final String INVALID_FORMAT_MSG = "Invalid format: string should contain float numeric value";

	Double d;

	public int[] sqlTypes() {
		return TYPES;
	}

	/**
	 * The class returned by <tt>nullSafeGet()</tt>.
	 * 
	 * @return Class
	 */
	public Class<String> returnedClass() {
		return String.class;
	}

	/**
	 * Compare two instances of the class mapped by this type for persistence
	 * "equality". Equality of the persistent state.
	 * 
	 * @param x
	 * @param y
	 * @return boolean
	 * @throws HibernateException
	 */
	public boolean equals(Object x, Object y) {
		if (x == y)
			return true;
		if (x == null || y == null)
			return false;
		return ((String) x).equals(((String) y));
	}

	/**
	 * Get a hashcode for the instance, consistent with persistence "equality"
	 */
	public int hashCode(Object x) {
		String a = (String) x;
		return a.hashCode();
	}

	/**
	 * Return a deep copy of the persistent state, stopping at entities and at
	 * collections.
	 * 
	 * @param value
	 *            generally a collection element or entity field
	 * @return Object a copy
	 * @throws HibernateException
	 */
	public Object deepCopy(Object x) {
		if (x == null)
			return null;
		String result = new String();
		String input = (String) x;
		result = input;
		return result;
	}

	/**
	 * Check if objects of this type mutable.
	 * 
	 * @return boolean
	 */
	public boolean isMutable() {
		return true;
	}

	/**
	 * Retrieve an instance of the mapped class from a JDBC resultset.
	 * Implementors should handle possibility of null values.
	 * 
	 * @param rs
	 *            a JDBC result set
	 * @param names
	 *            the column names
	 * @param owner
	 *            the containing entity
	 * @return Object
	 * @throws HibernateException
	 * @throws SQLException
	 */
	@Override
	public Object nullSafeGet(ResultSet rs, String[] names, SessionImplementor session, Object owner) throws HibernateException, SQLException {

		Double v = DoubleType.INSTANCE.nullSafeGet(rs, names[0], session);

		if (v == null)
			return null;
		else {
			return Double.toString(v);
		}
	}

	/**
	 * Write an instance of the mapped class to a prepared statement.
	 * Implementors should handle possibility of null values. A multi-column
	 * type should be written to parameters starting from <tt>index</tt>.
	 * 
	 * @param st
	 *            a JDBC prepared statement
	 * @param value
	 *            the object to write
	 * @param index
	 *            statement parameter index
	 * @throws HibernateException
	 * @throws SQLException
	 */
	@Override
	public void nullSafeSet(PreparedStatement st, Object value, int index, SessionImplementor session) throws HibernateException, SQLException {
		String str = (value == null) ? new String() : (String) value;
		parseStr(str);
		DoubleType.INSTANCE.nullSafeSet(st, d, index, session);
	}

	/**
	 * Get the "property names" that may be used in a query.
	 * 
	 * @return an array of "property names"
	 */
	public String[] getPropertyNames() {
		return new String[] { "value" };
	}

	/**
	 * Get the corresponding "property types".
	 * 
	 * @return an array of Hibernate types
	 */
	public Type[] getPropertyTypes() {
		return new Type[] { DoubleType.INSTANCE };
	}

	/**
	 * Get the value of a property.
	 * 
	 * @param component
	 *            an instance of class mapped by this "type"
	 * @param property
	 * @return the property value
	 * @throws HibernateException
	 */
	public Object getPropertyValue(Object component, int property) throws HibernateException {
		String str = (component == null) ? new String() : (String) component;
		parseStr(str);
		if (property == 0) {
			return d;
		} else {
			throw new HibernateException("Invalid property index");
		}
	}

	/**
	 * Set the value of a property.
	 * 
	 * @param component
	 *            an instance of class mapped by this "type"
	 * @param property
	 * @param value
	 *            the value to set
	 * @throws HibernateException
	 */
	public void setPropertyValue(Object component, int property, Object value) throws HibernateException {

		if (property == 0) {
			d = (Double) value;
		} else {
			throw new HibernateException("Invalid property index");
		}
	}

	/**
	 * Reconstruct an object from the cacheable representation. At the very
	 * least this method should perform a deep copy. (optional operation)
	 * 
	 * @param cached
	 *            the object to be cached
	 * @param owner
	 *            the owner of the cached object
	 * @return a reconstructed object from the cachable representation
	 * @throws HibernateException
	 */
	public Object assemble(Serializable cached, Object owner) {

		return deepCopy(cached);
	}

	/**
	 * Transform the object into its cacheable representation. At the very least
	 * this method should perform a deep copy. That may not be enough for some
	 * implementations, however; for example, associations must be cached as
	 * identifier values. (optional operation)
	 * 
	 * @param value
	 *            the object to be cached
	 * @return a cachable representation of the object
	 * @throws HibernateException
	 */
	public Serializable disassemble(Object value) {
		return (Serializable) deepCopy(value);
	}

	/**
	 * During merge, replace the existing (target) value in the entity we are
	 * merging to with a new (original) value from the detached entity we are
	 * merging. For immutable objects, or null values, it is safe to simply
	 * return the first parameter. For mutable objects, it is safe to return a
	 * copy of the first parameter. However, since composite user types often
	 * define component values, it might make sense to recursively replace
	 * component values in the target object.
	 * 
	 * @param original
	 * @param target
	 * @param session
	 * @param owner
	 * @return
	 * @throws HibernateException
	 */
	public Object replace(Object original, Object target, Object owner) throws HibernateException {
		return original;
	}

	/**
	 * Converts String to Double
	 * 
	 * @param str
	 *            String to parse
	 * @throws HibernateException
	 */
	private void parseStr(String str) throws HibernateException {
		if (str.isEmpty()) {
			d = null;
			return;
		}
		try {
			d = Double.valueOf(str);
		} catch (NumberFormatException e) {
			throw new HibernateException(INVALID_FORMAT_MSG);
		}
	}

}
