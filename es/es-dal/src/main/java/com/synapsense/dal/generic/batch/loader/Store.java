package com.synapsense.dal.generic.batch.loader;

public interface Store {
	void persist(Resource resource);
}
