package com.synapsense.dal.generic.entities;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

import org.apache.log4j.Logger;

import com.synapsense.dto.BinaryData;
import com.synapsense.dto.TO;
import com.synapsense.exception.UnableToConvertPropertyException;
import com.synapsense.service.impl.dao.to.PropertyTO;
import com.synapsense.service.impl.exception.DALSystemException;

/**
 * Class represents value of object's properties.
 * <p>
 * It holds all references to typed values (Integer , String , Double ...) with
 * cascade option.
 * 
 * @author shabanov
 * 
 */
public class ValueDO implements java.io.Serializable {

	final private static Logger logger = Logger.getLogger(ValueDO.class);

	private static final long serialVersionUID = -9223152924455469815L;
	/**
	 * PK
	 */
	private Integer valueId;
	/**
	 * version column for supporting optimistic locking mechanizm
	 */
	private int version;
	/**
	 * Value's last timestamp
	 */
	private Long timestamp;
	/**
	 * Reference to owner property
	 */
	private AttributeDO attribute;
	/**
	 * Reference to object
	 */
	private ObjectDO object;
	/**
	 * reference to historical values
	 */
	// private List<HistoricalValueDO> historicalValues = new
	// LinkedList<HistoricalValueDO>();
	/**
	 * reference to values which consist Links to Properties
	 */
	private Set<AbstractValue> valuePropertyLinks = new HashSet<AbstractValue>(0);
	/**
	 * reference to values which consist Links to Objects
	 */
	private Set<AbstractValue> valueObjLinks = new HashSet<AbstractValue>(0);

	/**
	 * reference to values which consist <code>String</code> values
	 */
	private Set<AbstractValue> valuevs = new HashSet<AbstractValue>(0);
	/**
	 * reference to values which consist <code>Long</code> values
	 */
	private Set<AbstractValue> valuels = new HashSet<AbstractValue>(0);
	/**
	 * reference to values which consist <code>Double</code> values
	 */
	private Set<AbstractValue> valueds = new HashSet<AbstractValue>(0);
	/**
	 * reference to values which consist <code>Date</code> values
	 */
	private Set<AbstractValue> valuedts = new HashSet<AbstractValue>(0);
	/**
	 * reference to values which consist <code>Integer</code> values
	 */
	private Set<AbstractValue> valueis = new HashSet<AbstractValue>(0);
	/**
	 * reference to values which consist BinaryData values
	 */
	private Set<AbstractValue> valuebs = new HashSet<AbstractValue>(0);

	/**
	 * returns typed set of values solution is correct only if all primitive
	 * types pre-defined , it's true now
	 * 
	 * @param value
	 * 
	 * @return Ref to Set of values of concrete type
	 * @throws DALSystemException
	 *             When <code>attribute</code>'s type is not supported
	 */
	private Set<AbstractValue> getValuesSet() {
		// check existing of the class
		Class<?> clazz = null;
		String type = this.getAttribute().getAttributeType().getType();
		try {
			clazz = Class.forName(type);
			if (clazz == String.class) {
				return valuevs;
			}
			if (clazz == Double.class) {
				return valueds;
			}
			if (clazz == Integer.class) {
				return valueis;
			}
			if (clazz == Long.class) {
				return valuels;
			}
			if (clazz == Date.class) {
				return valuedts;
			}
			if (clazz == TO.class) {
				return valueObjLinks;
			}
			if (clazz == PropertyTO.class) {
				return valuePropertyLinks;
			}
			if (clazz == BinaryData.class) {
				return valuebs;
			}
			throw new ClassNotFoundException(type);
		} catch (ClassNotFoundException e) {
			// if type is not supported throw
			throw new DALSystemException("The type [" + clazz.getName()
			        + "] is not supported by system and has to be defined before using");
		}
	}

	/**
	 * creates hibernate pojo wrapper for a native value
	 * 
	 * @param value
	 * @return AbstractValue
	 * @throws UnableToConvertPropertyException
	 */
	private AbstractValue createNewTypedValue(Object value) throws UnableToConvertPropertyException {
		AbstractValue result = null;

		Class<?> clazz = PropertyTypes.checkAndGetPropertyTypeClass(this.getAttribute().getAttributeType().getType());
		try {
			// creates new concrete value (scalar , link ...)
			result = (AbstractValue) clazz.getConstructor(ValueDO.class, Object.class).newInstance(this, value);
		} catch (InstantiationException e) {
			throw new DALSystemException("Object of [" + clazz.getSimpleName() + "] class cannot be instantiated.", e);
		} catch (IllegalAccessException e) {
			throw new DALSystemException("Constructor in [" + clazz.getSimpleName() + "] class is non-pulic.", e);
		} catch (InvocationTargetException e) {
			if (e.getCause() instanceof ClassCastException) {
				throw new UnableToConvertPropertyException(this.getAttribute().getAttributeName(), e);
			} else {
				throw new DALSystemException("Problems occured during instantiating an object of ["
				        + clazz.getSimpleName() + "] class", e);
			}
		} catch (NoSuchMethodException e) {
			throw new DALSystemException("No such constructor in " + clazz.getSimpleName() + " class.", e);
		}
		return result;
	}

	ValueDO() {
	}

	public ValueDO(AttributeDO attribute, ObjectDO object) {
		this(attribute, object, 0L);
	}

	public ValueDO(AttributeDO attribute, ObjectDO object, long timestamp) {
		this.attribute = attribute;
		this.object = object;
		this.timestamp = timestamp == 0L ? System.currentTimeMillis() : timestamp;
	}

	public Integer getValueId() {
		return this.valueId;
	}

	public int getVersion() {
		return this.version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public Long getTimestamp() {
		return this.timestamp;
	}

	// bug#1992 - hides the timestamp setter
	public void setTimestamp(Long timestamp) {
		this.timestamp = timestamp;
	}

	public AttributeDO getAttribute() {
		return this.attribute;
	}

	public void setAttribute(AttributeDO attribute) {
		this.attribute = attribute;
	}

	public ObjectDO getObject() {
		return this.object;
	}

	public void setObject(ObjectDO object) {
		this.object = object;
	}

	/**
	 * Puts value to right structure
	 * 
	 * @param value
	 * @param timestamp
	 * @throws UnableToConvertPropertyException
	 */
	public void setValue(Object value, long timestamp) throws UnableToConvertPropertyException {
		// get set reference
		Set<AbstractValue> valueSet = getValuesSet();
		// ref of hibernate related class

		if (logger.isTraceEnabled())
			logger.trace("Setting of property(TO/Property/Value/ValueType): " + getObject().getIdentity() + "/"
			        + getAttribute().getAttributeName() + "/" + ((value == null) ? "null" : value) + "/"
			        + ((value == null) ? "null" : value.getClass()));

		// updates timestamp to the specified time
		this.setTimestamp(timestamp);

		AbstractValue typedValue = null;
		// this means that value is new
		int setSize = valueSet.size();
		// TODO : don't remember to refactor GenericDAO setPropertyValue
		if (this.getAttribute().getCollection()) {
			if (value != null){
				if (value instanceof Collection) {
					Set<AbstractValue> tempSet = new HashSet<AbstractValue>();
					// filling temp set by given values
					for (Object valueInCollection : (Collection) value) {
						typedValue = createNewTypedValue(valueInCollection);
						tempSet.add(typedValue);
					}
					// merge of db and memory
					valueSet.retainAll(tempSet);
					valueSet.addAll(tempSet);
				} else {
					// add single value to collection
					typedValue = createNewTypedValue(value);
					valueSet.add(typedValue);
				}
			}
		} else if (setSize == 0) {
			typedValue = createNewTypedValue(value);
			valueSet.add(typedValue);
		} else if (setSize == 1) {
			// must be only one in a set
			AbstractValue tempVal = valueSet.iterator().next();
			try {
				tempVal.setValue(value);
			} catch (ClassCastException e) {
				throw new UnableToConvertPropertyException(this.getAttribute().getAttributeName(), e);
			}
		} else {
			throw new DALSystemException("Non-colleciton property [" + this.getAttribute().toString()
			        + "] has more than one value");
		}
	}

	public void removeValue(Object value, long timestamp) throws UnableToConvertPropertyException {
		// get set reference
		Set<AbstractValue> valueSet = getValuesSet();
		// ref of hibernate related class
		PropertyTypes.checkAndGetPropertyTypeClass(this.getAttribute().getAttributeType().getType());

		AbstractValue typedValue = createNewTypedValue(value);
		valueSet.remove(typedValue);
		this.setTimestamp(timestamp);
	}

	public Collection<Object> getValue() {
		Collection<Object> result = new LinkedList<Object>();
		for (AbstractValue<?> av : getValuesSet()) {
			result.add(av.getValue());
		}
		return result;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((attribute == null) ? 0 : attribute.hashCode());
		result = prime * result + ((object == null) ? 0 : object.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final ValueDO other = (ValueDO) obj;
		if (attribute == null) {
			if (other.attribute != null)
				return false;
		} else if (!attribute.equals(other.attribute))
			return false;
		if (object == null) {
			if (other.object != null)
				return false;
		} else if (!object.equals(other.object))
			return false;
		return true;
	}

	/*
	 * public List<HistoricalValueDO> getHistoricalValues() { return
	 * historicalValues; }
	 * 
	 * public void setHistoricalValues(List<HistoricalValueDO> historicalValues)
	 * { this.historicalValues = historicalValues; }
	 */
}
