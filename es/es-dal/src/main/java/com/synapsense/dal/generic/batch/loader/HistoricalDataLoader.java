package com.synapsense.dal.generic.batch.loader;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.log4j.Logger;

import com.synapsense.dal.generic.entities.HistoricalValueDO;

public final class HistoricalDataLoader {

	private static final Logger logger = Logger.getLogger(HistoricalDataLoader.class);

	private String bufferFile;

	private int rowsInBuffer = 0;
	private int bufferSize;
	/**
	 * store last persist time stamp
	 */
	private Date lastTimePersisted;

	private Writer writer;

	/**
	 * flag to keep stream opened for not just single thread
	 */
	private volatile boolean isSessionOpened = false;

	private HistoricalDataRowFormatter formatter;

	private Timer timer;

	private Store store;

	/**
	 * 
	 * @param bufferFile
	 *            File to accumulate data for one-shot load
	 * @param formatter
	 *            historical record formatter
	 * @param bufferSize
	 *            size to control persist frequency
	 * @param store
	 *            underlying store , delegate to it persisting data
	 */
	public HistoricalDataLoader(String bufferFile, HistoricalDataRowFormatter formatter, int bufferSize, Store store) {
		this.bufferFile = bufferFile;
		this.formatter = formatter;
		this.bufferSize = bufferSize;
		this.store = store;
	}

	/**
	 * Feature to spool accumulated changes at least one time per interval
	 * 
	 * @param interval
	 *            in ms to persist file
	 */
	public void autoPersistByInterval(final long interval) {
		this.timer = new Timer(true);

		timer.schedule(new TimerTask() {
			@Override
			public void run() {
				if (lastTimePersisted.before(new Date(System.currentTimeMillis() - interval))) {
					persist();
				}
			}
		}, new Date(), interval);
	}

	/**
	 * call it to load chunk of historical values. it is not guaranteed that
	 * given values will be persisted at call time.
	 * 
	 * @param values
	 */
	public void load(final List<HistoricalValueDO> values) {
		if (values.isEmpty())
			return;

		synchronized (this) {
			logger.debug("sorting historical point in PK order...");
			rowsInBuffer += write(values);
			if (rowsInBuffer > bufferSize) {
				persist();
			} else { 
				flushStream();
			}
		}

	}

	private int write(List<HistoricalValueDO> historicalPoints) {
		int counter = 0;
		Writer writer = getStream();
		try {
			for (HistoricalValueDO historicalPoint : historicalPoints) {
				writer.write(formatter.format(historicalPoint) + "\n");
				counter++;
			}
		} catch (IOException e) {
			recoverStream("Cannot write to historical data buffer file", e);
		}

		return counter;
	}

	private final void flushStream() {
		try {
			getStream().flush();
		} catch (IOException e) {
			recoverStream("Error flushing stream", e);
		}
	}

	private final void closeStream() {
		synchronized (this) {
			try {
				writer.close();
			} catch (IOException e) {
				recoverStream("error closing historical data buffer file", e);
			} finally {
				isSessionOpened = false;
			}
		}
	}

	// opens stream for concurrent writes
	private final Writer getStream() {
		if (isSessionOpened) {
			return writer;
		}

		try {
			synchronized (this) {
				if (isSessionOpened) {
					return writer;
				}
				writer = new VelocityWriter(new FileWriter(new File(bufferFile), rowsInBuffer != 0));
				isSessionOpened = true;
				return writer;
			}
		} catch (IOException e) {
			return recoverStream("cannot open stream to write historical data buffer", e);
		}

	}

	private void persist() {
		try {
			synchronized (this) {
				logger.debug("Persisting of " + rowsInBuffer + " historical points into database...");
				flushStream();
				store.persist(new FileResource(bufferFile));
				rowsInBuffer = 0;
				lastTimePersisted = new Date();
			}
		} finally {
			closeStream();
		}
	}

	private Writer recoverStream(String string, IOException e) {
		synchronized (this) {
			logger.warn("Loader is in recovery mode due to ", e);
			logger.debug("Trying to re-init stream...");
			isSessionOpened = false;
			return getStream();
		}
	}
}
