package com.synapsense.dal.generic.utils;

import java.util.Collection;
import java.util.List;

import javax.persistence.Query;

/**
 * Batch strategy
 * 
 * @author shabanov
 * 
 */
interface BatchQueryStrategy {
	<ENTITY> List<ENTITY> getResultList(Query query, String paramName, Collection<?> value);
}
