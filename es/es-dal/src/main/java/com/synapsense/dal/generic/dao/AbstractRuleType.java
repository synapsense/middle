package com.synapsense.dal.generic.dao;

import java.util.Collection;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import com.synapsense.dal.generic.entities.RuleTypeDO;

/**
 * Abstract class for Rule type implementation There are two actual
 * implementation related to DRE and CRE
 * 
 * @author shabanov
 * 
 */
public abstract class AbstractRuleType {
	/**
	 * SELECT ALL rule types query
	 */
	private static final String SELECT_ALL = "RuleTypeDO.by.discriminator";
	/**
	 * SELECT One rule type by name query
	 */
	private static final String SELECT_BY_NAME = "RuleTypeDO.by.name.and.discriminator";
	/**
	 * SELECT One rule type by id query
	 */
	private static final String SELECT_BY_ID = "RuleTypeDO.by.id.and.discriminator";

	/**
	 * Creates <code>Query</code> by specified <code>queryText</code>
	 * 
	 * @param queryText
	 *            String constists of query
	 * @return
	 */
	private Query createAdoptedQuery(String queryText) {
		return getEM().createNamedQuery(queryText).setParameter("discriminator", getDiscriminator());
	}

	/**
	 * Returns the underlying <code>EntityManager</code>
	 * 
	 * @return <code>EntityManager</code>
	 */
	protected abstract EntityManager getEM();

	/**
	 * Sets the underlying <code>EntityManager</code>
	 */
	protected abstract void setEM(EntityManager em);

	/**
	 * Returns discriminator of underlying implementaiton Used to decide the
	 * type belongs to concrete implementation
	 * 
	 * @return Integer discriminator value
	 */
	protected abstract Integer getDiscriminator();

	protected RuleTypeDO findRuleTypeByName(String ruleTypeName) {
		RuleTypeDO result = null;
		try {
			result = (RuleTypeDO) createAdoptedQuery(AbstractRuleType.SELECT_BY_NAME)
			        .setParameter("name", ruleTypeName).getSingleResult();
		} catch (NoResultException e) {

		}
		return result;
	}

	/**
	 * Finds Rule type in storage by id
	 * 
	 * @param id
	 * @return
	 */
	protected RuleTypeDO findRuleTypeById(Integer id) {
		RuleTypeDO result = null;
		try {
			result = (RuleTypeDO) createAdoptedQuery(AbstractRuleType.SELECT_BY_ID).setParameter("id", id)
			        .getSingleResult();
		} catch (NoResultException e) {

		}
		return result;
	}

	/**
	 * Finds all Rule types in storage
	 * 
	 * @return Collection of POJO
	 */
	protected Collection<RuleTypeDO> findAllRuleTypes() {
		Collection<RuleTypeDO> result = createAdoptedQuery(AbstractRuleType.SELECT_ALL).getResultList();
		return result;
	}
}
