package com.synapsense.dal.objectcache;

import com.synapsense.util.CollectionUtils;

import java.util.Collection;
import java.util.Collections;

/**
 * Untyped collection value
 */
//TODO : unambiguous nulls elements support
// now we can set null as an element of UntypedCollectionValue
// but we cannot remove such elements via removeProperty value because of its precondition
//TODO : Implement typed Collection (example - collection of text values)
class UntypedCollectionValue extends Value<Collection<Object>> {

	private static final long serialVersionUID = -2506106195466716123L;

    UntypedCollectionValue() {
        data = CollectionUtils.newSet();
    }

	@Override
	public Collection<Object> getValue() {
		return Collections.unmodifiableCollection(data);
	}

	@Override
	public void setValue(Collection<Object> value) {
		if (value == null)
			return;
		data = CollectionUtils.newSet();
		data.addAll(value);
	}
}