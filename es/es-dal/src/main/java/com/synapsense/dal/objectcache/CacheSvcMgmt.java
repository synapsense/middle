package com.synapsense.dal.objectcache;

public interface CacheSvcMgmt {

	void create() throws Exception;

	void start() throws Exception;

	void stop();

	void destroy();

	Object queryPropInfo(String type, Integer oid, String propName, String clazz);

	Object queryObjectInfo(String type, Integer obj);

	Object printAllInfo();

	Object queryTypeInfo(String typeName);

	void runAudit();

	void rebaseNumAlerts();
}
