package com.synapsense.dal.generic.entities;

public class Valuei extends AbstractValue<Integer> implements java.io.Serializable {
	private static final long serialVersionUID = 961134944682964442L;

	private Integer id;
	private ValueDO valueRef;
	private Integer value;

	Valuei() {
		super();
	}

	public Valuei(ValueDO valueRef) {
		this(valueRef, null);
	}

	public Valuei(ValueDO valueRef, Object value) {
		this();
		this.valueRef = valueRef;
		if (value == null) {
			return;
		}
		this.value = (Integer) value;
	}

	public Integer getId() {
		return this.id;
	}

	private void setId(Integer id) {
		this.id = id;
	}

	public ValueDO getValueRef() {
		return this.valueRef;
	}

	public void setValueRef(ValueDO valueRef) {
		this.valueRef = valueRef;
	}

	public Integer getValue() {
		return this.value;
	}

	public void setValue(Integer value) {
		this.value = value;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		result = prime * result + ((valueRef == null) ? 0 : valueRef.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final Valuei other = (Valuei) obj;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		if (valueRef == null) {
			if (other.valueRef != null)
				return false;
		} else if (!valueRef.equals(other.valueRef))
			return false;
		return true;
	}

}
