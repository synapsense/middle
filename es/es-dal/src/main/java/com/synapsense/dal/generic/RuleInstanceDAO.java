package com.synapsense.dal.generic;

import java.util.Collection;

import com.synapsense.dto.CRERule;
import com.synapsense.dto.RuleType;
import com.synapsense.dto.TO;
import com.synapsense.exception.ObjectExistsException;
import com.synapsense.exception.ObjectNotFoundException;

/**
 * Interface used to interact with rule instances storage mechanism
 * 
 * @author shabanov
 * 
 */
public interface RuleInstanceDAO<KEY_TYPE> {
	/**
	 * Allows caller to create new rule instance. After executing the new rule
	 * instance will be created without any reference to source/depend
	 * properties although the argument <code>config</code> includes information
	 * about one depend/source property.
	 * 
	 * @param rule
	 *            Rule object
	 * @return Rule instance ID
	 * @throws ObjectExistsException
	 *             if rule instance already exists
	 * @throws ObjectNotFoundException
	 *             if there is no rule type with the specified rule type name in
	 *             <code>config</code>
	 */
	public TO<KEY_TYPE> create(CRERule rule) throws ObjectExistsException, ObjectNotFoundException;

	/**
	 * Allows caller to get rule instance ID by the specified name;
	 * 
	 * @param name
	 *            Rule name
	 * @return ID of rule instance or null if no rule instance with the
	 *         specified <code>name</code>
	 */
	public TO<KEY_TYPE> getRuleInstanceByName(String name);

	/**
	 * Allows caller to get rule instance by the specified key;
	 * 
	 * @param ruleId
	 *            rule TO
	 * @return DTO of rule instance or null if no rule instance with the
	 *         specified <code>name</code>
	 */
	public CRERule getRuleDTOById(KEY_TYPE ruleId);

	/**
	 * Allows caller to get reference to rule type which owns the specified rule
	 * instance.
	 * 
	 * @param id
	 *            PK
	 * @return Rule type ID
	 * @throws ObjectNotFoundException
	 *             if there is no rule instance with the specified
	 *             <code>id</code>
	 */
	public TO<?> getRuleType(KEY_TYPE id) throws ObjectNotFoundException;

	/**
	 * Allows caller to get <code>RuleType</code> DTO of related rule type
	 * 
	 * @param id
	 *            PK
	 * @return <code>RuleType</code> instance
	 * @throws ObjectNotFoundException
	 *             if there is no rule instance with the specified
	 *             <code>id</code>
	 * @see #getRuleType(KEY_TYPE id) to get only pointer to rule type
	 */
	public RuleType getRuleTypeDTO(KEY_TYPE id) throws ObjectNotFoundException;

	/**
	 * Allows caller to get all rule instances which are owned by the specified
	 * <code>ruleTypeName</code>
	 * 
	 * @param ruleTypeName
	 *            Name of the rule type
	 * @return Collection of rule instances IDs
	 */
	public Collection<TO<KEY_TYPE>> getRuleInstancesByType(String ruleTypeName);

	/**
	 * Allows caller to get all rule DTOs which are owned by the specified
	 * <code>ruleTypeName</code>
	 * 
	 * @param ruleTypeName
	 *            name of the rule type
	 * @return collection of rule DTOs
	 */
	public Collection<CRERule> getRulesDTOByType(String ruleTypeName);

	/**
	 * Allows caller to get rule instance name.
	 * 
	 * @param id
	 *            PK
	 * @return Rule instance name
	 * @throws ObjectNotFoundException
	 *             if there is no rule instance with the specified
	 *             <code>id</code>
	 */
	public String getName(KEY_TYPE id) throws ObjectNotFoundException;

	/**
	 * Allows caller to set new rule instance name.
	 * 
	 * @param id
	 *            PK
	 * @param name
	 *            New name
	 * @throws ObjectNotFoundException
	 *             if there is no rule instance with the specified
	 *             <code>id</code>
	 * @throws ObjectExistsException
	 *             if rule instance with the specified <code>name</code> already
	 *             exists
	 */
	public void setName(KEY_TYPE id, String name) throws ObjectNotFoundException, ObjectExistsException;

	/**
	 * Allows caller to get all existing rule instances.
	 * 
	 * @return Collection of rule type IDs
	 */
	public Collection<TO<KEY_TYPE>> getAllRuleInstances();

	/**
	 * Allows caller to remove the specified rule instance.
	 * 
	 * @param id
	 *            PK
	 */
	public boolean delete(KEY_TYPE id);

	/**
	 * Allows caller to remove all existing rule instances.
	 */
	public void deleteAll();

	/**
	 * Returns all Rule which are tied with the passed hostTO
	 * 
	 * @param hostTO
	 * @return
	 */
	public Collection<CRERule> getRuleDTOsByHostObject(TO<?> hostTO);

	/**
	 * Returns all Rule which are tied with the passed hostTO
	 * 
	 * @param hostTO
	 * @return
	 */
	public Collection<TO<KEY_TYPE>> getRulesByHostObject(TO<?> hostTO);
}
