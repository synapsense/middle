package com.synapsense.dal.generic.batch;

import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import com.synapsense.util.CollectionUtils;
import com.synapsense.util.CollectionUtils.Predicate;

abstract class BaseCache<T, V> implements Iterable<T> {
	private ConcurrentMap<T, V> container;

	protected ConcurrentMap<T, V> getContainer() {
		return this.container;
	}

	protected BaseCache() {
		this.container = new ConcurrentHashMap<>();
	}

	protected BaseCache(ConcurrentMap<T, V> container) {
		this.container = container;
	}

	public abstract boolean isHistorical(T key);

	public void put(T key, V v) {
		if (container.containsKey(key))
			return;
		
		container.put(key, v);
	
		if (DbBatchUpdaterService.logger.isDebugEnabled()) {
			DbBatchUpdaterService.logger.debug("New mapping item was registered, key=" + key + ", value=" + v);
		}
	}

	public boolean containsKey(T key) {
		return container.containsKey(key);
	}

	public V get(T key) {
		return container.get(key);
	}

	public boolean remove(T key) {
		boolean removed = container.remove(key) != null;
		if (removed && DbBatchUpdaterService.logger.isDebugEnabled()) {
			DbBatchUpdaterService.logger.debug("Value mapping item was unregistered, key=" + key);
		}
		return removed;
	}

	public int remove(Predicate<T> predicate) {

		Set<T> removed = CollectionUtils.newSet();
		for (Iterator<T> i = container.keySet().iterator(); i.hasNext();) {
			T key = i.next();
			if (predicate.evaluate(key)) {
				i.remove();
				removed.add(key);
			}
		}

		if (!removed.isEmpty() && DbBatchUpdaterService.logger.isDebugEnabled()) {
			DbBatchUpdaterService.logger.debug("Released [" + removed + "] value mappings matching predicate");
		}

		return removed.size();
	}

	public void clear() {
		container.clear();
	}

	@Override
	public Iterator<T> iterator() {
		return container.keySet().iterator();
	}

}