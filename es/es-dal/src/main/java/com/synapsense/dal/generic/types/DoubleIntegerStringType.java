package com.synapsense.dal.generic.types;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.StringTokenizer;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.type.IntegerType;
import org.hibernate.type.ShortType;
import org.hibernate.type.Type;
import org.hibernate.usertype.CompositeUserType;

public class DoubleIntegerStringType implements CompositeUserType {

	private static final int[] TYPES = { Types.SMALLINT, Types.INTEGER };
	private final static String INVALID_FORMAT_MSG = "Invalid format: version string should contain version and revision numbers delimited by space";

	Short version;
	Integer revision;

	private static Object[] parseStr(String value) {
		StringTokenizer verAndRev = new StringTokenizer(value);
		if (verAndRev.countTokens() != 2)
			return null;
		try {
			short version = Short.parseShort(verAndRev.nextToken());
			if (version < 0 || version > 255) {
				return null;
			}
			int revision = Integer.parseInt(verAndRev.nextToken());
			if (revision < 0 || revision > 16777215) {
				return null;
			}

			return new Object[] { version, revision };

		} catch (NumberFormatException e) {
			return null;
		}

	}

	/**
	 * Validates input value to satisfy the internal format
	 * 
	 * @param value
	 *            String to validate
	 * @return true - if given value is correct or false
	 */
	public static boolean validate(String value) {
		return (parseStr(value) != null);
	}

	public int[] sqlTypes() {
		return TYPES;
	}

	/**
	 * The class returned by <tt>nullSafeGet()</tt>.
	 * 
	 * @return Class
	 */
	public Class<String> returnedClass() {
		return String.class;
	}

	/**
	 * Compare two instances of the class mapped by this type for persistence
	 * "equality". Equality of the persistent state.
	 * 
	 * @param x
	 * @param y
	 * @return boolean
	 * @throws HibernateException
	 */
	public boolean equals(Object x, Object y) {
		if (x == y)
			return true;
		if (x == null || y == null)
			return false;
		return ((String) x).equals(((String) y));
	}

	/**
	 * Get a hashcode for the instance, consistent with persistence "equality"
	 */
	public int hashCode(Object x) {
		String a = (String) x;
		return a.hashCode();
	}

	/**
	 * Return a deep copy of the persistent state, stopping at entities and at
	 * collections.
	 * 
	 * @param value
	 *            generally a collection element or entity field
	 * @return Object a copy
	 * @throws HibernateException
	 */
	public Object deepCopy(Object x) {
		if (x == null)
			return null;
		String result = new String();
		String input = (String) x;
		result = input;
		return result;
	}

	/**
	 * Check if objects of this type mutable.
	 * 
	 * @return boolean
	 */
	public boolean isMutable() {
		return true;
	}

	/**
	 * Retrieve an instance of the mapped class from a JDBC resultset.
	 * Implementors should handle possibility of null values.
	 * 
	 * @param rs
	 *            a JDBC result set
	 * @param names
	 *            the column names
	 * @param session
	 * @param owner
	 *            the containing entity
	 * @return Object
	 * @throws HibernateException
	 * @throws SQLException
	 */
	public Object nullSafeGet(ResultSet rs, String[] names, SessionImplementor session, Object owner)
	        throws HibernateException, SQLException {

		Short version = ShortType.INSTANCE.nullSafeGet(rs, names[0], session);
		Integer revision = IntegerType.INSTANCE.nullSafeGet(rs, names[1], session);

		if (version == null && revision == null)
			return null;
		else {
			return Short.toString(version) + " " + Integer.toString(revision);
		}
	}

	/**
	 * Write an instance of the mapped class to a prepared statement.
	 * Implementors should handle possibility of null values. A multi-column
	 * type should be written to parameters starting from <tt>index</tt>.
	 * 
	 * @param st
	 *            a JDBC prepared statement
	 * @param value
	 *            the object to write
	 * @param index
	 *            statement parameter index
	 * @param session
	 * @throws HibernateException
	 * @throws SQLException
	 */
	public void nullSafeSet(PreparedStatement st, Object value, int index, SessionImplementor session)
	        throws HibernateException, SQLException {
		if (value == null) {
			st.setNull(index, TYPES[0]);
			st.setNull(index + 1, TYPES[1]);
			return;
		}

		Object[] tuple = parseStr((String) value);
		if (tuple == null) {
			throw new HibernateException(INVALID_FORMAT_MSG);
		}
		ShortType.INSTANCE.nullSafeSet(st, tuple[0], index, session);
		ShortType.INSTANCE.nullSafeSet(st, tuple[1], index + 1, session);
	}

	/**
	 * Get the "property names" that may be used in a query.
	 * 
	 * @return an array of "property names"
	 */
	public String[] getPropertyNames() {
		return new String[] { "version", "revision" };
	}

	/**
	 * Get the corresponding "property types".
	 * 
	 * @return an array of Hibernate types
	 */
	public Type[] getPropertyTypes() {
		return new Type[] { ShortType.INSTANCE, IntegerType.INSTANCE };
	}

	/**
	 * Get the value of a property.
	 * 
	 * @param component
	 *            an instance of class mapped by this "type"
	 * @param property
	 * @return the property value
	 * @throws HibernateException
	 */
	public Object getPropertyValue(Object component, int property) throws HibernateException {
		String str = (component == null) ? new String() : (String) component;
		parseStr(str);
		switch (property) {
		case 0:
			return version;
		case 1:
			return revision;
		default:
			throw new HibernateException("Invalid property index");
		}
	}

	/**
	 * Set the value of a property.
	 * 
	 * @param component
	 *            an instance of class mapped by this "type"
	 * @param property
	 * @param value
	 *            the value to set
	 * @throws HibernateException
	 */
	public void setPropertyValue(Object component, int property, Object value) throws HibernateException {

		switch (property) {
		case 0:
			version = (Short) value;
			break;
		case 1:
			revision = (Integer) value;
			break;
		default:
			throw new HibernateException("Invalid property index");
		}

	}

	/**
	 * Reconstruct an object from the cacheable representation. At the very
	 * least this method should perform a deep copy. (optional operation)
	 * 
	 * @param cached
	 *            the object to be cached
	 * @param session
	 * @param owner
	 *            the owner of the cached object
	 * @return a reconstructed object from the cachable representation
	 * @throws HibernateException
	 */
	public Object assemble(Serializable cached, SessionImplementor session, Object owner) {

		return deepCopy(cached);
	}

	/**
	 * Transform the object into its cacheable representation. At the very least
	 * this method should perform a deep copy. That may not be enough for some
	 * implementations, however; for example, associations must be cached as
	 * identifier values. (optional operation)
	 * 
	 * @param value
	 *            the object to be cached
	 * @param session
	 * @return a cachable representation of the object
	 * @throws HibernateException
	 */
	public Serializable disassemble(Object value, SessionImplementor session) {
		return (Serializable) deepCopy(value);
	}

	/**
	 * During merge, replace the existing (target) value in the entity we are
	 * merging to with a new (original) value from the detached entity we are
	 * merging. For immutable objects, or null values, it is safe to simply
	 * return the first parameter. For mutable objects, it is safe to return a
	 * copy of the first parameter. However, since composite user types often
	 * define component values, it might make sense to recursively replace
	 * component values in the target object.
	 * 
	 * @param original
	 * @param target
	 * @param session
	 * @param owner
	 * @return
	 * @throws HibernateException
	 */
	public Object replace(Object original, Object target, SessionImplementor session, Object owner)
	        throws HibernateException {
		return original;
	}

}
