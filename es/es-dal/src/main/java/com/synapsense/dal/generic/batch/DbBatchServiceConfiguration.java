package com.synapsense.dal.generic.batch;

/**
 * Default values of batch updater configuration. Externalize this to file if
 * needed.
 * 
 * @author ashabanov
 * 
 */
public final class DbBatchServiceConfiguration {
	static final String SERVICE_NAME = "DB_BATCH_UPDATER";

	/**
	 * batch size
	 */
	static int BATCH_SIZE = 40000;

	/**
	 * max buffer capacity
	 */
	static int QUEUE_SIZE = 1000000;

	/**
	 * how many threads should be used by batch tasks executor
	 */
	static int THREADS_NUMBER = 1; // strongly recommended because of new batch
								   // updater impl

	/**
	 * time interval in ms to flush dirty data
	 */
	public static long FLUSH_CHANGES_DELAY = 1 * 1000; // ms

	/**
	 * time interval when first flush task will start
	 */
	static final long FIRST_FLUSH_CHANGES_DELAY = 5 * 1000; // ms

	static final String DECIMAL_FORMAT = "#########.###";

	static final String TIMESTAMP_FORMAT = "yyyy-MM-dd H:m:s";

	static final char DECIMAL_SEPARATOR = '.';

	static final String NULL_VALUE_REPLACER = "\\N";

	/**
	 * Configuration print
	 * 
	 * @return
	 */
	public static String getInfo() {
		StringBuilder sb = new StringBuilder(300);
		sb.append("Configuration:\n");
		sb.append("BATCH_SIZE=" + BATCH_SIZE + "\n");
		sb.append("QUEUE_SIZE=" + QUEUE_SIZE + "\n");
		sb.append("THREADS_NUMBER=" + THREADS_NUMBER + "\n");
		sb.append("FLUSH_CHANGES_DELAY=" + FLUSH_CHANGES_DELAY + "ms\n");
		sb.append("FIRST_FLUSH_CHANGES_DELAY=" + FIRST_FLUSH_CHANGES_DELAY + "ms\n");
		sb.append("DECIMAL_FORMAT=" + DECIMAL_FORMAT + "\n");
		sb.append("TIMESTAMP_FORMAT=" + TIMESTAMP_FORMAT + "\n");
		sb.append("DECIMAL_SEPARATOR=" + DECIMAL_SEPARATOR + "\n");
		sb.append("NULL_VALUE_REPLACER=" + NULL_VALUE_REPLACER + "\n");
		return sb.toString();
	}

}
