package com.synapsense.dal.generic;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.ejb.Local;
import javax.ejb.Singleton;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.interceptor.Interceptors;
import javax.transaction.UserTransaction;

import org.apache.log4j.Logger;

import com.synapsense.dal.generic.interceptors.SessionFilterNonStaticOnly;
import com.synapsense.dal.generic.utils.Util;
import com.synapsense.dal.objectcache.CacheDependant;
import com.synapsense.dal.objectcache.CacheSvc;
import com.synapsense.dal.objectcache.Status;
import com.synapsense.dto.BinaryData;
import com.synapsense.dto.CollectionTO;
import com.synapsense.dto.EnvObject;
import com.synapsense.dto.EnvObjectBundle;
import com.synapsense.dto.EnvObjectHistory;
import com.synapsense.dto.ObjectType;
import com.synapsense.dto.ObjectTypeMatchMode;
import com.synapsense.dto.PropertyDescr;
import com.synapsense.dto.Relation;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOBundle;
import com.synapsense.dto.Tag;
import com.synapsense.dto.TagDescriptor;
import com.synapsense.dto.Types;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.EnvException;
import com.synapsense.exception.EnvSystemException;
import com.synapsense.exception.IllegalInputParameterException;
import com.synapsense.exception.InvalidInitialDataException;
import com.synapsense.exception.ObjectExistsException;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.exception.PropertyNotFoundException;
import com.synapsense.exception.ServerConfigurationException;
import com.synapsense.exception.TagNotFoundException;
import com.synapsense.exception.UnableToConvertPropertyException;
import com.synapsense.exception.UnableToConvertTagException;
import com.synapsense.exception.UnsupportedRelationException;
import com.synapsense.search.Element;
import com.synapsense.search.QueryDescription;
import com.synapsense.search.Results;
import com.synapsense.service.Environment;
import com.synapsense.service.impl.dao.DataObjectDAO;
import com.synapsense.service.impl.dao.NetworkStatisticDAOIF;
import com.synapsense.service.impl.messages.base.StatisticData;
import com.synapsense.util.CollectionUtils;

//@PoolClass(value = org.jboss.ejb3.StrictMaxPool.class, maxSize = 100, timeout = 10000)
@Singleton
@TransactionManagement(TransactionManagementType.BEAN)
@Local({Environment.class, CacheDependant.class})
@ConcurrencyManagement(ConcurrencyManagementType.BEAN)
public class GenericEnvironment implements Environment, CacheDependant {
	
	final private static Logger log = Logger.getLogger(GenericEnvironment.class);

	@Resource
	private UserTransaction tx;

	private DataObjectDAO<Integer, StatisticData> statDAO;

	private CacheSvc cache;

	private Environment envDAO;

	public Environment getEnvDAO() {
		return envDAO;
	}

	@EJB(beanName="GenericDAO")
	public void setEnvDAO(final Environment envDAO) {
		this.envDAO = envDAO;
	}

	public DataObjectDAO<Integer, StatisticData> getStatDAO() {
		return statDAO;
	}

	@EJB(beanInterface=NetworkStatisticDAOIF.class,
			beanName="NetworkStatisticDAO")
	public void setStatDAO(final DataObjectDAO<Integer, StatisticData> statDAO) {
		this.statDAO = statDAO;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public <RETURN_TYPE> RETURN_TYPE getPropertyValue(final TO<?> objId, final String propName,
	        final Class<RETURN_TYPE> clazz) throws PropertyNotFoundException, UnableToConvertPropertyException,
	        ObjectNotFoundException {

		if (objId == null) {
			throw new IllegalInputParameterException("Supplied 'objId' is null");
		}

		if (propName == null) {
			throw new IllegalInputParameterException("Supplied 'propName' is null");
		}

		if (propName.isEmpty()) {
			throw new IllegalInputParameterException("Supplied 'propName' is empty");
		}

		if (clazz == null) {
			throw new IllegalInputParameterException("Supplied 'clazz' is null");
		}

		if (cache != null && cache.getStatus() == Status.ONLINE) {
			return cache.getPropertyValue(objId, propName, clazz);
		}

		try {
			return envDAO.getPropertyValue(objId, propName, clazz);
		} catch (ClassCastException e) {
			throw new UnableToConvertPropertyException(propName, clazz, e);
		}
	}

	@Override
	public <REQUESTED_TYPE> void setPropertyValue(final TO<?> objId, final String propName, final REQUESTED_TYPE value)
	        throws PropertyNotFoundException, ObjectNotFoundException, UnableToConvertPropertyException {
		setPropertyValue(objId, propName, value, true);
	}

	@Override
	public <REQUESTED_TYPE> void setPropertyValue(final TO<?> objId, final String propName, final REQUESTED_TYPE value,
	        final boolean persist) throws PropertyNotFoundException, ObjectNotFoundException,
	        UnableToConvertPropertyException {

		if (objId == null) {
			throw new IllegalInputParameterException("Supplied 'objId' is null");
		}

		if (propName == null) {
			throw new IllegalInputParameterException("Supplied 'propName' is null");
		}

		if (propName.isEmpty()) {
			throw new IllegalInputParameterException("Supplied 'propName' is empty");
		}

		if (value != null && value.getClass() == ValueTO.class) {
			setAllPropertiesValues(objId, new ValueTO[] { (ValueTO) value }, persist);
		} else {
			ValueTO valueTO = new ValueTO(propName, value, System.currentTimeMillis());
			setAllPropertiesValues(objId, new ValueTO[] { valueTO }, persist);
		}
	}

	@Override
	public <REQUESTED_TYPE> void addPropertyValue(final TO<?> objId, final String propName, REQUESTED_TYPE value)
	        throws ObjectNotFoundException, PropertyNotFoundException, UnableToConvertPropertyException {
		if (objId == null) {
			throw new IllegalInputParameterException("Supplied 'objId' is null");
		}

		if (propName == null) {
			throw new IllegalInputParameterException("Supplied 'propName' is null");
		}

		if (propName.isEmpty()) {
			throw new IllegalInputParameterException("Supplied 'propName' is empty");
		}
		
		if (value == null){
			throw new IllegalInputParameterException("Supplied 'value' is null");
		}

		begin(tx);
		try {
			envDAO.addPropertyValue(objId, propName, value);
			commit(tx);
		} catch (ObjectNotFoundException e) {
			rollback(tx, e);
			throw e;
		} catch (PropertyNotFoundException e) {
			rollback(tx, e);
			throw e;
		} catch (UnableToConvertPropertyException e) {
			rollback(tx, e);
			throw e;
		} catch (EnvSystemException e) {
			rollback(tx, e);
			throw e;
		} catch (Exception e) {
			rollback(tx, e);
			throw new EJBException("Transaction failed", e);
		}

		if (cache !=null && cache.getStatus() == Status.ONLINE) {
			Collection<REQUESTED_TYPE> c = CollectionUtils.newSetFromCollection(cache.getPropertyValue(objId, propName, Collection.class));
			c.add(value);
			cache.setPropertyValue(objId, propName, c);
		}

	}

	@Override
	public <REQUESTED_TYPE> void removePropertyValue(final TO<?> objId, final String propName,
	        final REQUESTED_TYPE value) throws ObjectNotFoundException, PropertyNotFoundException,
	        UnableToConvertPropertyException {

		if (objId == null) {
			throw new IllegalInputParameterException("Supplied 'objId' is null");
		}

		if (propName == null) {
			throw new IllegalInputParameterException("Supplied 'propName' is null");
		}

		if (propName.isEmpty()) {
			throw new IllegalInputParameterException("Supplied 'propName' is empty");
		}
		
		if (value == null){
			throw new IllegalInputParameterException("Supplied 'value' is null");
		}

		begin(tx);
		try {
			envDAO.removePropertyValue(objId, propName, value);
			commit(tx);
		} catch (ObjectNotFoundException e) {
			rollback(tx, e);
			throw e;
		} catch (PropertyNotFoundException e) {
			rollback(tx, e);
			throw e;
		} catch (UnableToConvertPropertyException e) {
			rollback(tx, e);
			throw e;
		} catch (Exception e) {
			rollback(tx, e);
		}

		if (cache !=null && cache.getStatus() == Status.ONLINE) {
			Collection<REQUESTED_TYPE> c = CollectionUtils.newSetFromCollection(cache.getPropertyValue(objId, propName, Collection.class));
			c.remove(value);
			cache.setPropertyValue(objId, propName, c);
		}

	}

	@Override
	public TO<?> createObject(final String typeName) throws EnvException {
		return createObject(typeName, new ValueTO[] {});
	}

	@Override
	public TOBundle createObjects(EnvObjectBundle objectsToCreate) throws EnvException {
		if (objectsToCreate == null)
			throw new IllegalInputParameterException("Supplied 'objectsToCreate' is null");

		if (objectsToCreate.isEmpty())
			throw new IllegalInputParameterException("Supplied 'objectsToCreate' is empty");

		TOBundle result = null;
		begin(tx);
		try {
			result = envDAO.createObjects(objectsToCreate);
			commit(tx);
		} catch (InvalidInitialDataException e) {
			rollback(tx, e);
			throw e;
		} catch (ObjectExistsException e) {
			rollback(tx, e);
			throw e;
		} catch (PropertyNotFoundException e) {
			rollback(tx, e);
			throw e;
		} catch (UnableToConvertPropertyException e) {
			rollback(tx, e);
			throw e;
		} catch (EnvException e) {
			rollback(tx, e);
			throw e;
		} catch (EnvSystemException e) {
			rollback(tx, e);
			throw e;
		} catch (Exception e) {
			rollback(tx, e);
			throw new EJBException("Transaction failed", e);
		}

		if (cache !=null && cache.getStatus() == Status.ONLINE) {
			for (String label : objectsToCreate.getLabels()) {
				TO to = result.get(label);
				EnvObject envObject = objectsToCreate.get(label);
				cache.createObject(to, envObject.getValues().toArray(new ValueTO[] {}), true);
				for (Tag tag : envObject.getTags()) {
					cache.setTagValue(to, tag.getPropertyName(), tag.getTagName(), tag.getValue());
				}
			}
		}

		return result;
	}

	@Override
	public TO<?> createObject(final String typeName, final ValueTO[] propertyValues) throws EnvException {
		if (typeName == null) {
			throw new IllegalInputParameterException("Supplied 'typeName' is null");
		}
		if (typeName.isEmpty()) {
			throw new IllegalInputParameterException("Supplied 'typeName' is empty");
		}

		if (propertyValues == null) {
			throw new IllegalInputParameterException("Supplied 'propertyValues' is null");
		}

		// check for null-value references and fill the map
		if (propertyValues != null) {
			if (Util.containsNull(Arrays.asList(propertyValues))) {
				throw new IllegalInputParameterException("Supplied 'propertyValues' has null-valued element");
			}
		}

		TO<?> result = null;
		begin(tx);
		try {
			result = envDAO.createObject(typeName, propertyValues);
			commit(tx);
		} catch (InvalidInitialDataException e) {
			rollback(tx, e);
			throw e;
		} catch (ObjectExistsException e) {
			rollback(tx, e);
			throw e;
		} catch (PropertyNotFoundException e) {
			rollback(tx, e);
			throw e;
		} catch (UnableToConvertPropertyException e) {
			rollback(tx, e);
			throw e;
		} catch (EnvException e) {
			rollback(tx, e);
			throw e;
		} catch (EnvSystemException e) {
			rollback(tx, e);
			throw e;
		} catch (Exception e) {
			rollback(tx, e);
			throw new EJBException("Transaction failed", e);
		}

		if (cache !=null && cache.getStatus() == Status.ONLINE) {
			cache.createObject(result, propertyValues, true);
		}

		return result;
	}

	@Override
	public ObjectType createObjectType(final String name) throws EnvException {
		if (name == null) {
			throw new IllegalInputParameterException("Supplied 'name' is null");
		}
		if (name.isEmpty()) {
			throw new IllegalInputParameterException("Supplied 'name' is empty");
		}

		ObjectType newObjectType = null;
		begin(tx);
		try {
			newObjectType = envDAO.createObjectType(name);
			commit(tx);
		} catch (EnvException e) {
			rollback(tx, e);
			throw e;
		} catch (EnvSystemException e) {
			rollback(tx, e);
			throw e;
		} catch (Exception e) {
			rollback(tx, e);
			throw new EJBException("Transaction failed", e);
		}

		if (cache !=null && cache.getStatus() == Status.ONLINE) {
			cache.createObjectType(newObjectType);
		}

		return newObjectType;
	}

	@Override
	public void deleteObject(final TO<?> objId) throws ObjectNotFoundException {
		deleteObject(objId, 0);
	}

	@Override
	public void deleteObject(final TO<?> objId, int level) throws ObjectNotFoundException {
		if (objId == null) {
			throw new IllegalInputParameterException("Supplied 'objId' is null");
		}

		if (level < -1)
			throw new IllegalInputParameterException("Supplied 'level' is invalid. Minimum possible value is -1");

		Collection<TO<?>> objectsToDelete = new LinkedList<TO<?>>();
		// host object should be deleted in any case
		objectsToDelete.add(objId);
		// considering special cases
		if (level == 0) {
			// 0 means to delete only host object
		} else if (level == -1) {
			// -1 means to delete all descendants
			objectsToDelete.addAll(cache.getRelatedObjects(objId, null, true));
		} else {
			objectsToDelete.addAll(cache.getRelatedObjects(objId, level, true));
		}

		begin(tx);
		try {
			for (TO<?> objToDelete : objectsToDelete) {
				envDAO.deleteObject(objToDelete);
			}
			commit(tx);
		} catch (ObjectNotFoundException e) {
			rollback(tx, e);
			throw e;
		} catch (EnvSystemException e) {
			rollback(tx, e);
			throw e;
		} catch (Exception e) {
			rollback(tx, e);
			throw new EJBException("Transaction failed", e);
		}

		if (cache !=null && cache.getStatus() == Status.ONLINE) {
			for (TO<?> objToDelete : objectsToDelete) {
				cache.deleteObject(objToDelete);
			}
		}
	}

	@Override
	public void deleteObjectType(final String typeName) throws EnvException {
		if (typeName == null) {
			throw new IllegalInputParameterException("Supplied 'typeName' is null");
		}
		if (typeName.isEmpty()) {
			throw new IllegalInputParameterException("Supplied 'typeName' is empty");
		}

		begin(tx);
		try {
			envDAO.deleteObjectType(typeName);
			commit(tx);
		} catch (EnvException e) {
			rollback(tx, e);
			throw e;
		} catch (EnvSystemException e) {
			rollback(tx, e);
			throw e;
		} catch (Exception e) {
			rollback(tx, e);
			throw new EJBException("Transaction failed", e);
		}

		if (cache !=null && cache.getStatus() == Status.ONLINE) {
			cache.deleteObjectType(typeName);
		}

	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public Collection<ValueTO> getAllPropertiesValues(final TO<?> objId) throws ObjectNotFoundException {
		if (objId == null) {
			throw new IllegalInputParameterException("Supplied 'objId' is null");
		}

		if (cache !=null && cache.getStatus() == Status.ONLINE) {
			return cache.getAllPropertiesValues(objId);
		}

		return envDAO.getAllPropertiesValues(objId);
	}

	
	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public <RETURN_TYPE> Collection<ValueTO> getHistory(final TO<?> objId, final String propName,
	        final Class<RETURN_TYPE> clazz) throws PropertyNotFoundException, UnableToConvertPropertyException,
	        ObjectNotFoundException {

		checkPropertyClass(objId, propName, clazz);

		switch (Types.decode(objId.getTypeName())) {
		case STAT:
			return statDAO.getData((Integer) objId.getID());
		default:
			return envDAO.getHistory(objId, propName, clazz);
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public <RETURN_TYPE> Collection<ValueTO> getHistory(final TO<?> objId, final String propName, final int size,
	        final int start, final Class<RETURN_TYPE> clazz) throws PropertyNotFoundException,
	        UnableToConvertPropertyException, ObjectNotFoundException {

		checkPropertyClass(objId, propName, clazz);

		if (start < 0) {
			throw new IllegalInputParameterException("Supplied 'start' is negative");
		}

		if (size < 1) {
			throw new IllegalInputParameterException("Supplied 'size' must be  > 1");
		}

		if (clazz == null) {
			throw new IllegalInputParameterException("Supplied 'clazz' is negative");
		}

		switch (Types.decode(objId.getTypeName())) {
		case STAT:
			return statDAO.getData((Integer) objId.getID(), size, start);
		default:
			return envDAO.getHistory(objId, propName, size, start, clazz);
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public EnvObjectHistory getHistory(TO<?> objId, Collection<String> propNames, Date start, Date end)
	        throws ObjectNotFoundException, PropertyNotFoundException {

		if (propNames == null || propNames.isEmpty())
			throw new IllegalInputParameterException("Collection is null or empty");

		if (start == null) {
			throw new IllegalInputParameterException("Supplied 'start' is null");
		}

		if (end == null) {
			throw new IllegalInputParameterException("Supplied 'end' is null");
		}

		if (start.after(end)) {
			throw new IllegalInputParameterException("Supplied 'end':" + end.toString() + " is less than 'start':"
			        + start.toString());
		}
		
		if (objId == null) {
			throw new IllegalInputParameterException("Supplied 'objId' is null");
		}

		if (!objId.getTypeName().equals("ALERT_HISTORY")) { 
			throw new IllegalInputParameterException("Supplied 'objId' has to be ALERT_HISTORY object");
		}
		
		checkProperties(objId, propNames);

		return envDAO.getHistory(objId, propNames, start, end);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public <RETURN_TYPE> Collection<ValueTO> getHistory(final TO<?> objId, final String propName, final Date start,
	        final Date end, final Class<RETURN_TYPE> clazz) throws PropertyNotFoundException,
	        UnableToConvertPropertyException, ObjectNotFoundException {

		checkPropertyClass(objId, propName, clazz);

		if (start == null) {
			throw new IllegalInputParameterException("Supplied 'start' is null");
		}

		if (end == null) {
			throw new IllegalInputParameterException("Supplied 'end' is null");
		}

		if (start.after(end)) {
			throw new IllegalInputParameterException("Supplied 'end':" + end.toString() + " is less than 'start':"
			        + start.toString());
		}

		if (clazz == null) {
			throw new IllegalInputParameterException("Supplied 'clazz' is null");
		}

		switch (Types.decode(objId.getTypeName())) {
		case STAT:
			return statDAO.getData((Integer) objId.getID(), start, end);
		default:
			return envDAO.getHistory(objId, propName, start, end, clazz);
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public <RETURN_TYPE> Collection<ValueTO> getHistory(final TO<?> objId, final String propName, final Date start,
	        final int numberPoints, final Class<RETURN_TYPE> clazz) throws ObjectNotFoundException,
	        PropertyNotFoundException, UnableToConvertPropertyException {

		checkPropertyClass(objId, propName, clazz);

		if (start == null) {
			throw new IllegalInputParameterException("Supplied 'start' is null");
		}

		if (numberPoints < 1) {
			throw new IllegalInputParameterException("Supplied 'numberPoints':" + numberPoints + " is zero or negative");
		}

		if (clazz == null) {
			throw new IllegalInputParameterException("Supplied 'clazz' is null");
		}

		switch (Types.decode(objId.getTypeName())) {
		case STAT:
			return statDAO.getData((Integer) objId.getID(), start, numberPoints);
		default:
			return envDAO.getHistory(objId, propName, start, numberPoints, clazz);
		}

	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public Integer getHistorySize(final TO<?> objId, final String propertyName) throws PropertyNotFoundException,
	        ObjectNotFoundException {

		checkProperty(objId, propertyName);

		switch (Types.decode(objId.getTypeName())) {
		case STAT:
			return statDAO.getDataSize((Integer) objId.getID());
		default:
			return envDAO.getHistorySize(objId, propertyName);
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public ObjectType[] getObjectTypes(final String[] names) {
		if (names == null) {
			throw new IllegalInputParameterException("Supplied 'names' is null");
		}

		// check for null-value references
		for (String name : names) {
			if (name == null) {
				throw new IllegalInputParameterException("Supplied 'names' has null-valued element");
			}

			if (name.isEmpty()) {
				throw new IllegalInputParameterException("Supplied 'names' has empty-valued element");
			}
		}

		List<ObjectType> result = new ArrayList<ObjectType>(names.length);
		ObjectType tempType = null;
		for (String name : names) {
			tempType = getObjectType(name);
			if (tempType != null) {
				result.add(tempType);
			}
		}
		return result.toArray(new ObjectType[result.size()]);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public ObjectType getObjectType(final String name) {
		if (name == null) {
			throw new IllegalInputParameterException("Supplied 'name' is null");
		}

		if (name.isEmpty()) {
			throw new IllegalInputParameterException("Supplied 'name' is empty");
		}

		if (cache !=null && cache.getStatus() == Status.ONLINE) {
			// try to find type by given 'name'
			ObjectType[] types = cache.getObjectTypes(new String[] { name });
			// no type with 'name'
			if (types.length == 0) {
				return null;
			}
			// means that the type has been found and placed as 0 element in the
			// array
			return types[0];
		}
		// generic
		ObjectType[] ots = envDAO.getObjectTypes(new String[] { name });
		if (ots.length == 0) {
			return null;
		}
		return ots[0];

	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public String[] getObjectTypes() {

		if (cache !=null && cache.getStatus() == Status.ONLINE) {
			return cache.getObjectTypes();
		}

		String[] genericTypes = envDAO.getObjectTypes();

		return genericTypes;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public Collection<TO<?>> getObjects(final String typeName, final ObjectTypeMatchMode matchMode,
	        final ValueTO[] propertyValues) {
		if (typeName == null) {
			throw new IllegalInputParameterException("Supplied 'typeName' is null");
		}
		if (typeName.isEmpty()) {
			throw new IllegalInputParameterException("Supplied 'typeName' is empty");
		}

		if (matchMode == null) {
			throw new IllegalInputParameterException("Supplied 'matchMode' is null");
		}

		ValueTO[] tempPropertyValues = (propertyValues == null) ? new ValueTO[] {} : propertyValues;
		// check for null-value references
		for (ValueTO value : tempPropertyValues) {
			if (value == null) {
				throw new IllegalInputParameterException("Supplied 'propertyValues' has null-valued element");
			}
		}

		if (cache !=null && cache.getStatus() == Status.ONLINE) {
			return cache.getObjects(typeName, matchMode, tempPropertyValues);
		}
		
		return envDAO.getObjects(typeName, matchMode, tempPropertyValues);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public Collection<TO<?>> getObjects(final String typeName, final ValueTO[] propertyValues) {
		if (typeName == null) {
			throw new IllegalInputParameterException("Supplied 'typeName' is null");
		}
		if (typeName.isEmpty()) {
			throw new IllegalInputParameterException("Supplied 'typeName' is empty");
		}
		
		if (propertyValues==null) { 
			throw new IllegalInputParameterException("Supplied 'propertyValues' is null");
		}
		
		if (propertyValues.length==0) { 
			throw new IllegalInputParameterException("Supplied 'propertyValues' is empty");
		}
		
		// check for null-value references
		return getObjects(typeName, ObjectTypeMatchMode.DEFAULT, propertyValues);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public Collection<TO<?>> getObjectsByType(final String typeName, final ObjectTypeMatchMode matchMode) {
		if (typeName == null) {
			throw new IllegalInputParameterException("Supplied 'typeName' is null");
		}
		if (typeName.isEmpty()) {
			throw new IllegalInputParameterException("Supplied 'typeName' is empty");
		}
		if (matchMode == null) {
			throw new IllegalInputParameterException("Supplied 'matchMode' is null");
		}

		// if cache is enabled , then proceed execution to the cache impl. and
		// return
		if (cache !=null && cache.getStatus() == Status.ONLINE) {
			return cache.getObjectsByType(typeName, matchMode);
		}

		return envDAO.getObjectsByType(typeName, matchMode);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public Collection<TO<?>> getObjectsByType(final String typeName) {
		return getObjectsByType(typeName, ObjectTypeMatchMode.EXACT_MATCH);
	}

	@Override
	public void setAllPropertiesValues(final TO<?> objId, final ValueTO[] values) throws ObjectNotFoundException,
	        PropertyNotFoundException, UnableToConvertPropertyException {
		setAllPropertiesValues(objId, values, true);
	}

	@Override
	public void setAllPropertiesValues(final TO<?> objId, final ValueTO[] values, final boolean persist)
	        throws ObjectNotFoundException, PropertyNotFoundException, UnableToConvertPropertyException {
		try {
			setAllPropertiesValues(objId, Arrays.asList(values), Collections.<Tag> emptyList(), persist);
		} catch (UnableToConvertTagException e) {
			// the choice was to use EnvException (this affects old cliend code)
			// in throws statement or to ignore impossible exceptions
			throw new EnvSystemException("Environment exception is thrown which should not", e);
		} catch (TagNotFoundException e) {
			// the choice was to use EnvException (this affects old cliend code)
			// in throws statement or to ignore impossible exceptions
			throw new EnvSystemException("Environment exception is thrown which should not", e);
		}
	}

	@Override
	public void setAllPropertiesValues(TO<?> objId, Collection<ValueTO> values, Collection<Tag> tags)
	        throws EnvException {
		setAllPropertiesValues(objId, values, tags, true);
	}

	private void setAllPropertiesValues(TO<?> objId, Collection<ValueTO> values, Collection<Tag> tags, boolean persist)
	        throws ObjectNotFoundException, PropertyNotFoundException, UnableToConvertPropertyException,
	        UnableToConvertTagException, TagNotFoundException {
		if (objId == null) {
			throw new IllegalInputParameterException("Supplied 'objId' is null");
		}

		if (values == null) {
			throw new IllegalInputParameterException("Supplied 'values' is null");
		}

		if (tags == null) {
			throw new IllegalInputParameterException("Supplied 'tags' is null");
		}

		long recordTime = System.currentTimeMillis();

		Collection<ValueTO> binaryValues = new LinkedList<ValueTO>();
		Collection<ValueTO> tempValues = new LinkedList<ValueTO>();
		// check for null-value references
		for (ValueTO value : values) {
			if (value == null) {
				throw new IllegalInputParameterException("Supplied 'values' has null-valued element");
			}

			// change of timestamp by current time if it equals to 0L
			if (value.getTimeStamp() == 0L) {
				value.setTimeStamp(recordTime);
			}
			
			// split binary and non-binary values
			if (value.getValue() instanceof BinaryData){
				binaryValues.add(value);
			}else{
				tempValues.add(value);
			}
		}
		values = tempValues;

		if (!tags.isEmpty()) { 
			validateTags(tags);
		}

		if ((!tags.isEmpty() || !binaryValues.isEmpty()) && persist) {
			// all work should be in single transaction
			begin(tx);
			try {
				// call underlying layer when necessary
				envDAO.setAllTagsValues(objId, tags);
				envDAO.setAllPropertiesValues(objId, binaryValues.toArray(new ValueTO[] {}));
				commit(tx);
			} catch (ObjectNotFoundException e) {
				rollback(tx, e);
				throw e;
			} catch (PropertyNotFoundException e) {
				rollback(tx, e);
				throw e;
			} catch (UnableToConvertTagException e) {
				rollback(tx, e);
				throw e;
			} catch (TagNotFoundException e) {
				rollback(tx, e);
				throw e;
			} catch (EnvSystemException e) {
				rollback(tx, e);
				throw e;
			} catch (Exception e) {
				rollback(tx, e);
				throw new EJBException("Transaction failed", e);
			}
		}

		if (cache !=null && cache.getStatus() == Status.ONLINE) {
			// this must be done in depends of persist flag
			cache.setAllPropertiesValues(objId, values.toArray(new ValueTO[] {}));
			cache.setAllPropertiesValues(objId, binaryValues.toArray(new ValueTO[] {}), false);
			// tags should be put in a cache only if they are stored to storage
			if (!tags.isEmpty() && persist) {
				for (Tag tag : tags) {
					cache.setTagValue(objId, tag.getPropertyName(), tag.getTagName(), tag.getValue());
				}
			}
		}
	}

	private void validateTags(Collection<Tag> tags) throws IllegalInputParameterException {
		for (Tag tag : tags) {
			String propName = tag.getPropertyName();
			String tagName = tag.getTagName();
			Object value = tag.getValue();
			if (propName == null) {
				throw new IllegalInputParameterException("Supplied 'propName' is null");
			}

			if (propName.isEmpty()) {
				throw new IllegalInputParameterException("Supplied 'propName' is empty");
			}

			if (tagName == null) {
				throw new IllegalInputParameterException("Supplied 'tagName' is null");
			}

			if (tagName.isEmpty()) {
				throw new IllegalInputParameterException("Supplied 'tagName' is empty");
			}

			if (value == null) {
				throw new IllegalInputParameterException("Supplied 'values' has null-valued element");
			}
		}
	}

	@Override
	public void updateObjectType(final ObjectType objType) throws EnvException {
		if (objType == null) {
			throw new IllegalInputParameterException("Supplied 'objType' is null");
		}

		begin(tx);
		try {
			envDAO.updateObjectType(objType);
			commit(tx);
		} catch (EnvException e) {
			rollback(tx, e);
			throw e;
		} catch (EnvSystemException e) {
			rollback(tx, e);
			throw e;
		} catch (Exception e) {
			rollback(tx, e);
			throw new EJBException("Transaction failed", e);
		}

		if (cache !=null && cache.getStatus() == Status.ONLINE) {
			cache.updateObjectType(objType);
		}
	}

	@Override
	@Interceptors({ SessionFilterNonStaticOnly.class })
	public void setRelation(final TO<?> parentId, final TO<?> childId) throws ObjectNotFoundException,
	        UnsupportedRelationException {
		if (parentId == null) {
			throw new IllegalInputParameterException("Supplied 'parentId' is null");
		}

		if (childId == null) {
			throw new IllegalInputParameterException("Supplied 'childId' is null");
		}

		setRelation(Arrays.asList(new Relation(parentId, childId)));
	}

	@Override
	@Interceptors({ SessionFilterNonStaticOnly.class })
	public void setRelation(List<Relation> relations) throws ObjectNotFoundException, UnsupportedRelationException {
		if (relations == null) {
			throw new IllegalInputParameterException("Supplied 'relations' is null");
		}

		if (relations.isEmpty()) {
			throw new IllegalInputParameterException("Supplied 'relations' is empty");
		}

		if (Util.containsNull(relations)) {
			throw new IllegalInputParameterException("Supplied 'relations' contains null");
		}

		verifyRelations(relations);

		begin(tx);
		try {
			envDAO.setRelation(relations);
			commit(tx);
		} catch (ObjectNotFoundException e) {
			rollback(tx, e);
			throw e;
		} catch (UnsupportedRelationException e) {
			rollback(tx, e);
			throw e;
		} catch (EnvSystemException e) {
			rollback(tx, e);
			throw e;
		} catch (Exception e) {
			rollback(tx, e);
			throw new EJBException("Transaction failed", e);
		}

		if (cache != null && cache.getStatus() == Status.ONLINE) {
			cache.setRelation(relations);
		}

	}

	private void verifyRelations(Collection<Relation> relations) throws UnsupportedRelationException {
		if (cache == null || cache.getStatus() != Status.ONLINE) {
			return;
		}

		for (Relation relation : relations) {
			TO parent = relation.getParent();
			for (TO child : relation.getChildren()) {
				Collection<TO<?>> descendants = cache.getRelatedObjects(child, null, true);
				// to avoid self-ref cycles
				descendants.add(child);
				if (descendants.contains(parent)) {
					throw new UnsupportedRelationException("Cycle was detected between given child : " + child
					        + " and parent : " + parent);
				}
			}
		}
	}

	@Override
	@Interceptors({ SessionFilterNonStaticOnly.class })
	public void removeRelation(final TO<?> parentId, final TO<?> childId) throws ObjectNotFoundException {
		if (parentId == null) {
			throw new IllegalInputParameterException("Supplied 'parentId' is null");
		}

		if (childId == null) {
			throw new IllegalInputParameterException("Supplied 'childId' is null");
		}

		removeRelation(Arrays.asList(new Relation(parentId, childId)));
	}

	@Override
	@Interceptors({ SessionFilterNonStaticOnly.class })
	public void removeRelation(List<Relation> relations) throws ObjectNotFoundException {
		if (relations == null) {
			throw new IllegalInputParameterException("Supplied 'relations' is null");
		}

		if (relations.isEmpty()) {
			throw new IllegalInputParameterException("Supplied 'relations' is empty");
		}

		if (Util.containsNull(relations)) {
			throw new IllegalInputParameterException("Supplied 'relations' contains null");
		}

		begin(tx);
		try {
			envDAO.removeRelation(relations);
			commit(tx);
		} catch (ObjectNotFoundException e) {
			rollback(tx, e);
			throw e;
		} catch (EnvSystemException e) {
			rollback(tx, e);
			throw e;
		} catch (Exception e) {
			rollback(tx, e);
			throw new EJBException("Transaction failed", e);
		}

		if (cache != null && cache.getStatus() == Status.ONLINE) {
			cache.removeRelation(relations);
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public Collection<TO<?>> getParents(final TO<?> objId, final String typeName) throws ObjectNotFoundException {
		if (objId == null) {
			throw new IllegalInputParameterException("Supplied 'objId' is null");
		}

		if (cache !=null && cache.getStatus() == Status.ONLINE) {
			return cache.getParents(objId, typeName);
		}

		Collection<TO<?>> result = new LinkedList<TO<?>>();
		result.addAll(envDAO.getParents(objId, typeName));
		return result;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public Collection<TO<?>> getParents(final TO<?> objId) throws ObjectNotFoundException {
		return getParents(objId, null);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public Collection<TO<?>> getChildren(final TO<?> objId) throws ObjectNotFoundException {
		return getChildren(objId, null);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public Collection<TO<?>> getChildren(final TO<?> objId, final String typeName) throws ObjectNotFoundException {
		if (objId == null) {
			throw new IllegalInputParameterException("Supplied 'objId' is null");
		}

		if (cache !=null && cache.getStatus() == Status.ONLINE) {
			return cache.getChildren(objId, typeName);
		}

		Collection<TO<?>> objIds = new LinkedList<TO<?>>();
		objIds.add(objId);
		Collection<CollectionTO> children = getChildren(objIds, typeName);

		Collection<TO<?>> result = new LinkedList<TO<?>>();
		if (children.size() == 1) {
			CollectionTO arrayOfChildren = children.iterator().next();
			for (ValueTO pvto : arrayOfChildren.getPropValues()) {
				result.add((TO<?>) pvto.getValue());
			}
		}

		return result;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public Collection<CollectionTO> getChildren(final Collection<TO<?>> objIds, final String typeName) {
		if (objIds == null) {
			throw new IllegalInputParameterException("Supplied 'objIds' is null");
		}
		if (objIds.isEmpty()) {
			throw new IllegalInputParameterException("Supplied 'objIds' is empty");
		}
		if (Util.containsNull(objIds))
			throw new IllegalInputParameterException("Supplied 'objIds' has null-valued element");

		if (cache !=null && cache.getStatus() == Status.ONLINE) {
			return cache.getChildren(objIds, typeName);
		}

		Collection<TO<?>> generics = new LinkedList<TO<?>>();
		for (TO<?> to : objIds) {
			generics.add(to);
		}

		Collection<CollectionTO> result = new LinkedList<CollectionTO>();

		// all generic objects children
		if (generics.size() > 0) {
			Collection<CollectionTO> genericChildren = envDAO.getChildren(generics, typeName);
			if (genericChildren != null) {
				result.addAll(genericChildren);
			}
		}

		return result;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public Collection<CollectionTO> getChildren(final Collection<TO<?>> objIds) {
		return getChildren(objIds, null);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public long getPropertyTimestamp(final TO<?> objId, final String propName) throws ObjectNotFoundException,
	        PropertyNotFoundException {
		if (objId == null) {
			throw new IllegalInputParameterException("Supplied 'objId' is null");
		}
		if (propName == null) {
			throw new IllegalInputParameterException("Supplied 'propName' is null");
		}
		if (propName.isEmpty()) {
			throw new IllegalInputParameterException("Supplied 'propName' is empty");
		}

		if (cache !=null && cache.getStatus() == Status.ONLINE) {
			try {
				return cache.getPropertyValue(objId, propName, ValueTO.class).getTimeStamp();
			} catch (UnableToConvertPropertyException e) {
				log.warn("Trying to ");
				return 0;
			}
		}

		return envDAO.getPropertyTimestamp(objId, propName);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public PropertyDescr getPropertyDescriptor(final String typeName, final String propName)
	        throws ObjectNotFoundException, PropertyNotFoundException {
		if (typeName == null) {
			throw new IllegalInputParameterException("Supplied 'typeName' is null");
		}

		if (typeName.isEmpty()) {
			throw new IllegalInputParameterException("Supplied 'typeName' is empty");
		}

		if (propName == null) {
			throw new IllegalInputParameterException("Supplied 'propName' is null");
		}

		if (propName.isEmpty()) {
			throw new IllegalInputParameterException("Supplied 'propName' is empty");
		}

		if (cache !=null && cache.getStatus() == Status.ONLINE) {
			return cache.getPropertyDescriptor(typeName, propName);
		}

		// gets object type by name
		ObjectType type = getObjectType(typeName);
		// if no such
		if (type == null) {
			throw new ObjectNotFoundException("There is no type with name : " + typeName);
		}

		PropertyDescr result = null;
		// finds property with the specified name
		for (PropertyDescr pd : type.getPropertyDescriptors()) {
			if (pd.getName().equals(propName)) {
				result = pd;
				break;
			}
		}
		// if no such property
		if (result == null) {
			throw new PropertyNotFoundException("There is no property : " + propName + " of type  : " + typeName);
		}

		return result;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public Collection<CollectionTO> getPropertyValue(final Collection<TO<?>> objIds, final String[] propNames) {
		if (objIds == null) {
			throw new IllegalInputParameterException("Supplied 'objIds' is null");
		}

		if (objIds.isEmpty()) {
			throw new IllegalInputParameterException("Supplied 'objIds' is empty");
		}

		if (Util.containsNull(objIds)) {
			throw new IllegalInputParameterException("Supplied 'objIds' has null-valued element");
		}

		if (propNames == null) {
			throw new IllegalInputParameterException("Supplied 'propNames' is null");
		}

		if (propNames.length == 0) {
			throw new IllegalInputParameterException("Supplied 'propNames' is empty");
		}

		for (String propName : propNames) {
			if (propName == null) {
				throw new IllegalInputParameterException("Supplied 'propNames' has null-valued element");
			}
			if (propName.isEmpty()) {
				throw new IllegalInputParameterException("Supplied 'propNames' has empty-valued element");
			}
		}

		if (cache !=null && cache.getStatus() == Status.ONLINE) {
			return cache.getPropertyValue(objIds, propNames);
		}

		Collection<TO<?>> generics = new LinkedList<TO<?>>();
		// separates collection of obj.ids by type
		for (TO<?> to : objIds) {
			generics.add(to);
		}
		Collection<CollectionTO> result = new LinkedList<CollectionTO>();

		// gets specified prop.values for every object which has been specified
		Collection<CollectionTO> genericProps = envDAO.getPropertyValue(generics, propNames);
		if (genericProps != null) {
			result.addAll(genericProps);
		}

		return result;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public Collection<CollectionTO> getAllPropertiesValues(final Collection<TO<?>> objIds) {

		if (objIds == null) {
			throw new IllegalInputParameterException("Supplied 'objIds' is null");
		}

		if (objIds.isEmpty()) {
			throw new IllegalInputParameterException("Supplied 'objIds' is empty");
		}

		if (Util.containsNull(objIds)) {
			throw new IllegalInputParameterException("Supplied 'objIds' has null-valued element");
		}

		if (cache != null && cache.getStatus() == Status.ONLINE) {
			return cache.getAllPropertiesValues(objIds);
		}
		// gets all prop.values for every object which has been specified
		// all generic objects props
		return envDAO.getAllPropertiesValues(objIds);
	}

	@Override
	public Collection<TO<?>> getRelatedObjects(final TO<?> objId, final String objectType, final boolean isChild) {
		if (objId == null) {
			throw new IllegalInputParameterException("Supplied 'objId' is null");
		}
		if (objectType == null) {
			throw new IllegalInputParameterException("Supplied 'objectType' is null");
		}
		if (objectType.isEmpty()) {
			throw new IllegalInputParameterException("Supplied 'objectType' is empty");
		}

		if (cache !=null && cache.getStatus() == Status.ONLINE) {
			return cache.getRelatedObjects(objId, objectType, isChild);
		}

		Collection<TO<?>> result = new HashSet<TO<?>>();
		Collection<TO<?>> related = envDAO.getRelatedObjects(objId, objectType, isChild);
		// if up by hierarchy
		if (!isChild) {
			return related;
		}
		// if down by ...
		for (TO child : related) {
			result.add(child);
		}
		return result;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public <RETURN_TYPE> RETURN_TYPE getPropertyValue(final String typeName, final String propName,
	        final Class<RETURN_TYPE> clazz) throws ObjectNotFoundException, PropertyNotFoundException,
	        UnableToConvertPropertyException {
		if (typeName == null) {
			throw new IllegalInputParameterException("Supplied 'typeName' is null");
		}

		if (typeName.isEmpty()) {
			throw new IllegalInputParameterException("Supplied 'typeName' is empty");
		}

		if (propName == null) {
			throw new IllegalInputParameterException("Supplied 'propName' is null");
		}

		if (propName.isEmpty()) {
			throw new IllegalInputParameterException("Supplied 'propName' is empty");
		}

		if (clazz == null) {
			throw new IllegalInputParameterException("Supplied 'clazz' is null");
		}

		if (cache !=null && cache.getStatus() == Status.ONLINE) {
			return cache.getPropertyValue(typeName, propName, clazz);
		}

		return envDAO.getPropertyValue(typeName, propName, clazz);
	}

	@Override
	public <REQUESTED_TYPE> void setPropertyValue(final String typeName, final String propName,
	        final REQUESTED_TYPE value) throws ObjectNotFoundException, PropertyNotFoundException,
	        UnableToConvertPropertyException {
		if (typeName == null) {
			throw new IllegalInputParameterException("Supplied 'typeName' is null");
		}

		if (typeName.isEmpty()) {
			throw new IllegalInputParameterException("Supplied 'typeName' is empty");
		}

		if (propName == null) {
			throw new IllegalInputParameterException("Supplied 'propName' is null");
		}

		if (propName.isEmpty()) {
			throw new IllegalInputParameterException("Supplied 'propName' is empty");
		}

		begin(tx);
		try {
			envDAO.setPropertyValue(typeName, propName, value);
			commit(tx);
		} catch (ObjectNotFoundException e) {
			rollback(tx, e);
			throw e;
		} catch (PropertyNotFoundException e) {
			rollback(tx, e);
			throw e;
		} catch (UnableToConvertPropertyException e) {
			rollback(tx, e);
			throw e;
		} catch (EnvSystemException e) {
			rollback(tx, e);
			throw e;
		} catch (Exception e) {
			rollback(tx, e);
			throw new EJBException("Transaction failed", e);
		}

		if (cache !=null && cache.getStatus() == Status.ONLINE) {
			cache.setPropertyValue(typeName, propName, value);
		}
	}

	public CacheSvc getCache() {
		return cache;
	}

	@Override
	public void setCache(final CacheSvc cache) {
		this.cache = cache;
	}

	@Override
	public void configurationComplete() throws ServerConfigurationException {
		// log message should be here
	}

	@Override
	public void configurationStart() throws ServerConfigurationException {
		// log message should be here
	}

	@Override
	public boolean exists(final TO<?> objId) {
		if (objId == null) {
			throw new IllegalInputParameterException("object is null");
		}

		if (objId.getTypeName() == null) {
			throw new IllegalInputParameterException("object Type name is null");
		}

		if (objId.getTypeName().isEmpty()) {
			throw new IllegalInputParameterException("object Type name is empty");
		}
		if (objId.getID() == null) {
			throw new IllegalInputParameterException("object id is null");
		}

		if (cache !=null && cache.getStatus() == Status.ONLINE) {
			return cache.exists(objId);
		}

		return envDAO.exists(objId);
	}

	private void checkProperty(TO<?> id, String propName) throws ObjectNotFoundException, PropertyNotFoundException,
	        IllegalInputParameterException {
		if (propName == null || propName.isEmpty())
			throw new IllegalInputParameterException("propName is null or empty");
		// check object's presence
		if (!exists(id))
			throw new ObjectNotFoundException(id);

		// check property presence
		if (cache !=null && cache.getStatus() == Status.ONLINE) {
			cache.getPropertyDescriptor(id.getTypeName(), propName);
		} else {
			envDAO.getPropertyDescriptor(id.getTypeName(), propName);
		}
	}

	private void checkProperties(TO<?> id, Collection<String> propNames) throws ObjectNotFoundException,
	        PropertyNotFoundException {
		for (String propName : propNames) {
			checkProperty(id, propName);
		}
	}

	private void checkPropertyClass(TO<?> id, String propName, Class<?> clazz) throws ObjectNotFoundException,
	        PropertyNotFoundException, UnableToConvertPropertyException, IllegalInputParameterException {
		if (propName == null || propName.isEmpty())
			throw new IllegalInputParameterException("propName is null or empty");
		if (clazz == null)
			throw new IllegalInputParameterException("clazz is null");

		// check object's presence
		if (!exists(id))
			throw new ObjectNotFoundException(id);

		// check property and its type
		PropertyDescr pd;
		if (cache !=null && cache.getStatus() == Status.ONLINE) {
			pd = cache.getPropertyDescriptor(id.getTypeName(), propName);
		} else {
			pd = envDAO.getPropertyDescriptor(id.getTypeName(), propName);
		}
		if (!clazz.equals(ValueTO.class) && !clazz.equals(pd.getType()))
			throw new UnableToConvertPropertyException(propName, pd.getType(), null);
	}

	@Override
	public TagDescriptor getTagDescriptor(String typeName, String propName, String tagName)
	        throws ObjectNotFoundException, PropertyNotFoundException, TagNotFoundException {

		if (typeName == null) {
			throw new IllegalInputParameterException("Supplied 'typeName' is null");
		}

		if (typeName.isEmpty()) {
			throw new IllegalInputParameterException("Supplied 'typeName' is empty");
		}

		if (propName == null) {
			throw new IllegalInputParameterException("Supplied 'propName' is null");
		}

		if (propName.isEmpty()) {
			throw new IllegalInputParameterException("Supplied 'propName' is empty");
		}

		if (tagName == null) {
			throw new IllegalInputParameterException("Supplied 'tagName' is null");
		}

		if (tagName.isEmpty()) {
			throw new IllegalInputParameterException("Supplied 'tagName' is empty");
		}

		if (cache !=null && cache.getStatus() == Status.ONLINE) {
			return cache.getTagDescriptor(typeName, propName, tagName);
		}

		// gets object type by name
		ObjectType type = getObjectType(typeName);
		// if no such
		if (type == null) {
			throw new ObjectNotFoundException("There is no type with name : " + typeName);
		}

		PropertyDescr propDescr = type.getPropertyDescriptor(propName);
		// finds property with the specified name

		// if no such property
		if (propDescr == null) {
			throw new PropertyNotFoundException("There is no property : " + propName + " of type  : " + typeName);
		}

		TagDescriptor result = propDescr.getTagDescriptor(tagName);
		// if no such tag
		if (result == null) {
			throw new TagNotFoundException("There is no tag : " + tagName + " of property  : " + propName
			        + " in type : " + typeName);
		}
		return result;
	}

	@Override
	public <RETURN_TYPE> RETURN_TYPE getTagValue(TO<?> objId, String propName, String tagName, Class<RETURN_TYPE> clazz)
	        throws ObjectNotFoundException, PropertyNotFoundException, TagNotFoundException,
	        UnableToConvertTagException {
		if (objId == null) {
			throw new IllegalInputParameterException("Supplied 'objId' is null");
		}

		if (propName == null) {
			throw new IllegalInputParameterException("Supplied 'propName' is null");
		}

		if (propName.isEmpty()) {
			throw new IllegalInputParameterException("Supplied 'propName' is empty");
		}

		if (tagName == null) {
			throw new IllegalInputParameterException("Supplied 'tagName' is null");
		}

		if (tagName.isEmpty()) {
			throw new IllegalInputParameterException("Supplied 'tagName' is empty");
		}

		if (clazz == null) {
			throw new IllegalInputParameterException("Supplied 'clazz' is null");
		}

		if (cache !=null && cache.getStatus() == Status.ONLINE) {
			return cache.getTagValue(objId, propName, tagName, clazz);
		}

		RETURN_TYPE rawResult;
		rawResult = envDAO.getTagValue(objId, propName, tagName, clazz);

		return rawResult;
	}

	@Override
	public Collection<Tag> getAllTags(TO<?> objId, String propName) throws ObjectNotFoundException,
	        PropertyNotFoundException {
		if (objId == null) {
			throw new IllegalInputParameterException("Supplied 'objId' is null");
		}

		if (propName == null) {
			throw new IllegalInputParameterException("Supplied 'propName' is null");
		}

		if (propName.isEmpty()) {
			throw new IllegalInputParameterException("Supplied 'propName' is empty");
		}

		if (cache !=null && cache.getStatus() == Status.ONLINE) {
			return cache.getAllTags(objId, propName);
		}

		Collection<Tag> tags = envDAO.getAllTags(objId, propName);
		return tags;
	}

	@Override
	public Collection<Tag> getAllTags(TO<?> objId) throws ObjectNotFoundException {
		if (objId == null) {
			throw new IllegalInputParameterException("Supplied 'objId' is null");
		}

		if (cache !=null && cache.getStatus() == Status.ONLINE) {
			return cache.getAllTags(objId);
		}

		Collection<Tag> tags = envDAO.getAllTags(objId);
		return tags;
	}

	@Override
	public Map<TO<?>, Collection<Tag>> getAllTags(String typeName) {
		if (typeName == null) {
			throw new IllegalInputParameterException("Supplied 'typeName' is null");
		}

		if (typeName.isEmpty()) {
			throw new IllegalInputParameterException("Supplied 'typeName' is empty");
		}

		if (cache !=null && cache.getStatus() == Status.ONLINE) {
			return cache.getAllTags(typeName);
		}

		Map<TO<?>, Collection<Tag>> tags = envDAO.getAllTags(typeName);
		return tags;
	}

	@Override
	public Map<TO<?>, Map<String, Collection<Tag>>> getTags(Collection<TO<?>> objIds, List<String> propertyNames,
	        List<String> tagNames) {
		if (objIds == null) {
			throw new IllegalInputParameterException("objects collection is null");
		}
		if (objIds.size() == 0) {
			throw new IllegalInputParameterException("objects collection is empty");
		}

		if (cache !=null && cache.getStatus() == Status.ONLINE) {
			return cache.getTags(objIds, propertyNames, tagNames);
		}
		return envDAO.getTags(objIds, propertyNames, tagNames);
	}

	@Override
	public void setTagValue(TO<?> objId, String propName, String tagName, Object value)
	        throws PropertyNotFoundException, ObjectNotFoundException, TagNotFoundException,
	        UnableToConvertTagException {
		this.setAllTagsValues(objId, Arrays.asList(new Tag(propName, tagName, value)));
	}

	@Override
	public void setAllTagsValues(TO<?> objId, Collection<Tag> tagsToSet) throws PropertyNotFoundException,
	        ObjectNotFoundException, TagNotFoundException, UnableToConvertTagException {
		try {
			setAllPropertiesValues(objId, Collections.<ValueTO> emptyList(), tagsToSet, true);
		} catch (UnableToConvertPropertyException e) {
			// the choice was to use EnvException (this affects old cliend code)
			// in throws statement or to ignore impossible exceptions
			throw new EnvSystemException("Environment exception is thrown which should not", e);
		}
	}

	@Override
	public Collection<TO<?>> getObjects(QueryDescription queryDescr) {
		Results r = cache.executeQuery(queryDescr);

		Collection<TO<?>> foundObjects = CollectionUtils.newList();
		for (Element e : r) {
			foundObjects.add(e.getValue(TO.class));
		}

		return foundObjects;
	}

	private void begin(UserTransaction tx) {
		try {
			tx.begin();
		} catch (Exception e) {
			throw new EJBException("Cannot start transaction", e);
		}
	}

	private void commit(UserTransaction tx) {
		try {
			int txStatus = tx.getStatus();
			if (txStatus == javax.transaction.Status.STATUS_ACTIVE) {
				tx.commit();
			} else {
				log.debug("Could not commit transaction because it is not active. Actual tx status is " + txStatus);
			}
		} catch (Exception e) {
			throw new EJBException("Commit failed", e);
		}
	}

	private void rollback(UserTransaction tx, Exception cause) {
		try {
			int txStatus = tx.getStatus();
			if (txStatus == javax.transaction.Status.STATUS_ACTIVE
			        || txStatus == javax.transaction.Status.STATUS_MARKED_ROLLBACK) {
				tx.rollback();
			} else {
				log.debug("Could not rollback transaction because it is not active. Actual tx status is " + txStatus);
			}
		} catch (Exception e) {
			throw new EJBException("Rollback failed", cause == null ? e : cause);
		}
	}
}
