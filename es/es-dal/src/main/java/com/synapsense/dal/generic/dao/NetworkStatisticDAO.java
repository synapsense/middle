package com.synapsense.dal.generic.dao;

import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import org.apache.log4j.Logger;

import com.synapsense.dal.generic.entities.NetworkStatnode;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.IllegalInputParameterException;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.service.impl.dao.NetworkStatisticDAOIF;
import com.synapsense.service.impl.messages.base.StatisticData;

@Stateless
public class NetworkStatisticDAO implements NetworkStatisticDAOIF<Integer, StatisticData> {

	private final static Logger log = Logger.getLogger(NetworkStatisticDAO.class);

	@PersistenceContext(unitName = "HibernateDAL")
	public void setEM(EntityManager em) {
		this.em = em;
	}

	private EntityManager em;

	private Collection<ValueTO> fillStatdataCollectionToReturn(Collection<NetworkStatnode> netStatistics) {
		Collection<ValueTO> result = new LinkedList<ValueTO>();
		for (NetworkStatnode data : netStatistics) {
			StatisticData sd = new StatisticData();
			sd.setBroadcastRetransmission(data.getBroadcastRetransmission());
			sd.setCrcFailPacketCount(data.getCrcFailPacketCount());
			sd.setForwardingPacketsDropped(data.getForwardingPacketsDropped());
			sd.setFreeSlotCount(data.getFreeSlotCount());
			sd.setLogicalId(data.getLogicalId());
			sd.setNodeLatency(data.getNodeLatency());
			sd.setNumberOneHopRetransmission(data.getNumberOneHopRetransmission());
			sd.setNwkTimestamp(data.getNwkTimestamp());
			sd.setPacketsForwarded(data.getPacketsForwarded());
			sd.setPacketsFromBaseStation(data.getPacketsFromBaseStation());
			sd.setPacketsRcvdCount(data.getPacketsRcvdCount());
			sd.setPacketsSent(data.getPacketsSent());
			sd.setPacketsToBaseStation(data.getPacketsToBaseStation());
			sd.setParentFailCount(data.getParentFailCount());
			sd.setRadioOnTime(data.getRadioOnTime());
			sd.setSendFailsFullQueue(data.getSendFailsFullQueue());
			sd.setSendFailsNoPath(data.getSendFailsNoPath());
			sd.setTimeSyncsLostDriftDetection(data.getTimeSyncsLostDriftDetection());
			sd.setTimeSyncsLostTimeout(data.getTimeSyncsLostTimeout());
			sd.setTotalSlotCount(data.getTotalSlotCount());
			sd.setWrongSlotCount(data.getWrongSlotCount());

			sd.setChStatus(data.getChStatus());
			sd.setHopCount(data.getHopCount());
			sd.setRouterState(data.getRouterState());
			sd.setParentState(data.getParentState());
			sd.setNoiseThresh(data.getNoiseThresh());

			result.add(new ValueTO("lastValue", sd, sd.getNwkTimestamp().getTime()));
		}
		return result;

	}

	@Override
	public void addBulk(Integer id, Collection<StatisticData> data) throws ObjectNotFoundException {

		if (id == null) {
			throw new IllegalInputParameterException("Supplied 'id' is null");
		}

		if (id < 1) {
			throw new IllegalInputParameterException("Supplied 'id' is negative or 0");
		}

		if (data == null) {
			throw new IllegalInputParameterException("Supplied 'data' is null");
		}

		for (StatisticData d : data) {
			addData(id, d);
		}
	}

	@Override
	public void addData(Integer id, StatisticData data) throws ObjectNotFoundException {

		if (id == null) {
			throw new IllegalInputParameterException("Supplied 'id' is null");
		}

		if (id < 1) {
			throw new IllegalInputParameterException("Supplied 'id' is negative or 0");
		}

		if (data == null) {
			throw new IllegalInputParameterException("Supplied 'data' is null");
		}

		NetworkStatnode netstat = new NetworkStatnode(data.getNwkTimestamp());
		{
			netstat.setLogicalId(id);
			netstat.setBroadcastRetransmission(data.getBroadcastRetransmission());
			netstat.setCrcFailPacketCount(data.getCrcFailPacketCount());
			netstat.setForwardingPacketsDropped(data.getForwardingPacketsDropped());
			netstat.setFreeSlotCount(data.getFreeSlotCount());

			netstat.setNodeLatency(data.getNodeLatency());
			netstat.setNumberOneHopRetransmission(data.getNumberOneHopRetransmission());
			netstat.setPacketsForwarded(data.getPacketsForwarded());
			netstat.setPacketsFromBaseStation(data.getPacketsFromBaseStation());
			netstat.setPacketsRcvdCount(data.getPacketsRcvdCount());
			netstat.setPacketsSent(data.getPacketsSent());
			netstat.setPacketsToBaseStation(data.getPacketsToBaseStation());
			netstat.setParentFailCount(data.getParentFailCount());
			netstat.setRadioOnTime(data.getRadioOnTime());
			netstat.setSendFailsFullQueue(data.getSendFailsFullQueue());
			netstat.setSendFailsNoPath(data.getSendFailsNoPath());
			netstat.setTimeSyncsLostDriftDetection(data.getTimeSyncsLostDriftDetection());
			netstat.setTimeSyncsLostTimeout(data.getTimeSyncsLostTimeout());
			netstat.setTotalSlotCount(data.getTotalSlotCount());
			netstat.setWrongSlotCount(data.getWrongSlotCount());
			// additional fields
			netstat.setChStatus(data.getChStatus());// array of ints, stored as
			                                        // string of hex
			netstat.setHopCount(data.getHopCount());
			netstat.setRouterState(data.getRouterState());
			netstat.setParentState(data.getParentState());
			netstat.setNoiseThresh(data.getNoiseThresh());

			em.persist(netstat);

			if (log.isDebugEnabled()) {
				log.debug("Trying to add statistic from Node with ID[" + id + "]");
			}
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public Collection<ValueTO> getData(Integer id) {

		if (id == null) {
			throw new IllegalInputParameterException("Supplied 'id' is null");
		}

		if (id < 1) {
			throw new IllegalInputParameterException("Supplied 'id' is negative or 0");
		}

		List<NetworkStatnode> networkStatistic = em.createNamedQuery("NetworkStatnode.by.logicalId", NetworkStatnode.class)
		        .setParameter("logicalId", id).getResultList();

		return fillStatdataCollectionToReturn(networkStatistic);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public Collection<ValueTO> getData(Integer id, int size, int start) {

		if (id == null) {
			throw new IllegalInputParameterException("Supplied 'id' is null");
		}

		if (id < 1) {
			throw new IllegalInputParameterException("Supplied 'id' is negative or 0");
		}

		if (size < 1) {
			throw new IllegalInputParameterException("Supplied 'size' is negative of 0");
		}

		if (start < 0) {
			throw new IllegalInputParameterException("Supplied 'start' is negative");
		}

		List<NetworkStatnode> networkStatistic = em.createNamedQuery("NetworkStatnode.by.logicalId", NetworkStatnode.class)
		        .setParameter("logicalId", id).setFirstResult(start).setMaxResults(size).getResultList();

		return fillStatdataCollectionToReturn(networkStatistic);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public Double getAverageSlotCount(Integer id, Date startDate, Date endDate) {
		if (id == null) {
			throw new IllegalInputParameterException("Supplied 'id' is null");
		}

		if (id < 1) {
			throw new IllegalInputParameterException("Supplied 'id' is negative or 0");
		}

		if (startDate == null) {
			throw new IllegalInputParameterException("Supplied 'startDate' is null");
		}

		if (endDate == null) {
			throw new IllegalInputParameterException("Supplied 'endDate' is null");
		}

		if (endDate.before(startDate)) {
			throw new IllegalInputParameterException("Supplied time period is wrong : 'endDate' < 'startDate'");
		}

		try {
			Double average = (Double) em
			        .createNamedQuery("NetworkStatnode.by.logicalId.and.between.start.and.end.average.slot.count")
			        .setParameter("logicalId", id).setParameter("start", startDate).setParameter("end", endDate)
			        .getSingleResult();
			return average;
		} catch (NoResultException e) {
			return 0.0;
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public Collection<ValueTO> getData(Integer id, Date startDate, Date endDate) {

		if (id == null) {
			throw new IllegalInputParameterException("Supplied 'id' is null");
		}

		if (id < 1) {
			throw new IllegalInputParameterException("Supplied 'id' is negative or 0");
		}

		if (startDate == null) {
			throw new IllegalInputParameterException("Supplied 'startDate' is null");
		}

		if (endDate == null) {
			throw new IllegalInputParameterException("Supplied 'endDate' is null");
		}

		if (endDate.before(startDate)) {
			throw new IllegalInputParameterException("Supplied time period is wrong : 'endDate' < 'startDate'");
		}

		List<NetworkStatnode> networkStatistic = em
		        .createNamedQuery("NetworkStatnode.by.logicalId.and.between.start.and.end", NetworkStatnode.class)
		        .setParameter("logicalId", id).setParameter("start", startDate).setParameter("end", endDate)
		        .getResultList();

		return fillStatdataCollectionToReturn(networkStatistic);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public Collection<ValueTO> getData(Integer id, Date startDate, int numberPoints) {

		if (id == null) {
			throw new IllegalInputParameterException("Supplied 'id' is null");
		}

		if (id < 1) {
			throw new IllegalInputParameterException("Supplied 'id' is negative or 0");
		}

		if (startDate == null) {
			throw new IllegalInputParameterException("Supplied 'startDate' is null");
		}

		List<NetworkStatnode> networkStatistic = em.createNamedQuery("NetworkStatnode.by.logicalId.and.start", NetworkStatnode.class)
		        .setParameter("logicalId", id).setParameter("start", startDate).setMaxResults(numberPoints)
		        .getResultList();

		return fillStatdataCollectionToReturn(networkStatistic);
	}

	@Override
	public void removeData(Integer id) {
		em.createNamedQuery("NetworkStatnode.delete.by.logicalId").setParameter("logicalId", id).executeUpdate();
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public int getDataSize(Integer id) {
		try {
			return (Integer) em.createNamedQuery("NetworkStatnode.count.by.logicalId").setParameter("logicalId", id)
			        .getSingleResult();
		} catch (NoResultException e) {
			return 0;
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public ValueTO getLatestData(Integer id) {
		try {
			NetworkStatnode networkStatistic = (NetworkStatnode) em
			        .createNamedQuery("NetworkStatnode.by.logicalId.latestFirst").setParameter("logicalId", id)
			        .setMaxResults(1).getSingleResult();
			return fillStatdataCollectionToReturn(Arrays.asList(networkStatistic)).iterator().next();
		} catch (NoResultException e) {
			return null;
		}
	}

}
