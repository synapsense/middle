package com.synapsense.dal.generic.entities;

// Generated 20-Jan-2010 17:34:28 by Hibernate Tools 3.2.0.CR1

/**
 * TagValuesDO generated by hbm2java
 */
public class TagValueDO implements java.io.Serializable {

	private static final long serialVersionUID = -2091256371353412001L;
	private Integer valueId;
	private String value;
	private TagDO tag;
	private ObjectDO object;

	public TagValueDO() {
	}

	public TagValueDO(TagDO tag, ObjectDO object) {
		this.tag = tag;
		this.object = object;
	}

	public TagValueDO(String value, TagDO tag, ObjectDO object) {
		this.value = value;
		this.tag = tag;
		this.object = object;
	}

	public Integer getValueId() {
		return this.valueId;
	}

	public void setValueId(Integer valueId) {
		this.valueId = valueId;
	}

	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public TagDO getTag() {
		return this.tag;
	}

	public void setTag(TagDO tag) {
		this.tag = tag;
	}

	public ObjectDO getObject() {
		return this.object;
	}

	public void setObject(ObjectDO object) {
		this.object = object;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		result = prime * result + ((tag == null) ? 0 : tag.hashCode());
		result = prime * result + ((object == null) ? 0 : object.hashCode());
		result = prime * result + ((valueId == null) ? 0 : valueId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final TagValueDO other = (TagValueDO) obj;
		if (object == null) {
			if (other.object != null)
				return false;
		} else if (!object.equals(other.object))
			return false;
		if (tag == null) {
			if (other.tag != null)
				return false;
		} else if (!tag.equals(other.tag))
			return false;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}
}
