package com.synapsense.dal.generic;

public interface DTOGetter<DTO_TYPE> {
	/**
	 * Returns DTO implementation of the entity
	 * 
	 * @return
	 */
	DTO_TYPE getDTO();

}
