package com.synapsense.dal.objectcache;

import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.search.Element;
import com.synapsense.search.EnvElement;
import com.synapsense.search.Path;
import com.synapsense.search.Traversable;
import com.synapsense.service.impl.dao.to.PropertyTO;
import com.synapsense.util.CollectionUtils;
import org.apache.log4j.Logger;

import java.util.Collection;
import java.util.Set;

class CacheTraversable implements Traversable {

	private final static Logger logger = Logger.getLogger(CacheTraversable.class);

	CacheTraversable(CacheSvcImpl cache) {
		this.cache = cache;
	}

	@Override
	public Collection<Element> traverseNext(Element e, Path path) {
		TO oid = e.getValue(TO.class);

		// TODO : Need some policy here because 'Path' can be configured to
		// move trough parent/child relations
		Set<PropertyTO> toLinks = getConnections(oid, path);
		Set<Element> wereTraversed = CollectionUtils.newSet();
		if (toLinks == null) {
			if (logger.isDebugEnabled()) {
				logger.debug("Cannot traverse cache because object " + oid + " has no outgoing links");
			}
			return wereTraversed;
		}

		for (PropertyTO pair : toLinks) {
			if (path.isPossibleToMove(pair.getPropertyName())) {
				wereTraversed.add(new EnvElement(pair.getObjId()));
			}
		}

		return wereTraversed;
	}

	private Set<PropertyTO> getConnections(TO<?> oid, Path path) {
		switch (path.getDirection()) {
		case OUTGOING:
			return outgoing(oid);
		case INCOMING:
			return incoming(oid);
		default:
			throw new IllegalStateException();
		}
	}

	// TODO : done via cache service api, consider performance and other
	// concerns
	private Set<PropertyTO> outgoing(TO<?> oid) {
		Set<PropertyTO> toLinks = CollectionUtils.newSet();
		try {
			for (ValueTO value : cache.getAllPropertiesValues(oid)) {
				if (value.getValue() instanceof TO) {
					toLinks.add(new PropertyTO(TO.class.cast(value.getValue()), value.getPropertyName()));
				} else if (value.getValue() instanceof Collection) {
					for (Object inCollValue : (Collection) value.getValue()) {
						if (!(inCollValue instanceof TO)) {
							break;
						}
						toLinks.add(new PropertyTO(TO.class.cast(inCollValue), value.getPropertyName()));
					}
				}
			}
		} catch (ObjectNotFoundException e) {
			return toLinks;
		}

		return toLinks;
	}

	private Set<PropertyTO> incoming(TO<?> oid) {
		return cache.getLinksToObject(oid);
	}

	private CacheSvcImpl cache;

}
