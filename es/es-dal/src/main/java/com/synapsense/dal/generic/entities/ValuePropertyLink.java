package com.synapsense.dal.generic.entities;

import com.synapsense.service.impl.dao.to.GenericObjectTO;
import com.synapsense.service.impl.dao.to.PropertyTO;

public class ValuePropertyLink extends AbstractValue<PropertyTO> implements java.io.Serializable {
	private static final long serialVersionUID = -267983495909483296L;
	private Integer id;
	private ValueDO valueRef;
	private Object objectId;
	private String objectType;
	private String propertyName;

	ValuePropertyLink() {
		super();
	}

	public ValuePropertyLink(ValueDO valueRef) {
		this(valueRef, null);
	}

	public ValuePropertyLink(ValueDO valueRef, Object value) {
		this();
		this.valueRef = valueRef;
		if (value == null) {
			return;
		}
		PropertyTO typedValue = (PropertyTO) value;
		this.objectId = typedValue.getObjId().getID();
		this.objectType = typedValue.getObjId().getTypeName();
		this.propertyName = typedValue.getPropertyName();
	}

	public Integer getId() {
		return this.id;
	}

	private void setId(Integer id) {
		this.id = id;
	}

	public ValueDO getValueRef() {
		return this.valueRef;
	}

	public void setValueRef(ValueDO valueRef) {
		this.valueRef = valueRef;
	}

	public PropertyTO getValue() {
		if (this.objectId == null) {
			return null;
		}
		if (this.objectType == null) {
			return null;
		}
		if (this.propertyName == null) {
			return null;
		}
		return new PropertyTO(new GenericObjectTO((Integer) this.objectId, this.objectType), this.propertyName);
	}

	public void setValue(PropertyTO value) {
		if (value == null) {
			this.objectId = null;
			this.objectType = null;
			this.propertyName = null;
		} else {
			this.objectId = (Integer) value.getObjId().getID();
			this.objectType = value.getObjId().getTypeName();
			this.propertyName = value.getPropertyName();
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((objectId == null) ? 0 : objectId.hashCode());
		result = prime * result + ((objectType == null) ? 0 : objectType.hashCode());
		result = prime * result + ((propertyName == null) ? 0 : propertyName.hashCode());
		result = prime * result + ((valueRef == null) ? 0 : valueRef.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final ValuePropertyLink other = (ValuePropertyLink) obj;
		if (objectId == null) {
			if (other.objectId != null)
				return false;
		} else if (!objectId.equals(other.objectId))
			return false;
		if (objectType == null) {
			if (other.objectType != null)
				return false;
		} else if (!objectType.equals(other.objectType))
			return false;
		if (propertyName == null) {
			if (other.propertyName != null)
				return false;
		} else if (!propertyName.equals(other.propertyName))
			return false;
		if (valueRef == null) {
			if (other.valueRef != null)
				return false;
		} else if (!valueRef.equals(other.valueRef))
			return false;
		return true;
	}

}
