package com.synapsense.dal.generic.entities;

import java.util.Date;

import com.synapsense.dto.BinaryData;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.dto.ValueTO;
import com.synapsense.service.impl.dao.to.PropertyTO;

public final class ValueProxy {

	private Object key;
	private boolean collectionFlag;
	private ValueTO value;

	public ValueProxy(Object key, String propName, boolean collectionFlag, long timestamp, String valuev,
	        Integer valuei, Long valuel, Double valued, Date valuedt, BinaryData valuebl, Integer valueLinkId,
	        String valueLinkType, Integer valuePLinkId, String valuePLinkType, String valuePLinkProperty) {

		this.key = key;
		this.collectionFlag = collectionFlag;

		Object nativeValue = getNotNullValue(valuev, valuei, valuel, valued, valuedt, valuebl, valueLinkId,
		        valueLinkType, valuePLinkId, valuePLinkType, valuePLinkProperty);

		this.value = new ValueTO(propName, nativeValue, timestamp);
	}

	public ValueProxy(String propName, Date timestamp, String valuev, Integer valuei, Long valuel, Double valued) {
		this(null, propName, false, timestamp.getTime(), valuev, valuei, valuel, valued, null, null, null, null, null,
		        null, null);
	}
	
	public ValueProxy(String propName, Date timestamp, BinaryData valuebl) {
		this(null, propName, false, timestamp.getTime(), null, null, null, null, null, valuebl, null, null, null,
		        null, null);
	}

	public ValueTO getProxy() {
		return value;
	}

	public Object getProxyKey() {
		return this.key;
	}

	public boolean isCollection() {
		return collectionFlag;
	}

	private Object getNotNullValue(String valuev, Integer valuei, Long valuel, Double valued, Date valuedt,
	        BinaryData valuebl, Integer valueLinkId, String valueLinkType, Integer valuePLinkId, String valuePLinkType,
	        String valuePLinkProperty) {
		if (valuev != null) {
			return valuev;
		}

		if (valuei != null) {
			return valuei;
		}

		if (valuel != null) {
			return valuel;
		}

		if (valued != null) {
			return valued;
		}

		if (valuedt != null) {
			return valuedt;
		}

		if (valuebl != null) {
			return valuebl;
		}

		if (valueLinkId != null) {
			return TOFactory.getInstance().loadTO(valueLinkType + ":" + valueLinkId);
		}

		if (valuePLinkId != null) {
			TO<?> to = TOFactory.getInstance().loadTO(valuePLinkType + ":" + valuePLinkId);
			return new PropertyTO(to, valuePLinkProperty);
		}

		return null;
	}
}
