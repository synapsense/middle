package com.synapsense.dal.generic.batch;

import java.util.List;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

import javax.transaction.UserTransaction;

import com.synapsense.commandprocessor.api.Command;
import com.synapsense.commandprocessor.api.CommandEngine;
import com.synapsense.util.executor.Executor;

public class CommandFactory {

	public static Command pollQueue(CommandEngine engine, Executor executor, UserTransaction userTransaction, HashedBlockingQueue queue,
	        ModelUpdater updater, int batchSize, int threadsNumber, Lock lock, Condition condition, long delayWhenEmpty) {
		return new PollQueueCommand(engine, updater, executor, userTransaction, queue, batchSize, threadsNumber, lock, condition,
		        delayWhenEmpty);
	}

	public static Command executeBatchTasks(List<List<SingleUpdateUnit>> sublists, CommandEngine engine,
	        Executor executor, ModelUpdater updater, UserTransaction userTransaction) {
		return new ExecuteBatchTasksCommand(sublists, engine, executor, updater, userTransaction);
	}

}
