package com.synapsense.dal.generic.batch;

public final class AttributeTuple {
	private int attrId;
	private boolean historical;
	private Class<?> type;

	public AttributeTuple(int attrId, boolean historical, String type) {
		this.attrId = attrId;
		this.historical = historical;
		try {
			this.type = Class.forName(type);
		} catch (ClassNotFoundException e) {
			throw new IllegalArgumentException("Cannot instantiate new attribute tuple { attr_id_fk = " + attrId
			        + "} of type {" + type + "} will be excluded from mappining"
			        + "Most probable issue is an error in seed db script.Check supported value types enumeration", e);
		}
	}

	public int getAttrId() {
		return attrId;
	}

	public boolean isHistorical() {
		return historical;
	}

	public Class<?> getType() {
		return type;
	}

	@Override
	public String toString() {
		return "AttributeTuple [attrId=" + attrId + ", historical=" + historical + ", type=" + type + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + attrId;
		result = prime * result + (historical ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AttributeTuple other = (AttributeTuple) obj;
		if (attrId != other.attrId)
			return false;
		if (historical != other.historical)
			return false;
		return true;
	}

}