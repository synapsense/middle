package com.synapsense.dal.generic.utils;

import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.apache.log4j.Logger;

/**
 * Simple JPQL queries factory.<br>
 * Used to create {@link Query} instance and to choose right fetch strategy for
 * it relying upon given parameters.
 * 
 * @author shabanov
 * 
 */
public class QueryFactory {
	/**
	 * logger
	 */
	private final static Logger logger = Logger.getLogger(QueryFactory.class);
	/**
	 * Query factory instance
	 */
	private static QueryFactory instance = new QueryFactory();

	/**
	 * private default constructor
	 */
	private QueryFactory() {
		// singleton
	}

	/**
	 * Singleton accessor
	 * 
	 * @return Query factory instance
	 */
	public static QueryFactory getInstance() {
		return instance;
	}

	public Query createSingleParamBatchQuery(EntityManager em, final String namedQuery, final String paramName,
	        final Collection<? extends Comparable<?>> paramValue, Map<String, Object> additionalParams,
	        final String namedQueryParamValueCount, Map<String, Object> paramsForCountQuery,
	        final String namedQueryAll, Map<String, Object> paramsForAllQuery) {
		if (logger.isDebugEnabled()) {
			logger.debug("Constructing batch query for : '" + namedQuery + "'");
		}

		// sorted set - created from input collection of comparable elements
		Set<? extends Comparable<?>> sortedParamValue = new TreeSet<Comparable<?>>(paramValue);

		// if the specified collection is of accepted size (<
		// QueryFactoryConfiguration.DEFAULT_QUERY_BATCH_SIZE)
		// just create query with simple batch strategy
		if (sortedParamValue.size() < QueryFactoryConfiguration.getProperty("com.synapsense.dal.queryBatchSize",
		        Integer.class)) {
			if (logger.isDebugEnabled()) {
				logger.debug("The 'use single IN clause ' batch strategy will be used");
			}

			Query nativeQuery = createQuery(em, namedQuery);

			applyParams(nativeQuery, additionalParams);

			Query resultQuery = new BatchQueryImpl(nativeQuery, new SimpleBatchQueryStrategy(), paramName,
			        sortedParamValue);

			return resultQuery;
		}

		long realNumberOfINJoinee = 0;

		Query countQuery = createQuery(em, namedQueryParamValueCount);
		applyParams(countQuery, paramsForCountQuery);

		realNumberOfINJoinee = (Long) countQuery.getSingleResult();

		// if input collection size = size of related table
		if (realNumberOfINJoinee == sortedParamValue.size()) {
			if (logger.isDebugEnabled()) {
				logger.debug("The 'batch all entities without IN clause' strategy will be used");
			}

			Query allQuery = createQuery(em, namedQueryAll);
			applyParams(allQuery, paramsForAllQuery);

			return allQuery;
		}

		if (logger.isDebugEnabled()) {
			logger.debug("The 'batch entities by pieces' with single piece size : "
			        + QueryFactoryConfiguration.getProperty("com.synapsense.dal.queryBatchSize", Integer.class)
			        + " strategy will be used");
		}

		// if BATCH_SIZE < input collection size < size of related table
		Query nativeQuery = createQuery(em, namedQuery);

		Query resultQuery = new BatchQueryImpl(nativeQuery, new BatchQueryStaticPiecesStrategy(
		        QueryFactoryConfiguration.getProperty("com.synapsense.dal.queryBatchSize", Integer.class)), paramName,
		        sortedParamValue);

		return resultQuery;

	}

	/**
	 * Returns {@link Query} instance in depends on given parameters. Given
	 * <code>paramValue</code> will be sorted and cleaned of duplicates.
	 * 
	 * @param em
	 *            Reference to {@link EntityManager}
	 * @param queryName
	 *            Named query
	 * @param paramName
	 *            Parameter name of <code>queryName</code> which is collection
	 * @param paramValue
	 *            <code>paramName</code>'s value
	 * @param queryParamValueCount
	 *            Named query to check the count of records related to
	 *            <code>paramName</code>. This query should return
	 *            <code>long</code> value.
	 * @param queryAll
	 *            Named query to use if <code>queryParamValueCount</code>'s
	 *            result size = <code>paramValue</code>'s size
	 * 
	 * @return Instance of {@link Query}
	 * 
	 */
	public Query createSingleParamBatchQuery(EntityManager em, final String namedQuery, final String paramName,
	        final Collection<? extends Comparable<?>> paramValue, final String namedQueryParamValueCount,
	        final String namedQueryAll) {
		return createSingleParamBatchQuery(em, namedQuery, paramName, paramValue, namedQueryParamValueCount, null,
		        namedQueryAll, null);
	}

	public Query createSingleParamBatchQuery(EntityManager em, final String namedQuery, final String paramName,
	        final Collection<? extends Comparable<?>> paramValue, final String namedQueryParamValueCount,
	        Map<String, Object> paramsForCountQuery, final String namedQueryAll, Map<String, Object> paramsForAllQuery) {
		return createSingleParamBatchQuery(em, namedQuery, paramName, paramValue, null, namedQueryParamValueCount,
		        paramsForCountQuery, namedQueryAll, paramsForAllQuery);
	}

	private Query createQuery(EntityManager em, String query) {
		Query resultQuery = null;
		resultQuery = em.createNamedQuery(query);
		return resultQuery;
	}

	private void applyParams(Query query, Map<String, Object> params) {
		if (params == null)
			return;
		for (Map.Entry<String, Object> entry : params.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
	}
}