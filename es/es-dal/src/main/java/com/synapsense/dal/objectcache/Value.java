package com.synapsense.dal.objectcache;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

import com.synapsense.exception.UnableToConvertValueException;

/**
 * This is used to store data into our cache
 * 
 * @param <T>
 *            value type
 * @author aborisov
 */

public abstract class Value<T> implements Serializable {
	private static final long DEFAULT_TIMESTAMP = 0L;

	protected volatile T data;
	protected long timestamp;

	static Value createDynamicTypedValue(Object value) {
		if (value == null)
			return createNullValue(Object.class);
		return createTypedValue(value);
	}

	static Value createNullValue(Class type) {
		if (type == null) {
			throw new IllegalArgumentException("Supplied type is null");
		}

		if (Collection.class.isAssignableFrom(type)) {
			return new UntypedCollectionValue();
		}

		if (String.class.isAssignableFrom(type)) {
			return new TextValue();
		}

		return new UntypedSingleValue();
	}

	static Value createTypedValue(Object value) {
		if (value == null)
			throw new IllegalArgumentException("Supplied value is null. Use createNullValue() instead");

		Value newValue = createNullValue(value.getClass());
		try {
			newValue.setValue(value);
		} catch (ClassCastException e) {
			throw new IllegalStateException("Instance of Value class is not valid", e);
		}
		newValue.setTimestamp(DEFAULT_TIMESTAMP);
		return newValue;
	}

	public abstract T getValue();

	public abstract void setValue(T value) throws ClassCastException;

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof Value))
			return false;

		Value that = (Value) o;

		if (data != null ? !data.equals(that.data) : that.data != null)
			return false;

		return true;
	}

	@Override
	public int hashCode() {
		return data != null ? data.hashCode() : 0;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder(50);
		if (data != null) {
			sb.append(data);
			sb.append("]:[");
			sb.append(data.getClass());
		}

		if (timestamp != 0) {
			sb.append("] timestamp [");
			sb.append(new Date(timestamp));
		}
		sb.append("]");

		return sb.toString();
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	public long getTimestamp() {
		return timestamp;
	}
}
