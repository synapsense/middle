package com.synapsense.dal.generic.utils;

import com.synapsense.dal.generic.dao.GenericDAO;
import com.synapsense.dto.Criterion;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StringType;

import java.util.*;

/**
 * Utility class. Build SQL string for searching objects by criteria. The
 * created query suits only for all_objects_props_values view!
 * 
 * @author shabanov
 * 
 */
public final class FindGenericSQL {
	private static Logger log = Logger.getLogger(GenericDAO.class);

	private static final String AND = "and ";

	private static final String WHERE = "where ";

	private static final String CLOSE_BRACKET = ")";

	private static final String CRLF = Util.CRLF;

	public static Query buildSQL(Session session, Criterion[] propertyValues) {
		StringBuilder sb = new StringBuilder();
		sb.append("select distinct item.object_id  as objectId , item.type_name as typeName ");
		sb.append("from all_objects_props_values as item ");
		sb.append(CRLF);
		sb.append(WHERE);
		sb.append(CRLF);

		Collection<Object> bindedList = new LinkedList<Object>();

		HashMap<String, Criterion> propMap = new LinkedHashMap<String, Criterion>();
		for (Criterion criterion : propertyValues) {
			propMap.put(criterion.getPropertyValue().getPropertyName().toLowerCase(), criterion);
		}

		boolean isFirstPredicate = true;
		// add top level predicate
		if (propMap.get("type") != null) {
			Criterion value = propMap.get("type");
			if (value != null) {
				if (!isFirstPredicate) {
					sb.append(AND);
				} else {
					isFirstPredicate = false;
				}
				addCriterion(sb, "item", "type_name", value);
				sb.append(CRLF);
				bindedList.add(propMap.get("type").getPropertyValue().getValue());

			}
			propMap.remove("type");
		}
		Criterion[] propArray = propMap.values().toArray(new Criterion[propMap.size()]);
		int lastCount = propArray.length;
		// add top level predicate
		int initial = 0;
		if (lastCount >= 1) {
			Criterion tempPropName = propArray[0];
			Object value = tempPropName.getPropertyValue().getValue();
			if (!isFirstPredicate) {
				sb.append(AND);
			} else {
				isFirstPredicate = false;
			}

			addCriterion(sb, "item", "propname", tempPropName);
			bindedList.add(tempPropName.getPropertyValue().getPropertyName());

			sb.append(CRLF);
			sb.append(AND);
			if (value == null) {
				addIsNullCriterion(sb, "item", "val", tempPropName);
			} else {
				addCriterion(sb, "item", "val", tempPropName);
				Object bindValue = convertBindedValue(value);
				bindedList.add(bindValue);
			}

			sb.append(CRLF);
			initial++;
		}

		// add more subselect
		int i = initial;
		for (i = initial; i < lastCount; i++) {
			Criterion criterion = propArray[i];
			Object value = criterion.getPropertyValue().getValue();
			if (!isFirstPredicate) {
				sb.append(AND);
			} else {
				isFirstPredicate = false;
			}

			String addOneCriteria = "object_id in ( SELECT object_id FROM all_objects_props_values where ";
			sb.append(addOneCriteria);
			addCriterion(sb, "all_objects_props_values", "propname", criterion);
			sb.append(AND);

			bindedList.add(criterion.getPropertyValue().getPropertyName());

			if (criterion.getPropertyValue().getValue() == null) {
				addIsNullCriterion(sb, "all_objects_props_values", "val", criterion);
			} else {
				addCriterion(sb, "all_objects_props_values", "val", criterion);
				Object bindValue = convertBindedValue(value);
				bindedList.add(bindValue);
			}

			sb.append(CRLF);
		}
		// close brackets
		for (int k = 0; k < i - 1; k++) {
			sb.append(CLOSE_BRACKET);
			sb.append(CRLF);
		}

		SQLQuery generatedQuery = session.createSQLQuery(sb.toString()).addScalar("objectId", IntegerType.INSTANCE)
		        .addScalar("typeName", StringType.INSTANCE);
		Iterator<Object> iterator = bindedList.iterator();
		int j = 0;
		while (iterator.hasNext()) {
			generatedQuery.setParameter(j, iterator.next());
			j++;
		}

		if (log.isDebugEnabled()) {
			log.debug("Genenric objects searching query :" + sb.toString());
		}

		return generatedQuery;
	}

	private static Object convertBindedValue(Object value) {
		if (value == null)
			return null;
		return value.toString();
	}

	private static void addCriterion(StringBuilder sb, String tableAlias, String fieldAlias, Criterion criterion) {
		sb.append(tableAlias + "." + fieldAlias + " " + criterion.getOperation() + " ? ");
	}

	private static void addIsNullCriterion(StringBuilder sb, String tableAlias, String fieldAlias, Criterion criterion) {
		sb.append(tableAlias + "." + fieldAlias + " IS NULL");
	}

}
