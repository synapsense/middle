package com.synapsense.dal.generic.entities;

// Generated 17.03.2008 17:58:20 by Hibernate Tools 3.2.0.CR1

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class ObjectTypeDO implements java.io.Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -316258876885659165L;

	private Integer id;
	private String name;
	private Integer generic;
	private Map<String, AttributeDO> attributes = new HashMap<String, AttributeDO>(0);
	private Set<ObjectDO> objects = new HashSet<ObjectDO>(0);

	public ObjectTypeDO() {
	}

	public ObjectTypeDO(String name) {
		this.name = name;
		this.generic = 1;
	}

	public ObjectTypeDO(String name, Map<String, AttributeDO> attributes) {
		this.name = name;
		this.attributes = attributes;
	}

	@Override
	public String toString() {
		return "TYPE[" + this.name + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final ObjectTypeDO other = (ObjectTypeDO) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	public Integer getId() {
		return this.id;
	}
	 
	// required by hibernate even if private
	@SuppressWarnings("unused")
	private void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Map<String, AttributeDO> getAttributes() {
		return this.attributes;
	}

	public void setAttributes(Map<String, AttributeDO> attributes) {
		this.attributes = attributes;
	}

	public Integer getGeneric() {
		return generic;
	}

	public void setGeneric(Integer generic) {
		this.generic = generic;
	}

	public Set<ObjectDO> getObjects() {
		return objects;
	}

	public void setObjects(Set<ObjectDO> objects) {
		this.objects = objects;
	}

}
