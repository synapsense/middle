package com.synapsense.dal.generic;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;

/**
 * DAO level exception. Thrown when client expect single value result from
 * method which has such exception in its contract but the result consists of
 * more than one value. Stores references to entities.
 * 
 * @author shabanov
 */
public class DAOTooManyEntitiesFoundException extends Exception {

	/**
	 * calling method result entities
	 */
	private Serializable[] entities;

	public DAOTooManyEntitiesFoundException(Collection<? extends Serializable> entities, Throwable cause) {
		super("Too many entites:" + entities.toString() + " , but must be only one ", cause);
		this.entities = entities.toArray(new Serializable[entities.size()]);
	}

	public DAOTooManyEntitiesFoundException(Collection<? extends Serializable> entities) {
		this(entities, null);
	}

	public String getEntitiesInfo() {
		return Arrays.toString(entities);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -7589651084010821710L;

}
