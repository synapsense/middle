package com.synapsense.dal.objectcache;

/**
 * Untyped value
 */
class UntypedSingleValue extends Value<Object> {

    private static final long serialVersionUID = 8597863317825050945L;

    UntypedSingleValue() {
    }

    @Override
    public Object getValue() {
        return data;
    }

    @Override
    public void setValue(Object value) throws ClassCastException {
        if (value == null) {
            data = null;
            return;
        }

        value.getClass().cast(data);
        data = value;
    }

}