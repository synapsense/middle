package com.synapsense.dal.generic.dao;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.LinkedList;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.*;

import org.apache.log4j.Logger;

import com.synapsense.dal.generic.RuleInstanceDAO;
import com.synapsense.dal.generic.entities.AttributeDO;
import com.synapsense.dal.generic.entities.ObjectDO;
import com.synapsense.dal.generic.entities.RuleDO;
import com.synapsense.dal.generic.entities.RuleObjectSrcPropertyDO;
import com.synapsense.dal.generic.entities.RuleObjectSrcPropertyId;
import com.synapsense.dal.generic.entities.RuleTypeDO;
import com.synapsense.dal.generic.utils.Util;
import com.synapsense.dto.CRERule;
import com.synapsense.dto.CRERule.PropertyBinding;
import com.synapsense.dto.RuleType;
import com.synapsense.dto.TO;
import com.synapsense.exception.ObjectExistsException;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.service.impl.exception.DALSystemException;

@Stateless
@Remote(RuleInstanceDAO.class)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
@TransactionManagement(TransactionManagementType.CONTAINER)
public class RuleObjectDAO implements RuleInstanceDAO<Integer> {

	private final static Logger log = Logger.getLogger(RuleObjectDAO.class);

	@PersistenceContext(unitName = "HibernateDAL")
	public void setEM(EntityManager em) {
		this.em = em;
	}

	private EntityManager em;

	@Override
	public TO<Integer> create(CRERule rule) throws ObjectExistsException, ObjectNotFoundException {
		RuleTypeDO ruleTypeDO = findRuleTypeByTo(rule.getRuleType());
		Util.assertNotNullEntity("No such RULE_TYPE with to [" + rule.getRuleType() + "]", ruleTypeDO);

		ObjectDO object = Util.getEntityById(em, ObjectDO.class, rule.getDestObjId().getID());
		Util.assertNotNullEntity("Object " + rule.getDestObjId() + " not found", object);

		AttributeDO attribute = object.getObjectType().getAttributes().get(rule.getDestPropertyName());
		Util.assertNotNullEntity("Attribute " + rule.getDestPropertyName() + " not found", attribute);

		RuleDO ruleDO = new RuleDO(rule.getName(), ruleTypeDO, object, attribute);
		try {
			object.getRules().add(ruleDO);
			ruleTypeDO.getRuleInstances().add(ruleDO);
			em.flush();
		} catch (PersistenceException e) {
			throw new ObjectExistsException("RULE : " + ruleDO.toString() + " already exists");
		}

		Collection<PropertyBinding> srcProps = rule.getSrcProperties();
		for (PropertyBinding pB : srcProps) {
			ObjectDO srcObj = Util.getEntityById(em, ObjectDO.class, pB.getObjId().getID());
			Util.assertNotNullEntity("Source property object " + pB.getObjId() + " not found", srcObj);

			AttributeDO srcAttr = srcObj.getObjectType().getAttributes().get(pB.getPropName());
			Util.assertNotNullEntity("Attribute " + pB.getPropName() + " not found", srcAttr);

			RuleObjectSrcPropertyDO ruleSrcProp = new RuleObjectSrcPropertyDO(new RuleObjectSrcPropertyId(
			        ruleDO.getRuleId(), srcAttr.getAttributeId(), srcObj.getObjectId()));
			ruleSrcProp.setBinding(pB.getPropBinding());

			ruleDO.getRuleSrcProperties().add(ruleSrcProp);
			srcObj.getRuleSrcProperties().add(ruleSrcProp);
		}

		return ruleDO.getIdentity();
	}

	@Override
	public TO<?> getRuleType(Integer id) throws ObjectNotFoundException {
		RuleDO ruleDO = findRuleById(id);
		Util.assertNotNullEntity("Rule with id : " + id.toString() + " does not exist", ruleDO);
		return ruleDO.getRuleType().getIdentity();
	}

	@Override
	public RuleType getRuleTypeDTO(Integer id) throws ObjectNotFoundException {
		RuleDO ruleDO = findRuleById(id);
		Util.assertNotNullEntity("Rule with id : " + id.toString() + " does not exist", ruleDO);
		return ruleDO.getRuleType().getDTO();
	}

	@Override
	public String getName(Integer id) throws ObjectNotFoundException {
		RuleDO ruleDO = findRuleById(id);
		Util.assertNotNullEntity("Rule with id : " + id.toString() + " does not exist", ruleDO);
		return ruleDO.getName();
	}

	@Override
	public void setName(Integer id, String name) throws ObjectNotFoundException, ObjectExistsException {
		RuleDO ruleDO = findRuleById(id);
		Util.assertNotNullEntity("Rule with id : " + id.toString() + " does not exist", ruleDO);
		RuleDO ruleByNameDO = findRuleByName(name);
		Util.assertNotNullEntity("Rule with name : " + name + " does not exist", ruleByNameDO);
		ruleDO.setName(name);
	}

	@Override
	public TO<Integer> getRuleInstanceByName(String name) {
		RuleDO ruleDO = findRuleByName(name);
		return ruleDO == null ? null : ruleDO.getIdentity();
	}

	@Override
	public Collection<TO<Integer>> getRuleInstancesByType(String ruleTypeName) {
		Collection<TO<Integer>> result = new LinkedList<TO<Integer>>();

		RuleTypeDO ruleTypeDO = findRuleTypeByName(ruleTypeName);
		if (ruleTypeDO == null) {
			if (log.isDebugEnabled()) {
				log.debug("Trying to get Rules of non-existing Rule type[" + ruleTypeName + "]");
			}
			return result;
		}
		for (RuleDO rule : ruleTypeDO.getRuleInstances()) {
			result.add(rule.getIdentity());
		}
		return result;
	}

	@Override
	public Collection<TO<Integer>> getAllRuleInstances() {
		Collection<TO<Integer>> result = new LinkedList<TO<Integer>>();
		Collection<RuleDO> ruleDOs = Util.getAllEntities(em, RuleDO.class);
		for (RuleDO rule : ruleDOs) {
			result.add(rule.getIdentity());
		}
		return result;
	}

	@Override
	public boolean delete(Integer id) {
		RuleDO ruleDO = findRuleById(id);
		if (ruleDO == null) {
			if (log.isDebugEnabled()) {
				log.debug("RULE with ID [" + id + "] does not exist");
			}
			return false;
		}
		RuleTypeDO ruleTypeDO = ruleDO.getRuleType();
		ruleTypeDO.getRuleInstances().remove(ruleDO);
		em.remove(ruleDO);
		return true;
	}

	@Override
	public void deleteAll() {
		Util.deleteAllEntities(em, RuleDO.class);
	}

	@Override
	public Collection<CRERule> getRuleDTOsByHostObject(TO<?> hostTO) {
		return getDTOQueryResults(em.createNamedQuery("RuleDO.by.objectId").setParameter("objectId",
		        (Integer) hostTO.getID()));
	}

	@Override
	public CRERule getRuleDTOById(Integer ruleId) {
		RuleDO ruleDO = findRuleById(ruleId);
		if (ruleDO == null) {
			return null;
		}
		return convertDOtoDTO(ruleDO);
	}

	@Override
	public Collection<CRERule> getRulesDTOByType(String ruleTypeName) {
		return getDTOQueryResults(em.createNamedQuery("RuleDO.by.ruleTypeName").setParameter("name", ruleTypeName));
	}

	private CRERule convertDOtoDTO(RuleDO ruleDO) {
		CRERule rule = new CRERule(ruleDO.getName(), ruleDO.getRuleType().getIdentity(), ruleDO.getObject()
		        .getIdentity(), ruleDO.getAttribute().getAttributeName());
		Collection<RuleObjectSrcPropertyDO> srcProps = ruleDO.getRuleSrcProperties();
		for (RuleObjectSrcPropertyDO srcProp : srcProps) {
			rule.addSrcProperty(srcProp.getObject().getIdentity(), srcProp.getAttribute().getAttributeName(),
			        srcProp.getBinding());
		}
		return rule;
	}

	private RuleDO findRuleByName(String ruleName) {
		Collection<RuleDO> ruleDOs = Util.getEntityByName(em, RuleDO.class, ruleName);
		if (ruleDOs.size() == 0) {
			return null;
		}
		if (ruleDOs.size() > 1) {
			throw new DALSystemException("DB is in inconsistent state.RULE [" + ruleName + "] is duplicated");
		}
		return ruleDOs.iterator().next();
	}

	private RuleDO findRuleById(Integer id) {
		return Util.getEntityById(em, RuleDO.class, id);
	}

	private RuleTypeDO findRuleTypeByName(String ruleTypeName) {
		Collection<RuleTypeDO> ruleTypeDOs = Util.getEntityByName(em, RuleTypeDO.class, ruleTypeName);
		if (ruleTypeDOs.size() == 0) {
			return null;
		}
		if (ruleTypeDOs.size() > 1) {
			throw new DALSystemException("DB is in inconsistent state.RULE_TYPE [" + ruleTypeName + "] is duplicated");
		}
		return ruleTypeDOs.iterator().next();
	}

	private RuleTypeDO findRuleTypeByTo(TO<?> ruleTypeTo) {
		return Util.getEntityById(em, RuleTypeDO.class, ruleTypeTo.getID());
	}

	private Collection<CRERule> getDTOQueryResults(Query query) {
		LinkedList<CRERule> result = new LinkedList<CRERule>();
		@SuppressWarnings("unchecked")
		Collection<RuleDO> rulesDo = new LinkedHashSet<RuleDO>(query.getResultList());
		for (RuleDO ruleDO : rulesDo) {
			result.add(convertDOtoDTO(ruleDO));
		}
		return result;
	}

	@Override
	public Collection<TO<Integer>> getRulesByHostObject(TO<?> hostTO) {
		@SuppressWarnings("unchecked")
		Collection<RuleDO> rulesDO = em.createNamedQuery("RuleDO.by.objectId").setParameter("objectId", hostTO.getID())
		        .getResultList();

		Collection<TO<Integer>> tos = new LinkedList<TO<Integer>>();
		for (RuleDO ruleDo : rulesDO) {
			tos.add(ruleDo.getIdentity());
		}
		return tos;
	}
}
