package com.synapsense.dal.generic.entities;

public class AllObjectsPropsValuesDO implements java.io.Serializable {

	private static final long serialVersionUID = 2989771726228702506L;

	private AllObjectsPropsValuesId id;

	private String propname;
	private String objectName;
	private String typeName;
	private Integer valId;
	private int objectId;
	private int typeId;
	private String val;

	public AllObjectsPropsValuesDO() {
	}

	public AllObjectsPropsValuesId getId() {
		return this.id;
	}

	public String getPropname() {
		return propname;
	}

	public String getObjectName() {
		return objectName;
	}

	public String getTypeName() {
		return typeName;
	}

	public Integer getValId() {
		return valId;
	}

	public int getObjectId() {
		return objectId;
	}

	public int getTypeId() {
		return typeId;
	}

	public String getVal() {
		return val;
	}
}
