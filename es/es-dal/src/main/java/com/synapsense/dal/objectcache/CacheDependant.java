package com.synapsense.dal.objectcache;

public interface CacheDependant {
	void setCache(CacheSvc cache);
}
