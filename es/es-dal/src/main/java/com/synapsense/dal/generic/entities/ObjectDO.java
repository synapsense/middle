package com.synapsense.dal.generic.entities;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.synapsense.dal.generic.dao.Identifiable;
import com.synapsense.dto.TO;
import com.synapsense.service.impl.dao.to.GenericObjectTO;

public class ObjectDO implements Identifiable<Integer>, java.io.Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6423046380519454465L;
	/**
	 * PK
	 */
	private Integer objectId;

	/**
	 * Determines that object is fake and serves only for storing static props
	 * of its object type
	 */
	private boolean staticFlag;

	/**
	 * Object name
	 */
	private String name;
	/**
	 * Object description
	 */
	private String description;
	/**
	 * Type of object
	 */
	private ObjectTypeDO objectType;

	/**
	 * Prop. values of object
	 */
	private Map<AttributeDO, ValueDO> attributesValues = new HashMap<AttributeDO, ValueDO>(0);
	/**
	 * Rules that are assigned with object
	 */
	private Set<RuleDO> rules = new HashSet<RuleDO>(0);
	/**
	 * Rule sources are assigned with object
	 */
	private Set<RuleObjectSrcPropertyDO> ruleSrcProperties = new HashSet<RuleObjectSrcPropertyDO>(0);
	/**
	 * All object ancestors set
	 */
	// private Set<ObjectDO> objectAncestors = new HashSet<ObjectDO>(0);
	/**
	 * All object children set
	 */
	// private Set<ObjectDO> objectObjects = new HashSet<ObjectDO>(0);
	/**
	 * 1 level ancestors
	 */
	private Set<ObjectDO> parents = new HashSet<ObjectDO>(0);

	/**
	 * 1 level children
	 */
	private Set<ObjectDO> childs = new HashSet<ObjectDO>(0);

	/**
	 * Tag values which belong to object
	 */
	private Set<TagValueDO> tagValues = new HashSet<TagValueDO>(0);

	public ObjectDO() {
	}

	public ObjectDO(final String name, final ObjectTypeDO objectType, final boolean isStatic) {
		this.name = name;
		this.objectType = objectType;
		staticFlag = isStatic;
	}

	public Integer getObjectId() {
		return objectId;
	}

	@SuppressWarnings("unused")
	private void setObjectId(final Integer objectId) {
		this.objectId = objectId;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

	public ObjectTypeDO getObjectType() {
		return objectType;
	}

	public void setObjectType(final ObjectTypeDO objectType) {
		this.objectType = objectType;
	}

	public Map<AttributeDO, ValueDO> getAttributesValues() {
		return attributesValues;
	}

	public void setAttributesValues(final Map<AttributeDO, ValueDO> attributesValues) {
		this.attributesValues = attributesValues;
	}

	public Set<RuleDO> getRules() {
		return rules;
	}

	public void setRules(final Set<RuleDO> rules) {
		this.rules = rules;
	}

	public Set<TagValueDO> getTagValues() {
		return tagValues;
	}

	public void setTagValues(final Set<TagValueDO> tagValues) {
		this.tagValues = tagValues;
	}

	public Set<RuleObjectSrcPropertyDO> getRuleSrcProperties() {
		return ruleSrcProperties;
	}

	public void setRuleSrcProperties(final Set<RuleObjectSrcPropertyDO> ruleSrcProperties) {
		this.ruleSrcProperties = ruleSrcProperties;
	}

	public Set<ObjectDO> getParents() {
		return parents;
	}

	public void setParents(final Set<ObjectDO> parents) {
		this.parents = parents;
	}

	public Set<ObjectDO> getChilds() {
		return childs;
	}

	public void setChilds(final Set<ObjectDO> childs) {
		this.childs = childs;
	}

	@Override
	public TO<Integer> getIdentity() {
		return new GenericObjectTO(objectId, objectType.getName());
	}

	@Override
	public String toString() {
		return "[" + objectType.getName() + ":" + this.getObjectId() + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((objectId == null) ? 0 : objectId.hashCode());
		result = prime * result + ((objectType == null) ? 0 : objectType.hashCode());
		result = prime * result + (staticFlag ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final ObjectDO other = (ObjectDO) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (objectId == null) {
			if (other.objectId != null)
				return false;
		} else if (!objectId.equals(other.objectId))
			return false;
		if (objectType == null) {
			if (other.objectType != null)
				return false;
		} else if (!objectType.equals(other.objectType))
			return false;
		if (staticFlag != other.staticFlag)
			return false;
		return true;
	}

	public boolean isStaticFlag() {
		return staticFlag;
	}

	public void setStaticFlag(final boolean staticFlag) {
		this.staticFlag = staticFlag;
	}

}
