package com.synapsense.dal.generic;


public interface DTOSetter<DTO_TYPE> {
	/**
	 * Merges entity by the specified dto (updates state form dto)
	 * 
	 * @param dto
	 */
	void setDTO(DTO_TYPE dto);
}
