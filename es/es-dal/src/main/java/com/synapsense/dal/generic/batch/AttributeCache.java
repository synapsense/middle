package com.synapsense.dal.generic.batch;

import com.synapsense.util.Pair;

class AttributeCache extends BaseCache<Pair<String, String>, AttributeTuple> {

	@Override
	public boolean isHistorical(Pair<String, String> key) {
		AttributeTuple tuple = getContainer().get(key);
		if (tuple == null)
			return false;
		return tuple.isHistorical();
	}
}