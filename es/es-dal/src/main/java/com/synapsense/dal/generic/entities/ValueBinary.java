package com.synapsense.dal.generic.entities;

import com.synapsense.dto.BinaryData;

public class ValueBinary extends AbstractValue<BinaryData> implements java.io.Serializable {
	private static final long serialVersionUID = 6289971102774563411L;

	private Integer id;
	private ValueDO valueRef;
	private BinaryData value;

	public ValueBinary() {
	}

	public ValueBinary(ValueDO valueRef) {
		this.valueRef = valueRef;
	}

	public ValueBinary(ValueDO valueRef, Object value) {
		this.valueRef = valueRef;
		this.value = (BinaryData) value;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public ValueDO getValueRef() {
		return this.valueRef;
	}

	public void setValueRef(ValueDO valueRef) {
		this.valueRef = valueRef;
	}

	public BinaryData getValue() {
		return this.value;
	}

	public void setValue(BinaryData value) {
		this.value = value;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		result = prime * result
				+ ((valueRef == null) ? 0 : valueRef.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ValueBinary other = (ValueBinary) obj;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		if (valueRef == null) {
			if (other.valueRef != null)
				return false;
		} else if (!valueRef.equals(other.valueRef))
			return false;
		return true;
	}
}
