package com.synapsense.dal.generic.entities;

import java.util.Arrays;
import java.util.Date;

public class NhtInfoDO implements java.io.Serializable {

	private static final long serialVersionUID = 3154216971133904048L;

	private Integer id;
	private Integer nodeId;
    private String nodeIdHex;
	private Integer numNeigh;
	private Date reportTimestamp;
	private String neighIdHex;
	private String rssi;
	private Integer state;
	private Integer hop;
	private Integer hoptm;

	public NhtInfoDO() {
	}

    public NhtInfoDO(Integer id, Integer nodeId, String nodeIdHex, Integer numNeigh, Date reportTimestamp, String neighIdHex, String rssi, Integer state, Integer hop, Integer hoptm) {
        this.id = id;
        this.nodeId = nodeId;
        this.nodeIdHex = nodeIdHex;
        this.numNeigh = numNeigh;
        this.reportTimestamp = reportTimestamp;
        this.neighIdHex = neighIdHex;
        this.rssi = rssi;
        this.state = state;
        this.hop = hop;
        this.hoptm = hoptm;
    }

    public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getNumNeigh() {
		return this.numNeigh;
	}

	public void setNumNeigh(Integer numNeigh) {
		this.numNeigh = numNeigh;
	}

	public Date getReportTimestamp() {
		return this.reportTimestamp;
	}

	public void setReportTimestamp(Date reportTimestamp) {
		this.reportTimestamp = reportTimestamp;
	}

    public Integer getNodeId() {
        return nodeId;
    }

    public void setNodeId(Integer nodeId) {
        this.nodeId = nodeId;
    }

    public String getNodeIdHex() {
        return nodeIdHex;
    }

    public void setNodeIdHex(String nodeIdHex) {
        this.nodeIdHex = nodeIdHex;
    }

    public String getNeighIdHex() {
        return neighIdHex;
    }

    public void setNeighIdHex(String neighIdHex) {
        this.neighIdHex = neighIdHex;
    }

    public String getRssi() {
        return rssi;
    }

    public void setRssi(String rssi) {
        this.rssi = rssi;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Integer getHop() {
        return hop;
    }

    public void setHop(Integer hop) {
        this.hop = hop;
    }

    public Integer getHoptm() {
        return hoptm;
    }

    public void setHoptm(Integer hoptm) {
        this.hoptm = hoptm;
    }
}