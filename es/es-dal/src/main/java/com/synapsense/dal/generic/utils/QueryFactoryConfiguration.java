package com.synapsense.dal.generic.utils;

import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;

/**
 * Query factory parameters.
 * 
 * @author shabanov
 * 
 */
public final class QueryFactoryConfiguration {

	private final static Logger logger = Logger.getLogger(QueryFactoryConfiguration.class);

	private static Properties configurableProperties;
	static {
		// puts default values for all configurable properties
		configurableProperties = new Properties();
		configurableProperties.put("com.synapsense.dal.queryBatchSize", 2000);
	}

	public synchronized static void configure(final Properties properties) {
		String batchSizeProperty = (String) properties.get("com.synapsense.dal.queryBatchSize");
		if (batchSizeProperty != null) {
			try {
				int batchSize = Integer.parseInt(batchSizeProperty);
				if (batchSize > 1) {
					configurableProperties.put("com.synapsense.dal.queryBatchSize", batchSize);
					logger.info("Parameter com.synapsense.dal.queryBatchSize is set to : " + batchSize);
				}
			} catch (NumberFormatException e) {
			}
		}
	}

	public static <T> T getProperty(String propertyName, Class<T> clazz) {
		return clazz.cast(configurableProperties.get(propertyName));
	}
}
