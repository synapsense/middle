package com.synapsense.dal.generic;

import java.io.Serializable;

/**
 * Entity association fetch modes. Defines all supported fetch modes - the ways
 * of entity association fetching.
 * 
 * @author shabanov
 * 
 */
public final class DAOFetchMode implements Serializable {

	private static final long serialVersionUID = 72726662815736808L;

	/**
	 * Fetch mode name
	 */
	private final String name;

	/**
	 * fetch mode name
	 * 
	 * @param name
	 */
	private DAOFetchMode(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return name;
	}

	/**
	 * Default the setting configured in the mappings.
	 */
	public static final DAOFetchMode DEFAULT = new DAOFetchMode("DEFAULT");

	/**
	 * Fetch using an outer join.
	 */
	public static final DAOFetchMode JOIN = new DAOFetchMode("JOIN");
	/**
	 * Fetch eagerly, using a separate select
	 */
	public static final DAOFetchMode SELECT = new DAOFetchMode("SELECT");

	/**
	 * Mode name
	 * 
	 * @return
	 */
	public String getModeName() {
		return name;
	}

}
