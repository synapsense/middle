package com.synapsense.dal.generic;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * DAO invocation context. When you invoke DAO methods which support some sort
 * of context you can instantiate this context and get it to the DAO method. The
 * context defines additional behavior to the DAO method. Lazy association
 * disabling , max query result restriction , query timeout restriction - this
 * is not a full list of what can be configured before DAO calling. It will
 * "grow up" as fast as DAO functional will be extended.
 * 
 * @author shabanov
 * 
 */
public final class DAOInvocationContext implements Serializable {

	private static final long serialVersionUID = -5905519971459278008L;

	/**
	 * additional comment
	 */
	private String comment;

	/**
	 * how many items must be taken
	 */
	private int fetchSize;

	/**
	 * first item index
	 */
	private int firstResult;

	/**
	 * max amount of items in result
	 */
	private int maxResults;

	/**
	 * timeout restriction , method execute time limit
	 */
	private int timeout;

	/**
	 * Association to fetch mode map. Stores association name (property name of
	 * entity) and fetch mode for it.
	 * 
	 */
	private Map<String, DAOFetchMode> assocFetchMap = new HashMap<String, DAOFetchMode>(0);

	/**
	 * Excluded properties set. The properties which will be excluded from query
	 * criteria.
	 * 
	 */
	private final Set<String> excludedProperties = new HashSet<String>(0);

	/**
	 * Exclude zero field from query criteria
	 */
	private boolean excludeZeroes = true;

	/**
	 * Unexpected null input parameter message
	 */
	private static final String NULL_INPUT_PARAMETER_MESSAGE = "Supplied argument {%s} cannot be null or empty";

	/**
	 * Default invocation context.
	 */
	public final static DAOInvocationContext DEFAULT = createContext("default_comment");

	/**
	 * Default fetch size constant
	 */
	private final static int DEFAULT_FETCH_SIZE = 0;

	/**
	 * Default fisrt result constant
	 */
	private static final int DEFAULT_FIRST_RESULT = 0;

	/**
	 * Default max result constant
	 */
	private static final int DEFAULT_MAX_RESULT = 0;

	/**
	 * Default timeout constant
	 */
	private static final int DEFAULT_TIMEOUT = 0;

	/**
	 * Factory method - creates <code>DAOInvocationContext</code> instance
	 * 
	 * @param comment
	 *            Comment
	 * @param fetchSize
	 *            required fetch size
	 * @param firstResult
	 *            first result value
	 * @param maxResults
	 *            max result value
	 * @param timeout
	 *            required timeout
	 * @return
	 */
	public static DAOInvocationContext createContext(final String comment, final int fetchSize, final int firstResult,
	        final int maxResults, final int timeout) {
		return new DAOInvocationContext(comment, fetchSize, firstResult, maxResults, timeout);
	}

	/**
	 * Factory method - creates <code>DAOInvocationContext</code> instance with
	 * default parameters
	 * 
	 * @param comment
	 * @return
	 */
	public static DAOInvocationContext createContext(final String comment) {
		return createContext(comment, DEFAULT_FETCH_SIZE, DEFAULT_FIRST_RESULT, DEFAULT_MAX_RESULT, DEFAULT_TIMEOUT);
	}

	public static DAOInvocationContext createContext(final String comment, final String... join) {
		final DAOInvocationContext ctx = createContext(comment, DEFAULT_FETCH_SIZE, DEFAULT_FIRST_RESULT,
		        DEFAULT_MAX_RESULT, DEFAULT_TIMEOUT);
		for (final String j : join) {
			ctx.addFetchModeToAssociation(j, DAOFetchMode.JOIN);
		}

		return ctx;
	}

	public static DAOInvocationContext createContext(final String comment, final int fetchSize, final int firstResult,
	        final int maxResults, final int timeout, final Map<String, DAOFetchMode> assocFetchMap) {
		if (assocFetchMap == null) {
			throw new IllegalArgumentException(String.format(NULL_INPUT_PARAMETER_MESSAGE, "assocFetchMap"));
		}
		final DAOInvocationContext newContext = createContext(comment, fetchSize, firstResult, maxResults, timeout);
		newContext.setFetchModeOfAssoc(assocFetchMap);

		return newContext;
	}

	private DAOInvocationContext(final String comment, final int fetchSize, final int firstResult,
	        final int maxResults, final int timeout) {

		if (comment == null) {
			throw new IllegalArgumentException(String.format(NULL_INPUT_PARAMETER_MESSAGE, "comment"));
		}
		this.comment = comment;

		if (fetchSize < 0) {
			throw new IllegalArgumentException(String.format(NULL_INPUT_PARAMETER_MESSAGE, "fetchSize"));
		}
		this.fetchSize = fetchSize;

		if (firstResult < 0) {
			throw new IllegalArgumentException(String.format(NULL_INPUT_PARAMETER_MESSAGE, "firstResult"));
		}
		this.firstResult = firstResult;

		if (maxResults < 0) {
			throw new IllegalArgumentException(String.format(NULL_INPUT_PARAMETER_MESSAGE, "maxResults"));
		}
		this.maxResults = maxResults;

		if (timeout < 0) {
			throw new IllegalArgumentException(String.format(NULL_INPUT_PARAMETER_MESSAGE, "timeout"));
		}

		this.timeout = timeout;
	}

	public DAOInvocationContext mergeAssoc(final DAOInvocationContext ctx) {
		for (final Map.Entry<String, DAOFetchMode> entry : ctx.getFetchModeOfAssoc().entrySet()) {
			this.addFetchModeToAssociation(entry.getKey(), entry.getValue());
		}
		return this;
	}

	/**
	 * Fetch mode of the named association.
	 * 
	 * @param assocName
	 * @param fetchMode
	 */
	public void addFetchModeToAssociation(final String assocName, final DAOFetchMode fetchMode) {
		assocFetchMap.put(assocName, fetchMode);
	}

	/**
	 * Fetch modes setting getter
	 * 
	 * @return
	 */
	public Map<String, DAOFetchMode> getFetchModeOfAssoc() {
		return assocFetchMap;
	}

	/**
	 * Fetch modes setting setter
	 */
	public void setFetchModeOfAssoc(final Map<String, DAOFetchMode> fetchModeOfAssoc) {
		assocFetchMap = fetchModeOfAssoc;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(final String comment) {
		this.comment = comment;
	}

	public int getFetchSize() {
		return fetchSize;
	}

	public void setFetchSize(final int fetchSize) {
		this.fetchSize = fetchSize;
	}

	public int getFirstResult() {
		return firstResult;
	}

	public void setFirstResult(final int firstResult) {
		this.firstResult = firstResult;
	}

	public int getMaxResults() {
		return maxResults;
	}

	public void setMaxResults(final int maxResults) {
		this.maxResults = maxResults;
	}

	public int getTimeout() {
		return timeout;
	}

	public void setTimeout(final int timeout) {
		this.timeout = timeout;
	}

	/**
	 * Returns unmodifiable set of properties to exclude
	 * 
	 * @return Set of properties names
	 */
	public Set<String> getExcludedProperties() {
		return Collections.unmodifiableSet(excludedProperties);
	}

	/**
	 * Overrides
	 * 
	 * @param excludedProperties
	 */
	public void setExcludedProperties(final Set<String> excludedProperties) {
		synchronized (this.excludedProperties) {
			this.excludedProperties.clear();
			this.excludedProperties.addAll(excludedProperties);
		}
	}

	/**
	 * Excludes specified property from properties used in 'query by example'
	 * approach.
	 * 
	 * @param propertyName
	 *            Property to exclude
	 */
	public void excludeProperty(final String propertyName) {
		synchronized (excludedProperties) {
			excludedProperties.add(propertyName);
		}
	}

	public void excludeProperty(final String[] propertyNames) {
		synchronized (excludedProperties) {
			excludedProperties.addAll(Arrays.asList(propertyNames));
		}
	}

	public boolean isExcludeZeroes() {
		return excludeZeroes;
	}

	public void setExcludeZeroes(final boolean excludeZeroes) {
		this.excludeZeroes = excludeZeroes;
	}
}