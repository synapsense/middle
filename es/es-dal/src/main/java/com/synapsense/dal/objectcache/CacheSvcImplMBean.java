package com.synapsense.dal.objectcache;

//dummy interface to make original CacheSvcMgmt compatible
//with JMX standard MBean naming convention
public interface CacheSvcImplMBean extends CacheSvcMgmt {

}
