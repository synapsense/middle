package com.synapsense.dal.generic.entities;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Vadim Gusev
 * @since Europa
 *        Date: 06.06.13
 *        Time: 14:15
 */
public class HistoricalBinaryValueId implements Serializable {

	private static final long serialVersionUID = 8860596770945067924L;
	
	private int value;
    private Date timestamp;
    
    HistoricalBinaryValueId() {
	}
    
    public HistoricalBinaryValueId(int value, Date timestamp) {
		this.value = value;
		this.timestamp = timestamp;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((timestamp == null) ? 0 : timestamp.hashCode());
		result = prime * result + value;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final HistoricalBinaryValueId other = (HistoricalBinaryValueId) obj;
		if (timestamp == null) {
			if (other.timestamp != null)
				return false;
		} else if (!timestamp.equals(other.timestamp))
			return false;
		if (value != other.value)
			return false;
		return true;
	}
}
