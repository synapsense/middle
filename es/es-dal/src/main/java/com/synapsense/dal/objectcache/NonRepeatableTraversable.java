package com.synapsense.dal.objectcache;

import java.util.Collection;
import java.util.Collections;
import java.util.Set;

import com.synapsense.search.Element;
import com.synapsense.search.Path;
import com.synapsense.search.Traversable;
import com.synapsense.util.CollectionUtils;

class NonRepeatableTraversable implements Traversable {

	public NonRepeatableTraversable(Traversable traverser) {
		this.traverser = traverser;
	}

	@Override
	public Collection<Element> traverseNext(Element e, Path path) {
		if (visited.contains(e)) {
			return Collections.emptyList();
		}
		visited.add(e);
		return traverser.traverseNext(e, path);
	}

	private Traversable traverser;
	private Set<Element> visited = CollectionUtils.newSet();
}