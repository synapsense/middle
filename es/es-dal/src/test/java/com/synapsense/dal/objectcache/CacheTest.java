package com.synapsense.dal.objectcache;

import com.synapsense.dto.*;
import com.synapsense.service.Environment;
import com.synapsense.service.impl.dao.to.PropertyTO;
import com.synapsense.service.nsvc.NotificationService;
import com.synapsense.util.executor.Executor;
import com.synapsense.util.executor.Executors;
import mockit.Deencapsulation;
import mockit.Mocked;
import mockit.NonStrictExpectations;
import mockit.StrictExpectations;
import mockit.integration.junit4.JMockit;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

import static org.junit.Assert.assertEquals;

@RunWith(JMockit.class)
public class CacheTest {

	static CacheSvcImpl c = new CacheSvcImpl();
	
	@BeforeClass
	public static void mockCdp() {
		Deencapsulation.setField(c, "cdp", new CacheDependant() {
			@Override
			public void setCache(CacheSvc cache) {
			}
		});
	}

	@Test
	public void testStartStopCache(@Mocked SimpleObject so, @Mocked SimpleType st, @Mocked ObjectType ot, @Mocked Executors exec,
								   @Mocked Environment env, @Mocked NotificationService ns, @Mocked Executor dataLoader) throws Exception {

		final Set<PropertyDescr> pds = new HashSet<PropertyDescr>();
		final PropertyDescr pdTO = new PropertyDescr("TO", TO.class);
		final PropertyDescr pdPropTO = new PropertyDescr("PropertyTo", PropertyTO.class);
		pds.add(pdTO);
		pds.add(pdPropTO);

        final TO alert1 = TOFactory.getInstance().loadTO("ALERT", 1);
		final Collection<TO<?>> tos = new LinkedList<TO<?>>();
		tos.add(alert1);
		
		final Collection<SimpleType> sts= new LinkedList<SimpleType>();
		sts.add(st);
		
		final Collection<String> typeNames = new LinkedList<String>();
		typeNames.add("ALERT");

		final Set<Integer> objs = new HashSet<Integer>();
		objs.add(1);
		
		new NonStrictExpectations() {
			{
				Executors.newPooledExecutor("cache service objects loader", 1);returns(dataLoader);

				env.getObjectTypes();returns(new String[] { "ALERT" });

				env.getObjectType("ALERT");returns(ot);
				
				SimpleType.newType(ot, withInstanceOf(IndexProvider.class));returns(st);
				
				ot.getPropertyDescriptors();returns(pds);

				env.getObjectsByType("ALERT");returns(tos);
				
				dataLoader.threadsNum();returns(1);
				
				dataLoader.doAction(withInstanceOf(Collection.class), true);
				
				dataLoader.shutdownAwait();
				
				Executors.newPooledExecutor("cache service relations loader", 1);returns(dataLoader);
				
				SimpleType.enumerateAllTypes();returns(sts);	
				
				dataLoader.doAction(withInstanceOf(Collection.class), true);
				
				SimpleType.enumerateAllTypeNames();times=2;returns(typeNames);
				
				SimpleType.findType("ALERT");returns(st);
				
				st.getAllObjects();returns(objs);
				
				st.getName();returns("ALERT");
				
				SimpleType.enumerateAllTypeNames();returns(typeNames);
				
				SimpleType.findType("ALERT");returns(st);

                TO hostTO = TOFactory.getInstance().loadTO("hostTO", 2);
				st.getValue(1, "hostTO", ValueTO.class);
				returns(new ValueTO("hostTO", hostTO));

				SimpleType.enumerateAllTypeNames();returns(typeNames);

                env.deleteObject(alert1);

				dataLoader.shutdownAwait();
				
				SimpleType.removeAllTypes();
			}
		};

		c.setEnv(env);
		c.setNotificationService(ns);
		c.create();
		assertEquals(0x00, c.getStatus());
		
		/*
		 * try to start already started cache
		 */
		c.start();
		
		c.stop();
		assertEquals(0xFF, c.getStatus());
		/*
		 * try to stop already stopped cache
		 */
		c.stop();
		
		c.destroy();
	}
}
