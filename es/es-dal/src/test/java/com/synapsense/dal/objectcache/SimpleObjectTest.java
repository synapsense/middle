package com.synapsense.dal.objectcache;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.sql.Date;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import mockit.Expectations;

import mockit.Mocked;
import mockit.StrictExpectations;
import mockit.integration.junit4.JMockit;
import org.junit.Test;

import com.synapsense.dto.ObjectType;
import com.synapsense.dto.PropertyDescr;
import com.synapsense.dto.Tag;
import com.synapsense.dto.TagDescriptor;
import com.synapsense.exception.PropertyNotFoundException;
import com.synapsense.exception.TagNotFoundException;
import com.synapsense.exception.UnableToConvertTagException;
import com.synapsense.util.CollectionUtils;
import com.synapsense.util.Pair;
import org.junit.runner.RunWith;

@RunWith(JMockit.class)
public class SimpleObjectTest {

	final PropertyDescr pdName = new PropertyDescr("name", String.class);
	final PropertyDescr pdValue = new PropertyDescr("value", Integer.class);

	@Test
	public void testCreateSimpleObject(@Mocked ObjectType type, @Mocked IndexProvider indexProvider) throws Exception {
		final Set<PropertyDescr> descrs = CollectionUtils.newSet();
		descrs.add(pdName);

		new Expectations() {
			{
				type.getName();
				times = 3;
				returns("TYPE");

				type.getPropertyDescriptors();
				times = 2;
				returns(descrs);

			}
		};
		try {
			SimpleType st = SimpleType.newType(type, indexProvider);
			SimpleObject so = new SimpleObject(st, 1);
			assertEquals((Integer) 1, so.getOid());
			assertEquals(0, so.getChildren().size());
			assertEquals(0, so.getParents().size());
		} finally {
			SimpleType.removeAllTypes();
		}
	}

	@Test
	public void testGetSetPropertyValue(@Mocked ObjectType type, @Mocked IndexProvider indexProvider) throws Exception {
        final Set<PropertyDescr> descriptors = CollectionUtils.newSet();
        descriptors.add(new PropertyDescr("name", String.class));

		new Expectations() {
			{
				type.getName();
				times = 3;
				returns("TYPE");

				type.getPropertyDescriptors();
				times = 2;
				returns(descriptors);

			}
		};
		try {
			SimpleType st = SimpleType.newType(type, indexProvider);
			SimpleObject so = st.addObject(1);
            so.setPropertyValue("name", "test_name", 0L);
			assertEquals("test_name", (String) so.getPropertyValue("name").getValue().getValue());
			assertEquals(0L, so.getPropertyTimestamp("name"));

			so.setPropertyValue("name", "other_name", 1L);
			assertEquals("other_name", (String) so.getPropertyValue("name").getValue().getValue());
			assertEquals(1L, so.getPropertyTimestamp("name"));

			so.removeProperty("name");
			/*
			 * numAlerts property
			 */
			assertEquals(1, so.getAllPropertiesValues().size());
			/*
			 * try to set value to non exist property
			 */

			so.setPropertyValue("name", "other_name", 1L);
			assertEquals(1, so.getAllPropertiesValues().size());
		} finally {
			SimpleType.removeAllTypes();
		}
	}

	@Test(expected = PropertyNotFoundException.class)
	public void testGetPropertyValueOfNonExistProperty(@Mocked ObjectType type, @Mocked IndexProvider indexProvider)
	        throws Exception {
		final Set<PropertyDescr> descrs = CollectionUtils.newSet();
		descrs.add(pdName);

		new Expectations() {
			{
				type.getName();
				times = 3;
				returns("TYPE");

				type.getPropertyDescriptors();
				times = 2;
				returns(descrs);

			}
		};
		try {
			SimpleType st = SimpleType.newType(type, indexProvider);
			SimpleObject so = new SimpleObject(st, 1);
			so.getPropertyValue("name");
		} finally {
			SimpleType.removeAllTypes();
		}
	}

	@Test
	public void testCreateProperty(@Mocked ObjectType type, @Mocked IndexProvider indexProvider) throws Exception {
		final Set<PropertyDescr> descriptors = CollectionUtils.newSet();
		descriptors.add(new PropertyDescr("name", String.class));

		new Expectations() {
			{
				type.getName();
				times = 3;
				returns("TYPE");

				type.getPropertyDescriptors();
				times = 2;
				result = descriptors;
			}
		};

		try {
			SimpleType st = SimpleType.newType(type, indexProvider);
			SimpleObject so = st.addObject(1);
			assertNull(so.getPropertyValue("name").getValue().getValue());
			so.setPropertyValue("name", "test_name", 0L);
			assertEquals("test_name", so.getPropertyValue("name").getValue().getValue());
		} finally {
			SimpleType.removeAllTypes();
		}
	}

	@Test
	public void testGetAllPropertiesValue(@Mocked ObjectType type, @Mocked IndexProvider indexProvider) throws Exception {
		final Set<PropertyDescr> descrs = CollectionUtils.newSet();
		descrs.add(pdName);
		descrs.add(pdValue);

		new Expectations() {
			{
				type.getName();
				times = 3;
				returns("TYPE");

				type.getPropertyDescriptors();
				times = 2;
				returns(descrs);

			}
		};
		try {
			SimpleType st = SimpleType.newType(type, indexProvider);
            SimpleObject so = st.addObject(1);
            so.setPropertyValue("name", "test_name", 0L);
            so.setPropertyValue("value", 100, 0L);
			/*
			 * 2+numAlerts=3
			 */
			assertEquals(3, so.getAllPropertiesValues().size());
			assertTrue(so.getAllPropertiesValues().contains(
			        new SimplePropertyValue("name", Value.createTypedValue("test_name"))));
			assertTrue(so.getAllPropertiesValues().contains(new SimplePropertyValue("value", Value.createTypedValue(100))));
		} finally {
			SimpleType.removeAllTypes();
		}
	}

	@Test
	public void testGetAllTags(@Mocked ObjectType type, @Mocked IndexProvider indexProvider) throws Exception {
		final Set<PropertyDescr> descrs = CollectionUtils.newSet();
		final PropertyDescr namePD = new PropertyDescr("name", String.class);
		final PropertyDescr valuePD = new PropertyDescr("value", Integer.class);
		namePD.getTagDescriptors().add(new TagDescriptor("name", String.class.getName()));
		valuePD.getTagDescriptors().add(new TagDescriptor("value", Integer.class.getName()));
		descrs.add(namePD);
		descrs.add(valuePD);

		final Set<PropertyDescr> descrs_empty = CollectionUtils.newSet();

		new Expectations() {
			{
				type.getName();
				times = 3;
				returns("TYPE");

				type.getPropertyDescriptors();
				times = 2;
				returns(descrs);
			}
		};
		try {
			SimpleType st = SimpleType.newType(type, indexProvider);
            SimpleObject so = st.addObject(1);
			assertEquals(2, so.getAllTags().size());
		} finally {
			SimpleType.removeAllTypes();
		}
	}

	@Test
	public void testGetAllTagsByPropName(@Mocked ObjectType type, @Mocked IndexProvider indexProvider) throws Exception {
		final Set<PropertyDescr> descrs = CollectionUtils.newSet();
		final PropertyDescr namePD = new PropertyDescr("name", String.class);
		final PropertyDescr valuePD = new PropertyDescr("value", Integer.class);
		namePD.getTagDescriptors().add(new TagDescriptor("name", String.class.getName()));
		namePD.getTagDescriptors().add(new TagDescriptor("length", Integer.class.getName()));
		valuePD.getTagDescriptors().add(new TagDescriptor("value", Integer.class.getName()));
		descrs.add(namePD);
		descrs.add(valuePD);

		new Expectations() {
			{
				type.getName();
				times = 3;
				returns("TYPE");

				type.getPropertyDescriptors();
				times = 2;
				returns(descrs);

			}
		};
		try {
			SimpleType st = SimpleType.newType(type, indexProvider);
            SimpleObject so = st.addObject(1);
			/*
			 * name+length=2
			 */
			assertEquals(2, so.getAllTags("name").size());
		} finally {
			SimpleType.removeAllTypes();
		}
	}

	@Test(expected = PropertyNotFoundException.class)
	public void testGetAllTagsByPropNamePropNotFound(@Mocked ObjectType type, @Mocked IndexProvider indexProvider)
	        throws Exception {
		final Set<PropertyDescr> descrs = CollectionUtils.newSet();
		final PropertyDescr namePD = new PropertyDescr("name", String.class);
		final PropertyDescr valuePD = new PropertyDescr("value", Integer.class);
		namePD.getTagDescriptors().add(new TagDescriptor("name", String.class.getName()));
		namePD.getTagDescriptors().add(new TagDescriptor("length", Integer.class.getName()));
		valuePD.getTagDescriptors().add(new TagDescriptor("value", Integer.class.getName()));
		descrs.add(namePD);
		descrs.add(valuePD);

		new Expectations() {
			{
				type.getName();
				times = 3;
				returns("TYPE");

				type.getPropertyDescriptors();
				times = 2;
				returns(descrs);

			}
		};
		try {
			SimpleType st = SimpleType.newType(type, indexProvider);
			SimpleObject so = new SimpleObject(st, 1);

			so.getAllTags("time").size();
		} finally {
			SimpleType.removeAllTypes();
		}
	}

	@Test
	public void testGetTag(@Mocked ObjectType type, @Mocked IndexProvider indexProvider) throws Exception {
		final Set<PropertyDescr> descrs = CollectionUtils.newSet();
		final PropertyDescr namePD = new PropertyDescr("name", String.class);
		final PropertyDescr valuePD = new PropertyDescr("value", Integer.class);
		namePD.getTagDescriptors().add(new TagDescriptor("name", String.class.getName()));
		namePD.getTagDescriptors().add(new TagDescriptor("length", Integer.class.getName()));
		valuePD.getTagDescriptors().add(new TagDescriptor("value", Integer.class.getName()));
		valuePD.getTagDescriptors().add(new TagDescriptor("count", Integer.class.getName()));
		descrs.add(namePD);
		descrs.add(valuePD);

		new Expectations() {
			{
				type.getName();
				times = 3;
				returns("TYPE");

				type.getPropertyDescriptors();
				times = 2;
				returns(descrs);

			}
		};
		try {
			SimpleType st = SimpleType.newType(type, indexProvider);
            SimpleObject so = st.addObject(1);

			List<String> propertyNames = new LinkedList<String>();
			List<String> tagNames = new LinkedList<String>();
			Map<String, Collection<Tag>> tags = so.getTags(propertyNames, tagNames);
			assertEquals(2, tags.size());

			propertyNames.add("name");
			propertyNames.add("value");
			tagNames.add("name");
			tagNames.add("count");

			tags = so.getTags(propertyNames, tagNames);
			assertEquals(2, tags.size());

			assertEquals("name", (String) so.getTag("name", "name").getPropertyName());
			assertEquals("name", (String) so.getTag("name", "name").getTagName());
			assertEquals(null, (String) so.getTag("name", "name").getValue());
		} finally {
			SimpleType.removeAllTypes();
		}
	}

	@Test
	public void testGetTagValue(@Mocked ObjectType type, @Mocked IndexProvider indexProvider) throws Exception {
		final Set<PropertyDescr> descrs = CollectionUtils.newSet();
		final PropertyDescr namePD = new PropertyDescr("name", String.class);
		namePD.getTagDescriptors().add(new TagDescriptor("name", String.class.getName()));
		descrs.add(namePD);

		new Expectations() {
			{
				type.getName();
				times = 3;
				returns("TYPE");

				type.getPropertyDescriptors();
				times = 2;
				returns(descrs);

			}
		};
		try {
			SimpleType st = SimpleType.newType(type, indexProvider);
            SimpleObject so = st.addObject(1);

			so.getTag("name", "name").setValue("tag_name");

			assertEquals("tag_name", so.getTagValue("name", "name", String.class));
		} finally {
			SimpleType.removeAllTypes();
		}
	}

	@Test
	public void testSetTagValue(@Mocked ObjectType type, @Mocked IndexProvider indexProvider) throws Exception {
		final Set<PropertyDescr> descrs = CollectionUtils.newSet();
		final PropertyDescr namePD = new PropertyDescr("name", String.class);
		namePD.getTagDescriptors().add(new TagDescriptor("name", String.class.getName()));
		descrs.add(namePD);

		new Expectations() {
			{
				type.getName();
				times = 3;
				returns("TYPE");

				type.getPropertyDescriptors();
				times = 2;
				returns(descrs);

			}
		};
		try {
			SimpleType st = SimpleType.newType(type, indexProvider);
            SimpleObject so = st.addObject(1);

			so.setTagValue("name", "name", "set_tag_name");

			assertEquals("set_tag_name", so.getTagValue("name", "name", String.class));
		} finally {
			SimpleType.removeAllTypes();
		}
	}

	@Test(expected = PropertyNotFoundException.class)
	public void testSetTagValueOfNonExistProp(@Mocked ObjectType type, @Mocked IndexProvider indexProvider)
	        throws Exception {
		final Set<PropertyDescr> descrs = CollectionUtils.newSet();
		final PropertyDescr namePD = new PropertyDescr("name", String.class);
		namePD.getTagDescriptors().add(new TagDescriptor("name", String.class.getName()));
		descrs.add(namePD);

		new Expectations() {
			{
				type.getName();
				times = 3;
				returns("TYPE");

				type.getPropertyDescriptors();
				times = 2;
				returns(descrs);

			}
		};
		try {
			SimpleType st = SimpleType.newType(type, indexProvider);
			SimpleObject so = new SimpleObject(st, 1);

			so.setTagValue("name", "name", "set_tag_name");
		} finally {
			SimpleType.removeAllTypes();
		}
	}

	@Test(expected = TagNotFoundException.class)
	public void testSetTagValueOfNonExistTag(@Mocked ObjectType type, @Mocked IndexProvider indexProvider) throws Exception {
		final Set<PropertyDescr> descrs = CollectionUtils.newSet();
		final PropertyDescr namePD = new PropertyDescr("name", String.class);
		namePD.getTagDescriptors().add(new TagDescriptor("name", String.class.getName()));
		descrs.add(namePD);

		new Expectations() {
			{
				type.getName();
				times = 3;
				returns("TYPE");

				type.getPropertyDescriptors();
				times = 2;
				returns(descrs);

			}
		};
		try {
			SimpleType st = SimpleType.newType(type, indexProvider);
            SimpleObject so = st.addObject(1);

			so.setTagValue("name", "length", "set_tag_name");
		} finally {
			SimpleType.removeAllTypes();
		}
	}

	@Test(expected = UnableToConvertTagException.class)
	public void testSetTagValueUnableToConvertException(@Mocked ObjectType type, @Mocked IndexProvider indexProvider)
	        throws Exception {
		final Set<PropertyDescr> descrs = CollectionUtils.newSet();
		final PropertyDescr namePD = new PropertyDescr("name", String.class);
		namePD.getTagDescriptors().add(new TagDescriptor("name", Integer.class.getName()));
		descrs.add(namePD);

		new Expectations() {
			{
				type.getName();
				times = 3;
				returns("TYPE");

				type.getPropertyDescriptors();
				times = 2;
				returns(descrs);

			}
		};
		try {
			SimpleType st = SimpleType.newType(type, indexProvider);
            SimpleObject so = st.addObject(1);
			so.setTagValue("name", "name", "set_tag_name");
		} finally {
			SimpleType.removeAllTypes();
		}
	}

	@Test(expected = PropertyNotFoundException.class)
	public void testGetTagValuePropNotFound(@Mocked ObjectType type, @Mocked IndexProvider indexProvider) throws Exception {
		final Set<PropertyDescr> descrs = CollectionUtils.newSet();
		final PropertyDescr namePD = new PropertyDescr("name", String.class);
		namePD.getTagDescriptors().add(new TagDescriptor("name", String.class.getName()));
		descrs.add(namePD);

		new Expectations() {
			{
				type.getName();
				times = 3;
				returns("TYPE");

				type.getPropertyDescriptors();
				times = 2;
				returns(descrs);

			}
		};
		try {
			SimpleType st = SimpleType.newType(type, indexProvider);
			SimpleObject so = new SimpleObject(st, 1);

			so.getTagValue("time", "name", String.class);
		} finally {
			SimpleType.removeAllTypes();
		}
	}

	@Test(expected = TagNotFoundException.class)
	public void testGetTagValueTagNotFound(@Mocked ObjectType type, @Mocked IndexProvider indexProvider) throws Exception {
		final Set<PropertyDescr> descrs = CollectionUtils.newSet();
		final PropertyDescr namePD = new PropertyDescr("name", String.class);
		namePD.getTagDescriptors().add(new TagDescriptor("name", String.class.getName()));
		descrs.add(namePD);

		new Expectations() {
			{
				type.getName();
				times = 3;
				returns("TYPE");

				type.getPropertyDescriptors();
				times = 2;
				returns(descrs);

			}
		};
		try {
			SimpleType st = SimpleType.newType(type, indexProvider);
            SimpleObject so = st.addObject(1);

            so.getTagValue("name", "time", String.class);
		} finally {
			SimpleType.removeAllTypes();
		}
	}

	@Test(expected = UnableToConvertTagException.class)
	public void testGetTagValueClassCastException(@Mocked ObjectType type, @Mocked IndexProvider indexProvider)
	        throws Exception {
		final Set<PropertyDescr> descrs = CollectionUtils.newSet();
		final PropertyDescr namePD = new PropertyDescr("name", String.class);
		namePD.getTagDescriptors().add(new TagDescriptor("name", String.class.getName()));
		descrs.add(namePD);

		new Expectations() {
			{
				type.getName();
				times = 3;
				returns("TYPE");

				type.getPropertyDescriptors();
				times = 2;
				returns(descrs);

			}
		};
		try {
			SimpleType st = SimpleType.newType(type, indexProvider);
            SimpleObject so = st.addObject(1);

			so.getTag("name", "name").setValue("tag_name");

			assertEquals("tag_name", so.getTagValue("name", "name", Integer.class));
		} finally {
			SimpleType.removeAllTypes();
		}
	}

	@Test
	public void testUpdatePropertyTags(@Mocked ObjectType type, @Mocked IndexProvider indexProvider) throws Exception {
		final Set<PropertyDescr> descrs = CollectionUtils.newSet();
		final PropertyDescr namePD = new PropertyDescr("name", String.class);
		final PropertyDescr valuePD = new PropertyDescr("value", Integer.class);
		namePD.getTagDescriptors().add(new TagDescriptor("name", String.class.getName()));
		valuePD.getTagDescriptors().add(new TagDescriptor("value", Integer.class.getName()));
		descrs.add(namePD);
		descrs.add(valuePD);

		new StrictExpectations() {
			{
				type.getName();
				times = 3;
				returns("TYPE");

				type.getPropertyDescriptors();
				times = 3;
				returns(descrs);

				type.getName();
				times = 1;
				returns("TYPE");

				type.getPropertyDescriptors();
				times = 1;
				returns(descrs);

				type.getName();
				times = 1;
				returns("TYPE");

				type.getPropertyDescriptors();
				times = 1;
				returns(descrs);

				type.getName();
				times = 1;
				returns("TYPE");

				type.getPropertyDescriptors();
				times = 1;
				returns(descrs);

				type.getName();
				times = 1;
				returns("TYPE");
			}
		};
		try {
			SimpleType st = SimpleType.newType(type, indexProvider);
            SimpleObject so = st.addObject(1);
			assertEquals(2, so.getAllTags().size());

			namePD.getTagDescriptors().add(new TagDescriptor("length", Integer.class.getName()));
			valuePD.getTagDescriptors().add(new TagDescriptor("status", String.class.getName()));
			valuePD.getTagDescriptors().remove((TagDescriptor) new TagDescriptor("value", Integer.class.getName()));

			st.updateEnvType(type);

			so.updatePropertyTags("name");
			so.updatePropertyTags("value");

			assertEquals(2, so.getAllTags("name").size());
			assertEquals(1, so.getAllTags("value").size());
			assertEquals("status", so.getTag("value", "status").getTagName());
			assertEquals("name", so.getTag("name", "name").getTagName());
			assertEquals("length", so.getTag("name", "length").getTagName());

			valuePD.getTagDescriptors().clear();
			st.updateEnvType(type);
			so.updatePropertyTags("value");
			assertEquals(0, so.getAllTags("value").size());
			System.out.println(so.toString());
		} finally {
			SimpleType.removeAllTypes();
		}
	}

	@Test
	public void testRemoveParents(@Mocked ObjectType type, @Mocked IndexProvider indexProvider) throws Exception {
		final Set<PropertyDescr> descrs = CollectionUtils.newSet();
		descrs.add(pdName);

		new Expectations() {
			{
				type.getName();
				times = 3;
				returns("TYPE");

				type.getPropertyDescriptors();
				times = 2;
				returns(descrs);

			}
		};
		try {
			SimpleType st = SimpleType.newType(type, indexProvider);
			SimpleObject so = new SimpleObject(st, 1);
			so.getParents().add(new Pair<String, Integer>("ALERT", 1));
			so.getParents().add(new Pair<String, Integer>("ALERT", 2));
			so.getParents().add(new Pair<String, Integer>("ALERT", 3));
			so.getParents().add(new Pair<String, Integer>("STAT", 4));
			so.getParents().add(new Pair<String, Integer>("STAT", 5));

			assertEquals(5, so.getParents().size());

			so.removeParents("STAT");
			assertEquals(3, so.getParents().size());
		} finally {
			SimpleType.removeAllTypes();
		}
	}

	@Test
	public void testGetSetPropertyTimestamp(@Mocked ObjectType type, @Mocked IndexProvider indexProvider) throws Exception {
		final Set<PropertyDescr> descrs = CollectionUtils.newSet();
		descrs.add(pdName);

		new Expectations() {
			{
				type.getName();
				times = 3;
				returns("TYPE");

				type.getPropertyDescriptors();
				times = 2;
				returns(descrs);

			}
		};
		try {
			SimpleType st = SimpleType.newType(type, indexProvider);
            SimpleObject so = st.addObject(1);

			so.setPropertyTimestamp("name", new Date(0));
			assertEquals(0L, so.getPropertyTimestamp("name"));

			so.setPropertyTimestamp("name", 100L);
			assertEquals(100L, so.getPropertyTimestamp("name"));

			/*
			 * try to set timestamp to non exist property
			 */
			so.setPropertyTimestamp("value", 100L);
			assertEquals(0, so.getPropertyTimestamp("value"));

		} finally {
			SimpleType.removeAllTypes();
		}
	}

	@Test
	public void testAddPropertyToObject(@Mocked ObjectType type, @Mocked IndexProvider indexProvider) throws Exception {
		final Set<PropertyDescr> descrs = CollectionUtils.newSet();

        final PropertyDescr pdName = new PropertyDescr("name", String.class);
        descrs.add(pdName);

        final PropertyDescr pdValue  = new PropertyDescr("value", Integer.class);
        descrs.add(pdValue);

		final PropertyDescr pdTime = new PropertyDescr("time", Long.class);
		pdTime.setHistorical(true);
        descrs.add(pdTime);

		final PropertyDescr pdHost = new PropertyDescr("host", Collection.class);
		pdHost.setCollection(true);
        descrs.add(pdHost);

		final PropertyDescr pdDescr = new PropertyDescr("description", String.class);
		pdDescr.setStatic(true);
        descrs.add(pdDescr);

		new Expectations() {
			{
				type.getName();
				times = 3;
				returns("TYPE");

				type.getPropertyDescriptors();
				times = 2;
				returns(descrs);

			}
		};
		try {
			SimpleType st = SimpleType.newType(type, indexProvider);
            SimpleObject so = st.addObject(1);

			/*
			 * 4 added + numAlerts = 5
			 * statics are not returned by getAllPropertiesValues
			 */
			assertEquals(5, so.getAllPropertiesValues().size());

		} finally {
			SimpleType.removeAllTypes();
		}
	}

	@Test
	public void testIsComplain(@Mocked ObjectType type, @Mocked IndexProvider indexProvider) throws Exception {
		final Set<PropertyDescr> descrs = CollectionUtils.newSet();
        final PropertyDescr pdName = new PropertyDescr("name", String.class);
        final PropertyDescr pdValue = new PropertyDescr("value", Integer.class);
		final PropertyDescr pdTime = new PropertyDescr("time", Long.class);
		final PropertyDescr pdHost = new PropertyDescr("host", Collection.class);
		final PropertyDescr pdDescr = new PropertyDescr("description", String.class);

        descrs.add(pdName);
		descrs.add(pdValue);
		descrs.add(pdTime);
		descrs.add(pdHost);
		descrs.add(pdDescr);

		new Expectations() {
			{
				type.getName();
				times = 3;
				returns("TYPE");

				type.getPropertyDescriptors();
				times = 2;
				returns(descrs);

			}
		};
		try {
			SimpleType st = SimpleType.newType(type, indexProvider);
            SimpleObject so = st.addObject(1);

            so.setPropertyValue("name", "test_name", 0L);
            so.setPropertyValue("value", 100 , 0L);
            so.setPropertyValue("time", 0L , 0L);
            so.setPropertyValue("host", CollectionUtils.newSet() , 0L);
            so.setPropertyValue("description", "test_description" , 0L);

            Map<String, Value<?>> conditions = new HashMap<String, Value<?>>();
			conditions.put("name", Value.createTypedValue("test_name"));
			conditions.put("value", Value.createTypedValue(100));
			conditions.put("time", Value.createTypedValue(0L));
			conditions.put("host", Value.createTypedValue(CollectionUtils.newSet()));
			conditions.put("description", Value.createTypedValue("test_description"));

			assertTrue("object does not match conditions", so.isComplain(conditions));

			conditions.put("description", Value.createTypedValue("bad_description"));
			assertFalse(so.isComplain(conditions));
			conditions.remove("description");

			conditions.put("bad_prop", Value.createTypedValue("bad_prop"));
			assertFalse(so.isComplain(conditions));
		} finally {
			SimpleType.removeAllTypes();
		}
	}
}
