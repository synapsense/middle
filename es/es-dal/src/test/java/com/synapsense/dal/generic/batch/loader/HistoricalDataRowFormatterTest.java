package com.synapsense.dal.generic.batch.loader;

import com.synapsense.dal.generic.entities.HistoricalValueDO;
import org.junit.Test;

import java.util.Date;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * @author : shabanov
 */
public class HistoricalDataRowFormatterTest {

	@Test
	public void formatStringValue() throws Exception {
		String nullValue = "\\N";
		HistoricalDataRowFormatter ldf = new HistoricalDataRowFormatter("#########.###", "yyyy-MM-dd H:m:s", '.',
		        nullValue);

		String fieldSeparator = ldf.getFieldSeparator();

		// test data
		int groupId = 1;
		int valueId = 1;
		Date recordTime = new Date(0L);
		HistoricalValueDO hvd = new HistoricalValueDO(valueId, recordTime, groupId);
		hvd.setUnifiedValue("\0String \"value\" with\n special\r\n characters\r\0");

		String row = ldf.format(hvd);

		assertThat(row, is("" + valueId + fieldSeparator + ldf.getDateFormat().format(recordTime) + fieldSeparator
		        + nullValue + fieldSeparator + nullValue + fieldSeparator + nullValue + fieldSeparator
		        + "String \"value\" with\\n special\\n characters\\n" + fieldSeparator + groupId));
	}

	@Test
	public void formatDoubleValue() throws Exception {
		String nullValue = "\\N";
		HistoricalDataRowFormatter ldf = new HistoricalDataRowFormatter("#########.###", "yyyy-MM-dd H:m:s", '.',
		        nullValue);

		String fieldSeparator = ldf.getFieldSeparator();

		// test data
		int groupId = 1;
		int valueId = 1;
		Date recordTime = new Date(0L);
		HistoricalValueDO hvd = new HistoricalValueDO(valueId, recordTime, groupId);
		hvd.setUnifiedValue(0.1D);

		String row = ldf.format(hvd);

		assertThat(row, is("" + valueId + fieldSeparator + ldf.getDateFormat().format(recordTime) + fieldSeparator
		        + nullValue + fieldSeparator + hvd.getUnifiedValue() + fieldSeparator + nullValue + fieldSeparator
		        + nullValue + fieldSeparator + groupId));
	}

	@Test
	public void formatIntValue() throws Exception {
		String nullValue = "\\N";
		HistoricalDataRowFormatter ldf = new HistoricalDataRowFormatter("#########.###", "yyyy-MM-dd H:m:s", '.',
		        nullValue);

		String fieldSeparator = ldf.getFieldSeparator();

		// test data
		int groupId = 1;
		int valueId = 1;
		Date recordTime = new Date(0L);
		HistoricalValueDO hvd = new HistoricalValueDO(valueId, recordTime, groupId);
		hvd.setUnifiedValue(123456789);

		String row = ldf.format(hvd);

		assertThat(row, is("" + valueId + fieldSeparator + ldf.getDateFormat().format(recordTime) + fieldSeparator
		        + hvd.getUnifiedValue() + fieldSeparator + nullValue + fieldSeparator + nullValue + fieldSeparator
		        + nullValue + fieldSeparator + groupId));
	}

	@Test
	public void formatLongValue() throws Exception {
		String nullValue = "\\N";
		HistoricalDataRowFormatter ldf = new HistoricalDataRowFormatter("#########.###", "yyyy-MM-dd H:m:s", '.',
		        nullValue);

		String fieldSeparator = ldf.getFieldSeparator();

		// test data
		int groupId = 1;
		int valueId = 1;
		Date recordTime = new Date(0L);
		HistoricalValueDO hvd = new HistoricalValueDO(valueId, recordTime, groupId);
		hvd.setUnifiedValue(1000L);

		String row = ldf.format(hvd);

		assertThat(row, is("" + valueId + fieldSeparator + ldf.getDateFormat().format(recordTime) + fieldSeparator
		        + nullValue + fieldSeparator + nullValue + fieldSeparator + hvd.getValuel() + fieldSeparator
		        + nullValue + fieldSeparator + groupId));
	}

}
