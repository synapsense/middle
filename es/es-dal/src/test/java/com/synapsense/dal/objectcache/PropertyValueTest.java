package com.synapsense.dal.objectcache;

import static org.junit.Assert.*;

import java.util.Collection;
import java.util.LinkedList;

import mockit.Expectations;
import mockit.Mock;
import mockit.MockUp;

import mockit.Mocked;
import mockit.StrictExpectations;
import mockit.integration.junit4.JMockit;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.synapsense.dto.CollectionTO;
import com.synapsense.dto.ObjectType;
import com.synapsense.dto.PropertyDescr;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.exception.PropertyNotFoundException;
import com.synapsense.exception.UnableToConvertPropertyException;
import com.synapsense.service.impl.dao.to.PropertyTO;
import com.synapsense.service.impl.nsvc.EventNotifier;
import com.synapsense.service.nsvc.Event;
import org.junit.runner.RunWith;

@RunWith(JMockit.class)
public class PropertyValueTest {
	
	final static class MockedCacheSvcImpl extends MockUp<CacheSvcImpl> {
		@Mock
		public void start(){}
	}

	static CacheSvcImpl c;
	
	@BeforeClass
	public static void se() {
		new MockedCacheSvcImpl();
		c = new CacheSvcImpl();
	}
	
	@Test
	@Ignore
	public void testGetPropertyValue(@Mocked SimpleType st) throws Exception {
		final Collection<String> typesNames = new LinkedList<String>();
		typesNames.add("TYPE");

		final TO<?> to = TOFactory.getInstance().loadTO("TYPE", 1);

		new Expectations() {
			{
				SimpleType.enumerateAllTypeNames();
				times = 1;
				result = typesNames;

				SimpleType.findType("TYPE");
				times = 1;
				result = st;

				st.getValue(1, "name", String.class);
				times = 1;
				result = "test";
			}
		};
		assertEquals("test", c.getPropertyValue(to, "name", String.class));
	}

	@Test(expected = ObjectNotFoundException.class)
	@Ignore
	public void testGetPropertyValueOfNonExistType(@Mocked SimpleType st) throws Exception {
		final Collection<String> typesNames = new LinkedList<String>();
		typesNames.add("TYPE");

		final TO<?> to = TOFactory.getInstance().loadTO("TYPE", 1);

		new Expectations() {
			{
				SimpleType.enumerateAllTypeNames();
				times = 1;
				result = typesNames;

				SimpleType.findType("TYPE");
				times = 1;
				result = null;
			}
		};
		c.getPropertyValue(to, "name", String.class);
	}

	@Test(expected = ObjectNotFoundException.class)
	public void testGetPropertyValueOfNullObject(@Mocked SimpleType st) throws Exception {
		final TO<?> to = null;

		c.getPropertyValue(to, "name", String.class);
	}

	@Test
	@Ignore
	public void testGetCollectionPropertyValue(@Mocked SimpleType st) throws Exception {
		final Collection<TO<?>> objIds = new LinkedList<TO<?>>();
		final TO<?> TO1 = TOFactory.getInstance().loadTO("TYPE", 1);
		final TO<?> TO2 = TOFactory.getInstance().loadTO("TYPE", 2);
		objIds.add(TO1);
		objIds.add(TO2);

		final Collection<String> typesNames = new LinkedList<String>();
		typesNames.add("TYPE");

		final Collection<CollectionTO> ret = new LinkedList<CollectionTO>();
		final CollectionTO cTO1 = new CollectionTO(TO1);
		final CollectionTO cTO2 = new CollectionTO(TO2);
		cTO1.addValue(new ValueTO("name", "test1"));
		cTO1.addValue(new ValueTO("value", 100));
		cTO2.addValue(new ValueTO("name", "test2"));
		cTO2.addValue(new ValueTO("value", 200));
		ret.add(cTO1);
		ret.add(cTO2);

		new Expectations() {
			{
				SimpleType.enumerateAllTypeNames();
				times = 1;
				result = typesNames;

				SimpleType.findType("TYPE");
				times = 1;
				result = st;

				st.getValue(1, "name", ValueTO.class);
				times = 1;
				result = new ValueTO("name", "test1");

				st.getValue(1, "value", ValueTO.class);
				times = 1;
				result = new ValueTO("value", 100);

				// ******************

				SimpleType.enumerateAllTypeNames();
				times = 1;
				result = typesNames;

				SimpleType.findType("TYPE");
				times = 1;
				result = st;

				st.getValue(2, "name", ValueTO.class);
				times = 1;
				result = new ValueTO("name", "test2");

				st.getValue(2, "value", ValueTO.class);
				times = 1;
				result = new ValueTO("value", 200);
			}
		};
		assertEquals(ret, c.getPropertyValue(objIds, new String[] { "name", "value" }));
	}

	@Test
	@Ignore
	public void testGetCollectionPropertyValueOfNonExistType(@Mocked SimpleType st) throws Exception {
		final Collection<String> typesNames = new LinkedList<String>();
		typesNames.add("TYPE");

		final Collection<TO<?>> objIds = new LinkedList<TO<?>>();
		final TO<?> TO1 = TOFactory.getInstance().loadTO("TYPE", 1);
		final TO<?> TO2 = TOFactory.getInstance().loadTO("TYPE", 2);
		objIds.add(TO1);
		objIds.add(TO2);

		new Expectations() {
			{
				SimpleType.enumerateAllTypeNames();
				times = 1;
				result = typesNames;

				SimpleType.findType("TYPE");
				times = 1;
				result = null;

				SimpleType.enumerateAllTypeNames();
				times = 1;
				result = typesNames;

				SimpleType.findType("TYPE");
				times = 1;
				result = null;
			}
		};
		assertTrue(c.getPropertyValue(objIds, new String[] { "name", "value" }).isEmpty());
	}

	@Test
	@Ignore
	public void testGetCollectionPropertyValueOfNonExistPropertyObject(@Mocked SimpleType st) throws Exception {
		final Collection<String> typesNames = new LinkedList<String>();
		typesNames.add("TYPE");

		final Collection<TO<?>> objIds = new LinkedList<TO<?>>();
		final TO<?> TO1 = TOFactory.getInstance().loadTO("TYPE", 1);
		objIds.add(TO1);

		new Expectations() {
			{
				SimpleType.enumerateAllTypeNames();
				times = 1;
				returns(typesNames);

				SimpleType.findType("TYPE");
				times = 1;
				returns(st);

				st.getValue(1, "name", ValueTO.class);
				times = 1;
				result = new ObjectNotFoundException(TOFactory.getInstance().loadTO("name", 1));
			}
		};
		c.getPropertyValue(objIds, new String[] { "name" });
	}

	@Test
	@Ignore
	public void testGetCollectionPropertyValueOfNonExistProperty(@Mocked SimpleType st) throws Exception {
		final Collection<String> typesNames = new LinkedList<String>();
		typesNames.add("TYPE");

		final Collection<TO<?>> objIds = new LinkedList<TO<?>>();
		final TO<?> TO1 = TOFactory.getInstance().loadTO("TYPE", 1);
		objIds.add(TO1);

		new Expectations() {
			{
				SimpleType.enumerateAllTypeNames();
				times = 1;
				returns(typesNames);

				SimpleType.findType("TYPE");
				times = 1;
				returns(st);

				st.getValue(1, "name", ValueTO.class);
				times = 1;
				result = new PropertyNotFoundException("name");
			}
		};
		c.getPropertyValue(objIds, new String[] { "name" });
	}

	@Test
	@Ignore
	public void testGetCollectionPropertyValueUnableToConvert(@Mocked SimpleType st) throws Exception {
		final Collection<String> typesNames = new LinkedList<String>();
		typesNames.add("TYPE");

		final Collection<TO<?>> objIds = new LinkedList<TO<?>>();
		final TO<?> TO1 = TOFactory.getInstance().loadTO("TYPE", 1);
		objIds.add(TO1);

		new Expectations() {
			{
				SimpleType.enumerateAllTypeNames();
				times = 1;
				returns(typesNames);

				SimpleType.findType("TYPE");
				times = 1;
				returns(st);

				st.getValue(1, "name", ValueTO.class);
				times = 1;
				result = new UnableToConvertPropertyException("name", String.class.getName());
			}
		};
		c.getPropertyValue(objIds, new String[] { "name" });
	}

	@Test
	@Ignore
	public void testGetPropertyValueByString(@Mocked SimpleType st) throws Exception {
		final Collection<String> typesNames = new LinkedList<String>();
		typesNames.add("TYPE");

		new Expectations() {
			{
				SimpleType.enumerateAllTypeNames();
				times = 1;
				result = typesNames;

				SimpleType.findType("TYPE");
				times = 1;
				result = st;

				st.getValue(null, "name", String.class);
				times = 1;
				result = "test";
			}
		};
		assertEquals("test", c.getPropertyValue("TYPE", "name", String.class));
	}

	@Test(expected = ObjectNotFoundException.class)
	@Ignore
	public void testGetPropertyValueByStringOfNonExistType(@Mocked SimpleType st) throws Exception {
		final Collection<String> typesNames = new LinkedList<String>();
		typesNames.add("TYPE");

		new Expectations() {
			{
				SimpleType.enumerateAllTypeNames();
				times = 1;
				result = typesNames;

				SimpleType.findType("TYPE");
				times = 1;
				result = null;

			}
		};
		c.getPropertyValue("TYPE", "name", String.class);
	}

	@Test
	@Ignore
	public void testGetPropertyDescriptor(@Mocked SimpleType st, @Mocked ObjectType ot) throws Exception {
		final Collection<String> typesNames = new LinkedList<String>();
		typesNames.add("TYPE");

		final PropertyDescr pd = new PropertyDescr("name", String.class.getName());

		new Expectations() {
			{
				SimpleType.enumerateAllTypeNames();
				times = 1;
				result = typesNames;

				SimpleType.findType("TYPE");
				times = 1;
				result = st;

				st.getEnvType();
				times = 1;
				returns(ot);

				ot.getPropertyDescriptor("name");
				times = 1;
				result = pd;
			}
		};
		assertEquals(pd, c.getPropertyDescriptor("TYPE", "name"));
	}

	@Test(expected = ObjectNotFoundException.class)
	@Ignore
	public void testGetPropertyDescriptorOfNonExistType(@Mocked SimpleType st) throws Exception {
		final Collection<String> typesNames = new LinkedList<String>();
		typesNames.add("TYPE");

		new Expectations() {
			{
				SimpleType.enumerateAllTypeNames();
				times = 1;
				returns(typesNames);

				SimpleType.findType("TYPE");
				times = 1;
				returns(null);
			}
		};
		c.getPropertyDescriptor("TYPE", "name");
	}

	@Test(expected = PropertyNotFoundException.class)
	@Ignore
	public void testGetNonExistPropertyDescriptor(@Mocked SimpleType st, @Mocked ObjectType ot) throws Exception {
		final Collection<String> typesNames = new LinkedList<String>();
		typesNames.add("TYPE");

		new Expectations() {
			{
				SimpleType.enumerateAllTypeNames();
				times = 1;
				returns(typesNames);

				SimpleType.findType("TYPE");
				times = 1;
				returns(st);

				st.getEnvType();
				times = 1;
				returns(ot);

				ot.getPropertyDescriptor("name");
				times = 1;
				returns(null);
			}
		};
		c.getPropertyDescriptor("TYPE", "name");
	}

	@Test
	public void testGetAllPropertiesValues(@Mocked SimpleType st, @Mocked SimpleObject so) throws Exception {
		final TO<?> to = TOFactory.getInstance().loadTO("TYPE", 1);
		final Collection<ValueTO> ret = new LinkedList<ValueTO>();
		final ValueTO vto1 = new ValueTO("name", "Test");
		final ValueTO vto2 = new ValueTO("value", 100);
		ret.add(vto1);
		ret.add(vto2);

		final Collection<SimplePropertyValue> data = new LinkedList<SimplePropertyValue>();
		data.add(new SimplePropertyValue("name", Value.createTypedValue("Test")));
		data.add(new SimplePropertyValue("value", Value.createTypedValue(100)));

		new Expectations() {
			{
				SimpleType.findType("TYPE");
				times = 1;
				returns(st);

				st.getObject(1);
				times = 1;
				returns(so);

				so.getAllPropertiesValues();
				times = 1;
				returns(data);
			}
		};
		assertEquals(ret, c.getAllPropertiesValues(to));
	}

	@Test(expected = ObjectNotFoundException.class)
	public void testGetAllPropertiesValuesOfNonExistObject(@Mocked SimpleType st) throws Exception {
		final TO<?> to = TOFactory.getInstance().loadTO("TYPE", 1);

		new Expectations() {
			{
				SimpleType.findType("TYPE");
				times = 1;
				returns(st);

				st.getObject(1);
				times = 1;
				returns(null);

			}
		};
		c.getAllPropertiesValues(to);
	}

	@Test
	public void testGetAllPropertiesValuesOfCollection(@Mocked SimpleType st, @Mocked SimpleObject so) throws Exception {
		final TO<?> to1 = TOFactory.getInstance().loadTO("TYPE", 1);
		final TO<?> to2 = TOFactory.getInstance().loadTO("TYPE", 2);
		final Collection<TO<?>> cto = new LinkedList<TO<?>>();
		cto.add(to1);
		cto.add(to2);

		final Collection<CollectionTO> ret = new LinkedList<CollectionTO>();
		final CollectionTO cto1 = new CollectionTO(to1);
		final CollectionTO cto2 = new CollectionTO(to2);
		final ValueTO vto1 = new ValueTO("name", "Test1");
		final ValueTO vto2 = new ValueTO("value", 100);
		final ValueTO vto3 = new ValueTO("name", "Test2");
		final ValueTO vto4 = new ValueTO("value", 200);
		cto1.addValue(vto1);
		cto1.addValue(vto2);
		cto2.addValue(vto3);
		cto2.addValue(vto4);
		ret.add(cto1);
		ret.add(cto2);

		final Collection<SimplePropertyValue> data1 = new LinkedList<SimplePropertyValue>();
		data1.add(new SimplePropertyValue("name", Value.createTypedValue("Test1")));
		data1.add(new SimplePropertyValue("value", Value.createTypedValue(100)));

		final Collection<SimplePropertyValue> data2 = new LinkedList<SimplePropertyValue>();
		data2.add(new SimplePropertyValue("name", Value.createTypedValue("Test2")));
		data2.add(new SimplePropertyValue("value", Value.createTypedValue(200)));

		new StrictExpectations() {
			{
				SimpleType.findType("TYPE");
				times = 1;
				returns(st);

				st.getObject(1);
				times = 1;
				returns(so);

				so.getAllPropertiesValues();
				times = 1;
				returns(data1);

				SimpleType.findType("TYPE");
				times = 1;
				returns(st);

				st.getObject(2);
				times = 1;
				returns(so);

				so.getAllPropertiesValues();
				times = 1;
				returns(data2);
			}
		};
		assertEquals(ret, c.getAllPropertiesValues(cto));
	}
	
	@Test
	public void testGetAllPropValuesOfCollectionWithNonExistPropObject(@Mocked SimpleType st, @Mocked SimpleObject so) throws Exception {
		final TO<?> to1 = TOFactory.getInstance().loadTO("TYPE", 1);
		final Collection<TO<?>> cto = new LinkedList<TO<?>>();
		cto.add(to1);
		
		final Collection<CollectionTO> ret = new LinkedList<CollectionTO>();
		final CollectionTO cto1 = new CollectionTO(to1);
		ret.add(cto1);

		new Expectations() {
			{
				SimpleType.findType("TYPE");
				times = 1;
				returns(st);

				st.getObject(1);
				times = 1;
				returns(null);
			}
		};
		assertEquals(ret, c.getAllPropertiesValues(cto));
	}

	@Test
	@Ignore
	public void testSetAllPropertiesValues(@Mocked SimpleType st, @Mocked SimpleObject so) throws Exception {
		final Collection<String> typesNames = new LinkedList<String>();
		typesNames.add("TYPE");
		final TO<?> to = TOFactory.getInstance().loadTO("TYPE", 1);
		final ValueTO[] vto = new ValueTO[3];
		vto[0] = new ValueTO("name", "test_name");
		vto[1] = new ValueTO("value", 100);
		vto[2] = new ValueTO("descr", "test property");

		new Expectations() {
			{
				SimpleType.enumerateAllTypeNames();returns(typesNames);
				SimpleType.findType("TYPE");returns(st);

                SimpleType.enumerateAllTypeNames();returns(typesNames);
                SimpleType.findType("TYPE");returns(st);

                st.getValue(1, vto[0].getPropertyName(), Object.class);
                result="test_name";
				st.setPropValue(1, vto[0]);
                SimpleType.findType("TYPE");returns(st);
				st.getObject(1);returns(so);

                SimpleType.enumerateAllTypeNames();returns(typesNames);
                SimpleType.findType("TYPE");returns(st);

                st.getValue(1, vto[1].getPropertyName(), Object.class);
                result=100;
                st.setPropValue(1, vto[1]);
                SimpleType.findType("TYPE");returns(st);
                st.getObject(1);returns(so);


                SimpleType.enumerateAllTypeNames();returns(typesNames);
                SimpleType.findType("TYPE");returns(st);

                st.getValue(1, vto[2].getPropertyName(), Object.class);
                result="test property";
                st.setPropValue(1, vto[2]);
                SimpleType.findType("TYPE");returns(st);
                st.getObject(1);returns(so);
			}
		};
		c.create();
		c.setAllPropertiesValues(to, vto, false);
	}

	@Test
	@Ignore
	public void testSetTOPropertiesValues(@Mocked SimpleType st, @Mocked SimpleObject so) throws Exception {
		final Collection<String> typesNames = new LinkedList<String>();
		typesNames.add("TYPE");

		final TO<?> to = TOFactory.getInstance().loadTO("TYPE", 1);
		final ValueTO[] vto = new ValueTO[1];
		vto[0] = new ValueTO("name", TOFactory.getInstance().loadTO("name_to", 1));

		new Expectations() {
			{
				SimpleType.enumerateAllTypeNames();returns(typesNames);
				SimpleType.findType("TYPE");returns(st);

                SimpleType.enumerateAllTypeNames();returns(typesNames);
                SimpleType.findType("TYPE");returns(st);

                st.getValue(1, vto[0].getPropertyName(), Object.class);
                result="name_to";
				st.setPropValue(1, vto[0]);

				SimpleType.findType("TYPE");returns(st);
				
				st.getObject(1);returns(null);

			}
		};
		c.create();
		c.setAllPropertiesValues(to, vto, false);
	}

	@Test
	@Ignore
	public void testSetCollectionPropertiesValues(@Mocked SimpleType st, @Mocked SimpleObject so, @Mocked ObjectType ot)
	        throws Exception {
		final Collection<String> typesNames = new LinkedList<String>();
		typesNames.add("TYPE");

		final TO<?> to = TOFactory.getInstance().loadTO("TYPE", 1);

		final ValueTO[] vto = new ValueTO[2];
		final Collection<TO<?>> col = new LinkedList<TO<?>>();
		col.add(TOFactory.getInstance().loadTO("host", 2));
		final Collection<PropertyTO> col1 = new LinkedList<PropertyTO>();
		col1.add(new PropertyTO(TOFactory.getInstance().loadTO("value", 3), "value"));
		vto[0] = new ValueTO("hostTO", col);
		vto[1] = new ValueTO("valueTO", col1);

		final PropertyDescr pd = new PropertyDescr("hostTO", TO.class.getName());
		final PropertyDescr pd1 = new PropertyDescr("valueTO", PropertyTO.class.getName());

		new Expectations() {
			{
				SimpleType.enumerateAllTypeNames();returns(typesNames);
				SimpleType.findType("TYPE");returns(st);

				// value #1
                SimpleType.enumerateAllTypeNames();returns(typesNames);
                SimpleType.findType("TYPE");returns(st);

                st.getValue(1, vto[0].getPropertyName(), Object.class);
                result="name_to";
				st.setPropValue(1, vto[0]);

				SimpleType.enumerateAllTypeNames();returns(typesNames);
				SimpleType.findType("TYPE");returns(st);
				st.getEnvType();returns(ot);
				ot.getPropertyDescriptor("hostTO");returns(pd);

				// value #2
				SimpleType.findType("TYPE");returns(st);
				st.getObject(1);returns(so);

                SimpleType.enumerateAllTypeNames();returns(typesNames);
                SimpleType.findType("TYPE");returns(st);

                st.getValue(1, vto[1].getPropertyName(), Object.class);
                result="name_to";
				st.setPropValue(1, vto[1]);

				SimpleType.enumerateAllTypeNames();returns(typesNames);
				SimpleType.findType("TYPE");returns(st);
				st.getEnvType();returns(ot);
				ot.getPropertyDescriptor("valueTO");returns(pd1);

				SimpleType.enumerateAllTypeNames();returns(typesNames);
				SimpleType.findType("TYPE");returns(st);
				st.getEnvType();returns(ot);
				ot.getPropertyDescriptor("valueTO");returns(pd1);

				SimpleType.findType("TYPE");returns(st);
				st.getObject(1);returns(so);
			}
		};
		c.create();
		c.setAllPropertiesValues(to, vto, false);
	}

	@Test
	@Ignore
	public void testSetCollectionPropertiesValuesOfNonExistObject(@Mocked SimpleType st, @Mocked SimpleObject so,
																  @Mocked ObjectType ot) throws Exception {
		final Collection<String> typesNames = new LinkedList<String>();
		typesNames.add("TYPE");

		final TO<?> to = TOFactory.getInstance().loadTO("TYPE", 1);

		final ValueTO[] vto = new ValueTO[1];
		final Collection<TO<?>> col = new LinkedList<TO<?>>();
		col.add(TOFactory.getInstance().loadTO("host", 2));
		vto[0] = new ValueTO("hostTO", col);

		new Expectations() {
			{
				SimpleType.enumerateAllTypeNames();returns(typesNames);
				SimpleType.findType("TYPE");returns(st);

				// value #1
                SimpleType.enumerateAllTypeNames();returns(typesNames);
                SimpleType.findType("TYPE");returns(st);

                st.getValue(1, vto[0].getPropertyName(), Object.class);
                result="name_to";
				st.setPropValue(1, vto[0]);

				SimpleType.enumerateAllTypeNames();returns(typesNames);
				SimpleType.findType("TYPE");returns(null);
				SimpleType.findType("TYPE");returns(null);
			}
		};
		c.create();
		c.setAllPropertiesValues(to, vto, false);
	}

	@Test
	@Ignore
	public void testSetCollectionPropertiesValuesOfNonExistProperty(@Mocked SimpleType st, @Mocked SimpleObject so,
																	@Mocked ObjectType ot) throws Exception {
		final Collection<String> typesNames = new LinkedList<String>();
		typesNames.add("TYPE");

		final TO<?> to = TOFactory.getInstance().loadTO("TYPE", 1);

		final ValueTO[] vto = new ValueTO[1];
		final Collection<TO<?>> col = new LinkedList<TO<?>>();
		col.add(TOFactory.getInstance().loadTO("host", 2));
		vto[0] = new ValueTO("hostTO", col);

		new Expectations() {
			{
				SimpleType.enumerateAllTypeNames();
				times = 1;
				result = typesNames;

				SimpleType.findType("TYPE");
				times = 1;
				returns(st);

				// value #1
                SimpleType.enumerateAllTypeNames();returns(typesNames);
                SimpleType.findType("TYPE");returns(st);

                st.getValue(1, vto[0].getPropertyName(), Object.class);
                result="name_to";
				st.setPropValue(1, vto[0]);
				times = 1;

				SimpleType.enumerateAllTypeNames();
				times = 1;
				result = typesNames;

				SimpleType.findType("TYPE");
				times = 1;
				result = st;

				st.getEnvType();
				times = 1;
				returns(ot);

				ot.getPropertyDescriptor("hostTO");
				times = 1;
				result = null;

				SimpleType.findType("TYPE");returns(st);
				st.getObject(1);returns(so);
			}
		};
		c.create();
		c.setAllPropertiesValues(to, vto, false);
	}

	@Test
	@Ignore
	public void testSetPropertyTOPropertiesValues(@Mocked SimpleType st, @Mocked SimpleObject so, @Mocked ObjectType ot,
												  @Mocked EventNotifier en) throws Exception {
		final Collection<String> typesNames = new LinkedList<String>();
		typesNames.add("TYPE");

		final TO<?> to = TOFactory.getInstance().loadTO("TYPE", 1);
		final TO<?> name_to = TOFactory.getInstance().loadTO("NAME_TO", 2);
		final PropertyTO pto = new PropertyTO(name_to, "name");
		final ValueTO[] vto = new ValueTO[1];
		vto[0] = new ValueTO("name", pto);

		new Expectations() {
			{
				SimpleType.enumerateAllTypeNames();
				times = 1;
				returns(typesNames);

				SimpleType.findType("TYPE");
				times = 1;
				returns(st);

				SimpleType.enumerateAllTypeNames();
				times = 1;
				returns(typesNames);

				SimpleType.findType("TYPE");
				times = 1;
				returns(st);

				st.getValue(1, "name", Object.class);
				times = 1;
				returns(name_to);

				st.setPropValue(1, vto[0]);
				times = 1;

				SimpleType.findType("TYPE");
				times = 1;
				returns(st);

				st.getObject(1);
				times = 1;
				returns(so);

				en.notify(withInstanceOf(Event.class));
				times = 1;
			}
		};
		c.create();
		c.setEventNotifier(en);
		c.setAllPropertiesValues(to, vto, true);
	}

	@Test(expected = ObjectNotFoundException.class)
	@Ignore
	public void testSetAllPropertiesValueOfNonExistType(@Mocked SimpleType st) throws Exception {
		final Collection<String> typesNames = new LinkedList<String>();
		typesNames.add("TYPE");

		final TO<?> to = TOFactory.getInstance().loadTO("TYPE", 1);
		final ValueTO[] vto = new ValueTO[1];
		vto[0] = new ValueTO("name", "test_name");

		new Expectations() {
			{
				SimpleType.enumerateAllTypeNames();
				times = 1;
				returns(typesNames);

				SimpleType.findType("TYPE");
				times = 1;
				returns(null);
			}
		};
		c.create();
		c.setAllPropertiesValues(to, vto, true);
	}

	@Test
	@Ignore
	public void testSetPropertyValue(@Mocked SimpleType st) throws Exception {
		final Collection<String> typesNames = new LinkedList<String>();
		typesNames.add("TYPE");

		new Expectations() {
			{
				SimpleType.enumerateAllTypeNames();
				times = 1;
				returns(typesNames);

				SimpleType.findType("TYPE");
				times = 1;
				returns(st);

				st.setPropValue(null, new ValueTO("name", "test_name"));
				times = 1;
			}
		};

		c.setPropertyValue("TYPE", "name", "test_name");
	}

	@Test(expected = ObjectNotFoundException.class)
	@Ignore
	public void testSetPropertyValueOfNonExistType(@Mocked SimpleType st) throws Exception {
		final Collection<String> typesNames = new LinkedList<String>();
		typesNames.add("TYPE");

		new Expectations() {
			{
				SimpleType.enumerateAllTypeNames();
				times = 1;
				returns(typesNames);

				SimpleType.findType("TYPE");
				times = 1;
				returns(null);
			}
		};

		c.setPropertyValue("TYPE", "name", "test_name");
	}

	@Test
	@Ignore
	public void testSetPropertyValueAsValueTO(@Mocked SimpleType st, @Mocked EventNotifier en, @Mocked SimpleObject so)
	        throws Exception {
		final TO<?> to = TOFactory.getInstance().loadTO("TYPE", 1);
		final Collection<String> typesNames = new LinkedList<String>();
		typesNames.add("TYPE");
		final ValueTO vto = new ValueTO("name", "test_name");

		new Expectations() {
			{
				SimpleType.enumerateAllTypeNames();
				times = 1;
				result = typesNames;

				SimpleType.findType("TYPE");
				times = 1;
				returns(st);

				SimpleType.enumerateAllTypeNames();
				times = 1;
				result = typesNames;

				SimpleType.findType("TYPE");
				times = 1;
				returns(st);

				st.getValue(1, "name", Object.class);
				times = 1;
				returns(vto.getPropertyName());

				st.setPropValue(1, vto);
				times = 1;

				SimpleType.findType("TYPE");
				times = 1;
				returns(st);

				st.getObject(1);
				times = 1;
				returns(so);

				en.notify(withInstanceOf(Event.class));
			}
		};
		c.create();
		c.setEventNotifier(en);
		c.setPropertyValue(to, "name", vto);
	}

	@Test
	@Ignore
	public void testSetPropertyValueAsString(@Mocked SimpleType st, @Mocked EventNotifier en, @Mocked SimpleObject so)
	        throws Exception {
		final TO<?> to = TOFactory.getInstance().loadTO("TYPE", 1);
		final Collection<String> typesNames = new LinkedList<String>();
		typesNames.add("TYPE");
		final String s = "test_name";
		new Expectations() {
			{
				SimpleType.enumerateAllTypeNames();
				times = 1;
				result = typesNames;

				SimpleType.findType("TYPE");
				times = 1;
				returns(st);

				SimpleType.enumerateAllTypeNames();
				times = 1;
				result = typesNames;

				SimpleType.findType("TYPE");
				times = 1;
				returns(st);

				st.getValue(1, "name", Object.class);
				times = 1;
				returns(s);

				st.setPropValue(1, withInstanceOf(ValueTO.class));
				times = 1;

				SimpleType.findType("TYPE");
				times = 1;
				returns(st);

				st.getObject(1);
				times = 1;
				returns(so);

				en.notify(withInstanceOf(Event.class));
			}
		};
		c.create();
		c.setEventNotifier(en);
		c.setPropertyValue(to, "name", s);
	}
}
