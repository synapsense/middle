package com.synapsense.dal.objectcache;

import com.synapsense.dto.CollectionTO;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.service.impl.nsvc.EventNotifier;
import com.synapsense.service.nsvc.Event;
import com.synapsense.util.Pair;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;
import mockit.Mock;
import mockit.MockUp;
import mockit.Mocked;
import mockit.StrictExpectations;
import mockit.integration.junit4.JMockit;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;


@RunWith(JMockit.class)
public class CacheObjectRelationsTest {
	
	final static class MockedCacheSvcImpl extends MockUp<CacheSvcImpl> {
		@Mock
		public void start(){}
	}

	static CacheSvcImpl c;
	
	@BeforeClass
	public static void se() {
		new MockedCacheSvcImpl();
		c = new CacheSvcImpl();
	}
	

	@Test
	public void testGetChildrenOfCollection(@Mocked SimpleType st, @Mocked SimpleObject so) throws Exception {
		final Collection<TO<?>> objIds = new LinkedList<TO<?>>();
		final TO<?> TO1 = TOFactory.getInstance().loadTO("TYPE", 1);
		final TO<?> TO2 = TOFactory.getInstance().loadTO("TYPE", 2);
		objIds.add(TO1);
		objIds.add(TO2);

		final Collection<CollectionTO> result = new LinkedList<CollectionTO>();
		final TO<?> childTO1 = TOFactory.getInstance().loadTO("CHILD", 1);
		final TO<?> childTO2 = TOFactory.getInstance().loadTO("CHILD", 2);
		final CollectionTO cTO1 = new CollectionTO(TO1);
		final CollectionTO cTO2 = new CollectionTO(TO2);
		cTO1.addValue(new ValueTO(childTO1.getID().toString(), childTO1));
		cTO2.addValue(new ValueTO(childTO2.getID().toString(), childTO2));
		result.add(cTO1);
		result.add(cTO2);

		final Set<Pair<String, Integer>> children1 = new HashSet<Pair<String, Integer>>(0);
		children1.add(new Pair<String, Integer>("CHILD", 1));
		final Set<Pair<String, Integer>> children2 = new HashSet<Pair<String, Integer>>(0);
		children2.add(new Pair<String, Integer>("CHILD", 2));

		new StrictExpectations() {
			{

				SimpleType.findType("TYPE");
				times = 1;
				returns(st);

				st.getObject(1);
				times = 1;
				returns(so);

				so.getChildren();
				times = 1;
				returns(children1);

				// ****************

				SimpleType.findType("TYPE");
				times = 1;
				returns(st);

				st.getObject(2);
				times = 1;
				returns(so);

				so.getChildren();
				times = 1;
				returns(children2);

			}
		};
		assertEquals(result, c.getChildren(objIds, "CHILD"));
	}

	@Test
	public void testGetChildrenOfObjectNotInCache(@Mocked SimpleType st) throws Exception {
		final Collection<TO<?>> objIds = new LinkedList<TO<?>>();
		final TO<?> TO1 = TOFactory.getInstance().loadTO("TYPE", 1);
		final TO<?> TO2 = TOFactory.getInstance().loadTO("TYPE", 2);
		objIds.add(TO1);
		objIds.add(TO2);

		final Collection<CollectionTO> result = new LinkedList<CollectionTO>();
		final CollectionTO cTO1 = new CollectionTO(TO1);
		final CollectionTO cTO2 = new CollectionTO(TO2);
		result.add(cTO1);
		result.add(cTO2);

		new StrictExpectations() {
			{

				SimpleType.findType("TYPE");
				times = 1;
				returns(st);

				st.getObject(1);
				times = 1;
				returns(null);

				// ****************

				SimpleType.findType("TYPE");
				times = 1;
				returns(st);

				st.getObject(2);
				times = 1;
				returns(null);
			}
		};
		assertEquals(result, c.getChildren(objIds, "CHILD"));
	}

	@Test
	public void testGetChildrenOfCollectionFiltered(@Mocked SimpleType st,@Mocked  final SimpleObject so) throws Exception {
		final Collection<TO<?>> objIds = new LinkedList<TO<?>>();
		final TO<?> TO1 = TOFactory.getInstance().loadTO("TYPE", 1);
		final TO<?> TO2 = TOFactory.getInstance().loadTO("TYPE", 2);
		objIds.add(TO1);
		objIds.add(TO2);

		final Collection<CollectionTO> result = new LinkedList<CollectionTO>();
		final TO<?> childTO1 = TOFactory.getInstance().loadTO("CHILD", 1);
		final CollectionTO cTO1 = new CollectionTO(TO1);
		final CollectionTO cTO2 = new CollectionTO(TO2);
		cTO1.addValue(new ValueTO(childTO1.getID().toString(), childTO1));
		result.add(cTO1);
		result.add(cTO2);

		final Set<Pair<String, Integer>> children1 = new HashSet<Pair<String, Integer>>(0);
		children1.add(new Pair<String, Integer>("CHILD", 1));
		final Set<Pair<String, Integer>> children2 = new HashSet<Pair<String, Integer>>(0);
		children2.add(new Pair<String, Integer>("BAD_CHILD", 2));

		new StrictExpectations() {
			{

				SimpleType.findType("TYPE");
				times = 1;
				returns(st);

				st.getObject(1);
				times = 1;
				returns(so);

				so.getChildren();
				times = 1;
				returns(children1);

				// ****************

				SimpleType.findType("TYPE");
				times = 1;
				returns(st);

				st.getObject(2);
				times = 1;
				returns(so);

				so.getChildren();
				times = 1;
				returns(children2);

			}
		};
		assertEquals(result, c.getChildren(objIds, "CHILD"));
	}

	@Test
	public void testGetChildren(@Mocked SimpleType st, @Mocked SimpleObject so) throws Exception {
		final TO<?> TO = TOFactory.getInstance().loadTO("TYPE", 1);
		final Collection<TO<?>> result = new LinkedList<TO<?>>();
		final TO<?> childTO = TOFactory.getInstance().loadTO("CHILD", 1);
		result.add(childTO);

		final Set<Pair<String, Integer>> children = new HashSet<Pair<String, Integer>>(0);
		children.add(new Pair<String, Integer>("CHILD", 1));

		new StrictExpectations() {
			{
				SimpleType.findType("TYPE");
				times = 1;
				returns(st);

				st.getObject(1);
				times = 1;
				returns(so);

				SimpleType.findType("TYPE");
				times = 1;
				returns(st);

				st.getObject(1);
				times = 1;
				returns(so);

				so.getChildren();
				times = 1;
				returns(children);

			}
		};
		assertEquals(result, c.getChildren(TO, "CHILD"));
	}

	@Test(expected = ObjectNotFoundException.class)
	public void testGetChildrenOfNonExistObject(@Mocked SimpleType st) throws Exception {
		final TO<?> TO = TOFactory.getInstance().loadTO("TYPE", 1);

		new StrictExpectations() {
			{
				SimpleType.findType("TYPE");
				times = 1;
				returns(st);

				st.getObject(1);
				times = 1;
				returns(null);

			}
		};
		c.getChildren(TO, "CHILD");
	}

	@Test
	public void testGetParents(@Mocked SimpleType st, @Mocked SimpleObject so) throws Exception {
		final TO<?> TO1 = TOFactory.getInstance().loadTO("TYPE", 1);
		final Set<Pair<String, Integer>> parents = new HashSet<Pair<String, Integer>>(0);
		parents.add(new Pair<String, Integer>("PARENT", 1));
		parents.add(new Pair<String, Integer>("PARENT", 2));

		final Collection<TO<?>> cto = new LinkedList<TO<?>>();
		cto.add(TOFactory.getInstance().loadTO("PARENT", 2));
		cto.add(TOFactory.getInstance().loadTO("PARENT", 1));

		new StrictExpectations() {
			{
				SimpleType.findType("TYPE");
				times = 1;
				returns(st);

				st.getObject(1);
				times = 1;
				returns(so);

				so.getParents();
				times = 1;
				returns(parents);

			}
		};
		assertEquals(cto, c.getParents(TO1, "PARENT"));
	}

	@Test(expected = ObjectNotFoundException.class)
	public void testGetParentsOfNonExistObject(@Mocked SimpleType st) throws Exception {
		final TO<?> TO1 = TOFactory.getInstance().loadTO("TYPE", 1);

		new StrictExpectations() {
			{
				SimpleType.findType("TYPE");
				times = 1;
				returns(st);

				st.getObject(1);
				times = 1;
				returns(null);
			}
		};
		c.getParents(TO1, "PARENT");
	}

	@Test
	@Ignore
	// The cascading issue, see https://rtc-prod.panduit.com/ccm/resource/itemName/com.ibm.team.workitem.WorkItem/31794
	public void testGetRelatedChildrenObjects(@Mocked SimpleType st, @Mocked SimpleObject so) throws Exception {
		final TO<?> parent = TOFactory.getInstance().loadTO("PARENT", 1);
		final TO<?> child1 = TOFactory.getInstance().loadTO("CHILD", 1);
		final TO<?> child2 = TOFactory.getInstance().loadTO("CHILD", 2);
		final TO<?> child3 = TOFactory.getInstance().loadTO("CHILD", 3);
		c.setRelation(parent, child1);
		c.setRelation(parent, child2);
		c.setRelation(child2, child3);

		final Collection<TO<?>> result = new HashSet<TO<?>>();
		result.add(child1);
		result.add(child2);
		result.add(child3);

		final Set<Pair<String, Integer>> children1 = new HashSet<Pair<String, Integer>>(0);
		children1.add(new Pair<String, Integer>("CHILD", 1));
		children1.add(new Pair<String, Integer>("CHILD", 2));

		final Set<Pair<String, Integer>> children2 = new HashSet<Pair<String, Integer>>(0);
		children2.add(new Pair<String, Integer>("CHILD", 3));

		new StrictExpectations() {
			{
				SimpleType.findType("PARENT");
				times = 1;
				returns(st);

				st.getObject(1);
				times = 1;
				returns(so);

				SimpleType.findType("PARENT");
				times = 1;
				returns(st);

				st.getObject(1);
				times = 1;
				returns(so);

				so.getChildren();
				times = 1;
				returns(children1);

				SimpleType.findType("CHILD");
				times = 1;
				returns(st);

				st.getObject(2);
				times = 1;
				returns(so);

				SimpleType.findType("CHILD");
				times = 1;
				returns(st);

				st.getObject(2);
				times = 1;
				returns(so);

				so.getChildren();
				times = 1;
				returns(children2);

				SimpleType.findType("CHILD");
				times = 1;
				returns(st);

				st.getObject(1);
				times = 1;
				returns(so);

				SimpleType.findType("CHILD");
				times = 1;
				returns(st);

				st.getObject(1);
				times = 1;
				returns(so);

				so.getChildren();
				times = 1;
				returns(new HashSet<Pair<String, Integer>>(0));
			}
		};

		assertEquals(result, c.getRelatedObjects(parent, 2, true));
	}

	@Test
	@Ignore
	// The cascading issue, see https://rtc-prod.panduit.com/ccm/resource/itemName/com.ibm.team.workitem.WorkItem/31794
	public void testGetRelatedParentsObjects(@Mocked SimpleType st, @Mocked SimpleObject so) throws Exception {
		final TO<?> child = TOFactory.getInstance().loadTO("CHILD", 1);
		final TO<?> parent1 = TOFactory.getInstance().loadTO("PARENT", 1);
		final TO<?> parent2 = TOFactory.getInstance().loadTO("PARENT", 2);
		final TO<?> parent3 = TOFactory.getInstance().loadTO("PARENT", 3);
		c.setRelation(child, parent1);
		c.setRelation(child, parent2);
		c.setRelation(parent2, parent3);

		final Collection<TO<?>> result = new HashSet<TO<?>>();
		result.add(parent1);
		result.add(parent2);
		result.add(parent3);

		final Set<Pair<String, Integer>> parents1 = new HashSet<Pair<String, Integer>>(0);
		parents1.add(new Pair<String, Integer>("PARENT", 1));
		parents1.add(new Pair<String, Integer>("PARENT", 2));

		final Set<Pair<String, Integer>> parents2 = new HashSet<Pair<String, Integer>>(0);
		parents2.add(new Pair<String, Integer>("PARENT", 3));

		new StrictExpectations() {
			{
				SimpleType.findType("CHILD");
				times = 1;
				returns(st);

				st.getObject(1);
				times = 1;
				returns(so);

				so.getParents();
				times = 1;
				returns(parents1);

				SimpleType.findType("PARENT");
				times = 1;
				returns(st);

				st.getObject(1);
				times = 1;
				returns(so);

				so.getParents();
				times = 1;
				returns(parents2);

				SimpleType.findType("PARENT");
				times = 1;
				returns(st);

				st.getObject(2);
				times = 1;
				returns(so);

				so.getParents();
				times = 1;
				returns(new HashSet<Pair<String, Integer>>(0));
			}
		};

		assertEquals(result, c.getRelatedObjects(child, 2, false));
	}

	@Test
	@Ignore
	// The cascading issue, see https://rtc-prod.panduit.com/ccm/resource/itemName/com.ibm.team.workitem.WorkItem/31794
	public void testGetRelatedObjectsFiltered(@Mocked SimpleType st, @Mocked SimpleObject so) throws Exception {
		final TO<?> parent = TOFactory.getInstance().loadTO("PARENT", 1);
		final TO<?> child1 = TOFactory.getInstance().loadTO("CHILD", 1);
		final TO<?> child2 = TOFactory.getInstance().loadTO("CHILD", 2);
		final TO<?> child3 = TOFactory.getInstance().loadTO("BAD_CHILD", 3);
		c.setRelation(parent, child1);
		c.setRelation(parent, child2);
		c.setRelation(child2, child3);

		final Collection<TO<?>> result = new LinkedList<TO<?>>();
		result.add(child2);
		result.add(child1);

		final Set<Pair<String, Integer>> children1 = new HashSet<Pair<String, Integer>>(0);
		children1.add(new Pair<String, Integer>("CHILD", 1));
		children1.add(new Pair<String, Integer>("CHILD", 2));

		final Set<Pair<String, Integer>> children2 = new HashSet<Pair<String, Integer>>(0);
		children2.add(new Pair<String, Integer>("BAD_CHILD", 3));

		new StrictExpectations() {
			{
				SimpleType.findType("PARENT");
				times = 1;
				returns(st);

				st.getObject(1);
				times = 1;
				returns(so);

				SimpleType.findType("PARENT");
				times = 1;
				returns(st);

				st.getObject(1);
				times = 1;
				returns(so);

				so.getChildren();
				times = 1;
				returns(children1);

				SimpleType.findType("CHILD");
				times = 1;
				returns(st);

				st.getObject(2);
				times = 1;
				returns(so);

				SimpleType.findType("CHILD");
				times = 1;
				returns(st);

				st.getObject(2);
				times = 1;
				returns(so);

				so.getChildren();
				times = 1;
				returns(children2);

				SimpleType.findType("CHILD");
				times = 1;
				returns(st);

				st.getObject(1);
				times = 1;
				returns(so);

				SimpleType.findType("CHILD");
				times = 1;
				returns(st);

				st.getObject(1);
				times = 1;
				returns(so);

				so.getChildren();
				times = 1;
				returns(new HashSet<Pair<String, Integer>>(0));
			}
		};

		assertEquals(result, c.getRelatedObjects(parent, "CHILD", true));
	}

	@Test
	public void testGetRelatedObjectsOfNonExistChild(@Mocked SimpleType st) throws Exception {
		final TO<?> parent = TOFactory.getInstance().loadTO("PARENT", 1);
		final Collection<TO<?>> result = new HashSet<TO<?>>();

		new StrictExpectations() {
			{
				SimpleType.findType("PARENT");
				times = 1;
				returns(st);

				st.getObject(1);
				times = 1;
				returns(null);

			}
		};
		assertEquals(result, c.getRelatedObjects(parent, 2, true));
	}

	@Test
	public void testGetRelatedObjectsOfNonExistParent(@Mocked SimpleType st) throws Exception {
		final TO<?> child = TOFactory.getInstance().loadTO("CHILD", 1);
		final Collection<TO<?>> result = new HashSet<TO<?>>();

		new StrictExpectations() {
			{
				SimpleType.findType("CHILD");
				times = 1;
				returns(st);

				st.getObject(1);
				times = 1;
				returns(null);

			}
		};
		assertEquals(result, c.getRelatedObjects(child, 2, false));
	}

	@Test
	@Ignore
	public void testSetRelation(@Mocked SimpleType st, @Mocked SimpleObject so, @Mocked EventNotifier en) throws Exception {
		final TO<?> parent = TOFactory.getInstance().loadTO("PARENT", 1);
		final TO<?> child = TOFactory.getInstance().loadTO("CHILD", 2);
		final Set<Pair<String, Integer>> parents = new HashSet<Pair<String, Integer>>();
		final Set<Pair<String, Integer>> children = new HashSet<Pair<String, Integer>>();
		final Collection<String> typesNames = new LinkedList<String>();
		typesNames.add("PARENT");
		typesNames.add("CHILD");

		new StrictExpectations() {
			{
				SimpleType.findType("PARENT");
				times = 1;
				returns(st);

				st.getObject(1);
				times = 1;
				returns(so);

				SimpleType.findType("CHILD");
				times = 1;
				returns(st);

				st.getObject(2);
				times = 1;
				returns(so);

				so.getParents();
				times = 1;
				returns(parents);

				so.getChildren();
				times = 1;
				returns(children);

				SimpleType.findType("PARENT");
				times = 1;
				returns(st);

				st.getObject(1);
				times = 1;
				returns(so);

				so.getParents();
				times = 1;
				returns(new HashSet<Pair<String, Integer>>(0));

				SimpleType.findType("PARENT");
				times = 1;
				returns(st);

				st.getObject(1);
				times = 1;
				returns(so);

				SimpleType.findType("PARENT");
				times = 1;
				returns(st);

				st.getObject(1);
				times = 1;
				returns(so);

				so.getChildren();
				times = 1;
				returns(children);

				SimpleType.findType("CHILD");
				times = 1;
				returns(st);

				st.getObject(2);
				times = 1;
				returns(so);

				SimpleType.findType("CHILD");
				times = 1;
				returns(st);

				st.getObject(2);
				times = 1;
				returns(so);

				so.getChildren();
				times = 1;
				returns(new HashSet<Pair<String, Integer>>(0));

				SimpleType.enumerateAllTypeNames();
				times = 1;
				returns(typesNames);

				SimpleType.findType("PARENT");
				times = 1;
				returns(st);

				SimpleType.enumerateAllTypeNames();
				times = 1;
				returns(typesNames);

				SimpleType.findType("PARENT");
				times = 1;
				returns(st);

				st.getValue((Integer) parent.getID(), "numAlerts", Object.class);
				times = 1;
				returns(0);

				st.setPropValue((Integer) parent.getID(), withInstanceOf(ValueTO.class));
				times = 1;

				SimpleType.findType("PARENT");
				times = 1;
				returns(st);

				st.getObject(1);
				times = 1;
				returns(so);

				en.notify(withInstanceOf(Event.class));
				times = 2;
			}
		};
		c.setEventNotifier(en);
		c.create();
		c.setRelation(parent, child);
	}

	@Test
	public void testSetRelationWithNullParentObject(@Mocked SimpleType st, @Mocked SimpleObject so) throws Exception {
		final TO<?> parent = TOFactory.getInstance().loadTO("PARENT", 1);
		final TO<?> child = TOFactory.getInstance().loadTO("CHILD", 2);

		new StrictExpectations() {
			{
				SimpleType.findType("PARENT");
				times = 1;
				returns(st);

				st.getObject(1);
				times = 1;
				returns(null);

				SimpleType.findType("CHILD");
				times = 1;
				returns(st);

				st.getObject(2);
				times = 1;
				returns(so);
			}
		};

		c.create();
		c.setRelation(parent, child);
	}

	@Test
	public void testSetRelationWithNullChildObject(@Mocked SimpleType st, @Mocked SimpleObject so) throws Exception {
		final TO<?> parent = TOFactory.getInstance().loadTO("PARENT", 1);
		final TO<?> child = TOFactory.getInstance().loadTO("CHILD", 2);

		new StrictExpectations() {
			{
				SimpleType.findType("PARENT");
				times = 1;
				returns(st);

				st.getObject(1);
				times = 1;
				returns(so);

				SimpleType.findType("CHILD");
				times = 1;
				returns(st);

				st.getObject(2);
				times = 1;
				returns(null);
			}
		};

		c.create();
		c.setRelation(parent, child);
	}

	@Test
	@Ignore
	public void testRemoveRelation(@Mocked SimpleType st, @Mocked SimpleObject so, @Mocked EventNotifier en) throws Exception {
		final TO<?> parent = TOFactory.getInstance().loadTO("PARENT", 1);
		final TO<?> child = TOFactory.getInstance().loadTO("CHILD", 2);
		final Set<Pair<String, Integer>> parents = new HashSet<Pair<String, Integer>>();
		final Set<Pair<String, Integer>> children = new HashSet<Pair<String, Integer>>();
		final Collection<String> typesNames = new LinkedList<String>();
		typesNames.add("PARENT");
		typesNames.add("CHILD");

		new StrictExpectations() {
			{
				SimpleType.findType("PARENT");
				times = 1;
				returns(st);

				st.getObject(1);
				times = 1;
				returns(so);

				SimpleType.findType("CHILD");
				times = 1;
				returns(st);

				st.getObject(2);
				times = 1;
				returns(so);

				so.getChildren();
				times = 1;
				returns(children);

				so.getParents();
				times = 1;
				returns(parents);

				SimpleType.findType("PARENT");
				times = 1;
				returns(st);

				st.getObject(1);
				times = 1;
				returns(so);

				so.getParents();
				times = 1;
				returns(new HashSet<Pair<String, Integer>>(0));

				SimpleType.findType("PARENT");
				times = 1;
				returns(st);

				st.getObject(1);
				times = 1;
				returns(so);

				SimpleType.findType("PARENT");
				times = 1;
				returns(st);

				st.getObject(1);
				times = 1;
				returns(so);

				so.getChildren();
				times = 1;
				returns(children);

				SimpleType.enumerateAllTypeNames();
				times = 1;
				returns(typesNames);

				SimpleType.findType("PARENT");
				times = 1;
				returns(st);

				SimpleType.enumerateAllTypeNames();
				times = 1;
				returns(typesNames);

				SimpleType.findType("PARENT");
				times = 1;
				returns(st);

				st.getValue((Integer) parent.getID(), "numAlerts", Object.class);
				times = 1;
				returns(0);

				st.setPropValue((Integer) parent.getID(), withInstanceOf(ValueTO.class));
				times = 1;

				SimpleType.findType("PARENT");
				times = 1;
				returns(st);

				st.getObject(1);
				times = 1;
				returns(so);

				en.notify(withInstanceOf(Event.class));
				times = 2;
			}
		};

		c.setEventNotifier(en);
		c.create();
		c.removeRelation(parent, child);
	}

	@Test
	public void testRemoveRelationWithNullParentObject(@Mocked SimpleType st, @Mocked SimpleObject so) throws Exception {
		final TO<?> parent = TOFactory.getInstance().loadTO("PARENT", 1);
		final TO<?> child = TOFactory.getInstance().loadTO("CHILD", 2);

		new StrictExpectations() {
			{
				SimpleType.findType("PARENT");
				times = 1;
				returns(st);

				st.getObject(1);
				times = 1;
				returns(null);

				SimpleType.findType("CHILD");
				times = 1;
				returns(st);

				st.getObject(2);
				times = 1;
				returns(so);
			}
		};

		c.create();
		c.removeRelation(parent, child);
	}

	@Test
	public void testRemoveRelationWithNullChildObject(@Mocked SimpleType st, @Mocked SimpleObject so) throws Exception {
		final TO<?> parent = TOFactory.getInstance().loadTO("PARENT", 1);
		final TO<?> child = TOFactory.getInstance().loadTO("CHILD", 2);

		new StrictExpectations() {
			{
				SimpleType.findType("PARENT");
				times = 1;
				returns(st);

				st.getObject(1);
				times = 1;
				returns(so);

				SimpleType.findType("CHILD");
				times = 1;
				returns(st);

				st.getObject(2);
				times = 1;
				returns(null);
			}
		};

		c.create();
		c.removeRelation(parent, child);
	}
}
