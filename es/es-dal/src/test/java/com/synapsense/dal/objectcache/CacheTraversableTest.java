package com.synapsense.dal.objectcache;

import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.EnvException;
import com.synapsense.search.*;
import com.synapsense.service.impl.dao.to.PropertyTO;
import com.synapsense.util.CollectionUtils;
import com.synapsense.util.Pair;
import org.jmock.Expectations;
import org.junit.Test;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class CacheTraversableTest extends AbstractTest {

	private CacheSvcImpl cache = mock(CacheSvcImpl.class);

	@Test
	public void traversePathNoLinks() {
		final Map<TO<?>, Set<Pair<TO<?>, String>>> links = mock(Map.class);
		final Element e = mock(Element.class);
		final Path path = mock(Path.class);

		final TO<?> startElem = mock(TO.class);
		final Set<PropertyTO> pLinks = mock(Set.class);

		Traversable t = new NonRepeatableTraversable(new CacheTraversable(cache));

		checking(new Expectations() {
			{
				one(e).getValue(TO.class);
				will(returnValue(startElem));

				one(path).getDirection();
				will(returnValue(Direction.INCOMING));

                one(cache).getLinksToObject(startElem);
                will(returnValue(pLinks));

				one(pLinks).iterator();
			}
		});

		Collection<Element> leafs = t.traverseNext(e, path);
		assertTrue(leafs.isEmpty());
	}

	@Test
	public void traverseIncomingAnyPathOneLevelDepthNoRepeatableElements() {

		final TO<?> rack1 = TO_FACTORY.loadTO("RACK:2");
		final TO<?> rack2 = TO_FACTORY.loadTO("RACK:3");
		final TO<?> sensor1 = TO_FACTORY.loadTO("SENSOR:4");

		final Map<TO<?>, Set<PropertyTO>> links = CollectionUtils.newMap();

		final Set<PropertyTO> sensor1Links = CollectionUtils.newSet();
		sensor1Links.add(new PropertyTO(rack1, "cTop"));
		sensor1Links.add(new PropertyTO(rack1, "cBottom"));
		sensor1Links.add(new PropertyTO(rack2, "cBottom"));
		links.put(sensor1, sensor1Links);

		final Element startElem = new EnvElement(sensor1);

		final Path anyPath = mock(Path.class);

		Traversable t = new NonRepeatableTraversable(new CacheTraversable(cache));

		checking(new Expectations() {
			{
				one(anyPath).getDirection();
				will(returnValue(Direction.INCOMING));

                one(cache).getLinksToObject(sensor1);
                will(returnValue(sensor1Links));

				one(anyPath).isPossibleToMove("cTop");
				will(returnValue(true));
				one(anyPath).isPossibleToMove("cBottom");
				will(returnValue(true));
				one(anyPath).isPossibleToMove("cBottom");
				will(returnValue(true));
			}
		});

		Collection<Element> nodes = t.traverseNext(startElem, anyPath);
		assertEquals(2, nodes.size());
		assertTrue(nodes.contains(new EnvElement(rack1)));
		assertTrue(nodes.contains(new EnvElement(rack2)));

	}

	@Test
	public void traverseOutgoingAnyPathOneLevelDepthNoRepeatableElements() throws EnvException {

		final TO<?> rack1 = TO_FACTORY.loadTO("RACK:2");
		final TO<?> rack2 = TO_FACTORY.loadTO("RACK:3");
		final TO<?> sensor1 = TO_FACTORY.loadTO("SENSOR:4");

		final Map<TO<?>, Set<Pair<TO<?>, String>>> links = CollectionUtils.newMap();

		final Set<Pair<TO<?>, String>> sensor1Links = CollectionUtils.newSet();
		sensor1Links.add(Pair.<TO<?>, String> newPair(rack1, "cTop"));
		sensor1Links.add(Pair.<TO<?>, String> newPair(rack2, "cBottom"));
		links.put(sensor1, sensor1Links);

		final Collection<ValueTO> rack1Values = CollectionUtils.newList();
		rack1Values.add(new ValueTO("cTop", sensor1));
		rack1Values.add(new ValueTO("name", "Rack_1"));

		final Element startElem = new EnvElement(rack1);
		final Path path = mock(Path.class);

		Traversable t = new NonRepeatableTraversable(new CacheTraversable(cache));

		checking(new Expectations() {
			{
				one(path).getDirection();
				will(returnValue(Direction.OUTGOING));

				one(cache).getAllPropertiesValues(rack1);
				will(returnValue(rack1Values));

				one(path).isPossibleToMove("cTop");
				will(returnValue(true));
			}
		});

		Collection<Element> nodes = t.traverseNext(startElem, path);
		assertEquals(1, nodes.size());
		assertTrue(nodes.contains(new EnvElement(sensor1)));

	}

	@Test
	public void traversePathIncomingDirectionIgnoringLoops() throws EnvException {

		final TO<?> rack1 = TO_FACTORY.loadTO("RACK:1");
		final TO<?> rack2 = TO_FACTORY.loadTO("RACK:2");

		final Map<TO<?>, Set<PropertyTO>> links = CollectionUtils.newMap();

		final Set<PropertyTO> rack1Links = CollectionUtils.newSet();
		rack1Links.add(new PropertyTO(rack1, "toRack"));
		links.put(rack2, rack1Links);

		final Set<PropertyTO> rack2Links = CollectionUtils.newSet();
		rack2Links.add(new PropertyTO(rack2, "toRack"));
		links.put(rack1, rack2Links);

		final Element startElem = new EnvElement(rack1);
		final Path path = mock(Path.class);

		Traversable t = new NonRepeatableTraversable(new CacheTraversable(cache));

		checking(new Expectations() {
			{
				one(path).getDirection();
				will(returnValue(Direction.INCOMING));

				one(cache).getLinksToObject(rack1);
				will(returnValue(rack2Links));

				one(path).isPossibleToMove("toRack");
				will(returnValue(true));

				one(path).getDirection();
				will(returnValue(Direction.INCOMING));

				one(cache).getLinksToObject(rack2);
				will(returnValue(rack1Links));

				one(path).isPossibleToMove("toRack");
				will(returnValue(true));
			}
		});

		// do one step , will get rack2 is referencing rack1, visited rack2
		Collection<Element> nodes = t.traverseNext(startElem, path);
		assertEquals(1, nodes.size());
		assertTrue(nodes.contains(new EnvElement(rack2)));

		// do next step, will get that rack1 is referencing rack2
		Collection<Element> nodesNext = t.traverseNext(new EnvElement(rack2), path);
		assertEquals(1, nodesNext.size());
		assertTrue(nodesNext.contains(new EnvElement(rack1)));

		// on this step we have visited both racks so break infinite loop
		Collection<Element> nodesLooping = t.traverseNext(new EnvElement(rack1), path);
		assertEquals(0, nodesLooping.size());
	}

}
