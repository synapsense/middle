package com.synapsense.dal.objectcache;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

import mockit.Expectations;
import mockit.Mock;
import mockit.MockUp;

import mockit.Mocked;
import mockit.integration.junit4.JMockit;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.synapsense.dto.ObjectType;
import com.synapsense.dto.ObjectTypeMatchMode;
import com.synapsense.dto.PropertyDescr;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.IllegalInputParameterException;
import com.synapsense.exception.PropertyNotFoundException;
import com.synapsense.service.impl.dao.to.EnvObjTypeTO;
import com.synapsense.service.impl.nsvc.EventNotifier;
import com.synapsense.service.nsvc.Event;
import com.synapsense.service.nsvc.events.ObjectTypeUpdatedEvent;
import com.synapsense.util.Pair;
import org.junit.runner.RunWith;

@RunWith(JMockit.class)
public class CacheObjectsTest {

	final static class MockedCacheSvcImpl extends MockUp<CacheSvcImpl> {
		@Mock
		public void start(){}
	}

	static CacheSvcImpl c;
	
	@BeforeClass
	public static void se() {
		new MockedCacheSvcImpl();
		c = new CacheSvcImpl();
	}

	@Test
	public void testGetObject(@Mocked SimpleType st) throws Exception {
		final ValueTO[] vto = new ValueTO[2];
		vto[0] = new ValueTO("name", "test_name");
		vto[1] = new ValueTO("value", 100);

		final Collection<String> typesNames = new LinkedList<String>();
		typesNames.add("TYPE");

		final TO<?> to1 = TOFactory.getInstance().loadTO("TYPE", 1);
		final TO<?> to2 = TOFactory.getInstance().loadTO("TYPE", 2);

		final Collection<TO<?>> ret = new LinkedList<TO<?>>();
		ret.add(to1);
		ret.add(to2);

		final Collection<TO<?>> result = new LinkedList<TO<?>>();
		result.add(to1);
		result.add(to2);

		new Expectations() {
			{
				SimpleType.enumerateAllTypeNames();
				times = 1;
				returns(typesNames);

				SimpleType.findType("TYPE");
				times = 1;
				returns(st);

				st.find(vto);
				times = 1;
				returns(ret);
			}
		};
		assertEquals(result, c.getObjects("TYPE", vto));
	}

	@Test
	public void testGetObjectsByType(@Mocked SimpleType st) throws Exception {
		final Collection<String> typesNames = new LinkedList<String>();
		typesNames.add("TYPE");

		final Set<Integer> keys = new HashSet<Integer>();
		keys.add(1);
		keys.add(2);

		final TO<?> to1 = TOFactory.getInstance().loadTO("TYPE", 1);
		final TO<?> to2 = TOFactory.getInstance().loadTO("TYPE", 2);

		final Collection<TO<?>> result = new LinkedList<TO<?>>();
		result.add(to1);
		result.add(to2);

		new Expectations() {
			{
				SimpleType.enumerateAllTypeNames();
				times = 1;
				returns(typesNames);

				SimpleType.findType("TYPE");
				times = 1;
				returns(st);

				st.getAllObjects();
				times = 1;
				returns(keys);

				st.getName();
				times = 2;
				returns("TYPE");
			}
		};
		assertEquals(result, c.getObjectsByType("TYPE", ObjectTypeMatchMode.DEFAULT));
	}

	@Test
	public void testGetObjectsByTypeWithNotFindType(@Mocked SimpleType st) throws Exception {
		final Collection<String> typesNames = new LinkedList<String>();
		typesNames.add("TYPE");

		final Collection<TO<?>> result = new LinkedList<TO<?>>();

		new Expectations() {
			{
				SimpleType.enumerateAllTypeNames();
				times = 1;
				returns(typesNames);

				SimpleType.findType("TYPE");
				times = 1;
				returns(null);
			}
		};
		assertEquals(result, c.getObjectsByType("TYPE", ObjectTypeMatchMode.DEFAULT));
	}

	@Test
	@Ignore
	public void testGetObjectType(@Mocked SimpleType st, @Mocked ObjectType ot) throws Exception {
		final String[] names = new String[1];
		names[0] = "TEST_TYPE";

		final Collection<String> test_type = new LinkedList<String>();
		test_type.add(names[0]);

		final Set<PropertyDescr> pds = new HashSet<PropertyDescr>();

		final ObjectType[] ots = new ObjectType[1];
		ots[0] = new EnvObjTypeTO(names[0], "name");

		new Expectations() {
			{
				SimpleType.enumerateAllTypeNames();
				times = 1;
				returns(test_type);

				SimpleType.findType(names[0]);
				times = 1;
				returns(st);

				st.getEnvType();
				times = 1;
				returns(ot);

				ot.getName();
				times = 1;
				returns(names[0]);

				ot.getPropertyDescriptors();
				times = 1;
				returns(pds);
			}
		};
		ObjectType[] result = c.getObjectTypes(names);
		assertEquals(ots[0].getName(), result[0].getName());
		assertEquals(ots[0].getPropertyDescriptors(), result[0].getPropertyDescriptors());
	}

	@Test
	@Ignore
	public void testExistObject(@Mocked SimpleType st) throws Exception {
		final TO<?> to = TOFactory.getInstance().loadTO("TYPE", 1);
		final Collection<String> typesNames = new LinkedList<String>();
		typesNames.add("TYPE");

		new Expectations() {
			{
				SimpleType.enumerateAllTypeNames();
				times = 1;
				returns(typesNames);

				SimpleType.findType("TYPE");
				times = 1;
				returns(st);

				st.exists(to.getID());
				times = 1;
				returns(true);
			}
		};

		assertTrue(c.exists(to));
	}

	@Test
	@Ignore
	public void testExistObjectOfNonExistType(@Mocked SimpleType st) throws Exception {
		final TO<?> to = TOFactory.getInstance().loadTO("TYPE", 1);
		final Collection<String> typesNames = new LinkedList<String>();
		typesNames.add("TYPE");

		new Expectations() {
			{
				SimpleType.enumerateAllTypeNames();
				times = 1;
				returns(typesNames);

				SimpleType.findType("TYPE");
				times = 1;
				returns(null);
			}
		};

		assertFalse(c.exists(to));
	}

	@Test(expected = IllegalInputParameterException.class)
	public void testExistObjectWithNullObject() throws Exception {
		final TO<?> to = null;

		c.exists(to);
	}

	@Test(expected = IllegalInputParameterException.class)
	public void testExistObjectWithNullTypeName(@Mocked TO<?> to) throws Exception {
		new Expectations() {
			{
				to.getTypeName();
				times = 1;
				returns(null);
			}
		};
		c.exists(to);
	}

	@Test(expected = IllegalInputParameterException.class)
	public void testExistObjectWithEmptyTypeName(@Mocked TO<?> to) throws Exception {
		new Expectations() {
			{
				to.getTypeName();
				times = 2;
				returns("");
			}
		};
		c.exists(to);
	}

	@Test(expected = IllegalInputParameterException.class)
	public void testExistObjectWithNullId(@Mocked TO<?> to) throws Exception {
		new Expectations() {
			{
				to.getTypeName();
				times = 2;
				returns("TYPE");

				to.getID();
				times = 1;
				returns(null);
			}
		};
		c.exists(to);
	}

	@Test
	@Ignore
	public void testCreateObject(@Mocked SimpleType st, @Mocked SimpleObject so, @Mocked EventNotifier en) throws Exception {
		final TO<?> to = TOFactory.getInstance().loadTO("TYPE", 1);
		final ValueTO[] propertyValues = new ValueTO[1];
		propertyValues[0] = new ValueTO("name", "test_name");

		final Collection<String> typesNames = new LinkedList<String>();
		typesNames.add("TYPE");

		new Expectations() {
			{
				SimpleType.enumerateAllTypeNames();
				times = 1;
				returns(typesNames);

				SimpleType.findType("TYPE");
				times = 1;
				returns(st);

				st.addObject((Integer) to.getID());
				times = 1;
				returns(so);
				
				st.setPropValue((Integer) to.getID(), propertyValues[0]);

				SimpleType.enumerateAllTypeNames();
				times = 1;
				result = typesNames;

				SimpleType.findType("TYPE");
				times = 1;
				returns(st);

				st.setPropValue(1, propertyValues[0]);
				times = 1;

				SimpleType.findType("TYPE");
				times = 1;
				returns(st);

				st.getObject(1);
				times = 1;
				returns(so);

				en.notify(withInstanceOf(Event.class));
				times = 1;
			}
		};

		c.create();
		c.setEventNotifier(en);
		c.createObject(to, propertyValues, true);
	}

	@Test
	@Ignore
	public void testCreateObjectWithNonExistTO(@Mocked SimpleType st, @Mocked SimpleObject so, @Mocked EventNotifier en)
	        throws Exception {
		final TO<?> to = TOFactory.getInstance().loadTO("TYPE", 1);
		final ValueTO[] propertyValues = new ValueTO[1];
		propertyValues[0] = new ValueTO("name", "test_name");

		final Collection<String> typesNames = new LinkedList<String>();
		typesNames.add("TYPE");

		new Expectations() {
			{
				SimpleType.enumerateAllTypeNames();
				times = 1;
				returns(typesNames);

				SimpleType.findType("TYPE");
				times = 1;
				returns(st);

				st.addObject((Integer) to.getID());
				times = 1;
				returns(so);

				SimpleType.enumerateAllTypeNames();returns(typesNames);
				SimpleType.findType("TYPE");returns(st);

                SimpleType.enumerateAllTypeNames();returns(typesNames);
                SimpleType.findType("TYPE");returns(st);

                st.getValue(1, propertyValues[0].getPropertyName(), Object.class);

                st.setPropValue(1, propertyValues[0]);

				SimpleType.findType("TYPE");returns(st);
    			st.getObject(1);returns(so);
				en.notify(withInstanceOf(Event.class));
				times = 1;
			}
		};

		c.create();
		c.setEventNotifier(en);
		c.createObject(to, propertyValues, true);
	}

	@Test(expected = IllegalStateException.class)
	@Ignore
	public void testCreateObjectOfNonExistType(@Mocked SimpleType st) throws Exception {
		final TO<?> to = TOFactory.getInstance().loadTO("TYPE", 1);
		final ValueTO[] propertyValues = new ValueTO[1];
		propertyValues[0] = new ValueTO("name", "test_name");

		final Collection<String> typesNames = new LinkedList<String>();
		typesNames.add("TYPE");

		new Expectations() {
			{
				SimpleType.enumerateAllTypeNames();
				times = 1;
				returns(typesNames);

				SimpleType.findType("TYPE");
				times = 1;
				returns(null);
			}
		};

		c.create();
		c.createObject(to, propertyValues, true);
	}

	@Test
	@Ignore
	public void testCreateAlertObject(@Mocked SimpleType st, @Mocked SimpleObject so, @Mocked EventNotifier en)
	        throws Exception {
		final TO<?> to = TOFactory.getInstance().loadTO("ALERT", 1);
		final ValueTO[] propertyValues = new ValueTO[1];
		propertyValues[0] = new ValueTO("name", "test_name");

		final Collection<String> typesNames = new LinkedList<String>();
		typesNames.add("ALERT");

		final SimplePropertyValue v = new SimplePropertyValue("hostTO", Value.createTypedValue(TOFactory.getInstance()
                .loadTO("hostTO", 1)));

		final Set<Pair<String, Integer>> parents = new HashSet<Pair<String, Integer>>(0);

		new Expectations() {
			{
				SimpleType.enumerateAllTypeNames();
				times = 1;
				returns(typesNames);

				SimpleType.findType("ALERT");
				times = 1;
				returns(st);

				st.addObject((Integer) to.getID());
				times = 1;
				returns(so);

				SimpleType.enumerateAllTypeNames();
				times = 1;
				result = typesNames;

				SimpleType.findType("ALERT");
				times = 1;
				returns(st);

				st.setPropValue(1, propertyValues[0]);
				times = 1;

				SimpleType.findType("ALERT");
				times = 1;
				returns(st);

				st.getObject(1);
				times = 1;
				returns(so);

				so.getPropertyValue("hostTO");
				times = 1;
				returns(v);

				SimpleType.enumerateAllTypeNames();
				times = 1;
				returns(typesNames);

				SimpleType.findType("hostTO");
				times = 1;
				returns(st);

				st.getObject(1);
				times = 1;
				returns(so);

				so.getParents();
				times = 1;
				returns(parents);

				en.notify(withInstanceOf(Event.class));
				times = 1;

			}
		};

		c.create();
		c.setEventNotifier(en);
		c.createObject(to, propertyValues, true);
	}

	@Test
	@Ignore
	public void testCreateAlertObjectWithNonExistProp(@Mocked SimpleType st, @Mocked SimpleObject so, @Mocked EventNotifier en)
	        throws Exception {
		final TO<?> to = TOFactory.getInstance().loadTO("ALERT", 1);
		final ValueTO[] propertyValues = new ValueTO[1];
		propertyValues[0] = new ValueTO("name", "test_name");

		final Collection<String> typesNames = new LinkedList<String>();
		typesNames.add("ALERT");

		final SimplePropertyValue v = new SimplePropertyValue("hostTO", Value.createTypedValue(TOFactory.getInstance()
                .loadTO("hostTO", 1)));

		final Set<Pair<String, Integer>> parents = new HashSet<Pair<String, Integer>>(0);

		new Expectations() {
			{
				SimpleType.enumerateAllTypeNames();
				times = 1;
				returns(typesNames);

				SimpleType.findType("ALERT");
				times = 1;
				returns(st);

				st.addObject((Integer) to.getID());
				times = 1;
				returns(so);
				
				SimpleType.enumerateAllTypeNames();
				times = 1;
				result = typesNames;

				SimpleType.findType("ALERT");
				times = 1;
				returns(st);

				st.setPropValue(1, propertyValues[0]);
				times = 1;

				SimpleType.findType("ALERT");
				times = 1;
				returns(st);

				st.getObject(1);
				times = 1;
				returns(so);

				so.getPropertyValue("hostTO");
				times = 1;
				result = new PropertyNotFoundException("");
			}
		};

		c.create();
		c.setEventNotifier(en);
		c.createObject(to, propertyValues, true);
	}

	@Test
	@Ignore
	public void testCreateObjectType(@Mocked ObjectType type, @Mocked SimpleType st, @Mocked EventNotifier en)
	        throws Exception {
		new Expectations() {
			{
				type.getName();
				times = 1;
				returns("TYPE");

				SimpleType.newType(type, withInstanceOf(IndexProvider.class));
				times = 1;

				en.notify(withInstanceOf(Event.class));
				times = 1;
			}
		};

		c.create();
		c.setEventNotifier(en);
		c.createObjectType(type);
	}

	@Test
	@Ignore
	public void testDeleteObject(@Mocked SimpleType st, @Mocked SimpleObject so, @Mocked EventNotifier en) throws Exception {
		final TO<?> to = TOFactory.getInstance().loadTO("TYPE", 1);

		final Collection<String> typesNames = new LinkedList<String>();
		typesNames.add("TYPE");

		final Set<Pair<String, Integer>> parents = new HashSet<Pair<String, Integer>>(0);
		parents.add(new Pair<String, Integer>("PARENT", 1));

		new Expectations() {
			{
				SimpleType.enumerateAllTypeNames();
				times = 1;
				returns(typesNames);

				SimpleType.findType("TYPE");
				times = 1;
				returns(st);

				SimpleType.findType("TYPE");
				times = 1;
				returns(st);

				st.getObject(1);
				times = 1;
				returns(so);

				so.getParents();
				times = 1;
				returns(parents);

				st.removeObject((Integer) to.getID());
				times = 1;

				SimpleType.findType("PARENT");
				times = 1;
				returns(st);

				st.getObject(1);
				times = 1;
				returns(so);

				so.getParents();
				times = 1;
				returns(new HashSet<Pair<String, Integer>>(0));

				SimpleType.findType("PARENT");
				times = 1;
				returns(st);

				st.getObject(1);
				times = 1;
				returns(so);

				SimpleType.findType("PARENT");
				times = 1;
				returns(st);

				st.getObject(1);
				times = 1;
				returns(so);

				so.getChildren();
				times = 1;
				returns(new HashSet<Pair<String, Integer>>(0));

				SimpleType.enumerateAllTypeNames();
				times = 1;
				returns(typesNames);

				en.notify(withInstanceOf(Event.class));
				times = 1;

			}
		};
		c.create();
		c.setEventNotifier(en);
		c.deleteObject(to);

		/*
		 * try to delete null
		 */
		c.deleteObject(null);
	}

	@Test
	@Ignore
	public void testDeleteAlertObject(@Mocked SimpleType st, @Mocked SimpleObject so, @Mocked EventNotifier en)
	        throws Exception {
		final TO<?> to = TOFactory.getInstance().loadTO("ALERT", 1);
		final Collection<String> typesNames = new LinkedList<String>();
		typesNames.add("ALERT");

		final Collection<String> parentTypesNames = new LinkedList<String>();
		typesNames.add("PARENT");

		final Set<Pair<String, Integer>> parents = new HashSet<Pair<String, Integer>>(0);
		parents.add(new Pair<String, Integer>("PARENT", 2));

		new Expectations() {
			{
				SimpleType.enumerateAllTypeNames();
				times = 1;
				returns(typesNames);

				SimpleType.findType("ALERT");
				times = 1;
				returns(st);

				st.getValue((Integer) to.getID(), "hostTO", TO.class);
				times = 1;
				returns(TOFactory.getInstance().loadTO("hostTO", 1));

				SimpleType.enumerateAllTypeNames();
				times = 1;
				returns(typesNames);

				SimpleType.findType("ALERT");
				times = 1;
				returns(st);

				SimpleType.findType("ALERT");
				times = 1;
				returns(st);

				st.getObject(1);
				times = 1;
				returns(so);

				so.getParents();
				times = 1;
				returns(parents);

				st.removeObject((Integer) to.getID());
				times = 1;

				SimpleType.findType("PARENT");
				times = 1;
				returns(st);

				st.getObject(2);
				times = 1;
				returns(so);

				so.getParents();
				times = 1;
				returns(new HashSet<Pair<String, Integer>>(0));

				SimpleType.findType("PARENT");
				times = 1;
				returns(st);

				st.getObject(2);
				times = 1;
				returns(so);

				SimpleType.findType("PARENT");
				times = 1;
				returns(st);

				st.getObject(2);
				times = 1;
				returns(so);

				so.getChildren();
				times = 1;
				returns(new HashSet<Pair<String, Integer>>(0));

				SimpleType.enumerateAllTypeNames();
				times = 1;
				returns(parentTypesNames);

				en.notify(withInstanceOf(Event.class));
				times = 1;
			}
		};
		c.create();
		c.setEventNotifier(en);
		c.deleteObject(to);
	}

	@Test
	@Ignore
	public void testDeleteAlertObjectWithProblemGetHostTO(@Mocked SimpleType st, @Mocked SimpleObject so,
														  @Mocked EventNotifier en) throws Exception {
		final TO<?> to = TOFactory.getInstance().loadTO("ALERT", 1);
		final Collection<String> typesNames = new LinkedList<String>();
		typesNames.add("ALERT");

		new Expectations() {
			{
				SimpleType.enumerateAllTypeNames();
				times = 1;
				returns(typesNames);

				SimpleType.findType("ALERT");
				times = 1;
				returns(null);

				SimpleType.enumerateAllTypeNames();
				times = 1;
				returns(typesNames);

				SimpleType.findType("ALERT");
				times = 1;
				returns(null);

			}
		};
		c.create();
		c.deleteObject(to);
	}

	@Test
	@Ignore
	public void testDeleteAlertObjectWithDeletedParents(@Mocked SimpleType st, @Mocked SimpleObject so,
														@Mocked EventNotifier en) throws Exception {
		final TO<?> to = TOFactory.getInstance().loadTO("ALERT", 1);
		final Collection<String> typesNames = new LinkedList<String>();
		typesNames.add("ALERT");

		final Set<Pair<String, Integer>> parents = new HashSet<Pair<String, Integer>>(0);
		parents.add(new Pair<String, Integer>("PARENT", 2));

		new Expectations() {
			{
				SimpleType.enumerateAllTypeNames();
				times = 1;
				returns(typesNames);

				SimpleType.findType("ALERT");
				times = 1;
				returns(st);

				st.getValue((Integer) to.getID(), "hostTO", TO.class);
				times = 1;
				returns(TOFactory.getInstance().loadTO("hostTO", 1));

				SimpleType.enumerateAllTypeNames();
				times = 1;
				returns(typesNames);

				SimpleType.findType("ALERT");
				times = 1;
				returns(st);

				SimpleType.findType("ALERT");
				times = 1;
				returns(null);
			}
		};
		c.create();
		c.setEventNotifier(en);
		c.deleteObject(to);
	}

	@Test
	@Ignore
	public void testDeleteObjectType(@Mocked SimpleType st, @Mocked EventNotifier en, @Mocked SimpleObject so)
	        throws Exception {
		final Collection<String> typesNames = new LinkedList<String>();
		typesNames.add("TYPE");
		final Set<Integer> objs = new HashSet<Integer>();
		objs.add(2);

		new Expectations() {
			{
				SimpleType.enumerateAllTypeNames();
				times = 1;
				returns(typesNames);

				SimpleType.findType("TYPE");
				times = 1;
				returns(st);

				st.getAllObjects();
				times = 1;
				returns(objs);

				st.getName();
				times = 1;
				returns("TYPE");

				SimpleType.enumerateAllTypeNames();
				times = 1;
				returns(typesNames);

				SimpleType.findType("TYPE");
				times = 1;
				returns(st);

				SimpleType.findType("TYPE");
				times = 1;
				returns(st);

				st.getObject(2);
				times = 1;
				returns(so);

				so.getParents();
				times = 1;
				returns(new HashSet<Pair<String, Integer>>(0));

				st.removeObject(2);
				times = 1;

				en.notify(withInstanceOf(Event.class));
				times = 1;

				SimpleType.removeType("TYPE");
				times = 1;
				returns(st);

				en.notify(withInstanceOf(Event.class));
				times = 1;
			}
		};

		c.create();
		c.setEventNotifier(en);
		c.deleteObjectType("TYPE");
	}

	@Test
	public void testDeleteNonExistObjectType(@Mocked SimpleType st, @Mocked EventNotifier en) throws Exception {
		final Collection<String> typesNames = new LinkedList<String>();
		typesNames.add("TYPE");

		new Expectations() {
			{
				SimpleType.enumerateAllTypeNames();
				times = 1;
				returns(typesNames);

				SimpleType.findType("TYPE");
				times = 1;
				returns(null);

				SimpleType.removeType("TYPE");
				times = 1;
				returns(null);

				en.notify(withInstanceOf(Event.class));
				times = 1;
			}
		};

		c.create();
		c.setEventNotifier(en);
		c.deleteObjectType("TYPE");
	}

	@Test
	@Ignore
	public void testUpdateObjectType(@Mocked SimpleType st, @Mocked ObjectType ot, @Mocked ObjectTypeUpdatedEvent otue,
									 @Mocked EventNotifier en) throws Exception {
		final Collection<String> typesNames = new LinkedList<String>();
		typesNames.add("TYPE");

		new Expectations() {
			{
				ot.getName();
				times = 1;
				returns("TYPE");

				SimpleType.enumerateAllTypeNames();
				times = 1;
				returns(typesNames);

				SimpleType.findType("TYPE");
				times = 1;
				returns(st);

				st.updateEnvType(ot);
				times = 1;
				returns(otue);

				en.notify(otue);
				times = 1;
			}
		};
		c.create();
		c.setEventNotifier(en);
		c.updateObjectType(ot);
	}

	@Test
	@Ignore
	public void testUpdateNonExistObjectType(@Mocked SimpleType st, @Mocked ObjectType ot) throws Exception {
		final Collection<String> typesNames = new LinkedList<String>();
		typesNames.add("TYPE");

		new Expectations() {
			{
				ot.getName();
				times = 1;
				returns("TYPE");

				SimpleType.enumerateAllTypeNames();
				times = 1;
				returns(typesNames);

				SimpleType.findType("TYPE");
				times = 1;
				returns(null);
			}
		};
		c.create();
		c.updateObjectType(ot);
	}
}
