package com.synapsense.dal.generic.batch.loader;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.jmock.Expectations;
import org.jmock.Mockery;
import org.jmock.integration.junit4.JUnit4Mockery;
import org.jmock.lib.legacy.ClassImposteriser;
import org.junit.Before;
import org.junit.Test;

import com.synapsense.dal.generic.entities.HistoricalValueDO;

public class HistoricalDataLoaderTest {
	private static Mockery context = new JUnit4Mockery() {
		{
			setImposteriser(ClassImposteriser.INSTANCE);
		}
	};

	private static HistoricalDataRowFormatter formatter = context.mock(HistoricalDataRowFormatter.class);
	private static Store store = context.mock(Store.class);
	private static final String bufferFileName;
	
	static {
		File bufferFolder = new File(HistoricalDataRowFormatter.class.getResource("./").getFile());
		bufferFileName = new File(bufferFolder, "buffer.txt").getAbsolutePath().replaceAll("%20", " ");
	}
	
	@Before
	public void before() {
		new File(bufferFileName).delete();
	}

	@Test
	public void testLoadSinglePoint() {
		HistoricalDataLoader loader = new HistoricalDataLoader(bufferFileName, formatter, 1000, store);

		final HistoricalValueDO point1 = new HistoricalValueDO(1, new Date());
		point1.setUnifiedValue(10.5);

		final List<HistoricalValueDO> values = Arrays.asList(point1);

		context.checking(new Expectations() {
			{
				exactly(1).of(formatter).format(values.iterator().next());
				will(returnValue("1,2010-10-20 00:00:00,\\N,10.5,\\N,\\N"));
			}
		});

		loader.load(values);

	}

	@Test
	public void testOverflowBufferSize() {
		HistoricalDataLoader loader = new HistoricalDataLoader(bufferFileName, formatter, 0, store);

		final HistoricalValueDO point1 = new HistoricalValueDO(1, new Date());
		point1.setUnifiedValue(10.5);

		final HistoricalValueDO point2 = new HistoricalValueDO(2, new Date());
		point2.setUnifiedValue(10.5);

		final List<HistoricalValueDO> values = Arrays.asList(point1, point2);

		context.checking(new Expectations() {
			{
				exactly(1).of(formatter).format(point1);
				exactly(1).of(formatter).format(point2);

				exactly(1).of(store).persist(with(any(Resource.class)));
			}
		});

		loader.load(values);

	}

	@Test
	public void testOverflowBufferSizeManyTimes() {
		HistoricalDataLoader loader = new HistoricalDataLoader(bufferFileName, formatter, 100, store);

		final List<HistoricalValueDO> values = new LinkedList<HistoricalValueDO>();
		for (int i = 0; i < 1000; i++) {
			values.add(new HistoricalValueDO(i, new Date()));
		}

		context.checking(new Expectations() {
			{
				exactly(1000).of(formatter).format(with(any(HistoricalValueDO.class)));

				exactly(10).of(store).persist(with(any(Resource.class)));
			}
		});

		loader.load(values);

	}

	@Test
	public void testFileContent() {
		final int bufferSize = 999;
		HistoricalDataLoader loader = new HistoricalDataLoader(bufferFileName, formatter, bufferSize, store);

		final List<HistoricalValueDO> values = new LinkedList<HistoricalValueDO>();

		HistoricalDataRowFormatter rowFormatter = new HistoricalDataRowFormatter("#########.###", "yyyy-MM-dd H:m:s",
		        '.', "\\N");
		StringBuilder expectedFileContent = new StringBuilder();
		final int HISTORY_SIZE = 1000;

		final ArrayList<String> list = new ArrayList<String>();
		for (int i = 1; i <= HISTORY_SIZE; i++) {
			HistoricalValueDO point = new HistoricalValueDO(i, new Date());
			point.setUnifiedValue(i + 0.5);
			values.add(point);
			String formatted = rowFormatter.format(point);
			expectedFileContent.append(formatted + "\n");
			list.add(formatted);
		}

		context.checking(new Expectations() {
			{
				for (int i = 1; i <= HISTORY_SIZE; i++) {
					exactly(1).of(formatter).format(with(any(HistoricalValueDO.class)));
					will(returnValue(list.get(i - 1)));
				}
				exactly(HISTORY_SIZE / bufferSize).of(store).persist(with(any(Resource.class)));
			}
		});

		loader.load(values);

		try {
			String fileContent = readFile(bufferFileName);
			assertEquals(expectedFileContent.toString(), fileContent);
		} catch (IOException e) {
			fail(e.getMessage());
		}
	}

	@Test
	public void testFileContentTooBigBuffer() {
		final int bufferSize = 2000;
		HistoricalDataLoader loader = new HistoricalDataLoader(bufferFileName, formatter, bufferSize, store);

		final List<HistoricalValueDO> values = new LinkedList<HistoricalValueDO>();

		HistoricalDataRowFormatter rowFormatter = new HistoricalDataRowFormatter("#########.###", "yyyy-MM-dd H:m:s",
		        '.', "\\N");
		StringBuilder expectedFileContent = new StringBuilder();
		final int HISTORY_SIZE = 1000;

		final ArrayList<String> list = new ArrayList<String>();
		for (int i = 1; i <= HISTORY_SIZE; i++) {
			HistoricalValueDO point = new HistoricalValueDO(i, new Date());
			point.setUnifiedValue(i + 0.5);
			values.add(point);
			String formatted = rowFormatter.format(point);
			expectedFileContent.append(formatted + "\n");
			list.add(formatted);
		}

		context.checking(new Expectations() {
			{
				for (int i = 1; i <= HISTORY_SIZE; i++) {
					exactly(1).of(formatter).format(with(any(HistoricalValueDO.class)));
					will(returnValue(list.get(i - 1)));
				}
				exactly(HISTORY_SIZE / bufferSize).of(store).persist(with(any(Resource.class)));
			}
		});

		loader.load(values);

		try {
			String fileContent = readFile(bufferFileName);
			assertEquals(expectedFileContent.toString(), fileContent);
		} catch (IOException e) {
			fail(e.getMessage());
		}
	}

	@Test
	public void testFileConsistentUnderHighConcurrency() throws InterruptedException {
		final int THREADS = 100;
		final int BUFFER_SIZE = 99999;
		final int HISTORY_SIZE = 1000;

		HistoricalDataRowFormatter rowFormatter = new HistoricalDataRowFormatter("#########.###", "yyyy-MM-dd H:m:s",
		        '.', "\\N");

		Store store = new Store() {
			@Override
			public void persist(Resource resource) {
			}
		};
		HistoricalDataLoader loader = new HistoricalDataLoader(bufferFileName, rowFormatter, BUFFER_SIZE, store);

		final List<HistoricalValueDO> values = generateValues(HISTORY_SIZE);

		StringBuilder expectedFileContent = new StringBuilder();
		final ArrayList<String> list = new ArrayList<String>();
		for (int i = 1; i <= THREADS; i++) {
			for (HistoricalValueDO point : values) {
				String formatted = rowFormatter.format(point);
				expectedFileContent.append(formatted + "\n");
				list.add(formatted);
			}
		}

		ExecutorService es = Executors.newFixedThreadPool(THREADS);
		for (int i = 0; i < THREADS; i++) {
			es.execute(new LoadWorker(loader, values));
		}

		es.shutdown();
		es.awaitTermination(1, TimeUnit.MINUTES);
		
		try {
			String fileContent = readFile(bufferFileName);
			assertEquals(expectedFileContent.toString(), fileContent);
		} catch (IOException e) {
			fail(e.getMessage());
		}
	}

	private String readFile(String fileName) throws IOException {
		StringBuilder bld = new StringBuilder();
		BufferedReader reader = new BufferedReader(new FileReader(new File(fileName)));
		String data;
		while ((data = reader.readLine()) != null) {
			bld.append(data + "\n");
		}
		reader.close();
		return bld.toString();

	}

	private static void startNew(Runnable w) throws InterruptedException {
			
	}

	private static List<HistoricalValueDO> generateValues(int size) {
		List<HistoricalValueDO> values = new LinkedList<HistoricalValueDO>();
		for (int i = 1; i <= size; i++) {
			HistoricalValueDO point = new HistoricalValueDO(i, new Date());
			point.setUnifiedValue(i + 0.5);
			values.add(point);
		}
		return values;
	}

	private static class LoadWorker implements Runnable {

		private HistoricalDataLoader fw;
		private List<HistoricalValueDO> data;

		public LoadWorker(HistoricalDataLoader fw, List<HistoricalValueDO> data) {
			this.fw = fw;
			this.data = data;
		}

		@Override
		public void run() {
			fw.load(data);
		}

	}

	private static class FWriter implements Runnable {

		private java.io.Writer fw;

		private int limit = 1000000;

		public FWriter(java.io.Writer fw) {

			super();

			this.fw = fw;

		}

		public FWriter(java.io.Writer fw, int limit) {

			super();

			this.fw = fw;

			this.limit = limit;

		}

		@Override
		public void run() {

			for (int i = 0; i < limit; i++) {
				try {
					fw.write(Thread.currentThread().getName() + "10000000000,2010-10-01 00:00:00,100.5,\\N,\\N,\\N"
					        + "\n");
				} catch (IOException e) {

					e.printStackTrace();

				}

			}

		}

	}

	public static void main(String[] args) throws IOException, InterruptedException {
		List<HistoricalValueDO> values = generateValues(2000000);
		HistoricalDataRowFormatter rowFormatter = new HistoricalDataRowFormatter("#########.###", "yyyy-MM-dd H:m:s",
		        '.', "\\N");
		long start = System.currentTimeMillis();

		for (HistoricalValueDO value : values) {
			rowFormatter.format(value);
		}
		System.out.println((System.currentTimeMillis() - start));

		FileWriter fw = new FileWriter(bufferFileName);
		start = System.currentTimeMillis();

		startNew(new FWriter(fw, 2000000));

		fw.close();

		System.out.println((System.currentTimeMillis() - start));

		VelocityWriter writer = new VelocityWriter(new FileWriter("buffer2.txt"), 20 * 1024 * 1024,

		true);

		start = System.currentTimeMillis();

		startNew(new FWriter(writer, 2000000));

		fw.close();

		System.out.println((System.currentTimeMillis() - start));

	}
}
