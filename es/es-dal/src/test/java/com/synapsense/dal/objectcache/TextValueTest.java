package com.synapsense.dal.objectcache;

import com.google.common.base.Strings;
import com.synapsense.exception.InvalidValueException;
import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class TextValueTest {

    @Test(expected = InvalidValueException.class)
    public void onCreateThrowExceptionIfValueExceedsUpperLimit() {
        new TextValue(Strings.repeat("S", 21834));
    }

    @Test(expected = InvalidValueException.class)
    public void onSetThrowExceptionIfValueExceedsUpperLimit() {
        TextValue value = new TextValue("text");
        value.setValue(Strings.repeat("S", 21834));
    }

    @Test
    public void setTimestampSuccess() {
        TextValue value = new TextValue("text");
        value.setTimestamp(1L);
        assertThat(value.getTimestamp(), is(1L));
    }

    @Test
    public void whatWeSetIWhatWeGet() {
        TextValue value = new TextValue("text");
        assertThat(value.getValue(), is("text"));
    }

}

