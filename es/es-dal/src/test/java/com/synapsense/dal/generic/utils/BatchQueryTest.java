package com.synapsense.dal.generic.utils;

import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.jmock.Expectations;
import org.jmock.Mockery;
import org.jmock.integration.junit4.JUnit4Mockery;
import org.junit.Test;

import com.synapsense.dal.generic.entities.ObjectDO;
import com.synapsense.dal.generic.entities.ObjectTypeDO;

public class BatchQueryTest {
	final Mockery context = new JUnit4Mockery();

	final EntityManager em = context.mock(EntityManager.class);
	final QueryFactory qf = QueryFactory.getInstance();
	final Query query = context.mock(Query.class);

	@Test
	public void testSimpleQueryStrategy() {
		final String qryName = "object.by.ids";
		final Set<Integer> value = new HashSet<Integer>();
		value.add(1);
		value.add(2);

		context.checking(new Expectations() {
			{
				oneOf(em).createNamedQuery(qryName).setParameter("ids", value);
				will(returnValue(query));
			}
		});

		Query batchQuery = qf.createSingleParamBatchQuery(em, "object.by.ids", "ids", value, "object.count",
		        "select * from ObjectDO");

		final List<ObjectDO> queryResult = new LinkedList<ObjectDO>();
		ObjectTypeDO ot = new ObjectTypeDO("type");
		queryResult.add(new ObjectDO("name1", ot, false));
		context.checking(new Expectations() {
			{
				oneOf(query).setParameter("ids", value);
				will(returnValue(query));
				oneOf(query).getResultList();
				will(returnValue(queryResult));
			}
		});

		batchQuery.getResultList();

	}

	@Test
	public void testQueryAllInsteadOfIN() {
		final String qryName = "object.by.ids";
		final Set<Integer> value = new HashSet<Integer>();
		
		final int batchThreshold = QueryFactoryConfiguration
		        .getProperty("com.synapsense.dal.queryBatchSize", Integer.class);
		// exceed batch threshold to test different strategy
		for (int i = 1; i <= batchThreshold+1; i++) {
			value.add(i);
		}
		final long count = value.size();
		context.checking(new Expectations() {
			{
				oneOf(em).createNamedQuery(qryName);
				will(returnValue(query));
				oneOf(em).createNamedQuery("object.count");
				will(returnValue(query));
				oneOf(query).getSingleResult();
				will(returnValue(count));
				oneOf(em).createNamedQuery("select * from ObjectDO");
			}
		});

		Query batchQuery = qf.createSingleParamBatchQuery(em, qryName, "ids", value, "object.count",
		        "select * from ObjectDO");

		context.checking(new Expectations() {
			{
				oneOf(query).getResultList();
			}
		});

		batchQuery.getResultList();

	}

	@Test
	public void testQueryDivideParamValueByPieces() {
		final String qryName = "object.by.ids";
		final Set<Integer> value = new HashSet<Integer>();

		for (int i = 1; i <= 2000; i++) {
			value.add(i);
		}

		final long count = value.size() * 3;
		context.checking(new Expectations() {
			{
				oneOf(em).createNamedQuery("object.count");
				will(returnValue(query));
				oneOf(query).getSingleResult();
				will(returnValue(count));
				oneOf(em).createNamedQuery(qryName);
				will(returnValue(query));
			}
		});

		Query batchQuery = qf.createSingleParamBatchQuery(em, qryName, "ids", value, "object.count",
		        "select * from ObjectDO");

		context.checking(new Expectations() {
			{
				exactly(2).of(query).setParameter(with("ids"), with(any(Collection.class)));
				exactly(2).of(query).getResultList();
			}
		});

		batchQuery.getResultList();
	}
}
