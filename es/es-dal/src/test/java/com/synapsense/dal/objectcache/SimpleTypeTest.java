package com.synapsense.dal.objectcache;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Collection;
import java.util.LinkedList;
import java.util.Set;

import mockit.Expectations;

import mockit.Mocked;
import mockit.StrictExpectations;
import mockit.integration.junit4.JMockit;
import org.junit.After;
import org.junit.Test;

import com.synapsense.dal.objectcache.SimpleType.PropType;
import com.synapsense.dto.ObjectType;
import com.synapsense.dto.PropertyDescr;
import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.exception.PropertyNotFoundException;
import com.synapsense.exception.UnableToConvertPropertyException;
import com.synapsense.service.nsvc.events.ObjectTypeUpdatedEvent;
import com.synapsense.util.CollectionUtils;
import com.synapsense.util.Pair;
import org.junit.runner.RunWith;

@RunWith(JMockit.class)
public class SimpleTypeTest {

	final PropertyDescr pdName = new PropertyDescr("name", String.class);

	@After
	public void afterTest() {
		SimpleType.removeAllTypes();
	}

	@Test
	public void testCreateRemoveType(@Mocked ObjectType type, @Mocked IndexProvider indexProvider) throws Exception {
		final Set<PropertyDescr> descrs = CollectionUtils.newSet();
		descrs.add(pdName);
		final Collection<String> names = new LinkedList<String>();
		names.add("TYPE");
		names.add("ALERT");
		names.add("OTHER");

		new StrictExpectations() {
			{
				type.getName();
				times = 3;
				returns("TYPE");

				type.getPropertyDescriptors();
				times = 2;
				returns(descrs);

				type.getName();
				times = 3;
				returns("ALERT");

				type.getPropertyDescriptors();
				times = 2;
				returns(descrs);

				type.getName();
				times = 3;
				returns("OTHER");

				type.getPropertyDescriptors();
				times = 2;
				returns(descrs);
			}
		};

		SimpleType st = SimpleType.newType(type, indexProvider);
		SimpleType.newType(type, indexProvider);
		SimpleType.newType(type, indexProvider);
		assertEquals("TYPE", st.getName());
		assertEquals(st, SimpleType.findType(st.getName()));

		assertEquals(3, SimpleType.enumerateAllTypeNames().size());
		assertTrue(SimpleType.enumerateAllTypeNames().containsAll(names));

		SimpleType.removeType(st.getName());
		assertEquals(2, SimpleType.enumerateAllTypes().size());

		SimpleType.removeAllTypes();
		assertEquals(0, SimpleType.enumerateAllTypes().size());

	}

	@Test
	public void testGetSetPropertyValue(@Mocked ObjectType type, @Mocked IndexProvider indexProvider) throws Exception {
		final Set<PropertyDescr> descrs = CollectionUtils.newSet();
		final PropertyDescr pdVto = new PropertyDescr("vto", ValueTO.class);
		descrs.add(pdName);
		descrs.add(pdVto);

		new Expectations() {
			{
				type.getName();
				times = 3;
				returns("TYPE");

				type.getPropertyDescriptors();
				times = 2;
				returns(descrs);

			}
		};

		SimpleType st = SimpleType.newType(type, indexProvider);
		st.addObject(1);
		assertTrue(st.exists(1));

		ValueTO vto = new ValueTO("name", "test_name");
		st.setPropValue(1, new ValueTO("name", "test_name"));
		st.setPropValue(1, new ValueTO("vto", vto));
		assertEquals("test_name", st.getValue(1, "name", String.class));
		assertEquals(new ValueTO("vto", vto), st.getValue(1, "vto", ValueTO.class));

		st.removeObject(1);
		assertEquals(0, st.getAllObjects().size());

	}

	@Test(expected = UnableToConvertPropertyException.class)
	public void testGetPropertyValueClassCastException(@Mocked ObjectType type, @Mocked IndexProvider indexProvider)
	        throws Exception {
		final Set<PropertyDescr> descrs = CollectionUtils.newSet();
		descrs.add(pdName);

		new Expectations() {
			{
				type.getName();
				times = 3;
				returns("TYPE");

				type.getPropertyDescriptors();
				times = 2;
				returns(descrs);

			}
		};

		SimpleType st = SimpleType.newType(type, indexProvider);
		st.addObject(1);

		st.setPropValue(1, new ValueTO("name", "test_name"));
		st.getValue(1, "name", Integer.class);

	}

	@Test(expected = IllegalArgumentException.class)
	public void testSetPropertyValueWithNullObjectID(@Mocked ObjectType type, @Mocked IndexProvider indexProvider)
	        throws Exception {
		final Set<PropertyDescr> descrs = CollectionUtils.newSet();
		descrs.add(pdName);

		new Expectations() {
			{
				type.getName();
				times = 3;
				returns("TYPE");

				type.getPropertyDescriptors();
				times = 2;
				returns(descrs);
			}
		};

		SimpleType st = SimpleType.newType(type, indexProvider);
		st.setPropValue(null, new ValueTO("name", "test_name"));

	}

	@Test(expected = ObjectNotFoundException.class)
	public void testSetPropertyValueOfNonExistObject(@Mocked ObjectType type, @Mocked IndexProvider indexProvider)
	        throws Exception {
		final Set<PropertyDescr> descrs = CollectionUtils.newSet();
		descrs.add(pdName);

		new Expectations() {
			{
				type.getName();
				times = 3;
				returns("TYPE");

				type.getPropertyDescriptors();
				times = 2;
				returns(descrs);
			}
		};

		SimpleType st = SimpleType.newType(type, indexProvider);
		st.setPropValue(1, new ValueTO("name", "test_name"));

	}

	@Test(expected = PropertyNotFoundException.class)
	public void testSetPropertyValueOfNonExistProperty(@Mocked ObjectType type, @Mocked IndexProvider indexProvider)
	        throws Exception {
		final Set<PropertyDescr> descrs = CollectionUtils.newSet();
		descrs.add(pdName);

		new Expectations() {
			{
				type.getName();
				times = 3;
				returns("TYPE");

				type.getPropertyDescriptors();
				times = 2;
				returns(descrs);
			}
		};

		SimpleType st = SimpleType.newType(type, indexProvider);
		st.setPropValue(1, new ValueTO("value", 100));

	}

	@Test(expected = ObjectNotFoundException.class)
	public void testGetPropertyValueOfNonExistObject(@Mocked ObjectType type, @Mocked IndexProvider indexProvider)
	        throws Exception {
		final Set<PropertyDescr> descrs = CollectionUtils.newSet();
		descrs.add(pdName);

		new Expectations() {
			{
				type.getName();
				times = 3;
				returns("TYPE");

				type.getPropertyDescriptors();
				times = 2;
				returns(descrs);
			}
		};

		SimpleType st = SimpleType.newType(type, indexProvider);
		st.getValue(1, "name", String.class);

	}

	@Test(expected = PropertyNotFoundException.class)
	public void testGetStaticPropertyValueOfNonExistProperty(@Mocked ObjectType type, @Mocked IndexProvider indexProvider)
	        throws Exception {

		final PropertyDescr pd = new PropertyDescr("name", String.class);
		pd.setStatic(true);
		final Set<PropertyDescr> descrs = CollectionUtils.newSet();
		descrs.add(pd);

		new Expectations() {
			{
				type.getName();
				times = 3;
				returns("TYPE");

				type.getPropertyDescriptors();
				times = 2;
				returns(descrs);

			}
		};

		SimpleType st = SimpleType.newType(type, indexProvider);
		st.addObject(1);
		st.getValue(1, "value", Integer.class);

	}

	@Test
	public void testGetSetStaticPropertyValue(@Mocked ObjectType type, @Mocked IndexProvider indexProvider)
	        throws Exception {

		final PropertyDescr pd = new PropertyDescr("name", String.class);
		pd.setStatic(true);
		final Set<PropertyDescr> descrs = CollectionUtils.newSet();
		descrs.add(pd);

		new Expectations() {
			{
				type.getName();
				times = 3;
				returns("TYPE");

				type.getPropertyDescriptors();
				times = 2;
				returns(descrs);

			}
		};

		SimpleType st = SimpleType.newType(type, indexProvider);
		st.addObject(1);
		st.setPropValue(1, new ValueTO("name", "test_name"));
		assertEquals("test_name", st.getValue(1, "name", String.class));
		System.out.println(st.toString());

	}

	@Test
	public void testGetStaticPropertyValueShouldBeNotNull(@Mocked ObjectType type, @Mocked IndexProvider indexProvider)
	        throws Exception {

		final PropertyDescr pd = new PropertyDescr("name", String.class);
		pd.setStatic(true);
		final Set<PropertyDescr> descrs = CollectionUtils.newSet();
		descrs.add(pd);

		new Expectations() {
			{
				type.getName();
				times = 3;
				returns("TYPE");

				type.getPropertyDescriptors();
				times = 2;
				returns(descrs);

			}
		};

		SimpleType st = SimpleType.newType(type, indexProvider);
		assertEquals(null, st.getValue(null, "name", String.class));

	}

	@Test
	public void testCreateRemoveObject(@Mocked ObjectType type, @Mocked IndexProvider indexProvider) throws Exception {
		final Set<PropertyDescr> descrs = CollectionUtils.newSet();
		descrs.add(pdName);

		new Expectations() {
			{
				type.getName();
				times = 3;
				returns("TYPE");

				type.getPropertyDescriptors();
				times = 2;
				returns(descrs);
			}
		};

		SimpleType st = SimpleType.newType(type, indexProvider);
		st.addObject(1);
		st.addObject(2);
		st.getObject(2).getChildren().add(new Pair<String, Integer>("TYPE", 1));
		st.getObject(2).getParents().add(new Pair<String, Integer>("TYPE", 3));
		st.getObject(2).getChildren().add(new Pair<String, Integer>("TYPE", 5));
		st.getObject(2).getParents().add(new Pair<String, Integer>("TYPE", 6));
		st.getObject(2).getChildren().add(new Pair<String, Integer>("BAD_TYPE", 5));
		st.getObject(2).getParents().add(new Pair<String, Integer>("BAD_TYPE", 6));
		st.addObject(3);
		st.removeObject(2);
		assertEquals(2, st.getAllObjects().size());
		assertTrue(st.getAllObjects().contains(1));
		assertTrue(st.getAllObjects().contains(3));

		/*
		 * try to remove non exist object
		 */
		st.removeObject(5);

		System.out.println(st.toString());

	}

	@Test
	public void testUpdateEnvType(@Mocked ObjectType type, @Mocked IndexProvider indexProvider) throws Exception {
		final Set<PropertyDescr> descrs = CollectionUtils.newSet();
		final PropertyDescr pdValue = new PropertyDescr("value", Collection.class);
		pdValue.setCollection(true);
		final PropertyDescr pdTime = new PropertyDescr("time", Long.class);
		pdTime.setHistorical(true);
		descrs.add(pdName);
		descrs.add(pdValue);
		descrs.add(pdTime);

		final Set<PropertyDescr> descrs1 = CollectionUtils.newSet();
		final Set<PropertyDescr> descrs2 = CollectionUtils.newSet();
		final PropertyDescr pdNum = new PropertyDescr("number", Integer.class);
		descrs2.add(pdNum);

		new StrictExpectations() {
			{
				type.getName();
				times = 3;
				returns("TYPE");

				type.getPropertyDescriptors();
				times = 2;
				returns(descrs);

				type.getPropertyDescriptors();
				times = 1;
				returns(descrs1);

				type.getName();
				times = 1;
				returns("ALERT");

				type.getPropertyDescriptors();
				times = 1;
				returns(descrs1);

				type.getName();
				times = 1;
				returns("ALERT");

				type.getPropertyDescriptors();
				times = 1;
				returns(descrs2);

				type.getName();
				times = 1;
				returns("NODE");

				type.getPropertyDescriptors();
				times = 1;
				returns(descrs2);

				type.getName();
				times = 1;
				returns("NODE");
			}
		};

		SimpleType st = SimpleType.newType(type, indexProvider);
		st.addObject(1);
		st.addObject(2);
		ObjectTypeUpdatedEvent otue = st.updateEnvType(type);
		assertEquals("ALERT", otue.getTypeName());
		assertTrue(otue.getAddedProperties().isEmpty());
		assertEquals(3, otue.getRemovedProperties().size());

		otue = st.updateEnvType(type);
		assertEquals("NODE", otue.getTypeName());
		assertEquals(1, otue.getAddedProperties().size());
		assertTrue(otue.getRemovedProperties().isEmpty());

	}

	@Test
	public void testEqualsType(@Mocked ObjectType type, @Mocked IndexProvider indexProvider) throws Exception {
		final Set<PropertyDescr> descrs = CollectionUtils.newSet();
		descrs.add(pdName);

		new StrictExpectations() {
			{
				type.getName();
				times = 3;
				returns("TYPE");

				type.getPropertyDescriptors();
				times = 2;
				returns(descrs);

				type.getName();
				times = 3;
				returns("ALERT");

				type.getPropertyDescriptors();
				times = 2;
				returns(descrs);

			}
		};

		SimpleType st = SimpleType.newType(type, indexProvider);
		SimpleType st1 = SimpleType.newType(type, indexProvider);
		assertTrue(st.equals(st));
		assertFalse(st.equals(null));
		assertFalse(st.equals("string"));
		assertFalse(st.equals(st1));

	}

	@Test
	public void testFind(@Mocked ObjectType type, @Mocked IndexProvider indexProvider) throws Exception {
		final Set<PropertyDescr> descrs = CollectionUtils.newSet();
		descrs.add(pdName);

		new Expectations() {
			{
				type.getName();
				times = 3;
				returns("TYPE");

				type.getPropertyDescriptors();
				times = 2;
				returns(descrs);
			}
		};

		SimpleType st = SimpleType.newType(type, indexProvider);
		st.addObject(1);
		st.addObject(2);
		st.addObject(3);
		st.setPropValue(1, new ValueTO("name", "test_name"));
		st.setPropValue(3, new ValueTO("name", "test_name"));
		Collection<TO<?>> ids = st.find(new ValueTO[] { new ValueTO("name", "test_name") });
		assertEquals(2, ids.size());

	}

	@Test
	public void testPropertyTypeEquals() throws Exception {
		SimpleType.PropertyType pt1 = new SimpleType.PropertyType(pdName);
		SimpleType.PropertyType pt2 = new SimpleType.PropertyType(pdName);
		assertEquals(String.class.getName(), pt1.getClazz().getName());
		assertTrue(pt1.equals(pt1));
		assertTrue(pt1.equals(pt2));
		assertEquals(pt1.hashCode(), pt2.hashCode());
		assertFalse(pt1.equals(null));
		assertFalse(pt1.equals("string"));
		pt1.setClazz(null);
		assertFalse(pt1.equals(pt2));
		pt1.setClazz(Integer.class);
		assertFalse(pt1.equals(pt2));
		pt1.setClazz(String.class);
		pt1.setType(null);
		assertFalse(pt1.equals(pt2));
		pt1.setType(PropType.STATIC);
		assertFalse(pt1.equals(pt2));
	}
}
