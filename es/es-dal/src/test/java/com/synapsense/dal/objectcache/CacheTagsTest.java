package com.synapsense.dal.objectcache;

import static org.junit.Assert.assertEquals;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import mockit.Mock;
import mockit.MockUp;

import mockit.Mocked;
import mockit.StrictExpectations;
import mockit.integration.junit4.JMockit;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.synapsense.dto.ObjectType;
import com.synapsense.dto.PropertyDescr;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.dto.Tag;
import com.synapsense.dto.TagDescriptor;
import com.synapsense.exception.ObjectNotFoundException;
import org.junit.runner.RunWith;

@RunWith(JMockit.class)
public class CacheTagsTest {

	final static class MockedCacheSvcImpl extends MockUp<CacheSvcImpl> {
		@Mock
		public void start(){}
	}

	static CacheSvcImpl c;
	
	@BeforeClass
	public static void se() {
		new MockedCacheSvcImpl();
		c = new CacheSvcImpl();
	}
	

	@Test
	public void testGetTags(@Mocked SimpleType st, @Mocked SimpleObject so) throws Exception {
		final Collection<TO<?>> objIds = new LinkedList<TO<?>>();
		final TO<?> to1 = TOFactory.getInstance().loadTO("TYPE", 1);
		final TO<?> to2 = TOFactory.getInstance().loadTO("TYPE", 2);
		objIds.add(to1);
		objIds.add(to2);

		final List<String> propertyNames = new LinkedList<String>();
		propertyNames.add("name");
		propertyNames.add("value");

		final List<String> tagNames = new LinkedList<String>();
		tagNames.add("test_tag1");
		tagNames.add("test_tag2");
		tagNames.add("test_tag3");
		tagNames.add("test_tag4");

		final Collection<Tag> tags1 = new LinkedList<Tag>();
		tags1.add(new Tag("name", "test_tag1", "test_name1"));
		final Collection<Tag> tags2 = new LinkedList<Tag>();
		tags2.add(new Tag("value", "test_tag2", 100));

		final Map<String, Collection<Tag>> allTags1 = new HashMap<String, Collection<Tag>>();
		allTags1.put("name", tags1);
		allTags1.put("value", tags2);

		final Collection<Tag> tags3 = new LinkedList<Tag>();
		tags1.add(new Tag("name", "test_tag3", "test_name2"));
		final Collection<Tag> tags4 = new LinkedList<Tag>();
		tags2.add(new Tag("value", "test_tag4", 300));

		final Map<String, Collection<Tag>> allTags2 = new HashMap<String, Collection<Tag>>();
		allTags2.put("name", tags3);
		allTags2.put("value", tags4);

		final Map<TO<?>, Map<String, Collection<Tag>>> result = new HashMap<TO<?>, Map<String, Collection<Tag>>>();
		result.put(to1, allTags1);
		result.put(to2, allTags2);

		new StrictExpectations() {
			{
				SimpleType.findType("TYPE");
				times = 1;
				returns(st);

				st.getObject(1);
				times = 1;
				returns(so);

				so.getTags(propertyNames, tagNames);
				times = 1;
				returns(allTags1);

				// *************

				SimpleType.findType("TYPE");
				times = 1;
				returns(st);

				st.getObject(2);
				times = 1;
				returns(so);

				so.getTags(propertyNames, tagNames);
				times = 1;
				returns(allTags2);
			}
		};
		assertEquals(result, c.getTags(objIds, propertyNames, tagNames));
	}

	@Test
	public void testGetTagsOfNonExistObject(@Mocked SimpleType st) throws Exception {
		final Collection<TO<?>> objIds = new LinkedList<TO<?>>();
		final TO<?> to1 = TOFactory.getInstance().loadTO("TYPE", 1);
		final TO<?> to2 = TOFactory.getInstance().loadTO("TYPE", 2);
		objIds.add(to1);
		objIds.add(to2);

		final List<String> propertyNames = new LinkedList<String>();
		propertyNames.add("name");
		propertyNames.add("value");

		final List<String> tagNames = new LinkedList<String>();
		tagNames.add("test_tag1");
		tagNames.add("test_tag2");
		tagNames.add("test_tag3");
		tagNames.add("test_tag4");

		final Map<TO<?>, Map<String, Collection<Tag>>> result = new HashMap<TO<?>, Map<String, Collection<Tag>>>();

		new StrictExpectations() {
			{
				SimpleType.findType("TYPE");
				times = 1;
				returns(st);

				st.getObject(1);
				times = 1;
				returns(null);

				// *************

				SimpleType.findType("TYPE");
				times = 1;
				returns(st);

				st.getObject(2);
				times = 1;
				returns(null);
			}
		};
		assertEquals(result, c.getTags(objIds, propertyNames, tagNames));
	}

	@Test
	public void testGetAllTags(@Mocked SimpleType st, @Mocked SimpleObject so) throws Exception {
		final TO<?> to = TOFactory.getInstance().loadTO("TYPE", 1);
		final Collection<Tag> tags = new HashSet<Tag>();
		tags.add(new Tag());

		new StrictExpectations() {
			{
				SimpleType.findType("TYPE");
				times = 1;
				result = st;

				st.getObject(1);
				times = 1;
				returns(so);

				so.getAllTags();
				times = 1;
				returns(tags);
			}
		};
		assertEquals(tags, c.getAllTags(to));
	}

	@Test(expected = ObjectNotFoundException.class)
	public void testGetAllTagsOfNonExistObject(@Mocked SimpleType st) throws Exception {
		final TO<?> to = TOFactory.getInstance().loadTO("TYPE", 1);

		new StrictExpectations() {
			{
				SimpleType.findType("TYPE");
				times = 1;
				result = st;

				st.getObject(1);
				times = 1;
				returns(null);
			}
		};
		c.getAllTags(to);
	}

	@Test
	public void testGetAllTagsOfPropName(@Mocked SimpleType st, @Mocked SimpleObject so) throws Exception {
		final TO<?> to = TOFactory.getInstance().loadTO("TYPE", 1);
		final Collection<Tag> tags = new HashSet<Tag>();
		tags.add(new Tag());

		new StrictExpectations() {
			{
				SimpleType.findType("TYPE");
				times = 1;
				result = st;

				st.getObject(1);
				times = 1;
				returns(so);

				so.getAllTags("name");
				times = 1;
				returns(tags);
			}
		};
		assertEquals(tags, c.getAllTags(to, "name"));
	}

	@Test(expected = ObjectNotFoundException.class)
	public void testGetAllTagsOfPropNameOfNonExistObject(@Mocked SimpleType st) throws Exception {
		final TO<?> to = TOFactory.getInstance().loadTO("TYPE", 1);

		new StrictExpectations() {
			{
				SimpleType.findType("TYPE");
				times = 1;
				result = st;

				st.getObject(1);
				times = 1;
				returns(null);
			}
		};
		c.getAllTags(to, "name");
	}

	@Test
	public void testGetAllTagsByString(@Mocked SimpleType st, @Mocked SimpleObject so) throws Exception {
		final Collection<String> typesNames = new LinkedList<String>();
		typesNames.add("TYPE");

		final Set<Integer> objs = new HashSet<Integer>();
		objs.add(1);

		final Collection<Tag> tags = new HashSet<Tag>();
		tags.add(new Tag("name", "tag_name", "string"));
		tags.add(new Tag("value", "tag_value", 100));

		final Map<TO<?>, Collection<Tag>> objectTags = new HashMap<TO<?>, Collection<Tag>>();
		objectTags.put(TOFactory.getInstance().loadTO("TYPE", 1), tags);

		new StrictExpectations() {
			{
				SimpleType.enumerateAllTypeNames();
				times = 1;
				returns(typesNames);

				SimpleType.findType("TYPE");
				times = 1;
				returns(st);

				st.getAllObjects();
				times = 1;
				returns(objs);

				st.getName();
				times = 1;
				returns("TYPE");

				SimpleType.findType("TYPE");
				times = 1;
				result = st;

				st.getObject(1);
				times = 1;
				returns(so);

				so.getAllTags();
				times = 1;
				returns(tags);

			}
		};
		assertEquals(objectTags, c.getAllTags("TYPE"));
	}

	@Test
	public void testGetAllTagsByStringOfNonExistObject(@Mocked SimpleType st, @Mocked SimpleObject so) throws Exception {
		final Collection<String> typesNames = new LinkedList<String>();
		typesNames.add("TYPE");

		final Set<Integer> objs = new HashSet<Integer>();
		objs.add(1);

		final Map<TO<?>, Collection<Tag>> objectTags = new HashMap<TO<?>, Collection<Tag>>();

		new StrictExpectations() {
			{
				SimpleType.enumerateAllTypeNames();
				times = 1;
				returns(typesNames);

				SimpleType.findType("TYPE");
				times = 1;
				returns(st);

				st.getAllObjects();
				times = 1;
				returns(objs);

				st.getName();
				times = 1;
				returns("TYPE");

				SimpleType.findType("TYPE");
				times = 1;
				result = st;

				st.getObject(1);
				times = 1;
				returns(null);
			}
		};
		assertEquals(objectTags, c.getAllTags("TYPE"));
	}

	@Test
	@Ignore
	public void testGetTagDescriptors(@Mocked SimpleType st, @Mocked ObjectType ot) throws Exception {
		final Collection<String> typesNames = new LinkedList<String>();
		typesNames.add("TYPE");

		final PropertyDescr pd = new PropertyDescr("name", String.class.getName());
		final TagDescriptor td = new TagDescriptor("tag", "TYPE");
		pd.getTagDescriptors().add(td);

		new StrictExpectations() {
			{
				SimpleType.enumerateAllTypeNames();
				times = 1;
				result = typesNames;

				SimpleType.findType("TYPE");
				times = 1;
				result = st;

				st.getEnvType();
				times = 1;
				returns(ot);

				ot.getPropertyDescriptor("name");
				times = 1;
				result = pd;
			}
		};

		assertEquals(td, c.getTagDescriptor("TYPE", "name", "tag"));
	}

	@Test
	public void testGetTagValue(@Mocked SimpleType st, @Mocked SimpleObject so) throws Exception {
		final TO<?> to = TOFactory.getInstance().loadTO("TYPE", 1);

		new StrictExpectations() {
			{
				SimpleType.findType("TYPE");
				times = 1;
				returns(st);

				st.getObject(1);
				times = 1;
				returns(so);

				so.getTagValue("name", "name_tag", String.class);
				times = 1;
				returns("tag");
			}
		};
		assertEquals("tag", c.getTagValue(to, "name", "name_tag", String.class));
	}

	@Test(expected = ObjectNotFoundException.class)
	public void testGetTagValueOfNonExistObject(@Mocked SimpleType st) throws Exception {
		final TO<?> to = TOFactory.getInstance().loadTO("TYPE", 1);

		new StrictExpectations() {
			{
				SimpleType.findType("TYPE");
				times = 1;
				returns(st);

				st.getObject(1);
				times = 1;
				returns(null);
			}
		};
		c.getTagValue(to, "name", "name_tag", String.class);
	}

	@Test
	public void testSetTagValue(@Mocked SimpleType st, @Mocked SimpleObject so) throws Exception {
		final TO<?> to = TOFactory.getInstance().loadTO("TYPE", 1);

		new StrictExpectations() {
			{
				SimpleType.findType("TYPE");
				times = 1;
				returns(st);

				st.getObject(1);
				times = 1;
				returns(so);

				so.setTagValue("name", "name_tag", "test_tag");
				times = 1;
			}
		};
		c.create();
		c.setTagValue(to, "name", "name_tag", "test_tag");
	}

	@Test(expected = ObjectNotFoundException.class)
	public void testSetTagValueOfNonExistObject(@Mocked SimpleType st) throws Exception {
		final TO<?> to = TOFactory.getInstance().loadTO("TYPE", 1);

		new StrictExpectations() {
			{
				SimpleType.findType("TYPE");
				times = 1;
				returns(st);

				st.getObject(1);
				times = 1;
				returns(null);
			}
		};
		c.create();
		c.setTagValue(to, "name", "name_tag", "test_tag");
	}
}
