package com.synapsense.dal.objectcache;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.jmock.Expectations;
import org.jmock.Sequence;
import org.junit.After;
import org.junit.Test;

import com.google.common.base.Stopwatch;
import com.google.common.collect.Multimap;
import com.synapsense.dto.ObjectType;
import com.synapsense.dto.PropertyDescr;
import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.exception.PropertyNotFoundException;
import com.synapsense.exception.UnableToConvertPropertyException;
import com.synapsense.util.CollectionUtils;

public class CacheIndexTest extends AbstractTest {

	final static int OBJECTS_BUNCH_SIZE = 10;

	final PropertyDescr pdName = new PropertyDescr("name", String.class);
	final PropertyDescr pdValue = new PropertyDescr("value", Double.class);
	final ObjectType envType = mock(ObjectType.class);
	final IndexProvider indexProvider = mock(IndexProvider.class);

	@After
	public void after() {
		SimpleType.removeType("TYPE");
	}

	@Test
	public void addNewEmptyIndexToSimpleType() {
		final Set<PropertyDescr> descrs = CollectionUtils.newSet();
		descrs.add(pdName);

		final Multimap index = mock(Multimap.class);

		checking(new Expectations() {
			{
				exactly(3).of(envType).getName();
				will(returnValue("TYPE"));

				exactly(2).of(envType).getPropertyDescriptors();
				will(returnValue(descrs));

				exactly(1).of(indexProvider).get();
				will(returnValue(index));

			}
		});

		SimpleType st = SimpleType.newType(envType, indexProvider);
		st.addIndex("name");
	}

	@Test
	public void searchBySingleColumnIndexOnSimpleTypeWithBunchOfObjects() throws ObjectNotFoundException,
	        PropertyNotFoundException, UnableToConvertPropertyException {
		final Multimap index = mock(Multimap.class);

		final Set<PropertyDescr> descrs = CollectionUtils.newSet();
		descrs.add(pdName);
		descrs.add(pdValue);

		checking(new Expectations() {
			{
				exactly(3).of(envType).getName();
				will(returnValue("TYPE"));

				exactly(2).of(envType).getPropertyDescriptors();
				will(returnValue(descrs));

				exactly(1).of(indexProvider).get();
				will(returnValue(index));

				exactly(OBJECTS_BUNCH_SIZE).of(index).put(with(any(Object.class)), with(any(Integer.class)));

				exactly(1).of(index).get("name_1");
				will(returnValue(Arrays.asList(1)));
			}
		});

		SimpleType st = SimpleType.newType(envType, indexProvider);

		for (int i = 0; i < OBJECTS_BUNCH_SIZE; i++) {
			st.addObject(i);
			st.setPropValue(i, new ValueTO(pdName.getName(), "name_" + i));
			st.setPropValue(i, new ValueTO(pdValue.getName(), i % (OBJECTS_BUNCH_SIZE) + 0.01));
		}

		st.addIndex(pdName.getName());

		Stopwatch timer = Stopwatch.createStarted();
		Collection<TO<?>> objects = st.find(new ValueTO[] { new ValueTO("name", "name_1") });
		timer.stop();
		assertNotNull(objects);
		assertFalse(objects.isEmpty());
		assertEquals(1, objects.size());

		printOutput("Single column index search time :  %s %s", timer.elapsed(TimeUnit.MICROSECONDS),
		        TimeUnit.MICROSECONDS);
	}

	@Test
	public void searchByTwoColumnIndexOnSimpleTypeWithBunchOfObjects() throws ObjectNotFoundException,
	        PropertyNotFoundException, UnableToConvertPropertyException {
		final Multimap indexOne = mock(Multimap.class, "idx_name");
		final Multimap indexTwo = mock(Multimap.class, "idx_value");

		final Set<PropertyDescr> descrs = CollectionUtils.newSet();
		descrs.add(pdName);
		descrs.add(pdValue);

		checking(new Expectations() {
			{
				exactly(3).of(envType).getName();
				will(returnValue("TYPE"));

				exactly(2).of(envType).getPropertyDescriptors();
				will(returnValue(descrs));

				// add first index by name
				exactly(1).of(indexProvider).get();
				will(returnValue(indexOne));

				exactly(OBJECTS_BUNCH_SIZE).of(indexOne).put(with(any(Object.class)), with(any(Integer.class)));

				// add second - by value
				exactly(1).of(indexProvider).get();
				will(returnValue(indexTwo));

				exactly(OBJECTS_BUNCH_SIZE).of(indexTwo).put(with(any(Object.class)), with(any(Integer.class)));

				exactly(1).of(indexOne).get("name_1");
				will(returnValue(Arrays.asList(1)));

				exactly(1).of(indexTwo).get(1.01);
				will(returnValue(Arrays.asList(1)));
			}
		});

		SimpleType st = SimpleType.newType(envType, indexProvider);

		for (int i = 0; i < OBJECTS_BUNCH_SIZE; i++) {
			st.addObject(i);
			st.setPropValue(i, new ValueTO(pdName.getName(), "name_" + i % (OBJECTS_BUNCH_SIZE)));
			st.setPropValue(i, new ValueTO(pdValue.getName(), i % (OBJECTS_BUNCH_SIZE) + 0.01));
		}

		st.addIndex(pdName.getName());
		st.addIndex(pdValue.getName());

		Stopwatch timer = Stopwatch.createStarted();
		Collection<TO<?>> objects = st
		        .find(new ValueTO[] { new ValueTO("name", "name_1"), new ValueTO("value", 1.01) });
		timer.stop();
		assertNotNull(objects);
		assertFalse(objects.isEmpty());
		assertEquals(1, objects.size());

		printOutput("Two column index search time : %s %s", timer.elapsed(TimeUnit.MICROSECONDS),
		        TimeUnit.MICROSECONDS);
	}

	@Test
	public void removeObjectWhenIndexAlreadySeedByData() throws Exception {
		final Multimap indexOne = mock(Multimap.class, "idx_name");

		final Set<PropertyDescr> descrs = CollectionUtils.newSet();
		descrs.add(pdName);

		checking(new Expectations() {
			{
				exactly(3).of(envType).getName();
				will(returnValue("TYPE"));

				exactly(2).of(envType).getPropertyDescriptors();
				will(returnValue(descrs));

				// add first index by name
				exactly(1).of(indexProvider).get();
				will(returnValue(indexOne));

				exactly(OBJECTS_BUNCH_SIZE).of(indexOne).put(with(any(Object.class)), with(any(Integer.class)));

				exactly(1).of(indexOne).values();
				will(returnValue(CollectionUtils.range(0, OBJECTS_BUNCH_SIZE)));

				exactly(1).of(indexOne).get("name_0");
				will(returnValue(Collections.emptyList()));

			}
		});

		final SimpleType st = SimpleType.newType(envType, indexProvider);
		for (int i = 0; i < OBJECTS_BUNCH_SIZE; i++) {
			st.addObject(i);
			st.setPropValue(i, new ValueTO(pdName.getName(), "name_" + i));
		}

		st.addIndex("name");
		st.removeObject(0);

		assertFalse(st.exists(0));
		Collection<TO<?>> objects = st.find(new ValueTO[] { new ValueTO("name", "name_0") });
		assertTrue(objects.isEmpty());

		assertNull(st.getObject(0));
	}

	@Test
	public void updateIndexWhenSetProperty() throws Exception {
		final Multimap indexOne = mock(Multimap.class, "idx_name");

		final Set<PropertyDescr> descrs = CollectionUtils.newSet();
		descrs.add(pdName);

		checking(new Expectations() {
			{
				exactly(3).of(envType).getName();
				will(returnValue("TYPE"));

				exactly(2).of(envType).getPropertyDescriptors();
				will(returnValue(descrs));

				// add first index by name
				exactly(1).of(indexProvider).get();
				will(returnValue(indexOne));

				exactly(OBJECTS_BUNCH_SIZE).of(indexOne).put(with(any(Object.class)), with(any(Integer.class)));

				// index remove
				exactly(1).of(indexOne).remove("name_1", 1);
				will(returnValue(true));

				// index put
				exactly(1).of(indexOne).put("name_2", 1);
				will(returnValue(true));

				exactly(1).of(indexOne).get("name_2");
				will(returnValue(Arrays.asList(1, 2)));

				exactly(1).of(indexOne).get("name_1");
				will(returnValue(Collections.emptyList()));
			}

		});

		final SimpleType st = SimpleType.newType(envType, indexProvider);
		for (int i = 0; i < OBJECTS_BUNCH_SIZE; i++) {
			st.addObject(i);
			st.setPropValue(i, new ValueTO(pdName.getName(), "name_" + i));
		}

		st.addIndex("name");

		st.setPropValue(1, new ValueTO(pdName.getName(), "name_2"));

		Collection<TO<?>> objects = st.find(new ValueTO[] { new ValueTO("name", "name_2") });
		assertFalse(objects.isEmpty());
		assertEquals("Index should has 2 objects", 2, objects.size());
		assertTrue(objects.contains(TO_FACTORY.loadTO("TYPE", 1)));
		assertTrue(objects.contains(TO_FACTORY.loadTO("TYPE", 2)));

		objects = st.find(new ValueTO[] { new ValueTO("name", "name_1") });
		assertTrue(objects.isEmpty());
	}

	@Test
	public void updateIndexWhenRemoveProperty() throws Exception {
		final Multimap indexOne = mock(Multimap.class, "idx_name");
		final Multimap indexTwo = mock(Multimap.class, "idx_value");

		final Set<PropertyDescr> descrs = CollectionUtils.newSet();
		descrs.add(pdName);
		descrs.add(pdValue);

		final Set<PropertyDescr> udpatedDescriptors = CollectionUtils.newSet();
		udpatedDescriptors.add(pdName);

		final Sequence order = sequence("order");
		checking(new Expectations() {
			{
				exactly(3).of(envType).getName();
				inSequence(order);
				will(returnValue("TYPE"));

				exactly(2).of(envType).getPropertyDescriptors();
				inSequence(order);
				will(returnValue(descrs));

				// add first index by name
				exactly(1).of(indexProvider).get();
				will(returnValue(indexOne));
				exactly(OBJECTS_BUNCH_SIZE).of(indexOne).put(with(any(Object.class)), with(any(Integer.class)));

				// add second index by name
				exactly(1).of(indexProvider).get();
				will(returnValue(indexTwo));
				exactly(OBJECTS_BUNCH_SIZE).of(indexTwo).put(with(any(Object.class)), with(any(Integer.class)));

				// index exists
				exactly(1).of(indexTwo).get(1.01);
				will(returnValue(Arrays.asList(1)));
			}
		});

		final SimpleType st = SimpleType.newType(envType, indexProvider);

		for (int i = 0; i < OBJECTS_BUNCH_SIZE; i++) {
			st.addObject(i);
			st.setPropValue(i, new ValueTO(pdName.getName(), "name_" + i));
			st.setPropValue(i, new ValueTO(pdValue.getName(), i + 0.01));
		}

		st.addIndex(pdName.getName());
		st.addIndex(pdValue.getName());

		Collection<TO<?>> objects = st.find(new ValueTO[] { new ValueTO(pdValue.getName(), 1.01) });
		assertFalse(objects.isEmpty());
		assertEquals("Index should has 1 objects", 1, objects.size());
		assertTrue(objects.contains(TO_FACTORY.loadTO("TYPE", 1)));

		final Sequence udpateTypeSeq = sequence("udpateType");
		checking(new Expectations() {
			{
				// udpate env type sequence
				exactly(1).of(envType).getPropertyDescriptors();
				inSequence(udpateTypeSeq);
				will(returnValue(udpatedDescriptors));

				exactly(1).of(envType).getName();
				inSequence(udpateTypeSeq);
				will(returnValue("TYPE"));

				exactly(1).of(envType).getPropertyDescriptors();
				inSequence(udpateTypeSeq);
				will(returnValue(udpatedDescriptors));

				exactly(1).of(envType).getName();
				inSequence(udpateTypeSeq);
				will(returnValue("TYPE"));
			}
		});

		st.updateEnvType(envType);

		// here index "value" does not exists
		// so no expectaions
		objects = st.find(new ValueTO[] { new ValueTO(pdValue.getName(), 1.01) });
		assertTrue(objects.isEmpty());

		// but index "name" still exist and works
		checking(new Expectations() {
			{
				// udpate env type sequence
				exactly(1).of(indexOne).get("name_1");
				will(returnValue(Arrays.asList(1)));

			}
		});

		objects = st.find(new ValueTO[] { new ValueTO(pdName.getName(), "name_1") });
		assertFalse(objects.isEmpty());
		assertEquals("Index should has 1 objects", 1, objects.size());
		assertTrue(objects.contains(TO_FACTORY.loadTO("TYPE", 1)));
	}
}
