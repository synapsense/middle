package com.synapsense.dre.service;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.synapsense.dre.service.MessageProcessor;

public class MessageProcessorTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void decodeNormalTest() {
		String testString = "xx";
		assertEquals(testString, MessageProcessor.decode(testString));
	}

	@Test
	public void decodePercentTest() {
		String testString = "%";
		assertEquals(testString, MessageProcessor.decode(testString));
	}

	@Test
	public void decodePercenetTwoLettersTest() {
		String testString = "%A2D3";
		String result = MessageProcessor.decode(testString);
		assertFalse(testString.equals(result));
		assertFalse("?D3".equals(result));
	}

	@Test
	public void decodeEncodedPercentTest() {
		String testString = "%25";
		assertEquals("%", MessageProcessor.decode(testString));
	}
	
	@Test
	public void decodeEncodedPercentAndNumTest() {
		String testString = "%253";
		assertEquals("%3", MessageProcessor.decode(testString));
	}
	
	@Test
	public void decodeEncodedDecodeNonASCIITest() {
		String testString = "тестовая строка";
		assertEquals(testString, MessageProcessor.decode(MessageProcessor.encode(testString)));
	}
	@Test
	public void decodeEncodedDecodeNonASCIIWithPercentTest() {
		String testString = "%ASтестовая строка";
		assertEquals(testString, MessageProcessor.decode(MessageProcessor.encode(testString)));
	}
	@Test
	public void decodeEncodedDecodeNonASCIIWithPercentInMiddleTest() {
		String testString = "тестовая%ASстрока";
		assertEquals(testString, MessageProcessor.decode(MessageProcessor.encode(testString)));
	}
	@Test
	public void decodeEncodedDecodeNonASCIIWithPercentValTest() {
		String testString = "тестовая%25строка";
		assertEquals("тестовая%25строка", MessageProcessor.decode(MessageProcessor.encode(testString)));
	}
}
