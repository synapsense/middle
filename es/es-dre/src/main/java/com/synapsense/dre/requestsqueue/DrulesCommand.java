package com.synapsense.dre.requestsqueue;

import com.synapsense.dto.TO;
import com.synapsense.exception.MalformedObjectException;

/**
 * Basic DRules command interface.
 * 
 * @author stikhon
 * 
 */
public interface DrulesCommand {
	/**
	 * Executes the command.
	 * 
	 * @throws MalformedObjectException
	 */
	public void execute() throws MalformedObjectException;

	/**
	 * Gets the associated with the command TO object.
	 * 
	 * @return A TO the command is executed for.
	 */
	public TO<?> getToHandle();
}
