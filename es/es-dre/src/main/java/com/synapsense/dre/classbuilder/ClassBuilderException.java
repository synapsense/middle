package com.synapsense.dre.classbuilder;

public class ClassBuilderException extends Exception {
	public ClassBuilderException(String desc) {
		super(desc);
	}

	public ClassBuilderException(String desc, Throwable reason) {
		super(desc, reason);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -5394211797476658973L;

}
