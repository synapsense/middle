package com.synapsense.dre.classbuilder.asm;

import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;

import org.apache.log4j.PropertyConfigurator;

import com.synapsense.dto.ObjectType;
import com.synapsense.dto.PropertyDescr;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.dto.ValueTO;
import com.synapsense.service.Environment;
import com.synapsense.util.JNDINames;
import com.synapsense.util.ServerLoginModule;

public class ASMTest {
	private final static String TYPENAME = "ASMT";

	private final static void myAssert(boolean cause) {
		if (!cause)
			throw new IllegalStateException("assertion failed");
	}

	public static void main(String[] args) throws Exception {
		PropertyConfigurator.configure("lib/log4j.properties");

		Properties environment = new Properties();
		environment.put(Context.INITIAL_CONTEXT_FACTORY, "org.jnp.interfaces.NamingContextFactory");
		environment.put(Context.URL_PKG_PREFIXES, "org.jboss.naming:org.jnp.interfaces");
		environment.put(Context.PROVIDER_URL, "jnp://localhost:1099");

		System.out.println("creating context...");
		InitialContext ctx = new InitialContext(environment);
		System.out.println("looking for environment...");
		Environment env = ServerLoginModule.getProxy(ctx, JNDINames.ENVIRONMENT, "admin", "admin", Environment.class);

		System.out.println("using class loader: " + Thread.currentThread().getContextClassLoader());
		ASMClassBuilder builder = new ASMClassBuilder(env, Thread.currentThread().getContextClassLoader());

		System.out.println("looking for " + TYPENAME + " object type...");
		ObjectType ot = env.getObjectType(TYPENAME);
		if (ot != null) {

			System.out.println(TYPENAME + " is found. deleting...");
			env.deleteObjectType(TYPENAME);
		}

		System.out.println("creating " + TYPENAME + " object type");
		ot = env.createObjectType(TYPENAME);
		System.out.println("creating property 'name' (string)");
		ot.getPropertyDescriptors().add(new PropertyDescr("name", String.class));
		System.out.println("creating property 'value' (double)");
		ot.getPropertyDescriptors().add(new PropertyDescr("value", Integer.class));
		System.out.println("updating environment...");
		env.updateObjectType(ot);
		System.out.println("done.");

		System.out.println("creating " + TYPENAME + " instance (name='inst', value='1')...");
		TO<?> to1 = env.createObject(TYPENAME, new ValueTO[] { new ValueTO("name", "inst"), new ValueTO("value", 1) });
		System.out.println("creating domain object for instance..");
		Object obj = builder.buildJavaBeanInstance(to1);
		System.out.println("done.");

		// check for getters
		System.out.println("name using domain class=" + obj.getClass().getMethod("getName", null).invoke(obj, null));
		System.out.println("value using domain class=" + obj.getClass().getMethod("getValue", null).invoke(obj, null));
		myAssert("inst".equals(obj.getClass().getMethod("getName", null).invoke(obj, null)));
		myAssert((Integer) obj.getClass().getMethod("getValue", null).invoke(obj, null) == 1);

		// check for setter
		System.out.println("setting value = 2 using domain class");
		obj.getClass().getMethod("setValue", Integer.class).invoke(obj, new Integer(2));
		System.out.println("value using environment=" + env.getPropertyValue(to1, "value", Integer.class));
		myAssert(env.getPropertyValue(to1, "value", Integer.class) == 2);

		// check for native to
		System.out.println("native to using domain class="
		        + obj.getClass().getMethod("getNativeTO", null).invoke(obj, null));
		myAssert(obj.getClass().getMethod("getNativeTO", null).invoke(obj, null).equals(to1));

		// check for serialized to
		System.out.println("serialized to using domain class="
		        + obj.getClass().getMethod("getSerializedTO", null).invoke(obj, null));
		myAssert(obj.getClass().getMethod("getSerializedTO", null).invoke(obj, null)
		        .equals(TOFactory.getInstance().saveTO(to1)));

		// check for hashcode
		System.out.println("hashcode for domain object=" + obj.hashCode());
		Object obj2 = builder.buildJavaBeanInstance(to1);
		System.out.println("hashcode for second copy of domain object=" + obj2.hashCode());
		myAssert(obj2.hashCode() == obj.hashCode());

		// check for equals()
		System.out.println("domain object equals null=" + obj.equals(null));
		myAssert(!obj.equals(null));
		System.out.println("domain object equals some other object=" + obj.equals("some"));
		myAssert(!obj.equals("some"));
		System.out.println("domain object equals domain copy with the same to=" + obj.equals(obj2));
		myAssert(obj.equals(obj2));
		System.out.println("domain object == copy with the same to=" + (obj == obj2));
		myAssert(!(obj == obj2));

		System.out.println("cleaning up environment...");
		env.deleteObjectType(TYPENAME);
		System.out.println("bye");
	}
}
