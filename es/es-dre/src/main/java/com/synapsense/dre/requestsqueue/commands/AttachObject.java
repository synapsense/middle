package com.synapsense.dre.requestsqueue.commands;

import com.synapsense.dre.requestsqueue.DrulesCommandsReceiver;
import com.synapsense.dto.TO;
import com.synapsense.exception.MalformedObjectException;

/**
 * The command allows to attach an object to the drules engine.
 * 
 * @author stikhon
 * 
 */
public class AttachObject extends AbstractDrulesCommand {

	public AttachObject(final DrulesCommandsReceiver commandsReceiver, final TO<?> toHandle) {
		super(commandsReceiver, toHandle);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsense.dre.requestsqueue.DrulesCommand#execute()
	 */
	public void execute() throws MalformedObjectException {
		if (commandsReceiver != null) {
			commandsReceiver.attachObjectUnqueued(toHandle);
		}
	}
}
