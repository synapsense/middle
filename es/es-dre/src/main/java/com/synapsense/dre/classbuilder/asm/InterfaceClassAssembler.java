package com.synapsense.dre.classbuilder.asm;

import static org.objectweb.asm.Opcodes.*;

import org.apache.log4j.Logger;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Type;

import com.synapsense.dre.classbuilder.ClassBuilderException;
import com.synapsense.dto.ObjectType;
import com.synapsense.dto.PropertyDescr;

/**
 * BeanGetterSetterBuilder implementation which builds a pair of abstract
 * getter/setter methods without code inside It is used to build a interface
 * class wrapping environment type
 * 
 * @author anechaev
 * 
 */
final class InterfaceClassAssembler extends AbstractClassAssembler {
	private final static Logger logger = Logger.getLogger(InterfaceClassAssembler.class);

	private final static String CLASS_SUFFIX = "Interface";

	public InterfaceClassAssembler(ObjectType ot) {
		super(ot);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.synapsense.dre.classbuilder.AbstractBuilder#createClassHeader(org
	 * .objectweb.asm.ClassWriter, com.synapsense.environment.ObjectType)
	 */
	@Override
	protected void createClassHeader() {
		String className = getClassName();
		logger.info("Creating a class header for class " + className + "...");
		cw.visit(V1_6, ACC_ABSTRACT + ACC_INTERFACE, className, null, "java/lang/Object", null);

		MethodVisitor mv = cw.visitMethod(ACC_PUBLIC + ACC_ABSTRACT, "getNativeTO", "()Lcom/synapsense/dto/TO;", null,
		        null);
		mv.visitEnd();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.synapsense.dre.classbuilder.AbstractBuilder#createMethodsPair(org
	 * .objectweb.asm.ClassWriter,
	 * com.synapsense.environment.dao.to.PropertyDescr)
	 */
	@Override
	protected void createMethodsPair(PropertyDescr prop) throws ClassBuilderException {
		logger.info("Creating a getter/setter pair for property " + prop + ".");
		String methodName = capitalize(prop.getName());
		String dataName;
		try {
			dataName = Type.getDescriptor(Class.forName(prop.getTypeName()));
		} catch (ClassNotFoundException e) {
			throw new ClassBuilderException("Cannot obtain the class for property type " + prop.getTypeName() + ".", e);
		}

		logger.info("Getter signature: " + prop.getTypeName() + " get" + methodName + "();");
		logger.info("Setter signature: void set" + methodName + "(" + prop.getTypeName() + ");");
		MethodVisitor mv = cw.visitMethod(ACC_PUBLIC + ACC_ABSTRACT, "get" + methodName, "()" + dataName, null, null);
		mv.visitEnd();
		mv = cw.visitMethod(ACC_PUBLIC + ACC_ABSTRACT, "set" + methodName, "(" + dataName + ")V", null, null);
		mv.visitEnd();
	}

	@Override
	public String getClassName() {
		return CLASSES_PACKAGE + ot.getName() + CLASS_SUFFIX;
	}
}
