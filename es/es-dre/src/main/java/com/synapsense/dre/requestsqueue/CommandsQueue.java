package com.synapsense.dre.requestsqueue;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.log4j.Logger;

import com.synapsense.exception.MalformedObjectException;

/**
 * The queue which holds and executes DRules engine commands.
 * 
 * @author stikhon
 * 
 */
public class CommandsQueue<T extends DrulesCommand> implements Runnable {
	private final static Logger logger = Logger.getLogger(CommandsQueue.class);
	protected final BlockingQueue<T> queue;
	private boolean interrupted = false;

	public CommandsQueue(int capacity) {
		this(new LinkedBlockingQueue<T>(capacity));
	}

	public CommandsQueue() {
		this(Integer.MAX_VALUE);
	}

	public CommandsQueue(BlockingQueue<T> queue) {
		this.queue = queue;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Runnable#run()
	 */
	public void run() {
		interrupted = false;
		while (!interrupted) {
			try {
				T command = queue.take();
				command.execute();
			} catch (MalformedObjectException e) {
				logger.warn("Cannot execute Drules command", e);
			} catch (InterruptedException e) {
				interrupted = true;
			}
		}
	}

	public void add(T element) {
		queue.add(element);
		logger.debug("Command is queued for execution:" + element.toString());
	}

	public void interrupt() {
		interrupted = true;
	}
}
