package com.synapsense.dre.requestsqueue;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

public class UniqueElementsStorage<T> implements BlockingQueue<T> {
	private final LinkedBlockingQueue<T> queue;
	private final Set<T> index;
	private final int capacity;

	public UniqueElementsStorage(int capacity) {
		this.capacity = capacity;
		queue = new LinkedBlockingQueue<T>(capacity);
		index = Collections.newSetFromMap(new ConcurrentHashMap<T, Boolean>(capacity));
	}

	@Override
	public boolean add(T e) {
		boolean result = false;
		if (index.add(e)) {
			result = queue.offer(e);
		}
		return result;
	}

	@Override
	public boolean contains(Object o) {
		return index.contains(o);
	}

	@Override
	public int drainTo(Collection<? super T> c) {
		throw new UnsupportedOperationException("not implemented yet");
	}

	@Override
	public int drainTo(Collection<? super T> c, int maxElements) {
		throw new UnsupportedOperationException("not implemented yet");
	}

	@Override
	public boolean offer(T e) {
		throw new UnsupportedOperationException("not implemented yet");
	}

	@Override
	public boolean offer(T e, long timeout, TimeUnit unit) throws InterruptedException {
		throw new UnsupportedOperationException("not implemented yet");
	}

	@Override
	public T poll(long timeout, TimeUnit unit) throws InterruptedException {
		throw new UnsupportedOperationException("not implemented yet");
	}

	@Override
	public void put(T e) throws InterruptedException {
		throw new UnsupportedOperationException("not implemented yet");
	}

	@Override
	public int remainingCapacity() {
		throw new UnsupportedOperationException("not implemented yet");
	}

	@Override
	public boolean remove(Object o) {
		throw new UnsupportedOperationException("not implemented yet");
	}

	@Override
	public T take() throws InterruptedException {
		T e = queue.take();
		index.remove(e);
		return e;
	}

	@Override
	public T element() {
		throw new UnsupportedOperationException("not implemented yet");
	}

	@Override
	public T peek() {
		throw new UnsupportedOperationException("not implemented yet");
	}

	@Override
	public T poll() {
		throw new UnsupportedOperationException("not implemented yet");
	}

	@Override
	public T remove() {
		throw new UnsupportedOperationException("not implemented yet");
	}

	@Override
	public boolean addAll(Collection<? extends T> c) {
		throw new UnsupportedOperationException("not implemented yet");
	}

	@Override
	public void clear() {
		index.clear();
		queue.clear();
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		return index.containsAll(c);
	}

	@Override
	public boolean isEmpty() {
		return queue.isEmpty();
	}

	@Override
	public Iterator<T> iterator() {
		return queue.iterator();
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		return (queue.removeAll(c) && index.removeAll(c));
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		return (queue.retainAll(c) && index.retainAll(c));
	}

	@Override
	public int size() {
		return queue.size();
	}

	@Override
	public Object[] toArray() {
		return queue.toArray();
	}

	@Override
	public <T> T[] toArray(T[] a) {
		return queue.toArray(a);
	}
}
