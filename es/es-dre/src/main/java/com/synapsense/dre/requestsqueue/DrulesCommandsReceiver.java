package com.synapsense.dre.requestsqueue;

import com.synapsense.dto.TO;
import com.synapsense.exception.MalformedObjectException;

/**
 * The interface for commands receiver. Describes allowed operations.
 * 
 * @author stikhon
 * 
 */
public interface DrulesCommandsReceiver {

	public void changeObjectStateUnqueued(TO<?> objHandle);

	public void attachObjectUnqueued(TO<?> objHandle) throws MalformedObjectException;

	public void retractObjectUnqueued(TO<?> objHandle);

}
