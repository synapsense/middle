package com.synapsense.dre.configurator;

import javax.interceptor.InvocationContext;

import org.apache.log4j.Logger;

class AddRulesFromStringsExtractor implements RulesExtractor {

	@Override
	public String extractDrl(InvocationContext ctx) {
		return (String) ctx.getParameters()[0];
	}

	@Override
	public String extractDsl(InvocationContext ctx) {
		return (String) ctx.getParameters()[0];
	}
}
