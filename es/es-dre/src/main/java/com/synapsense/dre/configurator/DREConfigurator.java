package com.synapsense.dre.configurator;

import java.lang.reflect.Method;

import javax.ejb.EJB;
import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.log4j.Logger;

import com.synapsense.dto.RuleType;
import com.synapsense.dto.TO;
import com.synapsense.exception.DREException;
import com.synapsense.exception.EnvException;
import com.synapsense.service.DRuleTypeService;
import com.synapsense.service.DRulesEngine;

public class DREConfigurator {
	private final static Logger logger = Logger.getLogger(DREConfigurator.class);

	// private final String DRE_NAME =
	// "jboss.j2ee:ear=SynapServer.ear,jar=DRulesEngine.jar,name=DRulesEngineService,service=EJB3,type=ManagementInterface";

	@EJB(name = "ejb/DRuleObjectDAO")
	private DRuleTypeService<Integer> rulesConfig;

	// it is a method interceptor, so it should follow the context.proceed()
	// signature
	@AroundInvoke
	public Object onAddRulesPackage(InvocationContext ctx) throws Exception {
		logger.info("DRE Configuration interceptor is being called...");
		try {
			Object result = ctx.proceed();
			RulesExtractor extractor = getParametersExtractor(ctx.getMethod());
			process(extractor.extractDrl(ctx), extractor.extractDsl(ctx));
			logger.info("DRE config has been changed and stored.");
			return result;
		} catch (Exception e) {
			logger.warn("An exception occured! Rules storing has been refused", e);
			throw e;
		}
	}

	// @PostActivate
	// @PostLoad
	// @PostConstruct
	// public void onStart(InvocationContext ctx) throws Exception {
	// logger.info("something happened");
	// }

	private final void process(String drl, String dsl) {
		String packageName = extractPackageName(drl);
		if (rulesConfig.getRuleTypeByName(packageName) != null) {
			logger.warn("The rules package '" + packageName + "' already exists in DRE configuration. Skipping.");
			return;
		}

		try {
			rulesConfig.create(new RuleType(packageName, drl, "drools", dsl));
		} catch (EnvException e) {
			logger.warn("Cannot add package " + packageName + " to DRE configuration.", e);
			return;
		}
	}

	private final static String extractPackageName(String drl) {
		return "package-" + Integer.toString(drl.hashCode());
	}

	private static RulesExtractor getParametersExtractor(Method method) {
		if (method.getName().equalsIgnoreCase("addRules")) {
			return new AddRulesFromStringsExtractor();
		} else if (method.getName().equalsIgnoreCase("addRulesFromFile")) {
			return new AddRulesFromFileExtractor();
		}
		throw new IllegalArgumentException("Intercepts wrong method " + method.getName());
	}

	public void seedDREatStartup(DRulesEngine dre) {
		InitialContext ctx;
		try {
			ctx = new InitialContext();
		} catch (NamingException e) {
			throw new IllegalStateException("Cannot create initial context", e);
		}

		DRuleTypeService<Integer> initialRulesConfig = null;
		try {
			initialRulesConfig = (DRuleTypeService<Integer>) ctx.lookup("java:global/es-ear/es-dal/DRuleTypeObjectDAO");
		} catch (NamingException e1) {
			logger.warn("Cannot lookup DRuleObjectDAO", e1);
			return;
		}

		try {
			logger.info("Rules config is: " + initialRulesConfig);
			for (TO<Integer> ruleHandle : initialRulesConfig.getAllRuleTypes()) {
				try {
					dre.addRulesFromDAO(ruleHandle.getID());
				} catch (DREException e) {
					logger.warn("An error occured at DRE startup configuration. Skip rule package "
					        + initialRulesConfig.getName(ruleHandle.getID()), e);
                }
			}
		} catch (EnvException e) {
			logger.error("Unexpected error in Environment", e);
		}
	}
}
