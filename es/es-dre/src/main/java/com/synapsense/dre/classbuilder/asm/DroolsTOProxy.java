package com.synapsense.dre.classbuilder.asm;

import com.synapsense.dto.TO;

/**
 * The base interface for all the auto-generated TO proxy objects to be used
 * inside the Drools rules engine.
 * 
 * @author stikhon
 * 
 */

public interface DroolsTOProxy {

	String getSerializedTO();

	TO getNativeTO();

}
