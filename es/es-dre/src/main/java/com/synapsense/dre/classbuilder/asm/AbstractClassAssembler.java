package com.synapsense.dre.classbuilder.asm;

import java.util.Collection;

import org.apache.log4j.Logger;
import org.objectweb.asm.ClassWriter;

import com.synapsense.dre.classbuilder.ClassBuilderException;
import com.synapsense.dto.ObjectType;
import com.synapsense.dto.PropertyDescr;

/**
 * 
 * Common interface for concrete builders. Each builder is capable to define
 * getter/setter pair for Environment property using ASM library
 * 
 * @author anechaev
 * 
 */
abstract class AbstractClassAssembler {
	private final static Logger logger = Logger.getLogger(AbstractClassAssembler.class);

	protected final static String CLASSES_PACKAGE = "com.synapsense.dre.domainobjects.";
	protected final ClassWriter cw;
	protected final ObjectType ot;

	private byte[] bytecode;

	public AbstractClassAssembler(ObjectType ot) {
		cw = new ClassWriter(0); // no flags specified
		this.ot = ot;
		bytecode = null;
	}

	/**
	 * Helper method which capitalizes first character of the incoming string
	 * 
	 * @param data
	 *            - string to capitalize
	 * @return - capitalized string
	 */
	protected static String capitalize(String data) {
		if (data == null)
			throw new IllegalArgumentException("data is null");
		if (data.length() == 0)
			return data;
		if (data.length() == 1)
			return data.toUpperCase();
		return data.substring(0, 1).toUpperCase() + data.substring(1);
	}

	/**
	 * Generates unique class name for given type. Note that each
	 * AbstractBuilter implementation must define its own way to define class
	 * name. Class name generated by one implementation shouldn't be equal to
	 * class name generated by other implementation.
	 * 
	 * @param type
	 * @return class name
	 */
	public abstract String getClassName();

	/**
	 * 
	 * @return a serialized form of the class;
	 * @throws ClassBuilderException
	 *             when the <code>createMethodsPair()</code> method fails
	 */
	public synchronized byte[] getClassBytecode() throws ClassBuilderException {
		if (bytecode == null) {
			Collection<PropertyDescr> props = ot.getPropertyDescriptors();
			if (props.isEmpty())
				throw new ClassBuilderException("No properties are defined for type: " + ot.getName() + ".");

			logger.debug("Building domain class for ObjectType: " + ot.getName());
			createClassHeader();
			for (PropertyDescr pd : ot.getPropertyDescriptors()) {
				createMethodsPair(pd);
			}
			bytecode = cw.toByteArray();
			logger.debug("Domain class for ObjectType " + ot.getName() + " has been built successfully");
		}
		return bytecode;
	}

	/**
	 * Builds a class header bytecode (including class-level variables, ctor,
	 * getNativeTO, getSerializedTO, equals & hashCode);
	 */
	protected abstract void createClassHeader();

	/**
	 * Builds a bytecode for getter and setter for the specified property
	 * 
	 * @param prop
	 *            - a source property
	 * @throws ClassBuilderException
	 *             when the prop contains invalid property type (it is
	 *             impossible to retrieve the Class for that property)
	 */
	protected abstract void createMethodsPair(PropertyDescr prop) throws ClassBuilderException;
}
