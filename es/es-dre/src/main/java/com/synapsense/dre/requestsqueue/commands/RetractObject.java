/**
 * 
 */
package com.synapsense.dre.requestsqueue.commands;

import com.synapsense.dre.requestsqueue.DrulesCommandsReceiver;
import com.synapsense.dto.TO;
import com.synapsense.exception.MalformedObjectException;

/**
 * The command allows to retract an object from the drules engine.
 * 
 * @author stikhon
 * 
 */
public class RetractObject extends AbstractDrulesCommand {

	public RetractObject(final DrulesCommandsReceiver commandsReceiver, final TO<?> toHandle) {
		super(commandsReceiver, toHandle);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsense.dre.requestsqueue.DrulesCommand#execute()
	 */
	public void execute() throws MalformedObjectException {
		if (commandsReceiver != null) {
			commandsReceiver.retractObjectUnqueued(toHandle);
		}
	}

}
