/**
 * 
 */
package com.synapsense.dre.requestsqueue.commands;

import com.synapsense.dre.requestsqueue.DrulesCommandsReceiver;
import com.synapsense.dto.TO;
import com.synapsense.exception.MalformedObjectException;

/**
 * The command allows to update an object inside the drules engine.
 * 
 * @author stikhon
 * 
 */
public class UpdateObject extends AbstractDrulesCommand {

	public UpdateObject(final DrulesCommandsReceiver commandsReceiver, final TO<?> toHandle) {
		super(commandsReceiver, toHandle);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsense.dre.requestsqueue.DrulesCommand#execute()
	 */
	public void execute() throws MalformedObjectException {
		if (commandsReceiver != null) {
			commandsReceiver.changeObjectStateUnqueued(toHandle);
		}
	}
}
