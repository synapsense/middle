package com.synapsense.dre.classbuilder;

import com.synapsense.dto.TO;

public interface IClassBuilder {
	Class<?> buildJavaBeanClass(String typeName) throws ClassBuilderException;

	Object buildJavaBeanInstance(TO<?> objHandle) throws ClassBuilderException;

	ClassLoader getClassLoader();
}