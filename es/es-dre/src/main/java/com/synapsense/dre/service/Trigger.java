package com.synapsense.dre.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.synapsense.dre.classbuilder.asm.DroolsTOProxy;

/**
 * The Trigger class is needed for the delayed alarm generation. It contains
 * unique trigger id and the reference object, which should be used in alarm.
 * 
 * @author stikhon
 */
public class Trigger {

	private String ruleId;
	private String compositeId;
	private List<String> referencedObjects;
	private List<String> referencedSensors;

	/**
	 * Instantiates a new trigger.
	 * 
	 * @param id
	 *            The unique trigger id.
	 */
	public Trigger(final String ruleId) {
		this.ruleId = ruleId;
		this.compositeId = ruleId;
		referencedObjects = new ArrayList<String>();
		referencedSensors = new ArrayList<String>();
	}

	/**
	 * Gets the rule Id.
	 * 
	 * @return The ruleId.
	 */
	public String getRuleId() {
		return ruleId;
	}

	/**
	 * Gets the composite Id.
	 * 
	 * @return The compositeId.
	 */
	public String getCompositeId() {
		return compositeId;
	}

	/**
	 * Adds the referenced object to trigger.
	 * 
	 * @param index
	 *            The object's index.
	 * @param object
	 *            The object to add.
	 */
	public void addObject(final int index, final DroolsTOProxy object) {
		if (object != null) {
			int size = referencedObjects.size();
			if (index > size) {
				referencedObjects.addAll(Collections.nCopies(index - size, (String) null));
			}
			referencedObjects.add(index, object.getSerializedTO());
			compositeId += object.getSerializedTO();
		}
	}

	/**
	 * Gets the referenced object by index.
	 * 
	 * @param index
	 *            The object's index.
	 * @return The found object.
	 */
	public String getObject(final int index) {
		if (index > referencedObjects.size()) {
			return null;
		}
		return referencedObjects.get(index);
	}

	/**
	 * Adds the referenced sensor to trigger.
	 * 
	 * @param index
	 *            The object's index.
	 * @param sensor
	 *            The sensor to add.
	 */
	public void addSensor(final int index, final DroolsTOProxy sensor) {
		if (sensor != null) {
			int size = referencedSensors.size();
			if (index > size) {
				referencedSensors.addAll(Collections.nCopies(index - size, (String) null));
			}
			referencedSensors.add(index, sensor.getSerializedTO());
			compositeId += sensor.getSerializedTO();
		}
	}

	/**
	 * Gets the referenced sensor by index.
	 * 
	 * @param index
	 *            The sensor's index.
	 * @return The found sensor.
	 */
	public String getSensor(final int index) {
		if (index > referencedSensors.size()) {
			return null;
		}
		return referencedSensors.get(index);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((compositeId == null) ? 0 : compositeId.hashCode());
		result = prime * result + ((referencedObjects == null) ? 0 : referencedObjects.hashCode());
		result = prime * result + ((referencedSensors == null) ? 0 : referencedSensors.hashCode());
		result = prime * result + ((ruleId == null) ? 0 : ruleId.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Trigger other = (Trigger) obj;
		if (compositeId == null) {
			if (other.compositeId != null)
				return false;
		} else if (!compositeId.equals(other.compositeId))
			return false;
		if (referencedObjects == null) {
			if (other.referencedObjects != null)
				return false;
		} else if (!referencedObjects.equals(other.referencedObjects))
			return false;
		if (referencedSensors == null) {
			if (other.referencedSensors != null)
				return false;
		} else if (!referencedSensors.equals(other.referencedSensors))
			return false;
		if (ruleId == null) {
			if (other.ruleId != null)
				return false;
		} else if (!ruleId.equals(other.ruleId))
			return false;
		return true;
	}

}
