package com.synapsense.dre.classbuilder.janino;

import java.io.StringReader;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.codehaus.janino.SimpleCompiler;

import com.synapsense.dre.classbuilder.ClassBuilderException;
import com.synapsense.dre.classbuilder.IClassBuilder;
import com.synapsense.dto.ObjectType;
import com.synapsense.dto.PropertyDescr;
import com.synapsense.dto.TO;
import com.synapsense.service.Environment;

public class JaninoClassBuilder implements IClassBuilder {
	private final static Logger logger = Logger.getLogger(JaninoClassBuilder.class);

	private final static String PACKAGE_NAME = "com.synapsense.dre.domainobjects";

	private final Environment env;

	private Map<String, Class<?>> definedClasses = new HashMap<String, Class<?>>();

	private SimpleCompiler compiler;

	public JaninoClassBuilder(Environment env, ClassLoader classLoader) {
		this.env = env;
		compiler = new SimpleCompiler();
		compiler.setParentClassLoader(classLoader);
	}

	private ObjectType retrieveObjectType(String typeName) throws ClassBuilderException {
		ObjectType type = null;
		ObjectType[] types = env.getObjectTypes(new String[] { typeName });
		if (types == null) {
			throw new ClassBuilderException("Cannot retrieve ObjectType for type '" + typeName + "'.");
		}
		if (types.length != 1) {
			throw new ClassBuilderException("Request for type '" + typeName + "' returned a set of types types: "
			        + types + ".");
		}
		if (!types[0].getName().equals(typeName)) {
			throw new ClassBuilderException("Unexpected object type was returned:" + types[0].getName()
			        + " instead of " + typeName + ".");
		}
		type = types[0];
		return type;
	}

	private static String generateClassName(ObjectType type) {
		return PACKAGE_NAME + "." + type.getName();
	}

	public Class<?> buildJavaBeanClass(String typeName) throws ClassBuilderException {
		ObjectType type = retrieveObjectType(typeName);

		Class<?> cls = definedClasses.get(generateClassName(type));

		if (cls != null) {
			logger.info("Type '" + typeName + "' has been registered already with serving class " + cls + ".");
			return cls;
		}

		logger.info("Building new class for environment type: " + typeName + ".");

		ClassAssembler asm = new ClassAssembler(PACKAGE_NAME, typeName);
		for (PropertyDescr prop : type.getPropertyDescriptors()) {
			asm.createMethodsPair(prop);
		}

		String resultCode = asm.getResult();
		logger.info("Java source equivalent:\n" + resultCode);

		try {
			compiler.cook(new StringReader(resultCode));
			cls = compiler.getClassLoader().loadClass(generateClassName(type));
		} catch (Exception e) {
			throw new ClassBuilderException("Cannot build class", e);
		}
		return cls;
	}

	public Object buildJavaBeanInstance(TO<?> objHandle) throws ClassBuilderException {
		Class<?> cls = buildJavaBeanClass(objHandle.getTypeName());
		try {
			return cls.getConstructor(new Class<?>[] { TO.class, Environment.class }).newInstance(objHandle, env);
		} catch (Exception e) {
			throw new ClassBuilderException("Cannot instantiate class " + cls, e);
		}
	}

	@Override
	public ClassLoader getClassLoader() {
		return compiler.getClassLoader();
	}
}
