package com.synapsense.dre.configurator;

import javax.interceptor.InvocationContext;

/**
 * An interface class for interceptors within DREConfigurator
 * 
 * @author anechaev
 * 
 */
interface RulesExtractor {
	/**
	 * Extracts a string containing rules text from context
	 * 
	 * @param ctx
	 * @return DRL
	 */
	String extractDrl(InvocationContext ctx);

	/**
	 * Extracts a DSL in form of String from context
	 * 
	 * @param ctx
	 * @return DSL
	 */
	String extractDsl(InvocationContext ctx);
}
