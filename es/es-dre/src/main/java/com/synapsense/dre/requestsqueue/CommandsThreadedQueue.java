package com.synapsense.dre.requestsqueue;

/**
 * Thread holder for the DRules commands queue.
 * 
 * @author stikhon
 * 
 */
public class CommandsThreadedQueue<T extends DrulesCommand> {
	private final CommandsQueue<T> queue;
	private final Thread thread;

	public CommandsThreadedQueue(CommandsQueue<T> queue, boolean isDaemon) {
		this.queue = queue;
		thread = new Thread(queue);
		thread.setDaemon(isDaemon);
		thread.start();
	}

	public void add(T element) {
		queue.add(element);
	}

	public void interrupt() {
		thread.interrupt();
		queue.interrupt();
	}
}
