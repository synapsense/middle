package com.synapsense.dre.service;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.lang.management.ManagementFactory;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.DependsOn;
import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.management.InstanceAlreadyExistsException;
import javax.management.MBeanServer;
import javax.management.ObjectName;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.log4j.Logger;
import org.drools.FactHandle;
import org.drools.RuleBase;
import org.drools.RuleBaseConfiguration;
import org.drools.RuleBaseFactory;
import org.drools.WorkingMemory;
import org.drools.common.InternalWorkingMemory;
import org.drools.compiler.DrlParser;
import org.drools.compiler.PackageBuilder;
import org.drools.compiler.PackageBuilderConfiguration;
import org.drools.compiler.PackageBuilderErrors;
import org.drools.lang.descr.PackageDescr;
import org.drools.rule.Package;
import org.drools.rule.Rule;
import org.drools.rule.builder.dialect.java.JavaDialectConfiguration;
import org.drools.xml.XmlPackageReader;

import com.synapsense.cdi.MBean;
import com.synapsense.dal.objectcache.CacheEnvironmentDispatcher;
import com.synapsense.dal.objectcache.CacheSvcImpl;
import com.synapsense.dre.classbuilder.ClassBuilderException;
import com.synapsense.dre.classbuilder.IClassBuilder;
import com.synapsense.dre.classbuilder.asm.ASMClassBuilder;
import com.synapsense.dre.classbuilder.asm.DroolsTOProxy;
import com.synapsense.dre.configurator.DREConfigurator;
import com.synapsense.dre.requestsqueue.CommandsQueue;
import com.synapsense.dre.requestsqueue.CommandsThreadedQueue;
import com.synapsense.dre.requestsqueue.DrulesCommand;
import com.synapsense.dre.requestsqueue.DrulesCommandsReceiver;
import com.synapsense.dre.requestsqueue.UniqueElementsStorage;
import com.synapsense.dre.requestsqueue.commands.AttachObject;
import com.synapsense.dre.requestsqueue.commands.RetractObject;
import com.synapsense.dre.requestsqueue.commands.UpdateObject;
import com.synapsense.dto.Alert;
import com.synapsense.dto.ChangedValueTO;
import com.synapsense.dto.TO;
import com.synapsense.exception.DREException;
import com.synapsense.exception.EnvException;
import com.synapsense.exception.MalformedObjectException;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.exception.RuleCompilationException;
import com.synapsense.exception.RuleInstantiationException;
import com.synapsense.service.AlertService;
import com.synapsense.service.DRuleTypeService;
import com.synapsense.service.DRulesEngine;
import com.synapsense.service.Environment;
import com.synapsense.service.nsvc.NotificationService;
import com.synapsense.service.nsvc.events.ConfigurationCompleteEvent;
import com.synapsense.service.nsvc.events.ConfigurationCompleteEventProcessor;
import com.synapsense.service.nsvc.events.DispatchingProcessor;
import com.synapsense.service.nsvc.events.ObjectCreatedEvent;
import com.synapsense.service.nsvc.events.ObjectCreatedEventProcessor;
import com.synapsense.service.nsvc.events.ObjectDeletedEvent;
import com.synapsense.service.nsvc.events.ObjectDeletedEventProcessor;
import com.synapsense.service.nsvc.events.ObjectTypeCreatedEvent;
import com.synapsense.service.nsvc.events.ObjectTypeCreatedEventProcessor;
import com.synapsense.service.nsvc.events.PropertiesChangedEvent;
import com.synapsense.service.nsvc.events.PropertiesChangedEventProcessor;
import com.synapsense.service.nsvc.events.ReferencedObjectChangedEvent;
import com.synapsense.service.nsvc.events.ReferencedObjectChangedEventProcessor;

/**
 * DRules engine MBean service implementation class.
 * 
 * @author anechaev, stikhon
 */
@Startup @Singleton @Remote(DRulesEngine.class)
@DependsOn("SynapCache")
@ConcurrencyManagement(ConcurrencyManagementType.BEAN)
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class DRulesEngineService implements DRulesEngineServiceMBean, DRulesEngine, DrulesCommandsReceiver,
        ObjectCreatedEventProcessor, ObjectDeletedEventProcessor, PropertiesChangedEventProcessor,
        ConfigurationCompleteEventProcessor, ObjectTypeCreatedEventProcessor, ReferencedObjectChangedEventProcessor {
	private final static Logger logger = Logger.getLogger(DRulesEngineService.class);
	private final static int QUEUE_CAPACITY = 100000;
	private static final String ALERT_TYPE_NAME = "ALERT";
	private static final String HOST_TO_PROP_NAME = "hostTO";
	private static final String NUM_ALERTS_PROPERTY_NAME = "numAlerts";
	private static final String RULES_PACKAGE_NAME = "ConditionalAlerts";
	private Map<String, Trigger> triggers;

	static private volatile boolean isRunning;

	private static Environment env;
	private static AlertService performer;

	private RuleBase ruleBase;
	private WorkingMemory wm;
	private Timer pool = new Timer(true);
	private IClassBuilder classBuilder;
	private CommandsThreadedQueue<DrulesCommand> objectsQueue;

	private Object memoryUpdateLock = new Object();

	private Map<TO<?>, FactPair> knownFacts = new HashMap<TO<?>, FactPair>();

	private DispatchingProcessor dispProcessor;

	private NotificationService notificationService;
	
	private volatile boolean newObjectTypesAdded = false;
	private Set<String> newObjectTypes = Collections.synchronizedSet(new HashSet<String>());
	private Set<String> failedRules = Collections.synchronizedSet(new HashSet<String>());

	public NotificationService getNotificationService() {
		return notificationService;
	}

	@EJB(beanName="LocalNotificationService")
	public void setNotificationService(NotificationService notificationService) {
		this.notificationService = notificationService;
	}
	
	private void jmxRegister() throws Exception {
	    final ObjectName objectName = new ObjectName("com.synapsense:type=DRulesEngineService");
	    MBeanServer server = ManagementFactory.getPlatformMBeanServer();
	    
	    if(server.isRegistered(objectName))
		server.unregisterMBean(objectName);
	    
	    server.registerMBean(this, objectName);
	}

	public class DREHelper {
		Timer timer = new Timer(true);

		public void sendAlert(String alertName, String alertType, String message, TO<?> ruleID, TO<?> hostObjectID) {
			String decodedAlertName = MessageProcessor.decode(alertName);
			String decodedAlertType = MessageProcessor.decode(alertType);
			String decodedMessage = MessageProcessor.decode(message);
			String decodedMessageWithFixedValues = MessageProcessor.fixSensedValuesInMsg(decodedMessage);

			if (!isRunning)
				return;
			Alert alert = new Alert(decodedAlertName, decodedAlertType, new Date(), decodedMessageWithFixedValues,
			        hostObjectID);
			performer.raiseAlert(alert);
		}

		public void sendAlert(String alertName, String alertType, String message, TO<?> ruleID, TO<?> hostObjectID,
		        Trigger t, long delay) {
			if (!isRunning)
				return;
			String decodedAlertName = MessageProcessor.decode(alertName);
			String decodedAlertType = MessageProcessor.decode(alertType);
			String decodedMessage = MessageProcessor.decode(message);
			String decodedMessageWithFixedValues = MessageProcessor.fixSensedValuesInMsg(decodedMessage);

			Alert alert = new Alert(decodedAlertName, decodedAlertType, new Date(), decodedMessageWithFixedValues,
			        hostObjectID);
			DelayedAlertSchedulingTask schedulingTask = new DelayedAlertSchedulingTask(alert, t);
			try {
				timer.schedule(schedulingTask, delay);
			} catch (IllegalStateException e) {
				// The timer was canceled due to some exception during task
				// execution, restart it
				timer.cancel();
				timer = new Timer(true);
				timer.schedule(schedulingTask, delay);
			}

		}

		public double readDouble(TO<?> objectID, String propName) {
			Double result = null;
			try {
				result = env.getPropertyValue(objectID, propName, Double.class);
			} catch (ObjectNotFoundException e) {
				// Fix for the bug #4452. RACK object might not have some
				// properties(CMid, CBot)in some cases. This is a normal
				// situation and the log record in this case should be made on
				// the debug level only.
				logger.debug("Cannot read " + propName + "because requested object cannot be found.");
			} catch (Exception e) {
				logger.warn("Cannot read " + propName + " from " + objectID + ". Reason: " + e.getMessage());
			}
			return (result == null ? -3000 : result);
		}

		public boolean isChildOf(TO<?> child, TO<?> parent) {
			try {
				return env.getChildren(parent).contains(child);
			} catch (ObjectNotFoundException e) {
				logger.warn(
				        "Cannot determine parent-child relationship (parent: " + parent + ", child: " + child + ")", e);
				return false;
			}
		}

		public boolean isParentOf(TO<?> parent, TO<?> child) {
			try {
				return env.getParents(child).contains(parent);
			} catch (ObjectNotFoundException e) {
				logger.warn(
				        "Cannot determine parent-child relationship (parent: " + parent + ", child: " + child + ")", e);
				return false;
			}
		}

		public Object getParent(DroolsTOProxy objectID, String typeName) {
			Object result = null;
			Collection<TO<?>> parents;
			try {
				parents = env.getParents(objectID.getNativeTO(), typeName);
				if (parents != null && parents.size() > 0) {
					// since we don't know which parent to get, return first
					TO<?> parent = parents.iterator().next();
					FactPair fact = knownFacts.get(parent);
					if (fact != null) {
						result = fact.getFact();
					}
				}
			} catch (ObjectNotFoundException e) {
				logger.warn("Unable to get parent for object" + objectID.getSerializedTO()
				        + "because the object doesn't exist");
			}

			return result;
		}

		public Integer getDataClassId(DroolsTOProxy objectID) {
			Integer result = null;
			try {
				result = env.getPropertyValue(objectID.getNativeTO(), "dataclass", Integer.class);
				if (result == null) {
					logger.warn("Unable to get Data Class Id for object" + objectID.getSerializedTO());
				}
			} catch (EnvException e) {
				logger.warn("Unable to get Data Class Id for object" + objectID.getSerializedTO(), e);
			}

			return result;
		}

		public void destroy() {
			timer.cancel();
		}
	}

	public class DelayedAlertSchedulingTask extends TimerTask {
		private final Alert alert;
		private final Trigger trigger;

		public DelayedAlertSchedulingTask(final Alert alert, final Trigger trigger) {
			this.alert = alert;
			this.trigger = trigger;
		}

		@Override
		public void run() {
			if (triggers.containsKey(trigger.getCompositeId())) {
				performer.raiseAlert(alert);
				synchronized (memoryUpdateLock) {
					triggers.remove(trigger.getCompositeId());
					FactHandle fh = wm.getFactHandle(trigger);
					if (fh != null) {
						wm.retract(fh);
						wm.fireAllRules();
					}
				}
			}
		}
	}

	private static DREHelper helper; // TODO: remove the static reference

	private void addRules(Reader rules, Reader dsl) throws RuleCompilationException, RuleInstantiationException {
		if (rules == null)
			throw new IllegalArgumentException("rules package cannot be null");
		PackageBuilder builder = buildPackage(rules, dsl);
		Package rulePackage = builder.getPackage();

		addRules(rulePackage);
	}

	private void addRules(Package rulePackage) throws RuleInstantiationException {
		synchronized (memoryUpdateLock) {
			try {
				((InternalWorkingMemory) wm).getLock().lock();
				ruleBase.addPackage(rulePackage);
				logger.info("Rules package has been loaded sucessfully");
			} catch (Exception e) {
				logger.info("Cannot instantiate rules package.");
				throw new RuleInstantiationException("Cannot instantiate rules package." + e.getMessage());
			} finally {
				((InternalWorkingMemory) wm).getLock().unlock();
			}

		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.synapsense.dre.service.DRulesEngineMBean#addRules(java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public void addRules(String rulesPackage, String dslPackage) throws RuleCompilationException,
	        RuleInstantiationException {
		Reader dsl = null;
		if (rulesPackage == null)
			throw new IllegalArgumentException("rulesPackage string cannot be null");
		Reader rules = new StringReader(rulesPackage);
		logger.info("Trying to add rules package: \n" + "\tRules package name: " + rulesPackage + " (" + rules + ")");
		if (dslPackage != null) {
			if (dslPackage.length() > 0) {
				dsl = new StringReader(dslPackage);
			}
		}
		try {
			addRules(rules, dsl);
		} catch (RuleCompilationException e) {
			failedRules.add(rulesPackage);
			throw e;
		} catch (RuleInstantiationException e) {
			failedRules.add(rulesPackage);
			throw e;
		}
	}

	@Override
	public void attachObjectUnqueued(TO<?> objHandle) throws MalformedObjectException {
		synchronized (memoryUpdateLock) {
			if (knownFacts.get(objHandle) != null) {
				logger.warn("object " + objHandle + " (ID:" + objHandle.getID() + "; type:" + objHandle.getTypeName()
				        + ") has been already registered");
				return;
			}
			logger.debug("Registering object " + objHandle + " (ID:" + objHandle.getID() + "; type:"
			        + objHandle.getTypeName() + ").");
			Object fact;
			try {
				fact = classBuilder.buildJavaBeanInstance(objHandle);
			} catch (ClassBuilderException e1) {
				throw new MalformedObjectException("Cannot build Domain Class instance", e1);
			}
			FactHandle fh = null;
			try {
				fh = wm.insert(fact);

				knownFacts.put(objHandle, new FactPair(fact, fh));
				wm.fireAllRules();
				logger.debug("New object " + objHandle + " (ID:" + objHandle.getID() + "; type:"
				        + objHandle.getTypeName() + ") has been registered.");
			} catch (IllegalStateException ex) {
				// During the load Drools engine checks the object's properties.
				// In case of alert objects, it is possible what the alert has
				// been already dismissed while notification is processed. In
				// this case the ObjectNotFoundException is raised.
				Throwable cause = ex.getCause();
				if (cause instanceof ObjectNotFoundException) {
					TO<?> to = ((ObjectNotFoundException) cause).getObject();
					if (to != null && "ALERT".equals(to.getTypeName())) {
						if (logger.isDebugEnabled()) {
							logger.debug("RETE calculation tried to access a dismissed alert", ex);
						}
					}
				}
			} catch (Exception e) {
				logger.warn("object " + objHandle + " (ID:" + objHandle.getID() + "; type:" + objHandle.getTypeName()
				        + ") is malformed", e);
			}
		}
	}

	public void retractObjectUnqueued(TO<?> objHandle) {
		synchronized (memoryUpdateLock) {
			FactPair fp = knownFacts.remove(objHandle);
			if (fp == null) {
				logger.warn("Coudln't find an object to retract (" + objHandle + ")");
				return;
			}
			wm.retract(fp.getFh());
			wm.fireAllRules();
		}
	}

	public void changeObjectStateUnqueued(TO<?> objHandle) {
		logger.debug("TO (ID:" + objHandle.getID() + "; type:" + objHandle.getTypeName() + ") has changed its state.");
		if (!isRunning) {
			logger.info("Skip firing rules - they are being loaded");
			return;
		}
		FactPair fact = knownFacts.get(objHandle);
		if (fact == null) {
			return;
		}

		synchronized (memoryUpdateLock) {
			try {
				wm.update(fact.getFh(), fact.getFact());
				wm.fireAllRules();
			} catch (IllegalStateException ex) {
				// scenario when Drools rule tries to access an alert object
				// which was dismissed
				Throwable cause = ex.getCause();
				if (cause instanceof ObjectNotFoundException) {
					TO<?> to = ((ObjectNotFoundException) cause).getObject();
					if (to != null && "ALERT".equals(to.getTypeName())) {
						if (logger.isDebugEnabled()) {
							logger.debug("RETE calculation tried to access a dismissed alert", ex);
						}
					}
				}
			} catch (Exception e) {
				// Drools invokes RETE graph recalculation during object
				// insertion. but object may be not
				// well constructed (some properties may have default values set
				// to null). Null values cause
				// Drools to throw NullPointerException, so we need to catch and
				// process it.
				// What to process - that's the question.
				logger.warn("Exception occured during RETE calculation", e);
			}
		}
	}

	@Override
	public void addRulesFromFile(String rulesPackageFile) throws RuleCompilationException, RuleInstantiationException {
		if (rulesPackageFile == null)
			throw new IllegalArgumentException("rules package file must be set.");
		logger.info("Trying to add rules package: " + rulesPackageFile);
		Reader rules = null;
		try {
			rules = new FileReader(rulesPackageFile.trim());
		} catch (FileNotFoundException e) {
			throw new RuleCompilationException("Cannot set reader for file name " + rulesPackageFile, e);
		}
		addRules(rules, null);
	}

	// that's the only one synchronized method thus there is no need to use
	// synchronized blocks
	@Override
	public synchronized void clearWorkingMemory() {
		logger.info("Cleaning working memory");
		for (TO<?> handle : knownFacts.keySet()) {
			wm.retract(knownFacts.get(handle).getFh());
		}
		knownFacts.clear();
	}

	// TODO: move it to the helper
	/**
	 * Allows to raise alert by calling <code>AlertService</code> component
	 * 
	 * @param alertName
	 *            Alert name
	 * @param alertType
	 *            Alert type
	 * @param message
	 *            any text that shows what happened
	 * @param hostObjectID
	 *            ID of ojbect that is a source of alert
	 */
	public static void sendAlert(String alertName, String alertType, String message, TO<?> ruleID, TO<?> hostObjectID) {
		helper.sendAlert(alertName, alertType, message, ruleID, hostObjectID);
	}

	public static void sendAlert(String alertName, String alertType, String message, TO<?> ruleID, TO<?> hostObjectID,
	        Trigger t, long delay) {
		helper.sendAlert(alertName, alertType, message, ruleID, hostObjectID, t, delay);
	}

	@EJB(beanName="Environment")
	public void setEnvironment(Environment env) {
		// TODO: change to non-static variable
		DRulesEngineService.env = env;
	}

	@EJB
	public void setPerformer(AlertService alertingSerivce) {
		// TODO: change to non-static variable
		DRulesEngineService.performer = alertingSerivce;
	}

	// TODO: move it to the helper
	/**
	 * @param objectID
	 * @param propName
	 * @return
	 */
	public static double readDouble(TO<?> objectID, String propName) {
		return helper.readDouble(objectID, propName);
	}

	// TODO: move it to the helper
	/**
	 * Determine whether the one TO is a child of other TO
	 * 
	 * @param child
	 * @param parent
	 * @return true if it's true :-), false otherwise; false is returned in case
	 *         of <code>ObjectNotFoundException</code>
	 */
	public static boolean isChildOf(TO<?> child, TO<?> parent) {
		return helper.isChildOf(child, parent);
	}

	// TODO: move it to the helper
	/**
	 * Determine whether the one TO is a parent of other TO
	 * 
	 * @param parent
	 * @param child
	 * @return true if it's true :-), false otherwise; false is returned in case
	 *         of <code>ObjectNotFoundException</code>
	 */
	public static boolean isParentOf(TO<?> parent, TO<?> child) {
		return helper.isParentOf(parent, child);
	}

	/**
	 * Gets the first parent for the object.
	 * 
	 * @param child
	 * @return The first parent for the given child.
	 */
	public static Object getParent(DroolsTOProxy objectID, String typeName) {
		return helper.getParent(objectID, typeName);
	}

	/**
	 * Gets the first parent for the object.
	 * 
	 * @param child
	 * @return The first parent for the given child.
	 */
	public static Integer getDataClassId(DroolsTOProxy objectID) {
		return helper.getDataClassId(objectID);
	}

	// AFAIR it is J2EE convention to use such signature
	@Override
	@PostConstruct
	public void create() throws Exception {
		isRunning = false;

		DRulesEngineService.env = CacheEnvironmentDispatcher.getEnvironment(CacheSvcImpl.getInstance(), env);
		
		CommandsQueue<DrulesCommand> commands = new CommandsQueue<DrulesCommand>(
		        new UniqueElementsStorage<DrulesCommand>(QUEUE_CAPACITY));

		objectsQueue = new CommandsThreadedQueue<DrulesCommand>(commands, true);

		classBuilder = new ASMClassBuilder(env, this.getClass().getClassLoader());
		RuleBaseConfiguration conf = new RuleBaseConfiguration(classBuilder.getClassLoader());
		ruleBase = RuleBaseFactory.newRuleBase(conf);
		wm = ruleBase.newStatefulSession();

		String typeNames[] = env.getObjectTypes();
		for (String typeName : typeNames) {
			try {
				classBuilder.buildJavaBeanClass(typeName);
			} catch (ClassBuilderException e) {
				logger.warn("An error occured during building implementation class for env type: " + typeName, e);
				// if JavaBean class cannot be constructed it's not necessary to
				// load objects of this type. this may cause performance
				// slowdown because of multiple exceptions (as many as number of
				// objects of erroneous object type)
				continue;
			}

			Collection<TO<?>> tos = env.getObjectsByType(typeName);
			if (tos == null) {
				continue;
			}
			for (TO<?> to : tos) {
				try {
					attachObjectUnqueued(to);
				} catch (MalformedObjectException e) {
					logger.warn("Cannot attach TO at startup", e);
				}
			}
		}

		logger.info("DRulesEngineService create");
		start();
	}

	@Override
	public void destroy() {
		synchronized (memoryUpdateLock) {
			for (FactPair pair : knownFacts.values()) {
				wm.retract(pair.getFh());
			}
			knownFacts.clear();
			triggers.clear();
			helper.destroy();
			if (objectsQueue != null) {
				objectsQueue.interrupt();
				objectsQueue = null;
			}
		}

		logger.info("DRulesEngineService destroy");
	}

	@Override
	public void start() throws Exception {
		initGlobals();
		new DREConfigurator().seedDREatStartup(this);

		// subscribe to events
		dispProcessor = new DispatchingProcessor().putOcep(this).putOdep(this).putRocep(this).putPcep(this)
		        .putCcep(this).putOtcep(this);
		notificationService.registerProcessor(dispProcessor, null);

		logger.info("DRulesEngineService start");
		isRunning = true;
		// We have to clear triggers at the end of the start since there might
		// be some
		// rules fired during WM seed. These fired rules create triggers and try
		// to schedule
		// DelayedAlertSchedulingTask in the sendAlert method, but the task will
		// not be scheduled
		// since the service is not fully started.
		triggers.clear();
		
		//fix for #7296. revert to @MBean when the root cause is resolved.
		jmxRegister();
	}

	public void initGlobals() {
		String rule = "package " + DRulesEngineService.RULES_PACKAGE_NAME + "\n"
		        + "import com.synapsense.dre.service.DRulesEngineService.DREHelper;\n"
		        + "import com.synapsense.dre.service.Trigger;\n"
		        + "global java.util.concurrent.ConcurrentHashMap triggers; \n" + "global DREHelper helper;";

		try {
			// Load drools rule with globals definitions
			addRules(rule, null);

			// Set loaded globals
			triggers = new ConcurrentHashMap<String, Trigger>();
			wm.setGlobal("triggers", triggers);
			helper = new DREHelper(); // TODO: remove the static reference
			wm.setGlobal("helper", helper);
		} catch (RuleCompilationException e) {
			logger.error(e.getLocalizedMessage(), e);
		} catch (RuleInstantiationException e) {
			logger.error(e.getLocalizedMessage(), e);
		}
	}

	@Override
	@PreDestroy
	public void stop() {
		if (!isRunning)
			return; // already stopped
		isRunning = false;

		// not a subscriber now
		notificationService.deregisterProcessor(dispProcessor);
		dispProcessor = null;

		logger.info("DRulesEngineService stop");
		destroy();
	}

	@Override
	public String[][] enumerateRules() {
		ArrayList<String[]> ruleNames = new ArrayList<String[]>();
		for (Package pack : ruleBase.getPackages()) {
			for (Rule rule : pack.getRules()) {
				ruleNames.add(new String[] { pack.getName(), rule.getName() });
			}
		}
		return ruleNames.toArray(new String[][] { {} });
	}

	@Override
	public void removeRule(String packageName, String ruleName) {
		if (packageName == null)
			throw new IllegalArgumentException("packageName argument cannot be null.");
		if (ruleName == null)
			throw new IllegalArgumentException("ruleName argument cannot be null.");

		Package pack = ruleBase.getPackage(packageName);
		if (pack == null) {
			throw new IllegalArgumentException("No rule packages named '" + packageName + "' are available.");
		}
		Rule rule = pack.getRule(ruleName);
		if (rule == null) {
			throw new IllegalArgumentException("No rule named '" + ruleName + "' is available within package "
			        + packageName + ".");
		}
		// pack.removeRule(rule);
		synchronized (memoryUpdateLock) {
			try {
				((InternalWorkingMemory) wm).getLock().lock();
				ruleBase.removeRule(packageName, ruleName);
				Iterator<Trigger> iterator = triggers.values().iterator();
				while (iterator.hasNext()) {
					Trigger trigger = iterator.next();
					if (ruleName.equalsIgnoreCase(trigger.getRuleId())) {
						FactHandle fh = wm.getFactHandle(trigger);
						if (fh != null) {
							wm.retract(fh);
						}
						iterator.remove();
					}
				}
				wm.fireAllRules();
				logger.info("Rule " + ruleName + " has been removed from package " + packageName + ".");
			} finally {
				((InternalWorkingMemory) wm).getLock().unlock();
			}
		}

	}

	@Override
	public void removeRulePackage(String packageName) {
		if (packageName == null)
			throw new IllegalArgumentException("packageName argument cannot be null.");
		if (ruleBase.getPackage(packageName) == null) {
			throw new IllegalArgumentException("No rule packages named '" + packageName + "' are available.");
		}
		synchronized (memoryUpdateLock) {
			ruleBase.removePackage(packageName);
		}
		logger.info("Package " + packageName + " has been removed.");
	}

	@Override
	public void attachObject(final TO<?> objHandle, long periodicalUpdatePeriod) throws MalformedObjectException {
		attachObject(objHandle);
		pool.scheduleAtFixedRate(new TimerTask() {
			public void run() {
				synchronized (memoryUpdateLock) {
					try {
						changeObjectState(objHandle);
						wm.fireAllRules();
					} catch (Exception e) {
						logger.warn("Error occured during RETE recalc", e);
					}
				}
			}
		}, new Date(), periodicalUpdatePeriod);
	}

	@Override
	public void createDomainClass(String typeName) {
		try {
			classBuilder.buildJavaBeanClass(typeName);
		} catch (ClassBuilderException e) {
			throw new IllegalArgumentException("Cannot build domain class for " + typeName, e);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void addRulesFromDAO(int ruleDaoID) throws RuleCompilationException, RuleInstantiationException {
		logger.info("Trying to add Rules package from DRulesTypeDAO...");
		DRuleTypeService<Integer> rulesConfig = null;
		try {
			rulesConfig = (DRuleTypeService<Integer>) new InitialContext()
			        .lookup("java:global/es-ear/es-dal/DRuleTypeObjectDAO");
		} catch (NamingException e1) {
			logger.warn("Cannot lookup DRuleObjectDAO", e1);
			return;
		}

		try {
			String drl = rulesConfig.getSource(ruleDaoID);
			String dsl = rulesConfig.getDSL(ruleDaoID);
			if (dsl == null || dsl.isEmpty()) {
				addRules(drl, null);
			} else {
				addRules(drl, dsl);
			}
		} catch (ObjectNotFoundException e) {
			throw new RuleCompilationException("Cannot find rule ID specified (" + ruleDaoID + ")", e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.synapsense.dre.service.DRulesEngineMBean#tryToCompileRule(java.lang
	 * .String, java.lang.String)
	 */
	@Override
	public void compileRule(String drlText, String dslText) throws RuleCompilationException {
		Reader dsl = null;
		if (dslText != null && !dslText.isEmpty())
			dsl = new StringReader(dslText);
		buildPackage(new StringReader(drlText), dsl);
	}

	private PackageBuilder buildPackage(Reader drl, Reader dsl) throws RuleCompilationException {
		PackageBuilderConfiguration conf = new PackageBuilderConfiguration(classBuilder.getClassLoader());
		JavaDialectConfiguration cfgJava = (JavaDialectConfiguration) conf.getDialectConfiguration("java");
		cfgJava.setCompiler(JavaDialectConfiguration.JANINO);
		PackageBuilder builder = new PackageBuilder(conf);
		try {
			String ruleStr = readToString(drl);
			// encode rule source to URI-like string (encode all non US-ASCII
			// symbols with percent encoding)
			ruleStr = MessageProcessor.encode(ruleStr);

			PackageDescr packDescr = null;
			if (dsl == null) {
				DrlParser parser = new DrlParser();
				packDescr = parser.parse(ruleStr);
				if (packDescr.getName() == "") {
					XmlPackageReader xmlParser = new XmlPackageReader();
					packDescr = xmlParser.read(new StringReader(ruleStr));
				}
			} else {
				DrlParser parser = new DrlParser();
				packDescr = parser.parse(ruleStr, dsl);
				if (packDescr.getName() == "") {
					XmlPackageReader xmlParser = new XmlPackageReader();
					packDescr = xmlParser.read(new StringReader(ruleStr));
				}
			}
			builder.addPackage(packDescr);
		} catch (Exception e) {// NullPointerException is not a rare thing in
			// Drools
			logger.info("Cannot compile rules package.", e);
			throw new RuleCompilationException("Cannot compile rules package.");
		}

		PackageBuilderErrors errors = builder.getErrors();
		if (errors.getErrors().length != 0) {
			logger.info("Cannot compile rules package.");
			throw new RuleCompilationException("Cannot compile rules package: " + errors.toString());
		}

		return builder;
	}

	private String readToString(Reader reader) {
		String buffer = new String();
		try {
			int c = reader.read();
			while (c != -1) {
				buffer += (char) c;// .concat(new String({(char)c}));
				c = reader.read();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return buffer;
	}

	public void attachObject(TO<?> objHandle) throws MalformedObjectException {
		try {
			objectsQueue.add(new AttachObject(this, objHandle));
		} catch (IllegalStateException e) {
			logger.warn("Insertion of the object declined due to: " + e.getMessage());
		}

	}

	public void retractObject(TO<?> objHandle) {
		try {
			objectsQueue.add(new RetractObject(this, objHandle));
		} catch (IllegalStateException e) {
			logger.warn("Insertion of the object declined due to: " + e.getMessage());
		}

	}

	public void changeObjectState(TO<?> objHandle) {
		try {
			objectsQueue.add(new UpdateObject(this, objHandle));
		} catch (IllegalStateException e) {
			logger.warn("Insertion of the object declined due to: " + e.getMessage());
		}
	}

	@Override
	public ObjectCreatedEvent process(ObjectCreatedEvent event) {
		try {
			attachObject(event.getObjectTO());
		} catch (MalformedObjectException e) {
			logger.warn("Couldn't add object " + event.getObjectTO(), e);
		}
		return event;
	}

	@Override
	public ObjectDeletedEvent process(ObjectDeletedEvent e) {
		retractObject(e.getObjectTO());
		return e;
	}

	@Override
	public PropertiesChangedEvent process(PropertiesChangedEvent e) {
		// we need to avoid updating working memory when numAlerts
		// property changes.
		// It causes alerts to trigger again when alert is dismissed.
		for (ChangedValueTO cvTO : e.getChangedValues()) {
			if (!NUM_ALERTS_PROPERTY_NAME.equals(cvTO.getPropertyName())) {
				if (logger.isDebugEnabled())
					logger.debug("Received PropertiesChangedEvent: " + e);
				changeObjectState(e.getObjectTO());
				break;
			}
		}
		return e;
	}

	@Override
	public ReferencedObjectChangedEvent process(ReferencedObjectChangedEvent e) {
		// Filter ALERT:hostTO updates due to sensor lastValue changes
		// to prevent autodismiss rule from rescheduling.
		TO<?> to = e.getReferencingObj();
		if (logger.isDebugEnabled())
			logger.debug("Received ReferencedObjectChangedEvent: " + e);
		changeObjectState(to);
		return e;
	}

	@Override
	public ConfigurationCompleteEvent process(ConfigurationCompleteEvent e) {
		if (newObjectTypesAdded) {
			logger.info("New object types were added since last configuration complete. Generating new classes... ");
			for (String typeName : newObjectTypes) {
				try {
					createDomainClass(typeName);
				} catch (IllegalArgumentException e1) {
					logger.warn("Unable to create class for type name [" + typeName + "]", e1);
				}
			}
			newObjectTypesAdded = false;
			newObjectTypes.clear();
			logger.info("Trying to reload rules...");
			Iterator<String> i = failedRules.iterator();
			while (i.hasNext()) {
				String rulesPackage = i.next();
				try {
					addRules(rulesPackage, null);
					i.remove();
				} catch (DREException e1) {
                    logger.warn("failed to reload rule: \n" + rulesPackage + " \n on ConfigurationComplete event", e1);
                }
            }
		}
		return null;
	}

	@Override
	public ObjectTypeCreatedEvent process(ObjectTypeCreatedEvent e) {
		newObjectTypes.add(e.getObjectType().getName());
		newObjectTypesAdded = true;
		return null;
	}

}
