package com.synapsense.dre.service;

import java.io.CharArrayWriter;
import java.net.URLDecoder;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

public class MessageProcessor {
	static private final int caseDiff = ('a' - 'A');
	static private Charset utf8 = Charset.forName("UTF-8");
	static private Map<String, String> mapToReplace = new HashMap<String, String>();
	static {
		mapToReplace.put("-3,000F", "Unknown");
		mapToReplace.put("-2,000F", "Unknown");
		mapToReplace.put("-5,000F", "Unknown");
	}

	public static String fixSensedValuesInMsg(String src) {
		String result = src;
		if (src != null && src != "") {
			for (String valueToReplace : mapToReplace.keySet()) {
				result = result.replaceAll(valueToReplace, mapToReplace.get(valueToReplace));
			}
		}
		return result;
	}

	/**
	 * Encodes provided string. All non US-ASCII symbols are replaced with their
	 * 2 bytes code using percent notation (e.g. %D0%BF).
	 * 
	 * @param src
	 *            Source string to encode.
	 * @return Encoded string.
	 */
	public static String encode(String src) {
		StringBuffer out = new StringBuffer(src.length());
		CharArrayWriter charArrayWriter = new CharArrayWriter();
		for (int i = 0; i < src.length();) {
			int c = (int) src.charAt(i);
			if (isNotEncoded(c)) {
				out.append((char) c);
				i++;
			} else {
				// convert to external encoding before hex conversion
				do {
					charArrayWriter.write(c);
					/*
					 * If this character represents the start of a Unicode
					 * surrogate pair, then pass in two characters. It's not
					 * clear what should be done if a bytes reserved in the
					 * surrogate pairs range occurs outside of a legal surrogate
					 * pair. For now, just treat it as if it were any other
					 * character.
					 */
					if (c >= 0xD800 && c <= 0xDBFF) {
						if ((i + 1) < src.length()) {
							int d = (int) src.charAt(i + 1);
							if (d >= 0xDC00 && d <= 0xDFFF) {
								charArrayWriter.write(d);
								i++;
							}
						}
					}
					i++;
				} while (i < src.length() && !isNotEncoded(c = (int) src.charAt(i)));

				charArrayWriter.flush();
				String str = new String(charArrayWriter.toCharArray());
				byte[] ba = str.getBytes(utf8);
				for (int j = 0; j < ba.length; j++) {
					out.append('%');
					char ch = Character.forDigit((ba[j] >> 4) & 0xF, 16);
					// converting to use uppercase letter as part of
					// the hex value if ch is a letter.
					if (Character.isLetter(ch)) {
						ch -= caseDiff;
					}
					out.append(ch);
					ch = Character.forDigit(ba[j] & 0xF, 16);
					if (Character.isLetter(ch)) {
						ch -= caseDiff;
					}
					out.append(ch);
				}
				charArrayWriter.reset();

			}
		}
		return out.toString();
	}

	/**
	 * Decode provided string. All percent notated byte pairs are converted to
	 * their UTF values.
	 * 
	 * @param src
	 *            Source string to decode.
	 * @return Decoded string.
	 */
	public static String decode(String src) {
		int numChars = src.length();
		StringBuffer sb = new StringBuffer(numChars > 500 ? numChars / 2 : numChars);
		int i = 0;

		char c;
		byte[] bytes = null;
		while (i < numChars) {
			c = src.charAt(i);
			switch (c) {
			case '%':
				/*
				 * Starting with this instance of %, process all consecutive
				 * substrings of the form %xy. Each substring %xy will yield a
				 * byte. Convert all consecutive bytes obtained this way to
				 * whatever character(s) they represent in the provided
				 * encoding.
				 */
				int pos = 0;
				try {

					// (numChars-i)/3 is an upper bound for the number
					// of remaining bytes
					if (bytes == null)
						bytes = new byte[(numChars - i) / 3];
					while (((i + 2) < numChars) && (c == '%')) {
						byte byeToWrite = (byte) Integer.parseInt(src.substring(i + 1, i + 3), 16);
						bytes[pos++] = byeToWrite;
						i += 3;
						if (i < numChars)
							c = src.charAt(i);
					}
					// A trailing, incomplete byte encoding such as
					// "%x" won't be decoded

					if ((i < numChars) && (c == '%')) {
						String rest = src.substring(i);
						sb.append(rest);
						i += rest.length();
					} else {
						sb.append(new String(bytes, 0, pos, utf8));
					}
				} catch (NumberFormatException e) {
					sb.append(new String(bytes, 0, pos, utf8));
					// Do not decode %XX when XX is not hex
					String notHex = src.substring(i, i + 3);
					sb.append(notHex);
					i+=3;
				}
				break;
			default:
				sb.append(c);
				i++;
				break;
			}
		}
		return sb.toString();
	}

	private static boolean isNotEncoded(int ch) {
		return (ch < 128)&&(ch != 37);
	}
}
