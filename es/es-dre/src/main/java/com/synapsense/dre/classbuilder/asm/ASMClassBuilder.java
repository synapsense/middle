package com.synapsense.dre.classbuilder.asm;

import java.security.AccessController;
import java.security.PrivilegedAction;
import java.security.ProtectionDomain;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import com.synapsense.dre.classbuilder.ClassBuilderException;
import com.synapsense.dre.classbuilder.IClassBuilder;
import com.synapsense.dto.ObjectType;
import com.synapsense.dto.TO;
import com.synapsense.service.Environment;

/**
 * Builds a implementation class or interface according to the type defined in
 * environment model
 * 
 * @author anechaev
 * 
 */
public final class ASMClassBuilder implements IClassBuilder {
	private final static Logger logger = Logger.getLogger(ASMClassBuilder.class);

	private final static ProtectionDomain PROTECTION_DOMAIN;
	static {
		PROTECTION_DOMAIN = (ProtectionDomain) AccessController.doPrivileged(new PrivilegedAction<ProtectionDomain>() {
			public ProtectionDomain run() {
				return ASMClassBuilder.class.getProtectionDomain();
			}
		});
	}

	private ConcurrentHashMap<String, Class<?>> definedClasses = new ConcurrentHashMap<String, Class<?>>();
	private Environment env;
	private ByteArrayClassLoader classLoader;

	public ASMClassBuilder(Environment env, ClassLoader parentClassLoader) {
		this.env = env;
		this.classLoader = new ByteArrayClassLoader(parentClassLoader);
	}

	private ObjectType retrieveObjectType(String typeName) throws ClassBuilderException {
		ObjectType type = null;
		ObjectType[] types = env.getObjectTypes(new String[] { typeName });
		if (types == null) {
			throw new ClassBuilderException("Cannot retrieve ObjectType for type '" + typeName + "'.");
		}
		if (types.length != 1) {
			throw new ClassBuilderException("Request for type '" + typeName + "' returned a set of types types: "
			        + types + ".");
		}
		if (!types[0].getName().equals(typeName)) {
			throw new ClassBuilderException("Unexpected object type was returned:" + types[0].getName()
			        + " instead of " + typeName + ".");
		}
		type = types[0];
		return type;
	}

	private Class<?> instantiateClass(String className, byte[] dump) throws ClassBuilderException {
		try {
			return classLoader.defineClass(className, dump, PROTECTION_DOMAIN);
		} catch (Exception e) {
			throw new ClassBuilderException("Cannot instantiate class from bytecode.", e);
		}

	}

	private Class<?> buildClass(String typeName, AbstractClassAssembler builder) throws ClassBuilderException {
		Class<?> result = definedClasses.get(builder.getClassName());
		if (result != null) {
			logger.debug("Type '" + typeName + "' has been registered already with serving class " + result + ".");
			return result;
		}

		try {
			result = classLoader.loadClassByParentCL(builder.getClassName());
			logger.warn("Environment type " + typeName + " is served by precompiled domain class " + result
			        + ". Use it.");
			return result;
		} catch (Exception e) {
		}
		// it's just an inverse of instructions flow...
		// if we receive an exception, we should try to create our own class

		result = instantiateClass(builder.getClassName(), builder.getClassBytecode());

		result = definedClasses.putIfAbsent(builder.getClassName(), result);
		if (result == null)
			result = definedClasses.get(builder.getClassName());
		logger.info("Built java bean class for type [" + typeName + "]");
		return result;
	}

	@Override
	public Class<?> buildJavaBeanClass(String typeName) throws ClassBuilderException {
		return buildClass(typeName, new ImplClassAssembler(retrieveObjectType(typeName)));
	}

	@Override
	public Object buildJavaBeanInstance(TO<?> objHandle) throws ClassBuilderException {
		Class<?> cls = buildJavaBeanClass(objHandle.getTypeName());
		try {
			return cls.getConstructor(new Class<?>[] { TO.class, Environment.class }).newInstance(objHandle, env);
		} catch (Exception e) {
			throw new ClassBuilderException("Cannot instantiate class " + cls, e);
		}
	}

	@Override
	public ClassLoader getClassLoader() {
		return classLoader;
	}

	// public static ClassLoader getClassLoader(ClassLoader parent) {
	// if(classLoader == null) {
	// classLoader = new ByteArrayClassLoader(parent);
	// }
	// return classLoader;
	// }

	private class ByteArrayClassLoader extends ClassLoader {
		private ClassLoader parent;

		public ByteArrayClassLoader(final ClassLoader parent) {
			super(parent);
			this.parent = parent;
		}

		private Class<?> defineClass(final String name, final byte[] bytes, final ProtectionDomain domain) {
			return defineClass(name, bytes, 0, bytes.length, domain);
		}

		@Override
		public Class<?> loadClass(String className) throws ClassNotFoundException {
			try {
				return super.loadClass(className);
			} catch (Exception e) {
				// DON'T PROPAGATE EXCEPTION CAUGHT TO THE
				// ClassNotFoundException! J
				// Janino compiler contains a bug, that affects Drools
				// functionality...
				// see org.codehaus.janino.ClassLoaderIClassLoader.findIClass()
				// method if you want...
				if (!definedClasses.containsKey(className))
					throw new ClassNotFoundException(className);

				return definedClasses.get(className);
			}
		}

		public Class<?> loadClassByParentCL(String className) throws ClassNotFoundException {
			return parent.loadClass(className);
		}
	}
}
