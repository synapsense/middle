package com.synapsense.dre.requestsqueue.commands;

import java.util.Comparator;

import com.synapsense.dre.requestsqueue.DrulesCommand;
import com.synapsense.dto.TOFactory;

public class CommandsComparator implements Comparator<DrulesCommand> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	public int compare(DrulesCommand o1, DrulesCommand o2) {
		if (o1.getClass() == o2.getClass()) {
			return TOFactory.getInstance().comparator().compare(o1.getToHandle(), o2.getToHandle());
		} else {
			return 1;
		}
	}
}
