package com.synapsense.dre.service;

import org.drools.FactHandle;

class FactPair {
	private FactHandle fh;
	private Object fact;

	public FactPair(Object fact, FactHandle fh) {
		this.fact = fact;
		this.fh = fh;
	}

	public FactHandle getFh() {
		return fh;
	}

	public Object getFact() {
		return fact;
	}
}
