package com.synapsense.dre.domainproxy;

import java.beans.Introspector;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

import org.apache.log4j.Logger;

import com.synapsense.dto.TO;
import com.synapsense.service.Environment;

/**
 * Proxy class which redirects invocations of domain interface method to the
 * corresponding environment API request
 * 
 * @author anechaev
 * 
 */
public class TOProxy implements InvocationHandler {
	private final static Logger logger = Logger.getLogger(TOProxy.class);
	private Environment env;
	private TO<?> objHandle;

	public TOProxy(TO<?> objHandle, Environment env) {
		if (objHandle == null)
			throw new IllegalArgumentException("Object handle (TO) is not set");
		if (env == null)
			throw new IllegalArgumentException("Environment is not set");
		this.env = env;
		this.objHandle = objHandle;
	}

	@Override
	public Object invoke(Object proxy, Method method, Object[] params) throws Throwable {
		String methodName = method.getName();
		if (methodName.startsWith("get")) {
			logger.info("Invoke method " + methodName + " for TO (ID: " + objHandle.getID() + "; type: "
			        + objHandle.getTypeName() + ")");
			String propName = Introspector.decapitalize(methodName.substring(3));
			return env.getPropertyValue(objHandle, propName, method.getReturnType());
		} else if (methodName.startsWith("is")) {
			String propName = Introspector.decapitalize(methodName.substring(2));
			return env.getPropertyValue(objHandle, propName, method.getReturnType());
		} else if (methodName.startsWith("set")) {
			String propName = Introspector.decapitalize(methodName.substring(3));
			env.setPropertyValue(objHandle, propName, params);
			return null;
		}
		// method name should never be null. so I suppose that given notation
		// seems more human-friendly
		else if (methodName.equals("hashCode")) {
			return hashCode();
		} else if (methodName.equals("equals")) {
			if (Proxy.isProxyClass(params[0].getClass())) {
				return equals(Proxy.getInvocationHandler(params[0]));
			} else {
				return equals(params[0]);
			}
		}
		return method.invoke(objHandle, params);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((objHandle == null) ? 0 : objHandle.getID().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (this.getClass() != obj.getClass())
			return false;
		final TOProxy other = (TOProxy) obj;
		if (objHandle == null) {
			if (other.objHandle != null)
				return false;
		} else if (!objHandle.getID().equals(other.objHandle.getID()))
			return false;
		return true;
	}
}
