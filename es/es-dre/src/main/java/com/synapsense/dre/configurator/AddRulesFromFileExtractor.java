package com.synapsense.dre.configurator;

import java.io.DataInputStream;
import java.io.FileInputStream;

import javax.interceptor.InvocationContext;

public class AddRulesFromFileExtractor implements RulesExtractor {

	@Override
	public String extractDrl(InvocationContext ctx) {
		try {
			DataInputStream in = new DataInputStream(new FileInputStream((String) ctx.getParameters()[0]));
			byte[] temp = new byte[in.available()];
			in.readFully(temp);
			return new String(temp);
		} catch (Exception e) {
			throw new IllegalArgumentException("Cannot retrieve drl from file", e);
		}
	}

	@Override
	public String extractDsl(InvocationContext ctx) {
		return null;
	}
}
