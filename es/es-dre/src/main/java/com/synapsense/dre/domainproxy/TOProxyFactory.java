package com.synapsense.dre.domainproxy;

import java.io.IOException;
import java.lang.reflect.Proxy;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.synapsense.dto.TO;
import com.synapsense.service.Environment;

public final class TOProxyFactory {
	private final static Logger logger = Logger.getLogger(TOProxyFactory.class);

	private static Map<String, Class<?>> interfacesMap = new HashMap<String, Class<?>>();
	static {
		Properties props = new Properties();
		try {
			props.load(Thread.currentThread().getClass().getResourceAsStream("/InterfacesMapping.properties"));
		} catch (IOException e1) {
			logger.error("Interfaces mapping file not found.");
			throw new IllegalStateException("Cannot process interfaces mapping file", e1);
		}
		try {
			for (Object type : props.keySet()) {
				interfacesMap.put((String) type, Class.forName(props.getProperty((String) type)));
			}
		} catch (ClassNotFoundException e) {
			logger.warn("Interfaces mapping error", e);
		}
	}

	public static Object createTOProxy(TO<?> objHandle, Environment env, ClassLoader classLoader) {
		if (objHandle == null)
			throw new IllegalArgumentException("objHandle is not set");

		Class<?> proxie = interfacesMap.get(objHandle.getTypeName());
		if (proxie == null) {
			throw new IllegalArgumentException("cannot map TO type '" + objHandle.getTypeName()
			        + "' to corresponding interface");
		}
		return Proxy.newProxyInstance(classLoader, new Class[] { proxie }, new TOProxy(objHandle, env));
	}
}
