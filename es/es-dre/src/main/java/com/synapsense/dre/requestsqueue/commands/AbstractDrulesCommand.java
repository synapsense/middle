package com.synapsense.dre.requestsqueue.commands;

import com.synapsense.dre.requestsqueue.DrulesCommand;
import com.synapsense.dre.requestsqueue.DrulesCommandsReceiver;
import com.synapsense.dto.TO;

/**
 * Base class for drules command.
 * 
 * @author stikhon
 * 
 */
public abstract class AbstractDrulesCommand implements DrulesCommand {
	protected DrulesCommandsReceiver commandsReceiver;
	protected TO<?> toHandle;

	public AbstractDrulesCommand(final DrulesCommandsReceiver commandsReceiver, final TO<?> toHandle) {
		this.commandsReceiver = commandsReceiver;
		this.toHandle = toHandle;
	}

	public DrulesCommandsReceiver getCommandsReceiver() {
		return commandsReceiver;
	}

	public TO<?> getToHandle() {
		return toHandle;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return "Name:" + getClass().getSimpleName() + " TO:" + toHandle.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((toHandle == null) ? 0 : toHandle.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AbstractDrulesCommand other = (AbstractDrulesCommand) obj;
		if (toHandle == null) {
			if (other.toHandle != null)
				return false;
		} else if (!toHandle.equals(other.toHandle))
			return false;
		return true;
	}

}
