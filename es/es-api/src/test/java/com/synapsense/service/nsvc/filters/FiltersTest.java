package com.synapsense.service.nsvc.filters;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.Collections;

import org.junit.Test;

import com.synapsense.dto.PropertyDescr;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.service.impl.dao.to.EnvObjTypeTO;
import com.synapsense.service.nsvc.events.ObjectCreatedEvent;
import com.synapsense.service.nsvc.events.ObjectDeletedEvent;
import com.synapsense.service.nsvc.events.ObjectTypeCreatedEvent;
import com.synapsense.service.nsvc.events.ObjectTypeDeletedEvent;
import com.synapsense.service.nsvc.events.ObjectTypeUpdatedEvent;
import com.synapsense.service.nsvc.events.RelationRemovedEvent;
import com.synapsense.service.nsvc.events.RelationSetEvent;

public class FiltersTest {
	private TOMatcher matcher1 = new TOMatcher("N.*|WINTER");
	private TOMatcher matcher2 = new TOMatcher("N.*|SENSOR");
	private TO<?> noteTO = TOFactory.getInstance().loadTO("NOTE:100");
	private TO<?> nightTO = TOFactory.getInstance().loadTO("NIGHT:3");
	private TO<?> winterTO = TOFactory.getInstance().loadTO("WINTER:7");
	private TO<?> sensorTO = TOFactory.getInstance().loadTO("SENSOR:1");
	private TO<?> unknownTO = TOFactory.getInstance().loadTO("UNKNOWN:1");

	@Test
	public void testObjectTypeCreatedEventFilter() {
		ObjectTypeCreatedEventFilter filter = new ObjectTypeCreatedEventFilter("WSN.*");
		ObjectTypeCreatedEvent event = new ObjectTypeCreatedEvent(new EnvObjTypeTO("WSNNODE", null));
		assertNotNull(filter.process(event));
		event = new ObjectTypeCreatedEvent(new EnvObjTypeTO("ROOT", null));
		assertNull(filter.process(event));
	}

	@Test
	public void testObjectTypeUpdatedEventFilter() {
		ObjectTypeUpdatedEventFilter filter = new ObjectTypeUpdatedEventFilter("WSN.+");
		ObjectTypeUpdatedEvent event = new ObjectTypeUpdatedEvent("WSNSENSOR", Collections.<PropertyDescr> emptySet(),
		        Collections.<String> emptySet());
		assertNotNull(filter.process(event));
		event = new ObjectTypeUpdatedEvent("ROOM", Collections.<PropertyDescr> emptySet(),
		        Collections.<String> emptySet());
		assertNull(filter.process(event));
	}

	@Test
	public void testObjectTypeDeletedEventFilter() {
		ObjectTypeDeletedEventFilter filter = new ObjectTypeDeletedEventFilter("WSN.*");
		ObjectTypeDeletedEvent event = new ObjectTypeDeletedEvent("WSNNODE");
		assertNotNull(filter.process(event));
		event = new ObjectTypeDeletedEvent("ROOT");
		assertNull(filter.process(event));
	}

	@Test
	public void testObjectCreatedEventFilter() {
		ObjectCreatedEvent event = new ObjectCreatedEvent(nightTO);
		ObjectCreatedEventFilter filter = new ObjectCreatedEventFilter(matcher1);
		assertNotNull(filter.process(event));

		event = new ObjectCreatedEvent(winterTO);
		filter = new ObjectCreatedEventFilter(matcher2);
		assertNull(filter.process(event));
	}

	@Test
	public void testObjectDeletedEventFilter() {
		ObjectDeletedEvent event = new ObjectDeletedEvent(nightTO);
		ObjectDeletedEventFilter filter = new ObjectDeletedEventFilter(matcher1);
		assertNotNull(filter.process(event));

		event = new ObjectDeletedEvent(winterTO);
		filter = new ObjectDeletedEventFilter(matcher2);
		assertNull(filter.process(event));
	}

	@Test
	public void testRelationSetEventFilter() {
		RelationSetEvent event = new RelationSetEvent(noteTO, winterTO);
		RelationSetEventFilter filter = new RelationSetEventFilter(matcher2, matcher1);
		assertNotNull(filter.process(event));

		event = new RelationSetEvent(unknownTO, sensorTO);
		filter = new RelationSetEventFilter(null, matcher2);
		assertNotNull(filter.process(event));

		event = new RelationSetEvent(sensorTO, unknownTO);
		filter = new RelationSetEventFilter(matcher2, null);
		assertNotNull(filter.process(event));

		event = new RelationSetEvent(unknownTO, sensorTO);
		filter = new RelationSetEventFilter(matcher1, matcher2);
		assertNull(filter.process(event));
	}

	@Test
	public void testRelationRemovedEventFilter() {
		RelationRemovedEvent event = new RelationRemovedEvent(noteTO, winterTO);
		RelationRemovedEventFilter filter = new RelationRemovedEventFilter(matcher2, matcher1);
		assertNotNull(filter.process(event));

		event = new RelationRemovedEvent(unknownTO, sensorTO);
		filter = new RelationRemovedEventFilter(null, matcher2);
		assertNotNull(filter.process(event));

		event = new RelationRemovedEvent(sensorTO, unknownTO);
		filter = new RelationRemovedEventFilter(matcher2, null);
		assertNotNull(filter.process(event));

		event = new RelationRemovedEvent(unknownTO, sensorTO);
		filter = new RelationRemovedEventFilter(matcher1, matcher2);
		assertNull(filter.process(event));
	}
}
