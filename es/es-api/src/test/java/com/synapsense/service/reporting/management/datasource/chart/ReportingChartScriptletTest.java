package com.synapsense.service.reporting.management.datasource.chart;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.BeforeClass;
import org.junit.Test;

import com.synapsense.exception.ObjectExistsException;
import com.synapsense.service.reporting.dto.ReportTaskInfo;
import com.synapsense.service.reporting.dto.ReportTemplateInfo;
import com.synapsense.service.reporting.management.ReportParameter;
import com.synapsense.service.reporting.management.ReportingException;
import com.synapsense.service.reporting.management.ReportingManagementService;
import com.synapsense.service.reporting.olap.custom.Cube;
import com.synapsense.service.reporting.service.ReportingServiceTestUtils;

public class ReportingChartScriptletTest {
	
	
	private static final String TEST_REPORT_TEMPLATE_NAME = "Room RH and RI_Customization";
	
	private static final String TEST_REPORT_TASK_NAME = "cu_test";
	
	private int taskId = 0;
	
	@BeforeClass
	public static void setup() {
		ReportingServiceTestUtils.init();
	}

	@Test
	public final void customization() throws ObjectExistsException, ReportingException, JSONException{
		
		ReportTaskInfo taskInfo = prepareTaskInfo();
		ReportingManagementService rm = ReportingServiceTestUtils.getReportingManagementService();
		rm.runComplexReportTask(taskInfo);
		
		Collection<ReportTaskInfo> reportTasks = rm.getReportTasks(TEST_REPORT_TEMPLATE_NAME);
		for (ReportTaskInfo reportTask: reportTasks) {
        	if (reportTask.getName().equals(TEST_REPORT_TASK_NAME)) {
        		taskId = reportTask.getTaskId();
        	}
        }
		rm.removeReportTask(taskId);		
				
	}

	private ReportTaskInfo prepareTaskInfo() throws JSONException, ReportingException {

		String cronExpression =  null;
		String[] exportFormats =  new String[]{"html"};

        String templateName =  TEST_REPORT_TEMPLATE_NAME;
        String reportName =  TEST_REPORT_TASK_NAME;

        ReportingManagementService rm = ReportingServiceTestUtils.getReportingManagementService();
        ReportTemplateInfo templateInfo = rm.getReportTemplateInfo(templateName);

        Collection<ReportParameter> reportParameters = templateInfo.getParameters();
        Map<String, Class<?>> parsTypes = new HashMap<String, Class<?>>();

        for(ReportParameter rp : reportParameters){
            parsTypes.put(rp.getName(), rp.getType());
        }

        JSONArray jsonArray = new JSONArray();
        jsonArray.put(new JSONObject().put("name", "Objects").put("value","ROOM:193"));
        jsonArray.put(new JSONObject().put("name", "AggregationRange").put("value", Cube.LEVEL_HOUR));
        jsonArray.put(new JSONObject().put("name", "Report Title").put("value", "test"));
        jsonArray.put(new JSONObject().put("name", "DateRange").put("value", "{\"selectorType\":\"Predefined\",\"selectorValue\":\"0\"}"));
        
        JSONObject columnProperties = new JSONObject();
        
        //Properties
        JSONArray properties = new JSONArray();
        
        JSONObject propertyValue1 = new JSONObject()
        	.put("name", "ROOM.rhAmbient.avg")
        	.put("value", true)
        	.put("lineColor", 65280);
        properties.put(propertyValue1);
        
        JSONObject propertyValue2 = new JSONObject()
	    	.put("name", "ROOM.avgRI.avg")
	    	.put("value", true)
	    	.put("lineColor", 8388608);
    	properties.put(propertyValue2);
        
        columnProperties.put("properties", properties);
        
        JSONArray dimensions = new JSONArray();
        JSONObject dimension = new JSONObject()
        	.put("name", "200")
        	.put("value", new JSONObject().put("start", 20).put("end", 80));
        dimensions.put(dimension);
        columnProperties.put("dimensions", dimensions);
        
        jsonArray.put(new JSONObject().put("name", "ColumnProperties").put("value", columnProperties));

        ReportTaskInfo taskInfo = ReportTaskInfo.newInstance(templateName, reportName);

        taskInfo.setCron(cronExpression);
        taskInfo.addExportFormats(Arrays.asList(exportFormats));

        for(int i = 0; i < jsonArray.length(); i++) {
            JSONObject obj = jsonArray.getJSONObject(i);
            String paramName = obj.getString("name");
            Object value = obj.get("value");

            Class<?> parClass = parsTypes.get(paramName);
            Object paramValue = rm.parseReportParameterValue(value.toString(), parClass);
            taskInfo.addParameterValue(paramName, paramValue);
        }
		
		return taskInfo;
	}

}
