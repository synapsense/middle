package com.synapsense.service.reporting.management.datasource;

import java.util.Iterator;
import java.util.List;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.rules.TestName;
import com.synapsense.service.reporting.DimensionalQuery;
import com.synapsense.service.reporting.datasource.ReportingTableDataSource;
import com.synapsense.service.reporting.dto.CellTO;
import com.synapsense.service.reporting.dto.RowTO;
import com.synapsense.service.reporting.dto.TableTO;
import com.synapsense.service.reporting.olap.AxisSet;
import com.synapsense.service.reporting.olap.EmptySet;
import static com.synapsense.service.reporting.service.ReportingServiceTestUtils.init;

public class AbstractReportingDataSourceTest {
	static final int ROW_HEADER = 0;
	static final int COLUMN_HEADER = 1;
	static DimensionalQuery query;
	ReportingTableDataSource dataSource;

	@BeforeClass
	public static void setUp() throws Exception {
		init();
	}

	@Rule
	public TestName name = new TestName();

	AxisSet slicer;
	AxisSet rows;
	AxisSet columns;

	Object slicerObject;
	Object rowsObject;
	Object columnsObject;
	
	TableTO result;
	static final String RACKS = "RACK";
	static final String ROOMS = "ROOM";
	static final String DC = "DC";

	@Before
	public void outputName() {
		rows = new EmptySet();
		columns = new EmptySet();
		slicer = new EmptySet();
		
		System.out.println("Sample name: " + name.getMethodName());
	}

	@After
	public void printQuery() {
		System.out.println("Rows dimensionalities: " + rows.getDimensionalities());
		System.out.println("Rows: " + rows);
		System.out.println("Columns dimensionalities: " + columns.getDimensionalities());
		System.out.println("Columns: " + columns);
		System.out.println("Slicer dimensionalities: " + slicer.getDimensionalities());
		System.out.println("Slicer: " + slicer);
		if (result != null){
			Iterator<String> rowHeadersIterator = result.getHeader(ROW_HEADER).iterator();
			List<String> columnHeaders = result.getHeader(COLUMN_HEADER);
			System.out.println("Resulting Table:");
			Iterator<RowTO> rowIterator = result.iterator();
			System.out.print("Columns: ");
			for (String header : columnHeaders){
				System.out.print(header + " | ");
			}
			System.out.println();
			int rowIndex = 0;
			while (rowIterator.hasNext()) {
				RowTO row = rowIterator.next();
				System.out.print(rowHeadersIterator.next() + "| ");
				int columnIndex = 0;
				for (CellTO cell : row) {
					System.out.print("Row" + rowIndex + "Column" + columnIndex + " Value: " + cell.getValue() + " | ");
					columnIndex++;
				}
				System.out.println();
				rowIndex++;
			}
		}
	}

}