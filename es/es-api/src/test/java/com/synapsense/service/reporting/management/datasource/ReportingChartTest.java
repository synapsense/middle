package com.synapsense.service.reporting.management.datasource;

import java.util.Arrays;
import java.util.Collection;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import com.synapsense.service.reporting.datasource.ReportingTableDataSource;
import com.synapsense.service.reporting.olap.RangeSet;
import com.synapsense.service.reporting.olap.custom.Cube;
import com.synapsense.service.reporting.olap.custom.TimePeriodType;

@RunWith(Parameterized.class)
public class ReportingChartTest extends AbstractReportingDataSourceTest{
	
	private Boolean expectedResult;
	
	public ReportingChartTest(String rowsObject,
			Boolean expectedResult) {
		this.rowsObject = rowsObject;
		this.expectedResult = expectedResult;
	}
	
	@Parameterized.Parameters
	public static Collection<Object[]> primeNumbers() {
		return Arrays.asList(new Object[][] { { RACKS, true }, { ROOMS, true },
				{ DC, true } });
	}
	   
	@Test
	public final void today() {
		slicerObject = new RangeSet(Cube.TIME, TimePeriodType.TODAY);
		getData();
	}

	@Test
	public final void yesterday() {
		slicerObject = new RangeSet(Cube.TIME, TimePeriodType.YESTERDAY);
		getData();
	}

	@Test
	public final void thisWeek() {
		slicerObject = new RangeSet(Cube.WEEK, TimePeriodType.THIS_WEEK);
		getData();
	}

	@Test
	public final void lastWeek() {
		slicerObject = new RangeSet(Cube.WEEK, TimePeriodType.LAST_WEEK);
		getData();
	}

	@Test
	public final void thisMonth() {
		slicerObject = new RangeSet(Cube.TIME, TimePeriodType.THIS_MONTH);
		getData();
	}

	@Test
	public final void lastMonth() {
		slicerObject = new RangeSet(Cube.TIME, TimePeriodType.LAST_MONTH);
		getData();
	}

	@Test
	public final void twoHours() {
		slicerObject = RangeSet.newCustomRangeSet(Cube.TIME, 2, Cube.LEVEL_HOUR);
		getData();
	}

	@Test
	public final void twoDays() {
		slicerObject = RangeSet.newCustomRangeSet(Cube.TIME, 2, Cube.LEVEL_DAY);
		getData();
	}

	@Test
	public final void twoWeeks() {
		slicerObject = RangeSet.newCustomRangeSet(Cube.WEEK, 2, Cube.LEVEL_WEEK);
		getData();
	}

	@Test
	public final void twoMonths() {
		slicerObject = RangeSet.newCustomRangeSet(Cube.TIME, 2, Cube.LEVEL_MONTH);
		getData();
	}

	protected void getData() {
		
    	String[] propertiesArray = new String[] { "RACK.cTop.avg", "RACK.cMid.avg", "RACK.cBot.avg" };
    	
    	String aggregationRange = "Hour";
    	RangeSet period = (RangeSet) slicerObject;
    	
        dataSource = ReportingTableDataSource.newChart(
        		rowsObject, propertiesArray, aggregationRange, period);
		result = dataSource.getData();
		
	}

}