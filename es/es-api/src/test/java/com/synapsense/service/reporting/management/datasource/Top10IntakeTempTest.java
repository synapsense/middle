package com.synapsense.service.reporting.management.datasource;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import com.synapsense.service.reporting.Formula;
import com.synapsense.service.reporting.Formula.TransformType;
import com.synapsense.service.reporting.datasource.ReportingTableDataSource;
import com.synapsense.service.reporting.olap.AllMemberSet;
import com.synapsense.service.reporting.olap.RangeSet;
import com.synapsense.service.reporting.olap.custom.Cube;
import com.synapsense.service.reporting.olap.custom.TimePeriodType;
import com.synapsense.util.CollectionUtils;

@RunWith(Parameterized.class)
public class Top10IntakeTempTest extends AbstractReportingDataSourceTest {

	private Boolean expectedResult;
	
	public Top10IntakeTempTest(String columnsObject,
			Boolean expectedResult) {
		this.columnsObject = columnsObject;
		this.expectedResult = expectedResult;
	}
	
	
	@Parameterized.Parameters
	public static Collection<Object[]> primeNumbers() {
		return Arrays.asList(new Object[][] { { RACKS, true }, { ROOMS, true },
				{ DC, true } });
	}
	   
	@Test
	public final void today() {
		rowsObject = new RangeSet(Cube.TIME, TimePeriodType.TODAY);
		getData();
	}

	@Test
	public final void yesterday() {
		rowsObject = new RangeSet(Cube.TIME, TimePeriodType.YESTERDAY);
		getData();
	}

	@Test
	public final void thisWeek() {
		rowsObject = new RangeSet(Cube.WEEK, TimePeriodType.THIS_WEEK);
		getData();
	}

	@Test
	public final void lastWeek() {
		rowsObject = new RangeSet(Cube.WEEK, TimePeriodType.LAST_WEEK);
		getData();
	}

	@Test
	public final void thisMonth() {
		rowsObject = new RangeSet(Cube.TIME, TimePeriodType.THIS_MONTH);
		getData();
	}

	@Test
	public final void lastMonth() {
		rowsObject = new RangeSet(Cube.TIME, TimePeriodType.LAST_MONTH);
		getData();
	}

	@Test
	public final void twoHours() {
		rowsObject = RangeSet.newCustomRangeSet(Cube.TIME, 2, Cube.LEVEL_HOUR);
		getData();
	}

	@Test
	public final void twoDays() {
		rowsObject = RangeSet.newCustomRangeSet(Cube.TIME, 2, Cube.LEVEL_DAY);
		getData();
	}

	@Test
	public final void twoWeeks() {
		rowsObject = RangeSet.newCustomRangeSet(Cube.WEEK, 2, Cube.LEVEL_WEEK);
		getData();
	}

	@Test
	public final void twoMonths() {
		rowsObject = RangeSet.newCustomRangeSet(Cube.TIME, 2, Cube.LEVEL_MONTH);
		getData();
	}
	
	protected void getData() {
		
		columns = new AllMemberSet(Cube.OBJECT, (String)columnsObject);
		
		List<String[]> properties = CollectionUtils.newArrayList(1);
		properties.add(new String[] { "RACK.cTop.avg", "RACK.cMid.avg", "RACK.cBot.avg" });
		slicerObject = properties;

		dataSource = ReportingTableDataSource.newAggregatedTable(new Formula("avg", Cube.LEVEL_DAY, TransformType.COLUMN_10)
				, slicerObject
				, rowsObject 
				, columnsObject);
		result = dataSource.getData();
		
	}
	
}
