package com.synapsense.service.reporting;

import java.util.List;

import junit.framework.Assert;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;

import com.synapsense.service.reporting.olap.Member;
import com.synapsense.service.reporting.olap.OrderedHierarchy;
import com.synapsense.service.reporting.olap.RangeSet;
import com.synapsense.service.reporting.olap.Tuple;
import com.synapsense.service.reporting.olap.RangeSet.*;
import com.synapsense.service.reporting.olap.SimpleMember;
import com.synapsense.service.reporting.olap.custom.BaseLevel;
import com.synapsense.service.reporting.olap.custom.Cube;
import com.synapsense.service.reporting.olap.custom.Level;
import com.synapsense.service.reporting.olap.custom.TimeHierarchy;
import com.synapsense.service.reporting.olap.custom.TimePeriodType;
import com.synapsense.service.reporting.olap.custom.WeekLevel;

public class TimeHierarchyTest {
	private static final String CUSTOM_PERIOD = "Custom";
	private static final String PREDEFINED_PERIOD = "Predefined";
	private static final String SELECTOR_TYPE = "selectorType";
	private static final String SELECTOR_VALUE = "selectorValue";
	
	@Test
	public void simpleYearMember() {
		TimeHierarchy h = new TimeHierarchy();
		SimpleMember year = new SimpleMember(Cube.TIME, Cube.LEVEL_YEAR, 2012);
		SimpleMember month = new SimpleMember(Cube.TIME, Cube.LEVEL_MONTH, 5, year);
		List<Member> members = h.getMembers(new BaseLevel(Cube.TIME, Cube.LEVEL_DAY), month);
		System.out.println(members);
		System.out.println(members.size());
	}
	
	@Test
	public void startEndMembers() {
		TimeHierarchy h = new TimeHierarchy();
		SimpleMember year = new SimpleMember(Cube.TIME, Cube.LEVEL_YEAR, 2012);
		SimpleMember month = new SimpleMember(Cube.TIME, Cube.LEVEL_MONTH, 5, year);
		SimpleMember startDay = new SimpleMember(Cube.TIME, Cube.LEVEL_DAY, 1, month);
		SimpleMember endDay = new SimpleMember(Cube.TIME, Cube.LEVEL_DAY, 25, month);

		List<Member> members = h.getMembers(startDay, endDay);
		System.out.println(members);
		System.out.println(members.size());
	}
	
	
	@Test
	public void getPreviousMember() throws JSONException {
		Level weekLevel = new WeekLevel();
		OrderedHierarchy weekHierarchy = (OrderedHierarchy) weekLevel.getHierarchy();
		
		RangeSet customWeek = new RangeSet(weekHierarchy, 1, Cube.LEVEL_WEEK);
		String weeksUIName = UILevelNameType.getTypeByName(Cube.LEVEL_WEEK).getUiLevelName();
		Assert.assertEquals("toString is incorrect", "{\"" + SELECTOR_TYPE + "\":\"" + CUSTOM_PERIOD + "\",\"" + SELECTOR_VALUE + "\":\"1:" + weeksUIName + "\"}", customWeek.toString());
		System.out.println(customWeek);
		
		RangeSet customHour = new RangeSet(Cube.TIME, 1, Cube.LEVEL_HOUR);
		String hoursUIName = UILevelNameType.getTypeByName(Cube.LEVEL_HOUR).getUiLevelName();
		Assert.assertEquals("toString is incorrect", "{\"" + SELECTOR_TYPE + "\":\"" + CUSTOM_PERIOD + "\",\"" + SELECTOR_VALUE + "\":\"1:" + hoursUIName + "\"}", customHour.toString());
		System.out.println(customHour);
		
		RangeSet customDay = new RangeSet(Cube.TIME, 1, Cube.LEVEL_DAY);
		String daysUIName = UILevelNameType.getTypeByName(Cube.LEVEL_DAY).getUiLevelName();
		Assert.assertEquals("toString is incorrect", "{\"" + SELECTOR_TYPE + "\":\"" + CUSTOM_PERIOD + "\",\"" + SELECTOR_VALUE + "\":\"1:" + daysUIName + "\"}", customDay.toString());
		System.out.println(customDay);
		
		RangeSet customMonth = new RangeSet(Cube.TIME, 1, Cube.LEVEL_MONTH);
		String monthsUIName = UILevelNameType.getTypeByName(Cube.LEVEL_MONTH).getUiLevelName();
		Assert.assertEquals("toString is incorrect", "{\"" + SELECTOR_TYPE + "\":\"" + CUSTOM_PERIOD + "\",\"" + SELECTOR_VALUE + "\":\"1:" + monthsUIName + "\"}", customMonth.toString());
		System.out.println(customMonth);

		JSONObject object = new JSONObject();
		object.put(SELECTOR_TYPE, PREDEFINED_PERIOD);
		object.put(SELECTOR_VALUE, TimePeriodType.TODAY.getId());
		
		RangeSet today = new RangeSet((OrderedHierarchy)TimePeriodType.TODAY.getLevel().getHierarchy(), TimePeriodType.getTypeById(object.getInt(SELECTOR_VALUE)));		
		Assert.assertEquals("toString is incorrect", "{\"" + SELECTOR_TYPE + "\":\"" + PREDEFINED_PERIOD + "\",\"" + SELECTOR_VALUE + "\":\"0\"}", today.toString());
		System.out.println(TimePeriodType.TODAY + ": " + today);
		RangeSet yesterday = new RangeSet((OrderedHierarchy)TimePeriodType.YESTERDAY.getLevel().getHierarchy(), TimePeriodType.YESTERDAY);
		Assert.assertEquals("toString is incorrect", "{\"" + SELECTOR_TYPE + "\":\"" + PREDEFINED_PERIOD + "\",\"" + SELECTOR_VALUE + "\":\"1\"}", yesterday.toString());
		System.out.println(TimePeriodType.YESTERDAY + ": " + yesterday);
		RangeSet thisWeek = new RangeSet((OrderedHierarchy)TimePeriodType.THIS_WEEK.getLevel().getHierarchy(), TimePeriodType.THIS_WEEK);
		Assert.assertEquals("toString is incorrect", "{\"" + SELECTOR_TYPE + "\":\"" + PREDEFINED_PERIOD + "\",\"" + SELECTOR_VALUE + "\":\"2\"}", thisWeek.toString());
		System.out.println(TimePeriodType.THIS_WEEK + ": " + thisWeek);
		RangeSet lastWeek = new RangeSet((OrderedHierarchy)TimePeriodType.LAST_WEEK.getLevel().getHierarchy(), TimePeriodType.LAST_WEEK);
		Assert.assertEquals("toString is incorrect", "{\"" + SELECTOR_TYPE + "\":\"" + PREDEFINED_PERIOD + "\",\"" + SELECTOR_VALUE + "\":\"3\"}", lastWeek.toString());
		System.out.println(TimePeriodType.LAST_WEEK + ": " + lastWeek);
		RangeSet thisMonth = new RangeSet((OrderedHierarchy)TimePeriodType.THIS_MONTH.getLevel().getHierarchy(), TimePeriodType.THIS_MONTH);
		Assert.assertEquals("toString is incorrect", "{\"" + SELECTOR_TYPE + "\":\"" + PREDEFINED_PERIOD + "\",\"" + SELECTOR_VALUE + "\":\"4\"}", thisMonth.toString());
		System.out.println(TimePeriodType.THIS_MONTH + ": " + thisMonth);
		RangeSet lastMonth = new RangeSet((OrderedHierarchy)TimePeriodType.LAST_MONTH.getLevel().getHierarchy(), TimePeriodType.LAST_MONTH);
		Assert.assertEquals("toString is incorrect", "{\"" + SELECTOR_TYPE + "\":\"" + PREDEFINED_PERIOD + "\",\"" + SELECTOR_VALUE + "\":\"5\"}", lastMonth.toString());
		System.out.println(TimePeriodType.LAST_MONTH + ": " + lastMonth);		
	}
	
	@Test
	public void rangeSet() {
		RangeSet rangeSet = new RangeSet(Cube.WEEK, TimePeriodType.LAST_WEEK);
		rangeSet.setAggregationRange(Cube.LEVEL_DAY);
		List<Tuple> tuples = rangeSet.getTuples();
		Assert.assertEquals("Week days number is not 7", 7, tuples.size());
		for (Tuple tuple : tuples) {
			System.out.println(tuple);
		}
		
		rangeSet = new RangeSet(Cube.WEEK, TimePeriodType.LAST_WEEK);
		rangeSet.setAggregationRange(Cube.LEVEL_HOUR);
		tuples = rangeSet.getTuples();
		Assert.assertEquals("Week hours number is not 168", 168, tuples.size());
		for (Tuple tuple : tuples) {
			System.out.println(tuple);
		}
	}
}
