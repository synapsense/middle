package com.synapsense.service.reporting.management.datasource;

import java.util.Arrays;
import java.util.Collection;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import com.synapsense.service.reporting.Formula;
import com.synapsense.service.reporting.Formula.TransformType;
import com.synapsense.service.reporting.datasource.ReportingTableDataSource;
import com.synapsense.service.reporting.olap.AllMemberSet;
import com.synapsense.service.reporting.olap.custom.Cube;

@RunWith(Parameterized.class)
public class RackInletsTest extends AllReadingIntakeTempTest{
	private Boolean expectedResult;
	
	public RackInletsTest(String rowsObject,
			Boolean expectedResult) {
		super(rowsObject, expectedResult);
	}
	
	@Parameterized.Parameters
	public static Collection<Object[]> primeNumbers() {
		return Arrays.asList(new Object[][] { { RACKS, true } });
	}
	
	@Override
	protected void getData() {
		
		rows = new AllMemberSet(Cube.OBJECT, (String)rowsObject);
		
		String[] properties = new String[] { "RACK.cBot.min", "RACK.cBot.avg",
				"RACK.cBot.max", "RACK.cMid.min", "RACK.cMid.avg",
				"RACK.cMid.max", "RACK.cTop.min", "RACK.cTop.avg",
				"RACK.cTop.max" };
		columnsObject = properties;
		
		dataSource = ReportingTableDataSource.newAggregatedTable(new Formula(Cube.ALL_RANGE, TransformType.ORIGINAL)
				, slicerObject
				, rowsObject 
				, columnsObject);
		result = dataSource.getData();
		
	}

}