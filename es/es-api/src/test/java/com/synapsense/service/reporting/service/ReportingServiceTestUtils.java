package com.synapsense.service.reporting.service;

import java.util.Calendar;
import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.junit.BeforeClass;

import com.synapsense.service.Environment;
import com.synapsense.service.UserManagementService;
import com.synapsense.service.reporting.ReportingService;
import com.synapsense.service.reporting.management.ReportingManagementService;
import com.synapsense.service.reporting.olap.Member;
import com.synapsense.service.reporting.olap.custom.BaseLevel;
import com.synapsense.service.reporting.olap.custom.Cube;
import com.synapsense.service.reporting.olap.custom.ReportingEnvironmentDAO;
import com.synapsense.util.JNDINames;
import com.synapsense.util.ServerLoginModule;

public class ReportingServiceTestUtils {
	
	public static final String ADMIN = "admin";

	private static ReportingEnvironmentDAO reportingEnvDAO;
	private static ReportingService reportingService;
	private static ReportingManagementService reportingManagementService;
	private static Environment env;
	private static UserManagementService userManagement;
	private static InitialContext ctx;
	
	public static final int ROW_HEADER = 0;
	public static final int COLUMN_HEADER = 1;
	
	private ReportingServiceTestUtils() {
		init();
	}
	
	public static ReportingEnvironmentDAO getReportingEnvironmentDAO() {
		if (ctx == null) {
			init();
		}
		if (reportingEnvDAO == null) {
			reportingEnvDAO = new ReportingEnvironmentDAO();
			Cube.setReportingDataSource(reportingEnvDAO);
		} 
		return reportingEnvDAO;
	}
	
	public static ReportingService getReportingService() {
		if (ctx == null) {
			init();
		}
		if (reportingService == null) {
			reportingService = getProxy(ctx, JNDINames.REPORTING_SERVICE_NAME, ReportingService.class);
		} 
		return reportingService;
	}

	public static ReportingManagementService getReportingManagementService() {
		if (ctx == null) {
			init();
		}
		if (reportingService == null) {
			reportingManagementService = getProxy(ctx,
					JNDINames.REPORTING_MANAGEMENT_SERVICE_NAME,
					ReportingManagementService.class);
		} 
		return reportingManagementService;		
	}
	
	public static Member currentTimeLevel(String levelName) {
		Calendar calendar = Calendar.getInstance();
		return Cube.TIME.getMemberOfCalendar(calendar, new BaseLevel(Cube.TIME, levelName), new BaseLevel(Cube.TIME, Cube.LEVEL_YEAR));
	}
	
	@BeforeClass
	public static void init() {
		final String SERVER_NAME = "localhost";
		final String INITIAL_CONTEXT_FACTORY = "org.jboss.naming.remote.client.InitialContextFactory";
		final String PROVIDER_URL = "remote://" + SERVER_NAME + ":4447";

		Properties connectProperties = new Properties();
		connectProperties.put(Context.INITIAL_CONTEXT_FACTORY, INITIAL_CONTEXT_FACTORY);
		connectProperties.put(Context.PROVIDER_URL, PROVIDER_URL);

		connectProperties.put("jboss.naming.client.ejb.context", "true");
		connectProperties.put("jboss.naming.client.connect.options.org.xnio.Options.SASL_POLICY_NOPLAINTEXT", "false");

		connectProperties.put(Context.SECURITY_PRINCIPAL, "admin");
		connectProperties.put(Context.SECURITY_CREDENTIALS, "admin");
		connectProperties.put("SynapEnvRemote", "SynapServer/Environment/remote");

		connectProperties.put(Context.URL_PKG_PREFIXES, "org.jboss.naming.client:org.jnp.interfaces");

		try {
			ctx = new InitialContext(connectProperties);
			reportingEnvDAO = new ReportingEnvironmentDAO();
			Cube.setReportingDataSource(reportingEnvDAO);
//			reporingService = ServerLoginModule.getProxy(ctx, JNDINames.REPORTING_SERVICE_NAME, ReportingService.class);
//			reporingManagementService = ServerLoginModule.getProxy(ctx, JNDINames.REPORTING_MANAGEMENT_SERVICE_NAME, ReportingManagementService.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	protected static <INTERFACE> INTERFACE getProxy(InitialContext ctx, String jndiTarget, Class<INTERFACE> clazz) {
		try {
			return clazz.cast(ServerLoginModule.getProxy(ctx, jndiTarget, clazz));
		} catch (NamingException e) {
			throw new IllegalStateException("Error acquiring reference to " + jndiTarget, e);
		}
	}

	public static Environment getEnvironment() {
		if (ctx == null) {
			init();
		}
		if (env == null) {
			env = getProxy(ctx, JNDINames.ENVIRONMENT, Environment.class);
		}
		return env;
	}

	public static UserManagementService getUserManagement() {
		if (ctx == null) {
			init();
		}
		if (userManagement == null) {
			userManagement = getProxy(ctx, JNDINames.USER_SERVICE_JNDI_NAME, UserManagementService.class);
		}
		return userManagement;
	}

}
