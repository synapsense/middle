package com.synapsense.service.reporting.management.datasource.chart;

import static com.synapsense.service.reporting.service.ReportingServiceTestUtils.ADMIN;
import static com.synapsense.service.reporting.service.ReportingServiceTestUtils.getEnvironment;
import static com.synapsense.service.reporting.service.ReportingServiceTestUtils.getUserManagement;

import java.awt.Color;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.jfree.chart.JFreeChart;
import org.jfree.chart.annotations.XYBoxAnnotation;
import org.jfree.chart.plot.ValueMarker;
import org.jfree.chart.plot.XYPlot;
import org.jfree.ui.Layer;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;
import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.EnvException;
import com.synapsense.service.Environment;
import com.synapsense.service.UserManagementService;
import com.synapsense.service.reporting.olap.RangeSet;
import com.synapsense.service.reporting.olap.custom.Cube;
import com.synapsense.service.reporting.scriplets.CustomQueryChartScriptlet;
import com.synapsense.service.reporting.scriplets.charts.AdvancedChartData;
import com.synapsense.service.reporting.scriplets.charts.AdvancedChartData.ThresholdData;
import com.synapsense.service.reporting.scriplets.charts.ChartDescr;
import com.synapsense.service.reporting.scriplets.charts.ChartDescr.CQ_FIELD;
import com.synapsense.util.CollectionUtils;

@RunWith(Parameterized.class)
public class AdvancedChartScriptletTest {

    private TO<?> customQuery;
    
    private Boolean expectedResult;
    private RangeSet period = RangeSet.newCustomRangeSet(Cube.TIME, 2, Cube.LEVEL_HOUR);

	private static final String CUSTOMQUERY = "CUSTOMQUERY";
	private static final String QUERY_DESC = "queryDesc";
	private static final String ASSOCIATED_SENSORS = "associatedSensors";
	private static final String NUM_ALERTS = "numAlerts";
    private static final String USER_ID = "userId";
    private static final String NAME = "name"; 
	private static final String PRIVATE_CHECK = "privateCheck";
	private static final String LINES = "lines";
	private static final String CHARTDESCR = "chartDescr";
	private static Environment env = getEnvironment();
	private static UserManagementService userManagement = getUserManagement();
    	
	static final TO<?> FULL_CUSTOM_QUERY = createFullCustomQuery();
    static final TO<?> NULL_CUSTOM_QUERY = createNullCustomQuery();
	public AdvancedChartScriptletTest(TO<?> customQuery,
                                    Boolean expectedResult) {
		this.customQuery = customQuery;
        this.expectedResult = expectedResult;
    }

    private static final String MAX_MIN_AVG = null;
    private static final String THRESHOLDS = null;
    private static final String BARS = null;

    @After
    public void clean() throws Exception {
    	env.deleteObject(customQuery);       
    }

    @Parameterized.Parameters
    public static Collection<Object[]> primeNumbers() throws EnvException {
        return Arrays.asList(new Object[][]{
                {FULL_CUSTOM_QUERY, true},
                {NULL_CUSTOM_QUERY, true}});
    }

    @Test
    public final void advanced() throws Exception {

    	CustomQueryChartScriptlet scriptlet = new CustomQueryChartScriptlet();
        JFreeChart chart = scriptlet.getChart(customQuery, period);
		AdvancedChartData advancedChartData = scriptlet.getAdvancedChartData();
        XYPlot plot = chart.getXYPlot();
        
        Map<String, Color> colorMap = CollectionUtils.newMap();
        String [] dataArr = env.getPropertyValue(customQuery, CHARTDESCR, String.class).split(",");
        addColor(colorMap, "avg", Integer.valueOf(dataArr[CQ_FIELD.AVG_LINE_COLOR]));
        addColor(colorMap, "min", Integer.valueOf(dataArr[CQ_FIELD.MIN_LINE_COLOR]));
        addColor(colorMap, "max", Integer.valueOf(dataArr[CQ_FIELD.MAX_LINE_COLOR]));
        
        ListMultimap<String, Number> barValueMap = ArrayListMultimap.create();
        Map<String, Number> valueMap = CollectionUtils.newMap();
        if (!advancedChartData.getDimensionMeanMap().isEmpty()) {
        	valueMap.put("avg", advancedChartData.getDimensionMeanMap().entries().iterator().next().getValue());
        }
        if (!advancedChartData.getValueAxisMinimumMap().isEmpty()) {
        	valueMap.put("min", advancedChartData.getValueAxisMinimumMap().entrySet().iterator().next().getValue());
        }
        if (!advancedChartData.getValueAxisMaximumMap().isEmpty()) {
        	valueMap.put("max", advancedChartData.getValueAxisMaximumMap().entrySet().iterator().next().getValue());
        }
        if (Boolean.valueOf(dataArr[CQ_FIELD.USE_THRESHOLDS])) {
	        for (ThresholdData thresholdData : ChartDescr.getThresholds(dataArr[CQ_FIELD.THRESHOLDS])) {
	        	switch (thresholdData.getThresholdType()) {
	        		case LINE:
	        			valueMap.put(String.valueOf(thresholdData.getStartValue()), thresholdData.getStartValue());
	        			colorMap.put(String.valueOf(thresholdData.getStartValue()), new Color(thresholdData.getStartColor()));
	        			break;
	        		case BAR:
	        			colorMap.put(String.valueOf(thresholdData.getStartValue()), new Color(thresholdData.getStartColor()));
	        			barValueMap.put(String.valueOf(thresholdData.getStartValue()), thresholdData.getStartValue());
	        			barValueMap.put(String.valueOf(thresholdData.getStartValue()), thresholdData.getEndValue());
	        			break;
	        	}
	        }
        }
        assertPlot(plot, colorMap, valueMap, barValueMap);
    }
    
    private void addColor(Map<String, Color> colorMap, String aggregation, int value) {
    	if (value != CQ_FIELD.EMPTY) {
    		colorMap.put(aggregation, new Color(value));
    	}
    	
    }
    
    private void assertPlot(XYPlot plot, Map<String, Color> colorMap, Map<String, Number> valueMap, ListMultimap<String, Number> barValueMap) {
        //Assert range markers
        if (valueMap.isEmpty()) {
        	assert plot.getRangeMarkers(Layer.FOREGROUND) == null;
        } else {        	
	        Collection<ValueMarker> rangeMarkers = plot.getRangeMarkers(Layer.FOREGROUND);	        
	        for(ValueMarker marker : rangeMarkers) {
	        	assert colorMap.get(marker.getLabel()).equals(((Color)marker.getPaint()));
	        	Number markerValue = marker.getValue();
	        	assert markerValue == valueMap.get(marker.getLabel());
	        }
    	}
        //Assert bars
        if (barValueMap.isEmpty()) {
        	assert plot.getAnnotations() == null;
        } else {
	        Collection<XYBoxAnnotation> annotaions = plot.getAnnotations();
	        for (XYBoxAnnotation annotation : annotaions) {
	    		String[] tooltip = annotation.getToolTipText().split(":");
	    		//check color
	        	
	        	assert colorMap.get(tooltip[0]).equals(new Color(Integer.valueOf(tooltip[1])));
	        	List<Number> barValues = barValueMap.get(tooltip[0]);
	        	//check startValue
	        	Number start = barValues.get(0);
	        	assert Integer.valueOf(tooltip[0]) == start;
	        	//check endValue
	        	Number end = barValues.get(1);
	        	assert Integer.valueOf(tooltip[2]) == end;       	
	        }
    	}
    }

    private static TO<?> createFullCustomQuery() {
    	String chartDescr = "16777215,true,Data Analysis Chart,bottom,0,3368703,16711680,true,50:16711935:null:-1:null;71:65280:null:-1:null;60:65535:62:65535:null";
    	return createCustomQuery(chartDescr);
    }
    
    private static TO<?> createNullCustomQuery() {
    	String chartDescr = "16777215,true,Data Analysis Chart,bottom,-1,-1,-1,false,";
    	return createCustomQuery(chartDescr);
    }
    
    private static TO<?> createCustomQuery(String chartDescr) {

		TO<?> user = userManagement.getUser(ADMIN);
		int userId = (Integer) user.getID();
    	String associatedSensors = env.getObjectsByType("ROOM").iterator().next().toString() + "->" + "avgST";
    	TO<?> customQuery = null;
		try {
			customQuery = env.createObject(CUSTOMQUERY,
					new ValueTO[] { 
					new ValueTO(QUERY_DESC, ""),
					new ValueTO(ASSOCIATED_SENSORS, associatedSensors),
					new ValueTO(USER_ID, userId),
							new ValueTO(NAME, "test"),
							new ValueTO(PRIVATE_CHECK, 0),
							new ValueTO(LINES, "[[1,0]]"),
							new ValueTO(CHARTDESCR, chartDescr) });
		} catch (EnvException e) {
			e.printStackTrace();
		}
    	return customQuery;
    }
}
