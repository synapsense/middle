package com.synapsense.service.nsvc;

import static org.junit.Assert.*;

import java.io.Serializable;
import java.util.regex.Pattern;

import org.junit.Test;

import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.service.nsvc.filters.TOMatcher;

public class TOMatcherTest {
	String pattern = "SENSOR|N.*";
	TO<?> networkTO = TOFactory.getInstance().loadTO("NETWORK:1");
	TO<?> sensorTO = TOFactory.getInstance().loadTO("SENSOR:7");
	TO<?> nodeTO = TOFactory.getInstance().loadTO("NODE:2");
	TO<?> unknownTO = TOFactory.getInstance().loadTO("UNKNOWN:2");

	public void testSerializability() {
		TOMatcher matcher = new TOMatcher("");
		assertTrue(Serializable.class.isInstance(matcher));
	}

	@Test
	public void testTOMatcherByString() {
		TOMatcher matcher = new TOMatcher(pattern);
		assertEquals(true, matcher.match(sensorTO));
		assertEquals(true, matcher.match(nodeTO));
		assertEquals(true, matcher.match(networkTO));
		assertEquals(false, matcher.match(unknownTO));
	}

	@Test
	public void testTOMatcherByPattern() {
		TOMatcher matcher = new TOMatcher(Pattern.compile(pattern));
		assertEquals(true, matcher.match(sensorTO));
		assertEquals(true, matcher.match(nodeTO));
		assertEquals(true, matcher.match(networkTO));
		assertEquals(false, matcher.match(unknownTO));
	}

	@Test
	public void testTOMatcherByTO() {
		TOMatcher matcher = new TOMatcher(unknownTO);
		assertEquals(false, matcher.match(networkTO));
		assertEquals(false, matcher.match(nodeTO));
		assertEquals(false, matcher.match(sensorTO));
		assertEquals(true, matcher.match(unknownTO));
	}
}
