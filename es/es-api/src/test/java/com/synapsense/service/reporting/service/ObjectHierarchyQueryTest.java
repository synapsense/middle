package com.synapsense.service.reporting.service;

import java.sql.SQLException;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;

import com.synapsense.exception.EnvException;
import com.synapsense.service.reporting.DimensionalQuery;
import com.synapsense.service.reporting.Formula;
import com.synapsense.service.reporting.Formula.TransformType;
import com.synapsense.service.reporting.ReportingService;
import com.synapsense.service.reporting.dto.CellTO;
import com.synapsense.service.reporting.dto.RowTO;
import com.synapsense.service.reporting.dto.TableTO;
import com.synapsense.service.reporting.olap.AllMemberSet;
import com.synapsense.service.reporting.olap.AxisSet;
import com.synapsense.service.reporting.olap.CrossJoin;
import com.synapsense.service.reporting.olap.EmptySet;
import com.synapsense.service.reporting.olap.Member;
import com.synapsense.service.reporting.olap.RangeSet;
import com.synapsense.service.reporting.olap.SimpleMember;
import com.synapsense.service.reporting.olap.TupleSet;
import com.synapsense.service.reporting.olap.custom.BaseLevel;
import com.synapsense.service.reporting.olap.custom.Cube;
import com.synapsense.service.reporting.olap.custom.TimePeriodType;

import static com.synapsense.service.reporting.service.ReportingServiceTestUtils.currentTimeLevel;

public class ObjectHierarchyQueryTest {
	private static ReportingService repServ;
	public static final int ROW_HEADER = 0;
	public static final int COLUMN_HEADER = 1;
	static DimensionalQuery query;

	@BeforeClass
	public static void setUp() throws Exception {
		repServ = ReportingServiceTestUtils.getReportingService();
	}

	@Rule
	public TestName name = new TestName();

	private AxisSet rows;
	private AxisSet columns;
	private AxisSet slicer;
	private TableTO result;

	@Before
	public void outputName() {
		rows = new EmptySet();
		columns = new EmptySet();
		slicer = new EmptySet();
		System.out.println("Sample name: " + name.getMethodName());
	}

	@After
	public void printQuery() {
		System.out.println("Rows dimensionalities: " + rows.getDimensionalities());
		System.out.println("Rows: " + rows);
		System.out.println("Columns dimensionalities: " + columns.getDimensionalities());
		System.out.println("Columns: " + columns);
		System.out.println("Slicer dimensionalities: " + slicer.getDimensionalities());
		System.out.println("Slicer: " + slicer);
		if (result != null){
			Iterator<String> rowHeadersIterator = result.getHeader(ROW_HEADER).iterator();
			List<String> columnHeaders = result.getHeader(COLUMN_HEADER);
			System.out.println("Resulting Table:");
			Iterator<RowTO> rowIterator = result.iterator();
			System.out.print("Columns: ");
			for (String header : columnHeaders){
				System.out.print(header + " | ");
			}
			System.out.println();
			int rowIndex = 0;
			while (rowIterator.hasNext()) {
				RowTO row = rowIterator.next();
				System.out.print(rowHeadersIterator.next() + "| ");
				int columnIndex = 0;
				for (CellTO cell : row) {
					System.out.print("Row" + rowIndex + "Column" + columnIndex + " Value: " + cell.getValue() + " | ");
					columnIndex++;
				}
				System.out.println();
				rowIndex++;
			}
		}
	}

	
	@Test
	public void testNoSlicerNoColumns() {

		// The result report should be as simple as this
		//
		// Data Value
		// Object.RACK.RACK:x,Measures.Name.RACK.cTop.avg,Year.2013,Month.7,Day.1,Hour.0: xx 
		// .......
		// Object.RACK.RACK:x,Measures.Name.RACK.cTop.avg,Year.2013,Month.7,Day.1,Hour.15: xx

		AxisSet objectMembers = new AllMemberSet(Cube.OBJECT, "RACK");
		
		AxisSet timeMembers = new RangeSet(Cube.TIME, TimePeriodType.TODAY);
		Member propertyMembers = new SimpleMember(Cube.PROPERTY, "Name", "RACK"
				+ "." + "cTop" + "." + "avg");

		rows = new CrossJoin(objectMembers, propertyMembers, timeMembers);
		columns = new EmptySet();
		
		Formula formula = new Formula(Cube.LEVEL_HOUR, TransformType.ORIGINAL);
		DimensionalQuery query = new DimensionalQuery(formula, null, rows, columns);
		result = repServ.executeQuery(query);
	}
	
//	@Test
//	public void testObjectHourPagination() {
//
//		// The result report should be as simple as this
//		//
//		// Date, supplyT, returnT
//		// Month x,Year xxxx: xx, xx
//		// .......
//		// Month x,Year xxxx: xx, xx
//		//
//		// for all RACKs
//		
////		slicer = new AllMemberSet(Cube.LEVEL_HOUR, Cube.TIME, Cube.LEVEL_HOUR);
//
//		rows = new AllMemberSet(Cube.OBJECT, "RACK");
//		
//		TupleSet day = new TupleSet(new SimpleMember(Cube.TIME, Cube.LEVEL_DAY, 25
//				, new SimpleMember(Cube.TIME, Cube.LEVEL_MONTH, 1
//						, new SimpleMember(Cube.TIME, Cube.LEVEL_YEAR, 2013))));
//		
//		columns = new TupleSet(new SimpleMember(Cube.PROPERTY, "Name",
//						"RACK.cTop.avg"), new SimpleMember(Cube.PROPERTY, "Name",
//								"RACK.cBot.avg"));
//		
//		Formula formula = new Formula(Cube.LEVEL_DAY, TransformType.ORIGINAL);
//		DimensionalQuery query = new DimensionalQuery(formula, null, rows,
//				columns);
////		result = repServ.executeQuery(query, 1, 1000);
//	}
//	
//	@Test
//	public void testObjectPagination() {
//
//		// The result report should be as simple as this
//		//
//		// Date, supplyT, returnT
//		// Month x,Year xxxx: xx, xx
//		// .......
//		// Month x,Year xxxx: xx, xx
//		//
//		// for all RACKs
//		
//		slicer = new AllMemberSet(Cube.LEVEL_HOUR, Cube.TIME, new BaseLevel(Cube.TIME, Cube.LEVEL_HOUR));
//
//		rows = new AllMemberSet(Cube.OBJECT, "RACK");
//		
//		TupleSet day = new TupleSet(new SimpleMember(Cube.TIME, Cube.LEVEL_DAY, 25
//				, new SimpleMember(Cube.TIME, Cube.LEVEL_MONTH, 1
//						, new SimpleMember(Cube.TIME, Cube.LEVEL_YEAR, 2013))));
//		
//		columns = new CrossJoin(new TupleSet(new SimpleMember(Cube.PROPERTY, "Name",
//						"RACK.cTop"), new SimpleMember(Cube.PROPERTY, "Name",
//						"RACK.cMid"))
//				, day);
//		
//		Formula formula = new Formula(Cube.LEVEL_DAY, TransformType.ORIGINAL);
//		DimensionalQuery query = new DimensionalQuery(formula, slicer, rows,
//				columns);
////		result = repServ.executeQuery(query, 1, 30);
//	}
	
	@Test
	public final void testMinuteTimeAllMemberSetAxis() throws EnvException, SQLException {

		// The result report should be as simple as this
		//
		// Date, RACK1 cTop, RACK1 cMid, RACK2 cTop, RACK2 cMid
		// Month x,Year xxxx: xx, xx
		// .......
		// Month x,Year xxxx: xx, xx
		// This test checks Minute Time AllMemberSet with no parent case

		Calendar calendar = Calendar.getInstance();
		Member today = Cube.TIME.getMemberOfCalendar(calendar, new BaseLevel(Cube.TIME, Cube.LEVEL_DAY), new BaseLevel(Cube.TIME, Cube.LEVEL_YEAR));
		slicer = today;
		
		rows = new AllMemberSet(Cube.TIME, new BaseLevel(Cube.TIME,Cube.LEVEL_ONE_MINUTE)
				, new SimpleMember(Cube.TIME, Cube.LEVEL_HOUR, 12));
		
		columns = new CrossJoin(new AllMemberSet(Cube.OBJECT, "RACK"),
				new TupleSet(new SimpleMember(Cube.PROPERTY, "Name",
						"RACK.cTop.avg"), new SimpleMember(Cube.PROPERTY, "Name",
						"RACK.cMid.avg"))
				);
		
		Formula formula = new Formula(Cube.LEVEL_ONE_MINUTE, TransformType.RAW);
		query = new DimensionalQuery(formula, slicer, rows, columns);
		result = repServ.executeQuery(query);
	}
	
	@Test
	public final void testCrossJoinMinuteTimeAllMemberSetAxis() throws EnvException, SQLException {

		// The result report should be as simple as this
		//
		// Date, RACK1 cTop, RACK1 cMid, RACK2 cTop, RACK2 cMid
		// Month x,Year xxxx: xx, xx
		// .......
		// Month x,Year xxxx: xx, xx
		// This test checks Minute Time AllMemberSet with no parent case
		
		Calendar calendar = Calendar.getInstance();
		Member today = Cube.TIME.getMemberOfCalendar(calendar, new BaseLevel(Cube.TIME, Cube.LEVEL_DAY), new BaseLevel(Cube.TIME, Cube.LEVEL_YEAR));
		columns = new CrossJoin(new AllMemberSet(Cube.OBJECT, "RACK"),
				new TupleSet(new SimpleMember(Cube.PROPERTY, "Name",
						"RACK.cTop.avg"), new SimpleMember(Cube.PROPERTY, "Name",
						"RACK.cMid.avg"))
				, today);
		
		rows = new CrossJoin(new CrossJoin(currentTimeLevel(Cube.LEVEL_HOUR))
				, new AllMemberSet(Cube.LEVEL_HOUR, Cube.TIME, new BaseLevel(Cube.TIME, Cube.LEVEL_ONE_MINUTE)));
		
		Formula formula = new Formula(Cube.LEVEL_ONE_MINUTE, TransformType.RAW);
		query = new DimensionalQuery(formula, slicer, rows, columns);
		result = repServ.executeQuery(query);
	}
	
	@Test
	public void testAllMemberSetSlicer() {

		// The result report should be as simple as this
		//
		// Date, supplyT, returnT
		// Month x,Year xxxx: xx, xx
		// .......
		// Month x,Year xxxx: xx, xx
		//
		// for all RACKs
		
		slicer = new AllMemberSet(Cube.OBJECT, "RACK");

		rows = new AllMemberSet(Cube.LEVEL_YEAR, Cube.TIME, new BaseLevel(Cube.TIME, Cube.LEVEL_MONTH));

		columns = new TupleSet(new SimpleMember(Cube.PROPERTY, "Name",
				"RACK.cTop.avg"), new SimpleMember(Cube.PROPERTY, "Name",
				"RACK.cMid.avg"));
		
		Formula formula = new Formula(Cube.LEVEL_MONTH, TransformType.ORIGINAL);
		DimensionalQuery query = new DimensionalQuery(formula, slicer, rows,
				columns);
		result = repServ.executeQuery(query);
	}

	@Test
	public final void testAllMemberSetAxis() throws EnvException, SQLException {

		// The result report should be as simple as this
		//
		// Date, RACK1 cTop, RACK1 cMid, RACK2 cTop, RACK2 cMid
		// Month x,Year xxxx: xx, xx
		// .......
		// Month x,Year xxxx: xx, xx

		rows = new AllMemberSet(Cube.LEVEL_HOUR, Cube.TIME, new BaseLevel(Cube.TIME, Cube.LEVEL_HOUR));

		Calendar calendar = Calendar.getInstance();
		Member today = Cube.TIME.getMemberOfCalendar(calendar, new BaseLevel(Cube.TIME, Cube.LEVEL_DAY), new BaseLevel(Cube.TIME, Cube.LEVEL_YEAR));
		
		columns = new CrossJoin(new AllMemberSet(Cube.OBJECT, "RACK"),
				new TupleSet(new SimpleMember(Cube.PROPERTY, "Name",
						"RACK.cTop.avg"), new SimpleMember(Cube.PROPERTY, "Name",
						"RACK.cMid.avg"))
				, today);
		
		Formula formula = new Formula(Cube.LEVEL_HOUR, TransformType.ORIGINAL);
		query = new DimensionalQuery(formula, null, rows, columns);
		result = repServ.executeQuery(query);
	}

}
