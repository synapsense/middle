package com.synapsense.service.reporting.management.datasource;

import java.util.Arrays;
import java.util.Collection;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import com.synapsense.service.reporting.datasource.ReportingTableDataSource;
import com.synapsense.service.reporting.olap.AllMemberSet;
import com.synapsense.service.reporting.olap.RangeSet;
import com.synapsense.service.reporting.olap.custom.Cube;
import com.synapsense.service.reporting.olap.custom.TimePeriodType;

@RunWith(Parameterized.class)
public class RacksTempDailyTest extends AbstractReportingDataSourceTest{
	
	protected Boolean expectedResult;
	
	protected String objectType;
	
	protected RangeSet dateRange;
	
	public RacksTempDailyTest(String objectType,
			Boolean expectedResult) {
		this.objectType = objectType;
		this.expectedResult = expectedResult;
	}
	
	@Parameterized.Parameters
	public static Collection<Object[]> primeNumbers() {
		return Arrays.asList(new Object[][] { { RACKS, true }, { ROOMS, true },
				{ DC, true } });
	}
	   
	@Test
	public final void today() {
		dateRange = new RangeSet(Cube.TIME, TimePeriodType.TODAY);
		getData();
	}

	@Test
	public final void yesterday() {
		dateRange = new RangeSet(Cube.TIME, TimePeriodType.YESTERDAY);
		getData();
	}

	@Test
	public final void thisWeek() {
		dateRange = new RangeSet(Cube.WEEK, TimePeriodType.THIS_WEEK);
		getData();
	}

	@Test
	public final void lastWeek() {
		dateRange = new RangeSet(Cube.WEEK, TimePeriodType.LAST_WEEK);
		getData();
	}

	@Test
	public final void thisMonth() {
		dateRange = new RangeSet(Cube.TIME, TimePeriodType.THIS_MONTH);
		getData();
	}

	@Test
	public final void lastMonth() {
		dateRange = new RangeSet(Cube.TIME, TimePeriodType.LAST_MONTH);
		getData();
	}

	@Test
	public final void twoHours() {
		dateRange = RangeSet.newCustomRangeSet(Cube.TIME, 2, Cube.LEVEL_HOUR);
		getData();
	}

	@Test
	public final void twoDays() {
		dateRange = RangeSet.newCustomRangeSet(Cube.TIME, 2, Cube.LEVEL_DAY);
		getData();
	}

	@Test
	public final void twoWeeks() {
		dateRange = RangeSet.newCustomRangeSet(Cube.WEEK, 2, Cube.LEVEL_WEEK);
		getData();
	}

	@Test
	public final void twoMonths() {
		dateRange = RangeSet.newCustomRangeSet(Cube.TIME, 2, Cube.LEVEL_MONTH);
		getData();
	}

	protected void getData() {
		
		String[] properties = new String[] { "RACK.cTop.avg", "RACK.cMid.avg", "RACK.cBot.avg" };
		
		dataSource = ReportingTableDataSource.newRAWTable(objectType
				, properties
				, "Day" 
				, dateRange);
		result = dataSource.getData();
		
	}

}