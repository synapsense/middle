package com.synapsense.service.reporting.service;

import java.sql.SQLException;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;

import com.synapsense.exception.EnvException;
import com.synapsense.service.reporting.DimensionalQuery;
import com.synapsense.service.reporting.Formula;
import com.synapsense.service.reporting.Formula.TransformType;
import com.synapsense.service.reporting.ReportingService;
import com.synapsense.service.reporting.dto.CellTO;
import com.synapsense.service.reporting.dto.RowTO;
import com.synapsense.service.reporting.dto.TableTO;
import com.synapsense.service.reporting.exception.ReportingServiceSystemException;
import com.synapsense.service.reporting.olap.AllMemberSet;
import com.synapsense.service.reporting.olap.AxisSet;
import com.synapsense.service.reporting.olap.CrossJoin;
import com.synapsense.service.reporting.olap.EmptySet;
import com.synapsense.service.reporting.olap.Filter;
import com.synapsense.service.reporting.olap.FilterableAllMemberSet;
import com.synapsense.service.reporting.olap.Member;
import com.synapsense.service.reporting.olap.RangeSet;
import com.synapsense.service.reporting.olap.SimpleMember;
import com.synapsense.service.reporting.olap.Tuple;
import com.synapsense.service.reporting.olap.TupleSet;
import com.synapsense.service.reporting.olap.custom.BaseLevel;
import com.synapsense.service.reporting.olap.custom.Cube;
import com.synapsense.service.reporting.olap.filters.Filters;

public class ImplicitNamedObjectsTest {
	private static ReportingService repServ;
	public static final int ROW_HEADER = 0;
	public static final int COLUMN_HEADER = 1;
	static DimensionalQuery query;

	@BeforeClass
	public static void setUp() throws Exception {
		repServ = ReportingServiceTestUtils.getReportingService();
	}

	@Rule
	public TestName name = new TestName();

	private AxisSet rows;
	private AxisSet columns;
	private AxisSet slicer;
	private TableTO result;

	@Before
	public void outputName() {
		rows = new EmptySet();
		columns = new EmptySet();
		slicer = new EmptySet();
		System.out.println("Sample name: " + name.getMethodName());
	}

	@After
	public void printQuery() {
		System.out.println("Rows dimensionalities: " + rows.getDimensionalities());
		System.out.println("Rows: " + rows);
		System.out.println("Columns dimensionalities: " + columns.getDimensionalities());
		System.out.println("Columns: " + columns);
		System.out.println("Slicer dimensionalities: " + slicer.getDimensionalities());
		System.out.println("Slicer: " + slicer);
		if (result != null){
			Iterator<String> rowHeadersIterator = result.getHeader(ROW_HEADER).iterator();
			List<String> columnHeaders = result.getHeader(COLUMN_HEADER);
			System.out.println("Resulting Table:");
			Iterator<RowTO> rowIterator = result.iterator();
			System.out.print("Columns: ");
			for (String header : columnHeaders){
				System.out.print(header + " | ");
			}
			System.out.println();
			int rowIndex = 0;
			while (rowIterator.hasNext()) {
				RowTO row = rowIterator.next();
				System.out.print(rowHeadersIterator.next() + "| ");
				int columnIndex = 0;
				for (CellTO cell : row) {
					System.out.print("Row" + rowIndex + "Column" + columnIndex + " Value: " + cell.getValue() + " | ");
					columnIndex++;
				}
				System.out.println();
				rowIndex++;
			}
		}
	}
	
	@Test
	public void testAllMemberSetAllTime() {

		// The result report should be as simple as this
		//
		// CRAC, supplyT, returnT
		// CRAC1, xx, xx
		// CRAC2, xx, xx
		// ...........
		// CRACn, xx, xx
		//
		// for all time

		rows = new AllMemberSet(Cube.OBJECT, "CRAC");
		
		columns = new TupleSet(new SimpleMember(Cube.PROPERTY, "Name",
				"CRAC.supplyT.avg"), new SimpleMember(Cube.PROPERTY, "Name",
				"CRAC.returnT.avg"));
		Formula formula = new Formula(Cube.ALL_RANGE, TransformType.ORIGINAL);
		DimensionalQuery query = new DimensionalQuery(formula, null, rows, columns);
		result = repServ.executeQuery(query);
	}
	
	@Test
	public final void testRangeSet() throws EnvException, SQLException {

		// The result report should be as simple as this
		//
		// Date, CRAC1 supplyT, CRAC1 returnT, CRAC2 supplyT, CRAC2 returnT
		// 18/01/2013, xx, xx
		// ........
		// 16/03/2013, xx, xx
		//
		
		rows = RangeSet.newCustomRangeSet(Cube.TIME, 2, Cube.LEVEL_MONTH);
		
		columns = new CrossJoin(new AllMemberSet(Cube.OBJECT, "CRAC"),
				new TupleSet(new SimpleMember(Cube.PROPERTY, "Name",
						"CRAC.supplyT.avg"), new SimpleMember(Cube.PROPERTY,
						"Name", "CRAC.returnT.avg")));

		Formula formula = new Formula(Cube.LEVEL_DAY, TransformType.ORIGINAL);		
		query = new DimensionalQuery(formula, null, rows, columns);
		result = repServ.executeQuery(query);
	}

	@Test
	public void testCrossJoin() {

		// The result report should be as simple as this
		//
		// Year, Room1 Rack cTop August, Room1 CRAC supplyT August, Room1 Rack
		// cTop September, Room1 CRAC supplyT September,....RoomN CRAC supplyT May
		// 2013, ...
		//

		List<Tuple> allYears = new AllMemberSet(Cube.TIME, Cube.LEVEL_YEAR).getTuples();
		Member thisYear = allYears.get(allYears.size() - 1).itemByHierarchy(Cube.TIME);
		rows = new TupleSet(thisYear);

		columns = new CrossJoin(new AllMemberSet(Cube.OBJECT, "ROOM"), new TupleSet(
		        new SimpleMember(Cube.PROPERTY, "Name", "RACK.cTop.avg"), new SimpleMember(Cube.PROPERTY, "Name", "CRAC.supplyT.avg")),
		        RangeSet.newCustomRangeSet(Cube.TIME, 2, Cube.LEVEL_MONTH));

		Formula formula = new Formula(Cube.LEVEL_MONTH, TransformType.ORIGINAL);		
		query = new DimensionalQuery(formula, null, rows, columns);
		result = repServ.executeQuery(query);
	}
	
	@Test
	public void testAllMemberSetRowsAndSlicer() {

		// The result report should be as simple as this
		//
		// CRAC, supplyT, returnT
		// CRAC1, xx, xx
		// CRAC2, xx, xx
		// ...........
		// CRACn, xx, xx
		//
		// for August 2012

		Calendar calendar = Calendar.getInstance();
		Member thisMonth = Cube.TIME.getMemberOfCalendar(calendar, new BaseLevel(Cube.TIME, Cube.LEVEL_MONTH), new BaseLevel(Cube.TIME, Cube.LEVEL_YEAR));
		slicer = thisMonth;

		rows = new AllMemberSet(Cube.OBJECT, "CRAC");

		columns = new TupleSet(new SimpleMember(Cube.PROPERTY, "Name",
				"CRAC.supplyT.avg"), new SimpleMember(Cube.PROPERTY, "Name",
				"CRAC.returnT.avg"));
		
		Formula formula = new Formula(Cube.LEVEL_MONTH, TransformType.ORIGINAL);
		DimensionalQuery query = new DimensionalQuery(formula, slicer, rows, columns);
		result = repServ.executeQuery(query);
	}
	
	@Test
	public final void testSimpleCrossJoin() throws EnvException, SQLException {

		// The result report should be as simple as this
		//
		// Year, Room1 Rack cTop, Room1 Rack cMid,....RoomN RACK cMid
		// Month x,Year xxxx: xx, xx
		// .......
		// Month x,Year xxxx: xx, xx
		//

		rows = new AllMemberSet(Cube.LEVEL_MONTH, Cube.TIME, new BaseLevel(Cube.TIME, Cube.LEVEL_MONTH));
		
		TupleSet properties = new TupleSet(new SimpleMember(Cube.PROPERTY,
				"Name", "RACK.cTop.avg"), new SimpleMember(Cube.PROPERTY, "Name",
				"RACK.cMid.avg"));
		columns = new CrossJoin(new AllMemberSet(Cube.OBJECT, "ROOM"),
				properties);
		
		Formula formula = new Formula(Cube.LEVEL_MONTH, TransformType.ORIGINAL);
		DimensionalQuery query = new DimensionalQuery(formula, null, rows, columns);
		result = repServ.executeQuery(query);
	}
	
	@Test
	public void testFilterableAllMemberSet() {

		// The result report should be as simple as this
		//
		// CRAC, RACK.cTop, RACK.cMid, RACK.cBot
		// RACK1, xx, xx
		// RACK2, xx, xx
		// ...........
		// RACKn, xx, xx
		//
		// for RACKs with 'x' more than 218.0
		
		Filter filter = Filters.moreThan("x", 218.0);
		rows = new FilterableAllMemberSet(Cube.OBJECT, "RACK", filter);

		columns = new TupleSet(new SimpleMember(Cube.PROPERTY, "Name", "RACK.cTop.avg")
			, new SimpleMember(Cube.PROPERTY, "Name", "RACK.cMid.avg")
			, new SimpleMember(Cube.PROPERTY, "Name", "RACK.cBot.avg"));
		
		Formula formula = new Formula(Cube.LEVEL_MONTH, TransformType.ORIGINAL);
		DimensionalQuery query = new DimensionalQuery(formula, null, rows, columns);
		result = repServ.executeQuery(query);
	}

	@Test
	public void testAllMembersSlicer() {

		// The result report should be as simple as this
		//
		// Date, supplyT, returnT
		// Month x,Year xxxx: xx, xx
		// .......
		// Month x,Year xxxx: xx, xx
		//
		// for all CRACs
		slicer = new AllMemberSet(Cube.OBJECT, "CRAC");
		
		rows = new AllMemberSet(Cube.LEVEL_YEAR, Cube.TIME, new BaseLevel(Cube.TIME, Cube.LEVEL_MONTH));
		
		columns = new TupleSet(
				new SimpleMember(Cube.PROPERTY, "Name", "CRAC.supplyT.avg")
				, new SimpleMember(Cube.PROPERTY, "Name", "CRAC.returnT.avg"));
		
		Formula formula = new Formula(Cube.LEVEL_MONTH, TransformType.ORIGINAL);
		DimensionalQuery query = new DimensionalQuery(formula, slicer, rows, columns);
		result = repServ.executeQuery(query);
	}
	
	@Test
	public void testNonFilteredAllMemberSet() {
		
		Filter filter = Filters.lessThan("x", 218.0);
		rows = new FilterableAllMemberSet(Cube.OBJECT, "RACK", filter);
		
		columns = new TupleSet(new SimpleMember(Cube.PROPERTY, "Name", "RACK.cTop.avg"));
		
		Formula formula = new Formula(Cube.LEVEL_MONTH, TransformType.ORIGINAL);
		DimensionalQuery query = new DimensionalQuery(formula, null, rows, columns);
		try {
			result = repServ.executeQuery(query);
			throw new ReportingServiceSystemException("No objects found that meet query criteria");
		} catch (ReportingServiceSystemException e){
		}
	}
	
	@Test
	public final void testTimeAllMemberSet() throws EnvException, SQLException {
		// The result report should be as simple as this
		//
		// Date, CRAC.CRAC1, CRAC.supplyT | CRAC.CRAC1, CRAC.returnT | CRAC.CRAC2, CRAC.supplyT | CRAC.CRAC2, CRAC.returnT
		// Month x,Year xxxx: xx, xx
		// .......
		// Month x,Year xxxx: xx, xx
		//
		// for all time
		
		rows = new AllMemberSet(Cube.LEVEL_YEAR, Cube.TIME, new BaseLevel(Cube.TIME, Cube.LEVEL_MONTH));
		
		columns = new CrossJoin (new AllMemberSet(Cube.OBJECT, "CRAC"),
				new TupleSet(new SimpleMember(Cube.PROPERTY, "Name", "CRAC.supplyT.avg")
				, new SimpleMember(Cube.PROPERTY, "Name", "CRAC.returnT.avg")));
		
		Formula formula = new Formula(Cube.LEVEL_MONTH, TransformType.ORIGINAL);
		query = new DimensionalQuery(formula, null, rows, columns);
		result = repServ.executeQuery(query);
	}
	
}
