package com.synapsense.service.reporting.olap.filters;

import java.util.HashMap;

import junit.framework.Assert;

import org.junit.Test;

import com.synapsense.service.reporting.olap.Filter;
import com.synapsense.service.reporting.olap.filters.Filters;

public class FiltersTest {

	private static void testFilter(Filter filter, Object trueValue, Object falseValue) {
		HashMap<String, Object> props = new HashMap<>();
		props.put("test", trueValue);
		Assert.assertTrue(filter.accept(props));

		props.put("test", falseValue);
		Assert.assertFalse(filter.accept(props));

	}

	@Test
	public void equalsCheck() {
		testFilter(Filters.equalsTo("test", 1.0), 1.0, 2.0);
	}

	@Test
	public void notEqualCheck() {
		testFilter(Filters.notEqualTo("test", 1.0), 3.0, 1.0);
	}

	@Test
	public void moreThan() {
		testFilter(Filters.moreThan("test", 1.0), 2.0, 0.0);
	}

	@Test
	public void lessThan() {
		testFilter(Filters.lessThan("test", 1), 0, 3);
	}

	@Test
	public void lessThanOrEquals() {
		testFilter(Filters.lessThanOrEqualsTo("test", 1), 0, 3);
		testFilter(Filters.lessThanOrEqualsTo("test", 1), 1, 5);
	}

	@Test
	public void moreThanOrEquals() {
		testFilter(Filters.moreThanOrEqualsTo("test", 2), 5, -9);
		testFilter(Filters.moreThanOrEqualsTo("test", 1), 1, -4);
	}

	@Test
	public void andFilter() {
		Filter equals = Filters.equalsTo("test1", 1.0);
		Filter moreThan = Filters.moreThan("test2", 4.0);
		Filter lessThan = Filters.lessThan("test3", -2.5);

		Filter and = Filters.and(equals, moreThan, lessThan);

		HashMap<String, Object> props = new HashMap<>();
		props.put("test1", 1.0);
		props.put("test2", 10.4);
		props.put("test3", -10.0);
		Assert.assertTrue(and.accept(props));

		props.put("test1", 0.0);
		Assert.assertFalse(and.accept(props));
	}

	@Test
	public void orFilter() {
		Filter equals = Filters.equalsTo("test1", 1.0);
		Filter moreThanOrEquals = Filters.moreThanOrEqualsTo("test2", 4.0);
		Filter lessThan = Filters.lessThan("test3", -2.5);

		Filter and = Filters.and(equals, moreThanOrEquals);

		Filter or = Filters.or(and, equals, lessThan);

		HashMap<String, Object> props = new HashMap<>();
		props.put("test1", 1.0);
		props.put("test2", 10.4);
		props.put("test3", -10.0);
		Assert.assertTrue(or.accept(props));

		props.put("test1", 0.0);
		props.put("test3", 10.0);
		Assert.assertFalse(or.accept(props));
	}
}
