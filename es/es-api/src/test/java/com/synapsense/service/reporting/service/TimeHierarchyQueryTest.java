package com.synapsense.service.reporting.service;

import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;

import com.synapsense.exception.EnvException;
import com.synapsense.service.reporting.DimensionalQuery;
import com.synapsense.service.reporting.Formula;
import com.synapsense.service.reporting.ReportingService;
import com.synapsense.service.reporting.Formula.TransformType;
import com.synapsense.service.reporting.dto.CellTO;
import com.synapsense.service.reporting.dto.RowTO;
import com.synapsense.service.reporting.dto.TableTO;
import com.synapsense.service.reporting.olap.AllMemberSet;
import com.synapsense.service.reporting.olap.AxisSet;
import com.synapsense.service.reporting.olap.CrossJoin;
import com.synapsense.service.reporting.olap.EmptySet;
import com.synapsense.service.reporting.olap.Member;
import com.synapsense.service.reporting.olap.SimpleMember;
import com.synapsense.service.reporting.olap.TupleSet;
import com.synapsense.service.reporting.olap.custom.BaseLevel;
import com.synapsense.service.reporting.olap.custom.Cube;
import com.synapsense.service.reporting.olap.custom.Level;

import static com.synapsense.service.reporting.service.ReportingServiceTestUtils.currentTimeLevel;

public class TimeHierarchyQueryTest {

	private static ReportingService repServ;
	public static final int ROW_HEADER = 0;
	public static final int COLUMN_HEADER = 1;
	
	static DimensionalQuery query;
	

	@BeforeClass
	public static void setUp() throws Exception {
		repServ = ReportingServiceTestUtils.getReportingService();
	}

	@Rule
	public TestName name = new TestName();

	private AxisSet rows;
	private AxisSet columns;
	private AxisSet slicer;
	private TableTO result;

	@Before
	public void outputName() {
		rows = new EmptySet();
		columns = new EmptySet();
		slicer = new EmptySet();
		System.out.println("Sample name: " + name.getMethodName());
	}

	@After
	public void printQuery() {
		System.out.println("Rows dimensionalities: " + rows.getDimensionalities());
		System.out.println("Rows: " + rows);
		System.out.println("Columns dimensionalities: " + columns.getDimensionalities());
		System.out.println("Columns: " + columns);
		System.out.println("Slicer dimensionalities: " + slicer.getDimensionalities());
		System.out.println("Slicer: " + slicer);
		if (result != null){
			Iterator<String> rowHeadersIterator = result.getHeader(ROW_HEADER).iterator();
			List<String> columnHeaders = result.getHeader(COLUMN_HEADER);
			System.out.println("Resulting Table:");
			Iterator<RowTO> rowIterator = result.iterator();
			System.out.print("Columns: ");
			for (String header : columnHeaders){
				System.out.print(header + " | ");
			}
			System.out.println();
			int rowIndex = 0;
			while (rowIterator.hasNext()) {
				RowTO row = rowIterator.next();
				System.out.print(rowHeadersIterator.next() + "| ");
				int columnIndex = 0;
				for (CellTO cell : row) {
					System.out.print("Row" + rowIndex + "Column" + columnIndex + " Value: " + cell.getValue() + " | ");
					columnIndex++;
				}
				System.out.println();
				rowIndex++;
			}
		}
	}

	@Test
	public final void allDaysMultipleMonths() throws EnvException, SQLException {

		// The result report should be as simple as this
		//
		// Date, RACK1 cTop, RACK1 cMid, RACK2 cTop, RACK2 cMid
		// Month x,Year xxxx: xx, xx
		// .......
		// Month x,Year xxxx: xx, xx
		// This test checks Time AllMemberSet with no parent case, 
		// checks pagination support for this case
		// Also it checks case when historical data collected then rack was added
		// - and collected data for this rack present in table as zeros

		Level dayLevel = new BaseLevel(Cube.TIME, Cube.LEVEL_DAY);
		rows = new AllMemberSet(Cube.LEVEL_DAY, Cube.TIME, dayLevel);

		TupleSet months = new TupleSet(
				Cube.TIME.getPrevious(currentTimeLevel(Cube.LEVEL_MONTH)),
				currentTimeLevel(Cube.LEVEL_MONTH));
		columns = new CrossJoin(new AllMemberSet(Cube.OBJECT, "RACK"),
				new TupleSet(new SimpleMember(Cube.PROPERTY, "Name",
						"RACK.cTop.avg"), new SimpleMember(Cube.PROPERTY, "Name",
						"RACK.cMid.avg"))
				, months);
		
		Formula formula = new Formula(Cube.LEVEL_DAY, TransformType.ORIGINAL);
		query = new DimensionalQuery(formula, null, rows, columns);
		result = repServ.executeQuery(query);
	}
	
	@Test
	public final void dayAllMemberSetAxis() throws EnvException, SQLException {

		// The result report should be as simple as this
		//
		// Date, RACK1 cTop, RACK1 cMid, RACK2 cTop, RACK2 cMid
		// Month x,Year xxxx: xx, xx
		// .......
		// Month x,Year xxxx: xx, xx
		// This test checks Time AllMemberSet with no parent case, 
		// checks pagination support for this case
		// Also it checks case when historical data collected then rack was added
		// - and collected data for this rack present in table as zeros

		rows = new AllMemberSet(Cube.LEVEL_DAY, Cube.TIME, new BaseLevel(Cube.TIME, Cube.LEVEL_DAY));

		Member thisMonth = currentTimeLevel(Cube.LEVEL_MONTH);

		TupleSet months = new TupleSet(Cube.TIME.getPrevious(thisMonth), thisMonth);
		columns = new CrossJoin(new AllMemberSet(Cube.OBJECT, "RACK"),
				new TupleSet(new SimpleMember(Cube.PROPERTY, "Name",
						"RACK.cTop.avg"), new SimpleMember(Cube.PROPERTY, "Name",
						"RACK.cMid.avg"))
				, months);
		
		Formula formula = new Formula(Cube.LEVEL_DAY, TransformType.ORIGINAL);
		query = new DimensionalQuery(formula, null, rows, columns);
		result = repServ.executeQuery(query);
	}
	
	@Test
	public final void hourAllMemberSetAxis() throws EnvException, SQLException {

		// The result report should be as simple as this
		//
		// Date, RACK1 cTop, RACK1 cMid, RACK2 cTop, RACK2 cMid
		// Month x,Year xxxx: xx, xx
		// .......
		// Month x,Year xxxx: xx, xx

		rows = new AllMemberSet(Cube.LEVEL_HOUR, Cube.TIME, new BaseLevel(Cube.TIME, Cube.LEVEL_HOUR));

		columns = new CrossJoin(new AllMemberSet(Cube.OBJECT, "RACK"),
				new TupleSet(new SimpleMember(Cube.PROPERTY, "Name",
						"RACK.cTop.avg"), new SimpleMember(Cube.PROPERTY,
						"Name", "RACK.cMid.avg")),
				new TupleSet(Cube.TIME
						.getPrevious(currentTimeLevel(Cube.LEVEL_DAY)),
						currentTimeLevel(Cube.LEVEL_DAY)));
		
		Formula formula = new Formula(Cube.LEVEL_HOUR, TransformType.ORIGINAL);
		query = new DimensionalQuery(formula, null, rows, columns);
		result = repServ.executeQuery(query);
	}
	
	@Test
	public final void allMonthsMultipleYears() throws EnvException, SQLException {
		// The result report should be as simple as this
		//
		// Date, RACK.RACK1, RACK.cTop | RACK.RACK1, RACK.cMid | RACK.RACK2, RACK.cTop | RACK.RACK2, RACK.cMid
		// January,2012, xx, xx
		// February,2012, xx, xx
		// ...........
		// December,13, xx, xx

		Member thisYear = currentTimeLevel(Cube.LEVEL_YEAR);
		rows = new CrossJoin(new TupleSet(Cube.TIME.getPrevious(thisYear),
				thisYear), new AllMemberSet(Cube.LEVEL_MONTH, Cube.TIME,
				new BaseLevel(Cube.TIME, Cube.LEVEL_MONTH)));
		
		columns = new CrossJoin (new AllMemberSet(Cube.OBJECT, "RACK"),
				new TupleSet(new SimpleMember(Cube.PROPERTY, "Name", "RACK.cTop.avg"), new SimpleMember(Cube.PROPERTY, "Name", "RACK.cMid.avg")));
		
		Formula formula = new Formula(Cube.LEVEL_MONTH, TransformType.ORIGINAL);
		query = new DimensionalQuery(formula, null, rows, columns);
		result = repServ.executeQuery(query);
	}
	
	@Test
	public final void allMemberSet() throws EnvException, SQLException {
		// The result report should be as simple as this
		//
		// Date, RACK.RACK1, RACK.cTop | RACK.RACK1, RACK.cMid | RACK.RACK2, RACK.cTop | RACK.RACK2, RACK.cMid
		// 01/01/2013, xx, xx
		// 02/01/2013, xx, xx
		// ...........
		// 31/01/2013, xx, xx
		
		rows = new CrossJoin(new AllMemberSet(Cube.LEVEL_DAY, Cube.TIME, new BaseLevel(Cube.TIME, Cube.LEVEL_DAY)),
				new TupleSet(currentTimeLevel(Cube.LEVEL_MONTH)));
		
		columns = new CrossJoin (new AllMemberSet(Cube.OBJECT, "RACK"),
				new TupleSet(new SimpleMember(Cube.PROPERTY, "Name", "RACK.cTop.avg")
				, new SimpleMember(Cube.PROPERTY, "Name", "RACK.cMid.avg")));
		
		Formula formula = new Formula(Cube.LEVEL_DAY, TransformType.ORIGINAL);
		query = new DimensionalQuery(formula, null, rows, columns);
		result = repServ.executeQuery(query);
	}
	
	@Test
	public final void dayAllMemberSetWithSlicer() throws EnvException, SQLException {
		// The result report should be as simple as this
		//
		// Date, RACK.RACK1, RACK.cTop | RACK.RACK1, RACK.cMid | RACK.RACK2, RACK.cTop | RACK.RACK2, RACK.cMid
		// 01/01/2011, xx, xx
		// 02/01/2011, xx, xx
		// ...........
		// 31/12/2013, xx, xx
		//
		// for 01/2013
		
		slicer = new TupleSet(currentTimeLevel(Cube.LEVEL_MONTH));

		rows = new AllMemberSet(Cube.LEVEL_DAY, Cube.TIME, new BaseLevel(Cube.TIME, Cube.LEVEL_DAY));
		
		columns = new CrossJoin (new AllMemberSet(Cube.OBJECT, "RACK"),
				new TupleSet(new SimpleMember(Cube.PROPERTY, "Name", "RACK.cTop.avg")
				, new SimpleMember(Cube.PROPERTY, "Name", "RACK.cMid.avg")));

		Formula formula = new Formula(Cube.LEVEL_DAY, TransformType.ORIGINAL);		
		query = new DimensionalQuery(formula, slicer, rows, columns);
		result = repServ.executeQuery(query);
	}	
	
	@Test
	public final void monthAllMemberSetWithSlicer() throws EnvException, SQLException {
		// The result report should be as simple as this
		//
		// Date, RACK.RACK1, RACK.cTop | RACK.RACK1, RACK.cMid | RACK.RACK2, RACK.cTop | RACK.RACK2, RACK.cMid
		// Month x,Year xxxx: xx, xx
		// .......
		// Month x,Year xxxx: xx, xx
		//
		// for 2013

		slicer = currentTimeLevel(Cube.LEVEL_YEAR);
		
		rows = new CrossJoin(new AllMemberSet(Cube.LEVEL_MONTH, Cube.TIME, new BaseLevel(Cube.TIME, Cube.LEVEL_MONTH)));
		
		columns = new CrossJoin (new AllMemberSet(Cube.OBJECT, "RACK"),
				new TupleSet(new SimpleMember(Cube.PROPERTY, "Name", "RACK.cTop.avg"), new SimpleMember(Cube.PROPERTY, "Name", "RACK.cMid.avg")));
		
		Formula formula = new Formula(Cube.LEVEL_MONTH, TransformType.ORIGINAL);
		query = new DimensionalQuery(formula, slicer, rows, columns);
		result = repServ.executeQuery(query);
	}	
	
}

