package com.synapsense.service.reporting.management.datasource;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ AllReadingIntakeTempTest.class, RackInletsTest.class, RacksTempDailyTest.class, 
	CracTempDailyTest.class, Top10IntakeTempTest.class, ReportingChartTest.class})
public class ReportingDatasourceTestSuite {
}