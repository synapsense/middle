package com.synapsense.service.reporting.service;

import java.util.Iterator;
import java.util.List;
import javax.naming.InitialContext;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;

import com.synapsense.exception.EnvException;
import com.synapsense.service.Environment;
import com.synapsense.service.reporting.DimensionalQuery;
import com.synapsense.service.reporting.Formula;
import com.synapsense.service.reporting.Formula.TransformType;
import com.synapsense.service.reporting.ReportingService;
import com.synapsense.service.reporting.dto.CellTO;
import com.synapsense.service.reporting.dto.RowTO;
import com.synapsense.service.reporting.dto.TableTO;
import com.synapsense.service.reporting.olap.AllMemberSet;
import com.synapsense.service.reporting.olap.AxisSet;
import com.synapsense.service.reporting.olap.CrossJoin;
import com.synapsense.service.reporting.olap.EmptySet;
import com.synapsense.service.reporting.olap.RangeMember;
import com.synapsense.service.reporting.olap.SimpleMember;
import com.synapsense.service.reporting.olap.custom.Cube;
import static com.synapsense.service.reporting.service.ReportingServiceTestUtils.currentTimeLevel;

public class RangeMemberTest {
	private static Environment env;
	private static ReportingService repServ;
	public static InitialContext ctx;	
	public static final int ROW_HEADER = 0;
	public static final int COLUMN_HEADER = 1;
	static DimensionalQuery query;
	

	@BeforeClass
	public static void setUp() throws Exception {
		repServ = ReportingServiceTestUtils.getReportingService();
	}

	@Rule
	public TestName name = new TestName();

	private AxisSet rows;
	private AxisSet columns;
	private AxisSet slicer;
	private TableTO result;

	@Before
	public void outputName() {
		rows = new EmptySet();
		columns = new EmptySet();
		slicer = new EmptySet();
		System.out.println("Sample name: " + name.getMethodName());
	}

	@After
	public void printQuery() {
		System.out.println("Rows dimensionalities: " + rows.getDimensionalities());
		System.out.println("Rows: " + rows);
		System.out.println("Columns dimensionalities: " + columns.getDimensionalities());
		System.out.println("Columns: " + columns);
		System.out.println("Slicer dimensionalities: " + slicer.getDimensionalities());
		System.out.println("Slicer: " + slicer);
		if (result != null){
			Iterator<String> rowHeadersIterator = result.getHeader(ROW_HEADER).iterator();
			List<String> columnHeaders = result.getHeader(COLUMN_HEADER);
			System.out.println("Resulting Table:");
			Iterator<RowTO> rowIterator = result.iterator();
			System.out.print("Columns: ");
			for (String header : columnHeaders){
				System.out.print(header + " | ");
			}
			System.out.println();
			int rowIndex = 0;
			while (rowIterator.hasNext()) {
				RowTO row = rowIterator.next();
				System.out.print(rowHeadersIterator.next() + "| ");
				int columnIndex = 0;
				for (CellTO cell : row) {
					System.out.print("Row" + rowIndex + "Column" + columnIndex + " Value: " + cell.getValue() + " | ");
					columnIndex++;
				}
				System.out.println();
				rowIndex++;
			}
		}
	}
	
	@Test
	public final void testRoom() throws EnvException {

		// The result report should be as simple as this
		//
		// Date, RACK1 cTop, RACK1 cMid, RACK2 cTop, RACK2 cMid
		// Month x,Year xxxx: xx, xx
		// .......
		// Month x,Year xxxx: xx, xx
		// This test checks Time AllMemberSet with no parent case, 
		// checks pagination support for this case
		// Also it checks case when historical data collected then rack was added
		// - and collected data for this rack present in table as zeros
		
		SimpleMember hour = new SimpleMember(Cube.TIME, Cube.LEVEL_HOUR, 9, currentTimeLevel(Cube.LEVEL_DAY));

		rows = hour;
		
		AxisSet properties = new RangeMember(new SimpleMember(Cube.PROPERTY,
				"Name", "RACK.cTop.avg"), new SimpleMember(Cube.PROPERTY,
				"Name", "RACK.cMid.avg"), new SimpleMember(Cube.PROPERTY,
				"Name", "RACK.cBot.avg"));		
		columns = new CrossJoin(new AllMemberSet(Cube.OBJECT, "RACK"), properties);
		
		Formula formula = new Formula("avg", Cube.LEVEL_HOUR, TransformType.COLUMN_10);
		query = new DimensionalQuery(formula, null, rows, columns);
		result = repServ.executeQuery(query);
	}
	
	@Test
	public final void testPropertyRangeMember() throws EnvException {

		// The result report should be as simple as this
		//
		// Date, RACK1 cTop, RACK1 cMid, RACK2 cTop, RACK2 cMid
		// Month x,Year xxxx: xx, xx
		// .......
		// Month x,Year xxxx: xx, xx
		// This test checks Time AllMemberSet with no parent case, 
		// checks pagination support for this case
		// Also it checks case when historical data collected then rack was added
		// - and collected data for this rack present in table as zeros

		
		SimpleMember hour = new SimpleMember(Cube.TIME, Cube.LEVEL_HOUR, 10, currentTimeLevel(Cube.LEVEL_DAY));
		rows = hour;
				
		AxisSet properties = new RangeMember(new SimpleMember(Cube.PROPERTY,
				"Name", "RACK.cTop.avg"), new SimpleMember(Cube.PROPERTY,
				"Name", "RACK.cMid.avg"), new SimpleMember(Cube.PROPERTY,
				"Name", "RACK.cBot.avg"));
		columns = new CrossJoin(new AllMemberSet(Cube.OBJECT, "RACK"), properties);
		//For TransformType.COLUMN_10 it is needed to set aggregation range basing on which top 10 objects will be calculated
		Formula formula = new Formula("avg", Cube.LEVEL_HOUR, TransformType.COLUMN_10);
		query = new DimensionalQuery(formula, null, rows, columns);
		result = repServ.executeQuery(query);
	}

//	@Test
//	public final void testFiveMinutesLevel() throws EnvException, SQLException {
//
//		// The result report should be as simple as this
//		//
//		// Date, RACK1 cTop, RACK1 cMid, RACK2 cTop, RACK2 cMid
//		// Month x,Year xxxx: xx, xx
//		// .......
//		// Month x,Year xxxx: xx, xx
//		// This test checks Time AllMemberSet with no parent case, 
//		// checks pagination support for this case
//		// Also it checks case when historical data collected then rack was added
//		// - and collected data for this rack present in table as zeros
//
//
//		AxisSet properties = new TupleSet(new SimpleMember(Cube.PROPERTY, "Name", "RACK.cMid.avg"));
////		,new SimpleMember(Cube.PROPERTY, "Name", "RACK.cMid.avg"),
////		new SimpleMember(Cube.PROPERTY, "Name", "RACK.cBot.avg")
//		
//		rows = new CrossJoin(new AllMemberSet(Cube.OBJECT, "ROOM"), properties);
//		
//
//		SimpleMember hour = new SimpleMember(Cube.TIME, Cube.LEVEL_HOUR, 10, currentTimeLevel(Cube.LEVEL_DAY));
//		Level fiveMinutesLevel = new CustomLevel(Cube.TIME, Cube.LEVEL_ONE_MINUTE, 5);
////		AxisSet hours = new AllMemberSet(Cube.TIME, new BaseLevel(Cube.TIME, Cube.LEVEL_HOUR), day);
//		AxisSet fiveMinutes = new AllMemberSet(Cube.TIME, fiveMinutesLevel, hour);
//		columns = fiveMinutes;
//		
//		Formula formula = new Formula(Cube.LEVEL_ONE_MINUTE, TransformType.ORIGINAL);
//		query = new DimensionalQuery(formula, null, rows, columns);
//		result = repServ.executeQuery(query);
//	}
	
//	@Test
//	public final void testCustomHourLevel() throws EnvException, SQLException {
//
//		// The result report should be as simple as this
//		//
//		// Date, RACK1 cTop, RACK1 cMid, RACK2 cTop, RACK2 cMid
//		// Month x,Year xxxx: xx, xx
//		// .......
//		// Month x,Year xxxx: xx, xx
//
//
//		SimpleMember day = new SimpleMember(Cube.TIME, Cube.LEVEL_DAY, 28
//				, new SimpleMember(Cube.TIME, Cube.LEVEL_MONTH, 3
//						, new SimpleMember(Cube.TIME, Cube.LEVEL_YEAR, 2013)));
//		SimpleMember hour = new SimpleMember(Cube.TIME, Cube.LEVEL_HOUR, 20, day);
//		Level fiveMinutesLevel = new CustomLevel(Cube.TIME, Cube.LEVEL_ONE_MINUTE, 5);
//		AxisSet properties = new TupleSet(new SimpleMember(Cube.PROPERTY, "Name", "RACK.cTop.avg"));
//		rows = new CrossJoin(new AllMemberSet(Cube.OBJECT, "RACK"), properties);
//		AxisSet hours = new AllMemberSet(Cube.TIME, new BaseLevel(Cube.TIME, Cube.LEVEL_HOUR), day);
////		AxisSet hours = new AllMemberSet(Cube.TIME, fiveMinutesLevel, hour);
//		columns = hours;
//		query = new DimensionalQuery("avg", null, rows, columns);
//		result = repServ.executeQuery(query);
//	}
	
	
//	@Test
//	public final void testCustomWeekLevel() throws EnvException, SQLException {
//
//		// The result report should be as simple as this
//		//
//		// Date, RACK1 cTop, RACK1 cMid, RACK2 cTop, RACK2 cMid
//		// Month x,Year xxxx: xx, xx
//		// .......
//		// Month x,Year xxxx: xx, xx
//		// This test checks Time AllMemberSet with no parent case, 
//		// checks pagination support for this case
//		// Also it checks case when historical data collected then rack was added
//		// - and collected data for this rack present in table as zeros
//
//
//		Level weekLevel = new WeekLevel();
//		
//		rows = new AllMemberSet(Cube.WEEK, weekLevel
//				, currentTimeLevel(Cube.LEVEL_YEAR));
//		rows.getTuples();
//
//		columns = new CrossJoin(new AllMemberSet(Cube.OBJECT, "RACK"),
//				new TupleSet(new SimpleMember(Cube.PROPERTY, "Name",
//						"RACK.cTop.avg"), new SimpleMember(Cube.PROPERTY, "Name",
//						"RACK.cMid.avg"))
//				);	
//		
//		Formula formula = new Formula(Cube.LEVEL_WEEK, TransformType.ORIGINAL);
//		query = new DimensionalQuery(formula, null, rows, columns);
//		result = repServ.executeQuery(query);
//	}
	
}