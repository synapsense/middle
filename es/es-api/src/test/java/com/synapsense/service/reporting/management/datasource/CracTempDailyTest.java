package com.synapsense.service.reporting.management.datasource;

import java.util.Arrays;
import java.util.Collection;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import com.synapsense.service.reporting.datasource.ReportingTableDataSource;

@RunWith(Parameterized.class)
public class CracTempDailyTest extends RacksTempDailyTest{
	
	private static final String CRACS = "CRAC";
	
	public CracTempDailyTest(String objectType,
			Boolean expectedResult) {
		super(objectType, expectedResult);
	}
	
	@Parameterized.Parameters
	public static Collection<Object[]> primeNumbers() {
		return Arrays.asList(new Object[][] { { CRACS, true }, { ROOMS, true },
				{ DC, true } });
	}
	   
	protected void getData() {
		
		String[] properties = new String[] { "CRAC.supplyT.avg", "CRAC.returnT.avg" };
		
		dataSource = ReportingTableDataSource.newRAWTable(objectType
				, properties
				, "Day" 
				, dateRange);
		result = dataSource.getData();
		
	}

}