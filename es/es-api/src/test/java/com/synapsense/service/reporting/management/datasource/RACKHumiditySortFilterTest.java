package com.synapsense.service.reporting.management.datasource;

import java.util.List;

import com.synapsense.service.reporting.olap.filters.ComparatorFilter;
import com.synapsense.service.reporting.olap.filters.Filters;
import com.synapsense.util.CollectionUtils;

/**
 * Created by Grigory Ryabov on 20.03.14.
 */
public class RACKHumiditySortFilterTest extends RACKTemperatureSortFilterTest {

    public RACKHumiditySortFilterTest(String rowsObject, Boolean expectedResult) {
        super(rowsObject, expectedResult);
    }

    @Override
    protected ComparatorFilter getFilter() {
        return (ComparatorFilter)Filters.lessThan("RACK.rh.avg", "any.WSNSENSOR.201.rMax", true);
    }

    @Override
    protected void fillColumnObject() {
        List<String[]> properties = CollectionUtils.newArrayList(2);
        properties.add(new String[] { "RACK.rh.avg" });
        columnsObject = properties;
    }
}
