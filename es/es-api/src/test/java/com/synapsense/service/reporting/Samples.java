package com.synapsense.service.reporting;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;

import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.service.reporting.olap.AllMemberSet;
import com.synapsense.service.reporting.olap.AxisSet;
import com.synapsense.service.reporting.olap.CrossJoin;
import com.synapsense.service.reporting.olap.EmptySet;
import com.synapsense.service.reporting.olap.Member;
import com.synapsense.service.reporting.olap.MemberTuple;
import com.synapsense.service.reporting.olap.RangeSet;
import com.synapsense.service.reporting.olap.SimpleMember;
import com.synapsense.service.reporting.olap.Tuple;
import com.synapsense.service.reporting.olap.TupleSet;
import com.synapsense.service.reporting.olap.custom.Cube;

public class Samples {

	@Rule
	public TestName name = new TestName();

	private AxisSet rows;
	private AxisSet columns;
	private AxisSet slicer;

	@Before
	public void outputName() {
		rows = new EmptySet();
		columns = new EmptySet();
		slicer = new EmptySet();
		System.out.println("Sample name: " + name.getMethodName());
	}

	@After
	public void printQuery() {
		System.out.println("Rows dimensionalities: " + rows.getDimensionalities());
		System.out.println("Rows: " + rows);
		System.out.println("Columns dimensionalities: " + columns.getDimensionalities());
		System.out.println("Columns: " + columns);
		System.out.println("Slicer dimensionalities: " + slicer.getDimensionalities());
		System.out.println("Slicer: " + slicer);
		System.out.println();
	}

	@Test
	public void simpleReportCreation() {

		// The result report should be as simple as this
		//
		// Year, cTop, cBot
		// 2011, xx, xx
		// 2012, xx, xx
		//
		// for Rack1 from DataCenter and Room1

		slicer = new SimpleMember(Cube.OBJECT, "RACK", "Rack1").setParent(new SimpleMember(Cube.OBJECT, "ROOM", "Room1")
		        .setParent(new SimpleMember(Cube.OBJECT, "DC", "DataCenter")));
		rows = new TupleSet(new SimpleMember(Cube.TIME, "Year", 2011), new SimpleMember(Cube.TIME, "Year", 2012));
		columns = new TupleSet(new SimpleMember(Cube.PROPERTY, "RACK", "cTop"), new SimpleMember(Cube.PROPERTY, "RACK", "cBot"));

	}

	@Test
	public void createTuplesAndRanges() throws ObjectNotFoundException {

		// The result report should be as simple as this
		//
		// Date, CRAC1 supplyT, CRAC2 suplyT
		// 01/01/2012, xx, xx
		// 01/02/2012, xx, xx
		// ........
		// 01/31/2012, xx, xx
		//

		Tuple crac1Tuple = new MemberTuple(new SimpleMember(Cube.OBJECT, "CRAC", "CRAC1").setParent(new SimpleMember(Cube.OBJECT, 
		        "ROOM", "Room1").setParent(new SimpleMember(Cube.OBJECT, "DC", "DataCenter"))), new SimpleMember(Cube.PROPERTY, "CRAC",
		        "supplyT"));

		Tuple crac2Tuple = new MemberTuple(new SimpleMember(Cube.OBJECT, "CRAC", "CRAC2").setParent(new SimpleMember(Cube.OBJECT, 
		        "ROOM", "Room1").setParent(new SimpleMember(Cube.OBJECT, "DC", "DataCenter"))), new SimpleMember(Cube.PROPERTY, "CRAC",
		        "supplyT"));

		columns = new TupleSet(crac1Tuple, crac2Tuple);

		SimpleMember timeStartMember = new SimpleMember(Cube.TIME, "Day", 1);
		timeStartMember.setParent(new SimpleMember(Cube.TIME, "Month", 1).setParent(new SimpleMember(Cube.TIME, "Year", 2012)));

		SimpleMember timeEndMember = new SimpleMember(Cube.TIME, "Day", 31);
		timeEndMember.setParent(new SimpleMember(Cube.TIME, "Month", 1).setParent(new SimpleMember(Cube.TIME, "Year", 2012)));

		// rows = new RangeSet(timeStartMember, timeEndMember);
		rows = new TupleSet(timeStartMember, timeEndMember);
		CrossJoin crossJoin = new CrossJoin(rows, columns);
		System.out.println(crossJoin.getTuples());

	}

	@Test
	public void allMembersTest() {

		// The result report should be as simple as this
		//
		// CRAC, supplyT, returnT
		// CRAC1, xx, xx
		// CRAC2, xx, xx
		// ...........
		// CRACn, xx, xx
		//
		// for cracs from Room1 for July 2012

		rows = new AllMemberSet(Cube.OBJECT, "CRAC", new SimpleMember(Cube.OBJECT, "ROOM", "Room1"));
		columns = new TupleSet(new SimpleMember(Cube.PROPERTY, "CRAC", "supplyT"), new SimpleMember(Cube.PROPERTY, "CRAC", "returnT"));
		slicer = new SimpleMember(Cube.TIME, Cube.LEVEL_MONTH, 7).setParent(new SimpleMember(Cube.TIME, Cube.LEVEL_YEAR, 2012));

	}

	@Test
	public void crossjoin() {

		// The result report should be as simple as this
		//
		// Year, Room1 Rack humidity April, Room1 CRAC supplyT April, Room1 Rack
		// humidity May, Room1 CRAC supplyT May,....RoomN CRAC supplyT May
		// 2010, ...
		// 2011, ...
		// 2012, ...
		//

		SimpleMember start = new SimpleMember(Cube.TIME, Cube.LEVEL_MONTH, 4);
		SimpleMember end = new SimpleMember(Cube.TIME, Cube.LEVEL_MONTH, 5);

		rows = new CrossJoin(new AllMemberSet(Cube.OBJECT, "ROOM"), new TupleSet(
		        new SimpleMember(Cube.PROPERTY, "RACK", "humidity"), new SimpleMember(Cube.PROPERTY, "CRAC", "supplyT")),
		        new RangeSet(Cube.TIME, start, end));

		columns = new TupleSet(new SimpleMember(Cube.TIME, Cube.LEVEL_YEAR, 2010), new SimpleMember(Cube.TIME, Cube.LEVEL_YEAR, 2011),
		        new SimpleMember(Cube.TIME, Cube.LEVEL_YEAR, 2012));
	}

	@Test
	public void firstGetTuplesTest() throws ObjectNotFoundException {
		rows = new CrossJoin(new TupleSet(new SimpleMember(Cube.OBJECT, "RACK", "Rack1")), new TupleSet(new SimpleMember(Cube.PROPERTY, 
		        "RACK", "humidity"), new SimpleMember(Cube.PROPERTY, "RACK", "cMid")), new SimpleMember(Cube.TIME, Cube.LEVEL_YEAR, 2012));

		System.out.println(rows.getTuples());
	}

	@Test
	public void mergeTest() {
		Member start = new SimpleMember(Cube.TIME, Cube.LEVEL_DAY, 1).setParent(new SimpleMember(Cube.TIME, Cube.LEVEL_YEAR, 2010));
		Member end = new SimpleMember(Cube.TIME, Cube.LEVEL_HOUR, 12).setParent(new SimpleMember(Cube.TIME, Cube.LEVEL_MONTH, 5)
		        .setParent(new SimpleMember(Cube.TIME, Cube.LEVEL_YEAR, 2010)));

		System.out.println(start.merge(end));
	}

	@Test
	public void joinMergableTuples() {
		MemberTuple tuple1 = new MemberTuple(new SimpleMember(Cube.TIME, Cube.LEVEL_YEAR, 2012), new SimpleMember(Cube.PROPERTY, "RACK", "cMid"));
		MemberTuple tuple2 = new MemberTuple(new SimpleMember(Cube.TIME, Cube.LEVEL_MONTH, 5),
		        new SimpleMember(Cube.OBJECT, "RACK", "Rack1"));

		System.out.println(tuple1.join(tuple2));
	}

	@Test
	public void crossjoinMergableSets() throws ObjectNotFoundException {
		rows = new CrossJoin(new TupleSet(new MemberTuple(new SimpleMember(Cube.OBJECT, "RACK", "Rack1"), new SimpleMember(Cube.TIME, 
		        Cube.LEVEL_YEAR, 2011)), new MemberTuple(new SimpleMember(Cube.OBJECT, "RACK", "Rack1"), new SimpleMember(Cube.TIME, 
		        Cube.LEVEL_YEAR, 2012))), new TupleSet(new MemberTuple(new SimpleMember(Cube.PROPERTY, "RACK", "humidity"),
		        new SimpleMember(Cube.TIME, Cube.LEVEL_MONTH, 4)), new MemberTuple(new SimpleMember(Cube.PROPERTY, "RACK", "cTop"),
		        new SimpleMember(Cube.TIME, Cube.LEVEL_MONTH, 5))));

		System.out.println(rows.getTuples());

		// columns = new TupleSet(new GenericMember(Cube.TIME, Cube.LEVEL_YEAR, 2010), new
		// GenericMember(Cube.TIME, Cube.LEVEL_YEAR, 2011),
		// new GenericMember(Cube.TIME, Cube.LEVEL_YEAR, 2012));
		//
		// new ReportingLite().getReportSet(rows, columns);
	}
}
