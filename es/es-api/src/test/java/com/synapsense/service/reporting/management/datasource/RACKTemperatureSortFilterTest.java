package com.synapsense.service.reporting.management.datasource;

import com.synapsense.dto.TO;
import com.synapsense.service.reporting.Formula;
import com.synapsense.service.reporting.datasource.ReportingTableDataSource;
import com.synapsense.service.reporting.olap.AllMemberSet;
import com.synapsense.service.reporting.olap.RangeSet;
import com.synapsense.service.reporting.olap.custom.Cube;
import com.synapsense.service.reporting.olap.custom.TimePeriodType;
import com.synapsense.service.reporting.olap.filters.ComparatorFilter;
import com.synapsense.service.reporting.olap.filters.Filters;
import com.synapsense.util.CollectionUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Set;

@RunWith(Parameterized.class)
public class RACKTemperatureSortFilterTest extends AbstractReportingDataSourceTest{


    private Boolean expectedResult;

    public RACKTemperatureSortFilterTest(String rowsObject,
                                         Boolean expectedResult) {
        this.rowsObject = rowsObject;
        this.expectedResult = expectedResult;
    }


    @Parameterized.Parameters
    public static Collection<Object[]> primeNumbers() {
        return Arrays.asList(new Object[][]{{ROOMS, true},
                {DC, true}});
    }

    @Test
    public final void today() {
        slicerObject = new RangeSet(Cube.TIME, TimePeriodType.TODAY);
        getData();
    }

    @Test
    public final void yesterday() {
        slicerObject = new RangeSet(Cube.TIME, TimePeriodType.YESTERDAY);
        getData();
    }

    @Test
    public final void thisWeek() {
        slicerObject = new RangeSet(Cube.WEEK, TimePeriodType.THIS_WEEK);
        getData();
    }

    @Test
    public final void lastWeek() {
        slicerObject = new RangeSet(Cube.WEEK, TimePeriodType.LAST_WEEK);
        getData();
    }

    @Test
    public final void thisMonth() {
        slicerObject = new RangeSet(Cube.TIME, TimePeriodType.THIS_MONTH);
        getData();
    }

    @Test
    public final void lastMonth() {
        slicerObject = new RangeSet(Cube.TIME, TimePeriodType.LAST_MONTH);
        getData();
    }

    @Test
    public final void twoHours() {
        slicerObject = RangeSet.newCustomRangeSet(Cube.TIME, 2, Cube.LEVEL_HOUR);
        getData();
    }

    @Test
    public final void twoDays() {
        slicerObject = RangeSet.newCustomRangeSet(Cube.TIME, 2, Cube.LEVEL_DAY);
        getData();
    }

    @Test
    public final void twoWeeks() {
        slicerObject = RangeSet.newCustomRangeSet(Cube.WEEK, 2, Cube.LEVEL_WEEK);
        getData();
    }

    @Test
    public final void twoMonths() {
        slicerObject = RangeSet.newCustomRangeSet(Cube.TIME, 2, Cube.LEVEL_MONTH);
        getData();
    }

    protected void getData() {

        String sortingPropertiesJSON = "{\"properties\":[{\"lineColor\":\"\",\"name\":\"RACK.location\",\"value\":false},{\"lineColor\":\"\",\"name\":\"RACK.cTop.avg\",\"value\":true}]}";
        List<ComparatorFilter> filters = CollectionUtils.newList();
        filters.add(getFilter());
        Formula formula = new Formula(sortingPropertiesJSON, filters, Formula.TransformType.FILTER, Formula.TransformType.SORT);

        TO<?> parent = (TO<?>)new AllMemberSet(Cube.OBJECT, (String)rowsObject).getTuples().iterator().next().itemByHierarchy(Cube.OBJECT).getKey();
        Set<TO<?>> objects = CollectionUtils.newSet();
        objects.add(parent);
        rowsObject = objects;

        fillColumnObject();

        dataSource = ReportingTableDataSource.newAggregatedTable(formula
                , slicerObject
                , rowsObject
                , columnsObject);
        result = dataSource.getData();

    }

    protected ComparatorFilter getFilter() {
        return (ComparatorFilter) Filters.lessThan("RACK.cTop.avg", "any.WSNSENSOR.200.rMax", true);
    }

    protected void fillColumnObject() {
        List<String[]> properties = CollectionUtils.newArrayList(2);
        properties.add(new String[] { "RACK.cTop.max", "RACK.cMid.max", "RACK.cBot.max" });
        properties.add(new String[] { "RACK.cTop.avg", "RACK.cMid.avg", "RACK.cBot.avg" });
        columnsObject = properties;
    }

}
