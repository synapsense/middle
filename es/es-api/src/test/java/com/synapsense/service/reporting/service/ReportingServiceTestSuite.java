package com.synapsense.service.reporting.service;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ ExplicitNamedObjectsTest.class, FormulaPropertyMemberTest.class, ImplicitNamedObjectsTest.class, 
	ObjectHierarchyQueryTest.class, RangeMemberTest.class, TimeHierarchyQueryTest.class, StandartDeviationTest.class})
public class ReportingServiceTestSuite {
}