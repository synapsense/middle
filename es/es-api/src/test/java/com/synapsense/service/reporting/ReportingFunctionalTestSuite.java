package com.synapsense.service.reporting;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.synapsense.service.reporting.management.datasource.ReportingDatasourceTestSuite;
import com.synapsense.service.reporting.olap.filters.FiltersTest;
import com.synapsense.service.reporting.service.ReportingServiceTestSuite;

@RunWith(Suite.class)
@SuiteClasses({ ReportingServiceTestSuite.class,
		ReportingDatasourceTestSuite.class, Samples.class,
		TimeHierarchyTest.class, FiltersTest.class })
public class ReportingFunctionalTestSuite {
}