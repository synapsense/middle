package com.synapsense.service.reporting.service;

import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;

import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.exception.EnvException;
import com.synapsense.service.Environment;
import com.synapsense.service.reporting.DimensionalQuery;
import com.synapsense.service.reporting.Formula;
import com.synapsense.service.reporting.Formula.TransformType;
import com.synapsense.service.reporting.ReportingService;
import com.synapsense.service.reporting.dto.CellTO;
import com.synapsense.service.reporting.dto.RowTO;
import com.synapsense.service.reporting.dto.TableTO;
import com.synapsense.service.reporting.olap.AllMemberSet;
import com.synapsense.service.reporting.olap.AxisSet;
import com.synapsense.service.reporting.olap.CrossJoin;
import com.synapsense.service.reporting.olap.EmptySet;
import com.synapsense.service.reporting.olap.Member;
import com.synapsense.service.reporting.olap.RangeSet;
import com.synapsense.service.reporting.olap.SimpleMember;
import com.synapsense.service.reporting.olap.Tuple;
import com.synapsense.service.reporting.olap.TupleSet;
import com.synapsense.service.reporting.olap.custom.BaseLevel;
import com.synapsense.service.reporting.olap.custom.Cube;
import com.synapsense.service.reporting.olap.custom.TimePeriodType;

public class StandartDeviationTest {
	private static ReportingService repServ;
	public static final int ROW_HEADER = 0;
	public static final int COLUMN_HEADER = 1;
	static DimensionalQuery query;
	private static Environment env;

	@BeforeClass
	public static void setUp() throws Exception {
		repServ = ReportingServiceTestUtils.getReportingService();
		env = ReportingServiceTestUtils.getEnvironment();
	}

	@Rule
	public TestName name = new TestName();

	private AxisSet rows;
	private AxisSet columns;
	private AxisSet slicer;
	private TableTO result;

	@Before
	public void outputName() {
		rows = new EmptySet();
		columns = new EmptySet();
		slicer = new EmptySet();
		System.out.println("Sample name: " + name.getMethodName());
	}

	@After
	public void printQuery() {
		System.out.println("Rows dimensionalities: " + rows.getDimensionalities());
		System.out.println("Rows: " + rows);
		System.out.println("Columns dimensionalities: " + columns.getDimensionalities());
		System.out.println("Columns: " + columns);
		System.out.println("Slicer dimensionalities: " + slicer.getDimensionalities());
		System.out.println("Slicer: " + slicer);
		if (result != null){
			Iterator<String> rowHeadersIterator = result.getHeader(ROW_HEADER).iterator();
			List<String> columnHeaders = result.getHeader(COLUMN_HEADER);
			System.out.println("Resulting Table:");
			Iterator<RowTO> rowIterator = result.iterator();
			System.out.print("Columns: ");
			for (String header : columnHeaders){
				System.out.print(header + " | ");
			}
			System.out.println();
			int rowIndex = 0;
			while (rowIterator.hasNext()) {
				RowTO row = rowIterator.next();
				System.out.print(rowHeadersIterator.next() + "| ");
				int columnIndex = 0;
				for (CellTO cell : row) {
					System.out.print("Row" + rowIndex + "Column" + columnIndex + " Value: " + cell.getValue() + " | ");
					columnIndex++;
				}
				System.out.println();
				rowIndex++;
			}
		}
	}
	
//	@Test
//	public void setModelVersion() throws EnvException {
//		env.setPropertyValue(env.getObjectsByType("ROOT").iterator().next(), "modelVersion", "2.7");
//		Collection<TO<?>> dcs = env.getObjectsByType("DC");    
//	       
//		   for (TO<?> dc : dcs) {
//		    env.setPropertyValue(dc, "modelVersion", "2.7");
//		   }
//
//	}

	@Test
	public void slicerWeekLevel_9510() {
		
        slicer = RangeSet.newCustomRangeSet(Cube.WEEK, 2, Cube.LEVEL_WEEK);
		
        rows = new AllMemberSet(Cube.OBJECT, "RACK"); 
        
        columns = new TupleSet(
                new SimpleMember(Cube.PROPERTY, "Name", "RACK.cTop.avg")
                , new SimpleMember(Cube.PROPERTY, "Name", "RACK.cBot.avg"));

        Formula formula = new Formula(Cube.ALL_RANGE, TransformType.ORIGINAL);
        DimensionalQuery query = new DimensionalQuery(formula, slicer, rows, columns);
        result = repServ.executeQuery(query);
	}
	
	@Test
	public void monthWeekLevel_9510() {
		slicer = new AllMemberSet(Cube.OBJECT, "RACK"); 

        rows = RangeSet.newCustomRangeSet(Cube.TIME, 1, Cube.LEVEL_MONTH);
        columns = new TupleSet(
                new SimpleMember(Cube.PROPERTY, "Name", "RACK.cTop.avg")
                , new SimpleMember(Cube.PROPERTY, "Name", "RACK.cBot.avg"));

        Formula formula = new Formula(Cube.LEVEL_WEEK, TransformType.ORIGINAL);
        DimensionalQuery query = new DimensionalQuery(formula, slicer, rows, columns);
        result = repServ.executeQuery(query);
	}
	
	@Test
	public void daysWeekLevel_9510() {
		slicer = new AllMemberSet(Cube.OBJECT, "RACK"); 

        rows = RangeSet.newCustomRangeSet(Cube.TIME, 18, Cube.LEVEL_DAY);
        columns = new TupleSet(
                new SimpleMember(Cube.PROPERTY, "Name", "RACK.cTop.avg")
                , new SimpleMember(Cube.PROPERTY, "Name", "RACK.cBot.avg"));

        Formula formula = new Formula(Cube.LEVEL_WEEK, TransformType.ORIGINAL);
        DimensionalQuery query = new DimensionalQuery(formula, slicer, rows, columns);
        result = repServ.executeQuery(query);
	}
	
	
	@Test
	public void bug9510() {
		slicer = new AllMemberSet(Cube.OBJECT, "RACK"); 

        rows = RangeSet.newCustomRangeSet(Cube.WEEK, 1, Cube.LEVEL_WEEK);
        columns = new TupleSet(
                new SimpleMember(Cube.PROPERTY, "Name", "RACK.cTop.avg")
                , new SimpleMember(Cube.PROPERTY, "Name", "RACK.cBot.avg"));

        Formula formula = new Formula(Cube.LEVEL_DAY, TransformType.ORIGINAL);
        DimensionalQuery query = new DimensionalQuery(formula, slicer, rows, columns);
        result = repServ.executeQuery(query);
	}

	@Test
	public void bug9510MultipleObjectIdTablePerformance() {
		slicer = new AllMemberSet(Cube.OBJECT, "RACK");
		
		rows = RangeSet.newCustomRangeSet(Cube.TIME, 20, Cube.LEVEL_DAY);
		
		columns = new TupleSet(
				new SimpleMember(Cube.PROPERTY, "Name", "RACK.cTop.avg"));
		
		Formula formula = new Formula(Cube.LEVEL_DAY, TransformType.ORIGINAL);
		DimensionalQuery query = new DimensionalQuery(formula, slicer, rows, columns);
		result = repServ.executeQuery(query);
	}
	
	
	@Test
	public void weekLevel_9510() {
		slicer = new AllMemberSet(Cube.OBJECT, "RACK"); 

        rows = RangeSet.newCustomRangeSet(Cube.WEEK, 10, Cube.LEVEL_WEEK);
        columns = new TupleSet(
                new SimpleMember(Cube.PROPERTY, "Name", "RACK.cTop.avg")
                , new SimpleMember(Cube.PROPERTY, "Name", "RACK.cBot.avg"));

        Formula formula = new Formula(Cube.LEVEL_WEEK, TransformType.ORIGINAL);
        DimensionalQuery query = new DimensionalQuery(formula, slicer, rows, columns);
        result = repServ.executeQuery(query);
	}
	
	@Test
	public void allMemberSet_9509() {
		
		slicer = new AllMemberSet(Cube.OBJECT, "RACK"); 

		rows = new AllMemberSet(Cube.TIME, Cube.LEVEL_YEAR);

		columns = new TupleSet(new SimpleMember(Cube.PROPERTY, "Name",
		"RACK.cTop.avg"), new SimpleMember(Cube.PROPERTY, "Name", "RACK.cBot.avg"));

		Formula formula = new Formula(Cube.LEVEL_DAY, TransformType.ORIGINAL);
		DimensionalQuery query = new DimensionalQuery(formula, slicer, rows, columns);
		result = repServ.executeQuery(query);
	}
	
	@Test
	public void rootRacksCracsProperties() {
		slicer = new RangeSet(Cube.WEEK, TimePeriodType.THIS_WEEK);
		AxisSet allRoots = new AllMemberSet(Cube.OBJECT, "ROOT");
		Member root = allRoots.getTuples().iterator().next().itemByHierarchy(Cube.OBJECT);

		rows = new CrossJoin(root,
				new TupleSet(
						new SimpleMember(Cube.PROPERTY, "Name", "CRAC.supplyT.STDDEV_SAMP"),
						new SimpleMember(Cube.PROPERTY, "Name", "CRAC.returnT.STDDEV_SAMP"),
				 new SimpleMember(Cube.PROPERTY, "Name", "RACK.cTop.STDDEV_SAMP")
				, new SimpleMember(Cube.PROPERTY, "Name", "RACK.cMid.STDDEV_SAMP")
				));

		columns = new EmptySet();

		Formula formula = new Formula(Cube.ALL_RANGE, TransformType.ORIGINAL);		
		query = new DimensionalQuery(formula, slicer, rows, columns);
		result = repServ.executeQuery(query);
	}

	@Test
	public void roomRacksWithCustomTimePeriod() {
		slicer = RangeSet.newCustomRangeSet(Cube.TIME, 5, Cube.LEVEL_DAY);
		List<Tuple> rooms = new AllMemberSet(Cube.OBJECT, "ROOM").getTuples();

		for (Tuple roomTuple : rooms) {
			Member room = roomTuple.itemByHierarchy(Cube.OBJECT);
			AllMemberSet racks = new AllMemberSet(Cube.OBJECT, new BaseLevel(Cube.OBJECT, "RACK"), room);
			if (!racks.getTuples().isEmpty()) {
				rows = racks;
				break;
			}
		}

		columns = new SimpleMember(Cube.PROPERTY, "Name", "RACK.cTop.STDDEV_SAMP");

		Formula formula = new Formula(Cube.ALL_RANGE, TransformType.ORIGINAL);		
		query = new DimensionalQuery(formula, slicer, rows, columns);
		result = repServ.executeQuery(query);
	}
	
	@Test
	public void dcRacksWithStartEndDatePeriod() {
		slicer = new RangeSet(Cube.TIME, new Date(System.currentTimeMillis() - 2 * 60 * 60 * 1000), new Date());

		Member dc = new AllMemberSet(Cube.OBJECT, "DC").getTuples().iterator().next().itemByHierarchy(Cube.OBJECT);
		rows = new AllMemberSet(Cube.OBJECT, new BaseLevel(Cube.OBJECT, "RACK"), dc);
		
		columns = new SimpleMember(Cube.PROPERTY, "Name", "RACK.cMid.STDDEV_SAMP");

		Formula formula = new Formula(Cube.ALL_RANGE, TransformType.ORIGINAL);		
		query = new DimensionalQuery(formula, slicer, rows, columns);
		result = repServ.executeQuery(query);
	}
}
