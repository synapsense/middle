package com.synapsense.service.reporting.service;

import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;

import com.synapsense.exception.EnvException;
import com.synapsense.service.reporting.DimensionalQuery;
import com.synapsense.service.reporting.Formula;
import com.synapsense.service.reporting.ReportingService;
import com.synapsense.service.reporting.Formula.TransformType;
import com.synapsense.service.reporting.dto.CellTO;
import com.synapsense.service.reporting.dto.RowTO;
import com.synapsense.service.reporting.dto.TableTO;
import com.synapsense.service.reporting.olap.AllMemberSet;
import com.synapsense.service.reporting.olap.AxisSet;
import com.synapsense.service.reporting.olap.CrossJoin;
import com.synapsense.service.reporting.olap.EmptySet;
import com.synapsense.service.reporting.olap.SimpleMember;
import com.synapsense.service.reporting.olap.TupleSet;
import com.synapsense.service.reporting.olap.custom.BaseLevel;
import com.synapsense.service.reporting.olap.custom.Cube;
import com.synapsense.service.reporting.olap.custom.Level;

public class FormulaPropertyMemberTest {
	private static ReportingService repServ;	
	public static final int ROW_HEADER = 0;
	public static final int COLUMN_HEADER = 1;
	static DimensionalQuery query;
	

	@BeforeClass
	public static void setUp() throws Exception {
		repServ = ReportingServiceTestUtils.getReportingService();
	}

	@Rule
	public TestName name = new TestName();

	private AxisSet rows;
	private AxisSet columns;
	private AxisSet slicer;
	private TableTO result;

	@Before
	public void outputName() {
		rows = new EmptySet();
		columns = new EmptySet();
		slicer = new EmptySet();
		System.out.println("Sample name: " + name.getMethodName());
	}

	@After
	public void printQuery() {
		System.out.println("Rows dimensionalities: " + rows.getDimensionalities());
		System.out.println("Rows: " + rows);
		System.out.println("Columns dimensionalities: " + columns.getDimensionalities());
		System.out.println("Columns: " + columns);
		System.out.println("Slicer dimensionalities: " + slicer.getDimensionalities());
		System.out.println("Slicer: " + slicer);
		if (result != null){
			Iterator<String> rowHeadersIterator = result.getHeader(ROW_HEADER).iterator();
			List<String> columnHeaders = result.getHeader(COLUMN_HEADER);
			System.out.println("Resulting Table:");
			Iterator<RowTO> rowIterator = result.iterator();
			System.out.print("Columns: ");
			for (String header : columnHeaders){
				System.out.print(header + " | ");
			}
			System.out.println();
			int rowIndex = 0;
			while (rowIterator.hasNext()) {
				RowTO row = rowIterator.next();
				System.out.print(rowHeadersIterator.next() + "| ");
				int columnIndex = 0;
				for (CellTO cell : row) {
					System.out.print("Row" + rowIndex + "Column" + columnIndex + " Value: " + cell.getValue() + " | ");
					columnIndex++;
				}
				System.out.println();
				rowIndex++;
			}
		}
	}

	@Test
	public final void testFormulaPropertyMember() throws EnvException, SQLException {

		// The result report should be as simple as this
		//
		// Year, Room1 Rack cTop, Room1 Rack cMid,....RoomN RACK cMid
		// Month x,Year xxxx: xx, xx
		// .......
		// Month x,Year xxxx: xx, xx
		//
		
		Level monthLevel = new BaseLevel(Cube.TIME, Cube.LEVEL_MONTH);
		rows = new AllMemberSet(Cube.LEVEL_YEAR, Cube.TIME, monthLevel);

		TupleSet properties = new TupleSet(new SimpleMember(Cube.PROPERTY,
				"Name", "RACK.cTop.avg"), new SimpleMember(Cube.PROPERTY, "Name",
				"RACK.cMid.max"));
		columns = new CrossJoin(new AllMemberSet(Cube.OBJECT, "ROOM"),
				properties);
		
		Formula formula = new Formula(Cube.LEVEL_MONTH, TransformType.ORIGINAL);
		DimensionalQuery query = new DimensionalQuery(formula, null, rows, columns);
		result = repServ.executeQuery(query);
	}
	
}