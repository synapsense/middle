grammar sdx;

// Works with antlr 4.3! Be careful if you're trying either newer or older versions.

options {
//	memoize=true;
	language = Java;
	output = AST;
	ASTLabelType=CommonTree;
}



tokens {
// Keywords
	NON			= 'NON';
	EMPTY 			= 'EMPTY';
	ON 			= 'ON';
	SELECT 			= 'SELECT';
	WHERE 			= 'WHERE';
	AXIS			= 'AXIS';
	MEMBERS			= 'members';
	ROWS			= 'ROWS';
	COLUMNS			= 'COLUMNS';
	
// Symbols
	ASTERISK 	= '*';
	COLON 		= ':';
	COMMA 		= ',';
	DOT 		= '.';
	LBRACE 		= '{';
	LPAREN 		= '(';
	PLUS 		= '+';
	RBRACE 		= '}';
	RPAREN 		= ')';
}

@parser::header {
package com.synapsense.service.reporting.lang;
	
import java.io.DataInputStream;
import com.synapsense.service.reporting.*;
import com.synapsense.service.reporting.olap.*;
import com.synapsense.service.reporting.olap.custom.*;
import org.antlr.*;
}

@parser::members {
	private DimensionalQuery dq;

	public DimensionalQuery getDimensionalQuery() {
		return dq;
	}
	  
	@Override
	public void reportError(RecognitionException e) {
    		throw new RuntimeException("I quit!\n" + e, e); 
  	}
  	
  	private SimpleMember buildMemberFromPath(List<SimpleMember> members, Hierarchy hierarchy) {
  		for(int i = members.size() - 1; i >= 0; i --){ 
  			members.get(i).setHierarchy(hierarchy); 
	  		if (i != 0) members.get(i).setParent(members.get(i-1));
  		} 
  		return members.get(members.size() - 1);
  	}
  	
}

@lexer::header {
package com.synapsense.service.reporting.lang;
}

@lexer::members {
  @Override
  public void reportError(RecognitionException e) {
    throw new RuntimeException("I quit!\n" + e, e); 
  }
}

sdx_statement 
	: 	select_statement EOF
	;

select_statement
	:	{dq = new DimensionalQuery();} SELECT (fun)? axis_specification_list (WHERE slicer_specification)?
	;
			
axis_specification_list 
	:	axis_specification (COMMA axis_specification)* 
	;
			
axis_specification
	:	(NON EMPTY)? setDef = set_definition ON axisDef = axis_name {dq.addAxisSet(setDef.axisSet, axisDef.axisId);}
	;
					
slicer_specification
	:	slicerDef = set_definition {dq.setSlicer(slicerDef.axisSet);}
	;					

axis_name returns [int axisId;]
 	:	ROWS {retval.axisId = 0;}
 	|	COLUMNS {retval.axisId = 1;}
 	| 	AXIS axisNum = LPAREN NUMBER RPAREN {retval.axisId = Integer.valueOf(axisNum.getText());}
	;
			
set_definition returns [AxisSet axisSet = null;]
	: 	firstSetDef = simple_set_definition {retval.axisSet = firstSetDef.set;} (opDef = set_operation setDef = simple_set_definition {retval.axisSet = opDef.opSet.add(retval.axisSet).add(setDef.set);})*
	;
	
simple_set_definition returns [AxisSet set = null;]
	:	
		LBRACE (tupleListDef = tuple_list)? RBRACE {retval.set = tupleListDef.tupleSet;}
	|	tupleDef = tuple_definition {retval.set = tupleDef.tuple;}
	|	memberDef = member_definition {retval.set = memberDef.member;}
	|	allmemberDef = all_member_definition {retval.set = allmemberDef.set;}
	|	rangeDef = range_set_definition {retval.set = rangeDef.set;}
	;

set_operation returns [CompositeAxisSet opSet = null;]
	: 	ASTERISK {retval.opSet = new CrossJoin();}
	|	PLUS {retval.opSet = new Union();}
	;

tuple_list returns [TupleSet tupleSet = new TupleSet();]
	:	firstTupleDef = tuple_definition {retval.tupleSet.add(firstTupleDef.tuple);} (COMMA tupleDef = tuple_definition {retval.tupleSet.add(tupleDef.tuple);})?
	| 	member_list
	;
	
tuple_definition returns [Tuple tuple = null;]
	:	LPAREN listDef = member_list RPAREN {retval.tuple = listDef.memberList.size() == 1 ? listDef.memberList.get(0) : new MemberTuple(listDef.memberList);}
	;

member_list returns [ArrayList<Member> memberList = new ArrayList<>();]
	:	firstMemberDef = member_definition {retval.memberList.add(firstMemberDef.member);} (COMMA memberDef = member_definition {retval.memberList.add(memberDef.member);})*
	;

all_member_definition returns [AllMemberSet set = null;]
	:	hierarchy = quoted_identifier path = member_path_definition DOT level = quoted_identifier DOT MEMBERS 
		{retval.set = new AllMemberSet(Cube.getMemberHierarchy(hierarchy.idValue), level.idValue, buildMemberFromPath(path.members, Cube.getHierarchy(hierarchy.idValue)));}
	;

range_set_definition returns [RangeSet set = null;]
	:	startDef = member_definition COLON endDef = member_definition {retval.set = new RangeSet(Cube.getOrderedHierarchy(startDef.member.getHierarchy().getName()), startDef.member, endDef.member);}
	;

member_definition returns [SimpleMember member = null;]
	:	 hierarchy = quoted_identifier path = non_empty_member_path_definition {retval.member = buildMemberFromPath(path.members, Cube.getHierarchy(hierarchy.idValue));}
	;
	
non_empty_member_path_definition returns [ArrayList<SimpleMember> members = new ArrayList<SimpleMember>();]
	:	(DOT memberDef = one_level_member_definition {retval.members.add(memberDef.member);})+
	;

member_path_definition returns [ArrayList<SimpleMember> members = new ArrayList<SimpleMember>();]
	:	(DOT memberDef = one_level_member_definition {retval.members.add(memberDef.member);})*
	;

one_level_member_definition returns [SimpleMember member = null]
	:	level = quoted_identifier DOT memberName = amp_quoted_identifier {retval.member = new SimpleMember(null, level.idValue, memberName.idValue); }
	;
							
unquoted_identifier returns [String name = null]
	:	nameToken = ID {retval.name = nameToken.getText();}
	;
					
amp_quoted_identifier returns [String idValue = null;]
	:	idToken = AMP_QUOTED_ID{String allStr = idToken.getText(); retval.idValue = allStr.substring(2, allStr.length() - 1);}
	;
					
quoted_identifier returns [String idValue = null;]
	:	idToken = QUOTED_ID{String allStr = idToken.getText(); retval.idValue = allStr.substring(1, allStr.length() - 1);}
	;

fun	:	formulaToken=FUN {dq.setFormula(formulaToken.getText());}
	;

// Typed
QUOTE 		: '\'';
//ASTERISK 	: '*';
COLON 		: ':';
SEMICOLON 	: ';';
COMMA 		: ',';
CONCAT 		: '||';
DOT 		: '.';
EQ 			: '=';
GE 			: '>=';
GT 			: '>';
LBRACE 		: '{';
LE 			: '<=';
LPAREN 		: '(';
LT 			: '<';
MINUS 		: '-';
NE 			: '<>';
//PLUS 		: '+';
RBRACE 		: '}';
RPAREN 		: ')';
SOLIDUS 	: '/';

FUN		: 'AVG' | 'MAX' | 'MIN' | 'COUNT' | 'SUM';

NUMBER  	: ('0'..'9')+
		;
ID		:  ('a'..'z'|'A'..'Z'|'_'|'$') ('a'..'z'|'A'..'Z'|'_'|'0'..'9'|'$')*
		;

AMP_QUOTED_ID: '[&' (ID ((' ' | '\t')+ ID)* | NUMBER) ']'
		;
		
QUOTED_ID: ('[' (ID ((' ' | '\t')+ ID)* | NUMBER) ']')
		;
		
STRING  : '"' (~'"')* '"'
		| '\'' (~'\'')* '\''
		;
		
WS  :   (   ' '
        |   '\t'
        |   '\r'
        |   '\f'
        |   '\n'
        )+
        { $channel=HIDDEN; }
    ;
