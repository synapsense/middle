package com.synapsense.search;

import java.util.Collection;

public interface Traversable {
	Collection<Element> traverseNext(Element e, Path path);
    // TODO : Iteration 2
	// consider some (maybe) useful extensions
	// Traverser traverse(Element startFrom);
	// Traverser traverse(Predicate startFrom);
	// Traverser traverse(Index startFrom);
	//
	// interface Traverser {
	// Element next();
	//
	// Element next(Path path);
	// }

}