package com.synapsense.search;

public class Relations {
	private final static Relation PARENT = named("parent");
	private final static Relation CHILD = named("child");

	public static Relation named(String name) {
		return new NamedRelation(name);
	}

	public static Relation any() {
		return new AnyRelation();
	}
	
	private Relations() {
	}
}

final class AnyRelation implements Relation {
	private static final long serialVersionUID = -4490268679190160189L;

	@Override
	public boolean theSameAs(String name) {
		return true;
	}

	@Override
    public boolean equals(Object obj) {
		if (obj == null) return false;
	    return (AnyRelation.class == obj.getClass());
    }

}

class NamedRelation implements Relation {
	private static final long serialVersionUID = -4490268679190160189L;

	public NamedRelation(String name) {
		super();
		this.name = name;
	}

	@Override
	public boolean theSameAs(String name) {
		return this.name.equals(name);
	}

	private String name;

	@Override
    public int hashCode() {
	    final int prime = 31;
	    int result = 1;
	    result = prime * result + ((name == null) ? 0 : name.hashCode());
	    return result;
    }

	@Override
    public boolean equals(Object obj) {
	    if (this == obj)
		    return true;
	    if (obj == null)
		    return false;
	    if (getClass() != obj.getClass())
		    return false;
	    NamedRelation other = (NamedRelation) obj;
	    if (name == null) {
		    if (other.name != null)
			    return false;
	    } else if (!name.equals(other.name))
		    return false;
	    return true;
    }

}