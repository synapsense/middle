package com.synapsense.search;

import java.io.Serializable;

//TODO : Iteration 2 
//make this to do real work
//need parent-child abstraction to support
public interface Relation extends Serializable {
	boolean theSameAs(String name);
}
