package com.synapsense.search;

import com.synapsense.dto.TO;

public class EnvElement implements Element {
    private static final long serialVersionUID = 8883092819923296870L;

	public EnvElement(TO<?> oid) {
		if (oid==null) throw new NullPointerException("Supplied 'oid' is null");
		this.oid = oid;
	}

	@Override
	public String getKey() {
		return oid.toString();
	}

	@Override
	public Object getValue() {
		return oid;
	}

	@Override
	public <T> T getValue(Class<T> castTo) throws ClassCastException {
		if (castTo==null) throw new NullPointerException("Supplied 'castTo' is null");
		return castTo.cast(oid);
	}
    
	@Override
    public int hashCode() {
	    final int prime = 31;
	    int result = 1;
	    result = prime * result + ((oid == null) ? 0 : oid.hashCode());
	    return result;
    }

	@Override
    public boolean equals(Object obj) {
	    if (this == obj)
		    return true;
	    if (obj == null)
		    return false;
	    if (getClass() != obj.getClass())
		    return false;
	    EnvElement other = (EnvElement) obj;
	    if (oid == null) {
		    if (other.oid != null)
			    return false;
	    } else if (!oid.equals(other.oid))
		    return false;
	    return true;
    }
	
	private TO<?> oid;
	
}
