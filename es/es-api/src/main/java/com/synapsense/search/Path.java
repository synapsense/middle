package com.synapsense.search;

import java.io.Serializable;

public class Path implements Serializable {
	private static final long serialVersionUID = -6026685476247391393L;

	// TODO : Iteration 2
	// make this class to do some real work
	// consider :
	// one Path abstraction instead of single pieces...
	// each path step pruning
	// each path step filtering(result set)

	public Path(Relation relation, Direction direction, boolean includeToResult) {
		this.relation = relation;
		this.direction = direction;
		this.includeToResult = includeToResult;
	}

	public Path(Relation relation, Direction direction) {
		this(relation, direction, true);
	}

	public boolean isPossibleToMove(String to) {
		return (relation.theSameAs(to));
	}

	/**
	 * predicate parody )
	 * 
	 * @return
	 */
	// TODO : need to improve working with predicates
	public boolean includeElements() {
		return includeToResult;
	}

	public Direction getDirection() {
		return direction;
	}

	private Relation relation;
	private Direction direction;
	private boolean includeToResult;

	@Override
    public int hashCode() {
	    final int prime = 31;
	    int result = 1;
	    result = prime * result + ((direction == null) ? 0 : direction.hashCode());
	    result = prime * result + ((relation == null) ? 0 : relation.hashCode());
	    return result;
    }

	@Override
    public boolean equals(Object obj) {
	    if (this == obj)
		    return true;
	    if (obj == null)
		    return false;
	    if (getClass() != obj.getClass())
		    return false;
	    Path other = (Path) obj;
	    if (direction != other.direction)
		    return false;
	    if (relation == null) {
		    if (other.relation != null)
			    return false;
	    } else if (!relation.equals(other.relation))
		    return false;
	    return true;
    }

}
