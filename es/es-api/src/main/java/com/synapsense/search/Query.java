package com.synapsense.search;

public interface Query {
	Results execute(Element from, Traversable traversable);
}
