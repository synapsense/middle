package com.synapsense.search;

/**
 * Implement it if your service support queries
 *  
 * @author shabanov
 *
 */
public interface Queryable {
	Results executeQuery(QueryDescription queryDescr);
}
