package com.synapsense.search;

public interface Predicate {
	boolean eval(Element e);
}
