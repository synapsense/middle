package com.synapsense.search;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import org.apache.log4j.Logger;

import com.synapsense.util.CollectionUtils;

public class QueryImpl implements Query {

	private final static Logger logger = Logger.getLogger(QueryImpl.class);

	public QueryImpl(QueryDescription desc) {
		this.depth = desc.getDepth();
		this.paths = desc.getPaths();
		this.addStartElement = desc.isAddStartElementToResult();
	}

	@Override
	public Results execute(Element from, Traversable traversable) {
		Stack<Element> stack = new Stack<Element>();
		stack.add(from);
		List<Element> filtered = traverse(traversable, stack, 1);
		if (addStartElement) {
			filtered.add(from);
		}
		return new ResultsImpl(filtered);
	}

	private List<Element> traverse(Traversable traversable, Stack<Element> e, int level) {
		List<Element> result = CollectionUtils.newList();
		Stack<Element> stack = new Stack<Element>();
		if (level > depth) {
			return stack;
		}
		if (e.isEmpty()) {
			return stack;
		}

		// when no explicit path specified , only pruning action is possible
		List<Path> levelPath = paths.get(level);
		if (levelPath == null) {
			if (logger.isDebugEnabled()) {
				logger.debug("Level " + level + " paths to traverse are not specified. Prune.");
			}
			return stack;
		}

		for (Path path : levelPath) {
			while (!e.isEmpty()) {
				Collection<Element> toTraverse = traversable.traverseNext(e.pop(), path);
				for (Element elem : toTraverse) {
					stack.push(elem);
					if (path.includeElements()) {
						result.add(elem);
					}
				}
			}
		}

		result.addAll(traverse(traversable, stack, ++level));
		return result;
	}

	private boolean addStartElement;
	private int depth;
	private Map<Integer, List<Path>> paths;

}
