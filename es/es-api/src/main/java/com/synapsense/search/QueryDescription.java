package com.synapsense.search;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.synapsense.dto.TO;
import com.synapsense.util.CollectionUtils;

/**
 * Query descriptor.
 * Use it to request objects from <code>Environment</code>.
 * 
 * Limitations : 
 * 	does not support advanced pruning and filtering
 *  
 * @author shabanov
 *
 */
public class QueryDescription implements Serializable {
	private static final long serialVersionUID = -2491858294454626257L;

	public QueryDescription(int depth) {
		this.depth = depth;
	}

	public QueryDescription addPath(int level, final Path path) {
		if (level > depth)
			throw new IllegalArgumentException("Supplied 'level' cannot be less than or equal to " + depth);

		paths.put(level, new LinkedList<Path>() {
			private static final long serialVersionUID = 816535267522642521L;
			{
				add(path);
			}
		});

		return this;
	}

	public QueryDescription startQueryFrom(TO<?> element) {
		this.startFrom = element;
		return this;
	}

	public QueryDescription addStartElementToResult(boolean flag) {
		this.addStartElementToResult = flag;
		return this;
	}

	public Map<Integer, List<Path>> getPaths() {
		return paths;
	}

	public int getDepth() {
		return depth;
	}

	public TO<?> getStartElement() {
		return startFrom;
	}

	public boolean isAddStartElementToResult() {
		return addStartElementToResult;
	}

	@Override
    public int hashCode() {
	    final int prime = 31;
	    int result = 1;
	    result = prime * result + (addStartElementToResult ? 1231 : 1237);
	    result = prime * result + depth;
	    result = prime * result + ((paths == null) ? 0 : paths.hashCode());
	    result = prime * result + ((startFrom == null) ? 0 : startFrom.hashCode());
	    return result;
    }

	@Override
    public boolean equals(Object obj) {
	    if (this == obj)
		    return true;
	    if (obj == null)
		    return false;
	    if (getClass() != obj.getClass())
		    return false;
	    QueryDescription other = (QueryDescription) obj;
	    if (addStartElementToResult != other.addStartElementToResult)
		    return false;
	    if (depth != other.depth)
		    return false;
	    if (paths == null) {
		    if (other.paths != null)
			    return false;
	    } else if (!paths.equals(other.paths))
		    return false;
	    if (startFrom == null) {
		    if (other.startFrom != null)
			    return false;
	    } else if (!startFrom.equals(other.startFrom))
		    return false;
	    return true;
    }
	
	private TO<?> startFrom;
	private int depth = 1;
	private Map<Integer, List<Path>> paths = CollectionUtils.newMap();
	private boolean addStartElementToResult;

}
