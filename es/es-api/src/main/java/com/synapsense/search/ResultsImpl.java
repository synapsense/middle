package com.synapsense.search;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class ResultsImpl implements Results {
	private static final long serialVersionUID = 399385197734479207L;

	private List<Element> results;

	public ResultsImpl(final List<Element> results) {
		this.results = Collections.unmodifiableList(results);
	}

	@Override
	public int size() {
		return results.size();
	}

	@Override
	public Iterator<Element> iterator() {
		return results.iterator();
	}

	@Override
	public String toString() {
		return results.toString();
	}

}
