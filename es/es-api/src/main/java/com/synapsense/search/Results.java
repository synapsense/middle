package com.synapsense.search;

import java.io.Serializable;

public interface Results extends Iterable<Element>, Serializable {
	int size();
}
