package com.synapsense.search;

import java.io.Serializable;

public interface Element extends Serializable {
	String getKey();

	Object getValue();

	<T> T getValue(Class<T> castTo) throws ClassCastException;
}
