package com.synapsense.dto;

import java.io.Serializable;

/**
 * Import Error.
 * 
 * @author Alex Pakhunov
 * @author Gabriel Helman
 * 
 *         Reports an import error for a specific object. Generally, this acts
 *         as a wrapper for an exception thrown in the bowels of the server.
 *         Should be returned to as part of the errors list in a
 *         <code>DLImportResult</code> object.
 * 
 * @see DLImportResult
 */
public class DLImportError implements Serializable {
	private static final long serialVersionUID = 2157888152010985906L;

	private String dlid;
	private String message;
	private Throwable cause;

	/**
	 * Constructs a new error not based directly on an exception.
	 * 
	 * @param dlid
	 *            the unique ID provided by the exporting program (let's be
	 *            honest; that means MapSense.)
	 * @param message
	 *            any text message that should be echoed to the user of the
	 *            exporting program about this error
	 */
	public DLImportError(String dlid, String message) {
		super();
		this.dlid = dlid;
		this.message = message;
	}

	/**
	 * Constructs a new error wrapping an exception (or other Throwable) as the
	 * cause of the error.
	 * 
	 * @param dlid
	 *            the unique ID provided by the exporting program (let's be
	 *            honest; that means MapSense.)
	 * @param message
	 *            any text message that should be echoed to the user of the
	 *            exporting program about this error in addition to any message
	 *            from the cause itself
	 * @param cause
	 *            the exception that caused this error to be reported
	 */
	public DLImportError(String dlid, String message, Throwable cause) {
		super();
		this.dlid = dlid;
		this.message = message;
		this.cause = cause;
	}

	public String getDlid() {
		return dlid;
	}

	public String getMessage() {
		return message;
	}

	public Throwable getCause() {
		return cause;
	}

	public void setCause(Throwable cause) {
		this.cause = cause;
	}

	@Override
	public String toString() {
		return "DLImportError [dlid=" + dlid + ", message=" + message + ", cause=" + cause + "]";
	}

}
