package com.synapsense.dto;

/**
 * Interface of ID transfer object. This ID is used to uniquely identify objects
 * in the environment model. All identifiable object types is defined in
 * <code>Types</code> enum.
 * 
 * Read-only interface.
 * 
 * @author shabanov
 * @param <KEY_TYPE>
 *            Type of TO identifier
 */
public interface TO<KEY_TYPE> {

	/**
	 * Allows to get object ID.
	 * 
	 * @return object ID
	 */
	KEY_TYPE getID();

	/**
	 * Allows to get TO type name.
	 * 
	 * @return type name
	 */
	String getTypeName();
	
	/**
	 * Allows to get ES ID.
	 * 
	 * @return ES ID
	 */
	KEY_TYPE getEsID();
}
