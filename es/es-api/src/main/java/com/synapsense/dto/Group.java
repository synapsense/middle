package com.synapsense.dto;

public class Group implements java.io.Serializable {

	// private static final long serialVersionUID = -8668862155452334833L;

	private String name;
	private String description;
	private String statistics;

	public Group(String name, String description) {
		if (name == null || name.isEmpty())
			throw new IllegalArgumentException("name must be not null or empty");
		this.name = name;
		this.description = description != null ? description : "";
		this.statistics = "";
	}

	public Group(String name, String description, String statistics) {
		this(name, description);
		this.setStatistics(statistics);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description != null ? description : "";
	}

	public String getStatistics() {
		return statistics;
	}

	public void setStatistics(String statistics) {
		this.statistics = statistics != null ? statistics : "";
	}

	@Override
	public int hashCode() {
		return name.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Group other = (Group) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

}
