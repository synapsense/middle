/**
 * Contains Data Transfer Object classes used
 * for information exchange between Environment
 * services and client applications.
 */
package com.synapsense.dto;