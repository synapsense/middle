package com.synapsense.dto;

import java.io.Serializable;
import java.util.Iterator;
import java.util.TreeSet;

/**
 * Transfer object to carry history of object as a single object instance
 * 
 * Not thread-safe
 * 
 * @author shabanov
 * 
 */
public final class EnvObjectHistory implements Iterable<HistoricalRecord>, Serializable {
    private static final long serialVersionUID = 4680408142988170581L;
    
	public EnvObjectHistory(TO<?> envRef) {
		this.envRef = envRef;
	}

	public void addHistoricalRecord(HistoricalRecord newRecord) {
		historicalValues.add(newRecord);
	}

	public int size() {
		return historicalValues.size();
	}

	@Override
	public Iterator<HistoricalRecord> iterator() {
		return historicalValues.descendingIterator();
	}

	public HistoricalRecord getLast() {
		return historicalValues.last();
	}
	
	public HistoricalRecord getFirst() {
		return historicalValues.first();
	}
	
	public TO<?> historyOwner() { 
		return envRef;
	}
	
	private TO<?> envRef;
	private TreeSet<HistoricalRecord> historicalValues = new TreeSet<HistoricalRecord>();

}
