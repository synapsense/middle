package com.synapsense.dto;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.Set;

import com.synapsense.util.CollectionUtils;
import com.synapsense.util.Pair;

/**
 * Data transfer object for alert filter concept. To create
 * <code>AlertFilter</code> instance please use {@link AlertFilterBuilder}
 * helper.
 * 
 * @author shabanov
 * 
 */
public class AlertFilter implements Serializable {
	private static final long serialVersionUID = -4567841647180663449L;

	private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS Z");
	private final Set<TO<?>> hosts = CollectionUtils.newSet();
	private final Set<Pair<Date, Date>> intervals = CollectionUtils.newSet();
	private final Set<AlertStatus> statuses = CollectionUtils.newSet();
	private boolean propagate = false;

	AlertFilter() {
	}
	
	AlertFilter(AlertFilter copy) {
		this.hosts.addAll(copy.hosts);
		this.intervals.addAll(copy.intervals);
		this.statuses.addAll(copy.statuses);
		this.propagate = copy.propagate;
	}

	public Set<TO<?>> getHosts() {
		return hosts;
	}

	public Set<Pair<Date, Date>> getIntervals() {
		return intervals;
	}

	public Set<AlertStatus> getStatuses() {
		return statuses;
	}

	public boolean isPropagate() {
		return propagate;
	}

	public boolean isActive() {
		return (isAnyStatus() || statuses.contains(AlertStatus.ACKNOWLEDGED) || statuses.contains(AlertStatus.OPENED));
	}

	public boolean isHistorical() {
		return (isAnyStatus() || statuses.contains(AlertStatus.RESOLVED) || statuses.contains(AlertStatus.DISMISSED));
	}

	public boolean isAnyStatus() {
		return statuses.isEmpty() || statuses.size() == 4;
	}

	void addHosts(final Collection<? extends TO<?>> hostsToAdd) {
		for (TO<?> host : hostsToAdd) {
			hosts.add(host);
		}
	}

	void addHost(final TO<?> host) {
		addHosts(Arrays.<TO<?>>asList(host));
	}

	void setPropagate(boolean flag) {
			propagate = flag;
	}

	void addInterval(final Date begin, final Date end) {
		if (begin.after(end)) {
			throw new IllegalArgumentException("Supplied interval is not valid");
		}

		Pair<Date, Date> newInterval = new Pair<Date, Date>(begin, end);

		for (Pair<Date, Date> interval : intervals) {
			if (hasIntersection(interval, newInterval)) {
				throw new IllegalArgumentException("Supplied interval [" + sdf.format(begin) + ".." + sdf.format(end)
				        + "] intercepts existing one : [" + sdf.format(interval.getFirst()) + ".."
				        + sdf.format(interval.getSecond()) + "]");
			}
		}
		intervals.add(newInterval);
	}

	void addStatus(AlertStatus alertStatus) {
		statuses.add(alertStatus);
	}

	void clearStatuses() {
		statuses.clear();
	}

	void clearHosts() {
		hosts.clear();
	}

	private boolean hasIntersection(Pair<Date, Date> fi, Pair<Date, Date> si) {
		long r1 = fi.getFirst().getTime();
		long r2 = fi.getSecond().getTime();
		long r3 = si.getFirst().getTime();
		long r4 = si.getSecond().getTime();

		if (r1 >= r3 && r1 <= r4) {
			return true;
		}
		if (r2 >= r3 && r2 <= r4) {
			return true;
		}
		return false;
	}

}
