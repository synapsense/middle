package com.synapsense.dto;

import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;

/**
 * 
 * Describes a single CRE rule instance. Contains the target object and property
 * descriptors as well as source properties identifiers
 * 
 * @author Dmitry Grudzinskiy
 * 
 */
public class CRERule implements Serializable {

	private static final long serialVersionUID = -5454507085209908687L;

	/**
	 * Name of the rule. Must be unique among all the rules in the system
	 * 
	 */
	private String name;

	/**
	 * Identifier of the type of this rule
	 * 
	 */
	private TO<?> ruleType;

	/**
	 * Identifier of the object the rule corresponds to
	 * 
	 */
	private TO<?> destObjId;

	/**
	 * Name of the property the rule calculates
	 * 
	 */
	private String destPropertyName;

	/**
	 * Collection of source properties of the rule
	 * 
	 */
	private HashSet<PropertyBinding> srcProperties = new HashSet<PropertyBinding>();

	public CRERule(String name, TO<?> ruleType, TO<?> destObjId, String destPropertyName) {
		this.name = name;
		this.ruleType = ruleType;
		this.destObjId = destObjId;
		this.destPropertyName = destPropertyName;
	}

	public CRERule(String name, TO<?> ruleType) {
		this(name, ruleType, null, null);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public TO<?> getRuleType() {
		return ruleType;
	}

	public void setRuleType(TO<?> ruleType) {
		this.ruleType = ruleType;
	}

	public TO<?> getDestObjId() {
		return destObjId;
	}

	public void setDestObjId(TO<?> destObjId) {
		this.destObjId = destObjId;
	}

	public String getDestPropertyName() {
		return destPropertyName;
	}

	public void setDestPropertyName(String destPropertyName) {
		this.destPropertyName = destPropertyName;
	}

	/**
	 * The method adds a source property to the rule
	 * 
	 * @param objId
	 *            TO of the object of the property
	 * @param propName
	 *            environment property name
	 * @param propBinding
	 *            name of the property member inside rule source code
	 * 
	 * @return true if the rule did not already contain this specific binding
	 */
	public boolean addSrcProperty(TO<?> objId, String propName, String propBinding) {
		return srcProperties.add(new PropertyBinding(objId, propName, propBinding));
	}

	public Collection<PropertyBinding> getSrcProperties() {
		return Collections.unmodifiableCollection(srcProperties);
	}

	public class PropertyBinding implements Serializable {

		private static final long serialVersionUID = 3888713143393011230L;

		private TO<?> objId;
		private String propName;
		private String propBinding;

		private PropertyBinding(TO<?> objId, String propName, String propBinding) {
			this.objId = objId;
			this.propName = propName;
			this.propBinding = propBinding;
		}

		public String getPropName() {
			return propName;
		}

		public String getPropBinding() {
			return propBinding;
		}

		public TO<?> getObjId() {
			return objId;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + getOuterType().hashCode();
			result = prime * result + ((objId == null) ? 0 : objId.hashCode());
			result = prime * result + ((propBinding == null) ? 0 : propBinding.hashCode());
			result = prime * result + ((propName == null) ? 0 : propName.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			PropertyBinding other = (PropertyBinding) obj;
			if (!getOuterType().equals(other.getOuterType()))
				return false;
			if (objId == null) {
				if (other.objId != null)
					return false;
			} else if (!objId.equals(other.objId))
				return false;
			if (propBinding == null) {
				if (other.propBinding != null)
					return false;
			} else if (!propBinding.equals(other.propBinding))
				return false;
			if (propName == null) {
				if (other.propName != null)
					return false;
			} else if (!propName.equals(other.propName))
				return false;
			return true;
		}

		private CRERule getOuterType() {
			return CRERule.this;
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((destObjId == null) ? 0 : destObjId.hashCode());
		result = prime * result + ((destPropertyName == null) ? 0 : destPropertyName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CRERule other = (CRERule) obj;
		if (destObjId == null) {
			if (other.destObjId != null)
				return false;
		} else if (!destObjId.equals(other.destObjId))
			return false;
		if (destPropertyName == null) {
			if (other.destPropertyName != null)
				return false;
		} else if (!destPropertyName.equals(other.destPropertyName))
			return false;
		return true;
	}
}
