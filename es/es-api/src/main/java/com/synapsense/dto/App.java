package com.synapsense.dto;

import java.util.HashSet;
import java.util.Set;

public enum App {
	ES(0x01), WEB_CONSOLE(0x02);
	
	private int system;
	
	private App(int system){
		this.system = system;
	}
	
	public int getSystem(){
		return system;
	}
	
	public static int getSystem(Set<App> apps){
		int status = 0x00;
		for(App app : apps){
			status|=app.system;
		}
		return status;
	}
	
	public static Set<App> getApps(int status){
		Set<App> apps = new HashSet<App>();
		for(App app : values()){
			if ((app.system & status) == app.system){
				apps.add(app);
			}
		}
		
		return apps;
	}
	
}
