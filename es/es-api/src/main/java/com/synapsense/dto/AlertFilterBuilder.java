package com.synapsense.dto;

import java.util.Collection;
import java.util.Date;

/**
 * Alert filter builder.<br>
 * Call methods to build filter you need Fluent design is used here allowing
 * statements like :
 * 
 * <pre>
 * {@code 
 * 	AlertFilter filter = new AlertFilterBuilder().active().propagate().onHost(TO).between(Date s, Date e).build();
 * 	//...
 * }
 * </pre>
 * 
 * You can mix any options provided.
 * 
 * @author shabanov
 * 
 */
public class AlertFilterBuilder {

	private AlertFilter filter;

	public AlertFilterBuilder() {
		filter = new AlertFilter();
	}

	public AlertFilterBuilder(AlertFilter toClone) {
		filter = new AlertFilter(toClone);
	}

	public AlertFilter build() {
		// add validation code here if needed
		if (filter.getIntervals().isEmpty()) {
			filter.addInterval(new Date(0L), new Date());
		}
		return filter;
	}

	public AlertFilterBuilder newFilter() {
		this.filter = new AlertFilter();
		return this;
	}

	public AlertFilterBuilder onHosts(Collection<TO<?>> ids) {
		if (ids == null) {
			throw new IllegalArgumentException("Supplied 'ids' is null");
		}
		filter.addHosts(ids);
		return this;
	}

	public AlertFilterBuilder onHost(TO<?> id) {
		if (id == null) {
			throw new IllegalArgumentException("Supplied 'id' is null");
		}
		filter.addHost(id);
		return this;
	}

	public AlertFilterBuilder system() {
		filter.addHost(TOFactory.EMPTY_TO);
		return this;
	}

	public AlertFilterBuilder between(Date begin, Date end) {
		if (begin == null) {
			throw new IllegalArgumentException("Supplied 'begin' is null");
		}
		if (end == null) {
			throw new IllegalArgumentException("Supplied 'end' is null");
		}
		filter.addInterval(begin, end);
		return this;
	}

	public AlertFilterBuilder active() {
		filter.addStatus(AlertStatus.OPENED);
		filter.addStatus(AlertStatus.ACKNOWLEDGED);
		return this;
	}

	public AlertFilterBuilder historical() {
		filter.addStatus(AlertStatus.DISMISSED);
		filter.addStatus(AlertStatus.RESOLVED);
		return this;
	}

	public AlertFilterBuilder acknowledged() {
		filter.addStatus(AlertStatus.ACKNOWLEDGED);
		return this;
	}

	public AlertFilterBuilder dismissed() {
		filter.addStatus(AlertStatus.DISMISSED);
		return this;
	}

	public AlertFilterBuilder resolved() {
		filter.addStatus(AlertStatus.RESOLVED);
		return this;
	}

	public AlertFilterBuilder opened() {
		filter.addStatus(AlertStatus.OPENED);
		return this;
	}

	public AlertFilterBuilder propagate(boolean flag) {
		filter.setPropagate(flag);
		return this;
	}

	public AlertFilterBuilder onSingleHostOnly(TO<?> host) {
		if (host == null) {
			throw new IllegalArgumentException("Supplied 'host' is null");
		}
		filter.clearHosts();
		filter.addHost(host);
		return this;
	}

}
