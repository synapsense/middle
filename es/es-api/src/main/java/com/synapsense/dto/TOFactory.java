package com.synapsense.dto;

import java.io.Serializable;
import java.net.MalformedURLException;
import java.util.Comparator;

import com.synapsense.service.impl.dao.to.GenericObjectTO;

/**
 * TO factory. Can create TO instance from string and save TO instance to the
 * string. Provides single-valued transformation {TO-String-TO}.
 * 
 * @author shabanov
 * 
 */
public class TOFactory {
	/**
	 * single instance reference
	 */
	public static TOFactory INSTANCE = new TOFactory();

	/**
	 * Default TO comparator
	 */
	private static final Comparator<TO<?>> DEFAULT_COMPARATOR = new Comparator<TO<?>>() {
		@Override
		public int compare(TO<?> to1, TO<?> to2) {
			// compares type names
			int typeNameCompare = to1.getTypeName().compareTo(to2.getTypeName());
			// if they are not equals
			if (typeNameCompare != 0) {
				// return comparing result "as is"
				return typeNameCompare;
			}
			// compares TO ids
			int idCompare = ((Integer) to1.getID()).compareTo((Integer) to2.getID());
			// return comparing result
			return idCompare;
		}

	};

	/**
	 * Empty TO reference.
	 */
	public final static TO<?> EMPTY_TO = new EmptyTOImpl();

	/**
	 * Class that implements Empty TO concept.
	 * 
	 * @author shabanov
	 * 
	 */
	private static final class EmptyTOImpl implements TO<Serializable>, Serializable {

		private static final long serialVersionUID = -6901888807478642565L;

		private int id;

		private String type = "";

		private EmptyTOImpl() {
			id = -1;
		}

		@Override
		public Serializable getID() {
			return this.id;
		}

		@Override
		public String getTypeName() {
			return this.type;
		}

		@Override
		public Serializable getEsID() {
			return null;
		}

		@Override
		public int hashCode() {
			return 0;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			// bug#1541
			if (!getClass().equals(obj.getClass()))
				return false;
			return true;
		}

		@Override
		public String toString() {
			return "EMPTY_TO";
		}
	}

	/**
	 * Helper class for parsing string.
	 * 
	 * @author shabanov
	 * 
	 */
	private static class TOFormatParser {
		public static final String DEFAULT_DELIMITER = ":";

		/**
		 * parse input string to the two parts : substring of toData from begin
		 * to the first hit of delimiter-1 substring of toData from delimiter+1
		 * to the end of string. The all other delimiter hits are ignored.
		 * 
		 * @param toData
		 *            Input string that has suitable format
		 * @param delimiter
		 * @return Split toData as String[2]
		 */
		public static String[] parseData(String toData, String delimiter) {
			int delimiterPos = toData.indexOf(delimiter);
			int delimiterPos2 = toData.indexOf(delimiter, delimiterPos + 1);
			// if delimiter was not found or it is placed as first char in the
			// input string
			if (delimiterPos < 1) {
				// return empty array
				return new String[0];
			}
			String[] parsed;
			if (delimiterPos2 == -1) {
				parsed = new String[2];
				parsed[0] = toData.substring(0, delimiterPos);
				parsed[1] = toData.substring(delimiterPos + 1);
			} else {
				parsed = new String[3];
				parsed[0] = toData.substring(0, delimiterPos);
				parsed[1] = toData.substring(delimiterPos + 1, delimiterPos2);
				parsed[2] = toData.substring(delimiterPos2 + 1);
			}
			return parsed;
		}

		public static String[] parseData(String toData) {
			return parseData(toData, DEFAULT_DELIMITER);
		}
	}

	private TOFactory() {
		// singletone
	}

	public static TOFactory getInstance() {
		return INSTANCE;
	}

	/**
	 * Returns the actual TO comparator.
	 * 
	 * @return Internal default <code>Comparator</code> implementation
	 */
	public Comparator<TO<?>> comparator() {
		return DEFAULT_COMPARATOR;
	}

	public boolean isValid(String toData) {
		if (toData == null || toData.isEmpty()) {
			throw new IllegalArgumentException("Supplied 'toData' is null or empty");
		}

		String[] typeIdPair = TOFormatParser.parseData(toData);

		if (typeIdPair.length != 2 && typeIdPair.length != 3) {
			return false;
		}

		if (typeIdPair[0].isEmpty()) {
			return false;
		}

		if (typeIdPair[1].isEmpty()) {
			return false;
		}

		try {
			int id = Integer.parseInt(typeIdPair[1]);
			if (id < 1 && id != -1) {
				return false;
			}
		} catch (NumberFormatException e) {
			return false;
		}

		if (typeIdPair.length == 3) {
			if (typeIdPair[2].isEmpty()) {
				return false;
			}

			int esId = 0;
			try {
				esId = Integer.parseInt(typeIdPair[2]);
			} catch (NumberFormatException e) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Returns string representation of TO. The format of returning string is
	 * always {TypeName:ObjectId}
	 * 
	 * @param to
	 *            transfer object to save
	 * @return String
	 */
	public String saveTO(TO<?> to) {
		return to.getTypeName() + ":" + to.getID() + ((to.getEsID() != null) ? ":" + to.getEsID() : "");
	}

	public TO<?> loadTO(String type, int id) {
		if (id < 1 && id != -1) {
			throw new IllegalArgumentException("Supplied 'id' is negative");
		}

		if (id == -1) {
			return EMPTY_TO;
		}

		return new GenericObjectTO(id, type);
	}

	/**
	 * Allows to get TO instance from string.
	 * 
	 * @param toData
	 * @return
	 */
	public TO<?> loadTO(String toData) {
		if (toData == null || toData.isEmpty()) {
			throw new IllegalArgumentException("Supplied 'toData' is null or empty");
		}

		String[] typeIdPair = TOFormatParser.parseData(toData);

		if (typeIdPair.length != 2 && typeIdPair.length != 3) {
			throw new IllegalArgumentException(
			        "Supplied 'toData' has to be of the following format: TypeName:ObjectId or TypeName:ObjectId:EsID");
		}

		if (typeIdPair[0].isEmpty()) {
			throw new IllegalArgumentException("Supplied 'toData' has empty 'TypeName' part");
		}

		if (typeIdPair[1].isEmpty()) {
			throw new IllegalArgumentException("Supplied 'toData' has empty 'ObjectId' part");
		}

		int id = 0;
		try {
			id = Integer.parseInt(typeIdPair[1]);
		} catch (NumberFormatException e) {
			throw new IllegalArgumentException("Supplied 'ObjectId' part of 'toData' has invalid number format", e);
		}

		if (typeIdPair.length == 3) {
			if (typeIdPair[2].isEmpty()) {
				throw new IllegalArgumentException("Supplied 'toData' has empty 'EsID' part");
			}

			int esId = 0;
			try {
				esId = Integer.parseInt(typeIdPair[2]);
			} catch (NumberFormatException e) {
				throw new IllegalArgumentException("Supplied 'EsID' part of 'toData' has invalid number format", e);
			}

			return loadTO(typeIdPair[0], id, esId);
		}

		return loadTO(typeIdPair[0], id);
	}

	public TO<?> loadTO(String type, int id, int esId) {
		if (id < 1 && id != -1) {
			throw new IllegalArgumentException("Supplied 'id' is negative");
		}

		return new GenericObjectTO(id, type, esId);
	}

	/**
	 * Test example
	 * 
	 * @param args
	 *            not need
	 * @throws MalformedURLException
	 * @throws ClassNotFoundException
	 */
	public static void main(String[] args) throws MalformedURLException, ClassNotFoundException {
		TOFactory factory = TOFactory.getInstance();

		// null toData check
		try {
			factory.loadTO(null);
			assert false;
		} catch (IllegalArgumentException e) {
		}

		// empty toData check
		try {
			factory.loadTO("");
			assert false;
		} catch (IllegalArgumentException e) {
		}

		// empty type name
		try {
			factory.loadTO(":102");
			assert false;
		} catch (IllegalArgumentException e) {
		}

		// invalid objectId
		try {
			factory.loadTO("ALERT:1o2");
			assert false;
		} catch (IllegalArgumentException e) {
		}

		// empty object id
		try {
			factory.loadTO("ALERT:");
			assert false;
		} catch (IllegalArgumentException e) {
		}

		// too much delimiters check
		try {
			factory.loadTO("ALERT::123");
			assert false;
		} catch (IllegalArgumentException e) {
		}

		// no delimiter check
		try {
			factory.loadTO("ALERT123");
			assert false;
		} catch (IllegalArgumentException e) {
		}

		// negative object id
		try {
			factory.loadTO("ALERT:-1");
			assert false;
		} catch (IllegalArgumentException e) {
		}

		// zero object id
		try {
			factory.loadTO("ALERT:0");
			assert false;
		} catch (IllegalArgumentException e) {
		}

		try {
			factory.loadTO("ALERT:" + Integer.MIN_VALUE);
			assert false;
		} catch (IllegalArgumentException e) {
		}

		try {
			factory.loadTO("ALERT:10.2");
		} catch (IllegalArgumentException e) {
		}

		try {
			factory.loadTO("ALERT:" + Long.MAX_VALUE);
		} catch (IllegalArgumentException e) {
		}

		TO<?> toOriginAlert = factory.loadTO("ALERT:102");
		TO<?> toLoadedAlert = factory.loadTO("ALERT:102");
		assert toOriginAlert.equals(toLoadedAlert) : "The saved version of TO is incorrect";

		String savedToAlert = factory.saveTO(toOriginAlert);
		assert "ALERT:102".equals(savedToAlert) : "The saved version of TO is incorrect";

		assert factory.EMPTY_TO.equals(factory.EMPTY_TO);

		assert "EMPTY_TO".equals(factory.EMPTY_TO.toString());

		/*
		 * URL urlTOFactory1 = new File(
		 * "d://git//synapsense3//SynapServ//SynapServer//release//synapsense-client-all.jar"
		 * ).toURL();
		 * 
		 * URL urlTOFactory2 = new File(
		 * "d://git//synapsense3//SynapServ//SynapServer//synapsense-client-all.jar"
		 * ).toURL();
		 * 
		 * 
		 * URL[] urls1 = new URL[]{urlTOFactory1}; URL[] urls2 = new
		 * URL[]{urlTOFactory2};
		 * 
		 * ClassLoader cl1 = new URLClassLoader(urls1);
		 * 
		 * ClassLoader cl2 = new URLClassLoader(urls2);
		 * 
		 * Class c1 = cl1.loadClass("com.synapsense.dto.TOFactory");
		 * 
		 * Class c2 = cl2.loadClass("com.synapsense.dto.TOFactory");
		 * 
		 * assert c1.equals(c2);
		 */

		TO sensor = factory.loadTO("SENSOR:1");
		TO node1 = factory.loadTO("NODE:1");
		TO node2 = factory.loadTO("NODE:2");

		long start = System.currentTimeMillis();
		for (int i = 0; i < 1000000; i++) {
			factory.comparator().compare(node2, node1);
		}

		long total = System.currentTimeMillis() - start;
		System.out.print(total);

		final int OBJECTS_NUMBER = 10000000;
		start = System.currentTimeMillis();
		for (int i = 1; i < OBJECTS_NUMBER; i++) {
			TO id = new GenericObjectTO(i, "type");
		}
		System.out.println("\n.....testing performance.....");
		total = System.currentTimeMillis() - start;
		System.out.println("Creating explicitly via constructor:" + total);

		start = System.currentTimeMillis();
		for (int i = 1; i < OBJECTS_NUMBER; i++) {
			TO id = factory.loadTO("type:" + i);
		}
		total = System.currentTimeMillis() - start;
		System.out.println("Creating via factory:" + total);

	}
}
