package com.synapsense.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Class for representing collection of object properties values DTO for
 * transfer object id based collections of any typed values.
 * 
 * @author shabanov
 */
public class CollectionTO implements Serializable {

	private static final long serialVersionUID = 6756474647417839793L;

	/**
	 * TO of the object
	 */
	private TO<?> objId;

	/**
	 * Props. values map
	 */
	private Map<String, List<ValueTO>> propertyMap;

	/**
	 * All fields constructor
	 * 
	 * @param objId
	 *            Object ID
	 * @param propValuesSize
	 *            Initial size for value list
	 */
	public CollectionTO(TO<?> objId, int propValuesSize) {
		this.objId = objId;
		this.propertyMap = new HashMap<String, List<ValueTO>>(propValuesSize);
	}

	/**
	 * @see #CollectionTO(TO, int)
	 * @param objId
	 *            Object ID
	 */
	public CollectionTO(TO<?> objId) {
		this(objId, 10);
	}

	public CollectionTO(TO<?> objId, List<ValueTO> propValues) {
		this.objId = objId;
		this.propertyMap = new HashMap<String, List<ValueTO>>(propValues.size());
		addAll(propValues);
	}

	/**
	 * Returns the id of the object
	 * 
	 * @return TO of the object
	 */
	public TO<?> getObjId() {
		return objId;
	}

	/**
	 * Add the specified property to the collection
	 * 
	 * @param data
	 *            - ValueTO of the property
	 */
	public void addValue(ValueTO data) {
		List<ValueTO> namedValues = propertyMap.get(data.getPropertyName());
		if (namedValues == null) {
			namedValues = new ArrayList<ValueTO>();
			propertyMap.put(data.getPropertyName(), namedValues);
		}
		namedValues.add(data);
	}

	/**
	 * Adds all of the specified properties to the collection
	 * 
	 * @param valuesTO
	 *            - the collection to be inserted
	 */
	public void addAll(Collection<? extends ValueTO> valuesTO) {
		for (ValueTO valueTO : valuesTO) {
			addValue(valueTO);
		}
	}

	/**
	 * Removes the property from the collectionTO if it is present
	 * 
	 * @param valueTO
	 *            - valueTO for the property to remove
	 * @return true if the collection contained specified property
	 */
	public boolean remove(ValueTO valueTO) {
		List<ValueTO> namedValues = propertyMap.get(valueTO.getPropertyName());
		if (namedValues == null || !namedValues.remove(valueTO)) {
			// this property is not present
			return false;
		}
		if (namedValues.isEmpty()) {
			propertyMap.remove(valueTO.getPropertyName());
		}
		return true;
	}

	/**
	 * Removes from this CollectionTO all of its valuesTO that are contained in
	 * the specified collection
	 * 
	 * @param valuesTO
	 *            - collection to remove
	 * @return true if the collectionTO changed as a result of the call
	 */
	public boolean removeAll(Collection<ValueTO> valuesTO) {
		boolean isChanged = false;
		for (ValueTO valueTO : valuesTO) {
			if (remove(valueTO)) {
				isChanged = true;
			}
		}
		return isChanged;
	}

	/**
	 * Returns read-only list of all the properties from the collection
	 * 
	 * @return an unmodifiable list of all the properties from the collection
	 */
	public List<ValueTO> getPropValues() {
		List<ValueTO> allValues = new ArrayList<ValueTO>();
		for (String propName : propertyMap.keySet()) {
			allValues.addAll(propertyMap.get(propName));
		}
		return Collections.unmodifiableList(allValues);
	}

	/**
	 * Returns property values from the collection by the specified name or null
	 * if the collection doesn't contain the property with this name
	 * 
	 * @param propertyName
	 *            - name of the required property
	 * @return List of ValueTO of the property
	 */
	public List<ValueTO> getPropValue(String propertyName) {
		return propertyMap.get(propertyName);
	}

	/**
	 * Returns the first element from the list of property values or null if the
	 * collection doesn't contain the property with this name or property list
	 * is empty.
	 * 
	 * @param propertyName
	 *            - name of the required property
	 * @return List of ValueTO of the property
	 */
	public ValueTO getSinglePropValue(String propertyName) {
		List<ValueTO> values = propertyMap.get(propertyName);
		if (values == null || values.isEmpty()) {
			return null;
		}
		return values.get(0);
	}

	@Override
	public String toString() {
		return "Props.array for TO[" + objId.toString() + "];values:" + this.getPropValues().toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((objId == null) ? 0 : objId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final CollectionTO other = (CollectionTO) obj;
		if (objId == null) {
			if (other.objId != null)
				return false;
		} else if (!objId.equals(other.objId))
			return false;
		if (propertyMap == null) {
			if (other.propertyMap != null)
				return false;
		} else if (!propertyMap.equals(other.propertyMap))
			return false;
		return true;
	}
}
