package com.synapsense.dto;

/**
 * Enumeration of all supported varieties of notification messages.
 * 
 * @author shabanov
 * 
 */
public enum MessageType {
SMTP(1), SMS(2), SNMP(3), CONSOLE(4),AUDIO_VISUAL(5), NT_LOG(6);

private int code;

private MessageType(final int code) {
	this.code = code;
}

public int code() {
	return code;
}

public static MessageType decode(String code) {
	try {
		int codeAsInt = Integer.parseInt(code);
		return decode(codeAsInt);
	} catch (NumberFormatException e) {
		for (MessageType t : values()) {
			if (t.name().equals(code)) {
				return t;
			}
		}
	}

	throw new IllegalArgumentException("Trying to decide invalid message type : " + code);
}

public static MessageType decode(int code) {
	switch (code) {
	case 1: {
		return SMTP;
	}
	case 2: {
		return SMS;
	}
	case 3: {
		return SNMP;
	}

	case 4: {
		return CONSOLE;
	}
	case 5:{
		return AUDIO_VISUAL;
	}
	case 6:{
		return NT_LOG;
	}
	default: {
		throw new IllegalArgumentException();
	}
	}
}
}
