package com.synapsense.dto;

public enum MessageTemplateType {
/*
 * e.g. "Console Notification". Console uses these templates by default when
 * user defines new alert. All default templates are always predefined.
 */
DEFAULT(0),
/*
 * Templates seeded into db. User can't delete default and predefined templates
 * ()
 */
PRE_DEFINED(1),
/*
 * Named templates defined by user in "Alerts->Notification Templates"
 */
USER_DEFINED(2),
/*
 *  Template specific to certain alert type 
 */
CUSTOM(3);

private int code;

private MessageTemplateType(final int code) {
	this.code = code;
}

public int code() {
	return code;
}
}
