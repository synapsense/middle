package com.synapsense.dto;

public enum AlertStatus {
OPENED(0, ""), ACKNOWLEDGED(1, "$localize(alert_comment_ack)$"), DISMISSED(2, "$localize(alert_comment_dis)$"), RESOLVED(3, "$localize(alert_comment_res)$");

private int statusCode;
private String messageDelimiter;

private AlertStatus(int num, String delim) {
	this.statusCode = num;
	this.messageDelimiter = delim;
}

public static boolean isActive(int status) {
	return status < DISMISSED.statusCode;
}

public static AlertStatus decode(int status) {
	for (AlertStatus s : AlertStatus.values()) {
		if (s.statusCode == status)
			return s;
	}
	throw new IllegalArgumentException("Not supported alert state, status=" + status);
}

public int code() {
	return statusCode;
}

public boolean isActive() {
	return isActive(this.statusCode);
}

public String compoundComment(String comment) {
	String result = null;

	if ((comment == null || comment.isEmpty())) {
		result = messageDelimiter;
	} else {
		String totalDelimiter = "; ".concat(messageDelimiter);
		result = comment.concat(totalDelimiter);
	}

	return result;
}

/**
 * Simplest transition table implementation
 * 
 * @param status To proceed to
 * @return transition is possible or not
 */
public boolean canProceedTo(AlertStatus status) {
	if (statusCode >= status.statusCode)
		return false;
	return true;
}
}
