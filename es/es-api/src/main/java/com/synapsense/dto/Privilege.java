package com.synapsense.dto;

import java.util.HashSet;
import java.util.Set;

import com.synapsense.exception.IllegalInputParameterException;

public class Privilege implements java.io.Serializable {

	// private static final long serialVersionUID = -8668862155452334833L;

	private String name;
	private String description;
	private Set<App> apps;
	private String tag;
	private Integer sortOrder;

	public Privilege(String name, String description, App app, String tag, Integer sortOrder) {
		this(name, description);
		if (app == null) {
			throw new IllegalInputParameterException("Supplied `app` is null");
		}
		Set<App> apps = new HashSet<App>();
		apps.add(app);
		this.apps = apps;
		this.tag = tag;
		this.sortOrder = sortOrder;
	}

	public Privilege(String name, String description, Set<App> apps, String tag, Integer sortOrder) {
		this(name, description);
		if (apps == null || apps.isEmpty()) {
			throw new IllegalInputParameterException("Supplied `apps` is null or empty");
		}
		this.apps = apps;
		this.tag = tag;
		this.sortOrder = sortOrder;
	}

	private Privilege(String name, String description) {
		if (name == null || name.isEmpty())
			throw new IllegalInputParameterException("name must be not null or empty");
		this.name = name;
		this.description = description != null ? description : "";
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description != null ? description : "";
	}

	public boolean isApp(App app) {
		return apps.contains(app);
	}

	public boolean isApps(Set<App> apps) {
		return apps.containsAll(apps);
	}

	public Set<App> getApps() {
		return apps;
	}

	public String getTag() {
    	return tag;
    }

	public void setTag(String tag) {
    	this.tag = tag;
    }

	public Integer getSortOrder() {
    	return sortOrder;
    }

	public void setSortOrder(Integer sortOrder) {
    	this.sortOrder = sortOrder;
    }

	@Override
	public int hashCode() {
		return name.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Privilege other = (Privilege) obj;
		if (apps == null) {
			if (other.apps != null)
				return false;
		} else if (!apps.equals(other.apps))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (sortOrder == null) {
			if (other.sortOrder != null)
				return false;
		} else if (!sortOrder.equals(other.sortOrder))
			return false;
		if (tag == null) {
			if (other.tag != null)
				return false;
		} else if (!tag.equals(other.tag))
			return false;
		return true;
	}
}
