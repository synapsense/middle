package com.synapsense.dto;

import java.io.Serializable;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import com.synapsense.util.CollectionUtils;

/**
 * Alert priority transfer object.
 * 
 * @author shabanov
 * 
 */
public class AlertPriority implements Serializable {

	private static final long serialVersionUID = -2709543378796028400L;

	private String name;

	private Long dismissInterval;

	private boolean redSignal;

	private boolean greenSignal;

	private boolean yellowSignal;

	private long durationLight;

	private boolean audioSignal;

	private long durationAudio;

	private boolean ntLog;

	/**
	 * set of escalation tiers
	 */
	private List<PriorityTier> tiers;

	/**
	 * Creates new alert priority instance. Requires all fields to be specified.
	 * 
	 * @param name
	 *            Priority name
	 * @param value
	 *            Integer value in range [1..100]
	 * @param tiers
	 *            Escalation tiers
	 */
	public AlertPriority(String name, Long dismissInterval, List<PriorityTier> tiers) {
		this.name = name;
		this.dismissInterval = dismissInterval;
		this.tiers = new LinkedList<PriorityTier>(tiers);
		Collections.sort(this.tiers);
	}

	public AlertPriority(String name, Long dismissInterval, List<PriorityTier> tiers, Boolean redSignal,
	        Boolean greenSignal, Boolean yellowSignal, Long durationLight, Boolean audioSignal, Long durationAudio,
	        Boolean ntLog) {
		this.name = name;
		this.dismissInterval = dismissInterval;
		this.tiers = new LinkedList<PriorityTier>(tiers);
		Collections.sort(this.tiers);
		this.redSignal = redSignal;
		this.greenSignal = greenSignal;
		this.yellowSignal = yellowSignal;
		this.durationLight = durationLight;
		this.audioSignal = audioSignal;
		this.durationAudio = durationAudio;
		this.ntLog = ntLog;

	}

	public String getName() {
		return name;
	}

	public Long getDismissInterval() {
		return dismissInterval;
	}

	public PriorityTier getTier(final String name) {
		return CollectionUtils.search(tiers, new CollectionUtils.Predicate<PriorityTier>() {
			@Override
			public boolean evaluate(PriorityTier t) {
				if (t.getName().equals(name)) {
					return true;
				}
				return false;
			}
		});
	}

	public List<PriorityTier> getTiers() {
		return Collections.unmodifiableList(tiers);
	}

	public boolean isRedSignal() {
		return redSignal;
	}

	public boolean isGreenSignal() {
		return greenSignal;
	}

	public boolean isYellowSignal() {
		return yellowSignal;
	}

	public boolean isAudioSignal() {
		return audioSignal;
	}

	public long getLightDuration() {
		return durationLight;
	}

	public long getAudioDuration() {
		return durationAudio;
	}

	public boolean isNtLog() {
		return ntLog;
	}

}
