package com.synapsense.dto;

/**
 * Same class as ValueTO but with previous property value included.
 * 
 * @author Oleg Stepanov
 * 
 */
public class ChangedValueTO extends ValueTO {

	private static final long serialVersionUID = 6529081387129907797L;

	private Object oldValue;

	public ChangedValueTO(ValueTO value, Object oldValue) { 
		this(value.getPropertyName(),value.getValue(), oldValue, value.getTimeStamp());
	}
	
	public ChangedValueTO(String propertyName, Object value) {
		super(propertyName, value);
		this.oldValue = null;
	}

	public ChangedValueTO(String propertyName, Object value, Object oldValue) {
		super(propertyName, value);
		this.oldValue = oldValue;
	}

	public ChangedValueTO(String propertyName, Object value, long timeStamp) {
		super(propertyName, value, timeStamp);
		this.oldValue = null;
	}

	public ChangedValueTO(String propertyName, Object value, Object oldValue, long timeStamp) {
		super(propertyName, value, timeStamp);
		this.oldValue = oldValue;
	}

	public void setOldValue(Object oldValue) {
		this.oldValue = oldValue;
	}

	public Object getOldValue() {
		return oldValue;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((oldValue == null) ? 0 : oldValue.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		ChangedValueTO other = (ChangedValueTO) obj;
		if (oldValue == null) {
			if (other.oldValue != null)
				return false;
		} else if (!oldValue.equals(other.oldValue))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Property[" + this.getPropertyName() + "="
		        + ((this.getValue() == null) ? "null" : this.getValue().toString()) + ", oldValue="
		        + ((this.getOldValue() == null) ? "null" : this.getOldValue().toString()) + "] timestamp["
		        + this.getTimeStamp() + "]";
	}

}
