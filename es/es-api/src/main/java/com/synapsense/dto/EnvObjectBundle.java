package com.synapsense.dto;

import java.io.Serializable;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

/**
 * Container for unique pairs : label , <code>EnvObject</code>
 * 
 * @author ashabanov
 * 
 */
public final class EnvObjectBundle implements Iterable<EnvObject>, Serializable {

	private static final long serialVersionUID = -4433110341928383816L;

	private Map<String, EnvObject> bundleMap = new LinkedHashMap<String, EnvObject>();

	/**
	 * Adds a new <code>EnvObject</code> into container marked by given
	 * <code>label</code>
	 * 
	 * @param label
	 *            Unique label
	 * @param envObject
	 */
	public void addObject(final String label, final EnvObject envObject) {
		if (label == null)
			throw new IllegalArgumentException("Supplied 'label' is null");
		if (envObject == null)
			throw new IllegalArgumentException("Supplied 'envObject' is null");

		if (bundleMap.get(label) != null)
			throw new IllegalArgumentException("Object with label:" + label + " already exists");

		bundleMap.put(label, envObject);
	}

	@Override
    public String toString() {
	    return "EnvObjectBundle [bundleMap=" + bundleMap + "]";
    }

	/**
	 * Returns <code>EnvObject</code> from container by label
	 * 
	 * @param label
	 * @return EnvObject instance or null
	 */
	public EnvObject get(final String label) {
		return bundleMap.get(label);
	}

	@Override
	public Iterator<EnvObject> iterator() {
		return Collections.unmodifiableCollection(bundleMap.values()).iterator();
	}

	public Set<String> getLabels() {
		return Collections.unmodifiableSet(bundleMap.keySet());
	}

	public int size() {
		return bundleMap.size();
	}

	public boolean isEmpty() {
		return size() == 0;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((bundleMap == null) ? 0 : bundleMap.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EnvObjectBundle other = (EnvObjectBundle) obj;
		if (bundleMap == null) {
			if (other.bundleMap != null)
				return false;
		} else if (!bundleMap.equals(other.bundleMap))
			return false;
		return true;
	}

}
