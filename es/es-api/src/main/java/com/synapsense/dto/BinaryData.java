package com.synapsense.dto;

/**
 * Interface of object contained binary data.
 * 
 * @author aalexeev
 */
public interface BinaryData {

	public byte[] getValue();

	/**
	 * Sets given byte array.
	 * 
	 * @param arr
	 *            (max arr.lenght=16*1024*1024-1)
	 * 
	 */
	public void setValue(byte[] arr);
}
