package com.synapsense.dto;

import java.io.Serializable;

/**
 * Descriptor of object type property tag. Unique identity of this object is
 * name.
 * 
 * @author aalexeev
 */
public class TagDescriptor implements Serializable {
	private static final long serialVersionUID = 2537357717072232219L;

	/**
	 * Property name
	 */
	private String name;

	/**
	 * Property type name (class name actually)
	 */
	private String typeName;

	/**
	 * Main all-fields constructor.
	 * <p>
	 * Allows to create full initialized instance of <code>PropertyDescr</code>
	 * class
	 * 
	 * @param name
	 *            Property name
	 * @param typeName
	 *            Type name of property (actually the class name)
	 */
	public TagDescriptor(String name, String typeName) {
		if (name == null || name.isEmpty()) {
			throw new IllegalArgumentException("Supplied 'name' is null or empty");
		}
		this.name = name;

		this.typeName = typeName;
	}

	/**
	 * Copy constructor.
	 * <p>
	 * Allows to create full initialized instance of <code>PropertyDescr</code>
	 * class based on the given object.
	 * 
	 * @param tagDescr
	 *            TagDescriptor to copy values from.
	 */
	public TagDescriptor(final TagDescriptor tagDescr) {
		if (tagDescr == null) {
			throw new IllegalArgumentException("Supplied 'tagDescr' is null");
		}
		this.name = tagDescr.getName();

		this.typeName = tagDescr.getTypeName();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((typeName == null) ? 0 : typeName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final TagDescriptor other = (TagDescriptor) obj;

		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;

		if (typeName == null) {
			if (other.typeName != null)
				return false;
		} else if (!typeName.equals(other.typeName))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Propety name:" + this.name + ";type:" + this.typeName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

}
