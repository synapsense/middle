package com.synapsense.dto;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * 
 * 
 * @author shabanov
 * 
 */
public class ObjectTypeMatchMode implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3660619098735041517L;

	private ObjectTypeMatchMode() {
	}

	public static final ObjectTypeMatchMode EXACT_MATCH = new ObjectTypeMatchMode();

	public static final ObjectTypeMatchMode DEFAULT = EXACT_MATCH;

	public static final ObjectTypeMatchMode START_WITH = new ObjectTypeMatchMode() {

		private static final long serialVersionUID = 4756529764544397747L;

		public Criterion createCriterion(ValueTO value) {
			Object nativeValue = value.getValue();
			String newLikeValue = (nativeValue == null) ? "%" : nativeValue + "%";
			ValueTO valueLike = new ValueTO(value.getPropertyName(), newLikeValue);
			return Criterion.like(valueLike);
		}

		/**
		 * Uses "starts with" algo to filter strings
		 */
		public Set<String> filterCollection(Collection<String> elements, String value) {
			String upperCaseValue = value.toUpperCase();
			Set<String> result = new HashSet<String>();
			for (String element : elements) {
				if (element.toUpperCase().startsWith(upperCaseValue)) {
					result.add(element);
				}
			}
			return result;
		}

	};

	public static final ObjectTypeMatchMode END_WITH = new ObjectTypeMatchMode() {

		private static final long serialVersionUID = -1660877414901166987L;

		public Criterion createCriterion(ValueTO value) {
			Object nativeValue = value.getValue();
			String newLikeValue = (nativeValue == null) ? "%" : "%" + nativeValue;
			ValueTO valueLike = new ValueTO(value.getPropertyName(), newLikeValue);
			return Criterion.like(valueLike);
		}

		/**
		 * Uses "ends with" algo to filter strings
		 */
		public Set<String> filterCollection(Collection<String> elements, String value) {
			String upperCaseValue = value.toUpperCase();
			Set<String> result = new HashSet<String>();
			for (String element : elements) {
				if (element.toUpperCase().endsWith(upperCaseValue)) {
					result.add(element);
				}
			}
			return result;
		}

	};

	public static final ObjectTypeMatchMode CONTAINS = new ObjectTypeMatchMode() {

		private static final long serialVersionUID = -902768084089194830L;

		public Criterion createCriterion(ValueTO value) {
			Object nativeValue = value.getValue();
			String newLikeValue = (nativeValue == null) ? "%" : "%" + nativeValue + "%";
			ValueTO valueLike = new ValueTO(value.getPropertyName(), newLikeValue);
			return Criterion.like(valueLike);
		}

		/**
		 * Uses "contains" algo to filter strings
		 */
		public Set<String> filterCollection(Collection<String> elements, String value) {
			String upperCaseValue = value.toUpperCase();
			Set<String> result = new HashSet<String>();
			for (String element : elements) {
				if (element.toUpperCase().contains(upperCaseValue)) {
					result.add(element);
				}
			}
			return result;
		}
	};

	public Criterion createCriterion(ValueTO value) {
		return Criterion.eq(value);
	}

	/**
	 * Filters input collection of strings. Finds elements in <tt>elements</tt>
	 * which are equal to the given <tt>value</tt> The method is case
	 * insensitive.
	 * 
	 * @param elements
	 *            Input collection to filter
	 * @param value
	 *            Filter value
	 * @return Collection of filtered strings
	 */
	public Set<String> filterCollection(Collection<String> elements, String value) {
		Set<String> result = new HashSet<String>();
		for (String element : elements) {
			if (element.equalsIgnoreCase(value)) {
				result.add(element);
				break;
			}
		}
		return result;
	}
}