package com.synapsense.dto;


import java.io.Serializable;
import java.util.Map;

public interface Message extends Serializable {
	Integer getAlertID();

	String getPriority();

	String getProtocol();

	String getHeader();

	String getContent();

	String getDestination();

    Map<String, Object> getMediaData();
}