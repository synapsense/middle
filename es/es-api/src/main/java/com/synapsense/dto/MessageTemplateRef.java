package com.synapsense.dto;

import java.io.Serializable;

import org.apache.log4j.Logger;

import com.synapsense.exception.EnvException;
import com.synapsense.service.Environment;

/**
 * Message template implementation.
 * 
 * Consists of message type, message header and message body. Used to
 * distinguish message templates by message type within {@link AlertType}.
 * 
 * @author shabanov
 * 
 */
public class MessageTemplateRef implements MessageTemplate, Serializable {
	private final static Logger logger = Logger.getLogger(MessageTemplateRef.class);
	
	private static final long serialVersionUID = 8034528428161274276L;

	private final MessageType messageType;
	private final TO<?> toRef;

	public MessageTemplateRef(final MessageType type, final TO<?> toRef) {
		messageType = type;
		this.toRef = toRef;
	}

	/**
	 * Message header getter
	 * 
	 * @return
	 */
	@Override
	public String getHeader(final Environment env) {
		try {
			return env.getPropertyValue(toRef, "header", String.class);
		} catch (final EnvException e) {
			logger.warn("Detected malformed object : " + toRef, e);
		}
		return null;
	}

	/**
	 * Message body getter
	 * 
	 * @return
	 */
	@Override
	public String getBody(final Environment env) {
		try {
			return env.getPropertyValue(toRef, "body", String.class);
		} catch (final EnvException e) {
			logger.warn("Detected malformed object : " + toRef, e);
		}
		return null;
	}

	/**
	 * Message type getter
	 * 
	 * @return
	 */
	@Override
	public MessageType getMessageType() {
		return messageType;
	}

	@Override
	public TO getRef() {
		return toRef;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((messageType == null) ? 0 : messageType.hashCode());
		result = prime * result + ((toRef == null) ? 0 : toRef.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final MessageTemplateRef other = (MessageTemplateRef) obj;
		if (messageType != other.messageType)
			return false;
		if (toRef == null) {
			if (other.toRef != null)
				return false;
		} else if (!toRef.equals(other.toRef))
			return false;
		return true;
	}

}
