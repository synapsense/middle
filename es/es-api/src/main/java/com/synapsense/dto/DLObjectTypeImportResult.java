package com.synapsense.dto;

import java.io.Serializable;

/**
 * Result of a type definition import. One of these objects should be returned
 * for each obejct type created.
 * 
 * @author Oleg Stepanov
 * @author Gabriel Helman
 * 
 * @see com.synapsense.service.DLImportService#importObjectTypes(String)
 */
public class DLObjectTypeImportResult implements Serializable {

	private static final long serialVersionUID = 6539035924409585590L;

	private final String typeName;
	private final Exception e;

	/**
	 * Creates successful type import result. Same as
	 * <code>DLImportResult(typeName, null)</code>.
	 * 
	 * @param typeName
	 *            name of successfully imported type.
	 */
	public DLObjectTypeImportResult(String typeName) {
		this(typeName, null);
	}

	/**
	 * Creates successful or failed type import result.
	 * 
	 * @param typeName
	 *            name of imported/failed to import type.
	 * @param e
	 *            Exception occurred during type definition import (
	 *            <code>null</code> for success).
	 */
	public DLObjectTypeImportResult(String typeName, Exception e) {
		if (typeName == null || typeName.isEmpty())
			throw new IllegalArgumentException("typeName argument cannot be null or empty string");
		this.typeName = typeName;
		this.e = e;
	}

	public String getTypeName() {
		return typeName;
	}

	public Exception getException() {
		return e;
	}

	public boolean isSuccessful() {
		return e == null;
	}

	@Override
	public String toString() {
		return "DLImportResult{" + "typeName='" + typeName + '\'' + ", e=" + e + '}';
	}
}
