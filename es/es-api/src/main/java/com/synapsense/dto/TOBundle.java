package com.synapsense.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Set;

/**
 * Container for unique pairs : label , <code>TO</code> <br>
 * 
 * @author ashabanov
 * 
 */
public class TOBundle implements Iterable<TO<?>>, Serializable {

	private static final long serialVersionUID = 7755199888448625444L;

	private LinkedHashMap<String, TO<?>> bundleMap = new LinkedHashMap<String, TO<?>>();

	public void add(String label, TO<?> identity) {
		checkDuplicate(label);

		bundleMap.put(label, identity);
	}

	public void addAll(TOBundle other) {
		bundleMap.putAll(other.bundleMap);
	}

	public TO<?> getFirst() {
		return bundleMap.values().iterator().next();
	}

	public TO<?> getLast() {
		ListIterator<TO<?>> reverse = new LinkedList<TO<?>>(bundleMap.values()).listIterator(bundleMap.values().size());
		return reverse.previous();
	}

	public TO<?> get(String label) {
		return bundleMap.get(label);
	}

	@Override
	public Iterator<TO<?>> iterator() {
		return Collections.unmodifiableCollection(bundleMap.values()).iterator();
	}

	public Object size() {
		return bundleMap.size();
	}

	public TO<?> get(int index) {
		List<TO<?>> list = new ArrayList<TO<?>>(bundleMap.values());
		if (index <= 0 || index > list.size()) {
			throw new IllegalArgumentException("Supplied 'index'= " + index + " is out of range");
		}
		return list.get(index - 1);
	}

	private void checkDuplicate(String label) {
		if (bundleMap.get(label) != null) {
			throw new IllegalArgumentException("Given label : " + label + " already exist in a bundle");
		}
	}

	public Set<String> keySet() {
		return bundleMap.keySet();

	}
}
