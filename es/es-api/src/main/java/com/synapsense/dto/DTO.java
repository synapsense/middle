package com.synapsense.dto;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Utility methods for the dto package.
 *
 * <p>We also have com.synapsense.impl.utils.EnvironmentUtils, but that's in es-core and modules like bacnet-gw only depend on es-api.</p>
 */
public final class DTO {

	private DTO() {
	}

	/** Converts an Iterable of ValueTO to a Map */
	public static Map<String, Object> toMap(Iterable<ValueTO> values) {
		Map<String, Object> map = new LinkedHashMap<>();
		for (ValueTO valueTO : values) {
			map.put(valueTO.getPropertyName(), valueTO.getValue());
		}
		return map;
	}

	/** Converts an array of ValueTO to a Map */
	public static Map<String, Object> toMap(ValueTO[] values) {
		Map<String, Object> map = new LinkedHashMap<>();
		for (ValueTO valueTO : values) {
			map.put(valueTO.getPropertyName(), valueTO.getValue());
		}
		return map;
	}
}
