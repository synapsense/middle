package com.synapsense.dto;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * 
 * @author ashabanov
 * 
 */
public class Relation implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2161708949756595341L;

	private TO<?> parent;
	private Set<TO<?>> children;

	/**
	 * Single parent-child relation
	 * 
	 * @param parent
	 * @param child
	 */
	public Relation(TO<?> parent, TO<?> child) {
		this(parent, Arrays.<TO<?>>asList(child));
	}

	/**
	 * Multiple parent-child relations bundle
	 * 
	 * @param parent
	 * @param children
	 */
	public Relation(TO<?> parent, Collection<TO<?>> children) {
		if (parent == null)
			throw new IllegalArgumentException("Supplied 'parent' is null");
		if (children == null)
			throw new IllegalArgumentException("Supplied 'children' is null");
		if (children.isEmpty())
			throw new IllegalArgumentException("Supplied 'children' is enmpty");
		for (TO<?> to : children) {
			if (to == null)
				throw new IllegalArgumentException("Supplied 'children' contains null references");
		}
		this.parent = parent;
		this.children = new HashSet<TO<?>>(children);
	}

	public TO<?> getParent() {
		return parent;
	}

	public Set<TO<?>> getChildren() {
		return Collections.unmodifiableSet(children);
	}

	@Override
	public String toString() {
		return "Relation [parent=" + parent + ", children=" + children + "]";
	}
}
