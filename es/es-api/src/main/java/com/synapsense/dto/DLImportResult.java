package com.synapsense.dto;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * The result of importing an object instance (either creating or updating.) One
 * of these should be returned for each object imported.
 * 
 * @author Gabriel Helman
 * @author Oleg Stepanov
 * @author Alex Pakhunov
 * 
 * @see com.synapsense.service.DLImportService#importObjectInstances(String)
 * @see DLImportError
 * 
 */
public class DLImportResult implements Serializable {
	private static final long serialVersionUID = 4297506663590445120L;

	/**
	 * List of errors that happened while this object was being imported. By
	 * definition, any errors means that the import was not successful.
	 */
	private List<DLImportError> errors = new LinkedList<DLImportError>();

	/**
	 * The DeploymentLab unique id for this object.
	 */
	private String dlid;

	/**
	 * The TO for this object. If this field is filled in, that means the object
	 * was created (or already existed) on the server, regardless of any other
	 * errors.
	 */
	private TO<?> key;

	/**
	 * True if custom tags on the object were all successfully updated in the
	 * database. An object with no tags will have this set to true.
	 */
	private boolean tagsSuccessful;

	/**
	 * True if all parent-child relationships involving this object were
	 * successfully updated in the database. An object with no relationships
	 * will have this set to true.
	 */
	private boolean relationsSuccessful;

	/**
	 * True if both all dockingpoints and all collections involving this object
	 * were successfully updated in the database. An object with no links at all
	 * will have this set to true.
	 */
	private boolean linksSuccessful;
	// split dockingpoints & collections?

	/**
	 * True if all rules with this object as the destination were successfully
	 * updated in the database. An object with no rules will have this set to
	 * true.
	 */
	private boolean rulesSuccessful;

	/**
	 * True if this object was successfully deleted. (Presumably, by request.)
	 */
	private boolean deleted;

	public List<DLImportError> getErrors() {
		return errors;
	}

	public void addError(DLImportError _e) {
		errors.add(_e);
	}

	public DLImportResult() {
		this.dlid = null;
		this.key = null;
		relationsSuccessful = true;
		linksSuccessful = true;
		rulesSuccessful = true;
		deleted = false;

	}

	public DLImportResult(String dlid, TO<?> key) {
		this.dlid = dlid;
		this.key = key;
		relationsSuccessful = true;
		linksSuccessful = true;
		rulesSuccessful = true;
		deleted = false;
	}

	public boolean isSuccessful() {
		return (this.errors.size() == 0);
	}

	public String getDlid() {
		return dlid;
	}

	public void setDlid(String dlid) {
		this.dlid = dlid;
	}

	public TO<?> getKey() {
		return key;
	}

	public void setKey(TO<?> key) {
		this.key = key;
	}

	public boolean isTagsSuccessful() {
		return tagsSuccessful;
	}

	public void setTagsSuccessful(boolean tagsSuccessful) {
		this.tagsSuccessful = tagsSuccessful;
	}

	public boolean isRelationsSuccessful() {
		return relationsSuccessful;
	}

	public void setRelationsSuccessful(boolean relationsSuccessful) {
		this.relationsSuccessful = relationsSuccessful;
	}

	public boolean isLinksSuccessful() {
		return linksSuccessful;
	}

	public void setLinksSuccessful(boolean linksSuccessful) {
		this.linksSuccessful = linksSuccessful;
	}

	public boolean isRulesSuccessful() {
		return rulesSuccessful;
	}

	public void setRulesSuccessful(boolean rulesSuccessful) {
		this.rulesSuccessful = rulesSuccessful;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	@Override
	public String toString() {
		return "DLImportResult [errors=" + errors + ", dlid=" + dlid + ", key=" + key + ", tagsSuccessful="
		        + tagsSuccessful + ", relationsSuccessful=" + relationsSuccessful + ", linksSuccessful="
		        + linksSuccessful + ", rulesSuccessful=" + rulesSuccessful + ", deleted=" + deleted + "]";
	}

}
