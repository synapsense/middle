package com.synapsense.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * Alert DTO. As a fact , corresponds an alert instance descriptor. Used as a
 * transfer object in <code>AlertingService</code>. To create Alert instance is
 * necessary to instantiate <code>Alert</code> object and get it into
 * <code>AlertingService</code> as one of the input parameters or raise
 * procedure. The uniqueness of alert is defined by composite of properties:
 * alertType hostTO message ruleTO status This means that set of these
 * properties must have unique set of their values.
 * 
 * @author shabanov
 */
public class Alert implements Serializable {
	private static final long serialVersionUID = -3918414400850154112L;

	/**
	 * Alert id
	 */
	private Integer alertId;

	/**
	 * Alert name Must be unique within the border of <code>alertType</code>
	 */
	private String name;

	/**
	 * Type of alert
	 */
	private String alertType;

	/**
	 * Alert generating time
	 */
	private Long alertTime;

	/**
	 * Alert current status
	 */
	private AlertStatus status;

	/**
	 * Alert message.
	 * <p>
	 * Will be placed in a final alert message as add-on of alertType message
	 * setting
	 */
	private String message;

	/**
	 * ID of object that causes by a reason of alert
	 */
	private TO<?> hostTO;

	/**
	 * Acknowledgement text for dismissed alert
	 */
	private String acknowledgement;

	/**
	 * Alert acknowledgement time
	 */
	private Date acknowledgementTime;

	/**
	 * User who dismissed alert
	 */
	private String user;

	/**
	 * how many times alert was tried to be raised before dismiss
	 * 
	 */
	private Integer raiseNum;

	private String fullMessage;
	
	private String typeDescr;
	
	private String priority;

	/**
	 * Constructor with predefined PK
	 * 
	 * @param id
	 *            new PK
	 */
	public Alert() {
		setAlertId(null);
		setName("");
		setAlertType("");
		setAlertTime(0L);
		setMessage("");
		setHostTO(TOFactory.EMPTY_TO);
		setStatus(AlertStatus.OPENED);
	}

	/**
	 * Copy constructor
	 * 
	 * @param alert
	 *            Alert to copy
	 */
	public Alert(Alert alert) {
		if (alert == null) {
			throw new IllegalArgumentException("Supplied 'alert' is null");
		}
		setAlertId(alert.getAlertId());
		setName(alert.getName());
		setAlertType(alert.getAlertType());
		setAlertTime(alert.getAlertTime());
		setStatus(alert.getStatus());
		setMessage(alert.getMessage());
		setHostTO(alert.getHostTO());
		setAcknowledgement(alert.getAcknowledgement());
		setAcknowledgementTime(alert.getAcknowledgementTime());
		setUser(alert.getUser());
	}

	/**
	 * Constructor for alert creation without information about host object and
	 * rule.
	 * 
	 * @param name
	 * @param alertType
	 * @param alertTime
	 * @param message
	 */
	public Alert(String name, String alertType, Date alertTime, String message) {
		this(null, name, alertType, alertTime, message);
	}

	/**
	 * Constructor for alert creation without information about host object and
	 * rule. Plus the pk parameter for manually setting of pk.
	 * 
	 * @see #Alert(String, String, Date, String)
	 * @param alertId
	 * @param name
	 * @param alertType
	 * @param alertTime
	 * @param message
	 */
	public Alert(Integer alertId, String name, String alertType, Date alertTime, String message) {
		setAlertId(alertId);

		if (name == null || name.isEmpty()) {
			throw new IllegalArgumentException("Supplied 'name' is null or empty");
		}
		setName(name);

		if (alertType == null || alertType.isEmpty()) {
			throw new IllegalArgumentException("Supplied 'alertType' is null or empty");
		}
		setAlertType(alertType);

		if (message == null || message.isEmpty()) {
			throw new IllegalArgumentException("Supplied 'message' is null or empty");
		}
		setMessage(message);

		if (alertTime == null) {
			throw new IllegalArgumentException("Supplied 'alertTime' is null");
		}

		setAlertTime(alertTime.getTime());

		setStatus(AlertStatus.OPENED);

		setHostTO(TOFactory.EMPTY_TO);
	}

	/**
	 * Constructor for alert creation with link to rule which creates it and
	 * with host object link which is owner of alert.
	 * 
	 * @param name
	 * @param alertType
	 * @param alertTime
	 * @param message
	 * @param ruleTO
	 * @param hostTO
	 */
	public Alert(String name, String alertType, Date alertTime, String message, TO<?> hostTO) {
		this(null, name, alertType, alertTime, message, hostTO);
	}

	/**
	 * Constructor for alert creation with link to rule which creates it and
	 * with host object link which is owner of alert. Plus the pk parameter for
	 * manually setting of pk.
	 * 
	 * @param alertId
	 * @param name
	 * @param alertType
	 * @param alertTime
	 * @param message
	 * @param ruleTO
	 * @param hostTO
	 */
	public Alert(Integer alertId, String name, String alertType, Date alertTime, String message, TO<?> hostTO) {
		this(alertId, name, alertType, alertTime, message);
		setHostTO(hostTO);
	}

	/**
	 * Alert name getter
	 * 
	 * @return
	 */
	public String getName() {
		return name;
	}

	/**
	 * Alert type getter
	 * 
	 * @return
	 */
	public String getAlertType() {
		return alertType;
	}

	/**
	 * Alert time getter
	 * 
	 * @return
	 */
	public Long getAlertTime() {
		return alertTime;
	}

	/**
	 * Alert status getter
	 * 
	 * @return
	 */
	public AlertStatus getStatus() {
		return status;
	}

	/**
	 * Alert origin message getter
	 * 
	 * @return
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * Alert host object getter
	 * 
	 * @return
	 */
	public TO<?> getHostTO() {
		return hostTO;
	}

	/**
	 * Alert name setter
	 * 
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Alert type setter
	 * 
	 * @param alertType
	 */
	public void setAlertType(String alertType) {
		this.alertType = alertType;
	}

	/**
	 * Alert time setter
	 * 
	 * @param alertTime
	 */
	public void setAlertTime(Long alertTime) {
		this.alertTime = alertTime;
	}

	/**
	 * Alert status setter
	 * 
	 * @param status
	 */
	public void setStatus(AlertStatus status) {
		this.status = status;
	}

	/**
	 * Alert message setter
	 * 
	 * @param message
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * Alert host object setter
	 * 
	 * @param hostTO
	 */
	public void setHostTO(TO<?> hostTO) {
		if (hostTO == null) {
			this.hostTO = TOFactory.EMPTY_TO;
		} else {
			this.hostTO = hostTO;
		}
	}

	/**
	 * Alert PK getter
	 * 
	 * @return
	 */
	public Integer getAlertId() {
		return alertId;
	}

	/**
	 * Alert PK setter
	 * 
	 * @param alertId
	 */
	public void setAlertId(Integer alertId) {
		this.alertId = alertId;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder(200);
		sb.append("[");
		sb.append("type:" + this.alertType);
		sb.append(";");
		sb.append("name:" + this.name);
		sb.append(";");
		sb.append("time:" + new Date(this.alertTime));
		sb.append(";");
		sb.append("message:" + this.message);
		sb.append(";");
		sb.append("host:" + this.hostTO);
		sb.append("]");
		return sb.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((alertType == null) ? 0 : alertType.hashCode());
		// bug#1541
		// if host TO is EMPTY , message is used in hashCode
		if (TOFactory.EMPTY_TO.equals(hostTO)) {
			result = prime * result + ((message == null) ? 0 : message.hashCode());
		} else {
			// else , the hostTO is used in hashCode
			result = prime * result + hostTO.hashCode();
		}
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		return result;
	}

	/**
	 * Don't forget to change {@link #getKeyPropertiesNames()} when changing it.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final Alert other = (Alert) obj;
		if (alertType == null) {
			if (other.alertType != null)
				return false;
		} else if (!alertType.equals(other.alertType))
			return false;
		// bug#1541
		// checks if alert is system then checks message for equality
		if (TOFactory.EMPTY_TO.equals(hostTO)) {
			if (!TOFactory.EMPTY_TO.equals(other.hostTO))
				return false;
			if (message == null) {
				if (other.message != null)
					return false;
			} else if (!message.equals(other.message))
				return false;
			// if alert in on object then message is not necessary for checking
			// equality
		} else if (!hostTO.equals(other.hostTO))
			return false;

		if (status == null) {
			if (other.status != null)
				return false;
		} else if (!status.equals(other.status))
			return false;
		return true;
	}

	/**
	 * Alert acknowledegment getter
	 * 
	 * @return
	 */
	public String getAcknowledgement() {
		return acknowledgement;
	}

	/**
	 * Alert acknowledegment setter
	 * 
	 * @param acknowledgement
	 */
	public void setAcknowledgement(String acknowledgement) {
		this.acknowledgement = acknowledgement;
	}

	/**
	 * Alert acknowledegment time getter
	 * 
	 * @return
	 */
	public Date getAcknowledgementTime() {
		return acknowledgementTime;
	}

	/**
	 * Alert acknowledegment time setter
	 * 
	 * @param acknowledgementTime
	 */
	public void setAcknowledgementTime(Date acknowledgementTime) {
		this.acknowledgementTime = acknowledgementTime;
	}

	/**
	 * User getter
	 * 
	 * @return
	 */
	public String getUser() {
		return user;
	}

	/**
	 * User setter
	 * 
	 * @param user
	 */
	public void setUser(String user) {
		this.user = user;
	}

	/**
	 * Returns all key properties - properties which are determine the equality
	 * of objects of this class.
	 * 
	 * Needs to be modified when overriding <code>equals</code> or
	 * <code>hashCode</code>.
	 * 
	 * @return Array of key properties
	 */
	public static String[] getKeyPropertiesNames() {
		return new String[] { "hostTO", "message", "status", "alertType" };
	}

	public void setRaiseNum(Integer raiseNum) {
		this.raiseNum = raiseNum;
	}

	public Integer getRaiseNum() {
		return raiseNum;
	}

	public String getFullMessage() {
		return fullMessage;
	}

	public void setFullMessage(String fullMessage) {
		this.fullMessage = fullMessage;
	}

	public String getTypeDescr() {
	    return typeDescr;
    }

	public void setTypeDescr(String typeDescr) {
	    this.typeDescr = typeDescr;
    }

	public String getPriority() {
	    return priority;
    }

	public void setPriority(String priority) {
	    this.priority = priority;
    }

}
