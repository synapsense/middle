package com.synapsense.dto;

import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;

import com.synapsense.util.CollectionUtils;

public final class HistoricalRecord implements Comparable<HistoricalRecord>, Serializable {
	
    private static final long serialVersionUID = -6116519160568172281L;
    
	private Map<String, ValueTO> tuple = CollectionUtils.newLinkedMap();
	private long timestamp;
	private int group;
	
	public HistoricalRecord(ValueTO[] tuple, long timestamp, int group) {
		if (tuple == null || tuple.length == 0)
			throw new IllegalArgumentException("Supplied 'tuple' is null");
		for (ValueTO v : tuple) {
			this.tuple.put(v.getPropertyName(), v);
		}
		this.timestamp = timestamp;
		this.group = group;
	}

	public int getFieldsNum() {
		return this.tuple.size();
	}
	
	public Collection<String> getFieldsNames() {
		return Collections.unmodifiableCollection(tuple.keySet()); 
	}

	public Collection<ValueTO> getValues() { 
		return Collections.unmodifiableCollection(tuple.values());
	}
	
	public <T> T getTypedValue(final String fieldName, Class<T> castTo) {
		ValueTO value = this.tuple.get(fieldName);
		if (value == null)
			return null;

		return castTo.cast(value.getValue());
	}

	public Object getValue(final String fieldName) {
		return getTypedValue(fieldName, Object.class);
	}

	@Override
	public int compareTo(HistoricalRecord o) {
		if (this.timestamp < o.timestamp)
			return -1;
		if (this.timestamp > o.timestamp)
			return 1;
		// if timestamps are equal try to compare group ids 
		if (this.group < o.group) {
			return -1;
		}
		if (this.group > o.group) {
			return 1;
		}
		return 0;
	}

	@Override
    public int hashCode() {
	    final int prime = 31;
	    int result = 1;
	    result = prime * result + group;
	    result = prime * result + (int) (timestamp ^ (timestamp >>> 32));
	    result = prime * result + ((tuple == null) ? 0 : tuple.hashCode());
	    return result;
    }

	@Override
    public boolean equals(Object obj) {
	    if (this == obj)
		    return true;
	    if (obj == null)
		    return false;
	    if (getClass() != obj.getClass())
		    return false;
	    HistoricalRecord other = (HistoricalRecord) obj;
	    if (group != other.group)
		    return false;
	    if (timestamp != other.timestamp)
		    return false;
	    if (tuple == null) {
		    if (other.tuple != null)
			    return false;
	    } else if (!tuple.equals(other.tuple))
		    return false;
	    return true;
    }

}
