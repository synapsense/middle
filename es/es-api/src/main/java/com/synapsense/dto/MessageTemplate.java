package com.synapsense.dto;

import com.synapsense.service.Environment;

/**
 * Message template transfer object.
 * 
 * @author shabanov
 * 
 */
public interface MessageTemplate {
	String getHeader(Environment env);

	String getBody(Environment env);

	MessageType getMessageType();

	TO getRef();
}
