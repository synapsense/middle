package com.synapsense.dto;

import java.util.Set;

/**
 * Interface of environment object type.
 * 
 * @author Oleg Stepanov
 * 
 */
public interface ObjectType {

	/**
	 * Get type name.
	 * 
	 * @return type name
	 */
	String getName();

	/**
	 * Change type name.
	 * 
	 * @param name
	 *            new type name
	 */
	void setName(String name);

	/**
	 * Get property descriptors for this type.
	 * 
	 * @return set of type's property descriptors
	 */
	Set<PropertyDescr> getPropertyDescriptors();

	/**
	 * Returns <code>PropertyDescr</code> by given name or <code>null</code>
	 * 
	 * @param propertyName
	 *            name of the property
	 * 
	 * @return <code>PropertyDescr</code> instance if property exists or
	 *         <code>null</code>
	 */
	PropertyDescr getPropertyDescriptor(String propertyName);

}
