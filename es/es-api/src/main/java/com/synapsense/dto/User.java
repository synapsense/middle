package com.synapsense.dto;

import java.io.Serializable;

import com.synapsense.exception.IllegalInputParameterException;

/**
 * User descriptor. Used as a transfer object(DTO); Holds all information
 * regarding to user.
 * 
 * @author shabanov
 * 
 */
public class User implements Serializable {

	private static final long serialVersionUID = 7270338351145608526L;

	private static final String NULL_INPUT_PARAMETER_MESSAGE = "Supplied argument {%s} cannot be null or empty";
	/**
	 * User id - PK
	 */
	private Integer userId;
	/**
	 * Name
	 */
	private String userName;
	/**
	 * Password hint
	 */
	private String userPasswordHint;
	/**
	 * Password hint answer
	 */
	private String userPasswordHintAnswer;
	/**
	 * First name
	 */
	private String FName;
	/**
	 * Last name
	 */
	private String LName;
	/**
	 * Middle name
	 */
	private String middleInitial;
	/**
	 * Title of user
	 */
	private String title;
	/**
	 * Main user' phone number
	 */
	private String primaryPhonenumber;
	/**
	 * Secondary user's phone number
	 */
	private String secondaryPhonenumber;

	/**
	 * Main user's mail
	 */
	private String primaryEmail;
	/**
	 * Secondary user's mail
	 */
	private String secondaryEmail;
	/**
	 * SMS address
	 */
	private String smsAddress;
	/**
	 * User's description in a free form
	 */
	private String description;

	/**
	 * Simplified constructor with all mandatory fields
	 *
	 * @param userName
	 * @param roleName
	 * @param userPasswordHint
	 * @param userPasswordHintAnswer
	 * @param name
	 * @param name2
	 */
	public User(String userName, String userPasswordHint, String userPasswordHintAnswer, String name, String name2) {
		this(userName, userPasswordHint, userPasswordHintAnswer, name, name2, null, null, null, null, null, null, null,
		        null);
	}

	/**
	 * All fields constructor
	 *
	 * @param userName
	 * @param roleName
	 * @param userPasswordHint
	 * @param userPasswordHintAnswer
	 * @param name
	 * @param name2
	 * @param middleInitial
	 * @param title
	 * @param primaryPhonenumber
	 * @param secondaryPhonenumber
	 * @param primaryEmail
	 * @param secondaryEmail
	 * @param smsAddress
	 * @param description
	 */
	public User(String userName, String userPasswordHint, String userPasswordHintAnswer, String name, String name2,
	        String middleInitial, String title, String primaryPhonenumber, String secondaryPhonenumber,
	        String primaryEmail, String secondaryEmail, String smsAddress, String description) {
		if (userName == null || userName.isEmpty()) {
			throw new IllegalInputParameterException(String.format(NULL_INPUT_PARAMETER_MESSAGE, "userName"));
		}
		if (name == null || name.isEmpty()) {
			throw new IllegalInputParameterException(String.format(NULL_INPUT_PARAMETER_MESSAGE, "FName"));
		}
		if (name2 == null || name2.isEmpty()) {
			throw new IllegalInputParameterException(String.format(NULL_INPUT_PARAMETER_MESSAGE, "LName"));
		}

		this.userName = userName;
		this.userPasswordHint = userPasswordHint;
		this.userPasswordHintAnswer = userPasswordHintAnswer;
		this.FName = name;
		this.LName = name2;
		this.middleInitial = middleInitial;
		this.title = title;
		this.primaryPhonenumber = primaryPhonenumber;
		this.secondaryPhonenumber = secondaryPhonenumber;
		this.primaryEmail = primaryEmail;
		this.secondaryEmail = secondaryEmail;
		this.smsAddress = smsAddress;
		this.description = description;
	}

	/**
	 * User PK getter
	 */
	public Integer getUserId() {
		return userId;
	}

	/**
	 * User PK setter
	 *
	 * @param userId
	 */
	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	/**
	 * User name getter
	 *
	 * @return
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * User name setter
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * User password hint getter
	 *
	 * @return
	 */
	public String getUserPasswordHint() {
		return userPasswordHint;
	}

	/**
	 * User password hint setter
	 */
	public void setUserPasswordHint(String userPasswordHint) {
		this.userPasswordHint = userPasswordHint;
	}

	/**
	 * User password hint answer getter
	 *
	 * @return
	 */
	public String getUserPasswordHintAnswer() {
		return userPasswordHintAnswer;
	}

	/**
	 * User password hint answer setter
	 */
	public void setUserPasswordHintAnswer(String userPasswordHintAnswer) {
		this.userPasswordHintAnswer = userPasswordHintAnswer;
	}

	/**
	 * User first name getter
	 *
	 * @return
	 */
	public String getFName() {
		return FName;
	}

	/**
	 * User first name setter
	 */
	public void setFName(String name) {
		FName = name;
	}

	/**
	 * User last name getter
	 *
	 * @return
	 */
	public String getLName() {
		return LName;
	}

	/**
	 * User last name setter
	 */
	public void setLName(String name) {
		LName = name;
	}

	/**
	 * User middle name getter
	 *
	 * @return
	 */
	public String getMiddleInitial() {
		return middleInitial;
	}

	/**
	 * User middle name setter
	 */
	public void setMiddleInitial(String middleInitial) {
		this.middleInitial = middleInitial;
	}

	/**
	 * User title getter
	 *
	 * @return
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * User title setter
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * User primary phone number getter
	 *
	 * @return
	 */
	public String getPrimaryPhonenumber() {
		return primaryPhonenumber;
	}

	/**
	 * User primary phone number setter
	 */
	public void setPrimaryPhonenumber(String primaryPhonenumber) {
		this.primaryPhonenumber = primaryPhonenumber;
	}

	/**
	 * User secondary phone number getter
	 *
	 * @return
	 */
	public String getSecondaryPhonenumber() {
		return secondaryPhonenumber;
	}

	/**
	 * User secondary phone number setter
	 */
	public void setSecondaryPhonenumber(String secondaryPhonenumber) {
		this.secondaryPhonenumber = secondaryPhonenumber;
	}

	/**
	 * User primary email getter
	 *
	 * @return
	 */
	public String getPrimaryEmail() {
		return primaryEmail;
	}

	/**
	 * User primary email setter
	 */
	public void setPrimaryEmail(String primaryEmail) {
		this.primaryEmail = primaryEmail;
	}

	/**
	 * User secondary email getter
	 *
	 * @return
	 */
	public String getSecondaryEmail() {
		return secondaryEmail;
	}

	/**
	 * User secondary email setter
	 */
	public void setSecondaryEmail(String secondaryEmail) {
		this.secondaryEmail = secondaryEmail;
	}

	/**
	 * User sms address getter
	 *
	 * @return
	 */
	public String getSmsAddress() {
		return smsAddress;
	}

	/**
	 * User sms address setter
	 */
	public void setSmsAddress(String smsAddress) {
		this.smsAddress = smsAddress;
	}

	/**
	 * User description getter
	 *
	 * @return
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * User description setter
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder(200);
		sb.append("userName=");
		sb.append(userName);
		sb.append('\n');
		sb.append("userPasswordHint=");
		sb.append(userPasswordHint);
		sb.append('\n');
		sb.append("userPasswordHintAnswer=");
		sb.append(userPasswordHintAnswer);
		sb.append('\n');
		sb.append("FName=");
		sb.append(FName);
		sb.append('\n');
		sb.append("LName=");
		sb.append(LName);
		sb.append('\n');
		sb.append("middleInitial=");
		sb.append(middleInitial);
		sb.append('\n');
		sb.append("title=");
		sb.append(title);
		sb.append('\n');
		sb.append("primaryPhonenumber=");
		sb.append(primaryPhonenumber);
		sb.append('\n');
		sb.append("secondaryPhonenumber=");
		sb.append(secondaryPhonenumber);
		sb.append('\n');
		sb.append("primaryEmail=");
		sb.append(primaryEmail);
		sb.append('\n');
		sb.append("secondaryEmail=");
		sb.append(secondaryEmail);
		sb.append('\n');
		sb.append("smsAddress=");
		sb.append(smsAddress);
		sb.append('\n');
		sb.append("description=");
		sb.append(description);
		sb.append('\n');
		return sb.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((FName == null) ? 0 : FName.hashCode());
		result = prime * result + ((LName == null) ? 0 : LName.hashCode());
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((middleInitial == null) ? 0 : middleInitial.hashCode());
		result = prime * result + ((primaryEmail == null) ? 0 : primaryEmail.hashCode());
		result = prime * result + ((primaryPhonenumber == null) ? 0 : primaryPhonenumber.hashCode());
		result = prime * result + ((secondaryEmail == null) ? 0 : secondaryEmail.hashCode());
		result = prime * result + ((secondaryPhonenumber == null) ? 0 : secondaryPhonenumber.hashCode());
		result = prime * result + ((smsAddress == null) ? 0 : smsAddress.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		result = prime * result + ((userId == null) ? 0 : userId.hashCode());
		result = prime * result + ((userName == null) ? 0 : userName.hashCode());
		result = prime * result + ((userPasswordHint == null) ? 0 : userPasswordHint.hashCode());
		result = prime * result + ((userPasswordHintAnswer == null) ? 0 : userPasswordHintAnswer.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final User other = (User) obj;
		if (FName == null) {
			if (other.FName != null)
				return false;
		} else if (!FName.equals(other.FName))
			return false;
		if (LName == null) {
			if (other.LName != null)
				return false;
		} else if (!LName.equals(other.LName))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (middleInitial == null) {
			if (other.middleInitial != null)
				return false;
		} else if (!middleInitial.equals(other.middleInitial))
			return false;
		if (primaryEmail == null) {
			if (other.primaryEmail != null)
				return false;
		} else if (!primaryEmail.equals(other.primaryEmail))
			return false;
		if (primaryPhonenumber == null) {
			if (other.primaryPhonenumber != null)
				return false;
		} else if (!primaryPhonenumber.equals(other.primaryPhonenumber))
			return false;
		if (secondaryEmail == null) {
			if (other.secondaryEmail != null)
				return false;
		} else if (!secondaryEmail.equals(other.secondaryEmail))
			return false;
		if (secondaryPhonenumber == null) {
			if (other.secondaryPhonenumber != null)
				return false;
		} else if (!secondaryPhonenumber.equals(other.secondaryPhonenumber))
			return false;
		if (smsAddress == null) {
			if (other.smsAddress != null)
				return false;
		} else if (!smsAddress.equals(other.smsAddress))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		if (userName == null) {
			if (other.userName != null)
				return false;
		} else if (!userName.equals(other.userName))
			return false;
		if (userPasswordHint == null) {
			if (other.userPasswordHint != null)
				return false;
		} else if (!userPasswordHint.equals(other.userPasswordHint))
			return false;
		if (userPasswordHintAnswer == null) {
			if (other.userPasswordHintAnswer != null)
				return false;
		} else if (!userPasswordHintAnswer.equals(other.userPasswordHintAnswer))
			return false;
		return true;
	}

}
