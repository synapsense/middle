package com.synapsense.dto;

/**
 * Global types enumeration. Defines all predetermined object types
 * 
 * @author shabanov
 * 
 */
public enum Types {
/* SENSOR, NODE, NETWORK, ROUTE,*/ ALERT, STAT, OTHER;

static public Types decode(final String name) {
	try {
		return Types.valueOf(name.toUpperCase());
	} catch (Exception e) {
		return OTHER;
	}
}

}
