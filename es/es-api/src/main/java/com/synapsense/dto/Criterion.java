package com.synapsense.dto;

import java.io.Serializable;

/**
 * Unified criterion for <code>ValueTO</code> instance. Provides factory methods
 * to predefined criteria. Used only for DAL implementation's goals yet.
 * 
 * @author shabanov
 * 
 */
public class Criterion implements Serializable {

	private static final long serialVersionUID = -6812537380807193796L;

	/**
	 * Property value
	 */
	private ValueTO propertyValue;

	/**
	 * Operation that will be applied to <code>propertyValue</code> field.
	 */
	private String operation;

	/**
	 * Equals criterion
	 * 
	 * @param propertyValue
	 * @return
	 */
	public static Criterion eq(ValueTO propertyValue) {
		return new Criterion(" = ", propertyValue);
	}

	/**
	 * Greater than criterion
	 * 
	 * @param propertyValue
	 * @return
	 */
	public static Criterion gth(ValueTO propertyValue) {
		if (propertyValue.getValue() == null)
			throw new IllegalArgumentException("Greater Than : Such criterion does not supprot nulls");
		return new Criterion(" > ", propertyValue);
	}

	/**
	 * Like criterion
	 * 
	 * @param propertyValue
	 * @return
	 */
	public static Criterion like(ValueTO propertyValue) {
		if (propertyValue.getValue() == null)
			throw new IllegalArgumentException("LIKE : Such criterion does not supprot nulls");
		return new Criterion(" like ", propertyValue);
	}

	/**
	 * Is null criterion
	 * 
	 * @param propertyValue
	 * @return
	 */
	public static Criterion isNull(ValueTO propertyValue) {
		return new Criterion(" is null ", propertyValue);
	}

	/**
	 * 
	 * @param operation
	 * @param propertyValue
	 */
	private Criterion(String operation, ValueTO propertyValue) {
		this.operation = operation;
		this.propertyValue = propertyValue;
	}

	/**
	 * Criterion's operation string getter
	 * 
	 * @return
	 */
	public String getOperation() {
		return operation;
	}

	/**
	 * Criterion's property value getter
	 * 
	 * @return
	 */
	public ValueTO getPropertyValue() {
		return propertyValue;
	}

}
