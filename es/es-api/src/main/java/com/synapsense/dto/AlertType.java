package com.synapsense.dto;

import java.io.Serializable;
import java.util.Collections;
import java.util.Set;

import com.synapsense.util.CollectionUtils;

/**
 * Alert type descriptor. Used as a transfer object(DTO);
 * 
 * @author shabanov
 */
public class AlertType implements Serializable {
	private static final long serialVersionUID = 3302001913240167654L;

	static final String DEFAULT_TEMPLATE_HEADER = "";

	static final String DEFAULT_TEMPLATE_BODY = "";

	/**
	 * An unique alert type name.
	 */
	private String name;

	/**
	 * An displayable alert type name.
	 */
	private String displayName;

	/**
	 * Alert type description. Short information about what alert type is.
	 */
	private String description;

	/**
	 * Alert type message header, used by default if no message template is
	 * specified
	 */
	private String defaultHeader;

	/**
	 * Alert type message body, used by default if no message template is
	 * specified
	 */
	private String defaultBody;

	/**
	 * Alert type priority.
	 */
	private String priority;

	/**
	 * Alert type activity flag
	 */
	private Boolean isActive;

	/**
	 * Alert type auto dismiss flag
	 */
	private boolean autoDismiss;

	/**
	 * Auto dismiss is allowed to change or not
	 */
	private boolean autoDismissAllowed;

	/**
	 * Alert type auto acknowledge flag
	 */
	private boolean autoAcknowledge;

	/**
	 * 
	 */
	private Set<MessageTemplate> messageTemplates;

	/**
	 * Auto dismiss allowed by default.
	 */
	public AlertType(final String name, String displayName, final String description, final String priority,
	        final boolean isActive, final boolean autoDismiss, final boolean autoAcknowledge) {
		this(name, displayName, description, priority, DEFAULT_TEMPLATE_HEADER, DEFAULT_TEMPLATE_BODY, CollectionUtils
		        .newSet(MessageTemplate.class), isActive, autoDismiss, autoAcknowledge, true);
	}

	/**
	 * All fields constructor.
	 */
	public AlertType(final String name, String displayName, final String description, final String priority,
	        final String defaultHeader, final String defaultBody, final Set<MessageTemplate> messageTemplates,
	        final boolean isActive, final boolean autoDismiss, final boolean autoAcknowledge,
	        final boolean autoDismissAllowed) {
		this.name = name;
		this.displayName = displayName;
		this.description = description;
		this.priority = priority;
		this.isActive = isActive;
		this.autoDismiss = autoDismiss;
		this.autoAcknowledge = autoAcknowledge;
		this.autoDismissAllowed = autoDismissAllowed;
		this.defaultHeader = defaultHeader;
		this.defaultBody = defaultBody;
		this.messageTemplates = messageTemplates;
	}

	/**
	 * Auto dismiss allowed by default.
	 */
	public AlertType(final String name, String displayName, final String description, final String priority,
	        final String header, final String body, final boolean isActive, final boolean autoDismiss,
	        final boolean autoAcknowledge) {
		this(name, displayName, description, priority, header, body, CollectionUtils.newSet(MessageTemplate.class),
		        isActive, autoDismiss, autoAcknowledge, true);
	}

	public Set<MessageTemplate> getMessageTemplates() {
		return Collections.unmodifiableSet(messageTemplates);
	}

	public MessageTemplate getMessageTemplate(final MessageType mType) {
		return CollectionUtils.search(messageTemplates, new CollectionUtils.Predicate<MessageTemplate>() {
			@Override
			public boolean evaluate(MessageTemplate t) {
				return t.getMessageType().equals(mType);
			}
		});
	}

	/**
	 * Name getter
	 * 
	 * @return name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Displayable name getter
	 * 
	 * @return displayable name
	 */
	public String getDisplayName() {
		return displayName;
	}

	/**
	 * Description getter
	 * 
	 * @return description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Priority getter
	 * 
	 * @return
	 */
	public String getPriority() {
		return priority;
	}

	/**
	 * Status flag getter
	 * 
	 * @return
	 */
	public boolean isActive() {
		return isActive;
	}

	public void setIsActive(final boolean flag) {
		isActive = flag;
	}

	public boolean isAutoDismiss() {
		return autoDismiss;
	}

	public boolean isAutoDismissAllowed() {
		return autoDismissAllowed;
	}

	public boolean isAutoAcknowledge() {
		return autoAcknowledge;
	}

	public String getDefaultHeader() {
		return defaultHeader;
	}

	public String getDefaultBody() {
		return defaultBody;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof AlertType))
			return false;
		final AlertType other = (AlertType) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (isActive == null) {
			if (other.isActive != null)
				return false;
		} else if (!isActive.equals(other.isActive))
			return false;
		if (priority == null) {
			if (other.priority != null)
				return false;
		} else if (!priority.equals(other.priority))
			return false;
		return true;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder(100);
		sb.append("name=");
		sb.append(name);
		sb.append(';');
		sb.append("active=");
		sb.append(isActive);
		sb.append(';');

		return sb.toString();
	}

	public void setName(final String name) {
		this.name = name;
	}

	public void setDisplayName(final String displayName) {
		this.displayName = displayName;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

	public void setPriority(final String priority) {
		this.priority = priority;
	}

	public void setDefaultHeader(final String defaultHeader) {
		this.defaultHeader = defaultHeader;
	}

	public void setDefaultBody(final String defaultBody) {
		this.defaultBody = defaultBody;
	}

	public void setAutoDismiss(final boolean autoDismiss) {
		this.autoDismiss = autoDismiss;
	}

	public void setAutoDismissAllowed(boolean autoDismissAllowed) {
		this.autoDismissAllowed = autoDismissAllowed;
	}

	public void setAutoAcknowledge(final boolean autoAcknowledge) {
		this.autoAcknowledge = autoAcknowledge;
	}

	public void setMessageTemplates(final Set<MessageTemplate> messageTemplates) {
		this.messageTemplates = messageTemplates;
	}

}
