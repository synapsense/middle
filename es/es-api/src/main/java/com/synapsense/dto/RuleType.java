package com.synapsense.dto;

import java.io.Serializable;

/**
 * Rule type descriptor. Used as a transfer object(DTO);
 * 
 * @author shabanov
 * 
 */
public class RuleType implements Serializable {

	private static final long serialVersionUID = -613543226497874040L;

	public static final int RUN_ON_SCHEDULE = 0;
	public static final int ALWAYS_RUN = 1;

	/**
	 * Rule type name - unique
	 */
	private String name;
	/**
	 * Source code of the rule type
	 */
	private String source;
	/**
	 * Type of source code
	 */
	private String sourceType;

	/**
	 * Domain specific language
	 */
	private String dsl;
	/**
	 * Is source code compiled?
	 */
	private boolean precompiled;

	// For no src props rules
	// 0 - run only on schedule
	// 1 - run every time property value is requested
	private int methodBehavior = RUN_ON_SCHEDULE;

	/**
	 * Scheduler configuration
	 */
	private String ruleSchedulerConfig;

	public RuleType(String name, String source, String sourceType, boolean precompiled) {
		this(name, source, sourceType, precompiled, null, ALWAYS_RUN);
	}

	public RuleType(String name, String source, String sourceType, boolean precompiled, String config) {
		this(name, source, sourceType, precompiled, config, RUN_ON_SCHEDULE);
	}

	public RuleType(String name, String source, String sourceType, boolean precompiled, String config,
	        int methodBehavior) {
		if (name == null || name.isEmpty()) {
			throw new IllegalArgumentException("Supplied 'name' is null or empty!");
		}
		this.name = name;

		if (!precompiled && (source == null || source.isEmpty())) {
			throw new IllegalArgumentException(
			        "Supplied rule type has no source but it is precompiled!It is not possible to have no source in the rule which is not precompiled!");
		}
		this.source = source;

		if (sourceType == null) {
			throw new IllegalArgumentException("Supplied 'sourceType' is null or empty!");
		}
		this.sourceType = sourceType;

		this.precompiled = precompiled;

		if (methodBehavior == RUN_ON_SCHEDULE && (config == null || config.isEmpty())) {
			throw new IllegalArgumentException("Rule's behavior set to ON_SCHEDULE but supplied 'config' is null or empty");
		}

		if (config != null && config.length() > 15) {
			throw new IllegalArgumentException("The max 'config' length is 15 characters!");
		}

		this.ruleSchedulerConfig = config;
		this.methodBehavior = methodBehavior;
	}

	public RuleType(String name, String source, String sourceType, String dsl) {
		this(name, source, sourceType, false);
		// if (dsl == null || dsl.isEmpty()) {
		// throw new IllegalArgumentException(
		// "Supplied 'dsl' cannot be null or empty");
		// }
		this.dsl = dsl;
	}

	/**
	 * @return the ruleSchedulerConfig
	 */
	public String getRuleSchedulerConfig() {
		return ruleSchedulerConfig;
	}

	public String getName() {
		return name;
	}

	public String getSource() {
		return source;
	}

	public String getSourceType() {
		return sourceType;
	}

	public String getDsl() {
		return dsl;
	}

	public boolean isPrecompiled() {
		return precompiled;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public void setSourceType(String sourceType) {
		this.sourceType = sourceType;
	}

	public void setPrecompiled(boolean precompiled) {
		this.precompiled = precompiled;
	}

	public void setRuleSchedulerConfig(String ruleSchedulerConfig) {
		this.ruleSchedulerConfig = ruleSchedulerConfig;
	}

	public int getMethodBehavior() {
		return methodBehavior;
	}

	public void setMethodBehavior(int methodBehavior) {
		this.methodBehavior = methodBehavior;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder(100);
		sb.append("RULE_TYPE:");
		sb.append("name=" + this.name + ";");
		sb.append("source_type=" + this.sourceType + ";");
		return sb.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((sourceType == null) ? 0 : sourceType.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RuleType other = (RuleType) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (sourceType == null) {
			if (other.sourceType != null)
				return false;
		} else if (!sourceType.equals(other.sourceType))
			return false;
		return true;
	}
}
