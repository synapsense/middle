package com.synapsense.dto;

import java.io.Serializable;

/**
 * Alert priority tier transfer object. Implements concept of notification
 * escalation tier. Consists of data required to process notification when it
 * stands at this escalation tier. Alert priority should have at least one
 * escalation tier.
 * 
 * @author shabanov
 * 
 */
public class PriorityTier implements Serializable, Comparable<PriorityTier> {

	private static final long serialVersionUID = -802239151281105144L;

	private String name;
	/**
	 * interval between notifications
	 */
	private long interval;
	/**
	 * number of attempts to send
	 */
	private int notificationAttempts;

	/**
	 * flag to send additional message when escalating to next tier
	 */
	private boolean sendEscalation;

	/**
	 * flag to send addition message when alert is marked acknowledged while
	 * standing at this tier
	 * 
	 */
	private boolean sendAck;

	/**
	 * flag to send addition message when alert is marked as resolved while
	 * standing at this tier
	 * 
	 */
	private boolean sendResolve;

	/**
	 * 
	 * @param name
	 *            Tier's name
	 * @param interval
	 *            Interval between notifications on this tier(>1000L)
	 * @param notificationAttempts
	 *            How many times to send notifications on this tier
	 */
	public PriorityTier(String name, long interval, int notificationAttempts, boolean sendEscalation, boolean sendAck,
	        boolean sendResolve) {
		this.name = name;
		setNotificationAttempts(notificationAttempts);
		this.interval = interval;
		this.sendEscalation = sendEscalation;
		this.sendAck = sendAck;
		this.sendResolve = sendResolve;
	}

	public String getName() {
		return name;
	}

	public long getInterval() {
		return interval;
	}

	public int getNotificationAttempts() {
		return notificationAttempts;
	}

	public boolean isSendEscalation() {
		return sendEscalation;
	}

	public boolean isSendAck() {
		return sendAck;
	}

	public boolean isSendResolve() {
		return sendResolve;
	}

	@Override
	public String toString() {
		return "PriorityTier [name=" + name + ", interval=" + interval + ", notificationAttempts="
		        + notificationAttempts + ", sendEscalation=" + sendEscalation + ", sendAck=" + sendAck
		        + ", sendResolve=" + sendResolve + "]";
	}

	public void setInterval(long interval) {
		this.interval = interval;
	}

	public void setNotificationAttempts(int notificationAttempts) {
		if (notificationAttempts<0) {
			throw new IllegalArgumentException("Supplied 'notificationAttempts' is negative");
		}
		this.notificationAttempts = notificationAttempts;
	}

	public void setSendEscalation(boolean sendEscalation) {
		this.sendEscalation = sendEscalation;
	}

	public void setSendAck(boolean sendAck) {
		this.sendAck = sendAck;
	}

	public void setSendResolve(boolean sendResolve) {
		this.sendResolve = sendResolve;
	}

	@Override
    public int compareTo(PriorityTier o) {
		return name.compareTo(o.name);
    }

}
