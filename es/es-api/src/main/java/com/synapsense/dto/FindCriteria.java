package com.synapsense.dto;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Very simple implementation of searching query builder. Can have 3 state which
 * are reflected in the inner enumeration named <code>State</code>
 * 
 * @author shabanov
 * 
 */
public class FindCriteria implements Serializable {

	private static final long serialVersionUID = 937539555448404408L;

	/**
	 * Object of this class can have three state First - EMPTY - object has just
	 * initialized Second - CONDITION - means that has just done one of the
	 * condition(IN LTH GTH EQ) operations Third - JUNCTION - means that has
	 * just done one of the junction(AND OR) operations
	 * 
	 * @author shabanov
	 * 
	 */
	private enum State {
	CONDITION, JUNCTION, EMPTY;
	};

	private String alias = "";

	private static final String NULL_CONDITION_VALUE_MESSAGE = "Value in condition must be not null!";

	private Map<Integer, Object> queryParams = null;

	private StringBuilder sb = null;

	private State currentState;

	private void setState(State state) {
		this.currentState = state;
	}

	private State getState() {
		return this.currentState;
	}

	private void checkState(State state) {
		if (this.getState().equals(State.EMPTY) && state.equals(State.JUNCTION)) {
			throw new IllegalArgumentException("Illegal state of criteria.EMPTY \"where\" cannot start from JUNCTION");
		}

		if (this.getState().equals(state)) {
			throw new IllegalArgumentException("Illegal state of criteria." + this.getState().toString()
			        + " cannot be continued by " + state.toString());
		}

	}

	private Integer nextParamNo() {
		return queryParams.size() + 1;
	}

	private boolean isFirstConjunction() {
		return this.getState().equals(State.EMPTY);
	}

	/**
	 * Default constructor
	 */
	public FindCriteria() {
		sb = new StringBuilder();
		queryParams = new HashMap<Integer, Object>();
		reset();
	}

	/**
	 * Changes alias name
	 * 
	 * @param alias
	 */
	public void changeAliasName(String alias) {
		this.alias = (alias == null) ? "" : alias;
	}

	/**
	 * Gets alias of query root entity
	 * 
	 * @return
	 */
	public String getAlias() {
		return this.alias;
	}

	/**
	 * Adds equals(=) clause
	 * 
	 * @param propName
	 * @param value
	 * @return
	 */
	public FindCriteria EQ(String propName, Object value) {
		if (value == null) {
			throw new IllegalArgumentException(NULL_CONDITION_VALUE_MESSAGE);
		}
		checkState(State.CONDITION);
		sb.append(" ");
		sb.append(((alias.isEmpty()) ? "" : (alias + ".")) + propName + " = ?");
		queryParams.put(nextParamNo(), value);
		setState(State.CONDITION);
		return this;
	}

	/**
	 * Adds Not equals(propName<>value) clause
	 * 
	 * @param propName
	 * @param value
	 * @return
	 */
	public FindCriteria NEQ(String propName, Object value) {
		if (value == null) {
			throw new IllegalArgumentException(NULL_CONDITION_VALUE_MESSAGE);
		}
		checkState(State.CONDITION);
		sb.append(" ");
		sb.append(((alias.isEmpty()) ? "" : (alias + ".")) + propName + " <> ?");
		queryParams.put(nextParamNo(), value);
		setState(State.CONDITION);
		return this;
	}

	/**
	 * Adds Less-than(propName<value) clause
	 * 
	 * @param propName
	 * @param value
	 * @return
	 */
	public FindCriteria LTH(String propName, Object value) {
		if (value == null) {
			throw new IllegalArgumentException(NULL_CONDITION_VALUE_MESSAGE);
		}
		checkState(State.CONDITION);
		sb.append(" ");
		sb.append(((alias.isEmpty()) ? "" : (alias + ".")) + propName + " < ?");
		queryParams.put(nextParamNo(), value);
		setState(State.CONDITION);
		return this;
	}

	/**
	 * Adds Greater-than(propName>value) clause
	 * 
	 * @param propName
	 * @param value
	 * @return
	 */
	public FindCriteria GTH(String propName, Object value) {
		if (value == null) {
			throw new IllegalArgumentException(NULL_CONDITION_VALUE_MESSAGE);
		}
		checkState(State.CONDITION);
		sb.append(" ");
		sb.append(((alias.isEmpty()) ? "" : (alias + ".")) + propName + " > ?");
		queryParams.put(nextParamNo(), value);
		setState(State.CONDITION);
		return this;
	}

	/**
	 * Adds AND clause ( and )
	 * 
	 * @return
	 */
	public FindCriteria AND() {
		checkState(State.JUNCTION);
		if (!isFirstConjunction()) {
			sb.append(" ");
			sb.append("AND");
		}
		setState(State.JUNCTION);
		return this;
	}

	/**
	 * Adds OR clause ( or )
	 * 
	 * @return
	 */
	public FindCriteria OR() {
		checkState(State.JUNCTION);
		if (!isFirstConjunction()) {
			sb.append(" ");
			sb.append("OR");
		}
		setState(State.JUNCTION);
		return this;
	}

	/**
	 * Adds In clause (in (:values) )
	 * 
	 * @param propName
	 * @param values
	 * @return
	 */
	public FindCriteria IN(String propName, Object[] values) {
		if (values == null || values.length == 0) {
			throw new IllegalArgumentException(NULL_CONDITION_VALUE_MESSAGE);
		}
		checkState(State.CONDITION);
		sb.append(" ");

		sb.append(((alias.isEmpty()) ? "" : (alias + ".")) + propName + " IN (");
		for (int i = 0; i < values.length - 1; i++) {
			sb.append("?");
			sb.append(",");
			queryParams.put(nextParamNo(), values[i]);
		}
		sb.append("?");
		sb.append(")");
		queryParams.put(nextParamNo(), values[values.length - 1]);
		setState(State.CONDITION);
		return this;
	}

	/**
	 * Returns "where" clause from current object state
	 * 
	 * @return
	 */
	public String getWhere() {
		return sb.toString();
	}

	/**
	 * Simple parameters getter
	 * 
	 * @return
	 */
	public Map<Integer, Object> getParams() {
		return this.queryParams;
	}

	/**
	 * Gets parameters in addition order.
	 * 
	 * @return
	 */
	public List<?> getOrderedParams() {
		List list = new LinkedList();

		for (int i = 1; i <= this.queryParams.keySet().size(); i++) {
			list.add(this.queryParams.get(i));
		}
		return list;
	}

	/**
	 * Resets state of <code>FindCriteria</code> instance Sets state to
	 * <code>State.EMPTY</code> Resets query string buffer Cleans query's
	 * parameters map Resets alias name to empty string
	 */
	public final void reset() {
		setState(State.EMPTY);
		this.alias = "";
		this.sb.setLength(0);
		this.queryParams.clear();
	}

	/**
	 * Main methods. Serves only as test method.
	 */
	public static void main(String[] args) {
		FindCriteria fc = new FindCriteria();
		fc.changeAliasName("item");
		Object[] ids = new Integer[] { 1, 2, 3, 4, 5, 6 };
		fc = fc.EQ("field_1", "field_value_1");
		fc = fc.AND();
		fc = fc.EQ("field_2", "field_value_2");
		fc = fc.OR();
		fc = fc.IN("id", ids);
		fc = fc.AND();
		fc = fc.EQ("field_3", "field_value_3");

		// System.out.println(fc.getWhere());
		// System.out.println(fc.getParams());

	}
}
