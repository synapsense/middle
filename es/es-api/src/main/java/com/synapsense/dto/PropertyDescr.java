package com.synapsense.dto;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * Descriptor of object type property. Unique identity of this object is
 * property name.
 * <p>
 * Consists information about all property features like collection , historical
 * , calculated , static. DTO for transfer information about certain property.
 * 
 * @author shabanov
 */
public class PropertyDescr implements Serializable {
	private static final long serialVersionUID = -6714881239427000725L;
	/**
	 * Property name
	 */
	private String name;
	/**
	 * Property name is used by DM
	 */
	private String mappedName;
	/**
	 * Property type name (class name actually)
	 */
	private String typeName;
	/**
	 * Is property historical.
	 * <p>
	 * To store history or not
	 */
	private boolean historical;

	/**
	 * Is property collection.
	 * <p>
	 * For one property will be stored more than one value.
	 */
	private boolean collection;

	/**
	 * Is property static.
	 * <p>
	 * Static means that property can be accessible only via its type.
	 */
	private boolean statical;

	private Set<TagDescriptor> tagDescriptors = new HashSet<TagDescriptor>();

	/**
	 * Main all-fields constructor.
	 * <p>
	 * Allows to create full initialized instance of <code>PropertyDescr</code>
	 * class
	 * 
	 * @param name
	 *            Property name
	 * @param mappedName
	 *            Mapped name
	 * @param typeName
	 *            Type name of property (actually the class name)
	 * @param isHistorical
	 *            is property historical flag
	 * @param isCollection
	 *            is property collection flag
	 * @param isStatic
	 *            is property static flags
	 */
	public PropertyDescr(String name, String mappedName, String typeName, boolean isHistorical, boolean isCollection,
	        boolean isStatic) {
		if (name == null || name.isEmpty()) {
			throw new IllegalArgumentException("Supplied 'name' is null or empty");
		}
		this.name = name;
		this.mappedName = mappedName;
		if (typeName == null || typeName.isEmpty()) {
			throw new IllegalArgumentException("Supplied 'typeName' is null or empty");
		}
		this.typeName = typeName;
		this.historical = isHistorical;
		this.collection = isCollection;
		this.statical = isStatic;
	}

	public PropertyDescr(String name, String typeName) {
		this(name, null, typeName, false, false, false);
	}

	public PropertyDescr(String name, Class<?> type) {
		this(name, type.getName());
	}

	public PropertyDescr(String name, String typeName, boolean isHistorical) {
		this(name, null, typeName, isHistorical, false, false);
	}

	public PropertyDescr(String name, String typeName, boolean isHistorical, boolean isCollection) {
		this(name, null, typeName, isHistorical, isCollection, false);
	}

	public PropertyDescr(String name, String mappedName, String typeName) {
		this(name, mappedName, typeName, false, false, false);
	}

	public PropertyDescr(String name, String mappedName, String typeName, boolean isHistorical) {
		this(name, mappedName, typeName, isHistorical, false, false);
	}

	public PropertyDescr(String name, String mappedName, String typeName, boolean isHistorical, boolean isCollection) {
		this(name, mappedName, typeName, isHistorical, isCollection, false);
	}

	public PropertyDescr(PropertyDescr pd) {
		this(pd.getName(), pd.getMappedName(), pd.getTypeName(), pd.isHistorical(), pd.isCollection(), pd.isStatic());
		for (TagDescriptor tagDescr : pd.getTagDescriptors()) {
			tagDescriptors.add(new TagDescriptor(tagDescr));
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (collection ? 1231 : 1237);
		result = prime * result + (historical ? 1231 : 1237);
		result = prime * result + ((mappedName == null) ? 0 : mappedName.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + (statical ? 1231 : 1237);
		result = prime * result + ((typeName == null) ? 0 : typeName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final PropertyDescr other = (PropertyDescr) obj;
		if (collection != other.collection)
			return false;
		if (historical != other.historical)
			return false;
		if (mappedName == null) {
			if (other.mappedName != null)
				return false;
		} else if (!mappedName.equals(other.mappedName))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (statical != other.statical)
			return false;
		if (typeName == null) {
			if (other.typeName != null)
				return false;
		} else if (!typeName.equals(other.typeName))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Propety name:" + this.name + ";mapped name:" + this.mappedName + ";type:" + this.typeName;
	}

	public String getName() {
		return name;
	}

	public String getMappedName() {
		return mappedName;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTypeName() {
		return typeName;
	}

	public Class<?> getType() {
		Class<?> result;
		try {
			if (isCollection()) {
				result = Collection.class;
			} else {
				result = Class.forName(this.getTypeName());
			}
		} catch (ClassNotFoundException e) {
			throw new IllegalStateException(e);
		}
		return result;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public void setMappedName(String mappedName) {
		this.mappedName = mappedName;
	}

	public boolean isHistorical() {
		return historical;
	}

	public void setHistorical(boolean isHistorical) {
		this.historical = isHistorical;
	}

	public boolean isCollection() {
		return collection;
	}

	public void setCollection(boolean isCollection) {
		this.collection = isCollection;
	}

	public boolean isStatic() {
		return statical;
	}

	public void setStatic(boolean isStatic) {
		this.statical = isStatic;
	}

	public boolean isMapped() {
		return (mappedName != null && !mappedName.isEmpty());
	}

	/**
	 * Get tag descriptors for this property.
	 * 
	 * @return set of type's property tag descriptors
	 */
	public Set<TagDescriptor> getTagDescriptors() {
		return tagDescriptors;
	}

	/**
	 * Get tag descriptor for this name.
	 * 
	 * @return set of type's property tag descriptors
	 */
	public TagDescriptor getTagDescriptor(String tagName) {
		for (TagDescriptor pd : tagDescriptors) {
			if (pd.getName().equals(tagName)) {
				return pd;
			}
		}
		return null;
	}
}
