package com.synapsense.dto;

import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

import com.synapsense.util.CollectionUtils;
import com.synapsense.util.Pair;

/**
 * Initial data for Environment object instantiation. <br>
 * Used along with {@link EnvObjectBundle} to create objects in bulk mode.<br>
 * 
 * @author ashabanov
 * 
 */
public final class EnvObject implements Serializable {

	private static final long serialVersionUID = -4328491853099107494L;

	private String type;
	private Collection<ValueTO> values;
	private Collection<Tag> tags;

	public EnvObject(String typeName) {
		this(typeName, Collections.<ValueTO> emptyList());
	}

	public EnvObject(String typeName, Collection<ValueTO> values) {
		this(typeName, values, Collections.<Tag> emptyList());
	}

	public EnvObject(String typeName, Collection<ValueTO> values, Collection<Tag> tags) {
		if (typeName == null || typeName.isEmpty()) {
			throw new IllegalArgumentException("Supplied 'typeName' is null or empty string");
		}

		Collection<ValueTO> tempValues = values;
		if (tempValues == null) {
			tempValues = Collections.<ValueTO> emptyList();
		}

		Collection<Tag> tempTags = tags;
		if (tempTags == null) {
			tempTags = Collections.<Tag> emptyList();
		}

		checkForValueDuplicates(tempValues);
		checkForTagDuplicates(tempTags);

		this.type = typeName;
		this.values = new LinkedList<ValueTO>(tempValues);
		this.tags = new LinkedList<Tag>(tempTags);
	}

	/**
	 * @return Unmodifiable collection of specified property values
	 */
	public Collection<ValueTO> getValues() {
		return Collections.unmodifiableCollection(values);
	}

	/**
	 * @return Unmodifiable collection of specified tags
	 */
	public Collection<Tag> getTags() {
		return Collections.unmodifiableCollection(tags);
	}

	public String getTypeName() {
		return type;
	}

	@Override
    public String toString() {
	    return "EnvObject [type=" + type + ", values=" + values + ", tags=" + tags + "]";
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		result = prime * result + ((values == null) ? 0 : values.hashCode());
		result = prime * result + ((tags == null) ? 0 : tags.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EnvObject other = (EnvObject) obj;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		if (values == null) {
			if (other.values != null)
				return false;
		} else if (!values.equals(other.values))
			return false;
		if (tags == null) {
			if (other.tags != null)
				return false;
		} else if (!tags.equals(other.tags))
			return false;
		return true;
	}

	private void checkForTagDuplicates(Collection<Tag> tags) {
		Set<Pair<String, String>> tagUniqSet = CollectionUtils.newSet();
		for (Tag tag : tags) {
			String propertyName = tag.getPropertyName();
			String tagName = tag.getTagName();

			Pair<String, String> uniqKey = Pair.newPair(propertyName, tagName);
			if (tagUniqSet.contains(uniqKey)) {
				throw new IllegalArgumentException("Supplied 'tags' contains duplicate value for property : "
				        + propertyName + " and tag : " + tagName);
			}
			tagUniqSet.add(uniqKey);
		}
	}

	private void checkForValueDuplicates(Collection<ValueTO> values) {
		Set<String> propNames = new HashSet<String>();
		for (ValueTO valueTO : values) {
			String propertyName = valueTO.getPropertyName();
			if (propNames.contains(propertyName)) {
				throw new IllegalArgumentException("Supplied 'values' contains duplicate value for property : "
				        + propertyName);
			}
			propNames.add(propertyName);
		}
	}

}
