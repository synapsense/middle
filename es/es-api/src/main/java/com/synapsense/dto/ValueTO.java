package com.synapsense.dto;

import java.io.Serializable;

/**
 * Class for transferring property values.
 * 
 * @author Oleg Stepanov
 * 
 */
public class ValueTO implements Serializable {

	private static final long serialVersionUID = 208607278478796921L;

	private String propertyName;

	private long timeStamp;

	private Object value;

	/**
	 * Creates new property value.
	 * 
	 * @param propertyName
	 *            name of the property
	 * @param value
	 *            value of the property
	 */
	public ValueTO(String propertyName, Object value) {
		this(propertyName, value, 0L);
	}

	/**
	 * Creates new property value.
	 * 
	 * @param propertyName
	 *            name of the property (not null,not empty)
	 * @param value
	 *            value of the property
	 * @param timeStamp
	 *            value change timestamp(>=0L)
	 */
	public ValueTO(String propertyName, Object value, long timeStamp) {
		setPropertyName(propertyName);
		setValue(value);
		setTimeStamp(timeStamp);
	}

	public String getPropertyName() {
		return propertyName;
	}

	public void setPropertyName(String propertyName) {
		if (propertyName == null) {
			throw new IllegalArgumentException("Supplied 'propertyName' is null");
		}

		if (propertyName.isEmpty()) {
			throw new IllegalArgumentException("Supplied 'propertyName' is empty");
		}
		this.propertyName = propertyName;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}

	public long getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(long timeStamp) {
		if (timeStamp < 0L) {
			throw new IllegalArgumentException("Supplied 'timeStamp' is negative");
		}
		this.timeStamp = timeStamp;
	}

	@Override
	public String toString() {
		return "Property[" + this.getPropertyName() + "="
		        + ((this.getValue() == null) ? "null" : this.getValue().toString()) + "]timestamp["
		        + this.getTimeStamp() + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((propertyName == null) ? 0 : propertyName.hashCode());
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final ValueTO other = (ValueTO) obj;
		if (propertyName == null) {
			if (other.propertyName != null)
				return false;
		} else if (!propertyName.equals(other.propertyName))
			return false;
		if (timeStamp != other.timeStamp)
			return false;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}

}
