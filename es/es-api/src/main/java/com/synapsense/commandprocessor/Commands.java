package com.synapsense.commandprocessor;

import com.synapsense.commandprocessor.api.Command;
import com.synapsense.commandprocessor.api.CommandEngine;
import com.synapsense.commandprocessor.commands.*;

public final class Commands {

	public static Command executeInTransaction(CommandEngine engine, Command... commands) {
		return new TransactionalCommand(commands);
	}

	public static Command stop(CommandEngine engine) {
		return new Stop(engine);
	}

	public static Command sleep(CommandEngine engine, long pause) {
		return new Sleep(engine, pause);
	}

	public static Command makeLoop(Command loop, Command pause, CommandEngine engine) {
		return new CyclicCommand(loop, pause, engine);
	}

	public static Command noop() {
		return new Command() {
			@Override
			public void execute() {
				Thread.yield();
			}
		};
	}

	public static Command chaining(String name, Command... commands) {
		return new CommandChain(name, commands);
	}
}
