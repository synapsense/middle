package com.synapsense.commandprocessor.commands;

import java.util.Arrays;
import java.util.List;

import javax.ejb.EJBException;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.transaction.UserTransaction;

import org.apache.log4j.Logger;

import com.synapsense.commandprocessor.api.Command;

public class TransactionalCommand implements Command {

	private final static Logger logger = Logger.getLogger(TransactionalCommand.class);

	private List<Command> commands;

	public TransactionalCommand(Command... commands) {
		this.commands = Arrays.asList(commands);
	}

	@Override
	public void execute() {
		UserTransaction tx;
        try {
	        tx = lookup();
        } catch (NamingException e) {
        	throw new IllegalStateException("Failed to lookup user transaction due to " , e);
        }
		
		begin(tx);
		try {
			for (Command c : commands) {
				c.execute();
			}
			commit(tx);
		} catch (Exception e) {
			rollback(tx, e);
			throw new EJBException("Transaction failed", e);
		}

	}

	private UserTransaction lookup() throws NamingException { 
		return (UserTransaction) new InitialContext().lookup("UserTransaction");
	}
	
	private void begin(UserTransaction tx) {
		try {
			tx.begin();
		} catch (Exception e) {
			throw new EJBException("Cannot start transaction", e);
		}
	}

	private void commit(UserTransaction tx) {
		try {
			int txStatus = tx.getStatus();
			if (txStatus == javax.transaction.Status.STATUS_ACTIVE) {
				tx.commit();
			} else {
				logger.debug("Could not commit transaction because it is not active. Actual tx status is " + txStatus);
			}
		} catch (Exception e) {
			throw new EJBException("Commit failed", e);
		}
	}

	private void rollback(UserTransaction tx, Exception cause) {
		try {
			int txStatus = tx.getStatus();
			if (txStatus == javax.transaction.Status.STATUS_ACTIVE
			        || txStatus == javax.transaction.Status.STATUS_MARKED_ROLLBACK) {
				tx.rollback();
			} else {
				logger.debug("Could not rollback transaction because it is not active. Actual tx status is " + txStatus);
			}
		} catch (Exception e) {
			throw new EJBException("Rollback failed", cause == null ? e : cause);
		}
	}

}
