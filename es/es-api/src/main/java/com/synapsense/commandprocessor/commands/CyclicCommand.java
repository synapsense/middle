package com.synapsense.commandprocessor.commands;

import com.synapsense.commandprocessor.api.Command;
import com.synapsense.commandprocessor.api.CommandEngine;

public final class CyclicCommand implements Command {

	private Command pause;
	private Command loop;
	private CommandEngine engine;

	public CyclicCommand(Command loop, Command pause, CommandEngine engine) {
		this.pause = pause;
		this.loop = loop;
		this.engine = engine;
	}

	@Override
	public void execute() {
		loop.execute();
		engine.addCommand(pause);
		engine.addCommand(this);
	}

}
