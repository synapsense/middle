package com.synapsense.commandprocessor;

import java.util.concurrent.BlockingQueue;

import com.synapsense.commandprocessor.api.Command;
import com.synapsense.commandprocessor.api.CommandEngine;

public class CommandEngineFactory {

	public static CommandEngine newEngine(String name) {
		return new SimpleCommandEngine(name);
	}
	
	public static CommandEngine newEngineSharedBlockingQueue(String name, BlockingQueue<Command> queue) {
		return new ExtCommandEngine(name, queue);
	}
	
}
