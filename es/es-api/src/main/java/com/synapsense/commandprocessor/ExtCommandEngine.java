package com.synapsense.commandprocessor;

import java.util.concurrent.BlockingQueue;

import org.apache.log4j.Logger;

import com.synapsense.commandprocessor.api.Command;

public class ExtCommandEngine extends AbstractEngine {
	private static final Logger logger = Logger.getLogger(ExtCommandEngine.class);

	protected ExtCommandEngine(String name, BlockingQueue<Command> commandsQueue) {
		super(name);
		super.commandsQueue = commandsQueue;
	}

	@Override
	public void addCommand(Command command) {
		super.addCommand(command);
		logger.trace(identify() + ": command " + command.getClass().getName() + " is awaiting to be perormed");
	}

	@Override
	public void run() {
		super.run();
		logger.debug(identify() + ": starting...");
		while (isRunning || !commandsQueue.isEmpty()) {
			try {
				Command curr = commandsQueue.take();
				logger.trace(identify() + " : executing " + curr.getClass().getName());
				curr.execute();
				logger.trace(identify() + " : " + curr.getClass().getName() + " has been performed");
			} catch (InterruptedException e) {
				logger.debug("Command engine thread was interrupted ", e);
				Thread.currentThread().interrupt();
				shutdown();
			} catch (Exception e) { // to prevent thread dead
				// for the case when command throws unexpected exception
				logger.warn("Failed to execute command due to ", e);
			}

		}
	}
}
