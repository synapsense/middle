package com.synapsense.commandprocessor.commands;

import org.apache.log4j.Logger;

import com.synapsense.commandprocessor.api.Command;
import com.synapsense.commandprocessor.api.CommandEngine;

public class Stop implements Command {
	private static final Logger logger = Logger.getLogger(Stop.class);

	private CommandEngine e;

	public Stop(CommandEngine e) {
		super();
		this.e = e;
	}

	@Override
	public void execute() {
		logger.debug(e.identify() + ":Executing stop command...");
		e.shutdown();
	}

}
