package com.synapsense.commandprocessor.commands;

import org.apache.log4j.Logger;

import com.synapsense.commandprocessor.api.Command;
import com.synapsense.commandprocessor.api.CommandEngine;

public class Sleep implements Command {
	private static final Logger logger = Logger.getLogger(Sleep.class);

	private long time = 1;
	private CommandEngine e;

	public Sleep(CommandEngine e, long time) {
		this.e = e;
		this.time = time;
	}

	@Override
	public void execute() {
		try {
			logger.debug(e.identify() + ":Sleeping " + time + " ms ...");
			Thread.sleep(this.time);
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
		}
	}

};
