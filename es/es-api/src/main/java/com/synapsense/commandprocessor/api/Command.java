package com.synapsense.commandprocessor.api;

public interface Command {
	void execute();
}
