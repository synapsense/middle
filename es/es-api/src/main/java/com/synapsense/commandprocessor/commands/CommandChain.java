package com.synapsense.commandprocessor.commands;

import com.synapsense.commandprocessor.api.Command;
import com.synapsense.util.CollectionUtils;
import org.apache.log4j.Logger;

import java.util.Arrays;
import java.util.List;

public class CommandChain implements Command {
    private final static Logger logger = Logger.getLogger(CommandChain.class);

    private String name;
	private List<Command> commands = CollectionUtils.newList();
	private boolean stopOnSingleFailure = false;
	
	public CommandChain(String name, Command...commands) {
        this.name = name;
	    this.commands.addAll(Arrays.asList(commands));
    }

	@Override
	public void execute() {
		for (Command c : commands) {
			try {
				if (logger.isDebugEnabled()) {
					logger.debug("Executing next command in then chain : " + name);
				}
				c.execute();
			} catch (Exception e) {
                logger.warn("Failed to execute command chain : " + name, e);
				if (stopOnSingleFailure) {
					return;
				}
			}
		}
	}

}
