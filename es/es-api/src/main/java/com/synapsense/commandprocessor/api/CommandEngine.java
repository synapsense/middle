package com.synapsense.commandprocessor.api;

public interface CommandEngine extends Runnable {
	void addCommand(Command command);

	void addSyncCommand(Command command);

	/**
	 * Interrupts engine execution waiting for already executing command. All
	 * not performed commands will be lost.
	 * 
	 */
	void shutdown();

	String identify();

	@Override
	void run();

}
