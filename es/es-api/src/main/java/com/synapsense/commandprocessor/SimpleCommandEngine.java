package com.synapsense.commandprocessor;

import java.util.concurrent.LinkedBlockingQueue;

import org.apache.log4j.Logger;

import com.synapsense.commandprocessor.api.Command;

class SimpleCommandEngine extends AbstractEngine {
	private static final Logger logger = Logger.getLogger(SimpleCommandEngine.class);

	public SimpleCommandEngine(String name) {
		super(name);
		super.commandsQueue = new LinkedBlockingQueue<Command>();
	}

	public void addCommand(Command command) {
		super.addCommand(command);
		logger.trace(identify() + ": command " + command.getClass().getName() + " is awaiting to be perormed");
	}

	public void run() {
		super.run();
		logger.debug(identify() + ":Starting...");
		while (commandsQueue.size() != 0 && isRunning) {
			Command curr = commandsQueue.poll();
			if (curr == null)
				continue;
			logger.trace(identify() + ":Executing " + curr.getClass().getName());
			curr.execute();
		}
	}

}
