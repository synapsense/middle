package com.synapsense.commandprocessor;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CountDownLatch;

import org.apache.log4j.Logger;

import com.synapsense.commandprocessor.api.Command;
import com.synapsense.commandprocessor.api.CommandEngine;

abstract class AbstractEngine implements CommandEngine {
	private static final Logger logger = Logger.getLogger(AbstractEngine.class);

	protected BlockingQueue<Command> commandsQueue;

	protected volatile boolean isRunning = false;
	private String name;

	protected AbstractEngine(String name) {
		this.name = name;
	}

	@Override
	public void shutdown() {
		isRunning = false;
	}

	@Override
	public void run() {
		isRunning = true;
	}

	@Override
	public String identify() {
		return name;
	}

	@Override
	public void addCommand(Command command) {
		commandsQueue.add(command);
	}

	@Override
	public void addSyncCommand(Command command) {
		CountDownLatch startSignal = new CountDownLatch(1);
		CountDownLatch doneSignal = new CountDownLatch(1);
		try {
			commandsQueue.add(new SyncCommand(startSignal, doneSignal, command));
			startSignal.countDown();
			doneSignal.await();
		} catch (InterruptedException e) {
			logger.warn("Failed to add synchronized command", e);
			Thread.currentThread().interrupt();
		}
	}

	protected class SyncCommand implements Command {

		private CountDownLatch startSignal;
		private CountDownLatch doneSignal;
		private Command command;

		public SyncCommand(CountDownLatch startSignal, CountDownLatch doneSignal, Command command) {
			this.startSignal = startSignal;
			this.doneSignal = doneSignal;
			this.command = command;
		}

		@Override
		public void execute() {
			try {
				startSignal.await();
				command.execute();
				doneSignal.countDown();
			} catch (InterruptedException e) {
				logger.warn("Failed to execute synchronized command", e);
				Thread.currentThread().interrupt();
			}
		}
	}

}
