package com.synapsense.transport.rmi;

public class Constants {
	public static final String RMI_SERVICE_NAME = "com.synapsense.nsvc.transport.rmi.AsyncRmiClientTransport.RmiServiceName";
	public static final String RMI_REGISTRY_HOST = "com.synapsense.nsvc.transport.rmi.AsyncRmiClientTransport.RmiRegistryHost";
	public static final String RMI_REGISTRY_PORT = "com.synapsense.nsvc.transport.rmi.AsyncRmiClientTransport.RmiRegistryPort";
	public static final String CLIENT_PORT = "com.synapsense.nsvc.transport.rmi.AsyncRmiClientTransport.ClientPort";
	public static final String EJB_CLIENT_DEFAULT_HOST = "remote.connection.default.host";

	public static final String DEFAULT_SERVICE_NAME = "SynapServer/RmiNotificationTransport";
	public static final String DEFAULT_REGISTRY_HOST = "localhost";
	public static final String DEFAULT_REGISTRY_PORT = "1099";
}
