package com.synapsense.transport;

import java.util.Properties;

public interface ServerTransport {

	void start(Properties params);

	void stop();

}
