package com.synapsense.transport;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.ServiceLoader;

public class TransportManager {

	public static final String TRANSPORT_TYPE = "com.synapsense.transport";

	private static Map<String, ClientTransport> clientTransports;
	private static Map<String, ServerTransport> serverTransports;

	static {
		clientTransports = new HashMap<String, ClientTransport>();
		initServices(ClientTransport.class, clientTransports);

		serverTransports = new HashMap<String, ServerTransport>();
		initServices(ServerTransport.class, serverTransports);
	}

	private static <T> void initServices(Class<T> srvClass, Map<String, T> services) {
		ServiceLoader<T> transportLoader = ServiceLoader.load(srvClass);
		for (T t : transportLoader) {
			services.put(t.getClass().getName(), t);
		}
	}

	/**
	 * Client-side method. Returns proxy for specified service.
	 * 
	 * @param ctx
	 * @param serviceClass
	 * @throws InterfaceNotSupportedException
	 * @return
	 */
	public static <T> Proxy<T> getProxy(Properties ctx, Class<T> serviceClass) {
		String transportType = ctx.getProperty(TRANSPORT_TYPE);
		if (transportType == null)
			throw new IllegalArgumentException(TRANSPORT_TYPE + " property must be specified");
		ClientTransport t = clientTransports.get(transportType);
		if (t == null)
			throw new RuntimeException("Cannot find transport " + transportType);
		return t.getProxy(ctx, serviceClass);
	}

	public static void startServerTransports(Properties params) {
		for (ServerTransport t : serverTransports.values())
			t.start(params);
	}

	public static void stopServerTransports() {
		for (ServerTransport t : serverTransports.values())
			t.stop();
	}
	
	public static Map<String, ClientTransport> getClientTransports() {
		return Collections.unmodifiableMap(clientTransports);
	}

	public static Map<String, ServerTransport> getServerTransports() {
		return Collections.unmodifiableMap(serverTransports);
	}
}
