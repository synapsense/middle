package com.synapsense.transport;

/**
 * Generic interface for a class representing client-side entity for accessing
 * remote services.
 * 
 * @author Oleg Stepanov
 * 
 */
public interface Proxy<T> {

	/**
	 * @return Service proxied by this instance
	 */
	T getService();

	/**
	 * Release this proxy and any resources associated with it. Normally calling
	 * any other methods on the proxy after this method will be illegal.
	 */
	void release();

	/**
	 * Analog of 'ping' method. Implementation must never throw any exceptions.
	 * No listeners will be invoked.
	 * 
	 * @see #checkConnection()
	 * @return true if proxy implementation believes it is active now.
	 */
	boolean isConnected();

	/**
	 * Same as {@link #isConnected()} but throws NotConnectedException.
	 */
	void checkConnection() throws NotConnectedException;

	/**
	 * Connect proxy. If proxy is currently connected then it's up to its
	 * implementation whether anything should be done at all or not.
	 * Implementation must throw NotConnectedException if connection attempt has
	 * been failed and call disconnected() method on registered listeners;
	 * otherwise connected() method should be called if proxy performed a real
	 * connect/reconnect.
	 */
	void connect() throws NotConnectedException;

	/**
	 * Add new life-cycle listener. Implementation can use equals/hashCode
	 * methods for instances management.
	 * 
	 * @param listener
	 *            new listener.
	 */
	void addListener(LifecycleListener listener);

	/**
	 * Removes previously added listener (using equals/hashCode methods).
	 * 
	 * @param listener
	 *            life-cycle listener to be removed.
	 */
	void removeListener(LifecycleListener listener);

	/**
	 * Listener for proxy's life cycle events.<br>
	 * An example of the simplest reconnection listener may look as this:
	 * <code><pre>
	 * proxy.setListener(new LifecycleListener() {
	 * 
	 * 	public void disconnected(Proxy proxy) {
	 * 		try {
	 * 			proxy.connect();
	 * 		} catch (Exception e) {
	 * 			//do nothing or log it. if reconnection fails
	 * 			//this listener will be invoked again when
	 * 			//connection check period elapses.
	 * 		}
	 * });
	 * </pre></code> However, client's implementation may need a more elaborated
	 * processing. For example, in case of Notification Service it may need to
	 * reload local ES model caches since some events could have been lost
	 * during reconnection.
	 */
	interface LifecycleListener {
		/**
		 * Method will be invoked after each successful connection attempt.
		 * 
		 * @param proxy
		 */
		void connected(Proxy<?> proxy);

		/**
		 * Invoked when proxy's got disconnected. Note: for safety reasons it
		 * won't be invoked when proxy is being released to prevent any
		 * reconnection listener from keeping it alive.
		 * 
		 * @param proxy
		 */
		void disconnected(Proxy<?> proxy);
	}
}
