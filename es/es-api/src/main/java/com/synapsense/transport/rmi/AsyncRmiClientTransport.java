package com.synapsense.transport.rmi;

import com.synapsense.service.nsvc.Event;
import com.synapsense.service.nsvc.EventProcessor;
import com.synapsense.service.nsvc.NotificationService;
import com.synapsense.transport.AbstractProxy;
import com.synapsense.transport.ClientTransport;
import com.synapsense.transport.InterfaceNotSupportedException;
import com.synapsense.transport.NotConnectedException;
import com.synapsense.transport.Proxy;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UID;
import java.rmi.server.UnicastRemoteObject;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import org.apache.log4j.Logger;

public class AsyncRmiClientTransport implements ClientTransport {

	@SuppressWarnings("unchecked")
	@Override
	public <T> Proxy<T> getProxy(Properties ctx, Class<T> serviceClass) throws InterfaceNotSupportedException {
		if (!serviceClass.isAssignableFrom(NotificationService.class))
			throw new InterfaceNotSupportedException(AsyncRmiClientTransport.class, serviceClass);
		return (Proxy<T>) new NotificationServiceProxy(ctx);
	}

}

class NotificationServiceProxy extends AbstractProxy<NotificationService> implements NotificationService,
        AsyncRmiNotificationClient {

	private static final Logger log = Logger.getLogger(NotificationServiceProxy.class);

	private String rmiServiceName;
	private String rmiServiceHost;
	private int rmiServicePort;

	private AsyncRmiNotificationService serverTransport;
	private Properties env;
	private Remote thisRemote;
	private int clientPort;

	/**
	 * Two-way mapping between processors and their UIDs. Since processor
	 * (de)registrations are assumed to be relatively rare, all operations
	 * involving access to this map are synchronized for simplicity (excluding
	 * notify() which must be as fast as it can be).
	 */
	private final ProcessorsMap processors = new ProcessorsMap();

	private final Collection<ProcessorFilterPair> registrationQueue = new LinkedList<ProcessorFilterPair>();

	public NotificationServiceProxy(Properties env) {
		this.env = env;
		rmiServiceName = env.getProperty(Constants.RMI_SERVICE_NAME);
		if (rmiServiceName == null) {
			rmiServiceName = Constants.DEFAULT_SERVICE_NAME;
			log.info(Constants.RMI_SERVICE_NAME + " property is not specified, defaulted to " + rmiServiceName);
		}
		//if RMI_REGISTRY_HOST property is explicitly specified, then use it
		rmiServiceHost = env.getProperty(Constants.RMI_REGISTRY_HOST);
		if (rmiServiceHost == null) {
			//if it's not specified, try to read common EJB client host property
			rmiServiceHost = env.getProperty(Constants.EJB_CLIENT_DEFAULT_HOST);
			if (rmiServiceHost != null) {
				log.info("Using default EJB host for connection: " + rmiServiceHost);
			}
			else{
				//if it's not found too, use localhost as a default
				rmiServiceHost = Constants.DEFAULT_REGISTRY_HOST;
				log.info("Neither " + Constants.RMI_REGISTRY_HOST + " nor " + Constants.EJB_CLIENT_DEFAULT_HOST + " property is specified, defaulted to " + rmiServiceHost);
			}
		}

		try {
			clientPort = Integer.parseInt(env.getProperty(Constants.CLIENT_PORT, "0"));
		} catch (NumberFormatException e) {
			throw new IllegalArgumentException("Cannot parse \"" + Constants.CLIENT_PORT + "\" parameter", e);
		}

		try {
			rmiServicePort = Integer.parseInt(env.getProperty(Constants.RMI_REGISTRY_PORT, Constants.DEFAULT_REGISTRY_PORT));
		} catch (NumberFormatException e) {
			throw new IllegalArgumentException("Cannot parse \"" + Constants.RMI_REGISTRY_PORT + "\" parameter", e);
		}

		try {
			// we don't connect to the server in constructor, only publish the
			// local transport
			thisRemote = UnicastRemoteObject.exportObject((AsyncRmiNotificationClient) this, clientPort);
			log.info("Local transport bound to port " + clientPort);
		} catch (RemoteException e) {
			throw new RuntimeException("Cannot export client-side transport", e);
		} catch (Exception e) {
			throw new RuntimeException("Cannot connect to the server-side transport", e);
		}
	}

	@Override
	public void registerProcessor(EventProcessor ep, EventProcessor filter) {
		if (ep == null)
			throw new IllegalArgumentException("Event processor must be not null");

		ProcessorFilterPair pfp = new ProcessorFilterPair(ep, filter);
		// make sure we are connected
		synchronized (this) {
			if (!isConnected()) {
				try {
					connect();
				} catch (Exception e) {
					log.warn("Cannot connect and register processor, going to try later", e);
					// just put the registration into the queue
					synchronized (registrationQueue) {
						registrationQueue.add(pfp);
					}
					// and call disconnected on listeners
					fireDisconnectedEvent();
					return;
				}
			}
			// let's try to register this one
			try {
				registerProcessors(new ProcessorFilterPair[] { pfp });
			} catch (RemoteException e) {
				log.warn("Cannot register processor, going to try later", e);
				synchronized (registrationQueue) {
					registrationQueue.add(pfp);
				}
				// call disconnected on listeners
				fireDisconnectedEvent();
				return;
			}// non remote exceptions will be returned back to client

			// complete pending registrations, if any
			processRegistrationQueue();
		}
	}

	private int processRegistrationQueue() {
		ProcessorFilterPair[] tmp_regs;
		// drain registration queue
		synchronized (registrationQueue) {
			if (registrationQueue.size() == 0)
				return 0;
			tmp_regs = registrationQueue.toArray(new ProcessorFilterPair[registrationQueue.size()]);
			registrationQueue.clear();
		}

		try {
			registerProcessors(tmp_regs);
			return tmp_regs.length;
		} catch (Exception e) {
			// TODO figure out how to inform client about exceptions due to
			// programming errors
			log.warn("Failed to register " + tmp_regs.length + " processor(s), will attempt later", e);
			// put them back in the queue
			synchronized (registrationQueue) {
				registrationQueue.addAll(Arrays.asList(tmp_regs));
			}
			return 0;
		}
	}

	synchronized private void registerProcessors(ProcessorFilterPair[] processors) throws RemoteException {
		for (ProcessorFilterPair pfp : processors) {
			EventProcessor processor = pfp.processor;
			EventProcessor filter = pfp.filter;
			// let's see if the guy is already registered
			UID id = this.processors.getId(processor);
			if (id == null) {
				// it's the new one, subscribe him
				id = serverTransport
				        .subscribe((AsyncRmiNotificationClient) thisRemote, new EventProcessor[] { filter })[0];
				this.processors.addProcessor(new ProcessorFilterPair(processor, filter), id);
			} else {
				// renew the filter
				serverTransport.setFilter(new UID[] { id }, new EventProcessor[] { filter });
				this.processors.getProcessor(id).filter = filter;
			}
		}
	}

	@Override
	public void deregisterProcessor(EventProcessor ep) {
		if (ep == null)
			throw new IllegalArgumentException("Event processor must be not null");

		deregisterProcessors(new EventProcessor[] { ep });

	}

	synchronized private void deregisterProcessors(EventProcessor[] processors) {
		Collection<UID> ids = new LinkedList<UID>();
		for (EventProcessor ep : processors) {
			if (ep == null)
				continue;
			UID id = this.processors.getId(ep);
			if (id != null) {
				this.processors.removeProcessor(ep);
				ids.add(id);
			}
		}
		if (serverTransport == null) {
			fireDisconnectedEvent();
			return;
		}

		try {
			serverTransport.unSubscribe(ids.toArray(new UID[ids.size()]));
		} catch (Throwable t) {
			log.debug("Failed to unsubscribe processors on server", t);
			// exception can be ignored: server-side transport will
			// remove subscription next time when our notify() returns false
			fireDisconnectedEvent();
		}
	}

	@Override
	public boolean notify(Event e, UID subscriptionId) {
		ProcessorFilterPair pfp = processors.getProcessor(subscriptionId);
		if (pfp == null) {
			log.warn("Received event for subscriber " + subscriptionId + " which is not registered. Event: " + e);
			return false;
		}
		if (log.isTraceEnabled())
			log.trace("Sending event " + e + " to processor " + pfp.processor);
		try {
			pfp.processor.process(e);
		} catch (Exception exc) {
			log.warn("Processor " + pfp.processor + " failed to process " + e + ".", exc);
		}
		return true;
	}

	@Override
	public NotificationService getService() {
		return this;
	}

	@Override
	synchronized public void release() {
		// unsubscribe all processors belonging to this proxy
		try {
			serverTransport.unSubscribe(processors.getAllIds());
		} catch (Throwable t) {
			log.warn("Cannot deregister processors", t);
		}
		processors.clear();
		try {
			if (thisRemote != null) {
				UnicastRemoteObject.unexportObject((AsyncRmiNotificationClient) this, true);
				thisRemote = null;
			}
		} catch (Throwable t) {
			log.warn("Cannot unexport remote interface", t);
		}
		serverTransport = null;
		super.release();
	}

	@Override
	synchronized public void checkConnection() throws NotConnectedException {
		if (serverTransport == null)
			throw new NotConnectedException("Not connected to server-side transport");
		if (getMissedSubscriptions().length != 0) {
			throw new NotConnectedException("Some processors are not registered on the server");
		}
	}

	@Override
	synchronized protected boolean internal_reconnect() throws NotConnectedException {
		try {
			Registry registry = LocateRegistry.getRegistry(rmiServiceHost, rmiServicePort);
			serverTransport = (AsyncRmiNotificationService) registry.lookup(rmiServiceName);
			log.info("Connected to server transport " + serverTransport.toString());
		} catch (Exception e) {
			throw new NotConnectedException("Cannot connect to server", e);
		}

		// complete pending registration if any
		int registered = processRegistrationQueue();

		UID[] missed = getMissedSubscriptions();
		if (missed.length == 0 && registered == 0)
			return true;// no processors reconnect is needed
		// reconnect each processor
		Collection<ProcessorFilterPair> tmp_reg = new LinkedList<ProcessorFilterPair>();
		for (UID id : missed) {
			ProcessorFilterPair pfp = processors.getProcessor(id);
			if (pfp != null) {
				processors.removeProcessor(id);
				tmp_reg.add(pfp);
			}
		}
		try {
			registerProcessors(tmp_reg.toArray(new ProcessorFilterPair[tmp_reg.size()]));
		} catch (Exception e) {
			// k, let's add these processors into the registration queue then
			synchronized (registrationQueue) {
				registrationQueue.addAll(tmp_reg);
			}
			throw new NotConnectedException("Cannot connect to server", e);
		}
		return true;
	}

	private UID[] getMissedSubscriptions() throws NotConnectedException {
		if (processors.size() == 0)
			return new UID[0];// nothing more to check
		try {
			return serverTransport.getNotRegistered(processors.getAllIds());
		} catch (Exception e) {
			throw new NotConnectedException("Cannot access server", e);
		}
	}

	private static class ProcessorFilterPair {

		public EventProcessor processor;
		public EventProcessor filter;

		public ProcessorFilterPair(EventProcessor processor, EventProcessor filter) {
			this.processor = processor;
			this.filter = filter;
		}

	}

	/**
	 * Provides atomic synchronized access to uids-to-processors and
	 * processors-to-uids maps.
	 * 
	 */
	private static class ProcessorsMap {

		private final ReadWriteLock lock = new ReentrantReadWriteLock();
		private final Map<EventProcessor, UID> uids = new HashMap<EventProcessor, UID>();
		private final Map<UID, ProcessorFilterPair> processors = new HashMap<UID, ProcessorFilterPair>();

		public int size() {
			try {
				lock.readLock().lock();
				return uids.size();
			} finally {
				lock.readLock().unlock();
			}
		}

		public void clear() {
			try {
				lock.writeLock().lock();
				uids.clear();
				processors.clear();
			} finally {
				lock.writeLock().unlock();
			}
		}

		public ProcessorFilterPair getProcessor(UID id) {
			try {
				lock.readLock().lock();
				return processors.get(id);
			} finally {
				lock.readLock().unlock();
			}
		}

		public UID getId(EventProcessor ep) {
			try {
				lock.readLock().lock();
				return uids.get(ep);
			} finally {
				lock.readLock().unlock();
			}
		}

		public void addProcessor(ProcessorFilterPair pfp, UID id) {
			try {
				lock.writeLock().lock();
				processors.put(id, pfp);
				uids.put(pfp.processor, id);
			} finally {
				lock.writeLock().unlock();
			}
		}

		public void removeProcessor(EventProcessor ep) {
			try {
				lock.writeLock().lock();
				processors.remove(uids.get(ep));
				uids.remove(ep);
			} finally {
				lock.writeLock().unlock();
			}
		}

		public void removeProcessor(UID id) {
			try {
				lock.writeLock().lock();
				uids.remove(processors.get(id).processor);
				processors.remove(id);
			} finally {
				lock.writeLock().unlock();
			}
		}

		public UID[] getAllIds() {
			try {
				lock.readLock().lock();
				return uids.values().toArray(new UID[uids.values().size()]);
			} finally {
				lock.readLock().unlock();
			}
		}
	}
}
