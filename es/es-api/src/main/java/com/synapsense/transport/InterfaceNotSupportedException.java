package com.synapsense.transport;

public class InterfaceNotSupportedException extends RuntimeException {

	private static final long serialVersionUID = 8662221165379201364L;

	private final Class<?> transportClass;
	private final Class<?> interfaceClass;

	public InterfaceNotSupportedException(Class<?> transportClass, Class<?> interfaceClass) {
		super("Interface " + interfaceClass.getName() + " is not supported by " + transportClass.getName()
		        + " transport");
		this.transportClass = transportClass;
		this.interfaceClass = interfaceClass;
	}

	public Class<?> getTransportClass() {
		return transportClass;
	}

	public Class<?> getInterfaceClass() {
		return interfaceClass;
	}

}
