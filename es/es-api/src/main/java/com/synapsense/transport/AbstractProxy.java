package com.synapsense.transport;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentLinkedQueue;

public abstract class AbstractProxy<T> implements Proxy<T> {

	private final ConcurrentLinkedQueue<LifecycleListener> listeners;
	private final Timer timer;

	private static final long CONNECTIVITY_CHECK_INTERVAL = 60_000;
	private static final long CONNECTIVITY_CHECK_INITIAL_DELAY = 300_000; // Give a chance to initial re-connect logic to succeed before doing checks.

	public AbstractProxy() {
		listeners = new ConcurrentLinkedQueue<>();
		timer = new Timer("Connectivity Check Timer", true);
		timer.scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
				performConnectivityCheck();
			}
		}, CONNECTIVITY_CHECK_INITIAL_DELAY, CONNECTIVITY_CHECK_INTERVAL);
	}

	@Override
	public void release() {
		timer.cancel();
		listeners.clear();
	}

	@Override
	public void addListener(LifecycleListener listener) {
		listeners.add(listener);
	}

	@Override
	public void removeListener(LifecycleListener listener) {
		listeners.remove(listener);
	}

	private void performConnectivityCheck() {
		if (!isConnected()) {
			fireDisconnectedEvent();
		}
	}

	@Override
	public boolean isConnected() {
		try {
			checkConnection();
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	protected void fireDisconnectedEvent() {
		for (LifecycleListener listener : listeners) {
			listener.disconnected(AbstractProxy.this);
		}
	}

	@Override
	public void connect() throws NotConnectedException {
		try {
			if (internal_reconnect()) {
				fireConnectedEvent();
			}
		} catch (NotConnectedException e) {
			throw e;
		} catch (Exception e) {
			throw new NotConnectedException("Runtime exception from transport", e);
		}
	}

	/**
	 * Implementation must throw NotConnectedException or RTE upon unsuccessful
	 * reconnect!
	 *
	 * @return true if there was a real reconnection, false otherwise
	 */
	abstract protected boolean internal_reconnect() throws NotConnectedException;

	protected void fireConnectedEvent() {
		for (LifecycleListener listener : listeners) {
			listener.connected(AbstractProxy.this);
		}
	}
}
