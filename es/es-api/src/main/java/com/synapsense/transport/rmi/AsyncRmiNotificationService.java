package com.synapsense.transport.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.server.UID;

import com.synapsense.service.nsvc.EventProcessor;

public interface AsyncRmiNotificationService extends Remote {

	UID[] subscribe(AsyncRmiNotificationClient transport, EventProcessor[] filters) throws RemoteException;

	/**
	 * Subscriptions and filters arrays are expected to be 1-to-1 mapping (that
	 * is, an unzipped list of tuples).
	 * 
	 * @param subscriptions
	 * @param filters
	 * @throws RemoteException
	 */
	void setFilter(UID[] subscriptions, EventProcessor[] filters) throws RemoteException;

	void unSubscribe(UID[] subscriptions) throws RemoteException;

	UID[] getNotRegistered(UID[] subscriptions) throws RemoteException;

}
