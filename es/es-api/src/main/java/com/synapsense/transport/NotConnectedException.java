package com.synapsense.transport;

public class NotConnectedException extends Exception {

	private static final long serialVersionUID = -5254705633693779131L;

	public NotConnectedException() {
		super();
	}

	public NotConnectedException(String msg) {
		super(msg);
	}

	public NotConnectedException(Throwable cause) {
		super(cause);
	}

	public NotConnectedException(String msg, Throwable cause) {
		super(msg, cause);
	}

}
