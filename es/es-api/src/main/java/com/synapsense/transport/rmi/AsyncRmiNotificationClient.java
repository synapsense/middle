package com.synapsense.transport.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.server.UID;

import com.synapsense.service.nsvc.Event;

public interface AsyncRmiNotificationClient extends Remote {

	/**
	 * 
	 * @param e
	 * @param susbcriptionId
	 * @return false if this subscription was removed on the client (return
	 *         value is used for sync purposes).
	 * @throws RemoteException
	 */
	boolean notify(Event e, UID susbcriptionId) throws RemoteException;
}
