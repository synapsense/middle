package com.synapsense.transport;

import java.util.Properties;

public interface ClientTransport {

	/**
	 * Client-side method. Returns proxy for a specified interface class.
	 * 
	 * @param ctx
	 * @param proxyClass
	 * @throws InterfaceNotSupportedException
	 * @return
	 */
	<T> Proxy<T> getProxy(Properties ctx, Class<T> serviceClass);
}
