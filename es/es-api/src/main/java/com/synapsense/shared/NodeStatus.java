package com.synapsense.shared;

/** Constants for WSNNODE.status */
public class NodeStatus {
	public static final int NOT_REPORTING = 0;
	public static final int OK = 1;
	public static final int DISABLED = 2;
}
