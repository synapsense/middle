package com.synapsense.util.convimpl;

import java.io.Serializable;

import com.synapsense.util.unitconverter.Converter;
import com.synapsense.util.unitconverter.ConverterException;
import com.synapsense.util.unitconverter.CustomConverterFactory;
import com.synapsense.util.unitconverter.Dimension;
import com.synapsense.util.unitconverter.UnitSystem;

public class SensorSystemValuesFilter implements CustomConverterFactory, Serializable {

	private static final long serialVersionUID = 2023809257943345168L;

	@Override
	public Converter createDirectConverter(UnitSystem baseSystem, UnitSystem targetSystem, Dimension dim,
	        Converter defaultConverter) throws ConverterException {
		return new SystemValuesFilter(defaultConverter);
	}

	@Override
	public Converter createReverseConverter(UnitSystem baseSystem, UnitSystem targetSystem, Dimension dim,
	        Converter defaultConverter) throws ConverterException {
		return new SystemValuesFilter(defaultConverter);
	}

}

class SystemValuesFilter implements Converter, Serializable {

	private static final long serialVersionUID = -585165721620336810L;
	private final Converter defaultConverter;

	public SystemValuesFilter(Converter defaultConverter) {
		this.defaultConverter = defaultConverter;
	}

	@Override
	public double convert(double value) {
		// Since large negative numbers are used as "error codes" (
		// Values -2000 "Service required", -3000 "Sensor disconnected" and
		// -5000 "New sensor, has not reported yet") they should not be
		// converted
		if (value < -1000)
			return value;
		return defaultConverter.convert(value);
	}

}