package com.synapsense.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArraySet;

public final class CollectionUtils {

	public static <K, V> Map<K, V> newMap() {
		return new HashMap<K, V>(0);
	}

	public static <K, V> Map<K, V> newMap(Map<K, V> input, Predicate<K> keyFilter) {
		Map<K, V> result = newMap();
		if (input == null) {
			return result;
		}
		for (Map.Entry<K, V> entry : input.entrySet()) {
			if (keyFilter.evaluate(entry.getKey())) {
				result.put(entry.getKey(), entry.getValue());
			}
		}
		return result;
	}

	public static <K, V> Map<K, V> newConcurrentMap() {
		return new ConcurrentHashMap<K, V>(0);
	}

	public static <K, V> Map<K, V> newLinkedMap() {
		return new LinkedHashMap<K, V>(0);
	}

	public static <K, V> Map<K, V> newMap(final Class<K> keyType, final Class<V> valueType) {
		return newMap();
	}
	
	public static <V> Set<V> newSet() {
		return new HashSet<V>(0);
	}

	public static <V> Set<V> newLinkedSet() {
		return new LinkedHashSet<V>(0);
	}
	
	public static <V> Set<V> newCopyOnWriteSet() {
		return new CopyOnWriteArraySet<V>();
	}

	public static <V> Set<V> newCopyOnWriteSet(Collection<V> collection) {
		return new CopyOnWriteArraySet<V>(collection);
	}

    @SafeVarargs
    public static <V> Set<V> newSet(V...array) {
        return newSetFromCollection(Arrays.asList(array));
    }

	public static <V> Set<V> newSetFromCollection(Collection<V> collection) {
		Set<V> newSet = newSet();
		for (V v : collection) {
			newSet.add(v);
		}
		return newSet;
	}

	public static <V> Set<V> newSet(final Class<V> clazz) {
		return newSet();
	}

	public static <V> Set<V> newSet(final Set<V> copy) {
		return new HashSet<V>(copy);
	}

	public static <K, V> Map<K, V> newSynchronizedMap() {
		return Collections.synchronizedMap(new HashMap<K, V>(0));
	}

	public static <V> Set<V> newSynchronizedSet() {
		return Collections.synchronizedSet(CollectionUtils.<V> newSet());
	}

	public static <V> List<V> newList() {
		return new LinkedList<V>();
	}

	@SafeVarargs
	public static <V> List<V> newList(V... values) {
		List<V> list = newList();
		Collections.addAll(list, values);
		return list;
	}

	public static <V> List<V> newList(Collection<V> source) {
		List<V> list = newList();
		if (source != null)
			list.addAll(source);
		return list;
	}

	public static List<Integer> range(int start, int end) {
		List<Integer> list = newList();
		for (int i = start; i <= end; i++) {
			list.add(i);
		}
		return list;
	}

	public static <V> List<V> newArrayList(int size) {
		return new ArrayList<V>(size);
	}

	public static <V> Collection<V> searchAll(final Collection<V> collection, final Predicate<V> predicate) {
		Collection<V> found = newList();
		for (final V item : collection) {
			if (predicate.evaluate(item)) {
				found.add(item);
			}
		}
		return found;
	}

	public static <V> V search(final Collection<V> collection, final Predicate<V> predicate) {
		for (final V item : collection) {
			if (predicate.evaluate(item)) {
				return item;
			}
		}
		return null;
	}

	public static <T, F> T map(F input, Functor<T, F> functor) {
		return functor.apply(input);
	}

	public static <T, F> Collection<T> map(Collection<F> collection, Functor<T, F> functor) {
		Collection<T> result = CollectionUtils.newList();
		for (F f : collection) {
			result.add(functor.apply(f));
		}
		return result;
	}
	
	public static <U, V> void updateSetValuedMap(Map<U, Set<V>> map, U key, V value) {
		Set<V> valueSet = getValueSet(map, key);
		valueSet.add(value);
	}

	public static <U, V> void updateSetValuedMap(Map<U, Set<V>> map, U key, V[] values) {
		Set<V> valueSet = getValueSet(map, key);
		valueSet.addAll(Arrays.asList(values));
	}

	private static <U, V> Set<V> getValueSet(Map<U, Set<V>> map, U key) {
		Set<V> valueSet = map.get(key);
		if (valueSet == null) {
			valueSet = new LinkedHashSet<V>();
			map.put(key, valueSet);
		}
		return valueSet;
	}
	
	@SafeVarargs
	public static <T> LinkedList<T> list(T... elems) {
		LinkedList<T> list = new LinkedList<T>();
		for (T elem : elems) {
			list.add(elem);
		}
		return list;
	}

	@SafeVarargs
	public static <T> String separate(String separator, T... elems) {
		return separate(separator, Arrays.asList(elems));
	}

	public static <T> String separate(String separator, Collection<T> elems) {
		StringBuilder builder = new StringBuilder();
		for (T elem : elems) {
			builder.append(elem).append(separator);
		}
		return builder.length() == 0 ? "" : builder.delete(builder.length() - separator.length(), builder.length())
				.toString();
	}

	public static interface Predicate<T> {
		boolean evaluate(T t);
	}

	public static interface Functor<O, I> {
		O apply(I input);
	}

}
