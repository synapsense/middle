package com.synapsense.util.unitconverter;

/**
 * Interface for double value conversion algorithms.
 * 
 * @author Oleg Stepanov
 * 
 */
public interface Converter {
	/**
	 * Implementer's conversion algorithm overrides this method.
	 * 
	 * @param value
	 *            original value
	 * @return converted value
	 */
	double convert(double value);
}
