package com.synapsense.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.jboss.ejb.client.ContextSelector;
import org.jboss.ejb.client.EJBClientConfiguration;
import org.jboss.ejb.client.EJBClientContext;
import org.jboss.ejb.client.PropertiesBasedEJBClientConfiguration;
import org.jboss.ejb.client.ThreadLocalContextSelector;
import org.jboss.ejb.client.remoting.ConfigBasedEJBClientContextSelector;

/**
 * @author pahunov
 * 
 *         Utility class to provide host, login and password for thread current
 *         thread's connection
 * 
 */
public class EJBClientConfigurator {

	private static Map<Config, ContextSelector<EJBClientContext>> selectors = new HashMap<Config, ContextSelector<EJBClientContext>>();
	private static ThreadLocal<EJBClientContext> storage = new ThreadLocal<EJBClientContext>();
	private static ContextSelector<EJBClientContext> initialSelector;

	static {
		ThreadLocalContextSelector<EJBClientContext> ejbClientContextSelector = new ThreadLocalContextSelector<EJBClientContext>(
		        storage);
		initialSelector = EJBClientContext.setSelector(ejbClientContextSelector);
	}

	static private ContextSelector<EJBClientContext> getEJBClientContextSelector(String host, String user,
	        String password) {

		// TODO: write class
		Config id = new Config(host, user, password);
		ContextSelector<EJBClientContext> ejbClientContextSelector = selectors.get(id);

		if (ejbClientContextSelector == null) {
			// fetch the Properties somehow (depending on user application's
			// requirement)
			final Properties clientConfigProps = new Properties();
			clientConfigProps.put("remote.connection.default.host", host);
			clientConfigProps.put("remote.connection.default.port", "4447");
			clientConfigProps.put("remote.connection.default.username", user);
			clientConfigProps.put("remote.connection.default.password", password);
			clientConfigProps.put("remote.connection.default.connect.options.org.xnio.Options.SASL_DISALLOWED_MECHANISMS", "JBOSS-LOCAL-USER");
			clientConfigProps.put("remote.connectionprovider.create.options.org.xnio.Options.SSL_ENABLED", "false");
			clientConfigProps.put("remote.connections", "default");

			// Create a EJB client configuration from the properties
			final EJBClientConfiguration ejbClientConfiguration = new PropertiesBasedEJBClientConfiguration(
			        clientConfigProps);

			// EJB client context selection is based on selectors. So let's
			// create a ConfigBasedEJBClientContextSelector which uses our
			// EJBClientConfiguration created in previous step
			ejbClientContextSelector = new ConfigBasedEJBClientContextSelector(ejbClientConfiguration);
			selectors.put(id, ejbClientContextSelector);
		}

		return ejbClientContextSelector;
		// Now let's setup the EJBClientContext to use this selector
	}

	public static void setupEJBClientContextSelector(String host, String user, String password) {
		storage.set(getEJBClientContextSelector(host, user, password).getCurrent());
	}

	public static void resetEJBClientContextSelector() {
		storage.set(initialSelector.getCurrent());
	}

	private static class Config {

		String host;
		String user;
		String password;

		public Config(String host, String user, String password) {
			super();
			this.host = host;
			this.user = user;
			this.password = password;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((host == null) ? 0 : host.hashCode());
			result = prime * result + ((password == null) ? 0 : password.hashCode());
			result = prime * result + ((user == null) ? 0 : user.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Config other = (Config) obj;
			if (host == null) {
				if (other.host != null)
					return false;
			} else if (!host.equals(other.host))
				return false;
			if (password == null) {
				if (other.password != null)
					return false;
			} else if (!password.equals(other.password))
				return false;
			if (user == null) {
				if (other.user != null)
					return false;
			} else if (!user.equals(other.user))
				return false;
			return true;
		}
	}

}
