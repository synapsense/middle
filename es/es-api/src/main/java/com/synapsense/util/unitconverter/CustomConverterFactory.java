package com.synapsense.util.unitconverter;

public interface CustomConverterFactory {

	Converter createDirectConverter(UnitSystem baseSystem, UnitSystem targetSystem, Dimension dim,
	        Converter defaultConverter) throws ConverterException;

	Converter createReverseConverter(UnitSystem baseSystem, UnitSystem targetSystem, Dimension dim,
	        Converter defaultConverter) throws ConverterException;
}
