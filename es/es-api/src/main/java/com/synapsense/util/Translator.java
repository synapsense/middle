package com.synapsense.util;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;

import com.synapsense.service.LocalizationService;
import com.synapsense.util.LocalizationUtils;

public class Translator {

	private static String defaultLanguage = "en_US";

	private String currentLanguage;

	private static Map<Locale, Properties> translations;

	private static final long CACHE_TIMEOUT = 300000;

	private static long lastUpdate = 0;

	private static boolean caching = false;

	private static LocalizationService localizationService;

	public void setLanguage(String lang) {
		currentLanguage = lang;
	}

	public static void setLocalizationService(LocalizationService localizationService) {
		Translator.localizationService = localizationService;
		Translator.defaultLanguage = localizationService.getDefaultLocale().toString();
		Translator.update();
	}

	public static void setCaching(boolean caching) {
		Translator.caching = caching;
	}

	public static void update() {
		translations = localizationService.getTranslations();
	}

	public String translate(String string) {
		// conditions which do not require translation
		if (string == null) {
			return string;
		}

		if (string.isEmpty()) {
			return string;
		}

		if (localizationService == null) {
			return string;
		}

		if (translations == null) {
			return string;
		}

		if (currentLanguage == null) {
			return string;
		}

		if (defaultLanguage == null) {
			return string;
		}

		if (defaultLanguage.equals(currentLanguage)) {
			return string;
		}

		if (!caching) {
			return localizationService.getPartialTranslation(string, LocalizationUtils.getLocale(currentLanguage));
		}
		
		long time = System.currentTimeMillis();
		if (time > lastUpdate + CACHE_TIMEOUT) {
			lastUpdate = time;
			update();
		}

		String res = translations.get(LocalizationUtils.getLocale(currentLanguage)).getProperty(string);
		if (res != null) {
			return res;
		}

		// well, now try to split phrase by words and find longest sub-phase
		// that has translation
		List<String> words = Arrays.asList(string.split(" "));

		if (words.size() > 1) {
			for (int i = words.size() - 1; i >= 0; i--) {
				String s = join(words.subList(0, i));
				res = translations.get(LocalizationUtils.getLocale(currentLanguage)).getProperty(s);
				if (res != null) {
					return res + " " + translate(join(words.subList(i, words.size())));
				}
			}

			for (int i = 1; i < words.size(); i++) {
				String s = join(words.subList(i, words.size()));
				res = translations.get(LocalizationUtils.getLocale(currentLanguage)).getProperty(s);
				if (res != null) {
					return translate(join(words.subList(0, i))) + " " + res;
				}
			}
		}

		return string;
	}

	private String join(List<String> strs) {

		if (strs.isEmpty()) {
			return "";
		}

		StringBuffer b = new StringBuffer();

		for (String str : strs) {
			b.append(str).append(" ");
		}

		return b.substring(0, b.length() - 1);
	}

}
