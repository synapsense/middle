package com.synapsense.util;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Arrays;
import java.util.Collection;

import org.apache.log4j.Logger;

import com.synapsense.dto.CollectionTO;
import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;
import com.synapsense.service.Environment;
import com.synapsense.util.convimpl.DataclassUnitsResolver;
import com.synapsense.util.convimpl.ValueConverter;
import com.synapsense.util.unitconverter.ConverterException;
import com.synapsense.util.unitconverter.Dimension;
import com.synapsense.util.unitconverter.UnitConverter;
import com.synapsense.util.unitconverter.UnitResolvers;
import com.synapsense.util.unitconverter.UnitResolvers.DimensionRef;
import com.synapsense.util.unitconverter.UnitResolvers.PropertyUnits;
import com.synapsense.util.unitconverter.UnitSystem;
import com.synapsense.util.unitconverter.UnitSystems;

/**
 * Proxy-class for automatic units conversion for data passed via Environment
 * interface.
 * 
 * @author Oleg Stepanov
 * 
 */
public class ConvertingEnvironmentProxy implements InvocationHandler, Cloneable {

	private static final Logger log = Logger.getLogger(ConvertingEnvironmentProxy.class);

	private Environment env;
	private UnitSystems unitSystems;
	private UnitResolvers unitResolvers;
	private boolean conversionEnabled = true;
	private String targetSystem;

	/**
	 * Returns the original Environment proxied by this instance.
	 * 
	 * @return the environment being proxied.
	 */
	public Environment getProxiedEnvironment() {
		return env;
	}

	/**
	 * Set the proxied environment.
	 * 
	 * @param env
	 *            new environment to proxy.
	 */
	public void setProxiedEnvironment(Environment env) {
		this.env = env;
	}

	/**
	 * Unit systems used for conversions.
	 * 
	 * @return unit systems
	 */
	public UnitSystems getUnitSystems() {
		return unitSystems;
	}

	/**
	 * Set unit systems to be used by this instance.
	 * 
	 * @param cm
	 *            unit converters map.
	 */
	public void setUnitSystems(UnitSystems us) {
		this.unitSystems = us;
	}

	public UnitResolvers getUnitResolvers() {
		return unitResolvers;
	}

	public void setUnitResolvers(UnitResolvers ur) {
		this.unitResolvers = ur;
	}

	/**
	 * Part of InvocationHandler interface
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {

		// intercept extended ConvertingEnvironment methods
		if ("getDimension".equals(method.getName())) {
			try {
				// we have to return dimension in the same system
				// the converted values is going to be in
				if (TO.class.isAssignableFrom(args[0].getClass())) {
					return getDimension(
					        (TO<?>) args[0],
					        (String) args[1],
					        conversionEnabled && targetSystem != null ? targetSystem : getBaseSystem((TO<?>) args[0],
					                (String) args[1]).getName());
				} else {
					return getDimension(
					        (String) args[0],
					        (String) args[1],
					        conversionEnabled && targetSystem != null ? targetSystem : getBaseSystem((String) args[0],
					                (String) args[1]).getName());
				}
			} catch (ConvertingEnvironmentException e) {
				return null;// fulfill ConvertingEnvironment interface contract
			}
		}

		if ("getTargetSystem".equals(method.getName())) {
			return getTargetSystem();
		}

		if ("setTargetSystem".equals(method.getName())) {
			setTargetSystem((String) args[0]);
			return null;
		}

		if ("isConversionEnabled".equals(method.getName())) {
			return isConversionEnabled();
		}

		if ("setConversionEnabled".equals(method.getName())) {
			setConversionEnabled((Boolean) args[0]);
			return null;
		}

		if ("convertValueToBaseSystem".equals(method.getName())) {
			if (!conversionEnabled || targetSystem == null)// no conversion
				return args[2];
			if (TO.class.isAssignableFrom(args[0].getClass())) {
				return convert((TO<?>) args[0], null, (String) args[1], Double.class, args[2], true);
			} else {
				return convert(null, (String) args[0], (String) args[1], Double.class, args[2], true);
			}
		}

		// intercept the original Environment methods
		Object result = null;
		if ((!method.getName().equals("setPropertyValue") && !method.getName().equals("setAllPropertiesValues"))
		        || !conversionEnabled || targetSystem == null) {
			try {
				result = method.invoke(env, args);
			} catch (InvocationTargetException e) {
				throw e.getCause();
			}
		}
		if (conversionEnabled && targetSystem != null) {
			if (env == null)
				throw new IllegalStateException("Cannot perform call: proxied environment was not set");
			if (unitSystems == null)
				throw new IllegalStateException("Cannot perform call: converters map was not set");
			// dispatch known methods
			if ("getPropertyValue".equals(method.getName())) {
				// dispatch on arguments
				if (args.length > 0) {// just a sanity check
					if (TO.class.isAssignableFrom(args[0].getClass())) {
						return convert((TO<?>) args[0], null, (String) args[1], (Class<?>) args[2], result, false);
					}
					if (Collection.class.isAssignableFrom(args[0].getClass())) {
						// HACK: the next line pre-fills SensorUnitResolver
						// cache with sensors types if any were requested
						extractSensorTypes((Collection<CollectionTO>) result);
						return convertCtos((Collection<CollectionTO>) result);
					}
					if (String.class.isAssignableFrom(args[0].getClass())) {
						return convert(null, (String) args[0], (String) args[1], (Class<?>) args[2], result, false);
					}
				}
			}
			if ("getAllPropertiesValues".equals(method.getName())) {
				if (TO.class.isAssignableFrom(args[0].getClass())) {
					// HACK: the next line pre-fills SensorUnitResolver
					// cache with sensors types if any were requested
					extractSensorTypes((TO<?>) args[0], (Collection<ValueTO>) result);
					return convertVtos((TO<?>) args[0], null, (Collection<ValueTO>) result);
				}
				if (Collection.class.isAssignableFrom(args[0].getClass())) {
					// HACK: the next line pre-fills SensorUnitResolver
					// cache with sensors types if any were requested
					extractSensorTypes((Collection<CollectionTO>) result);
					return convertCtos((Collection<CollectionTO>) result);
				}
			}
			if ("getHistory".equals(method.getName())) {
				// all getHistory methods can be divided onto two groups:

				// those from the first get TO<?> as the first parameter
				// and return Collection<ValueTO> as a result.
				if (TO.class.isAssignableFrom(args[0].getClass())) {
					return convertVtos((TO<?>) args[0], null, (Collection<ValueTO>) result);
				}
				// those from the second get Collection<TO<?>> as the first
				// parameter
				// and return Collection<CollectionTO> as a result.
				if (Collection.class.isAssignableFrom(args[0].getClass())) {
					return convertCtos((Collection<CollectionTO>) result);
				}
			}
			if ("setPropertyValue".equals(method.getName())) {
				TO<?> to;
				String typeName;
				if (TO.class.isAssignableFrom(args[0].getClass())) {
					to = (TO<?>) args[0];
					typeName = null;
				} else {
					to = null;
					typeName = (String) args[0];
				}
				String propName = (String) args[1];
				if (args[2] != null) {
					args[2] = convert(to, typeName, propName, args[2].getClass(), args[2], true);
				}
				return method.invoke(env, args);
			}
			if ("setAllPropertiesValues".equals(method.getName())) {
				TO<?> to = (TO<?>) args[0];
				ValueTO[] valTos = (ValueTO[]) args[1];
				convertVtos(to, null, Arrays.asList(valTos)).toArray(valTos);
				return method.invoke(env, args);
			}
		}
		return result;
	}

	private void extractSensorTypes(Collection<CollectionTO> data) {
		for (CollectionTO cto : data) {
			extractSensorTypes(cto.getObjId(), cto.getPropValues());
		}
	}

	private void extractSensorTypes(TO<?> to, Collection<ValueTO> data) {
		if (!"WSNSENSOR".equalsIgnoreCase(to.getTypeName()))
			return;
		for (ValueTO val : data) {
			if ("dataclass".equals(val.getPropertyName())) {
				Integer classId = (Integer) val.getValue();
				DataclassUnitsResolver.setDimensionRef(to, new DimensionRef("Server", classId.toString()));
			}
		}
	}

	/**
	 * Gets new proxy object which will perform conversions on a wrapped
	 * Environment interface. All returned proxies are linked with the current
	 * instance so any changes in this ConvertingEnvironmentProxy instance will
	 * be reflected by all proxies crated from it.
	 * 
	 * @return new Environment proxy instance.
	 */
	public ConvertingEnvironment getProxy() {
		return (ConvertingEnvironment) Proxy.newProxyInstance(ConvertingEnvironment.class.getClassLoader(),
		        new Class[] { ConvertingEnvironment.class }, this);
	}

	/**
	 * Converts return value for either object's property or static property. If
	 * passed to is not null then typeName is ignored and conversion is done for
	 * object specified by to. Otherwise, typeName must be not null and
	 * conversion is done for static property.
	 * 
	 * @param to
	 *            object id.
	 * @param typeName
	 *            type name.
	 * @param propName
	 *            property name.
	 * @param clazz
	 *            return value type.
	 * @param res
	 * @return
	 */
	private Object convert(TO<?> to, String typeName, String propName, Class<?> clazz, Object res, boolean reverse) {
		if (res == null)
			return res;
		// check if that's not a string etc
		if (!ValueConverter.isConvertible(clazz))
			return res;
		try {
			// get conversion
			UnitConverter uc = getConverter(to, typeName, propName, reverse);
			if (uc == null)
				return res;

			if (res instanceof ValueTO) {
				return convertVto(to, typeName, (ValueTO) res);
			} else {
				return ValueConverter.fromDouble(uc.convert(ValueConverter.toDouble(res)), clazz);
			}
		} catch (ConverterException e) {
			// property units were specified but the converter is absent.
			// as agreed, we dump it in the log and return non-converted value
			log.warn("Cannot find converter for type \"" + to.getTypeName() + "\", property \"" + propName + "\"", e);
			return res;
		}
	}

	private ValueTO convertVto(TO<?> to, String typeName, ValueTO vto) {
		Object val = vto.getValue();
		if (val != null) {
			vto.setValue(convert(to, typeName, vto.getPropertyName(), val.getClass(), val, false));
		}
		return vto;
	}

	private Collection<ValueTO> convertVtos(TO<?> to, String typeName, Collection<ValueTO> res) {
		for (ValueTO vto : res) {
			convertVto(to, typeName, vto);
		}
		return res;
	}

	private Collection<CollectionTO> convertCtos(Collection<CollectionTO> res) {
		for (CollectionTO cto : res) {
			TO<?> to = cto.getObjId();
			convertVtos(to, null, cto.getPropValues());
		}
		return res;
	}

	/**
	 * Finds converter for either object property or static property. Either to
	 * or typeName must be not null.
	 * 
	 * @param to
	 *            object id. If not null then dynamic lookup is performed.
	 * @param typeName
	 *            type name. If not null then static units lookup is performed.
	 * @param propName
	 *            property name.
	 * @param reverse
	 *            return reverse or direct converter.
	 * @return
	 * @throws ConverterException
	 * @throws ConvertingEnvironmentException
	 */
	private UnitConverter getConverter(TO<?> to, String typeName, String propName, boolean reverse)
	        throws ConverterException, ConvertingEnvironmentException {
		// get property units
		String tName = to != null ? to.getTypeName() : typeName;
		PropertyUnits pu = unitResolvers.getUnits(tName, propName);
		if (pu == null) {// no units specified
			return null;// means no need to convert
		}
		DimensionRef dimRef = to != null ? pu.getDimensionReference(to, env) : pu.getDimensionReference();
		if (dimRef == null) {// no dimension specified
			return null;// means no need to convert
		}
		// check if target system is the same as property's base system
		if (dimRef.getSystem().equals(targetSystem)) {
			return null;// no need to convert to the same system
		}
		// check if dimension is convertible
		if (unitSystems.getSimpleDimensions().containsKey(dimRef.getDimension()))
			return null;// means no need to convert
		// get conversion
		if (!reverse) {
			return unitSystems.getUnitConverter(dimRef.getSystem(), targetSystem, dimRef.getDimension());
		} else {
			return unitSystems.getUnitConverter(targetSystem, dimRef.getSystem(), dimRef.getDimension());
		}
	}

	/**
	 * Returns dimension description for a given object property.
	 * 
	 * @param to
	 *            object id
	 * @param propName
	 *            property name
	 * @param targetSystem
	 *            Target unit system to get descriptor for. If <tt>null</tt>
	 *            then returned descriptor is for base unit system.
	 * @return Descriptor of dimension for specified property.
	 * @throws ConvertingEnvironmentException
	 *             if property dimension is not described either for type itself
	 *             or in requested unit system.
	 */
	public Dimension getDimension(TO<?> to, String propName, String targetSystem) {
		// get property units
		PropertyUnits pu = unitResolvers.getUnits(to.getTypeName(), propName);
		if (pu == null)
			throw new ConvertingEnvironmentException("Units are not described for type \"" + to.getTypeName()
			        + "\", property \"" + propName + "\"");
		DimensionRef dimRef = pu.getDimensionReference(to, env);
		// first check if that's an unconvertible dimension
		Dimension dim = unitSystems.getSimpleDimensions().get(dimRef.getDimension());
		if (dim == null) {
			// ok, let's try to get a convertible dimension then
			// (unconvertible dimensions are available for any system,
			// not only for defining system)
			try {
				dim = unitSystems.getUnitSystem(targetSystem == null ? dimRef.getSystem() : targetSystem).getDimension(
				        dimRef.getDimension());
			} catch (ConverterException e) {
				throw new ConvertingEnvironmentException("Cannot find dimension for type \"" + to.getTypeName()
				        + "\", property \"" + propName + "\"", e);
			}
		}
		return dim;
	}

	/**
	 * Returns dimension description for a given type static property.
	 * 
	 * @param typeName
	 *            type name
	 * @param propName
	 *            property name
	 * @param targetSystem
	 *            Target unit system to get descriptor for. If <tt>null</tt>
	 *            then returned descriptor is for base unit system.
	 * @return Descriptor of dimension for specified property.
	 * @throws ConvertingEnvironmentException
	 *             if property dimension is not described either for type itself
	 *             or in requested unit system.
	 */
	public Dimension getDimension(String typeName, String propName, String targetSystem) {
		// get property units
		PropertyUnits pu = unitResolvers.getUnits(typeName, propName);
		if (pu == null)
			throw new ConvertingEnvironmentException("Units are not described for type \"" + typeName
			        + "\", property \"" + propName + "\"");
		DimensionRef dimRef = pu.getDimensionReference();
		// first check if that's an unconvertible dimension
		Dimension dim = unitSystems.getSimpleDimensions().get(dimRef.getDimension());
		if (dim == null) {
			// ok, let's try to get a convertible dimension then
			// (unconvertible dimensions are available for any system,
			// not only for defining system)
			try {
				dim = unitSystems.getUnitSystem(targetSystem == null ? dimRef.getSystem() : targetSystem).getDimension(
				        dimRef.getDimension());
			} catch (ConverterException e) {
				throw new ConvertingEnvironmentException("Cannot find dimension for type \"" + typeName
				        + "\", property \"" + propName + "\"", e);
			}
		}
		return dim;
	}

	/**
	 * Returns base unit system (the one described in XML) for a given property
	 * or throws an exception of property has no unit system assigned.
	 * 
	 * @param to
	 *            Id of an object.
	 * @param propName
	 *            Property name.
	 * @return Base unit system of a given property (never null).
	 * @throws ConvertingEnvironmentException
	 *             if unit system cannot be retrieved (for example, was not
	 *             described).
	 */
	public UnitSystem getBaseSystem(TO<?> to, String propName) {
		// get property units
		PropertyUnits pu = unitResolvers.getUnits(to.getTypeName(), propName);
		if (pu == null)
			throw new ConvertingEnvironmentException("Units are not described for type \"" + to.getTypeName()
			        + "\", property \"" + propName + "\"");
		DimensionRef dimRef = pu.getDimensionReference(to, env);
		try {
			return unitSystems.getUnitSystem(dimRef.getSystem());
		} catch (ConverterException e) {
			throw new ConvertingEnvironmentException("System is not described for type \"" + to.getTypeName()
			        + "\", property \"" + propName + "\"", e);
		}
	}

	/**
	 * Returns base unit system (the one described in XML) for a given property
	 * or throws an exception of property has no unit system assigned.
	 * 
	 * @param typeName
	 *            Type name.
	 * @param propName
	 *            Property name.
	 * @return Base unit system of a given property (never null).
	 * @throws ConvertingEnvironmentException
	 *             if unit system cannot be retrieved (for example, was not
	 *             described).
	 */
	public UnitSystem getBaseSystem(String typeName, String propName) {
		// get property units
		PropertyUnits pu = unitResolvers.getUnits(typeName, propName);
		if (pu == null)
			throw new ConvertingEnvironmentException("Units are not described for type \"" + typeName
			        + "\", property \"" + propName + "\"");
		DimensionRef dimRef = pu.getDimensionReference();
		try {
			return unitSystems.getUnitSystem(dimRef.getSystem());
		} catch (ConverterException e) {
			throw new ConvertingEnvironmentException("System is not described for type \"" + typeName
			        + "\", property \"" + propName + "\"", e);
		}
	}

	/**
	 * Gets the conversion flag. When conversion is disabled all the values are
	 * passed as is.
	 * 
	 * @see getTargetSystem
	 * @return conversion flag.
	 */
	public boolean isConversionEnabled() {
		return conversionEnabled;
	}

	/**
	 * Enable or disable conversion. Setting it affects all proxies created from
	 * this instance.
	 * 
	 * @param enabled
	 *            true to enable conversion and false to turn it off.
	 */
	public void setConversionEnabled(boolean enabled) {
		this.conversionEnabled = enabled;
	}

	/**
	 * Gets name of the unit system this instance converts values to. Null value
	 * means the conversion is not performed (even if conversionEnabled is set
	 * to true). That is, for ConvertingEnvironmentProxy to perform any
	 * conversions both conversionEnabled and targetSystem must be set.
	 * 
	 * @return name of the target system.
	 */
	public String getTargetSystem() {
		return targetSystem;
	}

	/**
	 * Set the target unit system.
	 * 
	 * @param targetSystem
	 *            name of the target unit system. Null value will disable the
	 *            conversion.
	 * @throws ConvertingEnvironmentException
	 *             when target system is not described in the current converters
	 *             map (if latter was set).
	 */
	public void setTargetSystem(String targetSystem) throws ConvertingEnvironmentException {
		if (targetSystem != null && unitSystems != null) {
			try {
				unitSystems.getUnitSystem(targetSystem);
			} catch (ConverterException e) {
				throw new ConvertingEnvironmentException("Cannot find description for \"" + targetSystem
				        + "\" unit system", e);
			}
		}
		this.targetSystem = targetSystem;
	}

	public Object clone() {
		try {
			return super.clone();
		} catch (CloneNotSupportedException e) {
			return null;
		}
	}

}