package com.synapsense.util.unitconverter;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.JAXBException;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;

import org.xml.sax.SAXException;

import com.synapsense.dto.TO;
import com.synapsense.service.Environment;
import com.synapsense.util.ConvertingEnvironmentException;
import com.synapsense.util.typeunits.Units;

public class UnitResolvers implements Serializable {

	private static final long serialVersionUID = 4647575076731344059L;

	/**
	 * Implementations do not need to be serializable but must be present on the
	 * client's classpath.
	 * 
	 * @author Oleg Stepanov
	 * 
	 */
	public static interface UnitResolver {

		DimensionRef getDimension(Environment env, TO<?> to, String propName) throws ConvertingEnvironmentException;

	}

	// maps types to properties and properties to units
	private Map<String, Map<String, PropertyUnits>> typeUnitsMap = new HashMap<String, Map<String, PropertyUnits>>();

	public Map<String, Map<String, PropertyUnits>> getTypeUnitsMap(String typeName, String propName) {
		return typeUnitsMap;
	}

	public PropertyUnits getUnits(String typeName, String propName) {
		Map<String, PropertyUnits> map = typeUnitsMap.get(typeName);
		if (map == null)
			return null;
		return map.get(propName);
	}

	public void addTypePropertyUnits(String typeName, Map<String, PropertyUnits> propertyUnits) {
		typeUnitsMap.put(typeName, propertyUnits);
	}

	public static class DimensionRef implements Serializable {

		private static final long serialVersionUID = 2656042020451650738L;

		private String system;
		private String dimension;

		public DimensionRef(String system, String dimension) {
			this.system = system;
			this.dimension = dimension;
		}

		public String getSystem() {
			return system;
		}

		public String getDimension() {
			return dimension;
		}

		public String toString() {
			StringBuilder sb = new StringBuilder();
			sb.append("{ system: ");
			sb.append(this.system);
			sb.append(", dimension: ");
			sb.append(this.dimension);
			sb.append(" }");
			return sb.toString();
		}
	}

	public static class PropertyUnits implements Serializable {

		private static final long serialVersionUID = 897066115876283215L;

		private String typeName;
		private String propertyName;
		private DimensionRef dimRef;
		private String resolverName;
		private transient UnitResolver resolver;

		public PropertyUnits(String typeName, String propertyName, String system, String dimension) {
			super();
			this.typeName = typeName;
			this.propertyName = propertyName;
			this.dimRef = new DimensionRef(system, dimension);
		}

		public PropertyUnits(String typeName, String propertyName, String resolver) {
			super();
			this.typeName = typeName;
			this.propertyName = propertyName;
			this.dimRef = null;
			this.resolverName = resolver;
		}

		/**
		 * Locates dimension reference (system and dimension name) for stored
		 * property name being given object Id (dynamic variant)
		 * 
		 * @param to
		 *            object id
		 * @param env
		 * @return Dimension coordinates (unit system and dimension name)
		 * @throws ConvertingEnvironmentException
		 */
		public DimensionRef getDimensionReference(TO<?> to, Environment env) throws ConvertingEnvironmentException {
			return dimRef != null ? dimRef : getResolver().getDimension(env, to, propertyName);
		}

		/**
		 * Locates dimension reference (system and dimension name) for stored
		 * property (static variant)
		 * 
		 * @return
		 * @throws ConvertingEnvironmentException
		 */
		public DimensionRef getDimensionReference() throws ConvertingEnvironmentException {
			if (dimRef == null) {
				// dimRef set to null means this property's units
				// can be resolved only dynamically
				throw new ConvertingEnvironmentException("Type \"" + typeName + "\", property \"" + propertyName
				        + "\": property with resolver specified cannot be accessed statically");
			}
			return dimRef;
		}

		public String getTypeName() {
			return typeName;
		}

		public String getPropertyName() {
			return propertyName;
		}

		private UnitResolver getResolver() throws ConvertingEnvironmentException {
			if (resolver != null)
				return resolver;
			try {
				resolver = (UnitResolver) Thread.currentThread().getContextClassLoader().loadClass(resolverName)
				        .getConstructor().newInstance();
			} catch (Exception e) {
				throw new ConvertingEnvironmentException("Cannot load resolver " + resolver, e);
			}
			if (resolver == null) {
				throw new ConvertingEnvironmentException("Cannot load resolver " + resolver);
			}
			return resolver;
		}

		public String toString() {
			StringBuilder sb = new StringBuilder();
			sb.append("{ typeName: ");
			sb.append(this.typeName);
			sb.append(", propertyName: ");
			sb.append(this.propertyName);
			sb.append(", resolverName: ");
			sb.append(this.resolverName);
			sb.append(", dimRef: ");
			sb.append(this.dimRef == null ? "NULL" : this.dimRef.toString());
			sb.append(" }");
			return sb.toString();
		}

	}

	public void loadFromXml(File file) throws JAXBException, IOException, SAXException, ConverterConfigurationException {
		XmlConfigUtils utils = new XmlConfigUtils(loadSchemas(), Units.class);
		Units units = utils.read(file, Units.class);
		initResolvers(units);
	}

	public void loadFromXml(Reader reader) throws JAXBException, IOException, SAXException,
	        ConverterConfigurationException {
		XmlConfigUtils utils = new XmlConfigUtils(loadSchemas(), Units.class);
		Units units = utils.read(reader, Units.class);
		initResolvers(units);
	}

	public void loadFromXml(String fileName) throws JAXBException, IOException, SAXException,
	        ConverterConfigurationException {
		XmlConfigUtils utils = new XmlConfigUtils(loadSchemas(), Units.class);
		Units units = utils.read(fileName, Units.class);
		initResolvers(units);
	}

	private void initResolvers(Units units) throws ConverterConfigurationException {
		for (Units.Type type : units.getType()) {
			String typeName = type.getName();
			if (typeName == null || typeName.isEmpty())
				throw new ConverterConfigurationException("Empty type names are not allowed");
			Map<String, PropertyUnits> properties = new HashMap<String, PropertyUnits>();
			typeUnitsMap.put(typeName, properties);
			for (Units.Type.Property prop : type.getProperty()) {
				String propName = prop.getName();
				if (propName == null || propName.isEmpty()) {
					throw new ConverterConfigurationException("Type \"" + typeName
					        + "\": empty property names are not allowed");
				}
				// either resolver or both system/dimension must be specified
				String resolver = prop.getResolver();
				if (resolver == null || resolver.isEmpty()) {
					// use provided system and dimension
					String system = prop.getSystem();
					String dimension = prop.getDimension();
					if (system == null || system.isEmpty() || dimension == null || dimension.isEmpty()) {
						throw new ConverterConfigurationException("Type \"" + typeName + "\", property \"" + propName
						        + "\": either resolver or both system/dimension must be specified");
					}
					properties.put(propName, new PropertyUnits(typeName, propName, system, dimension));
				} else {
					properties.put(propName, new PropertyUnits(typeName, propName, resolver));
				}
			}
		}
	}

	private static Source[] loadSchemas() throws ConverterConfigurationException {
		InputStream typeUnitsSchema = UnitResolvers.class.getClassLoader().getResourceAsStream("schemas/TypeUnits.xsd");
		if (typeUnitsSchema == null) {
			throw new ConverterConfigurationException("File schemas/TypeUnits.xsd is not available in classpath");
		}
		Source[] res = new Source[1];
		res[0] = new StreamSource(typeUnitsSchema, "TypeUnits.xsd");
		return res;
	}

}
