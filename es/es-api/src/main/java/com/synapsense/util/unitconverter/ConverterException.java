package com.synapsense.util.unitconverter;

/**
 * Base class for checked exceptions thrown by unit conversion utilities.
 * 
 * @author Oleg Stepanov
 * 
 */
public class ConverterException extends Exception {

	private static final long serialVersionUID = -4696611743724367722L;

	public ConverterException() {
		super();
	}

	public ConverterException(String message) {
		super(message);
	}

	public ConverterException(Throwable cause) {
		super(cause);
	}

	public ConverterException(String message, Throwable cause) {
		super(message, cause);
	}

}
