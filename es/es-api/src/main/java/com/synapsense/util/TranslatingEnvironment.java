package com.synapsense.util;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.synapsense.dto.CollectionTO;
import com.synapsense.dto.EnvObjectBundle;
import com.synapsense.dto.EnvObjectHistory;
import com.synapsense.dto.ObjectType;
import com.synapsense.dto.ObjectTypeMatchMode;
import com.synapsense.dto.PropertyDescr;
import com.synapsense.dto.Relation;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOBundle;
import com.synapsense.dto.Tag;
import com.synapsense.dto.TagDescriptor;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.EnvException;
import com.synapsense.exception.IllegalInputParameterException;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.exception.PropertyNotFoundException;
import com.synapsense.exception.ServerConfigurationException;
import com.synapsense.exception.TagNotFoundException;
import com.synapsense.exception.UnableToConvertPropertyException;
import com.synapsense.exception.UnableToConvertTagException;
import com.synapsense.exception.UnsupportedRelationException;
import com.synapsense.search.QueryDescription;
import com.synapsense.service.Environment;

public class TranslatingEnvironment extends Translator implements Environment {

	private Environment impl;

	// TODO: rework, move to config
	private static Set<String> whiteList = new HashSet<String>();
	private static Map<String, List<String>> blackList = new HashMap<String, List<String>>();
	
	static {
		List<String> phaseProps = new LinkedList<String>();
		phaseProps.add("name");
		blackList.put("PHASE", phaseProps);

		whiteList.add("acknowledgement");
		whiteList.add("body");
		whiteList.add("desc");
		whiteList.add("descr");
		whiteList.add("description");
		whiteList.add("fullMessage");
		whiteList.add("header");
		whiteList.add("loc");
		whiteList.add("location");
		whiteList.add("message");
		whiteList.add("name");
		whiteList.add("notes");
		whiteList.add("platformName");
		whiteList.add("queryDesc");
		whiteList.add("period");
	}
	
	private static boolean isTranslatable(String typeName, String propertyName) {
		List<String> propList = blackList.get(typeName);
		
		if (propList != null) {
			if (propList.contains(propertyName)) {
				return false;
			}
		}
		
		return whiteList.contains(propertyName);
	}

	public TranslatingEnvironment(Environment env) throws NullPointerException {
		setEnvironment(env);
	}

	public void setEnvironment(Environment env) throws NullPointerException {
		if (env == null) {
			throw new NullPointerException("env in null");
		}
		impl = env;
	}
	
	public Environment getEnvironment() {
		return impl;
	}

	@Override
	public ObjectType createObjectType(String name) throws EnvException {
		return impl.createObjectType(name);
	}

	@Override
	public ObjectType[] getObjectTypes(String[] names) {
		return impl.getObjectTypes(names);
	}

	@Override
	public ObjectType getObjectType(String name) {
		return impl.getObjectType(name);
	}

	@Override
	public String[] getObjectTypes() {
		return impl.getObjectTypes();
	}

	@Override
	public void updateObjectType(ObjectType objType) throws EnvException {
		impl.updateObjectType(objType);
	}

	@Override
	public void deleteObjectType(String typeName) throws EnvException {
		impl.deleteObjectType(typeName);
	}

	@Override
	public TO<?> createObject(String typeName) throws EnvException {
		return impl.createObject(typeName);
	}

	@Override
	public TO<?> createObject(String typeName, ValueTO[] propertyValues) throws EnvException {
		return impl.createObject(typeName, propertyValues);
	}

	@Override
	public TOBundle createObjects(EnvObjectBundle objectsToCreate) throws EnvException {
		return impl.createObjects(objectsToCreate);
	}

	@Override
	public Collection<TO<?>> getObjectsByType(String typeName) {
		return impl.getObjectsByType(typeName);
	}

	@Override
	public Collection<TO<?>> getObjectsByType(String typeName, ObjectTypeMatchMode matchMode) {
		return impl.getObjectsByType(typeName, matchMode);
	}

	@Override
	public Collection<TO<?>> getObjects(String typeName, ValueTO[] propertyValues) {
		return impl.getObjects(typeName, propertyValues);
	}

	@Override
	public Collection<TO<?>> getObjects(String typeName, ObjectTypeMatchMode matchMode, ValueTO[] propertyValues) {
		return impl.getObjects(typeName, matchMode, propertyValues);
	}

	@Override
	public Collection<TO<?>> getObjects(QueryDescription queryDescr) {
		return impl.getObjects(queryDescr);
	}

	@Override
	public void deleteObject(TO<?> objId) throws ObjectNotFoundException {
		impl.deleteObject(objId);
	}

	@Override
	public void deleteObject(TO<?> objId, int level) throws ObjectNotFoundException {
		impl.deleteObject(objId, level);
	}

	@Override
	public Collection<ValueTO> getAllPropertiesValues(TO<?> objId) throws ObjectNotFoundException {
		Collection<ValueTO> res = impl.getAllPropertiesValues(objId);
		String typeName = objId.getTypeName();
		for (ValueTO v : res) {
			if (isTranslatable(typeName, v.getPropertyName())) {
				v.setValue(translate(v.getValue()));
			}
		}
		return res;
	}

	@Override
	public void setAllPropertiesValues(TO<?> objId, ValueTO[] values) throws ObjectNotFoundException,
	        PropertyNotFoundException, UnableToConvertPropertyException {
		impl.setAllPropertiesValues(objId, values);
	}

	@Override
	public void setAllPropertiesValues(TO<?> objId, Collection<ValueTO> values, Collection<Tag> tags)
	        throws EnvException {
		impl.setAllPropertiesValues(objId, values, tags);
	}

	@Override
	public <RETURN_TYPE> RETURN_TYPE getPropertyValue(TO<?> objId, String propName, Class<RETURN_TYPE> clazz)
	        throws ObjectNotFoundException, PropertyNotFoundException, UnableToConvertPropertyException {
		RETURN_TYPE res = impl.getPropertyValue(objId, propName, clazz);
		if (isTranslatable(objId.getTypeName(), propName)) {
			res = (RETURN_TYPE) translate(res);
		}
		return res;
	}

	@Override
	public <REQUESTED_TYPE> void setPropertyValue(TO<?> objId, String propName, REQUESTED_TYPE value)
	        throws ObjectNotFoundException, PropertyNotFoundException, UnableToConvertPropertyException {
		impl.setPropertyValue(objId, propName, value);
	}

	@Override
	public <REQUESTED_TYPE> void addPropertyValue(TO<?> objId, String propName, REQUESTED_TYPE value)
	        throws ObjectNotFoundException, PropertyNotFoundException, UnableToConvertPropertyException {
		impl.addPropertyValue(objId, propName, value);
	}

	@Override
	public <REQUESTED_TYPE> void removePropertyValue(TO<?> objId, String propName, REQUESTED_TYPE value)
	        throws ObjectNotFoundException, PropertyNotFoundException, UnableToConvertPropertyException {
		impl.removePropertyValue(objId, propName, value);
	}

	
	@Override
	public <RETURN_TYPE> Collection<ValueTO> getHistory(TO<?> objId, String propName, Class<RETURN_TYPE> clazz)
	        throws ObjectNotFoundException, UnableToConvertPropertyException, PropertyNotFoundException {
		return impl.getHistory(objId, propName, clazz);
	}

	@Override
	public <RETURN_TYPE> Collection<ValueTO> getHistory(TO<?> objId, String propName, int size, int start,
	        Class<RETURN_TYPE> clazz) throws ObjectNotFoundException, PropertyNotFoundException,
	        UnableToConvertPropertyException {
		return impl.getHistory(objId, propName, size, start, clazz);
	}

	@Override
	public <RETURN_TYPE> Collection<ValueTO> getHistory(TO<?> objId, String propName, Date start, Date end,
	        Class<RETURN_TYPE> clazz) throws ObjectNotFoundException, PropertyNotFoundException,
	        UnableToConvertPropertyException {
		return impl.getHistory(objId, propName, start, end, clazz);
	}

	@Override
	public <RETURN_TYPE> Collection<ValueTO> getHistory(TO<?> objId, String propName, Date start, int numberPoints,
	        Class<RETURN_TYPE> clazz) throws ObjectNotFoundException, PropertyNotFoundException,
	        UnableToConvertPropertyException {
		return impl.getHistory(objId, propName, start, numberPoints, clazz);
	}

	@Override
	public EnvObjectHistory getHistory(TO<?> objId, Collection<String> propNames, Date start, Date end)
	        throws ObjectNotFoundException, PropertyNotFoundException {
		return impl.getHistory(objId, propNames, start, end);
	}

	@Override
	public Integer getHistorySize(TO<?> objId, String propName) throws ObjectNotFoundException,
	        PropertyNotFoundException {
		return impl.getHistorySize(objId, propName);
	}

	@Override
	public void setRelation(TO<?> parentId, TO<?> childId) throws ObjectNotFoundException, UnsupportedRelationException {
		impl.setRelation(parentId, childId);
	}

	@Override
	public void setRelation(List<Relation> relations) throws ObjectNotFoundException, UnsupportedRelationException {
		impl.setRelation(relations);
	}

	@Override
	public void removeRelation(TO<?> parentId, TO<?> childId) throws ObjectNotFoundException {
		impl.removeRelation(parentId, childId);
	}

	@Override
	public void removeRelation(List<Relation> relations) throws ObjectNotFoundException {
		impl.removeRelation(relations);
	}

	@Override
	public Collection<TO<?>> getParents(TO<?> objId, String typeName) throws ObjectNotFoundException {
		return impl.getParents(objId, typeName);
	}

	@Override
	public Collection<TO<?>> getParents(TO<?> objId) throws ObjectNotFoundException {
		return impl.getParents(objId);
	}

	@Override
	public Collection<TO<?>> getChildren(TO<?> objId) throws ObjectNotFoundException {
		return impl.getChildren(objId);
	}

	@Override
	public Collection<TO<?>> getChildren(TO<?> objId, String typeName) throws ObjectNotFoundException {
		return impl.getChildren(objId, typeName);
	}

	@Override
	public Collection<CollectionTO> getChildren(Collection<TO<?>> objIds, String typeName) {
		return impl.getChildren(objIds, typeName);
	}

	@Override
	public Collection<CollectionTO> getChildren(Collection<TO<?>> objIds) {
		return impl.getChildren(objIds);
	}

	@Override
	public long getPropertyTimestamp(TO<?> objId, String propName) throws ObjectNotFoundException,
	        PropertyNotFoundException {
		return impl.getPropertyTimestamp(objId, propName);
	}

	@Override
	public PropertyDescr getPropertyDescriptor(String typeName, String propName) throws ObjectNotFoundException,
	        PropertyNotFoundException {
		return impl.getPropertyDescriptor(typeName, propName);
	}

	@Override
	public Collection<CollectionTO> getPropertyValue(Collection<TO<?>> objIds, String[] propNames) {
		return (Collection<CollectionTO>) translate(impl.getPropertyValue(objIds, propNames));
	}

	@Override
	public Collection<CollectionTO> getAllPropertiesValues(Collection<TO<?>> objIds) {
		return (Collection<CollectionTO>) translate(impl.getAllPropertiesValues(objIds));
	}

	@Override
	public Collection<TO<?>> getRelatedObjects(TO<?> objId, String objectType, boolean isChild) {
		return impl.getRelatedObjects(objId, objectType, isChild);
	}

	@Override
	public <RETURN_TYPE> RETURN_TYPE getPropertyValue(String typeName, String propName, Class<RETURN_TYPE> clazz)
	        throws ObjectNotFoundException, PropertyNotFoundException, UnableToConvertPropertyException {
		RETURN_TYPE res = impl.getPropertyValue(typeName, propName, clazz);
		if (isTranslatable(typeName, propName)) {
			return (RETURN_TYPE) translate(res);
		} else {
			return res;
		}
	}

	@Override
	public <REQUESTED_TYPE> void setPropertyValue(String typeName, String propName, REQUESTED_TYPE value)
	        throws ObjectNotFoundException, PropertyNotFoundException, UnableToConvertPropertyException {
		impl.setPropertyValue(typeName, propName, value);
	}

	@Override
	public void configurationComplete() throws ServerConfigurationException {
		impl.configurationComplete();
	}

	@Override
	public void configurationStart() throws ServerConfigurationException {
		impl.configurationStart();
	}

	@Override
	public void setAllPropertiesValues(TO<?> objId, ValueTO[] values, boolean persist) throws ObjectNotFoundException,
	        PropertyNotFoundException, UnableToConvertPropertyException {
		impl.setAllPropertiesValues(objId, values, persist);
	}

	@Override
	public <REQUESTED_TYPE> void setPropertyValue(TO<?> objId, String propName, REQUESTED_TYPE value, boolean persist)
	        throws PropertyNotFoundException, ObjectNotFoundException, UnableToConvertPropertyException {
		impl.setPropertyValue(objId, propName, value, persist);
	}

	@Override
	public boolean exists(TO<?> objId) throws IllegalInputParameterException {
		return impl.exists(objId);
	}

	@Override
	public TagDescriptor getTagDescriptor(String typeName, String propName, String tagName)
	        throws ObjectNotFoundException, PropertyNotFoundException, TagNotFoundException {
		return impl.getTagDescriptor(typeName, propName, tagName);
	}

	@Override
	public <RETURN_TYPE> RETURN_TYPE getTagValue(TO<?> objId, String propName, String tagName, Class<RETURN_TYPE> clazz)
	        throws ObjectNotFoundException, PropertyNotFoundException, TagNotFoundException,
	        UnableToConvertTagException {
		return impl.getTagValue(objId, propName, tagName, clazz);
	}

	@Override
	public Collection<Tag> getAllTags(TO<?> objId, String propName) throws ObjectNotFoundException,
	        PropertyNotFoundException {
		return impl.getAllTags(objId, propName);
	}

	@Override
	public Collection<Tag> getAllTags(TO<?> objId) throws ObjectNotFoundException {
		return impl.getAllTags(objId);
	}

	@Override
	public Map<TO<?>, Collection<Tag>> getAllTags(String typeName) {
		return impl.getAllTags(typeName);
	}

	@Override
	public Map<TO<?>, Map<String, Collection<Tag>>> getTags(Collection<TO<?>> objIds, List<String> propertyNames,
	        List<String> tagNames) {
		return impl.getTags(objIds, propertyNames, tagNames);
	}

	@Override
	public void setTagValue(TO<?> objId, String propName, String tagName, Object value)
	        throws PropertyNotFoundException, ObjectNotFoundException, TagNotFoundException,
	        UnableToConvertTagException {
		impl.setTagValue(objId, propName, tagName, value);
	}

	@Override
	public void setAllTagsValues(TO<?> objId, Collection<Tag> tagsToSet) throws PropertyNotFoundException,
	        ObjectNotFoundException, TagNotFoundException, UnableToConvertTagException {
		impl.setAllTagsValues(objId, tagsToSet);
	}

	private ValueTO translate(ValueTO valueTo) {
		Object value = valueTo.getValue();
		if (value != null && value instanceof String) {
			valueTo.setValue(translate((String) value));
		}
		return valueTo;
	}

	private CollectionTO translate(CollectionTO colTo) {
		String typeName = colTo.getObjId().getTypeName();
		for (ValueTO valueTo : colTo.getPropValues()) {
			if (isTranslatable(typeName, valueTo.getPropertyName())) {
				translate(valueTo);
			}
		}
		return colTo;
	}

	private Object translate(Object obj) {

		if (obj instanceof String) {
			return translate((String) obj);
		}

		if (obj instanceof ValueTO) {
			return translate((ValueTO) obj);
		}

		if (obj instanceof CollectionTO) {
			return translate((CollectionTO) obj);
		}

		if (obj instanceof Collection) {
			for (Object o : (Collection<?>) obj) {
				translate(o);
			}
			return obj;
		}

		return obj;
	}

	

}
