package com.synapsense.util.unitconverter;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class UnitSystem implements Serializable {
	private static final long serialVersionUID = -3963765624409981632L;
	private final String name;
	private final Map<String, Dimension> dimensions;

	public UnitSystem(String name) {
		this.name = name;
		this.dimensions = new HashMap<String, Dimension>();
	}

	public String getName() {
		return name;
	}

	public Dimension getDimension(String dimName) throws ConverterException {
		Dimension res = dimensions.get(dimName);
		if (res == null) {
			throw new ConverterException("Dimension \"" + dimName + "\" is not described for unit system \"" + name
			        + "\"");
		}
		return res;
	}

	public Map<String, Dimension> getDimensionsMap() {
		return dimensions;
	}

}
