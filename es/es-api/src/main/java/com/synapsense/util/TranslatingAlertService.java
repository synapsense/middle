package com.synapsense.util;

import java.util.Collection;
import java.util.Date;

import com.synapsense.dto.Alert;
import com.synapsense.dto.AlertFilter;
import com.synapsense.dto.AlertPriority;
import com.synapsense.dto.AlertType;
import com.synapsense.dto.MessageTemplate;
import com.synapsense.dto.MessageType;
import com.synapsense.dto.TO;
import com.synapsense.exception.AlertTypeAlreadyExistsException;
import com.synapsense.exception.NoSuchAlertPriorityException;
import com.synapsense.exception.NoSuchAlertTypeException;
import com.synapsense.service.AlertService;

public class TranslatingAlertService extends Translator implements AlertService {
	AlertService impl;

	public TranslatingAlertService(AlertService service) {
		setAlertService(service);
	}

	public void setAlertService(AlertService service) throws NullPointerException {
		if (service == null) {
			throw new NullPointerException("AlertService in null");
		}
		impl = service;
	}

	public AlertService getAlertService() {
		return impl;
	}

	@Override
	public void acknowledgeAlert(Integer arg0, String arg1) {
		impl.acknowledgeAlert(arg0, arg1);
	}

	@Override
	public void addActionDestination(String arg0, String arg1, MessageType arg2, String arg3)
	        throws NoSuchAlertTypeException {
		impl.addActionDestination(arg0, arg1, arg2, arg3);
	}

	@Override
	public void createAlertType(AlertType arg0) throws AlertTypeAlreadyExistsException {
		impl.createAlertType(arg0);
	}

	@Override
	public void deleteAlertType(String arg0) {
		impl.deleteAlertType(arg0);
	}

	@Override
	public void deleteAlerts(TO<?> arg0, boolean arg1) {
		impl.deleteAlerts(arg0, arg1);
	}

	@Override
	public void deleteAlertsHistoryOlderThan(Date arg0) {
		impl.deleteAlertsHistoryOlderThan(arg0);
	}

	@Override
	public void deleteAllAlerts() {
		impl.deleteAllAlerts();
	}

	@Override
	public void dismissAlert(Alert arg0, String arg1) {
		impl.dismissAlert(arg0, arg1);
	}

	@Override
	public void dismissAlert(Integer arg0, String arg1) {
		impl.dismissAlert(arg0, arg1);
	}

	@Override
	public void dismissAlerts(TO<?> arg0, boolean arg1, String arg2) {
		impl.dismissAlerts(arg0, arg1, arg2);
	}

	@Override
	public String getActionDestination(String arg0, String arg1, MessageType arg2, String arg3)
	        throws NoSuchAlertTypeException {
		return impl.getActionDestination(arg0, arg1, arg2, arg3);
	}

	@Override
	public Collection<Alert> getActiveAlerts(Collection<TO<?>> arg0, boolean arg1) {
		return translateAlerts(impl.getActiveAlerts(arg0, arg1));
	}

	@Override
	public Collection<Alert> getActiveAlerts(TO<?> arg0, boolean arg1) {
		return translateAlerts(impl.getActiveAlerts(arg0, arg1));
	}

	@Override
	public Alert getAlert(Integer arg0) {
		return translate(impl.getAlert(arg0));
	}

	@Override
	public Collection<String> getAlertPrioritiesNames() {
		return impl.getAlertPrioritiesNames();
	}

	@Override
	public AlertPriority getAlertPriority(String arg0) {
		return impl.getAlertPriority(arg0);
	}

	@Override
	public Collection<AlertType> getAlertTypes(String[] arg0) {
		return translateAlertTypes(impl.getAlertTypes(arg0));
	}

	@Override
	public Collection<Alert> getAlerts(AlertFilter arg0) {
		return translateAlerts(impl.getAlerts(arg0));
	}

	@Override
	public Collection<Alert> getAlerts(Integer[] arg0) {
		return translateAlerts(impl.getAlerts(arg0));
	}

	@Override
	public Collection<Alert> getAlertsHistory(TO<?> arg0, Date arg1, Date arg2, boolean arg3) {
		return translateAlerts(impl.getAlertsHistory(arg0, arg1, arg2, arg3));
	}

	@Override
	public Collection<AlertType> getAllAlertTypes() {
		return translateAlertTypes(impl.getAllAlertTypes());
	}

	@Override
	public boolean hasAlerts(TO<?> arg0, boolean arg1) {
		return impl.hasAlerts(arg0, arg1);
	}

	@Override
	public void raiseAlert(Alert arg0) {
		impl.raiseAlert(arg0);
	}

	@Override
	public void removeActionDestination(String arg0, String arg1, MessageType arg2, String arg3)
	        throws NoSuchAlertTypeException {
		impl.removeActionDestination(arg0, arg1, arg2, arg3);
	}

	@Override
	public void removeTemplate(String arg0, MessageType arg1) throws NoSuchAlertTypeException {
		impl.removeTemplate(arg0, arg1);
	}

	@Override
	public void resolveAlert(Integer arg0, String arg1) {
		impl.resolveAlert(arg0, arg1);
	}

	@Override
	public void setTemplate(String arg0, MessageTemplate arg1) throws NoSuchAlertTypeException {
		impl.setTemplate(arg0, arg1);
	}

	@Override
	public void updateAlertPriority(String arg0, AlertPriority arg1) throws NoSuchAlertPriorityException {
		impl.updateAlertPriority(arg0, arg1);
	}

	@Override
	public void updateAlertType(String arg0, AlertType arg1) throws NoSuchAlertTypeException {
		impl.updateAlertType(arg0, arg1);
	}

	private Collection<Alert> translateAlerts(Collection<Alert> alerts) {
		for (Alert alert : alerts) {
			translate(alert);
		}
		return alerts;
	}

	private Alert translate(Alert alert) {
		alert.setName(translate(alert.getName()));
		alert.setFullMessage(translate(alert.getFullMessage()));
		return alert;
	}

	private Collection<AlertType> translateAlertTypes(Collection<AlertType> alertTypes) {
		for (AlertType alertType : alertTypes) {
			alertType.setDescription(translate(alertType.getDescription()));
			alertType.setDisplayName(translate(alertType.getDisplayName()));
		}
		return alertTypes;
	}

	@Override
	public int getNumActiveAlerts(TO<?> arg0, boolean arg1) {
		return impl.getNumActiveAlerts(arg0, arg1);
	}
	
	@Override
	public int getNumAlerts(AlertFilter filter) {
		return impl.getNumAlerts(filter);
	}

	@Override
	public AlertType getAlertType(String arg0) {
		AlertType alertType = impl.getAlertType(arg0);
		alertType.setDescription(translate(alertType.getDescription()));
		alertType.setDisplayName(translate(alertType.getDisplayName()));
		return alertType;
	}

	@Override
	public Collection<AlertType> getAlertTypesByDestination(String arg0, MessageType arg1) {
		return translateAlertTypes(impl.getAlertTypesByDestination(arg0, arg1));
	}

	@Override
	public Collection<String> getActionDestinations(String alertTypeName, String tierName, MessageType messageType)
	        throws NoSuchAlertTypeException {
		return impl.getActionDestinations(alertTypeName, tierName, messageType);
	}
}
