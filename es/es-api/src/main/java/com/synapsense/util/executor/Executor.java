package com.synapsense.util.executor;

import java.util.Collection;

public interface Executor {

	void doAction(Action action, boolean await) throws ProcessingException;

	void doAction(Collection<Action> actions, boolean await) throws ProcessingException;

	int threadsNum();

	void shutdownAwait();

	void shutdown();

}