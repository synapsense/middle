package com.synapsense.util.unitconverter;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class SystemConverter implements Serializable {

	private static final long serialVersionUID = -1266496297168479943L;

	private final UnitSystem baseSystem;
	private final UnitSystem targetSystem;
	private final Map<String, UnitConverter> converters;
	private final Set<String> simpleDimensions;
	private final Converter converterStub;

	public SystemConverter(UnitSystem baseSystem, UnitSystem targetSystem, Set<String> simpleDimensions) {
		super();
		this.baseSystem = baseSystem;
		this.targetSystem = targetSystem;
		this.converters = new HashMap<String, UnitConverter>();
		this.simpleDimensions = simpleDimensions;
		this.converterStub = new ConverterStub();
	}

	public UnitConverter getConverter(final String dimension) {
		if (simpleDimensions.contains(dimension))
			return new UnitConverter(baseSystem, targetSystem, dimension, converterStub);
		return converters.get(dimension);
	}

	public UnitSystem getBaseSystem() {
		return baseSystem;
	}

	public UnitSystem getTargetSystem() {
		return targetSystem;
	}

	public String getBaseSystemName() {
		return baseSystem.getName();
	}

	public String getTargetSystemName() {
		return targetSystem.getName();
	}

	// Method has package visibility allowing access only for initialization
	// purposes
	Map<String, UnitConverter> getConverters() {
		return converters;
	}

	private class ConverterStub implements Converter, Serializable {

		private static final long serialVersionUID = -5939136768511316528L;

		@Override
		public double convert(double value) {
			return value;
		}

	}

}
