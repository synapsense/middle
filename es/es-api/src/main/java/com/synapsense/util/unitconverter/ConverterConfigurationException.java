package com.synapsense.util.unitconverter;

/**
 * Class of exceptions thrown when unit converters configuration is wrong.
 * 
 * @author Oleg Stepanov
 * 
 */
public class ConverterConfigurationException extends ConverterException {

	private static final long serialVersionUID = 1944513540876968558L;

	public ConverterConfigurationException() {
		super();
	}

	public ConverterConfigurationException(String message) {
		super(message);
	}

	public ConverterConfigurationException(Throwable cause) {
		super(cause);
	}

	public ConverterConfigurationException(String message, Throwable cause) {
		super(message, cause);
	}

}
