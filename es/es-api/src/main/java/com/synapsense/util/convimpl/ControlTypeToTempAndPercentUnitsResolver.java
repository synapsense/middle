package com.synapsense.util.convimpl;

import java.util.concurrent.ConcurrentHashMap;

import com.synapsense.dto.TO;
import com.synapsense.exception.EnvException;
import com.synapsense.service.Environment;
import com.synapsense.util.ConvertingEnvironmentException;
import com.synapsense.util.unitconverter.UnitResolvers.UnitResolver;
import com.synapsense.util.unitconverter.UnitResolvers.DimensionRef;

/**
 * @author apahunov
 * 
 *         This class returns dimension name base on "controltype" property
 *         "temperature" is mapped to "200" "pressure" is mapped to "percent"
 */
public class ControlTypeToTempAndPercentUnitsResolver implements UnitResolver {

	private static ConcurrentHashMap<TO<?>, DimensionRef> dimensionCache = new ConcurrentHashMap<TO<?>, DimensionRef>();

	private static long lastCacheUpdate = System.currentTimeMillis();

	// dimensions cache expiration interval
	private static final long CACHE_EXPIRE_INTERVAL = 5 * 60 * 1000;

	@Override
	public DimensionRef getDimension(Environment env, TO<?> to, String propName) throws ConvertingEnvironmentException {
		checkCacheExpiration();
		DimensionRef result = dimensionCache.get(to);
		if (result != null)
			return result;

		String controlType;
		try {
			controlType = env.getPropertyValue(to, "resource", String.class);
		} catch (EnvException e) {
			throw new ConvertingEnvironmentException("Cannot request controltype", e);
		}

		// CRAH algo parameters are temperatures( dimention id in
		// unitresolvers.xml - "200")
		// VFD algo parameters are percents( dimention id in unitresolvers.xml -
		// "percent")

		String dim = "percent";
		if("temperature".equals(controlType)){
			dim= "200";
		} 
		result = new DimensionRef("Server", dim);
		
		dimensionCache.put(to, result);
		return result;

	}

	public static void setDimensionRef(TO<?> to, DimensionRef dimRef) {
		checkCacheExpiration();
		dimensionCache.put(to, dimRef);
	}

	private static void checkCacheExpiration() {
		if ((System.currentTimeMillis() - lastCacheUpdate) > CACHE_EXPIRE_INTERVAL) {
			dimensionCache.clear();
			lastCacheUpdate = System.currentTimeMillis();
		}
	}

}
