package com.synapsense.util.convimpl;

import java.io.Serializable;
import java.util.concurrent.ConcurrentHashMap;

import com.synapsense.dto.TO;
import com.synapsense.exception.EnvException;
import com.synapsense.service.Environment;
import com.synapsense.util.ConvertingEnvironmentException;
import com.synapsense.util.unitconverter.UnitResolvers;
import com.synapsense.util.unitconverter.UnitResolvers.DimensionRef;

public class DataclassUnitsResolver implements UnitResolvers.UnitResolver, Serializable {

	private static final long serialVersionUID = 3940103219366593643L;

	private static ConcurrentHashMap<TO<?>, DimensionRef> dimensionCache = new ConcurrentHashMap<TO<?>, DimensionRef>();

	private static long lastCacheUpdate = System.currentTimeMillis();

	// dimensions cache expiration interval
	private static final long CACHE_EXPIRE_INTERVAL = 5 * 60 * 1000;

	@Override
	public DimensionRef getDimension(Environment env, TO<?> to, String propName) throws ConvertingEnvironmentException {
		checkCacheExpiration();
		DimensionRef result = dimensionCache.get(to);
		if (result != null)
			return result;

		Integer dataclass;
		try {
			dataclass = env.getPropertyValue(to, "dataclass", Integer.class);
		} catch (EnvException e) {
			throw new ConvertingEnvironmentException("Cannot request dataclass", e);
		}

		result = new DimensionRef("Server", dataclass.toString());
		dimensionCache.put(to, result);
		return result;

	}

	public static void setDimensionRef(TO<?> to, DimensionRef dimRef) {
		checkCacheExpiration();
		dimensionCache.put(to, dimRef);
	}

	private static void checkCacheExpiration() {
		if ((System.currentTimeMillis() - lastCacheUpdate) > CACHE_EXPIRE_INTERVAL) {
			dimensionCache.clear();
			lastCacheUpdate = System.currentTimeMillis();
		}
	}

}
