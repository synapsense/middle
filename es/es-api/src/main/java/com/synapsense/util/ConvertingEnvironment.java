package com.synapsense.util;

import com.synapsense.dto.TO;
import com.synapsense.service.Environment;
import com.synapsense.util.unitconverter.Dimension;

public interface ConvertingEnvironment extends Environment {

	/**
	 * Returns dimension description for a given object property. Returned
	 * dimension is in the current target system or in the base system if target
	 * system is not specified
	 * 
	 * @param to
	 *            object id
	 * @param propName
	 *            property name
	 * @return Descriptor of dimension for specified property or null if
	 *         dimension cannot be found.
	 */
	Dimension getDimension(TO<?> to, String propName);

	/**
	 * Returns dimension description for a given type static property. Returned
	 * dimension is in the current target system or in the base system if target
	 * system is not specified
	 * 
	 * @param typeName
	 *            type name
	 * @param propName
	 *            property name
	 * @return Descriptor of dimension for specified property or null if
	 *         dimension cannot be found.
	 */
	Dimension getDimension(String typeName, String propName);

	/**
	 * Gets name of the unit system this instance converts values to. Null value
	 * means the conversion is not performed (even if conversionEnabled is set
	 * to true). That is, for ConvertingEnvironment to perform any conversions
	 * both conversionEnabled and targetSystem must be set.
	 * 
	 * @return name of the target system.
	 */
	String getTargetSystem();

	/**
	 * Set the target unit system.
	 * 
	 * @param targetSystem
	 *            name of the target unit system. Null value will disable the
	 *            conversion.
	 * @throws ConvertingEnvironmentException
	 *             when target system is not described in the current converters
	 *             map (if latter was set).
	 */
	void setTargetSystem(String targetSystem) throws ConvertingEnvironmentException;

	/**
	 * Enables or disable conversion.
	 * 
	 * @return true - conversion is enabled, false otherwise
	 */
	boolean isConversionEnabled();

	/**
	 * Enables or disable conversion.
	 * 
	 * @param enabled
	 *            true - enables conversion, false - disables
	 */
	void setConversionEnabled(boolean enabled);

	/**
	 * Return value converted from the current system into base system in which
	 * units are stored in the database
	 * 
	 * @param objId
	 *            ID of object
	 * @param propName
	 *            Property name
	 * @param value
	 *            value to be converted
	 * @return converted value
	 */
	public Double convertValueToBaseSystem(TO<?> objId, String propName, Double value);

	/**
	 * Return value converted from the current system into base system in which
	 * units are stored in the database
	 * 
	 * @param typeName
	 *            type of object
	 * @param propName
	 *            Property name
	 * @param value
	 *            value to be converted
	 * @return converted value
	 */
	public Double convertValueToBaseSystem(String typeName, String propName, Double value);
}
