package com.synapsense.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.synapsense.service.impl.activitylog.ActivityLogService;
import com.synapsense.service.impl.activitylog.ActivityRecord;
import com.synapsense.service.impl.activitylog.ActivityRecordFilter;

public class TranslatingActivityLogService extends Translator implements ActivityLogService {
	private ActivityLogService impl;

	public TranslatingActivityLogService(ActivityLogService service) throws NullPointerException {
	    setActivityLogService(service);
    }

	public void setActivityLogService(ActivityLogService service) throws NullPointerException {
		if (service == null) {
			throw new NullPointerException("ActivityLogService in null");
		}
		impl = service;
	}
	
	public ActivityLogService getActivityLogService() {
		return impl;
	}

	@Override
	public void addRecord(ActivityRecord arg0) {
		impl.addRecord(arg0);
	}

	@Override
	public void addRecords(Collection<ActivityRecord> arg0) {
		impl.addRecords(arg0);
	}

	@Override
	public void deleteRecord(Collection<Integer> arg0) {
		impl.deleteRecord(arg0);
	}

	@Override
	public void deleteRecord(int arg0) {
		impl.deleteRecord(arg0);
	}

	@Override
	public List<String> getActivityActions() {
		return translate(impl.getActivityActions());
	}

	@Override
	public List<String> getActivityModules() {
		return translate(impl.getActivityModules());
	}

	@Override
	public ActivityRecord getRecord(int arg0) {
		return translate(impl.getRecord(arg0));
	}

	@Override
	public Collection<ActivityRecord> getRecords(ActivityRecordFilter arg0) {
		return translate(impl.getRecords(arg0));
	}

	@Override
	@Deprecated
	public void log(String arg0, String arg1, String arg2, String arg3) {
		impl.log(arg0, arg1, arg2, arg3);
	}

	@Override
	public void updateRecord(ActivityRecord arg0) {
		impl.updateRecord(arg0);
	}

	@Override
	public void updateRecord(Collection<ActivityRecord> arg0) {
		impl.updateRecord(arg0);
	}

	private ActivityRecord translate(ActivityRecord record) {
		record.setAction(record.getAction());
		record.setModule(record.getModule());
		record.setDescription(translate(record.getDescription()));

		return record;
	}

	private Collection<ActivityRecord> translate(Collection<ActivityRecord> records) {
		for (ActivityRecord item : records) {
			translate(item);
		}

		return records;
	}

	private List<String> translate(List<String> records) {
		List<String> res = new ArrayList<String>();
		for (String item : records) {
			if (item instanceof String) {
				res.add(translate((String) item));
			}
		}

		return res;
	}

}
