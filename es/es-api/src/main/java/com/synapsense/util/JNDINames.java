package com.synapsense.util;

public final class JNDINames {
	public final static String PROVIDER_URL = "remote://localhost:4447";
	public final static String ENVIRONMENT = "es-ear/es-core/Environment!com.synapsense.service.Environment";
	public final static String ALERTING_SERVICE_JNDI_NAME = "es-ear/es-core/AlertService!com.synapsense.service.AlertService";
	public final static String CACHE_SERVICE_JNDI_NAME = "es-ear/es-dal/SynapCache!com.synapsense.dal.objectcache.CacheSvc";
	public final static String USER_SERVICE_JNDI_NAME = "es-ear/es-core/UserManagementService!com.synapsense.service.UserManagementService";
	public final static String NET_STAT_BASE_JNDI_NAME = "SynapServer/NetworkStatisticBaseDAO/remote";
	public final static String NHT_INFO_JNDI_NAME = "SynapServer/NhtInfoDAO/remote";
	public final static String NET_STAT_NODE_JNDI_NAME = "SynapServer/NetworkStatisticDAO/remote";
	public final static String REGISTRAR = "es-ear/es-core/Registrar!com.synapsense.service.impl.registrar.RegistrarSvc";
	public final static String RULE = "es-ear/es-dal/RuleObjectDAO!com.synapsense.dal.generic.RuleInstanceDAO";
	public final static String RULE_TYPE = "es-ear/es-dal/RuleTypeObjectDAO!com.synapsense.service.RuleTypeService";
	public final static String DRULE_TYPE = "es-ear/es-dal/DRuleTypeObjectDAO!com.synapsense.service.DRuleTypeService";
	public final static String CRE_NAME = "es-ear/es-core/RulesEngineService!com.synapsense.service.RulesEngine";
	public final static String ACTIVITY_LOG_SERVICE_JNDI_NAME = "es-ear/es-dal/ActivityLogDAO!com.synapsense.service.impl.activitylog.ActivityLogService";
	public final static String DRE_NAME = "es-ear/es-dre/DRulesEngineService!com.synapsense.service.DRulesEngine";
	public final static String DRE_FILE = "D:/temp/RulesPackage.drl";
	public final static String AUDITOR_SVC_NAME = "es-ear/es-core/AuditorSvcImpl!com.synapsense.service.impl.audit.AuditorSvc";
	public final static String FILE_WRITTER_NAME = "es-ear/es-core/FileWriterService!com.synapsense.service.FileWriter";
	public final static String USER_NAME = "SynapServer/UserObjectDAO/remote";
	public final static String DLIMPORT_SERVICE_JNDI_NAME = "es-ear/es-core/DLImportService!com.synapsense.service.DLImportService";
	public final static String DIAGNOSTIC_SERVICE_NAME = "es-ear/es-core/DiagnosticService!com.synapsense.service.DiagnosticService";
	public final static String UNIT_SYSTEMS_SERVICE_NAME = "es-ear/es-core/UnitSystemsServiceImpl!com.synapsense.service.UnitSystemsService";
	public final static String LOCALIZATION_SERVICE_NAME = "es-ear/es-core/LocalizationService!com.synapsense.service.LocalizationService";
	public final static String REPORTING_SERVICE_NAME = "es-ear/es-core/ReportingService!com.synapsense.service.reporting.ReportingService";
	public final static String REPORTING_MANAGEMENT_SERVICE_NAME = "es-ear/es-core/ReportingManagementService!com.synapsense.service.reporting.management.ReportingManagementService";
	public final static String REPORTING_SCHEDULING_SERVICE_NAME = "es-ear/es-core/ReportingSchedulingService!com.synapsense.service.reporting.management.ReportingSchedulingService";
	
	private JNDINames() { 
	}
}
