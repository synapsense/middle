package com.synapsense.util.convimpl;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.Collection;
import java.util.concurrent.ConcurrentHashMap;

import javax.xml.bind.JAXBException;

import org.apache.log4j.Logger;
import org.xml.sax.SAXException;

import com.synapsense.dto.CollectionTO;
import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.EnvException;
import com.synapsense.search.Direction;
import com.synapsense.search.Path;
import com.synapsense.search.QueryDescription;
import com.synapsense.search.Relations;
import com.synapsense.service.Environment;
import com.synapsense.service.UnitSystemsService;
import com.synapsense.util.ConvertingEnvironmentException;
import com.synapsense.util.ConvertingEnvironmentProxy;
import com.synapsense.util.ServerLoginModule;
import com.synapsense.util.unitconverter.ConverterConfigurationException;
import com.synapsense.util.unitconverter.UnitResolvers;
import com.synapsense.util.unitconverter.UnitResolvers.DimensionRef;
import com.synapsense.util.unitconverter.UnitResolvers.PropertyUnits;


/**
 * Gathers unit dimension of the object from the link, pointing on it.
 */
public class ReferenceUnitsResolver implements UnitResolvers.UnitResolver, Serializable {
	private static final long serialVersionUID = -6980702269366558753L;

	private static final Logger log = Logger.getLogger(ReferenceUnitsResolver.class);
	
	final private static String FILE_STORAGE = "com.synapsense.filestorage";
	final private static String UNIT_SYSTEMS_FILE_NAME = "com.synapsense.unitsystems.usfile";
	final private static String UNIT_RESOLVERS_FILE_NAME = "com.synapsense.unitsystems.urfile";
	
	// dimensions cache
	private static ConcurrentHashMap<TO<?>, DimensionRef> dimensionCache = new ConcurrentHashMap<TO<?>, DimensionRef>();
	private static long lastCacheUpdate = System.currentTimeMillis();
	private static final long CACHE_EXPIRE_INTERVAL = 5 * 60 * 1000;

	@Override
	public DimensionRef getDimension(Environment env, TO<?> to, String propName) throws ConvertingEnvironmentException {
		log.info("Resolving unit by reference");
		checkCacheExpiration();
		DimensionRef result = dimensionCache.get(to);
		if (result != null)
			return result;
		
		UnitResolvers unitResolvers;
		try {
			String storageFileName = System.getProperty(FILE_STORAGE);
			String fileName = storageFileName + File.separator + System.getProperty(UNIT_RESOLVERS_FILE_NAME, "unitresolvers.xml");
			unitResolvers = new UnitResolvers();
			unitResolvers.loadFromXml(fileName);
		} catch (Exception e1) {
			log.error(e1.getLocalizedMessage(), e1);
			return null;
		}
		
		Integer dataclass;
		try {
			QueryDescription qd = new QueryDescription(1);
			qd.startQueryFrom(to);
			qd.addPath(1, new Path(Relations.any(), Direction.INCOMING));
			
			Collection<TO<?>> referencingObjects = env.getObjects(qd);
			log.info(">>>> objects referencing to " + to.toString());
			for(TO<?> tto : referencingObjects) {
				log.info(">> " + tto.toString());
			}
			
			PropertyUnits units = null;
			for(CollectionTO ref : env.getAllPropertiesValues(referencingObjects)) {
				for(ValueTO prop : ref.getPropValues()) {
					if(!to.equals(prop.getValue())) {
						continue;
					}
					
					units = unitResolvers.getUnits(ref.getObjId().getTypeName(), prop.getPropertyName()); 
					if(units != null) {
						break;
					}
				}
			}
			if(units != null) {
				result = units.getDimensionReference();
				dimensionCache.put(to, result);
			} else {
				return null;
			}
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
			throw new ConvertingEnvironmentException("Cannot request dataclass", e);
		}
		
		return result;
	}

	public static void setDimensionRef(TO<?> to, DimensionRef dimRef) {
		checkCacheExpiration();
		dimensionCache.put(to, dimRef);
	}

	private static void checkCacheExpiration() {
		if ((System.currentTimeMillis() - lastCacheUpdate) > CACHE_EXPIRE_INTERVAL) {
			dimensionCache.clear();
			lastCacheUpdate = System.currentTimeMillis();
		}
	}

}
