package com.synapsense.util.unitconverter;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import javax.xml.bind.JAXBException;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;

import org.xml.sax.SAXException;

import com.synapsense.util.unitsystems.Systems;

public class UnitSystems implements Serializable {

	private static final long serialVersionUID = 5298733015686416185L;

	// maps unit system name to system description
	private final Map<String, UnitSystem> systems;
	// maps dimension name to dimension description for non-convertible
	// dimensions
	private final Map<String, Dimension> simpleDimensions;
	// double map for unit systems converters;
	// the first string indicates base system, the second stands for target
	// system
	private final Map<String, Map<String, SystemConverter>> converters;

	public UnitSystems() {
		this.systems = new HashMap<String, UnitSystem>();
		this.converters = new HashMap<String, Map<String, SystemConverter>>();
		this.simpleDimensions = new HashMap<String, Dimension>();
	}

	public void loadFromXml(File file) throws JAXBException, IOException, SAXException, ConverterConfigurationException {
		XmlConfigUtils utils = new XmlConfigUtils(loadSchemas(), Systems.class);
		Systems unitSystems = utils.read(file, Systems.class);
		initMap(unitSystems);
	}

	public void loadFromXml(Reader reader) throws JAXBException, IOException, SAXException,
	        ConverterConfigurationException {
		XmlConfigUtils utils = new XmlConfigUtils(loadSchemas(), Systems.class);
		Systems unitSystems = utils.read(reader, Systems.class);
		initMap(unitSystems);
	}

	public void loadFromXml(String fileName) throws JAXBException, IOException, SAXException,
	        ConverterConfigurationException {
		XmlConfigUtils utils = new XmlConfigUtils(loadSchemas(), Systems.class);
		Systems unitSystems = utils.read(fileName, Systems.class);
		initMap(unitSystems);
	}

	private Source[] loadSchemas() throws ConverterConfigurationException {
		InputStream usSchema = this.getClass().getClassLoader().getResourceAsStream("schemas/UnitSystems.xsd");
		if (usSchema == null) {
			throw new ConverterConfigurationException("File schemas/UnitSystems.xsd is not available in classpath");
		}
		Source[] res = new Source[1];
		res[0] = new StreamSource(usSchema, "UnitSystems.xsd");
		return res;
	}

	public Map<String, UnitSystem> getUnitSystems() {
		return systems;
	}

	public UnitSystem getUnitSystem(String systemName) throws ConverterException {
		UnitSystem res = systems.get(systemName);
		if (res == null) {
			throw new ConverterException("Unit system \"" + systemName + "\" is not described");
		}
		return res;
	}

	public SystemConverter getSystemConverter(final String systemFrom, final String systemTo) throws ConverterException {
		Map<String, SystemConverter> map = converters.get(systemFrom);
		if (map == null)
			throw new ConverterException("Unit system \"" + systemFrom + "\" is not described");
		SystemConverter res = map.get(systemTo);
		if (res == null)
			throw new ConverterException("Unit system \"" + systemTo + "\" is not described");
		return res;
	}

	public UnitConverter getUnitConverter(final String baseSystem, final String targetSystem, final String dimension)
	        throws ConverterException {
		SystemConverter conv = getSystemConverter(baseSystem, targetSystem);
		UnitConverter uConv = conv.getConverter(dimension);
		if (uConv == null)
			throw new ConverterException("Conversion from \"" + baseSystem + "\" to \"" + targetSystem
			        + "\" for dimension \"" + dimension + "\" is not specified");
		return uConv;
	}

	private void initMap(Systems systemsDescr) throws ConverterConfigurationException {
		// Load simple dimensions first
		if (systemsDescr.getSimpleDimensions() != null) {
			for (com.synapsense.util.unitsystems.Dimension dim : systemsDescr.getSimpleDimensions().getDimension()) {
				Dimension tmpDim = new Dimension(dim.getName(), dim.getUnits(),
				        dim.getLongUnits() != null ? dim.getLongUnits() : "");
				// constraint: simple dimension's name must be unique within
				// simple dimensions
				if (simpleDimensions.put(dim.getName(), tmpDim) != null)
					throw new ConverterConfigurationException("Simple dimension \"" + dim.getName()
					        + "\": name must unique within simple dimensions");
			}
		}

		// read unit systems definitions
		if (systemsDescr.getSystem() != null) {
			for (Systems.System system : systemsDescr.getSystem()) {
				UnitSystem us = new UnitSystem(system.getName());
				// constraint: system's name must be unique
				if (systems.put(system.getName(), us) != null) {
					throw new ConverterConfigurationException(String.format("System \"%s\": name must be unique",
					        system.getName()));
				}
				for (com.synapsense.util.unitsystems.Dimension dim : system.getDimension()) {
					Dimension tmpDim = new Dimension(dim.getName(), dim.getUnits(),
					        dim.getLongUnits() != null ? dim.getLongUnits() : "");
					// constraint: dimension's name must be unique within system
					if (us.getDimensionsMap().put(dim.getName(), tmpDim) != null) {
						throw new ConverterConfigurationException(String.format(
						        "Dimension \"%s\": name must be unique within system \"%s\"", dim.getName(),
						        system.getName()));
					}
					// constraint dimension must not be already defined as
					// simple
					if (simpleDimensions.containsKey(dim.getName())) {
						throw new ConverterConfigurationException(String.format(
						        "Dimension \"%s\" in system \"%s\" is already defined as a simple dimension",
						        dim.getName(), system.getName()));
					}
				}
			}
		}

		// create converters
		for (Systems.Conversions conv : systemsDescr.getConversions()) {
			// constraint: conversion can be defined only for different systems
			if (conv.getFrom().equals(conv.getTo())) {
				throw new ConverterConfigurationException(String.format(
				        "Convertors into the same system (\"%s\"->\"%s\") make no sense", conv.getFrom(), conv.getTo()));
			}

			UnitSystem usFrom = systems.get(conv.getFrom());
			if (usFrom == null)
				throw new ConverterConfigurationException(String.format(
				        "Cannot load converters for \"%s\"->\"%s\": system \"%1$s\" is not defined", conv.getFrom(),
				        conv.getTo()));

			UnitSystem usTo = systems.get(conv.getTo());
			if (usTo == null)
				throw new ConverterConfigurationException(String.format(
				        "Cannot load converters for \"%s\"->\"%s\": system \"%2$s\" is not defined", conv.getFrom(),
				        conv.getTo()));

			for (com.synapsense.util.unitsystems.Systems.Conversions.Dimension dimConversion : conv.getDimension()) {
				if (!systems.get(conv.getFrom()).getDimensionsMap().containsKey(dimConversion.getName())
				        || !systems.get(conv.getTo()).getDimensionsMap().containsKey(dimConversion.getName())) {
					throw new ConverterConfigurationException(
					        String.format(
					                "Dimension \"%s\" referenced in conversions for \"%s\"->\"%s\" must be defined in both systems",
					                dimConversion.getName(), conv.getFrom(), conv.getTo()));
				}
				// get conversion coefficients
				double factor = dimConversion.getFactor();
				double shift = 0;
				try {
					shift = dimConversion.getShift();
				} catch (NullPointerException e) {
					// auto-unboxing failed with NPE on null shift
					// means that shift was not specified
					// (and thus remains zero in our case)
				}

				// create value converters
				Converter directConverter = new DirectConverter(factor, shift);
				Converter reverseConverter = new ReverseConverter(factor, shift);
				// let's see if custom converters factory is specified
				String convFactory = dimConversion.getConvFactoryClass();
				if (convFactory != null) {
					CustomConverterFactory factory;
					try {
						factory = (CustomConverterFactory) Thread.currentThread().getContextClassLoader()
						        .loadClass(convFactory).getConstructor().newInstance();
					} catch (Exception e) {
						throw new ConverterConfigurationException(String.format(
						        "Cannot load converters factory class \"%s\"", convFactory), e);
					}
					try {
						directConverter = factory.createDirectConverter(usFrom, usTo,
						        usFrom.getDimension(dimConversion.getName()), directConverter);
						reverseConverter = factory.createReverseConverter(usFrom, usTo,
						        usTo.getDimension(dimConversion.getName()), reverseConverter);
					} catch (ConverterException e) {
						throw new ConverterConfigurationException(String.format(
						        "Cannot create custom converter for dimension \"%s\" in \"%s\"->\"%s\"",
						        dimConversion.getName(), usFrom.getName(), usTo.getName()), e);
					}
				}

				// create direct conversion
				createConversion(usFrom, usTo, dimConversion.getName(), directConverter);
				// create revert conversion
				createConversion(usTo, usFrom, dimConversion.getName(), reverseConverter);
			}
		}
	}

	private void createConversion(UnitSystem baseSystem, UnitSystem targetSystem, String dim, Converter conv)
	        throws ConverterConfigurationException {
		Map<String, SystemConverter> map = converters.get(baseSystem.getName());
		if (map == null) {
			map = new HashMap<String, SystemConverter>();
			converters.put(baseSystem.getName(), map);
		}
		SystemConverter converter = map.get(targetSystem.getName());
		if (converter == null) {
			converter = new SystemConverter(baseSystem, targetSystem, new HashSet<String>(simpleDimensions.keySet()));
			map.put(targetSystem.getName(), converter);
		}
		converter.getConverters().put(dim, new UnitConverter(baseSystem, targetSystem, dim, conv));
	}

	private static class DirectConverter implements Converter, Serializable {

		private static final long serialVersionUID = -7500840428091843250L;
		private final double factor;
		private final double shift;

		public DirectConverter(double factor, double shift) {
			this.factor = factor;
			this.shift = shift;
		}

		@Override
		public double convert(double value) {
			return factor * value + shift;
		}
	}

	private static class ReverseConverter implements Converter, Serializable {

		private static final long serialVersionUID = 5718626373472765324L;
		private final double factor;
		private final double shift;

		public ReverseConverter(double factor, double shift) {
			this.factor = factor;
			this.shift = shift;
		}

		@Override
		public double convert(double value) {
			return (value - shift) / factor;
		}
	}

	public Map<String, Dimension> getSimpleDimensions() {
		return simpleDimensions;
	}
}
