package com.synapsense.util.executor;

public class ProcessingException extends RuntimeException {

	private static final long serialVersionUID = -4327083376862652061L;

	public ProcessingException() {
		super();
	}

	public ProcessingException(String message, Throwable cause) {
		super(message, cause);
	}

	public ProcessingException(String message) {
		super(message);
	}

	public ProcessingException(Throwable cause) {
		super(cause);
	}
}
