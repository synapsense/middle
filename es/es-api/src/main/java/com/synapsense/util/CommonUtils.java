package com.synapsense.util;

import com.synapsense.dto.ValueTO;

import java.util.Collection;
import java.util.Map;

/**
 * Created by wfi on 7/6/2015.
 */
public class CommonUtils {
    public static boolean nullSafeBoolean(Boolean b) {
        return b == null ? false : b;
    }

    public static int nullSafeInt(Integer i) {
        return i == null ? 0 : i;
    }

    public static boolean isStringEmpty(String s) {
        return s == null || s.trim().isEmpty();
    }

    public static Map<String, Object> valuesAsMap(Collection<ValueTO> values) {
        Map<String, Object> map = CollectionUtils.newMap();
        for (ValueTO v : values) {
            map.put(v.getPropertyName(), v.getValue());
        }
        return map;
    }

    public static Collection<ValueTO> mapAsValues(Map<String, Object> map) {
        Collection<ValueTO> values = CollectionUtils.newList();
        for (Map.Entry<String, Object> entry : map.entrySet()) {
            values.add(new ValueTO(entry.getKey(), entry.getValue()));
        }
        return values;
    }


}
