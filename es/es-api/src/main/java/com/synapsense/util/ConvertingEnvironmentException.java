package com.synapsense.util;

/**
 * Base class for checked exceptions thrown by ConvertingEnvironmentProxy.
 * 
 * @author Oleg Stepanov
 * 
 */
public class ConvertingEnvironmentException extends RuntimeException {

	private static final long serialVersionUID = -8541234787649360847L;

	public ConvertingEnvironmentException() {
		super();
	}

	public ConvertingEnvironmentException(String message) {
		super(message);
	}

	public ConvertingEnvironmentException(Throwable cause) {
		super(cause);
	}

	public ConvertingEnvironmentException(String message, Throwable cause) {
		super(message, cause);
	}

}
