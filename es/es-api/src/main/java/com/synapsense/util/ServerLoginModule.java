package com.synapsense.util;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.NameCallback;
import javax.security.auth.callback.PasswordCallback;
import javax.security.auth.callback.UnsupportedCallbackException;
import javax.security.auth.login.AppConfigurationEntry;
import javax.security.auth.login.Configuration;
import javax.security.auth.login.LoginContext;
import javax.security.auth.login.LoginException;

final public class ServerLoginModule {

	private static final String DEFAULT_LOGIN_CONFIG_NAME = "ES client";

	/**
	 * Stores the state of current login context. Has different state for
	 * different threads.
	 * 
	 */
	private static ThreadLocal<LoginContext> loginContext = new ThreadLocal<LoginContext>();

	private static Configuration loginConfig = null;

	private static InitialContext initialContext = null;

	/**
	 * Allows caller to get a proxy reference by the specified JNDI name
	 * <code>jndiTarget</code>. The returned proxy reference is not cached by
	 * <code>ServerLoginModule</code>. In other words, every call to this method
	 * leads to JNDI provider lookup call.<br>
	 * Note that security credentials must be specified separately by calling
	 * <tt>login</tt> method.
	 * 
	 * @see #getProxy(InitialContext, String, Class) if you have already
	 *      configured initial context
	 * 
	 * @param jndiTarget
	 *            JNDI name to lookup for
	 * @param clazz
	 *            Class of proxy reference being requested.
	 * @return The proxy with interface of type <code>clazz</code>
	 * @throws NamingException
	 *             if lookup for <code>jndiTarget</code> has failed.
	 */
	public static <INTERFACE> INTERFACE getProxy(String jndiTarget, Class<INTERFACE> clazz) throws NamingException {
		return getProxy(new InitialContext(), jndiTarget, clazz);
	}

	/**
	 * Allows caller to get a proxy reference by the specified JNDI name
	 * <code>jndiTarget</code> via predefined InitialContext. The returned proxy
	 * reference is not cached by <code>ServerLoginModule</code>. In other
	 * words, every call to this method leads to JNDI provider lookup call.<br>
	 * Note that security credentials must be specified separately by calling
	 * <tt>login</tt> method.
	 * 
	 * @see #getProxy(String, Class) if you want to use default initial context
	 *      {@link InitialContext}
	 * 
	 * @param icx
	 *            Caller-defined initial context
	 * @param jndiTarget
	 *            JNDI name to lookup for
	 * @param clazz
	 *            Class of proxy reference being requested.
	 * @return The proxy with interface of type <code>clazz</code>
	 * @throws NamingException
	 *             if lookup for <code>jndiTarget</code> has failed.
	 */
	public static <INTERFACE> INTERFACE getProxy(InitialContext icx, String jndiTarget, Class<INTERFACE> clazz)
	        throws NamingException {
		init(icx);
		return clazz.cast(icx.lookup(jndiTarget));
	}

	public static <INTERFACE> INTERFACE getService(String serviceName, Class<INTERFACE> clazz)
			throws NamingException {
		return getService(initialContext, "es-core", serviceName, clazz);
	}

	/**
	 * The only difference of this method from
	 * {@link #getProxy(InitialContext, String, Class)} is that target jndi name
	 * will be assembled by the method itself basing on the service's name and
	 * interface
	 * 
	 * @param icx
	 * @param serviceName
	 * @param clazz
	 * @return
	 * @throws NamingException
	 */
	public static <INTERFACE> INTERFACE getService(InitialContext icx, String serviceName, Class<INTERFACE> clazz)
	        throws NamingException {
		return getService(icx, "es-core", serviceName, clazz);
	}

	public static <INTERFACE> INTERFACE getService(InitialContext icx, String moduleName, String serviceName,
	        Class<INTERFACE> clazz) throws NamingException {

		String jndiName = "ejb:es-ear/" + moduleName + "/" + serviceName + '!' + clazz.getName();
		return getProxy(icx, jndiName, clazz);
	}

	/**
	 * The method was deprecated.
	 * 
	 * @see #getProxy(String, Class)
	 * 
	 */
	@Deprecated
	public static <INTERFACE> INTERFACE getProxy(String userName, String password, String jndiTarget,
	        Class<INTERFACE> clazz) throws NamingException, LoginException, NoSuchAlgorithmException,
	        UnsupportedEncodingException {
		return getProxy(jndiTarget, clazz);
	}

	/**
	 * The method was deprecated.
	 * 
	 * @see #getProxy(InitialContext, String, Class)
	 * 
	 */
	@Deprecated
	public static <INTERFACE> INTERFACE getProxy(InitialContext icx, String jndiTarget, String userName,
	        String password, Class<INTERFACE> clazz) throws NamingException, LoginException, NoSuchAlgorithmException,
	        UnsupportedEncodingException {
		return getProxy(icx, jndiTarget, clazz);
	}

	/**
	 * Allows caller to login and to get security credentials by the specified
	 * user name and password. After this method call a new login context will
	 * be opened.<br>
	 * Login context is a set of credentials which is mandatory passed to any
	 * secured service during a call of any of its methods.
	 * </tt>login</tt>method is not expensive and must be called before using
	 * any secured service.<br>
	 * 
	 * The login context is created per thread. If caller creates a new thread ,
	 * the login context of the current thread is not used in the new thread.
	 * Client must call login in the new thread separately.<br>
	 * 
	 * Note that credentials set by call of this method are used for any service
	 * called from this thread. That is, for the scenario when you:<br>
	 * 1. Get Service1Proxy<br>
	 * 2. Call login<br>
	 * 3. Get Service2Proxy<br>
	 * 4. Call login<br>
	 * 5. Call Service1Proxy and Service2Proxy<br>
	 * the credentials passed at step 4 will be used for all calls at step 5.
	 * 
	 * @param userName
	 *            Name of the user(not null,not empty)
	 * @param password
	 *            Password to login (not null)
	 * @throws IllegalArgumentException
	 *             if some of parameters are illegal
	 * @throws LoginException
	 *             If operation was not successfully.Indicates that errors has
	 *             occurred during login.
	 * 
	 */
	public static void login(final String userName, final String password) throws LoginException, NamingException {
		if (userName == null || userName.isEmpty()) {
			throw new IllegalArgumentException("Supplied 'userName' is null or empty");
		}
		if (password == null) {
			throw new IllegalArgumentException("Supplied 'password' is null");
		}

		if (initialContext != null) {
			initialContext.close();
			initialContext = null;
		}

		Properties props = new Properties();
		props.put(Context.URL_PKG_PREFIXES, "org.jboss.ejb.client.naming");
		props.put(Context.SECURITY_PRINCIPAL, userName);
		props.put(Context.SECURITY_CREDENTIALS, password);

		initialContext = new InitialContext(props);

		if (loginConfig == null) {
			init(initialContext);
		}

		// get thread's local copy
		Configuration conf = loginConfig;
		if (conf == null) {
			throw new IllegalStateException("Login configuration is not initialized.");
		}

		CallbackHandler handler = new CallbackHandler() {
			public void handle(Callback[] callbacks) throws IOException, UnsupportedCallbackException {
				for (Callback current : callbacks) {
					if (current instanceof NameCallback) {
						((NameCallback) current).setName(userName);
					} else if (current instanceof PasswordCallback) {
						((PasswordCallback) current).setPassword(password.toCharArray());
					} else {
						throw new UnsupportedCallbackException(current);
					}
				}
			}
		};

		LoginContext lc = loginContext.get();
		if (lc != null) {
			// logout thread's current login context to realize resources
			lc.logout();
		}

		lc = new LoginContext(DEFAULT_LOGIN_CONFIG_NAME, null, handler, conf);

		lc.login();

		// saves login context only after successful executing of lc.login();
		loginContext.set(lc);
	}

	public static synchronized void init(InitialContext ctx) throws NamingException {
		if (loginConfig != null)
			return;

		initialContext = ctx;

		// TODO later when we figure out how to feed the config from the server
		// (and if we still
		// think we need it) the code can be rewritten to obtain it from there
		loginConfig = new Configuration() {

			@Override
			public AppConfigurationEntry[] getAppConfigurationEntry(String name) {
				Map<String, String> options = new HashMap<String, String>();
				options.put("multi-threaded", "true");
				options.put("restore-login-identity", "false");

				AppConfigurationEntry clmEntry = new AppConfigurationEntry("org.jboss.security.ClientLoginModule",
				        AppConfigurationEntry.LoginModuleControlFlag.REQUIRED, options);

				return new AppConfigurationEntry[] { clmEntry };
			}
		};
	}

	/**
	 * Allows caller to logout.
	 * 
	 * @see #login(String, String) to login
	 * 
	 * @throws LoginException
	 *             if caller is not logged in or if some problem happen during
	 *             operation
	 */
	public static void logout() throws LoginException {
		LoginContext lc = loginContext.get();
		if (lc == null) {
			throw new LoginException("You are not logged in!");
		}

		lc.logout();

		loginContext.remove();
	}

	/**
	 * 
	 * This class provides password encoding service.
	 * 
	 */
	final public static class PasswordService {
		/**
		 * Encrypts specified password by the algorithm used on the server side.
		 * 
		 * @param text
		 *            Password to encrypt
		 * @return Encrypted password representation.
		 * @throws NoSuchAlgorithmException
		 * @throws UnsupportedEncodingException
		 */
		static public String encrypt(String text) throws NoSuchAlgorithmException, UnsupportedEncodingException {
			MessageDigest md = null;
			md = MessageDigest.getInstance("SHA1");
			md.update(text.getBytes());
			byte raw[] = md.digest();
			String password = new String();
			for (int i = 0; i < raw.length; i++) {
				if ((raw[i] & 0xF0) == 0)
					password += "0";
				password += Integer.toHexString(raw[i] & 0xFF);
			}
			return password;
		}
	}
}