package com.synapsense.util.executor;

public final class Executors {
	public static Executor newPooledExecutor(int threadsNum) {
		return newPooledExecutor("", threadsNum);
	}

	public static Executor newPooledExecutor(String name, int threadsNum) {
		return PooledExecutor.configure(name, threadsNum);
	}

}
