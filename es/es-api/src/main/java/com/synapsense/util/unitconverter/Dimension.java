package com.synapsense.util.unitconverter;

import java.io.Serializable;

public class Dimension implements Serializable {

	private static final long serialVersionUID = 1548970057376976764L;

	private final String name;
	private final String units;
	private final String longUnits;

	public Dimension(String name, String units, String longUnits) {
		super();
		this.name = name;
		this.units = units;
		this.longUnits = longUnits;
	}

	public String getName() {
		return name;
	}

	public String getUnits() {
		return units;
	}

	public String getLongUnits() {
		return longUnits;
	}
}