package com.synapsense.util.executor;

public interface Action {
	void execute() throws ProcessingException;
}
