package com.synapsense.util.unitconverter;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.SchemaOutputResolver;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamResult;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.w3c.dom.ls.LSInput;
import org.w3c.dom.ls.LSResourceResolver;
import org.xml.sax.SAXException;

/**
 * The class XmlConfigUtils is used to deal with XML serialization and XML
 * files. Class contains lot of methods which are useful in different
 * situations.
 * 
 * @author Oleg Stepanov
 */
public class XmlConfigUtils {

	private JAXBContext context;

	private Schema validationSchema;

	private Unmarshaller unmarshaller;

	private Marshaller marshaller;

	private Source[] schemas;

	/**
	 * Creates a new instance of XmlConfigUtils.
	 * 
	 * @param classes
	 *            list of configuration classes to be managed by this instance.
	 * @throws JAXBException
	 */
	public XmlConfigUtils(Class<?>... classes) throws JAXBException {
		this.context = JAXBContext.newInstance(classes);
	}

	/**
	 * Creates a new instance of XmlConfigUtils.
	 * 
	 * @param classes
	 *            list of configuration classes to be managed by this instance.
	 * @param schemas
	 *            XML schemas for validation of files being loaded.
	 * @throws JAXBException
	 */
	public XmlConfigUtils(Source[] schemas, Class<?>... classes) throws JAXBException {
		this.context = JAXBContext.newInstance(classes);
		this.schemas = schemas;
	}

	/**
	 * Write the given bean to the XML file.
	 * <p>
	 * Performs an atomic write, first writing in {@code <file>.new}, then
	 * renaming {@code <file>} to {@code <file>~}, then renaming renaming
	 * {@code <file>.new} to {@code <file>}.
	 * </p>
	 * 
	 * @param file
	 *            name of file to be written
	 * @param bean
	 *            The object to write in the XML file.
	 * @param beanClass
	 *            class of the bean to be written.
	 * @throws IOException
	 *             if write to file failed.
	 **/
	public void writeToFile(String file, Object bean, Class<?> beanClass) throws IOException {

		// Creates a new file named <file>.new
		final File f = newXmlTmpFile(file);
		try {
			final FileOutputStream out = new FileOutputStream(f);
			boolean failed = true;
			try {
				// writes to <file>.new
				write(bean, out, false, beanClass);

				// no exception: set failed=false for finaly {} block.
				failed = false;
			} finally {
				out.close();
				// An exception was raised: delete temporary file.
				if (failed)
					f.delete();
			}

			// rename <file> to <file>~ and <file>.new to <file>
			commit(file, f);
		} catch (JAXBException x) {
			final IOException io = new IOException("Failed to write object to " + file + ": " + x, x);
			throw io;
		}
	}

	/**
	 * Creates an XML string representation of the given bean.
	 * 
	 * @throws IllegalArgumentException
	 *             if the bean class is not known by the underlying XMLbinding
	 *             context.
	 * @return An XML string representation of the given bean.
	 **/
	public String toString(Object bean) {
		try {
			final ByteArrayOutputStream baos = new ByteArrayOutputStream();
			final Marshaller m = getMarshaller();
			m.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
			m.marshal(bean, baos);
			return baos.toString();
		} catch (JAXBException x) {
			final IllegalArgumentException iae = new IllegalArgumentException(
			        "Failed to write SessionConfigBean: " + x, x);
			throw iae;
		}
	}

	/**
	 * Creates an XML clone of the given bean.
	 * <p>
	 * In other words, this method XML-serializes the given bean, and
	 * XML-deserializes a copy of that bean.
	 * </p>
	 * 
	 * @return A deep-clone of the given bean.
	 * @throws IllegalArgumentException
	 *             if the bean class is not known by the underlying XML binding
	 *             context.
	 * @param bean
	 *            The bean to clone.
	 * @param beanClass
	 *            Class of the bean
	 */
	@SuppressWarnings("unchecked")
	public <T> T xmlClone(T bean, T beanClass) {
		return (T) copy(bean);
	}

	/**
	 * Creates an XML clone of the given bean.
	 * <p>
	 * In other words, this method XML-serializes the given bean, and
	 * XML-deserializes a copy of that bean.
	 * </p>
	 * 
	 * @throws IllegalArgumentException
	 *             if the bean class is not known by the underlying XML binding
	 *             context.
	 * @return A deep-clone of the given bean.
	 **/
	private Object copy(Object bean) {
		try {
			final ByteArrayOutputStream baos = new ByteArrayOutputStream();
			final Marshaller m = getMarshaller();
			m.marshal(bean, baos);
			final ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
			return getUnmarshaller().unmarshal(bais);
		} catch (Exception x) {
			final IllegalArgumentException iae = new IllegalArgumentException("Failed to write object: " + x, x);
			throw iae;
		}
	}

	/**
	 * Reads the configuration from the XML configuration file.
	 * 
	 * @param file
	 *            file to read.
	 * @param beanClass
	 *            class of the bean to read.
	 * @return A bean read from the XML configuration file.
	 * @throws IOException
	 *             if it fails to read the configuration.
	 * @throws SAXException
	 **/
	public <T> T read(String file, Class<T> beanClass) throws IOException, SAXException {
		final File f = new File(file);
		if (!f.exists())
			throw new IOException("No such file: " + file);
		if (!f.canRead())
			throw new IOException("Can't read file: " + file);
		try {
			return read(f, beanClass);
		} catch (JAXBException x) {
			final IOException io = new IOException("Failed to read from " + file + ": " + x, x);
			throw io;
		}
	}

	/**
	 * Reads the configuration from the given XML configuration file.
	 * 
	 * @param f
	 *            the file to read from.
	 * @return A {@code ScanManagerConfig} bean read from the XML configuration
	 *         file.
	 * @throws javax.xml.bind.JAXBException
	 *             if it fails to read the configuration.
	 * @throws SAXException
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	public <T> T read(File f, Class<T> beanClass) throws JAXBException, IOException, SAXException {
		final Unmarshaller u = getUnmarshaller();
		return (T) u.unmarshal(f);
	}

	/**
	 * Yet another version of read using Reader source. It is useful to
	 * deserialize bean classes from strings etc.
	 * 
	 * @param r
	 *            the reader to to read from
	 * @param beanClass
	 *            Type of object to be unmarshalled
	 * @return Unmarshalled object.
	 * @throws JAXBException
	 * @throws IOException
	 * @throws SAXException
	 */
	@SuppressWarnings("unchecked")
	public <T> T read(Reader r, Class<T> beanClass) throws JAXBException, IOException, SAXException {
		final Unmarshaller u = getUnmarshaller();
		return (T) u.unmarshal(r);
	}

	/**
	 * Writes the given bean to the given output stream.
	 * 
	 * @param bean
	 *            the bean to write.
	 * @param os
	 *            the output stream to write to.
	 * @param fragment
	 *            whether the {@code <?xml ... ?>} header should be included.
	 *            The header is not included if the bean is just an XML fragment
	 *            encapsulated in a higher level XML element.
	 * @throws JAXBException
	 *             An XML Binding exception occurred.
	 **/
	public void write(Object bean, OutputStream os, boolean fragment, Class<?> beanClass) throws JAXBException {
		writeXml((Object) bean, os, fragment);
	}

	/**
	 * Writes the given bean to the given output stream.
	 * 
	 * @param bean
	 *            the bean to write.
	 * @param os
	 *            the output stream to write to.
	 * @param fragment
	 *            whether the {@code <?xml ... ?>} header should be included.
	 *            The header is not included if the bean is just an XML fragment
	 *            encapsulated in a higher level XML element.
	 * @throws JAXBException
	 *             An XML Binding exception occurred.
	 **/
	private void writeXml(Object bean, OutputStream os, boolean fragment) throws JAXBException {
		final Marshaller m = getMarshaller();
		if (fragment)
			m.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
		m.marshal(bean, os);
	}

	/**
	 * Gets unmarshaller that can be used for parsing XML representation of
	 * types managed by this instance.
	 * 
	 * @return Validating unmarshaller.
	 * @throws JAXBException
	 *             if an error was encountered while creating the
	 *             <tt>Unmarshaller</tt> object
	 * @throws IOException
	 *             if an error occurred validation during schema generation.
	 * @throws SAXException
	 *             if an error occurred validation during schema parsing.
	 */
	public Unmarshaller getUnmarshaller() throws JAXBException, IOException, SAXException {
		if (unmarshaller == null) {
			unmarshaller = getContext().createUnmarshaller();
			unmarshaller.setSchema(getSchema());
		}
		return unmarshaller;
	}

	/**
	 * Gets marshaller for saving entities into XML.
	 * 
	 * @return Returned JAXB Marshaller is tuned for nicely XML formatted
	 *         output.
	 * @throws JAXBException
	 *             if an error was encountered while creating the Marshaller
	 *             object
	 */
	public Marshaller getMarshaller() throws JAXBException {
		if (marshaller == null) {
			marshaller = getContext().createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		}
		return marshaller;
	}

	/**
	 * Gets JAXB context used by this instance.
	 * 
	 * @return Returned JAXB context is the one used by this
	 *         <tt>XmlConfigUtils</tt> instance.
	 */
	public JAXBContext getContext() {
		return context;
	}

	/**
	 * Creates (if necessary) and returns a schema that can be used to validate
	 * XML files before unmarshalling. The same schema is used internally.
	 * 
	 * @return Schema used for validation of XML input before its unmarshalling.
	 * @throws IOException
	 *             if an error occurred during schema generation.
	 * @throws SAXException
	 *             if an error occurred during schema parsing.
	 */
	public Schema getSchema() throws IOException, SAXException {
		if (validationSchema == null) {
			validationSchema = getSchema(getContext(), schemas);
		}
		return validationSchema;
	}

	private static Schema getSchema(JAXBContext context, Source[] schemas) throws IOException, SAXException {
		SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
		if (schemas != null && schemas.length != 0) {
			// nice, we have explicit schemas to validate against
			return factory.newSchema(schemas);
		} else {
			// no schemas are given, let's try to get it from context itself:
			final List<StreamResult> results = new ArrayList<StreamResult>();
			context.generateSchema(new SchemaOutputResolver() {
				@Override
				public Result createOutput(String namespaceUri, String suggestedFileName) throws IOException {
					StreamResult res = new StreamResult(new StringWriter());
					res.setSystemId(suggestedFileName);
					results.add(res);
					return res;
				}
			});
			SchemaCache cache = new SchemaCache();
			for (StreamResult res : results) {
				cache.addSchema(res.getSystemId(), res.getWriter().toString());
			}
			factory.setResourceResolver(cache);
			return factory.newSchema();
		}
	}

	// Creates a new XML temporary file called <basename>.new
	// This method is used to implement atomic writing to file.
	// The usual sequence is:
	//
	// Final tmp = newXmlTmpFile(basename);
	// boolean failed = true;
	// try {
	// ... write to 'tmp' ...
	// // no exception: set failed=false for finaly {} block.
	// failed = false;
	// } finally
	// // failed==true means there was an exception and
	// // commit won't be called...
	// if (failed==true) tmp.delete();
	// }
	// commit(tmp,basename)
	//
	private static File newXmlTmpFile(String basename) throws IOException {
		final File f = new File(basename + ".new");
		if (!f.createNewFile())
			throw new IOException("file " + f.getName() + " already exists");

		try {
			final OutputStream newStream = new FileOutputStream(f);
			try {
				final String decl = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>";
				newStream.write(decl.getBytes("UTF-8"));
				newStream.flush();
			} finally {
				newStream.close();
			}
		} catch (IOException x) {
			f.delete();
			throw x;
		}
		return f;
	}

	// Commit the temporary file by renaming <basename> to <baesname>~
	// and tmpFile to <basename>.
	private static File commit(String basename, File tmpFile) throws IOException {
		try {
			final String backupName = basename + "~";
			final File desired = new File(basename);
			final File backup = new File(backupName);
			backup.delete();
			if (desired.exists() && !desired.renameTo(new File(backupName))) {
				throw new IOException("can't rename to " + backupName);
			}
			if (!tmpFile.renameTo(new File(basename)))
				throw new IOException("can't rename to " + basename);
		} catch (IOException x) {
			tmpFile.delete();
			throw x;
		}
		return new File(basename);
	}

	/**
	 * Creates a new committed XML file for {@code <basename>}, containing only
	 * the {@code <?xml ...?>} header.
	 * <p>
	 * This method will rename {@code <basename>} to {@code <basename>~}, if it
	 * exists.
	 * </p>
	 * 
	 * @return A newly created XML file containing the regular
	 *         {@code <?xml ...?>} header.
	 * @param basename
	 *            The name of the new file.
	 * @throws IOException
	 *             if the new XML file couldn't be created.
	 */
	public static File createNewXmlFile(String basename) throws IOException {
		return commit(basename, newXmlTmpFile(basename));
	}

}

/**
 * Storage for schemas used to validate beans instances managed by
 * XmlConfigUtils.
 * 
 * @author Oleg Stepanov
 * 
 */
class SchemaCache implements LSResourceResolver {

	// We store schemas as pairs of schema system id string
	// and SchemaInput with schema's content
	private Map<String, SchemaInput> cache = new HashMap<String, SchemaInput>();

	/**
	 * 
	 * @param systemId
	 * @param schema
	 */
	public void addSchema(String systemId, String schema) {
		cache.put(systemId, new SchemaInput(new StringReader(schema)));
	}

	@Override
	public LSInput resolveResource(String type, String namespaceURI, String publicId, String systemId, String baseURI) {
		return cache.get(systemId);
	}

	/**
	 * Class wraps <tt>org.w3c.dom.ls.LSInput</tt> interface around any
	 * <tt>Reader</tt>. Since we wrap only readers, implementations of methods
	 * not related to them are void.
	 * 
	 * @author Oleg Stepanov
	 * 
	 */
	static class SchemaInput implements LSInput {

		private final Reader reader;

		public SchemaInput(Reader reader) {
			this.reader = reader;
		}

		@Override
		public String getBaseURI() {
			return null;
		}

		@Override
		public InputStream getByteStream() {
			return null;
		}

		@Override
		public boolean getCertifiedText() {
			return false;
		}

		@Override
		public Reader getCharacterStream() {
			return reader;
		}

		@Override
		public String getEncoding() {
			return null;
		}

		@Override
		public String getPublicId() {
			return null;
		}

		@Override
		public String getStringData() {
			return null;
		}

		@Override
		public String getSystemId() {
			return null;
		}

		@Override
		public void setBaseURI(String baseURI) {
			// we wrap Readers only
		}

		/**
		 * 
		 */
		@Override
		public void setByteStream(InputStream byteStream) {
			// we wrap Readers only
		}

		@Override
		public void setCertifiedText(boolean certifiedText) {
			// we wrap Readers only
		}

		@Override
		public void setCharacterStream(Reader characterStream) {
			// we wrap Readers only
		}

		@Override
		public void setEncoding(String encoding) {
			// we wrap Readers only
		}

		@Override
		public void setPublicId(String publicId) {
			// we wrap Readers only
		}

		@Override
		public void setStringData(String stringData) {
			// we wrap Readers only
		}

		@Override
		public void setSystemId(String systemId) {
			// we wrap Readers only
		}

	}
}