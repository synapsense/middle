package com.synapsense.util.unitconverter;

import java.io.Serializable;

public class UnitConverter implements Serializable {

	private static final long serialVersionUID = 1860319363117071944L;
	private final UnitSystem baseSystem;
	private final UnitSystem targetSystem;
	private final String dimensionName;
	private final Converter converter;

	public UnitConverter(UnitSystem baseSystem, UnitSystem targetSystem, String dimensionName, Converter converter) {
		this.baseSystem = baseSystem;
		this.targetSystem = targetSystem;
		this.dimensionName = dimensionName;
		this.converter = converter;
	}

	public double convert(double val) {
		return converter.convert(val);
	}

	public UnitSystem getBaseSystem() {
		return baseSystem;
	}

	public UnitSystem getTargetSystem() {
		return targetSystem;
	}

	public String getDimensionName() {
		return dimensionName;
	}

}
