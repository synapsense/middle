package com.synapsense.util.executor;

import java.util.Arrays;
import java.util.Collection;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

class PooledExecutor implements Executor {

	static Logger logger = Logger.getLogger(PooledExecutor.class);

	private ExecutorService pool;

	private int threadsNum;

	private String name;

	private PooledExecutor(String name, int threadsNum) {
		this.threadsNum = threadsNum;
		this.name = name;
		pool = Executors.newFixedThreadPool(threadsNum);
	}

	private PooledExecutor() {
		this("", 1);
	}

	static Executor configure(String name, int threadsNum) {
		return new PooledExecutor(name, threadsNum);
	}

	@Override
	public void doAction(Action action, boolean await) throws ProcessingException {
		doAction(Arrays.asList(action), await);
	}

	@Override
	public void doAction(Collection<Action> actions, boolean await) throws ProcessingException {
		CountDownLatch cdl = new CountDownLatch(actions.size());
		for (Action action : actions) {
			if (logger.isDebugEnabled()) {
				logger.debug("Action :" + action.toString() + "has been added to executor");
			}
			TaskFactory.queueTask(pool, action, cdl);
		}
		// wait for finishing tasks
		if (await) {
			try {
				cdl.await();
			} catch (InterruptedException e) {
				throw new ProcessingException("Executor has been interrupted!", e);
			}
		}
	}

	@Override
	public int threadsNum() {
		return this.threadsNum;
	}

	@Override
	public void shutdownAwait() {
		pool.shutdown();
		try {
			while (!pool.awaitTermination(1, TimeUnit.MINUTES)) {
				logger.info("Awaiting pool " + name + " current tasks termination...");
			}
			logger.info("Execution pool [" + name + "] is stopped with all its tasks!");
		} catch (InterruptedException e) {
			logger.warn("Execution pool has been iterrupted!");
		}
	}

	@Override
	public void shutdown() {
		pool.shutdown();
		logger.info("Execution pool [" + name + "] is stopped , but current tasks are still executing!");
	}

}

final class TaskFactory {
	public static void queueTask(final ExecutorService executor, final Action action, final CountDownLatch latch) {
		executor.execute(new Runnable() {
			@Override
			public void run() {
				try {
					action.execute();
					if (PooledExecutor.logger.isDebugEnabled()) {
						PooledExecutor.logger.debug("Action : " + action.toString() + " has been executed!");
					}
				} finally {
					latch.countDown();
				}
			}
		});
	}
}