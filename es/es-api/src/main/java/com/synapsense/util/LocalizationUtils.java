package com.synapsense.util;

import java.util.Locale;

public class LocalizationUtils {

	private static final String localizePattern = "$localize(%1$s%2$s)$";

	/**
	 * @param id
	 *            - id of the string in the resource bundle
	 * @param args
	 *            - Arguments referenced by the format specifiers in the format
	 *            string if string in the resource bundle is a format string,
	 * @return
	 */
	public static String getLocalizedString(String id, String... args) {

		StringBuilder sb = new StringBuilder();
		for (String s : args) {
			sb.append(",").append("\"" + escapeQuotes(s) + "\"");
		}

		return String.format(localizePattern, id, sb.toString());
	}
	
	private static String escapeQuotes(String str) {
		if (str == null) {
			return null;
		}
		return str.replaceAll("\"", "\\\"");
	}

	public static Locale getLocale(String localeId) {
		String[] tokens = localeId.split("_");
		Locale locale = null;

		if (tokens.length == 1) {
			locale = new Locale(tokens[0]);
		} else if (tokens.length == 2) {
			locale = new Locale(tokens[0], tokens[1]);
		} else if (tokens.length == 3) {
			locale = new Locale(tokens[0], tokens[1], tokens[2]);
		}
		return locale;
	}

}
