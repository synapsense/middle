package com.synapsense.util.convimpl;

import java.io.Serializable;

import com.synapsense.dto.ValueTO;

public class ValueConverter implements Serializable {

	private static final long serialVersionUID = 1412106629605272163L;

	public static boolean isConvertible(Class<?> clazz) {
		return Number.class.isAssignableFrom(clazz) || ValueTO.class.isAssignableFrom(clazz);
	}

	public static Double toDouble(Object value) {
		if (value == null)
			return null;
		if (value instanceof Number)
			return ((Number) value).doubleValue();
		throw new IllegalArgumentException("Cannot convert value of class " + value.getClass());
	}

	public static Object fromDouble(Double value, Class<?> clazz) {
		if (value == null)
			return null;
		if (clazz.equals(Integer.class))
			return value.intValue();
		if (clazz.equals(Long.class))
			return value.longValue();
		if (clazz.equals(Double.class))
			return value;
		throw new IllegalArgumentException("Cannot convert value of class " + value.getClass());
	}
}
