package com.synapsense.exception;

public class AcApiException extends Exception {
	private static final long serialVersionUID = 1L;

	public AcApiException(String desc) {
		super(desc);
	}

	public AcApiException(String desc, Throwable cause) {
		super(desc, cause);
	}
}

