package com.synapsense.exception;

/**
 * @author anechaev this exception may be thrown by DRulesEngine service when it
 *         instantiates rules
 */
public final class RuleInstantiationException extends DREException {
	public RuleInstantiationException(String desc) {
		super(desc);
	}

	public RuleInstantiationException(String desc, Throwable reason) {
		super(desc, reason);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -7023175890814833718L;

}
