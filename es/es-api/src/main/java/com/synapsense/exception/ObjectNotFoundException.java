package com.synapsense.exception;

import javax.ejb.ApplicationException;

import com.synapsense.dto.TO;

/**
 * Thrown to indicate that object which client is trying to use does not exist
 * 
 * @author shabanov
 */
@ApplicationException(rollback = false)
public class ObjectNotFoundException extends EnvException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3251664900650149417L;

	/**
	 * the object which does not exist
	 */
	private TO<?> object;

	public ObjectNotFoundException(TO<?> to) {
		this(to, (Throwable) null);
	}

	public ObjectNotFoundException(TO<?> to, Throwable cause) {
		super("Object [ " + to.toString() + "] does not exist", cause);
		this.object = to;
	}

	public ObjectNotFoundException(String string) {
		super(string);
	}

	public TO<?> getObject() {
		return object;
	}

}
