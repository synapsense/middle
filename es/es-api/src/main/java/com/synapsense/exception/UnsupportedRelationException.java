package com.synapsense.exception;

import javax.ejb.ApplicationException;

/**
 * Thrown to indicate invalid relationship between env.objects <br>
 * 
 * @author shabanov
 */
@ApplicationException(rollback = false)
public class UnsupportedRelationException extends EnvException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2476386592158267842L;

	public UnsupportedRelationException(String message) {
		super(message);
	}

	public UnsupportedRelationException(String message, Throwable e) {
		super(message, e);
	}

	public UnsupportedRelationException(Throwable e) {
		super(e);
	}

}
