package com.synapsense.exception;

import javax.ejb.ApplicationException;

/**
 * Thrown when created alert type already exists and only update and delete
 * operation can be applicable. Holds alert type name as a field and provide
 * access to it.
 * 
 * @author shabanov
 * 
 */
@ApplicationException(rollback = true)
public class AlertTypeAlreadyExistsException extends Exception {
	private static final long serialVersionUID = -4156705528634985933L;

	private String alertType;

	public AlertTypeAlreadyExistsException(String alertTypeName) {
		this(alertTypeName, null);
	}

	public AlertTypeAlreadyExistsException(String alertTypeName, Throwable cause) {
		super("Alert type [" + alertTypeName + "] already exists", cause);
		this.alertType = alertTypeName;
	}

	public String getAlertType() {
		return alertType;
	}
}
