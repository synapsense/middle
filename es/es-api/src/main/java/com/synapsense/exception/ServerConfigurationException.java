package com.synapsense.exception;

public class ServerConfigurationException extends Exception {

	private static final long serialVersionUID = 2350322565007879349L;

	public ServerConfigurationException() {
		super();
	}

	public ServerConfigurationException(String arg0) {
		super(arg0);
	}

	public ServerConfigurationException(Throwable arg0) {
		super(arg0);
	}

	public ServerConfigurationException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}
}
