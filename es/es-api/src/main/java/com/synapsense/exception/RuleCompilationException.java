package com.synapsense.exception;

/**
 * @author anechaev this exception may be thrown by DRulesEngine service when it
 *         compiles rules
 */
public final class RuleCompilationException extends DREException {
	public RuleCompilationException(String desc) {
		super(desc);
	}

	public RuleCompilationException(String desc, Throwable reason) {
		super(desc, reason);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -7023175890814833718L;

}
