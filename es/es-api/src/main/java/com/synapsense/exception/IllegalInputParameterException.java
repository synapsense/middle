package com.synapsense.exception;

import javax.ejb.ApplicationException;

/**
 * Implementation of <code>EnvSystemException</code> indicating that some
 * parameter passed to the certain service was invalid (some parameter
 * constraints were fired)
 * 
 * @author shabanov
 * 
 */
@ApplicationException(rollback = true)
public class IllegalInputParameterException extends EnvSystemException {

	private static final long serialVersionUID = -8276194927625865387L;

	public IllegalInputParameterException(String message) {
		super(message);
	}

}
