package com.synapsense.exception;

import javax.ejb.ApplicationException;

/**
 * Thrown to indicate that the code has attempted to cast a value of property <br>
 * to a class of which it is not an instance.
 */
@ApplicationException(rollback = false)
public class UnableToConvertPropertyException extends EnvException {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8135360064926565148L;

	public UnableToConvertPropertyException(String property, Class<?> expectedClass, Throwable cause) {
		super("Property[" + property + "] cannot be casted to [" + expectedClass.getName() + "]", cause);
	}

	public UnableToConvertPropertyException(String property, Throwable cause) {
		super("Property[" + property + "] cannot be casted", cause);
	}

	public UnableToConvertPropertyException(String property, String message) {
		super("Property[" + property + "] cannot be casted." + message);
	}

	public UnableToConvertPropertyException(String property, String message, Throwable cause) {
		super("Property[" + property + "] cannot be casted." + message, cause);
	}

}
