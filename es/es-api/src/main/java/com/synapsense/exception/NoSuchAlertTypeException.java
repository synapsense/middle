package com.synapsense.exception;

import javax.ejb.ApplicationException;

@ApplicationException(rollback = true)
public class NoSuchAlertTypeException extends Exception {
	private static final long serialVersionUID = -1964408232401631236L;

	private String alertType;

	public NoSuchAlertTypeException(String alertTypeName) {
		this(alertTypeName, null);
	}

	public NoSuchAlertTypeException(String alertTypeName, Throwable cause) {
		super("No such alert type [" + alertTypeName + "]", cause);
		this.alertType = alertTypeName;
	}

	public String getAlertType() {
		return alertType;
	}
}
