/**
 * 
 */
package com.synapsense.exception;

import javax.ejb.ApplicationException;

/**
 * @author aborisov
 * 
 */
@ApplicationException(rollback = true)
public class UserManagementException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8551067620393386307L;

	public UserManagementException() {
		super();
	}

	public UserManagementException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public UserManagementException(String arg0) {
		super(arg0);
	}

	public UserManagementException(Throwable arg0) {
		super(arg0);
	}

}
