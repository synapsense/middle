package com.synapsense.exception;

import javax.ejb.ApplicationException;

/**
 * Thrown to indicate that the code which creates object <br>
 * does not provide all neccessary properties
 * 
 * @author shabanov
 * 
 */
@ApplicationException(rollback = true)
public class InvalidInitialDataException extends EnvException {

	public InvalidInitialDataException(String message, Throwable e) {
		super(message, e);
	}

	public InvalidInitialDataException(String message) {
		super(message);
	}

	public InvalidInitialDataException(Throwable e) {
		super(e);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -3062001711507689710L;

}
