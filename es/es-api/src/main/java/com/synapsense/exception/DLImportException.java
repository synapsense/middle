package com.synapsense.exception;

/**
 * Root exception class for DL Import Service
 * 
 * @author Oleg Stepanov
 * 
 */
public class DLImportException extends Exception {

	private static final long serialVersionUID = -4804324757190445428L;

	public DLImportException() {
		super();
	}

	public DLImportException(String message) {
		super(message);
	}

	public DLImportException(Throwable cause) {
		super(cause);
	}

	public DLImportException(String message, Throwable cause) {
		super(message, cause);
	}

}
