package com.synapsense.exception;

import javax.ejb.ApplicationException;

/**
 * Thrown to indicate that property <br>
 * which client code is trying to use does not exist
 * 
 * @author shabanov
 */
@ApplicationException(rollback = false)
public class PropertyNotFoundException extends EnvException {

	private static final long serialVersionUID = 2116298717090542553L;

    private String property;

	public PropertyNotFoundException(String property) {
		this(property, (Throwable) null);
	}

	public PropertyNotFoundException(String property, Throwable cause) {
		super("Property [" + property + "] does not exist", cause);
        this.property = property;
	}

	public PropertyNotFoundException(String object, String property) {
		super("Property:" + property + " of object:" + object + " does not exist");
        this.property = property;
	}

    public String getProperty() {
        return property;
    }
}
