package com.synapsense.exception;

import javax.ejb.ApplicationException;

/**
 * Base class for environment system exceptions. Such exceptions are not
 * recoverable.
 * 
 * @author shabanov
 * 
 */
@ApplicationException(rollback = true)
public class EnvSystemException extends RuntimeException {

	private static final long serialVersionUID = 5795212484866168330L;

	public EnvSystemException(String message, Throwable cause) {
		super(message, cause);
	}

	public EnvSystemException(String message) {
		this(message, null);
	}

	public EnvSystemException(Throwable cause) {
		this("", cause);
	}

}
