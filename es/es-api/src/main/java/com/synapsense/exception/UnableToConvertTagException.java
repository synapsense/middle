package com.synapsense.exception;

import javax.ejb.ApplicationException;

/**
 * Thrown to indicate that the code has attempted to cast a value of tag <br>
 * to a class of which it is not an instance.
 */
@ApplicationException(rollback = false)
public class UnableToConvertTagException extends EnvException {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8135360064926565148L;

	public UnableToConvertTagException(String tagName, String currentTagType, String expectedTagType) {
		super("Tag[" + tagName + "] with type[ " + currentTagType + "] cannot be casted to [" + expectedTagType + "]");
	}

	public UnableToConvertTagException(String tagName, String currentTagType, String expectedTagType, Exception exc) {
		super("Tag[" + tagName + "] with type[ " + currentTagType + "] cannot be casted to [" + expectedTagType + "]",
		        exc);
	}

}