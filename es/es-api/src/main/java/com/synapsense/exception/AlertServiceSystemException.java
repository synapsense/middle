package com.synapsense.exception;

import javax.ejb.ApplicationException;

@ApplicationException(rollback = true)
public class AlertServiceSystemException extends EnvSystemException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3050706356769281863L;

	public AlertServiceSystemException(String message, Throwable cause) {
		super(message, cause);
	}

	public AlertServiceSystemException(String message) {
		this(message, null);
	}

	public AlertServiceSystemException(Throwable cause) {
		this("", cause);
	}

}
