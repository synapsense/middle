package com.synapsense.exception;

/**
 * This exception is thrown by DrulesEngine service when the attached object has
 * some fields not set (nulls). It is a drools feature (==bug)
 * 
 * @author anechaev
 * 
 */
public class MalformedObjectException extends DREException {
	public MalformedObjectException(String desc) {
		super(desc);
	}

	public MalformedObjectException(String desc, Throwable reason) {
		super(desc, reason);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -8836880748149710932L;

}
