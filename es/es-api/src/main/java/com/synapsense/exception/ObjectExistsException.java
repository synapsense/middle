package com.synapsense.exception;

import javax.ejb.ApplicationException;

import com.synapsense.dto.TO;

/**
 * Thrown to indicate that object <br>
 * which client code is trying to create already exists
 * 
 * @author shabanov
 */
@ApplicationException(rollback = true)
public class ObjectExistsException extends EnvException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6041029965670676897L;

	/**
	 * the object which already exists
	 */
	private TO<?> object;

	public ObjectExistsException(String string) {
		this(string, (Throwable) null);
	}

	public ObjectExistsException(String string, Throwable cause) {
		super(string, cause);
	}

	public ObjectExistsException(TO<?> to) {
		this(to, (Throwable) null);
	}

	public ObjectExistsException(TO<?> to, Throwable cause) {
		this("Object [ " + to.toString() + "] already exists", cause);
		this.object = to;
	}

	public TO<?> getObject() {
		return object;
	}

}
