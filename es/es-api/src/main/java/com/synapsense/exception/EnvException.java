package com.synapsense.exception;

import javax.ejb.ApplicationException;

@ApplicationException(rollback = false)
public class EnvException extends Exception {

	private static final long serialVersionUID = 8988403560092297613L;

	public EnvException() {
		super();
	}

	public EnvException(String message, Throwable cause) {
		super(message, cause);
	}

	public EnvException(String message) {
		super(message);
	}

	public EnvException(Throwable cause) {
		super(cause);
	}

}
