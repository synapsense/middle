package com.synapsense.exception;

public class DREException extends Exception {
	public DREException(String desc) {
		super(desc);
	}

	public DREException(String desc, Throwable reason) {
		super(desc, reason);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -2139401998805986346L;
}
