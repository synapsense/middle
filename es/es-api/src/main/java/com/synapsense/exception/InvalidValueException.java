package com.synapsense.exception;

import javax.ejb.ApplicationException;

@ApplicationException(rollback = true)
public class InvalidValueException extends EnvSystemException {

    public InvalidValueException(String message) {
        super(message);
    }
}
