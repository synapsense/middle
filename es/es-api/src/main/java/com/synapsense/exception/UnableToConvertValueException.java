package com.synapsense.exception;

import javax.ejb.ApplicationException;

/**
 * Thrown to indicate that the code has attempted to cast a value <br>
 * to a class of which it is not an instance.
 */

@ApplicationException(rollback = false)
public class UnableToConvertValueException extends EnvException {

	private static final long serialVersionUID = 8381566232906016022L;

	public UnableToConvertValueException(String value, Class<?> expectedClass, Throwable cause) {
		super("Value[" + value + "] cannot be casted to [" + expectedClass.getName() + "]", cause);
	}

	public UnableToConvertValueException(String value, Throwable cause) {
		super("Value[" + value + "] cannot be casted", cause);
	}

	public UnableToConvertValueException(String value, String message) {
		super("Value[" + value + "] cannot be casted." + message);
	}

	public UnableToConvertValueException(String value, String message, Throwable cause) {
		super("Value[" + value + "] cannot be casted." + message, cause);
	}
}
