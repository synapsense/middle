package com.synapsense.exception;

public class CalculationFailedException extends EnvException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7847708421756111511L;

	public CalculationFailedException(String string, Throwable cause) {
		super(string, cause);
	}

	public CalculationFailedException(String string) {
		super(string);
	}

	public CalculationFailedException(Throwable cause) {
		super(cause);
	}

}
