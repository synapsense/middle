package com.synapsense.exception;

import javax.ejb.ApplicationException;

/**
 * Thrown to indicate that tag <br>
 * which client code is trying to use does not exist
 * 
 * @author aalexeev
 */
@ApplicationException(rollback = false)
public class TagNotFoundException extends EnvException {

	private static final long serialVersionUID = 2116298717090542553L;

	public TagNotFoundException(String tag) {
		this(tag, (Throwable) null);
	}

	public TagNotFoundException(String tag, Throwable cause) {
		super("Tag [" + tag + "] does not exist", cause);
	}

	public TagNotFoundException(String property, String tag) {
		super("Tag: " + tag + " of property: " + property + " does not exist");
	}

	public TagNotFoundException(String objectTypeName, String property, String tag) {
		super("Tag: " + tag + " of property: " + property + " of object: " + objectTypeName + " does not exist");
	}
}