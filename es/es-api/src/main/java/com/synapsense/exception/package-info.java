/**
 * Contains definition of exceptions
 * thrown by Environment API entities.
 */
package com.synapsense.exception;