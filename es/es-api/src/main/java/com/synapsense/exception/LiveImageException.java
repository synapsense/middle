package com.synapsense.exception;

import javax.ejb.ApplicationException;

/**
 * Indicates problems of executing "live imaging" service's methods.
 * 
 * @author shabanov
 * 
 */
@ApplicationException(rollback = true)
public class LiveImageException extends EnvSystemException {

	private static final long serialVersionUID = 8605927118585207655L;

	public LiveImageException(String message) {
		super(message);
	}

	public LiveImageException(String message, Throwable cause) {
		super(message, cause);
	}

}
