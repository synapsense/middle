package com.synapsense.exception;

import javax.ejb.ApplicationException;

@ApplicationException(rollback = true)
public class NoSuchAlertPriorityException extends Exception {

	private static final long serialVersionUID = 3410108353455482141L;

	private String priorityName;

	public NoSuchAlertPriorityException(String priorityName) {
		super("There is no such priority with name : " + priorityName);
		this.priorityName = priorityName;
	}

	public String getPriorityName() {
		return priorityName;
	}

}
