package com.synapsense.service.nsvc;

/**
 * Interface for event processors and filters.
 * <p>
 * If you want your processor/filter to accept events of any type then implement
 * this interface directly. Otherwise, implement the strongly typed interface
 * provided with a particular event then plug it into the framework using
 * {@link DispatchingProcessor} or {@link CompositeFilter}. If you are
 * implementing a filter, implementation rules are the same for whichever way
 * you choose (see comments for {@link #process(Event)}).
 * 
 * @author Oleg Stepanov
 * 
 */
public interface EventProcessor {

	/**
	 * Process or filter an event.
	 * <p>
	 * Processors implementation is up to clients as well as the way they treat
	 * the result. The picture is different for filters as long as they are
	 * passed and used by Notification Service on the server side. Filter
	 * implementation rules are
	 * <ul>
	 * <li>Returning {@code null} means bloking the event.</li>
	 * <li>Returning non-null means that the event passes the filter. In this
	 * case filter may pass the original event or modify it. <b>Note:</b>
	 * implementation must not change the original event; a deep copy must be
	 * created and modified instead.</li>
	 * </ul>
	 * 
	 * @param e
	 *            event to be processed/filtered.
	 * @return Processing/filtering result. For filters returning {@code null}
	 *         means blocking the event.
	 */
	Event process(Event e);

}
