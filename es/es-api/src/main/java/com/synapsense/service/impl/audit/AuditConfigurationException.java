/**
 * 
 */
package com.synapsense.service.impl.audit;

/**
 * @author aborisov
 * 
 */
public class AuditConfigurationException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5584970057183973562L;

	/**
	 * 
	 */
	public AuditConfigurationException() {
		super();
	}

	/**
	 * @param message
	 */
	public AuditConfigurationException(String message) {
		super(message);
	}

	/**
	 * @param cause
	 */
	public AuditConfigurationException(Throwable cause) {
		super(cause);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public AuditConfigurationException(String message, Throwable cause) {
		super(message, cause);
	}

}
