/**
 * 
 */
package com.synapsense.service.impl.messages;

/**
 * @author Alexander Borisov
 * 
 */
public class TpsMessage implements BusMessage<String, Integer> {
	private int id;
	private String data;

	private static final long serialVersionUID = -5843425844722922204L;

	public TpsMessage(int id, String data) {
		super();
		this.id = id;
		this.data = data;
	}

	@Override
	public Integer getId() {
		return id;
	}

	@Override
	public String getValue() {
		return data;
	}

	@Override
	public void setId(Integer id) {
		this.id = id;

	}

	@Override
	public void setValue(String value) {
		this.data = value;
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer(20);
		sb.append("Id=");
		sb.append(id);
		sb.append("\n");

		sb.append("message=");
		sb.append(data);
		sb.append("\n");

		return sb.toString();
	}

	@Override
	public Type getType() {
		return Type.TPD;
	}

}
