package com.synapsense.service.impl.devmanager;

import com.synapsense.dto.TO;
import com.synapsense.service.impl.messages.base.DuoConfData;
import com.synapsense.service.impl.messages.base.NodeVersionData;
import com.synapsense.service.impl.messages.base.PhaseData;
import com.synapsense.service.impl.messages.base.PlugData;
import com.synapsense.service.impl.messages.base.RefinedAlertData;
import com.synapsense.service.impl.messages.base.SandcrawlerConfData;
import com.synapsense.service.impl.messages.base.SensorData;
import com.synapsense.service.impl.messages.base.StatisticData;

public interface DeviceEventListener {
	void onSensorData(TO<?> objID, SensorData data);

	void onStatistic(TO<?> objID, StatisticData data);

	void onGWStatus(TO<?> objID, int status);

	void onAlert(TO<?> objID, RefinedAlertData data);

	void onNodeVersion(TO<?> objID, NodeVersionData data);

	void onPowerPackConf(TO<?> objID, SandcrawlerConfData data);

	void onPlugData(TO<?> objId, PlugData data);

	void onPhaseData(TO<?> objId, PhaseData data);

	void onDuoConf(TO<?> objId, DuoConfData data);
}
