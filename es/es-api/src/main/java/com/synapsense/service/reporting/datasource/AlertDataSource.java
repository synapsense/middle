package com.synapsense.service.reporting.datasource;

import com.synapsense.dto.*;
import com.synapsense.exception.AlertServiceSystemException;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.exception.PropertyNotFoundException;
import com.synapsense.exception.UnableToConvertPropertyException;
import com.synapsense.service.Environment;
import com.synapsense.service.reporting.olap.RangeSet;
import com.synapsense.service.reporting.olap.custom.Cube;
import com.synapsense.util.CollectionUtils;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * The <code>AlertDataSource</code> class implements Table Datasource for accessing Alert Data.<br>
 * Example: new AlertDataSource($P{Status}, $P{Priority}, $P{Start Date}, $P{End Date})
 * @author Grigory Ryabov, Alexander Pakhunov
 * 
 */
public class AlertDataSource extends AbstractJRDataSource {

	private static HashMap<String, String> namesMap = new HashMap<>();
	public static final String ALERT_DESCRIPTION_TYPE_NAME = "ALERT_TYPE";
	public static final String DEFAULT_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
	public static final String DEFAULT_ALERT_STATUS_TYPE = "ALL";
	public static final String DEFAULT_ALERT_PRIORITY_TYPE = "ALL";
	private static String mPriority;
	
	static {
		namesMap.put("Status", "status");
		namesMap.put("Alert Name", "name");
		namesMap.put("Alert Message", "message");
		namesMap.put("Priority", "priority");
		namesMap.put("Object", "hostTO");
		namesMap.put("Timestamp", "time");
		namesMap.put("Site", "site");
	}
	
	private Iterator<CollectionTO> values;
	
	private CollectionTO current;

	public AlertDataSource() throws Exception {
		values = getData(DEFAULT_ALERT_STATUS_TYPE, new Date(0L), new Date()).iterator();
	}
	
	public AlertDataSource(Date startDate) throws Exception {
		values = getData(DEFAULT_ALERT_STATUS_TYPE, startDate, new Date()).iterator();
	}

	public AlertDataSource(String status, String priority, Date startDate, Date endDate) throws Exception {
		mPriority = priority;
		values = getData(status, startDate, endDate).iterator();
	}
	
	public AlertDataSource(String status, String priority, RangeSet period) throws Exception {
		this(status, priority
				, Cube.TIME.getCalendarOfMember(period.getStartMember()).getTime()
				, Cube.TIME.getCalendarOfMember(period.getEndMember()).getTime());
	}


	public AlertDataSource(String startYear, String startMonth, String startDay, String endYear, String endMonth, String endDay) throws Exception {
		values = getData(DEFAULT_ALERT_STATUS_TYPE, setDate(startYear, startMonth, startDay), setDate(endYear, endMonth, endDay)).iterator();
	}

	
	
	@Override
	public Object getFieldValue(JRField field) throws JRException {
		String name = namesMap.get(field.getName());
		if (name != null) {
			ValueTO vTo = current.getSinglePropValue(name);
			if (vTo != null) {
				return vTo.getValue();
			}
		}
		return null;
	}

	@Override
	public boolean next() throws JRException {
		if (values.hasNext()) {
			current = values.next();
			return true;
		}
		return false;
	}
	
	public static Collection<CollectionTO> getAlertsData(Collection<Alert> alerts) 
			throws ObjectNotFoundException, PropertyNotFoundException, UnableToConvertPropertyException{
		Collection<CollectionTO> result = CollectionUtils.newList();
		if (mPriority == null){
			mPriority = DEFAULT_ALERT_PRIORITY_TYPE;
		}
		for (Alert alert : alerts){
			TO<?> hostObj = alert.getHostTO();
			CollectionTO alertData = new CollectionTO(hostObj);
			alertData.addAll(alertToProps(alert));
			if (mPriority.equals(DEFAULT_ALERT_PRIORITY_TYPE) || alertData.getSinglePropValue("priority").getValue().equals(mPriority)){
				result.add(alertData);
			}
		}
		return result;		
	}
	
	public static List<ValueTO> alertToProps(Alert alert) throws ObjectNotFoundException, PropertyNotFoundException, UnableToConvertPropertyException {
		List<ValueTO> values = CollectionUtils.newArrayList(10);
		values.add(new ValueTO("name", alert.getName()));

		TO<?> alertTypeRef = findAlertTypeByName(env, alert.getAlertType());
		if (alertTypeRef == null) {
			throw new AlertServiceSystemException("Alert type  :" + alert.getAlertType() + " does not exist");
		}
		values.add(new ValueTO("type", alertTypeRef));
		String dateFormat = null;
		Map<String, Object> preferences = userManagement.getUserPreferences(userManagement.getUser("admin"));
		dateFormat = unquoteString((String)preferences.get("DateFormat"));
		SimpleDateFormat formatter;
		if (dateFormat != null){
			formatter = new SimpleDateFormat(dateFormat);
		} else {
			formatter = new SimpleDateFormat(DEFAULT_DATE_FORMAT);
		}
		
		String timestamp = formatter.format(new Date(alert.getAlertTime()));
		values.add(new ValueTO("time", timestamp));
		values.add(new ValueTO("status", alert.getStatus().toString()));
		values.add(new ValueTO("message", alert.getFullMessage()));
		TO<?> hostObj = alert.getHostTO();
		if (!hostObj.equals(TOFactory.EMPTY_TO)){
			Collection<TO<?>> tos = new HashSet<TO<?>>();
			tos.add(hostObj);
			CollectionTO colTo = env.getPropertyValue(tos, new String[] {"name", "location"}).iterator().next(); 
			String objectName = (String) colTo.getSinglePropValue("name").getValue();
			values.add(new ValueTO("hostTO", objectName));
		} else {
			values.add(new ValueTO("hostTO", "System"));
		}
		values.add(new ValueTO("hostTO", TOFactory.EMPTY_TO.equals(hostObj) ? "System" : TOFactory.getInstance().saveTO(hostObj)));
		values.add(new ValueTO("acknowledgement", alert.getAcknowledgement()));
		Date ackTime = alert.getAcknowledgementTime();
		values.add(new ValueTO("acknowledgementTime", ackTime == null ? null : ackTime.getTime()));
		values.add(new ValueTO("user", alert.getUser()));
		values.add(new ValueTO("priority", alertService.getAlertType(alert.getAlertType()).getPriority()));
		//Adding site
		Collection<TO<?>> objDCs = env.getRelatedObjects(hostObj,  "DC", false);
       	if(!objDCs.isEmpty()){
       		TO<?> dc = objDCs.iterator().next();
       		values.add(new ValueTO("site", env.getPropertyValue(dc, "name", String.class)));
       	}
		return values;
	}

	private static String unquoteString(String str) {
		if (str != null && str.length() >= 2 && str.startsWith("\"")
				&& str.endsWith("\"")) {
			return str.substring(1, str.length() - 1);
		}
		return str;
	}
	
	
	public static Date setDate(String year, String month, String day){
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.HOUR_OF_DAY, cal.getActualMinimum(Calendar.HOUR_OF_DAY));
		cal.set(Calendar.MINUTE, cal.getActualMinimum(Calendar.MINUTE));
		cal.set(Calendar.SECOND, cal.getActualMinimum(Calendar.SECOND));
		cal.set(Calendar.MILLISECOND, cal.getActualMinimum(Calendar.MILLISECOND));
		cal.set(Integer.valueOf(year), Integer.valueOf(month) - 1, Integer.valueOf(day));
		return cal.getTime();
	}
	
	/**
	 * Gets all active alerts in system
	 * @return
	 * @throws ObjectNotFoundException
	 */
	public static TO<?> findAlertTypeByName(Environment env, String typeName) { 
		ValueTO[] criteria = new ValueTO[] { new ValueTO("name", typeName) };
		Collection<TO<?>> alertDescriptors = env.getObjects(ALERT_DESCRIPTION_TYPE_NAME, criteria);
		if (alertDescriptors.isEmpty()) {
			return null;
		}
		if (alertDescriptors.size() > 1) {
			throw new AlertServiceSystemException("Mupltiple alert types having the same name {" + typeName + "} , "
			        + alertDescriptors);
		}
		
		return alertDescriptors.iterator().next();
	}
	
	public static Collection<CollectionTO> getData(String status, Date startDate, Date endDate) throws Exception {
		AlertFilterBuilder builder = new AlertFilterBuilder().between(startDate, endDate);
		builder = getAlertFilter(builder, AlertStatusType.getTypeByName(status));
		Collection<Alert> alerts = alertService.getAlerts(builder.build());
		return getAlertsData(alerts);
    }
	
	public static AlertFilterBuilder getAlertFilter(AlertFilterBuilder builder, AlertStatusType type){
		switch(type){
			case ALL:
				return builder;
			case OPENED:
				return builder.opened();
			case ACKNOWLEDGED:
				return builder.acknowledged();
			case DISMISSED:
				return builder.dismissed();
			case RESOLVED:
				return builder.resolved();
			case ALL_ACTIVE:
				return builder.active();
			case ALL_CLOSED:
				return builder.historical();
		}
		return null;
	}
	
	public enum AlertStatusType {
		ALL("ALL"), OPENED("OPENED"), ACKNOWLEDGED("ACKNOWLEDGED"), DISMISSED("DISMISSED"), RESOLVED("RESOLVED"), ALL_ACTIVE("ALL_ACTIVE"), ALL_CLOSED("ALL_CLOSED");
		private AlertStatusType(String name) {
			this.name = name;
		}

		private String name;
		
		public static AlertStatusType getTypeByName(String name) {
			for (AlertStatusType parameter : AlertStatusType.values()) {
				if (parameter.getName().equals(name))
					return parameter;
			}
			throw new IllegalArgumentException("Not supported Alert Status Parameter, parameter=" + name);	
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}
	}
	
	public enum AlertPriorityParameter {
		ALL("ALL"), CRITICAL("CRITICAL"), MAJOR("MAJOR"), MINOR("MINOR"), INFORMATIONAL("INFORMATIONAL");
		private AlertPriorityParameter(String name) {
			this.name = name;
		}

		private String name;
		
		public static AlertPriorityParameter getTypeByName(String name) {
			for (AlertPriorityParameter parameter : AlertPriorityParameter.values()) {
				if (parameter.getName().equals(name))
					return parameter;
			}
			throw new IllegalArgumentException("Not supported Alert Priority Parameter, parameter=" + name);	
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}
	}
	
	public static void main(String[] args) throws Exception {
		Date start = AlertDataSource.setDate("2012", "8", "1");
		Date end = AlertDataSource.setDate("2012", "9", "1");
		System.out.println(start.toString());
		@SuppressWarnings("unused")
        AlertDataSource dataSource = new AlertDataSource("ALL", "ALL", start, end);
	}
}