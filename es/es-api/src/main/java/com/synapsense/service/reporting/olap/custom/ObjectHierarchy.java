package com.synapsense.service.reporting.olap.custom;

import java.io.Serializable;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import com.synapsense.dto.TO;
import com.synapsense.service.impl.dao.to.GenericObjectTO;
import com.synapsense.service.reporting.olap.Filter;
import com.synapsense.service.reporting.olap.FilterableHierarchy;
import com.synapsense.service.reporting.olap.Member;
import com.synapsense.service.reporting.olap.SimpleMember;

public class ObjectHierarchy implements FilterableHierarchy, Serializable {

	private static final long serialVersionUID = 8357735325029599117L;

	private transient EnvironmentDataSource ds;

	private Class<?> memberType;

	public ObjectHierarchy(Class<?> memberType) {
		this.memberType = memberType;
	}

	@Override
	public String getName() {
		return Cube.DIM_OBJECT;
	}

	@Override
	public Class<?> getMemberType() {
		return memberType;
	}

	@Override
	public boolean isDescendant(String desc, String ans) {
		// That's our contract for object hierarchy since it's really difficult
		// to actually figure this out
		return true;
	}

	@Override
	public List<Member> getMembers(String level, Member parent, Filter filter) {
		if (Cube.OBJECT.getMemberType().equals(String.class)) {
			return convertNamesToMembers(
					ds.getDescendantObjectNames(level, parent, filter), level,
					parent);
		} else if (Cube.OBJECT.getMemberType().equals(GenericObjectTO.class)) {
			return convertTOsToMembers(
					ds.getDescendantObjects(level, parent, filter), level,
					parent);
		}
		return new LinkedList<Member>();
	}

	private List<Member> convertTOsToMembers(Collection<TO<?>> objects,
			String level, Member parent) {
		LinkedList<Member> members = new LinkedList<>();
		for (TO<?> object : objects) {
			members.add(new SimpleMember(this, level, object, parent));
		}
		return members;
	}

	private List<Member> convertNamesToMembers(Collection<String> names,
			String level, Member parent) {
		LinkedList<Member> members = new LinkedList<>();
		for (String name : names) {
			members.add(new SimpleMember(this, level, name, parent));
		}
		return members;
	}

	void setReportingDataSource(EnvironmentDataSource ds) {
		this.ds = ds;
	}

	@Override
	public int hashCode() {
		return 1;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() == obj.getClass())
			return true;
		return false;
	}

	@Override
	public List<Member> getMembers(Level customLevel) {
		Member parent = null;
		return getMembers(customLevel, parent);
	}

	@Override
	public List<Member> getMembers(Level customLevel, Member parent) {
		String level = customLevel.getName();
		if (Cube.OBJECT.getMemberType().equals(String.class)) {
		return convertNamesToMembers(
				ds.getDescendantObjectNames(level, parent), level, parent);
		} else if (Cube.OBJECT.getMemberType().equals(GenericObjectTO.class)) {
		return convertTOsToMembers(ds.getDescendantObjects(level, parent),
				level, parent);
		}
		return new LinkedList<Member>();
	}

	@Override
	public List<Member> getMembers(Level customLevel, String limitLevel) {
		return this.getMembers(customLevel);
	}

	@Override
	public List<Member> getSimpleMembers(Member member) {
		throw new UnsupportedOperationException("getSimpleMembers() is not supported for ObjectHierarchy");
	}
}
