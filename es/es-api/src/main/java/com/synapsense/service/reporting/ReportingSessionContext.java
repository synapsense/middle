package com.synapsense.service.reporting;

/**
 * @author : shabanov
 */
//TODO : class should work on both client and server sides
public final class ReportingSessionContext {

    private static ThreadLocal<String> userNameLocal = new InheritableThreadLocal<>();

    public static String getUserName() {
        return userNameLocal.get();
    }

    public static void setUserName(final String userName) {
        if(userName == null)
            userNameLocal.remove();
        else
            userNameLocal.set(userName);

    }

    public static void clear() {
        userNameLocal.remove();
    }

    private ReportingSessionContext() {
    }

}
