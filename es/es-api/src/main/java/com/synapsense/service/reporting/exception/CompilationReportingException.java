package com.synapsense.service.reporting.exception;

import javax.ejb.ApplicationException;

import com.synapsense.service.reporting.management.ReportingException;

@ApplicationException(rollback = false)
public class CompilationReportingException extends ReportingException {

	private static final long serialVersionUID = -3029835574783261337L;

	public CompilationReportingException() {
	}

	public CompilationReportingException(String message, Throwable cause) {
		super(message, cause);
	}

	public CompilationReportingException(String message) {
		super(message);
	}

	public CompilationReportingException(Throwable cause) {
		super(cause);
	}
}
