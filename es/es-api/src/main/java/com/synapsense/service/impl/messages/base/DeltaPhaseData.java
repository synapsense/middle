package com.synapsense.service.impl.messages.base;

public class DeltaPhaseData extends PhaseData {

	private static final long serialVersionUID = -1841184142403422356L;

	public DeltaPhaseData(int rack, int rpdu, int phase, Double voltage, long timestamp) {
		super(rack, rpdu, phase, voltage, timestamp);
	}

	@Override
	public String getAvgCurrentProp() {
		return "deltaAvgCurrent";
	}

	@Override
	public String getMaxCurrentProp() {
		return "deltaMaxCurrent";
	}
}
