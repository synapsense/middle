package com.synapsense.service.reporting.olap.custom;

public enum TimePeriodType {
	TODAY("This", new BaseLevel(Cube.TIME, Cube.LEVEL_DAY), 0)
	, YESTERDAY("Previous", new BaseLevel(Cube.TIME, Cube.LEVEL_DAY), 1)
	, THIS_WEEK("This", new WeekLevel(), 2)
	, LAST_WEEK("Previous", new WeekLevel(), 3)
	, THIS_MONTH("This", new BaseLevel(Cube.TIME, Cube.LEVEL_MONTH), 4)
	, LAST_MONTH("Previous", new BaseLevel(Cube.TIME, Cube.LEVEL_MONTH), 5)
	, CUSTOM("Custom", null, -1);
	
	private TimePeriodType(String relation, Level level, int id) {
		this.relation = relation;
		this.level = level;
		this.id = id;
	}

	private String relation;
	private Level level;
	private int id;

	public static TimePeriodType getTypeByRelationAndLevel(String relation, Level level) {
		for (TimePeriodType type : TimePeriodType.values()) {
			if (type.getRelation().equals(relation) 
					&& type.getLevel().equals(level))
				return type;
		}
		throw new IllegalArgumentException("Not supported Time Period Type with relation " + relation + " and level" + level);	
	}

	
	public static TimePeriodType getTypeById(int id) {
		for (TimePeriodType type : TimePeriodType.values()) {
			if (type.getId() == id)
				return type;
		}
		throw new IllegalArgumentException("Not supported Time Period Type with id " + id);	
	}

	public int getId() {
		return id;
	}

	public String getRelation() {
		return relation;
	}

	public Level getLevel() {
		return level;
	}	
	
}
