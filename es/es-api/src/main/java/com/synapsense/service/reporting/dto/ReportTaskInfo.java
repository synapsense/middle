package com.synapsense.service.reporting.dto;

import com.synapsense.exception.IllegalInputParameterException;
import com.synapsense.service.reporting.olap.Hierarchy;
import com.synapsense.service.reporting.olap.RangeSet;
import com.synapsense.service.reporting.olap.custom.TimeHierarchy;
import com.synapsense.util.CollectionUtils;

import java.io.Serializable;
import java.util.*;

import org.json.JSONException;
import org.json.JSONObject;

public class ReportTaskInfo implements Serializable {

	private static final long serialVersionUID = 7559852356237691370L;

	private int id;

	private String name;

	private String templateName;

    // active by default
    private int status = 1;

    private String cron;

    private String owner;

    private Set<String> users = CollectionUtils.newSet();

    private Map<String, Object> paramVals = CollectionUtils.newLinkedMap();

	// This collections is for a complex task of generation-export-email
	private Set<String> exportFormats = CollectionUtils.newLinkedSet();

	private Set<String> emailDestinations = CollectionUtils.newLinkedSet();

    // This collection is for separate email tasks
    private Map<Integer, Set<String>> emailTasks = CollectionUtils.newLinkedMap();

    // This collection is for separate export tasks
    private Map<Integer, Set<String>> exportTasks = CollectionUtils.newLinkedMap();


    private ReportTaskInfo(String templateName, String name) {
		this.templateName = templateName;
		this.name = name.trim();
	}

    private ReportTaskInfo() {
    }

    public static ReportTaskInfo newInstance() {
        return new ReportTaskInfo();
    }

    public static ReportTaskInfo newInstance(String templateName, String name) {
		return new ReportTaskInfo(templateName, name);
	}

	public ReportInfo newReportInstance(long timestamp) {
		return new ReportInfo(name, timestamp);
	}

	public ReportTaskInfo addExportFormats(Collection<String> formats) {
		exportFormats.addAll(formats);
		return this;
	}

	public ReportTaskInfo addMailRecipient(String recipient) {
		users.add(recipient);
		return this;
	}

	public ReportTaskInfo addMailRecipients(Collection<String> recipients) {
		users.addAll(recipients);
		return this;
	}

	public ReportTaskInfo addEmailDestination(String destination) {
		emailDestinations.add(destination);
		return this;
	}

	public Collection<String> getExportFormats() {
        return Collections.unmodifiableCollection(exportFormats);
	}

	public Collection<String> getEmailDestinations() {
		return Collections.unmodifiableCollection(emailDestinations);
	}

	public String getReportTemplateName() {
		return templateName;
	}

	public String getCron() {
		return cron;
	}

	public void setCron(String cron) {
		this.cron = cron;
	}

	public void addParameterValue(String name, Object value) {
		paramVals.put(name, value);
	}

	public Map<String, Object> getParametersValues() {
		return new LinkedHashMap(paramVals);
	}

	/**
	 * Used to update parameters that have relative values, such as a predefined date range of "Last Month".
	 * This is a workaround for implementations that generate the absolute value at creation time and use the instance
	 * multiple times.  See Bug 9839 for one such scenario involving reports on a recurring schedule.
	 */
	public void refreshParameters() {
		for( Map.Entry<String,Object> entry : paramVals.entrySet() ) {
			// HAXXERZ: Probably should refactor the value provider map in DbReportTaskDAO so we can just reference it.
			//          Also need to refactor so that some of these definitions are in es-api. Not having time to check
			//          for breakage and since we want to redesign Reporting soon, I didn't want to make any drastic
			//          changes or have a circular dependency with es-core. So quick hack it is!
			//switch( ReportingConstants.ReportingParameterType.getTypeByClassName( entry.getValue().getClass().getName() )

			if( entry.getValue() instanceof RangeSet ) {
				RangeSet oldRange = (RangeSet) entry.getValue();

				for( Hierarchy hierarchy : oldRange.getDimensionalities() ) {
					if( hierarchy instanceof TimeHierarchy ) {
						JSONObject jsonObject = null;
						try {
							jsonObject = new JSONObject( oldRange.toString() );
						} catch( JSONException e ) {
							throw new IllegalInputParameterException( "JSON Object is not correct: " + oldRange.toString() );
						}
						entry.setValue( RangeSet.newRangeSet( jsonObject ) );
						break;
					}
				}
			}
		}
	}

    public boolean isActive() {
        return status != 0;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }


    public void clear() {
		exportFormats.clear();
		emailDestinations.clear();
        emailTasks.clear();
        exportTasks.clear();
	}

	public Set<String> getEmailRecipients() {
		return this.users;
	}

	public String getName() {
		return name;
	}

	public void setTaskId(int taskId) {
		this.id = taskId;
	}

	public int getTaskId() {
		return id;
	}

    public ReportTaskInfo addEmailTask(int reportId, Collection<String> destinations) {
        CollectionUtils.updateSetValuedMap(emailTasks, reportId, destinations.toArray(new String[destinations.size()]));
        return this;
    }

    public Map<Integer, Set<String>> getEmailTasks() {
        return new LinkedHashMap<Integer, Set<String>>(emailTasks);
    }

    public Map<Integer, Set<String>> getExportTasks() {
        return new LinkedHashMap<Integer, Set<String>>(exportTasks);
    }

    private ReportTaskInfo addExportTask(int reportId, Collection<String> formats) {
        CollectionUtils.updateSetValuedMap(exportTasks, reportId, formats.toArray(new String[formats.size()]));
        return this;
    }

    public ReportTaskInfo addExportTask(int reportId, String format) {
        CollectionUtils.updateSetValuedMap(exportTasks, reportId, format);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ReportTaskInfo that = (ReportTaskInfo) o;

        if (status != that.status) return false;
        if (cron != null ? !cron.equals(that.cron) : that.cron != null) return false;
        if (!emailDestinations.equals(that.emailDestinations)) return false;
        if (!exportFormats.equals(that.exportFormats)) return false;
        if (!name.equals(that.name)) return false;
        if (!owner.equals(that.owner)) return false;
        if (!paramVals.equals(that.paramVals)) return false;
        if (!templateName.equals(that.templateName)) return false;
        if (!users.equals(that.users)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + templateName.hashCode();
        return result;
    }

    @Override
	public String toString() {
		return "ReportTaskInfo{" + "name='" + name + '\'' + ", templateName='" + templateName + '\'' + ", cron='"
		        + cron + '\'' + ", owner='" + owner + '\'' + '}';
	}
}
