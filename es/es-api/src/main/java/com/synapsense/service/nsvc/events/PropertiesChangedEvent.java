package com.synapsense.service.nsvc.events;

import java.util.Arrays;

import com.synapsense.dto.ChangedValueTO;
import com.synapsense.dto.TO;
import com.synapsense.service.nsvc.Event;
import com.synapsense.service.nsvc.EventDispatcher;

public class PropertiesChangedEvent implements Event {

	private static final long serialVersionUID = -7142048413177526862L;

	private final TO<?> objectTO;
	private final ChangedValueTO[] changedValues;

	public PropertiesChangedEvent(TO<?> objectTO) {
		this.objectTO = objectTO;
		changedValues = new ChangedValueTO[] {};
	}

	public PropertiesChangedEvent(TO<?> objectTO, ChangedValueTO[] changedValues) {
		this.objectTO = objectTO;
		this.changedValues = changedValues != null ? changedValues : new ChangedValueTO[] {};
	}

	@Override
	public Event dispatch(EventDispatcher ed) {
		return ed.dispatch(this);
	}

	public TO<?> getObjectTO() {
		return objectTO;
	}

	public ChangedValueTO[] getChangedValues() {
		return changedValues;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(super.toString()).append(": TO=").append(objectTO).append(", properties=")
		        .append(Arrays.toString(changedValues));
		return sb.toString();
	}

}
