package com.synapsense.service.reporting.exception;

import javax.ejb.ApplicationException;

import com.synapsense.service.reporting.management.ReportingException;

@ApplicationException(rollback = false)
public class ExportingReportingException extends ReportingException {

	private static final long serialVersionUID = -2698625818887148906L;

	public ExportingReportingException() {
	}

	public ExportingReportingException(String message, Throwable cause) {
		super(message, cause);
	}

	public ExportingReportingException(String message) {
		super(message);
	}

	public ExportingReportingException(Throwable cause) {
		super(cause);
	}
}
