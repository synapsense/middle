package com.synapsense.service;

import com.synapsense.exception.AcApiException;

public interface ErlNodeService {
	public String call_ac(String json_rpc) throws AcApiException;
}
