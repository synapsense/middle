package com.synapsense.service.nsvc.events;

public interface ObjectCreatedEventProcessor {
	ObjectCreatedEvent process(ObjectCreatedEvent e);
}
