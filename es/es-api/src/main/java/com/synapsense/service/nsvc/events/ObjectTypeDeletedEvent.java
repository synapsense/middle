package com.synapsense.service.nsvc.events;

import com.synapsense.service.nsvc.Event;
import com.synapsense.service.nsvc.EventDispatcher;

public class ObjectTypeDeletedEvent implements Event {

	private static final long serialVersionUID = 4237584257078170239L;

	private final String typeName;

	public ObjectTypeDeletedEvent(String typeName) {
		if (typeName == null)
			throw new IllegalArgumentException("Argument typeName cannot be null");
		this.typeName = typeName;
	}

	public String getTypeName() {
		return typeName;
	}

	@Override
	public Event dispatch(EventDispatcher ed) {
		return ed.dispath(this);
	}

}
