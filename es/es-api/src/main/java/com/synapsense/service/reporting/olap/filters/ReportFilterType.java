package com.synapsense.service.reporting.olap.filters;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by Grigory Ryabov on 29.01.14.
 */
public enum ReportFilterType {

    EQUALS("equals", 0),
    MORE_THAN("moreThan", -1),
    LESS_THAN("lessThan", 1);
    private final List<Number> acceptables;

    private String name;

    ReportFilterType(String name, Number... acceptables) {
        this.name = name;
        this.acceptables = Arrays.asList(acceptables);
    }

    public static ReportFilterType getTypeByName(String name) {
        for (ReportFilterType type : ReportFilterType.values()) {
            if (type.getName().equals(name))
                return type;
        }
        throw new IllegalArgumentException("Not supported Report Filter Type" + name);
    }

    public static ReportFilterType getTypeByAcceptables(Number... acceptables) {
        for (ReportFilterType type : ReportFilterType.values()) {
            if (type.getAcceptables().equals(Arrays.asList(acceptables)))
                return type;
        }
        throw new IllegalArgumentException("Not supported Report Filter Type with acceptables" + acceptables);
    }

    public String getName() {
        return name;
    }

    public List<Number> getAcceptables() {
        return Collections.unmodifiableList(acceptables);
    }
}
