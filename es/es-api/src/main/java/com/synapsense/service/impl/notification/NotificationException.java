package com.synapsense.service.impl.notification;

public class NotificationException extends Exception {

	public NotificationException(String message, Throwable cause) {
		super(message, cause);
	}

	public NotificationException(String message) {
		super(message);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -6821618149364136065L;

}
