package com.synapsense.service.reporting.olap.custom;

import com.synapsense.service.reporting.olap.Hierarchy;

public class BaseLevel implements Level {
	
	private static final long serialVersionUID = 8474235256266948560L;

	private Hierarchy hierarchy;

	private String levelName;
	
	public BaseLevel(Hierarchy hierarchy, String levelName) {
		this.hierarchy = hierarchy;
		this.levelName = levelName;
	}
	
	@Override
	public String getName() {
		return this.levelName;
	}

	@Override
	public Hierarchy getHierarchy() {
		return this.hierarchy;
	}

	@Override
	public int getMembersRange() {
		return 1;
	}

	@Override
	public String getRangeLevel() {
		return getName();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((hierarchy == null) ? 0 : hierarchy.hashCode());
		result = prime * result
				+ ((levelName == null) ? 0 : levelName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		BaseLevel other = (BaseLevel) obj;
		if (hierarchy == null) {
			if (other.hierarchy != null) {
				return false;
			}
		} else if (!hierarchy.equals(other.hierarchy)) {
			return false;
		}
		if (levelName == null) {
			if (other.levelName != null) {
				return false;
			}
		} else if (!levelName.equals(other.levelName)) {
			return false;
		}
		return true;
	}

	@Override
	public BaseLevel copy() {
		BaseLevel copy = new BaseLevel(hierarchy, levelName);
		return copy;
	}
	
}
