package com.synapsense.service.reporting.exception;

import javax.ejb.ApplicationException;

import com.synapsense.exception.EnvSystemException;

@ApplicationException(rollback = true)
public class ReportingServiceSystemException extends EnvSystemException {

	private static final long serialVersionUID = -8677956645281969658L;

	public ReportingServiceSystemException(String message, Throwable cause) {
		super(message, cause);
	}

	public ReportingServiceSystemException(String message) {
		this(message, null);
	}

	public ReportingServiceSystemException(Throwable cause) {
		this("", cause);
	}

}
