/**
 * 
 */
package com.synapsense.service.impl.messages;

import com.synapsense.dto.Alert;

/**
 * @author aborisov
 * 
 */
public class AlertMessage implements BusMessage<Alert, Integer> {

	private static final long serialVersionUID = 4193069783408243697L;
	private Integer id;
	private Alert data;

	public AlertMessage(Integer id, Alert data) {
		this.id = id;
		this.data = data;
	}

	@Override
	public Integer getId() {
		return id;
	}

	@Override
	public Type getType() {
		return Type.ALERT;
	}

	@Override
	public Alert getValue() {
		return data;
	}

	@Override
	public void setId(Integer id) {
		this.id = id;
	}

	@Override
	public void setValue(Alert value) {
		this.data = value;
	}

}
