package com.synapsense.service.impl.exception;

import javax.ejb.ApplicationException;

import com.synapsense.exception.EnvSystemException;

/**
 * Thrown to indicate DAL system errors
 * 
 * @author shabanov
 */
@ApplicationException(rollback = true)
public class DALSystemException extends EnvSystemException {

	public DALSystemException(String message) {
		super(message);
	}

	public DALSystemException(String message, Throwable e) {
		super(message, e);
	}

	private static final long serialVersionUID = 3468857463952085285L;

}
