package com.synapsense.service.reporting.olap.custom;

import static com.synapsense.service.reporting.olap.custom.Cube.*;

public class WeekTimeHierarchy extends TimeHierarchy {

	private static final long serialVersionUID = 5319436245329891509L;

	public WeekTimeHierarchy() {
		super(DIM_WEEK_TIME, Integer.class, LEVEL_YEAR, LEVEL_WEEK);
	}

}
