package com.synapsense.service;

import com.synapsense.util.unitconverter.UnitResolvers;
import com.synapsense.util.unitconverter.UnitSystems;

/**
 * Unit Systems Service provides information about dimensions of objects'
 * properties, their units of measurement and conversions between different unit
 * systems.
 * 
 * @author Oleg Stepanov
 * 
 */
public interface UnitSystemsService {

	UnitSystems getUnitSystems();

	UnitResolvers getUnitReslovers();

}
