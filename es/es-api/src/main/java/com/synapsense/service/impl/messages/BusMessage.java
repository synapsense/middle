/**
 * 
 */
package com.synapsense.service.impl.messages;

import java.io.Serializable;

/**
 * @author Alexander Borisov
 * 
 */
public interface BusMessage<TYPE extends Serializable, ID> extends Serializable {
	public ID getId();

	public void setId(ID id);

	public TYPE getValue();

	public void setValue(TYPE value);

	public Type getType();
}
