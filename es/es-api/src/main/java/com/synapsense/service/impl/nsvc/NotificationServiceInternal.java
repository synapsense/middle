package com.synapsense.service.impl.nsvc;

import com.synapsense.service.nsvc.NotificationService;

public interface NotificationServiceInternal extends NotificationService, EventNotifier {

}
