package com.synapsense.service.reporting.olap.filters;

import com.synapsense.service.reporting.datasource.ReferenceObjectType;
import com.synapsense.service.reporting.olap.Filter;


@SuppressWarnings("rawtypes")
public class Filters {

	public static Filter equalsTo(String property, Comparable value) {
		return new ComparatorFilter(property, value, 0);
	}

	public static Filter moreThan(String property, Comparable value) {
		return new ComparatorFilter(property, value, -1);
	}

    public static Filter moreThan(String property, Comparable value, boolean calculated) {
        return new ComparatorFilter(property, value, calculated, -1);
    }

    public static Filter moreThan(ReferenceObjectType objectType, Comparable value) {
        return new ComparatorFilter(objectType, value, -1);
    }

    public static Filter lessThan(ReferenceObjectType objectType, Comparable value) {
        return new ComparatorFilter(objectType, value, 1);
    }
    
	public static Filter lessThan(String property, Comparable value) {
		return new ComparatorFilter(property, value, 1);
	}
	
	public static Filter lessThan(String property, Comparable value, boolean calculated) {
		return new ComparatorFilter(property, value, calculated, 1);
	}
	
	public static Filter moreThanOrEqualsTo(String property, Comparable value) {
		return new ComparatorFilter(property, value, -1, 0);
	}

	public static Filter lessThanOrEqualsTo(String property, Comparable value) {
		return new ComparatorFilter(property, value, 1, 0);
	}

	public static Filter notEqualTo(String property, Comparable value) {
		return new ComparatorFilter(property, value, 1, -1);
	}

	public static Filter or(Filter... filters) {
		return new OrFilter(filters);
	}

	public static Filter and(Filter... filters) {
		return new AndFilter(filters);
	}

	private Filters() {
	}
}
