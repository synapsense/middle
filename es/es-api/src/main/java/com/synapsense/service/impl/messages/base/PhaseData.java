package com.synapsense.service.impl.messages.base;

import java.io.Serializable;

public class PhaseData implements Serializable {

	private static final long serialVersionUID = 5524390173268656327L;

	private int rack;
	private int rpdu;
	private int phase;
	private Double voltage;
	private long timestamp;

	private Double avgCurrent;
	private Double maxCurrent;
	private Double demandPower;

	public PhaseData(int rack, int rpdu, int phase, Double voltage, long timestamp) {
		super();
		this.rack = rack;
		this.rpdu = rpdu;
		this.phase = phase;
		this.voltage = voltage;
		this.timestamp = timestamp;
	}

	public Double getAvgCurrent() {
		return avgCurrent;
	}

	public String getAvgCurrentProp() {
		return "avgCurrent";
	}

	public String getMaxCurrentProp() {
		return "maxCurrent";
	}

	public void setAvgCurrent(Double avgCurrent) {
		this.avgCurrent = avgCurrent;
	}

	public Double getMaxCurrent() {
		return maxCurrent;
	}

	public void setMaxCurrent(Double maxCurrent) {
		this.maxCurrent = maxCurrent;
	}

	public Double getDemandPower() {
		return demandPower;
	}

	public void setDemandPower(Double demandPower) {
		this.demandPower = demandPower;
	}

	public int getRack() {
		return rack;
	}

	public int getRpdu() {
		return rpdu;
	}

	public int getPhase() {
		return phase;
	}

	public Double getVoltage() {
		return voltage;
	}

	public long getTimestamp() {
		return timestamp;
	}

	@Override
	public String toString() {
		return "PhaseData [rack=" + rack + ", rpdu=" + rpdu + ", phase=" + phase + ", voltage=" + voltage
		        + ", timestamp=" + timestamp + ", avgCurrent=" + avgCurrent + ", maxCurrent=" + maxCurrent
		        + ", demandPower=" + demandPower + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(avgCurrent);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(demandPower);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(maxCurrent);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + phase;
		result = prime * result + rack;
		result = prime * result + rpdu;
		result = prime * result + (int) (timestamp ^ (timestamp >>> 32));
		temp = Double.doubleToLongBits(voltage);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PhaseData other = (PhaseData) obj;
		if (Double.doubleToLongBits(avgCurrent) != Double.doubleToLongBits(other.avgCurrent))
			return false;
		if (Double.doubleToLongBits(demandPower) != Double.doubleToLongBits(other.demandPower))
			return false;
		if (Double.doubleToLongBits(maxCurrent) != Double.doubleToLongBits(other.maxCurrent))
			return false;
		if (phase != other.phase)
			return false;
		if (rack != other.rack)
			return false;
		if (rpdu != other.rpdu)
			return false;
		if (timestamp != other.timestamp)
			return false;
		if (Double.doubleToLongBits(voltage) != Double.doubleToLongBits(other.voltage))
			return false;
		return true;
	}
}
