package com.synapsense.service.reporting.dto;

import java.io.Serializable;
import java.util.Map;

import com.synapsense.dto.TO;
import com.synapsense.service.reporting.olap.custom.Cube;
import com.synapsense.util.CollectionUtils;

/**
 * The <code>CellTO</code> interface presents Report Table Cell TO.
 * 
 * @author Grigory Ryabov
 */
public class CellTO implements Comparable<CellTO>, Serializable{

	private static final long serialVersionUID = 4031943921096527084L;
	private Map<String, Object> cellMap = CollectionUtils.newMap();
	public static final String VALUE = "DataValue";
	public static final String TIMESTAMP = "Timestamp";

    public CellTO(TO<?> object, String propertyName, long timestamp, Number value) {
		cellMap.put(Cube.DIM_OBJECT, object);
		cellMap.put(Cube.DIM_PROPERTY, propertyName);
		cellMap.put(Cube.DIM_TIME, timestamp);
		cellMap.put(VALUE, value);
	}
	
	public CellTO(Map<String, Object> cellMap) {
		this.cellMap.putAll(cellMap);
	}

	public TO<?> getObject() {
		return (TO<?>) get(Cube.DIM_OBJECT);
	}

	public String getPropertyName() {
		return (String) get(Cube.DIM_PROPERTY);
	}

	public long getTimestamp() {
		return (long) get(TIMESTAMP);
	}

	public Number getValue() {
		return (Number) get(VALUE);
	}

	@Override
	public int compareTo(CellTO other) {
		return Double.compare(getValue().doubleValue(), other.getValue().doubleValue());
	}

	public Object get(String field) {
		return cellMap.get(field);
	}
	
	public boolean contains (String field) {
		return cellMap.containsKey(field);
	}

}
