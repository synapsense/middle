package com.synapsense.service.nsvc;

public interface NotificationService {

	void registerProcessor(EventProcessor processor, EventProcessor filter);

	void deregisterProcessor(EventProcessor processor);

}
