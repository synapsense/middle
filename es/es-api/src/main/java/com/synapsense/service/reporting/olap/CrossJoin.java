package com.synapsense.service.reporting.olap;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import com.synapsense.service.reporting.olap.custom.Cube;
import com.synapsense.util.CollectionUtils;

/**
 * Represents a set build by crossjoining other sets.<br>
 * MDX-like Examples:<br>
 * [Object].[Rack].members * {[Measures].[Rack.cTop], [Measures].[Rack.cMid]}
 * defines a set of Rack-cTop, Rack-cMid tuples
 * 
 * @author Dmitry Grudzinskiy
 * 
 */
public class CrossJoin implements CompositeAxisSet, Serializable {

	private static final long serialVersionUID = -1374129971220843093L;

	private AxisSet[] sets;

	private LinkedHashSet<Hierarchy> dims = new LinkedHashSet<Hierarchy>();

	public CrossJoin(AxisSet... sets) {
		for (AxisSet set : sets) {
			add(set);
		}
		this.sets = sets;
	}

	public CrossJoin() {
	}

	public CrossJoin add(AxisSet set) {
		dims.addAll(set.getDimensionalities());
		return this;
	}

	public List<Hierarchy> getDimensionalities() {
		return new LinkedList<>(dims);
	}

	public List<Tuple> getTuples() {
		int startIndex = 0; 
		List<Tuple> result = CollectionUtils.newList();
		for (int j = startIndex; j < sets.length; j++) {
			result = sets[j].getTuples();
			if (!result.isEmpty()) {
				startIndex = j;
				break;
			}
		}
		
		for (int i = ++startIndex; i < sets.length; i++) {
			List<Tuple> start = result;
			result = new LinkedList<Tuple>();
			List<Tuple> setTuples = sets[i].getTuples();
			if (setTuples.isEmpty()) {
				result = start;
			}
			for (Tuple first : start) {
				for (Tuple second : setTuples) {
					Tuple joined = first.join(second);
					if (joined != null) {
						result.add(first.join(second));
					}
				}
			}
		}
		return result;
	}

	public static List<Tuple> getTuplesByHierarchy(CrossJoin crossJoin, Hierarchy hierarchy) {

		Set<Tuple> tuplesSet = new LinkedHashSet<Tuple>();
		for (Tuple tuple : crossJoin.getTuples()){
			Member member = tuple.itemByHierarchy(hierarchy);
			tuplesSet.add(member);
		}
		return new LinkedList<>(tuplesSet);
		
	}
	
	@Override
	public String toString() {
		return CollectionUtils.separate(" * ", sets);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(sets);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CrossJoin other = (CrossJoin) obj;
		if (!Arrays.equals(sets, other.sets))
			return false;
		return true;
	}

	@Override
	public void buildQuery(PlanProcessor planProcessor) {
		planProcessor.processSet(this);
	}

	public AxisSet[] getSets() {
		return Arrays.copyOf(sets, sets.length);
	}
}
