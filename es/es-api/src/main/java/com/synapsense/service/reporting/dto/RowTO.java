package com.synapsense.service.reporting.dto;

import com.synapsense.service.reporting.SortingProperty;
import com.synapsense.service.reporting.olap.custom.Cube;
import com.synapsense.util.CollectionUtils;
import java.io.Serializable;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * The <code>RowTO</code> interface presents Report Table Row TO.
 * 
 * @author Grigory Ryabov
 */
public class RowTO implements Iterable<CellTO>, Serializable, Comparable<RowTO> {

	private static final long serialVersionUID = 5435988141905597617L;
	private List<CellTO> rowCells;
	private int columnsNumber;

    private List<SortingProperty> sortingPropertyValues = CollectionUtils.newList();

    public RowTO(List<CellTO> rowCells) {
		this.rowCells = CollectionUtils.newList(rowCells);
		columnsNumber = rowCells.size();
	}

	public Iterator<CellTO> iterator() {
		return new CellIterator();
	}

	public int size() {
		return rowCells.size();
	}

	public RowTO getRow() {
		return new RowTO(rowCells);
	}

	public Object getFieldValue(String field, String hierarchyName) {
		for (CellTO cell : this) {
			if (hierarchyName.equals(Cube.DIM_PROPERTY)) {
                if (field.equals("Data Value")) {
                    return cell.getValue();
                }
//              return cell.getValue();
				if (field.equals(cell.get(field))) {
					return cell.getValue();
				}
			}
			if (hierarchyName.equals(Cube.DIM_OBJECT)) {
				return cell.get(hierarchyName);
			}
			if (hierarchyName.equals(Cube.DIM_TIME)) {
				return cell.get(field);
			}
		}
		return null;
	}
	
	public boolean containsField(String field) {
		for (CellTO cell : this) {
			if (cell.contains(field)) {
				return true;
			}
		}
		return false;
	}

    public class CellIterator implements Iterator<CellTO>, Serializable {

		private static final long serialVersionUID = -1515728300877313365L;
		
		private int column = 0;
		public CellIterator() {
		}

		@Override
		public boolean hasNext() {
			if (column + 1 > columnsNumber) {
				return false;
			}
			return true;
		}

		@Override
		public CellTO next() {
			return rowCells.get(++column - 1);
		}
		
		@Override
		public void remove() {
			throw new UnsupportedOperationException("Remove is not supported"); 
		}
	}

	@Override
	public int compareTo(RowTO otherRow) {

        int result = 0;
        int index = 0;
        List<SortingProperty> otherValues = otherRow.getSortingPropertyValues();
        while (result == 0 && index < sortingPropertyValues.size()) {
            result = sortingPropertyValues.get(index).compareTo(otherValues.get(index));
            index++;
        }
        return result;

	}

    public List<SortingProperty> getSortingPropertyValues() {
        return Collections.unmodifiableList(sortingPropertyValues);
    }

    public void setSortingPropertyValues(List<SortingProperty> sortingPropertyValues) {
        this.sortingPropertyValues = sortingPropertyValues;
    }

}
