package com.synapsense.service.reporting.olap;

/**
 * Interface for sets built as a composition of other sets
 * 
 * @author Dmitry Grudzinskiy
 * 
 */
public interface CompositeAxisSet extends AxisSet {

	/**
	 * Allows to add a set to a collecion of sets
	 * 
	 * @param set
	 *            axis set to add
	 * @return this
	 */
	CompositeAxisSet add(AxisSet set);
}
