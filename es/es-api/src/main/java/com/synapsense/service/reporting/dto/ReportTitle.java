package com.synapsense.service.reporting.dto;

import java.io.Serializable;

public class ReportTitle implements Serializable {

	private static final long serialVersionUID = 4160535531714792903L;

	private int reportId;

	private String taskName;

	private long timestamp;

	public ReportTitle(String taskName, long timestamp) {
		this.taskName = taskName;
		this.timestamp = timestamp;
	}

	public ReportTitle() {
	}

	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	public int getReportId() {
		return reportId;
	}

	public void setReportId(int id) {
		this.reportId = id;
	}

	public String getTaskName() {
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		ReportTitle that = (ReportTitle) o;

		if (timestamp != that.timestamp)
			return false;
		if (!taskName.equals(that.taskName))
			return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result = taskName.hashCode();
		result = 31 * result + (int) (timestamp ^ (timestamp >>> 32));
		return result;
	}
}
