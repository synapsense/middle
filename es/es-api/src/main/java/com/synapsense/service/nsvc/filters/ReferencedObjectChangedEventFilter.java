package com.synapsense.service.nsvc.filters;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import com.synapsense.service.nsvc.events.ReferencedObjectChangedEvent;
import com.synapsense.service.nsvc.events.ReferencedObjectChangedEventProcessor;

/**
 * Class for filtering ReferencedObjectChangedEvents
 * 
 * @author Oleg Stepanov
 * 
 */
public class ReferencedObjectChangedEventFilter implements ReferencedObjectChangedEventProcessor, Serializable {

	private static final long serialVersionUID = 5179735325248896499L;

	private final TOMatcher referencingToMatcher, referencedToMatcher;
	private final String referencingProperty;
	private final Set<String> referencedProperties;

	/**
	 * Same as ReferencedObjectChangedEventFilter(referencingToMatcher,
	 * referencingProperty, null, null);
	 */
	public ReferencedObjectChangedEventFilter(TOMatcher referencingToMatcher, String referencingProperty) {
		this(referencingToMatcher, referencingProperty, null, null);
	}

	/**
	 * Create filter for ReferencedObjectChangedEvent.
	 * 
	 * @param referencingToMatcher
	 *            TO matcher for referencing object's TO
	 * @param referencingProperty
	 *            referencing property name filter (null means any referencing
	 *            property)
	 * @param referencedToMatcher
	 *            TO matcher for referencing object's TO (null means any TO or
	 *            type)
	 * @param referencedProperties
	 *            referenced properties filter. Null or empty set means any
	 *            properties.
	 */
	public ReferencedObjectChangedEventFilter(TOMatcher referencingToMatcher, String referencingProperty,
	        TOMatcher referencedToMatcher, Set<String> referencedProperties) {
		if (referencingToMatcher == null) {
			throw new IllegalArgumentException("referencingToMatcher must not be null");
		}
		this.referencingToMatcher = referencingToMatcher;
		this.referencingProperty = referencingProperty;
		this.referencedToMatcher = referencedToMatcher;
		this.referencedProperties = new HashSet<String>();
		if (referencedProperties != null) {
			for (String referencedProperty : referencedProperties) {
				if (referencedProperty != null && !referencedProperty.isEmpty())
					this.referencedProperties.add(referencedProperty);
			}
		}
	}

	@Override
	public ReferencedObjectChangedEvent process(ReferencedObjectChangedEvent e) {
		// the whole condition can be written as a single but unreadable boolean
		// expression. let's split it onto two conditions: referencing part
		// match
		// and referenced part match.
		if (referencingToMatcher.match(e.getReferencingObj())
		        && (referencingProperty == null || referencingProperty.equals(e.getReferencingPropName()))) {
			if (referencedToMatcher == null || referencedToMatcher.match(e.getReferencedObj())) {
				Set<String> filteredProps = new HashSet<String>(e.getReferencedPropNames());
				if (!referencedProperties.isEmpty())// filter properties if
				                                    // needed
					filteredProps.retainAll(referencedProperties);
				if (!filteredProps.isEmpty()) {// is there anything left after
					                           // filtering?
					return new ReferencedObjectChangedEvent(e.getReferencingObj(), e.getReferencingPropName(),
					        e.getReferencedObj(), filteredProps);
				}
			}
		}
		return null;
	}

}
