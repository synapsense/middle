package com.synapsense.service;

import java.util.Locale;
import java.util.Map;
import java.util.Properties;

public interface LocalizationServiceManagement extends ServiceManagement {
	public Map<Locale, Properties> getTranslations();
	public Map<Locale, Properties> getStrings();

}
