package com.synapsense.service.impl.messages.base;

import java.io.Serializable;

/**
 * This class is responsible to store and pass Event information from Device
 * Manager to the Environment Server.
 * 
 * The stored parameters are: Event Name, Event Message and Event Status.
 * 
 * AFAIK, Event status does not processed everywhere in the Environment Server,
 * thus it can be set to any value.
 * 
 * @author anechaev
 */
public class RefinedAlertData implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3439349345974656457L;

	private final String name;
	private final String message;
	private final String type;

	public RefinedAlertData(String type, String name, String message) {
		this.type = type;
		this.name = name;
		this.message = message;
	}

	public RefinedAlertData(String name, String message) {
		this("UNKNOWN", name, message);
	}

	public String getName() {
		return name;
	}

	public String getMessage() {
		return message;
	}

	public String getType() {
		return type;
	}
}
