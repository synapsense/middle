package com.synapsense.service.impl.messages.base;

import java.io.Serializable;
import java.util.HashMap;

public class DuoConfData implements Serializable {

	private static final long serialVersionUID = -5090525416731820330L;

	private Long checksum;
	private String program;
	private HashMap<Integer, Integer> rpdus = new HashMap<Integer, Integer>();

	public Long getChecksum() {
		return checksum;
	}

	public void setChecksum(Long checksum) {
		this.checksum = checksum;
	}

	public String getProgram() {
		return program;
	}

	public void setProgram(String program) {
		this.program = program;
	}

	public void addRpduConfData(int id, boolean isDelta) {
		rpdus.put(id, isDelta ? 1 : 0);
	}

	public HashMap<Integer, Integer> getRpdus() {
		return rpdus;
	}

	@Override
	public String toString() {
		return "DuoConfData [checksum=" + checksum + ", program=" + program + ", rpdus=" + rpdus + "]";
	}
}
