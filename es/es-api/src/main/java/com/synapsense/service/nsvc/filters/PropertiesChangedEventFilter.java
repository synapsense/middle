package com.synapsense.service.nsvc.filters;

import java.io.Serializable;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import com.synapsense.dto.ChangedValueTO;
import com.synapsense.service.nsvc.events.PropertiesChangedEvent;
import com.synapsense.service.nsvc.events.PropertiesChangedEventProcessor;

public class PropertiesChangedEventFilter implements PropertiesChangedEventProcessor, Serializable {

	private static final long serialVersionUID = 658189310595623623L;

	private final TOMatcher matcher;
	private final Set<String> properties;

	/**
	 * Create filter from TOMatcher. Equivalent of
	 * PropertiesChangedEventFilter(matcher, null)
	 * 
	 * @param matcher
	 *            Matcher to use.
	 * @throws IllegalArgumentException
	 *             if matcher is <code>null</code>.
	 */
	public PropertiesChangedEventFilter(TOMatcher matcher) {
		this(matcher, null);
	}

	/**
	 * Create filter from TOMatcher and list of properties to be passed.
	 * 
	 * @param matcher
	 *            Matcher to use.
	 * @param properties
	 *            list of properties to be passed. If null or empty then any
	 *            property will pass this filter.
	 * @throws IllegalArgumentException
	 *             if matcher is <code>null</code>.
	 */
	public PropertiesChangedEventFilter(TOMatcher matcher, String[] properties) {
		if (matcher == null)
			throw new IllegalArgumentException("matcher must not be null");
		this.matcher = matcher;

		if (properties != null && properties.length != 0) {
			this.properties = new HashSet<String>(properties.length);
			for (String property : properties) {
				this.properties.add(property);
			}
		} else {
			this.properties = null;
		}
	}

	@Override
	public PropertiesChangedEvent process(PropertiesChangedEvent e) {
		if (matcher.match(e.getObjectTO())) {
			if (properties == null) {
				return e;
			} else {
				// filter properties
				List<ChangedValueTO> values = new LinkedList<ChangedValueTO>();
				for (ChangedValueTO changedValue : e.getChangedValues()) {
					if (properties.contains(changedValue.getPropertyName()))
						values.add(changedValue);
				}
				// check if there's anything left to send after filtering
				if (values.size() != 0) {
					ChangedValueTO[] res = new ChangedValueTO[values.size()];
					res = values.toArray(res);
					return new PropertiesChangedEvent(e.getObjectTO(), res);
				} else {
					return null;
				}
			}
		}
		return null;
	}

}
