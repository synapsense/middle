package com.synapsense.service.nsvc.events;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import com.synapsense.dto.TO;
import com.synapsense.service.nsvc.Event;
import com.synapsense.service.nsvc.EventDispatcher;

/**
 * Event class for notifications about changes in objects referenced by another
 * object.
 * 
 * @author Oleg Stepanov
 * 
 */
public class ReferencedObjectChangedEvent implements Event {

	private static final long serialVersionUID = -5908267596739278003L;

	protected TO<?> referencingObj, referencedObj;
	protected String referencingPropName;
	protected Set<String> referencedPropNames;

	public ReferencedObjectChangedEvent(TO<?> referencingObj, String referencingPropName, TO<?> referencedObj,
	        String referencedPropName) {
		this(referencingObj, referencingPropName, referencedObj, Arrays.asList(referencedPropName));
	}

	public ReferencedObjectChangedEvent(TO<?> referencingObj, String referencingPropName, TO<?> referencedObj,
	        Collection<String> referencedPropNames) {
		if (referencingObj == null) {
			throw new IllegalArgumentException("referencingObj must not be null");
		}
		if (referencingPropName == null || referencingPropName.isEmpty()) {
			throw new IllegalArgumentException("referencingPropName must not be null or empty string");
		}
		if (referencedObj == null) {
			throw new IllegalArgumentException("referencedObj must not be null");
		}
		if (referencedPropNames == null || referencedPropNames.size() == 0) {
			throw new IllegalArgumentException("referencedPropNames must not be null or empty collection");
		}
		this.referencingObj = referencingObj;
		this.referencingPropName = referencingPropName;
		this.referencedObj = referencedObj;
		this.referencedPropNames = new HashSet<String>(referencedPropNames.size());
		for (String referencedPropName : referencedPropNames) {
			if (referencedPropName == null || referencedPropName.isEmpty()) {
				throw new IllegalArgumentException("Referenced property names must not be null or empty string");
			}
			this.referencedPropNames.add(referencedPropName);
		}
	}

	public TO<?> getReferencingObj() {
		return referencingObj;
	}

	public String getReferencingPropName() {
		return referencingPropName;
	}

	public TO<?> getReferencedObj() {
		return referencedObj;
	}

	public Set<String> getReferencedPropNames() {
		return Collections.unmodifiableSet(referencedPropNames);
	}

	@Override
	public Event dispatch(EventDispatcher ed) {
		return ed.dispatch(this);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(super.toString()).append(": (").append(referencingObj).append(':').append(referencingPropName)
		        .append(")->(").append(referencedObj).append(':').append(referencedPropNames);
		return sb.toString();
	}

}
