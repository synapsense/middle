package com.synapsense.service.reporting.olap.custom;

import java.io.Serializable;

import com.synapsense.service.reporting.olap.Hierarchy;
/**
 * The Interface Level.
 */
public interface Level extends Serializable {
	
	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	String getName();
	
	/**
	 * Gets the members range.
	 *
	 * @return the members range
	 */
	int getMembersRange();

	/**
	 * Gets the hierarchy.
	 *
	 * @return the hierarchy
	 */
	Hierarchy getHierarchy();

	/**
	 * Gets the calendar level.
	 *
	 * @return the calendar level
	 */
	String getRangeLevel();

	/**
	 * Copies Level instance.
	 *
	 * @return the level
	 */
	Level copy();
}
