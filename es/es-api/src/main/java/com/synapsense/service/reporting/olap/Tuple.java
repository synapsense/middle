package com.synapsense.service.reporting.olap;

/**
 * Interface representing a tuple in a dimensional query
 * 
 * @author Dmitry Grudzinskiy
 * 
 */
public interface Tuple extends AxisSet {

	/**
	 * Size of the tuple
	 * 
	 * @return number of members in the tuple
	 */
	int getTupleSize();

	/**
	 * Returns item by it's index
	 * 
	 * @param index
	 * @return member that is associated with the specified index
	 */
	Member itemByIndex(int index);

	/**
	 * Returns item by it's hierarchy. Since in a tuple each hierarchy can
	 * appear only once this method will always return a single member if
	 * exists.
	 * 
	 * @param hierarchy
	 * @return member from the tuple of the specified hierarchy or null
	 */
	Member itemByHierarchy(Hierarchy hierarchy);

	/**
	 * Joins two tuples and returns a new one. Members of different hierarchies
	 * will appear in the new tuple unchanged while members from the same
	 * hierarchies will be merged.
	 * 
	 * @param tuple
	 * @return new tuple which is join of the two
	 */
	Tuple join(Tuple tuple);

	/**
	 * Returns item by it's dimension(Time, Measures, Object). Since in a tuple each dimension can
	 * appear only once this method will always return a single member if
	 * exists.
	 * 
	 * @param dimension
	 * @return member from the tuple of the specified dimension or null
	 */
    Member itemByDimension(String dimension);
}
