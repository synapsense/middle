package com.synapsense.service.reporting.scriplets.charts;

import java.awt.Font;
import java.sql.ResultSet;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.CombinedDomainCategoryPlot;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.category.DefaultCategoryDataset;

import com.synapsense.util.Pair;

public class BarChartBuilder extends ChartBuilder {
	private static final Log log = LogFactory.getLog(BarChartBuilder.class);
	private HashMap<String, DefaultCategoryDataset> datasetCategoriesMap = new HashMap<String, DefaultCategoryDataset>();
	
	private String categoryDateFormat = "MM-dd-yyyy";
	
	public void setCategoryDateFormat(String dateFormatStr){
		categoryDateFormat = dateFormatStr;
	}
	
	protected JFreeChart createChartType(String chartName) {
		
		JFreeChart chart = new JFreeChart(
				chartName,
	            new Font("SansSerif", Font.BOLD, 12),
	            new CombinedDomainCategoryPlot(),
	            showLegend
	        );
		
		return chart;
	}

	public JFreeChart getChart(NumberFormat numberFormat) {
        JFreeChart result =  createCategoryChart(datasetCategoriesMap, numberFormat);
		return result;
	}
	
	public void addValue(String class_id, String seriesName, String categoryName, String axisName, Float value){
		DefaultCategoryDataset dataset = createOrGetCategoryDataSet(class_id, axisName);		
		dataset.addValue(value, seriesName, categoryName);
	}	
	
	@Override		
	public void addDataSerie(String serie_id, List<SensorPoint> pointArray, String seriesName, String axisName, int bins){
		DefaultCategoryDataset dataset = createOrGetCategoryDataSet(serie_id, axisName);
		  
		  try{		 
			  for (SensorPoint p : pointArray){				  				  			
				  SimpleDateFormat sdf = new SimpleDateFormat(categoryDateFormat);
				  String category = sdf.format(p.time_stamp);				  
				  dataset.addValue(p.getValue(), seriesName, category);
			  }
			  
		  }catch(Exception e){
			  log.error(e.getLocalizedMessage(), e);
		  }
	}
	
	public void addDataSerie(String serie_id, ResultSet rs, String seriesName,
			String dateRSfieldName, String valueRSfieldName, String axisName) {
	  
	  DefaultCategoryDataset dataset = createOrGetCategoryDataSet(serie_id, axisName);
	  
	  try{		 
		  rs.beforeFirst();			
		  while (rs.next()) {									
			  Float value = rs.getFloat(valueRSfieldName);
			  java.sql.Timestamp date = rs.getTimestamp(dateRSfieldName);				
			  SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy");
			  String category = sdf.format(date);
						
			  dataset.addValue(value, seriesName, category);									
		  }
		  
	  }catch(Exception e){
		  log.error(e.getLocalizedMessage(), e);
	  }
  }

	private DefaultCategoryDataset createOrGetCategoryDataSet(String class_id, String axisName) {
		DefaultCategoryDataset result = null;
		
		if (datasetCategoriesMap.containsKey(class_id)){
			result = datasetCategoriesMap.get(class_id);
		}else{
			result = new DefaultCategoryDataset();
			datasetCategoriesMap.put(class_id, result);
		}
		
		axisNameMap.put(class_id, axisName);
		
		return result;
	}


	@Override
	protected void customizeChart(XYPlot plot) {
	}

	@Override
	protected Pair<String, String> getValueAxisPair(String entryKey,
			String currValueAxisName) {
		throw new UnsupportedOperationException();
	}

}
