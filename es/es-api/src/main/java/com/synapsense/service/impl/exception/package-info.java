/**
 * Contains definition of exceptions
 * thrown by Environment API implementation.
 */
package com.synapsense.service.impl.exception;