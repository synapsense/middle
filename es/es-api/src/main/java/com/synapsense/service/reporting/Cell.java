package com.synapsense.service.reporting;

import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import com.synapsense.dto.TO;
import com.synapsense.service.reporting.olap.Hierarchy;
import com.synapsense.service.reporting.olap.Member;
import com.synapsense.service.reporting.olap.Tuple;
import com.synapsense.service.reporting.olap.custom.Cube;

public class Cell implements Serializable, Comparable<Cell>{

	private static final long serialVersionUID = -5637720916642494556L;

	private Tuple tuple;

	private Number value;

	private LinkedList<Integer> valueIds = new LinkedList<>();
	
	private String formula;

	private long timestamp;

	private Integer unitConverterId;

    public Cell(Tuple tuple) {
		this.tuple = tuple;
	}

    public String getFormula() {
		Member propertyMember = tuple.itemByHierarchy(Cube.PROPERTY);
		if (propertyMember == null) {
			throw new IllegalArgumentException("Cell should contain Property Member. Tuple passed: " + tuple);
		}
		String [] keys= ((String)propertyMember.getKey()).split("\\.");
		return formula = keys[2];
	}

	public Member getMember(Hierarchy hierarchy) {
		return tuple.itemByHierarchy(hierarchy);
	}
	
	public Member getMember(String dimension) {
		return tuple.itemByDimension(dimension);
	}

	public Number getValue() {
		return value != null ? value : null;
	}

	public void setValue(Number value) {
		this.value = value;
	}	
	
	public List<Integer> getValueIds() {
		return Collections.unmodifiableList(valueIds);
	}

	public void setValueIds(Collection<Integer> valueIds) {
		this.valueIds = new LinkedList<>(valueIds);
	}

	public void addValueId(Integer valueId) {
		valueIds.add(valueId);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((formula == null) ? 0 : formula.hashCode());
		result = prime * result + ((tuple == null) ? 0 : tuple.hashCode());
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		result = prime * result
				+ ((valueIds == null) ? 0 : valueIds.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Cell other = (Cell) obj;
		if (formula == null) {
			if (other.formula != null) {
				return false;
			}
		} else if (!formula.equals(other.formula)) {
			return false;
		}
		if (tuple == null) {
			if (other.tuple != null) {
				return false;
			}
		} else if (!tuple.equals(other.tuple)) {
			return false;
		}
		if (value == null) {
			if (other.value != null) {
				return false;
			}
		} else if (!value.equals(other.value)) {
			return false;
		}
		if (valueIds == null) {
			if (other.valueIds != null) {
				return false;
			}
		} else if (!valueIds.equals(other.valueIds)) {
			return false;
		}
		return true;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public void setUnitConverterId(int id) {
		this.unitConverterId = id;
	}

	public Integer getUnitConverterId() {
		return (unitConverterId == null) ? unitConverterId : new Integer(unitConverterId);
	}

    @Override
	public int compareTo(Cell other) {
		Number value1 = getValue();
		Number value2 = other.getValue();
		if (value1 == null || value2 == null) {
			return 0;
		}
		return Double.compare(value1.doubleValue(), value2.doubleValue());
	}
	
}
