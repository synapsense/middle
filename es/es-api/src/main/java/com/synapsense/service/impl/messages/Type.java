/**
 * 
 */
package com.synapsense.service.impl.messages;

/**
 * @author Alexander Borisov
 * 
 */
public enum Type {
NODE_VERSION, SENSOR, ROUTING, EVENT, TPD, ALERT, REFINED_ALERT, GWSTATUS, STAT, POWER_RACK_CONF;
}
