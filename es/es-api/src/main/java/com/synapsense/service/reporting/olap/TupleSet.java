package com.synapsense.service.reporting.olap;

import java.io.Serializable;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;

import com.synapsense.util.CollectionUtils;

public class TupleSet implements AxisSet, Serializable {

	private static final long serialVersionUID = 1870277062299388937L;

	private LinkedList<Hierarchy> dims = new LinkedList<>();

	private LinkedHashSet<Tuple> set = new LinkedHashSet<Tuple>();

	public TupleSet(Tuple... tuples) {
		if (tuples.length == 0) {
			return;
		}
		dims.addAll(tuples[0].getDimensionalities());
		for (Tuple tuple : tuples) {
			add(tuple);
		}
	}

	public TupleSet() {
	}

	public TupleSet add(Tuple tuple) {
		if (dims.isEmpty()) {
			dims.addAll(tuple.getDimensionalities());
		} else if (!dims.equals(tuple.getDimensionalities())) {
			throw new IllegalArgumentException("Incompatible dimensionalities: " + dims + " and "
			        + tuple.getDimensionalities());
		}
		set.add(tuple);
		return this;
	}

	public List<Hierarchy> getDimensionalities() {
		return new LinkedList<>(dims);
	}

	public List<Tuple> getTuples() {
		return new LinkedList<Tuple>(set);
	}

	@Override
	public String toString() {
		return new StringBuilder("{").append(CollectionUtils.separate(", ", set)).append("}").toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((set == null) ? 0 : set.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TupleSet other = (TupleSet) obj;
		if (set == null) {
			if (other.set != null)
				return false;
		} else if (!set.equals(other.set))
			return false;
		return true;
	}

	@Override
	public void buildQuery(PlanProcessor planProcessor) {
		planProcessor.processSet(this);
	}
}
