package com.synapsense.service.impl.messages.base;

import java.io.Serializable;

public class ServerConfData implements Serializable, ServerInfo {

	private static final long serialVersionUID = 1413471487058050694L;
	private int uPos;
	private int uHeight;
	private int rack;
	private double demandPower;

	public ServerConfData() {
	}

	@Override
	public int getuPos() {
		return uPos;
	}

	public void setuPos(int uPos) {
		this.uPos = uPos;
	}

	@Override
	public int getuHeight() {
		return uHeight;
	}

	public void setuHeight(int uHeight) {
		this.uHeight = uHeight;
	}

	@Override
	public int getRack() {
		return rack;
	}

	public void setRack(int rack) {
		this.rack = rack;
	}

	public double getDemandPower() {
		return demandPower;
	}

	public void setDemandPower(double demandPower) {
		this.demandPower = demandPower;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(demandPower);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + rack;
		result = prime * result + uHeight;
		result = prime * result + uPos;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ServerConfData other = (ServerConfData) obj;
		if (Double.doubleToLongBits(demandPower) != Double.doubleToLongBits(other.demandPower))
			return false;
		if (rack != other.rack)
			return false;
		if (uHeight != other.uHeight)
			return false;
		if (uPos != other.uPos)
			return false;
		return true;
	}
}
