package com.synapsense.service.reporting.datasource;

import com.synapsense.dto.*;
import com.synapsense.exception.UserManagementException;
import com.synapsense.service.reporting.olap.RangeSet;
import com.synapsense.service.reporting.olap.custom.Cube;
import com.synapsense.service.reporting.scriplets.charts.ChartDescr;
import com.synapsense.service.user.EnvUserContext;
import com.synapsense.service.user.UserContext;
import com.synapsense.util.ConvertingEnvironment;
import com.synapsense.util.ConvertingEnvironmentProxy;
import com.synapsense.util.unitconverter.Dimension;
import com.synapsense.util.unitconverter.UnitResolvers;
import com.synapsense.util.unitconverter.UnitSystems;
import org.apache.log4j.Logger;
import org.json.JSONArray;

import java.awt.*;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.util.*;

/**
 * The <code>AbstractCustomQueryDataSource</code> class implements basic
 * parsing of Custom Query.<br>
 * 
 * @author Alexander Pakhunov
 * 
 */
public abstract class AbstractCustomQueryDataSource extends AbstractJRDataSource {
	
	private static final String ROOT_PERMISSION = "";
	private static final Logger LOG = Logger.getLogger(AbstractCustomQueryDataSource.class);

	protected NumberFormat numberFormat;
	protected DateFormat dateFormat;
	protected Locale locale;


	public AbstractCustomQueryDataSource(TO<?> customQueryTo, Date startDate, Date endDate) throws Exception {
		parseCustomQuery(customQueryTo, startDate, endDate);
	}
	
	public AbstractCustomQueryDataSource(TO<?> customQueryTo, RangeSet period) throws Exception {
		this(customQueryTo
				, Cube.TIME.getCalendarOfMember(period.getStartMember()).getTime()
				, Cube.TIME.getCalendarOfMember(period.getEndMember()).getTime());
	}

	protected void parseCustomQuery(TO<?> customQueryTo, Date startDate, Date endDate)  throws Exception {
        Integer userId = env.getPropertyValue(customQueryTo, "userId", Integer.class);
        String userName = userId == null ? getUserName() : getUserName(userId);

        UserContext userContext = new EnvUserContext(userName, userManagement);

		UnitSystems unitSystems = unitSystemsService.getUnitSystems();
		final UnitResolvers unitRes = unitSystemsService.getUnitReslovers();
		ConvertingEnvironmentProxy convertingEnvProxy = new ConvertingEnvironmentProxy();
		convertingEnvProxy.setUnitSystems(unitSystems);
		convertingEnvProxy.setUnitResolvers(unitRes);
		convertingEnvProxy.setProxiedEnvironment(env);
		convertingEnvProxy.setTargetSystem(userContext.getUnitSystem());
		ConvertingEnvironment convertingEnv = convertingEnvProxy.getProxy();

		numberFormat = userContext.getDataFormat();
		dateFormat = userContext.getDateFormat();
		locale = userContext.getLocale();

		String associatedSensorsString = env.getPropertyValue(customQueryTo, "associatedSensors", String.class);
		String[] properties = associatedSensorsString.split(",");

		String lineDescriptorsString = env.getPropertyValue(customQueryTo, "lines", String.class);
		JSONArray lineDescrsJsonArray = new JSONArray(lineDescriptorsString);

		String chartDescr = env.getPropertyValue(customQueryTo, "chartDescr", String.class);
		
		beforeProcessing(chartDescr);
		
		boolean advancedChartAverageNeeded = ChartDescr.isAdvancedChartAverageNeeded(chartDescr);
		
		//if properties length is more than 30 then legend will not be shown
		if (properties.length > 30) {
			String[] dataArr = chartDescr.split(",");
			boolean showLegend = Boolean.parseBoolean(dataArr[1]);
			showLegend = false;
			dataArr[1] = String.valueOf(showLegend);
			StringBuilder chartData = new StringBuilder();
			for (String dataItem : dataArr) {
				chartData.append(dataItem)
					.append(",");
			}
			chartDescr = chartData.substring(0, chartData.length() - 1);
		}
		for (int i = 0; i < properties.length; i++) {
			String property = properties[i];
			String[] tokens = property.split("->");
			TO<?> objId = TOFactory.getInstance().loadTO(tokens[0]);

			// room and dc permissions are checked inside
			if (!convertingEnv.exists(objId)) {
				continue;
			}

			String objType = objId.getTypeName();
			String propName = tokens[1];
			String objName = env.getPropertyValue(objId, "name", String.class);

			PropertyDescr propDesc = env.getObjectType(objType).getPropertyDescriptor(propName);

			Collection<ValueTO> valueTos;
			if ("com.synapsense.dto.TO".equals(propDesc.getTypeName())) {
				TO<?> linkTo = env.getPropertyValue(objId, propName, TO.class);
				valueTos = convertingEnv.getHistory(linkTo, "lastValue", startDate, endDate, Double.class);
			} else {
				valueTos = convertingEnv.getHistory(objId, propName, startDate, endDate, Double.class);
			}
			
			if (valueTos == null || valueTos.isEmpty()) {
				continue;
			}

			JSONArray lineArray = lineDescrsJsonArray.getJSONArray(i);

			Dimension dimension = convertingEnv.getDimension(objType, propName);

			processProperty(objName, objType, propName, dimension, valueTos,
					Float.parseFloat(lineArray.get(0).toString()), new Color(
							lineArray.getInt(1), false), advancedChartAverageNeeded);
		}

		afterProcessing(chartDescr);
	}
	
	protected abstract void beforeProcessing(String s);
	
	protected abstract void processProperty(String objName, String objType,
			String propName, Dimension dimension, Collection<ValueTO> valueTos,
			float lineWidth, Color lineColor, boolean advancedChartAverageNeeded);
	
	protected abstract void afterProcessing(String chartDescr);

	protected String getDisplayablePropertyName(String typeName, String propertyName, Locale locale) {
		return localizationService.getString(
		        "object_property_" + typeName.toLowerCase() + "_" + propertyName.toLowerCase(), locale);
	}

	protected Collection<String> getUserGroupNames(String userName) {
		Collection<String> names = Collections.emptyList();
		try {
			TO<?> user = userManagement.getUser(userName);
			names = userManagement.getUserGroupIds(user);
		} catch (UserManagementException e) {
			LOG.error(e.getMessage(), e);
		}
		return names;
	}

	protected boolean isRoot(String userName) {
        try {
			TO<?> user = userManagement.getUser(userName);
        	Collection<String> privs = userManagement.getUserPrivilegeIds(user);
    		if(privs == null || privs.isEmpty()){
    			return false;
    			}
    		return privs.contains(ROOT_PERMISSION);
        } catch (UserManagementException e) {
			LOG.error(e.getMessage(), e);
			return false;
        }
		
	}

    public NumberFormat getNumberFormat() {
        return numberFormat;
    }
}