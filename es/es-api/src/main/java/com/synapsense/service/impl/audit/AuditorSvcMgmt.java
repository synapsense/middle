package com.synapsense.service.impl.audit;

public interface AuditorSvcMgmt {

	String listAuditors();

	void removeAuditor(String auditorName);

	// AFAIR it is J2EE convention to use such signature
	void create() throws Exception;

	// AFAIR it is J2EE convention to use such signature
	void start() throws Exception;

	void stop();

	void destroy();

}
