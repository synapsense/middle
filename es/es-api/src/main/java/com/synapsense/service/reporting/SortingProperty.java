package com.synapsense.service.reporting;

import static com.synapsense.service.reporting.olap.custom.PropertyHierarchy.ReportingPropertyName;
import java.io.Serializable;

public class SortingProperty implements Serializable, Comparable<SortingProperty>{

	private static final long serialVersionUID = -1765742271079880564L;

	private String objectType;
	
	private String propertyName;

    private Comparable value = "";

    public SortingProperty() {
	}
    
    public SortingProperty(String objectType, String propertyName) {
		this.objectType = objectType;
		this.propertyName = propertyName;
	}

    public SortingProperty(String fullPropertyName) {
        ReportingPropertyName reportingPropertyName = new ReportingPropertyName(fullPropertyName);
        this.objectType = reportingPropertyName.getObjectTypeName();
        this.propertyName = fullPropertyName;
    }

	public SortingProperty(SortingProperty other) {
		this(other.getObjectType(), other.getPropertyName());
	}
	
	public String getObjectType() {
		return objectType;
	}

	public String getPropertyName() {
		return propertyName;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((objectType == null) ? 0 : objectType.hashCode());
		result = prime * result
				+ ((propertyName == null) ? 0 : propertyName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		SortingProperty other = (SortingProperty) obj;
		if (objectType == null) {
			if (other.objectType != null) {
				return false;
			}
		} else if (!objectType.equals(other.objectType)) {
			return false;
		}
		if (propertyName == null) {
			if (other.propertyName != null) {
				return false;
			}
		} else if (!propertyName.equals(other.propertyName)) {
			return false;
		}
		return true;
	}

    public SortingProperty setValue(Comparable<?> value) {
        this.value = value;
        return this;
    }


    public Comparable getValue() {
        return value;
    }

    @Override
    public int compareTo(SortingProperty otherSortingProperty) {
    	Comparable compareValue = value;
    	if (compareValue == null) {
    		compareValue = otherSortingProperty.getValue();
    		if (compareValue == null) {
    			value = "";
    			otherSortingProperty.setValue("");
    		} else {    			
    			value = (compareValue instanceof String) ? "": 0.0; 
    		}
    	} else if (otherSortingProperty.getValue() == null) {
    		otherSortingProperty.setValue((compareValue instanceof String) ? "": 0.0);
    	}
    	if (value instanceof Number){
        	return -this.value.compareTo(otherSortingProperty.getValue());
        }
        return this.value.compareTo(otherSortingProperty.getValue());
    }

}
