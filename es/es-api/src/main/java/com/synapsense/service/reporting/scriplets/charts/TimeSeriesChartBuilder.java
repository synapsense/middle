package com.synapsense.service.reporting.scriplets.charts;

import java.sql.ResultSet;
import java.text.NumberFormat;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;

import com.synapsense.util.CollectionUtils;
import com.synapsense.util.Pair;

public class TimeSeriesChartBuilder extends ChartBuilder {
	
	private static Log log = LogFactory.getLog(TimeSeriesChartBuilder.class);
	protected Map<String, TimeSeriesCollection> datasetTimeSeriesMap = CollectionUtils.newLinkedMap();
	
	@Override
	public JFreeChart getChart(NumberFormat numberFormat)
	{
		Map<String, XYDataset> commonMap = CollectionUtils.newLinkedMap();		
		commonMap.putAll(datasetTimeSeriesMap);
		
		return createXYChart(commonMap, axisNameMap, chartName, numberFormat);
		//return createChart(datasetTimeSeriesMap, axisNameMap, chartName);
	}
	
	public void clearSeries()
	{
		axisNameMap.clear();
		datasetTimeSeriesMap.clear();
	}
	
	public void addDataSerie(String serie_id, ResultSet rs, String seriesName,
			String dateRSfieldName, String valueRSfieldName, String axisName){
		
		TimeSeries currSeries = createTimeSeries(rs, seriesName, dateRSfieldName, valueRSfieldName);		
		TimeSeriesCollection currTSC = null;
		
		if (datasetTimeSeriesMap.containsKey(serie_id)) {
			currTSC = datasetTimeSeriesMap.get(serie_id);
		} else {
			currTSC = new TimeSeriesCollection();
			datasetTimeSeriesMap.put(serie_id, currTSC);
		}
		currTSC.addSeries(currSeries);
        axisNameMap.put(serie_id, axisName);
	}
	
	@Override
	public void addDataSerie(String serie_id, List<SensorPoint> pointArray, String seriesName, String axisName, int bins){

		TimeSeries currSeries = createTimeSeries(pointArray, seriesName);		
		TimeSeriesCollection currTSC = null;
		
		if (datasetTimeSeriesMap.containsKey(axisName)) {
			currTSC = datasetTimeSeriesMap.get(axisName);
		} else {
			currTSC = new TimeSeriesCollection();
			datasetTimeSeriesMap.put(axisName, currTSC);
		}			
		currTSC.addSeries(currSeries);			
		axisNameMap.put(axisName, serie_id);
		
	}
	
	private TimeSeries createTimeSeries(ResultSet rs, String seriesName,
			String dateRSfieldName, String valueRSfieldName) {
		TimeSeries ts = null;
		try {
			ts = new TimeSeries(seriesName, org.jfree.data.time.Second.class);
			rs.beforeFirst();	
			while (rs.next()) {
				Float value = rs.getFloat(valueRSfieldName);
				java.sql.Timestamp date = rs.getTimestamp(dateRSfieldName);				
				ts.addOrUpdate(new org.jfree.data.time.Second(date), value);				
			}
			//rs.close();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		return ts;
	}
	
	protected TimeSeries createTimeSeries(List<SensorPoint> pointArray, String seriesName) {
		
		TimeSeries ts = null;
		try {
			ts = new TimeSeries(seriesName, org.jfree.data.time.Second.class);			
			for(int i = 0; i < pointArray.size(); i++){
				ts.addOrUpdate(new org.jfree.data.time.Second(new java.sql.Timestamp(pointArray.get(i).time_stamp)), pointArray.get(i).getValue());
			}						
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		return ts;
	}
	
	protected synchronized JFreeChart createChartType(String chartName){
		
		JFreeChart timeChart = ChartFactory.createTimeSeriesChart(chartName, // title
				"", // x-axis label
				"Value", // y-axis label
				null, // data
				//showLegend, // create legend
				false, //create legend later
				true, // generate tool tips
				false // generate URLs
				);
		
		return timeChart;
	}

	@Override
	public void addValue(String class_id, String seriesName,
			String categoryName, String axisName, Float value) {
	}

	@Override
	protected void customizeChart(XYPlot plot) {
	}
	
	@Override
	protected Pair<String, String> getValueAxisPair(String entryKey, String currValueAxisName) {
		return Pair.newPair(entryKey, currValueAxisName);
	}		

}
