package com.synapsense.service.impl.dao.to;

import java.io.Serializable;

import com.synapsense.dto.TO;

/**
 * Abstract type of all <code>TO</code> implementations
 * 
 * @author shabanov
 * 
 * @param <KEY_TYPE>
 *            What type is implementor primary key
 */
public abstract class AbstractTO<KEY_TYPE> implements TO<KEY_TYPE>, Serializable {
	/**
	 * ID of underlying object
	 */
	private KEY_TYPE id;

	/**
	 * Object type name
	 */
	private String typeName;

	/**
	 * ID of ES which own this object. If esId is null, object belongs to local
	 * ES
	 */
	private KEY_TYPE esId;

	// we can cache the hash code as long as both id and typeName are immutable
	transient protected int cachedHashCode;

	protected AbstractTO(KEY_TYPE id, String typeName, KEY_TYPE esId) {
		if (id == null) {
			throw new IllegalArgumentException("Supplied id is null!");
		}

		if (typeName == null || typeName.isEmpty()) {
			throw new IllegalArgumentException("Supplied typeName is null or empty!");
		}

		this.id = id;
		this.typeName = typeName;
		this.esId = esId;
	}

	protected AbstractTO(KEY_TYPE id, String typeName) {
		if (id == null) {
			throw new IllegalArgumentException("Supplied id is null!");
		}

		if (typeName == null || typeName.isEmpty()) {
			throw new IllegalArgumentException("Supplied typeName is null or empty!");
		}

		this.id = id;
		this.typeName = typeName;
	}

	@Override
	public KEY_TYPE getID() {
		return this.id;
	}

	@Override
	public String getTypeName() {
		return this.typeName;
	}

	@Override
	public KEY_TYPE getEsID() {
		return this.esId;
	}

	/*
	 * @Override public void setTypeName(String typeName) { if (typeName == null
	 * || typeName.isEmpty()) { throw new IllegalArgumentException(
	 * "Supplied typeName is null or empty!"); } }
	 */

	/**
	 * No need to override the method in descendants.
	 * 
	 */
	@Override
	public int hashCode() {
		if (cachedHashCode != 0)
			return cachedHashCode;
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((typeName == null) ? 0 : typeName.hashCode());
		result = prime * result + ((esId == null) ? 0 : esId.hashCode());
		cachedHashCode = result;
		return result;
	}

	/**
	 * No need to override the method in descendants.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof AbstractTO))
			return false;
		final AbstractTO<?> other = (AbstractTO<?>) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (typeName == null) {
			if (other.typeName != null)
				return false;
		} else if (!typeName.equals(other.typeName))
			return false;
		if (esId == null) {
			if (other.esId != null)
				return false;
		} else if (!esId.equals(other.esId))
			return false;

		return true;
	}

	@Override
	public String toString() {
		return this.getTypeName() + ":" + this.getID() + ((this.getEsID() != null) ? ":" + this.getEsID() : "");
	}
}
