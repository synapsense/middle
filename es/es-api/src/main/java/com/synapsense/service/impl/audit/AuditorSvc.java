package com.synapsense.service.impl.audit;

import java.util.Map;

public interface AuditorSvc {
	Map<String, Auditor> getAuditors();

	void audit();
}
