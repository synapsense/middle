package com.synapsense.service.nsvc.events;

import com.synapsense.dto.Alert;
import com.synapsense.dto.TO;
import com.synapsense.service.nsvc.Event;
import com.synapsense.service.nsvc.EventDispatcher;

/**
 * 
 * @author vadgusev
 * @deprecated To catch raised alerts use ObjectCreatedEvent.
 * 
 */
@Deprecated
public class AlertRaisedEvent implements Event {

	private static final long serialVersionUID = -1274214372881310033L;

	private final Alert alert;
	private final TO<?> alertTO;
	private final boolean reRaised;

	public AlertRaisedEvent(Alert alert, TO<?> alertTO) {
		this(alert, alertTO, false);
	}

	public AlertRaisedEvent(Alert alert, TO<?> alertTO, boolean reRaised) {
		this.alert = alert;
		this.alertTO = alertTO;
		this.reRaised = reRaised;
	}

	public Alert getAlert() {
		return alert;
	}

	public TO<?> getAlertTO() {
		return alertTO;
	}

	public boolean isReRaised() {
		return reRaised;
	}

	@Override
	public Event dispatch(EventDispatcher ed) {
		return ed.dispatch(this);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(super.toString()).append(": Alert=").append(alert).append(", reRaised=").append(reRaised);
		return sb.toString();
	}

}
