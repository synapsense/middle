package com.synapsense.service.nsvc.filters;

import java.io.Serializable;

import com.synapsense.service.nsvc.events.ObjectCreatedEvent;
import com.synapsense.service.nsvc.events.ObjectCreatedEventProcessor;

/**
 * ObjectCreatedEvent filter that passes events with an object TO matching the
 * specified TOMatcher.
 * 
 * @author Oleg Stepanov
 * 
 */
public class ObjectCreatedEventFilter implements ObjectCreatedEventProcessor, Serializable {

	private static final long serialVersionUID = 4571525539719707854L;

	private final TOMatcher matcher;

	/**
	 * Create filter from TOMatcher.
	 * 
	 * @param matcher
	 *            Matcher to use.
	 * @throws IllegalArgumentException
	 *             if matcher is <code>null</code>.
	 */
	public ObjectCreatedEventFilter(TOMatcher matcher) {
		if (matcher == null)
			throw new IllegalArgumentException("matcher must not be null");
		this.matcher = matcher;
	}

	@Override
	public ObjectCreatedEvent process(ObjectCreatedEvent e) {
		return matcher.match(e.getObjectTO()) ? e : null;
	}

}
