package com.synapsense.service.reporting.olap;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

import com.synapsense.service.reporting.olap.custom.BaseLevel;
import com.synapsense.service.reporting.olap.custom.Cube;
import com.synapsense.service.reporting.olap.custom.Level;
import com.synapsense.util.CollectionUtils;

/**
 * This class represent a set which is defined by a level in an hierarchy and a
 * parent member. The resulting set would be all members under that parent
 * member and level.<br>
 * MDX-like Examples:<br>
 * [Object].[DC].[&DataCenter1].[Rack].members defined all racks in data center
 * DataCenter1<br>
 * [Time].[Year].[&2012].[Month].members defines all months of the year 2012.
 * 
 * @author Dmitry Grudzinskiy
 * 
 */
public class AllMemberSet implements AxisSet, Serializable {

	private static final long serialVersionUID = 6261820563791708984L;

	private MemberHierarchy hierarchy;
	private String level;
	private Member parent;
	
	/** The limit level can include current level. */
	private String limitLevel;
	private Level customLevel;

	public AllMemberSet(MemberHierarchy hierarchy, String level) {
		this(hierarchy, new BaseLevel(hierarchy, level), null);
	}

	public AllMemberSet(String limitLevel, MemberHierarchy hierarchy, Level level) {
		if (limitLevel.equals(Cube.TIME.getLower(Cube.TIME.getChildLevel(limitLevel), level.getName()))) {
			throw new IllegalArgumentException("LimitLevel " + limitLevel + "is descendant of " + level.getName());
		}
		
		this.limitLevel = limitLevel;
		this.customLevel = level;
		this.hierarchy = hierarchy;
		this.parent = null;
		this.level = level.getName();
	}

	public AllMemberSet(MemberHierarchy hierarchy, Level level, Member parent) {
		this.customLevel = level;
		this.hierarchy = hierarchy;
		this.parent = parent;
		this.level = level.getName();
	}

	public Level getCustomLevel() {
		return customLevel;
	}

	public AllMemberSet(MemberHierarchy hierarchy, String level, Member parent) {
		this.parent = parent;
		this.level = level;
		this.hierarchy = hierarchy;
	}

	public List<Hierarchy> getDimensionalities() {
		return new LinkedList<Hierarchy>(CollectionUtils.list(hierarchy));
	}

	public List<Tuple> getTuples() {
		if (hierarchy.equals(Cube.OBJECT)) {
			hierarchy = Cube.OBJECT;
		}
		List<Member> members = (limitLevel != null) ? hierarchy.getMembers(
				customLevel, limitLevel) : hierarchy.getMembers(customLevel,
				parent);
		return new LinkedList<Tuple>(members);
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public Member getParent() {
		return parent;
	}

	public void setParent(Member parent) {
		this.parent = parent;
	}

	@Override
	public String toString() {
		String str = ".[" + level + "].[members]";
		return parent == null ? "[" + hierarchy.getName() + "]" + str : parent
				.toString() + str;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((customLevel == null) ? 0 : customLevel.hashCode());
		result = prime * result
				+ ((hierarchy == null) ? 0 : hierarchy.hashCode());
		result = prime * result + ((level == null) ? 0 : level.hashCode());
		result = prime * result
				+ ((limitLevel == null) ? 0 : limitLevel.hashCode());
		result = prime * result + ((parent == null) ? 0 : parent.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		AllMemberSet other = (AllMemberSet) obj;
		if (customLevel == null) {
			if (other.customLevel != null) {
				return false;
			}
		} else if (!customLevel.equals(other.customLevel)) {
			return false;
		}
		if (hierarchy == null) {
			if (other.hierarchy != null) {
				return false;
			}
		} else if (!hierarchy.equals(other.hierarchy)) {
			return false;
		}
		if (level == null) {
			if (other.level != null) {
				return false;
			}
		} else if (!level.equals(other.level)) {
			return false;
		}
		if (limitLevel == null) {
			if (other.limitLevel != null) {
				return false;
			}
		} else if (!limitLevel.equals(other.limitLevel)) {
			return false;
		}
		if (parent == null) {
			if (other.parent != null) {
				return false;
			}
		} else if (!parent.equals(other.parent)) {
			return false;
		}
		return true;
	}

	@Override
	public void buildQuery(PlanProcessor planProcessor) {
		planProcessor.processSet(this);
	}
}
