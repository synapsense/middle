/**
 * 
 */
package com.synapsense.service.impl.registrar;

/**
 * @author Alexander Borisov
 * 
 */
public interface RegistrarMgmt {
	/**
	 * Register component on
	 * 
	 * @param componentName
	 * @param connectionInfo
	 */
	public void registerComponent(String componentName, String connectionInfo);

	public void unRegisterComponent(String componentName);

	public String print();

	void create() throws Exception;

	void start() throws Exception;

	void stop();

	void destroy();
}
