package com.synapsense.service.impl.activitylog;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.exception.IllegalInputParameterException;

public class ActivityRecordFilter implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9036352282110815601L;

	private StringBuffer query;
	private Boolean wasCreteriaAdded;

	public ActivityRecordFilter() {
		query = new StringBuffer("Select record from ActivityRecordDO as record"
		        + " left join fetch record.object as obj" + " left join fetch obj.objectType as type");
		wasCreteriaAdded = false;
	}

	/**
	 * 
	 * @param ID
	 */
	public ActivityRecordFilter filterById(int ID) {
		this.query.append(this.CheckCriteria());
		this.query.append(" record.id = ");
		this.query.append(ID);
		return this;
	}

	/**
	 * 
	 * @param action
	 */
	public ActivityRecordFilter filterByAction(String action) {
		if (action == null) {
			throw new IllegalInputParameterException("Supplied 'action' is null");
		} else {
			this.query.append(this.CheckCriteria());
			this.query.append(" record.action = ");
			this.query.append("'" + action + "'");
		}
		return this;
	}

	/**
	 * 
	 * @param module
	 */
	public ActivityRecordFilter filterByModule(String module) {
		if (module == null) {
			throw new IllegalInputParameterException("Supplied 'module' is null");
		} else {
			this.query.append(this.CheckCriteria());
			this.query.append(" record.module = ");
			this.query.append("'" + module + "'");
		}
		return this;
	}

	/**
	 * 
	 * @param minDate
	 * @param maxDate
	 */
	public ActivityRecordFilter filterByTime(Date minDate, Date maxDate) {
		if (minDate == null) {
			throw new IllegalInputParameterException("Supplied 'minDate'  is null");
		}
		if (maxDate == null) {
			throw new IllegalInputParameterException("Supplied 'maxDate'  is null");
		}
		if (minDate.after(maxDate)) {
			throw new IllegalInputParameterException("Supplied 'minDate' after 'maxDate' is null");
		} else {
			this.query.append(this.CheckCriteria());
			this.query.append(" record.time between ");
			this.query.append("'" + this.DateToSting(minDate) + "'");
			this.query.append(" and ");
			this.query.append("'" + this.DateToSting(maxDate) + "'");
		}
		return this;
	}

	private String DateToSting(Date date) {
		final String datePattern = "yyyy-MM-dd HH:mm:ss";
		final DateFormat dateFormatter = new SimpleDateFormat(datePattern);

		return dateFormatter.format(date);
	}

	/**
	 * 
	 * @param objId
	 */
	public ActivityRecordFilter filterByObject(TO<?> objId) {
		if (objId == null) {
			throw new IllegalInputParameterException("Supplied 'object' is null");
		}
		if (objId.equals(TOFactory.EMPTY_TO)) {
			this.query.append(this.CheckCriteria());
			this.query.append(" record.object is null ");
		} else {
			this.query.append(this.CheckCriteria());
			this.query.append(" record.object = ");
			this.query.append(objId.getID());
		}
		return this;
	}

	/**
	 * 
	 * @param userName
	 */
	public ActivityRecordFilter filterByUser(String userName) {
		if (userName == null) {
			throw new IllegalInputParameterException("Supplied 'object' is null");
		} else {
			this.query.append(this.CheckCriteria());
			this.query.append(" record.userName = ");
			this.query.append("'" + userName + "'");
		}
		return this;
	}

	public String getQuery() {
		return this.query.toString() + " order by record.time desc ";
	}

	private String CheckCriteria() {
		String crit = "";
		if (this.wasCreteriaAdded) {
			crit = " AND";
		} else {
			crit = " WHERE";
			this.wasCreteriaAdded = true;
		}
		return crit;
	}

}
