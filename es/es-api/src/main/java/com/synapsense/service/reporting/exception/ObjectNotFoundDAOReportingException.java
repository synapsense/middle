package com.synapsense.service.reporting.exception;

import javax.ejb.ApplicationException;

@ApplicationException(rollback = false)
public class ObjectNotFoundDAOReportingException extends EnvDAOReportingException {

	private static final long serialVersionUID = 3651618335668494448L;

	public ObjectNotFoundDAOReportingException() {
	}

	public ObjectNotFoundDAOReportingException(String message, Throwable cause) {
		super(message, cause);
	}

	public ObjectNotFoundDAOReportingException(String message) {
		super(message);
	}

	public ObjectNotFoundDAOReportingException(Throwable cause) {
		super(cause);
	}
}
