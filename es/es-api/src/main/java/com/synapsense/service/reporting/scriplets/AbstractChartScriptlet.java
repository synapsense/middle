package com.synapsense.service.reporting.scriplets;

import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.exception.EnvException;
import com.synapsense.exception.IllegalInputParameterException;
import com.synapsense.service.Environment;
import com.synapsense.service.LocalizationService;
import com.synapsense.service.UnitSystemsService;
import com.synapsense.service.UserManagementService;
import com.synapsense.service.reporting.ReportingSessionContext;
import com.synapsense.service.reporting.datasource.ReportingTableDataSource;
import com.synapsense.service.reporting.dto.CellTO;
import com.synapsense.service.reporting.dto.RowTO;
import com.synapsense.service.reporting.dto.TableTO;
import com.synapsense.service.reporting.exception.ReportingRuntimeException;
import com.synapsense.service.reporting.exception.ReportingServiceSystemException;
import com.synapsense.service.reporting.olap.RangeSet;
import com.synapsense.service.reporting.olap.custom.Cube;
import com.synapsense.service.reporting.olap.custom.PropertyHierarchy.ReportingPropertyName;
import com.synapsense.service.reporting.scriplets.charts.ChartBuilder;
import com.synapsense.service.reporting.scriplets.charts.SensorPoint;
import com.synapsense.service.user.EnvUserContext;
import com.synapsense.service.user.UserContext;
import com.synapsense.util.CollectionUtils;
import com.synapsense.util.ConvertingEnvironmentException;
import com.synapsense.util.ServerLoginModule;
import com.synapsense.util.unitconverter.ConverterException;
import com.synapsense.util.unitconverter.Dimension;
import com.synapsense.util.unitconverter.UnitResolvers;
import com.synapsense.util.unitconverter.UnitResolvers.DimensionRef;
import com.synapsense.util.unitconverter.UnitResolvers.PropertyUnits;
import com.synapsense.util.unitconverter.UnitSystems;
import net.sf.jasperreports.engine.JRDefaultScriptlet;
import net.sf.jasperreports.engine.JRPropertiesMap;
import net.sf.jasperreports.engine.JRScriptletException;
import net.sf.jasperreports.renderers.JCommonDrawableRenderer;

import org.jfree.chart.JFreeChart;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.awt.*;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.*;
import java.util.List;
import java.util.Map.Entry;

public abstract class AbstractChartScriptlet extends JRDefaultScriptlet
{
	private static LocalizationService localizationService;
	private static Environment env;
	private static UserManagementService userManagement;
	private static UnitSystemsService unitSystemsService;
	protected static boolean inited = false;
	
	public static final String DEFAULT_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
	private static InitialContext ctx;
	
	protected final static String OBJECTS = "Objects";
    protected final static String AGGREGATION_RANGE = "AggregationRange";
    protected final static String COLUMN_PROPERTIES = "ColumnProperties";
    protected final static String DATE_RANGE = "DateRange";
	private static final int ROW_HEADER = 0;
	private static final int COLUMN_HEADER = 1;
	private static final String TIMESTAMP = "Timestamp";
	protected final static String IS_CUSTOM_SPECIFIED = "isCustomSpecified";
	protected final static String PROPERTIES = "properties";
	protected final static String DIMENSIONS = "dimensions";
	protected final static String LINE_COLOR = "lineColor";
	protected final static String COLOR = "color";
	protected final static String RANGE = "range";
    protected final static String DIMENSION_NAME = "name";
    protected final static String DIMENSION_VALUE = "value";
    protected final static String DIMENSION_START = "start";
    protected final static String DIMENSION_END = "end";

	protected TableTO reportTable;
	protected ReportingTableDataSource reportingChartDataSource;
	private NumberFormat numberFormat;
	private DateFormat dateFormat;
	private Locale locale;
	protected Integer bins = 0;
	
	protected Map<String, Color> propertyColorMap = CollectionUtils.newMap();
	
	protected Map<String, ArrayList<Float>> boundsMap = CollectionUtils.newMap();
	
	protected String targetSystem;
	protected UnitResolvers unitResolvers;
	protected UnitSystems unitSystems;

    private JFreeChart getChart() throws Exception {

		JFreeChart timeChart = buildChart();
		//Customize the chart. 
		standartCustom(timeChart);
     	return timeChart;
		
	}

    protected abstract ChartBuilder getChartBuilder();
    
    protected Collection<TO<?>> getObjectsByType(String typeName) {
    	return env.getObjectsByType(typeName);
    }

	private JFreeChart buildChart() {
        ChartBuilder chartBuilder = getChartBuilder();

        Map<String, Color> seriesNameColorsMap = CollectionUtils.newMap();

		if (reportTable == null){
			return chartBuilder.getChart(numberFormat);
		}
			
			//Histogram
			TO<?> parent = null;
			try {
				parent = ((HashSet<TO<?>>)getParameterValue(OBJECTS)).iterator().next();
			} catch (JRScriptletException e) {
				throw new ReportingServiceSystemException("Unable to get report parent");
			} 
			
			Map<String, ArrayList<SensorPoint>> pointsMap = CollectionUtils.newLinkedMap();
			//Fill needed dimensions for columns
			Map<String, Dimension> seriesDimensionMap = CollectionUtils.newMap();
			List<String> columnHeaders = reportTable.getHeader(COLUMN_HEADER);
			for (String header : columnHeaders) {
				String members [] = header.split(",");
				String [] objectString = members[0].split("\\.");
				TO<?> object = TOFactory.getInstance().loadTO(objectString[2]);
				String [] property = members[1].split("\\.");
				String propertyName = property[3];
				seriesDimensionMap.put(propertyName, getDimension(targetSystem, unitResolvers, unitSystems, object, propertyName));
				if (object.getTypeName().equals(parent.getTypeName())) {
					pointsMap.put(header, new ArrayList<SensorPoint>());
				}
				
			}
			
			for (RowTO reportRow : reportTable) {	
				for (CellTO cell : reportRow) {
					StringBuilder seriesNameSB = new StringBuilder();
					TO<?> object = cell.getObject();
					if (!object.getTypeName().equals(parent.getTypeName())) {
						object = parent;
					} 
					String propertyName = cell.getPropertyName();
					seriesNameSB.append(Cube.DIM_OBJECT + ".")
						.append(object.getTypeName())
						.append(".")
						.append(object.toString())
						.append(",")
						.append(Cube.DIM_PROPERTY + ".")
						.append("Name.")
						.append(propertyName);
					
					//Getting time
					List<Long> checkedList = CollectionUtils.newList();
				    for (Object timestamp : List.class.cast(cell.get(TIMESTAMP))) {
				    	checkedList.add(Long.class.cast(timestamp));
				    }
					long timestamp;
				    if (!checkedList.isEmpty()) {
						timestamp = checkedList.iterator().next();
					} else {
						continue;
					}
					//Getting value
					Number value = cell.getValue();
					if (value != null) {
						String valueString = (String) numberFormat.format(value);
						try {
							value = numberFormat.parse(valueString).doubleValue();
						} catch (ParseException e) {
							throw new ReportingRuntimeException("Failed to parse "
									+ valueString, e);
						}
					}
					
					String seriesName = seriesNameSB.toString();
					ArrayList<SensorPoint> series = pointsMap.get(seriesName);
					SensorPoint sensorPoint = new SensorPoint(value, timestamp);
					if (series == null) {
						series = new ArrayList<>(reportRow.size());
						series.add(sensorPoint);
						pointsMap.put(seriesName, series);
					} else {
						pointsMap.get(seriesNameSB.toString()).add(sensorPoint);
					}
					
				}
			}
			
			for (Entry<String, ArrayList<SensorPoint>> entry : pointsMap.entrySet()) {
				
				String series = entry.getKey();
				
				String[] seriesAttributes = series.split(",");
				String name;
				TO<?> object = null;
				try {
					String [] objectString = seriesAttributes[0].split("\\.");
					object = TOFactory.getInstance().loadTO(objectString[2]);
					name = env.getPropertyValue(object, "name", String.class);
				} catch (EnvException e) {
					throw new ReportingServiceSystemException("Unable to get name for object " + object);
				}
				
				String [] property = seriesAttributes[1].split("\\.");
				String objType = property[2];
				String propName = property[3];
				Dimension dimension = seriesDimensionMap.get(propName);
				
				String seriesName = name + ":"
					+ getDisplayablePropertyName(objType, propName, locale);
				//put Color
				if (!propertyColorMap.isEmpty()) {
					seriesNameColorsMap.put(seriesName, propertyColorMap.get(propName));
				}
				
				String seriesId = null;
				String axisName = null;
				if (bins != 0) {
					axisName = "Measures";
					seriesId = dimension.getName();
				} else {
					axisName = getAxisName(dimension, objType + "." + propName + "." + property[4]);
					seriesId = dimension.getName();
				}
				
				chartBuilder.addDataSerie(
						seriesId,
						entry.getValue(),
						seriesName, axisName, bins);
			}
			if (!propertyColorMap.isEmpty()) {
				chartBuilder.setSeriesNameColorsMap(seriesNameColorsMap);
			}
			if (!boundsMap.isEmpty()) {
				for (Entry<String, ArrayList<Float>> entry : boundsMap.entrySet()) {
					ArrayList<Float> range = entry.getValue();
					Float bottomBound = range.get(0);
					Float upperBound = range.get(1);
					chartBuilder.addAxisBounds(entry.getKey(), bottomBound, upperBound);
				}
			}
    	JFreeChart timeChart = chartBuilder.getChart(numberFormat);
    	
//    	ChartCustomizer.standartCustom(timeChart);
//        standartCustom(timeChart);
    	return timeChart;
    	
	}

	private String getAxisName(Dimension dimension, String fullPropertyName) {
		String axisName = null;
		if (!dimension.getUnits().isEmpty()) {
			axisName = localizationService.getString(dimension.getUnits());
		} else if (!dimension.getLongUnits().isEmpty()){
			String longUnits = dimension.getLongUnits();
			if (longUnits != null && !longUnits.isEmpty() && longUnits.contains("/")) {
				longUnits = longUnits.replace("/", "_");
			}
			axisName = localizationService.getString(longUnits);
		} else {
			ReportingPropertyName reportingPropertyName = new ReportingPropertyName(fullPropertyName);
			axisName = getDisplayablePropertyName(reportingPropertyName.getObjectTypeName(), reportingPropertyName.getPropertyName(), locale);
		}
		return axisName;
	}
	
	protected abstract void standartCustom(JFreeChart timeChart);

    protected abstract void getData(RangeSet period) throws JRScriptletException;

	@Override
	public void afterReportInit() throws JRScriptletException
	{
        
		try {
        	RangeSet period = (RangeSet) this.getParameterValue(DATE_RANGE);
        	initConnection();
        	getData(period);
	        JFreeChart chart = getChart();
       	this.setVariableValue("Chart", new JCommonDrawableRenderer(chart));
        } catch (Exception e) {
	        e.printStackTrace();
        }
        
	}
	
	private void initConnection() {
		
		if (!inited) {
			synchronized (AbstractChartScriptlet.class) {
				if (!inited) {
					
					Properties connectProperties = new Properties();
					
					connectProperties.setProperty(Context.URL_PKG_PREFIXES, "org.jboss.ejb.client.naming");
					
					try{
						ctx = new InitialContext(connectProperties);
					} catch (NamingException e) {
						throw new IllegalStateException("Cannot create initial context to connect ES services", e);
					}
					
					userManagement = getProxy(ctx, "es-core", "UserManagementService", UserManagementService.class);
					env = getProxy(ctx, "es-core", "Environment", Environment.class);
					localizationService = getProxy(ctx, "es-core", "LocalizationService", LocalizationService.class);
					unitSystemsService = getProxy(ctx, "es-core", "UnitSystemsServiceImpl", UnitSystemsService.class);
					
				}
	            inited = true;
            }
		}

	}
	
	protected void initUserPreferences() {
    	String userName = getUserName(reportTable.getUserId());
    	UserContext userContext = new EnvUserContext(userName, userManagement);
		numberFormat = userContext.getDataFormat();
        dateFormat = userContext.getDateFormat();
		locale = userContext.getLocale();
		unitSystems = unitSystemsService.getUnitSystems();
		unitResolvers = unitSystemsService.getUnitReslovers();
		targetSystem = userContext.getUnitSystem();
		locale = userContext.getLocale();
	}
	
	protected static <INTERFACE> INTERFACE getProxy(InitialContext ctx, String moduleName, String jndiTarget, Class<INTERFACE> clazz) {
		try {
			return clazz.cast(ServerLoginModule.getService(ctx, moduleName, jndiTarget, clazz));
		} catch (NamingException e) {
			throw new IllegalStateException("Error acquiring reference to " + jndiTarget, e);
		}
	}
	
    public static String getDisplayablePropertyName(String typeName, String propertyName, Locale locale) {
		return localizationService.getString("object_property_" + typeName.toLowerCase() + "_" + propertyName.toLowerCase(), locale);
    }
    
    private TO<?> getUserById(Integer userId) {
    	for (TO<?> user : userManagement.getAllUsers()) {
    		if (user.getID().equals(userId)) {
    			return user;
    		}
    	}
    	return null;
    }
	
	public static Dimension getDimension(String targetSystem, UnitResolvers unitResolvers, UnitSystems unitSystems, TO<?> to, String propName) {
		// get property units
		PropertyUnits pu = unitResolvers.getUnits(to.getTypeName(), propName);
		if (pu == null)
			throw new ConvertingEnvironmentException("Units are not described for type \"" + to.getTypeName()
			        + "\", property \"" + propName + "\"");
		DimensionRef dimRef = pu.getDimensionReference(to, env);
		// first check if that's an unconvertible dimension
		Dimension dim = unitSystems.getSimpleDimensions().get(dimRef.getDimension());
		if (dim == null) {
			// ok, let's try to get a convertible dimension then
			// (unconvertible dimensions are available for any system,
			// not only for defining system)
			try {
				dim = unitSystems.getUnitSystem(targetSystem == null ? dimRef.getSystem() : targetSystem).getDimension(
				        dimRef.getDimension());
			} catch (ConverterException e) {
				throw new ConvertingEnvironmentException("Cannot find dimension for type \"" + to.getTypeName()
				        + "\", property \"" + propName + "\"", e);
			}
		}
		return dim;
	}
	
	protected void fillPropertyLineOptionMap(JSONArray propertiesJsonArray) throws JSONException {
        
		for (int i = 0; i < propertiesJsonArray.length(); i++) {
            JSONObject jsonObject = propertiesJsonArray.getJSONObject(i);
            if ((boolean) jsonObject.get("value") == true) {
            	String fullPropertyName = (String) jsonObject.get("name");                
                Color lineColor = null;
                String propertyName = null;
                if (jsonObject.has("lineColor") 
                		&& !jsonObject.get("lineColor").equals(null)
                		&& !jsonObject.get("lineColor").equals("")){
                	String [] objectString = fullPropertyName.split("\\.");
					lineColor = new Color((int) jsonObject.get("lineColor"));
					propertyName = objectString[1];
					propertyColorMap.put(propertyName, lineColor);
                }                
            }
        }
		
	}

    public static Dimension getDimensionByName (String dataclass, String targetSystem, UnitSystems unitSystems) {

        if (dataclass == null) {
            throw new IllegalInputParameterException("Dataclass shouldn't be null");
        }
        if (targetSystem == null) {
            throw new IllegalInputParameterException("Target System shouldn't be null");
        }

        Dimension dimension = unitSystems.getSimpleDimensions().get(dataclass);
        if (dimension == null) {
            try {
                dimension = unitSystems.getUnitSystem(targetSystem).getDimension(dataclass);
            } catch (ConverterException e) {
                throw new ReportingRuntimeException("Unable to retrieve dimension for name: " + dataclass, e);
            }
        }

        if (dimension == null) {
            throw new ReportingRuntimeException("Unable to retrieve dimension for name: " + dataclass);
        }
        return dimension;

    }

	protected void fillDimensionOptionMap(JSONArray dimensionsJsonArray, UnitSystems unitSystems) throws JSONException {
        
		for (int i = 0; i < dimensionsJsonArray.length(); i++) {
            JSONObject jsonObject = dimensionsJsonArray.getJSONObject(i);
            String dimensionName = (String) jsonObject.get(DIMENSION_NAME);
            Dimension dimension = getDimensionByName(dimensionName, targetSystem, unitSystems);

//        	Dimension dimension = unitSystemsService.getUnitSystems().getSimpleDimensions().get(dimensionName);
//        	if (dimension == null) {
//        		try {
//					dimension = unitSystemsService.getUnitSystems().getUnitSystem(targetSystem).getDimension(dimensionName);
//				} catch (ConverterException e) {
//					throw new ReportingRuntimeException("Unable to retrieve dimension for name: " + dimensionName, e);
//				}
//        	}
        	String propertyName = "";
        	if (jsonObject.has("propertyName")) {
        		propertyName = (String) jsonObject.get("propertyName");
        	}
        	String axisName = getAxisName(dimension, propertyName);
        	if (jsonObject.has("range")
        			&& !((String) jsonObject.getJSONObject("range").get("start")).equals("")
        			&& !((String) jsonObject.getJSONObject("range").get("end")).equals("")) {
            	JSONObject verticalRange = jsonObject.getJSONObject("range");
            	
            	ArrayList<Float> range = new ArrayList<Float>();
            	String bottomBound = (String) verticalRange.get("start");
            	if (bottomBound.equals("Auto")) {
            		range.add(null);
            	} else {
            		range.add(Float.valueOf(bottomBound));
            	}
            	
            	String upperBound = (String) verticalRange.get("end");
            	if (upperBound.equals("Auto")) {
            		range.add(null);
            	} else {
            		range.add(Float.valueOf(upperBound));
            	}

            	setBounds(axisName, range);
        	}        
        }
		
	}
	
	private void setBounds (String dimension, ArrayList<Float> newRange){
		
		if (boundsMap.containsKey(dimension)) {
			ArrayList<Float> range = new ArrayList<Float>();
			ArrayList<Float> currentRange = boundsMap.get(dimension);
			
			float currentBottomBound = currentRange.get(0);
			float newBottomBound = newRange.get(0);
			range.add(newBottomBound < currentBottomBound ? newBottomBound : currentBottomBound);

			float currentTopBound = currentRange.get(1);
			float newTopBound = newRange.get(1);			
			range.add(newTopBound > currentTopBound ? newTopBound : currentTopBound);
			
			boundsMap.put(dimension, range);
		} else {
			boundsMap.put(dimension, newRange);
		}
		
	}

    
	protected String getUserName(int userId) {
		if (userId <= 0) {
			throw new IllegalArgumentException("Supplied 'userId' is negative or zero");
		}

		String userName = getUserName();
		if (userName != null) {
			return userName;
		}

		TO<?> user = getUserById(userId);
		if (user == null) {
			throw new IllegalArgumentException("There is no user with given 'userId'=" + userId);
		}

		return (String)userManagement.getProperty(user, userManagement.USER_NAME);
	}
	

    protected String getUserName() {
        String userName = ReportingSessionContext.getUserName();
        return userName;
    }

	protected void processCustomValues(JRPropertiesMap reportPropertiesMap, JSONObject columnPropertiesJSON) throws JSONException {
		
		JRPropertiesMap propertiesMap = reportPropertiesMap.cloneProperties();
		//Setting properties colors
		JSONArray properties = columnPropertiesJSON.getJSONArray(PROPERTIES);
        JSONArray dimensions = new JSONArray();

		for (String parameterPropertyName : propertiesMap.getPropertyNames()) {
			for (int i = 0; i < properties.length(); i++) {
				JSONObject property = properties.getJSONObject(i);
				if (property.get("name").equals(parameterPropertyName)) {
					String [] valueArray = propertiesMap.getProperty(parameterPropertyName).split(",");
					for (String propertyParameter : valueArray) {
						String [] propertyParameterArray = propertyParameter.split(":");
						String name = propertyParameterArray[0];
						String value = propertyParameterArray[1];
						
						if (name.equals(COLOR)) {
							Color color = null;
							try {
								color = (Color) Color.class.getField(value).get(null);
							} catch (IllegalArgumentException
									| IllegalAccessException
									| NoSuchFieldException | SecurityException e) {
								throw new ReportingRuntimeException("Color name is not correctly spelled");
							}
							property.put(LINE_COLOR, color.getRGB());
						} else if (name.equals(RANGE)) {
							String[] propertyArray = parameterPropertyName
									.split("\\.");
							TO<?> firstObject = this
									.getObjectsByType(propertyArray[0])
									.iterator().next();
							Dimension dimension = getDimension(targetSystem, unitResolvers, unitSystems, firstObject,
									propertyArray[1]);
							String[] rangeArray = value.split("-");
							JSONObject dimensionJSON = new JSONObject().put(
									DIMENSION_NAME, dimension.getName()).put(
									RANGE,
									new JSONObject().put(DIMENSION_START,
											rangeArray[0]).put(DIMENSION_END,
													rangeArray[1]));
							dimensions.put(dimensionJSON);
						} else {
							throw new ReportingRuntimeException("Property parameter name is not 'color' or 'range'");
						}
					}
					
					
				}								
			}
			
		}
		columnPropertiesJSON.put(DIMENSIONS, dimensions);
		
	} 
}
