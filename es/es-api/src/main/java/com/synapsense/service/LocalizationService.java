package com.synapsense.service;

import java.util.Locale;
import java.util.Map;
import java.util.Properties;

import com.synapsense.exception.IllegalInputParameterException;

/**
 * 
 * 
 * @author Alexander Pakhunov
 * 
 */
public interface LocalizationService {

	/**
	 * Returns array of the available locales
	 * 
	 * @return
	 */
	Locale[] listLocales();

	/**
	 * Returns default locale
	 * 
	 * @return
	 */
	Locale getDefaultLocale();

	/**
	 * Returns a list of available unit systems
	 * 
	 * @return
	 */
	String[] listUnitSystems();

	/**
	 * Returns default unit system
	 * 
	 * @return
	 */
	String getDefaultUnitSystem();

	/**
	 * Returns all translations.
	 * 
	 * @return
	 */
	Map<Locale, Properties> getTranslations();

	/**
	 * Returns user-defined translations.
	 * 
	 * @return
	 */
	Map<Locale, Properties> getUserTranslations();

	/**
	 * Sets user-defined translations.
	 * 
	 * @param translations
	 *            - Map of translations
	 * @return
	 * @throws IllegalInputParameterException
	 *             if translations is null
	 * 
	 */
	void setUserTranslations(Map<Locale, Properties> translations);

	/**
	 * Returns translation from the default locale into specified one.
	 * 
	 * @param string
	 * @param locale
	 * @return
	 * @throws IllegalInputParameterException
	 *             if string is null or empty or locale is null
	 */
	String getTranslation(String string, Locale locale);

	/**
	 * Returns translation from the default locale into specified one. Can split
	 * phrases on sub-phrase and translate sub-phrase independently.
	 * 
	 * @param string
	 * @param locale
	 * @return
	 * @throws IllegalInputParameterException
	 *             if string is null or empty or locale is null
	 */
	String getPartialTranslation(String string, Locale locale);

	/**
	 * Returns localized string by id for default locale.
	 * 
	 * @param id
	 * @return
	 * @throws IllegalInputParameterException
	 *             if id is null or empty
	 */
	String getString(String id);

	/**
	 * Returns localized string by id for provided locale.
	 * 
	 * @param id
	 * @param locale
	 * @return
	 * @throws IllegalInputParameterException
	 *             if id is null or empty or locale is null
	 */
	String getString(String id, Locale locale);

	/**
	 * Returns predefined strings with ids.
	 * 
	 * @param id
	 * @param locale
	 * @return
	 */
	Map<Locale, Properties> getStrings();

}
