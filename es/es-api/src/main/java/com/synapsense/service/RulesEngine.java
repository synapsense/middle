package com.synapsense.service;

import java.io.Serializable;
import java.util.Collection;
import java.util.Set;

import com.synapsense.dto.CRERule;
import com.synapsense.dto.RuleType;
import com.synapsense.dto.TO;
import com.synapsense.exception.CalculationFailedException;
import com.synapsense.exception.ConfigurationException;
import com.synapsense.exception.ObjectExistsException;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.exception.PropertyNotFoundException;

/**
 * The main interface to manage calculated properties
 * 
 * 
 */
public interface RulesEngine {

	interface Result extends Serializable {
		Object getValue();

		boolean isCalculated();

		boolean isCycleDetected();
	}

	/**
	 * Adds a new rule to the model
	 * 
	 * @param rule
	 *            rule description
	 * @return id of just created rule
	 * @throws ObjectExistsException
	 *             if specified rule already exists
	 * @throws ObjectNotFoundException
	 *             if specified rule type inside this rule object doesn't exist
	 * @throws PropertyNotFoundException
	 *             if the specified destination property doesn't exist
	 * @throws ConfigurationException
	 *             if there was a problem to add specified rule to CRE graph
	 */
	TO<?> addRule(CRERule rule) throws ObjectExistsException, ObjectNotFoundException, PropertyNotFoundException,
	        ConfigurationException;

	/**
	 * Adds a rule type to the model
	 * 
	 * @param ruleType
	 *            DTO object describing the rule type
	 * @return identifier of just created rule type
	 * @throws ObjectExistsException
	 *             when passed rule type already exists
	 * @throws ConfigurationException
	 */
	TO<?> addRuleType(RuleType ruleType) throws ObjectExistsException, ConfigurationException;

	/**
	 * Calculates the specified properties
	 * 
	 * @param objIds
	 *            objects' ids
	 * @param propNames
	 *            name of the properties to calculate
	 */
	void calculateProperties(Collection<TO<?>> objIds, Set<String> propNames);

	/**
	 * Calculates specified property if its value is not up to date
	 * 
	 * @param objId
	 *            object id
	 * @param propName
	 *            property name
	 * @param onTimer
	 *            true if the call is scheduled and therefore property value
	 *            must be persisted
	 * @return result of the calculation
	 * @throws CalculationFailedException
	 *             when calculation failed
	 */
	Result calculateProperty(TO<?> objId, String propName, Boolean onTimer) throws CalculationFailedException;

	/**
	 * Returns rule description via its identifier
	 * 
	 * @param ruleId
	 *            rule identifier
	 * @return Rule DTO object describing the rule instance
	 */
	CRERule getRule(TO<?> ruleId);

	/**
	 * Returns rule identifier by name
	 * 
	 * @param name
	 *            rule name
	 * @return rule object TO or 'null' if the rule doesn't exist
	 */
	TO<?> getRuleId(String name);

	/**
	 * Returns all rules linked to specified object id
	 * 
	 * @param objectId
	 *            object identifier
	 * @return collection of rules description
	 */
	Collection<CRERule> getRulesByObjectId(TO<?> objectId);

	/**
	 * Returns all rules of the specified type
	 * 
	 * @param ruleType
	 *            rule type identifier
	 * @return collection of rules
	 * @throws ObjectNotFoundException
	 *             if specified rule type doesn't exist in the model
	 */
	Collection<CRERule> getRulesByType(TO<?> ruleType) throws ObjectNotFoundException;

	/**
	 * Returns all rules of the specified type
	 * 
	 * @param ruleTypeName
	 *            rule type name
	 * @return collection of rules
	 * @throws ObjectNotFoundException
	 *             if specified rule type doesn't exist in the model
	 */
	Collection<CRERule> getRulesByType(String ruleTypeName) throws ObjectNotFoundException;

	/**
	 * Returns rule type description by its identifier
	 * 
	 * @param ruleTypeId
	 *            rule type identifier
	 * @return RuleType DTO object describing the rule type or 'null' if rule
	 *         type with specified id doesn't exist
	 */
	RuleType getRuleType(TO<?> ruleTypeId);

	/**
	 * Returns rule type identifier by rule type full name
	 * 
	 * @param name
	 *            rule type name
	 * @return TO of the rule type or 'null' if rule type with specified name
	 *         doesn't exist
	 */
	TO<?> getRuleTypeId(String name);

	/**
	 * Returns all the rule types stored in the environment
	 * 
	 * @return a collection of rule types value objects
	 */
	Collection<RuleType> getRuleTypes();

	/**
	 * Determines if the passed environment property is calculated by one of the
	 * rules in CRE graph
	 * 
	 * @param objId
	 *            Identifier of the object
	 * @param propName
	 *            property name
	 * @return true if the property is calculated
	 */
	boolean isCalculated(TO<?> objId, String propName);

	/**
	 * Recalculates all the properties in the CRE graph
	 */
	void recalculateAllProperties();

	/**
	 * Removes a rule by its name
	 * 
	 * @param name
	 *            rule name
	 * @return 'false' if the rule doesn't exist in the model
	 */
	boolean removeRule(String name);

	/**
	 * Removes specified rule from the model
	 * 
	 * @param rule
	 *            rule id
	 * @return 'false' if there was an error of deleting a rule from CRE graph
	 */
	boolean removeRule(TO<?> rule);

	/**
	 * Removes specified rule type from the model
	 * 
	 * @param ruleTypeId
	 *            identifier of the rule type
	 * @return 'false' if specified rule type wasn't found or there was an error
	 *         in removing it from the graph
	 * 
	 */
	boolean removeRuleType(TO<?> ruleTypeId);

	/**
	 * Updates the rule type by its identifier
	 * 
	 * @param ruleTypeId
	 *            rule type identifier
	 * @param ruleType
	 *            rule type description
	 * @throws ObjectNotFoundException
	 *             if there is no rule type with passed ruleTypeId
	 * @throws ObjectExistsException
	 *             if there is another rule with the name specified in ruleType
	 *             parameter
	 * @throws ConfigurationException
	 *             if there was an error with updating the rule in CRE graph
	 */
	void updateRuleType(TO<?> ruleTypeId, RuleType ruleType) throws ObjectNotFoundException, ObjectExistsException,
	        ConfigurationException;
}
