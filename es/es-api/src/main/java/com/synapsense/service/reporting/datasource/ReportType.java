package com.synapsense.service.reporting.datasource;

/**
 * Created by ryabovgs on 3/9/14.
 */
public enum ReportType {
    AGGREGATED_TABLE("AggregatedTable"),
    CHART("Chart"),
    HISTOGRAM("Histogram");

    private String name;

    ReportType(String name) {
        this.name = name;
    }

    public static ReportType getTypeByName(String name) {
        for (ReportType type : ReportType.values()) {
            if (type.getName().equals(name))
                return type;
        }
        throw new IllegalArgumentException("Not supported Report Type" + name);
    }

    public String getName() {
        return name;
    }

}
