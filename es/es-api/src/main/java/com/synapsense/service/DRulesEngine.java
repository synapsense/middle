package com.synapsense.service;

import com.synapsense.dto.TO;
import com.synapsense.exception.MalformedObjectException;
import com.synapsense.exception.RuleCompilationException;
import com.synapsense.exception.RuleInstantiationException;

//@Remote
public interface DRulesEngine {
	/**
	 * Attaches object to DRE`s working memory
	 * 
	 * @param objHandle
	 *            handle to object
	 * @throws MalformedObjectException
	 *             when some properties of object used by DRE rules are set to
	 *             nulls
	 */
	void attachObject(TO<?> objHandle) throws MalformedObjectException;

	/**
	 * Retracts object from DRE`s working memory
	 * 
	 * @param objHandle
	 *            handle to object
	 */
	void retractObject(TO<?> objHandle);

	/**
	 * Same as above but periodically fires a rule
	 * 
	 * @param objHandle
	 * @param periodicalUpdatePeriod
	 *            - in millisecs
	 * @throws MalformedObjectException
	 */
	void attachObject(TO<?> objHandle, long periodicalUpdatePeriod) throws MalformedObjectException;

	/**
	 * Notifies the DRE about changes occured on given objHandle
	 * 
	 * @param objHandle
	 *            object being changed handle
	 */
	void changeObjectState(TO<?> objHandle);

	/**
	 * Creates a new domain class for given type name. It is useful when we are
	 * adding rules before to TOs
	 * 
	 * @param typeName
	 */
	void createDomainClass(String typeName);

	/**
	 * Compiles a rule text (checks the rule syntax)
	 * 
	 * @param rulePackage
	 *            - rule package text
	 * @param dslPackage
	 *            - used DSL (NULL if is not used)
	 * @throws RuleCompilationException
	 *             - when rule compilation fails
	 */
	void compileRule(String rulePackage, String dslPackage) throws RuleCompilationException;

	/**
	 * Compiles and adds a rule package stored in an Environmental object
	 * 
	 * @param ruleDAOID
	 *            - rule container index
	 * @throws RuleCompilationException
	 *             when rule text cannot be compiled due to any reason (wrong
	 *             syntax, unknown domain objects etc)
	 * @throws RuleInstantiationException
	 *             when rule was compiled but haven't been instantiated - it
	 *             uses wrong data types, NULLs in domain objects or something
	 *             else.
	 */
	void addRulesFromDAO(int ruleDAOID) throws RuleCompilationException, RuleInstantiationException;

	/**
	 * Removes a rule from DRE. Throws IllegalArgument exception if package or
	 * rule within package cannot be found.
	 * 
	 * @param packageName
	 *            - package name
	 * @param ruleName
	 *            - rule name within package
	 */
	void removeRule(String packageName, String ruleName);

	void clearWorkingMemory();
}
