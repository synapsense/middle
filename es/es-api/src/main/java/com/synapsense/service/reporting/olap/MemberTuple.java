package com.synapsense.service.reporting.olap;

import com.synapsense.service.reporting.olap.custom.Cube;
import com.synapsense.util.CollectionUtils;

import java.io.Serializable;
import java.util.*;

public class MemberTuple implements Tuple, Serializable {

	private static final long serialVersionUID = -6989178801948502268L;

	private List<Member> members;

	public MemberTuple(Member... members) {
		this(Arrays.asList(members));
	}

	public MemberTuple(Collection<Member> members) {
		HashSet<Hierarchy> dims = new HashSet<Hierarchy>();
		for (Member member : members) {
			if (dims.contains(member.getHierarchy())) {
				throw new IllegalArgumentException("Duplicate dimension " + member.getHierarchy().getName()
				        + " in a tuple");
			}
			dims.add(member.getHierarchy());
		}
		this.members = new ArrayList<>(members);
	}

	public int getTupleSize() {
		return members.size();
	}

	public Member itemByIndex(int index) {
		return members.get(index);
	}

	@Override
    public Member itemByHierarchy(Hierarchy hierarchy) {
		// Tuple contains no more than 3 members so linear search is faster
		for (Member member : members) {
			if (member.getHierarchy().equals(hierarchy)) {
				return member;
			}
		}
		// all member
	    return null;
    }

    @Override
    public Member itemByDimension(String dimension) {
        // Tuple contains no more than 3 members so linear search is faster
        for (Member member : members) {
            if (Cube.getDimension(member.getHierarchy().getName()).equals(dimension)) {
                return member;
            }
        }
        // all member
        return null;
    }

	public Tuple join(Tuple tuple) {

		ArrayList<Member> newMembers = new ArrayList<>(members);

		for (int i = 0; i < tuple.getTupleSize(); i++) {
			Member member2 = tuple.itemByIndex(i);
			boolean wasMerged = false;
			for (int j = 0; j < members.size(); j++) {
				Member member1 = itemByIndex(j);
				// Now this member might be either merged with some other or
				// added at the end
				if (member1.getHierarchy().equals(member2.getHierarchy())) {
					// merge them
					Member merged = member1.merge(member2);
					if (merged == null) {
						// empty tuple
						return null;
					}
					newMembers.set(j, merged);
					wasMerged = true;
					break;
				}
			}
			if (!wasMerged) {
				newMembers.add(member2);
			}
		}
		return new MemberTuple(newMembers);
	}

	public List<Hierarchy> getDimensionalities() {
		LinkedList<Hierarchy> dims = new LinkedList<>();
		for (Member member : members) {
			dims.addAll(member.getDimensionalities());
		}
		return dims;
	}

	public List<Tuple> getTuples() {
		LinkedList<Tuple> list = new LinkedList<Tuple>();
		list.add(this);
		return list;
	}

	@Override
	public String toString() {
		return new StringBuilder("(").append(CollectionUtils.separate(", ", members)).append(")").toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((members == null) ? 0 : members.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MemberTuple other = (MemberTuple) obj;
		if (members == null) {
			if (other.members != null)
				return false;
		} else if (!members.equals(other.members))
			return false;
		return true;
	}

	@Override
	public void buildQuery(PlanProcessor planProcessor) {
		planProcessor.processSet(this);
	}
}
