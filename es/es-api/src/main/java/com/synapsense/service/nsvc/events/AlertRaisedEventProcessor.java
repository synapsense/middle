package com.synapsense.service.nsvc.events;

/**
 * 
 * @author vadgusev
 * @deprecated Don't use this for catch raised alerts. To catch raised alerts
 *             use ObjectCreatedEventProcessor with filtering by 'ALERT' type.
 */
@Deprecated
public interface AlertRaisedEventProcessor {

	AlertRaisedEvent process(AlertRaisedEvent e);

}
