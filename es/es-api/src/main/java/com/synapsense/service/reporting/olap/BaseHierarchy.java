package com.synapsense.service.reporting.olap;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

public abstract class BaseHierarchy implements Hierarchy, Serializable {

	private static final long serialVersionUID = -7195301835919106607L;

	private String name;
	private Class<?> memberType;

	private List<String> levels;

	public BaseHierarchy(String name, Class<?> memberType, String... levels) {
		this.name = name;
		this.memberType = memberType;
		this.levels = Arrays.asList(levels);
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public Class<?> getMemberType() {
		return memberType;
	}

	@Override
	public boolean isDescendant(String desc, String ans) {
		int index1 = levels.indexOf(desc);
		int index2 = levels.indexOf(ans);
		if (index1 < 0 || index2 < 0) {
			throw new IllegalArgumentException("One of the levels (" + desc + "," + ans
					+ ") doesn't belong to hierarchy " + name);
		}
		return index1 >= index2;
	}

	public String getLower(String level1, String level2) {
		return isDescendant(level1, level2) ? level1 : level2;
	}

	public String getChildLevel(String level) {

		// the list is always going to be very small
		int index = levels.indexOf(level);

		if (index < 0 || index >= levels.size() - 1) {
			return null;
		}

		return levels.get(++index);
	}

	public String getParentLevel(String level) {
		// the list is always going to be very small
		int index = levels.indexOf(level);
		return index < 1 ? null : levels.get(--index);
	}

	@Override
	public String toString() {
		return "Hierarchy [name=" + name + ", levels=" + levels + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((levels == null) ? 0 : levels.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BaseHierarchy other = (BaseHierarchy) obj;
		if (levels == null) {
			if (other.levels != null)
				return false;
		} else if (!levels.equals(other.levels))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	
}
