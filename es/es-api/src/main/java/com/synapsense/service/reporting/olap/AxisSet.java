package com.synapsense.service.reporting.olap;

import java.util.List;

/**
 * Base interface for all possible implementations of sets that can define
 * either an axis or a slicer of a dimensional query
 * 
 * @author Dmitry Grudzinskiy
 * 
 */
public interface AxisSet {

	/**
	 * Returns all hierarchies members of which appear in the set
	 * 
	 * @return list of dimensionalities
	 */
	List<Hierarchy> getDimensionalities();

	/**
	 * Converts this set to a collection of tuples
	 * 
	 * @return list of tuples of this set
	 */
	List<Tuple> getTuples();
	
	void buildQuery(PlanProcessor planProcessor);
}
