package com.synapsense.service.impl.dao;

import java.util.Collection;
import java.util.Date;

import com.synapsense.dto.ValueTO;
import com.synapsense.exception.ObjectNotFoundException;

/**
 * Interface which can be implemented by services which need to provide access
 * to their internal time-spread data.
 * 
 * @author shabanov
 * 
 * @param <KEY_TYPE>
 *            The type of PK of objects which are served by implementor service
 * @param <DATA_CLASS>
 *            The class which serves as DTO for interface implementor service
 */
public interface DataObjectDAO<KEY_TYPE, DATA_CLASS> {

	/**
	 * Allows caller to get all data points by object ID
	 * 
	 * @param id
	 *            ID of object (>0)
	 * @return Collection of all data points
	 */
	public Collection<ValueTO> getData(KEY_TYPE id);

	/**
	 * Allows caller to get data points frame by object ID
	 * 
	 * @param id
	 *            ID of object (>0)
	 * @param size
	 *            The size of data to get (>0)
	 * @param start
	 *            The start pointer of data to get (>=0)
	 * @return Collection of data points which match to the specified conditions
	 */
	public Collection<ValueTO> getData(KEY_TYPE id, int size, int start);

	/**
	 * Allows caller to get the amount of data points which are related to the
	 * specified object.
	 * 
	 * @param id
	 *            ID of object (>0)
	 * @return The overall amount of data points
	 * @throws ObjectNotFoundException
	 *             If there is no object with <tt>id</tt>
	 */
	public int getDataSize(KEY_TYPE id) throws ObjectNotFoundException;

	/**
	 * Allows caller to get data points which lie in the specified time period
	 * 
	 * @param id
	 *            ID of object (>0)
	 * @param startDate
	 *            The start point of time period (not null, < endDate)
	 * @param endDate
	 *            The end point of time period (not null, > startDate)
	 * @return Collection of data points which match to the specified conditions
	 */
	public Collection<ValueTO> getData(KEY_TYPE id, Date startDate, Date endDate);

	/**
	 * Allows caller to get <tt>numberPoints</tt> data points which were created
	 * since <tt>startDate</tt>
	 * 
	 * @param id
	 *            ID of object (>0)
	 * @param startDate
	 *            The start point of time period (not null)
	 * @param numberPoints
	 *            The number of necessary data points(>0)
	 * @return Collection of data points which match to the specified conditions
	 */
	public Collection<ValueTO> getData(KEY_TYPE id, Date startDate, int numberPoints);

	/**
	 * Allows caller to add new data point to the specified object data history
	 * 
	 * @param id
	 *            ID of object (>0)
	 * @param data
	 *            DTO instance (not null)
	 * @throws ObjectNotFoundException
	 *             If there is no object with <tt>id</tt>
	 */
	public void addData(KEY_TYPE id, DATA_CLASS data) throws ObjectNotFoundException;

	/**
	 * Allows caller to add several data points to the specified object data
	 * history
	 * 
	 * @param id
	 *            ID of object (>0)
	 * @param data
	 *            Collection of DTO instances (not null)
	 * @throws ObjectNotFoundException
	 *             If there is no object with <tt>id</tt>
	 */
	public void addBulk(KEY_TYPE id, Collection<DATA_CLASS> data) throws ObjectNotFoundException;

	/**
	 * Allows caller to remove all data points from the specified object data
	 * history.
	 * 
	 * @param id
	 *            ID of object
	 */
	public void removeData(KEY_TYPE id);

}
