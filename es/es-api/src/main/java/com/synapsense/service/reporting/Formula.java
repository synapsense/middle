package com.synapsense.service.reporting;

import com.synapsense.dto.TO;
import com.synapsense.service.reporting.exception.ReportingRuntimeException;
import com.synapsense.service.reporting.olap.Filter;
import com.synapsense.service.reporting.olap.custom.Cube;
import com.synapsense.service.reporting.olap.filters.ComparatorFilter;
import com.synapsense.service.reporting.olap.filters.OrFilter;
import com.synapsense.service.reporting.transform.TableTransform;
import com.synapsense.service.reporting.transform.TableTransformData;
import com.synapsense.util.CollectionUtils;

import java.io.Serializable;
import java.util.*;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * The <code>Formula</code> class contains DimensionalQuery formula common for resulting Report Table.<br>
 * Example: new Formula("avg", "Day", TransformType.ORIGINAL)
 * @author Grigory Ryabov
 * 
 */
public class Formula implements Serializable {

	private static final long serialVersionUID = -3646074567189279696L;

	private String aggregationRange;
	
	private String aggregationType;

    private List<TransformType> transformTypes = CollectionUtils.newList();

    private ArrayList<SortingProperty> sortingProperties = new ArrayList<SortingProperty>();

    private LinkedHashMap<Class<?>, Object> transformDataMap = new LinkedHashMap<>();

	public Formula(String aggregationType, String aggregationRange, TransformType transformType) {
		this.aggregationType = aggregationType;
		this.aggregationRange = aggregationRange;
		this.transformTypes.add(transformType);
	}

	public Formula(String aggregationRange, TransformType transformType) {
		this.aggregationRange = aggregationRange;
		this.transformTypes.add(transformType);
	}

    public Formula(String aggregationRange, TransformType... transformTypes) {
        this.aggregationRange = aggregationRange;
        this.transformTypes.addAll(Arrays.asList(transformTypes));
    }

    public Formula(String sortingPropertiesString, List<ComparatorFilter> filters, TransformType... transformTypes) {

    	try {
			JSONArray sortingPropertiesJSON = new JSONObject(sortingPropertiesString).getJSONArray("properties");
			for (int i = 0; i < sortingPropertiesJSON.length(); i++) {
				JSONObject jsonObject = sortingPropertiesJSON.getJSONObject(i);
				if ((boolean) jsonObject.get("value") == true) {
					sortingProperties.add(new SortingProperty((String) jsonObject.get("name")));
				}
			}
		} catch (JSONException e) {
			throw new ReportingRuntimeException("Sorting Properties String has not conformed to JSON Object" + sortingPropertiesString, e);
		}
        this.transformTypes.addAll(Arrays.asList(transformTypes));
        this.transformDataMap.put(sortingProperties.getClass(), sortingProperties);
        this.aggregationRange = Cube.ALL_RANGE;
        transformDataMap.put(OrFilter.class, new OrFilter(filters.toArray(new Filter[]{})));
        
    }

    public Formula(TableTransformData... tableTransforms) {

    	for (TableTransformData transformData : tableTransforms) {
            for (Object tranformDataItem : transformData.getData()){
            		transformDataMap.put(tranformDataItem.getClass(), tranformDataItem);
            }
            this.transformTypes.add(transformData.getTransformType());

        }
        this.aggregationRange = Cube.ALL_RANGE;

    }

    public <RETURN_TYPE> RETURN_TYPE getTransformData(final Class<RETURN_TYPE> clazz) {
        return clazz.cast(transformDataMap.get(clazz));
    }

    public Formula(SortingProperty sortingProperty, TransformType transformType) {
		this.sortingProperties.add(sortingProperty);
		this.transformTypes.add(transformType);
		this.aggregationRange = Cube.ALL_RANGE;
	}

    public List<TransformType> getTransformTypes() {
        return Collections.unmodifiableList(transformTypes);
    }

    public TO<?> getHostTO() {
        return (TO<?>) getTransformData(HashSet.class).iterator().next();
    }

    public ArrayList<SortingProperty> getSortingProperties() {
        return getTransformData(ArrayList.class) != null ? getTransformData(ArrayList.class) : this.sortingProperties;
    }

    public OrFilter getFilters() {
        return getTransformData(OrFilter.class);
    }

    public String getGroupObjectType() {
        return getTransformData(String.class);
    }

    public static TableTransformData newGroupTransform(String groupObjectType) {
        return new TableTransform(TransformType.GROUP, groupObjectType);
    }

    public static TableTransformData newFilterTransform(List<Filter> filters) {
        return new TableTransform(TransformType.FILTER, filters);
    }

    public static TableTransformData newSortTransform(List<SortingProperty> sortingPropertyList) {
        return new TableTransform(TransformType.SORT, sortingPropertyList);
    }

//    public List<SortingProperty> getSortingProperties() {
//        return Collections.unmodifiableList(sortingProperties);
//    }
//
//    public List<ComparatorFilter> getFilters() {
//        return Collections.unmodifiableList(filters);
//    }

    @Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((aggregationRange == null) ? 0 : aggregationRange.hashCode());
		result = prime * result
				+ ((aggregationType == null) ? 0 : aggregationType.hashCode());
		result = prime
				* result
				+ ((sortingProperties == null) ? 0 : sortingProperties
						.hashCode());
		result = prime
				* result
				+ ((transformDataMap == null) ? 0 : transformDataMap.hashCode());
		result = prime * result
				+ ((transformTypes == null) ? 0 : transformTypes.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Formula other = (Formula) obj;
		if (aggregationRange == null) {
			if (other.aggregationRange != null) {
				return false;
			}
		} else if (!aggregationRange.equals(other.aggregationRange)) {
			return false;
		}
		if (aggregationType == null) {
			if (other.aggregationType != null) {
				return false;
			}
		} else if (!aggregationType.equals(other.aggregationType)) {
			return false;
		}
		if (sortingProperties == null) {
			if (other.sortingProperties != null) {
				return false;
			}
		} else if (!sortingProperties.equals(other.sortingProperties)) {
			return false;
		}
		if (transformDataMap == null) {
			if (other.transformDataMap != null) {
				return false;
			}
		} else if (!transformDataMap.equals(other.transformDataMap)) {
			return false;
		}
		if (transformTypes == null) {
			if (other.transformTypes != null) {
				return false;
			}
		} else if (!transformTypes.equals(other.transformTypes)) {
			return false;
		}
		return true;
	}

    /**
	 * The Enum TransformType.
	 */
	public enum TransformType {
		COLUMN_10("Column", 10),
		ORIGINAL("Original"),
		ROW_1("Row", 1), 
		RAW("Raw"),
		SORT("Sort"),
        FILTER("Filter"),
        GROUP("Group");
		private String name;
		private int size;

		private TransformType(String name, int size){
			this.name = name;
			this.size = size;
		}
		
		private TransformType(String name){
			this.name = name;
		}
		
		public static TransformType getTypeByName(String name) {
			for (TransformType type : TransformType.values()) {
				if (type.name.equals(name))
					return type;
			}
			throw new IllegalArgumentException("Not supported TransformType, type=" + name);	
		}

		public String getName() {
			return this.name;
		}
		
		public TransformType setSize(int size) {
			this.size = size;
			return this;
		}

		public int getSize() {
			return this.size;
		}
	}
	
	public String getAggregationRange() {
		return this.aggregationRange;
	}
	
	public String getAggregationType() {
		return this.aggregationType;
	}
	
	public String getOrderByClausePart() {
		return this.aggregationType + "(valued)";
	}

}
