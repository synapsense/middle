package com.synapsense.service.reporting.datasource;

/**
 * Created by Grigory Ryabov on 23.01.14.
 */
public enum ReferenceObjectType {
    ANY_DESCENDANT_OBJECT("any");

    private String name;

    ReferenceObjectType(String name) {
        this.name = name;
    }

    public static ReferenceObjectType getTypeByName(String name) {
        for (ReferenceObjectType type : ReferenceObjectType.values()) {
            if (type.getName().equals(name))
                return type;
        }
        throw new IllegalArgumentException("Not supported Reference Object Type" + name);
    }

    public String getName() {
        return name;
    }
}
