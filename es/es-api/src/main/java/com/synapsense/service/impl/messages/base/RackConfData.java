package com.synapsense.service.impl.messages.base;

import java.io.Serializable;
import java.util.HashMap;

public class RackConfData implements Serializable {

	private static final long serialVersionUID = 2730011283052467993L;

	public static final HashMap<Integer, String> RACK_PROP_MAP = new HashMap<Integer, String>();

	static {
		RACK_PROP_MAP.put(0, "leftRack");
		RACK_PROP_MAP.put(1, "centerRack");
		RACK_PROP_MAP.put(2, "rightRack");
	}

	private int rack;
	private int uHeight;
	private int rpduType;

	public int getRack() {
		return rack;
	}

	public int getuHeight() {
		return uHeight;
	}

	public int getRpduType() {
		return rpduType;
	}

	public void setRack(int rack) {
		this.rack = rack;
	}

	public void setuHeight(int uHeight) {
		this.uHeight = uHeight;
	}

	public void setRpduType(int rpduType) {
		this.rpduType = rpduType;
	}
}
