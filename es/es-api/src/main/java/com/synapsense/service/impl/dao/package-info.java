/**
 * Contains classes and interfaces specific for DAL implementation.
 */
package com.synapsense.service.impl.dao;