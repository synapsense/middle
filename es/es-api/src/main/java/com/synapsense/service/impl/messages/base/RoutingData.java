/**
 * 
 */
package com.synapsense.service.impl.messages.base;

import java.io.Serializable;

/**
 * @author Alexander Borisov
 * 
 */
public class RoutingData implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5376259810820499089L;
	private long parentId;
	private long childId;
	private int curId;

	public long getParentId() {
		return parentId;
	}

	public void setParentId(long parentId) {
		this.parentId = parentId;
	}

	public long getChildId() {
		return childId;
	}

	public void setChildId(long childId) {
		this.childId = childId;
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer(20);
		sb.append("parentId=");
		sb.append(parentId);
		sb.append("\n");

		sb.append("childId=");
		sb.append(childId);
		sb.append("\n");

		return sb.toString();
	}

	public RoutingData(long parentId, int curId, long childId) {
		super();
		this.parentId = parentId;
		this.childId = childId;
		this.curId = curId;
	}

	public int getCurId() {
		return curId;
	}

	public void setCurId(int curId) {
		this.curId = curId;
	}

}
