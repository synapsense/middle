package com.synapsense.service.nsvc.filters;

import java.io.Serializable;

import com.synapsense.service.nsvc.Event;
import com.synapsense.service.nsvc.EventProcessor;

public class NotFilter implements EventProcessor, Serializable {

	private static final long serialVersionUID = -6492940064752374809L;

	private final EventProcessor filter;

	public NotFilter(EventProcessor filter) {
		this.filter = filter;
	}

	@Override
	public Event process(Event e) {
		if (filter == null)
			return e;
		Event e1 = filter.process(e);
		return e1 == null ? e : null;
	}

}
