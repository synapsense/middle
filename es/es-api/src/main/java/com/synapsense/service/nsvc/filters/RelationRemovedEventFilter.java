package com.synapsense.service.nsvc.filters;

import java.io.Serializable;

import com.synapsense.service.nsvc.events.RelationRemovedEvent;
import com.synapsense.service.nsvc.events.RelationRemovedEventProcessor;

/**
 * RelationRemovedEvent filter that matches parentTo, childTo or both against
 * the specified TOMatchers.
 * 
 * @author Oleg Stepanov
 */
public class RelationRemovedEventFilter implements RelationRemovedEventProcessor, Serializable {

	private static final long serialVersionUID = 4822236605539057539L;

	private final TOMatcher parentMatcher, childMatcher;

	/**
	 * Create filter with parent matcher and/or child matcher (either one or
	 * both shall be specified).
	 * 
	 * @param parentMatcher
	 *            parent TO matcher.
	 * @param childMatcher
	 *            child TO matcher.
	 * @throws IllegalArgumentException
	 *             if both matchers are <code>null</code>
	 */
	public RelationRemovedEventFilter(TOMatcher parentMatcher, TOMatcher childMatcher) {
		if (parentMatcher == null && childMatcher == null)
			throw new IllegalArgumentException("Either parentMatcher or childMatcher must not be null");
		this.parentMatcher = parentMatcher;
		this.childMatcher = childMatcher;
	}

	@Override
	public RelationRemovedEvent process(RelationRemovedEvent e) {
		boolean matches = (parentMatcher == null || parentMatcher.match(e.getParentTO()))
		        && (childMatcher == null || childMatcher.match(e.getChildTO()));
		return matches ? e : null;
	}
}
