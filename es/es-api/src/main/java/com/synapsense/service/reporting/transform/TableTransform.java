package com.synapsense.service.reporting.transform;

import com.synapsense.service.reporting.Formula.TransformType;
import com.synapsense.util.CollectionUtils;

import java.util.Collections;
import java.util.List;

/**
 * Created by Grigory Ryabov on 03.03.14.
 */
public class TableTransform implements TableTransformData {

    protected List<Object> transformDataList = CollectionUtils.newList();

    protected TransformType transformType;

    public TableTransform(TransformType transformType, Object transformItem) {

        this.transformType = transformType;
        this.transformDataList.add(transformItem);

    }

    public TableTransform(TransformType transformType, Object... transformItems) {

        this.transformType = transformType;
        for (Object transformDataItem : transformItems) {
            this.transformDataList.add(transformDataItem);
        }

    }

    @Override
    public TransformType getTransformType() {
        return this.transformType;
    }

    @Override
    public List<Object> getData() {
        return Collections.unmodifiableList(transformDataList);
    }

}
