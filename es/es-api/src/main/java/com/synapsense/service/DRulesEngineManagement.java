package com.synapsense.service;

import com.synapsense.exception.RuleCompilationException;
import com.synapsense.exception.RuleInstantiationException;

/**
 * @author anechaev
 * Drool-based Rules engine management interface
 */
//@Management
/**
 * @author anechaev
 * 
 */
public interface DRulesEngineManagement {
	/**
	 * Compiles and adds a rule package into rule engine service
	 * 
	 * @param rulesPackage
	 *            - string containing rules notnull
	 * @param dslPackage
	 *            - rules DSL. empty string or null value means that no dsl is
	 *            used
	 * @throws RuleCompilationException
	 *             when rule package cannot be compiled.
	 * @throws RuleInstantiationException
	 *             when rule package cannot be instantiated. real error clause
	 *             is stored in exception description paramenter.
	 */
	void addRules(String rulesPackage, String dslPackage) throws RuleCompilationException, RuleInstantiationException;

	/**
	 * Compiles and adds a rule package stored in external file
	 * 
	 * @param rulesPackageFile
	 *            - file name
	 * @throws RuleCompilationException
	 * @throws RuleInstantiationException
	 */
	void addRulesFromFile(String rulesPackageFile) throws RuleCompilationException, RuleInstantiationException;

	/**
	 * Returns a set of rules (rule names) added to DRE. Each rule record
	 * consists of 2 fields: package name and rule name within package. Package
	 * name is stored in 0-indexed element of each record. Rule name is stored
	 * in 1-indexed element of record.
	 * 
	 * @return array containing rule
	 */
	String[][] enumerateRules();

	/**
	 * Removes a rule package from DRE. May throw IllegalArgumentException if
	 * packageName is null or package with such name is not found.
	 * 
	 * @param packageName
	 *            - package name
	 */
	void removeRulePackage(String packageName);

	/**
	 * 
	 * Life cycle :-)
	 */
	void create() throws Exception;

	void start() throws Exception;

	void stop();

	void destroy();

}
