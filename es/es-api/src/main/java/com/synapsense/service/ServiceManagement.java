package com.synapsense.service;

public interface ServiceManagement {

	void create() throws Exception;

	void destroy() throws Exception;

	void stop() throws Exception;

	void start() throws Exception;
}
