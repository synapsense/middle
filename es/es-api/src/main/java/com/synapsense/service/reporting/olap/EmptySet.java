package com.synapsense.service.reporting.olap;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

public class EmptySet implements AxisSet, Serializable {

	private static final long serialVersionUID = 2403121148339887156L;

	public List<Hierarchy> getDimensionalities() {
		return new LinkedList<Hierarchy>();
	}

	public List<Tuple> getTuples() {
		return new LinkedList<Tuple>();
	}

	@Override
	public String toString() {
		return "{}";
	}

	@Override
	public void buildQuery(PlanProcessor planProcessor) {
	}
}