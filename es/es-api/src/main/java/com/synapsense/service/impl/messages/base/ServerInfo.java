package com.synapsense.service.impl.messages.base;

public interface ServerInfo {

	public int getuPos();

	public int getuHeight();

	public int getRack();

}
