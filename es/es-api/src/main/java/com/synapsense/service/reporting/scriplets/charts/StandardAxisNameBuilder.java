package com.synapsense.service.reporting.scriplets.charts;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class StandardAxisNameBuilder extends AxisNameBuilder {
	private Integer MAX_AXIS_NAME_LENGTH = 50;

	public void addAxisNameAndUnit(String serie_id, String axis_name, String serie_unit){
		
		//put serie names
		List<String> axisNames = null;
		if(serieIdAxisNamesMap.containsKey(serie_id)){
			axisNames = serieIdAxisNamesMap.get(serie_id);
			if(!axisNames.contains(axis_name)){
				axisNames.add(axis_name);
			}			
		}else{
			axisNames = new ArrayList<String>();
			axisNames.add(axis_name);
			serieIdAxisNamesMap.put(serie_id, axisNames);
		}
		
		//put unit
		serieIdSerieUnitMap.put(serie_id, serie_unit);
	}
	
	public HashMap <String, String> getAxisNameMap(){
		HashMap <String, String> axisNameMap = new HashMap<String, String>();
				
		Iterator<String> it = serieIdSerieUnitMap.keySet().iterator();
		
		//for each serie id
		while(it.hasNext()){
			String serieId = it.next();
			
			//gather axis name
			List<String> axisNames = serieIdAxisNamesMap.get(serieId);
			String axisName = "";			
			for(int i = 0; i < axisNames.size(); i++){				
				if(i > 0){ axisName += ", "; }				
				axisName += axisNames.get(i);				
			}
			
			//add units to the axis name
			String unit = serieIdSerieUnitMap.get(serieId);
			if(unit != null && !unit.isEmpty()){
				axisName += ", " + unit;
			}
			
			//if axis name length is greater than MAX, set it name as unit name 
			if(axisName.length() > MAX_AXIS_NAME_LENGTH){
				axisName = unit;
			}
			
			//add axis name to the map
			axisNameMap.put(serieId, axisName);
		}
		
		return axisNameMap;
	}

}
