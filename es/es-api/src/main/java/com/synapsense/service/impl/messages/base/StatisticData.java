package com.synapsense.service.impl.messages.base;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;

public final class StatisticData implements Serializable {

	private static final long serialVersionUID = -1940813590170574043L;

	private Integer networkId;
	private Integer logicalId;
	private Date nwkTimestamp;
	private Integer sendFailsFullQueue;
	private Integer sendFailsNoPath;
	private Integer numberOneHopRetransmission;
	private Integer forwardingPacketsDropped;
	private Integer parentFailCount;
	private Integer broadcastRetransmission;
	private Integer packetsFromBaseStation;
	private Integer packetsToBaseStation;
	private Integer packetsForwarded;
	private Integer packetsSent;
	private Integer radioOnTime;
	private Integer freeSlotCount;
	private Integer wrongSlotCount;
	private Integer totalSlotCount;
	private Integer packetsRcvdCount;
	private Integer crcFailPacketCount;
	private Integer timeSyncsLostDriftDetection;
	private Integer timeSyncsLostTimeout;
	private Integer nodeLatency;
	private Integer outOfOrderPackets;
	private Integer duplicatePackets;
	private Integer packetsFromNodes;

	private int[] chStatus;
	private Integer hopCount;
	private Integer routerState;
	private Integer parentState;
	private Integer noiseThresh;

	public int[] getChStatus() {
		return chStatus.clone();
	}

	public void setChStatus(int[] chStatus) {
		if (chStatus == null)
			throw new IllegalArgumentException("Supplied 'chStatus' is null");
		if (chStatus.length > 16)
			throw new IllegalArgumentException("Supplied 'chStatus' should be <=16");
		this.chStatus = chStatus.clone();
	}

	public Integer getHopCount() {
		return hopCount;
	}

	public void setHopCount(Integer hopCount) {
		this.hopCount = hopCount;
	}

	public Integer getRouterState() {
		return routerState;
	}

	public void setRouterState(Integer routerState) {
		this.routerState = routerState;
	}

	public Integer getParentState() {
		return parentState;
	}

	public void setParentState(Integer parentState) {
		this.parentState = parentState;
	}

	public Integer getNoiseThresh() {
		return noiseThresh;
	}

	public void setNoiseThresh(Integer noiseThresh) {
		this.noiseThresh = noiseThresh;
	}

	public StatisticData() {
		this.chStatus = new int[16];
		this.hopCount = 0;
		this.routerState = 0;
		this.parentState = 0;
		this.noiseThresh = 0;
	}

	/**
	 * Full node statistic constructor. Initializes one record of network node
	 * statistic.
	 * 
	 * @param networkId
	 * @param logicalId
	 * @param nwkTimestamp
	 * @param sendFailsFullQueue
	 * @param sendFailsNoPath
	 * @param numberOneHopRetransmission
	 * @param forwardingPacketsDropped
	 * @param parentFailCount
	 * @param broadcastRetransmission
	 * @param packetsFromBaseStation
	 * @param packetsToBaseStation
	 * @param packetsForwarded
	 * @param packetsSent
	 * @param radioOnTime
	 * @param freeSlotCount
	 * @param wrongSlotCount
	 * @param totalSlotCount
	 * @param packetsRcvdCount
	 * @param crcFailPacketCount
	 * @param timeSyncsLostDriftDetection
	 * @param timeSyncsLostTimeout
	 * @param nodeLatency
	 * @param outOfOrderPackets
	 * @param duplicatePackets
	 * @param packetsFromNodes
	 */
	public StatisticData(Integer networkId, Integer logicalId, Date nwkTimestamp, Integer sendFailsFullQueue,
	        Integer sendFailsNoPath, Integer numberOneHopRetransmission, Integer forwardingPacketsDropped,
	        Integer parentFailCount, Integer broadcastRetransmission, Integer packetsFromBaseStation,
	        Integer packetsToBaseStation, Integer packetsForwarded, Integer packetsSent, Integer radioOnTime,
	        Integer freeSlotCount, Integer wrongSlotCount, Integer totalSlotCount, Integer packetsRcvdCount,
	        Integer crcFailPacketCount, Integer timeSyncsLostDriftDetection, Integer timeSyncsLostTimeout,
	        Integer nodeLatency, Integer outOfOrderPackets, Integer duplicatePackets, Integer packetsFromNodes) {
		this();
		this.networkId = networkId;
		this.logicalId = logicalId;
		this.nwkTimestamp = nwkTimestamp;
		this.sendFailsFullQueue = sendFailsFullQueue;
		this.sendFailsNoPath = sendFailsNoPath;
		this.numberOneHopRetransmission = numberOneHopRetransmission;
		this.forwardingPacketsDropped = forwardingPacketsDropped;
		this.parentFailCount = parentFailCount;
		this.broadcastRetransmission = broadcastRetransmission;
		this.packetsFromBaseStation = packetsFromBaseStation;
		this.packetsToBaseStation = packetsToBaseStation;
		this.packetsForwarded = packetsForwarded;
		this.packetsSent = packetsSent;
		this.radioOnTime = radioOnTime;
		this.freeSlotCount = freeSlotCount;
		this.wrongSlotCount = wrongSlotCount;
		this.totalSlotCount = totalSlotCount;
		this.packetsRcvdCount = packetsRcvdCount;
		this.crcFailPacketCount = crcFailPacketCount;
		this.timeSyncsLostDriftDetection = timeSyncsLostDriftDetection;
		this.timeSyncsLostTimeout = timeSyncsLostTimeout;
		this.nodeLatency = nodeLatency;
		this.outOfOrderPackets = outOfOrderPackets;
		this.duplicatePackets = duplicatePackets;
		this.packetsFromNodes = packetsFromNodes;
	}

	/**
	 * Only the base part statistic constructor. Initializes one record of
	 * network base statistic.
	 * 
	 * @param networkId
	 * @param logicalId
	 * @param nwkTimestamp
	 * @param sendFailsFullQueue
	 * @param sendFailsNoPath
	 * @param numberOneHopRetransmission
	 * @param forwardingPacketsDropped
	 * @param parentFailCount
	 * @param broadcastRetransmission
	 * @param packetsFromBaseStation
	 * @param packetsToBaseStation
	 * @param packetsForwarded
	 * @param packetsSent
	 * @param radioOnTime
	 * @param freeSlotCount
	 * @param wrongSlotCount
	 * @param totalSlotCount
	 * @param packetsRcvdCount
	 * @param crcFailPacketCount
	 * @param timeSyncsLostDriftDetection
	 * @param timeSyncsLostTimeout
	 * @param nodeLatency
	 * @param outOfOrderPackets
	 * @param duplicatePackets
	 * @param packetsFromNodes
	 */
	public StatisticData(Integer networkId, Integer logicalId, Date nwkTimestamp, Integer packetsSent,
	        Integer freeSlotCount, Integer wrongSlotCount, Integer totalSlotCount, Integer packetsRcvdCount,
	        Integer crcFailPacketCount, Integer timeSyncsLostDriftDetection, Integer timeSyncsLostTimeout,
	        Integer outOfOrderPackets, Integer duplicatePackets, Integer packetsFromNodes) {
		this();
		this.networkId = networkId;
		this.logicalId = logicalId;
		this.nwkTimestamp = nwkTimestamp;
		this.packetsSent = packetsSent;
		this.freeSlotCount = freeSlotCount;
		this.wrongSlotCount = wrongSlotCount;
		this.totalSlotCount = totalSlotCount;
		this.packetsRcvdCount = packetsRcvdCount;
		this.crcFailPacketCount = crcFailPacketCount;
		this.timeSyncsLostDriftDetection = timeSyncsLostDriftDetection;
		this.timeSyncsLostTimeout = timeSyncsLostTimeout;
		this.outOfOrderPackets = outOfOrderPackets;
		this.duplicatePackets = duplicatePackets;
		this.packetsFromNodes = packetsFromNodes;
	}

	/**
	 * @return the networkId
	 */
	public Integer getNetworkId() {
		return networkId;
	}

	/**
	 * @param networkId
	 *            the networkId to set
	 */
	public void setNetworkId(Integer networkId) {
		this.networkId = networkId;
	}

	/**
	 * @return the logicalId
	 */
	public Integer getLogicalId() {
		return logicalId;
	}

	/**
	 * @param logicalId
	 *            the logicalId to set
	 */
	public void setLogicalId(Integer logicalId) {
		this.logicalId = logicalId;
	}

	/**
	 * @return the nwkTimestamp
	 */
	public Date getNwkTimestamp() {
		return nwkTimestamp;
	}

	/**
	 * @param nwkTimestamp
	 *            the nwkTimestamp to set
	 */
	public void setNwkTimestamp(Date nwkTimestamp) {
		this.nwkTimestamp = nwkTimestamp;
	}

	/**
	 * @return the sendFailsFullQueue
	 */
	public Integer getSendFailsFullQueue() {
		return sendFailsFullQueue;
	}

	/**
	 * @param sendFailsFullQueue
	 *            the sendFailsFullQueue to set
	 */
	public void setSendFailsFullQueue(Integer sendFailsFullQueue) {
		this.sendFailsFullQueue = sendFailsFullQueue;
	}

	/**
	 * @return the sendFailsNoPath
	 */
	public Integer getSendFailsNoPath() {
		return sendFailsNoPath;
	}

	/**
	 * @param sendFailsNoPath
	 *            the sendFailsNoPath to set
	 */
	public void setSendFailsNoPath(Integer sendFailsNoPath) {
		this.sendFailsNoPath = sendFailsNoPath;
	}

	/**
	 * @return the numberOneHopRetransmission
	 */
	public Integer getNumberOneHopRetransmission() {
		return numberOneHopRetransmission;
	}

	/**
	 * @param numberOneHopRetransmission
	 *            the numberOneHopRetransmission to set
	 */
	public void setNumberOneHopRetransmission(Integer numberOneHopRetransmission) {
		this.numberOneHopRetransmission = numberOneHopRetransmission;
	}

	/**
	 * @return the forwardingPacketsDropped
	 */
	public Integer getForwardingPacketsDropped() {
		return forwardingPacketsDropped;
	}

	/**
	 * @param forwardingPacketsDropped
	 *            the forwardingPacketsDropped to set
	 */
	public void setForwardingPacketsDropped(Integer forwardingPacketsDropped) {
		this.forwardingPacketsDropped = forwardingPacketsDropped;
	}

	/**
	 * @return the parentFailCount
	 */
	public Integer getParentFailCount() {
		return parentFailCount;
	}

	/**
	 * @param parentFailCount
	 *            the parentFailCount to set
	 */
	public void setParentFailCount(Integer parentFailCount) {
		this.parentFailCount = parentFailCount;
	}

	/**
	 * @return the broadcastRetransmission
	 */
	public Integer getBroadcastRetransmission() {
		return broadcastRetransmission;
	}

	/**
	 * @param broadcastRetransmission
	 *            the broadcastRetransmission to set
	 */
	public void setBroadcastRetransmission(Integer broadcastRetransmission) {
		this.broadcastRetransmission = broadcastRetransmission;
	}

	/**
	 * @return the packetsFromBaseStation
	 */
	public Integer getPacketsFromBaseStation() {
		return packetsFromBaseStation;
	}

	/**
	 * @param packetsFromBaseStation
	 *            the packetsFromBaseStation to set
	 */
	public void setPacketsFromBaseStation(Integer packetsFromBaseStation) {
		this.packetsFromBaseStation = packetsFromBaseStation;
	}

	/**
	 * @return the packetsToBaseStation
	 */
	public Integer getPacketsToBaseStation() {
		return packetsToBaseStation;
	}

	/**
	 * @param packetsToBaseStation
	 *            the packetsToBaseStation to set
	 */
	public void setPacketsToBaseStation(Integer packetsToBaseStation) {
		this.packetsToBaseStation = packetsToBaseStation;
	}

	/**
	 * @return the packetsForwarded
	 */
	public Integer getPacketsForwarded() {
		return packetsForwarded;
	}

	/**
	 * @param packetsForwarded
	 *            the packetsForwarded to set
	 */
	public void setPacketsForwarded(Integer packetsForwarded) {
		this.packetsForwarded = packetsForwarded;
	}

	/**
	 * @return the packetsSent
	 */
	public Integer getPacketsSent() {
		return packetsSent;
	}

	/**
	 * @param packetsSent
	 *            the packetsSent to set
	 */
	public void setPacketsSent(Integer packetsSent) {
		this.packetsSent = packetsSent;
	}

	/**
	 * @return the radioOnTime
	 */
	public Integer getRadioOnTime() {
		return radioOnTime;
	}

	/**
	 * @param radioOnTime
	 *            the radioOnTime to set
	 */
	public void setRadioOnTime(Integer radioOnTime) {
		this.radioOnTime = radioOnTime;
	}

	/**
	 * @return the freeSlotCount
	 */
	public Integer getFreeSlotCount() {
		return freeSlotCount;
	}

	/**
	 * @param freeSlotCount
	 *            the freeSlotCount to set
	 */
	public void setFreeSlotCount(Integer freeSlotCount) {
		this.freeSlotCount = freeSlotCount;
	}

	/**
	 * @return the wrongSlotCount
	 */
	public Integer getWrongSlotCount() {
		return wrongSlotCount;
	}

	/**
	 * @param wrongSlotCount
	 *            the wrongSlotCount to set
	 */
	public void setWrongSlotCount(Integer wrongSlotCount) {
		this.wrongSlotCount = wrongSlotCount;
	}

	/**
	 * @return the totalSlotCount
	 */
	public Integer getTotalSlotCount() {
		return totalSlotCount;
	}

	/**
	 * @param totalSlotCount
	 *            the totalSlotCount to set
	 */
	public void setTotalSlotCount(Integer totalSlotCount) {
		this.totalSlotCount = totalSlotCount;
	}

	/**
	 * @return the packetsRcvdCount
	 */
	public Integer getPacketsRcvdCount() {
		return packetsRcvdCount;
	}

	/**
	 * @param packetsRcvdCount
	 *            the packetsRcvdCount to set
	 */
	public void setPacketsRcvdCount(Integer packetsRcvdCount) {
		this.packetsRcvdCount = packetsRcvdCount;
	}

	/**
	 * @return the crcFailPacketCount
	 */
	public Integer getCrcFailPacketCount() {
		return crcFailPacketCount;
	}

	/**
	 * @param crcFailPacketCount
	 *            the crcFailPacketCount to set
	 */
	public void setCrcFailPacketCount(Integer crcFailPacketCount) {
		this.crcFailPacketCount = crcFailPacketCount;
	}

	/**
	 * @return the timeSyncsLostDriftDetection
	 */
	public Integer getTimeSyncsLostDriftDetection() {
		return timeSyncsLostDriftDetection;
	}

	/**
	 * @param timeSyncsLostDriftDetection
	 *            the timeSyncsLostDriftDetection to set
	 */
	public void setTimeSyncsLostDriftDetection(Integer timeSyncsLostDriftDetection) {
		this.timeSyncsLostDriftDetection = timeSyncsLostDriftDetection;
	}

	/**
	 * @return the timeSyncsLostTimeout
	 */
	public Integer getTimeSyncsLostTimeout() {
		return timeSyncsLostTimeout;
	}

	/**
	 * @param timeSyncsLostTimeout
	 *            the timeSyncsLostTimeout to set
	 */
	public void setTimeSyncsLostTimeout(Integer timeSyncsLostTimeout) {
		this.timeSyncsLostTimeout = timeSyncsLostTimeout;
	}

	/**
	 * @return the nodeLatency
	 */
	public Integer getNodeLatency() {
		return nodeLatency;
	}

	/**
	 * @param nodeLatency
	 *            the nodeLatency to set
	 */
	public void setNodeLatency(Integer nodeLatency) {
		this.nodeLatency = nodeLatency;
	}

	public Integer getOutOfOrderPackets() {
		return outOfOrderPackets;
	}

	public void setOutOfOrderPackets(Integer outOfOrderPackets) {
		this.outOfOrderPackets = outOfOrderPackets;
	}

	public Integer getDuplicatePackets() {
		return duplicatePackets;
	}

	public void setDuplicatePackets(Integer duplicatePackets) {
		this.duplicatePackets = duplicatePackets;
	}

	public Integer getPacketsFromNodes() {
		return packetsFromNodes;
	}

	public void setPacketsFromNodes(Integer packetsFromNodes) {
		this.packetsFromNodes = packetsFromNodes;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((broadcastRetransmission == null) ? 0 : broadcastRetransmission.hashCode());
		result = prime * result + Arrays.hashCode(chStatus);
		result = prime * result + ((crcFailPacketCount == null) ? 0 : crcFailPacketCount.hashCode());
		result = prime * result + ((duplicatePackets == null) ? 0 : duplicatePackets.hashCode());
		result = prime * result + ((forwardingPacketsDropped == null) ? 0 : forwardingPacketsDropped.hashCode());
		result = prime * result + ((freeSlotCount == null) ? 0 : freeSlotCount.hashCode());
		result = prime * result + ((hopCount == null) ? 0 : hopCount.hashCode());
		result = prime * result + ((logicalId == null) ? 0 : logicalId.hashCode());
		result = prime * result + ((networkId == null) ? 0 : networkId.hashCode());
		result = prime * result + ((nodeLatency == null) ? 0 : nodeLatency.hashCode());
		result = prime * result + ((noiseThresh == null) ? 0 : noiseThresh.hashCode());
		result = prime * result + ((numberOneHopRetransmission == null) ? 0 : numberOneHopRetransmission.hashCode());
		result = prime * result + ((nwkTimestamp == null) ? 0 : nwkTimestamp.hashCode());
		result = prime * result + ((outOfOrderPackets == null) ? 0 : outOfOrderPackets.hashCode());
		result = prime * result + ((packetsForwarded == null) ? 0 : packetsForwarded.hashCode());
		result = prime * result + ((packetsFromBaseStation == null) ? 0 : packetsFromBaseStation.hashCode());
		result = prime * result + ((packetsFromNodes == null) ? 0 : packetsFromNodes.hashCode());
		result = prime * result + ((packetsRcvdCount == null) ? 0 : packetsRcvdCount.hashCode());
		result = prime * result + ((packetsSent == null) ? 0 : packetsSent.hashCode());
		result = prime * result + ((packetsToBaseStation == null) ? 0 : packetsToBaseStation.hashCode());
		result = prime * result + ((parentFailCount == null) ? 0 : parentFailCount.hashCode());
		result = prime * result + ((parentState == null) ? 0 : parentState.hashCode());
		result = prime * result + ((radioOnTime == null) ? 0 : radioOnTime.hashCode());
		result = prime * result + ((routerState == null) ? 0 : routerState.hashCode());
		result = prime * result + ((sendFailsFullQueue == null) ? 0 : sendFailsFullQueue.hashCode());
		result = prime * result + ((sendFailsNoPath == null) ? 0 : sendFailsNoPath.hashCode());
		result = prime * result + ((timeSyncsLostDriftDetection == null) ? 0 : timeSyncsLostDriftDetection.hashCode());
		result = prime * result + ((timeSyncsLostTimeout == null) ? 0 : timeSyncsLostTimeout.hashCode());
		result = prime * result + ((totalSlotCount == null) ? 0 : totalSlotCount.hashCode());
		result = prime * result + ((wrongSlotCount == null) ? 0 : wrongSlotCount.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final StatisticData other = (StatisticData) obj;
		if (broadcastRetransmission == null) {
			if (other.broadcastRetransmission != null)
				return false;
		} else if (!broadcastRetransmission.equals(other.broadcastRetransmission))
			return false;
		if (!Arrays.equals(chStatus, other.chStatus))
			return false;
		if (crcFailPacketCount == null) {
			if (other.crcFailPacketCount != null)
				return false;
		} else if (!crcFailPacketCount.equals(other.crcFailPacketCount))
			return false;
		if (duplicatePackets == null) {
			if (other.duplicatePackets != null)
				return false;
		} else if (!duplicatePackets.equals(other.duplicatePackets))
			return false;
		if (forwardingPacketsDropped == null) {
			if (other.forwardingPacketsDropped != null)
				return false;
		} else if (!forwardingPacketsDropped.equals(other.forwardingPacketsDropped))
			return false;
		if (freeSlotCount == null) {
			if (other.freeSlotCount != null)
				return false;
		} else if (!freeSlotCount.equals(other.freeSlotCount))
			return false;
		if (hopCount == null) {
			if (other.hopCount != null)
				return false;
		} else if (!hopCount.equals(other.hopCount))
			return false;
		if (logicalId == null) {
			if (other.logicalId != null)
				return false;
		} else if (!logicalId.equals(other.logicalId))
			return false;
		if (networkId == null) {
			if (other.networkId != null)
				return false;
		} else if (!networkId.equals(other.networkId))
			return false;
		if (nodeLatency == null) {
			if (other.nodeLatency != null)
				return false;
		} else if (!nodeLatency.equals(other.nodeLatency))
			return false;
		if (noiseThresh == null) {
			if (other.noiseThresh != null)
				return false;
		} else if (!noiseThresh.equals(other.noiseThresh))
			return false;
		if (numberOneHopRetransmission == null) {
			if (other.numberOneHopRetransmission != null)
				return false;
		} else if (!numberOneHopRetransmission.equals(other.numberOneHopRetransmission))
			return false;
		if (nwkTimestamp == null) {
			if (other.nwkTimestamp != null)
				return false;
		} else if (!nwkTimestamp.equals(other.nwkTimestamp))
			return false;
		if (outOfOrderPackets == null) {
			if (other.outOfOrderPackets != null)
				return false;
		} else if (!outOfOrderPackets.equals(other.outOfOrderPackets))
			return false;
		if (packetsForwarded == null) {
			if (other.packetsForwarded != null)
				return false;
		} else if (!packetsForwarded.equals(other.packetsForwarded))
			return false;
		if (packetsFromBaseStation == null) {
			if (other.packetsFromBaseStation != null)
				return false;
		} else if (!packetsFromBaseStation.equals(other.packetsFromBaseStation))
			return false;
		if (packetsFromNodes == null) {
			if (other.packetsFromNodes != null)
				return false;
		} else if (!packetsFromNodes.equals(other.packetsFromNodes))
			return false;
		if (packetsRcvdCount == null) {
			if (other.packetsRcvdCount != null)
				return false;
		} else if (!packetsRcvdCount.equals(other.packetsRcvdCount))
			return false;
		if (packetsSent == null) {
			if (other.packetsSent != null)
				return false;
		} else if (!packetsSent.equals(other.packetsSent))
			return false;
		if (packetsToBaseStation == null) {
			if (other.packetsToBaseStation != null)
				return false;
		} else if (!packetsToBaseStation.equals(other.packetsToBaseStation))
			return false;
		if (parentFailCount == null) {
			if (other.parentFailCount != null)
				return false;
		} else if (!parentFailCount.equals(other.parentFailCount))
			return false;
		if (parentState == null) {
			if (other.parentState != null)
				return false;
		} else if (!parentState.equals(other.parentState))
			return false;
		if (radioOnTime == null) {
			if (other.radioOnTime != null)
				return false;
		} else if (!radioOnTime.equals(other.radioOnTime))
			return false;
		if (routerState == null) {
			if (other.routerState != null)
				return false;
		} else if (!routerState.equals(other.routerState))
			return false;
		if (sendFailsFullQueue == null) {
			if (other.sendFailsFullQueue != null)
				return false;
		} else if (!sendFailsFullQueue.equals(other.sendFailsFullQueue))
			return false;
		if (sendFailsNoPath == null) {
			if (other.sendFailsNoPath != null)
				return false;
		} else if (!sendFailsNoPath.equals(other.sendFailsNoPath))
			return false;
		if (timeSyncsLostDriftDetection == null) {
			if (other.timeSyncsLostDriftDetection != null)
				return false;
		} else if (!timeSyncsLostDriftDetection.equals(other.timeSyncsLostDriftDetection))
			return false;
		if (timeSyncsLostTimeout == null) {
			if (other.timeSyncsLostTimeout != null)
				return false;
		} else if (!timeSyncsLostTimeout.equals(other.timeSyncsLostTimeout))
			return false;
		if (totalSlotCount == null) {
			if (other.totalSlotCount != null)
				return false;
		} else if (!totalSlotCount.equals(other.totalSlotCount))
			return false;
		if (wrongSlotCount == null) {
			if (other.wrongSlotCount != null)
				return false;
		} else if (!wrongSlotCount.equals(other.wrongSlotCount))
			return false;
		return true;
	}

}
