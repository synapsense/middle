package com.synapsense.service.impl.activitylog;

import java.io.Serializable;
import java.util.Date;
import com.synapsense.dto.TO;

public class ActivityRecord implements Serializable {

	private Integer id;
	private String action;
	private String module;
	private String description;
	private String userName;
	private Date time;
	private TO<?> object;

	private static final long serialVersionUID = -4659295022049413972L;

	public ActivityRecord(Integer id, String userName, String module, String action, String description, Date time,
	        TO<?> object) {
		super();
		this.id = id;
		this.action = action;
		this.module = module;
		this.description = description;
		this.userName = userName;
		this.time = time;
		this.object = object;
	}

	public ActivityRecord(String userName, String module, String action, String description) {
		this(null, userName, module, action, description, new Date(), null);
	}

	public String getAction() {
		return action;
	}

	public String getModule() {
		return module;
	}

	public String getDescription() {
		return description;
	}

	public String getUserName() {
		return userName;
	}

	public Date getTime() {
		return time;
	}

	public Integer getId() {
		return id;
	}

	public TO<?> getObject() {
		return object;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public void setModule(String module) {
		this.module = module;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public void setObject(TO<?> object) {
		this.object = object;
	}

	@Override
    public String toString() {
	    return "ActivityRecord [id=" + id + ", action=" + action + ", module=" + module + ", description="
	            + description + ", userName=" + userName + ", time=" + time + ", object=" + object + "]";
    }

}
