package com.synapsense.service.nsvc.filters;

import java.io.Serializable;

import com.synapsense.service.nsvc.events.ObjectDeletedEvent;
import com.synapsense.service.nsvc.events.ObjectDeletedEventProcessor;

/**
 * ObjectDeletedEvent filter that passes events with an object TO matching the
 * specified TOMatcher.
 * 
 * @author Oleg Stepanov
 * 
 */
public class ObjectDeletedEventFilter implements ObjectDeletedEventProcessor, Serializable {

	private static final long serialVersionUID = -4004858607677649671L;

	private final TOMatcher matcher;

	/**
	 * Create filter from TOMatcher.
	 * 
	 * @param matcher
	 *            Matcher to use.
	 * @throws IllegalArgumentException
	 *             if matcher is <code>null</code>.
	 */
	public ObjectDeletedEventFilter(TOMatcher matcher) {
		if (matcher == null)
			throw new IllegalArgumentException("matcher must not be null");
		this.matcher = matcher;
	}

	@Override
	public ObjectDeletedEvent process(ObjectDeletedEvent e) {
		return matcher.match(e.getObjectTO()) ? e : null;
	}

}
