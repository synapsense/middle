package com.synapsense.service.reporting.exception;

import javax.ejb.ApplicationException;

import com.synapsense.service.reporting.management.ReportingException;


@ApplicationException(rollback = false)
public class SchedulingReportingException extends ReportingException {

	private static final long serialVersionUID = -63583250795746386L;

	public SchedulingReportingException() {
	}

	public SchedulingReportingException(String message, Throwable cause) {
		super(message, cause);
	}

	public SchedulingReportingException(String message) {
		super(message);
	}

	public SchedulingReportingException(Throwable cause) {
		super(cause);
	}
}
