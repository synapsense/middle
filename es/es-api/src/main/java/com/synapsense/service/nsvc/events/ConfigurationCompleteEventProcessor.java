package com.synapsense.service.nsvc.events;

public interface ConfigurationCompleteEventProcessor {

	ConfigurationCompleteEvent process(ConfigurationCompleteEvent e);
}
