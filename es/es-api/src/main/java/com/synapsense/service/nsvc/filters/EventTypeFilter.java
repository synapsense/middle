package com.synapsense.service.nsvc.filters;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;

import com.synapsense.service.nsvc.Event;
import com.synapsense.service.nsvc.EventProcessor;

public class EventTypeFilter implements EventProcessor, Serializable {

	private static final long serialVersionUID = 8586398609318782209L;

	private HashSet<Class<? extends Event>> eventTypes = new HashSet<Class<? extends Event>>();

	public EventTypeFilter(Class<? extends Event> eventType) {
		if (eventType == null) {
			throw new IllegalArgumentException("Event type shouldn't be null");
		}
		eventTypes.add(eventType);
	}

	public EventTypeFilter(Collection<Class<? extends Event>> eventTypes) {
		if (eventTypes == null) {
			throw new IllegalArgumentException("Event types shouldn't be null");
		}
		this.eventTypes.addAll(eventTypes);
	}

	public EventTypeFilter() {
	}

	public EventTypeFilter addEventType(Class<? extends Event> eventType) {
		if (eventType == null) {
			throw new IllegalArgumentException("Event type shouldn't be null");
		}
		eventTypes.add(eventType);
		return this;
	}

	@Override
	public Event process(Event e) {
		if (e == null) {
			return null;
		}
		return eventTypes.contains(e.getClass()) ? e : null;
	}
}
