package com.synapsense.service.impl.messages;

import java.io.Serializable;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * That message is used to transfer data from plug-ins to ES
 * 
 * @author anechaev
 * 
 */
public final class SensedMessage implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6074360300764004805L;

	private final String objectID;
	private final String propertyName;
	private final Serializable payload;
	private final boolean ordered;

	public SensedMessage(String objectID, String propertyName, Serializable payload, boolean ordered) {
		if (objectID==null) { 
			throw new IllegalArgumentException("Supplied 'objectId' is null");
		}
		if (objectID.isEmpty()) { 
			throw new IllegalArgumentException("Supplied 'objectId' is empty");
		}
		this.objectID = objectID;
		
		if (propertyName==null) { 
			throw new IllegalArgumentException("Supplied 'propertyName' is null");
		}
		if (propertyName.isEmpty()) { 
			throw new IllegalArgumentException("Supplied 'propertyName' is empty");
		}
		this.propertyName = propertyName;
		
		this.payload = payload;
		this.ordered = ordered;
	}

	public String getObjectID() {
		return objectID;
	}

	public String getPropertyName() {
		return propertyName;
	}

	public Serializable getPayload() {
		return payload;
	}

	public boolean isOrdered() {
		return ordered;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
				.append("objectID", objectID)
				.append("propertyName", propertyName)
				.append("payload", payload)
				.append("ordered", ordered)
				.toString();
	}
}
