package com.synapsense.service.reporting.datasource;

/**
 * Created by Grigory Ryabov on 27.01.14.
 */
public enum RelatedObjectType {
    DESCENDANT("Descendant"),
    ANCESTOR("Ancestor"),
    DIMENSION("Dimension");

    private String name;

    RelatedObjectType(String name) {
        this.name = name;
    }

    public static RelatedObjectType getTypeByName(String name) {
        for (RelatedObjectType type : RelatedObjectType.values()) {
            if (type.getName().equals(name))
                return type;
        }
        throw new IllegalArgumentException("Not supported Related Object Type" + name);
    }

    public String getName() {
        return name;
    }
}
