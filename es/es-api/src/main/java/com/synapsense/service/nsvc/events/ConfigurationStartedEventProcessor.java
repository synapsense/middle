package com.synapsense.service.nsvc.events;

public interface ConfigurationStartedEventProcessor {

	ConfigurationStartedEvent process(ConfigurationStartedEvent e);
}
