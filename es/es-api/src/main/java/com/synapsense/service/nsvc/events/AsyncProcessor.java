package com.synapsense.service.nsvc.events;


import com.synapsense.service.nsvc.Event;
import com.synapsense.service.nsvc.EventProcessor;
import java.util.concurrent.Executor;
import org.apache.log4j.Logger;

/** Decorator that invokes an EventProcessor in the supplied Executor */
public class AsyncProcessor implements EventProcessor {

	// The EventProcessor that we delegate to
	private final EventProcessor delegate;

	// The Executor in which events are processes
	private final Executor executor;

	private static final Logger log = Logger.getLogger(AsyncProcessor.class);

	public AsyncProcessor(EventProcessor delegate, Executor executor) {
		this.delegate = delegate;
		this.executor = executor;
	}

	@Override
	public Event process(final Event event) {
		// invoke processAsynchronously() in the executor
		executor.execute(() -> processAsynchronously(event));
		return null;
	}

	private void processAsynchronously(Event event) {
		try {
			delegate.process(event);
		} catch (Exception e) {
			log.error("Failed to process event " + event, e);
		}
	}
}
