package com.synapsense.service.user;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import com.synapsense.dto.TO;
import com.synapsense.service.UserManagementService;
import com.synapsense.util.CollectionUtils;
import com.synapsense.util.CommonUtils;
import com.synapsense.util.LocalizationUtils;

public class EnvUserContext implements UserContext {
	private final UserManagementService userService;
	private final String userName;
	private final TO<?> user;

	public EnvUserContext(final String userName,
			final UserManagementService userService) {
		this.userService = userService;
		this.userName = userName;
		this.user = userService.getUser(userName);
	}

	@Override
	public String getUnitSystem() {
		String targetSystem = findUserPreference("UnitsSystem", user, userService);
		// console's user preferences are quoted strings
		return !CommonUtils.isStringEmpty(targetSystem) ? targetSystem
				: new NonUserContext().getUnitSystem();
	}

	@Override
	public NumberFormat getDataFormat() {
		String pref = findUserPreference("DecimalFormat", user, userService);
		NumberFormat nf = new NonUserContext().getDataFormat();
		if (!CommonUtils.isStringEmpty(pref)) {
			try {
				Integer pres = Integer.parseInt(pref);
				nf.setMaximumFractionDigits(pres);
				nf.setMinimumFractionDigits(pres);
			} catch (NumberFormatException e) {
				// well, we'll just use default in this case
			}
		}

		pref = findUserPreference("DecimalSeparator", user, userService);

		if (!CommonUtils.isStringEmpty(pref) && pref.length() == 1) {
			DecimalFormat df = (DecimalFormat) nf;
			DecimalFormatSymbols symbols = df.getDecimalFormatSymbols();
			symbols.setDecimalSeparator(pref.charAt(0));
			df.setDecimalFormatSymbols(symbols);
		}
		return nf;
	}

	@Override
	public DateFormat getDateFormat() {
		SimpleDateFormat dateFormat;

		String dateFormatString = getDateFormatString();
		if (dateFormatString == null) {
			dateFormat = (SimpleDateFormat) new NonUserContext() .getDateFormat();
		} else {
			dateFormat = new SimpleDateFormat(dateFormatString);
		}

		String timeZoneString = getTimeZoneString();
		if (timeZoneString != null) {
			TimeZone tz = TimeZone.getTimeZone(timeZoneString);
			dateFormat.setTimeZone(tz);
		}
		return dateFormat;
	}

	private String getDateFormatString() {
		String dateFormat = findUserPreference("DateFormat", user, userService);
		if (!CommonUtils.isStringEmpty(dateFormat)) {
			dateFormat += " z";// append time zone
			return dateFormat;
		}
		return null;
	}

	private String getTimeZoneString() {
		return findUserPreference("TimeZone", user, userService);
	}

	private static String findUserPreference(final String prefName,
			final TO<?> user, final UserManagementService userService) {
		final Map<String, Object> preferences = userService.getUserPreferences(user);
		String value = (String) preferences.get(prefName);
		// console's user preferences are quoted strings
		return !CommonUtils.isStringEmpty(value) ? unquoteString(value) : null;
	}

	private static String unquoteString(String str) {
		if (str != null && str.length() >= 2 && str.startsWith("\"")
				&& str.endsWith("\"")) {
			return str.substring(1, str.length() - 1);
		}
		return str;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((userName == null) ? 0 : userName.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final EnvUserContext other = (EnvUserContext) obj;
		if (userName == null) {
			if (other.userName != null)
				return false;
		} else if (!userName.equals(other.userName))
			return false;
		return true;
	}

	@Override
	public Locale getLocale() {
		String langPref = findUserPreference("SavedLanguage", user, userService);
		if (!CommonUtils.isStringEmpty(langPref)) {
			return LocalizationUtils.getLocale(langPref);
		}
		return new NonUserContext().getLocale();
	}
}