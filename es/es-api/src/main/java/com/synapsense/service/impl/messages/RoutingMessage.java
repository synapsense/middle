/**
 * 
 */
package com.synapsense.service.impl.messages;

import com.synapsense.service.impl.messages.base.RoutingData;

/**
 * @author Alexander Borisov
 * 
 */
public class RoutingMessage implements BusMessage<RoutingData, Integer> {
	private static final long serialVersionUID = 8869210022237607279L;

	private int id;
	private RoutingData data;

	public RoutingMessage(int id, RoutingData data) {
		super();
		this.id = id;
		this.data = data;
	}

	@Override
	public Integer getId() {
		return id;
	}

	@Override
	public RoutingData getValue() {
		return data;
	}

	@Override
	public void setId(Integer id) {
		this.id = id;
	}

	@Override
	public void setValue(RoutingData value) {
		data = value;
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer(20);
		sb.append("Id=");
		sb.append(id);
		sb.append("\n");

		sb.append("message=");
		sb.append(data);
		sb.append("\n");

		return sb.toString();
	}

	@Override
	public Type getType() {
		return Type.ROUTING;
	}

}
