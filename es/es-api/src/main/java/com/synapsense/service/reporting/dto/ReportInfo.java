package com.synapsense.service.reporting.dto;

import java.io.Serializable;
import java.util.*;

import com.synapsense.dto.TO;
import com.synapsense.util.CollectionUtils;

public class ReportInfo implements Serializable {

	private static final long serialVersionUID = -1741033545507963994L;

    private int reportId;
    private int reportTaskId;

	private String taskName;

	private long timestamp;

	private Map<String, String> exported =  CollectionUtils.newMap();

    private String user;

    public ReportInfo(String taskName, long time) {
        this.taskName = taskName;
        this.timestamp = time;
	}

	public void addExportedPath(String format, String path) {
		exported.put(format, path);
	}

	public String getExportedPath(String format) {
		return exported.get(format);
	}

	public HashMap<String, String> getExportedMap() {
		return new HashMap<String, String>(exported);
	}

	public int getReportId() {
		return reportId;
	}

	public void setReportId(int id) {
		this.reportId = id;
	}
	
	public int getReportTaskId() {
		return reportTaskId;
	}
	
	public void setReportTaskId(int reportTaskId) {
		this.reportTaskId = reportTaskId;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	@Override
	public String toString() {
		return "Report ID=" + reportId + ",  generated at " + new Date(timestamp);
	}

    public String getTaskName() {
        return taskName;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }
}
