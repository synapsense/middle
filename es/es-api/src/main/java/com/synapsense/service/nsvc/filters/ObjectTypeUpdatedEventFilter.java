package com.synapsense.service.nsvc.filters;

import java.io.Serializable;
import java.util.regex.Pattern;

import com.synapsense.service.nsvc.events.ObjectTypeUpdatedEvent;
import com.synapsense.service.nsvc.events.ObjectTypeUpdatedEventProcessor;

public class ObjectTypeUpdatedEventFilter implements ObjectTypeUpdatedEventProcessor, Serializable {

	private static final long serialVersionUID = 2700614756373965519L;

	private final Pattern typeNamePattern;

	public ObjectTypeUpdatedEventFilter(String typeNamePattern) {
		this.typeNamePattern = Pattern.compile(typeNamePattern);
	}

	@Override
	public ObjectTypeUpdatedEvent process(ObjectTypeUpdatedEvent e) {
		return typeNamePattern.matcher(e.getTypeName()).matches() ? e : null;
	}

}
