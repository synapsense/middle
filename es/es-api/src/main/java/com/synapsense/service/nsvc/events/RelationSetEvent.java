package com.synapsense.service.nsvc.events;

import com.synapsense.dto.TO;
import com.synapsense.service.nsvc.Event;
import com.synapsense.service.nsvc.EventDispatcher;

public class RelationSetEvent implements Event {

	private static final long serialVersionUID = -1506625894381202413L;

	private final TO<?> parentTO;
	private final TO<?> childTO;

	public RelationSetEvent(TO<?> parentTO, TO<?> childTO) {
		this.parentTO = parentTO;
		this.childTO = childTO;
	}

	public TO<?> getParentTO() {
		return parentTO;
	}

	public TO<?> getChildTO() {
		return childTO;
	}

	@Override
	public Event dispatch(EventDispatcher ed) {
		return ed.dispatch(this);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(super.toString()).append(": parentTO=").append(parentTO).append(", childTO=").append(childTO);
		return sb.toString();
	}

}
