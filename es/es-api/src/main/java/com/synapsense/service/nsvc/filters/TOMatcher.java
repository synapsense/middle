package com.synapsense.service.nsvc.filters;

import java.io.Serializable;
import java.util.regex.Pattern;

import com.synapsense.dto.TO;

/**
 * Utility class for matching object's id against specified id or type name
 * regex.
 * 
 * @author Oleg Stepanov
 */
public class TOMatcher implements Serializable {

	private static final long serialVersionUID = -4866663851603272848L;

	private final Pattern typeNamePattern;
	private final TO<?> objId;

	/**
	 * Constructs regex matcher from a string regex.
	 * 
	 * @param typeNamePattern
	 *            regular expression for matching TO's type name against. Cannot
	 *            be <code>null</code> (for setting the <code>null</code> value
	 *            use any of the other two constructors).
	 */
	public TOMatcher(String typeNamePattern) {
		this(Pattern.compile(typeNamePattern));
	}

	/**
	 * Constructs regex matcher from a compiled regex.
	 * 
	 * @param typeNamePattern
	 *            compiled regular expression for matching TO's type name
	 *            against.
	 */
	public TOMatcher(Pattern typeNamePattern) {
		this.typeNamePattern = typeNamePattern;
		objId = null;
	}

	/**
	 * Constructs matcher from a predefined TO.
	 * 
	 * @param objId
	 *            Object's TO to match.
	 */
	public TOMatcher(TO<?> objId) {
		this.typeNamePattern = null;
		this.objId = objId;
	}

	/**
	 * Matches objId.getTypeName() against either the specified type name regex
	 * or the whole TO depending on the way this TOMatcher was created.
	 * 
	 * @param objId
	 *            Object's TO to match. Can be <code>null</code>; in this case
	 *            the result is true only if the original regex or TO was also
	 *            set to <code>null</code>.
	 * @return match result
	 */
	public boolean match(TO<?> objId) {
		if (objId == null && typeNamePattern == null && this.objId == null) {
			return true;
		}
		return typeNamePattern == null ? objId.equals(this.objId) : typeNamePattern.matcher(objId.getTypeName())
		        .matches();
	}

}
