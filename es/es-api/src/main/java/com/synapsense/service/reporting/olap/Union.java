package com.synapsense.service.reporting.olap;

import java.io.Serializable;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;

/**
 * Represents a union of other sets.<br>
 * MDX-like Examples:<br>
 * [Object].[Rack].members + [Object].[CRAC].[&Crac1] defines a set of all racks
 * and one crac "Crac1"
 * 
 * @author Dmitry Grudzinskiy
 * 
 */
public class Union implements CompositeAxisSet, Serializable {

    private static final long serialVersionUID = -8107546239581355639L;

	private LinkedHashSet<AxisSet> sets = new LinkedHashSet<>();

	private LinkedList<Hierarchy> dims = new LinkedList<>();

	public Union(AxisSet... sets) {
		dims.addAll(sets[0].getDimensionalities());
		for (int i = 1; i < sets.length; i++) {
			add(sets[i]);
		}
	}

	public Union() {
	}
	
	public Union add(AxisSet set) {
		if (!dims.isEmpty() && !dims.equals(set.getDimensionalities())) {
			throw new IllegalArgumentException("Incompatible dimensionalities: " + dims + " and "
					+ set.getDimensionalities());
		}
		sets.add(set);
		return this;
	}

	public List<Hierarchy> getDimensionalities() {
		return Collections.unmodifiableList(dims);
	}

	public List<Tuple> getTuples() {
		LinkedList<Tuple> tuples = new LinkedList<Tuple>();
		for (AxisSet set : sets) {
			tuples.addAll(set.getTuples());
		}
		return tuples;
	}

	@Override
	public void buildQuery(PlanProcessor planProcessor) {
		planProcessor.processSet(this);
	}
}
