package com.synapsense.service.impl.messages;

import java.util.ArrayList;

import com.synapsense.service.impl.messages.base.PlugConfData;

public class PlugConfMessage implements BusMessage<ArrayList<PlugConfData>, Integer> {

	private static final long serialVersionUID = -5285535244502590065L;
	private int id;
	private ArrayList<PlugConfData> value;

	@Override
	public Integer getId() {
		return id;
	}

	@Override
	public void setId(Integer id) {
		this.id = id;

	}

	@Override
	public ArrayList<PlugConfData> getValue() {
		return value;
	}

	@Override
	public void setValue(ArrayList<PlugConfData> value) {
		this.value = value;

	}

	@Override
	public Type getType() {
		return Type.POWER_RACK_CONF;
	}

}
