package com.synapsense.service.reporting.management;

import com.synapsense.exception.ObjectExistsException;
import com.synapsense.service.reporting.dto.*;

import java.util.Collection;

/**
 * The <code>ReportingManagementService</code> provides API for working with reporting objects.
 */
public interface ReportingManagementService {

	/**
	 * Adds the report template.
	 * 
	 * @param reportTemplateDesign
	 *            the report template design
	 * @throws CompilationReportingException
	 *             thrown if template was unable to compile
	 * @throws ObjectExistsException
	 * 				thrown if template with specified name already exists 
	 */
	void addReportTemplate(String reportTemplateDesign) throws ReportingException, ObjectExistsException;

	/**
	 * Adds the report template.
	 * 
	 * @param reportTemplateDesign
	 *            the report template design
	 * @param system
	 *            the system
	 * @throws CompilationReportingException
	 *             thrown if template was unable to compile
	 * @throws ObjectExistsException
	 * 				thrown if template with specified name already exists 
	 */
	//void addReportTemplate(String reportTemplateDesign, boolean system) throws ReportingException, ObjectExistsException;

	/**
	 * Updates report template.
	 * 
	 * @param reportTemplateName
	 *            the report template name
	 * @param reportTemplateDesign
	 *            the report template design
	 * @throws CompilationReportingException
	 *             if template was unable to compile
	 * @throws ObjectNotFoundDAOReportingException
	 * 				thrown if template with specified name not found
	 * @throws ObjectExistsException
	 * 				thrown if template with specified name already exists 
	 */
	//void updateReportTemplate(String reportTemplateName, String reportTemplateDesign) throws ReportingException;

	/**
	 * Removes the report template.
	 * 
	 * @param reportTemplateName
	 *            the report template name
	 * @throws ObjectNotFoundDAOReportingException
	 * 				thrown if template with specified name not found
	 * @throws SchedulingReportingException
	 * 				thrown if it was unable to remove scheduled template tasks 
	 */
	void removeReportTemplate(String reportTemplateName) throws ReportingException;

	/**
	 * Gets the available report templates.
	 * 
	 * @return the available report templates
	 */
	Collection<String> getAvailableReportTemplates() ;

	/**
	 * Gets the report template info.
	 * 
	 * @param reportTemplateName
	 *            the report template name
	 * @return the report template info
	 * @throws ObjectNotFoundDAOReportingException
	 * 				thrown if template with specified name not found
	 */
	ReportTemplateInfo getReportTemplateInfo(String reportTemplateName) throws ReportingException;

	/**
	 * Gets the report template design.
	 *
	 * @return the report template design
	 * @throws ReportingException the reporting exception
	 */
	//String getReportTemplateDesign(String reportTemplateName) throws ReportingException;

	/**
	 * Gets the generated reports.
	 * 
	 * @return the generated reports
	 * @throws ObjectNotFoundDAOReportingException
	 * 				thrown if template with specified name not found or if template task not found by name
	 */
	Collection<ReportTitle> getGeneratedReports() throws ReportingException;

	/**
	 * Gets the report info.
	 * 
	 * @param reportId
	 *            the report id
	 * @return the report info
	 * @throws ObjectNotFoundDAOReportingException
	 * 				thrown if report with specified id not found
	 */
	ReportInfo getReportInfo(int reportId) throws ReportingException;

	/**
	 * Removes the report.
	 * 
	 * @param reportId
	 *            the report id
	 * @throws ObjectNotFoundDAOReportingException
	 * 				thrown if report with specified id not found or corresponding report task, report template not found 
	 */
	void removeReport(int reportId) throws ReportingException;

	/**
	 * Run complex report task.
	 * 
	 * @param taskInfo
	 *            the task info
	 * @return the remote command proxy
	 * @throws ObjectNotFoundDAOReportingException
	 * 				thrown if corresponding report template not found 
	 * @throws ObjectExistsException
	 * 				thrown if template task with specified name already exists 
	 */
	RemoteCommandProxy runComplexReportTask(ReportTaskInfo taskInfo) throws ReportingException, ObjectExistsException;

	/**
	 * Export report.
	 * 
	 * @param reportId
	 *            the report id
	 * @param format
	 *            the format
	 * @return the remote command proxy
	 * @throws ReportingException
	 *             thrown if report task contains no commands
	 * @throws ObjectNotFoundDAOReportingException
	 * 				thrown if report with specified id or corresponding report task not found 
	 */
	//RemoteCommandProxy exportReport(int reportId, String format) throws ReportingException;

	/**
	 * Email reports.
	 * 
	 * @param reportId
	 *            the report id
	 * @param destinations
	 *            the destinations
	 * @return the remote command proxy
	 * @throws ReportingException
	 *             thrown if report task contains no commands
	 * @throws ObjectNotFoundDAOReportingException
	 * 				thrown if report with specified id or corresponding report task not found 
	 */
	//RemoteCommandProxy emailReports(int reportId, Collection<String> destinations) throws ReportingException;

	/**
	 * Cancel report command.
	 * 
	 * @param commandId
	 *            thrown if report task contains no commands
	 */
	//void cancelReportCommand(int commandId);


	/**
	 * Clear report command.
	 *
	 * @param commandId the command id
	 */
	void clearReportCommand(int commandId);

	/**
	 * Clear report commands.
	 * 
	 */
	//void clearReportCommands();

	/**
	 * Gets the report commands.
	 * 
	 * @return the report commands
	 */
	Collection<RemoteCommandProxy> getReportCommands();

	/**
	 * Gets the file storage path.
	 * 
	 * @return the file storage path
	 */
	//String getFileStoragePath();

	/**
	 * Gets the report command progress.
	 * 
	 * @param commandId
	 *            the command id
	 * @return the report command progress
	 */
	double getReportCommandProgress(int commandId);

	/**
	 * Gets the report command status.
	 * 
	 * @param commandId
	 *            the command id
	 * @return the report command status
	 */
	CommandStatus getReportCommandStatus(int commandId);

	/**
	 * Parses the report parameter value: given by String it returns with corresponding class type.
	 * 
	 * @param value
	 *            the value
	 * @param type
	 *            the type
	 * @return the object
	 */
	Object parseReportParameterValue(String value, Class<?> type);


    /**
     * Schedule report task.
     *
     * @param taskInfo
     *            the task info
 	 * @throws ObjectNotFoundDAOReportingException
	 * 				thrown if corresponding report template not found 
	 * @throws ObjectExistsException
	 * 				thrown if template task with specified name already exists
	 * @throws SchedulingReportingException
	 * 				thrown if it was unable to schedule task 	  
    */
    void scheduleReportTask(ReportTaskInfo taskInfo) throws ReportingException, ObjectExistsException;

	/**
	 * Removes the scheduled report task.
     *
	 * @param id Task id
     *
	 * @throws ObjectNotFoundDAOReportingException
	 * 				thrown if report task with specified id not found
	 * @throws SchedulingReportingException
	 * 				thrown if it was unable to remove task with specified id  
	 */
	void removeReportTask(int id) throws ReportingException;

    /**
     * Returns report task description by its name
     *
     * @param taskName Name of the task
     * @return Task description
	 * @throws ObjectNotFoundDAOReportingException
	 * 				thrown if report task with specified id not found
     */
	//ReportTaskInfo getReportTask(String taskName) throws ReportingException;

	/**
	 * Updates report task according to given taskInfo. The method takes
	 * taskInfo.getTaskId to find existing report task.
	 *
	 * @param taskInfo
	 *         New report task state to set
	 * 
	 * @throws IllegalArgumentException
	 *             if taskInfo is null
	 * @throws ReportingException
	 *             if taskInfo.getTaskId is 0 or negative
	 * @throws ReportingException
	 *             if task with id=taskInfo.getTaskId is not found
	 * @throws ReportingException
	 *             if task with id=taskInfo.getTaskId is not found
	 * @throws ObjectNotFoundDAOReportingException
	 * 				thrown if report task with specified id not found
	 *
	 */
    void updateReportTask(ReportTaskInfo taskInfo) throws ReportingException, ObjectExistsException;

    /**
     * Gets the scheduled report tasks.
     *
     * @param reportTemplateName
     *            the report template name
     * @return the scheduled report tasks
	 * @throws ObjectNotFoundDAOReportingException
	 * 				thrown if report template with specified name not found or if task with corresponding id was not found
     */
    Collection<ReportTaskInfo> getReportTasks(String reportTemplateName) throws ReportingException;

    /**
     * Returns report task description by its id
     *
     * @param taskId Id of the task
     * @return Task description
	 * @throws ObjectNotFoundDAOReportingException
	 * 				thrown if report task with specified id not found
     */
    ReportTaskInfo getReportTask(int taskId) throws ReportingException;
}
