package com.synapsense.service;

/**
 * @Management
 * 
 */
public interface RulesEngineManagement extends ServiceManagement {

	long getLastPropertyCalculationTime();

	void runDataRefresher();

	String listGraphBranch(String type, Integer id, String propertyName);
}
