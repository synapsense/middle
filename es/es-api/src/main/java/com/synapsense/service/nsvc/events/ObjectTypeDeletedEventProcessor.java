package com.synapsense.service.nsvc.events;

public interface ObjectTypeDeletedEventProcessor {
	ObjectTypeDeletedEvent process(ObjectTypeDeletedEvent e);
}
