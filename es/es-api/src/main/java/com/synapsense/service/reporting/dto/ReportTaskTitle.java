package com.synapsense.service.reporting.dto;

import java.io.Serializable;

/**
 * @author : shabanov
 */
public class ReportTaskTitle implements Serializable {
	
	private static final long serialVersionUID = 5333709954016015673L;

	private int taskId;

	private String reportTemplateName;

	private String name;

    private int status;

    public ReportTaskTitle(String reportTemplateName, String name) {
		this.reportTemplateName = reportTemplateName;
		this.name = name;
	}

	public int getTaskId() {
		return taskId;
	}

	public void setTaskId(int taskId) {
		this.taskId = taskId;
	}

	public String getReportTemplateName() {
		return reportTemplateName;
	}

	public void setReportTemplateName(String reportTemplateName) {
		this.reportTemplateName = reportTemplateName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

    public boolean isActive() {
        return status != 0;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
