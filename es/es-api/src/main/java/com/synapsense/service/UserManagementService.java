package com.synapsense.service;

import java.util.Collection;
import java.util.Map;

import com.synapsense.dto.TO;
import com.synapsense.exception.IllegalInputParameterException;
import com.synapsense.exception.UserManagementException;

/**
 * User service provides an API for accessing user accounts information.
 * 
 * @author shabanov
 */
public interface UserManagementService {

	public static final String USER_NAME = "userName";
	public static final String USER_PASSWORD = "userPassword";
	public static final String USER_PASSWORD_HINT = "userPasswordHint";
	public static final String USER_PASSWORD_HINT_ANSWER = "userPasswordHintAnswer";
	public static final String USER_FIRST_NAME = "FName";
	public static final String USER_LAST_NAME = "LName";
	public static final String USER_MIDDLE_INITIAL = "middleInitial";
	public static final String USER_TITLE = "title";
	public static final String USER_PRIMARY_PHONE = "primaryPhonenumber";
	public static final String USER_SECONDARY_PHONE = "secondaryPhonenumber";
	public static final String USER_PRIMARY_EMAIL = "primaryEmail";
	public static final String USER_SECONDARY_EMAIL = "secondaryEmail";
	public static final String USER_SMS_ADDRESS = "smsAddress";
	public static final String USER_DESCRIPTION = "description";
	public static final String USER_GROUPS = "groups";
	public static final String USER_PREFERENCES = "preferences";

	public static final String GROUP_NAME = "name";
	public static final String GROUP_DESCRIPTION = "description";
	public static final String GROUP_STATISTICS = "statistics";

	public static final String PRIVILEGE_NAME = "name";
	public static final String PRIVILEGE_DESCRIPTION = "description";
	public static final String PRIVILEGE_SYSTEM = "system";
	public static final String PRIVILEGE_TAG = "tag";
	public static final String PRIVILEGE_SORT_ORDER = "sortOrder";

	public static final String USER_PREFERENCE_NAME = "name";
	public static final String USER_PREFERENCE_VALUE = "value";

	/**
	 * Creates new group
	 *
	 * @param name
	 *            Group name
	 * @throws UserManagementException
	 *             Indicates problems creating group(group may already exist)
	 * @throws IllegalInputParameterException
	 *             if name is null
	 */
	TO<?> createGroup(String name, String desc) throws UserManagementException;

	/**
	 * Creates non-system privilege.
	 *
	 * @param name
	 *            Group name
	 * @throws UserManagementException
	 *             if privilege cannot be created (e.g. name already exists).
	 * @throws IllegalInputParameterException
	 *             if name is null
	 */
	TO<?> createPrivilege(String name) throws UserManagementException;

	/**
	 * Creates new user
	 *
	 * @param userName    User's name
	 * @param pwd         Password
	 * @param group       Group to add user to.  If null, will add to 'Standard' group
	 * @throws UserManagementException
	 *             Indicates problems creating user(user may already exist)
	 * @throws IllegalInputParameterException
	 *             if user is null
	 */
	/**
	 *
	 * @param userName
	 * @param pwd
	 * @param group
	 * @return
	 * @throws UserManagementException
	 */
	TO<?> createUser(String userName, String pwd, Object group) throws UserManagementException;

	/**
	 * Delete group by its name. Group assignment will be removed from all users
	 * as well.<br>
	 * "Administrators" group cannot be deleted.
	 *
	 * @param group
	 *            Group DTO
	 * @throws UserManagementException
	 *             if group cannot be deleted (e.g. group does not exist).
	 * @throws IllegalInputParameterException
	 *             if groupName is null or empty
	 */
	void deleteGroup(TO<?> group) throws UserManagementException;

	/**
	 * Delete privilege by its name. Privilege will be deleted from all groups,
	 * too<br>
	 * System privileges cannot be deleted.
	 *
	 * @param privilege
	 *            Privilege DTO
	 * @throws UserManagementException
	 *             if privilege cannot be deleted (e.g. privilege does not exist
	 *             or is a system one).
	 * @throws IllegalInputParameterException
	 *             if privName is null or empty
	 */
	void deletePrivilege(TO<?> privilege) throws UserManagementException;

	/**
	 * Removes existing user.
	 *
	 * @param user
	 *            User DTO
	 * @throws UserManagementException
	 *             Indicates problems deleting user
	 * @throws IllegalInputParameterException
	 *             if userName is null or empty
	 */
	void deleteUser(TO<?> user) throws UserManagementException;

	/**
	 * @return collection of available groups
	 */
	Collection<TO<?>> getAllGroups();

	/**
	 * @return collection of available privileges
	 */
	Collection<TO<?>> getAllPrivileges();

	/**
	 * Gets user list.
	 * 
	 * @return Collection of User DTOs
	 */
	Collection<TO<?>> getAllUsers();

	/**
	 * Get children privileges (one level down only)
	 *
	 * @param parentName
	 *            parent privilege name
	 * @throws UserManagementException
	 *             if parentName is null or empty or parent privilege cannot be
	 *             found
	 */
	Collection<String> getChildrenPrivilegeIds(String parentName) throws UserManagementException;

	/**
	 * Gets group DTO
	 *
	 * @param name
	 *            Group's name
	 * @return
	 * @throws IllegalInputParameterException
	 *             if name is null or empty
	 */
	TO<?> getGroup(String name);

	/**
	 * Gets group information
	 *
	 * @param group
	 *            Group's DTO
	 * @return
	 * @throws IllegalInputParameterException
	 *             if group is null
	 */
	Map<String, Object> getGroupInformation(TO<?> group);

	/**
	 * Get current privileges assigned to group
	 *
	 * @param group
	 *            Group DTO
	 * @throws UserManagementException
	 *             if group cannot be found
	 * @throws IllegalInputParameterException
	 *             if groupName is null or empty
	 */
	Collection<String> getGroupPrivilegeIds(TO<?> group) throws UserManagementException;

	/**
	 * Get current privileges assigned to group
	 *
	 * @param group
	 *            Group DTO
	 * @throws UserManagementException
	 *             if group cannot be found
	 * @throws IllegalInputParameterException
	 *             if groupName is null or empty
	 */
	Collection<TO<?>> getGroupPrivileges(TO<?> group) throws UserManagementException;

	/**
	 * Retrieve all users in a given group.
	 *
	 * @param group
	 *            Group DTO
	 * @return users in group. Empty collection if none (that is, returned
	 *         collection is never null)
	 * @throws UserManagementException
	 *             if group cannot be found
	 * @throws IllegalInputParameterException
	 *             if groupName is null or empty
	 */
	Collection<String> getGroupUserIds(TO<?> group) throws UserManagementException;

	/**
	 * Retrieve all users in a given group.
	 *
	 * @param group
	 *            Group DTO
	 * @return users in group. Empty collection if none (that is, returned
	 *         collection is never null)
	 * @throws UserManagementException
	 *             if group cannot be found
	 * @throws IllegalInputParameterException
	 *             if groupName is null or empty
	 */
	Collection<TO<?>> getGroupUsers(TO<?> group) throws UserManagementException;

	/**
	 * Gets privilege DTO
	 *
	 * @param name
	 *            Privilege's name
	 * @return
	 * @throws IllegalInputParameterException
	 *             if name is null or empty
	 */
	TO<?> getPrivilege(String name);

	/**
	 * Gets privilege information
	 *
	 * @param privilege
	 *            Privilege's DTO
	 * @return
	 * @throws IllegalInputParameterException
	 *             if privilege is null
	 */
	Map<String, Object> getPrivilegeInformation(TO<?> privilege);

	/**
	 * Gets a property from a TO object
	 *
	 * @param objId       TO object
	 * @param propName	  Property name
	 * @return  The property value, or null if the property is not found
	 * @throws IllegalInputParameterException
	 *             if objId is null or propName is null or empty
	 */
	Object getProperty(TO<?> objId, String propName);

	/**
	 * Allows to get raw password string from db
	 *
	 * @param user
	 *            User DTO
	 * @return encrypted password string
	 * @throws UserManagementException
	 *             Indicates problems changing user's password
	 * @throws IllegalInputParameterException
	 *             if userName is null or empty
	 *
	 */
	String getRawPassword(TO<?> user) throws UserManagementException;

	/**
	 * Get groups assigned to user.
	 *
	 * @param user
	 *            User DTO
	 * @return user groups. Empty collection if none (that is, returned
	 *         collection is never null)
	 * @throws UserManagementException
	 *             if user cannot be found
	 * @throws IllegalInputParameterException
	 *             if name is null or empty
	 */
	Collection<String> getUserGroupIds(TO<?> user) throws UserManagementException;

	/**
	 * Get groups assigned to user.
	 *
	 * @param user
	 *            User DTO
	 * @return user groups. Empty collection if none (that is, returned
	 *         collection is never null)
	 * @throws UserManagementException
	 *             if user cannot be found
	 * @throws IllegalInputParameterException
	 *             if userName is null or empty
	 */
	Collection<TO<?>> getUserGroups(TO<?> user) throws UserManagementException;

	/**
	 * Gets user DTO
	 *
	 * @param userName
	 *            User's name
	 * @return
	 * @throws IllegalInputParameterException
	 *             if userName is null or empty
	 */
	TO<?> getUser(String userName);

	/**
	 * Gets user information
	 *
	 * @param user
	 *            User's DTO
	 * @return
	 * @throws IllegalInputParameterException
	 *             if userName is null or empty
	 */
	Map<String, Object> getUserInformation(TO<?> user);

	/**
	 * Gets all user's preferences
	 *
	 * @param user
	 *            User DTO
	 * @return Collection of configured user's preferences
	 * @throws IllegalInputParameterException
	 *             if userName is null or empty
	 */
	Map<String, Object> getUserPreferences(TO<?> user);

	/**
	 * Get effective set of privileges assigned to user.
	 *
	 * @param user
	 *            User DTO
	 * @return user privileges. Empty collection if none (that is, returned
	 *         collection is never null)
	 * @throws UserManagementException
	 *             if user cannot be found
	 * @throws IllegalInputParameterException
	 *             if userName is null or empty
	 */
	Collection<String> getUserPrivilegeIds(TO<?> user) throws UserManagementException;

	/**
	 * Removes preference from user's available preferences. It will not be
	 * visible via {@link #getUserPreferences(TO<?>)}.
	 *
	 * @param user
	 *            User DTO
	 * @param preferenceName
	 *            Removed preference
	 * @throws UserManagementException
	 *             If there is no the specified user or indicates execution
	 *             problems
	 * @throws IllegalInputParameterException
	 *             if userName is null or empty or if preference is null
	 */
	void removeUserPreference(TO<?> user, String preferenceName) throws UserManagementException;

	/**
	 * Update collection of privileges assigned to group
	 *
	 * @param group
	 *            Group DTO
	 * @param privileges
	 * @throws UserManagementException
	 *             if group cannot be found, group cannot be updated
	 *             (Administrators) or one of privileges cannot be found
	 * @throws IllegalInputParameterException
	 *             if groupName is null or empty or if privileges is null
	 */
	void setGroupPrivileges(TO<?> group, Collection<String> privileges) throws UserManagementException;

	/**
	 * Changes user password
	 *
	 * @param user
	 *            User DTO
	 * @param password
	 *            New user's password
	 * @throws UserManagementException
	 *             Indicates problems changing user's password
	 * @throws IllegalInputParameterException
	 *             if userName is null or empty or if password is null or empty
	 */
	void setPassword(TO<?> user, String password) throws UserManagementException;

	/**
	 * Sets a property in a TO object.
	 *
	 * @param objId			TO object
	 * @param propName		Property to set
	 * @param propValue		Property value
	 */
	void setProperty(TO<?> objId, String propName, Object propValue) throws UserManagementException;

	/**
	 * Allows to put raw password string to db
	 *
	 * @param user
	 *            User DTO
	 * @param password
	 *            New user's password
	 * @throws UserManagementException
	 *             Indicates problems changing user's password
	 * @throws IllegalInputParameterException
	 *             if userName is null or empty or if password is null or empty
	 */
	void setRawPassword(TO<?> user, String password) throws UserManagementException;

	/**
	 * Update collection of privileges assigned to user
	 *
	 * @param user
	 *            User DTO
	 * @param groups
	 * @throws UserManagementException
	 *             if user cannot be found, user cannot be updated ("admin") or
	 *             one of groups cannot be found
	 * @throws IllegalInputParameterException
	 *             if userName is null or empty or if groups is null
	 */
	void setUserGroups(TO<?> user, Object groups) throws UserManagementException;

	/**
	 * Sets user preference value. If user has no such preference it will be
	 * added.
	 *
	 * @param user
	 *            User DTO
	 * @param preferenceName
	 * 				Preference name
	 * @param value
	 *            Preference value
	 * @throws UserManagementException
	 *             If there is no the specified user or indicates execution
	 *             problems
	 * @throws IllegalInputParameterException
	 *             if userName is null or empty or if value is null
	 */
	void setUserPreferenceValue(TO<?> user, String preferenceName, String value) throws UserManagementException;

	/**
	 * Method for renaming group, changing its description or both.<br>
	 * "Administrators" group cannot be updated.<br>
	 * To rename group, specify its old name in groupName and set the new one in
	 * group parameter.<br>
	 * To update description, set new description in group parameter and pass
	 * groupName = group.getName().
	 *
	 * @param group
	 *            Group DTO
	 * @throws UserManagementException
	 *             if group cannot be updated (e.g. name does not exists)
	 * @throws IllegalInputParameterException
	 *             if groupName is null or empty or if group is null
	 */
	void updateGroup(TO<?> group, Map<String, Object> changedProps) throws UserManagementException;

	/**
	 * Method for updating usage statistics for group.
	 *
	 * @param group
	 *            Group DTO
	 * @param statistics
	 * @throws UserManagementException
	 *             if group cannot be updated (e.g. name does not exists)
	 * @throws IllegalInputParameterException
	 *             if groupName is null or empty
	 */
	void updateGroupStatistics(TO<?> group, String statistics) throws UserManagementException;

	/**
	 * Method for renaming privilege, changing its description or both.<br>
	 * System privileges cannot be updated.<br>
	 * To rename privilege, specify its old name in privName and set the new one
	 * in privilege parameter.<br>
	 * To update description, set new description in privilege parameter and
	 * pass privName = privilege.getName().
	 *
	 * @param privilege
	 *            Privilege DTO
	 * @throws UserManagementException
	 *             if privilege cannot be updated (e.g. name does not exists or
	 *             privilege is a system one).
	 * @throws IllegalInputParameterException
	 *             if privName is null or empty or if privilege is null
	 */
	void updatePrivilege(TO<?> privilege, Map<String, Object> changedProps) throws UserManagementException;

	/**
	 * Updates existing user by synchronizing its state with the specified DTO
	 * <code>user</code>.
	 *
	 * @param user
	 *            User DTO
	 * @throws UserManagementException
	 *             Indicates problems updating user's state
	 * @throws IllegalInputParameterException
	 *             if userName is null or empty or if user is null
	 */
	void updateUser(TO<?> user, Map<String, Object> changedProps) throws UserManagementException;
}
