package com.synapsense.service.reporting.scriplets;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.synapsense.service.reporting.scriplets.charts.ChartCustomizer;
import net.sf.jasperreports.engine.JRScriptletException;

import org.apache.log4j.Logger;
import org.jfree.chart.JFreeChart;
import org.json.JSONArray;
import org.json.JSONObject;

import com.synapsense.dto.TO;
import com.synapsense.service.reporting.Formula;
import com.synapsense.service.reporting.Formula.TransformType;
import com.synapsense.service.reporting.SortingProperty;
import com.synapsense.service.reporting.datasource.ReportingTableDataSource;
import com.synapsense.service.reporting.olap.RangeSet;
import com.synapsense.service.reporting.scriplets.charts.ChartBuilder;
import com.synapsense.service.reporting.scriplets.charts.HistogramChartBuilder;
import com.synapsense.util.CollectionUtils;

public class ReportingHistogramScriptlet extends AbstractChartScriptlet {
	
	private static final Logger log = Logger.getLogger(ReportingHistogramScriptlet.class);
    
	@Override
    protected ChartBuilder getChartBuilder() {
        return new HistogramChartBuilder();
    }

    @Override
    protected void standartCustom(JFreeChart timeChart) {
        ChartCustomizer.standartCustomXYPlot(timeChart);
    }

    @Override
    protected void getData(RangeSet period) throws JRScriptletException {
        try {
            String columnProperties = (String) this.getParameterValue(COLUMN_PROPERTIES);
            if (log.isInfoEnabled()) {
            	log.info(columnProperties);
            }
	        JSONObject columnPropertiesJSON = new JSONObject(columnProperties);
	        JSONArray propertiesJsonArray = columnPropertiesJSON.getJSONArray("properties");
            
            List<String[]> propertiesArrayList = CollectionUtils.newArrayList(propertiesJsonArray.length());
            for (int i = 0; i < propertiesJsonArray.length(); i++) {
                JSONObject jsonObject = propertiesJsonArray.getJSONObject(i);
                if ((boolean) jsonObject.get("value") == true) {
                    propertiesArrayList.add(((String)jsonObject.get("name")).split(","));
                }
            }

            Set<TO<?>> objects = (HashSet<TO<?>>)getParameterValue(OBJECTS);
            String sortingObjectType = (String)getParameterValue("SortingObjectType");
            SortingProperty sortingProperty = new SortingProperty(sortingObjectType, "name");
            Formula formula = new Formula(sortingProperty, TransformType.ORIGINAL);
            reportingChartDataSource = ReportingTableDataSource.newHistagram(
                        objects, propertiesArrayList, formula, period);
            bins = (int) this.getParameterValue("RangeDivider");
            reportTable = reportingChartDataSource.getData();
            initUserPreferences();
            //Set customized values
	        if (new JSONObject(this.parametersMap.get(COLUMN_PROPERTIES).getDescription()).has(IS_CUSTOM_SPECIFIED)) {
	        	processCustomValues(this.parametersMap.get(COLUMN_PROPERTIES).getPropertiesMap(), columnPropertiesJSON);
        	}
            //Customize lines
	        fillPropertyLineOptionMap(propertiesJsonArray);
	        //Customize dimensions
	        if (columnPropertiesJSON.has("dimensions") 
	        		&& columnPropertiesJSON.getJSONArray("dimensions").length() != 0) {
	        	fillDimensionOptionMap(columnPropertiesJSON.getJSONArray("dimensions"), unitSystems);
	        }
	        
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
