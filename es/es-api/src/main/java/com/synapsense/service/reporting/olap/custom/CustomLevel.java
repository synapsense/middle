package com.synapsense.service.reporting.olap.custom;

import com.synapsense.service.reporting.olap.Hierarchy;

public class CustomLevel implements Level {

	private static final long serialVersionUID = -130324213456381422L;

	private int membersRange;
	
	private Hierarchy hierarchy;

	private String levelName;
	
	public CustomLevel(Hierarchy hierarchy, String levelName, int membersRange) {
		this.hierarchy = hierarchy;
		this.levelName = levelName;
		this.membersRange = membersRange;
	}

	@Override
	public String getName() {
		return this.levelName;
	}

	@Override
	public int getMembersRange() {
		return this.membersRange;
	}

	@Override
	public Hierarchy getHierarchy() {
		return hierarchy;
	}

	@Override
	public String getRangeLevel() {
		return getName();
	}
	
	@Override
	public CustomLevel copy() {
		CustomLevel copy = new CustomLevel(hierarchy, levelName, membersRange);
		return copy;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((hierarchy == null) ? 0 : hierarchy.hashCode());
		result = prime * result
				+ ((levelName == null) ? 0 : levelName.hashCode());
		result = prime * result + membersRange;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		CustomLevel other = (CustomLevel) obj;
		if (hierarchy == null) {
			if (other.hierarchy != null) {
				return false;
			}
		} else if (!hierarchy.equals(other.hierarchy)) {
			return false;
		}
		if (levelName == null) {
			if (other.levelName != null) {
				return false;
			}
		} else if (!levelName.equals(other.levelName)) {
			return false;
		}
		if (membersRange != other.membersRange) {
			return false;
		}
		return true;
	}
	
}
