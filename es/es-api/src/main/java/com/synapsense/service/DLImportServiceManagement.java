package com.synapsense.service;

import java.util.Collection;

import com.synapsense.dto.DLImportResult;
import com.synapsense.dto.DLObjectTypeImportResult;
import com.synapsense.exception.DLImportException;

/**
 * Management interface for MapSense components import service.
 * 
 * @author Oleg Stepanov
 * @author Gabriel Helman
 * 
 */
public interface DLImportServiceManagement extends ServiceManagement {

	/**
	 * Import object type definitions from their XML form. Based on the format
	 * of the DeploymentLab objects.xml file; this input should only have the
	 * objects types needed. In an object type provided here already exists in
	 * the database, the type will be overwritten.
	 * 
	 * @param typeDefinitions
	 *            a String containing the objects definitions
	 * @return a Collection of results for the import, one for each type in the
	 *         source file
	 * @throws DLImportException
	 *             if something goes wrong.
	 * 
	 * @see DLObjectTypeImportResult
	 * @see DLImportException
	 */
	public Collection<DLObjectTypeImportResult> importObjectTypes(String typeDefinitions) throws DLImportException;

	/**
	 * Create and Update object instances on the server.
	 * 
	 * @param objectXML
	 *            a String holding an XML representation of the object model.
	 *            The format is based on the <code>objects</code> of the DL file
	 *            format.
	 * @return a Collection of DLImportResult objects, one for each object.
	 * @throws DLImportException
	 *             if something goes wrong. This should only happen in case of
	 *             an XML parse error or something totally unexpected; other
	 *             than that, any other errors should be returned as part of the
	 *             DLImportResult objects.
	 * 
	 * @see DLImportResult
	 * @see DLImportException
	 */
	public Collection<DLImportResult> importObjectInstances(String objectXML) throws DLImportException;

}
