package com.synapsense.service.nsvc.events;

import com.synapsense.dto.TO;
import com.synapsense.service.nsvc.Event;
import com.synapsense.service.nsvc.EventDispatcher;

public class ObjectDeletedEvent implements Event {

	private static final long serialVersionUID = 1179395455044496508L;
	private final TO<?> objectTO;

	public ObjectDeletedEvent(TO<?> objectTO) {
		this.objectTO = objectTO;
	}

	public TO<?> getObjectTO() {
		return objectTO;
	}

	@Override
	public Event dispatch(EventDispatcher ed) {
		return ed.dispatch(this);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(super.toString()).append(": TO=").append(objectTO);
		return sb.toString();
	}
}
