package com.synapsense.service.reporting.scriplets.charts;

import java.util.List;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import com.synapsense.util.Pair;

public class DimensionTimeSeriesChartBuilder extends TimeSeriesChartBuilder {

	@Override
	public void addDataSerie(String serie_id, List<SensorPoint> pointArray, String seriesName, String axisName, int bins){

		TimeSeries currSeries = createTimeSeries(pointArray, seriesName);		
		TimeSeriesCollection currTSC = null;
		
		if (datasetTimeSeriesMap.containsKey(serie_id)) {
			currTSC = datasetTimeSeriesMap.get(serie_id);
		} else {
			currTSC = new TimeSeriesCollection();
			datasetTimeSeriesMap.put(serie_id, currTSC);
		}			
		currTSC.addSeries(currSeries);			
		axisNameMap.put(serie_id, axisName);
		
	}

	@Override
	protected Pair<String, String> getValueAxisPair(String entryKey,
			String currValueAxisName) {
		return Pair.newPair(currValueAxisName, entryKey);
	}

}
