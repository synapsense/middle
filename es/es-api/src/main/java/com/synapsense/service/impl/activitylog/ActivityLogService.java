package com.synapsense.service.impl.activitylog;

import java.util.Collection;
import java.util.List;

/**
 * Interface of the service allowing to record activities in the system.
 * 
 * @author Stepanov Oleg.
 * 
 */
public interface ActivityLogService {

	/**
	 * Returns sorted list of activity actions available to browse.<br>
	 * Note that sort algorithm uses simple lexicographical order.<br>
	 * <br>
	 * If there is no activity log records an empty<code>List</code> instance
	 * will be returned.<br>
	 * Returns whole records from ES by ID or null if record doesn't exist.<br>
	 * 
	 * @return Sorted <code>List</code> of activity actions names
	 * @param ID
	 * @return <code>ActivityRecord</code>
	 */
	List<String> getActivityActions();

	/**
	 * Returns sorted list of activity modules available to browse.<br>
	 * Note that sort algorithm uses simple lexicographical order.<br>
	 * <br>
	 * If there is no activity log records an empty<code>List</code> instance
	 * will be returned.<br>
	 * 
	 * @return Sorted <code>List</code> of activity modules names
	 */
	List<String> getActivityModules();

	/**
	 * Returns whole records from ES by ID or null if record doesn't exist.<br>
	 * 
	 * @param ID
	 * @return <code>ActivityRecord</code>
	 */
	ActivityRecord getRecord(int ID);

	/**
	 * Logs an activity.
	 * 
	 * @param userName
	 *            user performed an action. (!= null && not empty)
	 * @param moduleName
	 *            module where the action was performed. (!= null && not empty)
	 * @param actionName
	 *            performed action.(!= null && not empty)
	 * @param description
	 *            action description.(!= null && not empty)
	 * @throws IllegalInputParameterException
	 *             if at least on of the arguments is null or empty
	 * 
	 *             replaced by
	 *             <code>addRecord(new ActivityRecord(String userName, String moduleName, String actionName, String description)))</code>
	 *             .
	 */
	@Deprecated
	void log(String userName, String moduleName, String actionName, String description);

	/**
	 * Adds new record
	 */
	void addRecord(ActivityRecord record);

	/**
	 * Adds new records
	 */
	void addRecords(Collection<ActivityRecord> recordsList);

	/**
	 * Removes existing record
	 * 
	 * @param ID
	 */
	void deleteRecord(int ID);

	/**
	 * Removes existing record
	 * 
	 * @param Collection
	 *            of ID's.
	 */
	void deleteRecord(Collection<Integer> IDs);

	/**
	 * existing record's properties
	 * 
	 * @param ActivityRecord
	 *            ;
	 */
	void updateRecord(ActivityRecord record);

	/**
	 * Updates existing record's properties
	 * 
	 * @param Collection
	 *            of activity records.
	 */
	void updateRecord(Collection<ActivityRecord> recordsList);

	/**
	 * Allows caller to get records using filter
	 * 
	 * @param ActivityRecordFilter
	 * @return Collection of activity records
	 */
	Collection<ActivityRecord> getRecords(ActivityRecordFilter filter);
}
