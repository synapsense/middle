package com.synapsense.service;

import com.synapsense.dto.BinaryData;
import com.synapsense.dto.DLImportResult;
import com.synapsense.dto.DLObjectTypeImportResult;
import com.synapsense.dto.TO;
import com.synapsense.exception.DLImportException;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.exception.PropertyNotFoundException;
import com.synapsense.exception.UnableToConvertPropertyException;

import java.util.Collection;
import java.util.HashMap;

/**
 * MapSense components import service.
 * 
 * @author Oleg Stepanov
 * @author Alex Pakhunov
 * @author Gabriel Helman
 */
public interface DLImportService {

	// DLImportResult importModel(byte[] model, byte[] config) throws
	// DLImportException;

	/**
	 * Import object type definitions from their XML form. Based on the format
	 * of the DeploymentLab objects.xml file; this input should only have the
	 * objects types needed. In an object type provided here already exists in
	 * the database, the type will be overwritten.
	 * 
	 * @param typeDefinitions
	 *            a String containing the objects definitions
	 * @return a Collection of results for the import, one for each type in the
	 *         source file
	 * @throws DLImportException
	 *             if something goes wrong.
	 * 
	 * @see DLObjectTypeImportResult
	 * @see DLImportException
	 * 
	 * Transaction: required
	 */
	Collection<DLObjectTypeImportResult> importObjectTypes(String typeDefinitions) throws DLImportException;

	/**
	 * Create and Update object instances on the server.
	 * 
	 * @param objectXML
	 *            a String holding an XML representation of the object model.
	 *            The format is based on the <code>objects</code> of the DL file
	 *            format.
	 * @return a Collection of DLImportResult objects, one for each object.
	 * @throws DLImportException
	 *             if something goes wrong. This should only happen in case of
	 *             an XML parse error or something totally unexpected; other
	 *             than that, any other errors should be returned as part of the
	 *             DLImportResult objects.
	 * 
	 * @see DLImportResult
	 * @see DLImportException
	 */
	Collection<DLImportResult> importObjectInstances(String objectXML) throws DLImportException;

	Collection<DLImportResult> importObjectInstances(String objectXML, HashMap<String, String> controlOptions)
	        throws DLImportException;


	Collection<DLImportResult> importObjectInstancesZipped(byte[] dlzFile, HashMap<String, String> controlOptions)
	        throws DLImportException;

	/*
	 * importModel(byte[] model, byte[] config, Map images) objects file as
	 * zipstream of xml dl file as zipstream of xml some kinda container of
	 * images (map of object name : images as bytestreams ?)
	 */

	boolean storeDLZFile(TO<?> dcKey, BinaryData config) throws DLImportException;

}
