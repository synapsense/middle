package com.synapsense.service.reporting.datasource;

import java.awt.Color;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;

import com.synapsense.util.CollectionUtils;
import net.sf.jasperreports.engine.JRField;
import net.sf.jasperreports.engine.design.JRDesignField;

import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;
import com.synapsense.service.reporting.olap.RangeSet;
import com.synapsense.util.unitconverter.Dimension;

/**
 * The <code>TabularCustomQueryDataSource</code> class implements Table
 * Datasource for accessing Tabular Custom Query Data.<br>
 * Example: new TabularCustomQueryDataSource($P{CustomQuery}, $P{Start Date},
 * $P{End Date})
 * 
 * DataSource has the following fields: "Object Name", "Data Type", "Data Value" and "Timestamp"
 * 
 * @author Alexander Pakhunov
 * 
 */
public class TabularCustomQueryDataSource extends AbstractCustomQueryDataSource {

	private Iterator<Map<String, String>> values;
	private Map<String, String> current;
	private Collection<Map<String, String>> records;

	public TabularCustomQueryDataSource(TO<?> customQueryTo, Date startDate, Date endDate) throws Exception {
		super(customQueryTo, startDate, endDate);
	}
	
	public TabularCustomQueryDataSource(TO<?> customQueryTo, RangeSet period) throws Exception {
		super(customQueryTo, period);
	}

	@Override
	public Object getFieldValue(JRField field) {
		return current.get(field.getName());
	}

	@Override
	public boolean next() {
		if (values.hasNext()) {
			current = values.next();
			return true;
		}
		return false;
	}

	@Override
    protected void beforeProcessing(String s) {
		records = CollectionUtils.newList();
    }

	@Override
    protected void processProperty(String objName, String objType, String propName, Dimension dimension, Collection<ValueTO> valueTos, float lineWidth, Color lineColor, boolean advancedChartAverageNeeded) {

		for (ValueTO valueTo : valueTos) {
			Object value = valueTo.getValue();
			if (value != null) {
				Map<String, String> record = new HashMap<>(4);
				record.put("Object Name", objName);
				record.put("Data Type", getDisplayablePropertyName(objType, propName, locale));
				record.put("Data Value", numberFormat.format(value));
				record.put("Timestamp", dateFormat.format(new Date(valueTo.getTimeStamp())));
				records.add(record);
			}
		}
    }

	@Override
    protected void afterProcessing(String chartDescr) {
		values = records.iterator();
    }

	public static void main(String[] args) throws Exception {
		TabularCustomQueryDataSource dataSource = new com.synapsense.service.reporting.datasource.TabularCustomQueryDataSource(
		        com.synapsense.dto.TOFactory.getInstance().loadTO("CUSTOMQUERY:47920"), new java.util.Date(
		                System.currentTimeMillis() - 2 * 60 * 60 * 1000), new java.util.Date());
		
		JRDesignField nameField = new JRDesignField();
		nameField.setName("Object Name");

		JRDesignField typeField = new JRDesignField();
		typeField.setName("Data Type");

		JRDesignField valueField = new JRDesignField();
		valueField.setName("Data Value");
		JRDesignField timestampField = new JRDesignField();
		timestampField.setName("Timestamp");

		while (dataSource.next()) {
			System.out.println(dataSource.getFieldValue(nameField) + "\t" + dataSource.getFieldValue(typeField) + "\t"
			        + dataSource.getFieldValue(valueField) + "\t" + dataSource.getFieldValue(timestampField));
		}
	}
}