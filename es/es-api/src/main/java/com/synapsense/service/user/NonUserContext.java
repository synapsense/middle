package com.synapsense.service.user;

import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;

import com.synapsense.util.LocalizationUtils;

public class NonUserContext implements UserContext {

	@Override
	public String getUnitSystem() {
		return System.getProperty("com.synapsense.defaultUnitSystem", "Server");
	}

	@Override
	public DateFormat getDateFormat() {
		return new SimpleDateFormat(System.getProperty("com.synapsense.defaultDateFormat", "yyyy-MM-dd HH:mm:ss") + " z");
	}

	@Override
	public NumberFormat getDataFormat() {
		NumberFormat numberFormatter = NumberFormat.getInstance(Locale.US);
		numberFormatter.setRoundingMode(RoundingMode.HALF_UP);
		numberFormatter.setGroupingUsed(false);
		numberFormatter.setMaximumFractionDigits(2);
		numberFormatter.setMinimumFractionDigits(2);

		String decimalPoint = System.getProperty("com.synapsense.defaultDecimalSeparator", ".");
		DecimalFormat df = (DecimalFormat) numberFormatter;
		DecimalFormatSymbols symbols = df.getDecimalFormatSymbols();
		symbols.setDecimalSeparator(decimalPoint.charAt(0));
		df.setDecimalFormatSymbols(symbols);
		
		return numberFormatter;
	}

	@Override
	public Locale getLocale() {
		return LocalizationUtils.getLocale(System.getProperty("com.synapsense.defaultLanguage", "en_US"));
	}

}