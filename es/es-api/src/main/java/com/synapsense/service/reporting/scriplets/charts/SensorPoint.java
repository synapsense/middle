package com.synapsense.service.reporting.scriplets.charts;

public class SensorPoint{
	public SensorPoint(){};
	
	public SensorPoint(double Value, long timeStamp){
		value = Value;
		time_stamp = timeStamp;
	};
	
	public SensorPoint(Number value, long timeStamp){
		this.value = value;
		time_stamp = timeStamp;
	};
	private Number value;
	
	
	public Number getValue() {
		return value;
	}
	//	public double value = 0.0;
	public long time_stamp = 0L;
};