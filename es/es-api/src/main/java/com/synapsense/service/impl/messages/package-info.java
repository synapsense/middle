/**
 * Contains classes and interfaces used to asynchronously
 * pass data to the Environment Service from various
 * external data sources like Device Manager. Data are passed
 * in form of JMS messages.
 */
package com.synapsense.service.impl.messages;