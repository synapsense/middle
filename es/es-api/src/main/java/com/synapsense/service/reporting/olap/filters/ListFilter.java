package com.synapsense.service.reporting.olap.filters;

import java.io.Serializable;
import java.util.*;

import com.synapsense.service.reporting.olap.Filter;
import com.synapsense.util.CollectionUtils;


public abstract class ListFilter implements Filter, Serializable {

	private static final long serialVersionUID = -1692097758566560993L;
	protected List<Filter> filters = CollectionUtils.newList();

	public ListFilter(Filter... filters) {
		this.filters = new LinkedList<>(Arrays.asList(filters));
	}

	public ListFilter() {
	}

	public ListFilter addFilter(Filter filter) {
		filters.add(filter);
		return this;
	}

	@Override
	public Collection<String> getProperties() {
		HashSet<String> props = new HashSet<>();
		for (Filter filter : filters) {
			props.addAll(filter.getProperties());
		}
		return props;
	}

    public List<Filter> getFilters() {
        return filters;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((filters == null) ? 0 : filters.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		ListFilter other = (ListFilter) obj;
		if (filters == null) {
			if (other.filters != null) {
				return false;
			}
		} else if (!filters.equals(other.filters)) {
			return false;
		}
		return true;
	}
    
    
}
