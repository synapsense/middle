package com.synapsense.service.nsvc.filters;

import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;

import com.synapsense.service.nsvc.Event;
import com.synapsense.service.nsvc.EventProcessor;

public class AndFilter implements EventProcessor, Serializable {

	private static final long serialVersionUID = 8795022569427774945L;

	private final Collection<EventProcessor> filters;

	public AndFilter(Collection<EventProcessor> filters) {
		if (filters == null)
			this.filters = Collections.emptyList();
		else
			this.filters = filters;
	}

	@Override
	public Event process(Event e) {
		Event result = e;
		for (EventProcessor filter : filters) {
			result = filter.process(e);
			if (result == null)
				return null;
		}
		return result;
	}

}
