package com.synapsense.service.reporting.scriplets.charts;

import java.awt.BasicStroke;
import java.awt.Color;

import org.jfree.chart.renderer.xy.XYItemRenderer;

public class LineDescr {
	private float lineWeigth = 1f;
	private Color lineColor = Color.black;
	private boolean isBasic = true;
	private float[] dashPattern = new float[] {10.0f, 6.0f};
	private float dashPhase = 0.0f;
	
	public LineDescr(){		
	}
	
	public LineDescr(float line_weight, Color line_color){
		lineWeigth = line_weight;
		lineColor = line_color;
	}
	
	public LineDescr(float line_weight, Color line_color, float[] dash_pattern, float dash_phase){
		lineWeigth = line_weight;
		lineColor = line_color;
		isBasic = false;
		dashPattern = dash_pattern;
		dashPhase = dash_phase;		
	}
	
	public String packToString(){		
		StringBuffer strBuf = new StringBuffer();
		
		strBuf.append(lineWeigth).append(",");		      
				
		if(!isBasic){			
		}				
		
		return strBuf.toString();
	}
	
	public static LineDescr unpackFromString(String str){
		LineDescr result = null;
		int phaseIndex = 2;
		int patternIndex = 3;
		String [] dataArr = str.split(",");
		
		float weight = Float.parseFloat(dataArr[0]);
		int colorInt = Integer.parseInt(dataArr[1]);		
		
		if(dataArr.length > phaseIndex){
			float phase = Float.parseFloat(dataArr[phaseIndex]);
			float [] pattern = new float[dataArr.length - patternIndex];
			int j = 0;
			for(int i = patternIndex; i < dataArr.length; i++){				
				pattern[j] = Float.parseFloat(dataArr[i]);
				j++;
			}
			
			result = new LineDescr(weight, new Color(colorInt), pattern, phase);
		}else{
			result = new LineDescr(weight, new Color(colorInt));
		}
						
		return result;
	}
	
	public void customizeSeries(XYItemRenderer renderer, int serieNumber){
		renderer.setSeriesPaint(serieNumber, lineColor);								
		
		if(isBasic){
			renderer.setSeriesStroke(serieNumber, new BasicStroke(lineWeigth));			
		}else{
			renderer.setSeriesStroke(serieNumber, new BasicStroke(lineWeigth,
	                BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND, 1.0f, 	                
	                dashPattern, dashPhase));
		}		
	}		
}
