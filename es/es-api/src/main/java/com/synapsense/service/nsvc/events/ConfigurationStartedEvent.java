package com.synapsense.service.nsvc.events;

import com.synapsense.service.nsvc.Event;
import com.synapsense.service.nsvc.EventDispatcher;

/**
 * Should be raised whenever DLImportService starts ES configuration
 * 
 * @author Dmitry Grudzinskiy
 * 
 */
public class ConfigurationStartedEvent implements Event {

	private static final long serialVersionUID = 5936663680038426569L;

	@Override
	public Event dispatch(EventDispatcher ed) {
		return ed.dispatch(this);
	}

	@Override
	public String toString() {
		return "ES configuration started";
	}
}
