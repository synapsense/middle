package com.synapsense.service.reporting.olap;

import java.util.List;

import com.synapsense.service.reporting.olap.custom.Level;

/**
 * Interface for a point on a dimension
 * 
 * @author Dmitry Grudzinskiy
 * 
 */
public interface Member extends Tuple {

	/**
	 * Hierarchy to which this member belongs
	 * 
	 * @return hierarchy object
	 */
	Hierarchy getHierarchy();

	/**
	 * Level of this member
	 * 
	 * @return level
	 */
	String getLevel();

	/**
	 * Parent member
	 * 
	 * @return member which is the parent of this one
	 * 
	 */
	Member getParent();

	/**
	 * Key associated with this member. Key must always be of type specified in
	 * the hierarchy of the member.
	 * 
	 * @return member key
	 */
	Object getKey();

	/**
	 * Returns whether the other member is from the same hierarchy and level
	 * 
	 * @param other
	 * @return true if this member is from the same hierarchy and level, false
	 *         otherwise
	 */
	boolean isBrother(Member other);

	/**
	 * Merges two members from the same hierarchies
	 * 
	 * @param other
	 * @return new member object or null if the crossjoin of the members is
	 *         empty
	 * 
	 */
	Member merge(Member other);

	/**
	 * Gets the custom level.
	 *
	 * @return the custom level
	 */
	Level getCustomLevel();

	/**
	 * Gets the simple members.
	 *
	 * @return the simple members
	 */
	List<Member> getSimpleMembers();
}
