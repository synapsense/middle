package com.synapsense.service.reporting.olap.filters;

import java.util.Map;

import com.synapsense.service.reporting.olap.Filter;


public class OrFilter extends ListFilter {

	private static final long serialVersionUID = 6575135076647676958L;

	public OrFilter(Filter... filters) {
		super(filters);
	}

	@Override
	public boolean accept(Map<String, Object> values) {
		for (Filter filter : filters) {
			if (filter.accept(values)) {
				return true;
			}
		}
		return false;
	}

}
