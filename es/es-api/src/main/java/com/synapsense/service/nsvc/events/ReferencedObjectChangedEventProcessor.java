package com.synapsense.service.nsvc.events;

public interface ReferencedObjectChangedEventProcessor {
	ReferencedObjectChangedEvent process(ReferencedObjectChangedEvent e);
}
