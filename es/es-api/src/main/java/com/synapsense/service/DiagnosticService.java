package com.synapsense.service;

import java.io.IOException;

public interface DiagnosticService {
	public String generateDiagnosticPackage() throws IOException;
	public byte[] readDiagnosticFileBytes(String packId, long fileOffset) throws IOException;
	public void deletePackage(String packId);
}
