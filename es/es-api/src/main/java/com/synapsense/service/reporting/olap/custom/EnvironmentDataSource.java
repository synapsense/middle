package com.synapsense.service.reporting.olap.custom;

import com.google.common.collect.ListMultimap;
import com.synapsense.dto.CollectionTO;
import com.synapsense.dto.PropertyDescr;
import com.synapsense.dto.TO;
import com.synapsense.service.Environment;
import com.synapsense.service.reporting.datasource.ReportType;
import com.synapsense.service.reporting.olap.Filter;
import com.synapsense.service.reporting.olap.Member;
import com.synapsense.util.unitconverter.UnitResolvers;
import com.synapsense.util.unitconverter.UnitSystems;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * The <code>EnvironmentDataSource</code> interface describes functionality 
 * for providing data source necessary for reporting.
 * 
 * @author Grigory Ryabov
 */
public interface EnvironmentDataSource {

	/**
	 * Gets the property value.
	 *
	 * @param objIds the obj ids
	 * @param propNames the prop names
	 * @return the property value
	 */
	Collection<CollectionTO> getPropertyValue(Collection<TO<?>> objIds, String[] propNames);

    /**
	 * Gets the objects by name.
	 *
	 * @param objectLevel the object level
	 * @param objectNames the object names
	 * @return the objects by name
	 */
	Map<String, TO<?>> getObjectsByName(String objectLevel, Set<String> objectNames);

	/**
	 * Gets the property descriptor.
	 *
	 * @param typeName the type name
	 * @param propertyName the property name
	 * @return the property descriptor
	 */
	PropertyDescr getPropertyDescriptor(String typeName, String propertyName);

	/**
	 * Gets the descendant object names.
	 *
	 * @param objectType the object type
	 * @param parent the parent
	 * @return the descendant objects
	 */
	List<String> getDescendantObjectNames(String objectType, Member parent);

	/**
	 * Gets the descendant object names.
	 *
	 * @param objectType the object type
	 * @param parent the parent
	 * @param filter the filter
	 * @return the descendant objects
	 */
	List<String> getDescendantObjectNames(String objectType, Member parent, Filter filter);

	/**
	 * Gets the related objects.
	 *
	 * @param cellObject the cell object
	 * @param propertyObjectType the property object type
	 * @param isChild the is child
	 * @return the related objects
	 */
	Collection<TO<?>> getRelatedObjects(TO<?> cellObject, String propertyObjectType, boolean isChild);

	/**
	 * Gets the object id.
	 *
	 * @param object the object
	 * @return the object id
	 */
	TO<?> getObjectId(Member object);

	/**
	 * Gets the descendant objects.
	 *
	 * @param objectType the object type
	 * @param parent the parent
	 * @return the descendant objects
	 */
	List<TO<?>> getDescendantObjects(String objectType, Member parent);

	/**
	 * Gets the descendant objects.
	 *
	 * @param objectType the object type
	 * @param parent the parent
	 * @param filter the filter
	 * @return the descendant objects
	 */
	List<TO<?>> getDescendantObjects(String objectType, Member parent, Filter filter);

	/**
	 * Gets the environment.
	 *
	 * @return the environment
	 */
	Environment getEnvironment();

}
