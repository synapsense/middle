package com.synapsense.service.reporting.olap.custom;

import java.io.Serializable;
import java.util.List;

import com.synapsense.service.reporting.olap.BaseHierarchy;
import com.synapsense.service.reporting.olap.Member;
import com.synapsense.util.CollectionUtils;

public class PropertyHierarchy extends BaseHierarchy implements Serializable{

	private static final long serialVersionUID = -6669736760744477141L;

	public PropertyHierarchy() {
		super(Cube.DIM_PROPERTY, String.class, Cube.LEVEL_PROPERTY);
	}
	
	@Override
	public List<Member> getSimpleMembers(Member member) {
	
		List<Member> simpleMembers = CollectionUtils.newList();
		if (member.getCustomLevel().getMembersRange() == 1) {
			simpleMembers.add(member);
			return simpleMembers;
		} else {
			return member.getSimpleMembers();
		}
		
	}

    public static class ReportingPropertyName {

        private String propertyName;
        private String objectTypeName;
        private String propertyAggregationType;

        public ReportingPropertyName(String fullPropertyName) {
           String[] propertyArray = fullPropertyName.split("\\.");
            this.objectTypeName = propertyArray[0];
            this.propertyName = propertyArray[1];
            if (propertyArray.length == 3) {
                this.propertyAggregationType = propertyArray[2];
            }
        }

        public String getPropertyName() {
            return propertyName;
        }

        public String getObjectTypeName() {
            return objectTypeName;
        }

        public String getPropertyAggregationType() {
            return propertyAggregationType;
        }

    }

}
