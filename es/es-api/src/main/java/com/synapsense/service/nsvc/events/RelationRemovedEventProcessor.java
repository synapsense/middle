package com.synapsense.service.nsvc.events;

public interface RelationRemovedEventProcessor {

	RelationRemovedEvent process(RelationRemovedEvent e);

}
