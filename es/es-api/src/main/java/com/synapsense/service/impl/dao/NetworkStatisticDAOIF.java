package com.synapsense.service.impl.dao;

import java.util.Date;

import com.synapsense.dto.ValueTO;

public interface NetworkStatisticDAOIF<KEY_TYPE, DATA_CLASS> extends DataObjectDAO<KEY_TYPE, DATA_CLASS> {
	public Double getAverageSlotCount(Integer id, Date startDate, Date endDate);
	public ValueTO getLatestData(Integer id);
}
