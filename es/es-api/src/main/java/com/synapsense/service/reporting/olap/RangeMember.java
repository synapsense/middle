package com.synapsense.service.reporting.olap;

import java.util.Arrays;
import java.util.List;

import com.synapsense.service.reporting.olap.custom.BaseLevel;
import com.synapsense.service.reporting.olap.custom.CustomLevel;
import com.synapsense.service.reporting.olap.custom.Level;
import com.synapsense.util.CollectionUtils;

public class RangeMember extends SimpleMember {

	private static final long serialVersionUID = -4619650226080002789L;

	private Level customLevel;
	
	private List<Member> simpleMembers = CollectionUtils.newList();
	
	public static final int START_POSITION = 0;
	public static final int END_POSITION = 1;
	
	public RangeMember(Level level, int key) {
		this(level, key, null);
		this.customLevel = level;
	}
	
	public RangeMember(Level level, int key, Member parent) {
		super(level.getHierarchy(), level.getName(), key, parent);
		this.customLevel = level;
	}
	
	public RangeMember(RangeMember other) {
		this(other.getCustomLevel(), (int)other.getKey(), other.getParent());
	}
	
	public RangeMember(SimpleMember... members) {
		super(Arrays.asList(members).iterator().next());
		simpleMembers.addAll(Arrays.asList(members));
		Member firstMember = simpleMembers.iterator().next();
		customLevel = new CustomLevel(firstMember.getHierarchy(), firstMember.getLevel(), simpleMembers.size());
	}
	
	public Level getCustomLevel() {
		return customLevel;
	}
	
	@Override
	public String toString() {
		String str = "[" + customLevel.getMembersRange() + "." + getLevel() + "].[&" + getKey() + "]";
		return getParent() == null ? "[" + getHierarchy().getName() + "]." + str : getParent().toString() + "." + str;
	}

	@Override
	public List<Tuple> getTuples() {
		List<Tuple> list = CollectionUtils.newList();
		list.add(this);
		return list;
	}
	
	@Override
	public void buildQuery(PlanProcessor planProcessor) {
		planProcessor.processSet(this);
	}
	
	@Override
	public Member merge(Member other) {

		if (other == null) {
			return new RangeMember(this);
		}

		if (!getHierarchy().isDescendant(getLevel(), other.getLevel())) {
			return other.merge(this);
		}

		if (getLevel().equals(other.getLevel())) {
			if (!other.getKey().equals(getKey())) {
				return null;
			}
			return merge(other.getParent());
		}

		Member mergedParent = getParent() == null ? other : getParent().merge(other);
		return mergedParent == null ? null : new RangeMember(this).setParent(mergedParent);
	}
	
	@Override
	public List<Member> getSimpleMembers() {
		if (simpleMembers.isEmpty()) {
			simpleMembers = getHierarchy().getSimpleMembers(this);
		}
		return simpleMembers;
	}
	
}
