package com.synapsense.service.reporting.olap.filters;

import java.util.Map;

import com.synapsense.service.reporting.olap.Filter;


public class AndFilter extends ListFilter {

	private static final long serialVersionUID = 3173661159851833653L;

	public AndFilter(Filter... filters) {
		super(filters);
	}

	@Override
	public boolean accept(Map<String, Object> values) {
		for (Filter filter : filters) {
			if (!filter.accept(values)) {
				return false;
			}
		}
		return true;
	}

}
