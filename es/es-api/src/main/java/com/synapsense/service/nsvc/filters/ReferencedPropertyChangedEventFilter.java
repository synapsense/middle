package com.synapsense.service.nsvc.filters;

import java.io.Serializable;

import com.synapsense.dto.TO;
import com.synapsense.service.nsvc.events.ReferencedPropertyChangedEvent;
import com.synapsense.service.nsvc.events.ReferencedPropertyChangedEventProcessor;

/**
 * Class for filtering {@link ReferencedPropertyChangedEvent}
 * 
 * @author Aleksey Shabanov
 * 
 */
public class ReferencedPropertyChangedEventFilter implements ReferencedPropertyChangedEventProcessor, Serializable {

    private static final long serialVersionUID = -6970729110095887286L;
    
	private final TOMatcher referencingToMatcher, referencedToMatcher;
	private final String referencingProperty;
	private final String referencedProperty;

	/**
	 * Simplified filter constructor.
	 * For those who wants to  
	 * 
	 * @see #ReferencedPropertyChangedEventFilter(TOMatcher, String, TOMatcher,
	 *      String)
	 */
	public ReferencedPropertyChangedEventFilter(TOMatcher referencingToMatcher, String referencingProperty) {
		this(referencingToMatcher, referencingProperty, null, null);
	}

	/**
	 * Create filter for {@link ReferencedPropertyChangedEvent}
	 * 
	 * @param referencingToMatcher
	 *            TO matcher for referencing object's TO (not null)
	 * @param referencingProperty
	 *            referencing property name filter (null means any referencing
	 *            property)
	 * @param referencedToMatcher
	 *            TO matcher for referencing object's TO (null means any TO or
	 *            type)
	 * @param referencedProperty
	 *            referenced property name filter (null means any referenced
	 *            property)
	 * @throws  IllegalArgumentException if {@link #referencingToMatcher} is null       
	 */
	public ReferencedPropertyChangedEventFilter(TOMatcher referencingToMatcher, String referencingProperty,
	        TOMatcher referencedToMatcher, String referencedProperty) {
		if (referencingToMatcher == null) {
			throw new IllegalArgumentException("referencingToMatcher must not be null");
		}
		this.referencingToMatcher = referencingToMatcher;
		this.referencingProperty = referencingProperty;
		this.referencedToMatcher = referencedToMatcher;
		this.referencedProperty = referencedProperty;
	}

	@Override
	public ReferencedPropertyChangedEvent process(ReferencedPropertyChangedEvent e) {
		boolean passed = matchReferencingSide(e.getReferencingObj(), e.getReferencingPropName());
		passed = passed && matchReferencedSide(e.getReferencedObj(), e.getReferencedPropName());

		if (!passed)
			return null;

		return new ReferencedPropertyChangedEvent(e.getReferencingObj(), e.getReferencedObj(),
		        e.getReferencingPropName(), e.getReferencedVal());
	}

	private boolean matchReferencingSide(TO<?> to, String propName) {
		return referencingToMatcher.match(to) && (referencingProperty == null || referencingProperty.equals(propName));
	}

	private boolean matchReferencedSide(TO<?> to, String propName) {
		boolean toIsMatching = referencedToMatcher==null || referencedToMatcher.match(to); 
		return toIsMatching && (referencedProperty == null || referencedProperty.equals(propName));
	}

}
