package com.synapsense.service;

import java.io.IOException;

import com.synapsense.dto.PropertyDescr;

/**
 * @author anechaev
 *
 */
/**
 * @author anechaev
 * 
 */
public interface Mapper {
	public enum Access {
	NONE, // means that the property is not mapped
	READ, // means that the property is mapped only for reading (reads come to
	      // the DM, writes are not);
	WRITE, // means that the property is mapped only for writing
	READWRITE // means that the property is mapped both for reading and writting
	}

	/**
	 * Check if the property is mapped. Returns NONE if there is no mapping
	 * 
	 * @param typeName
	 * @param propertyName
	 * @return mapping status
	 */
	Access getAccess(String typeName, String propertyName);

	/**
	 * Check if the property is mapped
	 * 
	 * @param pd
	 * @return
	 */
	Access getAccess(PropertyDescr pd);

	/**
	 * Sets the mapping status of the property. NONE access means that mapping
	 * is removed MappedName becomes equal to the property name Removes the
	 * previous mapping if it was set.
	 * 
	 * @param typeName
	 * @param propertyName
	 * @param access
	 */
	void setAccess(String typeName, String propertyName, Access access);

	/**
	 * Sets the mapping status of the property. NONE access means that mapping
	 * is removed. MappedName becomes equal to the property name. Removes the
	 * previous mapping if it was set.
	 * 
	 * @param pd
	 * @param access
	 */
	void setAccess(PropertyDescr pd, Access access);

	/**
	 * Get the mapped property name
	 * 
	 * @param typeName
	 * @param propertyName
	 * @return null if the property is not mapped
	 */
	String getMappedName(String typeName, String propertyName);

	/**
	 * @param pd
	 * @return
	 */
	String getMappedName(PropertyDescr pd);

	/**
	 * Sets both the mapped name and access for the property. Access should not
	 * be NONE. Removes the previous mapping if it was set.
	 * 
	 * @param typeName
	 * @param propertyName
	 * @param mappedName
	 * @param access
	 * @throws IllegalArgumentException
	 *             if the access equals NONE
	 */
	void setMapping(String typeName, String propertyName, String mappedName, Access access);

	/**
	 * Sets both the mapped name and access for the property. Access should not
	 * be NONE. Removes the previous mapping if it was set.
	 * 
	 * @param pd
	 * @param mappedName
	 * @param access
	 */
	void setMapping(PropertyDescr pd, String mappedName, Access access);

	/**
	 * removes the mapping
	 * 
	 * @param typeName
	 * @param propertyName
	 */
	void removeMapping(String typeName, String propertyName);

	/**
	 * removes the mapping
	 * 
	 * @param pd
	 */
	void removeMapping(PropertyDescr pd);

	/**
	 * @return mapper's auto flush status. see <code>setAutoFlush</code> for
	 *         details
	 */
	boolean getAutoFlush();

	/**
	 * Sets the auto flush status. If the auto flush is true, the service will
	 * store the data in the external file during each setXXX call. If the auto
	 * flush is false, the data will be stored only when <code>flush()</code>
	 * method is called
	 * 
	 * @param state
	 */
	void setAutoFlush(boolean state);

	void flush() throws IOException;

	/**
	 * @param typeName
	 * @param propertyName
	 * @return true if the mapping for the property is set either to READ or
	 *         READWRITE
	 */
	boolean isReadable(String typeName, String propertyName);

	boolean isReadable(PropertyDescr pd);

	boolean isWritable(String typeName, String propertyName);

	boolean isWritable(PropertyDescr pd);
}
