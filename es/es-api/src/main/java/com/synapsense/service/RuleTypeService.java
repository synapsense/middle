package com.synapsense.service;

import java.util.Collection;

import com.synapsense.dto.RuleType;
import com.synapsense.dto.TO;
import com.synapsense.exception.ObjectExistsException;
import com.synapsense.exception.ObjectNotFoundException;

/**
 * Interface for working with rule type entity
 * 
 * @author shabanov
 */
public interface RuleTypeService<KEY_TYPE> {
	TO<KEY_TYPE> create(RuleType config) throws ObjectExistsException;

	/**
	 * Allows caller to get rule type ID by the specified name;
	 * 
	 * @param name
	 *            Rule type name
	 * @return ID of rule type or <tt>null</tt> if no rule instance with the
	 *         specified <code>name</code>
	 */
	TO<KEY_TYPE> getRuleTypeByName(String name);

	RuleType getRuleTypeDTOByName(String name);

	/**
	 * Allows caller to get rule type ID by primary key
	 * 
	 * @param id
	 *            PK
	 * @return ID of rule type or null if no rule type with the specified
	 *         <code>id</code>
	 */
	TO<KEY_TYPE> getRuleTypeByID(KEY_TYPE id);

	/**
	 * Allows caller to get the name of the source class
	 * 
	 * @param id
	 *            PK
	 * @return Name of rule type
	 * @throws ObjectNotFoundException
	 *             if there is no rule type with the specified <code>id</code>
	 */
	String getName(KEY_TYPE id) throws ObjectNotFoundException;

	/**
	 * Allows caller to set new name for the specified rule type.
	 * 
	 * @param id
	 *            PK
	 * @param name
	 *            New rule type name
	 * @throws ObjectNotFoundException
	 *             if there is no rule type with the specified <code>id</code>
	 * @throws ObjectExistsException
	 *             if rule type with the specified <code>name</code> already
	 *             exists
	 */
	void setName(KEY_TYPE id, String name) throws ObjectNotFoundException, ObjectExistsException;

	/**
	 * Allows caller to get rule script language type
	 * 
	 * @param id
	 *            PK
	 * @return Source type name
	 * @throws ObjectNotFoundException
	 *             if there is no rule type with the specified <code>id</code>
	 */
	String getSourceType(KEY_TYPE id) throws ObjectNotFoundException;

	void setSourceType(KEY_TYPE id, String sourceType) throws ObjectNotFoundException;

	/**
	 * Allows caller to get rule type source code.
	 * 
	 * @param id
	 *            PK
	 * @return Source code of rule type
	 * @throws ObjectNotFoundException
	 *             if there is no rule type with the specified <code>id</code>
	 */
	String getSource(KEY_TYPE id) throws ObjectNotFoundException;

	/**
	 * Allows caller to set new source code.
	 * 
	 * @param id
	 *            PK
	 * @param source
	 *            New source code
	 * @throws ObjectNotFoundException
	 *             if there is no rule type with the specified <code>id</code>
	 */
	void setSource(KEY_TYPE id, String source) throws ObjectNotFoundException;

	/**
	 * Allows caller to check rule is pre-compiled.
	 * 
	 * @param id
	 *            PK
	 * @return True is rule type is pre-compiled or false if it is not.
	 * @throws ObjectNotFoundException
	 *             if there is no rule type with the specified <code>id</code>
	 */
	Boolean isPrecompiled(KEY_TYPE id) throws ObjectNotFoundException;

	/**
	 * Allows caller to set precompiled flag for the specified rule type.
	 * 
	 * @param id
	 *            PK
	 * @param isPrecompiled
	 *            new flag
	 * @throws ObjectNotFoundException
	 *             if there is no rule type with the specified <code>id</code>
	 */
	void setPrecompiled(KEY_TYPE id, boolean isPrecompiled) throws ObjectNotFoundException;

	/**
	 * Allows to get rule type run config. The resurning string has cron format
	 * and defines the trigger by which the rule will be fired.
	 * 
	 * @param id
	 *            PK
	 * @return Cron formatted string
	 * @throws ObjectNotFoundException
	 *             if there is no rule type with the specified <code>id</code>
	 */
	String getRuleRunConig(KEY_TYPE id) throws ObjectNotFoundException;

	/**
	 * Allows caller to set new run config. The new config is not checked for
	 * correctness. It will be checked by sheduler service.
	 * 
	 * @param id
	 *            PK
	 * @param config
	 *            New cron formatted string
	 * @throws ObjectNotFoundException
	 *             if there is no rule type with the specified <code>id</code>
	 */
	void setRuleRunConig(KEY_TYPE id, String config) throws ObjectNotFoundException;

	/**
	 * Allows caller to get all existing rule types.
	 * 
	 * @return Collection of rule type IDs
	 */
	Collection<TO<KEY_TYPE>> getAllRuleTypes();

	/**
	 * Allows caller to get all existing rule types as DTO<br>
	 * Returned <code>RuleType</code>s already have all their state<br>
	 * without needs to additional calls to database
	 * 
	 * @return All existing rule types as <code>RuleType</code> instances
	 */
	Collection<RuleType> getAllRuleTypesDTOs();

	/**
	 * Allows caller to remove the specified rule type.
	 * 
	 * @param id
	 *            PK
	 */
	void delete(KEY_TYPE id);

	/**
	 * Allows caller to remove all existing rule types. Can be applied when you
	 * need to clear the system , before seeding new config.
	 */
	void deleteAll();
}
