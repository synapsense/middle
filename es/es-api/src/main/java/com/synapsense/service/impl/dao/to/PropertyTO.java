package com.synapsense.service.impl.dao.to;

import java.io.Serializable;

import com.synapsense.dto.TO;

/**
 * This class is a implementation of link to property concept
 * 
 * @author shabanov
 * 
 */
public class PropertyTO implements Serializable {

	private static final long serialVersionUID = 510052649880330865L;

	private TO<?> objId;

	private String propertyName;

	public PropertyTO() {
	}

	public PropertyTO(TO<?> objId, String propertyName) {
		this.setObjId(objId);
		this.setPropertyName(propertyName);
	}

	public TO<?> getObjId() {
		return objId;
	}

	public void setObjId(TO<?> objId) {
		if (objId == null) {
			throw new IllegalArgumentException("Supplied 'objId' is null!");
		}
		this.objId = objId;
	}

	public String getPropertyName() {
		return propertyName;
	}

	public void setPropertyName(String propertyName) {
		if (propertyName == null || propertyName.isEmpty()) {
			throw new IllegalArgumentException("Supplied 'propertyName' is null or empty!");
		}
		this.propertyName = propertyName;
	}

	@Override
	public String toString() {
		return this.objId + ":" + this.propertyName;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((objId == null) ? 0 : objId.hashCode());
		result = prime * result + ((propertyName == null) ? 0 : propertyName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PropertyTO other = (PropertyTO) obj;
		if (objId == null) {
			if (other.objId != null)
				return false;
		} else if (!objId.equals(other.objId))
			return false;
		if (propertyName == null) {
			if (other.propertyName != null)
				return false;
		} else if (!propertyName.equals(other.propertyName))
			return false;
		return true;
	}
}
