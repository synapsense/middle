package com.synapsense.service.impl.messages.base;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class SandcrawlerConfData implements Serializable {

	private static final long serialVersionUID = 2036486488053781763L;

	private long checksum;
	private String program;
	private ArrayList<PlugConfData> plugData = new ArrayList<PlugConfData>();
	private ArrayList<ServerConfData> serverData = new ArrayList<ServerConfData>();
	private ArrayList<RackConfData> rackData = new ArrayList<RackConfData>();

	public void addPlugConfData(PlugConfData data) {
		plugData.add(data);
	}

	public void addServerConfData(ServerConfData data) {
		serverData.add(data);
	}

	public void addRackConfData(RackConfData data) {
		rackData.add(data);
	}

	public long getChecksum() {
		return checksum;
	}

	public List<PlugConfData> getPlugData() {
		return plugData;
	}

	public String getProgram() {
		return program;
	}

	public List<ServerConfData> getServerData() {
		return serverData;
	}

	public ArrayList<RackConfData> getRackData() {
		return rackData;
	}

	public void setChecksum(long checksum) {
		this.checksum = checksum;
	}

	public void setProgram(String program) {
		this.program = program;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (checksum ^ (checksum >>> 32));
		result = prime * result + ((plugData == null) ? 0 : plugData.hashCode());
		result = prime * result + ((program == null) ? 0 : program.hashCode());
		result = prime * result + ((serverData == null) ? 0 : serverData.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SandcrawlerConfData other = (SandcrawlerConfData) obj;
		if (checksum != other.checksum)
			return false;
		if (plugData == null) {
			if (other.plugData != null)
				return false;
		} else if (!plugData.equals(other.plugData))
			return false;
		if (program == null) {
			if (other.program != null)
				return false;
		} else if (!program.equals(other.program))
			return false;
		if (serverData == null) {
			if (other.serverData != null)
				return false;
		} else if (!serverData.equals(other.serverData))
			return false;
		return true;
	}
}
