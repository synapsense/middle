package com.synapsense.service.impl.messages;

import java.io.Serializable;

public class VirtualSensorMessage implements BusMessage<String, VirtualSensorMessage.VirtualSensorID> {
	public static class VirtualSensorID implements Serializable {
		private static final long serialVersionUID = 5797502176832139814L;
		private String deviceID;
		private String propertyPath;

		public VirtualSensorID(String deviceID, String propertyPath) {
			this.deviceID = deviceID;
			this.propertyPath = propertyPath;
		}

		public String getDeviceID() {
			return deviceID;
		}

		public String getPropertyPath() {
			return propertyPath;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((deviceID == null) ? 0 : deviceID.hashCode());
			result = prime * result + ((propertyPath == null) ? 0 : propertyPath.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			final VirtualSensorID other = (VirtualSensorID) obj;
			if (deviceID == null) {
				if (other.deviceID != null)
					return false;
			} else if (!deviceID.equals(other.deviceID))
				return false;
			if (propertyPath == null) {
				if (other.propertyPath != null)
					return false;
			} else if (!propertyPath.equals(other.propertyPath))
				return false;
			return true;
		}

		public void setDeviceID(String deviceID) {
			this.deviceID = deviceID;
		}

		public void setPropertyPath(String propertyPath) {
			this.propertyPath = propertyPath;
		}
	};

	private static final long serialVersionUID = 4774183352675416588L;

	private VirtualSensorID id;
	private String data;

	public VirtualSensorMessage(String deviceID, String propertyPath, String data) {
		this.id = new VirtualSensorID(deviceID, propertyPath);
		this.data = data;
	}

	@Override
	public VirtualSensorMessage.VirtualSensorID getId() {
		return id;
	}

	@Override
	public Type getType() {
		return Type.TPD;
	}

	@Override
	public String getValue() {
		return data;
	}

	@Override
	public void setId(VirtualSensorMessage.VirtualSensorID id) {
		this.id = id;
	}

	@Override
	public void setValue(String value) {
		data = value;
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer(20);
		sb.append("Id=");
		sb.append(id);
		sb.append("\n");

		sb.append("message=");
		sb.append(data);
		sb.append("\n");

		return sb.toString();
	}
}
