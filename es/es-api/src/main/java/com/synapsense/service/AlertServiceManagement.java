package com.synapsense.service;

/**
 * Alert service management interface.
 * 
 * @author shabanov
 * 
 */
public interface AlertServiceManagement {
	/**
	 * Life cycle method
	 * 
	 * @throws Exception
	 *             Indicates that some errors have occurred during its
	 *             execution.
	 */
	public void create() throws Exception;

	/**
	 * Life cycle method
	 * 
	 * @throws Exception
	 *             Indicates that some errors have occurred during its
	 *             execution.
	 */
	public void destroy() throws Exception;

	/**
	 * Life cycle method
	 * 
	 * @throws Exception
	 *             Indicates that some errors have occurred during its
	 *             execution.
	 */
	public void start() throws Exception;

	/**
	 * Life cycle method
	 * 
	 * @throws Exception
	 *             Indicates that some errors have occurred during its
	 *             execution.
	 */
	public void stop() throws Exception;

	/**
	 * Allows caller to show all active alerts.
	 * 
	 * @return String which contains all active alerts info.
	 */
	public String showActiveAlerts();

}
