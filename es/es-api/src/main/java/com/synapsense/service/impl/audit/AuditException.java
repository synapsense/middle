package com.synapsense.service.impl.audit;

import java.util.Collection;
import java.util.Collections;

import com.synapsense.dto.TO;
import com.synapsense.exception.EnvException;

public class AuditException extends EnvException {

	private static final long serialVersionUID = 7923111759678180787L;

	private Collection<AuditRecord> records = Collections.emptyList();

	public AuditException() {
		super();
	}

	public AuditException(String message, Throwable cause) {
		super(message, cause);
	}

	public AuditException(String message) {
		super(message);
	}

	public AuditException(Throwable cause) {
		super(cause);
	}

	public AuditException(String message, Collection<AuditRecord> records) {
		super(message);
		if (records != null)
			this.records = records;
	}

	public Collection<AuditRecord> getRecords() {
		return records;
	}

	public static class AuditRecord {
		public TO<?> objId;
		public String details;
	}
}
