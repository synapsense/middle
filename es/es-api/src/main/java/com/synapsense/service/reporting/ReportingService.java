package com.synapsense.service.reporting;

import com.synapsense.service.reporting.dto.TableTO;

/**
 * Reporting Service for getting historical environment data
 * 
 * @author Grigoriy Ryabov
 * 
 */
public interface ReportingService {

	/**
	 * Executes passed dimensional query and returns result table
	 * 
	 * @param query
	 *            query to execute

	 * @return iterable result table
	 */
	TableTO executeQuery(DimensionalQuery query);

}
