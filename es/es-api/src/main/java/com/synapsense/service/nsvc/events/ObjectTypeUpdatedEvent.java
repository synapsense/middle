package com.synapsense.service.nsvc.events;

import java.util.Collections;
import java.util.Set;

import com.synapsense.dto.PropertyDescr;
import com.synapsense.service.nsvc.Event;
import com.synapsense.service.nsvc.EventDispatcher;

public class ObjectTypeUpdatedEvent implements Event {

	private static final long serialVersionUID = 7598728336665947410L;

	private final String typeName;
	private final Set<PropertyDescr> addedProperties;
	private final Set<String> removedProperties;

	public ObjectTypeUpdatedEvent(String typeName, Set<PropertyDescr> addedProperties, Set<String> removedProperties) {
		if (typeName == null)
			throw new IllegalArgumentException("Argument typeName cannot be null");
		this.typeName = typeName;

		if (addedProperties != null) {
			this.addedProperties = addedProperties;
		} else {
			this.addedProperties = Collections.emptySet();
		}

		if (removedProperties != null) {
			this.removedProperties = removedProperties;
		} else {
			this.removedProperties = Collections.emptySet();
		}
	}

	public String getTypeName() {
		return typeName;
	}

	public Set<PropertyDescr> getAddedProperties() {
		return addedProperties;
	}

	public Set<String> getRemovedProperties() {
		return removedProperties;
	}

	@Override
	public Event dispatch(EventDispatcher ed) {
		return ed.dispath(this);
	}

}
