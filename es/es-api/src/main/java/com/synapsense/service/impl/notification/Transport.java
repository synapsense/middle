package com.synapsense.service.impl.notification;

import com.synapsense.dto.Message;

public interface Transport {
	boolean isValid(Message message);

	void send(Message message) throws NotificationException;

    String getName();
}