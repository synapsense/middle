package com.synapsense.service.nsvc.filters;

import java.io.Serializable;
import java.util.regex.Pattern;

import com.synapsense.service.nsvc.events.ObjectTypeDeletedEvent;
import com.synapsense.service.nsvc.events.ObjectTypeDeletedEventProcessor;

public class ObjectTypeDeletedEventFilter implements ObjectTypeDeletedEventProcessor, Serializable {

	private static final long serialVersionUID = -1618495030783709327L;

	private final Pattern typeNamePattern;

	public ObjectTypeDeletedEventFilter(String typeNamePattern) {
		this.typeNamePattern = Pattern.compile(typeNamePattern);
	}

	@Override
	public ObjectTypeDeletedEvent process(ObjectTypeDeletedEvent e) {
		return typeNamePattern.matcher(e.getTypeName()).matches() ? e : null;
	}

}
