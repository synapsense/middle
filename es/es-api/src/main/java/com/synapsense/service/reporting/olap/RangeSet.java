package com.synapsense.service.reporting.olap;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import com.synapsense.exception.IllegalInputParameterException;
import com.synapsense.service.reporting.exception.ReportingRuntimeException;
import com.synapsense.service.reporting.olap.custom.BaseLevel;
import com.synapsense.service.reporting.olap.custom.Cube;
import com.synapsense.service.reporting.olap.custom.Level;
import com.synapsense.service.reporting.olap.custom.TimeHierarchy;
import com.synapsense.service.reporting.olap.custom.TimePeriodType;
import com.synapsense.service.reporting.olap.custom.WeekLevel;
import com.synapsense.util.CollectionUtils;
import com.synapsense.util.Pair;

public class RangeSet implements AxisSet, Serializable {

	private static final long serialVersionUID = 3603478235859079601L;
	
	private static final String CUSTOM_PERIOD = "Custom";
	private static final String PREDEFINED_PERIOD = "Predefined";
	private static final String TIME_SELECTOR_TYPE = "selectorType";
	private static final String TIME_SELECTOR_VALUE = "selectorValue";

	private Member startMember;

	private Member endMember;

	private OrderedHierarchy hierarchy;
	
	private String json;
	//For Reporting Engine aggregation the least used level is Cube.LEVEL_HOUR
	private String aggregationRange = Cube.LEVEL_HOUR;

	public RangeSet(OrderedHierarchy hierarchy, Member startMember, Member endMember) {
		if (!startMember.isBrother(endMember)) {
			throw new IllegalArgumentException("start and end should have the same hierarchy and level: " + startMember
					+ " " + endMember);
		}
		this.hierarchy = hierarchy;
		this.startMember = startMember;
		this.endMember = endMember;
	}
	
	public RangeSet(OrderedHierarchy hierarchy, Date startDate, Date endDate) {

		Level level = null;
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(startDate);
		
		Member start = null;
		Member end = null;
		
		if (hierarchy.equals(Cube.TIME)) {
			if (calendar.get(Calendar.HOUR_OF_DAY) == 0
					&& calendar.get(Calendar.MINUTE) == 0
					&& calendar.get(Calendar.SECOND) == 0) {
				level = new BaseLevel(hierarchy, Cube.LEVEL_DAY);
			} else {
				level = new BaseLevel(hierarchy, Cube.LEVEL_ONE_MINUTE);
			}
		} else {
			level = new WeekLevel();
		}
		start = hierarchy.getMemberOfCalendar(calendar, level, new BaseLevel(hierarchy, Cube.LEVEL_YEAR));
		calendar.setTime(endDate);
		end = hierarchy.getMemberOfCalendar(calendar, level, new BaseLevel(hierarchy, Cube.LEVEL_YEAR));
		if (!start.isBrother(end)) {
			throw new IllegalArgumentException("Start and end should have the same hierarchy and level: " + start
					+ " " + end);
		}		
		
		this.startMember = start;
		this.endMember = end;
		
		this.hierarchy = Cube.getOrderedHierarchy(startMember.getHierarchy().getName());

	}
	
	public RangeSet(OrderedHierarchy hierarchy, int startDay, int startMonth, int startYear,
			int endDay, int endMonth, int endYear) {
		this.hierarchy = hierarchy;
		this.startMember = new SimpleMember(hierarchy, Cube.LEVEL_DAY,
				startDay, new SimpleMember(hierarchy, Cube.LEVEL_MONTH,
						startMonth, new SimpleMember(hierarchy,
								Cube.LEVEL_YEAR, startYear)));
		this.endMember = new SimpleMember(hierarchy, Cube.LEVEL_DAY,
				endDay, new SimpleMember(hierarchy, Cube.LEVEL_MONTH,
						endMonth, new SimpleMember(hierarchy,
								Cube.LEVEL_YEAR, endYear)));
		if (!startMember.isBrother(endMember)) {
			throw new IllegalArgumentException("start and end should have the same hierarchy and level: " + startMember
					+ " " + endMember);
		}
	}


	public RangeSet(OrderedHierarchy hierarchy, String previousPeriod) {
		this.hierarchy = hierarchy;
		Calendar calendar = Calendar.getInstance();
		Member member = hierarchy.getMemberOfCalendar(calendar, new BaseLevel(
				hierarchy, previousPeriod), new BaseLevel(Cube.TIME,
				Cube.LEVEL_YEAR));
		Member prevMember = hierarchy.getPrevious(member);
		this.startMember = prevMember;
		this.endMember = prevMember;
	}

	public RangeSet(OrderedHierarchy hierarchy, TimePeriodType periodType) {
		
		String selectorType = "Predefined";
		String selectorValue = String.valueOf(periodType.getId());
		json = fillJson(selectorType, selectorValue);
		Pair<Member, Member> range = hierarchy.getPeriod(periodType);
		this.startMember = range.getFirst();
		this.hierarchy = Cube.getOrderedHierarchy(startMember.getHierarchy().getName());
		this.endMember = range.getSecond();
	}


	public RangeSet(OrderedHierarchy hierarchy, int value, String levelName) {
		String selectorType = "Custom";
		String selectorValue = String.valueOf(value) + ":" + UILevelNameType.getTypeByName(levelName).getUiLevelName();
		json = fillJson(selectorType, selectorValue);

		this.hierarchy = hierarchy;
		Calendar calendar = Calendar.getInstance();
		Level secondsLevel = new BaseLevel(Cube.TIME, Cube.LEVEL_SECOND);
		this.endMember = Cube.TIME.getMemberOfCalendar(calendar, secondsLevel, new BaseLevel(Cube.TIME, Cube.LEVEL_YEAR));
		
		calendar.add(TimeHierarchy.getCalendarIndex(levelName), -value);
		
		this.startMember = Cube.TIME.getMemberOfCalendar(calendar, secondsLevel, new BaseLevel(Cube.TIME, Cube.LEVEL_YEAR));
	}
	
	private static String fillJson(String selectorType, String selectorValue) {
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put("selectorValue", selectorValue);
			jsonObject.put("selectorType", selectorType);
		} catch (JSONException e) {
			throw new ReportingRuntimeException("Error in building RangeSet toString: ", e);
		}
		return jsonObject.toString();
	}
	
	public static RangeSet newRangeSet(JSONObject jsonObject) {
		try {
			switch(jsonObject.getString(TIME_SELECTOR_TYPE)) {
			case CUSTOM_PERIOD:
				String[] timeSelectorValue = jsonObject.getString(TIME_SELECTOR_VALUE).split(":");
				Level level = UILevelNameType.getTypeByLevelName(timeSelectorValue[1]).getLevel();
				
				return newCustomRangeSet(Cube.getOrderedHierarchy(level.getHierarchy().getName())
						, Integer.valueOf(timeSelectorValue[0]), level.getName());
			case PREDEFINED_PERIOD:
				TimePeriodType timePeriod = TimePeriodType.getTypeById(Integer.valueOf(jsonObject.getString(TIME_SELECTOR_VALUE)));
				return new RangeSet(Cube.getOrderedHierarchy(timePeriod.getLevel().getHierarchy().getName())
						, timePeriod);
			}
		} catch (JSONException e) {
			throw new IllegalInputParameterException("JSON Object is not correct: " + jsonObject);
		}
		return null;
	}
	
	public static RangeSet newCustomRangeSet(OrderedHierarchy hierarchy, int value, String levelName) {
		
		String selectorType = "Custom";
		String selectorValue = String.valueOf(value) + ":" + UILevelNameType.getTypeByName(levelName).getUiLevelName();
		
		Calendar calendar = Calendar.getInstance();
		Date endDate = calendar.getTime();
		
		Member timeMember = hierarchy.getMemberOfCalendar(calendar, new BaseLevel(hierarchy, levelName), new BaseLevel(hierarchy, Cube.LEVEL_YEAR));
		calendar = hierarchy.getCalendarOfMember(timeMember);
		calendar.add(TimeHierarchy.getCalendarIndex(levelName), -value + 1);
		Date startDate = calendar.getTime();
		
		return new RangeSet(hierarchy, startDate, endDate, fillJson(selectorType, selectorValue));		
	}
	
	public RangeSet(OrderedHierarchy hierarchy, Date startDate, Date endDate, String json) {
		
		this(hierarchy, startDate, endDate);

		this.json = json;
	}

	public List<Hierarchy> getDimensionalities() {
		List<Hierarchy> dimensionalities = CollectionUtils.newList();
		dimensionalities.add(hierarchy);
		return dimensionalities;
	}

	@Override
	public String toString() {
		if (this.json == null) {
			return new StringBuilder("{").append(startMember).append(":").append(endMember).append("}").toString();
		} else {
			return json;
		}
	}

	public List<Tuple> getTuples() {
		
		String currentLevel = startMember.getLevel();
		if (aggregationRange.equals(currentLevel)) {
			return new LinkedList<Tuple>(hierarchy.getMembers(startMember, endMember));
		}
		Member start = startMember;
		Member end = endMember;
		OrderedHierarchy aggregationHierarchy = hierarchy;
		
		if (aggregationRange.equals(Cube.LEVEL_WEEK)
				&& hierarchy.equals(Cube.TIME)) {
            start = Cube.WEEK.convert(startMember, hierarchy, aggregationRange);
            end = Cube.WEEK.convert(endMember, hierarchy, aggregationRange);
            aggregationHierarchy = Cube.getOrderedHierarchy(start.getHierarchy().getName());
		} 
		else {
			//If aggregationRange is descendant of current level
			if (hierarchy.isDescendant(aggregationRange, currentLevel)){
                List<Member> startMembers = hierarchy.getMembers(new BaseLevel(hierarchy, aggregationRange), start);

                if (startMembers.isEmpty()) {
                    Calendar startCalendar = Calendar.getInstance();
                    startCalendar.add(TimeHierarchy.getCalendarIndex(Cube.LEVEL_MONTH), -TimeHierarchy.getHistoricalPeriod());
                    Level secondsLevel = new BaseLevel(Cube.TIME, Cube.LEVEL_SECOND);
                    Member startSecondsMember = Cube.TIME.getMemberOfCalendar(startCalendar, secondsLevel, new BaseLevel(Cube.TIME, Cube.LEVEL_YEAR));
                    start = hierarchy.getMemberOfCalendar(Cube.TIME.getCalendarOfMember(startSecondsMember)
                            , new BaseLevel(hierarchy, aggregationRange)
                            , new BaseLevel(Cube.TIME, Cube.LEVEL_YEAR));
                } else {
                    start = startMembers.iterator().next();
                }

                List<Member> allEndMembers = hierarchy.getMembers(new BaseLevel(hierarchy, aggregationRange), end);
				end = allEndMembers.get(allEndMembers.size() - 1);
			}
			//Otherwise if aggregationRange is ancestor of current level
			else {
				if (hierarchy.isDescendant(start.getLevel(), aggregationRange)) {
					while (!start.getLevel().equals(aggregationRange)) {
						start = start.getParent();
						end = end.getParent();
					}
				}
			}
		}
		return new LinkedList<Tuple>(aggregationHierarchy.getMembers(start, end));
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((aggregationRange == null) ? 0 : aggregationRange.hashCode());
		result = prime * result
				+ ((endMember == null) ? 0 : endMember.hashCode());
		result = prime * result
				+ ((hierarchy == null) ? 0 : hierarchy.hashCode());
		result = prime * result + ((json == null) ? 0 : json.hashCode());
		result = prime * result
				+ ((startMember == null) ? 0 : startMember.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		RangeSet other = (RangeSet) obj;
		if (aggregationRange == null) {
			if (other.aggregationRange != null) {
				return false;
			}
		} else if (!aggregationRange.equals(other.aggregationRange)) {
			return false;
		}
		if (endMember == null) {
			if (other.endMember != null) {
				return false;
			}
		} else if (!endMember.equals(other.endMember)) {
			return false;
		}
		if (hierarchy == null) {
			if (other.hierarchy != null) {
				return false;
			}
		} else if (!hierarchy.equals(other.hierarchy)) {
			return false;
		}
		if (json == null) {
			if (other.json != null) {
				return false;
			}
		} else if (!json.equals(other.json)) {
			return false;
		}
		if (startMember == null) {
			if (other.startMember != null) {
				return false;
			}
		} else if (!startMember.equals(other.startMember)) {
			return false;
		}
		return true;
	}
	
	@Override
	public void buildQuery(PlanProcessor planProcessor) {
		planProcessor.processSet(this);
	}

	public void setAggregationRange(String aggregationRange) {
		
		if (aggregationRange.equals(Cube.ALL_RANGE)) {
			this.aggregationRange = Cube.LEVEL_HOUR;
		} else {
			this.aggregationRange = aggregationRange;
		}
		
		Member start = startMember;
		Member end = endMember;
		
		if (hierarchy.equals(Cube.WEEK)) {
			
			if (!aggregationRange.equals(Cube.LEVEL_WEEK)) {
				// Convert WEEK Hierarchy members to TIME Hierarchy members
				start = Cube.TIME.convert(start, hierarchy, this.aggregationRange);

				end = Cube.TIME.getMemberOfCalendar(Calendar.getInstance(),
						new BaseLevel(Cube.TIME, Cube.LEVEL_SECOND),
						new BaseLevel(Cube.TIME, Cube.LEVEL_YEAR));
				if (Cube.TIME.isDescendant(end.getLevel(), this.aggregationRange)) {
					while (!end.getLevel().equals(this.aggregationRange)) {
						end = end.getParent();
					}
				}
			}
		}

			this.startMember = start;
			this.endMember = end;		
			this.hierarchy = Cube.getOrderedHierarchy(startMember
					.getHierarchy().getName());
	}

	public Member getStartMember() {
		return new SimpleMember(startMember.getSimpleMembers().iterator().next());
	}

	public Member getEndMember() {
		return new SimpleMember(endMember.getSimpleMembers().get(endMember.getSimpleMembers().size() - 1));
	}
	
	public enum UILevelNameType {
		HOUR("hours", new BaseLevel(Cube.TIME, Cube.LEVEL_HOUR))
		, DAY("days", new BaseLevel(Cube.TIME, Cube.LEVEL_DAY))
		, WEEK("weeks", new BaseLevel(Cube.WEEK, Cube.LEVEL_WEEK))
		, MONTH("months", new BaseLevel(Cube.TIME, Cube.LEVEL_MONTH));
		
		private String uiLevelName;
		private Level level;

		private UILevelNameType(String uiLevelName, Level level) {
			this.uiLevelName = uiLevelName;
			this.level = level;
		}

		public static UILevelNameType getTypeByLevelName(String levelName) {
			for (UILevelNameType type : UILevelNameType.values()) {
				if (type.getUiLevelName().equals(levelName))
					return type;
			}
			throw new IllegalArgumentException(
					"Not supported Level Name " + levelName);
		}
		
		public static UILevelNameType getTypeByName(String name) {
			for (UILevelNameType type : UILevelNameType.values()) {
				if (type.getLevel().getName().equals(name))
					return type;
			}
			throw new IllegalArgumentException(
					"Not supported Name " + name);
		}
		
		public String getUiLevelName() {
			return uiLevelName;
		}

		public Level getLevel() {
			return level;
		}			
		
	}
	
}
