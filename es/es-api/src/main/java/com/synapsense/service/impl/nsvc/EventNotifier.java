package com.synapsense.service.impl.nsvc;

import com.synapsense.service.nsvc.Event;

public interface EventNotifier {

	void notify(Event e);

}
