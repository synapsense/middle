package com.synapsense.service.nsvc.events;

import com.synapsense.service.nsvc.Event;
import com.synapsense.service.nsvc.EventDispatcher;

/**
 * Should be raised whenever DLImportService finishes ES configuration
 * 
 * @author Dmitry Grudzinskiy
 * 
 */
public class ConfigurationCompleteEvent implements Event {

	private static final long serialVersionUID = 1842573554880792938L;

	@Override
	public Event dispatch(EventDispatcher ed) {
		return ed.dispatch(this);
	}

	@Override
	public String toString() {
		return "ES configuration complete";
	}
}
