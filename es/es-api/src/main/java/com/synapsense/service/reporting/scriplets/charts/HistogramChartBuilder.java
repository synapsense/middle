package com.synapsense.service.reporting.scriplets.charts;

import java.sql.ResultSet;
import java.text.NumberFormat;
import java.util.List;

import org.apache.commons.lang3.ArrayUtils;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.statistics.HistogramDataset;
import com.synapsense.util.CollectionUtils;
import com.synapsense.util.Pair;

public class HistogramChartBuilder extends ChartBuilder {
	
	HistogramDataset dataset = new HistogramDataset();
	
	private String serie_id;
	
	@Override
	public void addDataSerie(String serie_id, ResultSet rs, String seriesName,
			String dateRSfieldName, String valueRSfieldName, String axisName) {
	}

	@Override
	public void addDataSerie(String serie_id, List<SensorPoint> pointArray,
			String seriesName, String axisName, int bins) {
	    
	    List<Double> pointValues = CollectionUtils.newList();
	    for (SensorPoint point : pointArray) {
	    	if (point.getValue() != null) {
	    		pointValues.add(point.getValue().doubleValue());
	    	}
	    }
	    
	    //Get horizontal range
	    double min = 0;
	    double max = 0;
	    if (!pointValues.isEmpty()) {
		    min = pointValues.get(0);
			max = min;
		    for(Double prop: pointValues ){
				min=Math.min(min, prop);
				max=Math.max(max, prop);
			}
	    }
	    
	    double [] values = ArrayUtils.toPrimitive(pointValues.toArray(new Double[pointValues.size()]));
	    dataset.addSeries(seriesName, values, bins, min, max);
		axisNameMap.put(seriesName, axisName);
		this.serie_id = serie_id;
	}

	@Override
	public void addValue(String class_id, String seriesName,
			String categoryName, String axisName, Float value) {
	}
	
	@Override
	protected synchronized JFreeChart createChartType(String chartName){
		
		JFreeChart timeChart = ChartFactory.createHistogram(chartName, // title
				"", // x-axis label
				"Value", // y-axis label
				dataset, // data
				PlotOrientation.VERTICAL,
				false, //create legend later
				true, // generate tool tips
				false // generate URLs
				);
		
		return timeChart;
	}

	@Override
	protected void customizeChart(XYPlot plot) {
	}

	@Override
	public JFreeChart getChart(NumberFormat numberFormat) {
		JFreeChart result = createHistogramChart(dataset, axisNameMap, serie_id, numberFormat);
		return result;	
	}

	@Override
	protected Pair<String, String> getValueAxisPair(String entryKey,
			String currValueAxisName) {
		throw new UnsupportedOperationException();
	}
	
}
