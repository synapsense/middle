package com.synapsense.service.reporting.olap;

import java.util.Collection;
import java.util.Map;

/**
 * Interface for filtering elements of {@link FilterableHierarchy} hierarchies.
 * Can be used for constructing {@link FilterableAllMemberSet} axis set.
 * 
 * @author Dmitry Grudzinskiy
 * 
 */
public interface Filter {

	/**
	 * Properties that this filter needs to be applied to
	 * 
	 * @return list of all properties names that this filter must be applied to
	 */
	Collection<String> getProperties();

	/**
	 * Determines whether passed properties values are acceptable by this filter
	 * 
	 * @param values
	 *            map of all properties values
	 * @return true if the specified properties values are acceptable by this
	 *         filter, false otherwise
	 */
	boolean accept(Map<String, Object> values);
}
