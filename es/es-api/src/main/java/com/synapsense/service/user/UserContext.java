package com.synapsense.service.user;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.util.Locale;

public interface UserContext {
	String getUnitSystem();

	DateFormat getDateFormat();

	NumberFormat getDataFormat();
	
	Locale getLocale();
}
