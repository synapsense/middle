package com.synapsense.service.impl.messages.base;

import java.io.Serializable;

public class NodeVersionData implements Serializable {
	private int appVersion;
	private int appRevision;
	private int nwkVersion;
	private int nwkRevision;
	private int osVersion;
	private int osRevision;

	public int getAppVersion() {
		return appVersion;
	}

	public void setAppVersion(int appVersion) {
		this.appVersion = appVersion;
	}

	public int getAppRevision() {
		return appRevision;
	}

	public void setAppRevision(int appRevision) {
		this.appRevision = appRevision;
	}

	public int getNwkVersion() {
		return nwkVersion;
	}

	public void setNwkVersion(int nwkVersion) {
		this.nwkVersion = nwkVersion;
	}

	public int getNwkRevision() {
		return nwkRevision;
	}

	public void setNwkRevision(int nwkRevision) {
		this.nwkRevision = nwkRevision;
	}

	public int getOsVersion() {
		return osVersion;
	}

	public void setOsVersion(int osVersion) {
		this.osVersion = osVersion;
	}

	public int getOsRevision() {
		return osRevision;
	}

	public void setOsRevision(int osRevision) {
		this.osRevision = osRevision;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + appRevision;
		result = prime * result + appVersion;
		result = prime * result + nwkRevision;
		result = prime * result + nwkVersion;
		result = prime * result + osRevision;
		result = prime * result + osVersion;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final NodeVersionData other = (NodeVersionData) obj;
		if (appRevision != other.appRevision)
			return false;
		if (appVersion != other.appVersion)
			return false;
		if (nwkRevision != other.nwkRevision)
			return false;
		if (nwkVersion != other.nwkVersion)
			return false;
		if (osRevision != other.osRevision)
			return false;
		if (osVersion != other.osVersion)
			return false;
		return true;
	}
}
