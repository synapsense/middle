package com.synapsense.service.reporting.transform;

import com.synapsense.service.reporting.Formula.TransformType;

import java.util.List;

/**
 * Created by Grigory Ryabov on 03.03.14.
 */
public interface TableTransformData {

    public TransformType getTransformType();

   public List<Object> getData();
}
