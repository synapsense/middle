package com.synapsense.service.nsvc.events;

import com.synapsense.dto.ObjectType;
import com.synapsense.service.nsvc.Event;
import com.synapsense.service.nsvc.EventDispatcher;

public class ObjectTypeCreatedEvent implements Event {

	private static final long serialVersionUID = -3701114364412108289L;

	private final ObjectType objectType;

	public ObjectTypeCreatedEvent(ObjectType objectType) {
		if (objectType == null)
			throw new IllegalArgumentException("Argument objectType cannot be null");
		this.objectType = objectType;
	}

	public ObjectType getObjectType() {
		return objectType;
	}

	@Override
	public Event dispatch(EventDispatcher ed) {
		return ed.dispath(this);
	}

}
