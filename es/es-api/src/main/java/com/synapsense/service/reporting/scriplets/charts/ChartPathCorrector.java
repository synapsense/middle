package com.synapsense.service.reporting.scriplets.charts;

import java.io.File;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * If necessary this class adds postfix to the file name and image URL to avoid browser cache.
 * If unnecessary, does nothing. 
 * @author ahorosh
 *
 */
public class ChartPathCorrector {
    private static Log LOG = LogFactory.getLog(ChartPathCorrector.class);
    private static final String UNCACHE_POSTFIX = "_c";
    
    /** Corrected fileName path */
    private String fileName;
    /** Corrected imgURL */
    private String imgUrl;
    
    public ChartPathCorrector(String fileName, String imgUrl){
        try{
            //correct path and URL to avoid cache
            correctAndStore(fileName, imgUrl);
        }catch(Exception e){
            this.fileName = fileName;
            this.imgUrl = imgUrl;
            LOG.error(e.getMessage(), e);
        }
    }
    
    /**
     * If file with name <code>fileName</code> exists, 
     * this function will delete it and create new fileName and imgUrl by
     * adding postfix to its end. Newly created paths will be stored as class members.  
     * @param fileName - origin file name
     * @param imgUrl - img URL
     */
    public void correctAndStore(String fileName, String imgUrl){
        File f = new File(fileName);
        
        //if file already exists, delete it and create file name with postfix instead
        if(f.exists()){
            f.delete();            
            
            int dotIndex = imgUrl.lastIndexOf(".");
            String newimgUrl = imgUrl.substring(0, dotIndex) + UNCACHE_POSTFIX + imgUrl.substring(dotIndex, imgUrl.length());
            
            fileName = fileName.replace(imgUrl, ""); //remove imgUrl from file name
            imgUrl = newimgUrl;
            fileName = fileName + imgUrl; //add new imgUrl to the file name
        }
        
        this.fileName = fileName;
        this.imgUrl = imgUrl;
    }
    
    public String getImgUrl(){
        return imgUrl;
    }
    
    public String getFileName(){
        return fileName;
    }

}
