package com.synapsense.service.reporting.olap;

import java.util.List;


public interface FilterableHierarchy extends MemberHierarchy {

	/**
	 * Finds all members for the specified level, parent and filter
	 * 
	 * @param level
	 *            level from this hierarchy to find members for
	 * @param parent
	 *            parent member that must be from this hierarchy
	 * @param filter
	 * @return all members for the specified level, parent and filter
	 */
	List<Member> getMembers(String level, Member parent, Filter filter);
}
