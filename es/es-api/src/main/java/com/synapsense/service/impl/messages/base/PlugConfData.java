package com.synapsense.service.impl.messages.base;

import java.io.Serializable;

public class PlugConfData implements Serializable, ServerInfo {

	private static final long serialVersionUID = -8217961541755877382L;
	private long plugId;
	private int uPos;
	private int uHeight;
	private int rack;
	private int rpdu;
	private int phase;

	public PlugConfData() {
	}

	public PlugConfData(final long plugId, final int uPos, final int uHeight, final int rack, final int rpdu,
	        final int phase) {
		this.plugId = plugId;
		this.uPos = uPos;
		this.uHeight = uHeight;
		this.rack = rack;
		this.rpdu = rpdu;
		this.phase = phase;
	}

	public long getPlugId() {
		return plugId;
	}

	public void setPlugId(long plugId) {
		this.plugId = plugId;
	}

	@Override
	public int getuPos() {
		return uPos;
	}

	public void setuPos(int uPos) {
		this.uPos = uPos;
	}

	@Override
	public int getuHeight() {
		return uHeight;
	}

	public void setuHeight(int uHeight) {
		this.uHeight = uHeight;
	}

	@Override
	public int getRack() {
		return rack;
	}

	public void setRack(int rack) {
		this.rack = rack;
	}

	public int getRpdu() {
		return rpdu;
	}

	public void setRpdu(int rpdu) {
		this.rpdu = rpdu;
	}

	public int getPhase() {
		return phase;
	}

	public void setPhase(int phase) {
		this.phase = phase;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + rpdu;
		result = prime * result + phase;
		result = prime * result + (int) (plugId ^ (plugId >>> 32));
		result = prime * result + rack;
		result = prime * result + uHeight;
		result = prime * result + uPos;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PlugConfData other = (PlugConfData) obj;
		if (rpdu != other.rpdu)
			return false;
		if (phase != other.phase)
			return false;
		if (plugId != other.plugId)
			return false;
		if (rack != other.rack)
			return false;
		if (uHeight != other.uHeight)
			return false;
		if (uPos != other.uPos)
			return false;
		return true;
	}

}
