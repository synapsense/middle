/**
 * 
 */
package com.synapsense.service.impl.messages;

import com.synapsense.service.impl.messages.base.SensorData;

/**
 * @author Alexander Borisov
 * 
 */
public class SensorMessage implements BusMessage<SensorData, Long> {
	private long id;
	private SensorData data;

	private static final long serialVersionUID = -5843425844722922204L;

	public SensorMessage(long id, SensorData data) {
		super();
		this.id = id;
		this.data = data;
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public SensorData getValue() {
		return data;
	}

	@Override
	public void setId(Long id) {
		this.id = id;

	}

	@Override
	public void setValue(SensorData value) {
		this.data = value;
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer(20);
		sb.append("Id=");
		sb.append(id);
		sb.append("\n");

		sb.append("message=");
		sb.append(data);
		sb.append("\n");

		return sb.toString();
	}

	@Override
	public Type getType() {
		return Type.SENSOR;
	}

}
