package com.synapsense.service;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.synapsense.dto.CollectionTO;
import com.synapsense.dto.EnvObjectBundle;
import com.synapsense.dto.EnvObjectHistory;
import com.synapsense.dto.ObjectType;
import com.synapsense.dto.ObjectTypeMatchMode;
import com.synapsense.dto.PropertyDescr;
import com.synapsense.dto.Relation;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOBundle;
import com.synapsense.dto.Tag;
import com.synapsense.dto.TagDescriptor;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.EnvException;
import com.synapsense.exception.IllegalInputParameterException;
import com.synapsense.exception.InvalidInitialDataException;
import com.synapsense.exception.ObjectExistsException;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.exception.PropertyNotFoundException;
import com.synapsense.exception.ServerConfigurationException;
import com.synapsense.exception.TagNotFoundException;
import com.synapsense.exception.UnableToConvertPropertyException;
import com.synapsense.exception.UnableToConvertTagException;
import com.synapsense.exception.UnsupportedRelationException;
import com.synapsense.search.QueryDescription;

/**
 * Interface provides access to the environment model.
 * 
 * @author Oleg Stepanov
 * @version 1.0
 * @created 05-Feb-2008 3:21:54 PM
 * 
 */
public interface Environment {
	/**
	 * Creates new object type with specified name.
	 * 
	 * @param name
	 *            new type name
	 * @return created object type
	 * @throws EnvException
	 *             if specified type already exists or is one of predefined
	 *             types
	 * @throws IllegalInputParameterException
	 *             if name is null or empty
	 */
	ObjectType createObjectType(String name) throws EnvException;

	/**
	 * Allows to query existing object types by their names.
	 * <p>
	 * If type with a name specified in the <code>names</code> does not exist,
	 * the name is ignored. Thus returned array may contain less entries than
	 * the <code>names</code> array.
	 * 
	 * @param names
	 *            type names array
	 * @return <tt>ObjectType</tt>s with specified names.
	 * @throws IllegalInputParameterException
	 *             if names is null or empty
	 */
	ObjectType[] getObjectTypes(String[] names);

	/**
	 * Allows to query object type by its name
	 * 
	 * @param name
	 *            name of the type to return
	 * @return <tt>ObjectType</tt> with specified name or null if such type does
	 *         not exist.
	 * @throws IllegalInputParameterException
	 *             if name is null or empty
	 */
	ObjectType getObjectType(String name);

	/**
	 * Allows to get names of all existing object types.
	 * 
	 * @return names of existing object types
	 */
	String[] getObjectTypes();

	/**
	 * Updates object type.
	 * 
	 * @param objType
	 *            Object type to update
	 * @throws ObjectNotFoundException
	 *             if<code>objType</code> does not exist
	 * @throws IllegalInputParameterException
	 *             if objType is null
	 */
	void updateObjectType(ObjectType objType) throws EnvException;

	/**
	 * Deletes object type with specified name. Also deletes all objects of
	 * specified type (cascade deleting)
	 * 
	 * @param typeName
	 *            name of object type to delete
	 * @throws EnvException
	 *             if object type cannot be deleted
	 * @throws IllegalInputParameterException
	 *             if typeName is null or empty
	 */
	void deleteObjectType(String typeName) throws EnvException;

	/**
	 * Creates new object of the specified type. The new object would have no
	 * properties.
	 * 
	 * @param typeName
	 *            name of object's type
	 * @return ID of created object
	 * @throws ObjectNotFoundException
	 *             if object type with the specified name does not exist if
	 *             typeName is null or empty
	 * @throws IllegalInputParameterException
	 *             if typeName is null or empty
	 */
	TO<?> createObject(String typeName) throws EnvException;

	/**
	 * Creates new object of specified type and sets its properties. <br>
	 * To create a new object it is necessary to define all properties which are
	 * mandatory for the specified object type.<br>
	 * <code>propertyValues</code> argument can be left blank but in this case
	 * there is more natural way to create object by using
	 * {@link #createObject(String)} method
	 * 
	 * @param typeName
	 *            name of object type
	 * @param propertyValues
	 *            initial properties values
	 * @return ID of created object
	 * 
	 * @throws InvalidInitialDataException
	 *             if one of initial property values is incorrect
	 * @throws ObjectNotFoundException
	 *             if object type with name <code>typeName</code> does not exist
	 * @throws ObjectExistsException
	 *             if object equals to one that is being created already exists
	 * @throws PropertyNotFoundException
	 *             if one of the initial properties does not belong to the
	 *             specified type
	 * @throws UnableToConvertPropertyException
	 *             if one of the initial property values has inconsistent type
	 * @throws IllegalInputParameterException
	 *             if typeName or propertyValues are null or if propertyValues
	 *             contains null-valued elements
	 */
	TO<?> createObject(String typeName, ValueTO[] propertyValues) throws EnvException;

	/**
	 * Creates objects from specified bundle of environment objects. <br>
	 * Works inside a single transaction , so in case of any error<br>
	 * all changes done by transaction will be rolled back. This method is not
	 * supposed to create ALERTs.
	 * 
	 * @param objectsToCreate
	 *            Bundle of environment objects to create
	 * @return Bundle of just created object IDs preserving elements order from
	 *         <code>objectsToCreate</code>
	 * @throws EnvException
	 */
	TOBundle createObjects(EnvObjectBundle objectsToCreate) throws EnvException;

	/**
	 * Returns <tt>IDs</tt> of all objects of specified type<br>
	 * If there are no objects of the specified type then returns empty
	 * <code>Collection</code>
	 * 
	 * @param typeName
	 *            type name of objects to be returned
	 * @return Collection of object <tt>ID</tt>s
	 * @throws IllegalInputParameterException
	 *             if typeName is null or empty
	 */
	Collection<TO<?>> getObjectsByType(String typeName);

	/**
	 * Returns <tt>IDs</tt> of all objects of the specified type<br>
	 * and of types which are match to the <code>typeName</code> by the
	 * specified <code>matchMode</code>. If there are no objects of the
	 * specified type then returns empty <code>Collection</code>
	 * 
	 * @param typeName
	 *            type name of objects to be returned
	 * @param matchMode
	 *            Object type match mode
	 * @return Collection of object <tt>ID</tt>s
	 * @throws IllegalInputParameterException
	 *             if typeName is null or empty or matchMode is null
	 */
	Collection<TO<?>> getObjectsByType(String typeName, ObjectTypeMatchMode matchMode);

	/**
	 * Allows to request IDs of objects with matching property values. If there
	 * are not such objects then returns empty <code>Collection</code>
	 * 
	 * @param typeName
	 *            type name of objects to be returned
	 * @param propertyValues
	 *            criteria to find objects
	 * @return IDs of objects with matching property values
	 * @throws IllegalInputParameterException
	 *             if typeName or propertyValues are null or empty or if
	 *             propertyValues contains null-valued elements
	 */
	Collection<TO<?>> getObjects(String typeName, ValueTO[] propertyValues);

	/**
	 * Allows caller to get all objects which match to given
	 * <code>queryDescr</code>
	 * 
	 * @param queryDescr
	 *            Query description
	 * @return IDs of objects
	 */
	Collection<TO<?>> getObjects(QueryDescription queryDescr);

	/**
	 * Allows to request IDs of objects with matching property values. Has the
	 * special parameter<code>matchMode</code> - how to interpret object type
	 * name. If there are no such objects then returns empty
	 * <code>Collection</code>. All object which have type name matching the
	 * specified type name will be returned.
	 * 
	 * @see #getObjects(String, ValueTO[]) where the mode is "exact match" by
	 *      default
	 * 
	 * @param typeName
	 *            type name of objects to be returned
	 * @param ObjectTypeMatchMode
	 *            matchMode What match mode to use for object type name
	 * @param propertyValues
	 *            criteria to find objects
	 * @return IDs of objects with matching property values
	 * @throws IllegalInputParameterException
	 *             if typeName or propertyValues are null or empty or if
	 *             matchMode is null or if propertyValues contains null-valued
	 *             elements
	 */
	Collection<TO<?>> getObjects(String typeName, ObjectTypeMatchMode matchMode, ValueTO[] propertyValues);

	/**
	 * Deletes object with specified ID. ALso clears all links to it from other
	 * objects.
	 * 
	 * @param objId
	 *            ID of object to delete
	 * @throws ObjectNotFoundException
	 *             if specified object does not exist
	 * @throws IllegalInputParameterException
	 *             if objId is null
	 */
	void deleteObject(TO<?> objId) throws ObjectNotFoundException;

	/**
	 * Deletes specified object together with its descendants in object
	 * hierarchy.
	 * 
	 * @param objId
	 *            Host object
	 * @param level
	 *            How many hierarchy levels to perform.<br>
	 *            There are special values: <br>
	 *            0 - equivalent to {@link #deleteObject(TO)} <br>
	 *            -1 - delete all descendants
	 * 
	 * @throws ObjectNotFoundException
	 */
	void deleteObject(TO<?> objId, int level) throws ObjectNotFoundException;

	/**
	 * Allows to get all property values of an object Returns all defined
	 * property values of an object If specified object does not exist then
	 * returns <tt>null</tt>
	 * 
	 * @param objId
	 *            object ID
	 * @return values of object properties
	 * @throws ObjectNotFoundException
	 *             if specified object does not exist
	 * @throws IllegalInputParameterException
	 *             if objId is null
	 */
	Collection<ValueTO> getAllPropertiesValues(TO<?> objId) throws ObjectNotFoundException;

	/**
	 * Allows caller to set values of several object properties in one call
	 * 
	 * @param objId
	 *            object ID
	 * @param values
	 *            values of properties to set
	 * @throws ObjectNotFoundException
	 *             if object with id <code>objId</code> does not exist
	 * @throws PropertyNotFoundException
	 *             if object's property with any property name from
	 *             <code>values</code> does not exist
	 * @throws UnableToConvertPropertyException
	 *             if specified value cannot be converted to property type
	 * @throws IllegalInputParameterException
	 *             if objId is null or if values is null or empty or if values
	 *             contains null-valued elements
	 */
	void setAllPropertiesValues(TO<?> objId, ValueTO[] values) throws ObjectNotFoundException,
	        PropertyNotFoundException, UnableToConvertPropertyException;

	/**
	 * Allows caller to set values and tags of several object properties in one
	 * call
	 * 
	 * @param objId
	 *            object ID
	 * @param values
	 *            values of properties to set
	 * @param tags
	 *            tags to set
	 */
	void setAllPropertiesValues(TO<?> objId, Collection<ValueTO> values, Collection<Tag> tags) throws EnvException;

	/**
	 * Allows to get a value of specified object's property
	 * 
	 * @param objId
	 *            object ID
	 * @param propName
	 *            property name
	 * @param clazz
	 *            expected type of requested property value
	 * @return property value if property exists or null
	 * @throws ObjectNotFoundException
	 *             if object with specified ID does not exist
	 * @throws PropertyNotFoundException
	 *             if property with specified name does not exist
	 * @throws UnableToConvertPropertyException
	 *             if property value cannot be casted to the specified class
	 * @throws IllegalInputParameterException
	 *             if objId is null or if propName is null or empty or if clazz
	 *             is null
	 */
	<RETURN_TYPE> RETURN_TYPE getPropertyValue(TO<?> objId, String propName, Class<RETURN_TYPE> clazz)
	        throws ObjectNotFoundException, PropertyNotFoundException, UnableToConvertPropertyException;

	/**
	 * Allows to set property value
	 * 
	 * @param objId
	 *            object ID
	 * @param propName
	 *            name of the property to be set
	 * @param value
	 *            new property value
	 * @throws ObjectNotFoundException
	 *             if object with specified ID does not exist
	 * @throws PropertyNotFoundException
	 *             if property with specified name does not exist
	 * @throws UnableToConvertPropertyException
	 *             if property value class cannot be casted to stored class
	 * @throws IllegalInputParameterException
	 *             if objId is null or if propName is null or empty or if value
	 *             is null
	 */
	<REQUESTED_TYPE> void setPropertyValue(TO<?> objId, String propName, REQUESTED_TYPE value)
	        throws ObjectNotFoundException, PropertyNotFoundException, UnableToConvertPropertyException;

	/**
	 * Allows to add new element to a collection-type property<br>
	 * Specified property must be a collection
	 * 
	 * @param objId
	 *            object ID
	 * @param propName
	 *            name of the property to be modified
	 * @param value
	 *            value to add
	 * @throws ObjectNotFoundException
	 *             if object with ID <code>objId</code> does not exist
	 * @throws PropertyNotFoundException
	 *             if property <code>propName</code> does not exist
	 * @throws UnableToConvertPropertyException
	 *             if property is not a collection or value has wrong type
	 * @throws IllegalInputParameterException
	 *             if objId is null or if propName is null or empty or if value
	 *             is null
	 */
	<REQUESTED_TYPE> void addPropertyValue(TO<?> objId, String propName, REQUESTED_TYPE value)
	        throws ObjectNotFoundException, PropertyNotFoundException, UnableToConvertPropertyException;

	/**
	 * Removes element from a collection-type property.<br>
	 * Specified property must be a collection.
	 * 
	 * @param objId
	 *            object ID
	 * @param propName
	 *            name of the property to be modified
	 * @param value
	 *            value to remove
	 * @throws ObjectNotFoundException
	 *             if object with ID <code>objId</code> does not exist
	 * @throws PropertyNotFoundException
	 *             if property <code>propName</code> does not exist
	 * @throws UnableToConvertPropertyException
	 *             if property is not a collection or value has wrong type
	 * @throws IllegalInputParameterException
	 *             if objId is null or if propName is null or empty or if value
	 *             is null
	 */
	<REQUESTED_TYPE> void removePropertyValue(TO<?> objId, String propName, REQUESTED_TYPE value)
	        throws ObjectNotFoundException, PropertyNotFoundException, UnableToConvertPropertyException;

	/**
	 * Allows to get history of specified object property Returns all historical
	 * data that is available
	 * 
	 * @param objId
	 *            object ID
	 * @param propName
	 *            property name which values are to be returned
	 * @param clazz
	 *            requested property value class
	 * @return all stored historical values
	 * @throws ObjectNotFoundException
	 *             if object with specified ID does not exist
	 * @throws PropertyNotFoundException
	 *             if property with specified name does not exist
	 * @throws UnableToConvertPropertyException
	 *             if property value class cannot be casted to stored class
	 * @throws IllegalInputParameterException
	 *             if objId is null or if propName is null or empty or if clazz
	 *             is null
	 */
	<RETURN_TYPE> Collection<ValueTO> getHistory(TO<?> objId, String propName, Class<RETURN_TYPE> clazz)
	        throws ObjectNotFoundException, UnableToConvertPropertyException, PropertyNotFoundException;

	/**
	 * Get a certain frame of object's property historical values. Full size of
	 * property history can be retrieved with <tt>getHistorySize</tt>
	 * 
	 * @param objId
	 *            object ID
	 * @param propName
	 *            property name which values are to be returned
	 * @param size
	 *            frame size
	 * @param start
	 *            frame start index. Zero means the oldest count.
	 * @param clazz
	 *            requested property value class
	 * @return requested frame of historical values
	 * @throws ObjectNotFoundException
	 *             if object with specified ID does not exist
	 * @throws PropertyNotFoundException
	 *             if property with specified name does not exist
	 * @throws UnableToConvertPropertyException
	 *             if property value class cannot be casted to stored class
	 * @throws IllegalInputParameterException
	 *             if objId is null or if propName is null or empty or if clazz
	 *             is null
	 */
	<RETURN_TYPE> Collection<ValueTO> getHistory(TO<?> objId, String propName, int size, int start,
	        Class<RETURN_TYPE> clazz) throws ObjectNotFoundException, PropertyNotFoundException,
	        UnableToConvertPropertyException;

	/**
	 * Get object's property historical values within specified time period.
	 * 
	 * @param objId
	 *            object ID
	 * @param propName
	 *            property name which values are to be returned
	 * @param start
	 *            period start mark
	 * @param end
	 *            period end mark
	 * @param clazz
	 *            requested property value class
	 * @return historical values within requested period
	 * @throws ObjectNotFoundException
	 *             if object with specified ID does not exist
	 * @throws PropertyNotFoundException
	 *             if property with specified name does not exist
	 * @throws UnableToConvertPropertyException
	 *             if property value class cannot be casted to stored class
	 * @throws IllegalInputParameterException
	 *             if objId is null or if propName is null or empty or if clazz
	 *             is null or if start is null or if end is null or if start >
	 *             end
	 */
	<RETURN_TYPE> Collection<ValueTO> getHistory(TO<?> objId, String propName, Date start, Date end,
	        Class<RETURN_TYPE> clazz) throws ObjectNotFoundException, PropertyNotFoundException,
	        UnableToConvertPropertyException;

	/**
	 * Get object's properties historical values within specified time period.
	 * Groups returning properties values into tuples by group_id field. This allow
	 * caller to view history of a whole (or a part of) object instead of
	 * history of single property.
	 * 
	 * @param objId
	 *            object ID
	 * @param propNames
	 *            array of properties names to get
	 * @param start
	 *            get history from
	 * @param end
	 *            get history to
	 * 
	 * @return Historical values within requested period
	 * @throws PropertyNotFoundException
	 * @throws ObjectNotFoundException
	 * @throws IllegalInputParameterException
	 */
	EnvObjectHistory getHistory(TO<?> objId, Collection<String> propNames, Date start, Date end)
	        throws ObjectNotFoundException, PropertyNotFoundException;

	/**
	 * Get object's property historical values
	 * 
	 * @param objId
	 *            object ID
	 * @param propName
	 *            property name which values are to be returned
	 * @param start
	 *            period start mark
	 * @param numberPoints
	 *            number of points
	 * @param clazz
	 *            requested property value class
	 * @return historical values within requested period
	 * @throws ObjectNotFoundException
	 *             if object with specified ID does not exist
	 * @throws PropertyNotFoundException
	 *             if property with specified name does not exist
	 * @throws UnableToConvertPropertyException
	 *             if property value class cannot be casted to stored class
	 * @throws IllegalInputParameterException
	 *             if objId is null or if propName is null or empty or if clazz
	 *             is null or if start is null or if numberPoints is < 1
	 */
	<RETURN_TYPE> Collection<ValueTO> getHistory(TO<?> objId, String propName, Date start, int numberPoints,
	        Class<RETURN_TYPE> clazz) throws ObjectNotFoundException, PropertyNotFoundException,
	        UnableToConvertPropertyException;

	/**
	 * Get history size for specified property.
	 * 
	 * @param objId
	 *            object ID
	 * @param propName
	 *            property name which history size is to be returned
	 * @return history size of specified property
	 * @throws ObjectNotFoundException
	 *             if object with specified ID does not exist
	 * @throws PropertyNotFoundException
	 *             if property with specified name does not exist
	 * @throws IllegalInputParameterException
	 *             if objId is null or if propName is null or empty
	 */
	Integer getHistorySize(TO<?> objId, String propName) throws ObjectNotFoundException, PropertyNotFoundException;

	/**
	 * Sets parent-child relationship between two objects
	 * 
	 * @param parentId
	 *            Parent object ID
	 * @param childId
	 *            Child object ID
	 * @throws ObjectNotFoundException
	 *             if parentId or childId doesn't exist
	 * @throws UnsupportedRelationException
	 *             if relation between specified objects cannot be set
	 * @throws IllegalInputParameterException
	 *             if parentId is null or if childId is null
	 */
	void setRelation(TO<?> parentId, TO<?> childId) throws ObjectNotFoundException, UnsupportedRelationException;

	/**
	 * Sets parent-child relation between objects in <code>relations</code>
	 * bundle <br>
	 * Call is processed inside a single transaction , so in case of any error<br>
	 * all changes done by transaction will be rolled back. <br>
	 * Relations will be performed in exact order of given list.
	 * 
	 * @param relations
	 *            List of relation bundles
	 * @throws ObjectNotFoundException
	 *             If some parent or child object doesn't exist
	 * @throws UnsupportedRelationException
	 *             if relation between specified objects cannot be set
	 */
	void setRelation(List<Relation> relations) throws ObjectNotFoundException, UnsupportedRelationException;

	/**
	 * Removes parent-child relation between objects
	 * 
	 * @param parentId
	 *            Parent object ID
	 * @param childId
	 *            Child object ID
	 * @throws ObjectNotFoundException
	 *             if parentId or childId do not exist
	 * @throws IllegalInputParameterException
	 *             if parentId is null or if childId is null
	 */
	void removeRelation(TO<?> parentId, TO<?> childId) throws ObjectNotFoundException;

	/**
	 * Removes parent-child relations between specified objects.
	 * 
	 * 
	 * @param relations
	 *            List of relation bundles
	 * @throws ObjectNotFoundException
	 *             If some parent or child object doesn't exist
	 */
	void removeRelation(List<Relation> relations) throws ObjectNotFoundException;

	/**
	 * Returns all parents of specified object. Only objects with specified
	 * object type or empty collection will be returned if object has no
	 * parents.
	 * 
	 * @param objId
	 *            ID of object
	 * @param typeName
	 *            filter result by type name(null or empty - disables filter)
	 * @return Collection of Object IDs as TO
	 * @throws ObjectNotFoundException
	 *             if object with the specified objId does not exist
	 * @throws IllegalInputParameterException
	 *             if objId is null
	 */
	Collection<TO<?>> getParents(TO<?> objId, String typeName) throws ObjectNotFoundException;

	/**
	 * Returns all parents of specified object. All parents of the specified
	 * object or empty collection will be returned if object has no parents.
	 * 
	 * @param objId
	 *            ID of object
	 * @return Collection of Object IDs as TO
	 * @throws ObjectNotFoundException
	 *             if object with the specified objId does not exist
	 * @throws IllegalInputParameterException
	 *             if objId is null
	 */
	Collection<TO<?>> getParents(TO<?> objId) throws ObjectNotFoundException;

	/**
	 * Returns all children of an object (from one level down by hierarchy) or
	 * empty collection if object has no children<br>
	 * All objects will be returned regardless of object type
	 * 
	 * @see #getChildren(Collection)
	 * @see #getChildren(TO, String)
	 * @see #getChildren(Collection, String)
	 * 
	 * @param objId
	 *            ID of object
	 * @return CCollection of Object IDs
	 * @throws ObjectNotFoundException
	 *             if the specified object does not exist
	 * 
	 * @throws IllegalInputParameterException
	 *             if objId is null
	 */
	Collection<TO<?>> getChildren(TO<?> objId) throws ObjectNotFoundException;

	/**
	 * Returns all children of an object (from one level down by hierarchy) or
	 * empty collection if object has no children<br>
	 * Only objects with specified type will be returned.<br>
	 * <code>typeName</code> can be left blank to not filter types.
	 * 
	 * @see #getChildren(Collection)
	 * @see #getChildren(TO)
	 * @see #getChildren(Collection, String)
	 * 
	 * @param objId
	 *            ID of object
	 * @param typeName
	 *            Name of object type
	 * @return Collection of Object IDs
	 * @throws ObjectNotFoundException
	 *             if the specified object does not exist
	 * @throws IllegalInputParameterException
	 *             if objId is null
	 */
	Collection<TO<?>> getChildren(TO<?> objId, String typeName) throws ObjectNotFoundException;

	/**
	 * Returns all children of objects (from one level down by hierarchy). The
	 * result collection will contain as many elements as size of
	 * <code>objIds</code> argument. If some objects from <code>objIds</code> do
	 * not exist IAC they will be contained in the result. The object children
	 * will be placed in {@link CollectionTO#getPropValues()} collection.
	 * 
	 * Only children with the specified type will be returned.
	 * <code>objIds</code> collection may consist of objects of any type from
	 * <code>Types</code> enum.
	 * 
	 * <code>typeName</code> can be left blank to not filter types.
	 * 
	 * The result will be ordered as input <code>objIds</code> collection. For
	 * WSN devices : Network can have only nodes as first-level children; Node
	 * can have only sensors as first-level children.
	 * 
	 * 
	 * @see #getChildren(Collection)
	 * @see #getChildren(TO)
	 * @see #getChildren(TO, String)
	 * 
	 * @param objIds
	 *            ID of objects
	 * @param typeName
	 *            Name of object type
	 * @return Collection of <code>CollectionTO</code>
	 * @throws ObjectNotFoundException
	 *             if specified object does not exist
	 * 
	 * @throws IllegalInputParameterException
	 *             if objIds is null or empty
	 */
	Collection<CollectionTO> getChildren(Collection<TO<?>> objIds, String typeName);

	/**
	 * Returns all children of objects (from one level down by hierarchy) If
	 * some objects from <code>objIds</code> do not exist IAC they will be
	 * contained in the result. The object children will be placed in
	 * {@link CollectionTO#getPropValues()} collection. The <code>objIds</code>
	 * collection may consist of objects of any type from <code>Types</code>
	 * enum.
	 * 
	 * @see #getChildren(Collection, String)
	 * @see #getChildren(TO)
	 * @see #getChildren(TO, String)
	 * 
	 * @param objIds
	 *            ID of objects
	 * @return Collection of <code>CollectionTO</code>
	 * @throws ObjectNotFoundException
	 *             if the specified object does not exist
	 * 
	 * @throws IllegalInputParameterException
	 *             if objIds is null or empty
	 */
	Collection<CollectionTO> getChildren(Collection<TO<?>> objIds);

	/**
	 * Allows to get timestamp of the last property value
	 * 
	 * @param objId
	 *            ID of object
	 * @param propName
	 *            Property name
	 * @return property tiemstamp in milliseconds
	 * @throws ObjectNotFoundException
	 *             if the specified object does not exist
	 * @throws PropertyNotFoundException
	 *             if the specified property does not exist
	 * @throws IllegalInputParameterException
	 *             if objId is null or if propName is null or empty
	 */
	long getPropertyTimestamp(TO<?> objId, String propName) throws ObjectNotFoundException, PropertyNotFoundException;

	/**
	 * Get <code>PropertyDescr</code> of specified property
	 * 
	 * @param typeName
	 *            Object type name
	 * @param propName
	 *            Property name
	 * @return PropertyDescr descriptor of requested property
	 * @throws ObjectNotFoundException
	 *             if the specified object type does not exist
	 * @throws PropertyNotFoundException
	 *             if the specified property does not exist
	 * @throws IllegalInputParameterException
	 *             if typeName is null or empty or if propName is null or empty
	 */
	PropertyDescr getPropertyDescriptor(String typeName, String propName) throws ObjectNotFoundException,
	        PropertyNotFoundException;

	/**
	 * Allows to get a bundle of objects property values
	 * <p>
	 * The method is more effective when it's necessary to get concrete
	 * properties of several objects. The input collection <code>objIds</code>
	 * must contains of object of the same type. By example you cannot get
	 * collection <code>objIds</code> like this : { TO node =
	 * TOFactory.load("NODE:1"); TO crac = TOFactory.load("CRAC:2");
	 * 
	 * Collection objIds = new LinkedList(); objIds.add(node); objIds.add(crac);
	 * 
	 * env.getPropertyValue(objIds,new String[]{name}); ->> here is
	 * IllegalInputParameterException }
	 * 
	 * @param objIds
	 *            Collection of TO
	 * @param propNames
	 *            Array of properties names
	 * @return Collection of <code>PropertyValueBundleTO</code>
	 * @throws IllegalInputParameterException
	 *             if objIds is null or empty or if propNames is null or empty
	 *             or contains null-valued elements
	 */
	Collection<CollectionTO> getPropertyValue(Collection<TO<?>> objIds, String[] propNames);

	/**
	 * Allows to get a bundle of objects property values
	 * <p>
	 * Returns all property values for each requested object. In other words,
	 * for each object from <code>objIds</code> which does exist a corresponding
	 * <code>CollectionTO</code> will be added to the resulting collection.
	 * Non-existing object IDs are ignored. The input collection
	 * <code>objIds</code> must contains of object of the same type. By example
	 * you cannot get collection <code>objIds</code> like this : { TO node =
	 * TOFactory.load("NODE:1"); TO crac = TOFactory.load("CRAC:2");
	 * 
	 * Collection objIds = new LinkedList(); objIds.add(node); objIds.add(crac);
	 * 
	 * env.getPropertyValue(objIds,new String[]{name}); ->> here is
	 * IllegalInputParameterException }
	 * 
	 * @param objIds
	 *            Collection of TO
	 * @return Collection of <code>PropertyValueBundleTO</code>
	 * @throws IllegalInputParameterException
	 *             if objIds is null or empty
	 */
	Collection<CollectionTO> getAllPropertiesValues(Collection<TO<?>> objIds);

	/**
	 * Allows to get object's children or parents. Returns all objects from
	 * hierarchy. Result can be filtered by type name of hierarchical objects.
	 * 
	 * @param objId
	 *            ID of object
	 * @param objectType
	 *            filter by object type
	 * @param isChild
	 *            defines what to return - parents or children
	 * @return Collection of TO which are related to the specified object from
	 *         all layers of hierarchy
	 * @throws IllegalInputParameterException
	 *             if objId is null or objectType is null or empty
	 */
	Collection<TO<?>> getRelatedObjects(TO<?> objId, String objectType, boolean isChild);

	/**
	 * Static property getter. Allows to get static properties values of the
	 * specified type.
	 * 
	 * @param typeName
	 *            name of type
	 * @param propName
	 *            property name
	 * @param clazz
	 *            expected type of requested property value
	 * @return property value if property exists or null if property has no
	 *         value yet.
	 * @throws ObjectNotFoundException
	 *             if object with specified ID does not exist
	 * @throws PropertyNotFoundException
	 *             if property with specified name does not exist
	 * @throws UnableToConvertPropertyException
	 *             if property value cannot be casted to the specified class
	 * @throws IllegalInputParameterException
	 *             if typeName is null or empty or if propName is null or empty
	 *             or clazz is null
	 */
	<RETURN_TYPE> RETURN_TYPE getPropertyValue(String typeName, String propName, Class<RETURN_TYPE> clazz)
	        throws ObjectNotFoundException, PropertyNotFoundException, UnableToConvertPropertyException;

	/**
	 * Static property setter. Allows to set static properties values of the
	 * specified type.
	 * 
	 * @param typeName
	 *            name of type
	 * @param propName
	 *            name of the property to be set
	 * @param value
	 *            new property value
	 * @throws ObjectNotFoundException
	 *             if object with specified ID does not exist
	 * @throws PropertyNotFoundException
	 *             if property with specified name does not exist
	 * @throws UnableToConvertPropertyException
	 *             if property value class cannot be casted to stored class
	 * @throws IllegalInputParameterException
	 *             if typeName is null or empty or if propName is null or empty
	 *             or value is null
	 */

	<REQUESTED_TYPE> void setPropertyValue(String typeName, String propName, REQUESTED_TYPE value)
	        throws ObjectNotFoundException, PropertyNotFoundException, UnableToConvertPropertyException;

	/**
	 * Tells the Environment Server to reconfigure internal components like CRE,
	 * DRE, DM. <br>
	 * Here is a counter to count how many times {@link #configurationStart()}
	 * was called.<br>
	 * This method should be called as many times as
	 * {@link #configurationStart()} was called<br>
	 * to complete server configuration process.<br>
	 * 
	 * @throws ServerConfigurationException
	 */
	void configurationComplete() throws ServerConfigurationException;

	/**
	 * Disables the services: CRE, DRE, DeviceMapper, scheduled tasks.<br>
	 * Here is a counter to count how many times this method was called.<br>
	 * It us used to determine how many clients are in a middle of server
	 * configuration process.<br>
	 * 
	 * @throws ServerConfigurationException
	 */
	void configurationStart() throws ServerConfigurationException;

	/**
	 * {@link #setAllPropertiesValues(TO, ValueTO[])}'s clone which has
	 * additional option to persist values in a permanent store. Normally should
	 * not be used by external the clients.
	 * 
	 * @param objId
	 *            Object ID
	 * @param values
	 *            Property values to set
	 * @param persist
	 *            To persist or not
	 * @throws ObjectNotFoundException
	 *             if object with id <code>objId</code> does not exist
	 * @throws PropertyNotFoundException
	 *             if object's property with any property name from
	 *             <code>values</code> does not exist
	 * @throws UnableToConvertPropertyException
	 *             if specified value cannot be converted to property type
	 * @throws IllegalInputParameterException
	 *             if objId is null or if values is null or empty or if values
	 *             contains null-valued elements
	 */
	void setAllPropertiesValues(TO<?> objId, ValueTO[] values, boolean persist) throws ObjectNotFoundException,
	        PropertyNotFoundException, UnableToConvertPropertyException;

	/**
	 * Same method as above but with optional persisting of value in a permanent
	 * store. Normally should not be used by the external clients.
	 * 
	 * @param <REQUESTED_TYPE>
	 * @param objId
	 * @param propName
	 * @param value
	 * @param persist
	 * @throws PropertyNotFoundException
	 * @throws ObjectNotFoundException
	 * @throws UnableToConvertPropertyException
	 */
	<REQUESTED_TYPE> void setPropertyValue(TO<?> objId, String propName, REQUESTED_TYPE value, boolean persist)
	        throws PropertyNotFoundException, ObjectNotFoundException, UnableToConvertPropertyException;

	/**
	 * check if object with objId exists in environment
	 * 
	 * @param objId
	 * 
	 * @return true if object with objId exists in environment
	 * @throws IllegalInputParameterException
	 *             if objId has illegal values or is null
	 */
	boolean exists(TO<?> objId) throws IllegalInputParameterException;

	/**
	 * Get <code>TagDescriptor</code> of specified property tag
	 * 
	 * @param typeName
	 *            Object type name
	 * @param propName
	 *            Property name
	 * @param tagName
	 *            Tag name
	 * @return TagDescriptor descriptor of requested property tag
	 * @throws ObjectNotFoundException
	 *             if the specified object type does not exist
	 * @throws PropertyNotFoundException
	 *             if the specified property does not exist
	 * @throws TagNotFoundException
	 *             if the specified property tag does not exist
	 * @throws IllegalInputParameterException
	 *             if object type isn't Generic or if typeName is null or empty
	 *             or if propName is null or empty or if tagName is null or
	 *             empty
	 */
	TagDescriptor getTagDescriptor(String typeName, String propName, String tagName) throws ObjectNotFoundException,
	        PropertyNotFoundException, TagNotFoundException;

	/**
	 * Allows to get a value of specified object's property tag
	 * 
	 * @param objId
	 *            object ID
	 * @param propName
	 *            property name
	 * @param tagName
	 *            Property Tag name
	 * @param clazz
	 *            expected type of requested property tag
	 * @return property tag value or null if tag doesn't contain value
	 * @throws ObjectNotFoundException
	 *             if object with specified ID does not exist
	 * @throws PropertyNotFoundException
	 *             if property with specified name does not exist
	 * @throws TagNotFoundException
	 *             if the specified property tag does not exist
	 * @throws UnableToConvertTagException
	 *             if property tag value cannot be casted to the specified class
	 * @throws IllegalInputParameterException
	 *             if object type isn't Generic or if objId is null or if
	 *             propName is null or empty or if tagName is null or empty or
	 *             if clazz is null
	 */
	<RETURN_TYPE> RETURN_TYPE getTagValue(TO<?> objId, String propName, String tagName, Class<RETURN_TYPE> clazz)
	        throws ObjectNotFoundException, PropertyNotFoundException, TagNotFoundException,
	        UnableToConvertTagException;

	/**
	 * Allows to get all property tags of specified object's property.
	 * 
	 * @param objId
	 *            object ID
	 * @param propName
	 *            property name
	 * @return collection of property tags, or empty collection if nothing was
	 *         found
	 * @throws ObjectNotFoundException
	 *             if specified object does not exist
	 * @throws PropertyNotFoundException
	 *             if property with specified name does not exist
	 * @throws IllegalInputParameterException
	 *             if object type isn't Generic or if objId is null or empty or
	 *             if propName is null or empty
	 */
	Collection<Tag> getAllTags(TO<?> objId, String propName) throws ObjectNotFoundException, PropertyNotFoundException;

	/**
	 * Allows to get all tags of all properties of specified object.
	 * 
	 * @param objId
	 *            object ID
	 * @return collection of property tags or empty collection if nothing was
	 *         found
	 * @throws ObjectNotFoundException
	 *             if specified object does not exist
	 * @throws IllegalInputParameterException
	 *             if object type isn't Generic or if objId is null or empty
	 */
	Collection<Tag> getAllTags(TO<?> objId) throws ObjectNotFoundException;

	/**
	 * Allows to get all tags of all properties of all specified type objects.
	 * 
	 * @param typeName
	 *            type name
	 * @return Map of property tags or empty Map if nothing was found.
	 * @throws IllegalInputParameterException
	 *             if object type isn't Generic or if typeName is null or
	 *             typeName is empty
	 */
	Map<TO<?>, Collection<Tag>> getAllTags(String typeName);

/**
	 * Allows to get tags of specified names of specified properties  of specified type objects.
	 * 
	 * @param objIds
	 *            Collection of object ids whose property tags needs
	 * @param propertyNames
	 *            List names of properties of ids onjects whose property tags needs, 
	 *            		if list is empty or null then tags with 'TagNames' names of all properties are searched 
	 * @param tagNames
	 *            List names of needed tags.
	 *            		if list is empty or null then all tags are searched
	 * @return Map of property tags or empty collection if nothing was found.
	 * 			Map<'object id', Map<Property Name, Collection<Tag>>>
	 * @throws IllegalInputParameterException
	 * 			   if even if one object type in objIds isn't Generic or 
	 *             if objIds is null or objIds is empty
	 */
	Map<TO<?>, Map<String, Collection<Tag>>> getTags(Collection<TO<?>> objIds, List<String> propertyNames,
	        List<String> tagNames);

	/**
	 * Allows to set property tag value
	 * 
	 * @param objId
	 *            object ID
	 * @param propName
	 *            name of the property to be set
	 * @param tagName
	 *            Property Tag name
	 * @param value
	 *            new property tag value
	 * @throws ObjectNotFoundException
	 *             if object with specified ID does not exist
	 * @throws PropertyNotFoundException
	 *             if property with specified name does not exist
	 * @throws TagNotFoundException
	 *             if the specified property tag does not exist
	 * @throws UnableToConvertPropertyException
	 *             if property tag value class cannot be casted to stored class
	 * @throws IllegalInputParameterException
	 *             if object type is not Generic or if objId is null or if
	 *             propName is null or empty or if value is null or if tagName
	 *             is null or empty
	 */
	void setTagValue(TO<?> objId, String propName, String tagName, Object value) throws PropertyNotFoundException,
	        ObjectNotFoundException, TagNotFoundException, UnableToConvertTagException;

	/**
	 * Sets values for multiple tags of the given object.
	 * 
	 * @param objId
	 *            object ID
	 * @param tagsToSet
	 *            Collection of Tag objects with values to set.
	 * @throws ObjectNotFoundException
	 *             if object with specified ID does not exist
	 * @throws PropertyNotFoundException
	 *             if one of the property with specified name in a Tag object
	 *             does not exist
	 * @throws TagNotFoundException
	 *             if one of the specified property's tags does not exist
	 * @throws UnableToConvertPropertyException
	 *             if property tag value class cannot be casted to stored class
	 * @throws IllegalInputParameterException
	 *             if object type is not Generic or if tagsToSet is null
	 */
	void setAllTagsValues(TO<?> objId, Collection<Tag> tagsToSet) throws PropertyNotFoundException,
	        ObjectNotFoundException, TagNotFoundException, UnableToConvertTagException;
}
