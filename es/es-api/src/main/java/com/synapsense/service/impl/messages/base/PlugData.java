package com.synapsense.service.impl.messages.base;

import java.io.Serializable;

public class PlugData implements Serializable {

	private static final long serialVersionUID = 4893333569290738055L;

	private long pId;
	private double avgCurrent;
	private double maxCurrent;
	private double demandPower;
	private long timestamp;

	public PlugData(long pId, double avgCurrent, double maxCurrent, double demandPower, long timestamp) {
		this.pId = pId;
		this.avgCurrent = avgCurrent;
		this.maxCurrent = maxCurrent;
		this.demandPower = demandPower;
		this.timestamp = timestamp;
	}

	public long getpId() {
		return pId;
	}

	public double getAvgCurrent() {
		return avgCurrent;
	}

	public double getMaxCurrent() {
		return maxCurrent;
	}

	public double getDemandPower() {
		return demandPower;
	}

	public long getTimestamp() {
		return timestamp;
	}

	@Override
	public String toString() {
		return new StringBuilder("JawaId=").append(pId).append(" avgCurrent=").append(avgCurrent)
		        .append(" maxCurrent=").append(maxCurrent).append(" demandPower=").append(demandPower)
		        .append(" timestamp=").append(timestamp).toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(avgCurrent);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(demandPower);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(maxCurrent);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + (int) (pId ^ (pId >>> 32));
		result = prime * result + (int) (timestamp ^ (timestamp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PlugData other = (PlugData) obj;
		if (Double.doubleToLongBits(avgCurrent) != Double.doubleToLongBits(other.avgCurrent))
			return false;
		if (Double.doubleToLongBits(demandPower) != Double.doubleToLongBits(other.demandPower))
			return false;
		if (Double.doubleToLongBits(maxCurrent) != Double.doubleToLongBits(other.maxCurrent))
			return false;
		if (pId != other.pId)
			return false;
		if (timestamp != other.timestamp)
			return false;
		return true;
	}
}
