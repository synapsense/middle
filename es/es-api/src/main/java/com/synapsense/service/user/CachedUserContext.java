package com.synapsense.service.user;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.util.Locale;

/**
 * Local cache for user context calls. <br>
 * Be ready to manually control this object instance lifecycle because there is
 * no way to evict cached values except to create a new instance of this class.
 * 
 * This class is thread-unsafe. Consider to implement synchronized version if
 * required.
 * 
 * @author ashabanov
 * 
 */
public class CachedUserContext implements UserContext {
	private UserContext ucx;

	private String unitSystemName;
	private DateFormat dateFormat;
	private NumberFormat numberFormat;
	private Locale locale;

	public CachedUserContext(UserContext ucx) {
		this.ucx = ucx;
	}

	@Override
	public String getUnitSystem() {
		if (unitSystemName == null) {
			unitSystemName = ucx.getUnitSystem();
		}
		return unitSystemName;
	}

	@Override
	public DateFormat getDateFormat() {
		if (dateFormat == null) {
			dateFormat = ucx.getDateFormat();
		}
		return dateFormat;
	}

	@Override
	public NumberFormat getDataFormat() {
		if (numberFormat == null) {
			numberFormat = ucx.getDataFormat();
		}
		return numberFormat;
	}

	@Override
	public Locale getLocale() {
		if (locale == null) {
			locale = ucx.getLocale();
		}
		return locale;
	}

}
