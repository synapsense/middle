package com.synapsense.service.nsvc.events;

public interface RelationSetEventProcessor {

	RelationSetEvent process(RelationSetEvent e);

}
