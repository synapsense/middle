package com.synapsense.service.nsvc.events;

public interface PropertiesChangedEventProcessor {
	PropertiesChangedEvent process(PropertiesChangedEvent e);
}
