package com.synapsense.service.reporting.olap;

import java.util.LinkedList;
import java.util.List;

import com.synapsense.service.reporting.olap.custom.Cube;


public class FilterableAllMemberSet extends AllMemberSet {

	private static final long serialVersionUID = -1804478598748723734L;

	private FilterableHierarchy hierarchy;
	private Filter filter;

	public FilterableAllMemberSet(FilterableHierarchy hierarchy, String level, Filter filter) {
		this(hierarchy, level, null, filter);
	}

	public FilterableAllMemberSet(FilterableHierarchy hierarchy, String level, Member parent, Filter filter) {
		super(hierarchy, level, parent);
		this.filter = filter;
	}

	public Filter getFilter() {
		return filter;
	}
	
	@Override
	public List<Tuple> getTuples() {
		hierarchy = Cube.OBJECT;
		return new LinkedList<Tuple>(hierarchy.getMembers(getLevel(), getParent(), filter));
	}

	@Override
	public void buildQuery(PlanProcessor planProcessor) {
		planProcessor.processSet(this);
	}
}
