package com.synapsense.service.reporting.olap.custom;

import static com.synapsense.service.reporting.olap.custom.Cube.DIM_TIME;
import static com.synapsense.service.reporting.olap.custom.Cube.LEVEL_DAY;
import static com.synapsense.service.reporting.olap.custom.Cube.LEVEL_FIVE_MINUTES;
import static com.synapsense.service.reporting.olap.custom.Cube.LEVEL_HALF_HOUR;
import static com.synapsense.service.reporting.olap.custom.Cube.LEVEL_HOUR;
import static com.synapsense.service.reporting.olap.custom.Cube.LEVEL_MONTH;
import static com.synapsense.service.reporting.olap.custom.Cube.LEVEL_ONE_MINUTE;
import static com.synapsense.service.reporting.olap.custom.Cube.LEVEL_SECOND;
import static com.synapsense.service.reporting.olap.custom.Cube.LEVEL_YEAR;

import java.io.Serializable;
import java.util.Date;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.synapsense.exception.IllegalInputParameterException;
import com.synapsense.service.reporting.olap.BaseHierarchy;
import com.synapsense.service.reporting.olap.Member;
import com.synapsense.service.reporting.olap.OrderedHierarchy;
import com.synapsense.service.reporting.olap.RangeMember;
import com.synapsense.service.reporting.olap.SimpleMember;
import com.synapsense.util.CollectionUtils;
import com.synapsense.util.Pair;

public class TimeHierarchy extends BaseHierarchy implements OrderedHierarchy,
		Serializable {

	private static final long serialVersionUID = 1766405769331842467L;
	
	private static final Logger log = Logger.getLogger(TimeHierarchy.class);

	public static final int PERIOD = 24;
	public static final int PERIOD_FIELD = Calendar.MONTH;
	public static final String THIS_PERIOD = "This";
	public static final String PREVIOUS_PERIOD = "Previous";

	private static Map<String, Integer> levelFieldMap = CollectionUtils.newLinkedMap();

	static {
		levelFieldMap.put(Cube.LEVEL_YEAR, Calendar.YEAR);
		levelFieldMap.put(Cube.LEVEL_MONTH, Calendar.MONTH);
		levelFieldMap.put(Cube.LEVEL_DAY, Calendar.DAY_OF_MONTH);
		levelFieldMap.put(Cube.LEVEL_HOUR, Calendar.HOUR_OF_DAY);
		levelFieldMap.put(Cube.LEVEL_ONE_MINUTE, Calendar.MINUTE);
		levelFieldMap.put(Cube.LEVEL_SECOND, Calendar.SECOND);
		levelFieldMap.put(Cube.LEVEL_WEEK, Calendar.WEEK_OF_YEAR);
	}

	private String root;

	public TimeHierarchy() {
		super(DIM_TIME, Integer.class, LEVEL_YEAR, LEVEL_MONTH, LEVEL_DAY,
				LEVEL_HOUR, LEVEL_ONE_MINUTE, LEVEL_SECOND);
		root = LEVEL_YEAR;
	}
	
	public TimeHierarchy(String name, Class<?> memberType, String... levels) {
		super(name, memberType, levels);
		root = LEVEL_YEAR;
	}
	
	public static int getCalendarIndex(String level) {
		return levelFieldMap.get(level);
	}
	
	private static Map<String, Integer> maximumMap = CollectionUtils.newMap();
	
	static{
		maximumMap.put(Cube.LEVEL_ONE_MINUTE, 60);
		maximumMap.put(Cube.LEVEL_HOUR, 24);
		maximumMap.put(Cube.LEVEL_DAY, 31);
		maximumMap.put(Cube.LEVEL_MONTH, 12);
	}
	
	public static int getMaximum(String level){
		return maximumMap.get(level);
	}
	
	@Override
	public boolean isFullMember(Member member) {
		Member current = member;
		while (!current.getLevel().equals(Cube.LEVEL_YEAR)
				&& current.getParent() != null){
			current = current.getParent();
		}
		return current.getLevel().equals(Cube.LEVEL_YEAR);  
	}
	
	@Override
	public boolean isInHistoricalPeriod(Member member) {
		List<Member> members = member.getSimpleMembers();
		if (members.size() == 1) {
			return isInPeriod(members.iterator().next());
		}
 		Member startMember = members.iterator().next();
		Member endMember = members.get(members.size() - 1);
		return isInPeriod(startMember)
				&& isInPeriod(endMember);
	}

	@Override
	public Calendar getCalendarOfMember(Member member) {
		Calendar cal = Calendar.getInstance();
		cal.clear();
		Member cur = member;
		while (cur != null) {
			cal.set(levelFieldMap.get(cur.getLevel()),
					cur.getLevel().equals(Cube.LEVEL_MONTH) ? (int) cur
							.getKey() - 1 : (int) cur.getKey());
			cur = cur.getParent();
		}
		
		return cal;
	}

	@Override
	public Member getMemberOfCalendar(Calendar calendar, Level bottomLevel,
			Level upLevel) {
		
		Member member = null;
		Member parent = null;
		String currentLevel = upLevel.getName();
		while (currentLevel != null 
				&& !currentLevel.equals(getChildLevel(bottomLevel.getName()))) {
			int value = calendar.get(getCalendarIndex(currentLevel));
			value = (currentLevel.equals(Cube.LEVEL_MONTH)) ? ++value : value;
			member = new SimpleMember(this, currentLevel, value, parent);
			parent = member;
			currentLevel = getChildLevel(currentLevel);
		}
		return member;
	}
	
	@Override
	public List<Member> getMembers(Level customLevel) {
		String level = customLevel.getName();
		if (level.equals(root)) {
			return getAllRootMembers();
		}
		Level parentLevel = new BaseLevel(this, getParentLevel(level));
		List<Member> parents = getMembers(parentLevel);
		List<Member> members = CollectionUtils.newList();
		for (Member parent : parents) {
			members.addAll(getMembers(customLevel, parent));
		}
		return members;
	}

	@Override
	public List<Member> getMembers(Level customLevel, String limitLevel) {
		String level = customLevel.getName();
		if (limitLevel == null) {
			limitLevel = root;
		}
		if (level.equals(root)) {
			return getAllRootMembers();
		}
		List<Member> parents = CollectionUtils.newList();
		List<Member> members = new LinkedList<>();
		if (level.equals(limitLevel)) {
			members = getDistinctMembers(level);
		} else {
			Level parentLevel = new BaseLevel(this, getParentLevel(level));
			parents = getMembers(parentLevel);
			for (Member parent : parents) {
				members.addAll(getMembers(customLevel, parent));
			}
		}
		return members;
	}
	
	@Override
	public List<Member> getMembers(Level customLevel, Member parent) {
		if (customLevel.getName().equals(root)) {
			return getAllRootMembers();
		}
		return isFullMember(parent) 
				? getPeriodMembers(getAllMembers(customLevel, parent))
					: getAllMembers(customLevel, parent);
	}

	@Override
	public List<Member> getMembers(Member startMember, Member endMember) {
		int startValue = (int) startMember.getKey();
		int endValue = (int) endMember.getKey();
		String level = startMember.getLevel();
		int startMin = getCalendarOfMember(startMember).getActualMinimum(
				levelFieldMap.get(level));
		int endMax = getCalendarOfMember(endMember).getActualMaximum(
				levelFieldMap.get(level));
		if (startValue < (level.equals(Cube.LEVEL_MONTH) ? startMin + 1
				: startMin)
				|| endValue > (level.equals(Cube.LEVEL_MONTH) ? endMax + 1
						: endMax)) {
			throw new IllegalArgumentException("Argument is illegal: start "
					+ startValue + ", end " + endValue);
		}
		List<Member> parents = CollectionUtils.newList();
		List<Member> members = CollectionUtils.newList();
		int start = 0, end = 0;
		if (startMember.getParent() != null) {
			Member startParent = startMember.getParent();
			Member endParent = endMember.getParent();
			parents = getMembers(startParent, endParent);
			for (Member parent : parents) {
				start = parent.equals(startParent) ? startValue
						: getCalendarOfMember(parent).getActualMinimum(
								levelFieldMap.get(level));
				end = parent.equals(endParent) ? endValue
						: getCalendarOfMember(parent).getActualMaximum(
								levelFieldMap.get(level));
				for (int i = start; i <= end; i++) {
					Member member = new SimpleMember(this, level, i, parent);
					if (isInPeriod(member)) {
						members.add(member);
					}
				}
			}
			return members;
		} else {
			if (level.equals(root)) {
				List<Member> allMembers = getAllRootMembers();
				start = allMembers.indexOf(startMember);
				end = allMembers.indexOf(endMember);
				for (int i = start; i <= end; i++) {
					members.add(allMembers.get(i));
				}
				return members;
			}
			start = startValue;
			end = endValue;
			for (int i = start; i <= end; i++) {
				members.add(new SimpleMember(this, level, i));
			}
			return members;
		}
	}
	
	@Override
	public Member getNext(Member member) {
		Member parent = member.getParent();
		Level customLevel = member.getCustomLevel();
		List<Member> brothers = CollectionUtils.newList();
		if (parent == null) { 
			//Fake Parent
			Calendar calendar = Calendar.getInstance();
			Level parentLevel = new BaseLevel(this, getParentLevel(member.getLevel()));
			parent = Cube.TIME.getMemberOfCalendar(calendar, parentLevel, parentLevel);

			List<Member> fakeBrothers = getAllMembers(customLevel, parent);
			for (Member fakeBrother : fakeBrothers) {
				brothers.add(new SimpleMember(this, fakeBrother.getLevel(), fakeBrother.getKey()));
			}
			//case if last brother without parent
			if (brothers.indexOf(member) + 1 == brothers.size()) {
				Member lastBrother = brothers.get(brothers.indexOf(member));
				return new SimpleMember(this, lastBrother.getLevel(), (int)lastBrother.getKey() + 1);
			}
		} else {
			brothers = getAllMembers(customLevel, parent);
		}
		if (brothers.indexOf(member) + 1 < brothers.size()) {
			return brothers.get(brothers.indexOf(member) + 1);
		} else {
			Member nextParent = getNext(parent);
			return getAllMembers(customLevel, nextParent).iterator().next();
		}
	}
	
	@Override
	public Pair<Member, Member> getPeriod(TimePeriodType periodType) {
		Calendar calendar = Calendar.getInstance();
		
		Member currentSecondsTimeMember = Cube.TIME.getMemberOfCalendar(
				calendar, new BaseLevel(Cube.TIME, Cube.LEVEL_SECOND),
				new BaseLevel(Cube.TIME, Cube.LEVEL_YEAR));
		Member thisMember = getMemberOfCalendar(calendar, periodType.getLevel(), new BaseLevel(Cube.TIME, Cube.LEVEL_YEAR));
		
		calendar = getCalendarOfMember(thisMember);
		Member thisLevelSecondsTimeMember = Cube.TIME.getMemberOfCalendar(
				calendar, new BaseLevel(Cube.TIME, Cube.LEVEL_SECOND),
				new BaseLevel(Cube.TIME, Cube.LEVEL_YEAR));
		Member start = null;
		Member end = null;
		switch (periodType.getRelation()) {
		case THIS_PERIOD:
			start = thisLevelSecondsTimeMember;
			end = currentSecondsTimeMember;
			break;
		case PREVIOUS_PERIOD:
			Member previousMember = getPrevious(thisMember);
			Calendar prevCalendar = getCalendarOfMember(previousMember);
			start = Cube.TIME.getMemberOfCalendar(prevCalendar, new BaseLevel(
					Cube.TIME, Cube.LEVEL_SECOND), new BaseLevel(Cube.TIME,
					Cube.LEVEL_YEAR));
			end = Cube.TIME.getPrevious(thisLevelSecondsTimeMember);
			break;
		}
		return Pair.newPair(start, end);
	}

	@Override
	public Member getPrevious(Member member) {
		
		Member parent = member.getParent();
		Level customLevel = member.getCustomLevel();
		List<Member> brothers = getMembers(customLevel, parent);
		if (brothers.indexOf(member) != 0) {
			return brothers.get(brothers.indexOf(member) - 1);
		} else {
			Member previousParent = getPrevious(parent);
			List<Member> previousParentMembers = getMembers(customLevel, previousParent);
			return previousParentMembers.get(previousParentMembers.size() - 1);
		}

	}
	
	@Override
	public List<Member> getSimpleMembers(Member member) {
		List<Member> simpleMembers = CollectionUtils.newList();
		if (member.getCustomLevel().getMembersRange() == 1) {
			simpleMembers.add(member);
			return simpleMembers;
		}
		Member parent = member.getParent();
		String levelName = member.getCustomLevel().getName();
		Level simpleMembersLevel = new BaseLevel(Cube.TIME, levelName);
		List<Member> allSimpleMembers = CollectionUtils.newList();
		if (parent != null) {
			allSimpleMembers = getMembers(simpleMembersLevel, parent);
		}
		// Get start index
		int edgeStartMemberIndex = (((int) member.getKey() - 1) + RangeMember.START_POSITION)
				* member.getCustomLevel().getMembersRange()
				- RangeMember.START_POSITION;
		int startIncludedIndex = (edgeStartMemberIndex < allSimpleMembers
				.size()) ? edgeStartMemberIndex : allSimpleMembers.size() - 1;
		// Get end index
		int edgeEndMemberIndex = (((int) member.getKey() - 1) + RangeMember.END_POSITION)
				* member.getCustomLevel().getMembersRange()
				- RangeMember.END_POSITION;
		int endExcudedIndex = (edgeEndMemberIndex < allSimpleMembers.size()) ? edgeEndMemberIndex
				: allSimpleMembers.size() - 1;

		simpleMembers.addAll(allSimpleMembers.subList(startIncludedIndex,
				endExcudedIndex + 1));
		return new LinkedList<Member>(simpleMembers);
	}
	
	@Override
	public Member convert(Member member, OrderedHierarchy hierarchy, String bottomLevel) {
		
		String currentDimension = Cube.getDimension(this.getName());
		if (!Cube.getDimension(hierarchy.getName()).equals(currentDimension)) {
			throw new IllegalInputParameterException(
					"Hierarchy to convert to (" + hierarchy.getName()
							+ ") is not same as current hierarchy:"
							+ this.getName());
		}
		return this.getMemberOfCalendar(hierarchy.getCalendarOfMember(member),
				new BaseLevel(this, bottomLevel), new BaseLevel(this, root));
		
	}

	private boolean isInPeriod(Member member) {
		Calendar calendar = this.getCalendarOfMember(member);
		Date now = new Date();
		if (calendar.getTime().after(now)) {
			return false;
		}
		calendar.add(PERIOD_FIELD, PERIOD);

		if (calendar.getTime().before(now)) {
			return false;
		}
		return true;
	}
	
	private List<Member> getAllMembers(Level customLevel, Member parent) {
		String level = customLevel.getName();
		if (parent == null) {
			return getMembers(customLevel);
		}
		if (parent != null && !isDescendant(level, parent.getLevel())) {
			throw new IllegalArgumentException("parent level must be higher");
		}
		List<Member> members = new LinkedList<>();
		//For SimpleMember
		if (customLevel.getMembersRange() == 1
				&& !getParentLevel(level).equals(parent.getLevel())) {
			List<Member> parents = CollectionUtils.newList();
			Level parentLevel = new BaseLevel(this, getParentLevel(level));
			parents = getMembers(parentLevel, parent);
			
			Level simpleLevel = new BaseLevel(this, level);
			for (Member midParent : parents) {
				members.addAll(getMembers(simpleLevel, midParent));
			}
			return members;
		}
		//For RangeMember
		String calendarLevel = customLevel.getRangeLevel();
		Calendar cal = getCalendarOfMember(parent);
		int start = cal.getActualMinimum(levelFieldMap.get(calendarLevel));
		int end = cal.getActualMaximum(levelFieldMap.get(calendarLevel));
		if (level.equals(Cube.LEVEL_MONTH)) {
			start++;
			end++;
		}
		int membersRange = customLevel.getMembersRange();
		int rangeIndex = 1;
		for (int i = start; i <= end; i = i + membersRange, rangeIndex++) {
			Member member = (membersRange == 1) ? new SimpleMember(this, level,
					i, parent) : new RangeMember(customLevel, rangeIndex,
					parent);
			members.add(member);
		}
		return members;
	}

	private List<Member> getAllRootMembers() {
		LinkedList<Member> allMembers = new LinkedList<>();

		Calendar cal = Calendar.getInstance();
		int currentMember = cal.get(levelFieldMap.get(root));
		// subtract supported number of months
		cal.add(PERIOD_FIELD, -PERIOD);
		int oldestMember = cal.get(levelFieldMap.get(root));

		for (int i = oldestMember; i <= currentMember; i++) {
			allMembers.add(new SimpleMember(this, root, i));
		}
		return allMembers;
	}

	private List<Member> getDistinctMembers(String level) {
		if (level.equals(root)) {
			return getAllRootMembers();
		}

		List<Member> members = new LinkedList<>();
		Calendar cal = Calendar.getInstance();
		int start = cal.getActualMinimum(levelFieldMap.get(level));
		int end = cal.getActualMaximum(levelFieldMap.get(level));
		if (level.equals(Cube.LEVEL_MONTH)) {
			start++;
			end++;
		}
		for (int i = start; i <= end; i++) {
			Member member = new SimpleMember(this, level, i);
			members.add(member);
		}
		return members;
	}
	
	private List<Member> getPeriodMembers(List<Member> allMembers) {
		List<Member> periodMembers = CollectionUtils.newList();
		for (Member member : allMembers) {
			if (isInHistoricalPeriod(member)) {
				periodMembers.add(member);
			}
		}
		return periodMembers;
	}

    public static int getHistoricalPeriod() {
        return PERIOD;
    }
	
}
