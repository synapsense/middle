package com.synapsense.service.impl.messages;

import java.io.Serializable;

public class GWStatusMessage implements BusMessage<Integer, Long> {
	private long id;
	private int value;

	public GWStatusMessage(long id, int value) {
		this.id = id;
		this.value = value;
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public Type getType() {
		return Type.GWSTATUS;
	}

	@Override
	public Integer getValue() {
		return value;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public void setValue(Integer value) {
		this.value = value;
	}
}
