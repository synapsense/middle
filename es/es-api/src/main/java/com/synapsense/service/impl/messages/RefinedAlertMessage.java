package com.synapsense.service.impl.messages;

import com.synapsense.service.impl.messages.base.RefinedAlertData;

public final class RefinedAlertMessage implements BusMessage<RefinedAlertData, Integer> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3907864727499056950L;
	private RefinedAlertData payload;

	public RefinedAlertMessage(RefinedAlertData payload) {
		this.payload = payload;
	}

	@Override
	public Integer getId() {
		return null;
	}

	@Override
	public Type getType() {
		return Type.REFINED_ALERT;
	}

	@Override
	public RefinedAlertData getValue() {
		return payload;
	}

	@Override
	public void setId(Integer id) {
		throw new UnsupportedOperationException("Not applicable for immutable RefinedAlertMessage.");
	}

	@Override
	public void setValue(RefinedAlertData value) {
		throw new UnsupportedOperationException("Not applicable for immutable RefinedAlertMessage.");
	}
}
