package com.synapsense.service.reporting.olap;

import java.util.List;

/**
 * Top interface of OLAP like hierarchy
 * 
 * @author Dmitry Grudzinskiy
 *
 */
public interface Hierarchy {

	/**
	 * Hierarchy name
	 * 
	 * @return unique name of the current hierarchy
	 */
	String getName();

	/**
	 * Member type
	 * 
	 * @return type of members' keys supported by this hierarchy
	 */
	Class<?> getMemberType();

	/**
	 * Determines whether one level is a descendant of the other one
	 * 
	 * @param desc
	 *            level to check whether it's a descendant
	 * @param ans
	 *            level to check whether it's an ancestor
	 * 
	 * @return true if the first level is a descendant of the second one
	 */
	boolean isDescendant(String desc, String ans);

	/**
	 * Gets the simple members.
	 *
	 * @param member the member
	 * @return the simple members
	 */
	List<Member> getSimpleMembers(Member member);
}