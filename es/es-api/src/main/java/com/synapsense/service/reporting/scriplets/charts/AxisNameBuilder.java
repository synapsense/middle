package com.synapsense.service.reporting.scriplets.charts;

import java.util.HashMap;
import java.util.List;

abstract public class AxisNameBuilder {
	protected HashMap <String, List<String>> serieIdAxisNamesMap = new HashMap <String, List<String>>();
	protected HashMap <String, String> serieIdSerieUnitMap = new HashMap <String, String>();
	
	public void clearAllData(){
		serieIdAxisNamesMap.clear();
		serieIdSerieUnitMap.clear();
	}
	
	abstract public void addAxisNameAndUnit(String serie_id, String axis_name, String serie_unit);
	abstract public HashMap <String, String> getAxisNameMap();
}
