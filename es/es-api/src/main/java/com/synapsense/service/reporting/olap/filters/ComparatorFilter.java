package com.synapsense.service.reporting.olap.filters;

import java.io.Serializable;
import java.util.*;

import com.synapsense.service.reporting.datasource.ReferenceObjectType;
import com.synapsense.service.reporting.olap.Filter;
import com.synapsense.util.CollectionUtils;

@SuppressWarnings({ "rawtypes", "unchecked" })
public class ComparatorFilter implements Filter, Serializable {

	private static final long serialVersionUID = 2872823399873730840L;
    private ReferenceObjectType objectType = null;
    private String property;
	private Comparable value;
	private List<Number> acceptables = CollectionUtils.newList();
	private boolean calculated = false;

	public ComparatorFilter(String property, Comparable value, Number... acceptables) {
		this.property = property;
		this.value = value;
		this.acceptables = Arrays.asList(acceptables);
	}

	public ComparatorFilter(String property, Comparable value, boolean calculated, Number... acceptables) {
		this.property = property;
		this.value = value;
		this.acceptables = Arrays.asList(acceptables);
		this.calculated = calculated;
	}

    public ComparatorFilter(ReferenceObjectType objectType, Comparable value, Number... acceptables) {
        this.objectType = objectType;
        this.value = value;
        this.acceptables = Arrays.asList(acceptables);
    }

    public ReportFilterType getFilterType() {
        return ReportFilterType.getTypeByAcceptables(acceptables.toArray(new Number[]{}));
    }

    @Override
	public Collection<String> getProperties() {
		return CollectionUtils.list(property);
	}

	@Override
	public boolean accept(Map<String, Object> values) {
        if(values.get(property) == null) {
            return false;
        }
		return acceptables.contains(value.compareTo(values.get(property)));
	}

    public boolean isCalculated() {
        return calculated;
    }

    public void setValue(Comparable value) {
        this.value = value;
    }

    public Comparable getValue() {
        return value;
    }

    public ReferenceObjectType getObjectType() {
        return objectType;
    }

    public List<Number> getAcceptables() {
        return Collections.unmodifiableList(acceptables);
    }
}
