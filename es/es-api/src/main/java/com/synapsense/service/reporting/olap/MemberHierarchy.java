package com.synapsense.service.reporting.olap;

import java.util.List;

import com.synapsense.service.reporting.olap.custom.Level;

public interface MemberHierarchy extends Hierarchy {
	/**
	 * Finds all members for the specified level
	 * 
	 * @param customLevel
	 *            level from this hierarchy to find members for
	 * @return all members for the specified level
	 */
	List<Member> getMembers(Level customLevel);
	
	/**
	 * Finds all members for the specified level till limit level
	 * 
	 * @param customLevel
	 *            level from this hierarchy to find members for
	 * @param parent
	 *            parent member that must be from this hierarchy
	 *            
	 * @return all members for the specified level
	 */
	List<Member> getMembers(Level customLevel, Member parent);

	/**
	 * Finds all members for the specified level till limit level
	 * 
	 * @param customLevel
	 *            level from this hierarchy to find members for
	 * @param limitLevel
	 *            level from this hierarchy to find members till
	 *            
	 * @return all members for the specified level
	 */
	List<Member> getMembers(Level customLevel, String limitLevel);
}
