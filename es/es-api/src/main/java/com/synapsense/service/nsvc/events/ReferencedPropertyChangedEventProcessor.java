package com.synapsense.service.nsvc.events;

public interface ReferencedPropertyChangedEventProcessor {
	ReferencedPropertyChangedEvent process(ReferencedPropertyChangedEvent e);
}
