package com.synapsense.service.reporting.scriplets.charts;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;
import com.synapsense.util.CollectionUtils;

public class AdvancedChartData {
	
	private ListMultimap<String, Number> dimensionMeanMap;

	private Map<String, Number> valueAxisMaximumMap;

	private Map<String, Number> valueAxisMinimumMap;
	
	private Map<String, Integer> aggregationColorsMap = CollectionUtils.newMap();
	
	private List<ThresholdData> thresholds = CollectionUtils.newList();

	private AdvancedChartData() {}

	private AdvancedChartData(AdvancedChartDataBuilder builder) {
		this.dimensionMeanMap = builder.getDimensionMeanMap();
		this.valueAxisMaximumMap = builder.getValueAxisMaximumMap();
		this.valueAxisMinimumMap = builder.getValueAxisMinimumMap();
		this.aggregationColorsMap = builder.getAggregationColorsMap();
		this.thresholds = builder.getThresholds();
	}
	
	public ListMultimap<String, Number> getDimensionMeanMap() {
		return dimensionMeanMap;
	}

	public Map<String, Number> getValueAxisMaximumMap() {
		return Collections.unmodifiableMap(valueAxisMaximumMap);
	}

	public Map<String, Number> getValueAxisMinimumMap() {
		return Collections.unmodifiableMap(valueAxisMinimumMap);
	}
		
	public Map<String, Integer> getAggregationColorsMap() {
		return Collections.unmodifiableMap(aggregationColorsMap);
	}
	
	public List<ThresholdData> getThresholds() {
		return Collections.unmodifiableList(thresholds);
	}

	public static class AdvancedChartDataBuilder {

		private ListMultimap<String, Number> dimensionMeanMap = ArrayListMultimap.create();

		private Map<String, Number> valueAxisMaximumMap = CollectionUtils.newMap();

		private Map<String, Number> valueAxisMinimumMap = CollectionUtils.newMap();
		
		private Map<String, Integer> aggregationColorsMap = CollectionUtils.newMap();
		
		private List<ThresholdData> thresholds = CollectionUtils.newList();
		
        public AdvancedChartDataBuilder() {
        }
        
		public AdvancedChartData build() {
            return new AdvancedChartData(this);
        }
        
        public AdvancedChartDataBuilder dimensionMeanMap(ListMultimap<String, Number> dimensionMeanMap) {
        	this.dimensionMeanMap.putAll(dimensionMeanMap);
        	return this;
        }

        public AdvancedChartDataBuilder valueAxisMaximumMap(Map<String, Number> valueAxisMaximumMap) {
        	this.valueAxisMaximumMap.putAll(valueAxisMaximumMap);
        	return this;
        }
        
        public AdvancedChartDataBuilder valueAxisMinimumMap(Map<String, Number> valueAxisMinimumMap) {
        	this.valueAxisMinimumMap.putAll(valueAxisMinimumMap);
        	return this;
        }	   
        
        public AdvancedChartDataBuilder thresholds(List<ThresholdData> thresholds) {
        	this.thresholds.addAll(thresholds);
        	return this;
        }
        
        
        public AdvancedChartDataBuilder aggregationColorsMap(Map<String, Integer> aggregationColorsMap) {
        	this.aggregationColorsMap.putAll(aggregationColorsMap);
        	return this;
        }	 

		ListMultimap<String, Number> getDimensionMeanMap() {
			return dimensionMeanMap;
		}

		Map<String, Number> getValueAxisMaximumMap() {
			return Collections.unmodifiableMap(valueAxisMaximumMap);
		}

		Map<String, Number> getValueAxisMinimumMap() {
			return Collections.unmodifiableMap(valueAxisMinimumMap);
		}
		
    	Map<String, Integer> getAggregationColorsMap() {
			return Collections.unmodifiableMap(aggregationColorsMap);
		}

		List<ThresholdData> getThresholds() {
    		return Collections.unmodifiableList(thresholds);
    	}
    	
	}
	
	public static class ThresholdData {
		
		private Number startValue;
		
		private Integer startColor;
		
		private Number endValue;
		
		private Integer rangeColor;		
		
		private ChartThresholdType thresholdType;

		ThresholdData(Number startValue, Integer startColor,
				Number endValue, Integer rangeColor) {
			this.startValue = startValue;
			this.startColor = startColor;
			this.endValue = endValue;
			this.rangeColor = rangeColor;
			if (endValue == null) {
				this.thresholdType = ChartThresholdType.LINE;
			} else {
				this.thresholdType = ChartThresholdType.BAR;
			}
		}

		public Number getStartValue() {
			return startValue;
		}

		public Integer getStartColor() {
			return startColor;
		}

		public Number getEndValue() {
			return endValue;
		}

		public Integer getRangeColor() {
			return rangeColor;
		}

		public ChartThresholdType getThresholdType() {
			return thresholdType;
		}
		
	}
	
	public enum ChartThresholdType {
	    LINE,
	    BAR
    }

}