package com.synapsense.service.reporting.dto;

import java.io.Serializable;

public final class CommandStatus implements Serializable {

	public static final CommandStatus NOT_STARTED = notStarted("");

	public static final CommandStatus IN_PROGRESS = inProgress("");

	public static final CommandStatus CANCELLED = cancelled("");

	public static final CommandStatus COMPLETED = completed("");

	public static final CommandStatus FAILED = failed("");

	public static final CommandStatus SUSPENDED = suspended("");

	private static final long serialVersionUID = -7866600901777850387L;

	private String name;
	private String message;

	public String toString() {
		return name;
	}

	private CommandStatus(String statusName, String message) {
		this.message = message;
		this.name = statusName;
	}

	public String getStatusMessage() {
		return message;
	}

	public static CommandStatus failed(final String message) {
		return new CommandStatus("FAILED", message);
	}

	public static CommandStatus notStarted(final String message) {
		return new CommandStatus("NOT STARTED", message);
	}

	public static CommandStatus cancelled(final String message) {
		return new CommandStatus("CANCELLED", message);
	}

	public static CommandStatus completed(final String message) {
		return new CommandStatus("COMPLETED", message);
	}

	public static CommandStatus inProgress(final String message) {
		return new CommandStatus("IN PROGRESS", message);
	}

	public static CommandStatus suspended(final String message) {
		return new CommandStatus("SUSPENDED", message);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof CommandStatus)) {
			return false;
		}
		final CommandStatus other = (CommandStatus) obj;
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		return true;
	}
}
