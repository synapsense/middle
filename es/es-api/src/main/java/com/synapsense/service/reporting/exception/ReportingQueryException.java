package com.synapsense.service.reporting.exception;

import javax.ejb.ApplicationException;


@ApplicationException(rollback = true)
public class ReportingQueryException extends ReportingServiceSystemException {

	private static final long serialVersionUID = 3143324792177683242L;

	public ReportingQueryException(String message, Throwable cause) {
		super(message, cause);
	}

	public ReportingQueryException(String message) {
		this(message, null);
	}

	public ReportingQueryException(Throwable cause) {
		this("", cause);
	}
}
