package com.synapsense.service.reporting;

import com.synapsense.dto.TO;
import com.synapsense.service.reporting.datasource.ReportType;
import com.synapsense.service.reporting.olap.RangeSet;
import com.synapsense.service.reporting.olap.custom.EnvironmentDataSource;
import com.synapsense.util.unitconverter.UnitResolvers;
import com.synapsense.util.unitconverter.UnitSystems;

import java.util.List;

/**
 * Created by ryabovgs on 1/26/14.
 * The interface builds DimensionalQuery from some Report Info,
 * like DB object holding report table content.
 */
public interface DimensionalQuieryBuilder {

    /**
     * Builds the DimensionalQuery.
     *
     *
     * @param parent the parent
     * @param dimensionNames the dimension names
     * @param period the period
     * @param ds the ds
     * @param unitResolvers the unit resolvers
     * @param unitSystems the unit systems
     * @param formula
     * @return the dimensional query
     */
    DimensionalQuery build(TO<?> parent, List<String> dimensionNames, RangeSet period, EnvironmentDataSource ds
            , UnitResolvers unitResolvers, UnitSystems unitSystems, Formula formula);

    ReportType getReportType();
}
