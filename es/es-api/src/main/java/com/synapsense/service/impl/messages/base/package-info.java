/**
 * Contains classes used as a payload of Environment Server
 * incoming data messages.
 */
package com.synapsense.service.impl.messages.base;