package com.synapsense.service.reporting.scriplets.charts;

public class ValueRanges {
	public double rMax = 0;
	public double rMin = 0;
	public double aMax = 0;
	public double aMin = 0;
	public Float min = 0.0f;
	public Float max = 0.0f;
	
	public String packToStr(){
	    String result = min + "," + max + "," + aMin + "," + aMax + "," + rMin + "," + rMax;
	    return result;
	}
}
