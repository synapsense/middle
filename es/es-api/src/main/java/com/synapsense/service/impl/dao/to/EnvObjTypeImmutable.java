package com.synapsense.service.impl.dao.to;

import java.io.Serializable;
import java.util.Collections;
import java.util.Set;

import com.synapsense.dto.ObjectType;
import com.synapsense.dto.PropertyDescr;

public class EnvObjTypeImmutable implements ObjectType, Serializable {
	private static final long serialVersionUID = -6581570900320595486L;

	private ObjectType type;

	public EnvObjTypeImmutable(ObjectType objType) {
		this.type = new EnvObjTypeTO(objType);
	}

	@Override
	public Set<PropertyDescr> getPropertyDescriptors() {
		return Collections.unmodifiableSet(type.getPropertyDescriptors());
	}

	@Override
	public String getName() {
		return type.getName();
	}

	@Override
	public PropertyDescr getPropertyDescriptor(String propertyName) {
		return type.getPropertyDescriptor(propertyName);
	}

	@Override
	public void setName(String name) {
		throw new UnsupportedOperationException();
	}

}
