package com.synapsense.service.impl.dao.to;

import java.io.Serializable;
import java.util.Arrays;

import javax.xml.bind.DatatypeConverter;

import com.synapsense.dto.BinaryData;

/**
 * Implementation of property type <code>BinaryData</code>
 * 
 * @author aalexeev
 * 
 */
final public class BinaryDataImpl implements BinaryData, Serializable {

	private static final long serialVersionUID = -697845756053227006L;

	private static final int MAX_VALUE_SIZE = 16 * 1024 * 1024 - 1;

	private byte[] value;

	public BinaryDataImpl(final byte[] arr) {
		setValue(arr);
	}

	@Override
	public byte[] getValue() {
		return value;
	}

	@Override
	public void setValue(final byte[] arr) {
		if (arr.length > MAX_VALUE_SIZE) {
			throw new IllegalArgumentException("Supplied binary array is too big! Max size is " + MAX_VALUE_SIZE
			        + " bytes");
		}
		value = arr;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(value);
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final BinaryDataImpl other = (BinaryDataImpl) obj;
		if (!Arrays.equals(value, other.value))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return DatatypeConverter.printBase64Binary(value);
	}
}
