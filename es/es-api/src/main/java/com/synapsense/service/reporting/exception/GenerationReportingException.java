package com.synapsense.service.reporting.exception;

import javax.ejb.ApplicationException;

import com.synapsense.service.reporting.management.ReportingException;

@ApplicationException(rollback = false)
public class GenerationReportingException extends ReportingException {

	private static final long serialVersionUID = -7814155184675786304L;

	public GenerationReportingException() {
	}

	public GenerationReportingException(String message, Throwable cause) {
		super(message, cause);
	}

	public GenerationReportingException(String message) {
		super(message);
	}

	public GenerationReportingException(Throwable cause) {
		super(cause);
	}
}
