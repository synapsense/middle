package com.synapsense.service.impl.dao.to;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import com.synapsense.dto.ObjectType;
import com.synapsense.dto.PropertyDescr;

public class EnvObjTypeTO implements ObjectType, Serializable {

	private static final long serialVersionUID = -9155200932966234337L;

	private String name;

	private String description;

	private Set<PropertyDescr> propertyDescriptors = new HashSet<PropertyDescr>();

	public EnvObjTypeTO(String name, String description) {
		setName(name);
		setDescription(description);
	}

	/**
	 * Copying constructor.
	 * 
	 * @param objType
	 *            Object type instance to copy
	 */
	public EnvObjTypeTO(ObjectType objType) {
		this(objType.getName(), null);
		for (PropertyDescr pd : objType.getPropertyDescriptors()) {
			propertyDescriptors.add(new PropertyDescr(pd));
		}
	}

	public String getDescription() {
		return this.description;
	}

	public String getName() {
		return name;
	}

	public Set<PropertyDescr> getPropertyDescriptors() {
		return this.propertyDescriptors;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setName(String name) {
		if (name == null) {
			throw new IllegalArgumentException("Supplied 'name' is null");
		}
		if (name.isEmpty()) {
			throw new IllegalArgumentException("Supplied 'name' is empty");
		}
		this.name = name;
	}

	public PropertyDescr getPropertyDescriptor(String propertyName) {
		for (PropertyDescr pd : this.getPropertyDescriptors()) {
			if (pd.getName().equals(propertyName)) {
				return pd;
			}
		}
		return null;
	}

}
