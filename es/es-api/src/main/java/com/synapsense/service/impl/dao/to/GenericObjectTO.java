package com.synapsense.service.impl.dao.to;

import java.io.Serializable;

public class GenericObjectTO extends AbstractTO<Integer> implements Serializable {

	private static final long serialVersionUID = 7103492838873416023L;

	public GenericObjectTO(Integer id, String typeName) {
		super(id, typeName);
	}

	public GenericObjectTO(Integer id, String typeName, Integer esId) {
		super(id, typeName, esId);
	}

}