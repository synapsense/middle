package com.synapsense.service.reporting.management;

import com.synapsense.service.reporting.dto.ReportTaskInfo;
import com.synapsense.service.reporting.exception.SchedulingReportingException;

import java.util.Collection;

public interface ReportingSchedulingService {
	void validateReportTask(ReportTaskInfo taskInfo) throws SchedulingReportingException;
	
	void scheduleReportTask(ReportTaskInfo taskInfo) throws SchedulingReportingException;

	Collection<ReportTaskInfo> getScheduledTasks(String reportTemplateName) throws SchedulingReportingException;

	boolean removeScheduledTask(String taskName, String reportTemplateName) throws SchedulingReportingException;

	void removeScheduledTasks(String reportTemplateName) throws SchedulingReportingException;
}
