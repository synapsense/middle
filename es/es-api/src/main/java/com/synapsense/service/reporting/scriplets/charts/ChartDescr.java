package com.synapsense.service.reporting.scriplets.charts;

import java.awt.Color;
import java.awt.Font;
import java.util.List;
import java.util.Map;

import org.jfree.chart.JFreeChart;
import org.jfree.chart.block.BlockBorder;
import org.jfree.chart.block.BlockContainer;
import org.jfree.chart.block.BorderArrangement;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.title.LegendTitle;
import org.jfree.ui.HorizontalAlignment;
import org.jfree.ui.RectangleEdge;
import org.jfree.ui.RectangleInsets;
import org.jfree.ui.VerticalAlignment;

import com.synapsense.service.reporting.scriplets.charts.AdvancedChartData.AdvancedChartDataBuilder;
import com.synapsense.service.reporting.scriplets.charts.AdvancedChartData.ThresholdData;
import com.synapsense.util.CollectionUtils;

import static com.synapsense.service.reporting.scriplets.charts.ChartDescr.CQ_FIELD.*;
import static com.synapsense.service.reporting.scriplets.charts.ChartDescr.CQ_FIELD.THRESHOLD_FIELD.*;

public class ChartDescr {
	private RectangleEdge legendPosition = RectangleEdge.BOTTOM;
	private boolean isLegendVisible = true;
	private Color backgroundPaint = Color.white;
	private String chartTitle = "";
			
	//Temp attributes, should be wrapped to the "legend" class
	private RectangleInsets legendPadding = null;
	private BlockBorder legendBorder = new BlockBorder(0.0, 0.0, 0.0, 0.0);//new BlockBorder(1.0, 1.0, 1.0, 1.0); 
	private HorizontalAlignment legendHAlignment = HorizontalAlignment.CENTER;
	private VerticalAlignment legendVAlignment = null;
	//end temp
	
	private Color canvasColor = null;
	private Font chartFont = new Font("Arial", Font.PLAIN, 11);	
	
	private AdvancedChartData advancedChartData;
			
	public ChartDescr(){		
		setDefaultValues();
	}
	
	public ChartDescr(String chart_title, RectangleEdge legend_position, boolean is_legend_visible, Color background_paint){
		setDefaultValues();		
		legendPosition = legend_position;
		backgroundPaint = background_paint;
		isLegendVisible = is_legend_visible;
		chartTitle = chart_title;
	}
	
	public ChartDescr(String chartTitle, RectangleEdge legendPosition,
			boolean isLegendVisible, Color backgroundPaint,
			AdvancedChartData advancedChartData) {
		this(chartTitle, legendPosition, isLegendVisible, backgroundPaint);
		this.advancedChartData = advancedChartData;
	}
	
	private void setDefaultValues(){
//		if(ProductManager.HP_PRODUCT.equals(ProductManager.getInstance().getProduct())){
//			legendPadding = new RectangleInsets(0, 12, 0, 0);
//			legendBorder = new BlockBorder(0.0, 0.0, 0.0, 0.0);
//			legendHAlignment = HorizontalAlignment.RIGHT;
//			legendVAlignment = VerticalAlignment.TOP;
//			legendPosition = RectangleEdge.RIGHT;
//			canvasColor = Color.white;
//			chartFont = new Font("Arial", Font.PLAIN, 11);
//		}
	}		

	public static ChartDescr unpackFromString(String str) {
		
		ChartDescr result = null;
		int titleIndex = 2;
		int positionIndex = 3;
		String [] dataArr = str.split(",");
		
		int colorInt = Integer.parseInt(dataArr[0]);		
		
		boolean showLegend = Boolean.parseBoolean(dataArr[1]);
		
		String title = "";
		if(dataArr.length > titleIndex){
		   title = dataArr[titleIndex];	
		}		
		
		String positionStr = "";		
		if(dataArr.length > positionIndex){
			positionStr = dataArr[positionIndex];
		}
				
		RectangleEdge legendPosition = unpackLegendPositionFromStr(positionStr);
		
//		/*RectangleEdge.BOTTOM;
//		if("bottom".equalsIgnoreCase(positionStr)){
//			legendPosition = RectangleEdge.BOTTOM;
//		}else if("top".equalsIgnoreCase(positionStr)){
//			legendPosition = RectangleEdge.TOP;
//		}else if("left".equalsIgnoreCase(positionStr)){
//			legendPosition = RectangleEdge.LEFT;
//		}else if("right".equalsIgnoreCase(positionStr)){
//			legendPosition = RectangleEdge.RIGHT;
//		}*/

		if (!ChartDescr.isAdvancedChart(str)) {
			result = new ChartDescr(title, legendPosition, showLegend, new Color(colorInt));			
			return result;
		}
		
//		Advanced Chart Example - 16777215,true,Data Analysis Chart,bottom,32768,255,8388736,true,80:16711680:null:-1:null;20:16711680:null:-1:null

		Map<String, Integer> aggregationColorsMap = CollectionUtils.newMap();
		int avgLineColor = Integer.valueOf(dataArr[AVG_LINE_COLOR]);
		if (avgLineColor != EMPTY) {
			aggregationColorsMap.put("avg", avgLineColor);
		}
		
		int minLineColor = Integer.valueOf(dataArr[MIN_LINE_COLOR]);
		if (minLineColor != EMPTY) {
			aggregationColorsMap.put("min", minLineColor);
		}
		
		int maxLineColor = Integer.valueOf(dataArr[MAX_LINE_COLOR]);
		if (maxLineColor != EMPTY) {
			aggregationColorsMap.put("max", maxLineColor);
		}
		List<ThresholdData> thresholds = CollectionUtils.newList();
        if (Boolean.valueOf(dataArr[USE_THRESHOLDS])) {
			thresholds = getThresholds(dataArr[THRESHOLDS]);
		}
		AdvancedChartData advancedChartData = new AdvancedChartDataBuilder()
			.aggregationColorsMap(aggregationColorsMap)
			.thresholds(thresholds)
			.build();
		
		result = new ChartDescr(title, legendPosition, showLegend, new Color(colorInt), advancedChartData);
		
		return result;
		
	}
	
	
	public static List<ThresholdData> getThresholds(String thresholdsString) {
		List<ThresholdData> thresholds = CollectionUtils.newList();
		String [] thresholdsArray = thresholdsString.split(";");
		for (String thresholdString : thresholdsArray) {
			String [] thresholdArray = thresholdString.split(":");
			thresholds.add(new ThresholdData((thresholdArray[START_VALUE].equals("null")) ? null : Integer.valueOf(thresholdArray[START_VALUE])
					, Integer.valueOf(thresholdArray[START_COLOR])
					, (thresholdArray[END_VALUE].equals("null")) ? null : Integer.valueOf(thresholdArray[END_VALUE])
					, Integer.valueOf(thresholdArray[RANGE_COLOR])));
		}
		return thresholds;
	}
	
	public static final class CQ_FIELD {
		public static final int COLOR_INT = 0;
		public static final int SHOW_LEGEND = 1;
		public static final int TITLE = 2;
		public static final int LEGEND_POSITION = 3;
		public static final int AVG_LINE_COLOR = 4;
		public static final int MIN_LINE_COLOR = 5;
		public static final int MAX_LINE_COLOR = 6;
		public static final int USE_THRESHOLDS = 7;
		public static final int THRESHOLDS = 8;
		public static final int EMPTY = -1;
		
		private CQ_FIELD() {
		}
		
		public static final class THRESHOLD_FIELD {
			
			public static final int START_VALUE = 0;
			public static final int START_COLOR = 1;
			public static final int END_VALUE = 2;
			public static final int RANGE_COLOR = 3;
			
			private THRESHOLD_FIELD() {
			}
		}
	}
	
	public AdvancedChartData getAdvancedChartData() {
		return advancedChartData;
	}

	public static RectangleEdge unpackLegendPositionFromStr(String positionStr){
		RectangleEdge legendPosition = RectangleEdge.BOTTOM;
		if("bottom".equalsIgnoreCase(positionStr)){
			legendPosition = RectangleEdge.BOTTOM;
		}else if("top".equalsIgnoreCase(positionStr)){
			legendPosition = RectangleEdge.TOP;
		}else if("left".equalsIgnoreCase(positionStr)){
			legendPosition = RectangleEdge.LEFT;
		}else if("right".equalsIgnoreCase(positionStr)){
			legendPosition = RectangleEdge.RIGHT;
		}
		
		return legendPosition;
	}
	
	public static HorizontalAlignment unpackLegendHAlignmentFromStr(String positionStr){
		HorizontalAlignment legendHAlignment = HorizontalAlignment.CENTER;
		
		if("center".equalsIgnoreCase(positionStr)){
			legendHAlignment = HorizontalAlignment.CENTER;
		}else if("left".equalsIgnoreCase(positionStr)){
			legendHAlignment = HorizontalAlignment.LEFT;
		}else if("right".equalsIgnoreCase(positionStr)){
			legendHAlignment = HorizontalAlignment.RIGHT;
		}		
		return legendHAlignment;
	}
		
	public void setLegendHAlignment(HorizontalAlignment h_alignment){
		legendHAlignment = h_alignment;
	}
	
	public void setLegendPosition(RectangleEdge legend_position){
		legendPosition = legend_position;
	}
	
	public void setLegendVisibility(boolean isVisible){
		isLegendVisible = isVisible;
	}
	
	public void setBackgroundPaint(Color background_paint){
		backgroundPaint = background_paint;
	}
	
	public void setChartTitle(String title){
		chartTitle = title;
	}
	
	public void customizeChart(JFreeChart chartType){
		if(legendPosition != null && isLegendVisible){
			customizeLegend(chartType);
		}		
		if(backgroundPaint != null){			
			chartType.getPlot().setBackgroundPaint(backgroundPaint);
		}
		if(canvasColor != null) {
				chartType.setBackgroundPaint(canvasColor);
		}
	}
	
	public void customizeXYPlot(XYPlot plot){
		//set chart font
		plot.getDomainAxis().setLabelFont(chartFont);		
		for(int i = 0 ; i < plot.getRangeAxisCount(); i++){
			plot.getRangeAxis(i).setLabelFont(chartFont);
		}
		plot.getDomainAxis().setTickLabelFont(chartFont);
	}
	
	private void customizeLegend(JFreeChart chart){
		
		LegendTitle legend = new LegendTitle(chart.getPlot());
		
        BlockContainer wrapper = new BlockContainer(new BorderArrangement());        
        if(legendBorder != null){
        	wrapper.setFrame(legendBorder);
        }        
        
        // *** this is important - you need to add the item container to
        // the wrapper, otherwise the legend items won't be displayed when
        // the wrapper is drawn... ***
        BlockContainer items = legend.getItemContainer();
        items.setPadding(2, 10, 5, 2);                
        
        wrapper.add(items);
        legend.setWrapper(wrapper);                        

        legend.setPosition(legendPosition);        
        
        if(canvasColor != null){
        	legend.setBackgroundPaint(canvasColor);
        }
        if(legendHAlignment != null){
        	legend.setHorizontalAlignment(legendHAlignment);
        }
        if(legendVAlignment != null){
        	legend.setVerticalAlignment(legendVAlignment);
        }
        if(legendPadding != null){
        	legend.setPadding(legendPadding);
        }
        if(chartFont != null){
        	legend.setItemFont(new Font("Arial", Font.PLAIN, 11));
        }
        
        chart.addSubtitle(legend); 
	}

	public static boolean isAdvancedChart(String chartDescr) {
		String [] dataArr = chartDescr.split(",");
		return dataArr.length > 4;
	}
	
	public static boolean isAdvancedChartAverageNeeded(String chartDescr) {
		String [] dataArr = chartDescr.split(",");
        return dataArr.length > 4 && Integer.valueOf(dataArr[4]) != -1;
    }

}
