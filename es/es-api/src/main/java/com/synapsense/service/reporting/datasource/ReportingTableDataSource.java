package com.synapsense.service.reporting.datasource;

import com.synapsense.dto.*;
import com.synapsense.exception.EnvException;
import com.synapsense.exception.IllegalInputParameterException;
import com.synapsense.service.Environment;
import com.synapsense.service.reporting.*;
import com.synapsense.service.reporting.Formula.TransformType;
import com.synapsense.service.reporting.dto.CellTO;
import com.synapsense.service.reporting.dto.RowTO;
import com.synapsense.service.reporting.dto.TableTO;
import com.synapsense.service.reporting.exception.ReportingRuntimeException;
import com.synapsense.service.reporting.olap.*;
import com.synapsense.service.reporting.olap.custom.Cube;
import com.synapsense.service.reporting.olap.custom.TimeHierarchy;
import com.synapsense.service.reporting.olap.filters.ComparatorFilter;
import com.synapsense.service.user.EnvUserContext;
import com.synapsense.service.user.UserContext;
import com.synapsense.util.CollectionUtils;
import com.synapsense.util.Pair;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static com.synapsense.service.reporting.olap.custom.PropertyHierarchy.ReportingPropertyName;

/**
 * The <code>ReportingTableDataSource</code> class implements Table Datasource for accessing Reporting Data.<br>
 * Example: new ReportingTableDataSource($P{ObjectType}, $P{ColumnProperties}, $P{AggregationRange}, $P{Start Date}, $P{End Date})
 * @author Grigory Ryabov
 * 
 */
public class ReportingTableDataSource extends AbstractJRDataSource implements Serializable {


    private static final long serialVersionUID = -429466941278758331L;
	
	protected final static String TIMESTAMP = "Timestamp";
	private static final String FIELD_PROPERTY_HIERARCHY = "Hierarchy";
	private static final String OBJECT_AND_MEASURES_FIELD = "Object,Measures";

    private static final String FIELD_PROPERTY_DESCENDANT = "Descendant";
    private static final String ANY_DESCENDANT_OBJECT = "any";
    private static final String ONE_LEVEL_SENSOR_PARENT = "ROOM";

	private Iterator<RowTO> values;
	protected RowTO current;
	protected TableTO reportTable;
	private Locale locale;
	
	private String userName;
	
	private NumberFormat numberFormat;
	
	private DateFormat dateFormat;

	private String aggregationRange;
	
	private static Map<Class<?>, HierarchySetProvider<?>> hierarchySetProviderMap = CollectionUtils.newMap();

	static {

        ObjectsHierarchySetProvider objectsHierarchySetProvider = new ObjectsHierarchySetProvider();
		hierarchySetProviderMap.put(HashSet.class, objectsHierarchySetProvider);
		
		ObjectTypeHierarchySetProvider objectTypeHierarchySetProvider = new ObjectTypeHierarchySetProvider();
		hierarchySetProviderMap.put(String.class, objectTypeHierarchySetProvider);
		
		SimplePropertyHierarchySetProvider simplePropertiesHierarchySetProvider = new SimplePropertyHierarchySetProvider();
		hierarchySetProviderMap.put(String[].class, simplePropertiesHierarchySetProvider);
		RangePropertyHierarchySetProvider rangePropertyHierarchySetProvider = new RangePropertyHierarchySetProvider();
		hierarchySetProviderMap.put(ArrayList.class, rangePropertyHierarchySetProvider);
		
		RangeTimeHierarchySetProvider pairTimeHierarchySetProvider = new RangeTimeHierarchySetProvider();
		hierarchySetProviderMap.put(Pair.class, pairTimeHierarchySetProvider);
		PeriodTimeHierarchySetProvider periodTimeHierarchySetProvider = new PeriodTimeHierarchySetProvider();
		hierarchySetProviderMap.put(Long.class, periodTimeHierarchySetProvider);
		RangeSetTimeHierarchySetProvider rangeSetTimeHierarchySetProvider = new RangeSetTimeHierarchySetProvider();
		hierarchySetProviderMap.put(RangeSet.class, rangeSetTimeHierarchySetProvider);

	}
	
	private static Map<Object, FieldValueProvider> fieldValueProviderMap = CollectionUtils.newMap();

	static {
		TimestampFieldValueProvider timestampFieldValueProvider = new TimestampFieldValueProvider();
		fieldValueProviderMap.put(Cube.DIM_TIME, timestampFieldValueProvider);
		TopFieldValueProvider topFieldValueProvider = new TopFieldValueProvider();
		fieldValueProviderMap.put("Top", topFieldValueProvider);
		TopFieldValueProvider objectsMeasuresFieldValueProvider = new TopFieldValueProvider();
		fieldValueProviderMap.put(OBJECT_AND_MEASURES_FIELD, objectsMeasuresFieldValueProvider);
		PropertyFieldValueProvider propertyFieldValueProvider = new PropertyFieldValueProvider();
		fieldValueProviderMap.put(Cube.DIM_PROPERTY, propertyFieldValueProvider);
		ObjectFieldValueProvider objectFieldValueProvider = new ObjectFieldValueProvider();
		fieldValueProviderMap.put(Cube.DIM_OBJECT, objectFieldValueProvider);
	}

    private EnvUserContext userContext;
//    private ConvertingEnvironmentProxy convertingEnvProxy;

    public ReportingTableDataSource(String objectType, String[] columnProperties, String aggregationRange, Date startDate, Date endDate) {
		this(new Formula(aggregationRange, TransformType.ORIGINAL)
				, hierarchySetProviderMap.get(Pair.newPair(startDate, endDate).getClass()).getAxisSet(Pair.newPair(startDate, endDate))
				, hierarchySetProviderMap.get(objectType.getClass()).getAxisSet(objectType)
				, hierarchySetProviderMap.get(columnProperties.getClass()).getAxisSet(columnProperties));
	}
		
	public static ReportingTableDataSource newRAWTable(String objectType, String[] columnProperties, String aggregationRange, RangeSet dateRange) {
		dateRange.setAggregationRange(aggregationRange);
		return new ReportingTableDataSource(new Formula(aggregationRange, TransformType.ORIGINAL)
			, null
			, new CrossJoin(hierarchySetProviderMap.get(objectType.getClass()).getAxisSet(objectType)
					, dateRange)
			, hierarchySetProviderMap.get(columnProperties.getClass()).getAxisSet(columnProperties));
	}
	
	
	public static ReportingTableDataSource newRAWTable(HashSet<TO<?>> objects, String[] columnProperties, String aggregationRange, RangeSet dateRange) {
		dateRange.setAggregationRange(aggregationRange);
		Formula formula = aggregationRange.equals(Cube.LEVEL_ONE_MINUTE) 
				? new Formula(aggregationRange, TransformType.RAW)
				: new Formula(aggregationRange, TransformType.ORIGINAL);
		return new ReportingTableDataSource(formula
			, null
			, new CrossJoin(hierarchySetProviderMap.get(objects.getClass()).getAxisSet(objects)
					, dateRange)
			, hierarchySetProviderMap.get(columnProperties.getClass()).getAxisSet(columnProperties));	
	}
	
	public static ReportingTableDataSource newChart(Object objects, String[] columnProperties, String aggregationRange, Date startDate, Date endDate) {

		return new ReportingTableDataSource(new Formula(aggregationRange, TransformType.ORIGINAL)
			, null
			, hierarchySetProviderMap.get(Pair.newPair(startDate, endDate).getClass()).getAxisSet(Pair.newPair(startDate, endDate))
			, new CrossJoin(hierarchySetProviderMap.get(objects.getClass()).getAxisSet(objects)
					, hierarchySetProviderMap.get(columnProperties.getClass()).getAxisSet(columnProperties)));
	}
	
	
	public static ReportingTableDataSource newChart(Object objects, Object columnProperties, String aggregationRange, RangeSet dateRange) {
		dateRange.setAggregationRange(aggregationRange);
		Formula formula = aggregationRange.equals(Cube.LEVEL_ONE_MINUTE) 
				? new Formula(aggregationRange, TransformType.RAW)
				: new Formula(aggregationRange, TransformType.ORIGINAL);
		return new ReportingTableDataSource(formula
			, null
			, dateRange
			, new CrossJoin(hierarchySetProviderMap.get(objects.getClass()).getAxisSet(objects)
					, hierarchySetProviderMap.get(columnProperties.getClass()).getAxisSet(columnProperties)));
	}
	
	public static ReportingTableDataSource newHistagram(Collection<TO<?>> objects, Object columnProperties, Formula formula, RangeSet dateRange) {
		if (!formula.getSortingProperties().isEmpty()) {
			Set<TO<?>> checkedSet = new HashSet<TO<?>>(0);
		    for (Object object : HashSet.class.cast(objects)) {
		    	checkedSet.add(TO.class.cast(object));
		    }
		    
			String specifiedObjectType = checkedSet.iterator().next().getTypeName();
			String sortingObjectType = formula.getSortingProperties().iterator().next().getObjectType();
			Set<TO<?>> result = CollectionUtils.newSet();
			initConnection();
			if (!sortingObjectType.equals(specifiedObjectType)) {
				result.addAll(env.getRelatedObjects(checkedSet.iterator().next(), sortingObjectType, true));
			} 
			objects = result;
		}
		return new ReportingTableDataSource(formula
			, dateRange
			, new EmptySet()
			, new CrossJoin(hierarchySetProviderMap.get(objects.getClass()).getAxisSet(objects)
					, hierarchySetProviderMap.get(columnProperties.getClass()).getAxisSet(columnProperties)));
	}
	
	public static ReportingTableDataSource newAggregatedTable (Formula formula, Object slicerObject, Object rowsObject, Object columnsObject) {

		if (!formula.getSortingProperties().isEmpty()) {
			Set<TO<?>> checkedSet = new HashSet<TO<?>>(0);
		    for (Object object : HashSet.class.cast(rowsObject)) {
		    	checkedSet.add(TO.class.cast(object));
		    }
		    
			String specifiedObjectType = checkedSet.iterator().next().getTypeName();
			String sortingObjectType = formula.getSortingProperties().iterator().next().getObjectType();
			Set<TO<?>> result = CollectionUtils.newSet();
			initConnection();
			if (!sortingObjectType.equals(specifiedObjectType)) {
				result.addAll(env.getRelatedObjects(checkedSet.iterator().next(), sortingObjectType, true));
			} 
			rowsObject = result;
		}
		
		//Getting slicer
		HierarchySetProvider<?> slicerProvider = hierarchySetProviderMap.get(slicerObject.getClass());
		AxisSet slicer = slicerProvider.getAxisSet(slicerObject);
		//Getting rows
		HierarchySetProvider<?> rowsProvider = hierarchySetProviderMap.get(rowsObject.getClass());
		AxisSet rows = rowsProvider.getAxisSet(rowsObject);
		//Getting columns
		HierarchySetProvider<?> columnsProvider = hierarchySetProviderMap.get(columnsObject.getClass());
		AxisSet columns = columnsProvider.getAxisSet(columnsObject);
		return new ReportingTableDataSource(formula, slicer, rows, columns);
		
	}

    public ReportingTableDataSource(DimensionalQuieryBuilder builder
            , Formula formula, Collection<TO<?>> objects, RangeSet period, String... dimensionNames) {
        if (builder == null || formula == null || objects == null || objects.isEmpty()
                || period == null || dimensionNames == null) {
            throw new IllegalInputParameterException("Parameters are not set correctly");
        }
        TO<?> parent = objects.iterator().next();

        DimensionalQuery dimensionalQuery = builder.build(parent, Arrays.asList(dimensionNames), period,
                dataSource, unitSystemsService.getUnitReslovers(), unitSystemsService.getUnitSystems(), formula);

        this.aggregationRange = dimensionalQuery.getFormula().getAggregationRange();
        reportTable = reportingService.executeQuery(dimensionalQuery);
        values = reportTable.iterator();
        initUserPreferences();
    }


    public ReportingTableDataSource(Formula formula, AxisSet slicer, AxisSet rows, AxisSet columns){

        if (formula == null || rows == null || columns == null) {
            throw new IllegalInputParameterException("Parameters are not set correctly");
        }
        setFilterValue(formula);
		this.aggregationRange = formula.getAggregationRange();
		DimensionalQuery query = new DimensionalQuery(formula, slicer, rows, columns);
		reportTable = reportingService.executeQuery(query);
		values = reportTable.iterator();
		initUserPreferences();
		
	}
    
    private void setFilterValue(Formula formula) {
    	
    	if (formula.getFilters() == null) {  
    		return;
    	}
    	for (Filter formulaFilter : formula.getFilters().getFilters()) {
			ComparatorFilter filter = (ComparatorFilter) formulaFilter;
			if(filter.isCalculated()){
                //example is "any.WSNSENSOR.200.rMax"
                String propertyString = (String)filter.getValue();
                String[] propertyArray = propertyString.split("\\.");
                String descendantType = propertyArray[0] + "." + propertyArray[1] + "." + propertyArray[2];
                String propertyName = propertyArray[3];
                String objectTypeName = new ReportingPropertyName((String)filter.getProperties().iterator().next()).getObjectTypeName();
                TO<?> object = env.getObjectsByType(objectTypeName).iterator().next();
                filter.setValue((Comparable)getDescendantValue(object, propertyName, descendantType));
            }
		}
    }
	
	public static DimensionalQuery getDimensionalQuery(Formula formula, AxisSet slicer, AxisSet rows, AxisSet columns) {
		
		for (Filter formulaFilter : formula.getFilters().getFilters()) {
			ComparatorFilter filter = (ComparatorFilter) formulaFilter;
			if(filter.isCalculated()){
                //example is "any.WSNSENSOR.200.rMax"
                String propertyString = (String)filter.getValue();
                String[] propertyArray = propertyString.split("\\.");
                String descendantType = propertyArray[0] + "." + propertyArray[1] + "." + propertyArray[2];
                String propertyName = propertyArray[3];
                String objectTypeName = new ReportingPropertyName((String)filter.getProperties().iterator().next()).getObjectTypeName();
                TO<?> object = env.getObjectsByType(objectTypeName).iterator().next();
                filter.setValue((Comparable)getDescendantValue(object, propertyName, descendantType));
            }
		}		
		return new DimensionalQuery(formula, slicer, rows, columns);
		
	}
	
	public ReportingTableDataSource(DimensionalQuery query) {
		this.aggregationRange = query.getFormula().getAggregationRange();
		reportTable = reportingService.executeQuery(query);
		values = reportTable.iterator();
		initUserPreferences();
	}
	
	private static interface HierarchySetProvider<TYPE extends Object> {
		AxisSet getAxisSet(Object dimensionObject);
	}
	
	private static class PeriodTimeHierarchySetProvider implements HierarchySetProvider<Long> {

		@Override
		public AxisSet getAxisSet(Object dimensionObject) {
			long period = Long.class.cast(dimensionObject);
			Date startDate = new java.util.Date(System.currentTimeMillis() - period * 60 * 60 * 1000);
			Date endDate = new Date();
			return new RangeSet(Cube.TIME, startDate, endDate);
		}
			
	}
	
	private static class RangeSetTimeHierarchySetProvider implements HierarchySetProvider<RangeSet> {

		@Override
		public AxisSet getAxisSet(Object dimensionObject) {
			return RangeSet.class.cast(dimensionObject);
		}
			
	}
	
	private static class RangeTimeHierarchySetProvider implements HierarchySetProvider<Pair<Date, Date>> {

		@Override
		public AxisSet getAxisSet(Object dimensionObject) {
			Date startDate = Date.class.cast(Pair.class.cast(dimensionObject).getFirst());
			Date endDate = Date.class.cast(Pair.class.cast(dimensionObject).getSecond());
			return new RangeSet(Cube.TIME, startDate, endDate);
		}
			
	}
	
	private static class ObjectTypeHierarchySetProvider implements HierarchySetProvider<String> {

		@Override
		public AxisSet getAxisSet(Object dimensionObject) {
			return new AllMemberSet(Cube.OBJECT, String.class.cast(dimensionObject));
		}
			
	}
	
	private static class ObjectsHierarchySetProvider implements HierarchySetProvider<HashSet<TO<?>>> {
		@Override
		public AxisSet getAxisSet(Object dimensionObject) {
			Set<TO<?>> checkedSet = new HashSet<TO<?>>(0);
		    for (Object object : HashSet.class.cast(dimensionObject)) {
		    	checkedSet.add(TO.class.cast(object));
		    }
		    
			List<Tuple> objectTuples = CollectionUtils.newList();
			for (TO<?> object : checkedSet) {
				Member objectMember = new SimpleMember(Cube.OBJECT, object.getTypeName(), object);
				objectTuples.add(new MemberTuple(objectMember));
			}
			return new TupleSet(objectTuples.toArray(new Tuple[objectTuples.size()]));
		}
		
	}
	


	private static class SimplePropertyHierarchySetProvider implements HierarchySetProvider<String[]> {
		@Override
		public AxisSet getAxisSet(Object dimensionObject) {
			String[] properties = String[].class.cast(dimensionObject);
			List<Member> propertyTuples = CollectionUtils.newList();
			for (String property : properties) {
				propertyTuples.add(new SimpleMember(Cube.PROPERTY, "Name", property));
			}
			return new TupleSet(propertyTuples.toArray(new SimpleMember[0]));
		}
	}

	private static class RangePropertyHierarchySetProvider implements HierarchySetProvider<ArrayList<String[]>> {
		@Override
		public AxisSet getAxisSet(Object dimensionObject) {
			List<String[]> checkedList = new ArrayList<String[]>(0);
		    for (Object object : ArrayList.class.cast(dimensionObject)) {
		    	checkedList.add(String[].class.cast(object));
		    }
			List<Member> propertyRanges = CollectionUtils.newList();
			for (String[] propertyRange : checkedList) {
				List<Member> propertyTuples = CollectionUtils.newList();
				for (String property : propertyRange) {
					propertyTuples.add(new SimpleMember(Cube.PROPERTY, "Name", property));
				}
				propertyRanges.add(new RangeMember(propertyTuples.toArray(new SimpleMember[propertyTuples.size()])));
			}
			return new TupleSet(propertyRanges.toArray(new RangeMember[0]));
		}
	}

	private void initUserPreferences() {
		userName = getUserName(reportTable.getUserId());

//        userContext = new EnvUserContext(userName, userManagement);
//
//        UnitSystems unitSystems = unitSystemsService.getUnitSystems();
//        final UnitResolvers unitRes = unitSystemsService.getUnitReslovers();
//        convertingEnvProxy = new ConvertingEnvironmentProxy();
//        convertingEnvProxy.setUnitSystems(unitSystems);
//        convertingEnvProxy.setUnitResolvers(unitRes);
//        convertingEnvProxy.setProxiedEnvironment(env);
//        convertingEnvProxy.setTargetSystem(userContext.getUnitSystem());
////        convertingEnv = convertingEnvProxy.getProxy();


		userContext = new EnvUserContext(userName, userManagement);
		numberFormat = getNumberFormat(userContext);
		dateFormat = getDateFormat(userContext, userName);
	}
	
	private static NumberFormat getNumberFormat(UserContext userContext) {
		return userContext.getDataFormat();		
	}

	private static DateFormat getDateFormat(UserContext userContext, String userName) {
		Map<String, Object> userPreferences = userManagement.getUserPreferences(userManagement.getUser(userName));
		String format = (String) userPreferences.get("DateFormat");
		return new SimpleDateFormat(format);
	}
	
	private enum FieldValueType {
		OBJECT_NAME("ObjectName", "ObjectName")
		, TIMESTAMP("Timestamp", "Timestamp")
		, TOP1("Top", "1")
		, TOP2("Top", "2")
		, TOP3("Top", "3")
		, TOP4("Top", "4")
		, TOP5("Top", "5")
		, TOP6("Top", "6")
		, TOP7("Top", "7")
		, TOP8("Top", "8")
		, TOP9("Top", "9")
		, TOP10("Top", "10");
		private FieldValueType(String fieldValueName, String fieldName) {
			this.fieldValueName = fieldValueName;
			this.fieldName = fieldName;
		}

		private String fieldValueName;
		private String fieldName;
		
		public static FieldValueType getTypeByName(String name) {
			for (FieldValueType parameter : FieldValueType.values()) {
				if (parameter.getFieldName().equals(name))
					return parameter;
			}
			throw new IllegalArgumentException("Not supported Field Value Type" + name);	
		}

		public String getFieldValueName() {
			return fieldValueName;
		}
		
		public String getFieldName() {
			return fieldName;
		}		
	}


	@Override
	public Object getFieldValue(JRField field) throws JRException  {
		String hierarchy = field.getPropertiesMap().getProperty(FIELD_PROPERTY_HIERARCHY);
		String name = field.getName();
		FieldValueProvider fieldValueProvider = null;
		if (hierarchy == null) {
			if (current.containsField(name)) {
				FieldValueType fieldValueType = FieldValueType.getTypeByName(field.getName());
				//Property field name can be any so if FieldValueType was not found then it is property field
				if (fieldValueType == null) {
					fieldValueProvider = fieldValueProviderMap.get(Cube.DIM_PROPERTY);
				} else {
					String fieldValueName = fieldValueType.getFieldValueName();
					fieldValueProvider = fieldValueProviderMap.get(fieldValueName);
				}
			} 
		} else {
			fieldValueProvider = fieldValueProviderMap.get(hierarchy);
		}
				
		return fieldValueProvider.getFieldValue(field, current, dateFormat, userName, aggregationRange, numberFormat);
	}
	
	static interface FieldValueProvider {
		Object getFieldValue(JRField field, RowTO current, DateFormat dateFormat, String userName, String aggregationRange, NumberFormat numberFormat);
	}
	
	private static class ObjectFieldValueProvider implements FieldValueProvider {

		@Override
		public Object getFieldValue(JRField field, RowTO current, DateFormat dateFormat, String userName, String aggregationRange, NumberFormat numberFormat) {
            TO<?> object = (TO<?>) current.getFieldValue(field.getName(), Cube.DIM_OBJECT);
            String ancestorType = field.getPropertiesMap().getProperty(RelatedObjectType.ANCESTOR.getName());

            if(ancestorType != null) {
                TO<?> parent = null;
                if (ancestorType.equals(object.getTypeName())) {
                	parent = object;
                } else {
                	parent = env.getRelatedObjects(object, ancestorType, false).iterator().next();
                }
                String[] ancestorPropertyName = field.getName().split("\\.");
                return getValue(parent, String.class, ancestorPropertyName[2], numberFormat, env);
            }

            String descendantType = field.getPropertiesMap().getProperty(FIELD_PROPERTY_DESCENDANT);

			return (descendantType == null) ? getObjectValue(current, field,
					numberFormat) : getDescendantValue(object, field.getName(), descendantType);
		}
		
		private Object getObjectValue(RowTO current, JRField field, NumberFormat numberFormat) {
			String name = field.getName();
			Class<?> clazz = field.getValueClass();
			if (name.equals("ObjectName")) {
				name = "name";
			}
			TO<?> object = (TO<?>) current.getFieldValue(name, Cube.DIM_OBJECT);
			return getValue(object, clazz, name, numberFormat, env);
			
		}
		
	}
	
	private static class PropertyFieldValueProvider implements FieldValueProvider {
		
		@Override
		public Object getFieldValue(JRField field, RowTO current, DateFormat dateFormat, String userName, String aggregationRange, NumberFormat numberFormat) {
			
			String dataclass = field.getPropertiesMap().getProperty(RelatedObjectType.DIMENSION.getName());
            if (dataclass != null) {
                return getDataValueByDimension(current, field, numberFormat);
            }
            
			String name = field.getName();
			Number value = (Number) current.getFieldValue(name, Cube.DIM_PROPERTY);
			if (value == null) {
				return value;
			}
			String valueString = numberFormat.format(value);
			try {
				return numberFormat.parse(valueString).doubleValue();
			} catch (ParseException e) {
				throw new ReportingRuntimeException("Failed to parse "
						+ valueString, e);
			}			
		}
		
		private Object getDataValueByDimension(RowTO currentRow, JRField field, NumberFormat numberFormat) {
             return null;
		}
		
	}

	private static class TopFieldValueProvider implements FieldValueProvider {
		@Override
		public Object getFieldValue(JRField field, RowTO current, DateFormat dateFormat, String userName, String aggregationRange, NumberFormat numberFormat) {
			String name = field.getName();
			int objectNumber = Integer.valueOf(name);
			if (objectNumber > current.size()) {
				return null;
			}
			CellTO cell = null;
			int i = 0;
			for (CellTO currentCell : current) {
				if (i++ == objectNumber - 1) {
					cell = currentCell;
					break;
				}
			}
			Number value = cell.getValue();
			if (value == null) {
				return value;
			}
			StringBuilder cellSB = new StringBuilder();
			//Get Object Name
			String objectName = null;
			TO<?> object = (TO<?>) cell.getObject();
			try {
				objectName = env.getPropertyValue(object, "name", String.class);
			} catch (EnvException e){
				throw new ReportingRuntimeException("Failed to get property "
						+ "name" + " for object " + object.toString(), e);
			}
			cellSB.append(objectName)
				.append(": ");
			String valueString = (String) numberFormat.format(value);
			cellSB.append(valueString);
			return cellSB.toString();
		}	
	}
	
	private static class TimestampFieldValueProvider implements FieldValueProvider {
		@Override
		public Object getFieldValue(JRField field, RowTO current, DateFormat dateFormat, String userName, String aggregationRange, NumberFormat numberFormat) {
			List<Long> checkedList = CollectionUtils.newList();
		    for (Object object : List.class.cast(current.getFieldValue(TIMESTAMP, Cube.DIM_TIME))) {
		    	checkedList.add(Long.class.cast(object));
		    }
		    if (checkedList.isEmpty()) {
		    	return null;
		    }
			StringBuilder datesSB = new StringBuilder();
			for (Long date : checkedList) {
				String dateString = getFormattedDate(date, dateFormat, aggregationRange);
				datesSB.append(dateString)
				.append(" to ");
			}
			return datesSB.substring(0, datesSB.length() - 3);
		}	
	}


    private static class SLAFieldValueProvider implements FieldValueProvider {

        @Override
        public Object getFieldValue(JRField field, RowTO current, DateFormat dateFormat, String userName, String aggregationRange, NumberFormat numberFormat) {
            return null;
        }

    }


	@Override
	public boolean next() throws JRException {
		if (values.hasNext()) {
			current = values.next();
			return true;
		}
		return false;
	}

	public TableTO getData(){
		return reportTable.getTable();		
	}
	
	private static String getFormattedDate(Long millis, DateFormat dateFormat, String aggregationRange) {
		
		if (!aggregationRange.equals("All")) {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(new Date(millis));
			String level = Cube.LEVEL_YEAR;
			StringBuilder formattedDateBuffer = new StringBuilder();
			while (!level.equals(Cube.TIME.getChildLevel(aggregationRange))) {
				int value = calendar.get(TimeHierarchy.getCalendarIndex(level));
				value = level.equals(Cube.LEVEL_MONTH) ? ++value : value;
				String valueString = String.valueOf(value);
				if (!level.equals(Cube.LEVEL_YEAR) && valueString.length() == 1) {
					valueString = "0" + valueString;
				}
				formattedDateBuffer.append(valueString)
					.append(delimiterFieldMap.get(level));
				level = Cube.TIME.getChildLevel(level);
			}
			String formattedDate = formattedDateBuffer.substring(0, formattedDateBuffer.length() - 1);
			if (aggregationRange.equals(Cube.LEVEL_HOUR)) {
				formattedDate = formattedDate + ":00:00";
			}
			return formattedDate;
		}
		
		String formattedDate = dateFormat.format(new Date(millis));		
		if (formattedDate != null && formattedDate.startsWith("\"")
				&& formattedDate.endsWith("\"") && formattedDate.length() > 1) {
			formattedDate = formattedDate.substring(1,
					formattedDate.length() - 1);
		}
		return formattedDate;
		
	}
	private static Map<String, String> delimiterFieldMap = CollectionUtils.newLinkedMap();

	static {
		delimiterFieldMap.put(Cube.LEVEL_YEAR, "-");
		delimiterFieldMap.put(Cube.LEVEL_MONTH, "-");
		delimiterFieldMap.put(Cube.LEVEL_DAY, " ");
		delimiterFieldMap.put(Cube.LEVEL_HOUR, ":");
		delimiterFieldMap.put(Cube.LEVEL_ONE_MINUTE, ":");
		delimiterFieldMap.put(Cube.LEVEL_SECOND, " ");
	}

    private static Object getDescendantValue(TO<?> object, String propertyName, String descendantType) {
        String [] descendantTypeArray = descendantType.split("\\.");
        String objectType = descendantTypeArray[0];
        //Getting descendant
        if (objectType.equals(ANY_DESCENDANT_OBJECT)) {
            return getAnyDescendantValue(object, objectType, descendantType, propertyName);
        } else {
            return null;
        }
    }

    private static Object getAnyDescendantValue (TO<?> ancestor, String objectType, String descendantType, String propertyName) {
        String [] descendantArray = descendantType.split("\\.");
        String descendantTypeName = descendantArray[1];
        String dimension = descendantArray[2];
        Collection<TO<?>> children;
        try {
            if (!env.getChildren(ancestor, descendantTypeName).isEmpty()) {
                children = env.getChildren(ancestor, descendantTypeName);
            } else {
                //If object doesn't have required descendants we go up by hierarchy
                TO<?> ancestorParent = env.getParents(ancestor, ONE_LEVEL_SENSOR_PARENT).iterator().next();
                children = env.getRelatedObjects(ancestorParent, descendantTypeName, true);
            }
            if (children.isEmpty()) {
                return null;
            }
            String dataclass = "dataclass";
            Collection<CollectionTO> allProps = env.getPropertyValue(children, new String []{propertyName, dataclass});
            for (CollectionTO props : allProps) {
                int objectDimension = (Integer) props.getSinglePropValue(dataclass).getValue();
                ValueTO valueTO = props.getSinglePropValue(propertyName);
                if (valueTO == null) {
                    continue;
                }
                Object value = props.getSinglePropValue(propertyName).getValue();
                if (objectDimension == Integer.valueOf(dimension)
                        && value != null) {
                    return value;
                }
            }
            return null;
        } catch (EnvException e){
            throw new ReportingRuntimeException("Failed to get descendant of type "
                    + descendantTypeName + " for object " + ancestor.toString(), e);
        }

    }

    public static Object getValue(TO<?> object, Class<?> clazz, String propertyName, NumberFormat numberFormat, Environment env) {
        TO<?> valueObject = null;
        String valuePropertyName = null;
        PropertyDescr propDesc = env.getObjectType(object.getTypeName()).getPropertyDescriptor(propertyName);
        if (propDesc != null && "com.synapsense.dto.TO".equals(propDesc.getTypeName())) {
            try {
                valueObject = env.getPropertyValue(object, propertyName, TO.class);
            } catch (EnvException e){
                throw new ReportingRuntimeException("Failed to get property "
                        + propertyName + " for object " + object.toString(), e);
            }
            valuePropertyName = "lastValue";
        } else {
            valueObject = object;
            valuePropertyName = propertyName;
        }

        // link can be null
        if (valueObject == null) return null;

        Object value;
        try {
            value = env.getPropertyValue(valueObject, valuePropertyName, clazz);
        } catch (EnvException e){
            throw new ReportingRuntimeException("Failed to get property "
                    + propertyName + " for object " + object.toString(), e);
        }


        if (value == null) {
            return null;
        }
        if (clazz.equals(Double.class)) {
            String valueString = numberFormat.format(value);
            try {
	            double valueDouble = numberFormat.parse(valueString).doubleValue();
	            // [23645] Filter out negative values
	            return valueDouble < 0 ? null : valueDouble;
            } catch (ParseException e) {
                throw new ReportingRuntimeException("Failed to parse "
                        + valueString, e);
            }
        } else {
            return value;
        }
    }

}
