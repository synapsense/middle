package com.synapsense.service.reporting.scriplets.charts;

import java.awt.Color;
import java.awt.BasicStroke;
import java.io.FileOutputStream;
import java.sql.ResultSet;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.ArrayList;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;

import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.SymbolAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.labels.ItemLabelAnchor;
import org.jfree.chart.labels.ItemLabelPosition;
import org.jfree.chart.labels.StandardCategoryItemLabelGenerator;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.CombinedDomainCategoryPlot;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.category.BarRenderer3D;
import org.jfree.chart.renderer.xy.StandardXYBarPainter;
import org.jfree.chart.renderer.xy.StandardXYItemRenderer;
import org.jfree.chart.renderer.xy.XYBarRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.data.Range;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.DatasetUtilities;
import org.jfree.data.statistics.HistogramDataset;
import org.jfree.data.xy.XYDataset;
import org.jfree.ui.TextAnchor;

import com.synapsense.util.CollectionUtils;
import com.synapsense.util.Pair;

public abstract class ChartBuilder {	
	
	protected HashMap <String, String> axisNameMap = new HashMap <String, String>();	
	protected String chartName = "";
	protected String leftAxisClassId = "";
	boolean showLegend = true;
	HashMap<String, ArrayList<Float>> boundsMap = new HashMap<String, ArrayList<Float>>();
	HashMap<String, Color> seriesNameColorsMap = new HashMap<String, Color>();
	
	HashMap<String, LineDescr> seriesNameDescrMap = new HashMap<String, LineDescr>();
	
	Map<String, Number> valueAxisMaximumMap = CollectionUtils.newMap();
	Map<String, Number> valueAxisMinimumMap = CollectionUtils.newMap();
	
	protected ChartDescr chartDescriptor = new ChartDescr();
	
	private static Log log = LogFactory.getLog(ChartBuilder.class);
	
	public void setAxisNamesMap(HashMap <String, String> axisNamesMap){
		axisNameMap = axisNamesMap;
	}
	
	public void setChartDescriptor(ChartDescr descriptor){
		chartDescriptor = descriptor;		
	}
	
	public ChartDescr getChartDescriptor(){
		return chartDescriptor;		
	}
	
	public Map<String, Number> getValueAxisMaximumMap() {
		return valueAxisMaximumMap;
	}

	public Map<String, Number> getValueAxisMinimumMap() {
		return valueAxisMinimumMap;
	}

	public void setSeriesNameDescrMap(Map<String, LineDescr> map){
		seriesNameDescrMap.clear();
		seriesNameDescrMap.putAll(map);				
	}
	
	public void setSeriesNameColorsMap(Map<String, Color> map){
		seriesNameColorsMap.clear();
		seriesNameColorsMap.putAll(map);				
	}
	
	public void addAxisBounds(String class_id, Float lower_bound, Float upper_bound)
	{
		if (! boundsMap.containsKey(class_id))
		{
			ArrayList<Float> arr = new ArrayList<Float>();
			arr.add(0, lower_bound);
			arr.add(1, upper_bound);
			
			boundsMap.put(class_id, arr);
		}		
	}
	public void setChartName(String chart_name)
	{
		chartName = chart_name;
	}
	
	public void setLegendVisibility(boolean isVisible)
	{
		showLegend = isVisible;
	}
	
	public void setLeftAxisClassId(String leftAxisClass_Id)
	{
		leftAxisClassId = leftAxisClass_Id;
	}
	
	protected boolean setAxisBounds(ValueAxis axis, String class_id){		
		if (boundsMap.containsKey(class_id))
		{
			Float f_low = (boundsMap.get(class_id)).get(0);
			if (f_low != null) {
				axis.setLowerBound(f_low.doubleValue());
			}
			
			Float f_up = (boundsMap.get(class_id)).get(1);
			if (f_up != null) {
				axis.setUpperBound(f_up.doubleValue());
			}
			
			return true;
		}
		return false;
	}
	
	abstract public void addDataSerie(String serie_id, ResultSet rs, String seriesName,
			String dateRSfieldName, String valueRSfieldName, String axisName);
	
	abstract public void addDataSerie(String serie_id, List<SensorPoint> pointArray, String seriesName, String axisName, int bins);
	
	abstract public void addValue(String class_id, String seriesName, String categoryName, String axisName, Float value);
	
	abstract protected Pair<String, String> getValueAxisPair(String entryKey, String currValueAxisName);
	
	protected abstract JFreeChart createChartType(String chartName);
		
	protected SymbolAxis getDualValueAxis(String axisName, String lowerValue, String upperValue){
		String s[] = new String[2];
		s[0] = lowerValue;						
		s[1] = upperValue;
		SymbolAxis axis = new SymbolAxis(axisName, s);
		axis.setRange(new Range(- 0.1, 1.1));
		axis.setAutoRange(true);
		axis.setGridBandsVisible(false);			
		return axis;
	}
	
	protected ValueAxis getValueAxis(String axisName, String dataClassId, NumberFormat numberFormat){
		ValueAxis axis = null;

		if("211".equals(dataClassId)){
			axis = getDualValueAxis(axisName, "Closed", "Open");
		}else if("213".equals(dataClassId)){
			axis = getDualValueAxis(axisName, "Absent", "Present");
		}else if("214".equals(dataClassId)){
			axis = getDualValueAxis(axisName, "On", "Off");
		}else{
			axis = new NumberAxis(axisName);
            NumberAxis numberAxis = (NumberAxis)axis;
            numberAxis.setNumberFormatOverride(numberFormat);
		}
		
		return axis;
	}
	
	abstract protected void customizeChart(XYPlot plot);
	
	protected Color getColorByIndex(int index){
		Color result = null;
		if (index < seriesColor.length){
			result = seriesColor[index];
		}else
		{
			Random rnd = new Random();		        		        
	        int r = rnd.nextInt(255);
	        int g = rnd.nextInt(255);
	        int b = rnd.nextInt(255);
	        
			result = new Color(r, g, b);
		}
		
		return result;
	}
	
	public abstract JFreeChart getChart(NumberFormat numberFormat);
	

	protected JFreeChart createHistogramChart(HistogramDataset dataset, Map<String, String> valueAxisNameMap, String serie_Id, NumberFormat numberFormat) {
		
		JFreeChart chartType = null;		
		String [] keyArr = new String[]{(String) dataset.getSeriesKey(0)};
		for (int i = 0; i < keyArr.length; i++)
		{
			if (leftAxisClassId.equalsIgnoreCase(keyArr[i]))
			{
				String glass = keyArr[0];
				keyArr[0] = leftAxisClassId; keyArr[i] = glass; 
				break;
			}
		}
							
		//create chart
		chartType = createChartType(chartName);		
		XYPlot plot = chartType.getXYPlot();				
		
		//Fill chart with datasets
		int datasetId = 0;		
		int drawn_series_count = 0; // Count of series that are already drawn
		for (int i = 0; i < keyArr.length; i++){			
						
			String entryKey = keyArr[i];
			XYDataset currDataset = dataset;
			
			String currValueAxisName = valueAxisNameMap.get(entryKey);				
			ValueAxis axis = null;
			plot.setDataset(currDataset);
			
			axis = getValueAxis(currValueAxisName, entryKey, numberFormat);
			if (axis instanceof NumberAxis) {
				((NumberAxis)axis).setAutoRangeIncludesZero(false);
			}
			
			axis.setAxisLineVisible(true);
			plot.setRangeAxis(datasetId, axis);		
			setAxisBounds(axis, serie_Id);
			plot.mapDatasetToRangeAxis(datasetId, datasetId);
			
			//-----------
		    plot.setForegroundAlpha(0.7F);
		    plot.setBackgroundPaint(Color.WHITE);
		    plot.setDomainGridlinePaint(new Color(150,150,150));
		    plot.setRangeGridlinePaint(new Color(150,150,150));
		    XYBarRenderer renderer = (XYBarRenderer)plot.getRenderer();
			
			//set default lines width
			renderer.setBaseStroke(new BasicStroke(1.5f));
			renderer.setShadowVisible(false);
		    renderer.setBarPainter(new StandardXYBarPainter());
	        //customize series
			for (int h = 0; h < currDataset.getSeriesCount(); h++)
			{
				String serieName = currDataset.getSeriesKey(h).toString();
				//if series has description customize it, else use default way
				if(seriesNameDescrMap.containsKey(serieName) && seriesNameDescrMap.get(serieName) != null){					
					seriesNameDescrMap.get(serieName).customizeSeries(renderer, h);					
				} else {
					
					chartDescriptor.setLegendVisibility(showLegend);
					
					//if series has define color use it, else use default colors
					if (seriesNameColorsMap != null && seriesNameColorsMap.containsKey(serieName)){														
						renderer.setSeriesPaint(h, seriesNameColorsMap.get(serieName));
					}else{
						renderer.setSeriesPaint(h, getColorByIndex(drawn_series_count));
					}				
					drawn_series_count++;
				}				
			}			
	        plot.setRenderer(datasetId, renderer);
			datasetId++;
		}					
		
		//customize chart
		chartDescriptor.customizeChart(chartType);				
		chartDescriptor.customizeXYPlot(plot);

	return chartType;
	
	}
	
	protected JFreeChart createXYChart(Map<String, XYDataset> datasetMap,			
			HashMap<String, String> valueAxisNameMap, String chartName, NumberFormat numberFormat){
	
	JFreeChart chartType = null;		

		//Set left axis (this one should be the first in the list)
		Set<String> keys = datasetMap.keySet();
		
		String [] keyArr = keys.toArray(new String[keys.size()]);
		
		for (int i = 0; i < keyArr.length; i++)
		{
			if (leftAxisClassId.equalsIgnoreCase(keyArr[i]))
			{
				String glass = keyArr[0];
				keyArr[0] = leftAxisClassId; keyArr[i] = glass; 
				break;
			}
		}
							
		//create chart
		chartType = createChartType(chartName);		
		XYPlot plot = chartType.getXYPlot();				
		
		//Fill chart with datasets
		int datasetId = 0;		
		int drawn_series_count = 0; // Count of series that are already drawn
		for (int i = 0; i < keyArr.length; i++){			
						
			String entryKey = keyArr[i];
			XYDataset currDataset = datasetMap.get(entryKey);																			
			
			String currValueAxisName = valueAxisNameMap.get(entryKey);
			ValueAxis axis = null;		
			
			plot.setDataset(datasetId, currDataset);
			
			AdvancedChartData advancedChartData = chartDescriptor.getAdvancedChartData();
			if (advancedChartData != null) {
				fillAggregationMaps(advancedChartData, currDataset, entryKey);
			}
							
			//int dataClassId = Integer.parseInt(entryKey);
			
			Pair<String, String> valueAxisPair = getValueAxisPair(entryKey, currValueAxisName);
			axis = getValueAxis(valueAxisPair.getFirst(), valueAxisPair.getSecond(), numberFormat);
			if (axis instanceof NumberAxis) {
				((NumberAxis)axis).setAutoRangeIncludesZero(false);
			}
			
			axis.setAxisLineVisible(true);
								
			plot.setRangeAxis(datasetId, axis);
			setAxisBounds(axis, entryKey);
			
			plot.mapDatasetToRangeAxis(datasetId, datasetId);
			
			XYItemRenderer renderer = new StandardXYItemRenderer();															
			
			//set default lines width
			renderer.setBaseStroke(new BasicStroke(1.5f));
		
	        //customize series
			for (int h = 0; h < currDataset.getSeriesCount(); h++)
			{
				String serieName = currDataset.getSeriesKey(h).toString();
				
				//if series has description customize it, else  default way
				if(seriesNameDescrMap.containsKey(serieName) && seriesNameDescrMap.get(serieName) != null){					
					seriesNameDescrMap.get(serieName).customizeSeries(renderer, h);					
				}else{
					
					chartDescriptor.setLegendVisibility(showLegend);
					
					//if series has define color use it, else use default colors
					if (seriesNameColorsMap != null && seriesNameColorsMap.containsKey(serieName)){														
						renderer.setSeriesPaint(h, seriesNameColorsMap.get(serieName));
					}else{
						renderer.setSeriesPaint(h, getColorByIndex(drawn_series_count));
					}				
					drawn_series_count++;
						
					//Temp solution: RA/ALR dashed stroke
					//TODO: 
//					if (serieName.equals(GetRealTimeData.CRAC_ALR_SENSOR_NAME) || serieName.equals(GetRealTimeData.RACK_RA_SENSOR_NAME)){
//						renderer.setSeriesStroke(h, new BasicStroke(2f,
//				                BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND, 1.0f, 
//				                new float[] {10.0f, 6.0f}, 0.0f));			                
//					}
				}				
			}			
	        plot.setRenderer(datasetId, renderer);
			datasetId++;
		}					
		
		//customize chart
		chartDescriptor.customizeChart(chartType);				
		chartDescriptor.customizeXYPlot(plot);

	return chartType;
	}
	
	private void fillAggregationMaps (AdvancedChartData advancedChartData, XYDataset currDataset, String entryKey) {
		if (advancedChartData != null) {
			Map<String, Integer> aggregationColorsMap = chartDescriptor.getAdvancedChartData().getAggregationColorsMap();
			if (aggregationColorsMap.containsKey("max")) {
				Number maximum = DatasetUtilities.findMaximumRangeValue(currDataset);
				if (valueAxisMaximumMap.isEmpty()) {
					this.valueAxisMaximumMap.put(entryKey, maximum);
				} else if (valueAxisMaximumMap.containsKey(entryKey)
						&& maximum.doubleValue() > valueAxisMaximumMap.get(entryKey).doubleValue()) {
					this.valueAxisMaximumMap.put(entryKey, maximum);
				}
			}
			if (aggregationColorsMap.containsKey("min")) {
				Number minimum = DatasetUtilities.findMinimumRangeValue(currDataset);
				if (valueAxisMinimumMap.isEmpty()) {
					this.valueAxisMinimumMap.put(entryKey, minimum);
				} else if (valueAxisMinimumMap.containsKey(entryKey)
						&& minimum.doubleValue() < valueAxisMaximumMap.get(entryKey).doubleValue()) {
					this.valueAxisMinimumMap.put(entryKey, minimum);
				}
			}
		}

	}
	
	protected JFreeChart createCategoryChart(HashMap<String, DefaultCategoryDataset> datasetMap, NumberFormat numberFormat){
		JFreeChart chartType = null;
		DefaultCategoryDataset dataset = null;
		Set<String> keys = datasetMap.keySet();
		if (datasetMap.get(keys.iterator().next()).getRowCount() > 30) {
			this.showLegend = false;
		}
		chartType = createChartType(chartName);
		
		CombinedDomainCategoryPlot plot = (CombinedDomainCategoryPlot)chartType.getPlot();
		
		plot.setGap(15.0);
		
		int drawn_series_count = 0; // Count of series that are already drawn

		for (String key : keys){
			dataset = datasetMap.get(key);			 												    
		        
		    NumberAxis rangeAxis = new NumberAxis(axisNameMap.get(key));		    
		    BarRenderer3D renderer = new BarRenderer3D();
		    renderer.setDrawBarOutline(false);		    		    
		    
		    renderer.setBaseItemLabelGenerator(new StandardCategoryItemLabelGenerator());
	        renderer.setBaseItemLabelsVisible(true);
	        renderer.setItemLabelAnchorOffset(9.0);
	        renderer.setBasePositiveItemLabelPosition(new ItemLabelPosition(ItemLabelAnchor.OUTSIDE12, TextAnchor.BASELINE_LEFT));
		    		    
		    
		    //Set series color
		    for (int i = 0; i < dataset.getRowCount(); i++){
		    	String serieName = dataset.getRowKey(i).toString();
		    			    	
		    	//if series has define color use it, else use default colors
				if (seriesNameColorsMap != null && seriesNameColorsMap.containsKey(serieName)){														
					renderer.setSeriesPaint(i, seriesNameColorsMap.get(serieName));					
				}else{
				    renderer.setSeriesPaint(i, getColorByIndex(drawn_series_count));				
				}												
				
				drawn_series_count++;		    	
		    }		    		    		    
		    
		    CategoryPlot subplot = new CategoryPlot(dataset, null, rangeAxis, renderer);
		    
		    renderer.setBaseItemLabelPaint(Color.BLACK);		    
		    
		    renderer.setMaximumBarWidth(0.30);
		    
		    subplot.setForegroundAlpha(0.6f);
		    subplot.setNoDataMessage("No data to display");		    		    
		    		    
		    //Increase axis upper bound on the 2,5% to avoid bar label cutting. 
		    //(since upper bound automatic calculation is not fine) 
		    for (int k = 0; k < subplot.getRangeAxisCount(); k++){
		    	NumberAxis ax = (NumberAxis)subplot.getRangeAxis(k);		    	
		    	double upperBound = ax.getUpperBound();		    	
		    	ax.setUpperBound(upperBound + upperBound*0.025);
		    }
		    		    
		    plot.add(subplot, 1);		    
		}						
		
		return chartType;
	}		
	
	public static void writeChartAsPNG(JFreeChart chart, String fileName, int width, int height)
	{
		try
		{
			FileOutputStream chartFOS = new FileOutputStream(fileName);
			ChartUtilities.writeChartAsPNG(chartFOS, chart, width, height);
			
			chartFOS.close();
		}
		catch(Exception e)
		{
			log.error(e.getLocalizedMessage(), e);
		}										
	}

	//------------ Color constants ------------
	protected static Color []seriesColor = {		                                          
												Color.BLACK, 		                                          	
												//Color.GREEN,	// removed due to almost exact to background and confusing 
												Color.RED,
												Color.BLUE,		                                          		                                          		                                          
												Color.GRAY,	
												// new Color(43, 124, 66), //deep green, the same
		                                          new Color(171, 81, 255), //purple
		                                          new Color(106, 68, 255),
		                                          new Color(119, 117, 255),
		                                          new Color(255, 43, 75),
		                                          new Color(255, 46, 0),
													Color.MAGENTA,
		                                          new Color(86, 123, 255),
		                                          new Color(255, 17, 124),
		                                          new Color(84, 68, 255),
		                                          new Color(255, 43, 75),
		                                          new Color(255, 0, 198),
		                                          new Color(140, 0, 255),
		                                          new Color(204, 0, 163),
		                                          new Color(0, 89, 255),		                                          
		                                          };
	
}
