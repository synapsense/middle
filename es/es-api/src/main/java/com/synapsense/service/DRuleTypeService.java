package com.synapsense.service;

import com.synapsense.exception.ObjectNotFoundException;

public interface DRuleTypeService<KEY_TYPE> extends RuleTypeService<KEY_TYPE> {
	String getDSL(KEY_TYPE id) throws ObjectNotFoundException;

	void setDSL(KEY_TYPE id, String dsl) throws ObjectNotFoundException;
}
