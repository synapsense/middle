package com.synapsense.service.impl.audit;

import java.io.Serializable;

public class Auditor implements Serializable {

	/**
	 * 
	 */
	private String name;
	private String configuration;
	private String lastStatus;

	public Auditor() {
	}

	public Auditor(String name, String configuration, String lastStatus) {
		this.name = name;
		this.configuration = configuration;
		this.lastStatus = lastStatus;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getConfiguration() {
		return configuration;
	}

	public void setConfiguration(String configuration) {
		this.configuration = configuration;
	}

	public String getLastStatus() {
		return lastStatus;
	}

	public void setLastStatus(String lastStatus) {
		this.lastStatus = lastStatus;
	}

}