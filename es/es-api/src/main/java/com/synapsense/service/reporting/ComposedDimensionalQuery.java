package com.synapsense.service.reporting;

import com.synapsense.service.reporting.olap.AxisSet;
import com.synapsense.service.reporting.olap.CrossJoin;
import com.synapsense.service.reporting.olap.Union;

import java.util.ArrayList;

/**
 * The <code>ComposedDimensionalQuery</code> class adds to ancestor the ability to contain already formed sequence of cells
 */
public class ComposedDimensionalQuery extends DimensionalQuery {

	private static final long serialVersionUID = -6067396696227970065L;
	private ArrayList<Cell> cells;

    public ComposedDimensionalQuery(ArrayList<Cell> cells, Formula formula, AxisSet slicer, AxisSet... sets) {
        super(formula, slicer, sets);
        this.cells = cells;
    }

    @Override
    public ArrayList<Cell> getCells() {
        return cells;
    }

    @Override
    public AxisSet getTableSet() {
    	return (getSlicer() == null) ? getAxisSet(0) : new CrossJoin(getSlicer(), getAxisSet(0));
    }
}
