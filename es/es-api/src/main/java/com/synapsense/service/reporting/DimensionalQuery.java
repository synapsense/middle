package com.synapsense.service.reporting;

import com.synapsense.service.reporting.olap.AxisSet;
import com.synapsense.service.reporting.olap.CrossJoin;
import com.synapsense.service.reporting.olap.Tuple;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * The <code>DimensionalQuery</code> class contains initial query parameters:
 * rows, columns, slicer axis.
 * Example: new DimensionalQuery(formula, slicer, rows, columns);
 * 
 * @author Grigory Ryabov
 * 
 */
public class DimensionalQuery implements Serializable {

	private static final long serialVersionUID = -717692988345335038L;

	private String simpleFormula;

	private List<AxisSet> sets;

	private AxisSet slicer;

	private Formula formula;

    public DimensionalQuery(String formula, AxisSet slicer, AxisSet... sets) {
		this.simpleFormula = formula;
		this.slicer = slicer;
		this.sets = Arrays.asList(sets);
	}

	public DimensionalQuery() {
		sets = new ArrayList<>(2);
	}

	public DimensionalQuery(Formula formula, AxisSet slicer, AxisSet... sets) {
		this.slicer = slicer;
		this.sets = Arrays.asList(sets);
		this.formula = formula;
	}

	public Formula getFormula() {
		return this.formula;
	}

	public AxisSet getSlicer() {
		return this.slicer;
	}

	public AxisSet getAxisSet(int index) {
		return sets.get(index);
	}

    public ArrayList<Cell> getCells() {

    	ArrayList<Cell> cells = new ArrayList<>();
        AxisSet rows = sets.get(0);
        AxisSet columns = sets.get(1);

        CrossJoin crossJoin = new CrossJoin(rows, columns);
        List<Tuple> tableTuples = crossJoin.getTuples();
        for (Tuple tuple : tableTuples) {
            cells.add(new Cell(tuple));
        }
        return cells;
    }

	public DimensionalQuery setFormula(String formula) {
		this.simpleFormula = formula;
		return this;
	}

	public DimensionalQuery setSlicer(AxisSet slicer) {
		this.slicer = slicer;
		return this;
	}

	public DimensionalQuery addAxisSet(AxisSet set, int axisIndex) {
		sets.add(axisIndex, set);
		return this;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder("SELECT").append(" ").append(formula).append(" ");
		for (int i = 0; i < sets.size(); i++) {
			if (i == 0) {
				builder.append(sets.get(i)).append(" ON ROWS");
			} else if (i == 1) {
				builder.append(sets.get(i)).append(" ON COLUMNS");
			} else {
				builder.append(sets.get(i)).append(" ON AXIS(").append(i).append(")");
			}
			if (i != sets.size() -1 ) {
				builder.append(", ");
			}
		}
		builder.append(" WHERE ").append(slicer);
		return builder.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((formula == null) ? 0 : formula.hashCode());
		result = prime * result + ((sets == null) ? 0 : sets.hashCode());
		result = prime * result + ((slicer == null) ? 0 : slicer.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DimensionalQuery other = (DimensionalQuery) obj;
		if (formula == null) {
			if (other.formula != null)
				return false;
		} else if (!formula.equals(other.formula))
			return false;
		if (sets == null) {
			if (other.sets != null)
				return false;
		} else if (!sets.equals(other.sets))
			return false;
		if (slicer == null) {
			if (other.slicer != null)
				return false;
		} else if (!slicer.equals(other.slicer))
			return false;
		return true;
	}

    public AxisSet getTableSet() {
        CrossJoin tableSet = (slicer == null) ? new CrossJoin(getAxisSet(0), this.getAxisSet(1)) : new CrossJoin(slicer, getAxisSet(0), this.getAxisSet(1));
        return tableSet;
    }
}
