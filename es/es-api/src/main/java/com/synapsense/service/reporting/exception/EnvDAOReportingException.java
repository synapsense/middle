package com.synapsense.service.reporting.exception;

import javax.ejb.ApplicationException;

/**
 * The Class ReportingDatasourceException describes exceptions occurred during reporting data source queries execution.
 */
@ApplicationException(rollback = false)
public class EnvDAOReportingException extends DAOReportingException {

	private static final long serialVersionUID = -4855218752536549537L;

	public EnvDAOReportingException() {
	}

	public EnvDAOReportingException(String message, Throwable cause) {
		super(message, cause);
	}

	public EnvDAOReportingException(String message) {
		super(message);
	}

	public EnvDAOReportingException(Throwable cause) {
		super(cause);
	}
}
