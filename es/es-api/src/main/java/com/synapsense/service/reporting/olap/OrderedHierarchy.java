package com.synapsense.service.reporting.olap;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.synapsense.service.reporting.olap.custom.Level;
import com.synapsense.service.reporting.olap.custom.TimePeriodType;
import com.synapsense.util.Pair;

/**
 * Hierarchy members of which can be ordered
 * 
 * @author Dmitry Grudzinskiy
 * 
 */
public interface OrderedHierarchy extends MemberHierarchy {

	/**
	 * Finds all members between two specified members. Members must be from
	 * this hierarchy and the same level.
	 * 
	 * @param start
	 * @param end
	 * @return list of all members between the specified members
	 */
	List<Member> getMembers(Member start, Member end);
	
	/**
	 * Gets the next.
	 *
	 * @param member the member
	 * @return the next
	 */
	Member getNext(Member member);

	/**
	 * Checks if is in historical period.
	 *
	 * @param member the member
	 * @return true, if is in historical period
	 */
	boolean isInHistoricalPeriod(Member member);

	/**
	 * Gets the member of calendar.
	 *
	 * @param calendar the calendar
	 * @param bottomLevel the bottom level
	 * @param upLevel the up level
	 * @return the member of calendar
	 */
	Member getMemberOfCalendar(Calendar calendar, Level bottomLevel, Level upLevel);

	/**
	 * Gets the previous.
	 *
	 * @param member the member
	 * @return the previous
	 */
	Member getPrevious(Member member);

	/**
	 * Gets the period.
	 *
	 * @param periodType the period type
	 * @return the period
	 */
	Pair<Member, Member> getPeriod(TimePeriodType periodType);

	/**
	 * Gets the calendar of member.
	 *
	 * @param member the member
	 * @return the calendar of member
	 */
	Calendar getCalendarOfMember(Member member);

	/**
	 * Converts this hierarchy member to member of specified hierarchy of same dimension.
	 *
	 * @param member the member
	 * @param hierarchy the hierarchy to convert from
	 * @param bottomLevel the bottom level
	 * @return the member
	 */
	Member convert(Member member, OrderedHierarchy hierarchy, String bottomLevel);

	/**
	 * Checks if is full member.
	 *
	 * @param timeMember the time member
	 * @return true, if is full member
	 */
	boolean isFullMember(Member timeMember);

}
