package com.synapsense.service.nsvc.filters;

import java.io.Serializable;
import java.util.regex.Pattern;

import com.synapsense.service.nsvc.events.ObjectTypeCreatedEvent;
import com.synapsense.service.nsvc.events.ObjectTypeCreatedEventProcessor;

/**
 * Class for filtering <code>ObjectTypeCreatedEvent</code>s by type name regexp.
 * 
 * @author Oleg Stepanov
 * 
 */
public class ObjectTypeCreatedEventFilter implements ObjectTypeCreatedEventProcessor, Serializable {

	private static final long serialVersionUID = 1745813986779032813L;

	private final Pattern typeNamePattern;

	public ObjectTypeCreatedEventFilter(String typeNamePattern) {
		this.typeNamePattern = Pattern.compile(typeNamePattern);
	}

	@Override
	public ObjectTypeCreatedEvent process(ObjectTypeCreatedEvent e) {
		return typeNamePattern.matcher(e.getObjectType().getName()).matches() ? e : null;
	}

}
