package com.synapsense.service.reporting.olap.custom;

import com.synapsense.service.impl.dao.to.GenericObjectTO;
import com.synapsense.service.reporting.olap.BaseHierarchy;
import com.synapsense.service.reporting.olap.Hierarchy;
import com.synapsense.service.reporting.olap.MemberHierarchy;
import com.synapsense.service.reporting.olap.OrderedHierarchy;
import com.synapsense.util.CollectionUtils;

import java.util.HashMap;
import java.util.Map;

public class Cube {

	public static final String DIM_PROPERTY = "Measures";
	public static final String LEVEL_PROPERTY = "Name";

	public static final String DIM_TIME = "Time";
	public static final String LEVEL_YEAR = "Year";
	public static final String LEVEL_MONTH = "Month";
	public static final String LEVEL_WEEK = "Week";
	public static final String LEVEL_DAY = "Day";
	public static final String LEVEL_HOUR = "Hour";
	public static final String LEVEL_HALF_HOUR = "HalfHour";
	public static final String LEVEL_FIVE_MINUTES = "FiveMin";
	public static final String LEVEL_ONE_MINUTE = "OneMin";
	public static final String LEVEL_SECOND = "Second";
	public static final String DIM_WEEK_TIME = "WeekTime";
	public static final String ALL_RANGE = "All";

	public static final String DIM_OBJECT = "Object";

	public static ObjectHierarchy OBJECT = new ObjectHierarchy(
			GenericObjectTO.class);
	public static TimeHierarchy TIME = new TimeHierarchy();
	public static TimeHierarchy WEEK = new WeekTimeHierarchy();
	public static PropertyHierarchy PROPERTY = new PropertyHierarchy();

	public static HashMap<String, Hierarchy> hMap = new HashMap<>();

	static {
		hMap.put(DIM_OBJECT, OBJECT);
		hMap.put(DIM_PROPERTY, PROPERTY);
		hMap.put(DIM_TIME, TIME);
        hMap.put(DIM_WEEK_TIME, WEEK);
	}

    public static Map<String, String> dimMap = CollectionUtils.newMap();


    static {
        dimMap.put(DIM_OBJECT, DIM_OBJECT);
        dimMap.put(DIM_PROPERTY, DIM_PROPERTY);
        dimMap.put(DIM_TIME, DIM_TIME);
        dimMap.put(DIM_WEEK_TIME, DIM_TIME);
    }


    public static String getDimension(String name) {
        return dimMap.get(name);
    }

    public static Hierarchy getHierarchy(String name) {
		return hMap.get(name);
	}

	public static BaseHierarchy getBaseHierarchy(String name) {
		return getHierarchy(name, BaseHierarchy.class);
	}


	public static MemberHierarchy getMemberHierarchy(String name) {
		return getHierarchy(name, MemberHierarchy.class);
	}
	
	public static OrderedHierarchy getOrderedHierarchy(String name) {
		return getHierarchy(name, OrderedHierarchy.class);
	}

	public static void setReportingDataSource(
			EnvironmentDataSource reportingDataSource) {
		OBJECT.setReportingDataSource(reportingDataSource);
	}

	private static <T> T getHierarchy(String name, Class<T> clazz) {
		Hierarchy h = hMap.get(name);
		if (h == null || !clazz.isInstance(h)) {
			throw new RuntimeException("No " + clazz.getName()
					+ " found for name " + name);
		}
		return clazz.cast(h);
	}
}
