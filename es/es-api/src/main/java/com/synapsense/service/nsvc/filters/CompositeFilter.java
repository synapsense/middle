package com.synapsense.service.nsvc.filters;

import java.io.Serializable;

import com.synapsense.service.nsvc.Event;
import com.synapsense.service.nsvc.EventProcessor;
import com.synapsense.service.nsvc.events.DispatchingProcessor;
import com.synapsense.service.nsvc.events.ObjectCreatedEventProcessor;
import com.synapsense.service.nsvc.events.ObjectDeletedEventProcessor;
import com.synapsense.service.nsvc.events.PropertiesChangedEventProcessor;

public class CompositeFilter implements EventProcessor, Serializable {

	private static final long serialVersionUID = 7484165091027365997L;

	private EventProcessor lhsFilter;

	public CompositeFilter(EventProcessor lhsFilter) {
		if (lhsFilter == null)
			throw new IllegalArgumentException("lhsFilter must not be null");
		this.lhsFilter = lhsFilter;
	}

	public CompositeFilter and(EventProcessor rhsFilter) {
		if (rhsFilter == null)
			throw new IllegalArgumentException("rhsFilter must not be null");

		lhsFilter = new And(lhsFilter, rhsFilter);

		return this;
	}

	public CompositeFilter and(ObjectCreatedEventProcessor rhsFilter) {
		return and(new DispatchingProcessor().putOcep(rhsFilter));
	}

	public CompositeFilter and(ObjectDeletedEventProcessor rhsFilter) {
		return and(new DispatchingProcessor().putOdep(rhsFilter));
	}

	public CompositeFilter and(PropertiesChangedEventProcessor rhsFilter) {
		return and(new DispatchingProcessor().putPcep(rhsFilter));
	}

	public CompositeFilter or(EventProcessor rhsFilter) {
		if (rhsFilter == null)
			throw new IllegalArgumentException("rhsFilter must not be null");

		lhsFilter = new Or(lhsFilter, rhsFilter);

		return this;
	}

	public CompositeFilter or(ObjectCreatedEventProcessor rhsFilter) {
		return or(new DispatchingProcessor().putOcep(rhsFilter));
	}

	public CompositeFilter or(ObjectDeletedEventProcessor rhsFilter) {
		return or(new DispatchingProcessor().putOdep(rhsFilter));
	}

	public CompositeFilter or(PropertiesChangedEventProcessor rhsFilter) {
		return or(new DispatchingProcessor().putPcep(rhsFilter));
	}

	@Override
	public Event process(Event e) {
		return lhsFilter.process(e);
	}

	public static class Not implements EventProcessor, Serializable {

		private static final long serialVersionUID = 5612263936162628584L;

		private final EventProcessor filter;

		public Not(EventProcessor filter) {
			this.filter = filter;
		}

		public Not(ObjectCreatedEventProcessor rhsFilter) {
			this.filter = new DispatchingProcessor().putOcep(rhsFilter);
		}

		public Not(ObjectDeletedEventProcessor rhsFilter) {
			this.filter = new DispatchingProcessor().putOdep(rhsFilter);
		}

		public Not(PropertiesChangedEventProcessor rhsFilter) {
			this.filter = new DispatchingProcessor().putPcep(rhsFilter);
		}

		@Override
		public Event process(Event e) {
			Event e1 = filter.process(e);
			return e1 == null ? e : null;
		}
	}

	private static class And implements EventProcessor, Serializable {

		private static final long serialVersionUID = -5616797822397303229L;

		private final EventProcessor lhsFilter, rhsFilter;

		public And(EventProcessor lhsFilter, EventProcessor rhsFilter) {
			this.lhsFilter = lhsFilter;
			this.rhsFilter = rhsFilter;
		}

		@Override
		public Event process(Event e) {
			Event e1 = lhsFilter.process(e);
			return e1 == null ? null : rhsFilter.process(e1);
		}
	}

	private static class Or implements EventProcessor, Serializable {

		private static final long serialVersionUID = 8366262230449629096L;

		private final EventProcessor lhsFilter, rhsFilter;

		public Or(EventProcessor lhsFilter, EventProcessor rhsFilter) {
			this.lhsFilter = lhsFilter;
			this.rhsFilter = rhsFilter;
		}

		@Override
		public Event process(Event e) {
			Event e1 = lhsFilter.process(e);
			return e1 != null ? e1 : rhsFilter.process(e);
		}
	}
}