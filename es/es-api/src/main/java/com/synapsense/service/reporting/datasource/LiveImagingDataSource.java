package com.synapsense.service.reporting.datasource;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;
import net.sf.jasperreports.engine.design.JRDesignField;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.synapsense.dto.CollectionTO;
import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;
import com.synapsense.service.reporting.olap.RangeSet;
import com.synapsense.service.reporting.olap.custom.Cube;
import com.synapsense.util.CollectionUtils;

/**
 * The <code>LiveImagingDataSource</code> class implements Datasource for accessing Live Imaging Data.<br>
 * Example: new LiveImagingDataSource($P{HostName}, $P{HostPort}, $P{ObjectName}, $P{ObjectType}, $P{ImageType}, $P{Start Date}, $P{End Date})
 * @author Grigory Ryabov, Alexander Pakhunov
 * 
 */
public class LiveImagingDataSource extends AbstractJRDataSource {

	private static HashMap<String, String> namesMap = new HashMap<>();

	static {
		namesMap.put("Image URL", "imageURL");
		namesMap.put("Legend URL", "legendURL");
		namesMap.put("Image TimeStamp", "imageTimeStamp");
		namesMap.put("DC Name", "dcName");
		namesMap.put("Room Name", "roomName");
		namesMap.put("Live Imaging Type", "imagingType");
	}

	private Iterator<CollectionTO> values;

	private CollectionTO current;

	private static final String DEFAULT_HOST = "localhost";
	private static final String DEFAULT_PORT = "9091";
	//TODO: read it from CONFIGURATION_DATA
	private static String mHost = DEFAULT_HOST;
	private static String mPort = DEFAULT_PORT;
	
	private static final String DC = "DC";
	private static final String ROOM = "ROOM";

	public LiveImagingDataSource(TO<?> liveImagingObject, String liveImagingType) throws Exception {
		Date endDate = new Date();

		Calendar cal = Calendar.getInstance();
		cal.setTime(endDate);
		cal.add(Calendar.MINUTE, -30);

		Date startDate = cal.getTime();
		values = getData(liveImagingObject,  LiveImagingType.getTypeByName(liveImagingType), startDate, endDate)
		        .iterator();
	}

	public LiveImagingDataSource(TO<?> liveImagingObject, String liveImagingType, String startYear,
	        String startMonth, String startDay, String endYear, String endMonth, String endDay) throws Exception {
		values = getData(liveImagingObject, LiveImagingType.getTypeByName(liveImagingType),
		        AlertDataSource.setDate(startYear, startMonth, startDay),
		        AlertDataSource.setDate(endYear, endMonth, endDay)).iterator();
	}

	public LiveImagingDataSource(String host, String port, TO<?> liveImagingObject, String liveImagingType,
	        String startYear, String startMonth, String startDay, String endYear, String endMonth, String endDay)
	        throws Exception {
		mHost = host;
		mPort = port;
		values = getData(liveImagingObject, LiveImagingType.getTypeByName(liveImagingType),
		        AlertDataSource.setDate(startYear, startMonth, startDay),
		        AlertDataSource.setDate(endYear, endMonth, endDay)).iterator();
	}

	public LiveImagingDataSource(String host, String port, TO<?> liveImagingObject, String liveImagingType,
	        Date start, Date end)
	        throws Exception {
		mHost = host;
		mPort = port;
		values = getData(liveImagingObject, LiveImagingType.getTypeByName(liveImagingType),
		        start, end).iterator();
	}

	public LiveImagingDataSource(String host, String port, TO<?> liveImagingObject, String liveImagingType,
	        RangeSet period)
	        throws Exception {
		this(host, port, liveImagingObject, liveImagingType
				, Cube.TIME.getCalendarOfMember(period.getStartMember()).getTime()
				, Cube.TIME.getCalendarOfMember(period.getEndMember()).getTime());
	}

	public LiveImagingDataSource(String host, String port, String objectName, String objectType, String liveImagingType,
	        Date start, Date end)
	        throws Exception {
		mHost = host;
		mPort = port;
		
		Collection<TO<?>> objs = env.getObjects(objectType, new ValueTO[] {new ValueTO("name", objectName)});
		
		if (objs == null || objs.isEmpty()) {
			throw new IllegalArgumentException("Object with name '" + objectName + "' and type '" + objectType + "' not found");
		} else if (objs.size() > 1) {
			throw new IllegalArgumentException("There is more than one object with name '" + objectName + "' and type '" + objectType + "'");
		}
		values = getData(objs.iterator().next(), LiveImagingType.getTypeByName(liveImagingType),
		        start, end).iterator();
	}

	@Override
	public Object getFieldValue(JRField field) throws JRException {
		String name = namesMap.get(field.getName());
		if (name != null) {
			ValueTO vTo = current.getSinglePropValue(name);
			if (vTo != null) {
				return vTo.getValue();
			}
		}
		return null;
	}

	@Override
	public boolean next() throws JRException {
		if (values.hasNext()) {
			current = values.next();
			return true;
		}
		return false;
	}

	private Collection<CollectionTO> getData(TO<?> liveImagingObject, LiveImagingType imagingType,
	        Date startDate, Date endDate) throws Exception {
		Collection<CollectionTO> resultCollection = CollectionUtils.newList();

		String dcName;
		String roomName;
		
		String typeName = liveImagingObject.getTypeName();
		if (DC.equalsIgnoreCase(typeName)) {
			dcName = env.getPropertyValue(liveImagingObject, "name", String.class);
			roomName = null;
		} else if (ROOM.equalsIgnoreCase(typeName)) {
			roomName =  env.getPropertyValue(liveImagingObject, "name", String.class);
			TO<?> dc = env.getParents(liveImagingObject, DC).iterator().next();
			dcName = env.getPropertyValue(dc, "name", String.class);
		} else {
			throw new IllegalArgumentException("TO type must be either DC or ROOM. Got " + typeName);
		}
		

		URL li_url = new URL("http://" + mHost + ":" + mPort + "/getImages?objId=" + liveImagingObject + "&layer="
		        + imagingType.getLayer() + "&dataclass=" + imagingType.getDataClassId() + "&start="
		        + startDate.getTime() + "&end=" + endDate.getTime());

		HttpURLConnection conn = (HttpURLConnection) li_url.openConnection();
		if (conn.getResponseCode() == 200) {
			StringBuilder result = new StringBuilder();
			BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			String line;
			while ((line = rd.readLine()) != null) {
				result.append(line);
			}
			rd.close();
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonImages = mapper.readTree(result.toString());
			for (JsonNode jsonImage : jsonImages) {
				CollectionTO imageCollection = new CollectionTO(liveImagingObject);
				imageCollection.addValue(new ValueTO("imageURL", "http://" + mHost + ":" + mPort + "/"
				        + jsonImage.get("url").asText()));
				imageCollection.addValue(new ValueTO("legendURL", "http://" + mHost + ":" + mPort + "/"
				        + jsonImage.get("legend").get("si_url").asText()));
				imageCollection.addValue(new ValueTO("imageTimeStamp", new Date(jsonImage.get("timestamp").asLong())));
				imageCollection.addValue(new ValueTO("dcName", dcName));
				imageCollection.addValue(new ValueTO("roomName", roomName));
				imageCollection.addValue(new ValueTO("imagingType", imagingType.getTypeName()));
				resultCollection.add(imageCollection);
			}
		}
		conn.disconnect();
		return resultCollection;
	}

	public static void main(String[] args) throws Exception {
		LiveImagingDataSource dataSource = new LiveImagingDataSource("dendrite", "9091", "Titan Datacenter2", "DC",
		        "Temp-Middle", new Date(System.currentTimeMillis() - 2 * 60 * 60 * 1000), new Date());
		JRDesignField field = new JRDesignField();
		field.setName("Image URL");
		while (dataSource.next()) {
			System.out.println(dataSource.getFieldValue(field));
		}
		System.out.println("Success");
	}
	
	public enum LiveImagingType {
		/*
		 * Temperature Top
		 */
		TEMP_TOP(200, 131072, "Temp-Top"), 
		TEMP_MIDDLE(200, 16384, "Temp-Middle"),
		TEMP_BOTTOM(200, 2048, "Temp-Bottom"),
		TEMP_SUBFLOOR(200, 256, "Temp-Subfloor"),
		TEMP_CONTROLLED(200, 128, "Temp-Controlled"),
		HUMIDITY(201, 131072, "Humidity"),
		PRESSURE(202, 256, "Pressure"),
		DEW_POINT(998, 131072, "Dew Point");

		private Integer dataClassId;
		private Integer layer;
		private String typeName;

		private LiveImagingType(Integer dataClassId, Integer layer, String typeName) {
			this.dataClassId = dataClassId;
			this.layer = layer;
			this.typeName = typeName;
		}

		public static LiveImagingType getTypeByName(String typeName) {
			for (LiveImagingType type : LiveImagingType.values()) {
				if (type.typeName.equals(typeName))
					return type;
			}
			throw new IllegalArgumentException("Not supported LiveImagingType, type=" + typeName);	
		}
		
		public Integer getDataClassId() {
			return dataClassId;
		}

		public Integer getLayer() {
			return layer;
		}

		public String getTypeName() {
			return typeName;
		}
	}
}
