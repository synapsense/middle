package com.synapsense.service.reporting.exception;

import javax.ejb.ApplicationException;

import com.synapsense.service.reporting.management.ReportingException;

@ApplicationException(rollback = false)
public class SmtpReportingException extends ReportingException {

	private static final long serialVersionUID = 8438474555584143684L;

	public SmtpReportingException() {
	}

	public SmtpReportingException(String message) {
		super(message);
	}

	public SmtpReportingException(Throwable cause) {
		super(cause);
	}

	public SmtpReportingException(String message, Throwable cause) {
		super(message, cause);
	}
}
