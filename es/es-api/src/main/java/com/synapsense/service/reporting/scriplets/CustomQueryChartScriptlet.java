package com.synapsense.service.reporting.scriplets;

import java.awt.Color;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import net.sf.jasperreports.engine.JRDefaultScriptlet;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;
import net.sf.jasperreports.engine.JRScriptletException;
import net.sf.jasperreports.renderers.JCommonDrawableRenderer;

import org.jfree.chart.JFreeChart;
import org.jfree.data.statistics.Statistics;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;
import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;
import com.synapsense.service.reporting.datasource.AbstractCustomQueryDataSource;
import com.synapsense.service.reporting.olap.AxisSet;
import com.synapsense.service.reporting.olap.Member;
import com.synapsense.service.reporting.olap.OrderedHierarchy;
import com.synapsense.service.reporting.olap.Tuple;
import com.synapsense.service.reporting.olap.custom.Cube;
import com.synapsense.service.reporting.scriplets.charts.AdvancedChartData;
import com.synapsense.service.reporting.scriplets.charts.AdvancedChartData.AdvancedChartDataBuilder;
import com.synapsense.service.reporting.scriplets.charts.ChartBuilder;
import com.synapsense.service.reporting.scriplets.charts.ChartCustomizer;
import com.synapsense.service.reporting.scriplets.charts.ChartDescr;
import com.synapsense.service.reporting.scriplets.charts.LineDescr;
import com.synapsense.service.reporting.scriplets.charts.SensorPoint;
import com.synapsense.service.reporting.scriplets.charts.TimeSeriesChartBuilder;
import com.synapsense.util.CollectionUtils;
import com.synapsense.util.unitconverter.Dimension;

public class CustomQueryChartScriptlet extends JRDefaultScriptlet {
	
	private static final String ID_PARAM_NAME = "Custom Query ID";
	private static final String INTERVAL_PARAM_NAME = "Report Period (hours)";
	private static final String PERIOD_PARAM_NAME = "Period";
	
	private AdvancedChartData advancedChartData; 

	private JFreeChart getChart(TO<?> customQueryTo, Date startDate, Date endDate) throws Exception {

		final ChartBuilder chartBuilder = new TimeSeriesChartBuilder();
		final Map<String, LineDescr> lineDescrMap = new HashMap<String, LineDescr>();
        final List<NumberFormat> numberFormats = CollectionUtils.newList();

		final ListMultimap<String, Number> dimensionMeanMap = ArrayListMultimap.create();
		final Set<String> aggregationDimensions = CollectionUtils.newSet();
		
		new AbstractCustomQueryDataSource(customQueryTo, startDate, endDate) {

			@Override
			public boolean next() throws JRException {
				return false;
			}

			@Override
			public Object getFieldValue(JRField arg0) throws JRException {
				return null;
			}

			@Override
			protected void processProperty(String objName, String objType, String propName, Dimension dimension,
			        Collection<ValueTO> valueTos, float lineWidth, Color lineColor, boolean advancedChartAverageNeeded) {

				ArrayList<SensorPoint> points = new ArrayList<SensorPoint>(valueTos.size());

				for (ValueTO valueTo : valueTos) {
					Object value = valueTo.getValue();
					if (value != null) {
						points.add(new SensorPoint((Double) value, valueTo.getTimeStamp()));

					}
				}
		        
				String serieId = dimension.getName();
				String serieName = objName + ":" + getDisplayablePropertyName(objType, propName, locale);
				String axisName = null;
				if (!dimension.getUnits().isEmpty()) {
					axisName = localizationService.getString(dimension.getUnits());
				} else {
					String longUnits = dimension.getLongUnits();
					if (longUnits != null && !longUnits.isEmpty() && longUnits.contains("/")) {
						longUnits = longUnits.replace("/", "_");
					}
					axisName = localizationService.getString(longUnits);
				}
				chartBuilder.addDataSerie(serieId, points, serieName,
						axisName, 0);
				if (advancedChartAverageNeeded) {
					List<Number> values = CollectionUtils.newList();
					for (SensorPoint point :points) {
						values.add(point.getValue());
					}
			        double mean = Statistics.calculateMean(values);
			        if (aggregationDimensions.isEmpty()) {
			        	aggregationDimensions.add(serieId);
			        }
			        if (serieId.equals(aggregationDimensions.iterator().next())) {
			        	dimensionMeanMap.put(serieId, mean);
			        }
				}
				
				lineDescrMap.put(serieName, new LineDescr(lineWidth, lineColor));

			}

			@Override
			protected void beforeProcessing(String chartDescr) {
                numberFormats.add(numberFormat);
			}

			@Override
			protected void afterProcessing(String chartDescrString) {
				chartBuilder.setSeriesNameDescrMap(lineDescrMap);
				ChartDescr chartDescr = ChartDescr.unpackFromString(chartDescrString);
				chartBuilder.setChartDescriptor(chartDescr);
			}
		};

		JFreeChart timeChart = chartBuilder.getChart(numberFormats.iterator().next());
		
		
		//Fill advanced chart data
		advancedChartData = chartBuilder.getChartDescriptor().getAdvancedChartData();
		
		if (advancedChartData != null) {
			advancedChartData = new AdvancedChartDataBuilder()
				.aggregationColorsMap(advancedChartData.getAggregationColorsMap())
				.thresholds(advancedChartData.getThresholds())
				.dimensionMeanMap(dimensionMeanMap)
				.valueAxisMaximumMap(chartBuilder.getValueAxisMaximumMap())
				.valueAxisMinimumMap(chartBuilder.getValueAxisMinimumMap())
				.build();			
		}
		ChartCustomizer.standartCustomXYPlot(timeChart, advancedChartData);
		
		return timeChart;
	}
	
	public AdvancedChartData getAdvancedChartData() {
		return advancedChartData;
	}

	public JFreeChart getChart(TO<?> customQueryTo, AxisSet period) throws Exception {
		List<Tuple> rangeTuples = period.getTuples();
		Member startMember = rangeTuples.get(0).itemByDimension(Cube.DIM_TIME);
		Member endMember = rangeTuples.get(rangeTuples.size() - 1).itemByDimension(Cube.DIM_TIME);
		OrderedHierarchy timeHierarchy = Cube.getOrderedHierarchy(startMember.getHierarchy().getName());
		return getChart(customQueryTo
				, timeHierarchy.getCalendarOfMember(startMember).getTime()
				, timeHierarchy.getCalendarOfMember(endMember).getTime());
	}
	
	@Override
	public void afterReportInit() throws JRScriptletException {
		try {
			String customQuery = (String) this.getParameterValue(ID_PARAM_NAME);
			Number numberValue = (Integer) this.getParameterValue(INTERVAL_PARAM_NAME);
			JFreeChart chart;
			if (numberValue != null) {
				Long intervalInHours = numberValue.longValue();
				chart = getChart(com.synapsense.dto.TOFactory.getInstance().loadTO(customQuery),
			        new java.util.Date(System.currentTimeMillis() - intervalInHours * 60 * 60 * 1000),
			        new java.util.Date());

			} else {
				AxisSet period = (AxisSet) this.getParameterValue(PERIOD_PARAM_NAME);
				chart = getChart(com.synapsense.dto.TOFactory
					.getInstance().loadTO(customQuery), period);
			}
			
			this.setVariableValue("Chart", new JCommonDrawableRenderer(chart));
		} catch (Exception e) {
			throw new JRScriptletException(e.getMessage(), e);
		}
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception {

		CustomQueryChartScriptlet scriplet = new CustomQueryChartScriptlet();

		JFreeChart timeChart = scriplet.getChart(
		        com.synapsense.dto.TOFactory.getInstance().loadTO("CUSTOMQUERY:57905"),
		        new java.util.Date(System.currentTimeMillis() - 2 * 60 * 60 * 1000), new java.util.Date());

		ChartBuilder.writeChartAsPNG(timeChart, "chart.png", 700, 400);

	}
}
