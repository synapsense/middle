package com.synapsense.service.impl.messages;

import com.synapsense.service.impl.messages.base.NodeVersionData;

public class NodeVersion implements BusMessage<NodeVersionData, Long> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8566700146109065119L;
	private long id;
	private NodeVersionData value;

	public NodeVersion(long id, NodeVersionData data) {
		super();
		this.id = id;
		this.value = data;
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public Type getType() {
		return Type.NODE_VERSION;
	}

	@Override
	public NodeVersionData getValue() {
		return value;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public void setValue(NodeVersionData value) {
		this.value = value;
	}
}
