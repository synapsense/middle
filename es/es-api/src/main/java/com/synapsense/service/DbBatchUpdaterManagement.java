package com.synapsense.service;

public interface DbBatchUpdaterManagement extends ServiceManagement {

	int showQueueSize();

	String showQueueInfo();

	String showSettings();

	String showValueMappingFor(int objId, String propName);

	String showAttributeMappingFor(String typeName, String propName);

	String showStatistic();

	void startCollectStatistics();

	void setBatchSize(int newSize);

	void setThreadsNumber(int newThreadsNumber);

	void setFlushDelay(long newDelay);

	void setQueueSize(int newSize);

}
