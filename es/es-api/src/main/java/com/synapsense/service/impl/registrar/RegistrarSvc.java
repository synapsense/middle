package com.synapsense.service.impl.registrar;

public interface RegistrarSvc {
	
	public void registerComponent(String componentName, String connectionInfo);

	public void unRegisterComponent(String componentName);

	public String getComponentConnection(String componentName);

	public String[] getComponents();

	public <COMPONENT_PROXY> COMPONENT_PROXY getComponentProxy(String name, Class<COMPONENT_PROXY> clazz);

	public void configureComponent(String componentName);
}
