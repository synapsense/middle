package com.synapsense.service.reporting.olap;

/**
 * Interface PlanProcessor.
 * Base interface for all possible implementations of sets that can define
 * either an axis or a slicer of a dimensional query
 */
public interface PlanProcessor {

	/**
	 * Process AxisSet.
	 *
	 * @param axisSet the axis set
	 */
	void processSet(AxisSet axisSet);
	
	/**
	 * Process CrossJoin.
	 *
	 * @param crossJoin the cross join
	 */
	void processSet(CrossJoin crossJoin);
	
	/**
	 * Process TupleSet.
	 *
	 * @param tupleSet the tuple set
	 */
	void processSet(TupleSet tupleSet);
	
	/**
	 * Process Tuple.
	 *
	 * @param tuple the tuple
	 */
	void processSet(Tuple tuple);
	
	/**
	 * Process Member.
	 *
	 * @param member the member
	 */
	void processSet(Member member);
	
	/**
	 * Process AllMemberSet.
	 *
	 * @param allmemberSet the allmember set
	 */
	void processSet(AllMemberSet allmemberSet);
	
	/**
	 * Process RangeSet.
	 *
	 * @param rangeSet the range set
	 */
	void processSet(RangeSet rangeSet);

	/**
	 * Process FilterableAllMemberSet.
	 *
	 * @param filterableAllMemberSet the filterable all member set
	 */
	void processSet(FilterableAllMemberSet filterableAllMemberSet);

	/**
	 * Process RangeMember.
	 *
	 * @param member the member
	 */
	void processSet(RangeMember member);

	/**
	 * Process SimpleMember.
	 *
	 * @param member the member
	 */
	void processSet(SimpleMember member);

    /**
     * Process Union.
     *
     * @param union the union
     */
    void processSet(Union union);
}
