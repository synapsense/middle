package com.synapsense.service;

import java.io.IOException;

public interface FileWriter {
	enum WriteMode {
	APPEND, REWRITE
	};

	void writeFile(String fileName, String data, WriteMode mode) throws IOException;

	String readFile(String fileName) throws IOException;

	void writeFileBytes(String fileName, byte[] data, WriteMode mode) throws IOException;

	byte[] readFileBytes(String fileName) throws IOException;
	
	byte[] readFileBytes(String fileName, int offset) throws IOException;

	void deleteFile(String fileName) throws IOException;
}
