package com.synapsense.service.reporting.scriplets.charts;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Paint;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.jfree.chart.JFreeChart;
import org.jfree.chart.annotations.XYBoxAnnotation;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.CategoryLabelPositions;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.DateTickUnit;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.Marker;
import org.jfree.chart.plot.Plot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.ValueMarker;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.statistics.Statistics;
import org.jfree.ui.LengthAdjustmentType;
import org.jfree.ui.RectangleAnchor;
import org.jfree.ui.TextAnchor;

import com.google.common.collect.ListMultimap;
import com.synapsense.service.reporting.scriplets.charts.AdvancedChartData.ChartThresholdType;
import com.synapsense.service.reporting.scriplets.charts.AdvancedChartData.ThresholdData;

public class ChartCustomizer {

	public static final String BG_STYLE_COLOR = "color";
	public static final String BG_STYLE_THRESHOLD = "threshold";
	
	public static void standartCustomXYPlot(JFreeChart chart)
	{
		standartCustomXYPlot(chart, null);
	}
	
	public static void standartCustomXYPlot(JFreeChart chart,
			AdvancedChartData advancedChartData)	{

		XYPlot plot = chart.getXYPlot();
		plot.setOutlinePaint(Color.black);
		plot.setOrientation(PlotOrientation.VERTICAL);
		// plot.setBackgroundPaint(Color.white);
		plot.setDomainGridlinePaint(Color.lightGray);
		plot.setRangeGridlinePaint(Color.lightGray);
		plot.setDomainCrosshairVisible(true);
		plot.setRangeCrosshairVisible(true);
		(plot.getDomainAxis()).setVerticalTickLabels(true);
		if (advancedChartData == null) {
			return;
		}
        drawRangeMarkers(plot, advancedChartData);
        drawBars(plot, advancedChartData.getThresholds());
	}
	
	private static void drawRangeMarkers(XYPlot plot, AdvancedChartData advancedChartData) {

		ListMultimap<String, Number> dimensionMeanMap = advancedChartData.getDimensionMeanMap();
		
		Map<String, Number> valueAxisMaximumMap = advancedChartData.getValueAxisMaximumMap();
		Map<String, Number> valueAxisMinimumMap = advancedChartData.getValueAxisMinimumMap();
		NumberFormat numberFormatter = NumberFormat.getInstance();
		numberFormatter.setMaximumFractionDigits(2);
		
		Map<String, Integer> aggregationColorsMap = advancedChartData.getAggregationColorsMap();
		//draw maximum line
		for (Entry<String, Number> entry : valueAxisMaximumMap.entrySet()) {
			if (entry.getValue() == null) {
				continue;
			}
	        drawRangeMarker("max", entry.getValue().doubleValue(), plot, new Color(aggregationColorsMap.get("max")));
		}
		//draw minimum line    
		for (Entry<String, Number> entry : valueAxisMinimumMap.entrySet()) {
			if (entry.getValue() == null) {
				continue;
			}
	        drawRangeMarker("min", entry.getValue().doubleValue(), plot, new Color(aggregationColorsMap.get("min")));
		}
		//draw average line
		for (String key : dimensionMeanMap.keySet()) {
			List<Number> dimensionAverages = dimensionMeanMap.get(key);
			Number dimesnionMean = Statistics.calculateMean(dimensionAverages);
			if (dimesnionMean == null) {
				continue;
			}
	        drawRangeMarker("avg", dimesnionMean.doubleValue(), plot, new Color(aggregationColorsMap.get("avg")));
		}
		//draw thresholds
		List<ThresholdData> thresholds = advancedChartData.getThresholds();
		
        for (ThresholdData threshold : thresholds) {
        	if (threshold.getThresholdType().equals(ChartThresholdType.LINE)) {
				drawRangeMarker(
						numberFormatter.format(threshold.getStartValue()),
						threshold.getStartValue().doubleValue(), plot,
						new Color(threshold.getStartColor()));
        	}
        }
	}
	
	private static void drawBars(XYPlot plot, List<ThresholdData> thresholds) {			
		
		NumberFormat numberFormatter = NumberFormat.getInstance();
		numberFormatter.setMaximumFractionDigits(2);
		for (ThresholdData threshold : thresholds) {
        	if (threshold.getThresholdType().equals(ChartThresholdType.BAR)) {
        		drawBar(
						numberFormatter.format(threshold.getStartValue()),
						threshold.getStartValue().doubleValue(), threshold.getEndValue().doubleValue(), plot,
						new Color(threshold.getStartColor()));
        	}
        }
		
	}
	
	private static void drawBar(String markerLabel, Double startValue, Double endValue, XYPlot plot, Color markerColor){
		float[] components = markerColor.getComponents(null);
		
		float r = components[0];
		float g = components[1];
		float b = components[2];
		float a = (float) (components[3] * 0.2);
		markerColor = new Color(r,g,b,a);
		ValueAxis ax = plot.getDomainAxis();
		double upperBound = ax.getUpperBound();
		double lowerBound = ax.getLowerBound();	
		XYBoxAnnotation annotation = new XYBoxAnnotation(
				lowerBound, startValue, 
				upperBound, endValue,
                new BasicStroke(0.0f), markerColor, markerColor);

		StringBuilder tooltipSB = new StringBuilder();
		tooltipSB.append(String.valueOf(startValue.intValue()))
			.append(":")
			.append(String.valueOf(markerColor.getRGB()))
			.append(":")
			.append(String.valueOf(endValue.intValue()))
			.append(":")
			.append(String.valueOf(markerColor.getRGB()))
			.append(":null");
		
		annotation.setToolTipText(tooltipSB.toString());
		plot.addAnnotation(annotation);
	}
	
    public static void standartCustomCategoryPlot(JFreeChart chart)
    {
        CategoryPlot plot = chart.getCategoryPlot();
        plot.setOutlinePaint(Color.black);
        plot.setOrientation(PlotOrientation.VERTICAL);
        //plot.setBackgroundPaint(Color.white);
        plot.setDomainGridlinePaint(Color.lightGray);
        plot.setRangeGridlinePaint(Color.lightGray);
        plot.setDomainCrosshairVisible(true);
        plot.setRangeCrosshairVisible(true);
    }

    private static void defaultCustom(Plot plot) {

	}
	
	public static void timeCustom(JFreeChart timeChart, SimpleDateFormat simpleDateFormat)
	{
		XYPlot plot = timeChart.getXYPlot();				
		
		DateAxis axis = (DateAxis) plot.getDomainAxis();
		axis.setDateFormatOverride(simpleDateFormat);
	}
	
	public static void timeCustomDayTick(JFreeChart timeChart,SimpleDateFormat sdf,DateTickUnit dtu)
	{
		DateAxis axis = (DateAxis)(timeChart.getXYPlot()).getDomainAxis();
		
		axis.setDateFormatOverride(sdf);
		axis.setTickUnit(dtu);		
	}
		
	public static void categoryStandartCustom(JFreeChart categoryChart){
		CategoryPlot plot = (CategoryPlot) categoryChart.getPlot();
		CategoryAxis domainAxis = plot.getDomainAxis();		
                domainAxis.setCategoryLabelPositions(CategoryLabelPositions.createUpRotationLabelPositions(Math.PI / 6.0));		        
                //domainAxis.setCategoryLabelPositions(CategoryLabelPositions.createUpRotationLabelPositions(Math.PI / 2.0));
	}
	
	public static void fillBackgroundByValueRange(JFreeChart chart, ValueRanges ranges, String backroundStyle){		
		//Fill background if we have not null values only
	    if (ranges!= null && !(ranges.aMin == 0 && ranges.aMax == 0 && ranges.rMin == 0 && ranges.rMax == 0))
	    {
	    	if(BG_STYLE_THRESHOLD.equals(backroundStyle)){
	    		NumberFormat numberFormatter = NumberFormat.getInstance();
		    	numberFormatter.setMaximumFractionDigits(2);
		    	
	    		//draw threshold markers
	    		XYPlot plot = chart.getXYPlot();	    	
		    	drawRangeMarker(numberFormatter.format(ranges.aMin), ranges.aMin, plot, new Color(153, 0, 0));
		    	drawRangeMarker(numberFormatter.format(ranges.aMax), ranges.aMax, plot, new Color(153, 0, 0));
		    	drawRangeMarker(numberFormatter.format(ranges.rMin), ranges.rMin, plot, new Color(255, 204,0));
		    	drawRangeMarker(numberFormatter.format(ranges.rMax), ranges.rMax, plot, new Color(255, 204,0));
	    	}else{
	    		ChartCustomizer.fillBackgroundWithZones(chart, ranges.aMin, ranges.rMin, ranges.aMax, ranges.rMax);
	    	}	    	
	    }
	}
	
	public static void drawRangeMarker(String markerLabel, Double value, XYPlot plot, Color markerColor){
		// add a labelled marker for the threshold...
		Marker threshold = new ValueMarker(value);
		threshold.setLabelOffsetType(LengthAdjustmentType.EXPAND);
		threshold.setPaint(markerColor);
		threshold.setStroke(new BasicStroke(1.0f));
		threshold.setLabel(markerLabel);
		threshold.setLabelFont(new Font("SansSerif", Font.PLAIN, 11));
		threshold.setLabelPaint(markerColor);
		threshold.setLabelAnchor(RectangleAnchor.TOP_LEFT);
		threshold.setLabelTextAnchor(TextAnchor.BOTTOM_LEFT);
		plot.addRangeMarker(threshold);
	}
	
	public static void fillBackgroundWithZones(JFreeChart chart, double a_min, double r_min, double a_max, double r_max)
	{							
		XYPlot plot = chart.getXYPlot();		
		ValueAxis ax = plot.getDomainAxis();			
		ValueAxis rangeAx = plot.getRangeAxis();
		
		double upperBound = ax.getUpperBound();
		double lowerBound = ax.getLowerBound();									
		
		Paint transpGreen = new Color(0, 255, 0, 60);
		Paint transpRed = new Color(255, 0, 0, 60);
		Paint transpYellow = new Color(255, 255, 50, 60);
		
		XYBoxAnnotation annotation;
		//Red zone
		annotation = new XYBoxAnnotation(
				lowerBound, rangeAx.getLowerBound(), 
				upperBound, a_min,
                new BasicStroke(0.0f), transpRed, transpRed);
		
		plot.addAnnotation(annotation);
		
		annotation = new XYBoxAnnotation(
				lowerBound, a_max, 
				upperBound, rangeAx.getUpperBound(),
                new BasicStroke(0.0f), transpRed, transpRed);
		
		plot.addAnnotation(annotation);
		
		//Yellow zone
		annotation = new XYBoxAnnotation(
				lowerBound, a_min, 
				upperBound, r_min,
                new BasicStroke(0.0f), transpYellow, transpYellow);
		
		plot.addAnnotation(annotation);
		
		annotation = new XYBoxAnnotation(
				lowerBound, r_max, 
				upperBound, a_max,
                new BasicStroke(0.0f), transpYellow, transpYellow);
	
		plot.addAnnotation(annotation);
		
		//Green zone
		annotation = new XYBoxAnnotation(
				lowerBound, r_min, 
				upperBound, r_max,
                new BasicStroke(0.0f), transpGreen, transpGreen);
	
		plot.addAnnotation(annotation);		
		
	}
	
     /**
     * Returns yAxis name by request class id
     * @param class_id
     */
	public static String getYAxisNameByClassId(String class_id){
		return classIdyAxisNamesMap.get(class_id);
	}
	
	//------------ yAxis Names Mapping
	protected final static HashMap<String, String> classIdyAxisNamesMap = new HashMap<String, String>();	
	static {		
		classIdyAxisNamesMap.put("200", "Temperature/deg. F");
		classIdyAxisNamesMap.put("201", "Relative Humidity/%");
		classIdyAxisNamesMap.put("202", "Pressure/inH2O");
		classIdyAxisNamesMap.put("203", "Purity");
		classIdyAxisNamesMap.put("204", "Particle Count");
		classIdyAxisNamesMap.put("205", "Voltage/V");
		classIdyAxisNamesMap.put("206", "Current/A");
		classIdyAxisNamesMap.put("207", "Energy/kWh");
		classIdyAxisNamesMap.put("208", "pH");
		classIdyAxisNamesMap.put("209", "Liquid Flow/GPM");
		classIdyAxisNamesMap.put("210", "Battery Strength/V");
		classIdyAxisNamesMap.put("211", "Door Status");
		classIdyAxisNamesMap.put("212", "Air Flow/SCCM");
		classIdyAxisNamesMap.put("213", "Liquid Presence");
		classIdyAxisNamesMap.put("214", "Equipment Status");
	}
}
