package com.synapsense.service.nsvc.events;

import java.util.Arrays;

import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;
import com.synapsense.service.nsvc.Event;
import com.synapsense.service.nsvc.EventDispatcher;

public class ObjectCreatedEvent implements Event {

	private static final long serialVersionUID = 6103174793109504255L;

	private final TO<?> objectTO;
	private final ValueTO[] values;

	public ObjectCreatedEvent(TO<?> objectTO) {
		this(objectTO, null);
	}

	public ObjectCreatedEvent(TO<?> objectTO, ValueTO[] values) {
		if (values == null)
			values = new ValueTO[] {};
		this.objectTO = objectTO;
		this.values = values;
	}

	public TO<?> getObjectTO() {
		return objectTO;
	}

	public ValueTO[] getValues() {
		return values;
	}

	@Override
	public Event dispatch(EventDispatcher ed) {
		return ed.dispatch(this);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(super.toString()).append(": TO=").append(objectTO).append(", properties=")
		        .append(Arrays.toString(values));
		return sb.toString();
	}

}
