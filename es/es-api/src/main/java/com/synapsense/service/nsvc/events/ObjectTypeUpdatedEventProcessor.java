package com.synapsense.service.nsvc.events;

public interface ObjectTypeUpdatedEventProcessor {
	ObjectTypeUpdatedEvent process(ObjectTypeUpdatedEvent e);
}
