package com.synapsense.service.reporting.dto;

import java.io.Serializable;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.synapsense.service.reporting.exception.ReportingQueryException;
import com.synapsense.util.CollectionUtils;

/**
 * The <code>TableTO</code> interface presents Report Table TO.
 * 
 * @author Grigory Ryabov
 */
public class TableTO implements Iterable<RowTO>, Serializable {

	private static final long serialVersionUID = -8516275075396623604L;
	private List<RowTO> reportRows;
	private int userId;
	private Map<Integer, List<String>> headerMap = CollectionUtils.newMap();
	private int rowsNumber;

	public TableTO(List<RowTO> reportRows, int userId, Map<Integer, List<String>> headerMap, int rowsNumber) {
		this.reportRows = CollectionUtils.newList(reportRows);
		this.userId = userId;
		this.headerMap.putAll(headerMap);
		this.rowsNumber = rowsNumber;
	}

	@Override
	public Iterator<RowTO> iterator() {
		return new RowIterator();
	}

	public int getUserId() {
		return this.userId;
	}

	public TableTO getTable() {
		return new TableTO(reportRows, userId, Collections.unmodifiableMap(headerMap), rowsNumber);
	}

	public List<String> getHeader(int index) {
		List<String> result = CollectionUtils.newList();
		if (!headerMap.containsKey(index)) {
			return result;
		}
		List<String> headers = headerMap.get(index);
		result.addAll(headers);
		return result;
	}
	
	public int getHeight() {
		return rowsNumber;
	}
	public void setHeight(int rows) {
		this.rowsNumber = rows;
	}

	public class RowIterator implements Iterator<RowTO>, Serializable {

		private static final long serialVersionUID = -7261799849746000436L;
		
		private int row = 0;
		public RowIterator() {
		}

		@Override
		public boolean hasNext() {
			if (row + 1 > rowsNumber) {
				return false;
			}
			return true;
		}

		@Override
		public RowTO next() {
			if (!hasNext()){
				return null;
			}
			return reportRows.get(++row - 1);
		}
		
		@Override
		public void remove() {
			throw new UnsupportedOperationException("Remove is not supported"); 
		}
	}

}
