package com.synapsense.service.reporting.olap.custom;

public class WeekLevel extends BaseLevel {

	private static final long serialVersionUID = -1042186984232651326L;

	public WeekLevel() {
		super(Cube.WEEK, Cube.LEVEL_WEEK);
	}
	
}
