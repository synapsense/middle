package com.synapsense.service.nsvc.events;

public interface ObjectTypeCreatedEventProcessor {
	ObjectTypeCreatedEvent process(ObjectTypeCreatedEvent e);
}
