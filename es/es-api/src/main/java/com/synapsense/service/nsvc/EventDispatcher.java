package com.synapsense.service.nsvc;

import com.synapsense.service.nsvc.events.AlertRaisedEvent;
import com.synapsense.service.nsvc.events.ConfigurationCompleteEvent;
import com.synapsense.service.nsvc.events.ConfigurationStartedEvent;
import com.synapsense.service.nsvc.events.ObjectCreatedEvent;
import com.synapsense.service.nsvc.events.ObjectDeletedEvent;
import com.synapsense.service.nsvc.events.ObjectTypeCreatedEvent;
import com.synapsense.service.nsvc.events.ObjectTypeDeletedEvent;
import com.synapsense.service.nsvc.events.ObjectTypeUpdatedEvent;
import com.synapsense.service.nsvc.events.PropertiesChangedEvent;
import com.synapsense.service.nsvc.events.ReferencedObjectChangedEvent;
import com.synapsense.service.nsvc.events.ReferencedPropertyChangedEvent;
import com.synapsense.service.nsvc.events.RelationRemovedEvent;
import com.synapsense.service.nsvc.events.RelationSetEvent;

public interface EventDispatcher {

	Event dispatch(Event e);

	ObjectTypeCreatedEvent dispath(ObjectTypeCreatedEvent e);

	ObjectTypeDeletedEvent dispath(ObjectTypeDeletedEvent e);

	ObjectTypeUpdatedEvent dispath(ObjectTypeUpdatedEvent e);

	ObjectCreatedEvent dispatch(ObjectCreatedEvent e);

	ObjectDeletedEvent dispatch(ObjectDeletedEvent e);

	PropertiesChangedEvent dispatch(PropertiesChangedEvent e);

	ReferencedObjectChangedEvent dispatch(ReferencedObjectChangedEvent e);

	RelationRemovedEvent dispatch(RelationRemovedEvent e);

	RelationSetEvent dispatch(RelationSetEvent e);

	/**
	 * @deprecated To catch raised alerts use ObjectCreatedEvent.
	 */
	@Deprecated
	AlertRaisedEvent dispatch(AlertRaisedEvent e);

	ConfigurationStartedEvent dispatch(ConfigurationStartedEvent e);

	ConfigurationCompleteEvent dispatch(ConfigurationCompleteEvent e);

	ReferencedPropertyChangedEvent dispatch(ReferencedPropertyChangedEvent e);
}
