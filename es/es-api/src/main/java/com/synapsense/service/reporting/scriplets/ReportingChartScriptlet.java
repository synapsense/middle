package com.synapsense.service.reporting.scriplets;

import com.synapsense.service.reporting.datasource.ReportingTableDataSource;
import com.synapsense.service.reporting.olap.RangeSet;
import com.synapsense.service.reporting.scriplets.charts.ChartBuilder;
import com.synapsense.service.reporting.scriplets.charts.ChartCustomizer;
import com.synapsense.service.reporting.scriplets.charts.DimensionTimeSeriesChartBuilder;
import com.synapsense.service.reporting.scriplets.charts.TimeSeriesChartBuilder;
import com.synapsense.util.CollectionUtils;

import net.sf.jasperreports.engine.JRScriptletException;
import org.jfree.chart.JFreeChart;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

public class ReportingChartScriptlet extends AbstractChartScriptlet {
	
    @Override
    protected void standartCustom(JFreeChart timeChart) {
        ChartCustomizer.standartCustomXYPlot(timeChart);
    }

    @Override
    protected ChartBuilder getChartBuilder() {
    	return new TimeSeriesChartBuilder();
    }

    @Override
    protected void getData(RangeSet period) throws JRScriptletException {
        try {
	        String columnProperties = (String) this.getParameterValue(COLUMN_PROPERTIES);
	        JSONObject columnPropertiesJSON = new JSONObject(columnProperties);
	        JSONArray propertiesJsonArray = columnPropertiesJSON.getJSONArray(PROPERTIES);
	        List<String> properties = CollectionUtils.newList();

	        for (int i = 0; i < propertiesJsonArray.length(); i++) {
	            JSONObject jsonObject = propertiesJsonArray.getJSONObject(i);
	            if ((boolean) jsonObject.get("value") == true) {
	            	String fullPropertyName = (String) jsonObject.get("name");
	                properties.add(fullPropertyName);	                
	            }
	        }
	        
	        Object objects = this.getParameterValue(OBJECTS);
	        String[] propertiesArray = properties.toArray(new String[properties.size()]);
	        String aggregationRange = (String) this.getParameterValue(AGGREGATION_RANGE);
	        reportingChartDataSource = ReportingTableDataSource.newChart(
	                objects, propertiesArray, aggregationRange, period);
	        
	        reportTable = reportingChartDataSource.getData();
	        initUserPreferences();
	        //Set customized values
	        if (new JSONObject(this.parametersMap.get(COLUMN_PROPERTIES).getDescription()).has(IS_CUSTOM_SPECIFIED)) {
	        	processCustomValues(this.parametersMap.get(COLUMN_PROPERTIES).getPropertiesMap(), columnPropertiesJSON);
        	}
	        //Customize lines
	        fillPropertyLineOptionMap(propertiesJsonArray);
	        //Customize dimensions
	        if (columnPropertiesJSON.has("dimensions") 
	        		&& columnPropertiesJSON.getJSONArray("dimensions").length() != 0) {
	        	fillDimensionOptionMap(columnPropertiesJSON.getJSONArray("dimensions"), unitSystems);
	        }
	        
        } catch (Exception e) {
            e.printStackTrace();
        }
    }  
    
}
