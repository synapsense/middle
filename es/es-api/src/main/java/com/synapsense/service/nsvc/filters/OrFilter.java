package com.synapsense.service.nsvc.filters;

import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;

import com.synapsense.service.nsvc.Event;
import com.synapsense.service.nsvc.EventProcessor;

public class OrFilter implements EventProcessor, Serializable {

	private static final long serialVersionUID = 6043765189000195654L;

	private final Collection<EventProcessor> filters;

	public OrFilter(Collection<EventProcessor> filters) {
		if (filters == null)
			this.filters = Collections.emptyList();
		else
			this.filters = filters;
	}

	@Override
	public Event process(Event e) {
		Event result = e;
		for (EventProcessor filter : filters) {
			result = filter.process(e);
			if (result != null)
				return result;
		}
		return result;
	}

}
