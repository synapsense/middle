package com.synapsense.service.impl.messages.base;

import java.io.Serializable;
import java.util.Date;

/**
 * Class represents the sensor data content.
 * <p>
 * The instances of this class are used to transmit the flow of sensor data from
 * clients to ES.
 * 
 * @author Alexander Borisov
 * 
 */
public final class SensorData implements Serializable {
	private static final long serialVersionUID = 226169101456763900L;

	/**
	 * sensor identity (>0)
	 * 
	 */
	private int sensorId;

	/**
	 * sensor data
	 */
	private double data;

	/**
	 * sensor data timestamp in ms. specifies the timestamp when the data has to
	 * be saved. (>=1000L)
	 */
	private long timestamp;

	/**
	 * network identity (>0)
	 */
	private int networkId;

	/**
	 * node platform idenity (>0)
	 */
	private int nodePlatform;

	/**
	 * sensor type identity (>0)
	 */
	private int typeId;

	/**
	 * All fields constructor. Allows caller to initialize well-formed piece of
	 * sensor data.
	 * 
	 * @param sensorId
	 *            ID of sensor (>0)
	 * @param data
	 *            Sensor's data
	 * @param timestamp
	 *            Sensor's data timestamp(>=1000L)
	 * @param networkId
	 *            ID of network (>0)
	 * @param nodePlatform
	 *            ID of node platform (>0)
	 * @param typeId
	 *            ID of sensor type (>0)
	 */
	public SensorData(int sensorId, double data, long timestamp, int networkId, int nodePlatform, int typeId) {
		setSensorId(sensorId);
		setData(data);
		setTimestamp(timestamp);
		setNetworkId(networkId);
		setNodePlatform(nodePlatform);
		setTypeId(typeId);
	}

	public int getSensorId() {
		return sensorId;
	}

	public void setSensorId(int sensorId) {
		this.sensorId = sensorId;
	}

	public double getData() {
		return data;
	}

	public void setData(double data) {
		this.data = data;
	}

	public long getTimestamp() {
		return timestamp;
	}

	/**
	 * Sensor's data timestamp setter
	 * 
	 * @param timestamp
	 *            The new timestamp (>=1000L)
	 */
	public void setTimestamp(long timestamp) {
		if (timestamp < 1000L) {
			throw new IllegalArgumentException(
			        "Supplied 'timestamp' is less than 1 second.Such presision is not supported.");
		}
		this.timestamp = timestamp;
	}

	public int getNetworkId() {
		return networkId;
	}

	public void setNetworkId(int networkId) {
		this.networkId = networkId;
	}

	public int getTypeId() {
		return typeId;
	}

	public void setTypeId(int typeId) {
		this.typeId = typeId;
	}

	public int getNodePlatform() {
		return nodePlatform;
	}

	public void setNodePlatform(int nodePlatform) {
		this.nodePlatform = nodePlatform;
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer(20);
		sb.append("sensor channel=");
		sb.append(sensorId);
		sb.append("\n");

		sb.append("data=");
		sb.append(data);
		sb.append("\n");

		sb.append("timestamp=");
		sb.append(new Date(timestamp));
		sb.append("\n");

		return sb.toString();
	}

}
