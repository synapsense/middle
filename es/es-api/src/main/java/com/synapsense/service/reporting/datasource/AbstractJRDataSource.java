package com.synapsense.service.reporting.datasource;

import com.synapsense.dto.TO;
import com.synapsense.service.*;
import com.synapsense.service.reporting.ReportingService;
import com.synapsense.service.reporting.ReportingSessionContext;
import com.synapsense.service.reporting.dto.TableTO;
import com.synapsense.service.reporting.olap.Member;
import com.synapsense.service.reporting.olap.RangeSet;
import com.synapsense.service.reporting.olap.SimpleMember;
import com.synapsense.service.reporting.olap.custom.Cube;
import com.synapsense.service.reporting.olap.custom.EnvironmentDataSource;
import com.synapsense.service.reporting.olap.custom.ReportingEnvironmentDAO;
import com.synapsense.util.CollectionUtils;
import com.synapsense.util.ServerLoginModule;
import net.sf.jasperreports.engine.JRDataSource;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.util.List;
import java.util.Properties;
/**
 * The abstract <code>AbstractJRDataSource</code> class for Table and Chart Datasources for accessing historical Data from Environment Server.<br>
 * @author Alexander Pakhunov
 * 
 */
public abstract class AbstractJRDataSource implements JRDataSource{
	protected static LocalizationService localizationService;
	protected static Environment env;
	protected static UserManagementService userManagement;
	protected static UnitSystemsService unitSystemsService;
	protected static AlertService alertService;
	protected static FileWriter fileWriter;
	protected static InitialContext ctx;
	protected static ReportingService reportingService;
	protected static EnvironmentDataSource dataSource;
	
	protected static boolean inited = false;

	public static final String DEFAULT_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
	
	protected AbstractJRDataSource() {
		initConnection();
	}
	
	protected static void initConnection() {
		
		if (!inited) {
			synchronized (AbstractJRDataSource.class) {
				if (!inited) {
					
					Properties connectProperties = new Properties();
					
					connectProperties.setProperty(Context.URL_PKG_PREFIXES, "org.jboss.ejb.client.naming");
					
					try{
						ctx = new InitialContext(connectProperties);
					} catch (NamingException e) {
						throw new IllegalStateException("Cannot create initial context to connect ES services", e);
					}
					
					userManagement = getProxy(ctx, "es-core", "UserManagementService", UserManagementService.class);
					env = getProxy(ctx, "es-core", "Environment", Environment.class);
					localizationService = getProxy(ctx, "es-core", "LocalizationService", LocalizationService.class);
					unitSystemsService = getProxy(ctx, "es-core", "UnitSystemsServiceImpl", UnitSystemsService.class);
					alertService = getProxy(ctx, "es-core", "AlertService", AlertService.class);
					fileWriter = getProxy(ctx, "es-core", "FileWriterService", FileWriter.class);
					reportingService = getProxy(ctx, "es-core", "ReportingService", ReportingService.class);
					dataSource = new ReportingEnvironmentDAO(env);
					
				}
	            inited = true;
            }
		}
	}
	
	protected static <INTERFACE> INTERFACE getProxy(InitialContext ctx, String moduleName, String jndiTarget, Class<INTERFACE> clazz) {
		try {
			return clazz.cast(ServerLoginModule.getService(ctx, moduleName, jndiTarget, clazz));
		} catch (NamingException e) {
			throw new IllegalStateException("Error acquiring reference to " + jndiTarget, e);
		}
	}
	
	protected String getUserName(int userId) {
		if (userId <= 0) {
			throw new IllegalArgumentException("Supplied 'userId' is negative or zero");
		}

		String userName = getUserName();
		if (userName != null) {
			return userName;
		}

		TO<?> user = getUserById(userId);
		if (user == null) {
			throw new IllegalArgumentException("There is no user with given 'userId'=" + userId);
		}

		return (String) userManagement.getProperty(user, userManagement.USER_NAME);
	}

    protected String getUserName() {
        String userName = ReportingSessionContext.getUserName();
        return userName;
    }

    protected TO<?> getUserById(int userId) {
        for (TO<?> user : userManagement.getAllUsers()) {
            if (user.getID().equals(userId)) {
                return user;
            }
        }
        return null;
    }

    
	protected List<Member> parseProperties(String[] allProperties) {
		List<Member> propertyTuples = CollectionUtils.newList();;
		// Setting columns
		for (String property : allProperties) {
			propertyTuples.add(new SimpleMember(Cube.PROPERTY, "Name", property));
		}
		return propertyTuples;
	}

    protected class DateRange {
		private RangeSet rangeSet;

		protected DateRange(int startDay, int startMonth, int startYear,
				int endDay, int endMonth, int endYear) {
			Member startMember = new SimpleMember(Cube.TIME, Cube.LEVEL_DAY,
					startDay, new SimpleMember(Cube.TIME, Cube.LEVEL_MONTH,
							startMonth, new SimpleMember(Cube.TIME,
									Cube.LEVEL_YEAR, startYear)));
			Member endMember = new SimpleMember(Cube.TIME, Cube.LEVEL_DAY,
					endDay, new SimpleMember(Cube.TIME, Cube.LEVEL_MONTH,
							endMonth, new SimpleMember(Cube.TIME,
									Cube.LEVEL_YEAR, endYear)));
			this.rangeSet = new RangeSet(Cube.TIME, startMember, endMember);
		}

		public RangeSet getRangeSet() {
			return rangeSet;
		}

	}

}
