package com.synapsense.service.reporting.olap.custom;

import com.google.common.collect.LinkedListMultimap;
import com.google.common.collect.ListMultimap;
import com.synapsense.dto.CollectionTO;
import com.synapsense.dto.PropertyDescr;
import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.EnvException;
import com.synapsense.service.Environment;
import com.synapsense.service.impl.dao.to.GenericObjectTO;
import com.synapsense.service.reporting.datasource.ReportType;
import com.synapsense.service.reporting.exception.ReportingServiceSystemException;
import com.synapsense.service.reporting.olap.Filter;
import com.synapsense.service.reporting.olap.Member;
import com.synapsense.service.reporting.olap.custom.PropertyHierarchy.ReportingPropertyName;
import com.synapsense.service.reporting.scriplets.AbstractChartScriptlet;
import com.synapsense.util.CollectionUtils;
import com.synapsense.util.ServerLoginModule;
import com.synapsense.util.unitconverter.Dimension;
import com.synapsense.util.unitconverter.UnitResolvers;
import com.synapsense.util.unitconverter.UnitSystems;
import org.apache.log4j.Logger;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.util.*;

public class ReportingEnvironmentDAO implements EnvironmentDataSource {

	private Environment environment;

	private static final Logger log = Logger.getLogger(ReportingEnvironmentDAO.class);
	
	private static boolean inited = false;
	
	private static InitialContext ctx;

	public ReportingEnvironmentDAO() {
		initConnection();
	}

    public ReportingEnvironmentDAO(Environment environment) {
        this.environment = environment;
    }
	
	@Override
	public Environment getEnvironment() {
		return environment;
	}

	protected void initConnection() {
		
		if (!inited) {
			synchronized (ReportingEnvironmentDAO.class) {
				if (!inited) {
					
					Properties connectProperties = new Properties();
					
					connectProperties.setProperty(Context.URL_PKG_PREFIXES, "org.jboss.ejb.client.naming");
					
					try{
						ctx = new InitialContext(connectProperties);
					} catch (NamingException e) {
						throw new IllegalStateException("Cannot create initial context to connect ES services", e);
					}
					
					environment = getProxy(ctx, "es-core", "Environment", Environment.class);
					
				}
	            inited = true;
            }
		}
	}

	protected static <INTERFACE> INTERFACE getProxy(InitialContext ctx, String moduleName, String jndiTarget, Class<INTERFACE> clazz) {
		try {
			return clazz.cast(ServerLoginModule.getService(ctx, moduleName, jndiTarget, clazz));
		} catch (NamingException e) {
			throw new IllegalStateException("Error acquiring reference to " + jndiTarget, e);
		}
	}
	
	@Override
	public Collection<CollectionTO> getPropertyValue(final Collection<TO<?>> objIds, final String[] propNames) {
		
		return environment.getPropertyValue(objIds, propNames);
		
	}

	/**
	 * Helper to get objects to names mapping
	 * 
	 * @return resulting map
	 */
	@Override
	public Map<String, TO<?>> getObjectsByName(final String level, final Set<String> objectNames) {
		if (level == null) {
			throw new IllegalArgumentException("Object Level shouldn't be null");
		}
		if (objectNames == null) {
			throw new IllegalArgumentException("Object Names shouldn't be null");
		}
		if (objectNames.isEmpty()) {
			throw new IllegalArgumentException("Object Names shouldn't be empty");
		}
		
		Map<String, TO<?>> result = CollectionUtils.newLinkedMap();
		Collection<TO<?>> allObjects = environment.getObjectsByType(level);
		Collection<CollectionTO> allNames = environment.getPropertyValue(allObjects, new String[] { "name" });
		for (CollectionTO nameCollection : allNames) {
			String name = nameCollection.getSinglePropValue("name").getValue().toString();
			if (objectNames.contains(name)) {
				result.put(name, nameCollection.getObjId());
			}
		}
		return result;
		
	}

	@Override
	public PropertyDescr getPropertyDescriptor(final String typeName, final String propertyName) {
		
		if (typeName == null) {
			throw new IllegalArgumentException("Type Name shouldn't be null");
		}
		if (propertyName == null) {
			throw new IllegalArgumentException("Property Name shouldn't be null");
		}

		try {
			return environment.getPropertyDescriptor(typeName, propertyName);
		} catch (EnvException e) {
			log.error(e.getLocalizedMessage(), e);
			throw new ReportingServiceSystemException("Failed to get property descriptor due to", e);
		}
		
	}

	@Override
	public List<TO<?>> getDescendantObjects(String objectType, Member parent) {
		
		List<TO<?>> objects = new LinkedList<>();
		Collection<TO<?>> allObjects = getDescendantTOs(objectType, parent);
		return (allObjects.isEmpty()) ? objects : new LinkedList<TO<?>>(allObjects);
		
	}
	

	@Override
	public List<TO<?>> getDescendantObjects(String objectType, Member parent, Filter filter) {
		
		List<TO<?>> objects = new LinkedList<>();
		Collection<TO<?>> allObjects = getDescendantTOs(objectType, parent);
		if (allObjects.isEmpty()) {
			return objects;
		}

		List<String> allProperties = new LinkedList<>(filter.getProperties());
		allProperties.add("name");

		Collection<CollectionTO> allPropertiesValues = environment.getPropertyValue(allObjects,
				allProperties.toArray(new String[allProperties.size()]));
		for (CollectionTO collTO : allPropertiesValues) {

			List<ValueTO> objVals = collTO.getPropValues();
			HashMap<String, Object> map = new HashMap<>();
			for (ValueTO objVal : objVals) {
				map.put(objVal.getPropertyName(), objVal.getValue());
			}
			if (filter.accept(map)) {
				objects.add(collTO.getObjId());
			}
		}
		return objects;

	}
	
	@Override
	public List<String> getDescendantObjectNames(final String objectType, Member parent) {
		
		Collection<TO<?>> objects = getDescendantTOs(objectType, parent);
		List<String> names = new LinkedList<>();

		if (objects.isEmpty()) {
			return names;
		}

		Collection<CollectionTO> allNames = environment.getPropertyValue(
				objects, new String[] { "name" });
		for (CollectionTO collTO : allNames) {
			names.add((String) collTO.getSinglePropValue("name").getValue());
		}
		return names;
		
	}
	
	@Override
	public List<String> getDescendantObjectNames(final String objectType, Member parent, Filter filter) {
		
		List<String> names = new LinkedList<>();
		Collection<TO<?>> objects = getDescendantTOs(objectType, parent);
		if (objects.isEmpty()) {
			return names;
		}

		List<String> allProperties = new LinkedList<>(filter.getProperties());
		allProperties.add("name");

		Collection<CollectionTO> allPropertiesValues = environment.getPropertyValue(objects,
				allProperties.toArray(new String[allProperties.size()]));
		for (CollectionTO collTO : allPropertiesValues) {

			List<ValueTO> objVals = collTO.getPropValues();
			HashMap<String, Object> map = new HashMap<>();
			for (ValueTO objVal : objVals) {
				map.put(objVal.getPropertyName(), objVal.getValue());
			}
			if (filter.accept(map)) {
				names.add((String) collTO.getSinglePropValue("name").getValue());
			}
		}
		return names;
		
	}
	
	@Override
	public TO<?> getObjectId(Member object) {
		
		if (object == null) {
			return null;
		}

		if (Cube.OBJECT.getMemberType().equals(String.class)) {
			if (object.getParent() == null) {
				Collection<TO<?>> objs = environment.getObjects(object.getLevel(), new ValueTO[] { new ValueTO("name",
						object.getKey()) });
				if (objs.isEmpty()) {
					return null;
				}
				return objs.iterator().next();
			}
			TO<?> parent = getObjectId(object.getParent());
	
			// cannot just get the descendant with a specific name, will have to
			// iterate over all of them
			Collection<TO<?>> allObjs = environment.getRelatedObjects(parent, object.getLevel(), true);
	
			if (allObjs.isEmpty()) {
				return null;
			}
	
			Collection<CollectionTO> allNames = environment.getPropertyValue(allObjs, new String[] { "name" });
			for (CollectionTO collTO : allNames) {
				if (object.getKey().equals(collTO.getSinglePropValue("name").getValue())) {
					return collTO.getObjId();
				}
			}
		} else if (Cube.OBJECT.getMemberType().equals(GenericObjectTO.class)) {
			return (TO<?>) object.getKey();
		}

		return null;
		
	}

	@Override
	public Collection<TO<?>> getRelatedObjects(TO<?> cellObject, final String propertyObjectType, boolean isChild) {
		
		if (cellObject == null) {
			return Collections.emptyList();
		}
		if (propertyObjectType == null) {
			throw new IllegalArgumentException("Property Object Type shouldn't be null");
		}
		
		return environment.getRelatedObjects(cellObject, propertyObjectType, isChild);
		
	}

	private Collection<TO<?>> getDescendantTOs(final String objectType, Member parent) {
		
		if (objectType == null) {
			throw new IllegalArgumentException("Object Type shouldn't be null");
		}
		if (parent == null) {
			return environment.getObjectsByType(objectType);
		}
		TO<?> parentId = getObjectId(parent);

		if (parentId == null) {
			return new LinkedList<>();
		}

		return environment.getRelatedObjects(parentId, objectType, true);
		
	}
}
