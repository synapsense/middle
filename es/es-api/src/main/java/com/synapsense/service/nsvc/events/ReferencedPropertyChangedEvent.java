package com.synapsense.service.nsvc.events;

import com.synapsense.dto.ChangedValueTO;
import com.synapsense.dto.TO;
import com.synapsense.service.nsvc.Event;
import com.synapsense.service.nsvc.EventDispatcher;

/**
 * Event class for notifications about changes in property referenced by property.
 * 
 * @author Aleksey Shabanov
 * 
 */
public class ReferencedPropertyChangedEvent implements Event {

    private static final long serialVersionUID = -5068336184155259297L;
    
	private TO<?> referencingObj;
	private TO<?> referencedObj;
	private String referencingProperty;
	private ChangedValueTO referencedVal;

	public ReferencedPropertyChangedEvent(TO<?> referencingObj, TO<?> referencedObj, String referencingProperty, ChangedValueTO referencedVal) {
		if (referencingObj == null) {
			throw new IllegalArgumentException("referencingObj must not be null");
		}
		this.referencingObj = referencingObj;
		
		if (referencedObj == null) {
			throw new IllegalArgumentException("referencedObj must not be null");
		}
		this.referencedObj = referencedObj;

		if (referencingProperty == null || referencingProperty.isEmpty()) {
			throw new IllegalArgumentException("referencingPropName must not be null or empty string");
		}
		this.referencingProperty = referencingProperty;
		
		this.referencedVal = referencedVal;
		
	}

	public TO<?> getReferencingObj() {
		return referencingObj;
	}

	public TO<?> getReferencedObj() {
		return referencedObj;
	}

	
	@Override
	public Event dispatch(EventDispatcher ed) {
		return ed.dispatch(this);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(super.toString()).append(": (").append(referencingObj).append(':').append(referencingProperty)
		        .append(")->(").append(referencedObj).append(':').append(referencedVal.getPropertyName());
		return sb.toString();
	}

	public String getReferencingPropName() {
	    return referencingProperty;
    }

	public String getReferencedPropName() {
	    return referencedVal.getPropertyName();
    }

	public ChangedValueTO getReferencedVal() {
    	return referencedVal;
    }

}
