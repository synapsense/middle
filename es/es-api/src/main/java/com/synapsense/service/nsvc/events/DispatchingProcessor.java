package com.synapsense.service.nsvc.events;

import java.io.Serializable;

import com.synapsense.service.nsvc.Event;
import com.synapsense.service.nsvc.EventDispatcher;
import com.synapsense.service.nsvc.EventProcessor;

public class DispatchingProcessor implements EventProcessor, EventDispatcher, Serializable {

	private static final long serialVersionUID = 3379779370449611943L;

	private EventProcessor ep;
	private ObjectTypeCreatedEventProcessor otcep;
	private ObjectTypeDeletedEventProcessor otdep;
	private ObjectTypeUpdatedEventProcessor otuep;
	private ObjectCreatedEventProcessor ocep;
	private ObjectDeletedEventProcessor odep;
	private PropertiesChangedEventProcessor pcep;
	private ReferencedObjectChangedEventProcessor rocep;
	private ReferencedPropertyChangedEventProcessor rpcep;
	private RelationRemovedEventProcessor rrep;
	private RelationSetEventProcessor rsep;
	@Deprecated
	private AlertRaisedEventProcessor arep;
	private ConfigurationStartedEventProcessor csep;
	private ConfigurationCompleteEventProcessor ccep;

	private boolean bypass;

	public DispatchingProcessor() {
		this(false);
	}

	public DispatchingProcessor(boolean bypass) {
		this.bypass = bypass;
	}

	public DispatchingProcessor putEp(EventProcessor p) {
		ep = p;
		return this;
	}

	public DispatchingProcessor putOtcep(ObjectTypeCreatedEventProcessor p) {
		otcep = p;
		return this;
	}

	public DispatchingProcessor putOtdep(ObjectTypeDeletedEventProcessor p) {
		otdep = p;
		return this;
	}

	public DispatchingProcessor putOtuep(ObjectTypeUpdatedEventProcessor p) {
		otuep = p;
		return this;
	}

	public DispatchingProcessor putOcep(ObjectCreatedEventProcessor p) {
		ocep = p;
		return this;
	}

	public DispatchingProcessor putOdep(ObjectDeletedEventProcessor p) {
		odep = p;
		return this;
	}

	public DispatchingProcessor putPcep(PropertiesChangedEventProcessor p) {
		pcep = p;
		return this;
	}

	public DispatchingProcessor putRocep(ReferencedObjectChangedEventProcessor p) {
		rocep = p;
		return this;
	}

	public DispatchingProcessor putRrep(RelationRemovedEventProcessor p) {
		rrep = p;
		return this;
	}

	public DispatchingProcessor putRsep(RelationSetEventProcessor p) {
		rsep = p;
		return this;
	}

	/**
	 * @deprecated Don't use this. To catch raised alerts use
	 *             ObjectCreatedEventProcessor with filtering by 'ALERT' type.
	 */
	@Deprecated
	public DispatchingProcessor putArep(AlertRaisedEventProcessor p) {
		arep = p;
		return this;
	}

	public DispatchingProcessor putCsep(ConfigurationStartedEventProcessor p) {
		csep = p;
		return this;
	}

	public DispatchingProcessor putCcep(ConfigurationCompleteEventProcessor p) {
		ccep = p;
		return this;
	}

	@Override
	public Event process(Event e) {
		return e.dispatch(this);
	}

	@Override
	public Event dispatch(Event e) {
		return ep != null ? ep.process(e) : (bypass ? e : null);
	}

	@Override
	public ObjectTypeCreatedEvent dispath(ObjectTypeCreatedEvent e) {
		return otcep != null ? otcep.process(e) : (bypass ? e : null);
	}

	@Override
	public ObjectTypeDeletedEvent dispath(ObjectTypeDeletedEvent e) {
		return otdep != null ? otdep.process(e) : (bypass ? e : null);
	}

	@Override
	public ObjectTypeUpdatedEvent dispath(ObjectTypeUpdatedEvent e) {
		return otuep != null ? otuep.process(e) : (bypass ? e : null);
	}

	@Override
	public ObjectCreatedEvent dispatch(ObjectCreatedEvent e) {
		return ocep != null ? ocep.process(e) : (bypass ? e : null);
	}

	@Override
	public ObjectDeletedEvent dispatch(ObjectDeletedEvent e) {
		return odep != null ? odep.process(e) : (bypass ? e : null);
	}

	@Override
	public PropertiesChangedEvent dispatch(PropertiesChangedEvent e) {
		return pcep != null ? pcep.process(e) : (bypass ? e : null);
	}

	@Override
	public ReferencedObjectChangedEvent dispatch(ReferencedObjectChangedEvent e) {
		return rocep != null ? rocep.process(e) : (bypass ? e : null);
	}

	@Override
	public ReferencedPropertyChangedEvent dispatch(ReferencedPropertyChangedEvent e) {
		return rpcep != null ? rpcep.process(e) : (bypass ? e : null);
	}

	@Override
	public RelationRemovedEvent dispatch(RelationRemovedEvent e) {
		return rrep != null ? rrep.process(e) : (bypass ? e : null);
	}

	@Override
	public RelationSetEvent dispatch(RelationSetEvent e) {
		return rsep != null ? rsep.process(e) : (bypass ? e : null);
	}

	/**
	 * @deprecated To catch raised alerts use ObjectCreatedEvent.
	 */
	@Deprecated
	@Override
	public AlertRaisedEvent dispatch(AlertRaisedEvent e) {
		return arep != null ? arep.process(e) : (bypass ? e : null);
	}

	@Override
	public ConfigurationStartedEvent dispatch(ConfigurationStartedEvent e) {
		return csep != null ? csep.process(e) : (bypass ? e : null);
	}

	@Override
	public ConfigurationCompleteEvent dispatch(ConfigurationCompleteEvent e) {
		return ccep != null ? ccep.process(e) : (bypass ? e : null);
	}

	public EventProcessor getEp() {
		return ep;
	}

	public void setEp(EventProcessor ep) {
		this.ep = ep;
	}

	public ObjectTypeCreatedEventProcessor getOtcep() {
		return otcep;
	}

	public void setOtcep(ObjectTypeCreatedEventProcessor otcep) {
		this.otcep = otcep;
	}

	public ObjectTypeUpdatedEventProcessor getOtuep() {
		return otuep;
	}

	public void setOtuep(ObjectTypeUpdatedEventProcessor otuep) {
		this.otuep = otuep;
	}

	public ObjectTypeDeletedEventProcessor getOtdep() {
		return otdep;
	}

	public void setOtdep(ObjectTypeDeletedEventProcessor otdep) {
		this.otdep = otdep;
	}

	public ObjectCreatedEventProcessor getOcep() {
		return ocep;
	}

	public void setOcep(ObjectCreatedEventProcessor ocep) {
		this.ocep = ocep;
	}

	public ObjectDeletedEventProcessor getOdep() {
		return odep;
	}

	public void setOdep(ObjectDeletedEventProcessor odep) {
		this.odep = odep;
	}

	public PropertiesChangedEventProcessor getPcep() {
		return pcep;
	}

	public void setPcep(PropertiesChangedEventProcessor pcep) {
		this.pcep = pcep;
	}

	public ReferencedObjectChangedEventProcessor getRocep() {
		return rocep;
	}

	public void setRocep(ReferencedObjectChangedEventProcessor rocep) {
		this.rocep = rocep;
	}

	public RelationRemovedEventProcessor getRrep() {
		return rrep;
	}

	public void setRrep(RelationRemovedEventProcessor rrep) {
		this.rrep = rrep;
	}

	public RelationSetEventProcessor getRsep() {
		return rsep;
	}

	public void setRsep(RelationSetEventProcessor rsep) {
		this.rsep = rsep;
	}

	public ConfigurationStartedEventProcessor getCsep() {
		return csep;
	}

	public ConfigurationCompleteEventProcessor getCcep() {
		return ccep;
	}

	public boolean isBypass() {
		return bypass;
	}

	public void setBypass(boolean bypass) {
		this.bypass = bypass;
	}

	public ReferencedPropertyChangedEventProcessor getRpcep() {
    	return rpcep;
    }

	public DispatchingProcessor putRpcep(ReferencedPropertyChangedEventProcessor rpcep) {
    	this.rpcep = rpcep;
    	return this;
    }

}
