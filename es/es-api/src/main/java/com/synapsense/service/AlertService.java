package com.synapsense.service;

import java.util.Collection;
import java.util.Date;

import com.synapsense.dto.Alert;
import com.synapsense.dto.AlertFilter;
import com.synapsense.dto.AlertPriority;
import com.synapsense.dto.AlertType;
import com.synapsense.dto.MessageTemplate;
import com.synapsense.dto.MessageType;
import com.synapsense.dto.TO;
import com.synapsense.exception.AlertServiceSystemException;
import com.synapsense.exception.AlertTypeAlreadyExistsException;
import com.synapsense.exception.IllegalInputParameterException;
import com.synapsense.exception.NoSuchAlertPriorityException;
import com.synapsense.exception.NoSuchAlertTypeException;

/**
 * Interface that provides API for working with alerts.
 * 
 * @author shabanov
 */
public interface AlertService {
	/**
	 * Allows caller to directly generate alert with specified alert data
	 * 
	 * @param alert
	 *            Alert instance
	 * @throws IllegalInputParameterException
	 *             if alert is null
	 */
	void raiseAlert(Alert alert);

	/**
	 * Allows caller to get alert by id.
	 * 
	 * @param alertId
	 *            Id of alert to get.
	 * 
	 */
	Alert getAlert(Integer alertId);

	/**
	 * Allows caller to get alerts filtered by given alert filter.<br>
	 * This method can be used instead of highly tailored methods like :<br>
	 * {@link #getActiveAlerts(TO, boolean)},<br>
	 * {@link #getActiveAlerts(Collection, boolean)},<br>
	 *
	 * @param filter
	 *            Alert filter which contains filter conditions
	 * 
	 * @return Collection of <code>Alert</code> objects satisfying to given
	 *         filter
	 */
	Collection<Alert> getAlerts(AlertFilter filter);

	/**
	 * Returns all active alerts which are associated with specified object with
	 * ID <code>objId</code>. The <code>TOFactory.EMPTY_TO</code> can be passed
	 * as <code>objId</code> argument's value<br>
	 * to get all active system alerts(alerts which are not related to any host
	 * object).
	 * 
	 * @param objId
	 *            object ID which can have active alerts
	 * @param propagate
	 *            flag for propagating behavior to the children of the specified
	 *            object
	 * @return Collection of <code>Alert</code> objects
	 * @throws IllegalInputParameterException
	 *             if objId is null
	 */
	Collection<Alert> getActiveAlerts(TO<?> objId, boolean propagate);

	/**
	 * Returns number of all active alerts which are associated with specified
	 * object with ID <code>objId</code>. The <code>TOFactory.EMPTY_TO</code>
	 * can be passed as <code>objId</code> argument's value<br>
	 * to get all active system alerts(alerts which are not related to any host
	 * object).
	 * 
	 * @param objId
	 *            object ID which can have active alerts
	 * @param propagate
	 *            flag for propagating behavior to the children of the specified
	 *            object
	 * @return Number of <code>Alert</code> objects
	 * @throws IllegalInputParameterException
	 *             if objId is null
	 */
	int getNumActiveAlerts(TO<?> objId, boolean propagate);

	/**
	 * Returns the number of active alerts matching the given alert filter. It
	 * is supposed to use for only active alerts. Filter's historical condition
	 * is prevented by throwing corresponding exception. Remember this method is
	 * not optimized as its counterpart {@link #getNumActiveAlerts(TO, boolean)}
	 * and could be run slower.
	 * 
	 * @param filter
	 * 
	 * @return Number of alerts
	 * 
	 * @throws IllegalInputParameterException
	 *             if filter has historical condition enabled
	 */
	int getNumAlerts(AlertFilter filter);
	
	/**
	 * Returns all active alerts which are associated with every object from
	 * <code>objIds</code>. This method is not fast if your <code>objIds</code>
	 * is large. The more appropriate way to get alerts is getting them from
	 * single object which has children. The amount of children doesn't decrease
	 * performance. The opposite case when you has got object and all its
	 * children and try to pass them to the method as <code>objIds</code> and
	 * <code>propagate</code> flag is true. It is the slowest way to get alerts.
	 * So , if all of your objects in <code>objIds</code> are children of one
	 * object the more effective way is to call
	 * {@link #getActiveAlerts(TO, boolean)} with this object and boolean flag
	 * in true.
	 * 
	 * @param objIds
	 *            object IDs which can have active alerts
	 * @param propagate
	 *            flag for propagating behavior to the children of the specified
	 *            objects
	 * @return Collection of <code>Alert</code> objects
	 * @throws IllegalInputParameterException
	 *             if objIds is null or empty
	 */
	Collection<Alert> getActiveAlerts(Collection<TO<?>> objIds, boolean propagate);

	/**
	 * Returns all dismissed alerts which are associated with object with ID
	 * <code>objId</code>. Dismissed means that somebody has reacted to it.<br>
	 * The <code>TOFactory.EMPTY_TO</code> can be passed as <code>objId</code>
	 * argument's value<br>
	 * to get all dismissed system alerts(alerts which are not related to any
	 * host object).
	 * 
	 * @param objId
	 *            object ID which can have dismissed alerts
	 * @param propagate
	 *            flag for propagating behavior to the children of the specified
	 *            object
	 * @return Collection of <code>Alert</code> objects
	 * @throws IllegalInputParameterException
	 *             if objId is null
	 */
	// Collection<Alert> getDismissedAlerts(TO<?> objId, boolean propagate);

	/**
	 * Returns all dismissed alerts which are associated with every object from
	 * <code>objIds</code>. Dismissed means that somebody has reacted to it.
	 * This method is not fast if your <code>objIds</code> is large. The more
	 * appropriate way to get alerts is getting them from single object which
	 * has children. The amount of children doesn't decrease performance. The
	 * opposite case when you has got object and all its children and try to
	 * pass them to the method as <code>objIds</code> and <code>propagate</code>
	 * flag is true. It is the slowest way to get alerts. So , if all of your
	 * objects in <code>objIds</code> are children of one object the more
	 * effective way is to call {@link #getDismissedAlerts(TO, boolean)} with
	 * this object and boolean flag in true.
	 * 
	 * @param objIds
	 *            object IDs which can have dismissed alerts
	 * @param propagate
	 *            flag for propagating behavior to the children of the specified
	 *            objects
	 * @return Collection of <code>Alert</code> objects
	 * @throws IllegalInputParameterException
	 *             if objIds is null or empty
	 */
	// Collection<Alert> getDismissedAlerts(Collection<TO<?>> objIds, boolean
	// propagate);

	/**
	 * Returns all existing alerts which are associated with object with ID
	 * <code>objIds</code><br>
	 * The <code>TOFactory.EMPTY_TO</code> can be passed as <code>objId</code>
	 * argument's value<br>
	 * to get all system alerts(alerts which are not related to any host
	 * object).
	 * 
	 * @param objId
	 *            object ID which can have associated alerts
	 * @param propagate
	 *            flag for propagating behavior to the children of the specified
	 *            object
	 * @return Collection of <code>Alert</code> objects
	 * @throws IllegalInputParameterException
	 *             if objId is null
	 */
	// Collection<Alert> getAllAlerts(TO<?> objId, boolean propagate);

	/**
	 * Returns all existing alerts which are associated with every object from
	 * <code>objIds</code> This method is not fast if your <code>objIds</code>
	 * is large. The more appropriate way to get alerts is getting them from
	 * single object which has children. The amount of children doesn't decrease
	 * performance. The opposite case when you has got object and all its
	 * children and try to pass them to the method as <code>objIds</code> and
	 * <code>propagate</code> flag is true. It is the slowest way to get alerts.
	 * So , if all of your objects in <code>objIds</code> are children of one
	 * object the more effective way is to call
	 * {@link #getAllAlerts(TO, boolean)} with this object and boolean flag in
	 * true.
	 * 
	 * @param objIds
	 *            object IDs which can have associated alerts
	 * @param propagate
	 *            flag for propagating behavior to the children of the specified
	 *            objects
	 * @return Collection of <code>Alert</code> objects
	 * @throws IllegalInputParameterException
	 *             if objIds is null or empty
	 */
	// Collection<Alert> getAllAlerts(Collection<TO<?>> objIds, boolean
	// propagate);

	/**
	 * Returns all alerts which are associated with object with ID
	 * <code>objIds</code> and which have generation time within the certain
	 * time frame.<br>
	 * The <code>TOFactory.EMPTY_TO</code> can be passed as <code>objId</code>
	 * argument's value<br>
	 * to get all system alerts from given time interval(alerts which are not
	 * related to any host object).
	 * 
	 * @param objId
	 *            object ID which have associated alerts
	 * @param start
	 *            interval start point
	 * @param end
	 *            interval end point
	 * @param propagate
	 *            flag for propagating behavior to the children of the specified
	 *            object
	 * @return Collection of <code>Alert</code> objects
	 * @throws IllegalInputParameterException
	 *             if objId is null or start is null or end is null or start >
	 *             end
	 */
	Collection<Alert> getAlertsHistory(TO<?> objId, Date start, Date end, boolean propagate);

	/**
	 * Returns all alerts which are associated with every object from
	 * <code>objIds</code> and which have generation time within the certain
	 * time frame. This method is not fast if your <code>objIds</code> is large.
	 * The more appropriate way to get alerts is getting them from single object
	 * which has children. The amount of children doesn't decrease performance.
	 * The opposite case when you has got object and all its children and try to
	 * pass them to the method as <code>objIds</code> and <code>propagate</code>
	 * flag is true. It is the slowest way to get alerts. So , if all of your
	 * objects in <code>objIds</code> are children of one object the more
	 * effective way is to call
	 * {@link #getAlertsHistory(TO, Date, Date, boolean)} with this object and
	 * boolean flag in true.
	 * 
	 * @param objIds
	 *            object IDs which have associated dismissed alerts
	 * @param start
	 *            interval start point
	 * @param end
	 *            interval end point
	 * @param propagate
	 *            flag for propagating behavior to the children of the specified
	 *            objects
	 * @return Collection of <code>Alert</code> objects
	 * @throws IllegalInputParameterException
	 *             if objIds is null or empty or start is null or end is null or
	 *             start > end
	 */
	// Collection<Alert> getAlertsHistory(Collection<TO<?>> objIds, Date start,
	// Date end, boolean propagate);

	/**
	 * Allows caller to retrieve a group of alerts by their ids.<br>
	 * Returns only existing active alerts. The result collection order is not
	 * supported.<br>
	 * If no alerts match the <code>alertIds</code> the empty collection will be
	 * returned.
	 * 
	 * @param alertIds
	 *            Arrays of alerts ids(not null,no null-valued elements,every id
	 *            >= 1)
	 * 
	 * @return Collection of found alerts or empty collection.
	 * 
	 * @throws IllegalInputParameterException
	 *             if some of the arguments don't match preconditions
	 */
	Collection<Alert> getAlerts(Integer[] alertIds);

	/**
	 * Returns all existing alert type descriptors.
	 * 
	 * @return Collection of <code>AlertType</code> objects
	 */
	Collection<AlertType> getAllAlertTypes();

	/**
	 * Returns alert types with specified names
	 * 
	 * @param typeNames
	 *            Array of alert type names
	 * @return Collection of <code>AlertType</code> objects
	 * @throws IllegalInputParameterException
	 *             if typeNames is null or empty
	 */
	Collection<AlertType> getAlertTypes(String[] typeNames);

	/**
	 * Returns alert type with specified names
	 * 
	 * @param typeName
	 *            Alert type name
	 * @return <code>AlertType</code> object
	 * @throws IllegalInputParameterException
	 *             if typeName is null or empty
	 */
	AlertType getAlertType(String typeName);

	/**
	 * Allows caller to dismiss an alert which satisfy to <code>alert</code>. It
	 * means that alert will be unavailable through
	 * {@link #getActiveAlerts(Collection, boolean)} and
	 * {@link #getActiveAlerts(TO, boolean)}<br>
	 * Saves given acknowledgement message to dismissed alert.<br>
	 * 
	 * NOTE that it is wrong just to create instance of <code>Alert</code> class<br>
	 * and pass it to this method to correctly dismiss it.<br>
	 * There is no guarantee that calling {@link #dismissAlert(Alert,String)}<br>
	 * with alert created by client as argument will dismiss the specified
	 * alert.<br>
	 * To dismiss alert the client should retrieve active alerts via<br>
	 * {@link #getActiveAlerts(Collection, boolean)},
	 * {@link #getActiveAlerts(TO, boolean)}<br>
	 * and then pass them to this method.Only in this case<br>
	 * you can be sure that the specifed alert will be dismissed.<br>
	 * 
	 * @param alert
	 *            Alert to dismiss
	 * @throws IllegalInputParameterException
	 *             if <code>alert</code> is null
	 * @throws AlertServiceSystemException
	 *             if operation has not been executed properly
	 * @see #dismissAlert(Alert, String) to dismiss without acknowlegment
	 */
	void dismissAlert(Alert alert, String comment);

	/**
	 * Allows caller to mark an alert with specified <code>alertId</code> as
	 * resolved. Alert becomes inactive after calling this method.
	 * 
	 * @param alertId
	 *            Id of alert to resolve
	 * @param comment
	 * 
	 */
	void resolveAlert(Integer alertId, String comment);

	/**
	 * Allows caller to mark an alert with specified <code>alertId</code> as
	 * acknowledged. This means that alert is still active but operator is aware
	 * that it exists.
	 * 
	 * @param alertId
	 *            Id of alert to acknowledge
	 * @param comment
	 * 
	 */
	void acknowledgeAlert(Integer alertId, String comment);

	/**
	 * Allows caller to dismiss an alert which has an id = <code>alertId</code>.<br>
	 * It means that alert will be unavailable through all methods which request
	 * active alerts.<br>
	 * Saves given acknowledgement message to dismissed alert.
	 * 
	 * @param alertId
	 *            Alert id to dismiss
	 * @param comment
	 * 
	 * @see #dismissAlert(Integer, String) to dismiss without acknowlegment
	 * 
	 */
	void dismissAlert(Integer alertId, String comment);

	/**
	 * Allows caller to dismiss all active alerts which are associated with the
	 * specified object.
	 * <p>
	 * It means that such alerts will be unavailable through
	 * {@link #getActiveAlerts(Collection, boolean)} and
	 * {@link #getActiveAlerts(TO, boolean)} The <code>TOFactory.EMPTY_TO</code>
	 * can be passed as <code>objId</code> argument's value<br>
	 * to dismiss all system alerts(alerts which are not related to any host
	 * object).<br>
	 * Saves given acknowledgment message to dismissed alerts.
	 * 
	 * @param objId
	 *            object ID which may have associated active alerts
	 * @param propagate
	 *            flag for propagating behavior to the children of the specified
	 *            object
	 * @param acknowledgement
	 *            Acknowledgment message
	 * @throws IllegalInputParameterException
	 *             When objIds is null or empty
	 * @throws AlertServiceSystemException
	 *             if operation has not been executed properly (at least one of
	 *             alert has not been dismissed)
	 * 
	 * @see #dismissAlerts(TO, boolean, String) to dismiss without acknowlegment
	 * 
	 */
	void dismissAlerts(TO<?> objId, boolean propagate, String acknowledgement);

	/**
	 * Allows caller to dismiss all active alerts which are associated with
	 * every object with ID from <code>objIds</code><br>
	 * It means that such alerts will be unavailable through
	 * {@link #getActiveAlerts(Collection, boolean)} and
	 * {@link #getActiveAlerts(TO, boolean)}.<br>
	 * Saves given acknowledgment message to dismissed alerts.
	 * 
	 * @param objIds
	 *            object IDs which have associated active alerts
	 * @param acknowledgement
	 *            Acknowledgment message
	 * @param propagate
	 *            flag for propagating behavior to the children of the specified
	 *            objects
	 * @throws IllegalInputParameterException
	 *             if objIds is null or empty
	 * 
	 * @see #dismissAlerts(Collection, boolean) to dismiss without acknowlegment
	 * 
	 */
	// void dismissAlerts(Collection<TO<?>> objIds, boolean propagate, String
	// acknowledgement);

	/**
	 * Creates new alert type.
	 * <p>
	 * All initial parameters must be delivered through <code>alertType</code>
	 * object.
	 * 
	 * @param alertType
	 *            initial configuration to create new alert type
	 * @throws AlertTypeAlreadyExistsException
	 *             if the specified alert type already exists.
	 * @throws IllegalInputParameterException
	 *             if alertType is null
	 */
	void createAlertType(AlertType alertType) throws AlertTypeAlreadyExistsException;

	/**
	 * Updates the specified alert type.
	 * <p>
	 * To change alert type is necessary to update <code>alertType</code> object
	 * properties
	 * 
	 * @param alertTypeName
	 *            Name of existing alert type
	 * @param alertType
	 *            new state for update existing alert type
	 * @throws NoSuchAlertTypeException
	 *             if the specified alert type does not exist.
	 * @throws IllegalInputParameterException
	 *             if alertTypeName is null or empty or alertType is null
	 */
	void updateAlertType(String alertTypeName, AlertType alertType) throws NoSuchAlertTypeException;

	/**
	 * Deletes the specified alert type.
	 * <p>
	 * All existing alerts of deleting alert type will be removed too. Only
	 * admin function cause it makes an unsafe deletion of active alerts which
	 * were not dismissed.
	 * 
	 * @param alertTypeName
	 *            Name of existing alert type
	 * @throws IllegalInputParameterException
	 *             if alertTypeName is null or empty
	 */
	void deleteAlertType(String alertTypeName);

	/**
	 * Removes all existing alerts regardless their type and status. Only admin
	 * function cause it makes an unsafe deletion of active alerts which were
	 * not dismissed.
	 * 
	 */
	void deleteAllAlerts();

	/**
	 * Removes all dismissed alerts on object with the specified ID. The
	 * <code>TOFactory.EMPTY_TO</code> can be passed as <code>objId</code>
	 * argument's value<br>
	 * to delete all system alerts(alerts which are not related to any host
	 * object).
	 * 
	 * @param objId
	 *            ID of object
	 * @param propagate
	 *            flag for propagating behavior to the children of the specified
	 *            object
	 * @throws IllegalInputParameterException
	 *             if objId is null
	 * 
	 * @deprecated since Jupiter
	 */
	@Deprecated
	void deleteAlerts(TO<?> objId, boolean propagate);

	/**
	 * Removes all dismissed alerts on objects with the specified IDs
	 * 
	 * @param propagate
	 *            flag for propagating behavior to the children of the specified
	 *            objects
	 * @throws IllegalInputParameterException
	 *             if objIds is null or empty
	 */
	// void deleteAlerts(Collection<TO<?>> objIds, boolean propagate);

	/**
	 * Removes all existing dismissed alerts.
	 */
	// void deleteAllAlertsHistory();

	/**
	 * Removes dismissed alerts which are older than <code>date</code>.
	 * 
	 * @param date
	 *            Date before which alerts should be deleted (not null)
	 * 
	 * @throws IllegalInputParameterException
	 *             if date is null
	 * @deprecated since Jupiter
	 */
	@Deprecated
	void deleteAlertsHistoryOlderThan(Date date);

	/**
	 * Allows caller to check existing of active alerts on the specified object.
	 * 
	 * @param objId
	 *            ID of object
	 * @param propagate
	 *            flag for propagating behavior to the children of the specified
	 *            objects
	 * @throws IllegalInputParameterException
	 *             if objId is null
	 */
	boolean hasAlerts(TO<?> objId, boolean propagate);

	/**
	 * Adds new destination to the configuration of the specified alert type's
	 * performer and tier
	 * 
	 * @param alertTypeName
	 * @param messageType
	 * @param tierName
	 * @param userName
	 * 
	 * @throws NoSuchAlertTypeException
	 *             if the specified alert type does not exist
	 * @throws IllegalInputParameterException
	 *             if <code>alertTypeName</code> or <code>performerName</code>
	 *             or <code>destination</code> or <code>tierName</code> are null
	 *             or empty
	 */
	void addActionDestination(String alertTypeName, String tierName, MessageType messageType, String userName)
	        throws NoSuchAlertTypeException;

	/**
	 * Removes the specified destination from the configuration of the specified
	 * alert type's performer and tier
	 * 
	 * @param alertTypeName
	 * @param messageType
	 * @param destination
	 * 
	 * @throws NoSuchAlertTypeException
	 *             if the specified alert type does not exist
	 * @throws IllegalInputParameterException
	 *             if <code>alertTypeName</code> or <code>performerName</code>
	 *             or <code>destination</code> or <code>tierName</code> are null
	 *             or empty
	 */
	void removeActionDestination(String alertTypeName, String tierName, MessageType messageType, String destination)
	        throws NoSuchAlertTypeException;

	/**
	 * Returns destination list related to the specified alert type's performer
	 * and tier.
	 * 
	 * @param alertTypeName
	 * @param tierName
	 * 
	 * @return Destinations list of the specified alert type , action , tier
	 * 
	 * @throws NoSuchAlertTypeException
	 *             if specified alert type does not exist
	 * @throws IllegalInputParameterException
	 *             if <code>alertTypeName</code> or <code>performerName</code>
	 *             or <code>tierName</code> are null or empty
	 */
	Collection<String> getActionDestinations(String alertTypeName, String tierName, MessageType messageType)
	        throws NoSuchAlertTypeException;

	/**
	 * Allows caller to get all configured alert priorities names. The returned
	 * names can be used in {@link #updateAlertPriority(String, AlertPriority)}
	 * method.
	 * 
	 * @return Collection of priorities names or empty collection if there are
	 *         no configured alert priorities
	 */
	Collection<String> getAlertPrioritiesNames();

	/**
	 * Allows caller to get alert priority data by name
	 * 
	 * @param name
	 *            Alert priority name
	 * 
	 * @return Found alert priority or null
	 * 
	 */
	AlertPriority getAlertPriority(String name);

	/**
	 * Allows caller to update alert priority settings of priority with
	 * specified <code>name</code>
	 * 
	 * @param name
	 *            Name of existing priority
	 * @param newPriority
	 *            New state to update
	 */
	void updateAlertPriority(String name, AlertPriority newPriority) throws NoSuchAlertPriorityException;

	void setTemplate(String alertType, MessageTemplate template) throws NoSuchAlertTypeException;

	void removeTemplate(String alertType, MessageType type) throws NoSuchAlertTypeException;

	/**
	 * Returns one of the user's destinations related to specified type of
	 * message.
	 * 
	 * Method returns only destination of user which was previously configured
	 * by calling
	 * {@link #addActionDestination(String, String, MessageType, String)}.
	 * 
	 * 
	 * @param alertTypeName
	 *            Name of alert type
	 * @param tierName
	 *            Certain tier name
	 * @param messageType
	 *            Message type to select destination
	 * @param userName
	 *            User name which was configured to recieve notifications about
	 *            alerts of alert type <code>alertTypeName</code> on tier
	 *            <code>tierName</code>
	 * 
	 * @return Destination as a string or null , if there is no destination
	 *         entry corresponding to given parameters. Or return empty string
	 *         if destination entry exists but user's email, sms addresses were
	 *         not set.
	 * @throws NoSuchAlertTypeException
	 *             if specified alert type does not exist
	 * @throws IllegalInputParameterException
	 *             if any argument is null or empty string
	 */
	String getActionDestination(final String alertTypeName, final String tierName, final MessageType messageType,
	        final String userName) throws NoSuchAlertTypeException;

	/**
	 * Returns every alert type having active <code>destination</code> in its
	 * destinations list Destination can be either plain address or user name.
	 * 
	 * @param destination
	 *            Destination as string
	 * @param messageType
	 *            One of the {@link MessageType} options
	 * @return Collection of alert types or empty collection
	 */
	Collection<AlertType> getAlertTypesByDestination(String destination, MessageType messageType);

}