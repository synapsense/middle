package com.synapsense.service.reporting.dto;

import java.io.Serializable;
import com.synapsense.service.reporting.dto.ReportTaskInfo;

public class RemoteCommandProxy implements Serializable  {

	private static final long serialVersionUID = -2127235125573050625L;
	
	private String commandName;
	
	private ReportTaskInfo reportTaskInfo;
	
	private int commandId;
	
	public RemoteCommandProxy(String commandName,
			ReportTaskInfo reportTaskInfo, int commandId) {
		this.commandName = commandName;
		this.reportTaskInfo = reportTaskInfo;
		this.commandId = commandId;
	}

	public String getCommandName() {
		return commandName;
	}

	public ReportTaskInfo getTaskInfo() {
		return reportTaskInfo;
	}
	
	public int getCommandId() {
		return commandId;
	}
}
