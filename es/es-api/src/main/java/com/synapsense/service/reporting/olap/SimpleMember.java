package com.synapsense.service.reporting.olap;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

import com.synapsense.service.reporting.olap.custom.BaseLevel;
import com.synapsense.service.reporting.olap.custom.Cube;
import com.synapsense.service.reporting.olap.custom.Level;
import com.synapsense.util.CollectionUtils;

public class SimpleMember implements Member, Serializable {

	private static final long serialVersionUID = 6988892541587875983L;

	private Hierarchy hierarchy;
	private String level;
	private Object key;
	private Member parent;
	
	public SimpleMember(Hierarchy hierarchy, String level, Object key) {
		this(hierarchy, level, key, null);
	}

	public SimpleMember(Member other) {
		this(other.getHierarchy(), other.getLevel(), other.getKey(), other.getParent());
	}

	public SimpleMember(String level, Object key) {
		this(null, level, key, null);
	}

	public SimpleMember(Hierarchy hierarchy, String level, Object key, Member parent) {
		if (hierarchy != null) {
			if (!key.getClass().equals(hierarchy.getMemberType())) {
				throw new IllegalArgumentException("Member type should be supported by hierarchy. Member type - "
				        + key.getClass() + " hierarchy supported type " + hierarchy.getMemberType());
			}
			this.hierarchy = hierarchy;
		}
		this.level = level;
		this.key = key;
		this.parent = parent;
	}

	public int getTupleSize() {
		return 1;
	}

	public Member itemByIndex(int index) {
		if (index < 0 || index >= getTupleSize()) {
			throw new IndexOutOfBoundsException("index:" + index + "size:" + getTupleSize());
		}
		return this;
	}

	@Override
	public Member itemByHierarchy(Hierarchy hierarchy) {
		return this.hierarchy.equals(hierarchy) ? this : null;
	}
	

	@Override
	public Member itemByDimension(String dimension) {		
		return Cube.getDimension(hierarchy.getName()).equals(dimension) ? this : null;
	}

	public Tuple join(Tuple tuple) {
		return new MemberTuple(this).join(tuple);
	}

	public List<Hierarchy> getDimensionalities() {
		return CollectionUtils.list(hierarchy);
	}

	public List<Tuple> getTuples() {
		LinkedList<Tuple> list = new LinkedList<Tuple>();
		list.add(this);
		return list;
	}

	public Hierarchy getHierarchy() {
		return hierarchy;
	}

	public String getLevel() {
		return level;
	}
	
	@Override
	public Level getCustomLevel() {
		return new BaseLevel(hierarchy, level);
	}

	public boolean isBrother(Member other) {
		return other.getHierarchy().equals(hierarchy) && other.getLevel().equals(level);
	}

	public Member getParent() {
		return parent;
	}

	public Object getKey() {
		return key;
	}

	@Override
	public Member merge(Member other) {

		if (other == null) {
			return new SimpleMember(this);
		}

		if (!hierarchy.isDescendant(level, other.getLevel())) {
			return other.merge(this);
		}

		if (level.equals(other.getLevel())) {
			if (!other.getKey().equals(key)) {
				return null;
			}
			return merge(other.getParent());
		}

		Member mergedParent = parent == null ? other : parent.merge(other);
		return mergedParent == null ? null : new SimpleMember(this).setParent(mergedParent);
	}

	public SimpleMember setParent(Member parent) {
		if (!hierarchy.isDescendant(level, parent.getLevel())) {
			throw new IllegalArgumentException("Cannot set parent from descendant level member=" + toString()
			        + " parent=" + parent);
		}
		this.parent = parent;
		return this;
	}

	public SimpleMember setHierarchy(Hierarchy hierarchy) {
		this.hierarchy = hierarchy;
		return this;
	}

	@Override
	public String toString() {
		String str = "[" + level + "].[&" + key + "]";
		return parent == null ? "[" + hierarchy.getName() + "]." + str : parent.toString() + "." + str;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((hierarchy == null) ? 0 : hierarchy.hashCode());
		result = prime * result + ((key == null) ? 0 : key.hashCode());
		result = prime * result + ((level == null) ? 0 : level.hashCode());
		result = prime * result + ((parent == null) ? 0 : parent.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SimpleMember other = (SimpleMember) obj;
		if (hierarchy == null) {
			if (other.hierarchy != null)
				return false;
		} else if (!hierarchy.equals(other.hierarchy))
			return false;
		if (key == null) {
			if (other.key != null)
				return false;
		} else if (!key.equals(other.key))
			return false;
		if (level == null) {
			if (other.level != null)
				return false;
		} else if (!level.equals(other.level))
			return false;
		if (parent == null) {
			if (other.parent != null)
				return false;
		} else if (!parent.equals(other.parent))
			return false;
		return true;
	}

	@Override
	public void buildQuery(PlanProcessor planProcessor) {
		planProcessor.processSet(this);
	}

	@Override
	public List<Member> getSimpleMembers() {
		return hierarchy.getSimpleMembers(this);
	}
	
}
