/**
 * 
 */
package com.synapsense.service.impl.messages;

import com.synapsense.service.impl.messages.base.StatisticData;

/**
 * @author Alexander Borisov
 * 
 */
public class StatMessage implements BusMessage<StatisticData, Long> {
	private static final long serialVersionUID = -7685636986726584418L;
	private long id;
	private StatisticData data;

	public StatMessage(long id, StatisticData data) {
		super();
		this.id = id;
		this.data = data;
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public StatisticData getValue() {
		return data;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public void setValue(StatisticData value) {
		data = value;
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer(20);
		sb.append("Id=");
		sb.append(id);
		sb.append("\n");

		sb.append("Id=");
		sb.append(id);
		sb.append("\n");

		return sb.toString();
	}

	@Override
	public Type getType() {
		return Type.STAT;
	}

}
