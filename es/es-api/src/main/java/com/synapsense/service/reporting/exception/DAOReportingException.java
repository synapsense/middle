package com.synapsense.service.reporting.exception;

import javax.ejb.ApplicationException;

import com.synapsense.service.reporting.management.ReportingException;

@ApplicationException(rollback = false)
public class DAOReportingException extends ReportingException {

	private static final long serialVersionUID = 5151216589208399411L;

	public DAOReportingException() {
	}

	public DAOReportingException(String message, Throwable cause) {
		super(message, cause);
	}

	public DAOReportingException(String message) {
		super(message);
	}

	public DAOReportingException(Throwable cause) {
		super(cause);
	}
}
