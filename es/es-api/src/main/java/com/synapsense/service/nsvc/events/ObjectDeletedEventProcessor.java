package com.synapsense.service.nsvc.events;

public interface ObjectDeletedEventProcessor {
	ObjectDeletedEvent process(ObjectDeletedEvent e);
}
