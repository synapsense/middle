package com.synapsense.impl;

import javax.management.ObjectName;

import org.jboss.mx.util.ObjectNameFactory;
import org.jboss.system.ServiceMBean;

/**
 * DataSourceWatcher management interface
 * 
 * @author Oleg Stepanov (stepanov@mera.ru)
 * 
 */
public interface DataSourceWatcherMBean extends ServiceMBean {

	/** The default object name */
	ObjectName OBJECT_NAME = ObjectNameFactory.create("synap:type=DataSourceWatcher,source=SynapDS");

	/**
	 * Returns a period between data source checks
	 * 
	 * @return period in milliseconds
	 */
	int getRetryPeriod();

	/**
	 * Sets a period between data source checks
	 * 
	 * @param period
	 *            period in milliseconds
	 */
	void setRetryPeriod(int period);

	/**
	 * Returns JNDI name of DataSource object to check
	 * 
	 * @return DataSource JNDI name
	 */
	String getDataSourceName();

	/**
	 * Sets JNDI name of DataSource object to check
	 * 
	 * @param name
	 *            DataSource JNDI name
	 */
	void setDataSourceName(String name);
}
