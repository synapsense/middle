package com.synapsense.impl;

import javax.management.MBeanServer;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;
import javax.naming.InitialContext;
import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.jboss.system.ServiceMBeanSupport;

/**
 * Simple MBean service for data source status checks.
 * 
 * Checks if data source is online by periodically requesting it with
 * "SELECT 1;" query. If request fails waits for a specified period of time
 * before issuing the next one.
 * 
 * @author Oleg Stepanov (stepanov@mera.ru)
 * 
 */
public class DataSourceWatcher extends ServiceMBeanSupport implements DataSourceWatcherMBean {

	private static final Logger log = Logger.getLogger(DataSourceWatcher.class);

	private String dsName = "java:/SynapDS";// default
	private int retryPeriod = 10000;// default

	@Override
	protected ObjectName getObjectName(MBeanServer server, ObjectName name) throws MalformedObjectNameException {
		return name == null ? OBJECT_NAME : name;
	}

	private boolean shuttingDown;

	@Override
	protected void startService() throws Exception {
		InitialContext ctx = new InitialContext();
		DataSource ds = (DataSource) ctx.lookup(dsName);

		Thread shutDownWatcher = new Thread() {
			@Override
			public void run() {
				shuttingDown = true;
			}
		};
		Runtime.getRuntime().addShutdownHook(shutDownWatcher);

		while (!isValid(ds)) {
			log.warn(dsName + " datasource is not online, waiting for " + retryPeriod + " msec");
			Thread.sleep(retryPeriod);
			if (shuttingDown) {
				return;
			}
		}
		log.info(dsName + " datasource is online, proceeding with startup");
		Runtime.getRuntime().removeShutdownHook(shutDownWatcher);
	}

	private static boolean isValid(DataSource ds) {
		try {
			ds.getConnection().prepareStatement("SELECT 1;").execute();
			return true;
		} catch (Throwable t) {
			return false;
		}
	}

	@Override
	public String getDataSourceName() {
		return dsName;
	}

	@Override
	public int getRetryPeriod() {
		return retryPeriod;
	}

	@Override
	public void setDataSourceName(String name) {
		dsName = name;
	}

	@Override
	public void setRetryPeriod(int period) {
		retryPeriod = period;
	}

}
