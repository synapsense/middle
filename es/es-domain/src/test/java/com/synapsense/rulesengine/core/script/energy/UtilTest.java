package com.synapsense.rulesengine.core.script.energy;

import java.util.Calendar;

import org.junit.Assert;
import org.junit.Test;

public class UtilTest {

	@Test
	public void checkTrueStartOfNewMonth() {
		long curTimestamp = System.currentTimeMillis();
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.DAY_OF_MONTH, 27);
		cal.set(Calendar.MONTH, cal.get(Calendar.MONTH) - 1);
		Assert.assertTrue(EnergyUtil.isNewMonth(cal.getTimeInMillis(), curTimestamp));
	}

	@Test
	public void checkFalseStartOfNewMonth() {
		long prevTimestamp = System.currentTimeMillis();
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.DATE, cal.getActualMaximum(Calendar.DATE));
		Assert.assertFalse(EnergyUtil.isNewMonth(prevTimestamp, cal.getTimeInMillis()));
	}

	@Test
	public void getBeginningOfThetMonth() {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.MONTH, 1);
		long begin = EnergyUtil.getMonthBeginning(cal.getTimeInMillis());
		cal.setTimeInMillis(begin);
		Assert.assertEquals(1, cal.get(Calendar.DAY_OF_MONTH));
		Assert.assertEquals(0, cal.get(Calendar.HOUR_OF_DAY));
		Assert.assertEquals(0, cal.get(Calendar.MINUTE));
		Assert.assertEquals(0, cal.get(Calendar.SECOND));
	}

	@Test
	public void getEndOfTheMonth() {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.MONTH, 0);
		long begin = EnergyUtil.getMonthEnd(cal.getTimeInMillis());
		cal.setTimeInMillis(begin);
		Assert.assertEquals(31, cal.get(Calendar.DAY_OF_MONTH));
		Assert.assertEquals(23, cal.get(Calendar.HOUR_OF_DAY));
		Assert.assertEquals(59, cal.get(Calendar.MINUTE));
		Assert.assertEquals(59, cal.get(Calendar.SECOND));
	}

	@Test
	public void getNumberOfWeekDaysInApril2011() {
		Assert.assertEquals(21, EnergyUtil.getNumberOfDays(2011, Calendar.APRIL, 62/* 0111110 */));
	}

	@Test
	public void getNumberOfWeekendsInFebruary2010() {
		Assert.assertEquals(8, EnergyUtil.getNumberOfDays(2010, Calendar.FEBRUARY, 65/* 1000001 */));
	}

	@Test
	public void getNumberOfAllInAugust2009() {
		Assert.assertEquals(31, EnergyUtil.getNumberOfDays(2009, Calendar.AUGUST, 127));
	}

	@Test
	public void getNumberOfDaysInFebruary2011() {
		Assert.assertEquals(28, EnergyUtil.getNumberOfDays(2011, Calendar.FEBRUARY));
	}

	@Test
	public void getNumberOfDaysIn2011() {
		Assert.assertEquals(365, EnergyUtil.getNumberOfDays(2011));
	}

	@Test
	public void getNumberOfDaysIn2008() {
		Assert.assertEquals(366, EnergyUtil.getNumberOfDays(2008));
	}

	@Test
	public void getMonthTest() {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.DAY_OF_MONTH, 27);
		cal.set(Calendar.MONTH, Calendar.APRIL);
		Assert.assertEquals(Calendar.APRIL, EnergyUtil.getMonth(cal.getTimeInMillis()));
	}
}
