package com.synapsense.rulesengine.core.script.energy;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ TierCostCalculatorTest.class, TimeOfUseCalculatorTest.class, EmissionEstimatorTest.class,
        UtilTest.class })
public class EnergyTestSuite {
}
