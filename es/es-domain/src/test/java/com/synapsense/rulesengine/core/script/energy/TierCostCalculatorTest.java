package com.synapsense.rulesengine.core.script.energy;

import java.util.Calendar;

import org.junit.Assert;
import org.junit.Test;

import com.synapsense.rulesengine.core.script.energy.TieredCostCalculator.Tier;

public class TierCostCalculatorTest {

	@Test
	public void calculateCostWhenKwhBiggerThanTiersDefined() {
		TieredCostCalculator calc = new TieredCostCalculator();
		calc.addTier(100, 0.7);
		calc.addTier(200, 0.8);

		double result = calc.calculateRt(100, 0, 200, 0);

		Assert.assertEquals(100 * 0.7 + 200 * 0.8, result, 0.001);
	}

	@Test
	public void calculateCostWhenKwhLessThanTiersDefined() {
		TieredCostCalculator calc = new TieredCostCalculator();
		calc.addTier(100, 0.7);
		calc.addTier(200, 0.8);
		calc.addTier(400, 0.9);
		calc.addTier(600, 1.1);

		double result = calc.calculateRt(400, 0, 100, 0);

		Assert.assertEquals(100 * 0.7 + 100 * 0.8 + 200 * 0.9 + 100 * 1.1, result, 0.001);
	}

	@Test
	public void addTiersNotSortedAndCalculate() {
		TieredCostCalculator calc = new TieredCostCalculator();
		calc.addTier(1050, 0.7);
		calc.addTier(150, 0.8);
		calc.addTier(20, 0.9);
		calc.addTier(500, 1.1);

		double result = calc.calculateRt(1950, 0, 50, 0);

		Assert.assertEquals(20 * 0.9 + 130 * 0.8 + 350 * 1.1 + 1500 * 0.7, result, 0.001);
	}

	@Test
	public void calculateZeroEnergy() {
		TieredCostCalculator calc = new TieredCostCalculator();
		calc.addTier(1050, 0.7);

		Assert.assertEquals(0, calc.calculateRt(0, 0, 0, 0), 0.001);
	}

	@Test(expected = IllegalArgumentException.class)
	public void addZeroKwhTier() {
		new TieredCostCalculator().addTier(0, 0.7);
	}

	@Test(expected = IllegalArgumentException.class)
	public void addNegativeKwhTier() {
		new TieredCostCalculator().addTier(-1, 0.7);
	}

	@Test(expected = IllegalArgumentException.class)
	public void addNegativeCostTier() {
		new TieredCostCalculator().addTier(-1, 0.7);
	}

	@Test(expected = IllegalArgumentException.class)
	public void addDuplicateKwhTier() {
		TieredCostCalculator calc = new TieredCostCalculator();
		calc.addTier(100, 0.7);
		calc.addTier(100, 0.9);
	}

	@Test(expected = IllegalStateException.class)
	public void calculateWithNoTiers() {
		new TieredCostCalculator().calculateRt(0, 0, 0, 0);
	}

	@Test
	public void loadTiersFromString() {
		TieredCostCalculator calc = TieredCostCalculator.load("200,0.5;400,0.7");
		Tier[] tiers = new Tier[] { new Tier(200, 0.5), new Tier(400, 0.7) };
		Assert.assertArrayEquals(tiers, calc.getTiers().toArray(new Tier[0]));
	}

	@Test
	public void load1TierFromString() {
		TieredCostCalculator calc = TieredCostCalculator.load("202,2.9;");
		Tier[] tiers = new Tier[] { new Tier(202, 2.9) };
		Assert.assertArrayEquals(tiers, calc.getTiers().toArray(new Tier[0]));
	}

	@Test
	public void loadSemicolon() {
		TieredCostCalculator calc = TieredCostCalculator.load(";");
		Assert.assertArrayEquals(new Tier[] {}, calc.getTiers().toArray(new Tier[0]));
	}


	@Test(expected = ParseCostTableException.class)
	public void loadTiersEmpty() {
		TieredCostCalculator.load("");
	}

	@Test(expected = ParseCostTableException.class)
	public void loadTiersWithNotANumber() {
		TieredCostCalculator.load("202,2ads;");
	}

	@Test(expected = ParseCostTableException.class)
	public void loadNotCorrectFormat() {
		TieredCostCalculator.load("202;12, 0.8");
	}

	@Test
	public void calculateCorrectAnnualCost() {
		TieredCostCalculator calc = TieredCostCalculator.load("200,0.5;400,0.7;600,1.0");
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.YEAR, 2011);
		int numberOfDays = cal.getActualMaximum(Calendar.DAY_OF_YEAR);
		Assert.assertEquals(24 * numberOfDays * (0.5 * 200 + 200 * 0.7 + 600), calc.estimateAnnual(2011, 1000), 0.0001);
	}

	@Test
	public void calculateCorrectMonthlyCost31() {
		TieredCostCalculator calc = TieredCostCalculator.load("200,0.5;400,0.7;600,1.0;800,1.5");
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.MONTH, Calendar.MAY);
		Assert.assertEquals(24 * 31 * (0.5 * 200 + 200 * 0.7 + 200 + 200 * 1.5),
		        calc.estimateMonthly(2011, Calendar.MAY, 800), 0.0001);
	}
	
	@Test
	public void calculateCorrectMonthlyCost30() {
		TieredCostCalculator calc = TieredCostCalculator.load("200,0.5;400,0.7;600,1.0;800,1.5");
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.MONTH, Calendar.APRIL);
		Assert.assertEquals(24 * 30 * (0.5 * 200 + 200 * 0.7 + 200 + 200 * 1.5),
		        calc.estimateMonthly(2011, Calendar.APRIL, 800), 0.0001);
	}
}
