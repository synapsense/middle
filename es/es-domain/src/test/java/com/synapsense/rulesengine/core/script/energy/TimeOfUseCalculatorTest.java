package com.synapsense.rulesengine.core.script.energy;

import java.util.Calendar;

import org.junit.Assert;
import org.junit.Test;

public class TimeOfUseCalculatorTest {

	@Test
	public void getCostWhenInBoundTime() {
		TouCostPeriod tou = new TouCostPeriod(Calendar.JANUARY, Calendar.DECEMBER, TouCostPeriod.ALL);
		tou.addTime(7, 0, 11, 0, 0.02).addTime(11, 0, 7, 0, 0.5);
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.HOUR_OF_DAY, 8);
		Assert.assertEquals(0.02, tou.getCost(cal.getTimeInMillis()), 0.0);
	}

	@Test
	public void getCostWhenOutBoundTime() {
		TouCostPeriod tou = new TouCostPeriod(Calendar.JANUARY, Calendar.DECEMBER, TouCostPeriod.ALL);
		tou.addTime(7, 0, 11, 0, 0.02).addTime(11, 0, 7, 0, 0.5);
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.HOUR_OF_DAY, 6);
		Assert.assertEquals(0.5, tou.getCost(cal.getTimeInMillis()), 0.0);
	}

	@Test
	public void touPeriodStartMonthGreaterThanEndMonth() {
		TouCostPeriod period = new TouCostPeriod(Calendar.NOVEMBER, Calendar.MARCH, TouCostPeriod.ALL);

		Assert.assertTrue(period.inPeriod(Calendar.JANUARY));
		Assert.assertFalse(period.inPeriod(Calendar.JUNE));

		Calendar cal = Calendar.getInstance();

		cal.set(Calendar.MONTH, Calendar.JANUARY);
		Assert.assertTrue(period.inPeriod(cal.getTimeInMillis()));

		cal.set(Calendar.MONTH, Calendar.JUNE);
		Assert.assertFalse(period.inPeriod(cal.getTimeInMillis()));
	}

	@Test
	public void getTestOnHourEdge() {
		TouCostPeriod tou = new TouCostPeriod(Calendar.JANUARY, Calendar.DECEMBER, TouCostPeriod.ALL);
		tou.addTime(1, 30, 6, 0, 0.02).addTime(13, 0, 1, 30, 1.5).addTime(12, 30, 13, 0, 1.0)
		        .addTime(6, 0, 12, 30, 0.5);
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.HOUR_OF_DAY, 13);
		cal.set(Calendar.MINUTE, 0);
		Assert.assertEquals("Cost", 1.5, tou.getCost(cal.getTimeInMillis()), 0.0);
	}

	@Test
	public void getTestOnMinuteEdge() {
		TouCostPeriod tou = new TouCostPeriod(Calendar.JANUARY, Calendar.DECEMBER, TouCostPeriod.ALL);
		tou.addTime(1, 30, 6, 0, 0.02).addTime(13, 0, 1, 30, 1.5).addTime(12, 30, 13, 0, 1.0)
		        .addTime(6, 0, 12, 30, 0.5);
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.HOUR_OF_DAY, 12);
		cal.set(Calendar.MINUTE, 30);
		Assert.assertEquals("Cost", 1.0, tou.getCost(cal.getTimeInMillis()), 0.0);
	}

	@Test(expected = IllegalArgumentException.class)
	public void throwExceptionWhenNotACoveredTimeFrame() {
		TouCostPeriod tou = new TouCostPeriod(Calendar.JANUARY, Calendar.DECEMBER, TouCostPeriod.ALL);
		tou.addTime(1, 30, 6, 0, 0.02).addTime(13, 0, 1, 30, 1.5).addTime(6, 0, 12, 30, 0.5);
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.HOUR_OF_DAY, 12);
		cal.set(Calendar.MINUTE, 30);
		tou.getCost(cal.getTimeInMillis());
	}

	@Test
	public void checkDayInPeriodOfWeekDays() {
		TouCostPeriod tou = new TouCostPeriod(Calendar.MARCH, Calendar.JUNE, TouCostPeriod.WEEKDAYS);
		Calendar cal = Calendar.getInstance();
		// [BC 05/2015] Use a fixed date. The old code broke at end of month.
		cal.set(2015, Calendar.APRIL, 13); // MONDAY
		for (int i = 0; i < 5; i++) {
			Assert.assertTrue(cal.getTime().toString(), tou.inPeriod(cal.getTimeInMillis()));
			cal.add(Calendar.DATE, 1);
		}
		for (int i = 0; i < 2; i++) {
			Assert.assertFalse(cal.getTime().toString(), tou.inPeriod(cal.getTimeInMillis()));
			cal.add(Calendar.DATE, 1);
		}
	}

	@Test
	public void checkDayInPeriodOfWeekEnds() {
		TouCostPeriod tou = new TouCostPeriod(Calendar.JANUARY, Calendar.DECEMBER, TouCostPeriod.WEEKENDS);
		Calendar cal = Calendar.getInstance();
		// [BC 05/2015] Use a fixed date. The old code broke at end of month.
		cal.set(2015, Calendar.JANUARY, 12); // MONDAY
		for (int i = 0; i < 5; i++) {
			Assert.assertFalse(cal.getTime().toString(), tou.inPeriod(cal.getTimeInMillis()));
			cal.add(Calendar.DATE, 1);
		}
		for (int i = 0; i < 2; i++) {
			Assert.assertTrue(cal.getTime().toString(), tou.inPeriod(cal.getTimeInMillis()));
			cal.add(Calendar.DATE, 1);
		}
	}

	@Test
	public void checkDayInPeriodOfAllDays() {
		TouCostPeriod tou = new TouCostPeriod(Calendar.SEPTEMBER, Calendar.DECEMBER, TouCostPeriod.ALL);
		Calendar cal = Calendar.getInstance();
		// [BC 05/2015] Use a fixed date. The old code broke at end of month.
		cal.set(2015, Calendar.DECEMBER, 14); // MONDAY
		for (int i = 0; i < 7; i++) {
			Assert.assertTrue(cal.getTime().toString(), tou.inPeriod(cal.getTimeInMillis()));
			cal.add(Calendar.DATE, 1);
		}
	}

	@Test
	public void checkNotInPeriod() {
		TouCostPeriod tou = new TouCostPeriod(Calendar.SEPTEMBER, Calendar.DECEMBER, TouCostPeriod.ALL);
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.MONTH, Calendar.JANUARY);
		Assert.assertFalse(tou.inPeriod(cal.getTimeInMillis()));
	}

	@Test(expected = IllegalArgumentException.class)
	public void setIllegalStartMonth() {
		new TouCostPeriod(13, Calendar.DECEMBER, TouCostPeriod.ALL);
	}

	@Test(expected = IllegalArgumentException.class)
	public void setIllegalEndMonth() {
		new TouCostPeriod(Calendar.FEBRUARY, -1, TouCostPeriod.ALL);
	}

	@Test(expected = IllegalArgumentException.class)
	public void setIllegalDayType() {
		new TouCostPeriod(Calendar.FEBRUARY, Calendar.APRIL, 0);
	}

	@Test
	public void calculateSummaryEnergyCost() {
		TimeOfUseCostCalculator calc = new TimeOfUseCostCalculator();
		calc.addCostPeriod(new TouCostPeriod(Calendar.JANUARY, Calendar.APRIL, TouCostPeriod.ALL)
		        .addTime(1, 30, 6, 0, 0.02).addTime(13, 0, 1, 30, 1.5).addTime(12, 30, 13, 0, 1.0)
		        .addTime(6, 0, 12, 30, 0.5));
		calc.addCostPeriod(new TouCostPeriod(Calendar.MAY, Calendar.DECEMBER, TouCostPeriod.WEEKDAYS)
		        .addTime(1, 30, 6, 0, 0.02).addTime(6, 0, 13, 00, 0.5).addTime(13, 0, 1, 30, 1.5));
		calc.addCostPeriod(new TouCostPeriod(Calendar.MAY, Calendar.DECEMBER, TouCostPeriod.WEEKENDS).addTime(1, 30,
		        13, 30, 0.02).addTime(13, 30, 1, 30, 0.5));

		final Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.MONTH, Calendar.JULY);
		calendar.set(Calendar.DAY_OF_WEEK, Calendar.SATURDAY);
		calendar.set(Calendar.HOUR_OF_DAY, 14);

		double cost = calc.calculateRt(0, 100, 10, calendar.getTimeInMillis());

		Assert.assertEquals(105.0, cost, 0.0);
	}

	@Test(expected = IllegalArgumentException.class)
	public void calculateCostForNonExistingPeriod() {
		TimeOfUseCostCalculator calc = new TimeOfUseCostCalculator();
		calc.addCostPeriod(new TouCostPeriod(Calendar.JANUARY, Calendar.APRIL, TouCostPeriod.ALL)
		        .addTime(1, 30, 6, 0, 0.02).addTime(13, 0, 1, 30, 1.5).addTime(12, 30, 13, 0, 1.0)
		        .addTime(6, 0, 12, 30, 0.5));
		calc.addCostPeriod(new TouCostPeriod(Calendar.MAY, Calendar.DECEMBER, TouCostPeriod.WEEKDAYS)
		        .addTime(1, 30, 6, 0, 0.02).addTime(6, 0, 13, 00, 0.5).addTime(13, 0, 1, 30, 1.5));
		final Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.MONTH, Calendar.JULY);
		calendar.set(Calendar.DAY_OF_WEEK, Calendar.SATURDAY);
		calendar.set(Calendar.HOUR_OF_DAY, 14);

		calc.calculateRt(0, 100, 10, calendar.getTimeInMillis());
	}

	@Test
	public void loadParametersFromString() {
		TimeOfUseCostCalculator calc = TimeOfUseCostCalculator
		        .load("[{beginmonth:'jan',endmonth:'dec',typeofday:'weekdays',"
		                + "timeline:[{begin:'07:00',end:'11:00',period:'mid_peak',cost:'0.5'},"
		                + "{begin:'11:00',end:'17:00',period:'on_peak',cost:'0.6'},"
		                + "{begin:'17:00',end:'22:00',period:'mid_peak',cost:'0.7'},"
		                + "{begin:'22:00',end:'07:00',period:'off_peak',cost:'1.0'}]}, "
		                + "{beginmonth:'jan',endmonth:'dec',typeofday:'weekends',"
		                + "timeline:[{begin:'07:00',end:'11:00',period:'mid_peak',cost:'0.01'},"
		                + "{begin:'11:00',end:'17:00',period:'on_peak',cost:'0.01'},"
		                + "{begin:'17:00',end:'22:00',period:'mid_peak',cost:'0.01'},"
		                + "{begin:'22:00',end:'07:00',period:'off_peak',cost:'0.1'}]}]");

		TouCostPeriod[] periods = new TouCostPeriod[] {
		        new TouCostPeriod(0, 11, TouCostPeriod.WEEKDAYS).addTime(7, 0, 11, 0, 0.5).addTime(11, 0, 17, 0, 0.6)
		                .addTime(17, 0, 22, 0, 0.7).addTime(22, 0, 7, 0, 1.0),
		        new TouCostPeriod(0, 11, TouCostPeriod.WEEKENDS).addTime(7, 0, 11, 0, 0.01).addTime(11, 0, 17, 0, 0.01)
		                .addTime(17, 0, 22, 0, 0.01).addTime(22, 0, 7, 0, 0.1) };

		Assert.assertArrayEquals(periods, calc.getPeriods().toArray(new TouCostPeriod[0]));
	}

	@Test
	public void loadParametersFromStringAll() {
		TimeOfUseCostCalculator calc = TimeOfUseCostCalculator
		        .load("[{beginmonth:'jan',endmonth:'jun',typeofday:'all',"
		                + "timeline:[{begin:'07:00',end:'11:00',period:'mid_peak',cost:'0.5'},"
		                + "{begin:'11:00',end:'17:00',period:'on_peak',cost:'0.6'},"
		                + "{begin:'17:00',end:'22:00',period:'mid_peak',cost:'0.7'},"
		                + "{begin:'22:00',end:'07:00',period:'off_peak',cost:'1.0'}]}, "
		                + "{beginmonth:'jul',endmonth:'dec',typeofday:'all',"
		                + "timeline:[{begin:'07:00',end:'11:00',period:'mid_peak',cost:'0.01'},"
		                + "{begin:'11:00',end:'17:00',period:'on_peak',cost:'0.01'},"
		                + "{begin:'17:00',end:'22:00',period:'mid_peak',cost:'0.01'},"
		                + "{begin:'22:00',end:'07:00',period:'off_peak',cost:'0.1'}]}]");

		TouCostPeriod[] periods = new TouCostPeriod[] {
		        new TouCostPeriod(0, 5, TouCostPeriod.ALL).addTime(7, 0, 11, 0, 0.5).addTime(11, 0, 17, 0, 0.6)
		                .addTime(17, 0, 22, 0, 0.7).addTime(22, 0, 7, 0, 1.0),
		        new TouCostPeriod(6, 11, TouCostPeriod.ALL).addTime(7, 0, 11, 0, 0.01).addTime(11, 0, 17, 0, 0.01)
		                .addTime(17, 0, 22, 0, 0.01).addTime(22, 0, 7, 0, 0.1) };

		Assert.assertArrayEquals(periods, calc.getPeriods().toArray(new TouCostPeriod[0]));
	}

	@Test(expected = ParseCostTableException.class)
	public void loadParametersFromStringIncorrectFormat() {
		TimeOfUseCostCalculator.load("[{beginmonth:'jan',endmonth:'dec',typeofday:'weekdays',"
		        + "timeline:[{begin:'07:00',end:'11:00',period:'mid_peak',cost:'0.5'},"
		        + "{begin:'11:00',end:'17:00',period:'on_peak',cost:'dfdf'}");
	}

	@Test(expected = ParseCostTableException.class)
	public void loadParametersFromStringEmpty() {
		TimeOfUseCostCalculator.load("");
	}

	@Test(expected = ParseCostTableException.class)
	public void loadParametersFromStringIncorrectTypeOfDay() {
		TimeOfUseCostCalculator.load("beginmonth:'jan',endmonth:'dec',typeofday:'weeks',"
		        + "timeline:begin:'07:00',end:'11:00',period:'mid_peak',cost:'0.5'");
	}

	@Test(expected = ParseCostTableException.class)
	public void loadParametersFromStringIncorrectMonth() {
		TimeOfUseCostCalculator.load("beginmonth:'ja',endmonth:'dec',typeofday:'weeks',"
		        + "timeline:begin:'07:00',end:'11:00',period:'mid_peak',cost:'0.5'");
	}

	@Test(expected = ParseCostTableException.class)
	public void loadParametersFromStringTokenWithoutEnd() {
		TimeOfUseCostCalculator.load("beginmonth:'jan',endmonth");
	}

	@Test(expected = ParseCostTableException.class)
	public void loadParametersFromStringTimeWithoutPeriod() {
		TimeOfUseCostCalculator.load("cost:'0.5'");
	}

	@Test(expected = ParseCostTableException.class)
	public void loadParametersFromStringIncorrectTime() {
		TimeOfUseCostCalculator.load("beginmonth:'jan',endmonth:'dec',typeofday:'weekends',"
		        + "timeline:begin:'07:00',end:'dd:00',period:'mid_peak',cost:'0.5'");
	}

	@Test
	public void calculateCorrectAnnual() {
		TimeOfUseCostCalculator calc = TimeOfUseCostCalculator
		        .load("[{beginmonth:'jan',endmonth:'jun',typeofday:'all',"
		                + "timeline:[{begin:'07:00',end:'11:00',period:'mid_peak',cost:'0.5'},"
		                + "{begin:'11:00',end:'17:00',period:'on_peak',cost:'0.6'},"
		                + "{begin:'17:00',end:'22:00',period:'mid_peak',cost:'0.7'},"
		                + "{begin:'22:00',end:'07:00',period:'off_peak',cost:'1.0'}]}, "
		                + "{beginmonth:'jul',endmonth:'dec',typeofday:'all',"
		                + "timeline:[{begin:'07:00',end:'11:00',period:'mid_peak',cost:'0.01'},"
		                + "{begin:'11:00',end:'17:00',period:'on_peak',cost:'0.01'},"
		                + "{begin:'17:00',end:'22:00',period:'mid_peak',cost:'0.01'},"
		                + "{begin:'22:00',end:'07:00',period:'off_peak',cost:'0.1'}]}]");
		// To determine how many days there is in february

		Assert.assertEquals(1000 * ((31 + 28 + 31 + 30 + 31 + 30) * (4 * 0.5 + 6 * 0.6 + 5 * 0.7 + 9 * 1.0) + (31 + 31
		        + 30 + 31 + 30 + 31)
		        * (4 * 0.01 + 6 * 0.01 + 5 * 0.01 + 9 * 0.1)), calc.estimateAnnual(2011, 1000), 0.0001);
	}

	@Test
	public void calculateCorrectAnnualWithDifferentDayTypes() {
		TimeOfUseCostCalculator calc = new TimeOfUseCostCalculator().addCostPeriod(
		        new TouCostPeriod(0, 2, 62).addTime(01, 30, 14, 30, 0.1).addTime(15, 30, 17, 00, 0.2)).addCostPeriod(
		        new TouCostPeriod(10, 11, 65).addTime(17, 30, 01, 00, 0.5));
		Assert.assertEquals(1000 * ((21 + 20 + 23) * (13 * 0.1 + 1.5 * 0.2) + (8 + 9) * 7.5 * 0.5),
		        calc.estimateAnnual(2011, 1000), 0.000001);
	}

	@Test
	public void calculateCorrectMonth31() {
		TimeOfUseCostCalculator calc = new TimeOfUseCostCalculator().addCostPeriod(
		        new TouCostPeriod(0, 2, 62).addTime(01, 30, 14, 30, 0.1).addTime(15, 30, 17, 00, 0.2)).addCostPeriod(
		        new TouCostPeriod(0, 2, 65).addTime(17, 30, 01, 00, 0.5));
		Assert.assertEquals(1000 * 21 * (13 * 0.1 + 1.5 * 0.2) + 1000 * 10 * 7.5 * 0.5,
		        calc.estimateMonthly(2011, Calendar.JANUARY, 1000), 0.000001);
	}

	@Test
	public void correctDefaultValueParsing() throws Exception {
		String defaultVal = "[{\"timeline\":[{\"period\":\"mid_peak\",\"cost\":\"0.00\",\"end\":\"11:00\"," +
				"\"begin\":\"07:00\"},{\"period\":\"on_peak\",\"cost\":\"0.00\",\"end\":\"17:00\",\"begin\":\"11:00\"}," +
				"{\"period\":\"mid_peak\",\"cost\":\"0.00\",\"end\":\"22:00\",\"begin\":\"17:00\"},{\"period\"" +
				":\"off_peak\",\"cost\":\"0.00\",\"end\":\"07:00\",\"begin\":\"22:00\"}],\"typeofday\":\"all\",\"endmonth\"" +
				":\"aug\",\"beginmonth\":\"jun\"},{\"timeline\":[{\"period\":\"mid_peak\",\"cost\":\"0.00\",\"end\"" +
				":\"11:00\",\"begin\":\"07:00\"},{\"period\":\"on_peak\",\"cost\":\"0.00\",\"end\":\"17:00\",\"begin\":" +
				"\"11:00\"},{\"period\":\"mid_peak\",\"cost\":\"0.00\",\"end\":\"22:00\",\"begin\":\"17:00\"},{\"period\":" +
				"\"off_peak\",\"cost\":\"0.00\",\"end\":\"07:00\",\"begin\":\"22:00\"}],\"typeofday\":\"all\",\"endmonth\":" +
				"\"nov\",\"beginmonth\":\"sep\"},{\"timeline\":[{\"period\":\"mid_peak\",\"cost\":\"0.00\",\"end\":\"11:00\"" +
				",\"begin\":\"07:00\"},{\"period\":\"on_peak\",\"cost\":\"0.00\",\"end\":\"17:00\",\"begin\":\"11:00\"},{\"period\"" +
				":\"mid_peak\",\"cost\":\"0.00\",\"end\":\"22:00\",\"begin\":\"17:00\"},{\"period\":\"off_peak\",\"cost\":\"0.00\"" +
				",\"end\":\"07:00\",\"begin\":\"22:00\"}],\"typeofday\":\"all\",\"endmonth\":\"may\",\"beginmonth\":\"apr\"},{\"timeline\"" +
				":[{\"period\":\"mid_peak\",\"cost\":\"0.00\",\"end\":\"11:00\",\"begin\":\"07:00\"},{\"period\"" +
				":\"on_peak\",\"cost\":\"0.00\",\"end\":\"17:00\",\"begin\":\"11:00\"},{\"period\":\"mid_peak\",\"cost\":" +
				"\"0.00\",\"end\":\"22:00\",\"begin\":\"17:00\"},{\"period\":\"off_peak\",\"cost\":\"0.00\",\"end\":" +
				"\"07:00\",\"begin\":\"22:00\"}],\"typeofday\":\"all\",\"endmonth\":\"mar\",\"beginmonth\":\"dec\"}]";

		TimeOfUseCostCalculator calc = TimeOfUseCostCalculator.load(defaultVal);
		calc.calculateRt(0, 0, 0, 1306257900801L);
	}
}
