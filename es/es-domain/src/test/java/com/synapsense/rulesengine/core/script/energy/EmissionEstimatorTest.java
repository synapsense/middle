package com.synapsense.rulesengine.core.script.energy;

import java.util.Calendar;

import org.junit.Assert;
import org.junit.Test;

public class EmissionEstimatorTest {

	@Test
	public void calculateMonthEstimation31() {
		Estimator est = new EmissionEstimator(0.5);
		Assert.assertEquals(31 * 24 * 0.5, est.estimateMonthly(2011, Calendar.MAY, 1000), 0.001);
	}

	@Test
	public void calculateMonthEstimation28() {
		Estimator est = new EmissionEstimator(0.5);
		Assert.assertEquals(28 * 24 * 0.5, est.estimateMonthly(2011, Calendar.FEBRUARY, 1000), 0.001);
	}

	@Test
	public void calculateYearEstimation2011() {
		Estimator est = new EmissionEstimator(0.5);
		Assert.assertEquals(365 * 24 * 0.5, est.estimateAnnual(2011, 1000), 0.001);
	}

	@Test
	public void calculateYearEstimation2008() {
		Estimator est = new EmissionEstimator(0.5);
		Assert.assertEquals(366 * 24 * 0.5 * 10, est.estimateAnnual(2008, 10000), 0.001);
	}
}
