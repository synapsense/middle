package com.synapsense.rulesengine.core.script.energy;

import java.util.Calendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.dto.TO;
import com.synapsense.exception.EnvException;
import com.synapsense.service.Environment;

/**
 * A few utilities and constants used for energy calculations
 *
 * @author Dmitry Grudzinskiy
 *
 */
public final class EnergyUtil {

	public static final String DC = "DC";

	public static final String COST_TYPE = "costType";
	public static final String TIERED_TABLE = "tiersTable";
	public static final String TOU_TABLE = "timeOfUseTable";

	public static final String ENERGY_USAGE = "energyUsage";
	public static final String ENERGY_COST = "energyCost";
	public static final String EMISSION = "emission";

	public static final String EMISSION_FACTOR = "emissionFactor";
	public static final String IT_POWER = "itPower";
	public static final String INF_POWER = "infrastructurePower";

	private static Log log = LogFactory.getLog(EnergyUtil.class);

	/**
	 * Returns month for given timestamp
	 * 
	 * @param timestamp
	 *            to get month for
	 * @return month for given timestamp
	 */
	public static int getMonth(long timestamp) {
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(timestamp);
		return cal.get(Calendar.MONTH);
	}

	/**
	 * Indicates whether first and second timestamps have different months
	 * 
	 * @param prevTimeStamp
	 *            first timestamp
	 * @param curTimeStamp
	 *            second timestamp
	 * @return true if second timestamp month isn't equal to first timestamp
	 *         month
	 */
	public static boolean isNewMonth(long prevTimeStamp, long curTimeStamp) {
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(prevTimeStamp);
		int prevMonth = cal.get(Calendar.MONTH);
		cal.setTimeInMillis(curTimeStamp);
		return cal.get(Calendar.MONTH) != prevMonth;
	}

	/**
	 * For a given timestamp returns another timestamp with the same year and
	 * month having the first day of the month and time equal to 00:00:00
	 * 
	 * @param timestamp
	 *            base to get a beginnnig for
	 * @return timestamp with the same year and month having the first day of
	 *         the month and time equal to 00:00:00
	 */
	public static long getMonthBeginning(long timestamp) {
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(timestamp);
		cal.set(Calendar.DATE, cal.getActualMinimum(Calendar.DATE));
		cal.set(Calendar.HOUR_OF_DAY, cal.getActualMinimum(Calendar.HOUR_OF_DAY));
		cal.set(Calendar.MINUTE, cal.getActualMinimum(Calendar.MINUTE));
		cal.set(Calendar.SECOND, cal.getActualMinimum(Calendar.SECOND));
		return cal.getTimeInMillis();
	}

	/**
	 * For a given timestamp returns another timestamp with the same year and
	 * month having the last day of the month and time equal to 23:59:59
	 * 
	 * @param timestamp
	 *            base to get an end for
	 * @return timestamp with the same year and month having the last day of the
	 *         month and time equal to 23:59:59
	 */
	public static long getMonthEnd(long timestamp) {
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(timestamp);
		cal.set(Calendar.DATE, cal.getActualMaximum(Calendar.DATE));
		cal.set(Calendar.HOUR_OF_DAY, cal.getActualMaximum(Calendar.HOUR_OF_DAY));
		cal.set(Calendar.MINUTE, cal.getActualMaximum(Calendar.MINUTE));
		cal.set(Calendar.SECOND, cal.getActualMaximum(Calendar.SECOND));
		return cal.getTimeInMillis();
	}

	/**
	 * Returns number of days of specific type in a specified year and month
	 * 
	 * @param year
	 * @param month
	 * @param typeOfDay
	 *            a bitmask that indicates necessary days of week to count, e.g.
	 *            0111110 indicates week days
	 * @return number of days of specific type in a specified year and month
	 */
	public static int getNumberOfDays(int year, int month, int typeOfDay) {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.YEAR, year);
		cal.set(Calendar.MONTH, month);
		cal.set(Calendar.DAY_OF_MONTH, cal.getActualMinimum(Calendar.DAY_OF_MONTH));
		int nbDays = 0;
		do {
			int day = cal.get(Calendar.DAY_OF_WEEK);
			if (((typeOfDay >> (day - 1)) & 1) == 1) {
				nbDays++;
			}
			cal.add(Calendar.DAY_OF_YEAR, 1);
		} while (cal.get(Calendar.MONTH) == month);
		return nbDays;
	}

	
	/**
	 * Returns number of days in a specified year and month.
	 * Similar to <code>getNumberOfDays(year, month, 127)</code>.
	 * 
	 * @param year
	 * @param month
	 * @return number of days in a specified year and month
	 */
	public static int getNumberOfDays(int year, int month) {
		return getNumberOfDays(year, month, 127);
	}

	/**
	 * Returns number of days in a specified year.
	 * 
	 * @param year
	 * @return number of days in a specified year
	 */
	public static int getNumberOfDays(int year) {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.YEAR, year);
		return cal.getActualMaximum(Calendar.DAY_OF_YEAR);
	}

	/**
	 * Returns aggregated values of specified properties of a single object
	 * 
	 * @param env
	 *            environment interface for retreiving properties values
	 * @param obj
	 *            object id
	 * @param props
	 *            list of properties
	 * @return aggregated values of specified properties of a single object or
	 *         'null' if one of the values is 'null'
	 * 
	 * @throws EnvException
	 */
	public static Double getSumPropertiesValues(Environment env, TO<?> obj, String... props) throws EnvException {
		Double sum = 0D;
		for (String prop : props) {
			Double val = env.getPropertyValue(obj, prop, Double.class);
			if (val == null) {
				return null;
			}
			sum += val;
		}
		return sum;
	}

	public static CostCalculator getCostCalculator(TO<?> pue, Environment env) throws EnvException {
		TO<?> dc = env.getParents(pue, DC).iterator().next();
		Integer type = env.getPropertyValue(dc, COST_TYPE, Integer.class);

		if (type != 0 && type != 1) {
			throw new IllegalArgumentException("Unknown cost type " + type);
		}

		String table = type == 0 ? env.getPropertyValue(dc, TIERED_TABLE, String.class) : env.getPropertyValue(dc,
		        TOU_TABLE, String.class);

		if (table == null || table.isEmpty()) {
			log.debug("Cannot get cost calculator. Cost table is null or empty");
			return null;
		}

		return type == 0 ? TieredCostCalculator.load(table) : TimeOfUseCostCalculator.load(table);
	}

	private EnergyUtil() {
	}
}
