package com.synapsense.rulesengine.core.script.power.model;

import static com.synapsense.rulesengine.core.script.power.model.PowerConstants.AVG_CURRENT;
import static com.synapsense.rulesengine.core.script.power.model.PowerConstants.DEMAND_POWER;
import static com.synapsense.rulesengine.core.script.power.model.PowerConstants.MAX_CURRENT;
import static com.synapsense.rulesengine.core.script.power.model.PowerConstants.MAX_HIST_CURRENT;
import static com.synapsense.rulesengine.core.script.power.model.PowerConstants.POTENTIAL_MAX_AP_POWER;
import static com.synapsense.rulesengine.core.script.power.model.PowerConstants.POTENTIAL_MAX_CURRENT;

import java.util.ArrayList;
import java.util.List;

import com.synapsense.dto.CollectionTO;
import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;
import com.synapsense.service.Environment;

public abstract class LinePowerObject extends PowerObject {

	protected Double avgCurrent;
	protected Double maxCurrent;
	protected Double maxHistCurrent;
	protected Double potentialMaxCurrent;

	public LinePowerObject(TO<?> objId, long resetTime) {
		super(objId, resetTime);
	}

	public LinePowerObject(CollectionTO objProps, long resetTime) {
		super(objProps, resetTime);
	}

	public LinePowerObject(CollectionTO objProps) {
		super(objProps);
	}

	public LinePowerObject(TO<?> objId) {
		super(objId);
	}
	
	protected void addAvgCurrent(Double avgCurrent) {
		this.avgCurrent = Util.addValue(this.avgCurrent, avgCurrent);
	}

	protected void addMaxCurrent(Double maxCurrent) {
		this.maxCurrent = Util.addValue(this.maxCurrent, maxCurrent);
	}

	protected void addPotentialMaxCurrent(Double potentialMaxCurrent) {
		this.potentialMaxCurrent = Util.addValue(this.potentialMaxCurrent, potentialMaxCurrent);
	}

	protected void processPower(Double voltage) {
		if (isValid(voltage)) {
			if (isValid(avgCurrent)) {
				apPower = voltage * avgCurrent / 1000;
			}
			if (isValid(maxCurrent)) {
				maxApPower = voltage * maxCurrent / 1000;
			}
		}
	}
	
	protected void processHistoricalMaxCurrent() {
		if (isValid(maxCurrent)) {
			if (maxHistCurrent == null || maxCurrent > maxHistCurrent || getResetTime() > 0) {
				maxHistCurrent = maxCurrent;
			}
		}
	}

	@Override
	protected void assignValues(CollectionTO objProps) {
		super.assignValues(objProps);
		avgCurrent = Util.getDoubleValue(objProps, AVG_CURRENT);
		maxCurrent = Util.getDoubleValue(objProps, MAX_CURRENT);
		maxHistCurrent = Util.getDoubleValue(objProps, MAX_HIST_CURRENT);
	}

	@Override
	protected List<ValueTO> createDefaultValueList() {
		List<ValueTO> values = super.createDefaultValueList();
		addValueTO(values, MAX_HIST_CURRENT, maxHistCurrent);
		return values;
	}

	@Override
	protected List<ValueTO> createCalculatedValuesList() {
		List<ValueTO> values = createDefaultValueList();
		addValueTO(values, AVG_CURRENT, avgCurrent);
		addValueTO(values, MAX_CURRENT, maxCurrent);
		addValueTO(values, DEMAND_POWER, demandPower);
		return values;
	}

	@Override
	protected void saveAll(Environment env) throws Exception {
		List<ValueTO> values = createCalculatedValuesList();
		addValueTO(values, POTENTIAL_MAX_CURRENT, potentialMaxCurrent);
		addValueTO(values, POTENTIAL_MAX_AP_POWER, potentialMaxApPower);
		saveValueList(values, env);
	}
	
	@Override
	protected void saveNulls(Environment env) throws Exception {
		super.saveNulls(env);
		List<ValueTO> values = new ArrayList<ValueTO>();
		addValueTO(values, MAX_HIST_CURRENT, null);
		addValueTO(values, AVG_CURRENT, null);
		addValueTO(values, POTENTIAL_MAX_CURRENT, null);
		addValueTO(values, MAX_CURRENT, null);
		saveValueList(values, env);
	}
}
