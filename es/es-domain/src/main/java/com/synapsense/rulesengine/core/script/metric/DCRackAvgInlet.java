package com.synapsense.rulesengine.core.script.metric;

import java.util.Collection;

import org.apache.log4j.Logger;
import com.synapsense.dto.TO;
import com.synapsense.rulesengine.core.environment.Property;
import com.synapsense.rulesengine.core.environment.RuleAction;
import com.synapsense.rulesengine.core.environment.RuleI;
import com.synapsense.service.Environment;

public class DCRackAvgInlet implements RuleAction {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private final static Logger logger = Logger.getLogger(DCRackAvgInlet.class);

	public Object run(final RuleI triggeredRule, final Property calculated) {
        if (logger.isDebugEnabled())
            logger.debug(triggeredRule.getName() + " is run");
		try {
			Environment env = calculated.getEnvironment();
			TO<?> nativeTO = calculated.getDataSource().getHostObjectTO();

			int RacksNumber = 0;
			Double sum_discharge = 0.0;

			Collection<TO<?>> ROOMs = env.getRelatedObjects(nativeTO, "ROOM", true);
			if (ROOMs == null || ROOMs.isEmpty()) {
				throw new IllegalStateException("DC '" + calculated.getDataSource().getNativePropertyName()
				        + "' has no children ROOMs.");
			} else {
				// iterate over ROOMs within the DC
				for (TO<?> ROOM : ROOMs) {
					// Extract Rack count
					Double ric = env.getPropertyValue(ROOM, "ric", Double.class);
					// Extract avg Rack discharge
					Double ard = env.getPropertyValue(ROOM, "avgRD", Double.class);

					if ((ric != null) && (ard != null)) {
						RacksNumber += ric;
						sum_discharge += (ard * ric);
					}
				}
			}

			if (RacksNumber != 0) {
				Double dcArd = sum_discharge / RacksNumber;
				return dcArd;
			} else {
				return null;
			}
		} catch (Exception e) {
			logger.error("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName() + "': error", e);
			return null;
		}
	}
}