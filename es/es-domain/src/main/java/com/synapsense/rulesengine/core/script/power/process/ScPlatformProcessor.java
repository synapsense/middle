package com.synapsense.rulesengine.core.script.power.process;

import java.util.Collection;
import java.util.Date;
import java.util.LinkedList;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.dto.Alert;
import com.synapsense.dto.TO;
import com.synapsense.exception.EnvException;
import com.synapsense.rulesengine.core.script.power.factory.AbstractPOFactory;
import com.synapsense.rulesengine.core.script.power.model.Phase;
import com.synapsense.rulesengine.core.script.power.model.Plug;
import com.synapsense.rulesengine.core.script.power.model.PowerConstants;
import com.synapsense.rulesengine.core.script.power.model.PowerObject;
import com.synapsense.rulesengine.core.script.power.model.PowerRack;
import com.synapsense.rulesengine.core.script.power.model.Rpdu;
import com.synapsense.rulesengine.core.script.power.model.Util;
import com.synapsense.service.AlertService;
import com.synapsense.service.Environment;
import com.synapsense.util.CRC8Converter;

/**
 * Performs object hierarchy processing in case the rack is instrumented by
 * jawas.
 * 
 * @see NoPlugStructureProcessor
 * 
 * @author Dmitry Grudzinskiy
 * 
 */
public class ScPlatformProcessor implements PowerPlatformProcessor {

	private static Log log = LogFactory.getLog(ScPlatformProcessor.class);

	private static final String QUORUM_ALERT_NAME = "Node not reporting";

	private AbstractPOFactory poFactory;

	private Environment env;

	private AlertService alertService;

	public ScPlatformProcessor(AbstractPOFactory poFactory, Environment env, AlertService alertService) {
		super();
		this.poFactory = poFactory;
		this.env = env;
		this.alertService = alertService;
		this.poFactory.setEnv(env);
	}

	@Override
	public void process(PowerRack rack) throws Exception {
		if (log.isDebugEnabled()) {
			log.debug(rack + " cre rule: checking jawas statuses");
		}

		if (!rackStatusOK(rack)) {
			log.warn(rack + " cre rule: number of reporting jawas doesn't meet quorum, calculations won't be performed");
			return;
		}

		if (log.isDebugEnabled()) {
			log.debug(rack + " cre rule: processing RPDUs, phases, servers and plugs...");
		}

		// Walk down to RPDU and then PHASE levels.
		// If Rack is NFI set 'null' for each computational parameter
		Map<TO<?>, PowerObject> serverMap = poFactory.createServers(rack);
		Collection<Rpdu> rpdus = poFactory.createRpdus(rack);
		for (Rpdu rpdu : rpdus) {
			if (log.isTraceEnabled()) {
				log.trace(rack + " cre rule: loaded rpdu class " + rpdu.getClass().getSimpleName() + " for " + rpdu);
			}
			// Now we can process phases
			Collection<Phase> phases = poFactory.createPhases(rpdu);
			for (Phase phase : phases) {
				if (log.isTraceEnabled()) {
					log.trace(rack + " cre rule: loaded phase class " + phase.getClass().getSimpleName() + " for "
					        + phase);
				}
				Collection<Plug> plugs = poFactory.createPlugs(phase, serverMap);
				for (Plug plug : plugs) {
					if (log.isTraceEnabled()) {
						log.trace(rack + " cre rule: loaded plug class " + plug.getClass().getSimpleName() + " for "
						        + plug);
					}
					plug.process(env);
				}
			}

			// We need all phases to be summed before we can process them
			for (Phase phase : phases) {
				phase.process(env);
			}
			rpdu.process(env);
		}

		// Process servers
		for (PowerObject server : serverMap.values()) {
			if (log.isTraceEnabled()) {
				log.trace(rack + " cre rule: loaded server class " + server.getClass().getSimpleName() + " for "
				        + server);
			}
			server.process(env);
		}
		rack.process(env);
	}

	public void setEnv(Environment env) {
		this.env = env;
		poFactory.setEnv(env);
	}

	public void setAlertService(AlertService alertService) {
		this.alertService = alertService;
	}

	private boolean rackStatusOK(PowerRack rack) throws EnvException {
		Collection<TO<?>> plugs = env.getRelatedObjects(rack.getObjId(), PowerConstants.PLUG, true);
		long currentTime = System.currentTimeMillis();
		LinkedList<TO<?>> obsoletePlugs = new LinkedList<TO<?>>();
		for (TO<?> plug : plugs) {
			long timestamp = env.getPropertyTimestamp(plug, PowerConstants.AVG_CURRENT);
			if (currentTime > (timestamp + 2 * PowerConstants.SAMPLE_INTERVAL)) {
				obsoletePlugs.add(plug);
			}
		}
		// Current rack status
		int currentStatus = env.getPropertyValue(rack.getObjId(), PowerConstants.STATUS, Integer.class);
		if (!obsoletePlugs.isEmpty()) {
			if (new Double(obsoletePlugs.size()) / new Double(plugs.size()) * 100D > (100 - rack.getQuorum())) {
				// Reporting jawas below quorum
				alertService.raiseAlert(new Alert("Rack P3 SmartPlugs are not reporting", QUORUM_ALERT_NAME,
				        new Date(), "Multiple P3 SmartPlugs are not reporting. Unable to perform power calculations.",
				        rack.getObjId()));
				// If it is not already not reporting update the status
				Util.updateRackStatus(rack, currentStatus, PowerConstants.STATUS_NOT_REPORTING, log, env);
				return false;
			}
			alertService.raiseAlert(new Alert("Rack P3 SmartPlugs are not reporting", QUORUM_ALERT_NAME, new Date(),
			        "The following P3 SmartPlugs are not reporting: " + buildPlugList(obsoletePlugs)
			                + ". Please check configuration.", rack.getObjId()));
		}
		// In case power rack is in 'not reporting' status we need to update it
		Util.updateRackStatus(rack, currentStatus, PowerConstants.STATUS_OK, log, env);
		return true;
	}

	private String buildPlugList(Collection<TO<?>> plugs) throws EnvException {
		StringBuilder builder = new StringBuilder();
		int i = 0;
		for (TO<?> plug : plugs) {
			String mac = CRC8Converter.codeToHr(env.getPropertyValue(plug, PowerConstants.PLUG_ID, Long.class));
			TO<?> server = env.getParents(plug, PowerConstants.SERVER).iterator().next();
			int ulocation = env.getPropertyValue(server, "uLocation", Integer.class);
			String name = env.getPropertyValue(server, "name", String.class);
			builder.append(mac).append(" (server ").append(name).append(", location ").append(ulocation).append(")");
			if (++i < plugs.size()) {
				builder.append("; ");
			}
		}
		return builder.toString();
	}
}
