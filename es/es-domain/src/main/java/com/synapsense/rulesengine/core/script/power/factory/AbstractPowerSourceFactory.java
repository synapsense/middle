package com.synapsense.rulesengine.core.script.power.factory;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import com.synapsense.dto.CollectionTO;
import com.synapsense.dto.TO;
import com.synapsense.exception.EnvException;
import com.synapsense.rulesengine.core.script.power.model.Phase;
import com.synapsense.rulesengine.core.script.power.model.PowerConstants;
import com.synapsense.rulesengine.core.script.power.model.PowerRack;
import com.synapsense.rulesengine.core.script.power.model.Rpdu;
import com.synapsense.service.Environment;

public abstract class AbstractPowerSourceFactory implements PowerSourceFactory {

	protected Environment env;

	@Override
	public Collection<Rpdu> createRpdus(final PowerRack parent) throws Exception {
		HashMap<String, Object> filters = new HashMap<String, Object>();
		filters.put(PowerConstants.RPDU_STATUS, Integer.valueOf(1));
		return new CollCreator<Rpdu>(parent.getObjId(), PowerConstants.RPDU, filters, PowerConstants.RPDU_PROPS)
		        .createCollection(new ElCreator<Rpdu>() {
			        public Rpdu create(CollectionTO collTo) {
				        return createRpdu(collTo, parent);
			        }
		        });
	}

	@Override
	public Collection<Phase> createPhases(final Rpdu parent) throws Exception {
		return new CollCreator<Phase>(parent.getObjId(), PowerConstants.PHASE, getPhaseProperties())
		        .createCollection(new ElCreator<Phase>() {
			        public Phase create(CollectionTO collTo) {
				        return createPhase(collTo, parent);
			        }
		        });
	}

	public void setEnv(Environment env) {
		this.env = env;
	}

	protected class CollCreator<T> {

		private Collection<CollectionTO> values;

		public CollCreator(TO<?> objId, String typeName, String[] props) throws EnvException {
			values = env.getPropertyValue(env.getChildren(objId, typeName), props);
		}

		public CollCreator(TO<?> objId, String typeName, Map<String, Object> filters, String[] props)
		        throws EnvException {
			Collection<TO<?>> children = env.getChildren(objId, typeName);
			Collection<CollectionTO> filterCollTos = env.getPropertyValue(children,
			        filters.keySet().toArray(new String[filters.size()]));

			for (CollectionTO filterCollTo : filterCollTos) {
				for (Map.Entry<String, Object> filter : filters.entrySet()) {
					if (!filter.getValue().equals(filterCollTo.getSinglePropValue(filter.getKey()).getValue())) {
						// Don't expect too many objects here, so delete in a
						// slow way
						children.remove(filterCollTo.getObjId());
						break;
					}
				}
			}

			values = env.getPropertyValue(children, props);
		}

		public Collection<T> createCollection(ElCreator<T> elCreator) throws Exception {
			LinkedList<T> list = new LinkedList<T>();
			for (CollectionTO collTo : values) {
				list.add(elCreator.create(collTo));
			}
			return list;
		}

		public Map<TO<?>, T> createMap(ElCreator<T> elCreator) throws Exception {
			HashMap<TO<?>, T> map = new HashMap<TO<?>, T>();
			for (CollectionTO collTo : values) {
				map.put(collTo.getObjId(), elCreator.create(collTo));
			}
			return map;
		}
	}

	public static interface ElCreator<T> {
		T create(CollectionTO collTo) throws Exception;
	}

	protected abstract Rpdu createRpdu(CollectionTO collTo, PowerRack parent);

	protected abstract Phase createPhase(CollectionTO collTo, Rpdu parent);

	protected abstract String[] getPhaseProperties();
}
