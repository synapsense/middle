package com.synapsense.rulesengine.core.script.energy;

import static com.synapsense.rulesengine.core.script.energy.EnergyUtil.INF_POWER;
import static com.synapsense.rulesengine.core.script.energy.EnergyUtil.IT_POWER;
import static com.synapsense.rulesengine.core.script.energy.EnergyUtil.getSumPropertiesValues;

import java.util.Calendar;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.EnvException;
import com.synapsense.service.Environment;

/**
 * Implements the common algorithm for reductions calculation
 * 
 * @author Dmitry Grudzinskiy
 *
 */
public class ReductionCalculator {

	private static Log log = LogFactory.getLog(ReductionCalculator.class);

	private static ReductionCalculator instance = new ReductionCalculator();

	public static ReductionCalculator getInstance() {
		return instance;
	}

	public Double calculateReduction(Double pueTarget, Double itLoadTarget, Double lastValue, Estimator estimator,
	        String propertyName, TO<?> pue, Environment env) throws EnvException {
		Double targetPower = pueTarget * itLoadTarget;
		if (targetPower == 0) {
			// Not set
			return null;
		}

		if (lastValue == null) {
			return null;
		}
		long lastTimeStamp = env.getPropertyTimestamp(pue, propertyName);

		// Get last power measurement
		Double energy = getSumPropertiesValues(env, pue, IT_POWER, INF_POWER);
		if (energy == null) {
			return null;
		}

		// To estimate annual value we need:
		// 1. For current month estimate using trivial averaging
		// 2. For calculated months use calculated values
		// 3. For the rest estimate values using last power measurement
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.MONTH, -10);
		double estimated = 0.0;
		for (int i = 1; i <= 11; i++) {
			Date date = new Date(EnergyUtil.getMonthBeginning(cal.getTimeInMillis()));
			ValueTO[] values = env.getHistory(pue, propertyName, date, 2, ValueTO.class).toArray(new ValueTO[0]);
			double monthVal = 0.0;
			if (values.length == 2 && EnergyUtil.getMonth(values[0].getTimeStamp()) == cal.get(Calendar.MONTH)
			        && EnergyUtil.getMonth(values[1].getTimeStamp()) == cal.get(Calendar.MONTH)
			        && values[1].getValue().equals(0.0D)) {
				// We store calculated values for previous month as the
				// first value of this month
				monthVal = (Double) values[0].getValue();
			} else {
				// estimate
				Calendar calPrMonth = Calendar.getInstance();
				calPrMonth.setTime(cal.getTime());
				calPrMonth.add(Calendar.MONTH, -1);
				monthVal = estimator.estimateMonthly(calPrMonth.get(Calendar.YEAR), calPrMonth.get(Calendar.MONTH),
				        energy);
			}
			estimated += monthVal;

			if (log.isDebugEnabled()) {
				log.debug(pue + ": Month after: " + cal.get(Calendar.MONTH) + " " + propertyName + ": " + monthVal);
			}

			cal.add(Calendar.MONTH, 1);
		}
		double curValue = estimateOverMonth(lastValue, lastTimeStamp);
		if (log.isDebugEnabled()) {
			log.debug(pue + ": Month: current Value: " + curValue);
		}
		// Current month
		estimated += curValue;

		// Calculate target
		Double target = estimator.estimateAnnual(Calendar.getInstance().get(Calendar.YEAR), targetPower);

		return target - estimated;
	}

	private static double estimateOverMonth(double value, Long timestamp) {
		Long endOfMonth = EnergyUtil.getMonthEnd(timestamp);
		Long startOfMonth = EnergyUtil.getMonthBeginning(timestamp);
		return value * (endOfMonth.doubleValue() - startOfMonth.doubleValue())
		        / (timestamp.doubleValue() - startOfMonth.doubleValue());
	}
}
