package com.synapsense.rulesengine.core.script.metric;

import java.util.Collection;

import org.apache.log4j.Logger;

import com.synapsense.dto.TO;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.exception.PropertyNotFoundException;
import com.synapsense.exception.UnableToConvertPropertyException;
import com.synapsense.rulesengine.core.environment.Property;
import com.synapsense.rulesengine.core.environment.RuleAction;
import com.synapsense.rulesengine.core.environment.RuleI;
import com.synapsense.service.Environment;

public class RoomRackAvgInlet implements RuleAction {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private final static Logger logger = Logger.getLogger(RoomRackAvgInlet.class);

	public Object run(final RuleI triggeredRule, final Property calculated) {
        if (logger.isDebugEnabled())
            logger.debug(triggeredRule.getName() + " is run");
		try {
			Environment env = calculated.getEnvironment();
			TO<?> nativeTO = calculated.getDataSource().getHostObjectTO();

			Double average = null;
			int racksNumber = 0;
			double sumAvgs = 0.0;

			Collection<TO<?>> racks = env.getRelatedObjects(nativeTO, "RACK", true);
			if ((racks != null) && (!racks.isEmpty())) {
				// iterate over racks within the room, collect the sums of inlet
				for (TO<?> rack : racks) {
					// Validate object status
					if (env.getPropertyValue(rack, "status", Integer.class) == 1) {

						Double coldTopVal = getValue(env, rack, "cTop");
						Double coldMidVal = getValue(env, rack, "cMid");
						Double coldBotVal = getValue(env, rack, "cBot");

						byte numberOfSensors = 0;
						double subTotal = 0.0;
						if (coldTopVal != null) {
							subTotal += coldTopVal;
							numberOfSensors++;
						}
						if (coldMidVal != null) {
							subTotal += coldMidVal;
							numberOfSensors++;
						}
						if (coldBotVal != null) {
							subTotal += coldBotVal;
							numberOfSensors++;
						}

						if (numberOfSensors > 0) {
							sumAvgs += (subTotal / numberOfSensors);
							racksNumber++;
						}
					}
				}
				if (racksNumber == 0) {
					if (logger.isDebugEnabled())
						logger.debug("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName()
						        + "': no recent or bad data.");
				}
			} else {
                if (logger.isDebugEnabled())
                    logger.debug("No Racks are found in this room to calculate average inlet temperature");
			}

			if (racksNumber != 0) {
				// Save Rack inlet count for future use
				env.setPropertyValue(nativeTO, "ric", (Integer) racksNumber);

				// Calculate average
				average = sumAvgs / racksNumber;
			} else {
				// if unsuccessful reset the property
				if (env.getPropertyValue(nativeTO, "ric", Integer.class) != new Integer(0)) {
					env.setPropertyValue(nativeTO, "ric", 0);
				}
			}
            if (logger.isDebugEnabled())
                logger.debug(triggeredRule.getName() + " is finished");
			return average;
		} catch (Exception e) {
			logger.error("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName() + "': error", e);
			return null;
		}
	}

	private Double getValue(Environment env, TO<?> obj, String propertyName) throws ObjectNotFoundException,
	        PropertyNotFoundException, UnableToConvertPropertyException {
		// Get sensor object
		TO<?> sensor = (TO<?>) env.getPropertyValue(obj, propertyName, Object.class);
		// checks that sensor is linked
		if (sensor == null) {
			return null;
		}

		// extract sensor value
		Double value = env.getPropertyValue(sensor, "lastValue", Double.class);

		// validate sensor data
		if ((value != null) && (value > -1000)) {
			return value;
		}
		return null;
	}
}