package com.synapsense.rulesengine.core.script.power.model;

import com.synapsense.dto.CollectionTO;
import com.synapsense.service.Environment;

/**
 * Abstract class for representing all phases objects.
 * 
 * @author Dmitry Grudzinskiy
 * 
 */
public abstract class Phase extends LinePowerObject {

	private String label;

	protected Double voltage;

	protected Rpdu parent;

	// In case of delta configuration we calculate delta values
	protected Double deltaAvgCurrent;
	protected Double deltaMaxCurrent;

	// Transient as we don't need to save the delta value
	protected Double deltaPotentialMaxCurrent;

	public Phase(Phase phase) {
		super(phase.objId);
		this.parent = phase.parent;
		this.label = phase.label;
		this.voltage = phase.voltage;
		setResetTime(parent.getResetTime());
	}

	public Phase(CollectionTO objProps, Rpdu parent) {
		super(objProps);
		this.voltage = Util.getDoubleValue(objProps, PowerConstants.VOLTAGE);
		this.label = (String) objProps.getSinglePropValue(PowerConstants.PHASE_LABEL).getValue();
		this.parent = parent;
	}

	@Override
	public void process(Environment env) throws Exception {
		calculate(env);
		aggregateParent(parent);
		
		// For phase balancing
		parent.addPhaseCurrent(avgCurrent);
		save(env);
	}

	public Double getVoltage() {
		return voltage;
	}

	public String getLabel() {
		return label;
	}

	public Double getDeltaAvgCurrent() {
		return deltaAvgCurrent;
	}

	public void addDeltaAvgCurrent(Double deltaAvgCurrent) {
		this.deltaAvgCurrent = Util.addValue(this.deltaAvgCurrent, deltaAvgCurrent);
	}

	public void addDeltaMaxCurrent(Double deltaMaxCurrent) {
		this.deltaMaxCurrent = Util.addValue(this.deltaMaxCurrent, deltaMaxCurrent);
	}

	public void addDeltaPotentialMaxCurrent(Double deltaPotentialMaxCurrent) {
		this.deltaPotentialMaxCurrent = Util.addValue(this.deltaPotentialMaxCurrent, deltaPotentialMaxCurrent);
	}

	protected abstract void calculate(Environment env) throws Exception;

	protected abstract void save(Environment env) throws Exception;
}
