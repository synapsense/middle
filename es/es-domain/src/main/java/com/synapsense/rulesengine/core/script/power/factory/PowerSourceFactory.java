package com.synapsense.rulesengine.core.script.power.factory;

import java.util.Collection;

import com.synapsense.rulesengine.core.script.power.model.Phase;
import com.synapsense.rulesengine.core.script.power.model.PowerRack;
import com.synapsense.rulesengine.core.script.power.model.Rpdu;

/**
 * Abstract factory for creating collection of power source objects - RPDUs and
 * PHASEs.
 * 
 * @see BcmsPowerSourceFactory
 * @see DuoPowerSourceFactory
 * @see ScFiFactory
 * @see ScNfiFactory
 * @see DeltaPOFactory
 * 
 * @author Dmitry Grudzinskiy
 * 
 */
public interface PowerSourceFactory {

	/**
	 * Creates a collection of rpdus using passed environment and parent power
	 * rack
	 * 
	 * @param rack
	 *            parent power rack
	 * @return collection of rpdus
	 * @throws Exception
	 */
	Collection<Rpdu> createRpdus(PowerRack rack) throws Exception;

	/**
	 * Creates a collection of phases using passed environment and parent rpdu
	 * 
	 * @param rpdu
	 *            parent rpdu
	 * @return collection of phases
	 * @throws Exception
	 */
	Collection<Phase> createPhases(Rpdu rpdu) throws Exception;
}
