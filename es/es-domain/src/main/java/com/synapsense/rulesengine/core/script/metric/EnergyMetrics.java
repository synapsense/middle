package com.synapsense.rulesengine.core.script.metric;

import static com.synapsense.rulesengine.core.script.energy.EnergyUtil.INF_POWER;
import static com.synapsense.rulesengine.core.script.energy.EnergyUtil.EMISSION;
import static com.synapsense.rulesengine.core.script.energy.EnergyUtil.EMISSION_FACTOR;
import static com.synapsense.rulesengine.core.script.energy.EnergyUtil.ENERGY_COST;
import static com.synapsense.rulesengine.core.script.energy.EnergyUtil.ENERGY_USAGE;
import static com.synapsense.rulesengine.core.script.energy.EnergyUtil.IT_POWER;
import static com.synapsense.rulesengine.core.script.energy.EnergyUtil.getCostCalculator;
import static com.synapsense.rulesengine.core.script.energy.EnergyUtil.getMonthBeginning;
import static com.synapsense.rulesengine.core.script.energy.EnergyUtil.getSumPropertiesValues;
import static com.synapsense.rulesengine.core.script.energy.EnergyUtil.isNewMonth;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.dto.TO;
import com.synapsense.exception.EnvException;
import com.synapsense.rulesengine.core.environment.Property;
import com.synapsense.rulesengine.core.environment.RuleAction;
import com.synapsense.rulesengine.core.environment.RuleI;
import com.synapsense.rulesengine.core.script.energy.CostCalculator;
import com.synapsense.rulesengine.core.script.energy.EnergyUtil;
import com.synapsense.service.Environment;

/**
 * Three pue metrics are calculated by this rule: <br>
 * - Energy usage<br>
 * - Energy cost <br>
 * - Carbon emission
 * 
 * @author Dmitry Grudzinskiy
 * 
 */
public class EnergyMetrics implements RuleAction {

	private static final long serialVersionUID = 4089949805048062316L;

	private static Log log = LogFactory.getLog(EnergyMetrics.class);

	@Override
	public Object run(RuleI triggeredRule, Property calculated) {
		long timestamp = System.currentTimeMillis();
		Environment env = calculated.getEnvironment();
		TO<?> pue = calculated.getDataSource().getHostObjectTO();
        if (log.isDebugEnabled())
            log.debug("Calculating energy metrics for " + pue);
		try {
			Double originalEmission = env.getPropertyValue(pue, EMISSION, Double.class);
			CostCalculator costCalculator = getCostCalculator(pue, env);
			if (costCalculator == null) {
				return originalEmission;
			}

			Double energy = getSumPropertiesValues(env, pue, IT_POWER, INF_POWER);
			if (energy == null) {
				return originalEmission;
			}

			long prevTimeStamp = env.getPropertyTimestamp(pue, ENERGY_USAGE);
			if (prevTimeStamp == 0) {
				// This is the first time of calculation
				prevTimeStamp = getMonthBeginning(timestamp);
			}

			// Get necessary properties
			Double curEnergyUsage = env.getPropertyValue(pue, ENERGY_USAGE, Double.class);
			Double curEnergyCost = env.getPropertyValue(pue, ENERGY_COST, Double.class);
			Double emissionFactor = env.getPropertyValue(pue, EMISSION_FACTOR, Double.class);

			long lastTimestamp = EnergyUtil.getMonthBeginning(timestamp);
			if (isNewMonth(prevTimeStamp, timestamp)) {
                if (log.isDebugEnabled())
                    log.debug("A new month detected for " + pue);

				// Calculate the previous month total
				Double lastMonthEnergyUsage = energy * (lastTimestamp - prevTimeStamp) / (60 * 60 * 1000);
				
				Double totalEnergyCost = costCalculator.calculateRt(curEnergyUsage == null ? 0.0 : curEnergyUsage,
				        curEnergyCost == null ? 0.0 : curEnergyCost, lastMonthEnergyUsage, lastTimestamp);
				Double totalEnergyUsage = (curEnergyUsage == null ? 0.0 : curEnergyUsage) + lastMonthEnergyUsage;
				Double totalEmission = emissionFactor * totalEnergyUsage / 1000;

				env.setPropertyValue(pue, ENERGY_USAGE, totalEnergyUsage);
				env.setPropertyValue(pue, ENERGY_COST, totalEnergyCost);
				env.setPropertyValue(pue, EMISSION, totalEmission);

				// Save zeros to easily find total
				env.setPropertyValue(pue, ENERGY_USAGE, 0D);
				env.setPropertyValue(pue, ENERGY_COST, 0D);
				env.setPropertyValue(pue, EMISSION, 0D);

				prevTimeStamp = lastTimestamp;

				// drop currents to 0.0
				curEnergyUsage = 0.0;
				curEnergyCost = 0.0;
			}

			// Calculate parameters
			Double energyUsage = energy * (timestamp - prevTimeStamp) / (60 * 60 * 1000);
			Double sumEnergyUsage = (curEnergyUsage == null ? 0.0 : curEnergyUsage) + energyUsage;
			Double energyCost = costCalculator.calculateRt(curEnergyUsage == null ? 0.0 : curEnergyUsage,
			        curEnergyCost == null ? 0.0 : curEnergyCost, energyUsage, timestamp);
			Double emission = emissionFactor * sumEnergyUsage / 1000;

			// Save energy usage and energy cost
			env.setPropertyValue(pue, ENERGY_USAGE, sumEnergyUsage);
			env.setPropertyValue(pue, ENERGY_COST, energyCost);

			// Emission will be saved by CRE
			return emission;
		} catch (EnvException e) {
			log.error(e.getMessage(), e);
			return null;
		}
	}
}
