package com.synapsense.rulesengine.core.script.metric;

import java.util.Collection;
import java.util.Date;

import org.apache.log4j.Logger;

import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;
import com.synapsense.rulesengine.core.environment.Property;
import com.synapsense.rulesengine.core.environment.RuleAction;
import com.synapsense.rulesengine.core.environment.RuleI;
import com.synapsense.service.Environment;

public class RoomRMI implements RuleAction {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final static Logger logger = Logger.getLogger(RoomRMI.class);

	private static final double rMax = 59;
	private static final double aMax = 63;
	private static final double maxDelta = 4; // aMax - rMax;
	private static final double rMin = 41.9;
	private static final double aMin = 18;
	private static final double minDelta = 23.9; // rMin - aMin;

	public Object run(final RuleI triggeredRule, final Property calculated) {
        if (logger.isDebugEnabled())
            logger.debug(triggeredRule.getName() + " is run");
		try {
			Double res = null;

			Environment env = calculated.getEnvironment();
			TO<?> nativeTO = calculated.getDataSource().getHostObjectTO();

			Integer n = 0; // total number of valid intakes
			Integer badIntakeCounterHi = 0; // number of intakes above allowable
											// limit
			Integer badIntakeCounterLo = 0; // number of intakes below allowable
											// limit
			Double sumDeltaHi = 0.0;
			Double sumDeltaLo = 0.0;

			Collection<TO<?>> racks = env.getRelatedObjects(nativeTO, "RACK", true);
			if ((racks != null) && (!racks.isEmpty())) {
				Date end = new Date();
				Date start = new Date(end.getTime() - 1800000); // subtract 30
																// minutes

				// iterate over racks within the room, collect the sums of inlet
				for (TO<?> rack : racks) {
					Collection<ValueTO> cTopValues = env.getHistory(rack, "coldDp", start, end, Double.class);
					if (cTopValues.size() != 0) {
						Double avgCTop = calculateAverageValue(cTopValues);
						if (avgCTop != null) {
							// increment number of valid intakes
							n++;

							double deltaTHi = avgCTop - rMax;
							if (deltaTHi > 0) {
								sumDeltaHi += deltaTHi;
								if ((avgCTop - aMax) > 0) {
									badIntakeCounterHi++;
								}
							}
							double deltaTLo = rMin - avgCTop;
							if (deltaTLo > 0) {
								sumDeltaLo += deltaTLo;
								if ((aMin - avgCTop) > 0) {
									badIntakeCounterLo++;
								}
							}
						}
					}
				}

				if (n == 0) {
					if (logger.isDebugEnabled())
						logger.debug("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName()
						        + "': no valid intakes found.");
				} else {
					// Save Bad Intake Counters
					env.setPropertyValue(nativeTO, "bmich", badIntakeCounterHi);
					env.setPropertyValue(nativeTO, "bmicl", badIntakeCounterLo);
					// Save rmihi value
					env.setPropertyValue(nativeTO, "rmihi", ((1 - (sumDeltaHi / (maxDelta * n))) * 100));

					res = (1 - (sumDeltaLo / (minDelta * n))) * 100;
				}
			} else {
                if (logger.isDebugEnabled())
				    logger.debug("No Racks are found in this room.");
			}

			// if unsuccessful reset all properties
			if (res == null) {
				if (env.getPropertyValue(nativeTO, "bmich", Integer.class) != null) {
					env.setPropertyValue(nativeTO, "bmich", null);
				}
				if (env.getPropertyValue(nativeTO, "bmicl", Integer.class) != null) {
					env.setPropertyValue(nativeTO, "bmicl", null);
				}
				if (env.getPropertyValue(nativeTO, "rmihi", Double.class) != null) {
					env.setPropertyValue(nativeTO, "rmihi", null);
				}
			}

            if (logger.isDebugEnabled())
			    logger.debug(triggeredRule.getName() + " is finished");
			return res;
		} catch (Exception e) {
			logger.error("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName() + "': error", e);
			return null;
		}
	}

	private Double calculateAverageValue(Collection<ValueTO> values) {
		Double res = 0.0;
		int numberOfValidValues = 0;

		for (ValueTO dbl : values) {
			Double value = (Double) dbl.getValue();

			// validate that sensor value is not an error code
			if ((value != null) && (value > -1000)) {
				res += value;
				numberOfValidValues++;
			}
		}

		if (numberOfValidValues > 0) {
			res = res / numberOfValidValues;
		} else {
			res = null;
		}

		return res;
	}
}