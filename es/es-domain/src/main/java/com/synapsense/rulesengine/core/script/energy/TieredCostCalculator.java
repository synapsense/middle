package com.synapsense.rulesengine.core.script.energy;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

/**
 * Cost calculator implementation in case costs are defined as a tiered table.
 * 
 * @author Dmitry Grudzinskiy
 * 
 */
public class TieredCostCalculator implements CostCalculator {

	private ArrayList<Tier> tiers = new ArrayList<Tier>();

	public static TieredCostCalculator load(String src) {
		TieredCostCalculator calculator = new TieredCostCalculator();
		String[] tiersArray = src.split(";");
		for (String tierStr : tiersArray) {
			String[] values = tierStr.split(",");
			if (values.length != 2) {
				throw new ParseCostTableException("Tiered row is unparsebale: " + tierStr);
			}
			try {
				calculator.addTier(Double.valueOf(values[0]), Double.valueOf(values[1]));
			} catch (NumberFormatException e) {
				throw new ParseCostTableException("Tiered row is unparsebale: " + tierStr);
			}
		}
		return calculator;
	}

	@Override
	public double calculateRt(double prevEnergyUsage, double prevEnergyCost, double energyUsage, long timestamp) {
		return calculatePowerCost(prevEnergyUsage + energyUsage);
	}

	@Override
	public double estimateAnnual(int year, double power) {
		return 24 * EnergyUtil.getNumberOfDays(year) * calculatePowerCost(power);
	}

	@Override
	public double estimateMonthly(int year, int month, double power) {
		return 24 * EnergyUtil.getNumberOfDays(year, month) * calculatePowerCost(power);
	}

	public void addTier(double kwh, double cost) {
		Tier tier = new Tier(kwh, cost);
		if (tiers.contains(tier)) {
			throw new IllegalArgumentException("Duplicate tier kwh=" + tier.kwh);
		}
		tiers.add(tier);
	}

	public Collection<Tier> getTiers() {
		return Collections.unmodifiableCollection(tiers);
	}

	public static class Tier implements Comparable<Tier> {

		private Double kwh;
		private Double cost;

		public Tier(double kwh, double cost) {
			if (kwh <= 0 || cost < 0) {
				throw new IllegalArgumentException("Either kwh < 0 or cost <= 0: kwh=" + kwh + " cost=" + cost);
			}
			this.kwh = kwh;
			this.cost = cost;
		}

		@Override
		public int compareTo(Tier o) {
			return kwh.compareTo(o.kwh);
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			long temp;
			temp = Double.doubleToLongBits(kwh);
			result = prime * result + (int) (temp ^ (temp >>> 32));
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Tier other = (Tier) obj;
			if (Double.doubleToLongBits(kwh) != Double.doubleToLongBits(other.kwh))
				return false;
			return true;
		}

		@Override
		public String toString() {
			return "Tier [kwh=" + kwh + ", cost=" + cost + "]";
		}
	}

	private double calculatePowerCost(double energy) {
		if (tiers.isEmpty()) {
			throw new IllegalStateException("At least one tier must be available");
		}

		Collections.sort(tiers);

		int i = 0;
		double costSum = 0;
		double prevKwh = 0;

		while (energy > 0) {
			Tier tier = tiers.get(i++);

			// If it is last tier then the rest of the energy falls under its
			// cost
			double curEnergy = i == tiers.size() ? energy : Math.min(energy, tier.kwh - prevKwh);
			costSum += curEnergy * tier.cost;

			energy -= curEnergy;
			prevKwh = tier.kwh;
		}

		return costSum;
	}
}
