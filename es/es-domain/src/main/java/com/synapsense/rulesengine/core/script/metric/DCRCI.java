package com.synapsense.rulesengine.core.script.metric;

import java.util.Collection;

import org.apache.log4j.Logger;

import com.synapsense.dto.TO;
import com.synapsense.rulesengine.core.environment.Property;
import com.synapsense.rulesengine.core.environment.RuleAction;
import com.synapsense.rulesengine.core.environment.RuleI;
import com.synapsense.service.Environment;

public class DCRCI implements RuleAction {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final static Logger logger = Logger.getLogger(DCRCI.class);

	private static final double maxDelta = 9.4; // aMax - rMax;
	private static final double minDelta = 5.4; // rMin - aMin;

	public Object run(final RuleI triggeredRule, final Property calculated) {
        if (logger.isDebugEnabled())
            logger.debug(triggeredRule.getName() + " is run");
		try {
			Double res = null;

			Environment env = calculated.getEnvironment();
			TO<?> nativeTO = calculated.getDataSource().getHostObjectTO();

			Collection<TO<?>> ROOMs = env.getRelatedObjects(nativeTO, "ROOM", true);
			if ((ROOMs != null) && (!ROOMs.isEmpty())) {
				int bich = 0, bicl = 0;
				;
				Double totalOver = 0.0;
				Double totalUnder = 0.0;
				int n = 0;

				// iterate over ROOMs within the DC
				for (TO<?> roomTO : ROOMs) {
					Double roomOver = env.getPropertyValue(roomTO, "totalOver", Double.class);
					Double roomUnder = env.getPropertyValue(roomTO, "totalUnder", Double.class);

					if ((roomOver != null) && (roomUnder != null)) {
						int roomTotal = env.getPropertyValue(roomTO, "rcin", Integer.class);
						int roomBICh = env.getPropertyValue(roomTO, "bich", Integer.class);
						int roomBICl = env.getPropertyValue(roomTO, "bicl", Integer.class);
						if (roomTotal != 0) {
							totalOver += roomOver;
							totalUnder += roomUnder;
							n += roomTotal;
							bich += roomBICh;
							bicl += roomBICl;
						}
					}
				}

				if (n != 0) {
					env.setPropertyValue(nativeTO, "bich", bich);
					env.setPropertyValue(nativeTO, "bicl", bicl);
					env.setPropertyValue(nativeTO, "rcihi", ((1 - (totalOver / (maxDelta * n))) * 100));
					res = (1 - (totalUnder / (minDelta * n))) * 100;
				} else {
                    if (logger.isDebugEnabled())
					    logger.debug(triggeredRule.getName() + ": no data available");
				}
			} else {
				logger.error("Rule '" + triggeredRule.getName() + "': no Rooms found.");
			}

			// if unsuccessful reset all properties
			if (res == null) {
				if (env.getPropertyValue(nativeTO, "bich", Integer.class) != null) {
					env.setPropertyValue(nativeTO, "bich", null);
				}
				if (env.getPropertyValue(nativeTO, "bicl", Integer.class) != null) {
					env.setPropertyValue(nativeTO, "bicl", null);
				}
				if (env.getPropertyValue(nativeTO, "rcihi", Double.class) != null) {
					env.setPropertyValue(nativeTO, "rcihi", null);
				}
			}

            if (logger.isDebugEnabled())
                logger.debug(triggeredRule.getName() + " is finished");
			return res;
		} catch (Exception e) {
			logger.error("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName() + "': error", e);
			return null;
		}
	}
}