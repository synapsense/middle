package com.synapsense.rulesengine.core.script.metric;

import static com.synapsense.rulesengine.core.script.power.model.PowerConstants.AP_POWER;
import static com.synapsense.rulesengine.core.script.power.model.PowerConstants.DEMAND_POWER;
import static com.synapsense.rulesengine.core.script.power.model.PowerConstants.SERVER;
import static com.synapsense.rulesengine.core.script.power.model.PowerConstants.RPDU;

import java.util.Collection;

import org.apache.log4j.Logger;

import com.synapsense.dto.CollectionTO;
import com.synapsense.dto.TO;
import com.synapsense.rulesengine.core.environment.Property;
import com.synapsense.rulesengine.core.environment.RuleAction;
import com.synapsense.rulesengine.core.environment.RuleI;
import com.synapsense.rulesengine.core.script.power.model.Util;
import com.synapsense.service.Environment;

/**
 * Calculates asset group metrics using it servers values.
 * 
 * @author Dmitry Grudzinskiy
 * 
 */
public class GroupMetrics implements RuleAction {

	private static final long serialVersionUID = 7735055160593256446L;

	private static Logger logger = Logger.getLogger(PowerRackMetrics.class);

	private static String[] GROUP_PROPS = new String[] { DEMAND_POWER, AP_POWER };

	@Override
	public Object run(RuleI triggeredRule, Property calculated) {
		try {
			Environment env = calculated.getEnvironment();

			TO<?> group = calculated.getDataSource().getHostObjectTO();

			String name = env.getPropertyValue(group, "name", String.class);
            if (logger.isDebugEnabled())
                logger.debug("Calculating properties of group " + name);

			Collection<TO<?>> members = env.getChildren(group, SERVER);

			// Bug 5906. rpdus can be also group members
			members.addAll(env.getChildren(group, RPDU));

			Double apPower = null;
			Double demandPower = null;
			if (!members.isEmpty()) {
				Collection<CollectionTO> membersProps = env.getPropertyValue(members, GROUP_PROPS);

				for (CollectionTO memberProps : membersProps) {
					apPower = Util.addValue(apPower, Util.getDoubleValue(memberProps, AP_POWER));
					demandPower = Util.addValue(demandPower, Util.getDoubleValue(memberProps, DEMAND_POWER));
				}
			}

			env.setPropertyValue(group, AP_POWER, apPower);
			return demandPower;
		} catch (Exception e) {
			logger.error("Rule '" + triggeredRule.getName() + " error", e);
			return null;
		}
	}
}
