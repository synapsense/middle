package com.synapsense.rulesengine.core.script.power.model;

import com.synapsense.dto.CollectionTO;
import com.synapsense.service.Environment;

/**
 * The class implements rpdu parameters computation in case the corresponding
 * rack is instrumented by either DUO or BCMS. All the common values except
 * historical maximums are sums of children phases corresponding parameters.
 * 
 * @author Dmitry Grudzinskiy
 * 
 */
public class DuoBcmsRpdu extends Rpdu {

	public DuoBcmsRpdu(CollectionTO objProps, PowerRack parent) {
		super(objProps, parent);
	}

	@Override
	public void process(Environment env) throws Exception {
		processUnbalancedThreshold(env);
		aggregateParent(parent);
		processHistoricalMaxPower();
		saveCalculated(env);
	}
}
