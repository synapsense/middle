package com.synapsense.rulesengine.core.script.power.factory;

import com.synapsense.dto.CollectionTO;
import com.synapsense.rulesengine.core.script.power.model.DuoBcmsRpdu;
import com.synapsense.rulesengine.core.script.power.model.DuoPhase;
import com.synapsense.rulesengine.core.script.power.model.Phase;
import com.synapsense.rulesengine.core.script.power.model.PowerConstants;
import com.synapsense.rulesengine.core.script.power.model.PowerRack;
import com.synapsense.rulesengine.core.script.power.model.Rpdu;

/**
 * An implementation of {@link PowerSourceFactory} that creates DUO power
 * objects.
 * 
 * @author Dmitry Grudzinskiy
 * 
 */
public class DuoPowerSourceFactory extends AbstractPowerSourceFactory {

	@Override
	protected Rpdu createRpdu(CollectionTO collTo, PowerRack parent) {
		return new DuoBcmsRpdu(collTo, parent);
	}

	@Override
	protected Phase createPhase(CollectionTO collTo, Rpdu parent) {
		return new DuoPhase(collTo, parent);
	}

	@Override
	protected String[] getPhaseProperties() {
		return PowerConstants.PH_DUO_PROPS;
	}
}
