package com.synapsense.rulesengine.core.script.energy;

/**
 * The root interface for all energy cost calculation algorithms.
 * 
 * @author Dmitry Grudzinskiy
 * @see TieredCostCalculator
 * @see TimeOfUseCostCalculator
 */
public interface CostCalculator extends Estimator {

	/**
	 * The method is intended for real-time calculation of monthly energy cost.
	 * The result of the calculation is an aggregated energy cost for the period
	 * from the beginning of the month to passed timestamp.
	 * 
	 * @param prevEnergyUsage
	 *            sum energy usage (kWh) calculated during previous calculation
	 *            cycle
	 * @param prevEnergyCost
	 *            sum energy cost calculated during previous calculation cycle
	 * @param energyUsage
	 *            current energy usage
	 * @param timestamp
	 *            the time when energy usage was last calculated
	 * @return summary energy cost
	 */
	double calculateRt(double prevEnergyUsage, double prevEnergyCost, double energyUsage, long timestamp);
}