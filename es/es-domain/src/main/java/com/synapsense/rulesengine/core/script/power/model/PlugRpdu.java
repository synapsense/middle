package com.synapsense.rulesengine.core.script.power.model;

import com.synapsense.dto.CollectionTO;
import com.synapsense.service.Environment;

public class PlugRpdu extends Rpdu {

	public PlugRpdu(CollectionTO objProps, PowerRack parent) {
		super(objProps, parent);
	}

	@Override
	public void process(Environment env) throws Exception {
		processUnbalancedThreshold(env);
		aggregateParent(parent);
		processHistoricalMaxPower();

		// Additional potential maximum
		parent.addPotentialMaxApPower(potentialMaxApPower);
		saveAll(env);
	}
}
