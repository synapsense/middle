package com.synapsense.rulesengine.core.script.power.model;

import com.synapsense.dto.CollectionTO;
import com.synapsense.service.Environment;

/**
 * The class implements server parameters computation in case the corresponding
 * rack is instrumented by jawas.
 * 
 * @author Dmitry Grudzinskiy
 * 
 */
public class PlugServer extends PowerObject {

	public PlugServer(CollectionTO objProps, PowerRack parent) {
		super(objProps);
		setResetTime(parent.getResetTime());
	}

	@Override
	public void process(Environment env) throws Exception {
		processHistoricalMaxPower();
		saveCalculated(env);
		saveLastResetTime(env);
	}
}
