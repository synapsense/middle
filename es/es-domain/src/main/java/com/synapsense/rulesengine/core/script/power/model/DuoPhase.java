package com.synapsense.rulesengine.core.script.power.model;

import java.util.Collection;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.dto.CollectionTO;
import com.synapsense.dto.TO;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.exception.PropertyNotFoundException;
import com.synapsense.exception.UnableToConvertPropertyException;
import com.synapsense.service.Environment;

/**
 * The class impelements phase parameters computation in case the corresponding
 * rack is instrumented by DUO. Computations are similar to {@link PlugPhase}
 * but it is not necessary to save
 * <code>avgCurrent, maxCurrent, demandPower</code> as they are sent by the
 * device.
 * 
 * @author Dmitry Grudzinskiy
 * 
 */
public class DuoPhase extends Phase {

	private static Log log = LogFactory.getLog( DuoPhase.class );

	public DuoPhase(CollectionTO objProps, Rpdu parent) {
		super(objProps, parent);
		// There might be delta values
		this.deltaAvgCurrent = Util.getDoubleValue(objProps, PowerConstants.DELTA_AVG_CURRENT);
		this.deltaMaxCurrent = Util.getDoubleValue(objProps, PowerConstants.DELTA_MAX_CURRENT);

		// if smartlink doesn't report voltage we will use nominal voltage from
		// POWER_RACK
		if (voltage == null) {
			voltage = parent.parent.getNominalVoltage();
		}
	}

	@Override
	protected void calculate(Environment env) throws Exception {
		// Calculate apPower, maxApPower
		processPower(getVoltage());
		processDemandPower(env);

		// Update maxHistCurrent, maxHistApPower
		processHistoricalMaxCurrent();
		processHistoricalMaxPower();

	}

	@Override
	protected void save(Environment env) throws Exception {
		saveValueList(createDefaultValueList(), env);
		saveLastResetTime(env);
	}

	@Override
	protected void processPower(Double voltage) {
		if (isValid(voltage)) {
			Double current;
			Double mCurrent;
			if (parent.type == PowerConstants.DELTA) {
				// Voltage is always a Line (WYE) value due to upstream conversions, so convert it back to Delta for this.
				voltage = voltage * Math.sqrt(3);
				current = this.deltaAvgCurrent;
				mCurrent = this.deltaMaxCurrent;
			} else {
				current = this.avgCurrent;
				mCurrent = this.maxCurrent;
			}
			if (isValid(current)) {
				apPower = voltage * current / 1000;
			}
			if (isValid(mCurrent)) {
				maxApPower = voltage * mCurrent / 1000;
			}
		}

	}

	protected void processDemandPower(Environment env) throws ObjectNotFoundException, PropertyNotFoundException,
	        UnableToConvertPropertyException {
		if (!isValid(demandPower)) {
			Double current;
			Double voltage;
			if (parent.type == PowerConstants.DELTA) {
				// Voltage is always a Line (WYE) value due to upstream conversions, so convert it back to Delta for this.
				voltage = getVoltage() * Math.sqrt(3);
				current = this.deltaAvgCurrent;
			} else {
				voltage = getVoltage();
				current = this.avgCurrent;
			}
			if (isValid(current)) {
				Collection<TO<?>> relatedDC = env.getRelatedObjects(this.objId, PowerConstants.DC, false);
				if (!relatedDC.isEmpty()) {
					Double pf = env.getPropertyValue(relatedDC.iterator().next(), PowerConstants.POWER_FACTOR,
					        Double.class);
					this.demandPower = (current * voltage * pf) / 1000;

					if( log.isDebugEnabled() ) {
						StringBuilder str = new StringBuilder("demandPower ");
						str.append( parent.objId );
						str.append(".");
						str.append(	this.getLabel() );
						str.append( parent.type == PowerConstants.DELTA ? "d" : "y" );
						str.append(" = ( I ");
						str.append( current );
						str.append(" * V ");
						str.append( voltage );
						str.append(" * PF ");
						str.append( pf );
						str.append(" ) / 1000 = ");
						str.append( this.demandPower );
						log.debug( str.toString() );
					}
				}
			}
		}
	}
}
