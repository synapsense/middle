package com.synapsense.rulesengine.core.script.metric;

import java.util.Collection;

import org.apache.log4j.Logger;

import com.synapsense.dto.TO;
import com.synapsense.rulesengine.core.environment.Property;
import com.synapsense.rulesengine.core.environment.RuleAction;
import com.synapsense.rulesengine.core.environment.RuleI;
import com.synapsense.service.Environment;

public class RoomStandaloneAvgTemp implements RuleAction {

	private static final long serialVersionUID = -4283931128070535075L;
	private final static Logger logger = Logger
			.getLogger(RoomStandaloneAvgTemp.class);

	@Override
	public Object run(RuleI triggeredRule, Property calculated) {
        if (logger.isDebugEnabled())
            logger.debug(triggeredRule.getName() + " is run");
		try {
			Environment env = calculated.getEnvironment();
			TO<?> nativeTO = calculated.getDataSource().getHostObjectTO();

			Double res = null;

			Collection<TO<?>> gt = env.getRelatedObjects(nativeTO,
					"GENERICTEMPERATURE", true);
			if (gt != null && !gt.isEmpty()) {
				int n = 0;
				Double avg = 0.;

				for (TO<?> to : gt) {
					TO<?> sensorTO = env.getPropertyValue(to, "sensor",
							TO.class);
					Double value = env.getPropertyValue(sensorTO, "lastValue",
							Double.class);
					if (value != null && value > -1000) {
						avg += value;
						n++;
					}
				}

				if (n != 0) {
					res = avg / n;
					env.setPropertyValue(nativeTO, "avgST", res);
				} else {
					logger.warn(triggeredRule.getName() + ": no data available");
				}
			}

			if (res == null) {
				// clean up
				if (env.getPropertyValue(nativeTO, "avgST", Double.class) != null) {
					env.setPropertyValue(nativeTO, "avgST", null);
				}
			}

            if (logger.isDebugEnabled())
                logger.debug(triggeredRule.getName() + " is finished");
			return res;
		} catch (Exception e) {
			logger.error("Rule '" + triggeredRule.getName() + "', Property '"
					+ calculated.getName() + "': error", e);
			return null;
		}
	}

}
