package com.synapsense.rulesengine.core.script.metric;

import java.util.Collection;

import org.apache.log4j.Logger;

import com.synapsense.dto.TO;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.exception.PropertyNotFoundException;
import com.synapsense.exception.UnableToConvertPropertyException;
import com.synapsense.rulesengine.core.environment.Property;
import com.synapsense.rulesengine.core.environment.RuleAction;
import com.synapsense.rulesengine.core.environment.RuleI;
import com.synapsense.service.Environment;
import com.synapsense.service.impl.dao.to.PropertyTO;

public class RackBPA implements RuleAction {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final static Logger logger = Logger.getLogger(RackBPA.class);

	private Property ref = null;
    private Property hTop = null;
	
	public Object run(final RuleI triggeredRule, final Property calculated) {
		logger.debug(triggeredRule.getName() + " is run");
		try {
			double res = 0;
			double hotAvg = 0;

			Environment env = calculated.getEnvironment();
			TO<?> nativeTO = calculated.getDataSource().getHostObjectTO();

			if (env.getPropertyValue(nativeTO, "status", Integer.class) != 1) {
                if (logger.isDebugEnabled())
                    logger.debug("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName()
				        + "': some data is obsolete. Skipping...");
				return null;
			}

			TO<?> refSensor = (TO<?>) ref.getValue();
            TO<?> refHtopSensor = (TO<?>) hTop.getValue();

            Double refVal = getValue(env, refSensor, "lastValue");
			Double hTopVal = getValue(env, refHtopSensor, "lastValue");

			if ((refVal == null) || (hTopVal == null)) {
				if (logger.isDebugEnabled())
					logger.debug("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName()
					        + "': one of the inputs is not valid.");
				return null;
			}

			if ((env.getPropertyValue(nativeTO, "hMid", Object.class) == null)
			        || env.getPropertyValue(nativeTO, "hBot", Object.class) == null) {
				hotAvg = hTopVal;
			} else {
				Double hMidVal = getValue(env, nativeTO, "hMid");
				Double hBotVal = getValue(env, nativeTO, "hBot");
				if ((hMidVal == null) || (hBotVal == null)) {
					if (logger.isDebugEnabled())
						logger.debug("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName()
						        + "': one of the inputs is not valid.");
					return null;
				}
				hotAvg = (hTopVal + hMidVal + hBotVal) / 3;
			}

			Double deltaC = hotAvg - refVal;
			if (deltaC <= 0) {
                if (logger.isDebugEnabled())
				    logger.debug("Rule '" + triggeredRule.getName() + ": temperature raise is negative.");
				return null;
			}

			// get rack parent rooms
			Collection<TO<?>> rooms = env.getParents(nativeTO, "ROOM");
			if (rooms == null) {
				String nativePropName = calculated.getDataSource().getNativePropertyName();
				throw new IllegalStateException("Property '" + nativePropName
				        + "' has no parent room. Unable to find racks.");
			}

			// Calculate average CRAC return air temperature
			Double roomCRACReturnTotal = 0.0;
			int roomCount = 0;
			for (TO<?> room : rooms) {
				Double roomAvgCRACReturn = env.getPropertyValue(room, "avgCR", Double.class);

				if (roomAvgCRACReturn != null) {
					roomCRACReturnTotal += roomAvgCRACReturn;
					roomCount++;
				}
			}

			if (roomCount != 0) {
				Double avgCRACReturn = roomCRACReturnTotal / rooms.size();

				// Calculate delta t between average hot aisle temp and CRAC
				// return temp
				Double deltaH = hotAvg - avgCRACReturn;

				if (deltaH < 0) {
					// No cold air is bypassing the Rack
					res = 0;
				} else if (deltaC < deltaH) {
					// All cold air by pass the Rack
					res = 100;
				} else {
					res = (deltaH / deltaC) * 100;
				}

				return res;
			} else {
                if (logger.isDebugEnabled())
				    logger.debug("Rule '" + triggeredRule.getName() + ": no average CRAC return data available.");
			}

			return null;
		} catch (Exception e) {
			logger.error("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName() + "': error", e);
			return null;
		}
	}

	private Double getValue(Environment env, TO<?> obj, String propertyName) throws ObjectNotFoundException,
	        PropertyNotFoundException, UnableToConvertPropertyException {
		Double value = null;

		Object propObj = env.getPropertyValue(obj, propertyName, Object.class);
		// support direct values or links
		if (propObj == null) {
			return null;
		} else if (propObj instanceof TO) {
			value = env.getPropertyValue(((TO<?>) propObj), "lastValue", Double.class);
		} else if (propObj instanceof Double) {
			value = (Double) propObj;
		} else if (propObj instanceof PropertyTO) {
			value = env.getPropertyValue(((PropertyTO) propObj).getObjId(), ((PropertyTO) propObj).getPropertyName(),
			        Double.class);
		} else {
			throw new IllegalArgumentException("Unsupported type of object: "
			        + ((propObj == null) ? "null" : propObj.getClass().getCanonicalName()));
		}

		// validate sensor data
		if ((value != null) && (value > -1000)) {
			return value;
		}
		return null;
	}
	
	public Property getRef() {
		return ref;
	}

	public void setRef(Property ref) {
		this.ref = ref;
	}

    public Property gethTop() {
        return hTop;
    }

    public void sethTop(Property hTop) {
        this.hTop = hTop;
    }
}