package com.synapsense.rulesengine.core.script.metric;

import static com.synapsense.rulesengine.core.script.power.model.PowerConstants.DC;
import static com.synapsense.rulesengine.core.script.power.model.PowerConstants.LAST_RESET_TIME;
import static com.synapsense.rulesengine.core.script.power.model.PowerConstants.POWER_RACK_PROPS;
import static com.synapsense.rulesengine.core.script.power.model.PowerConstants.RESET_DAYS;
import static com.synapsense.rulesengine.core.script.power.model.PowerConstants.RPDU;
import static com.synapsense.rulesengine.core.script.power.model.PowerConstants.RPDU_TYPE;
import static com.synapsense.rulesengine.core.script.power.model.PowerConstants.STATUS;
import static com.synapsense.rulesengine.core.script.power.model.PowerConstants.UNBAL_PERC;
import static com.synapsense.rulesengine.core.script.power.model.PowerConstants.STATUS_DISABLED;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.dto.TO;
import com.synapsense.rulesengine.core.environment.Property;
import com.synapsense.rulesengine.core.environment.RuleAction;
import com.synapsense.rulesengine.core.environment.RuleI;
import com.synapsense.rulesengine.core.script.power.factory.BcmsPowerSourceFactory;
import com.synapsense.rulesengine.core.script.power.factory.DeltaDuoPowerSourceFactory;
import com.synapsense.rulesengine.core.script.power.factory.DeltaPOFactory;
import com.synapsense.rulesengine.core.script.power.factory.DuoPowerSourceFactory;
import com.synapsense.rulesengine.core.script.power.factory.ScFiFactory;
import com.synapsense.rulesengine.core.script.power.factory.ScNfiFactory;
import com.synapsense.rulesengine.core.script.power.model.PowerRack;
import com.synapsense.rulesengine.core.script.power.model.PowerRack.RackConfiguration;
import com.synapsense.rulesengine.core.script.power.model.PowerRack.RackConfiguration.Instrumentation;
import com.synapsense.rulesengine.core.script.power.model.PowerRack.RackConfiguration.PowerPlatform;
import com.synapsense.rulesengine.core.script.power.model.PowerRack.RackConfiguration.RpduConfiguration;
import com.synapsense.rulesengine.core.script.power.process.DuoPlatfromProcessor;
import com.synapsense.rulesengine.core.script.power.process.NoPlugStructureProcessor;
import com.synapsense.rulesengine.core.script.power.process.PowerPlatformProcessor;
import com.synapsense.rulesengine.core.script.power.process.ScPlatformProcessor;
import com.synapsense.service.AlertService;
import com.synapsense.service.Environment;

/**
 * The rule calculates metrics related to linked POWER_RACK object and its
 * descendants - SERVER, RPDU, PHASE, PLUG. The properties to calculate are as
 * follows:<br>
 * - avgCurrent<br>
 * - maxCurrent<br>
 * - demandPower<br>
 * - maxHistCurrent<br>
 * - apPower<br>
 * - maxApPower<br>
 * - maxHistApPower<br>
 * - potentialMaxCurrent<br>
 * - potentialMaxApPower
 * 
 * <p>
 * All the processing is located inside this one rule for performance reasons.
 * 
 * @author Dmitry Grudzinskiy
 */

public class PowerRackMetrics implements RuleAction {

	private static Log log = LogFactory.getLog(PowerRackMetrics.class);

	private static final long serialVersionUID = 1L;

	@Override
	public Object run(RuleI triggeredRule, Property calculated) {
		long start = System.currentTimeMillis();
		Environment env = calculated.getEnvironment();

		// Build map of processors
		HashMap<RackConfiguration, PowerPlatformProcessor> platformProcessors = buildProcessorsMap(env,
		        calculated.getAlertService());

		// Power rack TO
		TO<?> prTo = calculated.getDataSource().getHostObjectTO();
		try {

			Integer status = env.getPropertyValue(prTo, STATUS, Integer.class);
			if (status == STATUS_DISABLED) {
				if (log.isDebugEnabled())
					log.debug(prTo + " cre rule: rack is disabled, no calculations to be performed");
				// Power rack is not active - no need to calculate anything
				return null;
			}

            if (log.isDebugEnabled())
                log.debug("Calculating metrics of Power Rack " + prTo);

			// Collect all the necessary properties to build power rack
			PowerRack powerRack = buildPowerRack(prTo, env);

			// Now the magic begins.
			// We need to use a suitable processor and object factories
			// according to the rack platform, rpdu configuration and
			// instrumentation
			PowerPlatformProcessor processor = platformProcessors.get(powerRack.getRackConfiguration());
			if (processor == null) {
				log.error(prTo + " cre rule: unsupported configuration " + powerRack.getRackConfiguration()
				        + ", no calculations to be performed ");
				return null;
			}
			processor.process(powerRack);
			if (log.isDebugEnabled()) {
				log.debug(prTo + " cre rule completed in " + (System.currentTimeMillis() - start) + " mls");
			}

			return powerRack.getDemandPower();
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			return null;
		}
	}

	private static PowerRack buildPowerRack(TO<?> prTo, Environment env) throws Exception {

		PowerRack powerRack = new PowerRack(env.getPropertyValue(Arrays.asList(new TO<?>[] { prTo }), POWER_RACK_PROPS)
		        .iterator().next());
		// Now we need to obtain rack rpdu configuration
		// Actually it should be a property on a rack level as we don't
		// support multi-configured racks
		Collection<TO<?>> rpdus = env.getChildren(prTo, RPDU);
		if (!rpdus.isEmpty()) {
			powerRack.setRpduConfiguration(env.getPropertyValue(rpdus.iterator().next(), RPDU_TYPE, Integer.class));
		}
		if (log.isDebugEnabled()) {
			log.debug(prTo + " cre rule: rack configuration is " + powerRack.getRackConfiguration());
		}

		// Retrieve unbalancedPercentage to further determine if rpdus are
		// out of balance
		TO<?> dc = env.getRelatedObjects(prTo, DC, false).iterator().next();
		powerRack.setUnbalThresh(env.getPropertyValue(dc, UNBAL_PERC, Double.class) / 100);

		// Get reset days to determine whether it is necessary to reset
		// historical maximums
		Integer resetDays = env.getPropertyValue(dc, RESET_DAYS, Integer.class);

		// Determine whether it is necessary to reset historical values
		if (resetDays != null && !resetDays.equals(0)) {
			// Using time stamp instead of actual value
			Long lastResetTime = env.getPropertyTimestamp(prTo, LAST_RESET_TIME);
			Long newTime = System.currentTimeMillis();
			if ((newTime - lastResetTime) > (resetDays * 24L * 60L * 60L * 1000L)) {
				if (log.isDebugEnabled()) {
					log.debug(prTo + " cre rule: new object lastResetTime = " + newTime);
				}
				powerRack.setResetTime(newTime);
			}
		}
		return powerRack;
	}

	private static HashMap<RackConfiguration, PowerPlatformProcessor> buildProcessorsMap(Environment env,
	        AlertService alertService) {
		HashMap<RackConfiguration, PowerPlatformProcessor> platformProcessors = new HashMap<PowerRack.RackConfiguration, PowerPlatformProcessor>();
		// These are all supported configuration conmbinations
		// BCMS
		BcmsPowerSourceFactory bcmsFactory = new BcmsPowerSourceFactory();
		bcmsFactory.setEnv(env);
		NoPlugStructureProcessor bcmsProcessor = new NoPlugStructureProcessor(bcmsFactory);
		bcmsProcessor.setEnv(env);

		platformProcessors.put(new RackConfiguration(PowerPlatform.BCMS, Instrumentation.NFI, RpduConfiguration.WYE),
		        bcmsProcessor);
		platformProcessors.put(new RackConfiguration(PowerPlatform.BCMS, Instrumentation.NFI, RpduConfiguration.DELTA),
		        bcmsProcessor);

		// DUO
		DuoPowerSourceFactory duoFactory = new DuoPowerSourceFactory();
		duoFactory.setEnv(env);
		DuoPlatfromProcessor duoProcessor = new DuoPlatfromProcessor(duoFactory);
		duoProcessor.setEnv(env);

		platformProcessors.put(new RackConfiguration(PowerPlatform.DUO, Instrumentation.NFI, RpduConfiguration.WYE),
		        duoProcessor);

		DeltaDuoPowerSourceFactory deltaDuoFactory = new DeltaDuoPowerSourceFactory();
		deltaDuoFactory.setEnv(env);
		DuoPlatfromProcessor deltaDuoProcessor = new DuoPlatfromProcessor(deltaDuoFactory);
		deltaDuoProcessor.setEnv(env);

		platformProcessors.put(new RackConfiguration(PowerPlatform.DUO, Instrumentation.NFI, RpduConfiguration.DELTA),
				deltaDuoProcessor);

		// SC
		platformProcessors.put(new RackConfiguration(PowerPlatform.SC, Instrumentation.FI, RpduConfiguration.WYE),
		        new ScPlatformProcessor(new ScFiFactory(), env, alertService));
		platformProcessors.put(new RackConfiguration(PowerPlatform.SC, Instrumentation.NFI, RpduConfiguration.WYE),
		        new ScPlatformProcessor(new ScNfiFactory(), env, alertService));
		platformProcessors.put(new RackConfiguration(PowerPlatform.SC, Instrumentation.FI, RpduConfiguration.DELTA),
		        new ScPlatformProcessor(new DeltaPOFactory(new ScFiFactory()), env, alertService));
		platformProcessors.put(new RackConfiguration(PowerPlatform.SC, Instrumentation.NFI, RpduConfiguration.DELTA),
		        new ScPlatformProcessor(new DeltaPOFactory(new ScNfiFactory()), env, alertService));
		return platformProcessors;
	}
}
