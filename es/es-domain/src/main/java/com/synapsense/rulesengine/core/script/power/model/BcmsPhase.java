package com.synapsense.rulesengine.core.script.power.model;

import com.synapsense.dto.CollectionTO;
import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;
import com.synapsense.service.Environment;

/**
 * The class implements phase parameters computation in case the corresponding
 * rack is instrumented by BCMS. Current and power values are obtained using
 * circuit object properties.
 * 
 * @author Dmitry Grudzinskiy
 * 
 */
public class BcmsPhase extends Phase {

	private static final String POWER = "power";
	private static final String CURRENT = "current";
	private static final String POWERFACTOR = "powerFactor";

	private TO<?> circuit;

	public BcmsPhase(CollectionTO objProps, Rpdu parent) {
		super(objProps, parent);
		this.parent = parent;
		ValueTO cVal = objProps.getSinglePropValue(PowerConstants.CIRCUIT);
		if (cVal != null) {
			Object val = cVal.getValue();
			if (val != null) {
				circuit = (TO<?>) val;
			}
		}
	}

	@Override
	public void process(Environment env) throws Exception {
		if (circuit != null) {
			// No need to do anything
			super.process(env);
		}
	}

	@Override
	protected void calculate(Environment env) throws Exception {
		maxCurrent = avgCurrent = getModbusPropertyVal(env, CURRENT);
		demandPower = getModbusPropertyVal(env, POWER);
		if (isValid(demandPower)) {
			Double powerFactor = getModbusPropertyVal(env, POWERFACTOR);
			if (isValid(powerFactor)) {
				apPower = maxApPower = demandPower / powerFactor;
			}
		}
		processHistoricalMaxCurrent();
		processHistoricalMaxPower();
	}

	@Override
	protected void save(Environment env) throws Exception {
		saveCalculated(env);
		saveLastResetTime(env);
	}

	private Double getModbusPropertyVal(Environment env, String name) throws Exception {
		TO<?> modbusProp = env.getPropertyValue(circuit, name, TO.class);
		if (modbusProp == null) {
			return null;
		}

		return env.getPropertyValue(modbusProp, "lastValue", Double.class);
	}
}
