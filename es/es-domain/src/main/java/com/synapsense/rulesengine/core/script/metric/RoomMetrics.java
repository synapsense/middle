package com.synapsense.rulesengine.core.script.metric;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.apache.log4j.Logger;

import com.synapsense.dto.TO;
import com.synapsense.rulesengine.core.environment.Property;
import com.synapsense.rulesengine.core.environment.RuleAction;
import com.synapsense.rulesengine.core.environment.RuleI;
import com.synapsense.service.Environment;
import com.synapsense.service.impl.dao.to.PropertyTO;

public class RoomMetrics implements RuleAction {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private final static Logger logger = Logger.getLogger(RoomMetrics.class);

	public Object run(final RuleI triggeredRule, final Property calculated) {
        if (logger.isDebugEnabled())
            logger.debug(triggeredRule.getName() + " is run");

		List<String> objectNames = Arrays.asList("CRAC", "RACK");

		String[][] metrics = { { "CRAC", "returnDp", "CRDP" }, { "CRAC", "supplyDp", "CSDP" },
		        { "CRAC", "cracDeltaT", "CDT" }, { "CRAC", "alr", "ALR" }, { "RACK", "cTop", "RCT" },
		        { "RACK", "coldDp", "RDP" }, { "RACK", "bpa", "BPA" }, { "RACK", "ra", "RA" },
		        { "RACK", "cTROfChange", "CTROfChange" } };

		try {
			Environment env = calculated.getEnvironment();
			TO<?> nativeTO = calculated.getDataSource().getHostObjectTO();
			for (String objName : objectNames) {
				Collection<TO<?>> objects = env.getRelatedObjects(nativeTO, objName, true);
				if ((objects != null) && (!objects.isEmpty())) {
					for (int i = 0; i < (metrics.length); i++) {
						if (metrics[i][0].equals(objName)) {
							String propName = metrics[i][1];
							String metricName = metrics[i][2];
                            if (logger.isDebugEnabled())
                                logger.debug("Rule '" + triggeredRule.getName() + ": calculating " + metricName + "...");

							int n = 0;
							Double sum = 0.0;
							Double min = 1000.0;
							Double max = -1000.0;

							// iterate over objects within the room
							for (TO<?> object : objects) {
								Double retVal = null;

								Object propObj = env.getPropertyValue(object, propName, Object.class);
								if ((propObj instanceof Double) || (propObj instanceof PropertyTO)) {
									retVal = env.getPropertyValue(object, propName, Double.class);
								} else if (propObj instanceof TO) {
									if (env.getPropertyValue((TO<?>) propObj, "status", Integer.class) == 1) {
										retVal = env.getPropertyValue((TO<?>) propObj, "lastValue", Double.class);
									}
								}

								// validate the property has a valid value
								if ((retVal != null) && (retVal > -1000)) {
									n++;
									sum += retVal;
									if (retVal > max) {
										max = retVal;
									}
									if (retVal < min) {
										min = retVal;
									}
								}
							}

                            if (logger.isDebugEnabled())
                                logger.debug("Rule '" + triggeredRule.getName() + ": number of objects included = " + n);

							if (n != 0) {
								// Save minimum value
								env.setPropertyValue(nativeTO, "min" + metricName, min);
                                if (logger.isDebugEnabled())
                                    logger.debug("Rule '" + triggeredRule.getName() + ": min " + metricName + " = " + min);

								// Save maximum value
								env.setPropertyValue(nativeTO, "max" + metricName, max);
                                if (logger.isDebugEnabled())
                                    logger.debug("Rule '" + triggeredRule.getName() + ": max " + metricName + " = " + max);

								// Calculate average
								Double avg = sum / n;
								// Save average value
								env.setPropertyValue(nativeTO, "avg" + metricName, avg);
                                if (logger.isDebugEnabled())
                                    logger.debug("Rule '" + triggeredRule.getName() + ": avg " + metricName + " = " + avg);
							} else {
								cleanUp(env, nativeTO, metricName);
							}
						}
					}
				} else {
                    if (logger.isDebugEnabled())
                        logger.debug("Rule '" + triggeredRule.getName() + ": room has no " + objName + "s configured.");
					for (int i = 0; i < (metrics.length); i++) {
						if (metrics[i][0].equals(objName)) {
							cleanUp(env, nativeTO, metrics[i][2]);
						}
					}
				}
			}

			return null;
		} catch (Exception e) {
			logger.error("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName() + "': error", e);
			return null;
		}
	}

	private void cleanUp(Environment env, TO<?> nativeTO, String metricName) {
		try {
			List<String> prefixes = Arrays.asList("avg", "min", "max");
			for (String prefix : prefixes) {
				// if unsuccessful reset all properties
				if (env.getPropertyValue(nativeTO, prefix + metricName, Double.class) != null) {
					env.setPropertyValue(nativeTO, prefix + metricName, null);
				}
			}
		} catch (Exception e) {
			logger.error("Room metrics rule failed to clean up.", e);
		}
	}

}