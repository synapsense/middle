package com.synapsense.rulesengine.core.script.metric;

import java.util.Collection;
import java.util.Date;

import org.apache.log4j.Logger;

import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;
import com.synapsense.rulesengine.core.environment.Property;
import com.synapsense.rulesengine.core.environment.RuleAction;
import com.synapsense.rulesengine.core.environment.RuleI;
import com.synapsense.service.Environment;

public class RoomRCI implements RuleAction {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final static Logger logger = Logger.getLogger(RoomRCI.class);

	private static final double rMax = 80.6;
	private static final double aMax = 90;
	private static final double maxDelta = 9.4; // aMax - rMax;
	private static final double rMin = 64.4;
	private static final double aMin = 59;
	private static final double minDelta = 5.4; // rMin - aMin;

	public Object run(final RuleI triggeredRule, final Property calculated) {
        if (logger.isDebugEnabled())
            logger.debug(triggeredRule.getName() + " is run");
		try {
			Double res = null;

			Environment env = calculated.getEnvironment();
			TO<?> nativeTO = calculated.getDataSource().getHostObjectTO();

			Integer n = 0; // total number of valid intakes
			Integer badIntakeCounterHi = 0; // number of intakes above allowable
											// limit
			Integer badIntakeCounterLo = 0; // number of intakes below allowable
											// limit
			Double sumDeltaHi = 0.0;
			Double sumDeltaLo = 0.0;

			Collection<TO<?>> racks = env.getRelatedObjects(nativeTO, "RACK", true);
			if ((racks != null) && (!racks.isEmpty())) {
				Date end = new Date();
				Date start = new Date(end.getTime() - 1800000); // subtract 30
																// minutes

				// iterate over racks within the room, collect the sums of inlet
				for (TO<?> rack : racks) {
					TO<?> cTopSensor = (TO<?>) env.getPropertyValue(rack, "cTop", Object.class);
					if (cTopSensor != null) {
						Collection<ValueTO> cTopValues = env.getHistory(cTopSensor, "lastValue", start, end,
						        Double.class);
						if (cTopValues.size() != 0) {
							Double avgCTop = calculateAverageValue(cTopValues);
							if (avgCTop != null) {
								// increment number of valid intakes
								n++;

								double deltaTHi = avgCTop - rMax;
								if (deltaTHi > 0) {
									sumDeltaHi += deltaTHi;
									if ((avgCTop - aMax) > 0) {
										badIntakeCounterHi++;
									}
								}
								double deltaTLo = rMin - avgCTop;
								if (deltaTLo > 0) {
									sumDeltaLo += deltaTLo;
									if ((aMin - avgCTop) > 0) {
										badIntakeCounterLo++;
									}
								}
							}
						}
					}

					TO<?> cMidSensor = (TO<?>) env.getPropertyValue(rack, "cMid", Object.class);
					if (cMidSensor != null) {
						Collection<ValueTO> cMidValues = env.getHistory(cMidSensor, "lastValue", start, end,
						        Double.class);
						if (cMidValues.size() != 0) {
							Double avgCMid = calculateAverageValue(cMidValues);
							if (avgCMid != null) {
								// increment number of valid intakes
								n++;
								double deltaTHi = avgCMid - rMax;
								if (deltaTHi > 0) {
									sumDeltaHi += deltaTHi;
									if ((avgCMid - aMax) > 0) {
										badIntakeCounterHi++;
									}
								}
								double deltaTLo = rMin - avgCMid;
								if (deltaTLo > 0) {
									sumDeltaLo += deltaTLo;
									if ((aMin - avgCMid) > 0) {
										badIntakeCounterLo++;
									}
								}
							}
						}
					}

					TO<?> cBotSensor = (TO<?>) env.getPropertyValue(rack, "cBot", Object.class);
					if (cBotSensor != null) {
						Collection<ValueTO> cBotValues = env.getHistory(cBotSensor, "lastValue", start, end,
						        Double.class);
						if (cBotValues.size() != 0) {
							Double avgCBot = calculateAverageValue(cBotValues);
							if (avgCBot != null) {
								// increment number of valid intakes
								n++;
								double deltaTHi = avgCBot - rMax;
								if (deltaTHi > 0) {
									sumDeltaHi += deltaTHi;
									if ((avgCBot - aMax) > 0) {
										badIntakeCounterHi++;
									}
								}
								double deltaTLo = rMin - avgCBot;
								if (deltaTLo > 0) {
									sumDeltaLo += deltaTLo;
									if ((aMin - avgCBot) > 0) {
										badIntakeCounterLo++;
									}
								}
							}
						}
					}
				}

				if (n != 0) {
					// Save Bad Intake Counters
					env.setPropertyValue(nativeTO, "bich", badIntakeCounterHi);
					env.setPropertyValue(nativeTO, "bicl", badIntakeCounterLo);
					// Save Total number of valid equipment intakes
					env.setPropertyValue(nativeTO, "rcin", n);
					// Save sum of hi deltas
					env.setPropertyValue(nativeTO, "totalOver", sumDeltaHi);
					// Save sum of low deltas
					env.setPropertyValue(nativeTO, "totalUnder", sumDeltaLo);
					// Save rcihi value
					env.setPropertyValue(nativeTO, "rcihi", ((1 - (sumDeltaHi / (maxDelta * n))) * 100));

					res = (1 - (sumDeltaLo / (minDelta * n))) * 100;
				} else {
					if (logger.isDebugEnabled())
						logger.debug("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName()
						        + "': no valid intakes found.");
				}
			} else {
                if (logger.isDebugEnabled())
                    logger.debug("No Racks are found in this room : " + nativeTO);
			}

			// if unsuccessful reset all properties
			if (res == null) {
				if (env.getPropertyValue(nativeTO, "bich", Integer.class) != null) {
					env.setPropertyValue(nativeTO, "bich", null);
				}
				if (env.getPropertyValue(nativeTO, "bicl", Integer.class) != null) {
					env.setPropertyValue(nativeTO, "bicl", null);
				}
				if (env.getPropertyValue(nativeTO, "rcin", Integer.class) != new Integer(0)) {
					env.setPropertyValue(nativeTO, "rcin", 0);
				}
				if (env.getPropertyValue(nativeTO, "totalOver", Double.class) != null) {
					env.setPropertyValue(nativeTO, "totalOver", null);
				}
				if (env.getPropertyValue(nativeTO, "totalUnder", Double.class) != null) {
					env.setPropertyValue(nativeTO, "totalUnder", null);
				}
				if (env.getPropertyValue(nativeTO, "rcihi", Double.class) != null) {
					env.setPropertyValue(nativeTO, "rcihi", null);
				}
			}

            if (logger.isDebugEnabled())
                logger.debug(triggeredRule.getName() + " is finished");
			return res;
		} catch (Exception e) {
			logger.error("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName() + "': error", e);
			return null;
		}
	}

	private Double calculateAverageValue(Collection<ValueTO> values) {
		Double res = 0.0;
		int numberOfValidValues = 0;

		for (ValueTO dbl : values) {
			Double value = (Double) dbl.getValue();

			// validate that sensor value is not an error code
			if ((value != null) && (value > -1000)) {
				res += value;
				numberOfValidValues++;
			}
		}

		if (numberOfValidValues > 0) {
			res = res / numberOfValidValues;
		} else {
			res = null;
		}

		return res;
	}
}