package com.synapsense.rulesengine.core.script.power.model;

import static com.synapsense.rulesengine.core.script.power.model.PowerConstants.AP_POWER;
import static com.synapsense.rulesengine.core.script.power.model.PowerConstants.DEMAND_POWER;
import static com.synapsense.rulesengine.core.script.power.model.PowerConstants.MAX_AP_POWER;
import static com.synapsense.rulesengine.core.script.power.model.PowerConstants.MAX_HIST_AP_POWER;
import static com.synapsense.rulesengine.core.script.power.model.PowerConstants.POTENTIAL_MAX_AP_POWER;

import java.util.ArrayList;
import java.util.List;

import com.synapsense.dto.CollectionTO;
import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;
import com.synapsense.service.Environment;

/**
 * An abstract class that implements the common algorithms used by derived power
 * objects. Each derived class has to implement
 * {@link PowerObject#process(Environment)} method for correct calculation of
 * its parameters.
 * 
 * @author Dmitry Grudzinskiy
 */
public abstract class PowerObject {

	protected Double apPower = null;
	protected Double maxApPower = null;
	protected Double maxHistApPower = null;
	protected Double demandPower = null;
	protected Double potentialMaxApPower = null;

	private long resetTime = 0;

	protected TO<?> objId;

	public PowerObject(TO<?> objId, long resetTime) {
		this.objId = objId;
		this.resetTime = resetTime;
	}

	public PowerObject(CollectionTO objProps, long resetTime) {
		this(objProps.getObjId(), resetTime);
		assignValues(objProps);
	}

	public PowerObject(CollectionTO objProps) {
		this(objProps, 0);
	}

	public PowerObject(TO<?> objId) {
		this(objId, 0);
	}

	public TO<?> getObjId() {
		return objId;
	}

	public abstract void process(Environment env) throws Exception;

	public long getResetTime() {
		return resetTime;
	}

	public void setResetTime(long resetTime) {
		this.resetTime = resetTime;
	}

	protected void addApPower(Double apPower) {
		this.apPower = Util.addValue(this.apPower, apPower);
	}

	protected void addMaxApPower(Double maxApPower) {
		this.maxApPower = Util.addValue(this.maxApPower, maxApPower);
	}

	protected void addDemandPower(Double demandPower) {
		this.demandPower = Util.addValue(this.demandPower, demandPower);
	}

	protected void addPotentialMaxApPower(Double potentialMaxApPower) {
		this.potentialMaxApPower = Util.addValue(this.potentialMaxApPower, potentialMaxApPower);
	}

	@Override
	public String toString() {
		return objId.toString();
	}

	protected void processHistoricalMaxPower() {
		if (isValid(maxApPower)) {
			if (maxHistApPower == null || maxApPower > maxHistApPower || getResetTime() > 0) {
				maxHistApPower = maxApPower;
			}
		}
	}
	
	protected void aggregateParent(PowerObject parent) {
		parent.addApPower(apPower);
		parent.addDemandPower(demandPower);
		parent.addMaxApPower(maxApPower);
	}

	protected void addValueTO(List<ValueTO> values, String propName, Double value) {
		if (isValid(value)) {
			values.add(new ValueTO(propName, value));
		} else {
			values.add(new ValueTO(propName, null));
		}
	}

	protected List<ValueTO> createDefaultValueList() {
		List<ValueTO> values = new ArrayList<ValueTO>();
		addValueTO(values, AP_POWER, apPower);
		addValueTO(values, MAX_AP_POWER, maxApPower);
		addValueTO(values, MAX_HIST_AP_POWER, maxHistApPower);
		return values;
	}

	protected List<ValueTO> createCalculatedValuesList() {
		List<ValueTO> values = createDefaultValueList();
		addValueTO(values, DEMAND_POWER, demandPower);
		return values;
	}

	protected void saveCalculated(Environment env) throws Exception {
		saveValueList(createCalculatedValuesList(), env);
	}

	protected void saveAll(Environment env) throws Exception {
		List<ValueTO> values = createCalculatedValuesList();
		addValueTO(values, POTENTIAL_MAX_AP_POWER, potentialMaxApPower);
		saveValueList(values, env);
	}

	protected void saveNulls(Environment env) throws Exception {
		List<ValueTO> values = new ArrayList<ValueTO>();
		addValueTO(values, AP_POWER, null);
		addValueTO(values, MAX_AP_POWER, null);
		addValueTO(values, MAX_HIST_AP_POWER, null);
		addValueTO(values, DEMAND_POWER, null);
		addValueTO(values, POTENTIAL_MAX_AP_POWER, null);
		saveValueList(values, env);
	}

	protected void saveValueList(List<ValueTO> values, Environment env) throws Exception {
		if (!values.isEmpty()) {
			env.setAllPropertiesValues(objId, values.toArray(new ValueTO[values.size()]));
		}
	}

	protected void saveLastResetTime(Environment env) throws Exception {
		if (resetTime > 0) {
			env.setPropertyValue(objId, "lastResetTime", resetTime);
		}
	}

	protected boolean isValid(Double value) {
		if (value != null && value >= 0) {
			return true;
		}
		return false;
	}

	protected void assignValues(CollectionTO objProps) {
		demandPower = Util.getDoubleValue(objProps, DEMAND_POWER);
		maxHistApPower = Util.getDoubleValue(objProps, MAX_HIST_AP_POWER);
	}
}
