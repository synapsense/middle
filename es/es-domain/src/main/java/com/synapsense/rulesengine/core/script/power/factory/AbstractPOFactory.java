package com.synapsense.rulesengine.core.script.power.factory;

import java.util.Collection;
import java.util.Map;

import com.synapsense.dto.CollectionTO;
import com.synapsense.dto.TO;
import com.synapsense.rulesengine.core.script.power.model.FaceplateServer;
import com.synapsense.rulesengine.core.script.power.model.Phase;
import com.synapsense.rulesengine.core.script.power.model.Plug;
import com.synapsense.rulesengine.core.script.power.model.PlugServer;
import com.synapsense.rulesengine.core.script.power.model.PowerConstants;
import com.synapsense.rulesengine.core.script.power.model.PowerObject;
import com.synapsense.rulesengine.core.script.power.model.PowerRack;

public abstract class AbstractPOFactory extends AbstractPowerSourceFactory implements ServerPlugFactory {

	@Override
	public Map<TO<?>, PowerObject> createServers(final PowerRack rack) throws Exception {
		return new CollCreator<PowerObject>(rack.getObjId(), PowerConstants.SERVER, PowerConstants.HIST_PROPS)
		        .createMap(new ElCreator<PowerObject>() {
			        public PowerObject create(CollectionTO collTo) throws Exception {
				        int plugNb = env.getChildren(collTo.getObjId(), PowerConstants.PLUG).size();
				        if (plugNb > 0) {
					        return new PlugServer(collTo, rack);
				        }
				        return createFaceplateServer(collTo.getObjId(), rack);
			        }
		        });
	}

	@Override
	public Collection<Plug> createPlugs(final Phase phase, final Map<TO<?>, PowerObject> serverMap) throws Exception {
		return new CollCreator<Plug>(phase.getObjId(), PowerConstants.PLUG, PowerConstants.PL_CALC_PROPS)
		        .createCollection(new ElCreator<Plug>() {
			        public Plug create(CollectionTO collTo) throws Exception {
				        TO<?> server = env.getParents(collTo.getObjId(), PowerConstants.SERVER).iterator().next();
				        return new Plug(collTo, phase, serverMap.get(server));
			        }
		        });
	}

	@Override
	protected String[] getPhaseProperties() {
		return PowerConstants.PH_CALC_PROPS;
	}

	protected FaceplateServer createFaceplateServer(TO<?> objId, PowerRack parent) {
		return new FaceplateServer(objId, parent);
	}
}
