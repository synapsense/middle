package com.synapsense.rulesengine.core.script.metric;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.dto.TO;
import com.synapsense.exception.EnvException;
import com.synapsense.rulesengine.core.environment.Property;
import com.synapsense.rulesengine.core.environment.RuleAction;
import com.synapsense.rulesengine.core.environment.RuleI;
import com.synapsense.rulesengine.core.script.energy.EmissionEstimator;
import com.synapsense.rulesengine.core.script.energy.EnergyUtil;
import com.synapsense.rulesengine.core.script.energy.Estimator;
import com.synapsense.rulesengine.core.script.energy.ReductionCalculator;
import com.synapsense.service.Environment;

/**
 * This rule calculated projected annual carbon emission reduction.
 * 
 * @author Dmitry Grudzinskiy
 * 
 */
public class EstEmissionReduction implements RuleAction {

	private static final long serialVersionUID = -5360864648797418171L;

	private static Log log = LogFactory.getLog(EstEmissionReduction.class);

	private Property itLoadTarget;

	private Property pueTarget;

	private Property emission;

	@Override
	public Object run(RuleI triggeredRule, Property calculated) {
		TO<?> pue = calculated.getDataSource().getHostObjectTO();
		Environment env = calculated.getEnvironment();
        if (log.isDebugEnabled())
            log.debug("Calculating estimated annual emission reduction for " + pue);
		try {
			Double emissionFactor = env.getPropertyValue(pue, EnergyUtil.EMISSION_FACTOR, Double.class);
			Estimator estimator = new EmissionEstimator(emissionFactor);
			return ReductionCalculator.getInstance().calculateReduction((Double) pueTarget.getValue(),
			        (Double) itLoadTarget.getValue(), (Double) emission.getValue(), estimator, EnergyUtil.EMISSION,
			        pue, env);
		} catch (EnvException e) {
			log.error(e.getMessage(), e);
			return null;
		}
	}

	public Property getItLoadTarget() {
		return itLoadTarget;
	}

	public void setItLoadTarget(Property itLoadTarget) {
		this.itLoadTarget = itLoadTarget;
	}

	public Property getPueTarget() {
		return pueTarget;
	}

	public void setPueTarget(Property pueTarget) {
		this.pueTarget = pueTarget;
	}

	public Property getEmission() {
		return emission;
	}

	public void setEmission(Property emission) {
		this.emission = emission;
	}
}
