package com.synapsense.rulesengine.core.script.power.model;

import com.synapsense.service.Environment;

/**
 * The class implements phase currents conversion to WYE in case corresponding
 * RPDU is DELTA.
 * 
 * @author Dmitry Grudzinskiy
 * 
 */
public class DeltaPhase extends Phase {

	private Phase leg1;
	private Phase leg2;

	private double voltageFactor;

	private static Double convertCurrent(Double cur1, Double cur2) {
		if (cur1 == null) {
			cur1 = 0D;
		}
		if (cur2 == null) {
			cur2 = 0D;
		}
		return Math.sqrt(cur1 * cur1 + cur2 * cur2 + cur1 * cur2);
	}

	public DeltaPhase(Phase leg1, Phase leg2, double voltageFactor) {
		super(leg1);
		this.leg1 = leg1;
		this.leg2 = leg2;
		this.voltageFactor = voltageFactor;
	}

	public void addAvgCurrent(Double avgCurrent) {
		// Adding values to delta instead of default
		leg1.addDeltaAvgCurrent(avgCurrent);
	}

	public void addMaxCurrent(Double maxCurrent) {
		// Adding values to delta instead of default
		leg1.addDeltaMaxCurrent(maxCurrent);
	}

	public void addPotentialMaxCurrent(Double potentialMaxCurrent) {
		// Adding values to delta instead of default
		leg1.addDeltaPotentialMaxCurrent(potentialMaxCurrent);
	}

	@Override
	protected void calculate(Environment env) throws Exception {
		leg1.avgCurrent = convertCurrent(leg1.deltaAvgCurrent, leg2 == null ? null : leg2.deltaAvgCurrent);
		leg1.maxCurrent = convertCurrent(leg1.deltaMaxCurrent, leg2 == null ? null : leg2.deltaMaxCurrent);
		leg1.potentialMaxCurrent = convertCurrent(leg1.deltaPotentialMaxCurrent, leg2 == null ? null
		        : leg2.deltaPotentialMaxCurrent);

		// Convert it temporary to wye so that we can get wye power
		if (leg1.voltage != null && leg2 != null && leg2.voltage != null) {
			leg1.voltage = this.voltage / voltageFactor;
		} else if (leg1.voltage != null) {
			leg1.voltage = leg1.voltage / voltageFactor;
		} else if (leg2 != null && leg2.voltage != null) {
			leg1.voltage = leg2.voltage / voltageFactor;
		}
		leg1.calculate(env);
		leg1.voltage = this.voltage;
	}

	// Delegates

	public void addApPower(Double apPower) {
		leg1.addApPower(apPower);
	}

	public void addMaxApPower(Double maxApPower) {
		leg1.addMaxApPower(maxApPower);
	}

	public void addDemandPower(Double demandPower) {
		leg1.addDemandPower(demandPower);
	}

	public void addPotentialMaxApPower(Double potentialMaxApPower) {
		leg1.addPotentialMaxApPower(potentialMaxApPower);
	}

	public long getResetTime() {
		return leg1.getResetTime();
	}

	@Override
	protected void save(Environment env) throws Exception {
		leg1.save(env);
	}

	@Override
	protected void aggregateParent(PowerObject parent) {
		leg1.aggregateParent(parent);
	}
}
