package com.synapsense.rulesengine.core.script.power.model;

public final class PowerConstants {

	public static final String AVG_CURRENT = "avgCurrent";
	public static final String MAX_CURRENT = "maxCurrent";
	public static final String DELTA_AVG_CURRENT = "deltaAvgCurrent";
	public static final String DELTA_MAX_CURRENT = "deltaMaxCurrent";
	public static final String MAX_HIST_CURRENT = "maxHistCurrent";
	public static final String POTENTIAL_MAX_CURRENT = "potentialMaxCurrent";
	public static final String DEMAND_POWER = "demandPower";
	public static final String AP_POWER = "apPower";
	public static final String MAX_AP_POWER = "maxApPower";
	public static final String MAX_HIST_AP_POWER = "maxHistApPower";
	public static final String POTENTIAL_MAX_AP_POWER = "potentialMaxApPower";
	public static final String VOLTAGE = "voltage";
	public static final String STATUS = "status";
	public static final String UNBAL_PERC = "unbalancedPercentage";
	public static final String PH_OUTOFBAL = "phaseOutOfBalance";
	public static final String USE_FACEPLATE = "useFaceplate";
	public static final String FP_CURRENT = "fpCurrent";
	public static final String FP_APPOWER = "fpApPower";
	public static final String FP_DEMANDPOWER = "fpDemandPower";
	public static final String RPDU_TYPE = "type";
	public static final String RPDU_STATUS = "status";
	public static final String PHASE_LABEL = "name";
	public static final String CIRCUIT = "circuit";
	public static final String NOMINAL_VOLTAGE = "nominalVoltage";
	public static final String PLATFORM = "platform";
	public static final String INSTRUMENTATION = "instrumentation";
	public static final String RESET_DAYS = "resetDays";
	public static final String LAST_RESET_TIME = "lastResetTime";
	public static final String QUORUM = "quorum";
	public static final String PLUG_ID = "pId";
	public static final String POWER_FACTOR="powerFactor";

	public static final int STATUS_NOT_REPORTING = 0;
	public static final int STATUS_OK = 1;
	public static final int STATUS_DISABLED = 2;

	public static final String DC = "DC";
	public static final String RPDU = "RPDU";
	public static final String PHASE = "PHASE";
	public static final String PLUG = "PLUG";
	public static final String SERVER = "SERVER";

	public static final long SAMPLE_INTERVAL = 15 * 60 * 1000; // 15 minutes

	public static final int DELTA = 1;
	public static final int WYE = 0;

	public static final String[] PL_CALC_PROPS = new String[] { AVG_CURRENT, MAX_CURRENT, MAX_HIST_CURRENT,
	        MAX_HIST_AP_POWER, DEMAND_POWER };

	public static final String[] PH_CALC_PROPS = new String[] { MAX_HIST_CURRENT, MAX_HIST_AP_POWER, VOLTAGE,
	        PHASE_LABEL };

	public static final String[] PH_BCMS_PROPS = new String[] { MAX_HIST_CURRENT, MAX_HIST_AP_POWER, CIRCUIT,
	        PHASE_LABEL, VOLTAGE };

	public static final String[] PH_DUO_PROPS = new String[] { AVG_CURRENT, MAX_CURRENT, DELTA_AVG_CURRENT,
	        DELTA_MAX_CURRENT, MAX_HIST_CURRENT, DEMAND_POWER, MAX_HIST_AP_POWER, PHASE_LABEL, VOLTAGE };

	public static final String[] HIST_PROPS = new String[] { MAX_HIST_CURRENT, MAX_HIST_AP_POWER };

	public static final String[] POWER_RACK_PROPS = new String[] { MAX_HIST_AP_POWER, PLATFORM,
	        INSTRUMENTATION, NOMINAL_VOLTAGE, QUORUM };

	public static final String[] RPDU_PROPS = new String[] { MAX_HIST_AP_POWER, RPDU_TYPE };

	private PowerConstants() {
	}
}
