package com.synapsense.rulesengine.core.script.metric;

import java.util.Collection;

import org.apache.log4j.Logger;

import com.synapsense.dto.TO;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.exception.PropertyNotFoundException;
import com.synapsense.exception.UnableToConvertPropertyException;
import com.synapsense.rulesengine.core.environment.Property;
import com.synapsense.rulesengine.core.environment.RuleAction;
import com.synapsense.rulesengine.core.environment.RuleI;
import com.synapsense.service.Environment;
import com.synapsense.service.impl.dao.to.PropertyTO;

public class CRACALR implements RuleAction {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private final static Logger logger = Logger.getLogger(CRACALR.class);

	public Object run(final RuleI triggeredRule, final Property calculated) {
        if (logger.isDebugEnabled())
            logger.debug(triggeredRule.getName() + " is run");
		try {
			double res = 0;

			TO<?> nativeTO = calculated.getDataSource().getHostObjectTO();
			Environment env = calculated.getEnvironment();

			if (env.getPropertyValue(nativeTO, "status", Integer.class) != 1) {
				logger.debug("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName()
				        + "': object is offline. Skipping...");

				// Set CRAC deltaT to indicate an error
				env.setPropertyValue(nativeTO, "cracDeltaT", null);

				return null;
			}

			Double SATVal = getValue(env, nativeTO, "supplyT");
			Double RATVal = getValue(env, nativeTO, "returnT");

			if ((SATVal == null) || (RATVal == null)) {
				if (logger.isDebugEnabled())
					logger.debug("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName()
					        + "': CRAC supply or return is invalid.");

				// Set CRAC deltaT to indicate an error
				env.setPropertyValue(nativeTO, "cracDeltaT", null);

				return null;
			}

			Double deltaC = RATVal - SATVal;

			// Save CRAC deltaT
			env.setPropertyValue(nativeTO, "cracDeltaT", deltaC);

			// get parent rooms
			Collection<TO<?>> rooms = env.getParents(nativeTO, "ROOM");
			if (rooms == null) {
				throw new IllegalStateException("Property '" + calculated.getDataSource().getNativePropertyName()
				        + "' has no parent room.");
			}

			double sumAvgInlet = 0.0, sumAvgDischarge = 0.0;
			byte numInletValues = 0, numDischargeValues = 0;

			for (TO<?> room : rooms) {
				Collection<TO<?>> racks = env.getRelatedObjects(room, "RACK", true);
				if (racks == null || racks.isEmpty()) {
					continue;
				} else {
					Double averageInlet = env.getPropertyValue(room, "avgRI", Double.class);
					if (averageInlet != null) {
						sumAvgInlet += averageInlet;
						numInletValues++;
					}

					Double averageDischarge = env.getPropertyValue(room, "avgRD", Double.class);
					if (averageDischarge != null) {
						sumAvgDischarge += averageDischarge;
						numDischargeValues++;
					}
				}
			}

			if ((numInletValues == 0) || (numDischargeValues == 0)) {
				if (logger.isDebugEnabled())
					logger.debug("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName()
					        + "': can't obtain avg racks inlet or dicharge.");
				return null;
			}

			Double averageInlet = sumAvgInlet / numInletValues;
			Double averageDischarge = sumAvgDischarge / numDischargeValues;

			Double deltaR = averageDischarge - averageInlet;

			if (deltaR == 0) {
                if (logger.isDebugEnabled())
                    logger.debug("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName()
				        + "': Average temperature raise across all Racks is equal to 0!");
				return null;
			} else if (deltaC <= 0) {
                if (logger.isDebugEnabled())
				    logger.debug("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName()
				            + "': CRAC is heating the room!");
				res = 100;
			} else if (deltaC > deltaR) {
				res = 0;
			} else {
				res = ((deltaR - deltaC) / deltaR) * 100;
			}

            if (logger.isDebugEnabled())
                logger.debug(triggeredRule.getName() + " is finished");
			return res;
		} catch (Exception e) {
			logger.error("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName() + "': error", e);
			return null;
		}
	}

	private Double getValue(Environment env, TO<?> obj, String propertyName) throws ObjectNotFoundException,
	        PropertyNotFoundException, UnableToConvertPropertyException {
		Double value = null;

		Object propObj = env.getPropertyValue(obj, propertyName, Object.class);
		// support direct values or links
		if (propObj == null) {
			return null;
		} else if (propObj instanceof TO) {
			value = env.getPropertyValue(((TO<?>) propObj), "lastValue", Double.class);
		} else if (propObj instanceof Double) {
			value = (Double) propObj;
		} else if (propObj instanceof PropertyTO) {
			value = env.getPropertyValue(((PropertyTO) propObj).getObjId(), ((PropertyTO) propObj).getPropertyName(),
			        Double.class);
		} else {
			throw new IllegalArgumentException("Unsupported type of object: "
			        + ((propObj == null) ? "null" : propObj.getClass().getCanonicalName()));
		}

		// validate sensor data
		if ((value != null) && (value > -1000)) {
			return value;
		}

		return null;
	}
}