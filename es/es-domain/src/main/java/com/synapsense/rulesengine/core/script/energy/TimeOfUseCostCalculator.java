package com.synapsense.rulesengine.core.script.energy;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Locale;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

/**
 * Cost calculator implementation in case costs are defined as a 'time of use'
 * table.
 * 
 * @author Dmitry Grudzinskiy
 * 
 */
public class TimeOfUseCostCalculator implements CostCalculator {

	private static final String TOKEN_BEGINMONTH = "beginmonth";
	private static final String TOKEN_ENDMONTH = "endmonth";
	private static final String TOKEN_TYPEOFDAY = "typeofday";
	private static final String TOKEN_BEGIN = "begin";
	private static final String TOKEN_END = "end";
	private static final String TOKEN_COST = "cost";
	private static final String TOKEN_TIMELINE = "timeline";

	private static final HashMap<String, Integer> daysMap = new HashMap<String, Integer>();

	static {
		daysMap.put("weekdays", TouCostPeriod.WEEKDAYS);
		daysMap.put("weekends", TouCostPeriod.WEEKENDS);
		daysMap.put("all", TouCostPeriod.ALL);
	}

	private ArrayList<TouCostPeriod> periods = new ArrayList<TouCostPeriod>();

	@Override
	public double calculateRt(double prevEnergyUsage, double prevEnergyCost, double energyUsage, long timestamp) {
		for (TouCostPeriod period : periods) {
			if (period.inPeriod(timestamp)) {
				return prevEnergyCost + energyUsage * period.getCost(timestamp);
			}
		}
		throw new IllegalArgumentException("Time of use period is not found for " + timestamp);
	}

	@Override
	public double estimateAnnual(int year, double power) {
		double costPerOne = 0;
		for (TouCostPeriod period : periods) {
			costPerOne += period.getCostSummary(year);
		}
		return power * costPerOne;
	}

	@Override
	public double estimateMonthly(int year, int month, double power) {
		double costPerOne = 0.0;
		for (TouCostPeriod period : periods) {
			if (period.inPeriod(month)) {
				costPerOne += period.getMonthCostSummary(year, month);
			}
		}
		return power * costPerOne;
	}

	public TimeOfUseCostCalculator addCostPeriod(TouCostPeriod period) {
		periods.add(period);
		return this;
	}

	public Collection<TouCostPeriod> getPeriods() {
		return Collections.unmodifiableCollection(periods);
	}

	public static TimeOfUseCostCalculator load(String src) {
		TimeOfUseCostCalculator calculator = new TimeOfUseCostCalculator();
		try {
			JSONArray periodArray = new JSONArray(src);
			for (int i = 0; i < periodArray.length(); i++) {
				JSONObject jsPeriod = periodArray.getJSONObject(i);

				int beginMonth = getMonth(jsPeriod.getString(TOKEN_BEGINMONTH));
				int endMonth = getMonth(jsPeriod.getString(TOKEN_ENDMONTH));
				int typeOfDay = getTypeOfDay(jsPeriod.getString(TOKEN_TYPEOFDAY));
				TouCostPeriod period = new TouCostPeriod(beginMonth, endMonth, typeOfDay);
				calculator.addCostPeriod(period);

				JSONArray timeArray = jsPeriod.getJSONArray(TOKEN_TIMELINE);

				for (int j = 0; j < timeArray.length(); j++) {
					JSONObject jsTime = timeArray.getJSONObject(j);
					double cost = jsTime.getDouble(TOKEN_COST);
					String[] h1m1 = jsTime.getString(TOKEN_BEGIN).split(":");
					String[] h2m2 = jsTime.getString(TOKEN_END).split(":");
					period.addTime(Integer.valueOf(h1m1[0]), Integer.valueOf(h1m1[1]), Integer.valueOf(h2m2[0]),
					        Integer.valueOf(h2m2[1]), cost);
				}
			}
		} catch (JSONException e) {
			throw new ParseCostTableException("Unable to parse time of use table", e);
		}
		return calculator;
	}

	private static int getTypeOfDay(String token) {
		Integer type = daysMap.get(token);
		if (type == null) {
			throw new ParseCostTableException("Unknown type of day " + token);
		}
		return type;
	}

	private static int getMonth(String token) {
		SimpleDateFormat monthFormat = new SimpleDateFormat("MMM", Locale.US);
		Calendar cal = Calendar.getInstance();
		try {
			cal.setTime(monthFormat.parse(token));
		} catch (ParseException e) {
			throw new ParseCostTableException("Unable to parse month " + token, e);
		}
		return cal.get(Calendar.MONTH);
	}
}
