package com.synapsense.rulesengine.core.script.metric;

import java.util.Collection;
import java.util.Date;

import org.apache.log4j.Logger;

import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;
import com.synapsense.rulesengine.core.environment.Property;
import com.synapsense.rulesengine.core.environment.RuleAction;
import com.synapsense.rulesengine.core.environment.RuleI;
import com.synapsense.service.Environment;

public class RoomRTI implements RuleAction {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final static Logger logger = Logger.getLogger(RoomRTI.class);

	public Object run(final RuleI triggeredRule, final Property calculated) {
        if (logger.isDebugEnabled())
            logger.debug(triggeredRule.getName() + " is run");
		try {
			double res = 0;

			Environment env = calculated.getEnvironment();
			TO<?> roomTO = calculated.getDataSource().getHostObjectTO();

			Date end = new Date();
			Date start = new Date(end.getTime() - 1800000); // subtract 30
															// minutes

			Collection<ValueTO> ariValues = env.getHistory(roomTO, "avgRI", start, end, Double.class);
			Collection<ValueTO> ardValues = env.getHistory(roomTO, "avgRD", start, end, Double.class);
			Collection<ValueTO> acsValues = env.getHistory(roomTO, "avgCS", start, end, Double.class);
			Collection<ValueTO> acrValues = env.getHistory(roomTO, "avgCR", start, end, Double.class);

			if ((ariValues.size() != 0) && (ardValues.size() != 0) && (acsValues.size() != 0)
			        && (acrValues.size() != 0)) {
				Double average_inlet = calculateAverageValue(ariValues);
				Double average_discharge = calculateAverageValue(ardValues);
				Double average_supply = calculateAverageValue(acsValues);
				Double average_return = calculateAverageValue(acrValues);

				if ((average_discharge - average_inlet) > 0) {
					res = (average_return - average_supply) / (average_discharge - average_inlet);

					// Also calculate 1/RTI and save it
					if (res != 0) {
						env.setPropertyValue(roomTO, "irti", ((1 / res) * 100));
					}

					// Convert RTI value to percents
					res = res * 100;
				} else {
                    if (logger.isDebugEnabled())
				    	logger.debug(triggeredRule.getName() + ": deltaT is negative or no Racks in a room.");
					return null;
				}
			} else {
				if (logger.isDebugEnabled())
					logger.debug(triggeredRule.getName() + ": no data available");
				return null;
			}

            if (logger.isDebugEnabled())
                logger.debug(triggeredRule.getName() + " is finished");
			return res;
		} catch (Exception e) {
			logger.error("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName() + "': error", e);
			return null;
		}
	}

	private Double calculateAverageValue(Collection<ValueTO> values) {
		Double res = 0.0;

		for (ValueTO dbl : values) {
			res += (Double) dbl.getValue();
		}

		res = res / values.size();
		;

		return res;
	}
}