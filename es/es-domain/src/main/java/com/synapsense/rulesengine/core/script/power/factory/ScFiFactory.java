package com.synapsense.rulesengine.core.script.power.factory;

import com.synapsense.dto.CollectionTO;
import com.synapsense.rulesengine.core.script.power.model.Phase;
import com.synapsense.rulesengine.core.script.power.model.PlugPhase;
import com.synapsense.rulesengine.core.script.power.model.PlugRpdu;
import com.synapsense.rulesengine.core.script.power.model.PowerRack;
import com.synapsense.rulesengine.core.script.power.model.Rpdu;

/**
 * An implementation of {@link PowerSourceFactory}, {@link ServerPlugFactory}
 * that creates SC power objects in case the rack is fully instrumented by
 * jawas.
 * 
 * @author Dmitry Grudzinskiy
 * 
 */
public class ScFiFactory extends AbstractPOFactory {

	@Override
	protected Rpdu createRpdu(CollectionTO collTo, PowerRack parent) {
		return new PlugRpdu(collTo, parent);
	}

	@Override
	protected Phase createPhase(CollectionTO collTo, Rpdu parent) {
		return new PlugPhase(collTo, parent);
	}
}
