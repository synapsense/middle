package com.synapsense.rulesengine.core.script.power.factory;

import com.synapsense.dto.CollectionTO;
import com.synapsense.rulesengine.core.script.power.model.NfiPlugPhase;
import com.synapsense.rulesengine.core.script.power.model.NfiPlugRpdu;
import com.synapsense.rulesengine.core.script.power.model.Phase;
import com.synapsense.rulesengine.core.script.power.model.PowerRack;
import com.synapsense.rulesengine.core.script.power.model.Rpdu;

/**
 * An implementation of {@link PowerSourceFactory}, {@link ServerPlugFactory}
 * that creates SC power objects in case the rack has faceplate servers.
 * 
 * @author Dmitry Grudzinskiy
 * 
 */
public class ScNfiFactory extends AbstractPOFactory {

	@Override
	protected Rpdu createRpdu(CollectionTO collTo, PowerRack parent) {
		return new NfiPlugRpdu(collTo, parent);
	}

	@Override
	protected Phase createPhase(CollectionTO collTo, Rpdu parent) {
		return new NfiPlugPhase(collTo, parent);
	}
}
