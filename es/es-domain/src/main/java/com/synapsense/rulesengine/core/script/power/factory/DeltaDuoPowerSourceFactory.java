package com.synapsense.rulesengine.core.script.power.factory;

import java.util.Collection;
import java.util.HashMap;

import com.synapsense.rulesengine.core.script.power.model.DuoDeltaPhase;
import com.synapsense.rulesengine.core.script.power.model.Phase;
import com.synapsense.rulesengine.core.script.power.model.Rpdu;

public class DeltaDuoPowerSourceFactory extends DuoPowerSourceFactory {

	@Override
	public Collection<Phase> createPhases(Rpdu rpdu) throws Exception {
		Collection<Phase> phases = super.createPhases(rpdu);

		// A little tricky here. If there is at least one phase that reported
		// delta current instead of wye current we have to convert
		for (Phase phase : phases) {
			if (phase.getDeltaAvgCurrent() != null) {
				HashMap<String, Phase> phaseMap = new HashMap<String, Phase>();
				for (Phase ph : phases) {
					phaseMap.put(ph.getLabel(), ph);
				}
				phases.clear();

				Phase phaseAB = phaseMap.get("A");
				Phase phaseBC = phaseMap.get("B");
				Phase phaseCA = phaseMap.get("C");

				// Need to identify single line rpdu
				// A little harder than with jawas cause we always create all 3
				// phase objects
				double voltageFactor = (phaseAB.getVoltage() == null || phaseBC.getVoltage() == null || phaseCA
				        .getVoltage() == null) ? 2 : Math.sqrt(3.0);

				phases.add(new DuoDeltaPhase(phaseAB, phaseCA, voltageFactor));
				phases.add(new DuoDeltaPhase(phaseBC, phaseAB, voltageFactor));
				phases.add(new DuoDeltaPhase(phaseCA, phaseBC, voltageFactor));

				return phases;
			}
		}
		return phases;
	}
}
