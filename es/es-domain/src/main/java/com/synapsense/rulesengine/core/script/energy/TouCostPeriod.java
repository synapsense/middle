package com.synapsense.rulesengine.core.script.energy;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;

public class TouCostPeriod {

	public final static int WEEKDAYS = 62; // 0111110
	public final static int WEEKENDS = 65; // 1000001
	public final static int ALL = 127; // 1111111

	private int startMonth;
	private int endMonth;
	private int typeOfDay;

	private ArrayList<TouCostTime> times = new ArrayList<TouCostTime>();

	public TouCostPeriod(int startMonth, int endMonth, int typeOfDay) {
		if (startMonth < Calendar.JANUARY || startMonth > Calendar.DECEMBER) {
			throw new IllegalArgumentException("start month is out of range: " + startMonth);
		}
		if (endMonth < Calendar.JANUARY || endMonth > Calendar.DECEMBER) {
			throw new IllegalArgumentException("end month is out of range: " + endMonth);
		}
		if (typeOfDay != WEEKDAYS && typeOfDay != WEEKENDS && typeOfDay != ALL) {
			throw new IllegalArgumentException("Invalid type of day");
		}

		this.startMonth = startMonth;
		this.endMonth = endMonth;
		this.typeOfDay = typeOfDay;
	}

	public double getCost(long timestamp) {
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(timestamp);
		for (TouCostTime time : times) {
			if (time.inTimes(cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE))) {
				return time.cost;
			}
		}
		throw new IllegalArgumentException("Time frame for " + timestamp + " is not found. Available frames: " + times);
	}

	public boolean inPeriod(long timestamp) {
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(timestamp);

		int month = cal.get(Calendar.MONTH);
		int day = cal.get(Calendar.DAY_OF_WEEK);

		return inPeriod(month) && (((typeOfDay >> (day - 1)) & 1) == 1);
	}

	public boolean inPeriod(int month) {
		return (month >= startMonth) && (month <= endMonth) || (startMonth > endMonth)
		        && (month >= startMonth || month <= endMonth);
	}

	public double getCostSummary(int year) {

		// First find the number of days in the period
		int nbOfDays = 0;
		for (int i = startMonth; i <= endMonth; i++) {
			nbOfDays += EnergyUtil.getNumberOfDays(year, i, typeOfDay);
		}

		// Cost for the whole period
		return getCostPerDay() * nbOfDays;
	}

	public double getMonthCostSummary(int year, int month) {
		if (!inPeriod(month)) {
			throw new IllegalArgumentException("Month " + month + " is not in period");
		}
		return getCostPerDay() * EnergyUtil.getNumberOfDays(year, month, typeOfDay);
	}

	public TouCostPeriod addTime(int h1, int m1, int h2, int m2, double cost) {
		times.add(new TouCostTime(h1, m1, h2, m2, cost));
		return this;
	}

	public Collection<TouCostTime> getTimes() {
		return Collections.unmodifiableCollection(times);
	}

	private double getCostPerDay() {
		double costPerDay = 0.0;
		for (TouCostTime time : times) {
			costPerDay += time.getCostSummary();
		}
		return costPerDay;
	}

	public static class TouCostTime {
		private int m1;
		private int m2;
		private double cost;

		public TouCostTime(int h1, int m1, int h2, int m2, double cost) {
			this.m1 = h1 * 60 + m1;
			this.m2 = h2 * 60 + m2;
			this.cost = cost;
		}

		public boolean inTimes(int h, int m) {
			m = h * 60 + m;
			return (m >= m1) && (m < m2) || (m1 > m2) && (m >= m1 || m < m2);
		}

		public double getCostSummary() {
			double m = m2;
			if (m < m1) {
				m += 24 * 60;
			}
			return cost * (m - m1) / 60;
		}

		@Override
		public String toString() {
			return "TouCostTime [m1=" + m1 + ", m2=" + m2 + ", cost=" + cost + "]";
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			long temp;
			temp = Double.doubleToLongBits(cost);
			result = prime * result + (int) (temp ^ (temp >>> 32));
			result = prime * result + m1;
			result = prime * result + m2;
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			TouCostTime other = (TouCostTime) obj;
			if (Double.doubleToLongBits(cost) != Double.doubleToLongBits(other.cost))
				return false;
			if (m1 != other.m1)
				return false;
			if (m2 != other.m2)
				return false;
			return true;
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + endMonth;
		result = prime * result + startMonth;
		result = prime * result + ((times == null) ? 0 : times.hashCode());
		result = prime * result + typeOfDay;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TouCostPeriod other = (TouCostPeriod) obj;
		if (endMonth != other.endMonth)
			return false;
		if (startMonth != other.startMonth)
			return false;
		if (times == null) {
			if (other.times != null)
				return false;
		} else if (!times.equals(other.times))
			return false;
		if (typeOfDay != other.typeOfDay)
			return false;
		return true;
	}
}
