package com.synapsense.rulesengine.core.script.power.factory;

import java.util.Collection;
import java.util.Map;

import com.synapsense.dto.TO;
import com.synapsense.rulesengine.core.script.power.model.Phase;
import com.synapsense.rulesengine.core.script.power.model.Plug;
import com.synapsense.rulesengine.core.script.power.model.PowerObject;
import com.synapsense.rulesengine.core.script.power.model.PowerRack;

/**
 * Abstract factory for creating servers and jawas. This interface is separated
 * from {@link PowerSourceFactory} as in some cases power rack can be configured
 * without jawas and servers.
 * 
 * @see ScFiFactory
 * @see ScNfiFactory
 * @see DeltaPOFactory
 * 
 * @author Dmitry Grudzinskiy
 * 
 */
public interface ServerPlugFactory {

	/**
	 * Creates a map of server objects that are children of passed power rack.
	 * 
	 * @param rack
	 *            parent rack
	 * @return map of servers
	 * @throws Exception
	 */
	Map<TO<?>, PowerObject> createServers(PowerRack rack) throws Exception;

	/**
	 * Creates a collection of jawas (plug) that are children of the passed
	 * phase. Also corresponding server parent is set to each jawa using passed
	 * server objects map.
	 * 
	 * @param phase
	 *            parent phase
	 * @param serverMap
	 *            map of servers the jawas are connected to
	 * @return collection of jawas with set phase and server parents
	 * @throws Exception
	 */
	Collection<Plug> createPlugs(Phase phase, Map<TO<?>, PowerObject> serverMap) throws Exception;
}
