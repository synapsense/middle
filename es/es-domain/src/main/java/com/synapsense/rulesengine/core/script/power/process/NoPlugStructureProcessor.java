package com.synapsense.rulesengine.core.script.power.process;

import java.util.Collection;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.rulesengine.core.script.power.factory.PowerSourceFactory;
import com.synapsense.rulesengine.core.script.power.model.Phase;
import com.synapsense.rulesengine.core.script.power.model.PowerRack;
import com.synapsense.rulesengine.core.script.power.model.Rpdu;
import com.synapsense.service.Environment;

/**
 * Performs object hierarchy processing in case the rack is instrumented by
 * either BCMS or DUO.
 * 
 * @see ScPlatformProcessor
 * 
 * @author Dmitry Grudzinskiy
 * 
 */
public class NoPlugStructureProcessor implements PowerPlatformProcessor {

	protected static final Log log = LogFactory.getLog(NoPlugStructureProcessor.class);

	protected Environment env;

	private PowerSourceFactory powerSourceFactory;

	public NoPlugStructureProcessor(PowerSourceFactory powerSourceFactory) {
		this.powerSourceFactory = powerSourceFactory;
	}

	@Override
	public void process(PowerRack rack) throws Exception {
		if (log.isDebugEnabled()) {
			log.debug(rack + " cre rule: processing rack with no plugs configured (bcms/duo)");
		}
		Collection<Rpdu> rpdus = powerSourceFactory.createRpdus(rack);
		for (Rpdu rpdu : rpdus) {
			if (log.isTraceEnabled()) {
				log.trace(rack + " cre rule: loaded rpdu class " + rpdu.getClass().getSimpleName() + " for " + rpdu);
			}
			Collection<Phase> phases = powerSourceFactory.createPhases(rpdu);
			for (Phase phase : phases) {
				if (log.isTraceEnabled()) {
					log.trace(rack + " cre rule: loaded phase class " + phase.getClass().getSimpleName() + " for "
					        + phase);
				}
				phase.process(env);
			}
			rpdu.process(env);
		}
		rack.process(env);
	}

	public void setEnv(Environment env) {
		this.env = env;
	}
}
