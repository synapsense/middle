package com.synapsense.rulesengine.core.script.power.process;

import com.synapsense.rulesengine.core.script.power.model.PowerRack;

/**
 * The common interface for implementation of power object model processing
 * according to platform type.
 * <p>
 * Existing platform types are SC, BCMS and DUO.
 * 
 * @see ScPlatformProcessor
 * @see NoPlugStructureProcessor
 * 
 * @author Dmitry Grudzinskiy
 * 
 */
public interface PowerPlatformProcessor {

	/**
	 * Walks down power model object tree and performs all the necessay
	 * calculations on each level of hierarchy
	 * 
	 * @param rack
	 *            power rack to process
	 * @throws Exception
	 */
	void process(PowerRack rack) throws Exception;
}
