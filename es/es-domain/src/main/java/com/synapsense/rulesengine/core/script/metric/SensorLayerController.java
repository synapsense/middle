package com.synapsense.rulesengine.core.script.metric;

import java.util.Collection;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.dto.TO;
import com.synapsense.rulesengine.core.environment.Property;
import com.synapsense.rulesengine.core.environment.RuleAction;
import com.synapsense.rulesengine.core.environment.RuleI;
import com.synapsense.service.Environment;
import com.synapsense.service.impl.dao.to.PropertyTO;

public class SensorLayerController implements RuleAction {

	/**
     * 
     */
	private static final long serialVersionUID = 1114643119768100221L;
	private static Log log = LogFactory.getLog(SensorLayerController.class);

	private static final int TOP_BITMASK = 0x20000;
	private static final int MIDDLE_BITMASK = 0x04000;
	private static final int BOTTOM_BITMASK = 0x00800;
	private static final int CONTROL_BITMASK = 0x00080;

	private static final String TOP_POSITION = "top";
	private static final String MIDDLE_POSITION = "middle";
	private static final String BOTTOM_POSITION = "bottom";

	private static final Double OPERATION_ERROR = 0d;
	private static final Double OPERATION_SUCCESS = 1d;

	private static final String[] COIN_POSITIONS = { "t", "m", "b" };
	private static final String[] SLC_POSITIONS = { "top", "middle", "bottom" };
	private static final String[] COLD_SIDE_LINKS = { "cTop", "cMid", "cBot" };

	private Property position;

	public void setPosition(Property position) {
		this.position = position;
	}

	@Override
	public Object run(RuleI triggeredRule, Property calculated) {

        if (log.isDebugEnabled())
            log.debug("Rule " + triggeredRule.getName() + "; Property " + calculated.getName() + " is running");

		Environment env = calculated.getEnvironment();
		TO<?> slc = calculated.getDataSource().getHostObjectTO();

		TO<?> sensor = null;
		String newPosition = null;

		PropertyTO top = null;
		PropertyTO middle = null;
		PropertyTO bottom = null;

		PropertyTO now = null;

		TO<?> topRef = null;
		TO<?> middleRef = null;
		TO<?> bottomRef = null;
		try {

			sensor = env.getPropertyValue(slc, "sensor", TO.class);
			newPosition = (String) position.getValue();

			top = env.getPropertyValue(slc, "top", PropertyTO.class);
			middle = env.getPropertyValue(slc, "middle", PropertyTO.class);
			bottom = env.getPropertyValue(slc, "bottom", PropertyTO.class);

			topRef = env.getPropertyValue(top.getObjId(), top.getPropertyName(), TO.class);
			middleRef = env.getPropertyValue(middle.getObjId(), middle.getPropertyName(), TO.class);
			bottomRef = env.getPropertyValue(bottom.getObjId(), bottom.getPropertyName(), TO.class);

			if (topRef != null && topRef.equals(sensor)) {
				now = top;
			} else {
				if (middleRef != null && middleRef.equals(sensor)) {
					now = middle;
				} else {
					if (bottomRef != null && bottomRef.equals(sensor)) {
						now = bottom;
					}
				}
			}
		} catch (Exception e) {
			log.warn("Could not find the sensor", e);
			return OPERATION_ERROR;
		}

		if (newPosition.equals(TOP_POSITION)) {
			return setProperties(env, slc, now, top, sensor, TOP_BITMASK);
		}
		if (newPosition.equals(MIDDLE_POSITION)) {
			return setProperties(env, slc, now, middle, sensor, MIDDLE_BITMASK);
		}
		if (newPosition.equals(BOTTOM_POSITION)) {
			return setProperties(env, slc, now, bottom, sensor, BOTTOM_BITMASK);
		}

		return OPERATION_ERROR;
	}

	private Double setProperties(Environment env, TO<?> slc, PropertyTO now, PropertyTO target, TO<?> sensor,
	        Integer newBitmask) {
		Integer currentBitmask = null;
		Collection<TO<?>> racks;
		try {
			currentBitmask = env.getPropertyValue(sensor, "z", Integer.class);

			newBitmask = currentBitmask & (~(TOP_BITMASK | MIDDLE_BITMASK | BOTTOM_BITMASK)) | newBitmask;

			racks = env.getParents(slc, "RACK");

		} catch (Exception e) {
			log.warn("Could not get current position of the sensor", e);
			return OPERATION_ERROR;
		}

		try {
			if (now != null) {
				env.setPropertyValue(now.getObjId(), now.getPropertyName(), null);
			}
			env.setPropertyValue(target.getObjId(), target.getPropertyName(), sensor);
			env.setPropertyValue(sensor, "z", newBitmask);

			// we should update coins if sensor is located on cold side only
			if (isColdPositionChanged(target.getPropertyName())) {
				updateCoins(env, racks, sensor, newBitmask);
			}

		} catch (Exception e) {
			log.warn("Could not change position of the sensor; Trying to revert ");
			try {
				if (now != null) {
					env.setPropertyValue(now.getObjId(), now.getPropertyName(), sensor);
				}
				env.setPropertyValue(target.getObjId(), target.getPropertyName(), null);
				env.setPropertyValue(sensor, "z", currentBitmask);
				env.setPropertyValue(slc, "position", createPosition(env, currentBitmask, SLC_POSITIONS));
				updateCoins(env, racks, sensor, currentBitmask);
			} catch (Exception e1) {
				log.error("Could not revert position of the sensor", e1);
			}
			return OPERATION_ERROR;
		}

		return OPERATION_SUCCESS;

	}

	private boolean isColdPositionChanged(String propertyName) {
		if (propertyName.equals(COLD_SIDE_LINKS[0]) || propertyName.equals(COLD_SIDE_LINKS[1])
		        || propertyName.equals(COLD_SIDE_LINKS[2])) {
			return true;
		} else {
			return false;
		}
	}

	private void updateCoins(Environment env, Collection<TO<?>> racks, TO<?> sensor, Integer position) throws Exception {
		for (TO<?> rack : racks) {
			Collection<TO<?>> coinsingles = env.getChildren(rack, "controlpoint_singlecompare");
			for (TO<?> coin : coinsingles) {
				if (env.getPropertyValue(coin, "sensor", TO.class).equals(sensor)) {
					env.setPropertyValue(coin, "position", createPosition(env, position, COIN_POSITIONS));
				}
			}
			Collection<TO<?>> coinStrat = env.getChildren(rack, "controlpoint_strat");
			for (TO<?> coin : coinStrat) {
				replaceSensorsForStrat(env, sensor, coin);
			}
		}
	}

	private void replaceSensorsForStrat(Environment env, TO<?> sensor, TO<?> coin) throws Exception {
		TO<?> topSensor = env.getPropertyValue(coin, "top", TO.class);
		TO<?> bottomSensor = env.getPropertyValue(coin, "bottom", TO.class);
		if (topSensor.equals(sensor) || bottomSensor.equals(sensor)) {
			Integer topLocation = env.getPropertyValue(topSensor, "z", Integer.class);
			Integer bottomLocation = env.getPropertyValue(bottomSensor, "z", Integer.class);

			if (topLocation < bottomLocation) {
				env.setPropertyValue(coin, "top", bottomSensor);
				env.setPropertyValue(coin, "bottom", topSensor);
				env.setPropertyValue(coin, "position", createPosition(env, bottomLocation, topLocation, COIN_POSITIONS));
			} else {
				env.setPropertyValue(coin, "position", createPosition(env, topLocation, bottomLocation, COIN_POSITIONS));
			}
		}
	}

	private String createPosition(Environment env, Integer z, String[] positions) throws Exception {
		// Clear control layer tag
		z = z & (~CONTROL_BITMASK);
		switch (z) {
		case TOP_BITMASK:
			return positions[0];
		case MIDDLE_BITMASK:
			return positions[1];
		case BOTTOM_BITMASK:
			return positions[2];
		}
		throw new Exception("Could not find current position");
	}

	private String createPosition(Environment env, Integer zTop, Integer zMiddle, String[] positions) throws Exception {
		return createPosition(env, zTop, positions) + createPosition(env, zMiddle, positions);
	}

}
