package com.synapsense.rulesengine.core.script.metric;

import static com.synapsense.rulesengine.core.script.energy.EnergyUtil.getCostCalculator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.dto.TO;
import com.synapsense.exception.EnvException;
import com.synapsense.rulesengine.core.environment.Property;
import com.synapsense.rulesengine.core.environment.RuleAction;
import com.synapsense.rulesengine.core.environment.RuleI;
import com.synapsense.rulesengine.core.script.energy.CostCalculator;
import com.synapsense.rulesengine.core.script.energy.EnergyUtil;
import com.synapsense.rulesengine.core.script.energy.ReductionCalculator;
import com.synapsense.service.Environment;

public class EstCostReduction implements RuleAction {

	private static final long serialVersionUID = 8538918871663596870L;

	private static Log log = LogFactory.getLog(EstCostReduction.class);

	private Property itLoadTarget;

	private Property pueTarget;

	private Property energyCost;

	@Override
	public Object run(RuleI triggeredRule, Property calculated) {
		Environment env = calculated.getEnvironment();
		TO<?> pue = calculated.getDataSource().getHostObjectTO();

        if (log.isDebugEnabled())
            log.debug("Calculating estimated annual cost reduction for " + pue);
		try {
			CostCalculator costCalculator = getCostCalculator(pue, env);
			if (costCalculator == null) {
				return null;
			}
			return ReductionCalculator.getInstance().calculateReduction((Double) pueTarget.getValue(),
			        (Double) itLoadTarget.getValue(), (Double) energyCost.getValue(), costCalculator,
			        EnergyUtil.ENERGY_COST, pue, env);

		} catch (EnvException e) {
			log.error(e.getMessage(), e);
			return null;
		}
	}

	public void setItLoadTarget(Property itLoadTarget) {
		this.itLoadTarget = itLoadTarget;
	}

	public void setPueTarget(Property pueTarget) {
		this.pueTarget = pueTarget;
	}

	public void setEnergyCost(Property energyCost) {
		this.energyCost = energyCost;
	}
}
