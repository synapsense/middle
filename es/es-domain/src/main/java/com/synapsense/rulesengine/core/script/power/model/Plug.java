package com.synapsense.rulesengine.core.script.power.model;

import com.synapsense.dto.CollectionTO;
import com.synapsense.service.Environment;

/**
 * The class implements plug processing.
 * 
 * @author Dmitry Grudzinskiy
 * 
 */
public class Plug extends LinePowerObject {

	private PowerObject serverParent;
	private Phase phaseParent;

	public Plug(CollectionTO objProps, Phase phaseParent, PowerObject serverParent) {
		super(objProps);
		this.serverParent = serverParent;
		this.phaseParent = phaseParent;
	}

	@Override
	public void process(Environment env) throws Exception {

		// Calculate apPower, maxApPower
		processPower(phaseParent.getVoltage());

		// Update maxHistCurrent, maxHistApPower
		processHistoricalMaxCurrent();
		processHistoricalMaxPower();

		// Sum values to parents
		aggregateParent(serverParent);

		// 7051: We don't aggregate kVa from plugs
		phaseParent.addAvgCurrent(avgCurrent);
		phaseParent.addMaxCurrent(maxCurrent);
		phaseParent.addDemandPower(demandPower);

		// We sum measured historical maximums to be the potential maxs on phase
		// level
		phaseParent.addPotentialMaxCurrent(maxHistCurrent);

		// Save necessary values
		saveValueList(createDefaultValueList(), env);
	}
}
