package com.synapsense.rulesengine.core.script.power.model;

import static com.synapsense.rulesengine.core.script.power.model.PowerConstants.POTENTIAL_MAX_AP_POWER;
import static com.synapsense.rulesengine.core.script.power.model.PowerConstants.POTENTIAL_MAX_CURRENT;
import static com.synapsense.rulesengine.core.script.power.model.PowerConstants.DELTA_AVG_CURRENT;

import java.util.List;

import com.synapsense.dto.CollectionTO;
import com.synapsense.dto.ValueTO;
import com.synapsense.service.Environment;

/**
 * The class implements phase parameters computation in case the corresponding
 * rack is instrumented by jawas.
 * <code>avgCurrent, maxCurrent, demandPower, apPower, maxApPower</code> are
 * sums of children plugs corresponding parameters, historical maximums are
 * updated if necessay with computed maximums.
 * 
 * @author Dmitry Grudzinskiy
 * 
 */
public class PlugPhase extends Phase {

	public PlugPhase(CollectionTO objProps, Rpdu parent) {
		super(objProps, parent);
		this.parent = parent;
	}

	@Override
	protected void calculate(Environment env) throws Exception {

		// 7141: Calculate potentialApPower on the phase level to reduce error
		if (isValid(voltage) && isValid(potentialMaxCurrent)) {
			potentialMaxApPower = potentialMaxCurrent * voltage / 1000;
		}

		// 7057: Calculate ApPower on phase level
		processPower(getVoltage());
		processHistoricalMaxCurrent();
		processHistoricalMaxPower();
	}

	@Override
	protected void save(Environment env) throws Exception {
		List<ValueTO> values = createCalculatedValuesList();
		addValueTO(values, POTENTIAL_MAX_CURRENT, potentialMaxCurrent);
		addValueTO(values, POTENTIAL_MAX_AP_POWER, potentialMaxApPower);
		addValueTO(values, DELTA_AVG_CURRENT, deltaAvgCurrent);
		saveValueList(values, env);
		saveLastResetTime(env);
	}

	@Override
	protected void aggregateParent(PowerObject parent) {
		super.aggregateParent(parent);
		// Additional potential maximum
		parent.addPotentialMaxApPower(potentialMaxApPower);
	}
}
