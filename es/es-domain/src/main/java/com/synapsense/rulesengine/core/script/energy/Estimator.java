package com.synapsense.rulesengine.core.script.energy;

public interface Estimator {

	/**
	 * Calculates annual value assuming that the power value was constant
	 * throughout the year.
	 * 
	 * @param year
	 *            calculation year
	 * @param power
	 *            constant power
	 * @return annual value
	 */
	double estimateAnnual(int year, double power);

	/**
	 * Calculates monthly value assuming that the power value was constant
	 * throughout the month.
	 * 
	 * @param year
	 *            calculation year
	 * @param month
	 *            calculation month
	 * @param power
	 *            constant power
	 * @return monthly value
	 */
	double estimateMonthly(int year, int month, double power);
}
