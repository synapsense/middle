package com.synapsense.rulesengine.core.script.power.process;

import java.util.Collection;

import com.synapsense.dto.TO;
import com.synapsense.rulesengine.core.script.power.factory.PowerSourceFactory;
import com.synapsense.rulesengine.core.script.power.model.PowerConstants;
import com.synapsense.rulesengine.core.script.power.model.PowerRack;
import com.synapsense.rulesengine.core.script.power.model.Util;

public class DuoPlatfromProcessor extends NoPlugStructureProcessor {

	private static final String[] PROPS_TO_CHECK = new String[] { PowerConstants.AVG_CURRENT,
	        PowerConstants.MAX_CURRENT, PowerConstants.DELTA_AVG_CURRENT, PowerConstants.DELTA_MAX_CURRENT,
	        PowerConstants.DEMAND_POWER, PowerConstants.VOLTAGE };

	public DuoPlatfromProcessor(PowerSourceFactory powerSourceFactory) {
		super(powerSourceFactory);
	}

	@Override
	public void process(PowerRack rack) throws Exception {
		// Check if DUO is reporting status
		long currentTime = System.currentTimeMillis();
		int currentStatus = env.getPropertyValue(rack.getObjId(), PowerConstants.STATUS, Integer.class);
		int newStatus = PowerConstants.STATUS_OK;
		Collection<TO<?>> phases = env.getRelatedObjects(rack.getObjId(), PowerConstants.PHASE, true);
		if (!phases.isEmpty()) {
			newStatus = PowerConstants.STATUS_NOT_REPORTING;
			for (TO<?> phase : phases) {
				for (String prop : PROPS_TO_CHECK) {
					long timestamp = env.getPropertyTimestamp(phase, prop);
					if (currentTime <= (timestamp + 2 * PowerConstants.SAMPLE_INTERVAL)) {
						// At least one property has been updated recently
						newStatus = PowerConstants.STATUS_OK;
					}
				}
			}
		}
		Util.updateRackStatus(rack, currentStatus, newStatus, log, env);

		if (newStatus == PowerConstants.STATUS_NOT_REPORTING) {
			log.warn(rack + " cre rule: seems that DUO is not reporting, calculations won't be performed");
			return;
		}
		// Continue to processing
		super.process(rack);
	}
}
