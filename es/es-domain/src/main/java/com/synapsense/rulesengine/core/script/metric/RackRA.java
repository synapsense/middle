package com.synapsense.rulesengine.core.script.metric;

import org.apache.log4j.Logger;

import com.synapsense.dto.TO;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.exception.PropertyNotFoundException;
import com.synapsense.exception.UnableToConvertPropertyException;
import com.synapsense.rulesengine.core.environment.Property;
import com.synapsense.rulesengine.core.environment.RuleAction;
import com.synapsense.rulesengine.core.environment.RuleI;
import com.synapsense.service.Environment;
import com.synapsense.service.impl.dao.to.PropertyTO;

public class RackRA implements RuleAction {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private final static Logger logger = Logger.getLogger(RackRA.class);
	
	private Property ref = null;
	
	public Object run(final RuleI triggeredRule, final Property calculated) {
		logger.debug(triggeredRule.getName() + " is run");
		try {
			double res = 0;
			double total_heat_gain;

			Environment env = calculated.getEnvironment();
			TO<?> nativeTO = calculated.getDataSource().getHostObjectTO();

			if (env.getPropertyValue(nativeTO, "status", Integer.class) != 1) {
                if (logger.isDebugEnabled())
				    logger.debug("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName()
				        + "': some data is obsolete. Skipping...");
				return null;
			}

			TO<?> refSensor = (TO<?>) ref.getValue(); 
			Double refVal = getValue(env, refSensor, "lastValue");

			Double cTopVal = getValue(env, nativeTO, "cTop");
			Double cMidVal = getValue(env, nativeTO, "cMid");
			Double cBotVal = getValue(env, nativeTO, "cBot");
			Double hTopVal = getValue(env, nativeTO, "hTop");

			//if ((refVal == null) || (cTopVal == null) || (cMidVal == null) || (cBotVal == null) || (hTopVal == null)) {
			if ((refVal == null) || (cTopVal == null) || (hTopVal == null)) {
				if (logger.isDebugEnabled())
					logger.debug("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName()
					        + "': one of the inputs is not valid.");
				return null;
			}

			if ((env.getPropertyValue(nativeTO, "hMid", Object.class) == null)
			        || env.getPropertyValue(nativeTO, "hBot", Object.class) == null) {
				total_heat_gain = hTopVal - refVal;
			} else {
				Double hMidVal = getValue(env, nativeTO, "hMid");
				Double hBotVal = getValue(env, nativeTO, "hBot");
				if ((hMidVal == null) || (hBotVal == null)) {
					if (logger.isDebugEnabled())
						logger.debug("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName()
						        + "': one of the inputs is not valid.");
					return null;
				}
				double avg_hot_isle_temp = (hTopVal + hMidVal + hBotVal) / 3;
				total_heat_gain = avg_hot_isle_temp - refVal;
			}

			//both cMid and cBot are now optional, but we want to use whatever ones we have
			//double avg_cold_isle_temp = (cTopVal + cMidVal + cBotVal) / 3;

			int count = 0;
			int totalColdSide = 0;
			if(cTopVal!= null){
				count ++;
				totalColdSide += cTopVal;
			}
			if(cMidVal!= null){
				count ++;
				totalColdSide += cMidVal;
			}
			if(cBotVal!= null){
				count ++;
				totalColdSide += cBotVal;
			}
			double avg_cold_isle_temp =  totalColdSide / count;
			double cold_isle_heat_gain = avg_cold_isle_temp - refVal;

			if (total_heat_gain <= 0) { // do not divide by zero
				res = 100;
			} else if (cold_isle_heat_gain < 0) {
				res = 0;
			} else if (cold_isle_heat_gain > total_heat_gain) {
				res = 100;
			} else {
				res = cold_isle_heat_gain / (total_heat_gain) * 100; // multiply
																	 // by 100
			}

            if (logger.isDebugEnabled())
                logger.debug(triggeredRule.getName() + " is finished");
			return res;
		} catch (Exception e) {
			logger.error("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName() + "': error", e);
			return null;
		}
	}

	private Double getValue(Environment env, TO<?> obj, String propertyName) throws ObjectNotFoundException,
	        PropertyNotFoundException, UnableToConvertPropertyException {
		Double value = null;

		Object propObj = env.getPropertyValue(obj, propertyName, Object.class);
		// support direct values or links
		if (propObj == null) {
			return null;
		} else if (propObj instanceof TO) {
			value = env.getPropertyValue(((TO<?>) propObj), "lastValue", Double.class);
		} else if (propObj instanceof Double) {
			value = (Double) propObj;
		} else if (propObj instanceof PropertyTO) {
			value = env.getPropertyValue(((PropertyTO) propObj).getObjId(), ((PropertyTO) propObj).getPropertyName(),
			        Double.class);
		} else {
			throw new IllegalArgumentException("Unsupported type of object: "
			        + ((propObj == null) ? "null" : propObj.getClass().getCanonicalName()));
		}

		// validate sensor data
		if ((value != null) && (value > -1000)) {
			return value;
		}
		return null;
	}
	
	public Property getRef() {
		return ref;
	}

	public void setRef(Property ref) {
		this.ref = ref;
	}
}