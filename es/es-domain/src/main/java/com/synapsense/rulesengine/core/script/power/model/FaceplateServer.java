package com.synapsense.rulesengine.core.script.power.model;

import java.util.Arrays;

import com.synapsense.dto.CollectionTO;
import com.synapsense.dto.TO;
import com.synapsense.service.Environment;

/**
 * The class implements server parameters computation in case the corresponding
 * rack is instrumented by jawas but 'not fully instrumented (NFI)'. This means
 * that some servers have plug meters and some doesn't not. In this case we use
 * 'faceplate' data to estimate rack values.
 * 
 * @author Dmitry Grudzinskiy
 * 
 */
public class FaceplateServer extends PowerObject {

	private static final String[] MANUAL_PROPS = new String[] { PowerConstants.FP_CURRENT, PowerConstants.FP_APPOWER,
	        PowerConstants.FP_DEMANDPOWER };

	protected PowerRack parent;

	public FaceplateServer(TO<?> objId, PowerRack parent) {
		super(objId);
		this.parent = parent;
	}

	public void process(Environment env) {
		CollectionTO manualProps = env.getPropertyValue(Arrays.asList(new TO<?>[] { objId }), MANUAL_PROPS).iterator()
		        .next();

		Double apPower = Util.getDoubleValue(manualProps, PowerConstants.FP_APPOWER);
		parent.addApPower(apPower);
		parent.addMaxApPower(apPower);
		parent.addDemandPower(Util.getDoubleValue(manualProps, PowerConstants.FP_DEMANDPOWER));
	}
}
