package com.synapsense.rulesengine.core.script.metric;

import java.util.Collection;

import org.apache.log4j.Logger;

import com.synapsense.dto.TO;
import com.synapsense.rulesengine.core.environment.Property;
import com.synapsense.rulesengine.core.environment.RuleAction;
import com.synapsense.rulesengine.core.environment.RuleI;
import com.synapsense.service.Environment;

public class RoomCracAvgReturn implements RuleAction {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private final static Logger logger = Logger.getLogger(RoomCracAvgReturn.class);

	public Object run(final RuleI triggeredRule, final Property calculated) {
        if (logger.isDebugEnabled())
		    logger.debug(triggeredRule.getName() + " is run");
		try {
			Environment env = calculated.getEnvironment();
			TO<?> nativeTO = calculated.getDataSource().getHostObjectTO();

			int CRACsNumber = 0;
			Double TR = null;
			Double sumTR = 0.0;
			Double min = 1000.0;
			Double max = -1000.0;

			Collection<TO<?>> CRACs = env.getRelatedObjects(nativeTO, "CRAC", true);
			if ((CRACs != null) && (!CRACs.isEmpty())) {
				// iterate over CRACs within the room, collect the sums of
				// return
				for (TO<?> CRAC : CRACs) {
					// Get sensor object
					TO<?> sensor = (TO<?>) env.getPropertyValue(CRAC, "returnT", Object.class);
					// checks that sensor is linked
					if (sensor == null) {
						continue;
					}
					// Validate sensor status
					if (env.getPropertyValue(sensor, "status", Integer.class) == 1) {
						Double retVal = env.getPropertyValue(sensor, "lastValue", Double.class);

						// Validate sensor has a valid value
						if ((retVal != null) && (retVal > -1000)) {
							CRACsNumber++;
							sumTR += retVal;
							if (retVal > max) {
								max = retVal;
							}
							if (retVal < min) {
								min = retVal;
							}
						}
					}
					if (CRACsNumber == 0) {
						if (logger.isDebugEnabled())
							logger.debug("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName()
							        + "': no recent or bad data.");
					}
				}
			} else {
                if (logger.isDebugEnabled())
				    logger.debug("Rule '" + triggeredRule.getName() + ": room has no CRACs configured.");
			}

			if (CRACsNumber != 0) {
				// Save CRAC return count for future use
				env.setPropertyValue(nativeTO, "crc", (Integer) CRACsNumber);
				// Save minimum CRAC return
				env.setPropertyValue(nativeTO, "minCR", min);
				// Save maximum CRAC return
				env.setPropertyValue(nativeTO, "maxCR", max);

				// Calculate average
				TR = sumTR / CRACsNumber;
			} else {
				// if unsuccessful reset all properties
				if (env.getPropertyValue(nativeTO, "crc", Integer.class) != new Integer(0)) {
					env.setPropertyValue(nativeTO, "crc", 0);
				}
				if (env.getPropertyValue(nativeTO, "minCR", Double.class) != null) {
					env.setPropertyValue(nativeTO, "minCR", null);
				}
				if (env.getPropertyValue(nativeTO, "maxCR", Double.class) != null) {
					env.setPropertyValue(nativeTO, "maxCR", null);
				}
			}

			return TR;
		} catch (Exception e) {
			logger.error("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName() + "': error", e);
			return null;
		}
	}
}