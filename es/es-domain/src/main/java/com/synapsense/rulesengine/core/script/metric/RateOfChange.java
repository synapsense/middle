package com.synapsense.rulesengine.core.script.metric;

import java.util.Collection;
import java.util.Date;

import org.apache.log4j.Logger;

import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;
import com.synapsense.rulesengine.core.environment.Property;
import com.synapsense.rulesengine.core.environment.RuleAction;
import com.synapsense.rulesengine.core.environment.RuleI;
import com.synapsense.service.Environment;

/**
 * Rate of change calculation rule class.<br>
 * The rule is assumed to be linked with a property for which rate of change is
 * to be calculated
 * 
 * @author Alex Bogachek
 * 
 */
public class RateOfChange implements RuleAction {
	private static final long serialVersionUID = 1L;

	private static final Logger log = Logger.getLogger(RateOfChange.class);

	private Property sensor = null;

	@Override
	public Object run(RuleI triggeredRule, Property calculated) {
        if (log.isDebugEnabled())
            log.debug(triggeredRule.getName() + " is triggered");

		try {
			Environment env = calculated.getEnvironment();
			Date end = new Date();
			Date start = new Date(end.getTime() - 3600000); // subtract 60
															// minutes

			Collection<ValueTO> values = env.getHistory((TO<?>) sensor.getValue(), "lastValue", start, end,
			        Double.class); // get sensor values

			if (values.size() != 0) {
				Double min = 1000.0;
				Double max = -1000.0;

				for (ValueTO dbl : values) {
					Double value = (Double) dbl.getValue();

					// validate that sensor value is not an error code
					if ((value != null) && (value > -1000)) {
						if (value > max) {
							max = value;
						}
						if (value < min) {
							min = value;
						}
					}
				}

				// validate the valid values were found by checking min value
				if (max >= min) {
					// return delta
                    if (log.isDebugEnabled())
					    log.debug(triggeredRule.getName() + " is finished");
					return (max - min);
				}
			}
            if (log.isDebugEnabled())
			    log.debug(triggeredRule.getName() + ": no valid sensor values.");
			return null;
		} catch (Exception e) {
			log.error("Rule '" + triggeredRule.getName() + "', property '" + calculated.getName()
			        + "' calculation error", e);
			return null;
		}
	}

	public Property getSensor() {
		return sensor;
	}

	public void setSensor(Property sensor) {
		this.sensor = sensor;
	}
}
