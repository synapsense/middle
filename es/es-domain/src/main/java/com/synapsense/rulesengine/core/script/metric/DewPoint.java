package com.synapsense.rulesengine.core.script.metric;

import org.apache.log4j.Logger;

import com.synapsense.dto.TO;
import com.synapsense.rulesengine.core.environment.Property;
import com.synapsense.rulesengine.core.environment.RuleAction;
import com.synapsense.rulesengine.core.environment.RuleI;
import com.synapsense.service.Environment;

/**
 * Dew point calculation rule class.<br>
 * Input temperature is assumed to be in Fahrenheit (so is calculated dew
 * point).<br>
 * The rule is assumed to be linked with two properties referencing sensor
 * objects with "lastValue" properties holding data for temperature and relative
 * humidity.
 * 
 * @author Oleg Stepanov
 * 
 */
public class DewPoint implements RuleAction {
	private static final long serialVersionUID = 1L;

	private static final Logger log = Logger.getLogger(DewPoint.class);

	private Property airTempSensor = null;
	private Property airHumSensor = null;

	@Override
	public Object run(RuleI triggeredRule, Property calculated) {
        if (log.isDebugEnabled())
            log.debug(triggeredRule.getName() + " is triggered");

		try {
			Environment env = calculated.getEnvironment();

			// check sensor status
			if (env.getPropertyValue((TO<?>) airTempSensor.getValue(), "status", Integer.class) != 1) {
                if (log.isDebugEnabled())
                    log.debug("Rule '" + triggeredRule.getName() + "', property '" + calculated.getName()
				        + "' no recent temp data.");
				return null;
			}
			// check sensor status
			if (env.getPropertyValue((TO<?>) airHumSensor.getValue(), "status", Integer.class) != 1) {
                if (log.isDebugEnabled())
                    log.debug("Rule '" + triggeredRule.getName() + "', property '" + calculated.getName()
				        + "' no recent Rh data.");
				return null;
			}
			// get sensor values
			Double temp = env.getPropertyValue((TO<?>) airTempSensor.getValue(), "lastValue", Double.class);
			Double relHum = env.getPropertyValue((TO<?>) airHumSensor.getValue(), "lastValue", Double.class);

			// validate sensor values are valid
			if ((temp == null) || (relHum == null) || (temp < -1000) || (relHum < -1000)) {
				if (log.isDebugEnabled())
					log.debug("Rule '" + triggeredRule.getName() + "', property '" + calculated.getName()
					        + "' inputs are invalid.");
				return null;
			} else {
				// calculate dew point
				return C2F(calcDewPoint(F2C(temp), relHum));
			}
		} catch (Exception e) {
			log.error("Rule '" + triggeredRule.getName() + "', property '" + calculated.getName()
			        + "' calculation error", e);
			return null;
		}
	}

	// these two are according to
	// http://www.sensirion.com/pdf/product_information/Dewpoint_Calculation_Humidity_Sensor_E.pdf
	static final double beta = 17.62;
	static final double lambda = 243.12;

	// these two are according to
	// http://www.paroscientific.com/dewpoint.htm
	// static final double b = 17.27;
	// static final double l = 237.7;

	/**
	 * Calculates dew point temperature using Magnus-Tetens approximation.
	 * 
	 * @param t
	 *            air temperature in degrees Celsius
	 * @param rh
	 *            relative humidity (from 1 to 100).
	 * @return dew point temperature in degrees Celsuis.
	 */
	private static double calcDewPoint(double t, double rh) {
		double h = Math.log(rh / 100) + beta * t / (lambda + t);
		return lambda * h / (beta - h);
	}

	/**
	 * Converts degrees Fahrenheit into degrees Celsius
	 * 
	 * @param t
	 *            degrees Celsius
	 * @return degrees Fahrenheit
	 */
	private static double F2C(double t) {
		return (t - 32) * 5 / 9;
	}

	/**
	 * Converts degrees Celsius into degrees Fahrenheit
	 * 
	 * @param t
	 *            degrees Fahrenheit
	 * @return degrees Celsius
	 */
	private static double C2F(double t) {
		return t * 9 / 5 + 32;
	}

	public Property getAirTempSensor() {
		return airTempSensor;
	}

	public void setAirTempSensor(Property airTempSensor) {
		this.airTempSensor = airTempSensor;
	}

	public Property getAirHumSensor() {
		return airHumSensor;
	}

	public void setAirHumSensor(Property airHumSensor) {
		this.airHumSensor = airHumSensor;
	}

}
