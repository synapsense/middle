package com.synapsense.rulesengine.core.script.metric;

import java.util.Collection;

import org.apache.log4j.Logger;
import com.synapsense.dto.TO;
import com.synapsense.rulesengine.core.environment.Property;
import com.synapsense.rulesengine.core.environment.RuleAction;
import com.synapsense.rulesengine.core.environment.RuleI;
import com.synapsense.service.Environment;

public class DCCracAvgReturn implements RuleAction {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private final static Logger logger = Logger.getLogger(DCCracAvgReturn.class);

	public Object run(final RuleI triggeredRule, final Property calculated) {
        if (logger.isDebugEnabled())
            logger.debug(triggeredRule.getName() + " is run");
		try {
			Environment env = calculated.getEnvironment();
			TO<?> nativeTO = calculated.getDataSource().getHostObjectTO();

			int CRACsNumber = 0;
			Double sumTR = 0.0;

			Collection<TO<?>> ROOMs = env.getRelatedObjects(nativeTO, "ROOM", true);
			if (ROOMs == null || ROOMs.isEmpty()) {
				throw new IllegalStateException("DC '" + calculated.getDataSource().getNativePropertyName()
				        + "' has no children ROOMs.");
			} else {
				// iterate over ROOMs within the DC
				for (TO<?> ROOM : ROOMs) {
					// Extract CRAC count
					Double crc = env.getPropertyValue(ROOM, "crc", Double.class);
					// Extract avg CRAC return
					Double acr = env.getPropertyValue(ROOM, "avgCR", Double.class);

					if ((crc != null) && (acr != null)) {
						CRACsNumber += crc;
						sumTR += (acr * crc);
					}
				}
			}

			if (CRACsNumber != 0) {
				Double TR = sumTR / CRACsNumber;
				return TR;
			} else {
				return null;
			}
		} catch (Exception e) {
			logger.error("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName() + "': error", e);
			return null;
		}
	}
}