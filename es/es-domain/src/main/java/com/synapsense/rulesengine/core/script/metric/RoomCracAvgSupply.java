package com.synapsense.rulesengine.core.script.metric;

import java.util.Collection;

import org.apache.log4j.Logger;

import com.synapsense.dto.TO;
import com.synapsense.rulesengine.core.environment.Property;
import com.synapsense.rulesengine.core.environment.RuleAction;
import com.synapsense.rulesengine.core.environment.RuleI;
import com.synapsense.service.Environment;

public class RoomCracAvgSupply implements RuleAction {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private final static Logger logger = Logger.getLogger(RoomCracAvgSupply.class);

	public Object run(final RuleI triggeredRule, final Property calculated) {
        if (logger.isDebugEnabled())
            logger.debug(triggeredRule.getName() + " is run");
		try {
			Environment env = calculated.getEnvironment();
			TO<?> nativeTO = calculated.getDataSource().getHostObjectTO();

			int CRACsNumber = 0;
			Double TS = null;
			Double sumTS = 0.0;
			Double min = 1000.0;
			Double max = -1000.0;

			Collection<TO<?>> CRACs = env.getRelatedObjects(nativeTO, "CRAC", true);
			if ((CRACs != null) && (!CRACs.isEmpty())) {
				// iterate over CRACs within the room, collect the sums of
				// supply
				for (TO<?> CRAC : CRACs) {
					// Get sensor object
					TO<?> sensor = (TO<?>) env.getPropertyValue(CRAC, "supplyT", Object.class);
					// checks that sensor is linked
					if (sensor == null) {
						continue;
					}
					// Validate sensor status
					if (env.getPropertyValue(sensor, "status", Integer.class) == 1) {
						Double retVal = env.getPropertyValue(sensor, "lastValue", Double.class);

						// validate the sensor has a valid value
						if ((retVal != null) && (retVal > -1000)) {
							CRACsNumber++;
							sumTS += retVal;
							if (retVal > max) {
								max = retVal;
							}
							if (retVal < min) {
								min = retVal;
							}
						}
					}
				}
				if (CRACsNumber == 0) {
					if (logger.isDebugEnabled())
						logger.debug("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName()
						        + "': no recent or bad data.");
				}
			} else {
                if (logger.isDebugEnabled())
                    logger.debug("Rule '" + triggeredRule.getName() + ": room has no CRACs configured.");
			}

			if (CRACsNumber != 0) {
				// Save CRAC supply count for future use
				env.setPropertyValue(nativeTO, "csc", (Integer) CRACsNumber);
				// Save minimum CRAC supply
				env.setPropertyValue(nativeTO, "minCS", min);
				// Save maximum CRAC supply
				env.setPropertyValue(nativeTO, "maxCS", max);

				// Calculate average
				TS = sumTS / CRACsNumber;
			} else {
				// if unsuccessful reset all properties
				if (env.getPropertyValue(nativeTO, "csc", Integer.class) != new Integer(0)) {
					env.setPropertyValue(nativeTO, "csc", 0);
				}
				if (env.getPropertyValue(nativeTO, "minCS", Double.class) != null) {
					env.setPropertyValue(nativeTO, "minCS", null);
				}
				if (env.getPropertyValue(nativeTO, "maxCS", Double.class) != null) {
					env.setPropertyValue(nativeTO, "maxCS", null);
				}
			}

			return TS;
		} catch (Exception e) {
			logger.error("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName() + "': error", e);
			return null;
		}
	}
}