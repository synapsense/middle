package com.synapsense.rulesengine.core.script.metric;

import java.util.Collection;

import org.apache.log4j.Logger;
import com.synapsense.dto.TO;
import com.synapsense.rulesengine.core.environment.Property;
import com.synapsense.rulesengine.core.environment.RuleAction;
import com.synapsense.rulesengine.core.environment.RuleI;
import com.synapsense.service.Environment;

public class DCRackAvgDischarge implements RuleAction {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private final static Logger logger = Logger.getLogger(DCRackAvgDischarge.class);

	public Object run(final RuleI triggeredRule, final Property calculated) {
        if (logger.isDebugEnabled())
            logger.debug(triggeredRule.getName() + " is run");
		try {
			Environment env = calculated.getEnvironment();
			TO<?> nativeTO = calculated.getDataSource().getHostObjectTO();

			int RacksNumber = 0;
			Double sum_inlet = 0.0;

			Collection<TO<?>> ROOMs = env.getRelatedObjects(nativeTO, "ROOM", true);
			if (ROOMs == null || ROOMs.isEmpty()) {
				throw new IllegalStateException("DC '" + calculated.getDataSource().getNativePropertyName()
				        + "' has no children ROOMs.");
			} else {
				// iterate over ROOMs within the DC
				for (TO<?> ROOM : ROOMs) {
					// Extract Rack count
					Double rdc = env.getPropertyValue(ROOM, "rdc", Double.class);
					// Extract avg Rack inlet
					Double ari = env.getPropertyValue(ROOM, "avgRI", Double.class);

					if ((rdc != null) && (ari != null)) {
						RacksNumber += rdc;
						sum_inlet += (ari * rdc);
					}
				}
			}

			if (RacksNumber != 0) {
				Double dcAri = sum_inlet / RacksNumber;
				return dcAri;
			} else {
				return null;
			}
		} catch (Exception e) {
			logger.error("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName() + "': error", e);
			return null;
		}
	}

}