package com.synapsense.rulesengine.core.script.power.model;

import java.util.TreeSet;

import com.synapsense.dto.CollectionTO;
import com.synapsense.service.Environment;

/**
 * The class implements rpdu parameters computation in case the corresponding
 * rack is instrumented by jawas. All the common values except historical
 * maximums are sums of children phases corresponding parameters. One additional
 * parameter <code>unBalance</code> is computed that represents if an rpdu
 * phases are unbalanced.
 * 
 * @author Dmitry Grudzinskiy
 * 
 */
public abstract class Rpdu extends PowerObject {

	protected PowerRack parent;

	protected Integer type;

	private TreeSet<Double> phVals = new TreeSet<Double>();

	public Rpdu(CollectionTO objProps, PowerRack parent) {
		super(objProps);
		type = (Integer) objProps.getSinglePropValue(PowerConstants.RPDU_TYPE).getValue();
		this.parent = parent;
		setResetTime(parent.getResetTime());
	}

	public void addPhaseCurrent(Double avgCurrent) {
		if (isValid(avgCurrent)) {
			phVals.add(avgCurrent);
		}
	}

	public Integer getType() {
		return type;
	}

	public PowerRack getParent() {
		return parent;
	}

	protected void processUnbalancedThreshold(Environment env) throws Exception {
		Integer unBalance = 0;
		if (phVals.size() > 1) {
			Double[] phValsArr = phVals.toArray(new Double[phVals.size()]);
			if (phValsArr[0] * parent.getUnbalThresh() + phValsArr[0] > phValsArr[phValsArr.length - 1]) {
				unBalance = 1;
			}
		}
		env.setPropertyValue(objId, PowerConstants.PH_OUTOFBAL, unBalance);
	}
}
