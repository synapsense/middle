package com.synapsense.rulesengine.core.script.power.model;

import org.apache.commons.logging.Log;

import com.synapsense.dto.CollectionTO;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.EnvException;
import com.synapsense.service.Environment;

public final class Util {

	private Util() {
	}

	public static Double getDoubleValue(CollectionTO objProps, String propName) {
		if (objProps == null) {
			return null;
		}
		ValueTO valVTO = objProps.getSinglePropValue(propName);
		if (valVTO == null) {
			return null;
		}
		Double val = null;
		if (valVTO.getValue() != null) {
			val = (Double) valVTO.getValue();
		}
		return val;
	}

	public static Double addValue(Double to, Double what) {
		if (what == null || what < 0) {
			return to;
		}
		if (to == null || to < 0) {
			return what;
		}
		return to + what;
	}

	public static void updateRackStatus(PowerRack rack, int currentStatus, int newStatus, Log log, Environment env)
	        throws EnvException {
		if (currentStatus != newStatus) {
            if (log.isDebugEnabled())
                log.debug(rack + " cre rule: setting status to " + newStatus);
			env.setPropertyValue(rack.getObjId(), PowerConstants.STATUS, newStatus);
		}
	}
}
