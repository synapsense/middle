package com.synapsense.rulesengine.core.script.metric;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.exception.EnvException;
import com.synapsense.rulesengine.core.environment.Property;
import com.synapsense.rulesengine.core.environment.RuleAction;
import com.synapsense.rulesengine.core.environment.RuleI;

public class PowerPercentLoad implements RuleAction {

	private static final long serialVersionUID = -5898186054289241933L;

	private static Log log = LogFactory.getLog(PowerPercentLoad.class);

	private Property maxPowerThreshold;
	private Property maxApPower;

	@Override
	public Object run(RuleI triggeredRule, Property calculated) {
		try {
			return (maxPowerThreshold.getValue() != null && maxApPower.getValue() != null) ? (Double) maxApPower
			        .getValue() / (Double) maxPowerThreshold.getValue() * 100 : null;
		} catch (EnvException e) {
			log.error("Error calculating Percent Load on power rack " + calculated.getDataSource().getHostObjectTO(), e);
			return null;
		}
	}

	public void setMaxPowerThreshold(Property maxPowerThreshold) {
		this.maxPowerThreshold = maxPowerThreshold;
	}

	public void setMaxApPower(Property maxApPower) {
		this.maxApPower = maxApPower;
	}
}
