package com.synapsense.rulesengine.core.script.metric;

import java.util.Collection;

import org.apache.log4j.Logger;

import com.synapsense.dto.TO;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.exception.PropertyNotFoundException;
import com.synapsense.exception.UnableToConvertPropertyException;
import com.synapsense.rulesengine.core.environment.Property;
import com.synapsense.rulesengine.core.environment.RuleAction;
import com.synapsense.rulesengine.core.environment.RuleI;
import com.synapsense.service.Environment;

public class RoomRackAvgDischarge implements RuleAction {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private final static Logger logger = Logger.getLogger(RoomRackAvgDischarge.class);

	public Object run(final RuleI triggeredRule, final Property calculated) {
        if (logger.isDebugEnabled())
            logger.debug(triggeredRule.getName() + " is run");
		try {
			Environment env = calculated.getEnvironment();
			TO<?> nativeTO = calculated.getDataSource().getHostObjectTO();

			Double average = null;
			int racksNumber = 0;
			double sumAvgs = 0.0;

			Collection<TO<?>> racks = env.getRelatedObjects(nativeTO, "RACK", true);
			if ((racks != null) && (!racks.isEmpty())) {
				// iterate over racks within the room, collect the sums of
				// discharge
				for (TO<?> rack : racks) {
					// Validate object status
					if (env.getPropertyValue(rack, "status", Integer.class) == 1) {

						Double hotTopVal = getValue(env, rack, "hTop");
						Double hotMidVal = getValue(env, rack, "hMid");
						Double hotBotVal = getValue(env, rack, "hBot");

						byte numberOfSensors = 0;
						double subTotal = 0.0;
						if (hotTopVal != null) {
							subTotal += hotTopVal;
							numberOfSensors++;
						}
						if (hotMidVal != null) {
							subTotal += hotMidVal;
							numberOfSensors++;
						}
						if (hotBotVal != null) {
							subTotal += hotBotVal;
							numberOfSensors++;
						}

						if (numberOfSensors > 0) {
							sumAvgs += (subTotal / numberOfSensors);
							racksNumber++;
						}
					}
				}
				if (racksNumber == 0) {
					if (logger.isDebugEnabled())
						logger.debug("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName()
						        + "': no recent or bad data.");
				}
			} else {
                if (logger.isDebugEnabled())
                    logger.debug("No Racks are found in this room to calculate average discharge temperature");
			}

			if (racksNumber != 0) {
				// Save Rack discharge count for future use
				env.setPropertyValue(nativeTO, "rdc", (Integer) racksNumber);

				// Calculate average
				average = sumAvgs / racksNumber;
			} else {
				// if unsuccessful reset the property
				if (env.getPropertyValue(nativeTO, "rdc", Integer.class) != new Integer(0)) {
					env.setPropertyValue(nativeTO, "rdc", 0);
				}
			}

            if (logger.isDebugEnabled())
                logger.debug(triggeredRule.getName() + " is finished");
			return average;
		} catch (Exception e) {
			logger.error("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName() + "': error", e);
			return null;
		}
	}

	private Double getValue(Environment env, TO<?> obj, String propertyName) throws ObjectNotFoundException,
	        PropertyNotFoundException, UnableToConvertPropertyException {
		// Get sensor object
		TO<?> sensor = (TO<?>) env.getPropertyValue(obj, propertyName, Object.class);
		// checks that sensor is linked
		if (sensor == null) {
			return null;
		}

		// extract sensor value
		Double value = env.getPropertyValue(sensor, "lastValue", Double.class);

		// validate sensor data
		if ((value != null) && (value > -1000)) {
			return value;
		}
		return null;
	}
}