package com.synapsense.rulesengine.core.script.power.model;

import java.util.ArrayList;
import java.util.List;

import com.synapsense.dto.ValueTO;
import com.synapsense.service.Environment;

public class DuoDeltaPhase extends DeltaPhase {

	private Phase leg1;

	public DuoDeltaPhase(Phase leg1, Phase leg2, double voltageFactor) {
		super(leg1, leg2, voltageFactor);
		this.leg1 = leg1;
	}

	@Override
	protected void save(Environment env) throws Exception {
		super.save(env);

		// As long as in this case actually delta values are already saved we
		// have to save wye currents instead
		List<ValueTO> wyeCurrents = new ArrayList<ValueTO>();
		wyeCurrents.add(new ValueTO(PowerConstants.AVG_CURRENT, leg1.avgCurrent));
		wyeCurrents.add(new ValueTO(PowerConstants.MAX_CURRENT, leg1.maxCurrent));
		saveValueList(wyeCurrents, env);
	}
}
