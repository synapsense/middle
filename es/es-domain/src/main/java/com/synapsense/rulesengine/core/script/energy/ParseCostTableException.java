package com.synapsense.rulesengine.core.script.energy;

public class ParseCostTableException extends RuntimeException {

	private static final long serialVersionUID = -518821407076474852L;

	public ParseCostTableException() {
		super();
	}

	public ParseCostTableException(String message, Throwable cause) {
		super(message, cause);
	}

	public ParseCostTableException(String message) {
		super(message);
	}

	public ParseCostTableException(Throwable cause) {
		super(cause);
	}
}
