package com.synapsense.rulesengine.core.script.power.factory;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import com.synapsense.dto.CollectionTO;
import com.synapsense.dto.TO;
import com.synapsense.rulesengine.core.script.power.model.DeltaPhase;
import com.synapsense.rulesengine.core.script.power.model.Phase;
import com.synapsense.rulesengine.core.script.power.model.Plug;
import com.synapsense.rulesengine.core.script.power.model.PowerObject;
import com.synapsense.rulesengine.core.script.power.model.PowerRack;
import com.synapsense.rulesengine.core.script.power.model.Rpdu;
import com.synapsense.service.Environment;

/**
 * An implementation of {@link PowerSourceFactory}, {@link ServerPlugFactory}
 * that creates SC power objects in case the rack rpdu configuration is DELTA.
 * 
 * @author Dmitry Grudzinskiy
 * 
 */
public class DeltaPOFactory extends AbstractPOFactory {

	private AbstractPOFactory psFactory;

	public DeltaPOFactory(AbstractPOFactory psFactory) {
		this.psFactory = psFactory;
	}

	@Override
	public Collection<Phase> createPhases(Rpdu rpdu) throws Exception {
		Collection<Phase> phases = super.createPhases(rpdu);
		HashMap<String, Phase> phaseMap = new HashMap<String, Phase>();
		for (Phase phase : phases) {
			phaseMap.put(phase.getLabel(), phase);
		}

		Phase phaseAB = phaseMap.get("A");
		Phase phaseBC = phaseMap.get("B");
		Phase phaseCA = phaseMap.get("C");

		phases.clear();

		// A way to identify single line rpdu
		double voltageFactor = (phaseAB == null || phaseBC == null | phaseCA == null) ? 2 : Math.sqrt(3.0);

		if (phaseAB != null) {
			phases.add(new DeltaPhase(phaseAB, phaseCA, voltageFactor));
		}
		if (phaseBC != null) {
			phases.add(new DeltaPhase(phaseBC, phaseAB, voltageFactor));
		}
		if (phaseCA != null) {
			phases.add(new DeltaPhase(phaseCA, phaseBC, voltageFactor));
		}

		return phases;
	}

	public Collection<Rpdu> createRpdus(PowerRack parent, Environment env) throws Exception {
		return psFactory.createRpdus(parent);
	}

	public Collection<Plug> createPlugs(Phase phase, Map<TO<?>, PowerObject> serverMap, Environment env)
	        throws Exception {
		return psFactory.createPlugs(phase, serverMap);
	}

	@Override
	public void setEnv(Environment env) {
		super.setEnv(env);
		psFactory.setEnv(env);
	}

	@Override
	protected Rpdu createRpdu(CollectionTO collTo, PowerRack parent) {
		return psFactory.createRpdu(collTo, parent);
	}

	@Override
	protected Phase createPhase(CollectionTO collTo, Rpdu parent) {
		return psFactory.createPhase(collTo, parent);
	}
}
