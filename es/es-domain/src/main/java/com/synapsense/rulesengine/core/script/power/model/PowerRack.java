package com.synapsense.rulesengine.core.script.power.model;

import static com.synapsense.rulesengine.core.script.power.model.PowerConstants.INSTRUMENTATION;
import static com.synapsense.rulesengine.core.script.power.model.PowerConstants.NOMINAL_VOLTAGE;
import static com.synapsense.rulesengine.core.script.power.model.PowerConstants.PLATFORM;
import static com.synapsense.rulesengine.core.script.power.model.PowerConstants.POTENTIAL_MAX_AP_POWER;
import static com.synapsense.rulesengine.core.script.power.model.PowerConstants.QUORUM;

import java.util.List;

import com.synapsense.dto.CollectionTO;
import com.synapsense.dto.ValueTO;
import com.synapsense.rulesengine.core.script.metric.PowerRackMetrics;
import com.synapsense.service.Environment;

/**
 * The class implements Power Rack parameters computation. All the values except
 * historical maximums are sums of children corresponding parameters (servers or
 * rpdus). <code>demandPower</code> property is not sent to environment here as
 * it is returned by the
 * {@link PowerRackMetrics#run(com.synapsense.rulesengine.core.environment.RuleI, com.synapsense.rulesengine.core.environment.Property)}
 * method and hence is saved by CRE.
 * 
 * @author Dmitry Grudzinskiy
 * 
 */
public class PowerRack extends PowerObject {

	private Double unbalThresh;

	private Double nominalVoltage;

	private Integer quorum;

	private RackConfiguration rackConfiguration = new RackConfiguration();

	public PowerRack(CollectionTO objProps) {
		super(objProps);
		rackConfiguration.setInstrumentation((Integer) objProps.getSinglePropValue(INSTRUMENTATION).getValue());
		rackConfiguration.setPlatform((String) objProps.getSinglePropValue(PLATFORM).getValue());
		this.nominalVoltage = Util.getDoubleValue(objProps, NOMINAL_VOLTAGE);
		this.quorum = ((Integer) objProps.getSinglePropValue(QUORUM).getValue());
	}

	@Override
	public void process(Environment env) throws Exception {
		processHistoricalMaxPower();

		// We don't save demandPower here as we're gonna return it from the rule
		List<ValueTO> values = createDefaultValueList();
		addValueTO(values, POTENTIAL_MAX_AP_POWER, potentialMaxApPower);

		saveValueList(values, env);
		saveLastResetTime(env);
	}

	public Double getDemandPower() {
		return demandPower;
	}

	public Double getUnbalThresh() {
		return unbalThresh;
	}

	public void setUnbalThresh(Double unbalThresh) {
		this.unbalThresh = unbalThresh;
	}

	public Double getNominalVoltage() {
		return nominalVoltage;
	}

	public Integer getQuorum() {
		return quorum;
	}

	public void setNominalVoltage(Double nominalVoltage) {
		this.nominalVoltage = nominalVoltage;
	}

	public void setRpduConfiguration(int configuration) {
		this.rackConfiguration.setRpduConfiguration(configuration);
	}

	public RackConfiguration getRackConfiguration() {
		return rackConfiguration;
	}

	public static class RackConfiguration {

		public RackConfiguration() {
			// default to WYE
			this.rpduConfiguration = RpduConfiguration.WYE;
		}

		public RackConfiguration(PowerPlatform platform, Instrumentation instrumentation,
		        RpduConfiguration rpduConfiguration) {
			this.platform = platform;
			this.instrumentation = instrumentation;
			this.rpduConfiguration = rpduConfiguration;
		}

		public static enum PowerPlatform {
		SC, BCMS, DUO
		}

		public static enum Instrumentation {
		NFI(0), FI(1);

		private int id;

		private Instrumentation(int id) {
			this.id = id;
		}

		public static Instrumentation get(int id) {
			for (Instrumentation instr : Instrumentation.values()) {
				if (instr.id == id) {
					return instr;
				}
			}
			return Instrumentation.NFI;
		}

		}

		public static enum RpduConfiguration {
		WYE(0), DELTA(1);

		private int id;

		private RpduConfiguration(int id) {
			this.id = id;
		}

		public static RpduConfiguration get(int id) {
			for (RpduConfiguration conf : RpduConfiguration.values()) {
				if (conf.id == id) {
					return conf;
				}
			}
			return RpduConfiguration.WYE;
		}
		}

		private PowerPlatform platform;

		private Instrumentation instrumentation = Instrumentation.NFI;

		private RpduConfiguration rpduConfiguration;

		public PowerPlatform getPlatform() {
			return platform;
		}

		private void setPlatform(String platform) {
			this.platform = PowerPlatform.valueOf(platform);
		}

		private void setInstrumentation(int instrumentation) {
			this.instrumentation = Instrumentation.get(instrumentation);
		}

		private void setRpduConfiguration(int configuration) {
			this.rpduConfiguration = RpduConfiguration.get(configuration);
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((rpduConfiguration == null) ? 0 : rpduConfiguration.hashCode());
			result = prime * result + ((instrumentation == null) ? 0 : instrumentation.hashCode());
			result = prime * result + ((platform == null) ? 0 : platform.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			RackConfiguration other = (RackConfiguration) obj;
			if (rpduConfiguration != other.rpduConfiguration)
				return false;
			if (instrumentation != other.instrumentation)
				return false;
			if (platform != other.platform)
				return false;
			return true;
		}

		@Override
		public String toString() {
			return new StringBuilder().append("Platform = ").append(platform).append(", Instrumentation = ")
			        .append(instrumentation).append(", RPDU configuration = ").append(rpduConfiguration).toString();
		}
	}
}
