package com.synapsense.rulesengine.core.script.energy;

/**
 * Estimator implementation to calculator carbon dioxide emission
 *
 * @author Dmitry Grudzinskiy
 *
 */

public class EmissionEstimator implements Estimator {

	private Double emissionFactor;

	public EmissionEstimator(Double emissionFactor) {
		this.emissionFactor = emissionFactor;
	}

	@Override
	public double estimateAnnual(int year, double power) {
		return 24 * EnergyUtil.getNumberOfDays(year) * powerFactor(power);
	}

	@Override
	public double estimateMonthly(int year, int month, double power) {
		return 24 * EnergyUtil.getNumberOfDays(year, month) * powerFactor(power);
	}

	private double powerFactor(double power) {
		return power * emissionFactor / 1000;
	}
}
