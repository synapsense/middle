package com.synapsense.rulesengine.core.script.metric;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.dto.TO;
import com.synapsense.exception.EnvException;
import com.synapsense.rulesengine.core.environment.Property;
import com.synapsense.rulesengine.core.environment.RuleAction;
import com.synapsense.rulesengine.core.environment.RuleI;
import com.synapsense.service.Environment;
import com.synapsense.service.impl.dao.to.PropertyTO;

public class COINController implements RuleAction {

	/**
     * 
     */
	private static final long serialVersionUID = -7274548420279542763L;

	private static Log log = LogFactory.getLog(COINController.class);

	private static final String TOP_POSITION = "t";
	private static final String MIDDLE_POSITION = "m";
	private static final String BOTTOM_POSITION = "b";

	private static final String TOP_MIDDLE_STRAT = "tm";
	private static final String TOP_BOTTOM_STRAT = "tb";
	private static final String MIDDLE_BOTTOM_STRAT = "mb";

	private static final String ENABLED = "e";
	private static final String DISABLED = "d";

	private static final Double SUCCESS = 1d;
	private static final Double ERROR = 0d;

	private static final int TOP_BITMASK = 0x20000;
	private static final int MIDDLE_BITMASK = 0x04000;
	private static final int BOTTOM_BITMASK = 0x00800;
	private static final int CONTROL_BITMASK = 0x00080;

	private Property position;

	public void setPosition(Property position) {
		this.position = position;
	}

	@Override
	public Object run(RuleI triggeredRule, Property calculated) {
        if (log.isDebugEnabled())
		    log.debug("Rule " + triggeredRule.getName() + "; Property " + calculated.getName() + " is running");

		Environment env = calculated.getEnvironment();
		TO<?> coin = calculated.getDataSource().getHostObjectTO();

		String newPosition = null;
		try {
			newPosition = (String) position.getValue();
		} catch (EnvException e) {
			log.warn("Could not get value of property position for COIN[" + coin.getID() + "]");
		}

		if (newPosition.equals(TOP_POSITION)) {
			return setPropertiesSingle(env, coin, "rTop");

		}
		if (newPosition.equals(MIDDLE_POSITION)) {
			return setPropertiesSingle(env, coin, "rMiddle");
		}
		if (newPosition.equals(BOTTOM_POSITION)) {
			return setPropertiesSingle(env, coin, "rBottom");
		}
		if (newPosition.equals(TOP_BOTTOM_STRAT)) {
			return setPropertiesStrat(env, coin, "rTop", "rBottom");
		}
		if (newPosition.equals(TOP_MIDDLE_STRAT)) {
			return setPropertiesStrat(env, coin, "rTop", "rMiddle");
		}
		if (newPosition.equals(MIDDLE_BOTTOM_STRAT)) {
			return setPropertiesStrat(env, coin, "rMiddle", "rBottom");
		}

		if (newPosition.equals(ENABLED)) {
			return setStatus(env, coin, 1);
		}
		if (newPosition.equals(DISABLED)) {
			return setStatus(env, coin, 2);
		}

		return ERROR;
	}

	private Double setPropertiesSingle(Environment env, TO<?> coin, String pointer) {
		TO<?> oldSensor;
		String oldPosition;
		Integer oldSensorLiFlag;
		TO<?> sensor;
		try {
			oldSensor = env.getPropertyValue(coin, "sensor", TO.class);
			oldPosition = getOldPosition(env, oldSensor);
			oldSensorLiFlag = env.getPropertyValue(oldSensor, "z", Integer.class);

			PropertyTO sensorPointer = env.getPropertyValue(coin, pointer, PropertyTO.class);
			sensor = env.getPropertyValue(sensorPointer.getObjId(), sensorPointer.getPropertyName(), TO.class);
		} catch (Exception e2) {
			log.error("Could not find old position of sensor for COIN[" + coin.getID() + "]");
			return ERROR;
		}

		try {
			// Clear Control layer li flag from old sensor
			env.setPropertyValue(oldSensor, "z", oldSensorLiFlag & (~CONTROL_BITMASK));

			env.setPropertyValue(coin, "sensor", sensor);

			Integer liFlag = env.getPropertyValue(sensor, "z", Integer.class);
			env.setPropertyValue(sensor, "z", liFlag | CONTROL_BITMASK);

			env.setPropertyValue(coin, "controllable", 1);
			setStatus(env, coin, 1);
		} catch (Exception e) {
			log.warn("Could not set new position of sensor for COIN[" + coin.getID() + "]; Reset to previous position",
			        e);
			try {
				env.setPropertyValue(coin, "position", oldPosition);
				env.setPropertyValue(oldSensor, "z", oldSensorLiFlag);
				env.setPropertyValue(coin, "sensor", oldSensor);
				Integer liFlag = env.getPropertyValue(sensor, "z", Integer.class);
				env.setPropertyValue(sensor, "z", liFlag & (~CONTROL_BITMASK));
			} catch (Exception e1) {
				log.error("Could not reset to previous positions of sensors for COIN[" + coin.getID() + "]", e1);
				return ERROR;
			}

			return ERROR;
		}
		return SUCCESS;

	}

	private Double setPropertiesStrat(Environment env, TO<?> coin, String topPointer, String bottomPointer) {
		String oldPosition;
		try {
			oldPosition = getOldPosition(env, env.getPropertyValue(coin, "top", TO.class),
			        env.getPropertyValue(coin, "bottom", TO.class));
		} catch (Exception e2) {
			log.error("Could not find old position of sensor for COIN[" + coin.getID() + "]");
			return ERROR;
		}

		TO<?> currentTopSensor = null;
		TO<?> currentBottomSensor = null;

		TO<?> topSensor = null;
		TO<?> bottomSensor = null;
		try {
			currentTopSensor = env.getPropertyValue(coin, "top", TO.class);
			currentBottomSensor = env.getPropertyValue(coin, "bottom", TO.class);

			PropertyTO topSensorPointer = env.getPropertyValue(coin, topPointer, PropertyTO.class);
			topSensor = env.getPropertyValue(topSensorPointer.getObjId(), topSensorPointer.getPropertyName(), TO.class);

			PropertyTO bottomSensorPointer = env.getPropertyValue(coin, bottomPointer, PropertyTO.class);
			bottomSensor = env.getPropertyValue(bottomSensorPointer.getObjId(), bottomSensorPointer.getPropertyName(),
			        TO.class);
		} catch (Exception e) {
			log.warn("Could not set position of sensor for COIN[" + coin.getID() + "]", e);
			try {
				env.setPropertyValue(coin, "position", oldPosition);
			} catch (Exception e1) {
				log.error("Could not reset to previous positions of sensors for COIN[" + coin.getID() + "]", e1);
			}
			return ERROR;
		}

		try {
			env.setPropertyValue(coin, "top", topSensor);
			env.setPropertyValue(coin, "bottom", bottomSensor);
			env.setPropertyValue(coin, "controllable", 1);
			setStatus(env, coin, 1);
		} catch (Exception e) {
			log.warn("Could not set new positions of sensors for COIN[" + coin.getID()
			        + "]; Reset to previous position", e);
			try {
				env.setPropertyValue(coin, "top", currentTopSensor);
				env.setPropertyValue(coin, "bottom", currentBottomSensor);
				env.setPropertyValue(coin, "position", oldPosition);
			} catch (Exception e1) {
				log.error("Could not reset to previous positions of sensors for COIN[" + coin.getID() + "]", e1);
			}
			return ERROR;
		}

		return SUCCESS;
	}

	private Double setStatus(Environment env, TO<?> coin, Integer status) {
		Integer currentStatus;
		try {
			currentStatus = env.getPropertyValue(coin, "status", Integer.class);
		} catch (Exception e) {
			log.warn("Could not get current status of COIN[" + coin.getID() + "]", e);
			return ERROR;
		}

		try {
			if (status == 1) {
				if (currentStatus == 2) {
					env.setPropertyValue(coin, "status", status);
				}
			} else {
				if (currentStatus != 2) {
					env.setPropertyValue(coin, "status", status);
				}
			}
		} catch (Exception e) {
			log.warn("Could not set status to COIN[" + coin.getID(), e);
			return ERROR;
		}
		return SUCCESS;
	}

	private String getOldPosition(Environment env, TO<?> sensor) throws Exception {
		Integer z = env.getPropertyValue(sensor, "z", Integer.class);
		// Clear control layer tag
		z = z & (~CONTROL_BITMASK);
		switch (z) {
		case TOP_BITMASK:
			return "t";
		case MIDDLE_BITMASK:
			return "m";
		case BOTTOM_BITMASK:
			return "b";
		}
		throw new Exception("Could not find current position");
	}

	private String getOldPosition(Environment env, TO<?> topSensor, TO<?> bottomSensor) throws Exception {
		return getOldPosition(env, topSensor) + getOldPosition(env, bottomSensor);
	}

}
