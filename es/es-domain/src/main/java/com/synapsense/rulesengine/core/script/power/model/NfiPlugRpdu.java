package com.synapsense.rulesengine.core.script.power.model;

import com.synapsense.dto.CollectionTO;
import com.synapsense.service.Environment;

/**
 * The class implements rpdu parameters computation in case the corresponding
 * rack is instrumented by jawas but 'not fully instrumented (NFI)'. This means
 * that some servers have plug meters and some doesn't not. In this case we are
 * unable to compute accurate values for phase level and hence store 'null' for
 * all the parameters. But aggregation is made to rack level.
 * 
 * @author Dmitry Grudzinskiy
 * 
 */
public class NfiPlugRpdu extends Rpdu {

	public NfiPlugRpdu(CollectionTO objProps, PowerRack parent) {
		super(objProps, parent);
	}

	@Override
	public void process(Environment env) throws Exception {
		aggregateParent(parent);
		// Additional potential maximum
		parent.addPotentialMaxApPower(potentialMaxApPower);
		// We don't store values for NFI rpdus, but we need to update timestamps
		saveNulls(env);
	}
}
