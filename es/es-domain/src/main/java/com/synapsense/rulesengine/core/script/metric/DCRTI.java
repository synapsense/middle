package com.synapsense.rulesengine.core.script.metric;

import java.util.Collection;
import java.util.Date;

import org.apache.log4j.Logger;

import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;
import com.synapsense.rulesengine.core.environment.Property;
import com.synapsense.rulesengine.core.environment.RuleAction;
import com.synapsense.rulesengine.core.environment.RuleI;
import com.synapsense.service.Environment;

public class DCRTI implements RuleAction {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final static Logger logger = Logger.getLogger(DCRTI.class);

	public Object run(final RuleI triggeredRule, final Property calculated) {
        if (logger.isDebugEnabled())
            logger.debug(triggeredRule.getName() + " is run");
		try {
			double res = 0;

			Environment env = calculated.getEnvironment();
			TO<?> nativeTO = calculated.getDataSource().getHostObjectTO();

			Date end = new Date();
			Date start = new Date(end.getTime() - 1800000); // subtract 30
															// minutes

			Collection<TO<?>> ROOMs = env.getRelatedObjects(nativeTO, "ROOM", true);
			if (ROOMs == null || ROOMs.isEmpty()) {
				throw new IllegalStateException("Rule '" + triggeredRule.getName() + "': no Rooms found.");
			} else {
				Double totalAvgInlet = 0.0;
				Double totalAvgDischarge = 0.0;
				Double totalAvgSupply = 0.0;
				Double totalAvgReturn = 0.0;

				int n = 0;

				// iterate over ROOMs within the DC
				for (TO<?> roomTO : ROOMs) {
					Collection<ValueTO> ariValues = env.getHistory(roomTO, "ari", start, end, Double.class);
					Collection<ValueTO> ardValues = env.getHistory(roomTO, "ard", start, end, Double.class);
					Collection<ValueTO> acsValues = env.getHistory(roomTO, "acs", start, end, Double.class);
					Collection<ValueTO> acrValues = env.getHistory(roomTO, "acr", start, end, Double.class);

					if ((ariValues.size() != 0) && (ardValues.size() != 0) && (acsValues.size() != 0)
					        && (acrValues.size() != 0)) {
						totalAvgInlet += calculateAverageValue(ariValues);
						totalAvgDischarge += calculateAverageValue(ardValues);
						totalAvgSupply += calculateAverageValue(acsValues);
						totalAvgReturn += calculateAverageValue(acrValues);
						n++;
					}
				}

				if (n != 0) {
					totalAvgInlet = totalAvgInlet / n;
					totalAvgDischarge = totalAvgDischarge / n;
					totalAvgSupply = totalAvgSupply / n;
					totalAvgReturn = totalAvgReturn / n;

					if ((totalAvgDischarge - totalAvgInlet) > 0) {
						res = (totalAvgReturn - totalAvgSupply) / (totalAvgDischarge - totalAvgInlet);

						// Also calculate 1/RTI and save it
						if (res != 0) {
							env.setPropertyValue(nativeTO, "irti", ((1 / res) * 100));
						}

						// Convert RTI value to percents
						res = res * 100;
					} else {
						if (logger.isDebugEnabled())
							logger.debug(triggeredRule.getName() + ": deltaT is negative or no Racks in a DC.");
						return null;
					}
				} else {
					if (logger.isDebugEnabled())
						logger.debug(triggeredRule.getName() + ": no data available");
					return null;
				}
			}


            if (logger.isDebugEnabled())
                logger.debug(triggeredRule.getName() + " is finished");
			return res;
		} catch (Exception e) {
			logger.error("Rule '" + triggeredRule.getName() + "', Property '" + calculated.getName() + "': error", e);
			return null;
		}
	}

	private Double calculateAverageValue(Collection<ValueTO> values) {
		Double res = 0.0;

		for (ValueTO dbl : values) {
			res += (Double) dbl.getValue();
		}

		res = res / values.size();
		;

		return res;
	}
}