package com.synapsense.rulesengine.core.script.power.model;

import com.synapsense.dto.CollectionTO;
import com.synapsense.service.Environment;

/**
 * The class implements phase parameters computation in case the corresponding
 * rack is instrumented by jawas but 'not fully instrumented (NFI)'. This means
 * that some servers have plug meters and some doesn't not. In this case we are
 * unable to compute accurate values for phase level and hence store 'null' for
 * all the parameters.
 * 
 * @author Dmitry Grudzinskiy
 * 
 */
public class NfiPlugPhase extends Phase {

	public NfiPlugPhase(CollectionTO objProps, Rpdu parent) {
		super(objProps, parent);
	}

	@Override
	protected void calculate(Environment env) throws Exception {
		// We do not calculate anything for NFI phase
	}

	@Override
	protected void save(Environment env) throws Exception {
		// We don't store values for NFI phases, but we need to update
		// timestamps
		saveNulls(env);
	}

	@Override
	protected void aggregateParent(PowerObject parent) {
		super.aggregateParent(parent);
		// Potential maximum
		parent.addPotentialMaxApPower(potentialMaxApPower);
	}
}
