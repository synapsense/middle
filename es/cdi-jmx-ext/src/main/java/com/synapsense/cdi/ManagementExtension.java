package com.synapsense.cdi;

import java.lang.management.ManagementFactory;
import java.util.Set;

import javax.ejb.Singleton;
import javax.enterprise.context.spi.CreationalContext;
import javax.enterprise.event.Observes;
import javax.enterprise.inject.spi.AnnotatedType;
import javax.enterprise.inject.spi.Extension;
import javax.enterprise.inject.spi.InjectionPoint;
import javax.enterprise.inject.spi.InjectionTarget;
import javax.enterprise.inject.spi.ProcessInjectionTarget;
import javax.management.MBeanServer;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Simple CDI extension for automatic JMX registration of
 * EJB singletons marked with @Mbean annotation
 * @author Oleg Stepanov
 *
 */
public class ManagementExtension implements Extension {

	private static Logger log = LoggerFactory
			.getLogger(ManagementExtension.class);

	public <T> void processInjectionTarget(@Observes ProcessInjectionTarget<T> pit) {

		// check if the MBean annotation is present
		AnnotatedType<T> at = pit.getAnnotatedType();
		if (at.isAnnotationPresent(MBean.class)) {
			// it makes sense to register JMX interfaces only for singletons
			if (!at.isAnnotationPresent(Singleton.class)) {
				log.warn("Ignoring attemt to register JMX interface for a non-singleton EJB: "
						+ at.getJavaClass().getName());
				return;
			}

			try {
				// decorate the InjectionTarget
				InjectionTarget<T> delegate = pit.getInjectionTarget();
				InjectionTarget<T> wrapper = new JmxInjectionTarget<T>(delegate, getObjectName(at));
	
				// change the InjectionTarget with the decorated one
				pit.setInjectionTarget(wrapper);
			} catch (Exception e) {
				log.warn("Cannot get JMX object name for: " + at.getJavaClass().getName(), e);
			}

		}
	}

	private <T> ObjectName getObjectName(AnnotatedType<T> at) throws MalformedObjectNameException {

		String name = at.getAnnotation(MBean.class).value();

		if (name.isEmpty()) {
			name = at.getJavaClass().getPackage().getName() + ":type="
					+ at.getJavaClass().getSimpleName();
		}

		return new ObjectName(name);
	}

	private class JmxInjectionTarget<T> implements InjectionTarget<T> {

		private final InjectionTarget<T> d;
		private final ObjectName objectName;
		
		public JmxInjectionTarget(InjectionTarget<T> delegate, ObjectName objectName) {
			this.d = delegate;
			this.objectName = objectName;
		}
		@Override
		public void dispose(T instance) {
			d.dispose(instance);
		}

		@Override
		public Set<InjectionPoint> getInjectionPoints() {
			return d.getInjectionPoints();
		}

		@Override
		public T produce(CreationalContext<T> ctx) {
			return d.produce(ctx);
		}

		@Override
		public void inject(T instance, CreationalContext<T> ctx) {
			d.inject(instance, ctx);
			//the next piece of code better be done in postConstruct but...
			//got no idea why but postConstruct never gets called
			//for Singleton EJB bean
			MBeanServer mBeanServer = ManagementFactory.getPlatformMBeanServer();
			try {
			    if(mBeanServer.isRegistered(objectName))
				mBeanServer.unregisterMBean(objectName);
			    mBeanServer.registerMBean(instance, objectName);
			} catch (Exception e) {
				log.warn("Cannot register "+objectName, e);
				return;
			}
			log.info("added JMX registration for: " + objectName);
		}

		@Override
		public void postConstruct(T instance) {
			d.postConstruct(instance);
		}

		@Override
		public void preDestroy(T instance) {
			d.preDestroy(instance);
		}
		
	}
}

