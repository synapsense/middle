package com.synapsense.plugin;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.channels.spi.AbstractSelector;
import java.nio.channels.spi.SelectorProvider;

import mockit.Deencapsulation;
import mockit.Mocked;
import mockit.NonStrictExpectations;

import org.junit.Before;
import org.junit.Test;

import com.synapsense.modbus.transports.HLTransport;
import com.synapsense.modbus.transports.LLTCPTransport;
import com.synapsense.modbus.transports.MBTCPWrapper;

public class LLTCPTransportTest {
	LLTCPTransport lltr;
	@Mocked
	SocketChannel ch;
	@Mocked
	AbstractSelector selector;
	@Mocked
	SelectorProvider selProv;
	@Mocked
	ServerSocketChannel socketChannel;
	@Mocked
	SocketChannel socketChan;
	@Mocked
	ServerSocket serverSocket;
	@Mocked
	Socket socket;

	@Before
	public void beforeTest() throws IOException {

		new NonStrictExpectations() {
			{
				SelectorProvider.provider();
				result = selProv;
				selProv.openSelector();
				result = selector;
			}

		};

		lltr = new LLTCPTransport("127.0.0.1", 4444);

	}

	@Test
	public void testSendResponse() throws IOException {
		boolean isStarted = true;
		Deencapsulation.setField(lltr, "isStarted", isStarted);
		byte[] data = { 00, 01, 00, 00, 00, 06, 01, 04, 00, 00, 00, 18 };
		lltr.sendResponse(ch, data);
		lltr.stopAll();

	}

	@Test
	public void testSend() throws IOException, InterruptedException {
		boolean isStarted = true;
		new NonStrictExpectations() {
			{
				SocketChannel socketChannel = null;
				SocketChannel.open();
				returns(socketChannel = socketChan);
				socketChannel.socket();
				result = socket;
				socketChannel.finishConnect();
				result = true;
			}
		};
		Deencapsulation.setField(lltr, "isStarted", isStarted);
		byte[] data = { 00, 01, 00, 00, 00, 06, 01, 04, 00, 00, 00, 18 };
		lltr.send("localhost", 4444, data);
		lltr.stopAll();

	}

	@Test
	public void testStartServer() throws Exception {
		new NonStrictExpectations() {
			{
				ServerSocketChannel.open();
				result = socketChannel;
				socketChannel.socket();
				result = serverSocket;

			}
		};
		lltr.startServer();
		lltr.init();
		lltr.stopAll();

	}

	@Test
	public void testSetHLTransport() throws InterruptedException, IOException {
		HLTransport hltr = new MBTCPWrapper("127.0.0.1", lltr);
		lltr.setHLTransport(hltr);
		assertEquals(hltr, lltr.getHLTransport());
		lltr.stopAll();

	}

}
