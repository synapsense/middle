package com.synapsense.plugin;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class TestHelperEX {
	static File tmpFile;

	static public File createConfigFile() {
		tmpFile = new File("modbusPlugin.tmp");
		tmpFile.setWritable(true);
		StringBuilder sb = new StringBuilder();

		sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<DMConfig xmlns=\"http://www.synapsense.com/plugin/apinew\"\n");
		sb.append("xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n");
		sb.append("\txsi:schemaLocation=\"http://www.synapsense.com/plugin/apinew/dmPluginConfig.xsd\">\n");
		sb.append("\t<plugin name=\"modbus\">\n");
		sb.append("\t\t<obj id=\"MODBUSNETWORK:316\">\n");
		sb.append("\t\t\t<prop name=\"port\" value=\"TCP:192.168.200.183\" />\n");
		sb.append("\t\t\t<prop name=\"parity\" value=\"2\" />\n");
		sb.append("\t\t\t<prop name=\"stopbits\" value=\"1\" />\n");
		sb.append("\t\t\t<prop name=\"name\" value=\"PDIBCMS\">\n");
		sb.append("\t\t\t\t<tag name=\"pluginId\" value=\"modbus\" />\n");
		sb.append("\t\t\t</prop>\n");
		sb.append("\t\t\t<prop name=\"databits\" value=\"8\" />\n");
		sb.append("\t\t\t<prop name=\"timeout\" value=\"3000\" />\n");
		sb.append("\t\t\t<prop name=\"baudrate\" value=\"9600\" />\n");
		sb.append("\t\t\t<obj id=\"MODBUSDEVICE:745\">\n");
		sb.append("\t\t\t\t<prop name=\"id\" value=\"4\" />\n");
		sb.append("\t\t\t\t<prop name=\"maxpacketlen\" value=\"42\" />\n");
		sb.append("\t\t\t\t<obj id=\"MODBUSPROPERTY:806\">\n");
		sb.append("\t\t\t\t\t<prop name=\"id\" value=\"40196\" />\n");
		sb.append("\t\t\t\t\t<prop name=\"scale\" value=\"10.0\" />\n");
		sb.append("\t\t\t\t\t<prop name=\"msw\" value=\"0\" />\n");
		sb.append("\t\t\t\t\t<prop name=\"lastValue\" value=\"\">\n");
		sb.append("\t\t\t\t\t\t<tag name=\"poll\" value=\"9000\"/>\n");
		sb.append("\t\t\t\t\t</prop>\n\t\t\t\t</obj>\n");
		sb.append("\t\t\t\t<obj id=\"MODBUSPROPERTY:807\">\n");
		sb.append("\t\t\t\t\t<prop name=\"id\" value=\"40197\" />\n");
		sb.append("\t\t\t\t\t<prop name=\"scale\" value=\"10.0\" />\n");
		sb.append("\t\t\t\t\t<prop name=\"msw\" value=\"40198\" />\n");
		sb.append("\t\t\t\t\t<prop name=\"lastValue\" value=\"\" >\n");
		sb.append("\t\t\t\t\t\t<tag name=\"poll\" value=\"9000\"/>\n");
		sb.append("\t\t\t\t\t\t<tag name=\"elConvGet\" value=\"#Double(#Integer(#getPropertyValue(#parentObjId, \'msw\'))* 2^16 + #Integer(#value))*#Double(#getPropertyValue(#parentObjId, \'scale\'))\"/>\n");
		sb.append("\t\t\t\t\t</prop>\n");
		sb.append("\t\t\t\t</obj>\n");
		sb.append("\t\t\t\t<obj id=\"MODBUSPROPERTY:804\">\n");
		sb.append("\t\t\t\t\t<prop name=\"id\" value=\"40194\" />\n");
		sb.append("\t\t\t\t\t<prop name=\"msw\" value=\"0\" />\n");
		sb.append("\t\t\t\t\t<prop name=\"lastValue\" value=\"\">\n");
		sb.append("\t\t\t\t\t\t<tag name=\"poll\" value=\"9000\"/>\n");
		sb.append("\t\t\t\t\t</prop>\n\t\t\t\t</obj>\n\t\t\t</obj>\n");
		sb.append("\t\t</obj>\n\t</plugin>\n");
		sb.append("</DMConfig>");
		try {
			FileWriter fw = new FileWriter(tmpFile);
			fw.append(sb.toString());
			fw.flush();
			fw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return tmpFile;
	}

	static public void deleteConfigFile() {
		if (tmpFile != null) {
			tmpFile.delete();
		}
	}
}
