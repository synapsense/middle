package com.synapsense.plugin;

import java.io.IOException;

import mockit.Mocked;
import mockit.NonStrictExpectations;

import org.junit.Before;
import org.junit.Test;

import com.synapsense.modbus.MBEClient;
import com.synapsense.modbus.commands.AbstractCommand;
import com.synapsense.modbus.commands.Command01;
import com.synapsense.modbus.commands.CommandsFactory;
import com.synapsense.modbus.exceptions.ModbusException;
import com.synapsense.modbus.transports.TransportWrapper;

public class MBEClientTest {
	@Mocked
	TransportWrapper transport;

	CommandsFactory comFac;

	AbstractCommand command;
	AbstractCommand response = new Command01();
	MBEClient client;

	@Before
	public void beforeTest() {
		client = new MBEClient(transport);
		Integer[] data = { 0, 1, 0, 3, 45, 23 };
		response = new Command01();
		response.setData(data);
		response.setByteCounter(3);
		response.setErrorCode(0);
		response.setReadingAddress(200);
		response.setRDataCounter(1);
		response.setSlaveAddress(1);
	}

	@SuppressWarnings("unused")
	@Test
	public void testRead() throws IOException, ModbusException {
		client.init();
		command = CommandsFactory.getCommand(1);
		new NonStrictExpectations() {
			{
				transport.sendRequest((AbstractCommand) any);
				result = response;
			}
		};
		Integer[] data = client.read(1, 2000, 1);
		Integer[] data2 = client.read(1, 12000, 1);
		Integer[] data3 = client.read(1, 32000, 1);
		Integer[] data4 = client.read(1, 42000, 1);

	}

	@Test
	public void testWrite() throws ModbusException {
		client.init();
		Integer[] data = { 0, 1, 0, 3, 45, 23 };
		Integer[] dataNull = { 1 };
		new NonStrictExpectations() {
			{
				transport.sendRequest((AbstractCommand) any);
				result = response;

			}
		};
		client.write(1, 2000, data);
		client.write(1, 42000, data);
		client.write(1, 2000, dataNull);
		client.write(1, 42000, dataNull);

	}

	@Test
	public void testReadWrite() throws ModbusException {
		Integer[] data = { 0, 1, 0, 3, 45, 23 };
		int slave = 3;
		int addr_w = 50;
		int addr_r = 87;
		int number = 3;
		new NonStrictExpectations() {
			{
				transport.sendRequest((AbstractCommand) any);
				result = response;
			}
		};
		client.readWrite(slave, addr_r, addr_w, number, data);
		client.close();
	}
}