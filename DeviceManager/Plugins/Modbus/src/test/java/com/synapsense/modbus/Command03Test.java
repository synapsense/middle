package com.synapsense.modbus;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.synapsense.modbus.commands.AbstractCommand;
import com.synapsense.modbus.commands.CommandsFactory;
import com.synapsense.modbus.exceptions.IllegalModbusCommandException;
import com.synapsense.modbus.exceptions.ModbusException;

public class Command03Test {
	AbstractCommand command;
	List<AbstractCommand> commandList;

	@Before
	public void beforeTest() throws IllegalModbusCommandException {
		command = CommandsFactory.getCommand(0x03);

	}

	@Test
	public void testAssembleRequestData() throws ModbusException {
		Integer[] data = { 00, 01, 02, 03, 04 };
		command.setRDataCounter(3);
		command.setReadingAddress(41000);
		command.setData(data);
		command.setSlaveAddress(1);
		command.assembleRequestPacket();

	}

	@Test
	public void testAssembleResponseData() throws ModbusException {
		Integer[] data = { 00, 01, 02, 03, 04 };
		command.setRDataCounter(3);
		command.setReadingAddress(12);
		command.setData(data);
		command.setSlaveAddress(1);
		command.assembleResponsePacket();
	}

	@Test
	public void testparseRequestPacket() {
		List<Byte> pac = new ArrayList<Byte>();
		pac.add((byte) 00);
		pac.add((byte) 03);
		pac.add((byte) 02);
		pac.add((byte) 03);
		pac.add((byte) 00);
		pac.add((byte) 01);
		pac.add((byte) 02);
		pac.add((byte) 03);
		command.parseRequestPacket(pac);
	}

	@Test
	public void testparseResponsePacket() {
		List<Byte> pac = new ArrayList<Byte>();
		pac.add((byte) 00);
		pac.add((byte) 03);
		pac.add((byte) 02);
		pac.add((byte) 03);
		pac.add((byte) 00);
		pac.add((byte) 01);
		pac.add((byte) 02);
		pac.add((byte) 03);
		command.parseResponsePacket(pac);

	}

	@SuppressWarnings("static-access")
	@Test
	public void testpackettoHex() {
		byte[] data = { 00, 01, 00, 00, 00, 06, 01, 04, 00, 00, 00, 18 };
		command.packetToHexStr(data);
	}

	@SuppressWarnings("deprecation")
	@Test
	public void otherTest() {
		command.setFunctionCode(2);
		assertEquals((int) 2, (int) command.getFunctionCode());
		command.setSlaveAddress(1);
		assertEquals((int) 1, (int) command.getSlaveAddress());
		command.setReadingAddress(12);
		assertEquals((int) 12, (int) command.getReadingAddress());
		command.setWrittingAddress(3);
		assertEquals((int) 3, (int) command.getWrittingAddress());
		command.setRDataCounter(3);
		assertEquals((int) 3, (int) command.getRDataCounter());
		command.setWDataCounter(2);
		assertEquals((int) 2, (int) command.getWDataCounter());
		command.setByteCounter(4);
		assertEquals((int) 4, (int) command.getByteCounter());
		Integer[] data = { 00, 01, 02, 03, 04 };
		command.setData(data);
		assertEquals(data, command.getData());
		assertNull(command.getCrc16());
		assertEquals((int) 3, (int) command.getCommandType());
		command.setErrorCode(0);
		assertEquals((int) 0, (int) command.getErrorCode());

	}
}
