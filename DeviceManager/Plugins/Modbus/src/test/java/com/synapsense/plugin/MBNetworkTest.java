package com.synapsense.plugin;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.io.File;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.synapsense.modbus.exceptions.ModbusException;
import com.synapsense.plugin.api.exceptions.ConfigPluginException;
import com.synapsense.plugin.apinew.PluginConfig;
import com.synapsense.plugin.configuration.DmXmlConfigReader;
import com.synapsense.plugin.modbus.MBNetwork;

public class MBNetworkTest {
	PluginConfig config;
	MBNetwork network;
	File file;

	@Before
	public void beforeTest() throws ConfigPluginException {
		file = TestHelperEX.createConfigFile();
		Map<String, PluginConfig> confAllPlugin = DmXmlConfigReader.loadPluginsConfig(file);
		config = confAllPlugin.get("modbus");
		network = new MBNetwork(config.getObject("MODBUSNETWORK:316"));
	}

	@After
	public void afterTest() {
		TestHelperEX.deleteConfigFile();
	}

	//@Test
	public void createWithoutRetriesAndTimeout() throws ConfigPluginException, ModbusException {

		Map<String, PluginConfig> confAllPlugin = DmXmlConfigReader.loadPluginsConfig(file);
		PluginConfig config = confAllPlugin.get("modbus");
		MBNetwork network = new MBNetwork(config.getObject("MODBUSNETWORK:316"));
		network.init();
		network.disconnect();
	}

	//@Test
	public void createWitnIncorrectValue() throws ConfigPluginException, ModbusException {

		Map<String, PluginConfig> confAllPlugin = DmXmlConfigReader.loadPluginsConfig(file);
		PluginConfig config = confAllPlugin.get("modbus");
		MBNetwork network = new MBNetwork(config.getObject("MODBUSNETWORK:316"));
		network.init();
		network.disconnect();
	}

	@Test
	public void test() {
		network.start();
	}

	//@Test
	public void testInit() throws ModbusException {
		network.init();
		network.disconnect();
	}

	@Test
	public void otherTest() {
		assertEquals(3, network.getRetries());
		assertEquals(3000, network.getTimeout());
		assertNull(network.getClient());
		assertEquals("{}", network.getStorages().toString());
	}
}
