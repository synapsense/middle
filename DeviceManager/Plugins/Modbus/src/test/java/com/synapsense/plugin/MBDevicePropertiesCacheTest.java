package com.synapsense.plugin;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import mockit.Deencapsulation;
import mockit.Mocked;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.synapsense.modbus.MBEClient;
import com.synapsense.modbus.exceptions.ModbusException;
import com.synapsense.plugin.api.exceptions.ConfigPluginException;
import com.synapsense.plugin.apinew.ObjectDesc;
import com.synapsense.plugin.apinew.PluginConfig;
import com.synapsense.plugin.configuration.DmXmlConfigReader;
import com.synapsense.plugin.modbus.cache.MBDevicePropertiesCache;
import com.synapsense.plugin.modbus.cache.RegisterInf;

public class MBDevicePropertiesCacheTest {
	MBDevicePropertiesCache mbDevice;
	@Mocked
	MBEClient client;
	PluginConfig config;
	Map<Integer, RegisterInf> values;

	@Before
	public void beforeTest() throws ConfigPluginException, ModbusException {
		File file = TestHelperEX.createConfigFile();
		Map<String, PluginConfig> confAllPlugin = DmXmlConfigReader.loadPluginsConfig(file);
		config = confAllPlugin.get("modbus");
		ObjectDesc device = config.getObject("MODBUSDEVICE:745");
		mbDevice = new MBDevicePropertiesCache(client, device, 3, 3000);

		values = new HashMap<Integer, RegisterInf>();
		RegisterInf ri = new RegisterInf();
		ri.updateTime = 100;
		values.put(1, ri);

	}

	@After
	public void afterTest() {
		TestHelperEX.deleteConfigFile();
	}

	@Test
	public void testgetValue() throws ModbusException {
		Deencapsulation.setField(mbDevice, "values", values);
		mbDevice.getValue(1);
	}

	@Test
	public void testGetUpdateTime() {
		Deencapsulation.setField(mbDevice, "values", values);
		assertEquals(100, mbDevice.getUpdateTime(1));
	}

	//@Test
	public void testStartStop() throws InterruptedException {
		Deencapsulation.setField(mbDevice, "values", values);
		mbDevice.start();
		Thread.sleep(1000);
		mbDevice.stop();
	}

	@Test
	public void testSetValue() throws ModbusException {
		Deencapsulation.setField(mbDevice, "values", values);
		mbDevice.setValue(1, 1);
	}

}
