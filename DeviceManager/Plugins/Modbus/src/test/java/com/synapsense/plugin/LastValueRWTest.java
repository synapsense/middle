package com.synapsense.plugin;

import com.synapsense.modbus.MBEClient;
import com.synapsense.modbus.exceptions.ModbusException;
import com.synapsense.plugin.modbus.LastValueRW;
import com.synapsense.plugin.modbus.cache.MBDevicePropertiesCache;
import junit.framework.Assert;
import mockit.Expectations;
import mockit.Mocked;
import mockit.NonStrictExpectations;
import org.junit.Test;

public class LastValueRWTest {
	LastValueRW lv;
	@Mocked
	MBDevicePropertiesCache storage;
	@Mocked
	MBEClient client;

	@Test
	public void testReadINT16() throws ModbusException {
		new NonStrictExpectations() {
			{
				storage.getValue(1);
				result = 1;
			}
		};
		LastValueRW.read("INT16", storage, 1, 3);
		LastValueRW.read("UINT16", storage, 1, 3);

	}

	@Test
	public void testWriteINT16() throws ModbusException {
		LastValueRW.write("INT16", storage, 1, 3, 3);
		LastValueRW.write("UINT16", storage, 1, 3, 3);
	}

	@Test
	public void testReadUINT16LargerThanShortMax() throws ModbusException {
		Number dataOut = 0x8000;
		new Expectations() {
			{
				storage.getValue(1);
				result = dataOut;
			}
		};
		Number res = LastValueRW.read("UINT16", storage, 1, 3);
		Assert.assertEquals(res, dataOut);

	}

	@Test
	public void testReadUINT16() throws ModbusException {
		Number dataOut = 0x1;
		new Expectations() {
			{
				storage.getValue(1);
				result = dataOut;
			}
		};
		Number res = LastValueRW.read("UINT16", storage, 1, 3);
		Assert.assertEquals(res, dataOut);

	}


	@Test
	public void testReadUINT16Max() throws ModbusException {
		Number dataOut = Integer.toUnsignedLong((int)Math.pow(2,16)-1); // Max Unsigned Short value 2**16-1
		new Expectations() {
			{
				storage.getValue(1);
				result = dataOut;
			}
		};
		Number res = LastValueRW.read("UINT16", storage, 1, 3);
		Assert.assertEquals(res.intValue(), dataOut.intValue());
	}



	@Test
	public void testReadINT32() throws ModbusException {
		new NonStrictExpectations() {
			{
				storage.getValue(1);
				result = 1;
				storage.getValue(3);
				result = 3;
			}
		};
		LastValueRW.read("INT32", storage, 1, 3);
		LastValueRW.read("UINT32", storage, 1, 3);
	}

	@Test
	public void testWriteINT32() throws ModbusException {
		LastValueRW.write("INT32", storage, 1, 3, 3);
		LastValueRW.write("UINT32", storage, 1, 3, 3);
	}

	@Test
	public void testReadFloat32() throws ModbusException {
		new NonStrictExpectations() {
			{
				storage.getValue(1);
				result = 1;
				storage.getValue(3);
				result = 3;
			}
		};
		LastValueRW.read("FLOAT32", storage, 1, 3);

	}

	@Test
	public void testWriteFloat32() throws ModbusException {
		LastValueRW.write("FLOAT32", storage, 1, 3, 3);
	}

}
