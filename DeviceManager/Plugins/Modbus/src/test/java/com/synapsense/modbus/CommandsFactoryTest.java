package com.synapsense.modbus;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.synapsense.modbus.commands.AbstractCommand;
import com.synapsense.modbus.commands.Command01;
import com.synapsense.modbus.commands.Command02;
import com.synapsense.modbus.commands.Command03;
import com.synapsense.modbus.commands.Command04;
import com.synapsense.modbus.commands.Command05;
import com.synapsense.modbus.commands.Command06;
import com.synapsense.modbus.commands.Command0F;
import com.synapsense.modbus.commands.Command10;
import com.synapsense.modbus.commands.Command17;
import com.synapsense.modbus.commands.CommandsFactory;
import com.synapsense.modbus.exceptions.IllegalModbusCommandException;
import com.synapsense.modbus.exceptions.ModbusException;

public class CommandsFactoryTest {
	CommandsFactory comFac = new CommandsFactory();

	@Test(expected = IllegalModbusCommandException.class)
	public void getCommandTest() throws ModbusException {
		assertEquals(true, CommandsFactory.getCommand((0x01)) instanceof Command01);
		assertEquals(true, CommandsFactory.getCommand((0x02)) instanceof Command02);
		assertEquals(true, CommandsFactory.getCommand((0x03)) instanceof Command03);
		assertEquals(true, CommandsFactory.getCommand((0x04)) instanceof Command04);
		assertEquals(true, CommandsFactory.getCommand((0x05)) instanceof Command05);
		assertEquals(true, CommandsFactory.getCommand((0x06)) instanceof Command06);
		assertEquals(true, CommandsFactory.getCommand((0x0F)) instanceof Command0F);
		assertEquals(true, CommandsFactory.getCommand((0x10)) instanceof Command10);
		assertEquals(true, CommandsFactory.getCommand((0x17)) instanceof Command17);
		CommandsFactory.getCommand(345);

	}

	@Test(expected = IllegalModbusCommandException.class)
	public void testgetCommand() throws IllegalModbusCommandException {
		byte[] pac01 = { 0x01, 0x01 };
		AbstractCommand command = CommandsFactory.getCommand(pac01);
		assertEquals(true, command instanceof Command01);
		byte[] pac02 = { 0x01, 0x02 };
		command = CommandsFactory.getCommand(pac02);
		assertEquals(true, command instanceof Command02);
		byte[] pac03 = { 0x01, 0x03 };
		command = CommandsFactory.getCommand(pac03);
		assertEquals(true, command instanceof Command03);
		byte[] pac04 = { 0x01, 0x04 };
		command = CommandsFactory.getCommand(pac04);
		assertEquals(true, command instanceof Command04);
		byte[] pac05 = { 0x01, 0x05 };
		command = CommandsFactory.getCommand(pac05);
		assertEquals(true, command instanceof Command05);
		byte[] pac06 = { 0x01, 0x06 };
		command = CommandsFactory.getCommand(pac06);
		assertEquals(true, command instanceof Command06);
		byte[] pac0F = { 0x01, 0x0F };
		command = CommandsFactory.getCommand(pac0F);
		assertEquals(true, command instanceof Command0F);
		byte[] pac10 = { 0x01, 0x10 };
		command = CommandsFactory.getCommand(pac10);
		assertEquals(true, command instanceof Command10);
		byte[] pac17 = { 0x01, 0x17 };
		command = CommandsFactory.getCommand(pac17);
		assertEquals(true, command instanceof Command17);
		byte[] pacError = {};
		command = CommandsFactory.getCommand(pacError);
	}
}
