package com.synapsense.plugin;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import mockit.Mocked;
import mockit.NonStrictExpectations;

import org.junit.Before;
import org.junit.Test;

import com.synapsense.modbus.commands.AbstractCommand;
import com.synapsense.modbus.exceptions.IllegalModbusCommandException;
import com.synapsense.modbus.exceptions.ModbusException;
import com.synapsense.modbus.transports.LLTCPTransport;
import com.synapsense.modbus.transports.LLTransport;
import com.synapsense.modbus.transports.MBTCPWrapper;
import com.synapsense.modbus.utils.Waiter;

public class MBTCPWrapperTest {
	LLTransport lltr;
	MBTCPWrapper wrapper;
	@Mocked
	AbstractCommand c;
	@Mocked
	AbstractCommand response;
	@Mocked
	Waiter w;

	@Before
	public void beforeTest() throws IOException {
		lltr = new LLTCPTransport("127.0.0.1", 4444);
		wrapper = new MBTCPWrapper("127.0.0.1", lltr);
	}

	//@Test
	public void testOnData() throws IllegalModbusCommandException {

		byte[] b1 = { 00, 02, 00, 00, 00, 06, 01, 04, 00, 00, 00, 18 };
		wrapper.onData(b1);
	}

	//@Test
	public void testSendRequest() throws ModbusException {
		final byte[] b1 = { 00, 01, 00, 00, 00, 06, 01, 04, 00, 00, 00, 18 };
		new NonStrictExpectations() {
			{
				c.assembleRequestPacket();
				result = b1;
				int delay = 3000;
				@SuppressWarnings("unused")
				AbstractCommand res = (AbstractCommand) w.sleep(delay);
				returns(res = c);
			}
		};
		wrapper.sendRequest(c);

	}

	//@Test
	public void otherTest() throws ModbusException, IOException {
		assertEquals(3000, wrapper.getDelay());
		assertEquals("127.0.0.1", wrapper.getPort());
		assertEquals(lltr, wrapper.getTransport());
		wrapper.setDelay(2);
		assertEquals(2, wrapper.getDelay());
		wrapper.init();
		wrapper.listen();
		wrapper.close();

	}

}
