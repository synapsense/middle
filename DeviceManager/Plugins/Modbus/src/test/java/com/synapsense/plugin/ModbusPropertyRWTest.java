package com.synapsense.plugin;

import java.io.File;
import java.util.Map;

import mockit.Mocked;
import mockit.NonStrictExpectations;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.synapsense.modbus.exceptions.ModbusException;
import com.synapsense.plugin.api.exceptions.ConfigPluginException;
import com.synapsense.plugin.apinew.ObjectDesc;
import com.synapsense.plugin.apinew.PluginConfig;
import com.synapsense.plugin.apinew.PropertyDesc;
import com.synapsense.plugin.configuration.DmXmlConfigReader;
import com.synapsense.plugin.modbus.LastValueRW;
import com.synapsense.plugin.modbus.ModbusConstants;
import com.synapsense.plugin.modbus.ModbusPropertyRW;
import com.synapsense.plugin.modbus.cache.MBDevicePropertiesCache;

public class ModbusPropertyRWTest {
	@Mocked
	LastValueRW lastValue;
	@Mocked
	MBDevicePropertiesCache storage;
	@Mocked
	ObjectDesc obj;
	@Mocked
	PropertyDesc property;

	public static final String PROPERTY_LAST_VALUE = "id";
	PluginConfig config;

	@Before
	public void beforeTest() throws ConfigPluginException {
		File file = TestHelperEX.createConfigFile();
		Map<String, PluginConfig> confAllPlugin = DmXmlConfigReader.loadPluginsConfig(file);
		config = confAllPlugin.get("modbus");
	}

	@After
	public void afterTest() {
		TestHelperEX.deleteConfigFile();
	}

	@Test
	public void testReadProperty() throws NumberFormatException, ModbusException {
		new NonStrictExpectations() {
			{
				property.getName();
				result = "lastValue";
				property.getParent();
				result = obj;
				obj.getPropertyValue(ModbusConstants.PROPERTY_TYPE);
				result = "INT16";
				obj.getPropertyValue(ModbusConstants.PROPERTY_ID);
				result = "220";
				obj.getPropertyValue(ModbusConstants.PROPERTY_MSW);
				result = "0";
				LastValueRW.read("INT16", storage, 220, 0);
				result = 1;
			}
		};
		ModbusPropertyRW.readProperty(property, storage);
	}

	@Test
	public void testReadPropertyWithDifferentName() throws NumberFormatException, ModbusException {
		PropertyDesc property = config.getObject("MODBUSPROPERTY:807").getProperty("id");
		ModbusPropertyRW.readProperty(property, storage);
	}

	@Test
	public void testWriteProperty() throws NumberFormatException, ModbusException {
		new NonStrictExpectations() {
			{
				property.getName();
				result = "lastValue";
				property.getParent();
				result = obj;
				obj.getPropertyValue(ModbusConstants.PROPERTY_SCALE);
				result = "10.0";
				obj.getPropertyValue(ModbusConstants.PROPERTY_ID);
				result = "220";
				obj.getPropertyValue(ModbusConstants.PROPERTY_MSW);
				result = "0";
			}
		};
		ModbusPropertyRW.writeProperty(property, storage, 1);
	}

	@Test
	public void testWritePropertyWithDifferentName() throws NumberFormatException, ModbusException {
		PropertyDesc property = config.getObject("MODBUSPROPERTY:807").getProperty("id");
		ModbusPropertyRW.writeProperty(property, storage, 12);
	}

}
