package com.synapsense.modbus.transports;

import java.io.IOException;

import com.synapsense.modbus.exceptions.ModbusException;

public abstract class HLTransport implements TransportWrapper {
	protected int delay = 3000; // ms
	private LLTransport transport;
	private String address;
	protected String clientHostname;
	protected int clientPort = 502;

	abstract public byte[] onData(byte[] b1);

	public void initHLTransport(String address, LLTransport transport) {
		this.transport = transport;
		this.address = address;
		transport.setHLTransport(this);
		if (address != null && address.length() >= 4) {
			String addr = address.substring(4, address.length());
			String[] tmp = addr.split(":");
			clientHostname = tmp[0];
			if (tmp.length > 1) {
				clientPort = Integer.parseInt(tmp[1]);
			}
		}
	}

	public String getPort() {
		return address;
	}

	public void processData(Object channel, byte[] data) throws IOException {
		byte[] ret = onData(data);
		if (ret != null) {
			transport.sendResponse(channel, ret);
		}
	}

	public void send(byte[] packet) throws IOException {
		transport.send(clientHostname, clientPort, packet);
	}

	public void listen() throws IOException {
		transport.startServer();
	}

	public void close() {
		transport.stopAll();
	}

	public void init() throws ModbusException {
		try {
			transport.init();
		} catch (Exception e) {
			throw new ModbusException(e);
		}
	}

	public LLTransport getTransport() {
		return transport;
	}

	@Override
	public int getDelay() {
		return delay;
	}

	@Override
	public void setDelay(int delay) {
		this.delay = delay;
	}

}
