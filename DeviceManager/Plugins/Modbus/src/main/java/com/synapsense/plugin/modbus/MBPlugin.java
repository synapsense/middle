package com.synapsense.plugin.modbus;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.synapsense.modbus.exceptions.ModbusException;
import com.synapsense.plugin.api.exceptions.ConfigPluginException;
import com.synapsense.plugin.api.exceptions.IncorrectStateException;
import com.synapsense.plugin.api.exceptions.UnableGetDataException;
import com.synapsense.plugin.apinew.ObjectDesc;
import com.synapsense.plugin.apinew.Plugin;
import com.synapsense.plugin.apinew.PluginConfig;
import com.synapsense.plugin.apinew.PropertyDesc;
import com.synapsense.plugin.apinew.Transport;
import com.synapsense.plugin.modbus.cache.MBDevicePropertiesCache;

public class MBPlugin implements Plugin {
	private final static Logger logger = Logger.getLogger(MBPlugin.class);
	private Map<String, MBNetwork> allNets = new HashMap<String, MBNetwork>();
	private Transport transport;
	private Boolean started = false;
	private Boolean configured = false;
	private final static String ID = "modbus";
	private static Properties PLUGIN_DESC = new Properties();
	static {
		PLUGIN_DESC.put("PLUGIN_ID", ID);
		PLUGIN_DESC.put("ROOT_TYPES", "MODBUSNETWORK");
	}

	@Override
	public void configure(PluginConfig config) throws ConfigPluginException {
		stop();
		for (ObjectDesc modbusNetwork : config.getRoots()) {
			MBNetwork net;
			String name = modbusNetwork.getPropertyValue(ModbusConstants.NET_NAME);
			net = new MBNetwork(modbusNetwork);
			try {
				net.init();
			} catch (ModbusException e1) {
				logger.warn("Modbus network: " + name + " with main port: "
				        + modbusNetwork.getPropertyValue(ModbusConstants.NET_ADDRESS)
				        + " failed to initialize, it will be skipped");
				if (logger.isDebugEnabled()) {
					logger.debug("Details: " + e1.getLocalizedMessage());
				}
			}
			allNets.put(name, net);
			for (ObjectDesc device : modbusNetwork.getChildrenByType(ModbusConstants.TYPE_DEVICE)) {
				try {
					MBDevicePropertiesCache st = new MBDevicePropertiesCache(net.getClient(), device, net.getRetries(),
					        net.getTimeout());
					net.getStorages().put(device.getID(), st);
				} catch (ModbusException e) {
					throw new ConfigPluginException(
					        "Unable to configure Modbus plugin. Error creating the MBStorage object", e);
				}
			}
		}
		synchronized (configured) {
			configured = true;
		}
		logger.debug("Plugin " + ID + " is configured.");
	}

	@Override
	public String getID() {
		return ID;
	}

	@Override
	public Properties getPluginDescription() {
		return PLUGIN_DESC;
	}

	@Override
	public void start() throws IncorrectStateException {
		if (!configured) {
			throw new IncorrectStateException("Plugin " + ID + " has to be configured before start.");
		}
		synchronized (configured) {
			if (!started) {
				for (MBNetwork obj : allNets.values()) {
					try {
						((MBNetwork) obj).start();
					} catch (Exception e) {
						logger.error("Cannot start network polling.", e);
					}
				}
				started = true;
				logger.debug("Plugin " + ID + " is started.");
			}
		}
	}

	@Override
	public void stop() {
		if (started) {
			for (MBNetwork nt : allNets.values()) {
				nt.disconnect();
			}
		}
		TransportManager.clear();
		allNets.clear();
		configured = false;
		started = false;
		logger.debug("Plugin " + ID + " is stopped.");
	}

	public Transport getTransport() {
		return transport;
	}

	public void setTransport(Transport transport) {
		this.transport = transport;
	}

	@Override
	public Serializable getPropertyValue(ObjectDesc object, String propertyName) {
		Serializable result = null;
		if (object != null) {
			PropertyDesc prop = object.getProperty(propertyName);
			ObjectDesc parentDevice = object.getParent();
			ObjectDesc parentNetwork = parentDevice.getParent();
			String name = parentNetwork.getPropertyValue(ModbusConstants.NET_NAME);
			MBNetwork network = allNets.get(name);
			if (prop != null && network != null) {
				try {
					result = ModbusPropertyRW.readProperty(prop, network.getStorages().get(parentDevice.getID()));
				} catch (ModbusException e) {
					throw new UnableGetDataException("Cannot read property [" + propertyName
					        + "] from modbus device with network name [" + name + "], address ["
					        + parentNetwork.getPropertyValue(ModbusConstants.NET_ADDRESS) + "], device ID ["
					        + parentDevice.getPropertyValue(ModbusConstants.DEVICE_NUMBER) + "] and register ["
					        + object.getPropertyValue(ModbusConstants.PROPERTY_ID) + "]. Reason:" + e.getMessage());
				}
			}
		}
		return result;
	}

	@Override
	public List<Serializable> getPropertyValue(ObjectDesc object, List<String> propertyNames) {
		List<Serializable> result = new ArrayList<Serializable>();
		for (String property : propertyNames) {
			result.add(getPropertyValue(object, property));
		}
		return result;
	}

	@Override
	public void setPropertyValue(ObjectDesc object, String propertyName, Serializable value) {
		if (object != null && value instanceof Integer) {
			ObjectDesc parentDevice = object.getParent();
			ObjectDesc parentNetwork = parentDevice.getParent();
			String name = parentNetwork.getPropertyValue(ModbusConstants.NET_NAME);
			MBNetwork network = allNets.get(name);
			PropertyDesc prop = object.getProperty(propertyName);
			if (prop != null && network != null) {
				logger.debug("Set Property Value: " + object.getID() + "." + propertyName + "=" + value);
				try {
					ModbusPropertyRW.writeProperty(prop, network.getStorages().get(parentDevice.getID()),
					        (Number) value);
				} catch (ModbusException e) {
					logger.error("Cannot write property [" + propertyName + "] from modbus device with network name ["
					        + name + "], address [" + parentNetwork.getPropertyValue(ModbusConstants.NET_ADDRESS)
					        + "], device ID [" + parentDevice.getPropertyValue(ModbusConstants.DEVICE_NUMBER)
					        + "] and register [" + object.getPropertyValue(ModbusConstants.PROPERTY_ID) + "]. Reason: "
					        + e.getMessage());
				}
			}

		}
	}

	@Override
	public void setPropertyValue(ObjectDesc object, List<String> propertyNames, List<Serializable> values) {
		if (propertyNames == null || values == null || propertyNames.size() != values.size()) {
			throw new IllegalArgumentException("Unable to set properties values.");
		}
		for (int i = 0; i < propertyNames.size(); i++) {
			setPropertyValue(object, propertyNames.get(i), values.get(i));
		}
	}

}
