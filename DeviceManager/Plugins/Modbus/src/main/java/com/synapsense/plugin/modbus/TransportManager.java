package com.synapsense.plugin.modbus;

import java.io.IOException;
import java.util.HashMap;

import com.synapsense.modbus.transports.LLTCPTransport;
import com.synapsense.modbus.transports.LLTransport;
import com.synapsense.modbus.transports.MBTCPWrapper;
import com.synapsense.modbus.transports.TransportWrapper;

public class TransportManager {
	private static HashMap<String, TransportWrapper> addrToWrapper = new HashMap<String, TransportWrapper>();

	private static String getTypeOfAddr(String a) {
		if (a != null && a.length() >= 4) {
			String type = a.substring(0, 3);
			return type;
		}
		return "";
	}

	public static TransportWrapper createWrapper(String clientAddr) throws IOException {
		TransportWrapper trwr = addrToWrapper.get(clientAddr);
		if (trwr != null)
			return trwr;
		LLTransport t = null;
		TransportWrapper tw = null;
		if ("TCP".equalsIgnoreCase(getTypeOfAddr(clientAddr))) {
			t = new LLTCPTransport(null, 0);
			tw = new MBTCPWrapper(clientAddr, t);
		}
		addrToWrapper.put(clientAddr, tw);
		return tw;
	}

	public static void clear() {
		addrToWrapper.clear();
	}
}
