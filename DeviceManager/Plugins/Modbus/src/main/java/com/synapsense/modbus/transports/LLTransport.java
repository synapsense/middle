package com.synapsense.modbus.transports;

import java.io.IOException;

public interface LLTransport {
	public void sendResponse(Object ch, byte[] data) throws IOException;

	public void send(String hostName, int port, byte[] data) throws IOException;

	public void startServer() throws IOException;

	public void stopAll();

	public void init() throws Exception;

	public void setHLTransport(HLTransport transport);

	public HLTransport getHLTransport();
}
