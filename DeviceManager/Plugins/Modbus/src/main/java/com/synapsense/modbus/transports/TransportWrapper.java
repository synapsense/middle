package com.synapsense.modbus.transports;

import com.synapsense.modbus.commands.AbstractCommand;
import com.synapsense.modbus.exceptions.ModbusException;

public interface TransportWrapper {
	public int getDelay();

	public void setDelay(int d);

	public AbstractCommand sendRequest(AbstractCommand c) throws ModbusException;

	public void init() throws ModbusException;

	public void close();

	public byte[] onData(byte[] b);
}
