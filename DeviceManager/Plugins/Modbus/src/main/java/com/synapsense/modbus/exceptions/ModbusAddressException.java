package com.synapsense.modbus.exceptions;

public class ModbusAddressException extends ModbusException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2944595603448473252L;

	public ModbusAddressException() {
		super();
	}

	public ModbusAddressException(final String message, final Throwable cause) {
		super(message, cause);
	}

	public ModbusAddressException(final String message) {
		super(message);
	}

	public ModbusAddressException(final Throwable cause) {
		super(cause);
	}
}
