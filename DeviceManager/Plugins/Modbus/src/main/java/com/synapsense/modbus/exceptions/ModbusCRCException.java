package com.synapsense.modbus.exceptions;

public class ModbusCRCException extends ModbusException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6291715130090292039L;

	public ModbusCRCException() {
		super();
	}

	public ModbusCRCException(final String message) {
		super(message);
	}

	public ModbusCRCException(final Throwable cause) {
		super(cause);
	}

	public ModbusCRCException(final String message, final Throwable cause) {
		super(message, cause);
	}

}
