package com.synapsense.modbus.exceptions;

public class ModbusReadException extends ModbusException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8436834793389246895L;

	public ModbusReadException() {
		super();
	}

	public ModbusReadException(final String message, final Throwable cause) {
		super(message, cause);
	}

	public ModbusReadException(final String message) {
		super(message);
	}

	public ModbusReadException(final Throwable cause) {
		super(cause);
	}
}
