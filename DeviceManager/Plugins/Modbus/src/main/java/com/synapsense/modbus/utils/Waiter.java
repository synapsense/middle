package com.synapsense.modbus.utils;

public class Waiter {
	private Integer wfcLock = new Integer(0);
	private Object data = null;

	public Object sleep(long t, boolean condition) {
		if (condition)
			return true;
		synchronized (wfcLock) {
			try {
				wfcLock.wait(t);
			} catch (Exception e) {
			}
		}
		return this.data;
	}

	public Object sleep(long t) {
		synchronized (wfcLock) {
			try {
				if (data == null) {
					wfcLock.wait(t);
				}
			} catch (Exception e) {
			}
		}
		return this.data;
	}

	public void wakeup(Object data) {
		synchronized (wfcLock) {
			this.data = data;
			wfcLock.notify();
		}
	}
}
