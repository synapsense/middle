package com.synapsense.plugin.modbus.cache;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class Splitter {
	/**
	 * This method splits the provided registers following these rules:
	 * <ul>
	 * <li>One group has a set of registers, which number follows each other.</li>
	 * <li>Registers in the same group have the same poll period.</li>
	 * <li>Number of registers in group is limited by maxNumber parameter.</li>
	 * </ul>
	 * 
	 * @param properties
	 *            Properties to split.
	 * @param maxNumber
	 *            Max number of registers in the group.
	 * @return Splitted registers.
	 */
	public static List<PacketInformation> splitList2(Map<Integer, RegisterInf> properties, int maxNumber) {
		LinkedList<PacketInformation> pkl = new LinkedList<PacketInformation>();
		Integer[] registers = new Integer[properties.size()];
		int i = 0;
		for (Integer regAddr : properties.keySet()) {
			registers[i] = regAddr;
			++i;
		}
		Arrays.sort(registers);
		PacketInformation pk = new PacketInformation();
		int pre = registers[0];
		pk.address = properties.get(registers[0]).address;
		pk.numbers = 1;
		pkl.add(pk);

		for (i = 1; i < registers.length; i++) {
			int cur = registers[i];
			int npp = cur - (pre + 1);
			if (npp == 0 && pk.numbers < maxNumber) {
				pk.numbers++;
			} else {
				pk = new PacketInformation();
				pk.address = properties.get(registers[i]).address;
				pk.numbers = 1;
				pkl.add(pk);
			}
			pre = cur;
		}
		return pkl;
	}
}
