/**
 * 
 */
package com.synapsense.plugin.modbus;

/**
 * @author stikhon
 * 
 */
public class ModbusConstants {
	public static final String TYPE_NETWORK = "MODBUSNETWORK";
	public static final String TYPE_DEVICE = "MODBUSDEVICE";
	public static final String TYPE_PROPERTY = "MODBUSPROPERTY";

	public static final String NET_ADDRESS = "port";
	public static final String NET_NAME = "name";
	public static final String NET_RETRIES = "retries";
	public static final String NET_TIMEOUT = "timeout";

	public static final String DEVICE_NUMBER = "id";
	public static final String DEVICE_MAX_PACKET_LENGTH = "maxpacketlen";

	public static final String PROPERTY_ID = "id";
	public static final String PROPERTY_LAST_VALUE = "lastValue";
	public static final String PROPERTY_SCALE = "scale";
	public static final String PROPERTY_TYPE = "type";
	public static final String PROPERTY_MSW = "msw";

	public static final int DEFAULT_RETRIES_NUMBER = 3;
	public static final int DEFAULT_TIMEOUT = 3000;
}
