package com.synapsense.modbus.commands;

import com.synapsense.modbus.exceptions.ModbusException;

/**
 * @author Dmitry Koryshev
 * 
 *         Class extends {@link AbstractCommand}
 * 
 */
public class Command03 extends AbstractCommand {

	public Command03() {
		super();
		commandType = 0x03;
	}

	@Override
	protected void assembleRequestData() throws ModbusException {
		if (readingAddress == null) {
			throw new ModbusException("readingAddress is null.");
		}
		if (readingAddress < 40000 || readingAddress > 49999) {
			throw new ModbusException("Illegal readingAddress.\n" + "\tNow address is " + readingAddress + "\n"
			        + "\tbut address must be between 40000 and 49999");
		}
		if (rDataCounter == null) {
			throw new ModbusException("rDataCounter is null.");
		}
		packet = new byte[8];
		packet[2] = hiInt16(readingAddress - 40000);
		packet[3] = loInt16(readingAddress - 40000);
		packet[4] = hiInt16(rDataCounter);
		packet[5] = loInt16(rDataCounter);
	}

	@Override
	protected void assembleResponseData() throws ModbusException {
		if (data == null) {
			throw new ModbusException("data is null.");
		}
		byteCounter = data.length * 2;
		packet = new byte[2 + 1 + byteCounter + 2];
		packet[2] = loInt16(byteCounter);

		for (int i = 0; i < data.length; ++i) {
			packet[3 + 2 * i] = hiInt16(data[i]);
			packet[3 + 2 * i + 1] = loInt16(data[i]);
		}
	}

	@Override
	protected int parseRequestData() {
		int n = 2; // index
		int len = packet.length;
		// len<slaveAddess:functionCode:readingAddress:rDataCounter:crc16?
		// len<1+1+2+2+2?
		if (len < 8) {
			return -2;
		}
		readingAddress = dbToi(packet[n], packet[n + 1]);
		n += 2;
		readingAddress += 40000;
		if (readingAddress < 40000 || readingAddress > 49999) {
			return -4;
		}
		rDataCounter = dbToi(packet[n], packet[n + 1]);
		n += 2;
		return n;
	}

	@Override
	protected int parseResponseData() {
		int n = 2; // index
		int len = packet.length;
		// len<slaveAddess:functionCode:byteCounter?
		// len<1+1+1?
		if (len < 3) {
			return -2;
		}
		byteCounter = bToi(packet[n]);
		++n;
		// len<slaveAddess:functionCode:byteCounter:data:crc16?
		// len<1+1+1+byteCounter+2?
		if (len < 5 + byteCounter) {
			return -2;
		}
		data = new Integer[byteCounter / 2];
		int i; // index
		for (i = 0; i < data.length; ++i) {
			data[i] = dbToi(packet[3 + 2 * i], packet[3 + 2 * i + 1]);
		}
		n += i * 2;
		return n;
	}

	/**
	 * Wraps assembleRequestPacket() method
	 * 
	 * @param slaveAddress
	 *            is a slave address
	 * @param readingAddress
	 *            is a reading address
	 * @param rDataCounter
	 *            is a reading data counter
	 * @return assembled packet
	 * @throws ModbusException
	 */
	public byte[] assembleRequestPacket(final Integer slaveAddress, final Integer readingAddress,
	        final Integer rDataCounter) throws ModbusException {
		this.slaveAddress = slaveAddress;
		this.readingAddress = readingAddress;
		this.rDataCounter = rDataCounter;
		return super.assembleRequestPacket();
	}

	/**
	 * 
	 * Wraps assembleResponsePacket() method
	 * 
	 * @param slaveAddress
	 *            is a slave address
	 * @param data
	 *            is a data
	 * @return assembled packet
	 * @throws ModbusException
	 */
	public byte[] assembleResponsePacket(final Integer slaveAddress, final Integer[] data) throws ModbusException {
		this.slaveAddress = slaveAddress;
		this.data = data.clone();
		return super.assembleResponsePacket();
	}

}
