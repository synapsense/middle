package com.synapsense.modbus.exceptions;

public class ModbusException extends Exception {

	public ModbusException() {
		super();
	}

	public ModbusException(final String message, final Throwable cause) {
		super(message, cause);
	}

	public ModbusException(final String message) {
		super(message);
	}

	public ModbusException(final Throwable cause) {
		super(cause);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 4076821421533806430L;

}
