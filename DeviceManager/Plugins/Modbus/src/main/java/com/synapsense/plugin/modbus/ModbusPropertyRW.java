package com.synapsense.plugin.modbus;

import java.io.Serializable;

import org.apache.log4j.Logger;

import com.synapsense.modbus.exceptions.ModbusException;
import com.synapsense.plugin.apinew.ObjectDesc;
import com.synapsense.plugin.apinew.PropertyDesc;
import com.synapsense.plugin.modbus.cache.MBDevicePropertiesCache;

/**
 * 
 * @author stikhon
 * 
 */

public class ModbusPropertyRW {

	private final static Logger logger = Logger.getLogger(ModbusPropertyRW.class);

	public static Serializable readProperty(final PropertyDesc property, final MBDevicePropertiesCache storage)
	        throws NumberFormatException, ModbusException {
		// TODO fix properties names
		if (ModbusConstants.PROPERTY_LAST_VALUE.equals(property.getName())) {
			ObjectDesc obj = property.getParent();
			logger.debug("trying to read " + obj.getPropertyValue(ModbusConstants.PROPERTY_TYPE).toString() + " from "
			        + obj.getPropertyValue(ModbusConstants.PROPERTY_ID));
			Double val = LastValueRW.read(obj.getPropertyValue(ModbusConstants.PROPERTY_TYPE), storage,
			        new Integer(obj.getPropertyValue(ModbusConstants.PROPERTY_ID)),
			        new Integer(obj.getPropertyValue(ModbusConstants.PROPERTY_MSW))).doubleValue();
			// return val / new Double(
			// obj.getPropertyValue(ModbusConstants.PROPERTY_SCALE));
			// scaling handled by MVEL script in elCOnvGet tag
			return val;
		} else {
			return property.getValue();
		}

	}

	public static void writeProperty(final PropertyDesc property, final MBDevicePropertiesCache storage,
	        final Number value) throws NumberFormatException, ModbusException {
		if (ModbusConstants.PROPERTY_LAST_VALUE.equals(property.getName())) {
			ObjectDesc obj = property.getParent();
			Double val = value.doubleValue() * new Double(obj.getPropertyValue(ModbusConstants.PROPERTY_SCALE));
			Integer intVal = val.intValue();
			LastValueRW.write(obj.getPropertyValue(ModbusConstants.PROPERTY_TYPE), storage,
			        new Integer(obj.getPropertyValue(ModbusConstants.PROPERTY_ID)),
			        new Integer(obj.getPropertyValue(ModbusConstants.PROPERTY_MSW)), intVal);
		} else {
			property.setValue(value.toString());
		}

	}
}
