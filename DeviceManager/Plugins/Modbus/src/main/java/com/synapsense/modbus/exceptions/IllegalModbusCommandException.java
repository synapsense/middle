package com.synapsense.modbus.exceptions;

public class IllegalModbusCommandException extends ModbusException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6513888950772578179L;

	public IllegalModbusCommandException() {
		super();
	}

	public IllegalModbusCommandException(final String message, final Throwable cause) {
		super(message, cause);
	}

	public IllegalModbusCommandException(final String message) {
		super(message);
	}

	public IllegalModbusCommandException(final Throwable cause) {
		super(cause);
	}
}
