package com.synapsense.plugin.modbus;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.synapsense.modbus.exceptions.ModbusException;
import com.synapsense.plugin.modbus.cache.MBDevicePropertiesCache;

public class LastValueRW {
	private static Map<String, RegistersRW> registersRWs = new HashMap<String, RegistersRW>();
	private final static Logger logger = Logger.getLogger(LastValueRW.class);
	static {
		registersRWs.put("INT16", new RegistersRW() {
			@Override
			public void write(MBDevicePropertiesCache storage, int high, int low, Number value) throws ModbusException {
				int intVal = value.intValue();
				storage.setValue(high, intVal);
			}

			@Override
			public Number read(MBDevicePropertiesCache storage, int high, int low) throws ModbusException {

				logger.debug("For an INT16, storage reports reg " + high + " has value "
				        + storage.getValue(high).toString());
				int v = storage.getValue(high);
				if ((v & 0x8000) != 0) {
					v = (v + 32768) % 65536 - 32768;
				}
				return v;
			}
		});
		registersRWs.put("UINT16", new RegistersRW() {
			@Override
			public void write(MBDevicePropertiesCache storage, int high, int low, Number value) throws ModbusException {
				int intVal = value.intValue();
				storage.setValue(high, intVal);
			}

			@Override
			public Number read(MBDevicePropertiesCache storage, int high, int low) throws ModbusException {
				return storage.getValue(high);
			}
		});
		registersRWs.put("INT32", new RegistersRW() {
			@Override
			public void write(MBDevicePropertiesCache storage, int high, int low, Number value) throws ModbusException {
				int intVal = value.intValue();
				storage.setValue(high, (intVal >> 16) & 0xFFFF);
				storage.setValue(low, intVal & 0xFFFF);
			}

			@Override
			public Number read(MBDevicePropertiesCache storage, int high, int low) throws ModbusException {
				int v = ((storage.getValue(high) << 16) + storage.getValue(low));

				if ((v & 0x80000000) != 0) {
					v = ((-1) ^ 0xFFFFFFFF) | v;
				}

				return v;
			}
		});
		registersRWs.put("UINT32", new RegistersRW() {
			@Override
			public void write(MBDevicePropertiesCache storage, int high, int low, Number value) throws ModbusException {
				int intValue = value.intValue();
				storage.setValue(high, (intValue >> 16) & 0xFFFF);
				storage.setValue(low, intValue & 0xFFFF);
			}

			@Override
			public Number read(MBDevicePropertiesCache storage, int high, int low) throws ModbusException {
				long hiV = storage.getValue(high);
				long loV = storage.getValue(low);
				return (hiV << 16 | loV);
			}
		});
		registersRWs.put("FLOAT32", new RegistersRW() {
			@Override
			public void write(MBDevicePropertiesCache storage, int high, int low, Number value) throws ModbusException {
				int intVal = Float.floatToIntBits(value.floatValue());
				storage.setValue(high, (intVal >> 16) & 0xFFFF);
				storage.setValue(low, intVal & 0xFFFF);
			}

			@Override
			public Number read(MBDevicePropertiesCache storage, int high, int low) throws ModbusException {
				int v = ((storage.getValue(high) << 16) + storage.getValue(low));
				return Float.intBitsToFloat(v);

			}
		});
	}

	public static Number read(String type, MBDevicePropertiesCache storage, int high, int low) throws ModbusException {
		RegistersRW reader = registersRWs.get(type);
		if (reader != null) {
			return reader.read(storage, high, low);
		} else {
			throw new ModbusException("No suitable reader is found for type [" + type + "]");
		}
	}

	public static void write(String type, MBDevicePropertiesCache storage, int high, int low, Number value)
	        throws ModbusException {
		RegistersRW writer = registersRWs.get(type);
		if (writer != null) {
			writer.write(storage, high, low, value);
		} else {
			throw new ModbusException("No suitable writer is found for type [" + type + "]");
		}
	}
}

interface RegistersRW {
	public Number read(MBDevicePropertiesCache storage, int high, int low) throws ModbusException;

	public void write(MBDevicePropertiesCache storage, int high, int low, Number value) throws ModbusException;
}
