package com.synapsense.plugin.modbus;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.synapsense.modbus.MBEClient;
import com.synapsense.modbus.exceptions.ModbusException;
import com.synapsense.modbus.transports.TransportWrapper;
import com.synapsense.plugin.api.exceptions.PropertyNotFoundException;
import com.synapsense.plugin.apinew.ObjectDesc;
import com.synapsense.plugin.modbus.cache.MBDevicePropertiesCache;

public class MBNetwork {
	private ObjectDesc network;
	private MBEClient cl = null;
	private int retries;
	private int timeout;
	private Map<String, MBDevicePropertiesCache> storages;

	private final static Logger logger = Logger.getLogger(MBNetwork.class);

	public MBNetwork(ObjectDesc network) {
		this.network = network;
		try {
			retries = Integer.parseInt(network.getPropertyValue(ModbusConstants.NET_RETRIES));
		} catch (PropertyNotFoundException e) {
			logger.warn("The 'retries' parameter for network [" + network.getPropertyValue(ModbusConstants.NET_NAME)
			        + "] is not found in the provided configuration, default value("
			        + ModbusConstants.DEFAULT_RETRIES_NUMBER + ") will be used instead.");
			retries = ModbusConstants.DEFAULT_RETRIES_NUMBER;
		} catch (NumberFormatException e) {
			logger.warn("The 'retries' parameter for network is not a number, default value("
			        + ModbusConstants.NET_RETRIES + ") will be used instead.");
			if (logger.isDebugEnabled()) {
				logger.debug("The 'retries' parameter contains the following as a value: "
				        + network.getPropertyValue(ModbusConstants.NET_RETRIES));
			}
			retries = ModbusConstants.DEFAULT_RETRIES_NUMBER;
		}
		try {
			timeout = Integer.parseInt(network.getPropertyValue(ModbusConstants.NET_TIMEOUT));
		} catch (PropertyNotFoundException e) {
			logger.warn("The 'timeout' parameter for network [" + network.getPropertyValue(ModbusConstants.NET_NAME)
			        + "] is not found in the provided configuration, default value(" + ModbusConstants.DEFAULT_TIMEOUT
			        + ") will be used instead.");
			retries = ModbusConstants.DEFAULT_TIMEOUT;
		} catch (NumberFormatException e) {
			logger.warn("The 'timeout' parameter for network is not a number, default value("
			        + ModbusConstants.NET_TIMEOUT + ") will be used instead.");
			if (logger.isDebugEnabled()) {
				logger.debug("The 'timeout' parameter contains the following as a value: "
				        + network.getPropertyValue(ModbusConstants.NET_TIMEOUT));
			}
			retries = ModbusConstants.DEFAULT_TIMEOUT;
		}
		storages = new HashMap<String, MBDevicePropertiesCache>();
	}

	public void start() {
		for (MBDevicePropertiesCache st : storages.values()) {
			st.start();
		}
	}

	public void init() throws ModbusException {
		try {
			String mPort = network.getPropertyValue(ModbusConstants.NET_ADDRESS);

			TransportWrapper tw = TransportManager.createWrapper(mPort);
			tw.setDelay(timeout);
			cl = new MBEClient(tw);
			cl.init();
		} catch (NumberFormatException e) {
			throw new ModbusException(e);
		} catch (IOException e) {
			throw new ModbusException(e);
		} catch (PropertyNotFoundException e) {
			if (logger.isDebugEnabled()) {
				logger.debug("Modbus network: " + network.getPropertyValue(ModbusConstants.NET_NAME)
				        + " doesn't have the port configured");
			}
			throw new ModbusException(e);
		}
	}

	public void disconnect() {
		for (MBDevicePropertiesCache st : storages.values()) {
			st.stop();
		}
		storages.clear();
		if (cl != null)
			cl.close();
	}

	public MBEClient getClient() {
		return cl;
	}

	public int getRetries() {
		return retries;
	}

	public Map<String, MBDevicePropertiesCache> getStorages() {
		return storages;
	}

	public int getTimeout() {
		return timeout;
	}

}
