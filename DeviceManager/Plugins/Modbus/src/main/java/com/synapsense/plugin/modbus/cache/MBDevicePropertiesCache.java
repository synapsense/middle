package com.synapsense.plugin.modbus.cache;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.log4j.Logger;

import com.synapsense.modbus.MBEClient;
import com.synapsense.modbus.exceptions.ModbusException;
import com.synapsense.plugin.api.exceptions.ConfigPluginException;
import com.synapsense.plugin.apinew.ObjectDesc;
import com.synapsense.plugin.apinew.TagDesc;
import com.synapsense.plugin.modbus.ModbusConstants;

/**
 * This class is responsible for pre-fetching Modbus register values and caching
 * them. Since some Modbus devices can take some time to respond(provide
 * register value), all values are requested and cached before the actual poll
 * is scheduled. One MBStorage object is created per Device.
 * 
 */
public class MBDevicePropertiesCache {
	private final static Logger logger = Logger.getLogger(MBDevicePropertiesCache.class);
	private int timeout;
	private int retries;
	private int period;
	// device number
	private int slave;
	private int maxNumber;
	// cached values
	private Map<Integer, RegisterInf> values = new HashMap<Integer, RegisterInf>();
	// ModBus reader/writer
	private MBEClient cl;
	// Timer, which holds pre-fetching tasks
	private Timer t;
	private List<PacketInformation> registersPackets;
	private boolean isStarted = false;

	/**
	 * Main constructor.
	 * 
	 * @param cl
	 *            ModBus reader/writer to set.
	 * @param device
	 *            Device object to query.
	 * @throws ConfigPluginException
	 * 
	 */
	public MBDevicePropertiesCache(MBEClient client, ObjectDesc device, int retries, int timeout)
	        throws ModbusException, ConfigPluginException {
		if (client == null) {
			throw new ModbusException("MBEClient is NULL!");
		}
		if (device == null) {
			throw new ModbusException("device is NULL!");
		}
		this.cl = client;
		this.retries = retries;
		this.timeout = timeout;
		this.slave = new Integer(device.getPropertyValue(ModbusConstants.DEVICE_NUMBER));
		this.maxNumber = new Integer(device.getPropertyValue(ModbusConstants.DEVICE_MAX_PACKET_LENGTH));
		List<ObjectDesc> properties = device.getChildren();
		if (properties == null || properties.size() == 0) {
			throw new ConfigPluginException("Modbus device doesn't have properties");
		}
		TagDesc pollTag = properties.get(0).getProperty(ModbusConstants.PROPERTY_LAST_VALUE).getTag("poll");
		if (pollTag == null) {
			throw new ConfigPluginException("Modbus device doesn't have poll period configured");
		}
		try {
			period = new Integer(pollTag.getValue());
		} catch (NumberFormatException e) {
			throw new ConfigPluginException("Modbus device has incorrect poll period configured");
		}
		for (ObjectDesc prop : properties) {
			addProperty(prop);
		}
		registersPackets = Splitter.splitList2(values, maxNumber);
		// trying to cache values 1st time
		try {
			for (PacketInformation packet : registersPackets) {
				Integer[] data = cl.read(slave, packet.address, packet.numbers);
				if (data != null) {
					updateValuesPacketInCache(packet, data);
				} else {
					for (int i = 0; i < packet.numbers; i++) {
						RegisterInf ri = values.get(packet.address + i);
						ri.updateTime = -1;
					}
				}
			}
		} catch (Exception e) {
			logger.warn("Unable to update values cache first time.", e);
		}
	}

	/**
	 * Gets given register's value from cache.
	 * 
	 * @param reg
	 *            Register number to get.
	 * @return Register value.
	 * @throws ModbusException
	 *             Thrown if register is not found in cache or it's never been
	 *             fetched (it's timestamp < 0).
	 */
	public Integer getValue(int reg) throws ModbusException {
		synchronized (values) {
			RegisterInf ri = values.get(reg);
			if (ri == null) {
				throw new ModbusException("Unknown register address:" + reg);
			}
			if (ri.updateTime < 0) {
				throw new ModbusException("Data is unavailable.");
			}
		}
		return values.get(reg).value;
	}

	/**
	 * Gets last update timestamp for the provided register number.
	 * 
	 * @param reg
	 *            Register number to get.
	 * @return Last update timestamp.
	 */
	public long getUpdateTime(int reg) {
		synchronized (values) {
			return values.get(reg).updateTime;
		}
	}

	/**
	 * Sets new property value into cache. The value is sent to the device and
	 * cached.
	 * 
	 * @param reg
	 * @param value
	 * @throws ModbusException
	 */
	public void setValue(int reg, int value) throws ModbusException {
		cl.write(slave, reg, new Integer[] { value });
		synchronized (values) {
			RegisterInf ri = values.get(reg);
			if (ri == null) {
				throw new ModbusException("Unknown register address:" + reg);
			}
			ri.value = value;
			ri.updateTime = System.currentTimeMillis();
		}
	}

	public void start() {
		isStarted = true;
		t = new Timer(true);

		if (logger.isDebugEnabled()) {
			logger.debug("Starting MBDevicePropertyCache timer for DEVICE #" + slave);
		}

		// the task supposed to be scheduled to prefetch values from devices
		// before polling service asks for them
		int schedulingPeriod = period - timeout * retries;
		if (schedulingPeriod < 0) {
			// Dew to non typical configuration we get negative value for
			// period - timeout * retries (either period is too small, or there
			// are to many retries/big timeout). Start the task right away.
			logger.warn("Incorrect scheduling information is provided. Zero delay will be used for the Modbus device cache with id:"
			        + slave + ". \nConfigured period: " + period + ", timeout:" + timeout + ", retries:" + retries);
			schedulingPeriod = 0;
		}
		t.schedule(new ReaderTask(registersPackets), schedulingPeriod, period);
	}

	public void stop() {
		synchronized (values) {
			t.cancel();
			values.clear();
		}
		isStarted = false;
	}

	private void addProperty(ObjectDesc prop) throws ModbusException {
		if (prop == null || !ModbusConstants.TYPE_PROPERTY.equals(prop.getType())) {
			throw new ModbusException("property is NULL or malformed!");
		}
		Integer id = new Integer(prop.getPropertyValue(ModbusConstants.PROPERTY_ID));
		Integer msw = new Integer(prop.getPropertyValue(ModbusConstants.PROPERTY_MSW));

		if (id > 0) {
			this.addRegister(id);

		}
		if (msw > 0) {
			this.addRegister(msw);
		}
	}

	/**
	 * Adds register to the storage.
	 * 
	 * @param regAddr
	 *            Register address.
	 * 
	 * 
	 * @throws ModbusException
	 */
	private void addRegister(int regAddr) throws ModbusException {
		if (isStarted) {
			throw new ModbusException("Pulling is started. You need to stop it before.");
		}
		RegisterInf ri = new RegisterInf();
		ri.address = regAddr;
		synchronized (values) {
			values.put(regAddr, ri);
		}

	}

	private void updateValuesPacketInCache(PacketInformation packet, Integer[] data) {
		int reg = packet.address;
		for (int i = 0; i < data.length; i++) {
			synchronized (values) {
				int regNum = reg + i;
				RegisterInf ri = values.get(regNum);
				ri.value = data[i];
				ri.updateTime = System.currentTimeMillis();
				if (logger.isDebugEnabled()) {
					logger.debug("Register #" + regNum + " is updated with value [" + ri.value + "]");
				}
			}
		}

	}

	private class ReaderTask extends TimerTask {
		private List<PacketInformation> packets;

		public ReaderTask(final List<PacketInformation> packets) {
			super();
			this.packets = new ArrayList<PacketInformation>(packets);
		}

		@Override
		public void run() {
			Integer[] data = null;
			boolean connected = false;
			for (PacketInformation packet : packets) {
				// for all packet blocks we are gonna update values, giving 3
				// tries to each packet
				for (int tryNumber = 0; tryNumber < retries; tryNumber++) {
					try {
						if (logger.isDebugEnabled()) {
							logger.debug("Trying to read register: #" + packet.address);
						}
						data = cl.read(slave, packet.address, packet.numbers);
					} catch (Exception e) {
						logger.warn("Unable to read register" + "\n\tSlave - " + slave + "\n\tRegister #"
						        + packet.address + "\n\tNumber - " + packet.numbers + "\n\tTry #" + (tryNumber + 1)
						        + "/" + retries, e);
						// skip the following check
						continue;
					}
					if (data != null) {
						// we successfully got the data, update values in cache
						updateValuesPacketInCache(packet, data);
						// set the flag
						connected = true;
						// and next packet and skip next tries
						break;
					}
				}
				if (!connected) {
					// if we get here, we were not able to get values for the
					// first
					// packet most likely device doesn't respond, and it's
					// pointless to try to get values for the rest of the
					// packets
					// since they are all from the same device
					break;
				}
			}
			if (!connected) {
				for (PacketInformation packet : packets) {
					for (int i = 0; i < packet.numbers; i++) {
						synchronized (values) {
							RegisterInf ri = values.get(packet.address + i);
							if (ri == null) {
								// the task is no longer in sync with the
								// data
								// in cache, no need to do retries.
								continue;
							}
							ri.updateTime = -1;

						}
					}
					logger.error("Unable to read register" + "\n\tSlave - " + slave + "\n\tRegister #" + packet.address
					        + "\n\tNumber - " + packet.numbers);
				}
			}
		}
	}
}
