package com.synapsense.modbus.commands;

import com.synapsense.modbus.exceptions.ModbusException;

/**
 * @author Dmitry Koryshev
 * 
 *         Class extends {@link AbstractCommand}
 * 
 */

public class Command05 extends AbstractCommand {

	public Command05() {
		super();
		commandType = 0x05;
	}

	@Override
	protected void assembleRequestData() throws ModbusException {
		if (writtingAddress == null) {
			throw new ModbusException("writtingAddress is null.");
		}
		if (writtingAddress < 0 || writtingAddress > 9999) {
			throw new ModbusException("Illegal writtingAddress.");
		}
		if (data == null) {
			throw new ModbusException("data is null.");
		}
		if (data.length != 1) {
			throw new ModbusException("data length isn't equals 1.");
		}
		packet = new byte[8];
		packet[2] = hiInt16(writtingAddress);
		packet[3] = loInt16(writtingAddress);
		if (data[0] != 0)
			data[0] = 0xFF00;
		packet[4] = hiInt16(data[0]);
		packet[5] = loInt16(data[0]);
	}

	@Override
	protected void assembleResponseData() throws ModbusException {
		if (writtingAddress == null) {
			throw new ModbusException("writtingAddress is null.");
		}
		if (writtingAddress < 0 || writtingAddress > 9999) {
			throw new ModbusException("Illegal writtingAddress.");
		}
		if (data == null) {
			throw new ModbusException("data is null.");
		}
		if (data.length != 1) {
			throw new ModbusException("data length isn't equals 1.");
		}
		packet = new byte[8];
		packet[2] = hiInt16(writtingAddress);
		packet[3] = loInt16(writtingAddress);
		if (data[0] != 0)
			data[0] = 0xFF00;
		packet[4] = hiInt16(data[0]);
		packet[5] = loInt16(data[0]);
	}

	@Override
	protected int parseRequestData() {
		int n = 2; // index
		int len = packet.length;
		// len<slaveAddess:functionCode:writtingAddress:data:crc16?
		// len<1+1+2+2+2?
		if (len < 8) {
			return -2;
		}
		writtingAddress = dbToi(packet[n], packet[n + 1]);
		n += 2;
		if (writtingAddress < 0 || writtingAddress > 9999) {
			return -4;
		}
		data = new Integer[1];
		data[0] = dbToi(packet[n], packet[n + 1]);
		if (data[0] != 0)
			data[0] = 1;
		n += 2;
		return n;
	}

	@Override
	protected int parseResponseData() {
		int n = 2; // index
		int len = packet.length;
		// len<slaveAddess:functionCode:writtingAddress:data:crc16?
		// len<1+1+2+2+2?
		if (len < 8) {
			return -2;
		}
		writtingAddress = dbToi(packet[n], packet[n + 1]);
		n += 2;
		if (writtingAddress < 0 || writtingAddress > 9999) {
			return -4;
		}
		data = new Integer[1];
		data[0] = dbToi(packet[n], packet[n + 1]);
		if (data[0] != 0)
			data[0] = 1;
		n += 2;
		return n;
	}

	/**
	 * 
	 * Wraps assembleRequestPacket() method
	 * 
	 * @param slaveAddress
	 *            is a slave address
	 * @param writtingAddress
	 *            is a writing address
	 * @param data
	 *            is a data
	 * @return assembled packet
	 * @throws ModbusException
	 */
	public byte[] assembleRequestPacket(final Integer slaveAddress, final Integer writtingAddress, final Integer[] data)
	        throws ModbusException {
		this.slaveAddress = slaveAddress;
		this.writtingAddress = writtingAddress;
		this.data = data.clone();
		return super.assembleRequestPacket();
	}

	/**
	 * 
	 * Wraps assembleResponsePacket() method
	 * 
	 * @param slaveAddress
	 *            is a slave address
	 * @param writtingAddress
	 *            is a writing address
	 * @param data
	 *            is a data
	 * @return assembled packet
	 * @throws ModbusException
	 */
	public byte[] assembleResponsePacket(final Integer slaveAddress, final Integer writtingAddress, final Integer[] data)
	        throws ModbusException {
		this.slaveAddress = slaveAddress;
		this.writtingAddress = writtingAddress;
		this.data = data.clone();
		return super.assembleResponsePacket();
	}

}
