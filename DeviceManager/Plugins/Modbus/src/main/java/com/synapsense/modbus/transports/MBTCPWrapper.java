package com.synapsense.modbus.transports;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;

import org.apache.log4j.Logger;

import com.synapsense.modbus.commands.AbstractCommand;
import com.synapsense.modbus.commands.CommandsFactory;
import com.synapsense.modbus.exceptions.IllegalModbusCommandException;
import com.synapsense.modbus.exceptions.ModbusException;
import com.synapsense.modbus.utils.Waiter;

public class MBTCPWrapper extends HLTransport {
	private final static Logger logger = Logger.getLogger(MBTCPWrapper.class);
	private HashMap<Integer, Waiter> sidToWaiter = new HashMap<Integer, Waiter>();
	private Integer session = 0;

	public MBTCPWrapper(String addr, LLTransport lltr) {
		super.initHLTransport(addr, lltr);
	}

	private Waiter createWaiter(int sid) {
		synchronized (sidToWaiter) {
			sidToWaiter.put(sid, new Waiter());
			return sidToWaiter.get(sid);
		}
	}

	private Waiter getWaiter(int sid) {
		synchronized (sidToWaiter) {
			return sidToWaiter.get(sid);
		}
	}

	private void removeWaiter(int sid) {
		synchronized (sidToWaiter) {
			sidToWaiter.remove(sid);
		}
	}

	private int getSession() {
		synchronized (session) {
			++session;
			if (session >= 65536) {
				session = 0;
			}
		}
		return session;
	}

	@Override
	public synchronized AbstractCommand sendRequest(AbstractCommand c) throws ModbusException {
		byte[] b = c.assembleRequestPacket();
		byte[] packet = new byte[7 - 1 + b.length - 2];
		// Transaction Identifier
		int sid = getSession();
		packet[0] = AbstractCommand.hiInt16(sid);
		packet[1] = AbstractCommand.loInt16(sid);
		// Protocol Identifier (0x0000 = Modbus)
		packet[2] = 0x00;
		packet[3] = 0x00;
		// Length
		packet[4] = AbstractCommand.hiInt16(b.length - 2);
		packet[5] = AbstractCommand.loInt16(b.length - 2);
		for (int i = 0; i < b.length - 2; i++) {
			packet[6 + i] = b[i];
		}
		Waiter w = createWaiter(sid);
		try {
			super.send(packet);
		} catch (IOException e) {
			throw new ModbusException(e);
		}
		AbstractCommand res = (AbstractCommand) w.sleep(delay);
		removeWaiter(sid);
		if (res == null) {
			throw new ModbusException("Device does not respond." + "\n\tAddress - tcp:" + clientHostname + ":"
			        + clientPort);
		}
		return res;
	}

	private LinkedList<Byte> plist = new LinkedList<Byte>();
	private int plen = 0;

	@Override
	public byte[] onData(byte[] b1) {
		byte[] retm = null;
		Integer rsid = null;
		for (int i = 0; i < b1.length; i++) {
			plist.add(b1[i]);
		}
		int len = plist.size();
		if (len >= 6 && plen == 0) {
			byte[] tmp1 = new byte[plist.size()];
			for (int i = 0; i < plist.size(); i++) {
				tmp1[i] = plist.get(i);
			}
			int packetLen = AbstractCommand.dbToi(tmp1[4], tmp1[5]);
			plen = packetLen;
			if (plen + 6 > b1.length) {
				retm = null;
				return retm;
			}
		} else {
			retm = null;
			return retm;
		}
		plen = 0;
		byte h = plist.poll();
		byte l = plist.poll();
		rsid = AbstractCommand.dbToi(h, l);
		// int protocolid = AbstractCommand.dbToi(tmp1[2], tmp1[3]);
		for (int i = 0; i < 4; i++)
			plist.poll();
		byte[] tmp2 = new byte[plist.size()];
		for (int i = 0; i < plist.size(); i++) {
			tmp2[i] = plist.get(i);
		}
		int crc16 = AbstractCommand.countCRC16(tmp2, tmp2.length);
		byte[] packet = new byte[tmp2.length + 2];
		for (int i = 0; i < tmp2.length; i++) {
			packet[i] = tmp2[i];
		}
		packet[packet.length - 2] = AbstractCommand.loInt16(crc16);
		packet[packet.length - 1] = AbstractCommand.hiInt16(crc16);
		plist.add(packet[packet.length - 2]);
		plist.add(packet[packet.length - 1]);
		AbstractCommand response = null;
		try {
			response = CommandsFactory.getCommand(packet);
			int res1 = response.parseResponsePacket(plist);
			if (res1 > 0) {
				if (rsid != null) {
					Waiter w = getWaiter(rsid);
					if (w != null) {
						w.wakeup(response);
					}
				}
				logger.debug("Received packet:\n\t" + AbstractCommand.packetToHexStr(packet));
				retm = null;
			}
		} catch (IllegalModbusCommandException e) {
			logger.warn("Cannot parse packet.", e);
		} catch (Exception e) {
			logger.error("Cannot assemble response.", e);
		}
		plist.clear();
		return retm;
	}

}
