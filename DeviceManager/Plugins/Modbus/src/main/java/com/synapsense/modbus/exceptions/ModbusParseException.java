package com.synapsense.modbus.exceptions;

public class ModbusParseException extends ModbusException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2961979398511513466L;

	public ModbusParseException() {
		super();
	}

	public ModbusParseException(final String message, final Throwable cause) {
		super(message, cause);
	}

	public ModbusParseException(final String message) {
		super(message);
	}

	public ModbusParseException(final Throwable cause) {
		super(cause);
	}
}
