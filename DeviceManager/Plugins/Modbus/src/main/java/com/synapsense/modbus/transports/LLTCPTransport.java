package com.synapsense.modbus.transports;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.channels.spi.SelectorProvider;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.log4j.Logger;

public class LLTCPTransport extends Thread implements LLTransport {

	private static AtomicInteger id = new AtomicInteger(1);

	public final static long RECONNECTION_DELAY = 120000;
	private final static int MAX_PACKET_LEN = 65536;
	private final static Logger logger = Logger.getLogger(LLTCPTransport.class);
	private boolean stop = false;
	private ServerSocketChannel serverChannel = null;
	private Selector selector;
	private String address;
	private int port;
	private HLTransport transport = null;
	private HashMap<SocketChannel, LinkedList<Byte>> toSend = new HashMap<SocketChannel, LinkedList<Byte>>();
	private boolean addAcceptKey = false;
	private boolean con = true;
	private boolean isStarted = false;
	private Timer ct = new Timer(true);
	private TimerTask cttask = new TimerTask() {
		@Override
		public void run() {
			con = true;
		}
	};

	public LLTCPTransport(String address, int port) throws IOException {
		super(String.valueOf("LLTCPTransport_" + id.getAndIncrement()));
		this.address = address;
		this.port = port;
		ct.schedule(cttask, RECONNECTION_DELAY, RECONNECTION_DELAY);
		selector = SelectorProvider.provider().openSelector();
	}

	@Override
	public void sendResponse(Object ch, byte[] data) {
		if (isStarted) {
			if (ch != null) {
				synchronized (toSend) {
					LinkedList<Byte> buf;
					if (!toSend.containsKey(ch)) {
						buf = new LinkedList<Byte>();
						toSend.put((SocketChannel) ch, buf);
					} else {
						buf = toSend.get(ch);
					}
					for (int i = 0; i < data.length; i++) {
						buf.add(data[i]);
					}
				}
			}
			selector.wakeup();
		}
	}

	private SocketChannel clich = null;

	@Override
	public void send(String hostName, int port, byte[] data) throws IOException {
		if (isStarted) {
			connect(hostName, port);
			synchronized (toSend) {
				LinkedList<Byte> buf;
				if (!toSend.containsKey(clich)) {
					buf = new LinkedList<Byte>();
					toSend.put(clich, buf);
				} else {
					buf = toSend.get(clich);
				}
				for (int i = 0; i < data.length; i++) {
					buf.add(data[i]);
				}
			}
			selector.wakeup();
		}
	}

	private SocketChannel createSocketChannel(String hostName, int port) throws IOException {

		SocketChannel socketChannel = SocketChannel.open();
		socketChannel.configureBlocking(false);
		socketChannel.socket().setKeepAlive(true);
		socketChannel.socket().setTcpNoDelay(true);
		socketChannel.socket().setReuseAddress(true);
		socketChannel.connect(new InetSocketAddress(hostName, port));
		while (!socketChannel.finishConnect()) {
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return socketChannel;
	}

	private ServerSocketChannel createServerSocketChannel(String hostName, int port) throws IOException {

		ServerSocketChannel socketChannel = ServerSocketChannel.open();
		socketChannel.configureBlocking(false);
		socketChannel.socket().setReuseAddress(true);
		socketChannel.socket().bind(new InetSocketAddress(hostName, port));
		return socketChannel;
	}

	private void accept(SelectionKey key) throws IOException {
		ServerSocketChannel serverSocketChannel = (ServerSocketChannel) key.channel();
		SocketChannel socketChannel = serverSocketChannel.accept();
		socketChannel.configureBlocking(false);
		socketChannel.register(this.selector, SelectionKey.OP_READ);
	}

	private void write(SelectionKey key) throws IOException {
		SocketChannel ch = (SocketChannel) key.channel();
		synchronized (toSend) {
			if (toSend.containsKey(ch)) {
				Object[] os = toSend.get(ch).toArray();
				byte[] b = new byte[os.length];
				for (int i = 0; i < os.length; i++) {
					b[i] = (Byte) os[i];
				}
				try {
					ch.write(ByteBuffer.wrap(b));
				} catch (IOException e) {
					toSend.remove(ch);
					ch.close();
					key.cancel();
					logger.warn(e.getLocalizedMessage(), e);
					return;
				}
				toSend.remove(ch);
			}
		}
		ch.register(selector, SelectionKey.OP_READ);
	}

	private int readFromChannel(SocketChannel ch, SelectionKey key, ByteBuffer buf) throws IOException {
		int n = -1;
		try {
			n = ch.read(buf);
		} catch (IOException e) {
			ch.close();
			key.cancel();
			logger.warn(e.getLocalizedMessage(), e);
		}
		return n;
	}

	private void read(SelectionKey key) throws IOException {
		SocketChannel ch = (SocketChannel) key.channel();
		ByteBuffer buf = ByteBuffer.allocate(MAX_PACKET_LEN);
		if (!ch.isOpen())
			return;
		int n;
		n = readFromChannel(ch, key, buf);
		if (n == -1) {
			ch.close();
			key.cancel();
			return;
		}
		while (n > 0) {
			n = readFromChannel(ch, key, buf);
		}
		buf.flip();
		byte[] dataArray = buf.array();
		transport.processData(ch, Arrays.copyOfRange(dataArray, 0, buf.limit()));
	}

	@Override
	public void run() {
		while (!stop) {
			try {
				synchronized (toSend) {
					Set<SocketChannel> channels = toSend.keySet();
					for (SocketChannel channel : channels) {
						if (channel != null && channel.isOpen() && channel.isConnected()) {
							channel.register(selector, SelectionKey.OP_WRITE);
						}
					}
				}
				if (addAcceptKey) {
					if (serverChannel != null && serverChannel.isOpen()) {
						serverChannel.register(selector, SelectionKey.OP_ACCEPT);
					}
					addAcceptKey = false;
				}
				selector.select();
				if (stop) {
					disconnect();
					continue;
				}
				Iterator<SelectionKey> selectedKeys = selector.selectedKeys().iterator();
				while (selectedKeys.hasNext()) {
					SelectionKey key = (SelectionKey) selectedKeys.next();
					selectedKeys.remove();
					if (!key.isValid()) {
						key.cancel();
						continue;
					}
					if (key.isWritable()) {
						write(key);
					} else if (key.isReadable()) {
						read(key);
					} else if (key.isAcceptable()) {
						accept(key);
					}
				}
			} catch (Exception e) {
				logger.error(e.getLocalizedMessage(), e);
			}
		}
	}

	@Override
	public void startServer() throws IOException {
		serverChannel = createServerSocketChannel(address, port);
		addAcceptKey = true;
		selector.wakeup();
	}

	@Override
	public void stopAll() {
		stop = true;
		ct.cancel();
		try {
			selector.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void connect(String hostName, int port) throws IOException {
		if (con) {
			logger.debug("Connecting to " + hostName + ":" + port);
			try {
				if (clich == null || !clich.isOpen()) {
					clich = createSocketChannel(hostName, port);
				}
			} catch (IOException e) {
				con = false;
				throw e;
			}
		}
	}

	private void disconnect() throws IOException {
		selector.close();
		if (serverChannel != null) {
			serverChannel.close();
		}
		if (clich != null) {
			clich.close();
		}
	}

	@Override
	public HLTransport getHLTransport() {
		return transport;
	}

	@Override
	public void setHLTransport(HLTransport transport) {
		this.transport = transport;
	}

	@Override
	public void init() {
		if (!isStarted) {
			this.start();
			isStarted = true;
		}
	}

}
