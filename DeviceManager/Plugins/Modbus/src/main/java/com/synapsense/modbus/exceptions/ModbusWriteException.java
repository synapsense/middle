package com.synapsense.modbus.exceptions;

public class ModbusWriteException extends ModbusException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7324842030551959329L;

	public ModbusWriteException() {
		super();
	}

	public ModbusWriteException(final String message, final Throwable cause) {
		super(message, cause);
	}

	public ModbusWriteException(final String message) {
		super(message);
	}

	public ModbusWriteException(final Throwable cause) {
		super(cause);
	}
}
