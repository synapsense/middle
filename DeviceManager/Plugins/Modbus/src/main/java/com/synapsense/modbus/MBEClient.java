package com.synapsense.modbus;

//import org.apache.log4j.Logger;
import java.util.Arrays;

import com.synapsense.modbus.commands.AbstractCommand;
import com.synapsense.modbus.commands.CommandsFactory;
import com.synapsense.modbus.exceptions.ModbusAddressException;
import com.synapsense.modbus.exceptions.ModbusException;
import com.synapsense.modbus.exceptions.ModbusReadException;
import com.synapsense.modbus.transports.TransportWrapper;

public class MBEClient {
	// private final static Logger logger = Logger.getLogger(MBEClient.class);
	private TransportWrapper transport;

	public MBEClient(TransportWrapper transport) {
		this.transport = transport;
	}

	public Integer[] read(int slave, int addr, int number) throws ModbusException {
		if (transport == null) {
			throw new ModbusException("Initialize the Modbus transport!");
		}
		Integer functionCode = null;
		AbstractCommand com;
		addr--;
		if (addr >= 0 && (addr < 20000 || addr >= 30000) && addr < 50000) {
			if (addr < 10000) {
				functionCode = AbstractCommand.RMCRW;
			} else if (addr < 20000) {
				functionCode = AbstractCommand.RMCRO;
			} else if (addr < 40000) {
				functionCode = AbstractCommand.RMRRO;
			} else if (addr < 50000) {
				functionCode = AbstractCommand.RMRRW;
			}
		} else {
			throw new ModbusAddressException("Address must be >0");
		}
		com = CommandsFactory.getCommand(functionCode);
		com.setSlaveAddress(slave);
		com.setReadingAddress(addr);
		com.setRDataCounter(number);
		AbstractCommand response = transport.sendRequest(com);
		if (response.getErrorCode() != 0) {
			throw new ModbusReadException("Modbus device returns error: " + response.getErrorCode());
		}
		Integer[] data = null;
		Integer[] tdata = response.getData();
		if (tdata.length >= number) {
			data = Arrays.copyOf(tdata, number);
		}
		return data;
	}

	public void write(int slave, int addr, Integer[] data) throws ModbusException {
		if (transport == null) {
			throw new ModbusException("Initialize the Modbus transport!");
		}
		if (data.length == 0) {
			throw new ModbusException("Data is empty.");
		}
		Integer functionCode = null;
		AbstractCommand com;
		addr--;
		if ((addr >= 0 && addr < 10000)) {
			if (data.length > 1) {
				functionCode = AbstractCommand.WMC;
			} else {
				functionCode = AbstractCommand.WOC;
			}
		} else if (addr >= 40000 && addr < 50000) {
			if (data.length > 1) {
				functionCode = AbstractCommand.WMR;
			} else {
				functionCode = AbstractCommand.WOR;
			}
		} else {
			new ModbusAddressException("Address must be between 1 and 10000 or between 40001 and 50000");
		}
		com = CommandsFactory.getCommand(functionCode);
		com.setSlaveAddress(slave);
		com.setWrittingAddress(addr);
		com.setData(data);
		AbstractCommand response = transport.sendRequest(com);
		if (response.getErrorCode() != 0) {
			throw new ModbusReadException("Modbus device returns error: " + response.getErrorCode());
		}
	}

	public Integer[] readWrite(int slave, int addr_r, int addr_w, int number, Integer[] data_w) throws ModbusException {
		if (transport == null) {
			throw new ModbusException("Initialize the Modbus transport!");
		}
		Integer[] data = new Integer[number];
		Integer functionCode = null;
		AbstractCommand com;
		addr_r--;
		functionCode = AbstractCommand.RWMRW;
		com = CommandsFactory.getCommand(functionCode);
		com.setSlaveAddress(slave);
		com.setReadingAddress(addr_r);
		com.setRDataCounter(number);
		com.setWrittingAddress(addr_w);
		com.setData(data_w);
		AbstractCommand response = transport.sendRequest(com);
		if (response.getErrorCode() != 0) {
			throw new ModbusReadException("Modbus device returns error: " + response.getErrorCode());
		}
		data = response.getData();
		return data;
	}

	public void init() throws ModbusException {
		transport.init();
	}

	public void close() {
		transport.close();
	}
}
