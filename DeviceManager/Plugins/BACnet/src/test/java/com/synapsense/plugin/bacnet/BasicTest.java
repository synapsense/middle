package com.synapsense.plugin.bacnet;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import com.synapsense.plugin.api.exceptions.IncorrectStateException;
import com.synapsense.plugin.apinew.ObjectDesc;
import com.synapsense.plugin.apinew.PluginConfig;
import com.synapsense.plugin.apinew.PropertyDesc;
import com.synapsense.plugin.apinew.Transport;
import com.synapsense.plugin.apinew.impl.ObjectDescImpl;
import com.synapsense.plugin.apinew.impl.PropertyDescImpl;
import com.synapsense.plugin.apinew.impl.TagDescImpl;

public class BasicTest {
	static BACnetPlugin plugin;
	static PluginConfig pluginConfig;
	static ObjectDescImpl device;
	static ObjectDescImpl obj1;
	static ObjectDescImpl prop1;
	static ObjectDescImpl prop2;
	static ObjectDescImpl prop3;

	public static void debug(String msg) {
		System.out.println(msg);
	}

	/**
	 * @param args
	 */
	@Before
	public void setUp() {

		PropertyDescImpl prop;

		device = new ObjectDescImpl("BACNETDEVICE:10");
		device.addProperty(prop = new PropertyDescImpl("objectName", "BACnet Gateway"));
		prop.setParent(device);
		device.addProperty(prop = new PropertyDescImpl("destination", "synaptest:47808"));
		prop.setParent(device);
		device.addProperty(prop = new PropertyDescImpl("DNET", "2"));
		prop.setParent(device);
		device.addProperty(prop = new PropertyDescImpl("instance", "77000"));
		prop.setParent(device);

		obj1 = new ObjectDescImpl("BACNETOBJECT:20");
		obj1.addProperty(prop = new PropertyDescImpl("instance", "3"));
		prop.setParent(obj1);
		obj1.addProperty(prop = new PropertyDescImpl("name", "DB"));
		prop.setParent(obj1);
		obj1.addProperty(prop = new PropertyDescImpl("type", "Database"));
		prop.setParent(obj1);

		device.addChild(obj1);
		obj1.setParent(device);

		final ObjectDescImpl prop1 = new ObjectDescImpl("BACNETPROPERTY:33");
		prop1.addProperty(prop = new PropertyDescImpl("id", "77"));
		prop.setParent(prop1);
		prop1.addProperty(prop = new PropertyDescImpl("name", "objectName"));
		prop.setParent(prop1);
		prop1.addProperty(prop = new PropertyDescImpl("type", "CharacterString"));
		prop.setParent(prop1);
		prop1.addProperty(prop = new PropertyDescImpl("access", "readonly"));
		prop.setParent(prop1);
		prop1.addProperty(prop = new PropertyDescImpl("lastValue", ""));
		prop.setParent(prop1);
		obj1.addChild(prop1);
		prop1.setParent(obj1);

		final ObjectDescImpl obj2 = new ObjectDescImpl("BACNETOBJECT:21");
		obj2.addProperty(prop = new PropertyDescImpl("instance", "2"));
		prop.setParent(obj2);
		obj2.addProperty(prop = new PropertyDescImpl("name", "WEB"));
		prop.setParent(obj2);
		obj2.addProperty(prop = new PropertyDescImpl("type", "Webserver"));
		prop.setParent(obj2);

		device.addChild(obj2);
		obj2.setParent(device);

		prop2 = new ObjectDescImpl("BACNETPROPERTY:31");
		prop2.addProperty(prop = new PropertyDescImpl("id", "41"));
		prop.setParent(prop2);
		prop2.addProperty(prop = new PropertyDescImpl("name", "description"));
		prop.setParent(prop2);
		prop2.addProperty(prop = new PropertyDescImpl("type", "CharacterString"));
		prop.setParent(prop2);
		prop2.addProperty(prop = new PropertyDescImpl("access", "readonly"));
		prop.setParent(prop2);
		prop2.addProperty(prop = new PropertyDescImpl("lastValue", ""));
		prop.setParent(prop2);

		obj2.addChild(prop2);
		prop2.setParent(obj2);

		prop3 = new ObjectDescImpl("BACNETPROPERTY:30");
		prop3.addProperty(prop = new PropertyDescImpl("id", "77"));
		prop.setParent(prop3);
		prop3.addProperty(prop = new PropertyDescImpl("name", "objectName"));
		prop.setParent(prop3);
		prop3.addProperty(prop = new PropertyDescImpl("type", "CharacterString"));
		prop.setParent(prop3);
		prop3.addProperty(prop = new PropertyDescImpl("access", "readonly"));
		prop.setParent(prop3);
		prop3.addProperty(prop = new PropertyDescImpl("lastValue", ""));
		prop.addTag(new TagDescImpl("poll", "2000"));
		prop.setParent(prop3);

		device.addChild(prop3);
		prop3.setParent(device);

		final ObjectDescImpl prop4 = new ObjectDescImpl("BACNETPROPERTY:32");
		prop4.addProperty(prop = new PropertyDescImpl("id", "28"));
		prop.setParent(prop4);
		prop4.addProperty(prop = new PropertyDescImpl("name", "description"));
		prop.setParent(prop4);
		prop4.addProperty(prop = new PropertyDescImpl("type", "CharacterString"));
		prop.setParent(prop4);
		prop4.addProperty(prop = new PropertyDescImpl("access", "readwrite"));
		prop.setParent(prop4);
		prop4.addProperty(prop = new PropertyDescImpl("lastValue", ""));
		prop.setParent(prop4);

		device.addChild(prop4);
		prop4.setParent(device);

		pluginConfig = new PluginConfig() {
			List<PropertyDescImpl> pluginProps = Arrays.asList(new PropertyDescImpl("transportType", "ip"),
			        new PropertyDescImpl("serialDataBits", "8"), new PropertyDescImpl("serialParity", "0"),
			        new PropertyDescImpl("serialPassword", ""), new PropertyDescImpl("serialPortName", "COM1"),
			        new PropertyDescImpl("serialSpeed", "9600"), new PropertyDescImpl("serialStopBits", "1"),
			        new PropertyDescImpl("ethernetInterfaceIdx", "0"), new PropertyDescImpl("ipInterfaceIdx", "0"),
			        new PropertyDescImpl("ipMask", "24"));

			@Override
			public List<ObjectDesc> getRoots() {
				List<ObjectDesc> list = new ArrayList<ObjectDesc>();
				list.add(device);
				return list;
			}

			@Override
			public PropertyDesc getPluginProperty(String propertyName) {
				for (PropertyDesc prop : pluginProps) {
					if (prop.getName().equals(propertyName)) {
						return prop;
					}
				}
				return null;
			}

			@Override
			public Map<String, PropertyDesc> getPluginProperties() {
				Map<String, PropertyDesc> map = new HashMap<String, PropertyDesc>();
				for (PropertyDesc prop : pluginProps) {
					map.put(prop.getName(), prop);
				}
				return map;
			}

			@Override
			public String getPluginName() {
				return "BACnetPlugin";
			}

			@Override
			public Map<String, ObjectDesc> getObjects() {
				Map<String, ObjectDesc> map = new HashMap<String, ObjectDesc>();
				map.put(device.getID(), device);
				map.put(obj1.getID(), obj1);
				map.put(prop1.getID(), prop1);
				map.put(obj2.getID(), obj2);
				map.put(prop2.getID(), prop2);
				map.put(prop3.getID(), prop3);
				map.put(prop4.getID(), prop4);

				return map;
			}

			@Override
			public ObjectDesc getObject(String id) {
				Map<String, ObjectDesc> map = new HashMap<String, ObjectDesc>();
				map.put(device.getID(), device);
				map.put(obj1.getID(), obj1);
				map.put(prop1.getID(), prop1);
				map.put(obj2.getID(), obj2);
				map.put(prop2.getID(), prop2);
				map.put(prop3.getID(), prop3);
				map.put(prop4.getID(), prop4);
				if (map.containsKey(id)) {
					return map.get(id);
				}
				return null;
			}
		};

		TestTransport transport = new TestTransport();
		plugin = new BACnetPlugin();

		plugin.setTransport(transport);
	}

	@Test
	public void test1Test() {
		try {
			plugin.start();
		} catch (IncorrectStateException e1) {
		}

		try {
			plugin.configure(pluginConfig);
		} catch (Exception e) {
		}

		plugin.stop();

		try {
			plugin.configure(pluginConfig);
		} catch (Exception e) {
		}
		try {
			plugin.start();
		} catch (IncorrectStateException e) {
		}

		// Object result = null;
		// result = plugin.getPropertyValue(device, "objectName");
		// result = plugin.getPropertyValue(obj1, "objectName");

		// result = plugin.getPropertyValue(prop1, "lastValue");
		// result = plugin.getPropertyValue(prop2, "lastValue");
		// result = plugin.getPropertyValue(prop3, "lastValue");

		// result = null;
	}

	private static class TestTransport implements Transport {

		@Override
		public void connect() throws IOException {
		}

		@Override
		public int getPriority() {
			return 0;
		}

		@Override
		public void sendData(String objectID, String propertyName, Serializable value) {
			System.out.println("Transport message:\n\tObject ID: " + objectID + "\n\tProperty name:" + propertyName
			        + "\n\tValue:" + value);

		}

		@Override
		public void sendOrderedData(String objectID, String propertyName, Serializable value) {
			sendData(objectID, propertyName, value);
		}
	}

}
