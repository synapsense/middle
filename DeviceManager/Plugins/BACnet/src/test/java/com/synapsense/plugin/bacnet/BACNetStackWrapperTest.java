package com.synapsense.plugin.bacnet;

import java.net.DatagramSocket;
import java.net.SocketException;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

@Ignore("Known failure, see http://pandunite.panduit.com/show_bug.cgi?id=24476")
public class BACNetStackWrapperTest {

	private static final int DEFAULT_BACNET_PORT = 47808;
	BACnetStackWrapper stackWrapper;

	@Before
	public void setUp() {
		stackWrapper = new BACnetStackWrapper(0, 23);
	}

	@Test
	public void testStartStop() throws SocketException, InterruptedException {
		stackWrapper.startCapture();
		Thread.sleep(100);
		stackWrapper.stopCapture();
		DatagramSocket socket = new DatagramSocket(DEFAULT_BACNET_PORT);
		socket.close();

	}

	@Test
	public void testStartStopWithoutDelay() throws SocketException, InterruptedException {
		stackWrapper.startCapture();
		stackWrapper.stopCapture();
		DatagramSocket socket = new DatagramSocket(DEFAULT_BACNET_PORT);
		socket.close();

	}
}
