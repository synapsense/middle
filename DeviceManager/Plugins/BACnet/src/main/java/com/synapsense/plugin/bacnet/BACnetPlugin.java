package com.synapsense.plugin.bacnet;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.synapsense.plugin.api.exceptions.ConfigPluginException;
import com.synapsense.plugin.api.exceptions.IncorrectStateException;
import com.synapsense.plugin.apinew.ObjectDesc;
import com.synapsense.plugin.apinew.Plugin;
import com.synapsense.plugin.apinew.PluginConfig;
import com.synapsense.plugin.apinew.PropertyDesc;
import com.synapsense.plugin.apinew.StatelessPlugin;
import com.synapsense.plugin.apinew.Transport;

public class BACnetPlugin implements Plugin, StatelessPlugin {

	private final static String ID = "bacnet";

	private static Properties PLUGIN_DESC = new Properties();
	static {
		PLUGIN_DESC.put(PLUGIN_ID, ID);
		PLUGIN_DESC.put(ROOT_TYPES, "BACNETNETWORK");
	}
	private static final Logger logger = Logger.getLogger(BACnetPlugin.class);
	private Map<String, BACnetStackWrapper> stackWrappers = new HashMap<String, BACnetStackWrapper>();
	private BACnetStackWrapper statelessStackWrapper;
	private Transport transport = null;
	private Map<String, PropertyDesc> pluginConfiguration;
	private boolean configured = false;
	private boolean started = false;
	private Properties prop;

	public BACnetPlugin() {
		prop = new Properties();
		logger.debug("Trying to load config file from the default location: [" + BACnetConstants.CONF_FILE + "]");
		File configFile = new File(BACnetConstants.CONF_FILE);
		if (configFile.exists()) {
			// we always create a new stack wrapper in the constructor if we
			// have a config file either with
			// the default parameters or with the ones from the config file in
			// case of any issues
			logger.debug("Found the config file...");
			try {
				FileInputStream fis = new FileInputStream(configFile);
				prop.load(fis);
				fis.close();

			} catch (IOException e) {
				logger.warn("Unable to load configuration properties file from default location: ["
				        + BACnetConstants.CONF_FILE + "]");
			}
			Integer interfaceIdx;
			Integer mask;
			try {
				interfaceIdx = Integer.parseInt(prop.getProperty(BACnetConstants.BN_NETWORK_IP_INTERFACE_INDEX, "0"));
				mask = Integer.parseInt(prop.getProperty(BACnetConstants.BN_NETWORK_IP_MASK, "24"));
				logger.debug("The config file is successfully parced.");

			} catch (NumberFormatException e) {
				logger.warn("Properties file [" + BACnetConstants.CONF_FILE
				        + "] contains invalid configuraiton. Using default interface index [0] and mask [24]");
				interfaceIdx = 0;
				mask = 24;
			}
			statelessStackWrapper = new BACnetStackWrapper(interfaceIdx, mask);
			stackWrappers.put(statelessStackWrapper.getId(), statelessStackWrapper);
			statelessStackWrapper.startCapture();
			DevicesHolder.getInstance().discoverMedia(statelessStackWrapper.getMedia());
		}
	}

	@Override
	public void configure(PluginConfig config) throws ConfigPluginException {
		if (config == null) {
			throw new IllegalArgumentException("config cannot be null");
		}
		stop();
		pluginConfiguration = config.getPluginProperties();
		for (ObjectDesc bnNetwork : config.getRoots()) {
			BACnetStackWrapper stackWrapper;
			String key = BACnetStackWrapper.getKeyFromNetwork(bnNetwork);
			if (stackWrappers.containsKey(key)) {
				stackWrapper = stackWrappers.get(key);
			} else {
				stackWrapper = new BACnetStackWrapper(bnNetwork);
			}
			stackWrappers.put(stackWrapper.getId(), stackWrapper);
			for (ObjectDesc bnDevice : bnNetwork.getChildrenByType(BACnetConstants.TYPE_DEVICE)) {
				try {
					BACnetDevice device = new BACnetDevice(stackWrapper.getMedia(), bnDevice);
					DevicesHolder.getInstance().putDevice(bnDevice.getID(), device);

				} catch (IllegalArgumentException e) {
					logger.warn("Cannot create the device: '"
					        + bnDevice.getPropertyValue(BACnetConstants.BN_NETWORK_NAME) + "'");
				}
			}
		}

		configured = true;
		logger.debug("Plugin " + ID + " is configured.");

	}

	@Override
	public String getID() {
		return ID;
	}

	@Override
	public Properties getPluginDescription() {
		return PLUGIN_DESC;
	}

	@Override
	public void setTransport(Transport transport) {
		this.transport = transport;
	}

	@Override
	public void start() throws IncorrectStateException {
		if (!configured) {
			throw new IncorrectStateException("Plugin " + ID + " has to be configured before start.");
		}
		if (!started) {
			try {
				for (BACnetStackWrapper wrapper : stackWrappers.values()) {
					wrapper.startCapture();
				}
			} catch (Exception e) {
				logger.fatal("Cannot capture the '" + pluginConfiguration.get("transportType") + "' interface.");
				return;
			}
			started = true;
			logger.debug("Plugin " + ID + " is started.");
		}
	}

	@Override
	public void stop() {
		if (started) {
			for (BACnetStackWrapper wrapper : stackWrappers.values()) {
				wrapper.stopCapture();
			}
		}
		stackWrappers.clear();
		DevicesHolder.getInstance().clear();
		configured = false;
		started = false;
		logger.debug("Plugin " + ID + " is stopped.");
	}

	public Transport getTransport() {
		return transport;
	}

	@Override
	public Serializable getPropertyValue(ObjectDesc obj, String propertyName) {
		Serializable result = null;
		if (obj != null) {
			PropertyDesc prop = obj.getProperty(propertyName);
			if (prop != null) {
				try {
					result = BACnetPropertyRW.readProperty(prop, obj);
				} catch (IOException e) {
					logger.warn("Cannot read property: '" + prop + "' from object " + obj);
				}
			}
		}
		return result;
	}

	@Override
	public List<Serializable> getPropertyValue(ObjectDesc object, List<String> propertyNames) {
		List<Serializable> result = new ArrayList<Serializable>();
		for (String property : propertyNames) {
			result.add(getPropertyValue(object, property));
		}
		return result;
	}

	@Override
	public void setPropertyValue(ObjectDesc obj, String propertyName, Serializable value) {
		if (obj != null) {

			PropertyDesc prop = obj.getProperty(propertyName);
			if (prop != null) {
				try {
					BACnetPropertyRW.writeProperty(prop, obj, value);
				} catch (IOException e) {
					logger.warn("Cannot set value: '" + value + "' of '" + prop.getName() + "' property");
				}
			}
		}

	}

	@Override
	public void setPropertyValue(ObjectDesc object, List<String> propertyNames, List<Serializable> values) {
		if (propertyNames == null || values == null || propertyNames.size() != values.size()) {
			throw new IllegalArgumentException("Unable to set properties values.");
		}
		for (int i = 0; i < propertyNames.size(); i++) {
			setPropertyValue(object, propertyNames.get(i), values.get(i));
		}
	}

	@Override
	public void setPropertyValue(Properties params, Serializable newValue) {
		BACnetPropertyRW.writeProperty(statelessStackWrapper, params, newValue);

	}

	@Override
	public Serializable getPropertyValue(Properties params) {
		return BACnetPropertyRW.readProperty(statelessStackWrapper, params);
	}

}
