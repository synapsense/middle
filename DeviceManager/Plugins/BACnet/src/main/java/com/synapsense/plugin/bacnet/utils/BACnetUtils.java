/**
 * 
 */
package com.synapsense.plugin.bacnet.utils;

import java.math.BigInteger;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class BACnetUtils {

	private static long PROCESS_IDENTIFIER = 0;

	public static String toHex(String mac) {
		if (mac == null)
			return "";
		String hexmac = "";
		byte[] b = mac.getBytes();
		for (int i = 0; i < b.length; ++i) {
			if (!hexmac.isEmpty())
				hexmac = hexmac + "-";
			hexmac = hexmac + (Integer.toHexString((b[i] & 0xFF) | 0x100).substring(1, 3).toUpperCase());
		}
		return hexmac;
	}

	public static String toHex(byte[] b) {
		String hexmac = "";
		if (b == null)
			return hexmac;
		for (int i = 0; i < b.length; ++i) {
			if (!hexmac.isEmpty())
				hexmac = hexmac + ":";
			hexmac = hexmac + (Integer.toHexString((b[i] & 0xFF) | 0x100).substring(1, 3).toUpperCase());
		}
		return hexmac;
	}

	public static String toMac(int mac) {
		byte[] bytes = { 0, 0, (byte) (mac >> 24), (byte) (mac >> 16), (byte) (mac >> 8), (byte) (mac) };
		return new String(bytes);
	}

	public static synchronized long getProcessIdentifier() {
		if ((PROCESS_IDENTIFIER + 1) > Long.MAX_VALUE) {
			return ++PROCESS_IDENTIFIER;
		}
		PROCESS_IDENTIFIER = 1;
		return PROCESS_IDENTIFIER;
	}

	public static byte[] getDestinationMac(String destination) {
		byte[] result = null;
		String ip = destination;
		if (ip != null) {
			byte[] portBytes = new byte[] { (byte) 0xBA, (byte) 0xC0 };
			if (ip.indexOf(":") != -1) {
				int port = Integer.parseInt(ip.substring(ip.indexOf(":") + 1));
				portBytes[0] = (byte) (port >> 8);
				portBytes[1] = (byte) port;
				ip = ip.substring(0, ip.indexOf(":"));
			}

			String[] splitIp = ip.split("\\.");
			if (splitIp.length != 4) {
				try {
					InetAddress inet = InetAddress.getByName(ip);
					splitIp = inet.getHostAddress().split("\\.");
				} catch (UnknownHostException e) {
				}
			}
			if (splitIp.length == 4) {
				result = new byte[splitIp.length + 2];
				for (int i = 0; i < splitIp.length; i++) {
					result[i] = (byte) Integer.parseInt(splitIp[i]);
				}
				result[result.length - 2] = portBytes[0];
				result[result.length - 1] = portBytes[1];
			}
		}

		return result;
	}

	public static byte[] getDestinationBytes(String destination, int len) {
		byte[] result = new byte[len];
		for (int i = 0; i < len; i++) {
			result[i] = 0;
		}
		if (destination != null) {
			int num = Integer.parseInt(destination);
			byte[] byteArray = new BigInteger(Integer.toHexString(num), 16).toByteArray();
			int byteArrayLength = byteArray.length;
			if (byteArrayLength < len) {
				for (int i = 1; i <= byteArrayLength; i++) {
					result[len - i] = byteArray[byteArrayLength - i];
				}
			}
		}

		return result;
	}
}
