package com.synapsense.plugin.bacnet;

import java.io.IOException;
import java.io.Serializable;

import com.synapsense.bacnet.network.pdu.apdu.codecs.PrimitivesEncoder;
import com.synapsense.bacnet.system.shared.BACnetObjectType;

public class ValueCoder {

	public static Serializable decodeValue(short type, String val) {
		if (type == BACnetObjectType.ANALOG_INPUT || type == BACnetObjectType.ANALOG_OUTPUT
		        || type == BACnetObjectType.ANALOG_VALUE) {
			return new Double(val);
		} else if (type == BACnetObjectType.MULTI_STATE_INPUT || type == BACnetObjectType.MULTI_STATE_OUTPUT
		        || type == BACnetObjectType.MULTI_STATE_VALUE) {
			return new Integer(val);
		} else if ((type == BACnetObjectType.BINARY_INPUT || type == BACnetObjectType.BINARY_OUTPUT || type == BACnetObjectType.BINARY_VALUE)) {
			return new Integer(val);
		}
		throw new IllegalArgumentException("Unable to decode value. Unsupported Object type with id [" + type + "]");

	}

	public static byte[] encodeValue(short type, String val) throws IOException {
		PrimitivesEncoder encoder = new PrimitivesEncoder();
		encoder.encodeSequenceStart(3);
		if (type == BACnetObjectType.ANALOG_INPUT || type == BACnetObjectType.ANALOG_OUTPUT
		        || type == BACnetObjectType.ANALOG_VALUE) {
			encoder.encodeReal(null, new Float(val));
		} else if (type == BACnetObjectType.MULTI_STATE_INPUT || type == BACnetObjectType.MULTI_STATE_OUTPUT
		        || type == BACnetObjectType.MULTI_STATE_VALUE) {
			encoder.encodeUInt(null, new Integer(val));

		} else if ((type == BACnetObjectType.BINARY_INPUT || type == BACnetObjectType.BINARY_OUTPUT || type == BACnetObjectType.BINARY_VALUE)) {
			encoder.encodeEnum(null, new Long(val));
		} else {
			throw new IllegalArgumentException("Unable to encode value. Unsupported Object type with id [" + type + "]");
		}
		encoder.encodeSequenceStop(3);
		return encoder.getEncodedBuffer();

	}

	public static byte[] encodeValue(short type, Serializable val) throws IOException {
		return encodeValue(type, val.toString());
	}

}
