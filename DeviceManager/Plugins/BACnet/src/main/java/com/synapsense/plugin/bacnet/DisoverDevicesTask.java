package com.synapsense.plugin.bacnet;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.TimerTask;

import org.apache.log4j.Logger;

import com.synapsense.bacnet.network.Constants;
import com.synapsense.bacnet.network.media.MPDUHeader;
import com.synapsense.bacnet.network.media.Media;
import com.synapsense.bacnet.network.pdu.apdu.svc.SvcBase;
import com.synapsense.bacnet.network.pdu.apdu.svc.SvcResult;
import com.synapsense.bacnet.network.pdu.apdu.svc.request.WhoIs;
import com.synapsense.bacnet.network.pdu.apdu.svc.responce.IAm;
import com.synapsense.plugin.bacnet.utils.BACnetUtils;

public class DisoverDevicesTask extends TimerTask {
	private final static Logger log = Logger.getLogger(DisoverDevicesTask.class);
	private Media media;

	public DisoverDevicesTask(Media media) {
		this.media = media;
	}

	@Override
	public void run() {
		log.debug("Discovering devices BACnet device...");
		MPDUHeader header = new MPDUHeader();
		header.media = media;
		header.SRC_MAC = header.media.getActiveEthMac();
		header.DST_MAC = header.media.getBroadcast();
		header.DNET = Constants.BAC_NET_NET_BROADCAST;
		log.debug("Header was created: {media=\"" + header.media + "\", " + "SRC_MAC="
		        + BACnetUtils.toHex(header.SRC_MAC) + ", DST_MAC=" + BACnetUtils.toHex(header.DST_MAC) + "}");
		WhoIs request = new WhoIs(header);
		log.debug("WhoIs service was created.");

		// Collect all device responses and wait until timeout
		request.createSession(Integer.MAX_VALUE);
		SvcResult results = request.getResult();
		DevicesHolder deviceHolder = DevicesHolder.getInstance();
		for (SvcBase response : results) {
			if (response instanceof IAm) {
				int instance = ((IAm) response).getIAmDeviceIdentifier().getInstanceNumber();
				MPDUHeader responseHeader = response.getHeader();
				int snet = -1;
				if (responseHeader.SNET != Constants.PROPERTY_NOT_SPECIFIED) {
					snet = responseHeader.SNET;
				}
				try {
					InetAddress ipv4 = (Inet4Address) Inet4Address.getByAddress(Arrays
					        .copyOf(responseHeader.SRC_MAC, 4));
					String ip = ipv4.getHostAddress();
					BACnetDevice device = new BACnetDevice(media, snet, ip, instance);
					MPDUHeader head = device.getHead();
					head.media = media;
					head.SRC_MAC = responseHeader.DST_MAC;
					head.DST_MAC = responseHeader.SRC_MAC;
					if (snet != -1) {
						head.DNET = snet;
					}
					if (responseHeader.SADR != null) {
						head.DADR = responseHeader.SADR;
						head.DLEN = head.DADR.length;
						head.HopCount = 255;
					}
					String key = device.getKey();
					if (!deviceHolder.containsKey(key)) {
						deviceHolder.putDevice(device.getKey(), device);
						log.info("Discovered device with " + device.toString());
					}
				} catch (UnknownHostException e) {
					log.warn("Unable to parse IP address for device with Network [" + snet + "] Instance Id ["
					        + instance);
				}

			}
		}
	}

}
