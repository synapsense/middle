/**
 * 
 */
package com.synapsense.plugin.bacnet;

import java.util.HashMap;

import com.synapsense.bacnet.system.shared.BACnetObjectType;

/**
 * @author stikhon
 * 
 */
public class BACnetConstants {

	public static final String TYPE_NETWORK = "BACNETNETWORK";
	public static final String TYPE_DEVICE = "BACNETDEVICE";
	public static final String TYPE_OBJECT = "BACNETOBJECT";
	public static final String TYPE_PROPERTY = "BACNETPROPERTY";

	public static final String BN_NETWORK_IP_INTERFACE_INDEX = "ipInterfaceIdx";
	public static final String BN_NETWORK_IP_MASK = "ipMask";
	public static final String BN_NETWORK_NAME = "name";
	public static final String BN_NETWORK_DNET = "DNET";
	public static final String BN_NETWORK_DESTINATION = "destination";
	public static final String BN_DEVICE_INSTANCE = "instance";

	public static final String BN_OBJECT_INSTANCE = "instance";
	public static final String BN_OBJECT_TYPE = "type";

	public static final String BN_PROPERTY_ID = "id";
	public static final String BN_PROPERTY_ACCESS = "access";

	public final static String CONF_FILE = "conf" + System.getProperty("file.separator") + "bacnet.properties";
	public static HashMap<String, Short> types = new HashMap<String, Short>();
	static {
		types.put(ObjectType.DEVICE.getValue(), BACnetObjectType.DEVICE);
		types.put(ObjectType.ANALOG_INPUT.getValue(), BACnetObjectType.ANALOG_INPUT);
		types.put(ObjectType.ANALOG_OUTPUT.getValue(), BACnetObjectType.ANALOG_OUTPUT);
		types.put(ObjectType.ANALOG_VALUE.getValue(), BACnetObjectType.ANALOG_VALUE);
		types.put(ObjectType.BINARY_INPUT.getValue(), BACnetObjectType.BINARY_INPUT);
		types.put(ObjectType.BINARY_OUTPUT.getValue(), BACnetObjectType.BINARY_OUTPUT);
		types.put(ObjectType.BINARY_VALUE.getValue(), BACnetObjectType.BINARY_VALUE);
		types.put(ObjectType.MULTY_STATE_INPUT.getValue(), BACnetObjectType.MULTI_STATE_INPUT);
		types.put(ObjectType.MULTY_STATE_OUTPUT.getValue(), BACnetObjectType.MULTI_STATE_OUTPUT);
		types.put(ObjectType.MULTY_STATE_VALUE.getValue(), BACnetObjectType.MULTI_STATE_VALUE);

	}

	public enum ObjectType {

	DEVICE("Device"), ANALOG_INPUT("Analog-Input"), ANALOG_OUTPUT("Analog-Output"), ANALOG_VALUE("Analog-Value"), BINARY_INPUT(
	        "Binary-Input"), BINARY_OUTPUT("Binary-Output"), BINARY_VALUE("Binary-Value"), MULTY_STATE_INPUT(
	        "Multy-State-Input"), MULTY_STATE_OUTPUT("Multy-State-Output"), MULTY_STATE_VALUE("Multy-State-Value");
	private final String value;

	ObjectType(String v) {
		value = v;
	}

	public String getValue() {
		return value;
	}
	}

	public enum AccessType {

	READONLY("readonly"), READWRITE("readwrite");
	private final String value;

	AccessType(String v) {
		value = v;
	}

	public String getValue() {
		return value;
	}
	}
}
