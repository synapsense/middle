package com.synapsense.plugin.bacnet;

import java.util.Collection;
import java.util.Map;
import java.util.Properties;
import java.util.Timer;
import java.util.concurrent.ConcurrentHashMap;

import com.synapsense.bacnet.network.media.Media;

public class DevicesHolder {

	private static long TASK_PERIOD = 60 * 60 * 1000;// 60 min
	private static long FIRST_DELAY = 10000L;// 10 sec
	private Map<String, BACnetDevice> devices = new ConcurrentHashMap<String, BACnetDevice>();
	private Timer devicesDiscoveryTimer = new Timer("DevicesDiscovery", true);
	private static DevicesHolder instance = new DevicesHolder();

	private DevicesHolder() {
	}

	public static DevicesHolder getInstance() {
		if (instance == null) {
			instance = new DevicesHolder();
		}
		return instance;
	}

	public BACnetDevice getDevice(String key) {
		return devices.get(key);
	}

	public BACnetDevice getDevice(Properties props) {
		return devices.get(getKey(props));
	}

	public BACnetDevice putDevice(String key, BACnetDevice device) {
		return devices.put(key, device);
	}

	public BACnetDevice putDevice(BACnetDevice device) {
		return devices.put(device.getKey(), device);
	}

	public Collection<BACnetDevice> getDevices() {
		return devices.values();
	}

	public void clear() {
		devices.clear();
	}

	public boolean containsKey(String key) {
		return devices.containsKey(key);
	}

	public void discoverMedia(Media media) {
		devicesDiscoveryTimer.schedule(new DisoverDevicesTask(media), FIRST_DELAY, TASK_PERIOD);
	}

	private String getKey(Properties props) {
		StringBuffer key = new StringBuffer(props.getProperty("ip") + ":");
		key.append(props.getProperty("net") + ":");
		key.append(props.getProperty("dev"));
		return key.toString();
	}
}
