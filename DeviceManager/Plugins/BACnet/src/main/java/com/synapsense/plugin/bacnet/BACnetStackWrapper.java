/**
 * 
 */
package com.synapsense.plugin.bacnet;

import org.apache.log4j.Logger;

import com.synapsense.bacnet.network.NetworkProcessor;
import com.synapsense.bacnet.network.jni.PropertyNotFoundException;
import com.synapsense.bacnet.network.jni.UnableToSetUpInterfaceException;
import com.synapsense.bacnet.network.media.MPDUHeader;
import com.synapsense.bacnet.network.media.Media;
import com.synapsense.bacnet.network.media.IP.IP;
import com.synapsense.bacnet.network.pdu.PDUBasePacket;
import com.synapsense.plugin.apinew.ObjectDesc;

public class BACnetStackWrapper {
	private static final Logger log = Logger.getLogger(BACnetStackWrapper.class);
	private IP media = null;
	private BACnetStackThread stackThread = null;
	private int interfaceIndex;
	private int mask;
	private String wrapperId;

	public BACnetStackWrapper(ObjectDesc network) {
		String interfaceIndexPropStr = network.getPropertyValue(BACnetConstants.BN_NETWORK_IP_INTERFACE_INDEX);
		String maskPropStr = network.getPropertyValue(BACnetConstants.BN_NETWORK_IP_MASK);
		if (interfaceIndexPropStr == null || maskPropStr == null) {
			log.warn("Cannot create a new IP media. Check protocol parameters");
			throw new IllegalArgumentException("Cannot create a new IP media. Check protocol parameters");
		} else {
			interfaceIndex = Integer.parseInt(interfaceIndexPropStr);
			mask = Integer.parseInt(maskPropStr);
		}

		wrapperId = Integer.toString(this.interfaceIndex) + ":" + Integer.toString(this.mask);
		media = new IP();
		media.setMask(mask);
	}

	public BACnetStackWrapper(int interfaceIndex, int mask) {
		this.interfaceIndex = interfaceIndex;
		this.mask = mask;
		wrapperId = Integer.toString(this.interfaceIndex) + ":" + Integer.toString(this.mask);
		media = new IP();
		media.setMask(mask);
	}

	public String getId() {
		return wrapperId;
	}

	public void startCapture() {
		if (stackThread == null) {
			stackThread = new BACnetStackThread();
			stackThread.start();
		}
	}

	public void stopCapture() {
		if (stackThread != null) {
			stackThread.setStarted(false);
			media.stopCapture();
			stackThread = null;
		}
	}

	public Media getMedia() {
		return (Media) media;
	}

	public static String getKeyFromNetwork(ObjectDesc network) {
		String interfaceIndexPropStr = network.getPropertyValue(BACnetConstants.BN_NETWORK_IP_INTERFACE_INDEX);
		String maskPropStr = network.getPropertyValue(BACnetConstants.BN_NETWORK_IP_MASK);
		return interfaceIndexPropStr + ":" + maskPropStr;
	}

	public class BACnetStackThread extends Thread {
		volatile boolean started = false;

		public BACnetStackThread() {
			super("BACnetStackThread");
			setDaemon(true);

		}

		@Override
		public void run() {
			while (!started) {

				try {
					media.startCapture(interfaceIndex, false);
					log.info("Connected to IP: {interfaceIndex=" + interfaceIndex + "; mask=" + mask + "}");
					log.debug("Starting the capturing the IP media...");
					started = true;
				} catch (UnableToSetUpInterfaceException e) {
					log.error("Unable to connect to IP: {interfaceIndex=" + interfaceIndex + "; mask=" + mask + "}");
					log.debug("Waiting 60 sec...");
					try {
						Thread.sleep(60000);
					} catch (InterruptedException e1) {
					}
					log.info("Next attempt");
				}
			}
			NetworkProcessor.getInstance().start();
			while (started) {
				try {
					MPDUHeader header = new MPDUHeader();
					header.media = media;
					PDUBasePacket packet = header.receive();
					if (packet != null) {
						NetworkProcessor.getInstance().addPacket(packet);
					}
				} catch (PropertyNotFoundException e) {
					log.debug("Missing property while processing a packet. " + e.getMessage());
				}

				catch (Exception e) {
					log.debug("Error while processing packet. ", e);
				}
			}
			NetworkProcessor.getInstance().stop();
			log.info("Network processor is stopped");
		}

		public boolean isStarted() {
			return started;
		}

		public void setStarted(boolean started) {
			this.started = started;
		}

	}
}
