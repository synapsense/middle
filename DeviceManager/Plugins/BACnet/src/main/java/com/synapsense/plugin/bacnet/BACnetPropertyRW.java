package com.synapsense.plugin.bacnet;

import java.io.IOException;
import java.io.Serializable;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.synapsense.bacnet.enums.BACnetAPDUSize;
import com.synapsense.bacnet.network.pdu.apdu.svc.SvcResult;
import com.synapsense.bacnet.network.pdu.apdu.svc.request.ReadProperty;
import com.synapsense.bacnet.network.pdu.apdu.svc.request.WriteProperty;
import com.synapsense.bacnet.network.pdu.apdu.svc.responce.ErrorSvc;
import com.synapsense.bacnet.network.pdu.apdu.svc.responce.ReadPropertyAck;
import com.synapsense.bacnet.network.pdu.apdu.svc.responce.WritePropertyAck;
import com.synapsense.bacnet.system.BACnetError;
import com.synapsense.bacnet.system.shared.BACnetObjectIdentifier;
import com.synapsense.plugin.apinew.ObjectDesc;
import com.synapsense.plugin.apinew.PropertyDesc;

public class BACnetPropertyRW {

	private static final Logger log = Logger.getLogger(BACnetPropertyRW.class);

	public static Serializable readProperty(final PropertyDesc property, final ObjectDesc obj) throws IOException {
		ObjectDesc bacNetProp = obj;
		ObjectDesc bacNetParentObject = bacNetProp.getParent();
		Short type = BACnetConstants.types.get(bacNetParentObject.getPropertyValue(BACnetConstants.BN_OBJECT_TYPE));
		if (type == null) {
			throw new IllegalArgumentException("Unsupported type '"
			        + bacNetParentObject.getPropertyValue(BACnetConstants.BN_OBJECT_TYPE) + "' parameter");
		}
		BACnetObjectIdentifier objIdentifier = new BACnetObjectIdentifier(type, Integer.parseInt(bacNetParentObject
		        .getPropertyValue(BACnetConstants.BN_OBJECT_INSTANCE)));
		Integer propertyId = Integer.parseInt(bacNetProp.getPropertyValue(BACnetConstants.BN_PROPERTY_ID));
		String res = readProperty(getDevice(obj), objIdentifier, propertyId);
		Double returnValue;
		try {
			returnValue = new Double(res);
			return returnValue;
		} catch (NumberFormatException e) {
			log.debug("Value[" + res + "] of the property with id [" + propertyId
			        + "] for object with instance number [" + objIdentifier.getInstanceNumber() + "] can not be parced");

			throw new BACnetException("Wrong answer came from device {" + res + "}.");
		}
	}

	public static Serializable readProperty(BACnetStackWrapper sw, Properties properties) {
		// assert required properties exist
		if (!properties.containsKey("ip")) {
			throw new IllegalArgumentException("Missing 'ip' parameter");
		} else if (!properties.containsKey("type")) {
			throw new IllegalArgumentException("Missing 'type' parameter");
		} else if (!properties.containsKey("net")) {
			throw new IllegalArgumentException("Missing 'net' parameter");
		} else if (!properties.containsKey("obj")) {
			throw new IllegalArgumentException("Missing 'obj' parameter");
		} else if (!properties.containsKey("dev")) {
			throw new IllegalArgumentException("Missing 'dev' parameter");
		} else if (!properties.containsKey("id")) {
			throw new IllegalArgumentException("Missing 'id' parameter");
		}
		Short type = BACnetConstants.types.get(properties.getProperty("type"));
		if (type == null) {
			throw new IllegalArgumentException("Unsupported type '" + properties.getProperty("type") + "' parameter");
		}
		BACnetDevice device = getDevice(sw, properties);
		String res = "";

		BACnetObjectIdentifier objIdentifier = new BACnetObjectIdentifier(type, Integer.parseInt(properties
		        .getProperty("obj")));
		Integer propertyId = Integer.parseInt(properties.getProperty("id"));
		try {
			res = readProperty(device, objIdentifier, propertyId);
			return ValueCoder.decodeValue(type, res);
		} catch (NumberFormatException e) {
			log.debug("Value[" + res + "] of the property with id [" + propertyId
			        + "] for object with instance number [" + objIdentifier.getInstanceNumber() + "] can not be parced");
			throw new BACnetException("wrong_answer");
		}
	}

	private static String readProperty(BACnetDevice dev, BACnetObjectIdentifier objIdentifier, Integer propertyId) {
		ReadProperty readProp = new ReadProperty(dev.getHead());
		readProp.setObjectIdentifier(objIdentifier);
		readProp.setPropertyIdentifier(propertyId);
		readProp.setMax_APDU_length_accepted(BACnetAPDUSize.UP_TO_1476);
		readProp.createSession();
		SvcResult results = readProp.getResult();

		if (results.size() < 1) {
			String error = "No response on [Read Propery] request to Device {" + dev.toString() + "}, Object id {"
			        + objIdentifier.toString() + "}" + "Property ID [" + propertyId + "]";
			log.error(error);
			dev.setConnected(false);
			throw new BACnetException("no_reply");
		} else if (results.size() > 1) {
			String error = "Multiple replyes on [Read Propery] request to Device {" + dev.toString() + "}, Object id {"
			        + objIdentifier.toString() + "}" + "Property ID [" + propertyId + "]";
			log.error(error);
			throw new BACnetException("wrong_answer");
		}

		if (results.firstElement() instanceof ReadPropertyAck) {
			String res = ((ReadPropertyAck) results.firstElement()).getPropertyValue();
			return res;
		}
		if (results.firstElement() instanceof ErrorSvc) {
			BACnetError error = ((ErrorSvc) results.firstElement()).getError();
			throw new BACnetException(error.getErrorCode().toString().toLowerCase());
		}
		throw new BACnetException("wrong_answer");
	}

	public static void writeProperty(BACnetStackWrapper sw, Properties properties, Serializable newValue) {
		// assert required properties exist
		if (!properties.containsKey("ip")) {
			throw new IllegalArgumentException("Missing 'ip' parameter");
		} else if (!properties.containsKey("net")) {
			throw new IllegalArgumentException("Missing 'net' parameter");
		} else if (!properties.containsKey("type")) {
			throw new IllegalArgumentException("Missing 'type' parameter");
		} else if (!properties.containsKey("obj")) {
			throw new IllegalArgumentException("Missing 'obj' parameter");
		} else if (!properties.containsKey("dev")) {
			throw new IllegalArgumentException("Missing 'dev' parameter");
		} else if (!properties.containsKey("id")) {
			throw new IllegalArgumentException("Missing 'id' parameter");
		}
		Short type = BACnetConstants.types.get(properties.getProperty("type"));
		if (type == null) {
			throw new IllegalArgumentException("Unsupported type '" + properties.getProperty("type") + "' parameter");
		}

		BACnetObjectIdentifier objIdentifier = new BACnetObjectIdentifier(type, Integer.parseInt(properties
		        .getProperty("obj")));
		BACnetDevice dev = getDevice(sw, properties);
		Integer propertyId = Integer.parseInt(properties.getProperty("id"));
		byte[] encodedValue = null;
		try {
			encodedValue = ValueCoder.encodeValue(objIdentifier.getObjectType(), newValue);
		} catch (IOException e) {
			String error = e.getMessage() + " Device {" + dev.toString() + "}, Object id {" + objIdentifier.toString()
			        + "]" + " property ID [" + propertyId + "]";
			log.error(error);
			throw new BACnetException(error);
		}
		writeProperty(dev, objIdentifier, propertyId, encodedValue);
	}

	public static void writeProperty(final PropertyDesc property, final ObjectDesc obj, final Serializable value)
	        throws IOException {

		ObjectDesc bacNetProp = obj;
		ObjectDesc bacNetParentObject = bacNetProp.getParent();

		if (bacNetProp.getID() == null || BACnetConstants.TYPE_PROPERTY.equals(bacNetProp.getType())) {
			throw new IllegalArgumentException("");
		}

		if (!BACnetConstants.AccessType.READWRITE.getValue().equals(
		        bacNetProp.getPropertyValue(BACnetConstants.BN_PROPERTY_ACCESS))) {
			throw new IOException("The parameter is not writable");
		}
		Short type = BACnetConstants.types.get(bacNetParentObject.getPropertyValue(BACnetConstants.BN_OBJECT_TYPE));
		if (type == null) {
			throw new IllegalArgumentException("Unsupported type '"
			        + bacNetParentObject.getPropertyValue(BACnetConstants.BN_OBJECT_TYPE) + "' parameter");
		}
		BACnetObjectIdentifier objIdentifier = new BACnetObjectIdentifier(type, Integer.parseInt(bacNetParentObject
		        .getPropertyValue(BACnetConstants.BN_OBJECT_INSTANCE)));
		int propertyId = Integer.parseInt(bacNetProp.getPropertyValue(BACnetConstants.BN_PROPERTY_ID));
		BACnetDevice dev = getDevice(obj);
		byte[] encodedValue = null;
		try {
			encodedValue = ValueCoder.encodeValue(objIdentifier.getObjectType(), (String) value);
		} catch (IOException e) {
			String error = e.getMessage() + " Device {" + dev.toString() + "}, Object id {" + objIdentifier.toString()
			        + "]" + " property ID [" + propertyId + "]";
			log.error(error);
			throw new BACnetException(error);
		}
		writeProperty(dev, objIdentifier, propertyId, encodedValue);

	}

	private static void writeProperty(BACnetDevice dev, BACnetObjectIdentifier objIdentifier, Integer propertyId,
	        byte[] byteValue) {
		// Create WriteProperty service
		WriteProperty writeProp = new WriteProperty(dev.getHead());
		writeProp.setObjectIdentifier(objIdentifier);
		writeProp.setPropertyIdentifier(propertyId);
		writeProp.setMax_APDU_length_accepted(BACnetAPDUSize.UP_TO_1476);
		writeProp.setPropertyValue(byteValue);
		writeProp.createSession();
		SvcResult results = writeProp.getResult();

		if (results.size() < 1) {
			String error = "No response on [Write Propery] request to Device {" + dev.toString() + "}, Object id {"
			        + objIdentifier.toString() + "]" + " property ID [" + propertyId + "]";
			log.error(error);
			dev.setConnected(false);
			throw new BACnetException("no_reply");
		} else if (results.size() > 1) {
			String error = "Multiple replyes on [Write Propery] request to Device {" + dev.toString()
			        + "}, Object id {" + objIdentifier.toString() + "}" + "Property ID [" + propertyId + "]";
			log.error(error);
			throw new BACnetException("wrong_answer");
		}
		Object result = results.firstElement();
		if (result instanceof ErrorSvc) {
			log.error("Wrong answer came on [Write Propery] request to Device {" + dev.toString() + "}, Object id {"
			        + objIdentifier.toString() + "}" + "Property ID [" + propertyId + "] ");
			BACnetError error = ((ErrorSvc) results.firstElement()).getError();
			throw new BACnetException(error.getErrorCode().toString().toLowerCase());
		}
		if (result instanceof WritePropertyAck && log.isDebugEnabled()) {
			// good answer case
			log.debug("Successfull [Write Propery] request to Device {" + dev.toString() + "}, Object id {"
			        + objIdentifier.toString() + "}" + "Property ID [" + propertyId + "]" + "Value is set to ["
			        + results + "]");
		}
	}

	private static BACnetDevice getDevice(BACnetStackWrapper sw, Properties props) {
		BACnetDevice dev = DevicesHolder.getInstance().getDevice(props);
		if (dev == null) {
			dev = new BACnetDevice(sw.getMedia(), props);
			// connect to the device, send WhoIs to discover device's MAC
			// address
			DevicesHolder.getInstance().putDevice(dev);
		}
		if (!dev.isConnected()) {
			boolean deviceAvailable = dev.connect();
			if (!deviceAvailable) {
				throw new BACnetException("no_reply");
			}
			DevicesHolder.getInstance().putDevice(dev);
		}
		return dev;
	}

	private static BACnetDevice getDevice(ObjectDesc obj) {
		ObjectDesc bacNetParentObject = obj.getParent();
		BACnetDevice dev = null;
		if (bacNetParentObject != null)
			if (BACnetConstants.TYPE_DEVICE.equals(bacNetParentObject.getType())) {
				dev = DevicesHolder.getInstance().getDevice(bacNetParentObject.getID());
			} else {
				dev = getDevice(bacNetParentObject);
			}
		if (!dev.isConnected()) {
			boolean deviceAvailable = dev.connect();
			if (!deviceAvailable) {
				throw new BACnetException("no_reply");
			}
		}
		return dev;
	}

}
