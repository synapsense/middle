package com.synapsense.plugin.bacnet;

public class BACnetException extends RuntimeException {

	private static final long serialVersionUID = 9038044545026206850L;

	public BACnetException(String reason) {
		super(reason);

	}

	public BACnetException(Throwable t) {
		super(t);

	}

	public BACnetException(String reason, Throwable t) {
		super(reason, t);
	}
}
