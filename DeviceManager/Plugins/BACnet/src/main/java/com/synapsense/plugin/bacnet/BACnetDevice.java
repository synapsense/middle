package com.synapsense.plugin.bacnet;

import java.util.Iterator;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.synapsense.bacnet.network.media.MPDUHeader;
import com.synapsense.bacnet.network.media.Media;
import com.synapsense.bacnet.network.pdu.apdu.svc.SvcBase;
import com.synapsense.bacnet.network.pdu.apdu.svc.SvcResult;
import com.synapsense.bacnet.network.pdu.apdu.svc.request.WhoIs;
import com.synapsense.bacnet.network.pdu.apdu.svc.responce.IAm;
import com.synapsense.plugin.apinew.ObjectDesc;
import com.synapsense.plugin.bacnet.utils.BACnetUtils;

public class BACnetDevice {
	private final static Logger log = Logger.getLogger(BACnetDevice.class);
	private int dnet;
	private int instance;
	private String name = null;
	private MPDUHeader head = new MPDUHeader();
	private String ip;
	private Media media;
	private boolean connected = false;

	public BACnetDevice(Media media, ObjectDesc bndevice) {
		ObjectDesc bnNetwork = bndevice.getParent();
		this.media = media;
		this.dnet = Integer.parseInt(bnNetwork.getPropertyValue(BACnetConstants.BN_NETWORK_DNET));

		this.name = bnNetwork.getPropertyValue(BACnetConstants.BN_NETWORK_NAME);
		this.ip = bnNetwork.getPropertyValue(BACnetConstants.BN_NETWORK_DESTINATION);

		this.instance = Integer.parseInt(bndevice.getPropertyValue(BACnetConstants.BN_DEVICE_INSTANCE));

		if (name == null || name.isEmpty()) {
			throw new IllegalArgumentException("'Name' cannot be NULL or EMPTY");
		}

		if (dnet < -1 || dnet > 0xFFFF) {
			throw new IllegalArgumentException("'DNET' is out of range. Must be from '0' to '0xFFFF'");
		}

		if (instance < 0 || instance > 0x3FFFFF) {
			throw new IllegalArgumentException("'Instance' is out of range. Must be from '0' to '0x3FFFFF'");
		}
	}

	public BACnetDevice(Media media, Properties properties) {
		this.media = media;
		this.dnet = Integer.parseInt(properties.getProperty("net"));

		this.name = "BACNet_Network_" + dnet;
		this.ip = properties.getProperty("ip");

		this.instance = Integer.parseInt(properties.getProperty("dev"));

		if (dnet < -1 || dnet > 0xFFFF) {
			throw new IllegalArgumentException("'DNET' is out of range. Must be from '0' to '0xFFFF'");
		}

		if (instance < 0 || instance > 0x3FFFFF) {
			throw new IllegalArgumentException("'Instance' is out of range. Must be from '0' to '0x3FFFFF'");
		}
	}

	public BACnetDevice(Media media, int dnet, String ip, int dev) {
		this.media = media;
		this.dnet = dnet;

		this.name = "BACNet_Network_" + dnet;
		this.ip = ip;
		this.instance = dev;

		if (dnet < -1 || dnet > 0xFFFF) {
			throw new IllegalArgumentException("'DNET' is out of range. Must be from '0' to '0xFFFF'");
		}

		if (instance < 0 || instance > 0x3FFFFF) {
			throw new IllegalArgumentException("'Instance' is out of range. Must be from '0' to '0x3FFFFF'");
		}
	}

	public int getDnet() {
		return dnet;
	}

	public int getInstance() {
		return instance;
	}

	public String getName() {
		return name;
	}

	public MPDUHeader getHead() {
		return head;
	}

	/**
	 * Try to connect to BACnet device with such scenario: 1. Send Who-is to
	 * device and expect I-Am 2. Fill BACnetAddressBinding instance
	 */
	public boolean connect() {
		log.debug("Trying to connect to the  BACnet device instance [" + instance + "]");
		MPDUHeader header = new MPDUHeader();
		header.media = media;
		header.SRC_MAC = header.media.getActiveEthMac();
		if (ip == null) {
			header.DST_MAC = header.media.getBroadcast();
		} else {
			header.DST_MAC = BACnetUtils.getDestinationMac(ip);
		}
		if (dnet != -1) {
			header.DNET = dnet;
			header.HopCount = 255;
			header.DLEN = 0;// (header.DADR==null)?0:header.DADR.length();
		} else {
			log.info("DNET is not specifyed for device instance [" + instance + "]");
		}
		log.debug("Header was created: {media=\"" + header.media + "\", " + "SRC_MAC="
		        + BACnetUtils.toHex(header.SRC_MAC) + ", DST_MAC=" + BACnetUtils.toHex(header.DST_MAC) + "}");

		WhoIs request = new WhoIs(header);
		request.setDeviceInstanceRangeLowLimit(instance);
		request.setDeviceInstanceRangeHighLimit(instance);

		log.debug("WhoIs service was created: {lowLimit=" + request.getDeviceInstanceRangeLowLimit() + ", highLimit="
		        + request.getDeviceInstanceRangeHighLimit() + "}");

		request.createSession();

		SvcResult results = request.getResult();
		if (results.size() < 1 && ip != null) {
			log.info("Trying to send broadcast WhoIs request for device instance [" + instance + "]");
			header.DST_MAC = header.media.getBroadcast();
			request = new WhoIs(header);
			request.setDeviceInstanceRangeLowLimit(instance);
			request.setDeviceInstanceRangeHighLimit(instance);
			request.createSession();
			results = request.getResult();
		}
		if (results.size() < 1) {
			log.warn("None of devices answered to Who-Is request for device instance [" + instance + "]");
			return false;
		} else {

			Iterator<SvcBase> it = results.iterator();
			while (it.hasNext()) {
				SvcBase response = it.next();
				if (response instanceof IAm
				        && instance == ((IAm) response).getIAmDeviceIdentifier().getInstanceNumber()) {
					MPDUHeader responseHeader = response.getHeader();

					head.media = media;
					head.SRC_MAC = responseHeader.DST_MAC;
					head.DST_MAC = responseHeader.SRC_MAC;

					if (responseHeader.SADR != null) {
						head.DNET = responseHeader.SNET;
						head.DADR = responseHeader.SADR;
						head.DLEN = head.DADR.length;
						head.HopCount = 255;
					}

					if (log.isDebugEnabled()) {
						log.debug("Request header was created: {media=\"" + head.media.getMediaType() + "\", "
						        + "SRC_MAC=" + BACnetUtils.toHex(head.SRC_MAC) + ", DST_MAC="
						        + BACnetUtils.toHex(head.DST_MAC) + ",\nDNET=" + head.DNET + ", DADR="
						        + BACnetUtils.toHex(head.DADR) + ", DLEN=" + head.DLEN + ", HopCount=" + head.HopCount
						        + ", SADR=" + BACnetUtils.toHex(head.SADR) + "}");
					}

					log.info("Succesfully connected to device instance [" + instance + "]");
					connected = true;
					return connected;
				}

			}
			log.warn("Wrong answer came from device  instance [" + instance + "]" + results);
			return false;
		}
	}

	public boolean isConnected() {
		return connected;
	}

	public void setConnected(boolean connected) {
		this.connected = connected;
	}

	public String toString() {
		return "Ip [" + ip + "], Network [" + dnet + "], Instance Id [" + instance + "]";
	}

	public String getKey() {
		StringBuffer key = new StringBuffer(ip);
		key.append(":");
		key.append(dnet);
		key.append(":");
		key.append(instance);
		return key.toString();
	}
}
