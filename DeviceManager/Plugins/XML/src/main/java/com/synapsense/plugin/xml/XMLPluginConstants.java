package com.synapsense.plugin.xml;

public class XMLPluginConstants {

	public static final String TYPE_XML_HOST = "WEBSERVICEHOST";
	public static final String TYPE_XML_PROP = "WEBSERVICE";

	public static final String HOST_ADDRESS = "host";
	public static final String HOST_NAME = "name";

	public static final String PROP_NAME = "name";
	public static final String PROP_QUERY = "query";
	public static final String PROP_XPAPH = "xpath";
	public static final String PROP_LAST_VALUE = "lastValue";

}
