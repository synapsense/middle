package com.synapsense.plugin.xml;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.xml.xpath.XPathConstants;

import org.apache.log4j.Logger;

import com.synapsense.plugin.api.exceptions.ConfigPluginException;
import com.synapsense.plugin.api.exceptions.IncorrectStateException;
import com.synapsense.plugin.apinew.ObjectDesc;
import com.synapsense.plugin.apinew.Plugin;
import com.synapsense.plugin.apinew.PluginConfig;
import com.synapsense.plugin.apinew.Transport;
import com.synapsense.plugin.xml.utils.XMLHelper;

public class XMLPlugin implements Plugin {
	private static final Logger logger = Logger.getLogger(XMLPlugin.class);
	private static final String ID = "XML";
	private static Properties PLUGIN_DESC = new Properties();
	static {
		PLUGIN_DESC.put(PLUGIN_ID, ID);
		PLUGIN_DESC.put(ROOT_TYPES, "WEBSERVICEHOST");
	}
	private boolean configured = false;
	private boolean started = false;
	@SuppressWarnings("unused")
	private Transport transport = null;

	@Override
	public String getID() {
		return ID;
	}

	@Override
	public Properties getPluginDescription() {
		return PLUGIN_DESC;
	}

	public void setPropertyValue(String objectID, String propertyName, Serializable value) {
	}

	@Override
	public void setTransport(Transport transport) {
		this.transport = transport;
	}

	@Override
	public void start() throws IncorrectStateException {
		if (!configured) {
			throw new IncorrectStateException("Plugin " + ID + " has to be configured before start.");
		}
		if (!started) {
			started = true;
			logger.debug("Plugin " + ID + " is started.");
		}
	}

	public void stop() {
		configured = false;
		started = false;
		logger.debug("Plugin " + ID + " is stopped.");
	}

	@Override
	public void setPropertyValue(ObjectDesc object, String propertyName, Serializable value) {
		// Do nothing

	}

	@Override
	public void setPropertyValue(ObjectDesc object, List<String> propertyName, List<Serializable> values) {
		// Do nothing

	}

	@Override
	public Serializable getPropertyValue(ObjectDesc object, String propertyName) {
		String result = null;
		if (object != null) {
			ObjectDesc webhost = object.getParent();
			if (webhost != null) {
				try {
					// the plugin now pulls the entire request (URI & xpath)
					// from the input object, and nothing from the datasource
					logger.debug("Trying to pull a value from uri "
					        + object.getPropertyValue(XMLPluginConstants.PROP_QUERY) + " at xpath "
					        + object.getPropertyValue(XMLPluginConstants.PROP_XPAPH));
					String temp = (String) XMLHelper.evaluate(object.getPropertyValue(XMLPluginConstants.PROP_QUERY),
					        object.getPropertyValue(XMLPluginConstants.PROP_XPAPH), XPathConstants.STRING);
					result = temp;
					logger.debug("XPath '" + object.getPropertyValue(XMLPluginConstants.PROP_XPAPH) + "' results in '"
					        + result + "'");
				} catch (Exception e) {
					throw new RuntimeException(e);
				}
			} else {
				throw new IllegalArgumentException("Property with name='" + propertyName + "' doesn't exist.");
			}
		}
		return result;
	}

	@Override
	public List<Serializable> getPropertyValue(ObjectDesc object, List<String> propertyNames) {
		List<Serializable> result = new ArrayList<Serializable>();
		for (String property : propertyNames) {
			result.add(getPropertyValue(object, property));
		}
		return result;
	}

	@Override
	public void configure(PluginConfig config) throws ConfigPluginException {
		stop();
		configured = true;
		logger.debug("Plugin " + ID + " is configured.");
	}

	public static void main(String[] args) {
		if (args.length == 1) {
			try {
				XMLHelper.printDoc(args[0]);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			printHelp();
		}
	}

	public static void printHelp() {
		System.out.println("Usage: java -jar xmlplugin.jar URL");
	}

}
