package com.synapsense.plugin.xml.utils;

import java.io.InputStream;
import java.io.StringReader;
import java.net.Authenticator;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Map;

import javax.xml.namespace.QName;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

import org.apache.log4j.Logger;
import org.htmlcleaner.CleanerProperties;
import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.PrettyXmlSerializer;
import org.htmlcleaner.TagNode;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

public class XMLHelper {
	private static final Logger log = Logger.getLogger(XMLHelper.class);
	private static final XPath xpath = XPathFactory.newInstance().newXPath();
	private static Map<String, Integer> indexes = new HashMap<String, Integer>();

	public static Object evaluate(String url, String expression, QName returnType) throws Exception {
		String docStr = getDocumentAsString(url);
		StringReader strR = new StringReader(docStr);
		InputSource is = new InputSource(strR);
		BasicXMLNamespace ns = new BasicXMLNamespace();
		xpath.setNamespaceContext(ns);
		// NOTE: HtmlCleaner will coerce all tags to lower case; since xpaths
		// are case sensitive, we need to do the same to the xpath expression
		Object xpathresult = xpath.evaluate(expression.toLowerCase(), is, returnType);
		// If we have an empty string, we need to check and see if that's
		// actually *empty*, or an xpath that resolves to nothing
		if (xpathresult.toString().equals("")) {
			strR = new StringReader(docStr);
			is = new InputSource(strR);
			Object noderesult = xpath.evaluate(expression, is, XPathConstants.NODE);
			if (noderesult == null) {
				log.warn("XPath " + expression + " on URI " + url + " does not resolve to a value.");
				xpathresult = null;
			}
		}

		return (xpathresult);
	}

	public static String getDocumentAsString(String url) throws Exception {
		HtmlCleaner cleaner = new HtmlCleaner();
		CleanerProperties props = cleaner.getProperties();
		props.setNamespacesAware(false);
		URL _url = new URL(url);
		// TagNode cleanNode = cleaner.clean( _url );
		String user = "";
		String pass = "";

		if (_url.getUserInfo() != null) {
			String[] info = _url.getUserInfo().split(":");
			if (info.length > 0) {
				user = info[0];
			}
			if (info.length > 1) {
				pass = info[1];
			}
			Authenticator.setDefault(new XMLAuth(user, pass));
		}

		URLConnection conn = _url.openConnection();
		// System.out.println( conn.getContentType() );
		log.debug("Content Type for URL '" + url + "' is '" + conn.getContentType() + "' ");
		// System.out.println( conn.getContent() );
		if (conn.getContentType().contains("html")) {
			props.setOmitHtmlEnvelope(false);
		} else {
			props.setOmitHtmlEnvelope(true);
		}
		InputStream is = conn.getInputStream();
		TagNode cleanNode = cleaner.clean(is);
		PrettyXmlSerializer xs = new PrettyXmlSerializer(props);
		String docStr = xs.getAsString(cleanNode);
		log.debug("Document after cleanup:");
		log.debug(docStr);
		return (docStr);
	}

	public static Document getDocument(String url) throws Exception {
		throw new UnsupportedOperationException();
	}

	public static void printDoc(String url) throws Exception {
		Document doc = getDocument(url);
		NodeList children = doc.getChildNodes();
		indexes.clear();
		for (int i = 0; i < children.getLength(); i++) {
			printNode(children.item(i), "");
		}
	}

	private static void printNode(Node node, String xpath) {
		xpath = xpath + (xpath.isEmpty() ? "" : "/") + node.getNodeName();
		saveXpath(xpath);

		System.out.println("\tNode");
		System.out.println("\t\txPath='" + xpath + (getIndex(xpath) != 1 ? "[" + getIndex(xpath) + "]" : "")
		        + "'; value='" + node.getNodeValue() + "'");
		printAttributes(node);
		printChildren(node, xpath);
	}

	private static void saveXpath(String xpath) {
		Integer index = indexes.get(xpath);
		if (index == null)
			index = 0;
		indexes.put(xpath, index + 1);
	}

	private static int getIndex(String xpath) {
		return indexes.get(xpath) == null ? 1 : indexes.get(xpath);
	}

	private static void printChildren(Node node, String xpath) {
		NodeList children = node.getChildNodes();
		for (int i = 0; i < children.getLength(); i++) {
			printNode(children.item(i), xpath);
		}
	}

	private static void printAttributes(Node node) {
		NamedNodeMap map = node.getAttributes();
		if (map.getLength() != 0) {
			System.out.println("\t\tAttributes");
		}
		for (int i = 0; i < map.getLength(); i++) {
			Node atr = map.item(i);
			System.out.println("\t\t\tname='@" + atr.getNodeName() + "'; value='" + atr.getNodeValue() + "'");
		}
	}
}