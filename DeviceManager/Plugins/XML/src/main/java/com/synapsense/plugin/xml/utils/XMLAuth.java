package com.synapsense.plugin.xml.utils;

import java.net.Authenticator;
import java.net.PasswordAuthentication;

import org.apache.log4j.Logger;

class XMLAuth extends Authenticator {
	private static final Logger log = Logger.getLogger(XMLAuth.class);
	private String username;
	private String pass;

	public XMLAuth(String u, String p) {
		super();
		this.username = u;
		this.pass = p;
	}

	public PasswordAuthentication getPasswordAuthentication() {
		log.debug("Requesting Authentication Scheme is " + getRequestingScheme());
		// System.out.println( this.username + " " + this.pass );
		return new PasswordAuthentication(username, pass.toCharArray());
	}
}
