package com.synapsense.plugin.xml;

import java.io.IOException;
import java.io.Serializable;

import org.apache.log4j.Logger;

import com.synapsense.plugin.apinew.Transport;

public class LogTransport implements Transport {
	private static final Logger log = Logger.getLogger(LogTransport.class);

	@Override
	public void connect() throws IOException {
	}

	@Override
	public int getPriority() {
		return 0;
	}

	@Override
	public void sendData(String objectID, String propertyName, Serializable value) {
		log.debug("ObjectID='" + objectID + "'; propertyName='" + propertyName + "'; value='" + value + "'");
	}

	@Override
	public void sendOrderedData(String objectID, String propertyName, Serializable value) {
		sendData(objectID, propertyName, value);
	}
}
