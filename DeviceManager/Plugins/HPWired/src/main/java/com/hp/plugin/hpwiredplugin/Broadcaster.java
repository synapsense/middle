package com.hp.plugin.hpwiredplugin;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

import org.apache.log4j.Logger;

/**
 * <b>Broadcaster</b>
 * <p>
 * Thread to broadcast data packets to HP wired base stations
 * <p>
 * When an initialization data packet of type BS is received a {@link Cmd} type
 * data packet is created to acknowledge the init sequence. The {@link Cmd} type
 * message is sent back to the base station by the broadcasted back to the base
 * station.
 * 
 * @see Cmd Cmd
 */
public class Broadcaster extends Thread {

	private String _mess;
	private InetAddress _addr;
	private Integer _port;

	private DatagramSocket _socket;
	private DatagramPacket _dp;

	/**
	 * <b>Broadcaster</b>
	 * <p>
	 * <code>Broadcaster({@link InetAddress} addr, {@link Integer} port, {@link String} message)</code>
	 * <p>
	 * Constructor with broadcasting thread configuration parameters.
	 * 
	 * @param addr
	 *            broadcast - address to send packet
	 * @param port
	 *            broadcast - port to send packet
	 * @param message
	 *            String - message to send
	 */
	public Broadcaster(InetAddress addr, Integer port, String message) {
		this._addr = addr;
		this._port = port;
		this._mess = message;
	}

	public void run() {

		Logger log = Logger.getRootLogger();

		try {
			byte[] data = this._mess.getBytes();
			this._socket = new DatagramSocket();
			// InetAddress addr =
			// InetAddress.getByName(p.getProperty("outBroadcastIP"));
			// Integer port = new Integer(p.getProperty("outBroadcastPort"));

			this._dp = new DatagramPacket(data, data.length, this._addr, this._port);

			this._socket.send(_dp);

			this._socket.close();
		} catch (Exception e) {
			log.error("Broadcasting Exception : " + e.toString());
		}

	}

}
