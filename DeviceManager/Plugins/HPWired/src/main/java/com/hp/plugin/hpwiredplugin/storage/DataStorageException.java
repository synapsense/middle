package com.hp.plugin.hpwiredplugin.storage;

/**
 * <b>DataStorageException</b>
 * <p>
 * Exception for problems in DataStorage
 * 
 * @see com.hp.plugin.hpwiredplugin.storage.DataStorage DataStorage
 */
public class DataStorageException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3006897383385274205L;

	public DataStorageException(String mess) {
		super(mess);
	}

	public String toString() {
		return super.getMessage();
	}
}
