package com.hp.plugin.hpwiredplugin;

import java.io.ByteArrayInputStream;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.SocketTimeoutException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.log4j.Logger;

import com.hp.plugin.hpwiredplugin.storage.DataStorage;
import com.hp.plugin.hpwiredplugin.storage.DataStorageException;
// JABX
// LOG

/**
 * <p>
 * Broadcast data packets listener thread.
 * <p>
 * Thread listen and respond to broadcast {@literal <data>} and {@literal <bs>}
 * packets from HP wired base stations. It get the network configuration during
 * construction.
 * 
 * 
 */
public class BroadcastListener extends Thread {

	private InetAddress _addrIn;
	private InetAddress _addrOut;
	private Integer _portIn;
	private Integer _portOut;
	private InetAddress _interIn;
	@SuppressWarnings("unused")
	private InetAddress _interOut;

	private MulticastSocket _dsocket;
	private DatagramPacket _dpacket;

	// packet buffer
	private byte[] _buffer = new byte[1024];

	// flag to stop thread
	private boolean _enableListen = true;

	// receiver packet string
	private String _packetStr = null;

	private DateFormat df = null;

	// XML packet parsing stuff
	private JAXBContext jaxbContext;
	private Unmarshaller unmarshaller;

	// DataStorage
	private DataStorage _dataStorage = null;

	/**
	 * <b>BroadcastListener</b>
	 * <p>
	 * <code>BroadcastListener({@link InetAddress} addrIn, {@link Integer} portIn, {@link InetAddress} interIn, {@link InetAddress} addrOut, {@link Integer} portOut, {3link InetAddress] interOut, {@link DataStorage} dataStorage) throws {@link Exception}</code>
	 * <p>
	 * Broadcast listener thread constructor.
	 * 
	 * @param addrIn
	 *            - in broadcast address
	 * @param portIn
	 *            - in broadcast port number
	 * @param interIn
	 *            - in interface ip address
	 * @param addrOutm
	 *            - out broadcast address
	 * @param portOut
	 *            - out broadcast port number
	 * @param interOut
	 *            - out interface IP address
	 * @param dataStorage
	 *            - reference to
	 *            {@link com.hp.plugin.hpwiredplugin.storage#DataStorage
	 *            DataStorage} object
	 * @throws Exception
	 *             throws exception when constructor fails
	 */
	public BroadcastListener(InetAddress addrIn, Integer portIn, InetAddress interIn, InetAddress addrOut,
	        Integer portOut, InetAddress interOut, DataStorage dataStorage) throws Exception {
		this._addrIn = addrIn;
		this._portIn = portIn;
		this._interIn = interIn;

		this._addrOut = addrOut;
		this._portOut = portOut;
		this._interOut = interOut;

		if (dataStorage == null)
			throw new DataStorageException("null DataStorage reference");

		try {

			jaxbContext = JAXBContext.newInstance(Helper.PLUGIN_PACKET_PACKAGE_NAME);
			unmarshaller = jaxbContext.createUnmarshaller();

			this._dataStorage = dataStorage;

			// Datetime format from Hpwired basestations
			// df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss z");
			df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");

		} catch (Exception e) {
			throw e;
		}

	}

	// request thread stop
	public void stopListenning() {
		this._enableListen = false;
	}

	public void run() {
		Logger log = Logger.getRootLogger();

		if (log.isInfoEnabled())
			log.info("run called. setting listener:" + this._addrIn.toString() + ":" + this._portIn.toString());

		try {

			// open socket
			_dpacket = new DatagramPacket(_buffer, _buffer.length);
			_dsocket = new MulticastSocket(new Integer(_portIn));
			// connect
			_dsocket.setSoTimeout(500); // block block 500ms
			_dsocket.setInterface(_interIn);
			_dsocket.joinGroup(_addrIn);

			if (log.isInfoEnabled())
				log.info("run called. Listening:" + this._addrIn.toString() + ":" + this._portIn.toString());

		} catch (Exception e) {

			log.error("Exception error - " + e.toString());

			// do not enter loop
			this._enableListen = false;

		}

		// read packet until STOP
		while (this._enableListen) {
			try {

				// receive
				// clear _dpacket
				// _dpacket.setLength(0);
				// implemented with timeout (will throw exception)
				_dsocket.receive(_dpacket);
				_packetStr = new String(_dpacket.getData(), 0, _dpacket.getLength());

				if (log.isDebugEnabled())
					log.debug("Received packet: " + _packetStr);

				// handle new data packet
				if (_packetStr.length() > 0)
					HandleXMLDataPacket(_packetStr);

				// OS yield
				Thread.sleep(10);

			} catch (SocketTimeoutException e) {

				// _dsocket.received timed out
				// check if thread is interrupted
				if (this.isInterrupted()) {
					// exit loop
					this._enableListen = false;

					log.error("run called. thread Interrupted.");

				}

			} catch (InterruptedException e) {

				this._enableListen = false;

				log.error("run called. thread Interrupted.");

			} catch (Exception e) {

				log.error("Exception error - " + e.toString());

			}

		}

		// clean
		if (log.isInfoEnabled())
			log.info("Exit thread run().");

	}

	/**
	 * Process received XML {@link com.hp.plugin.hpwiredplugin.Data#Data Data}
	 * or {@link com.hp.plugin.hpwiredplugin.Bs#Bs Bs} data packets.
	 * 
	 * @param packetStr
	 *            received XML data packet
	 * @see com.hp.plugin.hpwiredplugin.Data Data
	 * @see com.hp.plugin.hpwiredplugin.Bs Bs
	 */
	private void HandleXMLDataPacket(String packetStr) {
		Logger log = Logger.getRootLogger();

		String sKey;

		float fTemp;

		try {

			// check packet type
			// <bs>
			if (packetStr.startsWith("<bs")) {
				// bs init request packet
				Bs bsPacket = (Bs) unmarshaller.unmarshal(new ByteArrayInputStream(packetStr.getBytes()));

				if (log.isDebugEnabled())
					log.debug("marshaled <bs> : " + bsPacket.getXML());

				// get mac address
				String mac = bsPacket.getMac();

				// Instantiate response cmd packet
				Cmd command = new Cmd(4, mac);

				if (log.isDebugEnabled())
					log.debug("broadcasting cmd : " + command.getXML());

				// SEND ACK;
				new Broadcaster(this._addrOut, this._portOut, command.getXML()).start();

			}
			// <data>
			else if (packetStr.startsWith("<data")) {
				// bs data packet
				Data dataPacket = (Data) unmarshaller.unmarshal(new ByteArrayInputStream(packetStr.getBytes()));

				if (log.isDebugEnabled())
					log.debug("marshaled <data> : " + dataPacket.getXML());

				// got Sandoval data packet
				// SEND to EDGE
				// mac address from Sandoval data packet is in AA:BB:CC:DD:00:00
				// format
				// convert to long
				String mac = dataPacket.getBsmac().replaceAll(":", "");
				Long macLong = Long.valueOf(mac, 16);
				// Date timeStamp = new
				// Date(Date.parse(dataPacket.getTimeStamp()));
				// Date timeStamp =
				// DateFormat.getInstance().parse(dataPacket.getTimeStamp());
				Date timeStamp = df.parse(dataPacket.getTimeStamp());

				// break dataPacket from Sandoval station into sensorData
				// objects
				// to be updated in dataStorage
				for (Piad p : dataPacket.getPiad()) {
					// determine if strip side to assign sensor number
					// front (Cold) 1(bottom) - 5(top)
					// back (Hot) 6(bottom) - 10(top)
					int sensorOffset = 0;
					if (p.getType() == 2)
						sensorOffset = 0;
					if (p.getType() == 3)
						sensorOffset = 5;

					for (Temp s : p.getTemp()) {
						// create key from mac long value and sensor channel
						// number
						sKey = Helper.createSensorDataStorageKey(macLong.toString(), s.getNo() + sensorOffset);
						fTemp = (float) Helper.tempConvertCtoF(s.getValue());
						_dataStorage.updateSensorData(sKey, timeStamp, fTemp);

					}

				}

			} else {
				// unrecognized broadcast packet type received
				// packet string start characters are not recognized
				if (log.isDebugEnabled())
					log.debug("Unrecognized broadcast packet received : " + packetStr);
			}

		} catch (JAXBException e) {
			log.error("Unmarshaller Exception : " + e.toString());
		} catch (Exception e) {
			log.error("Exception : " + e.toString());
		}

	}

	/**
	 * Not used
	 * 
	 * @param target
	 * @return
	 */
	@SuppressWarnings("unused")
	private String createACKCommandXML(String target) {
		StringBuilder cmd = new StringBuilder();
		;

		// check params TODO

		cmd.append("<cmd type=\"4\" ");
		cmd.append("target=\"" + target + "\" />");

		return cmd.toString();

	}

}
