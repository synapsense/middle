package com.hp.plugin.hpwiredplugin;

import java.util.concurrent.LinkedBlockingQueue;

import org.apache.log4j.Logger;

import com.synapsense.plugin.apinew.Transport;

//LOG

/**
 * <p>
 * Edge Application Server data inserter thread.
 * <p>
 * Thread gets data items from queue send new sensor values to Edge. Actual data
 * insertion is handled by the transport reference.
 * 
 * @see HPWiredPlugIn
 */
public class EdgeDataInserter extends Thread {
	// external objects reference
	private LinkedBlockingQueue<EdgeDataItem> _qSensorData;
	private Transport _trasport;

	private boolean _stopThread = false;

	private Logger log;

	/**
	 * <b>EdgeDataInserter</b>
	 * <p>
	 * Constructor
	 * <p>
	 * <code>public {@link EdgeDataInserter}({@link LinkedBlockingQueue}{@literal<{@link EdgeDataItem}>} queue, {@link Transport} t) throws {@link Exception}</code>
	 * 
	 * @param queue
	 *            - queue to get new data for application server
	 * @param t
	 *            - Transport reference to forward new data
	 * @throws Exception
	 */
	public EdgeDataInserter(LinkedBlockingQueue<EdgeDataItem> queue, Transport t) throws Exception {
		if (queue == null)
			throw new Exception("invalid queue reference.");
		if (t == null)
			throw new Exception("invalid Transport reference.");

		this._qSensorData = queue;
		this._trasport = t;

		log = Logger.getRootLogger();

	}

	public void run() {

		if (log.isInfoEnabled())
			log.info("Edge data inserter thread started.");

		// data from queue to send to Edge
		EdgeDataItem edgeDataItem = null;

		while (!_stopThread)

			try {

				// check if new data available
				if (this._qSensorData.size() > 0) {
					// data is available process
					// extract from queue
					edgeDataItem = (EdgeDataItem) this._qSensorData.take();

					if (log.isDebugEnabled())
						log.debug("Processing data item from queue : " + edgeDataItem.toString());

					// send sensor value to Edge
					// this._transport.sendData("SENSORID:1", "lastvalue",
					// 35.5);

					// DEBUG
					// GenericMessage gMess = new
					// GenericMessage(GenericMessageTypes.WSN_SENSOR_DATA);

					// fill generic message pairs
					// gMess.put("NODE_ID", edgeDataItem.getNODE_ID());
					// gMess.put("SENSOR_ID", edgeDataItem.getSENSOR_ID());
					// gMess.put("DATA", edgeDataItem.getDATA());
					// gMess.put("TIMESTAMP", edgeDataItem.getTIMESTAMP());
					// gMess.put("NETWORK_ID", edgeDataItem.getNETWORK_ID());
					// gMess.put("NODE_PLATFORM",
					// edgeDataItem.getNODE_PLATFORM());
					// gMess.put("TYPE_ID", edgeDataItem.getTYPE_ID());

					// send sensor value to Edge
					// sample: this._transport.sendData("SENSORID:1",
					// "lastValue", 25.5);
					this._trasport.sendData(edgeDataItem.getGlobalSensorID(), HPConstants.SENSOR_LAST_VALUE,
					        new Double(edgeDataItem.getDATA()));

					// get .sendData status and log

					// slowdown inserts?
					Thread.sleep(100);

				} else {
					// no new data to send to Edge OS yield
					Thread.sleep(1000);
				}

			} catch (InterruptedException e) {

				if (log.isInfoEnabled())
					log.info("Thread interrupted.");
				_stopThread = true;

			} catch (Exception e) {

				log.error(e.toString());
			}

	} // end loop

}
