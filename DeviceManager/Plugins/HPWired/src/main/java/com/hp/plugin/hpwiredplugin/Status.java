package com.hp.plugin.hpwiredplugin;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "status", propOrder = { "code", "reason" })
@XmlRootElement
public class Status {

	@XmlAttribute
	protected String code;
	@XmlAttribute
	protected int reason;

	public String[] getCode() {
		return code.split(",");
	}

	public int getReason() {
		return reason;
	}

	public String getXML() {
		StringBuilder sb = new StringBuilder();

		sb.append("<status code=\"");
		sb.append(code);
		sb.append("\" reason=\"");
		sb.append(reason);
		sb.append("\" />");

		return sb.toString();
	}

}
