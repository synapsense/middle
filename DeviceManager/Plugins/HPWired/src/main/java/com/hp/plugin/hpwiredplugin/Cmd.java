package com.hp.plugin.hpwiredplugin;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Defines the Cmd type broadcast message properties.
 * 
 * @see Broadcaster
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "cmd")
@XmlRootElement
public class Cmd {

	@XmlAttribute
	protected int type;
	@XmlAttribute
	protected String target;

	public Cmd(int type, String targetMac) {
		this.type = type;
		this.target = targetMac;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getTarget() {
		return target;
	}

	public void setTarget(String targetMac) {
		this.target = targetMac;
	}

	public String toString() {
		StringBuilder sb = new StringBuilder();

		sb.append("TYPE=");
		sb.append(this.type);
		sb.append(",TARGET=");
		sb.append(this.target);

		return sb.toString();
	}

	/**
	 * <b>getXML</b>
	 * <p>
	 * Gets the xml string packet format.
	 * 
	 * @return xml packet string
	 */
	public String getXML() {
		StringBuilder sb = new StringBuilder();
		;

		sb.append("<cmd type=\"");
		sb.append(this.type);
		sb.append("\" ");
		sb.append("target=\"");
		sb.append(this.target);
		sb.append("\" />");

		return sb.toString();
	}

}
