package com.hp.plugin.hpwiredplugin;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Class to represent the broadcast {@literal <ds>} packets.
 * <p>
 * XML data packets are unmarshaled and stored in instances of this class.
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "bs", propOrder = { "hw", "loc", "name", "fw", "sn", "pn", "mac", "pias", "status" })
@XmlRootElement
public class Bs {

	@XmlAttribute
	protected String hw;
	@XmlAttribute
	protected String loc;
	@XmlAttribute
	protected String name;
	@XmlAttribute
	protected String fw;
	@XmlAttribute
	protected String sn;
	@XmlAttribute
	protected String pn;
	@XmlAttribute
	protected String mac;

	protected Pias pias;

	protected Status status;

	public String getMac() {
		return mac;
	}

	/**
	 * Generates the XML for the {@literal <bs>} packet object.
	 * 
	 * @return XML format {@literal <bs>} packet.
	 */
	public String getXML() {
		StringBuilder sb = new StringBuilder();
		sb.append("<bs hw=\"");
		sb.append(hw);
		sb.append("\" ");
		sb.append("loc=\"");
		sb.append(loc);
		sb.append("\" ");
		sb.append("mac=\"");
		sb.append(mac);
		sb.append("\" ");
		sb.append("piacnt=\"");
		sb.append(pias.getCnt());
		sb.append("\" ");

		for (int i = 0; i < pias.getCnt(); i++) {
			sb.append("pia");
			sb.append(i);
			sb.append("=\"");
			sb.append(pias.getPia().get(i).getType());
			sb.append("\" ");

		}
		sb.append("/>");

		return sb.toString();
	}

}
