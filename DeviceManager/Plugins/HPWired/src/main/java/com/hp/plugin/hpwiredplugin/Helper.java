package com.hp.plugin.hpwiredplugin;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;
import java.util.concurrent.LinkedBlockingQueue;

public class Helper {

	public static final String LOG_FILE = "SandovalDMPlugIn_Log.txt";
	public static final String DATA_FILE = "SandovalDMPlugIn_Data.txt";
	public static final String DEBUG_FILE = "SandovalDMPlugIn_Debug.txt";

	public static final String PLUGIN_CONFIG_PACKAGE_NAME = "com.hp.plugin.hpwiredplugin.configuration";
	public static final String PLUGIN_PACKET_PACKAGE_NAME = "com.hp.plugin.hpwiredplugin";
	public static final int SENSORDATA_AGING_MINUTES = 5;

	public static class Globals {

		public static LinkedBlockingQueue<EdgeDataItem> edgeSendDataQueue = null;

	}

	public static class Configuration {

		public static boolean _enableLogFile = true;
		public static boolean _enableDataLogFile = false;
		public static boolean _enableDebugLogFile = false;

		public static int REFRESHRATE = 1;

		public static boolean _enableBroadcastListener = true;

		public static void sendToLogFile(String filename, String line) {

			try {
				FileWriter fw = new FileWriter(filename, true);
				fw.write(new Date().toString() + ":");
				fw.write(line + "\r\n");
				fw.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	// Creates key for DataStorage sensor data.
	public static String createSensorDataStorageKey(String mac, int channel) {
		StringBuilder sensorID = new StringBuilder();
		sensorID.append("MAC:");
		// sensorID.append(mac.replaceAll(":", ""));
		sensorID.append(mac);
		sensorID.append("|CHANNEL:");
		sensorID.append(channel);

		return sensorID.toString();

	}

	public static double tempConvertFtoC(double tempF) {
		return ((5.0 * tempF - 160.0) / 9);
	}

	public static double tempConvertCtoF(double tempC) {
		return ((9.0 * tempC / 5.0) + 32.0);
	}
}
