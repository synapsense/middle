package com.hp.plugin.hpwiredplugin.storage;

import java.util.Date;

/**
 * <b>SensorType</b>
 * <p>
 * Enumeration to hold sensor type
 * 
 */
enum SensorType {

TEMP, UNDEFINED

}

/**
 * <b>SensorData</b>
 * <p>
 * Holds sensor configuration and last received sensor value data.
 * <p>
 * The SensorData class defines the properties that need to be stored for proper
 * handling and manipulation of received sensor values. Some of the properties
 * are defined by the configuration and others are collected as the values are
 * received from broadcast messages.
 * <p>
 * Instances of SensorData for each base station sensor are stored in an object
 * of type {@link com.hp.plugin.hpwiredplugin.DataStorage DataStorage}
 * 
 * @see com.hp.plugin.hpwiredplugin.DataStorage DataStorage
 */
public class SensorData {

	private String _globalSensorID;
	private SensorType _sensorType;
	private int _refreshRate; // in minutes
	private float _deltaSendThreshold;

	// sensor generic message parameters
	private long _nodeid; // NODE_ID
	private int _sensorId; // SENSOR_ID
	private int _channel; // CHANNEL_ID
	private int _networkId; // NETWORK_ID
	private int _nodePlatform; // NODE_PLATFORM
	private int _typeId; // TYPE_ID

	private Date _lastReceived; // TIMESTAMP
	private Date _lastSent;
	private float _prevValue;
	private float _currValue; // DATA

	SensorData() {
		this._prevValue = 0;
		this._currValue = 0;
		this._deltaSendThreshold = 0;
		this._sensorType = SensorType.UNDEFINED;
		this._sensorId = 0;
		this._channel = 0;
		this._nodeid = 0;
		this._networkId = 0;
		this._nodePlatform = 0;
		this._typeId = 0;

		this._lastReceived = new Date();
		this._lastReceived.setTime(0);
		this._lastSent = new Date();
		this._lastSent.setTime(0);
	}

	/**
	 * <b>SensorData</b>
	 * <p>
	 * <code>SensorData( {@link java.lang.String String} globalSensorID, 
	 * 						{@link com.hp.plugin.hpwiredplugin.storage.SensorType SensorType} type, 
	 * 						long node_id, 
	 * 						int sensor_id, 
	 * 						int channel, 
	 * 						int network_id, 
	 * 						int node_platform, 
	 * 						int type_id, 
	 * 						int refreshRate, 
	 * 						float delta, 
	 * 						float currValue)</code>
	 * <p>
	 * Constructor
	 * 
	 * @param globalSensorID
	 *            - Synapsense global sensor identification name
	 *            "SANDOVALSENSOR:55"
	 * @param type
	 *            - sensor type
	 * @param node_id
	 *            - base station node unique identification number
	 * @param sensor_id
	 *            - configuration sensor identification number
	 * @param channel
	 *            - configuration sensor channel number
	 * @param network_id
	 *            - configuration network identification
	 * @param node_platform
	 *            - configuration platform identification
	 * @param type_id
	 *            - configurtion sensor type identification
	 * @param refreshRate
	 *            - sensor data update to application server
	 * @param delta
	 *            - sensor value change that will force a application server
	 *            data update
	 * @param currValue
	 *            - sensor data current value
	 */
	SensorData(String globalSensorID, SensorType type, long node_id, int sensor_id, int channel, int network_id,
	        int node_platform, int type_id, int refreshRate, float delta, float currValue) {
		// global edge sensor id SENSOR:1
		this._globalSensorID = globalSensorID;

		this._sensorType = type;

		// generic message parameters
		this._nodeid = node_id;
		this._sensorId = sensor_id;
		this._channel = channel;
		this._networkId = network_id;
		this._nodePlatform = node_platform;
		this._typeId = type_id;

		this._refreshRate = refreshRate;
		this._deltaSendThreshold = delta;
		this._currValue = currValue;

		this._lastReceived = new Date();
		this._lastReceived.setTime(0);
		this._lastSent = new Date();
		this._lastSent.setTime(0);
	}

	/**
	 * <b>getGlobalSensorID</b>
	 * <p>
	 * <code>public {@link java.lang.String String} getGlobalSensor()</code>
	 * <p>
	 * Access method
	 * 
	 * @return configuration global sensor name
	 */
	public String getGlobalSensorID() {
		return this._globalSensorID;
	}

	/**
	 * <b>getNODE_ID</b>
	 * <p>
	 * <code>long getNODE_ID()</code>
	 * <p>
	 * Access method
	 * 
	 * @return base station node identification number
	 */
	public long getNODE_ID() {
		return this._nodeid;
	}

	/**
	 * <b>getSENSOR_ID</b>
	 * <p>
	 * <code>public int getNODE_ID()</code>
	 * <p>
	 * Access method
	 * 
	 * @return configuration sensor global identification number
	 */
	public int getSENSOR_ID() {
		return this._sensorId;
	}

	/**
	 * <b>getCHANNEL_ID</b>
	 * <p>
	 * <code>public int getCHANNEL_ID()</code>
	 * <p>
	 * Access method
	 * 
	 * @return configuration sensor channel number
	 */
	public int getCHANNEL_ID() {
		return this._channel;
	}

	/**
	 * <b>getNETWORK_ID</b>
	 * <p>
	 * <code>public int getNETWORK_ID()</code>
	 * <p>
	 * Access method
	 * 
	 * @return configuration wired network number
	 */
	public int getNETWORK_ID() {
		return this._networkId;
	}

	/**
	 * <b>getNODE_PLATFORM</b>
	 * <p>
	 * <code>public int getNODE_PLATFORM()</code>
	 * <p>
	 * Access method
	 * 
	 * @return configuration node platform number
	 */
	public int getNODE_PLATFORM() {
		return this._nodePlatform;
	}

	/**
	 * <b>getTYPE_ID</b>
	 * <p>
	 * <code>public int getTYPE_ID()</code>
	 * <p>
	 * Access method
	 * 
	 * @return configuration sensor type number
	 */
	public int getTYPE_ID() {
		return this._typeId;
	}

	/**
	 * <b> setSENSOR_ID</b>
	 * <p>
	 * <code>public int setSENSOR_ID( int c)</code>
	 * <p>
	 * Access method
	 * 
	 * @param c
	 *            - sensor configuration identification number
	 */
	public void setSENSOR_ID(int c) {
		this._sensorId = c;
	}

	/**
	 * <b>setCHANNEL</b>
	 * <p>
	 * <code>public int setCHANNEL( int c)</code>
	 * <p>
	 * Access method
	 * 
	 * @param c
	 *            - sensor configuration channel number
	 */
	public void setChannel(int c) {
		this._channel = c;
	}

	/**
	 * <b>getCHANNEL</b>
	 * <p>
	 * <code>public int getCHANNEL()</code>
	 * <p>
	 * Access method
	 * 
	 * @return sensor configuration channel number
	 */
	public int getChannel() {
		return this._channel;
	}

	/**
	 * <b>setRefreshRate</b>
	 * <p>
	 * <code>public void setRefreshRate( int rate)</code>
	 * <p>
	 * Access method
	 * 
	 * @param rate
	 *            - sensor new value send rate in minutes to application server
	 */
	public void setRefreshRate(int rate) {
		this._refreshRate = rate;
	}

	/**
	 * <b>getRefreshRate</b>
	 * <p>
	 * <code>public int getRefreshRate()</code>
	 * <p>
	 * Access method
	 * 
	 * @return sensor new value send rate in minutes to application server
	 */
	public int getRefreshRate() {
		return this._refreshRate;
	}

	/**
	 * <b>setLastReceived</b>
	 * <p>
	 * <code>public void setLastReceived( {@link java.util.Date Date} dt)</code>
	 * <p>
	 * Access method
	 * 
	 * @param dt
	 *            - last sensor value receive timestamp
	 */
	public void setLastReceived(Date dt) {
		this._lastReceived = dt;
	}

	/**
	 * <b>getLastReceived</b>
	 * <p>
	 * <code>public {@link java.util.Date Date} getLastReceived()</code>
	 * <p>
	 * Access method
	 * 
	 * @return last sensor value received timestamp
	 */
	public Date getLastReceived() {
		return this._lastReceived;
	}

	/**
	 * <b>setLastSent</b>
	 * <p>
	 * <code>public void setLastSent( {@link java.util.Date Date} dt)</code>
	 * <p>
	 * Access method
	 * 
	 * @param dt
	 *            - sensor last value sent timestamp to application server
	 */
	public void setLastSent(Date dt) {
		this._lastSent = dt;
	}

	/**
	 * <b>getLastSent</b>
	 * <p>
	 * <code>public {@link javautil.Date Date} getLastSent()</code>
	 * <p>
	 * Access method
	 * 
	 * @return sensor last value sent timestamp to application server
	 */
	public Date getLastSent() {
		return this._lastSent;
	}

	/**
	 * <b>setCurrValue</b>
	 * <p>
	 * <code>public void setCurrValue( float val)</code>
	 * <p>
	 * Access method
	 * 
	 * @param val
	 *            - sensor current value
	 */
	public void setCurrValue(float val) {
		this._prevValue = this._currValue;
		this._currValue = val;
	}

	/**
	 * <b>getCurrValue</b>
	 * <p>
	 * <code>public float getCurrValue()</code>
	 * <p>
	 * Access method
	 * 
	 * @return sensor current value
	 */
	public float getCurrValue() {
		return this._currValue;
	}

	/**
	 * <b>getPrevValue</b>
	 * <p>
	 * <code>public float getPrevValue()</code>
	 * <p>
	 * Access method
	 * 
	 * @return sensor previous value
	 */
	public float getPrevValue() {
		return this._prevValue;
	}

	/**
	 * <b>setDeltaSendThreshold</b>
	 * <p>
	 * <code>public void setDeltaSendThreshold( float delta)</code>
	 * <p>
	 * Access method
	 * 
	 * @param delta
	 *            - sensor value change that will force data send to application
	 *            server
	 */
	public void setDeltaSendThreshold(float delta) {
		this._deltaSendThreshold = delta;

	}

	/**
	 * <b>getDeltaSendThreshold</b>
	 * <p>
	 * <code>public float getDeltaSendTreshold()</code>
	 * <p>
	 * Access method
	 * 
	 * @return sensor value chance that force data send to application server
	 */
	public float getDeltaSendThreshold() {
		return this._deltaSendThreshold;
	}

	/**
	 * <b>toString</b>
	 * <p>
	 * <code>public {@link java.lang.String String} toString()</code>
	 * <p>
	 * Readable object properties.
	 * 
	 * @return object properties values string
	 */
	public String toString() {
		StringBuilder sb = new StringBuilder();

		sb.append("SENSORID=");
		sb.append(this._globalSensorID);
		sb.append(",TYPE=");
		sb.append(this._sensorType.toString());
		sb.append(",NODE_ID=");
		sb.append(this._nodeid);
		sb.append(",SENSOR_ID=");
		sb.append(this._sensorId);
		sb.append(",CHANNEL=");
		sb.append(this._channel);
		sb.append(",REFRFESHRATE=");
		sb.append(this._refreshRate);
		sb.append(",DELTASENDTRESHOLD=");
		sb.append(this._deltaSendThreshold);
		sb.append(",CURRVALUE=");
		sb.append(this._currValue);
		sb.append(",PREVIOUSVALUE=");
		sb.append(this._prevValue);
		sb.append(",LASTRECEIVED=");
		sb.append(this._lastReceived);
		sb.append(",LASTSENT=");
		sb.append(this._lastSent);

		return sb.toString();
	}
}
