package com.hp.plugin.hpwiredplugin;

import java.io.Serializable;
import java.net.InetAddress;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.log4j.Logger;

import com.hp.plugin.hpwiredplugin.storage.DataStorage;
import com.hp.plugin.hpwiredplugin.storage.DataStorageException;
import com.synapsense.plugin.api.exceptions.ConfigPluginException;
import com.synapsense.plugin.api.exceptions.IncorrectStateException;
import com.synapsense.plugin.apinew.ObjectDesc;
import com.synapsense.plugin.apinew.Plugin;
import com.synapsense.plugin.apinew.PluginConfig;
import com.synapsense.plugin.apinew.Transport;

;

/**
 * HPWiredPlugIn
 * 
 * <p>
 * HP Device Manager PlugIn implementation to support Sandoval LAN wired base
 * stations.
 * <p>
 * The class implements the com.synapsense.plugin.apinew.Plugin to create a
 * custom PlugIn supported but the Synapsense architecture.
 * 
 * @see com.hp.plugin.hpwiredplugin.BroadcastListener BroadcastListener
 * @see com.hp.plugin.hpwiredplugin.EdgeDataInserter EdgeDataInserter
 * 
 */
public class HPWiredPlugIn implements Plugin {

	/**
	 * Plugin unique identifying name returned by {@link HPWiredPlugIn#getID()
	 * getID()}
	 */
	private static final String ID = "HPWiredPlugIn";
	private static Properties PLUGIN_DESC = new Properties();
	static {
		PLUGIN_DESC.put(PLUGIN_ID, ID);
		PLUGIN_DESC.put(ROOT_TYPES, "SANDOVALNETWORK");
	}
	private final static Logger logger = Logger.getLogger(HPWiredPlugIn.class);
	// enabled features
	private boolean started = false;
	private boolean configured = false;

	// threads
	private Thread _tBroadcastListener;
	private Thread _tEdgeDataInserter;

	private Thread _tSensorDataMonitor;

	private String _inAddressStr;
	private String _inPortStr;
	private String _inInterfaceStr;

	private String _outAddressStr;
	private String _outPortStr;
	private String _outInterfaceStr;

	private InetAddress _inAddress;
	private Integer _inPort;
	private InetAddress _inInterface;

	private InetAddress _outAddress;
	private Integer _outPort;
	private InetAddress _outInterface;

	private DataStorage _dataStorage = null;

	private Transport _t = null;

	// @Override
	// public void enable() {
	/**
	 * Enables main plugin broadcast listening threads. Only called after when
	 * both {@link #start()} and {@link #configure(String)} are called by the
	 * Device Manager service.
	 */
	private void enable() {

		logger.info("Starting PlugIn...");

		try {
			// verify Transport reference
			if (_t == null)
				throw new Exception("Invalid Transport reference");

			// start threads
			if (Helper.Configuration._enableBroadcastListener) {
				// get network configuration parameters

				// start edge data inserter thread
				LinkedBlockingQueue<EdgeDataItem> q = new LinkedBlockingQueue<EdgeDataItem>();
				Helper.Globals.edgeSendDataQueue = q;
				_tEdgeDataInserter = new EdgeDataInserter(Helper.Globals.edgeSendDataQueue, _t);
				_tEdgeDataInserter.start();

				// start listener thread
				_tBroadcastListener = new BroadcastListener(_inAddress, _inPort, _inInterface, _outAddress, _outPort,
				        _outInterface, this._dataStorage);
				_tBroadcastListener.start();

				// start Sensor Data Monitor thread
				_tSensorDataMonitor = new SensorDataMonitor(this._dataStorage);
				_tSensorDataMonitor.start();
			}

			logger.info("PlugIn started.");
			started = true;
		} catch (Exception e) {
			logger.error(e.toString());
		}

	}

	private void stopListeningThreads() {

		if (_tBroadcastListener != null) {
			try {

				if (_tBroadcastListener.isAlive()) {
					_tBroadcastListener.interrupt();
					_tBroadcastListener.join(10000);

					if (logger.isInfoEnabled())
						logger.info("BroadcastListen thread terminated.");

				}
				_tBroadcastListener = null;

			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();

			} catch (Exception e) {
				//
				logger.error("Exception: " + e.toString());
			}
		}

	}

	/**
	 * Called by Device Manager when the configuration is ready for processing.
	 * The configuration String is provided by the service from xml file or
	 * application server for standalone or application server mode
	 * respectively.
	 * 
	 * @param arg0
	 *            contains the xml text for the plugin configuration. The string
	 *            is provided by the Device Manager.
	 * @exception ConfigPluginException
	 *                thrown if any problem was detected processing the
	 *                configuration xml string
	 */
	@Override
	public void configure(PluginConfig pluginConfig) throws ConfigPluginException {

		try {

			if (logger.isInfoEnabled())
				logger.info("configure() method called.");

			if (pluginConfig == null) {
				logger.error("Empty plugin config for the HP Wired plugin.");
				throw new ConfigPluginException("Empty plugin config for the HP Wired plugin");
			}
			stop();
			if (logger.isInfoEnabled())
				logger.info("Loading DataStorage structure.");
			// provide real Sandovalsn object
			DataStorage.InitDataStorage(pluginConfig);
			_dataStorage = DataStorage.getDataStorage();

			// since only one network is supported(???) get the first one in the
			// list
			ObjectDesc network = pluginConfig.getRoots().get(0);
			_inAddressStr = network.getPropertyValue(HPConstants.NETWORK_IN_BROADCAST_IP);
			_inPortStr = network.getPropertyValue(HPConstants.NETWORK_IN_BROADCAST_PORT);
			_inInterfaceStr = network.getPropertyValue(HPConstants.NETWORK_BROADCAST_INTERF_IP);
			_outAddressStr = network.getPropertyValue(HPConstants.NETWORK_OUT_BROADCAST_IP);
			_outPortStr = network.getPropertyValue(HPConstants.NETWORK_OUT_BROADCAST_PORT);
			_outInterfaceStr = network.getPropertyValue(HPConstants.NETWORK_BROADCAST_INTERF_IP);

			// net settings
			_inAddress = InetAddress.getByName(_inAddressStr);
			_inPort = new Integer(_inPortStr);
			_inInterface = InetAddress.getByName(_inInterfaceStr);

			_outAddress = InetAddress.getByName(_outAddressStr);
			_outPort = new Integer(_outPortStr);
			_outInterface = InetAddress.getByName(_outInterfaceStr);
			// configuration loaded ok
			// set flag
			this.configured = true;

		} catch (Exception e) {
			throw new ConfigPluginException(e.getMessage());
		}
		logger.debug("Plugin " + ID + " is configured.");

	}

	/**
	 * Used by Device Manager service to get the plugin unique identification
	 * name
	 * 
	 * @return the plugin name
	 * @see HPWiredPlugIn#PLUGIN_ID
	 */
	@Override
	public String getID() {

		return ID;
	}

	@Override
	public Properties getPluginDescription() {
		return PLUGIN_DESC;
	}

	/**
	 * Called by Device Manager to pass Transport object reference
	 * 
	 * @param arg0
	 *            Transport object reference provided by Device manager
	 */
	@Override
	public void setTransport(Transport arg0) {

		// set transport reference
		this._t = arg0;

		if (logger.isInfoEnabled())
			logger.info("Transport set.");

	}

	/**
	 * Called by Device Manager after the plugin is loaded and ready to start.
	 * The plugin main logic is enabled after both {@link #start()} and
	 * {@link #configure()} has been called.
	 * 
	 * @throws IncorrectStateException
	 */
	@Override
	public void start() throws IncorrectStateException {

		if (logger.isInfoEnabled())
			logger.info("start() method called ...");
		if (!configured) {
			throw new IncorrectStateException("Plugin " + ID + " has to be configured before start.");
		}
		// check if configure() was called
		if (started) {
			enable();
			logger.debug("Plugin " + ID + " is started.");
		}

	}

	@Override
	public void stop() {
		if (started) {
			stopListeningThreads();
			_tEdgeDataInserter.interrupt();
			_tSensorDataMonitor.interrupt();
			try {
				DataStorage.getDataStorage().clearDataStorage();
			} catch (DataStorageException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		configured = false;
		started = false;
		logger.debug("Plugin " + ID + " is stopped.");
	}

	@Override
	public void setPropertyValue(ObjectDesc object, String propertyName, Serializable value) {
		throw new UnsupportedOperationException("Set property value is not supported by the HP Wired plug-in");

	}

	@Override
	public void setPropertyValue(ObjectDesc object, List<String> propertyName, List<Serializable> values) {
		throw new UnsupportedOperationException("Set property value is not supported by the HP Wired plug-in");

	}

	@Override
	public Serializable getPropertyValue(ObjectDesc object, String propertyName) {
		throw new UnsupportedOperationException("Get property value is not supported by the HP Wired plug-in");

	}

	@Override
	public List<Serializable> getPropertyValue(ObjectDesc object, List<String> propertyNames) {
		throw new UnsupportedOperationException("Get property value is not supported by the HP Wired plug-in");
	}

}
