package com.hp.plugin.hpwiredplugin;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "temp", propOrder = { "no", "value" })
@XmlRootElement
public class Temp {

	@XmlAttribute
	protected int no;
	@XmlValue
	protected float value;

	public int getNo() {
		return this.no;
	}

	public float getValue() {
		return value;
	}

	public String getXML() {
		StringBuilder sb = new StringBuilder();

		sb.append("<temp ");
		sb.append("no=\"");
		sb.append(this.no);
		sb.append("\">");
		sb.append(this.value);
		sb.append("</temp>");

		return sb.toString();

	}
}
