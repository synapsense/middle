package com.hp.plugin.hpwiredplugin;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "pias", propOrder = { "cnt", "pia" })
@XmlRootElement
public class Pias {

	@XmlAttribute
	protected int cnt;

	protected List<Pia> pia;

	public int getCnt() {
		return cnt;
	}

	public List<Pia> getPia() {
		return pia;
	}

}
