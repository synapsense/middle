package com.hp.plugin.hpwiredplugin.storage;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Set;

import org.apache.log4j.Logger;

import com.hp.plugin.hpwiredplugin.EdgeDataItem;
import com.hp.plugin.hpwiredplugin.HPConstants;
import com.hp.plugin.hpwiredplugin.Helper;
import com.synapsense.plugin.apinew.ObjectDesc;
import com.synapsense.plugin.apinew.PluginConfig;

/**
 * <b>DataStorage</b>
 * <p>
 * Provides storage for sensor data received from base stations. It contains a
 * HashMap indexed by the base station node id (mac address) and the sensor id
 * within the station.
 * 
 */
public class DataStorage {

	// Map for base stations DSC info
	private HashMap<String, SensorData> _sdHashMap;

	private Logger _log;

	private static DataStorage _ds;
	private SimpleDateFormat _dateFormat;

	private DataStorage() {
		_sdHashMap = new HashMap<String, SensorData>();
		_log = Logger.getRootLogger();

		_dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
	}

	/**
	 * <b>getDataStorage</b>
	 * <p>
	 * <code>public static {@link com.hp.plugin.hpwiredplugin.storage.DataStorage DataStorage} getDataStorage()</code>
	 * <p>
	 * Returns the DataStorage instance.
	 * 
	 * @return instance of the DataStorage single object
	 * @throws DataStorageException
	 *             throws this exception when DataStorage object is not valid.
	 * @see DataStorageException
	 */
	public static DataStorage getDataStorage() throws DataStorageException {
		// if (_ds == null)
		// {
		// _ds = new DataStorage();
		// }
		//
		if (_ds == null)
			throw new DataStorageException("DataStorage not initialized.");
		return _ds;

	}

	/**
	 * <b>clearDataStorage</b>
	 * <p>
	 * <code>public void clearDataStorage()</code>
	 * <p>
	 * Cleans the sensor data map
	 */
	public void clearDataStorage() {
		if (this._sdHashMap != null)
			this._sdHashMap.clear();
	}

	/**
	 * <b>InitDataStorage</b>
	 * <p>
	 * <code>public static void InitDataStorage( {@link com.hp.plugin.hpwiredplugin.configuration.Sandovalsn Sandovalsn} config)</code>
	 * <p>
	 * Initializes the DataStorage instance with configuration object created
	 * from XML configuration.
	 * 
	 * @param config
	 *            - configuration object created at
	 *            {@link com.hp.plugin.hpwiredplugin.HPWiredPlugIn#configure(String)
	 *            configure(String)}
	 * @throws DataStorageException
	 * @see Sandovalsn
	 */
	public static void InitDataStorage(PluginConfig config) throws DataStorageException {
		if (config == null)
			throw new DataStorageException("Invalid config reference");

		Logger log = Logger.getRootLogger();

		// place holders
		String sKey = null;
		SensorData sensorData = null;
		;
		float deltaT = 0;

		// create DataStorage sensor map from config reference.
		// DataStorage d = DataStorage.getDataStorage();

		try {
			// create instance of data storage if needed
			if (_ds == null) {
				_ds = new DataStorage();
			}

			// loop for every node in Sandovalsn and
			// add sensors to DataStorage using insertSensor()
			// configuration information
			_ds.clearDataStorage();

			for (ObjectDesc nwk : config.getRoots()) {
				for (ObjectDesc node : nwk.getChildren()) {
					for (ObjectDesc sensor : node.getChildren()) {
						int channel = Integer.parseInt(sensor.getPropertyValue(HPConstants.SENSOR_CHANNEL));
						// select delta for sensor rack side from channel
						if ((channel >= 1) && (channel <= 5))
							deltaT = new Float(node.getPropertyValue(HPConstants.NODE_FR_DELTA_SEND_THRES));
						if ((channel >= 6) && (channel <= 10))
							deltaT = new Float(node.getPropertyValue(HPConstants.NODE_BK_DELTA_SEND_THRES));

						String macNumberStr = node.getPropertyValue(HPConstants.NODE_MAC_ADDRESS).replace(":", "");
						// Note: mac address from config is in Long base 10
						// format
						Long macNumberLong = Long.valueOf(macNumberStr, 10);

						// get is numbers from config id
						Long nodeIdLong = Long.valueOf(node.getID().split(":")[1]);
						Integer sensorIdInt = Integer.valueOf(sensor.getID().split(":")[1]);
						Integer networkIdInt = new Integer(nwk.getID().split(":")[1]);

						// insert sensor in data storage
						sensorData = new SensorData(sensor.getID(), // global
						                                            // sensorid
						        SensorType.TEMP, nodeIdLong, sensorIdInt, channel, networkIdInt, new Integer(
						                node.getPropertyValue(HPConstants.NODE_TYPE)), // NODE_PLATFORM
						        1, // TYPE_ID
						        new Integer(node.getPropertyValue(HPConstants.NODE_REFRESH_RATE)), deltaT, 0 // data
						);

						sKey = Helper.createSensorDataStorageKey(macNumberLong.toString(), channel);
						if (log.isDebugEnabled())
							log.debug("DataStorage inserting ... " + sKey + " : " + sensorData.toString());
						_ds.insertSensor(sKey, sensorData);

					}
				}
			}

		} catch (Exception e) {
			log.error(e.getMessage());
			throw new DataStorageException(e.getMessage());
		}
	}

	/**
	 * <b>insertSensor</b>
	 * <p>
	 * <code>public void insertSensor( {@link java.lang.String String} sensorID, {@link com.hp.plugin.hpwiredplugin.storage.SensorData SensorData} sd)</code>
	 * <p>
	 * <code>void insertSensor( {@link java.lang.String String}, {@link com.hp.plugin.hpwiredplugin.storage.SensorData SensorData})</code>
	 * <p>
	 * Adds new sensor to DataStorage
	 * 
	 * @param sensorID
	 *            - unique sensor identification String made from node id and
	 *            sensor number
	 * @param sd
	 *            - sensor configuration information
	 * @throws DataStorageException
	 */
	public void insertSensor(String sensorID, SensorData sd) throws DataStorageException {
		try {
			this._sdHashMap.put(sensorID, sd);
		} catch (Exception e) {
			throw new DataStorageException("Error in DataStore:insertSensor() - " + e.getMessage());
		}
	}

	/**
	 * <b>updateSensorData</b>
	 * <p>
	 * <code>public void updateSensorData( {@link java.lang.String String} sensorKey, {@link java.util.Date Date} timestamp, float value)</code>
	 * <p>
	 * Updates sensor with new received value
	 * 
	 * @param sensorKey
	 *            - unique sensor DataStorage key
	 * @param timestamp
	 *            - received sensor value timestamp
	 * @param value
	 *            - new sensor value
	 * @throws DataStorageException
	 */
	public void updateSensorData(String sensorKey, Date timestamp, float value) throws DataStorageException {
		boolean sensorDataSendNeeded = false;

		try {

			SensorData sd = this._sdHashMap.get(sensorKey);
			if (sd != null) {
				// update data
				sd.setLastReceived(timestamp);
				sd.setCurrValue(value);

				// check if Edge needs new value for sensor
				// check for configuration refresh time
				Date dsend = new Date();
				dsend.setTime(sd.getLastSent().getTime() + sd.getRefreshRate() * 60000);
				// DEBUG
				if (_log.isDebugEnabled())
					_log.debug(sensorKey + " : Next send " + _dateFormat.format(dsend) + " and current "
					        + _dateFormat.format(timestamp));

				if (timestamp.after(dsend)) {
					if (_log.isDebugEnabled())
						_log.debug("Updating sensor value due to Next send < current timestamp");

					// Edge update needed
					sensorDataSendNeeded = true;
				}

				// DEBUG
				if (_log.isDebugEnabled())
					_log.debug(sensorKey + " : check temp delta " + sd.getCurrValue() + " - " + sd.getPrevValue()
					        + " >= " + sd.getDeltaSendThreshold());

				// check if send needed by delta value
				if (Math.abs(sd.getCurrValue() - sd.getPrevValue()) >= sd.getDeltaSendThreshold()) {
					if (_log.isDebugEnabled())
						_log.debug("Updating sensor value due to delta temp");

					// Edge update needed
					sensorDataSendNeeded = true;
				}

				// Edge needs value update
				if (sensorDataSendNeeded) {
					// add send parameter to queue.
					// Edge send thread will handle the item
					EdgeDataItem dItem = new EdgeDataItem(sd.getGlobalSensorID(), sd.getNODE_ID(), sd.getSENSOR_ID(),
					        sd.getCurrValue(), sd.getLastReceived().getTime(), sd.getNETWORK_ID(),
					        sd.getNODE_PLATFORM(), sd.getTYPE_ID());

					Helper.Globals.edgeSendDataQueue.put(dItem);

					// update last time sent
					sd.setLastSent(timestamp);
					// Debug
					if (_log.isDebugEnabled())
						_log.debug("Sent to queue EdgeDataItem: " + dItem.toString());

				}
			} else {
				// sensor not in data storage
				// IGNORE - WHAT to DO with data from stations not configured?

				if (_log.isDebugEnabled())
					_log.debug("Received data for sensor not in configuration SKey:" + sensorKey + " Timestamp:"
					        + timestamp + " Value: " + value);
			}

		} catch (Exception e) {

			// log error
			_log.error(e.getMessage());

			throw new DataStorageException(e.getMessage());

		}
	}

	/**
	 * <b>monitorSensorData</b>
	 * <p>
	 * <code>public void monitorSensorData( {@link java.util.Date Date} timestamp)</code>
	 * <p>
	 * Verifies each sensor data item
	 * 
	 * @param timestamp
	 *            - monitor aging value timestamp
	 * @throws DataStorageException
	 */
	public void monitorSensorData(Date timestamp) throws DataStorageException {
		boolean sensorAlertSendNeeded = false;
		float alertValue = 0.0f;

		try {

			Set<String> keySet = this._sdHashMap.keySet();

			for (String key : keySet) {
				sensorAlertSendNeeded = false;
				SensorData sd = this._sdHashMap.get(key);
				if (sd != null) {
					// lock sensor data object
					synchronized (sd) {
						// perform verifications

						// Aging

						// check if Edge needs Aging alert for sensor
						// check for configuration refresh time
						Date oldestRecv = new Date();
						oldestRecv.setTime(timestamp.getTime() - Helper.SENSORDATA_AGING_MINUTES * 60000);

						// DEBUG
						if (_log.isDebugEnabled())
							_log.debug(key + " : Oldest Recv " + _dateFormat.format(oldestRecv) + " and last Recv "
							        + _dateFormat.format(sd.getLastReceived()));

						if (sd.getLastSent().before(oldestRecv)) {
							if (_log.isDebugEnabled())
								_log.debug("Sensor Data ALERT last Recv < Oldest Recv timestamp");

							// Edge update needed
							alertValue = -3000.0f;
							sensorAlertSendNeeded = true;
						}

						// Edge needs Alert sensor value update
						if (sensorAlertSendNeeded) {
							// add send parameter to queue.
							// Edge send thread will handle the item
							EdgeDataItem dItem = new EdgeDataItem(sd.getGlobalSensorID(), sd.getNODE_ID(),
							        sd.getSENSOR_ID(), alertValue, sd.getLastReceived().getTime(), sd.getNETWORK_ID(),
							        sd.getNODE_PLATFORM(), sd.getTYPE_ID());

							Helper.Globals.edgeSendDataQueue.put(dItem);

							// update last time sent
							// sd.setLastSent(timestamp);

							// Debug
							if (_log.isDebugEnabled())
								_log.debug("Sent to queue EdgeDataItem: " + dItem.toString());

						}
					}
				}
			}
		} catch (Exception e) {

			// log error
			_log.error(e.getMessage());

			throw new DataStorageException(e.getMessage());

		}
	}
}
