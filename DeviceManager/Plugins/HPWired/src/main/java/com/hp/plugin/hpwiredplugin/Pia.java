package com.hp.plugin.hpwiredplugin;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "pia", propOrder = { "hw", "type", "pn", "sn", "id" })
@XmlRootElement
public class Pia {

	@XmlAttribute
	protected String hw;
	@XmlAttribute
	protected int type;
	@XmlAttribute
	protected String pn;
	@XmlAttribute
	protected String sn;
	@XmlAttribute
	protected String id;

	public String getHw() {
		return hw;
	}

	public void setHw(String value) {
		this.hw = value;
	}

	public int getType() {
		return type;
	}

	public void setType(int value) {
		this.type = value;
	}

	public String getPn() {
		return pn;
	}

	public void setPn(String value) {
		this.pn = value;
	}

	public String getSn() {
		return sn;
	}

	public void setSn(String value) {
		this.sn = value;
	}
}
