package com.hp.plugin.hpwiredplugin;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "piad", propOrder = { "type", "id", "temp" })
@XmlRootElement
public class Piad {

	@XmlAttribute
	protected int type;
	@XmlAttribute
	protected String id;
	@XmlElement
	protected List<Temp> temp;

	public int getType() {
		return type;
	}

	public String getId() {
		return id;
	}

	public List<Temp> getTemp() {
		return temp;
	}

	public String getXML() {
		StringBuilder sb = new StringBuilder();

		sb.append("<piad type=\"");
		sb.append(this.type);
		sb.append("\" id=\"");
		sb.append(this.id);
		sb.append("\">");

		// get temps
		for (Temp t : temp) {
			sb.append(t.getXML());
		}

		sb.append("</piad>");

		return sb.toString();
	}

}
