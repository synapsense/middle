package com.hp.plugin.hpwiredplugin;

import java.util.Date;

import org.apache.log4j.Logger;

import com.hp.plugin.hpwiredplugin.storage.DataStorage;

//LOG

/**
 * <p>
 * Sensor Data Monitor thread.
 * <p>
 * Thread examines sensor data to verify aging. It calls DataStore monitor
 * method to perform verifcations on the state of sensor data.
 * 
 * @see HPWiredPlugIn
 */
public class SensorDataMonitor extends Thread {
	// external objects reference
	private DataStorage _ds;

	private boolean _stopThread = false;

	private Logger log;

	/**
	 * <b>SensorDataMonitor</b>
	 * <p>
	 * Constructor
	 * <p>
	 * <code>public {@link SensorDataMonitor}({@link DataStorage} ds) throws {@link Exception}</code>
	 * 
	 * @param ds
	 *            - DataStorage reference to check sensor data
	 * @throws Exception
	 */
	public SensorDataMonitor(DataStorage ds) throws Exception {
		if (ds == null)
			throw new Exception("invalid DataStorage reference.");

		this._ds = ds;

		log = Logger.getRootLogger();

	}

	public void run() {

		if (log.isInfoEnabled())
			log.info("Edge SensorDataMonitor thread started.");

		while (!_stopThread)

			try {

				// call DataStorage monitor method
				if (log.isDebugEnabled())
					log.debug("Calling DataStorage,monitorSensorData()...");

				this._ds.monitorSensorData(new Date());

				// no new data to send to Edge OS yield
				Thread.sleep(Helper.SENSORDATA_AGING_MINUTES * 60000);

			} catch (InterruptedException e) {

				if (log.isInfoEnabled())
					log.info("Thread interrupted.");
				_stopThread = true;

			} catch (Exception e) {

				log.error(e.toString());
			}

	} // end loop

}
