package com.hp.plugin.hpwiredplugin;

// Edge required parameters for dB
/**
 * <b>EdgeDataItem</b>
 * <p>
 * Class defines properties neeed to send sensor values to application server
 */
public class EdgeDataItem {
	// Edge sensor global id
	private String _globalSensorID;
	private long _node_id;
	// sensor channel
	private int _sensor_id; // SENSOR_ID
	private long _timestamp;
	private int _network_id;
	private int _node_platform;
	private int _type_id;

	// value
	private float _data;

	/**
	 * <b>EdgeDataItem</b>
	 * <p>
	 * <code>public EdgeDataItem( {@link java.lang.String String} globalSensorID, 
	 * 								long node_id, 
	 * 								int channel, 
	 * 								float data, 
	 * 								long timeStamp, 
	 * 								int network_id, 
	 * 								int node_platform, 
	 * 								int type_id)</code>
	 * <p>
	 * Stores sensor configuratuion and sampled value to be sent to application
	 * server
	 * 
	 * @param globalSensorID
	 *            - global sensor identification "SENSORID:55"
	 * @param node_id
	 *            - node unique id number
	 * @param channel
	 *            - sensor channel number
	 * @param data
	 *            - sensor value
	 * @param timeStamp
	 *            - when the value was sampled
	 * @param network_id
	 *            - network unique identification numbner
	 * @param node_platform
	 *            - node platform
	 * @param type_id
	 *            - sensor type
	 */
	public EdgeDataItem(String globalSensorID, long node_id, int channel, float data, long timeStamp, int network_id,
	        int node_platform, int type_id) {
		this._globalSensorID = globalSensorID;
		this._node_id = node_id;
		this._sensor_id = channel;
		this._data = data;
		this._timestamp = timeStamp;
		this._network_id = network_id;
		this._node_platform = node_platform;
		this._type_id = type_id;
	}

	public String getGlobalSensorID() {
		return this._globalSensorID;
	}

	public long getNODE_ID() {
		return this._node_id;
	}

	public int getSENSOR_ID() {
		return this._sensor_id;
	}

	public float getDATA() {
		return this._data;
	}

	public long getTIMESTAMP() {
		return this._timestamp;
	}

	public int getNETWORK_ID() {
		return this._network_id;
	}

	public int getNODE_PLATFORM() {
		return this._node_platform;
	}

	public int getTYPE_ID() {
		return this._type_id;
	}

	public String toString() {
		StringBuilder sb = new StringBuilder();

		sb.append("GLOBALSENSORID=");
		sb.append(this._globalSensorID);
		sb.append(",NODE_ID=");
		sb.append(this._node_id);
		sb.append(",SENSOR_ID=");
		sb.append(this._sensor_id);
		sb.append(",DATA=");
		sb.append(this._data);
		sb.append(",TIMESTAMP=");
		sb.append(this._timestamp);
		sb.append(",NETWORK_ID=");
		sb.append(this._network_id);
		sb.append(",NODE_PLATFORM=");
		sb.append(this._node_platform);
		sb.append(",TYPE_ID=");
		sb.append(this._type_id);

		return sb.toString();
	}
}
