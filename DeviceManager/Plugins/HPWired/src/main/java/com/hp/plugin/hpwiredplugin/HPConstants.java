package com.hp.plugin.hpwiredplugin;

/**
 * @author stikhon
 * 
 */
public class HPConstants {

	public static final String TYPE_NETWORK = "SANDOVALNETWORK";
	public static final String TYPE_NODE = "SANDOVALNODE";
	public static final String TYPE_SENSOR = "SANDOVALSENSOR";

	public static final String NETWORK_IN_BROADCAST_IP = "inBroadcastIP";
	public static final String NETWORK_IN_BROADCAST_PORT = "inBroadcastPort";
	public static final String NETWORK_OUT_BROADCAST_IP = "outBroadcastIP";
	public static final String NETWORK_OUT_BROADCAST_PORT = "outBroadcastPort";
	public static final String NETWORK_BROADCAST_INTERF_IP = "broadcastInterfaceIP";

	public static final String NODE_MAC_ADDRESS = "macAddress";
	public static final String NODE_REFRESH_RATE = "refreshRate";
	public static final String NODE_TYPE = "type";
	public static final String NODE_FR_DELTA_SEND_THRES = "frontDeltaSendThreshold";
	public static final String NODE_BK_DELTA_SEND_THRES = "backDeltaSendThreshold";

	public static final String SENSOR_CHANNEL = "channel";
	public static final String SENSOR_LAST_VALUE = "lastValue";
}
