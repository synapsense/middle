package com.hp.plugin.hpwiredplugin;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Class to represent the broadcast {@literal <data>} packets.
 * <p>
 * XML data packets are unmarshaled and stored in instances of this class.
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "data", propOrder = { "pno", "bsmac", "bsloc", "bsid", "timestamp", "piad", "status" })
@XmlRootElement
public class Data {

	@XmlAttribute
	protected int pno;
	@XmlAttribute
	protected String bsmac;
	@XmlAttribute
	protected String bsloc;
	@XmlAttribute
	protected String bsid;
	@XmlAttribute
	protected String timestamp;
	@XmlElement
	protected List<Piad> piad;

	protected Status status;

	public int getPno() {
		return pno;
	}

	public String getBsmac() {
		return bsmac;
	}

	public String getBsloc() {
		return bsloc;
	}

	public String getBsid() {
		return bsid;
	}

	public String getTimeStamp() {
		return timestamp;
	}

	public List<Piad> getPiad() {
		return piad;
	}

	public Status getStatus() {
		return status;
	}

	public String toString() {
		// StringBuilder sb = new StringBuilder();

		// sb.append("Not implemented yet");
		// return sb.toString();

		return this.getXML();

	}

	/**
	 * Generates the XML for the {@literal <data>} packet object.
	 * 
	 * @return XML format {@literal <data>} packet.
	 */
	public String getXML() {
		StringBuilder sb = new StringBuilder();

		sb.append("<data pno=\"");
		sb.append(pno);
		sb.append("\" bsmac=\"");
		sb.append(bsmac);
		sb.append("\" bsloc=\"");
		sb.append(bsloc);
		sb.append("\" bsid=\"");
		sb.append(bsid);
		sb.append("\" timestamp=\"");
		sb.append(timestamp);
		sb.append("\">");

		for (Piad p : piad) {
			sb.append(p.getXML());
		}

		if (status != null)
			sb.append(status.getXML());

		sb.append("</data>");

		return sb.toString();
	}
}
