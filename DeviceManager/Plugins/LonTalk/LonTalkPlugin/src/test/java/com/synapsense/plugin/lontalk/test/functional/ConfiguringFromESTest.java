package com.synapsense.plugin.lontalk.test.functional;

import java.io.IOException;
import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.security.auth.login.LoginException;

import com.synapsense.dto.ObjectType;
import com.synapsense.dto.PropertyDescr;
import com.synapsense.dto.TO;
import com.synapsense.dto.TagDescriptor;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.EnvException;
import com.synapsense.exception.ServerConfigurationException;
import com.synapsense.service.Environment;
import com.synapsense.service.impl.dao.to.EnvObjTypeTO;
import com.synapsense.util.JNDINames;
import com.synapsense.util.ServerLoginModule;

public class ConfiguringFromESTest {

	/**
	 * @param args
	 * @throws IOException
	 * @throws NamingException
	 * @throws ServerConfigurationException
	 * @throws EnvException
	 * @throws LoginException
	 */
	public static void main(String[] args) throws NamingException, ServerConfigurationException, LoginException,
	        EnvException {

		Properties p = new Properties();

		p.put(Context.PROVIDER_URL, "jnp://localhost:1099");
		p.put(Context.INITIAL_CONTEXT_FACTORY, "org.jnp.interfaces.NamingContextFactory");
		p.put(Context.URL_PKG_PREFIXES, "org.jboss.naming:org.jnp.interfaces");

		InitialContext ctx = new InitialContext(p);

		ServerLoginModule.init(ctx);
		ServerLoginModule.login("admin", "admin");

		Environment env = ServerLoginModule.getProxy(ctx, JNDINames.ENVIRONMENT, Environment.class);

		deleteFromBase(env);

		// //network type
		ObjectType networkType = (EnvObjTypeTO) env.createObjectType("lonnetwork");
		networkType.setName("lonnetwork");
		networkType.getPropertyDescriptors().add(new PropertyDescr("name", String.class));
		networkType.getPropertyDescriptor("name").getTagDescriptors()
		        .add(new TagDescriptor("pluginId", String.class.getName()));
		networkType.getPropertyDescriptors().add(new PropertyDescr("csaddress", String.class));
		networkType.getPropertyDescriptors().add(new PropertyDescr("csport", Integer.class));
		networkType.getPropertyDescriptors().add(new PropertyDescr("pluginaddress", String.class));
		networkType.getPropertyDescriptors().add(new PropertyDescr("pluginport", Integer.class));
		networkType.getPropertyDescriptors().add(new PropertyDescr("domain", String.class));
		networkType.getPropertyDescriptors().add(new PropertyDescr("pluginsubnet", Integer.class));
		networkType.getPropertyDescriptors().add(new PropertyDescr("pluginnode", Integer.class));
		networkType.getPropertyDescriptors().add(new PropertyDescr("pluginneuronid", String.class));
		env.updateObjectType(networkType);

		// /router type
		ObjectType routerType = (EnvObjTypeTO) env.createObjectType("lonrouter");
		routerType.setName("lonrouter");

		routerType.getPropertyDescriptors().add(new PropertyDescr("name", String.class));
		routerType.getPropertyDescriptors().add(new PropertyDescr("address", String.class));
		routerType.getPropertyDescriptors().add(new PropertyDescr("port", Integer.class));
		routerType.getPropertyDescriptors().add(new PropertyDescr("retries", Integer.class));
		routerType.getPropertyDescriptors().add(new PropertyDescr("timeout", Integer.class));
		env.updateObjectType(routerType);

		// //node type
		ObjectType nodeType = (EnvObjTypeTO) env.createObjectType("lonnode");
		nodeType.setName("lonnode");

		nodeType.getPropertyDescriptors().add(new PropertyDescr("name", String.class));
		nodeType.getPropertyDescriptors().add(new PropertyDescr("subnet", Integer.class));
		nodeType.getPropertyDescriptors().add(new PropertyDescr("node", Integer.class));

		env.updateObjectType(nodeType);
		// //property type
		ObjectType propertyType = (EnvObjTypeTO) env.createObjectType("lonproperty");
		propertyType.setName("lonproperty");

		propertyType.getPropertyDescriptors().add(new PropertyDescr("name", String.class));
		propertyType.getPropertyDescriptors().add(new PropertyDescr("type", String.class));
		propertyType.getPropertyDescriptors().add(new PropertyDescr("nvindex", Integer.class));
		propertyType.getPropertyDescriptors().add(new PropertyDescr("selector", Integer.class));
		propertyType.getPropertyDescriptors().add(new PropertyDescr("fieldname", String.class));
		propertyType.getPropertyDescriptors().add(new PropertyDescr("lastValue", Double.class));
		propertyType.getPropertyDescriptor("lastValue").getTagDescriptors()
		        .add(new TagDescriptor("poll", String.class.getName()));

		env.updateObjectType(propertyType);

		// ////start configuring
		env.configurationStart();

		// network configuring
		ValueTO[] networkPropertyValues = new ValueTO[9];
		networkPropertyValues[0] = new ValueTO("name", "LonWorks");
		networkPropertyValues[1] = new ValueTO("csaddress", "192.168.102.27");
		networkPropertyValues[2] = new ValueTO("csport", 1629);
		networkPropertyValues[3] = new ValueTO("pluginaddress", "172.30.200.151");
		networkPropertyValues[4] = new ValueTO("pluginport", 1628);
		networkPropertyValues[5] = new ValueTO("domain", "76");
		networkPropertyValues[6] = new ValueTO("pluginsubnet", 1);
		networkPropertyValues[7] = new ValueTO("pluginnode", 126);
		networkPropertyValues[8] = new ValueTO("pluginneuronid", "0B0B0B0B0B0B");

		TO<?> network = env.createObject("lonnetwork", networkPropertyValues);
		env.setTagValue(network, "name", "pluginId", "lontalk");

		// /router configuring
		ValueTO[] routerPropertyValues = new ValueTO[5];
		routerPropertyValues[0] = new ValueTO("name", "i-lon");
		routerPropertyValues[1] = new ValueTO("address", "192.168.102.100");
		routerPropertyValues[2] = new ValueTO("port", 1628);
		routerPropertyValues[3] = new ValueTO("retries", 2);
		routerPropertyValues[4] = new ValueTO("timeout", 3000);

		TO<?> router = env.createObject("lonrouter", routerPropertyValues);

		// /node configuring
		ValueTO[] nodePropertyValues = new ValueTO[3];
		nodePropertyValues[0] = new ValueTO("name", "thermokon");
		nodePropertyValues[1] = new ValueTO("subnet", 1);
		nodePropertyValues[2] = new ValueTO("node", 18);

		TO<?> node = env.createObject("lonnode", nodePropertyValues);
		env.configurationComplete();

		// /temp property configuring
		ValueTO[] temppropertyPropertyValues = new ValueTO[6];
		temppropertyPropertyValues[0] = new ValueTO("name", "HVACTemp");
		temppropertyPropertyValues[1] = new ValueTO("type", "SNVT_temp_p");
		temppropertyPropertyValues[2] = new ValueTO("nvindex", 6);
		temppropertyPropertyValues[3] = new ValueTO("selector", 16377);
		temppropertyPropertyValues[4] = new ValueTO("fieldname", "temp_p");
		temppropertyPropertyValues[5] = new ValueTO("lastValue", (double) 0);

		TO<?> tempproperty = env.createObject("lonproperty", temppropertyPropertyValues);
		env.setTagValue(tempproperty, "lastValue", "poll", "3000");

		// /occup property configuring
		ValueTO[] oocuppropertyPropertyValues = new ValueTO[6];
		oocuppropertyPropertyValues[0] = new ValueTO("name", "HVACOccup");
		oocuppropertyPropertyValues[1] = new ValueTO("type", "SNVT_occupancy");
		oocuppropertyPropertyValues[2] = new ValueTO("nvindex", 5);
		oocuppropertyPropertyValues[3] = new ValueTO("selector", 16378);
		oocuppropertyPropertyValues[4] = new ValueTO("fieldname", "occupancy");
		oocuppropertyPropertyValues[5] = new ValueTO("lastValue", (double) 0);

		TO<?> occupproperty = env.createObject("lonproperty", oocuppropertyPropertyValues);
		env.setTagValue(occupproperty, "lastValue", "poll", "3000");
		// /relationship

		env.setRelation(node, occupproperty);
		env.setRelation(node, tempproperty);
		env.setRelation(router, node);
		env.setRelation(network, router);

		// /complete
		env.configurationComplete();

	}

	private static void deleteFromBase(Environment env) {
		try {
			env.deleteObjectType("lonnetwork");
		} catch (EnvException e) {
			// do nothing
		}
		try {
			env.deleteObjectType("lonrouter");
		} catch (EnvException e) {
			// do nothing
		}
		try {
			env.deleteObjectType("lonnode");
		} catch (EnvException e) {
			// do nothing
		}
		try {
			env.deleteObjectType("lonproperty");
		} catch (EnvException e) {
			// do nothing
		}
	}

}
