package com.synapsense.plugin.lontalk.test.functional;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.Serializable;

import com.synapsense.plugin.api.exceptions.ConfigPluginException;
import com.synapsense.plugin.apinew.ObjectDesc;
import com.synapsense.plugin.apinew.PluginConfig;
import com.synapsense.plugin.configuration.DmXmlConfigReader;
import com.synapsense.plugin.conversion.ConversionService;
import com.synapsense.plugin.manager.CompositeTransport;
import com.synapsense.plugin.manager.DeviceManagerCore;
import com.synapsense.plugin.poll.PollingService;

public class TestGetOperation {

	/**
	 * @param args
	 * @throws IOException
	 * @throws ConfigPluginException
	 */
	public static void main(String[] args) throws IOException, ConfigPluginException {
		DeviceManagerCore dm = DeviceManagerCore.getInstance();
		ConversionService conversionService = ConversionService.getInstance();
		dm.setConversionService(conversionService);
		PollingService pollingService = new PollingService(dm);
		dm.setPollingService(pollingService);
		pollingService.start();
		CompositeTransport transport = new CompositeTransport();
		dm.setTransport(transport);
		transport.connect();
		File src = new File(
		        "C:/Users/isavinov/git-synapsense/middle/DeviceManager/Plugins/LonTalk/LonTalkPlugin/test/com/synapsense/plugin/lontalk/test/functional/LonTalkConfigNormal.xml");
		PluginConfig config = DmXmlConfigReader.loadPluginsConfig(src).get("lontalk");
		ObjectDesc object = config.getObject("lonproperty:1");
		FileInputStream str = new FileInputStream(src);
		byte[] content = new byte[(int) src.length()];
		str.read(content);
		str.close();
		String s = new String(content);
		dm.start();
		dm.configurePlugins(s);

		Serializable temp = dm.getPropertyValue(object.getID(), "lastValue");
		System.out.println("temp: " + temp);
		System.exit(0);
	}

}
