package com.synapsense.plugin.lontalk.testplugin;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import mockit.Mocked;
import mockit.NonStrictExpectations;

import org.junit.Test;

import com.synapsense.lontalk.apdu.variable.NetworkVariable;
import com.synapsense.lontalk.api.LonTalkStack;
import com.synapsense.lontalk.api.Network;
import com.synapsense.lontalk.exception.CouldNotGetData;
import com.synapsense.lontalk.exception.CouldNotSetData;
import com.synapsense.lontalk.exception.FieldNotFoundException;
import com.synapsense.lontalk.node.CNPIPDevice;
import com.synapsense.lontalk.node.LonTalkNode;
import com.synapsense.plugin.api.exceptions.ConfigPluginException;
import com.synapsense.plugin.lontalk.LonTalkPropertyRW;

public class LonTalkPropertyRWTest {
	@Mocked
	LonTalkStack stack;
	@Mocked
	NetworkVariable snvt;
	@Mocked
	Network network;
	CNPIPDevice device;
	LonTalkNode node;

	@Test
	public void testRead() throws ConfigPluginException, IOException, CouldNotGetData, FieldNotFoundException {

		new NonStrictExpectations() {
			{
				network.getStack();
				returns(stack);
				stack.getNV(snvt, device, node);
				returns(snvt);
				snvt.getFieldByName((String) any);
				returns((double) 21.0);
			}
		};
		Double value1 = (double) 21.0;
		Double value = LonTalkPropertyRW.readProperty(network, device, node, snvt, "temp_p");
		assertEquals(value1, value);
	}

	@Test
	public void testWrite() throws ConfigPluginException, IOException, CouldNotSetData, FieldNotFoundException {

		new NonStrictExpectations() {
			{
				network.getStack();
				returns(stack);
				stack.setNV(snvt, device, node);

			}
		};
		LonTalkPropertyRW.writeProperty(network, device, node, snvt, "temp_p", 27.0);

	}
}
