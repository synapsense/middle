package com.synapsense.plugin.stateless.test;

import java.util.Properties;

import com.synapsense.plugin.apinew.StatelessPlugin;
import com.synapsense.plugin.stateless.lontalk.LonTalkStatelessPlugin;

public class StatelessPluginTest {

	/**
	 * @param args
	 * @throws InterruptedException
	 */
	public static void main(String[] args) throws InterruptedException {
		StatelessPlugin plugin = new LonTalkStatelessPlugin();

		Properties paramsForTemp = new Properties();
		paramsForTemp.put("address", "192.168.102.100");
		paramsForTemp.put("port", "1628");
		paramsForTemp.put("retries", "3");
		paramsForTemp.put("timeout", "3000");
		paramsForTemp.put("subnet", "1");
		paramsForTemp.put("node", "18");
		paramsForTemp.put("type", "SNVT_temp_p");
		paramsForTemp.put("nvindex", "6");
		paramsForTemp.put("selector", "16377");
		paramsForTemp.put("fieldname", "temp_p");

		Properties paramsForOccup = new Properties();
		paramsForOccup.put("address", "192.168.102.100");
		paramsForOccup.put("port", "1628");
		paramsForOccup.put("subnet", "1");
		paramsForOccup.put("node", "18");
		paramsForOccup.put("type", "SNVT_occupancy");
		paramsForOccup.put("nvindex", "5");
		paramsForOccup.put("selector", "16378");
		paramsForOccup.put("fieldname", "occupancy");

		Double value;
		while (true) {
			value = (Double) plugin.getPropertyValue(paramsForTemp);
			System.out.println("temp: " + value);

			value = (Double) plugin.getPropertyValue(paramsForOccup);
			System.out.println("occup: " + value);

			plugin.setPropertyValue(paramsForOccup, (double) 1);

			value = (Double) plugin.getPropertyValue(paramsForTemp);
			System.out.println("temp: " + value);

			value = (Double) plugin.getPropertyValue(paramsForOccup);
			System.out.println("occup: " + value);

			plugin.setPropertyValue(paramsForOccup, (double) 3);

		}
	}

}
