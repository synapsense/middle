package com.synapsense.plugin.lontalk.test.functional;

import static org.junit.Assert.assertEquals;

import java.io.File;

import org.junit.Test;

import com.synapsense.lontalk.api.Network;
import com.synapsense.lontalk.node.CNPIPDevice;
import com.synapsense.lontalk.node.LonTalkNode;
import com.synapsense.plugin.api.exceptions.ConfigPluginException;
import com.synapsense.plugin.apinew.PluginConfig;
import com.synapsense.plugin.configuration.DmXmlConfigReader;
import com.synapsense.plugin.lontalk.LonTalkPlugin;

public class LonTalkPluginConfigureTest {

	@Test
	public void configTest() throws ConfigPluginException {
		LonTalkPlugin plugin = new LonTalkPlugin();
		File src = new File(
		        "C:/Users/isavinov/git-synapsense/middle/DeviceManager/Plugins/LonTalk/LonTalkPlugin/test/com/synapsense/plugin/lontalk/test/functional/LonTalkConfigBig.xml");
		PluginConfig config = DmXmlConfigReader.loadPluginsConfig(src).get("lontalk");
		plugin.configure(config);
		assertEquals(2, plugin.lonnetworks.size());
		Network network = plugin.lonnetworks.get("lonnetwork:2");
		CNPIPDevice router = network.getCNPIPDevice("lonrouter:3");
		assertEquals("192.168.102.102", router.getAddress());
		LonTalkNode node = router.getNode("lonnode:4");
		assertEquals(20, node.getNode());

	}
}
