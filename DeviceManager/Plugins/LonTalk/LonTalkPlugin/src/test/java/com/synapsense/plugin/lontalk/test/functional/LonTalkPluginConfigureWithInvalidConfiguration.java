package com.synapsense.plugin.lontalk.test.functional;

import java.io.File;

import org.junit.Test;

import com.synapsense.plugin.api.exceptions.ConfigPluginException;
import com.synapsense.plugin.api.exceptions.PropertyNotFoundException;
import com.synapsense.plugin.apinew.PluginConfig;
import com.synapsense.plugin.configuration.DmXmlConfigReader;
import com.synapsense.plugin.lontalk.LonTalkPlugin;

public class LonTalkPluginConfigureWithInvalidConfiguration {

	@Test(expected = PropertyNotFoundException.class)
	public void configTest() throws ConfigPluginException {
		LonTalkPlugin plugin = new LonTalkPlugin();
		File src = new File(
		        "C:/Users/isavinov/git-synapsense/middle/DeviceManager/Plugins/LonTalk/LonTalkPlugin/test/com/synapsense/plugin/lontalk/test/functional/LonTalkConfigInvalid.xml");
		PluginConfig config = DmXmlConfigReader.loadPluginsConfig(src).get("lontalk");
		plugin.configure(config);
	}
}
