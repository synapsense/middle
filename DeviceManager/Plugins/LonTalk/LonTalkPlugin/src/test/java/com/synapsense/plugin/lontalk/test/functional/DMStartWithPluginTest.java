package com.synapsense.plugin.lontalk.test.functional;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.Serializable;

import com.synapsense.plugin.api.exceptions.ConfigPluginException;
import com.synapsense.plugin.apinew.ObjectDesc;
import com.synapsense.plugin.apinew.PluginConfig;
import com.synapsense.plugin.configuration.DmXmlConfigReader;
import com.synapsense.plugin.conversion.ConversionService;
import com.synapsense.plugin.manager.CompositeTransport;
import com.synapsense.plugin.manager.DeviceManagerCore;
import com.synapsense.plugin.poll.PollingService;

public class DMStartWithPluginTest {

	/**
	 * @throws ConfigPluginException
	 * @throws IOException
	 * @throws InterruptedException
	 */

	public static void main(String[] arqs) throws ConfigPluginException, IOException, InterruptedException {
		DeviceManagerCore dm = DeviceManagerCore.getInstance();
		ConversionService conversionService = ConversionService.getInstance();
		dm.setConversionService(conversionService);
		PollingService pollingService = new PollingService(dm);
		dm.setPollingService(pollingService);
		pollingService.start();
		CompositeTransport transport = new CompositeTransport();
		dm.setTransport(transport);
		transport.connect();
		File src = new File(
		        "C:/Users/isavinov/git-synapsense/middle/DeviceManager/Plugins/LonTalk/LonTalkPlugin/test/com/synapsense/plugin/lontalk/test/functional/LonTalkConfigNormal.xml");

		PluginConfig config = DmXmlConfigReader.loadPluginsConfig(src).get("lontalk");
		ObjectDesc object = config.getObject("lonproperty:1");
		ObjectDesc object2 = config.getObject("lonproperty:2");
		FileInputStream str = new FileInputStream(src);
		byte[] content = new byte[(int) src.length()];
		str.read(content);
		String s = new String(content);
		str.close();
		dm.start();
		dm.configurePlugins(s);
		// Thread.sleep(10000);
		while (true) {
			Serializable temp = dm.getPropertyValue(object.getID(), "lastValue");
			Serializable occup = dm.getPropertyValue(object2.getID(), "lastValue");
			System.out.println("temp: " + temp + "        occup: " + occup);
			System.out.println();
			System.out.println("trying to set occup to 3");
			dm.setPropertyValue(object2.getID(), "lastValue", (double) 3);
			temp = dm.getPropertyValue(object.getID(), "lastValue");
			occup = dm.getPropertyValue(object2.getID(), "lastValue");
			System.out.println("temp: " + temp + "        occup: " + occup);
			System.out.println();
			System.out.println("trying to set occup to 1");
			dm.setPropertyValue(object2.getID(), "lastValue", (double) 1);

		}
	}

}
