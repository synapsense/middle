package com.synapsense.plugin.lontalk.testplugin;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import mockit.Mocked;
import mockit.NonStrictExpectations;

import org.junit.Before;
import org.junit.Test;

import com.synapsense.lontalk.apdu.variable.NetworkVariable;
import com.synapsense.lontalk.api.LonTalkStack;
import com.synapsense.lontalk.api.Network;
import com.synapsense.lontalk.node.CNPIPDevice;
import com.synapsense.lontalk.node.LonTalkNode;
import com.synapsense.plugin.api.exceptions.ConfigPluginException;
import com.synapsense.plugin.api.exceptions.IncorrectStateException;
import com.synapsense.plugin.apinew.ObjectDesc;
import com.synapsense.plugin.apinew.PluginConfig;
import com.synapsense.plugin.configuration.DmXmlConfigReader;
import com.synapsense.plugin.lontalk.LonTalkPlugin;
import com.synapsense.plugin.lontalk.LonTalkPropertyRW;

public class LonTalkPluginTest {
	LonTalkPlugin plugin;
	NetworkVariable nv;
	ObjectDesc object;
	LonTalkNode node;
	CNPIPDevice router;
	File file = new File(
	        "C:/Users/isavinov/git-synapsense/middle/DeviceManager/Plugins/LonTalk/LonTalkPlugin/test/com/synapsense/plugin/lontalk/test/functional/LonTalkConfigNormal.xml");

	@Mocked
	LonTalkStack stack;
	@Mocked
	LonTalkPropertyRW rw;

	@Before
	public void beforeTest() {
		plugin = new LonTalkPlugin();

	}

	@Test
	public void configTest() throws ConfigPluginException, IncorrectStateException, IOException {
		PluginConfig config = DmXmlConfigReader.loadPluginsConfig(file).get("lontalk");
		plugin.configure(config);
		new NonStrictExpectations() {
			{
				stack.startStack();
				stack.stopStack();

			}
		};
		plugin.start();
		plugin.stop();
	}

	@Test
	public void setPropertyTest() throws ConfigPluginException, IOException, InterruptedException {

		PluginConfig config = DmXmlConfigReader.loadPluginsConfig(file).get("lontalk");
		plugin.configure(config);
		ObjectDesc snvtField = config.getObject("lonproperty:1");
		String fieledName = snvtField.getID();
		fieledName = fieledName.substring(0, fieledName.indexOf(":"));
		new NonStrictExpectations() {
			{
				LonTalkPropertyRW.writeProperty((Network) any, router, node, nv, (String) any, (Serializable) any);
			}
		};
		plugin.setPropertyValue(snvtField, "lastValue", 1);
	}

	@Test
	public void setPropertyTestWithList() throws ConfigPluginException, IOException {

		PluginConfig config = DmXmlConfigReader.loadPluginsConfig(file).get("lontalk");
		plugin.configure(config);
		ObjectDesc snvtField = config.getObject("lonproperty:1");
		List<String> propertyName = new ArrayList<String>();
		propertyName.add("scaling");
		propertyName.add("lastValue");
		List<Serializable> values = new ArrayList<Serializable>();
		values.add("1;1;1");
		values.add("1");
		new NonStrictExpectations() {
			{
				LonTalkPropertyRW.writeProperty((Network) any, router, node, nv, (String) any, (Serializable) any);
			}
		};
		plugin.setPropertyValue(snvtField, propertyName, values);

	}

	@Test
	public void getPropertyTestWithList() throws ConfigPluginException, IOException {

		PluginConfig config = DmXmlConfigReader.loadPluginsConfig(file).get("lontalk");
		plugin.configure(config);
		ObjectDesc snvtField = config.getObject("lonproperty:1");
		List<String> propertyName = new ArrayList<String>();
		propertyName.add("scaling");
		propertyName.add("lastValue");

		new NonStrictExpectations() {
			{
				LonTalkPropertyRW.readProperty((Network) any, router, node, nv, (String) any);
			}
		};
		plugin.getPropertyValue(snvtField, propertyName);
	}

	@Test
	public void getPropertyTest() throws ConfigPluginException, IOException {
		PluginConfig config = DmXmlConfigReader.loadPluginsConfig(file).get("lontalk");
		plugin.configure(config);
		ObjectDesc snvtField = config.getObject("lonproperty:1");
		new NonStrictExpectations() {
			{
				LonTalkPropertyRW.readProperty((Network) any, router, node, nv, (String) any);
			}
		};
		plugin.getPropertyValue(snvtField, "lastValue");
	}
}
