package com.synapsense.plugin.lontalk;

import java.io.Serializable;

import org.apache.log4j.Logger;

import com.synapsense.lontalk.apdu.variable.NetworkVariable;
import com.synapsense.lontalk.api.LonTalkStack;
import com.synapsense.lontalk.api.Network;
import com.synapsense.lontalk.exception.CouldNotGetData;
import com.synapsense.lontalk.exception.CouldNotSetData;
import com.synapsense.lontalk.exception.FieldNotFoundException;
import com.synapsense.lontalk.exception.IncorrectValueException;
import com.synapsense.lontalk.node.CNPIPDevice;
import com.synapsense.lontalk.node.LonTalkNode;
import com.synapsense.plugin.api.exceptions.UnableGetDataException;

public class LonTalkPropertyRW {
	public static Logger logger = Logger.getLogger(LonTalkPropertyRW.class);

	public static Double readProperty(Network network, CNPIPDevice device, LonTalkNode node, NetworkVariable snvt,
	        String fieldName) {
		LonTalkStack stack = network.getStack();
		try {
			snvt = stack.getNV(snvt, device, node);
			Double result = snvt.getFieldByName(fieldName);
			return result;
		} catch (CouldNotGetData e) {
			logger.warn("could not get data", e);
			throw new UnableGetDataException("Could not get data", e);
		} catch (FieldNotFoundException e) {
			logger.warn("field [" + fieldName + "] is not found ", e);
			throw new UnableGetDataException("Could not get data", e);
		}
	}

	public static void writeProperty(Network network, CNPIPDevice device, LonTalkNode node, NetworkVariable nv,
	        String fieldName, Serializable value) {
		LonTalkStack stack = network.getStack();
		try {
			if (nv.isRoundtrip()) {
				nv = stack.getNV(nv, device, node);
				nv.setFieldByName(fieldName, value);
				stack.setNV(nv, device, node);
			} else {
				nv.setFieldByName(fieldName, value);
				stack.setNV(nv, device, node);
			}
		} catch (FieldNotFoundException e) {
			logger.warn("field [" + fieldName + "] is not found ", e);
		} catch (CouldNotSetData e) {
			logger.warn("could not set data", e);
		} catch (IncorrectValueException e) {
			logger.warn("value is not valid", e);
		} catch (CouldNotGetData e) {
			logger.warn("could not get data");
		}

	}

}
