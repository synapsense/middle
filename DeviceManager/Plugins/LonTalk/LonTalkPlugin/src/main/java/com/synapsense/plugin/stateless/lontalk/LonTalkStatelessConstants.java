package com.synapsense.plugin.stateless.lontalk;

public interface LonTalkStatelessConstants {

	public final static String CONF_FILE = "conf" + System.getProperty("file.separator") + "lontalk.properties";

	public static final String CS_ADDRESS = "csaddress";
	public static final String CS_PORT = "csport";
	public static final String PLUGIN_ADDRESS = "pluginaddress";
	public static final String PLUGIN_PORT = "pluginport";
	public static final String DOMAIN = "domain";
	public static final String PLUGIN_SUBNET = "pluginsubnet";
	public static final String PLUGIN_NODE = "pluginnode";
	public static final String PLUGIN_NEURONID = "pluginneuronID";

	public static final String ROUTER_ADDRESS = "address";
	public static final String ROUTER_PORT = "port";
	public static final String ROUTER_RETRIES = "retries";
	public static final String ROUTER_TIMEOUT = "timeout";

	public static final String NODE_SUBNET = "subnet";
	public static final String NODE_NODE = "node";

	public static final String SNVT_NAME = "type";
	public static final String SNVT_INDEX = "nvindex";
	public static final String SNVT_SELECTOR = "selector";

	public static final String SNVT_FIELD_NAME = "fieldname";
}
