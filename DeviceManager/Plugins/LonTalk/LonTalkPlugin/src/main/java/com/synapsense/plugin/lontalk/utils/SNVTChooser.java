package com.synapsense.plugin.lontalk.utils;

import org.apache.log4j.Logger;

import com.synapsense.lontalk.apdu.variable.NetworkVariable;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_Wm2_p;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_abs_humid;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_address;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_amp;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_amp_ac;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_amp_ac_mil;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_amp_f;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_amp_mil;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_angle;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_angle_deg;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_angle_f;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_angle_vel;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_angle_vel_f;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_btu_f;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_btu_kilo;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_btu_mega;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_defr_mode;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_defr_state;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_defr_term;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_dev_c_mode;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_elec_kwh;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_elec_kwh_l;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_elec_whr;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_elec_whr_f;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_ent_opmode;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_ent_state;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_fire_indcte;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_fire_init;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_flow;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_flow_dir;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_flow_f;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_flow_mil;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_flow_p;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_freq_f;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_freq_hz;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_freq_kilohz;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_freq_milhz;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_hvac_emerg;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_hvac_mode;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_hvac_type;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_motor_state;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_occupancy;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_power;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_power_f;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_power_kilo;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_ppm;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_ppm_f;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_press;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_press_f;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_press_p;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_pwr_fact;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_pwr_fact_f;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_res;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_res_f;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_res_kilo;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_temp;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_temp_diff_p;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_temp_f;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_temp_p;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_temp_ror;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_volt;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_volt_dbmv;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_volt_f;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_volt_kilo;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_volt_mil;
import com.synapsense.lontalk.apdu.variableimpl.different.SNVT_chlr_status;
import com.synapsense.lontalk.apdu.variableimpl.different.SNVT_ctrl_req;
import com.synapsense.lontalk.apdu.variableimpl.different.SNVT_ctrl_resp;
import com.synapsense.lontalk.apdu.variableimpl.different.SNVT_file_pos;
import com.synapsense.lontalk.apdu.variableimpl.different.SNVT_hvac_overid;
import com.synapsense.lontalk.apdu.variableimpl.different.SNVT_hvac_status;
import com.synapsense.lontalk.apdu.variableimpl.different.SNVT_pump_sensor;
import com.synapsense.lontalk.apdu.variableimpl.different.SNVT_pumpset_mn;
import com.synapsense.lontalk.apdu.variableimpl.different.SNVT_pumpset_sn;
import com.synapsense.lontalk.apdu.variableimpl.different.SNVT_setting;
import com.synapsense.lontalk.apdu.variableimpl.different.SNVT_switch;
import com.synapsense.lontalk.apdu.variableimpl.different.SNVT_temp_setpt;
import com.synapsense.plugin.api.exceptions.WrongTypeException;
import com.synapsense.plugin.apinew.ObjectDesc;
import com.synapsense.plugin.lontalk.LonTalkConstants;

public class SNVTChooser {
	public static Logger logger = Logger.getLogger(SNVTChooser.class);

	public static NetworkVariable getSNVT(ObjectDesc object) throws WrongTypeException {
		String id = object.getPropertyValue(LonTalkConstants.SNVT_TYPE);
		NetworkVariable nv = getSNVT(id);
		nv.setIndex(Short.parseShort(object.getPropertyValue(LonTalkConstants.SNVT_INDEX)));
		nv.setSelector(Short.parseShort(object.getPropertyValue(LonTalkConstants.SNVT_SELECTOR)));
		return nv;

	}

	public static NetworkVariable getSNVT(String name) throws WrongTypeException {
		EnumSNVT snvt = EnumSNVT.getType(name);
		NetworkVariable nv;
		switch (snvt) {
		// ///////////////simple structure
		case SNVT_abs_humid:
			nv = new SNVT_abs_humid();
			return nv;
		case SNVT_address:
			nv = new SNVT_address();
			return nv;
		case SNVT_amp:
			nv = new SNVT_amp();
			return nv;
		case SNVT_amp_ac:
			nv = new SNVT_amp_ac();
			return nv;
		case SNVT_amp_ac_mil:
			nv = new SNVT_amp_ac_mil();
			return nv;
		case SNVT_amp_f:
			nv = new SNVT_amp_f();
			return nv;
		case SNVT_amp_mil:
			nv = new SNVT_amp_mil();
			return nv;
		case SNVT_angle:
			nv = new SNVT_angle();
			return nv;
		case SNVT_angle_deg:
			nv = new SNVT_angle_deg();
			return nv;
		case SNVT_angle_f:
			nv = new SNVT_angle_f();
			return nv;
		case SNVT_angle_vel:
			nv = new SNVT_angle_vel();
			return nv;
		case SNVT_angle_vel_f:
			nv = new SNVT_angle_vel_f();
			return nv;
		case SNVT_btu_f:
			nv = new SNVT_btu_f();
			return nv;
		case SNVT_btu_kilo:
			nv = new SNVT_btu_kilo();
			return nv;
		case SNVT_btu_mega:
			nv = new SNVT_btu_mega();
			return nv;
		case SNVT_defr_mode:
			nv = new SNVT_defr_mode();
			return nv;
		case SNVT_defr_state:
			nv = new SNVT_defr_state();
			return nv;
		case SNVT_defr_term:
			nv = new SNVT_defr_term();
			return nv;
		case SNVT_dev_c_mode:
			nv = new SNVT_dev_c_mode();
			return nv;
		case SNVT_elec_kwh:
			nv = new SNVT_elec_kwh();
			return nv;
		case SNVT_elec_kwh_l:
			nv = new SNVT_elec_kwh_l();
			return nv;
		case SNVT_elec_whr:
			nv = new SNVT_elec_whr();
			return nv;
		case SNVT_elec_whr_f:
			nv = new SNVT_elec_whr_f();
			return nv;
		case SNVT_ent_opmode:
			nv = new SNVT_ent_opmode();
			return nv;
		case SNVT_ent_state:
			nv = new SNVT_ent_state();
			return nv;
		case SNVT_fire_indcte:
			nv = new SNVT_fire_indcte();
			return nv;
		case SNVT_fire_init:
			nv = new SNVT_fire_init();
			return nv;
		case SNVT_flow:
			nv = new SNVT_flow();
			return nv;
		case SNVT_flow_dir:
			nv = new SNVT_flow_dir();
			return nv;
		case SNVT_flow_f:
			nv = new SNVT_flow_f();
			return nv;
		case SNVT_freq_f:
			nv = new SNVT_freq_f();
			return nv;
		case SNVT_flow_mil:
			nv = new SNVT_flow_mil();
			return nv;
		case SNVT_flow_p:
			nv = new SNVT_flow_p();
			return nv;
		case SNVT_freq_hz:
			nv = new SNVT_freq_hz();
			return nv;
		case SNVT_freq_kilohz:
			nv = new SNVT_freq_kilohz();
			return nv;
		case SNVT_freq_milhz:
			nv = new SNVT_freq_milhz();
			return nv;
		case SNVT_hvac_emerg:
			nv = new SNVT_hvac_emerg();
			return nv;
		case SNVT_hvac_mode:
			nv = new SNVT_hvac_mode();
			return nv;
		case SNVT_hvac_type:
			nv = new SNVT_hvac_type();
			return nv;
		case SNVT_motor_state:
			nv = new SNVT_motor_state();
			return nv;
		case SNVT_occupancy:
			nv = new SNVT_occupancy();
			return nv;
		case SNVT_power:
			nv = new SNVT_power();
			return nv;
		case SNVT_power_f:
			nv = new SNVT_power_f();
			return nv;
		case SNVT_power_kilo:
			nv = new SNVT_power_kilo();
			return nv;
		case SNVT_ppm:
			nv = new SNVT_ppm();
			return nv;
		case SNVT_ppm_f:
			nv = new SNVT_ppm_f();
			return nv;
		case SNVT_press:
			nv = new SNVT_press();
			return nv;
		case SNVT_press_f:
			nv = new SNVT_press_f();
			return nv;
		case SNVT_press_p:
			nv = new SNVT_press_p();
			return nv;
		case SNVT_pwr_fact:
			nv = new SNVT_pwr_fact();
			return nv;
		case SNVT_pwr_fact_f:
			nv = new SNVT_pwr_fact_f();
			return nv;
		case SNVT_res:
			nv = new SNVT_res();
			return nv;
		case SNVT_res_f:
			nv = new SNVT_res_f();
			return nv;
		case SNVT_res_kilo:
			nv = new SNVT_res_kilo();
			return nv;
		case SNVT_temp:
			nv = new SNVT_temp();
			return nv;
		case SNVT_temp_diff_p:
			nv = new SNVT_temp_diff_p();
			return nv;
		case SNVT_temp_f:
			nv = new SNVT_temp_f();
			return nv;
		case SNVT_temp_p:
			nv = new SNVT_temp_p();
			return nv;
		case SNVT_temp_ror:
			nv = new SNVT_temp_ror();
			return nv;
		case SNVT_therm_mode:
			nv = new SNVT_res_f();
			return nv;
		case SNVT_volt:
			nv = new SNVT_volt();
			return nv;
		case SNVT_volt_ac:
			nv = new SNVT_res_f();
			return nv;
		case SNVT_volt_dbmv:
			nv = new SNVT_volt_dbmv();
			return nv;
		case SNVT_volt_f:
			nv = new SNVT_volt_f();
			return nv;
		case SNVT_volt_kilo:
			nv = new SNVT_volt_kilo();
			return nv;
		case SNVT_volt_mil:
			nv = new SNVT_volt_mil();
			return nv;
		case SNVT_Wm2_p:
			nv = new SNVT_Wm2_p();
			return nv;
			// ////////////////////////different structure
		case SNVT_chlr_status:
			nv = new SNVT_chlr_status();
			return nv;
		case SNVT_ctrl_req:
			nv = new SNVT_ctrl_req();
			return nv;
		case SNVT_ctrl_resp:
			nv = new SNVT_ctrl_resp();
			return nv;
		case SNVT_file_pos:
			nv = new SNVT_file_pos();
			return nv;
		case SNVT_hvac_overid:
			nv = new SNVT_hvac_overid();
			return nv;
		case SNVT_hvac_status:
			nv = new SNVT_hvac_status();
			return nv;
		case SNVT_pump_sensor:
			nv = new SNVT_pump_sensor();
			return nv;
		case SNVT_pumpset_mn:
			nv = new SNVT_pumpset_mn();
			return nv;
		case SNVT_pumpset_sn:
			nv = new SNVT_pumpset_sn();
			return nv;
		case SNVT_setting:
			nv = new SNVT_setting();
			return nv;
		case SNVT_switch:
			nv = new SNVT_switch();
			return nv;
		case SNVT_temp_setpt:
			nv = new SNVT_temp_setpt();
			return nv;
		default:
			throw new WrongTypeException("Type [" + name + "] is unknown");
		}
	}

}
