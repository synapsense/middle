package com.synapsense.plugin.lontalk;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.synapsense.lontalk.apdu.variable.NetworkVariable;
import com.synapsense.lontalk.api.LonTalkStack;
import com.synapsense.lontalk.api.Network;
import com.synapsense.lontalk.exception.IncorrectFormatException;
import com.synapsense.lontalk.node.CNPIPDevice;
import com.synapsense.lontalk.node.LonTalkNode;
import com.synapsense.plugin.api.exceptions.ConfigPluginException;
import com.synapsense.plugin.api.exceptions.IncorrectStateException;
import com.synapsense.plugin.api.exceptions.UnableGetDataException;
import com.synapsense.plugin.api.exceptions.WrongTypeException;
import com.synapsense.plugin.apinew.ObjectDesc;
import com.synapsense.plugin.apinew.Plugin;
import com.synapsense.plugin.apinew.PluginConfig;
import com.synapsense.plugin.apinew.Transport;
import com.synapsense.plugin.lontalk.utils.SNVTChooser;

public class LonTalkPlugin implements Plugin {

	private final static String ID = "lontalk";
	private static Properties PLUGIN_DESC = new Properties();
	static {
		PLUGIN_DESC.put(PLUGIN_ID, ID);
		PLUGIN_DESC.put(ROOT_TYPES, LonTalkConstants.LONTALK_NETWORK);
	}
	private static final Logger logger = Logger.getLogger(LonTalkPlugin.class);
	public Map<String, Network> lonnetworks = new HashMap<String, Network>();
	private Transport transport = null;
	private boolean configured = false;
	private boolean started = false;

	@Override
	public String getID() {
		return ID;
	}

	@Override
	public Properties getPluginDescription() {
		return PLUGIN_DESC;
	}

	@Override
	public void configure(PluginConfig config) throws ConfigPluginException {
		if (config == null) {
			throw new IllegalArgumentException("config cannot be null");
		}
		stop();
		for (ObjectDesc networkobject : config.getRoots()) {
			String csipaddress = networkobject.getPropertyValue(LonTalkConstants.CS_ADDRESS);
			int csport = Integer.parseInt(networkobject.getPropertyValue(LonTalkConstants.CS_PORT));
			String pluginaddress = networkobject.getPropertyValue(LonTalkConstants.PLUGIN_ADDRESS);
			int pluginport = Integer.parseInt(networkobject.getPropertyValue(LonTalkConstants.PLUGIN_PORT));
			String domain = networkobject.getPropertyValue(LonTalkConstants.DOMAIN);
			int pluginsubnet = Integer.parseInt(networkobject.getPropertyValue(LonTalkConstants.PLUGIN_SUBNET));
			int pluginnode = Integer.parseInt(networkobject.getPropertyValue(LonTalkConstants.PLUGIN_NODE));
			String neuronID = networkobject.getPropertyValue(LonTalkConstants.PLUGIN_NEURONID);

			Network network;
			try {
				network = new Network(csipaddress, csport, pluginaddress, pluginport, domain, pluginsubnet, pluginnode,
				        neuronID);
			} catch (IncorrectFormatException e) {
				throw new IllegalArgumentException("Object [" + networkobject.getID() + "] has incorrect format", e);
			}

			for (ObjectDesc routerobject : networkobject.getChildren()) {
				String routeraddress = routerobject.getPropertyValue(LonTalkConstants.ROUTER_ADDRESS);
				int routerport = Integer.parseInt(routerobject.getPropertyValue(LonTalkConstants.ROUTER_PORT));
				int routerretries = Integer.parseInt(routerobject.getPropertyValue(LonTalkConstants.ROUTER_RETRIES));
				int timeout = Integer.parseInt(routerobject.getPropertyValue(LonTalkConstants.ROUTER_TIMEOUT));

				CNPIPDevice device = new CNPIPDevice(routeraddress, routerport, timeout, routerretries);

				for (ObjectDesc node : routerobject.getChildren()) {

					int nodesubnet = Integer.parseInt(node.getPropertyValue(LonTalkConstants.NODE_SUBNET));
					int nodenode = Integer.parseInt(node.getPropertyValue(LonTalkConstants.NODE_NODE));
					LonTalkNode no = new LonTalkNode(nodesubnet, nodenode);

					device.addNode(node.getID(), no);
				}
				network.addCNPIPDevice(routerobject.getID(), device);
			}
			lonnetworks.put(networkobject.getID(), network);
		}
		configured = true;
		logger.debug("Plugin " + ID + " is configured.");
	}

	@Override
	public void start() throws IncorrectStateException {
		if (!configured) {
			throw new IncorrectStateException("Plugin " + ID + " has to be configured before start.");
		}
		if (started) {
			throw new IncorrectStateException("Plugin " + ID + "has already started.");
		}
		for (Network network : lonnetworks.values()) {
			LonTalkStack stack = network.getStack();
			stack.startStack();
		}
		started = true;
		logger.debug("Plugin " + ID + " is started.");
	}

	@Override
	public void stop() {
		for (Network network : lonnetworks.values()) {
			LonTalkStack stack = network.getStack();
			stack.stopStack();
		}
		logger.debug("Plugin is stopped");
		lonnetworks.clear();
		started = false;
	}

	@Override
	public void setPropertyValue(ObjectDesc object, String propertyName, Serializable value) {
		if (object == null) {
			throw new IllegalArgumentException("object couldnot be null");
		}

		ObjectDesc node = object.getParent();
		ObjectDesc router = node.getParent();
		ObjectDesc networkobject = router.getParent();
		Network network = lonnetworks.get(networkobject.getID());
		CNPIPDevice device = network.getCNPIPDevice(router.getID());
		LonTalkNode lonnode = device.getNode(node.getID());

		NetworkVariable nv;
		String fieldName = null;
		try {
			nv = SNVTChooser.getSNVT(object);
			fieldName = object.getPropertyValue(LonTalkConstants.SNVT_FIELD_NAME);

			LonTalkPropertyRW.writeProperty(network, device, lonnode, nv, fieldName, value);
		} catch (WrongTypeException e) {
			logger.warn("SNVT [" + object.getPropertyValue(LonTalkConstants.SNVT_TYPE) + "] is not supported", e);
		}

	}

	@Override
	public void setPropertyValue(ObjectDesc object, List<String> propertyName, List<Serializable> values) {
		int i = 0;
		Iterator<String> itprop = propertyName.iterator();
		Iterator<Serializable> itval = values.iterator();
		while (itprop.hasNext()) {
			String prop = itprop.next();
			if (prop.equals("lastValue")) {
				i++;
				Serializable val = itval.next();
				setPropertyValue(object, prop, val);
				break;
			}
			itval.next();
		}
		if (i == 0) {
			logger.warn("could not set data");
		}
	}

	@Override
	public Serializable getPropertyValue(ObjectDesc object, String propertyName) {
		if (object == null) {
			throw new IllegalArgumentException("object could not be null");
		}
		ObjectDesc node = object.getParent();
		ObjectDesc router = node.getParent();
		ObjectDesc networkobject = router.getParent();
		Network network = lonnetworks.get(networkobject.getID());
		CNPIPDevice device = network.getCNPIPDevice(router.getID());
		LonTalkNode lonnode = device.getNode(node.getID());

		NetworkVariable nv;
		String fieldName = null;
		try {
			nv = SNVTChooser.getSNVT(object);
			fieldName = object.getPropertyValue(LonTalkConstants.SNVT_FIELD_NAME);
			Double result = null;
			result = LonTalkPropertyRW.readProperty(network, device, lonnode, nv, fieldName);
			return result;
		} catch (WrongTypeException e) {
			logger.warn("SNVT [" + object.getPropertyValue(LonTalkConstants.SNVT_TYPE) + "] is not supported", e);
			throw new UnableGetDataException("Could not get data", e);
		} catch (UnableGetDataException e) {
			throw e;
		}

	}

	@Override
	public List<Serializable> getPropertyValue(ObjectDesc object, List<String> propertyNames) {
		List<Serializable> values = new ArrayList<Serializable>();
		if (object == null) {
			throw new IllegalArgumentException("object couldnot be null");
		}
		ObjectDesc node = object.getParent();
		ObjectDesc router = node.getParent();
		ObjectDesc networkobject = router.getParent();
		Network network = lonnetworks.get(networkobject.getID());
		CNPIPDevice device = network.getCNPIPDevice(router.getID());
		LonTalkNode lonnode = device.getNode(node.getID());

		NetworkVariable nv;
		String fieldName = null;
		try {
			nv = SNVTChooser.getSNVT(object);
			fieldName = object.getPropertyValue(LonTalkConstants.SNVT_FIELD_NAME);
			values.add(LonTalkPropertyRW.readProperty(network, device, lonnode, nv, fieldName));
			return values;
		} catch (WrongTypeException e) {
			logger.warn("SNVT [" + object.getPropertyValue(LonTalkConstants.SNVT_TYPE) + "] is not supported", e);
			throw new UnableGetDataException("Could not get data", e);
		} catch (UnableGetDataException e) {
			throw e;
		}

	}

	@Override
	public void setTransport(Transport transport) {
		this.transport = transport;
	}

	public Transport getTransport() {
		return transport;
	}

}
