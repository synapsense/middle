package com.synapsense.plugin.lontalk;

public interface LonTalkConstants {
	public static final String LONTALK_NETWORK = "lonnetwork";
	public static final String LONTALK_ROUTER = "lonrouter";
	public static final String LONTALK_NODE = "lonnode";
	public static final String LONTALK_SNVT = "lonproperty";

	public static final String CS_ADDRESS = "csaddress";
	public static final String CS_PORT = "csport";
	public static final String PLUGIN_ADDRESS = "pluginaddress";
	public static final String PLUGIN_PORT = "pluginport";
	public static final String DOMAIN = "domain";
	public static final String PLUGIN_SUBNET = "pluginsubnet";
	public static final String PLUGIN_NODE = "pluginnode";
	public static final String PLUGIN_NEURONID = "pluginneuronid";

	public static final String ROUTER_NAME = "name";
	public static final String ROUTER_ADDRESS = "address";
	public static final String ROUTER_PORT = "port";
	public static final String ROUTER_RETRIES = "retries";
	public static final String ROUTER_TIMEOUT = "timeout";

	public static final String NODE_SUBNET = "subnet";
	public static final String NODE_NODE = "node";

	public static final String SNVT_TYPE = "type";
	public static final String SNVT_INDEX = "nvindex";
	public static final String SNVT_SELECTOR = "selector";
	public static final String SNVT_FIELD_NAME = "fieldname";
	public static final String SNVT_FIELD_LAST_VALUE = "lastValue";

}
