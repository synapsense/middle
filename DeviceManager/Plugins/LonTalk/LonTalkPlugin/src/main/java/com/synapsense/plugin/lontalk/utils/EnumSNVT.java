package com.synapsense.plugin.lontalk.utils;

import com.synapsense.plugin.api.exceptions.WrongTypeException;

public enum EnumSNVT {

SNVT_abs_humid("SNVT_abs_humid"), SNVT_address("SNVT_address"), SNVT_amp("SNVT_amp"), SNVT_amp_ac("SNVT_amp_ac"), SNVT_amp_ac_mil(
        "SNVT_amp_ac_mil"), SNVT_amp_f("SNVT_amp_f"), SNVT_amp_mil("SNVT_amp_mil"), SNVT_angle("SNVT_angle"), SNVT_angle_deg(
        "SNVT_angle_deg"), SNVT_angle_f("SNVT_angle_f"), SNVT_angle_vel("SNVT_angle_vel"), SNVT_angle_vel_f(
        "SNVT_angle_vel_f"), SNVT_btu_f("SNVT_btu_f"), SNVT_btu_kilo("SNVT_btu_kilo"), SNVT_btu_mega("SNVT_btu_mega"), SNVT_defr_mode(
        "SNVT_defr_mode"), SNVT_defr_state("SNVT_defr_state"), SNVT_defr_term("SNVT_defr_term"), SNVT_dev_c_mode(
        "SNVT_dev_c_mode"), SNVT_elec_kwh("SNVT_elec_kwh"), SNVT_elec_kwh_l("SNVT_elec_kwh_l"), SNVT_elec_whr(
        "SNVT_elec_whr"), SNVT_elec_whr_f("SNVT_elec_whr_f"), SNVT_ent_opmode("SNVT_ent_opmode"), SNVT_ent_state(
        "SNVT_ent_state"), SNVT_fire_indcte("SNVT_fire_indcte"), SNVT_fire_init("SNVT_fire_init"), SNVT_flow(
        "SNVT_flow"), SNVT_flow_dir("SNVT_flow_dir"), SNVT_flow_f("SNVT_flow_f"), SNVT_flow_mil("SNVT_flow_mil"), SNVT_flow_p(
        "SNVT_flow_p"), SNVT_freq_f("SNVT_freq_f"), SNVT_freq_hz("SNVT_freq_hz"), SNVT_freq_kilohz("SNVT_freq_kilohz"), SNVT_freq_milhz(
        "SNVT_freq_milhz"), SNVT_hvac_emerg("SNVT_hvac_emerg"), SNVT_hvac_mode("SNVT_hvac_mode"), SNVT_hvac_type(
        "SNVT_hvac_type"), SNVT_motor_state("SNVT_motor_state"), SNVT_occupancy("SNVT_occupancy"), SNVT_power(
        "SNVT_power"), SNVT_power_f("SNVT_power_f"), SNVT_power_kilo("SNVT_power_kilo"), SNVT_ppm("SNVT_ppm"), SNVT_ppm_f(
        "SNVT_ppm_f"), SNVT_press("SNVT_press"), SNVT_press_f("SNVT_press_f"), SNVT_press_p("SNVT_press_p"), SNVT_pwr_fact(
        "SNVT_pwr_fact"), SNVT_pwr_fact_f("SNVT_pwr_fact_f"), SNVT_res("SNVT_res"), SNVT_res_f("SNVT_res_f"), SNVT_res_kilo(
        "SNVT_res_kilo"), SNVT_temp("SNVT_temp"), SNVT_temp_diff_p("SNVT_temp_diff_p"), SNVT_temp_f("SNVT_temp_f"), SNVT_temp_p(
        "SNVT_temp_p"), SNVT_temp_ror("SNVT_temp_ror"), SNVT_therm_mode("SNVT_therm_mode"), SNVT_volt("SNVT_volt"), SNVT_volt_ac(
        "SNVT_volt_ac"), SNVT_volt_dbmv("SNVT_volt_dbmv"), SNVT_volt_f("SNVT_volt_f"), SNVT_volt_kilo("SNVT_volt_kilo"), SNVT_volt_mil(
        "SNVT_volt_mil"), SNVT_Wm2_p("SNVT_Wm2_p"),
// ////////////////////////////////
SNVT_chlr_status("SNVT_chlr_status"), SNVT_ctrl_req("SNVT_ctrl_req"), SNVT_ctrl_resp("SNVT_ctrl_resp"), SNVT_file_pos(
        "SNVT_file_pos"), SNVT_hvac_overid("SNVT_hvac_overid"), SNVT_hvac_status("SNVT_hvac_status"), SNVT_pump_sensor(
        "SNVT_pump_sensor"), SNVT_pumpset_mn("SNVT_pumpset_mn"), SNVT_pumpset_sn("SNVT_pumpset_sn"), SNVT_setting(
        "SNVT_setting"), SNVT_switch("SNVT_switch"), SNVT_temp_setpt("SNVT_temp_setpt");

private String type;

private EnumSNVT(String type) {
	this.type = type;
}

static public EnumSNVT getType(String pType) throws WrongTypeException {
	for (EnumSNVT type : EnumSNVT.values()) {
		if (type.getTypeValue().equals(pType)) {
			return type;
		}
	}
	throw new WrongTypeException("unknown type");
}

private String getTypeValue() {
	return type;
}
}
