package com.synapsense.plugin.stateless.lontalk;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.synapsense.lontalk.apdu.variable.NetworkVariable;
import com.synapsense.lontalk.api.LonTalkStack;
import com.synapsense.lontalk.api.Network;
import com.synapsense.lontalk.exception.CouldNotGetData;
import com.synapsense.lontalk.exception.CouldNotSetData;
import com.synapsense.lontalk.exception.FieldNotFoundException;
import com.synapsense.lontalk.exception.IncorrectFormatException;
import com.synapsense.lontalk.exception.IncorrectValueException;
import com.synapsense.lontalk.node.CNPIPDevice;
import com.synapsense.lontalk.node.LonTalkNode;
import com.synapsense.plugin.api.exceptions.WrongTypeException;
import com.synapsense.plugin.apinew.StatelessPlugin;
import com.synapsense.plugin.lontalk.utils.SNVTChooser;

public class LonTalkStatelessPlugin implements StatelessPlugin {
	private static final String ID = "lontalk";
	private static final Logger logger = Logger.getLogger(LonTalkStatelessPlugin.class);
	private Properties prop;
	private LonTalkStack stack;

	public LonTalkStatelessPlugin() {
		prop = new Properties();
		logger.debug("Trying to load config file from the default location: [" + LonTalkStatelessConstants.CONF_FILE
		        + "]");
		File configFile = new File(LonTalkStatelessConstants.CONF_FILE);
		if (configFile.exists()) {
			try {
				FileInputStream inputstream = new FileInputStream(configFile);
				prop.load(inputstream);
				inputstream.close();
				String csaddress = prop.getProperty(LonTalkStatelessConstants.CS_ADDRESS);
				Integer csport = Integer.parseInt(prop.getProperty(LonTalkStatelessConstants.CS_PORT));
				String pluginaddress = prop.getProperty(LonTalkStatelessConstants.PLUGIN_ADDRESS);
				Integer pluginport = Integer.parseInt(prop.getProperty(LonTalkStatelessConstants.PLUGIN_PORT));
				String domain = prop.getProperty(LonTalkStatelessConstants.DOMAIN);
				Integer pluginsubnet = Integer.parseInt(prop.getProperty(LonTalkStatelessConstants.PLUGIN_SUBNET));
				Integer pluginnode = Integer.parseInt(prop.getProperty(LonTalkStatelessConstants.PLUGIN_NODE));
				String pluginneuronID = prop.getProperty(LonTalkStatelessConstants.PLUGIN_NEURONID);
				Network network = new Network(csaddress, csport, pluginaddress, pluginport, domain, pluginsubnet,
				        pluginnode, pluginneuronID);
				stack = network.getStack();
				stack.startStack();
			} catch (IOException e) {
				logger.warn("Unable to load configuration properties files from default location: ["
				        + LonTalkStatelessConstants.CONF_FILE + "]", e);
			} catch (IncorrectFormatException e) {
				logger.warn("Properties file [" + LonTalkStatelessConstants.CONF_FILE
				        + "] contains invalid configuration", e);
			} catch (NumberFormatException e) {
				logger.warn("property file contains incorrect value", e);
			}
		} else {
			logger.debug("property file does not found");
		}
	}

	@Override
	public String getID() {
		return ID;
	}

	@Override
	public void setPropertyValue(Properties params, Serializable newValue) {
		if (checkParams(params)) {
			CNPIPDevice router = getRouterFromProperties(params);
			LonTalkNode node = getNodeFromProperties(params);
			try {
				NetworkVariable nv = getNVFromProperties(params);
				String fieldname = params.getProperty(LonTalkStatelessConstants.SNVT_FIELD_NAME);
				if (nv.isRoundtrip()) {
					nv = stack.getNV(nv, router, node);
					nv.setFieldByName(fieldname, newValue);
					stack.setNV(nv, router, node);
				} else {
					nv.setFieldByName(fieldname, newValue);
					stack.setNV(nv, router, node);
				}
			} catch (WrongTypeException e) {
				logger.warn("Network variable does'n supported in this version", e);
			} catch (CouldNotGetData e) {
				logger.warn("could not set data", e);
			} catch (FieldNotFoundException e) {
				logger.warn("could not set data", e);
			} catch (IncorrectValueException e) {
				logger.warn("could not set data", e);
			} catch (CouldNotSetData e) {
				logger.warn("could not set data", e);
			}

		}
	}

	@Override
	public Serializable getPropertyValue(Properties params) {
		if (checkParams(params)) {
			CNPIPDevice router = getRouterFromProperties(params);
			LonTalkNode node = getNodeFromProperties(params);
			try {
				NetworkVariable nv = getNVFromProperties(params);
				String fieldname = params.getProperty(LonTalkStatelessConstants.SNVT_FIELD_NAME);
				nv = stack.getNV(nv, router, node);
				Double value = nv.getFieldByName(fieldname);
				return value;
			} catch (WrongTypeException e) {
				logger.warn("Network variable does'n supported in this version", e);
			} catch (CouldNotGetData e) {
				logger.warn("could not get data", e);
			} catch (FieldNotFoundException e) {
				logger.warn("could not get data", e);
			}
		}
		return null;
	}

	private CNPIPDevice getRouterFromProperties(Properties params) {
		String routeraddress = params.getProperty(LonTalkStatelessConstants.ROUTER_ADDRESS);
		Integer routerport = Integer.parseInt(params.getProperty(LonTalkStatelessConstants.ROUTER_PORT));
		Integer routerretries = Integer.parseInt(params.getProperty(LonTalkStatelessConstants.ROUTER_RETRIES, "1"));
		Integer routertimeout = Integer.parseInt(params.getProperty(LonTalkStatelessConstants.ROUTER_TIMEOUT, "3000"));
		return new CNPIPDevice(routeraddress, routerport, routertimeout, routerretries);
	}

	private LonTalkNode getNodeFromProperties(Properties params) {
		Integer nodesubnet = Integer.parseInt(params.getProperty(LonTalkStatelessConstants.NODE_SUBNET));
		Integer nodenode = Integer.parseInt(params.getProperty(LonTalkStatelessConstants.NODE_NODE));
		return new LonTalkNode(nodesubnet, nodenode);
	}

	private NetworkVariable getNVFromProperties(Properties params) throws WrongTypeException {
		String nvname = params.getProperty(LonTalkStatelessConstants.SNVT_NAME);
		Byte nvindex = Byte.parseByte(params.getProperty(LonTalkStatelessConstants.SNVT_INDEX));
		Short nvselector = Short.parseShort(params.getProperty(LonTalkStatelessConstants.SNVT_SELECTOR));
		NetworkVariable snvt = SNVTChooser.getSNVT(nvname);
		snvt.setIndex(nvindex);
		snvt.setSelector(nvselector);
		return snvt;
	}

	private boolean checkParams(Properties params) {
		if (!(params.containsKey(LonTalkStatelessConstants.ROUTER_ADDRESS))) {
			throw new IllegalArgumentException("Missing " + LonTalkStatelessConstants.ROUTER_ADDRESS + " parameter");
		}
		if (!params.containsKey(LonTalkStatelessConstants.ROUTER_PORT)) {
			throw new IllegalArgumentException("Missing " + LonTalkStatelessConstants.ROUTER_PORT + " parameter");
		}
		if (!params.containsKey(LonTalkStatelessConstants.NODE_SUBNET)) {
			throw new IllegalArgumentException("Missing " + LonTalkStatelessConstants.NODE_SUBNET + "parameter");
		}
		if (!params.containsKey(LonTalkStatelessConstants.NODE_NODE)) {
			throw new IllegalArgumentException("Missing " + LonTalkStatelessConstants.NODE_NODE + " parameter");
		}
		if (!params.containsKey(LonTalkStatelessConstants.SNVT_NAME)) {
			throw new IllegalArgumentException("Missing " + LonTalkStatelessConstants.SNVT_NAME + " parameter");
		}
		if (!params.containsKey(LonTalkStatelessConstants.SNVT_INDEX)) {
			throw new IllegalArgumentException("Missing " + LonTalkStatelessConstants.SNVT_INDEX + " parameter");
		}
		if (!params.containsKey(LonTalkStatelessConstants.SNVT_SELECTOR)) {
			throw new IllegalArgumentException("Missing " + LonTalkStatelessConstants.SNVT_SELECTOR + " parameter");
		}
		if (!params.containsKey(LonTalkStatelessConstants.SNVT_FIELD_NAME)) {
			throw new IllegalArgumentException("Missing " + LonTalkStatelessConstants.SNVT_FIELD_NAME + " parameter");
		}
		return true;
	}

}
