package com.synapsense.lontalk.network;

public interface TypeConstant {

	byte SPDU = 0x10;
	byte TPDU = 0x00;
	byte ACK = 0x20;
	byte RESPONSE = 0x20;
	byte APPLICATION_APDY = 0x33;
}
