package com.synapsense.lontalk.packet;

import com.synapsense.lontalk.packet.header.CNPIPHeader;

public class Acknowledged {

	private int dateTime = 0xd2a2d25d;
	private byte ackType = 0x00;
	private short requestId = 0;
	private byte segmentId = 0;

	public byte[] getPacket() {
		CNPIPHeader header = new CNPIPHeader();
		header.setAcknowledge();
		header.setDataPacketLenght((short) 28);
		byte[] headerData = header.getHeader();
		byte[] packet = new byte[28];
		for (int i = 0; i < headerData.length; i++) {
			packet[i] = headerData[i];
		}
		packet[20] = packet[20] |= dateTime >>> 24;
		packet[21] = packet[21] |= dateTime >>> 16;
		packet[22] = packet[22] |= dateTime >>> 8;
		packet[23] = packet[23] |= dateTime;
		packet[24] = ackType;
		packet[25] = packet[25] |= requestId >> 8;
		packet[26] = packet[26] |= requestId;
		packet[27] = segmentId;
		return packet;
	}
}
