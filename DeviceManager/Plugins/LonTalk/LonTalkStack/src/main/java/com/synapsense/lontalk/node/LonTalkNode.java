package com.synapsense.lontalk.node;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import com.synapsense.lontalk.apdu.variable.NetworkVariable;

public class LonTalkNode {

	private byte domain;
	private byte subnet;
	private byte node;
	private long neuronId;

	private HashMap<String, NetworkVariable> netvar = new HashMap<String, NetworkVariable>();

	public LonTalkNode(int subnet, int node) {
		this.subnet = 0;
		this.node = 0;
		this.subnet = this.subnet |= subnet;
		this.node = this.node |= node;
	}

	public LonTalkNode(int subnet, int node, long neuronId) {
		this.subnet = 0;
		this.node = 0;
		this.subnet = this.subnet |= subnet;
		this.node = this.node |= node;
		this.neuronId = neuronId;
	}

	public void addSNVT(String key, NetworkVariable variable) {
		netvar.put(key, variable);
	}

	public NetworkVariable getSNVT(String key) {
		return netvar.get(key);
	}

	public void addSNVT(List<String> keyList, List<NetworkVariable> nvlist) {
		Iterator<String> itkey = keyList.iterator();
		Iterator<NetworkVariable> itnv = nvlist.iterator();
		while (itkey.hasNext()) {
			addSNVT(itkey.next(), itnv.next());
		}
	}

	public List<NetworkVariable> getSNVT(List<String> keyList) {
		List<NetworkVariable> nvList = new ArrayList<NetworkVariable>();
		Iterator<String> it = keyList.iterator();
		while (it.hasNext()) {
			nvList.add(getSNVT(it.next()));
		}
		return nvList;
	}

	public byte getSubnet() {
		return subnet;
	}

	public void setSubnet(byte subnet) {
		this.subnet = subnet;
	}

	public byte getNode() {
		return node;
	}

	public void setNode(byte node) {
		this.node = node;
	}

	public long getNeuronId() {
		return neuronId;
	}

	public void setNeuronId(long neuronId) {
		this.neuronId = neuronId;
	}

	public void setDomain(byte domain) {
		this.domain = domain;
	}

	public byte getDomain() {
		return domain;
	}

}
