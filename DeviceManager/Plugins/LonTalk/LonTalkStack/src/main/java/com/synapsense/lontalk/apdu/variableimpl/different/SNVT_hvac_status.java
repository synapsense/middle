package com.synapsense.lontalk.apdu.variableimpl.different;

import java.io.Serializable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import com.synapsense.lontalk.apdu.variable.NetworkVariable;
import com.synapsense.lontalk.exception.FieldNotFoundException;
import com.synapsense.lontalk.exception.IncorrectValueException;

public class SNVT_hvac_status implements NetworkVariable {

	private short selector;
	private short index;

	private byte inputmode = 0;
	private double outputmode = 0;

	private short heat_output_primary = 0;
	private double outputprimary = 0;
	private byte[] scalingprimary = { 5, -3, 0 };

	private short heat_output_secondary = 0;
	private double outputsecondary = 0;
	private byte[] scalingsecondary = { 5, -3, 0 };

	private short cool_output = 0;
	private double outputcool = 0;
	private byte[] scalingcool = { 5, -3, 0 };

	private short econ_output = 0;
	private double outputecon = 0;
	private byte[] scalingecon = { 5, -3, 0 };

	private short fan_output = 0;
	private double outputfan = 0;
	private byte[] scalingfan = { 5, -3, 0 };

	private short in_alarm = 0;
	private double outputalarm = 0;
	private byte[] scalingalarm = { 1, 0, 0 };

	private static final boolean ROUNDTRIP = true;

	@Override
	public void setSelector(short selector) {
		this.selector = selector;
	}

	@Override
	public short getSelector() {
		return selector;
	}

	@Override
	public byte[] getData() {
		byte[] a = new byte[12];
		a[0] = inputmode;
		a[1] = (byte) ((heat_output_primary >> 8) & 0xff);
		a[2] = (byte) (heat_output_primary & 0xff);
		a[3] = (byte) ((heat_output_secondary >> 8) & 0xff);
		a[4] = (byte) (heat_output_secondary & 0xff);
		a[5] = (byte) ((cool_output >> 8) & 0xff);
		a[6] = (byte) (cool_output & 0xff);
		a[7] = (byte) ((econ_output >> 8) & 0xff);
		a[8] = (byte) (econ_output & 0xff);
		a[9] = (byte) ((fan_output >> 8) & 0xff);
		a[10] = (byte) (fan_output & 0xff);
		a[11] = a[11] |= in_alarm;
		return a;
	}

	@Override
	public void setData(byte[] newdata) {
		ByteBuffer bb = ByteBuffer.allocate(2);
		bb.order(ByteOrder.LITTLE_ENDIAN);

		inputmode = newdata[0];
		outputmode = inputmode;

		bb.put(newdata[2]);
		bb.put(newdata[1]);
		heat_output_primary = bb.getShort(0);
		outputprimary = scalingprimary[0] * Math.pow(10, scalingprimary[1]) * (heat_output_primary + scalingprimary[2]);

		bb.clear();
		bb.put(newdata[4]);
		bb.put(newdata[3]);
		heat_output_secondary = bb.getShort(0);
		outputsecondary = scalingsecondary[0] * Math.pow(10, scalingsecondary[1])
		        * (heat_output_secondary + scalingsecondary[2]);

		bb.clear();
		bb.put(newdata[6]);
		bb.put(newdata[5]);
		cool_output = bb.getShort(0);
		outputcool = scalingcool[0] * Math.pow(10, scalingcool[1]) * (cool_output + scalingcool[2]);

		bb.clear();
		bb.put(newdata[8]);
		bb.put(newdata[7]);
		econ_output = bb.getShort(0);
		outputecon = scalingecon[0] * Math.pow(10, scalingecon[1]) * (econ_output + scalingecon[2]);

		bb.clear();
		bb.put(newdata[10]);
		bb.put(newdata[9]);
		fan_output = bb.getShort(0);
		outputfan = scalingfan[0] * Math.pow(10, scalingfan[1]) * (fan_output + scalingfan[2]);

		bb.clear();
		bb.put(newdata[11]);
		bb.put((byte) 0);
		in_alarm = bb.getShort(0);
		outputalarm = scalingalarm[0] * Math.pow(10, scalingalarm[1]) * (in_alarm + scalingalarm[2]);

	}

	@Override
	public void setFieldByName(String name, Serializable value) throws FieldNotFoundException, IncorrectValueException {
		int i = 0;
		if (name.equalsIgnoreCase("mode")) {
			Double a = (Double) value;
			if (a < -1 || a > 20) {
				throw new IncorrectValueException("Value is not valid");
			}
			outputmode = (Double) value;
			inputmode = (byte) outputmode;
			i++;
		}
		if (name.equalsIgnoreCase("heat_output_primary")) {
			Double a = (Double) value;
			if (a < -163.840 || a >= 163.835) {
				throw new IncorrectValueException("Value is not valid");
			}
			heat_output_primary = (short) ((a / (scalingprimary[0] * Math.pow(10, scalingprimary[1]))) - scalingprimary[2]);
			outputprimary = scalingprimary[0] * Math.pow(10, scalingprimary[1])
			        * (heat_output_primary + scalingprimary[2]);
			i++;
		}
		if (name.equalsIgnoreCase("heat_output_secondary")) {
			Double a = (Double) value;
			if (a < -163.840 || a >= 163.835) {
				throw new IncorrectValueException("Value is not valid");
			}
			heat_output_secondary = (short) ((a / (scalingsecondary[0] * Math.pow(10, scalingsecondary[1]))) - scalingsecondary[2]);
			outputsecondary = scalingsecondary[0] * Math.pow(10, scalingsecondary[1])
			        * (heat_output_secondary + scalingsecondary[2]);
			i++;
		}
		if (name.equalsIgnoreCase("cool_output")) {
			Double a = (Double) value;
			if (a < -163.840 || a >= 163.835) {
				throw new IncorrectValueException("Value is not valid");
			}
			cool_output = (short) ((a / (scalingcool[0] * Math.pow(10, scalingcool[1]))) - scalingcool[2]);
			outputcool = scalingcool[0] * Math.pow(10, scalingcool[1]) * (cool_output + scalingcool[2]);
			i++;
		}
		if (name.equalsIgnoreCase("econ_output")) {
			Double a = (Double) value;
			if (a < -163.840 || a >= 163.835) {
				throw new IncorrectValueException("Value is not valid");
			}
			econ_output = (short) ((a / (scalingecon[0] * Math.pow(10, scalingecon[1]))) - scalingecon[2]);
			outputecon = scalingecon[0] * Math.pow(10, scalingecon[1]) * (econ_output + scalingecon[2]);
			i++;
		}
		if (name.equalsIgnoreCase("fan_output")) {
			Double a = (Double) value;
			if (a < -163.840 || a >= 163.835) {
				throw new IncorrectValueException("Value is not valid");
			}
			fan_output = (short) ((a / (scalingfan[0] * Math.pow(10, scalingfan[1]))) - scalingfan[2]);
			outputfan = scalingfan[0] * Math.pow(10, scalingfan[1]) * (fan_output + scalingfan[2]);
			i++;
		}
		if (name.equalsIgnoreCase("in_alarm")) {
			Double a = (Double) value;
			if (a < 0 || a > 255) {
				throw new IncorrectValueException("Value is not valid");
			}
			in_alarm = (short) ((a / (scalingalarm[0] * Math.pow(10, scalingalarm[1]))) - scalingalarm[2]);
			outputalarm = scalingalarm[0] * Math.pow(10, scalingalarm[1]) * (in_alarm + scalingalarm[2]);
			i++;
		}
		if (i == 0) {
			throw new FieldNotFoundException("Field is not found");
		}

	}

	@Override
	public Double getFieldByName(String name) throws FieldNotFoundException {
		if (name.equalsIgnoreCase("mode")) {
			return outputmode;
		}
		if (name.equalsIgnoreCase("heat_output_primary")) {
			return outputprimary;
		}
		if (name.equalsIgnoreCase("heat_output_secondary")) {
			return outputsecondary;
		}
		if (name.equalsIgnoreCase("cool_output")) {
			return outputcool;
		}
		if (name.equalsIgnoreCase("econ_output")) {
			return outputecon;
		}
		if (name.equalsIgnoreCase("fan_output")) {
			return outputfan;
		}
		if (name.equalsIgnoreCase("in_alarm")) {
			return outputalarm;
		}
		throw new FieldNotFoundException("Field is not found");
	}

	@Override
	public short getIndex() {
		return index;
	}

	@Override
	public void setIndex(short index) {
		this.index = index;

	}

	@Override
	public int getSize() {
		return 12;
	}

	@Override
	public boolean isRoundtrip() {
		return ROUNDTRIP;
	}

}
