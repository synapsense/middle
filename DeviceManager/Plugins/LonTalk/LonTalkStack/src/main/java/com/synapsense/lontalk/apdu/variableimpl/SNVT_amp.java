package com.synapsense.lontalk.apdu.variableimpl;

import java.io.Serializable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import com.synapsense.lontalk.apdu.variable.NetworkVariable;
import com.synapsense.lontalk.exception.FieldNotFoundException;
import com.synapsense.lontalk.exception.IncorrectValueException;

public class SNVT_amp implements NetworkVariable {

	private short selector;
	private short index;
	private short inputamp = 0;
	private double outputamp = 0;
	private byte[] scaling = { 1, -1, 0 };
	private static final boolean ROUNDTRIP = false;

	@Override
	public void setSelector(short selector) {
		this.selector = selector;
	}

	@Override
	public short getSelector() {
		return selector;
	}

	@Override
	public byte[] getData() {
		byte[] a = new byte[2];
		a[1] = (byte) (inputamp & 0xff);
		a[0] = (byte) ((inputamp >> 8) & 0xff);
		return a;
	}

	@Override
	public void setData(byte[] newdata) {
		ByteBuffer bb = ByteBuffer.allocate(2);
		bb.order(ByteOrder.LITTLE_ENDIAN);
		bb.put(newdata[1]);
		bb.put(newdata[0]);
		inputamp = bb.getShort(0);
		outputamp = scaling[0] * Math.pow(10, scaling[1]) * (inputamp + scaling[2]);

	}

	@Override
	public void setFieldByName(String name, Serializable value) throws FieldNotFoundException, IncorrectValueException {
		int i = 0;
		if (name.equalsIgnoreCase("AMP")) {
			double a = (Double) value;
			if (a < (double) -3276.8 || a > (double) 3276.7) {
				throw new IncorrectValueException("Value is not valid");
			}
			inputamp = (short) ((a / (scaling[0] * Math.pow(10, scaling[1]))) - scaling[2]);
			outputamp = scaling[0] * Math.pow(10, scaling[1]) * (inputamp + scaling[2]);
			i++;
		}
		if (i == 0) {
			throw new FieldNotFoundException("Field is not found");
		}

	}

	@Override
	public Double getFieldByName(String name) throws FieldNotFoundException {
		if (name.equalsIgnoreCase("AMP")) {
			return outputamp;
		}
		throw new FieldNotFoundException("Field is not found");
	}

	@Override
	public short getIndex() {
		return index;
	}

	@Override
	public void setIndex(short index) {
		this.index = index;

	}

	@Override
	public int getSize() {
		return 2;
	}

	@Override
	public boolean isRoundtrip() {
		return ROUNDTRIP;
	}

}
