package com.synapsense.lontalk.apdu.variableimpl.different;

import java.io.Serializable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import com.synapsense.lontalk.apdu.variable.NetworkVariable;
import com.synapsense.lontalk.exception.FieldNotFoundException;
import com.synapsense.lontalk.exception.IncorrectValueException;

public class SNVT_file_pos implements NetworkVariable {

	private short selector;
	private short index;

	private int input_rw_ptr = 0;
	private double output_rw_ptr = 0;
	private byte[] scalPtr = { 1, 0, 0 };

	private int input_rw_length = 0;
	private double output_rw_length = 0;
	private byte[] scalLen = { 1, 0, 0 };

	private static final boolean ROUNDTRIP = true;

	@Override
	public void setSelector(short selector) {
		this.selector = selector;
	}

	@Override
	public short getSelector() {
		return selector;
	}

	@Override
	public byte[] getData() {
		byte[] data = ByteBuffer.allocate(6).putInt(input_rw_ptr).array();
		data[4] = data[4] |= (input_rw_length >>> 8);
		data[5] = data[5] |= (input_rw_length);
		return data;
	}

	@Override
	public void setData(byte[] newdata) {
		ByteBuffer bb = ByteBuffer.allocate(4);
		bb.order(ByteOrder.LITTLE_ENDIAN);
		bb.put(newdata[5]);
		bb.put(newdata[4]);
		bb.put((byte) 0);
		bb.put((byte) 0);
		input_rw_length = bb.getInt(0);
		output_rw_length = scalLen[0] * Math.pow(10, scalLen[1]) * (input_rw_length + scalLen[2]);

		bb.clear();
		bb.put(newdata[3]);
		bb.put(newdata[2]);
		bb.put(newdata[1]);
		bb.put(newdata[0]);
		input_rw_ptr = bb.getInt(0);
		output_rw_ptr = scalPtr[0] * Math.pow(10, scalPtr[1]) * (input_rw_ptr + scalPtr[2]);

	}

	@Override
	public void setFieldByName(String name, Serializable value) throws FieldNotFoundException, IncorrectValueException {
		int i = 0;
		if (name.equalsIgnoreCase("rw_ptr")) {
			Double a = (Double) value;
			if (a < 0 || a > 2147483647) {
				throw new IncorrectValueException("Value is not valid");
			}
			Double output_rw_ptr = a;
			input_rw_ptr = (int) ((output_rw_ptr / (scalPtr[0] * Math.pow(10, scalPtr[1]))) - scalPtr[2]);
			i++;
		}
		if (name.equalsIgnoreCase("rw_length")) {
			Double a = (Double) value;
			if (a < 0 || a > 65535) {
				throw new IncorrectValueException("Value is not valid");
			}
			Double output_rw_length = a;
			input_rw_length = (int) ((output_rw_length / (scalPtr[0] * Math.pow(10, scalPtr[1]))) - scalPtr[2]);
			i++;
		}
		if (i == 0) {
			throw new FieldNotFoundException("Field is not found");
		}

	}

	@Override
	public Double getFieldByName(String name) throws FieldNotFoundException {
		if (name.equalsIgnoreCase("rw_ptr")) {
			return output_rw_ptr;
		}
		if (name.equalsIgnoreCase("rw_length")) {
			return output_rw_length;
		}
		throw new FieldNotFoundException("Field is not found");
	}

	@Override
	public short getIndex() {
		return index;
	}

	@Override
	public void setIndex(short index) {
		this.index = index;

	}

	@Override
	public int getSize() {
		return 6;
	}

	@Override
	public boolean isRoundtrip() {
		return ROUNDTRIP;
	}

}
