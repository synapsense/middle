package com.synapsense.lontalk.packet;

import com.synapsense.lontalk.apdu.variable.NetworkVariable;
import com.synapsense.lontalk.api.Network;
import com.synapsense.lontalk.network.CRC16CCITT;
import com.synapsense.lontalk.node.LonTalkNode;
import com.synapsense.lontalk.packet.header.CNPIPHeader;

public class DataPacket {

	public static byte[] getFetchNVDataPacket(Network network, LonTalkNode node, NetworkVariable nv) {
		LonDataPacket londata = new LonDataPacket(network, node, nv);
		byte[] londataPacket = londata.fetchNV();
		return compilePackage(londataPacket);

	}

	public static byte[] getSetNVDataPacket(Network network, LonTalkNode node, NetworkVariable nv) {
		LonDataPacket londata = new LonDataPacket(network, node, nv);
		byte[] londataPacket = londata.setNV();
		return compilePackage(londataPacket);
	}

	private static byte[] compilePackage(byte[] londataPacket) {
		byte[] crc = CRC16CCITT.getCRC(londataPacket);

		CNPIPHeader header = new CNPIPHeader();
		header.setDataType();
		header.dataPacketLenght = (short) (CNPIPHeader.SIZE + londataPacket.length + crc.length);
		byte[] headerPacket = header.getHeader();

		byte[] dataPacket = new byte[CNPIPHeader.SIZE + londataPacket.length + crc.length];

		for (int i = 0; i < headerPacket.length; i++) {
			dataPacket[i] = headerPacket[i];
		}
		for (int i = 0; i < londataPacket.length; i++) {
			dataPacket[i + headerPacket.length] = londataPacket[i];
		}
		for (int i = 0; i < crc.length; i++) {
			dataPacket[i + headerPacket.length + londataPacket.length] = crc[i];
		}

		return dataPacket;
	}
}
