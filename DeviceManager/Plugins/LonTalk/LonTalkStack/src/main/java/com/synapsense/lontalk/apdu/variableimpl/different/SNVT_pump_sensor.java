package com.synapsense.lontalk.apdu.variableimpl.different;

import java.io.Serializable;

import com.synapsense.lontalk.apdu.variable.NetworkVariable;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_amp;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_freq_hz;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_power_kilo;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_temp;
import com.synapsense.lontalk.exception.FieldNotFoundException;
import com.synapsense.lontalk.exception.IncorrectValueException;

public class SNVT_pump_sensor implements NetworkVariable {

	private short selector;
	private short index;

	private SNVT_freq_hz rotational_speed;
	private SNVT_temp body_temperature;
	private SNVT_temp motor_external_temperature;
	private SNVT_temp motor_internal_temperature;

	private byte motor_overloaded_input;
	private double motor_overloaded_output;

	private byte oil_level_low_input;
	private double oil_level_low_output;

	private byte phase_imbalance_detected_input;
	private double phase_imbalance_detected_output;

	private SNVT_amp current_usage;
	private SNVT_power_kilo power_usage;

	private byte temperature_control_input;
	private double temperature_control_output;

	private byte electromagnetic_brake_active_input;
	private double electromagnetic_brake_active_output;

	private byte friction_brake_active_input;
	private double friction_brake_active_output;

	private byte gas_brake_active_input;
	private double gas_brake_active_output;

	private static final boolean ROUNDTRIP = true;

	public SNVT_pump_sensor() {
		rotational_speed = new SNVT_freq_hz();
		body_temperature = new SNVT_temp();
		motor_external_temperature = new SNVT_temp();
		motor_internal_temperature = new SNVT_temp();
		current_usage = new SNVT_amp();
		power_usage = new SNVT_power_kilo();

	}

	@Override
	public byte[] getData() {
		byte[] data = new byte[19];
		byte[] snvt = new byte[2];
		snvt = rotational_speed.getData();
		data[0] = snvt[0];
		data[1] = snvt[1];
		snvt = body_temperature.getData();
		data[2] = snvt[0];
		data[3] = snvt[1];
		snvt = motor_external_temperature.getData();
		data[4] = snvt[0];
		data[5] = snvt[1];
		snvt = motor_internal_temperature.getData();
		data[6] = snvt[0];
		data[7] = snvt[1];
		data[8] = motor_overloaded_input;
		data[9] = oil_level_low_input;
		data[10] = phase_imbalance_detected_input;
		snvt = current_usage.getData();
		data[11] = snvt[0];
		data[12] = snvt[1];
		snvt = power_usage.getData();
		data[13] = snvt[0];
		data[14] = snvt[1];
		data[15] = temperature_control_input;
		data[16] = electromagnetic_brake_active_input;
		data[17] = friction_brake_active_input;
		data[18] = gas_brake_active_input;
		return data;
	}

	@Override
	public void setData(byte[] newdata) {
		byte[] snvt = new byte[2];
		snvt[0] = newdata[0];
		snvt[1] = newdata[1];
		rotational_speed.setData(snvt);
		snvt[0] = newdata[2];
		snvt[1] = newdata[3];
		body_temperature.setData(snvt);
		snvt[0] = newdata[4];
		snvt[1] = newdata[5];
		motor_external_temperature.setData(snvt);
		snvt[0] = newdata[6];
		snvt[1] = newdata[7];
		motor_internal_temperature.setData(snvt);
		motor_overloaded_input = newdata[8];
		motor_overloaded_output = (double) motor_overloaded_input;
		oil_level_low_input = newdata[9];
		oil_level_low_output = (double) oil_level_low_input;
		phase_imbalance_detected_input = newdata[10];
		phase_imbalance_detected_output = (double) phase_imbalance_detected_input;
		snvt[0] = newdata[11];
		snvt[1] = newdata[12];
		current_usage.setData(snvt);
		snvt[0] = newdata[13];
		snvt[1] = newdata[14];
		power_usage.setData(snvt);
		temperature_control_input = newdata[15];
		temperature_control_output = (double) temperature_control_input;
		electromagnetic_brake_active_input = newdata[16];
		electromagnetic_brake_active_output = (double) electromagnetic_brake_active_input;
		friction_brake_active_input = newdata[17];
		friction_brake_active_output = (double) friction_brake_active_input;
		gas_brake_active_input = newdata[18];
		gas_brake_active_output = (double) gas_brake_active_input;

	}

	@Override
	public short getSelector() {
		return selector;
	}

	@Override
	public short getIndex() {
		return index;
	}

	@Override
	public void setIndex(short index) {
		this.index = index;
	}

	@Override
	public void setSelector(short selector) {
		this.selector = selector;

	}

	@Override
	public int getSize() {
		return 19;
	}

	@Override
	public void setFieldByName(String name, Serializable value) throws FieldNotFoundException, IncorrectValueException {
		int i = 0;
		if (name.equalsIgnoreCase("rotational_speed")) {
			rotational_speed.setFieldByName("freq_hz", value);
			i++;
		}
		if (name.equalsIgnoreCase("body_temperature")) {
			body_temperature.setFieldByName("temp", value);
			i++;
		}
		if (name.equalsIgnoreCase("motor_external_temperature")) {
			motor_external_temperature.setFieldByName("temp", value);
			i++;
		}
		if (name.equalsIgnoreCase("motor_internal_temperature")) {
			motor_internal_temperature.setFieldByName("temp", value);
			i++;
		}
		if (name.equalsIgnoreCase("motor_overloaded")) {
			if ((Double) value < (double) -1 || (Double) value > (double) 1) {
				throw new IncorrectValueException("Value is not valid");
			}
			motor_overloaded_output = (Double) value;
			motor_overloaded_input = (byte) motor_overloaded_output;
			i++;
		}
		if (name.equalsIgnoreCase("oil_level_low")) {
			if ((Double) value < (double) -1 || (Double) value > (double) 1) {
				throw new IncorrectValueException("Value is not valid");
			}
			oil_level_low_output = (Double) value;
			oil_level_low_input = (byte) oil_level_low_output;
			i++;
		}
		if (name.equalsIgnoreCase("phase_imbalance_detected")) {
			if ((Double) value < (double) -1 || (Double) value > (double) 1) {
				throw new IncorrectValueException("Value is not valid");
			}
			phase_imbalance_detected_output = (Double) value;
			phase_imbalance_detected_input = (byte) phase_imbalance_detected_output;
			i++;
		}
		if (name.equalsIgnoreCase("current_usage")) {
			current_usage.setFieldByName("amp", value);
			i++;
		}
		if (name.equalsIgnoreCase("power_usage")) {
			power_usage.setFieldByName("power_kilo", value);
			i++;
		}
		if (name.equalsIgnoreCase("temperature_control")) {
			if ((Double) value < (double) -1 || (Double) value > (double) 3) {
				throw new IncorrectValueException("Value is not valid");
			}
			temperature_control_output = (Double) value;
			temperature_control_input = (byte) temperature_control_output;
			i++;
		}
		if (name.equalsIgnoreCase("electromagnetic_brake_active")) {
			if ((Double) value < (double) -1 || (Double) value > (double) 1) {
				throw new IncorrectValueException("Value is not valid");
			}
			electromagnetic_brake_active_output = (Double) value;
			electromagnetic_brake_active_input = (byte) electromagnetic_brake_active_output;
			i++;
		}
		if (name.equalsIgnoreCase("friction_brake_active")) {
			if ((Double) value < (double) -1 || (Double) value > (double) 1) {
				throw new IncorrectValueException("Value is not valid");
			}
			friction_brake_active_output = (Double) value;
			friction_brake_active_input = (byte) friction_brake_active_output;
			i++;
		}
		if (name.equalsIgnoreCase("gas_brake_active")) {
			if ((Double) value < (double) -1 || (Double) value > (double) 1) {
				throw new IncorrectValueException("Value is not valid");
			}
			gas_brake_active_output = (Double) value;
			gas_brake_active_input = (byte) gas_brake_active_output;
			i++;
		}
		if (i == 0) {
			throw new FieldNotFoundException("Field is not found");
		}

	}

	@Override
	public Double getFieldByName(String name) throws FieldNotFoundException {
		if (name.equalsIgnoreCase("rotational_speed")) {
			return rotational_speed.getFieldByName("freq_hz");
		}
		if (name.equalsIgnoreCase("body_temperature")) {
			return body_temperature.getFieldByName("temp");
		}
		if (name.equalsIgnoreCase("motor_external_temperature")) {
			return motor_external_temperature.getFieldByName("temp");
		}
		if (name.equalsIgnoreCase("motor_internal_temperature")) {
			return motor_internal_temperature.getFieldByName("temp");
		}
		if (name.equalsIgnoreCase("motor_overloaded")) {
			return motor_overloaded_output;
		}
		if (name.equalsIgnoreCase("oil_level_low")) {
			return oil_level_low_output;
		}
		if (name.equalsIgnoreCase("phase_imbalance_detected")) {
			return phase_imbalance_detected_output;
		}
		if (name.equalsIgnoreCase("current_usage")) {
			return current_usage.getFieldByName("amp");
		}
		if (name.equalsIgnoreCase("power_usage")) {
			return power_usage.getFieldByName("power_kilo");
		}
		if (name.equalsIgnoreCase("temperature_control")) {
			return temperature_control_output;
		}
		if (name.equalsIgnoreCase("electromagnetic_brake_active")) {
			return electromagnetic_brake_active_output;
		}
		if (name.equalsIgnoreCase("friction_brake_active")) {
			return friction_brake_active_output;
		}
		if (name.equalsIgnoreCase("gas_brake_active")) {
			return gas_brake_active_output;
		}
		throw new FieldNotFoundException("Field is not found");
	}

	@Override
	public boolean isRoundtrip() {
		return ROUNDTRIP;
	}

}
