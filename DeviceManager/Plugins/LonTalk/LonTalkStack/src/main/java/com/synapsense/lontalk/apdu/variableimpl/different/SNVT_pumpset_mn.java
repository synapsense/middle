package com.synapsense.lontalk.apdu.variableimpl.different;

import java.io.Serializable;

import com.synapsense.lontalk.apdu.variable.NetworkVariable;
import com.synapsense.lontalk.exception.FieldNotFoundException;
import com.synapsense.lontalk.exception.IncorrectValueException;

public class SNVT_pumpset_mn implements NetworkVariable {

	private short selector;
	private short index;

	private byte main_pump_input;
	private double main_pump_output;
	private byte booster_pump_input;
	private double booster_pump_output;
	private byte priority_level_input;
	private double priority_level_output;
	private byte process_ready_input;
	private double process_ready_output;
	private byte emergency_stop_activated_input;
	private double emergency_stop_activated_output;
	private byte main_pump_drive_enabled_input;
	private double main_pump_drive_enabled_output;
	private byte booster_pump_drive_enabled_input;
	private double booster_pump_drive_enabled_output;
	private byte maintenance_required_input;
	private double maintenance_required_output;

	private static final boolean ROUNDTRIP = true;

	@Override
	public byte[] getData() {
		byte[] data = new byte[8];
		data[0] = main_pump_input;
		data[1] = booster_pump_input;
		data[2] = priority_level_input;
		data[3] = process_ready_input;
		data[4] = emergency_stop_activated_input;
		data[5] = main_pump_drive_enabled_input;
		data[6] = booster_pump_drive_enabled_input;
		data[7] = maintenance_required_input;
		return data;
	}

	@Override
	public void setData(byte[] newdata) {
		main_pump_input = newdata[0];
		main_pump_output = (double) main_pump_input;
		booster_pump_input = newdata[1];
		booster_pump_output = (double) booster_pump_input;
		priority_level_input = newdata[2];
		priority_level_output = (double) priority_level_input;
		process_ready_input = newdata[3];
		process_ready_output = (double) process_ready_input;
		emergency_stop_activated_input = newdata[4];
		emergency_stop_activated_output = (double) emergency_stop_activated_input;
		main_pump_drive_enabled_input = newdata[5];
		main_pump_drive_enabled_output = (double) main_pump_drive_enabled_input;
		booster_pump_drive_enabled_input = newdata[6];
		booster_pump_drive_enabled_output = (double) booster_pump_drive_enabled_input;
		maintenance_required_input = newdata[7];
		maintenance_required_output = (double) maintenance_required_input;

	}

	@Override
	public short getSelector() {
		return selector;
	}

	@Override
	public short getIndex() {
		return index;
	}

	@Override
	public void setIndex(short index) {
		this.index = index;

	}

	@Override
	public void setSelector(short selector) {
		this.selector = selector;

	}

	@Override
	public int getSize() {
		return 8;
	}

	@Override
	public void setFieldByName(String name, Serializable value) throws FieldNotFoundException, IncorrectValueException {
		int i = 0;
		if (name.equalsIgnoreCase("main_pump")) {
			if ((Double) value < (double) -1 || (Double) value > (double) 7) {
				throw new IncorrectValueException("Value is not valid");
			}
			main_pump_output = (Double) value;
			main_pump_input = (byte) main_pump_output;
			i++;
		}
		if (name.equalsIgnoreCase("booster_pump")) {
			if ((Double) value < (double) -1 || (Double) value > (double) 7) {
				throw new IncorrectValueException("Value is not valid");
			}
			booster_pump_output = (Double) value;
			booster_pump_input = (byte) booster_pump_output;
			i++;
		}
		if (name.equalsIgnoreCase("priority_level")) {
			if ((Double) value < (double) -1 || (Double) value > (double) 11) {
				throw new IncorrectValueException("Value is not valid");
			}
			priority_level_output = (Double) value;
			priority_level_input = (byte) priority_level_output;
			i++;
		}
		if (name.equalsIgnoreCase("process_ready")) {
			if ((Double) value < (double) -1 || (Double) value > (double) 1) {
				throw new IncorrectValueException("Value is not valid");
			}
			process_ready_output = (Double) value;
			process_ready_input = (byte) process_ready_output;
			i++;
		}
		if (name.equalsIgnoreCase("emergency_stop_activated")) {
			if ((Double) value < (double) -1 || (Double) value > (double) 1) {
				throw new IncorrectValueException("Value is not valid");
			}
			emergency_stop_activated_output = (Double) value;
			emergency_stop_activated_input = (byte) emergency_stop_activated_output;
			i++;
		}
		if (name.equalsIgnoreCase("main_pump_drive_enabled")) {
			if ((Double) value < (double) -1 || (Double) value > (double) 1) {
				throw new IncorrectValueException("Value is not valid");
			}
			main_pump_drive_enabled_output = (Double) value;
			main_pump_drive_enabled_input = (byte) main_pump_drive_enabled_output;
			i++;
		}

		if (name.equalsIgnoreCase("booster_pump_drive_enabled")) {
			if ((Double) value < (double) -1 || (Double) value > (double) 1) {
				throw new IncorrectValueException("Value is not valid");
			}
			booster_pump_drive_enabled_output = (Double) value;
			booster_pump_drive_enabled_input = (byte) booster_pump_drive_enabled_output;
			i++;
		}
		if (name.equalsIgnoreCase("maintenance_required")) {
			if ((Double) value < (double) -1 || (Double) value > (double) 1) {
				throw new IncorrectValueException("Value is not valid");
			}
			maintenance_required_output = (Double) value;
			maintenance_required_input = (byte) maintenance_required_output;
			i++;
		}

		if (i == 0) {
			throw new FieldNotFoundException("Field is not found");
		}

	}

	@Override
	public Double getFieldByName(String name) throws FieldNotFoundException {
		if (name.equalsIgnoreCase("main_pump")) {
			return main_pump_output;
		}
		if (name.equalsIgnoreCase("booster_pump")) {
			return booster_pump_output;
		}
		if (name.equalsIgnoreCase("priority_level")) {
			return priority_level_output;
		}
		if (name.equalsIgnoreCase("process_ready")) {
			return process_ready_output;
		}
		if (name.equalsIgnoreCase("emergency_stop_activated")) {
			return emergency_stop_activated_output;
		}
		if (name.equalsIgnoreCase("main_pump_drive_enabled")) {
			return main_pump_drive_enabled_output;
		}
		if (name.equalsIgnoreCase("booster_pump_drive_enabled")) {
			return booster_pump_drive_enabled_output;
		}
		if (name.equalsIgnoreCase("maintenance_required")) {
			return maintenance_required_output;
		}
		throw new FieldNotFoundException("Field is not found");
	}

	@Override
	public boolean isRoundtrip() {
		return ROUNDTRIP;
	}

}
