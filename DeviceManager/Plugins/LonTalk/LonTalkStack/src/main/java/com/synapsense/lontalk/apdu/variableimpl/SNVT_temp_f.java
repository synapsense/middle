package com.synapsense.lontalk.apdu.variableimpl;

import java.io.Serializable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import com.synapsense.lontalk.apdu.variable.NetworkVariable;
import com.synapsense.lontalk.exception.FieldNotFoundException;
import com.synapsense.lontalk.exception.IncorrectValueException;

public class SNVT_temp_f implements NetworkVariable {

	private short selector;
	private short index;
	private float input;
	private static final boolean ROUNDTRIP = false;

	@Override
	public byte[] getData() {
		byte[] a = new byte[4];
		int valint = Float.floatToIntBits(input);
		a[0] = (byte) ((valint >> 24) & 0xff);
		a[1] = (byte) ((valint >> 16) & 0xff);
		a[2] = (byte) ((valint >> 8) & 0xff);
		a[3] = (byte) ((valint) & 0xff);
		return a;
	}

	@Override
	public void setData(byte[] newdata) {
		ByteBuffer bb = ByteBuffer.allocate(4);
		bb.order(ByteOrder.LITTLE_ENDIAN);
		bb.put(newdata[3]);
		bb.put(newdata[2]);
		bb.put(newdata[1]);
		bb.put(newdata[0]);
		input = bb.getFloat(0);

	}

	@Override
	public short getSelector() {
		return selector;
	}

	@Override
	public void setSelector(short selector) {
		this.selector = selector;
	}

	@Override
	public void setFieldByName(String name, Serializable value) throws FieldNotFoundException, IncorrectValueException {
		int i = 0;
		if (name.equalsIgnoreCase("temp_f")) {
			double b = (Double) value;
			if (b < (double) -273.17001342773 || b > 3.4028234663853e+38f) {
				throw new IncorrectValueException("Value is not valid");
			}
			input = (float) b;
			i++;
		}
		if (i == 0) {
			throw new FieldNotFoundException("Field is not found");
		}

	}

	@Override
	public Double getFieldByName(String name) throws FieldNotFoundException {
		if (name.equalsIgnoreCase("temp_f")) {
			return (double) input;
		}
		throw new FieldNotFoundException("Field is not found");
	}

	@Override
	public short getIndex() {
		return index;
	}

	@Override
	public void setIndex(short index) {
		this.index = index;

	}

	@Override
	public int getSize() {
		return 4;
	}

	@Override
	public boolean isRoundtrip() {
		return ROUNDTRIP;
	}

}
