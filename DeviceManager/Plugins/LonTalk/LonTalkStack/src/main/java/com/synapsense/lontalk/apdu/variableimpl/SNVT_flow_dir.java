package com.synapsense.lontalk.apdu.variableimpl;

import java.io.Serializable;

import com.synapsense.lontalk.apdu.variable.NetworkVariable;
import com.synapsense.lontalk.exception.FieldNotFoundException;
import com.synapsense.lontalk.exception.IncorrectValueException;

public class SNVT_flow_dir implements NetworkVariable {

	private short selector;
	private short index;
	private byte input = 0;
	private double output = 0;
	private static final boolean ROUNDTRIP = false;

	// FD_NUL -1 Invalid Value
	// FD_NONE 0 No flow/movement allowed
	// FD_OUT 1 Exit/out/away direction only
	// FD_IN 2 Entry/in/toward direction only
	// FD_ANY 3 No restriction on flow/movement

	@Override
	public byte[] getData() {
		byte[] a = { input };
		return a;
	}

	@Override
	public void setData(byte[] newdata) {
		input = newdata[0];
		output = (double) input;

	}

	@Override
	public short getSelector() {
		return selector;
	}

	@Override
	public void setSelector(short selector) {
		this.selector = selector;

	}

	@Override
	public void setFieldByName(String name, Serializable value) throws FieldNotFoundException, IncorrectValueException {
		int i = 0;
		if (name.equalsIgnoreCase("flow_dir")) {
			if ((Double) value < (double) -1 || (Double) value > (double) 3) {
				throw new IncorrectValueException("Value is not valid");
			}
			output = (Double) value;
			input = (byte) output;
			i++;
		}
		if (i == 0) {
			throw new FieldNotFoundException("Field is not found");
		}

	}

	@Override
	public Double getFieldByName(String name) throws FieldNotFoundException {
		if (name.equalsIgnoreCase("flow_dir")) {
			return output;
		}
		throw new FieldNotFoundException("Field is not found");
	}

	@Override
	public short getIndex() {
		return index;
	}

	@Override
	public void setIndex(short index) {
		this.index = index;

	}

	@Override
	public int getSize() {
		return 1;
	}

	@Override
	public boolean isRoundtrip() {
		return ROUNDTRIP;
	}

}
