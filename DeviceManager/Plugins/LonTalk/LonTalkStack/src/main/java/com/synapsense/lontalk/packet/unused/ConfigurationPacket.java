package com.synapsense.lontalk.packet.unused;

import com.synapsense.lontalk.packet.header.CNPIPHeader;
import com.synapsense.lontalk.transaction.SessionId;
// item 9.8 from ANSI CEA-852
//unused

public class ConfigurationPacket {
	private int dateTime;
	private byte reason = 0x4;
	private short requestId;
	private byte segmentId = 0;
	private int sinceDateTime = 0;

	public byte[] getConfigurationPacket() {
		CNPIPHeader cnpipheader = new CNPIPHeader();
		cnpipheader.setConfigurationType();
		cnpipheader.setDataPacketLenght((short) 40);

		requestId = SessionId.getInstance().getRequestId();
		byte[] packet = new byte[36];
		byte[] header = cnpipheader.getHeader();
		for (int i = 0; i < header.length; i++) {
			packet[i] = header[i];
		}
		packet[20] = packet[20] |= (dateTime >>> 24);
		packet[21] = packet[21] |= (dateTime >>> 16);
		packet[22] = packet[22] |= (dateTime >>> 8);
		packet[23] = packet[23] |= dateTime;
		packet[24] = reason;
		packet[25] = (byte) ((requestId >> 8) & 0Xff);
		packet[26] = (byte) (requestId & 0xFF);
		packet[27] = segmentId;
		packet[28] = packet[28] |= (sinceDateTime >>> 24);
		packet[29] = packet[29] |= (sinceDateTime >>> 16);
		packet[30] = packet[30] |= (sinceDateTime >>> 8);
		packet[31] = packet[31] |= sinceDateTime;
		packet[32] = 0;
		packet[33] = 0;
		packet[34] = 0;
		packet[35] = 0;
		packet[36] = 0;
		packet[37] = 0;
		packet[38] = 0;
		packet[39] = 0;
		return packet;
	}
}
