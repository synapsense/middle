package com.synapsense.lontalk.packet;

import java.net.InetAddress;
import java.net.UnknownHostException;

import com.synapsense.lontalk.api.Network;
import com.synapsense.lontalk.packet.header.CNPIPHeader;

public class ChannelRouting {

	private int dateTime = 0xd29c1817;
	private short ipMultiCastPort = 0;

	private short ipUniCastPort;

	private byte[] ipmcaddress = { 0, 0, 0, 0 };

	private byte[] ipunicastAddress;

	private byte ipflags = 0x01;
	private byte cnroutertype = 0x00;
	private byte cnflags = 0x03;
	private byte nodeType = 0x01;
	private short totalUniqueIDBytes = 6;

	private short totalSubnetNodeBytes = 0xC;
	private short totalDomainBytes = 0x48;

	private byte[] uniqueID;
	private byte subnetNumber;
	private byte nodeNumber;

	private short domainIndex = 0;
	private short uniqueidindex = 0;
	private byte[] subnetMsk = new byte[32];
	private byte[] groupMsk = new byte[32];
	private byte lenght;

	byte[] domain;

	public byte[] getChannelRoutingPacket(Network network) throws UnknownHostException {
		for (int i = 0; i < subnetMsk.length; i++) {
			subnetMsk[i] = 0;
			groupMsk[i] = 0;
		}
		ipUniCastPort = (short) network.pluginPort;
		ipunicastAddress = InetAddress.getByName(network.pluginIpAddress).getAddress();
		uniqueID = network.getNeuronIDBytes();
		subnetNumber = network.pluginSubnet;
		nodeNumber = network.pluginNode;
		lenght = (byte) network.domainLength;
		domain = network.getDomainInSixBytes();

		CNPIPHeader header = new CNPIPHeader();
		header.setRoutingType();
		header.setDataPacketLenght((short) 138);
		byte[] headerbyte = header.getHeader();
		byte[] packet = new byte[138];
		for (int i = 0; i < headerbyte.length; i++) {
			packet[i] = headerbyte[i];
		}
		packet[20] = packet[20] |= (dateTime >>> 24);
		packet[21] = packet[21] |= (dateTime >>> 16);
		packet[22] = packet[22] |= (dateTime >>> 8);
		packet[23] = packet[23] |= dateTime;
		packet[24] = (byte) ((ipMultiCastPort >> 8) & 0Xff);
		packet[25] = (byte) (ipMultiCastPort & 0xFF);
		packet[26] = (byte) ((ipUniCastPort >> 8) & 0Xff);
		packet[27] = (byte) (ipUniCastPort & 0xFF);
		packet[28] = ipmcaddress[0];
		packet[29] = ipmcaddress[1];
		packet[30] = ipmcaddress[2];
		packet[31] = ipmcaddress[3];
		packet[32] = ipunicastAddress[0];
		packet[33] = ipunicastAddress[1];
		packet[34] = ipunicastAddress[2];
		packet[35] = ipunicastAddress[3];
		packet[36] = ipflags;
		packet[37] = cnroutertype;
		packet[38] = cnflags;
		packet[39] = nodeType;
		packet[40] = (byte) ((totalUniqueIDBytes >> 8) & 0Xff);
		packet[41] = (byte) (totalUniqueIDBytes & 0xFF);
		packet[42] = 0;
		packet[43] = 0;
		packet[44] = (byte) ((totalSubnetNodeBytes >> 8) & 0Xff);
		packet[45] = (byte) (totalSubnetNodeBytes & 0xFF);
		packet[46] = (byte) ((totalDomainBytes >> 8) & 0Xff);
		packet[47] = (byte) (totalDomainBytes & 0xFF);
		packet[48] = uniqueID[0];
		packet[49] = uniqueID[1];
		packet[50] = uniqueID[2];
		packet[51] = uniqueID[3];
		packet[52] = uniqueID[4];
		packet[53] = uniqueID[5];
		packet[54] = subnetNumber;
		packet[55] = nodeNumber;
		packet[56] = (byte) ((domainIndex >> 8) & 0Xff);
		packet[57] = (byte) (domainIndex & 0xFF);
		packet[58] = (byte) ((uniqueidindex >> 8) & 0Xff);
		packet[59] = (byte) (uniqueidindex & 0xFF);

		packet[60] = 0x01;
		packet[61] = 0x04;
		packet[62] = (byte) ((domainIndex >> 8) & 0Xff);
		packet[63] = (byte) (domainIndex & 0xFF);
		packet[64] = (byte) ((uniqueidindex >> 8) & 0Xff);
		packet[65] = (byte) (uniqueidindex & 0xFF);

		for (int i = 0; i < subnetMsk.length; i++) {
			packet[i + 66] = subnetMsk[i];
		}
		for (int i = 0; i < groupMsk.length; i++) {
			packet[i + 98] = groupMsk[i];
		}
		packet[130] = lenght;
		packet[131] = 0;
		packet[132] = domain[0];
		packet[133] = domain[1];
		packet[134] = domain[2];
		packet[135] = domain[3];
		packet[136] = domain[4];
		packet[137] = domain[5];
		return packet;
	}

}
