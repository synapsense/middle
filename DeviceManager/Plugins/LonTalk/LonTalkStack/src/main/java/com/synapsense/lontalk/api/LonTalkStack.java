package com.synapsense.lontalk.api;

import com.synapsense.lontalk.apdu.variable.NetworkVariable;
import com.synapsense.lontalk.exception.CouldNotGetData;
import com.synapsense.lontalk.exception.CouldNotSetData;
import com.synapsense.lontalk.node.CNPIPDevice;
import com.synapsense.lontalk.node.LonTalkNode;
import com.synapsense.lontalk.packet.DataPacket;
import com.synapsense.lontalk.transport.UDPServer;
import com.synapsense.lontalk.transport.handler.ResponseHandler;

public class LonTalkStack {
	static UDPServer server;
	private Network network;

	public LonTalkStack(Network network) {
		this.network = network;
	}

	protected synchronized NetworkVariable sendNVFetchRequest(NetworkVariable nv, CNPIPDevice device, LonTalkNode node)
	        throws CouldNotGetData {
		byte[] data = DataPacket.getFetchNVDataPacket(network, node, nv);
		ResponseHandler handler = new ResponseHandler(nv);
		for (int i = 0; i < device.getRetries(); i++) {
			nv = server.write(device, nv, handler, data);
			if (nv != null) {
				return nv;
			}
		}
		throw new CouldNotGetData("Could not get data");
	}

	@SuppressWarnings("unchecked")
	public <T extends NetworkVariable> T getNV(T var, CNPIPDevice device, LonTalkNode node) throws CouldNotGetData {
		return (T) sendNVFetchRequest(var, device, node);
	}

	@SuppressWarnings("unchecked")
	public <T extends NetworkVariable> T setNV(T var, CNPIPDevice device, LonTalkNode node) throws CouldNotSetData {
		return (T) sendNVSetRequest(var, device, node);
	}

	protected synchronized NetworkVariable sendNVSetRequest(NetworkVariable nv, CNPIPDevice device, LonTalkNode node)
	        throws CouldNotSetData {
		byte[] data = DataPacket.getSetNVDataPacket(network, node, nv);
		ResponseHandler handler = new ResponseHandler(nv);
		for (int i = 0; i < device.getRetries(); i++) {
			nv = server.write(device, nv, handler, data);
			if (nv != null) {
				return nv;
			}
		}
		throw new CouldNotSetData("Could not set data");

	}

	public void startStack() {
		server = new UDPServer(network);
		server.start();

	}

	public void stopStack() {
		server.stop();
	}

}
