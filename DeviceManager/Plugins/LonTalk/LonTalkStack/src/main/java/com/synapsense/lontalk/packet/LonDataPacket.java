package com.synapsense.lontalk.packet;

import com.synapsense.lontalk.apdu.variable.NetworkVariable;
import com.synapsense.lontalk.api.Network;
import com.synapsense.lontalk.network.NetworkDataUnit;
import com.synapsense.lontalk.network.SessionDataUnit;
import com.synapsense.lontalk.node.LonTalkNode;

class LonDataPacket {
	private Network network;
	private LonTalkNode node;
	private NetworkVariable nv;

	protected LonDataPacket(Network network, LonTalkNode node, NetworkVariable nv) {
		this.node = node;
		this.nv = nv;
		this.network = network;
	}

	protected byte[] fetchNV() {
		NetworkDataUnit networkdataunit = new NetworkDataUnit(network, node);
		networkdataunit.setAddressType2();
		networkdataunit.setSPDUFormat();
		byte[] networkPacket = networkdataunit.getData();
		byte sessionPacket = SessionDataUnit.getRequestData();
		short index = nv.getIndex();
		if (index == 255) {
			byte[] data = new byte[networkPacket.length + 5];
			for (int i = 0; i < networkPacket.length; i++) {
				data[i] = networkPacket[i];
			}
			short selector = nv.getSelector();
			data[data.length - 5] = sessionPacket;
			data[data.length - 4] = 0x73;
			data[data.length - 3] = data[data.length - 3] |= nv.getIndex();
			data[data.length - 2] = data[data.length - 2] |= selector >>> 8;
			data[data.length - 1] = data[data.length - 1] |= selector;
			return data;
		} else {
			byte[] data = new byte[networkPacket.length + 3];
			for (int i = 0; i < networkPacket.length; i++) {
				data[i] = networkPacket[i];
			}
			data[data.length - 3] = sessionPacket;
			data[data.length - 2] = 0x73;
			data[data.length - 1] = data[data.length - 1] |= nv.getIndex();
			return data;
		}

	}

	protected byte[] setNV() {
		NetworkDataUnit networkdataunit = new NetworkDataUnit(network, node);
		networkdataunit.setAddressType2();
		networkdataunit.setTPDUFormat();
		byte[] networkPacket = networkdataunit.getData();
		byte sessionPacket = SessionDataUnit.getACKDData();
		byte[] data = new byte[networkPacket.length + nv.getSize() + 3];
		int nvType = 0x8000;
		nvType = nvType |= nv.getSelector();
		byte[] nvdata = nv.getData();
		for (int i = 0; i < networkPacket.length; i++) {
			data[i] = networkPacket[i];
		}
		data[networkPacket.length] = sessionPacket;
		data[networkPacket.length + 1] = data[data.length - 2] |= (nvType >>> 8);
		data[networkPacket.length + 2] = data[data.length - 1] |= (nvType);

		for (int i = 0; i < nvdata.length; i++) {
			data[networkPacket.length + 3 + i] = nvdata[i];
		}
		return data;
	}

}
