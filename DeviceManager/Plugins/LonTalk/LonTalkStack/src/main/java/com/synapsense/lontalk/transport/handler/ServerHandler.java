package com.synapsense.lontalk.transport.handler;

import org.apache.log4j.Logger;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.channel.ExceptionEvent;
import org.jboss.netty.channel.MessageEvent;
import org.jboss.netty.channel.SimpleChannelHandler;

import com.synapsense.lontalk.api.Network;
import com.synapsense.lontalk.packet.Acknowledged;
import com.synapsense.lontalk.packet.ChannelRouting;
import com.synapsense.lontalk.packet.RegistrationPacket;
import com.synapsense.lontalk.transport.PacketNameConstant;

public class ServerHandler extends SimpleChannelHandler {
	private static final Logger logger = Logger.getLogger(ServerHandler.class);
	private ResponseHandler handler;
	private Network network;

	public ServerHandler(Network network) {
		this.network = network;
	}

	@Override
	public void messageReceived(ChannelHandlerContext ctx, MessageEvent e) throws Exception {
		byte[] msg = (byte[]) e.getMessage();
		byte type = msg[3];

		switch (type) {
		case PacketNameConstant.DEVICE_REGISTRATION_REQUEST:
			logger.debug("Have got Device Registration Request from Configuration Server");
			e.getChannel().write(new RegistrationPacket().getRegistrationPacket(network), e.getRemoteAddress());
			break;
		case PacketNameConstant.CHANNEL_ROUTING_REQUEST:
			logger.debug("Have got Channel Routing Request from Configuration Server");
			e.getChannel().write(new ChannelRouting().getChannelRoutingPacket(network), e.getRemoteAddress());
			break;
		case PacketNameConstant.CHANNEL_MEMBERSHIP:
			logger.debug("Have got Channel Membership Packet from Configuration Server");
			e.getChannel().write(new Acknowledged().getPacket(), e.getRemoteAddress());
			break;
		case PacketNameConstant.DATA_PACKET:
			handler.messageReceived(msg);
			break;
		}
	}

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, ExceptionEvent e) throws Exception {
		logger.warn(e.getCause().getMessage(), e.getCause());
	}

	public void setResponseHandler(ResponseHandler handler) {
		this.handler = handler;
	}

	public ResponseHandler getResponseHandler() {
		return handler;
	}
}
