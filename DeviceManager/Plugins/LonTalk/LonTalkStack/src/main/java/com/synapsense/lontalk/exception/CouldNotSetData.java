package com.synapsense.lontalk.exception;

public class CouldNotSetData extends Exception {

	/**
     * 
     */
	private static final long serialVersionUID = 8059888597189236031L;

	public CouldNotSetData(String message) {
		super(message);
	}

	public CouldNotSetData(String msg, Throwable e) {
		super(msg, e);
	}

}
