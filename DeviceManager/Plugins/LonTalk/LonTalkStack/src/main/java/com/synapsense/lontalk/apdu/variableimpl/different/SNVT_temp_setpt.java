package com.synapsense.lontalk.apdu.variableimpl.different;

import java.io.Serializable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import com.synapsense.lontalk.apdu.variable.NetworkVariable;
import com.synapsense.lontalk.exception.FieldNotFoundException;
import com.synapsense.lontalk.exception.IncorrectValueException;

public class SNVT_temp_setpt implements NetworkVariable {

	private short selector;
	private short index;

	private short occupied_cool;
	private double occupiedcooloutput;

	private short standby_cool;
	private double standbycooloutput;

	private short unoccupied_cool;
	private double unoccupiedcooloutput;

	private short occupied_heat;
	private double occupiedheatoutput;

	private short standby_heat;
	private double standbyheatoutput;

	private short unoccupied_heat;
	private double unoccupiedheatoutput;

	private byte[] scaling = { 1, -2, 0 };

	private static final boolean ROUNDTRIP = true;

	@Override
	public byte[] getData() {
		byte[] a = new byte[12];
		a[0] = (byte) ((occupied_cool >> 8) & 0xff);
		a[1] = (byte) (occupied_cool & 0xff);
		a[2] = (byte) ((standby_cool >> 8) & 0xff);
		a[3] = (byte) (standby_cool & 0xff);
		a[4] = (byte) ((unoccupied_cool >> 8) & 0xff);
		a[5] = (byte) (unoccupied_cool & 0xff);
		a[6] = (byte) ((occupied_heat >> 8) & 0xff);
		a[7] = (byte) (occupied_heat & 0xff);
		a[8] = (byte) ((standby_heat >> 8) & 0xff);
		a[9] = (byte) (standby_heat & 0xff);
		a[10] = (byte) ((unoccupied_heat >> 8) & 0xff);
		a[11] = (byte) (unoccupied_heat & 0xff);
		return a;
	}

	@Override
	public void setData(byte[] newdata) {
		ByteBuffer bb = ByteBuffer.allocate(2);
		bb.order(ByteOrder.LITTLE_ENDIAN);
		bb.put(newdata[1]);
		bb.put(newdata[0]);
		occupied_cool = bb.getShort(0);
		occupiedcooloutput = scaling[0] * Math.pow(10, scaling[1]) * (occupied_cool + scaling[2]);
		bb.clear();
		bb.put(newdata[3]);
		bb.put(newdata[2]);
		standby_cool = bb.getShort(0);
		standbycooloutput = scaling[0] * Math.pow(10, scaling[1]) * (standby_cool + scaling[2]);
		bb.clear();
		bb.put(newdata[5]);
		bb.put(newdata[4]);
		unoccupied_cool = bb.getShort(0);
		unoccupiedcooloutput = scaling[0] * Math.pow(10, scaling[1]) * (unoccupied_cool + scaling[2]);
		bb.clear();
		bb.put(newdata[7]);
		bb.put(newdata[6]);
		occupied_heat = bb.getShort(0);
		occupiedheatoutput = scaling[0] * Math.pow(10, scaling[1]) * (occupied_heat + scaling[2]);
		bb.clear();
		bb.put(newdata[9]);
		bb.put(newdata[8]);
		standby_heat = bb.getShort(0);
		standbyheatoutput = scaling[0] * Math.pow(10, scaling[1]) * (standby_heat + scaling[2]);
		bb.clear();
		bb.put(newdata[11]);
		bb.put(newdata[10]);
		unoccupied_heat = bb.getShort(0);
		unoccupiedheatoutput = scaling[0] * Math.pow(10, scaling[1]) * (unoccupied_heat + scaling[2]);

	}

	@Override
	public short getSelector() {
		return selector;
	}

	@Override
	public void setSelector(short selector) {
		this.selector = selector;

	}

	@Override
	public void setFieldByName(String name, Serializable value) throws FieldNotFoundException, IncorrectValueException {
		int i = 0;
		if (name.equalsIgnoreCase("occupied_cool")) {
			Double a = (Double) value;
			if (a < -273.17 || a >= 327.67) {
				throw new IncorrectValueException("Value is not valid");
			}
			occupied_cool = (short) ((a / (scaling[0] * Math.pow(10, scaling[1]))) - scaling[2]);
			occupiedcooloutput = scaling[0] * Math.pow(10, scaling[1]) * (occupied_cool + scaling[2]);
			i++;
		}
		if (name.equalsIgnoreCase("standby_cool")) {
			Double a = (Double) value;
			if (a < -273.17 || a >= 327.67) {
				throw new IncorrectValueException("Value is not valid");
			}
			standby_cool = (short) ((a / (scaling[0] * Math.pow(10, scaling[1]))) - scaling[2]);
			standbycooloutput = scaling[0] * Math.pow(10, scaling[1]) * (standby_cool + scaling[2]);
			i++;
		}
		if (name.equalsIgnoreCase("unoccupied_cool")) {
			Double a = (Double) value;
			if (a < -273.17 || a >= 327.67) {
				throw new IncorrectValueException("Value is not valid");
			}
			unoccupied_cool = (short) ((a / (scaling[0] * Math.pow(10, scaling[1]))) - scaling[2]);
			unoccupiedcooloutput = scaling[0] * Math.pow(10, scaling[1]) * (unoccupied_cool + scaling[2]);
			i++;
		}
		if (name.equalsIgnoreCase("occupied_heat")) {
			Double a = (Double) value;
			if (a < -273.17 || a >= 327.67) {
				throw new IncorrectValueException("Value is not valid");
			}
			occupied_heat = (short) ((a / (scaling[0] * Math.pow(10, scaling[1]))) - scaling[2]);
			occupiedheatoutput = scaling[0] * Math.pow(10, scaling[1]) * (occupied_heat + scaling[2]);
			i++;
		}
		if (name.equalsIgnoreCase("standby_heat")) {
			Double a = (Double) value;
			if (a < -273.17 || a >= 327.67) {
				throw new IncorrectValueException("Value is not valid");
			}
			standby_heat = (short) ((a / (scaling[0] * Math.pow(10, scaling[1]))) - scaling[2]);
			standbyheatoutput = scaling[0] * Math.pow(10, scaling[1]) * (standby_heat + scaling[2]);
			i++;
		}
		if (name.equalsIgnoreCase("unoccupied_heat")) {
			Double a = (Double) value;
			if (a < -273.17 || a >= 327.67) {
				throw new IncorrectValueException("Value is not valid");
			}
			unoccupied_heat = (short) ((a / (scaling[0] * Math.pow(10, scaling[1]))) - scaling[2]);
			unoccupiedheatoutput = scaling[0] * Math.pow(10, scaling[1]) * (unoccupied_heat + scaling[2]);
			i++;
		}
		if (i == 0) {
			throw new FieldNotFoundException("Field is not found");
		}

	}

	@Override
	public Double getFieldByName(String name) throws FieldNotFoundException {
		if (name.equalsIgnoreCase("occupied_cool")) {
			return occupiedcooloutput;
		}
		if (name.equalsIgnoreCase("standby_cool")) {
			return standbycooloutput;
		}
		if (name.equalsIgnoreCase("unoccupied_cool")) {
			return unoccupiedcooloutput;
		}
		if (name.equalsIgnoreCase("occupied_heat")) {
			return occupiedheatoutput;
		}
		if (name.equalsIgnoreCase("standby_heat")) {
			return standbyheatoutput;
		}
		if (name.equalsIgnoreCase("unoccupied_heat")) {
			return unoccupiedheatoutput;
		}
		throw new FieldNotFoundException("Field is not found");
	}

	@Override
	public short getIndex() {
		return index;
	}

	@Override
	public void setIndex(short index) {
		this.index = index;

	}

	@Override
	public int getSize() {
		return 12;
	}

	@Override
	public boolean isRoundtrip() {
		return ROUNDTRIP;
	}

}
