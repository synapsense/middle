package com.synapsense.lontalk.transport.coder;

import static org.jboss.netty.buffer.ChannelBuffers.dynamicBuffer;

import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBufferOutputStream;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.handler.codec.oneone.OneToOneEncoder;

public class LonTalkEncoder extends OneToOneEncoder {

	@Override
	protected Object encode(ChannelHandlerContext ctx, Channel channel, Object msg) throws Exception {
		byte[] data = (byte[]) msg;
		ChannelBufferOutputStream bout = new ChannelBufferOutputStream(dynamicBuffer(data.length, ctx.getChannel()
		        .getConfig().getBufferFactory()));
		bout.write(data);
		bout.flush();
		bout.close();
		ChannelBuffer encoded = bout.buffer();
		return encoded;
	}

}
