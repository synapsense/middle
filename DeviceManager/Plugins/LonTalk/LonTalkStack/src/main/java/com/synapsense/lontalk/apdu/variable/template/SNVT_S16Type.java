package com.synapsense.lontalk.apdu.variable.template;

import java.io.Serializable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import com.synapsense.lontalk.apdu.variable.NetworkVariable;
import com.synapsense.lontalk.exception.FieldNotFoundException;

public class SNVT_S16Type implements NetworkVariable {

	private short selector;
	private short index;
	private short input = 0;
	private double output = 0;
	private byte[] scaling = new byte[3];
	private boolean roundtrip = false;

	public SNVT_S16Type(String scale) {
		parseString(scale);
	}

	@Override
	public byte[] getData() {
		byte[] a = new byte[2];
		a[1] = (byte) (input & 0xff);
		a[0] = (byte) ((input >> 8) & 0xff);
		return a;
	}

	@Override
	public void setData(byte[] newdata) {
		ByteBuffer bb = ByteBuffer.allocate(2);
		bb.order(ByteOrder.LITTLE_ENDIAN);
		bb.put(newdata[1]);
		bb.put(newdata[0]);
		input = bb.getShort(0);
		output = scaling[0] * Math.pow(10, scaling[1]) * (input + scaling[2]);

	}

	@Override
	public short getSelector() {
		return selector;
	}

	@Override
	public void setSelector(short selector) {
		this.selector = selector;

	}

	@Override
	public void setFieldByName(String name, Serializable value) throws FieldNotFoundException {
		double a = (Double) value;
		input = (short) ((a / (scaling[0] * Math.pow(10, scaling[1]))) - scaling[2]);
		output = scaling[0] * Math.pow(10, scaling[1]) * (input + scaling[2]);

	}

	@Override
	public Double getFieldByName(String name) throws FieldNotFoundException {
		return output;
	}

	private void parseString(String scale) {
		scale.trim();
		String[] a = scale.split(";");
		scaling[0] = Byte.parseByte(a[0]);
		scaling[1] = Byte.parseByte(a[1]);
		scaling[2] = Byte.parseByte(a[2]);
	}

	@Override
	public short getIndex() {
		return index;
	}

	@Override
	public void setIndex(short index) {
		this.index = index;

	}

	@Override
	public int getSize() {
		return 2;
	}

	@Override
	public boolean isRoundtrip() {
		return roundtrip;
	}

}
