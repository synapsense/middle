package com.synapsense.lontalk.transport.handler;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import com.synapsense.lontalk.apdu.variable.NetworkVariable;
import com.synapsense.lontalk.network.TypeConstant;
import com.synapsense.lontalk.packet.header.CNPIPHeader;

public class ResponseHandler {
	private boolean haveresponse;
	private NetworkVariable nv;

	public ResponseHandler(NetworkVariable nv) {
		this.nv = nv;
		haveresponse = false;
	}

	public void messageReceived(byte[] msg) {
		byte extendedHeaderSize = msg[4];
		byte[] londatapacket = new byte[msg.length - CNPIPHeader.SIZE - 4 * extendedHeaderSize];
		for (int i = 0; i < londatapacket.length; i++) {
			londatapacket[i] = msg[CNPIPHeader.SIZE + 4 * extendedHeaderSize + i];
		}
		byte npdu = londatapacket[1];
		int domainLenghtAlias = npdu & 0x3;
		int domainLenght = 1;
		switch (domainLenghtAlias) {
		case 0x1:
			domainLenght = 1;
			break;
		case 0x2:
			domainLenght = 3;
			break;
		case 0x3:
			domainLenght = 6;
			break;
		}
		npdu = (byte) (npdu & 0x30);
		switch (npdu) {
		case TypeConstant.SPDU:
			byte spdu = londatapacket[6 + domainLenght];
			spdu = (byte) (spdu & 0x30);
			if (spdu == TypeConstant.RESPONSE) {
				byte type = londatapacket[7 + domainLenght];
				byte index = londatapacket[8 + domainLenght];
				if (type == TypeConstant.APPLICATION_APDY) {
					if (index == (byte) -1) {
						ByteBuffer bb = ByteBuffer.allocate(2);
						bb.order(ByteOrder.LITTLE_ENDIAN);
						bb.put(londatapacket[10 + domainLenght]);
						bb.put(londatapacket[9 + domainLenght]);
						short selector = bb.getShort(0);
						if (selector == nv.getSelector()) {
							byte[] response = new byte[nv.getSize()];
							for (int i = 0; i < nv.getSize(); i++) {
								response[i] = londatapacket[11 + domainLenght + i];
							}
							nv.setData(response);
							haveresponse = true;
						}
					} else {
						if (index == nv.getIndex()) {
							byte[] response = new byte[nv.getSize()];
							for (int i = 0; i < nv.getSize(); i++) {
								response[i] = londatapacket[9 + domainLenght + i];
							}
							nv.setData(response);
							haveresponse = true;
						}

					}
				}
			}
			break;
		case TypeConstant.TPDU:
			byte tpdu = londatapacket[londatapacket.length - 3];
			tpdu = (byte) (tpdu & 0x30);
			if (tpdu == TypeConstant.ACK) {
				haveresponse = true;
			}
			break;
		}

	}

	public synchronized boolean haveResponse() {
		return haveresponse;
	}

}
