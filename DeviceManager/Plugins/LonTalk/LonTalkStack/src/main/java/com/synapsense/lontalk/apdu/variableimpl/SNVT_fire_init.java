package com.synapsense.lontalk.apdu.variableimpl;

import java.io.Serializable;

import com.synapsense.lontalk.apdu.variable.NetworkVariable;
import com.synapsense.lontalk.exception.FieldNotFoundException;
import com.synapsense.lontalk.exception.IncorrectValueException;

public class SNVT_fire_init implements NetworkVariable {

	private short selector;
	private short index;
	private byte input = 0;
	private double output = 0;
	private static final boolean ROUNDTRIP = false;

	// FI_NUL -1 Invalid value
	// FI_UNDEFINED 0 Initiator is undefined
	// FI_THERMAL_FIXED 1 Initiator is thermal fixed (heat)
	// FI_SMOKE_ION 2 Initiator is smoke and ion
	// FI_MULTI_ION_THERMAL 3 Initiator multi-ion and thermal
	// FI_SMOKE_PHOTO 4 Initiator is smoke and photo
	// FI_MULTI_PHOTO_THERMAL 5 Initiator is multi-photo and thermal
	// FI_MULTI_PHOTO_ION 6 Initiator is multi-photo and ion
	// FI_MULTI_PHOTO_ION_THERMAL 7 Initiator is multi-photo, ion and thermal
	// FI_THERMAL_ROR 8 Initiator is thermal fixed and Rate of Rise
	// FI_MULTI_THERMAL_ROR 9 Initiator is multi-thermal and Rate of Rise
	// FI_MANUAL_PULL 10 Initiator is manual pull
	// FI_MANUAL_FLOW 11 Initiator is water flow
	// FI_WATER_FLOW_TAMPER 12 Initiator is water flow and tamper
	// FI_STATUS_ONLY 13 Initiator is status only
	// FI_MANUAL_CALL 14 Initiator is a manual call point
	// FI_FIREMAN_CALL 15 Initiator is a fireman call point
	// FI_UNIVERSAL 16 General purpose initiator definition

	@Override
	public byte[] getData() {
		byte[] a = { input };
		return a;
	}

	@Override
	public void setData(byte[] newdata) {
		input = newdata[0];
		output = (double) input;

	}

	@Override
	public short getSelector() {
		return selector;
	}

	@Override
	public void setSelector(short selector) {
		this.selector = selector;

	}

	@Override
	public void setFieldByName(String name, Serializable value) throws FieldNotFoundException, IncorrectValueException {
		int i = 0;
		if (name.equalsIgnoreCase("fire_init")) {
			if ((Double) value < (double) -1 || (Double) value > (double) 16) {
				throw new IncorrectValueException("Value is not valid");
			}
			output = (Double) value;
			input = (byte) output;
			i++;
		}
		if (i == 0) {
			throw new FieldNotFoundException("Field is not found");
		}

	}

	@Override
	public Double getFieldByName(String name) throws FieldNotFoundException {
		if (name.equalsIgnoreCase("fire_init")) {
			return output;
		}
		throw new FieldNotFoundException("Field is not found");
	}

	@Override
	public short getIndex() {
		return index;
	}

	@Override
	public void setIndex(short index) {
		this.index = index;

	}

	@Override
	public int getSize() {
		return 1;
	}

	@Override
	public boolean isRoundtrip() {
		return ROUNDTRIP;
	}

}
