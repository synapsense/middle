package com.synapsense.lontalk.exception;

public class IncorrectValueException extends Exception {

	/**
     * 
     */
	private static final long serialVersionUID = 4299869211743760513L;

	public IncorrectValueException(String message) {
		super(message);
	}

	public IncorrectValueException(String msg, Throwable e) {
		super(msg, e);
	}

}
