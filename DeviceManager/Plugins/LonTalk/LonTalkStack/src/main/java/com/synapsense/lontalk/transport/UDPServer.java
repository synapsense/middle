package com.synapsense.lontalk.transport;

import java.net.InetSocketAddress;
import java.util.concurrent.Executors;

import org.apache.log4j.Logger;
import org.jboss.netty.bootstrap.ConnectionlessBootstrap;
import org.jboss.netty.channel.ChannelPipeline;
import org.jboss.netty.channel.ChannelPipelineFactory;
import org.jboss.netty.channel.Channels;
import org.jboss.netty.channel.FixedReceiveBufferSizePredictorFactory;
import org.jboss.netty.channel.socket.DatagramChannel;
import org.jboss.netty.channel.socket.DatagramChannelFactory;
import org.jboss.netty.channel.socket.nio.NioDatagramChannelFactory;

import com.synapsense.lontalk.apdu.variable.NetworkVariable;
import com.synapsense.lontalk.api.Network;
import com.synapsense.lontalk.node.CNPIPDevice;
import com.synapsense.lontalk.transport.coder.LonTalkDecoder;
import com.synapsense.lontalk.transport.coder.LonTalkEncoder;
import com.synapsense.lontalk.transport.handler.ResponseHandler;
import com.synapsense.lontalk.transport.handler.ServerHandler;

public class UDPServer {
	DatagramChannel c;
	ConnectionlessBootstrap b;
	ServerHandler handler;
	private static final Logger logger = Logger.getLogger(UDPServer.class);
	private Network network;
	boolean started = false;

	public UDPServer(Network network) {
		this.network = network;
	}

	public void start() {
		if (!started) {
			DatagramChannelFactory f = new NioDatagramChannelFactory(Executors.newCachedThreadPool());
			handler = new ServerHandler(network);
			b = new ConnectionlessBootstrap(f);
			b.setPipelineFactory(new ChannelPipelineFactory() {
				public ChannelPipeline getPipeline() throws Exception {
					return Channels.pipeline(new LonTalkEncoder(), new LonTalkDecoder(), handler);
				}
			});

			b.setOption("broadcast", "false");
			b.setOption("receiveBufferSizePredictorFactory", new FixedReceiveBufferSizePredictorFactory(1024));
			c = (DatagramChannel) b.bind(new InetSocketAddress(network.pluginPort));
			logger.debug("LonTalk stack is started");
			started = true;
		} else {
			logger.debug("LonTalk stack is started already");
		}

	}

	public NetworkVariable write(CNPIPDevice device, NetworkVariable nv, ResponseHandler responsehandler, byte[] data) {
		handler.setResponseHandler(null);
		handler.setResponseHandler(responsehandler);
		long currentTime = System.currentTimeMillis();
		c.write(data, new InetSocketAddress(device.getAddress(), device.getPort()));
		while ((System.currentTimeMillis() - currentTime) < device.getTimeout()) {
			if (handler.getResponseHandler().haveResponse()) {
				return nv;
			}
		}
		return null;
	}

	public void stop() {
		if (started) {
			c.close().awaitUninterruptibly();
			b.releaseExternalResources();
			while (c.isOpen()) {
				// /waiting for close
			}
			logger.debug("LonTalk stack is stoppped");
			started = false;
		} else {
			logger.debug("LonTalk stack is stopped already");
		}
	}

}
