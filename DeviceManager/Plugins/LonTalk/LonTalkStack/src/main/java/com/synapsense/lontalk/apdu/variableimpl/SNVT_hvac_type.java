package com.synapsense.lontalk.apdu.variableimpl;

import java.io.Serializable;

import com.synapsense.lontalk.apdu.variable.NetworkVariable;
import com.synapsense.lontalk.exception.FieldNotFoundException;
import com.synapsense.lontalk.exception.IncorrectValueException;

public class SNVT_hvac_type implements NetworkVariable {

	private short selector;
	private short index;
	private byte input = 0;
	private double output = 0;
	private static final boolean ROUNDTRIP = false;

	// HVT_NUL -1 Invalid Value
	// HVT_GENERIC 0 Generic
	// HVT_FAN_COIL 1 Fan Coil
	// HVT_VAV 2 Variable Air Volume Terminal
	// HVT_HEAT_PUMP 3 Heat Pump
	// HVT_ROOFTOP 4 Rooftop Unit
	// HVT_UNIT_VENT 5 Unit Ventilator
	// HVT_CHILL_CEIL 6 Chilled Ceiling
	// HVT_RADIATOR 7 Radiator
	// HVT_AHU 8 Air Handling Unit
	// HVT_SELF_CONT 9 Self-Contained Unit

	@Override
	public byte[] getData() {
		byte[] a = { input };
		return a;
	}

	@Override
	public void setData(byte[] newdata) {
		input = newdata[0];
		output = (double) input;

	}

	@Override
	public short getSelector() {
		return selector;
	}

	@Override
	public void setSelector(short selector) {
		this.selector = selector;

	}

	@Override
	public void setFieldByName(String name, Serializable value) throws FieldNotFoundException, IncorrectValueException {
		int i = 0;
		if (name.equalsIgnoreCase("hvac_type")) {
			if ((Double) value < (double) -1 || (Double) value > (double) 9) {
				throw new IncorrectValueException("Value is not valid");
			}
			output = (Double) value;
			input = (byte) output;
			i++;
		}
		if (i == 0) {
			throw new FieldNotFoundException("Field is not found");
		}

	}

	@Override
	public Double getFieldByName(String name) throws FieldNotFoundException {
		if (name.equalsIgnoreCase("hvac_type")) {
			return output;
		}
		throw new FieldNotFoundException("Field is not found");
	}

	@Override
	public short getIndex() {
		return index;
	}

	@Override
	public void setIndex(short index) {
		this.index = index;

	}

	@Override
	public int getSize() {
		return 1;
	}

	@Override
	public boolean isRoundtrip() {
		return ROUNDTRIP;
	}

}
