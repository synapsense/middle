package com.synapsense.lontalk.apdu.variable;

import java.io.Serializable;

import com.synapsense.lontalk.exception.FieldNotFoundException;
import com.synapsense.lontalk.exception.IncorrectValueException;

public interface NetworkVariable {

	public byte[] getData();

	public void setData(byte[] newdata);

	public short getSelector();

	public short getIndex();

	public void setIndex(short index);

	public void setSelector(short selector);

	public int getSize();

	public void setFieldByName(String name, Serializable value) throws FieldNotFoundException, IncorrectValueException;

	public Double getFieldByName(String name) throws FieldNotFoundException;

	public boolean isRoundtrip();
}
