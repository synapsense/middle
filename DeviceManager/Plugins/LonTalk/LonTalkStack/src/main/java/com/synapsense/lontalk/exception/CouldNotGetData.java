package com.synapsense.lontalk.exception;

public class CouldNotGetData extends Exception {

	/**
     * 
     */
	private static final long serialVersionUID = 5230550869339856108L;

	public CouldNotGetData(String message) {
		super(message);
	}

	public CouldNotGetData(String msg, Throwable e) {
		super(msg, e);
	}

}
