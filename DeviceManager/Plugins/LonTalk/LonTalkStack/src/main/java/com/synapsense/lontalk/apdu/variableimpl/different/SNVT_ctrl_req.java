package com.synapsense.lontalk.apdu.variableimpl.different;

import java.io.Serializable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import com.synapsense.lontalk.apdu.variable.NetworkVariable;
import com.synapsense.lontalk.exception.FieldNotFoundException;
import com.synapsense.lontalk.exception.IncorrectValueException;

public class SNVT_ctrl_req implements NetworkVariable {

	private short selector;
	private short index;

	private int receiveIDinput;
	private byte[] scalingReceiveID = { 1, 0, 0 };
	private double receiveIDOutput;

	private int senderIDinput;
	private byte[] scalingSenderID = { 1, 0, 0 };
	private double senderIDOutput;

	private short senderPrioinput;
	private byte[] scalingSenderPrio = { 1, 0, 0 };
	private double senderPrioOutput;

	private static final boolean ROUNDTRIP = true;

	@Override
	public byte[] getData() {
		byte[] a = new byte[5];
		a[0] = (byte) ((receiveIDinput >> 8) & 0xff);
		a[1] = (byte) (receiveIDinput & 0xff);
		a[2] = (byte) ((senderIDinput >> 8) & 0xff);
		a[3] = (byte) (senderIDinput & 0xff);
		a[4] = a[4] |= senderPrioinput;
		return a;
	}

	@Override
	public void setData(byte[] newdata) {
		ByteBuffer bb = ByteBuffer.allocate(4);
		bb.order(ByteOrder.LITTLE_ENDIAN);
		bb.put(newdata[1]);
		bb.put(newdata[0]);
		receiveIDinput = bb.getInt(0);
		receiveIDOutput = scalingReceiveID[0] * Math.pow(10, scalingReceiveID[1])
		        * (receiveIDinput + scalingReceiveID[2]);
		bb.clear();
		bb.put(newdata[3]);
		bb.put(newdata[2]);
		senderIDinput = bb.getInt(0);
		senderIDOutput = scalingSenderID[0] * Math.pow(10, scalingSenderID[1]) * (senderIDinput + scalingSenderID[2]);
		bb.clear();
		bb.put(newdata[4]);
		bb.put((byte) 0);
		senderPrioinput = bb.getShort(0);
		senderPrioOutput = scalingSenderPrio[0] * Math.pow(10, scalingSenderPrio[1])
		        * (senderPrioinput + scalingSenderPrio[2]);

	}

	@Override
	public short getSelector() {
		return selector;
	}

	@Override
	public void setSelector(short selector) {
		this.selector = selector;
	}

	@Override
	public void setFieldByName(String name, Serializable value) throws FieldNotFoundException, IncorrectValueException {
		int i = 0;
		if (name.equalsIgnoreCase("receiver_id")) {
			Double a = (Double) value;
			if (a < 1 || a > 65535) {
				throw new IncorrectValueException("Value is not valid");
			}
			receiveIDinput = (int) ((a / (scalingReceiveID[0] * Math.pow(10, scalingReceiveID[1]))) - scalingReceiveID[2]);
			receiveIDOutput = scalingReceiveID[0] * Math.pow(10, scalingReceiveID[1])
			        * (receiveIDinput + scalingReceiveID[2]);
			i++;
		}
		if (name.equalsIgnoreCase("sender_id")) {
			Double a = (Double) value;
			if (a < 1 || a > 65535) {
				throw new IncorrectValueException("Value is not valid");
			}
			senderIDinput = (int) ((a / (scalingSenderID[0] * Math.pow(10, scalingSenderID[1]))) - scalingSenderID[2]);
			senderIDOutput = scalingSenderID[0] * Math.pow(10, scalingSenderID[1])
			        * (senderIDinput + scalingSenderID[2]);
			i++;
		}
		if (name.equalsIgnoreCase("sender_prio")) {
			Double a = (Double) value;
			if (a < 0 || a > 200) {
				throw new IncorrectValueException("Value is not valid");
			}
			senderPrioinput = (short) ((a / (scalingSenderPrio[0] * Math.pow(10, scalingSenderPrio[1]))) - scalingSenderPrio[2]);
			senderPrioOutput = scalingSenderPrio[0] * Math.pow(10, scalingSenderPrio[1])
			        * (senderPrioinput + scalingSenderPrio[2]);
			i++;
		}
		if (i == 0) {
			throw new FieldNotFoundException("Field is not found");
		}

	}

	@Override
	public Double getFieldByName(String name) throws FieldNotFoundException {
		if (name.equalsIgnoreCase("receiver_id")) {
			return receiveIDOutput;
		}
		if (name.equalsIgnoreCase("sender_id")) {
			return senderIDOutput;
		}
		if (name.equalsIgnoreCase("sender_prio")) {
			return senderPrioOutput;
		}
		throw new FieldNotFoundException("Field is not found");
	}

	@Override
	public short getIndex() {
		return index;
	}

	@Override
	public void setIndex(short index) {
		this.index = index;

	}

	@Override
	public int getSize() {
		return 5;
	}

	@Override
	public boolean isRoundtrip() {
		return ROUNDTRIP;
	}

}
