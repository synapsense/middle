package com.synapsense.lontalk.network;

import com.synapsense.lontalk.transaction.SessionId;

public class SessionDataUnit {

	public static byte getRequestData() {
		SessionId id = SessionId.getInstance();
		byte session = id.getTransactionId();
		return session;
	}

	public static byte getACKDData() {
		SessionId id = SessionId.getInstance();
		byte session = id.getTransactionId();
		return session;
	}

}
