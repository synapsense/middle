package com.synapsense.lontalk.exception;

public class FieldNotFoundException extends Exception {

	/**
     * 
     */
	private static final long serialVersionUID = -3572282819615314144L;

	public FieldNotFoundException(String message) {
		super(message);
	}

	public FieldNotFoundException(String msg, Throwable e) {
		super(msg, e);
	}

}
