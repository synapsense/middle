package com.synapsense.lontalk.address;

//unicast: message, reminder or acknowledgment
public class AddressType2a {
	public byte srcSubnet = 1;
	public byte srcNode = 1;
	public byte dstSubnet;
	public byte dstNode;

	public AddressType2a(byte srcSubnet, byte srcNode, byte dstSubnet, byte dstNode) {
		this.srcSubnet = srcSubnet;
		this.srcNode = srcNode;
		this.dstSubnet = dstSubnet;
		this.dstNode = dstNode;
	}

	public byte[] getAddress() {
		srcNode = srcNode |= (-128);
		dstNode = dstNode |= (-128);
		byte[] address = { srcSubnet, srcNode, dstSubnet, dstNode };
		return address;
	}

	public void setSrcSubnet(byte a) {
		srcSubnet = a;
	}

	public void setSrcNode(byte a) {
		srcNode = a;
	}

	public void setDstSubnet(byte a) {
		dstSubnet = a;
	}

	public void setDstNode(byte a) {
		dstNode = a;
	}

	public byte getSrcSubnet() {
		return srcSubnet;
	}

	public byte getSrcNode() {
		return srcNode;
	}

	public byte getDstSubnet() {
		return dstSubnet;
	}

	public byte getDstNode() {
		return dstNode;
	}

}
