package com.synapsense.lontalk.apdu.variableimpl;

import java.io.Serializable;

import com.synapsense.lontalk.apdu.variable.NetworkVariable;
import com.synapsense.lontalk.exception.FieldNotFoundException;
import com.synapsense.lontalk.exception.IncorrectValueException;

public class SNVT_defr_term implements NetworkVariable {

	private short selector;
	private short index;
	private byte input = 0;
	private double output = 0;
	private static final boolean ROUNDTRIP = false;

	// DFT_NUL -1 Invalid Value
	// DFT_TERM_TEMP 0 Terminate on temperature
	// DFT_TERM_TIME 1 Terminate on time
	// DFT_TERM_FIRST 2 Terminate on first occurring
	// DFT_TERM_LAST 3 Terminate on last occurring
	// DFT_TERM_SENSOR 4 Terminate on sensor
	// DFT_TERM_DISCHARGE 5 Terminate on discharge
	// DFT_TERM_RETURN 6 Terminate on return
	// DFT_TERM_SW_OPEN 7 Terminate on "Switch Open"
	// DFT_TERM_SW_CLOSE 8 Terminate on "Switch Closed"
	// DFT_TERM_MANUF 100 Manufacturer-Defined termination state

	@Override
	public byte[] getData() {
		byte[] a = { input };
		return a;
	}

	@Override
	public void setData(byte[] newdata) {
		input = newdata[0];
		output = (double) input;

	}

	@Override
	public short getSelector() {
		return selector;
	}

	@Override
	public void setSelector(short selector) {
		this.selector = selector;

	}

	@Override
	public void setFieldByName(String name, Serializable value) throws FieldNotFoundException, IncorrectValueException {
		int i = 0;
		if (name.equalsIgnoreCase("defr_term")) {
			if ((Double) value != 100) {
				if (((Double) value < (double) -1 || (Double) value > (double) 8)) {
					throw new IncorrectValueException("Value is not valid");
				}
			}
			output = (Double) value;
			input = (byte) output;
			i++;
		}
		if (i == 0) {
			throw new FieldNotFoundException("Field is not found");
		}
	}

	@Override
	public Double getFieldByName(String name) throws FieldNotFoundException {
		if (name.equalsIgnoreCase("defr_term")) {
			return output;
		}
		throw new FieldNotFoundException("Field is not found");
	}

	@Override
	public short getIndex() {
		return index;
	}

	@Override
	public void setIndex(short index) {
		this.index = index;

	}

	@Override
	public int getSize() {
		return 1;
	}

	@Override
	public boolean isRoundtrip() {
		return ROUNDTRIP;
	}

}
