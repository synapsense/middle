package com.synapsense.lontalk.node;

import java.util.HashMap;

public class CNPIPDevice {

	private String address;
	private int lonport;
	private int timeout;
	private int retries;

	private HashMap<String, LonTalkNode> nodes = new HashMap<String, LonTalkNode>();

	public CNPIPDevice(String address, int port, int timeout, int retries) {
		this.setAddress(address);
		this.setPort(port);
		this.setTimeout(timeout);
		this.setRetries(retries);
	}

	public void addNode(String name, LonTalkNode node) {
		nodes.put(name, node);
	}

	public LonTalkNode getNode(String name) {
		return nodes.get(name);
	}

	public void removeNode(String name) {
		nodes.remove(name);
	}

	public void removeAllNode() {
		nodes.clear();
	}

	public CNPIPDevice(String address) {
		this.address = address;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getPort() {
		return lonport;
	}

	public void setPort(int port) {
		this.lonport = port;
	}

	public int getTimeout() {
		return timeout;
	}

	public void setTimeout(int timeout) {
		this.timeout = timeout;
	}

	public int getRetries() {
		return retries;
	}

	public void setRetries(int retries) {
		this.retries = retries;
	}

}
