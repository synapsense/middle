package com.synapsense.lontalk.address;

//multicast: Message or reminder
public class AddressType1 {
	public byte srcSubnet;
	public byte srcNode;
	public byte dstGroup;

	public AddressType1(byte srcSubnet, byte srcNode, byte dstGroup) {
		this.srcSubnet = srcSubnet;
		this.srcNode = srcNode;
		this.dstGroup = dstGroup;
	}

	public byte[] getAddress() {
		srcNode = srcNode |= (-128);
		byte[] address = { srcSubnet, srcNode, dstGroup };
		return address;
	}

	public void setSrcSubnet(byte a) {
		srcSubnet = a;
	}

	public void setSrcNode(byte a) {
		srcNode = a;
	}

	public void setDstGroup(byte a) {
		dstGroup = a;
	}

	public byte getSrcSubnet() {
		return srcSubnet;
	}

	public byte getSrcNode() {
		return srcNode;
	}

	public byte getDstGroup() {
		return dstGroup;
	}
}
