package com.synapsense.lontalk.apdu.variableimpl.different;

import java.io.Serializable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import com.synapsense.lontalk.apdu.variable.NetworkVariable;
import com.synapsense.lontalk.exception.FieldNotFoundException;
import com.synapsense.lontalk.exception.IncorrectValueException;

public class SNVT_setting implements NetworkVariable {

	private short selector;
	private short index;

	private byte function;
	private double functionoutput;

	private short setting;
	private byte[] settingscale = { 5, -1, 0 };
	private double settingoutput;

	private short rotation;
	private byte[] rotationscale = { 2, -2, 0 };
	private double rotationoutput;

	private static final boolean ROUNDTRIP = true;

	@Override
	public byte[] getData() {
		byte[] a = new byte[4];
		a[0] = function;
		a[1] = (byte) (0x00FF & setting);
		a[2] = (byte) ((rotation >> 8) & 0xff);
		a[3] = (byte) (rotation & 0xff);
		return a;
	}

	@Override
	public void setData(byte[] newdata) {
		function = newdata[0];
		functionoutput = function;
		setting = 0;
		setting = (short) (0x00FF & newdata[1]);
		settingoutput = settingscale[0] * Math.pow(10, settingscale[1]) * (setting + settingscale[2]);
		ByteBuffer bb = ByteBuffer.allocate(2);
		bb.order(ByteOrder.LITTLE_ENDIAN);
		bb.put(newdata[3]);
		bb.put(newdata[2]);
		rotation = bb.getShort(0);
		rotationoutput = rotationscale[0] * Math.pow(10, rotationscale[1]) * (rotation + rotationscale[2]);
	}

	@Override
	public short getSelector() {
		return selector;
	}

	@Override
	public void setSelector(short selector) {
		this.selector = selector;
	}

	@Override
	public void setFieldByName(String name, Serializable value) throws FieldNotFoundException, IncorrectValueException {
		int i = 0;
		if (name.equalsIgnoreCase("function")) {
			Double a = (Double) value;
			if (a < -1 || a > 5) {
				throw new IncorrectValueException("Value is not valid");
			}
			functionoutput = a;
			function = (byte) functionoutput;
			i++;
		}
		if (name.equalsIgnoreCase("setting")) {
			Double a = (Double) value;
			if (a < 0 || a > 100) {
				throw new IncorrectValueException("Value is not valid");
			}
			Double settingoutput = a;
			setting = (short) ((settingoutput / (settingscale[0] * Math.pow(10, settingscale[1]))) - settingscale[2]);
			i++;
		}
		if (name.equalsIgnoreCase("rotation")) {
			Double a = (Double) value;
			if (a < -359.98 || a > 360) {
				throw new IncorrectValueException("Value is not valid");
			}
			Double rotationoutput = a;
			rotation = (short) ((rotationoutput / (rotationscale[0] * Math.pow(10, rotationscale[1]))) - rotationscale[2]);
			i++;
		}
		if (i == 0) {
			throw new FieldNotFoundException("Field is not found");
		}

	}

	@Override
	public Double getFieldByName(String name) throws FieldNotFoundException {
		if (name.equalsIgnoreCase("function")) {
			return functionoutput;
		}
		if (name.equalsIgnoreCase("setting")) {
			return settingoutput;
		}
		if (name.equalsIgnoreCase("rotation")) {
			return rotationoutput;
		}
		throw new FieldNotFoundException("Field is not found");
	}

	@Override
	public short getIndex() {
		return index;
	}

	@Override
	public void setIndex(short index) {
		this.index = index;

	}

	@Override
	public int getSize() {
		return 4;
	}

	@Override
	public boolean isRoundtrip() {
		return ROUNDTRIP;
	}

}
