package com.synapsense.lontalk.packet.unused;

import java.net.InetAddress;
import java.net.UnknownHostException;

import com.synapsense.lontalk.node.CNPIPDevice;
import com.synapsense.lontalk.packet.header.CNPIPHeader;
import com.synapsense.lontalk.transaction.SessionId;

///unused
public class MembershipPacket {

	private int datetime = 0;
	private byte reason = 0x00;
	private short requestID;
	private byte segmentID = 0;
	private int sinceDatetime = 0;
	private byte[] unicastAddress;

	public byte[] getMembershipPacket(CNPIPDevice device) throws UnknownHostException {
		CNPIPHeader cnpipheader = new CNPIPHeader();
		cnpipheader.setMembershipType();
		cnpipheader.setDataPacketLenght((short) 36);

		requestID = SessionId.getInstance().getRequestId();
		unicastAddress = InetAddress.getByName(device.getAddress()).getAddress();
		byte[] packet = new byte[36];
		byte[] header = cnpipheader.getHeader();
		for (int i = 0; i < header.length; i++) {
			packet[i] = header[i];
		}
		packet[20] = packet[20] |= (datetime >>> 24);
		packet[21] = packet[21] |= (datetime >>> 16);
		packet[22] = packet[22] |= (datetime >>> 8);
		packet[23] = packet[23] |= datetime;
		packet[24] = reason;
		packet[25] = (byte) ((requestID >> 8) & 0Xff);
		packet[26] = (byte) (requestID & 0xFF);
		packet[27] = segmentID;
		packet[28] = packet[28] |= (sinceDatetime >>> 24);
		packet[29] = packet[29] |= (sinceDatetime >>> 16);
		packet[30] = packet[30] |= (sinceDatetime >>> 8);
		packet[31] = packet[31] |= sinceDatetime;
		packet[32] = unicastAddress[0];
		packet[33] = unicastAddress[1];
		packet[34] = unicastAddress[2];
		packet[35] = unicastAddress[3];
		return packet;
	}
}
