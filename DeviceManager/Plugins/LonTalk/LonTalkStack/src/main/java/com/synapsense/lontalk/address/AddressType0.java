package com.synapsense.lontalk.address;

//broadcast: Domain wide or by individual subnet
public class AddressType0 {

	public byte srcSubnet;
	public byte srcNode;
	public byte dstSubnet;

	public AddressType0(byte srcSubnet, byte srcNode, byte dstSubnet) {
		this.srcSubnet = srcSubnet;
		this.srcNode = srcNode;
		this.dstSubnet = dstSubnet;
	}

	public byte[] getAddress() {
		srcNode = srcNode |= (-128);
		byte[] address = { srcSubnet, srcNode, dstSubnet };
		return address;
	}

	public void setSrcSubnet(byte a) {
		srcSubnet = a;
	}

	public void setSrcNode(byte a) {
		srcNode = a;
	}

	public void setDstSubnet(byte a) {
		dstSubnet = a;
	}

	public byte getSrcSubnet() {
		return srcSubnet;
	}

	public byte getSrcNode() {
		return srcNode;
	}

	public byte getDstNode() {
		return dstSubnet;
	}

}