package com.synapsense.lontalk.apdu.variableimpl.different;

import java.io.Serializable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import com.synapsense.lontalk.apdu.variable.NetworkVariable;
import com.synapsense.lontalk.exception.FieldNotFoundException;
import com.synapsense.lontalk.exception.IncorrectValueException;

public class SNVT_hvac_overid implements NetworkVariable {

	private short selector;
	private short index;

	private byte state;
	private double outputstate;

	private short percent;
	private byte[] scalingpercent = { 5, -3, 0 };
	private double percentOutput;

	private int flow;
	private byte[] scalingflow = { 1, 0, 0 };
	private double flowoutput;

	private static final boolean ROUNDTRIP = true;

	@Override
	public byte[] getData() {
		byte[] a = new byte[5];
		a[0] = state;
		a[1] = (byte) ((percent >> 8) & 0xff);
		a[2] = (byte) (percent & 0xff);
		a[3] = (byte) ((flow >> 8) & 0xff);
		a[4] = (byte) (flow & 0xff);
		return a;
	}

	@Override
	public void setData(byte[] newdata) {
		state = newdata[0];
		outputstate = state;
		ByteBuffer bb = ByteBuffer.allocate(4);
		bb.order(ByteOrder.LITTLE_ENDIAN);
		bb.put(newdata[2]);
		bb.put(newdata[1]);
		percent = bb.getShort(0);
		percentOutput = scalingpercent[0] * Math.pow(10, scalingpercent[1]) * (percent + scalingpercent[2]);
		bb.clear();
		bb.put(newdata[4]);
		bb.put(newdata[3]);
		flow = bb.getInt(0);
		flowoutput = scalingflow[0] * Math.pow(10, scalingflow[1]) * (flow + scalingflow[2]);

	}

	@Override
	public short getSelector() {
		return selector;
	}

	@Override
	public void setSelector(short selector) {
		this.selector = selector;

	}

	@Override
	public void setFieldByName(String name, Serializable value) throws FieldNotFoundException, IncorrectValueException {
		int i = 0;
		if (name.equalsIgnoreCase("state")) {
			Double a = (Double) value;
			if (a < -1 || a > 48) {
				throw new IncorrectValueException("Value is not valid");
			}
			outputstate = a;
			state = (byte) outputstate;
			i++;
		}
		if (name.equalsIgnoreCase("percent")) {
			Double a = (Double) value;
			if (a < -163.840 || a >= 163.835) {
				throw new IncorrectValueException("Value is not valid");
			}
			percent = (short) ((a / (scalingpercent[0] * Math.pow(10, scalingpercent[1]))) - scalingpercent[2]);
			percentOutput = scalingpercent[0] * Math.pow(10, scalingpercent[1]) * (percent + scalingpercent[2]);
			i++;
		}
		if (name.equalsIgnoreCase("flow")) {
			Double a = (Double) value;
			if (a < 0 || a >= 65535) {
				throw new IncorrectValueException("Value is not valid");
			}
			flow = (short) ((a / (scalingflow[0] * Math.pow(10, scalingflow[1]))) - scalingflow[2]);
			flowoutput = scalingflow[0] * Math.pow(10, scalingflow[1]) * (flow + scalingflow[2]);
			i++;
		}
		if (i == 0) {
			throw new FieldNotFoundException("Field is not found");
		}

	}

	@Override
	public Double getFieldByName(String name) throws FieldNotFoundException {
		if (name.equalsIgnoreCase("state")) {
			return outputstate;
		}
		if (name.equalsIgnoreCase("percent")) {
			return percentOutput;
		}
		if (name.equalsIgnoreCase("flow")) {
			return flowoutput;
		}
		throw new FieldNotFoundException("Field is not found");
	}

	@Override
	public short getIndex() {
		return index;
	}

	@Override
	public void setIndex(short index) {
		this.index = index;

	}

	@Override
	public int getSize() {
		return 5;
	}

	@Override
	public boolean isRoundtrip() {
		return ROUNDTRIP;
	}

}
