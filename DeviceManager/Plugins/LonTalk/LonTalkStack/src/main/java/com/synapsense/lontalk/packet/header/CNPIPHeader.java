package com.synapsense.lontalk.packet.header;

import com.synapsense.lontalk.transaction.SessionId;

public class CNPIPHeader {

	public static final int SIZE = 20;
	public short dataPacketLenght;

	private byte version = 0x01;
	private byte packetType = 0x01;
	private byte extendedHeaderSize = 0;
	private byte protocolFlags = 0x00;
	private short vendorCode = 0;
	private int sessionId;
	private int sequenceNumber;
	private int timeStamp = 0;

	public byte[] getHeader() {
		SessionId s = SessionId.getInstance();
		sessionId = s.getSessionID();
		sequenceNumber = s.getSequenceNumber();

		byte[] header = new byte[20];
		header[0] = (byte) ((dataPacketLenght >>> 8) & 0xFF);
		header[1] = (byte) (dataPacketLenght & 0xFF);
		header[2] = version;
		header[3] = packetType;
		header[4] = extendedHeaderSize;
		header[5] = protocolFlags;
		header[6] = (byte) ((vendorCode >>> 8) & 0Xff);
		header[7] = (byte) (vendorCode & 0xFF);
		header[8] = header[8] |= (sessionId >>> 24);
		header[9] = header[9] |= (sessionId >>> 16);
		header[10] = header[10] |= (sessionId >>> 8);
		header[11] = header[11] |= sessionId;
		header[12] = header[12] |= (sequenceNumber >>> 24);
		header[13] = header[13] |= (sequenceNumber >>> 16);
		header[14] = header[14] |= (sequenceNumber >>> 8);
		header[15] = header[15] |= sequenceNumber;
		header[16] = header[16] |= (timeStamp >>> 24);
		header[17] = header[17] |= (timeStamp >>> 16);
		header[18] = header[18] |= (timeStamp >>> 8);
		header[19] = header[19] |= timeStamp;
		return header;
	}

	public void setDataPacketLenght(short dataPacketLenght) {
		this.dataPacketLenght = dataPacketLenght;
	}

	public void setAuthentication() {
		protocolFlags = 0x11;
	}

	public void setNonAuthentication() {
		protocolFlags = 0x1;
	}

	public void setDataType() {
		packetType = 0x01;
	}

	public void setMembershipType() {
		packetType = 0x64;
	}

	public void setRegistrationType() {
		packetType = 0x03;
	}

	public void setConfigurationType() {
		packetType = 0x63;
	}

	public void setRoutingType() {
		packetType = 0x08;
	}

	public void setAcknowledge() {
		packetType = 0x07;
	}

}
