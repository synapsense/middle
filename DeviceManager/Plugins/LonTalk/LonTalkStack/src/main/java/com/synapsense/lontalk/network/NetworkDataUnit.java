package com.synapsense.lontalk.network;

import com.synapsense.lontalk.address.AddressType2a;
import com.synapsense.lontalk.address.AddressType3;
import com.synapsense.lontalk.api.Network;
import com.synapsense.lontalk.node.LonTalkNode;

public class NetworkDataUnit {
	private LonTalkNode node;
	private Network network;
	private byte ppdu = 0x01;
	private byte npdu;
	private byte addrFmt;

	private byte[] domain;

	public NetworkDataUnit(Network network, LonTalkNode node) {
		this.node = node;
		this.network = network;
		this.domain = network.getDomainBytes();
	}

	public byte[] getData() {
		byte[] data = null;

		switch (addrFmt) {
		case 0x8: {
			data = new byte[6 + network.domainLength];
			data[0] = ppdu;
			data[1] = npdu |= addrFmt;
			switch (network.domainLength) {
			case 1:
				data[1] = data[1] |= 0x01;
				data[6] = domain[0];
				break;
			case 3:
				data[1] = data[1] |= 0x02;
				data[6] = domain[0];
				data[7] = domain[1];
				data[8] = domain[2];
				break;
			case 6:
				data[1] = data[1] |= 0x06;
				data[6] = domain[0];
				data[7] = domain[1];
				data[8] = domain[2];
				data[9] = domain[3];
				data[10] = domain[4];
				data[11] = domain[5];
				break;
			}

			AddressType2a address = new AddressType2a(network.pluginSubnet, network.pluginNode, node.getSubnet(),
			        node.getNode());
			byte[] adr = address.getAddress();
			for (int i = 0; i < adr.length; i++) {
				data[i + 2] = adr[i];
			}
			break;
		}
		case 0xC: {
			data = new byte[11 + network.domainLength];
			data[0] = ppdu;
			data[1] = npdu |= addrFmt;
			switch (network.domainLength) {
			case 1:
				data[1] = data[1] |= 0x01;
				data[11] = domain[0];
				break;
			case 3:
				data[1] = data[1] |= 0x02;
				data[11] = domain[0];
				data[12] = domain[1];
				data[13] = domain[2];
				break;
			case 6:
				data[1] = data[1] |= 0x06;
				data[11] = domain[0];
				data[12] = domain[1];
				data[13] = domain[2];
				data[14] = domain[3];
				data[15] = domain[4];
				data[16] = domain[5];
				break;
			}

			AddressType3 address = new AddressType3(network.pluginSubnet, network.pluginNode, node.getSubnet(),
			        node.getNeuronId());
			byte[] adr = address.getAddress();
			for (int i = 0; i < adr.length; i++) {
				data[i + 2] = adr[i];
			}
			break;
		}
		default: {
			data = new byte[6 + network.domainLength];
			data[0] = ppdu;
			data[1] = npdu |= addrFmt;
			switch (network.domainLength) {
			case 1:
				data[1] = data[1] |= 0x01;
				data[6] = domain[0];
				break;
			case 3:
				data[1] = data[1] |= 0x02;
				data[6] = domain[0];
				data[7] = domain[1];
				data[8] = domain[2];
				break;
			case 6:
				data[1] = data[1] |= 0x06;
				data[6] = domain[0];
				data[7] = domain[1];
				data[8] = domain[2];
				data[9] = domain[3];
				data[10] = domain[4];
				data[11] = domain[5];
				break;
			}

			AddressType2a address = new AddressType2a(network.pluginSubnet, network.pluginNode, node.getSubnet(),
			        node.getNode());
			byte[] adr = address.getAddress();
			for (int i = 0; i < adr.length; i++) {
				data[i + 2] = adr[i];
			}
			break;
		}
		}
		return data;
	}

	public void setAddressType2() {
		addrFmt = 0x8;
	}

	public void setAddressType3() {
		addrFmt = 0xC;
	}

	public void setSPDUFormat() {
		npdu = (byte) (npdu & 0xcf);
		npdu = npdu |= 0x10;
	}

	public void setTPDUFormat() {
		npdu = (byte) (npdu & 0xcf);
	}
}
