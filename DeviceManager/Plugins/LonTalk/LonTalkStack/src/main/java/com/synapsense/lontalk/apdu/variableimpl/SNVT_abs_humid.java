/**
 * 
 */
package com.synapsense.lontalk.apdu.variableimpl;

import java.io.Serializable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import com.synapsense.lontalk.apdu.variable.NetworkVariable;
import com.synapsense.lontalk.exception.FieldNotFoundException;
import com.synapsense.lontalk.exception.IncorrectValueException;

/**
 * @author isavinov
 * 
 */
public class SNVT_abs_humid implements NetworkVariable {

	private short selector;
	private short index;
	private byte[] scaling = { 1, -2, 0 };
	private int inputabshumid = 0;
	private double outputabshumid = 0;
	private static final boolean ROUNDTRIP = false;

	@Override
	public short getSelector() {
		return selector;
	}

	@Override
	public byte[] getData() {
		byte[] a = new byte[2];
		a[1] = a[1] |= (inputabshumid);
		a[0] = a[0] |= (inputabshumid >> 8);
		return a;
	}

	@Override
	public void setData(byte[] newdata) {
		ByteBuffer bb = ByteBuffer.allocate(4);
		bb.order(ByteOrder.LITTLE_ENDIAN);
		bb.put(newdata[1]);
		bb.put(newdata[0]);
		bb.put((byte) 0);
		bb.put((byte) 0);
		inputabshumid = bb.getInt(0);
		outputabshumid = scaling[0] * Math.pow(10, scaling[1]) * (inputabshumid + scaling[2]);
	}

	@Override
	public void setSelector(short selector) {
		this.selector = selector;
	}

	@Override
	public void setFieldByName(String name, Serializable value) throws FieldNotFoundException, IncorrectValueException {
		if (name.equalsIgnoreCase("abs_humid")) {
			Double a = (Double) value;
			if (a < 0 || a >= 655.35) {
				throw new IncorrectValueException("Value is not valid");
			}
			inputabshumid = (int) ((a / (scaling[0] * Math.pow(10, scaling[1]))) - scaling[2]);
			outputabshumid = scaling[0] * Math.pow(10, scaling[1]) * (inputabshumid + scaling[2]);
		} else {
			throw new FieldNotFoundException("Field is not found");
		}

	}

	@Override
	public Double getFieldByName(String name) throws FieldNotFoundException {
		if (name.equalsIgnoreCase("abs_humid")) {
			return outputabshumid;
		}
		throw new FieldNotFoundException("Field is not found");
	}

	@Override
	public short getIndex() {
		return index;
	}

	@Override
	public void setIndex(short index) {
		this.index = index;

	}

	@Override
	public int getSize() {
		return 2;
	}

	@Override
	public boolean isRoundtrip() {
		return ROUNDTRIP;
	}

}
