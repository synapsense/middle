package com.synapsense.lontalk.transport;

public interface PacketNameConstant {
	byte DATA_PACKET = 0x01;
	byte DEVICE_REGISTRATION_REQUEST = 0x63;
	byte CHANNEL_ROUTING_REQUEST = 0x68;
	byte CHANNEL_MEMBERSHIP = 0x04;

}
