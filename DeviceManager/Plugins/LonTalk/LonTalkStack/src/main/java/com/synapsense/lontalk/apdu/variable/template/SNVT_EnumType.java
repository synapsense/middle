package com.synapsense.lontalk.apdu.variable.template;

import java.io.Serializable;

import com.synapsense.lontalk.apdu.variable.NetworkVariable;
import com.synapsense.lontalk.exception.FieldNotFoundException;

public class SNVT_EnumType implements NetworkVariable {

	private short selector;
	private short index;
	private byte input = 0;
	private double output = 0;
	private boolean roundtrip = false;

	@Override
	public byte[] getData() {
		byte[] a = { input };
		return a;
	}

	@Override
	public void setData(byte[] newdata) {
		input = newdata[0];
		output = (double) input;

	}

	@Override
	public short getSelector() {
		return selector;
	}

	@Override
	public void setSelector(short selector) {
		this.selector = selector;

	}

	@Override
	public void setFieldByName(String name, Serializable value) throws FieldNotFoundException {
		input = 0;
		input = input |= (Integer) value;
		output = (double) input;

	}

	@Override
	public Double getFieldByName(String name) throws FieldNotFoundException {
		return output;
	}

	@Override
	public void setIndex(short index) {
		this.index = index;
	}

	@Override
	public short getIndex() {
		return index;
	}

	@Override
	public int getSize() {
		return 1;
	}

	@Override
	public boolean isRoundtrip() {
		return roundtrip;
	}

}
