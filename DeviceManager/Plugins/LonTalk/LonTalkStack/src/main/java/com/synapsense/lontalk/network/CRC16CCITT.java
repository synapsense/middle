package com.synapsense.lontalk.network;

public class CRC16CCITT {

	public static byte[] getCRC(byte[] data) {
		int crc = 0xFFFF;
		int polynomial = 0x1021;

		for (int i = 0; i < data.length; i++) {
			byte b = data[i];
			for (int j = 0; j < 8; j++) {
				boolean c15 = ((crc & 0x8000) == 0x8000);
				boolean bit = ((b & 0x80) == 0x80);
				crc <<= 1;
				if (c15 != bit) {
					crc = crc ^ polynomial;
				}
				b = (byte) (b << 1);
			}
		}
		crc = crc ^ 0xFFFF;
		crc = crc & 0xFFFF;
		byte[] crcBytes = new byte[2];
		crcBytes[0] = crcBytes[0] |= (crc >>> 8);
		crcBytes[1] = crcBytes[1] |= (crc);
		return crcBytes;
	}

}
