package com.synapsense.lontalk.transaction;

import java.util.Random;

public class SessionId {
	private static Random random = new Random();
	private static volatile SessionId instance;
	private static int session_id;
	private int sequence_number = 0x01;

	private short requestId = 1;
	private byte transactionId = 1;

	private SessionId() {

	}

	public static SessionId getInstance() {
		if (instance == null)
			synchronized (SessionId.class) {
				if (instance == null) {
					instance = new SessionId();
					session_id = random.nextInt();
				}
			}
		return instance;
	}

	public int getSequenceNumber() {
		if (sequence_number == 0xFFFFFFFF) {
			session_id = random.nextInt();
			sequence_number = 0x1;
			return 0xFFFFFFFF;
		}
		int a = sequence_number;
		sequence_number++;
		return a;
	}

	public int getSessionID() {
		return session_id;
	}

	public short getRequestId() {
		requestId++;
		return requestId;
	}

	public byte getTransactionId() {
		transactionId++;
		transactionId = (byte) (transactionId & 0x0F);
		if (transactionId == 0) {
			transactionId++;
		}
		return transactionId;
	}

}
