package com.synapsense.lontalk.exception;

public class IncorrectFormatException extends Exception {

	/**
     * 
     */
	private static final long serialVersionUID = -2250445907579924006L;

	public IncorrectFormatException(String message) {
		super(message);
	}

	public IncorrectFormatException(String msg, Throwable e) {
		super(msg, e);
	}

}
