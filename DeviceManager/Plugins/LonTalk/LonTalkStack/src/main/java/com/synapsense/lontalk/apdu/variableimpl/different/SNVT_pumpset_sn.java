package com.synapsense.lontalk.apdu.variableimpl.different;

import java.io.Serializable;

import com.synapsense.lontalk.apdu.variable.NetworkVariable;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_flow_mil;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_press;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_press_f;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_temp;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_volt;
import com.synapsense.lontalk.exception.FieldNotFoundException;
import com.synapsense.lontalk.exception.IncorrectValueException;

public class SNVT_pumpset_sn implements NetworkVariable {

	private short selector;
	private short index;

	private SNVT_flow_mil total_dilution_flow;
	private SNVT_temp exhaust_temperature;
	private SNVT_press exhaust_pressure;
	private SNVT_press shaft_seal_purge_pressure;
	private SNVT_press_f inlet_vacuum;
	private SNVT_volt supply_voltage;
	private SNVT_flow_mil coolant_flow;
	private byte coolant_flow_low_input;
	private double coolant_flow_low_output;
	private byte dilution_active_input;
	private double dilution_active_output;
	private byte ballast_dilution_active_input;
	private double ballast_dilution_active_output;
	private byte inlet_purge_dilution_active_input;
	private double inlet_purge_dilution_active_output;
	private byte exhaust_dilution_active_input;
	private double exhaust_dilution_active_output;
	private byte dilution_flow_out_of_range_input;
	private double dilution_flow_out_of_range_output;
	private byte power_supply_on_input;
	private double power_supply_on_output;

	private static final boolean ROUNDTRIP = true;

	public SNVT_pumpset_sn() {
		total_dilution_flow = new SNVT_flow_mil();
		exhaust_temperature = new SNVT_temp();
		exhaust_pressure = new SNVT_press();
		shaft_seal_purge_pressure = new SNVT_press();
		inlet_vacuum = new SNVT_press_f();
		supply_voltage = new SNVT_volt();
		coolant_flow = new SNVT_flow_mil();
	}

	@Override
	public byte[] getData() {
		byte[] data = new byte[23];
		byte[] snvt;
		snvt = total_dilution_flow.getData();
		data[0] = snvt[0];
		data[1] = snvt[1];
		snvt = exhaust_temperature.getData();
		data[2] = snvt[0];
		data[3] = snvt[1];
		snvt = exhaust_pressure.getData();
		data[4] = snvt[0];
		data[5] = snvt[1];
		snvt = shaft_seal_purge_pressure.getData();
		data[6] = snvt[0];
		data[7] = snvt[1];
		snvt = inlet_vacuum.getData();
		data[8] = snvt[0];
		data[9] = snvt[1];
		data[10] = snvt[2];
		data[11] = snvt[3];
		snvt = supply_voltage.getData();
		data[12] = snvt[0];
		data[13] = snvt[1];
		snvt = coolant_flow.getData();
		data[14] = snvt[0];
		data[15] = snvt[1];
		data[16] = coolant_flow_low_input;
		data[17] = dilution_active_input;
		data[18] = ballast_dilution_active_input;
		data[19] = inlet_purge_dilution_active_input;
		data[20] = exhaust_dilution_active_input;
		data[21] = dilution_flow_out_of_range_input;
		data[22] = power_supply_on_input;
		return data;
	}

	@Override
	public void setData(byte[] newdata) {
		byte[] snvt = new byte[2];
		byte[] snvt_f = new byte[4];
		snvt[0] = newdata[0];
		snvt[1] = newdata[1];
		total_dilution_flow.setData(snvt);
		snvt[0] = newdata[2];
		snvt[1] = newdata[3];
		exhaust_temperature.setData(snvt);
		snvt[0] = newdata[4];
		snvt[1] = newdata[5];
		exhaust_pressure.setData(snvt);
		snvt[0] = newdata[6];
		snvt[1] = newdata[7];
		shaft_seal_purge_pressure.setData(snvt);
		snvt_f[0] = newdata[8];
		snvt_f[1] = newdata[9];
		snvt_f[2] = newdata[10];
		snvt_f[3] = newdata[11];
		inlet_vacuum.setData(snvt_f);
		snvt[0] = newdata[12];
		snvt[1] = newdata[13];
		supply_voltage.setData(snvt);
		snvt[0] = newdata[14];
		snvt[1] = newdata[15];
		coolant_flow.setData(snvt);
		coolant_flow_low_input = newdata[16];
		coolant_flow_low_output = (double) coolant_flow_low_input;
		dilution_active_input = newdata[17];
		dilution_active_output = (double) dilution_active_output;
		ballast_dilution_active_input = newdata[18];
		ballast_dilution_active_output = (double) ballast_dilution_active_input;
		inlet_purge_dilution_active_input = newdata[19];
		inlet_purge_dilution_active_output = (double) inlet_purge_dilution_active_input;
		exhaust_dilution_active_input = newdata[20];
		exhaust_dilution_active_output = (double) exhaust_dilution_active_input;
		dilution_flow_out_of_range_input = newdata[21];
		dilution_flow_out_of_range_output = (double) dilution_flow_out_of_range_input;
		power_supply_on_input = newdata[22];
		power_supply_on_output = (double) power_supply_on_input;

	}

	@Override
	public short getSelector() {
		return selector;
	}

	@Override
	public short getIndex() {
		return index;
	}

	@Override
	public void setIndex(short index) {
		this.index = index;

	}

	@Override
	public void setSelector(short selector) {
		this.selector = selector;

	}

	@Override
	public int getSize() {
		return 23;
	}

	@Override
	public void setFieldByName(String name, Serializable value) throws FieldNotFoundException, IncorrectValueException {
		int i = 0;
		if (name.equalsIgnoreCase("total_dilution_flow")) {
			total_dilution_flow.setFieldByName("flow_mil", value);
			i++;
		}
		if (name.equalsIgnoreCase("exhaust_temperature")) {
			exhaust_temperature.setFieldByName("temp", value);
			i++;
		}
		if (name.equalsIgnoreCase("exhaust_pressure")) {
			exhaust_pressure.setFieldByName("press", value);
			i++;
		}
		if (name.equalsIgnoreCase("shaft_seal_purge_pressure")) {
			shaft_seal_purge_pressure.setFieldByName("press", value);
			i++;
		}
		if (name.equalsIgnoreCase("inlet_vacuum")) {
			inlet_vacuum.setFieldByName("press_f", value);
			i++;
		}
		if (name.equalsIgnoreCase("supply_voltage")) {
			supply_voltage.setFieldByName("volt", value);
			i++;
		}
		if (name.equalsIgnoreCase("coolant_flow")) {
			coolant_flow.setFieldByName("flow_mil", value);
			i++;
		}
		if (name.equalsIgnoreCase("coolant_flow_low")) {
			if ((Double) value < (double) -1 || (Double) value > (double) 1) {
				throw new IncorrectValueException("Value is not valid");
			}
			coolant_flow_low_output = (Double) value;
			coolant_flow_low_input = (byte) coolant_flow_low_output;
			i++;
		}
		if (name.equalsIgnoreCase("dilution_active")) {
			if ((Double) value < (double) -1 || (Double) value > (double) 1) {
				throw new IncorrectValueException("Value is not valid");
			}
			dilution_active_output = (Double) value;
			dilution_active_input = (byte) dilution_active_output;
			i++;
		}
		if (name.equalsIgnoreCase("ballast_dilution_active")) {
			if ((Double) value < (double) -1 || (Double) value > (double) 1) {
				throw new IncorrectValueException("Value is not valid");
			}
			ballast_dilution_active_output = (Double) value;
			ballast_dilution_active_input = (byte) ballast_dilution_active_output;
			i++;
		}
		if (name.equalsIgnoreCase("inlet_purge_dilution_active")) {
			if ((Double) value < (double) -1 || (Double) value > (double) 1) {
				throw new IncorrectValueException("Value is not valid");
			}
			inlet_purge_dilution_active_output = (Double) value;
			inlet_purge_dilution_active_input = (byte) inlet_purge_dilution_active_output;
			i++;
		}
		if (name.equalsIgnoreCase("exhaust_dilution_active")) {
			if ((Double) value < (double) -1 || (Double) value > (double) 1) {
				throw new IncorrectValueException("Value is not valid");
			}
			exhaust_dilution_active_output = (Double) value;
			exhaust_dilution_active_input = (byte) exhaust_dilution_active_output;
			i++;
		}
		if (name.equalsIgnoreCase("dilution_flow_out_of_range")) {
			if ((Double) value < (double) -1 || (Double) value > (double) 1) {
				throw new IncorrectValueException("Value is not valid");
			}
			dilution_flow_out_of_range_output = (Double) value;
			dilution_flow_out_of_range_input = (byte) dilution_flow_out_of_range_output;
			i++;
		}
		if (name.equalsIgnoreCase("power_supply_on")) {
			if ((Double) value < (double) -1 || (Double) value > (double) 1) {
				throw new IncorrectValueException("Value is not valid");
			}
			power_supply_on_output = (Double) value;
			power_supply_on_input = (byte) power_supply_on_output;
			i++;
		}
		if (i == 0) {
			throw new FieldNotFoundException("Field is not found");
		}

	}

	@Override
	public Double getFieldByName(String name) throws FieldNotFoundException {
		if (name.equalsIgnoreCase("total_dilution_flow")) {
			return total_dilution_flow.getFieldByName("flow_mil");
		}
		if (name.equalsIgnoreCase("exhaust_temperature")) {
			return exhaust_temperature.getFieldByName("temp");
		}
		if (name.equalsIgnoreCase("exhaust_pressure")) {
			return exhaust_pressure.getFieldByName("press");
		}
		if (name.equalsIgnoreCase("shaft_seal_purge_pressure")) {
			return shaft_seal_purge_pressure.getFieldByName("press");
		}
		if (name.equalsIgnoreCase("inlet_vacuum")) {
			return inlet_vacuum.getFieldByName("press_f");
		}
		if (name.equalsIgnoreCase("supply_voltage")) {
			return supply_voltage.getFieldByName("volt");
		}
		if (name.equalsIgnoreCase("coolant_flow")) {
			return coolant_flow.getFieldByName("flow_mil");
		}
		if (name.equalsIgnoreCase("dilution_active")) {
			return dilution_active_output;
		}
		if (name.equalsIgnoreCase("ballast_dilution_active")) {
			return ballast_dilution_active_output;
		}
		if (name.equalsIgnoreCase("inlet_purge_dilution_active")) {
			return inlet_purge_dilution_active_output;
		}
		if (name.equalsIgnoreCase("exhaust_dilution_active")) {
			return exhaust_dilution_active_output;
		}
		if (name.equalsIgnoreCase("dilution_flow_out_of_range")) {
			return dilution_flow_out_of_range_output;
		}
		if (name.equalsIgnoreCase("power_supply_on")) {
			return power_supply_on_output;
		}
		throw new FieldNotFoundException("Field is not found");
	}

	@Override
	public boolean isRoundtrip() {
		return ROUNDTRIP;
	}

}
