package com.synapsense.lontalk.apdu.variableimpl.different;

import java.io.Serializable;

import com.synapsense.lontalk.apdu.variable.NetworkVariable;
import com.synapsense.lontalk.exception.FieldNotFoundException;
import com.synapsense.lontalk.exception.IncorrectValueException;

public class SNVT_switch implements NetworkVariable {

	private short selector;
	private short index;

	private short value_input;
	private double value_output;
	private byte[] value_scaling = { 5, -1, 0 };

	private byte state_input;
	private double state_output;

	private static final boolean ROUNDTRIP = true;

	@Override
	public byte[] getData() {
		byte[] data = new byte[2];
		data[0] = data[0] |= value_input;
		data[1] = state_input;
		return null;
	}

	@Override
	public void setData(byte[] newdata) {
		value_input = newdata[0];
		value_output = value_scaling[0] * Math.pow(10, value_scaling[1]) * (value_input + value_scaling[2]);
		state_input = newdata[1];
		state_output = (double) state_input;

	}

	@Override
	public short getSelector() {
		return selector;
	}

	@Override
	public short getIndex() {
		return index;
	}

	@Override
	public void setIndex(short index) {
		this.index = index;
	}

	@Override
	public void setSelector(short selector) {
		this.selector = selector;
	}

	@Override
	public int getSize() {
		return 2;
	}

	@Override
	public void setFieldByName(String name, Serializable value) throws FieldNotFoundException, IncorrectValueException {
		int i = 0;
		if (name.equalsIgnoreCase("value")) {
			Double a = (Double) value;
			if (a < 1 || a > 65535) {
				throw new IncorrectValueException("Value is not valid");
			}
			value_input = (short) ((a / (value_scaling[0] * Math.pow(10, value_scaling[1]))) - value_scaling[2]);
			value_output = value_scaling[0] * Math.pow(10, value_scaling[1]) * (value_input + value_scaling[2]);
			i++;
		}
		if (name.equalsIgnoreCase("state")) {
			Double a = (Double) value;
			if (a < -1 || a > 1) {
				throw new IncorrectValueException("Value is not valid");
			}
			state_output = a;
			state_input = (byte) state_output;
			i++;
		}
		if (i == 0) {
			throw new FieldNotFoundException("Field is not found");
		}

	}

	@Override
	public Double getFieldByName(String name) throws FieldNotFoundException {
		if (name.equalsIgnoreCase("value")) {
			return value_output;
		}
		if (name.equalsIgnoreCase("state")) {
			return state_output;
		}
		throw new FieldNotFoundException("Field is not found");
	}

	@Override
	public boolean isRoundtrip() {
		return ROUNDTRIP;
	}

}
