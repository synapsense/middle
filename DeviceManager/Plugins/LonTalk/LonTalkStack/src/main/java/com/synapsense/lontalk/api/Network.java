package com.synapsense.lontalk.api;

import java.util.HashMap;
import java.util.Map;

import com.synapsense.lontalk.exception.IncorrectFormatException;
import com.synapsense.lontalk.node.CNPIPDevice;

public class Network {
	public String csIpAddress;
	public int csport;
	public String pluginIpAddress;
	public int pluginPort;
	public String domain;
	public int domainLength;
	private byte[] domainBytes;
	public byte pluginSubnet;
	public byte pluginNode;
	public String pluginNeuronID;
	private byte[] neuronIDbytes;

	private LonTalkStack stack;
	private Map<String, CNPIPDevice> routers = new HashMap<String, CNPIPDevice>();

	public Network(String csIpAddress, int csport, String pluginIpAddress, int pluginPort, String domain,
	        int pluginSubnet, int pluginNode, String pluginNeuronID) throws IncorrectFormatException {
		this.csIpAddress = csIpAddress;
		this.csport = csport;
		this.pluginIpAddress = pluginIpAddress;
		this.pluginPort = pluginPort;
		this.domain = domain;
		this.pluginSubnet = (byte) pluginSubnet;
		this.pluginNode = (byte) pluginNode;
		this.pluginNeuronID = pluginNeuronID;
		stack = new LonTalkStack(this);
		parseDomain();
		parseNeuronID();
	}

	private void parseNeuronID() throws IncorrectFormatException {
		if (!(pluginNeuronID.length() == 12)) {
			throw new IncorrectFormatException("NeuronID has an incorrect format");
		}
		neuronIDbytes = new byte[6];
		int k = 0;
		for (int i = 0; i < 12; i = i + 2) {
			Short neuronByte = Short.parseShort(pluginNeuronID.substring(i, i + 2), 16);
			neuronIDbytes[k] = 0;
			neuronIDbytes[k] = neuronIDbytes[k] |= neuronByte;
			k++;
		}
	}

	private void parseDomain() throws IncorrectFormatException {
		int length = domain.length();
		if (!((length == 2) | (length == 6) | (length == 12))) {
			throw new IncorrectFormatException("Domain has an incorrect format");
		}
		domainLength = length / 2;
		domainBytes = new byte[domainLength];
		int k = 0;
		for (int i = 0; i < length; i = i + 2) {
			Short domainByte = Short.parseShort(domain.substring(i, i + 2), 16);
			domainBytes[k] = 0;
			domainBytes[k] = domainBytes[k] |= domainByte;
			k++;
		}
	}

	public byte[] getDomainInSixBytes() {
		byte[] a = new byte[6];
		for (int i = 0; i < domainLength; i++) {
			a[i] = domainBytes[i];
		}
		for (int i = domainLength; i < 6; i++) {
			a[i] = 0;
		}
		return a;
	}

	public byte[] getDomainBytes() {
		return domainBytes;
	}

	public byte[] getNeuronIDBytes() {
		return neuronIDbytes;
	}

	public LonTalkStack getStack() {
		return stack;
	}

	public void addCNPIPDevice(String key, CNPIPDevice device) {
		routers.put(key, device);
	}

	public CNPIPDevice getCNPIPDevice(String key) {
		return routers.get(key);
	}
}