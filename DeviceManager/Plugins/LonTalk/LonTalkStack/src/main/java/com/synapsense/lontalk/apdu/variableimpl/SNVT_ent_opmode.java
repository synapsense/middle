package com.synapsense.lontalk.apdu.variableimpl;

import java.io.Serializable;

import com.synapsense.lontalk.apdu.variable.NetworkVariable;
import com.synapsense.lontalk.exception.FieldNotFoundException;
import com.synapsense.lontalk.exception.IncorrectValueException;

public class SNVT_ent_opmode implements NetworkVariable {

	private short selector;
	private short index;
	private byte input = 0;
	private double output = 0;
	private static final boolean ROUNDTRIP = false;

	// EM_NUL -1 Invalid Value
	// EM_UNDEFINED 0 Operation mode is not defined
	// EM_AUTO 1 Operation mode is Automatic
	// EM_AUTO_RED 2 Operation mode is AUTOMATIC with reduced width
	// EM_CLOSE_LOCK 3 Operation mode is CLOSE and LOCK
	// EM_CLOSE_UNLOCK 4 Operation mode is CLOSE and UNLOCK
	// EM_EXIT_ONLY 5 Operation mode is EXIT ONLY
	// EM_OPEN 6 Operation mode is OPEN
	// EM_OPEN_ONCE 7 Operation mode is OPEN AND CLOSE ONCE
	// EM_MANUAL 8 Operation mode is MANUAL
	// EM_FIRE 9 Operation mode is FIRE
	// EM_EVAC 10 Operation mode is EVACUATION
	// EM_WEATHER 11 Operation mode is WEATHER MODE
	// EM_DAY_LOCKING 12 Operation mode is DAY_LOCKING, locking with reduced
	// level of security
	// EM_NIGHT_LOCKING 13 Operation mode is NIGHT_LOCKING, locking with maximum
	// level of security
	// EM_BLOCKED 14 Operation mode is BLOCKED, no operations is allowed
	// EM_SERVICE 15 Operation mode is SERVICE
	// EM_ENTRY_ONLY 16 Operation mode is ENTRY_ONLY

	@Override
	public byte[] getData() {
		byte[] a = { input };
		return a;
	}

	@Override
	public void setData(byte[] newdata) {
		input = newdata[0];
		output = (double) input;

	}

	@Override
	public short getSelector() {
		return selector;
	}

	@Override
	public void setSelector(short selector) {
		this.selector = selector;

	}

	@Override
	public void setFieldByName(String name, Serializable value) throws FieldNotFoundException, IncorrectValueException {
		int i = 0;
		if (name.equalsIgnoreCase("ent_opmode")) {
			if ((Double) value < (double) -1 || (Double) value > (double) 16) {
				throw new IncorrectValueException("Value is not valid");
			}
			output = (Double) value;
			input = (byte) output;
			i++;
		}
		if (i == 0) {
			throw new FieldNotFoundException("Field is not found");
		}

	}

	@Override
	public Double getFieldByName(String name) throws FieldNotFoundException {
		if (name.equalsIgnoreCase("ent_opmode")) {
			return output;
		}
		throw new FieldNotFoundException("Field is not found");
	}

	@Override
	public short getIndex() {
		return index;
	}

	@Override
	public void setIndex(short index) {
		this.index = index;

	}

	@Override
	public int getSize() {
		return 1;
	}

	@Override
	public boolean isRoundtrip() {
		return ROUNDTRIP;
	}

}
