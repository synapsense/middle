package com.synapsense.lontalk.packet;

import java.net.InetAddress;
import java.net.UnknownHostException;

import com.synapsense.lontalk.api.Network;
import com.synapsense.lontalk.packet.header.CNPIPHeader;

public class RegistrationPacket {

	private int datetime = 0xd29f2bb8;
	private byte ipflags = 0x01;
	private byte routerflags = 0x00;
	private byte cnpflags = 0x02;
	private byte nodetype = 0x01;
	private byte mcaddresscount = 0;

	private short channeltimeout = 0;
	private short totalUniqueIdBytes = 6;

	private short ipunicastport;
	private byte[] ipunicastaddress = new byte[4];

	private int channelmembershipdatetime = 0xd29f2bb8;
	private int sendlistdatetime = 0;

	private byte[] configserveripaddress;

	private byte[] primarytimeserveripaddress = { 0, 0, 0, 0, };
	private byte[] secondarytimeserveripaddress = { 0, 0, 0, 0 };

	private short configserveripport;

	private short primarytimeserveripport = 0;
	private short secondarytimeserveripport = 0;

	private byte[] uniqueID;
	private byte nameLength = 6;
	private byte[] name = { 0x53, 0x59, 0x4e, 0x41, 0x50, 0x00 };

	public byte[] getRegistrationPacket(Network network) throws UnknownHostException {
		ipunicastport = (short) network.pluginPort;
		ipunicastaddress = InetAddress.getByName(network.pluginIpAddress).getAddress();
		configserveripaddress = InetAddress.getByName(network.csIpAddress).getAddress();
		configserveripport = (short) network.csport;
		uniqueID = network.getNeuronIDBytes();

		byte[] packet = new byte[81];
		CNPIPHeader header = new CNPIPHeader();
		header.setRegistrationType();
		header.setDataPacketLenght((short) 81);
		byte[] hd = header.getHeader();
		for (int i = 0; i < hd.length; i++) {
			packet[i] = hd[i];
		}

		packet[20] = packet[20] |= (datetime >>> 24);
		packet[21] = packet[21] |= (datetime >>> 16);
		packet[22] = packet[22] |= (datetime >>> 8);
		packet[23] = packet[23] |= (datetime);

		packet[24] = ipflags;
		packet[25] = routerflags;
		packet[26] = cnpflags;
		packet[27] = nodetype;

		packet[28] = mcaddresscount;
		packet[29] = 0; // /mbz
		packet[30] = (byte) ((channeltimeout >>> 8) & 0Xff);
		packet[31] = (byte) (channeltimeout & 0xFF);

		packet[32] = (byte) ((totalUniqueIdBytes >>> 8) & 0Xff);
		packet[33] = (byte) (totalUniqueIdBytes & 0xFF);
		packet[34] = (byte) ((ipunicastport >>> 8) & 0Xff);
		packet[35] = (byte) (ipunicastport & 0xFF);

		packet[36] = ipunicastaddress[0];
		packet[37] = ipunicastaddress[1];
		packet[38] = ipunicastaddress[2];
		packet[39] = ipunicastaddress[3];

		packet[40] = packet[40] |= (channelmembershipdatetime >>> 24);
		packet[41] = packet[41] |= (channelmembershipdatetime >>> 16);
		packet[42] = packet[42] |= (channelmembershipdatetime >>> 8);
		packet[43] = packet[43] |= (channelmembershipdatetime);

		packet[44] = packet[44] |= (sendlistdatetime >>> 24);
		packet[45] = packet[45] |= (sendlistdatetime >>> 16);
		packet[46] = packet[46] |= (sendlistdatetime >>> 8);
		packet[47] = packet[47] |= (sendlistdatetime);

		packet[48] = configserveripaddress[0];
		packet[49] = configserveripaddress[1];
		packet[50] = configserveripaddress[2];
		packet[51] = configserveripaddress[3];

		packet[52] = primarytimeserveripaddress[0];
		packet[53] = primarytimeserveripaddress[1];
		packet[54] = primarytimeserveripaddress[2];
		packet[55] = primarytimeserveripaddress[3];

		packet[56] = secondarytimeserveripaddress[0];
		packet[57] = secondarytimeserveripaddress[1];
		packet[58] = secondarytimeserveripaddress[2];
		packet[59] = secondarytimeserveripaddress[3];

		packet[60] = (byte) ((configserveripport >>> 8) & 0Xff);
		packet[61] = (byte) (configserveripport & 0xFF);
		packet[62] = (byte) ((primarytimeserveripport >>> 8) & 0Xff);
		packet[63] = (byte) (primarytimeserveripport & 0xFF);

		packet[64] = (byte) ((secondarytimeserveripport >>> 8) & 0Xff);
		packet[65] = (byte) (secondarytimeserveripport & 0xFF);
		packet[66] = 0;// mbz
		packet[67] = 0;// mbz

		packet[68] = uniqueID[0];
		packet[69] = uniqueID[1];
		packet[70] = uniqueID[2];
		packet[71] = uniqueID[3];
		packet[72] = uniqueID[4];
		packet[73] = uniqueID[5];

		packet[74] = nameLength;

		packet[75] = name[0];
		packet[76] = name[1];
		packet[77] = name[2];
		packet[78] = name[3];
		packet[79] = name[4];
		packet[80] = name[5];

		return packet;
	}

}
