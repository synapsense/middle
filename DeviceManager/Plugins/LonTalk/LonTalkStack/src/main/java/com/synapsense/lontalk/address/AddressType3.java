package com.synapsense.lontalk.address;

//unicast: message or reminder
public class AddressType3 {
	public byte srcSubnet = 0;
	public byte srcNode = 0;
	public byte dstSubnet;
	public long neuronId;

	public AddressType3(byte srcSubnet, byte srcNode, byte dstSubnet, long neuronId) {
		this.srcSubnet = srcSubnet;
		this.srcNode = srcNode;
		this.dstSubnet = dstSubnet;
		this.neuronId = neuronId;
	}

	public byte[] getAddress() {
		srcNode = srcNode |= (-128);
		byte[] address = new byte[9];
		address[0] = srcSubnet;
		address[1] = srcNode;
		address[2] = dstSubnet;
		address[3] = address[3] |= (neuronId >>> 40);
		address[4] = address[4] |= (neuronId >>> 32);
		address[5] = address[5] |= (neuronId >>> 24);
		address[6] = address[6] |= (neuronId >>> 16);
		address[7] = address[7] |= (neuronId >>> 8);
		address[8] = address[8] |= (neuronId);
		return address;
	}

	public byte getSrcSubnet() {
		return srcSubnet;
	}

	public void setSrcSubnet(byte srcSubnet) {
		this.srcSubnet = srcSubnet;
	}

	public byte getSrcNode() {
		return srcNode;
	}

	public void setSrcNode(byte srcNode) {
		this.srcNode = srcNode;
	}

	public byte getDstSubnet() {
		return dstSubnet;
	}

	public void setDstSubnet(byte dstSubnet) {
		this.dstSubnet = dstSubnet;
	}

	public long getNeuronId() {
		return neuronId;
	}

	public void setNeuronId(long neuronId) {
		this.neuronId = neuronId;
	}
}
