package com.synapsense.lontalk.apdu.variableimpl;

import java.io.Serializable;

import com.synapsense.lontalk.apdu.variable.NetworkVariable;
import com.synapsense.lontalk.exception.FieldNotFoundException;
import com.synapsense.lontalk.exception.IncorrectValueException;

public class SNVT_ent_state implements NetworkVariable {

	private short selector;
	private short index;
	private byte input = 0;
	private double output = 0;
	private static final boolean ROUNDTRIP = false;

	// ES_NUL -1 Invalid value
	// ES_UNDEFINED 0 State is not yet defined
	// ES_OPEN_PLUS 1 Open the device and close it when back in normal position
	// ES_OPEN 2 Open the device if not locked
	// ES_CLOSE 3 Close the device
	// ES_STOP 4 Stop the device
	// ES_STOP_RESUME 5 Continue after stop command
	// ES_ENTRY_REQ 6 Entry request, access in to the area
	// ES_EXIT_REQ 7 Exit request, access out from the ares
	// ES_KEY_REQ 8 Exit request, access out from the ares
	// ES_SAFETY_EXT_REQ 9 Safety request, the device will go to a pre defined
	// safety position/mode
	// ES_EMERGENCY_REQ 10 Emergency request, the device will go to an pre
	// defined emergency position/mode
	// ES_UPDATE_STATE 11 Update the current state and mode
	// ES_SAF_EXT_RESUME 12 Resume after Safety function
	// ES_EMERG_RESUME 13 Resume after Emergency function

	@Override
	public byte[] getData() {
		byte[] a = { input };
		return a;
	}

	@Override
	public void setData(byte[] newdata) {
		input = newdata[0];
		output = (double) input;

	}

	@Override
	public short getSelector() {
		return selector;
	}

	@Override
	public void setSelector(short selector) {
		this.selector = selector;

	}

	@Override
	public void setFieldByName(String name, Serializable value) throws FieldNotFoundException, IncorrectValueException {
		int i = 0;
		if (name.equalsIgnoreCase("ent_state")) {
			if ((Double) value < (double) -1 || (Double) value > (double) 13) {
				throw new IncorrectValueException("Value is not valid");
			}
			output = (Double) value;
			input = (byte) output;
			i++;
		}
		if (i == 0) {
			throw new FieldNotFoundException("Field is not found");
		}

	}

	@Override
	public Double getFieldByName(String name) throws FieldNotFoundException {
		if (name.equalsIgnoreCase("ent_state")) {
			return output;
		}
		throw new FieldNotFoundException("Field is not found");
	}

	@Override
	public short getIndex() {
		return index;
	}

	@Override
	public void setIndex(short index) {
		this.index = index;

	}

	@Override
	public int getSize() {
		return 1;
	}

	@Override
	public boolean isRoundtrip() {
		return ROUNDTRIP;
	}

}
