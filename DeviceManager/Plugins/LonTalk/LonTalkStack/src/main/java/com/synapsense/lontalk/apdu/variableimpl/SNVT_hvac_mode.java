package com.synapsense.lontalk.apdu.variableimpl;

import java.io.Serializable;

import com.synapsense.lontalk.apdu.variable.NetworkVariable;
import com.synapsense.lontalk.exception.FieldNotFoundException;
import com.synapsense.lontalk.exception.IncorrectValueException;

public class SNVT_hvac_mode implements NetworkVariable {

	private short selector;
	private short index;
	private byte input = 0;
	private double output = 0;
	private static final boolean ROUNDTRIP = false;

	// HVAC_NUL -1 Invalid value
	// HVAC_AUTO 0 Controller automatically changes between application modes
	// HVAC_HEAT 1 Heating only
	// HVAC_MRNG_WRMUP 2 Application-specific morning warm-up
	// HVAC_COOL 3 Cooling only
	// HVAC_NIGHT_PURGE 4 Application-specific night purge
	// HVAC_PRE_COOL 5 Application-specific pre-cool
	// HVAC_OFF 6 Controller not controlling outputs
	// HVAC_TEST 7 Equipment being tested
	// HVAC_EMERG_HEAT 8 Emergency heat mode (heat pump)
	// HVAC_FAN_ONLY 9 Air not conditioned, fan turned on
	// HVAC_FREE_COOL 10 Cooling with compressor not running
	// HVAC_ICE 11 Ice-making mode
	// HVAC_MAX_HEAT 12 Maximum heating mode
	// HVAC_ECONOMY 13 Economic Heat/Cool mode
	// HVAC_DEHUMID 14 Dehumidification mode
	// HVAC_CALIBRATE 15 Calibration mode
	// HVAC_EMERG_COOL 16 Emergency cool mode
	// HVAC_EMERG_STEAM 17 Emergency steam mode
	// HVAC_MAX_COOL 18
	// HVAC_HVC_LOAD 19
	// HVAC_NO_LOAD 20

	@Override
	public byte[] getData() {
		byte[] a = { input };
		return a;
	}

	@Override
	public void setData(byte[] newdata) {
		input = newdata[0];
		output = (double) input;

	}

	@Override
	public short getSelector() {
		return selector;
	}

	@Override
	public void setSelector(short selector) {
		this.selector = selector;

	}

	@Override
	public void setFieldByName(String name, Serializable value) throws FieldNotFoundException, IncorrectValueException {
		int i = 0;
		if (name.equalsIgnoreCase("hvac_mode")) {
			if ((Double) value < (double) -1 || (Double) value > (double) 20) {
				throw new IncorrectValueException("Value is not valid");
			}
			output = (Double) value;
			input = (byte) output;
			i++;
		}
		if (i == 0) {
			throw new FieldNotFoundException("Field is not found");
		}

	}

	@Override
	public Double getFieldByName(String name) throws FieldNotFoundException {
		if (name.equalsIgnoreCase("hvac_mode")) {
			return output;
		}
		throw new FieldNotFoundException("Field is not found");
	}

	@Override
	public short getIndex() {
		return index;
	}

	@Override
	public void setIndex(short index) {
		this.index = index;

	}

	@Override
	public int getSize() {
		return 1;
	}

	@Override
	public boolean isRoundtrip() {
		return ROUNDTRIP;
	}

}
