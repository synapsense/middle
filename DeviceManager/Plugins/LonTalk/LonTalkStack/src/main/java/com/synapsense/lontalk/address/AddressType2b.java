package com.synapsense.lontalk.address;

//multicast: acknowledgment 
public class AddressType2b {
	public byte srcSubnet;
	public byte srcNode;
	public byte dstSubnet;
	public byte dstNode;
	public byte group;
	public byte grpMemb;

	public AddressType2b(byte srcSubnet, byte srcNode, byte dstSubnet, byte dstNode, byte group, byte grpMemb) {
		this.srcSubnet = srcSubnet;
		this.srcNode = srcNode;
		this.dstSubnet = dstSubnet;
		this.dstNode = dstNode;
		this.group = group;
		this.grpMemb = grpMemb;
	}

	public byte[] getAddress() {
		dstNode = dstNode |= (-128);
		byte[] address = { srcSubnet, srcNode, dstSubnet, dstNode, group, grpMemb };
		return address;
	}

	public byte getSrcSubnet() {
		return srcSubnet;
	}

	public void setSrcSubnet(byte srcSubnet) {
		this.srcSubnet = srcSubnet;
	}

	public byte getSrcNode() {
		return srcNode;
	}

	public void setSrcNode(byte srcNode) {
		this.srcNode = srcNode;
	}

	public byte getDstSubnet() {
		return dstSubnet;
	}

	public void setDstSubnet(byte dstSubnet) {
		this.dstSubnet = dstSubnet;
	}

	public byte getDstNode() {
		return dstNode;
	}

	public void setDstNode(byte dstNode) {
		this.dstNode = dstNode;
	}

	public byte getGroup() {
		return group;
	}

	public void setGroup(byte group) {
		this.group = group;
	}

	public byte getGrpMemb() {
		return grpMemb;
	}

	public void setGrpMemb(byte grpMemb) {
		this.grpMemb = grpMemb;
	}
}
