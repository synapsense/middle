package com.synapsense.lontalk.apdu.variable.template;

import java.io.Serializable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import com.synapsense.lontalk.apdu.variable.NetworkVariable;
import com.synapsense.lontalk.exception.FieldNotFoundException;

public class SNVT_FloatType implements NetworkVariable {

	private short selector;
	private short index;
	private float input;
	private byte[] scaling = new byte[3];
	private boolean roundtrip = false;

	public SNVT_FloatType(String scale) {
		parseString(scale);
	}

	@Override
	public byte[] getData() {
		byte[] a = new byte[4];
		int valint = Float.floatToIntBits(input);
		a[0] = (byte) ((valint >> 24) & 0xff);
		a[1] = (byte) ((valint >> 16) & 0xff);
		a[2] = (byte) ((valint >> 8) & 0xff);
		a[3] = (byte) ((valint) & 0xff);
		return a;
	}

	@Override
	public void setData(byte[] newdata) {
		ByteBuffer bb = ByteBuffer.allocate(4);
		bb.order(ByteOrder.LITTLE_ENDIAN);
		bb.put(newdata[3]);
		bb.put(newdata[2]);
		bb.put(newdata[1]);
		bb.put(newdata[0]);
		input = bb.getFloat(0);
	}

	@Override
	public short getSelector() {
		return selector;
	}

	@Override
	public void setSelector(short selector) {
		this.selector = selector;

	}

	@Override
	public void setFieldByName(String name, Serializable value) throws FieldNotFoundException {
		double b = (Double) value;
		input = (float) b;
	}

	@Override
	public Double getFieldByName(String name) throws FieldNotFoundException {
		return (double) input;
	}

	private void parseString(String scale) {
		scale.trim();
		String[] a = scale.split(";");
		scaling[0] = Byte.parseByte(a[0]);
		scaling[1] = Byte.parseByte(a[1]);
		scaling[2] = Byte.parseByte(a[2]);
	}

	@Override
	public short getIndex() {
		return index;
	}

	@Override
	public void setIndex(short index) {
		this.index = index;

	}

	@Override
	public int getSize() {
		return 4;
	}

	@Override
	public boolean isRoundtrip() {
		return roundtrip;
	}

}
