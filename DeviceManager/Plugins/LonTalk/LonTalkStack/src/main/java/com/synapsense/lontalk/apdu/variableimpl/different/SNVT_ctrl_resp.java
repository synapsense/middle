package com.synapsense.lontalk.apdu.variableimpl.different;

import java.io.Serializable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import com.synapsense.lontalk.apdu.variable.NetworkVariable;
import com.synapsense.lontalk.exception.FieldNotFoundException;
import com.synapsense.lontalk.exception.IncorrectValueException;

public class SNVT_ctrl_resp implements NetworkVariable {

	private short selector;
	private short index;

	private byte status_input;
	private double status_output;

	private int senderID_input;
	private byte[] scaling_senderID = { 1, 0, 0 };
	private double senderID_output;

	private int sender_range_lower_input;
	private byte[] scaling_sender_range_lower = { 1, 0, 0 };
	private double sender_range_lower_output;

	private int sender_range_upper_input;
	private byte[] scaling_sender_range_upper = { 1, 0, 0 };
	private double sender_range_upper_output;

	private int controller_ID_input;
	private byte[] scaling_controller_id = { 1, 0, 0 };
	private double controller_id_output;

	private static final boolean ROUNDTRIP = true;

	@Override
	public byte[] getData() {
		byte[] data = new byte[9];
		data[0] = status_input;
		data[1] = (byte) ((senderID_input >> 8) & 0xff);
		data[2] = (byte) (senderID_input & 0xff);
		data[3] = (byte) ((sender_range_lower_input >> 8) & 0xff);
		data[4] = (byte) (sender_range_lower_input & 0xff);
		data[5] = (byte) ((sender_range_upper_input >> 8) & 0xff);
		data[6] = (byte) (sender_range_upper_input & 0xff);
		data[7] = (byte) ((controller_ID_input >> 8) & 0xff);
		data[8] = (byte) (controller_ID_input & 0xff);
		return data;
	}

	@Override
	public void setData(byte[] newdata) {
		status_input = newdata[0];
		status_output = (double) status_input;
		ByteBuffer bb = ByteBuffer.allocate(4);
		bb.order(ByteOrder.LITTLE_ENDIAN);
		bb.put(newdata[2]);
		bb.put(newdata[1]);
		senderID_input = bb.getInt(0);
		senderID_output = scaling_senderID[0] * Math.pow(10, scaling_senderID[1])
		        * (senderID_input + scaling_senderID[2]);
		bb.clear();
		bb.put(newdata[4]);
		bb.put(newdata[3]);
		sender_range_lower_input = bb.getInt(0);
		sender_range_lower_output = scaling_sender_range_lower[0] * Math.pow(10, scaling_sender_range_lower[1])
		        * (sender_range_lower_input + scaling_sender_range_lower[2]);
		bb.clear();
		bb.put(newdata[6]);
		bb.put(newdata[5]);
		sender_range_upper_input = bb.getInt(0);
		sender_range_upper_output = scaling_sender_range_upper[0] * Math.pow(10, scaling_sender_range_upper[1])
		        * (sender_range_upper_input + scaling_sender_range_upper[2]);
		bb.clear();

		bb.put(newdata[8]);
		bb.put(newdata[7]);
		controller_ID_input = bb.getInt(0);
		controller_id_output = scaling_controller_id[0] * Math.pow(10, scaling_controller_id[1])
		        * (controller_ID_input + scaling_controller_id[2]);
		bb.clear();

	}

	@Override
	public short getSelector() {
		return selector;
	}

	@Override
	public short getIndex() {
		return index;
	}

	@Override
	public void setIndex(short index) {
		this.index = index;

	}

	@Override
	public void setSelector(short selector) {
		this.selector = selector;

	}

	@Override
	public int getSize() {
		return 9;
	}

	@Override
	public void setFieldByName(String name, Serializable value) throws FieldNotFoundException, IncorrectValueException {
		int i = 0;
		if (name.equalsIgnoreCase("status")) {
			if ((Double) value < (double) -1 || (Double) value > (double) 5) {
				throw new IncorrectValueException("Value is not valid");
			}
			status_output = (Double) value;
			status_input = (byte) status_output;
			i++;
		}
		if (name.equalsIgnoreCase("sender_id")) {
			Double a = (Double) value;
			if (a < 1 || a > 65535) {
				throw new IncorrectValueException("Value is not valid");
			}
			senderID_input = (int) ((a / (scaling_senderID[0] * Math.pow(10, scaling_senderID[1]))) - scaling_senderID[2]);
			senderID_output = scaling_senderID[0] * Math.pow(10, scaling_senderID[1])
			        * (senderID_input + scaling_senderID[2]);
			i++;
		}
		if (name.equalsIgnoreCase("sender_range_lower")) {
			Double a = (Double) value;
			if (a < 1 || a > 65535) {
				throw new IncorrectValueException("Value is not valid");
			}
			sender_range_lower_input = (int) ((a / (scaling_sender_range_lower[0] * Math.pow(10,
			        scaling_sender_range_lower[1]))) - scaling_sender_range_lower[2]);
			sender_range_lower_output = scaling_sender_range_lower[0] * Math.pow(10, scaling_sender_range_lower[1])
			        * (sender_range_lower_input + scaling_sender_range_lower[2]);
			i++;
		}
		if (name.equalsIgnoreCase("sender_range_upper")) {
			Double a = (Double) value;
			if (a < 1 || a > 65535) {
				throw new IncorrectValueException("Value is not valid");
			}
			sender_range_upper_input = (int) ((a / (scaling_sender_range_upper[0] * Math.pow(10,
			        scaling_sender_range_upper[1]))) - scaling_sender_range_upper[2]);
			sender_range_upper_output = scaling_sender_range_upper[0] * Math.pow(10, scaling_sender_range_upper[1])
			        * (sender_range_upper_input + scaling_sender_range_upper[2]);
			i++;
		}
		if (i == 0) {
			throw new FieldNotFoundException("Field is not found");
		}

	}

	@Override
	public Double getFieldByName(String name) throws FieldNotFoundException {
		if (name.equalsIgnoreCase("status")) {
			return status_output;
		}
		if (name.equalsIgnoreCase("sender_id")) {
			return senderID_output;
		}
		if (name.equalsIgnoreCase("sender_range_lower")) {
			return sender_range_lower_output;
		}
		if (name.equalsIgnoreCase("sender_range_upper")) {
			return sender_range_upper_output;
		}
		if (name.equalsIgnoreCase("controller_id")) {
			return controller_id_output;
		}
		throw new FieldNotFoundException("Field is not found");
	}

	@Override
	public boolean isRoundtrip() {
		return ROUNDTRIP;
	}

}
