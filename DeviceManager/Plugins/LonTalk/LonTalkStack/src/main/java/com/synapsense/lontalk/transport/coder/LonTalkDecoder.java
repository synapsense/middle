package com.synapsense.lontalk.transport.coder;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.handler.codec.oneone.OneToOneDecoder;

public class LonTalkDecoder extends OneToOneDecoder {

	@Override
	protected Object decode(ChannelHandlerContext ctx, Channel channel, Object msg) throws Exception {
		ChannelBuffer channelBuf = (ChannelBuffer) msg;
		byte[] data = channelBuf.array();
		ByteBuffer bb = ByteBuffer.allocate(4);
		bb.order(ByteOrder.LITTLE_ENDIAN);
		bb.put(data[1]);
		bb.put(data[0]);
		int size = bb.getInt(0);
		byte[] returned = new byte[size];
		for (int i = 0; i < size; i++) {
			returned[i] = data[i];
		}
		return returned;
	}

}
