package com.synapsense.lontalk.apdu.variableimpl.different;

import java.io.Serializable;

import com.synapsense.lontalk.apdu.variable.NetworkVariable;
import com.synapsense.lontalk.exception.FieldNotFoundException;
import com.synapsense.lontalk.exception.IncorrectValueException;

public class SNVT_chlr_status implements NetworkVariable {

	private short selector;
	private short index;

	private byte chlr_run_mode_input;
	private double chlr_run_mode_Output;

	private byte chlr_op_mode_input;
	private double chlr_op_mode_Output;

	private byte chlr_state;
	private double in_alarm;
	private double run_enabled;
	private double local;
	private double limited;
	private double chw_flow;
	private double condw_flow;

	private static final boolean ROUNDTRIP = true;

	@Override
	public byte[] getData() {
		byte[] data = { chlr_run_mode_input, chlr_op_mode_input, chlr_state };
		return data;
	}

	@Override
	public void setData(byte[] newdata) {
		chlr_run_mode_input = newdata[0];
		chlr_run_mode_Output = (double) chlr_run_mode_input;
		chlr_op_mode_input = newdata[1];
		chlr_op_mode_Output = (byte) chlr_op_mode_input;
		chlr_state = newdata[2];
		in_alarm = chlr_state & 0x1;
		run_enabled = (chlr_state & 0x2) >>> 1;
		local = (chlr_state & 0x4) >>> 2;
		limited = (chlr_state & 0x8) >>> 3;
		chw_flow = (chlr_state & 0x10) >>> 4;
		condw_flow = (chlr_state & 0x20) >>> 5;
	}

	@Override
	public short getSelector() {
		return selector;
	}

	@Override
	public short getIndex() {
		return index;
	}

	@Override
	public void setIndex(short index) {
		this.index = index;

	}

	@Override
	public void setSelector(short selector) {
		this.selector = selector;

	}

	@Override
	public int getSize() {
		return 3;
	}

	@Override
	public void setFieldByName(String name, Serializable value) throws FieldNotFoundException, IncorrectValueException {
		int i = 0;
		if (name.equalsIgnoreCase("chlr_run_mode")) {
			if ((Double) value < (double) -1 || (Double) value > (double) 4) {
				throw new IncorrectValueException("Value is not valid");
			}
			chlr_run_mode_Output = (Double) value;
			chlr_run_mode_input = (byte) chlr_run_mode_Output;
			i++;
		}
		if (name.equalsIgnoreCase("chlr_op_mode")) {
			if ((Double) value < (double) -1 || (Double) value > (double) 20) {
				throw new IncorrectValueException("Value is not valid");
			}
			chlr_op_mode_Output = (Double) value;
			chlr_op_mode_input = (byte) chlr_op_mode_Output;
			i++;
		}
		if (name.equalsIgnoreCase("in_alarm")) {
			double a = (Double) value;
			if (a != 0 & a != 1) {
				throw new IncorrectValueException("Value is not valid");
			}
			in_alarm = a;
			chlr_state = chlr_state |= ((int) a & 0x1);
			i++;
		}
		if (name.equalsIgnoreCase("run_enabled")) {
			double a = (Double) value;
			if (a != 0 & a != 1) {
				throw new IncorrectValueException("Value is not valid");
			}
			run_enabled = a;
			chlr_state = chlr_state |= (((int) a & 0x1) << 1);
			i++;
		}
		if (name.equalsIgnoreCase("local")) {
			double a = (Double) value;
			if (a != 0 & a != 1) {
				throw new IncorrectValueException("Value is not valid");
			}
			local = a;
			chlr_state = chlr_state |= (((int) a & 0x1) << 2);
			i++;
		}
		if (name.equalsIgnoreCase("limited")) {
			double a = (Double) value;
			if (a != 0 & a != 1) {
				throw new IncorrectValueException("Value is not valid");
			}
			limited = a;
			chlr_state = chlr_state |= (((int) a & 0x1) << 3);
			i++;
		}
		if (name.equalsIgnoreCase("chw_flow")) {
			double a = (Double) value;
			if (a != 0 & a != 1) {
				throw new IncorrectValueException("Value is not valid");
			}
			chw_flow = a;
			chlr_state = chlr_state |= (((int) a & 0x1) << 4);
			i++;
		}
		if (name.equalsIgnoreCase("condw_flow")) {
			double a = (Double) value;
			if (a != 0 & a != 1) {
				throw new IncorrectValueException("Value is not valid");
			}
			local = a;
			chlr_state = chlr_state |= (((int) a & 0x1) << 5);
			i++;
		}
		if (i == 0) {
			throw new FieldNotFoundException("Field is not found");
		}

	}

	@Override
	public Double getFieldByName(String name) throws FieldNotFoundException {
		if (name.equalsIgnoreCase("chlr_run_mode")) {
			return chlr_run_mode_Output;
		}
		if (name.equalsIgnoreCase("chlr_op_mode")) {
			return chlr_op_mode_Output;
		}
		if (name.equalsIgnoreCase("in_alarm")) {
			return in_alarm;
		}
		if (name.equalsIgnoreCase("run_enabled")) {
			return run_enabled;
		}
		if (name.equalsIgnoreCase("local")) {
			return local;
		}
		if (name.equalsIgnoreCase("limited")) {
			return limited;
		}
		if (name.equalsIgnoreCase("chw_flow")) {
			return chw_flow;
		}
		if (name.equalsIgnoreCase("condw_flow")) {
			return condw_flow;
		}
		throw new FieldNotFoundException("Field is not found");
	}

	@Override
	public boolean isRoundtrip() {
		return ROUNDTRIP;
	}

}
