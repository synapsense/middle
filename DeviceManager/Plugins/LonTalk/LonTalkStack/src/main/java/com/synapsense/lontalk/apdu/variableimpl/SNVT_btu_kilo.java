package com.synapsense.lontalk.apdu.variableimpl;

import java.io.Serializable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import com.synapsense.lontalk.apdu.variable.NetworkVariable;
import com.synapsense.lontalk.exception.FieldNotFoundException;
import com.synapsense.lontalk.exception.IncorrectValueException;

public class SNVT_btu_kilo implements NetworkVariable {

	private short selector;
	private short index;
	private int input = 0;
	private double output = 0;
	private byte[] scaling = { 1, 0, 0 };
	private static final boolean ROUNDTRIP = false;

	@Override
	public byte[] getData() {
		byte[] a = new byte[2];
		a[1] = (byte) (input & 0xff);
		a[0] = (byte) ((input >> 8) & 0xff);
		return a;
	}

	@Override
	public void setData(byte[] newdata) {
		ByteBuffer bb = ByteBuffer.allocate(4);
		bb.order(ByteOrder.LITTLE_ENDIAN);
		bb.put(newdata[1]);
		bb.put(newdata[0]);
		input = bb.getInt(0);
		output = scaling[0] * Math.pow(10, scaling[1]) * (input + scaling[2]);
	}

	@Override
	public short getSelector() {
		return selector;
	}

	@Override
	public void setSelector(short selector) {
		this.selector = selector;

	}

	@Override
	public void setFieldByName(String name, Serializable value) throws FieldNotFoundException, IncorrectValueException {
		int i = 0;
		if (name.equalsIgnoreCase("btu_kilo")) {
			double a = (Double) value;
			if (a < (double) 0 || a > (double) 65535) {
				throw new IncorrectValueException("Value is not valid");
			}
			input = (int) ((a / (scaling[0] * Math.pow(10, scaling[1]))) - scaling[2]);
			output = scaling[0] * Math.pow(10, scaling[1]) * (input + scaling[2]);
			i++;
		}
		if (i == 0) {
			throw new FieldNotFoundException("Field is not found");
		}

	}

	@Override
	public Double getFieldByName(String name) throws FieldNotFoundException {
		if (name.equalsIgnoreCase("btu_kilo")) {
			return output;
		}
		throw new FieldNotFoundException("Field is not found");
	}

	@Override
	public short getIndex() {
		return index;
	}

	@Override
	public void setIndex(short index) {
		this.index = index;

	}

	@Override
	public int getSize() {
		return 2;
	}

	@Override
	public boolean isRoundtrip() {
		return ROUNDTRIP;
	}

}
