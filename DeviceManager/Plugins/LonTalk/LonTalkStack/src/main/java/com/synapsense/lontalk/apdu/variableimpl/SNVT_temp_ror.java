package com.synapsense.lontalk.apdu.variableimpl;

import java.io.Serializable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import com.synapsense.lontalk.apdu.variable.NetworkVariable;
import com.synapsense.lontalk.exception.FieldNotFoundException;
import com.synapsense.lontalk.exception.IncorrectValueException;

public class SNVT_temp_ror implements NetworkVariable {

	private short selector;
	private short index;
	private short input = 0;
	private double output = 0;
	private byte[] scaling = { 5, -1, 0 };
	private static final boolean ROUNDTRIP = false;

	@Override
	public void setSelector(short selector) {
		this.selector = selector;
	}

	@Override
	public short getSelector() {
		return selector;
	}

	@Override
	public byte[] getData() {
		byte[] a = new byte[2];
		a[1] = (byte) (input & 0xff);
		a[0] = (byte) ((input >>> 8) & 0xff);
		return a;
	}

	@Override
	public void setData(byte[] newdata) {
		ByteBuffer bb = ByteBuffer.allocate(2);
		bb.order(ByteOrder.LITTLE_ENDIAN);
		bb.put(newdata[1]);
		bb.put(newdata[0]);
		input = bb.getShort(0);
		output = scaling[0] * Math.pow(10, scaling[1]) * (input + scaling[2]);

	}

	@Override
	public void setFieldByName(String name, Serializable value) throws FieldNotFoundException, IncorrectValueException {
		int i = 0;
		if (name.equalsIgnoreCase("temp_ror")) {
			double a = (Double) value;
			if (a < (double) -16384.0 || a >= (double) 16383.5) {
				throw new IncorrectValueException("Value is not valid");
			}
			input = (short) ((a / (scaling[0] * Math.pow(10, scaling[1]))) - scaling[2]);
			output = scaling[0] * Math.pow(10, scaling[1]) * (input + scaling[2]);
			i++;
		}
		if (i == 0) {
			throw new FieldNotFoundException("Field is not found");
		}

	}

	@Override
	public Double getFieldByName(String name) throws FieldNotFoundException {
		if (name.equalsIgnoreCase("temp_ror")) {
			return output;
		}
		throw new FieldNotFoundException("Field is not found");
	}

	@Override
	public short getIndex() {
		return index;
	}

	@Override
	public void setIndex(short index) {
		this.index = index;

	}

	@Override
	public int getSize() {
		return 2;
	}

	@Override
	public boolean isRoundtrip() {
		return ROUNDTRIP;
	}

}
