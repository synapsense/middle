package com.synapsense.lontalk.apdu.variableimpl;

import java.io.Serializable;

import com.synapsense.lontalk.apdu.variable.NetworkVariable;
import com.synapsense.lontalk.exception.FieldNotFoundException;
import com.synapsense.lontalk.exception.IncorrectValueException;

public class SNVT_dev_c_mode implements NetworkVariable {

	private short selector;
	private short index;
	private byte input = 0;
	private double output = 0;
	private static final boolean ROUNDTRIP = false;

	// DCM_NUL -1 Invalid Value
	// DCM_SPEED_CONST 0
	// DCM_PRESS_CONST 1
	// DCM_PRESS_COMP 2
	// DCM_FLOW_CONST 3
	// DCM_FLOW_COMP 4
	// DCM_TEMP_CONST 5
	// DCM_TEMP_COMP 6
	// DCM_PRESS_AUTO 7
	// DCM_QUICK_OPEN 20 Valve works with Quick-Open flow characteristic
	// DCM_LINEAR 21 Valve works with Linear flow characteristics
	// DCM_EQUAL_PERCENT 22 Valve works with Equal Percent flow characteristics
	// DCM_QUADRATIC 23 Valve works with Quadratics flow characteristics
	// DCM_FREE_DEFINED 24 Valve works with free defined flow characteristic
	// DCM_2WAY_VALVE 27
	// DCM_MIXING_VALVE 28
	// DCM_DIVERTING_VALVE 29
	// DCM_INVFNC_QCK_OPN 30
	// DCM_INVFNC_EQL_PERC 31
	// DCM_INVFNC_QUAD 32

	@Override
	public byte[] getData() {
		byte[] a = { input };
		return a;
	}

	@Override
	public void setData(byte[] newdata) {
		input = newdata[0];
		output = (double) input;

	}

	@Override
	public short getSelector() {
		return selector;
	}

	@Override
	public void setSelector(short selector) {
		this.selector = selector;

	}

	@Override
	public void setFieldByName(String name, Serializable value) throws FieldNotFoundException, IncorrectValueException {
		int i = 0;
		if (name.equalsIgnoreCase("dev_c_mode")) {
			if ((Double) value < (double) -1 || (Double) value > (double) 32) {
				throw new IncorrectValueException("Value is not valid");
			}
			output = (Double) value;
			input = (byte) output;
			i++;
		}
		if (i == 0) {
			throw new FieldNotFoundException("Field is not found");
		}
	}

	@Override
	public Double getFieldByName(String name) throws FieldNotFoundException {
		if (name.equalsIgnoreCase("dev_c_mode")) {
			return output;
		}
		throw new FieldNotFoundException("Field is not found");
	}

	@Override
	public short getIndex() {
		return index;
	}

	@Override
	public void setIndex(short index) {
		this.index = index;

	}

	@Override
	public int getSize() {
		return 1;
	}

	@Override
	public boolean isRoundtrip() {
		return ROUNDTRIP;
	}

}
