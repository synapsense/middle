package com.synapsense.lontalk.transport.test;

import java.net.InetSocketAddress;

import mockit.Mocked;
import mockit.NonStrictExpectations;

import org.jboss.netty.bootstrap.ConnectionlessBootstrap;
import org.jboss.netty.channel.socket.DatagramChannel;
import org.junit.BeforeClass;
import org.junit.Test;

import com.synapsense.lontalk.apdu.variable.NetworkVariable;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_occupancy;
import com.synapsense.lontalk.api.Network;
import com.synapsense.lontalk.exception.IncorrectFormatException;
import com.synapsense.lontalk.node.CNPIPDevice;
import com.synapsense.lontalk.transport.UDPServer;
import com.synapsense.lontalk.transport.handler.ResponseHandler;
import com.synapsense.lontalk.transport.handler.ServerHandler;

public class UDPServerTest {
	@Mocked
	DatagramChannel c;
	@Mocked
	ConnectionlessBootstrap b;
	@Mocked
	ServerHandler serverHandler;
	@Mocked
	ResponseHandler responseHandler;

	static UDPServer server;
	static Network network;

	@BeforeClass
	public static void beforeTests() throws IncorrectFormatException {
		network = new Network("192.168.102.27", 1629, "192.168.102.27", 1628, "76", 0x01, 0x7f, "0A0A0A0A0A0A");
		server = new UDPServer(network);
	}

	@Test
	public void startTest() {

		new NonStrictExpectations() {
			{
				b.bind(new InetSocketAddress(network.pluginPort));
				result = c;
			}
		};
		server.start();
	}

	@Test
	public void writeTest() {
		NetworkVariable occupancy = new SNVT_occupancy();
		occupancy.setIndex((byte) 0x05);
		occupancy.setSelector((short) 0x3ffa);
		CNPIPDevice device = new CNPIPDevice("192.168.102.100", 1628, 5000, 3);

		new NonStrictExpectations() {
			{
				serverHandler.getResponseHandler();
				result = responseHandler;
				responseHandler.haveResponse();
				result = true;
			}
		};

		server.write(device, occupancy, responseHandler, new byte[2]);
	}
}
