package com.synapsense.lontalk.transport.handler;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.synapsense.lontalk.apdu.variable.NetworkVariable;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_amp;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_occupancy;
import com.synapsense.lontalk.exception.FieldNotFoundException;

public class ResponseHandlerTest {

	@Test
	public void ackTest() {
		byte[] ack = { 0x00, 0x1e, 0x01, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x6a, 0x00, 0x02, (byte) 0xa2,
		        (byte) 0xbd, 0x00, 0x00, 0x00, 0x00, 0x00, 0x09, 0x01, (byte) 0x8d, 0x01, (byte) 0xff, 0x76, 0x2e,
		        0x7d, (byte) 0x83 };
		ResponseHandler handler = new ResponseHandler(new SNVT_amp());
		handler.messageReceived(ack);
		assertEquals(true, handler.haveResponse());

	}

	@Test
	public void responseTest() throws FieldNotFoundException {
		byte[] response = { 0x00, 0x21, 0x01, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x6a, 0x00, 0x02,
		        (byte) 0xa2, (byte) 0xbd, 0x00, 0x00, 0x00, 0x00, 0x00, 0x19, 0x01, (byte) 0x8d, 0x01, (byte) 0xff,
		        0x76, 0x2f, 0x33, (byte) 0x05, 0x03, 0x56, (byte) 0xb0 };

		NetworkVariable nv = new SNVT_occupancy();
		nv.setIndex((byte) 5);
		ResponseHandler handler = new ResponseHandler(nv);
		handler.messageReceived(response);
		assertEquals(true, handler.haveResponse());
		assertEquals((double) 3, (double) nv.getFieldByName("occupancy"), 0);
	}

	@Test
	public void responseTest2() throws FieldNotFoundException {
		byte[] response = { 0x00, 0x23, 0x01, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x6a, 0x00, 0x02,
		        (byte) 0xa2, (byte) 0xbd, 0x00, 0x00, 0x00, 0x00, 0x00, 0x19, 0x01, (byte) 0x8d, 0x01, (byte) 0xff,
		        0x76, 0x2f, 0x33, (byte) 0xff, (byte) 0x00, 0x60, 0x03, 0x56, (byte) 0xb0 };

		NetworkVariable nv = new SNVT_occupancy();
		nv.setIndex((byte) 0xff);
		nv.setSelector((short) 0x60);
		ResponseHandler handler = new ResponseHandler(nv);
		handler.messageReceived(response);
		assertEquals(true, handler.haveResponse());
		assertEquals((double) 3, (double) nv.getFieldByName("occupancy"), 0);
	}

}
