package com.synapsense.lontalk.api.test;

import org.apache.log4j.Logger;

import com.synapsense.lontalk.apdu.variable.NetworkVariable;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_occupancy;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_temp_p;
import com.synapsense.lontalk.api.LonTalkStack;
import com.synapsense.lontalk.api.Network;
import com.synapsense.lontalk.exception.CouldNotGetData;
import com.synapsense.lontalk.exception.CouldNotSetData;
import com.synapsense.lontalk.exception.FieldNotFoundException;
import com.synapsense.lontalk.exception.IncorrectFormatException;
import com.synapsense.lontalk.exception.IncorrectValueException;
import com.synapsense.lontalk.node.CNPIPDevice;
import com.synapsense.lontalk.node.LonTalkNode;
import com.synapsense.lontalk.transport.handler.ServerHandler;

public class Test {
	private static final Logger logger = Logger.getLogger(ServerHandler.class);

	/**
	 * @param args
	 * @throws IncorrectFormatException
	 * @throws InterruptedException
	 */

	public static void main(String[] args) throws IncorrectFormatException, InterruptedException {
		Network network = new Network("192.168.102.27", 1629, "192.168.102.27", 1628, "76", 0x01, 0x7f, "0A0A0A0A0A0A");
		NetworkVariable temp = new SNVT_temp_p();
		temp.setIndex((byte) 0x06);
		NetworkVariable occupancy = new SNVT_occupancy();
		occupancy.setIndex((byte) 0x05);
		occupancy.setSelector((short) 0x3ffa);
		CNPIPDevice device = new CNPIPDevice("192.168.102.100", 1628, 5000, 3);
		LonTalkNode node = new LonTalkNode(0x01, 23);
		LonTalkStack stack = new LonTalkStack(network);
		stack.startStack();
		int count = 0;
		while (true) {
			try {
				System.out.println("**************************************");

				temp = stack.getNV(temp, device, node);
				count++;
				System.out.println("temp: " + temp.getFieldByName("temp_p"));

				occupancy = stack.getNV(occupancy, device, node);
				count++;
				System.out.println("occupancy " + occupancy.getFieldByName("occupancy"));

				occupancy.setFieldByName("occupancy", (double) 0);
				stack.setNV(occupancy, device, node);
				count++;

				occupancy = stack.getNV(occupancy, device, node);
				count++;
				System.out.println("occupancy: " + occupancy.getFieldByName("occupancy"));

				occupancy.setFieldByName("occupancy", (double) 1);
				stack.setNV(occupancy, device, node);
				count++;

				occupancy = stack.getNV(occupancy, device, node);
				count++;
				System.out.println("occupancy: " + occupancy.getFieldByName("occupancy"));

				occupancy.setFieldByName("occupancy", (double) 2);
				stack.setNV(occupancy, device, node);
				count++;

				occupancy = stack.getNV(occupancy, device, node);
				count++;
				System.out.println("occupancy: " + occupancy.getFieldByName("occupancy"));

				occupancy.setFieldByName("occupancy", (double) 3);
				stack.setNV(occupancy, device, node);
				count++;

				occupancy = stack.getNV(occupancy, device, node);
				count++;
				System.out.println("occupancy: " + occupancy.getFieldByName("occupancy"));

			} catch (FieldNotFoundException e) {
				logger.warn("field is not found" + count);
			} catch (CouldNotGetData e) {
				logger.warn("could not get data: request number " + count);
			} catch (CouldNotSetData e) {
				logger.warn("could not set data: request number " + count);
			} catch (IncorrectValueException e) {
				logger.warn("incorrect value");
			}
		}
	}

}
