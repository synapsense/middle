package com.synapsense.lontalk.api.test;

import org.junit.Test;

import com.synapsense.lontalk.apdu.variable.NetworkVariable;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_occupancy;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_temp_p;
import com.synapsense.lontalk.api.LonTalkStack;
import com.synapsense.lontalk.api.Network;
import com.synapsense.lontalk.exception.CouldNotGetData;
import com.synapsense.lontalk.exception.CouldNotSetData;
import com.synapsense.lontalk.exception.FieldNotFoundException;
import com.synapsense.lontalk.exception.IncorrectFormatException;
import com.synapsense.lontalk.exception.IncorrectValueException;
import com.synapsense.lontalk.node.CNPIPDevice;
import com.synapsense.lontalk.node.LonTalkNode;

public class FunctionalTest {

	/**
	 * @param args
	 * @throws IncorrectFormatException
	 * @throws InterruptedException
	 */
	@Test
	public void testStack() throws IncorrectFormatException, InterruptedException {
		Network network = new Network("192.168.102.27", 1629, "192.168.102.27", 1628, "76", 0x01, 0x7f, "0A0A0A0A0A0A");
		NetworkVariable temp = new SNVT_temp_p();
		temp.setIndex((byte) 0x06);
		NetworkVariable occupancy = new SNVT_occupancy();
		occupancy.setIndex((byte) 0x05);
		occupancy.setSelector((short) 0x3ffa);
		CNPIPDevice device = new CNPIPDevice("192.168.102.100", 1628, 50000, 2);
		LonTalkNode node = new LonTalkNode(0x01, 13);
		LonTalkStack stack = new LonTalkStack(network);
		stack.startStack();
		try {
			System.out.println("**************************************");

			temp = stack.getNV(temp, device, node);
			System.out.println("temp: " + temp.getFieldByName("temp_p"));

			occupancy = stack.getNV(occupancy, device, node);
			System.out.println("occupancy " + occupancy.getFieldByName("occupancy"));

			occupancy.setFieldByName("occupancy", (double) 0);
			stack.setNV(occupancy, device, node);

			occupancy = stack.getNV(occupancy, device, node);
			System.out.println("occupancy: " + occupancy.getFieldByName("occupancy"));

			occupancy.setFieldByName("occupancy", (double) 1);
			stack.setNV(occupancy, device, node);

			occupancy = stack.getNV(occupancy, device, node);
			System.out.println("occupancy: " + occupancy.getFieldByName("occupancy"));

			occupancy.setFieldByName("occupancy", (double) 2);
			stack.setNV(occupancy, device, node);

			occupancy = stack.getNV(occupancy, device, node);
			System.out.println("occupancy: " + occupancy.getFieldByName("occupancy"));

			occupancy.setFieldByName("occupancy", (double) 3);
			stack.setNV(occupancy, device, node);

			occupancy = stack.getNV(occupancy, device, node);
			System.out.println("occupancy: " + occupancy.getFieldByName("occupancy"));

		} catch (FieldNotFoundException e) {
			System.out.println("field is not found");
		} catch (CouldNotGetData e) {
			System.out.println("could not get data");
		} catch (CouldNotSetData e) {
			System.out.println("could not set data");
		} catch (IncorrectValueException e) {
			System.out.println("invalid value");
		}
	}

}
