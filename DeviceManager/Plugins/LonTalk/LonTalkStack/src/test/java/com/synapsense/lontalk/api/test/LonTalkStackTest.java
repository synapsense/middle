package com.synapsense.lontalk.api.test;

import static org.junit.Assert.assertEquals;
import mockit.Mocked;
import mockit.NonStrictExpectations;

import org.junit.Test;

import com.synapsense.lontalk.apdu.variable.NetworkVariable;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_occupancy;
import com.synapsense.lontalk.api.LonTalkStack;
import com.synapsense.lontalk.api.Network;
import com.synapsense.lontalk.exception.CouldNotGetData;
import com.synapsense.lontalk.exception.CouldNotSetData;
import com.synapsense.lontalk.exception.IncorrectFormatException;
import com.synapsense.lontalk.node.CNPIPDevice;
import com.synapsense.lontalk.node.LonTalkNode;
import com.synapsense.lontalk.transport.UDPServer;
import com.synapsense.lontalk.transport.handler.ResponseHandler;

public class LonTalkStackTest {
	@Mocked
	UDPServer server;

	@Test
	public void testStack() throws IncorrectFormatException, CouldNotGetData, CouldNotSetData {
		Network network = new Network("192.168.102.27", 1629, "192.168.102.27", 1628, "767676", 0x01, 0x7f,
		        "0A0A0A0A0A0A");
		LonTalkStack stack = network.getStack();
		final CNPIPDevice device = new CNPIPDevice("192.168.102.100", 1628, 50000, 2);
		LonTalkNode node = new LonTalkNode(0x01, 13);
		final NetworkVariable occupancy = new SNVT_occupancy();
		occupancy.setIndex((byte) 0x05);
		occupancy.setSelector((short) 0x3ffa);
		new NonStrictExpectations() {
			{
				server.start();
				server.stop();
				server.write(device, occupancy, (ResponseHandler) any, (byte[]) any);
				result = occupancy;
			}
		};
		stack.startStack();
		NetworkVariable nv = stack.getNV(occupancy, device, node);
		assertEquals(nv.getIndex(), occupancy.getIndex());
		stack.setNV(nv, device, node);
		stack.stopStack();
	}
}
