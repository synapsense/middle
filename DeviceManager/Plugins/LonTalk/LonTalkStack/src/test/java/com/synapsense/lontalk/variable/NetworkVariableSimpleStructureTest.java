package com.synapsense.lontalk.variable;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.synapsense.lontalk.apdu.variable.NetworkVariable;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_Wm2_p;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_abs_humid;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_address;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_amp;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_amp_ac;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_amp_ac_mil;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_amp_f;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_amp_mil;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_angle;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_angle_deg;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_angle_f;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_angle_vel;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_angle_vel_f;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_btu_f;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_btu_kilo;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_btu_mega;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_defr_mode;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_defr_state;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_defr_term;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_dev_c_mode;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_elec_kwh;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_elec_kwh_l;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_elec_whr;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_elec_whr_f;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_ent_opmode;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_ent_state;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_fire_indcte;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_fire_init;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_freq_f;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_freq_hz;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_freq_kilohz;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_freq_milhz;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_hvac_emerg;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_hvac_mode;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_hvac_type;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_motor_state;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_occupancy;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_power;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_power_f;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_power_kilo;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_ppm;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_ppm_f;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_res;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_res_f;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_res_kilo;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_temp;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_temp_diff_p;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_temp_f;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_temp_p;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_temp_ror;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_therm_mode;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_volt;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_volt_ac;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_volt_dbmv;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_volt_f;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_volt_kilo;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_volt_mil;
import com.synapsense.lontalk.exception.FieldNotFoundException;
import com.synapsense.lontalk.exception.IncorrectValueException;

public class NetworkVariableSimpleStructureTest {

	@Test
	public void SNVT_abs_humid_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_abs_humid();
		nv.setFieldByName("abs_humid", (double) 655.34);
		byte[] a = nv.getData();
		nv.setData(a);
		assertEquals((double) 655.34, (double) nv.getFieldByName("abs_humid"), 0);
	}

	@Test(expected = IncorrectValueException.class)
	public void SNVT_abs_humid_exception_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_abs_humid();
		nv.setFieldByName("abs_humid", (double) 655.35);
	}

	@Test
	public void SNVT_address_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_address();
		nv.setFieldByName("address", (double) 64767);
		byte[] a = nv.getData();
		nv.setData(a);
		assertEquals((double) 64767, (double) nv.getFieldByName("address"), 0);
	}

	@Test(expected = IncorrectValueException.class)
	public void SNVT_address_exception_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_address();
		nv.setFieldByName("address", (double) 65535);
	}

	@Test
	public void SNVT_amp_ac_mil_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_amp_ac_mil();
		nv.setFieldByName("amp_ac_mil", (double) 63000);
		byte[] a = nv.getData();
		nv.setData(a);
		assertEquals((double) 63000, (double) nv.getFieldByName("amp_ac_mil"), 0);
	}

	@Test(expected = IncorrectValueException.class)
	public void SNVT_amp_ac_mil_exception_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_amp_ac_mil();
		nv.setFieldByName("amp_ac_mil", (double) 65535);
	}

	@Test
	public void SNVT_amp_ac_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_amp_ac();
		nv.setFieldByName("amp_ac", (double) 63000);
		byte[] a = nv.getData();
		nv.setData(a);
		assertEquals((double) 63000, (double) nv.getFieldByName("amp_ac"), 0);
	}

	@Test(expected = IncorrectValueException.class)
	public void SNVT_amp_ac_exception_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_amp_ac();
		nv.setFieldByName("amp_ac", (double) 65535);
	}

	@Test
	public void SNVT_amp_f_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_amp_f();
		nv.setFieldByName("amp_f", (double) 63000);
		byte[] a = nv.getData();
		nv.setData(a);
		assertEquals((double) 63000, (double) nv.getFieldByName("amp_f"), 0);
	}

	@Test(expected = IncorrectValueException.class)
	public void SNVT_amp_f_exception_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_amp_f();
		nv.setFieldByName("amp_f", (double) 4.4028234663853e+38d);
	}

	@Test
	public void SNVT_amp_mil_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_amp_mil();
		nv.setFieldByName("amp_mil", (double) 327.6);
		byte[] a = nv.getData();
		nv.setData(a);
		assertEquals((double) 327.6, (double) nv.getFieldByName("amp_mil"), 0);
	}

	@Test(expected = IncorrectValueException.class)
	public void SNVT_amp_mil_exception_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_amp_mil();
		nv.setFieldByName("amp_mil", (double) 3276.9);
	}

	@Test
	public void SNVT_amp_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_amp();
		nv.setFieldByName("amp", (double) 650);
		byte[] a = nv.getData();
		nv.setData(a);
		assertEquals((double) 650, (double) nv.getFieldByName("amp"), 0);
	}

	@Test(expected = IncorrectValueException.class)
	public void SNVT_amp_exception_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_amp();
		nv.setFieldByName("amp", (double) 3276.9);
	}

	@Test
	public void SNVT_angle_deg_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_angle_deg();
		nv.setFieldByName("angle_deg", (double) 359.1);
		byte[] a = nv.getData();
		nv.setData(a);
		assertEquals((double) 359.1, (double) nv.getFieldByName("angle_deg"), 0);
	}

	@Test(expected = IncorrectValueException.class)
	public void SNVT_angle_deg_exception_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_angle_deg();
		nv.setFieldByName("angle_deg", (double) 361.00);
	}

	@Test
	public void SNVT_angle_f_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_angle_f();
		nv.setFieldByName("angle_f", (double) 564.89);
		byte[] a = nv.getData();
		nv.setData(a);
		assertEquals((double) 564.89, (double) nv.getFieldByName("angle_f"), 2);
	}

	@Test(expected = IncorrectValueException.class)
	public void SNVT_angle_f_exception_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_angle_f();
		nv.setFieldByName("angle_f", (double) -4.5028234663853E+38d);
	}

	@Test
	public void SNVT_angle_vel_f_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_angle_vel_f();
		nv.setFieldByName("angle_vel_f", (double) 564.89);
		byte[] a = nv.getData();
		nv.setData(a);
		assertEquals((double) 564.89, (double) nv.getFieldByName("angle_vel_f"), 2);
	}

	@Test(expected = IncorrectValueException.class)
	public void SNVT_angle_vel_f_exception_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_angle_vel_f();
		nv.setFieldByName("angle_vel_f", (double) -4.5028234663853E+38d);

	}

	@Test
	public void SNVT_angle_vel_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_angle_vel();
		nv.setFieldByName("angle_vel", (double) 1000);
		byte[] a = nv.getData();
		nv.setData(a);
		assertEquals((double) 1000, (double) nv.getFieldByName("angle_vel"), 0);
	}

	@Test(expected = IncorrectValueException.class)
	public void SNVT_angle_vel_exception_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_angle_vel();
		nv.setFieldByName("angle_vel", (double) 3276.8);
	}

	@Test
	public void SNVT_angle_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_angle();
		nv.setFieldByName("angle", (double) 56.3);
		byte[] a = nv.getData();
		nv.setData(a);
		assertEquals((double) 56.3, (double) nv.getFieldByName("angle"), 1);
	}

	@Test(expected = IncorrectValueException.class)
	public void SNVT_angle_exception_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_angle();
		nv.setFieldByName("angle", (double) -4);
	}

	@Test
	public void SNVT_btu_f_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_btu_f();
		nv.setFieldByName("btu_f", (double) 564.89);
		byte[] a = nv.getData();
		nv.setData(a);
		assertEquals((double) 564.89, (double) nv.getFieldByName("btu_f"), 2);
	}

	@Test(expected = IncorrectValueException.class)
	public void SNVT_btu_f_exception_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_btu_f();
		nv.setFieldByName("btu_f", (double) -3);
	}

	@Test
	public void SNVT_btu_kilo_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_btu_kilo();
		nv.setFieldByName("btu_kilo", (double) 65000);
		byte[] a = nv.getData();
		nv.setData(a);
		assertEquals((double) 65000, (double) nv.getFieldByName("btu_kilo"), 0);
	}

	@Test(expected = IncorrectValueException.class)
	public void SNVT_btu_kilo_exception_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_btu_kilo();
		nv.setFieldByName("btu_kilo", (double) 66000);
	}

	@Test
	public void SNVT_btu_mega_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_btu_mega();
		nv.setFieldByName("btu_mega", (double) 65000);
		byte[] a = nv.getData();
		nv.setData(a);
		assertEquals((double) 65000, (double) nv.getFieldByName("btu_mega"), 0);
	}

	@Test(expected = IncorrectValueException.class)
	public void SNVT_btu_mega_exception_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_btu_mega();
		nv.setFieldByName("btu_mega", (double) -65000);
	}

	@Test
	public void SNVT_defr_mode_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_defr_mode();
		nv.setFieldByName("defr_mode", (double) 2);
		byte[] a = nv.getData();
		nv.setData(a);
		assertEquals((double) 2, (double) nv.getFieldByName("defr_mode"), 0);
	}

	@Test(expected = IncorrectValueException.class)
	public void SNVT_defr_mode_exception_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_defr_mode();
		nv.setFieldByName("defr_mode", (double) 4);
	}

	@Test
	public void SNVT_defr_state_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_defr_state();
		nv.setFieldByName("defr_state", (double) 2);
		byte[] a = nv.getData();
		nv.setData(a);
		assertEquals((double) 2, (double) nv.getFieldByName("defr_state"), 0);
	}

	@Test(expected = IncorrectValueException.class)
	public void SNVT_defr_state_exception_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_defr_state();
		nv.setFieldByName("defr_state", (double) 5);
	}

	@Test
	public void SNVT_defr_term_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_defr_term();
		nv.setFieldByName("defr_term", (double) 4);
		byte[] a = nv.getData();
		nv.setData(a);
		assertEquals((double) 4, (double) nv.getFieldByName("defr_term"), 0);
	}

	@Test(expected = IncorrectValueException.class)
	public void SNVT_defr_term_exception_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_defr_term();
		nv.setFieldByName("defr_term", (double) 9);
	}

	@Test
	public void SNVT_dev_c_mode_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_dev_c_mode();
		nv.setFieldByName("dev_c_mode", (double) 32);
		byte[] a = nv.getData();
		nv.setData(a);
		assertEquals((double) 32, (double) nv.getFieldByName("dev_c_mode"), 0);
	}

	@Test(expected = IncorrectValueException.class)
	public void SNVT_dev_c_mode_exception_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_dev_c_mode();
		nv.setFieldByName("dev_c_mode", (double) 34);
	}

	@Test
	public void SNVT_elec_kwh_l_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_elec_kwh_l();
		nv.setFieldByName("elec_kwh_l", (double) 200000000);
		byte[] a = nv.getData();
		nv.setData(a);
		assertEquals((double) 200000000, (double) nv.getFieldByName("elec_kwh_l"), 0);
	}

	@Test(expected = IncorrectValueException.class)
	public void SNVT_elec_kwh_l_exception_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_elec_kwh_l();
		nv.setFieldByName("elec_kwh_l", (double) 300000000);
	}

	@Test
	public void SNVT_elec_kwh_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_elec_kwh();
		nv.setFieldByName("elec_kwh", (double) 65500);
		byte[] a = nv.getData();
		nv.setData(a);
		assertEquals((double) 65500, (double) nv.getFieldByName("elec_kwh"), 0);
	}

	@Test(expected = IncorrectValueException.class)
	public void SNVT_elec_kwh_exception_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_elec_kwh();
		nv.setFieldByName("elec_kwh", (double) 66500);
	}

	@Test
	public void SNVT_elec_whr_f_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_elec_whr_f();
		nv.setFieldByName("elec_whr_f", (double) 65500);
		byte[] a = nv.getData();
		nv.setData(a);
		assertEquals((double) 65500, (double) nv.getFieldByName("elec_whr_f"), 0);
	}

	@Test(expected = IncorrectValueException.class)
	public void SNVT_elec_whr_f_exception_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_elec_whr_f();
		nv.setFieldByName("elec_whr_f", (double) -65500);

	}

	@Test
	public void SNVT_elec_whr_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_elec_whr();
		nv.setFieldByName("elec_whr", (double) 6552.3);
		byte[] a = nv.getData();
		nv.setData(a);
		assertEquals((double) 6552.3, (double) nv.getFieldByName("elec_whr"), 2);
	}

	@Test(expected = IncorrectValueException.class)
	public void SNVT_elec_whr_exception_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_elec_whr();
		nv.setFieldByName("elec_whr", (double) 6652.3);
	}

	@Test
	public void SNVT_ent_opmode_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_ent_opmode();
		nv.setFieldByName("ent_opmode", (double) 15);
		byte[] a = nv.getData();
		nv.setData(a);
		assertEquals((double) 15, (double) nv.getFieldByName("ent_opmode"), 0);
	}

	@Test(expected = IncorrectValueException.class)
	public void SNVT_ent_opmode_exception_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_ent_opmode();
		nv.setFieldByName("ent_opmode", (double) 17);
	}

	@Test
	public void SNVT_ent_state_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_ent_state();
		nv.setFieldByName("ent_state", (double) 11);
		byte[] a = nv.getData();
		nv.setData(a);
		assertEquals((double) 11, (double) nv.getFieldByName("ent_state"), 0);
	}

	@Test(expected = IncorrectValueException.class)
	public void SNVT_ent_state_exception_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_ent_state();
		nv.setFieldByName("ent_state", (double) 14);
	}

	@Test
	public void SNVT_fire_indcte_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_fire_indcte();
		nv.setFieldByName("fire_indcte", (double) 4);
		byte[] a = nv.getData();
		nv.setData(a);
		assertEquals((double) 4, (double) nv.getFieldByName("fire_indcte"), 0);
	}

	@Test(expected = IncorrectValueException.class)
	public void SNVT_fire_indcte_exception_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_fire_indcte();
		nv.setFieldByName("fire_indcte", (double) 9);
	}

	@Test
	public void SNVT_fire_init_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_fire_init();
		nv.setFieldByName("fire_init", (double) 4);
		byte[] a = nv.getData();
		nv.setData(a);
		assertEquals((double) 4, (double) nv.getFieldByName("fire_init"), 0);
	}

	@Test(expected = IncorrectValueException.class)
	public void SNVT_fire_init_exception_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_fire_init();
		nv.setFieldByName("fire_init", (double) 17);
	}

	@Test
	public void SNVT_freq_f_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_freq_f();
		nv.setFieldByName("freq_f", (double) 4000);
		byte[] a = nv.getData();
		nv.setData(a);
		assertEquals((double) 4000, (double) nv.getFieldByName("freq_f"), 0);
	}

	@Test(expected = IncorrectValueException.class)
	public void SNVT_freq_f_exception_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_freq_f();
		nv.setFieldByName("freq_f", (double) -4000);
	}

	@Test
	public void SNVT_freq_hz_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_freq_hz();
		nv.setFieldByName("freq_hz", (double) 4000.1);
		byte[] a = nv.getData();
		nv.setData(a);
		assertEquals((double) 4000.1, (double) nv.getFieldByName("freq_hz"), 1);
	}

	@Test(expected = IncorrectValueException.class)
	public void SNVT_freq_hz_exception_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_freq_hz();
		nv.setFieldByName("freq_hz", (double) -5);
	}

	@Test
	public void SNVT_freq_kilohz_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_freq_kilohz();
		nv.setFieldByName("freq_kilohz", (double) 4000.1);
		byte[] a = nv.getData();
		nv.setData(a);
		assertEquals((double) 4000.1, (double) nv.getFieldByName("freq_kilohz"), 1);
	}

	@Test(expected = IncorrectValueException.class)
	public void SNVT_freq_kilohz_exception_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_freq_kilohz();
		nv.setFieldByName("freq_kilohz", (double) -4000);
	}

	@Test
	public void SNVT_freq_milhz_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_freq_milhz();
		nv.setFieldByName("freq_milhz", (double) 4.3);
		byte[] a = nv.getData();
		nv.setData(a);
		assertEquals((double) 4.3, (double) nv.getFieldByName("freq_milhz"), 1);
	}

	@Test(expected = IncorrectValueException.class)
	public void SNVT_freq_milhz_exception_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_freq_milhz();
		nv.setFieldByName("freq_milhz", (double) 7.3);
	}

	@Test
	public void SNVT_hvac_emerg_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_hvac_emerg();
		nv.setFieldByName("hvac_emerg", (double) 4);
		byte[] a = nv.getData();
		nv.setData(a);
		assertEquals((double) 4, (double) nv.getFieldByName("hvac_emerg"), 0);
	}

	@Test(expected = IncorrectValueException.class)
	public void SNVT_hvac_emerg_exception_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_hvac_emerg();
		nv.setFieldByName("hvac_emerg", (double) 7);
	}

	@Test
	public void SNVT_hvac_mode_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_hvac_mode();
		nv.setFieldByName("hvac_mode", (double) 4);
		byte[] a = nv.getData();
		nv.setData(a);
		assertEquals((double) 4, (double) nv.getFieldByName("hvac_mode"), 0);
	}

	@Test(expected = IncorrectValueException.class)
	public void SNVT_hvac_mode_exception_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_hvac_mode();
		nv.setFieldByName("hvac_mode", (double) 24);
	}

	@Test
	public void SNVT_hvac_type_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_hvac_type();
		nv.setFieldByName("hvac_type", (double) 4);
		byte[] a = nv.getData();
		nv.setData(a);
		assertEquals((double) 4, (double) nv.getFieldByName("hvac_type"), 0);
	}

	@Test(expected = IncorrectValueException.class)
	public void SNVT_hvac_type_exception_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_hvac_type();
		nv.setFieldByName("hvac_type", (double) 10);
	}

	@Test
	public void SNVT_motor_state_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_motor_state();
		nv.setFieldByName("motor_state", (double) 4);
		byte[] a = nv.getData();
		nv.setData(a);
		assertEquals((double) 4, (double) nv.getFieldByName("motor_state"), 0);
	}

	@Test(expected = IncorrectValueException.class)
	public void SNVT_motor_state_exception_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_motor_state();
		nv.setFieldByName("motor_state", (double) 8);
	}

	@Test
	public void SNVT_occupancy_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_occupancy();
		nv.setFieldByName("occupancy", (double) 3);
		byte[] a = nv.getData();
		nv.setData(a);
		assertEquals((double) 3, (double) nv.getFieldByName("occupancy"), 0);
	}

	@Test(expected = IncorrectValueException.class)
	public void SNVT_occupancy_exception_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_occupancy();
		nv.setFieldByName("occupancy", (double) 4);
	}

	@Test
	public void SNVT_power_f_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_power_f();
		nv.setFieldByName("power_f", (double) 345.3);
		byte[] a = nv.getData();
		nv.setData(a);
		assertEquals((double) 345.3, (double) nv.getFieldByName("power_f"), 1);
	}

	@Test(expected = IncorrectValueException.class)
	public void SNVT_power_f_exception_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_power_f();
		nv.setFieldByName("power_f", (double) -4.4028234663853E+038d);
	}

	@Test
	public void SNVT_power_kilo_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_power_kilo();
		nv.setFieldByName("power_kilo", (double) 3456.4);
		byte[] a = nv.getData();
		nv.setData(a);
		assertEquals((double) 3456.4, (double) nv.getFieldByName("power_kilo"), 0);
	}

	@Test(expected = IncorrectValueException.class)
	public void SNVT_power_kilo_exception_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_power_kilo();
		nv.setFieldByName("power_kilo", (double) -3456.4);
	}

	@Test
	public void SNVT_power_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_power();
		nv.setFieldByName("power", (double) 3456.4);
		byte[] a = nv.getData();
		nv.setData(a);
		assertEquals((double) 3456.4, (double) nv.getFieldByName("power"), 0);
	}

	@Test(expected = IncorrectValueException.class)
	public void SNVT_power_exception_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_power();
		nv.setFieldByName("power", (double) 6553.6);

	}

	@Test
	public void SNVT_ppm_f_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_ppm_f();
		nv.setFieldByName("ppm_f", (double) 3456.4);
		byte[] a = nv.getData();
		nv.setData(a);
		assertEquals((double) 3456.4, (double) nv.getFieldByName("ppm_f"), 2);
	}

	@Test(expected = IncorrectValueException.class)
	public void SNVT_ppm_f_exception_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_ppm_f();
		nv.setFieldByName("ppm_f", (double) -3456.4);
	}

	@Test
	public void SNVT_ppm_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_ppm();
		nv.setFieldByName("ppm", (double) 65000);
		byte[] a = nv.getData();
		nv.setData(a);
		assertEquals((double) 65000, (double) nv.getFieldByName("ppm"), 2);
	}

	@Test(expected = IncorrectValueException.class)
	public void SNVT_ppm_exception_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_ppm();
		nv.setFieldByName("ppm", (double) 65537);
	}

	@Test
	public void SNVT_res_f_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_res_f();
		nv.setFieldByName("res_f", (double) 3456.4);
		byte[] a = nv.getData();
		nv.setData(a);
		assertEquals((double) 3456.4, (double) nv.getFieldByName("res_f"), 2);
	}

	@Test(expected = IncorrectValueException.class)
	public void SNVT_res_f_exception_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_res_f();
		nv.setFieldByName("res_f", (double) -3456.4);
	}

	@Test
	public void SNVT_res_kilo_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_res_kilo();
		nv.setFieldByName("res_kilo", (double) 6500.0);
		byte[] a = nv.getData();
		nv.setData(a);
		assertEquals((double) 6500.0, (double) nv.getFieldByName("res_kilo"), 2);
	}

	@Test(expected = IncorrectValueException.class)
	public void SNVT_res_kilo_exception_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_res_kilo();
		nv.setFieldByName("res_kilo", (double) -6500.0);
	}

	@Test
	public void SNVT_res_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_res();
		nv.setFieldByName("res", (double) 6500.0);
		byte[] a = nv.getData();
		nv.setData(a);
		assertEquals((double) 6500.0, (double) nv.getFieldByName("res"), 2);
	}

	@Test(expected = IncorrectValueException.class)
	public void SNVT_res_exception_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_res();
		nv.setFieldByName("res", (double) 65600);
	}

	@Test
	public void SNVT_temp_diff_p_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_temp_diff_p();
		nv.setFieldByName("temp_diff_p", (double) 323.4);
		byte[] a = nv.getData();
		nv.setData(a);
		assertEquals((double) 323.4, (double) nv.getFieldByName("temp_diff_p"), 1);
	}

	@Test(expected = IncorrectValueException.class)
	public void SNVT_temp_diff_p_exception_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_temp_diff_p();
		nv.setFieldByName("temp_diff_p", (double) 328.4);
	}

	@Test
	public void SNVT_temp_f_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_temp_f();
		nv.setFieldByName("temp_f", (double) 3456.7);
		byte[] a = nv.getData();
		nv.setData(a);
		assertEquals((double) 3456.7, (double) nv.getFieldByName("temp_f"), 1);
	}

	@Test(expected = IncorrectValueException.class)
	public void SNVT_temp_f_exception_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_temp_f();
		nv.setFieldByName("temp_f", (double) -3456.7);
	}

	@Test
	public void SNVT_temp_p_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_temp_p();
		nv.setFieldByName("temp_p", (double) 327.3);
		byte[] a = nv.getData();
		nv.setData(a);
		assertEquals((double) 327.3, (double) nv.getFieldByName("temp_p"), 1);
	}

	@Test(expected = IncorrectValueException.class)
	public void SNVT_temp_p_exception_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_temp_p();
		nv.setFieldByName("temp_p", (double) -280.4);
	}

	@Test
	public void SNVT_temp_ror_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_temp_ror();
		nv.setFieldByName("temp_ror", (double) 15000);
		byte[] a = nv.getData();
		nv.setData(a);
		assertEquals((double) 15000, (double) nv.getFieldByName("temp_ror"), 1);
	}

	@Test(expected = IncorrectValueException.class)
	public void SNVT_temp_ror_exception_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_temp_ror();
		nv.setFieldByName("temp_ror", (double) 16385);
	}

	@Test
	public void SNVT_temp_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_temp();
		nv.setFieldByName("temp", (double) 36.6);
		byte[] a = nv.getData();
		nv.setData(a);
		assertEquals((double) 36.6, (double) nv.getFieldByName("temp"), 1);
	}

	@Test(expected = IncorrectValueException.class)
	public void SNVT_temp_exception_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_temp();
		nv.setFieldByName("temp", (double) -366);
	}

	@Test
	public void SNVT_therm_mode_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_therm_mode();
		nv.setFieldByName("therm_mode", (double) 1);
		byte[] a = nv.getData();
		nv.setData(a);
		assertEquals((double) 1, (double) nv.getFieldByName("therm_mode"), 1);
	}

	@Test(expected = IncorrectValueException.class)
	public void SNVT_therm_mode_exception_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_therm_mode();
		nv.setFieldByName("therm_mode", (double) 3);
	}

	@Test
	public void SNVT_volt_ac_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_volt_ac();
		nv.setFieldByName("volt_ac", (double) 6500);
		byte[] a = nv.getData();
		nv.setData(a);
		assertEquals((double) 6500, (double) nv.getFieldByName("volt_ac"), 1);
	}

	@Test(expected = IncorrectValueException.class)
	public void SNVT_volt_ac_exception_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_volt_ac();
		nv.setFieldByName("volt_ac", (double) 65537);
	}

	@Test
	public void SNVT_volt_dbmv_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_volt_dbmv();
		nv.setFieldByName("volt_dbmv", (double) 310.1);
		byte[] a = nv.getData();
		nv.setData(a);
		assertEquals((double) 310.1, (double) nv.getFieldByName("volt_dbmv"), 1);
	}

	@Test(expected = IncorrectValueException.class)
	public void SNVT_volt__exception_dbmv_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_volt_dbmv();
		nv.setFieldByName("volt_dbmv", (double) 330.2);
	}

	@Test
	public void SNVT_volt_f_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_volt_f();
		nv.setFieldByName("volt_f", (double) 310.1);
		byte[] a = nv.getData();
		nv.setData(a);
		assertEquals((double) 310.1, (double) nv.getFieldByName("volt_f"), 1);
	}

	@Test(expected = IncorrectValueException.class)
	public void SNVT_volt_f_expected_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_volt_f();
		nv.setFieldByName("volt_f", (double) -310.1);
	}

	@Test
	public void SNVT_volt_kilo_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_volt_kilo();
		nv.setFieldByName("volt_kilo", (double) 3210.1);
		byte[] a = nv.getData();
		nv.setData(a);
		assertEquals((double) 3210.1, (double) nv.getFieldByName("volt_kilo"), 1);
	}

	@Test(expected = IncorrectValueException.class)
	public void SNVT_volt_kilo_exception_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_volt_kilo();
		nv.setFieldByName("volt_kilo", (double) 3276.8);
	}

	@Test
	public void SNVT_volt_mil_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_volt_mil();
		nv.setFieldByName("volt_mil", (double) 3210.1);
		byte[] a = nv.getData();
		nv.setData(a);
		assertEquals((double) 3210.1, (double) nv.getFieldByName("volt_mil"), 1);
	}

	@Test(expected = IncorrectValueException.class)
	public void SNVT_volt_mil_exception_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_volt_mil();
		nv.setFieldByName("volt_mil", (double) -3276.9);
	}

	@Test
	public void SNVT_volt_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_volt();
		nv.setFieldByName("volt", (double) 3210.1);
		byte[] a = nv.getData();
		nv.setData(a);
		assertEquals((double) 3210.1, (double) nv.getFieldByName("volt"), 1);
	}

	@Test(expected = IncorrectValueException.class)
	public void SNVT_volt_exception_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_volt();
		nv.setFieldByName("volt", (double) 3276.9);
	}

	@Test
	public void SNVT_Wm2_p_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_Wm2_p();
		nv.setFieldByName("Wm2_p", (double) 3210.1);
		byte[] a = nv.getData();
		nv.setData(a);
		assertEquals((double) 3210.1, (double) nv.getFieldByName("Wm2_p"), 1);
	}

	@Test(expected = IncorrectValueException.class)
	public void SNVT_Wm2_p_exception_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_Wm2_p();
		nv.setFieldByName("Wm2_p", (double) -3210.1);
	}
}
