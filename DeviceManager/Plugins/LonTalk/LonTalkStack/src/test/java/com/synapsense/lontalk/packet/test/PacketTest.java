package com.synapsense.lontalk.packet.test;

import static org.junit.Assert.assertEquals;

import java.net.UnknownHostException;

import org.junit.Before;
import org.junit.Test;

import com.synapsense.lontalk.apdu.variable.NetworkVariable;
import com.synapsense.lontalk.apdu.variableimpl.SNVT_temp_p;
import com.synapsense.lontalk.api.Network;
import com.synapsense.lontalk.exception.IncorrectFormatException;
import com.synapsense.lontalk.node.LonTalkNode;
import com.synapsense.lontalk.packet.Acknowledged;
import com.synapsense.lontalk.packet.ChannelRouting;
import com.synapsense.lontalk.packet.DataPacket;
import com.synapsense.lontalk.packet.RegistrationPacket;

public class PacketTest {
	Network network;

	@Before
	public void beforeTest() throws IncorrectFormatException {
		network = new Network("192.168.102.27", 1629, "192.168.102.27", 1628, "76", 0x01, 0x7f, "0A0A0A0A0A0A");
	}

	@Test
	public void acknowledgedTest() {
		Acknowledged acknowledged = new Acknowledged();
		byte[] packet = acknowledged.getPacket();
		assertEquals(0, packet[0]);
		assertEquals(28, packet[1]);
		assertEquals(1, packet[2]);
		assertEquals(7, packet[3]);

		assertEquals((byte) 0xd2, packet[20]);
		assertEquals((byte) 0xa2, packet[21]);
		assertEquals((byte) 0xd2, packet[22]);
		assertEquals((byte) 0x5d, packet[23]);
	}

	@Test
	public void channelRoutingTest() throws UnknownHostException {
		ChannelRouting routing = new ChannelRouting();
		byte[] packet = routing.getChannelRoutingPacket(network);
		assertEquals(0, packet[0]);
		assertEquals((byte) 0x8A, packet[1]);
		assertEquals(1, packet[2]);
		assertEquals(8, packet[3]);

		assertEquals((byte) 0x0A, packet[48]);
		assertEquals((byte) 0x0A, packet[49]);
		assertEquals((byte) 0x0A, packet[50]);
		assertEquals((byte) 0x0A, packet[51]);
		assertEquals((byte) 0x0A, packet[52]);
		assertEquals((byte) 0x0A, packet[53]);

		assertEquals((byte) 0x76, packet[132]);
	}

	@Test
	public void registrationPacketTest() throws UnknownHostException {
		RegistrationPacket registration = new RegistrationPacket();
		byte[] packet = registration.getRegistrationPacket(network);

		assertEquals(0, packet[0]);
		assertEquals((byte) 81, packet[1]);
		assertEquals(1, packet[2]);
		assertEquals(3, packet[3]);

		assertEquals((byte) 0x0A, packet[68]);
		assertEquals((byte) 0x0A, packet[69]);
		assertEquals((byte) 0x0A, packet[70]);
		assertEquals((byte) 0x0A, packet[71]);
		assertEquals((byte) 0x0A, packet[72]);
		assertEquals((byte) 0x0A, packet[73]);
	}

	@Test
	public void dataPacketTest() {
		LonTalkNode node = new LonTalkNode(1, 1);
		NetworkVariable nv = new SNVT_temp_p();
		nv.setIndex((byte) 7);
		nv.setSelector((short) 100);
		byte[] packet = DataPacket.getFetchNVDataPacket(network, node, nv);
		assertEquals(0, packet[0]);
		assertEquals((byte) 32, packet[1]);
		assertEquals(1, packet[2]);
		assertEquals(1, packet[3]);

		assertEquals(7, packet[29]);

		packet = DataPacket.getSetNVDataPacket(network, node, nv);
		assertEquals(0, packet[0]);
		assertEquals((byte) 34, packet[1]);
		assertEquals(1, packet[2]);
		assertEquals(1, packet[3]);

		assertEquals(0, packet[31]);

		nv.setIndex((short) 0xff);
		packet = DataPacket.getFetchNVDataPacket(network, node, nv);
		assertEquals(0, packet[0]);
		assertEquals((byte) 34, packet[1]);
		assertEquals(1, packet[2]);
		assertEquals(1, packet[3]);

		assertEquals(-1, packet[29]);
		assertEquals(0, packet[30]);
		assertEquals(100, packet[31]);

	}
}
