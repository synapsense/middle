package com.synapsense.lontalk.variable;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.synapsense.lontalk.apdu.variable.NetworkVariable;
import com.synapsense.lontalk.apdu.variableimpl.different.SNVT_ctrl_req;
import com.synapsense.lontalk.apdu.variableimpl.different.SNVT_file_pos;
import com.synapsense.lontalk.apdu.variableimpl.different.SNVT_hvac_overid;
import com.synapsense.lontalk.apdu.variableimpl.different.SNVT_hvac_status;
import com.synapsense.lontalk.apdu.variableimpl.different.SNVT_setting;
import com.synapsense.lontalk.apdu.variableimpl.different.SNVT_temp_setpt;
import com.synapsense.lontalk.exception.FieldNotFoundException;
import com.synapsense.lontalk.exception.IncorrectValueException;

public class NetworkVariableDifferentStructure {

	@Test
	public void SNVT_ctrl_req_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_ctrl_req();
		nv.setFieldByName("receiver_id", (double) 65534);
		nv.setFieldByName("sender_id", (double) 65534);
		nv.setFieldByName("sender_prio", (double) 150);
		byte[] a = nv.getData();
		nv.setData(a);
		assertEquals((double) 65534, nv.getFieldByName("receiver_id"), 1);
		assertEquals((double) 65534, nv.getFieldByName("sender_id"), 1);
		assertEquals((double) 150, nv.getFieldByName("sender_prio"), 1);
	}

	@Test(expected = IncorrectValueException.class)
	public void SNVT_ctrl_req_exception_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_ctrl_req();
		nv.setFieldByName("receiver_id", (double) 66000);
		nv.setFieldByName("sender_id", (double) 66000);
		nv.setFieldByName("sender_prio", (double) 150);
		byte[] a = nv.getData();
		nv.setData(a);
		assertEquals((double) 65534, nv.getFieldByName("receiver_id"), 1);
		assertEquals((double) 65534, nv.getFieldByName("sender_id"), 1);
		assertEquals((double) 150, nv.getFieldByName("sender_prio"), 1);
	}

	@Test
	public void SNVT_file_pos_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_file_pos();
		nv.setFieldByName("rw_ptr", (double) 2147483644);
		nv.setFieldByName("rw_length", (double) 65534);
		byte[] a = nv.getData();
		nv.setData(a);
		assertEquals((double) 2147483644, nv.getFieldByName("rw_ptr"), 1);
		assertEquals((double) 65534, nv.getFieldByName("rw_length"), 1);
	}

	@Test(expected = IncorrectValueException.class)
	public void SNVT_file_pos_exception_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_file_pos();
		nv.setFieldByName("rw_ptr", (double) 2147483648l);
		nv.setFieldByName("rw_length", (double) 65534);
		byte[] a = nv.getData();
		nv.setData(a);
		assertEquals((double) 2147483644, nv.getFieldByName("rw_ptr"), 1);
		assertEquals((double) 65534, nv.getFieldByName("rw_length"), 1);
	}

	@Test
	public void SNVT_hvac_overid_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_hvac_overid();
		nv.setFieldByName("state", (double) 1);
		nv.setFieldByName("percent", (double) 160.1);
		nv.setFieldByName("flow", (double) 65534);
		byte[] a = nv.getData();
		nv.setData(a);
		assertEquals((double) 1, nv.getFieldByName("state"), 1);
		assertEquals((double) 160.1, nv.getFieldByName("percent"), 1);
		assertEquals((double) 65534, nv.getFieldByName("flow"), 1);
	}

	@Test(expected = IncorrectValueException.class)
	public void SNVT_hvac_overid_exception_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_hvac_overid();
		nv.setFieldByName("state", (double) 1);
		nv.setFieldByName("percent", (double) 163.835);
		nv.setFieldByName("flow", (double) 65534);
		byte[] a = nv.getData();
		nv.setData(a);
		assertEquals((double) 1, nv.getFieldByName("state"), 1);
		assertEquals((double) 160.1, nv.getFieldByName("percent"), 1);
		assertEquals((double) 65534, nv.getFieldByName("flow"), 1);
	}

	@Test
	public void SNVT_hvac_status_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_hvac_status();
		nv.setFieldByName("mode", (double) 1);
		nv.setFieldByName("heat_output_primary", (double) 160.1);
		nv.setFieldByName("heat_output_secondary", (double) 140.4);
		nv.setFieldByName("cool_output", (double) 128.5);
		nv.setFieldByName("econ_output", (double) 119.1);
		nv.setFieldByName("fan_output", (double) 103.8);
		nv.setFieldByName("in_alarm", (double) 199);
		byte[] a = nv.getData();
		nv.setData(a);
		assertEquals((double) 1, nv.getFieldByName("mode"), 1);
		assertEquals((double) 160.1, nv.getFieldByName("heat_output_primary"), 1);
		assertEquals((double) 140.4, nv.getFieldByName("heat_output_secondary"), 1);
		assertEquals((double) 128.5, nv.getFieldByName("cool_output"), 1);
		assertEquals((double) 119.1, nv.getFieldByName("econ_output"), 1);
		assertEquals((double) 103.8, nv.getFieldByName("fan_output"), 1);
		assertEquals((double) 199, nv.getFieldByName("in_alarm"), 1);
	}

	@Test(expected = IncorrectValueException.class)
	public void SNVT_hvac_status_exception_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_hvac_status();
		nv.setFieldByName("mode", (double) 1);
		nv.setFieldByName("heat_output_primary", (double) 160.1);
		nv.setFieldByName("heat_output_secondary", (double) 140.4);
		nv.setFieldByName("cool_output", (double) 163.835);
		nv.setFieldByName("econ_output", (double) 119.1);
		nv.setFieldByName("fan_output", (double) 103.8);
		nv.setFieldByName("in_alarm", (double) 199);
		byte[] a = nv.getData();
		nv.setData(a);
		assertEquals((double) 1, nv.getFieldByName("mode"), 1);
		assertEquals((double) 160.1, nv.getFieldByName("heat_output_primary"), 1);
		assertEquals((double) 140.4, nv.getFieldByName("heat_output_secondary"), 1);
		assertEquals((double) 128.5, nv.getFieldByName("cool_output"), 1);
		assertEquals((double) 119.1, nv.getFieldByName("econ_output"), 1);
		assertEquals((double) 103.8, nv.getFieldByName("fan_output"), 1);
		assertEquals((double) 199, nv.getFieldByName("in_alarm"), 1);
	}

	@Test
	public void SNVT_setting_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_setting();
		nv.setFieldByName("function", (double) 1);
		nv.setFieldByName("setting", (double) 95.2);
		nv.setFieldByName("rotation", (double) 296.1);
		byte[] a = nv.getData();
		nv.setData(a);
		assertEquals((double) 1, nv.getFieldByName("function"), 1);
		assertEquals((double) 95.2, nv.getFieldByName("setting"), 1);
		assertEquals((double) 296.1, nv.getFieldByName("rotation"), 1);

	}

	@Test(expected = IncorrectValueException.class)
	public void SNVT_setting_exception_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_setting();
		nv.setFieldByName("function", (double) 1);
		nv.setFieldByName("setting", (double) 101);
		nv.setFieldByName("rotation", (double) 296.1);
		byte[] a = nv.getData();
		nv.setData(a);
		assertEquals((double) 1, nv.getFieldByName("function"), 1);
		assertEquals((double) 95.2, nv.getFieldByName("setting"), 1);
		assertEquals((double) 296.1, nv.getFieldByName("rotation"), 1);

	}

	@Test
	public void SNVT_temp_setpt_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_temp_setpt();
		nv.setFieldByName("occupied_cool", (double) 325);
		nv.setFieldByName("standby_cool", (double) 320);
		nv.setFieldByName("unoccupied_cool", (double) 315);
		nv.setFieldByName("occupied_heat", (double) 310);
		nv.setFieldByName("standby_heat", (double) 305);
		nv.setFieldByName("unoccupied_heat", (double) 300);

		byte[] a = nv.getData();

		nv.setData(a);
		assertEquals((double) 325, (double) nv.getFieldByName("occupied_cool"), 0);
		assertEquals((double) 320, (double) nv.getFieldByName("standby_cool"), 0);
		assertEquals((double) 315, (double) nv.getFieldByName("unoccupied_cool"), 0);
		assertEquals((double) 310, (double) nv.getFieldByName("occupied_heat"), 0);
		assertEquals((double) 305, (double) nv.getFieldByName("standby_heat"), 0);
		assertEquals((double) 300, (double) nv.getFieldByName("unoccupied_heat"), 0);
	}

	@Test(expected = IncorrectValueException.class)
	public void SNVT_temp_setpt_exception_test() throws FieldNotFoundException, IncorrectValueException {
		NetworkVariable nv = new SNVT_temp_setpt();
		nv.setFieldByName("occupied_cool", (double) 325);
		nv.setFieldByName("standby_cool", (double) 327.67);
		nv.setFieldByName("unoccupied_cool", (double) 315);
		nv.setFieldByName("occupied_heat", (double) 310);
		nv.setFieldByName("standby_heat", (double) 305);
		nv.setFieldByName("unoccupied_heat", (double) 300);

		byte[] a = nv.getData();

		nv.setData(a);
		assertEquals((double) 325, (double) nv.getFieldByName("occupied_cool"), 0);
		assertEquals((double) 320, (double) nv.getFieldByName("standby_cool"), 0);
		assertEquals((double) 315, (double) nv.getFieldByName("unoccupied_cool"), 0);
		assertEquals((double) 310, (double) nv.getFieldByName("occupied_heat"), 0);
		assertEquals((double) 305, (double) nv.getFieldByName("standby_heat"), 0);
		assertEquals((double) 300, (double) nv.getFieldByName("unoccupied_heat"), 0);
	}

}
