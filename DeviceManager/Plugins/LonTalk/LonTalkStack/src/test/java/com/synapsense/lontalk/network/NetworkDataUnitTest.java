package com.synapsense.lontalk.network;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.synapsense.lontalk.api.Network;
import com.synapsense.lontalk.exception.IncorrectFormatException;
import com.synapsense.lontalk.node.LonTalkNode;

public class NetworkDataUnitTest {

	@Test
	public void test1() throws IncorrectFormatException {
		Network network = new Network("192.168.102.27", 1629, "192.168.102.27", 1628, "76", 0x01, 0x7f, "0A0A0A0A0A0A");
		LonTalkNode node = new LonTalkNode(0x01, 13);
		NetworkDataUnit nwdtunit = new NetworkDataUnit(network, node);
		nwdtunit.setAddressType2();
		byte[] data = nwdtunit.getData();
		assertEquals(0x76, data[6]);
	}

	@Test
	public void test2() throws IncorrectFormatException {
		Network network = new Network("192.168.102.27", 1629, "192.168.102.27", 1628, "767676", 0x01, 0x7f,
		        "0A0A0A0A0A0A");
		LonTalkNode node = new LonTalkNode(0x01, 13);
		NetworkDataUnit nwdtunit = new NetworkDataUnit(network, node);
		nwdtunit.setAddressType2();
		byte[] data = nwdtunit.getData();
		assertEquals(0x76, data[6]);
		assertEquals(0x76, data[7]);
		assertEquals(0x76, data[8]);
	}

	@Test
	public void test3() throws IncorrectFormatException {
		Network network = new Network("192.168.102.27", 1629, "192.168.102.27", 1628, "767676767676", 0x01, 0x7f,
		        "0A0A0A0A0A0A");
		LonTalkNode node = new LonTalkNode(0x01, 13);
		NetworkDataUnit nwdtunit = new NetworkDataUnit(network, node);
		nwdtunit.setAddressType2();
		byte[] data = nwdtunit.getData();
		assertEquals(0x76, data[6]);
		assertEquals(0x76, data[7]);
		assertEquals(0x76, data[8]);
		assertEquals(0x76, data[9]);
		assertEquals(0x76, data[10]);
		assertEquals(0x76, data[11]);
	}

	@Test
	public void test4() throws IncorrectFormatException {
		Network network = new Network("192.168.102.27", 1629, "192.168.102.27", 1628, "76", 0x01, 0x7f, "0A0A0A0A0A0A");
		LonTalkNode node = new LonTalkNode(0x01, 13);
		NetworkDataUnit nwdtunit = new NetworkDataUnit(network, node);
		byte[] data = nwdtunit.getData();
		assertEquals(0x76, data[6]);
	}

	@Test
	public void test5() throws IncorrectFormatException {
		Network network = new Network("192.168.102.27", 1629, "192.168.102.27", 1628, "767676", 0x01, 0x7f,
		        "0A0A0A0A0A0A");
		LonTalkNode node = new LonTalkNode(0x01, 13);
		NetworkDataUnit nwdtunit = new NetworkDataUnit(network, node);
		byte[] data = nwdtunit.getData();
		assertEquals(0x76, data[6]);
		assertEquals(0x76, data[7]);
		assertEquals(0x76, data[8]);
	}

	@Test
	public void test6() throws IncorrectFormatException {
		Network network = new Network("192.168.102.27", 1629, "192.168.102.27", 1628, "767676767676", 0x01, 0x7f,
		        "0A0A0A0A0A0A");
		LonTalkNode node = new LonTalkNode(0x01, 13);
		NetworkDataUnit nwdtunit = new NetworkDataUnit(network, node);
		byte[] data = nwdtunit.getData();
		assertEquals(0x76, data[6]);
		assertEquals(0x76, data[7]);
		assertEquals(0x76, data[8]);
		assertEquals(0x76, data[9]);
		assertEquals(0x76, data[10]);
		assertEquals(0x76, data[11]);
	}

	@Test
	public void test7() throws IncorrectFormatException {
		Network network = new Network("192.168.102.27", 1629, "192.168.102.27", 1628, "76", 0x01, 0x7f, "0A0A0A0A0A0A");
		LonTalkNode node = new LonTalkNode(0x01, 13, 0x121212101010l);
		NetworkDataUnit nwdtunit = new NetworkDataUnit(network, node);
		nwdtunit.setAddressType3();
		byte[] data = nwdtunit.getData();
		assertEquals(0x76, data[11]);

		assertEquals(16, data[10]);
		assertEquals(16, data[9]);
		assertEquals(16, data[8]);
		assertEquals(18, data[7]);
		assertEquals(18, data[6]);
		assertEquals(18, data[5]);
	}

	@Test
	public void test8() throws IncorrectFormatException {
		Network network = new Network("192.168.102.27", 1629, "192.168.102.27", 1628, "767676", 0x01, 0x7f,
		        "0A0A0A0A0A0A");
		LonTalkNode node = new LonTalkNode(0x01, 13, 0x121212101010l);
		NetworkDataUnit nwdtunit = new NetworkDataUnit(network, node);
		nwdtunit.setAddressType3();
		byte[] data = nwdtunit.getData();
		assertEquals(0x76, data[11]);
		assertEquals(0x76, data[12]);
		assertEquals(0x76, data[13]);

		assertEquals(16, data[10]);
		assertEquals(16, data[9]);
		assertEquals(16, data[8]);
		assertEquals(18, data[7]);
		assertEquals(18, data[6]);
		assertEquals(18, data[5]);
	}

	@Test
	public void test9() throws IncorrectFormatException {
		Network network = new Network("192.168.102.27", 1629, "192.168.102.27", 1628, "767676767676", 0x01, 0x7f,
		        "0A0A0A0A0A0A");
		LonTalkNode node = new LonTalkNode(0x01, 13, 0x121212101010l);
		NetworkDataUnit nwdtunit = new NetworkDataUnit(network, node);
		nwdtunit.setAddressType3();
		byte[] data = nwdtunit.getData();
		assertEquals(0x76, data[11]);
		assertEquals(0x76, data[12]);
		assertEquals(0x76, data[13]);
		assertEquals(0x76, data[14]);
		assertEquals(0x76, data[15]);
		assertEquals(0x76, data[16]);

		assertEquals(16, data[10]);
		assertEquals(16, data[9]);
		assertEquals(16, data[8]);
		assertEquals(18, data[7]);
		assertEquals(18, data[6]);
		assertEquals(18, data[5]);
	}
}
