package com.synapsense.plugin.wsplugin;

import java.util.HashMap;

import org.apache.cxf.endpoint.Client;
import org.apache.cxf.endpoint.dynamic.DynamicClientFactory;

public class WebClient {
	private static WebClient instance = new WebClient();
	private DynamicClientFactory dcf = DynamicClientFactory.newInstance();
	private HashMap<String, Client> clients = new HashMap<String, Client>();

	private WebClient() {
	}

	public static synchronized Client getClient(String URI) {
		Client client = instance.clients.get(URI);
		if (client == null) {
			try {
				client = instance.dcf.createClient(URI);
				instance.clients.put(URI, client);
			} catch (Exception e) {
				throw new CreateClientException("Cannot create a client by URL: " + URI, e);
			}
		}

		return client;
	}
}
