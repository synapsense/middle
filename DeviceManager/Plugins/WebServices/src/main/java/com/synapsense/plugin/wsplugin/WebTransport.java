package com.synapsense.plugin.wsplugin;

import java.io.IOException;
import java.io.Serializable;

import org.apache.log4j.Logger;

import com.synapsense.plugin.apinew.Transport;

public class WebTransport implements Transport {
	private static final Logger log = Logger.getLogger(WebTransport.class);
	private static final int priority = 1;

	public WebTransport() {
	}

	@Override
	public void connect() throws IOException {
	}

	@Override
	public int getPriority() {
		return WebTransport.priority;
	}

	@Override
	public void sendData(String objectID, String propertyName, Serializable value) {
		log.debug("ObjectID='" + objectID + "'; propertyName='" + propertyName + "'; value='" + value + "'");

	}

	@Override
	public void sendOrderedData(String objectID, String propertyName, Serializable value) {
		sendData(objectID, propertyName, value);
	}
}
