package com.synapsense.plugin.wsplugin;

public class CreateClientException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3918560917102602120L;

	public CreateClientException(Throwable cause) {
		super(cause);
	}

	public CreateClientException(String message, Throwable cause) {
		super(message, cause);
	}

}
