package com.synapsense.plugin.wsplugin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.cxf.endpoint.Client;
import org.apache.cxf.endpoint.Endpoint;
import org.apache.cxf.service.model.MessagePartInfo;
import org.apache.cxf.service.model.OperationInfo;
import org.apache.cxf.service.model.ServiceInfo;
import org.apache.ws.commons.schema.XmlSchemaComplexType;
import org.apache.ws.commons.schema.XmlSchemaElement;
import org.apache.ws.commons.schema.XmlSchemaSequence;
import org.apache.ws.commons.schema.XmlSchemaSimpleType;

import com.synapsense.plugin.apinew.ObjectDesc;

public class WsdlObject {

	private String url = null;
	private HashMap<String, String[]> methodInfo = new HashMap<String, String[]>();
	private boolean isInit = false;

	public WsdlObject(String url) {
		this.url = url;
	}

	@SuppressWarnings("unchecked")
	private boolean init() {
		Client client = WebClient.getClient(url);

		Endpoint endpoint = client.getEndpoint();
		ServiceInfo serviceInfo = endpoint.getService().getServiceInfos().get(0);

		for (OperationInfo opInfo : serviceInfo.getInterface().getOperations()) {

			String methodName = opInfo.getName().getLocalPart();
			methodInfo.put(methodName, new String[] { "" });
			List<String> argList = new ArrayList<String>();
			for (MessagePartInfo msgPart : opInfo.getInput().getMessageParts()) {
				if (msgPart.getXmlSchema() instanceof XmlSchemaSimpleType) {
					argList.add(msgPart.getName().getLocalPart());
				} else {
					XmlSchemaElement xmlMethod = (XmlSchemaElement) msgPart.getXmlSchema();

					XmlSchemaComplexType type = (XmlSchemaComplexType) xmlMethod.getSchemaType();
					XmlSchemaSequence sequence = (XmlSchemaSequence) type.getParticle();
					Iterator<XmlSchemaElement> argsIterator = sequence.getItems().getIterator();

					while (argsIterator.hasNext()) {
						XmlSchemaElement xmlArgument = (XmlSchemaElement) argsIterator.next();
						argList.add(xmlArgument.getName());
					}
				}
			}
			String[] argStr = new String[argList.size()];
			argList.toArray(argStr);
			methodInfo.put(methodName, argStr);
		}
		isInit = true;
		return true;
	}

	public boolean isMethodExists(ObjectDesc method) {

		if (isInit == false)
			if (init() == false)
				return false;

		boolean result = false;

		if (method != null) {
			String methodName = method.getPropertyValue(WsConstants.WS_METHOD_NAME);
			String[] xmlNames = methodInfo.get(methodName);

			if (xmlNames != null) {
				String[] names = new String[0];
				String paramNames = method.getPropertyValue(WsConstants.WS_PARAM_NAMES);
				if (paramNames.isEmpty() == false) {
					names = paramNames.split(",");
				}
				result = isEquals(xmlNames, names);
			}
		}
		return result;
	}

	public String[] getSortedValues(ObjectDesc method) {

		if (isInit == false)
			init();

		String[] result = new String[0];
		String methodName = method.getPropertyValue(WsConstants.WS_METHOD_NAME);
		String[] xmlNames = methodInfo.get(methodName);

		if (xmlNames != null && xmlNames.length > 0) {
			String paramNames = method.getPropertyValue(WsConstants.WS_PARAM_NAMES);
			String paramValues = method.getPropertyValue(WsConstants.WS_PARAM_VALUES);
			String[] names = paramNames.split(",");
			String[] values = paramValues.split(",");

			String[] sortedValues = new String[xmlNames.length];

			for (int i = 0; i < xmlNames.length; i++) {
				int index = find(names, xmlNames[i]);
				sortedValues[i] = values[index];
			}

			result = sortedValues;
		}

		return result;
	}

	private int find(String[] array, String value) {
		int index = -1;
		for (int i = 0; i < array.length; i++) {
			if (array[i].equalsIgnoreCase(value)) {
				index = i;
				break;
			}
		}
		return index;
	}

	private boolean isEquals(String[] array1, String[] array2) {
		boolean result = false;

		if (array1.length == array2.length) {
			result = true;
			for (int i = 0; i < array1.length; i++) {
				if (!contains(array2, array1[i])) {
					result = false;
					break;
				}
			}
		}
		return result;
	}

	private boolean contains(String[] array, String value) {
		boolean result = false;
		for (int i = 0; i < array.length; i++) {
			if (array[i].equalsIgnoreCase(value)) {
				result = true;
			}
		}
		return result;
	}
}
