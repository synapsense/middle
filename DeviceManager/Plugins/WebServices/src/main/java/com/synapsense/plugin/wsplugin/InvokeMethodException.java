package com.synapsense.plugin.wsplugin;

public class InvokeMethodException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4188941399823582582L;

	public InvokeMethodException(Throwable cause) {
		super(cause);
	}

	public InvokeMethodException(String message, Throwable cause) {
		super(message, cause);
	}
}
