package com.synapsense.plugin.wsplugin;

import java.io.Serializable;
import java.lang.reflect.Method;

import javax.xml.bind.JAXBElement;

import org.apache.cxf.endpoint.Client;
import org.apache.log4j.Logger;

import com.synapsense.plugin.apinew.ObjectDesc;
import com.synapsense.plugin.apinew.PropertyDesc;

public class WebPluginRW {
	private static final Logger log = Logger.getLogger(WebPluginRW.class);;

	@SuppressWarnings("rawtypes")
	public static String readProperty(final PropertyDesc property, final WsdlObject service) {
		String result = null;

		if (property == null) {
			throw new IllegalArgumentException("Unable to read property. The property object is null.");
		}

		if (WsConstants.WS_LAST_VALUE.equals(property.getName())) {
			ObjectDesc parentMethodObj = property.getParent();
			if (parentMethodObj == null) {
				throw new IllegalArgumentException(
				        "Property has invalid structure, unaple to find parent SOAP method object.");
			}
			ObjectDesc parentServiceObj = parentMethodObj.getParent();
			if (parentServiceObj == null) {
				throw new IllegalArgumentException(
				        "Property has invalid structure, unaple to find parent SOAP service object.");
			}

			String methodName = parentMethodObj.getPropertyValue(WsConstants.WS_METHOD_NAME);

			String URI = parentServiceObj.getPropertyValue(WsConstants.WS_URL);
			Client client = WebClient.getClient(URI);

			Object[] args = null;
			if (service.isMethodExists(parentMethodObj)) {
				args = service.getSortedValues(parentMethodObj);
			} else {
				args = parentMethodObj.getPropertyValue(WsConstants.WS_PARAM_VALUES).split(",");
				String paramNames = parentMethodObj.getPropertyValue(WsConstants.WS_PARAM_NAMES);
				log.debug("Cannot find method declaration in WSDL. Method="
				        + parentMethodObj.getPropertyValue(WsConstants.WS_METHOD_NAME) + "; paramNames='" + paramNames
				        + "'");
			}

			Object[] results = null;
			try {
				results = client.invoke(methodName, args);
			} catch (Exception e) {
				StringBuilder strArgs = new StringBuilder();
				for (int i = 0; i < args.length; i++) {
					if (i != 0)
						strArgs.append("; ");
					strArgs.append(args[i].toString());
				}
				String err = "Cannot invoke '" + methodName + "' method with args={" + strArgs.toString() + "}";
				throw new InvokeMethodException(err, e);
			}
			if (results != null) {
				try {
					Class<?> resultClass = results[0].getClass();
					if (resultClass.getName().matches("^.*Get.*Response$")) {
						Method m = resultClass.getDeclaredMethod("getReturn", new Class<?>[] {});
						if (m != null) {
							Object res = m.invoke(results[0], new Object[] {});
							if (res instanceof JAXBElement) {
								result = ((JAXBElement) res).getValue().toString();
							} else {
								result = res.toString();
							}
						}
					}
					if (result == null) {
						result = results[0].toString();
					}
				} catch (Exception e) {
					throw new InvokeMethodException("Cannot deserialize a result [" + results + "].", e);
				}
			}
		}
		return result;
	}

	public static void writeProperty(final PropertyDesc property, final Serializable value) {

		if (WsConstants.WS_LAST_VALUE.equalsIgnoreCase(property.getName())) {
			log.warn("'lastValue' property is accessible only for reading.");
		} else {
			property.setValue((String) value);
		}

	}
}
