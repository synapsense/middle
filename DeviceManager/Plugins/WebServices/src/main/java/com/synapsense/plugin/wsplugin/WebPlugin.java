package com.synapsense.plugin.wsplugin;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.synapsense.plugin.api.exceptions.ConfigPluginException;
import com.synapsense.plugin.api.exceptions.IncorrectStateException;
import com.synapsense.plugin.apinew.ObjectDesc;
import com.synapsense.plugin.apinew.Plugin;
import com.synapsense.plugin.apinew.PluginConfig;
import com.synapsense.plugin.apinew.Transport;

public class WebPlugin implements Plugin {
	private static final String ID = "WS";
	private static Properties PLUGIN_DESC = new Properties();
	static {
		PLUGIN_DESC.put(PLUGIN_ID, ID);
		PLUGIN_DESC.put(ROOT_TYPES, "SOAPSERVICE");
	}
	private static final Logger logger = Logger.getLogger(WebPlugin.class);

	private boolean configured = false;
	private boolean started = false;

	private Transport transport = null;

	private HashMap<String, WsdlObject> services = new HashMap<String, WsdlObject>();

	@Override
	public void configure(PluginConfig config) throws ConfigPluginException {
		if (config == null)
			throw new IllegalArgumentException("config cannot be null");
		stop();
		for (ObjectDesc wsService : config.getRoots()) {
			WsdlObject wsdlObject = new WsdlObject(wsService.getPropertyValue(WsConstants.WS_URL));
			services.put(wsService.getID(), wsdlObject);
		}

		configured = true;
		logger.debug("Plugin " + ID + " is configured.");
	}

	@Override
	public String getID() {
		return WebPlugin.ID;
	}

	@Override
	public Properties getPluginDescription() {
		return PLUGIN_DESC;
	}

	@Override
	public void setTransport(Transport transport) {
		this.transport = transport;
	}

	@Override
	public void stop() {
		services.clear();
		configured = false;
		started = false;
		logger.debug("Plugin " + ID + " is stopped.");
	}

	@Override
	public void start() throws IncorrectStateException {
		if (!configured) {
			throw new IncorrectStateException("Plugin " + ID + " has to be configured before start.");
		}
		if (configured && !started) {
			started = true;
			logger.debug("Plugin " + ID + " is started.");
		}
	}

	public Transport getTransport() {
		return transport;
	}

	@Override
	public void setPropertyValue(ObjectDesc object, String propertyName, Serializable value) {
		if (propertyName != null && !propertyName.isEmpty() && object != null
		        && WsConstants.TYPE_METHOD.equals(object.getType())) {
			WsdlObject service = services.get(object.getParent().getID());
			if (service == null) {
				logger.warn("Unable to set property: " + propertyName + " for Object" + object.getID()
				        + ". Proper Web Service can not be found.");
			}
			WebPluginRW.writeProperty(object.getProperty(propertyName), value);
		} else {
			logger.warn("Unknown property name='" + propertyName + "'");
		}

	}

	@Override
	public void setPropertyValue(ObjectDesc object, List<String> propertyNames, List<Serializable> values) {
		if (propertyNames == null || values == null || propertyNames.size() != values.size()) {
			throw new IllegalArgumentException("Unable to set properties values.");
		}
		for (int i = 0; i < propertyNames.size(); i++) {
			setPropertyValue(object, propertyNames.get(i), values.get(i));
		}
	}

	@Override
	public Serializable getPropertyValue(ObjectDesc object, String propertyName) {
		String result = null;
		if (WsConstants.WS_LAST_VALUE.equalsIgnoreCase(propertyName) && object != null
		        && WsConstants.TYPE_METHOD.equals(object.getType())) {
			WsdlObject service = services.get(object.getParent().getID());
			if (service == null) {
				logger.warn("Unable to get property: " + propertyName + " for Object" + object.getID()
				        + ". Proper Web Service can not be found.");
			}
			result = WebPluginRW.readProperty(object.getProperty(propertyName), service);
		} else {
			logger.warn("Unknown property name='" + propertyName + "'");
		}
		return result;
	}

	@Override
	public List<Serializable> getPropertyValue(ObjectDesc object, List<String> propertyNames) {
		List<Serializable> result = new ArrayList<Serializable>();
		for (String property : propertyNames) {
			result.add(getPropertyValue(object, property));
		}
		return result;
	}

}
