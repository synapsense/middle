package com.synapsense.plugin.wsplugin;

public class WsConstants {
	public static final String WS_URL = "url";
	public static final String WS_METHOD_NAME = "methodName";
	public static final String WS_PARAM_NAMES = "parameterNames";
	public static final String WS_PARAM_VALUES = "parameterValues";
	public static final String WS_LAST_VALUE = "lastValue";

	public static final String TYPE_SERVICE = "SOAPSERVICE";
	public static final String TYPE_METHOD = "SOAPMETHOD";
}
