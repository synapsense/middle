package com.synapsense.plugin.wsplugin;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ SimpleTest.class })
public class WSPluinTestSuite {
}
