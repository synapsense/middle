package com.synapsense.plugin.simulator.test;

import org.junit.Ignore;

import com.synapsense.plugin.simulator.SimulatorPlugin;

//no actual tests here; remove annotation if you add one
@Ignore
public class Standalone {

	/**
	 * @param args
	 * @throws ClassNotFoundException
	 * @throws InterruptedException
	 * @throws SecurityException
	 * @throws NoSuchFieldException
	 */
	public static void main(String[] args) throws ClassNotFoundException, InterruptedException {
		SimulatorPlugin factory = new SimulatorPlugin();
		factory.setTransport(new TestTransport());
		while (true) {
			Thread.sleep(10000l);
		}

		// String val = "10gf";
		//
		// int intValue = Integer.valueOf(val);

	}
}
