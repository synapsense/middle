package com.synapsense.plugin.simulator.test;

import java.io.IOException;
import java.io.Serializable;

import com.synapsense.plugin.apinew.Transport;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicLong;

import org.junit.Ignore;

//no actual tests here; remove annotation if you add one
@Ignore
public class TestTransport implements Transport {
	static Timer timer = new Timer("MONITOR");
	static AtomicLong counter = new AtomicLong(0);
	static int i = 1;
	static {
		timer.schedule(new TimerTask() {

			@Override
			public void run() {
				System.out.println("Time running: " + 30000 * i++);
				System.out.println("Transport messages sent: " + counter);

			}
		}, 30000, 30000);
	}

	@Override
	public int getPriority() {
		return 0;
	}

	@Override
	public void sendData(String objectID, String propertyName, Serializable value) {
		counter.incrementAndGet();
	}

	@Override
	public void sendOrderedData(String objectID, String propertyName, Serializable value) {

	}

	@Override
	public void connect() throws IOException {

	}

}