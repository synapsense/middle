package com.synapsense.plugin.simulator.test;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 * Created with IntelliJ IDEA. User: stikhon Date: 11/5/13 Time: 5:28 PM To
 * change this template use File | Settings | File Templates.
 */
public class SimulatorConfigNormalizer {
	static int sensorsNum = 4681;

	public static void main(String[] args) throws IOException, XMLStreamException, ParserConfigurationException,
	        SAXException {
		final int step = 300000 / sensorsNum;
		Path p = Paths.get("conf\\simulator_Equinix-SV3-032613-6.4.1.IO.xml");
		OutputStream out = new FileOutputStream("conf\\simulator_Equinix-SV3-032613-6.4.1.IO.xml_normalized.xml");
		XMLOutputFactory factory = XMLOutputFactory.newInstance();
		final XMLStreamWriter writer = factory.createXMLStreamWriter(out, "UTF-8");
        writer.setPrefix("", "");
		SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
		SAXParser parser = saxParserFactory.newSAXParser();
		parser.parse(p.toFile(), new DefaultHandler() {
            int currentDelay = 22;

            @Override
            public void startDocument() throws SAXException {
                try {
                    writer.writeStartDocument("UTF-8", "1.0");
                    writer.writeCharacters("\n");
                } catch (XMLStreamException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void startElement(String uri, String localName, String qName, Attributes attributes)
                    throws SAXException {

                try {
                    writer.writeEmptyElement(uri, qName);
                    for (int i = 0; i < attributes.getLength(); i++) {
                        String attrName = attributes.getQName(i);
                        String attrValue = attributes.getValue(i);
                        if ("firstDelay".equals(attrName)) {
                            writer.writeAttribute(attrName, new Integer(currentDelay += step).toString());
                        } else {
                            writer.writeAttribute(attrName, attrValue);
                        }
                    }
                } catch (XMLStreamException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void endElement(String uri, String localName, String qName) throws SAXException {
                try {
                    //writer.writeEndElement();
                    writer.writeCharacters("\n");
                } catch (XMLStreamException e) {
                    e.printStackTrace();
                }
            }

        });
	}
}
