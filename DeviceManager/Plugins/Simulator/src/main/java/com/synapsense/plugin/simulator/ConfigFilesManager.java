package com.synapsense.plugin.simulator;

import java.io.File;
import java.io.FilenameFilter;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Observable;
import java.util.Set;

import org.apache.log4j.Logger;

import com.synapsense.plugin.simulator.util.FileListner;
import com.synapsense.plugin.simulator.util.FileMonitor;
import com.synapsense.util.algo.SetsIntersector;
import com.synapsense.util.algo.SetsIntersector.Difference;

/**
 * This class monitors both config file folder (checks if any new files are
 * added or deleted) and all config files themselves(checks if config files are
 * changed).
 * 
 * @author stikhon
 * 
 */
public class ConfigFilesManager extends Observable {
	private final static Logger logger = Logger.getLogger(ConfigFilesManager.class);
	private Set<File> configFiles = Collections.synchronizedSet(new HashSet<File>());
	private FileMonitor configFilesMonitor;
	private FileMonitor configFolderMonitor;

	private final static FilenameFilter CONF_FILES_NAME_FILTER = new FilenameFilter() {

		@Override
		public boolean accept(File dir, String name) {
			if (name.startsWith(SimulatorPlugin.ID) && name.endsWith(".xml")) {
				return true;
			}
			return false;
		}
	};
	private final static Comparator<File> FILE_NAME_COMPARATOR = new Comparator<File>() {
		@Override
		public int compare(File f1, File f2) {
			try {

				if (f1.isDirectory() && !f2.isDirectory()) {
					return -1;
				} else if (!f1.isDirectory() && f2.isDirectory()) {
					return 1;
				} else {
					return f1.getName().compareToIgnoreCase(f2.getName());
				}
			} catch (ClassCastException ex) {
			}

			return 0;
		}
	};

	public ConfigFilesManager() {
		configFolderMonitor = new FileMonitor(SimulatorConstants.CONF_CHECK_INTERVAL);
		File confFilesDir = new File(SimulatorConstants.CONFIG_FILES_DIR);
		configFolderMonitor.addFile(confFilesDir);
		configFolderMonitor.addListner(new FileListner() {
			// Config files dir has been changed. We need to check if there are
			// new
			// config files added or existing configs deleted.
			@Override
			public void fileChanged(File fileToCheck) {
				refreshFolder();
				Collection<File> monitoredFiles = configFilesMonitor.getMonitoredFiles();
				SetsIntersector<File> intersector = new SetsIntersector<File>();
				// get all new files and add them to the configFilesMonitor
				Difference<File> dif = intersector.computeDifference(configFiles, monitoredFiles, FILE_NAME_COMPARATOR);
				for (File file : dif.getFMinusS()) {
					configFilesMonitor.addFile(file);
				}
				// get all removed files and remove them from the
				// configFilesMonitor
				for (File file : dif.getSMinusF()) {
					configFilesMonitor.removeFile(file);
				}
				setChanged();
				notifyObservers();

			}
		});
		refreshFolder();

		configFilesMonitor = new FileMonitor(SimulatorConstants.CONF_CHECK_INTERVAL);
		for (File f : configFiles) {
			configFilesMonitor.addFile(f);
		}
		configFilesMonitor.addListner(new FileListner() {

			@Override
			public void fileChanged(File fileToCheck) {
				setChanged();
				notifyObservers();

			}
		});

	}

	private void refreshFolder() {
		configFiles.clear();
		File confFilesDir = new File(SimulatorConstants.CONFIG_FILES_DIR);
		if (confFilesDir.exists()) {
			File[] filteredFiles = confFilesDir.listFiles(CONF_FILES_NAME_FILTER);
			if (filteredFiles != null && filteredFiles.length > 0) {
				for (File file : filteredFiles) {
					configFiles.add(file);
				}

			} else {
				logger.info("Simulator plugin didn't find configuration files using path: "
				        + SimulatorConstants.CONFIG_FILES_DIR);
			}
		}

	}

	public Set<File> getConfigFiles() {
		return configFiles;
	}
}
