package com.synapsense.plugin.simulator;

public class SimulatorConstants {
	public static final String SENSOR_EXPRESSION = "expression";
	public static final String SENSOR_FIRST_DELAY = "firstDelay";
	public static final String SENSOR_SLEEP_DELAY = "sleepDelay";
	public static final String SENSOR_LAST_VALUE = "lastValue";

	public static final String CONFIG_FILES_DIR = "conf";
	public static final long CONF_CHECK_INTERVAL = 5000;
}
