package com.synapsense.plugin.simulator.util;

import java.io.File;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Monitor, which checks files for changes. If a file is changed, notifies all
 * listeners.
 * 
 * @author stikhon
 * 
 */
public class FileMonitor {
	private Timer timer;
	private List<FileListner> listners;
	private Map<File, Long> files;

	public FileMonitor(long checkInterval) {
		listners = new Vector<FileListner>();
		files = new ConcurrentHashMap<File, Long>();
		timer = new Timer(true);
		timer.schedule(new FileMonitorTask(), 0, checkInterval);
	}

	public boolean addListner(FileListner listner) {
		return listners.add(listner);
	}

	public boolean removeListner(FileListner listner) {
		return listners.remove(listner);
	}

	public void addFile(File file) {
		files.put(file, file.exists() ? file.lastModified() : -1);
	}

	public void removeFile(File file) {
		files.remove(file);
	}

	public Collection<File> getMonitoredFiles() {
		return files.keySet();
	}

	private class FileMonitorTask extends TimerTask {

		@Override
		public void run() {
			for (File fileToCheck : files.keySet()) {
				Long lastModifiedTime = files.get(fileToCheck);
				Long newModifiedTime = fileToCheck.exists() ? fileToCheck.lastModified() : -1;
				if (!lastModifiedTime.equals(newModifiedTime)) {
					files.put(fileToCheck, newModifiedTime);
					for (FileListner listner : listners) {
						listner.fileChanged(fileToCheck);
					}
				}
			}

		}

	}
}
