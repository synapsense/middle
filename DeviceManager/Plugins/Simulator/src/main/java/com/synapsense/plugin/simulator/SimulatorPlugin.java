package com.synapsense.plugin.simulator;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.Properties;

import javax.xml.bind.JAXBException;

import org.apache.log4j.Logger;

import com.synapsense.plugin.api.exceptions.ConfigPluginException;
import com.synapsense.plugin.apinew.ObjectDesc;
import com.synapsense.plugin.apinew.Plugin;
import com.synapsense.plugin.apinew.PluginConfig;
import com.synapsense.plugin.apinew.Transport;
import com.synapsense.plugin.simulator.generated.Sensor;
import com.synapsense.plugin.simulator.generated.Simulator;
import com.synapsense.plugin.simulator.sensorplace.SensorImitator;
import com.synapsense.plugin.simulator.sensorplace.SensorsStore;
import com.synapsense.util.XmlConfigUtils;

public class SimulatorPlugin implements Plugin, Observer {
	public final static String ID = "simulator";
	private static Properties PLUGIN_DESC = new Properties();
	static {
		PLUGIN_DESC.put("PLUGIN_ID", ID);
		PLUGIN_DESC.put("ROOT_TYPES", "");
	}
	private final static Logger logger = Logger.getLogger(SimulatorPlugin.class);

	private final static XmlConfigUtils confBuilder;
	static {
		try {
			confBuilder = new XmlConfigUtils("conf", Simulator.class);
		} catch (JAXBException e) {
			throw new IllegalStateException("Cannot instantiate configuration builder", e);
		}
	}
	private ConfigFilesManager configFilesManager;

	@Override
	public void configure(PluginConfig config) throws ConfigPluginException {
		// Noop, the simulator is not configured through ES
	}

	@Override
	public String getID() {
		return ID;
	}

	@Override
	public Properties getPluginDescription() {
		return PLUGIN_DESC;
	}

	public Serializable getPropertyValue(String objectID, String propertyName) {
		throw new UnsupportedOperationException("not applicable");
	}

	public void setPropertyValue(String objectID, String propertyName, Serializable value) {
		throw new UnsupportedOperationException("not applicable");
	}

	@Override
	public void start() {
		// Noop, the simulator is not configured through ES
	}

	@Override
	public void stop() {
		// Noop, the simulator is not configured through ES
		// stop() will be called each time ES sends a configuration
	}

	@Override
	public void setTransport(Transport transport) {
		// Configure and start only once
		if (SensorsStore.getInst().isConfigured()) {
			logger.warn("Duplicate attempt to start the simulator ignored.");
			return;
		}

		configFilesManager = new ConfigFilesManager();
		configFilesManager.addObserver(this);
		for (File file : configFilesManager.getConfigFiles()) {
            loadConfFile(file);
        }
		SensorsStore.getInst().setTransport(transport);
		SensorsStore.getInst().setConfigured(true);
		SensorsStore.getInst().setEnabled(true);
		logger.info("Plugin " + ID + " started.");
	}

	@Override
	public void setPropertyValue(ObjectDesc object, String propertyName, Serializable value) {
	}

	@Override
	public void setPropertyValue(ObjectDesc object, List<String> propertyName, List<Serializable> values) {

	}

	@Override
	public Serializable getPropertyValue(ObjectDesc object, String propertyName) {
		return null;
		// return SensorsStore.getInst().getValue(object.getID() + ":" +
		// propertyName);
	}

	@Override
	public List<Serializable> getPropertyValue(ObjectDesc object, List<String> propertyNames) {
		return null;
		// List<Serializable> list = new
		// ArrayList<Serializable>(propertyNames.size());
		// for (String propertyName : propertyNames)
		// list.add(SensorsStore.getInst().getValue(object.getID()) + ":" +
		// propertyName);
		// return list;
	}

	private void loadConfFile(File file) {
		try {
			SensorsStore.getInst().loadLibraries();
			logger.trace("Reading Simulator configuration file: " + file.getName());
			Simulator config = confBuilder.read(file, Simulator.class);
			logger.trace("Simulator configuration file is read: " + file.getName());
			for (Sensor sensor : config.getSensor()) {
				long firstDelay = sensor.getFirstDelay();
				long sleepDelay;
				try {
					sleepDelay = Long.parseLong(sensor.getSleepDelay());
				} catch (NumberFormatException e) {
					sleepDelay = 300000;
				}
				SensorsStore.getInst().addSensor(
				        new SensorImitator(sensor.getId(), firstDelay, sleepDelay, sensor.getExpression(), sensor
				                .getProperty(), sensor.getType()));
			}
			logger.info("Loaded Simulator configuration file: " + "[" + SimulatorConstants.CONFIG_FILES_DIR + "/"
			        + file.getName() + "]");
		} catch (IOException e) {
			logger.error("Unable to configure Simulator using configuration file: ["
			        + SimulatorConstants.CONFIG_FILES_DIR + "/" + file.getName() + "]. File is corrupted or not found.", e);
		} catch (Exception e) {
			logger.error("Unable to configure Simulator using configuration file: " + "["
			        + SimulatorConstants.CONFIG_FILES_DIR + "/" + file.getName() + "]. The file is malformed.", e);
		}

	}

	@Override
	synchronized public void update(Observable o, Object arg) {
		logger.info("Reloading all configuration files from the [" + SimulatorConstants.CONFIG_FILES_DIR + "]");
		SensorsStore.getInst().stop();
		for (File file : configFilesManager.getConfigFiles()) {
			loadConfFile(file);
		}
		SensorsStore.getInst().setConfigured(true);
		SensorsStore.getInst().setEnabled(true);

	}

}
