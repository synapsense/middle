package com.synapsense.plugin.simulator.sensorplace;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.script.Bindings;
import javax.script.Compilable;
import javax.script.CompiledScript;
import javax.script.ScriptEngine;
import javax.script.ScriptException;

import org.apache.log4j.Logger;

import com.synapsense.plugin.simulator.SimulatorConstants;

/**
 * 
 * Entity class which stores known node and all its sensors
 * 
 * @author anechaev
 * @author opiruyan
 * 
 */
public class SensorImitator implements Runnable {
	private final static Logger logger = Logger.getLogger(SensorImitator.class);

	private static String depRegexp = "\\w{1,}\\_\\d{1,}\\_\\w{1,}";
	private String exprText;
	private String objectID;
	private String envObjectId;
	private String property;
	private String uniqueId;
	private long firstDelay;
	private long sleepDelay;
	private int wsnSensorType; // WSNSENSOR.type or 0 if not a WSNSENSOR.

	private CompiledScript exprScript;
	private Bindings bindings;
	private HashSet<String> deps = new HashSet<String>();

	private SensorsStore store;

	private Map<String, Object> state = new HashMap<String, Object>();

	public SensorImitator(String objectID, long firstDelay, long sleepDelay, String exprText, String property, int wsnSensorType) {
		this.envObjectId = objectID;
		this.objectID = objectID.replace(":", "_");
		this.firstDelay = firstDelay;
		this.sleepDelay = sleepDelay;
		this.exprText = exprText;
		this.property = property == null || property.isEmpty() ? SimulatorConstants.SENSOR_LAST_VALUE : property;
		this.uniqueId = this.objectID + "_" + this.property;
		this.wsnSensorType = wsnSensorType;
		compileScript();
	}

	/**
	 * Starts formula computation periodically.
	 * 
	 * @param pool
	 *            - TimerTask pool this task is to connect to.
	 * @param transport
	 *            - provider used to send message based on formula computation
	 */
	public void startImitation(SensorsStore store) {
		this.store = store;
		try {
			store.getScheduledThreadPool().scheduleWithFixedDelay(this, firstDelay, sleepDelay, TimeUnit.MILLISECONDS);
		} catch (Exception e) {
			logger.error("Unable to start imitation for sensor " + objectID + ":" + property, e);
		}
	}

	private void compileScript() {
		ScriptEngine engine = SensorsStore.getInst().getEngine();
		this.bindings = engine.createBindings();

		Compilable comp = (Compilable) engine;
		String resExpr = SensorsStore.getInst().getLibraries()
		        + (exprText.contains("function simulate") ? exprText + ";simulate();" : "function simulate() {"
		                + " return " + exprText + ";};simulate();");
		logger.trace("compiling script:  " + resExpr);
		try {
			exprScript = comp.compile(resExpr);
		} catch (ScriptException e) {
			logger.warn("Failed to compile expression: " + resExpr, e);
		}

		// add deps
		Pattern p = Pattern.compile(depRegexp);
		Matcher m = p.matcher(resExpr);
		while (m.find()) {
			String dep = m.group();
			deps.add(dep.trim());
			logger.trace(uniqueId + ": adding dependency " + dep);
		}
	}

	public void run() {

		try {
			logger.trace("Running expresssion for " + uniqueId);
			for (String dep : deps) {
				Object val = store.getValue(dep);
				logger.trace("binding " + dep + " = " + val);
				bindings.put(dep, val);
			}

			Object current = store.getValue(uniqueId);
			logger.trace("binding current = " + current);
			bindings.put("current", current);

			long timestamp = System.currentTimeMillis();
			bindings.put("time", timestamp);
			bindings.put("state", state);

			Object result;
			if (exprScript == null) {
				logger.debug("Skipping imitation for [" + uniqueId + "] due to expression failed compilation.");
			} else {
				result = exprScript.eval(bindings);
				logger.trace("Expression " + uniqueId + " = " + result);

				store.saveValue(uniqueId, result);
				store.sendValue(envObjectId, property, result, timestamp, wsnSensorType);
			}
		} catch (Exception e) {
			logger.warn("Failed to generate value for: " + uniqueId, e);
		}
	}

	public String getObjectID() {
		return objectID;
	}

	public String getProperty() {
		return property;
	}

	public String getUniqueId() {
		return uniqueId;
	}

	public String getexprText() {
		return exprText;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((envObjectId == null) ? 0 : envObjectId.hashCode());
		result = prime * result + ((exprText == null) ? 0 : exprText.hashCode());
		result = prime * result + (int) (firstDelay ^ (firstDelay >>> 32));
		result = prime * result + ((objectID == null) ? 0 : objectID.hashCode());
		result = prime * result + ((property == null) ? 0 : property.hashCode());
		result = prime * result + (int) (sleepDelay ^ (sleepDelay >>> 32));
		result = prime * result + ((uniqueId == null) ? 0 : uniqueId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SensorImitator other = (SensorImitator) obj;
		if (envObjectId == null) {
			if (other.envObjectId != null)
				return false;
		} else if (!envObjectId.equals(other.envObjectId))
			return false;
		if (exprText == null) {
			if (other.exprText != null)
				return false;
		} else if (!exprText.equals(other.exprText))
			return false;
		if (firstDelay != other.firstDelay)
			return false;
		if (objectID == null) {
			if (other.objectID != null)
				return false;
		} else if (!objectID.equals(other.objectID))
			return false;
		if (property == null) {
			if (other.property != null)
				return false;
		} else if (!property.equals(other.property))
			return false;
		if (sleepDelay != other.sleepDelay)
			return false;
		if (uniqueId == null) {
			if (other.uniqueId != null)
				return false;
		} else if (!uniqueId.equals(other.uniqueId))
			return false;
		return true;
	}

}
