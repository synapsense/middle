package com.synapsense.plugin.simulator.sensorplace;

import com.synapsense.plugin.messages.WSNSensorDataBuilder;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;

import org.apache.log4j.Logger;

import com.synapsense.plugin.apinew.Transport;

public class SensorsStore {

	private Transport transport;

	private final ConcurrentHashMap<String, Object> values = new ConcurrentHashMap<String, Object>();

	private final static Logger logger = Logger.getLogger(SensorsStore.class);

	private static SensorsStore inst = new SensorsStore();

	private Map<String, SensorImitator> sensors = new HashMap<String, SensorImitator>();

	private ScheduledExecutorService scheduledThreadPool;

	private boolean configured = false;
	private boolean enabled = false;

	private ScriptEngine engine;

	private String libraries = "";

	public static SensorsStore getInst() {
		return inst;
	}

	public SensorsStore() {
		ScriptEngineManager mgr = new ScriptEngineManager();
		engine = mgr.getEngineByName("JavaScript");
	}

	public Map<String, SensorImitator> getMap() {
		return sensors;
	}

	public String getLibraries() {
		return libraries;
	}

	public ScriptEngine getEngine() {
		return engine;
	}

	public void addSensor(SensorImitator sensor) {
		String id = sensor.getUniqueId();
		if (sensors.containsKey(id)) {
			SensorImitator oldSensor = sensors.get(id);
			if (!oldSensor.equals(sensor)) {
				if (enabled) {
					sensor.startImitation(this);
				}
			}
		}
		sensors.put(id, sensor);
	}

	public void tryStart() {
		if (!(configured && enabled))
			return;
		logger.info("Starting...");
		scheduledThreadPool = Executors.newScheduledThreadPool(4);
		for (SensorImitator sensor : sensors.values()) {
			sensor.startImitation(this);
		}
	}

	public void loadLibraries() {
		try {
			libraries = scriptInline();
		} catch (IOException e) {
			throw new RuntimeException("Failed to load simulator libraries.", e);
		}
	}

	public void setTransport(Transport transport) {
		this.transport = transport;
	}

	public boolean isConfigured() {
		return configured;
	}

	public void setConfigured(boolean configured) {
		this.configured = configured;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public ScheduledExecutorService getScheduledThreadPool() {
		return scheduledThreadPool;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
		tryStart();
	}

	public SensorsStore sendValue(String objId, String property, Object value, long timestamp, int wsnSensorType) {
		if (wsnSensorType != 0) {
			// For WSNSENSOR use a GenericMessage, just like the WSN plugin.
			// This allows passing the timestamp along with the value.
			transport.sendData(objId, property, message(value, timestamp, wsnSensorType));
		} else {
			// For all other sensors, the timestamp is assigned by ES when saving the value.
			transport.sendData(objId, property, (Serializable) value);
		}
		return this;
	}

	/** wraps the simulated value in a GenericMessage, just like the WSN plugin. */
	private Serializable message(Object value, long timestamp, int type) {
		return new WSNSensorDataBuilder(0, 0, ((Number) value).doubleValue(), timestamp, 0, 0, type).getMessage();
	}

	public SensorsStore saveValue(String name, Object value) {
		values.put(name, value);
		return this;
	}

	public Object getValue(String name) {
		return values.get(name);
	}

	public void stop() {
		logger.info("Stopping...");
		enabled = false;
		configured = false;
		scheduledThreadPool.shutdownNow();
		sensors.clear();
	}

	private static String scriptInline() throws IOException {
		File confFile = new File("conf/");
		String[] scripts;
		String res = "";
		scripts = confFile.list(new FilenameFilter() {

			@Override
			public boolean accept(File dir, String fileName) {
				return fileName.matches(".*.js");
			}
		});
        if(scripts != null){
	    	for (String scriptName : scripts) {
		    	res += readText(scriptName);
		    }
        }
		return res;
	}

	private static String readText(String fileName) throws IOException {
		BufferedReader bufreader = null;
		bufreader = new BufferedReader(new FileReader("./conf/" + fileName));
		StringBuilder sb = new StringBuilder();
		String line;
		while ((line = bufreader.readLine()) != null)
			sb.append(line);
		bufreader.close();

		String res = sb.toString();
		return res;
	}

}
