package com.synapsense.plugin.dbplugin;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.synapsense.plugin.api.exceptions.CreateDeviceException;
import com.synapsense.plugin.api.exceptions.UnableGetDataException;
import com.synapsense.plugin.apinew.ObjectDesc;

public class DBConnection {
	private final static Logger logger = Logger.getLogger(DBConnection.class);
	private String dbDriverName;
	private String dbPath;
	private String dbUser;
	private String dbPass;
	private Connection connection = null;
	private Integer lock = new Integer(0);

	public DBConnection(final String dbName, final String dbDriverName, final String dbPath, final String dbUser,
	        final String dbPass) throws IllegalArgumentException, CreateDeviceException {
		this.dbDriverName = dbDriverName;
		this.dbPath = getFullDbPath(dbPath, dbName);
		this.dbUser = dbUser;
		this.dbPath = dbPass;
	}

	public DBConnection(final ObjectDesc database) {
		this.dbDriverName = database.getPropertyValue(DBPluginConstants.DB_DBDRIVER);

		this.dbPath = getFullDbPath(database.getPropertyValue(DBPluginConstants.DB_DBPATH),
		        database.getPropertyValue(DBPluginConstants.DB_NAME));
		this.dbUser = database.getPropertyValue(DBPluginConstants.DB_USER);
		this.dbPass = database.getPropertyValue(DBPluginConstants.DB_PASSWORD);
		// try to load driver
		logger.info("Loading driver for database [" + dbDriverName + "]");
		try {
			Class.forName(dbDriverName).newInstance();
		} catch (InstantiationException e) {
			logger.error("Unable to load driver for database [" + dbDriverName + "]");
		} catch (IllegalAccessException e) {
			logger.error("Unable to load driver for database [" + dbDriverName + "]");
		} catch (ClassNotFoundException e) {
			logger.error("Unable to load driver for database [" + dbDriverName + "]");
		}
	}

	public void connect() throws CreateDeviceException {
		synchronized (lock) {
			try {

				if (connection == null || connection.isClosed()) {
					connection = DriverManager.getConnection(dbPath, dbUser, dbPass);
				}
			} catch (SQLException e) {
				// if we were not able to load driver for the database in the
				// constructor, exception with text
				// "No suitable driver found for url" will be thrown
				logger.error("Cannot connect to " + dbPath);
				throw new CreateDeviceException("Cannot connect to [" + dbPath + "]. " + e.getMessage());
			}
		}
	}

	public void disconnect() {
		synchronized (lock) {
			try {
				if (connection != null && !connection.isClosed()) {
					connection.close();
				}
			} catch (SQLException e) {
				logger.error("Cannot close connection.", e);
			}
		}
	}

	public void write(String query) {
		try {
			PreparedStatement ps = connection.prepareStatement(query);
			ps.executeUpdate();
		} catch (SQLException e) {
			logger.error("Faild to execute query " + query, e);
		}

	}

	public String read(final String field, String fromString) throws CreateDeviceException, SQLException {
		String result = null;
		if (connection == null || connection.isClosed()) {
			connect();
		}
		PreparedStatement countStatement = null;
		PreparedStatement originalStatement = null;
		String originalQuery = "SELECT " + field + fromString;
		try {
			// execute COUNT request first to make sure there is a row for this
			// query condition
			countStatement = connection.prepareStatement("SELECT count(*) " + fromString);
			logger.debug("Executing query [" + countStatement.toString() + "]");
			countStatement.execute();
			ResultSet rs = countStatement.getResultSet();
			if (rs.next()) {
				int rowNum = rs.getInt(1);
				if (rowNum > 0) {
					if (rowNum == 1) {
						originalStatement = connection.prepareStatement(originalQuery);
					} else {
						logger.warn("Multiple records found for query [" + originalQuery
						        + "]. Result from the 1st record will be returned.");
						originalStatement = connection.prepareStatement(originalQuery + " LIMIT 1");
					}
					logger.debug("Executing query [" + originalStatement.toString() + "]");
					originalStatement.execute();
					rs = originalStatement.getResultSet();
					boolean hnr = rs.next();
					if (hnr) {
						result = rs.getString(1);
					}
				}
			}
		} catch (SQLException e) {
			logger.error("Query execution is failed: [" + originalQuery + "]. " + e);
			throw e;
		} finally {
			if (countStatement != null) {
				try {
					countStatement.close();
				} catch (SQLException e) {
					logger.error("Unable to close statement: " + countStatement.toString(), e);
				}
			}
			if (originalStatement != null) {
				try {
					originalStatement.close();
				} catch (SQLException e) {
					logger.error("Unable to close statement: " + originalStatement.toString(), e);
				}
			}
		}

		if (result == null) {
			logger.error("No records found for query [" + originalQuery + "]");
			throw new UnableGetDataException("No records found for query [" + originalQuery + "]");
		}
		return result;

	}

	public String getConnectionString() {
		return dbDriverName + ":" + dbPath;
	}

	private String getFullDbPath(String path, String dbName) {
		String fullPath = path.endsWith("/") ? path + dbName : path + "/" + dbName;
		return fullPath;
	}
}
