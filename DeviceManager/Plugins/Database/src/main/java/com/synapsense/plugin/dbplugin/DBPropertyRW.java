package com.synapsense.plugin.dbplugin;

import java.sql.SQLException;

import com.synapsense.plugin.api.exceptions.CreateDeviceException;
import com.synapsense.plugin.apinew.ObjectDesc;

/**
 * 
 * @author stikhon
 * 
 */
public class DBPropertyRW {
	public static String readProperty(final ObjectDesc fieldObj, final DBConnection connection)
	        throws NumberFormatException, CreateDeviceException, SQLException {
		String result = null;
		ObjectDesc table = fieldObj.getParent();
		String field = fieldObj.getPropertyValue(DBPluginConstants.DB_FIELD_NAME);

		String st = " FROM " + table.getPropertyValue(DBPluginConstants.DB_TABLE_NAME) + " "
		        + table.getPropertyValue(DBPluginConstants.DB_TABLE_CONDITION);
		result = connection.read(field, st);
		return result;
	}

	public static void writeProperty(final ObjectDesc field, final DBConnection connection, final String value)
	        throws NumberFormatException, CreateDeviceException, SQLException {

		ObjectDesc table = field.getParent();
		String fieldName = field.getPropertyValue(DBPluginConstants.DB_FIELD_NAME);
		String hst = "INSERT INTO " + table.getPropertyValue(DBPluginConstants.DB_TABLE_NAME) + " ";
		String pst = "(" + fieldName;
		String vst = "VALUES(" + convertValue(value);

		for (ObjectDesc childField : table.getChildren()) {
			String fld = childField.getPropertyValue(DBPluginConstants.DB_FIELD_NAME);
			if (!fieldName.equalsIgnoreCase(fld)) {
				pst += "," + fld;
				vst += ",'" + readProperty(childField, connection) + "'";
			}
		}
		pst += ") ";
		vst += ") ";
		String st = hst + pst + vst;
		connection.write(st);
	}

	private static String convertValue(String value) {
		return "'" + value + "'";
	}
}
