package com.synapsense.plugin.dbplugin;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.synapsense.plugin.api.exceptions.ConfigPluginException;
import com.synapsense.plugin.api.exceptions.CreateDeviceException;
import com.synapsense.plugin.api.exceptions.IncorrectStateException;
import com.synapsense.plugin.api.exceptions.UnableGetDataException;
import com.synapsense.plugin.apinew.ObjectDesc;
import com.synapsense.plugin.apinew.Plugin;
import com.synapsense.plugin.apinew.PluginConfig;
import com.synapsense.plugin.apinew.PropertyDesc;
import com.synapsense.plugin.apinew.Transport;

public class DBPlg implements Plugin {
	private final static Logger logger = Logger.getLogger(DBPlg.class);
	private final static String ID = "database";
	private static Properties PLUGIN_DESC = new Properties();
	static {
		PLUGIN_DESC.put(PLUGIN_ID, ID);
		PLUGIN_DESC.put(ROOT_TYPES, "DATABASE");
	}
	private Boolean configured = false;
	private boolean started = false;

	private Map<ObjectDesc, DBConnection> dbConnections = new HashMap<ObjectDesc, DBConnection>();
	private Transport transport;

	public Transport getTransport() {
		return transport;
	}

	public void setTransport(Transport transport) {
		this.transport = transport;
	}

	@Override
	public void configure(PluginConfig config) throws ConfigPluginException {
		stop();
		List<ObjectDesc> dbs = config.getRoots();
		for (ObjectDesc db : dbs) {
			dbConnections.put(db, new DBConnection(db));
		}
		synchronized (configured) {
			configured = true;
		}
		logger.debug("Plugin " + ID + " is configured.");
	}

	@Override
	public String getID() {
		return ID;
	}

	@Override
	public Properties getPluginDescription() {
		return PLUGIN_DESC;
	}

	@Override
	public void start() throws IncorrectStateException {
		if (!configured) {
			throw new IncorrectStateException("Plugin " + ID + " has to be configured before start.");
		}
		if (!started) {
			synchronized (configured) {
				for (DBConnection db : dbConnections.values()) {
					try {
						db.connect();
					} catch (CreateDeviceException e) {
						logger.error("Unable to connect to the database " + db.getConnectionString(), e);
					}
				}
			}
			started = true;
			logger.debug("Plugin " + ID + " is started.");
		}
	}

	@Override
	public void stop() {
		if (started) {
			for (DBConnection db : dbConnections.values()) {
				db.disconnect();
			}
		}
		dbConnections.clear();
		configured = false;
		started = false;
		logger.debug("Plugin " + ID + " is stopped.");
	}

	@Override
	public void setPropertyValue(ObjectDesc object, List<String> propertyName, List<Serializable> values) {

	}

	@Override
	public Serializable getPropertyValue(ObjectDesc object, String propertyName) {
		String value = null;
		if (object != null) {
			PropertyDesc pro = object.getProperty(propertyName);
			if (pro != null && DBPluginConstants.TYPE_DBFIELD.equals(object.getType())) {
				try {
					ObjectDesc db = object.getParent().getParent();
					value = DBPropertyRW.readProperty(object, dbConnections.get(db));
				} catch (Exception e) {
					logger.error("Cannot read property [" + pro.getName() + "]. " + e.getMessage());
					throw new UnableGetDataException("Cannot read property [" + pro.getName() + "]. " + e.getMessage());
				}
			}
		}
		return value;
	}

	@Override
	public void setPropertyValue(ObjectDesc object, String propertyName, Serializable value) {
		ObjectDesc obj = object;
		if (obj != null) {
			PropertyDesc pro = object.getProperty(propertyName);
			if (pro != null && DBPluginConstants.TYPE_DBFIELD.equals(object.getType())) {
				try {
					ObjectDesc db = object.getParent().getParent();
					DBPropertyRW.writeProperty(object, dbConnections.get(db), (String) value);
				} catch (Exception e) {
					logger.error("Cannot write property" + "\n\tObject id:" + object.getID() + "\n\tProperty name:"
					        + pro.getName(), e);
				}
			}
		}
	}

	@Override
	public List<Serializable> getPropertyValue(ObjectDesc object, List<String> propertyNames) {
		// TODO Auto-generated method stub
		return null;
	}

}
