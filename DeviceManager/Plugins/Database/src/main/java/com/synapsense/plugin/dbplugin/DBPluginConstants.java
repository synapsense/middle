package com.synapsense.plugin.dbplugin;

public class DBPluginConstants {
	public static final String TYPE_DATABASE = "DATABASE";
	public static final String TYPE_DBTABLE = "DBTABLE";
	public static final String TYPE_DBFIELD = "DBFIELD";

	public static final String DB_FIELD_NAME = "name";
	public static final String DB_FIELD_LAST_VALUE = "lastValue";

	public static final String DB_TABLE_NAME = "name";
	public static final String DB_TABLE_CONDITION = "condition";

	public final static String DB_NAME = "name";
	public final static String DB_DBDRIVER = "dbdriver";
	public final static String DB_DBPATH = "dbpath";
	public final static String DB_USER = "user";
	public final static String DB_PASSWORD = "password";

}
