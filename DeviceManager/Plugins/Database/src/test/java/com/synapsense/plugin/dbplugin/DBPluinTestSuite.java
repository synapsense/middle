package com.synapsense.plugin.dbplugin;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.synapsense.plugin.dbplugin.tests.DBConnectionTest;
import com.synapsense.plugin.dbplugin.tests.DBPlgTest;
import com.synapsense.plugin.dbplugin.tests.DBPropertyRWTest;

@RunWith(Suite.class)
@SuiteClasses({ DBConnectionTest.class, DBPlgTest.class, DBPropertyRWTest.class })
public class DBPluinTestSuite {
}
