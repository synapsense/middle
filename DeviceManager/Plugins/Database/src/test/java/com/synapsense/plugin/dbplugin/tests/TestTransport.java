package com.synapsense.plugin.dbplugin.tests;

import java.io.IOException;
import java.io.Serializable;

import org.apache.log4j.Logger;
import org.junit.Test;

import com.synapsense.plugin.apinew.Transport;

public class TestTransport implements Transport {
	private final static Logger logger = Logger.getLogger(TestTransport.class);

	@Override
	public void connect() throws IOException {
	}

	@Override
	public int getPriority() {
		return 0;
	}

	@Override
	public void sendData(String objectID, String propertyName, Serializable value) {
		logger.debug("Transport message:\n\tObject ID: " + objectID + "\n\tProperty name:" + propertyName
		        + "\n\tValue:" + value);

	}

	@Override
	public void sendOrderedData(String objectID, String propertyName, Serializable value) {
		// TODO Auto-generated method stub

	}

	@Test
	public void emptyTest() {

	}

}
