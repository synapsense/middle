package com.synapsense.plugin.dbplugin.tests;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mockit.Mocked;
import mockit.NonStrictExpectations;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.synapsense.plugin.api.exceptions.ConfigPluginException;
import com.synapsense.plugin.api.exceptions.CreateDeviceException;
import com.synapsense.plugin.api.exceptions.IncorrectStateException;
import com.synapsense.plugin.apinew.ObjectDesc;
import com.synapsense.plugin.apinew.PluginConfig;
import com.synapsense.plugin.configuration.DmXmlConfigReader;
import com.synapsense.plugin.dbplugin.DBConnection;
import com.synapsense.plugin.dbplugin.DBPlg;
import com.synapsense.plugin.dbplugin.DBPropertyRW;

public class DBPlgTest {
	DBPlg dbPlugin;
	PluginConfig config;
	@Mocked
	ObjectDesc db;
	@Mocked
	DBPropertyRW dbProp;
	File tmpFile;

	@Before
	public void beforeTest() throws ConfigPluginException {
		tmpFile = new File("dbPlugin.tmp");
		tmpFile.setWritable(true);
		StringBuilder sb = new StringBuilder();

		sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
		sb.append("<DMConfig xmlns=\"http://www.synapsense.com/plugin/apinew\"\n");
		sb.append("xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n");
		sb.append("xsi:schemaLocation=\"http://www.synapsense.com/plugin/apinew/dmPluginConfig.xsd\">\n");
		sb.append("<plugin name=\"database\">" + "<obj id=\"DATABASE:12\">\n");
		sb.append("<prop name=\"name\" value=\"synap\">" + "<tag name=\"pluginId\" value=\"database\" />\n");
		sb.append("</prop>" + "<prop name=\"dbdriver\" value=\"com.mysql.jdbc.Driver\" />\n");
		sb.append("<prop name=\"dbpath\" value=\"jdbc:mysql://localhost:3306\" />\n");
		sb.append("<prop name=\"user\" value=\"root\" />" + "<prop name=\"password\" value=\"\" />\n");
		sb.append("<obj id=\"DBTABLE:33\">" + "<prop name=\"name\" value=\"roles\" />\n");
		sb.append("<prop name=\"condition\" value=\"role_id='1'\" />" + "<obj id=\"DBFIELD:66\">\n");
		sb.append("<prop name=\"name\" value=\"role\" />" + "<prop name=\"lastValue\" value=\"\">\n");
		sb.append("<tag name=\"poll\" value=\"9000\"/>" + "</prop>" + "</obj>" + "</obj>" + "</obj>" + "</plugin>\n");
		sb.append("</DMConfig>\n");
		try {
			FileWriter fw = new FileWriter(tmpFile);
			fw.append(sb.toString());
			fw.flush();
			fw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Map<String, PluginConfig> confAllPlugin = DmXmlConfigReader.loadPluginsConfig(tmpFile);
		config = confAllPlugin.get("database");
	}

	@After
	public void afterTest() {
		tmpFile.delete();
	}

	@Test
	public void testConfig() throws InterruptedException, IncorrectStateException, ConfigPluginException {
		TestTransport tt = new TestTransport();
		dbPlugin = new DBPlg();
		dbPlugin.configure(config);
		dbPlugin.setTransport(tt);
		assertEquals(tt, dbPlugin.getTransport());
		dbPlugin.start();
		assertEquals("database", dbPlugin.getID());
		assertEquals("{ROOT_TYPES=DATABASE, PLUGIN_ID=database}", dbPlugin.getPluginDescription().toString());
		Thread.sleep(2000);
		dbPlugin.stop();

	}

	@Test
	public void testGetProperty() throws ConfigPluginException, CreateDeviceException, SQLException {
		dbPlugin = new DBPlg();
		dbPlugin.configure(config);
		final ObjectDesc object = config.getObject("DBFIELD:66");
		new NonStrictExpectations() {
			{
				Map<ObjectDesc, DBConnection> dbConnections = new HashMap<ObjectDesc, DBConnection>();
				DBPropertyRW.readProperty(object, dbConnections.get(db));
			}
		};
		dbPlugin.getPropertyValue(object, "name");

	}

	@Test
	public void testSetProperty() throws ConfigPluginException, CreateDeviceException, SQLException {
		dbPlugin = new DBPlg();
		dbPlugin.configure(config);
		final ObjectDesc object = config.getObject("DBFIELD:66");
		new NonStrictExpectations() {
			{
				Serializable value = "Roles";
				Map<ObjectDesc, DBConnection> dbConnections = new HashMap<ObjectDesc, DBConnection>();
				DBPropertyRW.writeProperty(object, dbConnections.get(db), (String) value);
			}
		};

		dbPlugin.setPropertyValue(object, "name", "Roles");
	}

	@Test
	public void otherTest() throws ConfigPluginException {
		dbPlugin = new DBPlg();
		dbPlugin.configure(config);
		ObjectDesc object = config.getObject("DBFIELD:66");
		List<String> propertyName = new ArrayList<String>();
		propertyName.add("name");
		propertyName.add("lastValue");
		List<Serializable> values = new ArrayList<Serializable>();
		values.add("role");
		values.add("");

		dbPlugin.setPropertyValue(object, propertyName, values);
		dbPlugin.getPropertyValue(object, propertyName);
	}

}
