package com.synapsense.plugin.dbplugin.tests;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Map;

import mockit.Mocked;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.synapsense.plugin.api.exceptions.ConfigPluginException;
import com.synapsense.plugin.api.exceptions.CreateDeviceException;
import com.synapsense.plugin.apinew.ObjectDesc;
import com.synapsense.plugin.apinew.PluginConfig;
import com.synapsense.plugin.configuration.DmXmlConfigReader;
import com.synapsense.plugin.dbplugin.DBConnection;
import com.synapsense.plugin.dbplugin.DBPropertyRW;

public class DBPropertyRWTest {
	@Mocked
	DBConnection connection;
	ObjectDesc object;
	DBPropertyRW dbProp;
	File tmpFile;

	@Before
	public void beforeTest() throws ConfigPluginException {
		tmpFile = new File("dbPlugin.tmp");
		tmpFile.setWritable(true);
		StringBuilder sb = new StringBuilder();

		sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
		sb.append("<DMConfig xmlns=\"http://www.synapsense.com/plugin/apinew\"\n");
		sb.append("xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n");
		sb.append("xsi:schemaLocation=\"http://www.synapsense.com/plugin/apinew/dmPluginConfig.xsd\">\n");
		sb.append("<plugin name=\"database\">" + "<obj id=\"DATABASE:12\">\n");
		sb.append("<prop name=\"name\" value=\"synap\">" + "<tag name=\"pluginId\" value=\"database\" />\n");
		sb.append("</prop>" + "<prop name=\"dbdriver\" value=\"com.mysql.jdbc.Driver\" />\n");
		sb.append("<prop name=\"dbpath\" value=\"jdbc:mysql://localhost:3306\" />\n");
		sb.append("<prop name=\"user\" value=\"root\" />" + "<prop name=\"password\" value=\"\" />\n");
		sb.append("<obj id=\"DBTABLE:33\">" + "<prop name=\"name\" value=\"roles\" />\n");
		sb.append("<prop name=\"condition\" value=\"role_id='1'\" />" + "<obj id=\"DBFIELD:66\">\n");
		sb.append("<prop name=\"name\" value=\"role\" />" + "<prop name=\"lastValue\" value=\"\">\n");
		sb.append("<tag name=\"poll\" value=\"9000\"/>" + "</prop>" + "</obj>" + "</obj>" + "</obj>" + "</plugin>\n");
		sb.append("</DMConfig>\n");
		try {
			FileWriter fw = new FileWriter(tmpFile);
			fw.append(sb.toString());
			fw.flush();
			fw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@After
	public void afterTest() {
		tmpFile.delete();
	}

	@SuppressWarnings("static-access")
	@Test
	public void readTest() throws ConfigPluginException, NumberFormatException, CreateDeviceException, SQLException {
		Map<String, PluginConfig> confAllPlugin = DmXmlConfigReader.loadPluginsConfig(tmpFile);
		PluginConfig config = confAllPlugin.get("database");
		object = config.getObject("DBFIELD:66");

		dbProp.readProperty(object, connection);
	}

	@SuppressWarnings("static-access")
	@Test
	public void writeTest() throws ConfigPluginException, NumberFormatException, CreateDeviceException, SQLException {
		Map<String, PluginConfig> confAllPlugin = DmXmlConfigReader.loadPluginsConfig(tmpFile);
		PluginConfig config = confAllPlugin.get("database");
		object = config.getObject("DBFIELD:66");

		dbProp.writeProperty(object, connection, "admin");
	}
}
