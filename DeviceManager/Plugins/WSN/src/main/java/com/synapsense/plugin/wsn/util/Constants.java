package com.synapsense.plugin.wsn.util;

public class Constants {

	public static final int DEFAULT_PORT_FOR_CONSOLE_TO_BASE = 4920;

	public static final int SAMPLE_FREQUENCY_MINUTES = 1;
	public static final int SAMPLE_FREQUENCY_HOURS = 2;

	// For QUEUE SIZING
	public static final int BLOCKINGQUEUE_MAXSIZE_BEFOREBLOCK = 5000;

	public static final int ABNORMAL_SYSTEM_TERMINATION = 1;

	public static final int NODE_TYPE_MOTE = 0;
	// COMMAND CONSTANTS BEGIN HERE

	public static final int COMMAND_ID_RESET_NETWORK = 3;

	public static final int COMMAND_ID_RESET_NODE = 4;

	public static final int COMMAND_ID_IDENTIFY_NODE = 9;

	public static final int COMMAND_ID_NODE_SEND_CURRENT_DATA = 10;

	public static final int COMMAND_ID_ENTER_SMOTA = 1001;

	public static final int PARAM_ID_SAMPLE_FREQUENCY = 5;

	public static final int PARAM_ID_CMDPLUGMETER = 51;

	public static final int PARAM_ID_UPDATE_PULSE_WIDTH = 55;

	public static final int PARAM_ID_ACTUATOR = 81;

	public static final int PARAM_ID_YHWINGCONTROL = 88;

	// COMMAND CONSTANTS END HERE

	public static final int SMOTASERVER_PORT = 2024;
	public static final int COMMANDSERVER_PORT = 2025;
}
