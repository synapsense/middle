package com.synapsense.plugin.wsn.knownprops.exceptions;

public class UnknownDeviceException extends Exception {
	public UnknownDeviceException(String desc) {
		super(desc);
	}

	public UnknownDeviceException(String desc, Throwable reason) {
		super(desc, reason);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -5870159862216454801L;
}