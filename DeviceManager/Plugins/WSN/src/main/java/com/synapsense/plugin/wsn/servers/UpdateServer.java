package com.synapsense.plugin.wsn.servers;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.synapsense.plugin.wsn.NetworkWorker;
import com.synapsense.plugin.wsn.util.Constants;
import com.synapsense.plugin.wsn.util.SensorNetwork;

public class UpdateServer implements Runnable {

	private HashMap<SensorNetwork, NetworkWorker> workerMap;
	private static Logger log = Logger.getLogger(UpdateServer.class);

	public UpdateServer(HashMap<SensorNetwork, NetworkWorker> workerMap) {
		this.workerMap = workerMap;
	}

	public void run() {
		ServerSocket serversocket = null;
		try {
			serversocket = new ServerSocket(Constants.SMOTASERVER_PORT, 1);
			log.info("Update Server Starting, on port "
					+ Constants.SMOTASERVER_PORT);
		} catch (IOException e) {
			log.error("Can't start UpdateServer.", e);
			return;
		}
		Socket clientsocket = null;

		while (true) {
			try {
				clientsocket = serversocket.accept();
				new UpdateServerThread(clientsocket, workerMap).start();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				log.error("Cannot create new UpdateServerThread", e);
			}

		}

	}
}
