package com.synapsense.plugin.wsn.util.sensors;

import com.synapsense.plugin.wsn.util.SensorStates;

public class SHT11TemperatureSensor implements SensorConversionInterface {

	public double convertFromRaw(char raw) {

		/*
		 * double d1 = 0.01; double d2 = -39.6; double d3 = -2e-8; double f =
		 * 7000; double temp = d2 + d1 *(double)raw + d3 * ((double)raw -
		 * f)*((double)raw - f);
		 */
		double tempC = 0.01 * raw - 40;

		if (tempC <= -40 || tempC >= 100) {
			return SensorStates.SENSOR_SERVICE;
		}

		double tempF = (9.0 / 5.0) * tempC + 32;

		return tempF;
	}

	public double convertFromRawCompensated(char raw, double other) {
		return convertFromRaw(raw);
	}

	public int getMoteDataType() {
		return 1;
	}
}
