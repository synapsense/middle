package com.synapsense.plugin.wsn.util.sensors;

public class SDP610 implements SensorConversionInterface {

	public double convertFromRaw(char raw) {

		int val = (int) raw;
		if ((val & 0x8000) != 0) {
			val = val * -1;
			val = val & 0xFFFF; // Small sex change
			val = val * -1;
		}
		// Inches of water
		double value = (double) val / 16384.0;

		// if (value < -0.01) {
		// return SensorStates.SENSOR_SERVICE;
		// }
		return value;
	}

	public double convertFromRawCompensated(char raw, double other) {
		return convertFromRaw(raw);
	}

	public int getMoteDataType() {
		return 29;
	}

}
