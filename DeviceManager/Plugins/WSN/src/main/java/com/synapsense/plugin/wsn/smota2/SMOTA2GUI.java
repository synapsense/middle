package com.synapsense.plugin.wsn.smota2;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.TreeSet;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;
import javax.swing.SpringLayout;
import javax.swing.filechooser.FileNameExtensionFilter;

import com.synapsense.plugin.wsn.util.Constants;
import com.synapsense.plugin.wsn.util.IHexParser;
import com.synapsense.plugin.wsn.util.SpringUtilities;

public class SMOTA2GUI extends JFrame implements ItemListener, ActionListener {

	private JRadioButton[] imagetypebuttons;
	private JCheckBox[] mspcheckboxes;
	private JCheckBox[] pmcheckboxes;
	private JCheckBox[] efmcheckboxes;
	private JFileChooser fc;
	private JTextArea[] mspfilenamearea;
	private JButton[] mspfileopenbuttons;
	private JTextArea[] pmfilenamearea;
	private JButton[] pmfileopenbuttons;
	private JTextArea[] efmfilenamearea;
	private JButton[] efmfileopenbuttons;
	private JButton gobutton;
	private JTextArea outputtext;
	private int networkid;
	private IHexParser[] mspcodes;
	private int[] mspversions;
	private IHexParser[] pmcodes;
	private IHexParser[] efmcodes;
	private long[] efmversions;
	private Socket socket;
	private boolean updatestarted;
	private Object gobuttonlock;
	private int imageselected;

	private final String[] imagetypelist = { "Application", "Diag", "SMOTA" };
	private final int[] imagetypeids = { 1, 2, 3 };
	private final String[] imagesignatures = { "APP", "DIG", "OTA" };
	private final String[] msplist = { "GateWay", "ThermaNode",
			"Pressure node 2", "Constellation", "Constellation 2",
			"Chilled Water Energy", "ThermaNode 2", "P3 SmartPlug Controller",
			"SmartLink", "Thermanode DX" };
	private final int[] mspids = { 0, 11, 13, 14, 15, 16, 17, 51, 52, 20 };
	private final String[] efmlist = { "Damper Control Node", "Thermanode EZ",
			"Thermanode EZH", "NTB", "NTBNP" };
	private final int[] efmids = { 80, 81, 82, 83, 92, 93, 84, 85, 86, 87 };
	private final String[] efmsignatures = { "ACK", "YWI", "HWI", "NTB", "NTN" };
	private final String[] pmlist = { "P3 SmartPlug Controller",
			"P3 SmartPlug App", "P3 SmartPlug SMOTA" };
	private final String[] pmsignatures = { "SAC", "APP", "OTA" };

	private static final String smotaversion = "2.0";

	public SMOTA2GUI() {
		fc = new JFileChooser();
		fc.setCurrentDirectory(new File("."));
		FileNameExtensionFilter filter = new FileNameExtensionFilter(
				"IHex File", "ihex", "hex");
		fc.setFileFilter(filter);
		mspcodes = new IHexParser[msplist.length];
		mspversions = new int[msplist.length];
		efmcodes = new IHexParser[efmlist.length];
		efmversions = new long[efmlist.length];
		pmcodes = new IHexParser[pmlist.length];
		updatestarted = false;
		gobuttonlock = new Object();
		imageselected = 0;
	}

	public void addComponentsToPane(Container pane) {
		JPanel selectionpanel = new JPanel(new GridLayout(0, 1));
		JLabel label = new JLabel("WSN Node Update");
		selectionpanel.add(label);

		JPanel imagepanel = new JPanel(new GridLayout(0, imagetypelist.length));
		imagetypebuttons = new JRadioButton[imagetypelist.length];
		ButtonGroup group = new ButtonGroup();
		for (int i = 0; i < imagetypelist.length; i++) {
			imagetypebuttons[i] = new JRadioButton(imagetypelist[i]);
			imagetypebuttons[i].addActionListener(this);
			if (i == imageselected)
				imagetypebuttons[i].setSelected(true);
			group.add(imagetypebuttons[i]);
			imagepanel.add(imagetypebuttons[i]);
		}
		selectionpanel.add(imagepanel);

		mspcheckboxes = new JCheckBox[msplist.length];
		mspfilenamearea = new JTextArea[msplist.length];
		mspfileopenbuttons = new JButton[msplist.length];
		for (int i = 0; i < msplist.length; i++) {
			JPanel msppanel = new JPanel(new SpringLayout());
			mspcheckboxes[i] = new JCheckBox(msplist[i]);
			mspcheckboxes[i].addItemListener(this);
			msppanel.add(mspcheckboxes[i]);
			mspfilenamearea[i] = new JTextArea(1, 20);
			mspfilenamearea[i].setEditable(false);
			mspfilenamearea[i].setBackground(Color.lightGray);
			JScrollPane scrollPane = new JScrollPane(mspfilenamearea[i]);
			scrollPane
					.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
			msppanel.add(scrollPane);
			mspfileopenbuttons[i] = new JButton("...");
			msppanel.add(mspfileopenbuttons[i]);
			mspfileopenbuttons[i].addActionListener(this);
			SpringUtilities.makeCompactGrid(msppanel, 1, 3, 6, 6, 6, 6);
			selectionpanel.add(msppanel);
		}

		efmcheckboxes = new JCheckBox[efmlist.length];
		efmfilenamearea = new JTextArea[efmlist.length];
		efmfileopenbuttons = new JButton[efmlist.length];
		for (int i = 0; i < efmlist.length; i++) {
			JPanel efmpanel = new JPanel(new SpringLayout());
			efmcheckboxes[i] = new JCheckBox(efmlist[i]);
			efmcheckboxes[i].addItemListener(this);
			efmpanel.add(efmcheckboxes[i]);
			efmfilenamearea[i] = new JTextArea(1, 20);
			efmfilenamearea[i].setEditable(false);
			efmfilenamearea[i].setBackground(Color.lightGray);
			JScrollPane scrollPane = new JScrollPane(efmfilenamearea[i]);
			scrollPane
					.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
			efmpanel.add(scrollPane);
			efmfileopenbuttons[i] = new JButton("...");
			efmpanel.add(efmfileopenbuttons[i]);
			efmfileopenbuttons[i].addActionListener(this);
			SpringUtilities.makeCompactGrid(efmpanel, 1, 3, 6, 6, 6, 6);
			selectionpanel.add(efmpanel);
		}

		label = new JLabel("P3 SmartPlug Update");
		selectionpanel.add(label);

		pmcheckboxes = new JCheckBox[pmlist.length];
		pmfilenamearea = new JTextArea[pmlist.length];
		pmfileopenbuttons = new JButton[pmlist.length];
		for (int i = 0; i < pmcheckboxes.length; i++) {
			JPanel pmpanel = new JPanel(new SpringLayout());
			pmcheckboxes[i] = new JCheckBox(pmlist[i]);
			pmcheckboxes[i].addItemListener(this);
			pmpanel.add(pmcheckboxes[i]);
			pmfilenamearea[i] = new JTextArea(1, 20);
			pmfilenamearea[i].setEditable(false);
			pmfilenamearea[i].setBackground(Color.lightGray);
			JScrollPane scrollPane = new JScrollPane(pmfilenamearea[i]);
			scrollPane
					.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
			pmpanel.add(scrollPane);
			pmfileopenbuttons[i] = new JButton("...");
			pmpanel.add(pmfileopenbuttons[i]);
			pmfileopenbuttons[i].addActionListener(this);
			SpringUtilities.makeCompactGrid(pmpanel, 1, 3, 6, 6, 6, 6);
			selectionpanel.add(pmpanel);
		}

		gobutton = new JButton("GO");
		gobutton.addActionListener(this);
		selectionpanel.add(gobutton);
		JScrollPane selectionpanelscroll = new JScrollPane(selectionpanel);
		pane.add(selectionpanelscroll, BorderLayout.WEST);

		outputtext = new JTextArea(0, 60);
		outputtext.setEditable(false);
		outputtext.setLineWrap(true);
		JScrollPane scrollPane = new JScrollPane(outputtext);
		pane.add(scrollPane, BorderLayout.EAST);

	}

	private boolean checknumberimages(int countadded) {
		int count = countadded;
		for (int i = 1; i < msplist.length; i++) {
			if (mspcheckboxes[i].isSelected())
				count++;
		}
		for (int i = 0; i < efmlist.length; i++) {
			if (efmcheckboxes[i].isSelected())
				count = count + 2;
		}
		for (int i = 0; i < pmlist.length; i++) {
			if (pmcheckboxes[i].isSelected()) {
				count++;
				break;
			}
		}
		if (count > 8) {
			JOptionPane
					.showMessageDialog(
							this,
							"SMOTA2 only support up to 8 images excluding a gateway.\n"
									+ "Any number of Plugmeter images are considered as one image.\n"
									+ "Damper Control Node and Thermanode EZ image are considered as two images.",
							"Error", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		return true;
	}

	public void itemStateChanged(ItemEvent e) {
		if (e.getStateChange() == ItemEvent.SELECTED) {
			if (!checknumberimages(0)) {
				JCheckBox checkbox = (JCheckBox) (e.getSource());
				checkbox.setSelected(false);
				return;
			}
		}

		for (int i = 0; i < msplist.length; i++) {
			if (e.getSource() == mspcheckboxes[i]) {
				if (mspcheckboxes[i].isSelected())
					mspfilenamearea[i].setBackground(Color.white);
				else
					mspfilenamearea[i].setBackground(Color.lightGray);
			}
		}
		for (int i = 0; i < efmlist.length; i++) {
			if (e.getSource() == efmcheckboxes[i]) {
				if (efmcheckboxes[i].isSelected())
					efmfilenamearea[i].setBackground(Color.white);
				else
					efmfilenamearea[i].setBackground(Color.lightGray);
			}
		}
		for (int i = 0; i < pmlist.length; i++) {
			if (e.getSource() == pmcheckboxes[i]) {
				if (pmcheckboxes[i].isSelected())
					pmfilenamearea[i].setBackground(Color.white);
				else
					pmfilenamearea[i].setBackground(Color.lightGray);
			}
		}
	}

	public void actionPerformed(ActionEvent e) {
		// Check MSP file open button
		for (int i = 0; i < msplist.length; i++) {
			if (e.getSource() == mspfileopenbuttons[i]) {
				int addedimagesize = 0;
				if (!mspcheckboxes[i].isSelected())
					addedimagesize = 1;
				if (!checknumberimages(addedimagesize)) {
					return;
				}
				int returnVal = fc.showOpenDialog(SMOTA2GUI.this);
				if (returnVal == JFileChooser.APPROVE_OPTION) {
					File file = fc.getSelectedFile();
					try {
						int baseaddr = Firmware.MSPBASEADDR;
						int imagesize = Firmware.MSPIMAGESIZE;
						mspcodes[i] = new IHexParser(baseaddr, imagesize,
								file.getAbsolutePath());
						mspfilenamearea[i].setText(file.getAbsolutePath());
						mspcheckboxes[i].setSelected(true);
						mspversions[i] = 0;
					} catch (Exception ex) {
						mspcodes[i] = null;
						JOptionPane.showMessageDialog(this, "Error Opening "
								+ file.getAbsolutePath(), "Error",
								JOptionPane.ERROR_MESSAGE);
					}
					return;
				}
			}
		}

		// Check EFM file open button
		for (int i = 0; i < efmlist.length; i++) {
			if (e.getSource() == efmfileopenbuttons[i]) {
				int addedimagesize = 0;
				if (!efmcheckboxes[i].isSelected())
					addedimagesize = 2;
				if (!checknumberimages(addedimagesize)) {
					return;
				}
				int returnVal = fc.showOpenDialog(SMOTA2GUI.this);
				if (returnVal == JFileChooser.APPROVE_OPTION) {
					File file = fc.getSelectedFile();
					try {
						// Read Bootloader section to check the signature
						int baseaddr = Firmware.EFMBOOTADDR;
						int imagesize = Firmware.EFMBOOTSIZE;
						efmcodes[i] = new IHexParser(baseaddr, imagesize,
								file.getAbsolutePath());
						String signature = efmsignatures[i]
								+ imagesignatures[imageselected];
						if (!efmcodes[i].checkcontent(Firmware.EFMSIGADDR,
								signature.getBytes())) {
							efmcodes[i] = null;
							efmfilenamearea[i].setText("");
							efmfilenamearea[i].setBackground(Color.lightGray);
							efmcheckboxes[i].setSelected(false);
							JOptionPane.showMessageDialog(this,
									"Image is not recognized", "Error",
									JOptionPane.ERROR_MESSAGE);
							return;
						}
						baseaddr = Firmware.EFMBASEADDR;
						imagesize = Firmware.EFMIMAGESIZE;
						efmcodes[i] = new IHexParser(baseaddr, imagesize,
								file.getAbsolutePath());
						efmfilenamearea[i].setText(file.getAbsolutePath());
						efmcheckboxes[i].setSelected(true);
						byte[] versionbytes = efmcodes[i].getData(
								Firmware.EFMVERSION_LOCATION
										- Firmware.EFMBASEADDR, 4);
						efmversions[i] = (versionbytes[0] & 0xFF)
								| ((versionbytes[1] & 0xFF) << 8)
								| ((versionbytes[2] & 0xFF) << 16)
								| ((versionbytes[3] & 0xFF) << 24);
					} catch (Exception ex) {
						efmcodes[i] = null;
						JOptionPane.showMessageDialog(this, "Error Opening "
								+ file.getAbsolutePath(), "Error",
								JOptionPane.ERROR_MESSAGE);
					}
					return;
				}
			}
		}

		// Check PM file open button
		int addedimagesize = 1;
		for (int i = 0; i < pmlist.length; i++) {
			if (pmcheckboxes[i].isSelected()) {
				// one of the PM images is already selected. Not adding new
				// image size
				addedimagesize = 0;
				break;
			}
		}

		for (int i = 0; i < pmlist.length; i++) {
			if (e.getSource() == pmfileopenbuttons[i]) {
				if (!checknumberimages(addedimagesize)) {
					return;
				}
				int returnVal = fc.showOpenDialog(SMOTA2GUI.this);
				if (returnVal == JFileChooser.APPROVE_OPTION) {
					File file = fc.getSelectedFile();
					try {
						int baseaddr = Firmware.PMBASEADDR;
						int imagesize = Firmware.PMIMAGESIZE;
						pmcodes[i] = new IHexParser(baseaddr, imagesize,
								file.getAbsolutePath());
						if (!pmcodes[i].checkcontent(Firmware.PMSIGADDR,
								pmsignatures[i].getBytes())) {
							pmcodes[i] = null;
							pmfilenamearea[i].setText("");
							pmfilenamearea[i].setBackground(Color.lightGray);
							pmcheckboxes[i].setSelected(false);
							JOptionPane.showMessageDialog(this,
									"Image is not recognized", "Error",
									JOptionPane.ERROR_MESSAGE);
							return;
						}
						pmfilenamearea[i].setText(file.getAbsolutePath());
						pmcheckboxes[i].setSelected(true);
					} catch (Exception ex) {
						pmcodes[i] = null;
						JOptionPane.showMessageDialog(this, "Error Opening "
								+ file.getAbsolutePath(), "Error",
								JOptionPane.ERROR_MESSAGE);
					}
					return;
				}
			}
		}

		// If image type is changed, clear all file selection
		for (int i = 0; i < imagetypebuttons.length; i++) {
			if (e.getSource() == imagetypebuttons[i]) {
				if (i != imageselected) {
					for (int j = 0; j < msplist.length; j++) {
						mspcodes[j] = null;
						mspfilenamearea[j].setText("");
						mspfilenamearea[j].setBackground(Color.lightGray);
						mspcheckboxes[j].setSelected(false);
					}
					for (int j = 0; j < efmlist.length; j++) {
						efmcodes[j] = null;
						efmfilenamearea[j].setText("");
						efmfilenamearea[j].setBackground(Color.lightGray);
						efmcheckboxes[j].setSelected(false);
					}
					imageselected = i;
				}
				return;
			}
		}

		// Go button
		if (e.getSource() == gobutton) {
			if (updatestarted) {
				gobutton.setEnabled(false);
				synchronized (gobuttonlock) {
					gobuttonlock.notify();
				}
			} else {
				boolean updateexist = false;
				for (int i = 0; i < msplist.length; i++) {
					if (mspcheckboxes[i].isSelected()) {
						updateexist = true;
						break;
					}
				}
				for (int i = 0; i < efmlist.length; i++) {
					if (efmcheckboxes[i].isSelected()) {
						updateexist = true;
						break;
					}
				}
				for (int i = 0; i < pmlist.length; i++) {
					if (pmcheckboxes[i].isSelected()) {
						updateexist = true;
						break;
					}
				}
				if (!updateexist) {
					JOptionPane.showMessageDialog(this,
							"No image is selected for update.", "Error",
							JOptionPane.ERROR_MESSAGE);
					return;
				}
				int n = JOptionPane.showConfirmDialog(this,
						"Would you like to start?", "SMOTA2GUI",
						JOptionPane.YES_NO_OPTION);
				if (n == 0) {
					gobutton.setEnabled(false);
					for (int i = 0; i < imagetypelist.length; i++) {
						imagetypebuttons[i].setEnabled(false);
					}
					for (int i = 0; i < msplist.length; i++) {
						mspcheckboxes[i].setEnabled(false);
						mspfileopenbuttons[i].setEnabled(false);
					}
					for (int i = 0; i < efmlist.length; i++) {
						efmcheckboxes[i].setEnabled(false);
						efmfileopenbuttons[i].setEnabled(false);
					}
					for (int i = 0; i < pmlist.length; i++) {
						pmcheckboxes[i].setEnabled(false);
						pmfileopenbuttons[i].setEnabled(false);
					}
					updatestarted = true;
					new Thread(new Runnable() {
						public void run() {
							update();
						}
					}).start();
				}
			}
		}
	}

	public void connect(String ipaddr, String password) {
		TreeSet<Integer> networkset = new TreeSet<Integer>();
		try {
			socket = new Socket(ipaddr, Constants.SMOTASERVER_PORT);
			DataOutputStream out = new DataOutputStream(
					socket.getOutputStream());
			DataInputStream datain = new DataInputStream(
					socket.getInputStream());
			byte[] session = new byte[8];
			datain.readFully(session);
			MessageDigest hash = MessageDigest.getInstance("SHA-1");
			hash.update(session);
			byte[] hashbytes = hash.digest(password.getBytes("UTF-8"));
			out.write(hashbytes);
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					socket.getInputStream()));
			String serverversion = reader.readLine();
			if (serverversion == null) {
				throw new IOException("Server did not respond. wrong password?");
			}
			if (!serverversion.equalsIgnoreCase(smotaversion)) {
				JOptionPane.showMessageDialog(this,
						"Server reported different SMOTA version. Server: "
								+ serverversion + " Client: " + smotaversion,
						"Error", JOptionPane.ERROR_MESSAGE);
				System.exit(-1);
			}
			int numnetworks = Integer.parseInt(reader.readLine());
			for (int i = 0; i < numnetworks; i++)
				networkset.add(Integer.parseInt(reader.readLine()));
		} catch (Exception e) {
			JOptionPane.showMessageDialog(this, "Server Connection Error",
					"Error", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
			System.exit(-1);
		}
		Integer[] networksetarray = new Integer[networkset.size()];
		Iterator<Integer> iter = networkset.iterator();
		int count = 0;
		while (iter.hasNext()) {
			networksetarray[count] = iter.next();
			count++;
		}
		Integer networkidobj = (Integer) JOptionPane.showInputDialog(this,
				"Choose Network", "Network Selection",
				JOptionPane.PLAIN_MESSAGE, null, networksetarray,
				networksetarray[0]);
		if (networkidobj == null) {
			System.exit(-1);
		}
		networkid = networkidobj.intValue();
		this.setTitle("SMOTA for Network " + networkid);
	}

	public void update() {
		try {
			DataOutputStream out = new DataOutputStream(
					socket.getOutputStream());
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					socket.getInputStream()));
			out.writeInt(networkid); // Network id of 1
			for (int i = 0; i < imagetypelist.length; i++) {
				if (imagetypebuttons[i].isSelected()) {
					out.writeInt(imagetypeids[i]);
					break;
				}
			}
			out.writeBoolean(mspcheckboxes[0].isSelected());
			if (mspcheckboxes[0].isSelected()) {
				out.writeLong(mspversions[0]);
				out.write(mspcodes[0].getData());
			}
			int count = 0;
			for (int i = 1; i < msplist.length; i++) {
				if (mspcheckboxes[i].isSelected())
					count++;
			}
			for (int i = 0; i < efmlist.length; i++) {
				if (efmcheckboxes[i].isSelected())
					count = count + 2;
			}
			boolean pmupdate = false;
			for (int i = 0; i < pmlist.length; i++) {
				if (pmcheckboxes[i].isSelected()) {
					pmupdate = true;
					count++;
					break;
				}
			}
			out.writeInt(count);
			for (int i = 1; i < msplist.length; i++) {
				if (mspcheckboxes[i].isSelected()) {
					out.write(mspids[i]);
					out.writeLong(mspversions[i]);
					out.write(mspcodes[i].getData());
				}
			}
			for (int i = 0; i < efmlist.length; i++) {
				if (efmcheckboxes[i].isSelected()) {
					// EFM takes two image size
					out.write(efmids[i * 2]);
					out.writeLong(efmversions[i]);
					out.write(efmcodes[i].getData(0, 48 * 1024));
					out.write(efmids[i * 2 + 1]);
					out.writeLong(efmversions[i]);
					out.write(efmcodes[i].getData(48 * 1024, 48 * 1024));
				}
			}

			if (pmupdate) {
				out.write(71);
				long pmimagemask = 0;
				if (pmcheckboxes[0].isSelected())
					pmimagemask = pmimagemask | (1 << 8);
				if (pmcheckboxes[1].isSelected())
					pmimagemask = pmimagemask | (1 << 4);
				if (pmcheckboxes[2].isSelected())
					pmimagemask = pmimagemask | 1;
				out.writeLong(pmimagemask);
				for (int i = 0; i < 3; i++) {
					if (pmcheckboxes[i].isSelected())
						out.write(pmcodes[i].getData());
					else
						for (int j = 0; j < 16 * 1024; j++)
							out.write(0);
				}
			}

			SimpleDateFormat format = new SimpleDateFormat(
					"yyyy.MM.dd HH:mm:ss");
			String line = reader.readLine();
			while (line != null) {
				if (line.equals("JOINJOINJOIN")) {
					int n = JOptionPane.showConfirmDialog(this,
							"Do you want to retry nodes to join?", "Join Fail",
							JOptionPane.YES_NO_OPTION);
					if (n == 1) {
						out.write(0);
					} else if (n == 0) {
						outputtext
								.append("Please reset listed gateways if any and bring all nodes "
										+ "listed close to any gateway and reset, then click the GO button.\n");
						outputtext.setCaretPosition(outputtext.getDocument()
								.getLength());
						gobutton.setEnabled(true);
						synchronized (gobuttonlock) {
							gobuttonlock.wait();
						}
						out.write(1);
					}
				} else if (line.equals("HEARHEARHEAR")) {
					int n = JOptionPane.showConfirmDialog(this,
							"Do you want to retry nodes to hear the update?",
							"Tree Fail", JOptionPane.YES_NO_OPTION);
					if (n == 1) {
						out.write(0);
					} else if (n == 0) {
						outputtext
								.append("Please bring all nodes those can not hear update close to any gateway "
										+ "and reset, then click the GO button.\n");
						outputtext.setCaretPosition(outputtext.getDocument()
								.getLength());
						gobutton.setEnabled(true);
						synchronized (gobuttonlock) {
							gobuttonlock.wait();
						}
						out.write(1);
					}
				} else if (line.equals("FAILFAILFAIL")) {
					int n = JOptionPane.showConfirmDialog(this,
							"Do you want to retry failed nodes?",
							"Update Fail", JOptionPane.YES_NO_OPTION);
					if (n == 1) {
						out.write(0);
					} else if (n == 0) {
						outputtext
								.append("Please bring all failed nodes close to any gateway and reset"
										+ ", then click the GO button.\n");
						outputtext.setCaretPosition(outputtext.getDocument()
								.getLength());
						gobutton.setEnabled(true);
						synchronized (gobuttonlock) {
							gobuttonlock.wait();
						}
						out.write(1);
					}
				} else if (line.equals("ENTERENTER")) {
					outputtext.append("Press click GO button to continue.\n");
					outputtext.setCaretPosition(outputtext.getDocument()
							.getLength());
					gobutton.setEnabled(true);
					synchronized (gobuttonlock) {
						gobuttonlock.wait();
					}
					out.write(1);
				} else {
					outputtext.append(format.format(new Date()) + " " + line
							+ "\n");
					outputtext.setCaretPosition(outputtext.getDocument()
							.getLength());
				}
				line = reader.readLine();
			}
			out.close();
			reader.close();
			socket.close();
			/*
			 * int n = JOptionPane.showConfirmDialog(this,
			 * "Do you want to save log files?", "Save",
			 * JOptionPane.YES_NO_OPTION); if (n == 0) { int returnVal =
			 * fc.showSaveDialog(this); if (returnVal ==
			 * JFileChooser.APPROVE_OPTION) { File file = fc.getSelectedFile();
			 * BufferedWriter writer = new BufferedWriter(new FileWriter(file));
			 * writer.write(outputtext.getDocument().getText(0,
			 * outputtext.getDocument().getLength())); writer.close(); } }
			 */
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static void main(String[] args) {

		if (args.length < 1 || args.length > 1) {
			System.err.println("Usage: SMOTA2GUI [IP address]");
			System.exit(-1);
		}
		JPanel panel = new JPanel();
		final JPasswordField pf = new JPasswordField(15);
		panel.add(new JLabel("Password"));
		panel.add(pf);
		JOptionPane pane = new JOptionPane(panel, JOptionPane.QUESTION_MESSAGE,
				JOptionPane.OK_CANCEL_OPTION) {
			@Override
			public void selectInitialValue() {
				pf.requestFocusInWindow();
			}
		};
		pane.createDialog(null, "Enter Password").setVisible(true);
		if ((int) pane.getValue() == JOptionPane.CANCEL_OPTION)
			System.exit(-1);

		SMOTA2GUI gui = new SMOTA2GUI();
		gui.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		gui.connect(args[0], new String(pf.getPassword()));

		// Set up the content pane.
		gui.addComponentsToPane(gui.getContentPane());

		// Display the window.
		gui.pack();
		gui.setVisible(true);

	}
}
