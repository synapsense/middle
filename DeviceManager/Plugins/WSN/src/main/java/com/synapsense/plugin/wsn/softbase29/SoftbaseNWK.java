package com.synapsense.plugin.wsn.softbase29;

import java.util.Iterator;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.plugin.wsn.msgbus.Message;
import com.synapsense.plugin.wsn.softbase29.packet.OSSDataPacket;
import com.synapsense.plugin.wsn.softbase29.packet.RadioMsg;
import com.synapsense.plugin.wsn.softbase29.state.NodeInfoEntry;
import com.synapsense.plugin.wsn.util.serializer.LittleSerializer;
import com.synapsense.plugin.wsn.util.serializer.Serializer;
import com.synapsense.util.algo.Pair;

public class SoftbaseNWK {

	public static final int PHY_HEADER_INDEX = 0;
	public static final int PHY_LENGTH_INDEX = 0;
	public static final int MAC_HEADER_INDEX = 1;
	public static final int MAC_FCFHI_INDEX = 0;
	public static final int MAC_FCFLO_INDEX = 1;
	public static final int MAC_DESTADDR_INDEX = 5;
	public static final int NWK_HEADER_INDEX = 11;
	public static final int NWK_DESTADDR_INDEX = 1;
	public static final int NWK_SRCADDR_INDEX = 3;

	public static final byte CC2420_FCF_HI_ACK = 0x20;
	// Protocols
	public static final int Protocol_Simple_Send = 0;
	public static final int Protocol_Simple_BCast = 1;
	public static final int Protocol_ToBase = 2;
	public static final int Protocol_ToSensors = 3;
	public static final int Protocol_Internal = 4;
	public static final int Protocol_ToOneSensor = 5;

	// Protocol Subtypes
	public static final int ToBase_Data = 0;
	public static final int ToBase_DatawithAddr = 3;
	public static final int ToBase_Route = 4;

	public static final int ToSensors_Data = 0;

	public static final int MAXHOP = 20;
	public static final int MaxTTL = (MAXHOP - 1);

	public static final int TOS_MAX_RETRY = 5;

	public static final int MaxTSRetry = 3;

	private int ToSensorsSeqno;
	private TimerTask TSAvail_reset_timer_id;
	private boolean TSAvail;

	private Softbase softbase;
	private ConcurrentHashMap<Byte, ArrayBlockingQueue<RadioMsg>> waitingQueues;

	private Timer timer;
	private static Log log = LogFactory.getLog(SoftbaseNWK.class);

	public SoftbaseNWK(Softbase softbase) {
		ToSensorsSeqno = 0;
		TSAvail = true;

		this.softbase = softbase;
		waitingQueues = new ConcurrentHashMap<Byte, ArrayBlockingQueue<RadioMsg>>();
		for (int i = 0; i < 256; i++) {
			waitingQueues.put((byte) i, new ArrayBlockingQueue<RadioMsg>(100));
		}
		timer = new Timer("NWK Timer-" + Integer.toString(softbase.getNetId()));
	}

	/**
	 * Send a packet to the network
	 * 
	 * @param handlers
	 *            The Message class to use for chaining. Note that if send()
	 *            throws an Exception, the message onError will NOT be invoked.
	 *            This is a design bug.
	 * @param data
	 *            The data array to send
	 * @param dest
	 *            The logical ID of the node to send it to
	 * @param service
	 *            The logical service ID, from OSSServices
	 * @param protocol
	 *            The protocol to use, i.e. Protocol_ToSensors. Sorry this isn't
	 *            an enum
	 * @throws SendError
	 */
	public void send(Message handlers, byte[] data, char dest, int service, int protocol) throws SendError {
		if (data.length > OSSDataPacket.OSS_DATA_LENGTH)
			throw new TooMuchData("Data length exceeded valid packet size: " + data.length);

		if (protocol == Protocol_Internal)
			throw new WrongProtocol("Wrong protocol used - I don't know about internal sending on the server");
		OSSDataPacket datapk = null;
		switch (protocol) {
		case Protocol_ToSensors:
			if (TSActive() || !TSAvail)
				throw new TimeSyncUnavailable("Can't send without time info");
			TSAvail = false;
			datapk = new OSSDataPacket(data.length);
			datapk.chain(handlers);

			datapk.nwk_destaddr = 0xFFFF;
			datapk.mac_destaddr = 0xFFFF;
			datapk.setProtocolSubtype(ToSensors_Data);
			datapk.nwk_seqno = (byte) ToSensorsSeqno;
			ToSensorsSeqno++;
			if (ToSensorsSeqno > 255)
				ToSensorsSeqno = 0;
			TSAvail_reset_timer_id = new TSAvail_reset_timer_h();
			timer.schedule(TSAvail_reset_timer_id, MIN_TSInterval());
			datapk.phy_length = (byte) data.length;
			datapk.nwk_srcaddr = 0;
			datapk.nwk_datalen = (byte) data.length;
			datapk.setProtocol(protocol);
			datapk.nwk_service = (byte) service;
			System.arraycopy(data, 0, datapk.data, 0, data.length);
			Iterator<com.synapsense.plugin.wsn.softbase.GateWay> iter = softbase.getGateways().iterator();
			while (iter.hasNext()) {
				GateWay gateway = (GateWay) iter.next();
				gateway.startToSensors(datapk, ToSensorsSeqno);
			}

			break;

		case Protocol_ToOneSensor:
			Pair<GateWay, Pair<Character, List<Character>>> pathinfo = softbase.node_info.getPath(dest);
			if (pathinfo == null) {
				log.trace("TOS: NO ROUTE");
				throw new NoRoutingInformation();
			}

			int pathlen = softbase.node_info.pathSizeRequired(softbase.node_info.getPath(dest).getSecond().getSecond()
			        .size());
			if (pathlen + data.length > OSSDataPacket.OSS_DATA_LENGTH) {
				log.trace("TOS: TOO LONG");
				throw new PathTooLong("Path is too long", (pathlen + data.length) - (OSSDataPacket.OSS_DATA_LENGTH - 2));
			}
			int padding = 0;
			if (data.length % 2 == 1)
				padding = 1;
			datapk = new OSSDataPacket(data.length + padding);
			datapk.chain(handlers);

			GateWay gateway = softbase.node_info.buildPath(dest, datapk);
			if (gateway == null) {
				log.trace("TOS: NO ROUTE");
				throw new NoRoutingInformation();
			}
			datapk.nwk_ttl = (byte) MaxTTL;
			datapk.nwk_destaddr = dest;
			enableAck(datapk);
			datapk.phy_length = (byte) (data.length + padding + datapk.path.length);
			datapk.nwk_srcaddr = 0;
			datapk.nwk_datalen = (byte) data.length;
			datapk.setProtocol(protocol);
			datapk.nwk_service = (byte) service;
			System.arraycopy(data, 0, datapk.data, 0, data.length);
			boolean gwsend = gateway.sendRadioPacket(datapk);
			if (!gwsend)
				throw new GatewaySendError();
			break;

		default:
			throw new SendError("Wrong protocol or other error");
		}

	}

	public boolean send(Message handlers, byte[] data, char dest, int service, int protocol, GateWay gateway) {
		if (data.length > OSSDataPacket.OSS_DATA_LENGTH)
			return false;

		if (protocol == Protocol_Internal)
			return false;
		OSSDataPacket datapk = new OSSDataPacket();
		datapk.chain(handlers);

		datapk.data = new byte[data.length];
		switch (protocol) {
		case Protocol_Simple_Send:
			datapk.nwk_destaddr = dest;
			datapk.mac_destaddr = dest;
			disableAck(datapk);
			break;

		case Protocol_Simple_BCast:
			datapk.nwk_destaddr = 0xFFFF;
			datapk.mac_destaddr = 0xFFFF;
			break;

		default:
			return false;
		}
		datapk.phy_length = (byte) data.length;
		datapk.nwk_srcaddr = 0;
		datapk.nwk_datalen = (byte) data.length;
		datapk.setProtocol(protocol);
		datapk.nwk_service = (byte) service;
		System.arraycopy(data, 0, datapk.data, 0, data.length);
		return gateway.sendRadioPacket(datapk);
	}

	public RadioMsg receive(byte serviceid) throws InterruptedException {
		return waitingQueues.get(serviceid).take();
	}

	public void setApp(OSSDataPacket pk, GateWay gateway) {
		RadioMsg msg = new RadioMsg(pk, gateway);
		try {
			waitingQueues.get(pk.nwk_service).add(msg);
		} catch (IllegalStateException e) {
			log.error("Queue full, packet dropped for service " + pk.nwk_service, e);
		}
	}

	public void process(OSSDataPacket pk, GateWay gateway) {
		// Ignore packets from other gateways
		if (pk.mac_srcaddr == 0)
			return;

		switch (pk.getProtocol()) {
		case Protocol_Simple_Send:
		case Protocol_Simple_BCast:
			setApp(pk, gateway);
			break;
		case Protocol_ToBase:
			process_2base(pk, gateway);
			break;
		case Protocol_ToSensors:
			process_2sensors(pk, gateway);
			break;
		}
	}

	public void senddone(OSSDataPacket pk, GateWay gateway) {
		switch (pk.getProtocol()) {

		case Protocol_ToOneSensor:
			if (pk.mac_ack != 0) {
				log.trace(gateway.getPortname() + " TOS:SENT");
				pk.onSuccess();
			} else {
				if (pk.Sendcount < TOS_MAX_RETRY) {
					log.trace(gateway.getPortname() + " TOS:RETRY");
					gateway.sendRadioPacket(pk);
				} else {
					log.trace(gateway.getPortname() + " TOS:MAX RETRY");
					pk.onError(new TooManyRetries());

				}
			}
			break;

		}
	}

	private void process_2sensors(OSSDataPacket datapk, GateWay gateway) {
	}

	private void process_2base(OSSDataPacket datapk, GateWay gateway) {
		NodeInfoEntry node = null;
		node = softbase.node_info.getById(datapk.nwk_srcaddr);
		if (node == null)
			node = softbase.node_info.addNode(datapk.nwk_srcaddr);
		switch (datapk.getProtocolSubtype()) {
		case ToBase_DatawithAddr:
		case ToBase_Route:
			if (!node.hasSequence(datapk.nwk_seqno))
				process_path(datapk, gateway);
		case ToBase_Data:
			if (node.hasSequence(datapk.nwk_seqno)) {
				log.trace(gateway.getPortname() + " TB: DUP");
				gateway.duppkcount++;
			} else {
				log.trace(gateway.getPortname() + " TB: NEW");
				gateway.tbpkcount++;

				Byte nextSequence = node.nextSequence();

				if (nextSequence != null && nextSequence != datapk.nwk_seqno)
					gateway.outoforderpkcount++;
				if (datapk.getProtocolSubtype() != ToBase_Route)
					setApp(datapk, gateway);

				node.addSeqLast(datapk.nwk_seqno);
			}
			break;
		}
	}

	private void process_path(OSSDataPacket pk, GateWay gateway) {
		char previd = 0xFFFF;
		NodeInfoEntry node = softbase.node_info.getById(pk.nwk_srcaddr);
		if (node == null)
			node = softbase.node_info.addNode(pk.nwk_srcaddr);
		if (pk.path != null) {
			Serializer serial = new LittleSerializer(pk.path);
			int len = pk.path.length / 2;
			for (int i = 0; i < len; i++) {
				char id = serial.unpackU_INT16();
				node = softbase.node_info.getById(id);
				if (node == null)
					node = softbase.node_info.addNode(id);
				if (i != 0)
					softbase.node_info.setParent(previd, id);
				previd = id;
			}
		}
		node = softbase.node_info.getById(pk.mac_srcaddr);
		if (node == null)
			node = softbase.node_info.addNode(pk.mac_srcaddr);
		if (previd != 0xFFFF)
			softbase.node_info.setParent(previd, pk.mac_srcaddr);
		softbase.node_info.setParentGW(pk.mac_srcaddr, gateway);
		softbase.node_info.reportPathInfo(pk.nwk_srcaddr);
	}

	class TSAvail_reset_timer_h extends TimerTask {
		public void run() {
			TSAvail = true;
		}
	}

	private void enableAck(OSSDataPacket pk) {
		pk.mac_fcfhi |= CC2420_FCF_HI_ACK;
	}

	private void disableAck(OSSDataPacket pk) {
		pk.mac_fcfhi &= ~CC2420_FCF_HI_ACK;
	}

	private boolean TSActive() {
		Iterator<com.synapsense.plugin.wsn.softbase.GateWay> iter = softbase.getGateways().iterator();
		while (iter.hasNext()) {
			GateWay gateway = (GateWay) iter.next();
			if (gateway.TSActive)
				return true;
		}
		return false;
	}

	private int MIN_TSInterval() {
		return 600 * softbase.getMACInterval();
	}

	public int getToSensorsSeqno() {
		return ToSensorsSeqno;
	}

	public void setToSensorsSeqno(int toSensorsSeqno) {
		ToSensorsSeqno = toSensorsSeqno;
	}

	public void reset() {
		ToSensorsSeqno = 0;
		TSAvail = true;
	}

	public void close() {
		timer.cancel();
	}
}
