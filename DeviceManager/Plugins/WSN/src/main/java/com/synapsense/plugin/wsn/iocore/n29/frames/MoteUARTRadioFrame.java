package com.synapsense.plugin.wsn.iocore.n29.frames;

import com.synapsense.plugin.wsn.iocore.frames.MoteUARTBadFrame;
import com.synapsense.plugin.wsn.iocore.frames.MoteUARTFrame;
import com.synapsense.plugin.wsn.util.serializer.LittleSerializer;
import com.synapsense.plugin.wsn.util.serializer.Serializer;

public class MoteUARTRadioFrame extends MoteUARTFrame {

	/* Sizes */
	public static final int PHY_HEADER_SZ = 1;
	public static final int MAC_HEADER_SZ = 10;
	public static final int NET_HEADER_SZ = 9;

	/* Offsets */

	/* relative in mac */
	public static final int MAC_DEST_INDEX = 5;
	public static final int MAC_SRC_INDEX = 7;
	public static final int MAC_TYPE_INDEX = 9;

	public static final int NET_SEQ = 7;
	public static final int NET_TTL = 8;

	public final static int NEIGHBOR_DATA_FRAME = 4;

	/* Masks, etc */

	public static final int MAC_BEACON = 1;
	public static final int MAC_DATA = 2;
	public static final int MAC_DATA_ACK = 3;
	public static final int MAC_TIMEBEACON = 4;
	public static final int MAC_JAMMING = 5;
	public static final int MAC_TIMEREQBEACON = 6;
	public static final int MAC_RETIMEBEACON = 7;
	public final static int CC2420_DEF_FCF_HI_ACK = 0x2;
	public final static int CC2420_DEF_FCF_HI_ACK_MSK = 0x07;

	private String s;

	public MoteUARTRadioFrame(byte[] header, byte[] data, String portName) throws MoteUARTBadFrame {
		super(header, portName);
		StringBuffer buf;
		this.data = data;
		try {
			buf = new StringBuffer();
			buf.append(parse());
		} catch (ArrayIndexOutOfBoundsException e) {
			buf = new StringBuffer();
			buf.append("ERROR PARSING");
		}
		buf.append("\n");
		for (int i = 0; i < data.length; i++) {
			int bytevalue = data[i] & 0xff;
			if (bytevalue < 0x10)
				buf.append("0");
			buf.append(Integer.toHexString(bytevalue).toUpperCase() + " ");
		}
		s = buf.toString();
	}

	public String toString() {
		return s;
	}

	public String parse() {
		StringBuffer buf = new StringBuffer();
		if (getType() == MoteUARTFrame.TYPE_RECV && (data[data.length - 1] & 0x80) == 0) {
			return "CRC FAIL";
		}
		Serializer serial = new LittleSerializer(data);
		int length = serial.unpackU_INT8();
		int fcfhi = (byte) serial.unpackU_INT8();
		serial.unpackU_INT8();
		int dsn = serial.unpackU_INT8();
		if (getType() == MoteUARTFrame.TYPE_RECV && (fcfhi & CC2420_DEF_FCF_HI_ACK_MSK) == CC2420_DEF_FCF_HI_ACK) {
			return "ACK (DSN: " + dsn + ")";
		}
		serial.setPosition(PHY_HEADER_SZ + MAC_DEST_INDEX);
		int dest = serial.unpackU_INT16();
		int source = serial.unpackU_INT16();
		serial.setPosition(PHY_HEADER_SZ + MAC_TYPE_INDEX);
		int smotavertype = serial.unpackU_INT8();
		int version = (smotavertype & 0x78) >> 3;
		int mac_type = smotavertype & 0x7;
		buf.append("Ver. " + version + " ");
		switch (mac_type) {
		case MAC_BEACON:
		case MAC_TIMEBEACON:
		case MAC_TIMEREQBEACON:
		case MAC_RETIMEBEACON:
			if (mac_type == MAC_TIMEBEACON)
				buf.append("Time ");
			if (mac_type == MAC_TIMEREQBEACON)
				buf.append("Time Request ");
			if (mac_type == MAC_RETIMEBEACON)
				buf.append("Re Time ");
			buf.append("Beacon From " + Integer.toHexString(source).toUpperCase() + ": ");
			serial.setPosition(MAC_HEADER_SZ + PHY_HEADER_SZ);

			int prstate_powparent = serial.unpackU_INT8();
			int routerstate = (prstate_powparent & 0xC0) >> 6;
			int parentpowerstate = (prstate_powparent & 0x20) >> 5;

			// ritu changes .. preferred gateway info
			int preferredBase = serial.unpackU_INT16();
			int secondaryBase = serial.unpackU_INT16();
			int hopPbase = serial.unpackU_INT8();
			int hopSbase = serial.unpackU_INT8();

			int slots = serial.unpackU_INT16();
			int hoptmrateLPLCh = serial.unpackU_INT16();
			int hoptm = (hoptmrateLPLCh & 0xFC00) >> 10;
			int rate = (hoptmrateLPLCh & 0x03E0) >> 5;
			int LPL = (hoptmrateLPLCh & 0x0010) >> 4;
			int Ch = hoptmrateLPLCh & 0x000F;
			long nwkstarttime = serial.unpackU_INT32();
			// ritu changes .. ch adaptation
			char[] chDuration = new char[4];
			chDuration[0] = serial.unpackU_INT16();
			chDuration[1] = serial.unpackU_INT16();
			chDuration[2] = serial.unpackU_INT16();
			chDuration[3] = serial.unpackU_INT16();

			// buf.append("Hop: " + hopcount + " preBase: " +
			// Integer.toHexString(preferredBase).toUpperCase() + " Slot: " +
			// Integer.toHexString(slots).toUpperCase()
			// + " HopTM: " + hoptm + " rate: " + rate + " LPL: " + LPL
			// + " CH: "+Ch+" NWK Start Time: "+nwkstarttime);

			// Ritu changes .. powRouter
			buf.append("Hop: " + hopPbase + " preBase: " + Integer.toHexString(preferredBase).toUpperCase() + " Hop2: "
			        + hopSbase + " SecBase: " + Integer.toHexString(secondaryBase).toUpperCase() + " Slot: "
			        + Integer.toHexString(slots).toUpperCase() + " HopTM: " + hoptm + " rate: " + rate + " LPL: " + LPL
			        + " CH: " + Ch + " NWK Start Time: " + nwkstarttime + " routerState: " + routerstate
			        + " ParentState " + parentpowerstate);

			buf.append(" chDur: " + Integer.toHexString(chDuration[0]).toUpperCase() + " "
			        + Integer.toHexString(chDuration[1]).toUpperCase() + " "
			        + Integer.toHexString(chDuration[2]).toUpperCase() + " "
			        + Integer.toHexString(chDuration[3]).toUpperCase());
			if (mac_type == MAC_TIMEBEACON || mac_type == MAC_RETIMEBEACON) {
				long seq = serial.unpackU_INT32();
				long txtime = serial.unpackU_INT32();
				buf.append(" SEQ: " + seq + " TxTime: " + txtime);
			}
			if (mac_type == MAC_TIMEREQBEACON) {
				long seq = serial.unpackU_INT32();
				buf.append(" SEQ: " + seq);
				for (int i = 0; i < 2; i++) {
					buf.append(" ID: " + Integer.toHexString(serial.unpackU_INT16()).toUpperCase());
				}
			}
			break;
		case MAC_DATA:
			serial.setPosition(MAC_HEADER_SZ + PHY_HEADER_SZ + 1);
			int net_dest = serial.unpackU_INT16();
			int net_src = serial.unpackU_INT16();
			buf.append("DSN: " + dsn);
			serial.setPosition(PHY_HEADER_SZ + MAC_HEADER_SZ + NET_SEQ);
			int seqno = serial.unpackU_INT8();
			int ttl = serial.unpackU_INT8();
			buf.append("(MAC S: " + Integer.toHexString(source).toUpperCase() + " D: "
			        + Integer.toHexString(dest).toUpperCase() + ") " + "(NET S: "
			        + Integer.toHexString(net_src).toUpperCase() + " D: " + Integer.toHexString(net_dest).toUpperCase()
			        + " TTL: " + ttl + " SEQ: " + seqno + ") " + "Data: ");
			if (getType() == MoteUARTFrame.TYPE_SEND)
				length = length - MAC_HEADER_SZ - NET_HEADER_SZ - 2;
			for (int i = 0; i < length; i++) {
				int d = serial.unpackU_INT8();
				if (d < 16)
					buf.append("0");
				buf.append(Integer.toHexString(d).toUpperCase());
				buf.append(" ");
			}
			break;
		case MAC_DATA_ACK:
			buf.append("ACK DSN: " + dsn + " S: " + Integer.toHexString(source).toUpperCase() + " D: "
			        + Integer.toHexString(dest).toUpperCase());
			break;
		case MAC_JAMMING:
			buf.append("Jamming from: " + Integer.toHexString(source).toUpperCase());
			break;
		default:
			return "Invalid MAC type " + Integer.toString(mac_type);
		}
		return buf.toString();
	}
}
