package com.synapsense.plugin.wsn.softbase29;

import java.util.concurrent.LinkedBlockingQueue;

import org.apache.log4j.Logger;

import com.synapsense.plugin.messages.WSNGWStatusBuilder;
import com.synapsense.plugin.wsn.SynapStore;
import com.synapsense.plugin.wsn.WsnConstants;
import com.synapsense.plugin.wsn.configuration.Configuration;
import com.synapsense.plugin.wsn.configuration.WSNNode;
import com.synapsense.util.algo.Pair;

public class GatewayStatusQueue {
	private final static Logger logger = Logger.getLogger(GatewayStatusQueue.class);
	private static Thread workingThread;
	private static AsyncQueue queue;

	private static class AsyncQueue implements Runnable {
		private LinkedBlockingQueue<Pair<String, Integer>> messageQueue;

		public AsyncQueue() {
			messageQueue = new LinkedBlockingQueue<Pair<String, Integer>>();
		}

		public void add(Pair<String, Integer> msg) {
			if (msg == null)
				throw new IllegalArgumentException("msg cannot be null.");
			messageQueue.add(msg);
		}

		@Override
		public void run() {
			boolean interrupted = false;
			while (!interrupted) {
				try {
					Pair<String, Integer> data = messageQueue.take();
					WSNNode node = Configuration.getNodeByObjectID(data.getFirst());
					long physID;
					if (node == null) {
						logger.warn("Couldn't determine gateway physical address. Default value (0xffff) is used in data packet.");
						physID = 0xffff;
					} else {
						physID = node.getPhysID();
					}
					if (SynapStore.transport == null)
						continue;
					SynapStore.transport.sendOrderedData(data.getFirst(), WsnConstants.GATEWAY_STATUS,
					        new WSNGWStatusBuilder(physID, data.getSecond()).getMessage());

				} catch (InterruptedException e) {
					interrupted = true;
				}
			}
		}
	}

	static {
		if (queue == null) {
			queue = new AsyncQueue();
			workingThread = new Thread(queue);
			workingThread.setDaemon(true);
			workingThread.start();
		}
	}

	public static void sendStatus(String gwID, int status) {
		queue.add(new Pair<String, Integer>(gwID, status));
	}
}
