package com.synapsense.plugin.wsn.iocore;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Random;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.plugin.wsn.iocore.frames.MoteUARTMsgAckFrame;
import com.synapsense.plugin.wsn.iocore.frames.out.MoteUARTOutFrame;

public class MoteCommWriter implements Runnable {

	private static final long TIMEOUT_MSGACK_PROCESSED = 3L; // in seconds

	public static final int TIMEOUT_MSGACK_RECV_MIN = 200;
	public static final int TIMEOUT_MSGACK_RECV_MAX = 500; // in ms

	private static final int ACK_FAIL_DELAY_TIME = 15000;

	public static final int MAX_RETRY_COUNT = 10;

	private OutputStream outputStream;

	private BlockingQueue<MoteUARTOutFrame> commandQueue;

	private static Log log = LogFactory.getLog(MoteCommWriter.class);

	private MoteUARTOutFrame fout = null;

	private Semaphore writeBlock;
	private Semaphore recvBlock;

	private MoteCommWriterDelayed delayed;

	private IRestartable moteComm;

	public MoteCommWriter(BlockingQueue<MoteUARTOutFrame> _cq, OutputStream o, IRestartable mc) {
		outputStream = o;
		this.commandQueue = _cq;
		writeBlock = new Semaphore(0);
		recvBlock = new Semaphore(0);
		moteComm = mc;

	}

	public void writeAck(MoteUARTMsgAckFrame ack) {
		int ready = ack.ack_type;
		log.info(moteComm.getPortName() + " Writing ACK Frame: Ready flag: " + ack.successful + ", type flag: "
		        + ack.ack_type);
		if (ready == 1) {
			if (ack.successful) {
				if (fout == null)
					return;
				fout.setAck(true);
			} else {
				fout.setDelayedCount(fout.getDelayedCount() + 1);
				if (fout.getDelayTimeLeft() > 0) {
					log.debug(moteComm.getPortName() + " ACK FAIL - delaying frame for " + ACK_FAIL_DELAY_TIME / 1000
					        + " seconds - delayCount: " + fout.getDelayedCount() + " time left: "
					        + fout.getDelayTimeLeft() / 1000 + "s");
					fout.setDelayTimeLeft(fout.getDelayTimeLeft() - ACK_FAIL_DELAY_TIME);
					fout.setDelayValue(ACK_FAIL_DELAY_TIME); // 15 seconds
					delayed.delay(fout);
				} else {
					log.debug(moteComm.getPortName() + " ACK FAIL - dropping unsucessful frame (elapsed failure time)");
				}
				fout.setAck(false);
			}
			if (writeBlock.availablePermits() == 0)
				writeBlock.release();
		} else if (ready == 0) {
			// This was a received message
			if (ack.successful)
				if (recvBlock.availablePermits() == 0)
					recvBlock.release();
		}

	}

	protected void write(MoteUARTOutFrame fout) throws IOException {

		byte[] data = fout.getFullFrame();
		byte crc = 0;
		for (int i = 0; i < data.length - 1; i++)
			crc = (byte) (crc ^ data[i]);
		data[data.length - 1] = crc;
		// log.trace(moteComm.getPortName()+" Writing " + data.length +
		// "bytes out, len of packet is " + (int)data[2]);
		if (outputStream == null)
			throw new IOException("Output stream is down");
		outputStream.write(data);
		outputStream.flush();
	}

	public void run() {

		delayed = new MoteCommWriterDelayed(commandQueue);
		Thread delayedThread = new Thread(delayed, "MoteCommDelayedQueue");
		delayedThread.setDaemon(true);
		delayedThread.start();
		Random random = new Random();
		while (true) {
			moteComm.getSupervisor().writerAlive();
			try {
				fout = commandQueue.take();
				if (fout.getDelayValue() > 0) {
					log.trace(moteComm.getPortName() + " Delaying a frame for "
					        + (fout.getDelayValue() - System.currentTimeMillis()) + " ms");
					delayed.delay(fout);
					continue;
					// this.write(fout);
				} else {
					if (recvBlock.availablePermits() > 0)
						recvBlock.drainPermits();
					if (writeBlock.availablePermits() > 0)
						writeBlock.drainPermits();
					this.write(fout);
					fout.setFramesent(true);
				}
				int i;
				for (i = 0; i < MAX_RETRY_COUNT; i++) {
					long delay = TIMEOUT_MSGACK_RECV_MIN
					        + random.nextInt(TIMEOUT_MSGACK_RECV_MAX - TIMEOUT_MSGACK_RECV_MIN);
					if (recvBlock.tryAcquire(delay, TimeUnit.MILLISECONDS) == false) {
						log.warn(moteComm.getPortName() + " Timeout on msgack-recv from base. Re-sending frame.");
						this.write(fout);
					} else {
						break;
					}
				}
				if (i >= MAX_RETRY_COUNT) {
					log.warn(moteComm.getPortName() + " Retry limit hit. Skipping frame and going to a new one.");
					fout.setAck(false);
					continue;
				}
				if (writeBlock.tryAcquire(TIMEOUT_MSGACK_PROCESSED, TimeUnit.SECONDS) == false) {
					log.warn(moteComm.getPortName()
					        + " Timeout on msgack-next from base. Skipping frame and going to a new one.");
					fout.setAck(false);
				}
			} catch (IOException e) {
				log.error(moteComm.getPortName() + " IO Error on write. Shutting down.", e);
				delayedThread.interrupt();
				moteComm.getSupervisor().writerError();

				return;
			} catch (InterruptedException e) {
				delayedThread.interrupt();
				return;
			}
		}
	}
}
