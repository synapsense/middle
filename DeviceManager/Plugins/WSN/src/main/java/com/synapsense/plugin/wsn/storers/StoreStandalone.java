package com.synapsense.plugin.wsn.storers;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.synapsense.plugin.wsn.NetworkWorker;
import com.synapsense.plugin.wsn.SynapStore;
import com.synapsense.plugin.wsn.WsnConstants;
import com.synapsense.plugin.wsn.configuration.Configuration;
import com.synapsense.plugin.wsn.configuration.SensorNotFoundException;
import com.synapsense.plugin.wsn.configuration.WSNActuator;
import com.synapsense.plugin.wsn.configuration.WSNNode;
import com.synapsense.plugin.wsn.configuration.WSNSensor;
import com.synapsense.plugin.wsn.iocore.n29.frames.out.MoteUARTOutNodeIDFrame;
import com.synapsense.plugin.wsn.msgbus.Message;
import com.synapsense.plugin.wsn.msgbus.MsgNHTInfo;
import com.synapsense.plugin.wsn.msgbus.MsgNodeInfo;
import com.synapsense.plugin.wsn.msgbus.MsgNwkStat;
import com.synapsense.plugin.wsn.msgbus.MsgRouteInformation;
import com.synapsense.plugin.wsn.msgbus.MsgSensorData;
import com.synapsense.plugin.wsn.util.SensorNetwork;
import com.synapsense.plugin.wsn.util.sensors.SensorConversionInterface;
import com.synapsense.plugin.wsn.util.sensors.SensorFactory;
import com.synapsense.plugin.wsn.util.timesync.TimeSync;
import com.synapsense.plugin.wsn.util.timesync.TimeSyncTime;

public class StoreStandalone {

	private static Logger log = Logger.getLogger(StoreStandalone.class);
	private static final long BACKDATECUTOFF = 30 * 60 * 1000;

	public static void store(Message message, SensorNetwork net, TimeSync times, boolean addnodeonthefly) {
		int networkId = net.getNetworkId();
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss.SSS");
		Date date = new Date(System.currentTimeMillis());
		String curtimestring = sdf.format(date).toString();
		if (message instanceof MsgNodeInfo) {
			MsgNodeInfo infoframe = (MsgNodeInfo) message;
			WSNNode node = Configuration.getNodeByPhysID(infoframe.device);
			if (node != null && node.getNetworkID() != net.getNetworkId())
				node = null;
			if (node == null) {
				if (addnodeonthefly) {
					int lastnodeid = Configuration.getLastNodeId(net.getNetworkId());
					node = new WSNNode("N" + Integer.toString(lastnodeid + 1), net.getNetworkId(), infoframe.device,
					        lastnodeid + 1, infoframe.platformId, 300, "");
					if (node.getPlatformID() == WSNNode.PLATFORM_ACKBAR || node.getPlatformID() == WSNNode.PLATFORM_NTB) {
						WSNActuator actuator = new WSNActuator("A" + (lastnodeid + 1), 0, WSNActuator.DIRECTION_CW, 50,
						        10, 90, 150, 5);
						actuator.setNode(node);
						node.addActuator(actuator);
					}
					if (node.getPlatformID() == WSNNode.PLATFORM_YWING
					        || node.getPlatformID() == WSNNode.PLATFORM_HWING
					        || node.getPlatformID() == WSNNode.PLATFORM_NTBNP) {
						Map<String, String> options = new HashMap<String, String>();
						options.put(WsnConstants.SENSOR_DELTA_T_LOW, "5");
						options.put(WsnConstants.SENSOR_DELTA_T_HIGH, "5");
						options.put(WsnConstants.SENSOR_CRITICAL_POINT, "75");
						options.put("subSamples", "15");
						WSNSensor sensor = new WSNSensor(node, "S" + Integer.toString(lastnodeid + 1), 3, 0, 0, options);
						node.addSensor(sensor);
					}
					Configuration.registerNode(node, false);
					try {
						PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("IDtable.txt", true)));
						out.println(net.getNetworkId() + "\t" + Long.toHexString(infoframe.device).toUpperCase() + "\t"
						        + (lastnodeid + 1) + "\t" + infoframe.platformId);
						out.close();
					} catch (IOException e) {
					}
				} else {
					node = new WSNNode("NODE", net.getNetworkId(), infoframe.device, 0xFFFF, infoframe.platformId, 300,
					        "");
					log.warn("Node (physical id: 0x" + Long.toHexString(infoframe.device)
					        + ") is not registered. Send reject.");
				}
			}
			NetworkWorker worker = SynapStore.workerMap.get(net);
			MoteUARTOutNodeIDFrame frame = new MoteUARTOutNodeIDFrame(node.getPhysID(), (char) node.getLogicalID(),
			        node.getSamplingInSeconds(), infoframe.source, infoframe.gateway, false);

			for (int i : node.getSensorChannels()) {
				String subsamplesS = null;
				try {
					subsamplesS = node.getSensor(i).getOption("subSamples");
				} catch (SensorNotFoundException e) {
				}
				if (subsamplesS != null) {
					frame.setSubsamples(Integer.parseInt(subsamplesS));
					break;
				}
			}
			worker.commandQ.add(frame);

			try {
				PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("Meta.txt", true)));
				out.println(curtimestring + ": Meta info from " + Long.toHexString(infoframe.device).toUpperCase()
				        + " Platform: " + (int) (infoframe.platformId & 0xFF) + " Ver: " + infoframe.osVersion + "."
				        + infoframe.osRevision);
				out.close();
			} catch (IOException e) {
			}
		} else if (message instanceof MsgSensorData) {
			MsgSensorData sensor = (MsgSensorData) message;
			TimeSyncTime timestamp = times.convertTime(sensor.datatimestamp, times.systemTime());
			if (!timestamp.isDidConvert()) {
				log.warn("Message from sensor " + sensor.sensor + " of 0x" + Long.toHexString(sensor.device)
						+ " doesn't have a timestamp and will be discarded.");
				return;
			}
			if (timestamp.getConvertedTime() < times.systemTime() - BACKDATECUTOFF) {
				log.warn("Message from sensor " + sensor.sensor + " of 0x" + Long.toHexString(sensor.device)
						+ " is too old and will be discarded.");
				return;
			}
			sensor.datatimestamp = timestamp.getConvertedTime();
			timestamp = times.convertTime(sensor.sendtimestamp, times.systemTime());
			sensor.sendtimestamp = timestamp.getConvertedTime();
			date = new Date(sensor.datatimestamp);
			String datatimestring = sdf.format(date).toString();
			date = new Date(sensor.sendtimestamp);
			String sendtimestring = sdf.format(date).toString();

			DecimalFormat decFor = new DecimalFormat("0.00");
			double dataReading = 0.0;
			SensorConversionInterface tempSensor = SensorFactory.createFromId(sensor.sensor);
			if (tempSensor != null) {
				dataReading = tempSensor.convertFromRaw(sensor.reading);
			}

			try {
				PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("Data.txt", true)));
				if (sensor.sensor == WSNSensor.SENSOR_ACKBAR_POSITION) {
					out.println(curtimestring + ": DATA from " + Integer.toHexString(sensor.device) + " Ch: "
					        + sensor.channel + " Value: " + decFor.format(dataReading) + " Sense Time: "
					        + datatimestring + " Send Time: " + sendtimestring + " State: " + sensor.devicestate
					        + " CR: " + sensor.controlresponse);
					log.info("DATA from " + Integer.toHexString(sensor.device) + " Ch: " + sensor.channel + " Value: "
					        + decFor.format(dataReading) + " Sense Time: " + datatimestring + " Send Time: "
					        + sendtimestring + " State: " + sensor.devicestate + " CR: " + sensor.controlresponse);
				} else {
					out.println(curtimestring + ": DATA from " + Integer.toHexString(sensor.device) + " Ch: "
					        + sensor.channel + " Value: " + decFor.format(dataReading) + " Sense Time: "
					        + datatimestring + " Send Time: " + sendtimestring);
					log.info("DATA from " + Integer.toHexString(sensor.device) + " Ch: " + sensor.channel + " Value: "
					        + (int) sensor.reading + "->" + decFor.format(dataReading) + " Sense Time: "
					        + datatimestring + " Send Time: " + sendtimestring);
				}
				out.close();
			} catch (IOException e) {
			}
		} else if (message instanceof MsgNwkStat) {
			MsgNwkStat nwkstat = (MsgNwkStat) message;
			TimeSyncTime timestamp = times.convertTime(nwkstat.stattimestamp, times.systemTime());
			nwkstat.stattimestamp = timestamp.getConvertedTime();
			date = new Date(nwkstat.stattimestamp);
			String timestring = sdf.format(date).toString();
			if (nwkstat.deviceId >= 0xF000) {
				try {

					// Ritu comment .. write to Statbase file
					PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("statbase.txt", true)));
					out.print(networkId + "\t" + (int) nwkstat.deviceId + "\t" + timestring + "\t" + nwkstat.tbpkcount
					        + "\t" + nwkstat.duppkcount + "\t" + nwkstat.outoforderpkcount + "\t" + nwkstat.pksentcount
					        + "\t" + nwkstat.freeslotcount + "\t" + nwkstat.wrongslotcount + "\t"
					        + nwkstat.totalslotcount + "\t" + nwkstat.pkrcvdcount + "\t" + nwkstat.crcfailpkcount
					        + "\t" + nwkstat.timelostcountsync + "\t" + nwkstat.timelostcounttimeout);
					for (int i = 0; i < 16; i++) {
						out.print("\t" + nwkstat.chstatus[i]);
					}
					out.print("\t" + nwkstat.noisethresh);
					out.println();
					out.close();
				} catch (IOException e) {
				}
			} else {
				try {

					// Ritu comment .. Write to Statnode file
					PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("statnode.txt", true)));
					out.print(networkId + "\t" + (int) nwkstat.deviceId + "\t" + timestring + "\t" + nwkstat.sendfailSQ
					        + "\t" + nwkstat.sendfailNR + "\t" + nwkstat.tbretrycount + "\t" + nwkstat.fwdpkdrop + "\t"
					        + nwkstat.parentfailcount + "\t" + nwkstat.tsretrycount + "\t" + nwkstat.tspkcount + "\t"
					        + nwkstat.tbpkcount + "\t" + nwkstat.fwdpkcount + "\t" + nwkstat.pksentcount + "\t"
					        + nwkstat.radiouptime + "\t" + nwkstat.freeslotcount + "\t" + nwkstat.wrongslotcount + "\t"
					        + nwkstat.totalslotcount + "\t" + nwkstat.pkrcvdcount + "\t" + nwkstat.crcfailpkcount
					        + "\t" + nwkstat.timelostcountsync + "\t" + nwkstat.timelostcounttimeout + "\t"
					        + (int) (nwkstat.rcvdtimestamp - timestamp.getConvertedTime()) + "\t" + nwkstat.hopcount
					        + "\t" + nwkstat.routerstate + "\t" + nwkstat.parentstate); // Ritu
					                                                                    // changes
					                                                                    // ..
					                                                                    // powRouter
					for (int i = 0; i < 16; i++) {
						out.print("\t" + nwkstat.chstatus[i]);
					}
					out.print("\t" + nwkstat.noisethresh);
					out.println();
					out.close();
				} catch (IOException e) {
				}
			}
		} else if (message instanceof MsgRouteInformation) {
			try {
				PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("route.txt", true)));
				out.println(curtimestring + ": " + message.toString());
				out.close();
			} catch (IOException e) {
			}
		} else if (message instanceof MsgNHTInfo) {
			try {
				PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("nht.txt", true)));
				out.println(curtimestring + ": " + message.toString());
				out.close();
			} catch (IOException e) {
			}
		}
	}
}