package com.synapsense.plugin.wsn.iocore.n29.frames.out;

import com.synapsense.plugin.wsn.iocore.frames.out.MoteUARTOutFrame;
import com.synapsense.plugin.wsn.util.serializer.BigSerializer;

public class MoteUARTOutCodeDistFrame extends MoteUARTOutFrame {

	public static final byte CD_CODE = 2;
	public static final byte CD_COMMAND = 3;
	public static final byte CD_CE_LAYOUT = 5;

	public static final byte CMD_STOREANDREPORT = 1;
	public static final byte CMD_BEGIN = 2;
	public static final byte CMD_FINISH = 3;

	public int packet_type;
	public byte platform;

	public MoteUARTOutCodeDistFrame(int platform, int layout, int part, byte[] data) {
		super((char) MoteUARTOutFrame.TYPE_CODEDIST, (char) 68);
		BigSerializer ser = new BigSerializer(frame, 4);

		packet_type = CD_CE_LAYOUT;
		this.platform = (byte) platform;
		ser.packU_INT8(CD_CE_LAYOUT);
		ser.packU_INT8((byte) platform);
		ser.packU_INT8((byte) part);
		int pos = ser.getPosition();
		for (int i = 0; i < data.length; i++) {
			frame[pos] = data[i];
			pos++;
		}
		ser.setPosition(pos);
	}

	public byte[] code_code;
	public long code_section;
	public int code_pseq;

	public MoteUARTOutCodeDistFrame(int platform, long section, int pseq, byte[] data) {

		// CODE

		super((char) MoteUARTOutFrame.TYPE_CODEDIST, (char) 71);

		packet_type = CD_CODE;
		this.platform = (byte) platform;

		this.code_code = data;
		this.code_section = section;
		this.code_pseq = pseq;

		BigSerializer ser = new BigSerializer(frame, 4);
		ser.packU_INT8(CD_CODE);
		ser.packU_INT8((byte) platform);
		ser.packU_INT32(section);
		ser.packU_INT8((byte) pseq);
		int pos = ser.getPosition();
		for (int i = 0; i < data.length; i++) {
			frame[pos] = data[i];
			pos++;
		}
		ser.setPosition(pos);

		// 1, 1, 4, 1, 64

	}

	/**
	 * Command frame to Mote
	 * 
	 * @param platform
	 * @param code
	 * @param data
	 */

	public byte cmd_1;
	public byte cmd_2;
	public byte cmd_3;
	public byte cmd_4;

	public MoteUARTOutCodeDistFrame(int platform, int code, int data, int data2, int data3) {

		// COMMAND

		super((char) MoteUARTOutFrame.TYPE_CODEDIST, (char) 6);
		packet_type = CD_COMMAND;
		this.platform = (byte) platform;
		BigSerializer ser = new BigSerializer(frame, 4);
		ser.packU_INT8(CD_COMMAND);
		ser.packU_INT8((byte) platform);

		// This is ugly
		cmd_1 = (byte) code;
		cmd_2 = (byte) data;
		cmd_3 = (byte) data2;
		cmd_4 = (byte) data3;

		ser.packU_INT8((byte) code);
		ser.packU_INT8((byte) data);
		ser.packU_INT8((byte) data2);
		ser.packU_INT8((byte) data3);

	}

}
