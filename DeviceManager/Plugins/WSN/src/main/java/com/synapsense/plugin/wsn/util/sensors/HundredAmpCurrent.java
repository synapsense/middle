package com.synapsense.plugin.wsn.util.sensors;

import com.synapsense.plugin.wsn.util.SensorStates;

public class HundredAmpCurrent implements SensorConversionInterface {

	public double convertFromRaw(char raw) {
		// voltage is measured in 12bits ADCCounts
		// 0 -> 0V ; 4095 -> 2.5V
		double voltage = ((double) raw / 4095) * 2.5;

		// 0.4V -> 0 Amps
		// 2.0V -> 100 Amps

		if (voltage < 0.2)
			return SensorStates.SENSOR_DISCONNECT; // to indicate that sensor is
			                                       // disconnected

		double count = (voltage - 0.4) * 62.5;

		return (count > 0) ? count : 0;
	}

	public double convertFromRawCompensated(char raw, double other) {
		return convertFromRaw(raw);

	}

	public int getMoteDataType() {
		return 8;
	}

}
