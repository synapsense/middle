package com.synapsense.plugin.wsn.util.serializer;

public interface Serializer {

	public int unpackU_INT8();

	public void packU_INT8(byte b);

	public int unpack_INT8();

	public char unpackU_INT16();

	public void packU_INT16(char d);

	public long unpackU_INT32();

	public void packU_INT32(long l);

	public long unpack_INT64();

	public void pack_INT64(long l);

	public void setData(byte[] data);

	/**
	 * Copy data into the serialized packet in "raw mode"
	 * 
	 * @param data
	 */
	public void copyData(byte[] data);

	/**
	 * Get all of the data currently serialized
	 * 
	 * @return
	 */
	public byte[] getData();

	/**
	 * Return len number of raw bytes from stream
	 * 
	 * @param len
	 * @return
	 */
	public byte[] unpack_bytes(int len);

	/**
	 * Get data not yet deserialized
	 */
	public byte[] unpack_remaining_bytes();

	public int getPosition();

	public void setPosition(int position);

	public boolean hasBytes();
}
