package com.synapsense.plugin.wsn.msgbus;

import java.util.ArrayList;

public class MsgRouteInformation extends Message {
	public ArrayList<Character> childid;
	public ArrayList<Character> parentid;

	public String toString() {
		StringBuffer buf = new StringBuffer();
		for (int i = 0; i < childid.size(); i++) {
			buf.append("\n" + Integer.toString(childid.get(i)) + " -> " + Integer.toString(parentid.get(i)));
		}
		return buf.toString();
	}
}
