package com.synapsense.plugin.wsn.softbase29.packet;

import com.synapsense.plugin.wsn.msgbus.Message;
import com.synapsense.plugin.wsn.util.serializer.LittleSerializer;
import com.synapsense.plugin.wsn.util.serializer.Serializer;

public abstract class OSSPacket extends Message {

	public static final int MAC_TYPE_INDEX = 10;

	public static final int PACKET_VERSION = 5;

	public static final byte MAC_BEACON = 1;
	public static final byte MAC_DATA = 2;
	public static final byte MAC_TIME = 3;
	public static final byte MAC_TIMEBEACON = 4;
	public static final byte MAC_TIMEREQBEACON = 6;
	public static final byte MAC_RETIMEBEACON = 7;

	public static final int PHY_HEADER_SIZE = 1;
	public static final int MAC_HEADER_SIZE = 10;
	// Header
	public int phy_length;
	public byte mac_fcfhi;
	public byte mac_fcflo;
	public byte mac_dsn;
	public char mac_destpan;
	public char mac_destaddr;
	public char mac_srcaddr;
	public byte pkversion;
	public byte mac_type;

	// Footer
	public byte[] mac_mic;
	public byte mac_crc;
	public byte mac_ack;
	public int phy_rssi;
	public int phy_lqi;

	public OSSPacket() {
	}

	public OSSPacket(OSSPacket pk) {
		this.phy_length = pk.phy_length;
		this.mac_fcfhi = pk.mac_fcfhi;
		this.mac_fcflo = pk.mac_fcflo;
		this.mac_dsn = pk.mac_dsn;
		this.mac_destpan = pk.mac_destpan;
		this.mac_destaddr = pk.mac_destaddr;
		this.mac_srcaddr = pk.mac_srcaddr;
		this.pkversion = pk.pkversion;
		this.mac_type = pk.mac_type;
		if (pk.mac_mic == null)
			this.mac_mic = null;
		else {
			this.mac_mic = new byte[pk.mac_mic.length];
			System.arraycopy(pk.mac_mic, 0, this.mac_mic, 0, mac_mic.length);
		}
		this.mac_crc = pk.mac_crc;
		this.mac_ack = pk.mac_ack;
		this.phy_rssi = pk.phy_rssi;
		this.phy_lqi = pk.phy_lqi;
	}

	public static OSSPacket deserialize(byte[] message) {
		Serializer serial = new LittleSerializer(message);
		OSSPacket pkt = null;
		serial.setPosition(MAC_TYPE_INDEX);
		int smotavertype = serial.unpackU_INT8();
		byte pkversion = (byte) ((smotavertype & 0x78) >> 3);
		if (pkversion != PACKET_VERSION)
			return null;
		byte mac_type = (byte) (smotavertype & 0x7);
		switch (mac_type) {
		case MAC_DATA:
			pkt = new OSSDataPacket();
			break;
		case MAC_TIMEBEACON:
		case MAC_BEACON:
		case MAC_TIMEREQBEACON:
		case MAC_RETIMEBEACON:
			pkt = new OSSBeaconPacket();
			break;
		default:
			return null;
		}
		serial.setPosition(0);
		pkt.phy_length = (byte) serial.unpackU_INT8();
		pkt.mac_fcfhi = (byte) serial.unpackU_INT8();
		pkt.mac_fcflo = (byte) serial.unpackU_INT8();
		pkt.mac_dsn = (byte) serial.unpackU_INT8();
		pkt.mac_destpan = serial.unpackU_INT16();
		pkt.mac_destaddr = serial.unpackU_INT16();
		pkt.mac_srcaddr = serial.unpackU_INT16();
		serial.unpackU_INT8(); // read vertype
		pkt.pkversion = pkversion;
		pkt.mac_type = mac_type;
		pkt.deserializeDataWithSerializer(serial);
		pkt.mac_mic = new byte[4];
		pkt.mac_mic[0] = (byte) serial.unpackU_INT8();
		pkt.mac_mic[1] = (byte) serial.unpackU_INT8();
		pkt.mac_mic[2] = (byte) serial.unpackU_INT8();
		pkt.mac_mic[3] = (byte) serial.unpackU_INT8();
		pkt.mac_crc = (byte) serial.unpackU_INT8();
		pkt.mac_ack = (byte) serial.unpackU_INT8();
		pkt.phy_rssi = (byte) serial.unpackU_INT8();
		pkt.phy_lqi = (byte) serial.unpackU_INT8();
		return pkt;
	}

	public abstract byte[] serialize();

	public abstract void deserializeDataWithSerializer(Serializer serial);
}
