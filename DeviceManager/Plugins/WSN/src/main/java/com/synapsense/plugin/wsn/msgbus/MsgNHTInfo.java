package com.synapsense.plugin.wsn.msgbus;

public class MsgNHTInfo extends Message {
	public static final int MAX_NHTINFOENTRY = 10;
	public static final int RSSI_COUNT = 4;
	public char deviceId;
	public int numneigh;
	public long report_timestamp;
	public char[] nids;
	public int[][] rssi;
	public int[] state;
	public int[] hop;
	public int[] hoptm;

	public MsgNHTInfo() {

		nids = new char[MAX_NHTINFOENTRY];
		rssi = new int[MAX_NHTINFOENTRY][RSSI_COUNT];
		state = new int[MAX_NHTINFOENTRY];
		hop = new int[MAX_NHTINFOENTRY];
		hoptm = new int[MAX_NHTINFOENTRY];
	}

	public String toString() {
		StringBuffer buf = new StringBuffer();
		buf.append("\nNHT Info of " + Integer.toHexString(deviceId) + "\n");
		for (int i = 0; i < numneigh; i++) {
			buf.append(Integer.toHexString(nids[i]) + ":" + rssi[i][0] + " " + rssi[i][1] + " " + rssi[i][2] + " "
			        + rssi[i][3]);
			buf.append(" " + state[i] + " " + hop[i] + " " + hoptm[i] + "\n");
		}
		return buf.toString();
	}
}
