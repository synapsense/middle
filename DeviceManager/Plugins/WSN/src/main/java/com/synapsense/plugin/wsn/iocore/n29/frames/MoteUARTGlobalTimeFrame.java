package com.synapsense.plugin.wsn.iocore.n29.frames;

import com.synapsense.plugin.wsn.iocore.frames.MoteUARTBadFrame;
import com.synapsense.plugin.wsn.iocore.frames.MoteUARTFrame;
import com.synapsense.plugin.wsn.util.serializer.BigSerializer;

public class MoteUARTGlobalTimeFrame extends MoteUARTFrame {

	public long globaltime;

	public MoteUARTGlobalTimeFrame(byte[] header, byte[] data, String portName) throws MoteUARTBadFrame {
		super(header, portName);
		this.data = data;
		BigSerializer serial = new BigSerializer(data);
		globaltime = serial.unpackU_INT32();
	}
}
