package com.synapsense.plugin.wsn.p3;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.synapsense.plugin.messages.DuoDataType;
import com.synapsense.plugin.wsn.util.serializer.BigSerializer;
import com.synapsense.plugin.wsn.util.serializer.Serializer;

public class DuoData {

	private Map<DuoRpdu, Map<DuoDataType, Double>> values = new HashMap<DuoRpdu, Map<DuoDataType, Double>>();

	private static Logger log = Logger.getLogger(DuoData.class);

	public Map<DuoDataType, Double> getAllValuesFor(DuoRpdu rpdu) {
		return values.get(rpdu);
	}

	public DuoData(DuoConfiguration config, byte[] data) throws P3ConfigMismatchException {
		log.trace("DuoData: have " + data.length + " bytes");

		Serializer ser = new BigSerializer(data);

		while (ser.hasBytes()) {
			int atRpdu = ser.unpack_INT8();
			int entries = ser.unpackU_INT8();
			log.trace("DuoData: RPDU " + atRpdu + " has " + entries + " entries.");
			Map<DuoDataType, Double> v = new HashMap<DuoDataType, Double>();
			DuoRpdu rpdu = null;
			try {
				rpdu = config.getRpdus().get(atRpdu);
				if (rpdu == null) {
					log.warn("RPDU config not recognized or stale, re-asking for configuration");
					throw new P3ConfigMismatchException("RPDU config confusion");
				}
			} catch (Exception e) {
				log.warn("RPDU doesn't exist when there is data, re-asking for configuration");
				throw new P3ConfigMismatchException("RPDU doesn't exist");
			}
			log.trace("DuoData: RPDU is " + rpdu);
			values.put(rpdu, v);

			Map<Integer, Integer> powerEntries = new HashMap<Integer, Integer>();

			while (entries > 0) {
				int tag = ser.unpackU_INT8();
				int val = ser.unpackU_INT16();

				// special handling for power
				if (tag >= 0x16 || (tag >= 0x0A && tag <= 0x0c)) {
					powerEntries.put(tag, val);
				} else {
					double value = (double) val / 10.0;
					log.trace("DuoData: Unpacked tag " + tag + " value " + value);
					v.put(DuoDataType.fromType(tag), value);
				}
				entries--;
			}

			// handle power entries Duo 1.0.3 and older sends just kW data
			// Duo 1.0.4 and newer send same kW and new W data. Ignore kW and
			// substitute the W, scaled as kW but with better resolution
			for (Map.Entry<Integer, Integer> e : powerEntries.entrySet()) {
				int tag = e.getKey();
				int altTag = tag;
				double value;
				switch (tag) {
				case 0x16:
					if (powerEntries.containsKey(0x1A))
						altTag = 0x1A;
					break;
				case 0x0a:
					if (powerEntries.containsKey(0x17))
						altTag = 0x17;
					break;
				case 0x0b:
					if (powerEntries.containsKey(0x18))
						altTag = 0x18;
					break;
				case 0x0c:
					if (powerEntries.containsKey(0x19))
						altTag = 0x19;
					break;
				default: // 0x17, 0x18, 0x19, 0x1A:
				}

				if (tag == altTag) {
					value = e.getValue() / 10.0;
				} else {
					value = powerEntries.get(altTag) / 10000.0;
				}

				if (DuoDataType.fromType(tag) != null) {
					log.trace("DuoData: Unpacked tag " + tag + " value " + value);
					v.put(DuoDataType.fromType(tag), value);
				} else {
					log.trace("DuoData: Unpacked tag " + tag + " value " + value + "*");
				}

			}

		}
		log.trace("DuoData: Done unpacking data");

	}

}
