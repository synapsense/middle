package com.synapsense.plugin.wsn.util.sensors;

public class Generic010 implements SensorConversionInterface, RangeConversionInterface {

	private double min = 0;
	private double max = 100;

	public double convertFromRaw(char raw) {
		// voltage is measured in 12bits ADCCounts
		// 0 -> 0V ; 4095 -> 2.048V
		double voltage = ((double) raw / 4095) * 2.048;

		double count = voltage * ((max - min) / (10.0 - 0.0));
		count = count + min; // +b term
		return (count > 0) ? count : 0;
	}

	public double convertFromRawCompensated(char raw, double other) {
		return convertFromRaw(raw);

	}

	public int getMoteDataType() {

		return 37;
	}

	public void setDataRanges(double m, double x) {
		min = m;
		max = x;
	}

}
