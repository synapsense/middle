package com.synapsense.plugin.wsn.iocore;

import java.io.IOException;
import java.io.OutputStream;
import java.util.concurrent.BlockingQueue;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.plugin.wsn.iocore.frames.out.MoteUARTOutFrame;
import com.synapsense.plugin.wsn.iocore.secure.AESCFBEngine;

public class SecureMoteCommWriter extends MoteCommWriter {
	private Log log = LogFactory.getLog(SecureMoteCommWriter.class);
	private OutputStream outputStream;
	private final AESCFBEngine aesengine;

	@Override
	protected void write(MoteUARTOutFrame fout) throws IOException {
		byte[] data = fout.getFullFrame();
		aesengine.encrypt(data);
		outputStream.write(data);
		outputStream.flush();
		return;
	}

	public SecureMoteCommWriter(BlockingQueue<MoteUARTOutFrame> _cq, OutputStream o, IRestartable mc, byte[] iv,
	        byte[] key) {
		super(_cq, o, mc);
		byte[] curIv;
		curIv = iv.clone();
		outputStream = o;

		aesengine = new AESCFBEngine(key, iv);

		try {
			log.info("Writing out IV...");
			outputStream.write(curIv);
			outputStream.flush();
			log.info("IV sent");
		} catch (IOException e) {
			log.error("Cannot write IV", e);
			System.exit(-1);
		}
	}

}
