package com.synapsense.plugin.wsn.iocore.n29.frames.out;

import com.synapsense.plugin.wsn.iocore.frames.out.MoteUARTOutFrame;
import com.synapsense.plugin.wsn.util.serializer.BigSerializer;

//import com.synapsense.util.Serializer;

public class MoteUARTOutStartFrame extends MoteUARTOutFrame {

	public MoteUARTOutStartFrame(int panId) {
		super((char) MoteUARTOutFrame.TYPE_STARTFRAME, (char) 2);
		BigSerializer ser = new BigSerializer(frame, 4);
		ser.packU_INT16((char) panId);
	}
}
