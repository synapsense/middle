package com.synapsense.plugin.wsn.util.sensors;

public class BogoPower implements SensorConversionInterface {

	private double max = 0.5;

	public double convertFromRaw(char raw) {

		// 1 tick = 0.5KWh

		return ((double) raw * (max));
	}

	public double convertFromRawCompensated(char raw, double other) {
		return convertFromRaw(raw);

	}

	public int getMoteDataType() {

		return 20;
	}

}
