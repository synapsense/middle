package com.synapsense.plugin.wsn.storers;

import org.apache.log4j.Logger;

import com.synapsense.plugin.messages.WSNRoutingBuilder;
import com.synapsense.plugin.wsn.SynapStore;
import com.synapsense.plugin.wsn.WsnConstants;
import com.synapsense.plugin.wsn.configuration.Configuration;
import com.synapsense.plugin.wsn.configuration.WSNNode;
import com.synapsense.plugin.wsn.msgbus.MsgRouteInformation;
import com.synapsense.plugin.wsn.util.SensorNetwork;

public class StoreRoutingInfo {
	private static Logger log = Logger.getLogger(StoreRoutingInfo.class);

	public static void storeRoutingInfo(MsgRouteInformation message, SensorNetwork net) throws Exception {
		int networkId = net.getNetworkId();
		MsgRouteInformation RouteMsg = message;

		for (int i = 0; i < RouteMsg.childid.size(); i++) {
			log.trace("Storing route message: parent ID: " + Long.toHexString(RouteMsg.parentid.get(i))
			        + ", child ID: " + Long.toHexString(RouteMsg.childid.get(i)));
			WSNNode parentNode = Configuration.getNode(net.getNetworkId(), RouteMsg.parentid.get(i));
			WSNNode childNode = Configuration.getNode(net.getNetworkId(), RouteMsg.childid.get(i));
			if (parentNode == null) {
				log.warn("Route message cannot be sent. Parent node (logical ID:" + (int) RouteMsg.parentid.get(i)
				        + ") is not registered");
				return;
			}
			if (childNode == null) {
				log.warn("Route message cannot be sent. Child node (logical ID:" + (int) RouteMsg.childid.get(i)
				        + ") is not registered");
				return;
			}
			SynapStore.transport.sendData(parentNode.getId(), WsnConstants.NETWORK_ROUTE, new WSNRoutingBuilder(
			        parentNode.getPhysID(), childNode.getPhysID(), networkId).getMessage());
		}
	}
}
