package com.synapsense.plugin.wsn.util.sensors;

/**
 * Stab at a sensor object factory
 * @author Yann
 *
 */

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class SensorFactory {

	private static Log log = LogFactory.getLog(SensorFactory.class);
	private static Map<Integer, Class<? extends SensorConversionInterface>> factoryMap;

	public static void initFactory() {
		if (factoryMap == null) {
			factoryMap = new HashMap<Integer, Class<? extends SensorConversionInterface>>();
		}
		SensorFactory.register(1, com.synapsense.plugin.wsn.util.sensors.SHT11TemperatureSensor.class);
		SensorFactory.register(2, com.synapsense.plugin.wsn.util.sensors.SHT11HumiditySensor.class);
		SensorFactory.register(5, com.synapsense.plugin.wsn.util.sensors.ThreeVoltBatteryTwoFiveReference.class);
		SensorFactory.register(6, com.synapsense.plugin.wsn.util.sensors.LighthouseParticle.class);
		// SensorFactory.register(7,
		// com.synapsense.util.sensors.ExternalTempProbe.class);
		SensorFactory.register(8, com.synapsense.plugin.wsn.util.sensors.HundredAmpCurrent.class);
		SensorFactory.register(9, com.synapsense.plugin.wsn.util.sensors.DoorOpenCloseSensor.class);
		SensorFactory.register(10, com.synapsense.plugin.wsn.util.sensors.AirFlowSensor.class);
		SensorFactory.register(12, com.synapsense.plugin.wsn.util.sensors.LiquidLevelSensor.class);
		// SensorFactory.register(13,
		// com.synapsense.util.sensors.ExternalThermistor.class);
		// SensorFactory.register(14,
		// com.synapsense.util.sensors.TenAmpCurrent.class); // DEPRECATED
		// SensorFactory.register(15,
		// com.synapsense.util.sensors.TwentyAmpCurrent.class);
		// SensorFactory.register(16,
		// com.synapsense.util.sensors.FiftyAmpCurrent.class);
		SensorFactory.register(17, com.synapsense.plugin.wsn.util.sensors.Sid2TenAmpCurrent.class);
		SensorFactory.register(18, com.synapsense.plugin.wsn.util.sensors.Sid2TwentyAmpCurrent.class);
		SensorFactory.register(19, com.synapsense.plugin.wsn.util.sensors.Sid2FiftyAmpCurrent.class);
		SensorFactory.register(20, com.synapsense.plugin.wsn.util.sensors.BogoPower.class);
		SensorFactory.register(21, com.synapsense.plugin.wsn.util.sensors.Sid2ExternalThermistor.class);
		SensorFactory.register(22, com.synapsense.plugin.wsn.util.sensors.CcRtd.class);
		SensorFactory.register(23, com.synapsense.plugin.wsn.util.sensors.C250AmpCurrent.class);
		SensorFactory.register(24, com.synapsense.plugin.wsn.util.sensors.AirPressure.class);
		SensorFactory.register(25, com.synapsense.plugin.wsn.util.sensors.FusionThermistor.class);
		SensorFactory.register(26, com.synapsense.plugin.wsn.util.sensors.C200AmpCurrent.class);
		SensorFactory.register(27, com.synapsense.plugin.wsn.util.sensors.ThermanodeThermistor.class);
		SensorFactory.register(28, com.synapsense.plugin.wsn.util.sensors.WaterFlow.class);
		SensorFactory.register(29, com.synapsense.plugin.wsn.util.sensors.SDP610.class);
		SensorFactory.register(30, com.synapsense.plugin.wsn.util.sensors.Cons200AmpCurrent.class);
		SensorFactory.register(31, com.synapsense.plugin.wsn.util.sensors.ConsWaterFlow.class);
		SensorFactory.register(32, com.synapsense.plugin.wsn.util.sensors.WaterEnergy.class);
		SensorFactory.register(33, com.synapsense.plugin.wsn.util.sensors.FlexRTD.class);
		SensorFactory.register(34, com.synapsense.plugin.wsn.util.sensors.Generic420.class);
		SensorFactory.register(35, com.synapsense.plugin.wsn.util.sensors.GenericContactor.class);
		SensorFactory.register(36, com.synapsense.plugin.wsn.util.sensors.Thermanode2Thermistor.class);
		SensorFactory.register(37, com.synapsense.plugin.wsn.util.sensors.Generic010.class);
		SensorFactory.register(38, com.synapsense.plugin.wsn.util.sensors.ActuatorPosition.class);

		SensorFactory.register(40, com.synapsense.plugin.wsn.util.sensors.Halley.class);
		SensorFactory.register(41, HoneywellTemp.class);
		SensorFactory.register(42, HoneywellHumid.class);
	}

	public static int register(int sensor, Class<? extends SensorConversionInterface> classRef) {
		log.info("WSN: Registered sensor type " + Integer.toString(sensor) + " for class " + classRef.toString());
		Integer i = new Integer(sensor);
		factoryMap.put(i, classRef);
		return 0;

	}

	public static RangeConversionInterface getRangeConversion(SensorConversionInterface sci) {
		try {
			return (RangeConversionInterface) sci;
		} catch (ClassCastException e) {
			// Do nothing, just return null...
		}
		return null;
	}

	public static DeltaInterface getDeltaConversion(SensorConversionInterface sci) {
		try {
			return (DeltaInterface) sci;
		} catch (Exception e) {
			// Do nothing, just return null...
		}
		return null;

	}

	public static SensorConversionInterface createFromId(int sensor) {
		SensorConversionInterface interf;
		Integer sensorI = new Integer(sensor);
		Class<? extends SensorConversionInterface> classRef = factoryMap.get(sensorI);
		if (classRef == null) {
			log.error("No map entry for sensor type " + sensor);
			return null;
		}

		try {
			interf = classRef.newInstance();
			return interf;
		} catch (Exception e) {
			log.error("Cannot instantiate sensor class for id=" + sensor, e);
			return null;
		}

	}

}
