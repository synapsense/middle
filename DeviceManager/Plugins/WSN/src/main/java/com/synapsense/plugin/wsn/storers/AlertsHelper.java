package com.synapsense.plugin.wsn.storers;

import com.synapsense.plugin.messages.RefinedAlertsBuilder;
import com.synapsense.plugin.wsn.SynapStore;

public class AlertsHelper {
	private final static String ALERT_TYPENAME = "WSN_CONFIGURATION";
	private final static String LOCALIZE_PATTERN = "$localize(%1$s%2$s)$";
	public final static String ALERT_PROP = "alert";

	public static void sendAlert(String objectID, String alertMessage, String... args) {
		SynapStore.transport.sendData(objectID, ALERT_PROP, new RefinedAlertsBuilder(ALERT_TYPENAME, ALERT_TYPENAME,
		        getLocalizedString(alertMessage, args)).getMessage());
	}

	private static String getLocalizedString(String id, String... args) {

		StringBuilder sb = new StringBuilder();
		for (String s : args) {
			sb.append(",").append("\"" + escapeQuotes(s) + "\"");
		}

		return String.format(LOCALIZE_PATTERN, id, sb.toString());
	}

	private static String escapeQuotes(String str) {
		return str.replaceAll("\"", "\\\"");
	}
}
