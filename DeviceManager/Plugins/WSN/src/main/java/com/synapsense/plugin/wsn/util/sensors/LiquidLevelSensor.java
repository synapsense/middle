package com.synapsense.plugin.wsn.util.sensors;

public class LiquidLevelSensor implements SensorConversionInterface {

	static final int LIQUID_DETECTION_THRESHOLD = 100;

	public double convertFromRaw(char raw) {

		// 4095 indicates liquid is present

		return (raw > LIQUID_DETECTION_THRESHOLD) ? 1 : 0;
	}

	public double convertFromRawCompensated(char raw, double other) {
		return convertFromRaw(raw);

	}

	public int getMoteDataType() {

		return 12;
	}
}
