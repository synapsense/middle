package com.synapsense.plugin.wsn.softbase29.service;

public class OSSServices {

	// / Owner: Various
	// / Description: place holder for lptimesync service
	public static final byte OSS_SERVICE_LPTIMESYNC = (byte) 1;

	// / Owner: Various
	// / Description: Sensor data service
	public static final byte OSS_SERVICE_SENSING = (byte) 15;

	// Wireless control protocol
	public static final byte OSS_SERVICE_WCONTROL = (byte) 16;

	// / Owner: Yann
	// / Description: P3 Products configuration and data
	public static final byte OSS_SERVICE_P3 = (byte) 51;

	// / Owner: Yann
	// / Description: MODBUS Trunking
	public static final byte OSS_SERVICE_MODBUS = (byte) 53;

	// / Owner: Ritu
	// / Description: Protocol to initialize the network
	public static final byte OSS_SERVICE_INITIALIZATION = (byte) 200;

	// / Owner: Yann
	// / Description: Node control service (reboot, health state, etc)
	public static final byte OSS_SERVICE_NODE_CONTROL = (byte) 201;

	// / Owner: Various
	// / Description: Network statistic service
	public static final byte OSS_SERVICE_NWKSTAT = (byte) 202;

	// / Owner: Various
	// / Desciprtion: Network Service
	public static final byte OSS_SERVICE_NETWORK = (byte) 203;

	// / Owner: Yann
	// / Description: Service to distribute code images
	public static final byte OSS_SERVICE_CODE_DISTRIBUTION = (byte) 210;

	// / Owner: Yann
	// / Description: Diagnostic node service, for returning diagnostic messages
	public static final byte OSS_SERVICE_DIAGNOSTICS = (byte) 211;

	// / Owner: Paul
	// / Description: Service for SMOTA2
	public static final byte OSS_SERVICE_SMOTA2 = (byte) 212;

	// / Owner: Paul
	// / Description: Application specific initialization
	public static final byte OSS_SERVICE_APP_INITIALIZATION = (byte) 213;

	// / Owner: Yann
	// / Description: Cryptographic session overlay
	public static final byte OSS_SERVICE_CSESSION = (byte) 240;

}
