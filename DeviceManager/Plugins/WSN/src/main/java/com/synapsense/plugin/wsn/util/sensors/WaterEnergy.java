package com.synapsense.plugin.wsn.util.sensors;

public class WaterEnergy implements SensorConversionInterface, DeltaInterface {

	private double samplingRate = 60 * 5;
	private final double fifteenSampling = 60 * 60; // Divide out the hour

	public double convertFromRaw(char raw) {

		// 1 tick = 1 MegaJoule
		double kwh = ((double) raw) * 0.277777778;
		kwh *= (fifteenSampling / samplingRate); /* 15 min demand power */
		return kwh;
	}

	public double convertFromRawCompensated(char raw, double other) {
		return convertFromRaw(raw);

	}

	public int getMoteDataType() {
		return 32;
	}

	public void setDelta(int d) {

	}

	public void setSamplingRate(int rate) {
		samplingRate = rate;
	}
}
