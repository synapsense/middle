package com.synapsense.plugin.wsn.storers;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;

import com.synapsense.plugin.messages.WSNActuatorPositionBuilder;
import com.synapsense.plugin.messages.WSNSensorDataBuilder;
import com.synapsense.plugin.wsn.SynapStore;
import com.synapsense.plugin.wsn.WsnConstants;
import com.synapsense.plugin.wsn.configuration.ActuatorNotFoundException;
import com.synapsense.plugin.wsn.configuration.Configuration;
import com.synapsense.plugin.wsn.configuration.SensorNotFoundException;
import com.synapsense.plugin.wsn.configuration.WSNActuator;
import com.synapsense.plugin.wsn.configuration.WSNNode;
import com.synapsense.plugin.wsn.configuration.WSNSensor;
import com.synapsense.plugin.wsn.msgbus.Message;
import com.synapsense.plugin.wsn.msgbus.MsgSensorData;
import com.synapsense.plugin.wsn.util.DeltaEncoder;
import com.synapsense.plugin.wsn.util.SensorNetwork;
import com.synapsense.plugin.wsn.util.sensors.DeltaInterface;
import com.synapsense.plugin.wsn.util.sensors.RangeConversionInterface;
import com.synapsense.plugin.wsn.util.sensors.SensorConversionInterface;
import com.synapsense.plugin.wsn.util.sensors.SensorFactory;
import com.synapsense.plugin.wsn.util.timesync.TimeSync;
import com.synapsense.plugin.wsn.util.timesync.TimeSyncTime;

public class StoreSensor {
	private static Logger log = Logger.getLogger(StoreSensor.class);

	private static final SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy HH:mm:ss,SSS");
	private static final long BACKDATECUTOFF = 30 * 60 * 1000;

	public static void store(Message message, SensorNetwork net, TimeSync times) throws Exception {

		MsgSensorData sensor = (MsgSensorData) message;

		// Substitute node "global" timestamp with actual system global
		// timestamp
		TimeSyncTime timestamp = times.convertTime(sensor.datatimestamp, times.systemTime());
		if (!timestamp.isDidConvert()) {
			log.warn("Message from sensor " + sensor.sensor + " of 0x" + Long.toHexString(sensor.device)
			        + " doesn't have a timestamp and will be discarded.");
			return;
		}
		if (timestamp.getConvertedTime() < times.systemTime() - BACKDATECUTOFF) {
			log.warn("Message from sensor " + sensor.sensor + " of 0x" + Long.toHexString(sensor.device)
					+ " is too old and will be discarded.");
			return;
		}
		sensor.datatimestamp = timestamp.getConvertedTime();

		WSNNode node = Configuration.getNode(net.getNetworkId(), sensor.device);
		if (node == null) {
			for (WSNNode wrongLogicalIDNode : Configuration.getNodes()) {
				if (wrongLogicalIDNode.getLogicalID() == sensor.device) {
					AlertsHelper.sendAlert(wrongLogicalIDNode.getId(), "alert_message_probably_wrong_configured_node",
					        Integer.toString(net.getNetworkId()), Integer.toString(wrongLogicalIDNode.getNetworkID()));
				}
			}
			StringBuilder bld = new StringBuilder();
			bld.append("Node (logical id: 0x");
			bld.append(Long.toHexString(sensor.device));
			bld.append(") is not registered.Skip data: type: ");
			bld.append(sensor.sensor);
			bld.append(" channel: ");
			bld.append(sensor.channel);
			log.warn(bld.toString());
			return;
		}
		StoreSensorData(sensor, node);

	}

	private static void StoreSensorData(MsgSensorData sensor, WSNNode node) {

		double dataReading = 0.0;

		SensorConversionInterface tempSensor = SensorFactory.createFromId(sensor.sensor);

		if (tempSensor == null) {
			log.error("Couldn't instantiate converter for type " + Integer.toString(sensor.sensor));
			return;
		}

		WSNSensor wsensor = null;

		try {
			wsensor = node.getSensor(sensor.channel);
		} catch (SensorNotFoundException e) {
			AlertsHelper.sendAlert(node.getId(), "alert_message_unknown_sensor_channel",
			        Integer.toString(sensor.channel));
			log.error("Can't find the sensor on channel " + sensor.channel + " : ", e);
			return;
		}

		// Try to determine if a class implements RangeConversionInterface
		// If so, call its setDataRanges method
		RangeConversionInterface rc = SensorFactory.getRangeConversion(tempSensor);
		if (rc != null) {
			rc.setDataRanges(wsensor.getScaleMin(), wsensor.getScaleMax());
		}

		DeltaInterface dc = SensorFactory.getDeltaConversion(tempSensor);
		if (dc != null) {
			try {
				int delta = 0;
				String deltaS = wsensor.getOption("delta");

				if (deltaS != null && (!deltaS.equals("")))
					delta = Integer.parseInt(deltaS) / 100;
				else
					delta = DeltaEncoder.encode(node, wsensor);

				dc.setDelta(delta);
				if (wsensor.hasSampling())
					dc.setSamplingRate(wsensor.getSamplingInSec());
				else
					dc.setSamplingRate(node.getSamplingInSeconds());

			} catch (Exception e) {
				log.error("Exception when retrieving delta option from sensor.", e);
			}
		}

		// Call convertFromRawCompensated for certain types of sensors
		switch (sensor.sensor) {
		case 2:
			double lastTemp;
			try {
				lastTemp = node.getSensorLastValue(0); // Channel 0 is special
				                                       // case SHT15
			} catch (SensorNotFoundException e) {
				log.warn("Cannot find sensor for humidity special case", e);
				return;
			}
			log.trace("HUMIDITY_CONVERSION: Trying to do a compensated conversion using last temperature of "
			        + lastTemp + " and sensor reading " + (int) sensor.reading);
			dataReading = tempSensor.convertFromRawCompensated(sensor.reading, lastTemp);
			break;
		default:
			dataReading = tempSensor.convertFromRaw(sensor.reading);
		}

		if (log.isDebugEnabled()) {
			Date date = new Date(sensor.datatimestamp);
			String datatimestring = sdf.format(date).toString();
			log.debug("Sensor " + Integer.toString(sensor.sensor) + " (node physID:"
			        + Long.toHexString(node.getPhysID()) + ") : ch " + Integer.toString(sensor.channel) + " : raw "
			        + Integer.toString(sensor.reading) + " : conv " + Double.toString(dataReading) + " : time "
			        + datatimestring);
		}

		// HACK: Check if this is a On/Off sensor, and if so, invert the values
		if (wsensor.getType() == 100000) {
			if (dataReading > 0)
				dataReading = 0;
			else
				dataReading = 1;
		}

		SynapStore.transport.sendData(
		        wsensor.getId(),
		        WsnConstants.SENSOR_LAST_VALUE,
		        new WSNSensorDataBuilder(node.getPhysID(), sensor.channel, dataReading, sensor.datatimestamp, node
		                .getNetworkID(), node.getPlatformID(), wsensor.getType()).getMessage());

		if (node.getPlatformID() == WSNNode.PLATFORM_ACKBAR) {
			try {
				WSNActuator actuator = node.getActuator(sensor.channel);
				int moving = 0;
				if (sensor.controlresponse == WSNActuator.CONTROLRESPONSE_MOVING)
					moving = 1;
				SynapStore.transport.sendData(actuator.getId(), WsnConstants.ACTUATOR_POSITION,
				        new WSNActuatorPositionBuilder(dataReading, moving, sensor.devicestate, sensor.datatimestamp)
				                .getMessage());
			} catch (ActuatorNotFoundException e) {
				log.error("Can't find the actuator on channel " + sensor.channel + " : ", e);
			}
		}

		try {
			node.setSensorLastValue(sensor.channel, dataReading);
		} catch (SensorNotFoundException e) {

		}
	}
}
