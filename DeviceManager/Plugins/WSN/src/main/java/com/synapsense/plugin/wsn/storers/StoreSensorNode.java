package com.synapsense.plugin.wsn.storers;

import java.util.Map;

import org.apache.log4j.Logger;

import com.synapsense.plugin.messages.WSNNodeVersionBuilder;
import com.synapsense.plugin.wsn.NetworkWorker;
import com.synapsense.plugin.wsn.SynapStore;
import com.synapsense.plugin.wsn.WsnConstants;
import com.synapsense.plugin.wsn.configuration.Configuration;
import com.synapsense.plugin.wsn.configuration.SensorNotFoundException;
import com.synapsense.plugin.wsn.configuration.WSNNode;
import com.synapsense.plugin.wsn.iocore.n29.frames.out.MoteUARTOutNodeIDFrame;
import com.synapsense.plugin.wsn.msgbus.Message;
import com.synapsense.plugin.wsn.msgbus.MsgNodeInfo;
import com.synapsense.plugin.wsn.secmgr.AllowedParticipants;
import com.synapsense.plugin.wsn.softbase29.GateWay;
import com.synapsense.plugin.wsn.util.DeltaEncoder;
import com.synapsense.plugin.wsn.util.SensorNetwork;

public class StoreSensorNode {
	private static Logger log = Logger.getLogger(StoreSensorNode.class);
	static double lastTemp = 0;

	private static boolean checkNodeNet(WSNNode node, SensorNetwork net) {
		if (node == null || net == null)
			return false;
		if (node.getNetworkID() == net.getNetworkId())
			return true;

		AlertsHelper.sendAlert(node.getId(), "alert_message_node_joined_incorrect_network",
		        Long.toHexString(node.getPhysID()), Integer.toString(net.getNetworkId()),
		        Integer.toString(node.getNetworkID()));
		return false;
	}

	public static void store(Message message, SensorNetwork net) throws Exception {

		if (net != null) {

			int networkId = net.getNetworkId();
			MsgNodeInfo sensorNodeInfoMsg = (MsgNodeInfo) message;
			long deviceId = sensorNodeInfoMsg.device;

			WSNNode node = Configuration.getNodeByPhysID(deviceId);

			// N29 only behavior - reject nodes that were not registered
			if (sensorNodeInfoMsg.gateway != null && (node == null || node.getNetworkID() != net.getNetworkId())) {
				log.warn("Node (physical id: 0x" + Long.toHexString(deviceId) + ") is not registered. Send reject.");
				// Make temporary WSNNode with logical ID of 0xFFFF
				node = new WSNNode("NOTAREALNODE", networkId, deviceId, 0xFFFF, sensorNodeInfoMsg.platformId, 5 * 60,
				        "");
				sendNodeShortIdAndSampleFrequency(node, sensorNodeInfoMsg.source, sensorNodeInfoMsg.gateway);
				return;
			}

			checkNodeNet(node, net);
			// Send sample frequency parameter to the node
			if (deviceId != 0) {
				log.debug("Storing node information. Node is " + Long.toHexString(deviceId)
				        + " and contains version information (app/nwk/os) "
				        + Integer.toHexString(sensorNodeInfoMsg.appVersion) + "."
				        + Integer.toHexString(sensorNodeInfoMsg.appRevision) + " / "
				        + Integer.toHexString(sensorNodeInfoMsg.nwkVersion) + "."
				        + Integer.toHexString(sensorNodeInfoMsg.nwkRevision) + " / "
				        + Integer.toHexString(sensorNodeInfoMsg.osVersion) + "."
				        + Integer.toHexString(sensorNodeInfoMsg.osRevision));

				SynapStore.transport.sendData(node.getId(), WsnConstants.NETWORK_VERSION, new WSNNodeVersionBuilder(
				        node.getPhysID(), sensorNodeInfoMsg.appVersion, sensorNodeInfoMsg.appRevision,
				        sensorNodeInfoMsg.nwkVersion, sensorNodeInfoMsg.nwkRevision, sensorNodeInfoMsg.osVersion,
				        sensorNodeInfoMsg.osRevision).getMessage());
			}

			sendNodeShortIdAndSampleFrequency(node, sensorNodeInfoMsg.source, sensorNodeInfoMsg.gateway);

		} else {
			log.error("Trying to store on a Null SensorNetwork");
		}
	}

	public static void sendNodeShortIdAndSampleFrequency(WSNNode node, char source, GateWay gateway) {
		// Get the worker
		SensorNetwork sensorNet = Configuration.getSensorNetworkById(node.getNetworkID());
		NetworkWorker worker = SynapStore.workerMap.get(sensorNet);
		boolean secEnable = false;
		if (AllowedParticipants.platformAllowed(node.getPlatformID())) {
			secEnable = true;
		}

		// Preparing and sending network frame
		MoteUARTOutNodeIDFrame frame = new MoteUARTOutNodeIDFrame(node.getPhysID(), (char) node.getLogicalID(),
		        node.getSamplingInSeconds(), source, gateway, secEnable);
		boolean setSubsamples = false;

		for (int i : node.getSensorChannels()) {
			if (i < 3 || i > 9) // Skip builtin and extended channels
				continue;
			try {
				if (log.isDebugEnabled()) {
					Map<String, String> debugMap = node.getSensor(i).getOptions();
					for (String key : debugMap.keySet()) {
						log.debug("Sensor ch " + i + " has option: " + key + " = " + debugMap.get(key));
					}
				}

				int delta = 0;
				try {

					String deltaS = node.getSensor(i).getOption(WsnConstants.SENSOR_DELTA);

					if (deltaS != null && (!deltaS.equals(""))) {
						delta = Integer.parseInt(deltaS) / 100;
						log.debug("Using delta parsed from delta property");
					} else {
						Integer deltaN = DeltaEncoder.encode(node, node.getSensor(i));
						if (deltaN != null)
							delta = deltaN;
						log.debug("Using delta parsed from properties");

					}

				} catch (Exception e) {
					log.error("Can't set delta for sensor channel " + i + " due to missing or misconfigured property.",
					        e);
				}
				if (log.isDebugEnabled())
					log.debug("Sensor " + i + " has delta set to " + delta);
				frame.setDelta(i, delta); // Set to 0 in any failure case

				if (setSubsamples == false) {

					int subsamples = 0;
					try {
						String subsamplesS = node.getSensor(i).getOption(WsnConstants.SENSOR_SUBSAMPLE_INTERVAL);
						if (subsamplesS != null) {
							subsamples = Integer.parseInt(subsamplesS);
							frame.setSubsamples(subsamples);
							setSubsamples = true;
							log.debug("Set subsamples based on channel " + i + " to " + subsamples);
						}
					} catch (Exception e) {
						log.warn("Can't set subsamples for sensor channel " + i
						        + " due to missing or misconfigured property.", e);
					}

				}

			} catch (SensorNotFoundException e) {
				// This is odd
				log.error("This shouldn't happen", e);
			}
		}
		worker.commandQ.add(frame);
	}

}
