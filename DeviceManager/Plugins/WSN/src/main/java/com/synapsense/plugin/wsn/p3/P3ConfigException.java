package com.synapsense.plugin.wsn.p3;

public class P3ConfigException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5199768275530179614L;

	public P3ConfigException(String msg) {
		super(msg);
	}
}