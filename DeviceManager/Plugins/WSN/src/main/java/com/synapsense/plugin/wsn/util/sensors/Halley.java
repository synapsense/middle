package com.synapsense.plugin.wsn.util.sensors;

class Halley implements SensorConversionInterface {

	public double convertFromRaw(char raw) {
		double tempC = (0.00000128 * Math.pow((double) raw, 2)) - (0.046188 * (double) raw) + 136.96;
		return (9.0 / 5.0) * tempC + 32;

	}

	public double convertFromRawCompensated(char raw, double vRef) {
		return convertFromRaw(raw);
	}

	public int getMoteDataType() {
		return 40;
	}

}
