package com.synapsense.plugin.wsn;

import java.io.Serializable;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.synapsense.plugin.api.exceptions.ConfigPluginException;
import com.synapsense.plugin.api.exceptions.IncorrectStateException;
import com.synapsense.plugin.apinew.ObjectDesc;
import com.synapsense.plugin.apinew.Plugin;
import com.synapsense.plugin.apinew.PluginConfig;
import com.synapsense.plugin.apinew.Transport;
import com.synapsense.plugin.wsn.knownprops.KnownPropsStore;

public class WSNPlugin implements Plugin {
	private final static String ID = "wsn";
	private static Properties PLUGIN_DESC = new Properties();
	static {
		PLUGIN_DESC.put(PLUGIN_ID, ID);
		PLUGIN_DESC.put(ROOT_TYPES, "WSNNETWORK");
	}
	private final static Logger logger = Logger.getLogger(WSNPlugin.class);

	@Override
	public void configure(PluginConfig config) throws ConfigPluginException {
		if (config == null)
			throw new IllegalArgumentException("config cannot be null");
		try {
			SynapStore.configure(config);
		} catch (Exception e) {
			logger.error("Unable to configure plug-in [" + ID + "]");
			// TODO change to debug level
			logger.info("Details:", e);
			throw new ConfigPluginException("Cannot configure plug-in", e);
		}
		logger.debug("Plugin " + ID + " is configured.");
	}

	@Override
	public String getID() {
		return ID;
	}

	@Override
	public Properties getPluginDescription() {
		return PLUGIN_DESC;
	}

	@Override
	public void setTransport(Transport transport) {
		SynapStore.transport = transport;
	}

	@Override
	public void start() throws IncorrectStateException {
		SynapStore.tryStart();
		logger.debug("Plugin " + ID + " is started.");
	}

	@Override
	public void stop() {
		logger.warn("Stopping WSN plugin...");
		SynapStore.emptyConfig();
	}

	@Override
	public Serializable getPropertyValue(ObjectDesc object, String propertyName) {
		throw new UnsupportedOperationException("Cannot retrieve properties using pull-mode [" + object.getID() + ", "
		        + propertyName + "]");
	}

	@Override
	public void setPropertyValue(ObjectDesc object, String propertyName, Serializable value) {
		logger.info("Setting the property value [" + object.getID() + ", " + propertyName + "= " + value.toString()
		        + "]");
		try {
			KnownPropsStore.setPropertyValue(object.getID(), propertyName, value);
		} catch (Exception e) {
			logger.warn("Couldn't set a property", e);
		}

	}

	@Override
	public void setPropertyValue(ObjectDesc object, List<String> propertyNames, List<Serializable> values) {
		if (propertyNames == null || values == null || propertyNames.size() != values.size()) {
			throw new IllegalArgumentException("Unable to set properties values.");
		}
		for (int i = 0; i < propertyNames.size(); i++) {
			setPropertyValue(object, propertyNames.get(i), values.get(i));
		}
	}

	@Override
	public List<Serializable> getPropertyValue(ObjectDesc object, List<String> propertyNames) {
		throw new UnsupportedOperationException("Cannot retrieve properties using pull-mode [" + object.getID() + ", "
		        + propertyNames + "]");
	}

}
