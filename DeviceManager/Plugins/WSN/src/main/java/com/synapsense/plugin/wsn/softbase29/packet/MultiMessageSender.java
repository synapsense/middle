package com.synapsense.plugin.wsn.softbase29.packet;

import org.apache.log4j.Logger;

import com.synapsense.plugin.wsn.softbase29.service.P3Receive;

public class MultiMessageSender {
	private byte magic = 0;
	private int mmSize = -1; // total message size, not including 5 bytes
	                         // overhead
	private byte curPos = 0;
	private byte mmcurslot;
	private byte numSlots = -1; // total number of slots (1-numSlots)
	private byte[] out = new byte[MM_PAYLOAD]; // the contents of current slot
	                                           // being worked on

	private static byte mmmagic;
	public static final int MM_PAYLOAD = 60;
	// static final int mm_timeout = 120000; /* 120 seconds = 2 minutes */
	// static final int mm_rx_timeout = 900000; /* 15 min */
	// public static final long MM_TIMEOUT = 1000 * 60 * 10; /* 10 minutes */

	private static Logger log = Logger.getLogger(MultiMessage.class);

	public MultiMessageSender(int totalPayloadSize) {
		log.trace("New MultiMessageSender");

		if (totalPayloadSize > 65535 || totalPayloadSize <= 0) {
			// @TODO handle error
		} else {
			this.mmSize = totalPayloadSize;
			this.mmcurslot = 0;
			this.curPos = 0;
			this.magic = mmmagic;

			if (mmmagic < 126) { // increment for next mm sender
				mmmagic++;
			} else {
				mmmagic = 0; // cycle through again
			}

			/* Compute max slots */
			// Each payload adds 7 bytes of header
			this.numSlots = (byte) ((this.mmSize + (MM_PAYLOAD - 7) - 1) / (MM_PAYLOAD - 7));
			log.trace("MMSender: magic " + this.magic + " has " + this.mmSize + " bytes in " + this.numSlots + " slots");
		}

	}

	/*
	 * @param moreData one byte of data
	 * 
	 * @return empty array or a full mm payload ready to be sent
	 */
	public byte[] add_byte(byte bytetoadd) {

		if (curPos == 0) {
			log.trace("MM Sender Loading slot " + this.mmcurslot);
			out[curPos++] = 0; // 51; // Service = P3
			out[curPos++] = 0; // 51; // Service = P3
			out[curPos++] = 0; // 51; // Service = P3
			out[curPos++] = P3Receive.CMD_NEW_CONFIG; // 0x10; // Config Data
			out[curPos++] = this.mmcurslot;
			out[curPos++] = this.numSlots;
			out[curPos++] = this.magic;
		}
		out[curPos++] = bytetoadd;
		if (curPos == MM_PAYLOAD) {
			byte[] lastFrame = new byte[MM_PAYLOAD];
			log.trace("MM Sender frame slot " + mmcurslot + " is " + lastFrame.length + " bytes");

			for (int i = 0; i < curPos; i++) {
				lastFrame[i] = out[i];
			}
			curPos = 0;
			mmcurslot++;
			// return out;
			return lastFrame;

		} else {
			byte tmpout[] = { 0 };
			return tmpout;
		}
	}

	/*
	 * @param
	 * 
	 * @return returns last MM in sequence
	 */
	public byte[] flush() {
		log.trace("MM Sender flushing " + this.mmcurslot);
		log.trace("MM Sender curPos = " + this.curPos);
		byte[] lastFrame = new byte[curPos];
		log.trace("MM Sender lastframe is " + lastFrame.length + " bytes");

		for (int i = 0; i < curPos; i++) {
			lastFrame[i] = out[i];
		}
		return lastFrame;
	}

	public void mm_show(byte[] d, String header) {
		String msg = header + ": ";
		String msgIndexes = "indeces: ";

		for (int i = 0; i < d.length; i++) {
			msg += " " + d[i];
			msgIndexes += " " + i;
		}

		log.trace(msg);
		// System.out.println(msg2);

	}

	public int getNumSlots() {
		return this.numSlots;
	}

	public int getMmSize() {
		return this.mmSize;
	}

}
