package com.synapsense.plugin.wsn.iocore.n29.frames;

import com.synapsense.plugin.wsn.iocore.frames.MoteUARTBadFrame;
import com.synapsense.plugin.wsn.iocore.frames.MoteUARTFrame;

public class MoteUARTUnitTest extends MoteUARTFrame {

	private String results;

	public MoteUARTUnitTest(byte[] header, byte[] data, String portname) throws MoteUARTBadFrame {
		super(header, portname);
		this.data = data;

		results = new String(data);
	}

	public String toString() {
		return results;
	}
}
