package com.synapsense.plugin.wsn.util.sensors;

import com.synapsense.plugin.wsn.util.SensorStates;

public class ThermanodeThermistor implements SensorConversionInterface {

	public double convertFromRaw(char raw) {
		// voltage is measured in 12bits ADCCounts
		// 0 -> 0V
		// 4095 -> 2.048V
		double rawV = raw & 0xFFF;

		double Vref = 2.048;
		double Vadc = rawV / 4095 * 2.048;

		double beta = 3950;
		double T0 = 298.15;

		try {

			double tempK = (beta)
			        / (Math.log(((10000 * Vref * 1.25) - (10000 * Vadc)) / Vadc) + ((beta / T0) - Math.log(10000)));
			double tempC = tempK - 273.15;
			// System.err.println("tempC: " + tempC);
			// System.err.println(" Pat's calc: " +( -0.36+(0.11*(tempC/5))));
			// tempC += -0.36+(0.11*(tempC/5));
			// System.err.println(" after tempC: " + tempC);
			double tempF = (9.0 / 5.0) * tempC + 32;

			if (tempF <= -10) {
				return SensorStates.SENSOR_DISCONNECT;
			}

			if (tempF > 190) {
				return SensorStates.SENSOR_DISCONNECT;
			}

			return tempF;
		} catch (Exception e) {
			return SensorStates.SENSOR_DISCONNECT;
		}
	}

	public double convertFromRawCompensated(char raw, double vRef) {
		return convertFromRaw(raw);
	}

	public int getMoteDataType() {
		return 27;
	}
}
