package com.synapsense.plugin.wsn.util.sensors;

/**
 * This class implements the conversion on a 12bit ADC from a 3v battery read
 * half based on a 2.5 volt reference. It also switches to a 1.5V reference on
 * demand depending on the high bit
 * 
 * @author Yann
 * 
 */

public class ThreeVoltBatteryTwoFiveReference implements SensorConversionInterface {

	public double convertFromRaw(char raw) {
		final double max_v = 4095; // 12 bit ADC
		double max_x = 1.5; // Defines vRef - reference voltage
		if ((raw >> 15) == 1) { // High bit set = 2.5V reference
			raw = (char) (((int) raw) & ~(0x8000));
			max_x = 2.5;
		}

		double voltage = ((double) raw * max_x) / max_v;
		voltage = voltage * 2;
		return voltage;
	}

	public double convertFromRawCompensated(char raw, double other) {
		return convertFromRaw(raw);
	}

	public int getMoteDataType() {
		return 5;
	}
}
