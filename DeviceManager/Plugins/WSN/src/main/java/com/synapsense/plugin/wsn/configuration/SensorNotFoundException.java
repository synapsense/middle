package com.synapsense.plugin.wsn.configuration;

public class SensorNotFoundException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1317636535443212660L;

	public SensorNotFoundException(String desc) {
		super(desc);
	}

	public SensorNotFoundException(String desc, Throwable cause) {
		super(desc, cause);
	}
}
