package com.synapsense.plugin.wsn.iocore;

import com.synapsense.plugin.wsn.iocore.frames.MoteUARTMsgAckFrame;

public interface IMoteComm {

	public void startReading(boolean thread);

	public void startUpdating(boolean thread);

	/**
	 * Signals that the base mote has received the message and that future
	 * messages should be sent.
	 * 
	 */
	public void recvAck(MoteUARTMsgAckFrame ack);

	public void finish();

	public String getPortName();

	public void stop();

	public void reconnect();
}