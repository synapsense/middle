package com.synapsense.plugin.wsn.softbase29.packet;

import com.synapsense.plugin.wsn.util.serializer.LittleSerializer;
import com.synapsense.plugin.wsn.util.serializer.Serializer;

public class OSSDataPacket extends OSSPacket {
	public static final int NWK_HEADER_SIZE = 9;
	public static final int HEADER_SIZE = PHY_HEADER_SIZE + MAC_HEADER_SIZE + NWK_HEADER_SIZE;
	public static final int OSS_DATA_LENGTH = 100;

	public byte nwk_protocol;
	public char nwk_destaddr;
	public char nwk_srcaddr;
	public byte nwk_datalen;
	public byte nwk_service;
	public byte nwk_seqno;
	public byte nwk_ttl;
	public byte[] data;
	public byte[] path;

	public int Sendcount;

	public OSSDataPacket() {
		Sendcount = 0;
	}

	public OSSDataPacket(int size) {
		data = new byte[size];
		Sendcount = 0;
	}

	public OSSDataPacket(OSSDataPacket pk) {
		super(pk);
		this.nwk_protocol = pk.nwk_protocol;
		this.nwk_destaddr = pk.nwk_destaddr;
		this.nwk_srcaddr = pk.nwk_srcaddr;
		this.nwk_datalen = pk.nwk_datalen;
		this.nwk_service = pk.nwk_service;
		this.nwk_seqno = pk.nwk_seqno;
		this.nwk_ttl = pk.nwk_ttl;
		if (pk.data == null)
			this.data = null;
		else {
			this.data = new byte[pk.data.length];
			System.arraycopy(pk.data, 0, this.data, 0, data.length);
		}
		if (pk.path == null)
			this.path = null;
		else {
			this.path = new byte[pk.path.length];
			System.arraycopy(pk.path, 0, this.path, 0, path.length);
		}
	}

	public byte[] serialize() {
		byte[] retbytes;
		int bytelen = HEADER_SIZE + phy_length;
		if (path != null) {
			bytelen = bytelen + path.length;
		}
		retbytes = new byte[bytelen];
		Serializer serial = new LittleSerializer(retbytes);
		serial.packU_INT8((byte) phy_length);
		serial.packU_INT8(mac_fcfhi);
		serial.packU_INT8(mac_fcflo);
		serial.packU_INT8(mac_dsn);
		serial.packU_INT16(mac_destpan);
		serial.packU_INT16(mac_destaddr);
		serial.packU_INT16(mac_srcaddr);
		serial.packU_INT8((byte) ((pkversion << 4) | mac_type));
		serial.packU_INT8(nwk_protocol);
		serial.packU_INT16(nwk_destaddr);
		serial.packU_INT16(nwk_srcaddr);
		serial.packU_INT8(nwk_datalen);
		serial.packU_INT8(nwk_service);
		serial.packU_INT8(nwk_seqno);
		serial.packU_INT8(nwk_ttl);
		if (data != null)
			System.arraycopy(data, 0, retbytes, 20, data.length);
		if (path != null) {
			for (int i = 0; i < path.length; i++) {
				retbytes[HEADER_SIZE + data.length + i] = path[i];
			}
		}
		return retbytes;
	}

	public void deserializeDataWithSerializer(Serializer serial) {
		this.nwk_protocol = (byte) serial.unpackU_INT8();
		this.nwk_destaddr = serial.unpackU_INT16();
		this.nwk_srcaddr = serial.unpackU_INT16();
		this.nwk_datalen = (byte) serial.unpackU_INT8();
		this.nwk_service = (byte) serial.unpackU_INT8();
		this.nwk_seqno = (byte) serial.unpackU_INT8();
		this.nwk_ttl = (byte) serial.unpackU_INT8();
		this.data = new byte[this.nwk_datalen];
		int count = 0;
		for (int i = 0; i < this.nwk_datalen; i++) {
			this.data[i] = (byte) serial.unpackU_INT8();
			count++;
		}
		int pathlen = this.phy_length - this.nwk_datalen;
		if ((this.nwk_datalen & 0x1) == 0x1) {
			pathlen--;
			serial.unpackU_INT8();
			count++;
		}
		if (pathlen > 0) {
			this.path = new byte[pathlen];
			for (int i = 0; i < pathlen; i++) {
				this.path[i] = (byte) serial.unpackU_INT8();
				count++;
			}
		}
		// Read rest
		int rest = OSS_DATA_LENGTH - count;
		for (int i = 0; i < rest; i++)
			serial.unpackU_INT8();
	}

	public int getProtocol() {
		return (nwk_protocol >> 4) & 0x0F;
	}

	public int getProtocolSubtype() {
		return nwk_protocol & 0x0F;
	}

	public void setProtocol(int protocol) {
		nwk_protocol = (byte) ((nwk_protocol & 0x0F) | (protocol << 4));
	}

	public void setProtocolSubtype(int protocol_subtype) {
		nwk_protocol = (byte) ((nwk_protocol & 0xF0) | protocol_subtype);
	}

	public boolean equals(Object obj) {
		final OSSDataPacket other = (OSSDataPacket) obj;
		if (mac_dsn != other.mac_dsn)
			return false;
		if (nwk_datalen != other.nwk_datalen)
			return false;
		if (nwk_destaddr != other.nwk_destaddr)
			return false;
		if (nwk_protocol != other.nwk_protocol)
			return false;
		if (nwk_seqno != other.nwk_seqno)
			return false;
		if (nwk_service != other.nwk_service)
			return false;
		if (nwk_srcaddr != other.nwk_srcaddr)
			return false;
		for (int i = 0; i < nwk_datalen; i++)
			if (data[i] != other.data[i])
				return false;
		return true;
	}

	public String toString() {
		StringBuffer buf = new StringBuffer();
		buf.append("Prot: " + (int) (nwk_protocol & 0xFF));
		buf.append(" NWK Dest: " + Integer.toHexString(nwk_destaddr & 0xFFFF).toUpperCase());
		buf.append(" NWK Src: " + Integer.toHexString(nwk_srcaddr & 0xFFFF).toUpperCase());
		buf.append(" Len: " + (int) (nwk_datalen & 0xFF));
		buf.append(" Svc: " + (int) (nwk_service & 0xFF));
		buf.append(" Seq: " + (int) (nwk_seqno & 0xFF));
		buf.append(" TTL: " + (int) (nwk_ttl & 0xFF));
		if (data != null) {
			buf.append(" Data:");
			for (int i = 0; i < data.length; i++)
				buf.append(String.format(" %02X", data[i] & 0xFF));
		}
		if (path != null) {
			buf.append(" Path:");
			for (int i = 0; i < path.length; i++)
				buf.append(String.format(" %02X", path[i] & 0xFF));
		}
		return buf.toString();
	}
}
