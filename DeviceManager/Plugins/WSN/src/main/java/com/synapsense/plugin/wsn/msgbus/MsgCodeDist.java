package com.synapsense.plugin.wsn.msgbus;

public class MsgCodeDist extends Message {

	public static final int CD_ACK = 4;
	public static final int CD_CODE = 2;
	public static final int CD_COMMAND = 3;
	public static final int CD_PRESENCE = 1;
	public static final int CD_DIAGSTATUS = 10;

	public int type;
	public char id;

	// ACK
	public long section;
	public int pseq;
	public int state;
	public int reason;

	public MsgCodeDist() {

	}

}
