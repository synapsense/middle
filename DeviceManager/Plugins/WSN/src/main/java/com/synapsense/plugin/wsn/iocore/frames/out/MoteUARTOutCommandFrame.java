package com.synapsense.plugin.wsn.iocore.frames.out;

import com.synapsense.plugin.wsn.util.serializer.BigSerializer;

public class MoteUARTOutCommandFrame extends MoteUARTOutFrame {

	public char destination;
	public int command;
	public long data;

	public MoteUARTOutCommandFrame(char destination, int command, long data) {
		super((char) MoteUARTOutFrame.TYPE_COMMAND, (char) 14);
		BigSerializer ser = new BigSerializer(frame, 4);
		ser.packU_INT16(destination);
		ser.packU_INT16((char) command);
		ser.packU_INT32(data);
		this.destination = destination;
		this.command = command;
		this.data = data;
	}

	public MoteUARTOutCommandFrame(long destination, int command, long data) {
		super((char) MoteUARTOutFrame.TYPE_COMMAND, (char) 14);
		BigSerializer ser = new BigSerializer(frame, 4);
		ser.pack_INT64(destination);
		ser.packU_INT16((char) command);
		ser.packU_INT32(data);

	}

}
