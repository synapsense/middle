package com.synapsense.plugin.wsn.knownprops;

import java.io.Serializable;
import java.util.Map.Entry;

import org.apache.log4j.Logger;

import com.synapsense.plugin.wsn.NetworkWorker;
import com.synapsense.plugin.wsn.SynapStore;
import com.synapsense.plugin.wsn.util.SensorNetwork;

abstract class NodePropertyWorker implements KnownPropertyWorker {
	private final static Logger logger = Logger.getLogger(NodePropertyWorker.class);

	protected abstract boolean performAction(String objectID, String propertyName, Object value);

	protected abstract boolean checkItsMine(String propertyName);

	@Override
	public boolean processMessage(String objectID, String propertyName, Serializable value) {
		if (!checkItsMine(propertyName))
			return false;

		return performAction(objectID, propertyName, value);
	}

	/**
	 * looks for appropriate networkWorker within registered networks (gw`s)
	 * 
	 * @param networkID
	 * @return networkWorker or null if there is no network worker with given id
	 */
	protected NetworkWorker locateNetworkWorker(int networkID) {
		// it seems that sensornetwork class does not override hashCode
		for (Entry<SensorNetwork, NetworkWorker> net : SynapStore.workerMap.entrySet()) {
			if (net.getKey().getNetworkId() == networkID) {
				return net.getValue();
			}
		}
		return null;
	}
}
