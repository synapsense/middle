package com.synapsense.plugin.wsn.util.sensors;

import java.util.NavigableMap;
import java.util.TreeMap;

import com.synapsense.plugin.wsn.util.SensorStates;

public class Thermanode2Thermistor implements SensorConversionInterface {

	//
	// Lookup: rt degC
	private static NavigableMap<Double, Double> therm_lut;

	static {
		therm_lut = new TreeMap<Double, Double>();
		// Lookup table derived from US Sensor Datasheet, J curve, 10k nominal
		// Converted by make_lut.py
		// Satisfies f(x) = 5053.45/(5.65154 + log(x)) - 315.049 - exp(16.2026)
		// + exp(16.2026) (as per Eureqa)
		therm_lut.put(7389000.00, -80.00);
		therm_lut.put(6757530.00, -79.00);
		therm_lut.put(6184463.00, -78.00);
		therm_lut.put(5664013.00, -77.00);
		therm_lut.put(5190990.00, -76.00);
		therm_lut.put(4760763.00, -75.00);
		therm_lut.put(4369173.00, -74.00);
		therm_lut.put(4012500.00, -73.00);
		therm_lut.put(3687397.00, -72.00);
		therm_lut.put(3390863.00, -71.00);
		therm_lut.put(3120207.00, -70.00);
		therm_lut.put(2872997.00, -69.00);
		therm_lut.put(2647053.00, -68.00);
		therm_lut.put(2440407.00, -67.00);
		therm_lut.put(2251290.00, -66.00);
		therm_lut.put(2078100.00, -65.00);
		therm_lut.put(1919397.00, -64.00);
		therm_lut.put(1773873.00, -63.00);
		therm_lut.put(1640357.00, -62.00);
		therm_lut.put(1517777.00, -61.00);
		therm_lut.put(1405170.00, -60.00);
		therm_lut.put(1301663.00, -59.00);
		therm_lut.put(1206463.00, -58.00);
		therm_lut.put(1118853.00, -57.00);
		therm_lut.put(1038180.00, -56.00);
		therm_lut.put(963849.00, -55.00);
		therm_lut.put(895332.00, -54.00);
		therm_lut.put(832130.00, -53.00);
		therm_lut.put(773799.00, -52.00);
		therm_lut.put(719934.00, -51.00);
		therm_lut.put(670166.00, -50.00);
		therm_lut.put(624159.00, -49.00);
		therm_lut.put(581605.00, -48.00);
		therm_lut.put(542225.00, -47.00);
		therm_lut.put(505763.00, -46.00);
		therm_lut.put(471985.00, -45.00);
		therm_lut.put(440674.00, -44.00);
		therm_lut.put(411640.00, -43.00);
		therm_lut.put(384703.00, -42.00);
		therm_lut.put(359700.00, -41.00);
		therm_lut.put(336479.00, -40.00);
		therm_lut.put(314904.00, -39.00);
		therm_lut.put(294848.00, -38.00);
		therm_lut.put(276194.00, -37.00);
		therm_lut.put(258838.00, -36.00);
		therm_lut.put(242681.00, -35.00);
		therm_lut.put(227632.00, -34.00);
		therm_lut.put(213610.00, -33.00);
		therm_lut.put(200539.00, -32.00);
		therm_lut.put(188349.00, -31.00);
		therm_lut.put(176974.00, -30.00);
		therm_lut.put(166356.00, -29.00);
		therm_lut.put(156441.00, -28.00);
		therm_lut.put(147177.00, -27.00);
		therm_lut.put(138518.00, -26.00);
		therm_lut.put(130421.00, -25.00);
		therm_lut.put(122847.00, -24.00);
		therm_lut.put(115759.00, -23.00);
		therm_lut.put(109122.00, -22.00);
		therm_lut.put(102906.00, -21.00);
		therm_lut.put(97081.00, -20.00);
		therm_lut.put(91621.00, -19.00);
		therm_lut.put(86501.00, -18.00);
		therm_lut.put(81698.00, -17.00);
		therm_lut.put(77190.00, -16.00);
		therm_lut.put(72957.00, -15.00);
		therm_lut.put(68982.00, -14.00);
		therm_lut.put(65246.00, -13.00);
		therm_lut.put(61736.00, -12.00);
		therm_lut.put(58434.00, -11.00);
		therm_lut.put(55329.00, -10.00);
		therm_lut.put(52407.00, -9.00);
		therm_lut.put(49656.00, -8.00);
		therm_lut.put(47066.00, -7.00);
		therm_lut.put(44626.00, -6.00);
		therm_lut.put(42327.00, -5.00);
		therm_lut.put(40159.00, -4.00);
		therm_lut.put(38115.00, -3.00);
		therm_lut.put(36187.00, -2.00);
		therm_lut.put(34368.00, -1.00);
		therm_lut.put(32650.00, 0.00);
		therm_lut.put(31029.00, 1.00);
		therm_lut.put(29498.00, 2.00);
		therm_lut.put(28052.00, 3.00);
		therm_lut.put(26685.00, 4.00);
		therm_lut.put(25392.00, 5.00);
		therm_lut.put(24170.00, 6.00);
		therm_lut.put(23013.00, 7.00);
		therm_lut.put(21918.00, 8.00);
		therm_lut.put(20882.00, 9.00);
		therm_lut.put(19901.00, 10.00);
		therm_lut.put(18971.00, 11.00);
		therm_lut.put(18090.00, 12.00);
		therm_lut.put(17255.00, 13.00);
		therm_lut.put(16463.00, 14.00);
		therm_lut.put(15712.00, 15.00);
		therm_lut.put(14999.00, 16.00);
		therm_lut.put(14323.00, 17.00);
		therm_lut.put(13681.00, 18.00);
		therm_lut.put(13072.00, 19.00);
		therm_lut.put(12493.00, 20.00);
		therm_lut.put(11942.00, 21.00);
		therm_lut.put(11419.00, 22.00);
		therm_lut.put(10922.00, 23.00);
		therm_lut.put(10450.00, 24.00);
		therm_lut.put(10000.00, 25.00);
		therm_lut.put(9572.00, 26.00);
		therm_lut.put(9165.00, 27.00);
		therm_lut.put(8777.00, 28.00);
		therm_lut.put(8408.00, 29.00);
		therm_lut.put(8057.00, 30.00);
		therm_lut.put(7722.00, 31.00);
		therm_lut.put(7402.00, 32.00);
		therm_lut.put(7098.00, 33.00);
		therm_lut.put(6808.00, 34.00);
		therm_lut.put(6531.00, 35.00);
		therm_lut.put(6267.00, 36.00);
		therm_lut.put(6015.00, 37.00);
		therm_lut.put(5775.00, 38.00);
		therm_lut.put(5545.00, 39.00);
		therm_lut.put(5326.00, 40.00);
		therm_lut.put(5117.00, 41.00);
		therm_lut.put(4917.00, 42.00);
		therm_lut.put(4725.00, 43.00);
		therm_lut.put(4543.00, 44.00);
		therm_lut.put(4368.00, 45.00);
		therm_lut.put(4201.00, 46.00);
		therm_lut.put(4041.00, 47.00);
		therm_lut.put(3888.00, 48.00);
		therm_lut.put(3742.00, 49.00);
		therm_lut.put(3602.00, 50.00);
		therm_lut.put(3468.00, 51.00);
		therm_lut.put(3340.00, 52.00);
		therm_lut.put(3217.00, 53.00);
		therm_lut.put(3099.00, 54.00);
		therm_lut.put(2986.00, 55.00);
		therm_lut.put(2878.00, 56.00);
		therm_lut.put(2774.00, 57.00);
		therm_lut.put(2675.00, 58.00);
		therm_lut.put(2579.00, 59.00);
		therm_lut.put(2488.00, 60.00);
		therm_lut.put(2400.00, 61.00);
		therm_lut.put(2316.00, 62.00);
		therm_lut.put(2235.00, 63.00);
		therm_lut.put(2157.00, 64.00);
		therm_lut.put(2083.00, 65.00);
		therm_lut.put(2011.00, 66.00);
		therm_lut.put(1942.00, 67.00);
		therm_lut.put(1876.00, 68.00);
		therm_lut.put(1813.00, 69.00);
		therm_lut.put(1752.00, 70.00);
		therm_lut.put(1693.00, 71.00);
		therm_lut.put(1637.00, 72.00);
		therm_lut.put(1582.00, 73.00);
		therm_lut.put(1530.00, 74.00);
		therm_lut.put(1480.00, 75.00);
		therm_lut.put(1432.00, 76.00);
		therm_lut.put(1385.00, 77.00);
		therm_lut.put(1340.00, 78.00);
		therm_lut.put(1297.00, 79.00);
		therm_lut.put(1255.00, 80.00);
		therm_lut.put(1215.00, 81.00);
		therm_lut.put(1177.00, 82.00);
		therm_lut.put(1140.00, 83.00);
		therm_lut.put(1104.00, 84.00);
		therm_lut.put(1070.00, 85.00);
		therm_lut.put(1037.00, 86.00);
		therm_lut.put(1005.00, 87.00);
		therm_lut.put(973.80, 88.00);
		therm_lut.put(944.10, 89.00);
		therm_lut.put(915.50, 90.00);
		therm_lut.put(887.80, 91.00);
		therm_lut.put(861.20, 92.00);
		therm_lut.put(835.40, 93.00);
		therm_lut.put(810.60, 94.00);
		therm_lut.put(786.60, 95.00);
		therm_lut.put(763.50, 96.00);
		therm_lut.put(741.20, 97.00);
		therm_lut.put(719.60, 98.00);
		therm_lut.put(698.70, 99.00);
		therm_lut.put(678.60, 100.00);
		therm_lut.put(659.10, 101.00);
		therm_lut.put(640.30, 102.00);
		therm_lut.put(622.20, 103.00);
		therm_lut.put(604.60, 104.00);
		therm_lut.put(587.60, 105.00);
		therm_lut.put(571.20, 106.00);
		therm_lut.put(555.30, 107.00);
		therm_lut.put(539.90, 108.00);
		therm_lut.put(525.00, 109.00);
		therm_lut.put(510.60, 110.00);
		therm_lut.put(496.70, 111.00);
		therm_lut.put(483.20, 112.00);
		therm_lut.put(470.10, 113.00);
		therm_lut.put(457.50, 114.00);
		therm_lut.put(445.30, 115.00);
		therm_lut.put(433.40, 116.00);
		therm_lut.put(421.90, 117.00);
		therm_lut.put(410.80, 118.00);
		therm_lut.put(400.00, 119.00);
		therm_lut.put(389.60, 120.00);
		therm_lut.put(379.40, 121.00);
		therm_lut.put(369.60, 122.00);
		therm_lut.put(360.10, 123.00);
		therm_lut.put(350.90, 124.00);
		therm_lut.put(341.90, 125.00);
		therm_lut.put(333.20, 126.00);
		therm_lut.put(324.80, 127.00);
		therm_lut.put(316.60, 128.00);
		therm_lut.put(308.70, 129.00);
		therm_lut.put(301.00, 130.00);
		therm_lut.put(293.50, 131.00);
		therm_lut.put(286.30, 132.00);
		therm_lut.put(279.20, 133.00);
		therm_lut.put(272.40, 134.00);
		therm_lut.put(265.80, 135.00);
		therm_lut.put(259.30, 136.00);
		therm_lut.put(253.10, 137.00);
		therm_lut.put(247.00, 138.00);
		therm_lut.put(241.10, 139.00);
		therm_lut.put(235.30, 140.00);
		therm_lut.put(229.70, 141.00);
		therm_lut.put(224.30, 142.00);
		therm_lut.put(219.00, 143.00);
		therm_lut.put(213.90, 144.00);
		therm_lut.put(208.90, 145.00);
		therm_lut.put(204.10, 146.00);
		therm_lut.put(199.40, 147.00);
		therm_lut.put(194.80, 148.00);
		therm_lut.put(190.30, 149.00);
		therm_lut.put(186.10, 150.00);

	}

	public double convertFromRaw(char raw) {
		// voltage is measured in 12bits ADCCounts
		// 0 -> 0V
		// 4095 -> 2.048V
		final double rawV = raw & 0xFFF;

		final double maxADC = 4095.0;

		final double r1 = 20000;
		final double r2 = 10000;
		final double r3 = 10000;
		final double r4 = 10000;
		final double rf = 10000;

		try {

			final double resist = r2 * (1.0 + (r4 / r1)) / (rawV / maxADC + r4 / (r3 + r4) * rf / r1) - r2;

			Double floorkey = therm_lut.floorKey(resist);
			if (floorkey == null)
				return SensorStates.SENSOR_DISCONNECT;

			Double ceilkey = therm_lut.ceilingKey(resist);
			if (ceilkey == null)
				return SensorStates.SENSOR_DISCONNECT;

			final double fv = therm_lut.get(floorkey);
			final double cv = therm_lut.get(ceilkey);
			double tempC = SensorStates.SENSOR_DISCONNECT;

			if (fv == cv) {
				// Exact match, simply convert and return temp
				tempC = cv;
			} else {
				// Interpolate between fv and cv, linearaly
				// TODO: Replace this with a log2 interpolation to match
				// the B parameter equation!
				final double diffv = cv - fv;
				final double diffk = ceilkey - floorkey;
				final double m = diffv / diffk;
				tempC = m * (resist - floorkey) + fv;
			}

			double tempF = (9.0 / 5.0) * tempC + 32;

			if (tempF <= 20) {
				return SensorStates.SENSOR_DISCONNECT;
			}

			if (tempF > 190.0) {
				return SensorStates.SENSOR_DISCONNECT;
			}

			return tempF;
		} catch (Exception e) {
			return SensorStates.SENSOR_DISCONNECT;
		}
	}

	public double convertFromRawCompensated(char raw, double vRef) {

		return convertFromRaw(raw);
	}

	public int getMoteDataType() {

		return 36;
	}

}
