package com.synapsense.plugin.wsn.p3;

public class Jawa implements Comparable<Jawa> {
	private long jid;
	private int uPos;
	private int uHeight;
	private int rack;
	private int feed;
	private int phase;
	private Boolean isPhaseRef = false;

	public Jawa(byte[] data) {
		jid = ((data[0] << 24) & 0xff000000L) | ((data[1] << 16) & 0xff0000) | ((data[2] << 8) & 0xff00)
		        | ((data[3]) & 0xff);
		jid = jid & 0xffffffffL;
		uPos = data[4] & 0xFF;
		uHeight = data[5] & 0xFF;
		rack = ((data[6] >>> 6) & 0x3);
		feed = ((data[6] >>> 3) & 0x7);
		isPhaseRef = (((data[6] >>> 2) & 0x1) == 1 ? true : false);
		phase = data[6] & 0x3;
	}

	public byte[] pack() {
		byte[] data = new byte[7];
		data[0] = (byte) ((jid >>> 24) & 0xff);
		data[1] = (byte) ((jid >>> 16) & 0xff);
		data[2] = (byte) ((jid >>> 8) & 0xff);
		data[3] = (byte) (jid & 0xff);
		data[4] = (byte) uPos;
		data[5] = (byte) uHeight;
		data[6] = (byte) ((((rack) & 0x3) << 6) | (((feed) & 0x7) << 3) | (phase & 0x3));
		if (isPhaseRef)
			data[6] |= (1 << 2);
		return data;
	}

	public long getJid() {
		return jid;
	}

	public int getuPos() {
		return uPos;
	}

	public int getuHeight() {
		return uHeight;
	}

	public int getRack() {
		return rack;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		Jawa jawa = (Jawa) o;

		if (feed != jawa.feed)
			return false;
		if (jid != jawa.jid)
			return false;
		if (phase != jawa.phase)
			return false;
		if (rack != jawa.rack)
			return false;
		if (uHeight != jawa.uHeight)
			return false;
		if (uPos != jawa.uPos)
			return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result = (int) (jid ^ (jid >>> 32));
		result = 31 * result + uPos;
		result = 31 * result + uHeight;
		result = 31 * result + rack;
		result = 31 * result + feed;
		result = 31 * result + phase;
		return result;
	}

	public int getFeed() {
		return feed;
	}

	public int getPhase() {
		return phase;
	}

	public Boolean getIsPhaseRef() {
		return isPhaseRef;
	}

	public int compareTo(Jawa rhs) {
		if (this.rack != rhs.rack) {
			return this.rack - rhs.rack;
		}

		if (this.uPos != rhs.uPos) {
			return this.uPos - rhs.uPos;
		}

		if (this.jid == rhs.jid)
			return 0;
		if (this.jid > rhs.jid)
			return 1;
		return -1;
	}

	public String toString() {
		return "<JawaModel: JID " + Long.toHexString(jid) + " uPos:" + uPos + " uHeight: " + uHeight + " rack:" + rack
		        + " feed:" + feed + " phase:" + phase + " phRef:" + (isPhaseRef ? "REF" : "no") + ">";

	}

}