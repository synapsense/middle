package com.synapsense.plugin.wsn.util.timesync;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class TimeSync {
	private static Log log = LogFactory.getLog(TimeSync.class);
	// / Defines the maximum value of the mote timer
	public static final long generationAdd = (long) Math.pow(2, 32) - 1;
	// / Defines the midway point of the mote timer, trying to detect mote
	// resets - within 3 minutes of wraparound
	public static final long generationCutoff = (long) Math.pow(2, 32) - 32768 * 60 * 3;

	public static final long generationBackdate = (long) (Math.pow(2, 32) / 2.0);
	// The timestamp can be FUTURECUTOFF ms in the future.
	public static final long FUTURECUTOFF = 500;

	public int generationCounter = 0;
	private long TSoffset = -1;
	private long lastMoteTime = -1;

	public synchronized void processMsg(long t, long syst) {

		// long syst = System.currentTimeMillis();
		// syst/1000 = t/32768
		long ct = (long) (t / 32768.0 * 1000.0);
		if (t < lastMoteTime && lastMoteTime > generationCutoff) {
			generationCounter++;
			lastMoteTime = t;
			TSoffset = syst - ct;

		} else if (t < lastMoteTime && lastMoteTime < generationCutoff) { // base
			TSoffset = -1;
			lastMoteTime = t;
		} else {
			lastMoteTime = t;
			TSoffset = syst - ct;
		}
	}

	public synchronized TimeSyncTime convertTime(long moteTime, long nowtime) {
		TimeSyncTime returnTime = new TimeSyncTime();

		if (moteTime == 0) {
			log.trace("CONVERT: Returning local time");
			returnTime.setDidConvert(false);
			returnTime.setConvertedTime(nowtime);
			return returnTime;
		}

		if (moteTime < lastMoteTime && Math.abs(lastMoteTime - moteTime) > generationBackdate) {
			moteTime += generationAdd;
		} else if (moteTime > lastMoteTime && Math.abs(lastMoteTime - moteTime) > generationBackdate) {
			moteTime -= generationAdd;
		}
		long ct = (long) (moteTime / 32768.0 * 1000.0);

		if (TSoffset == -1) {
			returnTime.setConvertedTime(nowtime);
			returnTime.setDidConvert(false);
			return returnTime;
		} else {
			long now = nowtime;
			if (ct + TSoffset > now + FUTURECUTOFF) { // Do not go into the future!
				returnTime.setConvertedTime(now);
				returnTime.setDidConvert(false);
				return returnTime;
			} else {
				returnTime.setConvertedTime(Math.min(ct + TSoffset, nowtime));
				returnTime.setDidConvert(true);
				return returnTime;
			}
		}
	}

	public long systemTime() {
		return System.currentTimeMillis();
	}

	public TimeSync() {

	}

}
