package com.synapsense.plugin.wsn.softbase29.packet;

import com.synapsense.plugin.wsn.softbase29.GateWay;

public class RadioMsg {
	final public byte[] data;
	final public char sourceid;
	final public int size;
	final public int service;
	final public GateWay gateway;
	final public OSSDataPacket original_pkt;

	public RadioMsg(OSSDataPacket pkt, GateWay gateway) {
		this.data = pkt.data;
		this.sourceid = pkt.nwk_srcaddr;
		this.size = pkt.nwk_datalen;
		this.service = pkt.nwk_service;
		this.gateway = gateway;
		this.original_pkt = pkt;
	}
}
