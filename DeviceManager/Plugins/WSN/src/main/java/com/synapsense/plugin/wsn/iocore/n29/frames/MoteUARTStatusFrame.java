package com.synapsense.plugin.wsn.iocore.n29.frames;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.plugin.wsn.iocore.frames.MoteUARTBadFrame;
import com.synapsense.plugin.wsn.iocore.frames.MoteUARTFrame;
import com.synapsense.plugin.wsn.util.serializer.BigSerializer;

public class MoteUARTStatusFrame extends MoteUARTFrame {

	public boolean timemaster;
	public boolean LPLmode;
	public boolean SMOTAmode;
	public int hoptm;
	public char nodeid;
	public long nwkstarttime;
	public int ToSensorsSeqno;
	private static Log log = LogFactory.getLog(MoteUARTStatusFrame.class);

	public MoteUARTStatusFrame(byte[] header, byte[] data, String portname) throws MoteUARTBadFrame {
		super(header, portname);
		this.data = data;
		BigSerializer serial = new BigSerializer(data);
		int nwkversion = serial.unpackU_INT8();
		if (nwkversion != 29) {
			log.error("Gateway version is different");
			throw new MoteUARTBadFrame();
		}
		if (serial.unpackU_INT8() == 0)
			timemaster = false;
		else
			timemaster = true;

		if (serial.unpackU_INT8() == 0)
			LPLmode = false;
		else
			LPLmode = true;
		if (serial.unpackU_INT8() == 0)
			SMOTAmode = false;
		else
			SMOTAmode = true;

		this.hoptm = serial.unpackU_INT8();
		this.nodeid = serial.unpackU_INT16();
		this.nwkstarttime = serial.unpackU_INT32();
		this.ToSensorsSeqno = serial.unpackU_INT8();
	}

	public String toString() {
		return ("TM: " + timemaster + " LPL: " + LPLmode + " SMOTA: " + SMOTAmode + " HopTM: " + hoptm + " ID: "
		        + (int) nodeid + " NwkStartTime: " + nwkstarttime + " TSSeqno: " + ToSensorsSeqno);
	}
}
