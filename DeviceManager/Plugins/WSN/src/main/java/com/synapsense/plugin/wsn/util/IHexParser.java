package com.synapsense.plugin.wsn.util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

// Reference from:
// http://www.cs.net/lucid/intel.htm

public class IHexParser {

	private byte[] binary_data = null;
	private String filename;

	public IHexParser(int startAtAddr, int size, String filename) throws IOException {
		binary_data = new byte[size];

		for (int i = 0; i < size; i++) {
			binary_data[i] = (byte) 0xFF;
		}

		FileReader inf = new FileReader(filename);
		BufferedReader ins = new BufferedReader(inf);
		int baseaddr = 0;
		String line = null;
		int lineCo = 1;
		while ((line = ins.readLine()) != null) {
			if (line.charAt(0) != ':') {
				ins.close();
				throw new IOException("MALFORMED IHEX FILE, LINE " + lineCo);
			}
			char c[] = new char[2];
			c[0] = line.charAt(1);
			c[1] = line.charAt(2);
			int charCount = Integer.parseInt(new String(c), 16);
			c = new char[4];
			c[0] = line.charAt(3);
			c[1] = line.charAt(4);
			c[2] = line.charAt(5);
			c[3] = line.charAt(6);
			int addr = Integer.parseInt(new String(c), 16);

			c = new char[2];
			c[0] = line.charAt(7);
			c[1] = line.charAt(8);
			int type = Integer.parseInt(new String(c), 16);
			if (type == 0) {
				// Parse data
				c = new char[2];
				int pos = baseaddr + addr - startAtAddr;
				// Ignore the outside of the range
				if (pos < 0 || pos >= size)
					continue;
				int i = 0;
				for (i = 0; i < charCount * 2; i += 2) {
					c[0] = line.charAt(9 + i);
					c[1] = line.charAt(9 + i + 1);
					binary_data[pos] = (byte) Integer.parseInt(new String(c), 16);
					pos++;
				}

				// Check checksum

				c = new char[2];
				// Fill in the rest
				// The least significant byte of the two's complement sum of the
				// values represented by all the pairs of characters in the
				// record except the start code and checksum

			} else if (type == 1) {
				// End
				continue;
			} else if (type == 2) {
				// Segment base addr - ignore
				continue;
			} else if (type == 4) {
				c = new char[4];
				c[0] = line.charAt(9);
				c[1] = line.charAt(10);
				c[2] = line.charAt(11);
				c[3] = line.charAt(12);
				baseaddr = Integer.parseInt(new String(c), 16) << 16;
			}

			lineCo++;
		}
		ins.close();
		System.out.println("Loaded " + lineCo + " lines from iHEX " + filename);
		this.filename = filename;
	}

	public byte[] getData() {
		return binary_data;
	}

	public byte[] getData(int startoffset, int length) {
		byte[] ret = new byte[length];
		System.arraycopy(binary_data, startoffset, ret, 0, length);
		return ret;
	}

	public boolean checkcontent(int location, byte[] data) {
		for (int i = 0; i < data.length; i++) {
			if (binary_data[location + i] != data[i])
				return false;
		}
		return true;
	}

	public String getFilename() {
		return filename;
	}
}
