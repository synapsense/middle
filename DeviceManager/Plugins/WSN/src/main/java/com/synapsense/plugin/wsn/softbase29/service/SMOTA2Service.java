package com.synapsense.plugin.wsn.softbase29.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Random;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.plugin.wsn.configuration.Configuration;
import com.synapsense.plugin.wsn.configuration.WSNNode;
import com.synapsense.plugin.wsn.iocore.frames.out.MoteUARTOutCommandFrame;
import com.synapsense.plugin.wsn.iocore.frames.out.MoteUARTOutFrame;
import com.synapsense.plugin.wsn.iocore.n29.frames.out.MoteUARTOutBaseCommandFrame;
import com.synapsense.plugin.wsn.iocore.n29.frames.out.MoteUARTOutFirmwareFrame;
import com.synapsense.plugin.wsn.servers.UpdateServerThread;
import com.synapsense.plugin.wsn.smota2.Firmware;
import com.synapsense.plugin.wsn.softbase29.GateWay;
import com.synapsense.plugin.wsn.softbase29.SendError;
import com.synapsense.plugin.wsn.softbase29.Softbase;
import com.synapsense.plugin.wsn.softbase29.SoftbaseNWK;
import com.synapsense.plugin.wsn.softbase29.packet.RadioMsg;
import com.synapsense.plugin.wsn.softbase29.state.OSSNHT;
import com.synapsense.plugin.wsn.util.Constants;
import com.synapsense.plugin.wsn.util.serializer.LittleSerializer;
import com.synapsense.plugin.wsn.util.serializer.Serializer;

public class SMOTA2Service implements Runnable, Service {
	private final byte TYPE_ANNOUNCE = 0;
	private final byte TYPE_REPORT = 1;
	private final byte TYPE_SETFWD = 2;
	private final byte TYPE_RESETFWD = 3;
	private final byte TYPE_CODE = 4;
	private final byte TYPE_REQUEST = 5;
	private final byte TYPE_COMPLETE = 6;
	private final byte TYPE_RECOVERY = 7;
	private final byte TYPE_DONE = 8;
	private final byte TYPE_RESTART = 9;

	private final int SMOTASTARTDELAY = 12 * 60000; // Delay 12 min. for
	                                                // propagating start message
	private final int INIT_WAITTIME = 30 * 60000; // wait 30 min. for initially
	                                              // for joining, reporting, and
	                                              // recovery request
	private final int NODE_WAITTIME = 5 * 60000; // Wait 5 min. for node joining
	private final int HEAR_WAITTIME = 3 * 60000; // Wait 3 min. for node
	                                             // reporting that they can hear
	private final int DELAY_MULTIPLIER = 10 * 64;
	private final int REQUEST_WAITTIME = 6 * 60000; // Wait 6 min. for nodes to
	                                                // request
	private final int FWDREPORT_WAITTIME = 3 * 60000; // Wait 3 min. for fwd
	                                                  // node report after
	                                                  // distribution
	private final int RESTART_DELAY = 4 * 60000;
	private final int HIGH_PRIORITY_RECOVERY_COUNT = 20;
	private final int GATEWAY_RECOVER_WAIT = 5 * 60000; // Wait 5 min. for a
	                                                    // gateway to recover
	                                                    // itself
	private final int GW_JOIN_WAITTIME = 2 * 60000; // Wait 2 min. for a gateway
	                                                // to become alive and get
	                                                // timesynced.

	private Softbase softbase;
	private Announce announce;
	private Update update;
	private int smotastarttime;
	private boolean smotarunning;
	private char lastnodesetfwd;
	private boolean parentset;
	private int fwdcount;
	private long fwdreporttime;
	private long lastrequesttime;
	private int distseqno;
	private LinkedList<Integer> requestlist;
	private LinkedList<Character> nodelist;
	private Firmware firmware;
	private UpdateServerThread updateserver;
	private Object UpdateLock;
	private ArrayList<GateWay> gatewaylist;
	private boolean atleastonedone;
	private boolean updateinprogress;

	private static Log log = LogFactory.getLog(SMOTA2Service.class);

	private volatile boolean interrupted = false;

	public SMOTA2Service(Softbase softbase) {
		this.softbase = softbase;
		requestlist = new LinkedList<Integer>();
		nodelist = new LinkedList<Character>();
		smotarunning = false;
		UpdateLock = new Object();
		updateinprogress = false;
	}

	public void close() {

	}

	public void startUpdate(Firmware firmware, UpdateServerThread updateserver) {
		if (updateinprogress) {
			updateserver.println("Firmware update is already running on this network");
			return;
		}
		updateinprogress = true;
		this.firmware = firmware;
		this.updateserver = updateserver;
		update = new Update("SMOTA_UPDATE-" + Integer.toString(softbase.getNetId()));
		if (firmware.gatewayupdate() && !firmware.nodeupdate()) {
			update.updateGateway();
			if (firmware.getimagetype() == Firmware.IMAGE_NORM)
				update.restartNetwork();
			else {
				// Do not restart network if it is not app update
				log.info("SMOTA2 Done");
				updateserver.println("SMOTA2 Done");
			}
		} else {
			softbase.setSMOTAmode(true);
			update.start();
			synchronized (UpdateLock) {
				try {
					UpdateLock.wait();
				} catch (InterruptedException e) {
					log.error("Interrupted while waiting on UpdateLock", e);
				}
			}
		}
		updateinprogress = false;
	}

	public void run() {
		LinkedList<Character> prevrequestednodes = new LinkedList<Character>();
		while (!interrupted) {
			RadioMsg msg;
			try {
				msg = softbase.nwk.receive(OSSServices.OSS_SERVICE_SMOTA2);
			} catch (InterruptedException e) {
				break;
			}
			if (!smotarunning)
				continue;
			Serializer serial = new LittleSerializer(msg.data);
			int type = serial.unpackU_INT8();
			switch (type) {
			case TYPE_REPORT:
				serial.unpack_INT8(); // read dummy
				char parentid = serial.unpackU_INT16();
				softbase.node_info.reported(msg.sourceid);
				// new node can hear the update
				if (!softbase.node_info.getCanHear(msg.sourceid) && parentid != 0xFFFF) {
					synchronized (nodelist) {
						if (!nodelist.contains(msg.sourceid))
							nodelist.add(msg.sourceid);
					}
					softbase.node_info.setCanHear(msg.sourceid, parentid);
					log.info("Node " + Integer.toHexString(msg.sourceid) + " Hears from "
					        + Integer.toHexString(parentid));
					if (parentid < 0xF000 && !softbase.node_info.getFwd(parentid)) {
						fwdcount++;
						log.trace("Setting " + Integer.toHexString(parentid) + " FWD");
						softbase.node_info.setFwd(parentid, true);
						byte[] setfwdpk = new byte[1];
						setfwdpk[0] = TYPE_SETFWD;
						try {
							softbase.nwk.send(null, setfwdpk, parentid, OSSServices.OSS_SERVICE_SMOTA2,
							        SoftbaseNWK.Protocol_ToOneSensor);
						} catch (SendError e) {
							log.error("Send error", e);
						}
					}
				}
				// new node set newly selected forwarding node as parent
				if (lastnodesetfwd == parentid) {
					parentset = true;
				}
				break;
			case TYPE_REQUEST:
				int size = (msg.size - 2) / 2;
				serial.unpack_INT8(); // read dummy;
				LinkedList<Integer> temprequestlist = new LinkedList<Integer>();
				for (int i = 0; i < size; i++) {
					int seqno = serial.unpackU_INT16();
					if (seqno >= distseqno || seqno >= firmware.getpacketcount())
						continue;
					if (!requestlist.contains(seqno))
						temprequestlist.add(seqno);
				}
				synchronized (requestlist) {
					// Add requested packets first if requesting small number of
					// packets and the device did not request recently
					if (size < HIGH_PRIORITY_RECOVERY_COUNT && !prevrequestednodes.contains(msg.sourceid)) {
						requestlist.addAll(0, temprequestlist);
						log.trace("Req from " + (int) msg.sourceid + " added to the beginning: "
						        + temprequestlist.size() + " total: " + requestlist.size());
					} else {
						requestlist.addAll(temprequestlist);
						log.trace("Req from " + (int) msg.sourceid + " added to the end: " + temprequestlist.size()
						        + " total: " + requestlist.size());
					}
					requestlist.notify();
				}
				// Record the last 5 requested devices.
				if (!prevrequestednodes.contains(msg.sourceid)) {
					if (prevrequestednodes.size() < 5)
						prevrequestednodes.add(msg.sourceid);
					else {
						prevrequestednodes.removeFirst();
						prevrequestednodes.add(msg.sourceid);
					}
				}
				break;
			case TYPE_RECOVERY:
				fwdreporttime = System.currentTimeMillis();
				break;
			case TYPE_DONE:
				if (!atleastonedone) {
					// Set lastrequesttime when one node reports it is done
					atleastonedone = true;
					lastrequesttime = System.currentTimeMillis();
				}
				softbase.node_info.setDone(msg.sourceid);
				break;
			}
		}
	}

	public void interrupt() {
		interrupted = true;
	}

	class GatewayUpdateStatus {
		public GateWay gateway;
		public boolean working;

		public GatewayUpdateStatus(GateWay gateway) {
			this.gateway = gateway;
			working = true;
		}
	}

	class Update implements Runnable {
		private String name;
		private volatile Thread updatethread;

		public Update(String name) {
			this.name = name;
		}

		public void start() {
			updatethread = new Thread(this, name);
			updatethread.start();
		}

		public void updateGateway() {
			int printpercent = 10;
			log.info("Starting Gateway update");
			updateserver.println("Starting Gateway update");
			ArrayList<GatewayUpdateStatus> gateways = new ArrayList<GatewayUpdateStatus>();
			Iterator<com.synapsense.plugin.wsn.softbase.GateWay> gatewayiter = softbase.getGateways().iterator();
			while (gatewayiter.hasNext()) {
				GateWay gateway = (GateWay) gatewayiter.next();
				gateways.add(new GatewayUpdateStatus(gateway));
			}
			for (int i = 0; i < firmware.getgatewaycodecount(); i++) {
				if (((double) (i + 1)) / firmware.getgatewaycodecount() * 100 >= printpercent) {
					log.info(printpercent + "% done");
					updateserver.println(printpercent + "% done");
					printpercent = printpercent + 10;
				}
				log.info("Sending " + (i + 1) + "/" + firmware.getgatewaycodecount());
				// updateserver.println("Sending "+(i+1)+"/"+firmware.getgatewaycodecount());
				byte[] codebyte = firmware.getGatewaycode(i);
				MoteUARTOutFirmwareFrame frame = new MoteUARTOutFirmwareFrame(i, firmware.getimagetype(), codebyte);
				boolean sentonce = false;
				Iterator<GatewayUpdateStatus> iter = gateways.iterator();
				while (iter.hasNext()) {
					GatewayUpdateStatus gstatus = iter.next();
					if (gstatus.working) {
						boolean success = false;
						Random rand = new Random();
						for (int j = 0; j < 5; j++) {
							success = gstatus.gateway.sendFrame(frame);
							if (success)
								break;
							try {
								Thread.sleep(1000 + rand.nextInt(2000));
							} catch (InterruptedException e) {
								log.error("Interrupted while waiting for next attempt", e);
								return;
							}
						}
						if (success) {
							sentonce = true;
						} else {
							gstatus.working = false;
						}
					}
				}
				if (!sentonce)
					break;
				try {
					if (i == 0)
						Thread.sleep(2000);
					else
						Thread.sleep(500);
				} catch (InterruptedException e) {
					log.error("Interrupted while waiting before sending the next gateway code", e);
					return;
				}
			}
			// Wait for copying
			try {
				Thread.sleep(25000);
			} catch (InterruptedException e) {
				log.error("Interrupted while copying", e);
				return;
			}

			byte[] dummycode = new byte[128];
			MoteUARTOutFirmwareFrame frame = new MoteUARTOutFirmwareFrame(firmware.getgatewaycodecount(),
			        firmware.getimagetype(), dummycode);
			Iterator<GatewayUpdateStatus> iter = gateways.iterator();
			while (iter.hasNext()) {
				GatewayUpdateStatus gstatus = iter.next();
				if (gstatus.working) {
					if (!gstatus.gateway.sendFrame(frame)) {
						gstatus.working = false;
					}
				}
			}

			log.info("Gateway Update Done");
			updateserver.println("Gateway Update Done");
			Iterator<GatewayUpdateStatus> gstatusiter = gateways.iterator();
			while (gstatusiter.hasNext()) {
				GatewayUpdateStatus gstatus = gstatusiter.next();
				if (gstatus.working) {
					log.info(gstatus.gateway.getPortname() + " SUCCESS");
					updateserver.println(gstatus.gateway.getPortname() + " SUCCESS");
				} else {
					log.info(gstatus.gateway.getPortname() + " FAIL");
					updateserver.println(gstatus.gateway.getPortname() + " FAIL");
				}
			}
		}

		private void sendsmotacommand() {
			// Send SMOTA mode change command
			log.info("Sending SMOTA command to the network");
			updateserver.println("Sending SMOTA command to the network");
			smotastarttime = (int) (softbase.lptimesync.getGlobalTime() >> 5) + SMOTASTARTDELAY;
			MoteUARTOutCommandFrame sendsmotaframe = new MoteUARTOutCommandFrame(MoteUARTOutCommandFrame.ALL_NODES,
			        Constants.COMMAND_ID_ENTER_SMOTA, smotastarttime);
			softbase.getCommandQ().add(sendsmotaframe);

			// Change Gateways to SMOTA mode
			log.info("Switching the network to SMOTA mode in 12 minutes.");
			updateserver.println("Switching the network to SMOTA mode in 12 minutes.");
			smotarunning = true;
			Iterator<com.synapsense.plugin.wsn.softbase.GateWay> iter = softbase.getGateways().iterator();
			MoteUARTOutFrame smotamodeframe = new MoteUARTOutBaseCommandFrame(MoteUARTOutBaseCommandFrame.SMOTAMODE,
			        smotastarttime);
			int retrycount = 0;
			while (iter.hasNext()) {
				GateWay gateway = (GateWay) iter.next();
				for (int i = 0; i < 5; i++) {
					if (gateway.sendFrame(smotamodeframe))
						break;
					try {
						Thread.sleep(30000);
					} catch (InterruptedException e) {
						log.error("Interrupted while waiting before sending the next SMOTA frame", e);
						return;
					}
					retrycount++;
				}
			}
			softbase.initialization.LPLstarted();
			recollect_node_info();
			try {
				Thread.sleep(SMOTASTARTDELAY - 30000 * retrycount);
			} catch (InterruptedException e2) {
				log.error("Interrupted", e2);
				return;
			}
			softbase.lptimesync.setRate(OSSNHT.INIT_BEACON_RATE);
		}

		private void recollect_node_info() {
			softbase.node_info.clear();
			ArrayList<WSNNode> nodes = Configuration.getAllNodesinNetwork(softbase.getNetId());
			Iterator<WSNNode> iter = nodes.iterator();
			while (iter.hasNext()) {
				WSNNode node = iter.next();
				softbase.node_info.addNode((char) node.getLogicalID());
			}
			softbase.node_info.updateReset();
		}

		private boolean allgatewayconnected() {
			Iterator<com.synapsense.plugin.wsn.softbase.GateWay> iter = softbase.getGateways().iterator();
			while (iter.hasNext()) {
				GateWay gateway = (GateWay) iter.next();
				if (!gateway.isAlive()) {
					return false;
				}
			}
			return true;
		}

		private void printgatewaynotconnected() {
			log.info("These gateways are not connected");
			updateserver.println("These gateways are not connected");
			Iterator<com.synapsense.plugin.wsn.softbase.GateWay> iter = softbase.getGateways().iterator();
			while (iter.hasNext()) {
				GateWay gateway = (GateWay) iter.next();
				if (!gateway.isAlive()) {
					log.info(gateway.getPortname());
					updateserver.println(gateway.getPortname());
				}
			}
		}

		private void waitnodejoin() {
			// Wait for nodes to join
			log.info("Waiting for nodes to join");
			updateserver.println("Waiting for nodes to join");
			// Add (INIT_WAITTIME - NODE_WAITTIME) so that first timeout is
			// after INIT_WAITTIME min.
			softbase.initialization.setlastnodejointime(System.currentTimeMillis() + INIT_WAITTIME - NODE_WAITTIME);
			do {
				while (System.currentTimeMillis() - softbase.initialization.getlastnodejointime() < NODE_WAITTIME) {
					try {
						Thread.sleep(60000);
					} catch (InterruptedException e) {
						log.error("Interrupted while waiting for node to join", e);
						return;
					}
					if (softbase.node_info.allNodesReported() && allgatewayconnected())
						break;
					log.info("Found " + softbase.node_info.getReportedCount() + " nodes out of "
					        + softbase.node_info.size() + " nodes");
					updateserver.println("Found " + softbase.node_info.getReportedCount() + " nodes out of "
					        + softbase.node_info.size() + " nodes");
				}
				log.info("Found " + softbase.node_info.getReportedCount() + " nodes out of "
				        + softbase.node_info.size() + " nodes");
				updateserver.println("Found " + softbase.node_info.getReportedCount() + " nodes out of "
				        + softbase.node_info.size() + " nodes");
				if (softbase.node_info.allNodesReported() && allgatewayconnected()) {
					break;
				} else {
					if (!softbase.node_info.allNodesReported())
						softbase.node_info.printNotJoinedNodes(updateserver);
					if (!allgatewayconnected())
						printgatewaynotconnected();
					if (!updateserver.askNodeJoin())
						break;
					softbase.initialization.setlastnodejointime(System.currentTimeMillis());
				}
			} while (true);
			gatewaylist = new ArrayList<GateWay>();
			Iterator<com.synapsense.plugin.wsn.softbase.GateWay> iter = softbase.getGateways().iterator();
			while (iter.hasNext()) {
				GateWay gateway = (GateWay) iter.next();
				if (gateway.isAlive()) {
					gatewaylist.add(gateway);
				}
			}
		}

		private void findfwdnodes(boolean firstrun) {
			log.info("Finding forwarding nodes");
			updateserver.println("Building a distribution tree");
			// Start Announcing
			fwdcount = gatewaylist.size();
			announce = new Announce("SMOTA_ANNOUNCE-" + Integer.toString(softbase.getNetId()), TYPE_ANNOUNCE);
			announce.start();

			int curnodehearcount = 0;
			// Add (INIT_WAITTIME - HEAR_WAITTIME) so that first timeout is
			// after INIT_WAITTIME min.
			long lastnodeheartime = System.currentTimeMillis() + INIT_WAITTIME - HEAR_WAITTIME;
			lastnodesetfwd = 0;
			long printtime = System.currentTimeMillis();
			boolean atleastoneheard = false;
			while (true) {
				if (softbase.node_info.allCanHear())
					break;
				if (System.currentTimeMillis() - lastnodeheartime > HEAR_WAITTIME) {
					if (!parentset && lastnodesetfwd != 0) {
						// Reset forwarding state because no node set this node
						// as parent
						fwdcount--;
						log.trace("ReSetting " + Integer.toHexString(lastnodesetfwd) + " FWD");
						softbase.node_info.setFwd(lastnodesetfwd, false);
						byte[] resetfwdpk = new byte[1];
						resetfwdpk[0] = TYPE_RESETFWD;
						try {
							softbase.nwk.send(null, resetfwdpk, lastnodesetfwd, OSSServices.OSS_SERVICE_SMOTA2,
							        SoftbaseNWK.Protocol_ToOneSensor);
						} catch (SendError e) {
							log.error("Send error", e);
						}
					}
					lastnodesetfwd = 0;
					if (nodelist.size() != 0 && firstrun) {
						synchronized (nodelist) {
							log.trace("FIND FWD NODE");
							lastnodesetfwd = softbase.node_info.findNextFwdNode(nodelist);
						}
					}
					if (lastnodesetfwd != 0) {
						// set node to forward
						log.trace("NEXT FWD: " + (int) lastnodesetfwd);
						fwdcount++;
						parentset = false;
						log.trace("Setting " + Integer.toHexString(lastnodesetfwd) + " FWD");
						softbase.node_info.setFwd(lastnodesetfwd, true);
						byte[] setfwdpk = new byte[1];
						setfwdpk[0] = TYPE_SETFWD;
						try {
							softbase.nwk.send(null, setfwdpk, lastnodesetfwd, OSSServices.OSS_SERVICE_SMOTA2,
							        SoftbaseNWK.Protocol_ToOneSensor);
						} catch (SendError e) {
							log.error("Send error: ", e);
						}
						curnodehearcount = softbase.node_info.canHearCount();
						lastnodeheartime = System.currentTimeMillis();
					} else {
						// No more node to set forwarding
						softbase.node_info.printCannotHear(updateserver);
						if (updateserver.askNodeHear()) {
							try {
								// Wait for nodes to join
								Thread.sleep(2 * 60000);
							} catch (InterruptedException e) {
								log.error("Interrupted while waiting for nodes to join", e);
								return;
							}
							curnodehearcount = softbase.node_info.canHearCount();
							lastnodeheartime = System.currentTimeMillis();
							firstrun = false;
						} else
							break;
					}
				}
				try {
					Thread.sleep(30000);
				} catch (InterruptedException e) {
					log.error("Interrupted", e);
					return;
				}
				// Set lastnodeheartime when at least one node reports that it
				// has a parent
				// Set lastnodeheartime when the number of nodes having parent
				// is 3 more than last time checked
				if (!atleastoneheard || softbase.node_info.canHearCount() - curnodehearcount > 3) {
					atleastoneheard = true;
					lastnodeheartime = System.currentTimeMillis();
				}
				curnodehearcount = softbase.node_info.canHearCount();
				if (System.currentTimeMillis() - printtime > 55 * 1000) {
					log.info(softbase.node_info.canHearCount() + " out of " + softbase.node_info.getReportedCount()
					        + " are part of the distribution tree.");
					updateserver.println(softbase.node_info.canHearCount() + " out of "
					        + softbase.node_info.getReportedCount() + " are part of the distribution tree.");
					printtime = System.currentTimeMillis();
				}

			}
			log.info(softbase.node_info.canHearCount() + " out of " + softbase.node_info.getReportedCount()
			        + " are part of the distribution tree.");
			updateserver.println(softbase.node_info.canHearCount() + " out of " + softbase.node_info.getReportedCount()
			        + " are part of the distribution tree.");

			log.info(fwdcount + " nodes forward");
			updateserver.println(fwdcount + " nodes forward");
			softbase.node_info.printFwdNode();

			// All node can hear now
			// Stop Announcing
			announce.stop();
		}

		private void startdistribution() {
			int printpercent = 10;
			log.info("Starting Distribution");
			updateserver.println("Starting Distribution");
			distseqno = 0;
			requestlist = new LinkedList<Integer>();
			byte[] codepk = new byte[100];
			Serializer serial = new LittleSerializer(codepk);
			while (true) {

				serial.setPosition(0);
				serial.packU_INT8(TYPE_CODE);
				serial.packU_INT8((byte) 0);
				int sendseqno = 0;
				synchronized (requestlist) {
					if (requestlist.size() != 0) {
						// Send Requested packets
						sendseqno = requestlist.pollFirst();
					} else {
						if (distseqno >= firmware.getpacketcount())
							break;
						sendseqno = distseqno;
						distseqno++;
					}
				}
				serial.packU_INT16((char) sendseqno);
				byte[] codebyte = firmware.get(sendseqno);
				if (((double) (sendseqno + 1)) / firmware.getpacketcount() * 100 >= printpercent) {
					log.info(printpercent + "% done");
					updateserver.println(printpercent + "% done");
					printpercent = printpercent + 10;
				}
				log.info("Sending " + (sendseqno + 1) + "/" + firmware.getpacketcount());
				// updateserver.println("Sending "+(sendseqno+1)+"/"+firmware.getpacketcount());
				System.arraycopy(codebyte, 0, codepk, 4, 96);
				Iterator<GateWay> iter = gatewaylist.iterator();
				while (iter.hasNext()) {
					GateWay gateway = iter.next();
					// Check if the gateway is alive, time synced and have
					// packets to send
					// Wait for 5 min. for a gateway to recover itself
					while (true) {
						boolean gatewayworking = false;
						for (int i = 0; i < 10; i++) {
							if (!gateway.isAlive() || !gateway.isTimeSynced() || gateway.getSendMsgQueueSize() != 0) {
								log.info("Gateway " + gateway.getPortname() + " Alive: " + gateway.isAlive()
								        + " Time synched: " + gateway.isTimeSynced() + " SendQ: "
								        + gateway.getSendMsgQueueSize());
								try {
									Thread.sleep(GATEWAY_RECOVER_WAIT / 10);
								} catch (InterruptedException e) {
									log.error("Interrupted", e);
									return;
								}
								continue;
							}
							gatewayworking = true;
							break;
						}
						if (gatewayworking)
							break;
						else {
							// Gateway did not recover in 5 min. Prompt user
							log.info("Please reset gateway " + gateway.getPortname());
							updateserver.println("Please reset gateway " + gateway.getPortname());
							updateserver.waitforenter();
							try {
								Thread.sleep(GW_JOIN_WAITTIME);
							} catch (InterruptedException e) {
								log.error("Interrupted while waiting for GW to join", e);
								return;
							}
						}
					}
					softbase.nwk.send(null, codepk, (char) 0, OSSServices.OSS_SERVICE_SMOTA2,
					        SoftbaseNWK.Protocol_Simple_BCast, gateway);
					try {
						Thread.sleep(fwdcount * DELAY_MULTIPLIER / gatewaylist.size());
					} catch (InterruptedException e) {
						log.error("Interrupted", e);
						return;
					}
				}
			}
			log.info("Distribution Done");
			updateserver.println("Distribution Done");

			// Start announcing the distribution is completed
			announce = new Announce("SMOTA_ANNOUNCE-" + Integer.toString(softbase.getNetId()), TYPE_COMPLETE);
			announce.start();
		}

		private void serverequest() {
			atleastonedone = false;
			log.info("Recovering lost packets");
			updateserver.println("Recovering lost packets");
			// Send any requested packet
			fwdreporttime = System.currentTimeMillis();
			// Add (INIT_WAITTIME - REQUEST_WAITTIME) so that first timeout is
			// after INIT_WAITTIME min.
			lastrequesttime = System.currentTimeMillis() + INIT_WAITTIME - REQUEST_WAITTIME;
			long printtime = System.currentTimeMillis();
			while (true) {
				// log.info("Time: "+System.currentTimeMillis()+" Req: "+lastrequesttime+" Fwd: "+fwdreporttime);
				// If all nodes got update, finish serving requests
				if (softbase.node_info.isAllReportedUpdateDone())
					break;
				// If request timed out and forwarding nodes are done, finish
				if (softbase.node_info.isAllFwdNodeDone()
				        && System.currentTimeMillis() - lastrequesttime > REQUEST_WAITTIME)
					break;
				// If request timed out and forwarding node does not report,
				// finish
				if (System.currentTimeMillis() - lastrequesttime > REQUEST_WAITTIME
				        && System.currentTimeMillis() - fwdreporttime > FWDREPORT_WAITTIME)
					break;

				// Check every gateway is working
				Iterator<GateWay> iter = gatewaylist.iterator();
				while (iter.hasNext()) {
					GateWay gateway = iter.next();
					while (true) {
						boolean gatewayworking = false;
						for (int i = 0; i < 10; i++) {
							if (!gateway.isAlive() || !gateway.isTimeSynced() || gateway.getSendMsgQueueSize() != 0) {
								log.info("Gateway " + gateway.getPortname() + " Alive: " + gateway.isAlive()
								        + " Time synched: " + gateway.isTimeSynced() + " SendQ: "
								        + gateway.getSendMsgQueueSize());
								try {
									Thread.sleep(GATEWAY_RECOVER_WAIT / 10);
									// Add delay to recovery to complete
									lastrequesttime = lastrequesttime + GATEWAY_RECOVER_WAIT / 10;
								} catch (InterruptedException e) {
									log.error("Interrupted while waiting for gateway recovery", e);
									return;
								}
								continue;
							}
							gatewayworking = true;
							break;
						}
						if (gatewayworking)
							break;
						else {
							// Gateway did not recover in 5 min. Prompt user
							log.info("Please reset gateway " + gateway.getPortname());
							updateserver.println("Please reset gateway " + gateway.getPortname());
							updateserver.waitforenter();
							try {
								Thread.sleep(GW_JOIN_WAITTIME);
								// Add delay to recover to complete
								lastrequesttime = lastrequesttime + GW_JOIN_WAITTIME;
							} catch (InterruptedException e) {
								log.error("Interrupted", e);
								return;
							}
						}
					}
				}

				// Check any request packets need to be sent
				int sendseqno = 0xFFFF;
				synchronized (requestlist) {
					if (requestlist.size() == 0) {
						announce.noslowannounce();
						try {
							requestlist.wait(30000);
						} catch (InterruptedException e) {
							log.error("Interrupted", e);
							return;
						}
					}
				}
				synchronized (requestlist) {
					if (requestlist.size() != 0)
						sendseqno = requestlist.pollFirst();
				}
				if (sendseqno != 0xFFFF) {
					announce.slowannounce();
					byte[] codepk = new byte[100];
					Serializer serial = new LittleSerializer(codepk);
					serial.packU_INT8(TYPE_CODE);
					serial.packU_INT8((byte) 0);
					serial.packU_INT16((char) sendseqno);
					byte[] codebyte = firmware.get(sendseqno);
					System.arraycopy(codebyte, 0, codepk, 4, 96);
					log.info("Sending " + (sendseqno + 1) + "/" + firmware.getpacketcount() + " left: "
					        + requestlist.size());
					// updateserver.println("Sending "+(sendseqno+1)+"/"+firmware.getpacketcount());
					iter = gatewaylist.iterator();
					while (iter.hasNext()) {
						GateWay gateway = iter.next();
						softbase.nwk.send(null, codepk, (char) 0, OSSServices.OSS_SERVICE_SMOTA2,
						        SoftbaseNWK.Protocol_Simple_BCast, gateway);
						try {
							Thread.sleep(fwdcount * DELAY_MULTIPLIER / gatewaylist.size());
						} catch (InterruptedException e) {
							log.error("Interrupted", e);
							return;
						}
					}
					lastrequesttime = System.currentTimeMillis();
				}
				if (System.currentTimeMillis() - printtime > 55 * 1000) {
					log.info(softbase.node_info.getUpdateDoneCount() + " out of " + softbase.node_info.canHearCount()
					        + " are done.");
					updateserver.println(softbase.node_info.getUpdateDoneCount() + " out of "
					        + softbase.node_info.canHearCount() + " are done.");
					printtime = System.currentTimeMillis();
				}
			}
			log.info(softbase.node_info.getUpdateDoneCount() + " out of " + softbase.node_info.canHearCount()
			        + " are done.");
			updateserver.println(softbase.node_info.getUpdateDoneCount() + " out of "
			        + softbase.node_info.canHearCount() + " are done.");

			log.info("Recovery Done");
			updateserver.println("Recovery Done");

			// Stop announcing distribution complete
			announce.stop();
		}

		private void sendrestart() {
			log.info("Sending Restart Message");
			updateserver.println("Sending Restart Message");
			// Send restart
			announce = new Announce("SMOTA_ANNOUNCE-" + Integer.toString(softbase.getNetId()), TYPE_RESTART);
			announce.start();
			try {
				Thread.sleep(RESTART_DELAY);
			} catch (InterruptedException e) {
				log.error("Interrupted", e);
				return;
			}

			// Stop announcing
			announce.stop();
		}

		private void restartNetwork() {
			// Change Gateways to Normal mode
			log.info("Restarting the network");
			updateserver.println("Restarting the network");
			softbase.setSMOTAmode(false);
			softbase.restartNetwork();
			Iterator<com.synapsense.plugin.wsn.softbase.GateWay> iter = softbase.getGateways().iterator();
			MoteUARTOutBaseCommandFrame frame = new MoteUARTOutBaseCommandFrame(MoteUARTOutBaseCommandFrame.RESET);
			while (iter.hasNext()) {
				GateWay gateway = (GateWay) iter.next();
				gateway.sendFrame(frame);
			}
			log.info("SMOTA2 Done");
			updateserver.println("SMOTA2 Done");
		}

		public void run() {
			sendsmotacommand();

			boolean firstrun = true;

			do {
				waitnodejoin();

				findfwdnodes(firstrun);

				startdistribution();

				serverequest();

				// Report
				softbase.node_info.printUpdateResult(updateserver);

				if (softbase.node_info.isAllReportedUpdateDone())
					break;

				if (!updateserver.askRetry())
					break;

				softbase.node_info.resetFailedNodes();
				firstrun = false;
			} while (true);

			if (firmware.gatewayupdate())
				updateGateway();

			sendrestart();

			// Smota is completed
			smotarunning = false;

			restartNetwork();
			synchronized (UpdateLock) {
				UpdateLock.notify();
			}
		}
	}

	class Announce implements Runnable {
		private volatile Thread announcethread;
		private String name;
		private byte announcetype;
		private boolean announceslow;
		private int announceslowcount;

		public Announce(String name, byte type) {
			this.name = name;
			this.announcetype = type;
			announceslow = false;
		}

		public void slowannounce() {
			announceslow = true;
		}

		public void noslowannounce() {
			announceslow = false;
		}

		public void start() {
			announcethread = new Thread(this, name);
			announcethread.start();
		}

		public void stop() {
			announcethread = null;
		}

		public void run() {
			byte[] announcepk = null;
			if (announcetype == TYPE_ANNOUNCE) {
				announcepk = new byte[43];
				for (int i = 0; i < 8; i++)
					announcepk[i + 2] = (byte) 0xFF;
				for (int i = 0; i < firmware.getplatforms().length && i < 8; i++)
					announcepk[i + 2] = firmware.getplatforms()[i];
				Serializer serial = new LittleSerializer(announcepk, 10);
				for (int i = 0; i < firmware.getversions().length && i < 8; i++)
					serial.packU_INT32(firmware.getversions()[i]);
				announcepk[42] = firmware.getimagetype();
			} else if (announcetype == TYPE_COMPLETE) {
				announcepk = new byte[1];
			} else if (announcetype == TYPE_RESTART) {
				announcepk = new byte[1];
			}
			announcepk[0] = announcetype;
			Thread thisThread = Thread.currentThread();
			while (announcethread == thisThread) {
				try {
					Iterator<GateWay> iter = gatewaylist.iterator();
					while (iter.hasNext()) {
						GateWay gateway = iter.next();
						if (announcetype == TYPE_ANNOUNCE) {
							announcepk[1] = (byte) fwdcount;
						}
						if (!announceslow || announceslowcount == 6) {
							announceslowcount = 0;
							softbase.nwk.send(null, announcepk, (char) 0, OSSServices.OSS_SERVICE_SMOTA2,
							        SoftbaseNWK.Protocol_Simple_BCast, gateway);
						}
						Thread.sleep(30000 / gatewaylist.size());
					}
					announceslowcount++;
				} catch (InterruptedException e) {
					break;
				}
			}
			log.info("Announce Finish");
		}
	}

	public byte getServiceID() {
		return OSSServices.OSS_SERVICE_SMOTA2;
	}

}
