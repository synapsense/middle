package com.synapsense.plugin.wsn.p3;

import java.util.concurrent.BlockingQueue;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.plugin.wsn.iocore.frames.out.MoteUARTOutSetParameterFrame;
import com.synapsense.plugin.wsn.softbase29.Softbase;
//import org.apache.commons.logging.Log;
//import org.apache.commons.logging.LogFactory;
//import com.synapsense.plugin.wsn.msgbus.MsgDuoConfig;
//import com.synapsense.plugin.wsn.msgbus.MsgDuoData;
//import com.synapsense.plugin.wsn.msgbus.MsgJawaConfig;
//import com.synapsense.plugin.wsn.msgbus.MsgJawaData;
//import com.synapsense.plugin.wsn.p3.DuoConfiguration;
//import com.synapsense.plugin.wsn.p3.JawaConfiguration;
//import com.synapsense.plugin.wsn.p3.P3ConfigException;
//import com.synapsense.plugin.wsn.softbase29.packet.MultiMessage;
//import com.synapsense.plugin.wsn.softbase29.packet.RadioMsg;

// need these?
//import com.synapsense.plugin.wsn.softbase29.state.NodeInfoEntry;
//import com.synapsense.plugin.wsn.util.Constants;
//import com.synapsense.plugin.wsn.util.serializer.BigSerializer;

/*
 * all threads (one for each SC ) write to shared queue
 * this pulls from queue at regular intervals and sends over WSN
 */

public class JawaConfigSpooler implements Runnable {

	private Softbase softbase;
	private final BlockingQueue<MoteUARTOutSetParameterFrame> queue;
	public static final int DELAY = 5000;
	// public static final String STOP_MSG = "EndOfImport"; // TODO this doesn't
	// really work
	// private Boolean done = false; //TODO not using this really
	private static Log log = LogFactory.getLog(JawaConfigSpooler.class);

	public JawaConfigSpooler(Softbase softbase, BlockingQueue<MoteUARTOutSetParameterFrame> q) {
		this.softbase = softbase;
		queue = q;
		log.trace("JawaConfigSpooler started ...");
	}

	public void run() {
		try {
			while (true) {
				MoteUARTOutSetParameterFrame frame = queue.take();

				// byte[] content = frame.getExtData();
				log.trace("JawaConfigSpooler took a frame and sending it look out now ...");
				// String contentString = "   Content =";
				// for (int i = 0; i < content.length; i++)
				// contentString = contentString + " "+content[i];
				// log.trace(".."+contentString);
				softbase.getCommandQ().offer(frame);
				try {
					Thread.sleep(DELAY);
				} catch (InterruptedException e) {
					log.warn("Interrupted while waiting before sending JawaConfig frame", e);
				}
			}
		} catch (InterruptedException ex) {
		}
	}

	// public Boolean isDone() {
	// return this.done;
	// }

}
