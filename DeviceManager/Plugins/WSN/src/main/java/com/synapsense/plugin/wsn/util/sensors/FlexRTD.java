package com.synapsense.plugin.wsn.util.sensors;

/**
 * The RTD sensor attached to 4-20mA by constellation/btu node
 */

class FlexRTD extends Generic420 implements RangeConversionInterface {

	public int getMoteDataType() {
		return 33;
	}

}
