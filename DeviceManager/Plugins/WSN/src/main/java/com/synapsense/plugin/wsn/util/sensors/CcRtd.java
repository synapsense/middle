package com.synapsense.plugin.wsn.util.sensors;

import com.synapsense.plugin.wsn.util.SensorStates;

// A constant current platinum RTD

public class CcRtd implements SensorConversionInterface {

	public double convertFromRaw(char raw) {
		// voltage is measured in 12bits ADCCounts
		// 0 -> 0V
		// 4095 -> 2.5V
		double rawV = (double) raw;
		double volts = (double) rawV / 4095 * 2.5;

		double R0 = 100;
		double A = 3.9083e-3;
		double B = -5.775e-7;

		double R = ((volts / 7.24)) / 0.001;

		double tempC = -1
		        / 2
		        * (R0 * A + Math.pow((Math.pow(R0, 2) * Math.pow(A, 2) + 4 * R0 * B * R - 4 * Math.pow(R0, 2) * B),
		                (1 / 2)) / R0 / B);

		// variable resistor is used to measure temperature
		// volts measurments are taken from variable resistor

		double tempF;

		try {

			tempF = (9.0 / 5.0) * tempC + 32;

		} catch (Exception e) {
			tempF = 0;
		}
		if (tempF < -200) {
			return SensorStates.SENSOR_DISCONNECT;
		}
		if (tempF > 10000) {
			return SensorStates.SENSOR_SERVICE;
		}
		return tempF;

	}

	public double convertFromRawCompensated(char raw, double vRef) {

		return convertFromRaw(raw);
	}

	public int getMoteDataType() {
		return 22;
	}

}
