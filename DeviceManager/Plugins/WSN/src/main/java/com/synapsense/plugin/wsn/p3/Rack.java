package com.synapsense.plugin.wsn.p3;

public class Rack implements Comparable<Rack> {

	public int getRack() {
		return rack;
	}

	public int getHeight() {
		return height;
	}

	public int getRpdudelta() {
		return rpdudelta;
	}

	private int rack;
	private int height;
	private int rpdudelta;

	public Rack(byte[] data) {
		this.rack = data[0] & 0xff;
		this.height = data[1] & 0xff;
		this.rpdudelta = data[2] & 0xff;
	}

	public byte[] pack() {
		byte[] data = new byte[3];
		data[0] = (byte) rack;
		data[1] = (byte) height;
		data[2] = (byte) rpdudelta;
		return data;
	}

	public int compareTo(Rack rhs) {
		return rack - rhs.rack;
	}

}
