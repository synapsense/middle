package com.synapsense.plugin.wsn.p3;

public class Server implements Comparable<Server> {
	private int uPos;
	private int uHeight;
	private int rack;
	private double demandPower;
	private int rawDemandPower;

	int getRawDemandPower() {
		return rawDemandPower;
	}

	public Server(byte[] data) {
		int pos = 0;
		rawDemandPower = (data[pos++] & 0xff) << 8;
		rawDemandPower += (data[pos++] & 0xff);

		demandPower = rawDemandPower / 1000.0;

		uPos = data[pos++] & 0xFF;
		uHeight = data[pos++] & 0xFF;
		rack = ((data[pos] >> 6) & 0x3);
		// feed = ((data[6] >> 3) & 0x7);
		// phase = data[6] & 0x3;
	}

	public byte[] pack() {
		byte[] data = new byte[5];
		int pos = 0;
		data[pos++] = (byte) (rawDemandPower >> 8);
		data[pos++] = (byte) rawDemandPower;
		data[pos++] = (byte) uPos;
		data[pos++] = (byte) uHeight;
		data[pos++] = (byte) (rack << 6);
		return data;
	}

	public int getuPos() {
		return uPos;
	}

	public int getuHeight() {
		return uHeight;
	}

	public int getRack() {
		return rack;
	}

	/**
	 * 
	 * @return Demand Power of this server in kilowatthours
	 */
	public double getDemandPower() {
		return demandPower;
	}

	public int compareTo(Server rhs) {
		if (this.rack != rhs.rack) {
			return this.rack - rhs.rack;
		}

		if (this.uPos != rhs.uPos) {
			return this.uPos - rhs.uPos;
		}
		return 0;
	}

}
