package com.synapsense.plugin.wsn.servers;

import java.io.Console;
import java.io.IOException;
import java.io.PrintStream;
import java.net.Socket;

import com.synapsense.plugin.wsn.util.Constants;

public class CommandNetwork {
	private Socket socket;

	public static void main(String[] args) {
		CommandNetwork rn = new CommandNetwork();
		if (args.length < 1 || args.length > 1) {
			System.err.println("Usage: CommandNetwork [IP address]");
			System.exit(-1);
		}
		rn.ui(args[0]);
	}

	public void ui(String ipstring) {
		try {
			socket = new Socket(ipstring, Constants.COMMANDSERVER_PORT);
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(-1);
		}
		PrintStream out = null;
		try {
			out = new PrintStream(socket.getOutputStream());
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(-1);
		}
		Console console = System.console();

		while (true) {
			System.out.print("Enter objectID (0: quit): ");
			String objectID = console.readLine();
			if (objectID.equals("0"))
				break;
			out.println(objectID);
			System.out.print("Enter property: ");
			String propertyname = console.readLine();
			out.println(propertyname);
			System.out.print("Enter value: ");
			String value = console.readLine();
			out.println(value);
		}
		out.close();
		try {
			socket.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}