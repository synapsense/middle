package com.synapsense.plugin.wsn.util.sensors;

public class Sensor {

	public int sensorPhysicalType;
	public int sensorChannel;
	public double latestValue;

	public Sensor(int chan, int phys, double value) {
		this.sensorPhysicalType = phys;
		this.sensorChannel = chan;
		this.latestValue = value;
	}

}
