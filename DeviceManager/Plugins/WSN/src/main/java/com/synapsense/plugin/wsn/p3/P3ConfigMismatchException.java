package com.synapsense.plugin.wsn.p3;

public class P3ConfigMismatchException extends P3ConfigException {

	public P3ConfigMismatchException(String msg) {
		super(msg);
	}
}
