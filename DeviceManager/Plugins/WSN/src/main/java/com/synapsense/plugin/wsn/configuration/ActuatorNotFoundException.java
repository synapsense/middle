package com.synapsense.plugin.wsn.configuration;

public class ActuatorNotFoundException extends Exception {

	private static final long serialVersionUID = -5908813613426710034L;

	public ActuatorNotFoundException(String desc) {
		super(desc);
	}

	public ActuatorNotFoundException(String desc, Throwable cause) {
		super(desc, cause);
	}
}
