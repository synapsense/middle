package com.synapsense.plugin.wsn.configuration;

/**
 * Interface that all configurable objects must conform to. Generally used as
 * type safety over plain old Object.
 */
public interface ConfigurationObject {
}
