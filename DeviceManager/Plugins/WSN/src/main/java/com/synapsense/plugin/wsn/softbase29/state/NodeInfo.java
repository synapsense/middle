package com.synapsense.plugin.wsn.softbase29.state;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.plugin.wsn.WsnConstants;
import com.synapsense.plugin.wsn.configuration.Configuration;
import com.synapsense.plugin.wsn.configuration.WSNNode;
import com.synapsense.plugin.wsn.msgbus.MsgRouteInformation;
import com.synapsense.plugin.wsn.servers.UpdateServerThread;
import com.synapsense.plugin.wsn.softbase29.GateWay;
import com.synapsense.plugin.wsn.softbase29.Softbase;
import com.synapsense.plugin.wsn.softbase29.SoftbaseNWK;
import com.synapsense.plugin.wsn.softbase29.packet.OSSDataPacket;
import com.synapsense.plugin.wsn.util.serializer.LittleSerializer;
import com.synapsense.plugin.wsn.util.serializer.Serializer;
import com.synapsense.util.algo.Pair;

public class NodeInfo {

	private ArrayList<NodeInfoEntry> nodes = new ArrayList<NodeInfoEntry>();
	private Map<Character, NodeInfoEntry> nodesById = new HashMap<Character, NodeInfoEntry>();

	private Softbase softbase;

	private static Log log = LogFactory.getLog(NodeInfo.class);

	public NodeInfo(Softbase softbase) {
		this.softbase = softbase;
		clear();
		restoreRoutes();
	}

	public synchronized NodeInfoEntry addNode(char id) {

		for (NodeInfoEntry n : nodes) {
			if (n.getId() == id) {
				return n;
			}
		}

		NodeInfoEntry ninfo = new NodeInfoEntry();
		ninfo.setId(id);

		ninfo.setParent(null);
		ninfo.setPktCount(0);
		ninfo.setReported(false);

		nodes.add(ninfo);
		nodesById.put(id, ninfo);

		return ninfo;
	}

	public synchronized void reset(char id) {
		NodeInfoEntry ninfo = getById(id);
		if (ninfo != null) {
			ninfo.reset();
		}
	}

	public synchronized NodeInfoEntry getById(char id) {
		return nodesById.get(id);
	}

	public int size() {
		return nodes.size();
	}

	public synchronized NodeInfoEntry get(int index) {
		if (index >= nodes.size() || index < 0)
			return null;
		return nodes.get(index);
	}

	public void print() {
		log.trace("Node Information");

		for (NodeInfoEntry ninfo : nodes) {
			if (ninfo.getParent() == null) {
				if (ninfo.getParentgw() != null)
					log.trace(Integer.toHexString(ninfo.getId()) + " " + ninfo.getParentgw().getPortname());
				else
					log.trace(Integer.toHexString(ninfo.getId()));
			} else {
				log.trace(Integer.toHexString(ninfo.getId()) + " " + Integer.toHexString(ninfo.getParent().getId()));
			}
		}
	}

	public synchronized void setParent(char childid, char parentid) {
		NodeInfoEntry childnode = getById(childid);
		if (childnode == null)
			return;
		childnode.setParentgw(null);
		childnode.setParent(getById(parentid));
	}

	public synchronized void setParentGW(char childid, GateWay parentgw) {
		NodeInfoEntry childnode = getById(childid);
		if (childnode == null)
			return;
		childnode.setParentgw(parentgw);
		childnode.setParent(null);
	}

	/**
	 * Returns a list of the logical IDs traversed to reach destid
	 * 
	 * @param destid
	 *            The logical ID destination
	 * @return The list of intermediate devices.
	 */
	public synchronized Pair<GateWay, Pair<Character, List<Character>>> getPath(char destid) {
		ArrayList<Character> path = new ArrayList<Character>();
		NodeInfoEntry node = getById(destid);

		if (node == null)
			return null;

		NodeInfoEntry parent = node.getParent();
		GateWay parentgw = node.getParentgw();
		int hop = 0;

		while (parent != null) {
			if (hop >= (SoftbaseNWK.MAXHOP - 1))
				return null;
			path.add(node.getId());
			node = parent;
			parentgw = parent.getParentgw();
			parent = parent.getParent();
			hop++;
		}
		if (parentgw == null)
			return null;
		Pair<Character, List<Character>> list = new Pair<Character, List<Character>>(node.getId(), path);
		return new Pair<GateWay, Pair<Character, List<Character>>>(parentgw, list);
	}

	/**
	 * Return the amount of data required to store this path entry in a packet
	 * 
	 * @param pathlen
	 * @return
	 */
	public int pathSizeRequired(int pathlen) {
		return pathlen * 2;
	}

	public synchronized GateWay buildPath(char destid, OSSDataPacket pk) {
		Pair<GateWay, Pair<Character, List<Character>>> pathinfo = getPath(destid);
		if (pathinfo == null)
			return null;

		List<Character> path = pathinfo.getSecond().getSecond();
		GateWay parentgw = pathinfo.getFirst();

		pk.path = new byte[pathSizeRequired(path.size())];
		Serializer serial = new LittleSerializer(pk.path);
		Iterator<Character> iter = path.iterator();

		while (iter.hasNext()) {
			serial.packU_INT16(iter.next());
		}

		pk.mac_destaddr = pathinfo.getSecond().getFirst();
		return parentgw;
	}

	public synchronized void reportPathInfo(char id) {
		NodeInfoEntry ninfo = getById(id);
		if (ninfo != null) {
			MsgRouteInformation frame = new MsgRouteInformation();
			frame.childid = new ArrayList<Character>();
			frame.parentid = new ArrayList<Character>();
			while (ninfo.getParentgw() == null) {
				if (ninfo.getParent() == null)
					return;
				frame.childid.add(ninfo.getId());
				frame.parentid.add(ninfo.getParent().getId());
				ninfo = ninfo.getParent();
			}
			frame.childid.add(ninfo.getId());
			frame.parentid.add(ninfo.getParentgw().getNodeId());
			softbase.postMessage(frame);
		}
	}

	public synchronized void clear() {
		nodes.clear();
		nodesById.clear();
	}

	public synchronized void updateReset() {
		for (NodeInfoEntry ninfo : nodes) {
			ninfo.setCanHear(false);
			ninfo.setHearFrom((char) 0xFFFF);
			ninfo.setFordward(false);
			ninfo.setUpdateDone(false);
			ninfo.setReported(false);
		}
	}

	public synchronized boolean allNodesReported() {
		for (NodeInfoEntry ninfo : nodes) {
			if (!ninfo.isReported())
				return false;
		}
		return true;
	}

	public synchronized int getReportedCount() {
		int reportedcount = 0;
		for (NodeInfoEntry ninfo : nodes) {
			if (ninfo.isReported())
				reportedcount++;
		}
		return reportedcount;
	}

	public synchronized void reported(char id) {
		NodeInfoEntry ninfo = getById(id);
		if (ninfo != null) {
			ninfo.setReported(true);
		}

	}

	public synchronized void printNotJoinedNodes(UpdateServerThread updateserver) {
		log.info("These nodes did not join");
		updateserver.println("These nodes did not join");
		for (NodeInfoEntry ninfo : nodes) {
			if (!ninfo.isReported()) {
				WSNNode node = Configuration.getNode(softbase.getNetId(), ninfo.getId());
				if (node == null) {
					log.error("Cannot find node " + (int) ninfo.getId());
					continue;
				}
				log.info(Long.toHexString(node.getPhysID()).toUpperCase());
				updateserver.println(Long.toHexString(node.getPhysID()).toUpperCase());
			}
		}
	}

	public synchronized void setCanHear(char id, char parentid) {
		NodeInfoEntry ninfo = getById(id);
		if (ninfo != null) {
			ninfo.setCanHear(true);
			ninfo.setHearFrom(parentid);

		}
	}

	public synchronized boolean getCanHear(char id) {
		NodeInfoEntry ninfo = getById(id);
		if (ninfo != null) {
			return ninfo.canHear();
		}
		return false;
	}

	public synchronized int canHearCount() {
		int count = 0;
		for (NodeInfoEntry ninfo : nodes) {
			if (ninfo.canHear())
				count++;
		}
		return count;
	}

	public synchronized boolean getFwd(char id) {
		NodeInfoEntry ninfo = getById(id);
		if (ninfo != null) {
			return ninfo.isForward();
		}
		return false;
	}

	public void setFwd(char id, boolean fwd) {
		NodeInfoEntry ninfo = getById(id);
		if (ninfo != null) {
			ninfo.setFordward(fwd);
		}
	}

	public synchronized char findNextFwdNode(LinkedList<Character> nodelist) {
		for (NodeInfoEntry ninfo : nodes) {
			if (ninfo.getParent() == null)
				log.trace("Checking node " + (int) ninfo.getId() + " Hear " + ninfo.canHear());
			else
				log.trace("Checking node " + (int) ninfo.getId() + " Hear " + ninfo.canHear() + "  Parent: "
				        + (int) ninfo.getParent().getId());
			if (ninfo.isReported() && !ninfo.canHear() && ninfo.getParent() != null
			        && nodelist.contains(ninfo.getParent().getId())) {
				nodelist.remove(new Character(ninfo.getParent().getId()));
				return ninfo.getParent().getId();
			}
		}
		return 0;
	}

	public synchronized boolean allCanHear() {
		for (NodeInfoEntry ninfo : nodes) {
			if (ninfo.isReported() && !ninfo.canHear())
				return false;
		}
		return true;
	}

	public synchronized void printCannotHear(UpdateServerThread updateserver) {
		boolean first = true;
		for (NodeInfoEntry ninfo : nodes) {
			if (ninfo.isReported() && !ninfo.canHear()) {
				if (first) {
					log.info("These nodes are not part of the tree");
					updateserver.println("These nodes are not part of the tree");
					first = false;
				}
				WSNNode node = Configuration.getNode(softbase.getNetId(), ninfo.getId());
				log.info(Long.toHexString(node.getPhysID()).toUpperCase());
				updateserver.println(Long.toHexString(node.getPhysID()).toUpperCase());
			}
		}
	}

	public synchronized void printFwdNode() {
		boolean first = true;
		for (NodeInfoEntry ninfo : nodes) {
			if (ninfo.isForward()) {
				if (first) {
					log.info("Forwarding nodes");
					first = false;
				}
				WSNNode node = Configuration.getNode(softbase.getNetId(), ninfo.getId());
				log.info(Long.toHexString(node.getPhysID()).toUpperCase());
			}
		}
	}

	public synchronized void setDone(char id) {

		NodeInfoEntry ninfo = getById(id);

		if (ninfo != null) {
			if (!ninfo.isUpdateDone())
				log.info((int) id + " is done.");
			ninfo.setUpdateDone(true);
			return;
		}
		log.info((int) id + " is done, but not registered.");
	}

	public synchronized int getUpdateDoneCount() {
		int count = 0;
		for (NodeInfoEntry ninfo : nodes) {
			if (ninfo.canHear() && ninfo.isUpdateDone())
				count++;
		}
		return count;
	}

	public synchronized boolean isAllReportedUpdateDone() {
		for (NodeInfoEntry ninfo : nodes) {
			if (ninfo.canHear() && !ninfo.isUpdateDone())
				return false;
		}
		return true;
	}

	public synchronized boolean isAllFwdNodeDone() {
		for (NodeInfoEntry ninfo : nodes) {
			if (ninfo.isForward() && !ninfo.isUpdateDone())
				return false;
		}
		return true;
	}

	public synchronized boolean isAllUpdateDone() {
		for (NodeInfoEntry ninfo : nodes) {
			if (!ninfo.isUpdateDone())
				return false;
		}
		return true;
	}

	public void printUpdateFailedNodes(UpdateServerThread updateserver) {
		log.info("Failed nodes");
		updateserver.println("Failed nodes");
		for (int i = 0; i < nodes.size(); i++) {
			NodeInfoEntry ninfo = nodes.get(i);
			if (!ninfo.isUpdateDone()) {
				WSNNode node = Configuration.getNode(softbase.getNetId(), ninfo.getId());
				log.info(Long.toHexString(node.getPhysID()).toUpperCase());
				updateserver.println(Long.toHexString(node.getPhysID()).toUpperCase());
			}
		}
	}

	public void printUpdateResult(UpdateServerThread updateserver) {
		log.info(nodes.size() + " nodes are in the network");
		updateserver.println(nodes.size() + " nodes are in the network");
		int success = 0;
		int reported = 0;
		int canhear = 0;
		for (int i = 0; i < nodes.size(); i++) {
			NodeInfoEntry ninfo = nodes.get(i);
			if (ninfo.isUpdateDone())
				success++;
			if (ninfo.isReported())
				reported++;
			if (ninfo.canHear())
				canhear++;
		}
		log.info(reported + " nodes reported.");
		updateserver.println(reported + " nodes reported.");
		log.info(canhear + " nodes were part of the distribution tree.");
		updateserver.println(canhear + " nodes were part of the distribution tree.");
		log.info(success + " nodes succeeded.");
		updateserver.println(success + " nodes succeeded.");
		log.info((nodes.size() - success) + " nodes failed.");
		updateserver.println((nodes.size() - success) + " nodes failed.");
		if (nodes.size() - success > 0)
			printUpdateFailedNodes(updateserver);
	}

	public synchronized void resetFailedNodes() {
		for (NodeInfoEntry ninfo : nodes) {
			if (!ninfo.isUpdateDone()) {
				ninfo.setReported(false);
				ninfo.setCanHear(false);
			}
		}
	}

	private void restoreRoutes() {
		for (WSNNode node : Configuration.getNodes()) {

			String routeParentId = node.getRouteParentId();
			if (routeParentId != null && !routeParentId.isEmpty()) {
				WSNNode parentNode = Configuration.getNodeByObjectID(routeParentId);
				if (parentNode != null) {
					char childid = (char) node.getLogicalID();
					char parentid = (char) parentNode.getLogicalID();
					NodeInfoEntry childnode = getById(childid);
					NodeInfoEntry parentnode = getById(parentid);
					if (childnode == null) {
						addNode(childid);
					}
					if (parentnode == null) {
						addNode(parentid);
					}

					if (parentNode.getId().startsWith(WsnConstants.TYPE_NODE)) {
						setParent(childid, parentid);
					} else if (parentNode.getId().startsWith(WsnConstants.TYPE_GATEWAY)) {
						for (com.synapsense.plugin.wsn.softbase.GateWay gw : softbase.getGateways()) {
							if (parentid == gw.getNodeId()) {
								setParentGW(childid, (GateWay) gw);
							}
						}
					}
				}
			}
		}

	}
}
