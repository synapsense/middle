package com.synapsense.plugin.wsn.softbase29.state;

public class NeighborInfo {

	public static final int Neighbor_Normal = 1;
	public static final int Neighbor_Dead = 2;
	public static final int Neighbor_Unreachable = 3;
	public static final int Neighbor_OnlyReach = 4;
	public static final int Neighbor_Known = 5;

	private char id;
	private char slots;
	private int hopcount;
	private int hopcountTM;
	private int state;
	public final int[] rssi = new int[4];
	private int rssiindex;
	private byte RadioCh;
	private long rcvdtime;
	private int rate;
	/* Channel adaptation */
	public final char chDuration[] = new char[4];
	public char fillindex;
	public byte curmaxlevel;
	/* Powered Router */
	private int RouterState;
	private int ParentPowerState;
	/* Prefered Gateway System */
	private int preferredBase;
	private int secondaryBase;
	private int hopcount_secondaryBase;

	public void setId(char id) {
		this.id = id;
	}

	public char getId() {
		return id;
	}

	public void setSlots(char slots) {
		this.slots = slots;
	}

	public char getSlots() {
		return slots;
	}

	public void setHopCount(int hopcount) {
		this.hopcount = hopcount;
	}

	public int getHopCount() {
		return hopcount;
	}

	public void setHopCountTM(int hopcountTM) {
		this.hopcountTM = hopcountTM;
	}

	public int getHopCountTM() {
		return hopcountTM;
	}

	public void setState(int state) {
		this.state = state;
	}

	public int getState() {
		return state;
	}

	public void setRadioCh(byte radioCh) {
		RadioCh = radioCh;
	}

	public byte getRadioCh() {
		return RadioCh;
	}

	public void setRcvdTime(long rcvdtime) {
		this.rcvdtime = rcvdtime;
	}

	public long getRcvdTime() {
		return rcvdtime;
	}

	public void setRate(int rate) {
		this.rate = rate;
	}

	public int getRate() {
		return rate;
	}

	public void setRouterState(int routerState) {
		RouterState = routerState;
	}

	public int getRouterState() {
		return RouterState;
	}

	public void setParentPowerState(int parentPowerState) {
		ParentPowerState = parentPowerState;
	}

	public int getParentPowerState() {
		return ParentPowerState;
	}

	public void setPreferredBase(int preferredBase) {
		this.preferredBase = preferredBase;
	}

	public int getPreferredBase() {
		return preferredBase;
	}

	public void setSecondaryBase(int secondaryBase) {
		this.secondaryBase = secondaryBase;
	}

	public int getSecondaryBase() {
		return secondaryBase;
	}

	public void setHopCountSecondaryBase(int hopcount_secondaryBase) {
		this.hopcount_secondaryBase = hopcount_secondaryBase;
	}

	public int getHopCountSecondaryBase() {
		return hopcount_secondaryBase;
	}

	public int getRssiindex() {
		return rssiindex;
	}

	public void setRssiindex(int rssiindex) {
		this.rssiindex = rssiindex;
	}
}
