package com.synapsense.plugin.wsn.msgbus;

import com.synapsense.plugin.wsn.configuration.ConfigurationObject;
import com.synapsense.plugin.wsn.configuration.WSNNode;

public class MsgResendAppInit extends Message {
	public WSNNode node;
	public ConfigurationObject cObj;

	public MsgResendAppInit(WSNNode node) {
		this.node = node;
	}

	public MsgResendAppInit(WSNNode node, ConfigurationObject obj) {
		this.node = node;
		this.cObj = obj;
	}
}
