package com.synapsense.plugin.wsn.msgbus;

public class MsgNwkStat extends Message {

	public final int NWKSTAT_HOUR = 1;

	// Network statistics parameters
	// ritu changes .. for powRouter
	public int routerst_parentst_hopcnt;
	public int hopcount;
	public int routerstate;
	public int parentstate;

	// Network statistics parameters
	public char deviceId;
	public int type;
	public long stattimestamp;
	public long rcvdtimestamp;

	// NWK Stat
	public int sendfailSQ;
	public int sendfailNR;
	public int tbretrycount;
	public int fwdpkdrop;
	public int parentfailcount;
	public int tsretrycount;
	public int tspkcount;
	public int tbpkcount;
	public int fwdpkcount;

	// MAC Stat
	public int pksentcount;
	public long radiouptime;
	public int freeslotcount;
	public int wrongslotcount;
	public int totalslotcount;
	public int pkrcvdcount;
	public int crcfailpkcount;
	public int[] chstatus;

	// Time Sync Stat
	public int timelostcountsync;
	public int timelostcounttimeout;

	// Noise Info
	public int noisethresh;

	// Base specific network statistics
	public int duppkcount;
	public int outoforderpkcount;

	public MsgNwkStat() {
		super();
		chstatus = new int[16];
	}

	public String toString() {
		StringBuffer buf = new StringBuffer();
		buf.append("\nNWK Statistics for " + Long.toHexString(deviceId).toUpperCase());
		if (deviceId < 0xF000) {
			buf.append("\nSend Failed send queue full: " + sendfailSQ);
			buf.append("\nSend Failed No route: " + sendfailNR);
			buf.append("\nOne hop retransmission: " + tbretrycount);
			buf.append("\nForwarding Packet Dropped: " + fwdpkdrop);
			buf.append("\nParent failure: " + parentfailcount);
			buf.append("\nBroadcast retransmission: " + tsretrycount);
			buf.append("\nPacket from the base: " + tspkcount);
			buf.append("\nPacket sent to base: " + tbpkcount);
			buf.append("\nPacket forwarded: " + fwdpkcount);
			buf.append("\nPacket Sent: " + pksentcount);
			buf.append("\nRadio was on for: " + radiouptime);
			buf.append("\nFree Slots: " + freeslotcount);
			buf.append("\nWrong wakeup Slots: " + wrongslotcount);
			buf.append("\nTotal wakeup Slots: " + totalslotcount);
			buf.append("\nPacket Received: " + pkrcvdcount);
			buf.append("\nCRC Failed Packet Received: " + crcfailpkcount);
			buf.append("\n");
			for (int i = 0; i < 16; i++) {
				buf.append(" CH " + (i + 11) + ": " + chstatus[i]);
			}
			buf.append("\nTime lost due to out of sync: " + timelostcountsync);
			buf.append("\nTime lost due to Timeout: " + timelostcounttimeout);
			// ritu changes .. for powRouter
			// buf.append("\nHopcount: "+hopcount);
			buf.append("\nHopcount: " + hopcount + " routerState: " + routerstate + " parentstate: " + parentstate);
			buf.append("\nNoise Threshold: " + noisethresh);

		} else {
			buf.append("\nPacket from node: " + tbpkcount);
			buf.append("\nDuplicate Packet: " + duppkcount);
			buf.append("\nOut of order Packet: " + outoforderpkcount);
			buf.append("\nPacket Sent: " + pksentcount);
			buf.append("\nFree Slots: " + freeslotcount);
			buf.append("\nWrong wakeup Slots: " + wrongslotcount);
			buf.append("\nTotal wakeup Slots: " + totalslotcount);
			buf.append("\nPacket Received: " + pkrcvdcount);
			buf.append("\nCRC Failed Packet Received: " + crcfailpkcount);
			buf.append("\n");
			for (int i = 0; i < 16; i++) {
				buf.append(" CH " + (i + 11) + ": " + chstatus[i]);
			}
			buf.append("\nTime lost due to out of sync: " + timelostcountsync);
			buf.append("\nTime lost due to Timeout: " + timelostcounttimeout);
			buf.append("\nNoise Threshold: " + noisethresh);
		}
		return buf.toString();
	}

}
