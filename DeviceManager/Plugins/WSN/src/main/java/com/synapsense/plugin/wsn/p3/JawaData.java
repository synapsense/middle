package com.synapsense.plugin.wsn.p3;

/**
 * Data is a holder for a single smartplug data point
 */
public class JawaData {

	public double demandPower; /* 15 min demand power */
	public double avgCurrent;
	public double maxCurrent;

	public long physicalId; /* Node ID */
	public int rack;
	public int feed;
	public int phase;

	public long jid;
	public long timestamp;

	public String toString() {
		return "<Data node " + Long.toHexString(physicalId) + " jawa " + Long.toHexString(jid) + " rack " + rack
		        + " feed " + feed + " phase " + phase + " avgC " + avgCurrent + " maxCur " + maxCurrent + " energy "
		        + demandPower + " >";

	}

}
