package com.synapsense.plugin.wsn.util.serializer;

import java.util.ArrayList;
import java.util.List;

/**
 * Class which takes a byte[] and can pack and unpack common C types. Note that
 * this class will promote the C type to one larger of the Java type for correct
 * operation in signedness dependant code. I.e.: uint8_t -> Java char (16bit).
 * uint16_t -> Java char (16bit) because char is unsigned already :) uint32_t ->
 * Java long (64bit) int64_t -> Java long uint64_t unsupported (use int64_t)
 * 
 * @see com.synapsense.util.tests.SerializerTest
 * 
 * @author Yann
 * 
 */
public class BigSerializer implements Serializer {

	private byte[] data;
	private int position = 0;

	private List<Byte> overflow = new ArrayList<Byte>();

	private void addByte(byte b) {
		if (data.length <= position)
			overflow.add(b);
		data[position++] = b;
	}

	public BigSerializer(byte[] d) {
		data = d;
		position = 0;
	}

	public BigSerializer(byte[] d, int initial) {
		position = initial;
		data = d;
	}

	public int unpackU_INT8() {
		int c = data[position] & 0xFF;
		position++;
		return c;
	}

	public void packU_INT8(byte b) {
		addByte(b);
	}

	public void copyData(byte[] data) {
		for (byte b : data)
			packU_INT8(b);
	}

	public int unpack_INT8() {
		int c = data[position];
		position++;
		return c;
	}

	public char unpackU_INT16() {
		char r;
		r = (char) ((int) (data[position] & 0xFF) << 8);
		r |= (int) data[position + 1] & 0xFF;

		position += 2;
		return r;
	}

	public void packU_INT16(char d) {
		addByte((byte) (d >> 8));
		addByte((byte) d);
	}

	public int unpackU_INT24() {
		int r;
		r = (int) (((int) data[position] & 0xFF) << 16);
		r |= (int) (((int) data[position + 1] & 0xFF) << 8);
		r |= (int) (((int) data[position + 2] & 0xFF));
		position += 3;
		return r;
	}

	public long unpackU_INT48() {
		long r;
		r = (long) (((long) data[position] & 0xFF) << 40);
		r |= (long) (((long) data[position + 1] & 0xFF) << 32);
		r |= (long) (((long) data[position + 2] & 0xFF) << 24);
		r |= (long) (((long) data[position + 3] & 0xFF) << 16);
		r |= (long) (((long) data[position + 4] & 0xFF) << 8);
		r |= (long) (((long) data[position + 5] & 0xFF));
		position += 6;
		return r;
	}

	public long unpackU_INT32() {
		long r;
		r = (long) (((long) data[position] & 0xFF) << 24);
		r |= (long) (((long) data[position + 1] & 0xFF) << 16);
		r |= (long) (((long) data[position + 2] & 0xFF) << 8);
		r |= (long) (((long) data[position + 3] & 0xFF));
		position += 4;
		return r;
	}

	public void packU_INT32(long l) {
		addByte((byte) (l >>> 24));
		addByte((byte) (l >>> 16));
		addByte((byte) (l >>> 8));
		addByte((byte) l);
	}

	public long unpack_INT64() {
		long r;
		r = (long) (((long) data[position]) << 56); // don't mask!! signed!!!
		r |= (long) (((long) data[position + 1] & 0xFF) << 48);
		r |= (long) (((long) data[position + 2] & 0xFF) << 40);
		r |= (long) (((long) data[position + 3] & 0xFF) << 32);
		r |= (long) (((long) data[position + 4] & 0xFF) << 24);
		r |= (long) (((long) data[position + 5] & 0xFF) << 16);
		r |= (long) (((long) data[position + 6] & 0xFF) << 8);
		r |= (long) ((long) data[position + 7] & 0xFF);
		position += 8;

		return r;
	}

	public void pack_INT64(long l) {
		addByte((byte) (l >>> 56));
		addByte((byte) (l >>> 48));
		addByte((byte) (l >>> 40));
		addByte((byte) (l >>> 32));
		addByte((byte) (l >>> 24));
		addByte((byte) (l >>> 16));
		addByte((byte) (l >>> 8));
		addByte((byte) (l));

	}

	public void setData(byte[] data) {
		this.data = data;
		position = 0;
		this.overflow.clear();
	}

	public byte[] getData() {
		/* Capture overflow data */
		if (overflow.size() > 0) {
			byte[] newdata = new byte[data.length + overflow.size()];
			System.arraycopy(data, 0, newdata, 0, data.length);

			int pos = data.length;

			for (byte b : overflow) {
				newdata[pos++] = b;
			}

			this.data = newdata;
			position = newdata.length;
			overflow.clear();

		}
		return data;
	}

	public byte[] unpack_bytes(int len) {
		byte[] get = new byte[len];
		System.arraycopy(data, position, get, 0, len);
		position += len;
		return get;
	}

	public byte[] unpack_remaining_bytes() {
		byte[] remain = new byte[data.length - position];
		System.arraycopy(data, position, remain, 0, data.length - position);
		return remain;
	}

	public int getPosition() {
		return position;
	}

	public void setPosition(int position) {
		this.position = position;
	}

	public boolean hasBytes() {
		return data.length - position >= 1;
	}
}
