package com.synapsense.plugin.wsn.knownprops;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

public class KnownPropsStore {
	private static Collection<KnownPropertyWorker> workers;

	static {
		workers = new ArrayList<KnownPropertyWorker>();
		workers.add(new NodeSamplingInterval());
		workers.add(new NodeReset());
		workers.add(new NodeIdentify());
		workers.add(new NodeSendData());
		workers.add(new ActuatorSetParam());
		workers.add(new SensorSmartSend2());
	}

	public static boolean setPropertyValue(String objectID, String propName, Serializable value) {
		for (KnownPropertyWorker worker : workers) {
			if (worker.processMessage(objectID, propName, value))
				return true;
		}
		return false;
	}
}
