package com.synapsense.plugin.wsn.softbase29.packet;

import java.util.Map;
import java.util.TreeMap;

import org.apache.log4j.Logger;

public class MultiMessage {
	private int slots = -1;
	private int lastMagic = -1;
	private long lastUpdated;
	private Map<Integer, byte[]> data;

	private static Logger log = Logger.getLogger(MultiMessage.class);

	public static final int MM_PAYLOAD = 80;

	private static final long MM_TIMEOUT = 1000 * 60 * 10; /* 10 minutes */

	public MultiMessage() {
		log.trace("New MultiMessage");
		data = new TreeMap<Integer, byte[]>();
		lastUpdated = System.currentTimeMillis();
	}

	public boolean isTimedOut() {
		return (lastUpdated + MM_TIMEOUT) < System.currentTimeMillis();
	}

	public boolean isComplete() {
		log.trace("MM size " + data.size() + " for slots " + slots);
		if (data.size() == slots) {
			log.trace("Is complete");
			return true;
		}
		log.trace("Is not complete");
		return false;
	}

	public int getMagic() {
		return lastMagic;
	}

	public byte[] getData() {
		if (!isComplete())
			return null;

		log.trace("Reassembling message");
		int totalbytes = 0;
		for (int i = 0; i < slots; i++) {
			totalbytes = totalbytes + data.get(i).length;
		}
		byte[] assembly = new byte[totalbytes];
		int count = 0;
		for (int i = 0; i < slots; i++) {
			byte[] d = data.get(i);
			System.arraycopy(d, 0, assembly, count, d.length);
			count += d.length;
		}

		return assembly;
	}

	public boolean isPartOf(byte[] d) {
		int slot = d[0] & 0xff;
		// int maxslot = d[1] & 0xff;
		int magic = d[2] & 0xff;

		if (lastMagic == -1)
			return false;
		if (magic != lastMagic)
			return false;
		if (slot >= slots)
			return false;
		return true;
	}

	public boolean addMessage(byte[] d) {
		int slot = d[0] & 0xff;
		int maxslot = d[1] & 0xff;
		int magic = d[2] & 0xff;

		if (lastMagic == -1) {
			lastMagic = magic;
		} else if (magic != lastMagic) {
			return false; /* Not this message? */
		}

		this.slots = maxslot;

		if (slot >= slots) {
			log.trace("This MM is full?");
			return false;
		}

		log.trace("Adding segment to MM with magic " + lastMagic);
		log.trace("slot " + slot + " out of " + maxslot + " for magic " + magic);

		lastUpdated = System.currentTimeMillis();

		byte[] body = new byte[d.length - 3];
		System.arraycopy(d, 3, body, 0, d.length - 3);

		data.put(slot, body);
		return true;
	}

	public String toString() {
		return "<MM Magic " + lastMagic + ">";
	}

}
