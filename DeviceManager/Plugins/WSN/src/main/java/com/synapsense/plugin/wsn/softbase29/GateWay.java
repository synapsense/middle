package com.synapsense.plugin.wsn.softbase29;

import java.util.LinkedList;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.plugin.wsn.configuration.Configuration;
import com.synapsense.plugin.wsn.configuration.WSNGW;
import com.synapsense.plugin.wsn.iocore.IMoteComm;
import com.synapsense.plugin.wsn.iocore.IoFactory;
import com.synapsense.plugin.wsn.iocore.MoteCommWriter;
import com.synapsense.plugin.wsn.iocore.frames.MoteUARTFrame;
import com.synapsense.plugin.wsn.iocore.frames.MoteUARTMsgAckFrame;
import com.synapsense.plugin.wsn.iocore.frames.out.MoteUARTOutFrame;
import com.synapsense.plugin.wsn.iocore.n29.frames.MoteUARTRcvdRadioFrame;
import com.synapsense.plugin.wsn.iocore.n29.frames.out.MoteUARTOutBaseCommandFrame;
import com.synapsense.plugin.wsn.iocore.n29.frames.out.MoteUARTOutPingFrame;
import com.synapsense.plugin.wsn.iocore.n29.frames.out.MoteUARTOutSendRadioFrame;
import com.synapsense.plugin.wsn.iocore.n29.frames.out.MoteUARTOutStartFrame;
import com.synapsense.plugin.wsn.softbase29.packet.OSSBeaconPacket;
import com.synapsense.plugin.wsn.softbase29.packet.OSSDataPacket;
import com.synapsense.plugin.wsn.softbase29.state.OSSNHT;

public class GateWay implements com.synapsense.plugin.wsn.softbase.GateWay {
	class PingTimer extends TimerTask {
		@Override
		public void run() {
			MoteUARTOutPingFrame pingframe = new MoteUARTOutPingFrame();
			if (!sendFrame(pingframe)) {
				log.error(portname + " No Ping response");
				setAlive(false);
			}
			delayPing();
		}
	}

	class RadioTimeout extends TimerTask {
		@Override
		public void run() {
			if (curpk == null)
				return;
			MoteUARTOutBaseCommandFrame frame = new MoteUARTOutBaseCommandFrame(
					MoteUARTOutBaseCommandFrame.RADIOSENDSTATUS);
			if (sendFrame(frame)) {
				if (radiosendtimeoutcount > (RADIO_SEND_TIMEOUT / RADIO_TIMEOUT)) {
					log.error(portname + " not sending packet. Reset");
					frame = new MoteUARTOutBaseCommandFrame(
							MoteUARTOutBaseCommandFrame.RESET);
					sendFrame(frame);
					setAlive(false);
				} else {
					scheduleRadioTimeout();
				}
			} else {
				log.error(portname + " Timeout Sending Packet");
				if (radiotimeoutcount > RADIO_MAX_TIMEOUT_COUNT) {
					setAlive(false);
				} else {
					radiotimeoutcount++;
					MoteUARTRcvdRadioFrame doneframe = new MoteUARTRcvdRadioFrame(
							MoteUARTFrame.TYPE_RADIODONE, curpk, portname);
					if (!inQ.offer(doneframe)) {
						log.error("QUEUE FULL: Incoming gateway queue full");
					}
				}
			}
		}
	}

	class SendStatus_timer_h extends TimerTask {
		@Override
		public void run() {
			sendStatus();
		}
	}

	class TSRetransmit_timer_h extends TimerTask {
		@Override
		public void run() {
			log.trace(portname + " TS:RETRY");
			sendRadioPacket(TSpk);
			if (TSpk.Sendcount < SoftbaseNWK.MaxTSRetry) {
				TSRetransmit_timer_id = new TSRetransmit_timer_h();
				Random rand = new Random();
				long delay = (90 + (rand.nextInt(90)))
						* softbase.getMACInterval();
				timer.schedule(TSRetransmit_timer_id, delay);
			} else {
				log.trace(portname + " TS:DONE");
				TSActive = false;
			}
		}
	}

	private static Log log = LogFactory.getLog(GateWay.class);
	private static final int MAXSENDFRAMEDELAY = 30000;

	private static final int PINGDELAY = 120000;

	public static final int RADIO_TIMEOUT = 3000;
	private static final int RADIO_MAX_TIMEOUT_COUNT = 5;
	private static final int RADIO_SEND_TIMEOUT = 5 * 60000;
	private static final int SENDFAIL_MAX = 10;
	private static final int SENDSTATUS_INTERVAL = 5 * 60000;
	public static final long STARTSENDDELAY = 5000;
	private static final int MAX_SENDMSG = 50;

	private boolean alive;
	private IMoteComm comm;
	private OSSDataPacket curpk;
	// For NWK stat
	public int duppkcount;
	private int hopcountTM;
	private BlockingQueue<MoteUARTFrame> inQ;
	private OSSNHT nht;
	private char nodeid;
	private long nwkstarttime;
	private int oldStatus = 0xFF; // previous status value... 0xFF is never set
									// in normal operation (only 3 bits are
									// used)
	public int outoforderpkcount;
	private LinkedBlockingQueue<MoteUARTOutFrame> outQ;
	private TimerTask pingtimer_id;
	private String portname;
	private TimerTask Radio_timeout_id;
	private byte RadioCh;
	private boolean radiosending;
	private int sendfailcount;
	private LinkedList<OSSDataPacket> sendmsgQ;
	private long sendstatus_senttime = 0;
	private TimerTask sendstatus_timer_id;
	private Softbase softbase;

	private long startsenttime;
	public int tbpkcount;
	private boolean timemaster;

	private Timer timer;
	private boolean TimeSynced;
	public boolean TSActive;
	private OSSDataPacket TSpk;
	private TimerTask TSRetransmit_timer_id;
	private int radiotimeoutcount;
	private int radiosendtimeoutcount;

	GateWay() {

	}

	public GateWay(WSNGW gateway, BlockingQueue<MoteUARTFrame> inQ,
			char nodeid, Softbase softbase) throws GWInstantiationException {
		this.inQ = inQ;
		this.outQ = new LinkedBlockingQueue<MoteUARTOutFrame>();
		sendmsgQ = new LinkedList<OSSDataPacket>();
		nht = new OSSNHT();
		this.nodeid = nodeid;
		this.softbase = softbase;
		this.portname = gateway.getPortName();
		timer = new Timer("GateWay Timer-" + portname);
		init();
		sendstatus_timer_id = new SendStatus_timer_h();
		timer.schedule(sendstatus_timer_id, SENDSTATUS_INTERVAL);
		try {
			comm = IoFactory.generateIo(gateway, inQ, outQ, 29);
		} catch (Exception e) {
			log.error("Failed opening port :" + portname);
			throw new GWInstantiationException("Failed opening port :"
					+ portname, e);
		}
	}

	public void beaconSend(int hopcountTM) {
		nht.beaconSend();
		if (this.hopcountTM != hopcountTM) {
			MoteUARTOutBaseCommandFrame frame = new MoteUARTOutBaseCommandFrame(
					MoteUARTOutBaseCommandFrame.SETHOPCOUNTTM, this.hopcountTM);
			sendFrame(frame);
		}
	}

	public synchronized void delayPing() {
		if (pingtimer_id != null)
			pingtimer_id.cancel();
		pingtimer_id = new PingTimer();
		timer.schedule(pingtimer_id, PINGDELAY);
	}

	public OSSNHT getNHT() {
		return nht;
	}

	@Override
	public char getNodeId() {
		return nodeid;
	}

	public long getNwkstarttime() {
		return nwkstarttime;
	}

	@Override
	public String getPortname() {
		return portname;
	}

	public byte getRadioCh() {
		return RadioCh;
	}

	public int getSendMsgQueueSize() {
		return sendmsgQ.size();
	}

	/**
	 * @return a bit mask, representing the state of the gateway: 0 - Alive 1 -
	 *         TimeMaster 2 - TimeSync
	 */
	public int getStatusMask() {
		int mask = 0;
		if (isAlive())
			mask += 1;
		if (isTimemaster())
			mask += 2;
		if (isTimeSynced())
			mask += 4;
		return mask;
	}

	public synchronized void init() {
		timemaster = false;
		radiosending = false;
		TimeSynced = false;
		TSActive = false;
		curpk = null;
		alive = false;
		statReset();
		if (Radio_timeout_id != null) {
			Radio_timeout_id.cancel();
			Radio_timeout_id = null;
		}
		if (TSRetransmit_timer_id != null)
			TSRetransmit_timer_id.cancel();
		clearQueue();
		sendfailcount = 0;
		hopcountTM = OSSNHT.MAXHOPTM;
		nht.clear();
		// setAlive(true);
	}

	public boolean isAlive() {
		return alive;
	}

	public boolean isTimemaster() {
		return timemaster;
	}

	public boolean isTimeSynced() {
		return TimeSynced;
	}

	public void processBeacon(OSSBeaconPacket pkt) {
		nht.process(pkt);
		if (!timemaster) {
			int hopcountTM = nht.getHopCountTM();
			if ((hopcountTM != OSSNHT.MAXHOPTM)
					&& (this.hopcountTM != hopcountTM)) {
				this.hopcountTM = hopcountTM;
				MoteUARTOutBaseCommandFrame frame = new MoteUARTOutBaseCommandFrame(
						MoteUARTOutBaseCommandFrame.SETHOPCOUNTTM,
						this.hopcountTM);
				sendFrame(frame);
			}
		}
	}

	public synchronized void clearQueue() {
		outQ.clear();
	}

	public void recvAck(MoteUARTMsgAckFrame message) {
		comm.recvAck(message);
	}

	public synchronized OSSDataPacket sendDone(MoteUARTRcvdRadioFrame message) {
		if (curpk == null || message.pkt == null)
			return null;
		OSSDataPacket retpk;
		// DSN is generated from the gateway. It needs to be copied for checking
		// equality
		curpk.mac_dsn = message.pkt.mac_dsn;
		if (message.recvdframe) {
			if (!curpk.equals(message.pkt))
				return null;
			retpk = curpk;
			retpk.mac_ack = message.pkt.mac_ack;
			radiotimeoutcount = 0;
			radiosendtimeoutcount = 0;
		} else {
			retpk = (OSSDataPacket) message.pkt;
		}
		if (Radio_timeout_id != null) {
			Radio_timeout_id.cancel();
			Radio_timeout_id = null;
		}
		curpk = null;
		radiosending = false;
		if (sendmsgQ.size() > 0) {
			OSSDataPacket pk = sendmsgQ.removeFirst();
			log.trace(portname + " sending next packet from Q remaining "
					+ sendmsgQ.size());
			sendRadioPacket(pk);
		}
		return retpk;
	}

	public synchronized boolean sendFrame(MoteUARTOutFrame frame) {
		// Frame cannot be send to dead gateway
		// Only start frame can be sent
		if (!isAlive() && !(frame instanceof MoteUARTOutStartFrame)
				&& !(frame instanceof MoteUARTOutPingFrame)) {
			log.trace(portname + " Cannot send frame to dead gateway");
			return false;
		}
		frame.setFramesent(false);
		frame.setAckset(false);
		if (frame.getType() == MoteUARTOutFrame.TYPE_BASECOMMAND)
			log.trace(portname + " Send Frame " + (int) (frame.getType()) + " Command: " + (frame.getFullFrame()[4] & 0xFF));
		else
			log.trace(portname + " Send Frame " + (int) (frame.getType()));

		/* Try to queue outgoing frame */
		if (!outQ.offer(frame)) {
			log.error("QUEUE FULL: Gateway outgoing queue is full");
			return false;
		}

		long starttime = System.currentTimeMillis();
		while (true) {
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
			}
			if (System.currentTimeMillis() - starttime > MAXSENDFRAMEDELAY) {
				outQ.remove(frame);
				log.error(portname + " Timeout sending frame "
						+ (int) (frame.getType()));
				setAlive(false);
				return false;
			}
			if (frame.isFramesent())
				break;
		}
		log.trace(portname + " Sent Frame " + (int) (frame.getType()));
		starttime = System.currentTimeMillis();
		while (true) {
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
			}
			if (System.currentTimeMillis() - starttime > MoteCommWriter.MAX_RETRY_COUNT
					* MoteCommWriter.TIMEOUT_MSGACK_RECV_MAX)
				break;
			if (frame.isAckset())
				break;
		}
		log.trace(portname + " Recv ACK Frame " + (int) (frame.getType()));
		return frame.isAck();
	}

	public synchronized boolean sendRadioPacket(OSSDataPacket pk) {
		pk.Sendcount++;
		if (radiosending) {
			if (!sendmsgQ.contains(pk)) {
				if (sendmsgQ.size() >= MAX_SENDMSG) {
					log.trace(portname + " send Q full");
					return false;
				}
				sendmsgQ.addLast(pk);
				log.trace(portname + " added to send Q. remaining "
						+ sendmsgQ.size());
			} else
				log.trace(portname + " already in send Q");
		} else {
			curpk = pk;
			if (!alive) {
				log.trace(portname + " DEAD gateway cannot send");
				pk.mac_ack = 0;
				MoteUARTRcvdRadioFrame doneframe = new MoteUARTRcvdRadioFrame(
						MoteUARTFrame.TYPE_RADIODONE, pk, portname);
				if (!inQ.offer(doneframe)) {
					log.error("QUEUE FULL: Incoming gateway queue is full");
				}
				return false;
			} else {
				/* Use only broadcast slot for retransmission */
				char slot = OSSNHT.BROADCASTSLOT;
				if (pk.Sendcount < 2)
					slot = nht.getSlotInfo(curpk.mac_destaddr);
				byte radioch = nht.getRadioChannelInfo(curpk.mac_destaddr);
				char chDuration[] = nht
						.getNeighborChannelDuration(curpk.mac_destaddr);
				byte curmaxlevel = nht.getCurMaxLevel(curpk.mac_destaddr);
				char fillindex = nht.getFillIndex(curpk.mac_destaddr);
				MoteUARTOutSendRadioFrame frame = new MoteUARTOutSendRadioFrame(
						curpk.serialize(), slot, radioch, chDuration,
						curmaxlevel, fillindex);
				if (!sendFrame(frame)) {
					log.trace(portname + " Error Sending Packet");
					pk.mac_ack = 0;
					MoteUARTRcvdRadioFrame doneframe = new MoteUARTRcvdRadioFrame(
							MoteUARTFrame.TYPE_RADIODONE, pk, portname);

					if (!inQ.offer(doneframe)) {
						log.error("QUEUE FULL: Incoming gateway queue is full");
					}

					if (sendfailcount > SENDFAIL_MAX) {
						log.trace("Failed max number of packet sending");
						setAlive(false);
					} else
						sendfailcount++;
				} else {
					sendfailcount = 0;
					radiosending = true;
					if (Radio_timeout_id != null)
						Radio_timeout_id.cancel();
					Radio_timeout_id = new RadioTimeout();
					timer.schedule(Radio_timeout_id, RADIO_TIMEOUT);
				}
			}
		}
		return true;
	}

	public synchronized void sendStart(int panid) {
		if (System.currentTimeMillis() - startsenttime > STARTSENDDELAY) {
			MoteUARTOutStartFrame startframe = new MoteUARTOutStartFrame(panid);
			sendFrame(startframe);
			startsenttime = System.currentTimeMillis();
		}
	}

	private synchronized void sendStatus() {

		try {
			if (sendstatus_timer_id != null) {
				sendstatus_timer_id.cancel();
			}
		} catch (Exception e) {
			log.warn("TimerTask Illegal State mitigated - why?", e);
		}

		sendstatus_timer_id = new SendStatus_timer_h();
		timer.schedule(sendstatus_timer_id, SENDSTATUS_INTERVAL);
		com.synapsense.plugin.wsn.configuration.WSNNode node = Configuration
				.getNode(softbase.getNetId(), nodeid);
		if (node == null) {
			log.warn("Cannot determine GW physical ID. Logical-to-Physical maping is not set ("
					+ portname + ")");
			return;
		}

		int status = getStatusMask();
		if ((status == oldStatus)
				&& ((System.currentTimeMillis() - sendstatus_senttime) < SENDSTATUS_INTERVAL))
			return;

		log.info(portname + " GW status has been changed from "
				+ Long.toHexString(oldStatus) + " to "
				+ Long.toHexString(status));
		oldStatus = status;
		sendstatus_senttime = System.currentTimeMillis();
		GatewayStatusQueue.sendStatus(node.getId(), status);
	}

	public synchronized void setAlive(boolean alive) {
		if (!alive) {
			log.error(portname + " is DEAD");
			if (Radio_timeout_id != null) {
				Radio_timeout_id.cancel();
				Radio_timeout_id = null;
			}
			if (TSRetransmit_timer_id != null)
				TSRetransmit_timer_id.cancel();
			if (curpk != null) {
				MoteUARTRcvdRadioFrame doneframe = new MoteUARTRcvdRadioFrame(
						MoteUARTFrame.TYPE_RADIODONE, curpk, portname);
				if (!inQ.offer(doneframe)) {
					log.error("QUEUE FULL: Incoming gateway queue full");
				}
				curpk = null;
			}
			setTimeSynced(false);
		}
		if (!this.alive && alive) {
			log.info(portname + " is alive");
			curpk = null;
			radiosending = false;
			if (sendmsgQ.size() > 0) {
				OSSDataPacket pk = sendmsgQ.removeFirst();
				log.trace(portname + " sending next packet from Q remaining "
						+ sendmsgQ.size());
				sendRadioPacket(pk);
			}
			radiotimeoutcount = 0;
			radiosendtimeoutcount = 0;
		}
		if (!alive) {
			comm.reconnect();
		}
		this.alive = alive;
		sendStatus();
	}

	public void setHopCountTM(int hopcountTM) {
		if (timemaster && hopcountTM != 0)
			return;
		this.hopcountTM = hopcountTM;
	}

	@Override
	public void setNodeid(char nodeid) {
		this.nodeid = nodeid;
	}

	public void setNwkstarttime(long nwkstarttime) {
		this.nwkstarttime = nwkstarttime;
	}

	public void setRadioCh(byte radioCh) {
		RadioCh = radioCh;
	}

	public void setTimemaster(boolean timemaster) {
		if (timemaster) {
			TimeSynced = true;
			hopcountTM = 0;
		} else {
			TimeSynced = false;
			hopcountTM = OSSNHT.MAXHOPTM;
		}
		this.timemaster = timemaster;
		sendStatus();
	}

	public synchronized void setTimeSynced(boolean timeSynced) {
		if (timeSynced)
			log.trace(portname + " is Time synced");
		else
			log.trace(portname + " is NOT Time synced");
		TimeSynced = timeSynced;
		sendStatus();
	}

	@Override
	public void start() {
		comm.startUpdating(true);
		comm.startReading(true);

		delayPing();
	}

	public synchronized void startToSensors(OSSDataPacket pk, int ToSensorsSeqno) {
		if (!alive)
			return;
		TSActive = true;
		// Each gateway needs to maintain its own packet
		TSpk = new OSSDataPacket(pk);
		Random rand = new Random();
		TSRetransmit_timer_id = new TSRetransmit_timer_h();
		if (timemaster) {
			sendRadioPacket(TSpk);
			long delay = (90 + (rand.nextInt(90))) * softbase.getMACInterval();
			timer.schedule(TSRetransmit_timer_id, delay);
		} else {
			long delay = (rand.nextInt(90)) * softbase.getMACInterval();
			timer.schedule(TSRetransmit_timer_id, delay);
		}
		MoteUARTOutBaseCommandFrame frame = new MoteUARTOutBaseCommandFrame(
				MoteUARTOutBaseCommandFrame.SETTOSENSORSSEQNO, ToSensorsSeqno);
		sendFrame(frame);
	}

	public void statReset() {
		duppkcount = 0;
		outoforderpkcount = 0;
		tbpkcount = 0;
	}

	public void stop() {
		timer.cancel();
		comm.stop();
	}

	private synchronized void scheduleRadioTimeout() {
		if (Radio_timeout_id != null)
			Radio_timeout_id.cancel();
		Radio_timeout_id = new RadioTimeout();
		radiosendtimeoutcount++;
		timer.schedule(Radio_timeout_id, RADIO_TIMEOUT);
	}

	@Override
	public String toString() {
		return portname;
	}
}
