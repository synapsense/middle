package com.synapsense.plugin.wsn.msgbus;

public class MsgJawaData extends Message {

	public byte[] data;

	public char deviceId;
	public long timestamp;

	public MsgJawaData() {
		super();
	}

	public MsgJawaData(byte[] d, char deviceId, long time) {
		super();
		this.deviceId = deviceId;
		this.data = d;
		this.timestamp = time;
	}

}