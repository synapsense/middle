package com.synapsense.plugin.wsn.softbase29.service;

public interface Service {

	public void close();

	public byte getServiceID();

	// public void packetSent(byte[] data, char dest, boolean ack);
}
