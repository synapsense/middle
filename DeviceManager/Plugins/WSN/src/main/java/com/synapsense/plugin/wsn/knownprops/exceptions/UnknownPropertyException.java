package com.synapsense.plugin.wsn.knownprops.exceptions;

public class UnknownPropertyException extends Exception {
	public UnknownPropertyException(String desc) {
		super(desc);
	}

	public UnknownPropertyException(String desc, Throwable reason) {
		super(desc, reason);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 8104229939342491008L;

}
