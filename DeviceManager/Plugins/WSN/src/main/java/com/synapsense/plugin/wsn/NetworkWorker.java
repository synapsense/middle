/**
 *
 */
package com.synapsense.plugin.wsn;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.log4j.Logger;

import com.synapsense.plugin.wsn.msgbus.Message;
import com.synapsense.plugin.wsn.msgbus.MsgDuoConfig;
import com.synapsense.plugin.wsn.msgbus.MsgDuoData;
import com.synapsense.plugin.wsn.msgbus.MsgJawaConfig;
import com.synapsense.plugin.wsn.msgbus.MsgJawaData;
import com.synapsense.plugin.wsn.msgbus.MsgNHTInfo;
import com.synapsense.plugin.wsn.msgbus.MsgNodeInfo;
import com.synapsense.plugin.wsn.msgbus.MsgNwkStat;
import com.synapsense.plugin.wsn.msgbus.MsgRouteInformation;
import com.synapsense.plugin.wsn.msgbus.MsgSensorData;
import com.synapsense.plugin.wsn.msgbus.MsgTimesyncInformation;
import com.synapsense.plugin.wsn.softbase.Softbase;
import com.synapsense.plugin.wsn.storers.StoreNHTInfo;
import com.synapsense.plugin.wsn.storers.StoreNwkStatistics;
import com.synapsense.plugin.wsn.storers.StorePowerData;
import com.synapsense.plugin.wsn.storers.StorePowerRackConf;
import com.synapsense.plugin.wsn.storers.StoreRoutingInfo;
import com.synapsense.plugin.wsn.storers.StoreSensor;
import com.synapsense.plugin.wsn.storers.StoreSensorNode;
import com.synapsense.plugin.wsn.storers.StoreStandalone;
import com.synapsense.plugin.wsn.util.Constants;
import com.synapsense.plugin.wsn.util.SensorNetwork;
import com.synapsense.plugin.wsn.util.timesync.TimeSync;

public class NetworkWorker implements Runnable {

	private Logger log = Logger.getLogger(NetworkWorker.class);

	private SensorNetwork sensorNetwork;

	private TimeSync times = new TimeSync();

	public BlockingQueue<Message> messageQ = new LinkedBlockingQueue<Message>(
	        Constants.BLOCKINGQUEUE_MAXSIZE_BEFOREBLOCK);
	public BlockingQueue<Message> commandQ = new LinkedBlockingQueue<Message>(
	        Constants.BLOCKINGQUEUE_MAXSIZE_BEFOREBLOCK);

	private Thread softbaseThread;
	private Thread owningThread;
	private Softbase softbase;
	private boolean standalone;
	private boolean addnodeonthefly;

	public NetworkWorker(SensorNetwork net) throws Exception {
		this.sensorNetwork = net;
		log.info("Starting Softbased-backed NetworkWorker ");
		this.standalone = false;
		this.addnodeonthefly = false;
	}

	public NetworkWorker(SensorNetwork net, boolean standalone, boolean addnodeonthefly) throws Exception {
		this.sensorNetwork = net;
		log.info("Starting Stand-alone Softbased-backed NetworkWorker ");
		this.standalone = standalone;
		this.addnodeonthefly = addnodeonthefly;
	}

	public void run() {

		log.info("Network version is reported as " + sensorNetwork.getVersion());
		try {
			if (sensorNetwork.getVersion().equals("N29")) {
				log.info("Launching softbase29");
				softbase = new com.synapsense.plugin.wsn.softbase29.Softbase(sensorNetwork, messageQ, commandQ);
			} else {
				log.error("Unknown network version");
				return;
			}
			this.softbaseThread = new Thread((Runnable) softbase, "Softbase-"
			        + Integer.toString(sensorNetwork.getNetworkId()));
			this.softbaseThread.start();
		} catch (Exception e) {
			log.error("Failed to create softbase thread for network " + sensorNetwork.getNetworkId(), e);
			return;
		}

		log.info("Network " + sensorNetwork.getNetworkId() + " is Starting.");
		boolean interrupted = false;

		// never call this method directly. this hack is used to obtain
		// reference to the executor thread.
		owningThread = Thread.currentThread();

		while (!interrupted) {

			try {
				Message message = messageQ.take();
				// TODO: Refactor storestandalone?
				if (message instanceof MsgNodeInfo) {
					log.info("Meta Info: " + message.toString());
					if (standalone) {
						StoreStandalone.store(message, sensorNetwork, times, addnodeonthefly);
					} else
						StoreSensorNode.store(message, sensorNetwork);
				} else if (message instanceof MsgSensorData) {
					log.debug("Sensor Type DATA received");
					if (standalone) {
						StoreStandalone.store(message, sensorNetwork, times, addnodeonthefly);
					} else {
						StoreSensor.store(message, sensorNetwork, times);
					}
				} else if (message instanceof MsgNwkStat) {
					if (log.isDebugEnabled())
						log.debug("NWKSTAT: " + message.toString());
					if (standalone) {
						StoreStandalone.store(message, sensorNetwork, times, addnodeonthefly);
					} else
						StoreNwkStatistics.store(message, sensorNetwork, times);
				} else if (message instanceof MsgRouteInformation) {
					log.debug("ROUTE: " + message.toString());
					if (standalone) {
						StoreStandalone.store(message, sensorNetwork, times, addnodeonthefly);
					} else
						StoreRoutingInfo.storeRoutingInfo((MsgRouteInformation) message, sensorNetwork);
				} else if (message instanceof MsgNHTInfo) {
					if (log.isDebugEnabled())
						log.debug("NHT Info: " + message.toString());
					if (standalone) {
						StoreStandalone.store(message, sensorNetwork, times, addnodeonthefly);
					} else
						StoreNHTInfo.store((MsgNHTInfo) message, sensorNetwork, times);
				} else if (message instanceof MsgTimesyncInformation) {

					log.debug("Received timesync packet from gateway");
					MsgTimesyncInformation f = (MsgTimesyncInformation) message;
					if (log.isDebugEnabled()) {
						log.debug("System time now is: " + Long.toString(System.currentTimeMillis()));
						log.debug("Convert sent mote to my time is "
						        + times.convertTime(f.globaltime, times.systemTime()));
					}
					// TODO: Fix this for better latency
					times.processMsg(f.globaltime, System.currentTimeMillis());

				} else if (message instanceof MsgJawaConfig || message instanceof MsgDuoConfig) {
					log.debug("Power RACK config received");
					StorePowerRackConf.store(message, sensorNetwork);
				} else if (message instanceof MsgJawaData || message instanceof MsgDuoData) {
					log.debug("Power DATA");
					StorePowerData.store(message, sensorNetwork, times);
				} else {
					log.warn("Unprocessed Message Type in NetworkWorker");
				}
			} catch (InterruptedException e) {
				interrupted = true;
			} catch (Exception e) {
				log.error("Got exception while processing message", e);
			}
		}
	}

	public Softbase getSoftbase() {
		return softbase;
	}

	public void close() {
		softbaseThread.interrupt();
		softbase.close();

		if (owningThread != null) {
			owningThread.interrupt();
		}
	}
}
