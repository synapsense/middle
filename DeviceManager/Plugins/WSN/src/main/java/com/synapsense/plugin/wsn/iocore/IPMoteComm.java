package com.synapsense.plugin.wsn.iocore;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.security.NoSuchAlgorithmException;
import java.security.Security;
import java.util.concurrent.BlockingQueue;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.plugin.wsn.configuration.WSNGW;
import com.synapsense.plugin.wsn.iocore.frames.MoteUARTFrame;
import com.synapsense.plugin.wsn.iocore.frames.MoteUARTMsgAckFrame;
import com.synapsense.plugin.wsn.iocore.frames.out.MoteUARTOutFrame;

public class IPMoteComm implements IMoteComm, IRestartable {

	private static Log log = LogFactory.getLog(IPMoteComm.class);
	private static boolean bouncyAdded = false;
	private static final int CONNECT_TIMEOUT = 1000 * 30; // 30 seconds

	private InputStream inputStream;
	private OutputStream outputStream;
	private final BlockingQueue<MoteUARTFrame> sensorQueue;
	private final BlockingQueue<MoteUARTOutFrame> commandQueue;
	private Socket socket;

	private byte[] encKey = new byte[16];
	private boolean encryption;
	private byte[] iv;

	private Thread readThread = null;
	private Thread writeThread = null;
	private MoteCommWriter writer = null;

	private int frameVersion = 29;

	private Supervisor supervisor;
	private String ipAddress;
	private int tcpPort = 10001;

	private final WSNGW gateway;

	public IPMoteComm(WSNGW gateway, BlockingQueue<MoteUARTFrame> _q,
			BlockingQueue<MoteUARTOutFrame> _qo, int version)
			throws IOException {

		this.gateway = gateway;
		ipAddress = gateway.getAddress();

		if (ipAddress.contains(":")) {
			if (ipAddress.indexOf(":") == ipAddress.lastIndexOf(":")) {
				// Only one colon appears in the address. IPv4 address with port
				tcpPort = Integer.parseInt(ipAddress.substring(ipAddress
						.lastIndexOf(":") + 1));
				ipAddress = ipAddress.substring(0, ipAddress.lastIndexOf(":"));
				log.info("Parsing address as IPv4/Host " + ipAddress
						+ " and port " + tcpPort);
			} else if (ipAddress.contains("]:") && ipAddress.startsWith("[")) {
				// IPv6 address with port
				tcpPort = Integer.parseInt(ipAddress.substring(ipAddress
						.lastIndexOf(":") + 1));
				ipAddress = ipAddress.substring(1,
						ipAddress.lastIndexOf(":") - 1);
				log.info("Parsing address as IPv6/Host " + ipAddress
						+ " and port " + tcpPort);
			} else {
				// No port is specified for IPv6 address
				log.info("Parsing address as IPv6 " + ipAddress
						+ " with no port");
			}
		} else {
			// No port is specified for IPv4 address or host name
			log.info("Parsing address as IPv4/Host " + ipAddress
					+ " with no port");
		}

		this.frameVersion = version;

		if (gateway.getKey() != null && !gateway.getKey().equals("")) {
			setupSecurity(gateway);
			encryption = true;
		} else {
			encryption = false;
		}

		sensorQueue = _q;
		commandQueue = _qo;
		supervisor = new Supervisor(this, gateway.getPortName());

		try {
			connectPort();
		} catch (IOException e) {
			log.error(
					"REMOTE GATEWAY: Couldn't connect to remote gateway due to error.",
					e);
			supervisor.ConnectError();
		}
	}

	private void setupSecurity(WSNGW gateway) {
		String key = gateway.getKey();
		if (!bouncyAdded) {
			Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
			bouncyAdded = true;
		}

		for (int i = 0; i < 32; i += 2) {
			char a = key.charAt(i);
			char b = key.charAt(i + 1);
			String keystr = String.valueOf(a) + String.valueOf(b);
			int keyv = Integer.parseInt(keystr, 16);
			encKey[i / 2] = (byte) keyv;

		}

		String keystr = "";
		for (byte kb : encKey) {
			keystr += Integer.toHexString(0xff & kb) + " ";
		}

		log.debug("Parsed key: " + keystr);

		try {
			KeyGenerator kgen = KeyGenerator.getInstance("AES");
			kgen.init(128);
			SecretKey sk = kgen.generateKey();
			iv = sk.getEncoded();
		} catch (NoSuchAlgorithmException e) {
			log.error("Can't create an AES key generator at 128 bits.", e);
			return;
		}

		keystr = "";

		for (byte kb : iv) {
			keystr += Integer.toHexString(0xff & kb) + " ";
		}

		log.debug("Generated IV: " + keystr);
	}

	public void connectPort() throws IOException {

		socket = new Socket();

		socket.connect(new InetSocketAddress(ipAddress, tcpPort),
				CONNECT_TIMEOUT);

		socket.setTcpNoDelay(true);
		socket.setKeepAlive(true);
		socket.setTrafficClass(0x10); // Low delay traffic class

		inputStream = socket.getInputStream();
		outputStream = socket.getOutputStream();
		log.info("Connected to " + socket.getRemoteSocketAddress());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsense.synapstore.iocore.IMoteComm#startReading(boolean)
	 */
	public void startReading(boolean thread) {

		MoteCommReader reader;
		if (encryption) {
			reader = new SecureMoteCommReader(sensorQueue, inputStream,
					gateway.getPortName(), this, iv, encKey, frameVersion);
		} else {
			reader = new MoteCommReader(sensorQueue, inputStream,
					gateway.getPortName(), this, frameVersion);
		}
		if (thread) {
			readThread = new Thread(reader, "IP-MoteCommReader-"
					+ this.ipAddress);
			readThread.setDaemon(true);
			readThread.start();
		} else {
			reader.run();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsense.synapstore.iocore.IMoteComm#startUpdating(boolean)
	 */
	public void startUpdating(boolean thread) {

		if (encryption) {
			writer = new SecureMoteCommWriter(commandQueue, outputStream, this,
					iv, encKey);
		} else {
			writer = new MoteCommWriter(commandQueue, outputStream, this);
		}

		if (thread) {
			writeThread = new Thread(writer, "IP-MoteCommWriter-"
					+ this.ipAddress);
			writeThread.setDaemon(true);
			writeThread.start();
		} else {
			writer.run();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.synapsense.synapstore.iocore.IMoteComm#recvAck(com.synapsense.synapstore
	 * .iocore.frames.MoteUARTMsgAckFrame)
	 */
	public void recvAck(MoteUARTMsgAckFrame ack) {
		writer.writeAck(ack);
	}

	/**
	 * Wait for a thread to finish, time out after "ms" milliseconds
	 * 
	 * @param th
	 *            Thread to wait for
	 * @param ms
	 *            Timeout.
	 * @return
	 */
	private boolean waitForThreadMS(Thread th, long ms) {
		if (th == null)
			return true;

		while (th.isAlive()) {
			ms -= 100;
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				return !th.isAlive();
			}
			if (ms <= 0)
				return !th.isAlive();
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsense.synapstore.iocore.IMoteComm#finish()
	 */
	public void finish() {

		if (readThread != null) {
			readThread.interrupt();
		}

		waitForThreadMS(readThread, 5000);

		if (writeThread != null) {
			writeThread.interrupt();
		}

		waitForThreadMS(writeThread, 5000);

		if (inputStream != null) {
			try {
				inputStream.close();
			} catch (IOException e) {
				log.warn("On finish of input, exception occurred: ", e);
			}
			inputStream = null;
		}

		if (outputStream != null) {
			try {
				outputStream.close();
			} catch (IOException e) {
				log.warn("On finish of output, exception occurred: ", e);
			}
			outputStream = null;
		}

		try {
			if (socket != null)
				socket.close();
			socket = null;

		} catch (IOException e) {
			log.info("Couldn't close socket - probably already dead", e);
		}
	}

	public Supervisor getSupervisor() {
		return supervisor;
	}

	public void stop() {
		supervisor.stop();
		finish();
	}

	public String getPortName() {
		return gateway.getPortName();
	}

	public void reconnect() {
		supervisor.ConnectError();
	}
}
