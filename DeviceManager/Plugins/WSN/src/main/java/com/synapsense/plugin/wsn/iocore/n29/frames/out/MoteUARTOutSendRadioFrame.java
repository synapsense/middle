package com.synapsense.plugin.wsn.iocore.n29.frames.out;

import com.synapsense.plugin.wsn.iocore.frames.out.MoteUARTOutFrame;
import com.synapsense.plugin.wsn.util.serializer.BigSerializer;

public class MoteUARTOutSendRadioFrame extends MoteUARTOutFrame {
	public MoteUARTOutSendRadioFrame(byte[] data, char slots, byte RadioCh, char[] chDuration, byte curmaxlevel,
	        char fillindex) {
		super((char) MoteUARTOutFrame.TYPE_RADIO, (char) (data.length + 14));
		BigSerializer serial = new BigSerializer(frame, 4);
		// ritu changes .. ch adaptation
		serial.packU_INT16(chDuration[0]);
		serial.packU_INT16(chDuration[1]);
		serial.packU_INT16(chDuration[2]);
		serial.packU_INT16(chDuration[3]);
		serial.packU_INT16(slots);
		serial.packU_INT8(RadioCh);
		serial.packU_INT8(curmaxlevel);
		serial.packU_INT16(fillindex);
		System.arraycopy(data, 0, frame, 18, data.length);
		this.setDelayTimeLeft(0);
	}
}
