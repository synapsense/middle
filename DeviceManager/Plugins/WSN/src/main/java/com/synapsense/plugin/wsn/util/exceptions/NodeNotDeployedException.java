package com.synapsense.plugin.wsn.util.exceptions;

public class NodeNotDeployedException extends Exception {

	private static final long serialVersionUID = -274172814337091611L;

	public NodeNotDeployedException(String msg, long device) {
		super(msg + " Device: " + Long.toHexString(device));
	}
}
