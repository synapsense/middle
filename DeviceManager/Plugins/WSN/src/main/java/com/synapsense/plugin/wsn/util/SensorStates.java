package com.synapsense.plugin.wsn.util;

public final class SensorStates {

	public static final int SENSOR_SERVICE = -2000;
	public static final int SENSOR_DISCONNECT = -3000;

	public static final double ABSZERO_DEGREEF = -459.67;

}
