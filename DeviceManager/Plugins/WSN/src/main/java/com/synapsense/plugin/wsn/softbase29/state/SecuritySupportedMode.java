package com.synapsense.plugin.wsn.softbase29.state;

public enum SecuritySupportedMode {
UNKNOWN, NOT_SUPPORTED, SUPPORTED
}
