package com.synapsense.plugin.wsn.knownprops;

import java.io.Serializable;

/**
 * The implementation of the worker is responsible for passing the particular
 * property value from/to WSN device (usually node or network). The WSN packet
 * is built upon concrete device ID and concrete property and then it is sent to
 * the gateway.
 * 
 * @author anechaev
 * 
 */
interface KnownPropertyWorker {
	boolean processMessage(String objectID, String propertyName, Serializable value);
}
