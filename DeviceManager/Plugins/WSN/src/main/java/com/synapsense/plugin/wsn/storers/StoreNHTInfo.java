package com.synapsense.plugin.wsn.storers;

import org.apache.log4j.Logger;

import com.synapsense.plugin.messages.WSNNHTBuilder;
import com.synapsense.plugin.wsn.SynapStore;
import com.synapsense.plugin.wsn.WsnConstants;
import com.synapsense.plugin.wsn.configuration.Configuration;
import com.synapsense.plugin.wsn.configuration.WSNNode;
import com.synapsense.plugin.wsn.msgbus.MsgNHTInfo;
import com.synapsense.plugin.wsn.util.SensorNetwork;
import com.synapsense.plugin.wsn.util.timesync.TimeSync;
import com.synapsense.plugin.wsn.util.timesync.TimeSyncTime;

public class StoreNHTInfo {
    private static Logger log = Logger.getLogger(StoreNHTInfo.class);

    public static void store(MsgNHTInfo message, SensorNetwork net, TimeSync times) throws Exception {
        int networkId = net.getNetworkId();

        WSNNode node = Configuration.getNode(networkId, message.deviceId);

        if (node == null) {
            log.warn("NHT message cannot be sent. Unknown node logical id: " + (int)(message.deviceId));
            return;
        }

        TimeSyncTime timestamp = times.convertTime(message.report_timestamp, times.systemTime());
        long report_timestamp = timestamp.getConvertedTime();
        char nodeID =  message.deviceId;
        int numneigh = message.numneigh;
        char[] neighIDs = message.nids;
        int[][] rssis = message.rssi;
        int[] states = message.state;
        int[] hops = message.hop;
        int[] hoptms = message.hoptm;
        for (int i = 0; i < numneigh; i++) {
            SynapStore.transport.sendData(node.getId(), WsnConstants.NETWORK_NHT, new WSNNHTBuilder(nodeID,
                    message.numneigh, report_timestamp,neighIDs[i], rssis[i][0] + " " +
                    rssis[i][1] + " " + rssis[i][2]+ " " + rssis[i][3], states[i], hops[i], hoptms[i]).getMessage());
        }
    }
}
