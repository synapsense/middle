package com.synapsense.plugin.wsn.iocore;

import java.io.IOException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class Supervisor implements Runnable {
	private static Log log = LogFactory.getLog(Supervisor.class);

	protected Thread supervise;
	private String port;

	private IRestartable moteComm;

	private boolean readerAlive = false;
	private boolean writerAlive = false;

	private final Object monitor = new Object();
	private volatile boolean interrupted = false;

	public void run() {
		log.trace("Starting supervisor for port " + port);
		while (!interrupted) {
			try {
				synchronized (monitor) {
					monitor.wait();
				}
			} catch (InterruptedException e) {
				log.trace("Supervisor got an interrupt. Stopping", e);
				return;
			}
			log.trace("Supervisor has been activated - closing threads");
			moteComm.finish();

			try {
				Thread.sleep(4024);
			} catch (InterruptedException e1) {
				log.trace("Supervisor interrupted. Bailing.");
				return;
			}

			while (!interrupted) {
				log.trace("Trying to reconnect");
				try {
					moteComm.connectPort();
					break;
				} catch (IOException e) {
					log.error("Supervisor reconnect failed.", e);
				}
				try {
					Thread.sleep(4096);
				} catch (InterruptedException e) {
					log.trace("Supervisor interrupted. Bailing.");
					return;
				}
			}
			if (!interrupted) {
				log.trace("Supervisor sleeping");
				moteComm.startReading(true);
				moteComm.startUpdating(true);
			}
		}
	}

	synchronized void readerError() {
		log.trace("Reader error");
		readerAlive = false;
		log.trace("Notifying supervisor thread");
		synchronized (monitor) {
			monitor.notifyAll();
		}

	}

	synchronized void writerError() {
		log.trace("Writer error");
		writerAlive = false;
		log.trace("Notifying supervisor thread");
		synchronized (monitor) {
			monitor.notifyAll();
		}

	}

	synchronized void ConnectError() {
		log.trace("Connection error");
		readerAlive = false;
		writerAlive = false;
		log.trace("Notifying supervisor thread");
		synchronized (monitor) {
			monitor.notifyAll();
		}
	}

	synchronized void readerAlive() {
		readerAlive = true;
	}

	synchronized void writerAlive() {
		writerAlive = true;
	}

	public synchronized final boolean isReaderAlive() {
		return readerAlive;
	}

	public synchronized final boolean isWriterAlive() {
		return writerAlive;
	}

	public Supervisor(IRestartable mc, String port) {
		this.port = port;
		moteComm = mc;
		supervise();
	}

	protected void supervise() {
		supervise = new Thread(this, "Supervisor-" + port);
		supervise.start();
	}

	public void stop() {
		interrupted = true;
		supervise.interrupt();
	}
}
