package com.synapsense.plugin.wsn.iocore.n29.frames;

import com.synapsense.plugin.wsn.iocore.frames.MoteUARTBadFrame;
import com.synapsense.plugin.wsn.iocore.frames.MoteUARTFrame;

public class MoteUARTSendFrame extends MoteUARTFrame {
	// public long device;
	// public int sensor;
	// public char reading; // actually 16 bit, unsigned
	// public long timestamp; // actually 32bit, unsigned
	public String s = "";
	private StringBuffer buf;

	public MoteUARTSendFrame(byte[] header, byte[] data, String portName) throws MoteUARTBadFrame {
		super(header, portName);
		this.data = data;
		buf = new StringBuffer();
		// Serializer serial = new Serializer(data);
		s = new String(data);
		for (int i = 0; i < data.length; i++) {
			int d = data[i] & 0xFF;
			if (d < 16)
				buf.append("0");
			buf.append(Integer.toHexString(d));
			buf.append(" ");
		}

		// device = serial.unpack_INT64();

		// sensor = serial.unpackU_INT8();

		// reading = serial.unpackU_INT16();

		// timestamp = serial.unpackU_INT32();

	}

	public String toString() {
		return buf.toString().toUpperCase();
	}
}
