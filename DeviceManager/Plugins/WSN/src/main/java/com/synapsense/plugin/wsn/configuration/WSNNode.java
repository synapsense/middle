package com.synapsense.plugin.wsn.configuration;

import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import com.synapsense.plugin.apinew.ObjectDesc;
import com.synapsense.plugin.apinew.PropertyDesc;
import com.synapsense.plugin.wsn.WsnConstants;
import com.synapsense.plugin.wsn.WsnConstants.NodeOptions;
import com.synapsense.plugin.wsn.p3.DuoConfiguration;
import com.synapsense.plugin.wsn.p3.JawaConfiguration;
import com.synapsense.plugin.wsn.p3.P3ConfigException;
import com.synapsense.plugin.wsn.p3.P3Configuration;
import com.synapsense.plugin.wsn.util.Base64;
import com.synapsense.util.TimeSample;
import com.synapsense.util.TimeSampleFormatException;

/**
 * 
 * Entity class which stores known node and all its sensors
 * 
 * @author anechaev
 * 
 */
public class WSNNode implements ConfigurationObject {
	private static final String PROGRAM_SERIAL_NODE2 = "SerialNode2:";

	private static final String PROGRAM_DUO_CONFIG = "DuoConfig:";

	private static final String PROGRAM_P3_CONFIG = "P3Config:";

	public static final int PLATFORM_GATEWAY = 0;
	public static final int PLATFORM_TELOSB = 1;
	public static final int PLATFORM_THERMAL = 11;
	public static final int PLATFORM_FUSION = 10;
	public static final int PLATFORM_PRESSURE = 12;
	public static final int PLATFORM_PRESSURE2 = 13;
	public static final int PLATFORM_CONSTELLATION = 14;
	public static final int PLATFORM_CONSTELLATION2 = 15;
	public static final int PLATFORM_BTU = 16;
	public static final int PLATFORM_THERMAL2 = 17;
	public static final int PLATFORM_YODA = 18;
	public static final int PLATFORM_HALLEY = 20;
	public static final int PLATFORM_SANDCRAWLER = 51;
	public static final int PLATFORM_DUO = 52;
	public static final int PLATFORM_ACKBAR = 80;
	public static final int PLATFORM_YWING = 82;
	public static final int PLATFORM_NTB = 84;
	public static final int PLATFORM_NTBNP = 86;
	public static final int PLATFORM_HWING = 92;

	private static Logger log = Logger.getLogger(WSNNode.class);

	private final String id;
	private final long physID;
	// TODO change modifiers to private final when logical ID are corrected
	protected int logicalID;
	private final int platformID;
	private final int networkID;
	private final int samplingInSec;

	// Sandcrawler power rack links
	private String leftRack;
	private String centerRack;
	private String rightRack;

	private final java.util.Map<Integer, WSNSensor> sensors;
	private final Map<Integer, WSNActuator> actuators;
	private Map<String, String> options;

	private final String routeParentId;

	private P3Configuration p3Configuration;
	private long serverProgramChecksum = -1;

	private final NodeID uniqueID;

	public WSNNode(int networkID, ObjectDesc initial) throws NumberFormatException, TimeSampleFormatException {

		this(initial.getID(), networkID, Long.decode(initial.getPropertyValue(WsnConstants.NODE_PHYS_ID)), new Integer(
		        initial.getPropertyValue(WsnConstants.NODE_LOGICAL_ID)), new Integer(
		        initial.getPropertyValue(WsnConstants.NODE_PLATFORM_ID)), TimeSample.get(
		        initial.getPropertyValue(WsnConstants.NODE_SAMPLING)).toSeconds(), initial
		        .getPropertyValue(WsnConstants.NODE_ROUTE));
		sensors.clear();
		for (ObjectDesc sensInitial : initial.getChildrenByType(WsnConstants.TYPE_SENOR)) {
			sensors.put(new Integer(sensInitial.getPropertyValue(WsnConstants.SENSOR_CHANNEL)), new WSNSensor(
			        sensInitial, this));
		}

		leftRack = initial.getPropertyValue("leftRack");
		centerRack = initial.getPropertyValue("centerRack");
		rightRack = initial.getPropertyValue("rightRack");
		if (log.isTraceEnabled()) {
			log.trace(buildPowerRacksLogMessage(initial.getID(), leftRack, centerRack, rightRack));
		}

		actuators.clear();
		for (ObjectDesc sensInitial : initial.getChildrenByType(WsnConstants.TYPE_ACTUATOR)) {
			WSNActuator actuator = new WSNActuator(sensInitial);
			try {
				actuator.setSensor(getSensor(actuator.getActuatorNumber()));
				actuator.setNode(this);
				actuators.put(new Integer(sensInitial.getPropertyValue(WsnConstants.ACTUATOR_NUMBER)), actuator);
			} catch (SensorNotFoundException e) {
				log.error("Cannot find sensor", e);
			}
		}
		unpackOptions(initial);
	}

	void unpackOptions(ObjectDesc initial) {
		options.clear();
		p3Configuration = null;

		for (NodeOptions opt : NodeOptions.values()) {
			PropertyDesc optionalProp = initial.getProperty(opt.getValue());
			if (optionalProp != null) {
				options.put(optionalProp.getName(), optionalProp.getValue());
				log.info("WSNNode " + Long.toHexString(physID) + " working on " + optionalProp.getName());
				if (optionalProp.getName().equals("program")) {
					log.trace("Working on unpacking program types. ");
					log.trace("Program contents: " + optionalProp.getValue());

					unpackProgramField(optionalProp);

				} else if (optionalProp.getName().equals("checksum")) {
					unpackOptionsChecksum(optionalProp);
				}
			}

		}
	}

	private void unpackOptionsChecksum(PropertyDesc optionalProp) {
		try {
			if (optionalProp.getValue() != null && !(optionalProp.getValue().equals("")))
				serverProgramChecksum = Long.parseLong(optionalProp.getValue());
			else
				serverProgramChecksum = 0;
		} catch (Exception e) {
			serverProgramChecksum = -1;
			log.debug("Can't produce the server program checksum.", e);
		}
	}

	private void unpackProgramField(PropertyDesc optionalProp) {
		/*
		 * SandCrawler nodes need to know the current configuration of the
		 * plugmeters associated with them in order to interpret the data
		 * packets as they are devoid of any jawa ID information Here we
		 * unserialize the packed program and make a proper p3Configuration
		 * object.
		 */
		String optionalPropVal = optionalProp.getValue();
		if (optionalPropVal.startsWith(PROGRAM_P3_CONFIG)) {
			log.trace("Found SandCrawler configuration program");
			try {
				String ss = optionalPropVal.substring(PROGRAM_P3_CONFIG.length());
				byte[] b64 = Base64.decode(ss);
				p3Configuration = new JawaConfiguration(b64);
			} catch (IOException e) {
				log.error("Can't decode properties", e);
			} catch (P3ConfigException e) {
				log.error("Invalid P3 P3Configuration", e);
			}
		} else if (optionalPropVal.startsWith(PROGRAM_DUO_CONFIG)) {
			log.trace("Found Duo configuration program");
			/* Deserialize Duo configuration object */

			try {
				String ss = optionalPropVal.substring(PROGRAM_DUO_CONFIG.length());
				byte[] b64 = Base64.decode(ss);
				p3Configuration = new DuoConfiguration(b64);
			} catch (IOException e) {
				log.error("Can't decode properties", e);
			} catch (P3ConfigException e) {
				log.error("Invalid P3 P3Configuration", e);
			}

		} else if (optionalPropVal.startsWith(PROGRAM_SERIAL_NODE2)) {
			log.trace("Found SerialNode2 program");
			// optionalPropVal.substring(PROGRAM_SERIAL_NODE2.length());
		}
	}

	public WSNNode(String id, int networkID, long physID, int logicalID, int platformID, int samplingInSec,
	        String routeParentId) {
		this.id = id;
		this.physID = physID;
		this.logicalID = logicalID;
		this.platformID = platformID & 0xFF;
		this.samplingInSec = samplingInSec;
		this.networkID = networkID;
		this.routeParentId = routeParentId;
		uniqueID = new NodeID(networkID, logicalID);
		sensors = new HashMap<Integer, WSNSensor>();
		options = new HashMap<String, String>();
		actuators = new HashMap<Integer, WSNActuator>();
	}

	WSNNode(int networkID, WSNNode origNode) {
		this.id = origNode.id;
		this.physID = origNode.physID;
		this.logicalID = origNode.logicalID;
		this.platformID = origNode.platformID;
		this.networkID = networkID;
		this.samplingInSec = origNode.samplingInSec;
		this.routeParentId = origNode.routeParentId;
		uniqueID = new NodeID(networkID, logicalID);
		this.sensors = new HashMap<Integer, WSNSensor>(origNode.sensors);
		this.options = new HashMap<String, String>(origNode.options);
		this.p3Configuration = origNode.p3Configuration;
		actuators = new HashMap<Integer, WSNActuator>(origNode.actuators);
	}

	public NodeID getUniqueID() {
		return uniqueID;
	}

	public P3Configuration getP3Configuration() {
		return p3Configuration;
	}

	public long getPhysID() {
		return physID;
	}

	public int getLogicalID() {
		return logicalID;
	}

	public int getSamplingInSeconds() {
		return samplingInSec;
	}

	/**
	 * Stores last measured value for a node sensor
	 * 
	 * @param channel
	 *            - sensor channel ID within node device
	 * @param value
	 *            - last measured value
	 * @throws SensorNotFoundException
	 */
	public void setSensorLastValue(int channel, double value) throws SensorNotFoundException {
		getSensor(channel).setLastValue(value);
	}

	/**
	 * returns last measured value for a node sensor
	 * 
	 * @param channel
	 *            - sensor channel ID
	 * @return measured value
	 * @throws SensorNotFoundException
	 */
	public double getSensorLastValue(int channel) throws SensorNotFoundException {
		return getSensor(channel).getLastValue();
	}

	/**
	 * @param channel
	 *            - sensor channel ID
	 * @return
	 * @throws SensorNotFoundException
	 */
	public WSNSensor getSensor(int channel) throws SensorNotFoundException {
		synchronized (sensors) {
			WSNSensor sens = sensors.get(channel);
			if (sens == null)
				throw new SensorNotFoundException("Sensor channel " + channel + " is not found for node "
				        + Long.toHexString(physID));
			return sens;
		}
	}

	public Set<Integer> getSensorChannels() {
		synchronized (sensors) {
			return sensors.keySet();
		}
	}

	public void addSensor(WSNSensor sensor) {
		sensors.put(sensor.getChannel(), sensor);
	}

	public WSNActuator getActuator(int channel) throws ActuatorNotFoundException {
		synchronized (actuators) {
			WSNActuator actuator = actuators.get(channel);
			if (actuator == null)
				throw new ActuatorNotFoundException("Actuator channel " + channel + " is not found for node "
				        + Long.toHexString(physID));
			return actuator;
		}
	}

	public Set<Integer> getActuatorChannels() {
		synchronized (actuators) {
			return actuators.keySet();
		}
	}

	public void clearActuators() {
		actuators.clear();
	}

	public void addActuator(WSNActuator actuator) {
		actuators.put(actuator.getActuatorNumber(), actuator);
	}

	public int getPlatformID() {
		return platformID;
	}

	public int getNetworkID() {
		return networkID;
	}

	WSNNode migrateToNewNetwork(int newNetworkID) {
		if (newNetworkID == networkID)
			return this;
		WSNNode newNode = new WSNNode(newNetworkID, this);
		return newNode;
	}

	/**
	 * returns a specific option for the sensor
	 * 
	 * @param optionName
	 *            name of the option
	 * @return option value or null if the option with name
	 *         <code>optionName</code> is not specified
	 */
	public String getOption(String optionName) {
		return options.get(optionName);
	}

	public Collection<WSNSensor> getSensors() {
		return sensors.values();
	}

	public Collection<WSNActuator> getActuators() {
		return actuators.values();
	}

	public String getId() {
		return id;
	}

	public void setP3Configuration(P3Configuration p3Configuration) {
		this.p3Configuration = p3Configuration;
	}

	public String getLeftRack() {
		return leftRack;
	}

	public void setLeftRack(String leftRack) {
		this.leftRack = leftRack;
	}

	public String getCenterRack() {
		return centerRack;
	}

	public void setCenterRack(String centerRack) {
		this.centerRack = centerRack;
	}

	public String getRightRack() {
		return rightRack;
	}

	public void setRightRack(String rightRack) {
		this.rightRack = rightRack;
	}

	private static String buildPowerRacksLogMessage(String nodeId, String leftRack, String centerRack, String rightRack) {
		StringBuilder messageBuilder = new StringBuilder();
		messageBuilder.append("Processed ").append(nodeId).append(" power racks links. ");
		if (leftRack.isEmpty() && rightRack.isEmpty() && centerRack.isEmpty()) {
			messageBuilder.append("No power racks");
		} else {
			setLinkMessage("Left rack", leftRack, messageBuilder);
			setLinkMessage("Center rack", centerRack, messageBuilder);
			setLinkMessage("Right rack", rightRack, messageBuilder);
		}
		return messageBuilder.toString();
	}

	private static void setLinkMessage(String propertyName, String propertyValue, StringBuilder messageBuilder) {
		if (!propertyValue.isEmpty()) {
			messageBuilder.append(propertyName).append(" = ").append(propertyValue).append(". ");
		}
	}

	public long getServerProgramChecksum() {
		return serverProgramChecksum;
	}

	public boolean setServerProgramChecksumToP3Checksum() {
		if (p3Configuration != null) {
			serverProgramChecksum = p3Configuration.getChecksum();
			return true;
		}
		return false;
	}

	public String getRouteParentId() {
		return routeParentId;
	}

}
