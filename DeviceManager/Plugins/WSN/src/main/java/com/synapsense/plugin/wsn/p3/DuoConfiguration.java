package com.synapsense.plugin.wsn.p3;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.synapsense.plugin.wsn.util.serializer.BigSerializer;
import com.synapsense.plugin.wsn.util.serializer.Serializer;

public class DuoConfiguration implements P3Configuration {

	private static Logger log = Logger.getLogger(DuoConfiguration.class);

	private Map<Integer, DuoRpdu> rpdus = new HashMap<Integer, DuoRpdu>();

	private long sent_checksum;
	private int version_code;

	public byte[] serialize() {

		List<DuoRpdu> lrpdus = new ArrayList<DuoRpdu>(rpdus.values());
		Collections.sort(lrpdus);
		// TODO: Don't scan this twice, thats silly...
		int bytes_to_serialize = 6;
		for (DuoRpdu rpdu : lrpdus) {
			bytes_to_serialize += 5;
			bytes_to_serialize += rpdu.rawSysObjectId.length;
		}

		Serializer ser = new BigSerializer(new byte[bytes_to_serialize]);

		long cksum = getChecksum();
		ser.packU_INT32(cksum);
		ser.packU_INT8((byte) version_code);
		ser.packU_INT8((byte) rpdus.size());

		for (DuoRpdu rpdu : lrpdus) {
			ser.packU_INT8((byte) (rpdu.getId() & 0xFF));
			ser.packU_INT16((char) rpdu.rawflags);
			ser.packU_INT8((byte) rpdu.getNumberOfOutlets());
			ser.packU_INT8((byte) rpdu.rawSysObjectId.length);
			ser.copyData(rpdu.rawSysObjectId);
		}

		return ser.getData();
	}

	public Map<Integer, DuoRpdu> getRpdus() {
		getChecksum();
		return Collections.unmodifiableMap(rpdus);
	}

	public List<DuoRpdu> getRpduList() {
		List<DuoRpdu> lrpdus = new ArrayList<DuoRpdu>(rpdus.values());
		Collections.sort(lrpdus);
		return lrpdus;
	}

	public long getChecksum() {

		P3CRC32 crc = new P3CRC32();
		List<DuoRpdu> lrpdus = new ArrayList<DuoRpdu>(rpdus.values());
		Collections.sort(lrpdus);

		crc.update(version_code);

		for (DuoRpdu rpdu : lrpdus) {
			crc.update((rpdu.getId() & 0xFF));
			crc.update((rpdu.rawflags >> 8) & 0xFF);
			crc.update(rpdu.rawflags & 0xFF);
			crc.update(rpdu.getNumberOfOutlets());
			for (byte b : rpdu.rawSysObjectId) {
				crc.update(b);
			}
		}

		return crc.getValue() & 0xffffffffL;
	}

	public long getSentChecksum() {
		return sent_checksum;
	}

	public DuoConfiguration(byte[] data) throws P3ConfigException {
		Serializer ser = new BigSerializer(data);

		try {
			log.trace("Starting to deserialize DuoConfig...");
			long targetCkSum = ser.unpackU_INT32();
			this.sent_checksum = targetCkSum;

			version_code = ser.unpack_INT8();
			log.trace("Version code " + version_code);
			if (version_code != 2) {
				log.error("Not a valid Duo firmware version.");
				throw new P3ConfigException("Not a valid duo config version");
			}
			int count = ser.unpack_INT8();

			if (count > 2) {
				throw new P3ConfigException("Too many rpdus");
			}

			for (int i = 0; i < count; i++) {

				int pos = ser.unpack_INT8();
				log.trace("Unpacking Duo RPDU " + pos + " at position " + i + " in packet ");
				int flags = ser.unpackU_INT16();
				int outlets = ser.unpackU_INT8();
				int sysLen = ser.unpackU_INT8();

				byte[] sys = ser.unpack_bytes(sysLen);

				try {
					DuoRpdu rpdu = new DuoRpdu(pos, sys, outlets, flags);
					rpdus.put(pos, rpdu);
				} catch (UnsupportedEncodingException e) {
					log.error("Can't decode sysObjId", e);
				}

			}
		} catch (Exception e) {
			log.error("Problem unpacking duo config", e);
			throw new P3ConfigException("Can't unpack Duo config");
		}
		log.trace("Finished unpacking Duo RPDU");
	}

}
