package com.synapsense.plugin.wsn.msgbus;

import java.util.LinkedList;

import com.synapsense.plugin.wsn.softbase29.GateWay;
import com.synapsense.plugin.wsn.util.sensors.Sensor;

public class MsgNodeInfo extends Message {

	public long device;
	public byte platformId;
	public byte nodeType; // 1 is base station, 0 is normal sensing node
	public int appVersion;
	public int appRevision;
	public int nwkVersion;
	public int nwkRevision;
	public int osVersion;
	public int osRevision;
	public byte sensorCount;
	public int noisethreshold;
	public char source;
	public GateWay gateway;

	public LinkedList<Sensor> sensors = new LinkedList<Sensor>();

	public String toString() {
		StringBuffer buf = new StringBuffer();
		buf.append("\nDevice ID: " + Long.toHexString(device).toUpperCase() + " Platform: " + platformId);
		buf.append("\nApp Ver: " + appVersion + " Rev: " + Integer.toHexString(appRevision).toUpperCase());
		buf.append(" NWK Ver: " + nwkVersion + " Rev: " + Integer.toHexString(nwkRevision).toUpperCase());
		buf.append(" OS Ver: " + osVersion + " Rev: " + Integer.toHexString(osRevision).toUpperCase());
		buf.append("\nNumber of Sensors: " + sensorCount + " Noise Threshold: " + noisethreshold);
		return buf.toString();
	}
}
