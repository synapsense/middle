package com.synapsense.plugin.wsn.iocore;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.BlockingQueue;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.plugin.wsn.configuration.WSNGW;
import com.synapsense.plugin.wsn.iocore.frames.MoteUARTFrame;
import com.synapsense.plugin.wsn.iocore.frames.out.MoteUARTOutFrame;

public final class IoFactory {
	static Map<String, Class<? extends IMoteComm>> _ios;
	private static Log log = LogFactory.getLog(IoFactory.class);

	static {
		_ios = new HashMap<String, Class<? extends IMoteComm>>();
		_ios.put("tcp", IPMoteComm.class);
	}

	public static IMoteComm generateIo(WSNGW config, BlockingQueue<MoteUARTFrame> _q,
	        BlockingQueue<MoteUARTOutFrame> _qo, int version) throws IOException {
		IMoteComm inst = null;
		Class<? extends IMoteComm> clazz = null;
		String protocol = config.getProtocol();
		if (protocol.equals("TCP_RG2"))
			clazz = _ios.get("tcp");

		if (clazz == null) {
			log.error("Can't generate a IO layer for protocol " + protocol);
			throw new IOException("No protocol handler for " + protocol);
		}

		log.trace("IoFactory: Generate for version " + version);

		try {
			Constructor<? extends IMoteComm> cons = clazz.getConstructor(WSNGW.class, BlockingQueue.class,
			        BlockingQueue.class, int.class);
			inst = cons.newInstance(config, _q, _qo, version);

		} catch (Exception e) {
			log.error("Couldn't bring this gateway service online. Going to fail now. Gateway: " + config, e);
			throw new IOException("Can't connect to gateway due to IO Error");
		}

		return inst;
	}

}
