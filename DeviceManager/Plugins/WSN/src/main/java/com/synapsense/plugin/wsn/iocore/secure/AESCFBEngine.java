package com.synapsense.plugin.wsn.iocore.secure;

import java.security.InvalidKeyException;

import javax.crypto.Cipher;
import javax.crypto.ShortBufferException;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Implements an AES CFB based engine which can work on arbitrarily sized blocks
 * of data (aka, stream cipher) DO NOT USE ENCRYPT AND DECRYPT ON THE SAME
 * OBJECT - THE STATE MACHINE GETS MESSED UP! :)
 * 
 * @author Yann Ramin
 * 
 */

public final class AESCFBEngine {
	private byte[] curIv;
	private final byte[] key;

	private Log log = LogFactory.getLog(AESCFBEngine.class);

	private Cipher cipher;
	private byte[] cipherText = new byte[16];
	private int remaining = 0;

	public AESCFBEngine(byte[] key, byte[] iv) {
		curIv = iv.clone();
		this.key = key.clone();

		try {
			cipher = Cipher.getInstance("AES/ECB/NoPadding", "BC");
		} catch (Exception e) {
			log.error("Can't get an AES/ECB/NoPadding provider.", e);
		}

	}

	public void decrypt(byte[] toread) {
		for (int i = 0; i < toread.length; i++) {
			if (remaining == 0) {
				log.trace("Generating next read CFB key from IV");
				SecretKeySpec skey = new SecretKeySpec(key, "AES");
				try {
					cipher.init(Cipher.ENCRYPT_MODE, skey);
					cipher.update(curIv, 0, 16, cipherText, 0);
				} catch (InvalidKeyException e) {
					log.error("Invalid key - can't perform AES-CFB", e);
					return;
				} catch (ShortBufferException e) {
					log.error("No enough data to perform AES-CFB", e);
					return;
				}
				remaining = 16;
			}
			byte rbyte = toread[i];
			toread[i] = (byte) (0xff & (rbyte ^ cipherText[16 - remaining]));

			curIv[16 - remaining] = (byte) (0xff & rbyte);
			remaining--;

		}
	}

	public void encrypt(byte data[]) {
		for (int i = 0; i < data.length; i++) {
			if (remaining == 0) {
				SecretKeySpec skey = new SecretKeySpec(key, "AES");
				try {
					cipher.init(Cipher.ENCRYPT_MODE, skey);
					cipher.update(curIv, 0, 16, cipherText, 0);
				} catch (InvalidKeyException e) {
					log.error("Invalid key - can't perform AES-CFB", e);
					return;
				} catch (ShortBufferException e) {
					log.error("No enough data to perform AES-CFB", e);
					return;
				}
				remaining = 16;
			}

			curIv[16 - remaining] = (byte) (0xff & (data[i] ^ cipherText[16 - remaining]));
			data[i] = (byte) (0xff & (curIv[16 - remaining]));
			remaining--;
		}
	}

}
