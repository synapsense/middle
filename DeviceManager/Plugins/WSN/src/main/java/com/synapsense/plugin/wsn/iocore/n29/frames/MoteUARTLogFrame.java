package com.synapsense.plugin.wsn.iocore.n29.frames;

import com.synapsense.plugin.wsn.iocore.frames.MoteUARTBadFrame;
import com.synapsense.plugin.wsn.iocore.frames.MoteUARTFrame;

public class MoteUARTLogFrame extends MoteUARTFrame {
	public int type;
	public String s = "";

	public MoteUARTLogFrame(byte[] header, byte[] data, String portName) throws MoteUARTBadFrame {
		super(header, portName);
		this.data = data;

		type = MoteUARTFrame.TYPE_PRINT;
		s = new String(data);
	}

	public String toString() {
		return s;
	}
}
