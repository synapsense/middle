package com.synapsense.plugin.wsn.iocore;

import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.BlockingQueue;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.plugin.wsn.iocore.frames.MoteUARTBadFrame;
import com.synapsense.plugin.wsn.iocore.frames.MoteUARTFrame;
import com.synapsense.plugin.wsn.iocore.frames.MoteUARTMsgAckFrame;

public class MoteCommReader implements Runnable {
	private final int MAXBADFRAME = 10;
	private static Log log = LogFactory.getLog(MoteCommReader.class);
	private BlockingQueue<MoteUARTFrame> sensorQueue;

	private String portName;
	private InputStream inputStream;
	private IRestartable moteCommR;
	private SureReader reader;
	private int badframecounter;

	private int frameVersion = 29;

	public MoteCommReader(BlockingQueue<MoteUARTFrame> s, InputStream i, String portName, IRestartable moteCommR,
	        int version) {
		inputStream = i;
		sensorQueue = s;
		this.portName = portName;
		if (version != 0)
			this.frameVersion = version;
		this.moteCommR = moteCommR;
		log.trace("MoteCommReader up at version " + this.frameVersion);
	}

	protected void read(byte[] toread) throws IOException {
		reader.read(toread);
	}

	public void run() {
		byte[] header = new byte[4]; // 4 byte header
		byte[] th = new byte[1];
		byte[] rest = new byte[3];

		reader = new SureReader(inputStream);
		badframecounter = 0;

		while (true) {
			moteCommR.getSupervisor().readerAlive();
			try {
				read(th);
				if (!(th[0] == MoteUARTFrame.beginMagic)) {
					continue;
				}

				read(rest); // Quick hack to find 0xBE header
				header[0] = th[0];
				header[1] = rest[0];
				header[2] = rest[1];
				header[3] = rest[2];

				MoteUARTFrame frame = new MoteUARTFrame(header, portName);
				byte[] body = frame.getDataBuffer();

				read(body); // new buffer

				byte reportedcrc = (byte) reader.read();
				byte calculatedcrc = 0;
				for (int i = 0; i < 4; i++) {
					calculatedcrc = (byte) (calculatedcrc ^ header[i]);
				}
				for (int i = 0; i < body.length; i++) {
					calculatedcrc = (byte) (calculatedcrc ^ body[i]);
				}
				if (reportedcrc != calculatedcrc) {
					log.error("CRC Fail on Received Frame");
					throw new MoteUARTBadFrame();
				}

				MoteUARTFrame nframe = frame.promote(frameVersion);

				if (nframe != null) {
					if (nframe.getType() == MoteUARTFrame.TYPE_MSG_ACK) {
						moteCommR.recvAck((MoteUARTMsgAckFrame) nframe);
					} else {
						sensorQueue.put(nframe);
					}
				} else {
					log.error("Unknown Frame Type Received - " + frame.getType());
					throw new MoteUARTBadFrame();
				}

			} catch (IOException e) {
				log.error("IO Exception: can't read data from the serial port. Going to stop reading.", e);
				moteCommR.getSupervisor().readerError();
				return;
			} catch (MoteUARTBadFrame e) {
				log.error("Bad frame read", e);
				badframecounter++;
				if (badframecounter > MAXBADFRAME) {
					moteCommR.getSupervisor().readerError();
					return;
				}
			} catch (InterruptedException e) {
				return;
			}

		}

	}

}
