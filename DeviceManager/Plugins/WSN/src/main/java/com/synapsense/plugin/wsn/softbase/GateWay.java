package com.synapsense.plugin.wsn.softbase;

public interface GateWay {
	public String getPortname();

	public char getNodeId();

	public void setNodeid(char nodeid);

	public void start();

}
