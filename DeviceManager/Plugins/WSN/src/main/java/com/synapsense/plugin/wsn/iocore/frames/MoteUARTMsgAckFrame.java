package com.synapsense.plugin.wsn.iocore.frames;

import com.synapsense.plugin.wsn.util.serializer.BigSerializer;

public class MoteUARTMsgAckFrame extends MoteUARTFrame {

	public int ack_type = -1;
	public boolean successful = false;

	public MoteUARTMsgAckFrame() {
		super(MoteUARTFrame.TYPE_MSG_ACK);
	}

	public MoteUARTMsgAckFrame(byte[] header, byte[] data, String portName) throws MoteUARTBadFrame {
		super(header, portName);
		this.data = data;
		BigSerializer serial = new BigSerializer(data);

		ack_type = serial.unpackU_INT8();
		int retCode = serial.unpackU_INT8();
		if (retCode == 0) {
			successful = true;
		} else {
			successful = false;
		}

	}

}