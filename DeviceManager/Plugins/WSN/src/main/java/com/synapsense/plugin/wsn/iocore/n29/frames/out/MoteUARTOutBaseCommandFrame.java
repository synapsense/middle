package com.synapsense.plugin.wsn.iocore.n29.frames.out;

import com.synapsense.plugin.wsn.iocore.frames.out.MoteUARTOutFrame;
import com.synapsense.plugin.wsn.util.serializer.BigSerializer;

public class MoteUARTOutBaseCommandFrame extends MoteUARTOutFrame {

	public static final byte TIMEMASTER = 1;
	// public static final byte GETSTATUS = 2;
	public static final byte GETGLOBALTIME = 3;
	public static final byte SETLPLMODEDELAY = 4;
	// public static final byte SETLPLMODE = 5;
	// public static final byte SETBEACONRATE = 6;
	public static final byte NOTTIMEMASTER = 7;
	// public static final byte SETRADIOCH = 8;
	public static final byte SETINIT = 9;
	public static final byte SETHOPCOUNTTM = 10;
	public static final byte RESET = 11;
	public static final byte SMOTAMODE = 12;
	public static final byte RADIOSENDSTATUS = 13;
	public static final byte SETTOSENSORSSEQNO = 14;

	public MoteUARTOutBaseCommandFrame(byte command) {
		super((char) MoteUARTOutFrame.TYPE_BASECOMMAND, (char) 1);
		BigSerializer serial = new BigSerializer(frame, 4);
		serial.packU_INT8(command);
		this.setDelayTimeLeft(0);
	}

	public MoteUARTOutBaseCommandFrame(byte command, int value) {
		super((char) MoteUARTOutFrame.TYPE_BASECOMMAND, (char) 5);
		BigSerializer serial = new BigSerializer(frame, 4);
		serial.packU_INT8(command);
		serial.packU_INT32((long) value);
		this.setDelayTimeLeft(0);
	}

	public MoteUARTOutBaseCommandFrame(byte command, byte[] data) {
		super((char) MoteUARTOutFrame.TYPE_BASECOMMAND, (char) (1 + data.length));
		BigSerializer serial = new BigSerializer(frame, 4);
		serial.packU_INT8(command);
		for (int i = 0; i < data.length; i++) {
			serial.packU_INT8(data[i]);
		}
		this.setDelayTimeLeft(0);
	}
}
