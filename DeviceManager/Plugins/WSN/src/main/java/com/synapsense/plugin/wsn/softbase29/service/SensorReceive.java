package com.synapsense.plugin.wsn.softbase29.service;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.plugin.wsn.msgbus.MsgSensorData;
import com.synapsense.plugin.wsn.softbase29.Softbase;
import com.synapsense.plugin.wsn.softbase29.packet.RadioMsg;
import com.synapsense.plugin.wsn.util.serializer.BigSerializer;

public class SensorReceive implements Runnable, Service {
	private Softbase softbase;
	private static Log log = LogFactory.getLog(SensorReceive.class);

	private volatile boolean interrupted = false;

	public SensorReceive(Softbase softbase) {
		this.softbase = softbase;
	}

	public void close() {

	}

	public void run() {
		RadioMsg msg;
		while (!interrupted) {
			try {
				msg = softbase.nwk.receive(OSSServices.OSS_SERVICE_SENSING);
			} catch (InterruptedException e1) {
				interrupted = true;
				continue;
			}

			try {
				BigSerializer serial = new BigSerializer(msg.data);
				int ft = serial.unpackU_INT8();
				if (ft < 1 || ft > 4)
					continue;
				int count = 0;
				if (ft == 1 || ft == 2)
					count = serial.unpackU_INT8();

				long sendtime = 0;
				if (ft == 2)
					sendtime = serial.unpackU_INT32();

				long last_seen_time = 0;
				log.trace("SensorReceive message class is " + ft);

				if (ft == 1 || ft == 2) {

					for (int i = 0; i < count; i++) {
						MsgSensorData frame = new MsgSensorData();
						frame.device = msg.sourceid;
						frame.sensor = serial.unpackU_INT16();
						frame.channel = serial.unpackU_INT8();
						frame.reading = serial.unpackU_INT16();
						frame.datatimestamp = serial.unpackU_INT32();
						frame.sendtimestamp = sendtime;
						softbase.postMessage(frame);
					}
				} else if (ft == 3) {
					for (int i = 0; i < count; i++) {

						/* Look for a tagged timestamp */
						int sensor = serial.unpackU_INT8();
						if (sensor == 0xFF) {
							last_seen_time = serial.unpackU_INT32();
							log.trace("New timestamp " + last_seen_time);
							sensor = serial.unpackU_INT8();
						}
						MsgSensorData frame = new MsgSensorData();
						frame.device = msg.sourceid;
						frame.sensor = sensor;

						frame.channel = serial.unpackU_INT8();
						frame.reading = serial.unpackU_INT16();
						frame.datatimestamp = last_seen_time;
						frame.sendtimestamp = sendtime;
						softbase.postMessage(frame);
					}
				} else if (ft == 4) {
					// Damper readings
					int sensor = serial.unpackU_INT16();
					long datatimestamp = serial.unpackU_INT32();
					count = serial.unpackU_INT8();
					for (int i = 0; i < count; i++) {
						MsgSensorData frame = new MsgSensorData();
						frame.device = msg.sourceid;
						frame.sensor = sensor;
						frame.channel = serial.unpackU_INT8();
						frame.reading = (char) serial.unpackU_INT8();
						frame.devicestate = serial.unpackU_INT8();
						frame.controlresponse = serial.unpackU_INT8();
						frame.datatimestamp = datatimestamp;
						softbase.postMessage(frame);
					}
				}
			} catch (ArrayIndexOutOfBoundsException e) {
				log.error("Malformed Sensor reading from " + Integer.toHexString(msg.sourceid));
			}

		}
	}

	public void interrupt() {
		interrupted = true;
	}

	public byte getServiceID() {
		return OSSServices.OSS_SERVICE_SENSING;
	}

}
