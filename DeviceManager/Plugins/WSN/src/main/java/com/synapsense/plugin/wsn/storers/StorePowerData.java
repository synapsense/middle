package com.synapsense.plugin.wsn.storers;

import org.apache.log4j.Logger;

import com.synapsense.plugin.messages.DuoDataBuilder;
import com.synapsense.plugin.messages.PowerDataBuilder;
import com.synapsense.plugin.wsn.NetworkWorker;
import com.synapsense.plugin.wsn.SynapStore;
import com.synapsense.plugin.wsn.WsnConstants;
import com.synapsense.plugin.wsn.configuration.Configuration;
import com.synapsense.plugin.wsn.configuration.WSNNode;
import com.synapsense.plugin.wsn.msgbus.Message;
import com.synapsense.plugin.wsn.msgbus.MsgDuoData;
import com.synapsense.plugin.wsn.msgbus.MsgJawaData;
import com.synapsense.plugin.wsn.msgbus.MsgP3ResendConfig;
import com.synapsense.plugin.wsn.p3.DuoConfiguration;
import com.synapsense.plugin.wsn.p3.DuoData;
import com.synapsense.plugin.wsn.p3.DuoRpdu;
import com.synapsense.plugin.wsn.p3.JawaData;
import com.synapsense.plugin.wsn.p3.JawaDataHolder;
import com.synapsense.plugin.wsn.p3.JawaPhaseData;
import com.synapsense.plugin.wsn.p3.P3ConfigMismatchException;
import com.synapsense.plugin.wsn.util.SensorNetwork;
import com.synapsense.plugin.wsn.util.timesync.TimeSync;
import com.synapsense.plugin.wsn.util.timesync.TimeSyncTime;

public class StorePowerData {
	private static Logger log = Logger.getLogger(StorePowerData.class);

	private static void send(WSNNode node, PowerDataBuilder bld) {
		SynapStore.transport.sendData(node.getId(), WsnConstants.NETWORK_POWER_CONFIG, bld.getMessage());

	}

	private static void storeDuo(Message message, SensorNetwork net, TimeSync times) {
		MsgDuoData msg = (MsgDuoData) message;
		WSNNode node = Configuration.getNode(net.getNetworkId(), msg.deviceId);

		if (node.getP3Configuration() == null) {
			log.error("I can't find a configuration! I am going to ask for it from the node again");
			askForConfig(net, msg.deviceId);
			return;
		}

		TimeSyncTime timestamp = times.convertTime(msg.timestamp, times.systemTime());
		msg.timestamp = timestamp.getConvertedTime();

		DuoConfiguration config = (DuoConfiguration) node.getP3Configuration();
		DuoData data = null;
		try {
			data = new DuoData(config, msg.data);
		} catch (P3ConfigMismatchException e) {
			log.warn("Asking for configuration again");
			askForConfig(net, msg.deviceId);
			return;
		} catch (Exception e) {
			log.error("Unhandled P3 deserialize exception ", e);
			return;
		}

		if (log.isDebugEnabled()) {
			log.debug("Received DUO data packet for node " + node.getPhysID() + " msg timestamp " + msg.timestamp);
		}
		DuoDataBuilder builder = new DuoDataBuilder(node.getPhysID());
		for (DuoRpdu rpdu : config.getRpduList()) {
			builder.addRpduData(rpdu.getId(), data.getAllValuesFor(rpdu));
		}
		builder.setTimestamp(msg.timestamp);
		SynapStore.transport.sendData(node.getId(), WsnConstants.NETWORK_POWER_CONFIG, builder.getMessage());
	}

	public static void store(Message message, SensorNetwork net, TimeSync times) {
		try {
			if (message instanceof MsgJawaData)
				storeJawa(message, net, times);
			else if (message instanceof MsgDuoData)
				storeDuo(message, net, times);
		} catch (Exception e) {
			log.error("Can't store power data due to unknown exception: ", e);
		}
	}

	private static void storeJawa(Message message, SensorNetwork net, TimeSync times) {
		MsgJawaData msg = (MsgJawaData) message;

		WSNNode node = Configuration.getNode(net.getNetworkId(), msg.deviceId);

		if (node == null) {
			log.error("Can't find node with logical " + Integer.toHexString(msg.deviceId) + " which sent P3 data.");
			return;
		}

		if (node.getP3Configuration() == null) {
			log.error("I can't find a configuration! I am going to ask for it from the node again");
			askForConfig(net, msg.deviceId);
			return;
		}

		TimeSyncTime timestamp = times.convertTime(msg.timestamp, times.systemTime());
		msg.timestamp = timestamp.getConvertedTime();

		JawaDataHolder dh = null;
		try {
			dh = new JawaDataHolder(node, msg.data, msg.timestamp);
			if (dh.checksum != node.getP3Configuration().getChecksum()) {
				log.error("Configuration checksum doesn't match what I have - asking for config again");
				askForConfig(net, msg.deviceId);
				return;
			}
		} catch (Exception e) {
			log.error("Can't unpack P3 Data", e);
			return;
		}

		for (JawaData jawa : dh.jawaData) {
			if (log.isDebugEnabled())
				log.debug("Power Data for Jawa: " + jawa);
			PowerDataBuilder bld = new PowerDataBuilder(jawa.physicalId, jawa.jid, jawa.avgCurrent, jawa.maxCurrent,
			        jawa.demandPower, jawa.timestamp);
			send(node, bld);
		}

		for (JawaPhaseData pd : dh.phaseData) {
			if (log.isDebugEnabled())
				log.debug("Power Data for Phase: " + pd);
			for (int phase = 0; phase < 3; phase++) {
				if (pd.phaseVoltages[phase] > 0.1) {
					PowerDataBuilder bld = new PowerDataBuilder(node.getPhysID(), pd.rack, pd.feed, phase + 1,
					        pd.phaseVoltages[phase], pd.timestamp);
					send(node, bld);
				} else {
					int pphase = phase + 1;
					log.warn("Not sending message for phase with near zero voltage: Rack " + pd.rack + " Feed "
					        + pd.feed + " Phase " + pphase);
				}
			}
		}
	}

	private static void askForConfig(SensorNetwork net, char deviceId) {
		NetworkWorker worker = SynapStore.workerMap.get(net);
		worker.commandQ.add(new MsgP3ResendConfig(deviceId));
	}
}
