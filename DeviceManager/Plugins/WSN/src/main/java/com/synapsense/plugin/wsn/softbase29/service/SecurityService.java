package com.synapsense.plugin.wsn.softbase29.service;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.plugin.wsn.configuration.Configuration;
import com.synapsense.plugin.wsn.configuration.WSNNode;
import com.synapsense.plugin.wsn.msgbus.Message;
import com.synapsense.plugin.wsn.secmgr.ProcessMessageResult;
import com.synapsense.plugin.wsn.secmgr.WSNSecurityManager;
import com.synapsense.plugin.wsn.softbase29.SendError;
import com.synapsense.plugin.wsn.softbase29.Softbase;
import com.synapsense.plugin.wsn.softbase29.SoftbaseNWK;
import com.synapsense.plugin.wsn.softbase29.packet.OSSDataPacket;
import com.synapsense.plugin.wsn.softbase29.packet.RadioMsg;
import com.synapsense.plugin.wsn.softbase29.state.NodeInfoEntry;
import com.synapsense.plugin.wsn.softbase29.state.SecuritySupportedMode;

public class SecurityService implements Service, Runnable {

	private Softbase softbase;
	private boolean interrupted = false;

	private WSNSecurityManager cmanager;
	private static Log log = LogFactory.getLog(SecurityService.class);

	public SecurityService(Softbase ss) {
		softbase = ss;
		cmanager = new WSNSecurityManager(ss);
		cmanager.setNetworkKey(ss.getModel().getPublicKey(), ss.getModel().getEncipheredPrivateKey());
	}

	public void sendSecurely(Message handlers, byte[] data, char dest, byte service, boolean bcast) throws SendError {
		NodeInfoEntry nodeinfo = softbase.node_info.getById(dest);
		if (nodeinfo != null && nodeinfo.getSecuritySupportedMode() == SecuritySupportedMode.SUPPORTED) {
			// Make service type unsigned int
			cmanager.sendSecurely(handlers, dest, service & 0xFF, bcast, data);
			return;
		} else {

			int proto;
			if (bcast)
				proto = SoftbaseNWK.Protocol_ToSensors;
			else
				proto = SoftbaseNWK.Protocol_ToOneSensor;

			softbase.nwk.send(handlers, data, dest, service, proto);
			return;
		}
	}

	public void run() {
		RadioMsg msg = null;
		while (!interrupted) {
			try {
				msg = softbase.nwk.receive(OSSServices.OSS_SERVICE_CSESSION);
			} catch (InterruptedException e1) {
				interrupted = true;
				continue;
			}
			// If we receieve something here, go to SUPPORTED mode
			// This allows "lazy-loading" of the security service upon DM
			// restart at the risk of the device not receiving
			// packets at first.
			softbase.node_info.getById(msg.sourceid).setSecuritySupportedMode(SecuritySupportedMode.SUPPORTED);
			WSNNode node = Configuration.getNode(softbase.getNetId(), msg.sourceid);
			if (node == null) {
				log.error("Security message received from unknown node ID: " + (int) msg.sourceid);
				continue;
			}
			ProcessMessageResult r = cmanager.processMessage(node.getPhysID(), msg.sourceid, msg.data);
			if (r.hasData()) {
				// Re-inject the packet as a non-security packet
				OSSDataPacket newpk = msg.original_pkt;
				newpk.data = r.newdata();
				newpk.nwk_service = (byte) r.service();
				softbase.nwk.setApp(newpk, msg.gateway);
			}
		}
	}

	@Override
	public void close() {
		cmanager.close();
	}

	@Override
	public byte getServiceID() {
		return OSSServices.OSS_SERVICE_CSESSION;
	}

}
