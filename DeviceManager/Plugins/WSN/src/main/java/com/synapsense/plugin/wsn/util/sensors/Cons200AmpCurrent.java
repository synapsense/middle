package com.synapsense.plugin.wsn.util.sensors;

import com.synapsense.plugin.wsn.util.SensorStates;

public class Cons200AmpCurrent implements SensorConversionInterface {

	public double convertFromRaw(char raw) {
		// voltage is measured in 12bits ADCCounts
		// 0 -> 0V ; 4095 -> 2.048V
		double voltage = ((double) raw / 4095) * 2.048;

		// 0.2V -> 0 Amps
		// 2.0V -> 200 Amps

		if (voltage < 0.1)
			return SensorStates.SENSOR_DISCONNECT; // to indicate that sensor is
			                                       // disconnected
		// 100/47 is the change from 100ohm sensing resistor to 47 ohm sensing
		// resistor
		// ~ half the voltage, need to gain the reading up
		double count = (voltage - 0.4) * 62.5 * 2.0;

		return (count > 0) ? count : 0;
	}

	public double convertFromRawCompensated(char raw, double other) {
		return convertFromRaw(raw);

	}

	public int getMoteDataType() {
		return 30;
	}
}
