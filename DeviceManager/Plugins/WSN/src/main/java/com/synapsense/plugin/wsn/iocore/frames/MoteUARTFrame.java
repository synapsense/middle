package com.synapsense.plugin.wsn.iocore.frames;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.plugin.wsn.iocore.n29.frames.MoteUARTBeaconSendFrame;
import com.synapsense.plugin.wsn.iocore.n29.frames.MoteUARTCanIStartFrame;
import com.synapsense.plugin.wsn.iocore.n29.frames.MoteUARTGlobalTimeFrame;
import com.synapsense.plugin.wsn.iocore.n29.frames.MoteUARTIsTimeSyncFrame;
import com.synapsense.plugin.wsn.iocore.n29.frames.MoteUARTNWKStatSoftFrame;
import com.synapsense.plugin.wsn.iocore.n29.frames.MoteUARTPrintFrame;
import com.synapsense.plugin.wsn.iocore.n29.frames.MoteUARTRadioFrame;
import com.synapsense.plugin.wsn.iocore.n29.frames.MoteUARTRcvdRadioFrame;
import com.synapsense.plugin.wsn.iocore.n29.frames.MoteUARTStatusFrame;
import com.synapsense.plugin.wsn.iocore.n29.frames.MoteUARTTimesyncFrame;
import com.synapsense.plugin.wsn.iocore.n29.frames.MoteUARTUnitCmd;
import com.synapsense.plugin.wsn.iocore.vk.frames.MoteUARTVKRadioFrame;

public class MoteUARTFrame {

	static {
		initBaseFrames();
	}

	public String portName;
	public final long timestamp;

	protected byte[] header;

	protected int len;
	protected byte[] data;

	private String id = "ff";
	private Log log = LogFactory.getLog(MoteUARTFrame.class);
	private int type;

	public static final byte beginMagic = (byte) 0xBE;
	public static final byte endMagic = (byte) 0xEF;

	// public static final int TYPE_NETWORK_TOPOLGY = 2; // network topology
	// changes

	public static final int TYPE_SEND = 7;
	public static final int TYPE_RECV = 8;

	public static final int TYPE_PRINT = 16;

	public static final int TYPE_RECV_SNIFF = 19;

	public static final int TYPE_UNITCMD = 60;

	public static final int TYPE_TIMESYNC = 61; // timesync global time frame

	public static final int TYPE_CANISTART = 100;

	// SOFTBASE USE /////////////////
	public static final int TYPE_RADIO = 101;
	public static final int TYPE_RADIODONE = 102;
	public static final int TYPE_STATUS = 103;
	public static final int TYPE_GLOBALTIME = 104;
	public static final int TYPE_BEACONSEND = 105;
	public static final int TYPE_ISTIMESYNC = 106;
	public static final int TYPE_NWKSTATSOFT = 107;
	public static final int TYPE_VKRADIORAWRECV = 160;
	public static final int TYPE_VKRADIORAWSEND = 161;

	// ////////////////////////////////

	public static final int TYPE_CODEDIST = 201;

	public static final int TYPE_MSG_ACK = 254; // message was received by base
	                                            // node - send next message

	public MoteUARTFrame(byte[] header, String portName) throws MoteUARTBadFrame {
		timestamp = System.currentTimeMillis();

		if (header.length < 4) {
			throw new MoteUARTBadFrame();
		}
		if (header[0] != beginMagic || header[3] != endMagic) {
			log.error(portName + " bad header: ");
			for (byte b : header) {
				log.error((Integer.toHexString(b & 0xff)) + " ");
			}

			throw new MoteUARTBadFrame();
		}

		setType(header[1] & 0xFF);

		len = header[2] & 0xFF;

		if (header.length > 4) {

			id = Integer.toString(header[4] & 0xff);
		}

		this.header = header;
		if (len > 255) {
			len = 255;
		}
		if (len < 0) {
			len = 1;
		}
		data = new byte[len];
		this.portName = portName;
	}

	public MoteUARTFrame(int type) {
		timestamp = System.currentTimeMillis();
		setType(type);
	}

	public MoteUARTFrame(int type, String portName) {
		timestamp = System.currentTimeMillis();
		setType(type);
		this.portName = portName;
	}

	public static void initBaseFrames() {

		FrameFactory.registerFrame(TYPE_MSG_ACK, 29, MoteUARTMsgAckFrame.class);

		FrameFactory.registerFrame(TYPE_SEND, 29, MoteUARTRadioFrame.class);

		FrameFactory.registerFrame(TYPE_RECV, 29, MoteUARTRadioFrame.class);

		FrameFactory.registerFrame(TYPE_RECV_SNIFF, 29, MoteUARTRadioFrame.class);

		FrameFactory.registerFrame(TYPE_PRINT, 29, MoteUARTPrintFrame.class);

		FrameFactory.registerFrame(TYPE_UNITCMD, 29, MoteUARTUnitCmd.class);

		FrameFactory.registerFrame(TYPE_TIMESYNC, 29, MoteUARTTimesyncFrame.class);

		FrameFactory.registerFrame(TYPE_RADIO, 29, MoteUARTRcvdRadioFrame.class);

		FrameFactory.registerFrame(TYPE_RADIODONE, 29, MoteUARTRcvdRadioFrame.class);

		FrameFactory.registerFrame(TYPE_STATUS, 29, MoteUARTStatusFrame.class);

		FrameFactory.registerFrame(TYPE_GLOBALTIME, 29, MoteUARTGlobalTimeFrame.class);

		FrameFactory.registerFrame(TYPE_BEACONSEND, 29, MoteUARTBeaconSendFrame.class);

		FrameFactory.registerFrame(TYPE_ISTIMESYNC, 29, MoteUARTIsTimeSyncFrame.class);

		FrameFactory.registerFrame(TYPE_CANISTART, 29, MoteUARTCanIStartFrame.class);

		FrameFactory.registerFrame(TYPE_NWKSTATSOFT, 29, MoteUARTNWKStatSoftFrame.class);

		FrameFactory.registerFrame(TYPE_VKRADIORAWRECV, 29, MoteUARTVKRadioFrame.class);
		FrameFactory.registerFrame(TYPE_VKRADIORAWSEND, 29, MoteUARTVKRadioFrame.class);
	}

	public MoteUARTFrame promote(int version) throws MoteUARTBadFrame {
		// log.trace("Promoting a frame of type " + getType() + " for version "
		// + version);
		return FrameFactory.createFromId(getType(), version, this);
	}

	public byte[] getDataBuffer() {
		return data;
	}

	public String getId() {
		return id;
	}

	public void setType(int type) {
		this.type = type;
	}

	public int getType() {
		return type;
	}

	public byte[] getHeader() {
		return header;
	}

	public String toString() {
		return this.getClass().getName();
	}

}
