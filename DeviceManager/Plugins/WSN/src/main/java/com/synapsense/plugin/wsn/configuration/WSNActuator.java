package com.synapsense.plugin.wsn.configuration;

import org.apache.log4j.Logger;

import com.synapsense.plugin.apinew.ObjectDesc;
import com.synapsense.plugin.wsn.WsnConstants;

public class WSNActuator implements ConfigurationObject {

	private static Logger log = Logger.getLogger(WSNActuator.class);
	public static final int DIRECTION_CW = 1;
	public static final int DIRECTION_CCW = 2;
	public static final byte CONTROL_WSNACTUATE = 1;

	public static final int CONTROLRESPONSE_READY = 0;
	public static final int CONTROLRESPONSE_MOVING = 3;
	public static final int CONTROLRESPONSE_MOVECOMPLETE = 4;

	private int actuatorNumber;
	private int nextValue;
	private int direction;
	private int failsafesetpoint;
	private int minpoint;
	private int maxpoint;
	private int travelTime;
	private int positionTolerance;
	private final String id;
	private WSNSensor sensor;
	private WSNNode node;

	public WSNActuator(ObjectDesc initial) {

		this.id = initial.getID();

		log.trace("Building actuator object " + this.id);

		this.actuatorNumber = new Integer(initial.getPropertyValue(WsnConstants.ACTUATOR_NUMBER));
		this.direction = new Integer(initial.getPropertyValue(WsnConstants.ACTUATOR_DIRECTION));
		this.failsafesetpoint = new Integer(initial.getPropertyValue(WsnConstants.ACTUATOR_FAILSAFESETPOINT));
		this.minpoint = new Integer(initial.getPropertyValue(WsnConstants.ACTUATOR_MINPOINT));
		this.maxpoint = new Integer(initial.getPropertyValue(WsnConstants.ACTUATOR_MAXPOINT));
		this.travelTime = (new Double(initial.getPropertyValue(WsnConstants.ACTUATOR_TRAVELTIME))).intValue();
		this.positionTolerance = new Integer(initial.getPropertyValue(WsnConstants.ACTUATOR_POSITION_TOLERANCE));
		this.nextValue = -1; // Invalid setpoint initially
	}

	public WSNActuator(String id, int actuatorNumber, int direction, int failsafesetpoint, int minpoint, int maxpoint,
	        int travelTime, int positionTolerance) {
		this.id = id;
		this.actuatorNumber = actuatorNumber;
		this.direction = direction;
		this.failsafesetpoint = failsafesetpoint;
		this.minpoint = minpoint;
		this.maxpoint = maxpoint;
		this.travelTime = travelTime;
		this.nextValue = -1; // Invalid setpoint initially
		this.positionTolerance = positionTolerance;
	}

	public int getNextValue() {
		return nextValue;
	}

	public void setNextValue(int nextValue) {
		this.nextValue = nextValue;
	}

	public int getActuatorNumber() {
		return actuatorNumber;
	}

	public void setActuatorNumber(int actuatorNumber) {
		this.actuatorNumber = actuatorNumber;
	}

	public int getDirection() {
		return direction;
	}

	public void setDirection(int direction) {
		this.direction = direction;
	}

	public int getFailsafesetpoint() {
		return failsafesetpoint;
	}

	public void setFailsafesetpoint(int failsafesetpoint) {
		this.failsafesetpoint = failsafesetpoint;
	}

	public int getMinpoint() {
		return minpoint;
	}

	public void setMinpoint(int minpoint) {
		this.minpoint = minpoint;
	}

	public int getMaxpoint() {
		return maxpoint;
	}

	public void setMaxpoint(int maxpoint) {
		this.maxpoint = maxpoint;
	}

	public int getTravelTime() {
		return travelTime;
	}

	public void setTravelTime(int travelTime) {
		this.travelTime = travelTime;
	}

	public String getId() {
		return id;
	}

	public WSNSensor getSensor() {
		return sensor;
	}

	public void setSensor(WSNSensor sensor) {
		this.sensor = sensor;
	}

	public WSNNode getNode() {
		return node;
	}

	public void setNode(WSNNode node) {
		this.node = node;
	}

	public int getPositionTolerance() {
		return this.positionTolerance;
	}

	public void setPositionTolerance(int v) {
		this.positionTolerance = v;
	}

	public String toString() {
		return id + " : " + WsnConstants.ACTUATOR_NUMBER + "-" + actuatorNumber + ", "
		        + WsnConstants.ACTUATOR_DIRECTION + "-" + direction + ", " + WsnConstants.ACTUATOR_FAILSAFESETPOINT
		        + "-" + failsafesetpoint + ", " + WsnConstants.ACTUATOR_MINPOINT + "-" + minpoint + ", "
		        + WsnConstants.ACTUATOR_MAXPOINT + "-" + maxpoint + ", " + WsnConstants.ACTUATOR_TRAVELTIME + "-"
		        + travelTime + ", " + WsnConstants.ACTUATOR_NEXTVALUE + "-" + nextValue + ","
		        + WsnConstants.ACTUATOR_POSITION_TOLERANCE + " " + positionTolerance;
	}
}
