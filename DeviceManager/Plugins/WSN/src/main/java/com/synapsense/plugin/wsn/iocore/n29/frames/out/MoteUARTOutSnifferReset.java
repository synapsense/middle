package com.synapsense.plugin.wsn.iocore.n29.frames.out;

import com.synapsense.plugin.wsn.iocore.frames.out.MoteUARTOutFrame;
import com.synapsense.plugin.wsn.util.serializer.BigSerializer;

public class MoteUARTOutSnifferReset extends MoteUARTOutFrame {

	public MoteUARTOutSnifferReset(char panid, char ch) {
		super((char) MoteUARTOutFrame.TYPE_SNIFFER_RESET, (char) 4);
		BigSerializer ser = new BigSerializer(frame, 4);
		ser.packU_INT16(panid);
		ser.packU_INT16(ch);
	}
}
