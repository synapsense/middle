package com.synapsense.plugin.wsn.p3;

import java.util.concurrent.BlockingQueue;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.plugin.wsn.iocore.frames.out.MoteUARTOutSetParameterFrame;
import com.synapsense.plugin.wsn.softbase29.packet.MultiMessageSender;
import com.synapsense.plugin.wsn.util.Constants;

//runs each SandCrawler in parallel threads and writes to shared ArrayBlockQueue

public class P3Updater implements Runnable {

	private byte[] msgData;
	// private int num;
	private char to;
	private final BlockingQueue<MoteUARTOutSetParameterFrame> queue;
	// private Boolean done = false;
	private static Log log = LogFactory.getLog(P3Updater.class);

	public P3Updater(BlockingQueue<MoteUARTOutSetParameterFrame> q, char to, byte[] msgData) {
		queue = q;
		this.msgData = msgData;
		this.to = to;
	}

	public void run() {
		log.trace("P3Updater started sending " + msgData.length + " bytes");

		try {
			// for (int i = 0; i < num; i++) {
			// String s = message + ": " + i;
			// // System.out.println("                putting "+s);
			// queue.put(s);
			// }

			// TOD create MM_sender
			// put into M inoto frame
			// send frame to spooler

			// queue.put(frame);

			MultiMessageSender mm = new MultiMessageSender(msgData.length);
			MoteUARTOutSetParameterFrame frame;
			for (int i = 0; i < msgData.length; i++) {
				byte[] msg = mm.add_byte(msgData[i]);
				// log.trace("mms:  i = " + i + " returned  msg of " +
				// msg.length + " bytes");
				if (msg.length > 1) {
					// mm.mm_show(msg, "msg i = "+i);
					frame = new MoteUARTOutSetParameterFrame(to, Constants.PARAM_ID_CMDPLUGMETER, 0, msg);
					queue.put(frame);
					mm.mm_show(msg, "msg");
				}
			}

			byte[] msg = mm.flush();
			mm.mm_show(msg, "last msg");
			frame = new MoteUARTOutSetParameterFrame(to, Constants.PARAM_ID_CMDPLUGMETER, 0, msg);
			queue.put(frame);

		} catch (InterruptedException e) {
			log.warn("Interrupted while sending P3Update frame", e);
		}

		// This will run until everything is queue up, then stop

	}

	// public Boolean isDone (){
	// return this.done;
	// }

}
