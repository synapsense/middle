package com.synapsense.plugin.wsn;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileReader;
import java.security.KeyStore;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import com.synapsense.plugin.api.exceptions.IncorrectStateException;
import com.synapsense.plugin.apinew.PluginConfig;
import com.synapsense.plugin.apinew.Transport;
import com.synapsense.plugin.wsn.configuration.Configuration;
import com.synapsense.plugin.wsn.configuration.WSNActuator;
import com.synapsense.plugin.wsn.configuration.WSNNode;
import com.synapsense.plugin.wsn.configuration.WSNSensor;
import com.synapsense.plugin.wsn.servers.CommandServer;
import com.synapsense.plugin.wsn.servers.UpdateServer;
import com.synapsense.plugin.wsn.util.Constants;
import com.synapsense.plugin.wsn.util.SensorNetwork;
import com.synapsense.plugin.wsn.util.sensors.SensorFactory;

public class SynapStore {

	public static final String verString = "$SYNAPVER$ - $SYNAPREV$";

	// it is a bit dirty to set up such properties externally, but it is the
	// easiest way to do it.
	private static boolean isConfigured = false;
	public static boolean isRunning = false;

	// public static DeviceManagerTransport transport = null;
	public static Transport transport = null;
	private static Logger log = Logger.getLogger(SynapStore.class);
	public static HashMap<SensorNetwork, NetworkWorker> workerMap = new HashMap<SensorNetwork, NetworkWorker>();
	private static Thread synapStoreThread;

	/**
	 * Passes a config information to SynapStore. Configuring the SynapStore is
	 * an required but not condition to let SynapStore start.
	 * 
	 * @param config
	 */
	public static void configure(PluginConfig config) {
		Configuration.configure(config);
		isConfigured = true;
	}

	/**
	 * tries to start SynapStore. There are two conditions to meet to let
	 * SynapStore start: 1. SynapStore should be configured using
	 * <code>config</code> method 2. They should enable SynapStore to start
	 * gateways.
	 * 
	 * @throws IncorrectStateException
	 * 
	 */
	public static void tryStart() throws IncorrectStateException {
		if (!isConfigured) {
			throw new IncorrectStateException("Plugin " + WSNPlugin.PLUGIN_ID + " has to be configured before start.");
		}
		if (isRunning)
			return;

		synapStoreThread = new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					SynapStore.start();
				} catch (Exception e) {
					log.error(e.getLocalizedMessage(), e);
				}
			}

		});
		synapStoreThread.start();
	}

	private static void start() throws Exception {
		String PATHLOG4J = "lib/log4j.properties";
		PropertyConfigurator.configure(PATHLOG4J);
		isRunning = true;
		log.info("Welcome to WSNPlugin " + verString + " (c) 2007-2011 SynapSense Corporation");

		SensorFactory.initFactory();
		Collection<SensorNetwork> sensorNetworks = null;

		sensorNetworks = Configuration.getNetworks();
		if (sensorNetworks == null) {
			log.error("No sensor networks were detected...Please ensure that the configuration has a valid network Id for a network configured in the database.");
			System.exit(Constants.ABNORMAL_SYSTEM_TERMINATION);
		}
		/*
		 * else if (sensorNetworks.size()==0){ log.error(
		 * "No sensor networks were detected...Please ensure that the configuration has a valid network Id for a network configured in the database."
		 * ); System.exit(Constants.ABNORMAL_SYSTEM_TERMINATION); }
		 */
		// May have multiple networks...ok, so have a network worker per network
		loadNetworkThreads(sensorNetworks);

		UpdateServer updateserver = new UpdateServer(workerMap);
		Thread updateserverth = new Thread(updateserver, "Update Server");
		updateserverth.setDaemon(false);
		updateserverth.start();

		CommandServer commandserver = new CommandServer(workerMap);
		Thread commandserverth = new Thread(commandserver, "Command Server");
		commandserverth.setDaemon(false);
		commandserverth.start();

		while (true)
			Thread.sleep(100000);

	}

	private static void loadNetworkThreads(Collection<SensorNetwork> sensorNetworks) {

		for (SensorNetwork currSensorNet : sensorNetworks) {
			try {
				NetworkWorker nw = createOrReturnWorker(currSensorNet);
			} catch (Exception e) {
				log.error("NetworkWorker creation failed.", e);
			}
		}
	}

	public static NetworkWorker createOrReturnWorker(SensorNetwork newNet) throws Exception {
		NetworkWorker worker = null;

		if (SynapStore.workerMap.containsKey(newNet)) {
			return SynapStore.workerMap.get(newNet);
		}

		worker = new NetworkWorker(newNet);
		SynapStore.workerMap.put(newNet, worker);

		Thread workerTh = new Thread(worker, "NetworkWorker-" + Integer.toString(newNet.getNetworkId()));
		workerTh.setDaemon(false);
		workerTh.start();

		return worker;
	}

	public static void main(String args[]) throws Exception {
		if (args.length < 3) {
			log.error("Usage: java SynapStore [0/1(new node join)] [hex PANID] COM[PORT1#],COM[PORT2#] [0/1] [hex PANID] COM[PORT3#]...");
			System.exit(1);
		}

		try {
			BufferedReader in = new BufferedReader(new FileReader("IDtable.txt"));
			String line = in.readLine();
			while (line != null) {
				if (line.equals("")) {
					line = in.readLine();
					continue;
				}
				StringTokenizer tokens = new StringTokenizer(line);
				int network_id = Integer.parseInt(tokens.nextToken());
				long physical_id = Long.parseLong(tokens.nextToken(), 16);
				char logical_id = (char) (Integer.parseInt(tokens.nextToken()));
				int platform_id = Integer.parseInt(tokens.nextToken());
				WSNNode node = new WSNNode("N" + Integer.toString(logical_id), network_id, physical_id, logical_id,
				        platform_id, 300, "");
				if (node.getPlatformID() == WSNNode.PLATFORM_ACKBAR || node.getPlatformID() == WSNNode.PLATFORM_NTB) {
					WSNActuator actuator = new WSNActuator("A" + Integer.toString(logical_id), 0,
					        WSNActuator.DIRECTION_CW, 50, 10, 90, 150, 5);
					actuator.setNode(node);
					node.addActuator(actuator);
				}
				if (node.getPlatformID() == WSNNode.PLATFORM_YWING || node.getPlatformID() == WSNNode.PLATFORM_HWING
				        || node.getPlatformID() == WSNNode.PLATFORM_NTBNP) {
					Map<String, String> options = new HashMap<String, String>();
					options.put(WsnConstants.SENSOR_DELTA_T_LOW, "5");
					options.put(WsnConstants.SENSOR_DELTA_T_HIGH, "5");
					options.put(WsnConstants.SENSOR_CRITICAL_POINT, "75");
					options.put("subSamples", "15");
					WSNSensor sensor = new WSNSensor(node, "S" + Integer.toString(logical_id), 3, 0, 0, options);
					node.addSensor(sensor);
				}
				Configuration.registerNode(node, false);
				line = in.readLine();
			}
			in.close();
		} catch (Exception e) {
		}

		SensorFactory.initFactory();

		for (int i = 0; i < args.length; i = i + 3) {
			SensorNetwork currSensorNet = new SensorNetwork(args[i + 2].trim(), i / 3 + 1);
			currSensorNet.setPanId(Integer.parseInt(args[i + 1], 16));
			currSensorNet.setVersion("N29");
			NetworkWorker worker = null;
			if (SynapStore.workerMap.containsKey(currSensorNet)) {
				worker = SynapStore.workerMap.get(currSensorNet);
			} else {
				if (args[i].equals("0"))
					worker = new NetworkWorker(currSensorNet, true, false);
				else
					worker = new NetworkWorker(currSensorNet, true, true);

				SynapStore.workerMap.put(currSensorNet, worker);
			}

			Thread workerTh = new Thread(worker, "NetworkWorker-" + Integer.toString(i / 3 + 1));
			workerTh.setDaemon(false);
			workerTh.start();
		}

		UpdateServer updateserver = new UpdateServer(workerMap);
		Thread updateserverth = new Thread(updateserver, "Update Server");
		updateserverth.setDaemon(false);
		updateserverth.start();

		CommandServer commandserver = new CommandServer(workerMap);
		Thread commandserverth = new Thread(commandserver, "Command Server");
		commandserverth.setDaemon(false);
		commandserverth.start();

		while (true)
			Thread.sleep(100000);
	}

	public static void emptyConfig() {
		Configuration.deleteAllNets();
	}
}