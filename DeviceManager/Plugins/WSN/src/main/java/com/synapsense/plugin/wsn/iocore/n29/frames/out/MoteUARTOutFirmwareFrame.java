package com.synapsense.plugin.wsn.iocore.n29.frames.out;

import com.synapsense.plugin.wsn.iocore.frames.out.MoteUARTOutFrame;
import com.synapsense.plugin.wsn.util.serializer.BigSerializer;

public class MoteUARTOutFirmwareFrame extends MoteUARTOutFrame {

	public MoteUARTOutFirmwareFrame(int index, int imagetype, byte[] code) {
		super((char) MoteUARTOutFrame.TYPE_FIRMWARE, (char) 132);
		BigSerializer ser = new BigSerializer(frame, 4);
		ser.packU_INT16((char) imagetype);
		ser.packU_INT16((char) index);
		System.arraycopy(code, 0, frame, 8, 128);
		this.setDelayTimeLeft(0);
	}

}
