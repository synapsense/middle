package com.synapsense.plugin.wsn.p3;

public class JawaPhaseData {
	public long nodeId;
	public int rack;
	public int feed;
	public double[] phaseVoltages = new double[3];
	public long timestamp;

	public JawaPhaseData() {

	}

	public JawaPhaseData(long node, int rack, int feed, double[] voltages) {
		this.nodeId = node;
		this.rack = rack;
		this.feed = feed;
		System.arraycopy(voltages, 0, this.phaseVoltages, 0, 3);
	}

	public String toString() {
		/**
		 * @TODO: StringBuilder
		 */
		return "<PhaseData Node " + Long.toHexString(nodeId) + " rack " + rack + " feed " + feed + " volts "
		        + phaseVoltages[0] + " " + phaseVoltages[1] + " " + phaseVoltages[2] + ">";
	}

}
