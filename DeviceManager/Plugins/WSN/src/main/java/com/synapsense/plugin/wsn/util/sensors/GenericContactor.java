package com.synapsense.plugin.wsn.util.sensors;

public class GenericContactor implements SensorConversionInterface, RangeConversionInterface, DeltaInterface {

	// private double min = 0;
	private double max = 0.5;
	private int delta;
	private double samplingRate = 60 * 5;
	private final double fifteenSampling = 60 * 60;

	static final int DOOR_CLOSE_THRESHOLD = 1;

	public double convertFromRaw(char raw) {

		if ((delta & 0x7fff) == 0) {
			return (raw >= DOOR_CLOSE_THRESHOLD) ? 1.0 : 0.0;
		}
		/* Count mode = convert kw demand */
		return ((double) raw * (max));// * (fifteenSampling / samplingRate);
	}

	public double convertFromRawCompensated(char raw, double other) {
		return convertFromRaw(raw);

	}

	public int getMoteDataType() {

		return 35;
	}

	public void setDataRanges(double m, double x) {
		// min = m;
		max = x;
	}

	public void setDelta(int d) {
		delta = d;
	}

	public void setSamplingRate(int rate) {
		return;
	}
}
