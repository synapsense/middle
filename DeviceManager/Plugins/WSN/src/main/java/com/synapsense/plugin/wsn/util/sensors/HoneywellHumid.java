package com.synapsense.plugin.wsn.util.sensors;

public class HoneywellHumid implements SensorConversionInterface {

	@Override
	public double convertFromRaw(char raw) {
		return ((double) raw / 16383.0) * 100.0;
	}

	@Override
	public double convertFromRawCompensated(char raw, double other) {
		return convertFromRaw(raw);
	}

	@Override
	public int getMoteDataType() {
		return 42;
	}
}
