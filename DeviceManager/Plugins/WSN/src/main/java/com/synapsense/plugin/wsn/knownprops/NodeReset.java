package com.synapsense.plugin.wsn.knownprops;

import org.apache.log4j.Logger;

import com.synapsense.plugin.wsn.NetworkWorker;
import com.synapsense.plugin.wsn.WsnConstants;
import com.synapsense.plugin.wsn.configuration.Configuration;
import com.synapsense.plugin.wsn.configuration.WSNNode;
import com.synapsense.plugin.wsn.iocore.frames.out.MoteUARTOutCommandFrame;
import com.synapsense.plugin.wsn.util.Constants;

class NodeReset extends NodePropertyWorker {
	private final static Logger logger = Logger.getLogger(NodeReset.class);

	@Override
	protected boolean performAction(String objectID, String propertyName, Object value) {
		WSNNode node = Configuration.getNodeByObjectID(objectID);
		if (node == null)
			return false;
		NetworkWorker netWorker = locateNetworkWorker(node.getNetworkID());
		if (netWorker == null) {
			logger.warn("Unknown networkID:" + node.getNetworkID() + " for node physID:"
			        + Long.toHexString(node.getPhysID()));
			return false;
		}
		logger.info("Sending RESET command to the node with physID: " + Long.toHexString(node.getPhysID()));

		MoteUARTOutCommandFrame frame = new MoteUARTOutCommandFrame((char) node.getLogicalID(),
		        Constants.COMMAND_ID_RESET_NODE, 0);
		netWorker.commandQ.add(frame);
		return true;
	}

	@Override
	protected boolean checkItsMine(String propertyName) {
		return propertyName.equals(WsnConstants.NETWORK_RESET);
	}
}
