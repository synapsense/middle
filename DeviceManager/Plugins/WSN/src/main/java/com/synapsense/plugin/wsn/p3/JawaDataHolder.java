package com.synapsense.plugin.wsn.p3;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.plugin.wsn.configuration.WSNNode;

public class JawaDataHolder {

	private static Log log = LogFactory.getLog(JawaDataHolder.class);

	public List<JawaData> jawaData = new ArrayList<JawaData>();
	public List<JawaPhaseData> phaseData = new ArrayList<JawaPhaseData>();
	public long checksum;
	/**
	 * Decode one packet's worth of data
	 * 
	 * @param physicalId
	 * @param data
	 */

	private final static int JAWA_DLEN = 4;
	private final static int MAX_FEEDS = 8;

	public JawaDataHolder(WSNNode node, byte[] data, long timestamp) {
		log.info("Deserializing " + data.length + " bytes ");
		/* Decode data */
		int pos = 0;

		long checksum = (data[pos++] << 24) & 0xff000000L;
		checksum |= (data[pos++] << 16) & 0xff0000;
		checksum |= (data[pos++] << 8) & 0xff00;
		checksum |= (data[pos++] & 0xff);
		this.checksum = checksum & 0xFFFFFFFFL;
		int vcode = data[pos++] & 0xff;
		log.info("Jawa data version code is " + vcode);
		int offset = data[pos++] & 0xff;
		log.info("Data offset: " + offset);

		if (offset >= 253) {
			/* Decode phase data first */

			int feed = 0;
			int rack = offset - 253;
			// for (rack = 0; rack < 3; rack++) {
			int feedmarker = data[pos++] & 0xff;

			log.info("Feed marker reports value of " + Integer.toHexString(feedmarker));

			for (feed = 0; feed < MAX_FEEDS; feed++) {

				/* Check the feedmarker bitset to see if this feed is present */
				if ((feedmarker & (1 << feed)) == 0)
					continue;

				double volts[] = new double[3];
				for (int v = 0; v < 3; v++) {
					log.info("Decoding phase data rack " + rack + " feed " + feed + " phase " + v);
					int vt = 0;
					vt = (data[pos++] << 8) & 0xff00;
					vt |= data[pos++] & 0xff;
					volts[v] = (double) vt / 10.0;
					log.info("Phase volts is " + volts[v]);
				}
				JawaPhaseData pd = new JawaPhaseData(node.getPhysID(), rack, feed, volts);
				pd.timestamp = timestamp;
				phaseData.add(pd);

			}
			// }
			// Check if there is any Jawa data
			if (data.length <= pos) {
				log.info("No Jawa data present");
				return;
			}
			offset = data[pos++] & 0xff;
			log.info("Data offset: " + offset);
		}

		int count = (data.length - pos) / JAWA_DLEN;
		log.info("Deserializing up to " + count + " jawa data packets from offset " + offset);
		JawaConfiguration p3c = (JawaConfiguration) node.getP3Configuration();

		if (p3c == null) {
			log.error("No P3Configuration - I can't unpack Jawa data without it");
			return;
		}

		if (offset > p3c.getJawas().size()) {
			log.info("Can't unpack here - offset > # of jawas ");
			return;
		}
		List<Jawa> jawaOffset = null;
		try {
			int endOfList = Math.min(offset + count, p3c.getJawas().size());
			jawaOffset = p3c.getJawas().subList(offset, endOfList);
		} catch (Exception e) {
			log.error("Sublist error", e);
		}

		log.info("Sublist returned me " + jawaOffset.size() + " jawas ");
		try {
			for (Jawa jawa : jawaOffset) {
				log.info("Deserializing Jawa with JID " + Long.toHexString(jawa.getJid()));

				int avgcur = data[pos++] & 0xff;

				int maxcur = data[pos++] & 0xff;
				int energy = (data[pos++] << 8) & 0xff00;
				energy |= data[pos++] & 0xff;
				double kwDemand = (double) energy / 1000.0; /* MJ from 10kJ */
				kwDemand *= 0.2777777777777778 * 4; /*
													 * kWh and 15 minute
													 * kwDemand
													 */

				JawaData jd = new JawaData();
				jd.avgCurrent = (double) avgcur / 10.0;
				jd.maxCurrent = (double) maxcur / 10.0;
				jd.demandPower = kwDemand;
				jd.jid = jawa.getJid();
				jd.feed = jawa.getFeed();
				jd.phase = jawa.getPhase();
				jd.physicalId = node.getPhysID();
				jd.timestamp = timestamp;
				jd.rack = jawa.getRack();
				if (avgcur == 255) {
					log.error("Jawa is not reporting: " + Long.toHexString(jawa.getJid()));
				} else {
					jawaData.add(jd);
					log.info(jd);
				}

			}
		} catch (Exception e) {
			log.error("Deserializing glitch", e);
		}

	}

}
