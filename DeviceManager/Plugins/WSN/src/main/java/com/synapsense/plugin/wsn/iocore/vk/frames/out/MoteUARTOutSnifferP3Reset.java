package com.synapsense.plugin.wsn.iocore.vk.frames.out;

import com.synapsense.plugin.wsn.iocore.frames.out.MoteUARTOutFrame;
import com.synapsense.plugin.wsn.util.serializer.BigSerializer;

public class MoteUARTOutSnifferP3Reset extends MoteUARTOutFrame {

	public MoteUARTOutSnifferP3Reset(long vknetid, byte ch, byte[] ids) {
		super((char) MoteUARTOutFrame.TYPE_SNIFFER_RESET, (char) 9);
		BigSerializer ser = new BigSerializer(frame, 4);
		ser.packU_INT32(vknetid);
		for (int i = 0; i < ids.length || i < 4; i++)
			ser.packU_INT8(ids[i]);
		for (int i = 0; i < 4 - ids.length; i++)
			ser.packU_INT8((byte) 0xFF);
		ser.packU_INT8(ch);
	}
}
