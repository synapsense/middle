package com.synapsense.plugin.wsn.util.sensors;

import com.synapsense.plugin.wsn.util.SensorStates;

public class LighthouseParticle implements SensorConversionInterface {

	public double convertFromRaw(char raw) {
		// voltage is measured in 12bits ADCCounts
		// 0 -> 0V ; 4095 -> 2.5V
		double voltage = ((double) raw / 4095) * 2.5;

		// 0.4V -> 0 count
		// 2.0V -> 4000 count
		// 0.2V -> service level
		// 0V sensor disconnected

		if (voltage < 0.1)
			return SensorStates.SENSOR_DISCONNECT; // to indicate that PC sensor
			                                       // is disconnected
		if (voltage < 0.3)
			return SensorStates.SENSOR_SERVICE; // to indicate that PC sensor
			                                    // require service

		double count = (voltage - 0.4) * 2500;

		return (count > 0) ? count : 0;
	}

	public double convertFromRawCompensated(char raw, double other) {
		return convertFromRaw(raw);
	}

	public int getMoteDataType() {
		return 6;
	}
}
