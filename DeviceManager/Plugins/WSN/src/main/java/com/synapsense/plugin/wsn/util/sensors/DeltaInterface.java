package com.synapsense.plugin.wsn.util.sensors;

public interface DeltaInterface {
	abstract public void setDelta(int delta);

	abstract public void setSamplingRate(int rate);
}
