package com.synapsense.plugin.wsn.iocore.n29.frames;

import com.synapsense.plugin.wsn.iocore.frames.MoteUARTBadFrame;
import com.synapsense.plugin.wsn.iocore.frames.MoteUARTFrame;
import com.synapsense.plugin.wsn.softbase29.packet.OSSPacket;

public class MoteUARTRcvdRadioFrame extends MoteUARTFrame {
	public OSSPacket pkt;
	public boolean recvdframe;

	public MoteUARTRcvdRadioFrame(byte[] header, byte[] data, String portName) throws MoteUARTBadFrame {
		super(header, portName);
		this.data = data;
		try {
			pkt = OSSPacket.deserialize(data);
		} catch (Exception e) {
			throw new MoteUARTBadFrame();
		}
		recvdframe = true;
	}

	public MoteUARTRcvdRadioFrame(int type, OSSPacket pkt, String portName) {
		super(type, portName);
		this.pkt = pkt;
		recvdframe = false;
	}
}
