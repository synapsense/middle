package com.synapsense.plugin.wsn.p3;

public interface P3Configuration {

	public long getChecksum();

	public byte[] serialize();

}
