package com.synapsense.plugin.wsn.iocore;

import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.BlockingQueue;

import com.synapsense.plugin.wsn.iocore.frames.MoteUARTFrame;
import com.synapsense.plugin.wsn.iocore.secure.AESCFBEngine;

public class SecureMoteCommReader extends MoteCommReader {
	private final InputStream inS;
	private final IRestartable mr;

	private final AESCFBEngine aesengine;

	public SecureMoteCommReader(BlockingQueue<MoteUARTFrame> s, InputStream i, String portName, IRestartable moteCommR,
	        byte[] iv, byte[] key, int version) {
		super(s, i, portName, moteCommR, version);
		mr = moteCommR;
		inS = i;
		aesengine = new AESCFBEngine(key, iv);

	}

	@Override
	protected void read(byte[] toread) throws IOException {
		byte rarray[] = new byte[1];
		for (int i = 0; i < toread.length; i++) {

			int rint = inS.read();
			if (rint == -1) {
				throw new IOException();
			}
			byte rbyte = (byte) rint;

			rarray[0] = rbyte;
			aesengine.decrypt(rarray);
			toread[i] = rarray[0];
		}
		return;
	}

}
