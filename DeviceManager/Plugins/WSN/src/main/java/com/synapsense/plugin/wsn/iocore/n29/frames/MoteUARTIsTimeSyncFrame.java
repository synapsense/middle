package com.synapsense.plugin.wsn.iocore.n29.frames;

import com.synapsense.plugin.wsn.iocore.frames.MoteUARTBadFrame;
import com.synapsense.plugin.wsn.iocore.frames.MoteUARTFrame;

public class MoteUARTIsTimeSyncFrame extends MoteUARTFrame {

	public boolean issynced;

	public MoteUARTIsTimeSyncFrame(byte[] header, byte[] data, String portName) throws MoteUARTBadFrame {
		super(header, portName);
		if (data[0] == 0)
			issynced = false;
		else
			issynced = true;
	}

}
