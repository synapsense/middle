package com.synapsense.plugin.wsn.softbase29.service;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.plugin.wsn.iocore.frames.out.MoteUARTOutCommandFrame;
import com.synapsense.plugin.wsn.iocore.frames.out.MoteUARTOutFrame;
import com.synapsense.plugin.wsn.iocore.frames.out.MoteUARTOutSetParameterFrame;
import com.synapsense.plugin.wsn.softbase29.SendError;
import com.synapsense.plugin.wsn.softbase29.Softbase;
import com.synapsense.plugin.wsn.util.serializer.BigSerializer;

public class NodeService implements Service {

	private static Log log = LogFactory.getLog(NodeService.class);

	public static final byte NS_SETPARAM = 2;
	public static final byte NS_COMMAND = 1;

	private Softbase softbase;

	public NodeService(Softbase softbase) {
		this.softbase = softbase;
	}

	public void close() {

	}

	public void send_command(MoteUARTOutCommandFrame message) {
		byte[] pkt = new byte[7];
		BigSerializer serial = new BigSerializer(pkt);
		serial.packU_INT8(NS_COMMAND);
		serial.packU_INT16((char) message.command);
		serial.packU_INT32(message.data);
		boolean bcast = false;
		if (message.destination == MoteUARTOutFrame.ALL_NODES) {
			bcast = true;
		}
		SecurityService ss = (SecurityService) softbase.getService(OSSServices.OSS_SERVICE_CSESSION);
		try {
			ss.sendSecurely(message, pkt, message.destination, OSSServices.OSS_SERVICE_NODE_CONTROL, bcast);
		} catch (SendError e) {
			message.onError(e);
		}
	}

	public void setparams(MoteUARTOutSetParameterFrame message) {
		int len = 7;
		if (message.ext != null)
			len = len + message.ext.length;
		byte[] pkt = new byte[len];
		BigSerializer serial = new BigSerializer(pkt);
		serial.packU_INT8(NS_SETPARAM);
		serial.packU_INT16((char) message.parameter);
		serial.packU_INT32(message.data);
		if (message.ext != null) {
			for (int i = 0; i < message.ext.length; i++)
				serial.packU_INT8(message.ext[i]);
		}
		SecurityService ss = (SecurityService) softbase.getService(OSSServices.OSS_SERVICE_CSESSION);
		if (message.destination == MoteUARTOutFrame.ALL_NODES) {
			try {
				ss.sendSecurely(message, pkt, (char) 0xFFFF, OSSServices.OSS_SERVICE_NODE_CONTROL, true);
			} catch (SendError e) {
				log.error("Broadcast send error: ", e);
			}
		} else {
			try {
				ss.sendSecurely(message, pkt, message.destination, OSSServices.OSS_SERVICE_NODE_CONTROL, false);
			} catch (SendError e) {
				message.onError(e);
			}
		}
	}

	public byte getServiceID() {
		return OSSServices.OSS_SERVICE_NODE_CONTROL;
	}

}
