package com.synapsense.plugin.wsn.iocore;

import java.io.IOException;

interface IRestartable extends IMoteComm {

	public Supervisor getSupervisor();

	public void connectPort() throws IOException;

	public void finish();
}
