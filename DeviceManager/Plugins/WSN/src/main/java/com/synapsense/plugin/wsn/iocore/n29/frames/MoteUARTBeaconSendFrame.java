package com.synapsense.plugin.wsn.iocore.n29.frames;

import com.synapsense.plugin.wsn.iocore.frames.MoteUARTBadFrame;
import com.synapsense.plugin.wsn.iocore.frames.MoteUARTFrame;
import com.synapsense.plugin.wsn.util.serializer.BigSerializer;
import com.synapsense.plugin.wsn.util.serializer.Serializer;

public class MoteUARTBeaconSendFrame extends MoteUARTFrame {
	public int hopcountTM;

	public MoteUARTBeaconSendFrame(byte[] header, byte[] data, String portName) throws MoteUARTBadFrame {
		super(header, portName);
		Serializer serial = new BigSerializer(data);
		hopcountTM = serial.unpackU_INT8();
	}

}
