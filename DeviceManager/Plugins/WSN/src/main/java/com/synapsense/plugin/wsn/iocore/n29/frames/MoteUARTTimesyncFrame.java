package com.synapsense.plugin.wsn.iocore.n29.frames;

import com.synapsense.plugin.wsn.iocore.frames.MoteUARTBadFrame;
import com.synapsense.plugin.wsn.iocore.frames.MoteUARTFrame;
import com.synapsense.plugin.wsn.util.serializer.BigSerializer;

public class MoteUARTTimesyncFrame extends MoteUARTFrame {

	public long globaltime;

	public MoteUARTTimesyncFrame(byte[] header, byte[] data, String portname) throws MoteUARTBadFrame {
		super(header, portname);
		this.data = data;

		BigSerializer serial = new BigSerializer(data);

		globaltime = serial.unpackU_INT32();

	}

	public String toString() {
		return "TS Frame time " + Long.toString(globaltime);
	}

}
