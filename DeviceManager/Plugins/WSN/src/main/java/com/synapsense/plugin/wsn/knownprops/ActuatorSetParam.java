package com.synapsense.plugin.wsn.knownprops;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;

import com.synapsense.plugin.messages.WSNActuatorCommandResponseBuilder;
import com.synapsense.plugin.wsn.NetworkWorker;
import com.synapsense.plugin.wsn.SynapStore;
import com.synapsense.plugin.wsn.WsnConstants;
import com.synapsense.plugin.wsn.configuration.Configuration;
import com.synapsense.plugin.wsn.configuration.WSNActuator;
import com.synapsense.plugin.wsn.configuration.WSNNode;
import com.synapsense.plugin.wsn.msgbus.Message;
import com.synapsense.plugin.wsn.msgbus.MessageHandlerFromJava;
import com.synapsense.plugin.wsn.msgbus.MsgResendAppInit;

class ActuatorSetParam extends NodePropertyWorker {
	private final static Logger logger = Logger.getLogger(ActuatorSetParam.class);

	@Override
	protected boolean performAction(String objectID, String propertyName, Object value) {

		final WSNActuator actuator = Configuration.getActuatorByObjectID(objectID);
		if (actuator == null)
			return false;
		if (!(value instanceof Integer))
			throw new IllegalArgumentException(value + " is not an integer");
		final int numvalue = (Integer) value;

		if (propertyName.equals(WsnConstants.ACTUATOR_NUMBER)) {
			actuator.setActuatorNumber(numvalue);
			return true;
		} else if (propertyName.equals(WsnConstants.ACTUATOR_DIRECTION)) {
			if (numvalue != WSNActuator.DIRECTION_CW && numvalue != WSNActuator.DIRECTION_CCW)
				return false;
			actuator.setDirection(numvalue);
			return true;
		} else if (propertyName.equals(WsnConstants.ACTUATOR_FAILSAFESETPOINT)) {
			if (numvalue > 100)
				return false;
			actuator.setFailsafesetpoint(numvalue);
			return true;
		} else if (propertyName.equals(WsnConstants.ACTUATOR_MINPOINT)) {
			if (numvalue > 100)
				return false;
			actuator.setMinpoint(numvalue);
			return true;
		} else if (propertyName.equals(WsnConstants.ACTUATOR_MAXPOINT)) {
			if (numvalue > 100)
				return false;
			actuator.setMaxpoint(numvalue);
			return true;
		} else if (propertyName.equals(WsnConstants.ACTUATOR_TRAVELTIME)) {
			actuator.setTravelTime(numvalue);
			return true;
		} else if (propertyName.equals(WsnConstants.ACTUATOR_POSITION_TOLERANCE)) {
			actuator.setPositionTolerance(numvalue);
			return true;
		} else if (propertyName.equals(WsnConstants.ACTUATOR_NEXTVALUE)) {

			// Setting Nextvalue triggers sending the command
			actuator.setNextValue(numvalue);
			WSNNode node = actuator.getNode();
			NetworkWorker netWorker = locateNetworkWorker(node.getNetworkID());
			if (netWorker == null) {
				logger.warn("Unknown networkID:" + node.getNetworkID() + " for node physID:"
				        + Long.toHexString(node.getPhysID()));
				return false;
			}
			logger.info("Sending next Set Point command to the node with physID: " + Long.toHexString(node.getPhysID()));

			Message msg = new MsgResendAppInit(node, actuator);
			MessageHandlerFromJava c = new MessageHandlerFromJava() {
				public void onSuccess(Message m) {
					if (SynapStore.transport != null) {
						SynapStore.transport.sendData(actuator.getId(), WsnConstants.ACTUATOR_COMMAND_SENT,
						        new WSNActuatorCommandResponseBuilder(WsnConstants.ACTUATOR_NEXTVALUE, numvalue, true)
						                .getMessage());
					} else {
						try {
							PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("ActuatorCommand.txt",
							        true)));
							SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss.SSS");
							Date date = new Date(System.currentTimeMillis());
							String curtimestring = sdf.format(date).toString();
							out.println(curtimestring + ": " + actuator.getId() + " " + WsnConstants.ACTUATOR_NEXTVALUE
							        + " " + numvalue + " SUCCESS");
							out.close();
						} catch (IOException e) {
						}
					}
				}

				public void onError(Message m, Throwable t) {
					if (SynapStore.transport != null) {
						SynapStore.transport.sendData(actuator.getId(), WsnConstants.ACTUATOR_COMMAND_FAILED,
						        new WSNActuatorCommandResponseBuilder(WsnConstants.ACTUATOR_NEXTVALUE, numvalue, false)
						                .getMessage());
					} else {
						try {
							PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("ActuatorCommand.txt",
							        true)));
							SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss.SSS");
							Date date = new Date(System.currentTimeMillis());
							String curtimestring = sdf.format(date).toString();
							out.println(curtimestring + ": " + actuator.getId() + " " + WsnConstants.ACTUATOR_NEXTVALUE
							        + " " + numvalue + " FAIL");
							out.close();
						} catch (IOException e) {
						}
					}
				}
			};
			msg.addMessageHandlerJava(c);
			netWorker.commandQ.add(msg);
			return true;
		}
		return false;
	}

	@Override
	protected boolean checkItsMine(String propertyName) {
		if (propertyName.equals(WsnConstants.ACTUATOR_NUMBER))
			return true;
		if (propertyName.equals(WsnConstants.ACTUATOR_NEXTVALUE))
			return true;
		if (propertyName.equals(WsnConstants.ACTUATOR_DIRECTION))
			return true;
		if (propertyName.equals(WsnConstants.ACTUATOR_FAILSAFESETPOINT))
			return true;
		if (propertyName.equals(WsnConstants.ACTUATOR_MINPOINT))
			return true;
		if (propertyName.equals(WsnConstants.ACTUATOR_MAXPOINT))
			return true;
		if (propertyName.equals(WsnConstants.ACTUATOR_TRAVELTIME))
			return true;
		if (propertyName.equals(WsnConstants.ACTUATOR_POSITION_TOLERANCE))
			return true;
		return false;
	}
}
