package com.synapsense.plugin.wsn.smota2;

public class Firmware {
	public static final int IMAGE_NORM = 1;
	public static final int IMAGE_DIAG = 2;
	public static final int IMAGE_SMOTA = 3;

	public static final int PACKETSIZE = 96;
	public static final int UARTMSGSIZE = 128;

	public static final int MSPBASEADDR = 0x4000;
	public static final int MSPIMAGESIZE = 48 * 1024;
	public static final int PMBASEADDR = 0;
	public static final int PMIMAGESIZE = 16 * 1024;
	public static final int PMSIGADDR = 0x3FFD;
	public static final int EFMBOOTADDR = 0;
	public static final int EFMBOOTSIZE = 8 * 1024;
	public static final int EFMSIGADDR = 0x1FFA;
	public static final int EFMBASEADDR = 0x2000;
	public static final int EFMIMAGESIZE = 96 * 1024;

	public static final int EFMVERSION_LOCATION = 0x19FFC;

	private byte imagetype;
	private byte[] platforms;
	private long[] versions;
	private byte[][] codes;
	private byte[] gatewaycode;
	private long gatewaycodeversion;

	public Firmware(byte imagetype, byte[] platforms, long[] versions, byte[][] codes, long gatewaycodeversion,
	        byte[] gatewaycode) {
		this.imagetype = imagetype;
		this.platforms = platforms;
		this.versions = versions;
		this.codes = codes;
		this.gatewaycode = gatewaycode;
		this.gatewaycodeversion = gatewaycodeversion;
	}

	public byte[] get(int index) {
		byte[] ret = new byte[PACKETSIZE];
		int platformindex = index / (MSPIMAGESIZE / PACKETSIZE);
		int codeindex = index % (MSPIMAGESIZE / PACKETSIZE);
		System.arraycopy(codes[platformindex], codeindex * 96 + 0, ret, 0, PACKETSIZE);
		return ret;
	}

	public byte[] getGatewaycode(int index) {
		byte[] ret = new byte[UARTMSGSIZE];
		System.arraycopy(gatewaycode, 128 * index + 0, ret, 0, UARTMSGSIZE);
		return ret;
	}

	public int getgatewaycodecount() {
		return MSPIMAGESIZE / UARTMSGSIZE;
	}

	public boolean gatewayupdate() {
		return gatewaycode != null;
	}

	public boolean nodeupdate() {
		return platforms != null;
	}

	public byte getimagetype() {
		return imagetype;
	}

	public byte[] getplatforms() {
		return platforms;
	}

	public long[] getversions() {
		return versions;
	}

	public int getpacketcount() {
		return platforms.length * MSPIMAGESIZE / PACKETSIZE;
	}
}
