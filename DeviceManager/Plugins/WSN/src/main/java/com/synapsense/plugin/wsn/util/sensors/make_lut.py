#!/usr/bin/env python

import csv

f = open('jcurve.csv', 'r')
reader = csv.reader(f, delimiter=",", quotechar='"')

for row in reader:
    print "therm_lut.put(%0.02f, %0.02f);" % (float(row[1]), float(row[0]))
