package com.synapsense.plugin.wsn.msgbus;

public class MsgDuoData extends Message {

	public byte[] data;

	public char deviceId;
	public long timestamp;

	public MsgDuoData() {
		super();
	}

	public MsgDuoData(byte[] d, char deviceId, long time) {
		super();
		this.deviceId = deviceId;
		this.data = d;
		this.timestamp = time;
	}

}