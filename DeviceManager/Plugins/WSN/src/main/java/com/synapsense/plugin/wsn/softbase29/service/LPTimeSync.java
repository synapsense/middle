package com.synapsense.plugin.wsn.softbase29.service;

import java.util.Iterator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.plugin.wsn.iocore.n29.frames.out.MoteUARTOutBaseCommandFrame;
import com.synapsense.plugin.wsn.softbase29.GateWay;
import com.synapsense.plugin.wsn.softbase29.Softbase;
import com.synapsense.plugin.wsn.softbase29.state.OSSNHT;

public class LPTimeSync implements Runnable, Service {

	private Softbase softbase;
	private int rate;
	private boolean timercvd;
	private final Object timercvdlock;
	private final Object getglobaltimelock;
	private volatile GateWay tmgateway;
	private long tempglobaltime;
	private long prevreportedtime = 0;
	private long reportedtime;

	private static Log log = LogFactory.getLog(LPTimeSync.class);
	private volatile boolean interrupted = false;

	public LPTimeSync(Softbase softbase) {
		this.softbase = softbase;
		rate = OSSNHT.INIT_BEACON_RATE;
		timercvdlock = new Object();
		getglobaltimelock = new Object();
	}

	public void close() {
		tmgateway = null;
		interrupted = true;
	}

	public void run() {
		if (tmgateway == null) {
			while (!interrupted && !pickMaster())
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					return;
				}
		}
		while (!interrupted) {

			synchronized (timercvdlock) {
				timercvd = false;
				try {
					timercvdlock.wait((long) ((1 << rate) * 1.5));
				} catch (InterruptedException e) {
				}
			}

			if (!timercvd) {
				if (tmgateway != null) {
					log.error("Timeout on receiving time info");
					tmgateway.setAlive(false);
				}
				while (!pickMaster() && !interrupted) {

					try {
						Thread.sleep(5000);
					} catch (InterruptedException e) {
						interrupted = true;
						break;
					}
				}

			} else {
				if (softbase.isLPLmode() && prevreportedtime != 0) {
					if (reportedtime - prevreportedtime < (long) ((1 << rate) * 0.5) && !softbase.isSMOTAmode()) {
						// Timemaster is not LPL mode. Send LPL message
						softbase.gatewaySendInitState(tmgateway);
					}
				}
				prevreportedtime = reportedtime;
			}
		}
	}

	public void TimeRcvd(GateWay gateway) {
		if (gateway != null && !gateway.equals(tmgateway)) {
			// Received time from different gateway
			MoteUARTOutBaseCommandFrame frame = new MoteUARTOutBaseCommandFrame(
			        MoteUARTOutBaseCommandFrame.NOTTIMEMASTER);
			gateway.setTimemaster(false);
			if (!gateway.sendFrame(frame)) {
				log.error("not responding from setting nottimemaster");
				gateway.setAlive(false);
			}
			return;
		}
		synchronized (timercvdlock) {
			log.trace("Time received");
			timercvd = true;
			timercvdlock.notify();
			reportedtime = System.currentTimeMillis();
		}
	}

	public void setRate(int rate) {
		this.rate = rate;
		TimeRcvd(tmgateway);
	}

	public long getGlobalTime() {
		if (tmgateway == null)
			return 0;

		MoteUARTOutBaseCommandFrame frame = new MoteUARTOutBaseCommandFrame(MoteUARTOutBaseCommandFrame.GETGLOBALTIME);

		synchronized (getglobaltimelock) {
			if (!tmgateway.sendFrame(frame)) {
				log.error("not responding from getting global time");
				tmgateway.setAlive(false);
				if (pickMaster()) {
					return getGlobalTime();
				} else {
					return 0;
				}
			}
			try {
				getglobaltimelock.wait();
			} catch (InterruptedException e) {
			}
		}
		return tempglobaltime;
	}

	public void setGlobalTime(long time) {
		synchronized (getglobaltimelock) {
			tempglobaltime = time;
			getglobaltimelock.notify();
		}
	}

	public synchronized void clearGateway(GateWay gw) {
		if (gw.equals(tmgateway)) {
			tmgateway = null; // Clear out the removed gateway
			pickMaster(); // Attempt to find a new gateway on removal
			if (tmgateway != null && gw.equals(tmgateway)) {
				tmgateway = null; // Don't set a gateway to be the same
			}
		}
	}

	public synchronized boolean pickMaster() {
		MoteUARTOutBaseCommandFrame frame = new MoteUARTOutBaseCommandFrame(MoteUARTOutBaseCommandFrame.TIMEMASTER);
		if (tmgateway != null)
			tmgateway.setTimemaster(false);
		tmgateway = findMaster();
		if (tmgateway == null) {
			log.error("SETTING TIME MASTER FAIL");
			synchronized (timercvdlock) {
				timercvd = false;
				timercvdlock.notify();
			}
			return false;
		}
		if (!tmgateway.sendFrame(frame)) {
			log.trace("not responding from setting timemaster");
			tmgateway.setAlive(false);
			return pickMaster();
		}
		tmgateway.setTimemaster(true);
		TimeRcvd(tmgateway);
		log.trace("Time Master is set to " + tmgateway);
		return true;
	}

	private GateWay findMaster() {
		Iterator<com.synapsense.plugin.wsn.softbase.GateWay> iter = softbase.getGateways().iterator();
		while (iter.hasNext()) {
			GateWay gateway = (GateWay) iter.next();
			if (gateway.isAlive() && gateway.isTimeSynced())
				return gateway;
		}
		// No gateway is time synced
		iter = softbase.getGateways().iterator();
		while (iter.hasNext()) {
			GateWay gateway = (GateWay) iter.next();
			if (gateway.isAlive())
				return gateway;
		}
		return null;
	}

	public GateWay getMaster() {
		return tmgateway;
	}

	public synchronized void setMaster(GateWay gateway) {
		if (tmgateway != null && tmgateway != gateway) {
			log.error("Setting Time Master to " + gateway + " Fail. Current Time Master is " + tmgateway);
			MoteUARTOutBaseCommandFrame frame = new MoteUARTOutBaseCommandFrame(
			        MoteUARTOutBaseCommandFrame.NOTTIMEMASTER);
			if (!gateway.sendFrame(frame)) {
				log.trace("not responding from setting nottimemaster");
				gateway.setAlive(false);
			}
			return;
		}
		tmgateway = gateway;
		gateway.setTimemaster(true);
		log.trace("Time Master is already set to " + gateway);
	}

	public byte getServiceID() {
		return OSSServices.OSS_SERVICE_LPTIMESYNC;
	}

}
