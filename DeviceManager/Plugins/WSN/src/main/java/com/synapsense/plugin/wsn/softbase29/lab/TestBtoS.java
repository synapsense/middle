package com.synapsense.plugin.wsn.softbase29.lab;

import java.util.Timer;
import java.util.TimerTask;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.plugin.wsn.softbase29.SendError;
import com.synapsense.plugin.wsn.softbase29.Softbase;
import com.synapsense.plugin.wsn.softbase29.SoftbaseNWK;
import com.synapsense.plugin.wsn.softbase29.packet.RadioMsg;

public class TestBtoS implements Runnable {
	private Softbase softbase;
	private Timer timer;
	byte[] data;
	private static Log log = LogFactory.getLog(TestBtoS.class);

	public TestBtoS(Softbase softbase) {
		this.softbase = softbase;
		data = new byte[1];
		data[0] = 0;
		timer = new Timer("TestThread Timer");
		TimerTask task = new SendTask();
		timer.scheduleAtFixedRate(task, 15 * 60000, 11 * 60000);
	}

	public void run() {
		RadioMsg msg = null;
		while (true) {
			try {
				msg = softbase.nwk.receive((byte) 11);
			} catch (InterruptedException e) {
				break;
			}
			if (msg.data[0] == 0) {
				log.trace("TBTEST: " + Integer.toHexString(msg.sourceid) + " Missing " + msg.data[1] + " Received "
				        + msg.data[2]);
			}
			if (msg.data[0] == 1) {
				log.trace("TBTEST: " + Integer.toHexString(msg.sourceid) + " Sent " + msg.data[1] + " for "
				        + msg.data[2] + " times.");
			}
		}
	}

	class SendTask extends TimerTask {
		public void run() {
			log.trace("TBTEST: Sending " + data[0]);
			try {
				softbase.nwk.send(null, data, (char) 0, 10, SoftbaseNWK.Protocol_ToSensors);
			} catch (SendError e) {
				log.error("Send error occurred: ", e);
			}
			data[0]++;
		}
	}
}
