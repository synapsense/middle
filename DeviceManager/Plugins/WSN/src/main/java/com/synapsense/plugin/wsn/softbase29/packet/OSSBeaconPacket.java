package com.synapsense.plugin.wsn.softbase29.packet;

import com.synapsense.plugin.wsn.util.serializer.Serializer;

public class OSSBeaconPacket extends OSSPacket {

	public int prstate_powparent;

	// ritu changes .. preferred gateway info
	public int preferredBase;
	public int secondaryBase;
	public int hopPbase;
	public int hopSbase;

	public char slots;
	public int hoptmrateLPLCh;
	public long nwkstarttime;
	public char[] reqid;
	public long seq;
	public long txtime;

	// ritu changes .. ch adaptation
	public char chDuration[];

	public void deserializeDataWithSerializer(Serializer serial) {
		this.prstate_powparent = serial.unpackU_INT8(); // 0-1 Pow Router
		                                                // state,2 parent power
		                                                // state, 3-7 hop count
		                                                // to Base

		// ritu changes .. preferred gateway info
		this.preferredBase = serial.unpackU_INT16();
		this.secondaryBase = serial.unpackU_INT16();
		this.hopPbase = serial.unpackU_INT8();
		this.hopSbase = serial.unpackU_INT8();

		this.slots = serial.unpackU_INT16();
		this.hoptmrateLPLCh = serial.unpackU_INT16();
		this.nwkstarttime = serial.unpackU_INT32();
		// ritu changes .. ch adaptation
		this.chDuration = new char[4];
		this.chDuration[0] = serial.unpackU_INT16();
		this.chDuration[1] = serial.unpackU_INT16();
		this.chDuration[2] = serial.unpackU_INT16();
		this.chDuration[3] = serial.unpackU_INT16();
		switch (mac_type) {
		case OSSPacket.MAC_TIMEBEACON:
		case OSSPacket.MAC_RETIMEBEACON:
			seq = serial.unpackU_INT32();
			txtime = serial.unpackU_INT32();
			break;
		case OSSPacket.MAC_TIMEREQBEACON:
			seq = serial.unpackU_INT32();
			reqid = new char[2];
			reqid[0] = serial.unpackU_INT16();
			reqid[1] = serial.unpackU_INT16();
			// reqid[2] = serial.unpackU_INT16();
			// reqid[3] = serial.unpackU_INT16();
			break;
		default:
			serial.unpackU_INT32();
			serial.unpackU_INT32();
		}
	}

	public byte[] serialize() {
		return null;
	}

	public static void setHopTM(OSSBeaconPacket pk, int hoptm) {
		pk.hoptmrateLPLCh = (pk.hoptmrateLPLCh & 0x03FF) | (hoptm << 10);
	}

	public static int getHopTM(OSSBeaconPacket pk) {
		return (pk.hoptmrateLPLCh & 0xFC00) >> 10;
	}

	public static void setBrate(OSSBeaconPacket pk, int Brate) {
		pk.hoptmrateLPLCh = (pk.hoptmrateLPLCh & 0xFC1F) | (Brate << 5);
	}

	public static int getBrate(OSSBeaconPacket pk) {
		return (pk.hoptmrateLPLCh & 0x03E0) >> 5;
	}

	public static void setLPL(OSSBeaconPacket pk, int lpl) {
		pk.hoptmrateLPLCh = (pk.hoptmrateLPLCh & 0xFFEF) | (lpl << 4);
	}

	public static int getLPL(OSSBeaconPacket pk) {
		return (pk.hoptmrateLPLCh & 0x0010) >> 4;
	}

	public static void setCh(OSSBeaconPacket pk, byte ch) {
		pk.hoptmrateLPLCh = (pk.hoptmrateLPLCh & 0xFFF0) | ch;
	}

	public static byte getCh(OSSBeaconPacket pk) {
		return (byte) (pk.hoptmrateLPLCh & 0x000F);
	}

	public static void bpk_setRouterState(OSSBeaconPacket pk, int state) {
		pk.prstate_powparent = (pk.prstate_powparent & 0x3F) | (state << 6);
	}

	public static void bpk_setParentState(OSSBeaconPacket pk, int state) {
		pk.prstate_powparent = (pk.prstate_powparent & 0xDF) | (state << 5);
	}

	public static int bpk_getRouterState(OSSBeaconPacket pk) {
		return (pk.prstate_powparent & 0xC0) >> 6;
	}

	public static int bpk_getParentState(OSSBeaconPacket pk) {
		return (pk.prstate_powparent & 0x20) >> 5;
	}
}
