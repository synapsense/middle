package com.synapsense.plugin.wsn.util.sensors;

import com.synapsense.plugin.wsn.util.SensorStates;

public class Sid2ExternalThermistorG implements SensorConversionInterface {

	public double convertFromRaw(char raw) {
		// voltage is measured in 12bits ADCCounts
		// 0 -> 0V
		// 4095 -> 2.5V
		double rawV = raw;
		double Vadc = rawV / 4095 * 2.5;

		double Vsup = 12;
		double beta = 3580; // 3594.7;
		double T0 = 298.15; // Degrees Kelvin
		double R0 = 10000;

		double resistor = 649;

		try {

			double Rcalc = -(resistor * (Vadc - Vsup)) / Vadc;
			double tempK = (beta * T0) / (beta + Math.log(Rcalc / R0) * T0);
			double tempC = tempK - 273.15;

			double tempF = (9.0 / 5.0) * tempC + 32;

			if (tempF <= SensorStates.ABSZERO_DEGREEF + 10) {
				return SensorStates.SENSOR_DISCONNECT;
			}

			if (tempF > 3000) {
				return SensorStates.SENSOR_DISCONNECT;
			}

			return tempF;
		} catch (Exception e) {
			return SensorStates.SENSOR_DISCONNECT;
		}
	}

	public double convertFromRawCompensated(char raw, double vRef) {

		return convertFromRaw(raw);
	}

	public int getMoteDataType() {

		return 25;
	}

}
