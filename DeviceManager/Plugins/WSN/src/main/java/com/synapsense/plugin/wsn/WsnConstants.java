package com.synapsense.plugin.wsn;

public class WsnConstants {

	public static final String TYPE_NETWORK = "WSNNETWORK";
	public static final String TYPE_GATEWAY = "WSNGATEWAY";
	public static final String TYPE_NODE = "WSNNODE";
	public static final String TYPE_SENOR = "WSNSENSOR";
	public static final String TYPE_ACTUATOR = "WSNACTUATOR";

	public static final String NETWORK_SAMPLING = "sampling";
	public static final String NETWORK_PAN_ID = "panId";
	public static final String NETWORK_VERSION = "version";
	public static final String NETWORK_NAME = "name";
	public static final String NETWORK_IDENTIFY = "identify";
	public static final String NETWORK_RESET = "reset";
	public static final String NETWORK_WAKE_UP = "wakeup";
	public static final String NETWORK_NHT = "nht";
	public static final String NETWORK_STATISTICS = "statistics";
	public static final String NETWORK_ROUTE = "route";
	public static final String NETWORK_POWER_CONFIG = "powerConfig";

	public static final String NETWORK_SECURITYSUITE = "networkSecuritySuite";
	public static final String NETWORK_PRIVATEKEY = "networkPrivateKey";
	public static final String NETWORK_PUBLICKEY = "networkPublicKey";
	public static final String NETWORK_KEYSIGNATURE = "networkKeySignature";

	public static final String NODE_LOGICAL_ID = "id";
	public static final String NODE_PHYS_ID = "mac";
	public static final String NODE_PLATFORM_ID = "platformId";
	public static final String NODE_SAMPLING = "period";
	public static final String NODE_CHECKSUM = "checksum";
	public static final String NODE_ROUTE = "route";

	public static final String SENSOR_CHANNEL = "channel";
	public static final String SENSOR_MAX = "max";
	public static final String SENSOR_MIN = "min";
	public static final String SENSOR_TYPE = "type";
	public static final String SENSOR_SAMPLING = "sampling";
	public static final String SENSOR_LAST_VALUE = "lastValue";
	public static final String SENSOR_SMART_SEND_THRESHOLD = "smartSendThreshold";
	public static final String SENSOR_DELTA = "delta";
	public static final String SENSOR_MODE = "mode";
	public static final String SENSOR_INTERVAL = "interval";
	public static final String SENSOR_ENABLED = "enabled";
	public static final String SENSOR_PULSE_WIDTH = "pulseWidth";
	public static final String SENSOR_DELTA_T_LOW = "smartSendDeltaTempLow";
	public static final String SENSOR_DELTA_T_HIGH = "smartSendDeltaTempHigh";
	public static final String SENSOR_CRITICAL_POINT = "smartSendCriticalTemp";
	public static final String SENSOR_SUBSAMPLE_INTERVAL = "subSamples";

	public static final String GATEWAY_PROTOCOL = "protocol";
	public static final String GATEWAY_ADDRESS = "address";
	public static final String GATEWAY_KEY = "key";
	public static final String GATEWAY_STATUS = "status";

	public static final String ACTUATOR_NUMBER = "actuatorID";
	public static final String ACTUATOR_NEXTVALUE = "nextValue";
	public static final String ACTUATOR_DIRECTION = "direction";
	public static final String ACTUATOR_FAILSAFESETPOINT = "failsafeSetPoint";
	public static final String ACTUATOR_MINPOINT = "minimumSetting";
	public static final String ACTUATOR_MAXPOINT = "maximumSetting";
	public static final String ACTUATOR_TRAVELTIME = "travelTime";
	public static final String ACTUATOR_POSITION = "actuatorposition";
	public static final String ACTUATOR_POSITION_TOLERANCE = "positionTolerance";
	public static final String ACTUATOR_COMMAND_SENT = "actuatorcommandsent";
	public static final String ACTUATOR_COMMAND_FAILED = "actuatorcommandfailed";

	public enum SensorOptions {
	AMIN("aMin"), AMAX("aMax"), RMIN("rMin"), RMAX("rMax"), SUB_SAMPLES("subSamples"), DELTA("delta"), SCALE_MIN(
	        "scaleMin"), SCALE_MAX("scaleMax"), SENSOR_ENABLED("enabled"), SENSOR_MODE("mode"), SENSOR_INTERVAL(
	        "interval"), SENSOR_SMART_SEND_THRESHOLD("smartSendThreshold"), SENSOR_PULSE_WIDTH("pulseWidth"), SENSOR_DELTA_T_LOW(
	        "smartSendDeltaTempLow"), SENSOR_DELTA_T_HIGH("smartSendDeltaTempHigh"), SENSOR_CRITICAL_POINT(
	        "smartSendCriticalTemp");
	private final String value;

	private SensorOptions(final String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}
	}

	public enum NodeOptions {
	CHECKSUM("checksum"), PROGRAM("program");
	private final String value;

	private NodeOptions(final String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}
	}
}
