package com.synapsense.plugin.wsn.util.sensors;

public class ActuatorPosition implements SensorConversionInterface {

	public double convertFromRaw(char raw) {
		return raw;
	}

	public double convertFromRawCompensated(char raw, double other) {
		return raw;
	}

	public int getMoteDataType() {
		return 38;
	}

}
