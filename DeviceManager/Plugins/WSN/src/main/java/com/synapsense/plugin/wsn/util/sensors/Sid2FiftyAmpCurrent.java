package com.synapsense.plugin.wsn.util.sensors;

public class Sid2FiftyAmpCurrent implements SensorConversionInterface {

	public double convertFromRaw(char raw) {
		// voltage is measured in 12bits ADCCounts
		// 0 -> 0V ; 4095 -> 2.5V
		double voltage = ((double) raw / 4095) * 2.5;

		// 0.0V -> 0 Amps
		// 2.48V -> 50 Amps
		double amps = (voltage * 4.03 * 5);
		return amps;
	}

	public double convertFromRawCompensated(char raw, double other) {
		return convertFromRaw(raw);
	}

	public int getMoteDataType() {
		return 19;
	}
}
