package com.synapsense.plugin.wsn.iocore.frames;

import java.lang.reflect.Constructor;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

public class FrameFactory {

	private static final Logger log = Logger.getLogger(FrameFactory.class);

	private static Map<Integer, Map<Integer, Class<? extends MoteUARTFrame>>> _frameList = null;

	static {
		if (_frameList == null)
			_frameList = new HashMap<Integer, Map<Integer, Class<? extends MoteUARTFrame>>>();

	}

	private static Map<Integer, Class<? extends MoteUARTFrame>> getForVersion(int vers) {
		Map<Integer, Class<? extends MoteUARTFrame>> vf = _frameList.get(vers);
		if (vf == null) {
			vf = new HashMap<Integer, Class<? extends MoteUARTFrame>>();
			_frameList.put(vers, vf);
		}
		return vf;
	}

	public static void registerFrame(int frametype, int version, Class<? extends MoteUARTFrame> frameClass) {
		if (_frameList == null)
			_frameList = new HashMap<Integer, Map<Integer, Class<? extends MoteUARTFrame>>>();
		Map<Integer, Class<? extends MoteUARTFrame>> vf = getForVersion(version);
		vf.put(frametype, frameClass);
	}

	public static MoteUARTFrame createFromId(int framet, int version, MoteUARTFrame base) {
		MoteUARTFrame interf = null;

		Class<? extends MoteUARTFrame> classRef = getForVersion(version).get(framet);

		if (classRef == null) {
			log.error("Cannot find MoteUARTFrame impl for ver=" + version + ", framet=" + framet);
			return null;
		}

		try {
			Constructor<? extends MoteUARTFrame> cons = classRef.getConstructor(byte[].class, byte[].class,
			        String.class);
			interf = cons.newInstance(base.getHeader(), base.getDataBuffer(), base.portName);

			return interf;
		} catch (Exception e) {
			log.error("Cannot load MoteUARTFrame impl for ver=" + version + ", framet=" + framet, e);
			return null;
		}

	}

}
