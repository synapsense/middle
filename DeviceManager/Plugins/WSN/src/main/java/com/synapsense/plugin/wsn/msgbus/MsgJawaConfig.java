package com.synapsense.plugin.wsn.msgbus;

import com.synapsense.plugin.wsn.p3.JawaConfiguration;

public class MsgJawaConfig extends Message {

	public JawaConfiguration config;
	public char deviceId;

	public MsgJawaConfig(char device, JawaConfiguration config) {
		super();
		this.config = config;
		this.deviceId = device;
	}

}