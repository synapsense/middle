package com.synapsense.plugin.wsn.servers;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.Socket;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import org.apache.log4j.Logger;

import com.synapsense.plugin.wsn.NetworkWorker;
import com.synapsense.plugin.wsn.smota2.Firmware;
import com.synapsense.plugin.wsn.softbase29.Softbase;
import com.synapsense.plugin.wsn.util.SensorNetwork;

public class UpdateServerThread extends Thread {
	private HashMap<SensorNetwork, NetworkWorker> workerMap;
	private DataInputStream input;
	private PrintStream out;
	private Socket socket = null;
	private static Logger log = Logger.getLogger(UpdateServerThread.class);
	private static final String smotaversion = "2.0";
	private static final String password = "synap$ense";

	public UpdateServerThread(Socket socket,
			HashMap<SensorNetwork, NetworkWorker> workerMap) {
		super("Update Server Thread");
		this.socket = socket;
		this.workerMap = workerMap;
	}

	public void println(String s) {
		out.println(s);
	}

	public boolean askRetry() {
		try {
			while (input.available() > 0) {
				input.read();
			}
			out.println("FAILFAILFAIL");
			int value = input.read();
			if (value == 0)
				return false;
			else
				return true;
		} catch (IOException e) {
			log.error("Error in askRetry", e);
		}
		return false;
	}

	public boolean askNodeJoin() {
		try {
			while (input.available() > 0) {
				input.read();
			}
			out.println("JOINJOINJOIN");
			int value = input.read();
			if (value == 0)
				return false;
			else
				return true;
		} catch (IOException e) {
			log.error("Error in askNodeJoin", e);
		}
		return false;
	}

	public boolean askNodeHear() {
		try {
			while (input.available() > 0) {
				input.read();
			}
			out.println("HEARHEARHEAR");
			int value = input.read();
			if (value == 0)
				return false;
			else
				return true;
		} catch (IOException e) {
			log.error("Error in askNodeHear", e);
		}
		return false;
	}

	public void waitforenter() {
		try {
			while (input.available() > 0) {
				input.read();
			}
			out.println("ENTERENTER");
			input.read();
		} catch (IOException e) {
			log.error("Error in waitforenter", e);
		}
	}

	public void run() {
		try {
			log.info("Connected from " + socket.getInetAddress());
			input = new DataInputStream(socket.getInputStream());
			out = new PrintStream(socket.getOutputStream());
			byte[] session = new byte[8];
			SecureRandom random = new SecureRandom();
			random.nextBytes(session);
			out.write(session);
			MessageDigest hash = MessageDigest.getInstance("SHA-1");
			hash.update(session);
			byte[] hashbytes = hash.digest(password.getBytes("UTF-8"));
			byte[] incominghashbytes = new byte[hash.getDigestLength()];
			input.readFully(incominghashbytes);
			if (!MessageDigest.isEqual(hashbytes, incominghashbytes)) {
				input.close();
				out.close();
				socket.close();
				log.info("Wrong password");
				log.info("Disconnected.");
				return;
			}
			out.println(smotaversion);
			Set<SensorNetwork> keyset = workerMap.keySet();
			out.println(keyset.size());
			Iterator<SensorNetwork> iter = keyset.iterator();
			while (iter.hasNext()) {
				SensorNetwork network = iter.next();
				out.println(network.getNetworkId());
			}
			int networkid = input.readInt();
			log.info("Code for network: " + networkid);
			int imagetype = input.readInt();
			log.info("Image type: " + imagetype);
			boolean gatewayupdate = input.readBoolean();
			byte[] gatewaycode = null;
			long gatewaycodeversion = 0;
			if (gatewayupdate) {
				gatewaycode = new byte[Firmware.MSPIMAGESIZE];
				gatewaycodeversion = input.readLong();
				input.readFully(gatewaycode);
				log.info("Received code for Gateway");
			}
			int size = input.readInt();
			byte[] platforms = null;
			long[] versions = null;
			byte[][] codes = null;
			if (size > 0) {
				platforms = new byte[size];
				versions = new long[size];
				codes = new byte[size][48 * 1024];
				for (int i = 0; i < size; i++) {
					platforms[i] = (byte) input.read();
					versions[i] = input.readLong();
					input.readFully(codes[i]);
					log.info("Received code for platform: "
							+ platforms[i]
							+ " version: "
							+ Long.toHexString(versions[i] & 0xFFFFFFFF)
									.toUpperCase());
				}
			}
			Softbase softbase = null;
			keyset = workerMap.keySet();
			iter = keyset.iterator();
			boolean found = false;
			while (iter.hasNext()) {
				SensorNetwork network = iter.next();
				if (network.getNetworkId() == networkid) {
					softbase = (Softbase) workerMap.get(network).getSoftbase();
					found = true;
					break;
				}
			}
			if (found) {
				Firmware firmware = new Firmware((byte) imagetype, platforms,
						versions, codes, gatewaycodeversion, gatewaycode);
				softbase.UpdateFirmware(firmware, this);
			} else {
				out.println("Softbase not found");
			}
			input.close();
			out.close();
			socket.close();
			log.info("Disconnected.");
		} catch (IOException e) {
			log.error("Error in UpdateServerThread", e);
		} catch (NoSuchAlgorithmException e) {
			log.error(e.getLocalizedMessage(), e);
		}
	}
}
