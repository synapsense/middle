package com.synapsense.plugin.wsn.util.sensors;

import com.synapsense.plugin.wsn.util.SensorStates;

public class AirPressure implements SensorConversionInterface {

	public double convertFromRaw(char raw) {

		// voltage is measured in 12bits ADCCounts
		// 0 -> 0V
		// 4095 -> 2.5V
		double volts = (double) raw / 4095 * 2.5;

		if (volts < 0.1) {
			return SensorStates.SENSOR_DISCONNECT;
		}

		// Inches of water
		double value = (0.24 * volts) - 0.033;

		return (value > 0) ? value : 0;
	}

	public double convertFromRawCompensated(char raw, double other) {
		return convertFromRaw(raw);
	}

	public int getMoteDataType() {
		return 24;
	}
}
