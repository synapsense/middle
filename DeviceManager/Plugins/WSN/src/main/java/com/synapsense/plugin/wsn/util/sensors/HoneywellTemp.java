package com.synapsense.plugin.wsn.util.sensors;

public class HoneywellTemp implements SensorConversionInterface {
	@Override
	public double convertFromRaw(char raw) {
		double tempc = (((double) raw / (Math.pow(2, 14) - 1)) * 165) - 40;
		return (9.0 / 5.0) * tempc + 32;
	}

	@Override
	public double convertFromRawCompensated(char raw, double other) {
		return convertFromRaw(raw);
	}

	@Override
	public int getMoteDataType() {
		return 41;
	}
}
