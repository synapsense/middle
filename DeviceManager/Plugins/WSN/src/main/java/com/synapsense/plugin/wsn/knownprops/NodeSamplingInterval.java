package com.synapsense.plugin.wsn.knownprops;

import org.apache.log4j.Logger;

import com.synapsense.plugin.wsn.NetworkWorker;
import com.synapsense.plugin.wsn.WsnConstants;
import com.synapsense.plugin.wsn.configuration.Configuration;
import com.synapsense.plugin.wsn.configuration.WSNNode;
import com.synapsense.plugin.wsn.iocore.frames.out.MoteUARTOutSetParameterFrame;
import com.synapsense.plugin.wsn.util.Constants;
import com.synapsense.util.TimeSample;
import com.synapsense.util.TimeSampleFormatException;

class NodeSamplingInterval extends NodePropertyWorker {
	private final static Logger logger = Logger.getLogger(NodeSamplingInterval.class);

	@Override
	protected boolean performAction(String objectID, String propertyName, Object value) {
		WSNNode node = Configuration.getNodeByObjectID(objectID);
		if (node == null)
			return false;
		NetworkWorker netWorker = locateNetworkWorker(node.getNetworkID());
		if (netWorker == null) {
			logger.warn("Unknown networkID:" + node.getNetworkID() + " for node physID:"
			        + Long.toHexString(node.getPhysID()));
			return false;
		}
		logger.info("Setting sampling interval: " + value + " for node with physID: "
		        + Long.toHexString(node.getPhysID()));

		if (!(value instanceof String))
			throw new IllegalArgumentException(value + " is not a string");

		MoteUARTOutSetParameterFrame frame = new MoteUARTOutSetParameterFrame((char) node.getLogicalID(),
		        Constants.PARAM_ID_SAMPLE_FREQUENCY, computeSamplingInSeconds((String) value));
		netWorker.commandQ.add(frame);
		return true;
	}

	/**
	 * An external component sets the sampling interval in form of
	 * "[0-9]+ (sec|min|hr)" The function is responsible to parse source string
	 * and to transform it into seconds
	 * 
	 * @param dataString
	 *            - formatted data
	 * @return sampling in seconds
	 */
	private static int computeSamplingInSeconds(String dataString) {
		try {
			return TimeSample.get(dataString).toSeconds();
		} catch (TimeSampleFormatException e) {
			throw new IllegalArgumentException(dataString + " - invalid Sampling Interval format.", e);
		}
	}

	@Override
	protected boolean checkItsMine(String propertyName) {
		return propertyName.equals(WsnConstants.NETWORK_SAMPLING);
	}
}
