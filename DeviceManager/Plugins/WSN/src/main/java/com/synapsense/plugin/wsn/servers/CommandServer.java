package com.synapsense.plugin.wsn.servers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import org.apache.log4j.Logger;

import com.synapsense.plugin.wsn.NetworkWorker;
import com.synapsense.plugin.wsn.WsnConstants;
import com.synapsense.plugin.wsn.knownprops.KnownPropsStore;
import com.synapsense.plugin.wsn.softbase29.Softbase;
import com.synapsense.plugin.wsn.util.Constants;
import com.synapsense.plugin.wsn.util.SensorNetwork;

public class CommandServer implements Runnable {

	HashMap<SensorNetwork, NetworkWorker> workerMap;
	private static Logger log = Logger.getLogger(CommandServer.class);

	public CommandServer(HashMap<SensorNetwork, NetworkWorker> workerMap) {
		this.workerMap = workerMap;
	}

	public void run() {
		ServerSocket serversocket = null;
		try {
			serversocket = new ServerSocket(Constants.COMMANDSERVER_PORT, 1, InetAddress.getByName("localhost"));
			log.info("Command Server Starting, bound to " + InetAddress.getByName("localhost") + " on port "
			        + Constants.COMMANDSERVER_PORT);
		} catch (IOException e) {
			log.error("Can't start the WSNPlugin command server.", e);
			return;
		}

		Socket clientsocket = null;

		while (!Thread.currentThread().isInterrupted()) {
			try {
				clientsocket = serversocket.accept();
				log.info("Connected from " + clientsocket.getInetAddress());

				BufferedReader in = new BufferedReader(new InputStreamReader(clientsocket.getInputStream()));

				String line = in.readLine();
				while (line != null) {
					String objectID = line;
					String propertyName = in.readLine();
					String value = in.readLine();
					if (objectID.equals("NET")) {
						int networkid = Integer.parseInt(value);
						Set<SensorNetwork> keyset = workerMap.keySet();
						Iterator<SensorNetwork> iter = keyset.iterator();
						Softbase softbase = null;
						keyset = workerMap.keySet();
						iter = keyset.iterator();
						boolean found = false;
						while (iter.hasNext()) {
							SensorNetwork network = iter.next();
							if (network.getNetworkId() == networkid) {
								softbase = (Softbase) workerMap.get(network).getSoftbase();
								found = true;
								break;
							}
						}
						if (found) {
							if (propertyName.equals("reset")) {
								log.info("Network " + networkid + " reset");
								softbase.sendCommand(2, (char) 0);
							}
						}
					} else {
						log.info("Received " + objectID + " " + propertyName + " " + value);
						Serializable svalue = value;
						if (propertyName.equals(WsnConstants.ACTUATOR_NUMBER)
						        || propertyName.equals(WsnConstants.ACTUATOR_NEXTVALUE)
						        || propertyName.equals(WsnConstants.ACTUATOR_DIRECTION)
						        || propertyName.equals(WsnConstants.ACTUATOR_FAILSAFESETPOINT)
						        || propertyName.equals(WsnConstants.ACTUATOR_MINPOINT)
						        || propertyName.equals(WsnConstants.ACTUATOR_MAXPOINT)
						        || propertyName.equals(WsnConstants.ACTUATOR_TRAVELTIME))
							svalue = new Integer(Integer.parseInt(value));

						KnownPropsStore.setPropertyValue(objectID, propertyName, svalue);
					}
					line = in.readLine();
				}

				in.close();
				clientsocket.close();
				log.info("Disconnected.");
			} catch (IOException e) {
				log.error("Got error while processing command", e);
			}
		}

		try {
			serversocket.close();
		} catch (IOException e) {
			// ignore it
		}
	}
}
