package com.synapsense.plugin.wsn.util.sensors;

import com.synapsense.plugin.wsn.util.SensorStates;

public class Generic420 implements SensorConversionInterface, RangeConversionInterface {

	private double min = 0;
	private double max = 100;

	public double convertFromRaw(char raw) {
		// voltage is measured in 12bits ADCCounts
		// 0 -> 0V ; 4095 -> 2.048V
		double voltage = ((double) raw / 4095) * 2.048;

		if (voltage < 0.1)
			return SensorStates.SENSOR_DISCONNECT; // to indicate that sensor is
			                                       // disconnected

		voltage = voltage - 0.4; // 0 set
		double count = voltage * ((max - min) / (1.60 - 0.0));
		count = count + min; // +b term
		return (count > 0) ? count : 0;
	}

	public double convertFromRawCompensated(char raw, double other) {
		return convertFromRaw(raw);

	}

	public int getMoteDataType() {
		return 34;
	}

	public void setDataRanges(double m, double x) {
		min = m;
		max = x;
	}

}
