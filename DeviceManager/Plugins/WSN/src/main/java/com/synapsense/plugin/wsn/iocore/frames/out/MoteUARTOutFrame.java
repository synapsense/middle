package com.synapsense.plugin.wsn.iocore.frames.out;

import com.synapsense.plugin.wsn.iocore.frames.MoteUARTFrame;

public class MoteUARTOutFrame extends com.synapsense.plugin.wsn.msgbus.Message {

	public static char ALL_NODES = 0xFFFF;
	public static long ALL_NODES_LONG = 0xFFFFFFFFFFFFFFFFL;
	// Outgoing commands

	public static final int TYPE_COMMAND = 1; // commands
	public static final int TYPE_SETPARAMETER = 2;
	public static final int TYPE_NODEID = 3;
	public static final int TYPE_CODEDIST = MoteUARTFrame.TYPE_CODEDIST;
	public static final int TYPE_STARTFRAME = 100;

	// SOFT BASE USE
	public static final int TYPE_RADIO = 101;
	public static final int TYPE_BASECOMMAND = 102;
	public static final int TYPE_PING = 103;
	public static final int TYPE_FIRMWARE = 104;
	// //////////

	public static final int TYPE_SNIFFER_RESET = 150;
	public static final int FRAME_MAX_RETRY = 5;

	protected char type;
	protected char len;
	private long delayFor = 0;

	private int delayedCount = 0;
	private long delayTimeLeft = 1000 * 60 * 60;

	protected byte[] frame;

	private boolean ackset;
	private boolean ack;
	private boolean framesent;

	public boolean isFramesent() {
		return framesent;
	}

	public void setFramesent(boolean framesent) {
		this.framesent = framesent;
	}

	/**
	 * @return the delayTimeLeft
	 */
	public long getDelayTimeLeft() {
		return delayTimeLeft;
	}

	/**
	 * @param delayTimeLeft
	 *            the delayTimeLeft to set
	 */
	public void setDelayTimeLeft(long delayTimeLeft) {
		this.delayTimeLeft = delayTimeLeft;
	}

	/**
	 * @return the delayedCount
	 */
	public int getDelayedCount() {
		return delayedCount;
	}

	/**
	 * @param delayedCount
	 *            the delayedCount to set
	 */
	public void setDelayedCount(int delayedCount) {
		this.delayedCount = delayedCount;
	}

	public MoteUARTOutFrame(char type, char len) {
		frame = new byte[len + 5]; // 4 bytes for header 1 byte for CRC
		this.type = type;
		this.len = len;
		frame[0] = (byte) 0xBE;
		frame[3] = (byte) 0xEF;
		frame[1] = (byte) type;
		frame[2] = (byte) len;
		this.ack = false;
		this.ackset = false;
		this.framesent = false;
	}

	public long getDelayValue() {
		return delayFor;
	}

	public void setDelayValue(long time) {
		if (time == 0)
			delayFor = 0;
		else
			delayFor = time + System.currentTimeMillis();
	}

	public byte[] getFullFrame() {
		return frame;
	}

	public void setAck(boolean ack) {
		ackset = true;
		this.ack = ack;
	}

	public boolean isAckset() {
		return ackset;
	}

	public void setAckset(boolean ackset) {
		this.ackset = ackset;
	}

	public boolean isAck() {
		return ack;
	}

	public char getType() {
		return type;
	}
}
