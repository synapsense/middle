package com.synapsense.plugin.wsn.softbase29.state;

import java.util.HashMap;
import java.util.Iterator;

import com.synapsense.plugin.wsn.softbase29.packet.OSSBeaconPacket;

public class OSSNHT {

	public static final int INIT_BEACON_RATE = 15;
	public static final int FASTEST_BEACON_RATE = 17;
	public static final int MAX_LIFE_COUNTER = 5;

	private static final int MIN_GOODLINK_THRESHOLD = -80;
	private static final int MAX_GOODLINK_THRESHOLD = -85;

	public static final int NUMCH = 16;
	public static final int MAXLEVEL = 7;

	public static final char BROADCASTSLOT = 0x8000;

	public static final int MAXHOPTM = 63;

	private HashMap<Character, NeighborInfo> neighbors;

	public OSSNHT() {
		neighbors = new HashMap<Character, NeighborInfo>();
	}

	public void clear() {
		neighbors.clear();
	}

	private int avg4(int n[]) {
		int total = n[0] + n[1] + n[2] + n[3];
		return total / 4;
	}

	private boolean isGoodLink(int rssi[]) {
		return (avg4(rssi) >= MIN_GOODLINK_THRESHOLD);
	}

	private boolean isBadLink(int rssi[]) {
		return (avg4(rssi) <= MAX_GOODLINK_THRESHOLD);
	}

	public void process(OSSBeaconPacket pk) {
		NeighborInfo ninfo = null;
		if (neighbors.containsKey(pk.mac_srcaddr)) {
			ninfo = neighbors.get(pk.mac_srcaddr);
			ninfo.rssi[ninfo.getRssiindex()] = pk.phy_rssi;
			ninfo.setRssiindex(ninfo.getRssiindex() + 1);
			if (ninfo.getRssiindex() > 3)
				ninfo.setRssiindex(0);
		}

		if (ninfo == null && pk.phy_rssi >= MAX_GOODLINK_THRESHOLD) {
			ninfo = new NeighborInfo();
			neighbors.put(pk.mac_srcaddr, ninfo);
			ninfo.setId(pk.mac_srcaddr);
			ninfo.rssi[0] = pk.phy_rssi;
			ninfo.rssi[1] = -128;
			ninfo.rssi[2] = -128;
			ninfo.rssi[3] = -128;
			ninfo.setRssiindex(1);
		}

		if (ninfo != null) {
			ninfo.setHopCount(pk.hopPbase);
			ninfo.setRouterState(OSSBeaconPacket.bpk_getRouterState(pk));
			ninfo.setParentPowerState(OSSBeaconPacket.bpk_getParentState(pk));

			// ritu changes .. preferred gateway info
			ninfo.setPreferredBase(pk.preferredBase);
			ninfo.setSecondaryBase(pk.secondaryBase);
			ninfo.setHopCountSecondaryBase(pk.hopSbase);

			ninfo.setHopCountTM(OSSBeaconPacket.getHopTM(pk));
			ninfo.setSlots(pk.slots);
			ninfo.setRadioCh(OSSBeaconPacket.getCh(pk));
			ninfo.setRate(OSSBeaconPacket.getBrate(pk));
			ninfo.setRcvdTime(System.currentTimeMillis());
			// ritu changes .. ch adaptation
			ninfo.chDuration[0] = pk.chDuration[0];
			ninfo.chDuration[1] = pk.chDuration[1];
			ninfo.chDuration[2] = pk.chDuration[2];
			ninfo.chDuration[3] = pk.chDuration[3];
			findFillIndex(ninfo);
			if (isGoodLink(ninfo.rssi))
				ninfo.setState(NeighborInfo.Neighbor_Normal);
			if (isBadLink(ninfo.rssi) || ninfo.getState() == NeighborInfo.Neighbor_Unreachable)
				ninfo.setState(NeighborInfo.Neighbor_Known);
		}
	}

	public char getSlotInfo(char id) {
		NeighborInfo neighbor = neighbors.get(id);
		if (neighbor != null && neighbor.getSlots() != 0) {
			return neighbor.getSlots();
		}
		return BROADCASTSLOT;
	}

	public byte getRadioChannelInfo(char id) {
		NeighborInfo neighbor = neighbors.get(id);
		if (neighbor != null)
			return neighbor.getRadioCh();
		return 0;
	}

	public int getNeighborSize() {
		return neighbors.size();
	}

	public boolean isNeighbor(char id) {
		Iterator<NeighborInfo> iter = neighbors.values().iterator();
		while (iter.hasNext()) {
			NeighborInfo ninfo = iter.next();
			if (ninfo.getId() == id) {
				return (ninfo.getState() == NeighborInfo.Neighbor_Normal);
			}
		}
		return false;
	}

	public void beaconSend() {
		long curtime = System.currentTimeMillis();
		Iterator<NeighborInfo> iter = neighbors.values().iterator();
		while (iter.hasNext()) {
			NeighborInfo ninfo = iter.next();
			if ((curtime - ninfo.getRcvdTime()) > (1L << ninfo.getRate()) * MAX_LIFE_COUNTER) {
				if (ninfo.getState() != NeighborInfo.Neighbor_Dead) {
					ninfo.setState(NeighborInfo.Neighbor_Dead);
				}
			}
			if ((curtime - ninfo.getRcvdTime()) > (1L << ninfo.getRate()) * MAX_LIFE_COUNTER * 2) {
				iter.remove();
			}
		}
	}

	public int getHopCountTM() {
		int min = MAXHOPTM;
		Iterator<NeighborInfo> iter = neighbors.values().iterator();
		while (iter.hasNext()) {
			NeighborInfo ninfo = iter.next();
			if (ninfo.getHopCountTM() < min)
				min = ninfo.getHopCountTM();
		}
		if (min != MAXHOPTM)
			return min + 1;
		return 0xFF;
	}

	public char[] getNeighborChannelDuration(char id) {
		NeighborInfo neighbor = neighbors.get(id);
		if (neighbor != null) {
			return neighbor.chDuration;
		}
		char[] chDuration = new char[4];
		chDuration[0] = 0;
		chDuration[1] = 0;
		chDuration[2] = 0;
		chDuration[3] = 0;
		return chDuration;
	}

	public byte getCurMaxLevel(char id) {
		NeighborInfo neighbor = neighbors.get(id);
		if (neighbor != null)
			return neighbor.curmaxlevel;
		return MAXLEVEL;
	}

	public char getFillIndex(char id) {
		NeighborInfo neighbor = neighbors.get(id);
		if (neighbor != null)
			return neighbor.fillindex;
		return 0;
	}

	private int getLevel(char[] chDuration, int index) {
		int onechduration = chDuration[index / 4];
		int subindex = index - index / 4;
		return (onechduration >> (subindex * 4)) & 0xF;
	}

	public void findFillIndex(NeighborInfo ninfo) {
		int total = 0;
		int newmaxlevel = 0;
		for (int i = 0; i < NUMCH; i++) {
			int level = getLevel(ninfo.chDuration, i);
			total = total + ((1 << MAXLEVEL) - (1 << level));
			if (level > newmaxlevel)
				newmaxlevel = level;
		}
		int low = 0;
		int high = NUMCH * (1 << MAXLEVEL);
		int middle = 0;
		while (low <= high) {
			middle = (low + high) / 2;
			int middletotal = 0;
			for (int i = 0; i < NUMCH; i++) {
				int level = getLevel(ninfo.chDuration, i);
				middletotal = middletotal + getfillcount(middle, newmaxlevel, level);
			}
			if (middletotal == total) {
				low = middle;
				break;
			}
			if (middletotal < total)
				low = middle + 1;
			else
				high = middle - 1;
		}
		ninfo.curmaxlevel = (byte) newmaxlevel;
		ninfo.fillindex = (char) low;
	}

	public int getfillcount(int index, int max, int level) {
		return index / (1 << (max - level));
	}
}
