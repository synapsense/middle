package com.synapsense.plugin.wsn.iocore.n29.frames;

import com.synapsense.plugin.wsn.iocore.frames.MoteUARTBadFrame;
import com.synapsense.plugin.wsn.iocore.frames.MoteUARTFrame;

public class MoteUARTCanIStartFrame extends MoteUARTFrame {

	public MoteUARTCanIStartFrame(byte[] header, byte[] data, String portName) throws MoteUARTBadFrame {
		super(header, portName);

	}

}
