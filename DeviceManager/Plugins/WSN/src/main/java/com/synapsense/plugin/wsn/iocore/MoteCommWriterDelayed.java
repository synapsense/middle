package com.synapsense.plugin.wsn.iocore;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.BlockingQueue;

import com.synapsense.plugin.wsn.iocore.frames.out.MoteUARTOutFrame;

public class MoteCommWriterDelayed implements Runnable {

	private final LinkedList<MoteUARTOutFrame> delayedlist = new LinkedList<MoteUARTOutFrame>();

	private BlockingQueue<MoteUARTOutFrame> commandQ;

	public MoteCommWriterDelayed(BlockingQueue<MoteUARTOutFrame> commandQ) {

		this.commandQ = commandQ;
	}

	public void delay(MoteUARTOutFrame toDelay) {
		synchronized (delayedlist) {
			delayedlist.add(toDelay);
		}
	}

	public void run() {
		try {
			List<MoteUARTOutFrame> toRemoveList = new LinkedList<MoteUARTOutFrame>();
			while (true) {
				Thread.sleep(1000);

				synchronized (delayedlist) {
					for (MoteUARTOutFrame i : delayedlist) {
						if (i.getDelayValue() <= System.currentTimeMillis()) {
							i.setDelayValue(0);
							toRemoveList.add(i);
							commandQ.put(i);

						}

					}
					// Remove items now - otherwise delayed list iterator would
					// be invalidated above
					for (MoteUARTOutFrame i : toRemoveList) {

						delayedlist.removeFirstOccurrence(i);
					}
					toRemoveList.clear();
				}

			}

		} catch (InterruptedException e) {

			return;

		}

	}

}
