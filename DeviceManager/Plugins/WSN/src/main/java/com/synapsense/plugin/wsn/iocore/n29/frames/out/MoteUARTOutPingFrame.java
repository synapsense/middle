package com.synapsense.plugin.wsn.iocore.n29.frames.out;

import com.synapsense.plugin.wsn.iocore.frames.out.MoteUARTOutFrame;

public class MoteUARTOutPingFrame extends MoteUARTOutFrame {

	public MoteUARTOutPingFrame() {
		super((char) MoteUARTOutFrame.TYPE_PING, (char) 0);
	}
}
