package com.synapsense.plugin.wsn.iocore.frames.out;

import com.synapsense.plugin.wsn.util.serializer.BigSerializer;

/**
 * 
 * @author Yann
 * 
 */

public class MoteUARTOutSetParameterFrame extends MoteUARTOutFrame {

	// public static HashMap<String,Integer> parameters = new
	// HashMap<String,Integer>();
	public char destination;
	public int parameter;
	public long data;
	public byte[] ext;

	public MoteUARTOutSetParameterFrame(char destination, int parameter, long data) {
		super((char) MoteUARTOutFrame.TYPE_SETPARAMETER, (char) 8);
		BigSerializer ser = new BigSerializer(frame, 4);
		ser.packU_INT16(destination);
		ser.packU_INT16((char) parameter);
		ser.packU_INT32(data);
		this.destination = destination;
		this.parameter = parameter;
		this.data = data;
		this.ext = null;
	}

	public MoteUARTOutSetParameterFrame(char destination, int parameter, long data, byte[] ext) {
		super((char) MoteUARTOutFrame.TYPE_SETPARAMETER, (char) (8 + ext.length));
		BigSerializer ser = new BigSerializer(frame, 4);
		ser.packU_INT16(destination);
		ser.packU_INT16((char) parameter);
		ser.packU_INT32(data);
		for (int i = 0; i < ext.length; i++)
			ser.packU_INT8(ext[i]);
		this.destination = destination;
		this.parameter = parameter;
		this.data = data;
		this.ext = ext;
	}

	public MoteUARTOutSetParameterFrame(long destination, int parameter, long data) {
		super((char) MoteUARTOutFrame.TYPE_SETPARAMETER, (char) 14);
		BigSerializer ser = new BigSerializer(frame, 4);
		ser.pack_INT64(destination);
		ser.packU_INT16((char) parameter);
		ser.packU_INT32(data);

	}

	public byte[] getExtData() {
		return this.ext;
	}

}
