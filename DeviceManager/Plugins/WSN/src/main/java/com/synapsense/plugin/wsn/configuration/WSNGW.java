package com.synapsense.plugin.wsn.configuration;

import com.synapsense.plugin.apinew.ObjectDesc;
import com.synapsense.plugin.wsn.WsnConstants;

public class WSNGW extends WSNNode {

	private String port;
	private String protocol;
	private String address;
	private String key;

	/**
	 * Gateway is represented as a node to treat the Routing Information. This
	 * ctor is used to create a Node from a Gateway;
	 * 
	 * @param networkID
	 * @param gw
	 */
	public WSNGW(Integer networkID, ObjectDesc gw) {
		// TODO logical ids are to be assigned by mapsense during configuration.
		// The check won't be needed after it's implemented
		super(gw.getID(), networkID, new Long(gw.getPropertyValue(WsnConstants.NODE_PHYS_ID)), new Integer(
		        gw.getPropertyValue(WsnConstants.NODE_LOGICAL_ID)), WSNNode.PLATFORM_GATEWAY, 0, "");

		this.protocol = gw.getPropertyValue(WsnConstants.GATEWAY_PROTOCOL);
		this.address = gw.getPropertyValue(WsnConstants.GATEWAY_ADDRESS);
		this.key = gw.getPropertyValue(WsnConstants.GATEWAY_KEY);

		this.port = this.protocol + ":" + this.address;

	}

	public WSNGW(String id, Integer networkID, long physID, int logicalID, String port) {
		super(id, networkID, physID, logicalID, WSNNode.PLATFORM_GATEWAY, 0, "");
		this.protocol = port.substring(0, port.indexOf(":"));
		this.address = port.substring(port.indexOf(":") + 1);
		this.port = port;
	}

	/**
	 * Return a logical port name for this gateway. Unlike previous APIs, this
	 * API is not intened to allow connections to be made on the underlying
	 * information.
	 * 
	 * @return port name string
	 */
	public String getPortName() {
		return port;
	}

	/**
	 * Returns the registered protocol for this gateway. Options include:
	 * TCP_RG2 TCP_RG25 TCP_RG25_REVERSE
	 * 
	 * @return
	 */
	public String getProtocol() {
		return protocol;
	}

	/**
	 * Returns the address of the gatway. Address depends on protocol chosen.
	 * Options include: 192.168.0.1 (IPv4 dotted quad)
	 * thisisacoolgateway.coolco.co (DNS) fd00:9323::1 (IPv6 address)
	 * 
	 * @return
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * Returns the security key to be used for this connection.
	 * 
	 * @return
	 */
	public String getKey() {
		return key;
	}

	public boolean equals(Object t) {
		WSNGW g = (WSNGW) t;
		return (g.getPortName().equals(this.port));
	}

}
