package com.synapsense.plugin.wsn.iocore.vk.frames;

import org.apache.log4j.Logger;

import com.synapsense.plugin.wsn.iocore.frames.MoteUARTBadFrame;
import com.synapsense.plugin.wsn.iocore.frames.MoteUARTFrame;

public class MoteUARTVKRadioFrame extends MoteUARTFrame {

	private static final Logger log = Logger.getLogger(MoteUARTVKRadioFrame.class);

	private String s;

	private static final byte[] globalbcastaddr = { (byte) 0xFF, 0x00, (byte) 0xFF, 0x00, (byte) 0xFF };
	private static final byte localbcastaddr = (byte) 0x7E;

	private static final String[] pkttypestr = { "Beacon", "Data", "Config", "Stats", "Command", "Commandresponse" };
	private static final String[] datastr = { "Volts", "Current", "SMOTA", "SMOTAResponse" };
	private static final String[] configstr = { "Request", "Response" };
	private static final String[] commandstr = { "Promotechild", "Phaserotation", "SwitchSMOTA", "Rabbitlight",
	        "Identify", "Setblinknumber", "Reset" };
	private static final String[] commandresponsestr = { "Phaserotation" };
	private static final String[] modestr = { "App", "SMOTA" };

	public MoteUARTVKRadioFrame(byte[] header, byte[] data, String portName) throws MoteUARTBadFrame {
		super(header, portName);
		StringBuffer buf = new StringBuffer();
		this.data = data;
		try {
			buf.append(parse());
		} catch (Exception e) {
			buf.append("ERROR PARSING");
			log.error("Cannot parse data", e);
		}
		buf.append("\n");
		for (int i = 0; i < data.length; i++) {
			buf.append(String.format("%02X", data[i]) + " ");
		}
		s = buf.toString();
	}

	public String parse() {
		StringBuffer buf;
		buf = new StringBuffer();
		buf.append("Dest: ");
		boolean gbcast = true;
		for (int i = 0; i < 5; i++) {
			if (data[i] != globalbcastaddr[i]) {
				gbcast = false;
				break;
			}
		}
		if (gbcast) {
			buf.append("Global Bcast");
		} else if (data[0] == localbcastaddr) {
			buf.append("Net Bcast");
		} else {
			buf.append(String.format("%02X", data[0]));
		}

		buf.append(" Src: ");
		buf.append(String.format("%02X", data[5]));

		int type = data[6] & 0xF;
		int subtype = (data[6] >> 4) & 0xF;
		buf.append(" Type: " + pkttypestr[type - 1]);
		switch (type) {
		case 2:
			buf.append(" Sub: " + datastr[subtype - 1]);
			break;
		case 3:
			buf.append(" Sub: " + configstr[subtype - 1]);
			break;
		case 5:
			buf.append(" Sub: " + commandstr[subtype - 1]);
			break;
		case 6:
			buf.append(" Sub: " + commandresponsestr[subtype - 1]);
			break;
		}

		int version = data[7] & 0x7F;
		int mode = (data[7] >> 7) & 0x1;
		buf.append(" Ver: " + version);
		buf.append(" Mode: " + modestr[mode]);

		if (type == 1) {
			buf.append(" ChildCount: " + data[8]);
			buf.append(" Chindex: " + data[9]);
			long txtime = (data[13] & 0xFF);
			txtime = (txtime << 8) + (data[12] & 0xFF);
			txtime = (txtime << 8) + (data[11] & 0xFF);
			txtime = (txtime << 8) + (data[10] & 0xFF);
			buf.append(" Txtime: " + txtime);
		} else {
			buf.append(" Data: ");
			for (int i = 8; i < data.length; i++)
				buf.append(String.format("%02X", data[i]) + " ");
		}
		return buf.toString();
	}

	public String toString() {
		return s;
	}
}
