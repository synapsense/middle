package com.synapsense.plugin.wsn.configuration;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import com.synapsense.plugin.api.exceptions.PropertyNotFoundException;
import com.synapsense.plugin.apinew.ObjectDesc;
import com.synapsense.plugin.apinew.PluginConfig;
import com.synapsense.plugin.wsn.SynapStore;
import com.synapsense.plugin.wsn.WsnConstants;
import com.synapsense.plugin.wsn.knownprops.KnownPropsStore;
import com.synapsense.plugin.wsn.msgbus.MsgJawaConfig;
import com.synapsense.plugin.wsn.p3.JawaConfiguration;
import com.synapsense.plugin.wsn.softbase.GateWay;
import com.synapsense.plugin.wsn.softbase.Softbase;
import com.synapsense.plugin.wsn.storers.StorePowerRackConf;
import com.synapsense.plugin.wsn.util.Base64;
import com.synapsense.plugin.wsn.util.SensorNetwork;
import com.synapsense.util.TimeSample;
import com.synapsense.util.algo.Finder;
import com.synapsense.util.algo.Predicate;
import com.synapsense.util.algo.SetsIntersector;
import com.synapsense.util.algo.SetsIntersector.AllValues;
import com.synapsense.util.algo.SetsIntersector.Difference;

/**
 * Processes and stores WSN network configuration. Initial configuration method
 * <code>configure()</code> should be invoked prior to any other methods.
 * 
 * @author anechaev
 * 
 */
public class Configuration {
	private final static Logger log = Logger.getLogger(Configuration.class);

	private static Map<Integer, SensorNetwork> networks = new HashMap<Integer, SensorNetwork>();
	private static Map<NodeID, WSNNode> nodes = new ConcurrentHashMap<NodeID, WSNNode>();
	private final static Finder<WSNNode> nodeFinder = new Finder<WSNNode>(nodes.values());
	private static Map<String, WSNActuator> actuators = new ConcurrentHashMap<String, WSNActuator>();
	private final static Map<String, WSNSensor> sensors = new ConcurrentHashMap<String, WSNSensor>();

	/**
	 * This comparator class mentions these Node properties: physical id,
	 * network id
	 * 
	 * @author anechaev
	 * 
	 */
	protected static class NodePN implements Comparator<WSNNode> {
		private static int compareInternal(WSNNode o1, WSNNode o2) {
			if (o1.getPhysID() != o2.getPhysID())
				return ((Long) o1.getPhysID()).compareTo(o2.getPhysID());
			if (o1.getNetworkID() != o2.getNetworkID())
				return ((Integer) o1.getNetworkID()).compareTo(o2.getNetworkID());
			int tmp = o1.getId().compareTo(o2.getId());
			if (tmp != 0)
				return tmp;

			AllValues<WSNSensor> sensors = new SetsIntersector<WSNSensor>().doAllJob(o1.getSensors(), o2.getSensors(),
			        new Comparator<WSNSensor>() {
				        @Override
				        public int compare(WSNSensor o1, WSNSensor o2) {
					        int idComp = o1.getId().compareTo(o2.getId());
					        if (idComp != 0)
						        return idComp;

					        if (!o1.getOptions().entrySet().equals(o2.getOptions().entrySet()))
						        return 1;

					        return ((Integer) o1.getChannel()).compareTo(o2.getChannel());
				        }
			        });
			if (sensors.getDifference().getFMinusS().isEmpty() && sensors.getDifference().getSMinusF().isEmpty()) {
				return 0;
			}
			return 1; // here is no difference, what (1 or -1) to return;
		}

		@Override
		public int compare(WSNNode o1, WSNNode o2) {
			return compareInternal(o1, o2);
		}
	}

	private static class NodePNS implements Comparator<WSNNode> {
		@Override
		public int compare(WSNNode o1, WSNNode o2) {
			int result = NodePN.compareInternal(o1, o2);
			if (result != 0)
				return result;

			return ((Integer) o1.getSamplingInSeconds()).compareTo(o2.getSamplingInSeconds());
		}
	}

	private static class WSNActuatorComparator implements Comparator<WSNActuator> {
		private static int compareInternal(WSNActuator o1, WSNActuator o2) {
			if (!o1.getId().equals(o2.getId()))
				return o1.getId().compareTo(o2.getId());
			if (o1.getActuatorNumber() != o2.getActuatorNumber())
				return ((Integer) o1.getActuatorNumber()).compareTo(o2.getActuatorNumber());
			if (o1.getDirection() != o2.getDirection())
				return ((Integer) o1.getDirection()).compareTo(o2.getDirection());
			// Do not compare nextvalue
			// if (o1.getNextValue() != o2.getNextValue())
			// return ((Integer)
			// o1.getNextValue()).compareTo(o2.getNextValue());
			if (o1.getFailsafesetpoint() != o2.getFailsafesetpoint())
				return ((Integer) o1.getFailsafesetpoint()).compareTo(o2.getFailsafesetpoint());
			if (o1.getMinpoint() != o2.getMinpoint())
				return ((Integer) o1.getMinpoint()).compareTo(o2.getMinpoint());
			if (o1.getMaxpoint() != o2.getMaxpoint())
				return ((Integer) o1.getMaxpoint()).compareTo(o2.getMaxpoint());
			return ((Integer) o1.getTravelTime()).compareTo(o2.getTravelTime());
		}

		public int compare(WSNActuator o1, WSNActuator o2) {
			return compareInternal(o1, o2);
		}
	}

	private static class NodePNA implements Comparator<WSNNode> {
		@Override
		public int compare(WSNNode o1, WSNNode o2) {
			int result = NodePN.compareInternal(o1, o2);
			if (result != 0)
				return result;

			AllValues<WSNActuator> actuators = new SetsIntersector<WSNActuator>().doAllJob(o1.getActuators(),
			        o2.getActuators(), new WSNActuatorComparator());
			if (actuators.getDifference().getFMinusS().isEmpty() && actuators.getDifference().getSMinusF().isEmpty()) {
				return 0;
			}
			if (o1.getActuators().size() == 0 && o2.getActuators().size() != 0)
				return -1;
			if (o1.getActuators().size() != 0 && o2.getActuators().size() == 0)
				return 1;
			if (o1.getActuators().size() != o2.getActuators().size()) {
				return ((Integer) o1.getActuators().size()).compareTo(o2.getActuators().size());
			}
			List<WSNActuator> f = new ArrayList<WSNActuator>(o1.getActuators());
			List<WSNActuator> s = new ArrayList<WSNActuator>(o2.getActuators());
			Collections.sort(f, new WSNActuatorComparator());
			Collections.sort(s, new WSNActuatorComparator());
			return WSNActuatorComparator.compareInternal(f.get(0), s.get(0));
		}
	}

	// yes, we really comparing object of different types.
	@SuppressWarnings("rawtypes")
	protected static class WSNNetToSensorNetComparator implements Comparator {
		@Override
		public int compare(Object o1, Object o2) {
			Integer id1;
			Integer id2;
			if (o1 instanceof ObjectDesc) {
				id1 = getId((ObjectDesc) o1);
			} else if (o1 instanceof SensorNetwork) {
				id1 = ((SensorNetwork) o1).getNetworkId();
			} else
				throw new IllegalArgumentException("Expecting Network or SensorNetwork object as 1st argument");

			if (o2 instanceof ObjectDesc) {
				id2 = getId((ObjectDesc) o2);
			} else if (o2 instanceof SensorNetwork) {
				id2 = ((SensorNetwork) o2).getNetworkId();
			} else
				throw new IllegalArgumentException("Expecting Network or SensorNetwork object as 2nd argument");

			return id1.compareTo(id2);
		}
	}

	protected static class SoftGWTOWSNGWComparator implements Comparator<Object> {
		@Override
		public int compare(Object o1, Object o2) {
			String port1, port2;

			if (o1 instanceof GateWay) {
				port1 = ((GateWay) o1).getPortname();
			} else if (o1 instanceof ObjectDesc) {
				ObjectDesc obj = (ObjectDesc) o1;

				port1 = obj.getPropertyValue(WsnConstants.GATEWAY_PROTOCOL) + ":"
				        + obj.getPropertyValue(WsnConstants.GATEWAY_ADDRESS)
				        + obj.getPropertyValue(WsnConstants.GATEWAY_KEY);
			} else
				throw new IllegalArgumentException("Expecting Gateway or GateWay object as 1st argument");

			if (o2 instanceof GateWay) {
				port2 = ((GateWay) o2).getPortname();
			} else if (o2 instanceof ObjectDesc) {
				ObjectDesc obj = (ObjectDesc) o2;

				port2 = obj.getPropertyValue(WsnConstants.GATEWAY_PROTOCOL) + ":"
				        + obj.getPropertyValue(WsnConstants.GATEWAY_ADDRESS)
				        + obj.getPropertyValue(WsnConstants.GATEWAY_KEY);
			} else
				throw new IllegalArgumentException("Expecting Gateway or GateWay object as 1st argument");

			return port1.compareTo(port2);
		}
	}

	public static void configure(final PluginConfig config) {
		Collection<ObjectDesc> networks = config.getRoots();

		createNetworks(networks);
		createNodes(networks);
	}

	public static void deleteAllNets() {
		for (SensorNetwork oldNet : SynapStore.workerMap.keySet()) {
			log.info("Removing the network (net id: " + ((SensorNetwork) oldNet).getNetworkId() + ")");
			removeNetwork(((SensorNetwork) oldNet).getNetworkId());
		}
	}

	// be careful to change SetsIntersector parameters order. Types of result
	// components depend of it!
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private static void createNetworks(final Collection<ObjectDesc> networksFromConf) {
		if (networksFromConf == null)
			throw new IllegalStateException("wsn configuration has not been set");
		SetsIntersector intersector = new SetsIntersector();
		Configuration.WSNNetToSensorNetComparator comparator = new Configuration.WSNNetToSensorNetComparator();
		AllValues nets = intersector.doAllJob(networksFromConf, SynapStore.workerMap.keySet(), comparator);

		// remove nonexistent networks (
		for (Object oldNet : nets.getDifference().getSMinusF()) {
			log.info("Removing the network (net id: " + ((SensorNetwork) oldNet).getNetworkId() + ")");
			removeNetwork(((SensorNetwork) oldNet).getNetworkId());
		}
		// remove networks with changed PanID
		List<ObjectDesc> netsWithChangedPanID = new ArrayList<ObjectDesc>();
		List<ObjectDesc> netsToMergeGWs = new ArrayList<ObjectDesc>();
		for (Object updatedNet : nets.getIntersection()) {
			ObjectDesc updatedNetObj = (ObjectDesc) updatedNet;
			int id = getId(updatedNetObj);
			SensorNetwork origNet = Configuration.getSensorNetworkById(id);
			int origPanID = origNet.getPanId();
			int newPanID = Integer.parseInt(updatedNetObj.getPropertyValue(WsnConstants.NETWORK_PAN_ID));
			if (origPanID != newPanID) {
				log.info("panId for the network " + id + " has changed from " + origPanID + " to " + newPanID
				        + ". The network will be removed and added once again");
				netsWithChangedPanID.add(updatedNetObj);
				removeNetwork(id);
			} else {
				netsToMergeGWs.add(updatedNetObj);
			}
		}
		// add new networks
		List<ObjectDesc> netsToAdd = new ArrayList<ObjectDesc>(netsWithChangedPanID);
		netsToAdd.addAll(nets.getDifference().getFMinusS());
		for (ObjectDesc net : netsToAdd) {
			Collection<ObjectDesc> gateways = net.getChildrenByType(WsnConstants.TYPE_GATEWAY);
			Integer networkID = getId(net);
			if (gateways.isEmpty()) {
				log.warn("Network " + net.getProperty("name").getValue() + ": no gateways are defined.");
			} else {
				log.info("Creating the network (NetID: " + networkID + ")");

				int suite = Integer.parseInt(net.getPropertyValue(WsnConstants.NETWORK_SECURITYSUITE));

				log.info("Network security suite: " + suite);
				log.info("Public key: " + net.getPropertyValue(WsnConstants.NETWORK_PUBLICKEY));
				log.trace("Private key: " + net.getPropertyValue(WsnConstants.NETWORK_PRIVATEKEY));

				byte[] publicKey = null;
				byte[] privkey = null;
				try {
					publicKey = Base64.decode(net.getPropertyValue(WsnConstants.NETWORK_PUBLICKEY));
					privkey = Base64.decode(net.getPropertyValue(WsnConstants.NETWORK_PRIVATEKEY));
				} catch (IOException e) {
					log.error("Couldn't decode key information due to exception", e);
				}

				SensorNetwork newSNet = createNetwork(net, gateways, publicKey, privkey);
				networks.put(networkID, newSNet);
				try {
					SynapStore.createOrReturnWorker(newSNet);
				} catch (Exception e) {
					log.error("Couldn't create a new network worker instance dynamically. Something broke.", e);
				}
			}
		}

		// update GWs list for existent network
		for (Object curNet : netsToMergeGWs) {
			mergeNetworkGWs((ObjectDesc) curNet);
		}
	}

	// be careful to change SetsIntersector parameters order. Types of result
	// components depend of it!
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private static void mergeNetworkGWs(ObjectDesc net) {
		int networkID = getId(net);
		Softbase sb = getSoftbase(networkID);
		if (sb == null)
			throw new IllegalArgumentException("Expecting a running Softbase for the network (netID: " + networkID
			        + ")");

		Difference gws = new SetsIntersector().computeDifference(sb.getGateways(),
		        net.getChildrenByType(WsnConstants.TYPE_GATEWAY), new SoftGWTOWSNGWComparator());

		for (Object oldGW : gws.getFMinusS()) {
			log.info("Removing the gateway (netID: " + networkID + ", port: " + ((GateWay) oldGW).getPortname() + ")");
			sb.removeGateway(((GateWay) oldGW).getPortname());
		}

		for (Object newGW : gws.getSMinusF()) {
			ObjectDesc obj = (ObjectDesc) newGW;
			log.info("Adding the gateway (netID: " + networkID + ", port: " +

			obj.getPropertyValue(WsnConstants.GATEWAY_PROTOCOL) + ":"
			        + obj.getPropertyValue(WsnConstants.GATEWAY_ADDRESS)
			        + obj.getPropertyValue(WsnConstants.GATEWAY_KEY) + ")");
			sb.addGateway(new WSNGW(networkID, (ObjectDesc) newGW));
		}
	}

	private static void createNodes(final Collection<ObjectDesc> networksFromConf) {
		final SetsIntersector<WSNNode> proc = new SetsIntersector<WSNNode>();
		final Collection<WSNNode> allNewNodes = new ArrayList<WSNNode>();

		for (ObjectDesc net : networksFromConf) {
			Integer netId = getId(net);
			for (ObjectDesc node : net.getChildrenByType(WsnConstants.TYPE_NODE)) {
				try {
					allNewNodes.add(new WSNNode(netId, node));
				} catch (Exception e) {
					log.error(e.getLocalizedMessage(), e);
				}
			}
			for (ObjectDesc gw : net.getChildrenByType(WsnConstants.TYPE_GATEWAY)) {
				allNewNodes.add(new WSNGW(netId, gw));
			}
		}

		AllValues<WSNNode> res = proc.doAllJob(getNodes(), allNewNodes, new NodePNS());
		Collection<WSNNode> pns = res.getIntersection();
		Collection<WSNNode> pn = proc.intersect(getNodes(), allNewNodes, new NodePN());
		Collection<WSNNode> changedSampling = proc.computeDifference(pn, pns, new NodePNS()).getFMinusS();
		Collection<WSNNode> newSampling = proc.intersect(allNewNodes, changedSampling, new NodePN());
		Collection<WSNNode> pna = proc.intersect(getNodes(), allNewNodes, new NodePNA());
		Collection<WSNNode> changedActuatorSetting = proc.computeDifference(pn, pna, new NodePNA()).getFMinusS();
		Collection<WSNNode> newActuatorSetting = proc.intersect(allNewNodes, changedActuatorSetting, new NodePN());

		// Until NodePN comparator becomes smarter to any change, capture a list
		// of ObjectIDs for nodes which existed previously
		Set<String> previousNodeObj = new HashSet<String>();

		for (WSNNode node : res.getDifference().getFMinusS()) {
			previousNodeObj.add(node.getId());
			unregisterNode(node);
		}

		for (WSNNode node : res.getDifference().getSMinusF()) {
			// If the node was here before, perhaps we need to do a config
			// update on it
			boolean wasHereBefore = previousNodeObj.contains(node.getId());
			registerNode(node, wasHereBefore);
		}

		for (WSNNode node : newSampling) {
			changeSampling(node);
		}

		for (WSNNode node : getNodes()) {
			updateP3config(node, allNewNodes);
		}

		for (WSNNode node : newActuatorSetting) {
			changeActuatorSetting(node);
		}

	}

	private static void changeSampling(WSNNode node) {
		log.info("Changing samping interval for the node 0x" + Long.toHexString(node.getPhysID()));

		try {
			KnownPropsStore.setPropertyValue(node.getId(), WsnConstants.NETWORK_SAMPLING,
			        TimeSample.get(0, node.getSamplingInSeconds()).toString());
		} catch (Exception e) {
			log.warn("Couldn't set the sampling interval for the node 0x" + Long.toHexString(node.getPhysID()), e);
		}
	}

	private static void changeActuatorSetting(WSNNode node) {
		log.info("Updating Actuator setting for the node 0x" + Long.toHexString(node.getPhysID()));
		try {
			Collection<WSNActuator> newactuators = node.getActuators();
			WSNNode oldnode = getNodeByObjectID(node.getId());
			oldnode.clearActuators();
			for (WSNActuator newactuator : newactuators) {
				WSNActuator oldactuator = actuators.get(newactuator.getId());
				if (oldactuator != null) {
					// Copy old nextvalue, sensor, and node to new actuator
					newactuator.setNextValue(oldactuator.getNextValue());
					newactuator.setSensor(oldactuator.getSensor());
					newactuator.setNode(oldactuator.getNode());
					actuators.remove(oldactuator);
				} else {
					newactuator.setSensor(oldnode.getSensor(newactuator.getActuatorNumber()));
					newactuator.setNode(oldnode);
				}
				actuators.put(newactuator.getId(), newactuator);
				oldnode.addActuator(newactuator);
				log.info("Actuator " + newactuator.toString());
				// Setting nextvalue triggers sending the command
				KnownPropsStore.setPropertyValue(newactuator.getId(), WsnConstants.ACTUATOR_NEXTVALUE, new Integer(
				        newactuator.getNextValue()));
			}
		} catch (Exception e) {
			log.warn("Couldn't update the Actuator setting for the node 0x" + Long.toHexString(node.getPhysID()), e);
		}
	}

	private static void changeSmartSendSetting(WSNNode node) {
		try {
			Collection<WSNSensor> sensors = node.getSensors();
			for (WSNSensor s : sensors) {
				if (s.getDeltaTLow() != null) {
					log.info("Updating smartsend2 setting for the node 0x" + Long.toHexString(node.getPhysID()));
					KnownPropsStore.setPropertyValue(s.getId(), WsnConstants.SENSOR_DELTA_T_LOW, s.getDeltaTLow());
				}
			}
		} catch (Exception e) {
			log.warn("Can't set smartsend2", e);
		}
	}

	// @TODO: This should be refactored to "washerebefore" status
	private static void updateP3config(WSNNode node, Collection<WSNNode> allNewNodes) {
		for (WSNNode newnode : allNewNodes) {
			// log.trace("newnode: " + newnode.toString());
			// log.trace("node: " + node.getPlatformID());
			if (newnode.getPhysID() == node.getPhysID()) {
				boolean sendConfigToES = true;
				boolean sendConfigToSC = false;
				// TODO add tests for this: node is new
				// timestamp of new node is newer than pre data
				// checksums don't match
				// version code >0
				// it's a valid config
				// DON'T send if identical

				if (node.getP3Configuration() != null && newnode.getP3Configuration() != null) {
					// Check if the actual checksums match

					if (node.getP3Configuration().getChecksum() == node.getP3Configuration().getChecksum()) {
						if (node.getLeftRack().equals(newnode.getLeftRack())
						        && node.getCenterRack().equals(newnode.getCenterRack())
						        && node.getRightRack().equals(newnode.getRightRack())) {
							// Nothing has changed
							sendConfigToES = false;
						} else {
							// Update power rack links
							node.setLeftRack(newnode.getLeftRack());
							node.setCenterRack(newnode.getCenterRack());
							node.setRightRack(newnode.getRightRack());
						}
					}

					if (node.getP3Configuration().getChecksum() != newnode.getServerProgramChecksum()) {
						sendConfigToES = true;
						newnode.setServerProgramChecksumToP3Checksum();

					}

				} else if (node.getP3Configuration() == null) {
					// Configuration hasn't been sent from the sandcrawler/duo
					// yet
					sendConfigToES = false;
				}
				if (sendConfigToES) {
					// Send P3 configuration to ES again.
					// This is nested and ugly

					log.info("Sending P3Configuration to ES again");
					try {
						MsgJawaConfig message = new MsgJawaConfig((char) node.getLogicalID(),
						        (JawaConfiguration) node.getP3Configuration());
						StorePowerRackConf.store(message, getSensorNetworkById(node.getNetworkID()));
					} catch (ClassCastException e) {
						log.error("Unknown P3 Exception", e);
					}
				}

				if (sendConfigToSC) {
					log.error("sendConfigToSC ?  really?  we shouldn't be here ");// FIXME
					// TODO FIXME
					// do we know for sure that it's already running
					// P3Receive p3svc = (P3Receive)
					// services.get(OSSServices.OSS_SERVICE_P3);
					// p3svc.sendJawaConfigPacketImport(node.getId(),
					// node.getP3Configuration()) ;
					// will this even work or do we need to create a message and
					// send to softbase29?
				}
			}
		}
	}

	private static void removeNetwork(int networkID) {
		SensorNetwork nwk = null;
		for (SensorNetwork net : SynapStore.workerMap.keySet()) {
			if (net.getNetworkId() == networkID) {
				nwk = net;
				break;
			}
		}

		if (nwk == null) {
			log.warn("Couldn't find network (net id: " + networkID + ")");
			return;
		}

		SynapStore.workerMap.remove(nwk).close();
		log.info("Network (net id: " + networkID + ") has been removed.");
	}

	private static SensorNetwork createNetwork(ObjectDesc origNet, Collection<ObjectDesc> gws, byte[] pubkey,
	        byte[] privkey) {
		String sampling;
		try {
			sampling = origNet.getPropertyValue(WsnConstants.NETWORK_SAMPLING);
			// TODO clarify if sampling for networks has to be set in mapsense
		} catch (PropertyNotFoundException e) {
			sampling = "0";
		}
		TimeSample ts = TimeSample.get(0, new Integer(sampling));

		SensorNetwork net = new SensorNetwork(gws, getId(origNet));

		net.setNetworkName(origNet.getPropertyValue(WsnConstants.NETWORK_NAME));
		net.setSampleInterval(ts.getValueInUnits(), ts.getUnitID());
		net.setBaseStationPort(4920);
		net.setVersion(origNet.getPropertyValue(WsnConstants.NETWORK_VERSION));
		net.setPanId(Integer.parseInt(origNet.getPropertyValue(WsnConstants.NETWORK_PAN_ID)));
		net.setId(origNet.getID());
		net.setPublicKey(pubkey);
		net.setEncipheredPrivateKey(privkey);

		log.info("Created Sensor Network: " + net.getNetworkName() + " with PAN ID "
		        + Integer.toHexString(net.getPanId()) + " (original "
		        + origNet.getPropertyValue(WsnConstants.NETWORK_PAN_ID) + ")");

		return net;
	}

	public static Collection<SensorNetwork> getNetworks() {
		return networks.values();
	}

	public static SensorNetwork getSensorNetworkById(int networkId) {
		return networks.get(networkId);
	}

	public static WSNNode getNode(NodeID id) {
		return nodes.get(id);
	}

	public static WSNNode getNode(int networkID, int logicalID) {
		return nodes.get(new NodeID(networkID, logicalID));
	}

	public static ArrayList<WSNNode> getAllNodesinNetwork(int networkid) {
		ArrayList<WSNNode> list = new ArrayList<WSNNode>();
		for (WSNNode node : nodes.values()) {
			if (node.getNetworkID() == networkid && node.getPlatformID() != WSNNode.PLATFORM_GATEWAY)
				list.add(node);
		}
		return list;
	}

	public static WSNNode getNodeByPhysID(final long physID) {
		return nodeFinder.find(new Predicate<WSNNode>() {
			@Override
			public boolean evaluate(WSNNode value) {
				return value.getPhysID() == physID;
			}
		});
	}

	// Getting the highest logical ID. Used for WSNPlugin standalone mode
	public static int getLastNodeId(int networkID) {
		int lastnodeid = 0;
		for (WSNNode node : nodes.values()) {
			if (node.getNetworkID() == networkID && node.getLogicalID() > lastnodeid)
				lastnodeid = node.getLogicalID();
		}
		return lastnodeid;
	}

	public static WSNNode getNodeByObjectID(final String objectID) {
		// yet another dumb lookup
		return nodeFinder.find(new Predicate<WSNNode>() {
			@Override
			public boolean evaluate(WSNNode value) {
				return value.getId().equals(objectID);
			}
		});
	}

	public static WSNActuator getActuatorByObjectID(final String objectID) {
		return actuators.get(objectID);
	}

	public static WSNSensor getSensorByObjectID(final String objectID) {
		return sensors.get(objectID);
	}

	public static void registerNode(WSNNode node, boolean wasHereBefore) {
		log.info("registering new node (physID: " + Long.toHexString(node.getPhysID()) + ")");
		nodes.put(node.getUniqueID(), node);
		Collection<WSNActuator> nodeactuators = node.getActuators();
		for (WSNActuator actuator : nodeactuators) {
			actuators.put(actuator.getId(), actuator);
		}
		Collection<WSNSensor> s = node.getSensors();
		for (WSNSensor sen : s) {
			log.trace("Add sensor " + sen.getId());
			sensors.put(sen.getId(), sen);
		}

		if (wasHereBefore)
			changeSmartSendSetting(node);
	}

	public static void unregisterNode(WSNNode node) {
		log.info("unregistering the node (physID: " + Long.toHexString(node.getPhysID()) + ")");
		if (nodes.remove(node.getUniqueID()) == null) {
			log.warn("no node (physID: " + Long.toHexString(node.getPhysID()) + ") is found.");
		}
		Collection<WSNActuator> nodeactuators = node.getActuators();
		for (WSNActuator actuator : nodeactuators) {
			actuators.remove(actuator.getId());
		}
		Collection<WSNSensor> s = node.getSensors();
		for (WSNSensor sen : s) {
			sensors.remove(sen.getId());
		}
	}

	public static Collection<WSNNode> getNodes() {
		return Collections.unmodifiableCollection(nodes.values());
	}

	private static Softbase getSoftbase(int netID) {
		for (SensorNetwork net : SynapStore.workerMap.keySet()) {
			if (net.getNetworkId() == netID) {
				if (SynapStore.workerMap.get(net) == null) {
					return null;
				} else {
					return SynapStore.workerMap.get(net).getSoftbase();
				}
			}
		}
		return null;
	}

	private static int getId(ObjectDesc obj) {
		int result = -1;
		if (obj != null) {
			String toId = obj.getID();
			if (toId.contains(":")) {
				try {
					result = Integer.parseInt(toId.split(":")[1]);

				} catch (NumberFormatException e) {
					log.warn("Object has mailformed ID : " + obj.getID());
				}
			}
		}
		return result;
	}
}
