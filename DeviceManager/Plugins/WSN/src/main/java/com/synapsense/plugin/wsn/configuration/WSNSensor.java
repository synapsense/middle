package com.synapsense.plugin.wsn.configuration;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.synapsense.plugin.api.exceptions.PropertyNotFoundException;
import com.synapsense.plugin.apinew.ObjectDesc;
import com.synapsense.plugin.apinew.PropertyDesc;
import com.synapsense.plugin.wsn.WsnConstants;
import com.synapsense.plugin.wsn.WsnConstants.SensorOptions;

public class WSNSensor implements ConfigurationObject {

	private final static Logger log = Logger.getLogger(WSNSensor.class);

	public final static int SENSOR_ACKBAR_POSITION = 38;

	private final int channel;
	private final int type;
	private final double min;
	private final double max;
	private double lastValue;
	private final Map<String, String> options;
	private final int samplingInSec;
	private boolean hasSampling;
	private WSNNode node;
	private final String id;

	public WSNSensor(ObjectDesc initial, WSNNode node) {
		this.id = initial.getID();
		this.node = node;
		log.trace("Building sensor object " + this.id);

		this.channel = new Integer(initial.getPropertyValue(WsnConstants.SENSOR_CHANNEL));
		this.type = new Integer(initial.getPropertyValue(WsnConstants.SENSOR_TYPE));

		if (initial.getPropertyValue(WsnConstants.SENSOR_MIN) != null)
			this.min = new Double(initial.getPropertyValue(WsnConstants.SENSOR_MIN));
		else {
			this.min = 0;
			log.warn("Property SENSOR_MIN was NULL");
		}

		if (initial.getPropertyValue(WsnConstants.SENSOR_MAX) != null)
			this.max = new Double(initial.getPropertyValue(WsnConstants.SENSOR_MAX));
		else {
			this.max = 0;
			log.warn("Property SENSOR_MAX was NULL");
		}

		int sampl;
		try {
			sampl = new Integer(initial.getPropertyValue(WsnConstants.SENSOR_SAMPLING));
			this.hasSampling = true;
		} catch (PropertyNotFoundException e) {
			sampl = 0;
			this.hasSampling = false;
		}
		this.samplingInSec = sampl;
		lastValue = 0;
		options = new HashMap<String, String>();
		for (SensorOptions opt : SensorOptions.values()) {
			try {
				PropertyDesc optionalProp = initial.getProperty(opt.getValue());
				if (optionalProp != null && !"".equals(optionalProp.getValue())) {
					options.put(optionalProp.getName(), optionalProp.getValue());
				}
			} catch (PropertyNotFoundException e) {
				// property not found, skip it
			}
		}
	}

	public WSNSensor(WSNNode node, String id, int channel, int type, int sampl, Map<String, String> options) {
		this.id = id;
		this.node = node;
		this.channel = channel;
		this.type = type;
		this.min = 0;
		this.max = 0;
		this.samplingInSec = sampl;
		this.options = options;
	}

	public synchronized final boolean hasSampling() {
		return hasSampling;
	}

	public Map<String, String> getOptions() {
		return options;
	}

	public int getChannel() {
		return channel;
	}

	public int getType() {
		return type;
	}

	public double getMin() {
		return min;
	}

	public double getMax() {
		return max;
	}

	public double getScaleMin() {
		if (options.containsKey("scaleMin"))
			return Double.valueOf(options.get("scaleMin"));
		else
			return 0.0;
	}

	public double getScaleMax() {
		if (options.containsKey("scaleMax"))
			return Double.valueOf(options.get("scaleMax"));
		else
			return 0.0;
	}

	public Double getDeltaTLow() {
		String t = options.get(WsnConstants.SENSOR_DELTA_T_LOW);
		if (t == null)
			return null;
		return Double.valueOf(t);
	}

	public Double getDeltaTHigh() {
		String t = options.get(WsnConstants.SENSOR_DELTA_T_HIGH);
		if (t == null)
			return null;
		return Double.valueOf(t);
	}

	public Double getCriticalPoint() {
		String t = options.get(WsnConstants.SENSOR_CRITICAL_POINT);
		if (t == null)
			return null;
		return Double.valueOf(t);
	}

	public Integer getSubSampleInterval() {
		String value = options.get(WsnConstants.SENSOR_SUBSAMPLE_INTERVAL);
		if (value == null)
			return null;
		return Integer.valueOf(value);
	}

	public double getLastValue() {
		return lastValue;
	}

	public void setLastValue(double lastValue) {
		this.lastValue = lastValue;
	}

	/**
	 * returns a specific option for the sensor
	 * 
	 * @param optionName
	 *            name of the option
	 * @return option value or null if the option with name
	 *         <code>optionName</code> is not specified
	 */
	public String getOption(String optionName) {
		return options.get(optionName);
	}

	public int getSamplingInSec() {
		return samplingInSec;
	}

	public String getId() {
		return id;
	}

	public WSNNode getNode() {
		return node;
	}
}
