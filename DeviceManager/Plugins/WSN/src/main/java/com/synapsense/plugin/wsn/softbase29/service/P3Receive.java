package com.synapsense.plugin.wsn.softbase29.service;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.plugin.wsn.iocore.frames.out.MoteUARTOutSetParameterFrame;
import com.synapsense.plugin.wsn.msgbus.MsgDuoConfig;
import com.synapsense.plugin.wsn.msgbus.MsgDuoData;
import com.synapsense.plugin.wsn.msgbus.MsgJawaConfig;
import com.synapsense.plugin.wsn.msgbus.MsgJawaData;
import com.synapsense.plugin.wsn.p3.DuoConfiguration;
import com.synapsense.plugin.wsn.p3.JawaConfigSpooler;
import com.synapsense.plugin.wsn.p3.JawaConfiguration;
import com.synapsense.plugin.wsn.p3.P3ConfigException;
import com.synapsense.plugin.wsn.p3.P3Updater;
import com.synapsense.plugin.wsn.softbase29.Softbase;
import com.synapsense.plugin.wsn.softbase29.packet.MultiMessage;
import com.synapsense.plugin.wsn.softbase29.packet.RadioMsg;
import com.synapsense.plugin.wsn.softbase29.state.NodeInfoEntry;
import com.synapsense.plugin.wsn.util.Constants;
import com.synapsense.plugin.wsn.util.serializer.BigSerializer;

public class P3Receive implements Runnable, Service {
	private Softbase softbase;
	private static Log log = LogFactory.getLog(P3Receive.class);
	private volatile boolean interrupted = false;

	private static final int SUBTYPE_JAWA_CONFIG = 0x10;
	private static final int SUBTYPE_DUO_CONFIG = 0x11;
	private static final int SUBTYPE_JAWA_CONFIG_IMPORT_ERROR = 0x12;
	private static final int SUBTYPE_JAWA_DATA = 0x32;
	private static final int SUBTYPE_DUO_DATA = 0x33;

	private static final byte CMD_CKSUMOK = 0x12;
	private static final byte CMD_CKSUMFAIL = 0x13;
	private static final byte CMD_STALE_CONFIG = 0x14;
	public static final byte CMD_NEW_CONFIG = 0x15;

	private static BlockingQueue<MoteUARTOutSetParameterFrame> queue;
	private static JawaConfigSpooler sender;

	private static String[] JawaConfigInputErrors = { "NONE", "CHECKSUM_FAIL", "TIMESTAMP_TOO_OLD",
	        "MISSING_PREV_FRAME", "RX_IN_SMOTA", "OTHER" };

	public P3Receive(Softbase softbase) {
		this.softbase = softbase;
		queue = new ArrayBlockingQueue<MoteUARTOutSetParameterFrame>(10);
		sender = new JawaConfigSpooler(this.softbase, queue);
		new Thread(sender).start();
	}

	public void close() {

	}

	public void signalConfig(char to, long targetCkSum, boolean status) {
		byte[] resp = new byte[12];
		BigSerializer respserial = new BigSerializer(resp);

		if (!status) {
			respserial.packU_INT32(CMD_CKSUMFAIL);
			log.trace("Sending CMD_CKSUMFAIL");
		} else {
			respserial.packU_INT32(CMD_CKSUMOK);
			log.trace("Sending CMD_CKSUMOK");
		}

		respserial.packU_INT32(targetCkSum);

		MoteUARTOutSetParameterFrame frame = new MoteUARTOutSetParameterFrame(to, Constants.PARAM_ID_CMDPLUGMETER, 0,
		        resp);
		softbase.getCommandQ().offer(frame);
	}

	// public void sendJawaConfigPacketImport(char to, JawaConfiguration p3c,
	// boolean isStale) {
	public void signalConfigStale(char to, JawaConfiguration p3c) {
		byte[] resp = new byte[12];
		BigSerializer respserial = new BigSerializer(resp);

		if (p3c.getVersionCode() == 0) {
			log.trace("Tried to send Stale message to " + to + " but it has versioncode 0 !!!");
			return;
		}

		respserial.packU_INT32(CMD_STALE_CONFIG);
		respserial.packU_INT32(0x12345678); // dummy

		MoteUARTOutSetParameterFrame frame = new MoteUARTOutSetParameterFrame(to, Constants.PARAM_ID_CMDPLUGMETER, 0,
		        resp);
		softbase.getCommandQ().offer(frame);

	}

	public void sendJawaConfigPacketImport(char to, JawaConfiguration p3c) {
		// byte[] resp = new byte[20];
		// BigSerializer respserial = new BigSerializer(resp);

		if (p3c.getVersionCode() == 0) {
			log.trace("Tried to send message to " + to + " but it has versioncode 0 !!!");
			return;
		}

		log.trace("Sending config update message to " + to);
		// respserial.packU_INT32(CMD_NEW_CONFIG);
		// respserial.packU_INT32(0x00112233);// garbage data so I can see
		// // what's
		// // going on FIXME
		// respserial.packU_INT32(0x44556677);
		// respserial.packU_INT32(0x8899AABB);
		// respserial.packU_INT32(0xCCDDEEFF);

		// respserial.copyData(p3c.serialize());

		// log.trace("resp.length = " + resp.length);
		//
		// String s = "sending (" + 20 + ")";
		// for (int i = 0; i < 20; i++) {
		// s += " " + resp[i];
		// }
		// log.trace(s);

		// P3Updater p = new P3Updater(queue, to, resp);
		P3Updater p = new P3Updater(queue, to, p3c.serialize());
		new Thread(p).start();

		// MoteUARTOutSetParameterFrame frame = new
		// MoteUARTOutSetParameterFrame(to, Constants.PARAM_ID_CMDPLUGMETER, 0,
		// resp);
		//
		// try {
		// queue.put(frame);
		// } catch (InterruptedException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }

		// softbase.getCommandQ().offer(frame);

	}

	public void run() {
		RadioMsg msg;

		while (!interrupted) {
			try {
				try {
					msg = softbase.nwk.receive(OSSServices.OSS_SERVICE_P3);
				} catch (InterruptedException e1) {
					interrupted = true;
					continue;
				}

				long sendtime = 0;
				int subtype;

				BigSerializer serial = new BigSerializer(msg.data);
				sendtime = serial.unpackU_INT32();
				subtype = serial.unpackU_INT8();

				/* Do a debug print */
				StringBuffer buf = new StringBuffer();
				for (int i = 4; i < msg.data.length; i++)
					buf.append(Integer.toHexString(msg.data[i] & 0xFF) + " ");
				log.info("PM: " + sendtime + " " + buf.toString());

				NodeInfoEntry nodeInfo = softbase.node_info.getById(msg.sourceid);
				log.info("P3 Packet from " + Integer.toHexString(msg.sourceid) + " " + subtype);
				switch (subtype) {

				case SUBTYPE_JAWA_CONFIG:
					log.info("Jawa configuration");
					processJawaConfigPacket(msg, serial, nodeInfo);
					break;

				case SUBTYPE_DUO_CONFIG:
					log.info("Duo configuration");
					processDuoConfigPacket(msg, serial, nodeInfo);
					break;

				case SUBTYPE_JAWA_DATA:
					MsgJawaData p3data = new MsgJawaData(serial.unpack_remaining_bytes(), msg.sourceid, sendtime);
					softbase.postMessage(p3data);
					break;

				case SUBTYPE_DUO_DATA:
					MsgDuoData duodata = new MsgDuoData(serial.unpack_remaining_bytes(), msg.sourceid, sendtime);
					softbase.postMessage(duodata);
					break;

				case SUBTYPE_JAWA_CONFIG_IMPORT_ERROR:
					byte content[] = serial.unpack_remaining_bytes();
					log.error("SandCrawler Reports Power Asset Import Error: " + content[0] + " ("
					        + JawaConfigInputErrors[content[0]] + ")");
					break;

				default:
					log.info("Unknown P3 subtype: " + subtype);
					break;
				}
				log.info("P3 packet processed");

			} catch (Exception e) {
				log.error("P3 receive exception", e);
			}
		}
	}

	/**
	 * @TODO: Blatant copy and paste - make one method!
	 * @param msg
	 * @param serial
	 * @param nodeInfo
	 */

	private void processDuoConfigPacket(RadioMsg msg, BigSerializer serial, NodeInfoEntry nodeInfo) {

		byte[] mmd = serial.unpack_remaining_bytes();
		MultiMessage mm = nodeInfo.addToMultiMessage(mmd);

		if (mm.isComplete()) {
			try {
				log.info("Config packet complete!");
				DuoConfiguration p3c = new DuoConfiguration(mm.getData());
				log.info("Checksum from received packet:" + Long.toHexString(p3c.getSentChecksum()));
				log.info("Checksum computed from packet:" + Long.toHexString(p3c.getChecksum()));
				MsgDuoConfig cmsg = new MsgDuoConfig(msg.sourceid, p3c);

				if (p3c.getChecksum() != p3c.getSentChecksum()) {
					log.info("Sending checksum result of failed");
					signalConfig(msg.sourceid, p3c.getChecksum(), false);
				} else {
					log.info("Sending checksum result of passed");
					signalConfig(msg.sourceid, p3c.getSentChecksum(), true);
					softbase.postMessage(cmsg);
				}

			} catch (P3ConfigException e) {
				log.error("Can't deserialize the P3 Duo configuration - the world is broken", e);
				/* Signal error */
				signalConfig(msg.sourceid, -1, false);
			}

		}
	}

	private void processJawaConfigPacket(RadioMsg msg, BigSerializer serial, NodeInfoEntry nodeInfo) {

		byte[] mmd = serial.unpack_remaining_bytes();
		MultiMessage mm = nodeInfo.addToMultiMessage(mmd);

		if (mm.isComplete()) {
			log.info("Config packet complete!");
			try {
				log.info("Config packet complete! size = " + mm.getData().length); // FIXME??
				JawaConfiguration p3c = new JawaConfiguration(mm.getData());
				log.info("Checksum from received packet:" + Long.toHexString(p3c.getSentChecksum()));
				log.info("Checksum computed from packet:" + Long.toHexString(p3c.getChecksum()));
				MsgJawaConfig cmsg = new MsgJawaConfig(msg.sourceid, p3c);

				if (p3c.getChecksum() != p3c.getSentChecksum()) {
					log.info("Sending checksum result of failed");
					signalConfig(msg.sourceid, p3c.getChecksum(), false);
					// softbase.postMessage(cmsg);
				} else {
					log.info("Sending checksum result of passed");
					signalConfig(msg.sourceid, p3c.getSentChecksum(), true);
					softbase.postMessage(cmsg);

					// FIXME REMOVE THIS SECTION
					// signalConfigStale(msg.sourceid, p3c);
					// sendJawaConfigPacketImport(msg.sourceid, p3c);
					// FIXME REMOVE THIS SECTION
				}

			} catch (P3ConfigException e) {
				log.error("Can't deserialize the P3 Jawa configuration - the world is broken", e);
				/* Signal error */
				signalConfig(msg.sourceid, -1, false);
			}

		}
	}

	public void interrupt() {
		interrupted = true;
	}

	public byte getServiceID() {
		return OSSServices.OSS_SERVICE_P3;
	}

}
