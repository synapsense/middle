package com.synapsense.plugin.wsn.iocore.n29.frames.out;

import com.synapsense.plugin.wsn.iocore.frames.out.MoteUARTOutFrame;
import com.synapsense.plugin.wsn.softbase29.GateWay;
import com.synapsense.plugin.wsn.util.serializer.BigSerializer;

/**
 * 
 * @author Yann
 * 
 */

public class MoteUARTOutNodeIDFrame extends MoteUARTOutFrame {

	// public static HashMap<String,Integer> parameters = new
	// HashMap<String,Integer>();
	public long destination;
	public char id;
	public int sampleInterval;
	public char source;
	public GateWay gateway;
	public int delta[] = new int[10];
	public int subsamples;
	public boolean secEnable;

	public MoteUARTOutNodeIDFrame(long destination, char id, int sampleInterval, char source, GateWay gateway,
	        boolean secEnable) {
		super((char) MoteUARTOutFrame.TYPE_NODEID, (char) 16);
		BigSerializer ser = new BigSerializer(frame, 4);
		ser.pack_INT64(destination);
		ser.packU_INT16(id);
		ser.packU_INT32(sampleInterval);
		ser.packU_INT16(source);
		this.destination = destination;
		this.id = id;
		this.sampleInterval = sampleInterval;
		this.source = source;
		this.gateway = gateway;
		this.secEnable = secEnable;
	}

	/**
	 * Set the delta for a channel. Note that channels 0-2 are not at all
	 * supported for subsampling
	 */
	public void setDelta(int channel, int delta) {
		this.delta[channel - 3] = delta;
	}

	public void setSubsamples(int subsamples) {
		this.subsamples = subsamples;
	}

}
