package com.synapsense.plugin.wsn.knownprops.exceptions;

public class MalformedPropertyTokenException extends Exception {
	public MalformedPropertyTokenException(String desc) {
		super(desc);
	}

	public MalformedPropertyTokenException(String desc, Throwable reason) {
		super(desc, reason);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 5883072623349098077L;
}