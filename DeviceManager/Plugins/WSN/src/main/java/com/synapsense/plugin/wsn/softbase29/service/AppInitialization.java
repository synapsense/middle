package com.synapsense.plugin.wsn.softbase29.service;

import java.util.Collection;
import java.util.LinkedList;
import java.util.Random;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.plugin.wsn.WsnConstants;
import com.synapsense.plugin.wsn.configuration.Configuration;
import com.synapsense.plugin.wsn.configuration.ConfigurationObject;
import com.synapsense.plugin.wsn.configuration.SensorNotFoundException;
import com.synapsense.plugin.wsn.configuration.WSNActuator;
import com.synapsense.plugin.wsn.configuration.WSNNode;
import com.synapsense.plugin.wsn.configuration.WSNSensor;
import com.synapsense.plugin.wsn.iocore.frames.out.MoteUARTOutSetParameterFrame;
import com.synapsense.plugin.wsn.msgbus.Message;
import com.synapsense.plugin.wsn.softbase29.Softbase;
import com.synapsense.plugin.wsn.softbase29.packet.RadioMsg;
import com.synapsense.plugin.wsn.util.Constants;
import com.synapsense.plugin.wsn.util.serializer.BigSerializer;
import com.synapsense.plugin.wsn.util.serializer.Serializer;

public class AppInitialization implements Runnable, Service {

	private final static byte APPINIT_REQUEST = 1;
	private final static byte APPINIT_CONFIRM = 2;

	private final static int TOCONFIM_RETRY_LIMIT = 10;
	/* Minimum time before retrying in seconds */
	private final static int MIN_RETRY_TIME = 60;
	/* Minimum time between retries in seconds */
	private final static int MIN_RETRY_INTERVAL = 10;

	private static Log log = LogFactory.getLog(AppInitialization.class);

	private Softbase softbase;
	private volatile boolean interrupted = false;
	private RetryConfirm retryconfirm;

	/**
	 * Map of packets which need a APPCONFIRM notification from the device, or
	 * else they will be retried.
	 */
	public AppInitialization(Softbase softbase) {
		this.softbase = softbase;
		retryconfirm = new RetryConfirm();
		new Thread(retryconfirm, "App-Init retry").start();
	}

	public void close() {

	}

	public void run() {
		RadioMsg msg;

		while (!interrupted) {
			try {
				try {
					msg = softbase.nwk.receive(OSSServices.OSS_SERVICE_APP_INITIALIZATION);
				} catch (InterruptedException e) {
					interrupted = true;
					continue;
				}
				switch (msg.data[0]) {
				case APPINIT_REQUEST:
					log.info("Processing AppInit Request from 0x" + Integer.toHexString(msg.sourceid));
					WSNNode node = Configuration.getNode(softbase.getNetId(), msg.sourceid);
					sendAppInit(node, null, null);
					break;
				case APPINIT_CONFIRM:
					log.info("Processing AppInit Confirmation from 0x" + Integer.toHexString(msg.sourceid));
					WSNNode nodec = Configuration.getNode(softbase.getNetId(), msg.sourceid);
					Serializer s = new BigSerializer(msg.data);
					int code = s.unpack_INT8();
					long timeat = s.unpackU_INT32();
					processConfirm(nodec, timeat);
					break;
				default:
					log.info("Not processed App Init type: " + msg.data[0]);
				}
			} catch (Exception e) {
				log.error("Can't process appinit: ", e);
			}
		}

	}

	private void processConfirm(WSNNode node, long timeat) {
		final int logicalID = node.getLogicalID();
		ToConfirmPacket tcp = retryconfirm.get(logicalID);
		if (tcp != null) {
			if ((tcp.serial & 0xFFFFFFFFL) == (timeat & 0xFFFFFFFFL)) {
				retryconfirm.remove(logicalID);
				log.info("Processed confirmation from node 0x" + Integer.toHexString(logicalID));
			} else {
				log.warn("Confirmation for node 0x" + Integer.toHexString(logicalID)
				        + " is not of the same serial. Node: " + Long.toHexString(timeat) + " server "
				        + Long.toHexString(tcp.serial));
			}
		} else {
			log.warn("Logical ID 0x" + Integer.toHexString(logicalID) + " does not have a pending confirmation");
		}
	}

	public boolean sendAppInit(WSNNode node, ConfigurationObject cobj, Message msg) {
		if (node.getPlatformID() == WSNNode.PLATFORM_ACKBAR || node.getPlatformID() == WSNNode.PLATFORM_NTB) {
			sendActuatorAppInit(node, msg);
			return true;
		} else if (node.getPlatformID() == WSNNode.PLATFORM_CONSTELLATION2) {
			sendConstellationAppInit(node);
			return true;
		} else if (node.getPlatformID() == WSNNode.PLATFORM_YWING || node.getPlatformID() == WSNNode.PLATFORM_HWING
		        || node.getPlatformID() == WSNNode.PLATFORM_NTBNP) {
			sendYHwingAppInit(node);
			return true;
		} else {
			log.warn("Unknown app initialization platform for node " + Long.toHexString(node.getPhysID()));
			return false;
		}
	}

	private long getTimeSend() {
		long settime = System.currentTimeMillis() / 100;
		if ((settime & 0xFFFFFFFFL) == 0)
			settime = 1; // 0 is considered as not set in the node
		return settime & 0xFFFFFFFFL;
	}

	private void sendYHwingAppInit(WSNNode node) {
		long timesent = getTimeSend();
		byte[] command = new byte[4 + 2 + 2 + 2 + 4];
		Serializer serial = new BigSerializer(command);
		serial.packU_INT32(timesent);

		try {
			WSNSensor s = node.getSensor(3);
			Double dtld = s.getDeltaTLow();
			Double dthd = s.getDeltaTHigh();
			Double dtcp = s.getCriticalPoint();
			Integer subsampleinterval = s.getSubSampleInterval();
			log.trace(Long.toHexString(node.getPhysID()) + ": Y/H-Wing SmartSend2: dth: " + dthd + " dtl: " + dtld
			        + " crit: " + dtcp + " subsampling: " + subsampleinterval);
			if (dtld == null || dthd == null || dtcp == null || subsampleinterval == null) {
				log.error("SmartSend 2 properties are null, cannot continue.");
				return;
			}
			char dtl = (char) (dtld * 10.0);
			char dth = (char) (dthd * 10.0);
			char cp = (char) (dtcp * 10.0);

			serial.packU_INT16(cp);
			serial.packU_INT16(dth);
			serial.packU_INT16(dtl);
			serial.packU_INT32(subsampleinterval.intValue());

			Message m = new MoteUARTOutSetParameterFrame((char) node.getLogicalID(), Constants.PARAM_ID_YHWINGCONTROL,
			        0, command);
			// Register a packet into ToConfirmPacket list
			ToConfirmPacket tcp = new ToConfirmPacket(m, node.getLogicalID(), timesent);
			retryconfirm.add(tcp);
			log.trace("Updated Y/H-Wing appinit parameters sent to " + Long.toHexString(node.getPhysID()));

		} catch (SensorNotFoundException e) {
			log.error("Can't populate ywing app init due to missing sensor ", e);
		} catch (Exception e) {
			log.error("General appinit error", e);
		}

	}

	private void sendConstellationAppInit(WSNNode node) {
		int width = Integer.MAX_VALUE;

		// Locate the minimum valid sensor pulse width
		for (WSNSensor s : node.getSensors()) {
			String d = s.getOption(WsnConstants.SENSOR_PULSE_WIDTH);
			if (d != null) {
				int v = Integer.parseInt(d);
				if (v > 0 && v < width) {
					width = v;
				}
			}
		}

		MoteUARTOutSetParameterFrame f = new MoteUARTOutSetParameterFrame((char) node.getLogicalID(),
		        Constants.PARAM_ID_UPDATE_PULSE_WIDTH, width);
		softbase.getCommandQ().offer(f);
		log.info("Updating pulse sample width for constellation II to " + width + " ms");
	}

	private void sendActuatorAppInit(WSNNode node, Message msg) {
		Collection<WSNActuator> actuators = node.getActuators();
		if (actuators.size() == 0)
			return;
		// Send Initial configuration message when it has actuators
		byte[] command = new byte[7 + actuators.size() * 9];
		Serializer serial = new BigSerializer(command);
		serial.packU_INT8((byte) 1); // Version is 1
		long settime = this.getTimeSend();
		serial.packU_INT32(settime);
		serial.packU_INT8(WSNActuator.CONTROL_WSNACTUATE);
		serial.packU_INT8((byte) actuators.size());
		for (WSNActuator actuator : actuators) {
			serial.packU_INT8((byte) actuator.getActuatorNumber());
			serial.packU_INT8((byte) actuator.getDirection());
			serial.packU_INT8((byte) actuator.getNextValue());
			serial.packU_INT8((byte) actuator.getMinpoint());
			serial.packU_INT8((byte) actuator.getMaxpoint());
			serial.packU_INT16((char) actuator.getTravelTime());
			serial.packU_INT8((byte) actuator.getFailsafesetpoint());
			serial.packU_INT8((byte) actuator.getPositionTolerance());
		}
		MoteUARTOutSetParameterFrame frame = new MoteUARTOutSetParameterFrame((char) node.getLogicalID(),
		        Constants.PARAM_ID_ACTUATOR, 0, command);
		frame.chain(msg);
		softbase.getCommandQ().offer(frame);
	}

	public byte getServiceID() {
		return OSSServices.OSS_SERVICE_APP_INITIALIZATION;
	}

	/*
	 * Holder class for confirmation packets
	 */
	private class ToConfirmPacket {

		ToConfirmPacket(Message m, int id, long s) {
			toConfirm = m;
			serial = s;
			retry = TOCONFIM_RETRY_LIMIT;
			backoff = 0;
			nodeId = id;
		}

		public int nodeId;
		public final Message toConfirm;
		public final long serial;
		public int retry;
		public int backoff;

		public String toString() {
			return "0x" + Integer.toHexString(nodeId) + " retry: " + retry + " backoff: " + backoff;
		}
	}

	private class RetryConfirm implements Runnable {
		private final LinkedList<ToConfirmPacket> confirmPackets = new LinkedList<ToConfirmPacket>();

		public void run() {
			long senttime = System.currentTimeMillis();
			while (true) {
				synchronized (confirmPackets) {
					if (confirmPackets.size() > 0) {
						long waitstarttime = System.currentTimeMillis();
						long waittime = confirmPackets.getFirst().backoff * 1000;
						// Wait at least minimum interval
						if (waittime < (MIN_RETRY_INTERVAL * 1000) - (System.currentTimeMillis() - senttime))
							waittime = (MIN_RETRY_INTERVAL * 1000) - (System.currentTimeMillis() - senttime);
						try {
							if (waittime > 0)
								confirmPackets.wait(waittime);
						} catch (InterruptedException e) {
							break;
						}
						long waited = System.currentTimeMillis() - waitstarttime;
						for (ToConfirmPacket tcp : confirmPackets) {
							tcp.backoff = tcp.backoff - (int) (waited / 1000);
							if (tcp.backoff < 0)
								tcp.backoff = 0;
						}
						/* Wait at least MIN_RETRY_INTERVAL between send */
						if (System.currentTimeMillis() - senttime < MIN_RETRY_INTERVAL * 1000)
							continue;
						if (confirmPackets.size() > 0) {
							ToConfirmPacket tcp = confirmPackets.remove();
							softbase.getCommandQ().offer(tcp.toConfirm);
							log.info("Sending to 0x" + Integer.toHexString(tcp.nodeId));
							senttime = System.currentTimeMillis();
							tcp.retry--;
							if (tcp.retry > 0) {
								tcp.backoff = new Random()
								        .nextInt((int) Math.pow(2, TOCONFIM_RETRY_LIMIT - tcp.retry) * 4)
								        + MIN_RETRY_TIME;
								log.info("Repeating appinit message for 0x" + Integer.toHexString(tcp.nodeId)
								        + " set backoff to " + (int) tcp.backoff + " seconds.");
								add(tcp);
							}
						}

					} else {
						try {
							confirmPackets.wait();
						} catch (InterruptedException e) {
							break;
						}
					}
				}
			}
		}

		public void add(ToConfirmPacket newtcp) {
			synchronized (confirmPackets) {
				/* Remove if already present */
				for (int i = 0; i < confirmPackets.size(); i++) {
					if (confirmPackets.get(i).nodeId == newtcp.nodeId) {
						confirmPackets.remove(i);
						break;
					}
				}
				/* Maintain sorted list */
				int i;
				for (i = 0; i < confirmPackets.size(); i++) {
					if (confirmPackets.get(i).backoff > newtcp.backoff)
						break;
				}
				confirmPackets.add(i, newtcp);
				// logconfirm();
				confirmPackets.notify();
			}
		}

		public ToConfirmPacket get(int id) {
			for (ToConfirmPacket tcp : confirmPackets) {
				if (tcp.nodeId == id) {
					return tcp;
				}
			}
			return null;
		}

		public void remove(int id) {
			synchronized (confirmPackets) {
				for (ToConfirmPacket tcp : confirmPackets) {
					if (tcp.nodeId == id) {
						confirmPackets.remove(tcp);
						break;
					}
				}
			}
		}

		public void logconfirm() {
			log.info("Confirm list");
			for (ToConfirmPacket tcp : confirmPackets) {
				log.info(tcp);
			}
		}
	}
}
