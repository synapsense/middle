package com.synapsense.plugin.wsn.util.sensors;

import com.synapsense.plugin.wsn.util.SensorStates;

public class FusionThermistor implements SensorConversionInterface {

	public double convertFromRaw(char raw) {
		// voltage is measured in 12bits ADCCounts
		// 0 -> 0V
		// 4095 -> 2.5V
		double rawV = raw & 0xFFF;
		int rawVr = ((raw >> 12) & 0xF);

		rawVr *= 8;
		rawVr += 3276;

		double Vref = 2.048; // (double) rawVr / 4095 * 2.5;
		double Vadc = (double) rawV / 4095 * 2.5;
		// System.err.println(Vref + " " + Vadc);

		// double beta = 3892;
		double beta = 3950;
		double T0 = 298.15;

		try {

			double tempK = (beta)
			        / (Math.log(((10000 * Vref * 1.25) - (10000 * Vadc)) / Vadc) + ((beta / T0) - Math.log(10000)));
			double tempC = tempK - 273.15;
			// System.err.println("tempC: " + tempC);
			// System.err.println(" Pat's calc: " +( -0.36+(0.11*(tempC/5))));
			// tempC += -0.36+(0.11*(tempC/5));
			// System.err.println(" after tempC: " + tempC);
			double tempF = (9.0 / 5.0) * tempC + 32;

			// Fusion doesn't seem to go toward ABSF like other sensors. Our
			// spec is 0-40C, so -10 is right out
			if (tempF <= -10) {
				return SensorStates.SENSOR_DISCONNECT;
			}

			if (tempF > 190) {
				return SensorStates.SENSOR_DISCONNECT;
			}

			return tempF;
		} catch (Exception e) {
			return SensorStates.SENSOR_DISCONNECT;
		}
	}

	public double convertFromRawCompensated(char raw, double vRef) {
		return convertFromRaw(raw);
	}

	public int getMoteDataType() {
		return 25;
	}

}
