package com.synapsense.plugin.wsn.iocore.n29.frames;

import com.synapsense.plugin.wsn.iocore.frames.MoteUARTBadFrame;
import com.synapsense.plugin.wsn.iocore.frames.MoteUARTFrame;

public class MoteUARTPrintFrame extends MoteUARTFrame {

	public String s = "";

	public MoteUARTPrintFrame(byte[] header, byte[] data, String portName) throws MoteUARTBadFrame {
		super(header, portName);
		this.data = data;

		s = new String(data);
	}

	public byte getData(int pos) {
		return this.data[pos];
	}

	public String toString() {
		return s;
	}
}
