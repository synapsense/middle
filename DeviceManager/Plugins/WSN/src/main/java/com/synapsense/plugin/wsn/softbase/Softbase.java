package com.synapsense.plugin.wsn.softbase;

import java.util.List;

import com.synapsense.plugin.wsn.configuration.WSNGW;

public interface Softbase {
	public List<com.synapsense.plugin.wsn.softbase.GateWay> getGateways();

	public void removeGateway(String portName);

	public void addGateway(WSNGW gw);

	public void addGateway(WSNGW gw, char logicalID);

	public void sendCommand(int command, char nodeid);

	public int getPanId();

	public void close();
}
