package com.synapsense.plugin.wsn.softbase29.state;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;

import com.synapsense.plugin.wsn.softbase29.GateWay;
import com.synapsense.plugin.wsn.softbase29.packet.MultiMessage;

public class NodeInfoEntry implements Comparable<NodeInfoEntry> {

	private static Logger log = Logger.getLogger(NodeInfoEntry.class);

	private char id;
	private LinkedList<Byte> seqnolist = new LinkedList<Byte>();
	private NodeInfoEntry parent;
	private GateWay parentgw;
	private int pkcount;
	private boolean canhear;
	private char hearfrom;
	private boolean fwd;
	private boolean updatedone;
	private boolean reported;
	private SecuritySupportedMode securitySupportedMode = SecuritySupportedMode.UNKNOWN;

	private List<MultiMessage> multiMessages = new ArrayList<MultiMessage>();

	public NodeInfoEntry() {
		reset();
	}

	public synchronized void addSeqLast(Byte b) {
		if (seqnolist.size() > 10)
			seqnolist.removeFirst();
		seqnolist.addLast(b);

	}

	public synchronized boolean hasSequence(Byte b) {
		return (seqnolist.contains(b));
	}

	public Byte nextSequence() {
		if (seqnolist.size() == 0)
			return null;

		return new Byte((byte) (seqnolist.getLast() + 1));
	}

	public void reset() {
		seqnolist.clear();
		multiMessages.clear();
		setParent(null);
		setPktCount(0);
	}

	public MultiMessage addToMultiMessage(byte[] data) {
		Iterator<MultiMessage> imm = multiMessages.iterator();
		while (imm.hasNext()) {
			MultiMessage mm = imm.next();

			if (mm.isTimedOut()) {
				imm.remove();
				log.info("Expired MM " + mm);
				continue;
			}

			if (mm.isPartOf(data)) {
				log.trace("Found existing mm: " + mm);
				mm.addMessage(data);
				return mm;
			}
		}
		MultiMessage mm = new MultiMessage();
		mm.addMessage(data);
		multiMessages.add(mm);
		log.trace("Created new multi-message " + mm);
		return mm;
	}

	@Override
	public int compareTo(NodeInfoEntry rhs) {

		return rhs.getId() - getId();
	}

	public boolean equals(NodeInfoEntry rhs) {
		return rhs.getId() == getId();
	}

	public void setId(char id) {
		this.id = id;
	}

	public char getId() {
		return id;
	}

	public void setParent(NodeInfoEntry parent) {
		this.parent = parent;
	}

	public NodeInfoEntry getParent() {
		return parent;
	}

	public void setParentgw(GateWay parentgw) {
		this.parentgw = parentgw;
	}

	public GateWay getParentgw() {
		return parentgw;
	}

	public void setPktCount(int pkcount) {
		this.pkcount = pkcount;
	}

	public int getPktCount() {
		return pkcount;
	}

	public void setCanHear(boolean canhear) {
		this.canhear = canhear;
	}

	public boolean canHear() {
		return canhear;
	}

	public void setHearFrom(char hearfrom) {
		this.hearfrom = hearfrom;
	}

	public char getHearFrom() {
		return hearfrom;
	}

	public void setFordward(boolean fwd) {
		this.fwd = fwd;
	}

	public boolean isForward() {
		return fwd;
	}

	public void setUpdateDone(boolean updatedone) {
		this.updatedone = updatedone;
	}

	public boolean isUpdateDone() {
		return updatedone;
	}

	public void setReported(boolean reported) {
		this.reported = reported;
	}

	public boolean isReported() {
		return reported;
	}

	public SecuritySupportedMode getSecuritySupportedMode() {
		return securitySupportedMode;
	}

	public void setSecuritySupportedMode(SecuritySupportedMode securitySupportedMode) {
		this.securitySupportedMode = securitySupportedMode;
	}
}
