package com.synapsense.plugin.wsn.iocore;

import java.io.IOException;
import java.io.InputStream;

/**
 * This class exists for the sole purpose of making the SerialPort input stream
 * reader -really- block on reads, and not just pretend to and read 0 bytes in
 * the process.
 * 
 * @author Yann
 * 
 */
public class SureReader {
	InputStream inputStream;

	public SureReader(InputStream i) {

		inputStream = i;

	}

	public void read(byte[] data) throws IOException {
		int readSoFar = 0;
		int retries = 10000;
		while (readSoFar < data.length) {
			if (inputStream == null)
				throw new IOException("Input stream is down");
			int read = inputStream.read(data, readSoFar, data.length - readSoFar);
			readSoFar += read;
			retries--;
			if (retries <= 0)
				throw new IOException("Sure reader ran out of retries to surely read.");
		}
	}

	public int read() throws IOException {
		return inputStream.read();
	}
}
