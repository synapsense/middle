package com.synapsense.plugin.wsn.storers;

import org.apache.log4j.Logger;

import com.synapsense.plugin.messages.WSNStatisticsBuilder;
import com.synapsense.plugin.messages.WSNStatisticsBuilder.FieldConstants;
import com.synapsense.plugin.wsn.SynapStore;
import com.synapsense.plugin.wsn.WsnConstants;
import com.synapsense.plugin.wsn.configuration.Configuration;
import com.synapsense.plugin.wsn.configuration.WSNNode;
import com.synapsense.plugin.wsn.msgbus.Message;
import com.synapsense.plugin.wsn.msgbus.MsgNwkStat;
import com.synapsense.plugin.wsn.util.SensorNetwork;
import com.synapsense.plugin.wsn.util.timesync.TimeSync;
import com.synapsense.plugin.wsn.util.timesync.TimeSyncTime;

public class StoreNwkStatistics {
	private static Logger log = Logger.getLogger(StoreNwkStatistics.class);

	public static void store(Message message, SensorNetwork net, TimeSync times) throws Exception {
		int networkId = net.getNetworkId();
		MsgNwkStat NWKStatMsg = (MsgNwkStat) message;

		WSNNode node = Configuration.getNode(net.getNetworkId(), NWKStatMsg.deviceId);

		if (node == null) {
			log.warn("Statistics message cannot be sent. Unknown node logical id: " + NWKStatMsg.deviceId);
			return;
		}

		TimeSyncTime timestamp = times.convertTime(NWKStatMsg.stattimestamp, times.systemTime());
		NWKStatMsg.stattimestamp = timestamp.getConvertedTime();

		WSNStatisticsBuilder bld = new WSNStatisticsBuilder();
		// TODO: Need some better than >= 0xF000
		if (NWKStatMsg.deviceId >= 0xF000) {
			bld.putData(FieldConstants.NETWORK_ID, networkId);
			bld.putData(FieldConstants.LOGICAL_ID, (int) NWKStatMsg.deviceId);
			bld.putData(FieldConstants.NWK_TIMESTAMP, NWKStatMsg.stattimestamp);
			bld.putData(FieldConstants.PACKETS_FROM_NODE, NWKStatMsg.tbpkcount);
			bld.putData(FieldConstants.DUPLICATE_PACKETS, NWKStatMsg.duppkcount);
			bld.putData(FieldConstants.OUT_OF_ORDER_PACKETS, NWKStatMsg.outoforderpkcount);
			bld.putData(FieldConstants.PACKETS_SENT, NWKStatMsg.pksentcount);
			bld.putData(FieldConstants.FREE_SLOT_COUNT, NWKStatMsg.freeslotcount);
			bld.putData(FieldConstants.WRONG_SLOT_COUNT, NWKStatMsg.wrongslotcount);
			bld.putData(FieldConstants.TOTAL_SLOT_COUNT, NWKStatMsg.totalslotcount);
			bld.putData(FieldConstants.PACKETS_RCVD_COUNT, NWKStatMsg.pkrcvdcount);
			bld.putData(FieldConstants.CRC_FAIL_PACKET_COUNT, NWKStatMsg.crcfailpkcount);
			bld.putData(FieldConstants.TIME_SYNCS_LOST_DRIFT_DETECTION, NWKStatMsg.timelostcountsync);
			bld.putData(FieldConstants.TIME_SYNCS_LOST_TIMEOUT, NWKStatMsg.timelostcounttimeout);
		} else {
			bld.putData(FieldConstants.NETWORK_ID, networkId);
			bld.putData(FieldConstants.LOGICAL_ID, (int) NWKStatMsg.deviceId);
			bld.putData(FieldConstants.NWK_TIMESTAMP, NWKStatMsg.stattimestamp);
			bld.putData(FieldConstants.SEND_FAILS_FULL_QUEUE, NWKStatMsg.sendfailSQ);
			bld.putData(FieldConstants.SEND_FAILS_NO_PATH, NWKStatMsg.sendfailNR);
			bld.putData(FieldConstants.NUMBER_ONE_HOP_RETRANSMISSION, NWKStatMsg.tbretrycount);
			bld.putData(FieldConstants.FORWARDING_PACKETS_DROPPED, NWKStatMsg.fwdpkdrop);
			bld.putData(FieldConstants.PARENT_FAIL_COUNT, NWKStatMsg.parentfailcount);
			bld.putData(FieldConstants.BROADCAST_RETRANSMISSION, NWKStatMsg.tsretrycount);
			bld.putData(FieldConstants.PACKETS_FROM_BASE_STATION, NWKStatMsg.tspkcount);
			bld.putData(FieldConstants.PACKETS_TO_BASE_STATION, NWKStatMsg.tbpkcount);
			bld.putData(FieldConstants.PACKETS_FORWARDED, NWKStatMsg.fwdpkcount);
			bld.putData(FieldConstants.PACKETS_SENT, NWKStatMsg.pksentcount);
			bld.putData(FieldConstants.RADIO_ONTIME, (int) NWKStatMsg.radiouptime);
			bld.putData(FieldConstants.FREE_SLOT_COUNT, NWKStatMsg.freeslotcount);
			bld.putData(FieldConstants.WRONG_SLOT_COUNT, NWKStatMsg.wrongslotcount);
			bld.putData(FieldConstants.TOTAL_SLOT_COUNT, NWKStatMsg.totalslotcount);
			bld.putData(FieldConstants.PACKETS_RCVD_COUNT, NWKStatMsg.pkrcvdcount);
			bld.putData(FieldConstants.CRC_FAIL_PACKET_COUNT, NWKStatMsg.crcfailpkcount);
			bld.putData(FieldConstants.TIME_SYNCS_LOST_DRIFT_DETECTION, NWKStatMsg.timelostcountsync);
			bld.putData(FieldConstants.TIME_SYNCS_LOST_TIMEOUT, NWKStatMsg.timelostcounttimeout);
			bld.putData(FieldConstants.NODE_LATENCY, (int) (NWKStatMsg.rcvdtimestamp - timestamp.getConvertedTime()));
			bld.putData(FieldConstants.HOP_COUNT, NWKStatMsg.hopcount);
			bld.putData(FieldConstants.ROUTER_STATE, NWKStatMsg.routerstate);
			bld.putData(FieldConstants.PARENT_STATE, NWKStatMsg.parentstate);
		}
		bld.putData(FieldConstants.NODE_ID, node.getPhysID());
		bld.putData(FieldConstants.CHANNEL_STATUS, NWKStatMsg.chstatus);
		bld.putData(FieldConstants.NOISE_THRESH, NWKStatMsg.noisethresh);
		SynapStore.transport.sendData(node.getId(), WsnConstants.NETWORK_STATISTICS, bld.getMessage());
	}
}
