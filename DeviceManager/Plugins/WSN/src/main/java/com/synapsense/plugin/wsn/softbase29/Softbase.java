package com.synapsense.plugin.wsn.softbase29;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.plugin.wsn.configuration.WSNGW;
import com.synapsense.plugin.wsn.iocore.frames.MoteUARTFrame;
import com.synapsense.plugin.wsn.iocore.frames.out.MoteUARTOutCommandFrame;
import com.synapsense.plugin.wsn.iocore.frames.out.MoteUARTOutSetParameterFrame;
import com.synapsense.plugin.wsn.iocore.n29.frames.MoteUARTBeaconSendFrame;
import com.synapsense.plugin.wsn.iocore.n29.frames.MoteUARTGlobalTimeFrame;
import com.synapsense.plugin.wsn.iocore.n29.frames.MoteUARTIsTimeSyncFrame;
import com.synapsense.plugin.wsn.iocore.n29.frames.MoteUARTNWKStatSoftFrame;
import com.synapsense.plugin.wsn.iocore.n29.frames.MoteUARTRcvdRadioFrame;
import com.synapsense.plugin.wsn.iocore.n29.frames.MoteUARTStatusFrame;
import com.synapsense.plugin.wsn.iocore.n29.frames.MoteUARTTimesyncFrame;
import com.synapsense.plugin.wsn.iocore.n29.frames.out.MoteUARTOutBaseCommandFrame;
import com.synapsense.plugin.wsn.iocore.n29.frames.out.MoteUARTOutNodeIDFrame;
import com.synapsense.plugin.wsn.msgbus.Message;
import com.synapsense.plugin.wsn.msgbus.MsgNwkStat;
import com.synapsense.plugin.wsn.msgbus.MsgP3ResendConfig;
import com.synapsense.plugin.wsn.msgbus.MsgResendAppInit;
import com.synapsense.plugin.wsn.msgbus.MsgTimesyncInformation;
import com.synapsense.plugin.wsn.servers.UpdateServerThread;
import com.synapsense.plugin.wsn.smota2.Firmware;
import com.synapsense.plugin.wsn.softbase29.packet.OSSBeaconPacket;
import com.synapsense.plugin.wsn.softbase29.packet.OSSDataPacket;
import com.synapsense.plugin.wsn.softbase29.packet.OSSPacket;
import com.synapsense.plugin.wsn.softbase29.service.AppInitialization;
import com.synapsense.plugin.wsn.softbase29.service.Initialization;
import com.synapsense.plugin.wsn.softbase29.service.LPTimeSync;
import com.synapsense.plugin.wsn.softbase29.service.NWKStat;
import com.synapsense.plugin.wsn.softbase29.service.NodeService;
import com.synapsense.plugin.wsn.softbase29.service.OSSServices;
import com.synapsense.plugin.wsn.softbase29.service.P3Receive;
import com.synapsense.plugin.wsn.softbase29.service.SMOTA2Service;
import com.synapsense.plugin.wsn.softbase29.service.SecurityService;
import com.synapsense.plugin.wsn.softbase29.service.SensorReceive;
import com.synapsense.plugin.wsn.softbase29.service.Service;
import com.synapsense.plugin.wsn.softbase29.state.NodeInfo;
import com.synapsense.plugin.wsn.softbase29.state.OSSNHT;
import com.synapsense.plugin.wsn.util.Constants;
import com.synapsense.plugin.wsn.util.SensorNetwork;
import com.synapsense.plugin.wsn.util.serializer.BigSerializer;
import com.synapsense.plugin.wsn.util.serializer.Serializer;

public class Softbase implements Runnable, com.synapsense.plugin.wsn.softbase.Softbase {
	private static final int RESETDELAY = 10 * 60 * 1000;

	private SensorNetwork snModel;
	private final int panid;

	private final BlockingQueue<MoteUARTFrame> inQ;

	private Map<String, GateWay> portmap = new HashMap<String, GateWay>();
	private BlockingQueue<Message> messageQ;
	private BlockingQueue<Message> commandQ;
	private static Log log = LogFactory.getLog(Softbase.class);

	private byte radioch = 0;

	private boolean startLPL = true;

	private int netid;

	private Map<Byte, Service> services = new HashMap<Byte, Service>();

	public final SoftbaseNWK nwk;
	public final LPTimeSync lptimesync;
	public final Initialization initialization;

	private final List<com.synapsense.plugin.wsn.softbase.GateWay> gateways = new ArrayList<com.synapsense.plugin.wsn.softbase.GateWay>();
	public final NodeInfo node_info;

	private long nwkstarttime = 0;
	private boolean LPLmode;
	private boolean Initdone;
	private boolean SMOTAmode;
	private Timer timer;
	private TimerTask resettimer_id;
	private final Object statuslock;

	private Collection<Thread> workers = new ArrayList<Thread>();

	private void addService(Service service) {
		services.put(service.getServiceID(), service);
	}

	public Service getService(byte id) {
		return services.get(id);
	}

	/**
	 * For mocking
	 */
	Softbase() {
		nwk = null;
		lptimesync = null;
		initialization = null;
		node_info = null;
		panid = 12;
		inQ = null;
		statuslock = new Object();
	}

	public Softbase(SensorNetwork snModel, BlockingQueue<Message> messageQ, BlockingQueue<Message> commandQ) {
		this.setModel(snModel);
		this.messageQ = messageQ;
		this.commandQ = commandQ;
		this.panid = snModel.getPanId();
		this.setNetId(snModel.getNetworkId());

		Collection<WSNGW> gws = snModel.getGateways();

		Initdone = false;
		SMOTAmode = false;
		timer = new Timer("Softbase Timer-" + netid);
		statuslock = new Object();

		inQ = new LinkedBlockingQueue<MoteUARTFrame>();
		LPLmode = false;

		for (WSNGW gw : gws) {
			try {
				GateWay gateway = new GateWay(gw, inQ, (char) gw.getLogicalID(), this);
				gateways.add(gateway);
				portmap.put(gateway.getPortname(), gateway);
			} catch (GWInstantiationException e) {
				log.warn("Cannot add gateway to GW pool", e);
			}
		}
		nwk = new SoftbaseNWK(this);
		node_info = new NodeInfo(this);

		lptimesync = new LPTimeSync(this);
		addService(lptimesync);
		initialization = new Initialization(this);
		addService(initialization);
		addService(new NodeService(this));
		addService(new NWKStat(this));
		addService(new SMOTA2Service(this));
		addService(new SensorReceive(this));
		addService(new P3Receive(this));
		addService(new AppInitialization(this));
		addService(new SecurityService(this));
	}

	public void addGateway(WSNGW gw) {
		addGateway(gw, (char) gw.getLogicalID());
	}

	public void addGateway(WSNGW gw, char logicalID) {
		try {
			GateWay gateway = new GateWay(gw, inQ, logicalID, this);
			synchronized (gateways) {
				gateways.add(gateway);

				portmap.put(gateway.getPortname(), gateway);

				gateway.start();

				// Start up this new gateway

				gatewaySendStartup(gateway);
				gatewaySendInitState(gateway);
			}
		} catch (GWInstantiationException e) {
			log.warn("Cannot add gateway to GW pool", e);
		}
	}

	public void removeGateway(String portName) {
		GateWay gateway = portmap.get(portName);
		synchronized (gateways) {
			if (gateway == null)
				return;

			MoteUARTOutBaseCommandFrame frame = new MoteUARTOutBaseCommandFrame(MoteUARTOutBaseCommandFrame.RESET);
			gateway.sendFrame(frame);
			gateway.setAlive(false);
			lptimesync.clearGateway(gateway);
			gateways.remove(gateway);
			portmap.remove(portName);
			gateway.stop();
		}
	}

	public void cancelStartLPL() {
		initialization.cancelStartLPL();
	}

	private void startService(Service service) {
		Runnable run = null;
		try {
			run = (Runnable) service;
		} catch (Exception e) {
			log.info("Service " + service.getClass().getName() + " is not runnable.");
			return;
		}
		if (run != null) {
			Thread thread = new Thread(run, service.getClass().getName() + "-" + Integer.toString(getNetId()));
			workers.add(thread);
			thread.start();
		} else {
			log.info("Service " + service.getClass().getName() + " is not runnable.");
		}

		return;
	}

	private void startAllServices() {
		for (Service s : services.values())
			startService(s);
	}

	public void run() {
		log.trace("SoftBase is Starting");
		Thread thread = new Thread(new MessageHandler(), "Message Handler-" + Integer.toString(getNetId()));
		workers.add(thread);
		thread.start();

		Iterator<com.synapsense.plugin.wsn.softbase.GateWay> iter = gateways.iterator();

		while (iter.hasNext()) {
			com.synapsense.plugin.wsn.softbase.GateWay gateway = iter.next();
			gateway.start();
		}
		gatewayInit();

		startAllServices();

		// Process Commands coming in from the network manager
		while (true) {
			try {
				Message message = commandQ.take();
				if (message instanceof MoteUARTOutNodeIDFrame) {
					initialization.sendnodeid((MoteUARTOutNodeIDFrame) message);
				} else if (message instanceof MoteUARTOutCommandFrame) {
					NodeService ns = (NodeService) getService(OSSServices.OSS_SERVICE_NODE_CONTROL);
					ns.send_command((MoteUARTOutCommandFrame) message);
				} else if (message instanceof MoteUARTOutSetParameterFrame) {
					NodeService ns = (NodeService) getService(OSSServices.OSS_SERVICE_NODE_CONTROL);
					ns.setparams((MoteUARTOutSetParameterFrame) message);
				} else if (message instanceof MsgP3ResendConfig) {
					MsgP3ResendConfig p3c = (MsgP3ResendConfig) message;
					P3Receive p3r = (P3Receive) services.get(OSSServices.OSS_SERVICE_P3);
					p3r.signalConfig(p3c.to, 0, false);
				} else if (message instanceof MsgResendAppInit) {
					MsgResendAppInit rap = (MsgResendAppInit) message;
					AppInitialization svc = (AppInitialization) services
					        .get(OSSServices.OSS_SERVICE_APP_INITIALIZATION);
					if (!svc.sendAppInit(rap.node, rap.cObj, message)) {
						message.onError(new Exception("App init didn't complete"));
					}
				} else {
					message.onError(new Exception("Unhandled message type"));
				}
			} catch (InterruptedException e) {
				break;
			} catch (Exception e) {
				log.error("Got exception while processing Command Message", e);
			}
		}
	}

	private byte getRadioChannel() {
		byte orch = radioch;
		radioch = (byte) ((radioch + 3) % 16);
		return orch;
	}

	private void gatewaySendStartup(GateWay gateway) {
		gateway.setRadioCh(getRadioChannel());
		gateway.sendStart(panid);
	}

	public void gatewaySendInitState(GateWay gateway) {
		if (!gateway.isAlive())
			return;
		byte[] status = new byte[11];
		if (LPLmode) {
			status[0] = 1;
		} else {
			status[0] = 0;
		}
		if (LPLmode && !SMOTAmode) {
			status[1] = (byte) OSSNHT.FASTEST_BEACON_RATE;
		} else {
			status[1] = (byte) OSSNHT.INIT_BEACON_RATE;
		}
		if (SMOTAmode)
			status[2] = 1;
		else
			status[2] = 0;
		status[3] = gateway.getRadioCh();
		Serializer serial = new BigSerializer(status, 4);
		serial.packU_INT16(gateway.getNodeId());
		if (getNwkStartTime() == 0)
			setNwkStartTime(System.currentTimeMillis() / 1000);
		serial.packU_INT32(getNwkStartTime());
		serial.packU_INT8((byte) (nwk.getToSensorsSeqno()));
		MoteUARTOutBaseCommandFrame frame = new MoteUARTOutBaseCommandFrame(MoteUARTOutBaseCommandFrame.SETINIT, status);
		if (!gateway.sendFrame(frame)) {
			log.error("Fail setting init state");
			gateway.setAlive(false);
		}
	}

	private void gatewayInit() {
		// Get current status of gateways

		Iterator<com.synapsense.plugin.wsn.softbase.GateWay> iter = gateways.iterator();
		while (iter.hasNext()) {
			GateWay gateway = (GateWay) iter.next();
			gatewaySendStartup(gateway);
		}
		// Wait status frame at most 5 sec.
		for (int i = 0; i < 5000; i = i + 100) {
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
			}
			boolean allset = true;
			synchronized (statuslock) {
				iter = gateways.iterator();
				while (iter.hasNext()) {
					GateWay gateway = (GateWay) iter.next();
					// Gateway become alive when status is set
					if (!gateway.isAlive()) {
						allset = false;
						break;
					}
				}
			}
			// Break when all gateway is alive
			if (allset)
				break;
		}

		if (LPLmode)
			lptimesync.setRate(OSSNHT.FASTEST_BEACON_RATE);
		else
			lptimesync.setRate(OSSNHT.INIT_BEACON_RATE);
		// Set current status of network to gateways
		iter = gateways.iterator();
		while (iter.hasNext()) {
			GateWay gateway = (GateWay) iter.next();
			gatewaySendInitState(gateway);
		}
		Initdone = true;
	}

	private void wrongGatewayInit() {
		Iterator<com.synapsense.plugin.wsn.softbase.GateWay> iter = gateways.iterator();
		while (iter.hasNext()) {
			GateWay gateway = (GateWay) iter.next();
			if (gateway.getNwkstarttime() != this.getNwkStartTime())
				gatewaySendInitState(gateway);
		}
	}

	public int getMACInterval() {
		if (LPLmode)
			return 1000;
		else
			return 30;
	}

	public boolean isStartLPL() {
		return startLPL;
	}

	public boolean isLPLmode() {
		return LPLmode;
	}

	public void setLPLmode(boolean lmode) {
		LPLmode = lmode;
	}

	public boolean isSMOTAmode() {
		return SMOTAmode;
	}

	public void setSMOTAmode(boolean amode) {
		SMOTAmode = amode;
	}

	public void postMessage(Message message) {
		try {
			boolean accepted = messageQ.offer(message);
			if (!accepted) {
				log.error("QUEUE FULL: The NetworkWorker queue is full. Incoming message lost.");
			}
		} catch (NullPointerException e) {
			log.error("QUEUE: Message to post is NULL.", e);
		}
	}

	public BlockingQueue<Message> getCommandQ() {
		return commandQ;
	}

	public List<com.synapsense.plugin.wsn.softbase.GateWay> getGateways() {
		synchronized (gateways) {
			return gateways;
		}
	}

	public void UpdateFirmware(Firmware firmware, UpdateServerThread updateserver) {
		SMOTA2Service smota = (SMOTA2Service) getService(OSSServices.OSS_SERVICE_SMOTA2);
		smota.startUpdate(firmware, updateserver);
	}

	public void restartNetwork() {
		setNwkStartTime(System.currentTimeMillis() / 1000);
		LPLmode = false;
		initialization.restartLPL();
		nwk.reset();
	}

	public void sendCommand(int command, char nodeid) {
		MoteUARTOutCommandFrame frame = null;
		switch (command) {
		case 1:
			if (nodeid == 0)
				return;
			else
				frame = new MoteUARTOutCommandFrame(nodeid, Constants.COMMAND_ID_IDENTIFY_NODE, 0);
			break;
		case 2:
			if (nodeid == 0) {
				frame = new MoteUARTOutCommandFrame(MoteUARTOutCommandFrame.ALL_NODES,
				        Constants.COMMAND_ID_RESET_NETWORK, 0);
				resettimer_id = new ResetTimer();
				timer.schedule(resettimer_id, RESETDELAY);
			} else
				frame = new MoteUARTOutCommandFrame(nodeid, Constants.COMMAND_ID_RESET_NODE, 0);
			break;
		default:
			return;
		}
		boolean offered = getCommandQ().offer(frame);
		if (!offered) {
			log.error("QUEUE FULL: Command queue did not accept frame. Dropping.");
		}
	}

	public SensorNetwork getModel() {
		return snModel;
	}

	/**
	 * Set the network model. Currently this is very dangerous to change at
	 * runtime, but will be enhanced in the future.
	 * 
	 * @param snModel
	 */
	private void setModel(SensorNetwork snModel) {
		this.snModel = snModel;
	}

	class ResetTimer extends TimerTask {
		public void run() {
			restartNetwork();
			Iterator<com.synapsense.plugin.wsn.softbase.GateWay> iter = getGateways().iterator();
			MoteUARTOutBaseCommandFrame frame = new MoteUARTOutBaseCommandFrame(MoteUARTOutBaseCommandFrame.RESET);
			while (iter.hasNext()) {
				GateWay gateway = (GateWay) iter.next();
				gateway.sendFrame(frame);
			}
		}
	}

	class MessageHandler implements Runnable {

		OSSDataPacket prevmessage;

		public void run() {

			boolean interrupted = false;
			while (!interrupted) {
				try {
					MoteUARTFrame message = inQ.take();
					GateWay gateway = portmap.get(message.portName);
					if (gateway == null)
						continue;
					MoteUARTRcvdRadioFrame radiomessage;
					boolean fromrealgateway = true;
					if (message.getType() == MoteUARTFrame.TYPE_RADIODONE) {
						// Check the packet is generated internally
						radiomessage = (MoteUARTRcvdRadioFrame) message;
						if (!radiomessage.recvdframe)
							fromrealgateway = false;
					}
					if (fromrealgateway) {
						if (!gateway.isAlive()) {
							// Try sending start message for dead gateway
							// sending something
							gateway.init();
							gateway.sendStart(panid);
							if (message.getType() != MoteUARTFrame.TYPE_CANISTART
							        && message.getType() != MoteUARTFrame.TYPE_STATUS
							        && message.getType() != MoteUARTFrame.TYPE_PRINT)
								continue;
						}
						gateway.delayPing();
					}

					switch (message.getType()) {

					case MoteUARTFrame.TYPE_RADIO:
						radiomessage = (MoteUARTRcvdRadioFrame) message;
						if (radiomessage.pkt == null)
							break;
						switch (radiomessage.pkt.mac_type) {
						case OSSPacket.MAC_DATA:
							if (prevmessage == null
							        || ((OSSDataPacket) radiomessage.pkt).getProtocol() == SoftbaseNWK.Protocol_ToSensors
							        || !prevmessage.equals(radiomessage.pkt)) {

								// ritu comment .. now the data is sent to the
								// nwk layer
								nwk.process((OSSDataPacket) radiomessage.pkt, gateway);
								prevmessage = (OSSDataPacket) radiomessage.pkt;
							} else {
								log.trace(gateway.getPortname() + " received duplicate packet");
							}
							break;
						case OSSPacket.MAC_BEACON:
						case OSSPacket.MAC_TIMEBEACON:
							gateway.processBeacon((OSSBeaconPacket) radiomessage.pkt);
							break;
						}
						break;

					case MoteUARTFrame.TYPE_BEACONSEND:
						MoteUARTBeaconSendFrame bframe = (MoteUARTBeaconSendFrame) message;
						gateway.beaconSend(bframe.hopcountTM);
						break;

					case MoteUARTFrame.TYPE_RADIODONE:
						log.trace(gateway.getPortname() + " is done sending");
						radiomessage = (MoteUARTRcvdRadioFrame) message;
						OSSDataPacket pk = gateway.sendDone(radiomessage);
						if (pk != null)
							nwk.senddone(pk, gateway);
						break;
					case MoteUARTFrame.TYPE_PRINT:
						log.info(message.portName + " PRINT " + message);
						break;

					case MoteUARTFrame.TYPE_TIMESYNC:
						lptimesync.TimeRcvd(gateway);
						MsgTimesyncInformation m = new MsgTimesyncInformation();
						m.globaltime = ((MoteUARTTimesyncFrame) message).globaltime;
						postMessage(m);
						break;

					case MoteUARTFrame.TYPE_GLOBALTIME:
						MoteUARTGlobalTimeFrame gframe = (MoteUARTGlobalTimeFrame) message;
						lptimesync.setGlobalTime(gframe.globaltime);
						break;

					case MoteUARTFrame.TYPE_STATUS:
						synchronized (statuslock) {
							gateway.setAlive(true);
							MoteUARTStatusFrame statusframe = (MoteUARTStatusFrame) message;
							log.info("Status received from " + gateway.getPortname() + " " + message.toString());
							// Process status frame only gateway is in the same
							// mode as softbase
							if (SMOTAmode == statusframe.SMOTAmode) {
								if (statusframe.timemaster)
									lptimesync.setMaster(gateway);
								if (statusframe.LPLmode) {
									LPLmode = true;
									initialization.LPLstarted();
									lptimesync.setRate(OSSNHT.FASTEST_BEACON_RATE);
								}
								gateway.setHopCountTM(statusframe.hoptm);
								gateway.setNwkstarttime(statusframe.nwkstarttime);
								if (statusframe.nwkstarttime > Softbase.this.getNwkStartTime()
								        && statusframe.nwkstarttime < (System.currentTimeMillis() / 1000)) {
									if (Softbase.this.getNwkStartTime() != 0 && !Softbase.this.Initdone) {
										wrongGatewayInit();
									}
									Softbase.this.setNwkStartTime(statusframe.nwkstarttime);
								}
								if ((byte) statusframe.ToSensorsSeqno - (byte) nwk.getToSensorsSeqno() > 0)
									nwk.setToSensorsSeqno(statusframe.ToSensorsSeqno);
								if (Softbase.this.Initdone)
									gatewaySendInitState(gateway);
							} else {
								// Make gateway to restart in the right mode
								if (Softbase.this.Initdone)
									gatewaySendInitState(gateway);
							}
						}
						break;

					case MoteUARTFrame.TYPE_ISTIMESYNC:
						MoteUARTIsTimeSyncFrame istimesyncframe = (MoteUARTIsTimeSyncFrame) message;
						gateway.setTimeSynced(istimesyncframe.issynced);
						break;

					case MoteUARTFrame.TYPE_CANISTART:
						log.trace("Can I start message from " + gateway.getPortname());
						// Flush the queue
						gateway.clearQueue();
						gateway.sendStart(panid);
						// Gateway got reset
						// Send the current network info
						gateway.init();
						if (lptimesync.getMaster() != null && lptimesync.getMaster().equals(gateway)) {
							lptimesync.pickMaster();
						}
						break;

					case MoteUARTFrame.TYPE_NWKSTATSOFT:
						MoteUARTNWKStatSoftFrame statsoft = (MoteUARTNWKStatSoftFrame) message;
						MsgNwkStat nwkstatframe = new MsgNwkStat();
						nwkstatframe.deviceId = gateway.getNodeId();
						nwkstatframe.tbpkcount = gateway.tbpkcount;
						nwkstatframe.duppkcount = gateway.duppkcount;
						nwkstatframe.outoforderpkcount = gateway.outoforderpkcount;
						nwkstatframe.pksentcount = statsoft.pksentcount;
						nwkstatframe.freeslotcount = statsoft.freeslotcount;
						nwkstatframe.wrongslotcount = statsoft.wrongslotcount;
						nwkstatframe.totalslotcount = statsoft.totalslotcount;
						nwkstatframe.pkrcvdcount = statsoft.pkrcvdcount;
						nwkstatframe.crcfailpkcount = statsoft.crcfailpkcount;
						nwkstatframe.chstatus = statsoft.chstatus;
						nwkstatframe.timelostcountsync = statsoft.timelostcountsync;
						nwkstatframe.timelostcounttimeout = statsoft.timelostcounttimeout;
						nwkstatframe.noisethresh = statsoft.noisethresh;
						postMessage(nwkstatframe);
						gateway.statReset();
						break;

					case MoteUARTFrame.TYPE_SEND:
						log.trace(message.portName + " SEND " + message);
						break;

					case MoteUARTFrame.TYPE_RECV:
						log.trace(message.portName + " RECV " + message);
						break;

					default:
						log.trace("Unprocessed message type: " + message.getType());
					}
				} catch (InterruptedException e) {
					interrupted = true;
				} catch (Exception e) {
					log.error("Got exception while processing MoteUARTFrame", e);
				}
			}
		}
	}

	@Override
	public void close() {
		for (com.synapsense.plugin.wsn.softbase.GateWay gw : new ArrayList<com.synapsense.plugin.wsn.softbase.GateWay>(
		        gateways)) {
			removeGateway(gw.getPortname());
		}
		for (Thread thread : workers) {
			thread.interrupt();
			try {
				thread.join(10000);
			} catch (InterruptedException e) {
				log.error("Softbase thread " + thread + " is not terminating.");
			}
		}
		for (Service s : services.values()) {
			s.close();
		}
		services.clear();
		workers.clear();
		nwk.close();
	}

	public long getNwkStartTime() {
		return nwkstarttime;
	}

	public void setNwkStartTime(long nwkstarttime) {
		this.nwkstarttime = nwkstarttime;
	}

	public int getNetId() {
		return netid;
	}

	public int getPanId() {
		return panid;
	}

	public void setNetId(int netid) {
		this.netid = netid;
	}
}
