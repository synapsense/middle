package com.synapsense.plugin.wsn.util.sensors;

public interface RangeConversionInterface {
	abstract public void setDataRanges(double min, double max);
}
