package com.synapsense.plugin.wsn.util.sensors;

/**
 * Sensor conversion interface - can use some more work
 * 
 * @author Yann
 * 
 */
public interface SensorConversionInterface {

	/**
	 * Returns a converted value for the raw sensor reading.
	 * 
	 * @param raw
	 *            Raw data (usualy from an ADC)
	 * @return Converted value
	 */
	abstract public double convertFromRaw(char raw);

	/**
	 * Much like \ref converFromRaw, but allows for compensation of the value
	 * based on a previously converted value (such as temperature).
	 * 
	 * @param raw
	 *            Raw reading to be converted
	 * @param other
	 *            Value to compensate by (already converted)
	 * @return Converted value
	 */
	abstract public double convertFromRawCompensated(char raw, double other);

	abstract public int getMoteDataType();

}
