package com.synapsense.plugin.wsn.configuration;

import com.synapsense.util.algo.Pair;

/**
 * The node identifier with short form addressing. Captures both the network and
 * locial IDs of each node.
 */
public class NodeID extends Pair<Integer, Integer> {

	private static final long serialVersionUID = -1725865158208892030L;

	public NodeID(int networkID, int nodeLogicalID) {
		super(networkID, nodeLogicalID);
	}

	public Integer getNetworkID() {
		return first;
	}

	public Integer getLogicalID() {
		return second;
	}
}
