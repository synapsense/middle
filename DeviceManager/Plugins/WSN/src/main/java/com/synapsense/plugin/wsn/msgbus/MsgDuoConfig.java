package com.synapsense.plugin.wsn.msgbus;

import com.synapsense.plugin.wsn.p3.DuoConfiguration;

public class MsgDuoConfig extends Message {

	public DuoConfiguration config;
	public char deviceId;

	public MsgDuoConfig(char device, DuoConfiguration config) {
		super();
		this.config = config;
		this.deviceId = device;
	}

}
