package com.synapsense.plugin.wsn.softbase29.service;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.plugin.wsn.msgbus.MsgNwkStat;
import com.synapsense.plugin.wsn.softbase29.Softbase;
import com.synapsense.plugin.wsn.softbase29.packet.RadioMsg;
import com.synapsense.plugin.wsn.util.serializer.LittleSerializer;
import com.synapsense.plugin.wsn.util.serializer.Serializer;

public class NWKStat implements Runnable, Service {
	private Softbase softbase;
	private static Log log = LogFactory.getLog(NWKStat.class);
	private volatile boolean interrupted = false;

	public NWKStat(Softbase softbase) {
		this.softbase = softbase;
	}

	public void close() {

	}

	public void run() {
		while (!interrupted) {
			RadioMsg msg;
			try {
				msg = softbase.nwk.receive(OSSServices.OSS_SERVICE_NWKSTAT);
			} catch (InterruptedException e1) {
				interrupted = true;
				continue;
			}
			try {
				Serializer serial = new LittleSerializer(msg.data);
				MsgNwkStat frame = new MsgNwkStat();
				frame.rcvdtimestamp = System.currentTimeMillis();
				frame.deviceId = msg.sourceid;
				frame.type = serial.unpackU_INT8();
				// ritu changes .. for PowRouter
				// frame.hopcount = serial.unpackU_INT8(); //dummy
				frame.routerst_parentst_hopcnt = serial.unpackU_INT8();
				frame.hopcount = frame.routerst_parentst_hopcnt & 0x1F;
				frame.routerstate = (frame.routerst_parentst_hopcnt & 0xC0) >> 6;
				frame.parentstate = (frame.routerst_parentst_hopcnt & 0x20) >> 5;

				frame.stattimestamp = serial.unpackU_INT32();
				frame.sendfailSQ = serial.unpackU_INT16();
				frame.sendfailNR = serial.unpackU_INT16();
				frame.tbretrycount = serial.unpackU_INT16();
				frame.fwdpkdrop = serial.unpackU_INT16();
				frame.parentfailcount = serial.unpackU_INT16();
				frame.tsretrycount = serial.unpackU_INT16();
				frame.tspkcount = serial.unpackU_INT16();
				frame.tbpkcount = serial.unpackU_INT16();
				frame.fwdpkcount = serial.unpackU_INT16();
				frame.pksentcount = serial.unpackU_INT16();
				frame.radiouptime = (long) (serial.unpackU_INT32() / 30.517578125);
				frame.freeslotcount = serial.unpackU_INT16();
				frame.wrongslotcount = serial.unpackU_INT16();
				frame.totalslotcount = serial.unpackU_INT16();
				frame.pkrcvdcount = serial.unpackU_INT16();
				frame.crcfailpkcount = serial.unpackU_INT16();
				for (int i = 0; i < 16; i++) {
					frame.chstatus[i] = serial.unpackU_INT8();
					if (frame.chstatus[i] == 0xFF)
						frame.chstatus[i] = -1;
				}
				frame.timelostcountsync = serial.unpackU_INT8();
				frame.timelostcounttimeout = serial.unpackU_INT8();
				frame.noisethresh = serial.unpack_INT8();
				softbase.postMessage(frame);

			} catch (ArrayIndexOutOfBoundsException e) {
				log.error("Malformed Network Statistic from " + Integer.toHexString(msg.sourceid));
			}
		}
	}

	public void interrupt() {
		interrupted = true;
	}

	public byte getServiceID() {
		return OSSServices.OSS_SERVICE_NWKSTAT;
	}

}
