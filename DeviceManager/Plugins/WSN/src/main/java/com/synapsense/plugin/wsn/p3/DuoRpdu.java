package com.synapsense.plugin.wsn.p3;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;

public class DuoRpdu implements Comparable<DuoRpdu> {

	public final String getSysObjectId() {
		return sysObjectId;
	}

	public final boolean isHasPerPhaseCurrent() {
		return hasPerPhaseCurrent;
	}

	public final boolean isHasPerPhaseVN() {
		return hasPerPhaseVN;
	}

	public final boolean isHasPerPhasePP() {
		return hasPerPhasePP;
	}

	public final boolean isHasPerPhasePower() {
		return hasPerPhasePower;
	}

	public final boolean isDelta() {
		return isDelta;
	}

	public final boolean isSinglePhase() {
		return isSinglePhase;
	}

	public final int getNumberOfOutlets() {
		return numberOfOutlets;
	}

	public final int getId() {
		return id;
	}

	public DuoRpdu(int id, byte[] sysobjid, int outlets, int flags) throws UnsupportedEncodingException {
		this.id = id;
		this.rawflags = flags;
		this.rawSysObjectId = sysobjid;
		this.numberOfOutlets = outlets;

		this.sysObjectId = new String(sysobjid, "UTF-8");

		hasPerPhaseCurrent = ((flags & 1) != 0);
		hasPerPhaseVN = (((flags >> 1) & 1) != 0);
		hasPerPhasePP = (((flags >> 2) & 1) != 0); // per phase leg-leg voltage
		hasPerPhasePower = (((flags >> 3) & 1) != 0);  // per phase power
		isDelta = (((flags >> 4) & 1) != 0);
		isSinglePhase = (((flags >> 5) & 1) != 0);
	
	}

	/* ID = 0 for left RPDU, ID = 1 for right, mimicing SandCrawler */
	private int id;
	int rawflags;
	byte[] rawSysObjectId;

	private String sysObjectId;
	private boolean hasPerPhaseCurrent;
	private boolean hasPerPhaseVN;
	private boolean hasPerPhasePP;
	private boolean hasPerPhasePower;
	private boolean isDelta;
	private boolean isSinglePhase;

	private int numberOfOutlets;

	@Override
	public int compareTo(DuoRpdu rhs) {

		return this.id - rhs.id;
	}

	@Override
	public String toString() {
		return "DuoRpdu [id=" + id + ", rawflags=" + rawflags + ", rawSysObjectId=" + Arrays.toString(rawSysObjectId)
		        + ", sysObjectId=" + sysObjectId + ", hasPerPhaseCurrent=" + hasPerPhaseCurrent + ", hasPerPhaseVN="
		        + hasPerPhaseVN + ", hasPerPhasePP=" + hasPerPhasePP + ", hasPerPhasePower=" + hasPerPhasePower
		        + ", isDelta=" + isDelta  + ", isSinglePhase="+ isSinglePhase+", numberOfOutlets=" + numberOfOutlets + "]";
	}
}
