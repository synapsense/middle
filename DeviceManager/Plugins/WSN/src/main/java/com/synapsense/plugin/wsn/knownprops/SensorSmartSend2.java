package com.synapsense.plugin.wsn.knownprops;

import org.apache.log4j.Logger;

import com.synapsense.plugin.wsn.NetworkWorker;
import com.synapsense.plugin.wsn.WsnConstants;
import com.synapsense.plugin.wsn.configuration.Configuration;
import com.synapsense.plugin.wsn.configuration.WSNNode;
import com.synapsense.plugin.wsn.configuration.WSNSensor;
import com.synapsense.plugin.wsn.msgbus.Message;
import com.synapsense.plugin.wsn.msgbus.MsgResendAppInit;

class SensorSmartSend2 extends NodePropertyWorker {
	private final static Logger logger = Logger.getLogger(SensorSmartSend2.class);

	@Override
	protected boolean performAction(String objectID, String propertyName, Object value) {
		WSNSensor sen = Configuration.getSensorByObjectID(objectID);
		if (sen == null)
			return false;
		WSNNode node = sen.getNode();
		if (node == null)
			return false;
		NetworkWorker netWorker = locateNetworkWorker(node.getNetworkID());
		if (netWorker == null) {
			logger.warn("Unknown networkID:" + node.getNetworkID() + " for node physID:"
			        + Long.toHexString(node.getPhysID()));
			return false;
		}
		logger.info("Sending sensor smart send values for node with physID: " + Long.toHexString(node.getPhysID()));

		logger.trace("Asking for appinit resend for " + Long.toHexString(node.getPhysID()));
		Message msg = new MsgResendAppInit(node);
		netWorker.commandQ.add(msg);
		return true;
	}

	@Override
	protected boolean checkItsMine(String propertyName) {
		return propertyName.equals(WsnConstants.SENSOR_CRITICAL_POINT)
		        || propertyName.equals(WsnConstants.SENSOR_DELTA_T_LOW)
		        || propertyName.equals(WsnConstants.SENSOR_DELTA_T_HIGH)
		        || propertyName.equals(WsnConstants.SENSOR_SUBSAMPLE_INTERVAL);
	}
}
