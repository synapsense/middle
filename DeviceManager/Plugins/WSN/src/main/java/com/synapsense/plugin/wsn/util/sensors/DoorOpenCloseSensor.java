package com.synapsense.plugin.wsn.util.sensors;

public class DoorOpenCloseSensor implements SensorConversionInterface {

	static final int DOOR_CLOSE_THRESHOLD = 100;

	public double convertFromRaw(char raw) {

		// 0V when the door is closed

		return (raw > DOOR_CLOSE_THRESHOLD) ? 1 : 0;
	}

	public double convertFromRawCompensated(char raw, double other) {
		return convertFromRaw(raw);

	}

	public int getMoteDataType() {
		return 9;
	}

}
