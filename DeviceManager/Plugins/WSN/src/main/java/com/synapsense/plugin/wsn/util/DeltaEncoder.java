package com.synapsense.plugin.wsn.util;

import org.apache.log4j.Logger;

import com.synapsense.plugin.wsn.WsnConstants;
import com.synapsense.plugin.wsn.configuration.WSNNode;
import com.synapsense.plugin.wsn.configuration.WSNSensor;

/**
 * Disclaimer: This has nothing to do with delta encoding in the traditional
 * sense. This simply produces the "delta" field on a WSN node for N29 support.
 * 
 * @author yann
 * 
 */

public class DeltaEncoder {

	private static Logger log = Logger.getLogger(DeltaEncoder.class);

	private static Integer setSmartSend(WSNSensor sensor) {
		String ssT = sensor.getOption(WsnConstants.SENSOR_SMART_SEND_THRESHOLD);
		log.info("Using SST of " + ssT);
		if (ssT != null) {
			// We allow truncation of the smartsend value - so 5.0 becomes 5,
			// 5.1 becomes 5, etc
			double val = Double.parseDouble(ssT);
			int ival = (int) val;
			return Integer.valueOf(ival);
		} else {
			return null;
		}
	}

	public static Integer encode(WSNNode node, WSNSensor sensor) {
		try {

			int delta = 0;
			log.info("Delta node platform is " + node.getPlatformID());

			if (node.getPlatformID() == WSNNode.PLATFORM_THERMAL || node.getPlatformID() == WSNNode.PLATFORM_THERMAL2) {
				/* Thermanode 1 & 2 Code Path */
				return setSmartSend(sensor);
			}

			/* Only set smartsend on thermistors when encountering Duo/SC */
			/* Duo/SC door sensors are handled below */
			if (node.getPlatformID() == WSNNode.PLATFORM_DUO || node.getPlatformID() == WSNNode.PLATFORM_SANDCRAWLER) {
				if (sensor.getType() == 27 || sensor.getType() == 29) {
					return setSmartSend(sensor);
				}
			}

			String enS = sensor.getOption(WsnConstants.SENSOR_ENABLED);
			log.info("Sensor enabled: " + enS);
			int enI = Integer.parseInt(enS);
			if (enS != null && enI != 0)
				delta = 0x8000;

			String modeS = sensor.getOption(WsnConstants.SENSOR_MODE);
			log.info("Sensor mode: " + modeS);
			if (modeS != null && (modeS.equals("state_change_count") || modeS.equals("minmax"))) {
				String intervalS = sensor.getOption(WsnConstants.SENSOR_INTERVAL);
				int interval = Integer.parseInt(intervalS);
				delta |= interval;
			}

			return delta;

		} catch (Exception e) {
			log.error("Couldn't encode delta due to exception. ", e);
			return null;
		}

	}

}
