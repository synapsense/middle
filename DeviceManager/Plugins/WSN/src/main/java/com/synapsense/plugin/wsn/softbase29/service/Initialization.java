package com.synapsense.plugin.wsn.softbase29.service;

import java.util.Iterator;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.plugin.wsn.iocore.n29.frames.out.MoteUARTOutBaseCommandFrame;
import com.synapsense.plugin.wsn.iocore.n29.frames.out.MoteUARTOutNodeIDFrame;
import com.synapsense.plugin.wsn.msgbus.MsgNHTInfo;
import com.synapsense.plugin.wsn.msgbus.MsgNodeInfo;
import com.synapsense.plugin.wsn.softbase29.GateWay;
import com.synapsense.plugin.wsn.softbase29.SendError;
import com.synapsense.plugin.wsn.softbase29.Softbase;
import com.synapsense.plugin.wsn.softbase29.SoftbaseNWK;
import com.synapsense.plugin.wsn.softbase29.packet.RadioMsg;
import com.synapsense.plugin.wsn.softbase29.state.OSSNHT;
import com.synapsense.plugin.wsn.softbase29.state.SecuritySupportedMode;
import com.synapsense.plugin.wsn.util.sensors.Sensor;
import com.synapsense.plugin.wsn.util.serializer.LittleSerializer;
import com.synapsense.plugin.wsn.util.serializer.Serializer;

public class Initialization implements Runnable, Service {
	public static final byte INIT_PKT_METAINFO = 1;
	public static final byte INIT_PKT_NHTINFO = 2;
	public static final byte INIT_PKT_LPLINFO = 4;
	public static final byte INIT_PKT_TIMESTABLE = 6;
	public static final byte INIT_PKT_NODEIDINFO = 7;
	public static final byte INIT_PKT_NODEIDINFOANNOUNCE = 8;

	private static final int NodeIDInfo_SIZE = 38;
	private static final int LPL_INFO_SIZE = 6;

	// Wait for 10 min. from last node join before going LPL
	public static final int NODE_WAIT_TIME = 10 * 60 * 1000;
	public static final long LPL_DELAY = 30000L;
	public static final long NWK_RETRY_TIME = 5000;
	public static final int MAX_APPLIDATA_SIZE = 100;
	public static final long GATEWAY_TIMESYNCWAIT = 30000;
	// After sending this many node id, the network goes in LPL mode
	private static final int MAX_NODEIDSENT_BEFORE_LPL = 200;

	private byte[] LPL_info_pkt;
	private boolean sentLPL;
	private boolean cancelLPL;
	private TimerTask SendLPL_Msg_timer_id;
	private long LPL_StartTime;
	private long lastnodejointime;
	private int nodeidsentcount;

	private Timer timer;
	private Softbase softbase;
	private static Log log = LogFactory.getLog(Initialization.class);

	private volatile boolean interrupted = false;

	public Initialization(Softbase softbase) {
		this.softbase = softbase;
		timer = new Timer("Initialization Timer-" + Integer.toString(softbase.getNetId()), true);
		LPL_info_pkt = new byte[LPL_INFO_SIZE];
		sentLPL = false;
		lastnodejointime = 0;
		nodeidsentcount = 0;
	}

	public void close() {
		if (timer != null)
			try {
				timer.cancel();
			} catch (Exception e) {

			}
		return;

	}

	public void cancelStartLPL() {
		try {
			timer.cancel();
		} catch (Exception e) {

		}
		cancelLPL = true;
	}

	public void run() {
		if (!sentLPL && !cancelLPL) {
			SendLPL_Msg_timer_id = new SendLPL_Msg_h();
			timer.schedule(SendLPL_Msg_timer_id, NODE_WAIT_TIME);
		}
		RadioMsg msg;
		while (!interrupted) {
			try {
				msg = softbase.nwk.receive(OSSServices.OSS_SERVICE_INITIALIZATION);
			} catch (InterruptedException e1) {
				interrupted = true;
				continue;
			}
			try {
				switch (msg.data[0]) {
				case INIT_PKT_METAINFO:
					process_MetaInfo(msg.data, msg.sourceid, msg.gateway);
					break;
				case INIT_PKT_NHTINFO:
					process_NHTInfo(msg.data, msg.sourceid);
					break;
				case INIT_PKT_LPLINFO:
				case INIT_PKT_NODEIDINFOANNOUNCE:
					// Do nothing
					break;
				default:
					log.error("Unknown initialization packet received. Reports type " + (int) msg.data[0]);
				}
			} catch (ArrayIndexOutOfBoundsException e) {
				log.error("Malformed Initialization Message from " + Integer.toHexString(msg.sourceid));
			}
		}
	}

	public void restartLPL() {
		sentLPL = false;
		lastnodejointime = 0;
		nodeidsentcount = 0;
		SendLPL_Msg_timer_id = new SendLPL_Msg_h();
		timer.schedule(SendLPL_Msg_timer_id, NODE_WAIT_TIME);
	}

	private void process_NHTInfo(byte[] data, char sourceid) {
		MsgNHTInfo frame = new MsgNHTInfo();
		frame.deviceId = sourceid;
		Serializer serial = new LittleSerializer(data, 1);
		frame.numneigh = serial.unpackU_INT8();
		frame.report_timestamp = serial.unpackU_INT32();
		for (int i = 0; i < frame.numneigh; i++) {
			frame.nids[i] = serial.unpackU_INT16();
			frame.rssi[i][0] = serial.unpack_INT8();
			frame.rssi[i][1] = serial.unpack_INT8();
			frame.rssi[i][2] = serial.unpack_INT8();
			frame.rssi[i][3] = serial.unpack_INT8();
			int statehopinfo = serial.unpackU_INT16();
			frame.state[i] = (statehopinfo >> 11) & 0x7;
			frame.hop[i] = (statehopinfo >> 6) & 0x1F;
			frame.hoptm[i] = statehopinfo & 0x3F;
		}
		softbase.postMessage(frame);
	}

	private void process_MetaInfo(byte[] data, char sourceid, GateWay gateway) {
		log.info("RECVD META INFO");
		MsgNodeInfo frame = new MsgNodeInfo();
		Serializer serial = new LittleSerializer(data, 1);
		frame.platformId = (byte) serial.unpackU_INT8();
		frame.device = serial.unpack_INT64();
		frame.nodeType = 0;
		long app_ver = serial.unpackU_INT32();
		frame.appVersion = (int) (app_ver >> 24);
		frame.appRevision = (int) (app_ver & 0x00FFFFFF);
		long net_ver = serial.unpackU_INT32();
		frame.nwkVersion = (int) (net_ver >> 24);
		frame.nwkRevision = (int) (net_ver & 0x00FFFFFF);
		long os_ver = serial.unpackU_INT32();
		frame.osVersion = (int) (os_ver >> 24);
		frame.osRevision = (int) (os_ver & 0x00FFFFFF);
		frame.sensorCount = (byte) serial.unpackU_INT8();

		frame.noisethreshold = serial.unpack_INT8();
		if ((frame.sensorCount & (byte) 0x80) != 0) {
			// Non-sensor information
			// Reserved for future implementation :-)
		} else {
			for (int i = 0; i < frame.sensorCount; i++) {
				int channel = serial.unpackU_INT8();
				serial.unpackU_INT8(); // read dummy byte
				int type = serial.unpackU_INT16();
				frame.sensors.add(new Sensor(channel, type, 0));
			}
		}
		frame.source = sourceid;
		frame.gateway = gateway;
		softbase.postMessage(frame);
		// Notify the security service about the join to set an expected
		// security supported state

	}

	public void sendnodeid(MoteUARTOutNodeIDFrame frame) {
		if (frame.id != 0xFFFF) {
			if (SendLPL_Msg_timer_id != null)
				try {
					SendLPL_Msg_timer_id.cancel();
				} catch (IllegalStateException e) {
					log.error("Can't send node ID", e);
				}

			softbase.node_info.addNode(frame.id);
			softbase.node_info.reset(frame.id);

			if (frame.secEnable) {
				softbase.node_info.getById(frame.id).setSecuritySupportedMode(SecuritySupportedMode.SUPPORTED);
			}

			lastnodejointime = System.currentTimeMillis();
			nodeidsentcount++;

			if (!sentLPL && !cancelLPL) {
				SendLPL_Msg_timer_id = new SendLPL_Msg_h();
				if (nodeidsentcount > MAX_NODEIDSENT_BEFORE_LPL)
					SendLPL_Msg_timer_id.run();
				else
					timer.schedule(SendLPL_Msg_timer_id, NODE_WAIT_TIME);
			}
		}

		byte[] NodeIDInfo_pkt = new byte[NodeIDInfo_SIZE];
		Serializer serial = new LittleSerializer(NodeIDInfo_pkt);

		if (frame.source == 0xFFFF) {
			serial.packU_INT8(INIT_PKT_NODEIDINFOANNOUNCE);
			softbase.node_info.setParentGW(frame.id, frame.gateway);
		} else {
			serial.packU_INT8(INIT_PKT_NODEIDINFO);
			softbase.node_info.setParent(frame.id, frame.source);
		}

		serial.packU_INT8((byte) 0); // dummy byte
		serial.pack_INT64(frame.destination);
		serial.packU_INT16(frame.id);
		serial.packU_INT32(frame.sampleInterval);
		serial.packU_INT16((char) frame.subsamples);
		for (int i = 0; i < 10; i++)
			serial.packU_INT16((char) frame.delta[i]);

		if (frame.source == 0xFFFF) {
			softbase.nwk.send(null, NodeIDInfo_pkt, (char) 0, OSSServices.OSS_SERVICE_INITIALIZATION,
			        SoftbaseNWK.Protocol_Simple_BCast, frame.gateway);
		} else {
			try {
				softbase.nwk.send(null, NodeIDInfo_pkt, frame.source, OSSServices.OSS_SERVICE_INITIALIZATION,
				        SoftbaseNWK.Protocol_ToOneSensor);
			} catch (SendError e) {
				log.error("Packet send error: ", e);
			}
		}
	}

	public void LPLstarted() {
		sentLPL = true;
	}

	public void setlastnodejointime(long time) {
		lastnodejointime = time;
	}

	public long getlastnodejointime() {
		return lastnodejointime;
	}

	class SendLPL_Msg_h extends TimerTask {
		public void run() {
			if (sentLPL)
				return;
			sentLPL = true;
			softbase.node_info.print();
			LPL_StartTime = softbase.lptimesync.getGlobalTime() + (LPL_DELAY << 5);

			Serializer serial = new LittleSerializer(LPL_info_pkt);
			serial.packU_INT8(INIT_PKT_LPLINFO);
			serial.packU_INT8((byte) 0); // Dummy byte
			serial.packU_INT32(LPL_StartTime);

			try {
				softbase.nwk.send(null, LPL_info_pkt, (char) 0, OSSServices.OSS_SERVICE_INITIALIZATION,
				        SoftbaseNWK.Protocol_ToSensors);
				MoteUARTOutBaseCommandFrame frame = new MoteUARTOutBaseCommandFrame(
				        MoteUARTOutBaseCommandFrame.SETLPLMODEDELAY, (int) LPL_StartTime);
				Iterator<com.synapsense.plugin.wsn.softbase.GateWay> iter = softbase.getGateways().iterator();
				softbase.lptimesync.setRate(OSSNHT.FASTEST_BEACON_RATE);
				while (iter.hasNext()) {
					GateWay gw = (GateWay) iter.next();
					if (!gw.sendFrame(frame)) {
						log.error("Setting LPL mode fail on " + gw.getPortname());
						gw.setAlive(false);
					}
				}
				// log.trace("SEND BASE STATION META");

			} catch (SendError e) {
				log.error("SINK UNABLE TO SEND RETRY");
				timer.schedule(SendLPL_Msg_timer_id, NWK_RETRY_TIME);
			}

			try {
				Thread.sleep(LPL_DELAY);
			} catch (InterruptedException e) {
				log.error("Interrupted while waiting for initialization to complete", e);
				return;
			}
			softbase.setLPLmode(true);
		}
	}

	public byte getServiceID() {
		return OSSServices.OSS_SERVICE_INITIALIZATION;
	}

}
