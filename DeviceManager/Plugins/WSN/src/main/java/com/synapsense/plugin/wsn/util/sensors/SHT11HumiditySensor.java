package com.synapsense.plugin.wsn.util.sensors;

import com.synapsense.plugin.wsn.util.SensorStates;

public class SHT11HumiditySensor implements SensorConversionInterface {

	public double convertFromRaw(char raw) {
		double hum;

		hum = (double) raw * (double) raw * (-0.0000028) + 0.0405 * (double) raw + -4;
		if (hum >= 100) {
			hum = SensorStates.SENSOR_SERVICE;
		}
		if (hum < 0.1) {
			hum = SensorStates.SENSOR_SERVICE;
		}

		return hum;

	}

	public double convertFromRawCompensated(char raw, double tempF) {
		double hum;
		double temp = (tempF - 32.0) * 5.0 / 9.0;
		hum = (double) raw * (double) raw * (-0.0000028) + 0.0405 * (double) raw + -4;
		hum = (temp - 25) * (0.01 + 0.00008 * (double) raw) + hum; // compensated
		if (hum >= 100) {
			hum = SensorStates.SENSOR_SERVICE;
		}
		if (hum < 0.1) {
			hum = SensorStates.SENSOR_SERVICE;
		}

		return hum;

	}

	public int getMoteDataType() {
		return 2;
	}
}
