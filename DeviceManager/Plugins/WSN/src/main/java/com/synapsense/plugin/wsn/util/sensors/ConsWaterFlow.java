package com.synapsense.plugin.wsn.util.sensors;

import com.synapsense.plugin.wsn.util.SensorStates;

public class ConsWaterFlow implements SensorConversionInterface, RangeConversionInterface {

	private double min = 0;
	private double max = 100;

	public double convertFromRaw(char raw) {
		// voltage is measured in 12bits ADCCounts
		// 0 -> 0V ; 4095 -> 2.048V
		double voltage = ((double) raw / 4095) * 2.048;

		// 4ma = min gpm
		// 20ma = max gpm
		if (voltage < 0.1)
			return SensorStates.SENSOR_DISCONNECT; // to indicate that sensor is
			                                       // disconnected

		// 0 = min gpm
		// 16 = max gpm
		double count = (voltage - 0.4);

		count = count * ((max - min) / (1.60 - 0.0));
		return (count > 0) ? count : 0;
	}

	public double convertFromRawCompensated(char raw, double other) {
		return convertFromRaw(raw);

	}

	public int getMoteDataType() {

		return 31;
	}

	public void setDataRanges(double m, double x) {
		min = m;
		max = x;
	}

}
