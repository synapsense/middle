package com.synapsense.plugin.wsn.util.sensors;

import com.synapsense.plugin.wsn.util.SensorStates;

public class AirFlowSensor implements SensorConversionInterface {

	public double convertFromRaw(char raw) {

		// voltage is measured in 12bits ADCCounts
		// 0 -> 0V
		// 4095 -> 2.5V
		double volts = (double) raw / 4095 * 2.5;

		if (volts < 0.3)
			return SensorStates.SENSOR_DISCONNECT; // to indicate sensor
			                                       // disconnected status

		// Calculate airflow value in sccm
		// 0.05V -> 0 flow
		// 2.5V -> 30 sccm (max)
		double value = (volts - 0.5) * 15;

		return (value > 0) ? value : 0;
	}

	public double convertFromRawCompensated(char raw, double other) {
		return convertFromRaw(raw);
	}

	public int getMoteDataType() {
		return 10;
	}

}
