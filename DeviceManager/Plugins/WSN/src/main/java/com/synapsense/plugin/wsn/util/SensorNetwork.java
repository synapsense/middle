package com.synapsense.plugin.wsn.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.StringTokenizer;

import com.synapsense.plugin.apinew.ObjectDesc;
import com.synapsense.plugin.wsn.configuration.WSNGW;

public class SensorNetwork {

	public final static String NETWORK_NAME_PREAMBLE = "SynapSense Network #";
	private String network_name = null;

	// private String comPortString = null;
	private Collection<WSNGW> gws;

	private String apiId;

	private int networkId = -1;
	private int panId = -1;
	private String version = "1.1";
	private int sampleInterval = 15;
	private int sampleIntervalUnits = Constants.SAMPLE_FREQUENCY_MINUTES;
	private String baseStationHostName = null;
	private int baseStationPort = Constants.DEFAULT_PORT_FOR_CONSOLE_TO_BASE;

	private byte[] publicKey;
	private byte[] encipheredPrivateKey;

	@Override
	public int hashCode() {
		final int PRIME = 31;
		int result = 1;
		result = PRIME * result + networkId;
		return result;
	}

	public String getId() {
		return apiId;
	}

	public void setId(String id) {
		apiId = id;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final SensorNetwork other = (SensorNetwork) obj;
		if (networkId != other.networkId)
			return false;
		return true;
	}

	public SensorNetwork(int networkId) {
		super();
		this.networkId = networkId;
		this.network_name = retrieveNetworkName(networkId);
		this.gws = new ArrayList<WSNGW>();

	}

	/**
	 * @param comPort
	 * @param networkId
	 */
	public SensorNetwork(Collection<ObjectDesc> gws, int networkId) {
		super();
		this.networkId = networkId;
		this.network_name = retrieveNetworkName(networkId);
		this.gws = new ArrayList<WSNGW>();
		for (ObjectDesc gateway : gws) {
			WSNGW newGateway = new WSNGW(networkId, gateway);
			this.gws.add(newGateway);
		}
	}

	public SensorNetwork(String portnames, int networkId) {
		this.networkId = networkId;
		this.network_name = retrieveNetworkName(networkId);
		this.gws = new ArrayList<WSNGW>();

		StringTokenizer ports = new StringTokenizer(portnames, " \t,");
		char nodeid = 0xF000;
		while (ports.hasMoreTokens()) {
			String portname = ports.nextToken();
			WSNGW gw = new WSNGW("", networkId, nodeid, nodeid, portname);
			gws.add(gw);
			nodeid++;
		}
	}

	/**
	 * @param network_name
	 *            the Network name to set
	 */
	public void setNetworkName(String network_name) {
		this.network_name = network_name;
	}

	/**
	 * @return the list of gateways
	 */
	public Collection<WSNGW> getGateways() {
		return gws;
	}

	/**
	 * @return the networkId
	 */
	public int getNetworkId() {
		return networkId;
	}

	/**
	 * @param networkId
	 *            the networkId to set
	 */
	public void setNetworkId(int networkId) {
		this.networkId = networkId;
	}

	/**
	 * @return the sampleInterval
	 */
	public int getSampleInterval() {
		return calculateSampleIntervalInSeconds();
	}

	/**
	 * @param sampleInterval
	 *            the sampleInterval to set
	 */
	public void setSampleInterval(int sampleInterval) {
		this.sampleInterval = sampleInterval;
	}

	/**
	 * @param sampleInterval
	 *            the sampleInterval to set
	 */
	public void setSampleInterval(int sampleInterval, int sampleIntervalUnits) {
		this.sampleInterval = sampleInterval;
		this.sampleIntervalUnits = sampleIntervalUnits;
	}

	/**
	 * @return the sampleIntervalUnits
	 */
	private int getSampleIntervalUnits() {
		return sampleIntervalUnits;
	}

	/**
	 * @param sampleIntervalUnits
	 *            the sampleIntervalUnits to set
	 */
	public void setSampleIntervalUnits(int sampleIntervalUnits) {
		this.sampleIntervalUnits = sampleIntervalUnits;
	}

	private int calculateSampleIntervalInSeconds() {

		int sampleFrequencyInSecs = this.sampleInterval;

		switch (this.getSampleIntervalUnits()) {
		case Constants.SAMPLE_FREQUENCY_MINUTES:
			sampleFrequencyInSecs = this.sampleInterval * 60;
			break;
		case Constants.SAMPLE_FREQUENCY_HOURS:
			sampleFrequencyInSecs = this.sampleInterval * 3600;
			break;
		default:
			break;
		}

		return sampleFrequencyInSecs;
	}

	/**
	 * @return the baseStationHostName
	 */
	public String getBaseStationHostName() {
		return baseStationHostName;
	}

	/**
	 * @param baseStationHostName
	 *            the baseStationHostName to set
	 */
	public void setBaseStationHostName(String baseStationHostName) {
		this.baseStationHostName = baseStationHostName;
	}

	/**
	 * @return the baseStationPort
	 */
	public int getBaseStationPort() {
		return baseStationPort;
	}

	/**
	 * @param baseStationPort
	 *            the baseStationPort to set
	 */
	public void setBaseStationPort(int baseStationPort) {
		this.baseStationPort = baseStationPort;
	}

	public int getPanId() {
		return panId;
	}

	public void setPanId(int panId) {
		this.panId = panId;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	/**
	 * Retrieves Network name from DB
	 * 
	 * @param networkId
	 *            the Network ID which name we need to retrieve
	 * 
	 * @return Network name or "" if any error occurs
	 */
	public static String retrieveNetworkName(int networkId) {
		return NETWORK_NAME_PREAMBLE + networkId;
	}

	public String getNetworkName() {
		return network_name;
	}

	public byte[] getPublicKey() {
		return publicKey;
	}

	public void setPublicKey(byte[] publicKey) {
		this.publicKey = publicKey;
	}

	public byte[] getEncipheredPrivateKey() {
		return encipheredPrivateKey;
	}

	public void setEncipheredPrivateKey(byte[] encipheredPrivateKey) {
		this.encipheredPrivateKey = encipheredPrivateKey;
	}
}
