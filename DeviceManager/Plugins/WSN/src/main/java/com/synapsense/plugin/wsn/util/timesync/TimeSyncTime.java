package com.synapsense.plugin.wsn.util.timesync;

import java.text.SimpleDateFormat;
import java.util.Date;

public class TimeSyncTime {

	private boolean didConvert;
	private long convertedTime;

	public TimeSyncTime(boolean _d, long _c) {
		didConvert = _d;
		convertedTime = _c;
	}

	public TimeSyncTime() {

	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (convertedTime ^ (convertedTime >>> 32));
		result = prime * result + (didConvert ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TimeSyncTime other = (TimeSyncTime) obj;
		if (convertedTime != other.convertedTime)
			return false;
		if (didConvert != other.didConvert)
			return false;
		return true;
	}

	public String toString() {
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss.SSS");
		return "TimeSyncTime( " + convertedTime + "=" + sdf.format(new Date(convertedTime)).toString() + " conv: "
		        + didConvert + ")";
	}

	/**
	 * @param convertedTime
	 *            the convertedTime to set
	 */
	void setConvertedTime(long convertedTime) {
		this.convertedTime = convertedTime;
	}

	/**
	 * @param didConvert
	 *            the didConvert to set
	 */
	void setDidConvert(boolean didConvert) {
		this.didConvert = didConvert;
	}

	/**
	 * @return the convertedTime
	 */
	public long getConvertedTime() {
		return convertedTime;
	}

	/**
	 * @return the didConvert
	 */
	public boolean isDidConvert() {
		return didConvert;
	}

}
