package com.synapsense.plugin.wsn.iocore.n29.frames;

import com.synapsense.plugin.wsn.iocore.frames.MoteUARTBadFrame;
import com.synapsense.plugin.wsn.iocore.frames.MoteUARTFrame;
import com.synapsense.plugin.wsn.util.serializer.BigSerializer;

public class MoteUARTNWKStatSoftFrame extends MoteUARTFrame {

	public int pksentcount;
	public long radiouptime;
	public int freeslotcount;
	public int wrongslotcount;
	public int totalslotcount;
	public int pkrcvdcount;
	public int crcfailpkcount;
	public int[] chstatus;
	public int timelostcountsync;
	public int timelostcounttimeout;
	public int noisethresh;

	public MoteUARTNWKStatSoftFrame(byte[] header, byte[] data, String portname) throws MoteUARTBadFrame {
		super(header, portname);
		chstatus = new int[16];
		this.data = data;
		BigSerializer serial = new BigSerializer(data);
		pksentcount = (int) serial.unpackU_INT16();
		radiouptime = (serial.unpackU_INT32()) / 32;
		freeslotcount = (int) serial.unpackU_INT16();
		wrongslotcount = (int) serial.unpackU_INT16();
		totalslotcount = (int) serial.unpackU_INT16();
		pkrcvdcount = (int) serial.unpackU_INT16();
		crcfailpkcount = (int) serial.unpackU_INT16();
		for (int i = 0; i < 16; i++) {
			chstatus[i] = serial.unpackU_INT8();
			if (chstatus[i] == 0xFF)
				chstatus[i] = -1;
		}
		timelostcountsync = serial.unpackU_INT8();
		timelostcounttimeout = serial.unpackU_INT8();
		noisethresh = serial.unpack_INT8();
	}
}
