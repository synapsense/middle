package com.synapsense.plugin.wsn.p3;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;

public class JawaConfiguration implements P3Configuration {

	private static Logger log = Logger.getLogger(JawaConfiguration.class);

	private List<Server> servers = new ArrayList<Server>();
	private List<Rack> racks = new ArrayList<Rack>();
	private List<Jawa> jawas = new ArrayList<Jawa>();

	private long checksum;
	private long sent_checksum;
	private long sent_timestamp;

	private int version_code;

	private boolean isUpdated = false;

	public byte[] serialize() {
		log.trace("Starting to serialize configuration...");

		update();
		int pos = 0;

		int outsize = (jawas.size() * 7) + (racks.size() * 3) + (servers.size() * 5);
		log.trace("Going to serialize " + outsize + " bytes.");
		outsize += 1; /* Version code */
		outsize += 3; /* Counts */
		outsize += 4; /* Embedded checksum */

		byte[] outdata = new byte[outsize];

		outdata[pos++] = (byte) (checksum >> 24);
		outdata[pos++] = (byte) (checksum >> 16);
		outdata[pos++] = (byte) (checksum >> 8);
		outdata[pos++] = (byte) (checksum);

		outdata[pos++] = (byte) (version_code); /* Version Code */
		// @TODO if versioncode=1, put timestamp in here too (32 bit version)

		int count = jawas.size();
		outdata[pos++] = (byte) count;

		for (Jawa j : jawas) {
			log.trace("Serialized Jawa");
			byte[] tdata = j.pack();
			System.arraycopy(tdata, 0, outdata, pos, 7);
			pos += 7;
		}

		count = servers.size();
		outdata[pos++] = (byte) count;

		for (Server s : servers) {
			log.trace("Serialized Server");
			byte[] tdata = s.pack();
			System.arraycopy(tdata, 0, outdata, pos, 5);
			pos += 5;
		}

		count = racks.size();
		outdata[pos++] = (byte) count;

		for (Rack r : racks) {
			log.trace("Serialized rack");
			byte[] tdata = r.pack();
			System.arraycopy(tdata, 0, outdata, pos, 3);
			pos += 3;
		}

		return outdata;
	}

	/**
	 * Update internal state cache
	 */
	private void update() {
		log.trace("Starting P3Configuration CRC computation.");
		Collections.sort(jawas);
		Collections.sort(servers);
		Collections.sort(racks);

		P3CRC32 crc = new P3CRC32();
		for (Jawa j : jawas) {
			crc.update((int) (j.getJid() >> 24) & 0xFF);
			crc.update((int) (j.getJid() >> 16) & 0xFF);
			crc.update((int) (j.getJid() >> 8) & 0xFF);
			crc.update((int) (j.getJid()) & 0xFF);
			crc.update(j.getuPos() & 0xFF);
			crc.update(j.getuHeight() & 0xFF);
			crc.update(j.getRack() & 0xFF);
			crc.update(j.getFeed() & 0xFF);
			crc.update(j.getPhase() & 0xFF);
		}

		for (Server s : servers) {
			crc.update((s.getRawDemandPower() >> 8) & 0xff);
			crc.update(s.getRawDemandPower() & 0xff);
			crc.update(s.getuPos() & 0xff);
			crc.update(s.getuHeight() & 0xff);
			crc.update(s.getRack() & 0xff);
			crc.update(0); /* 0 for feed and phase */
			crc.update(0);
		}

		for (Rack r : racks) {
			crc.update(r.getRack());
			crc.update(r.getHeight());
			crc.update(r.getRpdudelta());
		}

		checksum = crc.getValue() & 0xffffffffL;
		log.trace("New CRC Value: " + Long.toHexString(checksum) + " (expected " + Long.toHexString(sent_checksum)
		        + ")");
		isUpdated = true;
	}

	public List<Jawa> getJawas() {
		if (!isUpdated) {
			update();
		}
		return jawas;
	}

	public long getSentChecksum() {
		return sent_checksum;
	}

	public long getSentTimestamp() {
		return sent_timestamp;
	}

	public long getVersionCode() {
		return version_code;
	}

	/**
	 * Unserialze constructor
	 */
	public JawaConfiguration(byte[] data) throws P3ConfigException {
		int pos = 0;
		log.trace("Starting to deserialize P3Config...");
		long targetCkSum = (data[pos++] << 24) & 0xff000000L;
		targetCkSum |= (data[pos++] << 16) & 0xff0000;
		targetCkSum |= (data[pos++] << 8) & 0xff00;
		targetCkSum |= (data[pos++]) & 0xff;

		this.sent_checksum = targetCkSum & 0xffffffffL;

		version_code = data[pos++] & 0xff;
		log.trace("Version code " + version_code);

		// if (version_code == 1){ FIXME
		if (version_code == 86) {
			long targetTimestamp = (data[pos++] << 24) & 0xff000000L;
			targetTimestamp |= (data[pos++] << 16) & 0xff0000;
			targetTimestamp |= (data[pos++] << 8) & 0xff00;
			targetTimestamp |= (data[pos++]) & 0xff;
			targetTimestamp = (targetTimestamp & 0xffffffffL) * 1000;
			this.sent_timestamp = targetTimestamp;
			// } else {
		}
		Timestamp tts = new Timestamp(this.sent_timestamp);
		// log.trace("Timestamp local "+ tts.toLocaleString()+", " +
		// tts.toGMTString() +" " + Long.toHexString(this.sent_timestamp));
		log.trace("Timestamp local " + tts.toLocaleString() + ", " + Long.toHexString(this.sent_timestamp));

		int count = data[pos++] & 0xff;

		log.trace(count + " jawas");
		byte[] idata = new byte[7];
		for (int i = 0; i < count; i++) {
			System.arraycopy(data, pos, idata, 0, 7);
			Jawa tj = new Jawa(idata);
			log.trace("Jawa: " + tj + (tj.getIsPhaseRef() ? " is Phase Ref" : ""));
			jawas.add(tj);
			pos += 7;

		}

		/* Servers */

		count = data[pos++] & 0xff;
		log.trace(count + " servers.");
		idata = new byte[5];
		for (int i = 0; i < count; i++) {
			System.arraycopy(data, pos, idata, 0, 5);
			Server ts = new Server(idata);
			servers.add(ts);
			pos += 5;
			log.trace("Server: demand power " + ts.getDemandPower());
		}

		idata = new byte[3];
		count = data[pos++] & 0xff;
		log.trace(count + " racks");
		for (int i = 0; i < count; i++) {
			System.arraycopy(data, pos, idata, 0, 3);
			Rack rs = new Rack(idata);
			racks.add(rs);
			pos += 3;
		}
		log.trace("Deserialize complete");
		update();
	}

	public List<Server> getServers() {
		return servers;
	}

	public List<Rack> getRacks() {
		return racks;
	}

	public JawaConfiguration() {

	}

	public long getChecksum() {
		if (!isUpdated)
			update();

		return checksum;
	}

}