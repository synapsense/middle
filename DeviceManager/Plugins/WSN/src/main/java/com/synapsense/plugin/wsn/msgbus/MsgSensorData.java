package com.synapsense.plugin.wsn.msgbus;

public class MsgSensorData extends Message {

	public char device;

	public long longDevice;

	public int sensor; // 16bits

	public int channel;

	public char reading; // actually 16 bit, unsigned

	public long datatimestamp;

	public long sendtimestamp;

	public int devicestate;

	public int controlresponse;

	public MsgSensorData() {
		super();
	}

	public String toString() {
		return "Received data from " + Long.toHexString(device) + " " + Integer.toString(sensor) + " channel "
		        + Integer.toString(channel) + " reading " + Integer.toString(reading);
	}

}
