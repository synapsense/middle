package com.synapsense.plugin.wsn.storers;

import org.apache.log4j.Logger;

import com.synapsense.plugin.messages.DuoConfBuilder;
import com.synapsense.plugin.messages.PowerRackConfBuilder;
import com.synapsense.plugin.wsn.SynapStore;
import com.synapsense.plugin.wsn.WsnConstants;
import com.synapsense.plugin.wsn.configuration.Configuration;
import com.synapsense.plugin.wsn.configuration.WSNNode;
import com.synapsense.plugin.wsn.msgbus.Message;
import com.synapsense.plugin.wsn.msgbus.MsgDuoConfig;
import com.synapsense.plugin.wsn.msgbus.MsgJawaConfig;
import com.synapsense.plugin.wsn.p3.DuoRpdu;
import com.synapsense.plugin.wsn.p3.Jawa;
import com.synapsense.plugin.wsn.p3.Rack;
import com.synapsense.plugin.wsn.p3.Server;
import com.synapsense.plugin.wsn.util.Base64;
import com.synapsense.plugin.wsn.util.SensorNetwork;

public class StorePowerRackConf {
	private static Logger log = Logger.getLogger(StorePowerRackConf.class);

	public static void store(Message message, SensorNetwork net) {
		if (message instanceof MsgJawaConfig)
			storeJawa(message, net);
		else if (message instanceof MsgDuoConfig)
			storeDuo(message, net);

	}

	private static void storeDuo(Message message, SensorNetwork net) {
		WSNNode node;
		DuoConfBuilder bld;
		try {
			MsgDuoConfig duoConfig = (MsgDuoConfig) message;

			node = Configuration.getNode(net.getNetworkId(), duoConfig.deviceId);
			if (node == null) {
				log.warn("DUO configuration data cannot be sent for the node with ID: " + duoConfig.deviceId);
				return;
			}
			node.setP3Configuration(duoConfig.config);

			bld = new DuoConfBuilder(node.getPhysID());
			for (DuoRpdu rpdu : duoConfig.config.getRpduList()) {
				// change this for bug 9132
				//	bld.addDuoRpdu(rpdu.getId(), rpdu.isDelta(), rpdu.isHasPerPhaseVN(), rpdu.isHasPerPhasePP(), rpdu.isSinglePhase());
				bld.addDuoRpdu(rpdu.getId(), rpdu.isDelta(), rpdu.isHasPerPhaseVN(), rpdu.isHasPerPhasePP());
			}

			bld.setChecksum(duoConfig.config.getChecksum());
			bld.setProgram("DuoConfig:" + Base64.encodeBytes(duoConfig.config.serialize()));
			SynapStore.transport.sendOrderedData(node.getId(), WsnConstants.NETWORK_POWER_CONFIG, bld.getMessage());
		} catch (Exception e) {
			log.error("General Duo configuration error", e);
		}

	}

	private static void storeJawa(Message message, SensorNetwork net) {
		MsgJawaConfig p3Config = (MsgJawaConfig) message;

		WSNNode node = Configuration.getNode(net.getNetworkId(), p3Config.deviceId);

		if (node == null) {
			log.warn("Power RACK configuration data cannot be sent for the node with ID: " + p3Config.deviceId);
			return;
		}

		node.setP3Configuration(p3Config.config); /*
												 * Set the P3 configuration for
												 * runtime lookup
												 */

		PowerRackConfBuilder bld = new PowerRackConfBuilder(node.getPhysID());

		for (Jawa jawa : p3Config.config.getJawas()) {
			bld.addPlug(jawa.getJid(), jawa.getPhase(), jawa.getFeed(), jawa.getuPos(), jawa.getuHeight(),
			        jawa.getRack());
		}

		for (Server server : p3Config.config.getServers()) {
			bld.addServer(server.getuPos(), server.getuHeight(), server.getRack(), server.getDemandPower());
		}

		for (Rack rack : p3Config.config.getRacks()) {
			bld.addRack(rack.getRack(), rack.getHeight(), rack.getRpdudelta());
		}

		bld.setChecksum(p3Config.config.getChecksum());
		bld.setProgram("P3Config:" + Base64.encodeBytes(p3Config.config.serialize()));

		log.trace("Loaded configuration packet. Checksum " + p3Config.config.getChecksum());

		SynapStore.transport.sendOrderedData(node.getId(), WsnConstants.NETWORK_POWER_CONFIG, bld.getMessage());
	}
}
