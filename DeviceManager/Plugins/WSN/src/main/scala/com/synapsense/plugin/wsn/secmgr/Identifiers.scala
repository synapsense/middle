package com.synapsense.plugin.wsn.secmgr

trait Identifier

trait IdToLittleEndian {
  def id: Long
  def bigEndian: Seq[Byte] = littleEndian.reverse

  def littleEndian: Seq[Byte] = {
    Seq[Byte]((id & 0xFF).toByte, ((id >> 8) & 0xFF).toByte,
      ((id >> 16) & 0xFF).toByte, ((id >> 24) & 0xFF).toByte,
      ((id >> 32) & 0xFF).toByte, ((id >> 40) & 0xFF).toByte,
      ((id >> 48) & 0xFF).toByte, ((id >> 56) & 0xFF).toByte)
  }
}

object NodeLongIdentifier {
  def fromLittleEndian(l: Long): NodeLongIdentifier = {
    val revlong = ((l >> 56) & 0xFF) |
      (((l >> 48) & 0xFF) << 8) |
      (((l >> 40) & 0xFF) << 16) |
      (((l >> 32) & 0xFF) << 24) |
      (((l >> 24) & 0xFF) << 32) |
      (((l >> 16) & 0xFF) << 40) |
      (((l >> 8) & 0xFF) << 48) |
      (((l & 0xFF) << 56))
    NodeLongIdentifier(revlong)
  }
}

case class NodeLongIdentifier(id: Long) extends Identifier with IdToLittleEndian {
  val isBcast = id == 0xFFFFFFFFFFFFFFFFL
}

case class NodeShortIdentifier(sid: Char) extends Identifier
