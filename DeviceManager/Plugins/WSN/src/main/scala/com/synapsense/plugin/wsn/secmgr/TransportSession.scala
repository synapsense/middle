package com.synapsense.plugin.wsn.secmgr

import java.util.Date

import org.apache.log4j.Logger

/**
 * Todo: Rewrite as a extractor?
 * @param ctr
 * @param ooWindow
 */
class NonceCounter(val ctr: Long, val ooWindow: Long) {

  def reconstruct(b: Byte): Long = {
    val rectr = (b.toLong & 0xFF) | (ctr & 0xFFFFFF00)
    val seqcounterbyte = ctr & 0xFF
    if ((b - seqcounterbyte).toByte > 0 && (rectr - ctr).toInt < 0)
      rectr + 0x100
    else if ((b - seqcounterbyte).toByte < 0 && (rectr - ctr).toInt > 0)
      rectr - 0x100
    else
      rectr
  }

  def update(recvCtr: Long): Option[NonceCounter] = {
    if ((recvCtr - ctr).toInt <= 0)
      None // Too old
    else if (recvCtr == ctr + 1) {
      var newctr = ctr
      var newWindow = ooWindow
      do {
        newctr = newctr + 1
        newWindow = newWindow >> 1 // Roll the window down 1
      } while ((newWindow & 1) == 1);
      Some(new NonceCounter(newctr, newWindow))
    } else {
      val offset = recvCtr - ctr - 1;
      if (offset >= 32) {
        Some(new NonceCounter(recvCtr, 0)) // Overshoot the window
      } else {
        if ((ooWindow & (1 << offset)) != 0) { // Check for matches in the OO window
          None
        } else {
          var newWindow = ooWindow | (1 << offset)
          Some(new NonceCounter(ctr, newWindow))
        }
      }
    }
  }
}

object ExpiresTime {
  private[this] val ng = new java.util.Random()
  def nextGaussian = ng.nextDouble()
  val regenPeriod = 1000L * 60L * 60L * 24L * 30L // 30 Days
  val minRegenPeriod = 1000L * 60L * 60L * 24L * 4L // 4 days
}

class ExpiresTime(val noexpire: Boolean = false) {
  var expiresTime = System.currentTimeMillis() + (ExpiresTime.nextGaussian * ExpiresTime.regenPeriod).toLong + ExpiresTime.minRegenPeriod
  def fresh = noexpire || System.currentTimeMillis() < expiresTime
}

class TransportSession(nid: NodeLongIdentifier, suite: SecuritySuite, val service: ServiceCode, val key: Key, noexpire: Boolean = false) {
  private[this] var outboundNonce: NonceCounter = new NonceCounter(0, 0)
  private[this] var inboundNonce: NonceCounter = new NonceCounter(0, 0)
  val expiresTime = new ExpiresTime(noexpire)
  private val log = Logger.getLogger(this.getClass)

  log.info("Session for " + nid.id.toHexString + " service " + service + " expires at " + new Date(expiresTime.expiresTime) + " diff(" + (expiresTime.expiresTime - System.currentTimeMillis()) + "ms)")

  private[this] def updateOutbound(): Long = {
    outboundNonce = outboundNonce.update(outboundNonce.ctr + 1).get // Outbounds always pass
    outboundNonce.ctr
  }

  def buildNonce(obctr: Long, nid: NodeLongIdentifier): Seq[Byte] = {
    Seq(0.toByte) ++ Seq((obctr >> 24).toByte, (obctr >> 16).toByte, (obctr >> 8).toByte, obctr.toByte) ++ nid.littleEndian
  }

  /**
   * Is this transport session in need of refreshing?
   * @return
   */
  def fresh = expiresTime.fresh

  def processInbound(inb: SessionInbound): (Boolean, Option[(Int, Seq[Byte])]) = {
    val r = inboundNonce.reconstruct(inb.nonce)
    //log.info(nid.id.toHexString + " Process " + r + " Key" + key.data(0).formatted(" %02X") + key.data(1).formatted(" %02X") + key.data(2).formatted(" %02X") + key.data(3).formatted(" %02X"))
    inboundNonce.update(r) match {
      case None => (false, None) // Security error
      case Some(n) => {
        val nonce = buildNonce(r, NodeLongIdentifier(0))
        suite.symmetric_algorithm.ccm(false, nonce, key, inb.cipherData) match {
          case None =>
            log.warn(nid.id.toHexString + " Inbound packet didn't verify")
            (true, None)
          case Some(ba) =>
            log.debug(nid.id.toHexString + " Inbound packet verified")
            inboundNonce = n
            (true, Some((inb.service, ba)))
        }

      }
    }
  }

  def encapsulateOutbound(data: Seq[Byte]): OutboundPacket = {
    val obctr = updateOutbound()
    val nonce = buildNonce(obctr, nid)
    //log.info("Out " + obctr + " Key" + key.data(0).formatted(" %02X") + key.data(1).formatted(" %02X") + key.data(2).formatted(" %02X") + key.data(3).formatted(" %02X"))
    val cipher = suite.symmetric_algorithm.ccm(true, nonce, key, data).getOrElse(Seq())
    SessionOutbound(service.service, Seq(obctr.toByte), cipher)
  }

  def encapsulateRequest(data: Seq[Byte]): OutboundPacket = {
    val obctr = updateOutbound()
    val nonce = buildNonce(obctr, nid)
    //log.info("Est " + obctr + " Key" + key.data(0).formatted(" %02X") + key.data(1).formatted(" %02X") + key.data(2).formatted(" %02X") + key.data(3).formatted(" %02X"))
    val cipher = suite.symmetric_algorithm.ccm(true, nonce, key, data).getOrElse(Seq())
    SessionEstablishWrapper(Seq(obctr.toByte), cipher)
  }

}

case class ServiceCode(service: Int)

class TransportSessions(suite: SecuritySuite, val node: NodeLongIdentifier, val masterKey: Key) {
  private[this] val sessions = scala.collection.mutable.HashMap[ServiceCode, TransportSession]()
  private[this] var failCounter = 0
  private val log = Logger.getLogger(this.getClass)

  sessions += (ServiceCode(0) -> new TransportSession(node, suite, ServiceCode(0), masterKey, noexpire = true)) // Add the master key here

  def hasSession(service: Int) = sessions.contains(ServiceCode(service))

  /**
   * Re-establish any sessions which have "expired"
   * @return
   */
  def refresh = sessions filter (s => !s._2.fresh && s._1.service > 0) map (p => establishSession(p._1.service))

  /**
   * Check if the session is having trouble in operation.
   * @return true if too many errors are generated
   */
  def sessionErroring: Boolean = failCounter > 10

  def send(service: Int, data: Seq[Byte]): Option[OutboundPacket] = {
    sessions.get(ServiceCode(service)) match {
      case None => None
      case Some(s) => Some(s.encapsulateOutbound(data))
    }
  }

  def checkoldsession(service: Int, inb: SessionInbound, failseqno: Boolean): (Option[OutboundPacket], Option[(Int, Seq[Byte])]) = {
    sessions.get(ServiceCode(-service)) match {
      case None =>
        if (failseqno) {
          (None, None)
        } else {
          failCounter += 2
          (None, None)
        }
      case Some(olds) =>
        if (olds.fresh) {
          log.warn(node.id.toHexString + " Trying old session");
          val (seqmatch, pkmatch) = olds.processInbound(inb)
          seqmatch match {
            case true =>
              pkmatch match {
                case Some(y) =>
                  (Some(establishSession(service)), Some(y))
                case None => failCounter += 2; (None, None)
              }
            case false =>
              log.info(node.id.toHexString + " ignore old sequence number")
              (None, None)
          }
        } else {
          if (failseqno) {
            (None, None)
          } else {
            failCounter += 2
            sessions -= ServiceCode(-service)
            (None, None)
          }
        }
    }
  }

  def recv(service: Int, inb: SessionInbound): (Option[OutboundPacket], Option[(Int, Seq[Byte])]) = {
    sessions.get(ServiceCode(service)) match {
      case None => (None, None)
      case Some(s) =>
        val (seqmatch, pkmatch) = s.processInbound(inb)
        seqmatch match {
          case true =>
            pkmatch match {
              case Some(y) =>
                if (failCounter > 0) failCounter -= 1; (None, Some(y))
              case None =>
                // Check old session
                checkoldsession(service, inb, false)
            }
          case false =>
            // Check old session
            log.info(node.id.toHexString + " ignore old sequence number")
            checkoldsession(service, inb, true)
        }
    }
  }

  def establishSession(service: Int): OutboundPacket = {
    var sendnew = true
    var key: Key = new SymmetricKey(suite.rng(16))
    sessions.get(ServiceCode(service)) match {
      case Some(s) =>
        if (s.fresh) {
          key = s.key
          sendnew = false
        } else {
          s.expiresTime.expiresTime = System.currentTimeMillis() + 1000L * 60L * 60L // Valid for an hour
          sessions += (ServiceCode(-service) -> s)
        }
      case None => None
    }

    if (sendnew) {
      sessions += (ServiceCode(service) -> new TransportSession(node, suite, ServiceCode(service), key))
    }
    if (node.isBcast)
      sessions(ServiceCode(0)).encapsulateRequest(SessionEstablishBcast(service, key).serializeBody)
    else
      sessions(ServiceCode(0)).encapsulateRequest(SessionEstablish(service, key).serializeBody)
  }
}
