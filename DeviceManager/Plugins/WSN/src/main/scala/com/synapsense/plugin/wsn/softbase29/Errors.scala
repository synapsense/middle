package com.synapsense.plugin.wsn.softbase29

class GWInstantiationException(desc: String, t: Throwable) extends Exception(desc) {
  def this(desc: String) = this(desc, null)
}

class SendError(desc: String) extends Throwable

class PathTooLong(desc: String, val maxData: Int) extends SendError(desc)

class TooMuchData(desc: String) extends SendError(desc)

class WrongProtocol(desc: String) extends SendError(desc)

class TimeSyncUnavailable(desc: String) extends SendError(desc)

class NoRoutingInformation() extends SendError("No route")

class GatewaySendError() extends SendError("Gateway send error")

class TooManyRetries() extends SendError("Too many retries - no response from device")