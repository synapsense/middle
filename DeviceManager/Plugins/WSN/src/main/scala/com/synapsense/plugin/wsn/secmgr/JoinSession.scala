package com.synapsense.plugin.wsn.secmgr

import org.bouncycastle.crypto.generators.ECKeyPairGenerator
import org.bouncycastle.crypto.params.{ ECPublicKeyParameters, ECPrivateKeyParameters, ECKeyGenerationParameters }

import org.apache.log4j.Logger
import org.bouncycastle.crypto.agreement.ECDHBasicAgreement

import java.lang.Integer
import BetterBigInteger._

trait JoinMessageResult
case class JoinInProgress(phase: Int, response: Option[Seq[OutboundPacket]]) extends JoinMessageResult {
  override def toString = "<JoinInProgress response " + response.getOrElse(Seq()).foreach(_.toString) + ">"
}
case class JoinComplete(transport: TransportSessions, response: Option[Seq[OutboundPacket]]) extends JoinMessageResult
trait JoinFailed extends JoinMessageResult
case class JoinInvalidMessage() extends JoinFailed
case class JoinExpiredSession() extends JoinFailed
case class JoinBusy() extends JoinFailed

/**
 * A stateful join session container which keeps track of all session
 * parameters during the join process.
 * @param suiteOrd : Ordinal for the security suite in use
 * @param deviceId : A long representing the node identifier
 * @param networkPan: An integer representing the network PAN address
 */
class JoinSession(networkKey: NetworkKeypair, suiteOrd: Int, deviceId: Long, networkPan: Int) {

  val suite = SecuritySuites.suiteByOrdinal(suiteOrd)
  val createdOn = System.currentTimeMillis()
  private val log = Logger.getLogger(this.getClass)

  val devicejointimeout = 3 * 60 * 1000

  private[this] var inPhase = 1

  // Little endian pack the PAN
  val encoded_pan = Seq[Byte](networkPan.toByte, (networkPan >> 8).toByte)

  def expired(now: Long = System.currentTimeMillis()): Boolean = {
    if (inPhase == 1) {
      (now - createdOn) > (1000 * 60) // 1 minute in phase 1
    } else
      (now - createdOn) > (1000 * 60 * 30) // 30 minute expiration
  }

  def generateTransportSession(): JoinMessageResult = {
    val agree = new ECDHBasicAgreement()
    agree.init(new ECPrivateKeyParameters(ephemPrivateNetwork.get.toBigInteger, suite.algorithm.domainParameters))
    val result = agree.calculateAgreement(new ECPublicKeyParameters(ephemPublicDevice.get.point, suite.algorithm.domainParameters))
    //log.trace("Raw ECDH: " + ToHexHelp.tohex(result.toFixedByteArray(32)))
    val kdf = suite.kdf(result.toFixedByteArray(32))
    val ts = new TransportSessions(suite, eui64.get, new SymmetricKey(kdf._1))
    //log.trace("Master key: " + ToHexHelp.tohex(kdf._1))
    JoinComplete(ts, None)
  }

  def processEndPhaseThree(): JoinMessageResult = {
    if (inPhase != 2) {
      log.warn("Phase three message while not in phase two, expiring join session.")
      JoinInvalidMessage()
    } else {
      val verify = suite.algorithm.verifier(staticPublicDevice.get)
      val hash = suite.hash.apply(suite.algorithm.compressKey(ephemPublicNetwork.get).key ++
        suite.algorithm.compressKey(ephemPublicDevice.get).key ++
        encoded_pan).toArray
      //log.trace("Verification hash: " + ToHexHelp.tohex(hash) + " compare to R: " +
      //ToHexHelp.tohex(signatureOnEphemWithDevice.get.r) + " S: " + ToHexHelp.tohex(signatureOnEphemWithDevice.get.s))
      verify.verifySignature(hash, signatureOnEphemWithDevice.get.rToBigInteger, signatureOnEphemWithDevice.get.sToBigInteger) match {
        case false => { log.warn("Signature does not match - failing"); JoinInvalidMessage() }
        case true => log.info("Generating transport session"); generateTransportSession()
      }
    }
  }

  def verifyDeviceSignature: Boolean = {
    // Validate the node is signed by something we trust

    val hashin = eui64.get.bigEndian ++ suite.algorithm.compressKey(staticPublicDevice.get).key
    val hash = suite.hash(hashin).toArray
    log.trace("Validation hash input: " + ToHexHelp.tohex(hashin))
    log.trace("Validation hash output: " + ToHexHelp.tohex(hash))
    log.trace("Sig R: " + ToHexHelp.tohex(signatureOnDeviceWithCA.get.r) + " S: " + ToHexHelp.tohex(signatureOnDeviceWithCA.get.s))
    val verifer = suite.algorithm.verifier(ca.get.pubkey)
    val res = verifer.verifySignature(hash, signatureOnDeviceWithCA.get.rToBigInteger, signatureOnDeviceWithCA.get.sToBigInteger)
    if (!res)
      log.error("Device signature was not valid for " + eui64.get.id.toHexString)
    else
      log.trace("Device signature verification passed")
    res
  }

  def generatePhaseTwoPackets: JoinMessageResult = {
    // Generate network private/public ephemeral keypair
    val gen = new ECKeyPairGenerator()
    gen.init(new ECKeyGenerationParameters(suite.algorithm.domainParameters, new java.security.SecureRandom()))
    val kp = gen.generateKeyPair()
    val pr = kp.getPrivate.asInstanceOf[ECPrivateKeyParameters].getD
    val pu = kp.getPublic.asInstanceOf[ECPublicKeyParameters].getQ
    ephemPrivateNetwork = Some(new ECCPrivKey(pr.toFixedByteArray(32)))
    ephemPublicNetwork = Some(new ECCPubKey(pu))

    // Start generating phase two packets
    val p1 = JoinPhaseTwoPacket1(suite.algorithm.compressKey(ephemPublicNetwork.get))
    val p15 = JoinPhaseTwoPacket1point5(suite.algorithm.compressKey(networkKey.pub), networkKey.ca)

    // Include the signature for the network key
    val p2 = JoinPhaseTwoPacket2(networkKey.sig.r)
    val p25 = JoinPhaseTwoPacket2point5(networkKey.sig.s)

    val signer = suite.algorithm.signer(networkKey.priv)
    val tohex = ToHexHelp.tohex

    log.trace("Signing with key " + tohex(networkKey.priv.raw))
    val epbc = suite.algorithm.compressKey(ephemPublicDevice.get).key
    val epnc = suite.algorithm.compressKey(ephemPublicNetwork.get).key
    val nca = Seq(networkKey.ca.byteValue)
    val cas = Seq(ca.get.ordinal.byteValue)

    log.trace("Hash input: epbc " + tohex(epbc) + " epnc " + tohex(epnc) + " nca " + tohex(nca) + " ca " + tohex(cas))

    val hash = suite.hash.apply(epbc ++ epnc ++ nca ++ cas ++ encoded_pan).toArray
    log.trace("Hash to sign: " + hash.map("%02X".format(_)).mkString)
    val rs = signer.generateSignature(hash)
    log.trace("Signed hash for signature: R: (len " + rs(0).toByteArray.length + ") " + ToHexHelp.tohex(rs(0).toByteArray) + " S: " + ToHexHelp.tohex(rs(1).toByteArray))

    val p3 = JoinPhaseTwoPacket3(rs(0).toFixedByteArray(32))
    val p35 = JoinPhaseTwoPacket3point5(rs(1).toFixedByteArray(32))

    val packets = Seq(new CoalescedOutboundPacket(p1, p15), new CoalescedOutboundPacket(p2, p25), new CoalescedOutboundPacket(p3, p35))
    log.trace("Sending outgoing packets for end of phase 1")
    // Return the needed packets
    inPhase = 2
    JoinInProgress(2, Some(packets))
  }

  def processEndPhaseOne(delay: Long, outbpipesize: Integer, outbdelay: Integer): JoinMessageResult = {
    if (inPhase != 1) {
      log.warn("Phase one message while not in phase one, expiring join session.")
      JoinExpiredSession()
    } else {
      if (eui64.isDefined && ephemPublicDevice.isDefined && staticPublicDevice.isDefined && ca.isDefined) {
        log.debug("Processing end of phase 1 join packet")
        // Check if security join message can be reached before device timeout with 20 seconds buffer
        if (delay + (outbpipesize + 3) * outbdelay * 1000 + 20000 > devicejointimeout)
          JoinBusy()
        else if (!verifyDeviceSignature)
          JoinInvalidMessage()
        else {
          generatePhaseTwoPackets
        }
      } else {
        log.debug("Not enough information to end phase 1")
        JoinInProgress(1, None)
      }
    }
  }

  def processPhaseOnePacket1(message: JoinPhaseOnePacket1, delay: Long, outbpipesize: Integer, outbdelay: Integer): JoinMessageResult = {
    eui64 = Some(message.eui64)
    ephemPublicDevice = Some(suite.algorithm.decompressKey(message.edpub))
    staticPublicDevice = Some(suite.algorithm.decompressKey(message.dpub))
    processEndPhaseOne(delay, outbpipesize, outbdelay)
  }

  def processPhaseOnePacket2(message: JoinPhaseOnePacket2, delay: Long, outbpipesize: Integer, outbdelay: Integer): JoinMessageResult = {
    ca = Some(BuiltinCA(message.caSpec))
    signatureOnDeviceWithCA = Some(message.signcd)
    processEndPhaseOne(delay, outbpipesize, outbdelay)
  }

  def processPhaseThreePacket1(message: JoinPhaseThreePacket1): JoinMessageResult = {
    signatureOnEphemWithDevice = Some(message.signd)
    processEndPhaseThree()
  }

  def processJoinMessage(message: JoinPacket, delay: Long, outbpipesize: Integer, outbdelay: Integer): JoinMessageResult = {
    if (expired())
      JoinExpiredSession()
    else
      message match {
        case p1: JoinPhaseOnePacket1 => processPhaseOnePacket1(p1, delay, outbpipesize, outbdelay)
        case p2: JoinPhaseOnePacket2 => processPhaseOnePacket2(p2, delay, outbpipesize, outbdelay)
        case p3: JoinPhaseThreePacket1 => processPhaseThreePacket1(p3)
        case _ => log.trace("Unknown inbound join message - sending invalid message" + message); JoinInvalidMessage()
      }
  }

  var eui64: Option[NodeLongIdentifier] = None
  var ephemPublicDevice: Option[ECCPubKey] = None
  var ephemPublicNetwork: Option[ECCPubKey] = None
  var ephemPrivateNetwork: Option[ECCPrivKey] = None

  var staticPublicDevice: Option[ECCPubKey] = None
  var ca: Option[CA] = None

  var signatureOnEphemWithDevice: Option[Signature] = None

  var signatureOnDeviceWithCA: Option[Signature] = _

}
