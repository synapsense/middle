package com.synapsense.plugin.wsn.msgbus

import org.apache.commons.logging.{ LogFactory, Log }

/**
 * Logging for all message types
 */
private[msgbus] object MessageLog {
  private[msgbus] val log: Log = LogFactory.getLog(classOf[Message])
}

/**
 * An interface/trait representing a class
 * which handles message types.
 */
trait MessageHandlerFromJava {
  def onSuccess(m: Message)
  def onError(m: Message, t: Throwable)
}

/**
 * Provides a base class for messages, and allows for a function
 * handler to be provided for success and error cases
 */
class Message {
  private[this] var successHandlers = List[(Message, (Message) => Unit)]()
  private[this] var errorHandlers = List[(Message, (Message, Throwable) => Unit)]()

  def getHandlers = (successHandlers, errorHandlers)

  def addSuccessHandler(func: (Message) => Unit) {
    successHandlers = successHandlers ++ List((this, func))
  }

  /**
   * Add a class handler for message types based upon a MessageHandlerFromJava
   * Since creating closures from Java is way more painful than it should be.
   * @param java
   */
  def addMessageHandlerJava(java: MessageHandlerFromJava) {
    addSuccessHandler({ m => java.onSuccess(m) })
    addErrorHandler({ (m, t) => java.onError(m, t) })
  }

  /**
   * Add a scala function to be invoked with the error message
   * @param func
   */
  def addErrorHandler(func: (Message, Throwable) => Unit) {
    errorHandlers = errorHandlers ++ List((this, func))
  }

  /**
   * Indicate an error on this message chain
   * @param t Throwable object which represents the error
   */
  def onError(t: Throwable) {
    MessageLog.log.warn("Message " + this.toString + " onError: ", t)
    errorHandlers.foreach(f => f._2(f._1, t))
  }

  /**
   * Indicate success for this message chain.
   */
  def onSuccess() {
    successHandlers.foreach(f => f._2(f._1))
  }

  /**
   * When messages get transformed into other messages, it may be important
   * to maintain the list of success and error handlers. This system
   * maintains a chained list of handlers and even their originating message
   * if so desired, so multiple entities can be notified of this success or
   * error.
   * @param m
   */
  def chain(m: Message) {
    if (m != null) {
      successHandlers = successHandlers ::: m.getHandlers._1
      errorHandlers = errorHandlers ::: m.getHandlers._2
    }
  }

}
