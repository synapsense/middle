package com.synapsense.plugin.wsn.secmgr

import com.synapsense.plugin.wsn.iocore.frames.{ ByteReader, BinaryParsers }

object PacketTypes {
  val phase1p1 = 10.toByte
  val phase1p2 = 11.toByte
  val phase2p1 = 14.toByte
  val phase2p1point5 = 141.toByte
  val phase2p2 = 15.toByte
  val phase2p2point5 = 151.toByte
  val phase2p3 = 16.toByte
  val phase2p3point5 = 161.toByte

  val phase3p1 = 12
  val phase3p2 = 13

  val sessionEstablish: Byte = 17
  val sessionEstablishBcast: Byte = 21
  val sessionInbound = 18
  val sessionOutbound: Byte = 19
  val sessionRequest: Byte = 21

  val resetSecState: Byte = 20
  val coalesced: Byte = 121
}

trait SecurityPacket
trait TransportPacket extends SecurityPacket
trait JoinPacket extends SecurityPacket
trait SessionEstablisher extends TransportPacket
trait SessionPacket extends TransportPacket

trait OutboundPacket {
  def version: Byte = 1
  def code: Byte
  def serializeBody: Seq[Byte]
  def serialize: Seq[Byte] = Seq(version) ++ Seq(code.toByte) ++ serializeBody
  def split: Option[Seq[OutboundPacket]] = None
}

/**
 * A packet which is combined from other packets.
 * Perhaps this really should be done by the respective
 * packets, or perhaps not.
 *
 * In short this smells
 */
class CoalescedOutboundPacket(packets: OutboundPacket*) extends OutboundPacket {
  def code = PacketTypes.coalesced
  def serializeBody = packets.foldLeft(List[Byte]()) { (b, a) => b ++ a.serialize }
  override def split = Some(packets)
}

case class ResetSecurityState() extends OutboundPacket {
  def code = PacketTypes.resetSecState
  def serializeBody = Seq[Byte]()
}
object ToHexHelp {
  val tohex = { seq: Seq[Byte] => seq.map("%02X".format(_)).mkString }
}

case class JoinPhaseOnePacket1(eui64: NodeLongIdentifier, edpub: ECCPubCompressedKey, dpub: ECCPubCompressedKey) extends JoinPacket
case class JoinPhaseOnePacket2(caSpec: Int, signcd: Signature) extends JoinPacket

case class JoinPhaseTwoPacket1(enpub: ECCPubCompressedKey) extends OutboundPacket {
  def code = PacketTypes.phase2p1
  def serializeBody = enpub.key
  override def toString = "JoinPhaseTwoPacket1 - code %x".format(code) + " data " + ToHexHelp.tohex(serializeBody)
}

case class JoinPhaseTwoPacket1point5(npub: ECCPubCompressedKey, caSpec: Int) extends OutboundPacket {
  def code = PacketTypes.phase2p1point5
  def serializeBody = npub.key ++ Seq(caSpec.toByte)
  override def toString = "JoinPhaseTwoPacket1point5 - code %x".format(code) + " data " + ToHexHelp.tohex(serializeBody)
}

case class JoinPhaseTwoPacket2(r: Seq[Byte]) extends OutboundPacket {
  def code = PacketTypes.phase2p2
  def serializeBody = r
  override def toString = "JoinPhaseTwoPacket2 - code %x".format(code) + " data " + ToHexHelp.tohex(serializeBody)
}

case class JoinPhaseTwoPacket2point5(s: Seq[Byte]) extends OutboundPacket {
  def code = PacketTypes.phase2p2point5
  def serializeBody = s
  override def toString = "JoinPhaseTwoPacket2point5 - code %x".format(code) + " data " + ToHexHelp.tohex(serializeBody)
}

case class JoinPhaseTwoPacket3(r: Seq[Byte]) extends OutboundPacket {
  def code = PacketTypes.phase2p3
  def serializeBody = r
  override def toString = "JoinPhaseTwoPacket3 - code %x".format(code) + " data " + ToHexHelp.tohex(serializeBody)
}

case class JoinPhaseTwoPacket3point5(s: Seq[Byte]) extends OutboundPacket {
  def code = PacketTypes.phase2p3point5
  def serializeBody = s
  override def toString = "JoinPhaseTwoPacket3point5 - code %x".format(code) + " data " + ToHexHelp.tohex(serializeBody)
}

case class JoinPhaseThreePacket1(signd: Signature) extends JoinPacket
case class JoinPhaseThreePacket2(hmac: Seq[Byte]) extends JoinPacket

/**
 * Session request and establish are containered inside a SessionInbound/SessionOutbound
 * packet. They are parsed separately.
 */
case class SessionRequest(service: Int) extends SessionEstablisher {
  override def toString = { "SessionRequest: %02X".format(service) }
}

case class SessionEstablishWrapper(nonce: Seq[Byte], data: Seq[Byte]) extends OutboundPacket {
  def code = PacketTypes.sessionEstablish
  def serializeBody = nonce ++ data
}

case class SessionEstablish(service: Int, key: Key) extends OutboundPacket {
  def code = PacketTypes.sessionEstablish
  def serializeBody = Seq(service.toByte) ++ key.data
}

case class SessionEstablishBcast(service: Int, key: Key) extends OutboundPacket {
  def code = PacketTypes.sessionEstablishBcast
  def serializeBody = Seq(service.toByte) ++ key.data
}

case class SessionInbound(service: Int, nonce: Byte, cipherData: Seq[Byte]) extends SessionPacket
case class SessionOutbound(service: Int, nonce: Seq[Byte], cipherData: Seq[Byte]) extends OutboundPacket {
  def code = PacketTypes.sessionOutbound
  def serializeBody = Seq(service.toByte) ++ nonce ++ cipherData
}

class SecurityMessageParser extends BinaryParsers {

  def phase3p2 = bytes(32) ^^ { case x => JoinPhaseThreePacket2(x) }
  def phase3p1 = bytes(64) ^^ { case x => JoinPhaseThreePacket1(new ECDSASignature(x)) }

  def phase1p2 = u1 ~ bytes(64) ^^ {
    case (x: Int) ~ y => JoinPhaseOnePacket2(x, new ECDSASignature(y))
  }

  def phase1p1 = u8 ~ bytes(33) ~ bytes(33) ^^ {
    case (x: Long) ~ (y: Seq[Byte]) ~ (z: Seq[Byte]) => JoinPhaseOnePacket1(NodeLongIdentifier.fromLittleEndian(x), ECCPubCompressedKey(y), ECCPubCompressedKey(z))
  }

  def sessionInbound = u1 ~ u1 ~ rest ^^ {
    case (x: Int) ~ (y: Int) ~ (z) => SessionInbound(x, y.toByte, z)
  }

  def sessionRequest = u1 ^^ {
    case (x: Int) => SessionRequest(x)
  }

  def body = ((intMatch(PacketTypes.phase1p1, 1) ~ phase1p1) |
    (intMatch(PacketTypes.phase1p2, 1) ~ phase1p2) |
    (intMatch(PacketTypes.sessionInbound, 1) ~ sessionInbound) |
    (intMatch(PacketTypes.phase3p1, 1) ~ phase3p1) |
    (intMatch(PacketTypes.phase3p2, 1) ~ phase3p2) |
    (intMatch(PacketTypes.sessionRequest, 1) ~ sessionRequest)) ^^ {
      case x ~ (y: SecurityPacket) => y
    }

  def version = intMatch(1, 1)

  def all = version ~ body ^^ {
    case x ~ y => y
  }

  def parseMain(input: ByteReader): Option[SecurityPacket] = {
    parse(all, input) match {
      case Success(t, _) => t match {
        case (x: SecurityPacket) => Option(x)
        case _ => { None }
      }
      case Failure(msg, _) => { None }
      case Error(msg, _) => { None }
    }
  }
}

object SecurityProtocols {
  def parser = new SecurityMessageParser
}
