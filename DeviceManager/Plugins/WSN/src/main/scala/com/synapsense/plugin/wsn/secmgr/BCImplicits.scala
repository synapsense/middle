package com.synapsense.plugin.wsn.secmgr

import org.bouncycastle.math.ec.{ ECCurve, ECPoint }

class ECPointWithCompression(c: org.bouncycastle.math.ec.ECCurve,
  fe: org.bouncycastle.math.ec.ECFieldElement, fe2: org.bouncycastle.math.ec.ECFieldElement) extends ECPoint.Fp(c, fe, fe2, true) {

  def enableCompressed(): ECPointWithCompression = {
    this.withCompression = true
    this
  }

  def disableCompressed(): ECPointWithCompression = {
    this.withCompression = false
    this
  }
}

object BCImplicits {

  implicit def toWithCompression(p: ECPoint): ECPointWithCompression = {
    new ECPointWithCompression(p.getCurve, p.getX, p.getY)
  }
}
