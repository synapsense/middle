package com.synapsense.plugin.wsn.secmgr

import scala.collection.mutable.HashMap
import org.bouncycastle.math.ec.ECPoint
import java.math.BigInteger

/**
 * Trait representing a private key in a symmetric system
 */
trait Key {
  /**
   * A network serialized key stream
   * @return little endian key data
   */
  def data: Seq[Byte]

}

class SymmetricKey(key: Seq[Byte]) extends Key {
  def data = key
}

/**
 * Trait representing a public key in a public key system
 */
trait PubKey

/**
 * Trait representing a private key in a public key system
 */
trait PrivKey {
  def toBigInteger: BigInteger
}

/**
 * A public key signature
 */
trait Signature {
  def r: Seq[Byte]
  def s: Seq[Byte]
  def rToBigInteger = new BigInteger(1, r.toArray)
  def sToBigInteger = new BigInteger(1, s.toArray)
}

/**
 * The result of a hash operation
 */
trait Hash

object ECCPubCompressedKey {
  def apply(raw: Seq[Byte]): ECCPubCompressedKey = new ECCPubCompressedKey(raw)
}

/**
 * A container for a little endian ECC Public Compressed Key
 * @param raw
 */
class ECCPubCompressedKey(raw: Seq[Byte]) {
  def key: Seq[Byte] = raw
}

/**
 * A deserialized form of a ECC Public Key
 * @param point
 */
case class ECCPubKey(point: ECPoint) extends PubKey {

}

/**
 * Container for an ECC Private Key
 * @param raw BIG ENDIAN representation of the private key
 */
case class ECCPrivKey(raw: Seq[Byte]) extends PrivKey {
  def toBigInteger = new BigInteger(1, raw.toArray)
}

class ECDSASignature(val bytes: Seq[Byte]) extends Signature {
  def r = bytes.slice(0, 32)
  def s = bytes.slice(32, 64)

}

trait CA {
  def pubkey: ECCPubKey
  def ordinal: Int
}

object BuiltinCA {
  private[this] val cas = new HashMap[Int, CA]()

  def apply(ordinal: Int): CA = {
    cas(ordinal)
  }

  cas.put(100, new BuiltinCA(TestingKeys.ecNetworkPublic, 100))
  cas.put(82, new BuiltinCA(AckbarKeys.ecNetworkPublic, 82))
  cas.put(80, new BuiltinCA(AckbarKeys.ecNetworkPublic, 80))
  cas.put(0, new NoCA())
}

class BuiltinCA(pkey: ECCPubKey, ord: Int) extends CA {
  def pubkey = pkey
  def ordinal = ord
}

class NoCA extends CA {
  def pubkey = new ECCPubKey(null)
  def ordinal = 0
}

trait SignedKey {
  def key: Key
  def signature: Signature
  def ca: CA
}

