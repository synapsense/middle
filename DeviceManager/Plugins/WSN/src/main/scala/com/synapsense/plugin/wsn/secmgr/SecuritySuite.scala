package com.synapsense.plugin.wsn.secmgr

import scala.collection.mutable.HashMap
import scala.collection.mutable.HashSet
import org.bouncycastle.asn1.sec.SECNamedCurves
import org.bouncycastle.asn1.x9.X9ECParameters
import org.bouncycastle.crypto.signers.ECDSASigner
import org.bouncycastle.crypto.engines.AESEngine
import org.bouncycastle.crypto.params._
import org.bouncycastle.crypto.modes.CCMBlockCipher
import org.bouncycastle.crypto.{ InvalidCipherTextException, BlockCipher }
import java.security.{ SecureRandom, MessageDigest, Security }

trait Algorithm {
  def name: String
}

trait HashAlgo {
  def apply(input: Seq[Byte]): Seq[Byte]
}

trait KDFAlgo {
  def apply(input: Seq[Byte]): (Seq[Byte], Seq[Byte])
}

trait PubKeyAlgo {
  def keySize: Int;
  def signatureSize: Int;

  def verifySignatureHash(hash: Hash, ca: CA): Boolean = {
    false;
  }
}

trait ECCAlgo {
  import BCImplicits._

  def asn_curve: X9ECParameters
  def curve = asn_curve.getCurve
  def curvePoint = asn_curve.getG
  def domainParameters = new ECDomainParameters(curve, curvePoint, asn_curve.getN, asn_curve.getH, asn_curve.getSeed)
  def decompressKey(key: ECCPubCompressedKey): ECCPubKey = ECCPubKey(curve.decodePoint(key.key.toArray).enableCompressed())
  def compressKey(key: ECCPubKey): ECCPubCompressedKey = ECCPubCompressedKey(key.point.enableCompressed().getEncoded)
  def signer(key: ECCPrivKey): ECDSASigner = {
    val signer = new ECDSASigner()
    signer.init(true, new ECPrivateKeyParameters(key.toBigInteger, domainParameters))
    signer
  }

  def verifier(key: ECCPubKey): ECDSASigner = {
    val signer = new ECDSASigner()
    signer.init(false, new ECPublicKeyParameters(key.point, domainParameters))
    signer
  }
}

trait SymmetricAlgo {
  def keySize: Int
  def engine: BlockCipher
  def nonceSize: Int
  def micSizeBits: Int

  def ccm(encrypt: Boolean, nonce: Seq[Byte], key: Key, data: Seq[Byte]): Option[Seq[Byte]] = {
    val param = new CCMParameters(new KeyParameter(key.data.toArray), micSizeBits, nonce.toArray, null)
    val mode = new CCMBlockCipher(engine)
    mode.init(encrypt, param)
    try {
      val r = mode.processPacket(data.toArray, 0, data.length)
      Some(r)
    } catch {
      case e: InvalidCipherTextException =>
        None
    }
  }
}

object AES128Algorithm extends Algorithm with SymmetricAlgo {
  def name = "AES-128"
  def keySize = 128
  def micSizeBits = 32
  def nonceSize = 13
  def engine = new AESEngine()
}

object ECCSecp256k1Algorithm extends Algorithm with PubKeyAlgo with ECCAlgo {
  def name = "ECC-256-secp256k1"
  def keySize = 256
  def signatureSize = keySize * 2
  def asn_curve = SECNamedCurves.getByName("secp256k1")
}

object ECCSecp128k1Algorithm extends Algorithm with PubKeyAlgo with ECCAlgo {
  def name = "ECC-128-secp128k1"
  def keySize = 128
  def signatureSize = keySize * 2
  def asn_curve = SECNamedCurves.getByName("secp128k1")
}

object SHA256Algorithm extends Algorithm with HashAlgo {
  def name = "SHA256"
  def apply(data: Seq[Byte]): Seq[Byte] = {
    val md = MessageDigest.getInstance("SHA-256", "BC")
    md.update(data.toArray)
    md.digest()
  }
}

object KDFWithSHA256Algorithm extends Algorithm with KDFAlgo {
  def name = "SHA256 - PKKDF#1"
  def apply(data: Seq[Byte]): (Seq[Byte], Seq[Byte]) = {
    var outdata = data
    for (i <- 0 until 100) {
      outdata = SHA256Algorithm(outdata)
    }
    outdata.splitAt(outdata.length / 2)
  }
}

trait RNG {
  def apply(n: Int): Seq[Byte]
  def bytes(n: Int): Seq[Byte]
}

object JavaSecureRNG extends RNG {
  def apply(n: Int) = bytes(n)
  def bytes(n: Int): Seq[Byte] = {
    val b = Array.fill(n) { 0.toByte }
    new SecureRandom().nextBytes(b)
    b
  }
}

trait SecuritySuite {
  def symmetric_algorithm: SymmetricAlgo
  def algorithm: ECCAlgo
  def hash: HashAlgo
  def name: String
  def identifier: Int
  def kdf: KDFAlgo
  def rng = JavaSecureRNG
}

object SecuritySuiteFull extends SecuritySuite {
  def name = "FullRev1"
  def identifier = 1
  def symmetric_algorithm = AES128Algorithm
  def algorithm = ECCSecp256k1Algorithm
  def hash = SHA256Algorithm
  def kdf = KDFWithSHA256Algorithm
}

class RestrictedSuite extends Exception {

}

object SecuritySuites {
  private val restricted_suites = new HashSet[Int]()
  private val suites = new HashMap[Int, SecuritySuite];
  Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider())
  suites += SecuritySuiteFull.identifier -> SecuritySuiteFull

  @throws(classOf[RestrictedSuite])
  def suiteByOrdinal(ordinal: Int): SecuritySuite = {
    if (!restricted_suites.contains(ordinal)) { suites(ordinal) }
    else throw new RestrictedSuite
  }

  def restrictSuite(ordinal: Int) {
    restricted_suites += ordinal
  }

  def clearRestrictions() {
    restricted_suites.clear()
  }

}
