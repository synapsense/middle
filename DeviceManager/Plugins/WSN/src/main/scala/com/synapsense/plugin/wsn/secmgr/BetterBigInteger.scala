package com.synapsense.plugin.wsn.secmgr

import scala.annotation.tailrec
import java.math.BigInteger

class BetterBigInteger(o: BigInteger) {

  @tailrec
  private[this] def extend(o: Seq[Byte], tolen: Int): Seq[Byte] = {
    if (o.length == tolen)
      o
    else if (o.length > tolen)
      extend(o.tail, tolen)
    else
      extend(Seq[Byte](0) ++ o, tolen)
  }

  /**
   * Make sure the byte array is exactly len bytes long, stripping any sign bits and padding leading 0s are required
   * @param len
   * @return
   */
  def toFixedByteArray(len: Int): Seq[Byte] = {
    val a = o.toByteArray
    extend(a, len)
  }
}

object BetterBigInteger {
  implicit def betterBigInt(o: BigInteger) = new BetterBigInteger(o)
}
