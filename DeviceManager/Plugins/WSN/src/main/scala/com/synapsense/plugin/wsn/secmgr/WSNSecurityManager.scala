package com.synapsense.plugin.wsn.secmgr

import collection.mutable.HashMap
import com.synapsense.plugin.wsn.iocore.frames.ByteReader
import org.apache.log4j.Logger
import com.synapsense.plugin.wsn.softbase29.{ PathTooLong, SendError, SoftbaseNWK, Softbase }
import com.synapsense.plugin.wsn.softbase29.service.OSSServices
import com.synapsense.plugin.wsn.msgbus.Message
import java.io.{ FileOutputStream, FileInputStream }
import java.net.InetAddress
import org.bouncycastle.jce.provider.BouncyCastleProvider
import java.math.BigInteger
import javax.crypto.spec.SecretKeySpec
import javax.crypto.Cipher
import java.security.{ MessageDigest, Security }
import java.util.{ Collection, Properties }
import java.util.concurrent._

/**
 * A collection of node types allowed to participate in security
 */
object AllowedParticipants {
  private val platforms = List(80, 82, 84, 86, 92)
  def platformAllowed(platform: Int): Boolean = platforms.contains(platform)
}

/**
 * Builds an obfuscation key for this system, based upon parameters such as a unique properties file, the host name
 * and potentially other properties.
 */
object PerSystemKeying {
  Security.addProvider(new BouncyCastleProvider)

  def hexStringToBytes(str: Seq[Char]): Seq[Byte] = {
    List.range(0, str.size, 2).map(i => str.slice(i, i + 2)).map(y => Integer.parseInt(y.mkString, 16).toByte)
  }

  lazy val hostUnique: Seq[Byte] = {
    InetAddress.getLocalHost.getHostName.map(_.toByte)
  }

  lazy val systemKey: Seq[Byte] = {
    val secProp = new Properties()
    try {
      val fis = new FileInputStream("psk.properties")
      secProp.load(fis)
      fis.close()
    } catch {
      case _ =>
      // Allow load to fail
    }

    if (!secProp.containsKey("PSK")) {
      val psk_array = Array.fill(1024) { 0.toByte }
      new java.security.SecureRandom().nextBytes(psk_array)
      secProp.setProperty("PSK", psk_array.map(_.formatted("%02X")).mkString)
      try {
        val fos = new FileOutputStream("psk.properties")
        secProp.store(fos, "NC")
        fos.close()
      } catch {
        case _ =>
      }
    }

    val str = secProp.getProperty("PSK").toList
    KDFWithSHA256Algorithm(hexStringToBytes(str) ++ hostUnique)._1
  }

}

case class NetworkKeypair(priv: ECCPrivKey, pub: ECCPubKey, sig: ECDSASignature, ca: Int)
case class ProcessMessageResult(hasData: Boolean, service: Int = -1, newdata: Array[Byte] = null)

/**
 * The core WSNSecurityManager, responsible for most of the sending and miscellaneay.
 *
 * Currently requires a Softbase29 as the interface version can't send packets ( :( )
 * @param softbase
 */
class WSNSecurityManager(softbase: Softbase) {
  def securityEnforced = { true }
  private val log = Logger.getLogger(this.getClass)
  val panid = softbase.getPanId
  private[this] val joinSessions = new HashMap[Long, JoinSession]
  private[this] val transportSessions = new HashMap[NodeShortIdentifier, TransportSessions]
  //val psk = PerSystemKeying.systemKey

  val outBoundDelay = 5
  // Delayed executor
  case class DelayedPacket(nodeSid: Char, packet: OutboundPacket, bcast: Boolean, handlers: Message) {
    override def equals(that: Any) = {
      that match {
        case o: DelayedPacket =>
          nodeSid == o.nodeSid && packet.serialize == o.packet.serialize && bcast == o.bcast && handlers == o.handlers
        case _ => false
      }
    }
  }

  val schedExec = new ScheduledThreadPoolExecutor(1)
  val outboundPipe = new java.util.concurrent.LinkedBlockingQueue[DelayedPacket]()

  val delayRun = new Runnable {
    def run() {
      val msg = outboundPipe.poll()
      if (msg != null) {
        log.info("Delay Packet left " + outboundPipe.size());
        sendDelayedPacket(msg)
      }
    }
  }
  schedExec.scheduleAtFixedRate(delayRun, 10, outBoundDelay, java.util.concurrent.TimeUnit.SECONDS)

  def close() {
    schedExec.shutdown()
  }

  /**
   * Immediately send a DelayedPacket to the network.
   * @param msg
   */
  def sendDelayedPacket(msg: DelayedPacket) {
    if (log.isDebugEnabled())
      log.debug("Sending security packet to (shortid)" + msg.nodeSid.toInt + " data " + msg.packet.serialize.toArray.map(_.formatted("%02X ")).mkString)
    try {
      softbase.nwk.send(msg.handlers, msg.packet.serialize.toArray, if (msg.bcast) 0xffff else msg.nodeSid, OSSServices.OSS_SERVICE_CSESSION,
        if (msg.bcast) SoftbaseNWK.Protocol_ToSensors else SoftbaseNWK.Protocol_ToOneSensor)
    } catch {
      case ptl: PathTooLong =>
        msg.packet.split match {
          case None =>
            log.error("Security frame was too big and won't be sent at all")
          case Some(s) =>
            log.trace("Re-sending packets as a split entity as opposed to a combined entity")
            s.foreach(ob => outboundPipe.add(DelayedPacket(msg.nodeSid, ob, msg.bcast, msg.handlers)))
        }
      case e: Throwable =>
        log.error("Problem sending data", e)
    }
  }

  /**
   * By default always load the testing keypair
   */
  private[this] var networkKeypair = NetworkKeypair(NewTestingKeys.priv, NewTestingKeys.pub,
    new ECDSASignature(Array.fill(64) { 0.toByte }), 0)

  /**
   * Clear all data for the given device
   * @param nodeSid The short identifier
   * @param nodeId The long identifier
   */
  def clearForDevice(nodeSid: Char, nodeId: Long) {
    joinSessions.remove(nodeId)
    transportSessions.remove(NodeShortIdentifier(nodeSid))
  }

  /**
   * Send a packet
   * @param nodeSid SHort ID of node
   * @param packets A sequence of OutboundPacket
   * @param bcast whether or not this is broadcast
   */
  def sendPacket(nodeSid: Char, packets: Iterable[OutboundPacket], bcast: Boolean) {
    packets.foreach({ ob =>
      val dpacket = DelayedPacket(nodeSid, ob, bcast, null)
      if (!outboundPipe.contains(dpacket))
        outboundPipe.offer(dpacket)
    })
  }

  case class JoinMessage(nodeId: Long, nodeSid: Char, pkt: JoinPacket, rcvdtime: Long)
  val JoinMessageQueue = new java.util.concurrent.LinkedBlockingDeque[JoinMessage](30)
  val JoinRunner = new Runnable {
    def run() {
      while (true) {
        val msg = JoinMessageQueue.take()
        log.info("Security join message left " + JoinMessageQueue.size());
        processJoinMessage(msg.nodeId, msg.nodeSid, msg.pkt, msg.rcvdtime)
      }
    }
  }
  new Thread(JoinRunner).start()

  def processJoinMessage(nodeId: Long, nodeSid: Char, pkt: JoinPacket, rcvdtime: Long, reenter: Boolean = false) {
    // Always remove transport sessions when in join mode
    transportSessions.remove(NodeShortIdentifier(nodeSid))

    if (joinSessions.contains(nodeId) == false) {
      joinSessions += (nodeId -> new JoinSession(networkKeypair, 1, nodeId, panid));
    }

    log.debug("Processing join message from " + nodeId.toHexString)
    val result = joinSessions(nodeId).processJoinMessage(pkt, System.currentTimeMillis() - rcvdtime, outboundPipe.size(), outBoundDelay)

    result match {
      case jf: JoinInvalidMessage => {
        log.warn("Removing join session for node " + nodeId.toHexString + " due to joininvalidmessage")
        joinSessions.remove(nodeId)
        log.warn("Reseting node security state")
        sendPacket(nodeSid, Seq(ResetSecurityState()), false)
      }
      case je: JoinExpiredSession => {
        log.warn("Removing join session for node " + nodeId.toHexString + " due to join expired")
        joinSessions.remove(nodeId)
        if (!reenter) { /* Attempt to process this again in a failed case */
          log.warn("Re-entering join session")
          processJoinMessage(nodeId, nodeSid, pkt, rcvdtime, true)
        } else {
          log.warn("Reseting node security state - no retry")
          sendPacket(nodeSid, Seq(ResetSecurityState()), false)
        }
      }
      case JoinComplete(ts, packets) => {
        log.info("Join session complete for node " + nodeId.toHexString)
        joinSessions.remove(nodeId)
        transportSessions += (NodeShortIdentifier(nodeSid) -> ts)
        packets match {
          case None => None
          case Some(y) => sendPacket(nodeSid, y, false)
        }
      }
      case JoinInProgress(phase, packets) => {
        log.debug("Join in progress message")
        packets match {
          case None => None
          case Some(y) => sendPacket(nodeSid, y, false)
        }
      }
      case jb: JoinBusy => {
        log.warn("Removing join session for node " + nodeId.toHexString + " due to busy handling other join requests")
        joinSessions.remove(nodeId)
      }
    }
  }

  def processTransportMessages(nodeId: Long, nodeSid: Char, pkt: TransportPacket): Option[(Int, Seq[Byte])] = {
    log.debug("Processing transport packet for " + nodeId.toHexString)
    pkt match {
      case si: SessionInbound =>
        transportSessions.get(NodeShortIdentifier(nodeSid)) match {
          case None =>
            log.error("No transport session for " + nodeId.toHexString)
            clearForDevice(nodeSid, nodeId)
            sendPacket(nodeSid, Seq(ResetSecurityState()), false)
            None
          case Some(ts) =>
            // Remove the session if its erroring
            if (ts.sessionErroring) {
              clearForDevice(nodeSid, nodeId)
              sendPacket(nodeSid, Seq(ResetSecurityState()), false)
              None
            } else {
              if (!ts.hasSession(si.service)) {
                sendPacket(nodeSid, Seq(ts.establishSession(si.service)), false)
                None
              } else {
                val (outbound, decoded) = ts.recv(si.service, si)
                outbound match {
                  case Some(pk) =>
                    sendPacket(nodeSid, Seq(pk), false)
                  case None => None
                }
                /* Initiate any refreshes of sessions */
                sendPacket(nodeSid, ts.refresh, false)
                decoded
              }
            }
        }
      case se: SessionRequest =>
        transportSessions.get(NodeShortIdentifier(nodeSid)) match {
          case None =>
            log.error("No transport session for " + nodeSid.toInt.toHexString)
            clearForDevice(nodeSid, nodeId)
            sendPacket(nodeSid, Seq(ResetSecurityState()), false)
            None
          case Some(ts) =>
            sendPacket(nodeSid, Seq(ts.establishSession(se.service)), false)
            None

        }
    }

  }

  @throws(classOf[SendError])
  def sendUnsecurely(message: Message, nodeSid: Char, service: Int, bcast: Boolean, data: Array[Byte]) {
    softbase.nwk.send(message, data, if (bcast) 0xFFFF else nodeSid, service, // Send non-securely
      if (bcast) SoftbaseNWK.Protocol_ToSensors else SoftbaseNWK.Protocol_ToOneSensor)
  }

  @throws(classOf[SendError])
  def sendSecurely(message: Message, nodeSid: Char, service: Int, bcast: Boolean, data: Array[Byte]) {
    // Bcast isn't supported yet, due to mixed networks. Fall back to insecure
    if (bcast) {
      sendUnsecurely(message, nodeSid, service, bcast, data)
    } else {
      transportSessions.get(NodeShortIdentifier(nodeSid)) match {
        case None => {
          log.warn("No transport session exists yet, sending data as unsecured. This won't work in a STRICT security mode.")
          sendUnsecurely(message, nodeSid, service, bcast, data)
        }
        case Some(ts) =>
          if (!ts.hasSession(service)) {
            // Start a session, but offer up the packet insecurely first
            outboundPipe.offer(DelayedPacket(nodeSid, ts.establishSession(service), bcast, null))
            log.warn("No session exists for service " + (service & 0xFF) + " sending data as unsecured. This won't work in a STRICT security mode.")
            sendUnsecurely(message, nodeSid, service, bcast, data)
          } else {
            log.trace("Dispatching to transport session")
            ts.send(service, data) match {
              case Some(ob) =>
                sendDelayedPacket(DelayedPacket(nodeSid, ob, false, message))
                log.trace("Dispatched to outbound pipe")
                /* Initiate any refreshes of sessions */
                sendPacket(nodeSid, ts.refresh, false)
              case None =>
                log.error("System didn't generate an encapsulated packet, thats broken.")

            }
          }
      }
    }

  }

  def processMessage(nodeId: Long, nodeSid: Char, message: Array[Byte]): ProcessMessageResult = {
    val parser = new SecurityMessageParser()

    log.trace(nodeId.toHexString + ":Security inbound (len " + message.length + "): " + message.map(_.formatted("%02X ")).mkString)
    try {
      parser.parseMain(new ByteReader(message, 0)) match {
        case None =>
          log.error("Packet was not a valid security frame")
          clearForDevice(nodeSid, nodeId)
          ProcessMessageResult(false)
        case Some(pkt) =>
          pkt match {
            case pkt: JoinPacket =>
              //processJoinMessage(nodeId, nodeSid, pkt)
              pkt match {
                // Prioritize phase three packet
                case p3: JoinPhaseThreePacket1 =>
                  if (JoinMessageQueue.remainingCapacity() <= 0)
                    // Remove one to enter this message
                    JoinMessageQueue.remove();
                  JoinMessageQueue.offerFirst(JoinMessage(nodeId, nodeSid, pkt, System.currentTimeMillis()))
                case _ =>
                  JoinMessageQueue.offer(JoinMessage(nodeId, nodeSid, pkt, System.currentTimeMillis()))
              }
              ProcessMessageResult(false)
            case pkt: TransportPacket => processTransportMessages(nodeId, nodeSid, pkt) match {
              case None =>
                ProcessMessageResult(false)
              case Some((service, array)) =>
                ProcessMessageResult(true, service, array.toArray)
            }
            case _ =>
              log.warn("No matching packet " + pkt)
              ProcessMessageResult(false)
          }
      }
    } catch {
      case t: Throwable =>
        log.error("Couldn't process security message due to exception ", t)
        ProcessMessageResult(false)

    }

  }

  /**
   * Set the network key from a public and private array length.
   *
   * This is the analog to MapSense's DeploymentLab.Security.NetworkKey
   * @param pub
   * @param hiddenPriv
   */
  def setNetworkKey(pub: Array[Byte], hiddenPriv: Array[Byte]): Boolean = {
    if (pub == null || hiddenPriv == null) {
      log.info("Null keys passed - retaining previous keys")
      false
    } else {
      import BCImplicits._
      val x = pub.slice(0, pub.length / 2)
      val y = pub.slice(pub.length / 2, pub.length)
      log.trace("Public X " + ToHexHelp.tohex(x))
      log.trace("Public Y " + ToHexHelp.tohex(y))
      val suite = SecuritySuiteFull
      val point = ECCPubKey(suite.algorithm.curve.createPoint(new BigInteger(1, x), new BigInteger(1, y), false).enableCompressed())

      if (point.point.isInfinity) {
        log.error("Public key is infinite.")
        false
      }

      if (!point.point.isCompressed) {
        log.error("Public key isn't compressed even though we insisted.")
        false
      }
      log.info("Compressed public key: " + ToHexHelp.tohex(suite.algorithm.compressKey(point).key))
      val spec = new SecretKeySpec(Array.fill(16)(0.toByte), "AES")
      val cipher = Cipher.getInstance("AES/ECB/NOPADDING")
      cipher.init(Cipher.DECRYPT_MODE, spec)
      val pt = cipher.doFinal(hiddenPriv)

      val verifiedHash = pt.slice(pt.length / 2, pt.length).toList
      val privateKey = pt.slice(0, pt.length / 2)
      //log.trace("DECODED PRIVATE KEY IS: " + ToHexHelp.tohex(privateKey))
      val digester = MessageDigest.getInstance("SHA-256")
      digester.update(privateKey)
      val calc = digester.digest().toList
      if (calc != verifiedHash) {
        log.error("Provided key does not pass verification checks - signature")
        log.trace("Calculated: " + calc.map("%02X".format(_)).mkString("-") + " Given: " + verifiedHash.map("%02X".format(_)).mkString("-"))
        false
      } else {
        log.info("Private key passed internal validation checks")
        log.trace("Calculated: " + calc.map("%02X".format(_)).mkString("-") + " Given: " + verifiedHash.map("%02X".format(_)).mkString("-"))

        networkKeypair = NetworkKeypair(ECCPrivKey(privateKey), point,
          new ECDSASignature(Array.fill(64) { 0.toByte }), 0)
        true
      }
    }
  }

}
