package com.synapsense.plugin.wsn.secmgr

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.FunSuite
import org.scalatest.matchers.ShouldMatchers

@RunWith(classOf[JUnitRunner])
class SecuritySuiteTests extends FunSuite with ShouldMatchers {
  test("always Good!") {
    assert(true == true)
  }

  test("Get security suites") {
    SecuritySuites.suiteByOrdinal(1) should be theSameInstanceAs (SecuritySuiteFull)
    val ss = SecuritySuites.suiteByOrdinal(1)
    ss.name should equal("FullRev1")
    ss.symmetric_algorithm should be theSameInstanceAs (AES128Algorithm)
    ss.algorithm should be theSameInstanceAs (ECCSecp256k1Algorithm)
  }

  test("Check sizes") {
    AES128Algorithm.keySize should equal(128)
    ECCSecp256k1Algorithm.signatureSize should equal(512)
    ECCSecp256k1Algorithm.keySize should equal(256)
    ECCSecp256k1Algorithm.name should include("ECC")
    ECCSecp128k1Algorithm.signatureSize should equal(256)
    ECCSecp128k1Algorithm.keySize should equal(128)
    ECCSecp128k1Algorithm.name should include("ECC")

  }

  test("Restrictions") {
    SecuritySuites.restrictSuite(1)
    evaluating { SecuritySuites.suiteByOrdinal(1) } should produce[RestrictedSuite]

    SecuritySuites.clearRestrictions()
    SecuritySuites.suiteByOrdinal(1) should be theSameInstanceAs (SecuritySuiteFull)
  }

  test("Hash SHA-256") {
    val ss = SecuritySuites.suiteByOrdinal(1)
    val input = Array.fill[Byte](64) { 12 }

    val result = ss.hash(input)
    val expected = List[Byte](-123, -117, 115, -42, 62, -78, 67, -97, 93, 55, 85, -2, 96, 2, -39, 65, -4, -55, -98, -102, 95, -37, 6, -5, 86, -118, 33, -1, 37, -103, 38, 47)
    result should be === (expected)
    assert(result sameElements expected);
    val result2 = ss.hash(input)
    result2 should be === (expected)
    assert(result2 sameElements expected);

    // Check that hash is not stateful

  }

  test("KDF") {
    val ss = SecuritySuites.suiteByOrdinal(1)
    val input = Array.fill[Byte](64) { 12 }
    ss.kdf(input)._1 should have length (16)
    ss.kdf(input)._2 should have length (16)
    ss.kdf(input)._1 should not equal (ss.kdf(input)._2)
    val expected = List[Byte](-31, -15, -68, -76, 23, 72, 118, 8, 78, -10, 41, -82, 19, -18, -60, 109)
    ss.kdf(input)._1 should be === (expected)
  }

  test("Point decompression") {
    val data = ECCPubCompressedKey(Array(0x02, 0x0A, 0x5B, 0x1A, 0x47, 0xCF, 0x71, 0x90, 0xFA, 0x4A, 0xFC, 0x7F,
      0x9F, 0x83, 0xA1, 0x02, 0x79, 0x77, 0xFA, 0x28, 0xDD, 0x99, 0xAD, 0x4C, 0x8F, 0x01, 0x88, 0x15, 0x81, 0x85,
      0xC0, 0xCB, 0xD9).map(_.toByte))
    SecuritySuiteFull.algorithm.decompressKey(data)

  }
}
