package com.synapsense.plugin.wsn.iocore.frames

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.FunSuite
import org.scalatest.matchers.ShouldMatchers

@RunWith(classOf[JUnitRunner])
class BinaryParsersTest extends FunSuite with ShouldMatchers {

  private class ParseMixed extends BinaryParsers {
    def head = bytes(4) ~ u1 //~ u1("Bar") //~ (( intMatch("Bar", 2, 1) ~ bytes("Barparam", 1)) ) | ( intMatch("Zoo", 3, 1) ~ bytes("Zooparam", 2)))
    def dov(input: ByteReader): Option[Int] = {
      parse(head, input) match {
        case Success(t, _) => t match {
          case _ ~ (x: Int) => Option(x)
          case _ => None
        }
        case NoSuccess(msg, _) => None
      }
    }
  }

  test("always Good!") {
    assert(true == true)
  }

  test("simpleParser") {
    val in4 = Array[Byte](0, 0, 0, 1, 1, 2, 9)
    val br4 = new ByteReader(in4, 0)

    val res = new ParseMixed().dov(br4)
    res should not be (None)
    res should be(Some(1))
  }

  private class ParseRest extends BinaryParsers {
    def all = bytes(1) ~ rest
    def parseAll(input: ByteReader): Seq[Seq[Byte]] = {
      parse(all, input) match {
        case Success(t, _) => t match {
          case x ~ y => List(x, y)
          case _ => List()
        }
        case NoSuccess(msg, _) => List()
      }
    }
  }

  test("Get rest") {
    val in4 = Array[Byte](1, 2, 3, 4, 5, 6, 7)
    val parser = new ParseRest()
    val res = parser.parseAll(new ByteReader(in4, 0))
    assert(res.length == 2);
    println(res);

    expect(true) {
      res(0).sameElements(in4.slice(0, 1))
    }

    expect(true) {
      println(res(1))

      res(1).sameElements(in4.slice(1, 7))
    }

  }

}