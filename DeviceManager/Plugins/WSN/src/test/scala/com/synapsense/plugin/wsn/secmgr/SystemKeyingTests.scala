package com.synapsense.plugin.wsn.secmgr

import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner
import org.scalatest.matchers.ShouldMatchers

@RunWith(classOf[JUnitRunner])
class SystemKeyingTests extends FunSuite with ShouldMatchers {
  test("System keying") {
    PerSystemKeying.hexStringToBytes("0001AB".toList) should equal(Seq[Byte](0, 1, 0xab.toByte))
  }

  test("Get system key twice") {
    val key1 = PerSystemKeying.systemKey
    val key2 = PerSystemKeying.systemKey
    key1 should equal(key2)
  }

  test("Host unique") {
    val hu = PerSystemKeying.hostUnique
    hu should not be ('empty)
  }
}
