package com.synapsense.plugin.wsn.secmgr

import org.scalatest.FunSuite
import org.scalatest.matchers.ShouldMatchers
import org.scalatest.junit.JUnitRunner
import org.junit.runner.RunWith

@RunWith(classOf[JUnitRunner])
class ExpiresTimeTest extends FunSuite with ShouldMatchers {

  test("In the future") {
    val ep = new ExpiresTime(noexpire = false)
    ep.fresh should equal(true)
  }

  test("Minimum time") {
    val ep = new ExpiresTime(noexpire = false)
    ep.expiresTime should be > (System.currentTimeMillis())
    ep.expiresTime should be > (System.currentTimeMillis() + ExpiresTime.minRegenPeriod)
  }

  test("Lots of expires") {
    for (i <- 1 to 10000) {
      val ep = new ExpiresTime(noexpire = false)
      ep.expiresTime should be > (System.currentTimeMillis())
      ep.expiresTime should be > (System.currentTimeMillis() + ExpiresTime.minRegenPeriod)
    }
  }

}
