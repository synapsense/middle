package com.synapsense.plugin.wsn.secmgr

import org.scalatest.FunSuite
import org.scalatest.matchers.ShouldMatchers
import java.math.BigInteger
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class BetterBigIntegerTest extends FunSuite with ShouldMatchers {

  import BetterBigInteger._
  test("Same length") {
    val bi = BigInteger.valueOf(0xFF)
    bi.toFixedByteArray(1) should have length (1)
  }

  test("Less than") {
    val bi = BigInteger.valueOf(0xFFFF)
    val bb = bi.toFixedByteArray(4)
    bb should have length (4)
    bb(0) should equal(0x00.toByte)
    bb(3) should equal(0xff.toByte)
  }

  test("More than") {
    val bi = BigInteger.valueOf(0xFFFF)
    val bb = bi.toFixedByteArray(1)
    bb should have length (1)
    bb(0) should equal(0xFF.toByte)
  }

}
