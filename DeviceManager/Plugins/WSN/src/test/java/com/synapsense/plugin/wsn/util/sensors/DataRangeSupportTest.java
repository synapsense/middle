package com.synapsense.plugin.wsn.util.sensors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.Before;
import org.junit.Test;

public class DataRangeSupportTest {

	@Before
	public void setup() {
		SensorFactory.initFactory();
	}

	@SuppressWarnings("unused")
	@Test
	public void testInstantiateFlow() {
		SensorConversionInterface ic = SensorFactory.createFromId(28);
	}

	@Test
	/**
	 * We can change the range of type 28 and 31, the flow sensor on SID2 and constellation units
	 */
	public void testExtendRange() {
		SensorConversionInterface ic = SensorFactory.createFromId(28);
		RangeConversionInterface rc = SensorFactory.getRangeConversion(ic);
		assertNotNull(rc);
		rc.setDataRanges(0.0, 100.0);

		ic = SensorFactory.createFromId(31);
		rc = SensorFactory.getRangeConversion(ic);
		assertNotNull(rc);
		rc.setDataRanges(0.0, 100.0);
		assertEquals(100, ic.convertFromRaw((char) 4000), .1);
		rc.setDataRanges(0.0, 50.0);
		assertEquals(50, ic.convertFromRaw((char) 4000), .1);
	}

	@Test
	/**
	 * We can't change the range of type 1
	 */
	public void testNonExtendRange() {

		SensorConversionInterface ic = SensorFactory.createFromId(1);
		RangeConversionInterface rc = SensorFactory.getRangeConversion(ic);
		assertNull(rc);

	}

}