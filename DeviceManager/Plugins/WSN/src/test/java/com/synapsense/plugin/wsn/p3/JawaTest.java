package com.synapsense.plugin.wsn.p3;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class JawaTest {
	@Test
	public void testPack() throws Exception {
		byte[] serializedJawa = { 1, 1, 1, 1, 2, 3, (byte) (2 << 6) | (1 << 3) | (1) };
		Jawa j = new Jawa(serializedJawa);
		assertArrayEquals(j.pack(), serializedJawa);
		assertEquals(j.getJid(), 0x01010101);
		assertEquals(j.getFeed(), 1);
		assertEquals(j.getPhase(), 1);
		assertEquals(j.getRack(), 2);
	}

	@Test
	public void testCompareTo() throws Exception {
		byte[] serializedJawa = { 1, 1, 1, 1, 2, 3, (byte) 0xff };
		Jawa j = new Jawa(serializedJawa);
		Jawa j2 = new Jawa(serializedJawa);
		assertTrue(j.equals(j2));
		assertTrue(j.compareTo(j2) == 0);
		assertEquals(j.hashCode(), j2.hashCode());
	}
}
