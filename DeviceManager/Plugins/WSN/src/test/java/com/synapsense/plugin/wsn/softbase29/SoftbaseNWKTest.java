package com.synapsense.plugin.wsn.softbase29;

import static org.junit.Assert.assertEquals;
import mockit.Mock;
import mockit.MockUp;

import org.junit.Test;

import com.synapsense.plugin.wsn.softbase29.packet.OSSDataPacket;
import com.synapsense.plugin.wsn.softbase29.packet.RadioMsg;

public class SoftbaseNWKTest {

	@Test
	public void testReceive() throws Exception {

		new MockUp<GateWay>() {
			@Mock
			String getPortname() {
				return "";
			}
		};

		new MockUp<Softbase>() {
			@Mock
			void $init() {
			}

			@Mock
			int getNetId() {
				return 0;
			}

		};

		SoftbaseNWK n = new SoftbaseNWK(new Softbase());
		OSSDataPacket dp = new OSSDataPacket(0);
		dp.nwk_service = 0x22;
		n.setApp(dp, new GateWay());
		try {
			RadioMsg rm = n.receive((byte) 0x22);
			assertEquals(rm.original_pkt, dp);
		} catch (InterruptedException e) {

		}

	}

}
