package com.synapsense.plugin.wsn.p3;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class P3CRC32Test {
	@Test
	public void testUpdate() throws Exception {
		P3CRC32 crc = new P3CRC32();
		assertEquals(0xffffffff, crc.getValue());
		crc = new P3CRC32();
		String input = "123456789";
		for (int b : input.getBytes()) {
			crc.update(b & 0xff);
		}
		assertEquals(-771566985, crc.getValue());
	}
}
