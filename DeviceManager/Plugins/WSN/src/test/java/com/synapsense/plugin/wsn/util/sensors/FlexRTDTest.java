package com.synapsense.plugin.wsn.util.sensors;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class FlexRTDTest {

	@Before
	public void setUp() throws Exception {
		SensorFactory.initFactory();
	}

	@Test
	public void testConvertFromRaw() {
		SensorConversionInterface sci = SensorFactory.createFromId(33);
		RangeConversionInterface rci = SensorFactory.getRangeConversion(sci);
		rci.setDataRanges(32, 86);
		assertEquals("Max", sci.convertFromRaw((char) 4000), 86, 1.0);
		assertEquals("Min", sci.convertFromRaw((char) 800), 32, 1.0);
	}

}
