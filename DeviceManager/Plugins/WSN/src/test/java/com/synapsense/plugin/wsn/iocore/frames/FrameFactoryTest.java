package com.synapsense.plugin.wsn.iocore.frames;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class FrameFactoryTest {
	@Test
	public void testCreateFromId() throws Exception {
		byte[] protoBytes = { (byte) 0xbe, 10, (byte) 254, (byte) 0xef };
		MoteUARTFrame protoFrame = new MoteUARTFrame(protoBytes, "Who knows");
		MoteUARTFrame ackFrame = FrameFactory.createFromId(254, 29, protoFrame);
		assertTrue(ackFrame instanceof MoteUARTMsgAckFrame);
		assertTrue(FrameFactory.createFromId(77777, 29, protoFrame) == null);
	}
}
