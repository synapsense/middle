package com.synapsense.plugin.wsn.util.sensors;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

public class ThermistorOutput {

	@Before
	public void setUp() throws Exception {

	}

	@Test
	public void testConvertFromRaw() {
		System.out.println("THERMANODE1:");
		char value = 0;

		for (value = 0; value <= 4095; value++) {
			ThermanodeThermistor therm = new ThermanodeThermistor();
			double val = therm.convertFromRaw(value);
			// System.out.println((int)value + "," + val);

			if (value < 367) {
				assertTrue(val == -3000);
			} else {
				assertTrue(val >= -10);
			}
		}
	}

	@Test
	public void testConvertFromRaw2() {
		System.out.println("THERMANODE2:");
		char value = 0;

		for (value = 0; value <= 4095; value++) {
			Thermanode2Thermistor therm = new Thermanode2Thermistor();
			double val = therm.convertFromRaw(value);
			// System.out.println((int)value + "," + val);

			if (value < 69) {
				assertTrue(val == -3000);
			} else {
				assertTrue(val >= 20);
			}
		}
	}

}
