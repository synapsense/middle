package com.synapsense.plugin.wsn.p3;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class RackTest {

	@Test
	public void testPack() throws Exception {
		byte[] r = { 1, 2, 3 };
		Rack r2 = new Rack(r);
		assertEquals(r2.getHeight(), r[1]);
		assertEquals(r2.getRack(), r[0]);
		assertEquals(r2.getRpdudelta(), r[2]);
	}

	@Test
	public void testCompareTo() throws Exception {
		byte[] data1 = { 1, 2, 3 };
		byte[] data2 = { 3, 4, 5 };
		assert (new Rack(data1).compareTo(new Rack(data2)) != 0);
		assert (new Rack(data1).compareTo(new Rack(data1)) == 0);
	}
}
