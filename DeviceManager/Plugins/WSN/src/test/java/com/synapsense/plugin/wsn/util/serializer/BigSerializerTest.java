package com.synapsense.plugin.wsn.util.serializer;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class BigSerializerTest {

	BigSerializer serial;

	@Before
	public void setUp() throws Exception {

	}

	@Test
	public void testSerializer() {
		byte[] foo = new byte[100];
		BigSerializer s = new BigSerializer(foo);
		assertEquals(foo, s.getData());

	}

	@Test
	public void testUnpackU_INT8() {
		byte[] test = new byte[4];
		test[0] = 100;
		test[1] = (byte) 130;
		test[2] = 1;
		test[3] = (byte) 255;

		serial = new BigSerializer(test);
		assertEquals(100, serial.unpackU_INT8());
		assertEquals(130, serial.unpackU_INT8());
		assertEquals(1, serial.unpackU_INT8());
		assertEquals(255, serial.unpackU_INT8());
	}

	@Test
	public void testPackU_INT8() {
		byte[] test = new byte[4];

		serial = new BigSerializer(test);
		serial.packU_INT8((byte) 100);
		serial.packU_INT8((byte) 129);
		serial.packU_INT8((byte) 255);
		serial.packU_INT8((byte) 0);

		serial = new BigSerializer(test);

		assertEquals(100, serial.unpackU_INT8());
		assertEquals(129, serial.unpackU_INT8());
		assertEquals(255, serial.unpackU_INT8());
		assertEquals(0, serial.unpackU_INT8());
	}

	@Test
	public void testUnpackU_INT16() {
		byte[] test = new byte[4];
		test[0] = 0x21;
		test[1] = (byte) 0xff;

		test[2] = (byte) 0x43;
		test[3] = (byte) 0xff;

		serial = new BigSerializer(test);
		assertEquals(8703, serial.unpackU_INT16());
		assertEquals((int) 17407, (int) serial.unpackU_INT16());

	}

	@Test
	public void testPackU_INT16() {
		byte[] test = new byte[4];

		serial = new BigSerializer(test);
		serial.packU_INT16((char) 0xfff0);
		serial.packU_INT16((char) 17407);

		serial = new BigSerializer(test);

		assertEquals(0xfff0, serial.unpackU_INT16());
		assertEquals(0x43ff, serial.unpackU_INT16());

	}

	@Test
	public void testUnpackU_INT32() {
		byte[] test = new byte[8];
		test[0] = 0x21;
		test[1] = (byte) 0x32;
		test[2] = (byte) 0x45;
		test[3] = (byte) 0xFA;

		test[4] = (byte) 0xFF;
		test[5] = (byte) 0x00;
		test[6] = (byte) 0x01;
		test[7] = (byte) 0x01;

		serial = new BigSerializer(test);
		assertEquals(556942842, serial.unpackU_INT32());
		assertEquals(4278190337L, (long) serial.unpackU_INT32());

	}

	@Test
	public void testPackU_INT32() {
		byte[] test = new byte[8];

		serial = new BigSerializer(test);
		serial.packU_INT32((long) 655329392L);
		serial.packU_INT32((long) 19328);

		serial = new BigSerializer(test);

		assertEquals(655329392L, serial.unpackU_INT32());
		assertEquals(19328, serial.unpackU_INT32());
	}

	@Test
	public void testUnpack_INT64() {
		byte[] test = new byte[8];
		test[0] = (byte) 0x21;
		test[1] = (byte) 0x32;
		test[2] = (byte) 0x45;
		test[3] = (byte) 0xFA;

		test[4] = (byte) 0xFF;
		test[5] = (byte) 0x00;
		test[6] = (byte) 0x01;
		test[7] = (byte) 0x01;

		serial = new BigSerializer(test);
		assertEquals(0x213245faff000101L, serial.unpack_INT64());
	}

	@Test
	public void testPack_INT64() {
		byte[] test = new byte[8];

		serial = new BigSerializer(test);
		serial.pack_INT64((long) 65392532939992L);

		serial = new BigSerializer(test);

		assertEquals(65392532939992L, serial.unpack_INT64());

	}
}
