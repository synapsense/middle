package com.synapsense.plugin.wsn.util.timesync;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class TimeSyncTest {

	private TimeSync times;

	@Before
	public void setUp() throws Exception {

	}

	@Test
	public void testSimple() {
		times = new TimeSync();
		long system = 100;
		long network = 1;

		times.processMsg(network, system);

		assertEquals("Start", new TimeSyncTime(true, system), times.convertTime(network, system));

		system += 2 * 1000; // 2 seconds
		network += 2 * 32768; // 2 seconds;

		times.processMsg(network, system);
		assertEquals("2 seconds", new TimeSyncTime(true, system), times.convertTime(network, system));

		system += 2 * 1000; // 2 seconds forward in the system, for the same
							// timestamp

		assertEquals("+2 seconds system", new TimeSyncTime(true, system - 2 * 1000), times.convertTime(network, system));

	}

	@Test
	public void testWrap() {
		times = new TimeSync();
		long system = 100;
		long network = 0xffffffffL;

		times.processMsg(network, system);
		assertEquals("Start", new TimeSyncTime(true, system), times.convertTime(network, system));
		network += 32768 * 100;
		network = network & 0xffffffffL;
		system += 1000 * 100;
		assertEquals("Wrap", new TimeSyncTime(true, system), times.convertTime(network, system));
	}

	@Test
	public void testReset() {
		times = new TimeSync();
		long system = 1000;
		long network = 32768 * 1000;
		times.processMsg(network, system);
		network = 500;
		times.processMsg(network, system);
		assertEquals("Reset", new TimeSyncTime(false, system), times.convertTime(network, system));

	}

	@Test
	public void testNoReset() {
		times = new TimeSync();
		long system = 1000;
		long network = TimeSync.generationCutoff + 10;
		times.processMsg(network, system);
		network = 500;
		system = 2000;
		times.processMsg(network, system);
		assertEquals("Reset", new TimeSyncTime(true, system), times.convertTime(network, system));

	}

	@Test
	public void testZero() {
		times = new TimeSync();
		long system = 1000;
		long network = 0;
		times.processMsg(network, system);
		network = 0;
		system = 2000;
		assertEquals("Reset", new TimeSyncTime(false, system), times.convertTime(network, system));

	}

	@Test
	public void testFuture() {
		times = new TimeSync();
		long system = 1000;
		long network = 0;
		times.processMsg(network, system);
		network = 999999999;
		assertEquals("Go into the future", new TimeSyncTime(false, system), times.convertTime(network, system));

	}

}
