package com.synapsense.plugin.wsn.secmgr

import org.scalatest.FunSuite
import org.scalatest.matchers.ShouldMatchers
import org.scalatest.junit.JUnitRunner
import org.junit.runner.RunWith
import com.synapsense.plugin.wsn.iocore.frames.ByteReader
import java.math.BigInteger


/**
 * TEST DATA
 * Ephemeral Device Key
 *
 * read EC key
Private-Key: (256 bit)
priv:
    00:81:9f:41:5c:0c:4d:8c:5f:9d:87:d5:04:1e:23:
    47:e8:a8:e4:d6:b3:86:06:56:ac:f7:9f:be:5a:af:
    b9:c3:17
pub:
    03:df:37:3c:54:d8:8d:f9:ec:6a:c2:b1:57:f6:e4:
    1d:6d:27:16:f9:1e:37:ef:43:7e:50:d9:f5:f9:8e:
    8d:95:24
ASN1 OID: secp256k1
*
 *
 * Static Device Key
 *
 * priv:
    00:a9:39:8a:2a:d2:7f:97:4f:20:c2:18:e8:9f:22:
    88:33:02:7e:6a:ad:ed:ec:89:7b:d9:19:44:39:a1:
    81:92:1a
pub: 
    03:86:61:39:48:8a:50:2a:b6:dc:bb:8b:a9:3d:58:
    ca:33:53:f9:80:de:5f:67:ad:de:a9:0a:ec:25:6f:
    15:76:20
ASN1 OID: secp256k1
 */

@RunWith(classOf[JUnitRunner])
class JoinSessionTest extends FunSuite with ShouldMatchers {
	val networkKeypair = NetworkKeypair(TestingKeys.ecNetworkPrivate, TestingKeys.ecNetworkPublic,
		new ECDSASignature(Array.fill(64) { 0.toByte }), 0)

	val test_device = 0x00336c0000000000L
	val base_nonce = Seq(0,0,0,0,0,0,0,0,0,0,0x6c,0x33,0).map(_.toByte)
	val one_block = Array.fill(16){0.toByte}
	val test_device_nli = NodeLongIdentifier(test_device)
	val pan = 0x1212

	val edpub_test = new ECCPubCompressedKey(Array(0x03, 0xdf, 0x37, 0x3c, 0x54, 0xd8, 0x8d, 0xf9, 0xec, 0x6a, 0xc2, 0xb1, 0x57, 0xf6, 0xe4,
		0x1d, 0x6d, 0x27, 0x16, 0xf9, 0x1e, 0x37, 0xef, 0x43, 0x7e, 0x50, 0xd9, 0xf5, 0xf9, 0x8e, 0x8d, 0x95, 0x24).map(_.toByte))

	val dpub_test = new ECCPubCompressedKey(Array(
		0x03, 0x86, 0x61, 0x39, 0x48, 0x8a, 0x50, 0x2a, 0xb6, 0xdc, 0xbb, 0x8b, 0xa9, 0x3d, 0x58, 0xca, 0x33, 0x53, 0xf9, 0x80, 0xde, 0x5f, 0x67, 0xad, 0xde, 0xa9, 0x0a, 0xec, 0x25, 0x6f, 0x15, 0x76, 0x20
	).map(_.toByte))

	val sym_key = new SymmetricKey(Array[Byte](0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15))

	test("Check initial join") {
		val join = new JoinSession(networkKeypair, 1, test_device, pan)
		val p1p1 = JoinPhaseOnePacket1(NodeLongIdentifier(test_device), edpub_test, dpub_test)
		join.processJoinMessage(p1p1, 0, 0, 5) should equal(JoinInProgress(1, None))
		val p1p2 = JoinPhaseOnePacket2(100, new ECDSASignature(Array.fill(64) { 0.toByte } ))
		val resp = join.processJoinMessage(p1p2, 0, 0, 5)
		resp should not equal (JoinInProgress(1, None))

	}


	test("Parser") {
		val pkt = Array(0x01, 0x0A,0xFF,0xA5, 0xA5, 0xA5, 0xA5, 0xA5, 0xA5, 0xA5,
			0x0A, 0x01, 0x00, 0x99, 0x52, 0x6C, 0x33, 0x00, 0xA5, 0xA5, 0xA5, 0xA5,
			0xa5, 0xA5, 0xA5, 0xA5, 0xA5, 0xA5, 0xA5, 0xA5, 0xA5, 0xA5, 0xA5, 0xA5, 0xA5, 0xA5,
			0xA5, 0xA5, 0xA5, 0xA5, 0xA5, 0xA5, 0xA5, 0x02, 0x5A, 0x6C, 0xFF, 0x21, 0x54, 0xB3, 0x41, 0xC9, 0x35,
			0xC5, 0x0A, 0x11, 0x40, 0x3F, 0xEA, 0x4A, 0x4C, 0x02, 0xBB, 0xC1, 0xA9, 0x02, 0xB7, 0x6A,
			0x32, 0x83, 0x42, 0x4E, 0xCF, 0x2D, 0xC1, 0x18).map(_.toByte)

		val parser = new SecurityMessageParser()

		val res = parser.parseMain(new ByteReader(pkt, 0))
		res should not be (None)

	}

	test("Session request parsing") {
		val pkt = Array(0x01, 0x15, 0xD5).map(_.toByte)
		val parser = new SecurityMessageParser()
		val res = parser.parseMain(new ByteReader(pkt, 0))

		res should not be (None)
		res should equal (Some(SessionRequest(0xd5)))

	}

	test("Build nonce") {
		val ts = new TransportSession(test_device_nli, SecuritySuiteFull, ServiceCode(1), sym_key)
		ts.buildNonce(0, test_device_nli) should equal (base_nonce)
	}

	test("CCM loopback") {
		val ts = new TransportSession(test_device_nli, SecuritySuiteFull, ServiceCode(1), sym_key)
		val ccm_encrypt = SecuritySuiteFull.symmetric_algorithm.ccm(true, base_nonce, sym_key, one_block).get
		println(ccm_encrypt)
		val ccm_verify = SecuritySuiteFull.symmetric_algorithm.ccm(false, base_nonce, sym_key, ccm_encrypt)
		println(ccm_verify)
		one_block.toList should equal (ccm_verify.get.toList) // Call toList to compare seqs, where == overrides
	}
}
