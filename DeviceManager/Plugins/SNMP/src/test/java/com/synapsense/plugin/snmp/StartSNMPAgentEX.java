package com.synapsense.plugin.snmp;

import org.snmp4j.agent.DuplicateRegistrationException;

public class StartSNMPAgentEX {

	public static void main(String[] arqs) throws InterruptedException {
		Thread thr = new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					SNMPTestAgentEX.startAgent();

				} catch (DuplicateRegistrationException e) {
					e.printStackTrace();
				}

			}
		});
		thr.setDaemon(true);
		thr.start();
		Thread.sleep(1000000);
		thr.interrupt();
	}
}