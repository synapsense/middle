package com.synapsense.plugin.snmp;

import java.io.IOException;
import java.io.Serializable;

import org.apache.log4j.Logger;

import com.synapsense.plugin.apinew.Transport;

public class TestTransportEX implements Transport {
	private final static Logger logger = Logger.getLogger(TestTransportEX.class);

	@Override
	public void connect() throws IOException {
	}

	@Override
	public int getPriority() {
		return 0;
	}

	@Override
	public void sendData(String objectID, String propertyName, Serializable value) {
		logger.debug("Transport message:\n\tObject ID: " + objectID + "\n\tProperty name:" + propertyName
		        + "\n\tValue:" + value);

	}

	@Override
	public void sendOrderedData(String objectID, String propertyName, Serializable value) {
		sendData(objectID, propertyName, value);
	}
}
