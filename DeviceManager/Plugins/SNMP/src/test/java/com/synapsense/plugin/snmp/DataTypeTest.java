package com.synapsense.plugin.snmp;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.snmp4j.smi.Counter32;
import org.snmp4j.smi.Counter64;
import org.snmp4j.smi.Gauge32;
import org.snmp4j.smi.Integer32;
import org.snmp4j.smi.IpAddress;
import org.snmp4j.smi.OID;
import org.snmp4j.smi.OctetString;
import org.snmp4j.smi.TimeTicks;
import org.snmp4j.smi.UnsignedInteger32;
import org.snmp4j.smi.Variable;

import com.synapsense.plugin.api.exceptions.WrongTypeException;

public class DataTypeTest {
	DataType dataType;

	@Before
	public void beforeTest() {

	}

	@SuppressWarnings("static-access")
	@Test
	public void testGetType() {
		assertEquals(dataType.Counter32, dataType.getType("counter32"));

	}

	@SuppressWarnings("static-access")
	@Test(expected = IllegalArgumentException.class)
	public void testGetTypeIlligalArgument() {
		dataType = dataType.getType("NullType");

	}

	@SuppressWarnings("static-access")
	@Test(expected = WrongTypeException.class)
	public void testConvertTypeWithNullValue() throws WrongTypeException {
		dataType.convertType(null, dataType);
	}

	@SuppressWarnings("static-access")
	@Test(expected = WrongTypeException.class)
	public void testConvertTypeWithIncorrectIp() throws WrongTypeException {
		DataType type = null;
		type = type.getType("IpAddress");
		dataType.convertType("300.168.1.1", type);
	}

	@SuppressWarnings("static-access")
	@Test
	public void testConvertType() throws WrongTypeException {
		Variable variable;
		DataType type = null;
		type = type.getType("Counter32");
		variable = new Counter32((long) 112342522);
		assertEquals(variable, dataType.convertType("112342522", type));

		type = type.getType("counter64");
		variable = new Counter64((long) 112342522);
		assertEquals(variable, dataType.convertType("112342522", type));

		type = type.getType("Gauge32");
		variable = new Gauge32((long) 112342522);
		assertEquals(variable, dataType.convertType("112342522", type));

		type = type.getType("Integer32");
		variable = new Integer32(112342522);
		assertEquals(variable, dataType.convertType("112342522", type));

		type = type.getType("IpAddress");
		variable = new IpAddress("192.168.1.1");
		assertEquals(variable, dataType.convertType("192.168.1.1", type));

		type = type.getType("OctetString");
		variable = new OctetString("OctetString");
		assertEquals(variable, dataType.convertType("OctetString", type));

		type = type.getType("OID");
		variable = new OID("1.3.6.2.1.5.104.97.108.108.111.1");
		assertEquals(variable, dataType.convertType("1.3.6.2.1.5.104.97.108.108.111.1", type));

		type = type.getType("TimeTicks");
		variable = new TimeTicks((long) 112342522);
		assertEquals(variable, dataType.convertType("112342522", type));

		type = type.getType("UnsignedInteger32");
		variable = new UnsignedInteger32((long) 112342522);
		assertEquals(variable, dataType.convertType("112342522", type));
	}

}
