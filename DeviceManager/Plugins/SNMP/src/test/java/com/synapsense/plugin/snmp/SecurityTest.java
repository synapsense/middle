package com.synapsense.plugin.snmp;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;
import org.snmp4j.smi.OID;

public class SecurityTest {
	Security security;

	@Before
	public void beforeTest() {
		security = new Security();
	}

	@SuppressWarnings("static-access")
	@Test
	public void testGetAuthProtocol() {
		OID oid = security.getAuthProtocol("SHA");
		assertNotNull(oid);
	}

	@SuppressWarnings("static-access")
	@Test(expected = IllegalArgumentException.class)
	public void testGetAuthProtocolWithException() {
		security.getAuthProtocol("MNS");
	}

	@SuppressWarnings("static-access")
	@Test(expected = IllegalArgumentException.class)
	public void testGetAuthProtocolWithException2() {
		security.getAuthProtocol(null);
	}

	@SuppressWarnings("static-access")
	@Test
	public void testGetPrivacy() {
		OID oid = security.getPrivacy("des");
		assertNotNull(oid);
	}

	@SuppressWarnings("static-access")
	@Test(expected = IllegalArgumentException.class)
	public void testGetPrivacyWithException() {
		@SuppressWarnings("unused")
		OID oid = security.getPrivacy("SHA");
	}

	@SuppressWarnings("static-access")
	@Test(expected = IllegalArgumentException.class)
	public void testGetPrivacyWithException2() {
		@SuppressWarnings("unused")
		OID oid = security.getPrivacy(null);
	}

	@SuppressWarnings("static-access")
	@Test
	public void testGetLevel() {
		int level = security.getLevel("auThpRiv");
		assertEquals(3, level);
		level = security.getLevel("auTH");
		assertEquals(2, level);
		level = security.getLevel("");
		assertEquals(1, level);

	}

	@SuppressWarnings("static-access")
	@Test(expected = IllegalArgumentException.class)
	public void testGetLevelWithException() {
		@SuppressWarnings("unused")
		int level = security.getLevel("auRiv");
	}

	@SuppressWarnings("static-access")
	@Test(expected = IllegalArgumentException.class)
	public void testGetLevelWithException2() {
		@SuppressWarnings("unused")
		int level = security.getLevel(null);
	}

}
