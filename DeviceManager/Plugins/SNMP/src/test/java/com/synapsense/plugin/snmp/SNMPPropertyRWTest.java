package com.synapsense.plugin.snmp;

import java.io.IOException;
import java.util.Vector;

import mockit.Mocked;
import mockit.NonStrictExpectations;

import org.junit.Test;
import org.snmp4j.MessageDispatcherImpl;
import org.snmp4j.PDU;
import org.snmp4j.Snmp;
import org.snmp4j.Target;
import org.snmp4j.event.ResponseEvent;
import org.snmp4j.smi.Variable;
import org.snmp4j.smi.VariableBinding;

import com.synapsense.plugin.apinew.impl.ObjectDescImpl;
import com.synapsense.plugin.apinew.impl.PropertyDescImpl;

import static org.junit.Assert.assertEquals;

@SuppressWarnings("ResultOfMethodCallIgnored")
public class SNMPPropertyRWTest {

	@Mocked
	Snmp snmpMock;
	@Mocked
	MessageDispatcherImpl messageDispatcherMock;
	@Mocked
	ResponseEvent responseEventMock;
	@Mocked
	PDU pduMock;
	@Mocked
	VariableBinding variableBindingMock;
	@Mocked
	Variable variableMock;

	@Test
	public void testReadProperty() throws IOException {
		ObjectDescImpl obj = new ObjectDescImpl("SNMPAGENT:2458");
		obj.addProperty(new PropertyDescImpl("privPwd", "dummy"));
		obj.addProperty(new PropertyDescImpl("secLevel", "auth"));
		obj.addProperty(new PropertyDescImpl("address", "UDP:192.168.1.2/2002"));
		obj.addProperty(new PropertyDescImpl("retries", "123"));
		obj.addProperty(new PropertyDescImpl("timeout", "2002"));
		obj.addProperty(new PropertyDescImpl("version", "V2C"));
		obj.addProperty(new PropertyDescImpl("privateCommunity", "dummy"));
		obj.addProperty(new PropertyDescImpl("publicCommunity", "dummy"));
		ObjectDescImpl childObj = new ObjectDescImpl("SNMPAGENT:2457");
		childObj.addProperty(new PropertyDescImpl("authProto", "MD5"));
		childObj.addProperty(new PropertyDescImpl("privProto", "AES128"));
		childObj.addProperty(new PropertyDescImpl("address", "UDP:192.168.1.2/2002"));
		childObj.addProperty(new PropertyDescImpl("retries", "123"));
		PropertyDescImpl prop = new PropertyDescImpl("timeout", "2002");
		prop.setParent(childObj);
		childObj.addProperty(prop);
		childObj.addProperty(new PropertyDescImpl("oid", "2.2.12.53.23.1.12"));
		obj.addChild(childObj);
		childObj.setParent(obj);

		final String expectedValue = "foo";

		new NonStrictExpectations() {{
			snmpMock.getMessageDispatcher();
			returns(messageDispatcherMock);

			snmpMock.get((PDU) any, (Target) any);
			returns(responseEventMock);

			responseEventMock.getResponse();
			returns(pduMock);

			pduMock.getVariableBindings();
			Vector<VariableBinding> variableBindings = new Vector<>();
			variableBindings.add(variableBindingMock);
			returns(variableBindings);

			variableBindingMock.getVariable();
			returns(variableMock);

			variableMock.toString();
			returns(expectedValue);
		}};

		assertEquals(expectedValue, SNMPPropertyRW.readProperty(prop, new SNMPConnection(childObj)));
	}

	@Test
	public void testWriteProperty() throws IOException {
		ObjectDescImpl obj = new ObjectDescImpl("SNMPAGENT:2458");
		obj.addProperty(new PropertyDescImpl("privPwd", "dummy"));
		obj.addProperty(new PropertyDescImpl("secLevel", "auth"));
		obj.addProperty(new PropertyDescImpl("address", "UDP:192.168.1.2/2002"));
		obj.addProperty(new PropertyDescImpl("retries", "123"));
		obj.addProperty(new PropertyDescImpl("timeout", "2002"));
		obj.addProperty(new PropertyDescImpl("version", "V2C"));
		obj.addProperty(new PropertyDescImpl("privateCommunity", "dummy"));
		obj.addProperty(new PropertyDescImpl("publicCommunity", "dummy"));
		ObjectDescImpl childObj = new ObjectDescImpl("SNMPAGENT:2457");
		childObj.addProperty(new PropertyDescImpl("authProto", "MD5"));
		childObj.addProperty(new PropertyDescImpl("privProto", "AES128"));
		childObj.addProperty(new PropertyDescImpl("address", "UDP:192.168.1.2/2002"));
		childObj.addProperty(new PropertyDescImpl("retries", "123"));
		childObj.addProperty(new PropertyDescImpl("type", "Integer32"));
		PropertyDescImpl prop = new PropertyDescImpl("timeout", "2002");
		prop.setParent(childObj);
		childObj.addProperty(prop);
		childObj.addProperty(new PropertyDescImpl("oid", "2.2.12.53.23.1.12"));
		obj.addChild(childObj);
		childObj.setParent(obj);

		new NonStrictExpectations() {{
			snmpMock.getMessageDispatcher();
			returns(messageDispatcherMock);

			snmpMock.set((PDU) any, (Target) any);
			returns(responseEventMock);

			responseEventMock.getResponse();
			returns(pduMock);

			pduMock.getVariableBindings();
			Vector<VariableBinding> variableBindings = new Vector<>();
			variableBindings.add(variableBindingMock);
			returns(variableBindings);

			variableBindingMock.getVariable();
			returns(variableMock);
		}};

		SNMPPropertyRW.writeProperty(prop, new SNMPConnection(childObj), "1001");
	}
}
