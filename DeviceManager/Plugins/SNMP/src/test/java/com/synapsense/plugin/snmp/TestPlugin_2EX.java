package com.synapsense.plugin.snmp;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.synapsense.plugin.api.exceptions.ConfigPluginException;
import com.synapsense.plugin.api.exceptions.IncorrectStateException;
import com.synapsense.plugin.apinew.Transport;
import com.synapsense.plugin.apinew.impl.ObjectDescImpl;
import com.synapsense.plugin.apinew.impl.PropertyDescImpl;
import com.synapsense.plugin.configuration.PluginConfigImpl;

public class TestPlugin_2EX {
	private final static Logger logger = Logger.getLogger(TestPlugin_2EX.class);
	private final static String FILE_SEPARATOR = System.getProperty("file.separator");

	public static PluginConfigImpl configue(File file) throws ParserConfigurationException, SAXException, IOException {
		ObjectDescImpl obj = new ObjectDescImpl("SNMPAGENT:2");
		ObjectDescImpl objChild = new ObjectDescImpl("SNMPOBJECT:4");
		PluginConfigImpl config = new PluginConfigImpl("SNMP");
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db;
		System.out.println("*Start parse XML*");
		db = dbf.newDocumentBuilder();
		Document doc = db.parse(file);
		doc.getDocumentElement().normalize();

		obj.addChild(objChild);
		objChild.setParent(obj);

		NodeList propertyList = doc.getElementsByTagName("prop");
		for (int i = 0; i < propertyList.getLength(); i++) {
			Node property = propertyList.item(i);
			String parentID = property.getParentNode().getAttributes().getNamedItem("id").toString();
			parentID = parentID.substring(parentID.indexOf("\"") + 1, parentID.lastIndexOf("\""));
			String name = property.getAttributes().getNamedItem("name").toString();
			name = name.substring(name.indexOf("\"") + 1, name.lastIndexOf("\""));
			String value = property.getAttributes().getNamedItem("value").toString();
			value = value.substring(value.indexOf("\"") + 1, value.lastIndexOf("\""));

			PropertyDescImpl propertyDesc = new PropertyDescImpl(name, value);
			System.out.println(propertyDesc);
			if (parentID.equals("SNMPAGENT:2")) {
				obj.addProperty(propertyDesc);
				propertyDesc.setParent(obj);
			}
			if (parentID.equals("SNMPOBJECT:4")) {
				objChild.addProperty(propertyDesc);
				propertyDesc.setParent(objChild);
			}
		}
		config.addObject(obj);
		config.addObject(objChild);
		config.addRoot(obj);
		System.out.println(config.getObject("SNMPAGENT:2").getProperties());
		System.out.println(config.getObject("SNMPOBJECT:4").getProperties());
		System.out.println(config.getRoots());
		System.out.println("*Finish parse XML*");
		return config;
	}

	/**
	 * @param args
	 */
	@Test
	public void Test() {
		PropertyConfigurator.configure("lib/log4j.properties");
		Transport tt = new TestTransportEX();
		SNMPPlugin dbdf = new SNMPPlugin();
		File file = new File("test" + FILE_SEPARATOR + "com" + FILE_SEPARATOR + "synapsense" + FILE_SEPARATOR
		        + "plugin" + FILE_SEPARATOR + "snmp" + FILE_SEPARATOR + "SNMPDevice.xml");
		File newFile = new File("test" + FILE_SEPARATOR + "com" + FILE_SEPARATOR + "synapsense" + FILE_SEPARATOR
		        + "plugin" + FILE_SEPARATOR + "snmp" + FILE_SEPARATOR + "NewSNMPDevice.xml");
		try {
			PluginConfigImpl config = configue(file);
			PluginConfigImpl confign1 = configue(newFile);
			tt.connect();
			dbdf.setTransport(tt);
			dbdf.configure(config);
			dbdf.start();
			dbdf.configure(confign1);
			Thread.sleep(3000);

		} catch (ParserConfigurationException | SAXException | ConfigPluginException | IOException | InterruptedException | IncorrectStateException e) {
			logger.error(e.getLocalizedMessage(), e);
		}

    }
}
