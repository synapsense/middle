package com.synapsense.plugin.snmp;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import mockit.Mocked;
import mockit.NonStrictExpectations;

import org.junit.Test;

import com.synapsense.plugin.api.exceptions.ConfigPluginException;
import com.synapsense.plugin.api.exceptions.IncorrectStateException;
import com.synapsense.plugin.apinew.PropertyDesc;
import com.synapsense.plugin.apinew.Transport;
import com.synapsense.plugin.apinew.impl.ObjectDescImpl;
import com.synapsense.plugin.apinew.impl.PropertyDescImpl;
import com.synapsense.plugin.configuration.PluginConfigImpl;

public class SNMPPluginTest {
	SNMPPlugin plugin;
	ObjectDescImpl obj;
	ObjectDescImpl objChild;
	PropertyDescImpl prop;
	PluginConfigImpl config;

	@Mocked
	SNMPPropertyRW propertyRW;

	@Test
	public void testPlugin() throws ConfigPluginException, IncorrectStateException, IOException {

		obj = new ObjectDescImpl("SNMPAGENT:2458");
		obj.addProperty(new PropertyDescImpl("address", "UDP:192.168.1.2/2002"));
		obj.addProperty(new PropertyDescImpl("version", "V1"));
		obj.addProperty(new PropertyDescImpl("privPwd", "dummy"));
		obj.addProperty(new PropertyDescImpl("secLevel", "auth"));
		obj.addProperty(new PropertyDescImpl("retries", "123"));
		obj.addProperty(new PropertyDescImpl("timeout", "2002"));
		obj.addProperty(new PropertyDescImpl("privateCommunity", "dummy"));
		obj.addProperty(new PropertyDescImpl("publicCommunity", "dummy"));

		objChild = new ObjectDescImpl("SNMPAGENT:2457");
		final PropertyDescImpl prop1 = new PropertyDescImpl("retries", "123");
		PropertyDescImpl prop2 = new PropertyDescImpl("timeout", "2002");
		PropertyDescImpl prop3 = new PropertyDescImpl("oid", "2.2.12.53.23.1.12");
		PropertyDescImpl prop4 = new PropertyDescImpl("authProto", "MD5");
		PropertyDescImpl prop5 = new PropertyDescImpl("privProto", "AES128");
		prop1.setParent(objChild);
		prop2.setParent(objChild);
		prop3.setParent(objChild);
		prop4.setParent(objChild);
		prop5.setParent(objChild);
		objChild.addProperty(prop1);
		objChild.addProperty(prop2);
		objChild.addProperty(prop3);
		objChild.addProperty(prop4);
		objChild.addProperty(prop5);

		obj.addChild(objChild);
		objChild.setParent(obj);

		plugin = new SNMPPlugin();
		config = new PluginConfigImpl("snmp");
		config.addRoot(obj);
		config.addObject(obj);
		config.addObject(objChild);

		plugin.configure(config);
		plugin.start();

		List<String> propertyNamesObjChild = new ArrayList<String>();
		propertyNamesObjChild.add("privProto");
		List<Serializable> valuesObjChild = new ArrayList<Serializable>();
		valuesObjChild.add("AES128");

		final PropertyDesc pd = new PropertyDescImpl("retries", "123");
		final SNMPConnection connection = new SNMPConnection(objChild);

		new NonStrictExpectations() {
			{

				result = SNMPPropertyRW.readProperty(pd, connection);
				returns(result = "123");
			}
		};

		plugin.setPropertyValue(objChild, "retries", "123");
		plugin.setPropertyValue(objChild, propertyNamesObjChild, valuesObjChild);
		plugin.getPropertyValue(objChild, "retries");
		plugin.getPropertyValue(objChild, propertyNamesObjChild);

		plugin.stop();
	}

	@Test(expected = IncorrectStateException.class)
	public void testPluginWithException() throws IncorrectStateException {
		plugin = new SNMPPlugin();
		plugin.start();
	}

	@Test
	public void otherTest() {
		plugin = new SNMPPlugin();
		assertEquals("SNMP", plugin.getID());
		assertEquals("{ROOT_TYPES=SNMPAGENT, PLUGIN_ID=SNMP}", plugin.getPluginDescription().toString());
		Transport transport = new TestTransportEX();
		plugin.setTransport(transport);
	}
}
