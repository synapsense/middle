package com.synapsense.plugin.snmp;

import com.synapsense.plugin.apinew.impl.ObjectDescImpl;
import com.synapsense.plugin.apinew.impl.PropertyDescImpl;
import java.io.IOException;
import java.net.UnknownHostException;
import org.junit.Before;
import org.junit.Test;
import org.snmp4j.mp.MPv3;
import org.snmp4j.security.SecurityModels;
import org.snmp4j.security.SecurityProtocols;
import org.snmp4j.security.USM;
import org.snmp4j.smi.OctetString;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class SNMPConnectionTest {
	ObjectDescImpl obj;
	ObjectDescImpl objChild;
	SNMPConnection snmpCon;

	@Before
	public void setUp() {
		SecurityModels.getInstance().addSecurityModel(
				new USM(SecurityProtocols.getInstance(), new OctetString(MPv3.createLocalEngineID()), 0));
	}


	@Test
	public void testSNMPConnection() throws UnknownHostException, IOException {
		obj = new ObjectDescImpl("SNMPAGENT:2458");
		obj.addProperty(new PropertyDescImpl("privPwd", "dummy"));
		obj.addProperty(new PropertyDescImpl("secLevel", "auth"));
		obj.addProperty(new PropertyDescImpl("address", "UDP:192.168.1.2/2002"));
		obj.addProperty(new PropertyDescImpl("retries", "123"));
		obj.addProperty(new PropertyDescImpl("timeout", "2002"));
		obj.addProperty(new PropertyDescImpl("version", "V1"));
		obj.addProperty(new PropertyDescImpl("privateCommunity", "dummy"));
		objChild = new ObjectDescImpl("SNMPAGENT:2457");
		objChild.addProperty(new PropertyDescImpl("authProto", "MD5"));
		objChild.addProperty(new PropertyDescImpl("privProto", "AES128"));
		objChild.addProperty(new PropertyDescImpl("address", "UDP:192.168.1.2/2002"));
		objChild.addProperty(new PropertyDescImpl("retries", "123"));
		objChild.addProperty(new PropertyDescImpl("timeout", "2002"));
		objChild.addProperty(new PropertyDescImpl("oid", "2.2.12.53.23.1.12"));

		obj.addChild(objChild);
		objChild.setParent(obj);
		@SuppressWarnings("unused")
		SNMPConnection snmpCon = new SNMPConnection(objChild);

	}

	@Test
	public void testSNMPConnectionV2() throws UnknownHostException, IOException {
		obj = new ObjectDescImpl("SNMPAGENT:2458");
		obj.addProperty(new PropertyDescImpl("privPwd", "dummy"));
		obj.addProperty(new PropertyDescImpl("secLevel", "auth"));
		obj.addProperty(new PropertyDescImpl("address", "UDP:192.168.1.2/2002"));
		obj.addProperty(new PropertyDescImpl("retries", "123"));
		obj.addProperty(new PropertyDescImpl("timeout", "2002"));
		obj.addProperty(new PropertyDescImpl("version", "V2C"));
		obj.addProperty(new PropertyDescImpl("privateCommunity", "dummy"));
		objChild = new ObjectDescImpl("SNMPAGENT:2457");
		objChild.addProperty(new PropertyDescImpl("authProto", "MD5"));
		objChild.addProperty(new PropertyDescImpl("privProto", "AES128"));
		objChild.addProperty(new PropertyDescImpl("address", "UDP:192.168.1.2/2002"));
		objChild.addProperty(new PropertyDescImpl("retries", "123"));
		objChild.addProperty(new PropertyDescImpl("timeout", "2002"));
		objChild.addProperty(new PropertyDescImpl("oid", "2.2.12.53.23.1.12"));

		obj.addChild(objChild);
		objChild.setParent(obj);
		@SuppressWarnings("unused")
		SNMPConnection snmpCon = new SNMPConnection(objChild);

	}

	@Test
	public void testSNMPConnectionV3() throws UnknownHostException, IOException {
		obj = new ObjectDescImpl("SNMPAGENT:2458");
		obj.addProperty(new PropertyDescImpl("privPwd", "dummy"));
		obj.addProperty(new PropertyDescImpl("secLevel", "auth"));
		obj.addProperty(new PropertyDescImpl("address", "UDP:192.168.1.2/2002"));
		obj.addProperty(new PropertyDescImpl("retries", "123"));
		obj.addProperty(new PropertyDescImpl("timeout", "2002"));
		obj.addProperty(new PropertyDescImpl("version", "V3"));
		obj.addProperty(new PropertyDescImpl("privateCommunity", "dummy"));
		obj.addProperty(new PropertyDescImpl("securityName", "dummy"));
		obj.addProperty(new PropertyDescImpl("contextName", "dummy"));
		obj.addProperty(new PropertyDescImpl("authProto", "MD5"));
		obj.addProperty(new PropertyDescImpl("authPwd", "dummydummy"));
		objChild = new ObjectDescImpl("SNMPAGENT:2457");
		objChild.addProperty(new PropertyDescImpl("authProto", "MD5"));
		objChild.addProperty(new PropertyDescImpl("privProto", "AES128"));
		objChild.addProperty(new PropertyDescImpl("address", "UDP:192.168.1.2/2002"));
		objChild.addProperty(new PropertyDescImpl("retries", "123"));
		objChild.addProperty(new PropertyDescImpl("timeout", "2002"));
		objChild.addProperty(new PropertyDescImpl("oid", "2.2.12.53.23.1.12"));

		obj.addChild(objChild);
		objChild.setParent(obj);

		@SuppressWarnings("unused")
		SNMPConnection snmpCon = new SNMPConnection(objChild);

	}

	@Test
	public void testSNMPConnectionV3SecurityLevel() throws UnknownHostException, IOException {
		obj = new ObjectDescImpl("SNMPAGENT:2458");
		obj.addProperty(new PropertyDescImpl("privPwd", "dummy"));
		obj.addProperty(new PropertyDescImpl("secLevel", ""));
		obj.addProperty(new PropertyDescImpl("address", "UDP:192.168.1.2/2002"));
		obj.addProperty(new PropertyDescImpl("retries", "123"));
		obj.addProperty(new PropertyDescImpl("timeout", "2002"));
		obj.addProperty(new PropertyDescImpl("version", "V3"));
		obj.addProperty(new PropertyDescImpl("privateCommunity", "dummy"));
		obj.addProperty(new PropertyDescImpl("securityName", "dummy"));
		obj.addProperty(new PropertyDescImpl("contextName", "dummy"));
		obj.addProperty(new PropertyDescImpl("authProto", "MD5"));
		obj.addProperty(new PropertyDescImpl("authPwd", "dummydummy"));
		objChild = new ObjectDescImpl("SNMPAGENT:2457");
		objChild.addProperty(new PropertyDescImpl("authProto", "MD5"));
		objChild.addProperty(new PropertyDescImpl("privProto", "AES128"));
		objChild.addProperty(new PropertyDescImpl("address", "UDP:192.168.1.2/2002"));
		objChild.addProperty(new PropertyDescImpl("retries", "123"));
		objChild.addProperty(new PropertyDescImpl("timeout", "2002"));
		objChild.addProperty(new PropertyDescImpl("oid", "2.2.12.53.23.1.12"));

		obj.addChild(objChild);
		objChild.setParent(obj);

		@SuppressWarnings("unused")
		SNMPConnection snmpCon = new SNMPConnection(objChild);

	}

	@Test
	public void testSNMPConnectionV3SecurityLevelAUTH_PRIV() throws UnknownHostException, IOException {
		obj = new ObjectDescImpl("SNMPAGENT:2458");
		obj.addProperty(new PropertyDescImpl("privPwd", "dummy"));
		obj.addProperty(new PropertyDescImpl("secLevel", "authpriv"));
		obj.addProperty(new PropertyDescImpl("address", "UDP:192.168.1.2/2002"));
		obj.addProperty(new PropertyDescImpl("retries", "123"));
		obj.addProperty(new PropertyDescImpl("timeout", "2002"));
		obj.addProperty(new PropertyDescImpl("version", "V3"));
		obj.addProperty(new PropertyDescImpl("privateCommunity", "dummy"));
		obj.addProperty(new PropertyDescImpl("securityName", "dummy"));
		obj.addProperty(new PropertyDescImpl("contextName", "dummy"));
		obj.addProperty(new PropertyDescImpl("authProto", "MD5"));
		obj.addProperty(new PropertyDescImpl("authPwd", "dummydummy"));
		obj.addProperty(new PropertyDescImpl("privProto", "aes128"));
		obj.addProperty(new PropertyDescImpl("privPwd", "dummydummy"));
		objChild = new ObjectDescImpl("SNMPAGENT:2457");
		objChild.addProperty(new PropertyDescImpl("authProto", "MD5"));
		objChild.addProperty(new PropertyDescImpl("privProto", "AES128"));
		objChild.addProperty(new PropertyDescImpl("address", "UDP:192.168.1.2/2002"));
		objChild.addProperty(new PropertyDescImpl("retries", "123"));
		objChild.addProperty(new PropertyDescImpl("timeout", "2002"));
		objChild.addProperty(new PropertyDescImpl("oid", "2.2.12.53.23.1.12"));

		obj.addChild(objChild);
		objChild.setParent(obj);

		SNMPConnection snmpCon = new SNMPConnection(objChild);

		assertEquals("2.2.12.53.23.1.12", snmpCon.getOid());
		assertNotNull(snmpCon.getSnmp());
		assertNotNull(snmpCon.getPdu());
		assertNotNull(snmpCon.getAddress());
		snmpCon.setTimeout(1000);
		assertEquals(1000, snmpCon.getTimeout());
		snmpCon.setRetries(5);
		assertEquals(5, snmpCon.getRetries());

		snmpCon.disconnect();

	}

	@Test
	public void testDisconnect() throws UnknownHostException, IOException {

	}

}
