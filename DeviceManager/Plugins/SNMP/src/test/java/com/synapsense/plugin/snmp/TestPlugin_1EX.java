package com.synapsense.plugin.snmp;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.synapsense.plugin.api.exceptions.ConfigPluginException;
import com.synapsense.plugin.api.exceptions.IncorrectStateException;
import com.synapsense.plugin.apinew.impl.ObjectDescImpl;
import com.synapsense.plugin.apinew.impl.PropertyDescImpl;
import com.synapsense.plugin.configuration.PluginConfigImpl;

public class TestPlugin_1EX {
	private final static Logger logger = Logger.getLogger(TestPlugin_1EX.class);
	private final static String FILE_SEPARATOR = System.getProperty("file.separator");
	static ObjectDescImpl obj = new ObjectDescImpl("SNMPAGENT:2");
	static ObjectDescImpl objChild = new ObjectDescImpl("SNMPOBJECT:4");

	/**
	 * @throws InterruptedException
	 * @throws ConfigPluginException
	 * @throws IOException
	 * @throws SAXException
	 * @throws ParserConfigurationException
	 */
	//@Test
	public void Test() throws InterruptedException, ConfigPluginException, IOException, ParserConfigurationException,
	        SAXException {
		//PropertyConfigurator.configure("lib/log4j.properties");
		SNMPPlugin snmpPlugin = new SNMPPlugin();
		PluginConfigImpl config = new PluginConfigImpl("SNMP");

		File file = new File("test" + FILE_SEPARATOR + "java"+ FILE_SEPARATOR + "com" + FILE_SEPARATOR + "synapsense" + FILE_SEPARATOR
		        + "plugin" + FILE_SEPARATOR + "snmp" + FILE_SEPARATOR + "SNMPDevice.xml");

		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db;
		System.out.println("*Start parse XML*");
		db = dbf.newDocumentBuilder();
		Document doc = db.parse(file);
		doc.getDocumentElement().normalize();

		obj.addChild(objChild);
		objChild.setParent(obj);

		NodeList propertyList = doc.getElementsByTagName("prop");
		for (int i = 0; i < propertyList.getLength(); i++) {
			Node property = propertyList.item(i);
			String parentID = property.getParentNode().getAttributes().getNamedItem("id").toString();
			parentID = parentID.substring(parentID.indexOf("\"") + 1, parentID.lastIndexOf("\""));
			String name = property.getAttributes().getNamedItem("name").toString();
			name = name.substring(name.indexOf("\"") + 1, name.lastIndexOf("\""));
			String value = property.getAttributes().getNamedItem("value").toString();
			value = value.substring(value.indexOf("\"") + 1, value.lastIndexOf("\""));

			PropertyDescImpl propertyDesc = new PropertyDescImpl(name, value);
			if (parentID.equals("SNMPAGENT:2")) {
				obj.addProperty(propertyDesc);
				propertyDesc.setParent(obj);
			}
			if (parentID.equals("SNMPOBJECT:4")) {
				objChild.addProperty(propertyDesc);
				propertyDesc.setParent(objChild);
			}
		}
		config.addObject(obj);
		config.addObject(objChild);
		config.addRoot(obj);
		System.out.println(config.getObject("SNMPAGENT:2").getProperties());
		System.out.println(config.getObject("SNMPOBJECT:4").getProperties());
		System.out.println(config.getRoots());
		System.out.println("*Finish parse XML*");

		Thread thrAgent = new Thread(new Runnable() {
			@Override
			public void run() {
				/*
				 * try { SNMPTestAgent.startAgent();
				 * 
				 * } catch (DuplicateRegistrationException e) { logger.error(e);
				 * }
				 */
			}
		});
		/*
		 * Thread thrServer = new Thread(new Runnable() {
		 * 
		 * @Override public void run() { try {
		 * PMRMIServerStarter.start("rmi://localhost:1099/PMServer",
		 * objectManager);
		 * 
		 * @SuppressWarnings("unused") PMRMIServerImpl server =
		 * (PMRMIServerImpl) PMRMIServerStarter.getServer(); } catch
		 * (RemoteException e) { logger.error(e); } catch (MalformedURLException
		 * e) { logger.error(e); }
		 * 
		 * }
		 * 
		 * });
		 * 
		 * thrAgent.setDaemon(true); thrAgent.start();
		 * thrServer.setDaemon(true); thrServer.start(); Thread.sleep(10000);
		 * snmpPlugin.configure(config); PMTransport tt = new PMTransport();
		 * tt.setServicePath("rmi://localhost:1099/PMServer");
		 * snmpPlugin.setTransport(tt); //tt.connect();
		 */

		try {
			snmpPlugin.start();

		} catch (IncorrectStateException e) {
			logger.error(e.getLocalizedMessage(), e);
		}

		Thread.sleep(5000);

		String value = "1";// snmpPlugin.getPropertyValue(objChild,
		                   // "name").toString();
		logger.info("*************************" + value + "*************************");
		assertEquals("1", value);
		Serializable newProp = "2";
		snmpPlugin.setPropertyValue(objChild, "name", newProp);

		// String newvalue = snmpPlugin.getPropertyValue(objChild,
		// "name").toString();
		// logger.info("!!!!!!!!!!!" + newvalue + "!!!!!!!!!!!!!!");
		// assertEquals("2", newvalue);

		thrAgent.interrupt();
		// thrServer.interrupt();
		snmpPlugin.stop();

	}

}
