package com.synapsense.plugin.snmp;

import java.io.IOException;
import java.net.UnknownHostException;

import org.apache.log4j.Logger;
import org.snmp4j.AbstractTarget;
import org.snmp4j.CommunityTarget;
import org.snmp4j.MessageDispatcherImpl;
import org.snmp4j.PDU;
import org.snmp4j.PDUv1;
import org.snmp4j.ScopedPDU;
import org.snmp4j.Snmp;
import org.snmp4j.TransportMapping;
import org.snmp4j.UserTarget;
import org.snmp4j.event.AuthenticationFailureEvent;
import org.snmp4j.event.AuthenticationFailureListener;
import org.snmp4j.mp.SnmpConstants;
import org.snmp4j.security.SecurityLevel;
import org.snmp4j.security.UsmUser;
import org.snmp4j.smi.Address;
import org.snmp4j.smi.GenericAddress;
import org.snmp4j.smi.OID;
import org.snmp4j.smi.OctetString;
import org.snmp4j.transport.DefaultTcpTransportMapping;
import org.snmp4j.transport.DefaultUdpTransportMapping;

import com.synapsense.plugin.apinew.ObjectDesc;

public class SNMPConnection {
	private final static Logger logger = Logger.getLogger(SNMPConnection.class);

	private Address address;
	private TransportMapping trnsprt;
	private Snmp snmp;
	private int retries;
	private int timeout;
	private String oid;
	private AbstractTarget target;
	private PDU pdu;

	public SNMPConnection(ObjectDesc obj) throws UnknownHostException, IOException {
		ObjectDesc parent = obj.getParent();

		String strAddress = parent.getPropertyValue(SNMPConstants.SNMP_AGENT_ADDRESS);
		String[] tmp = strAddress.split(":");
		String strTransport;
		if (tmp.length > 1) {
			strTransport = tmp[0];
		} else {
			strTransport = "udp";
		}
		address = GenericAddress.parse(strAddress);
		retries = new Integer(obj.getPropertyValue(SNMPConstants.SNMP_OBJECT_RETRIES));
		timeout = new Integer(obj.getPropertyValue(SNMPConstants.SNMP_OBJECT_TIMEOUT));
		if ("tcp".equalsIgnoreCase(strTransport)) {
			trnsprt = new DefaultTcpTransportMapping();
		} else if ("udp".equalsIgnoreCase(strTransport)) {
			trnsprt = new DefaultUdpTransportMapping();
		} else
			throw new IOException("Unknown transport: " + strTransport);
		snmp = new Snmp(trnsprt);
		// [BC 04/2015] SNMP4J discards authentication failures and the request eventually fails with an obscure timeout.
		// The best we ca do is register a listener and log the error.
		((MessageDispatcherImpl) snmp.getMessageDispatcher()).addAuthenticationFailureListener(new AuthenticationFailureListener() {
			@Override
			public void authenticationFailure(AuthenticationFailureEvent event) {
				logger.warn("SNMP Authentication Failure " + event.getError() + " against " + event.getAddress() + ". The OID will be logged separately. See SNMP_MP_XXX constants in SnmpConstants.java");
			}
		});
		snmp.listen();
		setOid(obj.getPropertyValue(SNMPConstants.SNMP_OBJECT_OID));
		String ver = parent.getPropertyValue(SNMPConstants.SNMP_AGENT_VERSION);
		Version vo = getVerByStr(ver);
		switch (vo) {
		case V1:
			initV1(parent);
			break;
		case V2C:
			initV2C(parent);
			break;
		case V3:
			initV3(parent);
			break;
		}
	}

	public void disconnect() {
		try {
			snmp.close();
		} catch (IOException e) {
			logger.error(e, e);
		}
	}

	private void initV1(ObjectDesc parent) throws IOException {
		setTarget(new CommunityTarget());
		getTarget().setVersion(SnmpConstants.version1);
		pdu = new PDUv1();
		String strCommunity = parent.getPropertyValue(SNMPConstants.V1_COMMUNITY);
		((CommunityTarget) getTarget()).setCommunity(new OctetString(strCommunity));
	}

	private void initV2C(ObjectDesc parent) throws IOException {
		setTarget(new CommunityTarget());
		getTarget().setVersion(SnmpConstants.version2c);
		pdu = new PDU();
	}

	private void initV3(ObjectDesc parent) throws IOException {
		pdu = new ScopedPDU();
		setTarget(new UserTarget());
		getTarget().setVersion(SnmpConstants.version3);
		String strSec = parent.getPropertyValue(SNMPConstants.V3_SEC_LEVEL);
		String strSecName = parent.getPropertyValue(SNMPConstants.V3_SECURITY_NAME);
		String strContextName = parent.getPropertyValue(SNMPConstants.V3_CONTEXT_NAME);
		((ScopedPDU) pdu).setContextName(new OctetString(strContextName));
		((UserTarget) getTarget()).setSecurityName(new OctetString(strSecName));
		String strAuthProtocol;
		String strAuthPassword;
		String strPrivProtocol;
		String strPrivPassword;
		OID authprot;
		OID privacy;
		switch (Security.getLevel(strSec)) {
		case SecurityLevel.AUTH_PRIV:
			strAuthProtocol = parent.getPropertyValue(SNMPConstants.V3_AUTH_PROTOCOL);
			strAuthPassword = parent.getPropertyValue(SNMPConstants.V3_AUTH_PASS);
			strPrivProtocol = parent.getPropertyValue(SNMPConstants.V3_PRIVACY_PROTOCOL);
			strPrivPassword = parent.getPropertyValue(SNMPConstants.V3_PRIVACY_PASS);
			authprot = Security.getAuthProtocol(strAuthProtocol);
			privacy = Security.getPrivacy(strPrivProtocol);
			snmp.getUSM().addUser(
			        new OctetString(strSecName),
			        new UsmUser(new OctetString(strSecName), authprot, new OctetString(strAuthPassword), privacy,
			                new OctetString(strPrivPassword)));
			((UserTarget) getTarget()).setSecurityLevel(SecurityLevel.AUTH_PRIV);
			break;
		case SecurityLevel.AUTH_NOPRIV:
			strAuthProtocol = parent.getPropertyValue(SNMPConstants.V3_AUTH_PROTOCOL);
			strAuthPassword = parent.getPropertyValue(SNMPConstants.V3_AUTH_PASS);
			authprot = Security.getAuthProtocol(strAuthProtocol);
			snmp.getUSM().addUser(new OctetString(strSecName),
			        new UsmUser(new OctetString(strSecName), authprot, new OctetString(strAuthPassword), null, null));
			((UserTarget) getTarget()).setSecurityLevel(SecurityLevel.AUTH_NOPRIV);
			break;
		case SecurityLevel.NOAUTH_NOPRIV:
			snmp.getUSM().addUser(new OctetString(strSecName),
			        new UsmUser(new OctetString(strSecName), null, null, null, null));
			((UserTarget) getTarget()).setSecurityLevel(SecurityLevel.NOAUTH_NOPRIV);
			break;
		}

	}

	public Version getVerByStr(String id) {
		Version res = Version.V2C;
		if ("v1".equalsIgnoreCase(id)) {
			res = Version.V1;
		} else if ("v2c".equalsIgnoreCase(id)) {
			res = Version.V2C;
		} else if ("v3".equalsIgnoreCase(id)) {
			res = Version.V3;
		}
		return res;
	}

	public void setTarget(AbstractTarget target) {
		this.target = target;
	}

	public AbstractTarget getTarget() {
		return target;
	}

	public void setOid(String oid) {
		this.oid = oid;
	}

	public String getOid() {
		return oid;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public Address getAddress() {
		return address;
	}

	public void setRetries(int retries) {
		this.retries = retries;
	}

	public int getRetries() {
		return retries;
	}

	public void setTimeout(int timeout) {
		this.timeout = timeout;
	}

	public int getTimeout() {
		return timeout;
	}

	public PDU getPdu() {
		return pdu;
	}

	public Snmp getSnmp() {
		return snmp;
	}

}
