/**
 * 
 */
package com.synapsense.plugin.snmp;

import java.io.IOException;
import java.io.Serializable;

import org.snmp4j.AbstractTarget;
import org.snmp4j.CommunityTarget;
import org.snmp4j.PDU;
import org.snmp4j.event.ResponseEvent;
import org.snmp4j.mp.SnmpConstants;
import org.snmp4j.smi.OID;
import org.snmp4j.smi.OctetString;
import org.snmp4j.smi.Variable;
import org.snmp4j.smi.VariableBinding;

import com.synapsense.plugin.api.exceptions.WrongTypeException;
import com.synapsense.plugin.apinew.ObjectDesc;
import com.synapsense.plugin.apinew.PropertyDesc;

/**
 * @author stikhon
 * 
 */
public class SNMPPropertyRW {
	public static String readProperty(final PropertyDesc property, final SNMPConnection connection) {
		ObjectDesc snmpObject = property.getParent();
		ObjectDesc snmpAgent = snmpObject.getParent();
		String result = null;
		VariableBinding vb = new VariableBinding(new OID(connection.getOid()));
		AbstractTarget target = connection.getTarget();
		synchronized (target) {
			if (target.getVersion() == SnmpConstants.version2c) {
				String strCommunity = snmpAgent.getPropertyValue(SNMPConstants.V2_PUBLIC_COMMUNITY);
				((CommunityTarget) target).setCommunity(new OctetString(strCommunity));
			}

			result = sendMessage(PDU.GET, vb, connection);

		}
		return result;
	}

	public static void writeProperty(final PropertyDesc property, final SNMPConnection connection,
	        final Serializable value) {
		if (value != null && value instanceof String) {

			ObjectDesc snmpObject = property.getParent();
			ObjectDesc snmpAgent = snmpObject.getParent();
			VariableBinding vb = new VariableBinding(new OID(connection.getOid()));
			AbstractTarget target = connection.getTarget();
			synchronized (target) {
				if (target.getVersion() == SnmpConstants.version2c) {
					String strCommunity = snmpAgent.getPropertyValue(SNMPConstants.V2_PRIVATE_COMMUNITY);
					((CommunityTarget) target).setCommunity(new OctetString(strCommunity));
				}
				String strType = snmpObject.getPropertyValue(SNMPConstants.SNMP_OBJECT_TYPE);
				DataType type = DataType.getType(strType);
				try {
					Variable var = DataType.convertType((String) value, type);
					vb.setVariable(var);
				} catch (WrongTypeException e) {
					throw new RuntimeException(e);
				}
				sendMessage(PDU.SET, vb, connection);
			}
		}
	}

	private static String sendMessage(int type, VariableBinding vb, SNMPConnection connection) {
		try {
			AbstractTarget target = connection.getTarget();
			target.setAddress(connection.getAddress());
			target.setRetries(connection.getRetries());
			target.setTimeout(connection.getTimeout());
			PDU pdu = connection.getPdu();
			pdu.clear();
			pdu.add(vb);
			pdu.setType(type);
			ResponseEvent response = type == PDU.SET ? connection.getSnmp().set(pdu, target) : connection.getSnmp().get(pdu, target);
			if (response == null) {
				throw new RuntimeException("Timeout retrieving OID [" + vb.getOid() + "] from ["+ connection.getAddress() +"]");
			}
			//noinspection ThrowableResultOfMethodCallIgnored
			if (response.getError() != null) {
				throw new RuntimeException("Error retrieving OID [" + vb.getOid() + "] from ["+ connection.getAddress() +"]", response.getError());
			}
			if (response.getResponse() == null) {
				// [BC 04/2015] This is separate from the first if because I'd rather have the exception from the second if.
				throw new RuntimeException("SNMP v3 authentication/privacy misconfiguration or timeout when retrieving OID [" + vb.getOid() + "] from ["+ connection.getAddress() +"]");
			}
			PDU responsePDU = response.getResponse();
			if (responsePDU.getErrorStatus() != SnmpConstants.SNMP_ERROR_SUCCESS) {
				throw new RuntimeException("Error retrieving OID [" + vb.getOid() + "] from [" + connection.getAddress() + "]: " + responsePDU);
			}
			if (responsePDU.getType() == PDU.REPORT) {
				throw new RuntimeException("SNMP REPORT error message: " + responsePDU);
			}
			// TODO [BC 04/2015] Is this check valid for SET?
			if (responsePDU.getVariableBindings().isEmpty()) {
				throw new RuntimeException("No VariableBinding returned for OID [" + vb.getOid() + "] from ["+ connection.getAddress() +"]: " + responsePDU);
			}
			VariableBinding variableBinding = (VariableBinding) responsePDU.getVariableBindings().get(0);
			if (variableBinding.isException()) {
				throw new RuntimeException("VariableBinding error for OID [" + vb.getOid() + "] from ["+ connection.getAddress() +"]: " + variableBinding);
			}
			if (SNMPPlugin.logger.isDebugEnabled()) {
				SNMPPlugin.logger.debug("OID [" + vb.getOid() + "] from ["+ connection.getAddress() +"]: " + variableBinding.getVariable().toString());
			}
			return variableBinding.getVariable().toString();
		} catch (IOException e) {
			throw new RuntimeException("Error retrieving OID [" + vb.getOid() + "] from ["+ connection.getAddress() +"]", e);
		}
	}
}
