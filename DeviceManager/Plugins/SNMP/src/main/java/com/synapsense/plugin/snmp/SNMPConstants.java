/**
 * 
 */
package com.synapsense.plugin.snmp;

/**
 * @author stikhon
 * 
 */
public class SNMPConstants {

	public static final String SNMP_AGENT_ADDRESS = "address";
	public static final String SNMP_AGENT_VERSION = "version";
	public static final String SNMP_OBJECT_OID = "oid";
	public static final String SNMP_OBJECT_RETRIES = "retries";
	public static final String SNMP_OBJECT_TIMEOUT = "timeout";
	public static final String SNMP_OBJECT_TYPE = "type";
	public static final String SNMP_OBJECT_LAST_VALUE = "lastValue";
	public static final String V_ID = "id";
	public static final String V1_COMMUNITY = SNMPConstants.V2_PRIVATE_COMMUNITY;
	public static final String V2_PRIVATE_COMMUNITY = "privateCommunity";
	public static final String V2_PUBLIC_COMMUNITY = "publicCommunity";
	public static final String V3_CONTEXT_NAME = "contextName";
	public static final String V3_SECURITY_NAME = "securityName";
	public static final String V3_SEC_LEVEL = "secLevel";
	public static final String V3_AUTH_PASS = "authPwd";
	public static final String V3_AUTH_PROTOCOL = "authProto";
	public static final String V3_PRIVACY_PASS = "privPwd";
	public static final String V3_PRIVACY_PROTOCOL = "privProto";

}
