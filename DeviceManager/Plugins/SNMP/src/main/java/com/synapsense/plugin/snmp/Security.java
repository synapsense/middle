package com.synapsense.plugin.snmp;

import java.util.HashMap;

import org.snmp4j.security.AuthMD5;
import org.snmp4j.security.AuthSHA;
import org.snmp4j.security.PrivAES128;
import org.snmp4j.security.PrivAES192;
import org.snmp4j.security.PrivAES256;
import org.snmp4j.security.PrivDES;
import org.snmp4j.security.SecurityLevel;
import org.snmp4j.smi.OID;

public class Security {
	public String name;
	public String context;
	public String privprotocol;
	public String privpassphrase;
	public String authprotocol;
	public String authpassphrase;
	public String level;
	public String community;

	private static HashMap<String, OID> privacy = new HashMap<String, OID>();
	private static HashMap<String, OID> authProtocol = new HashMap<String, OID>();

	static {
		privacy.put("des", PrivDES.ID);
		privacy.put("aes128", PrivAES128.ID);
		privacy.put("aes192", PrivAES192.ID);
		privacy.put("aes256", PrivAES256.ID);
	}
	static {
		authProtocol.put("MD5", AuthMD5.ID);
		authProtocol.put("SHA", AuthSHA.ID);
	}

	public static OID getAuthProtocol(String protocol) {
		if (protocol == null)
			throw new IllegalArgumentException("protocol is null");
		if (authProtocol.containsKey(protocol.toUpperCase()))
			return authProtocol.get(protocol.toUpperCase());
		else
			throw new IllegalArgumentException("\"" + protocol + "\" is wrong privacy protocol");
	}

	public static OID getPrivacy(String protocol) throws IllegalArgumentException {
		if (protocol == null)
			throw new IllegalArgumentException("protocol is null");
		if (privacy.containsKey(protocol.toLowerCase()))
			return privacy.get(protocol.toLowerCase());
		else
			throw new IllegalArgumentException("\"" + protocol + "\" is wrong privacy protocol");
	}

	public static int getLevel(String level) throws IllegalArgumentException {
		if (level == null)
			throw new IllegalArgumentException("level is null");
		if (level.equalsIgnoreCase("authpriv"))
			return SecurityLevel.AUTH_PRIV;
		else if (level.equalsIgnoreCase("auth"))
			return SecurityLevel.AUTH_NOPRIV;
		else if (level.equalsIgnoreCase(""))
			return SecurityLevel.NOAUTH_NOPRIV;
		else
			throw new IllegalArgumentException("\"" + level + "\" is wrong security level");
	}
}
