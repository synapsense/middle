package com.synapsense.plugin.snmp;

import java.io.IOException;
import java.io.Serializable;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.snmp4j.mp.MPv3;
import org.snmp4j.security.SecurityModels;
import org.snmp4j.security.SecurityProtocols;
import org.snmp4j.security.USM;
import org.snmp4j.smi.OctetString;

import com.synapsense.plugin.api.exceptions.ConfigPluginException;
import com.synapsense.plugin.api.exceptions.IncorrectStateException;
import com.synapsense.plugin.apinew.ObjectDesc;
import com.synapsense.plugin.apinew.Plugin;
import com.synapsense.plugin.apinew.PluginConfig;
import com.synapsense.plugin.apinew.PropertyDesc;
import com.synapsense.plugin.apinew.Transport;

public class SNMPPlugin implements Plugin {
	public final static Logger logger = Logger.getLogger(SNMPPlugin.class);
	private final static String ID = "SNMP";
	private static Properties PLUGIN_DESC = new Properties();
	static {
		PLUGIN_DESC.put(PLUGIN_ID, ID);
		PLUGIN_DESC.put(ROOT_TYPES, "SNMPAGENT");
	}
	@SuppressWarnings("unused")
	private Transport transport;
	private HashMap<String, SNMPConnection> allConnections = new HashMap<String, SNMPConnection>();
	private ArrayList<ObjectDesc> rootObjects = new ArrayList<ObjectDesc>();
	private boolean configured = false;
	private boolean started = false;

	public SNMPPlugin() {
		logger.info("SNMP Plugin has been created.");
	}

	@Override
	public void configure(PluginConfig config) throws ConfigPluginException {
		stop();
		SecurityModels.getInstance().addSecurityModel(
				new USM(SecurityProtocols.getInstance(), new OctetString(MPv3.createLocalEngineID()), 0));
		List<ObjectDesc> agents = config.getRoots();
		for (ObjectDesc snmpagent : agents) {
			if (snmpagent != null) {
				rootObjects.add(snmpagent);
			}

			List<ObjectDesc> objs = snmpagent.getChildren();
			for (ObjectDesc snmpobject : objs) {
				try {
					SNMPConnection connection = new SNMPConnection(snmpobject);
					allConnections.put(snmpobject.getID(), connection);
				} catch (UnknownHostException e) {
					logger.error("Object with id " + snmpobject.getID() + " cannot be accessed, the host is not found",
					        e);
					throw new ConfigPluginException("Unable to configure SNMP Plugin, an exception occired.");
				} catch (IOException e) {
					logger.error("Object with id " + snmpobject.getID() + " cannot be accessed", e);
					throw new ConfigPluginException("Unable to configure SNMP Plugin, an I/O exception occired.", e);
				}

			}
		}
		configured = true;
		logger.debug("Plugin " + ID + " is configured.");

	}

	@Override
	public String getID() {
		return ID;
	}

	@Override
	public Properties getPluginDescription() {
		return PLUGIN_DESC;
	}

	@Override
	public Serializable getPropertyValue(ObjectDesc obj, String propertyName) {
		String result = null;
		if (obj != null) {
			SNMPConnection connection = allConnections.get(obj.getID());
			PropertyDesc pd = obj.getProperty(propertyName);
			if (pd != null && connection != null) {
				result = SNMPPropertyRW.readProperty(pd, connection);
			}
		}
		return result;
	}

	@Override
	public void setPropertyValue(ObjectDesc obj, String propertyName, Serializable value) {
		if (obj != null) {
			SNMPConnection connection = allConnections.get(obj.getID());
			PropertyDesc pd = obj.getProperty(propertyName);
			if (pd != null && connection != null) {
				SNMPPropertyRW.writeProperty(pd, connection, value);
			}
		}
	}

	@Override
	public void start() throws IncorrectStateException {
		if (!configured) {
			throw new IncorrectStateException("Plugin " + ID + " has to be configured before start.");
		}
		if (!started) {
			started = true;
			logger.debug("Plugin " + ID + " is started.");
		}
	}

	public void stop() {
		if (started) {
			for (SNMPConnection connection : allConnections.values()) {
				connection.disconnect();
			}
		}
		allConnections.clear();
		rootObjects.clear();
		configured = false;
		started = false;
		logger.debug("Plugin " + ID + " is stopped.");
	}

	@Override
	public void setPropertyValue(ObjectDesc object, List<String> propertyNames, List<Serializable> values) {
		if (propertyNames == null || values == null || propertyNames.size() != values.size()) {
			throw new IllegalArgumentException("Unable to set properties values.");
		}
		for (int i = 0; i < propertyNames.size(); i++) {
			setPropertyValue(object, propertyNames.get(i), values.get(i));
		}
	}

	@Override
	public List<Serializable> getPropertyValue(ObjectDesc object, List<String> propertyNames) {
		List<Serializable> result = new ArrayList<Serializable>();
		for (String property : propertyNames) {
			result.add(getPropertyValue(object, property));
		}
		return result;
	}

	@Override
	public void setTransport(Transport transport) {
		this.transport = transport;
	}

}
