package com.synapsense.plugin.snmp;

import java.util.HashMap;

import org.snmp4j.smi.Counter32;
import org.snmp4j.smi.Counter64;
import org.snmp4j.smi.Gauge32;
import org.snmp4j.smi.Integer32;
import org.snmp4j.smi.IpAddress;
import org.snmp4j.smi.OID;
import org.snmp4j.smi.OctetString;
import org.snmp4j.smi.TimeTicks;
import org.snmp4j.smi.UnsignedInteger32;
import org.snmp4j.smi.Variable;

import com.synapsense.plugin.api.exceptions.WrongTypeException;

public enum DataType {
Counter32, Counter64, Gauge32, Integer32, IpAddress, OctetString, OID, TimeTicks, UnsignedInteger32;

private static HashMap<String, DataType> map = new HashMap<String, DataType>();

static {
	map.put("counter32", Counter32);
	map.put("counter64", Counter64);
	map.put("gauge32", Gauge32);
	map.put("integer32", Integer32);
	map.put("ipaddress", IpAddress);
	map.put("octetstring", OctetString);
	map.put("oid", OID);
	map.put("timeticks", TimeTicks);
	map.put("unsignedinteger32", UnsignedInteger32);
}

public static DataType getType(String type) throws IllegalArgumentException {
	if ((type != null) && map.containsKey(type.toLowerCase()))
		return map.get(type.toLowerCase());
	throw new IllegalArgumentException("The type \"" + type + "\" is wrong SnmpProperty DataType");
}

/**
 * Converts value into the SNMP Variable of the SNMP type
 * 
 * @param value
 * @return Variable as an object of the appropriate type.
 * @throws IllegalArgumentException
 * @throws TypeNotFoundException
 * @throws ParsingException
 */
public static Variable convertType(String value, DataType type) throws WrongTypeException {
	if (value == null || type == null)
		throw new WrongTypeException("Type is null");
	Variable variable;
	try {
		switch (type) {
		case Counter32:
			variable = new Counter32((long) Long.parseLong(value));
			break;
		case Counter64:
			variable = new Counter64((long) Long.parseLong(value));
			break;
		case Gauge32:
			variable = new Gauge32((long) Integer.parseInt(value));
			break;
		case Integer32:
			variable = new Integer32((int) Integer.parseInt(value));
			break;
		case IpAddress:
			variable = new IpAddress(value);
			if (!((IpAddress) variable).isValid()) {
				throw new WrongTypeException("The IP address is in the wrong format");
			}
			break;
		case OctetString:
			variable = new OctetString(value);
			break;
		case OID:
			variable = new OID(value);
			if (!((OID) variable).isValid()) {
				throw new WrongTypeException("The OID is in the wrong format");
			}
			break;
		case TimeTicks:
			variable = new TimeTicks((long) Long.parseLong(value));
			break;
		case UnsignedInteger32:
			variable = new UnsignedInteger32((long) Long.parseLong(value));
			break;
		default:
			throw new WrongTypeException("Fatal: Property Type is unknown");
		}
	} catch (RuntimeException e) {
		throw new WrongTypeException(e.getMessage());
	}
	return variable;
}
}
