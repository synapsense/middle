package com.synapsense.plugin.ipmi.unit;

import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Paths;

import org.junit.Test;

import com.synapsense.plugin.api.exceptions.ConfigPluginException;
import com.synapsense.plugin.apinew.Plugin;
import com.synapsense.plugin.apinew.PluginConfig;
import com.synapsense.plugin.configuration.DmXmlConfigReader;
import com.synapsense.plugin.ipmi.IPMIPlugin;

public class IPMIPluginConfigureTest {

	@Test
	public void configure() throws ConfigPluginException, URISyntaxException {
		Plugin ipmi = new IPMIPlugin();
		File src = new File(IPMIPluginConfigureTest.class.getClassLoader().getResource("ipmi.xml").toURI());
		PluginConfig config = DmXmlConfigReader.loadPluginsConfig(src).get("ipmi");
		ipmi.configure(config);
	}

	@Test
	public void configureIncorrectConfiguration() throws ConfigPluginException, URISyntaxException {
		Plugin ipmi = new IPMIPlugin();
		File src = new File(IPMIPluginConfigureTest.class.getClassLoader().getResource("ipmiMissing.xml").toURI());
		PluginConfig config = DmXmlConfigReader.loadPluginsConfig(src).get("ipmi");
		ipmi.configure(config);
	}

	@Test(expected = ConfigPluginException.class)
	public void configureIllegalAddress() throws ConfigPluginException, URISyntaxException {
		Plugin ipmi = new IPMIPlugin();
		File src = new File(IPMIPluginConfigureTest.class.getClassLoader().getResource("ipmiIllegalAddress.xml").toURI());
		PluginConfig config = DmXmlConfigReader.loadPluginsConfig(src).get("ipmi");
		ipmi.configure(config);
	}

}
