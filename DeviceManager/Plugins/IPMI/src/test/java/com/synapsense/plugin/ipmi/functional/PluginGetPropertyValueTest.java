package com.synapsense.plugin.ipmi.functional;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.synapsense.plugin.api.exceptions.ConfigPluginException;
import com.synapsense.plugin.api.exceptions.IncorrectStateException;
import com.synapsense.plugin.api.exceptions.UnableGetDataException;
import com.synapsense.plugin.apinew.ObjectDesc;
import com.synapsense.plugin.apinew.Plugin;
import com.synapsense.plugin.apinew.PluginConfig;
import com.synapsense.plugin.configuration.DmXmlConfigReader;
import com.synapsense.plugin.ipmi.IPMIPlugin;

public class PluginGetPropertyValueTest {

	/**
	 * @param args
	 * @throws IOException
	 * @throws ConfigPluginException
	 * @throws IncorrectStateException
	 * 
	 */
	public static Logger log = Logger.getLogger(PluginGetPropertyValueTest.class);

	public static void main(String[] args) throws IOException, ConfigPluginException, IncorrectStateException,
	        InterruptedException {
		Plugin ipmi = new IPMIPlugin();
		File src = new File("conf/ipmi.xml");

		PluginConfig config = DmXmlConfigReader.loadPluginsConfig(src).get("ipmi");
		ipmi.configure(config);
		ipmi.start();
		ObjectDesc cputemp = config.getObject("ipmisensor:1");
		ObjectDesc systemfan = config.getObject("ipmisensor:2");
		ObjectDesc system12v = config.getObject("ipmisensor:3");
		ObjectDesc inlettemp = config.getObject("ipmisensor:4");

		try {
			Serializable tempCPU = ipmi.getPropertyValue(cputemp, "lastValue");
			Serializable systemFan = ipmi.getPropertyValue(systemfan, "lastValue");
			Serializable system12V = ipmi.getPropertyValue(system12v, "lastValue");
			Serializable tempInlet = ipmi.getPropertyValue(inlettemp, "lastValue");
			log.warn("******** CPU TEMP: " + tempCPU + "; SYSTEM FAN: " + systemFan + "; SYSTEM V12: " + system12V
			        + "; TEMP INLET: " + tempInlet + " ***********");

			Thread.sleep(10000);

			List<String> tempcpuproperties = new ArrayList<String>();
			List<String> systemfanproperties = new ArrayList<String>();
			List<String> system12vproperties = new ArrayList<String>();
			List<String> tempinletproperties = new ArrayList<String>();

			List<Serializable> tempcpuvalues = ipmi.getPropertyValue(cputemp, tempcpuproperties);
			List<Serializable> systemfanvalues = ipmi.getPropertyValue(systemfan, systemfanproperties);
			List<Serializable> systemv12values = ipmi.getPropertyValue(system12v, system12vproperties);
			List<Serializable> tempinletvalues = ipmi.getPropertyValue(inlettemp, tempinletproperties);

			log.warn("******** CPU TEMP: " + tempcpuvalues.get(0) + "; SYSTEM FAN: " + systemfanvalues.get(0)
			        + "; SYSTEM V12: " + systemv12values.get(0) + "; TEMP INLET: " + tempinletvalues.get(0)
			        + " ***********");

		} catch (UnableGetDataException e) {
			log.warn(e.getMessage(), e);
		}
	}
}
