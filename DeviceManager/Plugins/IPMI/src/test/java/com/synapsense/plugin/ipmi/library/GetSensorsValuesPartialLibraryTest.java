package com.synapsense.plugin.ipmi.library;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;

import com.veraxsystems.vxipmi.api.async.ConnectionHandle;
import com.veraxsystems.vxipmi.api.sync.IpmiConnector;
import com.veraxsystems.vxipmi.coding.commands.IpmiCommandCoder;
import com.veraxsystems.vxipmi.coding.commands.IpmiVersion;
import com.veraxsystems.vxipmi.coding.commands.PrivilegeLevel;
import com.veraxsystems.vxipmi.coding.commands.sdr.GetSdr;
import com.veraxsystems.vxipmi.coding.commands.sdr.GetSdrResponseData;
import com.veraxsystems.vxipmi.coding.commands.sdr.GetSensorReading;
import com.veraxsystems.vxipmi.coding.commands.sdr.GetSensorReadingResponseData;
import com.veraxsystems.vxipmi.coding.commands.sdr.ReserveSdrRepository;
import com.veraxsystems.vxipmi.coding.commands.sdr.ReserveSdrRepositoryResponseData;
import com.veraxsystems.vxipmi.coding.commands.sdr.record.FullSensorRecord;
import com.veraxsystems.vxipmi.coding.protocol.AuthenticationType;
import com.veraxsystems.vxipmi.coding.security.CipherSuite;

public class GetSensorsValuesPartialLibraryTest {

	/**
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		IpmiConnector connector = new IpmiConnector(0);
		ConnectionHandle handle = connector.createConnection(InetAddress.getByName("172.30.201.204"));
		handle.setPrivilegeLevel(PrivilegeLevel.Administrator);
		List<CipherSuite> suites = connector.getAvailableCipherSuites(handle);
		CipherSuite suite = suites.get(2);
		connector.getChannelAuthenticationCapabilities(handle, suite, PrivilegeLevel.Administrator);

		connector.openSession(handle, "admin", "admin", null);

		IpmiCommandCoder requestReserve = new ReserveSdrRepository(IpmiVersion.V20, handle.getCipherSuite(),
		        AuthenticationType.RMCPPlus);
		ReserveSdrRepositoryResponseData responseReserve = (ReserveSdrRepositoryResponseData) connector.sendMessage(
		        handle, requestReserve);

		List<Integer> sensorsId = new ArrayList<Integer>();
		sensorsId.add(0x0100);
		sensorsId.add(0x0140);
		sensorsId.add(0x0180);
		sensorsId.add(0x01C0);

		sensorsId.add(0x0280);
		sensorsId.add(0x02C0);
		sensorsId.add(0x0300);
		sensorsId.add(0x0340);
		sensorsId.add(0x0380);
		sensorsId.add(0x03C0);
		sensorsId.add(0x0400);
		sensorsId.add(0x0440);
		sensorsId.add(0x0480);
		sensorsId.add(0x04C0);

		sensorsId.add(0x0500);
		sensorsId.add(0x0540);
		sensorsId.add(0x0580);
		sensorsId.add(0x05C0);
		sensorsId.add(0x0600);
		sensorsId.add(0x0640);

		for (Integer id : sensorsId) {

			GetSdr requestSdrPart1 = new GetSdr(IpmiVersion.V20, handle.getCipherSuite(), AuthenticationType.RMCPPlus,
			        responseReserve.getReservationId(), id, 0, 16);

			GetSdrResponseData responseSdrPart1 = (GetSdrResponseData) connector.sendMessage(handle, requestSdrPart1);

			byte[] part1 = responseSdrPart1.getSensorRecordData();

			GetSdr requestSdrPart2 = new GetSdr(IpmiVersion.V20, handle.getCipherSuite(), AuthenticationType.RMCPPlus,
			        responseReserve.getReservationId(), id, 16, 16);

			GetSdrResponseData responseSdrPart2 = (GetSdrResponseData) connector.sendMessage(handle, requestSdrPart2);

			byte[] part2 = responseSdrPart2.getSensorRecordData();

			GetSdr requestSdrPart3 = new GetSdr(IpmiVersion.V20, handle.getCipherSuite(), AuthenticationType.RMCPPlus,
			        responseReserve.getReservationId(), id, 32, 16);

			GetSdrResponseData responseSdrPart3 = (GetSdrResponseData) connector.sendMessage(handle, requestSdrPart3);

			byte[] part3 = responseSdrPart3.getSensorRecordData();

			GetSdr requestSdrPart4 = new GetSdr(IpmiVersion.V20, handle.getCipherSuite(), AuthenticationType.RMCPPlus,
			        responseReserve.getReservationId(), id, 48, 16);

			GetSdrResponseData responseSdrPart4 = (GetSdrResponseData) connector.sendMessage(handle, requestSdrPart4);

			byte[] part4 = responseSdrPart4.getSensorRecordData();

			byte[] response = new byte[part1.length + part2.length + part3.length + part4.length];

			for (int i = 0; i < part1.length; i++) {
				response[i] = part1[i];
			}

			for (int i = 0; i < part2.length; i++) {
				response[i + part1.length] = part2[i];
			}

			for (int i = 0; i < part3.length; i++) {
				response[i + part1.length + part2.length] = part3[i];
			}

			for (int i = 0; i < part4.length; i++) {
				response[i + part1.length + part2.length + part3.length] = part4[i];
			}

			FullSensorRecord fullrecord = (FullSensorRecord) FullSensorRecord.populateSensorRecord(response);

			String name = fullrecord.getName();
			String type = fullrecord.getSensorType().toString();

			GetSensorReading sensorreading = new GetSensorReading(IpmiVersion.V20, handle.getCipherSuite(),
			        AuthenticationType.RMCPPlus, fullrecord.getSensorNumber());
			GetSensorReadingResponseData readingResponse = (GetSensorReadingResponseData) connector.sendMessage(handle,
			        sensorreading);

			Double value = readingResponse.getSensorReading(fullrecord);
			System.out.println("Type: " + type + " " + "Name: " + name + " Value: " + value);
		}
		connector.closeSession(handle);
		connector.tearDown();

	}

}
