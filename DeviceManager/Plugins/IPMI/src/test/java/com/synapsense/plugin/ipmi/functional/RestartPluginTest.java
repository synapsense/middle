package com.synapsense.plugin.ipmi.functional;

import java.io.File;
import java.io.Serializable;

import com.synapsense.plugin.api.exceptions.ConfigPluginException;
import com.synapsense.plugin.api.exceptions.IncorrectStateException;
import com.synapsense.plugin.apinew.ObjectDesc;
import com.synapsense.plugin.apinew.Plugin;
import com.synapsense.plugin.apinew.PluginConfig;
import com.synapsense.plugin.configuration.DmXmlConfigReader;
import com.synapsense.plugin.ipmi.IPMIPlugin;

public class RestartPluginTest {

	/**
	 * @param args
	 * @throws ConfigPluginException
	 * @throws IncorrectStateException
	 */
	public static void main(String[] args) throws ConfigPluginException, IncorrectStateException {
		Plugin ipmi = new IPMIPlugin();
		File src = new File("conf/ipmi.xml");

		PluginConfig config = DmXmlConfigReader.loadPluginsConfig(src).get("ipmi");
		ObjectDesc cputemp = config.getObject("ipmisensor:1");
		ObjectDesc systemfan = config.getObject("ipmisensor:2");
		ObjectDesc system12v = config.getObject("ipmisensor:3");
		ObjectDesc inlettemp = config.getObject("ipmisensor:4");

		ipmi.configure(config);
		ipmi.start();

		Serializable tempCPU = ipmi.getPropertyValue(cputemp, "lastValue");
		Serializable systemFan = ipmi.getPropertyValue(systemfan, "lastValue");
		Serializable system12V = ipmi.getPropertyValue(system12v, "lastValue");
		Serializable tempInlet = ipmi.getPropertyValue(inlettemp, "lastValue");
		System.out.println("******** CPU TEMP: " + tempCPU + "; SYSTEM FAN: " + systemFan + "; SYSTEM V12: "
		        + system12V + "; TEMP INLET: " + tempInlet + " ***********");

		ipmi.stop();
		ipmi.configure(config);
		ipmi.start();

		tempCPU = ipmi.getPropertyValue(cputemp, "lastValue");
		systemFan = ipmi.getPropertyValue(systemfan, "lastValue");
		system12V = ipmi.getPropertyValue(system12v, "lastValue");
		tempInlet = ipmi.getPropertyValue(inlettemp, "lastValue");
		System.out.println("******** CPU TEMP: " + tempCPU + "; SYSTEM FAN: " + systemFan + "; SYSTEM V12: "
		        + system12V + "; TEMP INLET: " + tempInlet + " ***********");

		ipmi.stop();

	}

}
