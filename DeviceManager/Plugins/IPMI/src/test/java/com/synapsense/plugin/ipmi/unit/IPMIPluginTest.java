package com.synapsense.plugin.ipmi.unit;

import static org.junit.Assert.assertEquals;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mockit.Deencapsulation;
import mockit.Mocked;
import mockit.NonStrictExpectations;

import org.junit.Before;
import org.junit.Test;

import com.synapsense.plugin.api.exceptions.IncorrectStateException;
import com.synapsense.plugin.apinew.ObjectDesc;
import com.synapsense.plugin.apinew.Plugin;
import com.synapsense.plugin.ipmi.IPMIPlugin;
import com.synapsense.plugin.ipmi.IPMIPropertyRW;
import com.synapsense.plugin.objects.IpmiServer;

public class IPMIPluginTest {

	@Mocked
	private IpmiServer server1;

	@Mocked
	private IpmiServer server2;

	@Mocked
	private IpmiServer server3;

	@Mocked
	private IpmiServer server4;

	@Mocked
	private ObjectDesc sensor;

	@Mocked
	private ObjectDesc server;

	@Mocked
	private IPMIPropertyRW propertyRW;

	private Map<String, IpmiServer> serverdestination = new HashMap<String, IpmiServer>();

	private Plugin ipmi;

	@Before
	public void beforeTest() {
		ipmi = new IPMIPlugin();
		serverdestination.clear();
		serverdestination.put("ipmiserver:1", server1);
		serverdestination.put("ipmiserver:2", server2);
		serverdestination.put("ipmiserver:3", server3);
		serverdestination.put("ipmiserver:4", server4);
	}

	@Test
	public void startTest() throws IncorrectStateException {
		Deencapsulation.setField(ipmi, "serverdestination", serverdestination);
		Deencapsulation.setField(ipmi, "configured", true);
		ipmi.start();
		ipmi.stop();

	}

	@SuppressWarnings("static-access")
	@Test
	public void getPropertyTest() {
		Deencapsulation.setField(ipmi, "serverdestination", serverdestination);
		new NonStrictExpectations() {
			{
				sensor.getParent();
				result = server;

				server.getID();
				result = "ipmiserver:1";

				propertyRW.readProperty(server1, (String) any);
				result = 5.0;
			}
		};
		Serializable value = ipmi.getPropertyValue(sensor, "lastValue");
		assertEquals(5.0, value);
	}

	@SuppressWarnings("static-access")
	@Test
	public void getPropertyValues() {
		Deencapsulation.setField(ipmi, "serverdestination", serverdestination);
		new NonStrictExpectations() {
			{
				sensor.getParent();
				result = server;

				server.getID();
				result = "ipmiserver:1";

				propertyRW.readProperty(server1, (String) any);
				result = 5.0;
			}
		};
		List<String> properties = new ArrayList<String>();
		properties.add("lastValue");
		List<Serializable> value = ipmi.getPropertyValue(sensor, properties);
		assertEquals(5.0, value.get(0));
	}
}
