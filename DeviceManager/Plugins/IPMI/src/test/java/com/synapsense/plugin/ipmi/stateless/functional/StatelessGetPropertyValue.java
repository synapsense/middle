package com.synapsense.plugin.ipmi.stateless.functional;

import java.io.Serializable;
import java.util.Properties;

import com.synapsense.plugin.ipmi.stateless.IPMIStatelessConstant;
import com.synapsense.plugin.ipmi.stateless.IPMIStatelessPlugin;

public class StatelessGetPropertyValue {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Properties cputemps = new Properties();
		cputemps.put(IPMIStatelessConstant.IPMI_PLUGIN_ADDRESS, "192.168.102.27");
		cputemps.put(IPMIStatelessConstant.IPMI_SERVER_ADDRESS, "172.30.201.204");
		cputemps.put(IPMIStatelessConstant.IPMI_SERVER_PRIVILEGE_LEVEL, "Administrator");
		cputemps.put(IPMIStatelessConstant.IPMI_SERVER_USER, "admin");
		cputemps.put(IPMIStatelessConstant.IPMI_SERVER_PASSWORD, "admin");
		cputemps.put(IPMIStatelessConstant.IPMI_SENSOR_ID, "1280");

		Properties systemfan = new Properties();
		systemfan.put(IPMIStatelessConstant.IPMI_SERVER_ADDRESS, "172.30.201.204");
		systemfan.put(IPMIStatelessConstant.IPMI_SERVER_PRIVILEGE_LEVEL, "Administrator");
		systemfan.put(IPMIStatelessConstant.IPMI_SERVER_USER, "admin");
		systemfan.put(IPMIStatelessConstant.IPMI_SERVER_PASSWORD, "admin");
		systemfan.put(IPMIStatelessConstant.IPMI_SENSOR_ID, "256");

		Properties systemv12 = new Properties();
		systemv12.put(IPMIStatelessConstant.IPMI_SERVER_ADDRESS, "172.30.201.204");
		systemv12.put(IPMIStatelessConstant.IPMI_SERVER_PRIVILEGE_LEVEL, "Administrator");
		systemv12.put(IPMIStatelessConstant.IPMI_SERVER_USER, "admin");
		systemv12.put(IPMIStatelessConstant.IPMI_SERVER_PASSWORD, "admin");
		systemv12.put(IPMIStatelessConstant.IPMI_SENSOR_ID, "640");

		Properties inlettemp = new Properties();
		inlettemp.put(IPMIStatelessConstant.IPMI_SERVER_ADDRESS, "172.30.201.204");
		inlettemp.put(IPMIStatelessConstant.IPMI_SERVER_PRIVILEGE_LEVEL, "Administrator");
		inlettemp.put(IPMIStatelessConstant.IPMI_SERVER_USER, "admin");
		inlettemp.put(IPMIStatelessConstant.IPMI_SERVER_PASSWORD, "admin");
		inlettemp.put(IPMIStatelessConstant.IPMI_SENSOR_ID, "1536");

		IPMIStatelessPlugin plg = new IPMIStatelessPlugin();

		while (true) {
			Serializable tempCPU = plg.getPropertyValue(cputemps);
			Serializable systemFan = plg.getPropertyValue(systemfan);
			Serializable system12V = plg.getPropertyValue(systemv12);
			Serializable tempInlet = plg.getPropertyValue(inlettemp);

			System.out.println("******** CPU TEMP: " + tempCPU + "; SYSTEM FAN: " + systemFan + "; SYSTEM V12: "
			        + system12V + "; TEMP INLET: " + tempInlet + " ***********");
		}

	}

}
