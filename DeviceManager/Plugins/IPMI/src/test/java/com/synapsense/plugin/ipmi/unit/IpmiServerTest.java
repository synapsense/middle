package com.synapsense.plugin.ipmi.unit;

import static org.junit.Assert.assertTrue;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import mockit.Deencapsulation;
import mockit.Mocked;
import mockit.NonStrictExpectations;

import org.junit.Before;
import org.junit.Test;

import com.synapsense.plugin.api.exceptions.CreateDeviceException;
import com.synapsense.plugin.objects.IpmiServer;
import com.veraxsystems.vxipmi.api.async.ConnectionHandle;
import com.veraxsystems.vxipmi.api.sync.IpmiConnector;
import com.veraxsystems.vxipmi.coding.security.CipherSuite;

public class IpmiServerTest {

	private IpmiServer server;

	@Mocked
	private IpmiConnector connector;
	@Mocked
	private ConnectionHandle handle;

	private List<CipherSuite> suites = new ArrayList<CipherSuite>();
	@Mocked
	private CipherSuite suite;

	@Before
	public void beforeTest() throws CreateDeviceException {
		server = new IpmiServer("172.30.201.204", "admin", "admin", "");
		suites.add(suite);
	}

	@Test
	public void createServerTest() throws UnknownHostException {
		assertTrue(InetAddress.getByName("172.30.201.204").equals(server.getAddress()));
		assertTrue("admin".equals(server.getUser()));
		assertTrue("admin".equals(server.getPassword()));

	}

	@Test
	public void testOpenSession() throws Exception {
		new NonStrictExpectations() {
			{
				connector.createConnection((InetAddress) any);
				result = handle;

				connector.getAvailableCipherSuites((ConnectionHandle) any);
				result = suites;

				suite.getId();
				result = (byte) 3;

				handle.getCipherSuite();
				result = suite;

			}
		};
		server.openSession();
	}

	@Test
	public void testCloseSession() {
		Deencapsulation.setField(server, "connector", connector);
		server.closeSession();
	}

}
