package com.synapsense.plugin.ipmi.library;

import java.net.InetAddress;
import java.util.List;

import org.apache.log4j.Logger;

import com.veraxsystems.vxipmi.api.async.ConnectionHandle;
import com.veraxsystems.vxipmi.api.sync.IpmiConnector;
import com.veraxsystems.vxipmi.coding.commands.IpmiVersion;
import com.veraxsystems.vxipmi.coding.commands.PrivilegeLevel;
import com.veraxsystems.vxipmi.coding.commands.sdr.GetSensorReading;
import com.veraxsystems.vxipmi.coding.commands.sdr.GetSensorReadingResponseData;
import com.veraxsystems.vxipmi.coding.protocol.AuthenticationType;
import com.veraxsystems.vxipmi.coding.security.CipherSuite;

public class LibraryStressTest {

	private static Logger log = Logger.getLogger(LibraryStressTest.class);

	public static void main(String[] args) throws Exception {

		while (true) {

			IpmiConnector connector = new IpmiConnector(0);
			ConnectionHandle handle = connector.createConnection(InetAddress.getByName("172.30.201.204"));
			handle.setPrivilegeLevel(PrivilegeLevel.Administrator);
			List<CipherSuite> suites = connector.getAvailableCipherSuites(handle);
			CipherSuite suite = suites.get(2);
			connector.getChannelAuthenticationCapabilities(handle, suite, PrivilegeLevel.Administrator);

			GetSensorReading sensorreading;
			GetSensorReadingResponseData readingResponse;

			connector.openSession(handle, "admin", "admin", null);

			sensorreading = new GetSensorReading(IpmiVersion.V20, handle.getCipherSuite(), AuthenticationType.RMCPPlus,
			        34);
			readingResponse = (GetSensorReadingResponseData) connector.sendMessage(handle, sensorreading);

			log.warn("!!!!!!!!!!" + readingResponse.getSensorState());

			connector.closeSession(handle);
			connector.closeConnection(handle);
			connector.tearDown();
		}
	}

}
