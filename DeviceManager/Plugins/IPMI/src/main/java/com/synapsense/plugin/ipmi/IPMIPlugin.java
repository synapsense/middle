package com.synapsense.plugin.ipmi;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.synapsense.plugin.api.exceptions.ConfigPluginException;
import com.synapsense.plugin.api.exceptions.CreateDeviceException;
import com.synapsense.plugin.api.exceptions.IncorrectStateException;
import com.synapsense.plugin.api.exceptions.UnableGetDataException;
import com.synapsense.plugin.apinew.ObjectDesc;
import com.synapsense.plugin.apinew.Plugin;
import com.synapsense.plugin.apinew.PluginConfig;
import com.synapsense.plugin.apinew.Transport;
import com.synapsense.plugin.objects.IpmiServer;

public class IPMIPlugin implements Plugin {

	private final static String ID = "ipmi";
	private static Properties PLUGIN_DESC = new Properties();
	static {
		PLUGIN_DESC.put(PLUGIN_ID, ID);
		PLUGIN_DESC.put(ROOT_TYPES, IPMIPluginConstant.IPMI_NETWORK);
	}
	private static final Logger logger = Logger.getLogger(IPMIPlugin.class);
	private Map<String, IpmiServer> serverdestination = new HashMap<String, IpmiServer>();
	private Transport transport = null;
	private boolean configured = false;
	private boolean started = false;

	@Override
	public String getID() {
		return ID;
	}

	@Override
	public Properties getPluginDescription() {
		return PLUGIN_DESC;
	}

	@Override
	public void start() throws IncorrectStateException {
		if (!configured) {
			throw new IncorrectStateException("Plugin " + ID + " has to be configured before start.");
		}
		if (started) {
			throw new IncorrectStateException("Plugin " + ID + "has already started.");
		}

		for (IpmiServer server : serverdestination.values()) {
			try {
				server.openSession();
			} catch (IncorrectStateException e) {
				logger.error("could not connect to [" + server.getAddress().toString() + "]", e);
			}
		}
		if (serverdestination.size() == 0) {
			throw new IncorrectStateException("Plugin " + ID + " has not started.");
		}
		started = true;
		logger.debug("Plugin " + ID + " is started.");

	}

	@Override
	public void stop() {
		if (started) {
			for (IpmiServer server : serverdestination.values()) {
				if (server.isSessionOpened()) {
					server.closeSession();
				}
			}
			serverdestination.clear();
			started = false;
			logger.debug("Plugin " + ID + " is stopped");

		}
	}

	@Override
	public void setPropertyValue(ObjectDesc object, String propertyName, Serializable value) {
		logger.warn("Could not set property value for sensor. Method is not implemented");

	}

	@Override
	public void setPropertyValue(ObjectDesc object, List<String> propertyName, List<Serializable> values) {
		logger.warn("Could not set property value for sensor. Method is not implemented");

	}

	@Override
	public Serializable getPropertyValue(ObjectDesc object, String propertyName) {
		if (object == null) {
			throw new IllegalArgumentException("object could not be null");
		}
		ObjectDesc serverObject = object.getParent();
		IpmiServer server = serverdestination.get(serverObject.getID());
		if (server == null) {
			throw new IllegalArgumentException("could not find session with server [" + serverObject.getID());
		}
		try {
			Serializable value = IPMIPropertyRW.readProperty(server,
			        object.getPropertyValue(IPMIPluginConstant.IPMI_SENSOR_ID));
			return value;
		} catch (UnableGetDataException e) {
			logger.warn("Could not get value of sensor [ID: "
			        + object.getPropertyValue(IPMIPluginConstant.IPMI_SENSOR_ID) + "] from: " + server.getAddress() + e);
			throw e;
		}

	}

	@Override
	public List<Serializable> getPropertyValue(ObjectDesc object, List<String> propertyNames) {
		List<Serializable> values = new ArrayList<Serializable>();
		values.add(getPropertyValue(object, "lastValue"));
		return values;
	}

	@Override
	public void configure(PluginConfig config) throws ConfigPluginException {
		if (config == null) {
			throw new IllegalArgumentException("config cannot be null");
		}
		stop();
		List<ObjectDesc> networks = config.getRoots();

		for (ObjectDesc network : networks) {
			String plgaddress = network.getPropertyValue(IPMIPluginConstant.IPMI_NETWORK_ADDRESS);
			List<ObjectDesc> servers = network.getChildren();
			for (ObjectDesc server : servers) {
				String serveraddress = server.getPropertyValue(IPMIPluginConstant.IPMI_SERVER_ADDRESS);
				String serveruser = server.getPropertyValue(IPMIPluginConstant.IPMI_SERVER_USER);
				String serverpassword = server.getPropertyValue(IPMIPluginConstant.IPMI_SERVER_PASSWORD);
				try {
					IpmiServer ipmiserver = new IpmiServer(serveraddress, serveruser, serverpassword, plgaddress);
					serverdestination.put(server.getID(), ipmiserver);

				} catch (CreateDeviceException e) {
					throw new ConfigPluginException("Could not add server [" + serveraddress + "] to destination list",
					        e);
				}
			}
		}
		configured = true;
		logger.debug("Plugin " + ID + " is configured.");

	}

	@Override
	public void setTransport(Transport transport) {
		this.transport = transport;

	}

	public Transport getTransport() {
		return transport;
	}

}
