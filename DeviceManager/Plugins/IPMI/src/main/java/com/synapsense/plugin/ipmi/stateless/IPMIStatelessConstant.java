package com.synapsense.plugin.ipmi.stateless;

public interface IPMIStatelessConstant {

	public static final String IPMI_PLUGIN_ADDRESS = "ipmiplgaddress";
	public static final String IPMI_SERVER_ADDRESS = "ipmiserveraddress";
	public static final String IPMI_SERVER_PRIVILEGE_LEVEL = "ipmiserverprivilege";
	public static final String IPMI_SERVER_USER = "ipmiserveruser";
	public static final String IPMI_SERVER_PASSWORD = "ipmiserverpassword";
	public static final String IPMI_SENSOR_ID = "ipmisensorid";

	public static final Integer SDR_REQUEST_OFFSET_PART1 = 0;
	public static final Integer SDR_REQUEST_OFFSET_PART2 = 16;
	public static final Integer SDR_REQUEST_OFFSET_PART3 = 32;
	public static final Integer SDR_REQUEST_OFFSET_PART4 = 48;
	public static final Integer SDR_REQUEST_BYTES_TO_READ = 16;

	public static final String PRIVILEGE_LEVEL_CALLBACK = "Callback";
	public static final String PRIVILEGE_LEVEL_USER = "User";
	public static final String PRIVILEGE_LEVEL_OPERATOR = "Operator";
	public static final String PRIVILEGE_LEVEL_ADMINISTRATOR = "Administrator";
	public static final String PRIVILEGE_LEVEL_MAXIMUM = "MaximumAvailable";

}
