package com.synapsense.plugin.objects;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.List;

import org.apache.log4j.Logger;

import com.synapsense.plugin.api.exceptions.CreateDeviceException;
import com.synapsense.plugin.api.exceptions.IncorrectStateException;
import com.synapsense.plugin.ipmi.IPMIPlugin;
import com.synapsense.plugin.ipmi.IPMIPluginConstant;
import com.veraxsystems.vxipmi.api.async.ConnectionHandle;
import com.veraxsystems.vxipmi.api.sync.IpmiConnector;
import com.veraxsystems.vxipmi.coding.commands.IpmiCommandCoder;
import com.veraxsystems.vxipmi.coding.commands.IpmiVersion;
import com.veraxsystems.vxipmi.coding.commands.PrivilegeLevel;
import com.veraxsystems.vxipmi.coding.commands.sdr.ReserveSdrRepository;
import com.veraxsystems.vxipmi.coding.commands.sdr.ReserveSdrRepositoryResponseData;
import com.veraxsystems.vxipmi.coding.protocol.AuthenticationType;
import com.veraxsystems.vxipmi.coding.security.CipherSuite;

public class IpmiServer {

	private InetAddress address;
	private InetAddress plgaddress;
	private String user;
	private String password;

	private IpmiConnector connector;
	private ConnectionHandle handle;

	private boolean opensession = false;

	private static final Logger logger = Logger.getLogger(IPMIPlugin.class);

	public IpmiServer(String address, String user, String password, String plgaddress) throws CreateDeviceException {
		try {
			if (!plgaddress.equals("")) {
				this.plgaddress = InetAddress.getByName(plgaddress);
				if (!this.plgaddress.isAnyLocalAddress()) {
					throw new CreateDeviceException("Could not find network interface with address: " + plgaddress);
				}
			}
		} catch (UnknownHostException e) {
			throw new CreateDeviceException("Address of network interface is invalid", e);
		}

		try {
			this.user = user;
			this.password = password;
			this.address = InetAddress.getByName(address);
		} catch (UnknownHostException e) {
			throw new CreateDeviceException("Host [" + address + "] is invalid", e);
		}

	}

	public InetAddress getAddress() {
		return address;
	}

	public String getUser() {
		return user;
	}

	public String getPassword() {
		return password;
	}

	public IpmiConnector getConnector() {
		return connector;
	}

	public ConnectionHandle getHandle() {
		return handle;
	}

	public synchronized int getReservationId() throws Exception {
		IpmiCommandCoder requestReserve = new ReserveSdrRepository(IpmiVersion.V20, handle.getCipherSuite(),
		        AuthenticationType.RMCPPlus);
		ReserveSdrRepositoryResponseData responseReserve = (ReserveSdrRepositoryResponseData) connector.sendMessage(
		        handle, requestReserve);
		Integer reservationId = responseReserve.getReservationId();
		return reservationId;
	}

	public void openSession() throws IncorrectStateException {

		try {
			if ((plgaddress != null)) {
				connector = new IpmiConnector(0, plgaddress);
			} else {
				connector = new IpmiConnector(0);
			}
			handle = connector.createConnection(address);
			handle.setPrivilegeLevel(PrivilegeLevel.Administrator);
			List<CipherSuite> suites = connector.getAvailableCipherSuites(handle);

			CipherSuite suite = null;
			for (CipherSuite s : suites) {
				if (s.getId() == IPMIPluginConstant.CIPHER_SUITE_ID) {
					suite = s;
					break;
				}
			}
			if (suite == null) {
				throw new IncorrectStateException(
				        "Could not get Cipher Suite from server, probably server does not support IPMI");
			}

			connector.getChannelAuthenticationCapabilities(handle, suite, PrivilegeLevel.Administrator);

			connector.openSession(handle, user, password, null);
			opensession = true;

		} catch (Exception e) {
			throw new IncorrectStateException("could not open session");
		}

	}

	public void closeSession() {
		try {
			connector.closeSession(handle);
			connector.closeConnection(handle);
		} catch (Exception e) {
			logger.debug("could not close session", e);
		}
		connector.tearDown();
		opensession = false;
	}

	public boolean isSessionOpened() {
		return opensession;
	}

}
