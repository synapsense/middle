package com.synapsense.plugin.ipmi;

import org.apache.log4j.Logger;

import com.synapsense.plugin.api.exceptions.UnableGetDataException;
import com.synapsense.plugin.objects.IpmiServer;
import com.veraxsystems.vxipmi.api.async.ConnectionHandle;
import com.veraxsystems.vxipmi.api.sync.IpmiConnector;
import com.veraxsystems.vxipmi.coding.commands.IpmiVersion;
import com.veraxsystems.vxipmi.coding.commands.sdr.GetSdr;
import com.veraxsystems.vxipmi.coding.commands.sdr.GetSdrResponseData;
import com.veraxsystems.vxipmi.coding.commands.sdr.GetSensorReading;
import com.veraxsystems.vxipmi.coding.commands.sdr.GetSensorReadingResponseData;
import com.veraxsystems.vxipmi.coding.commands.sdr.record.FullSensorRecord;
import com.veraxsystems.vxipmi.coding.protocol.AuthenticationType;

public class IPMIPropertyRW {

	private static final Logger logger = Logger.getLogger(IPMIPropertyRW.class);

	public static Double readProperty(IpmiServer server, String sensorid) {

		IpmiConnector connector = server.getConnector();
		ConnectionHandle handle = server.getHandle();

		Integer id = Integer.parseInt(sensorid);

		try {

			synchronized (server) {

				int reservationId = server.getReservationId();

				GetSdr requestSdrPart1 = new GetSdr(IpmiVersion.V20, handle.getCipherSuite(),
				        AuthenticationType.RMCPPlus, reservationId, id, IPMIPluginConstant.SDR_REQUEST_OFFSET_PART1,
				        IPMIPluginConstant.SDR_REQUEST_BYTES_TO_READ);
				GetSdrResponseData responseSdrPart1 = (GetSdrResponseData) connector.sendMessage(handle,
				        requestSdrPart1);
				logger.debug("Get SDR part: 1");
				byte[] part1 = responseSdrPart1.getSensorRecordData();

				reservationId = server.getReservationId();

				GetSdr requestSdrPart2 = new GetSdr(IpmiVersion.V20, handle.getCipherSuite(),
				        AuthenticationType.RMCPPlus, reservationId, id, IPMIPluginConstant.SDR_REQUEST_OFFSET_PART2,
				        IPMIPluginConstant.SDR_REQUEST_BYTES_TO_READ);
				GetSdrResponseData responseSdrPart2 = (GetSdrResponseData) connector.sendMessage(handle,
				        requestSdrPart2);
				logger.debug("Get SDR part: 2");
				byte[] part2 = responseSdrPart2.getSensorRecordData();

				reservationId = server.getReservationId();

				GetSdr requestSdrPart3 = new GetSdr(IpmiVersion.V20, handle.getCipherSuite(),
				        AuthenticationType.RMCPPlus, reservationId, id, IPMIPluginConstant.SDR_REQUEST_OFFSET_PART3,
				        IPMIPluginConstant.SDR_REQUEST_BYTES_TO_READ);
				GetSdrResponseData responseSdrPart3 = (GetSdrResponseData) connector.sendMessage(handle,
				        requestSdrPart3);
				logger.debug("Get SDR part: 3");
				byte[] part3 = responseSdrPart3.getSensorRecordData();

				reservationId = server.getReservationId();
				GetSdr requestSdrPart4 = new GetSdr(IpmiVersion.V20, handle.getCipherSuite(),
				        AuthenticationType.RMCPPlus, reservationId, id, IPMIPluginConstant.SDR_REQUEST_OFFSET_PART4,
				        IPMIPluginConstant.SDR_REQUEST_BYTES_TO_READ);
				GetSdrResponseData responseSdrPart4 = (GetSdrResponseData) connector.sendMessage(handle,
				        requestSdrPart4);
				logger.debug("Get SDR part: 4");

				byte[] part4 = responseSdrPart4.getSensorRecordData();

				byte[] response = new byte[part1.length + part2.length + part3.length + part4.length];

				for (int i = 0; i < part1.length; i++) {
					response[i] = part1[i];
				}

				for (int i = 0; i < part2.length; i++) {
					response[i + part1.length] = part2[i];
				}

				for (int i = 0; i < part3.length; i++) {
					response[i + part1.length + part2.length] = part3[i];
				}

				for (int i = 0; i < part4.length; i++) {
					response[i + part1.length + part2.length + part3.length] = part4[i];
				}

				FullSensorRecord fullrecord = (FullSensorRecord) FullSensorRecord.populateSensorRecord(response);
				int sensorNumber = 0xFF & fullrecord.getSensorNumber();
				GetSensorReading sensorreading = new GetSensorReading(IpmiVersion.V20, handle.getCipherSuite(),
				        AuthenticationType.RMCPPlus, sensorNumber);
				GetSensorReadingResponseData readingResponse = (GetSensorReadingResponseData) connector.sendMessage(
				        handle, sensorreading);
				logger.debug("Get Sensor Reading");

				Double value = readingResponse.getSensorReading(fullrecord);
				return value;
			}

		} catch (Throwable t) {
			throw new UnableGetDataException("Could not get sensor value" + t);
		}

	}

}
