package com.synapsense.plugin.ipmi;

public interface IPMIPluginConstant {
	public static final String IPMI_NETWORK = "ipminetwork";
	public static final String IPMI_SERVER = "ipmiserver";
	public static final String IPMI_SENSOR = "ipmisensor";

	public static final String IPMI_NETWORK_NAME = "name";
	public static final String IPMI_NETWORK_ADDRESS = "address";

	public static final String IPMI_SERVER_NAME = "name";
	public static final String IPMI_SERVER_ADDRESS = "address";
	public static final String IPMI_SERVER_PRIVILEGE_LEVEL = "privilegelevel";
	public static final String IPMI_SERVER_USER = "user";
	public static final String IPMI_SERVER_PASSWORD = "password";

	public static final String IPMI_SENSOR_NAME = "name";
	public static final String IPMI_SENSOR_ID = "sensorid";
	public static final String IPMI_SENSOR_VALUE = "lastValue";

	public static final Integer SDR_REQUEST_OFFSET_PART1 = 0;
	public static final Integer SDR_REQUEST_OFFSET_PART2 = 16;
	public static final Integer SDR_REQUEST_OFFSET_PART3 = 32;
	public static final Integer SDR_REQUEST_OFFSET_PART4 = 48;
	public static final Integer SDR_REQUEST_BYTES_TO_READ = 16;

	public static final String PRIVILEGE_LEVEL_CALLBACK = "Callback";
	public static final String PRIVILEGE_LEVEL_USER = "User";
	public static final String PRIVILEGE_LEVEL_OPERATOR = "Operator";
	public static final String PRIVILEGE_LEVEL_ADMINISTRATOR = "Administrator";
	public static final String PRIVILEGE_LEVEL_MAXIMUM = "MaximumAvailable";

	public static final byte CIPHER_SUITE_ID = 3;

}
