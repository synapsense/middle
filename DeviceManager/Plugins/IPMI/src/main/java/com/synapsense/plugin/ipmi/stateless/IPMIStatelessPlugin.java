package com.synapsense.plugin.ipmi.stateless;

import java.io.Serializable;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.synapsense.plugin.api.exceptions.UnableGetDataException;
import com.synapsense.plugin.apinew.StatelessPlugin;
import com.synapsense.plugin.ipmi.IPMIPropertyRW;
import com.synapsense.plugin.objects.IpmiServer;

public class IPMIStatelessPlugin implements StatelessPlugin {

	private static final String ID = "ipmi";
	private static final Logger logger = Logger.getLogger(IPMIStatelessPlugin.class);

	@Override
	public String getID() {
		return ID;
	}

	@Override
	public void setPropertyValue(Properties params, Serializable newValue) {
		logger.warn("Could not set property value for sensor. Method is not implemented");

	}

	@Override
	public Serializable getPropertyValue(Properties params) {
		try {
			checkParams(params);
			String plgaddress = params.getProperty(IPMIStatelessConstant.IPMI_PLUGIN_ADDRESS, "");

			String serveraddres = params.getProperty(IPMIStatelessConstant.IPMI_SERVER_ADDRESS);
			String serveruser = params.getProperty(IPMIStatelessConstant.IPMI_SERVER_USER);
			String serverpassword = params.getProperty(IPMIStatelessConstant.IPMI_SERVER_PASSWORD);

			String id = params.getProperty(IPMIStatelessConstant.IPMI_SENSOR_ID);

			IpmiServer server = new IpmiServer(serveraddres, serveruser, serverpassword, plgaddress);

			server.openSession();

			Double value = IPMIPropertyRW.readProperty(server, id);

			server.closeSession();

			return value;

		} catch (Throwable t) {
			throw new UnableGetDataException("could not get data", t);
		}

	}

	private boolean checkParams(Properties params) throws IllegalArgumentException {
		if (!(params.containsKey(IPMIStatelessConstant.IPMI_SERVER_ADDRESS))) {
			throw new IllegalArgumentException("Missing " + IPMIStatelessConstant.IPMI_SERVER_ADDRESS + " parameter");
		}
		if (!params.containsKey(IPMIStatelessConstant.IPMI_SERVER_PRIVILEGE_LEVEL)) {
			throw new IllegalArgumentException("Missing " + IPMIStatelessConstant.IPMI_SERVER_PRIVILEGE_LEVEL
			        + "parameter");
		}
		if (!params.containsKey(IPMIStatelessConstant.IPMI_SERVER_USER)) {
			throw new IllegalArgumentException("Missing " + IPMIStatelessConstant.IPMI_SERVER_USER + " parameter");
		}
		if (!params.containsKey(IPMIStatelessConstant.IPMI_SERVER_PASSWORD)) {
			throw new IllegalArgumentException("Missing " + IPMIStatelessConstant.IPMI_SERVER_PASSWORD + " parameter");
		}
		if (!params.containsKey(IPMIStatelessConstant.IPMI_SENSOR_ID)) {
			throw new IllegalArgumentException("Missing " + IPMIStatelessConstant.IPMI_SENSOR_ID + " parameter");
		}

		return true;
	}
}
