#!/bin/ash

# define some overridable variables first
#var2=${var2:-foo} # same thing; just in case the short version won't work in Yoda ash
: ${YODA_CONFIG_DIR:="/mnt/misc"}
: ${DM_BASE_DIR:="$BASEDIR"}
: ${DM_DATA_DIR:="$YODA_CONFIG_DIR/dm-data"}
: ${DM_LOG_FILE:="/var/log/dm.log"}
: ${YODAUI_BASE_DIR:="/opt/yoda-admin"}
: ${IPCONF:="$YODAUI_BASE_DIR/bin/ipconfigurator"}

DM_DEFAULT_CONF_DIR="$DM_BASE_DIR/conf"
DM_TARGET_CONF_DIR="$DM_DATA_DIR/conf"

# some logging helpers to mimick Java logging
log()
{
	echo `date +"%Y-%m-%d %H:%M:%S.000 %Z"` "[native] $1 $0 - $2" >> "$DM_LOG_FILE"
}
log_info()
{
	log INFO "$1"
}
log_warn()
{
	log WARN "$1"
}
log_error()
{
	log ERROR "$1"
}

# finds the first non-corrupted config file and
# sets its name to the variable passed as an argument
get_config()
{
	local config=`$IPCONF -c`
	eval "$1=$config"
}

# returns property value from Java properties file
# param $1 filename
# param $2 property name
# param $3 variable to put the value to
get_prop_val()
{
	eval "$3=`sed '/^\#/d' $1 | grep $2  | tail -n 1 | cut -d \"=\" -f2- | sed 's/^[[:space:]]*//;s/[[:space:]]*$//'`"
}

# same as get_prop_val but prints error message and exits
# if parameter's value is empty or not found
require_prop_val()
{
	local ret=`sed '/^\#/d' $1 | grep $2  | tail -n 1 | cut -d "=" -f2- | sed 's/^[[:space:]]*//;s/[[:space:]]*$//'`
	if [ -z $ret ] ; then
		log_error "'Property $2 is not defined in file $1. Exiting.'"
		return 1
	fi
	eval "$3=$ret"
}

get_host()
{
	local DATA_IP=`/sbin/ip -4 address show eth0 | grep 'inet' | sed 's/.*inet\(6\)\? \(.*\)\/.*/\2/'`
	if [ -z $DATA_IP ] ; then
		DATA_IP=`/sbin/ip -6 address show eth0 | grep 'inet' | sed 's/.*inet\(6\)\? \(.*\)\/.*/\2/'`
		if [ -z $DATA_IP ] ; then
			log_warn "'Data Port interface has no IP assigned (is it up?)'"
		    DATA_IP=""
		fi
	fi
	eval "$1=$DATA_IP"
}

copy_default_files()
{
	# create target conf dir if needed
	mkdir -p "$DM_TARGET_CONF_DIR"

	# copy default conf files
	cp -r "$DM_DEFAULT_CONF_DIR"/* "$DM_TARGET_CONF_DIR"
}