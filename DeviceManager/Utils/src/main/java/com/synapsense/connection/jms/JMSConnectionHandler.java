package com.synapsense.connection.jms;

import java.io.IOException;
import java.util.Properties;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.ExceptionListener;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.naming.InitialContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.synapsense.connection.ConnectionHandlerBase;

public class JMSConnectionHandler extends ConnectionHandlerBase<JMSConnectionHandler.JMSConnection> {

	private static final Logger log = LoggerFactory.getLogger(JMSConnectionHandler.class);

	public static class JMSConnection {
		public final Connection connection;
		public final Session session;
		public final MessageProducer producer;

		public JMSConnection(Connection connection, Session session, MessageProducer producer) {
			this.connection = connection;
			this.session = session;
			this.producer = producer;
		}
	}

	private JMSConnection connection;
	private String destinationName;
	private Properties connectionProperties;

	public JMSConnectionHandler(String name, String destinationName, Properties connectionProperties) {
		super(name);
		this.destinationName = destinationName;
		this.connectionProperties = connectionProperties;
	}

	@Override
	public JMSConnection getConnection() {
		return connection;
	}

	@Override
	public void connect() throws Exception {
		InitialContext icx = new InitialContext(connectionProperties);
		ConnectionFactory conFactory = (ConnectionFactory) icx.lookup("jms/RemoteConnectionFactory");
		Connection conn = conFactory.createConnection();
		Destination dest = (Destination) icx.lookup(destinationName);
		Session session = conn.createSession(false, Session.AUTO_ACKNOWLEDGE);
		MessageProducer producer = session.createProducer(dest);

		connection = new JMSConnection(conn, session, producer);

		conn.setExceptionListener(new ExceptionListener() {
			@Override
			public void onException(JMSException exception) {
				try {
					close();
				} catch (Exception e) {
					log.debug("Error while closing connection", e);
				}
			}
		});

		conn.start();
		fireConnected();
	}

	@Override
	public boolean isConnected() {
		return connection != null;
	}

	@Override
	public void close() throws IOException {
		if (isConnected()) {
			try {
				if (connection.producer != null)
					connection.producer.close();
				if (connection.session != null)
					connection.session.close();
				if (connection.connection != null)
					connection.connection.close();
			} catch (JMSException e) {
				throw new IOException("Cannot close JMS connection", e);
			}
		}
		fireDisconnected();
	}

}
