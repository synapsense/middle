package com.synapsense.connection;

import java.util.concurrent.CopyOnWriteArraySet;

public abstract class ConnectionHandlerBase<T> implements ConnectionHandler<T> {

	private final String name;
	private CopyOnWriteArraySet<ConnectionStatusListener> listeners;

	protected long reconnectInterval = 60000;

	public ConnectionHandlerBase(String name) {
		this.name = name;
		listeners = new CopyOnWriteArraySet<>();
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public void addStatusListener(ConnectionStatusListener listener) {
		listeners.add(listener);
	}

	@Override
	public void removeStatusListener(ConnectionStatusListener listener) {
		listeners.remove(listener);
	}

	@Override
	public long getReconnectInterval() {
		return reconnectInterval;
	}

	public void setReconnectInterval(long interval) {
		if (interval < 0)
			throw new IllegalArgumentException("Negative reconnect interval");
		reconnectInterval = interval;
	}

	protected void fireConnected() {
		for (ConnectionStatusListener listener : listeners)
			listener.connected(this);
	}

	protected void fireDisconnected() {
		for (ConnectionStatusListener listener : listeners)
			listener.disconnected(this);
	}
}
