package com.synapsense.connection;

import java.io.Closeable;

public interface ConnectionHandler<T> extends Closeable {

	String getName();

	T getConnection();

	void connect() throws Exception;

	boolean isConnected();

	long getReconnectInterval();

	void addStatusListener(ConnectionStatusListener listener);

	void removeStatusListener(ConnectionStatusListener listener);
}
