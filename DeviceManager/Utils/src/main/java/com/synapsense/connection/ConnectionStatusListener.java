package com.synapsense.connection;

public interface ConnectionStatusListener {

	void connected(ConnectionHandler<?> connection);

	void disconnected(ConnectionHandler<?> connection);

}
