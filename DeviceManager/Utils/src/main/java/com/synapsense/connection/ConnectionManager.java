package com.synapsense.connection;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConnectionManager {

	private static final Logger log = LoggerFactory.getLogger(ConnectionManager.class);
	private static final ConnectionManager instance = new ConnectionManager();

	private Map<String, ConnectionHandler<?>> connections;
	private ScheduledExecutorService executor;

	private Reconnector reconnector;

	public static ConnectionManager getInstance() {
		return instance;
	}

	private ConnectionManager() {
		connections = new HashMap<>();

		executor = Executors.newSingleThreadScheduledExecutor(new ThreadFactory() {
			@Override
			public Thread newThread(Runnable r) {
				Thread t = new Thread(r, "ConnectionManager thread");
				t.setDaemon(true);
				return t;
			}
		});

		reconnector = new Reconnector();
	}

	synchronized public void addConnection(ConnectionHandler<?> connection) {
		if (connections.containsKey(connection.getName())) {
			throw new IllegalArgumentException("ConnectionHandler \"" + connection.getName()
			        + "\" is already registered; remove it first");
		}

		connection.addStatusListener(reconnector);
		connections.put(connection.getName(), connection);

		if (!connection.isConnected()) {
			reconnector.disconnected(connection);
		}
	}

	public void removeConnection(ConnectionHandler<?> connection) {
		removeConnection(connection.getName());
	}

	synchronized public void removeConnection(String connectionName) {
		ConnectionHandler<?> handler = connections.remove(connectionName);
		if (handler != null)
			handler.removeStatusListener(reconnector);
	}

	private class Reconnector implements ConnectionStatusListener {

		@Override
		public void connected(ConnectionHandler<?> connection) {
		}

		@Override
		public void disconnected(ConnectionHandler<?> connection) {
			reconnect(connection, 0);
		}

		private void reconnect(final ConnectionHandler<?> connection, long delay) {
			executor.schedule(new Runnable() {
				@Override
				public void run() {
					try {
						log.info("Connecting to {}", connection.getName());
						connection.connect();
						log.info("Connected to {}", connection.getName());
					} catch (Exception e) {
						log.warn("Failed to connect to " + connection.getName() + ", next attempt in " +
                        		connection.getReconnectInterval() + " msec", e);
						reconnect(connection, connection.getReconnectInterval());
					}
				}
			}, delay, TimeUnit.MILLISECONDS);
		}

	}
}
