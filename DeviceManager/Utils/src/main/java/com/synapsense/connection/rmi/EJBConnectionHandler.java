package com.synapsense.connection.rmi;

import java.io.IOException;
import java.util.Properties;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.synapsense.connection.ConnectionHandlerBase;

public class EJBConnectionHandler<T> extends ConnectionHandlerBase<T> {

	private String destinationName;
	private Properties connectionProperties;
	private InitialContext ctx;
	private T connection;

	public EJBConnectionHandler(String name, String destinationName, Properties connectionProperties) {
		super(name);
		this.destinationName = destinationName;
		this.connectionProperties = connectionProperties;
	}

	@Override
	public T getConnection() {
		return connection;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void connect() throws Exception {
		// EJBClientConfiguration ejbClientConfiguration = new
		// PropertiesBasedEJBClientConfiguration(connectionProperties);
		// ContextSelector<EJBClientContext> ejbContextSelector = new
		// ConfigBasedEJBClientContextSelector(
		// ejbClientConfiguration);
		//
		// EJBClientContext.setSelector(ejbContextSelector);
		//
		ctx = new InitialContext(connectionProperties);
		connection = (T) ctx.lookup(destinationName);
		fireConnected();
	}

	@Override
	public boolean isConnected() {
		return connection != null;
	}

	@Override
	public void close() throws IOException {
		try {
			if (ctx != null)
				ctx.close();
		} catch (NamingException e) {
			throw new IOException("Cannot close InitialContext", e);
		}
		connection = null;
		fireDisconnected();
	}

	public Properties getConnectionProperties() {
		return connectionProperties;
	}

	public void setConnectionProperties(Properties connectionProperties) {
		this.connectionProperties = connectionProperties;
	}

}
