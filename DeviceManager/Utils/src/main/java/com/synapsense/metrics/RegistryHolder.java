package com.synapsense.metrics;

import com.codahale.metrics.JmxReporter;
import com.codahale.metrics.MetricRegistry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 */
public class RegistryHolder {
	private MetricRegistry metricsRegistry;
	private final static Logger logger = LoggerFactory.getLogger(Metrics.class);

	public RegistryHolder() {
		metricsRegistry = new MetricRegistry();
		final JmxReporter reporter = JmxReporter.forRegistry(metricsRegistry).build();
		reporter.start();
	}

	MetricRegistry getMetricsRegistry() {
		return metricsRegistry;
	}

	public String name(Class clazz, String name) {
		return MetricRegistry.name(clazz, name);
	}

	public <T> void register(final Class clazz, final String name, final Gauge<T> gaugeWrapper) {
		String gaugeName = MetricRegistry.name(clazz, name);
		logger.info("Adding a new gauge [" + gaugeName + "]");
		metricsRegistry.register(gaugeName, new com.codahale.metrics.Gauge<T>() {
			@Override
			public T getValue() {
				return gaugeWrapper.getValue();
			}
		});
	}
}
