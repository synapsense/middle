package com.synapsense.metrics.wrapper;

import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.Timer;

/**
 */
public class TimerWrapper implements com.synapsense.metrics.Timer {
	private final Timer timer;
	private Timer.Context currentContext;

	public TimerWrapper(String name, MetricRegistry metricsRegistry) {
		timer = metricsRegistry.timer(name);
	}

	@Override
	public void time() {
		currentContext = timer.time();
	}

	@Override
	public void stop() {
		currentContext.stop();
	}
}
