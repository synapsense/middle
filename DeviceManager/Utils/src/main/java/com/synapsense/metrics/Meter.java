package com.synapsense.metrics;

/**
 */
public interface Meter {

	public void mark();

	public void mark(long n);

}
