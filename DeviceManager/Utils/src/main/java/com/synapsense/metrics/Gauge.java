package com.synapsense.metrics;

/**
 */
public interface Gauge<T> {

    public T getValue();

}
