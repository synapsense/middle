package com.synapsense.metrics;

public interface Timer {

	public void time();

	public void stop();

}
