package com.synapsense.metrics.wrapper;

import com.codahale.metrics.MetricRegistry;
import com.synapsense.metrics.Meter;
import com.synapsense.metrics.Metrics;
import com.synapsense.metrics.MetricsFactory;
import com.synapsense.metrics.Timer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class WrapperMetricsFactory implements MetricsFactory {
    private final MetricRegistry metricsRegistry;
    private final static Logger logger = LoggerFactory.getLogger(Metrics.class);

    public WrapperMetricsFactory(MetricRegistry metricsRegistry) {
        this.metricsRegistry = metricsRegistry;
    }

    @Override
	public Timer getTimer(Class clazz, String name) {
        String timerName = MetricRegistry.name(clazz, name);
        logger.info("Adding a new timer [" + timerName + "]");
		return new TimerWrapper(timerName, metricsRegistry);
	}

	@Override
	public Meter getMeter(Class clazz, String name) {
        String meeterName = MetricRegistry.name(clazz, name);
        logger.info("Adding a new meeter [" + meeterName + "]");
		return new MeterWrapper(meeterName, metricsRegistry);
	}
}
