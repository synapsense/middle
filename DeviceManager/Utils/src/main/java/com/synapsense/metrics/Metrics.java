package com.synapsense.metrics;

import com.synapsense.metrics.stub.StubMetricsFactory;
import com.synapsense.metrics.wrapper.WrapperMetricsFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 */
public class Metrics {
	private final static Logger logger = LoggerFactory.getLogger(Metrics.class);
	private static Metrics instance = new Metrics();
	private RegistryHolder registryHolder;
	private MetricsFactory currentFactory;

	public Metrics() {
		try {
			Class.forName("com.codahale.metrics.MetricRegistry");
			// metrics library is in the classpath and we can proceed with
			// creating Registry and Reporter
			logger.info("Creating metrics registry and metrics JMX reporter...");
			registryHolder = new RegistryHolder();
			currentFactory = new WrapperMetricsFactory(registryHolder.getMetricsRegistry());
		} catch (ClassNotFoundException e) {
			currentFactory = new StubMetricsFactory();
		}

	}

	// logger always starts first and initialize the singleton, no need for
	// locking
	public static Metrics getInstance() {
		return instance;
	}

	public MetricsFactory getFactory() {
		return currentFactory;
	}

	public <T> void register(final Class clazz, final String name, final Gauge<T> gaugeWrapper) {
		if (registryHolder != null) {
			registryHolder.register(clazz, name, gaugeWrapper);
		}
	}
}
