package com.synapsense.metrics.wrapper;

import com.codahale.metrics.Meter;
import com.codahale.metrics.MetricRegistry;

/**
 */
public class MeterWrapper implements com.synapsense.metrics.Meter {
	private final Meter meter;

	public MeterWrapper(String name, MetricRegistry metricsRegistry) {
		meter = metricsRegistry.meter(name);
	}

	@Override
	public void mark() {
		meter.mark();
	}

	@Override
	public void mark(long n) {
		meter.mark(n);
	}
}
