package com.synapsense.metrics.stub;

import com.synapsense.metrics.Meter;
import com.synapsense.metrics.MetricsFactory;
import com.synapsense.metrics.Timer;
import com.synapsense.metrics.stub.MeterStub;
import com.synapsense.metrics.stub.TimerStub;

/**
 */
public class StubMetricsFactory implements MetricsFactory {
    @Override
    public Timer getTimer(Class clazz, String name) {
        return new TimerStub();
    }

    @Override
    public Meter getMeter(Class clazz, String name) {
        return new MeterStub();
    }
}
