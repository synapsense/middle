package com.synapsense.metrics;

/**
 */
public interface MetricsFactory {

	public Timer getTimer(Class clazz, String name);

	public Meter getMeter(Class clazz, String name);
}
