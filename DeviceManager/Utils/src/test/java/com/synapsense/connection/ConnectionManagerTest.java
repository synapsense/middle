package com.synapsense.connection;

import mockit.Delegate;

import mockit.Mocked;
import mockit.StrictExpectations;
import mockit.integration.junit4.JMockit;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;

@RunWith(JMockit.class)
public class ConnectionManagerTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testReconnect(@Mocked ConnectionHandler<?> connection) throws Exception {
		Level oldLevel = ((Logger) LoggerFactory.getILoggerFactory().getLogger("ROOT")).getLevel();
		((Logger) LoggerFactory.getILoggerFactory().getLogger("ROOT")).setLevel(Level.INFO);
		final String name = "Test Server";
		final long reconnect = 50;
		new StrictExpectations() {
			{
				connection.getName();
				result = name;
				connection.addStatusListener(withInstanceOf(ConnectionStatusListener.class));
				connection.getName();
				result = name;
				connection.isConnected();
				result = false;
				connection.getName();
				result = name;
				connection.connect();
				result = new Exception("Server error");
				connection.getName();
				result = name;
				connection.getReconnectInterval();
				result = reconnect;
				connection.getReconnectInterval();
				result = reconnect;
				connection.getName();
				result = name;
				connection.connect();
				connection.getName();
				result = new Delegate<String>() {
					@SuppressWarnings("unused")
					String connect() {
						synchronized (name) {
							name.notifyAll();
						}
						return name;
					}
				};
			}
		};

		ConnectionManager.getInstance().addConnection(connection);
		synchronized (name) {
			name.wait();
		}
		((Logger) LoggerFactory.getILoggerFactory().getLogger("ROOT")).setLevel(oldLevel);
	}

}
