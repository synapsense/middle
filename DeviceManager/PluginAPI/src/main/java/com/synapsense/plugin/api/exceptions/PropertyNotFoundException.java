package com.synapsense.plugin.api.exceptions;

/**
 * @author stikhon
 * 
 */
public class PropertyNotFoundException extends RuntimeException {

	private static final long serialVersionUID = -1096026625200346414L;

	public PropertyNotFoundException() {
		super();
	}

	public PropertyNotFoundException(final String message) {
		super(message);
	}

}
