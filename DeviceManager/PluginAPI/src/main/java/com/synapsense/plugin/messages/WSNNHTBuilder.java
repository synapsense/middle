package com.synapsense.plugin.messages;

import java.io.Serializable;

import com.synapsense.plugin.messages.GenericMessage.GenericMessageTypes;

/**
 * The class used to build NHT message
 * 
 * @author anechaev
 * 
 */
public class WSNNHTBuilder implements MessageBuilder {
	public enum FieldConstants {
	NODE_ID, NEIGHBORS, REPORT_TIMESTAMP, NID, RSSI, STATE, HOP, HOPTM
	}

	private GenericMessage msg;

	public WSNNHTBuilder(char nodeId, int neighbors, long timestamp, char neighId, String rssi, int state,
	        int hop, int hoptm) {
		msg = new GenericMessage(GenericMessageTypes.WSN_NHT);

		msg.put(FieldConstants.NODE_ID.name(), nodeId);
		msg.put(FieldConstants.NEIGHBORS.name(), neighbors);
		msg.put(FieldConstants.REPORT_TIMESTAMP.name(), timestamp);
		msg.put(FieldConstants.NID.name(), neighId);
		msg.put(FieldConstants.RSSI.name(), rssi);
		msg.put(FieldConstants.STATE.name(), state);
		msg.put(FieldConstants.HOP.name(), hop);
		msg.put(FieldConstants.HOPTM.name(), hoptm);
	}

	@Override
	public GenericMessage getMessage() {
		return msg;
	}

	public void putData(FieldConstants field, Serializable data) {
		msg.put(field.name(), data);
	}
}
