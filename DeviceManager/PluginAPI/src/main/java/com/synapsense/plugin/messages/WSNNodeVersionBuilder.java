package com.synapsense.plugin.messages;

import com.synapsense.plugin.messages.GenericMessage.GenericMessageTypes;

public class WSNNodeVersionBuilder implements MessageBuilder {
	public enum FieldConstants {
	NODE_ID, APP_VERSION, APP_REVISION, NWK_VERSION, NWK_REVISION, OS_VERSION, OS_REVISION
	}

	private final GenericMessage msg;

	public WSNNodeVersionBuilder(long nodeID, int appVersion, int appRevision, int nwkVersion, int nwkRevision,
	        int osVersion, int osRevision) {
		msg = new GenericMessage(GenericMessageTypes.WSN_NODEVERSION);
		msg.put(FieldConstants.NODE_ID.name(), nodeID);
		msg.put(FieldConstants.APP_VERSION.name(), appVersion);
		msg.put(FieldConstants.APP_REVISION.name(), appRevision);
		msg.put(FieldConstants.NWK_VERSION.name(), nwkVersion);
		msg.put(FieldConstants.NWK_REVISION.name(), nwkRevision);
		msg.put(FieldConstants.OS_VERSION.name(), osVersion);
		msg.put(FieldConstants.OS_REVISION.name(), osRevision);
	}

	@Override
	public GenericMessage getMessage() {
		return msg;
	}
}
