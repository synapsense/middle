package com.synapsense.plugin.messages;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * 
 * The data storage used in data exchange between Device Manager Core and
 * Transports. Current implementation uses key-value format but it may be
 * changed in future.
 * 
 * @author anechaev
 * 
 */
public final class GenericMessage implements Map<String, Serializable>, Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1050971628365892414L;

	public static enum GenericMessageTypes {
	ALERT, WSN_SENSOR_DATA, WSN_ROUTING, WSN_STATISTICS, WSN_NHT, WSN_GWSTATUS, WSN_NODEVERSION, REFINED_ALERT, POWER_RACK_CONF, DUO_CONF, JAWA_DATA, PHASE_DATA, DUO_DATA, ACTUATOR_COMMAND_SENT, ACTUATOR_COMMAND_FAILED, ACTUATOR_POSITION, UNKNOWN
	}

	private Map<String, Serializable> container = new HashMap<String, Serializable>();

	private GenericMessageTypes type;

	public GenericMessage(GenericMessageTypes type) {
		this.type = type;
	}

	public GenericMessageTypes getType() {
		return type;
	}

	@Override
	public void clear() {
		container.clear();
	}

	@Override
	public boolean containsKey(Object arg0) {
		return container.containsKey(arg0);
	}

	@Override
	public boolean containsValue(Object arg0) {
		return container.containsValue(arg0);
	}

	@Override
	public Set<java.util.Map.Entry<String, Serializable>> entrySet() {
		return container.entrySet();
	}

	@Override
	public Serializable get(Object arg0) {
		return container.get(arg0);
	}

	@Override
	public boolean isEmpty() {
		return container.isEmpty();
	}

	@Override
	public Set<String> keySet() {
		return container.keySet();
	}

	@Override
	public Serializable put(String arg0, Serializable arg1) {
		return container.put(arg0, arg1);
	}

	@Override
	public void putAll(Map<? extends String, ? extends Serializable> arg0) {
		container.putAll(arg0);
	}

	@Override
	public Serializable remove(Object arg0) {
		return container.remove(arg0);
	}

	@Override
	public int size() {
		return container.size();
	}

	@Override
	public Collection<Serializable> values() {
		return container.values();
	}

	@Override
	public String toString() {
		return container.toString();
	}

}
