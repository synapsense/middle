package com.synapsense.plugin.api.exceptions;

public class UnableGetDataException extends RuntimeException {
	private static final long serialVersionUID = -2985963374769959549L;

	public UnableGetDataException(String message) {
		super(message);
	}

	public UnableGetDataException(String msg, Throwable e) {
		super(msg, e);
	}
}
