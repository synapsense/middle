package com.synapsense.plugin.messages;

import com.synapsense.plugin.messages.GenericMessage.GenericMessageTypes;

/**
 * The <code>MessageBuilder</code> implementation that builds GenericMessage
 * based on WSN sensor data flow.
 * 
 * @author anechaev
 * 
 */
public class WSNSensorDataBuilder implements MessageBuilder {
	public enum FieldConstants {
	NODE_ID, SENSOR_ID, DATA, TIMESTAMP, NETWORK_ID, NODE_PLATFORM, TYPE_ID
	};

	private GenericMessage msg;

	public WSNSensorDataBuilder(long nodeID, int sensorID, double data, long timestamp, int networkId,
	        int nodePlatform, int typeId) {
		msg = new GenericMessage(GenericMessageTypes.WSN_SENSOR_DATA);

		msg.put(FieldConstants.NODE_ID.name(), nodeID);
		msg.put(FieldConstants.SENSOR_ID.name(), sensorID);
		msg.put(FieldConstants.DATA.name(), data);
		msg.put(FieldConstants.TIMESTAMP.name(), timestamp);
		msg.put(FieldConstants.NETWORK_ID.name(), networkId);
		msg.put(FieldConstants.NODE_PLATFORM.name(), nodePlatform);
		msg.put(FieldConstants.TYPE_ID.name(), typeId);
	}

	@Override
	public GenericMessage getMessage() {
		return msg;
	}

}
