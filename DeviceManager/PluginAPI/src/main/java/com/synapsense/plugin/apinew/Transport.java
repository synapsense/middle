package com.synapsense.plugin.apinew;

import java.io.IOException;
import java.io.Serializable;

/**
 * Transport is used to work with an external consumer
 * 
 * @author anechaev
 * 
 */
public interface Transport {
	/**
	 * Specifies the order the transport wants to be invoked. The lesser the
	 * priority is, the sooner the transport will be invoked. The least priority
	 * (0) should not be used by any custom transport implementation.
	 * 
	 * @return priority value
	 */
	int getPriority();

	/**
	 * Sends sensed data to the external consumer
	 * 
	 * @param objectID
	 * @param propertyName
	 * @param value
	 */
	void sendData(String objectID, String propertyName, Serializable value);

	/**
	 * Sends data that is necessary to process synchroniously in the order it is
	 * sent
	 * 
	 * @param objectID
	 * @param propertyName
	 * @param value
	 */
	void sendOrderedData(String objectID, String propertyName, Serializable value);

	/**
	 * connects to the data consumer
	 * 
	 * @throws IOException
	 */
	void connect() throws IOException;
}
