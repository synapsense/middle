package com.synapsense.plugin.apinew.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.synapsense.plugin.api.exceptions.PropertyNotFoundException;
import com.synapsense.plugin.apinew.ObjectDesc;
import com.synapsense.plugin.apinew.PropertyDesc;

/**
 * The ObjectDescImpl is a basic implementation of the ObjectDesc interface,
 * which should be sufficient for most cases.
 * 
 * @author stikhon
 */
public class ObjectDescImpl implements ObjectDesc {
	public static final String DEFAULT_DELIMITER = ":";

	private Map<String, PropertyDesc> properties = new HashMap<String, PropertyDesc>();

	private Map<String, List<ObjectDesc>> childrenByType = new HashMap<String, List<ObjectDesc>>();

	private List<ObjectDesc> children = new LinkedList<ObjectDesc>();

	private ObjectDesc parent;

	private String id;

	private String type;

	public ObjectDescImpl() {
	}

	public ObjectDescImpl(String id) {
		this.id = id;
		this.type = id.substring(0, id.indexOf(DEFAULT_DELIMITER));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsense.plugin.apinew.ObjectDesc#getChildren()
	 */
	@Override
	public List<ObjectDesc> getChildren() {
		return children;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsense.plugin.apinew.ObjectDesc#getChildrenByType()
	 */
	@Override
	public List<ObjectDesc> getChildrenByType(String type) {
		List<ObjectDesc> children = childrenByType.get(type);
		if (children == null) {
			children = new ArrayList<ObjectDesc>();
		}
		return children;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsense.plugin.apinew.ObjectDesc#getID()
	 */
	@Override
	public String getID() {
		return id;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsense.plugin.apinew.ObjectDesc#getID()
	 */
	@Override
	public String getType() {
		return type;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsense.plugin.apinew.ObjectDesc#getParent()
	 */
	@Override
	public ObjectDesc getParent() {
		return parent;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsense.plugin.apinew.ObjectDesc#getProperties()
	 */
	@Override
	public Collection<PropertyDesc> getProperties() {
		return properties.values();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.synapsense.plugin.apinew.ObjectDesc#getProperty(java.lang.String)
	 */
	@Override
	public PropertyDesc getProperty(String propertyName) {
		PropertyDesc prop = properties.get(propertyName);
		if (prop == null) {
			throw new PropertyNotFoundException("Property [" + propertyName + "] cannot be found for object ["
			        + this.getID() + "]");
		}
		return prop;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.synapsense.plugin.apinew.ObjectDesc#getPropertyValue(java.lang.String
	 * )
	 */
	@Override
	public String getPropertyValue(String propertyName) {
		String value = null;
		PropertyDesc prop = properties.get(propertyName);
		if (prop == null) {
			throw new PropertyNotFoundException("Property [" + propertyName + "] cannot be found for object ["
			        + this.getID() + "]");
		}
		value = prop.getValue();
		return value;
	}

	@Override
	public boolean containsProperty(String propertyName) {
		return properties.containsKey(propertyName);

	}

	/**
	 * Sets the object's parent.
	 * 
	 * @param parent
	 *            The objects parent.
	 */
	public void setParent(ObjectDesc parent) {
		this.parent = parent;
	}

	/**
	 * Sets the object's id.
	 * 
	 * @param id
	 *            The object's id.
	 */
	public void setId(String id) {
		this.id = id;
		this.type = id.substring(0, id.indexOf(DEFAULT_DELIMITER));
	}

	/**
	 * Adds the child.
	 * 
	 * @param obj
	 *            The object to add to the children list.
	 */
	public void addChild(ObjectDesc obj) {
		children.add(obj);
		String type = obj.getType();
		List<ObjectDesc> childrenWithSameType;
		if (childrenByType.containsKey(type)) {
			childrenWithSameType = childrenByType.get(type);
		} else {
			childrenWithSameType = new ArrayList<ObjectDesc>();
			childrenByType.put(type, childrenWithSameType);
		}
		childrenWithSameType.add(obj);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.synapsense.plugin.apinew.ObjectDesc#addProperty(com.synapsense.plugin
	 * .apinew.PropertyDesc)
	 */
	@Override
	public void addProperty(PropertyDesc pro) {
		properties.put(pro.getName(), pro);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ObjectDescImpl other = (ObjectDescImpl) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return id;
	}

}
