package com.synapsense.plugin.messages;

public enum DuoDataType {
AVG_CURRENT_PHASE_A(0x1), AVG_CURRENT_PHASE_B(0x2), AVG_CURRENT_PHASE_C(0x3), VOLTAGE_A_N(0x4), VOLTAGE_B_N(0x5), VOLTAGE_C_N(
        0x6), VOLTAGE_A_B(0x7), VOLTAGE_B_C(0x8), VOLTAGE_C_A(0x9), DEMAND_POWER_A(0xa), DEMAND_POWER_B(0xb), DEMAND_POWER_C(
        0xc), MAX_CURRENT_A(0xd), MAX_CURRENT_B(0xe), MAX_CURRENT_C(0xf), AVG_CURRENT_PHASE_AB(0x10), AVG_CURRENT_PHASE_BC(
        0x11), AVG_CURRENT_PHASE_CA(0x12), MAX_CURRENT_PHASE_AB(0x13), MAX_CURRENT_PHASE_BC(0x14), MAX_CURRENT_PHASE_CA(
        0x15), DEMAND_POWER_TOTAL(0x16);

private final int type;

DuoDataType(int type) {
	this.type = type;
}

public int getType() {
	return type;
}

public static DuoDataType fromType(int type) {
	for (DuoDataType a : DuoDataType.values()) {
		if (a.getType() == type)
			return a;
	}
	return null;
}
}
