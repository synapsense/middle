package com.synapsense.plugin.apinew;

/**
 * Tag descriptor. Each tag has unique name among other tags.
 * 
 * @author stikhon
 * 
 */
public interface TagDesc {

	/**
	 * Gets the tag's name.
	 * 
	 * @return Tag's name.
	 */
	String getName();

	/**
	 * Gets the tag's value.
	 * 
	 * @return Tag's value.
	 */
	String getValue();
}
