package com.synapsense.plugin.apinew;

import java.util.Collection;
import java.util.List;

import com.synapsense.plugin.api.exceptions.PropertyNotFoundException;

/**
 * Object descriptor. Stores the information about properties and child objects
 * it contains. Every object has a globally-unique string identifier.
 * 
 * @author anechaev
 * 
 */
public interface ObjectDesc {
	/**
	 * gets an identifier
	 * 
	 * @return
	 */
	String getID();

	/**
	 * Gets an object type.
	 * 
	 * @return
	 */
	String getType();

	/**
	 * Gets a parent object, or null if this object is a root one.
	 * 
	 * @return
	 */
	ObjectDesc getParent();

	/**
	 * Gets children object descriptors. Returns empty collection if there are
	 * no children.
	 */
	List<ObjectDesc> getChildren();

	/**
	 * Gets children object descriptors by the given type.
	 */
	List<ObjectDesc> getChildrenByType(String type);

	/**
	 * Gets the list of properties of the object. Returns empty collection if
	 * there are no properties
	 */
	Collection<PropertyDesc> getProperties();

	/**
	 * @param propertyName
	 * @return
	 * @throws PropertyNotFoundException
	 *             if there is no property with given name
	 */
	PropertyDesc getProperty(String propertyName);

	/**
	 * Gets value for the given property name.
	 * 
	 * @param propertyName
	 * @return
	 * @throws PropertyNotFoundException
	 *             if there is no property with given name
	 */
	String getPropertyValue(String propertyName);

	/**
	 * Adds the property.
	 * 
	 * @param pro
	 *            The property to add to the object.
	 */
	void addProperty(PropertyDesc pro);

	/**
	 * Checks if Object has a property with the provided name.
	 * 
	 * @param propertyName
	 *            The name of the property.
	 * @return
	 */
	boolean containsProperty(String propertyName);

	/**
	 * Adds the child.
	 * 
	 * @param obj
	 *            The object to add to the children list.
	 */
	void addChild(ObjectDesc obj);

	// TODO think about event subscriptions
}
