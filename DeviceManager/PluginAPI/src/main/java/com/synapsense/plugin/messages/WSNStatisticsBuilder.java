package com.synapsense.plugin.messages;

import java.io.Serializable;

import com.synapsense.plugin.messages.GenericMessage.GenericMessageTypes;

/**
 * The class used to create Statistics message
 * 
 * @author anechaev
 * 
 */
public class WSNStatisticsBuilder implements MessageBuilder {
	public enum FieldConstants {
	NODE_ID, NETWORK_ID, LOGICAL_ID, NWK_TIMESTAMP, SEND_FAILS_FULL_QUEUE, SEND_FAILS_NO_PATH, NUMBER_ONE_HOP_RETRANSMISSION, FORWARDING_PACKETS_DROPPED, PARENT_FAIL_COUNT, BROADCAST_RETRANSMISSION, PACKETS_FROM_BASE_STATION, PACKETS_TO_BASE_STATION, PACKETS_FORWARDED, PACKETS_SENT, RADIO_ONTIME, FREE_SLOT_COUNT, WRONG_SLOT_COUNT, TOTAL_SLOT_COUNT, PACKETS_RCVD_COUNT, CRC_FAIL_PACKET_COUNT, TIME_SYNCS_LOST_DRIFT_DETECTION, TIME_SYNCS_LOST_TIMEOUT, NODE_LATENCY, PACKETS_FROM_NODE, DUPLICATE_PACKETS, OUT_OF_ORDER_PACKETS, CHANNEL_STATUS, HOP_COUNT, ROUTER_STATE, PARENT_STATE, NOISE_THRESH
	}

	private GenericMessage msg;

	// here are too many fields to be set, thus we leave default ctor available.
	// All data should be set via put() method
	public WSNStatisticsBuilder() {
		msg = new GenericMessage(GenericMessageTypes.WSN_STATISTICS);
	}

	@Override
	public GenericMessage getMessage() {
		return msg;
	}

	public void putData(FieldConstants field, Serializable data) {
		msg.put(field.name(), data);
	}
}
