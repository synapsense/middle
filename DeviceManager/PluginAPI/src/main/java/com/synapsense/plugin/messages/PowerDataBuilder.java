package com.synapsense.plugin.messages;

import com.synapsense.plugin.messages.GenericMessage.GenericMessageTypes;

/**
 * 
 * @author Dmitry Grudzinskiy
 * 
 */
public class PowerDataBuilder implements MessageBuilder {

	protected GenericMessage msg;

	public enum FieldConstants {
	NODE_ID, JAWA_ID, RACK, RPDU, PHASE, AVG_CURRENT, MAX_CURRENT, DEMANDPOWER, VOLTAGE, TIMESTAMP
	};

	/**
	 * This constructor should be used to send jawa data
	 * 
	 * @param nodeId
	 * @param jawaId
	 * @param avgCurrent
	 * @param maxCurrent
	 * @param demandPower
	 * @param timestamp
	 */
	public PowerDataBuilder(long nodeId, long jawaId, double avgCurrent, double maxCurrent, double demandPower,
	        long timestamp) {
		msg = new GenericMessage(GenericMessageTypes.JAWA_DATA);
		msg.put(FieldConstants.NODE_ID.name(), nodeId);
		msg.put(FieldConstants.JAWA_ID.name(), jawaId);
		msg.put(FieldConstants.AVG_CURRENT.name(), avgCurrent);
		msg.put(FieldConstants.MAX_CURRENT.name(), maxCurrent);
		msg.put(FieldConstants.DEMANDPOWER.name(), demandPower);
		msg.put(FieldConstants.TIMESTAMP.name(), timestamp);
	}

	/**
	 * This constructor should be used to send phase data
	 * 
	 * @param nodeId
	 * @param rack
	 * @param feed
	 * @param phase
	 * @param voltage
	 * @param timestamp
	 */
	public PowerDataBuilder(long nodeId, int rack, int feed, int phase, double voltage, long timestamp) {
		msg = new GenericMessage(GenericMessageTypes.PHASE_DATA);
		msg.put(FieldConstants.NODE_ID.name(), nodeId);
		msg.put(FieldConstants.RACK.name(), rack);
		msg.put(FieldConstants.RPDU.name(), feed);
		msg.put(FieldConstants.PHASE.name(), phase);
		msg.put(FieldConstants.VOLTAGE.name(), voltage);
		msg.put(FieldConstants.TIMESTAMP.name(), timestamp);
	}

	@Override
	public GenericMessage getMessage() {
		return msg;
	}
}
