/**
 * 
 */
package com.synapsense.plugin.messages;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import com.synapsense.plugin.messages.GenericMessage.GenericMessageTypes;

/**
 * @author stikhon
 * 
 */
public class PowerRackConfBuilder implements MessageBuilder {

	public static final int LEFT_SERVER_ID = 0;
	public static final int CENTER_SERVER_ID = 1;
	public static final int RIGHT_SERVER_ID = 2;

	public enum FieldConstants {
	NODE_ID, CHECKSUM, PROGRAM, PLUGS, SERVERS, RACKS
	};

	public enum PowerRackFields {
	PLUG_ID, PHASE_ID, RPDU_ID, RPDU_TYPE, UNIT_POSITION, UNIT_HEIGHT, RACK, DEMAND_POWER,
	};

	public enum PhasesLabels {
	A(1), B(2), C(3);

	int id;

	private PhasesLabels(int id) {
		this.id = id;
	}

	static public String getPhaseLabelById(int id) {
		for (PhasesLabels label : PhasesLabels.values()) {
			if (label.getId() == id) {
				return label.name();
			}
		}
		return null;
	}

	static public int getPhaseIdByLabel(String label) {
		for (PhasesLabels lable : PhasesLabels.values()) {
			if (lable.name().equals(label)) {
				return lable.getId();
			}
		}
		return -1;
	}

	static public int convertId(int id) {
		int ratio = id % values().length;
		return ratio == 0 ? values().length : ratio;
	}

	public int getId() {
		return id;
	}
	};

	private GenericMessage msg = new GenericMessage(GenericMessageTypes.POWER_RACK_CONF);
	private ArrayList<HashMap<PowerRackFields, Serializable>> plugs = new ArrayList<HashMap<PowerRackFields, Serializable>>();
	private ArrayList<HashMap<PowerRackFields, Serializable>> servers = new ArrayList<HashMap<PowerRackFields, Serializable>>();
	private ArrayList<HashMap<PowerRackFields, Serializable>> racks = new ArrayList<HashMap<PowerRackFields, Serializable>>();

	public PowerRackConfBuilder(Long nodeId) {
		msg.put(FieldConstants.NODE_ID.name(), nodeId);
		msg.put(FieldConstants.PLUGS.name(), plugs);
		msg.put(FieldConstants.SERVERS.name(), servers);
		msg.put(FieldConstants.RACKS.name(), racks);
	}

	@Override
	public GenericMessage getMessage() {
		return msg;
	}

	/**
	 * Add a left/center/right rack with its height in units
	 * 
	 * @param rack
	 *            [0,1,2]
	 * @param unitHeight
	 *            rack unit height
	 * @param rpduType
	 *            either 0 (Wye) or 1 (Delta)
	 */
	public void addRack(int rack, int unitHeight, int rpduType) {
		HashMap<PowerRackFields, Serializable> rackData = new HashMap<PowerRackFields, Serializable>();
		rackData.put(PowerRackFields.RACK, rack);
		rackData.put(PowerRackFields.UNIT_HEIGHT, unitHeight);
		rackData.put(PowerRackFields.RPDU_TYPE, rpduType);
		racks.add(rackData);
	}

	/**
	 * Adds a plug to the message's plugs list.
	 * 
	 * @param plugId
	 * @param phaseId
	 * @param rpduId
	 * @param unitPosition
	 * @param unitHeight
	 * @param rack
	 */
	public void addPlug(Long plugId, Integer phaseId, Integer rpduId, Integer unitPosition, Integer unitHeight,
	        Integer rack) {
		HashMap<PowerRackFields, Serializable> plugData = new HashMap<PowerRackFields, Serializable>();
		plugData.put(PowerRackFields.PLUG_ID, plugId);
		plugData.put(PowerRackFields.PHASE_ID, phaseId);
		plugData.put(PowerRackFields.RPDU_ID, rpduId);
		plugData.put(PowerRackFields.UNIT_POSITION, unitPosition);
		plugData.put(PowerRackFields.UNIT_HEIGHT, unitHeight);
		plugData.put(PowerRackFields.RACK, rack);

		plugs.add(plugData);
	}

	/**
	 * Adds a server to the message's plugs list.
	 * 
	 * @param unitPosition
	 * @param unitHeight
	 * @param demandPower
	 */
	public void addServer(Integer unitPosition, Integer unitHeight, Integer rack, Double demandPower) {
		HashMap<PowerRackFields, Serializable> serverData = new HashMap<PowerRackFields, Serializable>();
		serverData.put(PowerRackFields.UNIT_POSITION, unitPosition);
		serverData.put(PowerRackFields.UNIT_HEIGHT, unitHeight);
		serverData.put(PowerRackFields.RACK, rack);
		serverData.put(PowerRackFields.DEMAND_POWER, demandPower);

		servers.add(serverData);
	}

	/**
	 * Sets node checksum
	 * 
	 * @param checksum
	 */
	public void setChecksum(long checksum) {
		msg.put(FieldConstants.CHECKSUM.name(), checksum);
	}

	/**
	 * Sets serialized sandcroller configuration
	 * 
	 * @param program
	 */
	public void setProgram(String program) {
		msg.put(FieldConstants.PROGRAM.name(), program);
	}
}
