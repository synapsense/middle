package com.synapsense.plugin.messages;

import com.synapsense.plugin.messages.GenericMessage.GenericMessageTypes;

public class WSNActuatorPositionBuilder implements MessageBuilder {
	public enum FieldConstants {
	DATA, MOVING, STATUS, TIMESTAMP
	};

	private GenericMessage msg;

	public WSNActuatorPositionBuilder(double data, int moving, int status, long timestamp) {
		msg = new GenericMessage(GenericMessageTypes.ACTUATOR_POSITION);
		msg.put(FieldConstants.DATA.name(), data);
		msg.put(FieldConstants.MOVING.name(), moving);
		msg.put(FieldConstants.STATUS.name(), status);
		msg.put(FieldConstants.TIMESTAMP.name(), timestamp);
	}

	@Override
	public GenericMessage getMessage() {
		return msg;
	}

}
