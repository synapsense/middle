package com.synapsense.plugin;

import java.io.Serializable;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;
import java.util.Properties;

public interface DeviceManagerRMI extends Remote {

	/**
	 * Sets the value for the property of the particular devices
	 * 
	 * @param objectID
	 * @param propertyName
	 * @param value
	 */
	// TODO operation status!
	void setPropertyValue(String objectID, String propertyName, Serializable value) throws RemoteException;

	/**
	 * Gets the property value of the particular device
	 * 
	 * @param objectID
	 * @param propertyName
	 * @return
	 */
	Serializable getPropertyValue(String objectID, String propertyName) throws RemoteException;

	/**
	 * Configures the plugins from xml file
	 * 
	 * @param pluginID
	 * @param configuration
	 */
	void configurePlugins(String configuration) throws RemoteException;

	/**
	 * @return the list of DM plug-ins
	 */
	List<Properties> enumeratePlugins() throws RemoteException;
}
