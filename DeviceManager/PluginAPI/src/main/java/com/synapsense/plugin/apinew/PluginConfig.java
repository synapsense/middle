package com.synapsense.plugin.apinew;

import java.util.List;
import java.util.Map;

public interface PluginConfig {
	/**
	 * Timestamp property
	 */
	public static final String TIMESTAMP_PROP = "timestamp";

	public String getPluginName();

	public ObjectDesc getObject(final String id);

	public Map<String, ObjectDesc> getObjects();

	/**
	 * 
	 * @return
	 */
	public List<ObjectDesc> getRoots();

	/**
	 * Gets a single plugin's configuration property.
	 * 
	 * @return PropertyDesc object by with the given name or null if there is no
	 *         property for the name.
	 */
	public PropertyDesc getPluginProperty(final String propertyName);

	/**
	 * Gets a map with plugin's configuration properties or an empty map
	 * otherwise.
	 */
	public Map<String, PropertyDesc> getPluginProperties();

}