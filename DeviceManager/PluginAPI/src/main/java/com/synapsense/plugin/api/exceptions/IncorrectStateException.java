/**
 * 
 */
package com.synapsense.plugin.api.exceptions;

/**
 * @author stikhon
 * 
 */
public class IncorrectStateException extends DMException {

	private static final long serialVersionUID = 728758852454955493L;

	public IncorrectStateException(String message) {
		super(message);
	}
}
