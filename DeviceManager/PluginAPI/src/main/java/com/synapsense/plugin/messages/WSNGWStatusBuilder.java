package com.synapsense.plugin.messages;

import com.synapsense.plugin.messages.GenericMessage.GenericMessageTypes;

/**
 * This class is used to transfer WSN Gateway Status information to Data
 * Consumers Refer to <code>Gateway</code> class description, where status bits
 * info is explained.
 * 
 * @author anechaev
 * 
 */
public class WSNGWStatusBuilder implements MessageBuilder {
	public enum FieldConstants {
	PHYS_ID, STATUS
	}

	private GenericMessage msg;

	public WSNGWStatusBuilder(long gwPhysID, int status) {
		msg = new GenericMessage(GenericMessageTypes.WSN_GWSTATUS);
		msg.put(FieldConstants.PHYS_ID.name(), gwPhysID);
		msg.put(FieldConstants.STATUS.name(), status);
	}

	@Override
	public GenericMessage getMessage() {
		return msg;
	}

}
