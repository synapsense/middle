/**
 * 
 */
package com.synapsense.plugin.api.exceptions;

/**
 * @author stikhon
 * 
 */
public abstract class DMException extends Exception {

	private static final long serialVersionUID = -8486130898006898137L;

	public DMException(String message) {
		super(message);
	}

	public DMException(String msg, Throwable e) {
		super(msg, e);
	}

}
