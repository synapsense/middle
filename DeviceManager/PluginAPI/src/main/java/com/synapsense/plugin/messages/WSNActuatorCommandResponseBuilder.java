package com.synapsense.plugin.messages;

import com.synapsense.plugin.messages.GenericMessage.GenericMessageTypes;

public class WSNActuatorCommandResponseBuilder implements MessageBuilder {
	public enum FieldConstants {
	PROPERTY_NAME, DATA
	};

	private GenericMessage msg;

	public WSNActuatorCommandResponseBuilder(String propertyname, double data, boolean success) {
		msg = null;
		if (success)
			msg = new GenericMessage(GenericMessageTypes.ACTUATOR_COMMAND_SENT);
		else
			msg = new GenericMessage(GenericMessageTypes.ACTUATOR_COMMAND_FAILED);
		msg.put(FieldConstants.PROPERTY_NAME.name(), propertyname);
		msg.put(FieldConstants.DATA.name(), data);
	}

	@Override
	public GenericMessage getMessage() {
		return msg;
	}

}
