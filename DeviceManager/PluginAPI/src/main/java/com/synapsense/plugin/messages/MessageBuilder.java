package com.synapsense.plugin.messages;

/**
 * Interface used to build message. Most every implementations are examples of
 * builder DP.
 * 
 * @author anechaev
 * 
 */
public interface MessageBuilder {
	/**
	 * @return generated message
	 */
	GenericMessage getMessage();
}
