package com.synapsense.plugin.api.exceptions;

/**
 * 
 * This exception is thrown when a plugin cannot be configured due to some
 * reasons: 1. Wrong configuration format 2. Wrong configuration parameters 3.
 * Invalid mapping 4. etc
 * 
 * @author anechaev
 * 
 */
public class ConfigPluginException extends DMException {
	private static final long serialVersionUID = -2985963374769959549L;

	public ConfigPluginException(String message) {
		super(message);
	}

	public ConfigPluginException(String msg, Throwable e) {
		super(msg, e);
	}
}
