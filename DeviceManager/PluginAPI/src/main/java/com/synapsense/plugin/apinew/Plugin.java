package com.synapsense.plugin.apinew;

import java.io.Serializable;
import java.util.List;
import java.util.Properties;

import com.synapsense.plugin.api.exceptions.ConfigPluginException;
import com.synapsense.plugin.api.exceptions.IncorrectStateException;

/**
 * Data sensing plug-in interface
 * 
 * @author anechaev
 * 
 */
public interface Plugin {

	public static final String PLUGIN_ID = "PLUGIN_ID";
	public static final String ROOT_TYPES = "ROOT_TYPES";
	public static final String LAST_UPDATE_TIME = "LAST_UPDATE_TIME";

	/**
	 * Returns an unique plug-in identifier
	 * 
	 * @return
	 */
	String getID();

	/**
	 * Returns a PluginDesc object with plugin description.
	 * 
	 * @return
	 */
	Properties getPluginDescription();

	/**
	 * Lets the plug-in start working. The plug-in should be configured before
	 * it is started
	 * 
	 * @throws IncorrectStateException
	 */
	void start() throws IncorrectStateException;

	/**
	 * Stops the plugin.
	 */
	void stop();

	/**
	 * Sets the property value
	 * 
	 * @param object
	 *            objects which the property belongs to
	 * @param propertyName
	 *            name of the target property
	 * @param value
	 */
	void setPropertyValue(ObjectDesc object, String propertyName, Serializable value);

	/**
	 * Sets the property values for all the given property names of the object
	 * 
	 * @param object
	 *            objects which the property belongs to
	 * @param propertyNames
	 *            names of the target properties
	 * @param values
	 *            List of values to set.
	 */
	void setPropertyValue(ObjectDesc object, List<String> propertyName, List<Serializable> values);

	/**
	 * Retrieves the property value
	 * 
	 * @param objectID
	 * @param propertyName
	 * @return
	 */
	Serializable getPropertyValue(ObjectDesc object, String propertyName);

	/**
	 * Retrieves values for all the given property names of the object
	 * 
	 * @param objectID
	 * @param propertyNames
	 * @return
	 */
	List<Serializable> getPropertyValue(ObjectDesc object, List<String> propertyNames);

	/**
	 * Configures the plug-in
	 * 
	 * @param config
	 * @throws ConfigPluginException
	 */
	void configure(PluginConfig config) throws ConfigPluginException;

	/**
	 * Configures the transport being used by the plug-in
	 * 
	 * @param transport
	 */
	void setTransport(Transport transport);
}
