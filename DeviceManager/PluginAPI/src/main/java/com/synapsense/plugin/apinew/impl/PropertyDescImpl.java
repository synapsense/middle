package com.synapsense.plugin.apinew.impl;

import java.security.InvalidParameterException;
import java.util.HashMap;
import java.util.Map;

import com.synapsense.plugin.apinew.ObjectDesc;
import com.synapsense.plugin.apinew.PropertyDesc;
import com.synapsense.plugin.apinew.TagDesc;

/**
 * The PropertyDescImpl class is a basic implementation of the PropertyDesc
 * interface, which should be sufficient for most cases.
 */
public class PropertyDescImpl implements PropertyDesc {

	private String name;

	private String value;

	private Map<String, TagDesc> tags = new HashMap<String, TagDesc>();

	private ObjectDesc parent;

	public PropertyDescImpl() {
	}

	public PropertyDescImpl(String name, String value) {
		this.name = name;
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsense.plugin.apinew.PropertyDesc#getName()
	 */
	@Override
	public String getName() {
		return name;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsense.plugin.apinew.PropertyDesc#getValue()
	 */
	@Override
	public String getValue() {
		return value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsense.plugin.apinew.PropertyDesc#getTags()
	 */
	@Override
	public Map<String, TagDesc> getTags() {
		return tags;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsense.plugin.apinew.PropertyDesc#getTag(java.lang.String)
	 */
	@Override
	public TagDesc getTag(String tagName) {
		return tags.get(tagName);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsense.plugin.apinew.PropertyDesc#getParent()
	 */
	@Override
	public ObjectDesc getParent() {
		return parent;
	}

	/**
	 * Sets the value.
	 * 
	 * @param value
	 *            The new value.
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * Adds the tag.
	 * 
	 * @param tag
	 *            the tag
	 */
	public void addTag(TagDesc tag) {
		if (tag == null || tag.getName() == null || tag.getName() == "") {
			throw new InvalidParameterException("Tag is null or tag's name is not set.");
		}
		tags.put(tag.getName(), tag);
	}

	/**
	 * Sets the name.
	 * 
	 * @param name
	 *            the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Sets the parent.
	 * 
	 * @param parent
	 *            the new parent
	 */
	public void setParent(ObjectDesc parent) {
		this.parent = parent;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return name + ":" + value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PropertyDescImpl other = (PropertyDescImpl) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (parent == null) {
			if (other.parent != null)
				return false;
		} else if (!parent.equals(other.parent))
			return false;
		if (tags == null) {
			if (other.tags != null)
				return false;
		} else if (!tags.equals(other.tags))
			return false;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}

}
