package com.synapsense.plugin.api.exceptions;

/**
 * Wrong type is thrown when a property cannot be cast to String
 * 
 * @author anechaev
 * 
 */
public class WrongTypeException extends DMException {

	private static final long serialVersionUID = -6872039265785519392L;

	public WrongTypeException(String arg0) {
		super(arg0);

	}

}
