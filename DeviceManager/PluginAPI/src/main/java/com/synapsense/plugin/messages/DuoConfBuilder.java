package com.synapsense.plugin.messages;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import com.synapsense.plugin.messages.GenericMessage.GenericMessageTypes;

public class DuoConfBuilder implements MessageBuilder {

	private GenericMessage msg = new GenericMessage(GenericMessageTypes.DUO_CONF);

	public enum FieldConstants {
	NODE_ID, CHECKSUM, PROGRAM, DUORPDUS
	};

	public enum DuoRpduFields {
	ID, ISDELTA, HASPERPHASEVN, HASPERPHASEPP, ISSINGLE
	};

	private ArrayList<HashMap<DuoRpduFields, Serializable>> duoRpdus = new ArrayList<HashMap<DuoConfBuilder.DuoRpduFields, Serializable>>();

	public DuoConfBuilder(long nodeId) {
		msg.put(FieldConstants.NODE_ID.name(), nodeId);
		msg.put(FieldConstants.DUORPDUS.name(), duoRpdus);
	}

//	public void addDuoRpdu(int id, boolean isDelta, boolean hasPerPhaseVN, boolean hasPerPhasePP,boolean isSingle) {
	public void addDuoRpdu(int id, boolean isDelta, boolean hasPerPhaseVN, boolean hasPerPhasePP) {
		HashMap<DuoRpduFields, Serializable> rpdu = new HashMap<DuoConfBuilder.DuoRpduFields, Serializable>();
		rpdu.put(DuoRpduFields.ID, id);
		rpdu.put(DuoRpduFields.ISDELTA, isDelta);
		rpdu.put(DuoRpduFields.HASPERPHASEVN, hasPerPhaseVN);
		rpdu.put(DuoRpduFields.HASPERPHASEPP, hasPerPhasePP);
//		rpdu.put(DuoRpduFields.ISSINGLE, isSingle);
		duoRpdus.add(rpdu);
	}

	/**
	 * Sets node checksum
	 * 
	 * @param checksum
	 */
	public void setChecksum(long checksum) {
		msg.put(FieldConstants.CHECKSUM.name(), checksum);
	}

	/**
	 * Sets serialized duo configuration
	 * 
	 * @param program
	 */
	public void setProgram(String program) {
		msg.put(FieldConstants.PROGRAM.name(), program);
	}

	@Override
	public GenericMessage getMessage() {
		return msg;
	}
}
