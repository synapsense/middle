package com.synapsense.plugin.messages;

import java.util.HashMap;
import java.util.Map;

import com.synapsense.plugin.messages.GenericMessage.GenericMessageTypes;

public class DuoDataBuilder implements MessageBuilder {

	private GenericMessage msg = new GenericMessage(GenericMessageTypes.DUO_DATA);

	public enum FieldConstants {
	NODE_ID, RPDUS, TIMESTAMP
	};

	private HashMap<Integer, Map<DuoDataType, Double>> rpduData = new HashMap<Integer, Map<DuoDataType, Double>>();

	public DuoDataBuilder(long nodeId) {
		msg.put(FieldConstants.NODE_ID.name(), nodeId);
		msg.put(FieldConstants.RPDUS.name(), rpduData);
	}

	public void addRpduData(int rpduId, Map<DuoDataType, Double> data) {
		if (data != null) {
			rpduData.put(rpduId, data);
		}
	}

	public void setTimestamp(long timestamp) {
		msg.put(FieldConstants.TIMESTAMP.name(), timestamp);
	}

	@Override
	public GenericMessage getMessage() {
		return msg;
	}
}
