/**
 * 
 */
package com.synapsense.plugin.apinew;

import java.io.Serializable;
import java.util.Properties;

/**
 * @author stikhon
 * 
 */
public interface StatelessPlugin {

	/**
	 * Returns an unique plug-in identifier
	 * 
	 * @return
	 */
	String getID();

	void setPropertyValue(Properties params, Serializable newValue);

	Serializable getPropertyValue(Properties params);
}
