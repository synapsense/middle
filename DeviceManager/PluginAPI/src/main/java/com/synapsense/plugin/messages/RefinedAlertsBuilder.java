package com.synapsense.plugin.messages;

import com.synapsense.plugin.messages.GenericMessage.GenericMessageTypes;

public class RefinedAlertsBuilder implements MessageBuilder {
	public enum FieldConstants {
	MESSAGE, NAME, TYPE
	}

	private GenericMessage msg;

	public RefinedAlertsBuilder(String type, String name, String message) {
		msg = new GenericMessage(GenericMessageTypes.REFINED_ALERT);
		msg.put(FieldConstants.TYPE.name(), type);
		msg.put(FieldConstants.NAME.name(), name);
		msg.put(FieldConstants.MESSAGE.name(), message);
	}

	public RefinedAlertsBuilder(String name, String message) {
		this("UNKNOWN", name, message);
	}

	@Override
	public GenericMessage getMessage() {
		return msg;
	}

}
