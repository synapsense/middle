package com.synapsense.plugin.apinew;

import java.util.Map;

/**
 * Property descriptor. Each property has an unique name among other properties
 * of the same object. Property should have link to the parent object, which it
 * belongs to.
 * 
 * @author anechaev
 * 
 */
public interface PropertyDesc {
	/**
	 * Gets the property's unique name.
	 * 
	 * @return Property name.
	 */
	String getName();

	/**
	 * Gets property value.
	 * 
	 * @return Property value.
	 */
	String getValue();

	/**
	 * Sets property value.
	 * 
	 * @param value
	 *            The value to set.
	 */
	void setValue(String value);

	/**
	 * Gets all the tags for the property.
	 * 
	 * @return Map of all the tags.
	 */
	Map<String, TagDesc> getTags();

	/**
	 * Gets a property's tag by it's unique name.
	 * 
	 * @return Property tag for the given name.
	 */
	TagDesc getTag(String tagName);

	/**
	 * Gets ObjectDesc of an object, which this property belongs to.
	 * 
	 * @return ObjectDesc of the parent object.
	 */
	ObjectDesc getParent();
}
