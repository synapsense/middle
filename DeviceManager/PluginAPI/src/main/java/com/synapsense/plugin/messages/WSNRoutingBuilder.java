package com.synapsense.plugin.messages;

import com.synapsense.plugin.messages.GenericMessage.GenericMessageTypes;

/**
 * Implementation class used to build WSN Routing message
 * 
 * @author anechaev
 * 
 */
public class WSNRoutingBuilder implements MessageBuilder {
	public enum FieldConstants {
	PARENT_ID, CHILD_ID, NETWORK_ID
	}

	private GenericMessage msg;

	public WSNRoutingBuilder(long parentID, long childID, int networkID) {
		msg = new GenericMessage(GenericMessageTypes.WSN_ROUTING);

		msg.put(FieldConstants.PARENT_ID.name(), parentID);
		msg.put(FieldConstants.CHILD_ID.name(), childID);
		msg.put(FieldConstants.NETWORK_ID.name(), networkID);
	}

	@Override
	public GenericMessage getMessage() {
		return msg;
	}
}
