package com.synapsense.util;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import com.synapsense.util.algo.Pair;

/**
 * Works with time values given in different format: seconds, minutes and hours.
 * Supports packing, converting etc.
 * 
 * @author anechaev
 * 
 */
public class TimeSample extends Pair<Integer, Integer> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2062379741155594204L;
	private final static Pattern matcher = Pattern.compile("[0-9]+ (sec|min|hr)");
	private final static Pattern splitter = Pattern.compile(" ");

	private final static class Unit {
		private final String token;
		private final int factor;
		private final int id;

		Unit(String token, int factor, int id) {
			this.token = token;
			this.factor = factor;
			this.id = id;
		}

		public String getToken() {
			return token;
		}

		public int getFactor() {
			return factor;
		}

		public int getId() {
			return id;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + id;
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			final Unit other = (Unit) obj;
			if (id != other.id)
				return false;
			return true;
		}

	}

	private final static Unit[] idToUnit;
	private final static Map<String, Unit> tokenToUnit;
	static {
		Unit sec = new Unit("sec", 1, 0);
		Unit min = new Unit("min", 60, 1);
		Unit hr = new Unit("hr", 60 * 60, 2);
		idToUnit = new Unit[3];
		idToUnit[sec.getId()] = sec;
		idToUnit[min.getId()] = min;
		idToUnit[hr.getId()] = hr;
		tokenToUnit = new HashMap<String, Unit>();
		tokenToUnit.put(sec.getToken(), sec);
		tokenToUnit.put(min.getToken(), min);
		tokenToUnit.put(hr.getToken(), hr);
	}

	private final int seconds;

	private TimeSample(String sample) throws TimeSampleFormatException {
		super(0, 0);
		if (sample == null)
			throw new IllegalArgumentException("sample cannot be null");
		if (sample.isEmpty())
			throw new IllegalArgumentException("sample cannot be empty");
		if (!matcher.matcher(sample).matches())
			throw new TimeSampleFormatException(sample + " does not match timesample format");

		String data[] = splitter.split(sample);

		seconds = Integer.parseInt(data[0]) * tokenToUnit.get(data[1]).getFactor();
		packMe();
	}

	private TimeSample(int id, int value) {
		super(id, value);
		seconds = value * idToUnit[id].getFactor();
		packMe();
	}

	// timesample cache may be used to speed it up
	/**
	 * Creates a new timesample based upon string sample representation. Sample
	 * format: [0-9]+ (sec|min|hr)
	 * 
	 * @param sample
	 * @return timesample object
	 * @throws TimeSampleFormatException
	 *             if a sample does not meet the format
	 */
	public static TimeSample get(String sample) throws TimeSampleFormatException {
		return new TimeSample(sample);
	}

	/**
	 * Creates a timesample based on time unit id and value measured in units
	 * 
	 * @param id
	 *            unit id: 0-sec, 1-min, 2-hr
	 * @param value
	 *            time value in units
	 * @return
	 */
	public static TimeSample get(int id, int value) {
		return new TimeSample(id, value);
	}

	/**
	 * Packs the value in seconds: 60 sec -> 1 min 61 sec -> 61 sec 300 sec -> 5
	 * min 3600 sec -> 1 hr 3660 sec -> 61 min
	 */
	private void packMe() {
		first = 0;
		second = seconds;
		for (int i = 2; i >= 0; i--) {
			if ((seconds % idToUnit[i].getFactor()) == 0) {
				first = idToUnit[i].getId();
				second = seconds / idToUnit[i].getFactor();
				return;
			}
		}
	}

	/**
	 * returns the time sample value in seconds
	 * 
	 * @return
	 */
	public int toSeconds() {
		return seconds;
	}

	public String toString() {
		return second.toString() + " " + idToUnit[first].getToken();
	}

	public int getUnitID() {
		return first;
	}

	public int getValueInUnits() {
		return second;
	}
}
