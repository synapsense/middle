package com.synapsense.util;

import com.synapsense.util.Mapping.Type.Property;

/**
 * The default implementation class of Identity. The class overrides hashcode()
 * and equals() methods to support comparison logic.
 * 
 * @author anechaev
 * 
 */
public class IdentityImpl extends Identity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6159644664915207629L;
	private String propKey; // this is the unique key specifying the property

	public IdentityImpl(Property prop, Object value) {
		super(prop, value);
		propKey = generatePropKey(first);
	}

	@Override
	public Property getProperty() {
		return first;
	}

	@Override
	public Object getValue() {
		return second;
	}

	private static String generatePropKey(Property prop) {
		return prop.getName();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((propKey == null) ? 0 : propKey.hashCode());
		result = prime * result + ((second == null) ? 0 : second.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final IdentityImpl other = (IdentityImpl) obj;
		if (propKey == null) {
			if (other.propKey != null)
				return false;
		} else if (!propKey.equals(other.propKey))
			return false;
		if (second == null) {
			if (other.second != null)
				return false;
		} else if (!second.equals(other.second))
			return false;
		return true;
	}
}
