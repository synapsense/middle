package com.synapsense.util.algo;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class AsyncQueue<T> implements Runnable {
	private final UnaryFunctor<T> functor;
	protected final BlockingQueue<T> queue;
	private boolean interrupted = false;

	public AsyncQueue(UnaryFunctor<T> functor, int capacity) {
		this(functor, new LinkedBlockingQueue<T>(capacity));
	}

	public AsyncQueue(UnaryFunctor<T> functor) {
		this(functor, Integer.MAX_VALUE);
	}

	public AsyncQueue(UnaryFunctor<T> functor, BlockingQueue<T> queue) {
		this.functor = functor;
		this.queue = queue;
	}

	@Override
	public void run() {
		interrupted = false;
		while (!interrupted) {
			try {
				functor.process(queue.take());
			} catch (InterruptedException e) {
				interrupted = true;
			}
		}
	}

	public void add(T element) {
		queue.add(element);
	}

	public void interrupt() {
		interrupted = true;
	}
}
