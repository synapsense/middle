package com.synapsense.util;

import java.util.LinkedList;

/**
 * Mapping list represents a path used to find the particular object among group
 * of objects (by other words - it is used to navigate over devices and
 * particular device structure).
 * 
 * <p>
 * A short example: Let us to have a couple of devices with same structure:
 * 
 * <pre>
 * [A set
 *   [Device [identity [name="device 1"]]
 *    [Object [name="object 1"]
 *     [property [name="property 1", value="1"]]
 *    ]
 *   ]
 * 
 *   [Device [identity: name="device 2"]
 *    [Object [name="object 1"]
 *     [property [name="property 1", value="2"]]
 *    ]
 *   ]
 * ]
 * </pre>
 * 
 * and we want to address "property 1" from "device 1". Thus, we create given
 * mapping:
 * 
 * <pre>
 *  [MappingList
 *   [MappingItem [name="name" value="device 1"]] 
 *   [MappingItem [name="object_name" value="object 1"]]
 *  ]
 * </pre>
 * 
 * and pass it with property name "property 1" to the setter/getter method of
 * Device Manager.
 * 
 * See MappingItem description too.
 * 
 * @author anechaev
 * 
 */
public class MappingList extends LinkedList<MappingItem> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2815515417874495880L;

	/**
	 * A semantic sugar :-) for the first mapping item from mapping list.
	 * 
	 * @return
	 */
	public MappingItem getDeviceMapping() {
		return get(0);
	}

	public void addMapAtBeginning(MappingItem item) {
		addFirst(item);
	}
}
