package com.synapsense.util;

import java.util.Arrays;
import java.util.regex.Pattern;

import com.synapsense.util.algo.Pair;

/**
 * To simplify objects hierarchy navigation over objects of the particular
 * device the alternative mapping approach is proposed. It is based on
 * dot-separated notation of objects and properties. for a given device type:
 * [Device [name="device 1"] [Object [name="object 1"] [Object
 * [name="object 1-1"] [Property [name="property 1-1-1"]] ] ] [Object
 * [name="object 2"] [Property [name="property 2-1]] ] ] if we want to address
 * the property "property 1-1-1", we may point out only concrete device`s
 * identity and create a dot-separated property name
 * "object 1.object 1-1.property 1-1-1". It simplifies the MappingList. Object
 * names may be simple ones or indexed. Indexed Object name looks like this:
 * "object[<obj_index>]", where <obj_index> is a integer index. It is used to
 * address table entry for table objects.
 * 
 * The class PropPathSplitter is a helper class to perform the parsing procedure
 * of dot-separated property name.
 * 
 * @author anechaev
 * 
 */
public class PropPathSplitter extends java.util.LinkedList<String> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1583608824554746502L;

	private final static String ENC_PROP_DELIMITER = "\\.";

	private final static Pattern _isIndexed = Pattern.compile(".*\\[.*\\]");
	private final static Pattern _splitIndex = Pattern.compile("\\[|\\]");

	/**
	 * 
	 * An exception thrown in cases where given complex property name does not
	 * comply the dot-separated indexed token rules. The rules
	 * 
	 * @author anechaev
	 * 
	 */
	public static class MalformedPropertyPathException extends Exception {
		public MalformedPropertyPathException(String desc) {
			super(desc);
		}

		public MalformedPropertyPathException(String desc, Throwable reason) {
			super(desc, reason);
		}

		/**
		 * 
		 */
		private static final long serialVersionUID = -6307193621367123800L;

	}

	/**
	 * Converts dot-delimited String into a list of tokens.
	 * 
	 * @param mappedProp
	 */
	public PropPathSplitter(String mappedProp) {
		super(Arrays.asList(mappedProp.split(ENC_PROP_DELIMITER)));
	}

	/**
	 * @return true if the first token of the list is indexed, false otherwise
	 */
	public boolean isIndexed() {
		return isIndexed(0);
	}

	/**
	 * @param arg
	 *            - token number in the list
	 * @return true if the given token is indexed, false otherwise
	 */
	public boolean isIndexed(int arg) {
		return _isIndexed.matcher(get(arg)).matches();
	}

	/**
	 * For a given indexed token "token_name[index]" returns a pair, containing
	 * token_name as a first item and index as a second one.
	 * 
	 * @param arg
	 *            token number in the list
	 * @return generated pair of token name and token index
	 * @throws MalformedPropertyPathException
	 *             if given token is not indexed one.
	 */
	public Pair<String, Integer> splitIndex(int arg) throws MalformedPropertyPathException {
		try {
			String[] tokens = _splitIndex.split(get(arg));

			return new Pair<String, Integer>(tokens[0], Integer.parseInt(tokens[1]));
		} catch (Exception e) {
			throw new MalformedPropertyPathException("Cannot process indexed property.", e);
		}
	}
}
