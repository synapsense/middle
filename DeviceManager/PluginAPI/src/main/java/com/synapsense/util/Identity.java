package com.synapsense.util;

import java.io.Serializable;

import com.synapsense.util.Mapping.Type.Property;
import com.synapsense.util.algo.Pair;

/**
 * Class represents a unique property-value pair identifying an object among all
 * objects (and devices, too) with the same type
 * 
 * @author anechaev
 * 
 */
public abstract class Identity extends Pair<Property, Object> implements Serializable {
	/**
	 * @return property descriptor for the identity
	 */
	public abstract Property getProperty();

	/**
	 * @return value of the identity
	 */
	public abstract Object getValue();

	public Identity(Property property, Object value) {
		super(property, value);
	}
}
