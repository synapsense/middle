package com.synapsense.util;

import static org.junit.Assert.*;

public class Test {
	@org.junit.Test(expected = IllegalArgumentException.class)
	public void nullSample() throws Exception {
		TimeSample.get(null);
	}

	@org.junit.Test(expected = IllegalArgumentException.class)
	public void emptySample() throws Exception {
		TimeSample.get("");
	}

	@org.junit.Test
	public void goodSample() throws Exception {
		TimeSample.get("5 sec");
	}

	@org.junit.Test(expected = TimeSampleFormatException.class)
	public void badSample() throws Exception {
		TimeSample.get("a sec");
	}

	@org.junit.Test(expected = TimeSampleFormatException.class)
	public void anotherBadSample() throws Exception {
		TimeSample.get("5 df");
	}

	@org.junit.Test(expected = TimeSampleFormatException.class)
	public void yetAnotherBadSample() throws Exception {
		TimeSample.get("bbd");
	}

	@org.junit.Test
	public void packing() throws Exception {
		assertTrue(TimeSample.get("4 sec").toString().equals("4 sec"));
		assertTrue(TimeSample.get("60 sec").toString().equals("1 min"));
		assertTrue(TimeSample.get("61 sec").toString().equals("61 sec"));
		assertTrue(TimeSample.get("300 sec").toString().equals("5 min"));
		assertTrue(TimeSample.get("3600 sec").toString().equals("1 hr"));
		assertTrue(TimeSample.get("3660 sec").toString().equals("61 min"));
		assertTrue(TimeSample.get("3661 sec").toString().equals("3661 sec"));
	}

	@org.junit.Test
	public void toSeconds() throws Exception {
		assertTrue(TimeSample.get("4 sec").toSeconds() == 4);
		assertTrue(TimeSample.get("60 sec").toSeconds() == 60);
		assertTrue(TimeSample.get("61 sec").toSeconds() == 61);
		assertTrue(TimeSample.get("300 sec").toSeconds() == 300);
		assertTrue(TimeSample.get("3600 sec").toSeconds() == 3600);
		assertTrue(TimeSample.get("3660 sec").toSeconds() == 3660);
		assertTrue(TimeSample.get("3661 sec").toSeconds() == 3661);
	}

	@org.junit.Test
	public void equality() throws Exception {
		assertTrue(TimeSample.get("60 sec").equals(TimeSample.get("1 min")));
		assertTrue(TimeSample.get("300 sec").equals(TimeSample.get("5 min")));
		assertTrue(TimeSample.get("3600 sec").equals(TimeSample.get("1 hr")));
		assertTrue(TimeSample.get("60 min").equals(TimeSample.get("1 hr")));
	}
}
