package com.synapsense.plugin.transport.config;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.synapsense.plugin.configuration.ConfigurationService;
import org.apache.log4j.Logger;

import com.synapsense.plugin.apinew.Transport;
import com.synapsense.plugin.manager.DeviceManagerCore;

public class ConfigurationTransport implements Transport {
	private final static Logger logger = Logger.getLogger(ConfigurationTransport.class);
	private final static int PRIORITY = 2;
	private final static String DEFAULT_CONFIG_FILE_NAME = "pluginsConfig.xml";
	private final static String CONFIG_PROPERTY_NAME = "configFiles";

	private void configurePlugins() {
		if (!ConfigurationService.isAllowedToConfig(this)) {
			logger.info("ConfigurationTransport not allowed to configure plugins.");
			return;
		} else {
			List<String> configFileNames = new ArrayList<String>();
			// try to load semicolon separated file names list
			String namesFromConfig = System.getProperty(CONFIG_PROPERTY_NAME, null);
			if (namesFromConfig != null && !namesFromConfig.isEmpty()) {
				String[] splittedNames = namesFromConfig.split(";");
				configFileNames.addAll(Arrays.asList(splittedNames));
			} else {
				// no config files were found in the DM config file, try load
				// plugin config with default file name
				configFileNames.add(DEFAULT_CONFIG_FILE_NAME);
			}
			for (String configFileName : configFileNames) {
				File pluginConfig = new File("conf" + File.separator + configFileName);
				if (!pluginConfig.exists()) {
					if (DEFAULT_CONFIG_FILE_NAME.equals(configFileName)) {
						logger.warn("Plug-ins are not configured since no plug-in config files are provided. Put plug-ins config file with the name \""
						        + DEFAULT_CONFIG_FILE_NAME
						        + "\" into the conf directory or specify semicolon separated config file names list in the \""
						        + CONFIG_PROPERTY_NAME + "\" property of the DM config file.");
					} else {
						logger.warn("Cannot find specified plugins config file " + pluginConfig.getAbsolutePath()
						        + ". Plugins from this file will not be configured");
					}
					continue;
				}
				try {
					DeviceManagerCore.getInstance().configurePlugins(readFile(pluginConfig));
				} catch (Exception e) {
					logger.warn("Cannot configure plugins using file: " + configFileName, e);
					continue;
				}
			}
		}
	}

	private String readFile(File file) throws IOException {
		if (file == null)
			throw new IllegalArgumentException("file cannot be null");
		if (!file.exists())
			throw new IllegalArgumentException("file not found " + file);

		byte[] res = new byte[(int) file.length()];
		FileInputStream fis = new FileInputStream(file);
		try {
			fis.read(res);
		} finally {
			fis.close();
		}
		return new String(res);
	}

	@Override
	public void connect() throws IOException {
		configurePlugins();
	}

	@Override
	public int getPriority() {
		return PRIORITY;
	}

	@Override
	public void sendData(String objectID, String propertyName, Serializable value) {
		// nothing to do...
	}

	@Override
	public void sendOrderedData(String objectID, String propertyName, Serializable value) {
		// nothing to do...
	}
}
