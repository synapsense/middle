package com.synapsense.plugin.transport.rmi;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Collections;

public class InetAdressTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception {
		for (NetworkInterface ni : Collections.list(NetworkInterface.getNetworkInterfaces())) {
			System.out.println(ni.getDisplayName() + " : " + ni.getName());
			for (InetAddress ia : Collections.list(ni.getInetAddresses())) {
				System.out.println("\t"+ia.getHostAddress());
			}
		}

	}

}
