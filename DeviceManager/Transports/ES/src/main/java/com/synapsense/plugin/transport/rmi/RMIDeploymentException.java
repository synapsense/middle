package com.synapsense.plugin.transport.rmi;

public class RMIDeploymentException extends Exception {
	public RMIDeploymentException(String reason, Throwable e) {
		super(reason, e);
	}

	public RMIDeploymentException(String reason) {
		super(reason);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -8505410612814411337L;

}
