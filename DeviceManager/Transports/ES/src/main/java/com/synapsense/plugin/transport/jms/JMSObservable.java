package com.synapsense.plugin.transport.jms;

import java.util.Observable;

class JMSObservable extends Observable {
	@Override
	public void notifyObservers() {
		setChanged();
		super.notifyObservers();
	}
}
