package com.synapsense.plugin.transport.rmi;

import com.synapsense.plugin.DeviceManagerRMI;
import com.synapsense.plugin.apinew.Transport;
import com.synapsense.plugin.configuration.ConfigurationService;
import com.synapsense.plugin.manager.DeviceManagerCore;
import com.synapsense.plugin.transport.jms.JMSTransport;
import com.synapsense.service.impl.registrar.RegistrarSvc;
import org.apache.log4j.Logger;

import javax.management.MalformedObjectNameException;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.rmi.RemoteException;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.Properties;

public class RMITransport implements Transport, DeviceManagerRMI {
	private final static int PRIORITY = 2;
	private final static String JNDI_CONFIG = "jndi.properties";
	private final static Logger logger = Logger.getLogger(RMITransport.class);

	private Properties props;

	// BUG # 2710 fix;
	transient static RegistrarSvc proxy = null;

	private final class RegistrationAuditor extends Thread implements Observer {
		private Object lock = new Object();

		public RegistrationAuditor() {
			super("Registration auditor thread");
			setDaemon(true);
			JMSTransport.connectionStatus.addObserver(this);
		}

		@Override
		public void run() {
			boolean isRetry = false;
			while (true) {
				try {
					register();
					isRetry = false;
					synchronized (lock) {
						try {
							lock.wait();
						} catch (InterruptedException e) {
							return;
						}
					}
				} catch (Exception e) {
					logger.warn("Unable to register Device Manager on Environment Server....", e);
					// [BC 05/2016] Use the cached config only after a failed retry.
					// This makes sure we don't fall back to the cached config during normal startup when ES is not ready.
					if (isRetry) {
						try {
                            configurePlugins(null);
                        } catch (Exception e1) {
                            logger.error("Cannot reconfigure plugins with locally stored config", e1);
                        }
					}
					logger.info("Sleep for 60 sec before next attempt ...");
					try {
						Thread.sleep(60_000);
						isRetry = true;
					} catch (InterruptedException e1) {
						logger.debug("thread interrupted");
						return;
					}
					logger.info("Next attempt");
				}
			}
		}

		@Override
		public void update(Observable o, Object arg) {
			synchronized (lock) {
				lock.notifyAll();
			}
		}
	}

	private final static class Configuration implements Serializable {
		private static final long serialVersionUID = -4153997220278740597L;
		private final static String DEFAULT_REGISTRAR_JNDI = "es-ear/es-core/Registrar!com.synapsense.service.impl.registrar.RegistrarSvc";
		private final static String DEV_MAN_HANDSHAKE_TOKEN = "DeviceManager"; // the
		                                                                       // component
		                                                                       // name
		                                                                       // for
		                                                                       // Registrar
		                                                                       // service.
		                                                                       // DONT
		                                                                       // CHANGE
		                                                                       // IN
		                                                                       // UNTIL
		                                                                       // YOU
		                                                                       // CHANGE
		                                                                       // REGISTRAR

		int appPort = 2000;
		String appHost = "localhost";
		String registrarJndi = DEFAULT_REGISTRAR_JNDI;
		String componentName = DEV_MAN_HANDSHAKE_TOKEN;

		public static Configuration loadConfig() {
			Configuration dmConfig;

			dmConfig = new Configuration();
			try {
				dmConfig.appHost = System.getProperty("appHost", "localhost");
				dmConfig.appPort = Integer.parseInt(System.getProperty("appPort", "2000"));
				dmConfig.registrarJndi = System.getProperty("registrarJndi", DEFAULT_REGISTRAR_JNDI);
			} catch (Exception e) {
				if (logger.isDebugEnabled()) {
					// annoyning stack trace in log file is prohibited
					logger.warn("No valid config data... using default. (" + e.getLocalizedMessage() + ")");
				}
			}
			return dmConfig;
		}
	}

	transient private Configuration conf;

	public RMITransport() {
		props = new Properties();
		try {
			InputStreamReader config = new InputStreamReader(RMITransport.class.getClassLoader()
			        .getResource(JNDI_CONFIG).openStream());
			props.load(config);
			config.close();
		} catch (Exception e) {
			logger.warn("Couldn't load jndi.properties file", e);
		}
		conf = Configuration.loadConfig();
		Runtime.getRuntime().addShutdownHook(new Thread() {
			public void run() {
				if (proxy != null)
					proxy.unRegisterComponent(conf.componentName);
			}
		});

		try {
			RMIDeployer.deployApp(this, conf.appPort, conf.componentName);
		} catch (RMIDeploymentException e) {
			logger.error("Unable to deploy rmi part", e);
		}
	}

	private void register() throws NamingException, MalformedObjectNameException {
		logger.info("Registering the DM on Environment Server. ES to DM connection details: [" + conf.appHost + ":"
		        + conf.appPort + "]");
		InitialContext initialContext = new InitialContext(props);
		try {
			proxy = (RegistrarSvc) initialContext.lookup(conf.registrarJndi);
			proxy.registerComponent(conf.componentName, conf.appHost + ":" + conf.appPort);
		} finally {
			initialContext.close();
		}

		logger.info("DM registered");
	}

	@Override
	public void connect() throws IOException {
		new RegistrationAuditor().start();
	}

	@Override
	public int getPriority() {
		return PRIORITY;
	}

	@Override
	public void sendData(String objectID, String propertyName, Serializable value) {
		// do nothing
	}

	@Override
	public void sendOrderedData(String objectID, String propertyName, Serializable value) {
		// do nothing
	}

	@Override
	public void configurePlugins(String configuration) throws RemoteException {
		if (!ConfigurationService.isAllowedToConfig(this)) {
			logger.info("Not allowed to apply configuration to plugins");
			return;
		}

		DeviceManagerCore.getInstance().configurePlugins(configuration);
	}

	@Override
	public List<Properties> enumeratePlugins() {
		return DeviceManagerCore.getInstance().enumeratePlugins();
	}

	@Override
	public Serializable getPropertyValue(String objectID, String propertyName) {
		return DeviceManagerCore.getInstance().getPropertyValue(objectID, propertyName);
	}

	@Override
	public void setPropertyValue(String objectID, String propertyName, Serializable value) {
		DeviceManagerCore.getInstance().setPropertyValue(objectID, propertyName, value);
	}
}
