package com.synapsense.plugin.transport.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

public class RMIDeployer {
	private Registry srv;
	private static RMIDeployer inst = null;

	private RMIDeployer(int portNumber) throws RMIDeploymentException {
		try {
			srv = LocateRegistry.createRegistry(portNumber);
		} catch (RemoteException e) {
			throw new RMIDeploymentException(e.getLocalizedMessage());
		}
	}

	public synchronized static void deployApp(Remote app, int rmiPort, String mbeanDescr) throws RMIDeploymentException {
		if (inst == null) {
			inst = new RMIDeployer(rmiPort);
		}
		try {
			Remote stub = UnicastRemoteObject.exportObject(app, 0);
			inst.srv.rebind(mbeanDescr, stub);
		} catch (Exception e) {
			throw new RMIDeploymentException(e.getLocalizedMessage(), e);
		}
	}
}
