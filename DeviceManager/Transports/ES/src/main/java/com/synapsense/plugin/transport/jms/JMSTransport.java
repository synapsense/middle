package com.synapsense.plugin.transport.jms;

import com.synapsense.metrics.Metrics;
import com.synapsense.metrics.Timer;
import com.synapsense.plugin.apinew.Transport;
import com.synapsense.service.impl.messages.SensedMessage;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Observable;
import java.util.Properties;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Session;
import org.apache.log4j.Logger;
import org.hornetq.api.core.TransportConfiguration;
import org.hornetq.api.jms.HornetQJMSClient;
import org.hornetq.core.remoting.impl.netty.NettyConnectorFactory;
import org.hornetq.core.remoting.impl.netty.TransportConstants;
import org.hornetq.jms.client.HornetQJMSConnectionFactory;

public class JMSTransport implements Transport, Runnable {

    private final Thread thread;
    private final SendQueue sendQueue;
    private final String esHost;
    private final int jmsPort;

    // JMS state
    private Connection connection;
    private Session session;

    // [BC 05/2016] Legacy kludge for communicating with RMITransport.
    public static final Observable connectionStatus = new JMSObservable();

    private static final int PRIORITY = 1;
    private static final String DEFAULT_DESTINATION_NAME = "dm";
    private static final String JNDI_CONFIG = "jndi.properties";
    private static final Pattern ES_URL_PATTERN = Pattern.compile("remote://(\\S+):(\\d+)");

    private static final Logger logger = Logger.getLogger(JMSTransport.class);

    public JMSTransport() {
        thread = new Thread(this, "JMS Transport");
        thread.setDaemon(true);

        sendQueue = new SendQueue(Integer.parseInt(System.getProperty("jmsSendQueue", "60000")));

        Properties jndiProperties = loadProperties();
        esHost = extractEsHost(jndiProperties.getProperty("java.naming.provider.url"));
        jmsPort = extractJmsPort(jndiProperties.getProperty("jboss.jms.port"));
    }

    private static Properties loadProperties() {
        InputStream inputStream = ClassLoader.getSystemResourceAsStream(JNDI_CONFIG);
        if (inputStream == null) {
            throw new RuntimeException("Fatal error while initializing JMS transport. " + JNDI_CONFIG + " was not found on the classpath.");
        }
        Properties props = new Properties();
        try (InputStreamReader config = new InputStreamReader(inputStream)) {
            props.load(config);
        } catch (IOException e) {
            throw new RuntimeException("Fatal I/O error while initializing JMS transport.", e);
        }
        return props;
    }

    private static String extractEsHost(String namingUrl) {
        Matcher m = ES_URL_PATTERN.matcher(namingUrl);
        if (m.matches()) {
            return m.group(1);
        }
        throw new RuntimeException("Cannot get ES IP address from java.naming.provider.url=" + namingUrl);
    }

    private static int extractJmsPort(String string) {
        try {
            return string != null ? Integer.parseInt(string) : TransportConstants.DEFAULT_PORT;
        } catch (NumberFormatException e) {
            throw new RuntimeException("Invalid jboss.jms.port=" + string, e);
        }
    }

    @Override
    public int getPriority() {
        return PRIORITY;
    }

    @Override
    public void connect() {
        if (thread.isAlive()) {
            logger.error("Attempt to start JMS Transport multiple times ignored.");
            return;
        }
        thread.start();
        Runtime.getRuntime().addShutdownHook(new ShutdownHook());
    }

    @Override
    public void sendData(String objectID, String propertyName, Serializable value) {
        SensedMessage msg = new SensedMessage(objectID, propertyName, value, false);
        sendQueue.put(msg);
        if (logger.isDebugEnabled()) {
            logger.debug("Queued for sending to ES: " + msg);
        }
    }

    @Override
    public void sendOrderedData(String objectID, String propertyName, Serializable value) {
        SensedMessage msg = new SensedMessage(objectID, propertyName, value, true);
        sendQueue.put(msg);
        if (logger.isDebugEnabled()) {
            logger.debug("Queued for sending to ES: " + msg);
        }
    }

    @Override
    public void run() {
        try {
            //noinspection InfiniteLoopStatement
            while (true) {
                try {
                    connectJmsAndProcessQueue();
                } catch (JMSException e) {
                    logger.warn("JMS transport error.", e);
                } finally {
                    disconnectJms();
                }
                logger.info("Sleep for 70 before next attempt ...");
                Thread.sleep(70_000);
                logger.info("Next attempt ...");
            }
        } catch (InterruptedException e) {
            logger.info(thread + " terminated by interruption.");
        } catch (Exception e) {
            logger.error(thread + " thread terminated unexpectedly.", e);
        }
    }

    private void connectJmsAndProcessQueue() throws InterruptedException, JMSException {
        logger.info("Trying to connect to JMS [" + esHost + ":" + jmsPort + "]");

        Map<String, Object> params = new HashMap<>();
        params.put(TransportConstants.PORT_PROP_NAME, jmsPort);
        params.put(TransportConstants.HOST_PROP_NAME, esHost);
        TransportConfiguration conf = new TransportConfiguration(NettyConnectorFactory.class.getName(), params, "ES JMS Connection");

        ConnectionFactory connectionFactory = new HornetQJMSConnectionFactory(false, conf);
        connection = connectionFactory.createConnection();
        connection.start();
        Destination dest = HornetQJMSClient.createQueue(DEFAULT_DESTINATION_NAME);
        session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        MessageProducer publisher = session.createProducer(dest);

        logger.info("Connected to JMS [" + esHost + ":" + jmsPort + "]");

        processQueue(publisher);
    }

    /**
     * Processes the JMS send queue.
     *
     * <p>[BC 05/2016] This is a refactoring of the old JMSTransport.BatchPublisher (that actually didn't do any batching).</p>
     */
    private void processQueue(MessageProducer publisher) throws InterruptedException, JMSException {
        Timer sendTimer = Metrics.getInstance().getFactory().getTimer(JMSTransport.class, "send-data");
        while (!Thread.currentThread().isInterrupted()) {
            SensedMessage msg = sendQueue.take();
            sendTimer.time();
            try {
                ObjectMessage om = session.createObjectMessage(msg);
                om.setBooleanProperty("sequential", msg.isOrdered());
                publisher.send(om);
                if (logger.isDebugEnabled()) {
                    logger.debug("Sent to ES: " + msg);
                }
            } catch (JMSException e) {
                sendQueue.putBack(msg);
                throw e;
            } catch (RuntimeException e) {
                logger.warn("Failed to send to ES: " + msg, e);
            } finally {
                sendTimer.stop();
            }
        }
    }

    private void disconnectJms() {
        try {
            if (session != null) {
                try {
                    session.close();
                } catch (JMSException e) {
                    logger.info("Failed to close session.", e);
                }
                session = null;
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (JMSException e) {
                    logger.info("Failed to close connection.", e);
                }
                connection = null;
            }
            if (connectionStatus != null) {
                connectionStatus.notifyObservers();
            }
        } catch (Exception e) {
            logger.warn("Unexpected error while disconnecting.", e);
        }
    }

    private class ShutdownHook extends Thread {
        @Override
        public void run() {
            try {
                thread.interrupt();
                thread.join(1000);
                if (thread.isAlive()) {
                    logger.warn(thread.getName() + " could not be stopped in 1 sec.");
                }
            } catch (InterruptedException e) {
                logger.error("Shutdown hook interrupted. This should never happen.");
            }
        }
    }

    public static class SendQueue {
        private final BlockingDeque<SensedMessage> queue;
        // Once the queue size drops below this threshold, the discard warning is deactivated so it can be triggered again when capacity is exceeded.
        private final int discardWarningResetThreshold;

        // Access to this flag is not synchronized. In the worst case we can get multiple log messages and that's acceptable.
        private volatile boolean discardWarningActive;

        private SendQueue(int capacity) {
            queue = new LinkedBlockingDeque<>(capacity);
            discardWarningResetThreshold = capacity / 2;
            discardWarningActive = false;
            logger.info("JMS send queue with capacity " + capacity + " created.");
        }

        /** Unlike {@code BlockingQueue.put()}, this version drops items from the front of the queue to make room for the new one. */
        public void put(SensedMessage e) {
            while (!queue.offerLast(e)) {
                activateDiscardWarning();
                queue.pollFirst();
            }
        }

        private void activateDiscardWarning() {
            if (!discardWarningActive) {
                discardWarningActive = true;
                logger.warn("JMS send queue reached capacity and will start discarding old messages.");
            }
        }

        public SensedMessage take() throws InterruptedException {
            deactivateDiscardWarning();
            return queue.takeFirst();
        }

        private void deactivateDiscardWarning() {
            if (discardWarningActive && queue.size() < discardWarningResetThreshold) {
                discardWarningActive = false;
            }
        }

        public void putBack(SensedMessage e) {
            queue.offerFirst(e);
        }
    }
}
