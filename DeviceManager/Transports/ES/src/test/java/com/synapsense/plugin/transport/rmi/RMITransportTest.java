package com.synapsense.plugin.transport.rmi;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.rmi.RemoteException;
import java.util.List;
import java.util.Properties;

import javax.management.MBeanServerConnection;
import javax.naming.NamingException;

import mockit.Mocked;

import org.junit.Before;
import org.junit.Test;

public class RMITransportTest {
	RMITransport rmiTransport;
	@Mocked
	MBeanServerConnection srvInst;

	@Before
	public void beforeTest() {
		rmiTransport = new RMITransport();
	}

	public void testConnectFail() throws IOException, InterruptedException, NamingException {
		Thread thr = new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					rmiTransport.connect();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		});
		thr.start();
		Thread.sleep(100000);
	}

	@Test
	public void testconfigurePlugins() throws RemoteException {
		rmiTransport.configurePlugins("config");
	}

	@Test
	public void testEnumeratePlugin() {
		@SuppressWarnings("unused")
		List<Properties> list = rmiTransport.enumeratePlugins();
	}

	@Test
	public void testgetProperyValue() {
		rmiTransport.getPropertyValue("2358", "name");
	}

	@Test
	public void testSetPropertyValue() {
		rmiTransport.setPropertyValue("2358", "name", "ION 6200 Delta");
	}

	@Test
	public void testGetPriority() {
		assertEquals(2, rmiTransport.getPriority());
	}

	@Test
	public void testSendData() {
		rmiTransport.sendData("2358", "name", "ION 6200 Delta");
		rmiTransport.sendOrderedData("2358", "name", "ION 6200 Delta");
	}
}
