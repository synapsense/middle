package com.synapsense.plugin.transport.rmi;

import static org.junit.Assert.assertNotNull;

import com.synapsense.plugin.transport.rmi.RMIDeployer;
import com.synapsense.plugin.transport.rmi.RMIDeploymentException;
import java.rmi.Remote;
import java.rmi.registry.LocateRegistry;

import org.junit.Test;

public class RMIDeployerTest {

	Remote app = new Remote() {
	};

	@Test
	public void testDeployApp() throws Exception {
		RMIDeployer.deployApp(app, 17777, "app_name");
		assertNotNull(LocateRegistry.getRegistry("localhost", 17777).lookup("app_name"));
	}

	@Test(expected = RMIDeploymentException.class)
	public void testDeployAppWithException() throws RMIDeploymentException {
		RMIDeployer.deployApp(app, 0, null);
	}

}
