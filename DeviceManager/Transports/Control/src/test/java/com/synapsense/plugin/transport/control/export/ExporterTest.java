package com.synapsense.plugin.transport.control.export;

import junit.framework.Assert;

import org.junit.Test;

import com.synapsense.plugin.messages.GenericMessage;
import com.synapsense.plugin.messages.GenericMessage.GenericMessageTypes;

public class ExporterTest {

	@Test
	public void jsonExporterCorrectlySerializesGenericMessage() {
		GenericMessage msg = new GenericMessage(GenericMessageTypes.WSN_SENSOR_DATA);

		msg.put("f1", 2.0);
		msg.put("f2", "string");
		msg.put("f3", 1);

		String exported = new ControlGenericMessage("WSNSENSOR:1", "lastValue", msg)
		        .getStringMessage(new JsonExportStrategy());

		// don't care about the order or the full content just looking for
		// certain elements
		assertJsonElement("\"to\":\"WSNSENSOR:1\"", exported);
		assertJsonElement("\"propertyName\":\"lastValue\"", exported);
		assertJsonElement("\"f1\":2.0,\"", exported);
		assertJsonElement("\"f2\":\"string\"", exported);
		assertJsonElement("\"f3\":1", exported);
		assertJsonElement("\"type\":\"WSN_SENSOR_DATA\"", exported);
	}

	private static void assertJsonElement(String toCheck, String whole) {
		Assert.assertTrue(whole + " doesn't contain " + toCheck, whole.contains(toCheck));
	}
}
