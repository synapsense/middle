package com.synapsense.plugin.transport.control.export;

public interface ExportStrategy {

	/**
	 * Serializes <code>GenericControlMessage</code> to string
	 * 
	 * @param message
	 *            - message to export
	 * @return string representation of the passed object
	 */
	String exportMessage(ControlGenericMessage message);
}
