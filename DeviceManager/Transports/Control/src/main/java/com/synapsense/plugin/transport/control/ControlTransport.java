package com.synapsense.plugin.transport.control;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.synapsense.plugin.apinew.Transport;
import com.synapsense.plugin.messages.GenericMessage;
import com.synapsense.plugin.messages.GenericMessage.GenericMessageTypes;
import com.synapsense.plugin.messages.WSNSensorDataBuilder.FieldConstants;
import com.synapsense.plugin.transport.control.export.ControlGenericMessage;
import com.synapsense.plugin.transport.control.export.JsonExportStrategy;

public class ControlTransport implements Transport {
	private static final Logger log = Logger.getLogger(ControlTransport.class);
	private static final String CONFIG_FILE = "controltransport.properties";

	private DataServer dataServer;
	private Thread dsThread;

	private int serverConfPort = 9382;
	private int queueEntries = 100;

	private static HashSet<GenericMessageTypes> typesToProcess = new HashSet<GenericMessageTypes>();

	static {
		typesToProcess.add(GenericMessageTypes.WSN_SENSOR_DATA);
		typesToProcess.add(GenericMessageTypes.ACTUATOR_COMMAND_SENT);
		typesToProcess.add(GenericMessageTypes.ACTUATOR_COMMAND_FAILED);
		typesToProcess.add(GenericMessageTypes.ACTUATOR_POSITION);
	}

	@Override
	public int getPriority() {
		return 1;
	}

	@Override
	public void sendData(String objectID, String propertyName, Serializable value) {
		if (dataServer == null || dataServer.isAlive() == false) {
			log.debug("ControlTransport is not alive. Ignoring message.");
			return;
		}

		// too bad simulator message is not a generic message - i have to write
		// this hacky code
		// and also why allow sending any Serializable while sending only
		// GenericMessage?
		String message = null;
		if (value instanceof GenericMessage) {
			GenericMessage gm = (GenericMessage) value;
			if (typesToProcess.contains(gm.getType())) {
				message = new ControlGenericMessage(objectID, propertyName, gm)
				        .getStringMessage(new JsonExportStrategy());
			}
		} else if (value instanceof Double) {
			GenericMessage gm = new GenericMessage(GenericMessageTypes.WSN_SENSOR_DATA);
			gm.put(FieldConstants.DATA.name(), value);
			gm.put(FieldConstants.TIMESTAMP.name(), new Date().getTime());
			message = new ControlGenericMessage(objectID, propertyName, gm).getStringMessage(new JsonExportStrategy());
		}

		if (message == null) {
			log.trace("Unsupported message " + value.toString() + " ignoring...");
			return;
		}
		log.trace("Sending message " + value.toString() + " for " + objectID + " " + propertyName);

		dataServer.writeAll(message);
	}

	public void sendOrderedData(String objectID, String propertyName, Serializable value) {
		sendData(objectID, propertyName, value);
	}

	private void loadConfiguration() {
		Properties properties = new Properties();
		try {
            InputStreamReader config =  new InputStreamReader(ControlTransport.class.getClassLoader().getResource(CONFIG_FILE).openStream());
            properties.load(config);
            config.close();
			serverConfPort = Integer.parseInt(properties.getProperty("controlTransportPort"));
			int qe = Integer.parseInt(properties.getProperty("controlTransportMaxQueueEntries"));

			if (qe >= 1) {
				queueEntries = qe;
			}

			if (serverConfPort <= 1 || serverConfPort >= 65535) {
				log.error("Its not reasonable to bind to control or negative ports, defaulting to 9382");
				serverConfPort = 9382;
			}

		} catch (Exception e) {
			log.error("Can't load the configuration file or parse correctly.", e);
		}
	}

	public void connect() throws IOException {
		loadConfiguration();

		dataServer = new DataServer(serverConfPort, queueEntries);
		if (dataServer == null || dataServer.isAlive() == false) {
			log.error("Can't start the DataServer. JSONTransport is non-functional and will require a restart to device manager to recover.");
			return;
		}
		dsThread = new Thread(dataServer, "JSONTransport");
		dsThread.start();
	}
}
