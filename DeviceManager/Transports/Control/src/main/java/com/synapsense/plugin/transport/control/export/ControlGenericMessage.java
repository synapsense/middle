package com.synapsense.plugin.transport.control.export;

import com.synapsense.plugin.messages.GenericMessage;

public class ControlGenericMessage implements ControlMessage {

	private String objectId;

	private String propertyName;

	private GenericMessage message;;

	public ControlGenericMessage(String objectId, String propertyName, GenericMessage message) {
		this.objectId = objectId;
		this.propertyName = propertyName;
		this.message = message;
	}

	@Override
	public String getStringMessage(ExportStrategy exportStrategy) {
		return exportStrategy.exportMessage(this);
	}

	public String getObjectId() {
		return objectId;
	}

	public String getPropertyName() {
		return propertyName;
	}

	public GenericMessage getMessage() {
		return message;
	}
}
