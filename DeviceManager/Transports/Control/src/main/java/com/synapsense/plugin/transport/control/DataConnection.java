package com.synapsense.plugin.transport.control;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;

public class DataConnection {

	private java.util.concurrent.LinkedBlockingQueue<String> sendQ;

	private Socket client;
	private PrintWriter printWriter;
	private boolean active;
	private DCQueueWriter queued_writer;

	/**
	 * Provides yet another thread to manage writing to a socket. This will
	 * prevent the ES Transport from stalling when a receiving socket is not
	 * responding. Alternative: nio
	 */
	private class DCQueueWriter implements Runnable {
		public void run() {
			while (true) {
				String message = null;
				try {
					message = sendQ.take();
				} catch (InterruptedException e) {
					active = false;
					return;
				}
				printWriter.println(message);
			}
		}
	}

	public DataConnection(Socket t, int max_queue_entries) throws IOException {
		active = true;
		client = t;
		printWriter = new PrintWriter(client.getOutputStream(), true);
		sendQ = new java.util.concurrent.LinkedBlockingQueue<String>(max_queue_entries);
		queued_writer = this.new DCQueueWriter();
		Thread th = new Thread(queued_writer);
		th.start();
	}

	public void write(String message) throws IOException {
		/* Issue the request to the concurrent queue for sending */
		if (sendQ.offer(message) == false) {
			close();
			throw new IOException("jsonTransport DataConnection queue is full, something is wrong");
		}

	}

	public void close() {
		active = false;
		try {
			client.close();
		} catch (IOException e) {
			// Ignore
		}
	}

	public boolean isActive() {
		return active;
	}
}
