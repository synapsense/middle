package com.synapsense.plugin.transport.control.export;

public interface ControlMessage {

	/**
	 * Builds a string message using passed strategy
	 * 
	 * @param exportStrategy
	 *            strategy to use for serializing
	 * @return string representation of the message
	 */
	String getStringMessage(ExportStrategy exportStrategy);
}
