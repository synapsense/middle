package com.synapsense.plugin.transport.control.export;

import java.lang.reflect.Type;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.synapsense.plugin.messages.GenericMessage;

public class JsonExportStrategy implements ExportStrategy {

	private Gson gson;

	public JsonExportStrategy() {
		gson = new GsonBuilder().registerTypeAdapter(GenericMessage.class, new JsonGenericMessageSerializer())
		        .registerTypeAdapter(ControlGenericMessage.class, new GenericControlMessageSerializer()).create();
	}

	@Override
	public String exportMessage(ControlGenericMessage message) {
		return gson.toJson(message);
	}

	private static class GenericControlMessageSerializer implements JsonSerializer<ControlGenericMessage> {

		@Override
		public JsonElement serialize(ControlGenericMessage src, Type typeOfSrc, JsonSerializationContext context) {

			// serialize message first
			JsonObject element = (JsonObject) context.serialize(src.getMessage());

			// add the rest fields to it
			element.add("to", context.serialize(src.getObjectId()));
			element.add("propertyName", context.serialize(src.getPropertyName()));
			return element;
		}
	}

	private static class JsonGenericMessageSerializer implements JsonSerializer<GenericMessage> {

		@Override
		public JsonElement serialize(GenericMessage src, Type typeOfSrc, JsonSerializationContext context) {
			// make it serialize message like a map as it does by default
			JsonObject element = (JsonObject) context.serialize(src, Map.class);

			// now add message type to it
			element.add("type", context.serialize(src.getType()));

			return element;
		}
	}
}
