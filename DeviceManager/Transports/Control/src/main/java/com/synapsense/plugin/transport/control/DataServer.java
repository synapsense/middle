package com.synapsense.plugin.transport.control;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;

public class DataServer implements Runnable {

	private static final Logger log = Logger.getLogger(DataServer.class);
	private ServerSocket listenSocket;
	private List<DataConnection> dcList = new ArrayList<DataConnection>();
	private boolean alive = false;
	private int max_queue_entries = 100;

	public DataServer(int port, int queue_entries) {
		try {
			listenSocket = new ServerSocket();
			listenSocket.setReuseAddress(true);
			listenSocket.bind(new InetSocketAddress(port));
			log.info("JSONTransport is awaiting your connection on port " + port);
			alive = true;
			max_queue_entries = queue_entries;
		} catch (IOException e) {
			log.error("Can't start listener", e);
		}

	}

	public boolean isAlive() {
		return alive;
	}

	public void run() {
		while (true) {
			Socket client = null;
			try {
				client = listenSocket.accept();
				if (client != null) {
					log.info("New JSONTransport dataconnection from " + client.getInetAddress().getHostAddress());
					DataConnection dc = new DataConnection(client, max_queue_entries);
					synchronized (dcList) {
						dcList.add(dc);
					}
				}
			} catch (IOException e) {
				log.error("Can't accept", e);
			}
		}
	}

	public void writeAll(String message) {
		synchronized (dcList) {
			Iterator<DataConnection> dci = dcList.iterator();

			while (dci.hasNext()) {
				DataConnection dc = null;
				dc = dci.next();
				try {
					dc.write(message);
				} catch (IOException e) {
					log.warn("Exception writing to JSONTransport client:", e);
					dc.close();
					dci.remove();
				}
			}
		}
	}
}
