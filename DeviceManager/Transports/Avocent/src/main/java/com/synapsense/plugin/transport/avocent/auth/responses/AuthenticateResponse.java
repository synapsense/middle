package com.synapsense.plugin.transport.avocent.auth.responses;

import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPFault;
import javax.xml.soap.SOAPMessage;
import javax.xml.xpath.XPathConstants;

import org.apache.log4j.Logger;

import com.synapsense.plugin.transport.avocent.common.Response;
import com.synapsense.plugin.transport.avocent.utils.XmlHelper;

/**
 * 
 * @author Alexander Kravin
 * 
 */
public class AuthenticateResponse implements Response {
	private static final Logger log = Logger.getLogger(AuthenticateResponse.class);
	private String sessionId = null;
	private Boolean success = false;
	private boolean fault = false;

	@Override
	public void processMessage(SOAPMessage response) {
		try {
			SOAPBody body = response.getSOAPBody();
			SOAPFault fault = body.getFault();
			if (fault == null) {
				sessionId = (String) XmlHelper.evaluate(
				        "auth:authenticateResponse/auth:out/auth:entry[auth:key='SESSION_ID']/auth:value/text()", body,
				        XPathConstants.STRING);
				String returnCode = (String) XmlHelper.evaluate(
				        "auth:authenticateResponse/auth:out/auth:entry[auth:key='RETURN_CODE']/auth:value/text()",
				        body, XPathConstants.STRING);
				if ("SUCCESS".equalsIgnoreCase(returnCode)) {
					success = true;
				}
			} else {
				this.fault = true;
			}
		} catch (SOAPException ex) {
			log.warn("Cannot get a body. ", ex);
		}
	}

	public String getSessionId() {
		return sessionId;
	}

	public boolean isSuccess() {
		return success;
	}

	@Override
	public boolean isFault() {
		return fault;
	}

	public String toString() {
		return "{sessionId='" + sessionId + "'; success='" + success + "'}";
	}

	@Override
	public boolean isSessionExpired() {
		return false; // session is not used
	}
}
