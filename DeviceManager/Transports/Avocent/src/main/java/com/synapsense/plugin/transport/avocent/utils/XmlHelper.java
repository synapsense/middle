package com.synapsense.plugin.transport.avocent.utils;

import java.util.HashMap;
import java.util.Iterator;

import javax.xml.XMLConstants;
import javax.xml.namespace.NamespaceContext;
import javax.xml.namespace.QName;
import javax.xml.soap.SOAPFault;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.log4j.Logger;

/**
 * 
 * @author Alexander Kravin
 * 
 */
public class XmlHelper {
	private static final Logger log = Logger.getLogger(XmlHelper.class);
	private static XmlHelper instance = new XmlHelper();
	private HashMap<String, String> namespaces = new HashMap<String, String>();
	private XPath xpath = XPathFactory.newInstance().newXPath();

	private XmlHelper() {
		xpath.setNamespaceContext(new NamespaceContextImpl(this));
	}

	public static Object evaluate(String expression, Object element, QName returnType) {
		Object result = null;
		try {
			result = instance.xpath.evaluate(expression, element, returnType);
		} catch (XPathExpressionException e) {
			log.warn("Cannot evaluate expression='" + expression);
		}
		return result;
	}

	public static boolean isSessionExpired(SOAPFault fault) {
		boolean result = false;
		String faultString = (String) evaluate("soap:faultstring/text()", fault, XPathConstants.STRING);
		if ("Session has expired".equalsIgnoreCase(faultString)) {
			result = true;
		}
		return result;
	}

	public static void addNamespaceURI(String prefix, String namespaceURI) {
		instance.namespaces.put(prefix, namespaceURI);
	}

	private class NamespaceContextImpl implements NamespaceContext {
		private XmlHelper xmlHelper = null;

		public NamespaceContextImpl(XmlHelper xmlHelper) {
			this.xmlHelper = xmlHelper;
		}

		public String getNamespaceURI(String prefix) {
			if (prefix == null)
				throw new NullPointerException("Null prefix");

			String result = XMLConstants.NULL_NS_URI;
			if (xmlHelper.namespaces.containsKey(prefix)) {
				result = xmlHelper.namespaces.get(prefix);
			} else if ("xml".equals(prefix)) {
				result = XMLConstants.XML_NS_URI;
			}
			return result;
		}

		public String getPrefix(String uri) {
			throw new UnsupportedOperationException();
		}

		@SuppressWarnings("rawtypes")
		public Iterator getPrefixes(String uri) {
			throw new UnsupportedOperationException();
		}
	}
}
