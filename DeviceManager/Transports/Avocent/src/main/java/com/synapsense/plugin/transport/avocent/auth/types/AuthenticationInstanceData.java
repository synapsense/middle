package com.synapsense.plugin.transport.avocent.auth.types;

import java.util.ArrayList;
import java.util.List;

import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPException;
import javax.xml.xpath.XPathConstants;

import org.apache.log4j.Logger;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.synapsense.plugin.transport.avocent.common.ComplexType;
import com.synapsense.plugin.transport.avocent.utils.Prefix;
import com.synapsense.plugin.transport.avocent.utils.XmlHelper;

public class AuthenticationInstanceData implements ComplexType {
	private static final Logger log = Logger.getLogger(AuthenticationInstanceData.class);
	private List<AuthenticationField> fieldList = new ArrayList<AuthenticationField>();
	private String instanceName = null;
	private String instanceType = null;

	public AuthenticationInstanceData() {
	}

	public AuthenticationInstanceData(String instanceName, String instanceType) {
		this.instanceName = instanceName;
		this.instanceType = instanceType;
	}

	public void addAuthenticationField(AuthenticationField field) {
		fieldList.add(field);
	}

	@Override
	public void serialize(SOAPElement element) {
		SOAPElement tempElement;
		try {
			if (fieldList.size() > 0) {
				SOAPElement datafields = element.addChildElement("dataFields", Prefix.type);
				for (int i = 0; i < fieldList.size(); i++) {
					tempElement = datafields.addChildElement("AuthenticationField", Prefix.type);
					fieldList.get(i).serialize(tempElement);
				}
			}

			tempElement = element.addChildElement("instanceName", Prefix.type);
			tempElement.addTextNode(instanceName);

			tempElement = element.addChildElement("instanceType", Prefix.type);
			tempElement.addTextNode(instanceType);
		} catch (SOAPException e) {
			log.warn("Cannot serialize 'AuthenticationInstanceData': " + toString() + ".");
		}
	}

	@Override
	public void deserialize(Node element) {
		NodeList authFields = (NodeList) XmlHelper.evaluate("//type:dataFields/type:AuthenticationField", element,
		        XPathConstants.NODESET);
		if (authFields != null) {
			for (int i = 0; i < authFields.getLength(); i++) {
				AuthenticationField field = new AuthenticationField();
				field.deserialize(authFields.item(i));

				fieldList.add(field);
			}
		}

		instanceName = (String) XmlHelper.evaluate("type:instanceName/text()", element, XPathConstants.STRING);
		instanceType = (String) XmlHelper.evaluate("type:instanceType/text()", element, XPathConstants.STRING);
	}

	public String getInstanceName() {
		return instanceName;
	}

	public String getInstanceType() {
		return instanceType;
	}

	public String toString() {
		return "{instanceName='" + instanceName + "'; instanceType='" + instanceType + "'; fieldList=" + fieldList
		        + "}";
	}
}
