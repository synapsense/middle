package com.synapsense.plugin.transport.avocent.auth.requests;

import javax.xml.namespace.QName;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPBodyElement;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

import org.apache.log4j.Logger;

import com.synapsense.plugin.transport.avocent.common.BaseRequest;
import com.synapsense.plugin.transport.avocent.utils.Prefix;
import com.synapsense.plugin.transport.avocent.utils.SOAPHelper;

/**
 * 
 * @author Alexander Kravin
 * 
 */
public class IsAuthenticatedRequest extends BaseRequest {
	private static final Logger log = Logger.getLogger(IsAuthenticatedRequest.class);
	private String userName = null;
	private String checkedSessionId = null;
	private SOAPMessage request = null;

	public IsAuthenticatedRequest(String userName, String checkedSessionId, String sessionId) {
		super(sessionId);
		this.userName = userName;
		this.checkedSessionId = checkedSessionId;
	}

	private SOAPMessage createMessage() {
		SOAPMessage result = SOAPHelper.createMessage();
		try {
			addHeader(result);

			SOAPBody body = result.getSOAPBody();
			QName bodyName = new QName("http://service.authentication.amp.avocent.com", "isAuthenticated", Prefix.auth);
			SOAPBodyElement bodyElement = body.addBodyElement(bodyName);

			SOAPElement childElement = null;
			if (userName != null) {
				childElement = bodyElement.addChildElement("userName", Prefix.auth);
				childElement.addTextNode(userName);
			}
			if (checkedSessionId != null) {
				childElement = bodyElement.addChildElement("sessionId", Prefix.auth);
				childElement.addTextNode(checkedSessionId);
			}
		} catch (SOAPException e) {
			log.warn("Cannot create a message", e);
		}
		return result;
	}

	@Override
	public SOAPMessage getMessage() {
		if (request == null)
			request = createMessage();
		return request;
	}

	public String getCheckedSessionId() {
		return checkedSessionId;
	}
}
