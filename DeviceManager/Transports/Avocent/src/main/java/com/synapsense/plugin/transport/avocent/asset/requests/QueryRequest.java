package com.synapsense.plugin.transport.avocent.asset.requests;

import javax.xml.namespace.QName;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPBodyElement;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

import org.apache.log4j.Logger;

import com.synapsense.plugin.transport.avocent.asset.types.RequestedProperty;
import com.synapsense.plugin.transport.avocent.common.BaseRequest;
import com.synapsense.plugin.transport.avocent.utils.Prefix;
import com.synapsense.plugin.transport.avocent.utils.SOAPHelper;

/**
 * 
 * @author Alexander Kravin
 * 
 */
public class QueryRequest extends BaseRequest {
	private static final Logger log = Logger.getLogger(QueryRequest.class);
	private String queryString = null;
	private RequestedProperty property = null;
	private SOAPMessage request = null;

	public QueryRequest(String queryString, RequestedProperty property, String sessionId) {
		super(sessionId);
		this.queryString = queryString;
		this.property = property;
		this.sessionId = sessionId;
	}

	private SOAPMessage createMessage() {
		SOAPMessage result = SOAPHelper.createMessage();
		try {
			addHeader(result);
			SOAPBody body = result.getSOAPBody();
			QName bodyName = new QName("http://dvr.avocent.com/simple-asset", "QueryRequest", Prefix.asset);
			SOAPBodyElement bodyElement = body.addBodyElement(bodyName);

			SOAPElement tempElement = bodyElement.addChildElement("QueryString", Prefix.asset);
			if (queryString != null)
				tempElement.addTextNode(queryString);

			tempElement = bodyElement.addChildElement("RequestedProperties", Prefix.asset);
			property.serialize(tempElement);

		} catch (SOAPException e) {
			log.warn("Cannot create a message", e);
		}
		return result;
	}

	@Override
	public SOAPMessage getMessage() {
		if (request == null)
			request = createMessage();
		return request;
	}
}
