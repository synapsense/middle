package com.synapsense.plugin.transport.avocent.asset.responses;

import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPFault;
import javax.xml.soap.SOAPMessage;

import org.apache.log4j.Logger;

import com.synapsense.plugin.transport.avocent.common.Response;
import com.synapsense.plugin.transport.avocent.utils.XmlHelper;

/**
 * 
 * @author Alexander Kravin
 * 
 */
public class UnsetAssetResponse implements Response {
	private static final Logger log = Logger.getLogger(UnsetAssetResponse.class);
	private boolean fault = false;
	private boolean sessionExpired = false;

	@Override
	public boolean isFault() {
		return fault;
	}

	@Override
	public void processMessage(SOAPMessage response) {
		try {
			SOAPBody body = response.getSOAPBody();
			SOAPFault fault = body.getFault();
			if (fault != null) {
				this.fault = true;
				sessionExpired = XmlHelper.isSessionExpired(fault);
			}
		} catch (SOAPException ex) {
			log.warn("Cannot get a body. ", ex);
		}
	}

	@Override
	public boolean isSessionExpired() {
		return sessionExpired;
	}
}
