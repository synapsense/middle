package com.synapsense.plugin.transport.avocent.common;

import java.util.Hashtable;

import org.apache.log4j.Logger;

import com.synapsense.plugin.transport.avocent.asset.requests.GetAssetRequest;
import com.synapsense.plugin.transport.avocent.asset.requests.QueryRequest;
import com.synapsense.plugin.transport.avocent.asset.requests.SetAssetRequest;
import com.synapsense.plugin.transport.avocent.asset.requests.UnsetAssetRequest;
import com.synapsense.plugin.transport.avocent.asset.responses.GetAssetResponse;
import com.synapsense.plugin.transport.avocent.asset.responses.QueryResponse;
import com.synapsense.plugin.transport.avocent.asset.responses.SetAssetResponse;
import com.synapsense.plugin.transport.avocent.asset.responses.UnsetAssetResponse;
import com.synapsense.plugin.transport.avocent.auth.requests.AuthenticateRequest;
import com.synapsense.plugin.transport.avocent.auth.requests.GetAllInstancesRequest;
import com.synapsense.plugin.transport.avocent.auth.requests.IsAuthenticatedRequest;
import com.synapsense.plugin.transport.avocent.auth.responses.AuthenticateResponse;
import com.synapsense.plugin.transport.avocent.auth.responses.GetAllInstancesResponse;
import com.synapsense.plugin.transport.avocent.auth.responses.IsAuthenticatedResponse;

/**
 * 
 * @author Alexander Kravin
 * 
 */
public class MsgFactory {
	private static Logger log = Logger.getLogger(MsgFactory.class);
	private static final MsgFactory instance = new MsgFactory();

	// Mapping Request --> Response
	private Hashtable<Class<? extends Request>, Class<? extends Response>> mapping = new Hashtable<Class<? extends Request>, Class<? extends Response>>();

	private MsgFactory() {
		registerMessage(AuthenticateRequest.class, AuthenticateResponse.class);
		registerMessage(GetAllInstancesRequest.class, GetAllInstancesResponse.class);
		registerMessage(IsAuthenticatedRequest.class, IsAuthenticatedResponse.class);

		registerMessage(SetAssetRequest.class, SetAssetResponse.class);
		registerMessage(GetAssetRequest.class, GetAssetResponse.class);
		registerMessage(QueryRequest.class, QueryResponse.class);
		registerMessage(UnsetAssetRequest.class, UnsetAssetResponse.class);
	}

	public static MsgFactory getInstance() {
		return instance;
	}

	public void registerMessage(Class<? extends Request> request, Class<? extends Response> response) {
		log.debug("Registering REQUEST {" + request.getName() + "} RESPONSE {" + response.getName() + "}");
		mapping.put(request, response);
	}

	public Response createResponse(Class<? extends Request> request) {
		Response result = null;
		if (mapping.containsKey(request)) {
			try {
				result = mapping.get(request).newInstance();
			} catch (Exception e) {
				log.error("Cannot create a new instance of class {" + mapping.get(request).getName() + "}");
			}
		} else {
			log.warn("Class {" + request.getName() + "} is not registered.");
		}
		return result;
	}
}
