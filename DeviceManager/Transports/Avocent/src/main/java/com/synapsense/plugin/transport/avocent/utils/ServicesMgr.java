package com.synapsense.plugin.transport.avocent.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.synapsense.plugin.transport.avocent.auth.requests.AuthenticateRequest;
import com.synapsense.plugin.transport.avocent.auth.responses.AuthenticateResponse;

/**
 * 
 * @author Alexander Kravin
 * 
 */
public class ServicesMgr {
	private static final Logger log = Logger.getLogger(ServicesMgr.class);
	private static ServicesMgr instance = new ServicesMgr();
	private String config = "conf/avocent.properties";
	private String sessionId = null;
	private String authEndpoint = "https://amiedev2.avocent.com:8092/authenticate/";
	private String assetEndpoint = "https://amiedev2.avocent.com:8443/InfrastructureExplorer/ws/simple-asset/";
	private String userName = "syn";
	private String password = "syn!2GP12";

	private ServicesMgr() {
		Properties properties = new Properties();
		try {
			properties.load(new FileInputStream(config));
			// properties.loadFromXML(new FileInputStream(config));
			authEndpoint = properties.getProperty("authEndpoint");
			assetEndpoint = properties.getProperty("assetEndpoint");
			userName = properties.getProperty("username");
			password = properties.getProperty("password");
		} catch (IOException e) {
			log.fatal("Cannot load file='" + config + "'", e);
		}

	}

	private String pullSessionId() {
		AuthenticateRequest authRequest = new AuthenticateRequest(userName, password, "Internal", "");
		AuthenticateResponse authResponse = (AuthenticateResponse) SOAPHelper.sendRequest(authRequest, authEndpoint);
		return authResponse.getSessionId();
	}

	public static void updateSessionId() {
		instance.sessionId = instance.pullSessionId();
	}

	public static String getSessionId() {
		if (instance.sessionId == null)
			instance.sessionId = instance.pullSessionId();
		return instance.sessionId;
	}

	public static String getAssetEndpoint() {
		return instance.assetEndpoint;
	}
}
