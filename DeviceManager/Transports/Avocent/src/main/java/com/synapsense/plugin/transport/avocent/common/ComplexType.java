package com.synapsense.plugin.transport.avocent.common;

import javax.xml.soap.SOAPElement;

import org.w3c.dom.Node;

/**
 * 
 * @author Alexander Kravin
 * 
 */
public interface ComplexType {
	public void serialize(SOAPElement element);

	public void deserialize(Node element);
}
