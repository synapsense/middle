package com.synapsense.plugin.transport.avocent.auth.responses;

import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPFault;
import javax.xml.soap.SOAPMessage;
import javax.xml.xpath.XPathConstants;

import org.apache.log4j.Logger;

import com.synapsense.plugin.transport.avocent.common.Response;
import com.synapsense.plugin.transport.avocent.utils.XmlHelper;

/**
 * 
 * @author Alexander Kravin
 * 
 */
public class IsAuthenticatedResponse implements Response {
	private static final Logger log = Logger.getLogger(IsAuthenticatedResponse.class);
	private boolean fault = false;
	private Boolean authenticated = false;
	private boolean sessionExpired = false;

	@Override
	public boolean isFault() {
		return fault;
	}

	@Override
	public void processMessage(SOAPMessage response) {
		try {
			SOAPBody body = response.getSOAPBody();
			SOAPFault fault = body.getFault();
			if (fault == null) {
				authenticated = Boolean.parseBoolean((String) XmlHelper.evaluate(
				        "auth:isAuthenticatedResponse/auth:out/text()", body, XPathConstants.STRING));
			} else {
				this.fault = true;
				sessionExpired = XmlHelper.isSessionExpired(fault);
			}
		} catch (SOAPException ex) {
			log.warn("Cannot get a body. ", ex);
		}
	}

	public Boolean getAuthenticated() {
		return authenticated;
	}

	public String toString() {
		return "{authenticated='" + authenticated + "'}";
	}

	@Override
	public boolean isSessionExpired() {
		return sessionExpired;
	}
}
