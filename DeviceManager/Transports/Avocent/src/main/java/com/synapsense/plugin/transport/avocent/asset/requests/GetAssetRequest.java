package com.synapsense.plugin.transport.avocent.asset.requests;

import javax.xml.namespace.QName;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPBodyElement;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

import org.apache.log4j.Logger;

import com.synapsense.plugin.transport.avocent.asset.types.RequestedProperty;
import com.synapsense.plugin.transport.avocent.common.BaseRequest;
import com.synapsense.plugin.transport.avocent.utils.Prefix;
import com.synapsense.plugin.transport.avocent.utils.SOAPHelper;

/**
 * 
 * @author Alexander Kravin
 * 
 */
public class GetAssetRequest extends BaseRequest {
	private static final Logger log = Logger.getLogger(GetAssetRequest.class);
	private Long assetId = null;
	private RequestedProperty property = null;
	private SOAPMessage request = null;

	public GetAssetRequest(long assetId, RequestedProperty property, String sessionId) {
		super(sessionId);
		this.assetId = assetId;
		this.property = property;
	}

	private SOAPMessage createMessage() {
		SOAPMessage result = SOAPHelper.createMessage();
		try {
			addHeader(result);
			SOAPBody body = result.getSOAPBody();
			QName bodyName = new QName("http://dvr.avocent.com/simple-asset", "GetAssetRequest", Prefix.asset);
			SOAPBodyElement bodyElement = body.addBodyElement(bodyName);

			SOAPElement tempElement = bodyElement.addChildElement("AssetId", Prefix.asset);
			tempElement.addTextNode(assetId.toString());

			tempElement = bodyElement.addChildElement("RequestedProperties", Prefix.asset);
			property.serialize(tempElement);
		} catch (SOAPException e) {
			log.warn("Cannot create a message", e);
		}
		return result;
	}

	@Override
	public SOAPMessage getMessage() {
		if (request == null)
			request = createMessage();
		return request;
	}
}
