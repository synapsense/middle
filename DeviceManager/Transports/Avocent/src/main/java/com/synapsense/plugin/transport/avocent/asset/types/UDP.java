package com.synapsense.plugin.transport.avocent.asset.types;

import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPException;
import javax.xml.xpath.XPathConstants;

import org.apache.log4j.Logger;
import org.w3c.dom.Node;

import com.synapsense.plugin.transport.avocent.common.ComplexType;
import com.synapsense.plugin.transport.avocent.utils.Prefix;
import com.synapsense.plugin.transport.avocent.utils.XmlHelper;

/**
 * 
 * @author Alexander Kravin
 * 
 */
public class UDP implements ComplexType {
	private static final Logger log = Logger.getLogger(UDP.class);
	private String label = null;
	private String value = null;

	public UDP() {
	}

	public UDP(String label, String value) {
		this.label = label;
		this.value = value;
	}

	public String getLabel() {
		return label;
	}

	public String getValue() {
		return value;
	}

	@Override
	public void serialize(SOAPElement element) {
		SOAPElement tempElement;

		try {
			tempElement = element.addChildElement("Label", Prefix.asset);
			tempElement.addTextNode(label);

			tempElement = element.addChildElement("Value", Prefix.asset);
			tempElement.addTextNode(value);
		} catch (SOAPException e) {
			log.warn("Cannot serialize 'UDP': " + toString() + ".");
		}
	}

	@Override
	public void deserialize(Node element) {
		label = (String) XmlHelper.evaluate("sim:Label/text()", element, XPathConstants.STRING);
		value = (String) XmlHelper.evaluate("sim:Value/text()", element, XPathConstants.STRING);
	}

	public String toString() {
		return "{label='" + label + "'; value='" + value + "'}";
	}

}
