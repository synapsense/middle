package com.synapsense.plugin.transport.avocent.asset.requests;

import java.util.ArrayList;
import java.util.List;

import javax.xml.namespace.QName;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPBodyElement;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

import org.apache.log4j.Logger;

import com.synapsense.plugin.transport.avocent.common.BaseRequest;
import com.synapsense.plugin.transport.avocent.utils.Prefix;
import com.synapsense.plugin.transport.avocent.utils.SOAPHelper;

/**
 * 
 * @author Alexander Kravin
 * 
 */
public class UnsetAssetRequest extends BaseRequest {
	private static final Logger log = Logger.getLogger(UnsetAssetRequest.class);
	private Long assetId = null;
	private List<String> derateList = new ArrayList<String>();
	private List<String> udpList = new ArrayList<String>();
	private SOAPMessage request = null;

	public UnsetAssetRequest(long assetId, String sessionId) {
		super(sessionId);
		this.assetId = assetId;
	}

	public void addDerateLabel(String derateLabel) {
		derateList.add(derateLabel);
	}

	public void addUdpLabel(String udpLabel) {
		udpList.add(udpLabel);
	}

	private SOAPMessage createMessage() {
		SOAPMessage result = SOAPHelper.createMessage();
		try {
			addHeader(result);
			SOAPBody body = result.getSOAPBody();
			QName bodyName = new QName("http://dvr.avocent.com/simple-asset", "UnsetAssetRequest", Prefix.asset);
			SOAPBodyElement bodyElement = body.addBodyElement(bodyName);

			SOAPElement tempElement = null;
			if (udpList.size() > 0) {
				tempElement = bodyElement.addChildElement("UDP", Prefix.asset);
				for (int i = 0; i < udpList.size(); i++) {
					SOAPElement label = tempElement.addChildElement("Label", Prefix.asset);
					label.addTextNode(udpList.get(i));
				}
			}
			if (derateList.size() > 0) {
				tempElement = bodyElement.addChildElement("Derate", Prefix.asset);
				for (int i = 0; i < derateList.size(); i++) {
					SOAPElement label = tempElement.addChildElement("Label", Prefix.asset);
					label.addTextNode(derateList.get(i));
				}
			}
		} catch (SOAPException e) {
			log.warn("Cannot create a message", e);
		}
		return result;
	}

	@Override
	public SOAPMessage getMessage() {
		if (request == null)
			request = createMessage();
		return request;
	}

	public Long getAssetId() {
		return assetId;
	}

}
