package com.synapsense.plugin.transport.avocent.auth.responses;

import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPFault;
import javax.xml.soap.SOAPMessage;
import javax.xml.xpath.XPathConstants;

import org.apache.log4j.Logger;
import org.w3c.dom.Node;

import com.synapsense.plugin.transport.avocent.auth.types.AuthenticationInstanceData;
import com.synapsense.plugin.transport.avocent.common.Response;
import com.synapsense.plugin.transport.avocent.utils.XmlHelper;

/**
 * 
 * @author Alexander Kravin
 * 
 */
public class GetAllInstancesResponse implements Response {
	private static final Logger log = Logger.getLogger(GetAllInstancesResponse.class);
	private boolean fault = false;
	private AuthenticationInstanceData authData = null;

	public GetAllInstancesResponse() {
	}

	public GetAllInstancesResponse(AuthenticationInstanceData authData) {
		this.authData = authData;
	}

	@Override
	public void processMessage(SOAPMessage response) {
		try {
			SOAPBody body = response.getSOAPBody();
			SOAPFault fault = body.getFault();
			if (fault == null) {
				Node authDataNode = (Node) XmlHelper.evaluate(
				        "//auth:getAllInstancesResponse/auth:out/type:AuthenticationInstanceData", body,
				        XPathConstants.NODE);
				if (authData == null)
					authData = new AuthenticationInstanceData();
				authData.deserialize(authDataNode);
			} else {
				this.fault = true;
			}
		} catch (SOAPException ex) {
			log.warn("Cannot get a body. ", ex);
		}
	}

	@Override
	public boolean isFault() {
		return fault;
	}

	public String toString() {
		return "{authData=" + authData + "}";
	}

	@Override
	public boolean isSessionExpired() {
		return false; // session is not used
	}
}
