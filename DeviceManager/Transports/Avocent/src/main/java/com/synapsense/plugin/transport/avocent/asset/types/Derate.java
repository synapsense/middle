package com.synapsense.plugin.transport.avocent.asset.types;

import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPException;
import javax.xml.xpath.XPathConstants;

import org.apache.log4j.Logger;
import org.w3c.dom.Node;

import com.synapsense.plugin.transport.avocent.common.ComplexType;
import com.synapsense.plugin.transport.avocent.utils.Prefix;
import com.synapsense.plugin.transport.avocent.utils.XmlHelper;

/**
 * 
 * @author Alexander Kravin
 * 
 */
public class Derate implements ComplexType {
	private static final Logger log = Logger.getLogger(Derate.class);
	private Heat heat = null;
	private Weight weight = null;
	private Power power = null;

	public Derate() {
	}

	public Derate(Heat heat, Weight weight, Power power) {
		this.heat = heat;
		this.weight = weight;
		this.power = power;
	}

	public Weight getWeight() {
		return weight;
	}

	public void setWeight(Weight weight) {
		this.weight = weight;
	}

	public Heat getHeat() {
		return heat;
	}

	public void setHeat(Heat heat) {
		this.heat = heat;
	}

	public Power getPower() {
		return power;
	}

	public void setPower(Power power) {
		this.power = power;
	}

	@Override
	public void serialize(SOAPElement element) {
		SOAPElement tempElement;
		try {
			if (heat != null) {
				tempElement = element.addChildElement("Heat", Prefix.asset);
				heat.serialize(tempElement);
			}

			if (weight != null) {
				tempElement = element.addChildElement("Weight", Prefix.asset);
				weight.serialize(tempElement);
			}

			if (power != null) {
				tempElement = element.addChildElement("Power", Prefix.asset);
				power.serialize(tempElement);
			}
		} catch (SOAPException e) {
			log.warn("Cannot serialize 'Derate': " + toString() + ".");
		}
	}

	@Override
	public void deserialize(Node element) {
		Node heatNode = (Node) XmlHelper.evaluate("sim:Heat", element, XPathConstants.NODE);
		if (heatNode != null) {
			heat = new Heat();
			heat.deserialize(heatNode);
		}
		Node weightNode = (Node) XmlHelper.evaluate("sim:Weight", element, XPathConstants.NODE);
		if (weightNode != null) {
			weight = new Weight();
			weight.deserialize(weightNode);
		}
		Node powerNode = (Node) XmlHelper.evaluate("sim:Power", element, XPathConstants.NODE);
		if (powerNode != null) {
			power = new Power();
			power.deserialize(powerNode);
		}
	}

	public String toString() {
		return "{Heat=" + heat + "; Weight=" + weight + "; Power=" + power + "}";
	}
}
