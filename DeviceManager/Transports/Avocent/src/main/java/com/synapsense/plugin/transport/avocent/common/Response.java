package com.synapsense.plugin.transport.avocent.common;

import javax.xml.soap.SOAPMessage;

/**
 * 
 * @author Alexander Kravin
 * 
 */
public interface Response {
	public void processMessage(SOAPMessage response);

	public boolean isFault();

	public boolean isSessionExpired();
}
