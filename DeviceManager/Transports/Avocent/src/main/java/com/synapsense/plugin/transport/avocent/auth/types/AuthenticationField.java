package com.synapsense.plugin.transport.avocent.auth.types;

import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPException;
import javax.xml.xpath.XPathConstants;

import org.apache.log4j.Logger;
import org.w3c.dom.Node;

import com.synapsense.plugin.transport.avocent.common.ComplexType;
import com.synapsense.plugin.transport.avocent.utils.Prefix;
import com.synapsense.plugin.transport.avocent.utils.XmlHelper;

public class AuthenticationField implements ComplexType {
	private static final Logger log = Logger.getLogger(AuthenticationField.class);
	private String displayName = null;
	private String id = null;
	private Integer maxLength = null;
	private Boolean required = false;
	private String type = null;

	public AuthenticationField() {
	}

	@Override
	public void serialize(SOAPElement element) {
		SOAPElement tempElement;
		try {
			tempElement = element.addChildElement("displayName", Prefix.type);
			tempElement.addTextNode(displayName);

			tempElement = element.addChildElement("id", Prefix.type);
			tempElement.addTextNode(id);

			tempElement = element.addChildElement("maxLength", Prefix.type);
			tempElement.addTextNode(maxLength.toString());

			tempElement = element.addChildElement("required", Prefix.type);
			tempElement.addTextNode(required.toString());

			tempElement = element.addChildElement("type", Prefix.type);
			tempElement.addTextNode(type);
		} catch (SOAPException e) {
			log.warn("Cannot serialize 'AuthenticationField': " + toString() + ".");
		}
	}

	@Override
	public void deserialize(Node element) {
		displayName = (String) XmlHelper.evaluate("type:displayName/text()", element, XPathConstants.STRING);
		id = (String) XmlHelper.evaluate("type:id/text()", element, XPathConstants.STRING);
		maxLength = ((Double) XmlHelper.evaluate("type:maxLength/text()", element, XPathConstants.NUMBER)).intValue();
		required = Boolean.parseBoolean((String) XmlHelper.evaluate("type:required/text()", element,
		        XPathConstants.STRING));
		type = (String) XmlHelper.evaluate("type:type/text()", element, XPathConstants.STRING);
	}

	public String getDisplayName() {
		return displayName;
	}

	public String getId() {
		return id;
	}

	public Integer getMaxLength() {
		return maxLength;
	}

	public Boolean getRequired() {
		return required;
	}

	public String getType() {
		return type;
	}

	public String toString() {
		return "{displayName='" + displayName + "'; id='" + id + "'; maxLength=" + maxLength + "; required='"
		        + required + "'; type='" + type + "'}";
	}
}
