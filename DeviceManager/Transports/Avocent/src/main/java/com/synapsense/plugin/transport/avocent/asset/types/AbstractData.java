package com.synapsense.plugin.transport.avocent.asset.types;

import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPException;
import javax.xml.xpath.XPathConstants;

import org.apache.log4j.Logger;
import org.w3c.dom.Node;

import com.synapsense.plugin.transport.avocent.common.ComplexType;
import com.synapsense.plugin.transport.avocent.utils.Prefix;
import com.synapsense.plugin.transport.avocent.utils.XmlHelper;

/**
 * 
 * @author Alexander Kravin
 * 
 */
public abstract class AbstractData implements ComplexType {
	private static final Logger log = Logger.getLogger(AbstractData.class);
	protected String units = null;
	protected Float value = null;
	protected Boolean isAggregate = false;

	public AbstractData() {
	}

	public AbstractData(String units, float value, Boolean isAggregate) {
		this.units = units;
		this.value = value;
		this.isAggregate = isAggregate;
	}

	public String getUnits() {
		return units;
	}

	public float getValue() {
		return value;
	}

	public Boolean getIsAggregate() {
		return isAggregate;
	}

	@Override
	public void serialize(SOAPElement element) {
		SOAPElement tempElement;
		try {
			tempElement = element.addChildElement("Units", Prefix.asset);
			tempElement.addTextNode(units);

			tempElement = element.addChildElement("Value", Prefix.asset);
			tempElement.addTextNode(value.toString());

			tempElement = element.addChildElement("IsAggregate", Prefix.asset);
			tempElement.addTextNode(isAggregate.toString());
		} catch (SOAPException e) {
			log.warn("Cannot serialize complex type: " + toString() + ".");
		}
	}

	@Override
	public void deserialize(Node element) {
		units = (String) XmlHelper.evaluate("sim:Units", element, XPathConstants.STRING);
		value = ((Double) XmlHelper.evaluate("sim:Value", element, XPathConstants.NUMBER)).floatValue();
		isAggregate = new Boolean((String) XmlHelper.evaluate("sim:IsAggregate", element, XPathConstants.STRING));
	}

	public String toString() {
		return "{Value='" + value + "'; Units='" + units + "'; isAggregate='" + isAggregate + "'}";
	}
}
