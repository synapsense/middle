package com.synapsense.plugin.transport.avocent.asset.types;

/**
 * 
 * @author Alexander Kravin
 * 
 */
public class Power extends AbstractData {

	public Power() {
		super();
	}

	public Power(String units, float value, Boolean isAggregate) {
		super(units, value, isAggregate);
	}
}
