package com.synapsense.plugin.transport.avocent.utils;

/**
 * 
 * @author Alexander Kravin
 * 
 */
public class Prefix {
	public static String auth = "auth";
	public static String asset = "sim";
	public static String wsse = "wsse";
	public static String type = "type";
	public static String soap = "soap";

	static {
		XmlHelper.addNamespaceURI(auth, "http://service.authentication.amp.avocent.com");
		XmlHelper.addNamespaceURI(asset, "http://dvr.avocent.com/simple-asset");
		XmlHelper.addNamespaceURI(wsse,
		        "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
		XmlHelper.addNamespaceURI(type, "http://types.service.authentication.amp.avocent.com");
		XmlHelper.addNamespaceURI(soap, "http://schemas.xmlsoap.org/soap/envelope/");
	}
}
