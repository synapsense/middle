package com.synapsense.plugin.transport.avocent.asset.types;

import java.util.ArrayList;
import java.util.List;

import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPException;
import javax.xml.xpath.XPathConstants;

import org.apache.log4j.Logger;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.synapsense.plugin.transport.avocent.common.ComplexType;
import com.synapsense.plugin.transport.avocent.utils.Prefix;
import com.synapsense.plugin.transport.avocent.utils.XmlHelper;

/**
 * 
 * @author Alexander Kravin
 * 
 */
public class RequestedProperty implements ComplexType {
	private static final Logger log = Logger.getLogger(RequestedProperty.class);
	private String name = null;
	private List<String> derateList = new ArrayList<String>();
	private List<String> udpList = new ArrayList<String>();

	public RequestedProperty() {
	}

	public RequestedProperty(String name) {
		this.name = name;
	}

	public void addDerate(String derate) {
		derateList.add(derate);
	}

	public void addUdp(String udp) {
		udpList.add(udp);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<String> getDerateList() {
		return derateList;
	}

	public List<String> getUdpList() {
		return udpList;
	}

	@Override
	public void serialize(SOAPElement element) {
		SOAPElement tempElement;
		try {
			tempElement = element.addChildElement("Name", Prefix.asset);
			if (name != null)
				tempElement.addTextNode(name);

			for (int i = 0; i < derateList.size(); i++) {
				tempElement = element.addChildElement("Derate", Prefix.asset);
				tempElement.addTextNode(derateList.get(i));
			}

			for (int i = 0; i < udpList.size(); i++) {
				tempElement = element.addChildElement("UDP", Prefix.asset);
				tempElement.addTextNode(udpList.get(i));
			}
		} catch (SOAPException e) {
			log.warn("Cannot serialize 'RequestedProperty': " + toString() + ".");
		}
	}

	@Override
	public void deserialize(Node element) {
		name = (String) XmlHelper.evaluate("sim:Name/text()", element, XPathConstants.STRING);

		NodeList derates = (NodeList) XmlHelper.evaluate("sim:Derate", element, XPathConstants.NODESET);
		if (derates != null) {
			for (int i = 0; i < derates.getLength(); i++) {
				String derate = (String) XmlHelper.evaluate("text()", element, XPathConstants.STRING);
				derateList.add(derate);
			}
		}

		NodeList udps = (NodeList) XmlHelper.evaluate("sim:UDP", element, XPathConstants.NODESET);
		if (udps != null) {
			for (int i = 0; i < udps.getLength(); i++) {
				String udp = (String) XmlHelper.evaluate("text()", element, XPathConstants.STRING);
				udpList.add(udp);
			}
		}
	}

	public String toString() {
		return "{Name='" + name + "'; derateList=" + derateList + "; udpList=" + udpList + "}";
	}
}
