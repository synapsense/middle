package com.synapsense.plugin.transport.avocent.asset.types;

import java.util.ArrayList;
import java.util.List;

import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPException;
import javax.xml.xpath.XPathConstants;

import org.apache.log4j.Logger;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.synapsense.plugin.transport.avocent.common.ComplexType;
import com.synapsense.plugin.transport.avocent.utils.Prefix;
import com.synapsense.plugin.transport.avocent.utils.XmlHelper;

/**
 * 
 * @author Alexander Kravin
 * 
 */
public class Property implements ComplexType {
	private static final Logger log = Logger.getLogger(Property.class);
	private String name = null;
	private List<Derate> derateList = new ArrayList<Derate>();
	private List<UDP> udpList = new ArrayList<UDP>();

	public Property() {
	}

	public Property(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void addDerate(Derate derate) {
		derateList.add(derate);
	}

	public void addUdp(UDP udp) {
		udpList.add(udp);
	}

	@Override
	public void serialize(SOAPElement element) {
		SOAPElement tempElement;
		try {
			if (name != null) {
				tempElement = element.addChildElement("Name", Prefix.asset);
				tempElement.addTextNode(name);
			}

			for (int i = 0; i < derateList.size(); i++) {
				tempElement = element.addChildElement("Derate", Prefix.asset);
				derateList.get(i).serialize(tempElement);
			}

			for (int i = 0; i < udpList.size(); i++) {
				tempElement = element.addChildElement("UDP", Prefix.asset);
				udpList.get(i).serialize(tempElement);
			}
		} catch (SOAPException e) {
			log.warn("Cannot serialize 'Property': " + toString() + ".");
		}
	}

	@Override
	public void deserialize(Node element) {
		name = (String) XmlHelper.evaluate("sim:Name/text()", element, XPathConstants.STRING);

		NodeList derates = (NodeList) XmlHelper.evaluate("sim:Derate", element, XPathConstants.NODESET);
		if (derates != null) {
			for (int i = 0; i < derates.getLength(); i++) {
				Derate derate = new Derate();
				derate.deserialize(derates.item(i));

				derateList.add(derate);
			}
		}

		NodeList udps = (NodeList) XmlHelper.evaluate("sim:UDP", element, XPathConstants.NODESET);
		if (udps != null) {
			for (int i = 0; i < udps.getLength(); i++) {
				UDP udp = new UDP();
				udp.deserialize(udps.item(i));

				udpList.add(udp);
			}
		}
	}

	public String toString() {
		return "{Name='" + name + "'; derateList=" + derateList + "; udpList=" + udpList + "}";
	}

	public List<UDP> getUdpList() {
		return udpList;
	}
}
