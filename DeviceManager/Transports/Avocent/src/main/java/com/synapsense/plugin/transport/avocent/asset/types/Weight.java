package com.synapsense.plugin.transport.avocent.asset.types;

/**
 * 
 * @author Alexander Kravin
 * 
 */
public class Weight extends AbstractData {

	public Weight() {
		super();
	}

	public Weight(String units, float value, Boolean isAggregate) {
		super(units, value, isAggregate);
	}
}
