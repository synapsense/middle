package com.synapsense.plugin.transport.avocent.asset.responses;

import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPFault;
import javax.xml.soap.SOAPMessage;
import javax.xml.xpath.XPathConstants;

import org.apache.log4j.Logger;
import org.w3c.dom.Node;

import com.synapsense.plugin.transport.avocent.asset.types.Asset;
import com.synapsense.plugin.transport.avocent.common.Response;
import com.synapsense.plugin.transport.avocent.utils.XmlHelper;

/**
 * 
 * @author Alexander Kravin
 * 
 */
public class GetAssetResponse implements Response {
	private static final Logger log = Logger.getLogger(GetAssetResponse.class);
	private Asset asset = null;
	private boolean fault = false;
	private boolean sessionExpired = false;

	@Override
	public void processMessage(SOAPMessage response) {
		try {
			SOAPBody body = response.getSOAPBody();
			SOAPFault fault = body.getFault();
			if (fault == null) {
				Node assetNode = (Node) XmlHelper.evaluate("//sim:Asset", body, XPathConstants.NODE);
				if (asset == null)
					asset = new Asset();
				asset.deserialize(assetNode);
			} else {
				this.fault = true;
				sessionExpired = XmlHelper.isSessionExpired(fault);
			}
		} catch (SOAPException ex) {
			log.warn("Cannot get a body. ", ex);
		}
	}

	@Override
	public boolean isFault() {
		return fault;
	}

	@Override
	public boolean isSessionExpired() {
		return sessionExpired;
	}

	public String toString() {
		return "{asset=" + asset + "}";
	}
}
