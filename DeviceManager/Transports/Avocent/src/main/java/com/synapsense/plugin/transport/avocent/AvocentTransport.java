package com.synapsense.plugin.transport.avocent;

import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.synapsense.plugin.apinew.Transport;
import com.synapsense.plugin.messages.GenericMessage;
import com.synapsense.plugin.messages.WSNSensorDataBuilder.FieldConstants;
import com.synapsense.plugin.transport.avocent.asset.requests.QueryRequest;
import com.synapsense.plugin.transport.avocent.asset.requests.SetAssetRequest;
import com.synapsense.plugin.transport.avocent.asset.responses.QueryResponse;
import com.synapsense.plugin.transport.avocent.asset.responses.SetAssetResponse;
import com.synapsense.plugin.transport.avocent.asset.types.Asset;
import com.synapsense.plugin.transport.avocent.asset.types.Property;
import com.synapsense.plugin.transport.avocent.asset.types.RequestedProperty;
import com.synapsense.plugin.transport.avocent.asset.types.UDP;
import com.synapsense.plugin.transport.avocent.utils.SOAPHelper;
import com.synapsense.plugin.transport.avocent.utils.ServicesMgr;

/**
 * 
 * @author Alexander Kravin
 * 
 */
public class AvocentTransport implements Transport {
	private static final Logger log = Logger.getLogger(AvocentTransport.class);
	private static HashMap<String, String> mapping = new HashMap<String, String>();
	private HashMap<String, Asset> assets = new HashMap<String, Asset>(); // Maps
	                                                                      // SENSOR
	                                                                      // ID
	                                                                      // -->
	                                                                      // Asset

	static {
		mapping.put("0", "Temp - Top");
		mapping.put("1", "Humidity (%)");
		mapping.put("2", "Sensor Battery (V)");
		mapping.put("3", "Temp - Middle");
		mapping.put("4", "Temp - Bottom");
	}

	public static final int Priority = 85;

	public AvocentTransport() {
	}

	@Override
	public void connect() throws IOException {
		RequestedProperty queryProp = new RequestedProperty();
		queryProp.addUdp("Sensor ID");

		QueryRequest queryRequest = new QueryRequest("", queryProp, ServicesMgr.getSessionId());
		QueryResponse queryResponse = (QueryResponse) SOAPHelper.sendRequest(queryRequest,
		        ServicesMgr.getAssetEndpoint());
		if (queryResponse.isFault() == false) {
			for (Asset asset : queryResponse.getAssetList()) {
				for (Property property : asset.getPropertyList()) {
					for (UDP udp : property.getUdpList()) {
						if ("Sensor ID".equalsIgnoreCase(udp.getLabel())) {
							assets.put(udp.getValue(), asset);
							log.debug("Registered new mapping: NodeId='" + udp.getValue() + "'; Asset=" + asset);
						}
					}
				}
			}
		}
	}

	@Override
	public int getPriority() {
		return AvocentTransport.Priority;
	}

	@Override
	public void sendData(String objectID, String propertyName, Serializable value) {
		if (value instanceof GenericMessage) {
			GenericMessage map = (GenericMessage) value;
			String nodeId = map.get(FieldConstants.NODE_ID.name()).toString();
			if (assets.containsKey(nodeId)) {
				String channel = map.get(FieldConstants.SENSOR_ID.name()).toString();
				if (mapping.containsKey(channel)) {

					String sensorName = mapping.get(channel);
					String data = String.format("%.5g%n", map.get(FieldConstants.DATA.name()));

					Asset asset = new Asset(assets.get(nodeId).getAssetId());
					Property property = new Property();
					property.addUdp(new UDP(sensorName, data));
					asset.addProperty(property);

					SetAssetRequest setAssetRequest = new SetAssetRequest(asset, ServicesMgr.getSessionId());
					SetAssetResponse setAssetResponse = (SetAssetResponse) SOAPHelper.sendRequest(setAssetRequest,
					        ServicesMgr.getAssetEndpoint());

					if (setAssetResponse.isFault() && setAssetResponse.isSessionExpired()) {
						ServicesMgr.updateSessionId();
						setAssetRequest = new SetAssetRequest(asset, ServicesMgr.getSessionId());
						setAssetResponse = (SetAssetResponse) SOAPHelper.sendRequest(setAssetRequest,
						        ServicesMgr.getAssetEndpoint());
					}

					if (setAssetResponse.isFault()) {
						log.warn("Cannot send data. See log file.");
					} else {
						log.debug("Asset=" + asset + " was sent successful");
					}
				} else {
					log.warn("Cannot send data. Unknown sensor channel='" + channel + "'");
				}
			} else {
				log.warn("Cannot send data. " + "Cannot find any assets by NodeId='" + nodeId + "'");
			}
		} else {
			log.warn("Unknown value type='" + value.getClass().getName() + "'. {" + GenericMessage.class.getName()
			        + "} is expected.");
		}
	}

	@Override
	public void sendOrderedData(String objectID, String propertyName, Serializable value) {
		sendData(objectID, propertyName, value);
	}
}
