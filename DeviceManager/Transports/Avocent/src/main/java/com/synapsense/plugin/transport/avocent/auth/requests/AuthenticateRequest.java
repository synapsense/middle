package com.synapsense.plugin.transport.avocent.auth.requests;

import javax.xml.namespace.QName;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPBodyElement;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

import org.apache.log4j.Logger;

import com.synapsense.plugin.transport.avocent.common.Request;
import com.synapsense.plugin.transport.avocent.utils.Prefix;
import com.synapsense.plugin.transport.avocent.utils.SOAPHelper;

/**
 * 
 * @author Alexander Kravin
 * 
 */
public class AuthenticateRequest implements Request {
	private static final Logger log = Logger.getLogger(AuthenticateRequest.class);
	private String userName = null;
	private String password = null;
	private String instanceName = null;
	private String nextToken = null;
	private SOAPMessage request = null;

	public AuthenticateRequest(String userName, String password, String instanceName, String nextToken) {
		this.userName = userName;
		this.password = password;
		this.instanceName = instanceName;
		this.nextToken = nextToken;
	}

	private SOAPMessage createMessage() {
		SOAPMessage result = SOAPHelper.createMessage();
		try {
			result.getSOAPHeader().detachNode();

			SOAPBody body = result.getSOAPBody();

			QName bodyName = new QName("http://service.authentication.amp.avocent.com", "authenticate", Prefix.auth);
			SOAPBodyElement bodyElement = body.addBodyElement(bodyName);

			SOAPElement childElement = null;
			if (userName != null) {
				childElement = bodyElement.addChildElement("userName", Prefix.auth);
				childElement.addTextNode(userName);
			}
			if (password != null) {
				childElement = bodyElement.addChildElement("password", Prefix.auth);
				childElement.addTextNode(password);
			}
			if (instanceName != null) {
				childElement = bodyElement.addChildElement("instanceName", Prefix.auth);
				childElement.addTextNode(instanceName);
			}
			if (nextToken != null) {
				childElement = bodyElement.addChildElement("nextToken", Prefix.auth);
				childElement.addTextNode(nextToken);
			}
		} catch (SOAPException e) {
			log.warn("Cannot create a message", e);
		}
		return result;
	}

	@Override
	public SOAPMessage getMessage() {
		if (request == null)
			request = createMessage();
		return request;
	}
}
