package com.synapsense.plugin.transport.avocent.asset.types;

/**
 * 
 * @author Alexander Kravin
 * 
 */
public class Heat extends AbstractData {

	public Heat() {
		super();
	}

	public Heat(String units, float value, Boolean isAggregate) {
		super(units, value, isAggregate);
	}
}
