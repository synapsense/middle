package com.synapsense.plugin.transport.avocent.utils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPConstants;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

import org.apache.log4j.Logger;

import com.synapsense.plugin.transport.avocent.common.MsgFactory;
import com.synapsense.plugin.transport.avocent.common.Request;
import com.synapsense.plugin.transport.avocent.common.Response;

/**
 * 
 * @author Alexander Kravin
 * 
 */
public class SOAPHelper {
	private static final Logger log = Logger.getLogger(SOAPHelper.class);
	private static SOAPHelper instance = new SOAPHelper();
	private String certificate = "cert/avocent.jks";

	private SOAPConnection connection = null;
	private MessageFactory messageFactory = null;

	private SOAPHelper() {
		System.setProperty("javax.net.ssl.trustStore", certificate);
		HttpsURLConnection.setDefaultHostnameVerifier(new NullHostnameVerifier());
		try {
			messageFactory = MessageFactory.newInstance(SOAPConstants.SOAP_1_1_PROTOCOL);
			SOAPConnectionFactory factory = SOAPConnectionFactory.newInstance();
			connection = factory.createConnection();
		} catch (SOAPException e) {
			log.warn(e.getMessage(), e);
		}
	}

	public static SOAPMessage createMessage() {
		SOAPMessage result = null;
		try {
			result = instance.messageFactory.createMessage();
		} catch (SOAPException e) {
			e.printStackTrace();
		}
		return result;
	}

	public static Response sendRequest(Request request, String endpoint) {
		Response result = null;
		try {
			result = MsgFactory.getInstance().createResponse(request.getClass());
			SOAPMessage response = instance.sendMessage(request.getMessage(), endpoint);
			result.processMessage(response);
		} catch (Exception e) {
			log.warn("Cannot process response.", e);
		}
		return result;
	}

	private SOAPMessage sendMessage(SOAPMessage request, String endpoint) {
		OutputStream out = new ByteArrayOutputStream();
		SOAPMessage response = null;
		try {
			request.writeTo(out);
			out.flush();
			log.debug("REQUEST: " + out.toString());

			response = instance.connection.call(request, endpoint);

			out = new ByteArrayOutputStream();
			response.writeTo(out);
			out.flush();
			log.debug("RESPONSE: " + out.toString());
		} catch (IOException e) {
			log.warn("Cannot log message. ", e);
		} catch (SOAPException ex) {
			log.warn("Cannot send message. ", ex);
		}
		return response;
	}

	// public static void disconnect() {
	// try {
	// instance.connection.close();
	// } catch (SOAPException e) {
	// log.warn("Cannot close the connection. ",e);
	// }
	// }

	private static class NullHostnameVerifier implements HostnameVerifier {
		@Override
		public boolean verify(String host, SSLSession session) {
			return true;
		}
	}
}
