package com.synapsense.plugin.transport.avocent.asset.responses;

import java.util.ArrayList;
import java.util.List;

import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPFault;
import javax.xml.soap.SOAPMessage;
import javax.xml.xpath.XPathConstants;

import org.apache.log4j.Logger;
import org.w3c.dom.NodeList;

import com.synapsense.plugin.transport.avocent.asset.types.Asset;
import com.synapsense.plugin.transport.avocent.common.Response;
import com.synapsense.plugin.transport.avocent.utils.XmlHelper;

/**
 * 
 * @author Alexander Kravin
 * 
 */
public class QueryResponse implements Response {
	private static final Logger log = Logger.getLogger(QueryResponse.class);
	private String queryString = null;
	private List<Asset> assetList = new ArrayList<Asset>();
	private boolean fault = false;
	private boolean sessionExpired = false;

	public void addAsset(Asset asset) {
		assetList.add(asset);
	}

	@Override
	public void processMessage(SOAPMessage response) {
		try {
			SOAPBody body = response.getSOAPBody();
			SOAPFault fault = body.getFault();
			if (fault == null) {
				queryString = (String) XmlHelper.evaluate("sim:QueryResponse/sim:QueryString/text()", body,
				        XPathConstants.STRING);

				NodeList assetNodes = (NodeList) XmlHelper.evaluate("sim:QueryResponse/sim:Assets/sim:Asset", body,
				        XPathConstants.NODESET);
				if (assetNodes != null) {
					for (int i = 0; i < assetNodes.getLength(); i++) {
						Asset asset = new Asset();
						asset.deserialize(assetNodes.item(i));
						assetList.add(asset);
					}
				}
			} else {
				this.fault = true;
				sessionExpired = XmlHelper.isSessionExpired(fault);
			}
		} catch (SOAPException ex) {
			log.warn("Cannot get a body. ", ex);
		}
	}

	@Override
	public boolean isFault() {
		return fault;
	}

	public List<Asset> getAssetList() {
		return assetList;
	}

	public String getQueryString() {
		return queryString;
	}

	public String toString() {
		return "{queryString='" + queryString + "'}";
	}

	@Override
	public boolean isSessionExpired() {
		return sessionExpired;
	}
}
