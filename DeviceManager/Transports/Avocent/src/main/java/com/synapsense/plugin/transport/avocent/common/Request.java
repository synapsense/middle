package com.synapsense.plugin.transport.avocent.common;

import javax.xml.soap.SOAPMessage;

/**
 * 
 * @author Alexander Kravin
 * 
 */
public interface Request {
	public SOAPMessage getMessage();
}
