package com.synapsense.plugin.transport.avocent.auth.requests;

import javax.xml.namespace.QName;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

import org.apache.log4j.Logger;

import com.synapsense.plugin.transport.avocent.common.Request;
import com.synapsense.plugin.transport.avocent.utils.Prefix;
import com.synapsense.plugin.transport.avocent.utils.SOAPHelper;

/**
 * 
 * @author Alexander Kravin
 * 
 */
public class GetAllInstancesRequest implements Request {
	private static final Logger log = Logger.getLogger(GetAllInstancesRequest.class);
	private SOAPMessage request = null;

	private SOAPMessage createMessage() {
		SOAPMessage result = SOAPHelper.createMessage();
		try {
			result.getSOAPHeader().detachNode();

			SOAPBody body = result.getSOAPBody();

			QName bodyName = new QName("http://service.authentication.amp.avocent.com", "getAllInstances", Prefix.auth);
			body.addBodyElement(bodyName);

		} catch (SOAPException e) {
			log.warn("Cannot create a message", e);
		}
		return result;
	}

	@Override
	public SOAPMessage getMessage() {
		if (request == null)
			request = createMessage();
		return request;
	}
}
