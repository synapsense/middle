package com.synapsense.plugin.transport.avocent.common;

import javax.xml.namespace.QName;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPHeaderElement;
import javax.xml.soap.SOAPMessage;

import com.synapsense.plugin.transport.avocent.utils.Prefix;

/**
 * 
 * @author Alexander Kravin
 * 
 */
public abstract class BaseRequest implements Request {
	protected String sessionId = null;

	public BaseRequest(String sessionId) {
		this.sessionId = sessionId;
	}

	@Override
	public SOAPMessage getMessage() {
		throw new UnsupportedOperationException();
	}

	protected void addHeader(SOAPMessage message) throws SOAPException {
		SOAPHeader result = message.getSOAPHeader();
		QName headerName = new QName(
		        "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd", "Security",
		        Prefix.wsse);
		SOAPHeaderElement headerElement = result.addHeaderElement(headerName);
		SOAPElement securityElement = headerElement.addChildElement("SecurityContextToken", Prefix.wsse);
		SOAPElement identifierElement = securityElement.addChildElement("Identifier", Prefix.wsse);
		identifierElement.addTextNode(sessionId);
	}
}
