package com.synapsense.plugin.transport.avocent.asset.requests;

import javax.xml.namespace.QName;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPBodyElement;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

import org.apache.log4j.Logger;

import com.synapsense.plugin.transport.avocent.asset.types.Asset;
import com.synapsense.plugin.transport.avocent.common.BaseRequest;
import com.synapsense.plugin.transport.avocent.utils.Prefix;
import com.synapsense.plugin.transport.avocent.utils.SOAPHelper;

/**
 * 
 * @author Alexander Kravin
 * 
 */
public class SetAssetRequest extends BaseRequest {
	private static final Logger log = Logger.getLogger(SetAssetRequest.class);
	private Asset asset = null;
	private SOAPMessage request = null;

	public SetAssetRequest(Asset asset, String sessionId) {
		super(sessionId);
		this.asset = asset;
	}

	private SOAPMessage createMessage() {
		SOAPMessage result = SOAPHelper.createMessage();
		try {
			addHeader(result);
			SOAPBody body = result.getSOAPBody();
			QName bodyName = new QName("http://dvr.avocent.com/simple-asset", "SetAssetRequest", Prefix.asset);
			SOAPBodyElement bodyElement = body.addBodyElement(bodyName);

			asset.serialize(bodyElement);
		} catch (SOAPException e) {
			log.warn("Cannot create a message", e);
		}
		return result;
	}

	@Override
	public SOAPMessage getMessage() {
		if (request == null)
			request = createMessage();
		return request;
	}

	public Asset getAsset() {
		return asset;
	}
}
