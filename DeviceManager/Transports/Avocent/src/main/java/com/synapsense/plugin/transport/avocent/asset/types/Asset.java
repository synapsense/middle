package com.synapsense.plugin.transport.avocent.asset.types;

import java.util.ArrayList;
import java.util.List;

import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPException;
import javax.xml.xpath.XPathConstants;

import org.apache.log4j.Logger;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.synapsense.plugin.transport.avocent.common.ComplexType;
import com.synapsense.plugin.transport.avocent.utils.Prefix;
import com.synapsense.plugin.transport.avocent.utils.XmlHelper;

/**
 * 
 * @author Alexander Kravin
 * 
 */
public class Asset implements ComplexType {
	private static final Logger log = Logger.getLogger(Asset.class);
	private Long assetId = null;
	private List<Property> propertyList = new ArrayList<Property>();

	public Asset() {
	}

	public Asset(long assetId) {
		this.assetId = assetId;
	}

	public Long getAssetId() {
		return assetId;
	}

	public void addProperty(Property property) {
		propertyList.add(property);
	}

	public List<Property> getPropertyList() {
		return propertyList;
	}

	@Override
	public void serialize(SOAPElement element) {
		try {
			SOAPElement id = element.addChildElement("AssetId", Prefix.asset);
			id.addTextNode(assetId.toString());

			for (int i = 0; i < propertyList.size(); i++) {
				SOAPElement tempElement = element.addChildElement("Properties", Prefix.asset);
				propertyList.get(i).serialize(tempElement);
			}
		} catch (SOAPException e) {
			log.warn("Cannot serialize 'Asset': ", e);
		}
	}

	@Override
	public void deserialize(Node element) {
		assetId = ((Double) XmlHelper.evaluate("sim:AssetId/text()", element, XPathConstants.NUMBER)).longValue();
		NodeList properties = (NodeList) XmlHelper.evaluate("sim:Properties", element, XPathConstants.NODESET);
		for (int i = 0; i < properties.getLength(); i++) {
			Property property = new Property();
			property.deserialize(properties.item(i));
			propertyList.add(property);
		}
	}

	public String toString() {
		return "{assetId=" + assetId + "; propertyList=" + propertyList + "}";
	}
}
