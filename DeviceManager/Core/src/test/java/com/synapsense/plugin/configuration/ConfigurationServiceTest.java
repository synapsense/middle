package com.synapsense.plugin.configuration;

import com.synapsense.plugin.api.exceptions.ConfigPluginException;
import com.synapsense.plugin.apinew.PluginConfig;
import java.sql.SQLException;
import java.util.Map;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import static org.junit.Assert.assertEquals;

public class ConfigurationServiceTest {
    @Rule
    public TemporaryFolder folder = new TemporaryFolder();

    private ConfigurationService configurationService;

    private static String validConfig;
    @SuppressWarnings("FieldCanBeLocal")
    private static String invalidConfig;

    @BeforeClass
    public static void beforeClass() {
        StringBuilder sb = new StringBuilder();
        sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?> ");
        sb.append("<DMConfig xmlns=\"http://www.synapsense.com/plugin/apinew\" ");
        sb.append("xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" ");
        sb.append("xsi:schemaLocation=\"http://www.synapsense.com/plugin/apinew/dmPluginConfig.xsd\"> ");
        sb.append("<plugin name=\"bacnet\"> ");
        sb.append("</plugin> ");
        sb.append("</DMConfig>");
        validConfig = sb.toString();

        sb = new StringBuilder();
        sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?> ");
        sb.append("</////////////////////////////////////DMConfig xmlns=\"http://www.synapsense.com/plugin/apinew\" ");
        sb.append("xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" ");
        sb.append("xsi:schemaLocation=\"http://www.synapsense.com/plugin/apinew/dmPluginConfig.xsd\"> ");
        sb.append("<plugin name=\"bacnet\"> ");
        sb.append("</plugin> ");
        sb.append("</DMConfig>");
        invalidConfig = sb.toString();
    }

    @Before
    public void setUp() {
        configurationService = new ConfigurationService(folder.getRoot().toPath());
    }

    @Test
    public void parseConfig() throws ConfigPluginException {

        Map<String, PluginConfig> parsedConfig = configurationService.parseConfig(validConfig);
        assertEquals(1, parsedConfig.keySet().size());
        assertEquals("bacnet", parsedConfig.keySet().iterator().next());
    }

    @Test(expected = ConfigPluginException.class)
    public void parseInvalidConfig() throws ConfigPluginException {
        configurationService.parseConfig(invalidConfig);
    }

    @Test
    public void storeAndLoadConfig() throws SQLException, ConfigPluginException {
        configurationService.storeConfig(validConfig);
        assertEquals(validConfig, configurationService.getStoredConfig());
    }
}
