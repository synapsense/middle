package com.synapsense.plugin.conversion;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.Serializable;

import org.junit.Before;
import org.junit.Test;

import com.synapsense.plugin.apinew.impl.ObjectDescImpl;
import com.synapsense.plugin.apinew.impl.PropertyDescImpl;
import com.synapsense.plugin.apinew.impl.TagDescImpl;
import com.synapsense.plugin.manager.TagIDs;

public class UnitConverterTest {
	ConversionService conversionService;

	@Before
	public void setUp() {
		conversionService = ConversionService.getInstance();
	}

	@Test
	public void empty() {

	}

	public void testConvert() throws ValueConversionException {
		PropertyDescImpl prop = new PropertyDescImpl("a", "");
		prop.addTag(new TagDescImpl(TagIDs.UNIT_CONVERTER_SET, "SI:Imperial US:temperature"));
		ObjectDescImpl obj = new ObjectDescImpl("OBJ:1");
		obj.addProperty(prop);
		prop.setParent(obj);
		Serializable res;

		res = conversionService.convert(prop, "2");
		assertTrue(res instanceof Double);
		assertEquals(res, new Double(35.6));

	}
}
