package com.synapsense.plugin.conversion;

import static org.junit.Assert.assertEquals;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import mockit.Deencapsulation;
import mockit.Invocation;
import mockit.Mock;
import mockit.MockUp;
import mockit.Mocked;

import mockit.integration.junit4.JMockit;
import org.junit.BeforeClass;
import org.junit.Test;

import com.synapsense.plugin.apinew.ObjectDesc;
import com.synapsense.plugin.apinew.Plugin;
import com.synapsense.plugin.configuration.ConfigurationService;
import com.synapsense.plugin.manager.CompositeTransport;
import com.synapsense.plugin.manager.DeviceManagerCore;
import com.synapsense.plugin.poll.PollingService;
import org.junit.runner.RunWith;

@RunWith(JMockit.class)
public class ElConverterTest {
	private static DeviceManagerCore devManager;
	private static Thread dmLauncherThread;
	private static final String SENSOR_ID = "SENSOR:42";
	private static final String WSN_LIKE_TEST_CONFIG = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<DMConfig xmlns=\"http://www.synapsense.com/plugin/apinew\" "
	        + "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" "
	        + "xsi:schemaLocation=\"http://www.synapsense.com/plugin/apinew/dmPluginConfig.xsd\">\n"
	        + "<plugin name=\"simulator\">\n"
	        + "<obj id=\""
	        + SENSOR_ID
	        + "\">\n"
	        + "<prop name=\"expression\" value=\"55.240088574 + Sin[TIME/1000000]\"/>\n"

	        + "<prop name=\"firstDelay\" value=\"400\"/>\n"
	        + "<prop name=\"rollover\" value=\"30000000\"/>\n"
	        + "<prop name=\"sleepDelay\" value=\"410400\"/>\n"
	        + "<prop name=\"type\" value=\"double\"/>\n"
	        + "<prop name=\"property\" value=\"lastValue\"/>\n"
	        + "<prop name=\"lastValue\" value=\"400\">\n"
	        + "<tag name=\"elConvGet\" value=\"%1$s\" />\n" + "</prop>\n" + "</obj>\n" + "</plugin>\n" + "</DMConfig>";

	@Mocked
	private static Plugin simPlugin;
	static {
		simPlugin = new MockUp<Plugin>() {

			@Mock
			Serializable getPropertyValue(ObjectDesc object, String propertyName) {
				return Integer.valueOf(400);
			}

		}.getMockInstance();
	}

	@BeforeClass
	public static void setUp() throws Exception {
		// mock DeviceManagerCore plugin initialization
		new MockUp<DeviceManagerCore>() {
			@Mock
			private void loadPlugins(Invocation invocation) {
				Map<String, Plugin> plugins = new HashMap<String, Plugin>();
				plugins.put("simulator", simPlugin);
				Deencapsulation.setField(invocation.<Object>getInvokedInstance(), "plugins", plugins);
			}
		};
        new MockUp<ConfigurationService>() {
            @Mock
            public void storeConfig(String config) {
               //no need to store configs here
            }
        };
		// launching Device Manager in it's own thread
		dmLauncherThread = new Thread(new Runnable() {

			@Override
			public void run() {
				devManager = DeviceManagerCore.getInstance();
				PollingService pollingService = new PollingService(devManager);
				devManager.setConfigurationService(new ConfigurationService());
				devManager.setConversionService(ConversionService.getInstance());
				devManager.setPollingService(pollingService);
				CompositeTransport transport = new CompositeTransport();
				devManager.setTransport(transport);
			}
		});
		dmLauncherThread.setDaemon(true);
		dmLauncherThread.start();
		// wait for the DM to start
		dmLauncherThread.join();
		// configure DM with the test configuration

	}

	@Test
	public void testConvertScale() {
		devManager.configurePlugins(String.format(WSN_LIKE_TEST_CONFIG, "(Integer)(value/2)"));
		assertEquals(200, devManager.getPropertyValue(SENSOR_ID, "lastValue"));
	}

	@Test
	public void testConvertScaleAsDouble() {
		devManager.configurePlugins(String.format(WSN_LIKE_TEST_CONFIG, "(Double)(value/2)"));

		assertEquals(200.0, devManager.getPropertyValue(SENSOR_ID, "lastValue"));
	}

	@Test
	public void testConvertPropertyAccess() {
		devManager.configurePlugins(String.format(WSN_LIKE_TEST_CONFIG,
		        "'Name: ' + prop.getName() + ' Value: ' + (Integer)value"));

		assertEquals("Name: lastValue Value: 400", devManager.getPropertyValue(SENSOR_ID, "lastValue"));
	}

	@Test
	public void testConvertPropertyScale() {
		devManager
		        .configurePlugins(String.format(WSN_LIKE_TEST_CONFIG, "parentObj.getPropertyValue('rollover')/value"));

		assertEquals(75000, devManager.getPropertyValue(SENSOR_ID, "lastValue"));
	}
}
