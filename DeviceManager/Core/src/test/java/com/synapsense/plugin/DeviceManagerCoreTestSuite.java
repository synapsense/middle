package com.synapsense.plugin;

import com.synapsense.plugin.conversion.UnitConverterTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.synapsense.plugin.configuration.DMConfigReaderTest;
import com.synapsense.plugin.configuration.ObjectStoreTest;
import com.synapsense.plugin.configuration.PluginConfigImplTest;
import com.synapsense.plugin.conversion.ElConverterTest;
import com.synapsense.plugin.conversion.UnitConverter;

@RunWith(Suite.class)
@SuiteClasses({ ObjectStoreTest.class, DMConfigReaderTest.class, PluginConfigImplTest.class, ElConverterTest.class,
        UnitConverterTest.class, })
public class DeviceManagerCoreTestSuite {
}
