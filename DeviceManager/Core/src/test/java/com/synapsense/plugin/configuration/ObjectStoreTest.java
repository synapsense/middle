package com.synapsense.plugin.configuration;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import com.synapsense.plugin.apinew.ObjectDesc;
import com.synapsense.plugin.apinew.PropertyDesc;
import com.synapsense.plugin.apinew.TagDesc;
import com.synapsense.plugin.apinew.impl.ObjectDescImpl;
import com.synapsense.plugin.apinew.impl.PropertyDescImpl;
import com.synapsense.plugin.apinew.impl.TagDescImpl;
import com.synapsense.plugin.manager.ObjectsStore;

public class ObjectStoreTest {
	ObjectsStore objectStore;

	@Before
	public void beforeTest() {
		objectStore = new ObjectsStore();
		Map<String, ObjectDesc> newObjects = new HashMap<String, ObjectDesc>();

		ObjectDesc modBusNetwork = new ObjectDescImpl("MODBUSNETWORK:2357");
		PropertyDesc port = new PropertyDescImpl("port", "TCP:192.168.1.1");
		PropertyDesc parity = new PropertyDescImpl("Parity", "2");
		modBusNetwork.addProperty(port);
		modBusNetwork.addProperty(parity);

		ObjectDesc modBusDevice = new ObjectDescImpl("MODBUSDEVICE:2358");
		PropertyDesc maxpacketlen = new PropertyDescImpl("maxpacketlen", "42");
		PropertyDesc name = new PropertyDescImpl("name", "ION 6200 Delta");
		modBusDevice.addProperty(maxpacketlen);
		modBusDevice.addProperty(name);

		ObjectDesc modBusProperty2348 = new ObjectDescImpl("MODBUSPROPERTY:2348");
		PropertyDesc scale2348 = new PropertyDescImpl("scale", "10.0");
		PropertyDesc name2348 = new PropertyDescImpl("name", "Voltage B-C");
		PropertyDescImpl lastValue2348 = new PropertyDescImpl("lastValue", "");
		TagDesc poll2348 = new TagDescImpl("poll", "300000");
		lastValue2348.addTag(poll2348);
		modBusProperty2348.addProperty(lastValue2348);
		modBusProperty2348.addProperty(scale2348);
		modBusProperty2348.addProperty(name2348);

		ObjectDesc modBusProperty2347 = new ObjectDescImpl("MODBUSPROPERTY:2347");
		PropertyDesc scale2347 = new PropertyDescImpl("scale", "10.0");
		PropertyDesc name2347 = new PropertyDescImpl("name", "Vb-c THD");
		modBusProperty2347.addProperty(scale2347);
		modBusProperty2347.addProperty(name2347);

		modBusDevice.addChild(modBusProperty2347);
		modBusDevice.addChild(modBusProperty2348);
		modBusNetwork.addChild(modBusDevice);
		newObjects.put("2357", modBusNetwork);
		newObjects.put("2358", modBusDevice);
		newObjects.put("2348", modBusProperty2348);
		newObjects.put("2347", modBusProperty2347);
		String pluginID = "modbus";
		objectStore.addObjects(newObjects, pluginID);

	}

	@Test
	public void testGetPluginID() {
		String objectID = "2358";// input value
		assertEquals("modbus", objectStore.getPluginID(objectID));
		assertEquals(null, objectStore.getPluginID("2333"));
	}

	@Test
	public void testGetObjectString() {
		String objectID = "2357";// input value
		ObjectDesc object = objectStore.getObject(objectID);
		assertEquals("MODBUSNETWORK:2357", object.toString());
	}

	@Test
	public void testGetObjectStringString() {
		String objectId = "2348";// input value
		String pluginId = "modbus";// input value
		ObjectDesc object = objectStore.getObject(objectId, pluginId);
		assertEquals("MODBUSPROPERTY:2348", object.toString());
		assertEquals(null, objectStore.getObject("2435", "modbus"));

	}

	@Test
	public void testGetObjects() {
		String pluginId = "modbus";// input value
		Map<String, ObjectDesc> gettingObjects = objectStore.getObjects(pluginId);
		assertEquals("MODBUSNETWORK:2357", gettingObjects.get("2357").toString());
		assertEquals("MODBUSDEVICE:2358", gettingObjects.get("2358").toString());
		assertEquals("MODBUSPROPERTY:2348", gettingObjects.get("2348").toString());
		assertEquals("MODBUSPROPERTY:2347", gettingObjects.get("2347").toString());
	}

	@Test
	public void testMergeObjectsMergeProperties() {
		// add properties for MODBUSDEVICE:2358 and for MODBUSPROPERTY:2348
		Map<String, ObjectDesc> newObjects = new HashMap<String, ObjectDesc>();

		ObjectDesc modBusNetwork = new ObjectDescImpl("MODBUSNETWORK:2357");
		PropertyDesc port = new PropertyDescImpl("port", "TCP:192.168.1.1");
		PropertyDesc parity = new PropertyDescImpl("Parity", "2");
		modBusNetwork.addProperty(port);
		modBusNetwork.addProperty(parity);

		ObjectDesc modBusDevice = new ObjectDescImpl("MODBUSDEVICE:2358");
		PropertyDesc maxpacketlen = new PropertyDescImpl("maxpacketlen", "42");
		PropertyDesc name = new PropertyDescImpl("name", "ION 6200 Delta");
		PropertyDesc numAlerts = new PropertyDescImpl("numAlerts", "0");
		modBusDevice.addProperty(numAlerts);
		modBusDevice.addProperty(maxpacketlen);
		modBusDevice.addProperty(name);

		ObjectDesc modBusProperty2348 = new ObjectDescImpl("MODBUSPROPERTY:2348");
		PropertyDesc scale2348 = new PropertyDescImpl("scale", "10.0");
		PropertyDesc name2348 = new PropertyDescImpl("name", "Voltage B-C");
		PropertyDesc type2348 = new PropertyDescImpl("type", "UINT 16");
		modBusProperty2348.addProperty(type2348);
		modBusProperty2348.addProperty(scale2348);
		modBusProperty2348.addProperty(name2348);

		modBusDevice.addChild(modBusProperty2348);
		modBusNetwork.addChild(modBusDevice);
		newObjects.put("2357", modBusNetwork);
		newObjects.put("2358", modBusDevice);
		newObjects.put("2348", modBusProperty2348);

		objectStore.mergeObjects(newObjects, "modbus");
		assertEquals("0", objectStore.getObject("2358").getPropertyValue("numAlerts"));
		assertEquals("ION 6200 Delta", objectStore.getObject("2358").getPropertyValue("name"));
		assertEquals("UINT 16", objectStore.getObject("2348").getPropertyValue("type"));
		assertEquals("10.0", objectStore.getObject("2348").getPropertyValue("scale"));

	}

	@Test
	public void testMergeObjectsDeleteObject() {
		// Map doesn't have MODBUSPROPETY:2347, but storage does
		assertNotNull(objectStore.getObject("2347"));
		Map<String, ObjectDesc> newObjects = new HashMap<String, ObjectDesc>();

		ObjectDesc modBusNetwork = new ObjectDescImpl("MODBUSNETWORK:2357");
		PropertyDesc port = new PropertyDescImpl("port", "TCP:192.168.1.1");
		PropertyDesc parity = new PropertyDescImpl("Parity", "2");
		modBusNetwork.addProperty(port);
		modBusNetwork.addProperty(parity);

		ObjectDesc modBusDevice = new ObjectDescImpl("MODBUSDEVICE:2358");
		PropertyDesc maxpacketlen = new PropertyDescImpl("maxpacketlen", "42");
		PropertyDesc name = new PropertyDescImpl("name", "ION 6200 Delta");
		modBusDevice.addProperty(maxpacketlen);
		modBusDevice.addProperty(name);

		ObjectDesc modBusProperty2348 = new ObjectDescImpl("MODBUSPROPERTY:2348");
		PropertyDesc scale2348 = new PropertyDescImpl("scale", "10.0");
		PropertyDesc name2348 = new PropertyDescImpl("name", "Voltage B-C");
		modBusProperty2348.addProperty(scale2348);
		modBusProperty2348.addProperty(name2348);

		modBusDevice.addChild(modBusProperty2348);
		modBusNetwork.addChild(modBusDevice);
		newObjects.put("2357", modBusNetwork);
		newObjects.put("2358", modBusDevice);
		newObjects.put("2348", modBusProperty2348);

		objectStore.mergeObjects(newObjects, "modbus");
		assertNull(objectStore.getObject("2347"));

	}

	@Test
	public void testMergeObjectsAddObject() {
		// Maps have object MODBUSPROPERTY:2348, child of MODBUSDEVICE:2358.
		// Storage haven't it
		Map<String, ObjectDesc> newObjects = new HashMap<String, ObjectDesc>();

		ObjectDesc modBusNetwork = new ObjectDescImpl("MODBUSNETWORK:2357");
		PropertyDesc port = new PropertyDescImpl("port", "TCP:192.168.1.1");
		PropertyDesc parity = new PropertyDescImpl("Parity", "2");
		modBusNetwork.addProperty(port);
		modBusNetwork.addProperty(parity);

		ObjectDesc modBusDevice = new ObjectDescImpl("MODBUSDEVICE:2358");
		PropertyDesc maxpacketlen = new PropertyDescImpl("maxpacketlen", "42");
		PropertyDesc name = new PropertyDescImpl("name", "ION 6200 Delta");
		modBusDevice.addProperty(maxpacketlen);
		modBusDevice.addProperty(name);

		ObjectDesc modBusProperty2349 = new ObjectDescImpl("MODBUSPROPERTY:2349");
		PropertyDesc name2349 = new PropertyDescImpl("name", "kVar Demand");
		PropertyDesc type2349 = new PropertyDescImpl("type", "INT 16");
		modBusProperty2349.addProperty(name2349);
		modBusProperty2349.addProperty(type2349);

		ObjectDesc modBusProperty2348 = new ObjectDescImpl("MODBUSPROPERTY:2348");
		PropertyDesc scale2348 = new PropertyDescImpl("scale", "10.0");
		PropertyDesc name2348 = new PropertyDescImpl("name", "Voltage B-C");
		modBusProperty2348.addProperty(scale2348);
		modBusProperty2348.addProperty(name2348);

		ObjectDesc modBusProperty2347 = new ObjectDescImpl("MODBUSPROPERTY:2347");
		PropertyDesc scale2347 = new PropertyDescImpl("scale", "10.0");
		PropertyDesc name2347 = new PropertyDescImpl("name", "Vb-c THD");
		modBusProperty2347.addProperty(scale2347);
		modBusProperty2347.addProperty(name2347);

		modBusDevice.addChild(modBusProperty2347);
		modBusDevice.addChild(modBusProperty2348);
		modBusDevice.addChild(modBusProperty2349);
		modBusNetwork.addChild(modBusDevice);
		newObjects.put("2357", modBusNetwork);
		newObjects.put("2358", modBusDevice);
		newObjects.put("2348", modBusProperty2348);
		newObjects.put("2347", modBusProperty2347);
		newObjects.put("2349", modBusProperty2349);

		objectStore.mergeObjects(newObjects, "modbus");
		assertNotNull(objectStore.getObject("2349"));
		assertEquals("kVar Demand", objectStore.getObject("2349").getPropertyValue("name"));
		assertEquals("INT 16", objectStore.getObject("2349").getPropertyValue("type"));

	}

	@Test
	public void testMergeObjectsWithEmptyObjects() {
		Map<String, ObjectDesc> newObjects = new HashMap<String, ObjectDesc>();
		objectStore.mergeObjects(newObjects, "modbus");
		assertEquals(0, objectStore.getObjects("modbus").size());
	}

	@Test
	public void testMergeEmptyObjectsStore() {
		ObjectsStore objectForMerge = new ObjectsStore();

		Map<String, ObjectDesc> newObjects = new HashMap<String, ObjectDesc>();

		ObjectDesc modBusNetwork = new ObjectDescImpl("MODBUSNETWORK:2357");
		PropertyDesc port = new PropertyDescImpl("port", "TCP:192.168.1.1");
		PropertyDesc parity = new PropertyDescImpl("Parity", "2");
		modBusNetwork.addProperty(port);
		modBusNetwork.addProperty(parity);

		ObjectDesc modBusDevice = new ObjectDescImpl("MODBUSDEVICE:2358");
		PropertyDesc maxpacketlen = new PropertyDescImpl("maxpacketlen", "42");
		PropertyDesc name = new PropertyDescImpl("name", "ION 6200 Delta");
		modBusDevice.addProperty(maxpacketlen);
		modBusDevice.addProperty(name);

		ObjectDesc modBusProperty2348 = new ObjectDescImpl("MODBUSPROPERTY:2348");
		PropertyDesc scale2348 = new PropertyDescImpl("scale", "10.0");
		PropertyDesc name2348 = new PropertyDescImpl("name", "Voltage B-C");
		modBusProperty2348.addProperty(scale2348);
		modBusProperty2348.addProperty(name2348);

		ObjectDesc modBusProperty2347 = new ObjectDescImpl("MODBUSPROPERTY:2347");
		PropertyDesc scale2347 = new PropertyDescImpl("scale", "10.0");
		PropertyDesc name2347 = new PropertyDescImpl("name", "Vb-c THD");
		modBusProperty2347.addProperty(scale2347);
		modBusProperty2347.addProperty(name2347);

		modBusDevice.addChild(modBusProperty2347);
		modBusDevice.addChild(modBusProperty2348);
		modBusNetwork.addChild(modBusDevice);
		newObjects.put("2357", modBusNetwork);
		newObjects.put("2358", modBusDevice);
		newObjects.put("2348", modBusProperty2348);
		newObjects.put("2347", modBusProperty2347);

		objectForMerge.mergeObjects(newObjects, "modbus");
		assertEquals("MODBUSNETWORK:2357", objectForMerge.getObject("2357").toString());
		assertEquals("TCP:192.168.1.1", objectForMerge.getObject("2357").getPropertyValue("port"));
		assertEquals("2", objectForMerge.getObject("2357").getPropertyValue("Parity"));

		assertEquals("MODBUSDEVICE:2358", objectForMerge.getObject("2358").toString());
		assertEquals("42", objectForMerge.getObject("2358").getPropertyValue("maxpacketlen"));
		assertEquals("ION 6200 Delta", objectForMerge.getObject("2358").getPropertyValue("name"));

		assertEquals("MODBUSPROPERTY:2348", objectForMerge.getObject("2348").toString());
		assertEquals("Voltage B-C", objectForMerge.getObject("2348").getPropertyValue("name"));
		assertEquals("10.0", objectForMerge.getObject("2348").getPropertyValue("scale"));

		assertEquals("MODBUSPROPERTY:2347", objectForMerge.getObject("2347").toString());
		assertEquals("Vb-c THD", objectForMerge.getObject("2347").getPropertyValue("name"));
		assertEquals("10.0", objectForMerge.getObject("2347").getPropertyValue("scale"));
	}

	@Test
	public void testMergeWithObjectWhichHaveAnotherValueOfProperties() {
		// Property "Parity" for MODBUSNETWORK have another value
		// Property "name" for MODBUSDEVICE have another value
		// Property "name" for MODBUSPROPERTY:2348 have another value
		Map<String, ObjectDesc> newObjects = new HashMap<String, ObjectDesc>();

		ObjectDesc modBusNetwork = new ObjectDescImpl("MODBUSNETWORK:2357");
		PropertyDesc port = new PropertyDescImpl("port", "TCP:192.168.1.1");
		PropertyDesc parity = new PropertyDescImpl("Parity", "3");
		modBusNetwork.addProperty(port);
		modBusNetwork.addProperty(parity);

		ObjectDesc modBusDevice = new ObjectDescImpl("MODBUSDEVICE:2358");
		PropertyDesc maxpacketlen = new PropertyDescImpl("maxpacketlen", "42");
		PropertyDesc name = new PropertyDescImpl("name", "ION 6200 Alpha");
		modBusDevice.addProperty(maxpacketlen);
		modBusDevice.addProperty(name);

		ObjectDesc modBusProperty2348 = new ObjectDescImpl("MODBUSPROPERTY:2348");
		PropertyDesc scale2348 = new PropertyDescImpl("scale", "10.0");
		PropertyDesc name2348 = new PropertyDescImpl("name", "Voltage A-B-C");
		modBusProperty2348.addProperty(scale2348);
		modBusProperty2348.addProperty(name2348);

		ObjectDesc modBusProperty2347 = new ObjectDescImpl("MODBUSPROPERTY:2347");
		PropertyDesc scale2347 = new PropertyDescImpl("scale", "10.0");
		PropertyDesc name2347 = new PropertyDescImpl("name", "Vb-c THD");
		modBusProperty2347.addProperty(scale2347);
		modBusProperty2347.addProperty(name2347);

		modBusDevice.addChild(modBusProperty2347);
		modBusDevice.addChild(modBusProperty2348);
		modBusNetwork.addChild(modBusDevice);
		newObjects.put("2357", modBusNetwork);
		newObjects.put("2358", modBusDevice);
		newObjects.put("2348", modBusProperty2348);
		newObjects.put("2347", modBusProperty2347);

		objectStore.mergeObjects(newObjects, "modbus");
		assertEquals("3", objectStore.getObject("2357").getPropertyValue("Parity"));
		assertEquals("ION 6200 Alpha", objectStore.getObject("2358").getPropertyValue("name"));
		assertEquals("Voltage A-B-C", objectStore.getObject("2348").getPropertyValue("name"));
	}

	@Test
	public void testMergeAddTag() {
		// Map have tag, which haven't storage
		Map<String, ObjectDesc> newObjects = new HashMap<String, ObjectDesc>();

		ObjectDesc modBusNetwork = new ObjectDescImpl("MODBUSNETWORK:2357");
		PropertyDesc port = new PropertyDescImpl("port", "TCP:192.168.1.1");
		PropertyDesc parity = new PropertyDescImpl("Parity", "2");
		modBusNetwork.addProperty(port);
		modBusNetwork.addProperty(parity);

		ObjectDesc modBusDevice = new ObjectDescImpl("MODBUSDEVICE:2358");
		PropertyDesc maxpacketlen = new PropertyDescImpl("maxpacketlen", "42");
		PropertyDesc name = new PropertyDescImpl("name", "ION 6200 Delta");
		modBusDevice.addProperty(maxpacketlen);
		modBusDevice.addProperty(name);

		ObjectDesc modBusProperty2348 = new ObjectDescImpl("MODBUSPROPERTY:2348");
		PropertyDesc scale2348 = new PropertyDescImpl("scale", "10.0");
		PropertyDesc name2348 = new PropertyDescImpl("name", "Voltage B-C");
		PropertyDescImpl lastValue2348 = new PropertyDescImpl("lastValue", "");
		TagDesc poll2348 = new TagDescImpl("poll", "300000");
		TagDesc elConvGet2348 = new TagDescImpl("elConvGet",
		        "return (Double) (value / parentObj.getPropertyValue(&apos;scale&apos;))");
		lastValue2348.addTag(poll2348);
		lastValue2348.addTag(elConvGet2348);
		modBusProperty2348.addProperty(lastValue2348);
		modBusProperty2348.addProperty(scale2348);
		modBusProperty2348.addProperty(name2348);

		ObjectDesc modBusProperty2347 = new ObjectDescImpl("MODBUSPROPERTY:2347");
		PropertyDesc scale2347 = new PropertyDescImpl("scale", "10.0");
		PropertyDesc name2347 = new PropertyDescImpl("name", "Vb-c THD");
		modBusProperty2347.addProperty(scale2347);
		modBusProperty2347.addProperty(name2347);

		modBusDevice.addChild(modBusProperty2347);
		modBusDevice.addChild(modBusProperty2348);
		modBusNetwork.addChild(modBusDevice);
		newObjects.put("2357", modBusNetwork);
		newObjects.put("2358", modBusDevice);
		newObjects.put("2348", modBusProperty2348);
		newObjects.put("2347", modBusProperty2347);

		objectStore.mergeObjects(newObjects, "modbus");

		assertEquals("poll:300000", objectStore.getObject("2348").getProperty("lastValue").getTag("poll").toString());
		assertEquals("elConvGet:return (Double) (value / parentObj.getPropertyValue(&apos;scale&apos;))", objectStore
		        .getObject("2348").getProperty("lastValue").getTag("elConvGet").toString());
	}

	@Test
	public void testMergeChangeTag() {
		Map<String, ObjectDesc> newObjects = new HashMap<String, ObjectDesc>();

		ObjectDesc modBusNetwork = new ObjectDescImpl("MODBUSNETWORK:2357");
		PropertyDesc port = new PropertyDescImpl("port", "TCP:192.168.1.1");
		PropertyDesc parity = new PropertyDescImpl("Parity", "2");
		modBusNetwork.addProperty(port);
		modBusNetwork.addProperty(parity);

		ObjectDesc modBusDevice = new ObjectDescImpl("MODBUSDEVICE:2358");
		PropertyDesc maxpacketlen = new PropertyDescImpl("maxpacketlen", "42");
		PropertyDesc name = new PropertyDescImpl("name", "ION 6200 Delta");
		modBusDevice.addProperty(maxpacketlen);
		modBusDevice.addProperty(name);

		ObjectDesc modBusProperty2348 = new ObjectDescImpl("MODBUSPROPERTY:2348");
		PropertyDesc scale2348 = new PropertyDescImpl("scale", "10.0");
		PropertyDesc name2348 = new PropertyDescImpl("name", "Voltage B-C");
		PropertyDescImpl lastValue2348 = new PropertyDescImpl("lastValue", "");
		TagDesc poll2348 = new TagDescImpl("poll", "200000");
		lastValue2348.addTag(poll2348);
		modBusProperty2348.addProperty(lastValue2348);
		modBusProperty2348.addProperty(scale2348);
		modBusProperty2348.addProperty(name2348);

		ObjectDesc modBusProperty2347 = new ObjectDescImpl("MODBUSPROPERTY:2347");
		PropertyDesc scale2347 = new PropertyDescImpl("scale", "10.0");
		PropertyDesc name2347 = new PropertyDescImpl("name", "Vb-c THD");
		modBusProperty2347.addProperty(scale2347);
		modBusProperty2347.addProperty(name2347);

		modBusDevice.addChild(modBusProperty2347);
		modBusDevice.addChild(modBusProperty2348);
		modBusNetwork.addChild(modBusDevice);
		newObjects.put("2357", modBusNetwork);
		newObjects.put("2358", modBusDevice);
		newObjects.put("2348", modBusProperty2348);
		newObjects.put("2347", modBusProperty2347);

		objectStore.mergeObjects(newObjects, "modbus");
		assertEquals("poll:200000", objectStore.getObject("2348").getProperty("lastValue").getTag("poll").toString());
	}

	@Test
	public void testMergeDeleteTag() {
		Map<String, ObjectDesc> newObjects = new HashMap<String, ObjectDesc>();

		ObjectDesc modBusNetwork = new ObjectDescImpl("MODBUSNETWORK:2357");
		PropertyDesc port = new PropertyDescImpl("port", "TCP:192.168.1.1");
		PropertyDesc parity = new PropertyDescImpl("Parity", "2");
		modBusNetwork.addProperty(port);
		modBusNetwork.addProperty(parity);

		ObjectDesc modBusDevice = new ObjectDescImpl("MODBUSDEVICE:2358");
		PropertyDesc maxpacketlen = new PropertyDescImpl("maxpacketlen", "42");
		PropertyDesc name = new PropertyDescImpl("name", "ION 6200 Delta");
		modBusDevice.addProperty(maxpacketlen);
		modBusDevice.addProperty(name);

		ObjectDesc modBusProperty2348 = new ObjectDescImpl("MODBUSPROPERTY:2348");
		PropertyDesc scale2348 = new PropertyDescImpl("scale", "10.0");
		PropertyDesc name2348 = new PropertyDescImpl("name", "Voltage B-C");
		PropertyDescImpl lastValue2348 = new PropertyDescImpl("lastValue", "");
		TagDesc elConvGet2348 = new TagDescImpl("elConvGet",
		        "return (Double) (value / parentObj.getPropertyValue(&apos;scale&apos;))");
		lastValue2348.addTag(elConvGet2348);
		modBusProperty2348.addProperty(lastValue2348);
		modBusProperty2348.addProperty(scale2348);
		modBusProperty2348.addProperty(name2348);

		ObjectDesc modBusProperty2347 = new ObjectDescImpl("MODBUSPROPERTY:2347");
		PropertyDesc scale2347 = new PropertyDescImpl("scale", "10.0");
		PropertyDesc name2347 = new PropertyDescImpl("name", "Vb-c THD");
		modBusProperty2347.addProperty(scale2347);
		modBusProperty2347.addProperty(name2347);

		modBusDevice.addChild(modBusProperty2347);
		modBusDevice.addChild(modBusProperty2348);
		modBusNetwork.addChild(modBusDevice);
		newObjects.put("2357", modBusNetwork);
		newObjects.put("2358", modBusDevice);
		newObjects.put("2348", modBusProperty2348);
		newObjects.put("2347", modBusProperty2347);

		objectStore.mergeObjects(newObjects, "modbus");
		assertNull(objectStore.getObject("2348").getProperty("lastValue").getTag("poll"));
		assertEquals("elConvGet:return (Double) (value / parentObj.getPropertyValue(&apos;scale&apos;))", objectStore
		        .getObject("2348").getProperty("lastValue").getTag("elConvGet").toString());
	}
}
