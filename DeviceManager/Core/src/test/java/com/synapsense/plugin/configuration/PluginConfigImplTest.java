package com.synapsense.plugin.configuration;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.Map;

import mockit.Mock;
import mockit.MockUp;

import org.junit.Before;
import org.junit.Test;

import com.synapsense.plugin.apinew.ObjectDesc;
import com.synapsense.plugin.apinew.PropertyDesc;
import com.synapsense.plugin.apinew.impl.PropertyDescImpl;

public class PluginConfigImplTest {

	PluginConfigImpl impl = new PluginConfigImpl("plugin_name");
	ObjectDesc objectDesc;

	@Before
	public void before() {
		objectDesc = new MockUp<ObjectDesc>() {
			@Mock
			String getID() {
				return "id";
			}

		}.getMockInstance();
	}

	@Test
	public void testGetPluginName() {
		assertEquals("plugin_name", impl.getPluginName());
	}

	@Test
	public void testAddObject() {
		assertNull(impl.getObjects().get("id"));
		assertNull(impl.addObject(null));
		assertEquals(0, impl.getObjects().size());
		impl.addObject(objectDesc);
		assertEquals(objectDesc, impl.getObjects().get("id"));
	}

	@Test
	public void testAddRoots() {
		assertFalse(impl.getRoots().contains(objectDesc));
		impl.addRoot(null);
		assertEquals(1, impl.getRoots().size());
		impl.addRoot(objectDesc);
		assertTrue(impl.getRoots().contains(objectDesc));
	}

	@Test
	public void testAddProperty() {
		PropertyDesc propertyDesc = new PropertyDescImpl("name", "value");
		assertNull(impl.getPluginProperties().get("name"));
		assertNull(impl.addPropery(null));
		assertEquals(0, impl.getPluginProperties().size());
		impl.addPropery(propertyDesc);
		assertEquals(propertyDesc, impl.getPluginProperties().get("name"));
		assertEquals(propertyDesc, impl.getPluginProperty("name"));
	}

	@Test
	public void testGetSetObjects() {
		Map<String, ObjectDesc> objects = new HashMap<String, ObjectDesc>();
		impl.setObjects(objects);
		assertEquals(objects, impl.getObjects());
		assertNull(impl.getObject("id"));
	}
}
