package com.synapsense.plugin.configuration;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Map;

import org.junit.Test;

import com.synapsense.plugin.api.exceptions.ConfigPluginException;
import com.synapsense.plugin.apinew.PluginConfig;

/**
 * @author stikhon
 * 
 */
public class DMConfigReaderTest {
	@Test
	public void testLoadConfig() throws ConfigPluginException {
		String str = "";
		try {
			str = URLDecoder.decode(DMConfigReaderTest.class.getProtectionDomain().getCodeSource().getLocation()
			        .getPath(), "UTF-8");
		} catch (UnsupportedEncodingException e) {
		}

		String sep = System.getProperty("file.separator");
		java.io.File srcfile = new java.io.File(str + sep + "DMConfig.xml");
		Map<String, PluginConfig> plObjs = null;

		plObjs = DmXmlConfigReader.loadPluginsConfig(srcfile);

		assertEquals(6, plObjs.size());
		assertTrue(plObjs.containsKey("DBPlugin"));
		assertTrue(plObjs.containsKey("WSNPlugin"));

		PluginConfig dbPlugConf = plObjs.get("DBPlugin");

		assertNotNull(dbPlugConf.getObject("DB:1"));
		assertNotNull(dbPlugConf.getObject("TABLE:1"));
		assertNotNull(dbPlugConf.getObject("TABLE:1").getProperty("condition"));
		assertEquals("<ORDER BY id DESC limit 1>", dbPlugConf.getObject("TABLE:1").getProperty("condition").getValue());
		assertNotNull(dbPlugConf.getObject("FIELD:1"));
		assertNotNull(dbPlugConf.getObject("FIELD:1").getPropertyValue("name"));
		assertEquals("fiel&d1 number \"", dbPlugConf.getObject("FIELD:1").getPropertyValue("name"));
		assertNotNull(dbPlugConf.getObject("FIELD:2"));
		assertNotNull(dbPlugConf.getObject("FIELD:2").getProperty("lastValue"));
		assertNotNull(dbPlugConf.getObject("FIELD:2").getProperty("lastValue").getTag("poll"));
		assertEquals("100'00", dbPlugConf.getObject("FIELD:2").getProperty("lastValue").getTag("poll").getValue());
		assertNotNull(dbPlugConf.getObject("FIELD:3"));
		assertEquals(1, dbPlugConf.getRoots().size());
		assertEquals("DB:1", dbPlugConf.getRoots().iterator().next().getID());

		PluginConfig wsnPlugConf = plObjs.get("WSNPlugin");
		assertNotNull(wsnPlugConf.getObject("NETWORK:1"));
		assertNotNull(wsnPlugConf.getObject("NODE:1"));
		assertNotNull(wsnPlugConf.getObject("SENSOR:1"));
		assertNotNull(wsnPlugConf.getObject("NODE:2"));
		assertNotNull(wsnPlugConf.getObject("SENSOR:1"));
		assertNotNull(wsnPlugConf.getObject("GATEWAY:1"));
		assertNotNull(wsnPlugConf.getObject("GATEWAY:2"));

		assertEquals(1, wsnPlugConf.getRoots().size());
		assertEquals("NETWORK:1", wsnPlugConf.getRoots().iterator().next().getID());

		PluginConfig bacNetPlugConf = plObjs.get("bnPlugin");
		assertNotNull(bacNetPlugConf.getObject("BACNETPROPERTY:31"));
		assertNotNull(bacNetPlugConf.getObject("BACNETOBJECT:20"));
		assertNotNull(bacNetPlugConf.getObject("BACNETOBJECT:21"));
		assertNotNull(bacNetPlugConf.getObject("BACNETDEVICE:10"));
		assertNotNull(bacNetPlugConf.getObject("BACNETPROPERTY:31"));

		assertEquals(1, bacNetPlugConf.getRoots().size());
		assertEquals("BACNETDEVICE:10", bacNetPlugConf.getRoots().iterator().next().getID());

		assertEquals("ip", bacNetPlugConf.getPluginProperty("transportType").getValue());
	}
}
