CREATE TABLE IF NOT EXISTS config (
  config_id IDENTITY NOT NULL,
  timestamp BIGINT NOT NULL,
  data CLOB NOT NULL,
  );
