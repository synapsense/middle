package com.synapsense.plugin.configuration;

import com.synapsense.plugin.api.exceptions.ConfigPluginException;
import com.synapsense.plugin.apinew.PluginConfig;
import com.synapsense.plugin.apinew.Transport;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;
import org.apache.log4j.Logger;

/**
 * ConfigurationService handles configuration parsing and storing.
 */
public class ConfigurationService {
	private final static Logger logger = Logger.getLogger(ConfigurationService.class);

    private static final String PROP_NAME = "configTransport";
    private static final String CONFIG_XML = "config.xml";

    private boolean configRestored = false;
    private Path directory;

	public ConfigurationService() {
        this(Paths.get("."));
    }

    public ConfigurationService(Path directory) {
        this.directory = directory;
    }

    public  Map<String, PluginConfig> processConfig(String config){
        Map<String, PluginConfig> pluginConfigs = null;
        try {
            pluginConfigs = parseConfig(config);
        } catch (ConfigPluginException e) {
            logger.error("Provided plug-ins config cannot be parsed. Plugins are not configured. Waiting for the valid configuration file.", e);
            logger.info("Config: \n" + config);
            return null;
        }

		// if config is parsed successfully, store it
		try {
            storeConfig(config);
		} catch (Exception e) {
			logger.warn("Failed to store config: " + e.getMessage(), e);
		}
        return pluginConfigs;
    }

    public Map<String, PluginConfig> tryRestoreConfig() {
        Map<String, PluginConfig> pluginConfigs = null;
        if(!configRestored){
            try {
                String storedConfig = getStoredConfig();
                if(storedConfig != null){
                    pluginConfigs = parseConfig(storedConfig);
                    configRestored = true;
                } else {
                    logger.info("There's no cached config to restore.");
                }
            } catch (ConfigPluginException e) {
                logger.warn("Failed to restore cached config.", e);
            }
        }
        return pluginConfigs;
    }

	public Map<String, PluginConfig> parseConfig(String config) throws ConfigPluginException {
		logger.info("Parsing config ...");
        Map<String, PluginConfig> pluginConfigs = DmXmlConfigReader.loadPluginsConfig(config);
		logger.info("Config was parsed successfully.");
		return pluginConfigs;
	}

    /** Stores config to disk, overwriting the previous one. */
	public void storeConfig(String config) throws ConfigPluginException {
		try {
            logger.info("Storing config ...");
            Files.write(configFile(), config.getBytes(StandardCharsets.UTF_8));
            logger.info("Config was stored successfully.");
            //since we got a new config, we will have to restore it next time
            configRestored = false;
		} catch (IOException e) {
            throw new ConfigPluginException(e.getMessage(), e);
		}

	}

    private Path configFile() {
        return directory.resolve(CONFIG_XML);
    }

    /*
     * Gets config from disk.
     *
     * @return stored config if available, null otherwise.
     */
    public String getStoredConfig() throws ConfigPluginException {
        logger.info("Loading cached config ...");
		try {
            if(Files.exists(configFile())) {
                return new String(Files.readAllBytes(configFile()), StandardCharsets.UTF_8);
            }
            else{
                logger.info("There's no cached config.");
                return null;
            }
        } catch (IOException e) {
            throw new ConfigPluginException(e.getMessage(), e);
        }
    }

    /*
	 * Transports have to use this method to check if they are allowed to configure plugins.
	 */
	public static boolean isAllowedToConfig(Transport transport) {
		if (transport == null)
			throw new IllegalArgumentException("transport cannot be null");

		String transName = System.getProperty(PROP_NAME, null);
		if (transName == null || transName.isEmpty())
			return true; // no transport is specified. anybody is welcome to
		// configure

        return transport.getClass().getName().endsWith(transName);
    }
}
