package com.synapsense.plugin.conversion;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.mvel2.MVEL;
import org.mvel2.ParserContext;

import com.synapsense.plugin.DeviceManagerRMI;
import com.synapsense.plugin.apinew.PropertyDesc;
import com.synapsense.plugin.manager.DeviceManagerCore;
import com.synapsense.plugin.manager.TagIDs;

/**
 * ElConverter is responsible for expression language based value conversions
 * and provides following capabilities:
 * <ul>
 * <li>basic mathematical operations. The set of operation includes Addition,
 * Subtraction, Multiplication, Division, Modulus</li>
 * <li>java.lang.Math functions</li>
 * <li>access to converted property value, the property itself and property's
 * parent object</li>
 * <li>access DeviceManagerCore's getProperty and setProperty functions</li>
 * </ul>
 * <b>Available variables:</b>
 * <ul>
 * <li>value
 * <p>
 * In order to access convertible value from the expression, <code>value</code>
 * variable must be used.
 * </p>
 * </li>
 * <li>prop
 * <p>
 * PropertyDesc object, which value is converted. Example:
 * "'Name: ' + prop.getName() + ' Value: ' + prop.getValue()" will resolve into
 * 'Name: a Value: 2' if property name is 'a' and value is '2'.
 * </p>
 * </li>
 * <li>parentObj
 * <p>
 * ObjectDesc object, parent of the prop. Example:
 * "parentObj.getProperty(prop.name + '1').value + value". The result after
 * evaluation of this expression will be '12' if parentObj has two properties
 * with names 'a' and 'a1'(the property, convert method is called for) and
 * values '1' and '2' respectively.
 * </p>
 * </li>
 * <li>parentObjId
 * <p>
 * String value of the parent object id.
 * </p>
 * </li>
 * </ul>
 * 
 * For more info about mvel syntaxes and available operations go to <a
 * href="http://mvel.codehaus.org/Language+Guide+for+2.0"> MVEL2 Language
 * Guide</a>
 * 
 * @author stikhon
 * 
 */
public class ElConverter implements Converter {

	private final static Logger logger = Logger.getLogger(ElConverter.class);
	private Map<String, Serializable> compiledExprCache = new HashMap<String, Serializable>();
	ParserContext parcerCtx = new ParserContext();
	DeviceManagerRMI deviceManager;

	public ElConverter() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.synapsense.plugin.conversion.Converter#convert(com.synapsense.plugin
	 * .apinew.PropertyDesc, java.io.Serializable)
	 */
	public Serializable convert(final PropertyDesc property, final Serializable value) throws ValueConversionException {

		if (!property.getTags().containsKey(TagIDs.EL_CONVERTER_SET)) {
			return value;
		}
		return performConversion(property, value, property.getTags().get(TagIDs.EL_CONVERTER_SET).getValue());

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.synapsense.plugin.conversion.Converter#convertBack(com.synapsense
	 * .plugin.apinew.PropertyDesc, java.io.Serializable)
	 */
	public Serializable convertBack(PropertyDesc property, Serializable value) throws ValueConversionException {
		if (!property.getTags().containsKey(TagIDs.EL_CONVERTER_GET)) {
			return value;
		}
		return performConversion(property, value, property.getTags().get(TagIDs.EL_CONVERTER_GET).getValue());
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private Serializable performConversion(final PropertyDesc property, final Serializable value,
	        final String expression) throws ValueConversionException {
		Serializable compiledExpression;
		if (!compiledExprCache.containsKey(expression)) {
			compiledExpression = MVEL.compileExpression(expression, parcerCtx);
			compiledExprCache.put(expression, compiledExpression);
		} else {
			compiledExpression = compiledExprCache.get(expression);
		}

		Map evalContext = new HashMap();
		evalContext.put("prop", property);
		evalContext.put("parentObj", property.getParent());
		evalContext.put("parentObjId", property.getParent().getID());
		evalContext.put("value", value);
		evalContext.put("DeviceManager", DeviceManagerCore.getInstance());

		logger.debug("Trying to perform EL conversion for property: " + property + " , value: " + value
		        + " and expression: " + expression);
		try {
			return (Serializable) MVEL.executeExpression(compiledExpression, evalContext);
		} catch (Exception e) {
			if (e instanceof InvocationTargetException) {
				e = (Exception) ((InvocationTargetException) e).getTargetException();
			}
			logger.debug("EL Evaluation exception: " + e.getMessage());

			throw new ValueConversionException("Failed to perform EL conversion for the property " + property.getName()
			        + ". Reason: " + e.getMessage());
		}
	}
}
