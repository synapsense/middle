package com.synapsense.plugin.manager;

import java.io.Serializable;
import java.util.*;

import org.apache.log4j.Logger;

import com.synapsense.plugin.DeviceManagerRMI;
import com.synapsense.plugin.api.exceptions.ConfigPluginException;
import com.synapsense.plugin.api.exceptions.DMException;
import com.synapsense.plugin.api.exceptions.IncorrectStateException;
import com.synapsense.plugin.apinew.*;
import com.synapsense.plugin.configuration.ConfigurationService;
import com.synapsense.plugin.configuration.PluginConfigImpl;
import com.synapsense.plugin.conversion.ConversionService;
import com.synapsense.plugin.conversion.ValueConversionException;
import com.synapsense.plugin.messages.RefinedAlertsBuilder;
import com.synapsense.plugin.poll.PollingService;

public final class DeviceManagerCore implements DeviceManagerRMI {

	private static final String ALERT_TYPE = "Communication error";
	private PollingService pollingService;
	private final static Logger logger = Logger.getLogger(DeviceManagerCore.class);

	private final static DeviceManagerCore instance = new DeviceManagerCore();

	private Map<String, Plugin> plugins;
	private ObjectsStore objects;
	private Transport transport;
	private ConversionService conversionService;
	private ConfigurationService configurationService;

	private DeviceManagerCore() {
		objects = new ObjectsStore();
		loadPlugins();
	}

	public synchronized static DeviceManagerCore getInstance() {
		return instance;
	}

	private void loadPlugins() {
		plugins = new HashMap<String, Plugin>();
		ServiceLoader<Plugin> svcs = ServiceLoader.load(Plugin.class);
		Set<String> enabledPlugins = getEnabledPlugins();
        logger.info("Enabled plugins: " + enabledPlugins);
		for (Plugin plugin : svcs) {
			logger.info("Found plugin for service " + Plugin.class.toString() + " named "
			        + plugin.getClass().toString() + " with ID " + plugin.getID());
			if (plugins.get(plugin.getID()) != null) {
				logger.error("Key " + plugin.getID() + " is duplicated in "
				        + plugins.get(plugin.getID()).getClass().toString() + " and " + plugin.getClass().toString());
			} else if (enabledPlugins.isEmpty() || enabledPlugins.contains(plugin.getClass().getSimpleName())) {
				plugins.put(plugin.getID(), plugin);
			} else {
				logger.info("Plugin " + plugin.getClass().getSimpleName() + " is disabled.");
			}
		}
	}

	/** Returns a set of class names specified by the enabledPlugins property. The set is empty if the property is not defined. */
	private Set<String> getEnabledPlugins() {
        Set<String> result = new HashSet<>();
        String value = System.getProperty("enabledPlugins", "").trim();
        // avoid "".split() -> [""]
        if (!value.isEmpty()) {
            result.addAll(Arrays.asList(value.split("\\s+")));
        }
		return result;
	}

	public Plugin getPlugin(String name) {
		Plugin plugin = null;
		if (plugins.containsKey(name)) {
			plugin = plugins.get(name);
		} else {
			logger.error("Cannot find plugin " + name);
		}
		return plugin;
	}

	public void start() {
		for (Plugin plugin : plugins.values()) {
			try {
				plugin.start();
			} catch (IncorrectStateException e) {
				logger.warn(e.getMessage(), e);
			}
		}
	}

	public void setTransport(Transport transport) {
		this.transport = transport;
		for (Plugin plugin : plugins.values()) {
			plugin.setTransport(transport);
		}
	}

	public List<Properties> enumeratePlugins() {
		List<Properties> pluginsDescr = new ArrayList<Properties>();
		for (Plugin plugin : plugins.values()) {
			pluginsDescr.add(plugin.getPluginDescription());
		}
		return pluginsDescr;
	}

	/**
	 * Configures all the plug-ins, which config is found in the provided config
	 * xml.
	 *
	 * @param config
	 *            data to be processed by the plug-ins
	 */
	public void configurePlugins(final String config) {
		Map<String, PluginConfig> pluginConfigs;
		if (config == null) {
			logger.info("Trying to use cached config ...");
			pluginConfigs = configurationService.tryRestoreConfig();
		} else {
			// If DM has never been configured, then we must have received a full configuration.
			pluginConfigs = configurationService.processConfig(config);
		}
		if (pluginConfigs != null) {
			logger.info("Resetting Polling Service...");
			pollingService.reset();
			logger.info("Configuring plugins...");
			// if we didn't get configuration for some plugins, they need to be
			// stopped
			for (Plugin pluginToStop : plugins.values()) {
				if (!pluginConfigs.containsKey(pluginToStop.getID())) {
					pluginToStop.stop();
				}
			}
			Iterator<String> iterator = pluginConfigs.keySet().iterator();
			while (iterator.hasNext()) {
				String pluginID = iterator.next();
				try {
					internalConfigurePlugin(pluginID, pluginConfigs.get(pluginID));

				} catch (Exception e) {
					// configuration failure for one plugin should not affect
					// other
					logger.warn("Plugin " + pluginID + " cannot be configured.", e);
					// polling service is unavailable for plugins, which failed
					// to configure
					iterator.remove();
				}
			}

			logger.info("Starting and configuring Polling Service");
			pollingService.configure(pluginConfigs);
			pollingService.start();
		}
	}

	private void internalConfigurePlugin(String pluginId, PluginConfig partialConfig) throws DMException {

		if (!plugins.containsKey(pluginId)) {
			throw new ConfigPluginException("Plugin with ID [" + pluginId
			        + "] is not found for the provided configuration and will be skipped.");
		}
		Plugin plugin = plugins.get(pluginId);
		PropertyDesc timestampProperty = partialConfig.getPluginProperty(PluginConfig.TIMESTAMP_PROP);
		if (timestampProperty != null) {
			plugin.getPluginDescription().setProperty(Plugin.LAST_UPDATE_TIME, timestampProperty.getValue());
		}
		objects.mergeObjects(partialConfig.getObjects(), pluginId);
		// construct new PluginConfig instance with full configuration
		PluginConfigImpl fullConfig = new PluginConfigImpl(pluginId);
		fullConfig.setObjects(objects.getObjects(pluginId));
		for (ObjectDesc root : partialConfig.getRoots()) {
			fullConfig.addRoot(objects.getObject(root.getID(), pluginId));
		}
		for (PropertyDesc prop : partialConfig.getPluginProperties().values()) {
			fullConfig.addPropery(prop);
		}

		logger.info("Configuring [" + pluginId + "] plug-in.");

		plugin.configure(fullConfig);
		logger.info("Plug-in [" + pluginId + "] is successfully configured.");
		plugin.start();

	}

	public Serializable getPropertyValue(String objectID, String propertyName) {
		String pluginID = objects.getPluginID(objectID);
		if (pluginID != null) {
			ObjectDesc obj = objects.getObject(objectID, pluginID);
			Serializable value = plugins.get(pluginID).getPropertyValue(obj, propertyName);
			PropertyDesc prop = obj.getProperty(propertyName);
			try {
				return conversionService.convertBack(prop, value);
			} catch (ValueConversionException e) {
				logger.warn("Unable to perform property conversion for object's property  [" + objectID + ":"
				        + propertyName + "]. " + e.getMessage() + " Unable to get property value.");
				return null;
			} catch (Exception e) {
				transport.sendData(objectID, "alert", new RefinedAlertsBuilder(ALERT_TYPE,
				        "Property value is failed to get", e.getLocalizedMessage()).getMessage());
				logger.error(e.getLocalizedMessage(), e);
				return null;
			}
		} else {
			logger.warn("Unknown object " + objectID);
			return null;
		}
	}

	public void setPropertyValue(String objectID, String propertyName, Serializable value) {
		String pluginID = objects.getPluginID(objectID);
		if (pluginID != null) {
			ObjectDesc obj = objects.getObject(objectID, pluginID);
			try {
				PropertyDesc prop = obj.getProperty(propertyName);
				Serializable convertedValue = conversionService.convert(prop, value);
				plugins.get(pluginID).setPropertyValue(obj, propertyName, convertedValue);
			} catch (ValueConversionException e) {
				logger.warn("Unable to perform property conversion for object's property [" + objectID + ":"
				        + propertyName + "]." + e.getMessage() + " Unable to set property value.");
			} catch (Exception e) {
				transport.sendData(objectID, "alert", new RefinedAlertsBuilder(ALERT_TYPE,
				        "Property value is failed to set", e.getLocalizedMessage()).getMessage());
				logger.error(e.getLocalizedMessage(), e);
			}

		} else {
			logger.warn("Unknown object " + objectID);
		}
	}

	public void pushPropertyValue(String objectId, String propertyName) {
		try {
			Serializable value = getPropertyValue(objectId, propertyName);
			transport.sendData(objectId, propertyName, value);
		} catch (Exception e) {
			logger.warn("Error retrieving Object ID [" + objectId + "], property [" + propertyName + "]. Alert will be sent.", e);
			transport.sendData(objectId, "alert", new RefinedAlertsBuilder(ALERT_TYPE,
			        "Unable to retrieve property value from device.", e.getLocalizedMessage()).getMessage());
		}
	}

	public void setConversionService(ConversionService conversionService) {
		this.conversionService = conversionService;
	}

	public void setPollingService(PollingService pollingService) {
		this.pollingService = pollingService;
	}

	public void setConfigurationService(ConfigurationService configurationService) {
		this.configurationService = configurationService;
	}
}
