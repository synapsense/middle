package com.synapsense.plugin.conversion;

import java.io.Serializable;
import java.util.LinkedHashSet;
import java.util.Set;

import com.synapsense.plugin.apinew.PropertyDesc;

/**
 * The ConversionService is responsible for property value conversion.
 * 
 * @author stikhon
 * 
 */
public class ConversionService {

	private Set<Converter> converters;
	private static final ConversionService instance = new ConversionService();

	private ConversionService() {
		converters = new LinkedHashSet<Converter>();
		converters.add(new ElConverter());
		converters.add(new UnitConverter());
	}

	public static ConversionService getInstance() {
		return instance;
	}

	/**
	 * Converts value to the suitable for the Device Manager's plug-in format.
	 * Conversion is based on the property's tags. If no conversion tags found,
	 * the provided value will be returned back without any changes.
	 * 
	 * @param property
	 *            The property which is being converted.
	 * @param value
	 *            The value to convert.
	 * @return Converted value.
	 * @throws ValueConversionException
	 *             Thrown in case of an issue during the conversion.
	 */
	public Serializable convert(final PropertyDesc property, final Serializable value) throws ValueConversionException {
		// TODO change converters store organization, it should be extendible
		// and prioritized

		Serializable convertedValue = value;
		for (Converter converter : converters) {
			convertedValue = converter.convert(property, convertedValue);
		}

		return convertedValue;

	}

	/**
	 * Converts value to the suitable for the Device Manager's client format.
	 * Conversion is based on the property's tags. If no conversion tags found,
	 * the provided value will be returned back without any changes.
	 * 
	 * @param property
	 *            The property which is being converted.
	 * @param value
	 *            The value to convert.
	 * @return Converted value.
	 * @throws ValueConversionException
	 *             Thrown in case of an issue during the conversion.
	 */
	public Serializable convertBack(final PropertyDesc property, final Serializable value)
	        throws ValueConversionException {
		Serializable convertedValue = value;
		for (Converter converter : converters) {
			convertedValue = converter.convertBack(property, convertedValue);
		}
		return convertedValue;
	}

	public void addConverter(Converter converter) {
		converters.add(converter);
	}

}
