package com.synapsense.plugin.configuration;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.StringReader;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.digester.Digester;
import org.apache.commons.digester.Rule;
import org.apache.commons.digester.Substitutor;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.AttributesImpl;

import com.synapsense.plugin.api.exceptions.ConfigPluginException;
import com.synapsense.plugin.apinew.ObjectDesc;
import com.synapsense.plugin.apinew.PluginConfig;
import com.synapsense.plugin.apinew.impl.ObjectDescImpl;
import com.synapsense.plugin.apinew.impl.PropertyDescImpl;
import com.synapsense.plugin.apinew.impl.TagDescImpl;

public class DmXmlConfigReader {

	private static final String ROOT = "DMConfig";

	private static final String PLUGIN = ROOT + "/plugin";
	private static final String OBJECT = PLUGIN + "/obj";
	private static final String NESTED_OBJECT = "*/obj/obj";
	private static final String PLUGIN_PROPERTY = PLUGIN + "/prop";
	private static final String OBJECT_PROPERTY = "*/obj/prop";
	private static final String TAG = "*/prop/tag";

	private static final String NAME_ATTR = "name";

	/**
	 * Loads config from file.
	 * 
	 * @param src
	 *            Path to the file to read.
	 * @return Map of the plugin configs.
	 * @throws ConfigPluginException
	 */
	public static Map<String, PluginConfig> loadPluginsConfig(File src) throws ConfigPluginException {
		InputSource input;
		try {
			input = new InputSource(new FileInputStream(src));
			input.setSystemId(src.toURI().toURL().toString());
		} catch (IOException e) {
			throw new ConfigPluginException("Unable to read config file: " + src.toString(), e);
		}

		return parseConfig(input);
	}

	/**
	 * Load config from string.
	 * 
	 * @param config
	 *            XML config file.
	 * @return Map of the plugin configs.
	 * @throws ConfigPluginException
	 */
	public static Map<String, PluginConfig> loadPluginsConfig(String config) throws ConfigPluginException {
		InputSource input = new InputSource(new StringReader(config));
		return parseConfig(input);
	}

	@SuppressWarnings("unchecked")
	private static Map<String, PluginConfig> parseConfig(InputSource source) throws ConfigPluginException {
		Digester digester = new Digester();
		digester.setValidating(false);
		digester.setNamespaceAware(true);
		digester.setSubstitutor(new Substitutor() {

			@Override
			public String substitute(String bodyText) {
				return unescape(bodyText);
			}

			@Override
			public Attributes substitute(Attributes attributes) {
				AttributesImpl attr = new AttributesImpl(attributes);
				for (int i = 0; i < attr.getLength(); i++) {
					attr.setValue(i, unescape(attr.getValue(i)));
				}
				return attr;
			}

			public String unescape(String str) {
				StringBuffer buf = new StringBuffer(str.length());
				int i;
				for (i = 0; i < str.length(); ++i) {
					char ch = str.charAt(i);
					if (ch == '&') {
						int semi = str.indexOf(';', i + 1);
						if (semi == -1) {
							buf.append(ch);
							continue;
						}
						String entityName = str.substring(i + 1, semi);
						int entityValue;
						if ("quot".equals(entityName)) {
							entityValue = 34;
						} else if ("amp".equals(entityName)) {
							entityValue = 38;
						} else if ("lt".equals(entityName)) {
							entityValue = 60;
						} else if ("gt".equals(entityName)) {
							entityValue = 62;
						} else if ("apos".equals(entityName)) {
							entityValue = 39;
						} else {
							entityValue = -1;
						}
						if (entityValue == -1) {
							buf.append('&');
							buf.append(entityName);
							buf.append(';');
						} else {
							buf.append((char) (entityValue));
						}
						i = semi;
					} else {
						buf.append(ch);
					}
				}
				return buf.toString();
			}
		});
		addRules(digester);
		// initial map, which will be populated with objects
		Map<String, PluginConfig> objectsInPlugins = new HashMap<String, PluginConfig>();
		digester.push(objectsInPlugins);

		try {
			return (Map<String, PluginConfig>) digester.parse(source);
		} catch (IOException e) {
			throw new ConfigPluginException("Unable to read config.", e);
		} catch (SAXException e) {
			throw new ConfigPluginException("Config XML file has incorrect format.", e);
		}

	}

	private static void addRules(Digester digester) {

		// Rule for <plugin> tags. Adds a new ObjectDesc list to the result map.
		digester.addRule(PLUGIN, new Rule() {
			@SuppressWarnings("unchecked")
			@Override
			public void begin(Attributes attributes) throws Exception {
				String name = attributes.getValue(NAME_ATTR);
				Map<String, PluginConfig> objectsInPlugins = (Map<String, PluginConfig>) getDigester().peek();
				PluginConfigImpl config = new PluginConfigImpl(name);
				objectsInPlugins.put(name, config);
				getDigester().push(config);
			}

			@Override
			public void end() throws Exception {
				getDigester().pop();
			}
		});

		// Rule for <obj> tags, which follow right after the <plugin> tag. These
		// are the root <obj> tags.

		digester.addObjectCreate(OBJECT, ObjectDescImpl.class);
		digester.addSetProperties(OBJECT);

		digester.addRule(OBJECT, new Rule() {
			@Override
			public void end() throws Exception {
				// take the top object
				ObjectDesc obj = (ObjectDesc) digester.peek();
				// and put it into common objects map for this plugin. This
				// configuration
				// is the next the to root element, there digester.getCount() -
				// 1 is
				// the root.
				PluginConfigImpl config = (PluginConfigImpl) digester.peek(digester.getCount() - 2);
				config.addObject(obj);
				config.addRoot(obj);
			}
		});

		// Rules for all other internal <obj> tags. Create an ObjectDescImpl
		// object,
		// populate it's attributes and add it to the parent ObjectDescImpl as a
		// child.
		digester.addObjectCreate(NESTED_OBJECT, ObjectDescImpl.class);
		digester.addSetProperties(NESTED_OBJECT);
		digester.addSetNext(NESTED_OBJECT, "addChild");
		digester.addSetTop(NESTED_OBJECT, "setParent", ObjectDescImpl.class.getName());

		digester.addRule(NESTED_OBJECT, new Rule() {
			@Override
			public void end() throws Exception {
				// take the top object
				ObjectDesc obj = (ObjectDesc) digester.peek();
				// and put it into common objects map for this plugin.
				PluginConfigImpl config = (PluginConfigImpl) digester.peek(digester.getCount() - 2);
				config.addObject(obj);
			}
		});

		// Rules for plugin properties <prop> tags. Create an PropertyDescImpl
		// object,
		// populate it's attributes and add it to the PluginConfiguration
		// object.
		digester.addObjectCreate(PLUGIN_PROPERTY, PropertyDescImpl.class);
		digester.addSetProperties(PLUGIN_PROPERTY);
		digester.addRule(PLUGIN_PROPERTY, new Rule() {
			@Override
			public void end() throws Exception {
				// take the top property
				PropertyDescImpl prop = (PropertyDescImpl) digester.peek();
				// and put it into plugin properties configuration. This
				// configuration
				// is the next the to root element, there digester.getCount() -
				// 1 is
				// the root.
				PluginConfigImpl config = (PluginConfigImpl) digester.peek(digester.getCount() - 2);
				config.addPropery(prop);
			}
		});

		// Rules for <prop> tags. Create an PropertyDescImpl object,
		// populate it's attributes and add it to the parent ObjectDescImpl.
		digester.addObjectCreate(OBJECT_PROPERTY, PropertyDescImpl.class);
		digester.addSetProperties(OBJECT_PROPERTY);
		digester.addSetNext(OBJECT_PROPERTY, "addProperty", PropertyDescImpl.class.getName());
		digester.addSetTop(OBJECT_PROPERTY, "setParent", ObjectDescImpl.class.getName());

		// Rules for <tag> tags. Create an TagDescImpl object,
		// populate it's attributes and add it to the parent PropertyDescImpl.
		digester.addObjectCreate(TAG, TagDescImpl.class);
		digester.addSetProperties(TAG);
		digester.addSetNext(TAG, "addTag", TagDescImpl.class.getName());

	}
}
