package com.synapsense.plugin.transport.log;

import java.io.IOException;
import java.io.Serializable;

import org.apache.log4j.Logger;

public class LoggingTransport implements com.synapsense.plugin.apinew.Transport {
	private final static Logger logger = Logger.getLogger(LoggingTransport.class);
	private final static int PRIORITY = 100;

	@Override
	public void connect() throws IOException {
		logger.trace("connected to logger :-)");
	}

	@Override
	public int getPriority() {
		return PRIORITY;
	}

	@Override
	public void sendData(String objectID, String propertyName, Serializable value) {
		logMessage(objectID, propertyName, value, "data");
	}

	@Override
	public void sendOrderedData(String objectID, String propertyName, Serializable value) {
		logMessage(objectID, propertyName, value, "ordered");
	}

	private static void logMessage(String objectID, String propertyName, Serializable value, String type) {
		if (logger.isDebugEnabled()) {
			String valData = "NULL";
			if (value != null)
				valData = value.toString();
			logger.debug("Got " + type + " message: " + " [" + objectID + ", " + propertyName + ", " + valData + "]");
		}
	}
}
