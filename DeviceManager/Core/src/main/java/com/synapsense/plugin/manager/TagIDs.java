package com.synapsense.plugin.manager;

/**
 * Class, which contains all the property tags, used by the Device Manager.
 * 
 * @author stikhon
 * 
 */
public class TagIDs {
	public static final String POLL = "poll";
	public static final String EL_CONVERTER_GET = "elConvGet";
	public static final String EL_CONVERTER_SET = "elConvSet";
	public static final String TYPE_CONVERTER_GET = "typeConvGet";
	public static final String TYPE_CONVERTER_SET = "typeConvSet";
	public static final String UNIT_CONVERTER_GET = "unitConvGet";
	public static final String UNIT_CONVERTER_SET = "unitConvSet";
}
