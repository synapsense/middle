/**
 * 
 */
package com.synapsense.plugin.conversion;

/**
 * The ValueConversionException is thrown in case the value conversion is
 * failed.
 * 
 * @author stikhon
 * 
 */
public class ValueConversionException extends Exception {

	private static final long serialVersionUID = 1560704582341354607L;

	public ValueConversionException() {
		super();

	}

	public ValueConversionException(String msg) {
		super(msg);

	}

	public ValueConversionException(String msg, Throwable cause) {
		super(msg, cause);

	}
}
