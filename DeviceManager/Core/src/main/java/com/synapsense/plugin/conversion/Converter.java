package com.synapsense.plugin.conversion;

import java.io.Serializable;

import com.synapsense.plugin.apinew.PropertyDesc;

public interface Converter {
	/**
	 * Converts value of the given property in accordance with the specified
	 * conversion for set methods.
	 * 
	 * @throws ValueConversionException
	 *             Thrown if value conversion failed.
	 */
	Serializable convert(PropertyDesc property, Serializable value) throws ValueConversionException;

	/**
	 * Converts value of the given property in accordance with the specified
	 * conversion for get methods.
	 * 
	 * @throws ValueConversionException
	 *             Thrown if value conversion failed.
	 */
	Serializable convertBack(PropertyDesc property, Serializable value) throws ValueConversionException;

}
