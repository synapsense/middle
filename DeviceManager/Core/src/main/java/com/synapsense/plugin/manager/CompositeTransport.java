package com.synapsense.plugin.manager;

import java.io.IOException;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.ServiceLoader;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import org.apache.log4j.Logger;

import com.synapsense.plugin.apinew.Transport;

/**
 * This <code>DeviceManagerTransport</code> implementation allows to communicate
 * with
 * 
 * @author anechaev
 * 
 */
public class CompositeTransport implements Transport {
	private final static Logger logger = Logger.getLogger(CompositeTransport.class);
	private Set<Transport> transports;

	public CompositeTransport() {
		transports = new TreeSet<Transport>(new Comparator<Transport>() {
			@Override
			public int compare(Transport arg0, Transport arg1) {
				if (arg0.getPriority() < arg1.getPriority())
					return -1;
				if (arg0.getPriority() > arg1.getPriority())
					return 1;
				return arg0.getClass().getName().compareTo(arg1.getClass().getName());
			}
		});

		ServiceLoader<Transport> svcs = ServiceLoader.load(Transport.class);
		Set<String> enabledTransports = getEnabledTransports();
        logger.info("Enabled transports: " + enabledTransports);
        for (Transport transport : svcs) {
			if (enabledTransports.isEmpty() || enabledTransports.contains(transport.getClass().getSimpleName())) {
				addTransport(transport);
			} else {
				logger.info("Transport " + transport.getClass().getSimpleName() + " is disabled.");
			}
		}
	}

	/** Returns a set of class names specified by the enabledTransports property. The set is empty if the property is not defined. */
	private Set<String> getEnabledTransports() {
		Set<String> result = new HashSet<>();
        String value = System.getProperty("enabledTransports", "").trim();
        // avoid "".split() -> [""]
        if (!value.isEmpty()) {
            result.addAll(Arrays.asList(value.split("\\s+")));
        }
		return result;
	}

	/**
	 * The method is used to add transports manually. Seems to be useful in
	 * testing environment.
	 * 
	 * @param transport
	 */
	public void addTransport(Transport transport) {
		logger.info("Adding transport:" + transport.getClass().getName());
		if (!transports.add(transport)) {
			logger.warn("The transport " + transport.getClass().getName() + " has been added already.");
		}
	}

	@Override
	public void connect() throws IOException {
		for (Transport transport : transports) {
			transport.connect();
		}
	}

	@Override
	public int getPriority() {
		return 0;
	}

	@Override
	public void sendData(String objectID, String propertyName, Serializable value) {
		if (!LocalTimeChecker.isSystemTimeCorrect()) {
			if (logger.isDebugEnabled())
				logger.debug("Got data for property " + objectID + "/" + propertyName
				        + "; not sending due to incorrect local time");
			return;
		}

		if (logger.isDebugEnabled())
			logger.debug("Got data for property " + objectID + "/" + propertyName + "; sending to transports");

		for (Transport transport : transports) {
			transport.sendData(objectID, propertyName, value);
		}
	}

	@Override
	public void sendOrderedData(String objectID, String propertyName, Serializable value) {
		if (!LocalTimeChecker.isSystemTimeCorrect()) {
			if (logger.isDebugEnabled())
				logger.debug("Got ordered data for property " + objectID + "/" + propertyName
				        + "; not sending due to incorrect local time");
			return;
		}

		if (logger.isDebugEnabled())
			logger.debug("Got ordered data for property " + objectID + "/" + propertyName + "; sending to transports");

		for (Transport transport : transports) {
			transport.sendOrderedData(objectID, propertyName, value);
		}
	}

	private static class LocalTimeChecker {
		// 1/28/2014 6:00:00 PM PST;
		// an arbitrary timestamp after Jan 1 1970 would do
		private static final long REF_TIME = 1390960800000l;

		// the last time when logging was performed
		private static long lastLogTime;

		private static final long LOG_INTERVAL = 5 * 60_000;// 5 mins

		private static final String SUCC_FORMAT = "System time check: current time (%1$tF %1$tT %1$tZ) looks correct, start sending data";
		private static final String FAIL_FORMAT = "System time check: current time (%1$tF %1$tT %1$tZ) looks wrong, not sending data";

		private static final AtomicBoolean successLogged = new AtomicBoolean();
		private static final ReadWriteLock rwLock = new ReentrantReadWriteLock();

		public static boolean isSystemTimeCorrect() {

			// this call itself takes just a few or less milliseconds thus it's
			// OK to do it every time
			long currentTime = System.currentTimeMillis();
			if(currentTime > REF_TIME) {
				rwLock.readLock().lock();
				try {
					if (successLogged.compareAndSet(false, true)) {
						logger.info(String.format(SUCC_FORMAT, new Date(currentTime)));
					}
					return true;// under normal conditions this is where it ends
				} finally {
					rwLock.readLock().unlock();
				}
			}

			long timeSinceLastLog = currentTime - lastLogTime; //can be negative if NTP adjusted clocks back

			// log only once within LOG_INTERVAL
			if (timeSinceLastLog < 0 || timeSinceLastLog > LOG_INTERVAL) {
				try {
					rwLock.writeLock().lock();
					//check everything again

					currentTime = System.currentTimeMillis();
					if (currentTime > REF_TIME) {
						if (successLogged.compareAndSet(false, true)) {
							logger.info(String.format(SUCC_FORMAT, new Date(currentTime)));
						}
						return true;
					}

					timeSinceLastLog = currentTime - lastLogTime;
					if (timeSinceLastLog < 0 || timeSinceLastLog > LOG_INTERVAL) {
						lastLogTime = currentTime;
						logger.warn(String.format(FAIL_FORMAT, new Date(currentTime)));
						// fall through for ret false
					}
				} finally {
					rwLock.writeLock().unlock();
				}
			}

			return false;
		}
	}
}
