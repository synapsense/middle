package com.synapsense.plugin.manager;

import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.synapsense.plugin.apinew.ObjectDesc;
import com.synapsense.plugin.apinew.PropertyDesc;
import com.synapsense.plugin.apinew.TagDesc;
import com.synapsense.plugin.apinew.impl.PropertyDescImpl;
import com.synapsense.util.algo.SetsIntersector;

public class ObjectsStore {
	private final static Logger logger = Logger.getLogger(ObjectsStore.class);

	private final Map<String, Map<String, ObjectDesc>> objectsInPlugins = new HashMap<String, Map<String, ObjectDesc>>();
	SetsIntersector<String> idSetsIntersector = new SetsIntersector<String>();
	Comparator<String> stringComparator = new Comparator<String>() {

		@Override
		public int compare(String o1, String o2) {
			return o1.compareTo(o2);
		}
	};

	/**
	 * @param objectID
	 * @return plug-in ID for the particular objectID
	 */
	public String getPluginID(String objectID) {
		for (String pluginID : objectsInPlugins.keySet()) {
			Map<String, ObjectDesc> objects = objectsInPlugins.get(pluginID);
			if (objects.containsKey(objectID)) {
				return pluginID;
			}
		}

		logger.warn("Unable to find the Plug-in, which handles object with ID: " + objectID);
		return null;
	}

	/**
	 * Add objects to the storage. If there was an object with the particular
	 * ID, it is replaced by the new one.
	 * 
	 * @param objs
	 * @param pluginID
	 */
	public void addObjects(Map<String, ObjectDesc> objs, String pluginID) {
		Map<String, ObjectDesc> objects = objectsInPlugins.get(pluginID);
		if (objects == null) {
			objects = new HashMap<String, ObjectDesc>();
			objectsInPlugins.put(pluginID, objs);
		}
		objects.putAll(objs);
	}

	/**
	 * This method merges object instances using the following rules: <br />
	 * - If an Object exits both in the given map and in the storage, it's
	 * properties are merged. <br />
	 * - If an Object doesn't exist in the provided map but exists in the
	 * storage, it is deleted form the storage.<br />
	 * - If an Object exists in the provided map but don't exist in the storage,
	 * it is added to the storage.
	 * 
	 * @param newObjects
	 * @param pluginID
	 */
	public void mergeObjects(Map<String, ObjectDesc> newObjects, String pluginID) {
		logger.debug("Merging configuration objects...");
		Map<String, ObjectDesc> existingObjects = objectsInPlugins.get(pluginID);
		if (existingObjects == null) {
			logger.debug("Nothing to merge. All objects are added to configuration.");
			// we don't have any objects for this plugin, put all objects.
			objectsInPlugins.put(pluginID, newObjects);

		} else {
			Collection<String> values = idSetsIntersector.intersect(existingObjects.keySet(), newObjects.keySet(),
			        stringComparator);
			// Since configuration represents a tree, we will take the new tree
			// and add missing properties, which were not updated and therefore
			// were not sent to DM to this new objects tree.
			for (String key : values) {
				logger.debug("Merging properties for object [" + key + "]");
				ObjectDesc objectToMergeTo = newObjects.get(key);
				ObjectDesc objectToMergeFrom = existingObjects.get(key);
				for (PropertyDesc prop : objectToMergeFrom.getProperties()) {
					String propertyName = prop.getName();
					if (!objectToMergeTo.containsProperty(propertyName)) {
						PropertyDescImpl newProp = new PropertyDescImpl(propertyName, prop.getValue());
						newProp.setParent(objectToMergeTo);
						for (TagDesc tag : prop.getTags().values()) {
							newProp.addTag(tag);
						}
						objectToMergeTo.addProperty(newProp);
					}
				}
			}
			objectsInPlugins.put(pluginID, newObjects);
		}

	}

	public ObjectDesc getObject(String objectId) {
		for (Map<String, ObjectDesc> objects : objectsInPlugins.values()) {
			ObjectDesc result = objects.get(objectId);
			if (result != null) {
				return result;
			}
		}
		return null;
	}

	public ObjectDesc getObject(String objectId, String pluginId) {
		Map<String, ObjectDesc> objects = objectsInPlugins.get(pluginId);
		if (objects != null) {
			ObjectDesc result = objects.get(objectId);
			if (result != null) {
				return result;
			}
		}
		return null;
	}

	public Map<String, ObjectDesc> getObjects(String pluginId) {
		return objectsInPlugins.get(pluginId);
	}
}
