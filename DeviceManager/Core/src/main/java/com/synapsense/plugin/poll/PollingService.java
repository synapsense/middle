package com.synapsense.plugin.poll;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.synapsense.plugin.manager.DeviceManagerCore;
import org.apache.log4j.Logger;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;

import com.synapsense.plugin.apinew.ObjectDesc;
import com.synapsense.plugin.apinew.PluginConfig;
import com.synapsense.plugin.apinew.PropertyDesc;
import com.synapsense.plugin.apinew.TagDesc;
import com.synapsense.plugin.manager.TagIDs;

/**
 * The service which implements common polling mechanism for all the plug-ins.
 * 
 * @author stikhon
 * 
 */
public class PollingService {

    public final static String CORE_ID = "dmCore";
	public final static String PROPERTIES_LIST_ID = "propertiesList";
	public final static String GET_PROPERTY_JOB_NAME = "GetPropertyJob";

	private final static Logger log = Logger.getLogger(PollingService.class);

	private Scheduler scheduler;
    private DeviceManagerCore dmCore;

	public PollingService(DeviceManagerCore dmCore) {
        this.dmCore = dmCore;
		try {
			scheduler = StdSchedulerFactory.getDefaultScheduler();
		} catch (SchedulerException e) {
			log.error("Unable to instantiate polling service.");
		}

	}

	/**
	 * This method allows to configure and schedule quartz polling jobs. The
	 * method do the following:
	 * <ul>
	 * <li>For each plug-in a new trigger group is created.</li>
	 * <li>For all the properties of each object associated with the plug-in
	 * with the same poll period a new trigger is created.</li>
	 * </ul>
	 *
	 */
	public void configure(Map<String, PluginConfig> pluginConfigs) {

		// [BC 07/2015] After upgrading to quartz 2.2.1, I had to make getPropertyJob durable (required for jobs without triggers).
		JobDetail getPropertyJob = JobBuilder.newJob(GetPropertyJob.class).withIdentity(GET_PROPERTY_JOB_NAME).storeDurably().build();
        //since we use RAMJobStore we can safely put DeviceManagerCore(which is not serializable) into jobDataMap
        getPropertyJob.getJobDataMap().put(CORE_ID, dmCore);
		try {
			scheduler.addJob(getPropertyJob, true);
		} catch (SchedulerException e1) {
			log.error("Unable to schedue the Job " + getPropertyJob.toString(), e1);
		}
		for (String plugin : pluginConfigs.keySet()) {
			Map<Long, List<PropertyDesc>> groupedProperties = new HashMap<Long, List<PropertyDesc>>();
			PluginConfig pluginConfig = pluginConfigs.get(plugin);
			for (ObjectDesc object : pluginConfig.getObjects().values()) {
				for (PropertyDesc property : object.getProperties()) {
					TagDesc tag = property.getTag(TagIDs.POLL);
					if (tag != null) {
						Long pollPeriod = null;
						try {
							pollPeriod = Long.parseLong(tag.getValue());
						} catch (NumberFormatException e) {
							log.warn("Property [" + property.getName() + "] of the Object [" + object.getID()
							        + "] has tag [" + TagIDs.POLL + "] with unparsable as Long value ["
							        + tag.getValue() + "]. The property won't be added to the polling service.");
							continue;
						}
						if (pollPeriod > 0) {
							if (groupedProperties.containsKey(pollPeriod)) {
								groupedProperties.get(pollPeriod).add(property);
							} else {
								List<PropertyDesc> list = new ArrayList<PropertyDesc>();
								list.add(property);
								groupedProperties.put(pollPeriod, list);
							}
						}
					}
				}

			}
			for (Long pollPeriod : groupedProperties.keySet()) {

				Trigger t = TriggerBuilder
				        .newTrigger()
				        .forJob(GET_PROPERTY_JOB_NAME)
				        .withIdentity(pollPeriod.toString(), plugin)
				        .withSchedule(
				                SimpleScheduleBuilder.simpleSchedule().repeatForever()
				                        .withIntervalInMilliseconds(pollPeriod.longValue())).build();
				t.getJobDataMap().put(PROPERTIES_LIST_ID, groupedProperties.get(pollPeriod));

				try {
					scheduler.scheduleJob(t);
				} catch (SchedulerException e) {
					log.error("Unable to schedule the Job " + getPropertyJob.toString() + "with trigger " + toString(),
					        e);
				}
			}
		}
	}

	/**
	 * Starts the Quartz scheduler.
	 */
	public void start() {
		try {
			if (scheduler.isShutdown()) {
				scheduler = StdSchedulerFactory.getDefaultScheduler();
			}
			scheduler.start();
		} catch (SchedulerException e) {
			log.error("Unable to start polling service.", e);
		}
	}

	/**
	 * Stops the Quartz scheduler.
	 */
	public void stop() {
		try {
			scheduler.shutdown();
		} catch (SchedulerException e) {
			log.error("Unable to stop polling service.", e);
		}
	}

	/**
	 * Reset the Quartz scheduler (clear existing jobs).
	 */
	public void reset() {
		try {
			scheduler.clear();
		} catch (SchedulerException e) {
			log.error("Unable to reset polling service.", e);
		}
	}

}
