/**
 * 
 */
package com.synapsense.plugin.configuration;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.synapsense.plugin.apinew.ObjectDesc;
import com.synapsense.plugin.apinew.PluginConfig;
import com.synapsense.plugin.apinew.PropertyDesc;

/**
 * @author ser
 * 
 */
public class PluginConfigImpl implements PluginConfig {

	private final String pluginName;

	private Map<String, ObjectDesc> objects = new HashMap<String, ObjectDesc>();
	private List<ObjectDesc> roots = new LinkedList<ObjectDesc>();
	private Map<String, PropertyDesc> pluginProperties = new HashMap<String, PropertyDesc>();

	public PluginConfigImpl(final String pluginName) {
		this.pluginName = pluginName;
	}

	public ObjectDesc addObject(final ObjectDesc object) {
		if (object != null) {
			return objects.put(object.getID(), object);
		}
		return null;
	}

	public void addRoot(final ObjectDesc object) {
		roots.add(object);
	}

	public PropertyDesc addPropery(final PropertyDesc property) {
		if (property != null) {
			return pluginProperties.put(property.getName(), property);
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.synapsense.plugin.configuration.PluginConfiguration#getPluginName()
	 */
	@Override
	public String getPluginName() {
		return this.pluginName;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.synapsense.plugin.configuration.PluginConfiguration#getObject(java
	 * .lang.String)
	 */
	@Override
	public ObjectDesc getObject(final String id) {
		return objects.get(id);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsense.plugin.configuration.PluginConfiguration#getObjects()
	 */
	@Override
	public Map<String, ObjectDesc> getObjects() {
		return objects;
	}

	public void setObjects(Map<String, ObjectDesc> obejctsToSet) {
		this.objects = obejctsToSet;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsense.plugin.configuration.PluginConfiguration#getRoots()
	 */
	@Override
	public List<ObjectDesc> getRoots() {
		return roots;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.synapsense.plugin.configuration.PluginConfiguration#getProperty(java
	 * .lang.String)
	 */
	@Override
	public PropertyDesc getPluginProperty(final String propertyName) {
		return pluginProperties.get(propertyName);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.synapsense.plugin.configuration.PluginConfiguration#getProperties()
	 */
	@Override
	public Map<String, PropertyDesc> getPluginProperties() {
		return pluginProperties;
	}

}
