package com.synapsense.plugin.configuration;

import java.io.InputStream;
import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.URIResolver;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

public class DmXmlConfigMerger {

	private static final Transformer merger;

	static {
		try (InputStream is = DmXmlConfigMerger.class.getResourceAsStream("merger.xsl")) {
			merger = TransformerFactory.newInstance().newTransformer(new StreamSource(is));
		} catch (Exception e) {
			throw new RuntimeException("Cannot instantiate DM configuration merger", e);
		}
	}

	/**
	 * Merges data from newConfig to the oldCondig with the following algorithm:
	 * <ul>
	 * <li>If an object from oldConf is absent in newConf, delete it from the
	 * result</li>
	 * <li>If an object is present in newConf only, copy it to the result as is</li>
	 * <li>If an object is present in both oldConf and newConf, merge its
	 * properties as in the next step</li>
	 * <li>For plugin and object level properties, take all properties from
	 * oldConfig; override them with corresponding plugin/object properties from
	 * newConfig if present there</li>
	 * </ul>
	 * 
	 * <b>NOTE: this method does not validate input XML strings!</b>
	 * 
	 * @param oldConfig
	 *            old DM XML configuration
	 * @param newConfig
	 *            new DM XML configuration (can be partial)
	 * @return merged XML configuration
	 * @throws TransformerException
	 */
	public static String merge(final String oldConfig, final String newConfig) throws TransformerException {
		merger.reset();
		merger.setParameter("oldConfigName", "oldConfig.xml");
		merger.setURIResolver(new URIResolver() {
			@Override
			public Source resolve(String href, String base) throws TransformerException {
				return new StreamSource(new StringReader(oldConfig));
			}
		});

		StringWriter result = new StringWriter();
		merger.transform(new StreamSource(new StringReader(newConfig)), new StreamResult(result));
		return result.toString();
	}

}
