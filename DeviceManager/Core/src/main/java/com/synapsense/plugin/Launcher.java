package com.synapsense.plugin;

import java.net.URL;

import org.apache.log4j.Logger;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.joran.JoranConfigurator;
import ch.qos.logback.core.joran.spi.JoranException;

import com.synapsense.plugin.configuration.ConfigurationService;
import com.synapsense.plugin.conversion.ConversionService;
import com.synapsense.plugin.manager.CompositeTransport;
import com.synapsense.plugin.manager.DeviceManagerCore;
import com.synapsense.plugin.poll.PollingService;
import com.synapsense.plugin.transport.log.LoggingTransport;

public class Launcher {

	private final static Logger logger = Logger.getLogger(Launcher.class);

	private final static String DM_CONFIG = "dm.conf";
	private final static String DM_LOG_CONFIG = "dm-logback.xml";

	private static Object lock = new Object();
	private static PollingService pollingService;

	public static void main(String[] args) throws Exception {

		configureLogging();

		// [BC 04/2015] Log unhandled exceptions that crash threads!
		Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
			@Override
			public void uncaughtException(Thread t, Throwable e) {
				logger.error(t + " terminated because of an uncaught exception.", e);
			}
		});

		URL d = Launcher.class.getClassLoader().getResource(DM_CONFIG);
		System.getProperties().loadFromXML(d.openStream());

		Runtime.getRuntime().addShutdownHook(new Thread() {
			public void run() {
				logger.info("Device Manager is shutting down ...");
			}
		});

		logger.info("launching Device Manager...");
		DeviceManagerCore dm = DeviceManagerCore.getInstance();
		pollingService = new PollingService(dm);

		dm.setConfigurationService(new ConfigurationService());
		dm.setPollingService(pollingService);
		dm.setConversionService(ConversionService.getInstance());
		CompositeTransport transport = new CompositeTransport();
		dm.setTransport(transport);
		transport.addTransport(new LoggingTransport());
		transport.connect();
		pollingService.start();

		logger.info("Device Manager is launched.");
		logger.info("Entering main loop...");
		synchronized (lock) {
			lock.wait();
		}
	}

	public static void stop() {
		synchronized (lock) {
			lock.notifyAll();
		}
		pollingService.stop();
		System.exit(0);
	}

	private static void configureLogging() throws JoranException {
		LoggerContext lc = (LoggerContext) LoggerFactory.getILoggerFactory();
		JoranConfigurator configurator = new JoranConfigurator();
		configurator.setContext(lc);
		// don't reset the existing configuration
		lc.reset();
		configurator.doConfigure(Launcher.class.getClassLoader().getResourceAsStream(DM_LOG_CONFIG));
	}

}
