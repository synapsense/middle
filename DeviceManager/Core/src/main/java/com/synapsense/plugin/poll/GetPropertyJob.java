package com.synapsense.plugin.poll;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.synapsense.plugin.apinew.ObjectDesc;
import com.synapsense.plugin.apinew.PropertyDesc;
import com.synapsense.plugin.manager.DeviceManagerCore;

/**
 * Quartz job, which pushes values for all objects, found in the
 * JobExecutionContext. List of properties in the execution context depends on
 * fired trigger.
 * 
 * @author stikhon
 * 
 */
public class GetPropertyJob implements Job {

	private final static Logger log = Logger.getLogger(GetPropertyJob.class);
	private DeviceManagerCore dmCore;
	private List<PropertyDesc> propertiesList;

	@SuppressWarnings("unchecked")
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		for (PropertyDesc property : propertiesList) {
			ObjectDesc parentObject = property.getParent();
			if (parentObject == null) {
				log.error("Unable to execute GetPropertyJob for property " + property.getName()
				        + " since it has no parent object.");
			}

			dmCore.pushPropertyValue(parentObject.getID(), property.getName());
		}
	}

	/*
	 * This property is set by quartz JobFactory
	 */
	public void setDmCore(DeviceManagerCore dmCore) {
		this.dmCore = dmCore;
	}

	/*
	 * This property is set by quartz JobFactory
	 */
	public void setPropertiesList(List<PropertyDesc> propertiesList) {
		this.propertiesList = propertiesList;
	}
}
