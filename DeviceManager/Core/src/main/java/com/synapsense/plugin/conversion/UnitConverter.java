package com.synapsense.plugin.conversion;

import java.io.InputStreamReader;
import java.io.Serializable;
import java.security.InvalidParameterException;

import org.apache.commons.beanutils.ConvertUtilsBean;
import org.apache.log4j.Logger;

import com.synapsense.plugin.apinew.PropertyDesc;
import com.synapsense.plugin.manager.TagIDs;
import com.synapsense.util.unitconverter.UnitSystems;

/**
 * UnitConverter utilizes the UnitConverter from the EnvironmentAPI, which can
 * perform various unit conversions.
 * 
 * @author stikhon
 * 
 */
public class UnitConverter implements Converter {
	private static ConvertUtilsBean typeConverter = new ConvertUtilsBean();

	private UnitSystems unitSystems;
	private static final String CONFIG_PATH = "/systems.xml";
	private final static Logger logger = Logger.getLogger(ElConverter.class);

	public UnitConverter() {
		unitSystems = new UnitSystems();
		try {
			unitSystems.loadFromXml(new InputStreamReader(UnitConverter.class.getResourceAsStream(CONFIG_PATH)));
		} catch (Exception e) {
			logger.error(
			        "Unable to load unit conversions config (systems.xml) from classpath. UnitConvertor faild to initialize.",
			        e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.synapsense.plugin.conversion.Converter#convert(com.synapsense.plugin
	 * .apinew.PropertyDesc, java.io.Serializable)
	 */
	public Serializable convert(final PropertyDesc property, final Serializable value) throws ValueConversionException {

		if (!property.getTags().containsKey(TagIDs.UNIT_CONVERTER_SET)) {
			return value;
		}
		return performConversion(property, value, property.getTags().get(TagIDs.UNIT_CONVERTER_SET).getValue());

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.synapsense.plugin.conversion.Converter#convertBack(com.synapsense
	 * .plugin.apinew.PropertyDesc, java.io.Serializable)
	 */
	public Serializable convertBack(PropertyDesc property, Serializable value) throws ValueConversionException {
		if (!property.getTags().containsKey(TagIDs.UNIT_CONVERTER_GET)) {
			return value;
		}
		return performConversion(property, value, property.getTags().get(TagIDs.UNIT_CONVERTER_GET).getValue());
	}

	private Serializable performConversion(final PropertyDesc property, final Serializable value,
	        final String conversion) throws ValueConversionException {
		String[] parameters = conversion.split(":");
		if (parameters == null || parameters.length != 3) {
			throw new InvalidParameterException("The conversion parameters: " + conversion + " for property "
			        + property.getName() + " are incorrect. Unit conversion cannot be performed.");
		}
		try {

			com.synapsense.util.unitconverter.UnitConverter converter = unitSystems.getUnitConverter(parameters[0],
			        parameters[1], parameters[2]);
			if (converter == null) {
				logger.warn("No suitable unit convertes found to perform conversion from " + parameters[0]
				        + " system to " + parameters[1] + " with dimention " + parameters[2]);
				return value;
			}
			logger.debug("Trying to perform UNIT Conversion for property: [" + property + "] , value: [" + value
			        + "]  from " + parameters[0] + " system to " + parameters[1] + " with dimention " + parameters[2]);
			// the EnvironmentAPI Unit Converter works only with Double values
			Double result = converter.convert((Double) typeConverter.convert(value, Double.class));
			logger.debug("UNIT Conversion for property: [" + property + "] perfprmed successfully. Result value is: ["
			        + result + "]");
			return result;
		} catch (Exception e) {
			throw new ValueConversionException("Faild to perform UNIT conversion for property [" + property.getName()
			        + "] from [" + parameters[0] + "] system to [" + parameters[1] + "] with dimention ["
			        + parameters[2] + "]. Reason:" + e.getMessage());
		}
	}

}
