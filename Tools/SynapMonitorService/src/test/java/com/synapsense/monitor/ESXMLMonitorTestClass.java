package com.synapsense.monitor;

import static org.junit.Assert.*;

import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;

import mockit.Deencapsulation;

import org.junit.Test;

import com.synapsense.monitor.impl.ESXMLMonitor;

public class ESXMLMonitorTestClass {

	private static String SERVER = "localhost";
	private String testFileAddr = "http://" + SERVER + ":8080/synapsoft/getServicesStatus.xml";

	/*
	 * @Test public void testMonitoring() throws MalformedURLException,
	 * URISyntaxException{
	 * 
	 * Properties p = getProperties();
	 * 
	 * monitor = new ESXMLMonitor(p);
	 * 
	 * final URL testFileURL = new URL(testFileAddr); new
	 * NonStrictExpectations() { { new URL(anyString); returns(testFileURL);
	 * 
	 * }};
	 * 
	 * boolean isAvailable = monitor.validateNow(); assertTrue(isAvailable); }
	 */
	@Test
	public void testInvalidURLforMonitoring() throws Exception {

		Properties p = getProperties();
		p.setProperty("url", "http://synapsense:8080");

		ESXMLMonitor monitor = new ESXMLMonitor(p);

		boolean isAvailable = monitor.validateNow();

		isAvailable = monitor.validateNow();
		assertFalse(isAvailable);
	}

	@Test
	public void testMonitorUnreachableES() throws Exception {

		Properties p = getProperties();
		p.setProperty("url", "jnp://synapsense:1099");

		ESXMLMonitor monitor = new ESXMLMonitor(p);

		boolean isAvailable = monitor.validateNow();
		assertFalse("Should not be available", isAvailable);

	}

	@Test
	public void testESXMLMonitorReload() {

		Properties p = getProperties();

		ESXMLMonitor monitor = new ESXMLMonitor(p);
		String prop = "jnp://synapsense:1099";
		p.setProperty("url", prop);
		monitor.reload(p);

		prop = Deencapsulation.getField(monitor, "url");
		assertEquals(p.getProperty("url"), prop);

		p.setProperty("user", "admin");
		monitor.reload(p);
		prop = Deencapsulation.getField(monitor, "user");
		assertEquals(p.getProperty("user"), prop);

		p.setProperty("password", "admin");
		monitor.reload(p);
		prop = Deencapsulation.getField(monitor, "pass");
		assertEquals(p.getProperty("password"), prop);

		p.setProperty("monitoring_expression", "status='Not operational'");
		monitor.reload(p);
		prop = Deencapsulation.getField(monitor, "monitoringExpr");
		assertEquals(p.getProperty("monitoring_expression"), prop);

		p.setProperty("addresses", "synapsense@synapsense.com, cheloguz@mera.ru");
		monitor.reload(p);
		List<String> addresses = Deencapsulation.getField(monitor, "recipients");
		Iterator<String> itr2 = addresses.iterator();
		assertEquals(p.getProperty("addresses"), itr2.next() + "," + itr2.next());

		p.setProperty("report_time_threshold", "700");
		monitor.reload(p);
		int intprop = Deencapsulation.getField(monitor, "reportTimeThreshold");
		assertEquals(Integer.parseInt(p.getProperty("report_time_threshold")), intprop);

		p.setProperty("notifications", "4");
		monitor.reload(p);
		intprop = Deencapsulation.getField(monitor, "notifications");
		assertEquals(Integer.parseInt(p.getProperty("notifications")), intprop);

	}

	@Test(expected = MonitoringConfigurationException.class)
	public void testESXMLMonitorReloadEmptyUrl() {

		Properties p = getProperties();

		ESXMLMonitor monitor = new ESXMLMonitor(p);
		p.setProperty("url", "");
		monitor.reload(p);
	}

	@Test(expected = MonitoringConfigurationException.class)
	public void testESXMLMonitorReloadEmptyUser() {

		Properties p = getProperties();

		ESXMLMonitor monitor = new ESXMLMonitor(p);
		p.setProperty("user", "");
		monitor.reload(p);
	}

	@Test(expected = MonitoringConfigurationException.class)
	public void testESXMLMonitorReloadEmptyPass() {

		Properties p = getProperties();

		ESXMLMonitor monitor = new ESXMLMonitor(p);
		p.setProperty("password", "");
		monitor.reload(p);
	}

	@Test(expected = MonitoringConfigurationException.class)
	public void testESXMLMonitorReloadEmptyExpr() {

		Properties p = getProperties();

		ESXMLMonitor monitor = new ESXMLMonitor(p);
		p.setProperty("monitoring_expression", "");
		monitor.reload(p);
	}

	@Test(expected = MonitoringConfigurationException.class)
	public void testESXMLMonitorReloadEmptyAddreses() {

		Properties p = getProperties();

		ESXMLMonitor monitor = new ESXMLMonitor(p);
		p.setProperty("addresses", "");
		monitor.reload(p);
	}

	@Test(expected = MonitoringConfigurationException.class)
	public void testESXMLMonitorReloadEmptyReportTime() {

		Properties p = getProperties();

		ESXMLMonitor monitor = new ESXMLMonitor(p);
		p.setProperty("report_time_threshold", "");
		monitor.reload(p);
	}

	@Test(expected = MonitoringConfigurationException.class)
	public void testESXMLMonitorReloadEmptyNotifications() {

		Properties p = getProperties();

		ESXMLMonitor monitor = new ESXMLMonitor(p);
		p.setProperty("notifications", "");
		monitor.reload(p);
	}

	private Properties getProperties() {
		Properties p = new Properties();
		p.setProperty("url", "http://" + SERVER + ":8080");
		p.setProperty("user", "synapsense");
		p.setProperty("password", "synap$ense");
		p.setProperty("monitoring_expression", "status='Not started' or status='Not operational'");
		p.setProperty("addresses", "synapsense@synapsense.com");
		p.setProperty("report_time_threshold", "600");
		p.setProperty("notifications", "3");
		return p;
	}

}
