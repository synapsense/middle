package com.synapsense.monitor;

import static org.junit.Assert.*;

import org.junit.Test;

import com.synapsense.monitor.MonitoringService;
import com.synapsense.monitor.MonitoringConfiguration;

public class MonitorServiceTest {

	@Test
	public void testMonitorConfigLoading() throws Exception {
		MonitoringConfiguration configuration = new MonitoringConfiguration();
		assertNotNull(configuration.getHandlers());
	}

	@Test
	public void testMonitorServiceStart() throws Exception {
		MonitoringConfiguration configuration = new MonitoringConfiguration();

		MonitoringService service = new MonitoringService(configuration);

		service.startMonitoring();

	}

}
