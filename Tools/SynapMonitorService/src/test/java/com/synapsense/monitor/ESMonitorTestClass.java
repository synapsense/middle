package com.synapsense.monitor;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import mockit.Deencapsulation;
import mockit.Expectations;
import mockit.Mock;
import mockit.MockUp;
import mockit.Mocked;
import mockit.NonStrictExpectations;
import mockit.Verifications;
import org.junit.Test;

import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.monitor.Message;
import com.synapsense.monitor.Notifier;
import com.synapsense.monitor.impl.ESMonitor;
import com.synapsense.service.Environment;

public class ESMonitorTestClass {

	private static String SERVER = "localhost";
	@Mocked
	Environment mockedEnvironment;

	@Test
	public void testMonitorESOnSingleObject() throws Exception {

		final TO<?> checkpoint1 = TOFactory.getInstance().loadTO("SENSOR:1");

		Properties p = getProperties();
		p.setProperty("checkpoints", checkpoint1.toString());

		ESMonitor monitor = new ESMonitor(p);

		new NonStrictExpectations() {
			{
				new MockUp<ESMonitor>() {
					@Mock
					Environment lookupEnv(final String url, final String u, final String p) {
						return mockedEnvironment;
					}
				};
				mockedEnvironment.getPropertyValue(checkpoint1, "lastValue", Double.class);
				returns(1.0);
				mockedEnvironment.getPropertyTimestamp(checkpoint1, "lastValue");
				returns(System.currentTimeMillis() - 100);
			}
		};

		boolean isAvailable = monitor.validateNow();
		assertTrue(isAvailable);

		// ENVIRONMENT.deleteObject(checkpoint1);

		// isAvailable = monitor.validateNow();
		// assertFalse(isAvailable);

	}

	@Test
	public void testMonitorESOnMultipleObjectsBothAreOk() throws Exception {

		final TO<?> checkpoint1 = TOFactory.getInstance().loadTO("SENSOR:1");
		final TO<?> checkpoint2 = TOFactory.getInstance().loadTO("SENSOR:2");

		Properties p = getProperties();
		p.setProperty("checkpoints", checkpoint1.toString() + "," + checkpoint2.toString());

		ESMonitor monitor = new ESMonitor(p);

		new NonStrictExpectations() {
			{
				new MockUp<ESMonitor>() {
					@Mock
					Environment lookupEnv(final String url, final String u, final String p) {
						return mockedEnvironment;
					}
				};
				mockedEnvironment.getPropertyValue(checkpoint1, "lastValue", Double.class);
				returns(1.0);
				mockedEnvironment.getPropertyTimestamp(checkpoint1, "lastValue");
				returns(System.currentTimeMillis() - 100);
				mockedEnvironment.getPropertyValue(checkpoint2, "lastValue", Double.class);
				returns(1.0);
				mockedEnvironment.getPropertyTimestamp(checkpoint2, "lastValue");
				returns(System.currentTimeMillis() - 100);
			}
		};

		boolean isAvailable = monitor.validateNow();
		assertTrue(isAvailable);

	}

	@Test
	public void testMonitorESOnMultipleObjectsOneIsAbsent() throws Exception {

		final TO<?> checkpoint1 = TOFactory.getInstance().loadTO("SENSOR:1");
		final TO<?> checkpoint2 = TOFactory.getInstance().loadTO("SENSOR:2");

		Properties p = getProperties();
		p.setProperty("checkpoints", checkpoint1.toString() + "," + checkpoint2.toString());

		ESMonitor monitor = new ESMonitor(p);

		new NonStrictExpectations() {
			{
				new MockUp<ESMonitor>() {
					@Mock
					Environment lookupEnv(final String url, final String u, final String p) {
						return mockedEnvironment;
					}
				};
				mockedEnvironment.getPropertyValue(checkpoint1, "lastValue", Double.class);
				returns(1.0);
				mockedEnvironment.getPropertyTimestamp(checkpoint1, "lastValue");
				returns(System.currentTimeMillis() - 100);

			}

		};

		boolean isAvailable = monitor.validateNow();
		assertFalse(isAvailable);
	}

	@Test
	public void testMonitorESSingleObjectOutOfDate() throws Exception {

		final TO<?> checkpoint1 = TOFactory.getInstance().loadTO("SENSOR:1");

		Properties p = getProperties();
		p.setProperty("checkpoints", checkpoint1.toString());

		ESMonitor monitor = new ESMonitor(p);

		new NonStrictExpectations() {
			{
				new MockUp<ESMonitor>() {
					@Mock
					Environment lookupEnv(final String url, final String u, final String p) {
						return mockedEnvironment;
					}
				};

				mockedEnvironment.getPropertyValue(checkpoint1, "lastValue", Double.class);
				returns(1.0);
				mockedEnvironment.getPropertyTimestamp(checkpoint1, "lastValue");
				returns(System.currentTimeMillis() - 1000000);
			}
		};

		boolean isAvailable = monitor.validateNow();
		assertFalse("Should not be available", isAvailable);

	}

	@Test
	public void testMonitorUnreachableES() throws Exception {

		Properties p = getProperties();
		p.setProperty("url", "jnp://synapsense:1099");

		ESMonitor monitor = new ESMonitor(p);

		boolean isAvailable = monitor.validateNow();
		assertFalse("Should not be available", isAvailable);

	}


	/*
	 * @Test public void testMonitorESReportingByEmail() throws Exception {
	 * Mockery context = new JUnit4Mockery();
	 * 
	 * final TO<?> checkpoint1 = TOFactory.getInstance().loadTO("SENSOR:1");
	 * 
	 * Properties p = new Properties(); p.setProperty("url",
	 * "jnp://localhost:1099"); p.setProperty("user", "synapsense");
	 * p.setProperty("password", "synap$ense"); p.setProperty("checkpoints",
	 * "SENSOR:1"); p.setProperty("addresses", "synapsense@synapsense.com");
	 * p.setProperty("report_time_threshold", "600");
	 * p.setProperty("notifications", "3");
	 * 
	 * ESMonitor monitor = new ESMonitor(p); final Notifier mailer =
	 * context.mock(Notifier.class); monitor.setOnFailNotifier(mailer);
	 * 
	 * final Sequence seq = context.sequence("seq"); context.checking(new
	 * Expectations() { {
	 * 
	 * exactly(1).of(mailer).notify(with(any(Message.class))); inSequence(seq);
	 * } });
	 * 
	 * boolean isAvailable = monitor.validateNow();
	 * assertFalse("Should not be available", isAvailable);
	 * 
	 * context.assertIsSatisfied(); }
	 */

	@Test
	public void testESMonitorReload() {

		Properties p = getProperties();

		ESMonitor monitor = new ESMonitor(p);
		String prop = "jnp://synapsense:1099";
		p.setProperty("url", prop);
		monitor.reload(p);

		prop = Deencapsulation.getField(monitor, "providerUrl");
		assertEquals(p.getProperty("url"), prop);

		p.setProperty("user", "admin");
		monitor.reload(p);
		prop = Deencapsulation.getField(monitor, "user");
		assertEquals(p.getProperty("user"), prop);

		p.setProperty("password", "admin");
		monitor.reload(p);
		prop = Deencapsulation.getField(monitor, "pass");
		assertEquals(p.getProperty("password"), prop);

		p.setProperty("checkpoints", "SENSOR:1,SENSOR:2");
		monitor.reload(p);
		List<TO<?>> checkpoints = Deencapsulation.getField(monitor, "checkpoints");
		Iterator<TO<?>> itr1 = checkpoints.iterator();
		assertEquals(checkpoints.size(), 2);
		assertEquals(p.getProperty("checkpoints"), itr1.next().toString() + "," + itr1.next().toString());

		p.setProperty("addresses", "synapsense@synapsense.com, cheloguz@mera.ru");
		monitor.reload(p);
		List<String> addresses = Deencapsulation.getField(monitor, "recipients");
		Iterator<String> itr2 = addresses.iterator();
		assertEquals(p.getProperty("addresses"), itr2.next() + "," + itr2.next());

		p.setProperty("report_time_threshold", "700");
		monitor.reload(p);
		int intprop = Deencapsulation.getField(monitor, "reportTimeThreshold");
		assertEquals(Integer.parseInt(p.getProperty("report_time_threshold")), intprop);

		p.setProperty("notifications", "4");
		monitor.reload(p);
		intprop = Deencapsulation.getField(monitor, "notifications");
		assertEquals(Integer.parseInt(p.getProperty("notifications")), intprop);

	}

	@Test(expected = MonitoringConfigurationException.class)
	public void testESMonitorReloadEmptyUrl() {

		Properties p = getProperties();

		ESMonitor monitor = new ESMonitor(p);
		p.setProperty("url", "");
		monitor.reload(p);
	}

	@Test(expected = MonitoringConfigurationException.class)
	public void testESMonitorReloadEmptyUser() {

		Properties p = getProperties();

		ESMonitor monitor = new ESMonitor(p);
		p.setProperty("user", "");
		monitor.reload(p);
	}

	@Test(expected = MonitoringConfigurationException.class)
	public void testESMonitorReloadEmptyPass() {

		Properties p = getProperties();

		ESMonitor monitor = new ESMonitor(p);
		p.setProperty("password", "");
		monitor.reload(p);
	}

	@Test(expected = MonitoringConfigurationException.class)
	public void testESMonitorReloadEmptyCheckpoints() {

		Properties p = getProperties();

		ESMonitor monitor = new ESMonitor(p);
		p.setProperty("checkpoints", "");
		monitor.reload(p);
	}

	@Test(expected = MonitoringConfigurationException.class)
	public void testESMonitorReloadEmptyAddreses() {

		Properties p = getProperties();

		ESMonitor monitor = new ESMonitor(p);
		p.setProperty("addresses", "");
		monitor.reload(p);
	}

	@Test(expected = MonitoringConfigurationException.class)
	public void testESMonitorReloadEmptyReportTime() {

		Properties p = getProperties();

		ESMonitor monitor = new ESMonitor(p);
		p.setProperty("report_time_threshold", "");
		monitor.reload(p);
	}

	@Test(expected = MonitoringConfigurationException.class)
	public void testESMonitorReloadEmptyNotifications() {

		Properties p = getProperties();

		ESMonitor monitor = new ESMonitor(p);
		p.setProperty("notifications", "");
		monitor.reload(p);
	}

	private Properties getProperties() {
		Properties p = new Properties();
		p.setProperty("url", "jnp://" + SERVER + ":1099");
		p.setProperty("user", "synapsense");
		p.setProperty("password", "synap$ense");
		p.setProperty("checkpoints", "SENSOR:1");
		p.setProperty("addresses", "synapsense@synapsense.com");
		p.setProperty("report_time_threshold", "600");
		p.setProperty("notifications", "3");
		return p;
	}
}
