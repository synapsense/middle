package com.synapsense.monitor;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import mockit.Deencapsulation;

import org.junit.Test;

import com.synapsense.dto.TO;
import com.synapsense.monitor.impl.ESMonitor;

public class CompositeNotifierTest {

	@Test
	public void testNotStopNotifyIfOneFailCompositeNotifier() throws Exception {
		CompositeNotifier cn = new CompositeNotifier(false);
		cn.addNotifier(new TestNotifier(false));
		Message message = new Message(Arrays.asList("cheloguz@mera.ru"), "Test message", "Test Message content");
		boolean result = cn.notify(message);
		assertTrue(result);
		cn.addNotifier(new TestNotifier(false));
		cn.addNotifier(new TestNotifier(true));
		result = cn.notify(message);
		assertTrue(result);
	}

	@Test
	public void testStopNotifyIfOneFailCompositeNotifier() throws Exception {
		CompositeNotifier cn = new CompositeNotifier(true);
		cn.addNotifier(new TestNotifier(true));
		Message message = new Message(Arrays.asList("cheloguz@mera.ru"), "Test message", "Test Message content");
		boolean result = cn.notify(message);
		assertTrue(result);
		cn.addNotifier(new TestNotifier(false));
		cn.addNotifier(new TestNotifier(true));
		result = cn.notify(message);
		assertFalse(result);
	}

	@Test
	public void testAddinNotifiersToCompositeNotfier() {
		CompositeNotifier cn = new CompositeNotifier();
		TestNotifier trueNotifier = new TestNotifier(true);
		cn.addNotifier(trueNotifier);

		List<Notifier> notifierList = Deencapsulation.getField(cn, "notifiers");
		assertEquals(notifierList.size(), 1);
		boolean result = notifierList.contains(trueNotifier);
		assertTrue(result);

		TestNotifier falseNotifier = new TestNotifier(false);
		cn.addNotifier(falseNotifier);
		notifierList = Deencapsulation.getField(cn, "notifiers");
		assertEquals(notifierList.size(), 2);
		result = notifierList.contains(falseNotifier);
		assertTrue(result);

	}

	@Test
	public void testReloadinngCompositeNotfier() {
		Properties p = new Properties();
		p.setProperty("url", "jnp://localhost:1099");
		p.setProperty("user", "synapsense");
		p.setProperty("password", "synap$ense");
		p.setProperty("checkpoints", "SENSOR:1");
		p.setProperty("addresses", "synapsense@synapsense.com");
		p.setProperty("report_time_threshold", "600");
		p.setProperty("notifications", "3");

		ESMonitor monitor = new ESMonitor(p);
		p.setProperty("url", "jnp://synaptest:1099");
		monitor.reload(p);
		String prop = Deencapsulation.getField(monitor, "providerUrl");
		assertEquals(p.getProperty("url"), prop);

		p.setProperty("user", "admin");
		monitor.reload(p);
		prop = Deencapsulation.getField(monitor, "user");
		assertEquals(p.getProperty("user"), prop);

		p.setProperty("password", "admin");
		monitor.reload(p);
		prop = Deencapsulation.getField(monitor, "pass");
		assertEquals(p.getProperty("password"), prop);

		p.setProperty("checkpoints", "SENSOR:1,SENSOR:2");
		monitor.reload(p);
		List<TO<?>> checkpoints = Deencapsulation.getField(monitor, "checkpoints");
		Iterator<TO<?>> itr1 = checkpoints.iterator();
		assertEquals(checkpoints.size(), 2);
		assertEquals(p.getProperty("checkpoints"), itr1.next().toString() + "," + itr1.next().toString());

		p.setProperty("addresses", "synapsense@synapsense.com, cheloguz@mera.ru");
		monitor.reload(p);
		List<String> addresses = Deencapsulation.getField(monitor, "recipients");
		Iterator<String> itr2 = addresses.iterator();
		assertEquals(p.getProperty("addresses"), itr2.next() + "," + itr2.next());

		p.setProperty("report_time_threshold", "700");
		monitor.reload(p);
		int intprop = Deencapsulation.getField(monitor, "reportTimeThreshold");
		assertEquals(Integer.parseInt(p.getProperty("report_time_threshold")), intprop);

		p.setProperty("notifications", "4");
		monitor.reload(p);
		intprop = Deencapsulation.getField(monitor, "notifications");
		assertEquals(Integer.parseInt(p.getProperty("notifications")), intprop);

	}

	private class TestNotifier implements Notifier {
		private boolean result = true;

		public TestNotifier(boolean setResult) {
			result = setResult;
		}

		@Override
		public boolean notify(final Message message) {
			return result;
		}

		@Override
		public void reload(final Properties p) {
		}
	}

}
