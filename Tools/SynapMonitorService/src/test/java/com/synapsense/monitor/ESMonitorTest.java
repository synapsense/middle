package com.synapsense.monitor;

import static org.junit.Assert.*;

import java.util.Properties;

import org.jmock.Expectations;
import org.jmock.Mockery;
import org.jmock.Sequence;
import org.jmock.integration.junit4.JUnit4Mockery;
import org.junit.Test;

import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.monitor.Message;
import com.synapsense.monitor.Notifier;
import com.synapsense.monitor.impl.ESMonitor;
import com.synapsense.service.Environment;

public class ESMonitorTest {

	@Test
	public void testMonitorESOnSingleObject() throws Exception {
		Mockery context = new JUnit4Mockery();
		final Environment environment = context.mock(Environment.class);

		final TO<?> checkpoint1 = TOFactory.getInstance().loadTO("SENSOR:1");

		Properties p = new Properties();
		p.setProperty("url", "jnp://localhost:1099");
		p.setProperty("user", "synapsense");
		p.setProperty("password", "synap$ense");
		p.setProperty("checkpoints", "SENSOR:1");
		p.setProperty("addresses", "synapsense@synapsense.com");
		p.setProperty("report_time_threshold", "6000");
		p.setProperty("notifications", "3");

		ESMonitor monitor = new ESMonitor(p);

		context.checking(new Expectations() {
			{
				exactly(1).of(environment).exists(checkpoint1);
				will(returnValue(true));
				exactly(1).of(environment).getPropertyTimestamp(checkpoint1, "lastValue");
				will(returnValue(System.currentTimeMillis()));
			}
		});

		boolean isAvailable = monitor.validateNow();
		assertTrue(isAvailable);

		context.checking(new Expectations() {
			{
				exactly(1).of(environment).exists(checkpoint1);
				will(returnValue(false));
			}
		});
		isAvailable = monitor.validateNow();
		assertFalse(isAvailable);

		context.assertIsSatisfied();
	}

	@Test
	public void testMonitorESOnMultipleObjectsBothAreOk() throws Exception {
		Mockery context = new JUnit4Mockery();
		final Environment environment = context.mock(Environment.class);

		final TO<?> checkpoint1 = TOFactory.getInstance().loadTO("SENSOR:1");
		final TO<?> checkpoint2 = TOFactory.getInstance().loadTO("SENSOR:2");

		Properties p = new Properties();
		p.setProperty("url", "jnp://localhost:1099");
		p.setProperty("user", "synapsense");
		p.setProperty("password", "synap$ense");
		p.setProperty("checkpoints", "SENSOR:1,SENSOR:2");
		p.setProperty("addresses", "synapsense@synapsense.com");
		p.setProperty("report_time_threshold", "6000");
		p.setProperty("notifications", "3");

		ESMonitor monitor = new ESMonitor(p);

		final Sequence seq = context.sequence("seq");
		context.checking(new Expectations() {
			{
				exactly(1).of(environment).exists(checkpoint1);
				inSequence(seq);
				will(returnValue(true));

				exactly(1).of(environment).getPropertyTimestamp(checkpoint1, "lastValue");
				inSequence(seq);
				will(returnValue(System.currentTimeMillis()));

				exactly(1).of(environment).exists(checkpoint2);
				inSequence(seq);
				will(returnValue(true));

				exactly(1).of(environment).getPropertyTimestamp(checkpoint2, "lastValue");
				inSequence(seq);
				will(returnValue(System.currentTimeMillis()));
			}
		});

		boolean isAvailable = monitor.validateNow();
		assertTrue(isAvailable);

		context.assertIsSatisfied();
	}

	@Test
	public void testMonitorESOnMultipleObjectsOneIsAbsent() throws Exception {
		Mockery context = new JUnit4Mockery();
		final Environment environment = context.mock(Environment.class);

		final TO<?> checkpoint1 = TOFactory.getInstance().loadTO("SENSOR:1");
		final TO<?> checkpoint2 = TOFactory.getInstance().loadTO("SENSOR:2");

		Properties p = new Properties();
		p.setProperty("url", "jnp://localhost:1099");
		p.setProperty("user", "synapsense");
		p.setProperty("password", "synap$ense");
		p.setProperty("checkpoints", "SENSOR:1,SENSOR:2");
		p.setProperty("addresses", "synapsense@synapsense.com");
		p.setProperty("report_time_threshold", "6000");
		p.setProperty("notifications", "3");

		ESMonitor monitor = new ESMonitor(p);

		final Sequence seq = context.sequence("seq");
		context.checking(new Expectations() {
			{
				exactly(1).of(environment).exists(checkpoint1);
				inSequence(seq);
				will(returnValue(true));

				exactly(1).of(environment).getPropertyTimestamp(checkpoint1, "lastValue");
				inSequence(seq);
				will(returnValue(System.currentTimeMillis()));

				exactly(1).of(environment).exists(checkpoint2);
				inSequence(seq);
				will(returnValue(false));

			}
		});

		boolean isAvailable = monitor.validateNow();
		assertFalse(isAvailable);

		context.assertIsSatisfied();
	}

	@Test
	public void testMonitorESSingleObjectOutOfDate() throws Exception {
		Mockery context = new JUnit4Mockery();
		final Environment environment = context.mock(Environment.class);

		final TO<?> checkpoint1 = TOFactory.getInstance().loadTO("SENSOR:1");

		Properties p = new Properties();
		p.setProperty("url", "jnp://localhost:1099");
		p.setProperty("user", "synapsense");
		p.setProperty("password", "synap$ense");
		p.setProperty("checkpoints", "SENSOR:1");
		p.setProperty("addresses", "synapsense@synapsense.com");
		p.setProperty("report_time_threshold", "6000");
		p.setProperty("notifications", "3");

		ESMonitor monitor = new ESMonitor(p);

		final Sequence seq = context.sequence("seq");
		context.checking(new Expectations() {
			{
				exactly(1).of(environment).exists(checkpoint1);
				inSequence(seq);
				will(returnValue(true));

				exactly(1).of(environment).getPropertyTimestamp(checkpoint1, "lastValue");
				inSequence(seq);
				will(returnValue(System.currentTimeMillis() - 10 * 60 * 1000));

			}
		});

		boolean isAvailable = monitor.validateNow();
		assertFalse("Should not be available", isAvailable);

		context.assertIsSatisfied();
	}

	@Test
	public void testMonitorUnreachableES() throws Exception {
		Mockery context = new JUnit4Mockery();
		final Environment environment = context.mock(Environment.class);

		final TO<?> checkpoint1 = TOFactory.getInstance().loadTO("SENSOR:1");

		Properties p = new Properties();
		p.setProperty("url", "jnp://localhost:1099");
		p.setProperty("user", "synapsense");
		p.setProperty("password", "synap$ense");
		p.setProperty("checkpoints", "SENSOR:1");
		p.setProperty("addresses", "synapsense@synapsense.com");
		p.setProperty("report_time_threshold", "6000");
		p.setProperty("notifications", "3");

		ESMonitor monitor = new ESMonitor(p);

		final Sequence seq = context.sequence("seq");
		context.checking(new Expectations() {
			{
				exactly(1).of(environment).exists(checkpoint1);
				inSequence(seq);
				will(throwException(new RuntimeException("Server error")));
			}
		});

		boolean isAvailable = monitor.validateNow();
		assertFalse("Should not be available", isAvailable);

		context.assertIsSatisfied();
	}

	@Test
	public void testMonitorUnreachableESReporting() throws Exception {
		Mockery context = new JUnit4Mockery();
		final Environment environment = context.mock(Environment.class);

		final TO<?> checkpoint1 = TOFactory.getInstance().loadTO("SENSOR:1");

		Properties p = new Properties();
		p.setProperty("url", "jnp://localhost:1099");
		p.setProperty("user", "synapsense");
		p.setProperty("password", "synap$ense");
		p.setProperty("checkpoints", "SENSOR:1");
		p.setProperty("addresses", "synapsense@synapsense.com");
		p.setProperty("report_time_threshold", "6000");
		p.setProperty("notifications", "3");

		ESMonitor monitor = new ESMonitor(p);

		final Notifier mailer = context.mock(Notifier.class);
		monitor.setOnFailNotifier(mailer);

		final Sequence seq = context.sequence("seq");
		context.checking(new Expectations() {
			{
				exactly(1).of(environment).exists(checkpoint1);
				inSequence(seq);
				will(throwException(new RuntimeException("Server error")));

				exactly(1).of(mailer).notify(with(any(Message.class)));
				inSequence(seq);
			}
		});

		boolean isAvailable = monitor.validateNow();
		assertFalse("Should not be available", isAvailable);

		context.assertIsSatisfied();
	}

	@Test
	public void testMonitorESReportingByEmail() throws Exception {
		Mockery context = new JUnit4Mockery();
		final Environment environment = context.mock(Environment.class);

		final TO<?> checkpoint1 = TOFactory.getInstance().loadTO("SENSOR:1");

		Properties p = new Properties();
		p.setProperty("url", "jnp://localhost:1099");
		p.setProperty("user", "synapsense");
		p.setProperty("password", "synap$ense");
		p.setProperty("checkpoints", "SENSOR:1");
		p.setProperty("addresses", "synapsense@synapsense.com");
		p.setProperty("report_time_threshold", "6000");
		p.setProperty("notifications", "3");

		ESMonitor monitor = new ESMonitor(p);
		final Notifier mailer = context.mock(Notifier.class);
		monitor.setOnFailNotifier(mailer);

		final Sequence seq = context.sequence("seq");
		context.checking(new Expectations() {
			{
				exactly(1).of(environment).exists(checkpoint1);
				inSequence(seq);
				will(returnValue(true));

				exactly(1).of(environment).getPropertyTimestamp(checkpoint1, "lastValue");
				inSequence(seq);
				will(returnValue(System.currentTimeMillis() - 10 * 60 * 1000));

				exactly(1).of(mailer).notify(with(any(Message.class)));
				inSequence(seq);
			}
		});

		boolean isAvailable = monitor.validateNow();
		assertFalse("Should not be available", isAvailable);

		context.assertIsSatisfied();
	}

}
