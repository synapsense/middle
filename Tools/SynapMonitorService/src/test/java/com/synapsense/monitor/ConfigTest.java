package com.synapsense.monitor;

import static org.junit.Assert.*;

import java.io.*;
import java.net.URL;
import java.util.Iterator;
import java.util.Properties;

import org.apache.commons.configuration.HierarchicalINIConfiguration;
import org.apache.commons.configuration.SubnodeConfiguration;
import org.junit.Test;

public class ConfigTest {
	private static Properties notifiersProperties = new Properties();
	static {
		try {
			notifiersProperties.load(new InputStreamReader(ConfigTest.class.getClassLoader().getResourceAsStream("notifiers.conf")));
			notifiersProperties.remove("[SimpleSMTP]");
            notifiersProperties.remove("[SslSMTP]");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testConfigLoadViaThirdPartyINIReader() throws Exception {
        URL url =  ConfigTest.class.getClassLoader().getResource("notifiers.conf");
		HierarchicalINIConfiguration handlersConfig = new HierarchicalINIConfiguration();
		handlersConfig.load(url.getFile());

		Properties p = new Properties();
		for (Iterator<String> configIterator = handlersConfig.getSections().iterator(); configIterator.hasNext();) {
			String section = configIterator.next();
			if (section == null) {
				continue;
			}

			SubnodeConfiguration subNode = handlersConfig.getSection(section);

			Iterator<String> i = subNode.getKeys();
			while (i.hasNext()) {
				String key = i.next();
				String value = (String) subNode.getProperty(key);
				key = key.replaceAll("\\.\\.", ".");
				p.put(key, value);
			}
		}

		assertEquals(notifiersProperties, p);
	}

	@Test
	public void testEmptyConfigFiles() {

       // "emptyConf";
        URL url =  ConfigTest.class.getClassLoader().getResource("emptyConf");
		MonitoringConfiguration emptyConfiguration = new MonitoringConfiguration(url.getFile());
		assertTrue(emptyConfiguration.getHandlers().isEmpty());
	}

	@Test
	public void testWrongPath() {
		MonitoringConfiguration emptyConfiguration = new MonitoringConfiguration("Wrong  Path");
		assertTrue(emptyConfiguration.getHandlers().isEmpty());
	}

	@Test
	public void testConfigAutoRefresh() throws Exception {
		MonitoringConfiguration configuration = new MonitoringConfiguration();
		configuration.setAutoRefresh(5);
	}
}
