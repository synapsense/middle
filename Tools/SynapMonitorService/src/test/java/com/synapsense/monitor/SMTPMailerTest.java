package com.synapsense.monitor;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Properties;

import org.junit.Test;

import com.synapsense.monitor.Message;
import com.synapsense.monitor.Notifier;
import com.synapsense.monitor.impl.SMTPMailer;

public class SMTPMailerTest {

	@Test
	public void testHandlerSendEmailOneRecipient() throws Exception {
		Properties settings = new Properties();
		settings.load(new InputStreamReader(ConfigTest.class.getClassLoader().getResourceAsStream("notifiers.conf")));

		Notifier h = new SMTPMailer(settings);

		assertTrue(h.notify(new Message(Arrays.asList("shabanov@mera.ru"), "subject", "body")));
	}

	@Test
	public void testHandlerSendEmailMultipleRecipients() throws Exception {
		Properties settings = new Properties();
		settings.load(new InputStreamReader(ConfigTest.class.getClassLoader().getResourceAsStream("notifiers.conf")));

		Notifier h = new SMTPMailer(settings);

		assertTrue(h.notify(new Message(Arrays.asList("shabanov@mera.ru", "zorg1578@gmail.com"), "subject", "body")));
	}

}
