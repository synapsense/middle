; Multiple monitors can be defined in the configuration file. Please make sure you have no duplicate [<section name>] sections.

; 'polltime' parameter specifies the number of seconds between each monitor execution.
; This parameter applies to all monitors defined in the configuration file.
polltime=30

[ESMonitor]
; 'class' parameter is used to specify the name of monitor implementation - should remain unchanged
class=com.synapsense.monitor.impl.ESMonitor

; 'url' parameter is used to specify how to connect to JBoss instance. Replace localhost with the SynapSense Application Server IP address.
url=jnp://localhost:1099

; 'addresses' parameter is a comma separated list of e-mail recipients that the notification should be delivered to should the issue occur.
addresses=synapsense@synapsense.com

; 'subject' parameter is used to specify the subject for failure notification e-mail.
subject=SynapSense Monitoring Service Alert: ES Monitor failed

; 'login' and 'password' parameters should be used to specify credentials to connect to the Application Server.
user=synapsense
password=synap$ense

; 'checkpoints' parameter is a comma separated list of objects in the Application Server object model that should be used to
; assess system health. The list should be modified to include a few most representative objects in the model, i.e. critical
; wireless sense points, wired integration points and/or calculations. The objects in the list should have lastValue property
; that is supposed to be updated more frequently than the number of seconds specified in 'report_time_threshold' parameter.
checkpoints=WSNSENSOR:1,WSNSENSOR:2

; 'report_time_threshold' parameter specifies the number of seconds required before an object specified in the 'checkpoints' is
; considered to be not reporting.
report_time_threshold=600

; 'notification' parameter is used to determine the number of notification e-mails to be sent to recipients specified by
; 'addresses' parameter in case of failure.
notifications=3


[ESXMLMonitor]
; 'class' parameter is used to specify the name of monitor implementation - should remain unchanged
class=com.synapsense.monitor.impl.ESXMLMonitor

; 'url' parameter is used to specify how to connect to JBoss instance. Replace localhost with the SynapSense Web Console IP address.
url=http://localhost:8080

; 'addresses' parameter is a comma separated list of e-mail recipients that the notification should be delivered to should the issue occur.
addresses=synapsense@synapsense.com

; 'subject' parameter is used to specify the subject for failure notification e-mail.
subject=SynapSense Monitoring Service Alert: Service Auditor failed

; 'login' and 'password' parameters should be used to specify credentials to connect to the Application Server.
user=synapsense
password=synap$ense

; 'monitoring_expression' parameter is used to define criteria that indicates a failure of the software component.
monitoring_expression= "status='Not started' or status='Not operational'"

; 'report_time_threshold' parameter specifies the number of seconds required for the monitoring_expression to be positive
; to trigger a notification
report_time_threshold=600

; 'notification' parameter is used to determine the number of notification e-mails to be sent to recipients specified by
; 'addresses' parameter in case of failure.
notifications=3
