package com.synapsense.monitor.impl;

import java.io.IOException;
import java.io.InputStream;
import java.net.ConnectException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.StringTokenizer;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.*;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import com.synapsense.monitor.CompositeNotifier;
import com.synapsense.monitor.Handler;
import com.synapsense.monitor.Message;
import com.synapsense.monitor.MonitoringConfigurationException;
import com.synapsense.monitor.Notifier;

public class ESXMLMonitor implements Handler {

	private final static Logger logger = Logger.getLogger(ESXMLMonitor.class);

	/**
	 * Properties which are loaded from config file
	 */
	private String subject;
	private String user;
	private String pass;
	private int reportTimeThreshold;
	private List<String> recipients = new LinkedList<String>();
	private String monitoringExpr;
	private int notifications = 0;
	private String url;
	private HashMap<String, Long> timestamps;
	private int notificationCounter = 0;
	private Notifier onFailNotifier = new DefaultNotifier();

	public ESXMLMonitor(final Properties properties) {
		this.timestamps = new HashMap<String, Long>();
		reload(properties);
	}

	@Override
	public void reload(final Properties p) {
		synchronized (this) {
			this.subject = p.getProperty("subject", "ES is not available");

			String url = p.getProperty("url");
			if (url == null || url.isEmpty()) {
				throw new MonitoringConfigurationException(
				        "'url' property is not specified. Please check configuration file.");
			}
			this.url = url;

			String userLogin = p.getProperty("user");
			if (userLogin == null || userLogin.isEmpty()) {
				throw new MonitoringConfigurationException(
				        "'user' property is not specified. Please check configuration file.");
			}
			this.user = userLogin;

			String userPassword = p.getProperty("password");
			if (userPassword == null || userPassword.isEmpty()) {
				throw new MonitoringConfigurationException(
				        "'password' property is not specified. Please check configuration file.");
			}
			this.pass = userPassword;

			String reportTime = p.getProperty("report_time_threshold");
			if (reportTime == null || reportTime.isEmpty()) {
				throw new MonitoringConfigurationException(
				        "'report_time_threshold' property is not specified. Please check configuration file.");
			}
			this.reportTimeThreshold = Integer.parseInt(reportTime);

			String notifications = p.getProperty("notifications");
			if (notifications == null || notifications.isEmpty()) {
				throw new MonitoringConfigurationException(
				        "'notifications' property is not specified. Please check configuration file.");
			}
			this.notifications = Integer.parseInt(notifications);

			String monitExpr = p.getProperty("monitoring_expression");
			if (monitExpr == null || monitExpr.isEmpty()) {
				throw new MonitoringConfigurationException(
				        "'monitoring_expression' property is not specified. Please check configuration file.");
			}
			this.monitoringExpr = monitExpr;

			String addresses = p.getProperty("addresses");
			if (addresses == null || addresses.isEmpty()) {
				throw new MonitoringConfigurationException(
				        "'addresses' property is not specified. Please check configuration file.");
			}
			recipients.clear();

			StringTokenizer stAddresses = new StringTokenizer(addresses, ",");
			while (stAddresses.hasMoreTokens()) {
				String addressAsString = stAddresses.nextToken();
				recipients.add(addressAsString);
			}

			logger.info(this.getClass() + "'s configuration has been reloaded. New settings are : " + p);
		}
	}

	@Override
	public boolean validateNow() {
		String NotifyMassage = "";
		synchronized (this) {
			try {
				URL url = new URL(this.url + "/synapsoft/getServicesStatus");
				String userPassword = this.user + ":" + this.pass;
				String encoding = new sun.misc.BASE64Encoder().encode(userPassword.getBytes());
				URLConnection uc = url.openConnection();
				uc.setRequestProperty("Authorization", "Basic " + encoding);
				InputStream content = (InputStream) uc.getInputStream();
				DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
				DocumentBuilder builder = domFactory.newDocumentBuilder();
				Document doc = builder.parse(content);

				XPathFactory factory = XPathFactory.newInstance();
				XPath xpath = factory.newXPath();
				XPathExpression expr = xpath.compile("/xml/componentList/component[" + this.monitoringExpr
				        + "]/name/text()");

				Object result = expr.evaluate(doc, XPathConstants.NODESET);
				NodeList names = (NodeList) result;

				expr = xpath.compile("/xml/componentList/component[" + this.monitoringExpr + "]/details/text()");
				result = expr.evaluate(doc, XPathConstants.NODESET);
				NodeList details = (NodeList) result;

				HashMap<String, Long> tmpTimestamps = new HashMap<String, Long>();
				for (int i = 0; i < names.getLength(); i++) {
					if (this.timestamps.get(names.item(i).getNodeValue()) != null) {
						tmpTimestamps.put(names.item(i).getNodeValue(),
						        this.timestamps.get(names.item(i).getNodeValue()));
						if (this.timestamps.get(names.item(i).getNodeValue()) < System.currentTimeMillis()
						        - this.reportTimeThreshold * 1000) {
							NotifyMassage = NotifyMassage + "Specified object: " + names.item(i).getNodeValue()
							        + " is not started or not operational for more than " + this.reportTimeThreshold
							        + " seconds.\n" + "Details: " + details.item(i).getNodeValue() + "\n";
						}
					} else {
						tmpTimestamps.put(names.item(i).getNodeValue(), new Date().getTime());
					}
				}

				this.timestamps.clear();
				this.timestamps = tmpTimestamps;

			} catch (ConnectException e) {
				NotifyMassage = "Environment server is N/A: " + e.toString();
			} catch (IOException e) {
				NotifyMassage = "Check handlers.conf properties: " + e.toString();
			} catch (Exception e) {
				NotifyMassage = e.toString();
			}

			// If there are no errors, reset the notification counter, otherwise
			// increment it
			if (NotifyMassage.isEmpty()) {
				notificationCounter = 0;
			} else {
				notificationCounter++;

				if (notificationCounter <= notifications) {
					onFailNotifier.notify(newMessage(NotifyMassage));
					return false;
				}
			}

			return true;
		}
	}

	@Override
	public void setOnFailNotifier(Notifier notifier) {
		synchronized (this) {
			CompositeNotifier cn = new CompositeNotifier(true);
			cn.addNotifier(new DefaultNotifier());
			cn.addNotifier(notifier);
			this.onFailNotifier = cn;
		}

	}

	private class DefaultNotifier implements Notifier {

		public DefaultNotifier() {
		}

		@Override
		public boolean notify(final Message message) {
			if (notificationCounter > notifications) {
				// TODO : this is simpler yet than to involve State pattern
				// don't send notifications , it's enough
				return false;
			}

			logger.warn(message.getContent());
			return true;
		}

		@Override
		public void reload(final Properties p) {
		}
	}

	private Message newMessage(final String content) {
		return new Message(recipients, subject, content);
	}
}
