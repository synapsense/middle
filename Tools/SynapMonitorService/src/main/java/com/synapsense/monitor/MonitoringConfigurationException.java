package com.synapsense.monitor;

public class MonitoringConfigurationException extends RuntimeException {

	public MonitoringConfigurationException(final String message) {
		super(message, null);
	}

	public MonitoringConfigurationException(final String message, final Throwable e) {
		super(message, e);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1864805793938282718L;

}
