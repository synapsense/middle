package com.synapsense.monitor;

public interface Notifier extends ConfigurableComponent {
	boolean notify(final Message message);
}
