package com.synapsense.monitor.impl;

import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.StringTokenizer;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.security.auth.login.LoginException;

import org.apache.log4j.Logger;

import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.exception.EnvException;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.exception.PropertyNotFoundException;
import com.synapsense.monitor.CompositeNotifier;
import com.synapsense.monitor.Handler;
import com.synapsense.monitor.Message;
import com.synapsense.monitor.MonitoringConfigurationException;
import com.synapsense.monitor.Notifier;
import com.synapsense.service.Environment;
import com.synapsense.util.ServerLoginModule;

public class ESMonitor implements Handler {

	private final static Logger logger = Logger.getLogger(ESMonitor.class);

	/**
	 * Properties which are loaded from config file
	 */
	private String subject;
	private String user;
	private String pass;
	private int reportTimeThreshold;
	private List<String> recipients = new LinkedList<String>();
	private List<TO<?>> checkpoints = new LinkedList<TO<?>>();
	private int notifications;

	/**
	 * Local properties
	 */
	/**
	 * notification counter - to store how much
	 */
	private int notificationCounter = 0;
	private String providerUrl;
	private Environment env;

	// if not set externally , do nothing
	private Notifier onFailNotifier = new DefaultNotifier();

	public ESMonitor(final Properties properties) {
		reload(properties);
	}

	public boolean validateNow() {
		synchronized (this) {
			try {
				this.env = lookupEnv(this.providerUrl, this.user, this.pass);
				for (TO<?> id : checkpoints) {
					try {
						env.getPropertyValue(id, "lastValue", Double.class);
						if (env.getPropertyTimestamp(id, "lastValue") < System.currentTimeMillis()
						        - this.reportTimeThreshold * 1000) {
							notificationCounter++;
							onFailNotifier.notify(newMessage(id + " object did not report in the last "
							        + this.reportTimeThreshold + " seconds."));
							return false;
						}
					} catch (ObjectNotFoundException e) {
						notificationCounter++;
						onFailNotifier.notify(newMessage(id
						        + " object does not exist. Please check system configuration."));
						return false;
					} catch (PropertyNotFoundException e) {
						notificationCounter++;
						onFailNotifier.notify(newMessage(id + " objects has no 'lastValue' property. " + e));
						return false;
					} catch (EnvException e) {
						notificationCounter++;
						onFailNotifier.notify(newMessage("Generic Environment error: " + e.getMessage()));
						return false;
					}
				}
			} catch (Exception e) {
				notificationCounter++;
				onFailNotifier.notify(newMessage("Generic error occurred : " + e.getClass().getName() + " : "
				        + e.getMessage()));
				return false;
			}

			// TODO : this is simpler yet than to involve State pattern
			// if check was successful , reset counter
			if (notificationCounter >= notifications) {
				notificationCounter = 0;
			}
			return true;
		}
	}

	@Override
	public void setOnFailNotifier(final Notifier notifier) {
		synchronized (this) {
			CompositeNotifier cn = new CompositeNotifier(true);
			cn.addNotifier(new DefaultNotifier());
			cn.addNotifier(notifier);
			this.onFailNotifier = cn;
		}
	}

	@Override
	public void reload(final Properties p) {
		synchronized (this) {
			this.subject = p.getProperty("subject", "ES is not available");

			String url = p.getProperty("url");
			if (url == null || url.isEmpty()) {
				throw new MonitoringConfigurationException(
				        "'url' is does not specified. Please check configuration file.");
			}
			this.providerUrl = url;

			String userLogin = p.getProperty("user");
			if (userLogin == null || userLogin.isEmpty()) {
				throw new MonitoringConfigurationException(
				        "'user' property is not specified. Please check configuration file.");
			}
			this.user = userLogin;

			String userPassword = p.getProperty("password");
			if (userPassword == null || userPassword.isEmpty()) {
				throw new MonitoringConfigurationException(
				        "'password' property is not specified. Please check configuration file.");
			}
			this.pass = userPassword;

			String reportTime = p.getProperty("report_time_threshold");
			if (reportTime == null || reportTime.isEmpty()) {
				throw new MonitoringConfigurationException(
				        "'report_time_threshold' property is not specified. Please check configuration file.");
			}
			this.reportTimeThreshold = Integer.parseInt(reportTime);

			String notifications = p.getProperty("notifications");
			if (notifications == null || notifications.isEmpty()) {
				throw new MonitoringConfigurationException(
				        "'notifications' property is not specified. Please check configuration file.");
			}
			this.notifications = Integer.parseInt(notifications);

			String addresses = p.getProperty("addresses");
			if (addresses == null || addresses.isEmpty()) {
				throw new MonitoringConfigurationException(
				        "'addresses' property is not specified. Please check configuration file.");
			}
			recipients.clear();

			StringTokenizer stAddresses = new StringTokenizer(addresses, ",");
			while (stAddresses.hasMoreTokens()) {
				String addressAsString = stAddresses.nextToken();
				recipients.add(addressAsString);
			}

			String objectsToWatch = p.getProperty("checkpoints");
			if (objectsToWatch == null || objectsToWatch.isEmpty()) {
				throw new MonitoringConfigurationException(
				        "'checkpoints' property is not specified. Please check configuration file.");
			}
			checkpoints.clear();

			StringTokenizer stCheckpoints = new StringTokenizer(objectsToWatch, ",");
			while (stCheckpoints.hasMoreTokens()) {
				String toAsString = stCheckpoints.nextToken();
				checkpoints.add(TOFactory.getInstance().loadTO(toAsString));
			}

			logger.info(this.getClass() + "'s configuration has been reloaded. New settings are : " + p);
		}
	}

	@Override
	public String toString() {
		return "ES server monitor, provider url=" + this.providerUrl;
	}

	private Message newMessage(final String content) {
		return new Message(recipients, subject, content);
	}

	private Environment lookupEnv(final String url, final String u, final String p) throws NamingException,
	        LoginException {
		Properties environment = new Properties();
		environment.put(Context.INITIAL_CONTEXT_FACTORY, "org.jboss.naming.NamingContextFactory");
		environment.put(Context.URL_PKG_PREFIXES, "org.jboss.naming.client:org.jnp.interfaces");
		environment.put(Context.PROVIDER_URL, url);

		if (logger.isDebugEnabled()) {
			logger.debug("Looking up ES server reference from " + url + " using login " + u);
		}
		InitialContext ctx = new InitialContext(environment);
		Environment env = ServerLoginModule.getProxy(ctx, "SynapServer/Environment/remote", Environment.class);
		ServerLoginModule.login(u, p);

		return env;
	}

	private class DefaultNotifier implements Notifier {

		public DefaultNotifier() {
		}

		@Override
		public boolean notify(final Message message) {
			if (notificationCounter > notifications) {
				// TODO : this is simpler yet than to involve State pattern
				// don't send notifications , it's enough
				return false;
			}

			logger.warn(message.getContent());
			return true;
		}

		@Override
		public void reload(final Properties p) {
		}
	}

}
