package com.synapsense.monitor;

/**
 * Component handler.
 * 
 * @author ashabanov
 * 
 */
public interface Handler extends ConfigurableComponent {
	/**
	 * Validates state of handler.
	 * 
	 * @return flag of current state
	 */
	boolean validateNow();

	/**
	 * Updates "on fail" notifier reference.
	 * 
	 * @param notifier
	 *            Notifier instance
	 */
	void setOnFailNotifier(final Notifier notifier);
}
