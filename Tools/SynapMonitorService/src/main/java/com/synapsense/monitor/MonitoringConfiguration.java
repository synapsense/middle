package com.synapsense.monitor;

import java.io.File;
import java.net.URL;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.HierarchicalINIConfiguration;
import org.apache.commons.configuration.SubnodeConfiguration;
import org.apache.log4j.Logger;

public class MonitoringConfiguration {
	private final static Logger logger = Logger.getLogger(MonitoringConfiguration.class);

	private final static int DEFAULT_POLLTIME = 30;

	private Timer autoRefreshConfiguraion;

	private long configFileTimestamp;

	private int polltime = DEFAULT_POLLTIME;

	private String configDir;

	private File notifiersConfigFile;
	private File handlersConfigFile;

	private HierarchicalINIConfiguration handlersConfig;
	private HierarchicalINIConfiguration notifiersConfig;

	private Map<String, Handler> handlers = new LinkedHashMap<String, Handler>();
	private Map<String, Notifier> notifiers = new LinkedHashMap<String, Notifier>();

	public MonitoringConfiguration() {
		this("config");
	}

	public MonitoringConfiguration(final String dir) {
		this.configDir = dir;
		notifiersConfigFile = new File(configDir + "/notifiers.conf");
		handlersConfigFile = new File(configDir + "/handlers.conf");
		reload();
	}

	public void setAutoRefresh(final int seconds) {
		synchronized (this) {
			if (autoRefreshConfiguraion != null) {
				autoRefreshConfiguraion.cancel();
			}

			autoRefreshConfiguraion = new Timer("MonitoringConfigurationAutoRefresh", true);
			autoRefreshConfiguraion.schedule(new TimerTask() {
				@Override
				public void run() {
					long lastUpdate = Math.max(notifiersConfigFile.lastModified(), handlersConfigFile.lastModified());
					if (configFileTimestamp < lastUpdate) {
						try {
							reload();
							// catch to prevent thread stop
						} catch (MonitoringConfigurationException e) {
							logger.warn("Unable to reload configuration", e);
						}
					}
				}
			}, seconds * 1000, seconds * 1000);
		}
	}

	public Collection<Handler> getHandlers() {
		synchronized (this) {
			return Collections.unmodifiableCollection(handlers.values());
		}
	}

	public int getPolltime() {
		return polltime;
	}

	private void reload() throws MonitoringConfigurationException {
		synchronized (this) {
			try {
				logger.info("Reloading monitoring configuration...");
				try {
					handlersConfig = new HierarchicalINIConfiguration(handlersConfigFile);
				} catch (ConfigurationException e) {
					throw new MonitoringConfigurationException("Error occured during handlers initialization", e);
				}

				try {
					notifiersConfig = new HierarchicalINIConfiguration(notifiersConfigFile);
				} catch (ConfigurationException e) {
					throw new MonitoringConfigurationException("Error occured during notifiers initialization", e);
				}

				// loads main sections
				SubnodeConfiguration sbc = handlersConfig.getSection(null);
				if (!sbc.isEmpty()) {
					if (sbc.getInt("polltime") <= 0) {
						polltime = DEFAULT_POLLTIME;
					} else {
						polltime = sbc.getInt("polltime");
					}

				} else {
					logger.info("polltime is not specified. Default values will be used : polltime = " + polltime
					        + " sec");
				}

				if (handlersConfig.isEmpty() || notifiersConfig.isEmpty()) {
					logger.warn("Check configuration files. It appears that some of them are empty!");
					this.handlers.clear();
					this.notifiers.clear();
					return;
				}

				// removing components which was removed from configuration
				handlers.values().retainAll(handlersConfig.getSections());
				notifiers.values().retainAll(notifiersConfig.getSections());

				loadServices(handlersConfig, Handler.class, handlers);
				loadServices(notifiersConfig, Notifier.class, notifiers);

				CompositeNotifier compositeNotifier = new CompositeNotifier();
				for (Notifier n : notifiers.values()) {
					compositeNotifier.addNotifier(n);
				}

				for (Handler h : handlers.values()) {
					h.setOnFailNotifier(compositeNotifier);
				}

				configFileTimestamp = Math.max(handlersConfigFile.lastModified(), notifiersConfigFile.lastModified());

				logger.info("Monitoring configuration has been reloaded. " + this.toString());
			} catch (Exception e) {
				throw new MonitoringConfigurationException(
				        "Failed to reload monitoring configuration. Please check config files.", e);
			}
		}
	}

	@Override
	public String toString() {
		return "Configuration : " + handlers.size() + " handlers, " + notifiers.size() + " notifiers, polltime "
		        + polltime + " seconds, auto refresh is " + ((autoRefreshConfiguraion == null) ? "off" : "on");
	}

	private <T extends ConfigurableComponent> void loadServices(final HierarchicalINIConfiguration config,
	        final Class<T> clazz, final Map<String, T> containter) {
		for (Iterator<String> configIterator = config.getSections().iterator(); configIterator.hasNext();) {
			String section = configIterator.next();
			if (section == null) {
				continue;
			}

			SubnodeConfiguration subNode = config.getSection(section);

			Properties p = new Properties();
			Iterator<String> i = subNode.getKeys();
			while (i.hasNext()) {
				String key = i.next();
				String value = (String) subNode.getProperty(key);
				// TODO : by some reason , subNode.getKeys convert all '.' to
				// '..'
				key = key.replaceAll("\\.\\.", ".");
				p.put(key, value);
			}

			if (containter.containsKey(section)) {
				containter.get(section).reload(p);
			} else {
				try {
					Class<? extends T> c = (Class<? extends T>) Class.forName(subNode.getString("class"));
					containter.put(section, loadService(c, p));
				} catch (ClassNotFoundException e) {
					throw new MonitoringConfigurationException("There is no such service : "
					        + subNode.getString("class") + " in classpath!", e);
				}
			}
		}
	}

	private <T> T loadService(final Class<T> clazz, final Properties p) {
		try {
			T instance = clazz.getConstructor(Properties.class).newInstance(p);
			return instance;
		} catch (Exception e) {
			throw new MonitoringConfigurationException("There is no class : " + clazz
			        + " in classpath or instantiation exception has happened! Input properties : " + p, e);
		}
	}

}
