package com.synapsense.monitor;

import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

public class CompositeNotifier implements Notifier {
	private List<Notifier> notifiers;
	private boolean stopNotifyIfOneFail;

	public CompositeNotifier() {
		this(false);
	}

	/**
	 * Constructor with operation mode option
	 * 
	 * @param stopNotifyIfOneFail
	 *            Set to true if you want to stop executing of notification
	 *            chain at first notification failure
	 * 
	 */
	public CompositeNotifier(final boolean stopNotifyIfOneFail) {
		this.stopNotifyIfOneFail = stopNotifyIfOneFail;
		this.notifiers = new LinkedList<Notifier>();
	}

	public void addNotifier(final Notifier notifier) {
		notifiers.add(notifier);
	}

	@Override
	public boolean notify(final Message message) {
		boolean delivered = true;
		for (Notifier n : notifiers) {
			boolean lastNotification = n.notify(message);
			if (!lastNotification && !delivered)
				delivered = false;
			if (!lastNotification && stopNotifyIfOneFail)
				return false;
		}
		return delivered;
	}

	@Override
	public void reload(final Properties p) {
		for (Notifier n : notifiers) {
			n.reload(p);
		}
	}

}
