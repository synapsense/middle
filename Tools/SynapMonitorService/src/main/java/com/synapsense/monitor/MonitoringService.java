package com.synapsense.monitor;

import org.apache.log4j.Logger;

public class MonitoringService {

	private final static Logger logger = Logger.getLogger(MonitoringService.class);

	private MonitoringConfiguration configuration;

	public MonitoringService(final MonitoringConfiguration configuration) {
		this.configuration = configuration;
	}

	/**
	 * Actually start monitoring process
	 */
	public void startMonitoring() {
		logger.info("Starting monitoring components. Configuration : " + configuration.toString());
		while (true) {
			if (configuration.getHandlers().isEmpty()) {
				logger.warn("Monitor service is not configured.It is still working, just udpate configuration files");
			}

			for (Handler h : configuration.getHandlers()) {
				logger.info("Validating of " + h.toString());

				boolean isAvailable = h.validateNow();
				if (!isAvailable) {
					logger.warn(h.toString() + " reports : Failure");
				} else {
					logger.info(h.toString() + " reports : OK");
				}

			}

			try {
				logger.debug("Waiting for the next monitoring activity...");
				Thread.sleep(configuration.getPolltime() * 1000);
			} catch (InterruptedException e) {
				logger.warn("Monitor service has been interrupted. Stopping...");
				return;
			}
		}
	}

	/**
	 * internal monitor object. prevents java vm exit till host operation system
	 * stop signal happens
	 */
	private static Object lock = new Object();

	public static void main(final String[] args) throws InterruptedException {

		MonitoringConfiguration configuration = new MonitoringConfiguration();
		configuration.setAutoRefresh(5);

		final MonitoringService service = new MonitoringService(configuration);

		Thread mainService = new Thread(new Runnable() {
			@Override
			public void run() {
				service.startMonitoring();
			}
		}, "MonitorService_Worker_Thread");

		mainService.start();

		// infinite wait
		synchronized (lock) {
			lock.wait();
		}

	}

	/**
	 * Exits Java VM with exit code 0 <br>
	 * This method is called outside by host OS process
	 */
	public static void stop() {
		synchronized (lock) {
			lock.notifyAll();
		}
		System.exit(0);
	}

}
