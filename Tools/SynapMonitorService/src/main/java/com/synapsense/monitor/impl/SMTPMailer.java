package com.synapsense.monitor.impl;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.log4j.Logger;

import com.synapsense.monitor.Message;
import com.synapsense.monitor.Notifier;

public class SMTPMailer implements Notifier {

	private final static Logger logger = Logger.getLogger(SMTPMailer.class);

	private final static String DEFAULT_MESSAGE_TEMPLATE = "$message$";
	private final static String MESSAGE_MACRO = "$message$";

	private String mailFrom;
	private boolean auth = false;
	private String transport;
	private String host;
	private String port;
	private String user;
	private String password;
	private String template;
	private Properties javamailProperties;

	public SMTPMailer(final Properties settings) {
		reload(settings);
	}

	@Override
	public boolean notify(final Message message) {
		Session session = Session.getDefaultInstance(javamailProperties);
		Transport transport;
		try {
			transport = session.getTransport();
		} catch (NoSuchProviderException e) {
			logger.error("Mail transport protocol error. Check " + this.getClass() + "'s configuration", e);
			return false;
		}

		javax.mail.Message smtpMessage = new MimeMessage(session);
		try {
			smtpMessage.setFrom(new InternetAddress(mailFrom));
			smtpMessage.setSubject(message.getSubject());
			smtpMessage.setText(template.replace(MESSAGE_MACRO, message.getContent()));

			for (String nextRecipient : message.getRecipients()) {
				smtpMessage.addRecipient(javax.mail.Message.RecipientType.TO, new InternetAddress(nextRecipient));
			}

			if (user != null) {
				transport.connect(user, password);
			} else {
				transport.connect();
			}
			try {
				transport.sendMessage(smtpMessage, smtpMessage.getRecipients(javax.mail.Message.RecipientType.TO));
			} finally {
				transport.close();
			}

			logger.info("Notification has been sent to " + message.getRecipients());

		} catch (AddressException e) {
			logger.warn("Notification was not delivered because of address error", e);
			return false;
		} catch (MessagingException e) {
			logger.warn("Notification was not delivered", e);
			return false;
		}

		return true;
	}

	@Override
	public void reload(final Properties p) {
		javamailProperties = new Properties(p);
		mailFrom = javamailProperties.getProperty("mail.from");

		if (javamailProperties.getProperty("mail.smtp.user") != null) {
			user = javamailProperties.getProperty("mail.smtp.user");
			password = javamailProperties.getProperty("mail.smtp.password");
		} else if (javamailProperties.getProperty("mail.smtps.user") != null) {
			user = javamailProperties.getProperty("mail.smtps.user");
			password = javamailProperties.getProperty("mail.smtps.password");
		}

		String templateFileName = javamailProperties.getProperty("mail.template");
		template = DEFAULT_MESSAGE_TEMPLATE;
		if (templateFileName != null) {
			File messageTemplateFile = new File(templateFileName);

			if (messageTemplateFile.canRead()) {
				// read template file
				byte[] buffer = new byte[(int) messageTemplateFile.length()];
				FileInputStream f;
				try {
					f = new FileInputStream(messageTemplateFile);
					f.read(buffer);
				} catch (Exception e) {
					logger.error("Unable to read template file.", e);
				}
				template = new String(buffer);
			}
		}

		logger.info(this.getClass() + "'s configuration has been reloaded. New settings are : " + p);
	}
}
