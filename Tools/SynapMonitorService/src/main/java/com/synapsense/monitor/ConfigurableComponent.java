package com.synapsense.monitor;

import java.util.Properties;

/**
 * Configurable object interface.
 * 
 * @author ashabanov
 * 
 */
public interface ConfigurableComponent {

	/**
	 * Reloads configuration from given properties.
	 * 
	 * @param p
	 *            New properties
	 */
	void reload(Properties p);
}
