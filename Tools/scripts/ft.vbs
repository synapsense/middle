'----------------------------------------------------------------------------------------------------
' Usage:
'  ft.vbs <hostname> <ftp_user> <ftp_password> <db_user> <db_password> <dump 0|1|2> <DM logs 0|1> <ES logs 0|1> <LI logs 0|1>
' Example:
'  ft.vbs localhost user1 123 root dbuser 1 1 1 1
'----------------------------------------------------------------------------------------------------
tmpdir="synapsense-"&Month(Date)&Day(Date)&Year(Date) 
host="localhost"
user="user1"
password="123"
dbu="root"
dbp="dbuser"
fd=0
sDump=tmpdir&"\dump.sql"
zf=""
Dim cmd,dmpcmd(3),ftpcmds,tmpfile,tmpfilestr
Dim fs,sh,IEA,IEDoc
Dim fls(3),pathstoapps,logType,slo
pathstoapps=Array("C:\Program Files\SynapSense\SynapDM\logs","..\server\default\log","C:\Program Files\SynapSense\SynapSense LiveImaging Server\logs")
logType=detectEngine
parseArgs
init
prepareData
sendByFtp
If(logType=0) Then
	sh.Popup "Work is completed. See log file - "&tmpfile,5,"The end of process",64
Else
	WScript.Echo "Work is completed. See log file - "&tmpfile,5,"The end of process"
End If
clean
Function detectEngine
	cscrf=0
	If "CSCRIPT.EXE" = UCase( Right( WScript.Fullname, 11 ) ) Then
		cscrf=1
	End If 
	detectEngine=cscrf
End function
Sub parseArgs
	Set args=WScript.Arguments
	If(args.Count<9) Then
		WScript.Echo " Usage:"&vbCrLf& _
"     ft.vbs <ftp_hostname> <ftp_user> <ftp_password> <db_user> <db_password> <dump 0|1|2> <DM logs 0|1> <ES logs 0|1> <LI logs 0|1>"&vbCrLf& _
"	0 - disable"&vbCrLf&"	1 - enable"&vbCrLf&"	2 - today's data only (db dump)"&vbCrLf&vbCrLf& _
" Example:"&vbCrLf& _
"     ft.vbs localhost user1 123 root dbuser 1 1 1 1"

		WScript.Quit -1
	Else
		host=args(0)
		user=args(1)
		password=args(2)
		dbu=args(3)
		dbp=args(4)
		fd=args(5)
		fl=0
		For i=0 To 2
			fls(i)=args(6+i)
			fl=fl+fls(i)
		Next
		If(fl=0 And fd=0) Then	
			CreateObject("WScript.Shell").Popup "Work is not required",5,"The end of process",64
			WScript.Quit -2
		End if
	End If
End sub
Sub sendByFtp
	lprintln "Sending "&tmpdir&".zip to "&host&" by ftp"
	lprintln "Command "&cmd
	Set shExec = sh.Exec(cmd)
	Set txtStream = shExec.StdIn
	Set errStream = shExec.StdErr
	txtStream.Write(ftpcmds)
	lprintln "FTP Commands:<BR>"&Replace(ftpcmds,vbCrLf,"<BR>")
	Do While shExec.Status = 0
		WScript.Sleep 1000
		lprint "#"
	Loop
	lprint "<BR>"
	While Not errStream.AtEndOfStream
   		str=errStream.ReadLine
   		lprintln str
	Wend
End sub
Sub prepareData
	ftpcmds="user "&user&" "&password&vbCrLf
	fl=0
	For i=0 To UBound(fls)
		fl=fl+fls(i)
	Next
	If(fl<>0) Then 
		lprintln "Copying logs..."
		writeLog
	End If
	If(fd<>0) Then 
		lprintln "Create dump"
		writeDump
	End If
	lprintln "Compressing..."
	zf=createZip(tmpdir)
	ftpcmds=ftpcmds&"put "&Chr(34)&zf&Chr(34)&vbCrLf
	ftpcmds=ftpcmds&"quit"&vbCrLf
End Sub
Sub writeDump
	Dim shExec,txtStream,errStream
	a=0
	b=0
	If(fd=2) Then
		a=1
		b=2
	End if
	For i=a To b
		Set shExec = sh.Exec(dmpcmd(i))
		Set errStream = shExec.StdErr
		str=vbNullString
		While Not errStream.AtEndOfStream
   			str=errStream.ReadLine
   			lprintln str
		Wend
		Do While shExec.Status = 0
			WScript.Sleep 1000
		Loop
	Next	
End Sub
Function getLastEl(str,n)
	Dim w
	w=Split(str,"\")
	getLastEl=w(UBound(w)-n)
End function
Sub writeLog
	For i=0 To UBound(fls)
		If(fls(i)<>0) Then
			fn=getLastEl(pathstoapps(i),1)
			fn1=getLastEl(pathstoapps(i),0)
			If(not fs.FolderExists(tmpdir&"\"&fn)) Then fs.CreateFolder(tmpdir&"\"&fn)
			lprintln "Copying "& pathstoapps(i)
			If(fs.FolderExists(pathstoapps(i))) Then fs.CopyFolder pathstoapps(i),tmpdir&"\"&fn&"\"&fn1, True
		End if
	Next
End Sub
Function createZip(dir)
	Set shell=CreateObject("Shell.Application")
	zipFile = dir&".zip"
	zipHeader="PK" & Chr(5) & Chr(6) & String(18,0)
	Set strOut=fs.CreateTextFile(zipFile,true)
	strOut.Write(zipHeader)
	strOut.Close
	Set arFolder = shell.NameSpace(fs.GetAbsolutePathName(zipFile))
	Set dirf  = shell.NameSpace(fs.GetAbsolutePathName(dir))
	arFolder.CopyHere fs.GetAbsolutePathName(dir)
	Do Until arFolder.Items.Count = 1
        WScript.Sleep 1000
        lprint "#"
    Loop
    lprint "<BR>"
	WScript.Sleep(500)
	createZip=fs.GetAbsolutePathName(zipFile)
End Function
Sub init
	Set fs = CreateObject("Scripting.FileSystemObject")
	Set sh=CreateObject("WScript.Shell")
	linit
	lprintln "Initialize..."
	initPaths
	dmpcmd(0)="mysqldump -q --single-transaction -u "&dbu&" -p"&dbp&" -r "&tmpdir&"\tables.sql synap"
	dmpcmd(1)="mysqldump -q --single-transaction -u "&dbu&" -p"&dbp&" --extended-insert synap -r "&tmpdir&"\tables.sql --ignore-table=synap.tbl_sensor_data --ignore-table=synap.historical_values"
	dmpcmd(2)="mysqldump -q --single-transaction -u "&dbu&" -p"&dbp&" --extended-insert synap -r "&tmpdir&"\sensor_data.sql --tables tbl_sensor_data --where="&chr(34)&"data_time_stamp>'"&Year(Date)&"-"&Month(Date)&"-"&Day(Date)&" 00:00:00'"&chr(34)
	cmd="ftp -n "&host
	If(Not fs.FolderExists(tmpdir)) Then
		fs.CreateFolder(tmpdir)
	End if
End Sub
Sub clean
	If(fs.FolderExists(tmpdir)) Then fs.DeleteFolder tmpdir,True
	If(fs.FileExists(zf)) Then fs.DeleteFile zf,True
	lclean
End sub
Sub initPaths
	Err.Clear
	On Error Resume Next
	Dim w
	w=Split(fs.GetParentFolderName(sh.RegRead("HKEY_LOCAL_MACHINE\SYSTEM\ControlSet001\Services\synapdm\ImagePath"))&"\logs",Chr(34))
	pathstoapps(0)=w(UBound(w))
	w=Split(fs.GetParentFolderName(sh.RegRead("HKEY_LOCAL_MACHINE\SYSTEM\ControlSet001\Services\hpiedevmgr\ImagePath"))&"\logs",Chr(34))
	pathstoapps(0)=w(UBound(w))
	w=Split(fs.GetParentFolderName(sh.RegRead("HKEY_LOCAL_MACHINE\SYSTEM\ControlSet001\Services\SYNAPSERVER\ImagePath"))&"\..\server\default\log",Chr(34))
	pathstoapps(1)=w(UBound(w))
	w=Split(fs.GetParentFolderName(sh.RegRead("HKEY_LOCAL_MACHINE\SYSTEM\ControlSet001\Services\HPIEAPPSVR\ImagePath"))&"\..\server\default\log",Chr(34))
	pathstoapps(1)=w(UBound(w))
	w=Split(fs.GetParentFolderName(sh.RegRead("HKEY_LOCAL_MACHINE\SYSTEM\ControlSet001\Services\synapviz\ImagePath"))&"\logs",Chr(34))
	pathstoapps(2)=w(UBound(w))
	w=Split(fs.GetParentFolderName(sh.RegRead("HKEY_LOCAL_MACHINE\SYSTEM\ControlSet001\Services\hpieimgsvr\ImagePath"))&"\logs",Chr(34))
	pathstoapps(2)=w(UBound(w))
End Sub
Sub linit
	If(logType=0) Then
		tmpfile = fs.GetAbsolutePathName(tmpdir&"-log.html")
		Set tmpfilestr=fs.CreateTextFile(tmpfile, True)
		Set IEA = CreateObject("InternetExplorer.Application")
		IEA.Offline = True
		IEA.AddressBar = False
		IEA.Height = 480
		IEA.Width = 640
		IEA.MenuBar = False
		IEA.StatusBar = False
		IEA.Silent = True
		IEA.ToolBar = False
		IEA.Navigate "file://" & Replace(tmpfile,"\","/")
		Do While IEA.Busy
			WScript.Sleep 100
		Loop
		Set IEDoc = Nothing
		Do Until Not IEDoc Is Nothing
			WScript.Sleep 100
			Set IEDoc = IEA.Document
		Loop
		IEA.Visible = True			
		IEDoc.Open
		IEDoc.Write "<html><head><title>"
		IEDoc.Write "Progress...   "
		IEDoc.Write "</title></head><body>"
		tmpfilestr.Write "<html><head><title>"
		tmpfilestr.Write "Progress...   "
		tmpfilestr.Write "</title></head><body>"
		lprintln "Log file: "&tmpfile
	Else
			tmpfile = fs.GetAbsolutePathName(tmpdir&"-log.txt")
			Set tmpfilestr=fs.CreateTextFile(tmpfile, True)
	End If
End Sub
Sub lclean
	If(logType=0) Then
		IEDoc.Write "</body></html>"
		IEDoc.Close
		IEA.Quit
		tmpfilestr.Write "</body></html>"
	End If
	tmpfilestr.Close
End Sub
Sub lprintln(str)
	If(logType=0) Then
		IEDoc.Write "<b>"&Date()&" "&Time()&"</b>"&" -- "&str&"<BR>"
		tmpfilestr.Write "<b>"&Date()&" "&Time()&"</b>"&" -- "&str&"<BR>"
	Else
		WScript.StdOut.WriteLine Date()&" "&Time()&" -- "&Replace(str,"<BR>",vbCrLf)&vbCrLf
		tmpfilestr.Write Date()&" "&Time()&" -- "&Replace(str,"<BR>",vbCrLf)&vbCrLf
	End If
End Sub
Sub lprint(str)
	If(logType=0) Then
		IEDoc.Write str
		tmpfilestr.Write str
	Else
		WScript.StdOut.Write Replace(str,"<BR>",vbCrLf)
		tmpfilestr.Write Replace(str,"<BR>",vbCrLf)
	End If
End Sub
