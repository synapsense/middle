package com.synapsense.watchdog.remote;

import javax.management.JMRuntimeException;

public class MBeanProxyException extends JMRuntimeException {

	private static final long serialVersionUID = 1645758979897076375L;

	private final Exception exception;

	public MBeanProxyException() {
		this(null, null);
	}

	public MBeanProxyException(String message) {
		this(message, null);
	}

	public MBeanProxyException(Exception exception) {
		this(null, exception);
	}

	public MBeanProxyException(String message, Exception exception) {
		super(message);
		this.exception = exception;
	}

	public Throwable getCause() {
		return exception;
	}
}
