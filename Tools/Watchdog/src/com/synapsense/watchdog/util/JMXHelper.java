package com.synapsense.watchdog.util;

import java.io.File;

import javax.management.MBeanServer;
import javax.management.ObjectName;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;

import com.sun.tools.attach.VirtualMachine;
import com.synapsense.watchdog.remote.MBeanProxy;

public class JMXHelper {

	private static final String CONNECTOR_ADDRESS = "com.sun.management.jmxremote.localConnectorAddress";

	public static String getConnectorAddress(String jvmRef) throws Exception {

		// check whether we already have the address
		if (jvmRef.startsWith("service:jmx:"))
			return jvmRef;

		// attach to the target application
		VirtualMachine vm = VirtualMachine.attach(jvmRef);

		// get the connector address
		String connectorAddress = vm.getAgentProperties().getProperty(CONNECTOR_ADDRESS);

		// no connector address, so we start the JMX agent
		if (connectorAddress == null) {
			String agent = vm.getSystemProperties().getProperty("java.home") + File.separator + "lib" + File.separator
			        + "management-agent.jar";
			vm.loadAgent(agent);

			// agent is started, get the connector address
			connectorAddress = vm.getAgentProperties().getProperty(CONNECTOR_ADDRESS);
			assert connectorAddress != null;
		}
		return connectorAddress;
	}

	public static void installMBeanProxy(MBeanServer server, String remoteUrl, String remoteName, String localName)
	        throws Exception {
		JMXServiceURL url = new JMXServiceURL(getConnectorAddress(remoteUrl));
		JMXConnector c = JMXConnectorFactory.connect(url);

		MBeanProxy proxy = new MBeanProxy(new ObjectName(remoteName), c.getMBeanServerConnection());
		server.registerMBean(proxy, new ObjectName(localName));
	}

}
