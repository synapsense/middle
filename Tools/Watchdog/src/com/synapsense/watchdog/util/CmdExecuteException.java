package com.synapsense.watchdog.util;

public class CmdExecuteException extends Exception {

	private static final long serialVersionUID = 4864435615531485067L;

	public CmdExecuteException() {
		super();
	}

	public CmdExecuteException(String message, Throwable cause) {
		super(message, cause);
	}

	public CmdExecuteException(String message) {
		super(message);
	}

	public CmdExecuteException(Throwable cause) {
		super(cause);
	}

}
