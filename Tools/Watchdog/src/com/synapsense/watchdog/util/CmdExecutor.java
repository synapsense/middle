package com.synapsense.watchdog.util;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;

public class CmdExecutor {

	static final String LINE_SEPARATOR = System.getProperty("line.separator");

	public static CmdExecuteResult execute(String... cmd) throws CmdExecuteException {

		CmdExecuteResult result = new CmdExecuteResult();

		try {
			ProcessBuilder pb = new ProcessBuilder(cmd);
			pb.redirectErrorStream(true);
			Process proc = pb.start();
			InputStream stdout = proc.getInputStream();
			InputStreamReader isr = new InputStreamReader(stdout);
			BufferedReader br = new BufferedReader(isr);
			StringWriter output = new StringWriter();
			String line = null;
			while ((line = br.readLine()) != null)
				output.append(line).append(LINE_SEPARATOR);
			result.retVal = proc.waitFor();
			result.output = output.toString();
		} catch (Throwable t) {
			throw new CmdExecuteException("Cannot execute \"" + strArrayToStr(cmd) + "\"", t);
		}
		return result;
	}

	private static String strArrayToStr(String... strings) {
		StringBuilder sb = new StringBuilder();
		for (String str : strings) {
			sb.append(str).append(' ');
		}
		return sb.toString();
	}

	public static void main(String[] args) throws Exception {
		// CmdExecuteResult res = execute("sc", "query", "synapdms");
		CmdExecuteResult res = execute("net", "stop", "synapdm");
		System.out.println(res.output);
		System.out.println("Exit code: " + res.retVal);
	}

}
