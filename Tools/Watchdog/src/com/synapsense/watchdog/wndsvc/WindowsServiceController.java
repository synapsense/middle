package com.synapsense.watchdog.wndsvc;

import javax.management.JMException;
import javax.management.MXBean;

@MXBean
public interface WindowsServiceController {

	String getServiceName();

	void setServiceName(String name);

	String getStatus() throws JMException;

	int getServicePid() throws JMException;

	long getPollPeriod();

	void setPollPeriod(long period);

	void startService() throws JMException;

	void stopService() throws JMException;

}
