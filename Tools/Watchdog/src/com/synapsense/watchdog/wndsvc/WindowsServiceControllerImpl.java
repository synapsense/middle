package com.synapsense.watchdog.wndsvc;

import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.management.AttributeChangeNotification;
import javax.management.JMException;
import javax.management.MBeanException;
import javax.management.MBeanNotificationInfo;
import javax.management.NotificationBroadcasterSupport;

import com.synapsense.watchdog.util.CmdExecuteException;
import com.synapsense.watchdog.util.CmdExecuteResult;
import com.synapsense.watchdog.util.CmdExecutor;

public class WindowsServiceControllerImpl extends NotificationBroadcasterSupport implements WindowsServiceController {

	private static final Logger log = Logger.getLogger(WindowsServiceControllerImpl.class.getName());

	private static final String LINE_SEPARATOR = System.getProperty("line.separator");

	private static final Pattern PID_PATTERN = Pattern.compile(".*PID\\s*:\\s(\\d+)");

	private static final MBeanNotificationInfo SUPPORTED_NOTIFICATIONS = new MBeanNotificationInfo(
	        new String[] { AttributeChangeNotification.ATTRIBUTE_CHANGE }, AttributeChangeNotification.class.getName(),
	        "Notification on changes in controlled service attributes");

	private String serviceName = "";

	private String status = "";
	private Integer pid;

	private Timer timer;
	private long period;

	public WindowsServiceControllerImpl() {
		super(SUPPORTED_NOTIFICATIONS);
	}

	@Override
	public String getServiceName() {
		return serviceName;
	}

	@Override
	public void setServiceName(String name) {
		if (name == null)
			throw new IllegalArgumentException("Service name cannot be null");
		serviceName = name;
	}

	@Override
	public long getPollPeriod() {
		return period;
	}

	@Override
	public void setPollPeriod(long period) {
		if (period < 0)
			throw new IllegalArgumentException("Period must be non-negative");
		if (period == 0) {
			if (timer != null) {
				timer.cancel();
				timer = null;
			}
			this.period = period;
			return;
		}

		// TODO instead of canceling the whole timer track and cancel TimerTask
		if (timer != null) {
			timer.cancel();
		}
		timer = new Timer("Status poller for " + getServiceName() + " service", true);

		timer.scheduleAtFixedRate(new TimerTask() {

			@Override
			public void run() {
				try {
					getServicePid();
					// getStatus(); since getServicePid calls getStatus no need
					// to do it twice
				} catch (Throwable t) {
					// ignore that
				}
			}

		}, 0, period);
		this.period = period;
	}

	public String getStatus() throws JMException {
		if (log.isLoggable(Level.FINE))
			log.fine("getStatus for " + getServiceName());
		CmdExecuteResult res;
		try {
			res = CmdExecutor.execute("sc", "queryex", serviceName);
		} catch (CmdExecuteException e) {
			status = null;
			throw new MBeanException(e, "Cannot query status");
		}
		String old = status;
		status = res.output;
		if (old == res.output) {// if both are nulls (or unlikely the same ref)
			if (log.isLoggable(Level.FINE))
				log.fine("no change in status for " + getServiceName());
			return old;
		}

		if ((old == null && res.output != null) || (old != null && res.output == null) || !old.equals(res.output)) {
			AttributeChangeNotification notif = new AttributeChangeNotification(this, 0, System.currentTimeMillis(),
			        "Attribute change", "status", "String", old, status);
			if (log.isLoggable(Level.FINE))
				log.fine("status has changed for " + getServiceName());
			sendNotification(notif);
		} else if (log.isLoggable(Level.FINE)) {
			log.fine("no change in status for " + getServiceName());
		}

		return status;
	}

	@Override
	public int getServicePid() throws JMException {
		if (log.isLoggable(Level.FINE))
			log.fine("getServicePid for " + getServiceName());
		Matcher matcher = PID_PATTERN.matcher(getStatus());
		if (matcher.find()) {
			Integer old = pid;
			pid = Integer.parseInt(matcher.group(1));
			if (!pid.equals(old)) {
				AttributeChangeNotification notif = new AttributeChangeNotification(this, 0,
				        System.currentTimeMillis(), "Attribute change", "servicePid", "int", old, pid);
				if (log.isLoggable(Level.FINE))
					log.fine("pid has changed for " + getServiceName());
				sendNotification(notif);
			} else if (log.isLoggable(Level.FINE)) {
				log.fine("no change in pid for " + getServiceName());
			}

			return pid;
		}
		pid = null;
		throw new JMException("Cannot retrieve PID for service \"" + serviceName + "\"");
	}

	@Override
	public void startService() throws JMException {
		CmdExecuteResult res;
		try {
			res = CmdExecutor.execute("net", "start", serviceName);
		} catch (CmdExecuteException e) {
			throw new MBeanException(e, "Cannot start service \"" + serviceName + "\"");
		}
		if (res.retVal != 0) {
			throw new JMException("Cannot start service \"" + serviceName + "\":" + LINE_SEPARATOR + res.output);
		}
	}

	@Override
	public void stopService() throws JMException {
		CmdExecuteResult res;
		try {
			res = CmdExecutor.execute("net", "stop", serviceName);
		} catch (CmdExecuteException e) {
			throw new MBeanException(e, "Cannot stop service \"" + serviceName + "\"");
		}
		if (res.retVal != 0) {
			throw new JMException("Cannot stop service \"" + serviceName + "\":" + LINE_SEPARATOR + res.output);
		}
	}

}
