package com.synapsense.watchdog;

import java.beans.XMLDecoder;
import java.io.InputStream;
import java.io.Serializable;
import java.lang.management.ManagementFactory;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;

import javax.management.MBeanServer;
import javax.management.ObjectName;

public class Starter {

	private static final Logger log = Logger.getLogger(Starter.class.getName());

	private static final String JMX_DOMAIN = "com.synapsense.watchdog";

	private static final Object _lock = new Object();

	public static void main(String args[]) throws Exception {

		log.info("Starting watchdog service");

		for (Config.SvcParams svcParams : Config.readConfig()) {
			createWatchdog(svcParams);
		}

		log.info("Watchdog service started");

		// Thread.sleep(Long.MAX_VALUE);

		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				log.info("Stopping watchdog service");
				synchronized (_lock) {
					_lock.notifyAll();
				}
			}
		});

		try {
			synchronized (_lock) {
				_lock.wait();
			}
		} catch (InterruptedException e) {
			// This exception is by design and not logged
		}
		log.info("Watchdog service stopped");
	}

	public static void stop() {
		log.info("Stopping watchdog service");
		synchronized (_lock) {
			_lock.notifyAll();
		}
	}

	private static void createWatchdog(Config.SvcParams svcParams) throws Exception {
		MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
		log.info("Installing watchdog for " + svcParams.getName());

		WatchdogImpl watchdog = new WatchdogImpl(JMX_DOMAIN);
		watchdog.setServiceName(svcParams.getName());
		watchdog.setPeriod(svcParams.getPeriod());
		watchdog.setThreshold(svcParams.getThreshold());
		String watchdogName = JMX_DOMAIN + ":type=Watchdog,service=" + svcParams.getName();
		mbs.registerMBean(watchdog, new ObjectName(watchdogName));
		watchdog.start();
		log.info("Watchdog for " + svcParams.getName() + " installed, object name \"" + watchdogName + "\"");
	}

}

class Config {

	private static final Logger log = Logger.getLogger(Config.class.getName());

	private static final String CONF_NAME = "watchdog-conf.xml";

	public static class SvcParams implements Serializable {
		/**
		 * 
		 */
		private static final long serialVersionUID = -3880300924161098245L;

		private String name;
		private Float threshold;
		private Long period;

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public Float getThreshold() {
			return threshold;
		}

		public void setThreshold(Float threshold) {
			this.threshold = threshold;
		}

		public Long getPeriod() {
			return period;
		}

		public void setPeriod(Long period) {
			this.period = period;
		}
	}

	public static List<SvcParams> readConfig() throws Exception {
		List<SvcParams> res = new LinkedList<SvcParams>();
		InputStream stream = Thread.currentThread().getContextClassLoader().getResourceAsStream(CONF_NAME);
		if (stream == null)
			throw new Exception("Cannot find " + CONF_NAME + " in the classpath");
		XMLDecoder decoder = new XMLDecoder(stream);
		try {
			while (true) {
				res.add(check((SvcParams) decoder.readObject()));
			}
		} catch (ArrayIndexOutOfBoundsException exception) {
		} finally {
			decoder.close();
		}
		return res;
	}

	private static SvcParams check(SvcParams params) throws Exception {
		if (params.getName() == null || params.getName().isEmpty())
			throw new Exception("Found service descriptor with no service name");

		if (params.getThreshold() == null) {
			params.setThreshold(90.0f);
		} else if (params.getThreshold() < 0f || params.getThreshold() > 100f) {
			throw new Exception("Service " + params.getName() + ": memory threshold must be between 0 and 100 percents");
		}

		if (params.getPeriod() == null) {
			params.setPeriod((long) 1000 * 60);// 1 min
		} else if (params.getPeriod() <= 0) {
			throw new Exception("Service " + params.getName() + ": check period must be positive integer");
		}

		log.info("Read service config: name=\"" + params.getName() + "\", threshold=" + params.getThreshold()
		        + "%, period=" + params.getPeriod());
		return params;
	}

}