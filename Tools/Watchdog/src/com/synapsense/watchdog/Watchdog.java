package com.synapsense.watchdog;

import javax.management.JMException;
import javax.management.MXBean;

/**
 * Watchdog service JMX interface.
 * 
 * @author Oleg Stepanov
 * 
 */
@MXBean
public interface Watchdog {
	String getServiceName();

	void setServiceName(String name) throws JMException;

	float getThreshold();

	void setThreshold(float threshold);

	long getPeriod();

	void setPeriod(long period);

	void start() throws JMException;

	void stop() throws JMException;

	boolean isActive();
}
