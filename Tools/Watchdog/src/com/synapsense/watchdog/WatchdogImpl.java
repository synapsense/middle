package com.synapsense.watchdog;

import java.lang.management.ManagementFactory;
import java.lang.management.MemoryMXBean;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.management.AttributeChangeNotification;
import javax.management.JMException;
import javax.management.MBeanException;
import javax.management.MBeanRegistration;
import javax.management.MBeanServer;
import javax.management.Notification;
import javax.management.NotificationListener;
import javax.management.ObjectName;
import javax.management.monitor.CounterMonitor;
import javax.management.monitor.MonitorNotification;

import com.synapsense.watchdog.util.JMXHelper;
import com.synapsense.watchdog.wndsvc.WindowsServiceControllerImpl;

public class WatchdogImpl implements Watchdog, MBeanRegistration {
	private static final Logger log = Logger.getLogger(WatchdogImpl.class.getName());

	private static final String WND_CONTROLLER_NAME = "%1$s:type=WindowsServiceController,service=%2$s";
	private static final String MEMORY_MXBEAN_PROXY_NAME = "%1$s:type=MemoryMXBeanProxy,service=%2$s,pid=%3$s";
	private static final String COUNTER_MONITOR_NAME = "%1$s:type=CounterMonitor,service=%2$s";

	private static final long CONTROLLER_POLL_PERIOD = 5000; // 5 sec

	private String domainName;
	private String serviceName;

	private String wndSvcControllerName;
	private WindowsServiceControllerImpl wndSvcController;

	private String memBeanProxyName;
	private MemoryMXBean memoryMXBean;

	private String counterMonitorName;
	private CounterMonitor counterMonitor;

	private float threshold = 80f;

	private long period;

	private MBeanServer mbs;

	private boolean active;

	private NotificationListener controllerListener = new NotificationListener() {

		@Override
		public void handleNotification(Notification notification, Object handback) {
			if (notification instanceof AttributeChangeNotification) {
				AttributeChangeNotification notif = (AttributeChangeNotification) notification;
				if ("servicePid".equals(notif.getAttributeName())) {
					if (notif.getNewValue() != null && !notif.getNewValue().equals(new Integer(0))) {
						// service has been started
						log.info("Service " + getServiceName() + " has been started, activating memory monitor");
						try {
							disableMonitor();// for the case when service was
											 // restarted
							enableMonitor();
						} catch (Exception e) {
							log.log(Level.WARNING, "Cannot activate monitor for " + getServiceName(), e);
						}
					} else {
						log.info("Service " + getServiceName() + " has been stopped, disabling memory monitor");
						try {
							disableMonitor();
						} catch (Exception e) {
							log.log(Level.WARNING, "Cannot disable monitor for " + getServiceName(), e);
						}
					}
				}
			}
		}
	};

	private NotificationListener counterMonitorListener = new NotificationListener() {

		@Override
		public void handleNotification(Notification notification, Object handback) {
			if (MonitorNotification.THRESHOLD_VALUE_EXCEEDED.equals(notification.getType())) {
				log.info(String.format(
				        "Service %1$s has exceeded threshold of %2$d bytes (%3$.2f%% of %4$d). Restarting...",
				        getServiceName(), counterMonitor.getInitThreshold(), threshold, memoryMXBean
				                .getHeapMemoryUsage().getMax()));
				try {
					wndSvcController.stopService();
					wndSvcController.startService();
				} catch (Throwable t) {
					log.log(Level.WARNING, "Exception caught during " + getServiceName() + " restart", t);
					return;
				}
				log.info("Service " + getServiceName() + " restarted");
			}
		}

	};

	public WatchdogImpl(String domainName) {
		if (domainName == null || domainName.isEmpty())
			throw new IllegalArgumentException("JMX domain name must be not empty");
		this.domainName = domainName;
	}

	@Override
	public long getPeriod() {
		if (counterMonitor != null)
			period = counterMonitor.getGranularityPeriod();
		return period;
	}

	@Override
	public void setPeriod(long period) {
		if (period <= 0)
			throw new IllegalArgumentException("Period must be positive");
		if (counterMonitor != null)
			counterMonitor.setGranularityPeriod(period);
		this.period = period;
	}

	@Override
	public String getServiceName() {
		return serviceName;
	}

	@Override
	public void setServiceName(String name) throws JMException {
		if (name == null || name.isEmpty())
			throw new IllegalArgumentException("Service name must be not empty");
		if (isActive())
			stop();
		serviceName = name;
		// TODO changing name means we need to re-register all the beans...
	}

	@Override
	public float getThreshold() {
		return threshold;
	}

	@Override
	public void setThreshold(float threshold) {
		if (threshold < 0f || threshold > 100f)
			throw new IllegalArgumentException("Memory threshold must be between 0 and 100 percents");
		this.threshold = threshold;
		recalculateTreshold();
	}

	@Override
	public boolean isActive() {
		return active;
	}

	@Override
	public void start() throws JMException {
		if (isActive())
			throw new JMException("Watchdog is already started");
		if (mbs == null)
			throw new JMException("Watchdog is not registered in MBeanService");

		if (counterMonitor == null) { // create new one
			counterMonitorName = String.format(COUNTER_MONITOR_NAME, domainName, serviceName);
			CounterMonitor monitor = new CounterMonitor();
			mbs.registerMBean(monitor, new ObjectName(counterMonitorName));
			counterMonitor = monitor;
			counterMonitor.setGranularityPeriod(period);
			counterMonitor.setNotify(true);
			counterMonitor.addNotificationListener(counterMonitorListener, null, null);
			log.info("Counter monitor installed, object name \"" + counterMonitorName + "\"");
		}

		if (wndSvcController == null) { // create new one
			wndSvcControllerName = String.format(WND_CONTROLLER_NAME, domainName, serviceName);
			WindowsServiceControllerImpl controller = new WindowsServiceControllerImpl();
			controller.setServiceName(serviceName);
			mbs.registerMBean(controller, new ObjectName(wndSvcControllerName));
			wndSvcController = controller;
			wndSvcController.setPollPeriod(CONTROLLER_POLL_PERIOD);
			wndSvcController.addNotificationListener(controllerListener, null, null);
			log.info("Windows service controller installed, object name \"" + wndSvcControllerName + "\"");
		}

		if (wndSvcController.getServicePid() != 0)
			enableMonitor();

		active = true;
	}

	private void enableMonitor() throws JMException {
		int pid = wndSvcController.getServicePid();
		if (pid == 0)// service is not running
			throw new JMException("Cannot create heap monitor for " + getServiceName() + ": service is not running");

		if (memoryMXBean != null)
			return;// already activated

		memBeanProxyName = String.format(MEMORY_MXBEAN_PROXY_NAME, domainName, getServiceName(), Integer.toString(pid));

		try {
			JMXHelper.installMBeanProxy(mbs, Integer.toString(pid), ManagementFactory.MEMORY_MXBEAN_NAME,
			        memBeanProxyName);
			memoryMXBean = ManagementFactory.newPlatformMXBeanProxy(mbs, memBeanProxyName, MemoryMXBean.class);
			log.info("MemoryMXBean proxy for " + getServiceName() + " installed, object name \"" + memBeanProxyName);
		} catch (Exception e) {
			throw new MBeanException(e, "Cannot create memory bean proxy for service " + getServiceName() + ", pid="
			        + pid);
		}
		recalculateTreshold();

		// set counter to monitor memory
		counterMonitor.addObservedObject(new ObjectName(memBeanProxyName));
		counterMonitor.setObservedAttribute("HeapMemoryUsage.used");
		counterMonitor.start();
	}

	private void recalculateTreshold() {
		if (counterMonitor == null || memoryMXBean == null)
			return;
		// calculate memory threshold
		long maxMem = memoryMXBean.getHeapMemoryUsage().getMax();
		long threshold = (long) (maxMem / 100f * getThreshold());
		counterMonitor.setInitThreshold(new Long(threshold));
		log.info(String.format("Monitor threshold for %1$s set to %2$d bytes (%3$.2f%% of %4$d)", getServiceName(),
		        threshold, this.threshold, maxMem));
	}

	private void disableMonitor() throws JMException {
		if (counterMonitor != null) {
			counterMonitor.stop();
			if (memBeanProxyName != null)
				counterMonitor.removeObservedObject(new ObjectName(memBeanProxyName));
		}

		if (memoryMXBean != null) {
			mbs.unregisterMBean(new ObjectName(memBeanProxyName));
			memBeanProxyName = null;
			memoryMXBean = null;
		}
	}

	@Override
	public void stop() throws JMException {
		disableMonitor();
		if (counterMonitor != null) {
			counterMonitor.setNotify(false);
			counterMonitor.stop();
			counterMonitor.removeNotificationListener(counterMonitorListener);
			mbs.unregisterMBean(new ObjectName(counterMonitorName));
			counterMonitorName = null;
			counterMonitor = null;
		}

		if (wndSvcController != null) {
			wndSvcController.removeNotificationListener(controllerListener);
			mbs.unregisterMBean(new ObjectName(wndSvcControllerName));
			wndSvcController.setPollPeriod(0);
			wndSvcControllerName = null;
			wndSvcController = null;
		}
		active = false;
	}

	@Override
	public void postDeregister() {
	}

	@Override
	public void postRegister(Boolean registrationDone) {
	}

	@Override
	public void preDeregister() throws Exception {
		mbs = null;
	}

	@Override
	public ObjectName preRegister(MBeanServer server, ObjectName name) throws Exception {
		mbs = server;
		return name;
	}

}
