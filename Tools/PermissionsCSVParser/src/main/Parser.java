package main;

import java.io.FileReader;
import java.io.IOException;
import java.lang.String;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import au.com.bytecode.opencsv.CSVReader;

public class Parser {

	private static Parser parser;
	private Map<String, Integer> groups = new LinkedHashMap<String, Integer>();
	private Map<String, Integer> apps = new LinkedHashMap<String, Integer>();
	private Map<String, Integer> methods = new LinkedHashMap<String, Integer>();
	private Map<Entry, Integer> entries = new LinkedHashMap<Entry, Integer>();
	private int privIdCounter;
	private static int NUM_TEXT_COLUMNS = 5;

	private static final String GROUP_TYPE_ID = "@groupTypeId";
	private static final String GROUP_NAME_ID = "@groupNameId";
	private static final String GROUP_DESCR_ID = "@groupDescrId";
	private static final String GROUP_STAT_ID = "@groupStatId";

	private static final String PRIV_TYPE_ID = "@privTypeId";
	private static final String PRIV_NAME_ID = "@privNameId";
	private static final String PRIV_DESCR_ID = "@privDescrId";
	private static final String PRIV_SYS_ID = "@privSysId";
	private static final String PRIV_TAG_ID = "@privTagId";
	private static final String PRIV_SORT_ID = "@privSortId";

	
	public static Parser INSTANCE(){
		if(parser == null){
			parser = new Parser();
		}
		return parser;
	}

	private Parser() {
	}
	
	public void generate(String root_group, String sourceDir, String sourceFileName,
			String outputFileName, int numOfGroup, int numOfApps) throws IOException, ParserException{
		privIdCounter=2;
		groups.clear();
		apps.clear();
		methods.clear();
		entries.clear();
		String source = sourceDir+"\\"+sourceFileName;
		String output = sourceDir+"\\"+outputFileName;

		CSVReader reader = new CSVReader(new FileReader(source), ';');;
		String [] nextLine = reader.readNext();

		parseHead(nextLine, numOfGroup, numOfApps);
		int lineCount = 1;
		while ((nextLine = reader.readNext()) != null) {
			try {
				lineCount++;
				parseLine(nextLine);
			} catch (Exception e) {
				System.out.println("Unable to parse line = " + lineCount);
			}
		}

		// create sql script

		List<String> strToWright = new LinkedList<String>();

		// create groups

		strToWright.add("-- create objects 'USER_GROUP'\n");

		strToWright.add("INSERT INTO objects(object_type_id_fk) VALUES ("+GROUP_TYPE_ID+");");
		strToWright.add("SET @"+root_group+"_GroupId := last_insert_id();");
		strToWright.add("CALL insert_varchar_value(@"+root_group+"_GroupId, "+GROUP_NAME_ID+", @currentTime, '"+root_group+"');");
		strToWright.add("");

		for (java.util.Map.Entry<String, Integer> e : groups.entrySet()) {
			strToWright.add("INSERT INTO objects(object_type_id_fk) VALUES ("+GROUP_TYPE_ID+");");
			strToWright.add("SET @"+e.getKey()+"_GroupId := last_insert_id();");
			strToWright.add("CALL insert_varchar_value(@"+e.getKey()+"_GroupId, "+GROUP_NAME_ID+", @currentTime, '"+e.getKey()+"');");
			strToWright.add("");
		}

		// create system priv

		strToWright.add("-- create objects 'USER_PRIVILEGE' (system)\n");

		strToWright.add("INSERT INTO objects(object_type_id_fk) VALUES ("+PRIV_TYPE_ID+");");
		strToWright.add("SET @root_permission_PrivId := last_insert_id();");
		strToWright.add("CALL insert_varchar_value(@root_permission_PrivId, "+PRIV_NAME_ID+", @currentTime, 'root_permission');");
		strToWright.add("CALL insert_int_value(@root_permission_PrivId, "+PRIV_SYS_ID+", @currentTime, 1);");
		strToWright.add("");

		for (java.util.Map.Entry<String, Integer> e : methods.entrySet()) {
			strToWright.add("INSERT INTO objects(object_type_id_fk) VALUES ("+PRIV_TYPE_ID+");");
			strToWright.add("SET @"+e.getKey()+"_PrivId := last_insert_id();");
			strToWright.add("CALL insert_varchar_value(@"+e.getKey()+"_PrivId, "+PRIV_NAME_ID+", @currentTime, '"+e.getKey()+"');");
			strToWright.add("CALL insert_int_value(@"+e.getKey()+"_PrivId, "+PRIV_SYS_ID+", @currentTime, 1);");
			strToWright.add("");
		}

		List<String> sysprivToWright = new LinkedList<String>();
		List<String> groupprivToWright = new LinkedList<String>();
		
		groupprivToWright.add("INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@root_permission_PrivId, @"+root_group+"_GroupId);");
		
		// create priv

		strToWright.add("-- create objects 'USER_PRIVILEGE'\n");

		for (java.util.Map.Entry<Entry, Integer> e : entries.entrySet()) {
			strToWright.add("INSERT INTO objects(object_type_id_fk) VALUES ("+PRIV_TYPE_ID+");");
			strToWright.add("SET @"+e.getKey().name+"_PrivId := last_insert_id();");
			strToWright.add("CALL insert_varchar_value(@"+e.getKey().name+"_PrivId, "+PRIV_NAME_ID+", @currentTime, '"+e.getKey().name+"');");
			strToWright.add("CALL insert_varchar_value(@"+e.getKey().name+"_PrivId, "+PRIV_DESCR_ID+", @currentTime, '"+e.getKey().description+"');");
			strToWright.add("CALL insert_int_value(@"+e.getKey().name+"_PrivId, "+PRIV_SYS_ID+", @currentTime, "+e.getKey().system+");");
			strToWright.add("CALL insert_varchar_value(@"+e.getKey().name+"_PrivId, "+PRIV_TAG_ID+", @currentTime, '"+e.getKey().tag+"');");
			strToWright.add("CALL insert_int_value(@"+e.getKey().name+"_PrivId, "+PRIV_SORT_ID+", @currentTime, "+e.getKey().sortOrder+");");
			strToWright.add("");
			// set sysPriv-priv relations
			for (String sysPrivId : e.getKey().methods) {
				sysprivToWright.add("INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@"+sysPrivId+"_PrivId, @"+e.getKey().name+"_PrivId);");
			}
			// set group-priv relations
			for (String groupId : e.getKey().groups) {
				groupprivToWright.add("INSERT INTO object_parents(`object_id_fk`, `parent_id_fk`) values (@"+e.getKey().name+"_PrivId, @"+groupId+"_GroupId);");
			}
		}

		strToWright.add("-- create relations system privilege - privilege\n");
		strToWright.addAll(sysprivToWright);
		strToWright.add("");
		strToWright.add("-- create relations privilege - group\n");
		strToWright.addAll(groupprivToWright);

		// write to the file
		Files.write(Paths.get(output), strToWright, StandardCharsets.UTF_8);
	
	}


	private void parseHead(String[] head, int numOfGroup, int numOfApps) throws ParserException {
		try {

			for (int i = 0; i < numOfApps; i++) {
				apps.put(head[NUM_TEXT_COLUMNS + i], (int) (Math.pow(2, 1 + i)));
			}

			for (int i = 0; i < numOfGroup; i++) {
				groups.put(head[NUM_TEXT_COLUMNS + numOfApps + i], 2 + i);
			}

			for (int i = NUM_TEXT_COLUMNS + numOfApps + numOfGroup; i < head.length; i++) {
				methods.put(head[i], privIdCounter);
				privIdCounter++;
			}
		}catch (Exception e) {
			throw new ParserException("Unable to parse head, check settings", e);
		}
		
	}

	private void parseLine(String[] line) {
		String name = line[0];
		String descr = line[1];
		String sortOrder = line[2];
		String tooltip = line[3];
		String tag = line[4];
		Entry entry = new Entry(name, descr, tooltip, tag, sortOrder);
		
		int i = NUM_TEXT_COLUMNS;
		for (java.util.Map.Entry<String, Integer> e : apps.entrySet()) {
			if (line[i++].equals("x")) {
				entry.system+=e.getValue();
			}
		}
		for (java.util.Map.Entry<String, Integer> e : groups.entrySet()) {
			if (line[i++].equals("x")) {
				entry.groups.add(e.getKey());
			}
		}
		for (java.util.Map.Entry<String, Integer> e : methods.entrySet()) {
			if (line[i++].equals("x")) {
				entry.methods.add(e.getKey());
			}
		}
		entries.put(entry, privIdCounter);
		privIdCounter++;
	}

	private class Entry {
		String name;
		String description;
		String tooltip;
		String tag;
		String sortOrder;
		List<String> groups;
		List<String> methods;
		int system;

		public Entry(String name, String descr, String tooltip, String tag, String sortOrder) {
			this.name = name;
			this.description = descr;
			this.tag = tag;
			this.tooltip = tooltip;
			this.sortOrder = sortOrder;
			this.groups = new LinkedList<String>();
			this.methods = new LinkedList<String>();
			system = 0;
		}

		public String toString() {
			StringBuilder sb = new StringBuilder();
			sb.append("Name = " + name + " ");
			sb.append("Descr = " + description + " ");
			sb.append("Tooltip = " + tooltip + " ");
			sb.append("Tag = " + tag + " ");

			return sb.toString();
		}
	}
}
