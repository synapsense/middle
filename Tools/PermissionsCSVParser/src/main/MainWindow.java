package main;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.filechooser.FileFilter;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.File;
import java.awt.Toolkit;

public class MainWindow {

	private JFrame frmCsvParserFor;
	private JTextField textField;
	private JTextField txtAdministrators;
	private final JFileChooser fc = new JFileChooser();
	private File selectedFile;
	private JSpinner spinner;
	private JSpinner spinner_1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainWindow window = new MainWindow();
					window.frmCsvParserFor.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MainWindow() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmCsvParserFor = new JFrame();
		frmCsvParserFor.setTitle("CSV Parser for User Group Management");
		frmCsvParserFor.setResizable(false);
		frmCsvParserFor.setBounds(100, 100, 419, 300);
		frmCsvParserFor.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		FileFilter filter1 = new ExtensionFileFilter("CSV", new String[] { "CSV" });
	    fc.setFileFilter(filter1);

		JButton btnOpenCsv = new JButton("Open CSV");
		btnOpenCsv.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int result = fc.showOpenDialog(frmCsvParserFor);
				if (result == JFileChooser.APPROVE_OPTION) {
					selectedFile = fc.getSelectedFile();
					textField.setText(selectedFile.getAbsolutePath());
				}
			}
		});

		textField = new JTextField();
		textField.setEditable(false);
		textField.setColumns(10);

		JLabel lblRootGroup = new JLabel("Root group");

		txtAdministrators = new JTextField();
		txtAdministrators.setHorizontalAlignment(SwingConstants.RIGHT);
		txtAdministrators.setText("Administrators");
		txtAdministrators.setColumns(10);

		spinner = new JSpinner();
		spinner.setModel(new SpinnerNumberModel(new Integer(2), new Integer(0),
				null, new Integer(1)));

		JLabel lblNumOfGroups = new JLabel("Num of groups");

		JButton btnGenerateScript = new JButton("Generate script");
		btnGenerateScript.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (selectedFile == null) {
					JOptionPane.showMessageDialog(frmCsvParserFor,
							"Please, select CSV file", "Error",
							JOptionPane.INFORMATION_MESSAGE);
					return;
				}
				if (txtAdministrators.getText() == null
						|| txtAdministrators.getText().isEmpty()) {
					JOptionPane.showMessageDialog(frmCsvParserFor,
							"Please, input name of root group", "Error",
							JOptionPane.INFORMATION_MESSAGE);
					return;
				}

				try {
					Parser.INSTANCE().generate(txtAdministrators.getText(),
							selectedFile.getParent(), selectedFile.getName(),
							"output.sql", (int) (spinner.getValue()), (int) (spinner_1.getValue()) );
				} catch (Exception e1) {
					JOptionPane.showMessageDialog(frmCsvParserFor,
							e1.getMessage(), "Error",
							JOptionPane.ERROR_MESSAGE);
					return;
				}
				JOptionPane.showMessageDialog(frmCsvParserFor, "File saved: "
						+ selectedFile.getParent() + "\\output.sql", "Done",
						JOptionPane.INFORMATION_MESSAGE);
			}
		});
		
		JLabel lblNumOfApps = new JLabel("Num of apps");
		
		spinner_1 = new JSpinner();
		spinner_1.setModel(new SpinnerNumberModel(new Integer(2), new Integer(0), null, new Integer(1)));
		GroupLayout groupLayout = new GroupLayout(
				frmCsvParserFor.getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(137)
					.addComponent(btnGenerateScript, GroupLayout.DEFAULT_SIZE, 145, Short.MAX_VALUE)
					.addGap(162))
				.addGroup(groupLayout.createSequentialGroup()
					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(29)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(txtAdministrators, GroupLayout.PREFERRED_SIZE, 139, GroupLayout.PREFERRED_SIZE)
								.addComponent(textField, GroupLayout.DEFAULT_SIZE, 172, Short.MAX_VALUE))
							.addGap(18)
							.addComponent(spinner_1, GroupLayout.PREFERRED_SIZE, 61, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED))
						.addGroup(Alignment.LEADING, groupLayout.createSequentialGroup()
							.addGap(86)
							.addComponent(lblRootGroup)
							.addPreferredGap(ComponentPlacement.RELATED, 68, Short.MAX_VALUE)
							.addComponent(lblNumOfApps)))
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(18)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(spinner, GroupLayout.PREFERRED_SIZE, 57, GroupLayout.PREFERRED_SIZE)
								.addComponent(lblNumOfGroups)))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(26)
							.addComponent(btnOpenCsv)))
					.addGap(79))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(23)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(textField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(btnOpenCsv))
					.addGap(43)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblRootGroup)
						.addComponent(lblNumOfApps)
						.addComponent(lblNumOfGroups))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(txtAdministrators, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(spinner, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(spinner_1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED, 63, Short.MAX_VALUE)
					.addComponent(btnGenerateScript, GroupLayout.PREFERRED_SIZE, 48, GroupLayout.PREFERRED_SIZE)
					.addGap(32))
		);
		frmCsvParserFor.getContentPane().setLayout(groupLayout);
	}
	
	class ExtensionFileFilter extends FileFilter {
		  String description;

		  String extensions[];

		  public ExtensionFileFilter(String description, String extension) {
		    this(description, new String[] { extension });
		  }

		  public ExtensionFileFilter(String description, String extensions[]) {
		    if (description == null) {
		      this.description = extensions[0];
		    } else {
		      this.description = description;
		    }
		    this.extensions = (String[]) extensions.clone();
		    toLower(this.extensions);
		  }

		  private void toLower(String array[]) {
		    for (int i = 0, n = array.length; i < n; i++) {
		      array[i] = array[i].toLowerCase();
		    }
		  }

		  public String getDescription() {
		    return description;
		  }

		  public boolean accept(File file) {
		    if (file.isDirectory()) {
		      return true;
		    } else {
		      String path = file.getAbsolutePath().toLowerCase();
		      for (int i = 0, n = extensions.length; i < n; i++) {
		        String extension = extensions[i];
		        if ((path.endsWith(extension) && (path.charAt(path.length() - extension.length() - 1)) == '.')) {
		          return true;
		        }
		      }
		    }
		    return false;
		  }
	}
}
