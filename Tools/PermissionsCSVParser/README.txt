CSVParser User Guide

CSVParser is a tool for automatic SQL script generation from CSV file with Permission mappings.

The tool processes content of supported CSV file, generates SQL script with 
permissions, groups, users and relations between them.

BUILDING FROM SOURCES

Prerequisites:
- Java JDK 1.7 or higher
- Ant

After checking out sources from git cd into CSVParser folder and type 'ant'.

USAGE

To run program execute 'ant run' into CSVParser folder.
Tool has GUI and easy to setup.

After run tool, user should setup three options:
- choose csv file
- input name of root group (group with root permission)
- input number of applications in csv file 
- input number of group in csv file

By default root group name set to 'Administrators' and number of group in csv set to 2.

After setup to generate script use 'Generate script' button.
SQL file will be saved with name output.sql in the same folder as original csv file.