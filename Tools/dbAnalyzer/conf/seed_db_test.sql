use `synap`;

-- =============================================================================
-- Removing old data from tbl_neighbor_list
-- =============================================================================
DELETE FROM `tbl_neighbor_list`;
DELETE FROM `tbl_network_statnode`;

-- =============================================================================
-- Filling tbl_neighbor_list
-- =============================================================================

INSERT INTO `tbl_neighbor_list` (`node_physical_id`, `neighbor_physical_id`, `network_id`, `link_status`, `link_quality_index`, `received_signal_strength_index`) 
VALUES (80316575948734623, 84683518289903772, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_neighbor_list` (`node_physical_id`, `neighbor_physical_id`, `network_id`, `link_status`, `link_quality_index`, `received_signal_strength_index`) 
VALUES (80316575948734623, 113834088409858118, NULL, NULL, NULL, NULL);


INSERT INTO `tbl_neighbor_list` (`node_physical_id`, `neighbor_physical_id`, `network_id`, `link_status`, `link_quality_index`, `received_signal_strength_index`) 
VALUES (84683518289903772, 80316575948734623, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_neighbor_list` (`node_physical_id`, `neighbor_physical_id`, `network_id`, `link_status`, `link_quality_index`, `received_signal_strength_index`) 
VALUES (84683518289903772, 118832154720337932, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_neighbor_list` (`node_physical_id`, `neighbor_physical_id`, `network_id`, `link_status`, `link_quality_index`, `received_signal_strength_index`) 
VALUES (84683518289903772, 73301691763523831, NULL, NULL, NULL, NULL);

INSERT INTO `tbl_neighbor_list` (`node_physical_id`, `neighbor_physical_id`, `network_id`, `link_status`, `link_quality_index`, `received_signal_strength_index`) 
VALUES (118832154720337932, 84683518289903772, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_neighbor_list` (`node_physical_id`, `neighbor_physical_id`, `network_id`, `link_status`, `link_quality_index`, `received_signal_strength_index`) 
VALUES (118832154720337932, 73301691763523831, NULL, NULL, NULL, NULL);

INSERT INTO `tbl_neighbor_list` (`node_physical_id`, `neighbor_physical_id`, `network_id`, `link_status`, `link_quality_index`, `received_signal_strength_index`) 
VALUES (73301691763523831, 118832154720337932, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_neighbor_list` (`node_physical_id`, `neighbor_physical_id`, `network_id`, `link_status`, `link_quality_index`, `received_signal_strength_index`) 
VALUES (73301691763523831, 84683518289903772, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_neighbor_list` (`node_physical_id`, `neighbor_physical_id`, `network_id`, `link_status`, `link_quality_index`, `received_signal_strength_index`) 
VALUES (73301691763523831, 117426978860040202, NULL, NULL, NULL, NULL);

INSERT INTO `tbl_neighbor_list` (`node_physical_id`, `neighbor_physical_id`, `network_id`, `link_status`, `link_quality_index`, `received_signal_strength_index`) 
VALUES (117426978860040202, 73301691763523831, NULL, NULL, NULL, NULL);

INSERT INTO `tbl_neighbor_list` (`node_physical_id`, `neighbor_physical_id`, `network_id`, `link_status`, `link_quality_index`, `received_signal_strength_index`) 
VALUES (113834088409858118, 80316575948734623, NULL, NULL, NULL, NULL);

-- =============================================================================
-- Filling tbl_network_statnode
-- =============================================================================

INSERT INTO `tbl_network_statnode` (`logical_id`, `nwk_timestamp`, `send_fails_full_queue`, `radio_on_time`, `free_slot_count`, `time_syncs_lost_drift_detection`, 
`time_syncs_lost_timeout`, `hop_count`, `router_state`, `parent_state`, `ch_1`, `ch_2`, `ch_3`, `ch_4`, `ch_5`, `ch_6`, `ch_7`, `ch_8`, `ch_9`, `ch_10`, `ch_11`, `ch_12`, 
`ch_13`, `ch_14`, `ch_15`, `ch_16`) 
VALUES (1, CURRENT_TIMESTAMP, 0, 30000, 46000, 0, 1, 1, 1, 1, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');

INSERT INTO `tbl_network_statnode` (`logical_id`, `nwk_timestamp`, `send_fails_full_queue`, `radio_on_time`, `free_slot_count`, `time_syncs_lost_drift_detection`, 
`time_syncs_lost_timeout`, `hop_count`, `router_state`, `parent_state`, `ch_1`, `ch_2`, `ch_3`, `ch_4`, `ch_5`, `ch_6`, `ch_7`, `ch_8`, `ch_9`, `ch_10`, `ch_11`, `ch_12`, 
`ch_13`, `ch_14`, `ch_15`, `ch_16`) 
VALUES (2, CURRENT_TIMESTAMP, 10, 30000, 40000, 1, 1, 1, 1, 1, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_network_statnode` (`logical_id`, `nwk_timestamp`, `send_fails_full_queue`, `radio_on_time`, `free_slot_count`, `time_syncs_lost_drift_detection`, 
`time_syncs_lost_timeout`, `hop_count`, `router_state`, `parent_state`, `ch_1`, `ch_2`, `ch_3`, `ch_4`, `ch_5`, `ch_6`, `ch_7`, `ch_8`, `ch_9`, `ch_10`, `ch_11`, `ch_12`, 
`ch_13`, `ch_14`, `ch_15`, `ch_16`) 
VALUES (2, CURRENT_TIMESTAMP, 10, 30000, 40000, 1, 1, 2, 1, 1, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');


INSERT INTO `tbl_network_statnode` (`logical_id`, `nwk_timestamp`, `send_fails_full_queue`, `radio_on_time`, `free_slot_count`, `time_syncs_lost_drift_detection`, 
`time_syncs_lost_timeout`, `hop_count`, `router_state`, `parent_state`, `ch_1`, `ch_2`, `ch_3`, `ch_4`, `ch_5`, `ch_6`, `ch_7`, `ch_8`, `ch_9`, `ch_10`, `ch_11`, `ch_12`, 
`ch_13`, `ch_14`, `ch_15`, `ch_16`) 
VALUES (3, CURRENT_TIMESTAMP, 0, 37000, 45000, 0, 1, 2, 1, 1, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_network_statnode` (`logical_id`, `nwk_timestamp`, `send_fails_full_queue`, `radio_on_time`, `free_slot_count`, `time_syncs_lost_drift_detection`, 
`time_syncs_lost_timeout`, `hop_count`, `router_state`, `parent_state`, `ch_1`, `ch_2`, `ch_3`, `ch_4`, `ch_5`, `ch_6`, `ch_7`, `ch_8`, `ch_9`, `ch_10`, `ch_11`, `ch_12`, 
`ch_13`, `ch_14`, `ch_15`, `ch_16`) 
VALUES (3, CURRENT_TIMESTAMP, 0, 37000, 45000, 0, 1, 3, 1, 1, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_network_statnode` (`logical_id`, `nwk_timestamp`, `send_fails_full_queue`, `radio_on_time`, `free_slot_count`, `time_syncs_lost_drift_detection`, 
`time_syncs_lost_timeout`, `hop_count`, `router_state`, `parent_state`, `ch_1`, `ch_2`, `ch_3`, `ch_4`, `ch_5`, `ch_6`, `ch_7`, `ch_8`, `ch_9`, `ch_10`, `ch_11`, `ch_12`, 
`ch_13`, `ch_14`, `ch_15`, `ch_16`) 
VALUES (3, CURRENT_TIMESTAMP, 0, 37000, 45000, 0, 1, 4, 1, 1, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');

INSERT INTO `tbl_network_statnode` (`logical_id`, `nwk_timestamp`, `send_fails_full_queue`, `radio_on_time`, `free_slot_count`, `time_syncs_lost_drift_detection`, 
`time_syncs_lost_timeout`, `hop_count`, `router_state`, `parent_state`, `ch_1`, `ch_2`, `ch_3`, `ch_4`, `ch_5`, `ch_6`, `ch_7`, `ch_8`, `ch_9`, `ch_10`, `ch_11`, `ch_12`, 
`ch_13`, `ch_14`, `ch_15`, `ch_16`) 
VALUES (4, CURRENT_TIMESTAMP, 10, 37000, 45000, 1, 1, 2, 1, 1, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_network_statnode` (`logical_id`, `nwk_timestamp`, `send_fails_full_queue`, `radio_on_time`, `free_slot_count`, `time_syncs_lost_drift_detection`, 
`time_syncs_lost_timeout`, `hop_count`, `router_state`, `parent_state`, `ch_1`, `ch_2`, `ch_3`, `ch_4`, `ch_5`, `ch_6`, `ch_7`, `ch_8`, `ch_9`, `ch_10`, `ch_11`, `ch_12`, 
`ch_13`, `ch_14`, `ch_15`, `ch_16`) 
VALUES (4, CURRENT_TIMESTAMP, 10, 37000, 45000, 1, 1, 3, 1, 1, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_network_statnode` (`logical_id`, `nwk_timestamp`, `send_fails_full_queue`, `radio_on_time`, `free_slot_count`, `time_syncs_lost_drift_detection`, 
`time_syncs_lost_timeout`, `hop_count`, `router_state`, `parent_state`, `ch_1`, `ch_2`, `ch_3`, `ch_4`, `ch_5`, `ch_6`, `ch_7`, `ch_8`, `ch_9`, `ch_10`, `ch_11`, `ch_12`, 
`ch_13`, `ch_14`, `ch_15`, `ch_16`) 
VALUES (4, CURRENT_TIMESTAMP, 10, 37000, 45000, 1, 1, 4, 1, 1, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');


INSERT INTO `tbl_network_statnode` (`logical_id`, `nwk_timestamp`, `send_fails_full_queue`, `radio_on_time`, `free_slot_count`, `time_syncs_lost_drift_detection`, 
`time_syncs_lost_timeout`, `hop_count`, `router_state`, `parent_state`, `ch_1`, `ch_2`, `ch_3`, `ch_4`, `ch_5`, `ch_6`, `ch_7`, `ch_8`, `ch_9`, `ch_10`, `ch_11`, `ch_12`, 
`ch_13`, `ch_14`, `ch_15`, `ch_16`) 
VALUES (5, CURRENT_TIMESTAMP, 10, 37000, 45000, 1, 1, 5, 1, 1, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_network_statnode` (`logical_id`, `nwk_timestamp`, `send_fails_full_queue`, `radio_on_time`, `free_slot_count`, `time_syncs_lost_drift_detection`, 
`time_syncs_lost_timeout`, `hop_count`, `router_state`, `parent_state`, `ch_1`, `ch_2`, `ch_3`, `ch_4`, `ch_5`, `ch_6`, `ch_7`, `ch_8`, `ch_9`, `ch_10`, `ch_11`, `ch_12`, 
`ch_13`, `ch_14`, `ch_15`, `ch_16`) 
VALUES (5, CURRENT_TIMESTAMP, 10, 37000, 45000, 1, 1, 3, 1, 1, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `tbl_network_statnode` (`logical_id`, `nwk_timestamp`, `send_fails_full_queue`, `radio_on_time`, `free_slot_count`, `time_syncs_lost_drift_detection`, 
`time_syncs_lost_timeout`, `hop_count`, `router_state`, `parent_state`, `ch_1`, `ch_2`, `ch_3`, `ch_4`, `ch_5`, `ch_6`, `ch_7`, `ch_8`, `ch_9`, `ch_10`, `ch_11`, `ch_12`, 
`ch_13`, `ch_14`, `ch_15`, `ch_16`) 
VALUES (5, CURRENT_TIMESTAMP, 10, 37000, 45000, 1, 1, 4, 1, 1, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');


INSERT INTO `tbl_network_statnode` (`logical_id`, `nwk_timestamp`, `send_fails_full_queue`, `radio_on_time`, `free_slot_count`, `time_syncs_lost_drift_detection`, 
`time_syncs_lost_timeout`, `hop_count`, `router_state`, `parent_state`, `ch_1`, `ch_2`, `ch_3`, `ch_4`, `ch_5`, `ch_6`, `ch_7`, `ch_8`, `ch_9`, `ch_10`, `ch_11`, `ch_12`, 
`ch_13`, `ch_14`, `ch_15`, `ch_16`) 
VALUES (6, CURRENT_TIMESTAMP, 10, 37000, 45000, 1, 1, 1, 1, 1, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');


INSERT INTO `tbl_network_statnode` (`logical_id`, `nwk_timestamp`, `send_fails_full_queue`, `radio_on_time`, `free_slot_count`, `time_syncs_lost_drift_detection`, 
`time_syncs_lost_timeout`, `hop_count`, `router_state`, `parent_state`, `ch_1`, `ch_2`, `ch_3`, `ch_4`, `ch_5`, `ch_6`, `ch_7`, `ch_8`, `ch_9`, `ch_10`, `ch_11`, `ch_12`, 
`ch_13`, `ch_14`, `ch_15`, `ch_16`) 
VALUES (10, CURRENT_TIMESTAMP, 10, 37000, 45000, 1, 1, 11, 1, 1, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');

