package com.synapsense.diagnostic.db.utils;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

/**
 * This class is responsible for pool connection management
 */
public class ConnectionManager {
	private static final String URL = "com.synapsense.database.driver";
	private static final String USERNAME = "com.synapsense.database.username";
	private static final String PASSWORD = "com.synapsense.database.password";
	private static final String config = "conf/db.properties";
	private static ConnectionPool pool = null;

	static {
		try {
			init();
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
	}

	/**
	 * Creates a connection poll
	 * 
	 * @throws FileNotFoundException
	 *             if config file isn't found
	 * @throws IOException
	 *             if reading of config file is unsuccessful
	 */
	private static void init() throws FileNotFoundException, IOException {
		Properties properties = new Properties();
		properties.load(new FileReader(config));

		pool = new ConnectionPool(properties.getProperty(URL), properties.getProperty(USERNAME),
		        properties.getProperty(PASSWORD));
	}

	/**
	 * @return a connection from the poll
	 * @throws SQLException
	 *             if cannot create a connection
	 */
	public static Connection getConnection() throws SQLException {
		return pool.getConnection();
	}

	/**
	 * Close all opened connection
	 */
	public static void closeAllConnections() {
		pool.closeAllConnections();
	}
}
