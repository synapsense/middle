package com.synapsense.diagnostic.db.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.Vector;

/**
 * This class implements a connection poll
 */
public class ConnectionPool {
	private final String CONNECTION_POOL = "com.synapsense.database.connection.pool";
	private final Vector<DBConnection> connections = new Vector<DBConnection>();
	private final String url;
	private final String username;
	private final String password;

	private final int size;
	private int locked = 0;

	/**
	 * @param url
	 *            URL to database
	 * @param username
	 * @param password
	 */
	protected ConnectionPool(String url, String username, String password) {
		size = Integer.parseInt(System.getProperty(CONNECTION_POOL, "1"));
		this.url = url;
		this.username = username;
		this.password = password;
	}

	/**
	 * @return a connection from the pool
	 * @throws SQLException
	 */
	protected synchronized Connection getConnection() throws SQLException {
		for (DBConnection connection : connections) {
			if (!connection.isLocked()) {
				connection.lock();
				return connection;
			}
		}

		DBConnection connection = null;
		if (size > locked) {
			locked++;
			Connection c = DriverManager.getConnection(url, username, password);
			connection = new DBConnection(c, this);
			connection.lock();
			connections.add(connection);
			return connection;
		} else {
			synchronized (connections) {
				try {
					connections.wait();
					connection = (DBConnection) getConnection();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
		return connection;
	}

	/**
	 * Close all opened connections
	 */
	protected void closeAllConnections() {
		Enumeration<DBConnection> connlist = connections.elements();
		while ((connlist != null) && (connlist.hasMoreElements())) {
			DBConnection connection = connlist.nextElement();
			removeConnection(connection);
			try {
				connection.getConnection().close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		locked = 0;
	}

	protected void releaseConnection(DBConnection connection) {
		connection.unlock();
		synchronized (connections) {
			connections.notify();
		}
	}

	protected void removeConnection(DBConnection connection) {
		connections.removeElement(connection);
	}
}
