package com.synapsense.diagnostic.db.model;

import java.sql.Connection;
import java.util.Map;

import com.synapsense.diagnostic.db.appender.EventAppender;
import com.synapsense.diagnostic.db.interpreter.MessageInterpreter;
import com.synapsense.diagnostic.db.utils.TypeConvertor;
import com.synapsense.diagnostic.db.variables.Variable;
import com.synapsense.diagnostic.messages.Condition;
import com.synapsense.diagnostic.messages.Operation;

public class ConditionImpl implements Command {
	private final Class<?> clazz;
	private final String lValue;
	private final String rValue;
	private final Operation operation;

	public ConditionImpl(Condition condition) throws ClassNotFoundException {
		clazz = Class.forName(condition.getDataClass());
		lValue = condition.getLvalue();
		rValue = condition.getRvalue();
		operation = condition.getOperation();
	}

	@SuppressWarnings("unchecked")
	public boolean execute(Connection connection, EventAppender appender, Map<String, Variable> variables) {
		boolean result = false;
		Object l = TypeConvertor.convert(MessageInterpreter.getInterpretedMessage(lValue, variables), clazz);
		Object r = TypeConvertor.convert(MessageInterpreter.getInterpretedMessage(rValue, variables), clazz);
		if (l instanceof Comparable) {
			result = compare((Comparable) l, (Comparable) r, operation);
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	private static boolean compare(Comparable lValue, Comparable rValue, Operation operation) {
		switch (operation) {
		case EQUAL:
			return lValue.compareTo(rValue) == 0;
		case GREATER:
			return lValue.compareTo(rValue) > 0;
		case LESS:
			return lValue.compareTo(rValue) < 0;
		}
		return false;
	}
}
