package com.synapsense.diagnostic.db.model;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import com.synapsense.diagnostic.db.appender.EventAppender;
import com.synapsense.diagnostic.db.variables.Variable;
import com.synapsense.diagnostic.messages.And;
import com.synapsense.diagnostic.messages.Condition;
import com.synapsense.diagnostic.messages.Or;

public class AndImpl implements Command {
	private final Collection<Command> commands = new ArrayList<Command>();

	public AndImpl(And and) throws ClassNotFoundException {
		for (Condition condition : and.getCondition()) {
			commands.add(new ConditionImpl(condition));
		}
		if (and.getOr() != null) {
			for (Or or : and.getOr()) {
				commands.add(new OrImpl(or));
			}
		}
	}

	@Override
	public boolean execute(Connection connection, EventAppender appender, Map<String, Variable> vabiables) {
		boolean result = true;
		for (Command command : commands) {
			if (!command.execute(connection, appender, vabiables)) {
				result = false;
				break;
			}
		}
		return result;
	}
}
