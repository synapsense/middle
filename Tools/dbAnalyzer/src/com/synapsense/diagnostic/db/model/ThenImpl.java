package com.synapsense.diagnostic.db.model;

import java.sql.Connection;
import java.util.Map;

import com.synapsense.diagnostic.db.appender.EventAppender;
import com.synapsense.diagnostic.db.variables.Variable;
import com.synapsense.diagnostic.messages.Query;
import com.synapsense.diagnostic.messages.Then;

public class ThenImpl implements Command {
	private final Command command;

	public ThenImpl(Then t, Map<String, QueryImpl> queries) throws ClassNotFoundException {
		if (t.getPush() != null) {
			command = new PushImpl(t.getPush());
		} else if (t.getIf() != null) {
			command = new IfImpl(t.getIf(), queries);
		} else {
			Query q = t.getQuery();
			queries.put(q.getName(), new QueryImpl(q));
			if (t.getForeach() != null) {
				command = new ForeachImpl(t.getForeach(), queries);
			} else {
				command = new ForsetImpl(t.getForset(), queries);
			}
		}
	}

	@Override
	public boolean execute(Connection connection, EventAppender appender, Map<String, Variable> variables) {
		return command.execute(connection, appender, variables);
	}
}
