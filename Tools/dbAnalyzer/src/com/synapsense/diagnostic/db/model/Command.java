package com.synapsense.diagnostic.db.model;

import java.sql.Connection;
import java.util.Map;

import com.synapsense.diagnostic.db.appender.EventAppender;
import com.synapsense.diagnostic.db.variables.Variable;

public interface Command {
	public boolean execute(Connection connection, EventAppender appender, Map<String, Variable> variables);
}
