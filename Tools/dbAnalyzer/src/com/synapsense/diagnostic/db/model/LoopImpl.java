package com.synapsense.diagnostic.db.model;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.synapsense.diagnostic.db.appender.EventAppender;
import com.synapsense.diagnostic.db.variables.Variable;
import com.synapsense.diagnostic.messages.Loop;

public class LoopImpl implements Command {
	private final Command command;
	private final QueryImpl query;
	private int succeedTimes;

	public LoopImpl(Loop loop, Map<String, QueryImpl> queries) throws ClassNotFoundException {
		query = queries.get(loop.getQueryName());
		command = new ConditionsImpl(loop.getConditions());
	}

	@Override
	public boolean execute(Connection connection, EventAppender appender, Map<String, Variable> variables) {
		boolean result = true;
		succeedTimes = 0;
		try {
			Statement stm = connection.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
			String sql = query.getSql(variables);

			ResultSet res = stm.executeQuery(sql);

			ResultSetMetaData md = res.getMetaData();
			Set<String> labels = new HashSet<String>();
			for (int i = 1; i <= md.getColumnCount(); i++) {
				labels.add(md.getColumnLabel(i));
			}

			while (res.next()) {
				Variable variable = new Variable();
				for (String label : labels) {
					variable.addProperty(label, res.getObject(label));
				}
				variables.put(query.getName(), variable);
				if (command.execute(connection, appender, variables)) {
					succeedTimes++;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	public int getSucceedTimes() {
		return succeedTimes;
	}
}
