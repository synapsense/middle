package com.synapsense.diagnostic.db.model;

import com.synapsense.diagnostic.db.appender.EventAppender;

public interface Executor {
	public void addAppender(EventAppender appender);

	public void removeAppender(EventAppender appender);

	public void start();
}
