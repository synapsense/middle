package com.synapsense.diagnostic.db.model;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import com.synapsense.diagnostic.db.appender.EventAppender;
import com.synapsense.diagnostic.db.variables.Variable;
import com.synapsense.diagnostic.messages.And;
import com.synapsense.diagnostic.messages.Condition;
import com.synapsense.diagnostic.messages.Or;

public class OrImpl implements Command {
	private final Collection<Command> commands = new ArrayList<Command>();

	public OrImpl(Or or) throws ClassNotFoundException {
		for (Condition condition : or.getCondition()) {
			commands.add(new ConditionImpl(condition));
		}
		if (or.getAnd() != null) {
			for (And and : or.getAnd()) {
				commands.add(new AndImpl(and));
			}
		}
	}

	@Override
	public boolean execute(Connection connection, EventAppender appender, Map<String, Variable> vabiables) {
		boolean result = false;
		for (Command command : commands) {
			if (command.execute(connection, appender, vabiables)) {
				result = true;
				break;
			}
		}
		return result;
	}
}
