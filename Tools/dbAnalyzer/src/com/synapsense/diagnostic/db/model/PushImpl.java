package com.synapsense.diagnostic.db.model;

import java.sql.Connection;
import java.util.Map;

import com.synapsense.diagnostic.db.appender.EventAppender;
import com.synapsense.diagnostic.db.interpreter.MessageInterpreter;
import com.synapsense.diagnostic.db.variables.Variable;
import com.synapsense.diagnostic.messages.Push;

public class PushImpl implements Command {
	private final String message;

	public PushImpl(Push push) {
		message = push.getMessage();
	}

	@Override
	public boolean execute(Connection connection, EventAppender appender, Map<String, Variable> variables) {
		DbEvent dbEvent = new DbEvent(getMessage(variables));
		appender.doAppend(dbEvent);
		return true;
	}

	private String getMessage(Map<String, Variable> variables) {
		return MessageInterpreter.getInterpretedMessage(message, variables);
	}
}
