package com.synapsense.diagnostic.db.model;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.synapsense.diagnostic.db.appender.EventAppender;
import com.synapsense.diagnostic.db.variables.Variable;
import com.synapsense.diagnostic.messages.Forset;
import com.synapsense.diagnostic.messages.Operation;
import com.synapsense.diagnostic.messages.Query;

public class ForsetImpl implements Command {
	private final QueryImpl query;
	private final Operation operation;
	private final LoopImpl loop;
	private final ThenImpl then;
	private final int value;

	public ForsetImpl(Forset fs, Map<String, QueryImpl> queries) throws ClassNotFoundException {
		query = queries.get(fs.getQueryName());
		operation = fs.getOperation();

		Query q = fs.getQuery();
		queries.put(q.getName(), new QueryImpl(q));

		loop = new LoopImpl(fs.getLoop(), queries);
		then = new ThenImpl(fs.getThen(), queries);
		value = fs.getValue();
	}

	@Override
	public boolean execute(Connection connection, EventAppender appender, Map<String, Variable> variables) {
		boolean result = true;
		try {
			Statement stm = connection.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
			String sql = query.getSql(variables);

			ResultSet res = stm.executeQuery(sql);

			ResultSetMetaData md = res.getMetaData();
			Set<String> labels = new HashSet<String>();
			for (int i = 1; i <= md.getColumnCount(); i++) {
				labels.add(md.getColumnLabel(i));
			}

			while (res.next()) {
				Variable variable = new Variable();
				for (String label : labels) {
					variable.addProperty(label, res.getObject(label));
				}
				variables.put(query.getName(), variable);

				if (loop.execute(connection, appender, variables)) {
					boolean succeed = false;
					switch (operation) {
					case EQUAL:
						if (value == loop.getSucceedTimes())
							succeed = true;
						break;
					case GREATER:
						if (loop.getSucceedTimes() > value)
							succeed = true;
						break;
					case LESS:
						if (loop.getSucceedTimes() < value)
							succeed = true;
						break;
					}

					if (succeed) {
						then.execute(connection, appender, variables);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}
		return result;
	}
}
