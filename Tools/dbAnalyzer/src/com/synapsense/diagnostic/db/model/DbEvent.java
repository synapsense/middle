package com.synapsense.diagnostic.db.model;

public class DbEvent {
	private final String message;

	public DbEvent(String message) {
		this.message = message;
	}

	public String toString() {
		return "{message='" + message + "'}";
	}

	public String getMessage() {
		return message;
	}
}
