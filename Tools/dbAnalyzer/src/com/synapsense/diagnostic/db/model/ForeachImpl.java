package com.synapsense.diagnostic.db.model;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.synapsense.diagnostic.db.appender.EventAppender;
import com.synapsense.diagnostic.db.variables.Variable;
import com.synapsense.diagnostic.messages.Foreach;
import com.synapsense.diagnostic.messages.Query;

public class ForeachImpl implements Command {
	private final QueryImpl query;
	private final Command command;

	public ForeachImpl(Foreach fe, Map<String, QueryImpl> queries) throws ClassNotFoundException {
		query = queries.get(fe.getQueryName());

		if (fe.getPush() != null) {
			command = new PushImpl(fe.getPush());
		} else if (fe.getIf() != null) {
			command = new IfImpl(fe.getIf(), queries);
		} else {
			Query q = fe.getQuery();
			queries.put(q.getName(), new QueryImpl(q));
			if (fe.getForeach() != null) {
				command = new ForeachImpl(fe.getForeach(), queries);
			} else {
				command = new ForsetImpl(fe.getForset(), queries);
			}
		}
	}

	@Override
	public boolean execute(Connection connection, EventAppender appender, Map<String, Variable> variables) {
		boolean result = true;
		try {
			Statement stm = connection.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
			String sql = query.getSql(variables);

			ResultSet res = stm.executeQuery(sql);

			ResultSetMetaData md = res.getMetaData();
			Set<String> labels = new HashSet<String>();
			for (int i = 1; i <= md.getColumnCount(); i++) {
				labels.add(md.getColumnLabel(i));
			}

			while (res.next()) {
				Variable variable = new Variable();
				for (String label : labels) {
					variable.addProperty(label, res.getObject(label));
				}
				variables.put(query.getName(), variable);
				command.execute(connection, appender, variables);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}
}
