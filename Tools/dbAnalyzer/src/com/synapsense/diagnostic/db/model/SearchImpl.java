package com.synapsense.diagnostic.db.model;

import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;

import com.synapsense.diagnostic.db.appender.EventAppender;
import com.synapsense.diagnostic.db.variables.Variable;
import com.synapsense.diagnostic.messages.Query;
import com.synapsense.diagnostic.messages.Search;

public class SearchImpl implements Command {
	private final Map<String, QueryImpl> queries = new HashMap<String, QueryImpl>();
	private final Command command;

	public SearchImpl(Search search) throws ClassNotFoundException {
		for (Query query : search.getQuery()) {
			queries.put(query.getName(), new QueryImpl(query));
		}
		if (search.getForeach() != null) {
			command = new ForeachImpl(search.getForeach(), queries);
		} else {
			command = new ForsetImpl(search.getForset(), queries);
		}
	}

	@Override
	public boolean execute(Connection connection, EventAppender appender, Map<String, Variable> variables) {
		return command.execute(connection, appender, variables);
	}
}
