package com.synapsense.diagnostic.db.model;

import java.util.Map;

import com.synapsense.diagnostic.db.interpreter.MessageInterpreter;
import com.synapsense.diagnostic.db.variables.Variable;
import com.synapsense.diagnostic.messages.Query;

public class QueryImpl {
	private final String name;
	private final String sql;

	public QueryImpl(Query query) {
		name = query.getName();
		sql = query.getSql();
	}

	public String getName() {
		return name;
	}

	public String getSql(Map<String, Variable> variables) {
		return MessageInterpreter.getInterpretedMessage(sql, variables);
	}
}
