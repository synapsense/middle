package com.synapsense.diagnostic.db.model;

import java.sql.Connection;
import java.util.Map;

import com.synapsense.diagnostic.db.appender.EventAppender;
import com.synapsense.diagnostic.db.variables.Variable;
import com.synapsense.diagnostic.messages.Conditions;

public class ConditionsImpl implements Command {

	private final Command command;

	public ConditionsImpl(Conditions conditions) throws ClassNotFoundException {
		if (conditions.getCondition() != null) {
			command = new ConditionImpl(conditions.getCondition());
		} else if (conditions.getAnd() != null) {
			command = new AndImpl(conditions.getAnd());
		} else {
			command = new OrImpl(conditions.getOr());
		}
	}

	@Override
	public boolean execute(Connection connection, EventAppender appender, Map<String, Variable> variables) {
		return command.execute(connection, appender, variables);
	}
}
