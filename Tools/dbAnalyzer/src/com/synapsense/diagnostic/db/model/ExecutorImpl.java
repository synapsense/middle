package com.synapsense.diagnostic.db.model;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.synapsense.diagnostic.db.appender.EventAppender;
import com.synapsense.diagnostic.db.utils.ConnectionManager;
import com.synapsense.diagnostic.db.variables.Variable;
import com.synapsense.diagnostic.messages.DbAnalyzer;
import com.synapsense.diagnostic.messages.Search;

public class ExecutorImpl implements Executor, EventAppender {
	private List<EventAppender> appenders = new ArrayList<EventAppender>();
	private final Set<String> filter = new HashSet<String>();
	private final DbAnalyzer config;

	public ExecutorImpl(DbAnalyzer config) {
		this.config = config;
	}

	public void start() {
		for (Search s : config.getSearch()) {
			try {
				SearchImpl impl = new SearchImpl(s);
				Thread thread = new Thread(this.new Task(impl, ConnectionManager.getConnection(), this,
				        new HashMap<String, Variable>()));
				thread.start();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void addAppender(EventAppender appender) {
		appenders.add(appender);
	}

	public void removeAppender(EventAppender appender) {
		appenders.remove(appender);
	}

	class Task implements Runnable {
		private final SearchImpl search;
		private final Connection connection;
		private final EventAppender appender;
		private final Map<String, Variable> variables;

		public Task(SearchImpl search, Connection connection, EventAppender appender, Map<String, Variable> variables) {
			this.search = search;
			this.connection = connection;
			this.appender = appender;
			this.variables = variables;
		}

		@Override
		public void run() {
			search.execute(connection, appender, variables);
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public synchronized void doAppend(DbEvent dbEvent) {
		if (!filter.contains(dbEvent.getMessage())) {
			filter.add(dbEvent.getMessage());
			for (EventAppender appender : appenders) {
				appender.doAppend(dbEvent);
			}
		}
	}
}
