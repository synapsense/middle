package com.synapsense.diagnostic.db.model;

import java.sql.Connection;
import java.util.Map;

import com.synapsense.diagnostic.db.appender.EventAppender;
import com.synapsense.diagnostic.db.variables.Variable;
import com.synapsense.diagnostic.messages.If;

public class IfImpl implements Command {
	private final ConditionsImpl conditions;
	private final ThenImpl then;

	public IfImpl(If f, Map<String, QueryImpl> queries) throws ClassNotFoundException {
		conditions = new ConditionsImpl(f.getConditions());
		then = new ThenImpl(f.getThen(), queries);
	}

	@Override
	public boolean execute(Connection connection, EventAppender appender, Map<String, Variable> variables) {
		boolean result = false;
		if (conditions.execute(connection, appender, variables)) {
			then.execute(connection, appender, variables);
		}
		return result;
	}

}
