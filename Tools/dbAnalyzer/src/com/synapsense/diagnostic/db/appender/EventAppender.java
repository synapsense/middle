package com.synapsense.diagnostic.db.appender;

import com.synapsense.diagnostic.db.model.DbEvent;

public interface EventAppender {
	public void doAppend(DbEvent dbEvent);
}
