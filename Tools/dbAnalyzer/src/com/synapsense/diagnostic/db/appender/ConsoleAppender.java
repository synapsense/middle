package com.synapsense.diagnostic.db.appender;

import com.synapsense.diagnostic.db.model.DbEvent;

/**
 * Appender for the console
 */
public class ConsoleAppender implements EventAppender {

	@Override
	public void doAppend(DbEvent dbEvent) {
		processEvent(dbEvent);
	}

	/**
	 * Display <code>DbEvent</code>
	 * 
	 * @param dbEvent
	 */
	public void processEvent(DbEvent dbEvent) {
		System.out.println(dbEvent.getMessage());
	}
}
