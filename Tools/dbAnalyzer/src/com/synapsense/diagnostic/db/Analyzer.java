package com.synapsense.diagnostic.db;

import java.io.File;

import javax.xml.bind.JAXBException;

import com.synapsense.diagnostic.db.appender.ConsoleAppender;
import com.synapsense.diagnostic.db.model.Executor;
import com.synapsense.diagnostic.db.model.ExecutorImpl;
import com.synapsense.diagnostic.db.utils.XmlConfigUtils;
import com.synapsense.diagnostic.messages.DbAnalyzer;

/**
 * The main class
 */
public class Analyzer {
	private static final String conf = "conf/dbanalyzer.xml";

	private static XmlConfigUtils xmlBuilder = null;
	private DbAnalyzer analyzer = null;

	static {
		try {
			xmlBuilder = new XmlConfigUtils("conf", DbAnalyzer.class);
		} catch (JAXBException e) {
			e.printStackTrace();
			System.exit(1);
		}
	}

	public Analyzer() {
		try {
			analyzer = xmlBuilder.read(new File(conf), DbAnalyzer.class);
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
	}

	/**
	 * Start the analyzing
	 */
	public void start() {
		Executor executor = new ExecutorImpl(analyzer);
		executor.addAppender(new ConsoleAppender());
		executor.start();
	}

	public static void main(String[] args) {
		Analyzer analyzer = new Analyzer();
		analyzer.start();
	}
}
