package com.synapsense.diagnostic.db.interpreter;

import java.util.Map;
import com.synapsense.diagnostic.db.variables.GlobalProperties;
import com.synapsense.diagnostic.db.variables.Variable;

/**
 * Interprets a message from config file
 */
public class MessageInterpreter {

	/**
	 * @param msgPattern
	 *            a message pattern
	 * @param variables
	 *            variables gotten from SQL request
	 * @return an interpreted message
	 */
	public static String getInterpretedMessage(String msgPattern, Map<String, Variable> variables) {
		String[] parts = msgPattern.split("\\$\\{");

		StringBuilder builder = new StringBuilder();
		for (String part : parts) {
			if (part.indexOf("}") != -1) {
				String[] subparts = part.split("}");
				String value = GlobalProperties.getPropertyValue(subparts[0]);
				if (value == null && subparts[0].indexOf(".") != -1) {
					String[] vars = subparts[0].split("\\.");
					Variable variable = variables.get(vars[0]);
					if (variable != null) {
						Object val = variable.getPropertyValue(vars[1]);
						builder.append(val.toString());
						for (int i = 1; i < subparts.length; i++) {
							builder.append(subparts[i]);
						}
					} else {
						builder.append("${");
						for (String subpart : subparts) {
							builder.append(subpart);
						}
						builder.append("}");
					}
				} else {
					builder.append(value);
					for (int i = 1; i < subparts.length; i++) {
						builder.append(subparts[i]);
					}
				}
			} else {
				builder.append(part);
			}
		}

		return builder.toString();
	}
}
