package com.synapsense.diagnostic.db.variables;

import java.io.FileReader;
import java.util.Properties;

public class GlobalProperties {
	private static final String conf = "conf/global.properties";
	private static final Properties properties = new Properties();

	static {
		try {
			properties.load(new FileReader(conf));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static String getPropertyValue(String name) {
		return properties.getProperty(name);
	}
}
