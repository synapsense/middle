package com.synapsense.diagnostic.db.variables;

import java.util.HashMap;
import java.util.Map;

public class Variable {
	private final Map<String, Object> properties = new HashMap<String, Object>();

	public void addProperty(String name, Object value) {
		properties.put(name, value);
	}

	public Object getPropertyValue(String name) {
		return properties.get(name);
	}
}
