@echo off

setlocal
if not defined JAVA_HOME goto :JAVA_HOME_NOT_DEFINED

echo --------------------------------------------------------------------
echo Logs analyzing...
echo --------------------------------------------------------------------
cd log
"%JAVA_HOME%\bin\java.exe" -jar lib/logAnalyzer.jar > ../log.problems
echo DB analyzing...
echo --------------------------------------------------------------------
cd ../db
"%JAVA_HOME%\bin\java.exe" -jar lib/dbAnalyzer.jar > ../db.problems
echo.
echo Analyzing was finished successfully. See results in *.problems files.
goto :EXIT

:JAVA_HOME_NOT_DEFINED
echo JAVA_HOME variable is not defined. 

:EXIT
endlocal
