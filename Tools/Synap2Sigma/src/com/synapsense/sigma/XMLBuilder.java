package com.synapsense.sigma;

import java.io.File;
import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimerTask;
import java.util.Map.Entry;

import com.synapsense.dto.CollectionTO;
import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;
import com.synapsense.sigma.xml.DataDefinition;
import com.synapsense.sigma.xml.ExchangeSet;
import com.synapsense.sigma.xml.Identification;

/**
 * Class is responsible for building XML file in specified interval
 */
public class XMLBuilder extends TimerTask {

	private String dirName;
	private Map<String, Collection<String>> types;

	public XMLBuilder(String dirName, Map<String, Collection<String>> types) {
		this.dirName = dirName;
		this.types = types;
	}

	@Override
	public void run() {
		long start = System.currentTimeMillis();
		try {
			buildXML();
			double takes = (System.currentTimeMillis() - start) / 1000.0;
			System.out.println(new Date() + "\tBuilding takes " + takes + " sec.");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Builds XML file
	 * 
	 * @throws Exception
	 *             if there is no access to ES
	 */
	private void buildXML() throws Exception {
		Map<String, ExchangeSet> chest = new HashMap<String, ExchangeSet>();

		Collection<TO<?>> datacenters = ESContext.getEnv().getObjectsByType(ESType.DC);

		String[] filter = new String[2];
		filter[0] = "name";
		for (TO<?> datacenter : datacenters) {
			String roomName = ESContext.getEnv().getPropertyValue(datacenter, "name", String.class);

			if (chest.size() != 0)
				chest.clear();
			String fileName = dirName + "/" + roomName + ".xml";
			fileName = fileName.replace('\\', '/');
			System.out.println(new Date() + "\tBuilding " + fileName + " file...");

			for (Entry<String, Collection<String>> entry : types.entrySet()) {
				Collection<TO<?>> racks = ESContext.getEnv().getRelatedObjects(datacenter, entry.getKey(), true);

				for (String prop : entry.getValue()) {

					ExchangeSet exchangeSet = chest.get(roomName + prop);

					if (exchangeSet == null) {
						exchangeSet = new ExchangeSet();
						chest.put(roomName + prop, exchangeSet);
						Identification identification = exchangeSet.getIdentification();
						identification.setRoomName(roomName);
						identification.setFileName(fileName);
						identification.setAutoUpdate(true);
						identification.setName("Measured Temperature from " + entry.getKey() + ":" + prop);
					}

					filter[1] = prop;
					Collection<CollectionTO> rackProperties = ESContext.getEnv().getPropertyValue(racks, filter);

					Map<TO<?>, TO<?>> children = new HashMap<TO<?>, TO<?>>();
					Map<TO<?>, String> rackNames = new HashMap<TO<?>, String>();

					for (CollectionTO rackResult : rackProperties) {
						ValueTO value = rackResult.getSinglePropValue(prop);
						if (value != null) {
							TO<?> child = (TO<?>) value.getValue();
							if (child != null) {
								children.put(child, rackResult.getObjId());
								rackNames.put(rackResult.getObjId(),
								        (String) (rackResult.getSinglePropValue("name").getValue()));
							}
						}
					}

					Collection<TO<?>> copy = new ArrayList<TO<?>>();
					copy.addAll(children.keySet());

					Collection<CollectionTO> sensorProperties = ESContext.getEnv().getPropertyValue(copy,
					        new String[] { "status", "lastValue" });

					for (CollectionTO sensorResult : sensorProperties) {
						TO<?> parent = children.get(sensorResult.getObjId());
						String rackName = rackNames.get(parent);
						Double lastValue = (Double) (sensorResult.getSinglePropValue("lastValue").getValue());
						Integer status = (Integer) (sensorResult.getSinglePropValue("status").getValue());

						if (lastValue != null && (lastValue > -2000 && status == 1)) {
							BigDecimal round = new BigDecimal(lastValue);
							round = round.setScale(3, RoundingMode.HALF_UP);
							exchangeSet.getValues().addExchangeValue(rackName, round);
						} else {
							System.out.println("ERR: " + sensorResult.getObjId() + " on " + parent + " has status="
							        + status + " and lasValue=" + lastValue);
						}
					}
				}
			}

			FileOutputStream fio = null;
			try {
				DataDefinition dataDefenition = new DataDefinition();
				for (ExchangeSet set : chest.values()) {
					dataDefenition.addExchangeSet(set);
				}
				File file = new File(fileName);

				fio = new FileOutputStream(file);
				fio.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n".getBytes());
				dataDefenition.serialize(fio);
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				if (fio != null)
					fio.close();
			}
		}
	}
}
