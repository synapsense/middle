package com.synapsense.sigma;

import java.io.File;
import java.io.FileReader;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Timer;
import java.util.Vector;

/**
 * The main class
 */
public class Launcher {
	private Timer builder = new Timer(true);
	private String dirName;
	private static long period;
	private static final String conf = "conf/es.properties";
	private static Map<String, Collection<String>> types = new HashMap<String, Collection<String>>();
	private boolean stop = false;

	public Launcher(String dirName) {
		this.dirName = dirName;
	}

	static {
		try {
			init();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Parses "com.synapsense.objects" line from config file and initializes
	 * "types" map.
	 * 
	 * @throws Exception
	 *             if config file doesn't exist
	 */
	private static void init() throws Exception {
		Properties properties = new Properties();
		properties.load(new FileReader(conf));
		period = Long.parseLong(properties.getProperty("com.synapsense.update.interval"));
		String[] objects = properties.getProperty("com.synapsense.objects", "").split(",");
		for (String obj : objects) {
			String[] parts = obj.split("\\.");
			if (parts.length == 2) {
				Collection<String> list = types.get(parts[0]);
				if (list == null) {
					list = new Vector<String>();
					types.put(parts[0], list);
				}
				if (!list.contains(parts[1]))
					list.add(parts[1]);
			}
		}
	}

	/**
	 * Schedules and runs XML builder
	 */
	public void start() {
		XMLBuilder xmlBuilder = new XMLBuilder(dirName, types);
		builder.schedule(xmlBuilder, 0, period * 1000);
	}

	/**
	 * @param args
	 *            The first arg must be a directory name
	 */
	public static void main(String[] args) {
		// args = new String[1];
		// args[0] = "conf/";

		if (args.length == 1) {

			File file = new File(args[0]);
			if (file.isDirectory()) {
				String dir = file.getAbsoluteFile().getAbsolutePath();
				final Launcher launcher = new Launcher(dir);
				launcher.start();

				Runtime.getRuntime().addShutdownHook(new Thread() {
					@Override
					public void run() {
						System.out.println("Good Bye...");
						launcher.stop = true;
						synchronized (launcher) {
							launcher.notify();
						}
					}
				});

				synchronized (launcher) {
					while (!launcher.stop) {
						try {
							launcher.wait();
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
				}
				return;
			}
		}

		System.out.println("Usage: java -jar lib/synap2sigma.jar dirName");
	}
}
