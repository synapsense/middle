package com.synapsense.sigma.utils;

public class Utils {

	/**
	 * @param bool
	 *            Boolean value
	 * @return if bool is true it returns "Yes", otherwise "No"
	 */
	public static String getYN(boolean bool) {
		if (bool)
			return "Yes";
		return "No";
	}
}
