package com.synapsense.sigma.xml;

import java.io.IOException;
import java.io.OutputStream;

/**
 * Class describes ExchangeSet section of XML file
 */
public class ExchangeSet implements Serializable {
	private Identification identification = new Identification();
	private ObjectDetails objectDetails = new ObjectDetails();
	private PlotDetails plotDetails = new PlotDetails();
	private ValueDetails valueDetails = new ValueDetails();
	private Values values = new Values();

	public Identification getIdentification() {
		return identification;
	}

	public void setIdentification(Identification identification) {
		this.identification = identification;
	}

	public ObjectDetails getObjectDetails() {
		return objectDetails;
	}

	public void setObjectDetails(ObjectDetails objectDetails) {
		this.objectDetails = objectDetails;
	}

	public PlotDetails getPlotDetails() {
		return plotDetails;
	}

	public void setPlotDetails(PlotDetails plotDetails) {
		this.plotDetails = plotDetails;
	}

	public ValueDetails getValueDetails() {
		return valueDetails;
	}

	public void setValueDetails(ValueDetails valueDetails) {
		this.valueDetails = valueDetails;
	}

	public Values getValues() {
		return values;
	}

	public void setValues(Values values) {
		this.values = values;
	}

	/**
	 * Serializes ExchangeSet section
	 */
	@Override
	public void serialize(OutputStream out) throws IOException {
		out.write("\t<Exchange_Set>\n".getBytes());
		if (identification != null)
			identification.serialize(out);

		if (objectDetails != null)
			objectDetails.serialize(out);

		if (plotDetails != null)
			plotDetails.serialize(out);

		if (valueDetails != null)
			valueDetails.serialize(out);

		if (values != null)
			values.serialize(out);

		out.write("\t</Exchange_Set>\n".getBytes());
	}
}
