package com.synapsense.sigma.xml;

import java.io.IOException;
import java.io.OutputStream;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.synapsense.sigma.utils.Utils;

/**
 * Class describes PlotDetails section
 */
public class PlotDetails implements Serializable {
	public static final String SCALE_TYPE_CONTINUOUS = "Continuous";

	private String name = null;

	public void setName(String name) {
		this.name = name;
	}

	private String scaleType;
	private Boolean scaleReversed = null;
	private Boolean plot3D = null;
	private Integer scaleFactor = null;
	private String useValue = null;
	private Date useDateAndTime = null;
	private Boolean totalValues = null;

	public PlotDetails() {
		this.scaleType = SCALE_TYPE_CONTINUOUS;
	}

	public PlotDetails(String name, String scaleType) {
		this.name = name;
		this.scaleType = scaleType;
	}

	public Boolean isScaleReversed() {
		return scaleReversed;
	}

	/**
	 * @param scaleReversed
	 *            Only applicable to Continuous plots, decides if scale of
	 *            legend should be reversed (i.e. Red is Low Value)
	 */
	public void setScaleReversed(Boolean scaleReversed) {
		this.scaleReversed = scaleReversed;
	}

	public Boolean isPlot3D() {
		return plot3D;
	}

	/**
	 * 
	 * @param plot3d
	 *            Only applicable to Continuous plots, will replace object with
	 *            a variable height 3D box. Height related to value.
	 */
	public void setPlot3D(Boolean plot3d) {
		plot3D = plot3d;
	}

	public Integer getScaleFactor() {
		return scaleFactor;
	}

	/**
	 * @param scaleFactor
	 *            Multiplier for 3D plots to convert value to Meters (default=1)
	 */
	public void setScaleFactor(Integer scaleFactor) {
		this.scaleFactor = scaleFactor;
	}

	public String getUseValue() {
		return useValue;
	}

	/**
	 * @param useValue
	 *            Only applicable to Time Varying values. Defines which value to
	 *            use from curve to plot with.
	 * 
	 *            Possible values (Mean, Minimum ,Maximum or "Date and Time")
	 */
	public void setUseValue(String useValue) {
		this.useValue = useValue;
	}

	public Date getUseDateAndTime() {
		return useDateAndTime;
	}

	/**
	 * @param useDateAndTime
	 *            Date to define which value to use from curve to plot with.
	 *            Only applicable when Use_Value is set to Date and Time
	 */
	public void setUseDateAndTime(Date useDateAndTime) {
		this.useDateAndTime = useDateAndTime;
	}

	public Boolean isTotalValues() {
		return totalValues;
	}

	/**
	 * For sets where a total value is calculated by summing up values of child
	 * objects (e.g. IT Equipment in a Cabinet) setting this value to yes will
	 * use that total in the plots
	 * 
	 * @param totalValues
	 */
	public void setTotalValues(Boolean totalValues) {
		this.totalValues = totalValues;
	}

	public String getName() {
		return name;
	}

	public String getScaleType() {
		return scaleType;
	}

	/**
	 * Serializes PlotDetails section
	 */
	@Override
	public void serialize(OutputStream out) throws IOException {
		out.write("\t\t<Plot_Details>\n".getBytes());

		if (name != null) {
			out.write(("\t\t\t<Plot_Name>" + name + "</Plot_Name>\n").getBytes());
		}

		out.write(("\t\t\t<Scale_Type>" + scaleType + "</Scale_Type>\n").getBytes());

		if (scaleReversed != null) {
			out.write(("\t\t\t<Scale_Reversed>" + Utils.getYN(scaleReversed.booleanValue()) + "</Scale_Reversed>\n")
			        .getBytes());
		}

		if (plot3D != null) {
			out.write(("\t\t\t<Plot_3D>" + Utils.getYN(plot3D.booleanValue()) + "</Plot_3D>\n").getBytes());
		}

		if (scaleFactor != null) {
			out.write(("\t\t\t<Plot_3D_Scale_Factor>" + scaleFactor.toString() + "</Plot_3D_Scale_Factor>\n")
			        .getBytes());
		}

		if (useValue != null) {
			out.write(("\t\t\t<Use_Value>" + useValue + "</Use_Value>\n").getBytes());
		}

		if (useDateAndTime != null) {
			String format = "dd/mm/yyy";
			Format formatter = new SimpleDateFormat(format);
			out.write(("\t\t\t<Use_at_Date>" + formatter.format(useDateAndTime) + "</Use_at_Date>\n").getBytes());

			formatter = new SimpleDateFormat("HH:mm:ss");
			out.write(("\t\t\t<Use_at_Time>" + formatter.format(useDateAndTime) + "</Use_at_Time>\n").getBytes());
		}

		if (totalValues != null) {
			out.write(("\t\t\t<Plot_Total_Values>" + Utils.getYN(totalValues.booleanValue()) + "</Plot_Total_Values>\n")
			        .getBytes());
		}

		out.write("\t\t</Plot_Details>\n".getBytes());
	}
}
