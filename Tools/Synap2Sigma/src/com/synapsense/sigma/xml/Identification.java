package com.synapsense.sigma.xml;

import java.io.IOException;
import java.io.OutputStream;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.synapsense.sigma.utils.Utils;

/**
 * Class describes Identification section
 */
public class Identification implements Serializable {
	private String name;
	private String roomName;
	private String sendingSystem = null;
	private String fileName = null;

	private Boolean autoUpdate = null;
	private String dateFormat = null;
	private Date dateAndTime = null;

	public Identification() {
		this.name = "Measured Temperature";
		this.roomName = "My Room";
		this.sendingSystem = "SynapSense";
	}

	/**
	 * @param name
	 *            This is the name used for the Exchange Set in 6Sigma, will
	 *            appear in the list of Exchange Sets in the Virtual Facility.
	 *            The same set name can be used for different sets of identical
	 *            data on different objects, i.e. Measured Power can be used for
	 *            a set of Cabinets and a set of PDUs.
	 * @param roomName
	 *            This identifies the Room the data is to be associated with. If
	 *            this does not match the name of the room model when loaded
	 *            into 6Sigma it will report an error.
	 */
	public Identification(String name, String roomName) {
		this.name = name;
		this.roomName = roomName;
	}

	public String getSendingSystem() {
		return sendingSystem;
	}

	/**
	 * 
	 * @param sendingSystem
	 *            This identifies the System that wrote the XML file. This is
	 *            used purely as feedback to the user to help them identify
	 *            where that data came from.
	 */
	public void setSendingSystem(String sendingSystem) {
		this.sendingSystem = sendingSystem;
	}

	public String getFileName() {
		return fileName;
	}

	/**
	 * 
	 * @param fileName
	 *            Name of the XML file
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public Boolean isAutoUpdate() {
		return autoUpdate;
	}

	/**
	 * 
	 * @param autoUpdate
	 *            If set to Yes the software will monitor the XML file and if it
	 *            gets updated it will automatically reparse the file, and
	 *            update the Exchange Values in the 6Sigma Model
	 */
	public void setAutoUpdate(Boolean autoUpdate) {
		this.autoUpdate = autoUpdate;
	}

	public Date getDateAndTime() {
		return dateAndTime;
	}

	/**
	 * 
	 * @param dateAndTime
	 *            Date the data was "measured" if not set the date will be taken
	 *            from the file last modification time. This date is assumed to
	 *            be in the local time of the Room the data is associated with.
	 *            It is used to place the data into the 6SigmaFM timeline
	 */
	public void setDateAndTime(Date dateAndTime) {
		this.dateAndTime = dateAndTime;
	}

	public String getName() {
		return name;
	}

	public String getRoomName() {
		return roomName;
	}

	public String getDateFormat() {
		return dateFormat;
	}

	/**
	 * @param dateFormat
	 *            Format being used for dates, if not specified is assumed to be
	 *            dd/mm/yyyy.
	 */
	public void setDateFormat(String dateFormat) {
		this.dateFormat = dateFormat;
	}

	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @param roomName
	 */
	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}

	/**
	 * Serializes Identification section
	 */
	@Override
	public void serialize(OutputStream out) throws IOException {
		out.write("\t\t<Identification>\n".getBytes());

		out.write(("\t\t\t<Set_Name>" + name + "</Set_Name>\n").getBytes());
		out.write(("\t\t\t<Room_Name>" + roomName + "</Room_Name>\n").getBytes());

		if (sendingSystem != null) {
			out.write(("\t\t\t<Sending_System>" + sendingSystem + "</Sending_System>\n").getBytes());
		}

		if (fileName != null) {
			out.write(("\t\t\t<Source_Filename>" + fileName + "</Source_Filename>\n").getBytes());
		}

		if (autoUpdate != null) {
			out.write(("\t\t\t<Auto_Update>" + Utils.getYN(autoUpdate.booleanValue()) + "</Auto_Update>\n").getBytes());
		}

		if (dateFormat != null) {
			out.write(("\t\t\t<Date_Format>" + dateFormat + "</Date_Format>\n").getBytes());
		}

		if (dateAndTime != null) {
			String format = "dd/mm/yyyy";
			if (dateFormat != null)
				format = dateFormat;

			Format formatter = new SimpleDateFormat(format);
			out.write(("\t\t\t<Date>" + formatter.format(dateAndTime) + "</Date>\n").getBytes());

			formatter = new SimpleDateFormat("HH:mm:ss");
			out.write(("\t\t\t<Time>" + formatter.format(dateAndTime) + "</Time>\n").getBytes());
		}

		out.write("\t\t</Identification>\n".getBytes());
	}
}
