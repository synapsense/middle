package com.synapsense.sigma.xml;

import java.io.IOException;
import java.io.OutputStream;

/**
 * Class describes ObjectDetails section
 */
public class ObjectDetails implements Serializable {
	public static final String OBJECT_TYPE_CABINET = "Cabinet";

	private String objectType;
	private String objectIdType;

	public ObjectDetails() {
		objectType = OBJECT_TYPE_CABINET;
		objectIdType = "Unique ID";
	}

	/**
	 * 
	 * @param objectType
	 *            This is the Type of object in 6Sigma that data is to be
	 *            associated with. It should match exactly the values used in
	 *            6Sigma.
	 */
	public void setObjectType(String objectType) {
		this.objectType = objectType;
	}

	/**
	 * @param objectIdType
	 *            This identifies the attribute of the object in 6Sigma that
	 *            will be used to uniquely identify it, typically Unique ID or
	 *            Location ID.
	 */
	public void setObjectIdType(String objectIdType) {
		this.objectIdType = objectIdType;
	}

	public ObjectDetails(String objectType, String objectIdType) {
		this.objectType = objectType;
		this.objectIdType = objectIdType;
	}

	public String getObjectType() {
		return objectType;
	}

	public String getObjectIdType() {
		return objectIdType;
	}

	/**
	 * Serializes ObjectDetails section
	 */
	@Override
	public void serialize(OutputStream out) throws IOException {
		out.write("\t\t<Object_Details>\n".getBytes());

		out.write(("\t\t\t<Object_Type>" + objectType + "</Object_Type>\n").getBytes());
		out.write(("\t\t\t<Object_ID_Type>" + objectIdType + "</Object_ID_Type>\n").getBytes());

		out.write("\t\t</Object_Details>\n".getBytes());
	}
}
