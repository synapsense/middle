package com.synapsense.sigma.xml;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Collection;
import java.util.Vector;

/**
 * Class describes DataDefinition section of XML file
 */
public class DataDefinition implements Serializable {
	private String label;
	private String version;
	private Collection<ExchangeSet> list = new Vector<ExchangeSet>();

	public DataDefinition() {
		label = "Room Constructor";
		version = "5.42";
	}

	/**
	 * 
	 * @param label
	 *            Attribute of DataDefinition section
	 * @param version
	 *            Attribute of DataDefinition section
	 */
	public DataDefinition(String label, String version) {
		this.label = label;
		this.version = version;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public void addExchangeSet(ExchangeSet set) {
		list.add(set);
	}

	public void removeExchangeSet(ExchangeSet set) {
		list.remove(set);
	}

	/**
	 * Serializes DataDefinition section
	 */
	@Override
	public void serialize(OutputStream out) throws IOException {
		out.write(("<DataDefinition label=\"" + label + "\" version=\"" + version + "\">\n").getBytes());

		for (ExchangeSet set : list)
			set.serialize(out);

		out.write("</DataDefinition>\n".getBytes());
	}
}
