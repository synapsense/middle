package com.synapsense.sigma.xml;

import java.io.IOException;
import java.io.OutputStream;

import com.synapsense.sigma.utils.Utils;

/**
 * Class describes ValueDetails section
 */
public class ValueDetails implements Serializable {
	private String unitType;
	private String unitValue;
	private String valueType;
	private String compareWithSimulated = null;
	private String thresholdType = null;
	private String thresholdValue = null;
	private Boolean calculateTotalValue = null;
	private Boolean totalInRoomSummary = null;

	public ValueDetails() {
		this.unitType = "Temperature";
		this.unitValue = "F";
		this.valueType = "Snapshot";
	}

	public ValueDetails(String unitType, String unitValue, String valueType) {
		this.unitType = unitType;
		this.unitValue = unitValue;
		this.valueType = valueType;
	}

	public String getUnitType() {
		return unitType;
	}

	/**
	 * The type of units that the data is in, these can be units currently
	 * recognised by 6Sigma (e.g. Temperature, Power) or completely new units
	 * (e.g. Currency). Unit conversion will only be available for units
	 * recognised by 6Sigma.
	 * 
	 * @param unitType
	 *            Unit Type Unit Values Temperature C, K, F, R Power W, kW,
	 *            Btu/hour, tons(R), millionBtu/hour, HP, mW VolumeFlowRate
	 *            m^3/s, l/s, cfm, gpm(US), m^3/h, l/min, m^3/min Velocity m/s,
	 *            fpm Pressure Pa, mm/H2O, mm/Hg, psi, in/H2O, in/Hg Weight kg,
	 *            g, lbs
	 */
	public void setUnitType(String unitType) {
		this.unitType = unitType;
	}

	public String getUnitValue() {
		return unitValue;
	}

	/**
	 * The value of the units to use - should match the string used by 6Sigma if
	 * the Unit Type also matched.
	 * 
	 * @param unitValue
	 *            a value
	 */
	public void setUnitValue(String unitValue) {
		this.unitValue = unitValue;
	}

	public String getValueType() {
		return valueType;
	}

	/**
	 * Are the values a single value (Snapshot) or a Time History (curve of
	 * values against time)
	 * 
	 * @param valueType
	 *            Possible values are: "Snapshot" or "Time History"
	 */
	public void setValueType(String valueType) {
		this.valueType = valueType;
	}

	public String getCompareWithSimulated() {
		return compareWithSimulated;
	}

	/**
	 * Name of an existing attribute on the object in the 6Sigma model that
	 * values are to be compared with. Setting this will enable the
	 * Simulated-Value plots.
	 * 
	 * @param compareWithSimulated
	 *            a String value
	 */
	public void setCompareWithSimulated(String compareWithSimulated) {
		this.compareWithSimulated = compareWithSimulated;
	}

	public String getThresholdType() {
		return thresholdType;
	}

	/**
	 * Set to Upper or Lower if there is a Threshold that 6Sigma should monitor.
	 * Items with values outside this threshold will generate warnings, and be
	 * coloured with a bumblebee pattern in any related plots.
	 * 
	 * @param thresholdType
	 *            Possible values are: None, Upper or Lower
	 */
	public void setThresholdType(String thresholdType) {
		this.thresholdType = thresholdType;
	}

	public String getThresholdValue() {
		return thresholdValue;
	}

	/**
	 * @param thresholdValue
	 *            Value of Threshold
	 */
	public void setThresholdValue(String thresholdValue) {
		this.thresholdValue = thresholdValue;
	}

	public Boolean isCalculateTotalValue() {
		return calculateTotalValue;
	}

	/**
	 * Should the software try and sum up matching values on child objects e.g.
	 * IT Equipment in a Cabinet
	 * 
	 * @param calculateTotalValue
	 */
	public void setCalculateTotalValue(Boolean calculateTotalValue) {
		this.calculateTotalValue = calculateTotalValue;
	}

	public Boolean isTotalInRoomSummary() {
		return totalInRoomSummary;
	}

	/**
	 * @param totalInRoomSummary
	 *            Add up all values in the model for all sets with this option
	 *            set, and summarise a total for the whole model in
	 */
	public void setTotalInRoomSummary(Boolean totalInRoomSummary) {
		this.totalInRoomSummary = totalInRoomSummary;
	}

	/**
	 * Serializes ValueDetails section
	 */
	@Override
	public void serialize(OutputStream out) throws IOException {
		out.write("\t\t<Value_Details>\n".getBytes());

		out.write(("\t\t\t<Unit_Type>" + unitType + "</Unit_Type>\n").getBytes());
		out.write(("\t\t\t<Unit_Value>" + unitValue + "</Unit_Value>\n").getBytes());
		out.write(("\t\t\t<Value_Type>" + valueType + "</Value_Type>\n").getBytes());

		if (compareWithSimulated != null) {
			out.write(("\t\t\t<Compare_With_Simulated>" + compareWithSimulated + "</Compare_With_Simulated>\n")
			        .getBytes());
		}

		if (thresholdType != null) {
			out.write(("\t\t\t<Threshold_Type>" + thresholdType + "</Threshold_Type>\n").getBytes());
		}

		if (thresholdValue != null) {
			out.write(("\t\t\t<Threshold_Value>" + thresholdValue + "</Threshold_Value>\n").getBytes());
		}

		if (calculateTotalValue != null) {
			out.write(("\t\t\t<Calculate_Total_Value>" + Utils.getYN(calculateTotalValue.booleanValue()) + "</Calculate_Total_Value>\n")
			        .getBytes());
		}

		if (totalInRoomSummary != null) {
			out.write(("\t\t\t<Total_In_Room_Summary>" + Utils.getYN(totalInRoomSummary.booleanValue()) + "</Total_In_Room_Summary>\n")
			        .getBytes());
		}

		out.write("\t\t</Value_Details>\n".getBytes());
	}
}
