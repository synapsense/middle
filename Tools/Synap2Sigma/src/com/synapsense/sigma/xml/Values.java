package com.synapsense.sigma.xml;

import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

/**
 * Class describes Values section
 */
public class Values implements Serializable {
	private Map<String, Object> exchangeValues = new HashMap<String, Object>();

	public Values() {
	}

	/**
	 * 
	 * @param key
	 *            This is ID of the actual object in the model. This is assumed
	 *            to be unique to the object over time. If the unique id's in
	 *            the sending system do not match those in 6Sigma then a mapping
	 *            dialog will be shown on the first import of data. (and
	 *            subsequent imports if new objects introduced), where the ID's
	 *            can be matched up between the 2 systems.
	 * 
	 * @param value
	 *            The value of the data for this object
	 */
	public void addExchangeValue(String key, Object value) {
		exchangeValues.put(key, value);
	}

	public void removeExchangeValue(String key) {
		exchangeValues.remove(key);
	}

	/**
	 * Serializes Values section
	 */
	@Override
	public void serialize(OutputStream out) throws IOException {
		out.write("\t\t<Values>\n".getBytes());

		for (Entry<String, Object> entry : exchangeValues.entrySet()) {
			out.write("\t\t\t<Exchange_Value>\n".getBytes());

			out.write(("\t\t\t\t<Object_ID>" + entry.getKey() + "</Object_ID>\n").getBytes());
			out.write(("\t\t\t\t<Value>" + entry.getValue().toString() + "</Value>\n").getBytes());

			out.write("\t\t\t</Exchange_Value>\n".getBytes());
		}

		out.write("\t\t</Values>\n".getBytes());
	}
}
