package com.synapsense.sigma.xml;

import java.io.IOException;
import java.io.OutputStream;

public interface Serializable {
	public void serialize(OutputStream out) throws IOException;
}
