package com.synapsense.sigma;

import java.io.FileInputStream;
import java.util.Properties;

import javax.naming.InitialContext;
import javax.security.auth.login.LoginContext;
import javax.security.auth.login.LoginException;

import com.synapsense.service.Environment;
import com.synapsense.util.JNDINames;
import com.synapsense.util.ServerLoginModule;

/**
 * Singleton class which realizes access to Environment API.
 */
public class ESContext {

	private static final String SynapEnvConfFile = "conf/es.properties";
	private static ESContext synapEnv = null;
	private String password = null;
	private String username = null;

	private Environment env = null;

	Properties connectProperties = null;

	protected static LoginContext lc;

	private boolean init() {

		if (connectProperties == null) {
			connectProperties = new Properties();
			try {
				connectProperties.load(new FileInputStream(SynapEnvConfFile));
			} catch (Exception e) {
				e.printStackTrace();
				return false;
			}
		}

		username = connectProperties.getProperty("com.synapsense.es.username");
		password = connectProperties.getProperty("com.synapsense.es.password");

		try {
			InitialContext ctx = new InitialContext(connectProperties);
			ServerLoginModule.init(ctx);
			ServerLoginModule.login(username, password);
			env = ServerLoginModule.getProxy(ctx, JNDINames.ENVIRONMENT, Environment.class);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

		return true;
	}

	public static synchronized boolean testConnect() {
		if (synapEnv == null) {
			synapEnv = new ESContext();
		}
		return synapEnv.init();
	}

	private ESContext() {
	}

	/**
	 * Return Environment instance
	 * 
	 * @return Environment instance
	 */
	public static synchronized Environment getEnv() {
		if (synapEnv == null) {
			synapEnv = new ESContext();
			synapEnv.init();
		} else {
			try {
				ServerLoginModule.login(synapEnv.username, synapEnv.password);
			} catch (LoginException e) {
				e.printStackTrace();
			}
		}
		return synapEnv.env;
	}

	public static Properties getProperties() {
		return synapEnv.connectProperties;
	}
}
