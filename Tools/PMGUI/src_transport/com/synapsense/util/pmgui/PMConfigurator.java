package com.synapsense.util.pmgui;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class PMConfigurator {
	public final static String RMI_PM_PATH = "RMIPMPath";
	public final static String RMI_TRANSPORT_PATH = "RMITransportPath";

	private String confFile = "conf/pm.properties";
	private Properties properties = new Properties();

	public PMConfigurator(String confFile) throws FileNotFoundException, IOException {
		super();
		this.confFile = confFile;
		properties.load(new FileInputStream(this.confFile));
	}

	public Object getProperty(String name) {
		return properties.get(name);
	}

	public void setProperty(String name, String value) {
		properties.setProperty(name, value);
	}
}
