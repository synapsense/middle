package com.synapsense.util.pmgui;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import org.apache.log4j.Logger;
import com.synapsense.plugin.apinew.ObjectDesc;
import com.synapsense.plugin.apinew.PropertyDesc;
import com.synapsense.plugin.apinew.Transport;
import com.synapsense.plugin.manager.DeviceManagerCore;
import com.synapsense.util.pmgui.rmiserver.PMRMIServer;

public class PMTransport extends UnicastRemoteObject implements Transport, PMRMITransport {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7024651966227472050L;
	private final static Logger logger = Logger.getLogger(PMTransport.class);
	private final static int PRIORITY = 2;
	private String servicePath = "rmi://localhost:1099/PMServer";
	private String rmiAddress = "rmi://localhost:1099/PMTransport";
	private DeviceManagerCore dm;
	private boolean inited = false;
	private String configFile = "conf/pm.properties";
	private Connector con;

	public PMTransport() throws RemoteException, MalformedURLException {
		super();
		try {
			PMConfigurator cfg = new PMConfigurator(configFile);
			servicePath = (String) cfg.getProperty(PMConfigurator.RMI_PM_PATH);
			rmiAddress = (String) cfg.getProperty(PMConfigurator.RMI_TRANSPORT_PATH);
		} catch (FileNotFoundException e1) {
			logger.warn("Cannot load config file", e1);
		} catch (IOException e1) {
			logger.warn("Cannot load config file", e1);
		}
		try {
			@SuppressWarnings("unused")
			Registry reg = LocateRegistry.createRegistry(1099);
		} catch (RemoteException e) {
			logger.warn(e.getMessage(), e);

		}
		this.dm = DeviceManagerCore.getInst();
		Naming.rebind(rmiAddress, this);
		con = new Connector(5000, servicePath, rmiAddress);
		logger.info("PMTransport has been created.");
	}

	@Override
	public HashMap<String, Object[]> getAllObjects() {
		boolean ini = true;
		HashMap<String, Object[]> pach = new HashMap<String, Object[]>();
		String[] plugins = dm.enumeratePlugins();
		Collection<ObjectDesc> pToO = new ArrayList<ObjectDesc>();
		for (int i = 0; i < plugins.length; i++) {
			pToO.addAll(dm.getPlugin(plugins[i]).getObjects());
		}
		for (ObjectDesc obj : pToO) {
			Collection<ObjectDesc> ch = obj.getChildren();
			Collection<PropertyDesc> properties = obj.getProperties();
			for (PropertyDesc propertyDesc : properties) {
				PMRMIServer pmuisrvc = con.getPmuisrvc();
				if (pmuisrvc != null) {
					try {
						pmuisrvc.setData(obj.getID(), propertyDesc.getName(),
						        dm.getPropertyValue(obj.getID(), propertyDesc.getName()),
						        dm.getPropertyValue(obj.getID(), propertyDesc.getName()).getClass().getName());
					} catch (RemoteException e) {
						logger.error("Cannot send ALL objects." + e);
						logger.trace("Stack:", e);
						ini = false;
						con.setPmuisrvc(null);
					}
				}
			}
			Object[] s = new String[0];
			Collection<String> chNames = new ArrayList<String>();
			if (ch != null) {
				for (ObjectDesc objectDesc : ch) {
					chNames.add(objectDesc.getID());
				}
				s = chNames.toArray();
			}
			pach.put(obj.getID(), s);
		}
		inited = ini;
		return pach;
	}

	@Override
	public void connect() throws IOException {
		con.start();
	}

	@Override
	public int getPriority() {
		return PRIORITY;
	}

	@Override
	public void sendData(String objectID, String propertyName, Serializable value) {
		PMRMIServer pmuisrvc = con.getPmuisrvc();
		if (pmuisrvc != null && inited) {
			try {
				con.getPmuisrvc().setData(objectID, propertyName, value, value.getClass().getName());
			} catch (RemoteException e) {
				logger.error("Cannot send data.", e);
				con.setPmuisrvc(null);
				inited = false;
			}
		}
	}

	public String getServicePath() {
		return servicePath;
	}

	public void setServicePath(String address) {
		this.servicePath = address;
	}

	@Override
	public void setPropertyValue(String objId, String propertyName, Serializable value) {
		dm.setPropertyValue(objId, propertyName, value);
		PMRMIServer pmuisrvc = con.getPmuisrvc();
		if (pmuisrvc != null) {
			try {
				pmuisrvc.setData(objId, propertyName, dm.getPropertyValue(objId, propertyName),
				        dm.getPropertyValue(objId, propertyName).getClass().getName());
			} catch (RemoteException e) {
				logger.error("Cannot send new property value." + e);
				logger.trace("Stack:", e);
				con.setPmuisrvc(null);
			}
		}
	}
}
