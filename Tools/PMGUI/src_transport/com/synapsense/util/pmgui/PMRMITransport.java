package com.synapsense.util.pmgui;

import java.io.Serializable;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.HashMap;

public interface PMRMITransport extends Remote {
	public void setPropertyValue(String objId, String propertyName, Serializable value) throws RemoteException;

	public HashMap<String, Object[]> getAllObjects() throws RemoteException;
}
