package com.synapsense.util.pmgui;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

import org.apache.log4j.Logger;

import com.synapsense.util.pmgui.rmiserver.PMRMIServer;

public class Connector extends Thread {
	private Integer lock = new Integer(0);
	private PMRMIServer pmuisrvc = null;
	private final static Logger logger = Logger.getLogger(Connector.class);
	private long connectionTimeout = 5000;
	private String servicePath;
	private String thisPath;
	private boolean state = false;

	public Connector(long timeOut, String servicePath, String thisPath) {
		super();
		this.servicePath = servicePath;
		this.connectionTimeout = timeOut;
		this.thisPath = thisPath;
		setDaemon(true);
	}

	@Override
	public void run() {

		try {
			while (true) {
				synchronized (lock) {
					if (pmuisrvc == null) {
						state = false;
						try {
							pmuisrvc = (PMRMIServer) Naming.lookup(servicePath);
							logger.info("Lookup successful.");
						} catch (NotBoundException | MalformedURLException | RemoteException e) {
							logger.error("Cannot connect...", e);
						}
					}
				}
				if (pmuisrvc != null && state == false) {
					try {
						pmuisrvc.connect(thisPath);
						state = true;
					} catch (RemoteException e) {
						logger.error(e.getLocalizedMessage(), e);
						pmuisrvc = null;
					}
				}
				Thread.sleep(connectionTimeout);
			}
		} catch (InterruptedException e1) {
			logger.error(e1.getLocalizedMessage(), e1);
		}
	}

	public PMRMIServer getPmuisrvc() {
		synchronized (lock) {
			return pmuisrvc;
		}
	}

	public void setPmuisrvc(PMRMIServer pmuisrvc) {
		synchronized (lock) {
			this.pmuisrvc = pmuisrvc;
		}
	}

}
