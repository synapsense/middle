package com.synapsense.util.pmgui.ui;

import java.awt.Dimension;
import java.awt.Insets;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTree;
import javax.swing.ScrollPaneConstants;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;
import com.synapsense.util.pmgui.Configurator;
import com.synapsense.util.pmgui.DataChangeListener;
import com.synapsense.util.pmgui.ObjectManager;
import com.synapsense.util.pmgui.TreeElement;
import com.synapsense.util.pmgui.logging.GUILogger;
import com.synapsense.util.pmgui.logging.GUILoggerFactory;
import com.synapsense.util.pmgui.rmiserver.PMRMIServerImpl;
import com.synapsense.util.pmgui.rmiserver.PMRMIServerStarter;

public class MainWindow implements DataChangeListener, MouseListener, TableModelListener {
	private static GUILogger logger = GUILoggerFactory.getInstance().createLogger(MainWindow.class);
	private final static String ROOTNAME = "Objects";
	private Configurator cfg;
	private String title = "Plugin Manager";
	private static final int width = 640;
	private static final int height = 600;
	private static final Dimension size = new Dimension(width, height);
	private Collection<TreeElement> tree;
	private ObjectManager manager;
	private JFrame gFrame = new JFrame();
	private Integer locationX = 0;
	private Integer locationY = 0;
	private JPanel gPanel = new JPanel();
	private JTree gTree = null;
	private JTable gTable = null;
	private JButton gExitButton = new JButton("Exit");
	private JButton gStartButton = new JButton("Start");
	private JTextArea gLoggerArea;

	private DefaultMutableTreeNode rootNode = new DefaultMutableTreeNode(ROOTNAME);
	private DefaultTableModel model = new DefaultTableModel() {
		private static final long serialVersionUID = 823542087107510228L;

		public boolean isCellEditable(int row, int col) {
			if (col == 0) {
				return false;
			}
			return true;
		}
	};

	public MainWindow(Configurator cfg2) {
		super();
		this.cfg = cfg2;
		gLoggerArea = GUILoggerFactory.getInstance().getGLoggerArea();

		title = (String) this.cfg.getProperty(Configurator.MAIN_WINDOW_TITLE);
		locationX = Integer.parseInt((String) cfg2.getProperty(Configurator.CONFIGURATION_WINDOW_LOCATIONX));
		locationY = Integer.parseInt((String) cfg2.getProperty(Configurator.CONFIGURATION_WINDOW_LOCATIONY));
	}

	public void show() {

		tree = manager.getTree();
		drawFrame();
		drawTree();
		drawTable(null);
		gStartButton.addMouseListener(this);
		gExitButton.addMouseListener(this);
		gStartButton.setVisible(false);
		gTable.setPreferredSize(new Dimension(10 * width, height - 200));
		gTree.setPreferredSize(new Dimension(10 * width, 100000));
		gStartButton.setPreferredSize(new Dimension(width / 8, 24));
		gExitButton.setPreferredSize(new Dimension(width / 8, 24));
		gLoggerArea.setPreferredSize(new Dimension(width / 2, logger.logSize * 24));

		JScrollPane logScrollPane = new JScrollPane(gLoggerArea);
		logScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		logScrollPane.setPreferredSize(new Dimension(width, 6 * 24));

		JScrollPane treeScrollPane = new JScrollPane(gTree);
		treeScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		treeScrollPane.setPreferredSize(new Dimension(width / 2 - 8, height - 200));

		JScrollPane tableScrollPane = new JScrollPane(gTable);
		tableScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		tableScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
		tableScrollPane.setPreferredSize(new Dimension(width / 2 - 8, height - 200));
		addElementInto(gPanel, tableScrollPane, width / 2 + 8, 0);
		addElementInto(gPanel, treeScrollPane, 0, 0);
		addElementInto(gPanel, logScrollPane, 0, height - 176);
		addElementInto(gPanel, gStartButton, 0, height - 24);
		addElementInto(gPanel, gExitButton, width - width / 8, height - 24);
		showFrame();
	}

	private void drawTable(String objId) {
		synchronized (model) {
			if (objId != null) {
				Collection<String> propertiesNames;
				Collection<String> values;
				propertiesNames = manager.getPropertiesNames(objId);
				values = new ArrayList<String>();
				for (String string : propertiesNames) {
					String value = manager.getPropertyValue(objId, string);
					values.add(value);
				}

				if (gTable != null) {
					gTable.setVisible(false);
					int r = model.getRowCount();
					for (int j = 0; j < r; j++) {
						model.removeRow(0);
					}
				} else {
					model.addColumn("Property");
					model.addColumn("Value");
					model.addTableModelListener(this);
					gTable = new JTable(model);
				}
				int nor = propertiesNames.size();
				Object[] tableProperties = propertiesNames.toArray();
				Object[] tablValues = values.toArray();
				for (int i = 0; i < nor; i++) {
					Object[] vavs = { tableProperties[i], tablValues[i] };
					model.addRow(vavs);
				}
			} else {
				model.addColumn("Property");
				model.addColumn("Value");
				model.addTableModelListener(this);
				gTable = new JTable(model);
			}
			gTable.setVisible(true);
		}
	}

	private void drawFrame() {
		gFrame.setTitle(title);
		gFrame.setLocation(locationX, locationY);
		gFrame.setResizable(false);
		gFrame.getContentPane().add(gPanel);
		gPanel.setPreferredSize(size);

	}

	private void showFrame() {
		gFrame.pack();
		gFrame.setVisible(true);
		gFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	private void drawTree() {

		if (gTree != null) {
			gTree.setVisible(false);
			rootNode.removeAllChildren();
		} else {
			gTree = new JTree(rootNode);
			gTree.addMouseListener(this);
		}
		fillTree(rootNode, tree);
		gTree.setVisible(true);
	}

	private void addElementInto(JComponent c, JComponent e, int x, int y) {
		if (c.getLayout() != null) {
			c.setLayout(null);
		}
		c.add(e);
		Insets inserts = c.getInsets();
		Dimension size = e.getPreferredSize();
		e.setBounds(inserts.left + x, inserts.top + y, size.width, size.height);
	}

	private void fillTree(DefaultMutableTreeNode node, Collection<TreeElement> tree) {
		for (TreeElement treeElement : tree) {
			DefaultMutableTreeNode nd = new DefaultMutableTreeNode(treeElement.getName());
			node.add(nd);
			fillTree(nd, treeElement.getChildren());
		}
	}

	public void setManager(ObjectManager manager) {
		this.manager = manager;
	}

	@Override
	public void onObjectsChanged() {
		drawTree();
	}

	@Override
	public void onPropertiesChanged(String objId) {
		TreePath path = gTree.getSelectionPath();
		if (path != null) {
			DefaultMutableTreeNode node = (DefaultMutableTreeNode) path.getLastPathComponent();
			String id = (String) node.getUserObject();
			if (id.equals(objId)) {
				drawTable(objId);
			}
		}
	}

	@Override
	public void mouseClicked(MouseEvent arg0) {
		TreePath path = gTree.getSelectionPath();
		if (path != null) {
			DefaultMutableTreeNode node = (DefaultMutableTreeNode) path.getLastPathComponent();
			if (node != null) {
				String id = (String) node.getUserObject();
				if (id != null && !ROOTNAME.equals(id)) {
					logger.debug("Selected item: " + id);
					drawTable(id);
				}
			}
		}
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		if (arg0.getSource().equals(gExitButton)) {
			System.exit(0);
		} else if (arg0.getSource().equals(gStartButton)) {
			((PMRMIServerImpl) PMRMIServerStarter.getServer()).startCapture();
		}
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
	}

	@Override
	public void tableChanged(TableModelEvent arg0) {
		switch (arg0.getType()) {
		case TableModelEvent.UPDATE:
			int row = arg0.getFirstRow();
			int col = arg0.getColumn();
			TreePath path = gTree.getSelectionPath();
			if (path != null) {
				DefaultMutableTreeNode node = (DefaultMutableTreeNode) path.getLastPathComponent();
				if (node != null) {
					String id = (String) node.getUserObject();
					if (id != null && !ROOTNAME.equals(id)) {
						String propertyName = (String) gTable.getValueAt(row, 0);
						String value = (String) gTable.getValueAt(row, col);

						logger.debug("\n\t -- Object[" + id + "] Property[" + propertyName + "] Value[" + value + "]");
						try {
							((PMRMIServerImpl) PMRMIServerStarter.getServer()).setRemotePropertyValue(id, propertyName,
							        value);
						} catch (RemoteException e) {
							logger.warn(e.getMessage(), e);
						}
					}
				}
			}
			break;
		}
	}
}
