package com.synapsense.util.pmgui;

import java.net.MalformedURLException;
import java.rmi.RemoteException;

import com.synapsense.util.pmgui.rmiserver.PMRMIServerStarter;
import com.synapsense.util.pmgui.ui.MainWindow;

public class PMApplication {
	private ObjectManager objMan = new ObjectManager();
	private Configurator cfg;

	public PMApplication(Configurator cfg) {
		this.cfg = cfg;
	}

	public void start() throws RemoteException, MalformedURLException {
		MainWindow window = new MainWindow(cfg);
		window.setManager(objMan);
		objMan.setChangeListener(window);
		window.show();
		PMRMIServerStarter.start((String) cfg.getProperty(Configurator.RMI_ADDRESS), objMan);
	}

}
