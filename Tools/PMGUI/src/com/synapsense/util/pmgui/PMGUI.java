package com.synapsense.util.pmgui;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.rmi.RemoteException;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

public class PMGUI {
	private final static Logger logger = Logger.getLogger(PMGUI.class);

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		PropertyConfigurator.configure("lib/log4j.properties");
		try {
			Configurator cfg = new Configurator("conf/pmui.properties");
			PMApplication appl = new PMApplication(cfg);
			appl.start();
		} catch (RemoteException e) {
			logger.fatal("Cannot start RMI server.", e);
		} catch (MalformedURLException e) {
			logger.fatal("Cannot start RMI server.", e);
		} catch (FileNotFoundException e) {
			logger.fatal("Cannot read configuration.", e);
		} catch (IOException e) {
			logger.fatal("Cannot read configuration.", e);
		}
	}

}
