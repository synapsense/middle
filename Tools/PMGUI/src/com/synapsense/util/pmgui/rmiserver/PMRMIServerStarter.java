package com.synapsense.util.pmgui.rmiserver;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import com.synapsense.util.pmgui.ObjectManager;
import com.synapsense.util.pmgui.logging.GUILogger;
import com.synapsense.util.pmgui.logging.GUILoggerFactory;

public class PMRMIServerStarter {
	private static GUILogger logger = GUILoggerFactory.getInstance().createLogger(PMRMIServerStarter.class);
	private static PMRMIServer s = null;

	public static void start(String address, ObjectManager objMan) throws RemoteException, MalformedURLException {
		try {
			@SuppressWarnings("unused")
			Registry reg = LocateRegistry.createRegistry(1099);
		} catch (RemoteException e) {
			logger.warn(e.getMessage(), e);
		}
		s = new PMRMIServerImpl();
		((PMRMIServerImpl) s).setObjMan(objMan);
		Naming.rebind(address, s);
	}

	public static PMRMIServer getServer() {
		return s;
	}
}
