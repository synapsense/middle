package com.synapsense.util.pmgui.rmiserver;

import java.io.Serializable;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;
import com.synapsense.util.pmgui.ObjectManager;
import com.synapsense.util.pmgui.PMRMITransport;
import com.synapsense.util.pmgui.logging.GUILogger;
import com.synapsense.util.pmgui.logging.GUILoggerFactory;

public class PMRMIServerImpl extends UnicastRemoteObject implements PMRMIServer {
	private static GUILogger logger = GUILoggerFactory.getInstance().createLogger(PMRMIServerImpl.class);
	private static final String NAME = "PMServer";
	private PMRMITransport transport = null;
	private String transportPath = null;
	private ObjectManager objMan;
	private Boolean started = true;
	/**
	 * 
	 */
	private static final long serialVersionUID = -5439595184209553495L;

	protected PMRMIServerImpl() throws RemoteException {
		super();
	}

	public void startCapture() {
		if (transportPath != null) {
			try {
				transport = (PMRMITransport) Naming.lookup(transportPath);
			} catch (MalformedURLException e) {
				logger.error("Lookup failed.", e);
			} catch (NotBoundException e) {
				logger.error("Lookup failed.", e);
			} catch (RemoteException e) {
				logger.error("Lookup failed.", e);
			}
			try {
				HashMap<String, Object[]> objects = transport.getAllObjects();
				objMan.setObjects(objects);
			} catch (RemoteException e) {
				logger.error("Cannot get all objects.", e);
			}
		} else {
			logger.warn("Transport isn't registered.");
			synchronized (started) {
				started = true;
			}
		}
	}

	@Override
	public String getServiceName() throws RemoteException {
		return NAME;
	}

	@Override
	public void setData(String objectId, String propertyName, Serializable value, String className)
	        throws RemoteException {

		if (transport != null) {
			logger.debug("\n\tObject:" + objectId + "\n\tProperty:" + propertyName + "\n\tValue:" + value);
			objMan.setPropertyValue(objectId, propertyName, value.toString(), className);
		} else {
			logger.warn("Capturing isn't started.");
		}
	}

	@Override
	public void connect(String path) throws RemoteException {
		transportPath = path;
		logger.info("Registered " + transportPath);
		synchronized (started) {
			if (started) {
				startCapture();
			}
		}
	}

	public ObjectManager getObjMan() {
		return objMan;
	}

	public void setObjMan(ObjectManager objMan) {
		this.objMan = objMan;
	}

	public void setRemotePropertyValue(String objId, String propertyName, String value) throws RemoteException {
		Serializable newValue = convertValue(objId, propertyName, value);
		if (newValue != null) {
			if (transport != null) {
				transport.setPropertyValue(objId, propertyName, newValue);
			}
		} else {
			logger.error("Cannot set value(UNKNOWN value class).");
		}
	}

	private Serializable convertValue(String objId, String propertyName, String value) {
		Serializable res = null;
		String className = objMan.getPropertyClassName(objId, propertyName);
		if ("java.lang.String".equals(className)) {
			res = value;
		} else if ("java.lang.Integer".equals(className)) {
			res = Integer.parseInt(value);
		} else if ("java.lang.Long".equals(className)) {
			res = Long.parseLong(value);
		} else if ("java.lang.Double".equals(className)) {
			res = Double.parseDouble(value);
		} else if ("java.lang.Float".equals(className)) {
			res = Float.parseFloat(value);
		}
		return res;
	}

	public String getTransportPath() {
		return transportPath;
	}

}
