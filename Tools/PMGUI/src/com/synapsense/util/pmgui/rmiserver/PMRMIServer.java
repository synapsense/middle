package com.synapsense.util.pmgui.rmiserver;

import java.io.Serializable;
import java.rmi.Remote;
import java.rmi.RemoteException;

public interface PMRMIServer extends Remote {
	public void connect(String path) throws RemoteException;

	public void setData(String objectId, String propertyName, Serializable value, String className)
	        throws RemoteException;

	public String getServiceName() throws RemoteException;
}
