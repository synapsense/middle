package com.synapsense.util.pmgui;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

public class TreeElement {
	private String rootName;
	private Collection<TreeElement> e = new ArrayList<TreeElement>();

	public TreeElement(String rootName, HashMap<String, Object[]> objects, HashMap<String, TreeElement> processed) {
		this.rootName = rootName;
		Object[] children = objects.get(rootName);
		processed.put(rootName, this);
		if (children != null) {
			for (int i = 0; i < children.length; i++) {
				if (processed.containsKey(children[i])) {
					e.add(processed.get(children[i]));
				} else {
					TreeElement te = new TreeElement((String) children[i], objects, processed);
					e.add(te);
					processed.put((String) children[i], te);
				}
				objects.remove(children[i]);
			}
		}
	}

	public Collection<TreeElement> getChildren() {
		return e;
	}

	public String getName() {
		return rootName;
	}
}
