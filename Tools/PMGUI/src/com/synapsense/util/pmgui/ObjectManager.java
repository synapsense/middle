package com.synapsense.util.pmgui;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import com.synapsense.util.pmgui.logging.GUILogger;
import com.synapsense.util.pmgui.logging.GUILoggerFactory;

public class ObjectManager {
	private static GUILogger logger = GUILoggerFactory.getInstance().createLogger(ObjectManager.class);
	private Collection<TreeElement> tree = new ArrayList<TreeElement>();
	private HashMap<String, HashMap<String, String>> opv = new HashMap<String, HashMap<String, String>>();
	private HashMap<String, HashMap<String, String>> opc = new HashMap<String, HashMap<String, String>>();
	private DataChangeListener changeListener = null;

	public void setObjects(HashMap<String, Object[]> objects) {
		HashMap<String, TreeElement> p = new HashMap<String, TreeElement>();
		Object[] keys = objects.keySet().toArray();
		for (Object string : keys) {
			new TreeElement((String) string, objects, p);
		}
		keys = objects.keySet().toArray();
		tree.clear();
		for (Object string : keys) {
			TreeElement te = p.get(string);
			tree.add(te);
		}
		updateO();
	}

	public void setPropertyValue(String objId, String propertyName, String value, String className) {
		synchronized (opv) {
			HashMap<String, String> properties = opv.get(objId);
			HashMap<String, String> propertyToClassMap = opc.get(objId);
			if (properties == null) {
				properties = new HashMap<String, String>();
				opv.put(objId, properties);
				propertyToClassMap = new HashMap<String, String>();
				opc.put(objId, propertyToClassMap);
			}
			properties.put(propertyName, value);
			propertyToClassMap.put(propertyName, className);
			updateP(objId);
		}
	}

	public String getPropertyValue(String objId, String propertyName) {
		String value = null;
		synchronized (opv) {
			HashMap<String, String> properties = opv.get(objId);
			if (properties != null) {
				value = properties.get(propertyName);
				if (value == null) {
					logger.warn("Object '" + objId + "' doesn't have property '" + propertyName + "'");
				}
			} else {
				logger.warn("Cannot find object '" + objId + "'");
			}
		}
		return value;
	}

	public String getPropertyClassName(String objId, String propertyName) {
		String value = null;
		synchronized (opv) {
			HashMap<String, String> propertyToClassMap = opc.get(objId);
			if (propertyToClassMap != null) {
				value = propertyToClassMap.get(propertyName);
				if (value == null) {
					logger.warn("Object '" + objId + "' doesn't have property '" + propertyName + "'");
				}
			} else {
				logger.warn("Cannot find object '" + objId + "'");
			}
		}
		return value;
	}

	public Collection<String> getPropertiesNames(String objId) {
		Collection<String> value = null;
		HashMap<String, String> properties = opv.get(objId);
		if (properties != null) {
			value = properties.keySet();
		} else {
			logger.warn("Cannot find object '" + objId + "'");
		}
		return value;
	}

	public Collection<TreeElement> getTree() {
		return tree;
	}

	private void updateO() {
		if (changeListener != null) {
			changeListener.onObjectsChanged();
		}
	}

	private void updateP(String objId) {
		if (changeListener != null) {
			changeListener.onPropertiesChanged(objId);
		}
	}

	public DataChangeListener getChangeListener() {
		return changeListener;
	}

	public void setChangeListener(DataChangeListener changeListener) {
		this.changeListener = changeListener;
	}
}
