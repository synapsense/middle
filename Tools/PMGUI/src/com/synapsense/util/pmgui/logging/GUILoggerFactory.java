package com.synapsense.util.pmgui.logging;

import java.util.LinkedList;

import javax.swing.JTextArea;

public class GUILoggerFactory {
	private static JTextArea gLoggerArea = null;
	private static GUILoggerFactory factory = null;
	private static LinkedList<String> log = null;

	public static GUILoggerFactory getInstance() {
		if (factory == null) {
			gLoggerArea = new JTextArea();
			log = new LinkedList<String>();
			factory = new GUILoggerFactory();
		}
		return factory;
	}

	public GUILogger createLogger(Class<?> clazz) {
		return new GUILogger(clazz, gLoggerArea, log);
	}

	public JTextArea getGLoggerArea() {
		return gLoggerArea;
	}

	public void setGLoggerArea(JTextArea loggerArea) {
		gLoggerArea = loggerArea;
	}
}
