package com.synapsense.util.pmgui.logging;

import java.util.LinkedList;

import javax.swing.JTextArea;

import org.apache.log4j.Logger;

public class GUILogger {
	public final int logSize = 4096;
	private Logger logger = null;
	private Class<?> clazz;
	private JTextArea gLogger = null;
	private LinkedList<String> log;

	public GUILogger(Class<?> clazz, JTextArea gLogger, LinkedList<String> log) {
		super();
		this.clazz = clazz;
		this.gLogger = gLogger;
		this.log = log;
		logger = Logger.getLogger(this.clazz);
	}

	public void warn(String mes) {
		if (logger != null) {
			logger.warn(mes);
			if (gLogger != null) {
				append("WARN: " + mes + "\n");
			}
		}
	}

	public void warn(String mes, Throwable t) {
		if (logger != null) {
			logger.warn(mes, t);
			if (gLogger != null) {
				StackTraceElement[] tr = t.getStackTrace();
				append("WARN: " + mes + "\n");
				for (int i = 0; i < tr.length; i++) {
					append(tr[i].toString() + "\n");
				}
			}
		}
	}

	public void error(String mes) {
		if (logger != null) {
			logger.error(mes);
			if (gLogger != null) {
				append("ERROR: " + mes + "\n");
			}
		}
	}

	public void error(String mes, Throwable t) {
		if (logger != null) {
			logger.error(mes, t);
			if (gLogger != null) {
				StackTraceElement[] tr = t.getStackTrace();
				append("ERROR: " + mes + "\n");
				for (int i = 0; i < tr.length; i++) {
					append(tr[i].toString() + "\n");
				}
			}
		}
	}

	public void info(String mes) {
		if (logger != null) {
			logger.info(mes);
			if (gLogger != null) {
				append("INFO: " + mes + "\n");
			}
		}
	}

	public void info(String mes, Throwable t) {
		if (logger != null) {
			logger.info(mes, t);
			if (gLogger != null) {
				StackTraceElement[] tr = t.getStackTrace();
				append("INFO: " + mes + "\n");
				for (int i = 0; i < tr.length; i++) {
					append(tr[i].toString() + "\n");
				}
			}
		}
	}

	public void debug(String mes) {
		if (logger != null) {
			logger.debug(mes);
			if (gLogger != null && logger.isDebugEnabled()) {
				append("DEBUG: " + mes + "\n");
			}
		}
	}

	public void debug(String mes, Throwable t) {
		if (logger != null) {
			logger.debug(mes, t);
			if (gLogger != null && logger.isDebugEnabled()) {
				StackTraceElement[] tr = t.getStackTrace();
				append("DEBUG: " + mes + "\n");
				for (int i = 0; i < tr.length; i++) {
					append(tr[i].toString() + "\n");
				}
			}
		}
	}

	public void trace(String mes) {
		if (logger != null) {
			logger.trace(mes);
			if (gLogger != null && logger.isTraceEnabled()) {
				append("TRACE: " + mes + "\n");
			}
		}
	}

	public void trace(String mes, Throwable t) {
		if (logger != null) {
			logger.trace(mes, t);
			if (gLogger != null && logger.isTraceEnabled()) {
				StackTraceElement[] tr = t.getStackTrace();
				append("TRACE: " + mes + "\n");
				for (int i = 0; i < tr.length; i++) {
					append(tr[i].toString() + "\n");
				}
			}
		}
	}

	private void append(String l) {
		synchronized (gLogger) {
			String[] tmp = l.split("\n");
			gLogger.setText("");
			for (int i = 0; i < tmp.length; i++) {
				if (log.size() >= logSize) {
					log.removeFirst();
				}
				log.add(tmp[i] + "\n");
			}
			StringBuffer sb = new StringBuffer();
			for (String mes : log) {
				sb.append(mes);
			}
			gLogger.setText(sb.toString());
		}
	}
}
