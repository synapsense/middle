package com.synapsense.util.pmgui;

public interface DataChangeListener {
	public void onObjectsChanged();

	public void onPropertiesChanged(String objId);
}
