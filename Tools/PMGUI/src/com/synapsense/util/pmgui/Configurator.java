package com.synapsense.util.pmgui;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class Configurator {
	public final static String RMI_ADDRESS = "RMIAddress";

	public final static String MAIN_WINDOW_TITLE = "MainWindowTitle";
	public final static String MAIN_WINDOW_LOCATIONX = "MainWindowLocationX";
	public final static String MAIN_WINDOW_LOCATIONY = "MainWindowLocationY";

	public final static String CONFIGURATION_WINDOW_TITLE = "ConfigurationWindowTitle";
	public final static String CONFIGURATION_WINDOW_LOCATIONX = "ConfigurationWindowLocationX";
	public final static String CONFIGURATION_WINDOW_LOCATIONY = "ConfigurationWindowLocationY";

	private String confFile = "conf/pmui.properties";
	private Properties properties = new Properties();

	public Configurator(String confFile) throws FileNotFoundException, IOException {
		super();
		this.confFile = confFile;
		properties.load(new FileInputStream(this.confFile));
	}

	public Object getProperty(String name) {
		return properties.get(name);
	}

	public void setProperty(String name, String value) {
		properties.setProperty(name, value);
	}
}
