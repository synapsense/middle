<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
		xmlns="http://graphml.graphdrawing.org/xmlns"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
		xmlns:ei="http://synapsense.com/ejb-info"
		xsi:schemaLocation="http://synapsense.com/ejb-info ../out/ejb-info.xsd
							http://graphml.graphdrawing.org/xmlns http://graphml.graphdrawing.org/xmlns/1.0/graphml.xsd">
	
	<xsl:output method="xml" indent="yes"/>
	
	<xsl:template match="/">

		<!-- begin  graphml-->
		<graphml xsi:schemaLocation="http://graphml.graphdrawing.org/xmlns http://graphml.graphdrawing.org/xmlns/1.0/graphml.xsd">
		<key attr.name="name" attr.type="string" for="node" id="d0"/>
		<key attr.name="startup" attr.type="boolean" for="node" id="d1">
			<default>false</default>
		</key>
		<key attr.name="type" attr.type="string" for="node" id="d2">
			<default>unknown</default>
		</key>
		<key attr.name="name" attr.type="string" for="edge" id="d10"/>
		<key attr.name="type" attr.type="string" for="edge" id="d11">
			<default>ejb</default>
		</key>
		
		<!-- begin graph -->
		<graph id="G" edgedefault="directed">

		<!-- write nodes -->
		<xsl:for-each select="ei:units//ei:ejb">
			<xsl:element name="node">
				<xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute>
				<xsl:element name="data">
					<xsl:attribute name="key">d0</xsl:attribute>
					<xsl:value-of select="@name"/>
				</xsl:element>
				<xsl:choose>
					<xsl:when test="@xsi:type='ei:ssbDescriptor'">
						<xsl:element name="data">
							<xsl:attribute name="key">d2</xsl:attribute>
							<xsl:text>ssb</xsl:text>
						</xsl:element>
						<xsl:if test="@startup='true'">
							<xsl:element name="data">
								<xsl:attribute name="key">d1</xsl:attribute>
								<xsl:text>true</xsl:text>
							</xsl:element>
						</xsl:if>
					</xsl:when>
					<xsl:when test="@xsi:type='ei:slsbDescriptor'">
						<xsl:element name="data">
							<xsl:attribute name="key">d2</xsl:attribute>
							<xsl:text>slsb</xsl:text>
						</xsl:element>
					</xsl:when>
					<xsl:when test="@xsi:type='ei:sfsbDescriptor'">
						<xsl:element name="data">
							<xsl:attribute name="key">d2</xsl:attribute>
							<xsl:text>sfsb</xsl:text>
						</xsl:element>
					</xsl:when>
					<xsl:when test="@xsi:type='ei:mdbDescriptor'">
						<xsl:element name="data">
							<xsl:attribute name="key">d2</xsl:attribute>
							<xsl:text>mdb</xsl:text>
						</xsl:element>
					</xsl:when>
				</xsl:choose>
			</xsl:element>
		</xsl:for-each>
		
		<!-- write dependencies as edges -->		
		<xsl:for-each select="ei:units//ei:ejb/ei:dependencies/ei:dependency[@resolvedTo]">
			<xsl:variable name="ejbName" select="../../@name"/>
			<xsl:variable name="resolvedTo" select="@resolvedTo"/>
			<xsl:variable name="byMember" select="@byMember"/>
			<xsl:variable name="depName" select="/ei:units//ei:ejb[@id=$resolvedTo]/@name"/>
				
			<xsl:element name="edge">
				<xsl:attribute name="source"><xsl:value-of select="$ejbName"/></xsl:attribute>
				<xsl:attribute name="target"><xsl:value-of select="$depName"/></xsl:attribute>
				<xsl:element name="data">
					<xsl:attribute name="key">d10</xsl:attribute>
					<xsl:value-of select="$byMember"/>
				</xsl:element>
			</xsl:element>
		</xsl:for-each>

		<!-- write singleton startup dependencies -->		
		<xsl:for-each select="ei:units//ei:ejb[@xsi:type='ei:ssbDescriptor']/ei:startupDependencies/ei:dependency[@resolvedTo]">
			<xsl:variable name="ssbName" select="../../@name"/>
			<xsl:variable name="resolvedTo" select="@resolvedTo"/>
			<xsl:variable name="depName" select="/ei:units//ei:ejb[@id=$resolvedTo]/@name"/>
				
			<xsl:element name="edge">
				<xsl:attribute name="source"><xsl:value-of select="$ssbName"/></xsl:attribute>
				<xsl:attribute name="target"><xsl:value-of select="$depName"/></xsl:attribute>
				<data key="d11">singleton</data>
			</xsl:element>
		</xsl:for-each>
		
		<!-- end graph -->
		</graph>

		<!-- begin  graphml-->
		</graphml>

	</xsl:template>
</xsl:stylesheet>