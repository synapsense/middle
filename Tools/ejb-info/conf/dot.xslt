<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
		xmlns:ei="http://synapsense.com/ejb-info" xsi:schemaLocation="http://synapsense.com/ejb-info ../out/ejb-info.xsd">
	
	<xsl:output method="text"/>
		
	<xsl:template match="/">

		<!-- begin digraph -->
		<xsl:text>digraph G {&#10;</xsl:text>

		<!-- Process singleton startup dependencies -->		
		<xsl:for-each select="ei:units//ei:ejb[@xsi:type='ei:ssbDescriptor']/ei:startupDependencies/ei:dependency[@resolvedTo]">
			<xsl:variable name="ssb_name" select="../../@name"/>
			<xsl:variable name="resolvedTo" select="@resolvedTo"/>
			<xsl:variable name="depName" select="/ei:units//ei:ejb[@id=$resolvedTo]/@name"/>
				
			<xsl:text>&#9;</xsl:text><xsl:value-of select="$ssb_name"/>
			<xsl:text> -> </xsl:text><xsl:value-of select="$depName"/>
			<xsl:text> [style=dashed]</xsl:text>
			<xsl:text>;&#10;</xsl:text>
		</xsl:for-each>

		<!-- Process EJB dependencies -->		
		<xsl:for-each select="ei:units//ei:ejb/ei:dependencies/ei:dependency[@resolvedTo]">
			<xsl:variable name="ejb_name" select="../../@name"/>
			<xsl:variable name="resolvedTo" select="@resolvedTo"/>
			<xsl:variable name="byMember" select="@byMember"/>
			<xsl:variable name="depName" select="/ei:units//ei:ejb[@id=$resolvedTo]/@name"/>
				
			<xsl:text>&#9;</xsl:text><xsl:value-of select="$ejb_name"/>
			<xsl:text> -> </xsl:text><xsl:value-of select="$depName"/>
			<xsl:text> [label=&quot;</xsl:text><xsl:value-of select="$byMember"/><xsl:text>&quot;]</xsl:text>
			<xsl:text>;&#10;</xsl:text>
		</xsl:for-each>
		
		<!-- Make distinct shapes -->
		<xsl:for-each select="ei:units//ei:ejb[@xsi:type='ei:ssbDescriptor']">
			<xsl:text>&#9;</xsl:text><xsl:value-of select="@name"/>
			<xsl:text> [shape=box</xsl:text>
			<xsl:if test="@startup='true'">
				<xsl:text>,color=red</xsl:text>
			</xsl:if>
			<xsl:text>]</xsl:text>
			<xsl:text>;&#10;</xsl:text>
		</xsl:for-each>

		<!-- end digraph -->
		<xsl:text>}</xsl:text>

	</xsl:template>
</xsl:stylesheet>