package com.synapsense.ejbinfo.util.xml;

import java.io.InputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.bootstrap.DOMImplementationRegistry;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSInput;
import org.w3c.dom.ls.LSResourceResolver;

public class ClasspathResourceResolver implements LSResourceResolver {

    private static final Logger log = LoggerFactory
	    .getLogger(ClasspathResourceResolver.class);

    private final DOMImplementationLS domImplementationLS;

    private final String schemasRoot;

    public ClasspathResourceResolver(String schemasRoot)
	    throws ClassNotFoundException, IllegalAccessException,
	    InstantiationException {
	this.schemasRoot = schemasRoot;
	DOMImplementationRegistry registry = DOMImplementationRegistry
		.newInstance();
	domImplementationLS = (DOMImplementationLS) registry
		.getDOMImplementation("LS");
    }

    @Override
    public LSInput resolveResource(String type, String namespaceURI,
	    String publicId, String systemId, String baseURI) {
	if (systemId.endsWith("xml.xsd"))
	    systemId = "xml.xsd";

	log.debug("Resolving " + systemId + " schema");

	LSInput lsInput = domImplementationLS.createLSInput();

	String resourceName = "/" + schemasRoot + "/" + systemId;
	InputStream is = getClass().getResourceAsStream(resourceName);
	if (is == null) {
	    throw new RuntimeException("Cannot find classpath resource "
		    + resourceName);
	}

	lsInput.setByteStream(is);
	lsInput.setSystemId(systemId);
	return lsInput;
    }
}