package com.synapsense.ejbinfo.units.beans.session.ssb;

import java.util.Collection;
import java.util.concurrent.ConcurrentLinkedQueue;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlType;

import com.synapsense.ejbinfo.units.beans.BeanDescriptor;
import com.synapsense.ejbinfo.util.xml.UnitsObjectFactory;

@XmlType(factoryClass = UnitsObjectFactory.class, factoryMethod = "createSsbDescriptor")
@XmlAccessorType(XmlAccessType.PROPERTY)
public class SsbDescriptor extends BeanDescriptor {

    private final boolean startup;
    private final Collection<StartupDependency> startupDependencies;

    public SsbDescriptor(String name, Class<?> clazz, boolean startup) {
	super(name, clazz);
	this.startup = startup;
	startupDependencies = new ConcurrentLinkedQueue<>();
    }

    @XmlAttribute(required = true)
    public boolean isStartup() {
	return startup;
    }

    public void addStartupDependency(StartupDependency dependency) {
	startupDependencies.add(dependency);
    }

    @XmlElement(name = "dependency")
    @XmlElementWrapper(name = "startupDependencies")
    public Collection<StartupDependency> getStartupDependencies() {
	return startupDependencies;
    }

}
