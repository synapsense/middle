package com.synapsense.ejbinfo.units.beans.session.ssb;

import java.util.Objects;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.XmlType;

import com.synapsense.ejbinfo.units.beans.BeanDescriptor;
import com.synapsense.ejbinfo.util.xml.UnitsObjectFactory;

@XmlType(factoryClass = UnitsObjectFactory.class, factoryMethod = "createStartupDependency")
@XmlAccessorType(XmlAccessType.PROPERTY)
public class StartupDependency {
    @XmlAttribute
    public final String name;

    @XmlIDREF
    @XmlAttribute(required = false)
    public BeanDescriptor resolvedTo;

    public StartupDependency(String name) {
	this.name = Objects.requireNonNull(name, "name must not be null");
    }

}
