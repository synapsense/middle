package com.synapsense.ejbinfo.units.beans;

import java.util.Collection;
import java.util.Collections;
import java.util.Objects;
import java.util.concurrent.ConcurrentLinkedQueue;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import com.synapsense.ejbinfo.units.UnitDescriptor;
import com.synapsense.ejbinfo.units.beans.mdb.MdbDescriptor;
import com.synapsense.ejbinfo.units.beans.session.Export;
import com.synapsense.ejbinfo.units.beans.session.sfsb.SfsbDescriptor;
import com.synapsense.ejbinfo.units.beans.session.slsb.SlsbDescriptor;
import com.synapsense.ejbinfo.units.beans.session.ssb.SsbDescriptor;
import com.synapsense.ejbinfo.util.xml.UnitsObjectFactory;

@XmlType(factoryClass = UnitsObjectFactory.class, factoryMethod = "createBeanDescriptor")
@XmlAccessorType(XmlAccessType.PROPERTY)
@XmlSeeAlso({ SsbDescriptor.class, SlsbDescriptor.class, SfsbDescriptor.class,
	MdbDescriptor.class })
public class BeanDescriptor {

    private final String name;
    private final Class<?> clazz;

    private final Collection<Dependency> dependencies;
    private final Collection<Export> exports;

    private UnitDescriptor unit;

    public BeanDescriptor(String name, Class<?> clazz) {
	this.name = Objects.requireNonNull(name, "name must not be null");
	this.clazz = Objects.requireNonNull(clazz, "clazz must not be null");
	dependencies = new ConcurrentLinkedQueue<>();
	exports = new ConcurrentLinkedQueue<>();
    }

    @XmlAttribute(required = true)
    public String getName() {
	return name;
    }

    @XmlAttribute(name = "class", required = true)
    public Class<?> getBeanClass() {
	return clazz;
    }

    @XmlID
    @XmlAttribute(required = true)
    public String getId() {
	return "_" + Integer.toString(System.identityHashCode(this)) + "_";
    }

    public void addDependency(Dependency dependency) {
	dependency.bean = this;
	dependencies.add(dependency);
    }

    @XmlElement(name = "dependency")
    @XmlElementWrapper(name = "dependencies")
    public Collection<Dependency> getDependencies() {
	return Collections.unmodifiableCollection(dependencies);
    }

    public void addExport(Export export) {
	export.bean = this;
	exports.add(export);
    }

    @XmlElement(name = "export")
    @XmlElementWrapper(name = "exports")
    public Collection<Export> getExports() {
	return Collections.unmodifiableCollection(exports);
    }

    @XmlTransient
    public UnitDescriptor getUnit() {
	return unit;
    }

    public void setUnit(UnitDescriptor unit) {
	this.unit = unit;
    }

}
