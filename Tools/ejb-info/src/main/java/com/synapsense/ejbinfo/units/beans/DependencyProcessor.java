package com.synapsense.ejbinfo.units.beans;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Set;

import javax.ejb.EJB;

import org.reflections.ReflectionUtils;
import org.reflections.Reflections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.synapsense.ejbinfo.util.reflection.ReflectUtils;

public class DependencyProcessor implements BeanProcessor {

    private final static Logger log = LoggerFactory
	    .getLogger(DependencyProcessor.class);

    @Override
    public BeanDescriptor process(Class<?> bean, BeanDescriptor descr,
	    Reflections reflections) {
	processFields(bean, descr);
	processMethods(bean, descr);
	return descr;
    }

    private static void processFields(Class<?> bean, BeanDescriptor descr) {
	// find fields annotated with @EJB
	Set<Field> fields = ReflectionUtils.getAllFields(bean,
		ReflectionUtils.withAnnotation(EJB.class));
	for (Field field : fields) {

	    String beanName = (String) ReflectUtils.getAnnotationValue(field,
		    EJB.class, "beanName");
	    if (beanName.isEmpty())
		beanName = null;

	    Class<?> beanInterface = (Class<?>) ReflectUtils
		    .getAnnotationValue(field, EJB.class, "beanInterface");
	    if (beanInterface.equals(Object.class))
		beanInterface = field.getType();

	    Dependency dep = new Dependency(beanInterface, field.getName(),
		    beanName);

	    descr.addDependency(dep);
	    log.info(String
		    .format("Found EJB dependency field: %s.%s (beanInterface=%s, beanName=\"%s\")",
			    bean.getSimpleName(), field.getName(),
			    dep.iface.getName(), dep.name));
	}
    }

    private static void processMethods(Class<?> bean, BeanDescriptor descr) {
	// find fields annotated with @EJB
	Set<Method> methods = ReflectionUtils.getAllMethods(bean,
		ReflectionUtils.withAnnotation(EJB.class));
	for (Method method : methods) {

	    String beanName = (String) ReflectUtils.getAnnotationValue(method,
		    EJB.class, "beanName");
	    if (beanName.isEmpty())
		beanName = null;

	    Class<?> beanInterface = (Class<?>) ReflectUtils
		    .getAnnotationValue(method, EJB.class, "beanInterface");
	    if (beanInterface.equals(Object.class)) {
		// this must be a setter with exactly one parameter
		beanInterface = method.getParameterTypes()[0];
	    }

	    Dependency dep = new Dependency(beanInterface, method.getName(),
		    beanName);

	    descr.addDependency(dep);
	    log.info(String
		    .format("Found EJB dependency method: %s.%s() (beanInterface=%s, beanName=\"%s\")",
			    bean.getSimpleName(), method.getName(),
			    dep.iface.getName(), dep.name));
	}
    }

}
