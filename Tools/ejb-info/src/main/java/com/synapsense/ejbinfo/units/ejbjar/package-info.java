@XmlSchema(namespace = "http://synapsense.com/ejb-info", xmlns = { @XmlNs(prefix = "ei", namespaceURI = "http://synapsense.com/ejb-info") }, elementFormDefault = XmlNsForm.QUALIFIED, attributeFormDefault = XmlNsForm.UNQUALIFIED)
package com.synapsense.ejbinfo.units.ejbjar;

import javax.xml.bind.annotation.XmlNs;
import javax.xml.bind.annotation.XmlNsForm;
import javax.xml.bind.annotation.XmlSchema;

