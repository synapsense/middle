package com.synapsense.ejbinfo.units.beans.session.slsb;

import javax.ejb.Stateless;

import org.reflections.Reflections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.synapsense.ejbinfo.units.beans.BeanDescriptor;
import com.synapsense.ejbinfo.units.beans.session.ExportProcessor;
import com.synapsense.ejbinfo.util.reflection.ReflectUtils;

public class SlsbProcessor extends ExportProcessor {

    private static final Logger log = LoggerFactory
	    .getLogger(SlsbProcessor.class);

    @Override
    public BeanDescriptor process(Class<?> bean, BeanDescriptor descr,
	    Reflections reflections) {
	// create descriptor first
	String name = (String) ReflectUtils.getAnnotationValue(bean,
		Stateless.class, "name");
	if (name.isEmpty())
	    name = bean.getSimpleName();
	log.info("Found SLSB: " + name + "(" + bean.getName() + ")");

	SlsbDescriptor newDescr = new SlsbDescriptor(name, bean);

	return super.process(bean, newDescr, reflections);
    }

}
