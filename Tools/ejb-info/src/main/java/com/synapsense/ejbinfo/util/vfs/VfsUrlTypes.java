package com.synapsense.ejbinfo.util.vfs;

import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.jar.JarFile;

import org.reflections.vfs.SystemDir;
import org.reflections.vfs.Vfs.Dir;
import org.reflections.vfs.Vfs.UrlType;
import org.reflections.vfs.ZipDir;

public enum VfsUrlTypes implements UrlType {

    zipJar {
	@Override
	public boolean matches(URL url) {
	    if (!url.getProtocol().equals("file")
		    || !url.toExternalForm().contains(".jar"))
		return false;
	    Path path;
	    try {
		path = Paths.get(url.toURI());
	    } catch (URISyntaxException e) {
		return false;
	    }
	    return !Files.isDirectory(path);
	}

	@Override
	public Dir createDir(final URL url) throws Exception {
	    Path path = Paths.get(url.toURI());
	    return new ZipDir(new JarFile(path.toFile()));
	}
    },

    folderJar {
	@Override
	public boolean matches(URL url) {
	    if (!url.getProtocol().equals("file")
		    || !url.toExternalForm().contains(".jar"))
		return false;
	    Path path;
	    try {
		path = Paths.get(url.toURI());
	    } catch (URISyntaxException e) {
		return false;
	    }
	    return Files.isDirectory(path);
	}

	@Override
	public Dir createDir(final URL url) throws Exception {
	    Path path = Paths.get(url.toURI());
	    return new SystemDir(path.toFile());
	}
    }
}
