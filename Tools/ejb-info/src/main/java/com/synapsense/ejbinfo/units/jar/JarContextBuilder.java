package com.synapsense.ejbinfo.units.jar;

import org.jboss.vfs.VirtualFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.synapsense.ejbinfo.context.Context;
import com.synapsense.ejbinfo.context.ContextProcessor;

public class JarContextBuilder implements ContextProcessor {

    private static final Logger log = LoggerFactory
	    .getLogger(JarContextBuilder.class);

    @Override
    public boolean supports(Context context) {
	if (!context.getName().endsWith(".jar"))
	    return false;
	Context parent = context.getParent();
	return parent == null || !parent.getName().endsWith(".ear");
    }

    @Override
    public void process(Context context) throws Exception {
	// add this jar to scan path
	VirtualFile vf = context.getPath();
	log.info("Adding to scan path: "
		+ vf.getPathNameRelativeTo(vf.getParent().getParent()));
	context.addScanUrl(vf.getPhysicalFile().toURI().toURL());

    }

}
