package com.synapsense.ejbinfo.units;

import java.util.Collection;
import java.util.Collections;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import com.synapsense.ejbinfo.units.beans.BeanDescriptor;
import com.synapsense.ejbinfo.units.ear.EarUnitDescriptor;
import com.synapsense.ejbinfo.units.ejbjar.EjbJarUnitDescriptor;
import com.synapsense.ejbinfo.util.xml.UnitsObjectFactory;

@XmlType(factoryClass = UnitsObjectFactory.class, factoryMethod = "createUnitDescriptor")
@XmlAccessorType(XmlAccessType.PROPERTY)
@XmlSeeAlso({ EarUnitDescriptor.class, EjbJarUnitDescriptor.class })
public class UnitDescriptor {

    private final String name;
    private final ConcurrentHashMap<String, UnitDescriptor> subUnits;
    private final ConcurrentHashMap<String, BeanDescriptor> beans;
    private UnitDescriptor parent;

    public UnitDescriptor(String name) {
	this.name = Objects.requireNonNull(name, "name must not be null");
	parent = null;
	subUnits = new ConcurrentHashMap<>();
	beans = new ConcurrentHashMap<>();
    }

    @XmlAttribute(required = true)
    public String getName() {
	return name;
    }

    @XmlTransient
    public UnitDescriptor getParentUnit() {
	return parent;
    }

    public UnitDescriptor getSubUnit(String name) {
	return subUnits.get(name);
    }

    public void addSubUnit(UnitDescriptor unit) {
	unit.parent = this;
	subUnits.put(unit.name, unit);
    }

    @XmlElement(name = "unit")
    @XmlElementWrapper(name = "subunits")
    public Collection<UnitDescriptor> getSubUnits() {
	return Collections.unmodifiableCollection(subUnits.values());
    }

    public BeanDescriptor getBean(String name) {
	return beans.get(name);
    }

    public void addBean(BeanDescriptor bean) {
	bean.setUnit(this);
	beans.put(bean.getName(), bean);
    }

    @XmlElement(name = "ejb")
    @XmlElementWrapper(name = "ejbs")
    public Collection<BeanDescriptor> getBeans() {
	return Collections.unmodifiableCollection(beans.values());
    }
}
