package com.synapsense.ejbinfo.units.beans.mdb;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

import com.synapsense.ejbinfo.units.beans.BeanDescriptor;
import com.synapsense.ejbinfo.util.xml.UnitsObjectFactory;

@XmlType(factoryClass = UnitsObjectFactory.class, factoryMethod = "createMdbDescriptor")
@XmlAccessorType(XmlAccessType.PROPERTY)
public class MdbDescriptor extends BeanDescriptor {

    public MdbDescriptor(String name, Class<?> clazz) {
	super(name, clazz);
    }

}
