package com.synapsense.ejbinfo.units;

import java.util.ArrayList;
import java.util.Collection;

import org.jboss.vfs.VirtualFile;

import com.synapsense.ejbinfo.context.Context;
import com.synapsense.ejbinfo.context.ContextProcessor;
import com.synapsense.ejbinfo.units.ear.EarContextBuilder;
import com.synapsense.ejbinfo.units.ejbjar.EjbJarContextBuilder;
import com.synapsense.ejbinfo.units.jar.JarContextBuilder;

public class FolderContextBuilder implements ContextProcessor {

    private final Collection<ContextProcessor> builders;

    public FolderContextBuilder() {
	builders = new ArrayList<>(3);
	builders.add(new EarContextBuilder());
	builders.add(new EjbJarContextBuilder());
	builders.add(new JarContextBuilder());
	// builders.add(new WarContextBuilder());
    }

    @Override
    public boolean supports(Context context) {
	return true;
    }

    @Override
    public void process(Context context) throws Exception {
	// process current context
	for (ContextProcessor builder : builders) {
	    if (builder.supports(context)) {
		builder.process(context);
	    }
	}

	// discover useful subcontexts
	for (VirtualFile vf : context.getPath().getChildren()) {
	    Context subContext = new Context(vf);
	    // temporarily add subContext
	    context.addSubContext(subContext);
	    boolean keepSubContext = false;
	    for (ContextProcessor builder : builders) {
		if (builder.supports(subContext)) {
		    keepSubContext = true;
		    break;
		}
	    }
	    if (!keepSubContext) {
		context.removeSubContext(subContext.getName());
	    }
	}

	// process subcontexts
	for (Context subContext : context.getSubContexts()) {
	    process(subContext);
	}
    }
}
