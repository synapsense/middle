package com.synapsense.ejbinfo.units.resolvers;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.synapsense.ejbinfo.units.UnitDescriptor;
import com.synapsense.ejbinfo.units.Units;
import com.synapsense.ejbinfo.units.beans.BeanDescriptor;
import com.synapsense.ejbinfo.units.beans.Dependency;
import com.synapsense.ejbinfo.units.beans.session.Export;

public class LocalEjbDependencyResolver {

    private static final Logger log = LoggerFactory
	    .getLogger(LocalEjbDependencyResolver.class);

    public void process(Units units) {
	// we can only resolve dependencies locally per root unit
	for (UnitDescriptor unit : units.getUnits()) {
	    process(unit);
	}
    }

    private static void process(UnitDescriptor unit) {
	log.info("Resolving EJB dependencies in: " + unit.getName());
	Map<Class<?>, Map<String, BeanDescriptor>> exports = collectExports(unit);
	if (log.isDebugEnabled()) {
	    debugExports(exports);
	    // debugExportsAsDot(exports);
	}
	resolveIn(unit, exports);
    }

    private static void debugExports(
	    Map<Class<?>, Map<String, BeanDescriptor>> exports) {
	for (Map.Entry<Class<?>, Map<String, BeanDescriptor>> entry : exports
		.entrySet()) {
	    log.debug("Interface " + entry.getKey().getName()
		    + " is exported by:");
	    for (String beanName : entry.getValue().keySet()) {
		log.debug("\t" + beanName);
	    }
	}
    }

    @SuppressWarnings("unused")
    private static void debugExportsAsDot(
	    Map<Class<?>, Map<String, BeanDescriptor>> exports) {
	StringBuilder sb = new StringBuilder("digraph exports {\r\n");
	sb.append("\tnodesep=0.05;\r\n").append("\trankdir=TB;\r\n")
		.append("\tnode [shape=record];\r\n");

	/*
	 * sb.append("\tifaces [label=\""); int i = 0; for (Class<?> iface :
	 * exports.keySet()) { sb.append(String.format("<i%d> %s |", i++,
	 * iface.getName())); } sb.deleteCharAt(sb.length() -
	 * 1).append("\", height=2.5];\r\n");
	 */

	int i = 0;
	for (Map.Entry<Class<?>, Map<String, BeanDescriptor>> entry : exports
		.entrySet()) {
	    sb.append("\tsubgraph {\r\n");
	    for (Map.Entry<String, BeanDescriptor> beanEntry : entry.getValue()
		    .entrySet()) {
		sb.append(String
			.format("\t\t%1$s [label=\"{<n> %1$s | %2$s}\", group=\"%3$s\"];\r\n",
				beanEntry.getKey(), beanEntry.getValue()
					.getBeanClass().getName(), entry
					.getKey().getName()));
		sb.append(String.format("\t\t\"%s\" -> %s:n;\r\n", entry
			.getKey().getName(), beanEntry.getKey()));
	    }
	    sb.append("\t}\r\n");
	    i++;
	}
	sb.append("}");
	log.debug(sb.toString());
    }

    private static void resolveIn(UnitDescriptor unit,
	    Map<Class<?>, Map<String, BeanDescriptor>> exports) {
	for (BeanDescriptor bean : unit.getBeans()) {
	    for (Dependency dep : bean.getDependencies()) {
		// try to find dependency directly by the interface first
		Map<String, BeanDescriptor> ifaceExport = exports
			.get(dep.iface);
		Collection<Map<String, BeanDescriptor>> ifaceExports = ifaceExport != null ? Arrays
			.asList(ifaceExport) : Collections
			.<Map<String, BeanDescriptor>> emptyList();
		if (ifaceExports.isEmpty()) {
		    // try to find by subclassing
		    ifaceExports = new LinkedList<>();
		    for (Map.Entry<Class<?>, Map<String, BeanDescriptor>> entry : exports
			    .entrySet()) {
			if (dep.iface.isAssignableFrom(entry.getKey()))
			    ifaceExports.add(entry.getValue());
		    }
		}

		// check resolution results
		checkAndSetResolution(dep, ifaceExports);

	    }
	}

	// process subunits recursively
	for (UnitDescriptor subUnit : unit.getSubUnits()) {
	    resolveIn(subUnit, exports);
	}
    }

    private static Map<Class<?>, Map<String, BeanDescriptor>> collectExports(
	    UnitDescriptor unit) {

	Map<Class<?>, Map<String, BeanDescriptor>> exports = new HashMap<>();

	for (BeanDescriptor bean : unit.getBeans()) {
	    for (Export export : bean.getExports()) {
		Map<String, BeanDescriptor> ifaceExports = exports
			.get(export.iface);
		if (ifaceExports == null) {
		    // got no exports with this interface yet
		    ifaceExports = new HashMap<>();
		    exports.put(export.iface, ifaceExports);
		}
		assert (export.bean.equals(bean));
		ifaceExports.put(export.bean.getName(), export.bean);
	    }
	}
	// process subunits recursively
	for (UnitDescriptor subUnit : unit.getSubUnits()) {
	    mergeExports(collectExports(subUnit), exports);
	}
	return exports;
    }

    private static void mergeExports(
	    Map<Class<?>, Map<String, BeanDescriptor>> from,
	    Map<Class<?>, Map<String, BeanDescriptor>> to) {
	for (Map.Entry<Class<?>, Map<String, BeanDescriptor>> entry : from
		.entrySet()) {
	    Map<String, BeanDescriptor> ifaceExportsTo = to.get(entry.getKey());
	    if (ifaceExportsTo != null) {
		ifaceExportsTo.putAll(entry.getValue());
	    } else {
		to.put(entry.getKey(), entry.getValue());
	    }
	}
    }

    private static void checkAndSetResolution(Dependency dep,
	    Collection<Map<String, BeanDescriptor>> candidates) {
	if (candidates.isEmpty()) {
	    log.warn(String.format("Unresolved EJB dependency: %s->\"%s!%s\"",
		    dep.bean.getName(), dep.name, dep.iface.getName()));
	    return;
	}

	BeanDescriptor resolvedTo = null;

	if (dep.name == null) {
	    if (candidates.size() > 1
		    || candidates.iterator().next().size() > 1) {
		// when dependency name was not specified and there's more than
		// one candidate we need to find a single one in the local
		// module
		Collection<BeanDescriptor> localBeans = new LinkedList<>();
		for (Map<String, BeanDescriptor> namedCandidates : candidates) {
		    for (BeanDescriptor candidate : namedCandidates.values()) {
			if (dep.bean.getUnit().getName()
				.equals(candidate.getUnit().getName()))
			    localBeans.add(candidate);
		    }
		}
		if (localBeans.size() > 1) {
		    // still more than one? there's nothing else we can do
		    // (besides, it is quite an odd case from EJB standard point
		    // of view)
		    log.warn(String
			    .format("Unresolved EJB dependency (more than one local candidate): %s->\"%s!%s\"",
				    dep.bean.getName(), dep.name,
				    dep.iface.getName()));
		    return;
		}

		if (localBeans.size() == 0) {
		    // there are some non-local candidates but without a name we
		    // cannot pick any
		    log.warn(String
			    .format("Unresolved EJB dependency (more than one candidate): %s->\"%s!%s\"",
				    dep.bean.getName(), dep.name,
				    dep.iface.getName()));
		    return;
		}

		resolvedTo = localBeans.iterator().next();
	    } else {
		resolvedTo = candidates.iterator().next().entrySet().iterator()
			.next().getValue();
	    }
	} else {
	    Collection<BeanDescriptor> beans = new LinkedList<>();
	    for (Map<String, BeanDescriptor> namedCandidates : candidates) {
		for (Map.Entry<String, BeanDescriptor> entry : namedCandidates
			.entrySet()) {
		    if (dep.name.equals(entry.getKey()))
			beans.add(entry.getValue());
		}
	    }

	    if (beans.isEmpty()) {
		log.warn(String
			.format("Unresolved EJB dependency (no candidates with specified name): %s->\"%s!%s\"",
				dep.bean.getName(), dep.name,
				dep.iface.getName()));
		return;
	    }

	    if (beans.size() > 1) {
		log.warn(String
			.format("Unresolved EJB dependency (more than one candidate with specified name): %s->\"%s!%s\"",
				dep.bean.getName(), dep.name,
				dep.iface.getName()));
		return;
	    }

	    resolvedTo = beans.iterator().next();
	}

	dep.resolvedTo = resolvedTo;
	log.info(String.format("Resolved EJB dependency: %s->\"%s!%s\" to %s",
		dep.bean.getName(), dep.name, dep.iface.getName(),
		resolvedTo.getName()));
    }
}
