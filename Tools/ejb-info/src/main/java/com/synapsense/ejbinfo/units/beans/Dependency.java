package com.synapsense.ejbinfo.units.beans;

import java.util.Objects;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import com.synapsense.ejbinfo.util.xml.UnitsObjectFactory;

@XmlType(factoryClass = UnitsObjectFactory.class, factoryMethod = "createDependency")
@XmlAccessorType(XmlAccessType.FIELD)
public class Dependency {

    @XmlAttribute(name = "interface", required = true)
    public final Class<?> iface;

    @XmlAttribute
    public final String name;

    @XmlAttribute
    public final String byMember;

    @XmlIDREF
    @XmlAttribute(required = false)
    public BeanDescriptor resolvedTo;

    @XmlTransient
    public BeanDescriptor bean;

    public Dependency(Class<?> type, String byMember, String name) {
	this.byMember = Objects.requireNonNull(byMember,
		"byMember must not be null");
	this.iface = Objects.requireNonNull(type, "type must not be null");
	this.name = name;
    }

}
