package com.synapsense.ejbinfo.units.beans.session;

import java.lang.annotation.Annotation;

import javax.ejb.Local;
import javax.ejb.Remote;

import org.reflections.Reflections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.synapsense.ejbinfo.units.beans.BeanDescriptor;
import com.synapsense.ejbinfo.units.beans.DependencyProcessor;
import com.synapsense.ejbinfo.util.reflection.ReflectUtils;

public class ExportProcessor extends DependencyProcessor {

    private final static Logger log = LoggerFactory
	    .getLogger(ExportProcessor.class);

    @Override
    public BeanDescriptor process(Class<?> bean, BeanDescriptor descr,
	    Reflections reflections) {
	processExports(bean, descr, Local.class, Export.Type.LOCAL);
	processExports(bean, descr, Remote.class, Export.Type.REMOTE);
	exportDefaultLocals(bean, descr);
	exportNoInterfaceView(bean, descr);
	return super.process(bean, descr, reflections);
    }

    private static void processExports(Class<?> bean, BeanDescriptor descr,
	    Class<? extends Annotation> annotation, Export.Type type) {
	// read annotation markers from the bean itself
	if (bean.isAnnotationPresent(annotation)) {
	    Class<?>[] locals = (Class<?>[]) ReflectUtils.getAnnotationValue(
		    bean, annotation, "value");
	    for (Class<?> iface : locals) {
		Export export = new Export(iface, type);
		descr.addExport(export);
		log.info(String.format("Found %s interface: %s!%s",
			annotation.getSimpleName(), descr.getName(),
			iface.getName()));
	    }
	}

	// read annotation marker from bean interfaces
	Class<?>[] ifaces = bean.getInterfaces();
	for (Class<?> iface : ifaces) {
	    if (iface.isAnnotationPresent(annotation)) {
		Export export = new Export(iface, type);
		descr.addExport(export);
		log.info(String.format("Found %s interface: %s!%s",
			annotation.getSimpleName(), descr.getName(),
			iface.getName()));
	    }
	}
    }

    private static void exportDefaultLocals(Class<?> bean, BeanDescriptor descr) {
	// check if bean has explicit business interfaces
	if (!descr.getExports().isEmpty())
	    return;

	// if any interfaces are implemented export them as local
	// TODO this could be wrong for non-singletons
	Class<?>[] ifaces = bean.getInterfaces();
	for (Class<?> iface : ifaces) {
	    Export export = new Export(iface, Export.Type.LOCAL);
	    descr.addExport(export);
	    log.info(String.format("Found Local interface: %s!%s",
		    descr.getName(), iface.getName()));
	}
    }

    private static void exportNoInterfaceView(Class<?> bean,
	    BeanDescriptor descr) {
	// no-interface views are constructed only when business interfaces are
	// absent
	if (!descr.getExports().isEmpty())
	    return;

	Export export = new Export(bean, Export.Type.LOCAL);
	descr.addExport(export);
	log.info(String.format("Found no-interface view: %s!%s",
		descr.getName(), bean.getName()));
    }
}
