package com.synapsense.ejbinfo;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Arrays;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.GnuParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.PatternOptionBuilder;
import org.jboss.vfs.TempFileProvider;
import org.jboss.vfs.VFS;
import org.reflections.vfs.Vfs;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.synapsense.ejbinfo.context.Context;
import com.synapsense.ejbinfo.units.FolderContextBuilder;
import com.synapsense.ejbinfo.units.UnitCollector;
import com.synapsense.ejbinfo.units.Units;
import com.synapsense.ejbinfo.units.beans.BeanScanner;
import com.synapsense.ejbinfo.units.resolvers.LocalEjbDependencyResolver;
import com.synapsense.ejbinfo.units.resolvers.LocalSsbStartupDependencyResolver;
import com.synapsense.ejbinfo.util.vfs.VFSUtils;
import com.synapsense.ejbinfo.util.vfs.VfsUrlTypes;

public class Console {

    private static final Logger log;

    static {
	if (System.getProperty("logback.configurationFile") == null) {
	    System.setProperty("logback.configurationFile", "conf\\logback.xml");
	}
	log = LoggerFactory.getLogger(Console.class);
    }

    /**
     * @param args
     */
    public static void main(String[] args) throws Exception {
	CliOptions opts = parseAndCheckCliOptions(args);
	if (opts == null)
	    return;

	// set temporary folder
	String tmpFolder = "tmp";
	System.setProperty("jboss.server.temp.dir", tmpFolder);
	log.info(String.format("Tmp folder set to: %s",
		new File(tmpFolder).getAbsolutePath()));

	Vfs.setDefaultURLTypes(Arrays.<Vfs.UrlType> asList(VfsUrlTypes.zipJar,
		VfsUrlTypes.folderJar));

	log.info(String.format("Processing EJB info in: %s",
		opts.inFile.getAbsolutePath()));
	// set root context to the search folder
	try (Context root = new Context(VFS.getChild(opts.inFile.toURI()))) {
	    // create tmp provider for VFS
	    TempFileProvider provider = TempFileProvider
		    .create("ejb-info",
			    Executors
				    .newSingleThreadScheduledExecutor(new ThreadFactory() {
					@Override
					public Thread newThread(Runnable r) {
					    Thread t = new Thread(r,
						    "tmp cleaner");
					    t.setDaemon(true);
					    return t;
					}
				    }));

	    VFSUtils.setTempFileProvider(provider);

	    // initialize context
	    FolderContextBuilder fcb = new FolderContextBuilder();
	    fcb.process(root);

	    // scan context for beans
	    BeanScanner bs = new BeanScanner();
	    bs.process(root);

	    // collect units from the context
	    UnitCollector collector = new UnitCollector();
	    collector.process(root);
	    Units units = collector.getUnits();

	    // resolve dependencies
	    LocalSsbStartupDependencyResolver ssbResolver = new LocalSsbStartupDependencyResolver();
	    ssbResolver.process(units);

	    LocalEjbDependencyResolver ejbResolver = new LocalEjbDependencyResolver();
	    ejbResolver.process(units);

	    units.saveTo(opts.outFile);
	    File resFile = new File(opts.outFile, "ejb-info.xml");
	    log.info(String.format("Raw input written to: %s", resFile));

	    // let's see if we need to apply xslt
	    if (opts.xsltFile != null) {
		log.info(String.format("Applying XSLT: \"%s\" to \"%s\"",
			opts.xsltFile, resFile));
		try (InputStream ts = new BufferedInputStream(
			Files.newInputStream(opts.xsltFile.toPath()));
			InputStream is = new BufferedInputStream(
				Files.newInputStream(resFile.toPath()));
			OutputStream os = new BufferedOutputStream(
				Files.newOutputStream(opts.xsltResFile.toPath()))) {

		    Transformer xmlTransformer = TransformerFactory
			    .newInstance().newTransformer(new StreamSource(ts));
		    xmlTransformer.setOutputProperty(
			    "{http://xml.apache.org/xslt}indent-amount", "4");
		    xmlTransformer.setOutputProperty(OutputKeys.INDENT, "yes");
		    xmlTransformer.transform(new StreamSource(is),
			    new StreamResult(os));

		}
	    }
	} finally {
	    Path tmpPath = null;
	    try {
		VFSUtils.close();
		tmpPath = Paths.get(tmpFolder);
		log.info("Deleting temporary folder: "
			+ tmpPath.toAbsolutePath());
		deletePath(tmpPath);
		log.info("Deleted temporary folder: "
			+ tmpPath.toAbsolutePath());
	    } catch (Exception e) {
		log.warn("Cannot delete some temporary files; when you got nothing else to do please delete folder "
			+ tmpPath.toFile().getAbsolutePath());
	    }
	}

    }

    private static void deletePath(Path path) throws IOException {
	Files.walkFileTree(path, new SimpleFileVisitor<Path>() {

	    @Override
	    public FileVisitResult visitFile(Path file,
		    BasicFileAttributes attrs) throws IOException {

		log.debug("Deleting file: " + file);
		Files.delete(file);
		return FileVisitResult.CONTINUE;
	    }

	    @Override
	    public FileVisitResult postVisitDirectory(Path dir, IOException exc)
		    throws IOException {

		log.debug("Deleting dir: " + dir);
		if (exc == null) {
		    Files.delete(dir);
		    return FileVisitResult.CONTINUE;
		} else {
		    throw exc;
		}
	    }

	});
    }

    private static CliOptions parseAndCheckCliOptions(String[] args) {
	CliOptions opts = new CliOptions();
	GnuParser gp = new GnuParser();

	try {
	    CommandLine cl = gp.parse(getCliOptions(), args);

	    if (cl.hasOption("help")) {
		printUsage();
		return null;
	    }

	    String[] input = cl.getArgs();
	    if (input.length == 0) {
		printUsage();
		return null;
	    }

	    // get input path
	    if (input.length > 1) {
		log.info("Unrecoginzed option: " + input[0]);
		return null;
	    }
	    opts.inFile = new File(input[0]);
	    if (!Files.exists(Paths.get(opts.inFile.toURI()))) {
		log.info(String.format("Specified path does not exist: %s",
			input[0]));
		return null;
	    }

	    // get output folder
	    opts.outFile = cl.hasOption("out") ? (File) cl
		    .getParsedOptionValue("out") : new File("./out");
	    Path outPath = Paths.get(opts.outFile.toURI());
	    if (Files.exists(outPath) && !Files.isDirectory(outPath)) {
		log.info(String.format("Output path is not a directory: %s",
			opts.outFile.toString()));
		return null;
	    }

	    if (!Files.exists(outPath)) {
		Files.createDirectories(outPath);
	    }

	    // get xslt transformation
	    if (cl.hasOption("t")) {
		opts.xsltFile = (File) cl.getParsedOptionValue("t");
		Path xsltPath = Paths.get(opts.xsltFile.toURI());
		if (!Files.exists(xsltPath) || Files.isDirectory(xsltPath)) {
		    log.info(String.format(
			    "XSLT path does not exist or is not a file: %s",
			    opts.xsltFile.toString()));
		    return null;
		}

		opts.xsltResFile = new File(opts.outFile, cl.getOptionValue(
			"t_out", "ejb-info.res.xml"));
	    }

	} catch (Exception e) {
	    log.info("Cannot parse command line", e);
	    return null;
	}
	return opts;
    }

    @SuppressWarnings("static-access")
    private static Options getCliOptions() {
	Options options = new Options();
	options.addOption(OptionBuilder.withArgName("path").hasArg()
		.withType(PatternOptionBuilder.FILE_VALUE)
		.withDescription("output folder; default is \"./out\"")
		.create("out"));
	options.addOption(OptionBuilder
		.withType(PatternOptionBuilder.FILE_VALUE)
		.withArgName("path")
		.hasArg()
		.withDescription(
			"xslt transformation file to be applied to the resulting xml; default is none")
		.create("t"));
	options.addOption(OptionBuilder
		.withType(PatternOptionBuilder.STRING_VALUE)
		.withArgName("name")
		.hasArg()
		.withDescription(
			"name of the result file for xslt transformation; default is ejb-info.res.xml")
		.create("t_out"));
	options.addOption(OptionBuilder.withDescription("this help message")
		.create("help"));

	return options;
    }

    private static void printUsage() {
	HelpFormatter hf = new HelpFormatter();
	hf.printHelp("ejb-info [options] inputPath", getCliOptions());
    }
}

class CliOptions {
    File inFile;
    File outFile;
    File xsltFile;
    File xsltResFile;
}