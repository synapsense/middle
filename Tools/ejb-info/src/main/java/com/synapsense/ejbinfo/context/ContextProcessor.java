package com.synapsense.ejbinfo.context;

public interface ContextProcessor {

    boolean supports(Context context);

    void process(Context context) throws Exception;

}
