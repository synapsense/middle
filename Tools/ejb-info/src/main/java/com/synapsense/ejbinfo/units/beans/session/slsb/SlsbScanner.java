package com.synapsense.ejbinfo.units.beans.session.slsb;

import java.util.Set;

import javax.ejb.Stateless;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.synapsense.ejbinfo.context.Context;
import com.synapsense.ejbinfo.context.ContextProcessor;
import com.synapsense.ejbinfo.units.UnitDescriptor;
import com.synapsense.ejbinfo.units.beans.BeanDescriptor;

public class SlsbScanner implements ContextProcessor {

    private static final Logger log = LoggerFactory
	    .getLogger(SlsbScanner.class);

    private static final SlsbProcessor beanProcessor = new SlsbProcessor();

    @Override
    public boolean supports(Context context) {
	Context parent = context.getParent();
	return parent == null ? false : parent.getName().endsWith(".ear")
		&& context.getName().endsWith(".jar");
    }

    @Override
    public void process(Context context) throws Exception {
	log.info("Scanning for SLSB: " + context.getFullName());
	UnitDescriptor uDescr = context.getDescriptor();
	Set<Class<?>> slsbs = context.getReflections().getTypesAnnotatedWith(
		Stateless.class);
	for (Class<?> slsb : slsbs) {
	    BeanDescriptor descr = beanProcessor.process(slsb, null,
		    context.getReflections());
	    uDescr.addBean(descr);
	}
    }

}
