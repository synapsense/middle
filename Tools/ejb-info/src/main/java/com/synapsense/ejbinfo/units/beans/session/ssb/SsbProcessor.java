package com.synapsense.ejbinfo.units.beans.session.ssb;

import java.util.Arrays;

import javax.ejb.DependsOn;
import javax.ejb.Singleton;
import javax.ejb.Startup;

import org.reflections.Reflections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.synapsense.ejbinfo.units.beans.BeanDescriptor;
import com.synapsense.ejbinfo.units.beans.session.ExportProcessor;
import com.synapsense.ejbinfo.util.reflection.ReflectUtils;

public class SsbProcessor extends ExportProcessor {

    private static final Logger log = LoggerFactory
	    .getLogger(SsbProcessor.class);

    @Override
    public BeanDescriptor process(Class<?> bean, BeanDescriptor descr,
	    Reflections reflections) {
	// create descriptor first
	String name = (String) ReflectUtils.getAnnotationValue(bean,
		Singleton.class, "name");
	if (name.isEmpty())
	    name = bean.getSimpleName();

	boolean startup = bean.isAnnotationPresent(Startup.class);
	SsbDescriptor newDescr = new SsbDescriptor(name, bean, startup);
	log.info("Found SSB: startup=" + startup + ", " + name + "("
		+ bean.getName() + ")");

	if (bean.isAnnotationPresent(DependsOn.class)) {
	    String[] deps = (String[]) ReflectUtils.getAnnotationValue(bean,
		    DependsOn.class, "value");
	    log.info("Startup dependencies: " + Arrays.toString(deps));
	    for (String dep : deps) {
		newDescr.addStartupDependency(new StartupDependency(dep));
	    }
	}

	return super.process(bean, newDescr, reflections);
    }
}
