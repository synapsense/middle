package com.synapsense.ejbinfo.units.beans.session.slsb;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

import com.synapsense.ejbinfo.units.beans.BeanDescriptor;
import com.synapsense.ejbinfo.util.xml.UnitsObjectFactory;

@XmlType(factoryClass = UnitsObjectFactory.class, factoryMethod = "createSlsbDescriptor")
@XmlAccessorType(XmlAccessType.PROPERTY)
public class SlsbDescriptor extends BeanDescriptor {

    public SlsbDescriptor(String name, Class<?> clazz) {
	super(name, clazz);
    }

}
