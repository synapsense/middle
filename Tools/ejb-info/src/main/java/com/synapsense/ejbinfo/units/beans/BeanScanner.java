package com.synapsense.ejbinfo.units.beans;

import java.util.ArrayList;
import java.util.Collection;

import com.synapsense.ejbinfo.context.Context;
import com.synapsense.ejbinfo.context.ContextProcessor;
import com.synapsense.ejbinfo.units.beans.mdb.MdbScanner;
import com.synapsense.ejbinfo.units.beans.session.sfsb.SfsbScanner;
import com.synapsense.ejbinfo.units.beans.session.slsb.SlsbScanner;
import com.synapsense.ejbinfo.units.beans.session.ssb.SsbScanner;

public class BeanScanner implements ContextProcessor {

    private final Collection<ContextProcessor> scanners;

    public BeanScanner() {
	scanners = new ArrayList<>(4);
	scanners.add(new SsbScanner());
	scanners.add(new SfsbScanner());
	scanners.add(new SlsbScanner());
	scanners.add(new MdbScanner());
    }

    @Override
    public boolean supports(Context context) {
	return true;
    }

    @Override
    public void process(Context context) throws Exception {
	for (ContextProcessor scanner : scanners) {
	    if (scanner.supports(context)) {
		scanner.process(context);
	    }
	}

	// proceed children recursively
	for (Context subContext : context.getSubContexts()) {
	    process(subContext);
	}

    }

}
