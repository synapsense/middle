package com.synapsense.ejbinfo.util.reflection;

import java.lang.annotation.Annotation;
import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.InvocationTargetException;

public class ReflectUtils {

    public static Object getAnnotationValue(AnnotatedElement elem,
	    Class<? extends Annotation> annotationType, String attributeName) {

	Object value = null;

	Annotation annotation = elem.getAnnotation(annotationType);
	if (annotation != null) {
	    try {
		value = annotation.annotationType().getMethod(attributeName)
			.invoke(annotation);
	    } catch (IllegalAccessException | InvocationTargetException
		    | NoSuchMethodException | SecurityException e) {
		// TODO Auto-generated catch block
		throw new IllegalArgumentException(String.format(
			"Cannot read %s.%s()", annotationType.getName(),
			attributeName));
	    }
	} else {
	    throw new IllegalArgumentException("Annotation is absent "
		    + annotationType.getName());
	}

	return value;
    }
}
