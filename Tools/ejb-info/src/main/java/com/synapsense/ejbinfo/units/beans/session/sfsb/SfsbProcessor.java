package com.synapsense.ejbinfo.units.beans.session.sfsb;

import javax.ejb.Stateful;

import org.reflections.Reflections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.synapsense.ejbinfo.units.beans.BeanDescriptor;
import com.synapsense.ejbinfo.units.beans.session.ExportProcessor;
import com.synapsense.ejbinfo.util.reflection.ReflectUtils;

public class SfsbProcessor extends ExportProcessor {

    private static final Logger log = LoggerFactory
	    .getLogger(SfsbProcessor.class);

    @Override
    public BeanDescriptor process(Class<?> bean, BeanDescriptor descr,
	    Reflections reflections) {
	// create descriptor first
	String name = (String) ReflectUtils.getAnnotationValue(bean,
		Stateful.class, "name");
	if (name.isEmpty())
	    name = bean.getSimpleName();
	log.info("Found SFSB: " + name + "(" + bean.getName() + ")");

	SfsbDescriptor newDescr = new SfsbDescriptor(name, bean);

	return super.process(bean, newDescr, reflections);
    }

}
