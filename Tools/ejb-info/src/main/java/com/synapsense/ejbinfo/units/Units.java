package com.synapsense.ejbinfo.units;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.util.Collection;
import java.util.Collections;
import java.util.Objects;
import java.util.concurrent.ConcurrentLinkedQueue;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.SchemaOutputResolver;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.transform.Result;
import javax.xml.transform.stream.StreamResult;

@XmlRootElement
@XmlType(name = "")
@XmlAccessorType(XmlAccessType.PROPERTY)
public class Units {

    private final Collection<UnitDescriptor> rootUnits = new ConcurrentLinkedQueue<>();

    @XmlElement(name = "unit")
    public Collection<UnitDescriptor> getUnits() {
	return Collections.unmodifiableCollection(rootUnits);
    }

    public void add(UnitDescriptor descr) {
	rootUnits.add(descr);
    }

    public void saveTo(File path) throws Exception {
	JAXBContext context = JAXBContext.newInstance(Units.class);
	context.generateSchema(new UnitsSchemaOutputResolver(path));
	Marshaller m = context.createMarshaller();
	m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
	m.setProperty(Marshaller.JAXB_SCHEMA_LOCATION,
		"http://synapsense.com/ejb-info ejb-info.xsd");
	try (OutputStream os = new BufferedOutputStream(
		Files.newOutputStream(new File(path, "ejb-info.xml").toPath()))) {
	    m.marshal(this, os);
	}
    }

    private static class UnitsSchemaOutputResolver extends SchemaOutputResolver {

	private final File path;

	public UnitsSchemaOutputResolver(File path) {
	    this.path = Objects.requireNonNull(path, "path must not be null");
	}

	@Override
	public Result createOutput(String namespaceUri, String suggestedFileName)
		throws IOException {
	    return new StreamResult(new File(path, "ejb-info.xsd"));
	}
    }
}
