package com.synapsense.ejbinfo.context;

import java.io.Closeable;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.jboss.vfs.VirtualFile;
import org.reflections.Reflections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.synapsense.ejbinfo.units.UnitDescriptor;

public class Context implements Closeable {

    private static final Logger log = LoggerFactory.getLogger(Context.class);

    private final String name;
    private final Map<String, Context> subContexts;
    private final VirtualFile path;
    private final Collection<URL> classPath;
    private Context parent;

    private UnitDescriptor descriptor;

    private Reflections reflections;
    private URLClassLoader classLoader;

    public Context(VirtualFile path) {
	this.path = path;
	parent = null;
	name = path.getName();
	subContexts = new ConcurrentHashMap<>();
	classPath = new ConcurrentLinkedQueue<>();
    }

    public String getName() {
	return name;
    }

    public String getFullName() {
	if (parent == null)
	    return name;
	return parent.getFullName() + "/" + name;
    }

    public Context getParent() {
	return parent;
    }

    public VirtualFile getPath() {
	return path;
    }

    public UnitDescriptor getDescriptor() {
	return descriptor;
    }

    public void setDescriptor(UnitDescriptor descriptor) {
	this.descriptor = descriptor;
    }

    public Context getSubContext(String name) {
	return subContexts.get(name);
    }

    public Collection<Context> getSubContexts() {
	return Collections.unmodifiableCollection(subContexts.values());
    }

    public void addSubContext(Context context) {
	context.parent = this;
	subContexts.put(context.getName(), context);
    }

    public Context removeSubContext(String name) {
	Context subContext = subContexts.remove(name);
	if (subContext != null)
	    subContext.parent = null;
	return subContext;
    }

    public void addScanUrl(URL url) {
	classPath.add(url);
    }

    public Collection<URL> getScanUrls() {
	return Collections.unmodifiableCollection(classPath);
    }

    public Collection<URL> getAllScanUrls() {
	Collection<URL> result = new LinkedList<>(classPath);
	for (Context subContext : getSubContexts()) {
	    result.addAll(subContext.getAllScanUrls());
	}
	return Collections.unmodifiableCollection(result);
    }

    private URLClassLoader getRootClassLoader() throws Exception {
	// all classes must be loaded by the same class loader for proper
	// operation.
	// one option would be creating a hierarchy of URLClassloaders;
	// instead we simply create one class loader at the topmost parent
	// context and then cache it at every level
	if (parent == null) {
	    synchronized (this) {
		if (classLoader == null) {
		    // create URLClassLoader with all jars
		    Collection<URL> jars = getAllScanUrls();
		    if (log.isDebugEnabled())
			log.debug("Creating URLClassLoader for: " + jars);
		    URL[] urls = new URL[jars.size()];
		    classLoader = new URLClassLoader(jars.toArray(urls),
			    getClass().getClassLoader());
		}
	    }
	}

	if (classLoader == null) {
	    classLoader = parent.getRootClassLoader();
	}
	return classLoader;
    }

    @Override
    public void close() {
	for (Context subContext : getSubContexts())
	    subContext.close();
	if (classLoader != null) {
	    try {
		classLoader.close();
	    } catch (IOException e) {
		log.warn("Cannot close context class loader", e);
	    }
	}
    }

    public synchronized Reflections getReflections() throws Exception {
	if (reflections == null) {

	    // get root context
	    Context root = this;
	    while (root.getParent() != null)
		root = root.getParent();

	    reflections = new Reflections(getRootClassLoader(), getPath()
		    .getPhysicalFile().toURI().toURL());
	}
	return reflections;
    }
}
