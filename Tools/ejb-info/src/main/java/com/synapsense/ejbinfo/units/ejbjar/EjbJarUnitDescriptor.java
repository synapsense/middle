package com.synapsense.ejbinfo.units.ejbjar;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

import com.sun.java.xml.ns.javaee.EjbJarType;
import com.synapsense.ejbinfo.units.UnitDescriptor;
import com.synapsense.ejbinfo.util.xml.UnitsObjectFactory;

@XmlType(factoryClass = UnitsObjectFactory.class, factoryMethod = "createEjbJarUnitDescriptor")
@XmlAccessorType(XmlAccessType.NONE)
public class EjbJarUnitDescriptor extends UnitDescriptor {

    private final EjbJarType ejbJarXml;

    public EjbJarUnitDescriptor(String name, EjbJarType ejbJarXml) {
	super(name);
	this.ejbJarXml = ejbJarXml;
    }

    public EjbJarType getEjbJarXml() {
	return ejbJarXml;
    }

}
