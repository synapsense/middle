package com.synapsense.ejbinfo.units.beans.session.sfsb;

import java.util.Set;

import javax.ejb.Stateful;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.synapsense.ejbinfo.context.Context;
import com.synapsense.ejbinfo.context.ContextProcessor;
import com.synapsense.ejbinfo.units.UnitDescriptor;
import com.synapsense.ejbinfo.units.beans.BeanDescriptor;

public class SfsbScanner implements ContextProcessor {

    private static final Logger log = LoggerFactory
	    .getLogger(SfsbScanner.class);

    private static final SfsbProcessor beanProcessor = new SfsbProcessor();

    @Override
    public boolean supports(Context context) {
	Context parent = context.getParent();
	return parent == null ? false : parent.getName().endsWith(".ear")
		&& context.getName().endsWith(".jar");
    }

    @Override
    public void process(Context context) throws Exception {
	log.info("Scanning for SFSB: " + context.getFullName());
	UnitDescriptor uDescr = context.getDescriptor();
	Set<Class<?>> sfsbs = context.getReflections().getTypesAnnotatedWith(
		Stateful.class);
	for (Class<?> sfsb : sfsbs) {
	    BeanDescriptor descr = beanProcessor.process(sfsb, null,
		    context.getReflections());
	    uDescr.addBean(descr);
	}
    }

}
