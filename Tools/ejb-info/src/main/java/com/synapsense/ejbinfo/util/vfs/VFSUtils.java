package com.synapsense.ejbinfo.util.vfs;

import java.io.Closeable;
import java.io.IOException;
import java.util.Collection;
import java.util.LinkedList;

import org.jboss.vfs.TempFileProvider;
import org.jboss.vfs.VFS;
import org.jboss.vfs.VirtualFile;

public class VFSUtils {

    private static final Collection<Closeable> handlers = new LinkedList<>();
    private static TempFileProvider tfp;

    public static void setTempFileProvider(TempFileProvider tfp) {
	VFSUtils.tfp = tfp;
    }

    public static void expand(VirtualFile vf) throws IOException {
	if (vf.isFile()) {
	    handlers.add(VFS.mountZipExpanded(vf, vf, tfp));
	}
    }

    public static void close() {
	org.jboss.vfs.VFSUtils.safeClose(handlers);
    }
}
