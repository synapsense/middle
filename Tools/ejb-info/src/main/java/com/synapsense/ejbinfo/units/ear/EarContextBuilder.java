package com.synapsense.ejbinfo.units.ear;

import java.io.IOException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Unmarshaller;

import org.jboss.vfs.VirtualFile;
import org.jboss.vfs.util.FilterVirtualFileVisitor;
import org.jboss.vfs.util.SuffixMatchFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sun.java.xml.ns.javaee.ApplicationType;
import com.sun.java.xml.ns.javaee.ObjectFactory;
import com.synapsense.ejbinfo.context.Context;
import com.synapsense.ejbinfo.context.ContextProcessor;
import com.synapsense.ejbinfo.util.vfs.VFSUtils;
import com.synapsense.ejbinfo.util.xml.SchemaUtils;

public class EarContextBuilder implements ContextProcessor {

    private static final Logger log = LoggerFactory
	    .getLogger(EarContextBuilder.class);

    @Override
    public boolean supports(Context context) {
	return context.getName().endsWith(".ear");
    }

    @Override
    public void process(Context context) throws Exception {
	log.info("Building context for " + context.getFullName());

	VFSUtils.expand(context.getPath());

	ApplicationType app = parseApplicationDescr(context.getPath());
	EarUnitDescriptor earDescriptor = getDescriptor(context, app);
	context.setDescriptor(earDescriptor);

	VirtualFile libFolder = getLibFolder(app, context.getPath());

	// add ejb jars (located in ear's root)
	// addJarsToScan(context, context.getPath());
	// add library jars
	addJarsToScan(context, libFolder);

	log.info("Built context for " + context.getFullName());
    }

    private static EarUnitDescriptor getDescriptor(Context context,
	    ApplicationType app) {
	String name;
	// TODO switch to application-name
	if (app == null || app.getDisplayName().isEmpty()) {
	    name = context.getName();
	    log.info("Application name defaulted to: " + name);

	} else {
	    name = app.getDisplayName().get(0).getValue();
	    log.info("Application name taken from application.xml/display-name: "
		    + name);
	}
	return new EarUnitDescriptor(name, app);
    }

    private static void addJarsToScan(Context context, VirtualFile folder)
	    throws IOException {
	FilterVirtualFileVisitor visitor = new FilterVirtualFileVisitor(
		new SuffixMatchFilter(".jar"),
		org.jboss.vfs.VisitorAttributes.LEAVES_ONLY);

	folder.visit(visitor);

	for (VirtualFile vf : visitor.getMatched()) {
	    log.info("Adding to scan path: "
		    + vf.getPathNameRelativeTo(context.getPath().getParent()));
	    context.addScanUrl(vf.getPhysicalFile().toURI().toURL());
	}
    }

    private static VirtualFile getLibFolder(ApplicationType app,
	    VirtualFile earFile) {
	if (app != null) {
	    VirtualFile libFolder = earFile.getChild(app.getLibraryDirectory()
		    .getValue());
	    log.info("Lib folder set to: "
		    + libFolder.getPathNameRelativeTo(earFile.getParent()));
	    return libFolder;
	} else {
	    VirtualFile libFolder = earFile.getChild("lib");
	    log.info("Lib folder defaulted to: "
		    + libFolder.getPathNameRelativeTo(earFile));
	    return libFolder;
	}
    }

    private static ApplicationType parseApplicationDescr(VirtualFile earFile)
	    throws Exception {
	// locate application.xml
	VirtualFile appFile = earFile.getChild("META-INF/application.xml");
	if (appFile.exists()) {
	    log.info("Parsing "
		    + appFile.getPathNameRelativeTo(earFile.getParent()));
	    JAXBContext ctx = JAXBContext.newInstance(ObjectFactory.class);
	    Unmarshaller um = ctx.createUnmarshaller();
	    um.setSchema(SchemaUtils.app6Schema);

	    @SuppressWarnings("unchecked")
	    JAXBElement<ApplicationType> appElement = (JAXBElement<ApplicationType>) um
		    .unmarshal(appFile.getPhysicalFile());

	    return appElement.getValue();
	} else {
	    log.info(appFile.getPathNameRelativeTo(earFile.getParent())
		    + " not found");
	    return null;
	}
    }
}
