package com.synapsense.ejbinfo.units.resolvers;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.synapsense.ejbinfo.units.UnitDescriptor;
import com.synapsense.ejbinfo.units.Units;
import com.synapsense.ejbinfo.units.beans.BeanDescriptor;
import com.synapsense.ejbinfo.units.beans.session.ssb.SsbDescriptor;
import com.synapsense.ejbinfo.units.beans.session.ssb.StartupDependency;

public class LocalSsbStartupDependencyResolver {

    private static final Logger log = LoggerFactory
	    .getLogger(LocalSsbStartupDependencyResolver.class);

    public void process(Units units) {
	// we can only resolve dependencies locally per root unit
	for (UnitDescriptor unit : units.getUnits()) {
	    process(unit);
	}
    }

    private static void process(UnitDescriptor unit) {
	log.info("Resolving SSB startup dependencies in: " + unit.getName());
	resolveIn(unit, discoverSingletons(unit));
    }

    private static void resolveIn(UnitDescriptor unit,
	    Map<String, SsbDescriptor> singletons) {
	for (BeanDescriptor bean : unit.getBeans()) {
	    if (bean instanceof SsbDescriptor) {
		for (StartupDependency sd : ((SsbDescriptor) bean)
			.getStartupDependencies()) {
		    SsbDescriptor ssb = singletons.get(sd.name);
		    if (ssb != null) {
			sd.resolvedTo = ssb;
			log.info(String
				.format("Resolved SSB startup dependency: %s->\"%s\" to %s",
					bean.getName(), sd.name, ssb.getName()));
		    } else {
			log.warn(String
				.format("Unresolved SSB startup dependency: %s->\"%s\"",
					bean.getName(), sd.name));
		    }
		}
	    }
	}

	// process subunits recursively
	for (UnitDescriptor subUnit : unit.getSubUnits()) {
	    resolveIn(subUnit, singletons);
	}
    }

    private static Map<String, SsbDescriptor> discoverSingletons(
	    UnitDescriptor unit) {
	Map<String, SsbDescriptor> singletons = new HashMap<>();
	for (BeanDescriptor bean : unit.getBeans()) {
	    if (bean instanceof SsbDescriptor) {
		singletons.put(bean.getName(), (SsbDescriptor) bean);
	    }
	}
	// process subunits recursively
	for (UnitDescriptor subUnit : unit.getSubUnits()) {
	    singletons.putAll(discoverSingletons(subUnit));
	}
	return singletons;
    }
}
