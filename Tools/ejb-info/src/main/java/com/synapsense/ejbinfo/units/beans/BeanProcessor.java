package com.synapsense.ejbinfo.units.beans;

import org.reflections.Reflections;

public interface BeanProcessor {

    BeanDescriptor process(Class<?> bean, BeanDescriptor descr,
	    Reflections reflections);

}
