package com.synapsense.ejbinfo.units.beans.mdb;

import java.util.Set;

import javax.ejb.MessageDriven;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.synapsense.ejbinfo.context.Context;
import com.synapsense.ejbinfo.context.ContextProcessor;
import com.synapsense.ejbinfo.units.UnitDescriptor;
import com.synapsense.ejbinfo.units.beans.BeanDescriptor;

public class MdbScanner implements ContextProcessor {

    private static final Logger log = LoggerFactory.getLogger(MdbScanner.class);

    private static final MdbProcessor beanProcessor = new MdbProcessor();

    @Override
    public boolean supports(Context context) {
	Context parent = context.getParent();
	return parent == null ? false : parent.getName().endsWith(".ear")
		&& context.getName().endsWith(".jar");
    }

    @Override
    public void process(Context context) throws Exception {
	log.info("Scanning for MDB: " + context.getFullName());
	UnitDescriptor uDescr = context.getDescriptor();
	Set<Class<?>> mdbs = context.getReflections().getTypesAnnotatedWith(
		MessageDriven.class);
	for (Class<?> mdb : mdbs) {
	    BeanDescriptor descr = beanProcessor.process(mdb, null,
		    context.getReflections());
	    uDescr.addBean(descr);
	}
    }

}
