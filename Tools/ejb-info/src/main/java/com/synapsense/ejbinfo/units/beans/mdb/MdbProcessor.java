package com.synapsense.ejbinfo.units.beans.mdb;

import javax.ejb.MessageDriven;

import org.reflections.Reflections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.synapsense.ejbinfo.units.beans.BeanDescriptor;
import com.synapsense.ejbinfo.units.beans.DependencyProcessor;
import com.synapsense.ejbinfo.util.reflection.ReflectUtils;

public class MdbProcessor extends DependencyProcessor {

    private static final Logger log = LoggerFactory
	    .getLogger(MdbProcessor.class);

    @Override
    public BeanDescriptor process(Class<?> bean, BeanDescriptor descr,
	    Reflections reflections) {
	// create descriptor first
	String name = (String) ReflectUtils.getAnnotationValue(bean,
		MessageDriven.class, "name");
	if (name.isEmpty())
	    name = bean.getSimpleName();
	log.info("Found MDB: " + name + "(" + bean.getName() + ")");

	MdbDescriptor newDescr = new MdbDescriptor(name, bean);

	return super.process(bean, newDescr, reflections);
    }

}
