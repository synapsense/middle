package com.synapsense.ejbinfo.units;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.synapsense.ejbinfo.context.Context;
import com.synapsense.ejbinfo.context.ContextProcessor;

public class UnitCollector implements ContextProcessor {

    private final static Logger log = LoggerFactory
	    .getLogger(UnitCollector.class);

    private final Units units = new Units();

    @Override
    public boolean supports(Context context) {
	return true;
    }

    @Override
    public void process(Context context) throws Exception {
	UnitDescriptor descr = context.getDescriptor();
	if (descr != null) {
	    // check context's parent
	    Context parent = context.getParent();
	    if (parent == null || parent.getDescriptor() == null) {
		// unit from this context goes to roots
		units.add(descr);
		log.info(String.format("Unit %s is a root unit",
			descr.getName()));
	    } else {
		log.info(String.format("Unit %s is a subunit of unit %s",
			descr.getName(), parent.getDescriptor().getName()));
		parent.getDescriptor().addSubUnit(descr);
	    }
	}

	// go down recursively
	for (Context subContext : context.getSubContexts()) {
	    process(subContext);
	}

    }

    public Units getUnits() {
	return units;
    }

}
