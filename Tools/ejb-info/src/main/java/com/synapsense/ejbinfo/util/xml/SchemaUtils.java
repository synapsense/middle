package com.synapsense.ejbinfo.util.xml;

import java.io.InputStream;

import javax.xml.XMLConstants;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import com.synapsense.ejbinfo.units.ear.EarContextBuilder;

public class SchemaUtils {

    public static final Schema app6Schema;
    public static final Schema ejb31Schema;

    static {
	// Source[] schemaSources = new Source[4];
	//
	// InputStream schemaInput = EarContextBuilder.class
	// .getResourceAsStream("/schema/javaee_web_services_client_1_3.xsd");
	// schemaSources[0] = new StreamSource(schemaInput);
	//
	// schemaInput = EarContextBuilder.class
	// .getResourceAsStream("/schema/javaee_6.xsd");
	// schemaSources[1] = new StreamSource(schemaInput);

	InputStream schemaInput = EarContextBuilder.class
		.getResourceAsStream("/schema/ejb-jar_3_1.xsd");
	Source ejbSource = new StreamSource(schemaInput);

	schemaInput = EarContextBuilder.class
		.getResourceAsStream("/schema/application_6.xsd");
	Source appSource = new StreamSource(schemaInput);

	SchemaFactory sf = SchemaFactory
		.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);

	try {
	    sf.setResourceResolver(new ClasspathResourceResolver("schema"));
	    ejb31Schema = sf.newSchema(ejbSource);
	    app6Schema = sf.newSchema(appSource);
	} catch (Exception e) {
	    throw new RuntimeException("Cannot parse schemas", e);
	}
    }

}
