package com.synapsense.ejbinfo.util.xml;

import com.synapsense.ejbinfo.units.UnitDescriptor;
import com.synapsense.ejbinfo.units.beans.BeanDescriptor;
import com.synapsense.ejbinfo.units.beans.Dependency;
import com.synapsense.ejbinfo.units.beans.mdb.MdbDescriptor;
import com.synapsense.ejbinfo.units.beans.session.Export;
import com.synapsense.ejbinfo.units.beans.session.sfsb.SfsbDescriptor;
import com.synapsense.ejbinfo.units.beans.session.slsb.SlsbDescriptor;
import com.synapsense.ejbinfo.units.beans.session.ssb.SsbDescriptor;
import com.synapsense.ejbinfo.units.beans.session.ssb.StartupDependency;
import com.synapsense.ejbinfo.units.ear.EarUnitDescriptor;
import com.synapsense.ejbinfo.units.ejbjar.EjbJarUnitDescriptor;

public class UnitsObjectFactory {

    public static UnitDescriptor createUnitDescriptor() {
	return new UnitDescriptor("noname");
    }

    public static EarUnitDescriptor createEarUnitDescriptor() {
	return new EarUnitDescriptor("noname", null);
    }

    public static EjbJarUnitDescriptor createEjbJarUnitDescriptor() {
	return new EjbJarUnitDescriptor("noname", null);
    }

    public static BeanDescriptor createBeanDescriptor() {
	return new BeanDescriptor("noname", Object.class);
    }

    public static SsbDescriptor createSsbDescriptor() {
	return new SsbDescriptor("noname", Object.class, false);
    }

    public static SlsbDescriptor createSlsbDescriptor() {
	return new SlsbDescriptor("noname", Object.class);
    }

    public static SfsbDescriptor createSfsbDescriptor() {
	return new SfsbDescriptor("noname", Object.class);
    }

    public static MdbDescriptor createMdbDescriptor() {
	return new MdbDescriptor("noname", Object.class);
    }

    public static StartupDependency createStartupDependency() {
	return new StartupDependency("noname");
    }

    public static Dependency createDependency() {
	return new Dependency(Object.class, "unkMember", null);
    }

    public static Export createExport() {
	return new Export(Object.class, Export.Type.LOCAL);
    }
}
