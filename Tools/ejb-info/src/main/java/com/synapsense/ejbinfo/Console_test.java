package com.synapsense.ejbinfo;

import java.io.Closeable;
import java.io.File;
import java.lang.annotation.Annotation;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

import javax.ejb.Singleton;

import org.jboss.vfs.TempFileProvider;
import org.jboss.vfs.VFS;
import org.jboss.vfs.VirtualFile;
import org.jboss.vfs.util.FilterVirtualFileVisitor;
import org.jboss.vfs.util.SuffixMatchFilter;
import org.reflections.Reflections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Console_test {

    private static final Logger log = LoggerFactory.getLogger(Console_test.class);

    /**
     * @param args
     */
    public static void main(String[] args) throws Exception {

	System.out.println(new File("test/es-ear.ear").exists());

	VirtualFile homeDir = VFS.getChild("test");
	VirtualFile archive = homeDir.getChild("es-ear.ear");

	TempFileProvider provider = TempFileProvider.create("tmp",
		Executors.newSingleThreadScheduledExecutor(new ThreadFactory() {

		    @Override
		    public Thread newThread(Runnable r) {
			r.run();
			return null;
		    }
		}));

	try (Closeable handle = VFS
		.mountZipExpanded(archive, archive, provider)) {
	    SuffixMatchFilter jarFilter = new SuffixMatchFilter(".jar");
	    FilterVirtualFileVisitor visitor = new FilterVirtualFileVisitor(
		    jarFilter, org.jboss.vfs.VisitorAttributes.RECURSE);

	    archive.visit(visitor);

	    List<VirtualFile> jarFiles = visitor.getMatched();
	    Collection<URL> jars = new LinkedList<>();
	    for (VirtualFile vf : jarFiles) {
		URL jarUrl = vf.getPhysicalFile().toURI().toURL();
		log.info("Adding to scan: " + jarUrl);
		jars.add(jarUrl);
		// if (vf.getName().startsWith("es-core")) {
		// ClassLoader cl = new URLClassLoader(new URL[] { vf
		// .getPhysicalFile().toURI().toURL() },
		// Console.class.getClassLoader());
		// ClassLoader oldLoader = Thread.currentThread()
		// .getContextClassLoader();
		// Thread.currentThread().setContextClassLoader(cl);
		// Object clazz = Class
		// .forName("com.synapsense.impl.fws.FileWriterImpl",
		// false, cl);
		// Reflections refl = new Reflections(cl, vf.getPhysicalFile()
		// .toURI().toURL());
		// Set<Class<?>> singletons = refl
		// .getTypesAnnotatedWith(Singleton.class);
		// for (Class<?> singleton : singletons) {
		// System.out.println(singleton.getName());
		// }
		// Thread.currentThread().setContextClassLoader(oldLoader);
		// }
	    }

	    URL[] urls = new URL[jars.size()];
	    ClassLoader cl = new URLClassLoader(jars.toArray(urls));
	    for (URL jar : jars) {
		log.info("Scanning: " + jar);
		Reflections refl = new Reflections(cl, jar);
		Set<Class<?>> singletons = refl
			.getTypesAnnotatedWith(Singleton.class);
		for (Class<?> singleton : singletons) {
		    String name = getClassAnnotationValue(singleton,
			    Singleton.class, "name");
		    if (name == null || name.isEmpty())
			name = singleton.getSimpleName();
		    log.info("Singleton " + name + ": " + singleton.getName());
		}
	    }
	}

	provider.close();
    }

    private static String getClassAnnotationValue(Class<?> classType,
	    Class<? extends Annotation> annotationType, String attributeName) {
	String value = null;

	Annotation annotation = classType.getAnnotation(annotationType);
	if (annotation != null) {
	    try {
		value = (String) annotation.annotationType()
			.getMethod(attributeName).invoke(annotation);
	    } catch (Exception ex) {
	    }
	}

	return value;
    }
}
