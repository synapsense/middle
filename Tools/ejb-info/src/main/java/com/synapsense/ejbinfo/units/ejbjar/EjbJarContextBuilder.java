package com.synapsense.ejbinfo.units.ejbjar;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Unmarshaller;

import org.jboss.vfs.VirtualFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sun.java.xml.ns.javaee.EjbJarType;
import com.sun.java.xml.ns.javaee.ObjectFactory;
import com.synapsense.ejbinfo.context.Context;
import com.synapsense.ejbinfo.context.ContextProcessor;
import com.synapsense.ejbinfo.util.vfs.VFSUtils;
import com.synapsense.ejbinfo.util.xml.SchemaUtils;

public class EjbJarContextBuilder implements ContextProcessor {

    private static final Logger log = LoggerFactory
	    .getLogger(EjbJarContextBuilder.class);

    @Override
    public boolean supports(Context context) {
	Context parent = context.getParent();
	return parent == null ? false : parent.getName().endsWith(".ear")
		&& context.getName().endsWith(".jar");
    }

    @Override
    public void process(Context context) throws Exception {
	log.info("Building context for " + context.getFullName());

	VFSUtils.expand(context.getPath());

	EjbJarType ejbJarXml = parseEjbDescr(context.getPath());
	EjbJarUnitDescriptor ejbJarDescriptor = getDescriptor(context,
		ejbJarXml);
	context.setDescriptor(ejbJarDescriptor);

	// add ourselves to scan path
	VirtualFile vf = context.getPath();
	log.info("Adding to scan path: "
		+ vf.getPathNameRelativeTo(vf.getParent().getParent()));
	context.addScanUrl(vf.getPhysicalFile().toURI().toURL());

    }

    private static EjbJarUnitDescriptor getDescriptor(Context context,
	    EjbJarType ejbJarXml) {
	String name;
	if (ejbJarXml == null || ejbJarXml.getModuleName() == null) {
	    name = context.getName();
	    log.info("Module name defaulted to: " + name);

	} else {
	    name = ejbJarXml.getModuleName().getValue();
	    log.info("Module name taken from ejb-jar.xml/module-name: " + name);
	}
	return new EjbJarUnitDescriptor(name, ejbJarXml);
    }

    private static EjbJarType parseEjbDescr(VirtualFile jarFile)
	    throws Exception {
	// locate application.xml
	VirtualFile descrFile = jarFile.getChild("META-INF/ejb-jar.xml");
	if (descrFile.exists()) {
	    log.info("Parsing "
		    + descrFile.getPathNameRelativeTo(jarFile.getParent()));
	    JAXBContext ctx = JAXBContext.newInstance(ObjectFactory.class);
	    Unmarshaller um = ctx.createUnmarshaller();
	    um.setSchema(SchemaUtils.ejb31Schema);

	    @SuppressWarnings("unchecked")
	    JAXBElement<EjbJarType> ejbElement = (JAXBElement<EjbJarType>) um
		    .unmarshal(descrFile.getPhysicalFile());

	    return ejbElement.getValue();
	} else {
	    log.info(descrFile.getPathNameRelativeTo(jarFile.getParent())
		    + " not found");
	    return null;
	}
    }
}
