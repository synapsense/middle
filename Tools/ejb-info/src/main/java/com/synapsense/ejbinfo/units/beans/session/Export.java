package com.synapsense.ejbinfo.units.beans.session;

import java.util.Objects;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import com.synapsense.ejbinfo.units.UnitDescriptor;
import com.synapsense.ejbinfo.units.beans.BeanDescriptor;
import com.synapsense.ejbinfo.util.xml.UnitsObjectFactory;

@XmlType(factoryClass = UnitsObjectFactory.class, factoryMethod = "createExport")
@XmlAccessorType(XmlAccessType.FIELD)
public class Export {

    @XmlEnum(String.class)
    public enum Type {
	LOCAL, REMOTE
    }

    @XmlAttribute(name = "interface", required = true)
    public final Class<?> iface;

    @XmlAttribute(required = true)
    public final Type type;

    @XmlTransient
    public BeanDescriptor bean;

    public Export(Class<?> iface, Type type) {
	this.iface = Objects.requireNonNull(iface, "type must not be null");
	this.type = type;
    }

    public String getFullName() {
	StringBuilder sb = new StringBuilder(bean.getName() + "!"
		+ iface.getName());

	UnitDescriptor unit = bean.getUnit();
	while (unit != null) {
	    sb.insert(0, unit.getName() + "/");
	    unit = unit.getParentUnit();
	}

	return sb.insert(0, '/').toString();
    }

}
