package com.synapsense.ejbinfo.units.ear;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

import com.sun.java.xml.ns.javaee.ApplicationType;
import com.synapsense.ejbinfo.units.UnitDescriptor;
import com.synapsense.ejbinfo.util.xml.UnitsObjectFactory;

@XmlType(factoryClass = UnitsObjectFactory.class, factoryMethod = "createEarUnitDescriptor")
@XmlAccessorType(XmlAccessType.NONE)
public class EarUnitDescriptor extends UnitDescriptor {

    private final ApplicationType appXml;

    public EarUnitDescriptor(String name, ApplicationType appXml) {
	super(name);
	this.appXml = appXml;
    }

    public ApplicationType getAppXml() {
	return appXml;
    }
}
