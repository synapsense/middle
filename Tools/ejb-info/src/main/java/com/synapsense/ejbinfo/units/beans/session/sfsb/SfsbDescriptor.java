package com.synapsense.ejbinfo.units.beans.session.sfsb;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

import com.synapsense.ejbinfo.units.beans.BeanDescriptor;
import com.synapsense.ejbinfo.util.xml.UnitsObjectFactory;

@XmlType(factoryClass = UnitsObjectFactory.class, factoryMethod = "createSfsbDescriptor")
@XmlAccessorType(XmlAccessType.PROPERTY)
public class SfsbDescriptor extends BeanDescriptor {

    public SfsbDescriptor(String name, Class<?> clazz) {
	super(name, clazz);
    }

}
