package com.synapsense.ejbinfo.units.beans.session.ssb;

import java.util.Set;

import javax.ejb.Singleton;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.synapsense.ejbinfo.context.Context;
import com.synapsense.ejbinfo.context.ContextProcessor;
import com.synapsense.ejbinfo.units.UnitDescriptor;
import com.synapsense.ejbinfo.units.beans.BeanDescriptor;

public class SsbScanner implements ContextProcessor {

    private static final Logger log = LoggerFactory.getLogger(SsbScanner.class);

    private static final SsbProcessor beanProcessor = new SsbProcessor();

    @Override
    public boolean supports(Context context) {
	Context parent = context.getParent();
	return parent == null ? false : parent.getName().endsWith(".ear")
		&& context.getName().endsWith(".jar");
    }

    @Override
    public void process(Context context) throws Exception {
	log.info("Scanning for SSB: " + context.getFullName());
	UnitDescriptor uDescr = context.getDescriptor();
	Set<Class<?>> ssbs = context.getReflections().getTypesAnnotatedWith(
		Singleton.class);
	for (Class<?> ssb : ssbs) {
	    BeanDescriptor descr = beanProcessor.process(ssb, null,
		    context.getReflections());
	    uDescr.addBean(descr);
	}
    }

}
