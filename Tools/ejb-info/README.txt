Ejb Info User Guide

Ejb Info is a tool for automatic EJB information and dependencies analysis.

The tool processes contents of supported Java SE/EE archives and produces an XML file
with discovered information. The output file can be automatically transformed
using specified XSLT (a couple of is included) for further processing and analysis
with the third-party tools.

Please check release_notes.txt for supported archive types and provided XSLT templates. 


BUILDING FROM SOURCES

Prerequisites:
- Java JDK 1.7 or higher
- Maven 3.0 or higher
- Internet connection (for maven)

After checking out sources from git cd into ejb-info folder and type 'maven package'.
A successful build produces ejb-info/target/ejb-info-n.n-bin.zip file.


INSTALLATION

Grab the zip file and unpack it to any folder of your choice; that would be all for
installation.


USAGE

Prerequisites:
- Java JRE/JDK 1.7 or higher

From the unzipped folder, run ejb-info.bat with no parameters or with -help for
usage description.

The tool can analyze a single zipped/exploded SE/EE archive or a folder containing
any combination of those. For example, if you have an exploded EAR and a couple of
packed jars in D:\data\test just run (note: relative paths are supported, too):

  ejb-info D:\data\test

By default, the resulting files (xml and xsd) will be placed into ./out folder;
no xslt transformation will be applied. To apply any transformation use
-t parameter; to change the default transformed output file name (ejb-info.res.xml)
use -t_out parameter. For example, to produce DOT file res.dot run:

  ejb-info -t conf\dot.xslt -t_out res.dot D:\data\test

Supplied xslt files are located in conf folder.


LOGGING

SLF4J and Logback are used for logging (Logback is a native SLF4J implementation).
Logs are placed into log folder. Logback.xml configuration file is located in
conf folder.

For Logback configuration details see http://logback.qos.ch/manual/index.html.
For information on swapping SLF4J implementation see
http://www.slf4j.org/legacy.html

TROUBLESHOOTING

If all of your dependencies are included inside an EAR then you can simply point
 the tool at it directly. However, this is a rare situation: most likely you'll
 hit a NoClassDefFoundException showing that some of the dependencies are missed.
 Don't get frustrated: find the jar with a missed class (on your local system
 or using something like findjar.com) and drop it in a folder along with EAR;
 then point the tool to the folder. Repeat as necessary.

For example, to successfully parse our current es-ear.ear one would need the
following jars placed along with it:
- drools-compiler-4.0.6.jar
- drools-core-4.0.6.jar
- hibernate-core-4.0.1.Final.jar


UNIX NOTE

Although there's no .sh run script version provided one can easily figure out how
to launch the tool by looking at ejb-info.bat. Unix-style paths are supported.
