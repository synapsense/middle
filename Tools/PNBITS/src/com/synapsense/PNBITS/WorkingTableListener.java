package com.synapsense.PNBITS;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JTable;

public class WorkingTableListener implements KeyListener {
	int i;
	JTable[] workingTable;

	public WorkingTableListener(int workingTableNum, JTable[] workingTable) {
		super();
		this.i = workingTableNum;
		this.workingTable = workingTable;
	}

	@Override
	public void keyPressed(KeyEvent code) {
		int c;

		if (code.getKeyCode() == 10) {
			// System.out.print(code.getKeyCode());

			c = workingTable[i].getSelectedColumn();
			if (c < 5) {
				workingTable[i].changeSelection(5, c + 1, true, true);

			} else if (i < 4) {
				workingTable[i].requestFocus(false);
				workingTable[i + 1].requestFocus(true);
				workingTable[i + 1].changeSelection(5, 1, true, true);
				workingTable[i + 1].setRowSelectionInterval(0, 0);
				workingTable[i + 1].setColumnSelectionInterval(1, 1);
			} else {
				workingTable[0].requestFocus(true);
				workingTable[0].changeSelection(5, 1, true, true);
				workingTable[0].setRowSelectionInterval(0, 0);
				workingTable[0].setColumnSelectionInterval(1, 1);
			}

		}
	}

	@Override
	public void keyReleased(KeyEvent code) {
		// TODO Auto-generated method stub
		if (code.getKeyCode() == 10) {
			if (i < 4 && workingTable[i].getSelectedColumn() == 5 && workingTable[i].getSelectedRow() == 1) {
				workingTable[i].requestFocus(false);
				workingTable[i + 1].requestFocus(true);
				workingTable[i + 1].changeSelection(5, 1, true, true);
				workingTable[i + 1].setRowSelectionInterval(0, 0);
				workingTable[i + 1].setColumnSelectionInterval(1, 1);
			}
			/*
			 * else
			 * if(i==4&&workingTable[i].getSelectedColumn()==5&&workingTable
			 * [i].getSelectedRow()==1) { workingTable[i].requestFocus(false) ;
			 * workingTable[0].requestFocus(true);
			 * workingTable[0].changeSelection(5,1,true, true);
			 * workingTable[0].setRowSelectionInterval(0,0);
			 * workingTable[0].setColumnSelectionInterval(1,1); }
			 */
		}
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

}
