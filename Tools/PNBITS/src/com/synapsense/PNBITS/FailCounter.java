package com.synapsense.PNBITS;

import java.util.ArrayList;

public class FailCounter {

	public Long nodeID = new Long(0);
	public Integer messageNum = new Integer(0);
	public Integer failNum = new Integer(0);
	public Boolean joinedInTime = false;
	public Long lastMessageTime = new Long(0);

	public ArrayList<Double> pressureValues = new ArrayList<Double>();

	public FailCounter() {

	}

	public FailCounter(Long nodeID) {
		this.nodeID = nodeID;
	}

	public FailCounter(Long nodeID, Integer messageNum, Integer failNum, boolean joinedinTime, Long lastMessageTime) {
		this.nodeID = nodeID;
		this.messageNum = messageNum;
		this.failNum = failNum;
		this.joinedInTime = joinedinTime;
		this.lastMessageTime = lastMessageTime;
	}

}
