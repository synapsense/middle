package com.synapsense.PNBITS;

import java.beans.XMLDecoder;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.swing.JTable;
import javax.swing.JTextArea;

import org.apache.log4j.Logger;
import com.synapsense.plugin.api.DeviceManagerTransport;
import com.synapsense.plugin.api.TransportConnectionFailureListener;
import com.synapsense.plugin.manager.DeviceManagerCore;
import com.synapsense.plugin.messages.GenericMessage;
import com.synapsense.plugin.messages.GenericMessage.GenericMessageTypes;

public class Transport2 implements DeviceManagerTransport {
	private final static Logger logger = Logger.getLogger(Transport2.class);
	private final static int PRIORITY = 2;
	Collection<Serializable> incMessage;
	JTextArea jTextArea = null;
	JTable[] workingTable = new JTable[5];
	String[][] nodeIDs;
	int voltageType = 5;
	int pressureType = 29;
	Long startTime = 0L;
	boolean reportC;
	Config conf;
	LineFunc[] lineFunc = new LineFunc[5];

	public Transport2(JTextArea infoArea, JTable[] workingTable, String[][] nodeIDs) {

		this.jTextArea = infoArea;
		this.workingTable = workingTable;
		this.nodeIDs = nodeIDs;
		this.conf = new Config();
		for (int i = 0; i < 5; i++) {
			this.lineFunc[i] = new LineFunc();
		}
		readFromCFG();
		startTime = System.currentTimeMillis();

	}

	@Override
	public void connect() throws IOException {
		// nothing to connect to
	}

	@Override
	public void disconnect() {
		// nothing to disconnect from
	}

	@Override
	public int getPriority() {
		return PRIORITY;
	}

	@Override
	public void sendMessage(GenericMessage message) {
		if (message == null) {
			throw new IllegalArgumentException("message cannot be null");
		}

		if (System.currentTimeMillis() - startTime < conf.getJoinTime()) {
			waitForJoin(message);
		}

		if (System.currentTimeMillis() - startTime > conf.getJoinTime()
		        && System.currentTimeMillis() - startTime < (conf.getTestTime() + conf.getJoinTime())) {
			System.out.print("<<<<<<<<<<<<Transport sending message:" + message.getType() + "\n");
			Collection<Serializable> incMessage;
			ArrayList<Serializable> goodMessage = new ArrayList<Serializable>();

			if (message.getType() != GenericMessageTypes.WSN_SENSOR_DATA)
				return;

			System.out.print(message.getType() + "\n");
			System.out.print(message.values() + "\n");
			logger.info("Got WSN_SENSOR_DATA message: " + message.values());
			incMessage = message.values();
			Iterator<Serializable> iter = incMessage.iterator();

			while (iter.hasNext()) {
				goodMessage.add(iter.next());
			}

			try {
				if (goodMessage.get(1).equals(voltageType)) {
					for (int i = 0; i < 5; i++) {
						for (int j = 0; j < 6 && (workingTable[i].getValueAt(0, j) != null); j++) {
							if (new Long(goodMessage.get(2).toString()).equals(Long.decode(("0x" + workingTable[i]
							        .getValueAt(0, j).toString())))) {
								workingTable[i].setValueAt(goodMessage.get(6), 4, j);
							}
						}
					}
				}

				int k = 0;
				if (goodMessage.get(1).equals(pressureType)) {
					for (int i = 0; i < 5; i++) {
						Double valGold;
						Double valTest;
						for (int j = 0; j < 6; j++) { // not required to check
													  // against nulls. see
													  // worker2 - all nodes[][]
													  // are initialized
							if (new Long(goodMessage.get(2).toString()).equals(new Long(nodeIDs[i][j].toString()))) {
								workingTable[i].setValueAt(goodMessage.get(6), 3, j);
							}
							if (new Long(goodMessage.get(2).toString()).equals(new Long(MainWindow.failC[k].nodeID))) {
								MainWindow.failC[k].messageNum++;
								if (!SetupWindow.isApproximationOn.isSelected()) {
									valGold = new Double(Double.parseDouble((workingTable[i].getValueAt(3, 0)
									        .toString())));
								} else {
									valGold = new Double(Double.parseDouble((lineFunc[i].calculateValue(System
									        .currentTimeMillis()).toString())));
								}

								valTest = new Double(Double.parseDouble(workingTable[i].getValueAt(3, j).toString()));

								if (Math.abs(valGold - valTest) <= valGold * conf.getRange()) {
									workingTable[i].setValueAt("ok", 2, j);
								} else {
									logger.warn("Node 0x" + Long.toHexString(MainWindow.failC[i].nodeID)
									        + " failed: sensed data is out of range.");
									workingTable[i].setValueAt("fail", 2, j);
									MainWindow.failC[k].failNum++;
								}
								if (System.currentTimeMillis() - MainWindow.failC[k].lastMessageTime >= 2000 * conf
								        .getSampling() && MainWindow.failC[i].lastMessageTime != 0L) {
									logger.warn("Node 0x" + Long.toHexString(MainWindow.failC[i].nodeID)
									        + " failed: data wasn't sent in 2*sampling interval");
									MainWindow.failC[k].failNum++;
								}

								MainWindow.failC[k].lastMessageTime = System.currentTimeMillis();
							}
							k++;
						}
					}
				}
			} catch (NullPointerException e) {
			}
		}
	}

	@Override
	public void setFailuresListener(TransportConnectionFailureListener listener) {
		// no failures... everything is all right
	}

	@Override
	public void enable() {

		DeviceManagerCore.getInst().enablePushers();
	}

	private String convertHexToDec(String Hex) {
		Long temp = Long.decode("0x" + Hex);
		Hex = Long.toString(temp);
		return Hex;
	}

	private void waitForJoin(GenericMessage message) {

		if (message.getType() == GenericMessageTypes.WSN_SENSOR_DATA) {
			System.out.print("<<<<<<<<<<<<Transport sending message:" + message.getType() + "\n");

			Collection<Serializable> incMessage;
			ArrayList<Serializable> goodMessage = new ArrayList<Serializable>();

			System.out.print(message.getType() + "\n");
			System.out.print(message.values() + "\n");

			incMessage = message.values();
			Iterator<Serializable> iter = incMessage.iterator();
			while (iter.hasNext()) {
				goodMessage.add(iter.next());
			}
			int k = 0;
			for (int i = 0; i < 5; i++) {
				for (int j = 0; j < 6 && workingTable[i].getValueAt(0, j) != null; j++) {

					System.out.print(Long.toHexString(new Long(goodMessage.get(2).toString())) + "\n");
					System.out.print(workingTable[i].getValueAt(0, j).toString() + "\n");

					// Long
					// temp=Long.decode("0x"+workingTable[i].getValueAt(0,0).toString());

					if (MainWindow.failC[k].joinedInTime == false) {
						if (new Long(goodMessage.get(2).toString()).equals(Long.decode(("0x" + workingTable[i]
						        .getValueAt(0, j).toString())))) {
							workingTable[i].setValueAt("sending", 2, j);
							MainWindow.failC[k].joinedInTime = true;
							if (j == 0) {
								if (goodMessage.get(1).equals(pressureType)) {

									workingTable[i].setValueAt(goodMessage.get(6), 3, 0);
								}
								if (goodMessage.get(1).equals(voltageType)) {

									workingTable[i].setValueAt(goodMessage.get(6), 4, 0);
								}
							}
						} else {
							workingTable[i].setValueAt("not recieved yet", 2, j);
						}
					}
					k++;
					if (new Long(goodMessage.get(2).toString()).equals(Long.decode(("0x" + workingTable[i].getValueAt(
					        0, 0).toString())))
					        && new Integer(goodMessage.get(1).toString()).equals(pressureType)) {
						lineFunc[i]
						        .setParameters(System.currentTimeMillis(), new Double(goodMessage.get(6).toString()));
					}

				}
			}
		}
	}

	private void readFromCFG() {

		/*
		 * ObjectInputStream in = null; try { in = new ObjectInputStream(new
		 * BufferedInputStream( new FileInputStream("Config.cfg")));
		 * conf=(Config) in.readObject(); } catch ( IOException ex ) { //
		 * ex.printStackTrace();
		 * System.out.print("Cant read config, will load default config\n");
		 * MainWindow
		 * .infoArea.append("Cant read config, will load default config");
		 * conf=new Config(); } catch ( Exception ex ) { //
		 * ex.printStackTrace();
		 * System.out.print("Cant read config, will load default config\n");
		 * MainWindow
		 * .infoArea.append("Cant read config, will load default config");
		 * conf=new Config(); }finally { if ( in != null ) try { in.close(); }
		 * catch ( IOException ex ) { ex.printStackTrace(); } }
		 */
		FileInputStream in = null;
		try {
			in = new FileInputStream("Config.xml");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.print("Cant read config, will load default config\n");
			MainWindow.infoArea.append("Cant read config, will load default config");
			conf = new Config();
		}
		XMLDecoder xmlDecoder = new XMLDecoder(in);
		conf = (Config) xmlDecoder.readObject();
		xmlDecoder.close();

	}

}
