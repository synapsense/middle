package com.synapsense.PNBITS;

import java.beans.XMLDecoder;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import javax.swing.JProgressBar;
import javax.swing.JTextArea;

public class TimeControl extends Thread {

	Long testTime;
	Long joinTime;
	Long startTime;
	JTextArea jTextArea = null;
	JProgressBar jProgressBar = null;
	Config conf;
	Thread thread;
	MainWindow mainWindow;

	public TimeControl(JTextArea jTextArea, JProgressBar jProgressBar, MainWindow mainWindow) {
		this.conf = new Config();
		this.jTextArea = jTextArea;
		this.jProgressBar = jProgressBar;
		this.mainWindow = mainWindow;
	}

	@Override
	public void run() {
		while (System.currentTimeMillis() - startTime <= testTime + joinTime + 2000) {
			Integer value = new Integer(new Long((System.currentTimeMillis() - startTime) / 1000).toString());
			jProgressBar.setValue(value);

			Integer temp = jProgressBar.getValue();
			if (temp >= new Integer((new Long((testTime + joinTime) / 1000).toString()))) {
				System.out.print("Test is over");
				jTextArea.setText("Test is over");
				ReportWindow.getInst().showReport();

				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				this.mainWindow.executeStop();
			}
			try {
				TimeControl.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	private void readFromCFG() {

		/*
		 * ObjectInputStream in = null; try { in = new ObjectInputStream(new
		 * BufferedInputStream( new FileInputStream("Config.cfg")));
		 * conf=(Config) in.readObject(); } catch ( IOException ex ) { //
		 * ex.printStackTrace();
		 * System.out.print("Cant read config, will load default config\n");
		 * MainWindow
		 * .infoArea.append("Cant read config, will load default config");
		 * conf=new Config(); } catch ( Exception ex ) { //
		 * ex.printStackTrace();
		 * System.out.print("Cant read config, will load default config\n");
		 * MainWindow
		 * .infoArea.append("Cant read config, will load default config");
		 * conf=new Config(); }finally { if ( in != null ) try { in.close(); }
		 * catch ( IOException ex ) { ex.printStackTrace(); } }
		 */
		FileInputStream in = null;
		try {
			in = new FileInputStream("Config.xml");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.print("Cant read config, will load default config\n");
			MainWindow.infoArea.append("Cant read config, will load default config");
			conf = new Config();
		}
		XMLDecoder xmlDecoder = new XMLDecoder(in);
		conf = (Config) xmlDecoder.readObject();
		xmlDecoder.close();
	}

	public void startTimer() {

		readFromCFG();
		this.testTime = conf.getTestTime();
		this.joinTime = conf.getJoinTime();
		this.startTime = System.currentTimeMillis();
		Integer value = new Integer((new Long((testTime + joinTime) / 1000).toString()));
		jProgressBar.setMaximum(value);
		jProgressBar.setStringPainted(true);
		thread = new Thread(this);
		thread.start();
	}

	public void stopTimer() {
		jProgressBar.setValue(0);
		testTime = 0L;
		joinTime = 0L;
		startTime = 0L;
		thread = null;
	}
}
