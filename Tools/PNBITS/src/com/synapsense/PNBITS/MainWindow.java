package com.synapsense.PNBITS;

import javax.swing.*;

import com.synapsense.plugin.api.exceptions.ConfigPluginException;
import com.synapsense.plugin.wsn.NetworkWorker;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyListener;
import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class MainWindow {

	Object[][][] data = new Object[5][5][6];

	String[] colHeaders = { "GoldNode", "TestNode1", "TestNode2", "TestNode3", "TestNode4", "TestNode5" };

	JFrame jFrame = new JFrame("PNBITS");

	MyJTable[] workingTable = new MyJTable[5];
	MainWindow inst = this;

	public static JTextArea infoArea = new JTextArea(4, 42);
	JScrollPane jScrollPaneWorkingTable1 = null;
	JScrollPane jScrollPaneWorkingTable2 = null;
	JScrollPane jScrollPaneWorkingTable3 = null;
	JScrollPane jScrollPaneWorkingTable4 = null;
	JScrollPane jScrollPaneWorkingTable5 = null;

	JScrollPane jScrollPaneInfoArea = new JScrollPane(infoArea);
	JMenuBar jMenuBar = new JMenuBar();
	JButton startButton = new JButton("start");
	JButton setupButton = new JButton("setup");
	JButton stopButton = new JButton("stop");

	public static JProgressBar jProgressBar = null;

	Worker2 worker = null;
	TimeControl timeControl = null;

	public static FailCounter[] failC = new FailCounter[30];

	public static Config conf = null;
	NetworkWorker networker = null;
	Transport2 transport = null;
	final static int goldNodesMaxAmmount = 5;
	final static int testNodesMaxAmmount = 25;

	String[][] nodeIDs = new String[5][6];

	MainWindow() {

		setupGUI();

		conf = new Config();
		// sampling=new Integer(30);

		for (int i = 0; i < 30; i++) {
			MainWindow.failC[i] = new FailCounter();
		}

		readFromCFG();
		initActionListeners();

	}

	void initActionListeners() {

		KeyListener[] waitforEnter = new WorkingTableListener[5];

		for (int i = 0; i < 5; i++) {
			waitforEnter[i] = new WorkingTableListener(i, workingTable);
		}

		for (int i = 0; i < 5; i++) {
			workingTable[i].addKeyListener(waitforEnter[i]);
		}

		startButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				if (worker == null) {
					executeStart();
				} else {
					executeStop();
				}
			}

		});
		setupButton.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				new SetupWindow(inst);

			}

		});

	}

	public void executeStart() {
		Boolean isMACsOK = true;
		MainWindow.infoArea.setText("");
		for (int i = 0; i < 5; i++) {
			for (int j = 1; j < 5; j++) {
				for (int k = 0; k < 6; k++) {
					workingTable[i].setValueAt(null, j, k);
				}
			}
			for (int j = 0; j < 6; j++) {
				if (workingTable[i].getValueAt(0, j) != null && workingTable[i].getValueAt(0, j) != "") {
					try {
						Long temp = Long.decode("0x" + workingTable[i].getValueAt(0, j));
					} catch (java.lang.NumberFormatException e) {
						System.out.print("Wrong MAC ID was set, reset MAC ID in " + (i + 1) + " table " + (j + 1)
						        + " column\n");
						MainWindow.infoArea.append("Wrong MAC ID was set, reset MAC ID in " + (i + 1) + " table "
						        + (j + 1) + " column\n");
						isMACsOK = false;
					}
				}
			}
		}
		if (!isMACsOK) {
			return;
		}
		try {
			transport = new Transport2(infoArea, workingTable, nodeIDs);
			worker = new Worker2(nodeIDs, goldNodesMaxAmmount, testNodesMaxAmmount, transport, workingTable);
			worker.initWSNplugin();
			startButton.setText("Stop");
			setupButton.setEnabled(false);

			for (int i = 0; i < 5; i++) {
				if (workingTable[i].isEnabled())
					workingTable[i].setEnabled(false);
			}
		} catch (ConfigPluginException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		timeControl.startTimer();

	}

	public void executeStop() {
		System.out.print("stop pressed\n");
		try {
			worker.stopDM();
		} catch (ConfigPluginException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		worker = null;
		transport = null;

		for (int i = 0; i < 30; i++) {
			MainWindow.failC[i].failNum = 0;
			MainWindow.failC[i].messageNum = 0;
			MainWindow.failC[i].nodeID = 0L;
			MainWindow.failC[i].joinedInTime = false;
		}

		startButton.setText("Start");
		setupButton.setEnabled(true);
		for (int i = 0; i < 5; i++) {
			if (!workingTable[i].isEnabled())
				workingTable[i].setEnabled(true);

		}

		timeControl.stopTimer();

	}

	private void writeCFG(Config conf) {
		/*
		 * ObjectOutputStream out = null; try { out = new ObjectOutputStream(new
		 * BufferedOutputStream( new FileOutputStream("Config.cfg")));
		 * out.writeObject(conf); } catch ( IOException ex ) {
		 * ex.printStackTrace(); System.out.print("cant write config file \n");
		 * MainWindow.infoArea.append("cant write config file \n"); }finally {
		 * if ( out != null ) try { out.close(); } catch ( IOException ex ) {
		 * ex.printStackTrace(); } }
		 */

		FileOutputStream out = null;
		try {
			out = new FileOutputStream("Config.xml");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.print("cant write config file \n");
			MainWindow.infoArea.append("cant write config file \n");
		}
		XMLEncoder xmlEncoder = new XMLEncoder(out);
		xmlEncoder.writeObject(conf);
		xmlEncoder.flush();
		xmlEncoder.close();

	}

	public void readFromCFG() {
		FileInputStream in = null;
		try {
			in = new FileInputStream("Config.xml");
			XMLDecoder xmlDecoder = new XMLDecoder(in);
			conf = (Config) xmlDecoder.readObject();
			xmlDecoder.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			System.out.print("Cant read config, will load default config\n");
			MainWindow.infoArea.append("Cant read config, will load default config");
			conf = new Config();
			SetupWindow.writeCFG(conf);
		}
		for (int i = 0; i < 5; i++) {
			workingTable[i].setValueAt(conf.getGoldNodes()[i], 0, 0);
		}

	}

	private void setupGUI() {

		jFrame.getContentPane().setLayout(new FlowLayout());
		jFrame.setSize(1100, 800);
		jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		jFrame.setResizable(false);

		for (int i = 0; i < 5; i++) {
			workingTable[i] = new MyJTable(data[i], colHeaders);
			workingTable[i].setPreferredSize(new Dimension(1000, 80));
			workingTable[i].setEnabled(true);
			workingTable[i].setColumnSelectionAllowed(false);
			workingTable[i].setRowSelectionAllowed(false);
		}

		infoArea.setPreferredSize(new Dimension(1000, 50));
		infoArea.setLineWrap(true);

		jScrollPaneWorkingTable1 = new JScrollPane(workingTable[0]);
		jScrollPaneWorkingTable2 = new JScrollPane(workingTable[1]);
		jScrollPaneWorkingTable3 = new JScrollPane(workingTable[2]);
		jScrollPaneWorkingTable4 = new JScrollPane(workingTable[3]);
		jScrollPaneWorkingTable5 = new JScrollPane(workingTable[4]);
		jScrollPaneWorkingTable1.setPreferredSize(new Dimension(1000, 100));
		jScrollPaneWorkingTable2.setPreferredSize(new Dimension(1000, 100));
		jScrollPaneWorkingTable3.setPreferredSize(new Dimension(1000, 100));
		jScrollPaneWorkingTable4.setPreferredSize(new Dimension(1000, 100));
		jScrollPaneWorkingTable5.setPreferredSize(new Dimension(1000, 100));
		jScrollPaneInfoArea.setPreferredSize(new Dimension(1000, 100));

		MainWindow.jProgressBar = new JProgressBar();
		MainWindow.jProgressBar.setStringPainted(true);

		jFrame.getContentPane().add(jScrollPaneWorkingTable1);
		jFrame.getContentPane().add(jScrollPaneWorkingTable2);
		jFrame.getContentPane().add(jScrollPaneWorkingTable3);
		jFrame.getContentPane().add(jScrollPaneWorkingTable4);
		jFrame.getContentPane().add(jScrollPaneWorkingTable5);
		jFrame.getContentPane().add(jProgressBar);

		jFrame.getContentPane().add(jScrollPaneInfoArea);

		timeControl = new TimeControl(infoArea, jProgressBar, this);

		jMenuBar.add(startButton);
		jMenuBar.add(setupButton);

		jFrame.getContentPane().add(jMenuBar);
		jFrame.setVisible(true);

	}

	private class MyJTable extends JTable {

		public MyJTable(Object[][] objects, String[] colHeaders) {
			super(objects, colHeaders);

		}

		@Override
		public boolean isCellEditable(int row, int column) {
			if (row != 0 || column == 0) {
				return false;
			} else
				return true;
		}

	}

}