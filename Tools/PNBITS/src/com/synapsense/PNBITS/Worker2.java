package com.synapsense.PNBITS;

import java.beans.XMLDecoder;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import java.util.Collection;

import javax.swing.JTable;

import com.synapsense.devicemapper.typemapping.Mapping;
import com.synapsense.plugin.api.exceptions.ConfigPluginException;
import com.synapsense.plugin.manager.DeviceManagerCore;

public class Worker2 {

	String[][] nodeIDs;
	Transport2 transport = null;
	JTable[] workingTable;
	Config conf;
	DeviceManagerCore DM;

	Worker2(String[][] nodeIDs, int goldNodesMaxAmmount, int testNodesMaxAmmount, Transport2 transport,
	        JTable[] workingTable) {
		DM = DeviceManagerCore.getInst();
		this.conf = new Config();
		readFromCFG();
		this.workingTable = workingTable;
		this.transport = transport;
		this.nodeIDs = nodeIDs;
		int k = 0;
		for (int i = 0; i < goldNodesMaxAmmount; i++) {
			for (int j = 0; j < 6; j++) {
				// if(nodeIDs[i][j]==null){

				if (workingTable[i].getValueAt(0, j) != null) {
					nodeIDs[i][j] = workingTable[i].getValueAt(0, j).toString();
					nodeIDs[i][j] = convertHexToDec(nodeIDs[i][j]);
				} else {
					nodeIDs[i][j] = "0";
				}
				// }
				MainWindow.failC[k].nodeID = new Long(nodeIDs[i][j]);
				k++;
			}

		}

	}

	void initWSNplugin() throws ConfigPluginException {

		String config = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
		        + "<wsn:wsn xmlns:wsn=\"http://www.synapsense.com/devicemapper/wsn\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.synapsense.com/devicemapper/wsn wsn.xsd \">"
		        + "<wsn:network name=\"network1\" networkID=\"1\" panID=\""
		        + conf.getPanID()
		        + "\" sampling=\""
		        + conf.getSampling().toString()
		        + "\" softbased=\"true\" version=\"1.1\">"
		        + "<wsn:nodes>"
		        +
		        // gold node 1
		        "<wsn:node logicalID=\"1\" physID=\""
		        + nodeIDs[0][0]
		        + "\" platformID=\"1\" sampling=\""
		        + conf.getSampling().toString()
		        + "\">"
		        + "<wsn:sensor channel=\"2\" fwtype=\"0\" max=\"100.0\" min=\"0.0\" sampling=\"0\" type=\"1\">"
		        + "</wsn:sensor>"
		        + "<wsn:sensor channel=\"3\" fwtype=\"0\" max=\"100.0\" min=\"0.0\" sampling=\"0\" type=\"1\">"
		        + "</wsn:sensor>"
		        + "</wsn:node>"
		        +
		        // test node 1
		        "<wsn:node logicalID=\"2\" physID=\""
		        + nodeIDs[0][1]
		        + "\" platformID=\"1\" sampling=\""
		        + conf.getSampling().toString()
		        + "\">"
		        + "<wsn:sensor channel=\"2\" fwtype=\"0\" max=\"100.0\" min=\"0.0\" sampling=\"0\" type=\"1\">"
		        + "</wsn:sensor>"
		        + "<wsn:sensor channel=\"3\" fwtype=\"0\" max=\"100.0\" min=\"0.0\" sampling=\"0\" type=\"1\">"
		        + "</wsn:sensor>"
		        + "</wsn:node>"
		        +
		        // test node 2
		        "<wsn:node logicalID=\"3\" physID=\""
		        + nodeIDs[0][2]
		        + "\" platformID=\"1\" sampling=\""
		        + conf.getSampling().toString()
		        + "\">"
		        + "<wsn:sensor channel=\"2\" fwtype=\"0\" max=\"100.0\" min=\"0.0\" sampling=\"0\" type=\"1\">"
		        + "</wsn:sensor>"
		        + "<wsn:sensor channel=\"3\" fwtype=\"0\" max=\"100.0\" min=\"0.0\" sampling=\"0\" type=\"1\">"
		        + "</wsn:sensor>"
		        + "</wsn:node>"
		        +
		        // test node 3
		        "<wsn:node logicalID=\"4\" physID=\""
		        + nodeIDs[0][3]
		        + "\" platformID=\"1\" sampling=\""
		        + conf.getSampling().toString()
		        + "\">"
		        + "<wsn:sensor channel=\"2\" fwtype=\"0\" max=\"100.0\" min=\"0.0\" sampling=\"0\" type=\"1\">"
		        + "</wsn:sensor>"
		        + "<wsn:sensor channel=\"3\" fwtype=\"0\" max=\"100.0\" min=\"0.0\" sampling=\"0\" type=\"1\">"
		        + "</wsn:sensor>"
		        + "</wsn:node>"
		        +
		        // test node 4
		        "<wsn:node logicalID=\"5\" physID=\""
		        + nodeIDs[0][4]
		        + "\" platformID=\"1\" sampling=\""
		        + conf.getSampling().toString()
		        + "\">"
		        + "<wsn:sensor channel=\"2\" fwtype=\"0\" max=\"100.0\" min=\"0.0\" sampling=\"0\" type=\"1\">"
		        + "</wsn:sensor>"
		        + "<wsn:sensor channel=\"3\" fwtype=\"0\" max=\"100.0\" min=\"0.0\" sampling=\"0\" type=\"1\">"
		        + "</wsn:sensor>"
		        + "</wsn:node>"
		        +
		        // test node 5
		        "<wsn:node logicalID=\"6\" physID=\""
		        + nodeIDs[0][5]
		        + "\" platformID=\"1\" sampling=\""
		        + conf.getSampling().toString()
		        + "\">"
		        + "<wsn:sensor channel=\"2\" fwtype=\"0\" max=\"100.0\" min=\"0.0\" sampling=\"0\" type=\"1\">"
		        + "</wsn:sensor>"
		        + "<wsn:sensor channel=\"3\" fwtype=\"0\" max=\"100.0\" min=\"0.0\" sampling=\"0\" type=\"1\">"
		        + "</wsn:sensor>"
		        + "</wsn:node>"
		        +
		        // gold node 2
		        "<wsn:node logicalID=\"7\" physID=\""
		        + nodeIDs[1][0]
		        + "\" platformID=\"1\" sampling=\""
		        + conf.getSampling().toString()
		        + "\">"
		        + "<wsn:sensor channel=\"2\" fwtype=\"0\" max=\"100.0\" min=\"0.0\" sampling=\"0\" type=\"1\">"
		        + "</wsn:sensor>"
		        + "<wsn:sensor channel=\"3\" fwtype=\"0\" max=\"100.0\" min=\"0.0\" sampling=\"0\" type=\"1\">"
		        + "</wsn:sensor>"
		        + "</wsn:node>"
		        +
		        // test node 6
		        "<wsn:node logicalID=\"8\" physID=\""
		        + nodeIDs[1][1]
		        + "\" platformID=\"1\" sampling=\""
		        + conf.getSampling().toString()
		        + "\">"
		        + "<wsn:sensor channel=\"2\" fwtype=\"0\" max=\"100.0\" min=\"0.0\" sampling=\"0\" type=\"1\">"
		        + "</wsn:sensor>"
		        + "<wsn:sensor channel=\"3\" fwtype=\"0\" max=\"100.0\" min=\"0.0\" sampling=\"0\" type=\"1\">"
		        + "</wsn:sensor>"
		        + "</wsn:node>"
		        +
		        // test node 7
		        "<wsn:node logicalID=\"9\" physID=\""
		        + nodeIDs[1][2]
		        + "\" platformID=\"1\" sampling=\""
		        + conf.getSampling().toString()
		        + "\">"
		        + "<wsn:sensor channel=\"2\" fwtype=\"0\" max=\"100.0\" min=\"0.0\" sampling=\"0\" type=\"1\">"
		        + "</wsn:sensor>"
		        + "<wsn:sensor channel=\"3\" fwtype=\"0\" max=\"100.0\" min=\"0.0\" sampling=\"0\" type=\"1\">"
		        + "</wsn:sensor>"
		        + "</wsn:node>"
		        +
		        // test node 8
		        "<wsn:node logicalID=\"10\" physID=\""
		        + nodeIDs[1][3]
		        + "\" platformID=\"1\" sampling=\""
		        + conf.getSampling().toString()
		        + "\">"
		        + "<wsn:sensor channel=\"2\" fwtype=\"0\" max=\"100.0\" min=\"0.0\" sampling=\"0\" type=\"1\">"
		        + "</wsn:sensor>"
		        + "<wsn:sensor channel=\"3\" fwtype=\"0\" max=\"100.0\" min=\"0.0\" sampling=\"0\" type=\"1\">"
		        + "</wsn:sensor>"
		        + "</wsn:node>"
		        +
		        // test node 9
		        "<wsn:node logicalID=\"11\" physID=\""
		        + nodeIDs[1][4]
		        + "\" platformID=\"1\" sampling=\""
		        + conf.getSampling().toString()
		        + "\">"
		        + "<wsn:sensor channel=\"2\" fwtype=\"0\" max=\"100.0\" min=\"0.0\" sampling=\"0\" type=\"1\">"
		        + "</wsn:sensor>"
		        + "<wsn:sensor channel=\"3\" fwtype=\"0\" max=\"100.0\" min=\"0.0\" sampling=\"0\" type=\"1\">"
		        + "</wsn:sensor>"
		        + "</wsn:node>"
		        +
		        // test node 10
		        "<wsn:node logicalID=\"12\" physID=\""
		        + nodeIDs[1][5]
		        + "\" platformID=\"1\" sampling=\""
		        + conf.getSampling().toString()
		        + "\">"
		        + "<wsn:sensor channel=\"2\" fwtype=\"0\" max=\"100.0\" min=\"0.0\" sampling=\"0\" type=\"1\">"
		        + "</wsn:sensor>"
		        + "<wsn:sensor channel=\"3\" fwtype=\"0\" max=\"100.0\" min=\"0.0\" sampling=\"0\" type=\"1\">"
		        + "</wsn:sensor>"
		        + "</wsn:node>"
		        +
		        // gold node 3
		        "<wsn:node logicalID=\"13\" physID=\""
		        + nodeIDs[2][0]
		        + "\" platformID=\"1\" sampling=\""
		        + conf.getSampling().toString()
		        + "\">"
		        + "<wsn:sensor channel=\"2\" fwtype=\"0\" max=\"100.0\" min=\"0.0\" sampling=\"0\" type=\"1\">"
		        + "</wsn:sensor>"
		        + "<wsn:sensor channel=\"3\" fwtype=\"0\" max=\"100.0\" min=\"0.0\" sampling=\"0\" type=\"1\">"
		        + "</wsn:sensor>"
		        + "</wsn:node>"
		        +
		        // test node 11
		        "<wsn:node logicalID=\"14\" physID=\""
		        + nodeIDs[2][1]
		        + "\" platformID=\"1\" sampling=\""
		        + conf.getSampling().toString()
		        + "\">"
		        + "<wsn:sensor channel=\"2\" fwtype=\"0\" max=\"100.0\" min=\"0.0\" sampling=\"0\" type=\"1\">"
		        + "</wsn:sensor>"
		        + "<wsn:sensor channel=\"3\" fwtype=\"0\" max=\"100.0\" min=\"0.0\" sampling=\"0\" type=\"1\">"
		        + "</wsn:sensor>"
		        + "</wsn:node>"
		        +
		        // test node 12
		        "<wsn:node logicalID=\"15\" physID=\""
		        + nodeIDs[2][2]
		        + "\" platformID=\"1\" sampling=\""
		        + conf.getSampling().toString()
		        + "\">"
		        + "<wsn:sensor channel=\"2\" fwtype=\"0\" max=\"100.0\" min=\"0.0\" sampling=\"0\" type=\"1\">"
		        + "</wsn:sensor>"
		        + "<wsn:sensor channel=\"3\" fwtype=\"0\" max=\"100.0\" min=\"0.0\" sampling=\"0\" type=\"1\">"
		        + "</wsn:sensor>"
		        + "</wsn:node>"
		        +
		        // test node 13
		        "<wsn:node logicalID=\"16\" physID=\""
		        + nodeIDs[2][3]
		        + "\" platformID=\"1\" sampling=\""
		        + conf.getSampling().toString()
		        + "\">"
		        + "<wsn:sensor channel=\"2\" fwtype=\"0\" max=\"100.0\" min=\"0.0\" sampling=\"0\" type=\"1\">"
		        + "</wsn:sensor>"
		        + "<wsn:sensor channel=\"3\" fwtype=\"0\" max=\"100.0\" min=\"0.0\" sampling=\"0\" type=\"1\">"
		        + "</wsn:sensor>"
		        + "</wsn:node>"
		        +
		        // test node 14
		        "<wsn:node logicalID=\"17\" physID=\""
		        + nodeIDs[2][4]
		        + "\" platformID=\"1\" sampling=\""
		        + conf.getSampling().toString()
		        + "\">"
		        + "<wsn:sensor channel=\"2\" fwtype=\"0\" max=\"100.0\" min=\"0.0\" sampling=\"0\" type=\"1\">"
		        + "</wsn:sensor>"
		        + "<wsn:sensor channel=\"3\" fwtype=\"0\" max=\"100.0\" min=\"0.0\" sampling=\"0\" type=\"1\">"
		        + "</wsn:sensor>"
		        + "</wsn:node>"
		        +
		        // test node 15
		        "<wsn:node logicalID=\"18\" physID=\""
		        + nodeIDs[2][5]
		        + "\" platformID=\"1\" sampling=\""
		        + conf.getSampling().toString()
		        + "\">"
		        + "<wsn:sensor channel=\"2\" fwtype=\"0\" max=\"100.0\" min=\"0.0\" sampling=\"0\" type=\"1\">"
		        + "</wsn:sensor>"
		        + "<wsn:sensor channel=\"3\" fwtype=\"0\" max=\"100.0\" min=\"0.0\" sampling=\"0\" type=\"1\">"
		        + "</wsn:sensor>"
		        + "</wsn:node>"
		        +
		        // gold node 4
		        "<wsn:node logicalID=\"19\" physID=\""
		        + nodeIDs[3][0]
		        + "\" platformID=\"1\" sampling=\""
		        + conf.getSampling().toString()
		        + "\">"
		        + "<wsn:sensor channel=\"2\" fwtype=\"0\" max=\"100.0\" min=\"0.0\" sampling=\"0\" type=\"1\">"
		        + "</wsn:sensor>"
		        + "<wsn:sensor channel=\"3\" fwtype=\"0\" max=\"100.0\" min=\"0.0\" sampling=\"0\" type=\"1\">"
		        + "</wsn:sensor>"
		        + "</wsn:node>"
		        +
		        // test node 16
		        "<wsn:node logicalID=\"20\" physID=\""
		        + nodeIDs[3][1]
		        + "\" platformID=\"1\" sampling=\""
		        + conf.getSampling().toString()
		        + "\">"
		        + "<wsn:sensor channel=\"2\" fwtype=\"0\" max=\"100.0\" min=\"0.0\" sampling=\"0\" type=\"1\">"
		        + "</wsn:sensor>"
		        + "<wsn:sensor channel=\"3\" fwtype=\"0\" max=\"100.0\" min=\"0.0\" sampling=\"0\" type=\"1\">"
		        + "</wsn:sensor>"
		        + "</wsn:node>"
		        +
		        // test node 17
		        "<wsn:node logicalID=\"21\" physID=\""
		        + nodeIDs[3][2]
		        + "\" platformID=\"1\" sampling=\""
		        + conf.getSampling().toString()
		        + "\">"
		        + "<wsn:sensor channel=\"2\" fwtype=\"0\" max=\"100.0\" min=\"0.0\" sampling=\"0\" type=\"1\">"
		        + "</wsn:sensor>"
		        + "<wsn:sensor channel=\"3\" fwtype=\"0\" max=\"100.0\" min=\"0.0\" sampling=\"0\" type=\"1\">"
		        + "</wsn:sensor>"
		        + "</wsn:node>"
		        +
		        // test node 18
		        "<wsn:node logicalID=\"22\" physID=\""
		        + nodeIDs[3][3]
		        + "\" platformID=\"1\" sampling=\""
		        + conf.getSampling().toString()
		        + "\">"
		        + "<wsn:sensor channel=\"2\" fwtype=\"0\" max=\"100.0\" min=\"0.0\" sampling=\"0\" type=\"1\">"
		        + "</wsn:sensor>"
		        + "<wsn:sensor channel=\"3\" fwtype=\"0\" max=\"100.0\" min=\"0.0\" sampling=\"0\" type=\"1\">"
		        + "</wsn:sensor>"
		        + "</wsn:node>"
		        +
		        // test node 19
		        "<wsn:node logicalID=\"23\" physID=\""
		        + nodeIDs[3][4]
		        + "\" platformID=\"1\" sampling=\""
		        + conf.getSampling().toString()
		        + "\">"
		        + "<wsn:sensor channel=\"3\" fwtype=\"0\" max=\"100.0\" min=\"0.0\" sampling=\"0\" type=\"1\">"
		        + "</wsn:sensor>"
		        + "</wsn:node>"
		        +
		        // test node 20
		        "<wsn:node logicalID=\"24\" physID=\""
		        + nodeIDs[3][5]
		        + "\" platformID=\"1\" sampling=\""
		        + conf.getSampling().toString()
		        + "\">"
		        + "<wsn:sensor channel=\"2\" fwtype=\"0\" max=\"100.0\" min=\"0.0\" sampling=\"0\" type=\"1\">"
		        + "</wsn:sensor>"
		        + "<wsn:sensor channel=\"3\" fwtype=\"0\" max=\"100.0\" min=\"0.0\" sampling=\"0\" type=\"1\">"
		        + "</wsn:sensor>"
		        + "</wsn:node>"
		        +
		        // gold node 5
		        "<wsn:node logicalID=\"25\" physID=\""
		        + nodeIDs[4][0]
		        + "\" platformID=\"1\" sampling=\""
		        + conf.getSampling().toString()
		        + "\">"
		        + "<wsn:sensor channel=\"2\" fwtype=\"0\" max=\"100.0\" min=\"0.0\" sampling=\"0\" type=\"1\">"
		        + "</wsn:sensor>"
		        + "<wsn:sensor channel=\"3\" fwtype=\"0\" max=\"100.0\" min=\"0.0\" sampling=\"0\" type=\"1\">"
		        + "</wsn:sensor>"
		        + "</wsn:node>"
		        +
		        // test node 21
		        "<wsn:node logicalID=\"26\" physID=\""
		        + nodeIDs[4][1]
		        + "\" platformID=\"1\" sampling=\""
		        + conf.getSampling().toString()
		        + "\">"
		        + "<wsn:sensor channel=\"2\" fwtype=\"0\" max=\"100.0\" min=\"0.0\" sampling=\"0\" type=\"1\">"
		        + "</wsn:sensor>"
		        + "<wsn:sensor channel=\"3\" fwtype=\"0\" max=\"100.0\" min=\"0.0\" sampling=\"0\" type=\"1\">"
		        + "</wsn:sensor>"
		        + "</wsn:node>"
		        +
		        // test node 22
		        "<wsn:node logicalID=\"27\" physID=\""
		        + nodeIDs[4][2]
		        + "\" platformID=\"1\" sampling=\""
		        + conf.getSampling().toString()
		        + "\">"
		        + "<wsn:sensor channel=\"2\" fwtype=\"0\" max=\"100.0\" min=\"0.0\" sampling=\"0\" type=\"1\">"
		        + "</wsn:sensor>"
		        + "<wsn:sensor channel=\"3\" fwtype=\"0\" max=\"100.0\" min=\"0.0\" sampling=\"0\" type=\"1\">"
		        + "</wsn:sensor>"
		        + "</wsn:node>"
		        +
		        // test node 23
		        "<wsn:node logicalID=\"28\" physID=\""
		        + nodeIDs[4][3]
		        + "\" platformID=\"1\" sampling=\""
		        + conf.getSampling().toString()
		        + "\">"
		        + "<wsn:sensor channel=\"2\" fwtype=\"0\" max=\"100.0\" min=\"0.0\" sampling=\"0\" type=\"1\">"
		        + "</wsn:sensor>"
		        + "<wsn:sensor channel=\"3\" fwtype=\"0\" max=\"100.0\" min=\"0.0\" sampling=\"0\" type=\"1\">"
		        + "</wsn:sensor>"
		        + "</wsn:node>"
		        +
		        // test node 24
		        "<wsn:node logicalID=\"29\" physID=\""
		        + nodeIDs[4][4]
		        + "\" platformID=\"1\" sampling=\""
		        + conf.getSampling().toString()
		        + "\">"
		        + "<wsn:sensor channel=\"2\" fwtype=\"0\" max=\"100.0\" min=\"0.0\" sampling=\"0\" type=\"1\">"
		        + "</wsn:sensor>"
		        + "<wsn:sensor channel=\"3\" fwtype=\"0\" max=\"100.0\" min=\"0.0\" sampling=\"0\" type=\"1\">"
		        + "</wsn:sensor>"
		        + "</wsn:node>"
		        +
		        // test node 25
		        "<wsn:node logicalID=\"30\" physID=\""
		        + nodeIDs[4][5]
		        + "\" platformID=\"1\" sampling=\""
		        + conf.getSampling().toString()
		        + "\">"
		        + "<wsn:sensor channel=\"2\" fwtype=\"0\" max=\"100.0\" min=\"0.0\" sampling=\"0\" type=\"1\">"
		        + "</wsn:sensor>"
		        + "<wsn:sensor channel=\"3\" fwtype=\"0\" max=\"100.0\" min=\"0.0\" sampling=\"0\" type=\"1\">"
		        + "</wsn:sensor>"
		        + "</wsn:node>"
		        + "</wsn:nodes>"
		        + "<wsn:gateways>"
		        + "<wsn:gateway logicalID=\"32768\" physID=\"43690\" port=\""
		        + conf.getPort()
		        + "\"/>"
		        + ""
		        + "</wsn:gateways>" + "</wsn:network>" + "</wsn:wsn>";

		try {
			DM.configurePlugin("wsn", new Mapping(), config);
		} catch (ConfigPluginException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return;
		}
		Collection<String> str = DM.getPluginNames();
		System.out.print("DM with plugins" + str + "stated\n");
		DM.setTransport(transport);
		transport.enable();

	}

	private String convertHexToDec(String Hex) {
		Long temp = Long.decode("0x" + Hex);
		Hex = Long.toString(temp);

		return Hex;

	}

	private void readFromCFG() {

		/*
		 * ObjectInputStream in = null; try { in = new ObjectInputStream(new
		 * BufferedInputStream( new FileInputStream("Config.cfg")));
		 * conf=(Config) in.readObject(); } catch ( IOException ex ) { //
		 * ex.printStackTrace();
		 * System.out.print("Cant read config, will load default config\n");
		 * MainWindow
		 * .infoArea.append("Cant read config, will load default config");
		 * conf=new Config(); } catch ( Exception ex ) { //
		 * ex.printStackTrace();
		 * System.out.print("Cant read config, will load default config\n");
		 * MainWindow
		 * .infoArea.append("Cant read config, will load default config");
		 * conf=new Config(); }finally { if ( in != null ) try { in.close(); }
		 * catch ( IOException ex ) { ex.printStackTrace(); } }
		 */
		FileInputStream in = null;
		try {
			in = new FileInputStream("Config.xml");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.print("Cant read config, will load default config\n");
			MainWindow.infoArea.append("Cant read config, will load default config");
			conf = new Config();
		}
		XMLDecoder xmlDecoder = new XMLDecoder(in);
		conf = (Config) xmlDecoder.readObject();
		xmlDecoder.close();
	}

	public void stopDM() throws ConfigPluginException {
		DM.configurePlugin(
		        "wsn",
		        new Mapping(),
		        "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
		                + "<wsn:wsn xmlns:wsn=\"http://www.synapsense.com/devicemapper/wsn\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.synapsense.com/devicemapper/wsn wsn.xsd \">"
		                + "<wsn:network name=\"network1\" networkID=\"1\" panID=\"" + conf.getPanID()
		                + "\" sampling=\"" + conf.sampling.toString() + "\" softbased=\"true\" version=\"1.1\">"
		                + "<wsn:nodes>" + "<wsn:node logicalID=\"1\" physID=\"0\" platformID=\"1\" sampling=\"30\">"
		                + "<wsn:sensor channel=\"2\" fwtype=\"0\" max=\"100.0\" min=\"0.0\" sampling=\"0\" type=\"1\">"
		                + "</wsn:sensor>"
		                + "<wsn:sensor channel=\"3\" fwtype=\"0\" max=\"100.0\" min=\"0.0\" sampling=\"0\" type=\"1\">"
		                + "</wsn:sensor>" + "</wsn:node>" + "</wsn:nodes>" + "<wsn:gateways>"
		                + "<wsn:gateway logicalID=\"32768\" physID=\"43690\" port=\"" + conf.getPort() + "\"/>" + ""
		                + "</wsn:gateways>" + "</wsn:network>" + "</wsn:wsn>");

	}

}