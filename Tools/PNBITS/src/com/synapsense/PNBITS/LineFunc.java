package com.synapsense.PNBITS;

public class LineFunc {

	Double k;
	Double b;
	Long time1;
	Long time2;
	Double value1;
	Double value2;

	public LineFunc() {
		time1 = 0L;
		time2 = 0L;
		value1 = new Double(0);
		value2 = new Double(0);
		k = 0.0;
		b = 0.0;
	}

	public void setParameters(Long time, Double value) {
		if (this.time1 == 0 && this.value1 == 0) {
			this.time1 = time;
			this.value1 = value;
			return;
		} else if (this.time2 == 0 && this.value2 == 0) {
			this.time2 = time;
			this.value2 = value;
			return;
		} else {
			System.out.print("do nothing");
			return;
		}

	}

	public Double calculateValue(Long time) {
		if (time1 != 0 && time2 != 0) {
			// Double tempTime=new Double(time2-time1);
			k = new Double((value2 - value1) / new Double((time2 - time1)));
			b = new Double(value1 - new Double(time1 * k));
			b = new Double(value2 - new Double(time2 * k));
			return k * time + b;
		} else {
			System.out.print("Warning approximation function wasn't built\n");
			MainWindow.infoArea.setText("Warning approximation function wasn't built\n "
			        + "increase join time and restart the test");

			return -10000.0;
		}
	}

}
