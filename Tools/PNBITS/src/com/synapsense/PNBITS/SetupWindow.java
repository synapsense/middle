package com.synapsense.PNBITS;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;

public class SetupWindow {
	JDialog jFrame;
	JButton setButton = new JButton("set");
	JLabel jLabelPort = new JLabel("Port");
	JLabel jLabelPan = new JLabel("PANID");
	JLabel jLabelRange = new JLabel("Range (%)");
	JLabel jLabelTestTime = new JLabel("Test Time");
	JLabel jLabelJoinTime = new JLabel("Join Time");
	JLabel jLabelSampling = new JLabel("Sampling sec");

	Object[][] data = new Object[5][1];
	String[] headers = { "Gold nodes MAC IDs" };
	JTable jTableGoldNodes = new JTable(data, headers);
	JScrollPane jScrollPane = null;

	public static JCheckBox isApproximationOn = new JCheckBox("Approximation");

	JTextField jTextFieldPort = new JTextField(12);
	JTextField jTextFieldPan = new JTextField(12);
	JTextField jTextFieldRange = new JTextField(12);
	JTextField jTextFieldTestTime = new JTextField(10);
	JTextField jTextFieldJoinTime = new JTextField(10);
	JTextField jTextFieldSampling = new JTextField(5);

	Config conf;
	MainWindow mainWindow;

	public SetupWindow(MainWindow mainWindow) {
		conf = new Config();
		readFromCFG();
		this.mainWindow = mainWindow;
		jFrame = new JDialog(mainWindow.jFrame, "Setup");

		jScrollPane = new JScrollPane(jTableGoldNodes);
		jScrollPane.setPreferredSize(new Dimension(180, 100));

		jFrame.setSize(210, 380);
		jFrame.setResizable(false);
		jFrame.getContentPane().setLayout(new FlowLayout());
		jFrame.getContentPane().add(jLabelPort);
		jFrame.getContentPane().add(jTextFieldPort);
		jFrame.getContentPane().add(jLabelPan);
		jFrame.getContentPane().add(jTextFieldPan);
		jFrame.getContentPane().add(jLabelRange);
		jFrame.getContentPane().add(jTextFieldRange);
		jFrame.getContentPane().add(jLabelTestTime);
		jFrame.getContentPane().add(jTextFieldTestTime);
		jFrame.getContentPane().add(jLabelJoinTime);
		jFrame.getContentPane().add(jTextFieldJoinTime);
		jFrame.getContentPane().add(jLabelSampling);
		jFrame.getContentPane().add(jTextFieldSampling);
		jFrame.getContentPane().add(isApproximationOn);
		jFrame.getContentPane().add(jScrollPane);

		jFrame.getContentPane().add(setButton);
		jFrame.setVisible(true);
		jTextFieldPort.setEditable(true);
		jTextFieldPan.setEditable(true);
		jTextFieldRange.setEditable(true);
		jTextFieldTestTime.setEditable(true);
		jTextFieldJoinTime.setEditable(true);

		jTextFieldPort.setText(conf.getPort().toString());
		jTextFieldPan.setText(conf.getPanID().toString());
		jTextFieldRange.setText(conf.getRange().toString());
		jTextFieldTestTime.setText(new Long(conf.getTestTime() / 1000).toString());
		jTextFieldJoinTime.setText(new Long(conf.getJoinTime() / 1000).toString());
		jTextFieldSampling.setText(conf.getSampling().toString());
		isApproximationOn.setSelected(conf.getApproximation());

		for (int i = 0; i < 5; i++) {
			jTableGoldNodes.setValueAt(conf.getGoldNodes()[i], i, 0);
		}

		this.initActionListeners();
	}

	void initActionListeners() {
		setButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				conf.setPort(jTextFieldPort.getText());
				conf.setApproximation(isApproximationOn.isSelected());

				try {
					conf.setPanID(jTextFieldPan.getText());
					Long temp = Long.decode("0x" + conf.getPanID());
				} catch (java.lang.NumberFormatException e) {
					System.out.print("Wrong PanID was set, PanID will be reset to defaultvalue");
					MainWindow.infoArea.append("Wrong PanID was set, PanID will be reset to defaultvalue");
					conf.setPanID("AAAA");
				}

				try {
					conf.setRange(new Double(jTextFieldRange.getText()));
					if (conf.getRange() < 0 || conf.getRange() > 1) {
						System.out.print("Wrong range was set, range will be reset to defaultvalue\n");
						MainWindow.infoArea.append("Wrong range was set, range will be reset to defaultvalue");
						conf.setRange(0.05);
					}
				} catch (java.lang.NumberFormatException e) {
					System.out.print("Wrong range was set, range will be reset to defaultvalue\n");
					MainWindow.infoArea.append("Wrong range was set, range will be reset to defaultvalue");
					conf.setRange(0.05);
				}

				try {
					conf.setTestTime(new Long(new Long(jTextFieldTestTime.getText()) * 1000));
					if (conf.getTestTime() <= 0) {
						System.out.print("Wrong test time was set, time will be reset to defaultvalue\n");
						MainWindow.infoArea.append("Wrong test time was set, time will be reset to defaultvalue");
						conf.setTestTime(600000L);
					}
				} catch (java.lang.NumberFormatException e) {
					System.out.print("Wrong test time was set, time will be reset to defaultvalue\n");
					MainWindow.infoArea.append("Wrong test time was set, time will be reset to defaultvalue");
					conf.setTestTime(600000L);
				}

				try {
					conf.setJoinTime(new Long(new Long(jTextFieldJoinTime.getText()) * 1000));
					if (conf.getJoinTime() <= 0) {
						System.out.print("Wrong join time was set, time will be reset to defaultvalue\n");
						MainWindow.infoArea.append("Wrong join time was set, time will be reset to defaultvalue");
						conf.setJoinTime(600000L);
					}
				} catch (java.lang.NumberFormatException e) {
					System.out.print("Wrong join time was set, time will be reset to defaultvalue\n");
					MainWindow.infoArea.append("Wrong join time was set, time will be reset to defaultvalue");
					conf.setJoinTime(600000L);
				}

				try {
					conf.setSampling(new Integer(jTextFieldSampling.getText()));
					if (conf.getSampling() <= 0) {
						System.out.print("Wrong sampling time was set, time will be reset to defaultvalue\n");
						MainWindow.infoArea.append("Wrong sampling time was set, time will be reset to defaultvalue");
						conf.setSampling(30);
					}
					if (isApproximationOn.isSelected() && conf.getJoinTime() < conf.getSampling() * 3000) {
						conf.setJoinTime(conf.getSampling() * 4000L);
						MainWindow.infoArea
						        .append("Wrong joinTime was set, due to approximation mode joinTime should be at least 3 times more than sampling,"
						                + " join time was reset to " + conf.getJoinTime() / 1000);
					}
				} catch (java.lang.NumberFormatException e) {
					System.out.print("Wrong sampling time was set, time will be reset to defaultvalue\n");
					MainWindow.infoArea.append("Wrong sampling time was set, time will be reset to defaultvalue");
					conf.setSampling(30);
				}

				String[] tempStr = new String[5];
				for (int i = 0; i < 5; i++) {
					if ((jTableGoldNodes.getValueAt(i, 0) == null)
					        || (jTableGoldNodes.getValueAt(i, 0).toString().isEmpty())) {
						tempStr[i] = null;
					} else {
						try {
							Long.parseLong(jTableGoldNodes.getValueAt(i, 0).toString(), 16);
							tempStr[i] = jTableGoldNodes.getValueAt(i, 0).toString();
						} catch (NumberFormatException e) {
							MainWindow.infoArea
							        .append("Wrong MAC ID was set, reset MAC ID in " + (i + 1) + " string\n");
							tempStr[i] = null;
						}
					}
				}
				conf.setGoldNodes(tempStr);

				writeCFG(conf);
				jTextFieldPort.setText(conf.getPort());
				jTextFieldPan.setText(conf.getPanID());
				jTextFieldRange.setText(conf.getRange().toString());
				jTextFieldTestTime.setText(new Long(conf.getTestTime() / 1000).toString());
				jTextFieldJoinTime.setText(new Long(conf.getJoinTime() / 1000).toString());
				jTextFieldSampling.setText(conf.getSampling().toString());
				mainWindow.readFromCFG();
				jFrame.setVisible(false);
			}

		}

		);

	}

	public static void writeCFG(Config conf) {
		/*
		 * ObjectOutputStream out = null; try { out = new ObjectOutputStream(new
		 * BufferedOutputStream( new FileOutputStream("Config.cfg")));
		 * out.writeObject(conf); } catch ( IOException ex ) {
		 * ex.printStackTrace(); System.out.print("cant write config file \n");
		 * MainWindow.infoArea.append("cant write config file \n"); }finally {
		 * if ( out != null ) try { out.close(); } catch ( IOException ex ) {
		 * ex.printStackTrace(); } }
		 */

		FileOutputStream out = null;
		try {
			out = new FileOutputStream("Config.xml");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.print("cant write config file \n");
			MainWindow.infoArea.append("cant write config file \n");
		}
		XMLEncoder xmlEncoder = new XMLEncoder(out);
		xmlEncoder.writeObject(conf);
		xmlEncoder.flush();
		xmlEncoder.close();

	}

	private void readFromCFG() {

		/*
		 * ObjectInputStream in = null; try { in = new ObjectInputStream(new
		 * BufferedInputStream( new FileInputStream("Config.cfg")));
		 * conf=(Config) in.readObject(); } catch ( IOException ex ) { //
		 * ex.printStackTrace();
		 * System.out.print("Cant read config, will load default config\n");
		 * MainWindow
		 * .infoArea.append("Cant read config, will load default config");
		 * conf=new Config(); } catch ( Exception ex ) { //
		 * ex.printStackTrace();
		 * System.out.print("Cant read config, will load default config\n");
		 * MainWindow
		 * .infoArea.append("Cant read config, will load default config");
		 * conf=new Config(); }finally { if ( in != null ) try { in.close(); }
		 * catch ( IOException ex ) { ex.printStackTrace(); } }
		 */

		FileInputStream in = null;
		try {
			in = new FileInputStream("Config.xml");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.print("Cant read config, will load default config\n");
			MainWindow.infoArea.append("Cant read config, will load default config");
			conf = new Config();
		}
		XMLDecoder xmlDecoder = new XMLDecoder(in);
		conf = (Config) xmlDecoder.readObject();
		xmlDecoder.close();
	}

}
