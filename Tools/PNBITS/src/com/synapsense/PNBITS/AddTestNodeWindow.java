package com.synapsense.PNBITS;

import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AddTestNodeWindow {

	JFrame jFrame = new JFrame("Add Test Node");
	JButton addButton = new JButton("add");
	JButton closeButton = new JButton("close");
	// JTextField jTextField=new JTextField("81256525212942564");
	JTextField jTextField = new JTextField(20);

	JTable[] workingTable = new JTable[5];

	int i = 0;
	int j = 0;
	int k = 0;
	int n = 0;
	int m = 0;
	int p = 0;
	int testNodesMaxAmmount;

	String[][] nodeIDs;

	public AddTestNodeWindow(String[][] nodeIDs, final int testNodesMaxAmmount, JTable[] workingTable) {
		jFrame.getContentPane().setLayout(new FlowLayout());
		jFrame.setSize(250, 100);
		jFrame.getContentPane().add(jTextField);
		jFrame.getContentPane().add(addButton);
		jFrame.setVisible(true);

		jTextField.setEditable(true);

		this.testNodesMaxAmmount = testNodesMaxAmmount;
		this.workingTable = workingTable;

		this.nodeIDs = nodeIDs;
		this.initActionListeners();
	}

	void initActionListeners() {
		addButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				if (i < testNodesMaxAmmount) {
					if (i < 5) {
						j++;
						nodeIDs[0][j] = jTextField.getText();
						workingTable[0].setValueAt(nodeIDs[0][j], 0, j);
					}
					if (i >= 5 && i < 10) {
						k++;
						nodeIDs[1][k] = jTextField.getText();
						workingTable[1].setValueAt(nodeIDs[1][k], 0, k);
					}
					if (i >= 10 && i < 15) {
						n++;
						nodeIDs[2][n] = jTextField.getText();
						workingTable[2].setValueAt(nodeIDs[2][n], 0, n);
					}
					if (i >= 15 && i < 20) {
						m++;
						nodeIDs[3][m] = jTextField.getText();
						workingTable[3].setValueAt(nodeIDs[3][m], 0, m);
					}
					if (i >= 20 && i < 25) {
						p++;
						nodeIDs[4][p] = jTextField.getText();
						workingTable[4].setValueAt(nodeIDs[4][p], 0, p);
					}
					i++;
					jTextField.setText(null);
				}

			}

		});

		closeButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

			}

		});

	}

}
