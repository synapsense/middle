package com.synapsense.PNBITS;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import org.apache.log4j.Logger;

import com.synapsense.devicemapper.IdentityImpl;
import com.synapsense.devicemapper.MappingItem;
import com.synapsense.devicemapper.MappingList;
import com.synapsense.devicemapper.typemapping.Mapping.Type.Property;
import com.synapsense.plugin.manager.DeviceManagerCore;

public final class ReportWindow {
	private final static Logger logger = Logger.getLogger(ReportWindow.class);
	private final static ReportWindow inst = new ReportWindow();

	JFrame jFrame = new JFrame("report Window");
	Object[][] data = new Object[25][4];
	String[] colHeaders = { "MacID", "Received packets", "Failures", "Result" };
	JTable reportTable;
	JScrollPane jScrollPane;
	JButton saveButton;

	public ReportWindow() {

		saveButton = new JButton("save");
		reportTable = new JTable(data, colHeaders);
		jScrollPane = new JScrollPane(reportTable);
		jScrollPane.setPreferredSize(new Dimension(600, 400));
		reportTable.setPreferredSize(new Dimension(600, 400));
		reportTable.setEnabled(false);
		jFrame.getContentPane().setLayout(new FlowLayout());
		jFrame.setSize(700, 480);
		jFrame.getContentPane().add(jScrollPane);
		jFrame.getContentPane().add(saveButton);
		jFrame.setVisible(true);
		initReportWindow();
		initActionListeners();
	}

	void initReportWindow() {

		int j = 0;
		for (int i = 0; i < 30; i++) {
			// Long temp=(MainWindow.failC[i].nodeID);
			// Integer temp1=MainWindow.failC[i].messageNum;

			if (MainWindow.failC[i].nodeID > 0/*
											 * &&MainWindow.failC[i].messageNum>0
											 */) {
				reportTable.setValueAt(Long.toHexString(MainWindow.failC[i].nodeID), j, 0);
				reportTable.setValueAt(MainWindow.failC[i].messageNum, j, 1);
				reportTable.setValueAt(MainWindow.failC[i].failNum, j, 2);
				if (MainWindow.failC[i].failNum > 0
				        || MainWindow.failC[i].messageNum == 0
				        || (MainWindow.failC[i].lastMessageTime + 2 * MainWindow.conf.getSampling() < System
				                .currentTimeMillis())) {
					reportTable.setValueAt("fail", j, 3);
					logger.warn("Node 0x" + Long.toHexString(MainWindow.failC[i].nodeID)
					        + " failed. Perhaps either sensed data is out of range or node didn't send data.");
					MappingList map = new MappingList();
					Property p = new Property();
					p.setName("physID");
					map.addMapAtBeginning(new MappingItem(new IdentityImpl(p, MainWindow.failC[i].nodeID), ""));
					// p.setName(MainWindow.failC[i].nodeID.toString());
					// map.addMapAtBeginning(new MappingItem(new IdentityImpl(p,
					// Long.parseLong("142138210602975240")), ""));
					try {
						DeviceManagerCore.getInst().setPropertyValue(map, "identify", 0);
					} catch (Exception e) {
						e.printStackTrace();
					}

				} else {
					reportTable.setValueAt("ok", j, 3);
				}
				j++;
			}
		}
		if (reportTable.getValueAt(0, 0) == null) {
			reportTable.setValueAt("no nodes have joined in time", 0, 0);
			MainWindow.infoArea.setText("no nodes have joined in time");
		}

	}

	void initActionListeners() {

		saveButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.out.print("savebutton pressed");

				JFileChooser fileChooser = new JFileChooser();
				fileChooser.addChoosableFileFilter(new TxtFilter());
				fileChooser.showOpenDialog(saveButton);
				File selectedFile = fileChooser.getSelectedFile();

				StringBuilder strText = new StringBuilder();
				strText.append("PhysID           messagecount    failescount    result \n");
				for (int i = 0, j = 0; reportTable.getValueAt(i, j) != null; j++) {
					strText.append(reportTable.getValueAt(i, j).toString() + "          ");

					if (j == 3) {
						i++;
						j = -1;
						strText.append("\n");
					}
				}
				try {
					String str = new String(strText);
					FileWriter fw = new FileWriter(selectedFile);
					fw.write(str);
					fw.flush();
					fw.close();
					MainWindow.infoArea.setText("report saved");
					System.out.print("report saved");

				} catch (IOException ex) {
					MainWindow.infoArea.setText("unable to save report" + ex);
					System.out.print("unable to save report" + ex);
				}

			}
		});

	}

	public synchronized static ReportWindow getInst() {
		return inst;
	}

	public void showReport() {
		jFrame.setVisible(true);
	}

}

class TxtFilter extends javax.swing.filechooser.FileFilter {
	public String getDescription() {
		return "*.doc";
	}

	public boolean accept(File f) {
		String filename = f.getName();
		return f.isDirectory() || filename.endsWith("doc");
	}
}