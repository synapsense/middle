package com.synapsense.PNBITS;

import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AddGoldNodeWindow {

	JFrame jFrame = new JFrame("add Gold Node");
	JButton addButton = new JButton("add");
	JButton closeButton = new JButton("close");
	// JTextField jTextField=new JTextField("109410619953774757");
	JTextField jTextField = new JTextField(20);

	JTable[] workingTable = new JTable[5];

	int i = 0;
	int goldNodesMaxAmmount;

	// String [] goldNodePhysIDs;;
	String[][] nodeIDs;

	public AddGoldNodeWindow(String[][] nodeIDs, final int goldNodesMaxAmmount, JTable[] workingTable) {
		jFrame.getContentPane().setLayout(new FlowLayout());
		jFrame.setSize(250, 100);
		jFrame.getContentPane().add(jTextField);
		jFrame.getContentPane().add(addButton);
		// jFrame.getContentPane().add(closeButton);
		jFrame.setVisible(true);
		jTextField.setEditable(true);

		this.goldNodesMaxAmmount = goldNodesMaxAmmount;
		// goldNodePhysIDs=new String[goldNodesMaxAmmount];
		this.nodeIDs = nodeIDs;
		this.workingTable = workingTable;
		this.initActionListeners();
	}

	void initActionListeners() {
		addButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				if (i < goldNodesMaxAmmount) {
					nodeIDs[i][0] = jTextField.getText();
					workingTable[i].setValueAt(nodeIDs[i][0], 0, 0);
				}
				jTextField.setText(null);
				i++;

			}

		}

		);

		closeButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

			}

		});

	}

}
