package com.synapsense.PNBITS;

import java.io.Serializable;

public class Config implements Serializable {

	/**
	 * 
	 */
	// private static final long serialVersionUID = 1L;
	/**
	 * 
	 */
	String port;
	String panID;
	Double range;
	Long testTime;
	Long joinTime;
	Boolean approximation;
	Integer sampling;

	String[] goldNodes = new String[5];

	public Config() {
		super();
		this.port = new String("COM3");
		this.panID = new String("AAAA");
		this.range = new Double(0.05);
		this.testTime = new Long(600000L);
		this.joinTime = new Long(600000L);
		this.approximation = false;
		this.sampling = 30;
		for (int i = 0; i < 5; i++) {
			goldNodes[i] = "0";
		}
	}

	public Config(String port, String panID, Double range, Long testTime, Long joinTime) {
		super();
		this.port = port;
		this.panID = panID;
		this.range = range;
		this.testTime = testTime;
		this.joinTime = joinTime;
		this.approximation = false;
		this.sampling = 30;
		for (int i = 0; i < 5; i++) {
			goldNodes[i] = "0";
		}
	}

	public void setPort(String port) {
		this.port = port;
	}

	public void setPanID(String panID) {
		this.panID = panID;
	}

	public void setRange(Double range) {
		this.range = range;
	}

	public void setTestTime(Long testTime) {
		this.testTime = testTime;
	}

	public void setJoinTime(Long joinTime) {
		this.joinTime = joinTime;
	}

	public void setApproximation(Boolean approximation) {
		this.approximation = approximation;
	}

	public void setGoldNodes(String[] goldNodes) {
		this.goldNodes = goldNodes;
	}

	public void setSampling(Integer sampling) {
		this.sampling = sampling;
	}

	public String getPort() {
		return this.port;
	}

	public String getPanID() {
		return this.panID;
	}

	public Double getRange() {
		return this.range;
	}

	public Long getTestTime() {
		return this.testTime;
	}

	public Long getJoinTime() {
		return this.joinTime;
	}

	public Boolean getApproximation() {
		return this.approximation;
	}

	public String[] getGoldNodes() {
		return this.goldNodes;
	}

	public Integer getSampling() {
		return this.sampling;
	}
}
