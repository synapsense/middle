package com.synapsense.escomparator;

public class Launcher {
	public static void main(String[] args) throws Exception {
		if (args.length < 7) {
			usage();
			System.exit(1);
		}
		int level = Integer.parseInt(args[0]);
		ESConnection conn1 = new ESConnection(args[1], args[2], args[3]);
		conn1.start();

		ESConnection conn2 = new ESConnection(args[4], args[5], args[6]);
		conn2.start();

		new Processor(level, conn1, conn2).process();
	}

	private static void usage() {
		System.out
		        .println("Usage: \n\t"
		                + "java -jar escomparator.jar <level> <SRV1 IP> <SRV1 login> <SRV1 pass> "
		                + "<SRV2 IP> <SRV2 login> <SRV2 pass>\n\t"
		                + "level:\t1-check object types only (fastest mode)\n\t\t2-check property descriptors for each OT\n\t\t"
		                + "3-check object instances\n\t\t4-check property values (slowest mode)");
	}
}
