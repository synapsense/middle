package com.synapsense.escomparator;

public final class Task {
	public enum TaskIDs {
	GET_OTS, GET_PDS_FOR_OT, GET_OBJS_FOR_OT, GET_PROPS_FOR_OBJ
	}

	private final TaskIDs taskId;
	private final Object args;
	private Object result;
	private Object lock = new Object();

	public Task(TaskIDs taskId, Object args) {
		this.taskId = taskId;
		this.args = args;
	}

	public TaskIDs getTaskId() {
		return taskId;
	}

	public Object getArgs() {
		return args;
	}

	public Object getResult() {
		synchronized (lock) {
			try {
				lock.wait();
			} catch (InterruptedException e) {
			}
		}
		return result;
	}

	public void setResult(Object result) {
		this.result = result;
		synchronized (lock) {
			lock.notify();
		}
	}
}