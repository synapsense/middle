package com.synapsense.escomparator;

import java.util.Arrays;
import java.util.Comparator;

import com.synapsense.dto.PropertyDescr;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.dto.ValueTO;
import com.synapsense.util.algo.SetsIntersector;
import com.synapsense.util.algo.SetsIntersector.AllValues;

public class Processor {
	private final ESConnection conn1;
	private final ESConnection conn2;
	private final int level;

	public Processor(int level, ESConnection conn1, ESConnection conn2) {
		this.conn1 = conn1;
		this.conn2 = conn2;
		this.level = level;
	}

	public void process() {
		System.out.println("Comparing object types...");

		AllValues<String> otRes = new SetsIntersector<String>().doAllJob(Arrays.asList(conn1.getObjectTypes()),
		        Arrays.asList(conn2.getObjectTypes()), new Comparator<String>() {
			        @Override
			        public int compare(String o1, String o2) {
				        return o1.compareTo(o2);
			        }
		        });
		if (!otRes.getDifference().getFMinusS().isEmpty()) {
			System.out.println("These object types do not exist in the second ES:");
			for (String s : otRes.getDifference().getFMinusS()) {
				System.out.println("\t" + s);
			}
			System.exit(1);
		}

		if (!otRes.getDifference().getSMinusF().isEmpty()) {
			System.out.println("These object types do not exist in the first ES:");
			for (String s : otRes.getDifference().getSMinusF()) {
				System.out.println("\t" + s);
			}
			System.exit(1);
		}
		System.out.println("Comparison complete.");
		if (level > 1) {
			for (String s : otRes.getIntersection()) {
				checkProperties(s);
			}
		}
	}

	private void checkProperties(String otName) {
		System.out.println("Comparing property descriptors of " + otName + " object type...");

		AllValues<PropertyDescr> pds = new SetsIntersector<PropertyDescr>().doAllJob(
		        conn1.getPropertyDescriptors(otName), conn2.getPropertyDescriptors(otName),
		        new Comparator<PropertyDescr>() {
			        @Override
			        public int compare(PropertyDescr o1, PropertyDescr o2) {
				        if (!o1.getName().equals(o2.getName()))
					        return o1.getName().compareTo(o2.getName());
				        return o1.getTypeName().compareTo(o2.getTypeName());
			        }
		        });

		if (!pds.getDifference().getFMinusS().isEmpty()) {
			System.out.println("These property descriptors do not exist in the second ES or have a different type: ");
			for (PropertyDescr pd : pds.getDifference().getFMinusS()) {
				System.out.println("\t" + pd.getName() + "->" + pd.getTypeName());
			}
			System.exit(1);
		}

		if (!pds.getDifference().getSMinusF().isEmpty()) {
			System.out.println("These property descriptors do not exist in the first ES or have a different type: ");
			for (PropertyDescr pd : pds.getDifference().getSMinusF()) {
				System.out.println("\t" + pd.getName() + "->" + pd.getTypeName());
			}
			System.exit(1);
		}
		System.out.println("Comparison complete.");

		if (level > 2)
			checkObjects(otName);
	}

	private void checkObjects(String ot) {
		System.out.println("Comparing object instances of " + ot + " object type...");

		AllValues<TO<?>> objs = new SetsIntersector<TO<?>>().doAllJob(conn1.getObjects(ot), conn2.getObjects(ot),
		        new Comparator<TO<?>>() {
			        @Override
			        public int compare(TO<?> o1, TO<?> o2) {
				        return TOFactory.getInstance().saveTO(o1).compareTo(TOFactory.getInstance().saveTO(o2));
			        }
		        });

		if (!objs.getDifference().getFMinusS().isEmpty()) {
			System.out.println("These objects do not exist in the second ES:");
			for (TO<?> to : objs.getDifference().getFMinusS()) {
				System.out.println("\t" + TOFactory.getInstance().saveTO(to));
			}
			System.exit(1);
		}

		if (!objs.getDifference().getSMinusF().isEmpty()) {
			System.out.println("These objects do not exist in the first ES:");
			for (TO<?> to : objs.getDifference().getSMinusF()) {
				System.out.println("\t" + TOFactory.getInstance().saveTO(to));
			}
			System.exit(1);
		}

		if (level > 3) {
			for (TO<?> to : objs.getIntersection()) {
				checkProperties(to);
			}
		}
		System.out.println("Comparison complete.");
	}

	private void checkProperties(final TO<?> to) {
		AllValues<ValueTO> props = new SetsIntersector<ValueTO>().doAllJob(conn1.getAllProperties(to),
		        conn2.getAllProperties(to), new Comparator<ValueTO>() {
			        @Override
			        public int compare(ValueTO o1, ValueTO o2) {
				        if (!o1.getPropertyName().equals(o2.getPropertyName())) {
					        return o1.getPropertyName().compareTo(o2.getPropertyName());
				        }
				        if (o1.getValue() == null) {
					        if (o2.getValue() == null)
						        return 0;
					        return 1;
				        }
				        return -1;
			        }
		        });

		if (!props.getDifference().getFMinusS().isEmpty()) {
			System.out.println("These properties of the object " + TOFactory.getInstance().saveTO(to)
			        + " differ in the second ES:");
			for (ValueTO v : props.getDifference().getFMinusS()) {
				System.out.println("\t" + v.getPropertyName() + "=" + v.getValue());
			}
			System.exit(1);
		}

		if (!props.getDifference().getSMinusF().isEmpty()) {
			System.out.println("These properties of the object " + TOFactory.getInstance().saveTO(to)
			        + " differ in the first ES:");
			for (ValueTO v : props.getDifference().getSMinusF()) {
				System.out.println("\t" + v.getPropertyName() + "=" + v.getValue());
			}
			System.exit(1);
		}
	}

}
