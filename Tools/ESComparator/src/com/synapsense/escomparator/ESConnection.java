package com.synapsense.escomparator;

import java.util.Collection;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.LinkedBlockingQueue;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.synapsense.dto.PropertyDescr;
import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;
import com.synapsense.escomparator.Task.TaskIDs;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.service.Environment;
import com.synapsense.util.JNDINames;
import com.synapsense.util.ServerLoginModule;

public class ESConnection extends Thread {
	private LinkedBlockingQueue<Task> tasks = new LinkedBlockingQueue<Task>();

	private Environment env;
	private final String address;
	private final String user;
	private final String pass;

	public ESConnection(String address, String user, String pass) {
		super("ESConnection - " + address);
		setDaemon(true);
		this.address = address;
		this.user = user;
		this.pass = pass;
	}

	@Override
	public void run() {
		Properties props = new Properties();
		props.put(Context.INITIAL_CONTEXT_FACTORY, "org.jnp.interfaces.NamingContextFactory");
		props.put(Context.URL_PKG_PREFIXES, "org.jboss.naming:org.jnp.interfaces");
		props.put(Context.PROVIDER_URL, "jnp://" + address + ":1099");
		InitialContext ctx;
		try {
			ctx = new InitialContext(props);
		} catch (NamingException e) {
			throw new IllegalStateException(e);
		}
		;

		try {
			ServerLoginModule.init(ctx);
			ServerLoginModule.login(user, pass);
			env = ServerLoginModule.getProxy(ctx, JNDINames.ENVIRONMENT, Environment.class);
		} catch (Exception e) {
			try {
				ServerLoginModuleOld.login(user, pass);
				env = ServerLoginModuleOld.getProxy(ctx, JNDINames.ENVIRONMENT, Environment.class);
			} catch (Exception e1) {
				throw new IllegalStateException(e1);
			}
		}

		while (true) {
			try {
				process(tasks.take());
			} catch (InterruptedException e) {
			}
		}
	}

	private void process(Task t) {
		switch (t.getTaskId()) {
		case GET_OTS:
			t.setResult(processGetOTS());
			break;
		case GET_PDS_FOR_OT:
			t.setResult(processGetPD((String) t.getArgs()));
			break;
		case GET_OBJS_FOR_OT:
			t.setResult(processGetObjs((String) t.getArgs()));
			break;
		case GET_PROPS_FOR_OBJ:
			t.setResult(processGetProps((TO<?>) t.getArgs()));
			break;
		}
	}

	private String[] processGetOTS() {
		return env.getObjectTypes();
	}

	private Set<PropertyDescr> processGetPD(String otName) {
		return env.getObjectType(otName).getPropertyDescriptors();
	}

	private Collection<TO<?>> processGetObjs(String otName) {
		return env.getObjectsByType(otName);
	}

	private Collection<ValueTO> processGetProps(TO<?> to) {
		try {
			return env.getAllPropertiesValues(to);
		} catch (ObjectNotFoundException e) {
			throw new IllegalStateException(e);
		}
	}

	public String[] getObjectTypes() {
		Task t = new Task(TaskIDs.GET_OTS, null);
		tasks.add(t);
		return (String[]) t.getResult();
	}

	@SuppressWarnings("unchecked")
	public Set<PropertyDescr> getPropertyDescriptors(String otName) {
		Task t = new Task(TaskIDs.GET_PDS_FOR_OT, otName);
		tasks.add(t);
		return (Set<PropertyDescr>) t.getResult();
	}

	@SuppressWarnings("unchecked")
	public Collection<TO<?>> getObjects(String ot) {
		Task t = new Task(TaskIDs.GET_OBJS_FOR_OT, ot);
		tasks.add(t);
		return (Collection<TO<?>>) t.getResult();
	}

	@SuppressWarnings("unchecked")
	public Collection<ValueTO> getAllProperties(TO<?> to) {
		Task t = new Task(TaskIDs.GET_PROPS_FOR_OBJ, to);
		tasks.add(t);
		return (Collection<ValueTO>) t.getResult();
	}
}
