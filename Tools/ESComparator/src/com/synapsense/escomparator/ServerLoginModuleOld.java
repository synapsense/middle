package com.synapsense.escomparator;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.security.auth.login.LoginContext;
import javax.security.auth.login.LoginException;

import org.jboss.security.auth.callback.UsernamePasswordHandler;

final public class ServerLoginModuleOld {

	/**
	 * Stores the state of current login context. Has different state for
	 * different threads.
	 * 
	 */
	private static ThreadLocal<LoginContext> loginContext = new ThreadLocal<LoginContext>();

	/**
	 * Allows caller to get a proxy reference by the specified JNDI name
	 * <code>jndiTarget</code>. The returned proxy reference is not cached by
	 * <code>ServerLoginModule</code>. In other words, every call to this method
	 * leads to JNDI provider lookup call.<br>
	 * Note that security credentials must be specified separately by calling
	 * <tt>login</tt> method.
	 * 
	 * @see #getProxy(InitialContext, String, Class) if you have already
	 *      configured initial context
	 * 
	 * @param jndiTarget
	 *            JNDI name to lookup for
	 * @param clazz
	 *            Class of proxy reference being requested.
	 * @return The proxy with interface of type <code>clazz</code>
	 * @throws NamingException
	 *             if lookup for <code>jndiTarget</code> has failed.
	 */
	public static <INTERFACE> INTERFACE getProxy(String jndiTarget, Class<INTERFACE> clazz) throws NamingException {
		return getProxy(new InitialContext(), jndiTarget, clazz);
	}

	/**
	 * Allows caller to get a proxy reference by the specified JNDI name
	 * <code>jndiTarget</code> via predefined InitialContext. The returned proxy
	 * reference is not cached by <code>ServerLoginModule</code>. In other
	 * words, every call to this method leads to JNDI provider lookup call.<br>
	 * Note that security credentials must be specified separately by calling
	 * <tt>login</tt> method.
	 * 
	 * @see #getProxy(String, Class) if you want to use default initial context
	 *      {@link InitialContext}
	 * 
	 * @param icx
	 *            Caller-defined initial context
	 * @param jndiTarget
	 *            JNDI name to lookup for
	 * @param clazz
	 *            Class of proxy reference being requested.
	 * @return The proxy with interface of type <code>clazz</code>
	 * @throws NamingException
	 *             if lookup for <code>jndiTarget</code> has failed.
	 */
	@SuppressWarnings("unchecked")
	public static <INTERFACE> INTERFACE getProxy(InitialContext icx, String jndiTarget, Class<INTERFACE> clazz)
	        throws NamingException {
		INTERFACE proxy = (INTERFACE) icx.lookup(jndiTarget);
		return clazz.cast(proxy);
	}

	/**
	 * The method was deprecated.
	 * 
	 * @see #getProxy(String, Class)
	 * 
	 */
	@Deprecated
	public static <INTERFACE> INTERFACE getProxy(String userName, String password, String jndiTarget,
	        Class<INTERFACE> clazz) throws NamingException, LoginException, NoSuchAlgorithmException,
	        UnsupportedEncodingException {
		return getProxy(jndiTarget, clazz);
	}

	/**
	 * The method was deprecated.
	 * 
	 * @see #getProxy(InitialContext, String, Class)
	 * 
	 */
	@Deprecated
	@SuppressWarnings("unchecked")
	public static <INTERFACE> INTERFACE getProxy(InitialContext icx, String jndiTarget, String userName,
	        String password, Class<INTERFACE> clazz) throws NamingException, LoginException, NoSuchAlgorithmException,
	        UnsupportedEncodingException {
		return getProxy(icx, jndiTarget, clazz);
	}

	/**
	 * Allows caller to login and to get security credentials by the specified
	 * user name and password. After this method call a new login context will
	 * be opened.<br>
	 * Login context is a set of credentials which is mandatory passed to any
	 * secured service during a call of any of its methods.
	 * </tt>login</tt>method is not expensive and must be called before using
	 * any secured service.<br>
	 * 
	 * The login context is created per thread. If caller creates a new thread ,
	 * the login context of the current thread is not used in the new thread.
	 * Client must call login in the new thread separately.<br>
	 * 
	 * Note that credentials set by call of this method are used for any service
	 * called from this thread. That is, for the scenario when you:<br>
	 * 1. Get Service1Proxy<br>
	 * 2. Call login<br>
	 * 3. Get Service2Proxy<br>
	 * 4. Call login<br>
	 * 5. Call Service1Proxy and Service2Proxy<br>
	 * the credentials passed at step 4 will be used for all calls at step 5.
	 * 
	 * @param userName
	 *            Name of the user(not null,not empty)
	 * @param password
	 *            Password to login (not null)
	 * @throws IllegalArgumentException
	 *             if some of parameters are illegal
	 * @throws LoginException
	 *             If operation was not successfully.Indicates that errors has
	 *             occurred during login.
	 * 
	 */
	public static void login(String userName, String password) throws LoginException {
		if (userName == null || userName.isEmpty()) {
			throw new IllegalArgumentException("Supplied 'userName' is null or empty");
		}
		if (password == null) {
			throw new IllegalArgumentException("Supplied 'password' is null");
		}

		String encryptedPass;
		try {
			encryptedPass = PasswordService.encrypt(password);
		} catch (NoSuchAlgorithmException e) {
			throw new LoginException("Error occurs during login : " + userName + "Origin of problem : "
			        + e.getLocalizedMessage());
		} catch (UnsupportedEncodingException e) {
			throw new LoginException("Error occurs during login : " + userName + "Origin of problem : "
			        + e.getLocalizedMessage());
		}

		UsernamePasswordHandler handler = new UsernamePasswordHandler(userName, encryptedPass);

		LoginContext lc = loginContext.get();
		if (lc != null) {
			// logout thread's current login context to realize resources
			lc.logout();
		}

		lc = new LoginContext(JBossLoginConfiguration.CONFIG_NAME, null, handler, JBossLoginConfiguration.config);

		lc.login();

		// saves login context only after successful executing of lc.login();
		loginContext.set(lc);
	}

	/**
	 * Allows caller to logout.
	 * 
	 * @see #login(String, String) to login
	 * 
	 * @throws LoginException
	 *             if caller is not logged in or if some problem happen during
	 *             operation
	 */
	public static void logout() throws LoginException {
		LoginContext lc = loginContext.get();
		if (lc == null) {
			throw new LoginException("You are not logged in!");
		}

		lc.logout();

		loginContext.remove();
	}

	/**
	 * 
	 * This class provides password encoding service.
	 * 
	 */
	final public static class PasswordService {
		/**
		 * Encrypts specified password by the algorithm used on the server side.
		 * 
		 * @param text
		 *            Password to encrypt
		 * @return Encrypted password representation.
		 * @throws NoSuchAlgorithmException
		 * @throws UnsupportedEncodingException
		 */
		static public String encrypt(String text) throws NoSuchAlgorithmException, UnsupportedEncodingException {
			MessageDigest md = null;
			md = MessageDigest.getInstance("SHA1");
			md.update(text.getBytes());
			byte raw[] = md.digest();
			String password = new String();
			for (int i = 0; i < raw.length; i++) {
				if ((raw[i] & 0xF0) == 0)
					password += "0";
				password += Integer.toHexString(raw[i] & 0xFF);
			}
			return password;
		}
	}
}