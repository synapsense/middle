package com.synapsense.escomparator;

import java.util.HashMap;
import java.util.Map;

import javax.security.auth.login.AppConfigurationEntry;
import javax.security.auth.login.Configuration;

/**
 * Jboss login module configuration.
 * 
 */
public class JBossLoginConfiguration extends Configuration {

	private static final Map<String, Object> OPTIONS = new HashMap<String, Object>();

	private JBossLoginConfiguration() {
		// enable multi-threaded credentials and principals storage
		OPTIONS.put("multi-threaded", "true");
		// JIC
		OPTIONS.put("restore-login-identity", "false");
	}

	protected static final AppConfigurationEntry[] confEntry = new AppConfigurationEntry[] { new AppConfigurationEntry(
	        "org.jboss.security.ClientLoginModule", AppConfigurationEntry.LoginModuleControlFlag.REQUIRED, OPTIONS) };

	public static final String CONFIG_NAME = "JBoss Client Login Module Only";

	public static JBossLoginConfiguration config = new JBossLoginConfiguration();

	@Override
	public AppConfigurationEntry[] getAppConfigurationEntry(String name) {
		return CONFIG_NAME.equals(name) ? confEntry : null;
	}
}