package com.synapsense.tamagotchi;

public class TamagotchiException extends Exception {

	private static final long serialVersionUID = 2194908280752093836L;

	public TamagotchiException() {
		super();
	}

	public TamagotchiException(String message, Throwable cause) {
		super(message, cause);
	}

	public TamagotchiException(String message) {
		super(message);
	}

	public TamagotchiException(Throwable cause) {
		super(cause);
	}
}
