package com.synapsense.tamagotchi.function;

import java.util.LinkedList;
import java.util.List;

public class PolinomStringBuilder {

	private List<Monom> monoms;

	public PolinomStringBuilder() {
		this.monoms = new LinkedList<Monom>();
	}

	public PolinomStringBuilder appendMonom(String base, int degree, double coeff) {
		monoms.add(new Monom(base, degree, coeff));
		return this;
	}

	public String toString() {
		StringBuilder builder = new StringBuilder();
		for (Monom monom : monoms) {
			if ("".equals(monom.toString())) {
				continue;
			}
			if (monom.getCoeff() > 0) {
				builder.append("+");
			}
			builder.append(monom.toString());
		}
		return builder.toString();
	}

	private class Monom {

		private String base;
		private int degree;
		private double coeff;

		public Monom(String base, int degree, double coeff) {
			super();
			this.base = base;
			this.degree = degree;
			this.coeff = coeff;
		}

		public double getCoeff() {
			return coeff;
		}

		public String toString() {
			if (coeff == 0.0) {
				return "";
			}
			StringBuilder monomStr = new StringBuilder();
			String coeffStr = String.valueOf(coeff);

			// As MathEclipse doesn't accept values such 1.234E4 (must be
			// 1.234E+4)
			int scPos = coeffStr.indexOf('E');
			if (scPos != -1) {
				if (coeffStr.charAt(scPos + 1) != '-') {
					coeffStr = coeffStr.substring(0, scPos + 1) + "+" + coeffStr.substring(scPos + 1);
				}
			}
			monomStr.append(coeffStr);
			if (degree == 0) {
				return monomStr.toString();
			}
			monomStr.append("*").append(base);
			if (degree == 1) {
				return monomStr.toString();
			}
			return monomStr.append("^").append(degree).toString();
		}
	}
}
