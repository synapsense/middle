package com.synapsense.tamagotchi;

import java.io.File;
import java.io.FileReader;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.math.MathException;
import org.apache.commons.math.analysis.interpolation.DividedDifferenceInterpolator;
import org.apache.commons.math.analysis.polynomials.PolynomialFunctionNewtonForm;

import com.synapsense.dto.CollectionTO;
import com.synapsense.dto.NodePlatform;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.dto.ValueTO;
import com.synapsense.tamagotchi.es.EnvDelegate;
import com.synapsense.tamagotchi.export.HomerWsnSensorElement;
import com.synapsense.tamagotchi.export.WsnLikeConfigExporter;
import com.synapsense.tamagotchi.export.WsnSensorElement;
import com.synapsense.tamagotchi.function.PolinomStringBuilder;

public class SensorTamagotchi {

	public static void main(final String[] args) {

		Properties properties = new Properties();
		try {

			System.out.println("Loading configuration...");
			try {
				properties.load(new FileReader(new File("conf/tamagotchi.properties")));
			} catch (Exception e) {
				throw new TamagotchiException("Configuation loading failed - " + e.getMessage());
			}

			Date startDate;
			int size;
			try {
				startDate = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss").parse(properties.getProperty("start.date"));
				size = Integer.parseInt(properties.getProperty("point.size"));
			} catch (ParseException e) {
				throw new TamagotchiException("Unable to parse start date for generation, check property file");
			} catch (NumberFormatException e) {
				throw new TamagotchiException("Unable convert point size parameter to a number, check property file");
			}

			long start = System.currentTimeMillis();
			EnvDelegate env = new EnvDelegate(properties);
			System.out.println("Retreiving sensor data...");
			Collection<CollectionTO> sensorData = env.getSensorData(size, startDate);
			System.out.println("Interpolating sensors lastValue...");
			WsnLikeConfigExporter exporter = new WsnLikeConfigExporter();
			WsnLikeConfigExporter homerExporter = new WsnLikeConfigExporter();
			for (CollectionTO coll : sensorData) {
				List<ValueTO> props = coll.getPropValues();

				Set<ValueTO> sortedProps = new TreeSet<ValueTO>(new Comparator<ValueTO>() {

					@Override
					public int compare(ValueTO o1, ValueTO o2) {
						return new Long(o1.getTimeStamp()).compareTo(new Long(o2.getTimeStamp()));
					}

				});
				sortedProps.addAll(props);

				props = new ArrayList<ValueTO>(sortedProps);

				if (props.isEmpty()) {
					System.out.println("No data found for sensor " + TOFactory.getInstance().saveTO(coll.getObjId()));
					continue;
				}

				double[] x = new double[sortedProps.size() + 1];
				double[] y = new double[sortedProps.size() + 1];
				for (int i = 0; i < props.size(); i++) {
					x[i] = i;
					y[i] = (Double) props.get(i).getValue();
				}
				// Set the same values at the staring and ending point
				x[sortedProps.size()] = x[sortedProps.size() - 1] + 1;
				y[sortedProps.size()] = y[0];

				DividedDifferenceInterpolator interpolator = new DividedDifferenceInterpolator();
				PolynomialFunctionNewtonForm function;
				try {
					function = interpolator.interpolate(x, y);
				} catch (MathException e) {
					throw new TamagotchiException("Error interpolating sensor " + coll.getObjId().getID() + " -"
					        + e.getMessage());
				}
				double[] coeffs = function.getCoefficients();
				PolinomStringBuilder builder = new PolinomStringBuilder();
				for (int j = 0; j < coeffs.length; j++) {
					String timeBase = new DecimalFormat("0").format(x[x.length - 1]);
					builder.appendMonom(
					        "(Floor[TIME/300000] - Floor[TIME/300000/" + timeBase + "] * " + timeBase + ")", j,
					        coeffs[j]);
				}

				String expression = builder.toString();
				if (expression.isEmpty()) {
					System.out.println("Expression attribute of sensor : " + coll.getObjId()
					        + " is empty. It won't be added to output file");
					continue;
				}

				exporter.addElement(new WsnSensorElement(coll.getObjId(), expression));

				// homer
				int type = env.getProperty(coll.getObjId(), "type", com.synapsense.dto.SensorType.class).getType_id();
				int channel = env.getProperty(coll.getObjId(), "channel", Integer.class);
				TO<?> nodeTO = env.getParent(coll.getObjId(), "NODE");
				int networkID = env.getProperty(nodeTO, "networkId", Integer.class);
				long nodePhysID = env.getProperty(nodeTO, "nodePhysicalId", Long.class);
				int platformID = env.getProperty(nodeTO, "platform", NodePlatform.class).getId();
				homerExporter.addElement(new HomerWsnSensorElement(coll.getObjId(), expression, channel, networkID,
				        nodePhysID, type, platformID));
			}
			System.out.println("Exporting configuration...");
			exporter.export(properties.getProperty("output") + "/5.1/");
			homerExporter.export(properties.getProperty("output") + "/5.0/");
			System.out.println("Finished - " + (System.currentTimeMillis() - start) + "ms");
		} catch (TamagotchiException e) {
			System.err.println(e.getMessage());
			e.printStackTrace();
		}
	}
}
