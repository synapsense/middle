package com.synapsense.tamagotchi.export;

import java.util.LinkedHashMap;

import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;

public class WsnSensorElement implements XmlElement, Comparable<XmlElement> {

	public static final int DEFFAULT_SLEEP_DELAY = 300000;
	public static final int DEFFAULT_ROLLOVER = 30000000;
	public static final int FIRST_DELAY_FACTOR = 20;

	public static final String NAMESPACE = "wsnlike";
	public static final String LOCAL_NAME = "sensor";
	public static final String ATTRIBUTE_ID = "id";
	public static final String ATTRIBUTE_FUNCTION = "expression";
	public static final String ATTRIBUTE_FIRSTDELAY = "firstDelay";
	public static final String ATTRIBUTE_SLEEPDELAY = "sleepDelay";
	public static final String ATTRIBUTE_ROLLOVER = "rollover";

	private TO<?> sensorId;
	private String function;
	private int sleepDelay;
	private int rollover;

	public WsnSensorElement(TO<?> objId, String function, int sleepDelay, int rollover) {
		this.sensorId = objId;
		this.function = function;
		this.sleepDelay = sleepDelay;
		this.rollover = rollover;
	}

	public WsnSensorElement(TO<?> objId, String function) {
		this(objId, function, DEFFAULT_SLEEP_DELAY, DEFFAULT_ROLLOVER);
	}

	@Override
	public String getFullName() {
		return NAMESPACE + ":" + LOCAL_NAME;
	}

	public TO<?> getSensorId() {
		return sensorId;
	}

	public String getFunction() {
		return function;
	}

	public int getSleepDelay() {
		return sleepDelay;
	}

	public int getRollover() {
		return rollover;
	}

	public void setSensorId(TO<?> sensorId) {
		this.sensorId = sensorId;
	}

	public void setFunction(String function) {
		this.function = function;
	}

	public void setSleepDelay(int sleepDelay) {
		this.sleepDelay = sleepDelay;
	}

	public void setRollover(int rollover) {
		this.rollover = rollover;
	}

	public LinkedHashMap<String, Object> getAttributes() {
		LinkedHashMap<String, Object> attributes = new LinkedHashMap<String, Object>();
		attributes.put(ATTRIBUTE_ID, TOFactory.getInstance().saveTO(sensorId));
		attributes.put(ATTRIBUTE_FUNCTION, function);
		attributes.put(ATTRIBUTE_FIRSTDELAY, FIRST_DELAY_FACTOR * (Integer) sensorId.getID());
		attributes.put(ATTRIBUTE_SLEEPDELAY, sleepDelay);
		attributes.put(ATTRIBUTE_ROLLOVER, rollover);
		return attributes;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((sensorId == null) ? 0 : sensorId.getID().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		WsnSensorElement other = (WsnSensorElement) obj;
		if (sensorId == null) {
			if (other.sensorId != null)
				return false;
		} else if (!sensorId.getID().equals(other.sensorId.getID()))
			return false;
		return true;
	}

	@Override
	public int compareTo(XmlElement o) {
		if (o.getClass().equals(this.getClass())) {
			WsnSensorElement other = (WsnSensorElement) o;
			Integer thisId = (Integer) sensorId.getID();
			Integer otherId = (Integer) other.getSensorId().getID();
			return thisId.compareTo(otherId);
		}
		return getFullName().compareToIgnoreCase(o.getFullName());
	}
}
