package com.synapsense.tamagotchi.export;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamWriter;

public class WsnLikeConfigExporter {

	private Set<XmlElement> elements;

	public WsnLikeConfigExporter() {
		elements = new TreeSet<XmlElement>();
	}

	public void addElement(final XmlElement el) {
		elements.add(el);
	}

	public void export(final String folder) throws ExportException {
		try {
			File dir = new File(folder);
			dir.mkdirs();
			XMLOutputFactory xmlof = XMLOutputFactory.newInstance();
			XMLStreamWriter xmlw = xmlof.createXMLStreamWriter(new FileOutputStream(folder + "/wsnlike.xml"));
			xmlw.writeStartDocument();
			xmlw.writeStartElement("wsnlike:wsnlike");
			xmlw.writeNamespace("wsnlike", "http://www.synapsense.com/devicemapper/wsnlike");
			xmlw.writeNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance");
			xmlw.writeCharacters("\n");
			for (XmlElement el : elements) {
				xmlw.writeStartElement(el.getFullName());
				Map<String, Object> attrs = el.getAttributes();
				for (String attrName : attrs.keySet()) {
					xmlw.writeAttribute(attrName, attrs.get(attrName).toString());
				}
				xmlw.writeEndElement();
				xmlw.writeCharacters("\n");
			}
			xmlw.writeEndElement();
			xmlw.writeEndDocument();
			xmlw.close();
		} catch (Exception e) {
			throw new ExportException("Unable to export configuration", e);
		}
	}
}
