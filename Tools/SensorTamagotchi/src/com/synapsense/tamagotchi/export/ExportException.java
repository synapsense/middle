package com.synapsense.tamagotchi.export;

import com.synapsense.tamagotchi.TamagotchiException;

public class ExportException extends TamagotchiException {

	private static final long serialVersionUID = -6378943651449692643L;

	public ExportException() {
		super();
	}

	public ExportException(String message, Throwable cause) {
		super(message, cause);
	}

	public ExportException(String message) {
		super(message);
	}

	public ExportException(Throwable cause) {
		super(cause);
	}
}
