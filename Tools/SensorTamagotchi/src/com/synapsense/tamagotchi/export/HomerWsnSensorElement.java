package com.synapsense.tamagotchi.export;

import java.util.LinkedHashMap;

import com.synapsense.dto.TO;

public class HomerWsnSensorElement extends WsnSensorElement {

	private int channel;
	private int networkId;
	private long nodePhysId;
	private int type;
	private int platformId;

	public static final String ATTRIBUTE_CHANNEL = "channel";
	public static final String ATTRIBUTE_NETWORKID = "networkID";
	public static final String ATTRIBUTE_NODEPHYSID = "nodePhysID";
	public static final String ATTRIBUTE_PLATFORMID = "platformID";
	public static final String ATTRIBUTE_TYPE = "type";

	public HomerWsnSensorElement(TO<?> objId, String function, int sleepDelay, int rollover, int channel,
	        int networkId, long nodePhysId, int type, int platformId) {
		super(objId, function, sleepDelay, rollover);
		this.channel = channel;
		this.networkId = networkId;
		this.nodePhysId = nodePhysId;
		this.type = type;
		this.platformId = platformId;
	}

	public HomerWsnSensorElement(TO<?> objId, String function, int channel, int networkId, long nodePhysId, int type,
	        int platformId) {
		super(objId, function);
		this.channel = channel;
		this.networkId = networkId;
		this.nodePhysId = nodePhysId;
		this.type = type;
		this.platformId = platformId;
	}

	public int getChannel() {
		return channel;
	}

	public void setChannel(int channel) {
		this.channel = channel;
	}

	public int getNetworkId() {
		return networkId;
	}

	public void setNetworkId(int networkId) {
		this.networkId = networkId;
	}

	public long getNodePhysId() {
		return nodePhysId;
	}

	public void setNodePhysId(long nodePhysId) {
		this.nodePhysId = nodePhysId;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public int getPlatformId() {
		return platformId;
	}

	public void setPlatformId(int platformId) {
		this.platformId = platformId;
	}

	@Override
	public LinkedHashMap<String, Object> getAttributes() {
		LinkedHashMap<String, Object> attributes = new LinkedHashMap<String, Object>();
		attributes.put(ATTRIBUTE_CHANNEL, channel);
		attributes.put(ATTRIBUTE_NETWORKID, networkId);
		attributes.put(ATTRIBUTE_NODEPHYSID, nodePhysId);
		attributes.put(ATTRIBUTE_PLATFORMID, platformId);
		attributes.put(ATTRIBUTE_TYPE, type);
		attributes.put(ATTRIBUTE_FUNCTION, getFunction());
		attributes.put(ATTRIBUTE_FIRSTDELAY, FIRST_DELAY_FACTOR * (Integer) getSensorId().getID());
		attributes.put(ATTRIBUTE_SLEEPDELAY, getSleepDelay());
		attributes.put(ATTRIBUTE_ROLLOVER, getRollover());
		return attributes;
	}
}
