package com.synapsense.tamagotchi.export;

import java.util.LinkedHashMap;

public interface XmlElement {
	public String getFullName();

	public LinkedHashMap<String, Object> getAttributes();
}
