package com.synapsense.tamagotchi.es;

import com.synapsense.tamagotchi.TamagotchiException;

public class ResourceException extends TamagotchiException {

	private static final long serialVersionUID = 5142180162771002108L;

	public ResourceException() {
		super();
	}

	public ResourceException(String message, Throwable cause) {
		super(message, cause);
	}

	public ResourceException(String message) {
		super(message);
	}

	public ResourceException(Throwable cause) {
		super(cause);
	}
}
