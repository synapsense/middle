package com.synapsense.tamagotchi.es;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Properties;

import javax.naming.InitialContext;

import com.synapsense.dto.CollectionTO;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.dto.ValueTO;
import com.synapsense.service.Environment;
import com.synapsense.util.JNDINames;
import com.synapsense.util.ServerLoginModule;

public class EnvDelegate {

	private Environment env;
	private Properties properties;

	public EnvDelegate(Properties properties) {
		this.properties = properties;
	}

	public Collection<CollectionTO> getSensorData(int size, int start) throws ResourceException {
		connect();
		try {
			Collection<TO<?>> sensors = env.getObjectsByType("SENSOR");
			Collection<CollectionTO> allData = new ArrayList<CollectionTO>();
			for (TO<?> sensor : sensors) {
				Collection<ValueTO> histProps = env.getHistory(sensor, "lastValue", size, start, Double.class);
				CollectionTO col = new CollectionTO(sensor, new ArrayList<ValueTO>(histProps));
				allData.add(col);
			}
			return allData;
		} catch (Exception e) {
			throw new ResourceException("Unable to retreive sensors historical data", e);
		}
	}

	public Collection<CollectionTO> getSensorData(int size, Date start) throws ResourceException {
		connect();
		try {
			Collection<TO<?>> sensors = env.getObjectsByType("SENSOR");
			Collection<CollectionTO> allData = new ArrayList<CollectionTO>();
			for (TO<?> sensor : sensors) {
				Collection<ValueTO> histProps = env.getHistory(sensor, "lastValue", start, size, Double.class);
				CollectionTO col = new CollectionTO(sensor, new ArrayList<ValueTO>(histProps));
				allData.add(col);
			}
			return allData;
		} catch (Exception e) {
			throw new ResourceException("Unable to retreive sensors historical data", e);
		}
	}

	public <TYPE> TYPE getProperty(TO<?> objId, String propName, Class<TYPE> clazz) throws ResourceException {
		connect();
		try {
			return env.getPropertyValue(objId, propName, clazz);
		} catch (Exception e) {
			throw new ResourceException("Unable to retreive " + propName + " for object "
			        + TOFactory.getInstance().saveTO(objId), e);
		}
	}

	public TO<?> getParent(TO<?> objId, String type) throws ResourceException {
		connect();
		try {
			return env.getParents(objId, type).iterator().next();
		} catch (Exception e) {
			throw new ResourceException("Unable to retreive parent of type " + type + " for object "
			        + TOFactory.getInstance().saveTO(objId), e);
		}
	}

	private void connect() throws ResourceException {
		if (env != null) {
			// already connected
			return;
		}
		try {
			InitialContext context = new InitialContext(properties);
			env = ServerLoginModule.getProxy(context, JNDINames.ENVIRONMENT, Environment.class);
			ServerLoginModule.login(properties.getProperty("jboss.user.name"),
			        properties.getProperty("jboss.user.password"));
		} catch (Exception e) {
			throw new ResourceException("Unnable to get environment", e);
		}
	}
}
