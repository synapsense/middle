package com.synapsense.ct.test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import junit.framework.Assert;

import org.junit.Test;

import com.synapsense.ct.corr.TrendFinder;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.dto.ValueTO;

public class TrendFinderTest {

	@Test
	public void testCreateAndSetAllParameters() {
		TrendFinder finder = new TrendFinder("t");
		finder.setMinMagnitude(0.1);
		finder.setMinDuration(60000);
		finder.setNoiseThreshold(0.01);
	}

	@Test
	public void testSimpleTrend() {
		TOFactory tof = TOFactory.getInstance();
		TO<?> sensor1 = tof.loadTO("SENSOR:1");
		TO<?> sensor2 = tof.loadTO("SENSOR:2");

		TrendFinder finder = new TrendFinder("t");
		finder.setKernelTO(tof.loadTO("crac:1"));
		finder.setMinMagnitude(0.1);
		finder.setMinDuration(120000);
		finder.setNoiseThreshold(0.01);

		finder.addAffected(sensor1);
		finder.addAffected(sensor2);

		finder.nextSample();
		finder.setAffectedValue(sensor1, new ValueTO("sample", 1.0, 0));
		finder.setAffectedValue(sensor2, new ValueTO("sample", 1.0, 0));

		finder.nextSample();
		finder.setAffectedValue(sensor1, new ValueTO("sample", 2.0, 61000));
		finder.setAffectedValue(sensor2, new ValueTO("sample", 0.0, 61000));

		finder.nextSample();
		finder.setAffectedValue(sensor1, new ValueTO("sample", 3.0, 122000));
		finder.setAffectedValue(sensor2, new ValueTO("sample", 4.0, 122000));

		Collection<TO<?>> dependent = finder.correlate().getDependentObjects().values().iterator().next();
		Assert.assertEquals("Dependent size", 1, dependent.size());
		Assert.assertEquals("Dependent object", sensor1, dependent.iterator().next());
	}

	@Test
	public void testDifferentTrends() {
		@SuppressWarnings("serial")
		List<ValueTO> valuesTo = new ArrayList<ValueTO>() {
			{
				add(new ValueTO("sample", 20.0, 0));
				add(new ValueTO("sample", 18.0, 5));
				add(new ValueTO("sample", 14.0, 10));
				add(new ValueTO("sample", 12.0, 15));
				add(new ValueTO("sample", 16.0, 20));
			}
		};

		TrendFinder finder = new TrendFinder("t");
		finder.setNoiseThreshold(0.5);
		finder.setMinMagnitude(8);
		finder.setMinDuration(5);

		Assert.assertTrue("Trend", finder.isTrend(valuesTo));

		finder.setNoiseThreshold(3.0);
		Assert.assertFalse("No Trend", finder.isTrend(valuesTo));

		finder.setNoiseThreshold(0.0);
		finder.setMinDuration(20);
		Assert.assertFalse("No Trend", finder.isTrend(valuesTo));

		finder.setMinDuration(0);
		finder.setMinMagnitude(20.0);
		Assert.assertFalse("No Trend", finder.isTrend(valuesTo));

		valuesTo.add(new ValueTO("sample", 16.0, 25));
		valuesTo.add(new ValueTO("sample", 16.0, 30));
		valuesTo.add(new ValueTO("sample", 16.0, 35));
		valuesTo.add(new ValueTO("sample", 16.0, 40));
		valuesTo.add(new ValueTO("sample", 30.0, 45));
		valuesTo.add(new ValueTO("sample", 40.0, 50));
		valuesTo.add(new ValueTO("sample", 50.0, 5));

		Assert.assertTrue("Trend", finder.isTrend(valuesTo));

		finder.setMinMagnitude(00.0);
		finder.setNoiseThreshold(9.0);
		Assert.assertTrue("Trend", finder.isTrend(valuesTo));

		finder.setMinDuration(100);
		Assert.assertFalse("No Trend", finder.isTrend(valuesTo));
	}
}
