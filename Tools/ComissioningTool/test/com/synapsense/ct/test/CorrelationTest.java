package com.synapsense.ct.test;

import java.util.Collection;
import java.util.Map;

import junit.framework.Assert;

import org.junit.Test;

import com.synapsense.ct.corr.ClassicCorrelator;
import com.synapsense.ct.corr.Correlator;
import com.synapsense.ct.corr.PearsonsCorrelator;
import com.synapsense.ct.utils.CollectionUtils;
import com.synapsense.ct.utils.MathUtils;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;

public class CorrelationTest {

	@Test
	public void testPearsonsFillOneSample() {
		Correlator correlator = new PearsonsCorrelator("p");

		TOFactory tof = TOFactory.getInstance();

		correlator.addAffected(tof.loadTO("Sensor:1"));
		correlator.addAffected(tof.loadTO("Sensor:2"));

		correlator.nextSample();
		correlator.setKernelValue(1.0);
		correlator.setAffectedValue(tof.loadTO("Sensor:1"), 1.0);
		correlator.setAffectedValue(tof.loadTO("Sensor:1"), 2.0);
		correlator.setAffectedValue(tof.loadTO("Sensor:2"), 5.0);
		correlator.nextSample();

		Assert.assertEquals("Number of samples ", 1, correlator.getCompletedSamplesNumber());

		Map<TO<?>, Double> sampleVal = correlator.getAffectedSampleValues(0);

		Assert.assertEquals("Sensor1 value", 2.0, sampleVal.get(tof.loadTO("Sensor:1")), 0.0);
		Assert.assertEquals("Sensor2 value", 5.0, sampleVal.get(tof.loadTO("Sensor:2")), 0.0);

		Assert.assertEquals("Kernel value", 1.0, correlator.getKernelSampleValue(0), 0.0);
	}

	@Test
	public void testEmptyKernelSample() {
		TOFactory tof = TOFactory.getInstance();
		Correlator correlator = new PearsonsCorrelator("p");
		correlator.addAffected(tof.loadTO("Sensor:1"));

		correlator.nextSample();
		correlator.setKernelValue(1.0);
		correlator.setAffectedValue(tof.loadTO("Sensor:1"), 1.0);

		correlator.nextSample();
		correlator.setAffectedValue(tof.loadTO("Sensor:1"), 2.0);
		correlator.nextSample();

		Assert.assertEquals("Kernel value empty sample", 1.0, correlator.getKernelSampleValue(1), 0.0);
	}

	@Test
	public void testEmptyAffectedSample() {
		TOFactory tof = TOFactory.getInstance();
		Correlator correlator = new PearsonsCorrelator("p");
		correlator.addAffected(tof.loadTO("Sensor:1"));
		correlator.addAffected(tof.loadTO("Sensor:2"));
		correlator.addAffected(tof.loadTO("Sensor:3"));

		correlator.nextSample();
		correlator.setAffectedValue(tof.loadTO("Sensor:1"), 1.0);
		correlator.setAffectedValue(tof.loadTO("Sensor:2"), 5.5);
		correlator.nextSample();

		Assert.assertEquals("Kernel value empty sample", 0.0, correlator.getKernelSampleValue(0), 0.0);

		Map<TO<?>, Double> sampleValues = correlator.getAffectedSampleValues(0);
		Assert.assertEquals("Sensor1 value", 1.0, sampleValues.get(tof.loadTO("Sensor:1")), 0.0);
		Assert.assertEquals("Sensor2 value", 5.5, sampleValues.get(tof.loadTO("Sensor:2")), 0.0);
		Assert.assertEquals("Sensor3 value", 0.0, sampleValues.get(tof.loadTO("Sensor:3")), 0.0);

		correlator.setAffectedValue(tof.loadTO("Sensor:3"), 10.0);
		correlator.setKernelValue(9.0);
		correlator.nextSample();

		Assert.assertEquals("Kernel value empty sample", 9.0, correlator.getKernelSampleValue(1), 0.0);

		sampleValues = correlator.getAffectedSampleValues(1);
		Assert.assertEquals("Sensor1 value", 1.0, sampleValues.get(tof.loadTO("Sensor:1")), 0.0);
		Assert.assertEquals("Sensor2 value", 5.5, sampleValues.get(tof.loadTO("Sensor:2")), 0.0);
		Assert.assertEquals("Sensor3 value", 10.0, sampleValues.get(tof.loadTO("Sensor:3")), 0.0);
	}

	@Test
	public void testRunSimplePearsonCorrellator() {
		TOFactory tof = TOFactory.getInstance();
		ClassicCorrelator correlator = new PearsonsCorrelator("p");
		correlator.setDependencyThreshold(0.1);
		correlator.setKernelTO(tof.loadTO("crac:1"));
		correlator.addAffected(tof.loadTO("Sensor:1"));
		correlator.addAffected(tof.loadTO("Sensor:2"));

		double val[] = new double[50];
		double val1[] = new double[50];
		double val2[] = new double[50];

		for (int i = 0; i < 50; i++) {
			val[i] = i;
			val1[i] = Math.asin(Math.sqrt(1 - Math.pow(Math.cos(i), 2)));
			val2[i] = Math.pow(i, 0.00000000001);
			correlator.nextSample();
			correlator.setKernelValue(val[i]);
			correlator.setAffectedValue(tof.loadTO("Sensor:1"), val1[i]);
			correlator.setAffectedValue(tof.loadTO("Sensor:2"), val2[i]);
		}

		Collection<TO<?>> corrValues = correlator.correlate().getDependentObjects().values().iterator().next();
		Assert.assertEquals("Dependent size", 1, corrValues.size());
		Assert.assertEquals("Dependent sensor", tof.loadTO("Sensor:2"), corrValues.iterator().next());

		Assert.assertEquals("Sensor1 correlation", 0.041, correlator.correlateArrays(val, val1), 0.001);
		Assert.assertEquals("Sensor2 correlation", 0.242, correlator.correlateArrays(val, val2), 0.001);
	}

	@Test
	public void testClearCorrelator() {
		TOFactory tof = TOFactory.getInstance();
		Correlator correlator = new PearsonsCorrelator("p");
		correlator.addAffected(tof.loadTO("Sensor:1"));
		correlator.addAffected(tof.loadTO("Sensor:2"));

		for (int i = 0; i < 50; i++) {
			correlator.nextSample();
			correlator.setKernelValue(i);
			correlator.setAffectedValue(tof.loadTO("Sensor:1"), Math.asin(Math.sqrt(1 - Math.pow(Math.cos(i), 2))));
			correlator.setAffectedValue(tof.loadTO("Sensor:2"), Math.pow(i, 0.00000000001));
		}

		correlator.clearValues();

		Assert.assertTrue("Kernel values list is empty", correlator.getKernelValues().isEmpty());

		Assert.assertTrue("Sensor1 values list is empty", correlator.getAffectedValues().get(tof.loadTO("Sensor:1"))
		        .isEmpty());
		Assert.assertTrue("Sensor2 values list is empty", correlator.getAffectedValues().get(tof.loadTO("Sensor:2"))
		        .isEmpty());

		Assert.assertTrue("Not sampling", !correlator.isSampling());
		Assert.assertEquals("Current sample", 0, correlator.getCurSample());
	}

	@Test
	public void testValuable() {
		Map<Integer, Double> initial = CollectionUtils.newLinkedHashMap();
		initial.put(1, 10.0);
		initial.put(2, 10.0);
		initial.put(3, 10.0);
		initial.put(4, 8.0);
		initial.put(5, 7.0);
		initial.put(6, 9.0);
		initial.put(7, 1.0);
		initial.put(8, 3.0);
		initial.put(9, 0.0);
		initial.put(10, -1.0);

		Map<Integer, Double> expected = CollectionUtils.newLinkedHashMap();
		expected.put(1, 10.0);
		expected.put(2, 10.0);
		expected.put(3, 10.0);
		expected.put(6, 9.0);
		expected.put(4, 8.0);

		Map<Integer, Double> actual = MathUtils.getMostValuable(initial, 5);
		Assert.assertEquals(expected, actual);

	}
}
