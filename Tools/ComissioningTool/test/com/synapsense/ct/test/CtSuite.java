package com.synapsense.ct.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ CorrelationTest.class, ClassifierTest.class, EnvDummyDataAccessTest.class, TrendFinderTest.class })
public class CtSuite {
}
