package com.synapsense.ct.test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.Map;

import junit.framework.Assert;

import org.jmock.Expectations;
import org.jmock.Mockery;
import org.jmock.integration.junit4.JMock;
import org.jmock.integration.junit4.JUnit4Mockery;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.synapsense.ct.ProgressMonitor;
import com.synapsense.ct.classification.OnlineClassification;
import com.synapsense.ct.corr.ClassicCorrelator;
import com.synapsense.ct.corr.CorrelationResult;
import com.synapsense.ct.corr.Correlator;
import com.synapsense.ct.corr.PearsonsCorrelator;
import com.synapsense.ct.corr.TrendFinder;
import com.synapsense.ct.data.DataAccess;
import com.synapsense.ct.data.DataAccessException;
import com.synapsense.ct.utils.CollectionUtils;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.dto.ValueTO;

@RunWith(JMock.class)
public class ClassifierTest {

	private Mockery context;

	@Before
	public void setUp() {
		context = new JUnit4Mockery();
	}

	@Test
	public void testCreateClassificator() {

		OnlineClassification classifier = new OnlineClassification();
		ClassicCorrelator correlator = new PearsonsCorrelator("pearsons");
		correlator.setDependencyThreshold(0.5);
		classifier.setCorrelator(correlator);

		Assert.assertEquals("Dependency threshold", 0.5, correlator.getThreshold(), 0.0);
	}

	@Test
	public void testTrendClassification() {
		TrendFinder finder = new TrendFinder("Trend");
		finder.setMinDuration(0);
		finder.setMinMagnitude(8.0);
		finder.setNoiseThreshold(1.9);
		testSimpleClassification(finder);
	}

	@Test
	public void testPearsonsClassification() {
		testSimpleClassification(new PearsonsCorrelator("p"));
	}

	@SuppressWarnings("serial")
	private void testSimpleClassification(Correlator correlator) {

		final TOFactory tof = TOFactory.getInstance();

		final TO<?> crac1 = tof.loadTO("crac:1");
		final TO<?> crac2 = tof.loadTO("crac:2");

		final DataAccess dataAccessMock = context.mock(DataAccess.class);

		OnlineClassification classifier = new OnlineClassification();
		classifier.setSettleInterval(3);
		classifier.setStartCalibration(10);
		classifier.setCalibrationStep(2.0);
		classifier.setStepsNumber(5);

		classifier.setCorrelator(correlator);
		classifier.setDataAccess(dataAccessMock);

		final Collection<TO<?>> sensors = Arrays.asList(new TO<?>[] { tof.loadTO("sensor:1"), tof.loadTO("sensor:2") });

		try {
			context.checking(new Expectations() {
				{
					exactly(2).of(dataAccessMock).setControllableValue(crac1, 10.0);
					exactly(2).of(dataAccessMock).setControllableValue(crac2, 10.0);
					oneOf(dataAccessMock).setControllableValue(crac1, 12.0);
					oneOf(dataAccessMock).setControllableValue(crac2, 12.0);
					oneOf(dataAccessMock).setControllableValue(crac1, 14.0);
					oneOf(dataAccessMock).setControllableValue(crac2, 14.0);
					oneOf(dataAccessMock).setControllableValue(crac1, 16.0);
					oneOf(dataAccessMock).setControllableValue(crac2, 16.0);
					oneOf(dataAccessMock).setControllableValue(crac1, 18.0);
					oneOf(dataAccessMock).setControllableValue(crac2, 18.0);
					oneOf(dataAccessMock).setControllableValue(crac1, 20.0);
					oneOf(dataAccessMock).setControllableValue(crac2, 20.0);

					oneOf(dataAccessMock).getSensorsValues(sensors);
					will(returnValue(getValMap(0)));
					oneOf(dataAccessMock).getSensorsValues(sensors);
					will(returnValue(getValMap(1)));
					oneOf(dataAccessMock).getSensorsValues(sensors);
					will(returnValue(getValMap(2)));
					oneOf(dataAccessMock).getSensorsValues(sensors);
					will(returnValue(getValMap(3)));
					oneOf(dataAccessMock).getSensorsValues(sensors);
					will(returnValue(getValMap(4)));
					oneOf(dataAccessMock).getSensorsValues(sensors);
					will(returnValue(getValMap(5)));
					oneOf(dataAccessMock).getSensorsValues(sensors);
					will(returnValue(getValMap(6)));
					oneOf(dataAccessMock).getSensorsValues(sensors);
					will(returnValue(getValMap(7)));
					oneOf(dataAccessMock).getSensorsValues(sensors);
					will(returnValue(getValMap(8)));
					oneOf(dataAccessMock).getSensorsValues(sensors);
					will(returnValue(getValMap(9)));
					oneOf(dataAccessMock).getSensorsValues(sensors);
					will(returnValue(getValMap(10)));
					oneOf(dataAccessMock).getSensorsValues(sensors);
					will(returnValue(getValMap(11)));
					allowing(dataAccessMock).getSensorValue(crac1);
					will(returnValue(10.0));
					allowing(dataAccessMock).getSensorValue(crac2);
					will(returnValue(10.0));
				}
			});
		} catch (DataAccessException e1) {
			Assert.fail(e1.getMessage());
			e1.printStackTrace();
		}

		classifier.classify(new ArrayList<TO<?>>() {
			{
				add(crac1);
				add(crac2);
			}
		}, sensors, new ProgressMonitor() {

			@Override
			public void complete(CorrelationResult res) {
				// Regions expected = new Regions();
				// expected.addSensorToRegion(crac1, tof.loadTO("sensor:1"));
				// expected.addSensorToRegion(crac1, tof.loadTO("sensor:2"));
				// expected.addSensorToRegion(crac2, tof.loadTO("sensor:2"));
				// Assert.assertEquals("Regions", expected, res);
			}

			@Override
			public void fail(Throwable e) {
				Assert.fail("Failed " + e.getMessage());
				e.printStackTrace();
			}

			@Override
			public void setMessage(String message) {

			}

			@Override
			public void setProgress(double progress) {

			}
		});
	}

	private Map<TO<?>, ValueTO> getValMap(int index) {
		TOFactory tof = TOFactory.getInstance();
		Map<TO<?>, ValueTO> res = CollectionUtils.newHashMap();
		if (index < 6) {
			res.put(tof.loadTO("sensor:1"), new ValueTO("sample", 10 + 2.0 * index, new Date().getTime()));
			res.put(tof.loadTO("sensor:2"), new ValueTO("sample", 10 + 2.0 * index, new Date().getTime()));
		} else {
			res.put(tof.loadTO("sensor:1"), new ValueTO("sample", 10.0, new Date().getTime()));
			res.put(tof.loadTO("sensor:2"), new ValueTO("sample", 10 + 2.0 * index, new Date().getTime()));
		}
		return res;
	}
}
