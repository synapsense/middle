package com.synapsense.ct.test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import junit.framework.Assert;

import org.jmock.Expectations;
import org.jmock.Mockery;
import org.jmock.integration.junit4.JMock;
import org.jmock.integration.junit4.JUnit4Mockery;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.synapsense.ct.conf.ControlObjectDescription;
import com.synapsense.ct.data.DataAccessException;
import com.synapsense.ct.data.EnvDataAccess;
import com.synapsense.ct.data.EnvDummyDataAccess;
import com.synapsense.ct.utils.CollectionUtils;
import com.synapsense.ct.utils.Pair;
import com.synapsense.dto.CollectionTO;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.dto.ValueTO;
import com.synapsense.service.Environment;

@RunWith(JMock.class)
public class EnvDummyDataAccessTest {

	private Mockery context;

	@Before
	public void setUp() {
		context = new JUnit4Mockery();
	}

	@Test
	public void testSensorDivision() {
		final TOFactory tof = TOFactory.getInstance();
		Collection<TO<?>> cracs = new ArrayList<TO<?>>();

		cracs.add(tof.loadTO("crac:1"));
		cracs.add(tof.loadTO("crac:2"));
		cracs.add(tof.loadTO("crac:3"));

		Collection<TO<?>> sensors = new ArrayList<TO<?>>();

		sensors.add(tof.loadTO("sensor:1"));
		sensors.add(tof.loadTO("sensor:2"));
		sensors.add(tof.loadTO("sensor:3"));
		sensors.add(tof.loadTO("sensor:4"));
		sensors.add(tof.loadTO("sensor:5"));
		sensors.add(tof.loadTO("sensor:6"));
		sensors.add(tof.loadTO("sensor:7"));
		sensors.add(tof.loadTO("sensor:8"));
		sensors.add(tof.loadTO("sensor:9"));
		sensors.add(tof.loadTO("sensor:10"));
		sensors.add(tof.loadTO("sensor:11"));

		final Environment envMock = context.mock(Environment.class);

		try {
			context.checking(new Expectations() {
				{
					oneOf(envMock).setPropertyValue(tof.loadTO("crac:1"), "supplyT", 10.0);
					oneOf(envMock).setPropertyValue(tof.loadTO("crac:2"), "supplyT", 10.0);
					oneOf(envMock).setPropertyValue(tof.loadTO("crac:3"), "supplyT", 10.0);
					oneOf(envMock).setPropertyValue(tof.loadTO("sensor:1"), "lastValue", 10.0);
					oneOf(envMock).setPropertyValue(tof.loadTO("sensor:2"), "lastValue", 10.0);
					oneOf(envMock).setPropertyValue(tof.loadTO("sensor:3"), "lastValue", 10.0);
					oneOf(envMock).setPropertyValue(tof.loadTO("sensor:4"), "lastValue", 10.0);
					oneOf(envMock).setPropertyValue(tof.loadTO("sensor:5"), "lastValue", 10.0);
					oneOf(envMock).setPropertyValue(tof.loadTO("sensor:6"), "lastValue", 10.0);
					oneOf(envMock).setPropertyValue(tof.loadTO("sensor:7"), "lastValue", 10.0);
					oneOf(envMock).setPropertyValue(tof.loadTO("sensor:8"), "lastValue", 10.0);
					oneOf(envMock).setPropertyValue(tof.loadTO("sensor:9"), "lastValue", 10.0);
				}
			});
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}

		EnvDummyDataAccess access = new EnvDummyDataAccess(cracs, sensors);
		access.setEnvironment(envMock);
		try {
			access.setControllableValue(tof.loadTO("crac:1"), 10.0);
			access.setControllableValue(tof.loadTO("crac:2"), 10.0);
			access.setControllableValue(tof.loadTO("crac:3"), 10.0);
		} catch (DataAccessException e) {
			Assert.fail(e.getMessage());
		}
	}

	@SuppressWarnings("serial")
	@Test
	public void testHistoryValues() {

		final TOFactory tof = TOFactory.getInstance();

		final TO<?> crac = tof.loadTO("crac:1");
		final TO<?> sensor1 = tof.loadTO("sensor:1");
		final TO<?> sensor2 = tof.loadTO("sensor:2");

		final Date start = new Date(500000);
		final Date end = new Date(1000000);

		final Collection<ValueTO> cracValues = new ArrayList<ValueTO>();
		final Collection<ValueTO> sensor1Values = new ArrayList<ValueTO>();
		final Collection<ValueTO> sensor2Values = new ArrayList<ValueTO>();

		List<Pair<Date, List<Double>>> resList = CollectionUtils.newArrayList();

		for (int i = 0; i < 5; i++) {
			final int j = i;
			List<Double> vals = new ArrayList<Double>() {
				{
					add(1.0 * j);
					add(2.0 * j);
					add(3.0 * j);
				}
			};

			resList.add(CollectionUtils.newPair(new Date(i * 5), vals));

			cracValues.add(new ValueTO("returnT", 1.0 * j, i * 5));
			sensor1Values.add(new ValueTO("lastValue", 2.0 * j, i * 5 + 2));
			sensor2Values.add(new ValueTO("lastValue", 3.0 * j, i * 5 + 1));
		}

		final Environment envMock = context.mock(Environment.class);
		try {
			context.checking(new Expectations() {
				{
					oneOf(envMock).getHistory(crac, "lastValue", start, end, Double.class);
					will(returnValue(cracValues));
					oneOf(envMock).getHistory(sensor1, "lastValue", new Date(start.getTime() - 60000),
					        new Date(end.getTime() + 60000), Double.class);
					will(returnValue(sensor1Values));
					oneOf(envMock).getHistory(sensor2, "lastValue", new Date(start.getTime() - 60000),
					        new Date(end.getTime() + 60000), Double.class);
					will(returnValue(sensor2Values));
				}
			});

			EnvDataAccess access = new EnvDataAccess();
			access.setEnvironment(envMock);
			List<Pair<Date, List<Double>>> actual = access.getHistoricalValueTable(crac, new ArrayList<TO<?>>() {
				{
					add(sensor1);
					add(sensor2);
				}
			}, start, end);

			Assert.assertEquals("History results", resList, actual);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}

	@SuppressWarnings("serial")
	@Test
	public void getObjectsByDescriptionTest() {
		Collection<ControlObjectDescription> objDescrs = new ArrayList<ControlObjectDescription>();
		ControlObjectDescription descr = new ControlObjectDescription();
		descr.setTypeName("CRAC");
		descr.setPropertyName("returnT");
		descr.addFilter("name", "CRAC1");
		descr.addFilter("name", "CRAC2");

		objDescrs.add(descr);

		final Environment envMock = context.mock(Environment.class);

		final Collection<TO<?>> returnCracs = new ArrayList<TO<?>>() {

			{
				add(TOFactory.getInstance().loadTO("SENSOR:1"));
			}
		};

		final Collection<CollectionTO> sensProps = new ArrayList<CollectionTO>() {
			{
				add(new CollectionTO(TOFactory.getInstance().loadTO("SENSOR:1"), new ArrayList<ValueTO>() {
					{
						add(new ValueTO("returnT", TOFactory.getInstance().loadTO("SENSOR:2")));
					}
				}));
			}
		};

		final Collection<TO<?>> expected = new ArrayList<TO<?>>() {
			{
				add(TOFactory.getInstance().loadTO("SENSOR:2"));
			}
		};
		try {
			context.checking(new Expectations() {
				{
					oneOf(envMock).getObjects("CRAC", new ValueTO[] { new ValueTO("name", "CRAC1") });
					will(returnValue(returnCracs));
					oneOf(envMock).getObjects("CRAC", new ValueTO[] { new ValueTO("name", "CRAC2") });
					will(returnValue(new ArrayList<TO<?>>()));
					oneOf(envMock).getPropertyValue(returnCracs, new String[] { "returnT" });
					will(returnValue(sensProps));
				}
			});

			EnvDataAccess access = new EnvDataAccess();
			access.setEnvironment(envMock);
			Collection<TO<?>> actual = access.getSensorsByDescription(objDescrs);
			Assert.assertEquals("Objects, retreived by description", expected, actual);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}
}
