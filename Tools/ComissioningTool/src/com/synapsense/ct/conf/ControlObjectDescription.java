package com.synapsense.ct.conf;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import com.synapsense.ct.utils.CollectionUtils;

public class ControlObjectDescription {

	private String typeName;

	private String propertyName;

	private Map<String, Set<Object>> filters;

	private String to;

	public ControlObjectDescription() {
		filters = new HashMap<String, Set<Object>>();
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public String getPropertyName() {
		return propertyName;
	}

	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}

	public Map<String, Set<Object>> getFilters() {
		return filters;
	}

	public void addFilter(String name, Object value) {
		CollectionUtils.updateSetValuedMap(filters, name, value);
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}
}
