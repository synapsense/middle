package com.synapsense.ct.conf;

import java.io.File;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.naming.InitialContext;

import org.apache.commons.digester.Digester;
import org.apache.commons.digester.Rule;
import org.xml.sax.Attributes;

import com.synapsense.ct.classification.Classification;
import com.synapsense.ct.corr.Correlator;
import com.synapsense.ct.data.DataAccess;
import com.synapsense.ct.export.Exporter;
import com.synapsense.dto.TOFactory;
import com.synapsense.service.Environment;
import com.synapsense.util.JNDINames;
import com.synapsense.util.ServerLoginModule;

/**
 * The instance of this class holds all the objects and properties needed by
 * Commissioning tool: <br>
 * -Data Access object <br>
 * -Working mode (off-line/online) <br>
 * -Correlation methods -Output type <br>
 * -Sensors to correlate<br>
 * 
 * @author Dmitry Grudzinskiy
 * 
 */
public final class Configuration {

	private static final String ROOT = "CommisioningTool";

	private static final String DATA_ACCESS = ROOT + "/DataAccess";
	private static final String ENV_PROPERTY = DATA_ACCESS + "/property";

	private static final String CORRELATIONS = ROOT + "/Correlations";
	private static final String CORRELATION = CORRELATIONS + "/Correlation";
	private static final String CORRELATION_PROPERTY = CORRELATION + "/property";

	private static final String CLASSIFICATION = ROOT + "/Classification";
	private static final String CLASSIFICATION_PROPERTY = CLASSIFICATION + "/property";

	private static final String CONTROLLABLE = ROOT + "/Controllable";
	private static final String CONTROLLABLE_OBJECT = CONTROLLABLE + "/Object";
	private static final String CONTROLLABLE_OBJECT_FILTER = CONTROLLABLE_OBJECT + "/filter";

	private static final String CONTROLLED = ROOT + "/Controlled";
	private static final String CONTROLLED_OBJECT = CONTROLLED + "/Object";
	private static final String CONTROLLED_OBJECT_FILTER = CONTROLLED_OBJECT + "/filter";

	private static final String EXPORTER = ROOT + "/Exporter";
	private static final String EXPORTER_PROPERTY = EXPORTER + "/property";

	private static final String NAME_ATTR = "name";
	private static final String VALUE_ATTR = "value";
	private static final String PROPERTY_ATTR = "property";
	private static final String TYPE_ATTR = "type";
	private static final String TO_ATTR = "to";

	public static Configuration load(String src) throws ConfigurationException {

		Digester digester = new Digester();
		digester.setValidating(false);
		digester.addObjectCreate(ROOT, "com.synapsense.ct.conf.Configuration");

		// Load Data Access implementation and ES properties
		processDataAccess(digester);

		// Load all correlation methods to run
		processCorrelations(digester);

		// Load classification method
		processClassification(digester);

		// Load Exporter
		processExporter(digester);

		// Load description of the objects to correlate
		processObjects(digester);

		try {
			return (Configuration) digester.parse(new File(src));
		} catch (Exception e) {
			throw new ConfigurationException(e.getMessage(), e);
		}
	}

	private static void processDataAccess(Digester digester) {

		final Properties envProps = new Properties();
		digester.addRule(ENV_PROPERTY, new Rule() {

			@Override
			public void begin(Attributes attributes) throws Exception {
				String name = attributes.getValue(NAME_ATTR);
				String value = attributes.getValue(VALUE_ATTR);
				if (name == null || value == null) {
					throw new ConfigurationException("Unable to parse environment property");
				}
				envProps.setProperty(name, value);
			}
		});

		digester.addRule(DATA_ACCESS, new Rule() {

			@Override
			public void begin(Attributes attributes) throws Exception {
				String className = attributes.getValue("class");
				if (className == null) {
					throw new ConfigurationException("No class attribute for Data Access element found");
				}
				getDigester().push((DataAccess) Class.forName(className).newInstance());
			}

			@Override
			public void end() throws Exception {
				DataAccess dataAccess = (DataAccess) getDigester().pop();
				try {
					Method setEnv = dataAccess.getClass().getMethod("setEnvironment", Environment.class);
					InitialContext context = new InitialContext(envProps);
					Environment env = ServerLoginModule.getProxy(context, JNDINames.ENVIRONMENT, Environment.class);
					ServerLoginModule.login(envProps.getProperty("jboss.user.name"),
					        envProps.getProperty("jboss.user.password"));

					setEnv.invoke(dataAccess, env);
				} catch (NoSuchMethodException e) {
					// If no setEnvironment method then just not set it
				}

				Configuration conf = (Configuration) this.getDigester().getRoot();
				conf.setDataAccess(dataAccess);
			}
		});
	}

	private static void processCorrelations(Digester digester) {
		digester.addRule(CORRELATION, new Rule() {

			@Override
			public void begin(Attributes attributes) throws Exception {
				String name = attributes.getValue("name");
				String className = attributes.getValue("class");
				if (name == null || className == null) {
					throw new ConfigurationException("Unable to parse correlation method");
				}
				getDigester()
				        .push((Correlator) Class.forName(className).getConstructor(String.class).newInstance(name));
			}

			@Override
			public void end() throws Exception {
				Configuration conf = (Configuration) getDigester().getRoot();
				conf.addCorrelator((Correlator) getDigester().pop());
			}
		});
		digester.addSetProperty(CORRELATION_PROPERTY, NAME_ATTR, VALUE_ATTR);
	}

	private static void processClassification(Digester digester) {
		digester.addRule(CLASSIFICATION, new Rule() {

			@Override
			public void begin(Attributes attributes) throws Exception {
				String className = attributes.getValue("class");
				if (className == null) {
					throw new ConfigurationException("Unable to parse classification method");
				}
				getDigester().push((Classification) Class.forName(className).newInstance());
			}

			@Override
			public void end() throws Exception {
				Configuration conf = (Configuration) getDigester().getRoot();
				conf.setClassification((Classification) getDigester().pop());
			}
		});
		digester.addSetProperty(CLASSIFICATION_PROPERTY, NAME_ATTR, VALUE_ATTR);
	}

	private static void processExporter(Digester digester) {
		digester.addRule(EXPORTER, new Rule() {
			@Override
			public void begin(Attributes attributes) throws Exception {
				String className = attributes.getValue("class");
				if (className == null) {
					throw new ConfigurationException("Unable to parse classification method");
				}
				Exporter exp = (Exporter) Class.forName(className).newInstance();
				getDigester().push(exp);

				((Configuration) getDigester().getRoot()).setExporter(exp);
			}
		});
		digester.addSetProperty(EXPORTER_PROPERTY, NAME_ATTR, VALUE_ATTR);
	}

	private static void processObjects(Digester digester) {
		digester.addRule(CONTROLLABLE_OBJECT, new ObjectRule() {
			@Override
			public void end() throws Exception {
				((Configuration) getDigester().getRoot()).addControllable((ControlObjectDescription) getDigester()
				        .pop());
			}
		});
		digester.addRule(CONTROLLABLE_OBJECT_FILTER, new FilterRule());

		digester.addRule(CONTROLLED_OBJECT, new ObjectRule() {
			@Override
			public void end() throws Exception {
				((Configuration) getDigester().getRoot()).addControlled((ControlObjectDescription) getDigester().pop());
			}
		});
		digester.addRule(CONTROLLED_OBJECT_FILTER, new FilterRule());
	}

	private static class ObjectRule extends Rule {

		@Override
		public void begin(Attributes attributes) throws Exception {
			ControlObjectDescription descr = new ControlObjectDescription();
			getDigester().push(descr);
			String to = attributes.getValue(TO_ATTR);
			if (to != null) {
				descr.setTo(to);
				return;
			}
			String type = attributes.getValue(TYPE_ATTR);
			if (type == null) {
				throw new ConfigurationException("Nor to neither object type specified for an object");
			}
			descr.setTypeName(type);
			descr.setPropertyName(attributes.getValue(PROPERTY_ATTR));
		}
	}

	private static class FilterRule extends Rule {

		@Override
		public void begin(Attributes attributes) throws Exception {
			String name = attributes.getValue(PROPERTY_ATTR);
			String type = attributes.getValue(TYPE_ATTR);
			String value = attributes.getValue(VALUE_ATTR);
			if (name == null || type == null || value == null) {
				throw new ConfigurationException("Missing one of the required filter properties: property, type, value");
			}

			Object typedValue;

			if (type.equals("com.synapsense.dto.TO")) {
				TOFactory tof = TOFactory.getInstance();
				typedValue = tof.loadTO(value);
			} else if (type.equals("java.lang.String")) {
				typedValue = value;
			} else {
				Class<?> valueClass = Class.forName(type);
				Method valueOf = valueClass.getDeclaredMethod("valueOf", String.class);
				typedValue = valueOf.invoke(valueClass.newInstance(), value);
			}

			ControlObjectDescription descr = (ControlObjectDescription) getDigester().peek();
			descr.addFilter(name, typedValue);
		}
	}

	private DataAccess dataAccess;

	private List<Correlator> correlators = new ArrayList<Correlator>();

	private Classification classification;

	private List<ControlObjectDescription> controllable = new ArrayList<ControlObjectDescription>();

	private List<ControlObjectDescription> controlled = new ArrayList<ControlObjectDescription>();

	private Exporter exporter;

	public void addControllable(ControlObjectDescription descr) {
		controllable.add(descr);
	}

	public void addControlled(ControlObjectDescription descr) {
		controlled.add(descr);
	}

	public List<ControlObjectDescription> getControllable() {
		return controllable;
	}

	public List<ControlObjectDescription> getControlled() {
		return controlled;
	}

	public Classification getClassification() {
		return classification;
	}

	public void setClassification(Classification classification) {
		this.classification = classification;
	}

	public DataAccess getDataAccess() {
		return dataAccess;
	}

	public List<Correlator> getCorrelators() {
		return correlators;
	}

	public void addCorrelator(Correlator correlator) {
		correlators.add(correlator);
	}

	public void setDataAccess(DataAccess dataAccess) {
		this.dataAccess = dataAccess;
	}

	public Exporter getExporter() {
		return exporter;
	}

	public void setExporter(Exporter exporter) {
		this.exporter = exporter;
	}
}
