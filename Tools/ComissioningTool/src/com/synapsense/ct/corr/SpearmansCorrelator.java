package com.synapsense.ct.corr;

import org.apache.commons.math.stat.correlation.SpearmansCorrelation;

public class SpearmansCorrelator extends ClassicCorrelator {

	public SpearmansCorrelator(String name) {
		super(name);
	}

	@Override
	public double correlateArrays(double[] x, double[] y) {
		return new SpearmansCorrelation().correlation(x, y);
	}
}
