package com.synapsense.ct.corr;

import org.apache.commons.math.stat.correlation.PearsonsCorrelation;

public class PearsonsCorrelator extends ClassicCorrelator {

	public PearsonsCorrelator(String name) {
		super(name);
	}

	@Override
	public double correlateArrays(double[] x, double[] y) {
		return new PearsonsCorrelation().correlation(x, y);
	}
}
