package com.synapsense.ct.corr;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.synapsense.ct.utils.CollectionUtils;
import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;

public abstract class Correlator {

	private String name;

	private boolean sampling = false;
	private int curSample = 0;

	private TO<?> kernelTO;

	private List<ValueTO> kernelValues;

	private Map<TO<?>, List<ValueTO>> affectedValues;

	public Correlator(String name) {
		this.name = name;
		kernelValues = CollectionUtils.newArrayList();
		affectedValues = CollectionUtils.newLinkedHashMap();
	}

	public void addAffected(TO<?> id) {
		if (sampling) {
			throw new IllegalStateException("Unable to add affected object when sampling");
		}
		affectedValues.put(id, new ArrayList<ValueTO>());
	}

	public void nextSample() {
		if (sampling) {
			closeSample();
			curSample++;
		} else {
			sampling = true;
		}
	}

	public void setKernelValue(ValueTO value) {
		if (!sampling) {
			throw new IllegalStateException("Unable to add value while not sampling");
		}
		if (kernelValues.size() == curSample + 1) {
			kernelValues.set(curSample, value);
		} else {
			assert kernelValues.size() == curSample;
			kernelValues.add(value);
		}
	}

	public void setKernelValue(double value) {
		setKernelValue(new ValueTO("sample", value));
	}

	public void setAffectedValue(TO<?> affId, ValueTO value) {
		if (!sampling) {
			throw new IllegalStateException("Unable to add value while not sampling");
		}
		List<ValueTO> affVal = affectedValues.get(affId);
		if (affVal == null) {
			throw new IllegalArgumentException("No affected object with name " + affId + " found");
		}

		if (affVal.size() == curSample + 1) {
			affVal.set(curSample, value);
		} else {
			assert affVal.size() == curSample;
			affVal.add(value);
		}
	}

	public void setAffectedValue(TO<?> affName, Double value) {
		setAffectedValue(affName, new ValueTO("sample", value, new Date().getTime()));
	}

	public void setAllAffectedValuesTO(Map<TO<?>, ValueTO> values) {
		for (Map.Entry<TO<?>, ValueTO> entry : values.entrySet()) {
			setAffectedValue(entry.getKey(), entry.getValue());
		}
	}

	public void setAllAffectedValues(Map<TO<?>, Double> values) {
		for (Map.Entry<TO<?>, Double> entry : values.entrySet()) {
			setAffectedValue(entry.getKey(), entry.getValue());
		}
	}

	public int getCompletedSamplesNumber() {
		return sampling ? curSample : 0;
	}

	public Map<TO<?>, ValueTO> getAffectedSampleValuesTO(int sample) {
		if (sample >= curSample || sample < 0) {
			throw new IllegalArgumentException("Sample index");
		}
		Map<TO<?>, ValueTO> res = CollectionUtils.newHashMap();
		for (Map.Entry<TO<?>, List<ValueTO>> affValEntry : affectedValues.entrySet()) {
			res.put(affValEntry.getKey(), affValEntry.getValue().get(sample));
		}
		return res;
	}

	public Map<TO<?>, Double> getAffectedSampleValues(int sample) {
		if (sample >= curSample || sample < 0) {
			throw new IllegalArgumentException("Sample index");
		}
		Map<TO<?>, Double> res = CollectionUtils.newLinkedHashMap();
		for (Map.Entry<TO<?>, List<ValueTO>> affValEntry : affectedValues.entrySet()) {
			res.put(affValEntry.getKey(), (Double) affValEntry.getValue().get(sample).getValue());
		}
		return res;
	}

	public ValueTO getKernelSampleValueTO(int sample) {
		if (sample >= curSample || sample < 0) {
			throw new IllegalArgumentException("Sample index");
		}
		return kernelValues.get(sample);
	}

	public Double getKernelSampleValue(int sample) {
		if (sample >= curSample || sample < 0) {
			throw new IllegalArgumentException("Sample index");
		}
		return (Double) kernelValues.get(sample).getValue();
	}

	public void clearValues() {
		sampling = false;
		kernelValues.clear();
		for (List<ValueTO> value : affectedValues.values()) {
			value.clear();
		}
		curSample = 0;
	}

	public List<ValueTO> getKernelValuesTO() {
		return new ArrayList<ValueTO>(kernelValues);
	}

	public List<Double> getKernelValues() {
		List<Double> result = new ArrayList<Double>();
		for (ValueTO valueTO : kernelValues) {
			result.add((Double) valueTO.getValue());
		}
		return result;
	}

	public Map<TO<?>, List<ValueTO>> getAffectedValuesTO() {
		Map<TO<?>, List<ValueTO>> res = CollectionUtils.newLinkedHashMap();
		for (Map.Entry<TO<?>, List<ValueTO>> affValEntry : affectedValues.entrySet()) {
			res.put(affValEntry.getKey(), new ArrayList<ValueTO>(affValEntry.getValue()));
		}
		return res;
	}

	public Map<TO<?>, List<Double>> getAffectedValues() {
		Map<TO<?>, List<Double>> res = CollectionUtils.newLinkedHashMap();
		for (Map.Entry<TO<?>, List<ValueTO>> affValEntry : affectedValues.entrySet()) {
			List<Double> values = new ArrayList<Double>();
			for (ValueTO valueTO : affValEntry.getValue()) {
				values.add((Double) valueTO.getValue());
			}
			res.put(affValEntry.getKey(), values);
		}
		return res;
	}

	public boolean isSampling() {
		return sampling;
	}

	public int getCurSample() {
		return curSample;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public TO<?> getKernelTO() {
		return kernelTO;
	}

	public void setKernelTO(TO<?> kernelTO) {
		this.kernelTO = kernelTO;
	}

	public abstract CorrelationResult correlate();

	protected void closeSample() {
		closeArray(curSample, kernelValues);
		for (Map.Entry<TO<?>, List<ValueTO>> affValEntry : affectedValues.entrySet()) {
			closeArray(curSample, affValEntry.getValue());
		}
	}

	private void closeArray(int size, List<ValueTO> array) {
		int arraySize = array.size();
		if (arraySize != size + 1) {
			assert arraySize == size;
			if (size == 0) {
				array.add(new ValueTO("sample", 0.0, new Date().getTime()));
			} else {
				array.add(array.get(size - 1));
			}
		}
	}
}
