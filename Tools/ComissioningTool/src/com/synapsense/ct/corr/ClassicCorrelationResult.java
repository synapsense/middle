package com.synapsense.ct.corr;

import java.text.DecimalFormat;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import com.synapsense.ct.export.ExportBuilder;
import com.synapsense.ct.utils.CollectionUtils;
import com.synapsense.dto.TO;

public class ClassicCorrelationResult implements CorrelationResult {

	private TO<?> kernel;

	private Map<TO<?>, Double> dependent = new TreeMap<TO<?>, Double>(new TOComparator());

	public ClassicCorrelationResult(TO<?> kernel) {
		this.kernel = kernel;
	}

	@Override
	public void buildExporter(ExportBuilder builder) {
		builder.openRegion(kernel.toString(), new HashMap<String, String>());
		for (Map.Entry<TO<?>, Double> dep : dependent.entrySet()) {
			Map<String, String> attr = CollectionUtils.newHashMap();
			attr.put("correlation", new DecimalFormat("0.##").format(dep.getValue()));
			builder.addDependent(dep.getKey().toString(), attr);
		}
		builder.closeRegion();
	}

	@Override
	public Map<TO<?>, Collection<TO<?>>> getDependentObjects() {
		Map<TO<?>, Collection<TO<?>>> map = CollectionUtils.newHashMap();
		map.put(kernel, Collections.unmodifiableCollection(dependent.keySet()));
		return map;

	}

	public void addDependent(TO<?> to, Double corrValue) {
		dependent.put(to, corrValue);
	}

	public void addAllDependent(Map<TO<?>, Double> all) {
		dependent.putAll(all);
	}

	public TO<?> getKernel() {
		return kernel;
	}
}
