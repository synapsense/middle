package com.synapsense.ct.corr;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import com.synapsense.ct.export.ExportBuilder;
import com.synapsense.ct.utils.CollectionUtils;
import com.synapsense.dto.TO;

public class CompositeCorrelationResult implements CorrelationResult {

	private List<CorrelationResult> results = new ArrayList<CorrelationResult>();

	public void addCorrelationResult(CorrelationResult result) {
		results.add(result);
	}

	@Override
	public void buildExporter(ExportBuilder builder) {
		for (CorrelationResult cor : results) {
			cor.buildExporter(builder);
		}
	}

	@Override
	public Map<TO<?>, Collection<TO<?>>> getDependentObjects() {
		Map<TO<?>, Collection<TO<?>>> map = CollectionUtils.newHashMap();
		for (CorrelationResult cor : results) {
			map.putAll(cor.getDependentObjects());
		}
		return map;
	}
}
