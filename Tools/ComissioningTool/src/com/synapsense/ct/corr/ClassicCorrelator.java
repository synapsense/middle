package com.synapsense.ct.corr;

import static com.synapsense.ct.utils.MathUtils.filterNoises;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import com.synapsense.ct.utils.CollectionUtils;
import com.synapsense.ct.utils.MathUtils;
import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;

public abstract class ClassicCorrelator extends Correlator {

	private double dependencyThreshold;

	private double noiseThreshold;

	private int maxRegionSize;

	public ClassicCorrelator(String name) {
		super(name);
	}

	@Override
	public CorrelationResult correlate() {

		if (getKernelTO() == null) {
			throw new IllegalStateException("KernelTO should be set before correlating");
		}

		closeSample();
		ClassicCorrelationResult result = new ClassicCorrelationResult(getKernelTO());
		Map<TO<?>, Double> allDependant = CollectionUtils.newHashMap();
		double[] kernelArray = filterNoises(CollectionUtils.collectionToDoubleArray(getKernelValuesTO()),
		        noiseThreshold);
		for (Map.Entry<TO<?>, List<ValueTO>> affValEntry : getAffectedValuesTO().entrySet()) {
			double[] affArray = filterNoises(CollectionUtils.collectionToDoubleArray(affValEntry.getValue()),
			        noiseThreshold);
			double corr = correlateArrays(kernelArray, affArray);
			if (corr > dependencyThreshold) {
				allDependant.put(affValEntry.getKey(), corr);
			}
		}
		if (maxRegionSize <= 0) {
			result.addAllDependent(allDependant);
		} else {
			result.addAllDependent(MathUtils.getMostValuable(allDependant, maxRegionSize));
		}
		return result;
	}

	public Collection<Double> getCorrelation() {
		closeSample();
		Collection<Double> values = CollectionUtils.newArrayList();
		double[] kernelArray = CollectionUtils.collectionToDoubleArray(getKernelValuesTO());
		for (Map.Entry<TO<?>, List<ValueTO>> affValEntry : getAffectedValuesTO().entrySet()) {
			double[] affArray = CollectionUtils.collectionToDoubleArray(affValEntry.getValue());
			double corr = correlateArrays(kernelArray, affArray);
			values.add(corr);
		}
		return values;
	}

	public double getThreshold() {
		return dependencyThreshold;
	}

	public double getNoiseThreshold() {
		return noiseThreshold;
	}

	public void setNoiseThreshold(double noiseThreshold) {
		this.noiseThreshold = noiseThreshold;
	}

	public double getDependencyThreshold() {
		return dependencyThreshold;
	}

	public int getMaxRegionSize() {
		return maxRegionSize;
	}

	public void setMaxRegionSize(int maxRegionSize) {
		this.maxRegionSize = maxRegionSize;
	}

	public void setDependencyThreshold(double dependencyThreshold) {
		if (dependencyThreshold < -1 || dependencyThreshold > 1) {
			throw new IllegalArgumentException("Threshold must be in [-1, 1]");
		}
		this.dependencyThreshold = dependencyThreshold;
	}

	public abstract double correlateArrays(double[] x, double[] y);
}
