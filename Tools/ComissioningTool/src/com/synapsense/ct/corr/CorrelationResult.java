package com.synapsense.ct.corr;

import java.util.Collection;
import java.util.Comparator;
import java.util.Map;

import com.synapsense.ct.export.ExportBuilder;
import com.synapsense.dto.TO;

public interface CorrelationResult {

	Map<TO<?>, Collection<TO<?>>> getDependentObjects();

	void buildExporter(ExportBuilder builder);

	class TOComparator implements Comparator<TO<?>> {

		@Override
		public int compare(TO<?> o1, TO<?> o2) {
			Integer id1 = (Integer) o1.getID();
			Integer id2 = (Integer) o2.getID();

			if (id1 > id2) {
				return 1;
			}

			if (id1 < id2) {
				return -1;
			}

			return 0;
		}
	}
}
