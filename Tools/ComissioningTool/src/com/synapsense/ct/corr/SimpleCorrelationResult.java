package com.synapsense.ct.corr;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import com.synapsense.ct.export.ExportBuilder;
import com.synapsense.ct.utils.CollectionUtils;
import com.synapsense.dto.TO;

public class SimpleCorrelationResult implements CorrelationResult {

	private TO<?> kernel;

	private Set<TO<?>> dependent = new TreeSet<TO<?>>(new TOComparator());

	public SimpleCorrelationResult(TO<?> kernel) {
		this.kernel = kernel;
	}

	public void addDependent(TO<?> to) {
		dependent.add(to);
	}

	@Override
	public Map<TO<?>, Collection<TO<?>>> getDependentObjects() {
		Map<TO<?>, Collection<TO<?>>> map = CollectionUtils.newHashMap();
		map.put(kernel, Collections.unmodifiableCollection(dependent));
		return map;
	}

	@Override
	public void buildExporter(ExportBuilder builder) {
		builder.openRegion(kernel.toString(), new HashMap<String, String>());
		for (TO<?> dep : dependent) {
			builder.addDependent(dep.toString(), new HashMap<String, String>());
		}
		builder.closeRegion();
	}

	public TO<?> getKernel() {
		return kernel;
	}
}
