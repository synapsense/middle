package com.synapsense.ct.corr;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;

public class TrendFinder extends Correlator {

	private double minMagnitude;

	private long minDuration;

	private double noiseThreshold;

	public TrendFinder(String name) {
		super(name);
	}

	@Override
	public CorrelationResult correlate() {
		if (getKernelTO() == null) {
			throw new IllegalStateException("KernelTO should be set before correlating");
		}

		SimpleCorrelationResult result = new SimpleCorrelationResult(getKernelTO());
		Map<TO<?>, List<ValueTO>> allValues = getAffectedValuesTO();
		for (Map.Entry<TO<?>, List<ValueTO>> mapEntry : allValues.entrySet()) {
			if (isTrend(mapEntry.getValue())) {
				result.addDependent(mapEntry.getKey());
			}
		}
		return result;
	}

	public boolean isTrend(List<ValueTO> valuesTos) {
		return isTrend(0, valuesTos);
	}

	private boolean isTrend(int index, List<ValueTO> valueTos) {
		if (index >= valueTos.size() - 1) {
			return false;
		}

		// Trying to find increasing trend starting with this index
		int incrTrend = tryFindTrend(index, valueTos, new TrendCondition() {

			@Override
			public boolean check(List<ValueTO> list, int index1, int index2) {
				return getDoubleValueMinusNoise(list, index2) > getDoubleValue(list, index1);
			}
		});

		if (incrTrend == 0) {
			// found increasing trend
			return true;
		}

		// Trying to find decreasing trend starting with this index
		int decrTrend = tryFindTrend(index, valueTos, new TrendCondition() {

			@Override
			public boolean check(List<ValueTO> list, int index1, int index2) {
				return getDoubleValuePlusNoise(list, index2) < getDoubleValue(list, index1);
			}
		});

		if (decrTrend == 0) {
			// found decreasing trend
			return true;
		}

		int nextIndex = Math.max(decrTrend, incrTrend);
		if (nextIndex == index) {
			nextIndex++;
		}

		return isTrend(nextIndex, valueTos);
	}

	private int tryFindTrend(int index, List<ValueTO> valueTos, TrendCondition trendCondition) {
		List<ValueTO> trend = new ArrayList<ValueTO>();
		trend.add(valueTos.get(index));
		while (index < valueTos.size() - 1 && trendCondition.check(valueTos, index, index + 1)) {
			trend.add(valueTos.get(index + 1));
			index++;
		}
		if (checkTrend(trend)) {
			return 0;
		}
		return index + 1;
	}

	private boolean checkTrend(List<ValueTO> trend) {
		if (Math.abs(getDoubleValue(trend, 0) - getDoubleValue(trend, trend.size() - 1)) < minMagnitude) {
			return false;
		}
		if (Math.abs(getTimeStamp(trend, 0) - getTimeStamp(trend, trend.size() - 1)) < minDuration) {
			return false;
		}
		return true;
	}

	public double getMinMagnitude() {
		return minMagnitude;
	}

	public void setMinMagnitude(double minMagnitude) {
		this.minMagnitude = minMagnitude;
	}

	public long getMinDuration() {
		return minDuration;
	}

	public void setMinDuration(long minDuration) {
		this.minDuration = minDuration;
	}

	public double getNoiseThreshold() {
		return noiseThreshold;
	}

	public void setNoiseThreshold(double noiseThreshold) {
		this.noiseThreshold = noiseThreshold;
	}

	private double getDoubleValue(List<ValueTO> list, int index) {
		return (Double) list.get(index).getValue();
	}

	private double getDoubleValueMinusNoise(List<ValueTO> list, int index) {
		return getDoubleValue(list, index) - noiseThreshold;
	}

	private double getDoubleValuePlusNoise(List<ValueTO> list, int index) {
		return getDoubleValue(list, index) + noiseThreshold;
	}

	private long getTimeStamp(List<ValueTO> list, int index) {
		return list.get(index).getTimeStamp();
	}

	private interface TrendCondition {

		public boolean check(List<ValueTO> list, int index1, int index2);
	}
}
