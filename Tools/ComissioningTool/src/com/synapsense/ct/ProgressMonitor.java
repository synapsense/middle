package com.synapsense.ct;

import com.synapsense.ct.corr.CorrelationResult;

public interface ProgressMonitor {

	void setProgress(double progress);

	void setMessage(String message);

	void fail(Throwable e);

	void complete(CorrelationResult result);
}
