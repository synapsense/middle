package com.synapsense.ct;

public class CTException extends Exception {

	private static final long serialVersionUID = 7202603662800251824L;

	public CTException() {
	}

	public CTException(String message) {
		super(message);
	}

	public CTException(Throwable cause) {
		super(cause);
	}

	public CTException(String message, Throwable cause) {
		super(message, cause);
	}
}
