package com.synapsense.ct;

import java.util.Collection;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.log4j.xml.DOMConfigurator;

import com.synapsense.ct.classification.Classification;
import com.synapsense.ct.conf.Configuration;
import com.synapsense.ct.conf.ConfigurationException;
import com.synapsense.ct.corr.CorrelationResult;
import com.synapsense.ct.corr.Correlator;
import com.synapsense.ct.data.DataAccess;
import com.synapsense.ct.export.ExportException;
import com.synapsense.ct.export.Exporter;
import com.synapsense.dto.TO;

public class CommissioningTool {

	private static Log log = LogFactory.getLog(CommissioningTool.class);

	public static void main(String[] args) {

		DOMConfigurator.configure("./lib/log4j.xml");

		if (args.length != 1) {
			System.err.println("Number of input arguments must equal to 1");
			System.exit(-1);
		}

		// Load xml configuration file
		Configuration configuration;
		try {

			configuration = Configuration.load(args[0]);

		} catch (ConfigurationException e) {
			log.error("Unable to load configuration", e);
			System.exit(-1);
			return;
		}

		DataAccess access = configuration.getDataAccess();
		if (access == null) {
			log.error("Data access not loaded");
			System.exit(-1);
		}

		Classification classifier = configuration.getClassification();
		if (classifier == null) {
			log.error("Classification not loaded");
			System.exit(-1);
		}
		classifier.setDataAccess(access);

		final Exporter exporter = configuration.getExporter();
		if (exporter == null) {
			log.error("Exporter not loaded");
			System.exit(-1);
		}
		try {
			Collection<TO<?>> controllableSensors = access.getSensorsByDescription(configuration.getControllable());
			Collection<TO<?>> controlledSensors = access.getSensorsByDescription(configuration.getControlled());

			List<Correlator> correlators = configuration.getCorrelators();
			for (final Correlator correlator : correlators) {
				classifier.setCorrelator(correlator);

				classifier.classify(controllableSensors, controlledSensors, new ProgressMonitor() {

					@Override
					public void complete(CorrelationResult result) {
						log.info("Classification completed, exporting results");
						try {
							result.buildExporter(exporter);
							exporter.export(correlator.getName());
							log.info("Results have been exported");
						} catch (ExportException e) {
							log.error("Unable to export results", e);
						}
					}

					@Override
					public void fail(Throwable e) {
						log.error("Classification failed", e);
					}

					@Override
					public void setMessage(String message) {
						log.info(message);
					}

					@Override
					public void setProgress(double progress) {
						log.debug("Progress " + progress);
					}
				});
			}
		} catch (CTException e) {
			log.error(e.getMessage(), e);
		}
		log.info("Commisioning tool work completed");
	}
}
