package com.synapsense.ct.export;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;

public class XMLExporter implements Exporter {

	private static final String LINE_SEPARATOR = System.getProperty("line.separator");

	private String outputDir;

	private StringBuilder xmlBuilder = new StringBuilder();

	@Override
	public void export(String name) throws ExportException {

		BufferedWriter writer;
		try {
			writer = new BufferedWriter(new FileWriter(outputDir + "/" + name + ".xml"));
		} catch (IOException e) {
			throw new ExportException(e.getMessage(), e);
		}

		xmlBuilder.insert(0, "<regions>").append(LINE_SEPARATOR);
		xmlBuilder.append("</regions>").append(LINE_SEPARATOR);

		try {
			writer.write(xmlBuilder.toString());
			writer.close();
			xmlBuilder.setLength(0);
		} catch (IOException e) {
			throw new ExportException(e.getMessage(), e);
		}
	}

	@Override
	public void addDependent(String id, Map<String, String> attributes) {
		appendElement("element", id, attributes);
		xmlBuilder.append("/>");
		xmlBuilder.append(LINE_SEPARATOR);
	}

	@Override
	public void openRegion(String id, Map<String, String> attributes) {
		appendElement("region", id, attributes);
		xmlBuilder.append(">");
		xmlBuilder.append(LINE_SEPARATOR);
	}

	@Override
	public void closeRegion() {
		xmlBuilder.append("</region>").append(LINE_SEPARATOR);
	}

	public String getOutputDir() {
		return outputDir;
	}

	public void setOutputDir(String outputDir) {
		this.outputDir = outputDir;
	}

	private void appendElement(String name, String id, Map<String, String> attributes) {
		xmlBuilder.append("<" + name + " id=\"").append(id).append("\" ");
		for (Map.Entry<String, String> attribute : attributes.entrySet()) {
			xmlBuilder.append(attribute.getKey() + "=\"" + attribute.getValue() + " \" ");
		}
	}
}
