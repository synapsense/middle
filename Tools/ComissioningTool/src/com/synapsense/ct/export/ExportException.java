package com.synapsense.ct.export;

import com.synapsense.ct.CTException;

public class ExportException extends CTException {

	private static final long serialVersionUID = -820038796018922974L;

	public ExportException() {
	}

	public ExportException(String message) {
		super(message);
	}

	public ExportException(Throwable cause) {
		super(cause);
	}

	public ExportException(String message, Throwable cause) {
		super(message, cause);
	}
}
