package com.synapsense.ct.export;

public interface Exporter extends ExportBuilder {

	public void export(String name) throws ExportException;
}
