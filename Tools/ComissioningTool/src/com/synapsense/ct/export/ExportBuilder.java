package com.synapsense.ct.export;

import java.util.Map;

public interface ExportBuilder {

	public void openRegion(String id, Map<String, String> attributes);

	public void closeRegion();

	public void addDependent(String id, Map<String, String> attributes);
}
