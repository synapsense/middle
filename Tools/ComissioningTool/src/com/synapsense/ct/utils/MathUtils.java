package com.synapsense.ct.utils;

import java.util.Comparator;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class MathUtils {

	public static long findClosest(Long[] values, Long val) {
		int left = 0;
		int right = values.length - 1;
		int mid = 0;
		do {
			mid = (left + right) / 2;
			if (values[mid] < val) {
				left = mid;
			} else {
				right = mid;
			}
		} while (left < right - 1);

		if (Math.abs(values[left] - val) > Math.abs(values[right] - val)) {
			return values[right];
		}
		return values[left];
	}

	public static double[] filterNoises(double[] array, double noiseThreshold) {
		for (int i = 1; i < array.length; i++) {
			if (array[i] < array[i - 1] + noiseThreshold && array[i] > array[i - 1] - noiseThreshold) {
				array[i] = array[i - 1];
			}
		}
		return array;
	}

	public static <V> Map<V, Double> getMostValuable(Map<V, Double> values, int maxSize) {
		if (maxSize >= values.size()) {
			return values;
		}
		Map<Double, Set<V>> reverted = new TreeMap<Double, Set<V>>(new Comparator<Double>() {

			@Override
			public int compare(Double o1, Double o2) {
				if (o1 > o2) {
					return -1;
				}
				if (o1 < o2) {
					return 1;
				}
				return 0;
			}
		});

		for (Map.Entry<V, Double> entry : values.entrySet()) {
			CollectionUtils.updateSetValuedMap(reverted, entry.getValue(), entry.getKey());
		}

		Map<V, Double> result = CollectionUtils.newLinkedHashMap();
		int i = 0;
		for (Map.Entry<Double, Set<V>> entry : reverted.entrySet()) {
			for (V v : entry.getValue()) {
				if (i >= maxSize) {
					break;
				}
				result.put(v, entry.getKey());
				i++;
			}
		}
		return result;
	}

	private MathUtils() {
	}
}
