package com.synapsense.ct.utils;

import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.naming.InitialContext;

import com.synapsense.ct.corr.ClassicCorrelator;
import com.synapsense.ct.corr.PearsonsCorrelator;
import com.synapsense.ct.corr.SpearmansCorrelator;
import com.synapsense.ct.corr.TrendFinder;
import com.synapsense.ct.data.EnvDataAccess;
import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;
import com.synapsense.service.Environment;
import com.synapsense.util.JNDINames;
import com.synapsense.util.ServerLoginModule;

public class CSVBuilder {

	public static void main(String[] args) {
		try {
			final Properties props = new Properties();
			props.load(new FileInputStream(args[0]));

			Environment env;

			InitialContext context = new InitialContext(props);
			env = ServerLoginModule.getProxy(context, JNDINames.ENVIRONMENT, Environment.class);
			ServerLoginModule.login(props.getProperty("jboss.user.name"), props.getProperty("jboss.user.password"));

			DateFormat df = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");

			Date start = df.parse(props.getProperty("date.start"));

			Date end = df.parse(props.getProperty("date.end"));

			String[] cracNames = props.getProperty("cracs.name").split(",");

			List<TO<?>> sensors = new ArrayList<TO<?>>();
			StringBuilder fLineBuilder = new StringBuilder();
			Collection<TO<?>> racks = env.getObjectsByType("RACK");
			fLineBuilder.append(";;");
			String lineSeparator = System.getProperty("line.separator");
			for (TO<?> rack : racks) {
				String name = env.getPropertyValue(rack, "name", String.class);
				fLineBuilder.append(name).append(";;;");
			}
			fLineBuilder.append(lineSeparator);
			fLineBuilder.append("Date").append(";").append("CRAC RAT").append(";");
			for (TO<?> rack : racks) {
				sensors.add(env.getPropertyValue(rack, "cBot", TO.class));
				sensors.add(env.getPropertyValue(rack, "cMid", TO.class));
				sensors.add(env.getPropertyValue(rack, "cTop", TO.class));
				fLineBuilder.append("cBot").append(";").append("cMid").append(";").append("cTop").append(";");
			}
			fLineBuilder.append(lineSeparator);

			EnvDataAccess access = new EnvDataAccess();
			access.setEnvironment(env);
			PearsonsCorrelator pearsonCorrelator = new PearsonsCorrelator("Pearson");
			pearsonCorrelator.setNoiseThreshold(0.5);
			SpearmansCorrelator spearmanCorrelator = new SpearmansCorrelator("Spearman");
			spearmanCorrelator.setNoiseThreshold(0.5);

			TrendFinder finder = new TrendFinder("Trend");
			finder.setNoiseThreshold(0.5);

			for (TO<?> sensor : sensors) {
				pearsonCorrelator.addAffected(sensor);
				spearmanCorrelator.addAffected(sensor);
			}

			for (String cracName : cracNames) {

				Collection<TO<?>> cracs = env.getObjects("CRAC", new ValueTO[] { new ValueTO("name", cracName) });

				for (TO<?> crac : cracs) {

					List<Pair<Date, List<Double>>> values = access.getHistoricalValueTable(crac, sensors, start, end);
					BufferedWriter writer = new BufferedWriter(new FileWriter(cracName + ".csv"));

					writer.write(fLineBuilder.toString());
					DecimalFormat decf = new DecimalFormat("0.####");

					for (Pair<Date, List<Double>> pair : values) {
						writer.write(df.format(pair.getFirst()) + ";");
						pearsonCorrelator.nextSample();
						spearmanCorrelator.nextSample();
						finder.nextSample();

						for (int i = 0; i < pair.getSecond().size(); i++) {

							Double val = pair.getSecond().get(i);

							writer.write(decf.format(val) + ";");
							if (i == 0) {
								pearsonCorrelator.setKernelValue(val);
								spearmanCorrelator.setKernelValue(val);
							} else {
								pearsonCorrelator.setAffectedValue(sensors.get(i - 1), val);
								spearmanCorrelator.setAffectedValue(sensors.get(i - 1), val);
							}
						}
						writer.newLine();
					}

					// Correlation
					writer.write(";PEARSON;");
					writeCorr(pearsonCorrelator, writer, decf);
					writer.write(";SPEARMAN;");
					writeCorr(spearmanCorrelator, writer, decf);
					writer.close();

					pearsonCorrelator.clearValues();
					spearmanCorrelator.clearValues();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static void writeCorr(ClassicCorrelator corr, BufferedWriter writer, DecimalFormat decf) throws IOException {
		Collection<Double> pCorr = corr.getCorrelation();
		for (Double p : pCorr) {
			if (p.isNaN()) {
				writer.write("NaN");
			} else if (p.isInfinite()) {
				writer.write("Infinite");
			} else {
				writer.write(decf.format(p));
			}
			writer.write(";");
		}
		writer.newLine();

	}
}
