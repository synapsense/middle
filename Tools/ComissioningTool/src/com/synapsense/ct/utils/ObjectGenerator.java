package com.synapsense.ct.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Collection;
import java.util.Properties;
import java.util.Set;

import javax.naming.InitialContext;

import com.synapsense.ct.CTException;
import com.synapsense.dto.CollectionTO;
import com.synapsense.dto.ObjectType;
import com.synapsense.dto.PropertyDescr;
import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.EnvException;
import com.synapsense.service.Environment;
import com.synapsense.util.JNDINames;
import com.synapsense.util.ServerLoginModule;

public class ObjectGenerator {

	public static void main(String[] args) {
		try {
			final Properties props = new Properties();
			try {
				props.load(new FileInputStream(args[0]));
			} catch (IOException e) {
				throw new CTException(e.getMessage(), e);
			}

			Environment env;

			try {
				InitialContext context = new InitialContext(props);
				env = ServerLoginModule.getProxy(context, JNDINames.ENVIRONMENT, Environment.class);
				ServerLoginModule.login(props.getProperty("jboss.user.name"), props.getProperty("jboss.user.password"));
			} catch (Exception e) {
				throw new CTException("Unnable to get environment", e);
			}

			String typeName = props.getProperty("sensor.type.name");

			try {
				env.createObjectType(typeName);
				ObjectType type = env.getObjectType(typeName);
				Set<PropertyDescr> descrs = type.getPropertyDescriptors();
				descrs.add(new PropertyDescr("sensor", TO.class));
				descrs.add(new PropertyDescr("controltype", String.class));
				env.updateObjectType(type);
			} catch (EnvException e) {
				throw new CTException("Unable to create object type " + typeName, e);
			}

			Collection<TO<?>> allRacks = env.getObjectsByType("RACK");
			Collection<CollectionTO> allProps = env.getPropertyValue(allRacks, new String[] { "cTop", "cBot", "cMid" });
			for (CollectionTO prop : allProps) {
				System.out.println("Creating objects for  " + prop.getObjId());
				for (ValueTO valueTo : prop.getPropValues()) {
					try {
						env.createObject(typeName, new ValueTO[] { new ValueTO("sensor", valueTo.getValue()),
						        new ValueTO("controltype", "temperature") });
					} catch (EnvException e) {
						throw new CTException("Unable to create object ", e);
					}
				}
			}
		} catch (CTException e) {
			e.printStackTrace();
		}
	}
}
