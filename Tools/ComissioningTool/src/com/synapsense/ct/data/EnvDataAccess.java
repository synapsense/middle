package com.synapsense.ct.data;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.synapsense.ct.conf.ControlObjectDescription;
import com.synapsense.ct.utils.CollectionUtils;
import com.synapsense.ct.utils.MathUtils;
import com.synapsense.ct.utils.Pair;
import com.synapsense.dto.CollectionTO;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.dto.ValueTO;
import com.synapsense.service.Environment;

public class EnvDataAccess implements DataAccess {

	protected Environment env;

	public void setEnvironment(Environment env) {
		this.env = env;
	}

	@Override
	public Map<TO<?>, ValueTO> getSensorsValues(Collection<TO<?>> sensors) throws DataAccessException {
		Collection<CollectionTO> collValues = env.getPropertyValue(sensors, new String[] { "lastValue" });
		Map<TO<?>, ValueTO> res = new LinkedHashMap<TO<?>, ValueTO>();
		for (CollectionTO collVal : collValues) {
			res.put(collVal.getObjId(), collVal.getSinglePropValue("lastValue"));
		}
		return res;
	}

	@Override
	public void setControllableValue(TO<?> crac, Double temp) throws DataAccessException {
		Collection<TO<?>> cracControlColl = env.getObjects("CONTROLALG_TRIMRESPOND ", new ValueTO[] { new ValueTO(
		        "crac", crac) });
		if (cracControlColl.size() != 1) {
			throw new DataAccessException("Unable to find exactly one CRAC control object for CRAC " + crac);
		}

		TO<?> cracControl = cracControlColl.iterator().next();
		try {
			env.setAllPropertiesValues(cracControl, new ValueTO[] { new ValueTO("mode", "manual"),
			        new ValueTO("manualOutput", temp) });
		} catch (Exception e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	@Override
	public List<Pair<Date, List<Double>>> getHistoricalValueTable(TO<?> cracSensor, Collection<TO<?>> sensors,
	        Date start, Date end) throws DataAccessException {
		try {

			Collection<ValueTO> cracValues = env.getHistory(cracSensor, "lastValue", start, end, Double.class);

			List<Pair<Date, List<Double>>> res = CollectionUtils.newArrayList();

			int i = 0;
			for (TO<?> sensor : sensors) {

				Map<Long, Double> sensorVals = createValueTreeMap(env.getHistory(sensor, "lastValue",
				        new Date(start.getTime() - 60000), new Date(end.getTime() + 60000), Double.class));

				int j = 0;
				for (ValueTO valueTo : cracValues) {
					long timestamp = valueTo.getTimeStamp();
					Pair<Date, List<Double>> pairValues;
					if (i == 0) {
						List<Double> vals = new ArrayList<Double>();
						vals.add((Double) valueTo.getValue());
						pairValues = CollectionUtils.newPair(new Date(timestamp), vals);
						res.add(pairValues);
					} else {
						pairValues = res.get(j);
					}
					if (!sensorVals.isEmpty()) {
						pairValues.getSecond().add(getClosestValue(sensorVals, timestamp));
					} else {
						pairValues.getSecond().add(0.0);
					}
					j++;
				}
				i++;
			}

			return res;
		} catch (Exception e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	@Override
	public Double getSensorValue(TO<?> sensor) throws DataAccessException {
		try {
			return env.getPropertyValue(sensor, "lastValue", Double.class);
		} catch (Exception e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	@Override
	public Collection<TO<?>> getSensorsByDescription(Collection<ControlObjectDescription> descColl)
	        throws DataAccessException {
		List<TO<?>> sensors = new ArrayList<TO<?>>();
		for (ControlObjectDescription descr : descColl) {
			if (descr.getTo() != null) {
				// Exact TO specified
				sensors.add(TOFactory.getInstance().loadTO(descr.getTo()));
				continue;
			}

			for (Map.Entry<String, Set<Object>> filter : descr.getFilters().entrySet()) {
				for (Object val : filter.getValue()) {
					ValueTO valueTO = new ValueTO(filter.getKey(), val);
					Collection<TO<?>> objects = env.getObjects(descr.getTypeName(), new ValueTO[] { valueTO });
					if (descr.getPropertyName() != null && !objects.isEmpty()) {
						Collection<CollectionTO> sensProps = env.getPropertyValue(objects,
						        new String[] { descr.getPropertyName() });
						for (CollectionTO sensProp : sensProps) {
							sensors.add((TO<?>) sensProp.getSinglePropValue(descr.getPropertyName()).getValue());
						}
					} else {
						sensors.addAll(objects);
					}
				}
			}
		}
		return sensors;
	}

	private double getClosestValue(Map<Long, Double> sensValues, Long toFind) {
		return sensValues.get(MathUtils.findClosest(sensValues.keySet().toArray(new Long[sensValues.size()]), toFind));
	}

	private Map<Long, Double> createValueTreeMap(Collection<ValueTO> values) {
		Map<Long, Double> res = CollectionUtils.newTreeMap();
		for (ValueTO valueTO : values) {
			res.put(valueTO.getTimeStamp(), (Double) valueTO.getValue());
		}
		return res;
	}
}
