package com.synapsense.ct.data;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.synapsense.ct.conf.ControlObjectDescription;
import com.synapsense.ct.utils.Pair;
import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;

public interface DataAccess {

	Collection<TO<?>> getSensorsByDescription(Collection<ControlObjectDescription> descColl) throws DataAccessException;

	void setControllableValue(TO<?> crac, Double temp) throws DataAccessException;

	Double getSensorValue(TO<?> sensor) throws DataAccessException;

	Map<TO<?>, ValueTO> getSensorsValues(Collection<TO<?>> sensors) throws DataAccessException;

	List<Pair<Date, List<Double>>> getHistoricalValueTable(TO<?> crac, Collection<TO<?>> sensors, Date start, Date end)
	        throws DataAccessException;
}
