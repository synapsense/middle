package com.synapsense.ct.data;

import com.synapsense.ct.CTException;

public class DataAccessException extends CTException {

	private static final long serialVersionUID = 1858679246124349517L;

	public DataAccessException() {
		super();
	}

	public DataAccessException(String message, Throwable cause) {
		super(message, cause);
	}

	public DataAccessException(String message) {
		super(message);
	}

	public DataAccessException(Throwable cause) {
		super(cause);
	}
}
