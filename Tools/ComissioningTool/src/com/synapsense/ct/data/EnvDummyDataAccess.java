package com.synapsense.ct.data;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import com.synapsense.ct.utils.CollectionUtils;
import com.synapsense.dto.TO;

public class EnvDummyDataAccess extends EnvDataAccess {

	private Map<TO<?>, List<TO<?>>> cracSensorsMap;

	public EnvDummyDataAccess(Collection<TO<?>> cracs, Collection<TO<?>> sensors) {
		buildMap(cracs, sensors);
	}

	@Override
	public void setControllableValue(TO<?> crac, Double temp) throws DataAccessException {
		try {
			env.setPropertyValue(crac, "supplyT", temp);
			List<TO<?>> cracSensors = cracSensorsMap.get(crac);
			if (cracSensors != null) {
				for (TO<?> sensor : cracSensors) {
					env.setPropertyValue(sensor, "lastValue", temp);
				}
			}
		} catch (Exception e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	private void buildMap(Collection<TO<?>> cracs, Collection<TO<?>> sensors) {
		cracSensorsMap = CollectionUtils.newHashMap();
		int part = sensors.size() / cracs.size();
		if (part == 0) {
			part = 1;
		}

		int i = 0;
		for (TO<?> crac : cracs) {
			List<TO<?>> cracSensors = new ArrayList<TO<?>>(sensors).subList(i * part,
			        Math.min((i + 1) * part, sensors.size()));
			cracSensorsMap.put(crac, cracSensors);
			i++;
		}
	}
}
