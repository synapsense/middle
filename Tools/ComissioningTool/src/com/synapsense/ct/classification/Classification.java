package com.synapsense.ct.classification;

import java.util.Collection;

import com.synapsense.ct.ProgressMonitor;
import com.synapsense.ct.corr.Correlator;
import com.synapsense.ct.data.DataAccess;
import com.synapsense.dto.TO;

public abstract class Classification {

	protected DataAccess dataAccess;

	protected Correlator correlator;

	public void setDataAccess(DataAccess dataAccess) {
		this.dataAccess = dataAccess;
	}

	public void setCorrelator(Correlator correlator) {
		this.correlator = correlator;
	}

	public abstract void classify(Collection<TO<?>> cracSensors, Collection<TO<?>> measuredSensors,
	        ProgressMonitor monitor);
}
