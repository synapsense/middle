package com.synapsense.ct.classification;

import java.util.Collection;

import com.synapsense.ct.ProgressMonitor;
import com.synapsense.ct.corr.CompositeCorrelationResult;
import com.synapsense.ct.corr.CorrelationResult;
import com.synapsense.ct.corr.Correlator;
import com.synapsense.ct.data.DataAccess;
import com.synapsense.dto.TO;

public class OnlineClassification extends Classification {

	private long settleInterval;

	private double startCalibration;

	private double calibrationStep;

	private int stepsNumber;

	@Override
	public void classify(Collection<TO<?>> controllable, Collection<TO<?>> controlled, ProgressMonitor monitor) {
		try {
			CompositeCorrelationResult allResults = new CompositeCorrelationResult();
			for (TO<?> sensor : controlled) {
				correlator.addAffected(sensor);
			}

			monitor.setMessage("Setting all the controllables to their initial value");

			for (TO<?> obj : controllable) {
				dataAccess.setControllableValue(obj, startCalibration);
			}
			monitor.setProgress(0.1);
			monitor.setMessage("Waiting for data center to settle");

			waitToSettle(settleInterval);

			double cracNumber = 0;
			for (TO<?> cracSensor : controllable) {

				correlator.setKernelTO(cracSensor);
				correlator.clearValues();
				monitor.setMessage("Started building region for Object \"" + cracSensor + "\"");

				correlator.nextSample();
				correlator.setKernelValue(startCalibration);
				correlator.setAllAffectedValuesTO(dataAccess.getSensorsValues(controlled));

				for (int i = 0; i < stepsNumber; i++) {

					double nextValue = startCalibration + calibrationStep * (i + 1);

					monitor.setMessage("Updating controllable value");
					dataAccess.setControllableValue(cracSensor, nextValue);

					monitor.setMessage("Waiting for data center to settle");
					waitToSettle(settleInterval);

					monitor.setMessage("Reading sample values for object \"" + cracSensor + "\", sample " + (i + 1));
					correlator.nextSample();
					correlator.setKernelValue(dataAccess.getSensorValue(cracSensor));
					correlator.setAllAffectedValuesTO(dataAccess.getSensorsValues(controlled));
				}

				monitor.setMessage("Correlating sensor values for object \"" + cracSensor + "\"");

				CorrelationResult result = correlator.correlate();
				allResults.addCorrelationResult(result);
				cracNumber += 1.0;
				monitor.setProgress(cracNumber * 0.5 / controllable.size());

				monitor.setMessage("Setting object \"" + cracSensor + "\" value to initial value");
				dataAccess.setControllableValue(cracSensor, startCalibration);

				monitor.setMessage("Waiting for data center to settle");
				waitToSettle(settleInterval);
			}

			monitor.setMessage("Classification completed successfully");
			monitor.setProgress(1.0);
			monitor.complete(allResults);
		} catch (Throwable e) {
			monitor.fail(e);
		}
	}

	public void setDataAccess(DataAccess dataAccess) {
		this.dataAccess = dataAccess;
	}

	public void setCorrelator(Correlator correlator) {
		this.correlator = correlator;
	}

	public long getSettleInterval() {
		return settleInterval;
	}

	public double getStartCalibration() {
		return startCalibration;
	}

	public double getCalibrationStep() {
		return calibrationStep;
	}

	public int getStepsNumber() {
		return stepsNumber;
	}

	public void setSettleInterval(long settleInterval) {
		this.settleInterval = settleInterval;
	}

	public void setStartCalibration(double startCalibration) {
		this.startCalibration = startCalibration;
	}

	public void setCalibrationStep(double calibrationStep) {
		this.calibrationStep = calibrationStep;
	}

	public void setStepsNumber(int stepsNumber) {
		this.stepsNumber = stepsNumber;
	}

	private void waitToSettle(long settleInterval) {
		try {
			Thread.sleep(settleInterval);
		} catch (InterruptedException e) {
			return;
		}
	}
}
