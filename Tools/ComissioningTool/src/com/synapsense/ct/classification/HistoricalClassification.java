package com.synapsense.ct.classification;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import com.synapsense.ct.ProgressMonitor;
import com.synapsense.ct.corr.CompositeCorrelationResult;
import com.synapsense.ct.utils.Pair;
import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;

public class HistoricalClassification extends Classification {

	private String startDate;

	private String endDate;

	@Override
	public void classify(Collection<TO<?>> cracSensors, Collection<TO<?>> measuredSensors, ProgressMonitor monitor) {
		List<TO<?>> sensors = new ArrayList<TO<?>>(measuredSensors);
		for (TO<?> sensor : sensors) {
			correlator.addAffected(sensor);
		}

		CompositeCorrelationResult allResults = new CompositeCorrelationResult();

		monitor.setMessage("Starting building regions by correlator " + correlator.getName());

		double progressCounter = 0;
		for (TO<?> cracSensor : cracSensors) {
			monitor.setMessage("Building region for object " + cracSensor);
			correlator.setKernelTO(cracSensor);
			List<Pair<Date, List<Double>>> values;
			try {
				values = dataAccess.getHistoricalValueTable(cracSensor, sensors, new SimpleDateFormat(
				        "yyyy.MM.dd HH:mm:ss").parse(startDate), new SimpleDateFormat("yyyy.MM.dd HH:mm:ss")
				        .parse(endDate));
			} catch (Exception e) {
				monitor.fail(e);
				return;
			}

			for (Pair<Date, List<Double>> pair : values) {
				correlator.nextSample();

				for (int i = 0; i < pair.getSecond().size(); i++) {
					Double val = pair.getSecond().get(i);

					if (i == 0) {
						correlator.setKernelValue(val);
					} else {
						correlator.setAffectedValue(sensors.get(i - 1), new ValueTO("lastValue", val, pair.getFirst()
						        .getTime()));
					}
				}
			}
			allResults.addCorrelationResult(correlator.correlate());
			correlator.clearValues();
			progressCounter += 1.0;
			monitor.setProgress(progressCounter / (cracSensors.size() * 1.0));
		}
		monitor.setProgress(1.0);
		monitor.setMessage("Correlation completed");
		monitor.complete(allResults);
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
}
