/**
 * 
 */
package com.synapsense.hpplugin.constants;

/**
 * Class consists of constant access values from XML file if READ the
 * SET-command is disabled, if WRITE - enabled
 * 
 * @author Alexander Kravin
 * 
 */
public enum EnumAccess {
READ, WRITE;
}
