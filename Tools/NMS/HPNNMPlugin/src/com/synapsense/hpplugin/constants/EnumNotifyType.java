/**
 * 
 */
package com.synapsense.hpplugin.constants;

import org.snmp4j.smi.OID;

/**
 * @author Alexander Kravin
 * 
 */
public enum EnumNotifyType {
INFO(40000000), WARN(40000001), ERROR(40000002);

private int num;
private static final String ENTERPRISE = "1.3.6.1.4.1.29078.1.0";

private EnumNotifyType(int num) {
	this.num = num;
}

public OID getOID() {
	OID oid = new OID(ENTERPRISE);
	oid.append(num);
	return oid;
}
}
