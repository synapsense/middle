/**
 * 
 */
package com.synapsense.hpplugin.constants;

/**
 * @author Alexander Kravin
 * 
 */
public enum EnumSNMPTransport {
Unset, UDP, TCP;
}
