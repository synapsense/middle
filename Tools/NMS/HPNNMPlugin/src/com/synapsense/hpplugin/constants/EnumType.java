package com.synapsense.hpplugin.constants;

import java.util.ArrayList;

/**
 * 
 * @author Alexander Kravin
 */
public enum EnumType {
INT, UINT, LONG, DOUBLE, STRING;

private static ArrayList<EnumType> numbers = new ArrayList<EnumType>(4);

static {
	numbers.add(INT);
	numbers.add(UINT);
	numbers.add(LONG);
	numbers.add(DOUBLE);
}

public boolean isDigit() {
	return numbers.contains(this);
}
}
