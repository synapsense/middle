/**
 * 
 */
package com.synapsense.hpplugin.constants;

import org.snmp4j.security.AuthMD5;
import org.snmp4j.security.AuthSHA;
import org.snmp4j.smi.OID;

/**
 * @author Alexander Kravin
 * 
 */
public enum EnumSNMPAuthProtocol {
Unset(null), MD5(AuthMD5.ID), SHA(AuthSHA.ID);

private OID oid;

private EnumSNMPAuthProtocol(OID oid) {
	this.oid = oid;
}

public OID getOID() {
	return oid;
}
}
