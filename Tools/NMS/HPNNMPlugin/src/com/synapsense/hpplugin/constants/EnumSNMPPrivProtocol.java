/**
 * 
 */
package com.synapsense.hpplugin.constants;

import org.snmp4j.security.PrivAES128;
import org.snmp4j.security.PrivAES192;
import org.snmp4j.security.PrivAES256;
import org.snmp4j.security.PrivDES;
import org.snmp4j.smi.OID;

/**
 * @author Alexander Kravin
 * 
 */
public enum EnumSNMPPrivProtocol {
Unset(null), DES(PrivDES.ID), AES128(PrivAES128.ID), AES192(PrivAES192.ID), AES256(PrivAES256.ID);

private OID oid;

private EnumSNMPPrivProtocol(OID oid) {
	this.oid = oid;
}

public OID getOID() {
	return oid;
}
}
