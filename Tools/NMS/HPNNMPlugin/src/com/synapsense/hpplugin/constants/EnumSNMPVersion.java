/**
 * 
 */
package com.synapsense.hpplugin.constants;

/**
 * @author Alexander Kravin
 * 
 */
public enum EnumSNMPVersion {
V2C, V3;
}
