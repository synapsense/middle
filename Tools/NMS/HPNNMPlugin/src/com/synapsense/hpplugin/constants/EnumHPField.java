package com.synapsense.hpplugin.constants;

import java.util.HashMap;

/**
 * Class consists of constant fields from registrations files
 * "$OvInstallDir/fields/C/synap_fields"
 * 
 * @author Alexander Kravin
 */
public enum EnumHPField {

// Application fields
FLD_SYNAPNETWORKLAYOUT("SynapNetworkLayout", "Network Submap layout", 3, 0, 0), FLD_SYNAPNODELAYOUT("SynapNodeLayout",
        "Node Submap layout", 3, 0, 0), FLD_SYNAPSENSORKLAYOUT("SynapSensorLayout", "Sensor Submap layout", 3, 0, 0), FLD_SYNAPNETPOLLINTERVAL(
        "SynapNetPollingInterval", "Network polling interval", 0, 0, 1),

// Common fields
FLD_ADDRESS("IP Hostname", "SNMP Agent IP Address", 1, -1, 1), FLD_PORT("SynapSNMPAgentPort", "SNMP Agent Port", 0, 0,
        1), FLD_SNMPTRANSPORT("SynapSNMPTransport", "SNMP Transport", 3, 0, 1), FLD_SNMPTIMEOUT("SynapSNMPTimeout",
        "SNMP Timeout", 0, 0, 1), FLD_SNMPRETRIES("SynapSNMPRetries", "SNMP Retries", 0, 0, 1),

// SNMP v2c fields
FLD_IS_SYNAPPCSNMP2C("isSynapPCSNMP2c", "isSynapPCSNMP2c", 2, 1, 1), FLD_COMMUNITYPUBLIC(
        "SynapSNMPAgentCommunityPublic", "SNMP Agent Community Public", 1, 0, 1), FLD_COMMUNITYPRIVATE(
        "SynapSNMPAgentCommunityPrivate", "SNMP Agent Community Private", 1, 0, 0),

// SNMP v3 fields
FLD_IS_SYNAPPCSNMP3("isSynapPCSNMP3", "isSynapPCSNMP3", 2, 1, 0), FLD_SNMPSECNAME("SynapSNMPSecurityName",
        "SNMP Security Name", 1, 0, 1), FLD_SNMPCONTEXTNAME("SynapSNMPContextName", "SNMP Context Name", 1, 0, 1), FLD_SNMPAUTHPROTOCOL(
        "SynapSNMPAuthProtocol", "SNMP Authentication Protocol", 3, 0, 0), FLD_SNMPAUTHPWD("SynapSNMPAuthPassword",
        "SNMP Authentication Password", 1, 0, 0), FLD_SNMPPRIVPROTOCOL("SynapSNMPPrivacyProtocol",
        "SNMP Privacy Protocol", 3, 0, 0), FLD_SNMPPRIVPWD("SynapSNMPPrivPassword", "SNMP Privacy Password", 1, 0, 0),

// Different fields
FLD_NETWORK_ID("SynapNetworkId", "SynapNetworkId", 0, 0, 1), FLD_NODE_ID("SynapNodeId", "SynapNodeId", 0, 0, 1), FLD_SENSOR_ID(
        "SynapSensorId", "SynapSensorId", 0, 0, 1),

FLD_IS_SYNAPNETWORK("isSynapNetwork", "isSynapNetwork", 2, 1, 0), FLD_IS_SYNAPNODE("isSynapNode", "isSynapNode", 2, 1,
        0), FLD_IS_SYNAPSENSOR("isSynapSensor", "isSynapSensor", 2, 1, 0),

FLD_IS_SYNAPSTORE("isSynapstore", "isSynapstore", 2, 1, 0), FLD_IS_SYNAPDATABASE("isSynapDatabase", "isSynapDatabase",
        2, 1, 0), FLD_IS_SYNAPWEBSERVER("isSynapWebServer", "isSynapWebServer", 2, 1, 0), FLD_IS_SYNAPSNMPAGENT(
        "isSynapSNMPAgent", "isSynapSNMPAgent", 2, 1, 0);

private String dbName; // Database name
private String label; // Display name

private int type; // 0-integer; 1-string; 2-boolean; 3-enumeration
private int flag; // -1 - system field; 0 - application field; 1 - capability
				  // flag
private int checked; // 0 - not checked; 1 - checked

private static final HashMap<String, EnumHPField> map = new HashMap<String, EnumHPField>(26);

static {

	// Application fields
	map.put(FLD_SYNAPNETWORKLAYOUT.dbName, FLD_SYNAPNETWORKLAYOUT);
	map.put(FLD_SYNAPNODELAYOUT.dbName, FLD_SYNAPNODELAYOUT);
	map.put(FLD_SYNAPSENSORKLAYOUT.dbName, FLD_SYNAPSENSORKLAYOUT);
	map.put(FLD_SYNAPNETPOLLINTERVAL.dbName, FLD_SYNAPNETPOLLINTERVAL);

	// Common fields
	map.put(FLD_ADDRESS.dbName, FLD_ADDRESS);
	map.put(FLD_PORT.dbName, FLD_PORT);
	map.put(FLD_SNMPTRANSPORT.dbName, FLD_SNMPTRANSPORT);
	map.put(FLD_SNMPTIMEOUT.dbName, FLD_SNMPTIMEOUT);
	map.put(FLD_SNMPRETRIES.dbName, FLD_SNMPRETRIES);

	// SNMP v2c fields
	map.put(FLD_IS_SYNAPPCSNMP2C.dbName, FLD_IS_SYNAPPCSNMP2C);
	map.put(FLD_COMMUNITYPUBLIC.dbName, FLD_COMMUNITYPUBLIC);
	map.put(FLD_COMMUNITYPRIVATE.dbName, FLD_COMMUNITYPRIVATE);

	// SNMP v3 fields
	map.put(FLD_IS_SYNAPPCSNMP3.dbName, FLD_IS_SYNAPPCSNMP3);
	map.put(FLD_SNMPSECNAME.dbName, FLD_SNMPSECNAME);
	map.put(FLD_SNMPCONTEXTNAME.dbName, FLD_SNMPCONTEXTNAME);
	map.put(FLD_SNMPAUTHPROTOCOL.dbName, FLD_SNMPAUTHPROTOCOL);
	map.put(FLD_SNMPAUTHPWD.dbName, FLD_SNMPAUTHPWD);
	map.put(FLD_SNMPPRIVPROTOCOL.dbName, FLD_SNMPPRIVPROTOCOL);
	map.put(FLD_SNMPPRIVPWD.dbName, FLD_SNMPPRIVPWD);

	// Different fields
	map.put(FLD_NETWORK_ID.dbName, FLD_NETWORK_ID);
	map.put(FLD_NODE_ID.dbName, FLD_NODE_ID);
	map.put(FLD_SENSOR_ID.dbName, FLD_SENSOR_ID);

	map.put(FLD_IS_SYNAPNETWORK.dbName, FLD_IS_SYNAPNETWORK);
	map.put(FLD_IS_SYNAPNODE.dbName, FLD_IS_SYNAPNODE);
	map.put(FLD_IS_SYNAPSENSOR.dbName, FLD_IS_SYNAPSENSOR);

	map.put(FLD_IS_SYNAPSTORE.dbName, FLD_IS_SYNAPSTORE);
	map.put(FLD_IS_SYNAPDATABASE.dbName, FLD_IS_SYNAPDATABASE);
	map.put(FLD_IS_SYNAPWEBSERVER.dbName, FLD_IS_SYNAPWEBSERVER);
	map.put(FLD_IS_SYNAPSNMPAGENT.dbName, FLD_IS_SYNAPSNMPAGENT);
}

/**
 * @param dbName
 *            A field's name from registration file
 *            "$OvInstallDir/fields/C/synap_fields"
 * 
 * @param type
 *            A field's type
 * @param whose
 *            Must be APP_FIELD or SYS_FIELD
 */
/**
 * 
 * @param dbName
 *            a field's name from registration file
 *            <code>"$OvInstallDir/fields/C/synap_fields"</code>
 * @param label
 *            a display name
 * @param type
 *            a type of field
 * @param flag
 *            a flag of field from registration file
 *            <code>"$OvInstallDir/fields/C/synap_fields"</code>
 * @param checked
 *            if equals 1 then checked field
 */
private EnumHPField(String dbName, String label, int type, int flag, int checked) {
	this.dbName = dbName;
	this.label = label;
	this.type = type;
	this.flag = flag;
	this.checked = checked;
}

/**
 * @return a field's name
 */
public String getDbName() {
	return dbName;
}

/**
 * @return a display name
 */
public String getLabel() {
	return label;
}

/**
 * @return TRUE if not set any flags
 */
public boolean isAppField() {
	return (flag == 0);
}

/**
 * @return TRUE if set a capability flag
 */
public boolean isCapabilityField() {
	return (flag == 1);
}

/**
 * @return TRUE if it's a system field
 */
public boolean isSystem() {
	return (flag == -1);
}

/**
 * @return TRUE if a field will be checked
 */
public boolean isChecked() {
	return (checked == 1);
}

/**
 * @return TRUE if field's type is an integer
 */
public boolean isInteger() {
	return (type == 0);
}

/**
 * @return TRUE if field's type is a boolean
 */
public boolean isBoolean() {
	return (type == 2);
}

/**
 * @return TRUE if field's type is a string
 */
public boolean isString() {
	return (type == 1);
}

/**
 * @return TRUE if field's type is an enumeration
 */
public boolean isEnumeration() {
	return (type == 3);
}

/**
 * Gets an HPFields instance by specified field's name from registration file
 * "$OvInstallDir/fields/C/synap_fields"
 * 
 * @param fieldName
 *            a field's name
 * @return <code>HPFields</code> instance
 */
public static EnumHPField getValueByFieldName(String fieldName) {
	return map.get(fieldName);
}
}
