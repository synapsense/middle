/**
 * 
 */
package com.synapsense.hpplugin.constants;

import java.util.HashMap;

/**
 * Class consists of symbol's types from a registrations files in the
 * "$OvInstallDir/symbols/C" directory of HPOpenView
 * 
 * @author Alexander Kravin
 */

public enum EnumHPSymbol {

SYNAPPCSNMP2C("Synapsense:SynapPCSNMP2c", null, 0, -1), SYNAPPCSNMP3("Synapsense:SynapPCSNMP3", null, 0, -1),

SYNAPNETWORK("Synapsense:SynapNetwork", "Network", 1, 1), SYNAPNODE("Synapsense:SynapNode", "Node", 2, 1), SYNAPSENSOR(
        "Synapsense:SynapSensor", "Sensor", 3, 1),

SYNAPSTORE("Synapsense:SynapStore", "Synapstore", 1, 0), SYNAPDATABASE("Synapsense:SynapDatabase", "Database", 1, 0), SYNAPWEBSERVER(
        "Synapsense:SynapWebServer", "Webserver", 1, 0), SYNAPSNMPAGENT("Synapsense:SynapSNMPAgent", "SNMPAgent", 1, 0);

private String symbolType;
private String objectName;
private int level; // Level of hierarchy
private int type; // -1 - no type; 0 - simple type; 1 - table type

private static final HashMap<String, EnumHPSymbol> map = new HashMap<String, EnumHPSymbol>(9);

static {
	map.put(SYNAPPCSNMP2C.symbolType, SYNAPPCSNMP2C);
	map.put(SYNAPPCSNMP3.symbolType, SYNAPPCSNMP3);
	map.put(SYNAPNETWORK.symbolType, SYNAPNETWORK);
	map.put(SYNAPNODE.symbolType, SYNAPNODE);
	map.put(SYNAPSENSOR.symbolType, SYNAPSENSOR);
	map.put(SYNAPSTORE.symbolType, SYNAPSTORE);
	map.put(SYNAPDATABASE.symbolType, SYNAPDATABASE);
	map.put(SYNAPWEBSERVER.symbolType, SYNAPWEBSERVER);
	map.put(SYNAPSNMPAGENT.symbolType, SYNAPSNMPAGENT);
}

/**
 * 
 * @param symbolType
 *            a symbol's type from registration file HPOpenView
 * @param level
 *            a level on the map
 * @param isSimpleType
 *            TRUE if symbols is a simple type
 */
private EnumHPSymbol(String symbolType, String objectName, int level, int type) {
	this.symbolType = symbolType;
	this.objectName = objectName;
	this.level = level;
	this.type = type;
}

/**
 * @return A symbol's type
 */
public String getSymbolType() {
	return symbolType;
}

/**
 * @return An object's name from XML file
 */
public String getObjectName() {
	return objectName;
}

/**
 * @return TRUE if the hpSymbol has a simple type
 */
public boolean isSimple() {
	return (type == 0);
}

/**
 * @return TRUE if the hpSymbol has a table type
 */
public boolean isTable() {
	return (type == 1);
}

public EnumHPSymbol getChildSymbol() {
	switch (this) {
	case SYNAPPCSNMP2C:
		return SYNAPNETWORK;
	case SYNAPPCSNMP3:
		return SYNAPNETWORK;
	case SYNAPNETWORK:
		return SYNAPNODE;
	case SYNAPNODE:
		return SYNAPSENSOR;
	default:
		return this;
	}
}

/**
 * @return A level on the map
 */
public int getLevel() {
	return level;
}

/**
 * Gets an EnumHPSymbol instance by specified symbol's type from registrations
 * files in the "$OvInstallDir/symbols/C" directory
 * 
 * @param symbolType
 *            A symbol's type
 * @return EnumHPSymbol instance
 */
public static EnumHPSymbol getValueBySymbolType(String symbolType) {
	return map.get(symbolType);
}

}
