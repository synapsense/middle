/**
 * 
 */
package com.synapsense.hpplugin.constants;

import java.util.ArrayList;

/**
 * Class consists of constant SNMP values from XML file which uses for sending
 * SET-command
 * 
 * @author Alexander Kravin
 * 
 */
public enum EnumSNMPType {
OID, INTEGER32, GAUGE32, OCTETSTRING, TIMETICKS;

private static ArrayList<EnumSNMPType> numbers = new ArrayList<EnumSNMPType>(4);

static {
	numbers.add(INTEGER32);
	numbers.add(GAUGE32);
	numbers.add(TIMETICKS);
}

public boolean isDigit() {
	return numbers.contains(this);
}
}
