/**
 * 
 */
package com.synapsense.hpplugin.hpdialogs;

import java.util.ArrayList;

import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;

import com.synapsense.hpplugin.configuration.XMLObject;
import com.synapsense.hpplugin.configuration.XMLProperty;
import com.synapsense.hpplugin.constants.EnumAccess;
import com.synapsense.hpplugin.synapobjects.SynapObject;

/**
 * Class is a model of the table. Also class holds a data from the table.
 * 
 * @author Alexander Kravin
 * 
 */
public class HPTableModel extends AbstractTableModel implements TableModelListener {

	private static final long serialVersionUID = 1L;
	private String[] columnName = { "ID", "Name", "Value" };
	private Object[][] data = null; // ID, name, value, type, snmpType, min,
									// max, access, changed, old value

	/**
	 * Creates the instance and fills a data from <code>SynapObject</code>.
	 * 
	 * @param synapObject
	 *            an <code>SynapObject</code> instance.
	 */
	public HPTableModel(SynapObject synapObject) {

		// Add listener
		addTableModelListener(this);

		XMLObject xmlObject = synapObject.getXMLObject();
		ArrayList<XMLProperty> arrayProperty = xmlObject.getArrayXMLProperty();
		int count = arrayProperty.size();

		data = new String[count][10];

		for (int i = 0; i < count; i++) {
			XMLProperty xmlProperty = arrayProperty.get(i);

			data[i][0] = "" + xmlProperty.getId(); // ID
			data[i][1] = xmlProperty.getName(); // Name
			data[i][2] = synapObject.getValue(i); // Value
			data[i][3] = xmlProperty.getType().toString(); // Type
			data[i][4] = xmlProperty.getSNMPType().toString(); // snmpType
			data[i][5] = "" + xmlProperty.getMin(); // Min
			data[i][6] = "" + xmlProperty.getMax(); // Max
			data[i][7] = xmlProperty.getAccess().toString(); // Access
			data[i][8] = ""; // Changed
			data[i][9] = synapObject.getValue(i); // Old value

		}
	}

	/**
	 * Creates the instance and fills a data from <code>Object[][]</code>.
	 * 
	 * @param columnName
	 *            a columns' names.
	 * @param data
	 *            a table's data.
	 */
	public HPTableModel(String[] columnName, Object[][] data) {
		this.columnName = columnName;
		this.data = data;
	}

	public int getRowByTableId(int id) {
		int row;
		for (row = 0; row < data.length; row++) {
			String strId = data[row][0].toString();
			int pos = strId.indexOf(".");
			if (pos != -1) {
				strId = strId.substring(0, pos);
			}
			if (Integer.parseInt(strId) == id) {
				break;
			}
		}
		return row;
	}

	@Override
	public String getColumnName(int column) {
		return columnName[column];
	}

	@Override
	public int getColumnCount() {
		return columnName.length;
	}

	@Override
	public int getRowCount() {
		return data.length;
	}

	@Override
	public boolean isCellEditable(int row, int col) {
		if (col == 2 && isAccess(row))
			return true;
		return false;
	}

	@Override
	public Object getValueAt(int row, int column) {
		return data[row][column];
	}

	@Override
	public void tableChanged(TableModelEvent event) {
		System.out.println("table changed");
	}

	@Override
	public void setValueAt(Object value, int row, int column) {
		data[row][column] = value;
	}

	/**
	 * Checks an access to a cell.
	 * 
	 * @param row
	 *            a row's number.
	 * 
	 * @return TRUE if a editable cell.
	 */
	public boolean isAccess(int row) {
		EnumAccess access = EnumAccess.valueOf(data[row][7].toString());
		return (EnumAccess.WRITE == access);
	}

}
