/**
 * 
 */
package com.synapsense.hpplugin.hpdialogs;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.KeyAdapter;
import java.awt.event.MouseEvent;
import java.util.EventObject;

import javax.swing.AbstractCellEditor;
import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.TableCellEditor;

import com.synapsense.hpplugin.constants.EnumSNMPType;
import com.synapsense.hpplugin.constants.EnumType;

/**
 * This class uses an <code>JTextField</code> element in the capacity of editor
 * of the cell.
 * 
 * @author Alexander Kravin
 * 
 */
public class CellEditor extends AbstractCellEditor implements TableCellEditor {

	private static final long serialVersionUID = 1L;
	// This is the component that will handle the editing of the cell value
	JComponent component;
	private EnumType type;
	private EnumSNMPType snmpType;
	private JTable table;
	int tableRow, dataRow, column;
	double min, max;

	/**
	 * Adds new <code>JTextField</code> instance in the capacity of cell editor
	 * and adds key listener on it.
	 */
	public CellEditor() {
		component = new JTextField();
		component.addKeyListener(new CellKeyListener());
	}

	/**
	 * Class extends KeyAdapter class and checks the user data input.
	 * 
	 * @author Alexander Kravin
	 */
	public class CellKeyListener extends KeyAdapter {
		public void keyTyped(java.awt.event.KeyEvent event) {
			char ch = event.getKeyChar();
			if (!isKeyTRUE(ch))
				event.consume();
		}
	}

	/**
	 * Checks a key what a user press.
	 * 
	 * @param ch
	 *            the symbol of a key which was pressed by the user.
	 * 
	 * @return TRUE if a symbol is valid.
	 */
	private boolean isKeyTRUE(char ch) {
		String text = ((JTextField) component).getText();
		switch (type) {
		case DOUBLE:
			if (!Character.isDigit(ch) && ch != '.' && ch != '-' && ch != '\n') {
				return false;
			} else if (ch == '.') {
				if (text.indexOf('.') != -1)
					return false;
			}
			break;

		case UINT:
			if (!Character.isDigit(ch) && ch != '\n')
				return false;
			break;

		case STRING:
			break;
		default:
			if (!Character.isDigit(ch) && ch != '-' && ch != '\n') {
				return false;
			}
			break;
		}

		// If a minus number
		if (ch == '-') {
			if (text.indexOf('-') != -1) {
				return false;
			} else {
				((JTextField) component).setText("-" + ((JTextField) component).getText());
				return false;
			}
		}
		return true;
	}

	/**
	 * This method is called when editing is completed. It must return the new
	 * value to be stored in the cell.
	 */
	public Object getCellEditorValue() {
		return ((JTextField) component).getText();
	}

	/**
	 * This method is called when a cell value is edited by the user.
	 */
	@Override
	public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
		int id = Integer.parseInt(table.getValueAt(row, 0).toString());
		dataRow = ((HPTableModel) table.getModel()).getRowByTableId(id);
		this.tableRow = row;
		this.column = column;
		this.table = table;

		min = Double.parseDouble(table.getModel().getValueAt(dataRow, 5).toString());
		max = Double.parseDouble(table.getModel().getValueAt(dataRow, 6).toString());
		type = EnumType.valueOf(table.getModel().getValueAt(dataRow, 3).toString());
		snmpType = EnumSNMPType.valueOf(table.getModel().getValueAt(dataRow, 4).toString());

		if (isSelected) {
			// cell (and perhaps other cells) are selected
		}

		// Configure the component with the specified value
		((JTextField) component).setText((String) value);

		// Return the configured component
		component.setBorder(BorderFactory.createMatteBorder(2, 5, 2, 5, table.getBackground()));
		return component;
	}

	@Override
	public boolean shouldSelectCell(EventObject evt) {
		component.setForeground(new Color(255, 0, 0));
		return true;
	}

	@Override
	public boolean isCellEditable(EventObject evt) {
		if (evt instanceof MouseEvent) {
			int clickCount;
			// For double-click activation
			clickCount = 2;
			return ((MouseEvent) evt).getClickCount() >= clickCount;
		}
		return true;
	}

	/**
	 * This method is called just before the cell value is saved. If the value
	 * is not valid, false should be returned.
	 */
	@Override
	public boolean stopCellEditing() {
		Object value = getCellEditorValue();
		if (isRange(value)) {
			table.setValueAt(value, tableRow, column);
			if (table.getModel().getValueAt(dataRow, 9).equals(value)) {
				table.getModel().setValueAt("", dataRow, 8);
			} else {
				table.getModel().setValueAt("*", dataRow, 8);
			}

			return super.stopCellEditing();
		}
		JOptionPane.showMessageDialog(table.getParent(), "The value must be in a range from [" + min + "] up [" + max
		        + "]");
		return false;
	}

	/**
	 * Checks the value in the range or not.
	 * 
	 * @param value
	 *            a checked value.
	 * 
	 * @return TRUE if a value is in the range.
	 */
	public boolean isRange(Object value) {
		if (snmpType.isDigit()) {
			double val = Double.parseDouble(value.toString());
			return (val >= min && val <= max);
		}
		return (value.toString().length() <= max);
	}
}
