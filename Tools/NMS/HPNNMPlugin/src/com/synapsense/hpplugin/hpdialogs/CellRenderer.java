/**
 * 
 */
package com.synapsense.hpplugin.hpdialogs;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.border.Border;
import javax.swing.table.TableCellRenderer;

import com.synapsense.hpplugin.constants.EnumAccess;
import com.synapsense.hpplugin.constants.EnumType;

/**
 * Class redraws a <code>JLabel</code> element in each cell of the table
 * 
 * @author Alexander Kravin
 * 
 */
public class CellRenderer extends JLabel implements TableCellRenderer {

	private static final long serialVersionUID = 1L;

	private Border unselectedBorder = null;
	private Border selectedBorder = null;
	private Font selectedFont = null;

	/**
	 * Initialize the instance
	 */
	public CellRenderer() {
		setOpaque(true);
	}

	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
	        int row, int column) {
		String strid = table.getValueAt(row, 0).toString();
		int pos = strid.indexOf('.');
		if (pos != -1) {
			strid = strid.substring(0, pos);
		}
		int id = Integer.parseInt(strid);
		row = ((HPTableModel) table.getModel()).getRowByTableId(id);

		setFont(table.getFont());

		setText(value.toString());

		// ID
		if (column == 0) {
			setHorizontalAlignment(JLabel.CENTER);
			return this;
		}

		if (selectedFont == null) {
			Font tblFont = table.getFont();
			selectedFont = new Font(tblFont.getName(), Font.BOLD, tblFont.getSize());
		}

		if (isSelected) {
			if (selectedBorder == null || selectedFont == null) {
				selectedBorder = BorderFactory.createMatteBorder(2, 5, 2, 5, table.getSelectionBackground());
			}
			setBackground(table.getSelectionBackground());
			setBorder(selectedBorder);
			setFont(selectedFont);
		} else {
			if (unselectedBorder == null) {
				unselectedBorder = BorderFactory.createMatteBorder(2, 5, 2, 5, table.getBackground());
			}
			setBorder(unselectedBorder);
			setBackground(table.getBackground());
		}

		// Name
		if (column == 1) {
			setForeground(new Color(46, 33, 163));
			setHorizontalAlignment(JLabel.LEFT);
			return this;
		}

		EnumType type = EnumType.valueOf(table.getModel().getValueAt(row, 3).toString());

		if (EnumType.STRING == type)
			setHorizontalAlignment(JLabel.LEFT);
		else
			setHorizontalAlignment(JLabel.RIGHT);

		EnumAccess access = EnumAccess.valueOf(table.getModel().getValueAt(row, 7).toString());
		String min = table.getModel().getValueAt(row, 5).toString();
		String max = table.getModel().getValueAt(row, 6).toString();

		if (EnumAccess.WRITE == access) {
			if (table.getModel().getValueAt(row, 8).equals("*")) {
				setForeground(new Color(255, 0, 0));
			} else {
				setForeground(new Color(51, 113, 36));
			}

			setToolTipText("Type: " + type + " Min: " + min + " Max: " + max);
		} else {
			setToolTipText("Constant value");
			setFont(selectedFont);
			setForeground(Color.BLACK);
		}

		return this;
	}
}
