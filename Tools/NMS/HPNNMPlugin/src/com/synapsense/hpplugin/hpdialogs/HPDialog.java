package com.synapsense.hpplugin.hpdialogs;

import java.awt.BorderLayout;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.table.TableColumn;
import javax.swing.table.TableRowSorter;

import org.apache.log4j.Logger;
import org.snmp4j.PDU;
import org.snmp4j.Snmp;
import org.snmp4j.event.ResponseEvent;
import org.snmp4j.event.ResponseListener;
import org.snmp4j.smi.OID;

import com.synapsense.hpplugin.constants.EnumHPField;
import com.synapsense.hpplugin.constants.EnumHPSymbol;
import com.synapsense.hpplugin.constants.EnumSNMPType;
import com.synapsense.hpplugin.hpobjects.HPObject;
import com.synapsense.hpplugin.snmpobjects.SNMPObject;
import com.synapsense.hpplugin.snmpobjects.SynapSNMP;
import com.synapsense.hpplugin.synapobjects.SynapObject;

/**
 * Shows a dialog.
 * 
 * @author Alexander Kravin
 * 
 */
public class HPDialog extends Dialog {

	private static final Logger log = Logger.getLogger(HPDialog.class);

	private final static String newline = "\n";
	private static final long serialVersionUID = 1L;
	private static final String OK = "    OK    ";
	private static final String Apply = "  Apply   ";
	private static final String Cancel = "  Cancel  ";

	private SynapObject synapObject;
	private HPObject hpObject;
	private SNMPObject snmpObject;

	private JTable table;
	private JTextArea txtComment;
	private HPButton btnOK;
	private HPButton btnApply;
	private HPButton btnCancel;

	String address;
	int port;
	String community;

	private final int iSizeX = 370, iSizeY = 420;

	/**
	 * Initializes class.
	 * 
	 * @param snmpObject
	 *            an <code>SNMPObject</code> instance.
	 * @param hpObject
	 *            an <code>HPObject</code> instance.
	 * @param synapObject
	 *            an <code>SynapObject<code> instance.
	 * @param parent
	 *            a parent frame.
	 * @param sTitle
	 *            a title of dialog box.
	 * @param bModal
	 *            if TRUE then the dialog box will be modal.
	 */
	public HPDialog(SNMPObject snmpObject, HPObject hpObject, SynapObject synapObject, Frame parent, String sTitle,
	        boolean bModal) {
		super(parent, sTitle, bModal);

		this.snmpObject = snmpObject;
		this.synapObject = synapObject;
		this.hpObject = hpObject;

		setLayout(new BorderLayout()); // Set main layout
		setSize(iSizeX, iSizeY);
		setCentre(this);

		JPanel topPanel = new JPanel(new BorderLayout());
		HPTableModel tableModel = new HPTableModel(synapObject);
		topPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 0, 5));
		table = new JTable(tableModel);

		table.addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseMoved(java.awt.event.MouseEvent evn) {
				JTable tbl = (JTable) evn.getSource();
				int tblRowHeight = tbl.getRowHeight();
				int mouseY = evn.getY();
				int curRow = (int) mouseY / tblRowHeight;

				tbl.setRowSelectionAllowed(true);
				tbl.setRowSelectionInterval(curRow, curRow);
			}
		});

		// Set column width and cell rendering for each column
		TableColumn tableColumn;

		tableColumn = table.getColumn(tableModel.getColumnName(0)); // ID
		tableColumn.setCellRenderer(new CellRenderer());
		tableColumn.setMaxWidth(40);
		tableColumn.setResizable(false);

		tableColumn = table.getColumn(tableModel.getColumnName(1)); // Name
		tableColumn.setCellRenderer(new CellRenderer());
		tableColumn.setPreferredWidth(2);

		tableColumn = table.getColumn(tableModel.getColumnName(2)); // Value
		tableColumn.setCellRenderer(new CellRenderer());
		tableColumn.setCellEditor(new CellEditor());

		// Set table row height
		table.setRowHeight(20);

		// Set row sorter
		TableRowSorter<HPTableModel> sorter = new TableRowSorter<HPTableModel>(tableModel);
		table.setRowSorter(sorter);

		// Setting all other table properties
		table.setAutoCreateColumnsFromModel(true);
		table.setShowHorizontalLines(true);
		table.setRowSelectionAllowed(true);
		table.setColumnSelectionAllowed(false);

		// Setting scroll pane to the table
		JScrollPane scrollpane = new JScrollPane(table);

		// Setting table border
		table.setBorder(BorderFactory.createEtchedBorder());

		txtComment = new JTextArea(5, 20);

		txtComment.setBorder(BorderFactory.createLoweredBevelBorder());
		txtComment.setBackground(scrollpane.getBackground());
		txtComment.setText("You can change \"Value\" which " + "is highlighted with green");
		JScrollPane scrollComment = new JScrollPane(txtComment);
		scrollComment.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		txtComment.setEditable(false);
		txtComment.setWrapStyleWord(true);
		txtComment.setLineWrap(true); // Setting txtComment as multiline text
									  // box

		scrollComment.setPreferredSize(new Dimension(150, 55));

		JPanel midPanel = new JPanel(new BorderLayout());
		JLabel label = new JLabel("Messages:");
		label.setPreferredSize(new Dimension(0, 20));
		label.setFont(table.getFont());
		midPanel.setFont(table.getFont());
		midPanel.add(label, BorderLayout.PAGE_START);

		midPanel.add(scrollComment, BorderLayout.PAGE_END);
		topPanel.add(scrollpane, BorderLayout.CENTER);
		topPanel.add(midPanel, BorderLayout.PAGE_END);

		// Add table to the dialog
		add(topPanel, BorderLayout.CENTER);

		JPanel bottomPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));

		btnOK = new HPButton(OK);
		btnApply = new HPButton(Apply);
		btnCancel = new HPButton(Cancel);

		btnOK.addActionListener(btnOK);
		btnApply.addActionListener(btnApply);
		btnCancel.addActionListener(btnCancel);

		bottomPanel.add(btnOK);
		bottomPanel.add(btnApply);
		bottomPanel.add(btnCancel);
		add(bottomPanel, BorderLayout.PAGE_END);

		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent we) {
				dispose();
			}
		});
	}

	/**
	 * Class overrides JButton class and implements ActionListener interface for
	 * capture action from user.
	 * 
	 * @author Alexander Kravin
	 */

	class HPButton extends JButton implements ActionListener {
		private static final long serialVersionUID = 1L;

		/**
		 * Creates the instance.
		 */
		public HPButton() {
			super();
		}

		/**
		 * Creates the instance with specified caption.
		 * 
		 * @param caption
		 *            a button's caption.
		 */
		public HPButton(String caption) {
			super(caption);
		}

		@Override
		public void actionPerformed(ActionEvent event) {
			String action = event.getActionCommand();
			if (OK.equals(action) || Apply.equals(action)) {

				try {
					save();
					// txtComment.setText("SET command was sent...");
				} catch (IOException e) {
					log.error(e.getLocalizedMessage(), e);
					txtComment.setText("Cannot send SET command because: " + e.getLocalizedMessage());
				}
				table.updateUI();
			}

			if (Cancel.equals(action) || OK.equals(action)) {
				dispose();
			}
		}
	}

	/**
	 * Saves changed values.
	 * 
	 * @throws IOException
	 *             if cannot send SET command.
	 */
	public void save() throws IOException {

		ArrayList<Integer> changedLine = new ArrayList<Integer>();
		// ID, name, value, type, min, max, access, changed
		int rowCount = table.getRowCount();
		for (int i = 0; i < rowCount; i++) {
			String ch = table.getModel().getValueAt(i, 8).toString();
			if ("*".equals(ch)) {
				changedLine.add(new Integer(i));
			}
		}
		// if changes not find
		if (changedLine.size() == 0)
			return;

		// Create connect to SynapSNMPAgent
		SynapSNMP synapsnmp = new SynapSNMP(snmpObject);
		synapsnmp.connect();
		synapsnmp.addSNMPListener(listener);

		for (int i = 0; i < changedLine.size(); i++) {
			int line = changedLine.get(i).intValue();

			// Create OID
			EnumHPSymbol hpSymbol = hpObject.getHPSymbol();
			String id = table.getModel().getValueAt(line, 0).toString();
			OID oid = synapObject.getXMLObject().getOIDById(id);

			int netId = 0, nodeId = 0, sensorId = 0;
			boolean device = false;

			switch (hpSymbol) {
			case SYNAPSENSOR:
				sensorId = Integer.parseInt(hpObject.getHPFieldBindList().getStringValue(EnumHPField.FLD_SENSOR_ID));
				device = true;
			case SYNAPNODE:
				nodeId = Integer.parseInt(hpObject.getHPFieldBindList().getStringValue(EnumHPField.FLD_NODE_ID));
				if (device) {
					oid.append(sensorId);
					oid.append(nodeId);
					break;
				}
				device = true;
			case SYNAPNETWORK:
				netId = Integer.parseInt(hpObject.getHPFieldBindList().getStringValue(EnumHPField.FLD_NETWORK_ID));
				if (device) {
					oid.append(nodeId);
					oid.append(netId);
					break;
				}
				oid.append(netId);
			}
			EnumSNMPType type = EnumSNMPType.valueOf(table.getModel().getValueAt(line, 4).toString());
			synapsnmp.addVariable(oid, type, table.getModel().getValueAt(line, 2));
			table.getModel().setValueAt("", line, 8);
		}

		synapsnmp.snmpSet();

		// Disconnect from SNMP agent
		synapsnmp.disconnect();
	}

	/**
	 * Aligns window in centr of the screen.
	 * 
	 * @param dlg
	 *            an <code>java.awt.Dialog</code> instance.
	 */
	private void setCentre(Dialog dlg) {
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		Dimension frameSize = dlg.getSize();
		if (frameSize.height > screenSize.height) {
			frameSize.height = screenSize.height;
		}
		if (frameSize.width > screenSize.width) {
			frameSize.width = screenSize.width;
		}
		dlg.setLocation((screenSize.width - frameSize.width) / 2, (screenSize.height - frameSize.height) / 2);
	}

	/**
	 * It's a listener for SET command.
	 */
	ResponseListener listener = new ResponseListener() {
		public void onResponse(ResponseEvent event) {
			// Always cancel async request when response has been received
			// otherwise a memory leak is created! Not canceling a request
			// immediately can be useful when sending a request to a broadcast
			// address.
			((Snmp) event.getSource()).cancel(event.getRequest(), this);
			PDU pdu = event.getRequest();
			if (pdu != null) {
				txtComment.setText("Set Status is: " + pdu.getErrorStatusText());
				txtComment.append(newline);
				for (int i = 0; i < pdu.size(); i++) {
					txtComment.append(pdu.get(i).toString());
					txtComment.append(newline);
				}
			} else {
				log.warn(pdu);
				txtComment.setText(pdu.toString());
			}

			synchronized (event.getUserObject()) {
				event.getUserObject().notify();
			}

			if (log.isDebugEnabled()) {
				log.debug(pdu);
			}
		}
	};

}
