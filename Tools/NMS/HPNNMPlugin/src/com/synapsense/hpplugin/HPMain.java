/**
 * 
 */
package com.synapsense.hpplugin;

import java.awt.Frame;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.snmp4j.smi.OID;
import org.snmp4j.smi.OctetString;

import com.synapsense.hpplugin.auditor.Auditor;
import com.synapsense.hpplugin.configuration.Configuration;
import com.synapsense.hpplugin.configuration.XMLObject;
import com.synapsense.hpplugin.configuration.XMLProperty;
import com.synapsense.hpplugin.constants.EnumHPField;
import com.synapsense.hpplugin.constants.EnumHPSymbol;
import com.synapsense.hpplugin.constants.EnumNotifyType;
import com.synapsense.hpplugin.constants.EnumSNMPAuthProtocol;
import com.synapsense.hpplugin.constants.EnumSNMPPrivProtocol;
import com.synapsense.hpplugin.constants.EnumSNMPTransport;
import com.synapsense.hpplugin.hpdialogs.HPDialog;
import com.synapsense.hpplugin.hpexception.HPErrorException;
import com.synapsense.hpplugin.hpexception.HPWarnException;
import com.synapsense.hpplugin.hpobjects.HPFieldBindList;
import com.synapsense.hpplugin.hpobjects.HPLibovw;
import com.synapsense.hpplugin.hpobjects.HPObject;
import com.synapsense.hpplugin.snmpobjects.ObjectFiller;
import com.synapsense.hpplugin.snmpobjects.SNMPFactory;
import com.synapsense.hpplugin.snmpobjects.SNMPNotify;
import com.synapsense.hpplugin.snmpobjects.SNMPObject;
import com.synapsense.hpplugin.snmpobjects.SNMPv2c;
import com.synapsense.hpplugin.snmpobjects.SNMPv3;
import com.synapsense.hpplugin.snmpobjects.SynapSNMP;
import com.synapsense.hpplugin.synapobjects.SynapObject;
import com.synapsense.hpplugin.utils.HPUtils;

import hp.ov.libovw.LibOvw;
import hp.ov.libovw.LibOvwException;
import hp.ov.libovw.OVwEventAdapter;
import hp.ov.libovw.OVwEvents;
import hp.ov.libovw.OVwFieldBindList;
import hp.ov.libovw.OVwFieldValue;
import hp.ov.libovw.OVwMapInfo;
import hp.ov.libovw.OVwObjectIdList;
import hp.ov.libovw.OVwObjectInfo;
import hp.ov.libovw.OVwObjectList;
import hp.ov.libovw.OVwSubmapInfo;
import hp.ov.libovw.OVwSymbolInfo;
import hp.ov.libovw.OVwSymbolList;
import hp.ov.libovw.StringVectorParm;

/**
 * Class connects to the HP Open View and processes it events.
 * 
 * @author Alexander Kravin
 * 
 */
public class HPMain extends OVwEventAdapter {

	private static final Logger log = Logger.getLogger(HPMain.class);

	private static final String PATHXML = "conf/synapmapconfig.xml";
	private static final String PATHXSD = "conf/synapmapconfig.xsd";
	private static final String PATHLOG4J = "lib/log4j.properties";

	public static final String APP_NAME = "SynapSense SNMP Map";
	private static final String SNMPOPEN_ACTION = "changeSNMPData";

	private static int networkLayout;
	private static int nodeLayout;
	private static int sensorLayout;
	private static int pollTime;

	/**
	 * Entry point to start application
	 * 
	 * @param args
	 *            a list of strings the first parameter is a session ID
	 */
	public static void main(String[] args) {

		// java.util.Properties p = System.getProperties();
		// java.util.Enumeration keys = p.keys();
		// while( keys.hasMoreElements() ) {
		// System.out.println( keys.nextElement() );
		// }

		HPMain hpMain = new HPMain();

		String pathLog4j = getAppPath() + PATHLOG4J;
		// String pathLog4j = PATHLOG4J;

		PropertyConfigurator.configure(pathLog4j);
		String session = getSessionArg(args); // "n100990.merann.ru:0";
		// String session = "n100990.merann.ru:0";

		try {
			hpMain.connect(APP_NAME, session);
			hpMain.addCallback();
			hpMain.init();
		} catch (Exception e) {
			log.fatal("Cannot connect to HP OpenView. Seesion: " + session + ". ", e);
			SNMPNotify.getInstance().sendMessage(EnumNotifyType.ERROR,
			        "Cannot connect" + " to HP Open View (for the more information see a log file)");
			System.exit(1);
		}

		try {
			Configuration.getInstance().loadXML(getAppPath() + PATHXML, getAppPath() + PATHXSD);
			// Configuration.getInstance().loadXML(PATHXML, PATHXSD);
		} catch (Exception e) {
			log.fatal("Cannot load XML file. ", e);
			SNMPNotify.getInstance().sendMessage(EnumNotifyType.ERROR,
			        "Cannot load XML file " + "(for the more information see a log file)");
			System.exit(1);
		}

		// Create the auditor
		Auditor.runAudit();
		// a.resume();

		hpMain.waitForDisconnect();

		log.debug("The END");
		System.exit(0);
	}

	/**
	 * @return a full application path
	 */
	private static String getAppPath() {
		String jarPath = System.getProperty("java.class.path");
		jarPath = jarPath.replace("\\", "/");
		int endIndex = jarPath.lastIndexOf('/');
		return jarPath.substring(0, endIndex) + "/";
	}

	/**
	 * Parsing command line arguments of the application.
	 * 
	 * @param args
	 *            a list of strings.
	 * 
	 * @return Session's number in format "HostName:SessionNumber".
	 */
	static String getSessionArg(String args[]) {
		String session = "localhost";
		for (int a = 0; a < args.length; a++) {
			if (args[a].equalsIgnoreCase("-session") && ((a + 1) < args.length)) {
				session = args[a + 1];
				break;
			}
		}
		int colon = session.indexOf(":");
		if (colon > 0) {
			String colonStr = session.substring(colon);
			int dotAfterColon = colonStr.indexOf(".");
			if (dotAfterColon > 0) {
				session = session.substring(0, colon + dotAfterColon);
			}
		}
		return session;
	}

	/**
	 * Establishing connection with HPOpenView application.
	 * 
	 * @param className
	 *            the application name from a registration file of HPOpenView.
	 * @param session
	 *            a string in format "HostName:SessionNumber".
	 * 
	 * @return TRUE if the connection is success.
	 * 
	 * @throws LibOvwException
	 *             If the session was not opened.
	 */
	private void connect(String className, String session) throws LibOvwException {
		if (log.isInfoEnabled()) {
			log.info("Try to connect...: " + session);
		}
		HPLibovw.getInstance().OVwInitSession(className, session);
		if (log.isInfoEnabled()) {
			log.info("Connection is success");
		}
	}

	/**
	 * Disconnect with HPOpenView and finishes work of the application.
	 */
	protected void disconnect() {
		HPLibovw.getInstance().OVwDone();
		if (log.isInfoEnabled()) {
			log.info("Disconnected from the HP Open View");
		}
	}

	/**
	 * Terminates a main tread when HPOpenView is closing.
	 */
	protected void waitForDisconnect() {
		while (HPLibovw.getInstance() != null) {
			try {
				Thread.sleep(1000);
			} catch (Exception ex) {
				log.fatal("Error from WaitForDisconnect");
			}
		}
	}

	/**
	 * Initialization of the application.
	 * 
	 * @throws LibOvwException
	 *             if can not get options of the application.
	 */
	protected void init() throws LibOvwException {
		OVwFieldBindList configParams = HPLibovw.getInstance().OVwGetAppConfigValues(
		        HPLibovw.getInstance().OVwGetMapInfo(), APP_NAME);
		changeConfig(configParams);
	}

	/**
	 * Applies are new parameters when a user press an "Aplly" button in HP
	 * dialog.
	 * 
	 * @param configParams
	 *            are new parameters from HP OpenView dialog.
	 */
	protected void changeConfig(OVwFieldBindList configParams) {

		for (int i = 0; i < configParams.count; i++) {
			String fieldName = HPLibovw.getInstance().OVwDbFieldIdToFieldName(configParams.fields[i].field_id);
			EnumHPField field = EnumHPField.getValueByFieldName(fieldName);
			if (field == null)
				continue;

			switch (field) {
			case FLD_SYNAPNETWORKLAYOUT:
				networkLayout = configParams.fields[i].field_val.enum_val;
				break;
			case FLD_SYNAPNODELAYOUT:
				nodeLayout = configParams.fields[i].field_val.enum_val;
				break;
			case FLD_SYNAPSENSORKLAYOUT:
				sensorLayout = configParams.fields[i].field_val.enum_val;
				break;
			case FLD_SYNAPNETPOLLINTERVAL:
				pollTime = configParams.fields[i].field_val.int_val;
				break;
			}
		}
		if (log.isDebugEnabled()) {
			log.debug("Configuration has been changed: " + this);
		}
	}

	/**
	 * Adding call back functions to the application
	 * 
	 * @throws LibOvwException
	 *             if cannot add call back functions
	 */
	public void addCallback() throws LibOvwException {

		long mask = OVwEvents.ovwUserSubmapCreateMask | OVwEvents.ovwSubmapCloseMask
		        | OVwEvents.ovwQueryDescribeChangeMask | OVwEvents.ovwQueryAppConfigChangeMask
		        | OVwEvents.ovwQueryAddSymbolMask | OVwEvents.ovwMapCloseMask | OVwEvents.ovwEndSessionMask
		        | OVwEvents.ovwConfirmUnmanageObjectsMask | OVwEvents.ovwConfirmManageObjectsMask
		        | OVwEvents.ovwConfirmDescribeChangeMask | OVwEvents.ovwConfirmDeleteSymbolsMask
		        | OVwEvents.ovwConfirmAppConfigChangeMask | OVwEvents.ovwConfirmAddSymbolMask;

		HPLibovw.getInstance().OVwAddCallback(mask, this);
		HPLibovw.getInstance().OVwAddActionCallback(SNMPOPEN_ACTION, this);
	}

	/**
	 * Gets an array of not tabular <code>HPObject</code> instances.
	 * 
	 * @param synapsnmp
	 *            an <code>SynapSNMP</code> instance.
	 * 
	 * @return an array of <code>HPObject</code> instances.
	 */
	private ArrayList<HPObject> getSimpleObjects(SynapSNMP synapsnmp) {

		ArrayList<HPObject> hpObjectArray = new ArrayList<HPObject>();
		int count = 3;
		HPObject[] hpObjects = new HPObject[count];
		EnumHPSymbol[] hpSymbols = { EnumHPSymbol.SYNAPSNMPAGENT, EnumHPSymbol.SYNAPDATABASE,
		        EnumHPSymbol.SYNAPWEBSERVER };
		EnumHPField[] hpFields = { EnumHPField.FLD_IS_SYNAPSNMPAGENT, EnumHPField.FLD_IS_SYNAPDATABASE,
		        EnumHPField.FLD_IS_SYNAPWEBSERVER };

		// Create software symbols
		for (int i = 0; i < count; i++) {
			// Create new software symbol
			hpObjects[i] = new HPObject();
			hpObjects[i].setSymbolType(hpSymbols[i]);
			hpObjects[i].setLabel(hpSymbols[i].getObjectName());
			hpObjects[i].addFieldBinding(hpFields[i], OVwFieldValue.ovwBooleanField, new Boolean(true));
		}

		// Getting data from SNM agent
		for (int i = 0; i < count; i++) {
			XMLObject xmlObject = Configuration.getInstance().getXMLObjectByName(hpSymbols[i]);
			synapsnmp.clearPDU();
			synapsnmp.addVariable(xmlObject.getOIDs()[0]);
			try {
				String response = synapsnmp.snmpGet();
				if (response.isEmpty()) {
					hpObjects[i].setStatus(OVwSymbolInfo.ovwCriticalStatus);
				} else if (response.indexOf("Not") != -1) {
					hpObjects[i].setStatus(OVwSymbolInfo.ovwCriticalStatus);
				}
			} catch (Exception e) {
				log.error("The object: " + xmlObject + " doesn't answer. Exception: " + e);
				hpObjects[i].setStatus(OVwSymbolInfo.ovwCriticalStatus);
			}
		}

		// Add symbols to array
		for (int i = 0; i < count; i++) {
			hpObjectArray.add(hpObjects[i]);
		}
		return hpObjectArray;
	}

	/**
	 * Gets an array of the tabular <code>HPObject</code> instances.
	 * 
	 * @param snmpObjectList
	 *            a list of SNMP objects.
	 * @param symbolType
	 *            a type of symbol.
	 */
	private ArrayList<HPObject> getTableObjects(ArrayList<SynapObject> snmpObjectList, EnumHPSymbol hpSymbol) {
		ArrayList<HPObject> hpObjectArray = new ArrayList<HPObject>();

		SynapObject synapObject;
		int count = snmpObjectList.size();

		for (int i = 0; i < count; i++) {
			// Get SynapObject From ArrayList
			synapObject = snmpObjectList.get(i);

			// Get reference ID From

			int refObjectID = synapObject.getXMLObject().getRefObjectId();
			int refParentID = synapObject.getXMLObject().getRefParentId();
			int refNameID = synapObject.getXMLObject().getRefNameId();

			String label = (String) synapObject.getValueByXmlObjectID(refNameID);
			if (label.isEmpty())
				label = hpSymbol.toString();

			HPObject hpObject = new HPObject();
			hpObject.setLabel(label);
			hpObject.setSymbolType(hpSymbol);

			switch (hpSymbol) {
			case SYNAPNETWORK:
				hpObject.addFieldBinding(EnumHPField.FLD_NETWORK_ID, OVwFieldValue.ovwIntField,
				        synapObject.getValueByXmlObjectID(refObjectID));
				hpObject.addFieldBinding(EnumHPField.FLD_IS_SYNAPNETWORK, OVwFieldValue.ovwBooleanField, new Boolean(
				        true));
				break;
			case SYNAPNODE:
				hpObject.addFieldBinding(EnumHPField.FLD_NETWORK_ID, OVwFieldValue.ovwIntField,
				        synapObject.getValueByXmlObjectID(refParentID));
				hpObject.addFieldBinding(EnumHPField.FLD_NODE_ID, OVwFieldValue.ovwIntField,
				        synapObject.getValueByXmlObjectID(refObjectID));

				hpObject.addFieldBinding(EnumHPField.FLD_IS_SYNAPNODE, OVwFieldValue.ovwBooleanField, new Boolean(true));
				break;
			case SYNAPSENSOR:
				hpObject.addFieldBinding(EnumHPField.FLD_NODE_ID, OVwFieldValue.ovwIntField,
				        synapObject.getValueByXmlObjectID(refParentID));
				hpObject.addFieldBinding(EnumHPField.FLD_SENSOR_ID, OVwFieldValue.ovwIntField,
				        synapObject.getValueByXmlObjectID(refObjectID));
				hpObject.addFieldBinding(EnumHPField.FLD_IS_SYNAPSENSOR, OVwFieldValue.ovwBooleanField, new Boolean(
				        true));
				break;
			default:
				throw new IllegalArgumentException(hpSymbol + " is not a table object");
			}

			hpObjectArray.add(hpObject);
		}
		return hpObjectArray;
	}

	/**
	 * Determines a configuration was changed or not when a user press an
	 * "Apply" button in HP dialog of the application settings.
	 * 
	 * @param configParams
	 *            are new parameters from HP OpenView dialog.
	 * 
	 * @return TRUE if <code>configParams</code> were changed.
	 */
	protected boolean isConfigChange(OVwFieldBindList configParams) {
		for (int i = 0; i < configParams.count; i++) {
			String fieldName = HPLibovw.getInstance().OVwDbFieldIdToFieldName(configParams.fields[i].field_id);
			EnumHPField field = EnumHPField.getValueByFieldName(fieldName);
			if (field == null)
				continue;

			switch (field) {
			case FLD_SYNAPNETWORKLAYOUT:
				if (networkLayout != configParams.fields[i].field_val.enum_val)
					return true;
				break;
			case FLD_SYNAPNODELAYOUT:
				if (nodeLayout != configParams.fields[i].field_val.enum_val)
					return true;
				break;
			case FLD_SYNAPSENSORKLAYOUT:
				if (sensorLayout != configParams.fields[i].field_val.enum_val)
					return true;
				break;
			case FLD_SYNAPNETPOLLINTERVAL:
				if (pollTime != configParams.fields[i].field_val.int_val)
					return true;
				break;
			}
		}
		return false;
	}

	/**
	 * Shows dialog.
	 * 
	 * @param symbol
	 *            an <code>OVwSymbolInfo</code> instance.
	 * 
	 * @throws IOException
	 *             if SNMP connection is not established.
	 * @throws HPErrorException
	 *             if remote host doesn't answer or timeout occurred.
	 * @throws HPWarnException
	 *             if SNMP get command return a NULL value.
	 * @throws InterruptedException
	 *             if can't retrieve the data from the SNMP agent.
	 */
	protected void showHPDialog(OVwSymbolInfo symbol) throws IOException, HPErrorException, HPWarnException,
	        InterruptedException {

		HPObject hpObject = null;
		ObjectFiller objectFiller;
		XMLObject xmlObject = null;

		OID lowerBoundIndex = null;
		OID upperBoundIndex = null;

		String netID, netIDPrev, nodeID, nodeIDPrev, sensorID;

		EnumHPSymbol hpSymbol = EnumHPSymbol.getValueBySymbolType(symbol.symbol_type);
		hpObject = new HPObject(symbol);
		switch (hpSymbol) {

		case SYNAPNETWORK:
			netID = hpObject.getHPFieldBindList().getStringValue(EnumHPField.FLD_NETWORK_ID);
			netIDPrev = "" + (Integer.parseInt(netID) - 1);
			if (netIDPrev.equals("-1"))
				netIDPrev = "0";
			lowerBoundIndex = new OID(netIDPrev);
			upperBoundIndex = new OID(netID);
			break;

		case SYNAPNODE:
			nodeID = hpObject.getHPFieldBindList().getStringValue(EnumHPField.FLD_NODE_ID);
			netID = hpObject.getHPFieldBindList().getStringValue(EnumHPField.FLD_NETWORK_ID);
			netIDPrev = "" + (Integer.parseInt(netID) - 1);
			lowerBoundIndex = new OID(nodeID + "." + netIDPrev);
			upperBoundIndex = new OID(nodeID + "." + netID);
			break;
		case SYNAPSENSOR:
			sensorID = hpObject.getHPFieldBindList().getStringValue(EnumHPField.FLD_SENSOR_ID);
			nodeID = hpObject.getHPFieldBindList().getStringValue(EnumHPField.FLD_NODE_ID);
			nodeIDPrev = "" + (Integer.parseInt(nodeID) - 1);
			lowerBoundIndex = new OID(sensorID + "." + nodeIDPrev);
			upperBoundIndex = new OID(sensorID + "." + nodeID);
			break;
		}

		xmlObject = Configuration.getInstance().getXMLObjectByName(hpSymbol);

		SNMPObject snmpObject = null;
		HPObject parentHPObject = hpObject.getParentHPObject();

		if (parentHPObject == null) {
			log.warn("Cannot find a parent object from: " + hpObject);
			throw new HPWarnException("Cannot find a parent object from: " + hpObject);
		}

		snmpObject = SNMPFactory.getSNMPObject(parentHPObject);

		// Connect to SNMP agent
		SynapSNMP synapsnmp = new SynapSNMP(snmpObject);
		synapsnmp.connect();

		SynapObject synapObject = null;
		if (hpSymbol.isSimple()) {
			synapObject = synapsnmp.fillSimpleObject(xmlObject);
		} else {
			// Create a new ObjectFiller and set filter
			objectFiller = new ObjectFiller(xmlObject);
			synapsnmp.fillTableObject(objectFiller, lowerBoundIndex, upperBoundIndex);
			ArrayList<SynapObject> listObject = objectFiller.getListSynapObject();
			if (listObject.size() != 0) {
				synapObject = listObject.get(0);
			}
		}
		synapsnmp.disconnect();

		if (synapObject == null) {
			log.error("No data returned from object:" + xmlObject);
			throw new HPWarnException("No data returned from object: " + xmlObject.getName());
		}

		// Show dialog
		Frame parent = new Frame();
		HPDialog dlg = new HPDialog(snmpObject, hpObject, synapObject, parent, "SNMP data", false);
		dlg.setAlwaysOnTop(true);
		dlg.setVisible(true);
	}

	/**
	 * Creates a new submap when user double-click on the symbol
	 * 
	 * @param symbol
	 *            an <code>OVwSymbolInfo</code> instance on which user
	 *            double-click
	 * 
	 * @throws IOException
	 *             if SNMP connection is not established.
	 * @throws InterruptedException
	 *             if can't retrieve the data from the SNMP agent.
	 * @throws HPWarnException
	 *             if SNMP get command return a NULL value.
	 */
	protected void UserSubmapCreate(OVwSymbolInfo symbol) throws IOException, InterruptedException, HPWarnException {

		String err;
		HPObject hpObject = null;
		ObjectFiller objectFiller;
		XMLObject xmlObject = null;
		String[] filter = null;

		EnumHPSymbol hpSymbol = null;

		int refParentId;
		int layout = 0;

		int submapId = LibOvw.ovwNullSubmapId;

		hpSymbol = EnumHPSymbol.getValueBySymbolType(symbol.symbol_type);
		hpObject = new HPObject(symbol);
		xmlObject = Configuration.getInstance().getXMLObjectByName(hpSymbol.getChildSymbol());

		switch (hpSymbol) {
		case SYNAPPCSNMP3:
			layout = networkLayout;
			break;
		case SYNAPPCSNMP2C:
			layout = networkLayout;
			break;

		case SYNAPNETWORK:
			layout = nodeLayout;

			// Create filter
			refParentId = xmlObject.getRefParentId();
			int netIDIndex = xmlObject.getIndexByXmlObjectId(refParentId);
			String netID = hpObject.getHPFieldBindList().getStringValue(EnumHPField.FLD_NETWORK_ID);

			filter = new String[netIDIndex + 1];
			for (int i = 0; i < netIDIndex + 1; i++)
				filter[i] = "";
			filter[netIDIndex] = netID;
			break;

		case SYNAPNODE:
			layout = sensorLayout;

			// Create filter
			refParentId = xmlObject.getRefParentId();
			int nodeIDIndex = xmlObject.getIndexByXmlObjectId(refParentId);
			String nodeID = hpObject.getHPFieldBindList().getStringValue(EnumHPField.FLD_NODE_ID);

			filter = new String[nodeIDIndex + 1];
			for (int i = 0; i < nodeIDIndex + 1; i++)
				filter[i] = "";
			filter[nodeIDIndex] = nodeID;
			break;

		default:
			err = "Symbol [" + hpSymbol + "] mustn't have any childs elements";
			log.warn(err);
			throw new HPWarnException(err);
		}

		SNMPObject snmpObject = null;
		HPObject parentHPObject = hpObject.getParentHPObject();
		if (log.isDebugEnabled()) {
			log.debug("Parent HP Object: " + parentHPObject);
		}

		if (parentHPObject == null) {
			err = "Cannot find a parent object from: " + hpObject;
			log.warn(err);
			throw new HPWarnException(err);
		}

		snmpObject = SNMPFactory.getSNMPObject(parentHPObject);

		// Connect to SNMP agent
		SynapSNMP synapsnmp = new SynapSNMP(snmpObject);

		// Create connect to SynapSNMPAgent
		synapsnmp.connect();

		// Create a new ObjectFiller and set filter
		objectFiller = new ObjectFiller(xmlObject);
		objectFiller.setFilter(filter);

		// Create a new submap
		submapId = HPLibovw.getInstance().OVwCreateSubmap(HPLibovw.getInstance().OVwGetMapInfo(), hpObject.getId(),
		        OVwSubmapInfo.ovwExclusiveSubmap, 0, hpObject.getLabel(), layout, OVwSubmapInfo.ovwNoSubmapFlags);

		synapsnmp.fillTableObject(objectFiller, null, null);

		// Create software symbols
		if (hpSymbol.getLevel() == 0) {
			HPUtils.drawIconSymbols(submapId, getSimpleObjects(synapsnmp));
		}

		synapsnmp.disconnect();

		// Draw icon symbols
		HPUtils.drawIconSymbols(submapId, getTableObjects(objectFiller.getListSynapObject(), hpSymbol.getChildSymbol()));

		// draw connection symbols
		HPUtils.drawConnectSymbols(submapId, layout);

		// Open a new submap
		HPLibovw.getInstance().OVwDisplaySubmap(HPLibovw.getInstance().OVwGetMapInfo(), submapId);

		// Close a parent symbol submap
		HPLibovw.getInstance().OVwCloseSubmap(HPLibovw.getInstance().OVwGetMapInfo(), symbol.submap_id);

		// Answer
		HPLibovw.getInstance().OVwAckUserSubmapCreate(HPLibovw.getInstance().OVwGetMapInfo(), submapId,
		        symbol.symbol_id);

	}

	/**
	 * Checks the object and to set a symbol status.
	 * 
	 * @param hpObject
	 *            an <code>HPObject</code> instance is the checked object.
	 * 
	 * @throws IOException
	 *             if SNMP connection is not established.
	 * @throws HPWarnException
	 *             if SNMP get command return a NULL value.
	 * @throws HPErrorException
	 *             if remote host doesn't answer or timeout occurred.
	 */
	protected void confirmManageObject(HPObject hpObject) throws IOException, HPWarnException, HPErrorException {

		HPObject parentHPObject = hpObject.getParentHPObject();
		if (parentHPObject != null) {

			SNMPObject snmpObject = SNMPFactory.getSNMPObject(parentHPObject);

			if (hpObject.getHPSymbol().getLevel() == 0) {
				tryConnect(hpObject.getHPSymbol(), hpObject.getHPFieldBindList().getFieldBindList());
				return;
			}

			SynapSNMP synapsnmp = new SynapSNMP(snmpObject);
			synapsnmp.connect();

			XMLObject xmlObject = Configuration.getInstance().getXMLObjectByName(hpObject.getHPSymbol());

			if (hpObject.getHPSymbol().isSimple()) {
				synapsnmp.clearPDU();
				synapsnmp.addVariable(xmlObject.getOIDs()[0]);
				String response = synapsnmp.snmpGet();
				if (response.isEmpty()) {
					throw new HPWarnException("SNMP GET command response NULL");
				}

				if (response.indexOf("Not") != -1) {
					throw new HPWarnException("Not accessible");
				}
			} else {
				OID oid = xmlObject.getOIDs()[0];
				int netId = 0, nodeId = 0, sensorId = 0;
				boolean device = false;

				switch (hpObject.getHPSymbol()) {
				case SYNAPSENSOR:
					sensorId = Integer
					        .parseInt(hpObject.getHPFieldBindList().getStringValue(EnumHPField.FLD_SENSOR_ID));
					device = true;
				case SYNAPNODE:
					nodeId = Integer.parseInt(hpObject.getHPFieldBindList().getStringValue(EnumHPField.FLD_NODE_ID));
					if (device) {
						oid.append(sensorId);
						oid.append(nodeId);
						break;
					}
					device = true;
				case SYNAPNETWORK:
					netId = Integer.parseInt(hpObject.getHPFieldBindList().getStringValue(EnumHPField.FLD_NETWORK_ID));
					if (device) {
						oid.append(nodeId);
						oid.append(netId);
						break;
					}
					oid.append(netId);
				}
				synapsnmp.clearPDU();
				synapsnmp.addVariable(oid);
				String response = synapsnmp.snmpGet();
				if (response.isEmpty()) {
					throw new HPWarnException("SNMP GET command response NULL");
				}
			}
		}
	}

	/**
	 * Try to connect by specified parameters.
	 * 
	 * @param hpSymbol
	 *            an <code>EnumHPSymbol</code> instance.
	 * @param hpFieldBindList
	 *            an <code>HPFieldBindList</code> instance.
	 * 
	 * @throws IOException
	 *             if SNMP connection is not established.
	 * @throws HPWarnException
	 *             if SNMP get command return a NULL value.
	 * @throws HPErrorException
	 *             if remote host doesn't answer or timeout occurred.
	 */
	private void tryConnect(EnumHPSymbol hpSymbol, OVwFieldBindList bindList) throws IOException, HPWarnException,
	        HPErrorException {

		if (log.isDebugEnabled()) {
			log.debug("Try of connection with SNMP Agent...");
		}

		HPFieldBindList hpFieldBindList = new HPFieldBindList();
		hpFieldBindList.addFieldBindingList(bindList);

		SNMPObject snmpObject = null;
		String address = hpFieldBindList.getStringValue(EnumHPField.FLD_ADDRESS);
		int port = Integer.parseInt(hpFieldBindList.getStringValue(EnumHPField.FLD_PORT));
		EnumSNMPTransport transport = EnumSNMPTransport.valueOf(hpFieldBindList
		        .getStringValue(EnumHPField.FLD_SNMPTRANSPORT));
		int timeout = Integer.parseInt(hpFieldBindList.getStringValue(EnumHPField.FLD_SNMPTIMEOUT));
		int retries = Integer.parseInt(hpFieldBindList.getStringValue(EnumHPField.FLD_SNMPRETRIES));

		String temp;
		switch (hpSymbol) {
		case SYNAPPCSNMP2C:
			OctetString communityPublic = null;
			OctetString communityPrivate = null;
			communityPublic = new OctetString(hpFieldBindList.getStringValue(EnumHPField.FLD_COMMUNITYPUBLIC));
			temp = hpFieldBindList.getStringValue(EnumHPField.FLD_COMMUNITYPRIVATE);
			if (temp != null)
				communityPrivate = new OctetString(temp);
			snmpObject = new SNMPv2c(address, port, transport, communityPublic, communityPrivate);
			break;
		case SYNAPPCSNMP3:
			OctetString securityName = null;
			OctetString contextName = null;

			EnumSNMPAuthProtocol authProtocol;
			OctetString authPassword = null;

			EnumSNMPPrivProtocol privProtocol;
			OctetString privPassword = null;

			securityName = new OctetString(hpFieldBindList.getStringValue(EnumHPField.FLD_SNMPSECNAME));
			contextName = new OctetString(hpFieldBindList.getStringValue(EnumHPField.FLD_SNMPCONTEXTNAME));

			authProtocol = EnumSNMPAuthProtocol.valueOf(hpFieldBindList
			        .getStringValue(EnumHPField.FLD_SNMPAUTHPROTOCOL));
			temp = hpFieldBindList.getStringValue(EnumHPField.FLD_SNMPAUTHPWD);
			if (temp != null)
				authPassword = new OctetString(temp);

			privProtocol = EnumSNMPPrivProtocol.valueOf(hpFieldBindList
			        .getStringValue(EnumHPField.FLD_SNMPPRIVPROTOCOL));
			temp = hpFieldBindList.getStringValue(EnumHPField.FLD_SNMPPRIVPWD);
			if (temp != null)
				privPassword = new OctetString(temp);

			snmpObject = new SNMPv3(address, port, transport, securityName, contextName, authProtocol, authPassword,
			        privProtocol, privPassword);
			break;
		}

		snmpObject.setTimeoutAndRetries(timeout, retries);
		if (log.isDebugEnabled()) {
			log.debug("A new SNMPObject was created: " + snmpObject);
		}

		// Connect to SNMP agent
		SynapSNMP synapsnmp = new SynapSNMP(snmpObject);
		synapsnmp.connect();
		XMLObject xmlObject = Configuration.getInstance().getXMLObjectByName(EnumHPSymbol.SYNAPSNMPAGENT);
		// Try to get information from SNMP agent
		for (int i = 0; i < xmlObject.getArrayXMLProperty().size(); i++) {
			XMLProperty xmlProperty = xmlObject.getArrayXMLProperty().get(i);
			synapsnmp.clearPDU();
			synapsnmp.addVariable(xmlObject.getOIDById(xmlProperty.getId()));
			String response = synapsnmp.snmpGet();
			if (response.isEmpty()) {
				throw new HPWarnException("SNMP GET command response NULL");
			}
		}
		synapsnmp.disconnect();
	}

	/**
	 * Checks the parameters when a user adds a new symbol on the map.
	 * 
	 * @param capabilityFields
	 *            a capability fields.
	 * @param dialogBoxFields
	 *            a HP Open View symbol dialog box fields.
	 * 
	 * @throws LibOvwException
	 *             if any capability field no exists in database.
	 * @throws IOException
	 *             if SNMP connection is not established.
	 * @throws HPWarnException
	 *             if fields in the dialog box not filled.
	 * @throws HPErrorException
	 *             if remote host doesn't answer or timeout occurred.
	 */
	protected void queryAddSymbol(OVwFieldBindList capabilityFields, OVwFieldBindList dialogBoxFields)
	        throws LibOvwException, IOException, HPWarnException, HPErrorException {

		for (int i = 0; i < capabilityFields.count; i++) {
			String fieldName = HPLibovw.getInstance().OVwDbFieldIdToFieldName(capabilityFields.fields[i].field_id);
			EnumHPField hpField = EnumHPField.getValueByFieldName(fieldName);
			if (hpField == null)
				continue;

			if (hpField == EnumHPField.FLD_IS_SYNAPPCSNMP2C || hpField == EnumHPField.FLD_IS_SYNAPPCSNMP3) {

				EnumHPSymbol hpSymbol = null;

				switch (hpField) {
				case FLD_IS_SYNAPPCSNMP2C:
					hpSymbol = EnumHPSymbol.SYNAPPCSNMP2C;
					break;
				case FLD_IS_SYNAPPCSNMP3:
					hpSymbol = EnumHPSymbol.SYNAPPCSNMP3;
					break;
				}
				String fields = HPUtils.getEmptyFields(dialogBoxFields);
				if (!fields.isEmpty()) {
					throw new HPWarnException("Next fields should be entered: " + fields);
				}

				// Try to connect
				tryConnect(hpSymbol, dialogBoxFields);
			}
		}
	}

	/**
	 * Deletes symbols, a submap and all objects from database when a user is
	 * closing submap.
	 * 
	 * @param submap
	 *            a closing submap
	 * 
	 * @throws HPWarnException
	 *             if a closing submap has a child and cannot be deleted.
	 * @throws LibOvwException
	 *             if a submap cannot be deleted.
	 */
	protected void submapClose(OVwSubmapInfo submap) throws LibOvwException {

		if (APP_NAME.equals(submap.app_name)) {
			OVwSymbolList symbolList = HPLibovw.getInstance().OVwListSymbols(HPLibovw.getInstance().OVwGetMapInfo(),
			        submap.submap_id, OVwSymbolInfo.ovwAppPlane, APP_NAME);

			// If symbol has a child then do nothing
			for (int i = 0; i < symbolList.count; i++) {
				OVwSymbolInfo symbol = symbolList.symbols[i];
				if (symbol.object.child_submap_id != 0) {
					String msg = "Cannot delete a submap [" + submap.submap_name + "] " + "because symbol ["
					        + symbol.symbol_label + "] has a child";
					if (log.isDebugEnabled()) {
						log.debug(msg);
					}
					return;
				}
			}

			ArrayList<Integer> objectIdList = new ArrayList<Integer>();
			long count = 0;

			// Delete symbols from the submap
			for (int i = 0; i < symbolList.count; i++) {
				OVwSymbolInfo symbol = symbolList.symbols[i];
				EnumHPSymbol hpSymbol = EnumHPSymbol.getValueBySymbolType(symbol.symbol_type);
				if (hpSymbol == null)
					continue;
				HPLibovw.getInstance().OVwDeleteSymbol(HPLibovw.getInstance().OVwGetMapInfo(), symbol.symbol_id);
				objectIdList.add(symbol.object.object_id);
				count++;
			}
			if (log.isDebugEnabled()) {
				log.debug("Delete " + count + " symbols");
			}

			// Delete a submap
			HPLibovw.getInstance().OVwDeleteSubmap(HPLibovw.getInstance().OVwGetMapInfo(), submap.submap_id);
			if (log.isDebugEnabled()) {
				log.debug("Delete submap: [" + submap.submap_name + "]");
			}

			// Delete objects from database
			count = 0;
			for (int i = 0; i < objectIdList.size(); i++) {
				try {
					OVwFieldBindList fieldList = HPLibovw.getInstance().OVwDbGetFieldValues(objectIdList.get(i));
					for (int j = 0; j < fieldList.count; j++) {
						String fieldName = HPLibovw.getInstance().OVwDbFieldIdToFieldName(fieldList.fields[j].field_id);
						EnumHPField hpField = EnumHPField.getValueByFieldName(fieldName);
						if (hpField == null)
							continue;

						if (hpField.isAppField()) {
							try {
								HPLibovw.getInstance().OVwDbUnsetFieldValue(objectIdList.get(i),
								        fieldList.fields[j].field_id);
							} catch (LibOvwException e) {
								log.warn("Error unset field: [" + fieldName + "]");
								continue;
							}
						}
					}
					HPLibovw.getInstance().OVwDbDeleteObject(objectIdList.get(i));
					count++;
				} catch (LibOvwException e) {
					continue;
				}
			}
			if (log.isDebugEnabled()) {
				log.debug("Delete " + count + " objects");
			}
		}
	}

	/**
	 * Checks parameters when a user click on "OK" button in the
	 * "Object properties" dialog box.
	 * 
	 * @param object
	 *            an edit <code>OVwObjectInfo</code> instance.
	 * @param dialogBoxFields
	 *            a dialog box fields.
	 * 
	 * @return a result string of checking.
	 * 
	 * @throws LibOvwException
	 *             if any capability field no exists in database.
	 * @throws IOException
	 *             if SNMP connection is not established.
	 * @throws HPWarnException
	 *             if not all fields was entered.
	 * @throws HPErrorException
	 *             if remote host doesn't answer or timeout occurred.
	 */
	protected String QueryDescribe(OVwObjectInfo object, OVwFieldBindList dialogBoxFields) throws LibOvwException,
	        IOException, HPWarnException, HPErrorException {

		OVwSymbolList symbolList = HPLibovw.getInstance().OVwGetSymbolsByObject(HPLibovw.getInstance().OVwGetMapInfo(),
		        object.object_id);

		for (int i = 0; i < symbolList.count; i++) {
			OVwSymbolInfo symbol = symbolList.symbols[i];
			EnumHPSymbol hpSymbol = EnumHPSymbol.getValueBySymbolType(symbol.symbol_type);
			if (hpSymbol == null)
				continue;

			if (hpSymbol.getLevel() == 0) {
				OVwFieldBindList objectList = HPLibovw.getInstance().OVwDbGetFieldValues(object.object_id);

				if (HPUtils.isFieldsChanged(dialogBoxFields, objectList)) {

					String fields = HPUtils.getEmptyFields(dialogBoxFields);
					if (!fields.isEmpty()) {
						throw new HPWarnException("Next fields should be entered: " + fields);
					}

					tryConnect(hpSymbol, dialogBoxFields);
					return "This information is valid.  Press OK to continue";
				} else {
					return ("The information is not changed. Press OK to continue");
				}

			}
		}
		return "";
	}

	/**
	 * @return a network polling time.
	 */
	public static int getPoolTime() {
		return pollTime;
	}

	/**
	 * @return a network layout.
	 */
	protected int getNetworkLayout() {
		return networkLayout;
	}

	/**
	 * @return a node layout.
	 */
	protected int getNodeLayout() {
		return nodeLayout;
	}

	/**
	 * @return a sensor layout.
	 */
	protected int getSensorLayout() {
		return sensorLayout;
	}

	@Override
	public String toString() {
		return "{networkLayout=" + networkLayout + ", nodeLayout=" + nodeLayout + ", sensorLayout=" + sensorLayout
		        + ", pollTime=" + pollTime + "}";
	}

	// ///////////////////////////////////////////////////////////
	// EVENTS
	// ///////////////////////////////////////////////////////////

	@Override
	public void OVwAddActionCB(String actionId, String menuItemId, OVwObjectIdList selected, int argc,
	        StringVectorParm argv, OVwMapInfo map, int submapId) {
		if (SNMPOPEN_ACTION.equals(actionId)) {
			OVwSymbolList symbolList = HPLibovw.getInstance().OVwGetSymbolsByObject(map, selected.object_ids[0]);
			if (log.isDebugEnabled()) {
				log.debug("\"" + SNMPOPEN_ACTION + "\" action with " + HPUtils.symbolToString(symbolList.symbols[0]));
			}
			try {
				showHPDialog(symbolList.symbols[0]);
			} catch (Exception e) {
				log.warn("Cannot show a dialog. ", e);
				JOptionPane.showMessageDialog(null, "Cannot open a dialog box. " + e.getLocalizedMessage(), "Warning",
				        JOptionPane.WARNING_MESSAGE);
			}
		}
	}

	@Override
	public void OVwConfirmAddSymbolCB(OVwMapInfo map, OVwSymbolInfo symbol, OVwFieldBindList capabilityFields,
	        OVwFieldBindList dialogBoxFields) {

		EnumHPSymbol hpSymbol = EnumHPSymbol.getValueBySymbolType(symbol.symbol_type);
		if (hpSymbol == null)
			return;

		// Get submap info
		OVwSubmapInfo info = HPLibovw.getInstance().OVwGetSubmapInfo(map, symbol.submap_id);
		HPLibovw.getInstance().OVwSetSymbolApp(map, symbol.symbol_id);
		HPLibovw.getInstance().OVwSetSymbolStatusSource(map, symbol.symbol_id, OVwSymbolInfo.ovwObjectStatusSource);
		HPLibovw.getInstance().OVwSetStatusOnObject(map, symbol.object.object_id, OVwSymbolInfo.ovwNormalStatus);

		if (info.layout_style == OVwSubmapInfo.ovwBusLayout || info.layout_style == OVwSubmapInfo.ovwRingLayout) {
			HPLibovw.getInstance().OVwCreateConnSymbol(HPLibovw.getInstance().OVwGetMapInfo(), symbol.object.object_id,
			        OVwSymbolInfo.ovwSubmapBackbone, symbol.symbol_id, null, null, OVwSymbolInfo.ovwNormalStatus,
			        OVwSymbolInfo.ovwObjectStatusSource, OVwSymbolInfo.ovwNoSymbolFlags);
		}

		String msg = "A new node was added: " + HPUtils.symbolToString(symbol);
		if (log.isInfoEnabled()) {
			log.info(msg);
		}

		SNMPNotify.getInstance().sendMessage(EnumNotifyType.INFO, msg);
	}

	@Override
	public void OVwConfirmAppConfigCB(OVwMapInfo map, OVwFieldBindList configParams) {
		changeConfig(configParams);
	}

	@Override
	public void OVwConfirmDeleteSymbolsCB(OVwMapInfo map, OVwSymbolList symbolList, int arg2) {

		for (int i = 0; i < symbolList.count; i++) {
			OVwSymbolInfo symbol = symbolList.symbols[i];
			EnumHPSymbol hpSymbol = EnumHPSymbol.getValueBySymbolType(symbol.symbol_type);
			if (hpSymbol == null)
				continue;
			try {
				OVwFieldBindList fieldList = HPLibovw.getInstance().OVwDbGetFieldValues(symbol.object.object_id);
				for (int j = 0; j < fieldList.count; j++) {
					String fieldName = HPLibovw.getInstance().OVwDbFieldIdToFieldName(fieldList.fields[j].field_id);
					EnumHPField hpField = EnumHPField.getValueByFieldName(fieldName);
					if (hpField == null)
						continue;

					if (!hpField.isSystem()) {
						try {
							HPLibovw.getInstance().OVwDbUnsetFieldValue(symbol.object.object_id,
							        fieldList.fields[j].field_id);
							if (log.isDebugEnabled()) {
								log.debug("Unset field: [" + fieldName + "]");
							}
						} catch (LibOvwException e) {
							log.warn("Error unset field: [" + fieldName + "]", e);
							continue;
						}
						HPLibovw.getInstance().OVwDbDeleteObject(symbol.object.object_id);
					}
				}
			} catch (LibOvwException e) {
				continue;
			}
		}
	}

	@Override
	public void OVwConfirmDescribeCB(OVwMapInfo map, OVwObjectInfo object, OVwFieldBindList dialogBoxFields) {
		if (dialogBoxFields.count > 0) {
			OVwSymbolList symbolList = HPLibovw.getInstance().OVwGetSymbolsByObject(map, object.object_id);
			for (int i = 0; i < symbolList.count; i++) {
				OVwSymbolInfo symbol = symbolList.symbols[i];
				EnumHPSymbol hpSymbol = EnumHPSymbol.getValueBySymbolType(symbol.symbol_type);
				if (hpSymbol == null)
					continue;
				HPUtils.setSymbolStatus(symbol, OVwSymbolInfo.ovwNormalStatus);
			}
		}
	}

	@Override
	public void OVwConfirmManageObjectsCB(OVwMapInfo map, OVwObjectList objectList) {
		for (int i = 0; i < objectList.count; i++) {
			int objectId = objectList.objects[i].object_id;

			OVwFieldBindList bind = HPLibovw.getInstance().OVwDbGetFieldValues(objectId);
			OVwSymbolList symbolList = HPLibovw.getInstance().OVwGetSymbolsByObject(map, objectId);
			for (int j = 0; j < symbolList.count; j++) {
				OVwSymbolInfo symbol = symbolList.symbols[j];
				EnumHPSymbol hpSymbol = EnumHPSymbol.getValueBySymbolType(symbol.symbol_type);
				if (hpSymbol == null)
					continue;
				HPObject hpObject = new HPObject(symbol);
				hpObject.getHPFieldBindList().addFieldBindingList(bind);

				try {
					confirmManageObject(hpObject);
				} catch (Exception e) {
					log.warn(e.getMessage(), e);
					HPUtils.setSymbolStatus(symbol, OVwSymbolInfo.ovwCriticalStatus);
					continue;
				}
				HPUtils.setSymbolStatus(symbol, OVwSymbolInfo.ovwNormalStatus);
			}
		}
	}

	@Override
	public void OVwEndSessionCB(boolean normalEnd) {
		if (log.isDebugEnabled()) {
			log.debug("End session status: " + normalEnd);
		}
	}

	@Override
	public void OVwMapCloseCB(OVwMapInfo arg0, int arg1, int arg2) {
		disconnect();
	}

	@Override
	public void OVwQueryAddSymbolCB(OVwMapInfo map, OVwSubmapInfo submap, OVwFieldBindList capabilityFields,
	        OVwFieldBindList dialogBoxFields) {

		try {
			queryAddSymbol(capabilityFields, dialogBoxFields);
		} catch (Exception e) {
			log.warn(e.getMessage(), e);
			HPLibovw.getInstance().OVwVerifyAdd(map, dialogBoxFields, 0, 1, e.getLocalizedMessage());
			return;
		}

		HPLibovw.getInstance().OVwVerifyAdd(map, dialogBoxFields, 1, 1,
		        "This information is valid.  Press OK to continue");

	}

	@Override
	public void OVwQueryAppConfigCB(OVwMapInfo map, OVwFieldBindList configParams) {

		String message = "These values are valid. " + "You can modify one or more fields and press Verify "
		        + "or you can press OK to continue. ";
		if (isConfigChange(configParams)) {
			String fields = HPUtils.getEmptyFields(configParams);
			if (!fields.isEmpty()) {
				HPLibovw.getInstance().OVwVerifyAppConfigChange(map, configParams, 0,
				        "Next fields should be entered: " + fields);
				return;
			}
			message += "NOTE: If you are changing the configuration " + "of an existing map this may take a long time "
			        + "since the entire map must be read.";
		}
		HPLibovw.getInstance().OVwVerifyAppConfigChange(map, configParams, 1, message);
	}

	@Override
	public void OVwQueryDescribeCB(OVwMapInfo map, OVwObjectInfo object, OVwFieldBindList dialogBoxFields) {

		String response = "";
		try {
			response = QueryDescribe(object, dialogBoxFields);
		} catch (Exception e) {
			HPLibovw.getInstance().OVwVerifyDescribeChange(map, object, dialogBoxFields, 0, e.getLocalizedMessage());
			return;
		}

		HPLibovw.getInstance().OVwVerifyDescribeChange(map, object, dialogBoxFields, 1, response);

	}

	@Override
	public void OVwSubmapCloseCB(OVwMapInfo map, OVwSubmapInfo submap) {
		try {
			submapClose(submap);
		} catch (Exception e) {
			log.warn(e.getMessage(), e);
		}
	}

	@Override
	public void OVwUserSubmapCreateCB(OVwMapInfo map, OVwSymbolInfo symbol, OVwSubmapInfo submap) {
		try {
			UserSubmapCreate(symbol);
		} catch (Exception e) {
			log.warn(e.getMessage(), e);
			HPLibovw.getInstance().OVwAckUserSubmapCreate(HPLibovw.getInstance().OVwGetMapInfo(),
			        LibOvw.ovwNullSubmapId, symbol.symbol_id);
		}
	}
}
