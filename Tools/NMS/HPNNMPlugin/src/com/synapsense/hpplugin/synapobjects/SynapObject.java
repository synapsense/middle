/**
 * 
 */
package com.synapsense.hpplugin.synapobjects;

import java.util.ArrayList;

import com.synapsense.hpplugin.configuration.XMLObject;
import com.synapsense.hpplugin.configuration.XMLProperty;

/**
 * 
 * @author Alexander Kravin
 * 
 */
public class SynapObject {

	private String[] data;
	private XMLObject xmlObject;

	/**
	 * Initialize the instance
	 * 
	 * @param xmlObject
	 *            a <code>XMLObject</code> instance
	 */
	public SynapObject(XMLObject xmlObject) {
		this.xmlObject = xmlObject;
		data = new String[xmlObject.getArrayXMLProperty().size()];
	}

	/**
	 * Sets a value in the data by specified index
	 * 
	 * @param index
	 *            an index in the array of data
	 * @param value
	 *            a string value
	 */
	public void setValue(int index, String value) {
		int count = data.length;
		if (index < 0 || index > count)
			throw new IndexOutOfBoundsException("Index out of bounds");
		data[index] = value;
	}

	/**
	 * @return a size of array
	 */
	public int getDataSize() {
		return data.length;
	}

	/**
	 * @param xmlID
	 *            a properties' id from XML file
	 * @return a value from the data's array
	 */
	public Object getValueByXmlObjectID(int xmlID) {
		if (xmlID != 0) {
			ArrayList<XMLProperty> arrayProperty = xmlObject.getArrayXMLProperty();
			int count = arrayProperty.size();
			for (int i = 0; i < count; i++) {
				XMLProperty xmlProperty = arrayProperty.get(i);
				if (xmlProperty.getId().equals("" + xmlID)) {
					return data[i];
				}
			}
		}
		return null;
	}

	/**
	 * Gets a value by specified index
	 * 
	 * @param index
	 *            an array's index
	 * @return a value from the data's array
	 */
	public String getValue(int index) {
		if (index < 0 || index > data.length)
			throw new IndexOutOfBoundsException("Index out of bounds");
		return data[index];
	}

	/**
	 * @return a <code>XMLObject</code> instance
	 */
	public XMLObject getXMLObject() {
		return xmlObject;
	}

	@Override
	public String toString() {
		String str = "";
		ArrayList<XMLProperty> arrayProperty = xmlObject.getArrayXMLProperty();
		for (int i = 0; i < data.length; i++) {
			XMLProperty xmlProperty = arrayProperty.get(i);
			str = str + xmlProperty.getName() + ": " + data[i] + "\n";
		}
		return str;
	}
}
