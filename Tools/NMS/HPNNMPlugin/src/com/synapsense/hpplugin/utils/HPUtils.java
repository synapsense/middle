/**
 * 
 */
package com.synapsense.hpplugin.utils;

import java.util.ArrayList;

import hp.ov.libovw.LibOvw;
import hp.ov.libovw.LibOvwException;
import hp.ov.libovw.OVwFieldBindList;
import hp.ov.libovw.OVwFieldInfo;
import hp.ov.libovw.OVwFieldValue;
import hp.ov.libovw.OVwMapInfo;
import hp.ov.libovw.OVwSubmapInfo;
import hp.ov.libovw.OVwSymbolCreateInfo;
import hp.ov.libovw.OVwSymbolCreateList;
import hp.ov.libovw.OVwSymbolInfo;
import hp.ov.libovw.OVwSymbolList;

import com.synapsense.hpplugin.HPMain;
import com.synapsense.hpplugin.constants.EnumHPField;
import com.synapsense.hpplugin.constants.EnumNotifyType;
import com.synapsense.hpplugin.hpobjects.HPFieldBindList;
import com.synapsense.hpplugin.hpobjects.HPLibovw;
import com.synapsense.hpplugin.hpobjects.HPObject;
import com.synapsense.hpplugin.snmpobjects.SNMPNotify;

/**
 * @author Alexander Kravin
 * 
 */
public class HPUtils {

	/**
	 * Sets a status of the symbol.
	 * 
	 * @param symbol
	 *            an <code>OVwSymbolInfo</code> instance.
	 * @param status
	 *            a new status of the symbol.
	 * 
	 * @throws LibOvwException
	 *             if cannot change a status of the symbol.
	 */
	public static void setSymbolStatus(OVwSymbolInfo symbol, int status) throws LibOvwException {
		if (symbol.symbol_status != status) {
			OVwMapInfo map = HPLibovw.getInstance().OVwGetMapInfo();
			switch (symbol.status_source) {
			case OVwSymbolInfo.ovwSymbolStatusSource:
				HPLibovw.getInstance().OVwSetStatusOnSymbol(map, symbol.symbol_id, status);
				break;
			case OVwSymbolInfo.ovwObjectStatusSource:
				HPLibovw.getInstance().OVwSetStatusOnObject(map, symbol.object.object_id, status);
				break;
			case OVwSymbolInfo.ovwCompoundStatusSource:
				HPLibovw.getInstance().OVwSetSymbolStatusSource(map, symbol.symbol_id,
				        OVwSymbolInfo.ovwObjectStatusSource);
				HPLibovw.getInstance().OVwSetStatusOnObject(map, symbol.object.object_id, status);
				break;
			}

			switch (status) {
			case OVwSymbolInfo.ovwNormalStatus:
				SNMPNotify.getInstance().sendMessage(EnumNotifyType.INFO,
				        "Connection has restored with " + symbolToString(symbol));
				break;
			case OVwSymbolInfo.ovwCriticalStatus:
				SNMPNotify.getInstance().sendMessage(EnumNotifyType.WARN,
				        "Connection has lost with " + symbolToString(symbol));
				break;
			default:
				throw new IllegalArgumentException("Illegal a \"status\" parameter");
			}
		}
	}

	/**
	 * Concatenates two strings together
	 * 
	 * @param one
	 *            a first string
	 * @param two
	 *            a second string
	 * 
	 * @return a concatenate string
	 */
	public static String concat(String one, String two) {
		if (!one.isEmpty())
			one += ", ";
		return one + two;
	}

	/**
	 * Checks on empty fields
	 * 
	 * @param bindList
	 *            an <code>OVwFieldBindList</code> instance
	 * 
	 * @return a string of empty fields
	 */
	public static String getEmptyFields(OVwFieldBindList bindList) {
		String msg = "";

		HPFieldBindList hpFieldBindList = new HPFieldBindList();
		hpFieldBindList.addFieldBindingList(bindList);

		for (int i = 0; i < hpFieldBindList.size(); i++) {

			EnumHPField hpField = hpFieldBindList.getHPField(i);
			if (!hpField.isChecked())
				continue;

			String value = hpFieldBindList.getStringValue(hpField);

			if (value == null) {
				msg = concat(msg, "[" + hpField.getLabel() + "] should not be NULL");
			} else if (hpField.isString()) {
				if (value.isEmpty()) {
					msg = concat(msg, "[" + hpField.getLabel() + "] should not be EMPTY");
				}
			} else if (hpField.isEnumeration()) {
				if (value.equalsIgnoreCase("Unset")) {
					msg = concat(msg, "[" + hpField.getLabel() + "] should not be UNSET");
				}
			} else if (hpField.isInteger()) {
				if (Integer.parseInt(value) <= 0) {
					msg = concat(msg, "[" + hpField.getLabel() + "] should be > 0");
				}
			}
		}
		return msg;
	}

	/**
	 * Checks the fields on differences
	 * 
	 * @param dialogFields
	 *            a dialog box fields
	 * @param objectList
	 *            an object field binding list
	 * 
	 * @return TRUE if fields was changed
	 */
	public static boolean isFieldsChanged(OVwFieldBindList dialogFields, OVwFieldBindList objectList) {
		for (int i = 0; i < dialogFields.count; i++) {
			boolean find = false;
			int j;
			for (j = 0; j < objectList.count; j++) {
				if (dialogFields.fields[i].field_id == objectList.fields[j].field_id) {
					find = true;
					break;
				}
			}
			if (find) {
				OVwFieldInfo dlgInfo = HPLibovw.getInstance().OVwDbGetFieldInfo(dialogFields.fields[i].field_id);
				switch (dlgInfo.field_type) {
				case OVwFieldValue.ovwIntField:
					if (dialogFields.fields[i].field_val.int_val != objectList.fields[j].field_val.int_val) {
						return true;
					}
					break;
				case OVwFieldValue.ovwStringField:
					String strVal1 = dialogFields.fields[i].field_val.string_val;
					String strVal2 = objectList.fields[j].field_val.string_val;
					if (strVal1 != null && strVal2 != null) {
						if (!strVal1.equals(strVal2)) {
							return true;
						}
					}
					break;
				case OVwFieldValue.ovwBooleanField:
					if (dialogFields.fields[i].field_val.bool_val != objectList.fields[j].field_val.bool_val) {
						return true;
					}
					break;
				case OVwFieldValue.ovwEnumField:
					if (dialogFields.fields[i].field_val.int_val != objectList.fields[j].field_val.int_val) {
						return true;
					}
					break;
				}
			}
		}
		return false;
	}

	/**
	 * @param symbol
	 *            an <code>OVwSymbolInfo</code> instance.
	 * 
	 * @return a string which represent the specified symbol.
	 * 
	 * @throws LibOvwException
	 *             If cannot get a submap info.
	 */
	public static String symbolToString(OVwSymbolInfo symbol) throws LibOvwException {
		LibOvw libovw = HPLibovw.getInstance();
		OVwSubmapInfo submap = libovw.OVwGetSubmapInfo(libovw.OVwGetMapInfo(), symbol.submap_id);
		return "{symbol=\"" + symbol.symbol_label + "\", type=\"" + symbol.symbol_type + "\", submap=\""
		        + submap.submap_name + "\"}";
	}

	/**
	 * Draws icon symbols on the submap
	 * 
	 * @param submapId
	 *            a submap's id on which will draw symbols
	 * @param hpObjectArray
	 *            an <code>ArrayList<HPObject></code> instance consists of
	 *            objects which will be drawing
	 * 
	 * @throws LibOvwException
	 *             if cannot draw symbols on the specified submap
	 */
	public static void drawIconSymbols(int submapId, ArrayList<HPObject> hpObjectArray) throws LibOvwException {

		HPObject hpObject;
		int count = hpObjectArray.size();
		OVwSymbolCreateList symlist = new OVwSymbolCreateList();
		symlist.count = count;
		symlist.symbols = new OVwSymbolCreateInfo[count];

		for (int i = 0; i < count; i++) {

			hpObject = hpObjectArray.get(i);

			symlist.symbols[i] = new OVwSymbolCreateInfo();
			symlist.symbols[i].submap_name_style = OVwSymbolCreateInfo.ovwSubmapIdValue;

			symlist.symbols[i].id = submapId;
			symlist.symbols[i].object_name_style = OVwSymbolCreateInfo.ovwObjectIdValue;

			symlist.symbols[i].object_id = hpObject.getId();
			symlist.symbols[i].status = hpObject.getStatus();
			symlist.symbols[i].status_source = OVwSymbolInfo.ovwSymbolStatusSource;
			symlist.symbols[i].symbol_type = hpObject.getHPSymbol().getSymbolType();
			symlist.symbols[i].symbol_variety = OVwSymbolInfo.ovwIconSymbol;

			symlist.symbols[i].position = null;
			symlist.symbols[i].label = hpObject.getLabel();

			HPLibovw.getInstance().OVwCreateSymbol(HPLibovw.getInstance().OVwGetMapInfo(), submapId,
			        symlist.symbols[i].object_id, symlist.symbols[i].symbol_type, symlist.symbols[i].label,
			        symlist.symbols[i].status, symlist.symbols[i].status_source, null,
			        OVwSymbolInfo.ovwMergeDefaultCapabilities);

		}

		// create symbols
		// HPLibovw.getInstance().OVwCreateSymbols(
		// HPLibovw.getInstance().OVwGetMapInfo(), symlist);
	}

	/**
	 * Draws connection symbols on the submap
	 * 
	 * @param submapId
	 *            a submap's id
	 * @param layout
	 *            a submap's layout
	 */
	public static void drawConnectSymbols(int submapId, int layout) {
		OVwSymbolList symbolList = HPLibovw.getInstance().OVwListSymbols(HPLibovw.getInstance().OVwGetMapInfo(),
		        submapId, OVwSymbolInfo.ovwAllPlanes, HPMain.APP_NAME);
		if (layout == OVwSubmapInfo.ovwBusLayout || layout == OVwSubmapInfo.ovwRingLayout) {
			for (int i = 0; i < symbolList.count; i++) {
				HPLibovw.getInstance().OVwCreateConnSymbol(HPLibovw.getInstance().OVwGetMapInfo(),
				        symbolList.symbols[i].object.object_id, OVwSymbolInfo.ovwSubmapBackbone,
				        symbolList.symbols[i].symbol_id, null, null, OVwSymbolInfo.ovwNormalStatus,
				        OVwSymbolInfo.ovwCompoundStatusSource, OVwSymbolInfo.ovwNoSymbolFlags);
			}
		} else {

			for (int i = 0; i < symbolList.count - 1; i++) {
				HPLibovw.getInstance().OVwCreateConnSymbol(HPLibovw.getInstance().OVwGetMapInfo(),
				        symbolList.symbols[i].object.object_id, symbolList.symbols[i].symbol_id,
				        symbolList.symbols[i + 1].symbol_id, null, null, OVwSymbolInfo.ovwNormalStatus,
				        OVwSymbolInfo.ovwCompoundStatusSource, OVwSymbolInfo.ovwNoSymbolFlags);
			}
		}
	}

}
