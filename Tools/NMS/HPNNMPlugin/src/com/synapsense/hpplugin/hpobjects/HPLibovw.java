/**
 * 
 */
package com.synapsense.hpplugin.hpobjects;

import hp.ov.libovw.LibOvw;

/**
 * This class handle a life time of <code>LibOvw</code> variable
 * 
 * @author Alexander Kravin
 * 
 */
public class HPLibovw extends LibOvw {
	private static HPLibovw _instance = null;
	private static boolean done = false;

	/**
	 * @return an instance of object
	 */
	public synchronized static HPLibovw getInstance() {
		if (_instance == null && done == false)
			_instance = new HPLibovw();
		return _instance;
	}

	@Override
	public void OVwDone() {
		done = true;
		super.OVwDone();
		_instance = null;
	}
}
