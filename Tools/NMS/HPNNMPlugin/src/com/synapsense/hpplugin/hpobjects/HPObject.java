/**
 * 
 */
package com.synapsense.hpplugin.hpobjects;

import com.synapsense.hpplugin.constants.EnumHPField;
import com.synapsense.hpplugin.constants.EnumHPSymbol;

import hp.ov.libovw.LibOvwException;
import hp.ov.libovw.OVwFieldBindList;
import hp.ov.libovw.OVwFieldBinding;
import hp.ov.libovw.OVwFieldValue;
import hp.ov.libovw.OVwSubmapInfo;
import hp.ov.libovw.OVwSymbolInfo;
import hp.ov.libovw.OVwSymbolList;

/**
 * This class represent an object from the HP Open View database and allows to
 * create new objects and fields
 * 
 * @author Alexander Kravin
 */
public class HPObject {

	private int id;
	private String label;
	private EnumHPSymbol hpSymbol;
	private HPFieldBindList hpFieldBindList;
	private int status = OVwSymbolInfo.ovwNormalStatus;

	/**
	 * Create a new instance.
	 * 
	 * @throws LibOvwException
	 *             if cannot create a new object.
	 */
	public HPObject() throws LibOvwException {
		hpFieldBindList = new HPFieldBindList();
		id = HPLibovw.getInstance().OVwDbCreateObjectBySelectionName(null);
	}

	/**
	 * Creates an instance on the basis of existing.
	 * 
	 * @param symbol
	 *            an <code>OVwSymbolInfo</code> instance.
	 */
	public HPObject(OVwSymbolInfo symbol) {
		this(symbol.object.object_id);
		label = symbol.symbol_label;
		hpSymbol = EnumHPSymbol.getValueBySymbolType(symbol.symbol_type);
	}

	/**
	 * Creates an instance on the basis of existing.
	 * 
	 * @param id
	 *            an object identifier
	 */
	public HPObject(int id) {
		this.id = id;
		hpFieldBindList = new HPFieldBindList();
		OVwFieldBindList fieldBindList = HPLibovw.getInstance().OVwDbGetFieldValues(id);
		hpFieldBindList.addFieldBindingList(fieldBindList);
	}

	/**
	 * Gets a parent object from the parent submap.
	 * 
	 * @return an <code>HPObject</code> instance
	 * 
	 * @throws LibOvwException
	 *             if cannot get a parent object
	 */
	public HPObject getParentHPObject() throws LibOvwException {
		if (hpSymbol.getLevel() != 0) {
			OVwSymbolList symbolList = HPLibovw.getInstance().OVwGetSymbolsByObject(
			        HPLibovw.getInstance().OVwGetMapInfo(), id);
			for (int i = 0; i < symbolList.count; i++) {
				OVwSymbolInfo symbol = symbolList.symbols[i];
				OVwSubmapInfo info = HPLibovw.getInstance().OVwGetSubmapInfo(HPLibovw.getInstance().OVwGetMapInfo(),
				        symbol.submap_id);

				// Getting a parent symbol's type
				OVwSymbolList parentSymbolList = HPLibovw.getInstance().OVwGetSymbolsByObject(
				        HPLibovw.getInstance().OVwGetMapInfo(), info.parent_object_id);

				for (int j = 0; j < parentSymbolList.count; j++) {
					OVwSymbolInfo parentSymbol = parentSymbolList.symbols[i];
					EnumHPSymbol parentHPSymbol = EnumHPSymbol.getValueBySymbolType(parentSymbol.symbol_type);
					if (parentHPSymbol == null)
						return null;

					HPObject hpObject = new HPObject(parentSymbol);

					if (hpObject == null)
						return null;

					return hpObject.getParentHPObject();
				}
			}
		}
		return this;
	}

	/**
	 * Adds a new binding field.
	 * 
	 * @param field
	 *            a field from <code>EnumHPField</code> class.
	 * @param fieldType
	 *            a field's type from <code>HPFieldType</code> class.
	 * @param value
	 *            a field's value.
	 */
	public void addFieldBinding(EnumHPField field, int fieldType, Object value) {

		int fldId = HPLibovw.getInstance().OVwDbFieldNameToFieldId(field.getDbName());
		OVwFieldBinding fldBind = new OVwFieldBinding();

		fldBind.field_val = new OVwFieldValue();

		fldBind.field_id = fldId;
		fldBind.field_val.field_type = fieldType;
		switch (fieldType) {
		case OVwFieldValue.ovwBooleanField:
			fldBind.field_val.bool_val = (Boolean) value;
			break;
		case OVwFieldValue.ovwIntField:
			fldBind.field_val.int_val = Integer.parseInt(value.toString());
			break;
		case OVwFieldValue.ovwStringField:
			fldBind.field_val.string_val = (String) value;
			break;
		case OVwFieldValue.ovwEnumField:
			fldBind.field_val.enum_val = Integer.parseInt(value.toString());
			break;
		}

		fldBind.field_val.is_list = false;

		// add field to object
		HPLibovw.getInstance().OVwDbSetFieldValue(id, fldBind.field_id, fldBind.field_val);
		hpFieldBindList.addFieldBinding(fldBind);
	}

	/**
	 * Adds a new binding field.
	 * 
	 * @param fldBind
	 *            an <code>OVwFieldBinding</code> instance.
	 */
	public void addFieldBinding(OVwFieldBinding fldBind) {
		hpFieldBindList.addFieldBinding(fldBind);
		HPLibovw.getInstance().OVwDbSetFieldValue(id, fldBind.field_id, fldBind.field_val);
	}

	/**
	 * @return an <code>HPFieldBindList</code> instance.
	 */
	public HPFieldBindList getHPFieldBindList() {
		return hpFieldBindList;
	}

	/**
	 * @return an object identifier.
	 */
	public int getId() {
		return id;
	}

	/**
	 * Sets an object's label.
	 * 
	 * @param label
	 *            a new object's label.
	 */
	public void setLabel(String label) {
		this.label = label;
	}

	/**
	 * @param status
	 *            a symbol's status.
	 */
	public void setStatus(int status) {
		this.status = status;
	}

	/**
	 * @return a status of symbol.
	 */
	public int getStatus() {
		return status;
	}

	/**
	 * Sets a type of symbol
	 * 
	 * @param hpSymbol
	 *            an <code>EnumHPSymbol</code> instance.
	 */
	public void setSymbolType(EnumHPSymbol hpSymbol) {
		this.hpSymbol = hpSymbol;
	}

	/**
	 * @return an object's label.
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * @return an object's type.
	 */
	public EnumHPSymbol getHPSymbol() {
		return hpSymbol;
	}

	@Override
	public String toString() {
		return "{id=" + id + ", label=\"" + label + "\", hpSymbol=\"" + hpSymbol + "\", status=" + status + "}";
	}
}
