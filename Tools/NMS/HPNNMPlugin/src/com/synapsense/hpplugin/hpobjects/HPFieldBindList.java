/**
 * 
 */
package com.synapsense.hpplugin.hpobjects;

import java.util.ArrayList;

import com.synapsense.hpplugin.constants.EnumHPField;

import hp.ov.libovw.OVwFieldBindList;
import hp.ov.libovw.OVwFieldBinding;
import hp.ov.libovw.OVwFieldIdList;
import hp.ov.libovw.OVwFieldValue;

/**
 * Class represents a wrap for field bindings of symbols.
 * 
 * @author Alexander Kravin
 */
public class HPFieldBindList {

	private ArrayList<OVwFieldBinding> arrayFieldBinding;

	/**
	 * Default constructor.
	 */
	public HPFieldBindList() {
		arrayFieldBinding = new ArrayList<OVwFieldBinding>();
	}

	/**
	 * Adds a new field binding in the array list.
	 * 
	 * @param field
	 *            a field's name from the <code>HPFieldName</code> class
	 * @param fieldType
	 *            a field's type from the <code>OVwFieldValue</code> class
	 * @param value
	 *            a field's value
	 */
	public void addField(EnumHPField field, int fieldType, Object value) {

		int fldId = HPLibovw.getInstance().OVwDbFieldNameToFieldId(field.getDbName());
		OVwFieldBinding fldBind = new OVwFieldBinding();
		fldBind.field_val = new OVwFieldValue();
		fldBind.field_id = fldId;
		fldBind.field_val.field_type = fieldType;

		switch (fieldType) {
		case OVwFieldValue.ovwBooleanField:
			fldBind.field_val.bool_val = (Boolean) value;
			break;

		case OVwFieldValue.ovwIntField:
			fldBind.field_val.int_val = (Integer) value;
			break;

		case OVwFieldValue.ovwStringField:
			fldBind.field_val.string_val = (String) value;
			break;

		case OVwFieldValue.ovwEnumField:
			fldBind.field_val.enum_val = (Integer) value;
			break;

		default:
			break;
		}
		fldBind.field_val.is_list = false;

		arrayFieldBinding.add(fldBind);
	}

	/**
	 * Gets a field binding by specified index
	 * 
	 * @param index
	 *            an array index
	 * 
	 * @return an <code>OVwFieldBinding</code> instance from array list
	 */
	public OVwFieldBinding getFieldBinding(int index) {
		int count = arrayFieldBinding.size();
		if (index < 0 || index > count)
			throw new IndexOutOfBoundsException("Index out of bounds");
		return arrayFieldBinding.get(index);
	}

	/**
	 * Gets a field binding by specified name
	 * 
	 * @param field
	 *            A field's name from class <code>HPFieldName</code>
	 * @return "OVwFieldBinding" object from array list
	 */
	public OVwFieldBinding getFieldBindingByName(EnumHPField field) {
		int count = arrayFieldBinding.size();
		for (int i = 0; i < count; i++) {
			String name = HPLibovw.getInstance().OVwDbFieldIdToFieldName(arrayFieldBinding.get(i).field_id);
			if (name.equals(field.getDbName())) {
				return arrayFieldBinding.get(i);
			}
		}
		return null;
	}

	/**
	 * Gets a value from HPOpenView database and converts it to string format
	 * 
	 * @param field
	 *            a field's name from the <code>EnumHPField</code> class
	 * 
	 * @return A string field's value
	 */
	public String getStringValue(EnumHPField field) {

		for (int i = 0; i < arrayFieldBinding.size(); i++) {
			String name = HPLibovw.getInstance().OVwDbFieldIdToFieldName(arrayFieldBinding.get(i).field_id);

			if (name.equals(field.getDbName())) {
				switch (arrayFieldBinding.get(i).field_val.field_type) {
				case OVwFieldValue.ovwBooleanField:
					return Boolean.toString(arrayFieldBinding.get(i).field_val.bool_val);
				case OVwFieldValue.ovwIntField:
					return Integer.toString(arrayFieldBinding.get(i).field_val.int_val);
				case OVwFieldValue.ovwStringField:
					return arrayFieldBinding.get(i).field_val.string_val;
				case OVwFieldValue.ovwEnumField:
					return HPLibovw.getInstance().OVwDbGetEnumName(arrayFieldBinding.get(i).field_id,
					        arrayFieldBinding.get(i).field_val.enum_val);
				default:
					throw new IllegalArgumentException("Illegal a field type");
				}
			}
		}
		return null;
	}

	/**
	 * Gets an <code>EnumHPField</code> instance by specified array index.
	 * 
	 * @param index
	 *            an array index.
	 * 
	 * @return an <code>EnumHPField</code> instance.
	 */
	public EnumHPField getHPField(int index) {
		String fieldName = HPLibovw.getInstance().OVwDbFieldIdToFieldName(arrayFieldBinding.get(index).field_id);
		return EnumHPField.getValueByFieldName(fieldName);
	}

	/**
	 * Adds a new field binding in the array list
	 * 
	 * @param fldBind
	 *            A new "OVwFieldBinding" object
	 */
	public void addFieldBinding(OVwFieldBinding fldBind) {
		if (fldBind == null) {
			throw new NullPointerException("Cannot add null field binding");
		}
		String fieldName = HPLibovw.getInstance().OVwDbFieldIdToFieldName(fldBind.field_id);
		EnumHPField hpField = EnumHPField.getValueByFieldName(fieldName);
		if (hpField != null) {
			arrayFieldBinding.add(fldBind);
		}
	}

	/**
	 * Adds all fields from parameter to the array list
	 * 
	 * @param fieldBindList
	 *            OVwFieldBindList instance
	 */
	public void addFieldBindingList(OVwFieldBindList fieldBindList) {
		for (int i = 0; i < fieldBindList.count; i++) {
			String fieldName = HPLibovw.getInstance().OVwDbFieldIdToFieldName(fieldBindList.fields[i].field_id);
			EnumHPField hpField = EnumHPField.getValueByFieldName(fieldName);
			if (hpField != null) {
				arrayFieldBinding.add(fieldBindList.fields[i]);
			}
		}
	}

	/**
	 * @return An object of OVwFieldBindList type
	 */
	public OVwFieldBindList getFieldBindList() {
		int count = arrayFieldBinding.size();
		OVwFieldBindList fldBindList = new OVwFieldBindList();

		fldBindList.count = count;
		fldBindList.fields = new OVwFieldBinding[count];

		for (int i = 0; i < count; i++) {
			fldBindList.fields[i] = arrayFieldBinding.get(i);
		}
		return fldBindList;
	}

	/**
	 * Creates OVwFieldIdList type object
	 * 
	 * @return OVwFieldIdList type object
	 */
	public OVwFieldIdList getFieldIdList() {
		int count = arrayFieldBinding.size();
		OVwFieldIdList fldIdList = new OVwFieldIdList();

		fldIdList.count = count;
		fldIdList.field_ids = new int[count];

		for (int i = 0; i < count; i++) {
			fldIdList.field_ids[i] = arrayFieldBinding.get(i).field_id;
		}
		return fldIdList;
	}

	/**
	 * Gets amount of elements from the array list
	 * 
	 * @return Amount of elements in the array list
	 */
	public int size() {
		return arrayFieldBinding.size();
	}
}
