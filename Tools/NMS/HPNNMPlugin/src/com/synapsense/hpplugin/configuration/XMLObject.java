/**
 * 
 */
package com.synapsense.hpplugin.configuration;

import java.util.ArrayList;

import org.snmp4j.smi.OID;

/**
 * Class represents a frame of the one entity from XML file.
 * 
 * @author Alexander Kravin
 */
public class XMLObject {

	private String name;
	private OID oid;
	private int refObjectId;
	private int refParentId;
	private int refNameId;

	private ArrayList<XMLProperty> arrayXMLProperty;

	/**
	 * Initializes class
	 * 
	 * @param objectName
	 *            an object's name from XML file
	 * @param oid
	 *            an object's ID from XML file
	 * @param refObjectId
	 *            a reference on ID of object in XML file
	 * @param refParentId
	 *            a reference on ID of parent object in XML file
	 * @param refNameId
	 *            a reference on ID of object's name in XML file
	 */
	public XMLObject(String objectName, String oid, int refObjectId, int refParentId, int refNameId) {

		arrayXMLProperty = new ArrayList<XMLProperty>();
		this.oid = new OID(oid);
		this.name = objectName;
		this.refObjectId = refObjectId;
		this.refParentId = refParentId;
		this.refNameId = refNameId;
	}

	/**
	 * Adds a new XML property
	 * 
	 * @param property
	 *            XMLProperty instance
	 */
	public void addProperty(XMLProperty property) {
		if (property == null)
			throw new IllegalArgumentException("Can not add a null XMLProperty object");
		arrayXMLProperty.add(property);
	}

	/**
	 * Gets XMLProperty instance by the property's name
	 * 
	 * @param propertyName
	 *            A property's name from XMLConstants class
	 * @return XMLProperty instance
	 */
	public XMLProperty getPropertyByName(String propertyName) {
		int count = arrayXMLProperty.size();
		for (int i = 0; i < count; i++) {
			XMLProperty xmlProperty = arrayXMLProperty.get(i);
			if (xmlProperty.getName().equals(propertyName)) {
				return xmlProperty;
			}
		}
		return null;
	}

	/**
	 * Gets XMLProperty instance by object's ID
	 * 
	 * @param id
	 *            Object ID from XML file
	 * @return XMLProperty instance
	 */
	/*
	 * public XMLProperty getPropertyById(String id) { int count =
	 * arrayXMLProperty.size(); for(int i=0; i<count; i++) { XMLProperty
	 * xmlProperty = arrayXMLProperty.get(i); if(xmlProperty.getId().equals(id))
	 * { return xmlProperty; } } return null; }
	 */

	/**
	 * Gets OIDs' array
	 * 
	 * @return OIDs' array
	 */
	public OID[] getOIDs() {
		int count = arrayXMLProperty.size();

		OID[] oids = new OID[count];
		for (int i = 0; i < count; i++) {
			OID newOID = new OID(oid);
			newOID.append(arrayXMLProperty.get(i).getId());
			oids[i] = newOID;
		}
		return oids;
	}

	/**
	 * Gets an object's OID by property's Id
	 * 
	 * @param id
	 *            An object's ID from XML file
	 * @return An object OID
	 */
	public OID getOIDById(String id) {
		OID newOID = new OID(oid);
		newOID.append(id);
		return newOID;
	}

	/**
	 * Gets objects's array of XML property
	 * 
	 * @return ArrayList<XMLProperty> instance
	 */
	public ArrayList<XMLProperty> getArrayXMLProperty() {
		return arrayXMLProperty;
	}

	/**
	 * Gets an index of array list by ID of object
	 * 
	 * @param xmlId
	 *            An object's Id from XML file
	 * @return Index from the array list
	 */
	public int getIndexByXmlObjectId(int xmlId) {
		int count = arrayXMLProperty.size();
		for (int i = 0; i < count; i++) {
			XMLProperty xmlProperty = arrayXMLProperty.get(i);
			if (xmlProperty.getId().equals("" + xmlId)) {
				return i;
			}
		}
		return 0;
	}

	/**
	 * Sets a reference on object's Id
	 * 
	 * @param refObjectId
	 *            A reference on the object's id from XML file
	 */
	public void setRefObjectId(int refObjectId) {
		this.refObjectId = refObjectId;
	}

	/**
	 * Sets a reference on the parent object's Id
	 * 
	 * @param refParentId
	 *            A reference on the parent object's id from XML file
	 */
	public void setRefParentId(int refParentId) {
		this.refParentId = refParentId;
	}

	/**
	 * Sets a reference on object name's Id
	 * 
	 * @param refNameId
	 *            A reference on object name's id from XML file
	 */
	public void setRefNameId(int refNameId) {
		this.refNameId = refNameId;
	}

	/**
	 * @return An object's name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return A reference on the object's Id
	 */
	public int getRefObjectId() {
		return refObjectId;
	}

	/**
	 * @return A reference on the parent object's Id
	 */
	public int getRefParentId() {
		return refParentId;
	}

	/**
	 * @return A reference on the object name's Id
	 */
	public int getRefNameId() {
		return refNameId;
	}

	@Override
	public String toString() {
		return "{name=\"" + name + "\", OID=\"" + oid + "\", " + "refObjectId=" + refObjectId + ", refParentId="
		        + refParentId + ", refNameId=" + refNameId + "}";
	}
}
