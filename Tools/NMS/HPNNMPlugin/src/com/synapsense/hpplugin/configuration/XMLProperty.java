/**
 * 
 */
package com.synapsense.hpplugin.configuration;

import com.synapsense.hpplugin.constants.EnumAccess;
import com.synapsense.hpplugin.constants.EnumSNMPType;
import com.synapsense.hpplugin.constants.EnumType;

/**
 * Class represents a wrap for one property from XML file.
 * 
 * 
 * <code>
 * <property ID="1" name="ID" type="UINT" snmpType="GAUGE32" min="0" max="4294967295" access="READ"/>
 * </code>
 * 
 * @author Alexander Kravin
 */
public class XMLProperty {

	private String id;
	private String name;
	private EnumType type;
	private EnumSNMPType snmpType;
	private EnumAccess access;
	private long min;
	private long max;

	/**
	 * Constructor without parameters. It makes nothing
	 */
	public XMLProperty() {
	}

	/**
	 * Initializes class
	 * 
	 * @param id
	 *            an attribute's value with name "ID" from XML file
	 * @param name
	 *            an attribute's value with name "name" from XML file
	 * @param type
	 *            an attribute's value with name "type" from XML file
	 * @param snmpType
	 *            an attribute's value with name "snmpType" from XML file
	 * @param min
	 *            an attribute's value with name "min" from XML file
	 * @param max
	 *            an attribute's value with name "max" from XML file
	 * @param access
	 *            an attribute's value with name "access" from XML file
	 */
	public XMLProperty(String id, String name, EnumType type, EnumSNMPType snmpType, long min, long max,
	        EnumAccess access) {

		this.id = id;
		this.name = name;
		this.snmpType = snmpType;
		this.type = type;
		this.min = min;
		this.max = max;
		this.access = access;
	}

	/**
	 * 
	 * @param id
	 *            an attribute's value with name "ID" from XML file
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 
	 * @param name
	 *            an attribute's value with name "name" from XML file
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 
	 * @param type
	 *            an attribute's value with name "type" from XML file
	 */
	public void setType(EnumType type) {
		this.type = type;
	}

	/**
	 * 
	 * @param snmpType
	 *            an attribute's value with name "snmpType" from XML file
	 */
	public void setSNMPType(EnumSNMPType snmpType) {
		this.snmpType = snmpType;
	}

	/**
	 * 
	 * @param min
	 *            an attribute's value with name "min" from XML file
	 */
	public void setMin(long min) {
		this.min = min;
	}

	/**
	 * 
	 * @param max
	 *            an attribute's value with name "max" from XML file
	 */
	public void setMax(long max) {
		this.max = max;
	}

	/**
	 * 
	 * @param access
	 *            an attribute's value with name "access" from XML file
	 */
	public void setAccess(EnumAccess access) {
		this.access = access;
	}

	/**
	 * @return an id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @return a name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return a type
	 */
	public EnumType getType() {
		return type;
	}

	/**
	 * @return SNMPType's value
	 */
	public EnumSNMPType getSNMPType() {
		return snmpType;
	}

	/**
	 * @return MIN's value
	 */
	public long getMin() {
		return min;
	}

	/**
	 * @return MAX's value
	 */
	public long getMax() {
		return max;
	}

	/**
	 * @return Access' value
	 */
	public EnumAccess getAccess() {
		return access;
	}

	@Override
	public String toString() {
		return "{Id=" + id + ", name=\"" + name + ", type=\"" + type + ", min=" + min + ", max=" + max + ", access=\""
		        + access + "\"}";
	}
}
